	.text
	.p2align 4,,15
	.globl	__wcpcpy
	.type	__wcpcpy, @function
__wcpcpy:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	call	__wcslen@PLT
	leaq	1(%rax), %rdx
	movq	%rax, %rbx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__wmemcpy
	leaq	(%rax,%rbx,4), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__wcpcpy, .-__wcpcpy
	.weak	wcpcpy
	.set	wcpcpy,__wcpcpy
	.hidden	__wmemcpy
