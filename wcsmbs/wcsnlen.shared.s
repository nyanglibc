	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcsnlen
	.type	__wcsnlen, @function
__wcsnlen:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rdx
	movq	%rsi, %rbx
	xorl	%esi, %esi
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__GI___wmemchr
	movq	%rax, %rdx
	subq	%rbp, %rdx
	sarq	$2, %rdx
	testq	%rax, %rax
	movq	%rbx, %rax
	cmovne	%rdx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__wcsnlen, .-__wcsnlen
	.weak	wcsnlen
	.set	wcsnlen,__wcsnlen
