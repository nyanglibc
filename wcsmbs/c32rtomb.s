	.text
	.p2align 4,,15
	.globl	c32rtomb
	.type	c32rtomb, @function
c32rtomb:
	leaq	state(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	jmp	wcrtomb
	.size	c32rtomb, .-c32rtomb
	.local	state
	.comm	state,8,8
	.hidden	wcrtomb
