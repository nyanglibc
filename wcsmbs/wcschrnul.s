	.text
	.p2align 4,,15
	.globl	__wcschrnul
	.type	__wcschrnul, @function
__wcschrnul:
	movl	(%rdi), %edx
	movq	%rdi, %rax
	cmpl	%edx, %esi
	jne	.L7
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$4, %rax
	movl	(%rax), %edx
	cmpl	%esi, %edx
	je	.L2
.L7:
	testl	%edx, %edx
	jne	.L9
.L2:
	rep ret
	.size	__wcschrnul, .-__wcschrnul
	.weak	wcschrnul
	.set	wcschrnul,__wcschrnul
