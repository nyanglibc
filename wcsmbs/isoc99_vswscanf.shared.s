	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isoc99_vswscanf
	.hidden	__GI___isoc99_vswscanf
	.type	__GI___isoc99_vswscanf, @function
__GI___isoc99_vswscanf:
	pushq	%r13
	pushq	%r12
	leaq	_IO_wstr_jumps(%rip), %r8
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movl	$32768, %esi
	subq	$488, %rsp
	xorl	%edx, %edx
	leaq	240(%rsp), %rbx
	movq	%rsp, %rcx
	movq	$0, 376(%rsp)
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_IO_fwide@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_IO_wstr_init_static@PLT
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$2, %ecx
	call	__vfwscanf_internal
	addq	$488, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___isoc99_vswscanf, .-__GI___isoc99_vswscanf
	.globl	__isoc99_vswscanf
	.set	__isoc99_vswscanf,__GI___isoc99_vswscanf
	.hidden	__vfwscanf_internal
	.hidden	_IO_wstr_jumps
