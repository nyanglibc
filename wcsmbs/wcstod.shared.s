	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstod_internal
	.hidden	__GI___wcstod_internal
	.type	__GI___wcstod_internal, @function
__GI___wcstod_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstod_l_internal
	.size	__GI___wcstod_internal, .-__GI___wcstod_internal
	.globl	__wcstod_internal
	.set	__wcstod_internal,__GI___wcstod_internal
	.p2align 4,,15
	.weak	__GI_wcstod
	.hidden	__GI_wcstod
	.type	__GI_wcstod, @function
__GI_wcstod:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstod_l_internal
	.size	__GI_wcstod, .-__GI_wcstod
	.globl	wcstod
	.set	wcstod,__GI_wcstod
	.weak	wcstof32x
	.set	wcstof32x,wcstod
	.weak	wcstof64
	.set	wcstof64,wcstod
	.hidden	____wcstod_l_internal
