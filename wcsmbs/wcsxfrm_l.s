	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../string/strxfrm_l.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"((uintptr_t) l_data.table) % __alignof__ (l_data.table[0]) == 0"
	.align 8
.LC2:
	.string	"((uintptr_t) l_data.weights) % __alignof__ (l_data.weights[0]) == 0"
	.align 8
.LC3:
	.string	"((uintptr_t) l_data.extra) % __alignof__ (l_data.extra[0]) == 0"
	.align 8
.LC4:
	.string	"((uintptr_t) l_data.indirect) % __alignof__ (l_data.indirect[0]) == 0"
	.text
	.p2align 4,,15
	.globl	__wcsxfrm_l
	.hidden	__wcsxfrm_l
	.type	__wcsxfrm_l, @function
__wcsxfrm_l:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$152, %rsp
	movq	24(%rcx), %rax
	movq	%rdi, -128(%rbp)
	movq	%rsi, -184(%rbp)
	movq	%rdx, -88(%rbp)
	movl	64(%rax), %ebx
	testq	%rbx, %rbx
	movq	%rbx, -80(%rbp)
	jne	.L2
	movq	%rdx, %r15
	movq	%rsi, %rdi
	call	__wcslen@PLT
	testq	%r15, %r15
	movq	%rax, %rbx
	jne	.L464
.L1:
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L2:
	movq	-184(%rbp), %rbx
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L465
	movq	72(%rax), %rbx
	movq	152(%rax), %rdi
	movq	144(%rax), %r13
	movq	%rbx, -136(%rbp)
	movq	136(%rax), %rbx
	movq	160(%rax), %rax
	movq	%rdi, -96(%rbp)
	movq	%rbx, -56(%rbp)
	andl	$3, %ebx
	movq	%rax, -112(%rbp)
	jne	.L466
	testb	$3, %r13b
	jne	.L467
	testb	$3, -96(%rbp)
	jne	.L468
	testb	$3, -112(%rbp)
	jne	.L469
	subq	$16400, %rsp
	xorl	%r14d, %r14d
	movq	%r13, -72(%rbp)
	leaq	15(%rsp), %rax
	subq	$4112, %rsp
	movq	%r14, %r15
	movq	-184(%rbp), %r12
	andq	$-16, %rax
	movq	%rax, %rbx
	movq	%rax, -104(%rbp)
	leaq	15(%rsp), %rax
	movq	%rbx, %r13
	andq	$-16, %rax
	movq	%rax, -64(%rbp)
	movq	%rax, %r14
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L264:
	movq	%rbx, %r12
.L9:
	movl	%eax, %edx
	andl	$16777215, %eax
	sarl	$24, %edx
	movl	%eax, 0(%r13,%r15,4)
	leaq	1(%r15), %rax
	movb	%dl, (%r14,%r15)
	movl	(%r12), %edx
	testl	%edx, %edx
	je	.L25
	cmpq	$4095, %rax
	je	.L470
	movq	%rax, %r15
.L26:
	movl	(%r12), %esi
	movq	-56(%rbp), %rdi
	leaq	4(%r12), %rbx
	call	__collidx_table_lookup
	testl	%eax, %eax
	jns	.L264
	movq	-96(%rbp), %rdi
	negl	%eax
	cltq
	leaq	(%rdi,%rax,4), %rdx
.L10:
	movl	(%rdx), %eax
	leaq	8(%rdx), %rdi
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	js	.L471
.L11:
	cmpq	$-2, %rcx
	movq	$-2, %rsi
	cmovbe	%rcx, %rsi
	testq	%rcx, %rcx
	je	.L15
	movl	4(%r12), %r9d
	cmpl	%r9d, 8(%rdx)
	jne	.L16
	xorl	%edx, %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	movl	4(%r12,%rdx,4), %r11d
	cmpl	%r11d, (%rdi,%rdx,4)
	jne	.L18
.L17:
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	ja	.L19
.L18:
	cmpq	%rdx, %rcx
	leaq	0(,%rcx,4), %rsi
	je	.L472
.L20:
	leaq	(%rdi,%rsi), %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %rdi
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	jns	.L11
.L471:
	movq	%rcx, %rsi
	subq	$1, %rsi
	je	.L12
	movl	4(%r12), %r9d
	cmpl	%r9d, 8(%rdx)
	jne	.L13
	xorl	%edx, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L22:
	movl	4(%r12,%rdx,4), %r10d
	cmpl	%r10d, (%rdi,%rdx,4)
	jne	.L13
.L14:
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L22
.L12:
	leaq	0(,%rcx,4), %r11
	movl	(%r12,%rcx,4), %esi
	leaq	0(,%rcx,8), %rdx
	movl	-4(%rdi,%r11), %r10d
	cmpl	%esi, %r10d
	jg	.L453
	cmpl	-4(%rdi,%rdx), %esi
	jg	.L453
	leaq	(%rbx,%r11), %r12
	subl	%r10d, %esi
	negl	%eax
	movq	-112(%rbp), %rbx
	movslq	%esi, %rdx
	cltq
	addq	%rdx, %rax
	movl	(%rbx,%rax,4), %eax
	jmp	.L9
.L465:
	xorl	%ebx, %ebx
	cmpq	$0, -88(%rbp)
	je	.L1
	movq	-128(%rbp), %rax
	movl	$0, (%rax)
	jmp	.L1
.L13:
	leaq	(%rdi,%rcx,8), %rdx
	jmp	.L10
.L453:
	addq	%rdi, %rdx
	jmp	.L10
.L472:
	movq	%rsi, %rcx
.L15:
	leaq	(%rbx,%rcx), %r12
	jmp	.L9
.L25:
	movq	%r15, %r14
	movq	%rax, %r15
	movq	-64(%rbp), %rax
	movq	-104(%rbp), %rbx
	movq	-88(%rbp), %r11
	movq	-72(%rbp), %r13
	movq	%r15, -56(%rbp)
	movb	$0, (%rax,%r15)
	movzbl	(%rax), %eax
	leaq	-4(%rbx,%r14,4), %rdi
	movq	$0, -112(%rbp)
	movq	$0, -72(%rbp)
	imulq	-80(%rbp), %rax
	movq	%rdi, -160(%rbp)
	leaq	-4(%rbx,%r15,4), %rdi
	movq	%r14, -176(%rbp)
	movq	%rdi, -168(%rbp)
	movq	-136(%rbp), %rdi
	addq	%rdi, %rax
	movq	%rax, -152(%rbp)
	movq	%rbx, %rax
	leaq	-4(%rbx), %rbx
	movq	%rax, %r15
	movq	%rbx, -120(%rbp)
.L63:
	movq	-152(%rbp), %rax
	movq	-72(%rbp), %rbx
	movzbl	(%rax,%rbx), %edi
	movl	%edi, %eax
	testb	$4, %al
	movq	-112(%rbp), %rax
	jne	.L266
	movq	-136(%rbp), %rsi
	xorl	%r8d, %r8d
	movq	$-1, %rcx
	addq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L38:
	andl	$1, %edi
	je	.L29
	cmpq	$-1, %rcx
	je	.L30
	cmpq	%r8, %rcx
	jnb	.L30
	movq	-120(%rbp), %rdi
	leaq	(%rdi,%r8,4), %rsi
	leaq	(%rdi,%rcx,4), %r12
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L31:
	addl	%edx, %r9d
	movq	%rdi, %rax
	movl	%r9d, (%rsi)
.L32:
	subq	$4, %rsi
	cmpq	%rsi, %r12
	je	.L30
.L34:
	movslq	(%rsi), %rdx
	leal	1(%rdx), %r9d
	movq	%rdx, %rcx
	leaq	0(,%rdx,4), %r10
	movl	0(%r13,%rdx,4), %edx
	movl	%r9d, (%rsi)
	leaq	(%rdx,%rax), %rdi
	cmpq	%rdi, %r11
	jbe	.L31
	testq	%rdx, %rdx
	leaq	-1(%rdx), %r14
	je	.L32
	movq	-128(%rbp), %r9
	leaq	4(%r13,%r10), %r10
	salq	$2, %rdx
	movl	%ecx, -96(%rbp)
	leaq	(%r9,%rax,4), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%r10,%rax), %ecx
	movl	%ecx, (%r9,%rax)
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L33
	movl	-96(%rbp), %ecx
	subq	$4, %rsi
	leal	2(%rcx,%r14), %eax
	movl	%eax, 4(%rsi)
	cmpq	%rsi, %r12
	movq	%rdi, %rax
	jne	.L34
.L30:
	movslq	(%r15,%r8,4), %rdx
	leal	1(%rdx), %ecx
	movq	%rdx, %rsi
	leaq	0(,%rdx,4), %r10
	movl	0(%r13,%rdx,4), %edx
	movl	%ecx, (%r15,%r8,4)
	leaq	(%rdx,%rax), %rdi
	cmpq	%rdi, %r11
	ja	.L473
	addl	%edx, %ecx
	movq	%rdi, %rax
	movl	%ecx, (%r15,%r8,4)
	movq	$-1, %rcx
.L36:
	movq	-64(%rbp), %rdi
	addq	$1, %r8
	movzbl	(%rdi,%r8), %edx
	imulq	-80(%rbp), %rdx
	cmpq	%r8, -56(%rbp)
	movzbl	(%rbx,%rdx), %edi
	jne	.L38
	cmpq	$-1, %rcx
	leaq	1(%rax), %rdx
	je	.L41
	cmpq	%rcx, -56(%rbp)
	jbe	.L41
	movq	-120(%rbp), %rbx
	movq	-168(%rbp), %rsi
	leaq	(%rbx,%rcx,4), %rbx
	jmp	.L45
.L42:
	addl	%ecx, %edx
	movq	%r8, %rax
	movl	%edx, (%rsi)
.L43:
	subq	$4, %rsi
	cmpq	%rsi, %rbx
	je	.L474
.L45:
	movslq	(%rsi), %rcx
	leal	1(%rcx), %edx
	movq	%rcx, %rdi
	leaq	0(,%rcx,4), %r10
	movl	0(%r13,%rcx,4), %ecx
	movl	%edx, (%rsi)
	leaq	(%rcx,%rax), %r8
	cmpq	%r8, %r11
	jbe	.L42
	testq	%rcx, %rcx
	leaq	-1(%rcx), %r9
	je	.L43
	movq	-128(%rbp), %rdx
	leaq	4(%r13,%r10), %r12
	salq	$2, %rcx
	leaq	(%rdx,%rax,4), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L44:
	movl	(%r12,%rax), %edx
	movl	%edx, (%r10,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L44
	leal	2(%rdi,%r9), %eax
	subq	$4, %rsi
	movl	%eax, 4(%rsi)
	cmpq	%rsi, %rbx
	movq	%r8, %rax
	jne	.L45
.L474:
	leaq	1(%rax), %rdx
.L41:
	addq	$1, -72(%rbp)
	cmpq	%rax, %r11
	movq	-72(%rbp), %rbx
	jbe	.L62
	xorl	%ecx, %ecx
	cmpq	%rbx, -80(%rbp)
	movq	-128(%rbp), %rbx
	seta	%cl
	movl	%ecx, (%rbx,%rax,4)
.L62:
	movq	-72(%rbp), %rdi
	cmpq	%rdi, -80(%rbp)
	je	.L475
	movq	%rdx, -112(%rbp)
	jmp	.L63
.L29:
	cmpq	$-1, %rcx
	cmove	%r8, %rcx
	jmp	.L36
.L266:
	movq	-136(%rbp), %rbx
	movq	-72(%rbp), %rsi
	movl	$1, %r10d
	xorl	%r8d, %r8d
	movq	$-1, %rcx
	movq	%rax, %r14
	movq	%r11, -96(%rbp)
	addq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L28:
	andl	$1, %edi
	je	.L46
	cmpq	$-1, %rcx
	je	.L47
	cmpq	%r8, %rcx
	jnb	.L47
	movq	-120(%rbp), %rax
	movq	%r8, -104(%rbp)
	leaq	(%rax,%r8,4), %rsi
	leaq	(%rax,%rcx,4), %r11
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L478:
	leaq	1(%rcx,%r14), %r12
	cmpq	%r12, -96(%rbp)
	ja	.L476
.L49:
	addl	%r9d, %edx
	subq	$4, %rsi
	movl	%edi, %r10d
	movl	%edx, 4(%rsi)
	cmpq	%rsi, %r11
	movq	%r12, %r14
	je	.L477
.L52:
	movslq	(%rsi), %rcx
	leal	1(%rcx), %edx
	leaq	0(,%rcx,4), %rax
	movl	0(%r13,%rcx,4), %ecx
	movl	%edx, (%rsi)
	testq	%rcx, %rcx
	movq	%rcx, %r9
	jne	.L478
	subq	$4, %rsi
	addl	$1, %r10d
	cmpq	%rsi, %r11
	jne	.L52
.L477:
	movq	-104(%rbp), %r8
.L47:
	movslq	(%r15,%r8,4), %rdx
	leal	1(%rdx), %eax
	leaq	0(,%rdx,4), %r9
	movl	0(%r13,%rdx,4), %edx
	movl	%eax, (%r15,%r8,4)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	je	.L53
	leaq	1(%rdx,%r14), %rsi
	cmpq	%rsi, -96(%rbp)
	ja	.L479
.L54:
	addl	%ecx, %eax
	movl	%edi, %r10d
	movq	%rsi, %r14
	movl	%eax, (%r15,%r8,4)
	movq	$-1, %rcx
.L56:
	movq	-64(%rbp), %rax
	addq	$1, %r8
	movzbl	(%rax,%r8), %eax
	imulq	-80(%rbp), %rax
	cmpq	%r8, -56(%rbp)
	movzbl	(%rbx,%rax), %edi
	jne	.L28
	leaq	1(%r14), %r8
	cmpq	$-1, %rcx
	movq	%r14, %rax
	movq	-96(%rbp), %r11
	movq	%r8, %rdx
	je	.L41
	cmpq	%rcx, -176(%rbp)
	jbe	.L41
	movq	-120(%rbp), %rbx
	movq	-160(%rbp), %rdi
	leaq	(%rbx,%rcx,4), %r12
	jmp	.L61
.L481:
	addq	%rsi, %r8
	cmpq	%r8, %r11
	ja	.L480
.L58:
	leal	(%rcx,%r9), %eax
	leaq	1(%r8), %rdx
	movl	$1, %r10d
	movl	%eax, (%rdi)
	movq	%r8, %rax
.L60:
	subq	$4, %rdi
	cmpq	%rdi, %r12
	je	.L41
	leaq	1(%rax), %r8
.L61:
	movslq	(%rdi), %rdx
	movl	0(%r13,%rdx,4), %esi
	leal	1(%rdx), %ecx
	leaq	0(,%rdx,4), %rbx
	movq	%r8, %rdx
	movl	%ecx, (%rdi)
	testq	%rsi, %rsi
	movq	%rsi, %r9
	jne	.L481
	addl	$1, %r10d
	jmp	.L60
.L46:
	cmpq	$-1, %rcx
	cmove	%r8, %rcx
	jmp	.L56
.L53:
	addl	$1, %r10d
	movq	$-1, %rcx
	jmp	.L56
.L476:
	movq	-128(%rbp), %r8
	movl	%edx, -144(%rbp)
	leaq	(%r8,%r14,4), %r14
	movq	-104(%rbp), %r8
	movl	%r10d, (%r14)
	leaq	4(%r13,%rax), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L50:
	movl	(%r10,%rax,4), %edx
	movl	%edx, 4(%r14,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L50
	movl	-144(%rbp), %edx
	movq	%r8, -104(%rbp)
	jmp	.L49
.L473:
	testq	%rdx, %rdx
	leaq	-1(%rdx), %r9
	movq	$-1, %rcx
	je	.L36
	leaq	4(%r13,%r10), %rcx
	movq	-128(%rbp), %r10
	salq	$2, %rdx
	leaq	(%r10,%rax,4), %r10
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L37:
	movl	(%rcx,%rax), %r12d
	movl	%r12d, (%r10,%rax)
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L37
	leal	2(%rsi,%r9), %eax
	movq	$-1, %rcx
	movl	%eax, (%r15,%r8,4)
	movq	%rdi, %rax
	jmp	.L36
.L16:
	leaq	0(,%rcx,4), %rsi
	jmp	.L20
.L464:
	leaq	1(%rax), %rax
	movq	%r15, %rdi
	movq	-184(%rbp), %rsi
	cmpq	%r15, %rax
	cmovbe	%rax, %rdi
	movq	%rdi, %rdx
	movq	-128(%rbp), %rdi
	call	__wcpncpy@PLT
	jmp	.L1
.L479:
	movq	-128(%rbp), %r11
	leaq	(%r11,%r14,4), %r12
	movl	%r10d, (%r12)
	leaq	4(%r13,%r9), %r10
	xorl	%r9d, %r9d
.L55:
	movl	(%r10,%r9,4), %r11d
	movl	%r11d, 4(%r12,%r9,4)
	addq	$1, %r9
	cmpq	%r9, %rdx
	jne	.L55
	jmp	.L54
.L480:
	movq	-128(%rbp), %rdx
	leaq	(%rdx,%rax,4), %r14
	xorl	%eax, %eax
	movl	%r10d, (%r14)
	leaq	4(%r13,%rbx), %r10
.L59:
	movl	(%r10,%rax,4), %edx
	movl	%edx, 4(%r14,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L59
	jmp	.L58
.L475:
	cmpq	$2, %rdx
	jbe	.L64
	movq	-112(%rbp), %rcx
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	je	.L482
.L64:
	leaq	-1(%rdx), %rbx
	jmp	.L1
.L482:
	cmpq	%rax, -88(%rbp)
	movq	%rax, %rdx
	jb	.L64
	movq	-128(%rbp), %rax
	movl	$0, -4(%rax,%rdx,4)
	jmp	.L64
.L470:
	movq	-64(%rbp), %rax
	movq	-72(%rbp), %r13
	movq	$0, -192(%rbp)
	movq	$0, -104(%rbp)
	movb	$0, 4095(%rax)
.L261:
	movq	-184(%rbp), %rax
	movl	-104(%rbp), %r8d
	movq	-56(%rbp), %rdi
	movl	(%rax), %esi
	movl	%r8d, -64(%rbp)
	call	__collidx_table_lookup
	testl	%eax, %eax
	movl	-64(%rbp), %r8d
	js	.L483
.L65:
	sarl	$24, %eax
	movslq	%r8d, %rdx
	movq	-184(%rbp), %r12
	movzbl	%al, %eax
	imulq	-80(%rbp), %rax
	addq	-136(%rbp), %rax
	testb	$4, (%rax,%rdx)
	movq	-192(%rbp), %rax
	movq	%rax, -72(%rbp)
	je	.L272
	movl	(%r12), %esi
	xorl	%r15d, %r15d
	movl	$1, -120(%rbp)
	movq	%r15, %r10
	movq	$0, -64(%rbp)
	movq	%r13, %r15
	movl	%r8d, %r13d
	testl	%esi, %esi
	je	.L484
	.p2align 4,,10
	.p2align 3
.L217:
	movq	-56(%rbp), %rdi
	leaq	4(%r12), %rax
	movq	%r10, -144(%rbp)
	movq	%rax, -160(%rbp)
	call	__collidx_table_lookup
	testl	%eax, %eax
	movq	-144(%rbp), %r10
	js	.L485
.L151:
	movl	%eax, %ebx
	andl	$16777215, %eax
	leal	1(%rax), %esi
	cltq
	shrl	$24, %ebx
	movl	(%r15,%rax,4), %ecx
	testl	%r13d, %r13d
	movl	%ebx, %edx
	movl	%esi, -176(%rbp)
	movq	%rcx, -152(%rbp)
	jle	.L167
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L168:
	addl	%esi, %ecx
	addl	$1, %eax
	leal	1(%rcx), %esi
	cmpl	%eax, %r13d
	movslq	%ecx, %rcx
	movl	(%r15,%rcx,4), %ecx
	jne	.L168
	movl	%esi, -176(%rbp)
	movq	%rcx, -152(%rbp)
.L167:
	movzbl	%dl, %edx
	movq	-136(%rbp), %rax
	addq	-104(%rbp), %rax
	imulq	-80(%rbp), %rdx
	testb	$1, (%rax,%rdx)
	je	.L169
	testq	%r10, %r10
	je	.L170
	cmpq	$0, -64(%rbp)
	je	.L170
	leaq	4(%r10), %rax
	movq	%r10, %r14
	movq	%rax, -168(%rbp)
	.p2align 4,,10
	.p2align 3
.L212:
	movl	(%r14), %esi
	movq	-56(%rbp), %rdi
	call	__collidx_table_lookup
	testl	%eax, %eax
	movq	-168(%rbp), %rbx
	js	.L486
.L171:
	andl	$16777215, %eax
	testl	%r13d, %r13d
	leal	1(%rax), %ecx
	cltq
	movl	(%r15,%rax,4), %eax
	jle	.L187
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L188:
	addl	%ecx, %eax
	addl	$1, %edx
	leal	1(%rax), %ecx
	cmpl	%edx, %r13d
	cltq
	movl	(%r15,%rax,4), %eax
	jne	.L188
.L187:
	cmpq	$1, -64(%rbp)
	jbe	.L189
	movq	%rbx, %rax
	movq	%r14, -144(%rbp)
	movl	%r13d, %ebx
	movl	$1, %r12d
	movq	%r15, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L208:
	movl	0(%r13), %esi
	movq	-56(%rbp), %rdi
	leaq	4(%r13), %r15
	call	__collidx_table_lookup
	testl	%eax, %eax
	js	.L487
	movq	%r15, %r13
.L190:
	andl	$16777215, %eax
	testl	%ebx, %ebx
	leal	1(%rax), %ecx
	cltq
	movl	(%r14,%rax,4), %eax
	jle	.L206
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L207:
	addl	%ecx, %eax
	addl	$1, %esi
	leal	1(%rax), %ecx
	cmpl	%esi, %ebx
	cltq
	movl	(%r14,%rax,4), %eax
	jne	.L207
.L206:
	addq	$1, %r12
	cmpq	-64(%rbp), %r12
	jne	.L208
	movq	%r14, %r15
	movq	-144(%rbp), %r14
	movl	%ebx, %r13d
.L189:
	testq	%rax, %rax
	je	.L209
	movq	-72(%rbp), %rbx
	leaq	1(%rbx,%rax), %rsi
	cmpq	%rsi, -88(%rbp)
	ja	.L488
.L281:
	movq	%rsi, -72(%rbp)
	movl	$1, -120(%rbp)
.L210:
	subq	$1, -64(%rbp)
	jne	.L212
.L170:
	movq	-152(%rbp), %rax
	testq	%rax, %rax
	je	.L213
	movq	-72(%rbp), %rbx
	leaq	1(%rbx,%rax), %rcx
	cmpq	%rcx, -88(%rbp)
	ja	.L489
.L282:
	movq	%rcx, -72(%rbp)
	xorl	%r10d, %r10d
	movl	$1, -120(%rbp)
.L214:
	movq	-160(%rbp), %r12
	movl	(%r12), %esi
	testl	%esi, %esi
	jne	.L217
.L484:
	movq	-72(%rbp), %rax
	movl	%r13d, %r8d
	movq	%r15, %r13
	addq	$1, %rax
	testq	%r10, %r10
	movq	%rax, -144(%rbp)
	movq	%rax, %rcx
	je	.L150
	cmpq	$0, -64(%rbp)
	je	.L150
	leaq	4(%r10), %r14
	movq	%r13, %r15
	movl	%r8d, %r13d
	movq	%r14, -160(%rbp)
	movq	%r10, %r14
.L259:
	movl	(%r14), %esi
	movq	-56(%rbp), %rdi
	call	__collidx_table_lookup
	testl	%eax, %eax
	movq	-160(%rbp), %rbx
	js	.L490
.L218:
	andl	$16777215, %eax
	testl	%r13d, %r13d
	leal	1(%rax), %edx
	cltq
	movl	(%r15,%rax,4), %eax
	jle	.L234
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L235:
	addl	%edx, %eax
	addl	$1, %ecx
	leal	1(%rax), %edx
	cmpl	%ecx, %r13d
	cltq
	movl	(%r15,%rax,4), %eax
	jne	.L235
.L234:
	cmpq	$1, -64(%rbp)
	jbe	.L236
	movq	%rbx, %rax
	movq	%r14, -152(%rbp)
	movl	%r13d, %ebx
	movl	$1, %r12d
	movq	%r15, %r14
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L255:
	movl	0(%r13), %esi
	movq	-56(%rbp), %rdi
	leaq	4(%r13), %r15
	call	__collidx_table_lookup
	testl	%eax, %eax
	js	.L491
	movq	%r15, %r13
.L237:
	andl	$16777215, %eax
	testl	%ebx, %ebx
	leal	1(%rax), %edx
	cltq
	movl	(%r14,%rax,4), %eax
	jle	.L253
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L254:
	addl	%edx, %eax
	addl	$1, %esi
	leal	1(%rax), %edx
	cmpl	%esi, %ebx
	cltq
	movl	(%r14,%rax,4), %eax
	jne	.L254
.L253:
	addq	$1, %r12
	cmpq	-64(%rbp), %r12
	jne	.L255
	movq	%r14, %r15
	movq	-152(%rbp), %r14
	movl	%ebx, %r13d
.L236:
	testq	%rax, %rax
	movq	-144(%rbp), %rcx
	je	.L256
	leaq	(%rax,%rcx), %rsi
	cmpq	%rsi, -88(%rbp)
	leaq	1(%rsi), %rcx
	ja	.L492
.L286:
	subq	$1, -64(%rbp)
	movq	%rsi, -72(%rbp)
	movl	$1, -120(%rbp)
	je	.L451
.L504:
	movq	-72(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -144(%rbp)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L487:
	movq	-96(%rbp), %rdi
	negl	%eax
	cltq
	leaq	(%rdi,%rax,4), %rcx
.L191:
	movl	(%rcx), %eax
	leaq	8(%rcx), %rdi
	movslq	4(%rcx), %rsi
	testl	%eax, %eax
	js	.L493
.L192:
	cmpq	$-2, %rsi
	movq	$-2, %r8
	cmovbe	%rsi, %r8
	testq	%rsi, %rsi
	je	.L196
	movl	4(%r13), %edx
	cmpl	%edx, 8(%rcx)
	jne	.L197
	xorl	%edx, %edx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L200:
	movl	4(%r13,%rdx,4), %ecx
	cmpl	%ecx, (%rdi,%rdx,4)
	jne	.L199
.L198:
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jb	.L200
.L199:
	cmpq	%rdx, %rsi
	leaq	0(,%rsi,4), %rcx
	je	.L494
.L201:
	addq	%rdi, %rcx
	movl	(%rcx), %eax
	leaq	8(%rcx), %rdi
	movslq	4(%rcx), %rsi
	testl	%eax, %eax
	jns	.L192
.L493:
	movq	%rsi, %r8
	subq	$1, %r8
	je	.L193
	movl	8(%rcx), %ecx
	cmpl	%ecx, 4(%r13)
	jne	.L194
	xorl	%edx, %edx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L203:
	movl	4(%r13,%rdx,4), %ecx
	cmpl	%ecx, (%rdi,%rdx,4)
	jne	.L194
.L195:
	addq	$1, %rdx
	cmpq	%r8, %rdx
	jne	.L203
.L193:
	leaq	0(,%rsi,4), %rcx
	movl	0(%r13,%rsi,4), %r10d
	leaq	0(,%rsi,8), %rdx
	movl	-4(%rdi,%rcx), %r11d
	cmpl	%r10d, %r11d
	jg	.L460
	cmpl	-4(%rdi,%rdx), %r10d
	jg	.L460
	subl	%r11d, %r10d
	negl	%eax
	movq	-112(%rbp), %rdi
	movslq	%r10d, %r10
	cltq
	leaq	(%r15,%rcx), %r13
	addq	%r10, %rax
	movl	(%rdi,%rax,4), %eax
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	(%rdi,%rsi,8), %rcx
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L460:
	leaq	(%rdi,%rdx), %rcx
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L209:
	addl	$1, -120(%rbp)
	jmp	.L210
.L494:
	movq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L196:
	leaq	(%r15,%rsi), %r13
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L197:
	leaq	0(,%rsi,4), %rcx
	jmp	.L201
.L169:
	testq	%r10, %r10
	cmove	%r12, %r10
	addq	$1, -64(%rbp)
	jmp	.L214
.L486:
	movq	-96(%rbp), %rbx
	negl	%eax
	cltq
	leaq	(%rbx,%rax,4), %rbx
.L172:
	movl	(%rbx), %eax
	leaq	8(%rbx), %rsi
	movslq	4(%rbx), %rcx
	testl	%eax, %eax
	js	.L495
.L173:
	cmpq	$-2, %rcx
	movq	$-2, %rdi
	cmovbe	%rcx, %rdi
	testq	%rcx, %rcx
	je	.L177
	movl	8(%rbx), %ebx
	cmpl	%ebx, 4(%r14)
	jne	.L178
	xorl	%edx, %edx
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L181:
	movl	4(%r14,%rdx,4), %ebx
	cmpl	%ebx, (%rsi,%rdx,4)
	jne	.L180
.L179:
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jb	.L181
.L180:
	cmpq	%rdx, %rcx
	leaq	0(,%rcx,4), %rbx
	je	.L496
.L459:
	addq	%rsi, %rbx
	movl	(%rbx), %eax
	leaq	8(%rbx), %rsi
	movslq	4(%rbx), %rcx
	testl	%eax, %eax
	jns	.L173
.L495:
	movq	%rcx, %rdi
	subq	$1, %rdi
	je	.L174
	movl	4(%r14), %edx
	cmpl	%edx, 8(%rbx)
	jne	.L175
	xorl	%edx, %edx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L184:
	movl	4(%r14,%rdx,4), %ebx
	cmpl	%ebx, (%rsi,%rdx,4)
	jne	.L175
.L176:
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L184
.L174:
	leaq	0(,%rcx,4), %r9
	movl	(%r14,%rcx,4), %edx
	leaq	0(,%rcx,8), %rbx
	movl	-4(%rsi,%r9), %edi
	cmpl	%edx, %edi
	jg	.L459
	cmpl	-4(%rsi,%rbx), %edx
	jg	.L459
	subl	%edi, %edx
	movq	-168(%rbp), %rbx
	negl	%eax
	movq	-112(%rbp), %rdi
	movslq	%edx, %rdx
	cltq
	addq	%rdx, %rax
	addq	%r9, %rbx
	movl	(%rdi,%rax,4), %eax
	jmp	.L171
.L175:
	leaq	(%rsi,%rcx,8), %rbx
	jmp	.L172
.L488:
	movq	-128(%rbp), %rbx
	movq	-72(%rbp), %rdi
	movslq	%ecx, %rcx
	leaq	(%r15,%rcx,4), %r9
	xorl	%edx, %edx
	leaq	(%rbx,%rdi,4), %rdi
	movl	-120(%rbp), %ebx
	movl	%ebx, (%rdi)
	.p2align 4,,10
	.p2align 3
.L211:
	movl	(%r9,%rdx,4), %ecx
	movl	%ecx, 4(%rdi,%rdx,4)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L211
	jmp	.L281
.L213:
	addl	$1, -120(%rbp)
	xorl	%r10d, %r10d
	jmp	.L214
.L485:
	movq	-96(%rbp), %rbx
	negl	%eax
	cltq
	leaq	(%rbx,%rax,4), %rsi
.L152:
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	js	.L497
.L153:
	cmpq	$-2, %rdx
	movq	$-2, %rdi
	cmovbe	%rdx, %rdi
	testq	%rdx, %rdx
	je	.L157
	movl	8(%rsi), %ebx
	cmpl	%ebx, 4(%r12)
	jne	.L158
	xorl	%ecx, %ecx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L161:
	movl	4(%r12,%rcx,4), %ebx
	cmpl	%ebx, (%r8,%rcx,4)
	jne	.L160
.L159:
	addq	$1, %rcx
	cmpq	%rcx, %rdi
	ja	.L161
.L160:
	cmpq	%rcx, %rdx
	leaq	0(,%rdx,4), %rdi
	je	.L498
.L162:
	leaq	(%r8,%rdi), %rsi
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	jns	.L153
.L497:
	movq	%rdx, %rdi
	subq	$1, %rdi
	je	.L154
	movl	8(%rsi), %ebx
	cmpl	%ebx, 4(%r12)
	jne	.L155
	xorl	%ecx, %ecx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L164:
	movl	4(%r12,%rcx,4), %ebx
	cmpl	%ebx, (%r8,%rcx,4)
	jne	.L155
.L156:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L164
.L154:
	leaq	0(,%rdx,4), %r9
	movl	(%r12,%rdx,4), %esi
	salq	$3, %rdx
	movl	-4(%r8,%r9), %edi
	cmpl	%esi, %edi
	jg	.L458
	cmpl	-4(%r8,%rdx), %esi
	jg	.L458
	subl	%edi, %esi
	negl	%eax
	movq	-112(%rbp), %rbx
	movslq	%esi, %rdx
	cltq
	addq	%r9, -160(%rbp)
	addq	%rdx, %rax
	movl	(%rbx,%rax,4), %eax
	jmp	.L151
.L155:
	leaq	(%r8,%rdx,8), %rsi
	jmp	.L152
.L458:
	leaq	(%r8,%rdx), %rsi
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L491:
	movq	-96(%rbp), %rdi
	negl	%eax
	cltq
	leaq	(%rdi,%rax,4), %rdx
.L238:
	movl	(%rdx), %eax
	leaq	8(%rdx), %rcx
	movslq	4(%rdx), %rsi
	testl	%eax, %eax
	js	.L499
.L239:
	cmpq	$-2, %rsi
	movq	$-2, %rdi
	cmovbe	%rsi, %rdi
	testq	%rsi, %rsi
	je	.L243
	movl	8(%rdx), %edx
	cmpl	%edx, 4(%r13)
	jne	.L244
	xorl	%edx, %edx
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L247:
	movl	4(%r13,%rdx,4), %r11d
	cmpl	%r11d, (%rcx,%rdx,4)
	jne	.L246
.L245:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	ja	.L247
.L246:
	cmpq	%rdx, %rsi
	leaq	0(,%rsi,4), %r10
	je	.L500
.L248:
	leaq	(%rcx,%r10), %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %rcx
	movslq	4(%rdx), %rsi
	testl	%eax, %eax
	jns	.L239
.L499:
	movq	%rsi, %rdi
	subq	$1, %rdi
	je	.L240
	movl	4(%r13), %r9d
	cmpl	%r9d, 8(%rdx)
	jne	.L241
	xorl	%edx, %edx
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L250:
	movl	4(%r13,%rdx,4), %r9d
	cmpl	%r9d, (%rcx,%rdx,4)
	jne	.L241
.L242:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L250
.L240:
	leaq	0(,%rsi,4), %rdx
	movl	0(%r13,%rsi,4), %r10d
	salq	$3, %rsi
	movl	-4(%rcx,%rdx), %r11d
	cmpl	%r10d, %r11d
	jg	.L462
	cmpl	-4(%rcx,%rsi), %r10d
	jg	.L462
	subl	%r11d, %r10d
	negl	%eax
	movq	-112(%rbp), %rdi
	leaq	(%r15,%rdx), %r13
	cltq
	movslq	%r10d, %rdx
	addq	%rdx, %rax
	movl	(%rdi,%rax,4), %eax
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L241:
	leaq	(%rcx,%rsi,8), %rdx
	jmp	.L238
.L462:
	leaq	(%rcx,%rsi), %rdx
	jmp	.L238
.L505:
	movq	-72(%rbp), %rax
	testq	%r14, %r14
	leaq	1(%rax), %rcx
	je	.L150
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L128
	movq	%rax, %r12
.L149:
	movl	(%r14), %esi
	movq	-56(%rbp), %rdi
	leaq	4(%r14), %rbx
	call	__collidx_table_lookup
	testl	%eax, %eax
	js	.L501
	movq	%rbx, %r14
.L129:
	andl	$16777215, %eax
	testl	%r15d, %r15d
	leal	1(%rax), %ecx
	cltq
	movl	0(%r13,%rax,4), %eax
	jle	.L145
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L146:
	addl	%ecx, %eax
	addl	$1, %edx
	leal	1(%rax), %ecx
	cmpl	%edx, %r15d
	cltq
	movl	0(%r13,%rax,4), %eax
	jne	.L146
.L145:
	movq	-72(%rbp), %rbx
	leaq	(%rbx,%r12), %rdx
	cmpq	%rdx, -88(%rbp)
	ja	.L502
.L147:
	subq	%rax, %r12
	jne	.L149
.L128:
	movq	-120(%rbp), %rbx
	addq	%rbx, -72(%rbp)
	movq	-72(%rbp), %rax
	leaq	1(%rax), %rcx
.L150:
	addq	$1, -104(%rbp)
	movq	-72(%rbp), %rbx
	cmpq	%rbx, -88(%rbp)
	movq	-104(%rbp), %rax
	jbe	.L260
	cmpq	%rax, -80(%rbp)
	movq	-128(%rbp), %rdi
	seta	%al
	movzbl	%al, %eax
	movl	%eax, (%rdi,%rbx,4)
.L260:
	movq	-104(%rbp), %rbx
	cmpq	%rbx, -80(%rbp)
	je	.L503
	movq	%rcx, -192(%rbp)
	jmp	.L261
.L256:
	addl	$1, -120(%rbp)
	subq	$1, -64(%rbp)
	jne	.L504
.L451:
	movq	%r15, %r13
	jmp	.L150
.L500:
	movq	%r10, %rsi
.L243:
	leaq	(%r15,%rsi), %r13
	jmp	.L237
.L244:
	leaq	0(,%rsi,4), %r10
	jmp	.L248
.L272:
	xorl	%r14d, %r14d
	movq	$0, -120(%rbp)
	movl	%r8d, %r15d
	.p2align 4,,10
	.p2align 3
.L79:
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L505
	movq	-56(%rbp), %rdi
	leaq	4(%r12), %rax
	movq	%rax, -64(%rbp)
	call	__collidx_table_lookup
	testl	%eax, %eax
	js	.L506
.L81:
	movl	%eax, %ebx
	andl	$16777215, %eax
	shrl	$24, %ebx
	leal	1(%rax), %r9d
	testl	%r15d, %r15d
	cltq
	movl	%ebx, %edx
	movl	0(%r13,%rax,4), %ebx
	jle	.L97
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L98:
	leal	(%r9,%rbx), %ecx
	addl	$1, %eax
	cmpl	%eax, %r15d
	leal	1(%rcx), %r9d
	movslq	%ecx, %rcx
	movl	0(%r13,%rcx,4), %ebx
	jne	.L98
.L97:
	movzbl	%dl, %edx
	movq	-136(%rbp), %rax
	addq	-104(%rbp), %rax
	imulq	-80(%rbp), %rdx
	testb	$1, (%rax,%rdx)
	je	.L99
	testq	%r14, %r14
	movq	-72(%rbp), %rax
	je	.L100
	movq	-120(%rbp), %rax
	testq	%rax, %rax
	je	.L101
	movq	%rbx, -152(%rbp)
	movl	%r9d, -144(%rbp)
	movl	%r15d, %ebx
	movq	%r14, %r15
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L122:
	movl	(%r15), %esi
	movq	-56(%rbp), %rdi
	leaq	4(%r15), %r12
	call	__collidx_table_lookup
	testl	%eax, %eax
	js	.L507
	movq	%r12, %r15
.L102:
	andl	$16777215, %eax
	testl	%ebx, %ebx
	leal	1(%rax), %edx
	cltq
	movl	0(%r13,%rax,4), %eax
	jle	.L118
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L119:
	addl	%edx, %eax
	addl	$1, %ecx
	leal	1(%rax), %edx
	cmpl	%ecx, %ebx
	cltq
	movl	0(%r13,%rax,4), %eax
	jne	.L119
.L118:
	movq	-72(%rbp), %rdi
	leaq	(%rdi,%r14), %rcx
	cmpq	%rcx, -88(%rbp)
	ja	.L508
.L120:
	subq	%rax, %r14
	jne	.L122
	movl	%ebx, %r15d
	movl	-144(%rbp), %r9d
	movq	-152(%rbp), %rbx
.L101:
	movq	-72(%rbp), %rax
	addq	-120(%rbp), %rax
	movq	$0, -120(%rbp)
.L100:
	leaq	(%rax,%rbx), %rdi
	xorl	%r14d, %r14d
	cmpq	%rdi, -88(%rbp)
	movq	%rdi, -72(%rbp)
	ja	.L509
.L123:
	movq	-64(%rbp), %r12
	jmp	.L79
.L508:
	testq	%rax, %rax
	je	.L120
	movslq	%edx, %rdx
	subq	%rax, %rcx
	leaq	0(,%rax,4), %rdi
	leaq	0(%r13,%rdx,4), %rsi
	movq	-128(%rbp), %rdx
	leaq	(%rdx,%rcx,4), %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L121:
	movl	(%rsi,%rdx), %r8d
	movl	%r8d, (%rcx,%rdx)
	addq	$4, %rdx
	cmpq	%rdx, %rdi
	jne	.L121
	jmp	.L120
.L99:
	testq	%r14, %r14
	cmove	%r12, %r14
	addq	%rbx, -120(%rbp)
	jmp	.L123
.L507:
	movq	-96(%rbp), %rdi
	negl	%eax
	cltq
	leaq	(%rdi,%rax,4), %rsi
.L103:
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	js	.L510
.L104:
	cmpq	$-2, %rdx
	movq	$-2, %rdi
	cmovbe	%rdx, %rdi
	testq	%rdx, %rdx
	je	.L108
	movl	8(%rsi), %esi
	cmpl	%esi, 4(%r15)
	jne	.L109
	xorl	%ecx, %ecx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L112:
	movl	4(%r15,%rcx,4), %esi
	cmpl	%esi, (%r8,%rcx,4)
	jne	.L111
.L110:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jb	.L112
.L111:
	cmpq	%rcx, %rdx
	leaq	0(,%rdx,4), %rsi
	je	.L511
.L113:
	addq	%r8, %rsi
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	jns	.L104
.L510:
	movq	%rdx, %rdi
	subq	$1, %rdi
	je	.L105
	movl	4(%r15), %ecx
	cmpl	%ecx, 8(%rsi)
	jne	.L106
	xorl	%ecx, %ecx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L115:
	movl	4(%r15,%rcx,4), %esi
	cmpl	%esi, (%r8,%rcx,4)
	jne	.L106
.L107:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L115
.L105:
	leaq	0(,%rdx,4), %r11
	movl	(%r15,%rdx,4), %esi
	salq	$3, %rdx
	movl	-4(%r8,%r11), %edi
	cmpl	%esi, %edi
	jg	.L456
	cmpl	-4(%r8,%rdx), %esi
	jg	.L456
	subl	%edi, %esi
	negl	%eax
	movq	-112(%rbp), %rdi
	movslq	%esi, %rdx
	cltq
	leaq	(%r12,%r11), %r15
	addq	%rdx, %rax
	movl	(%rdi,%rax,4), %eax
	jmp	.L102
.L106:
	leaq	(%r8,%rdx,8), %rsi
	jmp	.L103
.L456:
	leaq	(%r8,%rdx), %rsi
	jmp	.L103
.L506:
	movq	-96(%rbp), %rbx
	negl	%eax
	cltq
	leaq	(%rbx,%rax,4), %rsi
.L82:
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	js	.L512
.L83:
	cmpq	$-2, %rdx
	movq	$-2, %rdi
	cmovbe	%rdx, %rdi
	testq	%rdx, %rdx
	je	.L87
	movl	8(%rsi), %ebx
	cmpl	%ebx, 4(%r12)
	jne	.L88
	xorl	%ecx, %ecx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L91:
	movl	4(%r12,%rcx,4), %ebx
	cmpl	%ebx, (%r8,%rcx,4)
	jne	.L90
.L89:
	addq	$1, %rcx
	cmpq	%rcx, %rdi
	ja	.L91
.L90:
	cmpq	%rcx, %rdx
	leaq	0(,%rdx,4), %rdi
	je	.L513
.L92:
	leaq	(%r8,%rdi), %rsi
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	jns	.L83
.L512:
	movq	%rdx, %rdi
	subq	$1, %rdi
	je	.L84
	movl	8(%rsi), %ebx
	cmpl	%ebx, 4(%r12)
	jne	.L85
	xorl	%ecx, %ecx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L94:
	movl	4(%r12,%rcx,4), %ebx
	cmpl	%ebx, (%r8,%rcx,4)
	jne	.L85
.L86:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L94
.L84:
	leaq	0(,%rdx,4), %r9
	movl	(%r12,%rdx,4), %esi
	salq	$3, %rdx
	movl	-4(%r8,%r9), %edi
	cmpl	%esi, %edi
	jg	.L455
	cmpl	-4(%r8,%rdx), %esi
	jg	.L455
	subl	%edi, %esi
	negl	%eax
	movq	-112(%rbp), %rbx
	movslq	%esi, %rdx
	cltq
	addq	%r9, -64(%rbp)
	addq	%rdx, %rax
	movl	(%rbx,%rax,4), %eax
	jmp	.L81
.L85:
	leaq	(%r8,%rdx,8), %rsi
	jmp	.L82
.L455:
	leaq	(%r8,%rdx), %rsi
	jmp	.L82
.L502:
	testq	%rax, %rax
	je	.L147
	movq	-128(%rbp), %rbx
	movslq	%ecx, %rcx
	subq	%rax, %rdx
	leaq	0(,%rax,4), %rsi
	leaq	0(%r13,%rcx,4), %rdi
	leaq	(%rbx,%rdx,4), %r11
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L148:
	movl	(%rdi,%rdx), %ecx
	movl	%ecx, (%r11,%rdx)
	addq	$4, %rdx
	cmpq	%rdx, %rsi
	jne	.L148
	jmp	.L147
.L509:
	testq	%rbx, %rbx
	je	.L276
	movq	-128(%rbp), %rdi
	movslq	%r9d, %r9
	salq	$2, %rbx
	leaq	0(%r13,%r9,4), %rsi
	leaq	(%rdi,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L124:
	movl	(%rsi,%rax), %edx
	movl	%edx, (%rcx,%rax)
	addq	$4, %rax
	cmpq	%rax, %rbx
	jne	.L124
	xorl	%r14d, %r14d
	jmp	.L123
.L483:
	movq	-96(%rbp), %rbx
	negl	%eax
	movq	-184(%rbp), %rsi
	cltq
	leaq	(%rbx,%rax,4), %rdx
.L66:
	movl	(%rdx), %eax
	leaq	8(%rdx), %rcx
	movslq	4(%rdx), %r10
	testl	%eax, %eax
	js	.L514
.L67:
	cmpq	$-2, %r10
	movq	$-2, %rdi
	cmovbe	%r10, %rdi
	testq	%r10, %r10
	je	.L65
	movl	8(%rdx), %ebx
	cmpl	%ebx, 4(%rsi)
	jne	.L71
	xorl	%edx, %edx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	movl	4(%rsi,%rdx,4), %ebx
	cmpl	%ebx, (%rcx,%rdx,4)
	jne	.L73
.L72:
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jb	.L74
.L73:
	cmpq	%rdx, %r10
	je	.L65
.L71:
	leaq	(%rcx,%r10,4), %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %rcx
	movslq	4(%rdx), %r10
	testl	%eax, %eax
	jns	.L67
.L514:
	movq	%r10, %rdi
	subq	$1, %rdi
	je	.L68
	movl	8(%rdx), %ebx
	cmpl	%ebx, 4(%rsi)
	jne	.L69
	xorl	%edx, %edx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L76:
	movl	4(%rsi,%rdx,4), %ebx
	cmpl	%ebx, (%rcx,%rdx,4)
	jne	.L69
.L70:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L76
.L68:
	movl	-4(%rcx,%r10,4), %r9d
	movl	(%rsi,%r10,4), %edi
	leaq	0(,%r10,8), %rdx
	cmpl	%edi, %r9d
	jg	.L454
	cmpl	-4(%rcx,%rdx), %edi
	jg	.L454
	subl	%r9d, %edi
	negl	%eax
	movq	-112(%rbp), %rbx
	movslq	%edi, %rdx
	cltq
	addq	%rdx, %rax
	movl	(%rbx,%rax,4), %eax
	jmp	.L65
.L69:
	leaq	(%rcx,%r10,8), %rdx
	jmp	.L66
.L501:
	movq	-96(%rbp), %rdi
	negl	%eax
	cltq
	leaq	(%rdi,%rax,4), %rsi
.L130:
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	js	.L515
.L131:
	cmpq	$-2, %rdx
	movq	$-2, %rdi
	cmovbe	%rdx, %rdi
	testq	%rdx, %rdx
	je	.L135
	movl	4(%r14), %ecx
	cmpl	%ecx, 8(%rsi)
	jne	.L136
	xorl	%ecx, %ecx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L139:
	movl	4(%r14,%rcx,4), %esi
	cmpl	%esi, (%r8,%rcx,4)
	jne	.L138
.L137:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jb	.L139
.L138:
	cmpq	%rcx, %rdx
	leaq	0(,%rdx,4), %rdi
	je	.L516
.L140:
	leaq	(%r8,%rdi), %rsi
	movl	(%rsi), %eax
	leaq	8(%rsi), %r8
	movslq	4(%rsi), %rdx
	testl	%eax, %eax
	jns	.L131
.L515:
	movq	%rdx, %rdi
	subq	$1, %rdi
	je	.L132
	movl	8(%rsi), %esi
	cmpl	%esi, 4(%r14)
	jne	.L133
	xorl	%ecx, %ecx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L142:
	movl	4(%r14,%rcx,4), %esi
	cmpl	%esi, (%r8,%rcx,4)
	jne	.L133
.L134:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L142
.L132:
	leaq	0(,%rdx,4), %r11
	movl	(%r14,%rdx,4), %esi
	salq	$3, %rdx
	movl	-4(%r8,%r11), %edi
	cmpl	%esi, %edi
	jg	.L457
	cmpl	-4(%r8,%rdx), %esi
	jg	.L457
	leaq	(%rbx,%r11), %r14
	subl	%edi, %esi
	negl	%eax
	movq	-112(%rbp), %rbx
	movslq	%esi, %rdx
	cltq
	addq	%rdx, %rax
	movl	(%rbx,%rax,4), %eax
	jmp	.L129
.L133:
	leaq	(%r8,%rdx,8), %rsi
	jmp	.L130
.L511:
	movq	%rsi, %rdx
.L108:
	leaq	(%r12,%rdx), %r15
	jmp	.L102
.L490:
	movq	-96(%rbp), %rbx
	negl	%eax
	cltq
	leaq	(%rbx,%rax,4), %rbx
.L219:
	movl	(%rbx), %eax
	leaq	8(%rbx), %rsi
	movslq	4(%rbx), %rcx
	testl	%eax, %eax
	js	.L517
.L220:
	cmpq	$-2, %rcx
	movq	$-2, %rdi
	cmovbe	%rcx, %rdi
	testq	%rcx, %rcx
	je	.L224
	movl	4(%r14), %edx
	cmpl	%edx, 8(%rbx)
	jne	.L225
	xorl	%edx, %edx
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L228:
	movl	4(%r14,%rdx,4), %ebx
	cmpl	%ebx, (%rsi,%rdx,4)
	jne	.L227
.L226:
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	ja	.L228
.L227:
	cmpq	%rdx, %rcx
	leaq	0(,%rcx,4), %rbx
	je	.L518
.L461:
	addq	%rsi, %rbx
	movl	(%rbx), %eax
	leaq	8(%rbx), %rsi
	movslq	4(%rbx), %rcx
	testl	%eax, %eax
	jns	.L220
.L517:
	movq	%rcx, %rdi
	subq	$1, %rdi
	je	.L221
	movl	8(%rbx), %ebx
	cmpl	%ebx, 4(%r14)
	jne	.L222
	xorl	%edx, %edx
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L231:
	movl	4(%r14,%rdx,4), %ebx
	cmpl	%ebx, (%rsi,%rdx,4)
	jne	.L222
.L223:
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L231
.L221:
	leaq	0(,%rcx,4), %r9
	movl	(%r14,%rcx,4), %edx
	leaq	0(,%rcx,8), %rbx
	movl	-4(%rsi,%r9), %edi
	cmpl	%edx, %edi
	jg	.L461
	cmpl	-4(%rsi,%rbx), %edx
	jg	.L461
	subl	%edi, %edx
	movq	-160(%rbp), %rbx
	negl	%eax
	movq	-112(%rbp), %rdi
	movslq	%edx, %rdx
	cltq
	addq	%rdx, %rax
	addq	%r9, %rbx
	movl	(%rdi,%rax,4), %eax
	jmp	.L218
.L222:
	leaq	(%rsi,%rcx,8), %rbx
	jmp	.L219
.L496:
	movq	%rbx, %rcx
.L177:
	movq	-168(%rbp), %rbx
	addq	%rcx, %rbx
	jmp	.L171
.L109:
	leaq	0(,%rdx,4), %rsi
	jmp	.L113
.L178:
	leaq	0(,%rcx,4), %rbx
	jmp	.L459
.L454:
	addq	%rcx, %rdx
	jmp	.L66
.L457:
	leaq	(%r8,%rdx), %rsi
	jmp	.L130
.L492:
	movq	-128(%rbp), %rbx
	movq	-72(%rbp), %rdi
	movslq	%edx, %rdx
	leaq	(%r15,%rdx,4), %r9
	xorl	%edx, %edx
	leaq	(%rbx,%rdi,4), %rdi
	movl	-120(%rbp), %ebx
	movl	%ebx, (%rdi)
.L258:
	movl	(%r9,%rdx,4), %r8d
	movl	%r8d, 4(%rdi,%rdx,4)
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L258
	jmp	.L286
.L489:
	movq	-128(%rbp), %rax
	movq	-152(%rbp), %r8
	leaq	(%rax,%rbx,4), %rsi
	movl	-120(%rbp), %eax
	movl	%eax, (%rsi)
	movslq	-176(%rbp), %rax
	leaq	(%r15,%rax,4), %rdi
	xorl	%eax, %eax
.L215:
	movl	(%rdi,%rax,4), %edx
	movl	%edx, 4(%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L215
	jmp	.L282
.L498:
	movq	%rdi, %rdx
.L157:
	addq	%rdx, -160(%rbp)
	jmp	.L151
.L513:
	movq	%rdi, %rdx
.L87:
	addq	%rdx, -64(%rbp)
	jmp	.L81
.L158:
	leaq	0(,%rdx,4), %rdi
	jmp	.L162
.L88:
	leaq	0(,%rdx,4), %rdi
	jmp	.L92
.L503:
	cmpq	$2, %rcx
	jbe	.L262
	movq	-192(%rbp), %rax
	addq	$1, %rax
	cmpq	%rcx, %rax
	je	.L519
.L262:
	leaq	-1(%rcx), %rbx
	jmp	.L1
.L518:
	movq	%rbx, %rcx
.L224:
	movq	-160(%rbp), %rbx
	addq	%rcx, %rbx
	jmp	.L218
.L516:
	movq	%rdi, %rdx
.L135:
	leaq	(%rbx,%rdx), %r14
	jmp	.L129
.L225:
	leaq	0(,%rcx,4), %rbx
	jmp	.L461
.L136:
	leaq	0(,%rdx,4), %rdi
	jmp	.L140
.L276:
	movq	%rax, -72(%rbp)
	jmp	.L123
.L519:
	movq	-72(%rbp), %rcx
	cmpq	%rcx, -88(%rbp)
	jb	.L262
	movq	-128(%rbp), %rax
	movl	$0, -4(%rax,%rcx,4)
	jmp	.L262
.L466:
	leaq	__PRETTY_FUNCTION__.8883(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$705, %edx
	call	__assert_fail
.L467:
	leaq	__PRETTY_FUNCTION__.8883(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$706, %edx
	call	__assert_fail
.L468:
	leaq	__PRETTY_FUNCTION__.8883(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$707, %edx
	call	__assert_fail
.L469:
	leaq	__PRETTY_FUNCTION__.8883(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$708, %edx
	call	__assert_fail
	.size	__wcsxfrm_l, .-__wcsxfrm_l
	.weak	wcsxfrm_l
	.set	wcsxfrm_l,__wcsxfrm_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8883, @object
	.size	__PRETTY_FUNCTION__.8883, 12
__PRETTY_FUNCTION__.8883:
	.string	"__wcsxfrm_l"
	.hidden	__assert_fail
	.hidden	__collidx_table_lookup
