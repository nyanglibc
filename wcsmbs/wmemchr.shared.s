	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wmemchr
	.hidden	__GI___wmemchr
	.type	__GI___wmemchr, @function
__GI___wmemchr:
	cmpq	$3, %rdx
	jbe	.L13
	cmpl	%esi, (%rdi)
	je	.L15
	cmpl	4(%rdi), %esi
	je	.L4
	cmpl	%esi, 8(%rdi)
	je	.L5
	cmpl	%esi, 12(%rdi)
	je	.L6
	subq	$4, %rdx
	movq	%rdx, %rcx
	andl	$3, %ecx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	%esi, (%rdi)
	je	.L15
	cmpl	%esi, 4(%rdi)
	je	.L4
	cmpl	%esi, 8(%rdi)
	je	.L5
	cmpl	%esi, 12(%rdi)
	je	.L6
	subq	$4, %rdx
.L10:
	addq	$16, %rdi
	cmpq	%rdx, %rcx
	jne	.L11
.L2:
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1
	cmpl	%esi, (%rdi)
	movq	%rdi, %rax
	je	.L1
	cmpq	$1, %rcx
	je	.L19
	cmpl	%esi, 4(%rdi)
	je	.L25
	addq	$8, %rax
	cmpq	$2, %rcx
	je	.L19
	cmpl	%esi, 8(%rdi)
	movl	$0, %edx
	cmovne	%rdx, %rax
	ret
.L19:
	xorl	%eax, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	4(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	12(%rdi), %rax
	ret
.L13:
	movq	%rdx, %rcx
	jmp	.L2
.L25:
	addq	$4, %rax
	ret
	.size	__GI___wmemchr, .-__GI___wmemchr
	.globl	__wmemchr
	.set	__wmemchr,__GI___wmemchr
	.weak	__GI_wmemchr
	.hidden	__GI_wmemchr
	.set	__GI_wmemchr,__wmemchr
	.weak	wmemchr
	.set	wmemchr,__GI_wmemchr
