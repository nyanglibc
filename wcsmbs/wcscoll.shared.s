	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcscoll
	.hidden	__GI___wcscoll
	.type	__GI___wcscoll, @function
__GI___wcscoll:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdx
	jmp	__GI___wcscoll_l
	.size	__GI___wcscoll, .-__GI___wcscoll
	.globl	__wcscoll
	.set	__wcscoll,__GI___wcscoll
	.weak	wcscoll
	.set	wcscoll,__wcscoll
