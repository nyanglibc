	.text
	.p2align 4,,15
	.globl	wcstok
	.type	wcstok, @function
wcstok:
	testq	%rdi, %rdi
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	je	.L9
.L2:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	call	wcsspn
	leaq	(%rbx,%rax,4), %rbx
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L10
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	wcspbrk
	testq	%rax, %rax
	je	.L11
	movl	$0, (%rax)
	addq	$4, %rax
	movq	%rax, (%r12)
.L1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rdx), %rbx
	testq	%rbx, %rbx
	jne	.L2
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rbx, %rax
	movq	$0, (%r12)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	$0, (%r12)
	xorl	%ebx, %ebx
	jmp	.L1
	.size	wcstok, .-wcstok
	.hidden	wcspbrk
	.hidden	wcsspn
