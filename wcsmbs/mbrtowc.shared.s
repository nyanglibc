	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"mbrtowc.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
	.section	.rodata.str1.1
.LC3:
	.string	"__mbsinit (data.__statep)"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___mbrtowc
	.hidden	__GI___mbrtowc
	.type	__GI___mbrtowc, @function
__GI___mbrtowc:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	leaq	state(%rip), %rsi
	movq	%rdi, %rbx
	subq	$96, %rsp
	testq	%rdi, %rdi
	leaq	28(%rsp), %rax
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	cmove	%rax, %rbx
	testq	%rcx, %rcx
	cmove	%rsi, %rcx
	testq	%rbp, %rbp
	movq	%rcx, 80(%rsp)
	je	.L18
	testq	%rdx, %rdx
	jne	.L4
.L14:
	addq	$96, %rsp
	movq	$-2, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC0(%rip), %rbp
	movq	%rax, %rbx
	movl	$1, %edx
.L4:
	leaq	4(%rbx), %rax
	movq	%rbx, 48(%rsp)
	movq	%rax, 56(%rsp)
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %r12
	movq	40(%r12), %r14
	testq	%r14, %r14
	je	.L30
.L7:
	addq	%rbp, %rdx
	movq	%rbp, 40(%rsp)
	movq	%rdx, %r13
	jc	.L31
.L8:
	movq	(%r14), %rax
	cmpq	$0, (%rax)
	movq	40(%rax), %r12
	je	.L9
#APP
# 84 "mbrtowc.c" 1
	ror $2*8+1, %r12
xor %fs:48, %r12
# 0 "" 2
#NO_APP
.L9:
	movq	%r12, %rdi
	call	__GI__dl_mcount_wrapper_check
	leaq	40(%rsp), %rdx
	leaq	48(%rsp), %rsi
	pushq	$1
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	(%r14), %rdi
	leaq	48(%rsp), %r9
	call	*%r12
	testl	$-5, %eax
	popq	%rcx
	popq	%rsi
	je	.L10
	leal	-5(%rax), %edx
	cmpl	$2, %edx
	ja	.L32
	cmpl	$5, %eax
	je	.L10
	cmpl	$7, %eax
	je	.L14
.L15:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	addq	$96, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%rbx, 48(%rsp)
	je	.L13
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L13
	movq	80(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L33
	addq	$96, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	40(%rsp), %rax
	addq	$96, %rsp
	popq	%rbx
	subq	%rbp, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %r12
	je	.L19
	movq	%r12, %rdi
	movq	%rdx, 8(%rsp)
	call	__wcsmbs_load_conv
	movq	40(%r12), %r14
	movq	8(%rsp), %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L31:
	movq	$-1, %r13
	cmpq	%r13, %rbp
	jne	.L8
	jmp	.L15
.L19:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %r14
	jmp	.L7
.L33:
	leaq	__PRETTY_FUNCTION__.9857(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$105, %edx
	call	__GI___assert_fail
.L32:
	leaq	__PRETTY_FUNCTION__.9857(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$96, %edx
	call	__GI___assert_fail
	.size	__GI___mbrtowc, .-__GI___mbrtowc
	.globl	__mbrtowc
	.set	__mbrtowc,__GI___mbrtowc
	.weak	__GI_mbrtowc
	.hidden	__GI_mbrtowc
	.set	__GI_mbrtowc,__mbrtowc
	.weak	mbrtowc
	.set	mbrtowc,__GI_mbrtowc
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9857, @object
	.size	__PRETTY_FUNCTION__.9857, 10
__PRETTY_FUNCTION__.9857:
	.string	"__mbrtowc"
	.local	state
	.comm	state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
