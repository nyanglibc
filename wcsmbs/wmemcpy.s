	.text
	.p2align 4,,15
	.globl	__wmemcpy
	.hidden	__wmemcpy
	.type	__wmemcpy, @function
__wmemcpy:
	salq	$2, %rdx
	jmp	memcpy@PLT
	.size	__wmemcpy, .-__wmemcpy
	.weak	wmemcpy
	.set	wmemcpy,__wmemcpy
