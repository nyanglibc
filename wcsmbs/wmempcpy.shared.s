	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wmempcpy
	.hidden	__wmempcpy
	.type	__wmempcpy, @function
__wmempcpy:
	salq	$2, %rdx
	jmp	__GI_mempcpy@PLT
	.size	__wmempcpy, .-__wmempcpy
	.weak	wmempcpy
	.set	wmempcpy,__wmempcpy
