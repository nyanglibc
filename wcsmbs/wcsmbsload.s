	.text
	.p2align 4,,15
	.globl	_nl_cleanup_ctype
	.hidden	_nl_cleanup_ctype
	.type	_nl_cleanup_ctype, @function
_nl_cleanup_ctype:
	pushq	%rbx
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1
	movq	24(%rbx), %rsi
	movq	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	16(%rbx), %rdi
	call	__gconv_close_transform
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	call	__gconv_close_transform
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	ret
	.size	_nl_cleanup_ctype, .-_nl_cleanup_ctype
	.p2align 4,,15
	.globl	__wcsmbs_getfct
	.hidden	__wcsmbs_getfct
	.type	__wcsmbs_getfct, @function
__wcsmbs_getfct:
	pushq	%rbx
	movq	%rdx, %rbx
	xorl	%r8d, %r8d
	subq	$16, %rsp
	leaq	8(%rsp), %rdx
	movq	%rsp, %rcx
	call	__gconv_find_transform
	testl	%eax, %eax
	jne	.L9
	movq	(%rsp), %rsi
	movq	8(%rsp), %rax
	cmpq	$1, %rsi
	ja	.L11
	movq	%rsi, (%rbx)
.L5:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rax, %rdi
	call	__gconv_close_transform
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	jmp	.L5
	.size	__wcsmbs_getfct, .-__wcsmbs_getfct
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"TRANSLIT"
.LC1:
	.string	""
.LC2:
	.string	"INTERNAL"
	.text
	.p2align 4,,15
	.globl	__wcsmbs_load_conv
	.hidden	__wcsmbs_load_conv
	.type	__wcsmbs_load_conv, @function
__wcsmbs_load_conv:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	je	.L13
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L13:
	cmpq	$0, 40(%rbx)
	jne	.L14
	movl	$32, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L15
	cmpl	$1, 52(%rbx)
	movl	52(%rbx), %eax
	leaq	.LC0(%rip), %rsi
	movq	176(%rbx), %r8
	sbbq	%rdx, %rdx
	notq	%rdx
	andl	$8, %edx
	cmpl	$1, 52(%rbx)
	sbbq	%rdi, %rdi
	andq	$-8, %rdi
	addq	$11, %rdi
	testl	%eax, %eax
	leaq	.LC1(%rip), %rax
	cmove	%rax, %rsi
	movq	%r8, %rax
	xorl	%r9d, %r9d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$1, %rax
	cmpb	$47, %cl
	sete	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %r9
.L17:
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	jne	.L19
	subq	%r8, %rax
	movsbq	(%r8), %rcx
	leaq	30(%rdi,%rax), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	testb	%cl, %cl
	je	.L31
	movq	120+_nl_C_locobj(%rip), %rdi
	movq	%r13, %rax
	.p2align 4,,10
	.p2align 3
.L21:
	movl	(%rdi,%rcx,4), %ecx
	addq	$1, %r8
	addq	$1, %rax
	movb	%cl, -1(%rax)
	movsbq	(%r8), %rcx
	testb	%cl, %cl
	jne	.L21
.L20:
	cmpq	$1, %r9
	jbe	.L45
.L22:
	leaq	8(%r12), %rdx
	leaq	.LC2(%rip), %rdi
	movb	$0, (%rax)
	movq	%r13, %rsi
	call	__wcsmbs_getfct
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L24
	leaq	24(%r12), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%r13, %rdi
	call	__wcsmbs_getfct
	testq	%rax, %rax
	movq	%rax, 16(%r12)
	je	.L46
.L25:
	leaq	_nl_cleanup_ctype(%rip), %rax
	movq	%r12, 40(%rbx)
	movq	%rax, 32(%rbx)
.L14:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L12
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L12:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$0, 16(%r12)
	jne	.L25
.L27:
	movq	%r12, %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	movq	%rax, 40(%rbx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L45:
	testq	%r9, %r9
	movb	$47, (%rax)
	jne	.L47
	movb	$47, 1(%rax)
	addq	$2, %rax
	testq	%rdx, %rdx
	je	.L22
	movq	%rax, %rdi
	call	__mempcpy@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$1, %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L27
	movq	8(%r12), %rsi
	call	__gconv_close_transform
	movq	%r12, %rdi
	call	free@PLT
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r13, %rax
	jmp	.L20
	.size	__wcsmbs_load_conv, .-__wcsmbs_load_conv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"Fatal glibc error: gconv module reference counter overflow\n"
	.text
	.p2align 4,,15
	.globl	__wcsmbs_clone_conv
	.hidden	__wcsmbs_clone_conv
	.type	__wcsmbs_clone_conv, @function
__wcsmbs_clone_conv:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L65
.L50:
	movdqu	(%rax), %xmm0
	movups	%xmm0, 0(%rbp)
	movdqu	16(%rax), %xmm0
	movups	%xmm0, 16(%rbp)
#APP
# 231 "wcsmbsload.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L51
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
.L52:
	movq	0(%rbp), %rdx
	xorl	%r8d, %r8d
	cmpq	$0, (%rdx)
	je	.L53
	movl	16(%rdx), %eax
	xorl	%r8d, %r8d
	addl	$1, %eax
	jo	.L66
.L54:
	movl	%eax, 16(%rdx)
	andl	$1, %r8d
.L53:
	movq	16(%rbp), %rdx
	cmpq	$0, (%rdx)
	je	.L56
	movl	16(%rdx), %eax
	xorl	%ecx, %ecx
	addl	$1, %eax
	jo	.L67
.L57:
	movl	%eax, 16(%rdx)
	orl	%ecx, %r8d
.L56:
#APP
# 241 "wcsmbsload.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L59
	subl	$1, __gconv_lock(%rip)
.L60:
	testb	%r8b, %r8b
	jne	.L68
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L62
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __gconv_lock(%rip)
	je	.L52
	leaq	__gconv_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%eax, %eax
#APP
# 241 "wcsmbsload.c" 1
	xchgl %eax, __gconv_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L60
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__gconv_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 241 "wcsmbsload.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L60
.L62:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L50
.L68:
	leaq	.LC3(%rip), %rdi
	call	__libc_fatal
.L66:
	movl	$1, %r8d
	jmp	.L54
.L67:
	movl	$1, %ecx
	jmp	.L57
	.size	__wcsmbs_clone_conv, .-__wcsmbs_clone_conv
	.p2align 4,,15
	.globl	__wcsmbs_named_conv
	.hidden	__wcsmbs_named_conv
	.type	__wcsmbs_named_conv, @function
__wcsmbs_named_conv:
	pushq	%rbp
	pushq	%rbx
	leaq	8(%rdi), %rdx
	movq	%rdi, %rbx
	leaq	.LC2(%rip), %rdi
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	__wcsmbs_getfct
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L69
	leaq	24(%rbx), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rbp, %rdi
	call	__wcsmbs_getfct
	movq	%rax, %rdx
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L75
.L69:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	__gconv_close_transform
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__wcsmbs_named_conv, .-__wcsmbs_named_conv
	.hidden	__wcsmbs_gconv_fcts_c
	.globl	__wcsmbs_gconv_fcts_c
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	__wcsmbs_gconv_fcts_c, @object
	.size	__wcsmbs_gconv_fcts_c, 32
__wcsmbs_gconv_fcts_c:
	.quad	to_wc
	.quad	1
	.quad	to_mb
	.quad	1
	.section	.rodata.str1.1
.LC4:
	.string	"ANSI_X3.4-1968//TRANSLIT"
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	to_mb, @object
	.size	to_mb, 104
to_mb:
	.quad	0
	.quad	0
	.long	2147483647
	.zero	4
	.quad	.LC2
	.quad	.LC4
	.quad	__gconv_transform_internal_ascii
	.quad	0
	.quad	0
	.quad	0
	.long	4
	.long	4
	.long	1
	.long	1
	.long	0
	.zero	4
	.quad	0
	.align 32
	.type	to_wc, @object
	.size	to_wc, 104
to_wc:
	.quad	0
	.quad	0
	.long	2147483647
	.zero	4
	.quad	.LC4
	.quad	.LC2
	.quad	__gconv_transform_ascii_internal
	.quad	__gconv_btwoc_ascii
	.quad	0
	.quad	0
	.long	1
	.long	1
	.long	4
	.long	4
	.long	0
	.zero	4
	.quad	0
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	__libc_fatal
	.hidden	__lll_lock_wait_private
	.hidden	_nl_C_LC_CTYPE
	.hidden	__gconv_lock
	.hidden	_nl_current_LC_CTYPE
	.hidden	_nl_C_locobj
	.hidden	__libc_setlocale_lock
	.hidden	__gconv_find_transform
	.hidden	__gconv_close_transform
