	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_wcsspn
	.hidden	__GI_wcsspn
	.type	__GI_wcsspn, @function
__GI_wcsspn:
	movl	(%rdi), %r8d
	testl	%r8d, %r8d
	je	.L13
	movl	(%rsi), %r9d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%r9d, %r9d
	je	.L1
	cmpl	%r8d, %r9d
	je	.L4
	movq	%rsi, %rdx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	%r8d, %ecx
	je	.L4
.L6:
	addq	$4, %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rax
	movl	(%rdi,%rax,4), %r8d
	testl	%r8d, %r8d
	jne	.L7
.L1:
	rep ret
.L13:
	xorl	%eax, %eax
	ret
	.size	__GI_wcsspn, .-__GI_wcsspn
	.globl	wcsspn
	.set	wcsspn,__GI_wcsspn
