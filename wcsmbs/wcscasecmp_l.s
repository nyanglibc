	.text
	.p2align 4,,15
	.globl	__wcscasecmp_l
	.hidden	__wcscasecmp_l
	.type	__wcscasecmp_l, @function
__wcscasecmp_l:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r13
	subq	$8, %rsp
	cmpq	%rsi, %rdi
	je	.L1
	movq	%rdi, %r12
	movq	%rsi, %rbp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	%eax, %ebx
	jne	.L3
.L4:
	addq	$4, %r12
	movl	-4(%r12), %edi
	movq	%r13, %rsi
	addq	$4, %rbp
	call	__towlower_l
	movl	-4(%rbp), %edi
	movl	%eax, %ebx
	movq	%r13, %rsi
	call	__towlower_l
	testl	%ebx, %ebx
	jne	.L13
.L3:
	subl	%eax, %ebx
	movl	%ebx, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__wcscasecmp_l, .-__wcscasecmp_l
	.weak	wcscasecmp_l
	.set	wcscasecmp_l,__wcscasecmp_l
	.hidden	__towlower_l
