	.text
	.p2align 4,,15
	.globl	__wcscoll
	.hidden	__wcscoll
	.type	__wcscoll, @function
__wcscoll:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdx
	jmp	__wcscoll_l
	.size	__wcscoll, .-__wcscoll
	.weak	wcscoll
	.set	wcscoll,__wcscoll
	.hidden	__wcscoll_l
