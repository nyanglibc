	.text
	.p2align 4,,15
	.globl	wcsncmp
	.type	wcsncmp, @function
wcsncmp:
	cmpq	$3, %rdx
	jbe	.L2
	movq	%rdx, %rax
	shrq	$2, %rax
	.p2align 4,,10
	.p2align 3
.L12:
	movl	(%rdi), %ecx
	movl	(%rsi), %r8d
	testl	%ecx, %ecx
	je	.L16
	cmpl	%r8d, %ecx
	jne	.L16
	movl	4(%rdi), %ecx
	movl	4(%rsi), %r8d
	testl	%ecx, %ecx
	je	.L16
	cmpl	%r8d, %ecx
	jne	.L16
	movl	8(%rdi), %ecx
	movl	8(%rsi), %r8d
	testl	%ecx, %ecx
	je	.L16
	cmpl	%r8d, %ecx
	jne	.L16
	addq	$16, %rdi
	movl	-4(%rdi), %ecx
	addq	$16, %rsi
	movl	-4(%rsi), %r8d
	testl	%ecx, %ecx
	je	.L16
	cmpl	%r8d, %ecx
	jne	.L16
	subq	$1, %rax
	jne	.L12
	andl	$3, %edx
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rdx, %rdx
	je	.L23
	movl	(%rdi), %ecx
	movl	(%rsi), %r8d
	cmpl	%ecx, %r8d
	jne	.L16
	salq	$2, %rdx
	testl	%ecx, %ecx
	movl	$4, %eax
	jne	.L13
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	movl	(%rdi,%rax), %ecx
	movl	(%rsi,%rax), %r8d
	addq	$4, %rax
	testl	%ecx, %ecx
	je	.L16
	cmpl	%r8d, %ecx
	jne	.L16
.L13:
	cmpq	%rdx, %rax
	jne	.L15
.L23:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	cmpl	%r8d, %ecx
	movl	$1, %edx
	setl	%al
	negl	%eax
	cmpl	%r8d, %ecx
	cmovg	%edx, %eax
	ret
	.size	wcsncmp, .-wcsncmp
