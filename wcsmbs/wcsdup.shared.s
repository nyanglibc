	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wcsdup
	.type	wcsdup, @function
wcsdup:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__wcslen@PLT
	leaq	4(,%rax,4), %rbp
	movq	%rbp, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L2
	addq	$8, %rsp
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	movq	%rax, %rdi
	jmp	__GI_memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	wcsdup, .-wcsdup
