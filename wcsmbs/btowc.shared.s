	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__btowc
	.hidden	__btowc
	.type	__btowc, @function
__btowc:
	leal	128(%rdi), %edx
	cmpl	$383, %edx
	ja	.L10
	cmpl	$-1, %edi
	je	.L10
	testl	$-128, %edi
	movl	%edi, %eax
	je	.L27
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$80, %rsp
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbp
	movq	40(%rbp), %r12
	testq	%r12, %r12
	je	.L31
.L4:
	movq	(%r12), %rax
	cmpq	$0, (%rax)
	movq	48(%rax), %rbp
	je	.L5
#APP
# 51 "btowc.c" 1
	ror $2*8+1, %rbp
xor %fs:48, %rbp
# 0 "" 2
#NO_APP
.L5:
	cmpq	$1, 8(%r12)
	jne	.L6
	testq	%rbp, %rbp
	je	.L6
	movq	%rbp, %rdi
	call	__GI__dl_mcount_wrapper_check
	movzbl	%bl, %esi
	movq	(%r12), %rdi
	call	*%rbp
.L1:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	11(%rsp), %rax
	leaq	32(%rsp), %rbp
	movq	$0, 72(%rsp)
	movb	%bl, 11(%rsp)
	movq	$1, 48(%rsp)
	movq	%rax, 16(%rsp)
	leaq	12(%rsp), %rax
	movl	$1, 56(%rsp)
	movq	%rax, 32(%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 40(%rsp)
	leaq	40(%rbp), %rax
	movq	%rax, 64(%rsp)
	movq	(%r12), %rax
	cmpq	$0, (%rax)
	movq	40(%rax), %rbx
	je	.L7
#APP
# 87 "btowc.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L7:
	movq	%rbx, %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	16(%rsp), %rax
	leaq	16(%rsp), %rdx
	pushq	$1
	pushq	$0
	xorl	%r8d, %r8d
	movq	%rbp, %rsi
	movq	(%r12), %rdi
	leaq	40(%rsp), %r9
	leaq	1(%rax), %rcx
	call	*%rbx
	movl	%eax, %edx
	leal	-4(%rax), %eax
	popq	%rcx
	cmpl	$1, %eax
	popq	%rsi
	jbe	.L13
	testl	%edx, %edx
	movl	$-1, %eax
	jne	.L1
.L13:
	movl	12(%rsp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbp
	je	.L11
	movq	%rbp, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbp), %r12
	jmp	.L4
.L11:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %r12
	jmp	.L4
	.size	__btowc, .-__btowc
	.weak	btowc
	.set	btowc,__btowc
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
