	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstol_internal
	.hidden	__GI___wcstol_internal
	.type	__GI___wcstol_internal, @function
__GI___wcstol_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	____wcstol_l_internal
	.size	__GI___wcstol_internal, .-__GI___wcstol_internal
	.globl	__wcstol_internal
	.set	__wcstol_internal,__GI___wcstol_internal
	.globl	__GI___wcstoll_internal
	.set	__GI___wcstoll_internal,__wcstol_internal
	.globl	__wcstoll_internal
	.set	__wcstoll_internal,__wcstol_internal
	.p2align 4,,15
	.globl	__wcstol
	.type	__wcstol, @function
__wcstol:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	____wcstol_l_internal
	.size	__wcstol, .-__wcstol
	.weak	__GI_wcstol
	.hidden	__GI_wcstol
	.set	__GI_wcstol,__wcstol
	.weak	wcstol
	.set	wcstol,__GI_wcstol
	.weak	wcstoimax
	.set	wcstoimax,wcstol
	.weak	wcstoq
	.set	wcstoq,wcstol
	.weak	wcstoll
	.set	wcstoll,wcstol
	.hidden	____wcstol_l_internal
