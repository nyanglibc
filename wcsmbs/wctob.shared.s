	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wctob
	.type	wctob, @function
wctob:
	cmpl	$-1, %edi
	je	.L20
	cmpl	$127, %edi
	movl	%edi, %eax
	ja	.L21
	rep ret
	.p2align 4,,10
	.p2align 3
.L21:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$96, %rsp
	leaq	32(%rsp), %rbp
	leaq	48(%rsp), %r13
	movq	$0, 88(%rsp)
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	leaq	16(%rbp), %rax
	movq	%rbp, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	40(%r13), %rax
	movq	%rax, 80(%rsp)
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %r14
	movq	40(%r14), %r12
	testq	%r12, %r12
	je	.L22
.L5:
	leaq	12(%rsp), %rax
	movl	%ebx, 12(%rsp)
	movq	%rax, 24(%rsp)
	movq	16(%r12), %rax
	cmpq	$0, (%rax)
	movq	40(%rax), %rbx
	je	.L6
#APP
# 69 "wctob.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L6:
	movq	%rbx, %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	24(%rsp), %rax
	leaq	24(%rsp), %rdx
	movq	16(%r12), %rdi
	pushq	$1
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r13, %rsi
	leaq	32(%rsp), %r9
	leaq	4(%rax), %rcx
	call	*%rbx
	leal	-4(%rax), %edx
	popq	%rcx
	cmpl	$1, %edx
	popq	%rsi
	jbe	.L10
	testl	%eax, %eax
	jne	.L7
.L10:
	addq	$1, %rbp
	cmpq	%rbp, 48(%rsp)
	jne	.L7
	movzbl	32(%rsp), %eax
.L1:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %r14
	je	.L9
	movq	%r14, %rdi
	call	__wcsmbs_load_conv
	movq	40(%r14), %r12
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$-1, %eax
	ret
.L9:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %r12
	jmp	.L5
	.size	wctob, .-wctob
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
