	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wcsrtombs.c"
.LC1:
	.string	"data.__outbuf[-1] == '\\0'"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"data.__outbuf != (unsigned char *) dst"
	.section	.rodata.str1.1
.LC3:
	.string	"__mbsinit (data.__statep)"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
	.text
	.p2align 4,,15
	.globl	__wcsrtombs
	.hidden	__wcsrtombs
	.type	__wcsrtombs, @function
__wcsrtombs:
	pushq	%r15
	pushq	%r14
	leaq	state(%rip), %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %r15
	subq	$360, %rsp
	testq	%rcx, %rcx
	cmove	%rax, %rcx
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L29
.L4:
	movq	16(%rax), %r12
	cmpq	$0, (%r12)
	movq	40(%r12), %rbx
	je	.L5
#APP
# 60 "wcsrtombs.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L5:
	testq	%rbp, %rbp
	movq	(%r14), %r13
	je	.L30
	movq	%r15, %rsi
	movq	%r13, %rdi
	addq	%rbp, %r15
	call	__wcsnlen@PLT
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)
	movq	%r15, 56(%rsp)
	leaq	4(%r13,%rax,4), %r13
	call	_dl_mcount_wrapper_check
	leaq	48(%rsp), %rsi
	pushq	$1
	pushq	$0
	movq	%r13, %rcx
	xorl	%r8d, %r8d
	movq	%r14, %rdx
	leaq	112(%rsp), %r9
	movq	%r12, %rdi
	call	*%rbx
	movq	64(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	movq	%rdx, %r13
	subq	%rbp, %r13
	testl	$-5, %eax
	je	.L31
.L10:
	testl	%eax, %eax
	leal	-4(%rax), %edx
	je	.L14
	cmpl	$3, %edx
	jbe	.L14
	leaq	__PRETTY_FUNCTION__.9884(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$134, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%eax, %eax
	je	.L1
	cmpl	$1, %edx
	ja	.L32
.L1:
	addq	$360, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpb	$0, -1(%rdx)
	jne	.L10
	cmpq	%rbp, %rdx
	je	.L33
	movq	80(%rsp), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L34
	movq	$0, (%r14)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L32:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r13
	movl	$84, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r13, %rdi
	leaq	96(%rsp), %rbp
	leaq	32(%rsp), %r14
	call	__wcslen@PLT
	leaq	4(%r13,%rax,4), %r15
	movq	80(%rsp), %rax
	movq	%r13, 24(%rsp)
	xorl	%r13d, %r13d
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	352(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	24(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)
	call	_dl_mcount_wrapper_check
	pushq	$1
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %rcx
	movq	16(%rsp), %rdx
	movq	24(%rsp), %rsi
	movq	%r12, %rdi
	movq	%r14, %r9
	call	*%rbx
	movq	64(%rsp), %rdx
	popq	%rdi
	popq	%r8
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	addq	%rcx, %r13
	cmpl	$5, %eax
	je	.L7
	testl	$-5, %eax
	jne	.L10
	cmpb	$0, -1(%rdx)
	jne	.L35
.L27:
	subq	$1, %r13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L17
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L4
.L17:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L4
.L34:
	leaq	__PRETTY_FUNCTION__.9884(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$123, %edx
	call	__assert_fail
.L33:
	leaq	__PRETTY_FUNCTION__.9884(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$122, %edx
	call	__assert_fail
.L35:
	leaq	__PRETTY_FUNCTION__.9884(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$94, %edx
	call	__assert_fail
	.size	__wcsrtombs, .-__wcsrtombs
	.weak	wcsrtombs
	.set	wcsrtombs,__wcsrtombs
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9884, @object
	.size	__PRETTY_FUNCTION__.9884, 12
__PRETTY_FUNCTION__.9884:
	.string	"__wcsrtombs"
	.local	state
	.comm	state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
	.hidden	__assert_fail
	.hidden	_dl_mcount_wrapper_check
	.hidden	_nl_current_LC_CTYPE
