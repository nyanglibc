	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_wcspbrk
	.hidden	__GI_wcspbrk
	.type	__GI_wcspbrk, @function
__GI_wcspbrk:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movl	(%rdi), %esi
	testl	%esi, %esi
	je	.L4
	movq	%rdi, %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$4, %rbx
	movl	(%rbx), %esi
	testl	%esi, %esi
	je	.L1
.L3:
	movq	%rbp, %rdi
	call	__GI_wcschr
	testq	%rax, %rax
	je	.L9
	movq	%rbx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	jmp	.L1
	.size	__GI_wcspbrk, .-__GI_wcspbrk
	.globl	wcspbrk
	.set	wcspbrk,__GI_wcspbrk
