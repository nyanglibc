	.text
	.p2align 4,,15
	.globl	____wcstol_l_internal
	.hidden	____wcstol_l_internal
	.type	____wcstol_l_internal, @function
____wcstol_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r14d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r12d
	movq	%r8, %rbx
	subq	$56, %rsp
	testl	%ecx, %ecx
	movq	%rdi, 8(%rsp)
	movq	%rsi, 16(%rsp)
	jne	.L79
.L2:
	cmpl	$1, %r12d
	sete	%r13b
	cmpl	$36, %r12d
	seta	%al
	orb	%al, %r13b
	je	.L37
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$22, %fs:(%rax)
.L1:
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	8(%rsp), %rbp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$4, %rbp
.L3:
	movl	0(%rbp), %edi
	movq	%rbx, %rsi
	call	__iswspace_l
	testl	%eax, %eax
	jne	.L5
	movl	0(%rbp), %edx
	movl	%eax, %r8d
	testl	%edx, %edx
	je	.L38
	cmpl	$45, %edx
	je	.L80
	cmpl	$43, %edx
	movl	$0, 44(%rsp)
	jne	.L8
	movl	4(%rbp), %edx
	movl	%eax, 44(%rsp)
	addq	$4, %rbp
.L8:
	cmpl	$48, %edx
	je	.L81
	testl	%r12d, %r12d
	je	.L13
.L10:
	cmpl	$10, %r12d
	je	.L13
	leal	-2(%r12), %eax
	xorl	%esi, %esi
.L12:
	leaq	__strtol_ul_max_tab(%rip), %rcx
	cltq
	cmpq	%rbp, %rsi
	movq	(%rcx,%rax,8), %rdi
	leaq	__strtol_ul_rem_tab(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
	movq	%rdi, (%rsp)
	movl	%eax, 40(%rsp)
	je	.L6
	testl	%edx, %edx
	je	.L6
	xorl	%ebx, %ebx
	movb	%r13b, 31(%rsp)
	movq	%rbp, 32(%rsp)
	movq	%rbx, %rdi
	movl	%r12d, %r13d
	movq	%rbp, %rbx
	movl	%r14d, 24(%rsp)
	movl	%edx, %ebp
	movq	%rsi, %r12
	movl	%r8d, %r14d
	movq	%rdi, %r15
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L82:
	cmpl	24(%rsp), %ebp
	jne	.L47
	cmpb	$0, 31(%rsp)
	je	.L47
.L19:
	addq	$4, %rbx
	movl	(%rbx), %ebp
	cmpq	%rbx, %r12
	je	.L21
	testl	%ebp, %ebp
	je	.L21
.L24:
	leal	-48(%rbp), %eax
	cmpl	$9, %eax
	ja	.L82
.L18:
	cmpl	%r13d, %eax
	jge	.L21
	cmpq	%r15, (%rsp)
	jb	.L44
	jne	.L23
	cmpl	40(%rsp), %eax
	jbe	.L23
.L44:
	movl	$1, %r14d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L80:
	movl	4(%rbp), %edx
	movl	$1, 44(%rsp)
	addq	$4, %rbp
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%ebp, %edi
	call	__iswalpha_l
	testl	%eax, %eax
	je	.L21
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%ebp, %edi
	call	__towupper_l
	subl	$55, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L23:
	movslq	%r13d, %rdx
	imulq	%r15, %rdx
	leaq	(%rax,%rdx), %r15
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	movq	32(%rsp), %rbp
	movq	%r15, %rax
	movq	%rbx, %r15
	movl	%r14d, %r8d
	movq	%rax, %rbx
	cmpq	%rbp, %r15
	je	.L6
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.L26
	movq	%r15, (%rax)
.L26:
	testl	%r8d, %r8d
	je	.L83
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$0, 44(%rsp)
	movabsq	$9223372036854775807, %rbx
	movl	$34, %fs:(%rax)
	movabsq	$-9223372036854775808, %rax
	cmovne	%rax, %rbx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%r15, %r15
	jne	.L84
	movl	$8, %eax
	movl	$10, %r12d
	xorl	%esi, %esi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L38:
	movq	8(%rsp), %rbp
.L6:
	xorl	%ebx, %ebx
	cmpq	$0, 16(%rsp)
	je	.L1
	movq	%rbp, %rax
	subq	8(%rsp), %rax
	cmpq	$4, %rax
	jg	.L85
.L30:
	movq	16(%rsp), %rax
	movq	8(%rsp), %rdi
	xorl	%ebx, %ebx
	movq	%rdi, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L79:
	movq	8(%r8), %rdx
	movq	80(%rdx), %r15
	movzbl	(%r15), %eax
	subl	$1, %eax
	cmpb	$125, %al
	ja	.L35
	movl	96(%rdx), %r14d
	movl	$0, %eax
	testl	%r14d, %r14d
	cmove	%rax, %r15
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L83:
	movl	44(%rsp), %eax
	testl	%eax, %eax
	je	.L86
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rbx
	ja	.L87
	negq	%rbx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L81:
	testl	$-17, %r12d
	jne	.L10
	movl	4(%rbp), %edi
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r8d, (%rsp)
	call	__towupper_l
	cmpl	$88, %eax
	movl	(%rsp), %r8d
	je	.L11
	testl	%r12d, %r12d
	movl	0(%rbp), %edx
	jne	.L10
	movl	$6, %eax
	movl	$8, %r12d
	xorl	%esi, %esi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L86:
	testq	%rbx, %rbx
	jns	.L1
	movq	__libc_errno@gottpoff(%rip), %rax
	movabsq	$9223372036854775807, %rbx
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	%edx, %r14d
	je	.L6
	testl	%edx, %edx
	movl	%edx, %r12d
	movq	%rbp, %r13
	je	.L14
	leaq	_nl_C_locobj(%rip), %rbx
	movq	%rbp, (%rsp)
	movl	%r8d, %r13d
	.p2align 4,,10
	.p2align 3
.L16:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L17
	cmpl	%r14d, %r12d
	je	.L17
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	__iswalpha_l
	testl	%eax, %eax
	je	.L76
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	__towupper_l
	subl	$55, %eax
	cmpl	$9, %eax
	jg	.L76
.L17:
	addq	$4, %rbp
	movl	0(%rbp), %r12d
	testl	%r12d, %r12d
	jne	.L16
.L76:
	movl	%r13d, %r8d
	movq	%rbp, %r13
	movq	(%rsp), %rbp
.L14:
	movq	%r13, %rsi
	movl	%r14d, %edx
	movq	%r15, %rcx
	movq	%rbp, %rdi
	movl	%r8d, (%rsp)
	movl	$10, %r12d
	call	__correctly_grouped_prefixwc
	movl	$1, %r13d
	movq	%rax, %rsi
	movl	0(%rbp), %edx
	movl	$8, %eax
	movl	(%rsp), %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L85:
	movl	-4(%rbp), %edi
	leaq	_nl_C_locobj(%rip), %rsi
	call	__towupper_l
	cmpl	$88, %eax
	jne	.L30
	cmpl	$48, -8(%rbp)
	jne	.L30
	movq	16(%rsp), %rax
	subq	$4, %rbp
	movq	%rbp, (%rax)
	jmp	.L1
.L11:
	movl	8(%rbp), %edx
	movl	$14, %eax
	addq	$8, %rbp
	movl	$16, %r12d
	xorl	%esi, %esi
	jmp	.L12
.L87:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movq	%rax, %rbx
	movl	$34, %fs:(%rdx)
	jmp	.L1
	.size	____wcstol_l_internal, .-____wcstol_l_internal
	.globl	____wcstoll_l_internal
	.set	____wcstoll_l_internal,____wcstol_l_internal
	.p2align 4,,15
	.weak	__wcstol_l
	.hidden	__wcstol_l
	.type	__wcstol_l, @function
__wcstol_l:
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	____wcstol_l_internal
	.size	__wcstol_l, .-__wcstol_l
	.weak	wcstoll_l
	.set	wcstoll_l,__wcstol_l
	.weak	__wcstoll_l
	.set	__wcstoll_l,__wcstol_l
	.weak	wcstol_l
	.set	wcstol_l,__wcstol_l
	.hidden	__correctly_grouped_prefixwc
	.hidden	__towupper_l
	.hidden	__iswalpha_l
	.hidden	_nl_C_locobj
	.hidden	__strtol_ul_rem_tab
	.hidden	__strtol_ul_max_tab
	.hidden	__iswspace_l
