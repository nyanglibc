	.text
	.p2align 4,,15
	.globl	__isoc99_vwscanf
	.type	__isoc99_vwscanf, @function
__isoc99_vwscanf:
.LFB71:
	.cfi_startproc
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	stdin(%rip), %rdi
	movl	$2, %ecx
	jmp	__vfwscanf_internal
	.cfi_endproc
.LFE71:
	.size	__isoc99_vwscanf, .-__isoc99_vwscanf
	.hidden	__vfwscanf_internal
