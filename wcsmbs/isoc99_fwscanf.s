	.text
	.p2align 4,,15
	.globl	__isoc99_fwscanf
	.type	__isoc99_fwscanf, @function
__isoc99_fwscanf:
.LFB71:
	.cfi_startproc
	subq	$216, %rsp
	.cfi_def_cfa_offset 224
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L3
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L3:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	movl	$2, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vfwscanf_internal
	addq	$216, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE71:
	.size	__isoc99_fwscanf, .-__isoc99_fwscanf
	.hidden	__vfwscanf_internal
