	.text
	.p2align 4,,15
	.globl	__wcsncpy
	.type	__wcsncpy, @function
__wcsncpy:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rdx, %rbx
	movq	%rdx, %rsi
	movq	%r12, %rdi
	subq	$8, %rsp
	call	__wcsnlen@PLT
	cmpq	%rax, %rbx
	movq	%rax, %rbp
	je	.L2
	subq	%rax, %rbx
	leaq	0(%r13,%rax,4), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	call	__wmemset
.L2:
	addq	$8, %rsp
	movq	%rbp, %rdx
	movq	%r12, %rsi
	popq	%rbx
	movq	%r13, %rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__wmemcpy
	.size	__wcsncpy, .-__wcsncpy
	.weak	wcsncpy
	.set	wcsncpy,__wcsncpy
	.hidden	__wmemcpy
	.hidden	__wmemset
