 .text
.globl wcsrchr
.type wcsrchr,@function
.align 1<<4
wcsrchr:
 movd %rsi, %xmm1
 mov %rdi, %rcx
 punpckldq %xmm1, %xmm1
 pxor %xmm2, %xmm2
 punpckldq %xmm1, %xmm1
 and $63, %rcx
 cmp $48, %rcx
 ja .Lcrosscache
 movdqu (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rcx
 pmovmskb %xmm0, %rax
 add $16, %rdi
 test %rax, %rax
 jnz .Lunaligned_match1
 test %rcx, %rcx
 jnz .Lreturn_null
 and $-16, %rdi
 xor %r8, %r8
 jmp .Lloop
 .p2align 4
.Lunaligned_match1:
 test %rcx, %rcx
 jnz .Lprolog_find_zero_1
 mov %rax, %r8
 mov %rdi, %rsi
 and $-16, %rdi
 jmp .Lloop
 .p2align 4
.Lcrosscache:
 and $15, %rcx
 and $-16, %rdi
 pxor %xmm3, %xmm3
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm3
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm3, %rdx
 pmovmskb %xmm0, %rax
 shr %cl, %rdx
 shr %cl, %rax
 add $16, %rdi
 test %rax, %rax
 jnz .Lunaligned_match
 test %rdx, %rdx
 jnz .Lreturn_null
 xor %r8, %r8
 jmp .Lloop
 .p2align 4
.Lunaligned_match:
 test %rdx, %rdx
 jnz .Lprolog_find_zero
 mov %rax, %r8
 lea (%rdi, %rcx), %rsi
 .p2align 4
.Lloop:
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rcx
 pmovmskb %xmm0, %rax
 or %rax, %rcx
 jnz .Lmatches
 movdqa (%rdi), %xmm3
 pcmpeqd %xmm3, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm3
 pmovmskb %xmm2, %rcx
 pmovmskb %xmm3, %rax
 or %rax, %rcx
 jnz .Lmatches
 movdqa (%rdi), %xmm4
 pcmpeqd %xmm4, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm4
 pmovmskb %xmm2, %rcx
 pmovmskb %xmm4, %rax
 or %rax, %rcx
 jnz .Lmatches
 movdqa (%rdi), %xmm5
 pcmpeqd %xmm5, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm5
 pmovmskb %xmm2, %rcx
 pmovmskb %xmm5, %rax
 or %rax, %rcx
 jz .Lloop
 .p2align 4
.Lmatches:
 test %rax, %rax
 jnz .Lmatch
.Lreturn_value:
 test %r8, %r8
 jz .Lreturn_null
 mov %r8, %rax
 mov %rsi, %rdi
 test $15 << 4, %ah
 jnz .Lmatch_fourth_wchar
 test %ah, %ah
 jnz .Lmatch_third_wchar
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lmatch:
 pmovmskb %xmm2, %rcx
 test %rcx, %rcx
 jnz .Lfind_zero
 mov %rax, %r8
 mov %rdi, %rsi
 jmp .Lloop
 .p2align 4
.Lfind_zero:
 test $15, %cl
 jnz .Lfind_zero_in_first_wchar
 test %cl, %cl
 jnz .Lfind_zero_in_second_wchar
 test $15, %ch
 jnz .Lfind_zero_in_third_wchar
 and $1 << 13 - 1, %rax
 jz .Lreturn_value
 test $15 << 4, %ah
 jnz .Lmatch_fourth_wchar
 test %ah, %ah
 jnz .Lmatch_third_wchar
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lfind_zero_in_first_wchar:
 test $1, %rax
 jz .Lreturn_value
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lfind_zero_in_second_wchar:
 and $1 << 5 - 1, %rax
 jz .Lreturn_value
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lfind_zero_in_third_wchar:
 and $1 << 9 - 1, %rax
 jz .Lreturn_value
 test %ah, %ah
 jnz .Lmatch_third_wchar
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lprolog_find_zero:
 add %rcx, %rdi
 mov %rdx, %rcx
.Lprolog_find_zero_1:
 test $15, %cl
 jnz .Lprolog_find_zero_in_first_wchar
 test %cl, %cl
 jnz .Lprolog_find_zero_in_second_wchar
 test $15, %ch
 jnz .Lprolog_find_zero_in_third_wchar
 and $1 << 13 - 1, %rax
 jz .Lreturn_null
 test $15 << 4, %ah
 jnz .Lmatch_fourth_wchar
 test %ah, %ah
 jnz .Lmatch_third_wchar
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lprolog_find_zero_in_first_wchar:
 test $1, %rax
 jz .Lreturn_null
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lprolog_find_zero_in_second_wchar:
 and $1 << 5 - 1, %rax
 jz .Lreturn_null
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lprolog_find_zero_in_third_wchar:
 and $1 << 9 - 1, %rax
 jz .Lreturn_null
 test %ah, %ah
 jnz .Lmatch_third_wchar
 test $15 << 4, %al
 jnz .Lmatch_second_wchar
 lea -16(%rdi), %rax
 ret
 .p2align 4
.Lmatch_second_wchar:
 lea -12(%rdi), %rax
 ret
 .p2align 4
.Lmatch_third_wchar:
 lea -8(%rdi), %rax
 ret
 .p2align 4
.Lmatch_fourth_wchar:
 lea -4(%rdi), %rax
 ret
 .p2align 4
.Lreturn_null:
 xor %rax, %rax
 ret
.size wcsrchr,.-wcsrchr
