	.text
	.p2align 4,,15
	.globl	__wcsncasecmp_l
	.hidden	__wcsncasecmp_l
	.type	__wcsncasecmp_l, @function
__wcsncasecmp_l:
	cmpq	%rsi, %rdi
	je	.L10
	xorl	%eax, %eax
	testq	%rdx, %rdx
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$4, %rbp
	movl	-4(%rbp), %edi
	movq	%r14, %rsi
	addq	$4, %r12
	call	__towlower_l
	movl	-4(%r12), %edi
	movl	%eax, %ebx
	movq	%r14, %rsi
	call	__towlower_l
	testl	%ebx, %ebx
	je	.L14
	cmpl	%eax, %ebx
	jne	.L14
	subq	$1, %r13
	jne	.L5
.L14:
	subl	%eax, %ebx
	movl	%ebx, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	ret
	.size	__wcsncasecmp_l, .-__wcsncasecmp_l
	.weak	wcsncasecmp_l
	.set	wcsncasecmp_l,__wcsncasecmp_l
	.hidden	__towlower_l
