	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wcsxfrm
	.type	wcsxfrm, @function
wcsxfrm:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	__GI___wcsxfrm_l
	.size	wcsxfrm, .-wcsxfrm
