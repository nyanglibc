	.text
	.p2align 4,,15
	.globl	__wmemcmp
	.type	__wmemcmp, @function
__wmemcmp:
	cmpq	$3, %rdx
	jbe	.L16
	movl	(%rdi), %eax
	movl	(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	movl	4(%rdi), %eax
	movl	4(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	movl	8(%rdi), %eax
	movl	8(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	movl	12(%rdi), %eax
	movl	12(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	subq	$4, %rdx
	movq	%rdx, %r8
	andl	$3, %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movl	(%rdi), %eax
	movl	(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	movl	4(%rdi), %eax
	movl	4(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	movl	8(%rdi), %eax
	movl	8(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	movl	12(%rdi), %eax
	movl	12(%rsi), %ecx
	cmpl	%ecx, %eax
	jne	.L6
	subq	$4, %rdx
.L12:
	addq	$16, %rdi
	addq	$16, %rsi
	cmpq	%rdx, %r8
	jne	.L13
.L2:
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L1
	movl	(%rsi), %edx
	cmpl	%edx, (%rdi)
	je	.L14
.L31:
	setg	%al
	movzbl	%al, %eax
	leal	-1(%rax,%rax), %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	%eax, %ecx
	setl	%al
	movzbl	%al, %eax
	leal	-1(%rax,%rax), %eax
	ret
.L14:
	cmpq	$1, %r8
	je	.L1
	movl	4(%rsi), %edx
	cmpl	%edx, 4(%rdi)
	jne	.L31
	cmpq	$2, %r8
	je	.L1
	movl	8(%rsi), %edx
	cmpl	%edx, 8(%rdi)
	jne	.L31
	rep ret
.L16:
	movq	%rdx, %r8
	jmp	.L2
	.size	__wmemcmp, .-__wmemcmp
	.weak	wmemcmp
	.set	wmemcmp,__wmemcmp
