	.text
	.p2align 4,,15
	.globl	wcspbrk
	.hidden	wcspbrk
	.type	wcspbrk, @function
wcspbrk:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movl	(%rdi), %esi
	testl	%esi, %esi
	je	.L4
	movq	%rdi, %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$4, %rbx
	movl	(%rbx), %esi
	testl	%esi, %esi
	je	.L1
.L3:
	movq	%rbp, %rdi
	call	wcschr
	testq	%rax, %rax
	je	.L9
	movq	%rbx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	jmp	.L1
	.size	wcspbrk, .-wcspbrk
	.hidden	wcschr
