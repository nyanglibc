	.text
	.p2align 4,,15
	.globl	__mbrlen
	.hidden	__mbrlen
	.type	__mbrlen, @function
__mbrlen:
	leaq	internal(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	__mbrtowc
	.size	__mbrlen, .-__mbrlen
	.weak	mbrlen
	.set	mbrlen,__mbrlen
	.local	internal
	.comm	internal,8,8
	.hidden	__mbrtowc
