	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wcscspn
	.type	wcscspn, @function
wcscspn:
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %esi
	testl	%esi, %esi
	je	.L4
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %rbx
	movl	0(%rbp,%rbx,4), %esi
	testl	%esi, %esi
	je	.L1
.L3:
	movq	%r12, %rdi
	call	__GI_wcschr
	testq	%rax, %rax
	je	.L8
.L1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%ebx, %ebx
	jmp	.L1
	.size	wcscspn, .-wcscspn
