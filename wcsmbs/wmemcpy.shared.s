	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wmemcpy
	.hidden	__wmemcpy
	.type	__wmemcpy, @function
__wmemcpy:
	salq	$2, %rdx
	jmp	__GI_memcpy@PLT
	.size	__wmemcpy, .-__wmemcpy
	.weak	wmemcpy
	.set	wmemcpy,__wmemcpy
