	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__isoc99_vwscanf
	.type	__isoc99_vwscanf, @function
__isoc99_vwscanf:
.LFB71:
	.cfi_startproc
	movq	stdin@GOTPCREL(%rip), %rax
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movl	$2, %ecx
	movq	(%rax), %rdi
	jmp	__vfwscanf_internal
	.cfi_endproc
.LFE71:
	.size	__isoc99_vwscanf, .-__isoc99_vwscanf
	.hidden	__vfwscanf_internal
