 .text
.globl __wcschr
.type __wcschr,@function
.align 1<<4
__wcschr:
 movd %rsi, %xmm1
 pxor %xmm2, %xmm2
 mov %rdi, %rcx
 punpckldq %xmm1, %xmm1
 punpckldq %xmm1, %xmm1
 and $63, %rcx
 cmp $48, %rcx
 ja .Lcross_cache
 movdqu (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 or %rax, %rdx
 jnz .Lmatches
 and $-16, %rdi
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 or %rax, %rdx
 jnz .Lmatches
 jmp .Lloop
.Lcross_cache:
 and $15, %rcx
 and $-16, %rdi
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 sar %cl, %rdx
 sar %cl, %rax
 test %rax, %rax
 je .Lunaligned_no_match
 bsf %rax, %rax
 test %rdx, %rdx
 je .Lunaligned_match
 bsf %rdx, %rdx
 cmp %rdx, %rax
 ja .Lreturn_null
.Lunaligned_match:
 add %rdi, %rax
 add %rcx, %rax
 ret
 .p2align 4
.Lunaligned_no_match:
 test %rdx, %rdx
 jne .Lreturn_null
 pxor %xmm2, %xmm2
 add $16, %rdi
 .p2align 4
.Lloop:
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 or %rax, %rdx
 jnz .Lmatches
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 or %rax, %rdx
 jnz .Lmatches
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 or %rax, %rdx
 jnz .Lmatches
 movdqa (%rdi), %xmm0
 pcmpeqd %xmm0, %xmm2
 add $16, %rdi
 pcmpeqd %xmm1, %xmm0
 pmovmskb %xmm2, %rdx
 pmovmskb %xmm0, %rax
 or %rax, %rdx
 jnz .Lmatches
 jmp .Lloop
 .p2align 4
.Lmatches:
 pmovmskb %xmm2, %rdx
 test %rax, %rax
 jz .Lreturn_null
 bsf %rax, %rax
 test %rdx, %rdx
 je .Lmatch
 bsf %rdx, %rcx
 cmp %rcx, %rax
 ja .Lreturn_null
.Lmatch:
 sub $16, %rdi
 add %rdi, %rax
 ret
 .p2align 4
.Lreturn_null:
 xor %rax, %rax
 ret
.size __wcschr,.-__wcschr
.weak wcschr
wcschr = __wcschr

