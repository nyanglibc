	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcscasecmp
	.type	__wcscasecmp, @function
__wcscasecmp:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	je	.L1
	movq	%rdi, %r12
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	%eax, %ebx
	jne	.L3
.L4:
	addq	$4, %r12
	movl	-4(%r12), %edi
	addq	$4, %rbp
	call	__GI_towlower
	movl	-4(%rbp), %edi
	movl	%eax, %ebx
	call	__GI_towlower
	testl	%ebx, %ebx
	jne	.L13
.L3:
	subl	%eax, %ebx
	movl	%ebx, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__wcscasecmp, .-__wcscasecmp
	.weak	wcscasecmp
	.set	wcscasecmp,__wcscasecmp
