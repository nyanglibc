	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcpncpy
	.type	__wcpncpy, @function
__wcpncpy:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rsi
	movq	%r13, %rdi
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	__wcsnlen@PLT
	movq	%r12, %rdi
	movq	%rax, %rbp
	movq	%rax, %rdx
	movq	%r13, %rsi
	call	__wmemcpy
	cmpq	%rbp, %rbx
	leaq	(%r12,%rbp,4), %rdi
	je	.L1
	addq	$8, %rsp
	subq	%rbp, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI_wmemset
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__wcpncpy, .-__wcpncpy
	.weak	wcpncpy
	.set	wcpncpy,__wcpncpy
	.hidden	__wmemcpy
