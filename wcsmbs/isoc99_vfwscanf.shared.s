	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isoc99_vfwscanf
	.hidden	__GI___isoc99_vfwscanf
	.type	__GI___isoc99_vfwscanf, @function
__GI___isoc99_vfwscanf:
.LFB71:
	.cfi_startproc
	movl	$2, %ecx
	jmp	__vfwscanf_internal
	.cfi_endproc
.LFE71:
	.size	__GI___isoc99_vfwscanf, .-__GI___isoc99_vfwscanf
	.globl	__isoc99_vfwscanf
	.set	__isoc99_vfwscanf,__GI___isoc99_vfwscanf
	.hidden	__vfwscanf_internal
