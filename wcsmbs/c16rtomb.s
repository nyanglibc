	.text
	.p2align 4,,15
	.globl	c16rtomb
	.type	c16rtomb, @function
c16rtomb:
	leaq	state(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	testq	%rdi, %rdi
	movl	(%rdx), %eax
	je	.L13
	testl	%eax, %eax
	js	.L14
	movzwl	%si, %ecx
	leal	-55296(%rcx), %esi
	cmpl	$1023, %esi
	ja	.L4
	orl	$-2147483648, %eax
	movl	%ecx, 4(%rdx)
	movl	%eax, (%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	andl	$2147483647, %eax
	movl	4(%rdx), %ecx
	movl	%eax, (%rdx)
	movzwl	%si, %eax
	subl	$56320, %eax
	cmpl	$1023, %eax
	ja	.L7
	sall	$10, %ecx
	andl	$1023, %esi
	andl	$1047552, %ecx
	orl	%esi, %ecx
	addl	$65536, %ecx
.L7:
	movl	$0, 4(%rdx)
.L4:
	movl	%ecx, %esi
	jmp	wcrtomb
	.p2align 4,,10
	.p2align 3
.L13:
	andl	$2147483647, %eax
	xorl	%ecx, %ecx
	movl	$0, 4(%rdx)
	movl	%eax, (%rdx)
	movl	%ecx, %esi
	jmp	wcrtomb
	.size	c16rtomb, .-c16rtomb
	.local	state
	.comm	state,8,8
	.hidden	wcrtomb
