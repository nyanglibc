	.text
	.p2align 4,,15
	.globl	__wcstoul_internal
	.hidden	__wcstoul_internal
	.type	__wcstoul_internal, @function
__wcstoul_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	____wcstoul_l_internal
	.size	__wcstoul_internal, .-__wcstoul_internal
	.globl	__wcstoull_internal
	.set	__wcstoull_internal,__wcstoul_internal
	.p2align 4,,15
	.globl	__wcstoul
	.type	__wcstoul, @function
__wcstoul:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	____wcstoul_l_internal
	.size	__wcstoul, .-__wcstoul
	.weak	wcstoul
	.hidden	wcstoul
	.set	wcstoul,__wcstoul
	.weak	wcstoumax
	.set	wcstoumax,wcstoul
	.weak	wcstouq
	.set	wcstouq,wcstoul
	.weak	wcstoull
	.set	wcstoull,wcstoul
	.hidden	____wcstoul_l_internal
