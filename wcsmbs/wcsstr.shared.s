	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wcsstr
	.type	wcsstr, @function
wcsstr:
	movl	(%rsi), %eax
	leaq	-4(%rdi), %rdx
	testl	%eax, %eax
	jne	.L3
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L42:
	cmpl	%ecx, %eax
	je	.L41
	movq	%rdi, %rdx
.L3:
	movl	4(%rdx), %ecx
	leaq	4(%rdx), %rdi
	testl	%ecx, %ecx
	jne	.L42
.L17:
	xorl	%edi, %edi
.L2:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movl	4(%rsi), %r8d
	testl	%r8d, %r8d
	je	.L2
	movl	8(%rdx), %edx
.L4:
	testl	%edx, %edx
	leaq	4(%rdi), %rcx
	je	.L17
	cmpl	%edx, %r8d
	je	.L43
.L7:
	cmpl	%eax, %edx
	movl	4(%rcx), %r9d
	je	.L44
	testl	%r9d, %r9d
	je	.L17
	movq	%rcx, %rdi
	addq	$4, %rcx
.L12:
	cmpl	%r9d, %eax
	je	.L45
	movl	8(%rdi), %edx
	leaq	8(%rdi), %rcx
	testl	%edx, %edx
	jne	.L7
	xorl	%edi, %edi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L43:
	movl	8(%rsi), %edx
	cmpl	%edx, 8(%rdi)
	je	.L46
.L9:
	testl	%edx, %edx
	je	.L2
	movl	%r8d, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L45:
	movl	4(%rcx), %edx
	movq	%rcx, %rdi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%r9d, %edx
	movq	%rcx, %rdi
	jmp	.L4
.L46:
	testl	%edx, %edx
	je	.L2
	movl	12(%rsi), %edx
	cmpl	12(%rdi), %edx
	jne	.L9
	testl	%edx, %edx
	je	.L2
	movl	16(%rsi), %edx
	cmpl	16(%rdi), %edx
	leaq	16(%rdi), %r10
	leaq	16(%rsi), %r9
	je	.L10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L38:
	movl	4(%r9), %edx
	cmpl	%edx, 4(%r10)
	jne	.L9
	testl	%edx, %edx
	je	.L2
	addq	$8, %r9
	addq	$8, %r10
	movl	(%r9), %edx
	cmpl	%edx, (%r10)
	jne	.L9
.L10:
	testl	%edx, %edx
	jne	.L38
	jmp	.L2
	.size	wcsstr, .-wcsstr
	.weak	wcswcs
	.set	wcswcs,wcsstr
