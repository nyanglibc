	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	round_away, @function
round_away:
	cmpl	$1024, %r8d
	je	.L3
	jle	.L18
	cmpl	$2048, %r8d
	je	.L6
	cmpl	$3072, %r8d
	jne	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r8d, %r8d
	jne	.L2
	orl	%esi, %ecx
	movl	$0, %eax
	testb	%dl, %dl
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmovne	%ecx, %eax
	ret
.L2:
	subq	$8, %rsp
	call	__GI_abort
	.size	round_away, .-round_away
	.p2align 4,,15
	.type	round_and_return, @function
round_and_return:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %rdx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 46(%rsp)
# 0 "" 2
#NO_APP
	movzwl	46(%rsp), %eax
	andw	$3072, %ax
	cmpw	$1024, %ax
	je	.L21
	jbe	.L60
	cmpw	$2048, %ax
	je	.L24
	cmpw	$3072, %ax
	jne	.L20
	movl	$3072, %r10d
.L23:
	cmpq	$-16382, %rbx
	jge	.L26
.L62:
	cmpq	$-16446, %rbx
	jge	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	fldt	.LC0(%rip)
	movl	$34, %fs:(%rax)
	jne	.L61
.L45:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	fmul	%st(0), %st
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$-16382, %rbx
	movl	$2048, %r10d
	jl	.L62
.L26:
	cmpq	$16383, %rbx
	jle	.L63
.L37:
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	fldt	.LC2(%rip)
	movl	$34, %fs:(%rax)
	je	.L45
	fldt	.LC3(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	testw	%ax, %ax
	jne	.L20
	xorl	%r10d, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$-1, %r14
	movl	%r8d, %ecx
	movq	$-16382, %rax
	movq	%r14, %rdi
	subq	%rbx, %rax
	movq	0(%rbp), %r11
	salq	%cl, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	testq	%rdx, %rcx
	setne	%cl
	movzbl	%cl, %ecx
	orl	%ecx, %r9d
	movl	%r9d, %edi
	andl	$1, %edi
	cmpq	$64, %rax
	movb	%dil, 27(%rsp)
	je	.L64
	movq	$-16383, %rcx
	subq	%rbx, %rcx
	movq	%r11, %rbx
	shrq	%cl, %rbx
	movl	%ecx, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	cmpq	$1, %rax
	je	.L32
	movq	%rbp, %rsi
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%rbp, %rdi
	movl	%r9d, 28(%rsp)
	movq	%r11, 16(%rsp)
	movl	%r10d, 8(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r12
	movl	8(%rsp), %r10d
	movq	16(%rsp), %r11
	movl	28(%rsp), %r9d
	movl	%r12d, %esi
	andl	$1, %esi
.L31:
	andl	$1, %ebx
	jne	.L35
	cmpb	$0, 27(%rsp)
	je	.L65
.L35:
	fldt	.LC0(%rip)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	fmul	%st(0), %st
	fstp	%st(0)
	movq	$-16383, %rbx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L61:
	fldt	.LC1(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%r8d, %ecx
	movq	%r11, %rsi
	movl	%r10d, %r8d
	shrq	%cl, %rdx
	andl	$1, %esi
	movl	%r9d, %ecx
	andl	$1, %edx
	movl	%r15d, %edi
	movl	%r10d, 28(%rsp)
	movl	%r9d, 16(%rsp)
	movq	%r11, 8(%rsp)
	call	round_away
	movq	8(%rsp), %r11
	movq	%rbp, %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rdi
	cmpq	$-1, %r11
	sete	%r12b
	andl	%eax, %r12d
	call	__mpn_rshift
	testb	%r12b, %r12b
	movq	0(%rbp), %r12
	movq	8(%rsp), %r11
	movl	16(%rsp), %r9d
	movl	28(%rsp), %r10d
	movl	%r12d, %esi
	je	.L33
	andl	$1, %esi
	movq	%r14, %r11
	movq	$-16383, %rbx
.L34:
	testl	%r9d, %r9d
	movl	$1, %r14d
	jne	.L38
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
.L36:
	testq	%rax, %rax
	setne	%r14b
	movzbl	%r14b, %r9d
.L38:
	movzbl	%r13b, %edx
	movl	%r10d, %r8d
	movl	%r9d, %ecx
	movl	%r15d, %edi
	call	round_away
	testb	%al, %al
	je	.L39
	addq	$1, %r12
	movq	%r12, 0(%rbp)
	jc	.L66
.L42:
	cmpq	$-16383, %rbx
	je	.L44
.L39:
	movl	%ebx, %esi
.L43:
	testb	%r14b, %r14b
	jne	.L52
	testb	%r13b, %r13b
	jne	.L52
.L46:
	movl	%r15d, %edx
	movq	%rbp, %rdi
	call	__mpn_construct_long_double
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	fld1
	fldt	.LC0(%rip)
	faddp	%st, %st(1)
	fstp	%st(0)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L63:
	movq	0(%rbp), %r12
	movq	%rdx, %r13
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movl	%r8d, 4(%rsp)
	movq	%rdx, %r11
	andl	$1, %r13d
	movl	%r12d, %esi
	andl	$1, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$1, %rbx
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mpn_rshift
	movabsq	$-9223372036854775808, %rax
	orq	%rax, 0(%rbp)
	cmpq	$16384, %rbx
	je	.L37
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L44:
	shrq	$63, %r12
	leal	-16383(%r12), %esi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
	jne	.L35
	movq	$-16383, %rbx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r11, %rbx
	movq	$0, 0(%rbp)
	xorl	%esi, %esi
	shrq	$63, %rbx
	xorl	%r12d, %r12d
	movl	$63, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	jmp	.L31
.L33:
	andl	$1, %esi
	jmp	.L31
.L20:
	call	__GI_abort
	.size	round_and_return, .-round_and_return
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC6:
	.string	"../stdlib/strtod_l.c"
.LC7:
	.string	"digcnt > 0"
.LC8:
	.string	"*nsize < MPNSIZE"
	.text
	.p2align 4,,15
	.type	str_to_mpn, @function
str_to_mpn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testl	%esi, %esi
	movq	$0, (%rcx)
	jle	.L101
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rcx, %r14
	movq	%r8, %r13
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r15, %rdi
.L68:
	movslq	(%rdi), %rax
	leaq	4(%rdi), %r15
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L77
	movslq	4(%rdi), %rax
	leaq	8(%rdi), %r15
.L77:
	leaq	(%rbx,%rbx,4), %rcx
	addl	$1, %edx
	subl	$1, %ebp
	leaq	-48(%rax,%rcx,2), %rbx
	je	.L102
	cmpl	$19, %edx
	jne	.L69
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L70
	movq	%rbx, (%r12)
	movq	$1, (%r14)
	xorl	%ebx, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, %rsi
	movabsq	$-8446744073709551616, %rcx
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%edx, %edx
	addq	(%r12), %rbx
	movq	(%r14), %rsi
	setc	%dl
	movq	%rbx, (%r12)
	testq	%rdx, %rdx
	jne	.L103
.L73:
	testq	%rax, %rax
	je	.L90
	movq	(%r14), %rdx
	cmpq	$858, %rdx
	jg	.L104
	movq	%rax, (%r12,%rdx,8)
	xorl	%ebx, %ebx
	addq	$1, (%r14)
	xorl	%edx, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L102:
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	jle	.L79
	movl	$19, %eax
	subl	%edx, %eax
	cltq
	cmpq	%rax, %rcx
	jle	.L105
.L79:
	movq	_tens_in_limb@GOTPCREL(%rip), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rcx
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L81
.L106:
	movq	%rbx, (%r12)
	movq	$1, (%r14)
.L67:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L103:
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%r12,%rdx,8), %rbx
	leaq	1(%rbx), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L73
.L74:
	cmpq	%rdx, %rsi
	jne	.L75
	addq	$1, %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L105:
	movq	_tens_in_limb@GOTPCREL(%rip), %rax
	movslq	%edx, %rdx
	movq	$0, 0(%r13)
	imulq	(%rax,%rcx,8), %rbx
	addq	%rdx, %rcx
	movq	(%r14), %rdx
	movq	(%rax,%rcx,8), %rcx
	testq	%rdx, %rdx
	je	.L106
.L81:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%edx, %edx
	addq	(%r12), %rbx
	movq	(%r14), %rsi
	setc	%dl
	movq	%rbx, (%r12)
	testq	%rdx, %rdx
	je	.L85
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r12,%rdx,8), %rbx
	leaq	1(%rbx), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L85
.L86:
	cmpq	%rsi, %rdx
	jne	.L87
	addq	$1, %rax
.L85:
	testq	%rax, %rax
	je	.L67
	movq	(%r14), %rdx
	cmpq	$858, %rdx
	jg	.L107
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movq	%rax, (%r12,%rdx,8)
	jmp	.L67
.L107:
	leaq	__PRETTY_FUNCTION__.11984(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$453, %edx
	call	__GI___assert_fail
.L104:
	leaq	__PRETTY_FUNCTION__.11984(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$397, %edx
	call	__GI___assert_fail
.L101:
	leaq	__PRETTY_FUNCTION__.11984(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$380, %edx
	call	__GI___assert_fail
	.size	str_to_mpn, .-str_to_mpn
	.section	.rodata.str1.1
.LC13:
	.string	"decimal != L'\\0'"
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC14:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC15:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"i"
	.string	""
	.string	""
	.string	"t"
	.string	""
	.string	""
	.string	"y"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC16:
	.string	"n"
	.string	""
	.string	""
	.string	"a"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"dig_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC18:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_EXP - MANT_DIG) / 4"
	.align 8
.LC19:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX / 4"
	.align 8
.LC20:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_EXP - 3) / 4"
	.align 8
.LC21:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_10_EXP - MANT_DIG)"
	.align 8
.LC22:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC23:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_10_EXP - 1)"
	.section	.rodata.str1.1
.LC24:
	.string	"dig_no >= int_no"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"lead_zero <= (base == 16 ? (uintmax_t) INTMAX_MAX / 4 : (uintmax_t) INTMAX_MAX)"
	.align 8
.LC26:
	.string	"lead_zero <= (base == 16 ? ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN) / 4 : ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN))"
	.section	.rodata.str1.1
.LC27:
	.string	"bits != 0"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"int_no <= (uintmax_t) (exponent < 0 ? (INTMAX_MAX - bits + 1) / 4 : (INTMAX_MAX - exponent - bits + 1) / 4)"
	.align 8
.LC29:
	.string	"dig_no > int_no && exponent <= 0 && exponent >= MIN_10_EXP - (DIG + 2)"
	.section	.rodata.str1.1
.LC30:
	.string	"int_no > 0 && exponent == 0"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"int_no == 0 && *startp != L_('0')"
	.section	.rodata.str1.1
.LC32:
	.string	"need_frac_digits > 0"
.LC33:
	.string	"numsize == 1 && n < d"
.LC34:
	.string	"empty == 1"
.LC35:
	.string	"numsize == densize"
.LC36:
	.string	"cy != 0"
	.text
	.p2align 4,,15
	.globl	____wcstold_l_internal
	.hidden	____wcstold_l_internal
	.type	____wcstold_l_internal, @function
____wcstold_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$13912, %rsp
	testl	%edx, %edx
	movq	8(%rcx), %rax
	movq	%rdi, 40(%rsp)
	movq	%rsi, 32(%rsp)
	movq	%rcx, 8(%rsp)
	jne	.L563
.L109:
	movl	88(%rax), %eax
	testl	%eax, %eax
	movl	%eax, 24(%rsp)
	je	.L564
	movq	40(%rsp), %rax
	movq	$0, 128(%rsp)
	movq	8(%rsp), %r15
	leaq	-4(%rax), %rbx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L356:
	movq	%rbp, %rbx
.L111:
	movl	4(%rbx), %r12d
	movq	%r15, %rsi
	leaq	4(%rbx), %rbp
	movl	%r12d, %edi
	call	__GI___iswspace_l
	testl	%eax, %eax
	jne	.L356
	cmpl	$45, %r12d
	movl	%eax, %r15d
	je	.L565
	cmpl	$43, %r12d
	movl	$0, 52(%rsp)
	je	.L566
.L113:
	cmpl	%r12d, 24(%rsp)
	je	.L567
.L114:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	ja	.L568
.L115:
	cmpl	$48, %r12d
	movl	$10, 16(%rsp)
	je	.L569
.L125:
	testl	%r13d, %r13d
	movq	%rbp, %r9
	setne	%al
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$4, %r9
	movl	(%r9), %r12d
.L126:
	cmpl	$48, %r12d
	je	.L127
	cmpl	%r13d, %r12d
	sete	%bl
	andb	%al, %bl
	jne	.L127
	movq	8(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r9, 56(%rsp)
	call	__GI___towlower_l
	leal	-48(%r12), %edx
	movq	56(%rsp), %r9
	cmpl	$9, %edx
	ja	.L570
.L128:
	testl	%r13d, %r13d
	movq	%r9, %r10
	sete	%cl
	xorl	%r11d, %r11d
	cmpl	$9, %edx
	ja	.L137
.L140:
	addq	$1, %r11
.L138:
	addq	$4, %r10
	movl	(%r10), %r12d
	leal	-48(%r12), %edx
	cmpl	$9, %edx
	jbe	.L140
.L137:
	cmpl	$16, 16(%rsp)
	je	.L571
.L139:
	cmpl	%r13d, %r12d
	setne	%al
	orb	%cl, %al
	movb	%al, 72(%rsp)
	je	.L138
	testq	%r14, %r14
	je	.L143
	cmpq	%r10, %rbp
	jb	.L572
.L143:
	xorl	%r14d, %r14d
	testq	%r11, %r11
	movq	%r11, %rbp
	sete	%r14b
	negq	%r14
	cmpl	%r12d, 24(%rsp)
	je	.L573
.L152:
	testq	%rbp, %rbp
	js	.L341
	movq	8(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r11, 64(%rsp)
	movq	%r10, 56(%rsp)
	movq	%r9, 40(%rsp)
	call	__GI___towlower_l
	cmpl	$16, 16(%rsp)
	movq	40(%rsp), %r9
	movq	56(%rsp), %r10
	movq	64(%rsp), %r11
	jne	.L339
	cmpl	$112, %eax
	jne	.L339
	movl	4(%r10), %ecx
	cmpl	$45, %ecx
	je	.L574
.L161:
	cmpl	$43, %ecx
	je	.L575
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L576
.L368:
	movq	%r10, %r13
.L160:
	cmpq	%rbp, %r11
	jnb	.L191
	cmpl	$48, -4(%r10)
	jne	.L192
	.p2align 4,,10
	.p2align 3
.L193:
	subq	$4, %r10
	subq	$1, %rbp
	cmpl	$48, -4(%r10)
	je	.L193
	cmpq	%rbp, %r11
	ja	.L577
.L191:
	cmpq	%rbp, %r11
	jne	.L151
	testq	%rbp, %rbp
	je	.L151
	cmpq	$0, 128(%rsp)
	js	.L578
.L192:
	cmpq	$0, 32(%rsp)
	jne	.L344
.L201:
	testq	%r14, %r14
	je	.L202
	movl	24(%rsp), %eax
	cmpl	%eax, (%r9)
	je	.L203
	.p2align 4,,10
	.p2align 3
.L204:
	addq	$4, %r9
	cmpl	%eax, (%r9)
	jne	.L204
.L203:
	cmpl	$16, 16(%rsp)
	leaq	4(%r9,%r14,4), %r9
	je	.L579
	testq	%r14, %r14
	movq	%r14, %rcx
	js	.L346
	movq	128(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	cmpq	%r14, %rdx
	jb	.L208
.L209:
	subq	%rcx, %rax
	subq	%r14, %rbp
	movq	%rax, 128(%rsp)
.L202:
	cmpl	$16, 16(%rsp)
	je	.L580
	movq	128(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L581
	movq	%rbp, %rax
	subq	%r11, %rax
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
.L230:
	leaq	(%rax,%r11), %rbx
	subq	%rax, %rcx
	movl	$4933, %eax
	movq	%rcx, 128(%rsp)
	subq	%rbx, %rax
	cmpq	%rax, %rcx
	jg	.L557
	cmpq	$-4951, %rcx
	jl	.L582
	testq	%rbx, %rbx
	jne	.L235
	testq	%rbp, %rbp
	je	.L238
	leaq	4951(%rcx), %rax
	cmpq	$4951, %rax
	ja	.L238
	cmpl	$48, (%r9)
	je	.L260
	movl	$1, %eax
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movl	$16447, %esi
	shrq	%rdx
	leal	65(%rdx), %eax
	movl	%ecx, %edx
	cmpl	$16447, %eax
	cmovg	%esi, %eax
	addl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L583
	leaq	128(%rsp), %rax
	movl	$0, 8(%rsp)
	movq	$0, 56(%rsp)
	movq	%rax, 88(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 16(%rsp)
.L262:
	movslq	%ecx, %rax
	movq	%rbp, %rsi
	movl	%ebp, %ecx
	subl	%ebx, %ecx
	subq	%rbx, %rsi
	cmpq	%rsi, %rax
	movslq	%ecx, %rcx
	cmovg	%rcx, %rax
	addq	%rbx, %rax
	cmpq	%rax, %rbp
	jle	.L264
	movq	%rax, %rbp
	movl	$1, %r15d
.L264:
	movq	16(%rsp), %rax
	movl	%ebp, %r13d
	movl	%r15d, 64(%rsp)
	subl	%ebx, %r13d
	leaq	_fpioconst_pow10(%rip), %r12
	movl	$1, %ebx
	movl	%r13d, 40(%rsp)
	xorl	%r14d, %r14d
	subl	%edx, %r13d
	movq	%rax, 32(%rsp)
	leaq	7024(%rsp), %rax
	movq	%r9, 72(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rax, %r15
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	0(,%rbp,8), %rdx
	movq	%r15, %rdi
	movq	%rbp, %r14
	call	__GI_memcpy@PLT
.L265:
	addl	%ebx, %ebx
	addq	$24, %r12
	testl	%r13d, %r13d
	je	.L584
.L267:
	testl	%r13d, %ebx
	je	.L265
	movq	8(%r12), %rax
	leaq	__tens(%rip), %rsi
	xorl	%ebx, %r13d
	testq	%r14, %r14
	leaq	-1(%rax), %rbp
	movq	(%r12), %rax
	leaq	8(%rsi,%rax,8), %rsi
	je	.L585
	movq	%rbp, %rdx
	movq	32(%rsp), %rbp
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%rbp, %rdi
	call	__mpn_mul
	movq	8(%r12), %rdx
	testq	%rax, %rax
	leaq	-1(%r14,%rdx), %r14
	jne	.L379
	movq	%r15, %rax
	subq	$1, %r14
	movq	%rbp, %r15
	movq	%rax, 32(%rsp)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L570:
	leal	-97(%rax), %ecx
	cmpl	$5, %ecx
	ja	.L387
	cmpl	$16, 16(%rsp)
	je	.L128
.L387:
	cmpl	%r12d, 24(%rsp)
	je	.L586
	cmpl	$16, 16(%rsp)
	je	.L587
	cmpl	$101, %eax
	je	.L128
.L131:
	movq	%r9, %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rbp, %rdi
	movq	%r9, 8(%rsp)
	call	__correctly_grouped_prefixwc
	cmpq	$0, 32(%rsp)
	movq	8(%rsp), %r9
	je	.L135
	cmpq	%rax, %rbp
	je	.L588
.L136:
	movq	32(%rsp), %rsi
	movq	%rax, (%rsi)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L566:
	movl	4(%rbp), %r12d
	cmpl	%r12d, 24(%rsp)
	leaq	8(%rbx), %rbp
	movl	%eax, 52(%rsp)
	jne	.L114
.L567:
	movl	4(%rbp), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L115
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L115
.L568:
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r12d, %edi
	call	__GI___towlower_l
	cmpl	$105, %eax
	je	.L589
	cmpl	$110, %eax
	je	.L590
.L144:
	cmpq	$0, 32(%rsp)
	jne	.L591
.L124:
	fldz
.L108:
	addq	$13912, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L573:
	movl	%r15d, 40(%rsp)
	movb	%bl, 56(%rsp)
	movq	%r14, %r15
	leaq	4(%r10), %r13
	movl	4(%r10), %r12d
	movq	%r9, 64(%rsp)
	movq	%r11, %rbx
	movq	%r11, %r14
	movl	16(%rsp), %ebp
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L593:
	movq	8(%rsp), %rsi
	movl	%r12d, %edi
	call	__GI___towlower_l
	subl	$97, %eax
	cmpl	$5, %eax
	ja	.L592
.L155:
	cmpl	$48, %r12d
	je	.L154
	cmpq	$-1, %r15
	movq	%r14, %rax
	sete	%dl
	subq	%rbx, %rax
	testb	%dl, %dl
	cmovne	%rax, %r15
.L154:
	addq	$4, %r13
	movl	0(%r13), %r12d
	addq	$1, %r14
.L153:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L155
	cmpl	$16, %ebp
	je	.L593
	movq	%r14, %rbp
	movq	%rbx, %r11
	movq	%r15, %r14
	testq	%rbp, %rbp
	movq	64(%rsp), %r9
	movzbl	56(%rsp), %ebx
	movl	40(%rsp), %r15d
	js	.L341
	movq	8(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r11, 56(%rsp)
	movq	%r9, 40(%rsp)
	call	__GI___towlower_l
	movq	40(%rsp), %r9
	movq	56(%rsp), %r11
	movq	%r13, %r10
.L339:
	cmpl	$16, 16(%rsp)
	je	.L368
	cmpl	$101, %eax
	jne	.L368
	movl	4(%r10), %ecx
	cmpl	$45, %ecx
	jne	.L161
.L574:
	movl	8(%r10), %ecx
	leaq	8(%r10), %r13
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L368
	cmpl	$16, 16(%rsp)
	je	.L594
	movabsq	$9223372036854770812, %rax
	cmpq	%rax, %r11
	ja	.L595
	leaq	4995(%r11), %rsi
	movabsq	$-3689348814741910323, %rdx
	movq	%rsi, %rax
	movq	%rsi, %rdi
	movl	$1, %esi
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
.L168:
	movq	128(%rsp), %rax
	movzbl	72(%rsp), %r8d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L178:
	je	.L596
.L180:
	leaq	(%rax,%rax,4), %rax
	addq	$4, %r13
	movl	%r8d, %ebx
	leaq	(%rcx,%rax,2), %rax
	movl	0(%r13), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L597
.L182:
	cmpq	%rdx, %rax
	jle	.L178
.L181:
	testb	%bl, %bl
	jne	.L598
.L179:
	cmpq	$-1, %r14
	je	.L599
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%esi, %esi
	movl	$34, %fs:(%rax)
	je	.L186
	movl	52(%rsp), %ebx
	fldt	.LC0(%rip)
	testl	%ebx, %ebx
	jne	.L600
.L188:
	fmul	%st(0), %st
	.p2align 4,,10
	.p2align 3
.L189:
	addq	$4, %r13
	movl	0(%r13), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L189
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L108
	movq	%r13, (%rax)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L565:
	movl	4(%rbp), %r12d
	movl	$1, 52(%rsp)
	leaq	8(%rbx), %rbp
	jmp	.L113
.L572:
	movl	%r13d, %edx
	movq	%r10, %rsi
	movq	%r14, %rcx
	movq	%rbp, %rdi
	movq	%r11, 80(%rsp)
	movq	%r9, 64(%rsp)
	movq	%r10, 56(%rsp)
	call	__correctly_grouped_prefixwc
	movq	56(%rsp), %r10
	movq	%rax, %r13
	movq	64(%rsp), %r9
	movq	80(%rsp), %r11
	cmpq	%rax, %r10
	je	.L143
	cmpq	%rax, %rbp
	je	.L144
	cmpq	%rax, %r9
	ja	.L536
	movq	%r9, %rax
	movl	$0, %ebp
	jnb	.L536
.L146:
	movl	(%rax), %esi
	leal	-48(%rsi), %edx
	cmpl	$10, %edx
	adcq	$0, %rbp
	addq	$4, %rax
	cmpq	%rax, %r13
	ja	.L146
	movq	%rbp, %r11
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$0, 32(%rsp)
	je	.L200
.L344:
	movq	32(%rsp), %rax
	movq	%r13, (%rax)
.L200:
	testq	%rbp, %rbp
	jne	.L201
.L135:
	movl	52(%rsp), %r12d
	testl	%r12d, %r12d
	je	.L124
	fldz
	fchs
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L576:
	cmpl	$16, 16(%rsp)
	leaq	4(%r10), %r13
	je	.L165
.L166:
	testq	%r11, %r11
	je	.L174
	testq	%r14, %r14
	jne	.L389
	testq	%r11, %r11
	js	.L389
	movl	$4933, %esi
	subq	%r11, %rsi
.L171:
	testq	%rsi, %rsi
	movl	$0, %eax
	movabsq	$-3689348814741910323, %rdx
	cmovs	%rax, %rsi
	movq	%rsi, %rax
	movq	%rsi, %rdi
	xorl	%esi, %esi
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L586:
	cmpl	$16, 16(%rsp)
	jne	.L128
	cmpq	%r9, %rbp
	jne	.L128
	movl	4(%r9), %edi
	leal	-48(%rdi), %eax
	cmpl	$9, %eax
	jbe	.L128
	movq	8(%rsp), %rsi
	movq	%r9, 56(%rsp)
	movl	%edx, 64(%rsp)
	call	__GI___towlower_l
	subl	$97, %eax
	movq	56(%rsp), %r9
	cmpl	$5, %eax
	ja	.L131
	movl	64(%rsp), %edx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L578:
	movl	16(%rsp), %esi
	xorl	%eax, %eax
	movq	%r13, 56(%rsp)
	movq	%r14, 64(%rsp)
	leaq	-4(%r10), %r12
	movq	%r9, 40(%rsp)
	movq	%r11, %r13
	cmpl	$16, %esi
	sete	%al
	leaq	1(%rax,%rax,2), %rbx
	movq	%rbx, %r14
	movl	%esi, %ebx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L196:
	subl	$48, %edi
	cmpl	$9, %edi
	seta	%al
.L197:
	testb	%al, %al
	jne	.L198
	cmpl	$48, (%r12)
	jne	.L547
	movq	128(%rsp), %rax
	subq	$1, %r13
	addq	%r14, %rax
	subq	$1, %rbp
	movq	%rax, 128(%rsp)
	je	.L547
	testq	%rax, %rax
	jns	.L547
.L198:
	subq	$4, %r12
.L195:
	cmpl	$16, %ebx
	movl	(%r12), %edi
	jne	.L196
	movq	8(%rsp), %rsi
	call	__GI___iswxdigit_l
	testl	%eax, %eax
	sete	%al
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L580:
	movq	%r9, %r12
	movq	%r11, %r14
	movq	8(%rsp), %r13
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L372:
	movq	%rdx, %r12
.L211:
	movl	(%r12), %edi
	movq	%r13, %rsi
	call	__GI___iswxdigit_l
	leaq	4(%r12), %rdx
	testl	%eax, %eax
	movq	%rdx, %rbx
	je	.L372
	movl	(%r12), %edi
	movq	%r12, %r9
	movq	%r14, %r11
	cmpl	$48, %edi
	je	.L213
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L373:
	movq	%rdx, %r9
	addq	$4, %rdx
.L213:
	movl	(%rdx), %edi
	cmpl	$48, %edi
	je	.L373
	leaq	8(%r9), %rbx
.L212:
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	ja	.L214
	movslq	%edx, %rdx
.L215:
	leaq	nbits.12074(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	je	.L601
	movl	$64, %ecx
	movl	$63, %r13d
	movslq	%eax, %rsi
	subl	%eax, %ecx
	subl	%eax, %r13d
	salq	%cl, %rdx
	movq	128(%rsp), %rcx
	movq	%rdx, 136(%rsp)
	testq	%rcx, %rcx
	js	.L602
	movabsq	$9223372036854775807, %rdx
	subq	%rcx, %rdx
	subq	%rsi, %rdx
	leaq	4(%rdx), %rsi
	addq	$1, %rdx
	cmovs	%rsi, %rdx
	sarq	$2, %rdx
.L218:
	cmpq	%r11, %rdx
	jb	.L603
	subl	$1, %eax
	cltq
	leaq	-4(%rax,%r11,4), %rsi
	addq	%rcx, %rsi
	subq	$1, %rbp
	movq	%rsi, 128(%rsp)
	je	.L220
	movq	8(%rsp), %r12
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L223:
	cmpl	$2, %r13d
	movq	136(%rsp), %rdi
	leaq	-1(%rbp), %rdx
	jle	.L224
	leal	-3(%r13), %ecx
	movq	%rsi, %rax
	subl	$4, %r13d
	movq	%r14, %rbx
	movq	%rdx, %rbp
	salq	%cl, %rax
	orq	%rdi, %rax
	testq	%rdx, %rdx
	movq	%rax, 136(%rsp)
	je	.L604
.L225:
	movl	(%rbx), %edi
	movq	%r12, %rsi
	leaq	4(%rbx), %r14
	call	__GI___iswxdigit_l
	testl	%eax, %eax
	jne	.L221
	leaq	8(%rbx), %rax
	movq	%r14, %rbx
	movq	%rax, %r14
.L221:
	movl	(%rbx), %edi
	leal	-48(%rdi), %eax
	cmpl	$9, %eax
	movslq	%eax, %rsi
	jbe	.L223
	movq	%r12, %rsi
	call	__GI___towlower_l
	leal	-87(%rax), %esi
	jmp	.L223
.L590:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	jne	.L144
	cmpl	$40, 12(%rbp)
	leaq	12(%rbp), %rbx
	je	.L605
	flds	.LC5(%rip)
.L121:
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L123
	movq	%rbx, (%rax)
.L123:
	movl	52(%rsp), %r13d
	testl	%r13d, %r13d
	je	.L108
	fchs
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L591:
	movq	32(%rsp), %rax
	movq	40(%rsp), %rsi
	fldz
	movq	%rsi, (%rax)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L571:
	movq	8(%rsp), %rsi
	movl	%r12d, %edi
	movb	%cl, 80(%rsp)
	movq	%r11, 72(%rsp)
	movq	%r10, 64(%rsp)
	movq	%r9, 56(%rsp)
	call	__GI___towlower_l
	subl	$97, %eax
	movq	56(%rsp), %r9
	movq	64(%rsp), %r10
	cmpl	$5, %eax
	movq	72(%rsp), %r11
	movzbl	80(%rsp), %ecx
	jbe	.L140
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L569:
	movl	4(%rbp), %edi
	movq	8(%rsp), %rsi
	call	__GI___towlower_l
	cmpl	$120, %eax
	jne	.L125
	movl	8(%rbp), %r12d
	xorl	%r14d, %r14d
	addq	$8, %rbp
	movl	$16, 16(%rsp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L596:
	cmpq	%rdi, %rcx
	jle	.L180
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L575:
	movl	8(%r10), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L368
	cmpl	$16, 16(%rsp)
	leaq	8(%r10), %r13
	jne	.L166
.L165:
	testq	%r11, %r11
	je	.L606
	testq	%r14, %r14
	jne	.L388
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r11
	ja	.L388
	movl	$4096, %eax
	subq	%r11, %rax
	leaq	3(,%rax,4), %rsi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L592:
	movq	%rbx, %r11
	movq	%r14, %rbp
	movq	64(%rsp), %r9
	movq	%r15, %r14
	movzbl	56(%rsp), %ebx
	movl	40(%rsp), %r15d
	movq	%r13, %r10
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L563:
	movq	80(%rax), %r14
	movzbl	(%r14), %edi
	leal	-1(%rdi), %edx
	cmpb	$125, %dl
	ja	.L354
	movl	96(%rax), %r13d
	movl	$0, %edx
	testl	%r13d, %r13d
	cmove	%rdx, %r14
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%r13, %r11
	movq	40(%rsp), %r9
	movq	56(%rsp), %r13
	movq	64(%rsp), %r14
	jmp	.L151
.L597:
	movq	%rax, %rdx
	negq	%rdx
	testl	%esi, %esi
	cmovne	%rdx, %rax
	movq	%rax, 128(%rsp)
	jmp	.L160
.L589:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	jne	.L144
	movq	32(%rsp), %r15
	testq	%r15, %r15
	je	.L118
	leaq	12(%rbp), %rbx
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	addq	$32, %rbp
	movq	%rbx, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	cmove	%rbp, %rbx
	movq	%rbx, (%r15)
.L118:
	movl	52(%rsp), %r14d
	testl	%r14d, %r14d
	je	.L358
	flds	.LC12(%rip)
	jmp	.L108
.L599:
	movl	52(%rsp), %ebp
	fldz
	testl	%ebp, %ebp
	je	.L189
	fchs
	jmp	.L189
.L581:
	movq	%r11, %rax
	negq	%rax
	cmpq	%rcx, %rax
	cmovl	%rcx, %rax
	jmp	.L230
.L186:
	movl	52(%rsp), %r11d
	fldt	.LC2(%rip)
	testl	%r11d, %r11d
	je	.L188
	fldt	.LC3(%rip)
	fmulp	%st, %st(1)
	jmp	.L189
.L587:
	cmpl	$112, %eax
	jne	.L131
	cmpq	%r9, %rbp
	jne	.L128
	jmp	.L131
.L354:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L109
.L174:
	cmpq	$-1, %r14
	je	.L370
	movabsq	$9223372036854770874, %rax
	cmpq	%rax, %r14
	ja	.L607
	leaq	4933(%r14), %rsi
.L555:
	movq	%rsi, %rax
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	movq	%rsi, %rdi
	xorl	%esi, %esi
	jmp	.L168
.L600:
	fldt	.LC1(%rip)
	fmulp	%st, %st(1)
	jmp	.L189
.L606:
	cmpq	$-1, %r14
	je	.L369
	movabsq	$2305843009213689855, %rax
	cmpq	%rax, %r14
	ja	.L608
	leaq	16387(,%r14,4), %rsi
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L584:
	movq	%r15, %rbp
	cmpq	16(%rsp), %rbp
	movl	%r13d, 104(%rsp)
	movq	72(%rsp), %r9
	movl	64(%rsp), %r15d
	je	.L609
.L268:
	movq	88(%rsp), %r8
	movq	80(%rsp), %rcx
	movq	%r9, %rdi
	movq	16(%rsp), %rdx
	movl	40(%rsp), %esi
	call	str_to_mpn
	leaq	-1(%r14), %rax
	bsrq	7024(%rsp,%rax,8), %rbx
	movq	%rax, 40(%rsp)
	xorq	$63, %rbx
	testl	%ebx, %ebx
	jne	.L269
.L558:
	movq	120(%rsp), %rdx
.L270:
	movq	56(%rsp), %rax
	cmpq	$1, %r14
	movq	%rax, 128(%rsp)
	je	.L273
	cmpq	$2, %r14
	jne	.L610
	cmpq	$1, %rdx
	movq	7024(%rsp), %rdi
	movq	7032(%rsp), %rsi
	movq	144(%rsp), %rbp
	jg	.L281
	cmpq	%rbp, %rsi
	jbe	.L380
	movl	8(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L611
	movl	$64, %r14d
	subl	8(%rsp), %r14d
	leaq	136(%rsp), %rax
	movq	%rax, 56(%rsp)
	je	.L285
	movl	%r14d, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	__mpn_lshift
	movq	144(%rsp), %rbp
.L285:
	movq	%rbp, %rbx
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L286:
	orq	%r12, %rbx
	movl	$63, %r8d
	setne	%r9b
	subl	%r14d, %r8d
	orl	%r15d, %r9d
.L560:
	movq	128(%rsp), %rax
	movl	52(%rsp), %edx
	andl	$1, %r9d
	movq	56(%rsp), %rdi
	movslq	%r8d, %r8
	movq	%rbp, %rcx
	leaq	-1(%rax), %rsi
	call	round_and_return
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%r15, %rax
	movq	32(%rsp), %r15
	movq	%rax, 32(%rsp)
	jmp	.L265
.L269:
	movq	24(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	16(%rsp), %rdi
	movq	120(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L558
	movq	120(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, 144(%rsp,%rcx,8)
	movq	%rdx, 120(%rsp)
	jmp	.L270
.L235:
	leaq	144(%rsp), %rax
	leaq	128(%rsp), %r8
	leaq	120(%rsp), %rcx
	movq	%r9, %rdi
	movl	%ebx, %esi
	movq	%rax, %rdx
	movq	%r8, 88(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%rax, 16(%rsp)
	call	str_to_mpn
	movq	128(%rsp), %rdx
	movq	%rax, %r9
	movq	120(%rsp), %r12
	testq	%rdx, %rdx
	jle	.L240
	leaq	7024(%rsp), %rax
	leaq	_fpioconst_pow10(%rip), %r14
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	%rbx, %r12
	movq	%rax, 24(%rsp)
	movq	%rax, %r10
	movq	16(%rsp), %rax
	movq	%rax, 8(%rsp)
.L241:
	movslq	%r13d, %rax
	testq	%rdx, %rax
	je	.L242
.L613:
	movq	8(%r14), %rsi
	xorq	%rdx, %rax
	movq	%r9, 40(%rsp)
	movq	%rax, 128(%rsp)
	movq	(%r14), %rax
	leaq	-1(%rsi), %rbx
	leaq	__tens(%rip), %rsi
	cmpq	%rbx, %rdi
	leaq	8(%rsi,%rax,8), %rsi
	jl	.L243
	movq	%rsi, %rcx
	movq	8(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%rbx, %r8
	movq	%r10, %rdi
	movq	%r10, 32(%rsp)
	call	__mpn_mul
	movq	32(%rsp), %r10
	movq	40(%rsp), %r9
.L244:
	movq	120(%rsp), %rdi
	movq	128(%rsp), %rdx
	addq	%rbx, %rdi
	testq	%rax, %rax
	movq	%rdi, 120(%rsp)
	jne	.L245
	subq	$1, %rdi
	movq	%rdi, 120(%rsp)
.L245:
	addl	%r13d, %r13d
	addq	$24, %r14
	testq	%rdx, %rdx
	je	.L612
	movq	8(%rsp), %rax
	movq	%r10, 8(%rsp)
	movq	%rax, %r10
	movslq	%r13d, %rax
	testq	%rdx, %rax
	jne	.L613
.L242:
	addl	%r13d, %r13d
	addq	$24, %r14
	jmp	.L241
.L243:
	movq	8(%rsp), %rcx
	movq	%rdi, %r8
	movq	%rbx, %rdx
	movq	%r10, %rdi
	movq	%r10, 32(%rsp)
	call	__mpn_mul
	movq	40(%rsp), %r9
	movq	32(%rsp), %r10
	jmp	.L244
.L612:
	cmpq	24(%rsp), %r10
	movq	%r12, %rbx
	movq	%rdi, %r12
	jne	.L240
	leaq	0(,%rdi,8), %rdx
	movq	16(%rsp), %rdi
	movq	%r10, %rsi
	movq	%r9, 8(%rsp)
	call	__GI_memcpy@PLT
	movq	8(%rsp), %r9
.L240:
	leaq	-1(%r12), %rdx
	movl	%r12d, %r13d
	sall	$6, %r13d
	bsrq	144(%rsp,%rdx,8), %rax
	xorq	$63, %rax
	subl	%eax, %r13d
	cmpl	$16384, %r13d
	movl	%r13d, 8(%rsp)
	jg	.L557
	cmpl	$64, %r13d
	jle	.L249
	leal	-64(%r13), %ecx
	movl	%ecx, %eax
	sarl	$6, %eax
	andl	$63, %ecx
	movslq	%eax, %rsi
	movq	144(%rsp,%rsi,8), %r10
	jne	.L250
	subq	$1, %rsi
	movq	%r10, 136(%rsp)
	movl	$63, %r8d
	movq	144(%rsp,%rsi,8), %r10
.L251:
	cmpq	$0, 144(%rsp)
	jne	.L252
	movq	16(%rsp), %rdx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%eax, %r15d
	addq	$1, %rax
	cmpq	$0, -8(%rdx,%rax,8)
	je	.L253
.L252:
	cmpq	%rbx, %rbp
	movl	$1, %r9d
	ja	.L254
	movslq	%r15d, %rax
	xorl	%r9d, %r9d
	cmpq	%rsi, %rax
	setl	%r9b
.L254:
	leal	-1(%r13), %esi
	movl	52(%rsp), %edx
	leaq	136(%rsp), %rdi
	movq	%r10, %rcx
	movslq	%esi, %rsi
	call	round_and_return
	jmp	.L108
.L604:
	movq	128(%rsp), %rsi
.L220:
	movl	52(%rsp), %edx
	leaq	136(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L108
.L594:
	movabsq	$2305843009213689840, %rax
	cmpq	%rax, %r11
	ja	.L614
	leaq	16445(,%r11,4), %rsi
	movabsq	$-3689348814741910323, %rdx
	movq	%rsi, %rax
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	movq	%rsi, %rdi
	movl	$1, %esi
	jmp	.L168
.L579:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L346
	movq	128(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	cmpq	%r14, %rdx
	jb	.L208
	leaq	0(,%r14,4), %rcx
	jmp	.L209
.L224:
	movl	$3, %ecx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	subl	%r13d, %ecx
	shrq	%cl, %rdi
	leal	61(%r13), %ecx
	orq	%rdi, %rax
	salq	%cl, %rsi
	testq	%rdx, %rdx
	movq	%rax, 136(%rsp)
	movq	%rsi, %rcx
	je	.L226
	cmpl	$48, (%r14)
	jne	.L375
	leaq	-2(%rbp), %rdx
	xorl	%eax, %eax
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	addq	$1, %rax
	cmpl	$48, (%r14,%rax,4)
	jne	.L375
.L227:
	cmpq	%rdx, %rax
	jne	.L228
.L226:
	movl	52(%rsp), %edx
	movq	128(%rsp), %rsi
	leaq	136(%rsp), %rdi
	movl	%r15d, %r9d
	movl	$63, %r8d
	call	round_and_return
	jmp	.L108
.L273:
	cmpq	$1, %rdx
	movq	144(%rsp), %rcx
	movq	7024(%rsp), %rsi
	jne	.L390
	cmpq	%rsi, %rcx
	jnb	.L390
	movl	8(%rsp), %r10d
	xorl	%edi, %edi
	movl	$64, %r8d
.L275:
	movq	%rcx, %rdx
	movq	%rdi, %rax
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	testl	%r10d, %r10d
	movq	%rax, %r9
	movq	%rdx, %rcx
	je	.L615
	movl	$64, %r12d
	movq	%rax, %rbx
	leaq	136(%rsp), %rax
	movl	%r12d, %r13d
	movq	%rdx, %rbp
	subl	%r10d, %r13d
	movq	%rax, 56(%rsp)
	je	.L280
	movl	%r13d, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	__mpn_lshift
	movl	%r12d, %ecx
	movq	%rbx, %rax
	subl	%r13d, %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
.L280:
	movq	128(%rsp), %rax
	testq	%rbp, %rbp
	movl	$63, %r8d
	setne	%r9b
	movl	52(%rsp), %edx
	movq	56(%rsp), %rdi
	orl	%r15d, %r9d
	subl	%r13d, %r8d
	movq	%rbx, %rcx
	leaq	-1(%rax), %rsi
	andl	$1, %r9d
	movslq	%r8d, %r8
	call	round_and_return
	jmp	.L108
.L615:
	testq	%r9, %r9
	jne	.L278
.L616:
	movq	%r9, %rax
	movq	%rcx, %rdx
	subq	$64, 128(%rsp)
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	movq	%rax, %r9
	movq	%rdx, %rcx
	testq	%r9, %r9
	je	.L616
.L278:
	bsrq	%r9, %rax
	movl	%r8d, %r10d
	movq	%r9, 136(%rsp)
	xorq	$63, %rax
	movslq	%eax, %rdx
	subl	%eax, %r10d
	subq	%rdx, 128(%rsp)
	jmp	.L275
.L610:
	movq	40(%rsp), %rax
	movq	24(%rsp), %rdi
	movq	7024(%rsp,%rax,8), %r12
	leaq	-2(%r14), %rax
	movq	%rax, 88(%rsp)
	movq	7024(%rsp,%rax,8), %r13
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rsi
	movq	16(%rsp), %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	movq	120(%rsp), %rdx
	js	.L303
	movq	120(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	$0, 144(%rsp,%rax,8)
	movq	%rdx, 120(%rsp)
.L303:
	cmpq	%rdx, %r14
	jle	.L304
	movq	%r14, %rbx
	movl	8(%rsp), %ecx
	subq	%rdx, %rbx
	movq	%rbx, %rax
	salq	$6, %rax
	testl	%ecx, %ecx
	je	.L617
	addq	56(%rsp), %rax
	cmpq	$64, %rax
	jne	.L307
	cmpq	$1, %rbx
	jne	.L618
	leaq	136(%rsp), %rax
	movq	$0, 136(%rsp)
	movq	%rax, 56(%rsp)
.L309:
	movl	%ebx, %eax
	sall	$6, %eax
	addl	%eax, 8(%rsp)
.L306:
	testl	%edx, %edx
	movslq	%edx, %rcx
	jle	.L313
	subl	$1, %edx
	movq	16(%rsp), %rax
	addq	%rbx, %rcx
	movslq	%edx, %rdi
	movl	%edx, %edx
	leaq	0(,%rdi,8), %rsi
	salq	$3, %rdx
	subq	%rdi, %rcx
	addq	%rsi, %rax
	addq	56(%rsp), %rsi
	subq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L312:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L312
.L313:
	movq	16(%rsp), %rax
	movq	%rax, %rdx
	addq	$8, %rax
	leaq	(%rax,%rbx,8), %rcx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L619:
	addq	$8, %rax
.L311:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L619
	cmpl	$64, 8(%rsp)
	movq	$0, 7024(%rsp,%r14,8)
	movq	144(%rsp,%r14,8), %rbx
	jg	.L383
.L317:
	leaq	1(%r14), %rax
	movq	16(%rsp), %rdi
	movl	%r15d, 108(%rsp)
	movq	%r14, %r15
	movq	%rax, 64(%rsp)
	leal	-2(%r14), %eax
	movslq	%eax, %rcx
	movl	%eax, %eax
	leaq	0(,%rcx,8), %rdx
	salq	$3, %rax
	leaq	(%rdi,%rdx), %rsi
	addq	56(%rsp), %rdx
	movq	%rsi, 72(%rsp)
	subq	%rax, %rdx
	leal	-1(%r14), %eax
	movq	%rdx, %r14
	movslq	%eax, %r8
	movl	%eax, 80(%rsp)
	subq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L321:
	cmpq	%r12, %rbx
	movq	$-1, %rbp
	je	.L322
	movq	40(%rsp), %rax
	movq	%rbx, %rdx
	movq	88(%rsp), %rsi
	movq	144(%rsp,%rax,8), %rax
	movq	%rax, 32(%rsp)
#APP
# 1727 "../stdlib/strtod_l.c" 1
	divq %r12
# 0 "" 2
#NO_APP
	movq	%rax, %rbp
	movq	%rdx, %rbx
	movq	%r13, %rax
#APP
# 1728 "../stdlib/strtod_l.c" 1
	mulq %rbp
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L620:
	xorl	%edx, %edx
	cmpq	%r13, %rax
	setb	%dl
	subq	%r13, %rax
	subq	%rdx, %rcx
.L323:
	cmpq	%rbx, %rcx
	ja	.L326
	jne	.L322
	cmpq	%rax, 144(%rsp,%rsi,8)
	jnb	.L322
.L326:
	subq	$1, %rbp
	addq	%r12, %rbx
	jnc	.L620
.L322:
	movq	16(%rsp), %rbx
	movq	64(%rsp), %rdx
	movq	%rbp, %rcx
	movq	24(%rsp), %rsi
	movq	%r8, 32(%rsp)
	movq	%rbx, %rdi
	call	__mpn_submul_1
	cmpq	%rax, 144(%rsp,%r15,8)
	movq	32(%rsp), %r8
	je	.L327
	movq	24(%rsp), %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	__mpn_add_n
	testq	%rax, %rax
	movq	32(%rsp), %r8
	je	.L621
	subq	$1, %rbp
.L327:
	movq	40(%rsp), %rax
	movl	80(%rsp), %edx
	movq	144(%rsp,%rax,8), %rbx
	testl	%edx, %edx
	movq	72(%rsp), %rax
	movq	%rbx, 144(%rsp,%r15,8)
	jle	.L332
	.p2align 4,,10
	.p2align 3
.L329:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%r8,8)
	subq	$8, %rax
	cmpq	%rax, %r14
	jne	.L329
.L332:
	movl	8(%rsp), %eax
	movq	$0, 144(%rsp)
	testl	%eax, %eax
	jne	.L622
	testq	%rbp, %rbp
	jne	.L333
	subq	$64, 128(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L622:
	movl	8(%rsp), %eax
	testl	%eax, %eax
	leal	64(%rax), %r10d
	jle	.L623
	movl	$64, %r11d
	movl	%r11d, %eax
	subl	8(%rsp), %eax
	movl	%r11d, 32(%rsp)
	movl	%eax, 104(%rsp)
	je	.L336
	movq	56(%rsp), %rdi
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%r8, 96(%rsp)
	movl	%r10d, 8(%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	32(%rsp), %r11d
	movq	%rbp, %rax
	movq	96(%rsp), %r8
	movl	8(%rsp), %r10d
	movl	%r11d, %ecx
	subl	104(%rsp), %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
.L336:
	cmpl	$64, %r10d
	jg	.L624
	movl	%r10d, 8(%rsp)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L333:
	bsrq	%rbp, %rax
	movl	$64, %edx
	movq	%rbp, 136(%rsp)
	xorq	$63, %rax
	subl	%eax, %edx
	cltq
	subq	%rax, 128(%rsp)
	movl	%edx, 8(%rsp)
	jmp	.L321
.L623:
	movq	%rbp, 136(%rsp)
	jmp	.L336
.L624:
	movq	%r15, %r14
	movl	108(%rsp), %r15d
.L320:
	testl	%r14d, %r14d
	movl	%r14d, %edx
	js	.L337
	movslq	%r14d, %rax
	cmpq	$0, 144(%rsp,%rax,8)
	jne	.L337
	leal	-1(%r14), %eax
	movl	%r14d, %r14d
	movq	16(%rsp), %rsi
	cltq
	movq	%rax, %rcx
	subq	%r14, %rcx
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L625:
	subq	$1, %rax
	cmpq	$0, 8(%rsi,%rax,8)
	jne	.L337
.L338:
	cmpq	%rax, %rcx
	movl	%eax, %edx
	jne	.L625
.L337:
	notl	%edx
	movl	%r15d, %r9d
	movl	$63, %r8d
	shrl	$31, %edx
	andl	$1, %r9d
	subl	104(%rsp), %r8d
	orl	%edx, %r9d
	jmp	.L560
.L602:
	movabsq	$-9223372036854775808, %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	jmp	.L218
.L214:
	movq	8(%rsp), %rsi
	movq	%r11, 16(%rsp)
	call	__GI___towlower_l
	movq	16(%rsp), %r11
	leal	-87(%rax), %edx
	jmp	.L215
.L557:
	movl	52(%rsp), %r10d
	movq	__libc_errno@gottpoff(%rip), %rax
	fldt	.LC2(%rip)
	testl	%r10d, %r10d
	movl	$34, %fs:(%rax)
	je	.L234
	fldt	.LC3(%rip)
	fmulp	%st, %st(1)
	jmp	.L108
.L375:
	movl	$1, %r15d
	jmp	.L226
.L249:
	cmpq	%rbx, %rbp
	jne	.L255
	subl	$1, %r13d
	movl	$1, %ebx
	leaq	136(%rsp), %rsi
	movl	%r13d, %edx
	movq	%rbx, %rbp
	sarl	$31, %edx
	subq	%r12, %rbp
	movq	%rsi, 56(%rsp)
	shrl	$26, %edx
	leaq	(%rsi,%rbp,8), %rdi
	leal	0(%r13,%rdx), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$63, %eax
	je	.L626
	movq	16(%rsp), %rsi
	movl	$63, %ecx
	movq	%r12, %rdx
	subl	%eax, %ecx
	call	__mpn_lshift
	movq	%rbx, %rax
	subq	120(%rsp), %rax
	testq	%rax, %rax
	jle	.L258
.L257:
	movq	$0, 136(%rsp)
.L258:
	movl	52(%rsp), %edx
	movq	56(%rsp), %rdi
	movslq	%r13d, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L108
.L234:
	fmul	%st(0), %st
	jmp	.L108
.L609:
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdi
	leaq	0(,%r14,8), %rdx
	movq	%r9, 32(%rsp)
	call	__GI_memcpy@PLT
	movq	32(%rsp), %r9
	jmp	.L268
.L358:
	flds	.LC11(%rip)
	jmp	.L108
.L281:
	movq	152(%rsp), %rbx
.L284:
	movl	8(%rsp), %ecx
	movq	%rdi, %r9
	xorl	%r8d, %r8d
	movl	$64, %r10d
	negq	%r9
.L300:
	cmpq	%rsi, %rbx
	jne	.L287
	addq	%rbp, %rbx
	jnc	.L290
	subq	%rdi, %rbx
	movq	%r8, %r12
#APP
# 1607 "../stdlib/strtod_l.c" 1
	addq %rdi,%r12
	adcq $0,%rbx
# 0 "" 2
#NO_APP
	testl	%ecx, %ecx
	je	.L381
	movl	%ecx, 8(%rsp)
	movq	$-1, %rbp
.L292:
	movl	$64, %r13d
	leaq	136(%rsp), %rax
	movl	%r13d, %r14d
	subl	8(%rsp), %r14d
	jne	.L352
	movq	%rax, 56(%rsp)
	jmp	.L286
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%rbp, %rax
	movq	%rbx, %rdx
#APP
# 1615 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	movq	%rax, %rbp
	movq	%rdx, %rbx
	movq	%rdi, %rax
#APP
# 1616 "../stdlib/strtod_l.c" 1
	mulq %rbp
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L298:
	cmpq	%rbx, %rdx
	ja	.L294
	jne	.L295
	testq	%rax, %rax
	je	.L295
.L294:
	subq	$1, %rbp
#APP
# 1625 "../stdlib/strtod_l.c" 1
	subq %rdi,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%rsi, %rbx
	jnc	.L298
.L295:
	movq	%r8, %r12
#APP
# 1630 "../stdlib/strtod_l.c" 1
	subq %rax,%r12
	sbbq %rdx,%rbx
# 0 "" 2
#NO_APP
	testl	%ecx, %ecx
	jne	.L549
	testq	%rbp, %rbp
	movl	$64, %eax
	jne	.L627
.L299:
	movq	%rbp, 136(%rsp)
	subq	%rax, 128(%rsp)
	movq	%r12, %rbp
	jmp	.L300
.L549:
	movl	%ecx, 8(%rsp)
	jmp	.L292
.L290:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rdi, %rdx
	setne	%al
	movq	$-1, %rbp
	subq	%rax, %rdx
	movq	%r9, %rax
	jmp	.L298
.L381:
	movl	$64, %ecx
	xorl	%eax, %eax
	movq	$-1, %rbp
	jmp	.L299
.L352:
	movl	%r14d, %ecx
	movq	%rax, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, 56(%rsp)
	call	__mpn_lshift
	movl	%r13d, %ecx
	movq	%rbp, %rax
	subl	%r14d, %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
	jmp	.L286
.L304:
	jne	.L628
	testl	%r14d, %r14d
	jle	.L629
	leal	-1(%r14), %ecx
	movq	16(%rsp), %rax
	leaq	136(%rsp), %rsi
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	movq	%rsi, 56(%rsp)
	salq	$3, %rdx
	salq	$3, %rcx
	addq	%rdx, %rax
	addq	%rsi, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L318:
	movq	(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rax, %rcx
	jne	.L318
.L319:
	movq	$0, 144(%rsp)
	movq	$0, 7024(%rsp,%r14,8)
	movq	144(%rsp,%r14,8), %rbx
	jmp	.L317
.L582:
	movl	52(%rsp), %r9d
	movq	__libc_errno@gottpoff(%rip), %rax
	fldt	.LC0(%rip)
	testl	%r9d, %r9d
	movl	$34, %fs:(%rax)
	je	.L234
	fldt	.LC1(%rip)
	fmulp	%st, %st(1)
	jmp	.L108
.L370:
	xorl	%esi, %esi
	movl	$3, %edi
	movl	$493, %edx
	jmp	.L168
.L588:
	cmpl	$16, 16(%rsp)
	leaq	-4(%r9), %rax
	cmovne	40(%rsp), %rax
	jmp	.L136
.L255:
	movq	16(%rsp), %rsi
	leaq	136(%rsp), %rdi
	leaq	0(,%r12,8), %rdx
	movq	%r9, 24(%rsp)
	call	__GI_memcpy@PLT
	cmpq	%rbx, %rbp
	jbe	.L238
	movq	128(%rsp), %rax
	leaq	4951(%rax), %rdx
	cmpq	$4951, %rdx
	ja	.L238
	testl	%r13d, %r13d
	jle	.L260
	testq	%rax, %rax
	movq	24(%rsp), %r9
	jne	.L630
	movslq	8(%rsp), %rax
	movl	$65, %ecx
	xorl	%edx, %edx
	subl	%eax, %ecx
	movq	%rax, 56(%rsp)
	jmp	.L262
.L250:
	movslq	%ecx, %r8
	movq	%r10, %rdi
	subq	$1, %r8
	shrq	%cl, %rdi
	cmpq	%rsi, %rdx
	jg	.L349
	movq	%rdi, 136(%rsp)
	jmp	.L251
.L380:
	xorl	%ebx, %ebx
	jmp	.L284
.L369:
	xorl	%esi, %esi
	movl	$7, %edi
	movl	$1638, %edx
	jmp	.L168
.L536:
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L135
	movq	%r13, (%rax)
	jmp	.L135
.L605:
	leaq	7024(%rsp), %rsi
	leaq	16(%rbp), %rdi
	movl	$41, %edx
	call	__GI___wcstold_nan
	movq	7024(%rsp), %rax
	cmpl	$41, (%rax)
	leaq	4(%rax), %rdx
	cmove	%rdx, %rbx
	jmp	.L121
.L617:
	subq	%rax, 128(%rsp)
	leaq	136(%rsp), %rax
	movl	$0, 104(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.L306
.L307:
	movl	$64, %eax
	subl	8(%rsp), %eax
	leaq	136(%rsp), %rdi
	movq	%rdi, 56(%rsp)
	movl	%eax, %ecx
	movl	%eax, 104(%rsp)
	je	.L309
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	120(%rsp), %rdx
	jmp	.L309
.L349:
	addl	$1, %eax
	movslq	%eax, %rdx
	movl	$64, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	144(%rsp,%rdx,8), %rax
	salq	%cl, %rax
	orq	%rdi, %rax
	movq	%rax, 136(%rsp)
	jmp	.L251
.L626:
	movq	16(%rsp), %rsi
	leaq	0(,%r12,8), %rdx
	call	__GI_memcpy@PLT
	testq	%rbp, %rbp
	jle	.L258
	jmp	.L257
.L611:
	movq	56(%rsp), %rax
	movq	%rbp, %rbx
	xorl	%ebp, %ebp
	subq	$64, %rax
	movq	%rax, 128(%rsp)
	jmp	.L284
.L383:
	xorl	%ebp, %ebp
	jmp	.L320
.L629:
	leaq	136(%rsp), %rax
	movq	%rax, 56(%rsp)
	jmp	.L319
.L260:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC31(%rip), %rdi
	movl	$1376, %edx
	call	__GI___assert_fail
.L627:
	bsrq	%rbp, %rdx
	movl	%r10d, %ecx
	xorq	$63, %rdx
	movslq	%edx, %rax
	subl	%edx, %ecx
	jmp	.L299
.L564:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$593, %edx
	call	__GI___assert_fail
.L595:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$938, %edx
	call	__GI___assert_fail
.L621:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$1750, %edx
	call	__GI___assert_fail
.L628:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$1708, %edx
	call	__GI___assert_fail
.L618:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$1671, %edx
	call	__GI___assert_fail
.L390:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	movl	$1497, %edx
	call	__GI___assert_fail
.L341:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$875, %edx
	call	__GI___assert_fail
.L598:
	movq	%rax, 128(%rsp)
	jmp	.L179
.L208:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$1076, %edx
	call	__GI___assert_fail
.L346:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$1072, %edx
	call	__GI___assert_fail
.L614:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$906, %edx
	call	__GI___assert_fail
.L389:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$946, %edx
	call	__GI___assert_fail
.L388:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$914, %edx
	call	__GI___assert_fail
.L603:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$1121, %edx
	call	__GI___assert_fail
.L238:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$1360, %edx
	call	__GI___assert_fail
.L607:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$958, %edx
	call	__GI___assert_fail
.L583:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$1397, %edx
	call	__GI___assert_fail
.L630:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$1370, %edx
	call	__GI___assert_fail
.L608:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$926, %edx
	call	__GI___assert_fail
.L601:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$1100, %edx
	call	__GI___assert_fail
.L577:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	movl	$1021, %edx
	call	__GI___assert_fail
	.size	____wcstold_l_internal, .-____wcstold_l_internal
	.p2align 4,,15
	.weak	__GI___wcstold_l
	.hidden	__GI___wcstold_l
	.type	__GI___wcstold_l, @function
__GI___wcstold_l:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	jmp	____wcstold_l_internal
	.size	__GI___wcstold_l, .-__GI___wcstold_l
	.globl	__wcstold_l
	.set	__wcstold_l,__GI___wcstold_l
	.weak	wcstold_l
	.set	wcstold_l,__wcstold_l
	.weak	wcstof64x_l
	.set	wcstof64x_l,wcstold_l
	.globl	__GI_wcstold_l
	.set	__GI_wcstold_l,__wcstold_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11984, @object
	.size	__PRETTY_FUNCTION__.11984, 11
__PRETTY_FUNCTION__.11984:
	.string	"str_to_mpn"
	.section	.rodata
	.align 32
	.type	nbits.12074, @object
	.size	nbits.12074, 64
nbits.12074:
	.long	0
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12028, @object
	.size	__PRETTY_FUNCTION__.12028, 23
__PRETTY_FUNCTION__.12028:
	.string	"____wcstold_l_internal"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC1:
	.long	0
	.long	2147483648
	.long	32769
	.long	0
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	65534
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC5:
	.long	2143289344
	.align 4
.LC11:
	.long	2139095040
	.align 4
.LC12:
	.long	4286578688
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
	.hidden	__mpn_cmp
	.hidden	__mpn_lshift
	.hidden	_nl_C_locobj
	.hidden	__correctly_grouped_prefixwc
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	__mpn_mul_1
	.hidden	__mpn_construct_long_double
	.hidden	__mpn_rshift
