	.text
	.p2align 4,,15
	.globl	wcwidth
	.type	wcwidth, @function
wcwidth:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	160(%rax), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	cmpl	4(%rdx), %ecx
	movl	$-1, %eax
	jnb	.L1
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L1
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L1
	andl	16(%rdx), %edi
	addq	%rdi, %rdx
	movzbl	(%rdx,%rcx), %eax
	cmpl	$255, %eax
	je	.L9
.L1:
	rep ret
.L9:
	orl	$-1, %eax
	ret
	.size	wcwidth, .-wcwidth
	.hidden	_nl_current_LC_CTYPE
