	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wcsncat
	.type	wcsncat, @function
wcsncat:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %r12
	subq	$8, %rsp
	call	__wcslen@PLT
	leaq	(%rbx,%rax,4), %rbp
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__wcsnlen@PLT
	movq	%r12, %rsi
	movl	$0, 0(%rbp,%rax,4)
	movq	%rbp, %rdi
	movq	%rax, %rdx
	call	__wmemcpy
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	wcsncat, .-wcsncat
	.hidden	__wmemcpy
