	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__isoc99_swscanf
	.type	__isoc99_swscanf, @function
__isoc99_swscanf:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$688, %rsp
	testb	%al, %al
	movq	%rdx, 528(%rsp)
	movq	%rcx, 536(%rsp)
	movq	%r8, 544(%rsp)
	movq	%r9, 552(%rsp)
	je	.L3
	movaps	%xmm0, 560(%rsp)
	movaps	%xmm1, 576(%rsp)
	movaps	%xmm2, 592(%rsp)
	movaps	%xmm3, 608(%rsp)
	movaps	%xmm4, 624(%rsp)
	movaps	%xmm5, 640(%rsp)
	movaps	%xmm6, 656(%rsp)
	movaps	%xmm7, 672(%rsp)
.L3:
	leaq	272(%rsp), %rbx
	leaq	32(%rsp), %rcx
	leaq	_IO_wstr_jumps(%rip), %r8
	xorl	%edx, %edx
	movl	$32768, %esi
	movq	%rbx, %rdi
	movq	$0, 408(%rsp)
	call	_IO_no_init@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_IO_fwide@PLT
	movq	%r12, %rsi
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_IO_wstr_init_static@PLT
	leaq	720(%rsp), %rax
	leaq	8(%rsp), %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$2, %ecx
	movl	$16, 8(%rsp)
	movq	%rax, 16(%rsp)
	leaq	512(%rsp), %rax
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vfwscanf_internal
	addq	$688, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__isoc99_swscanf, .-__isoc99_swscanf
	.hidden	__vfwscanf_internal
	.hidden	_IO_wstr_jumps
