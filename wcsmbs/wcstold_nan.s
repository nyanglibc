	.text
	.p2align 4,,15
	.globl	__wcstold_nan
	.hidden	__wcstold_nan
	.type	__wcstold_nan, @function
__wcstold_nan:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$4, %rbx
.L2:
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L3
	leal	-48(%rcx), %eax
	cmpl	$9, %eax
	jbe	.L3
	cmpl	$95, %ecx
	je	.L3
	cmpl	%edx, %ecx
	je	.L4
.L9:
	flds	.LC0(%rip)
.L5:
	testq	%rbp, %rbp
	je	.L1
	movq	%rbx, 0(%rbp)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L4:
	leaq	8(%rsp), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	____wcstoull_l_internal
	cmpq	%rbx, 8(%rsp)
	jne	.L9
	flds	.LC0(%rip)
	movq	%rax, %rdx
	shrq	$32, %rdx
	fstpt	16(%rsp)
	movl	%edx, %ecx
	movl	%eax, 16(%rsp)
	andl	$1073741823, %ecx
	movl	20(%rsp), %edx
	andl	$-1073741824, %edx
	orl	%ecx, %edx
	movl	%edx, 20(%rsp)
	orl	%eax, %edx
	je	.L9
	fldt	16(%rsp)
	jmp	.L5
	.size	__wcstold_nan, .-__wcstold_nan
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.hidden	____wcstoull_l_internal
	.hidden	_nl_C_locobj
