	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstof128_nan
	.hidden	__GI___wcstof128_nan
	.type	__GI___wcstof128_nan, @function
__GI___wcstof128_nan:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$4, %rbx
.L2:
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L3
	leal	-48(%rcx), %eax
	cmpl	$9, %eax
	jbe	.L3
	cmpl	$95, %ecx
	je	.L3
	cmpl	%edx, %ecx
	je	.L4
.L9:
	movdqa	.LC0(%rip), %xmm0
.L5:
	testq	%rbp, %rbp
	je	.L1
	movq	%rbx, 0(%rbp)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L4:
	leaq	24(%rsp), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	____wcstoull_l_internal
	cmpq	%rbx, 24(%rsp)
	jne	.L9
	xorl	%esi, %esi
	movq	%rax, %rcx
	movabsq	$9223231299366420480, %rdi
	movq	%rsi, (%rsp)
	movq	(%rsp), %rdx
	shrq	$32, %rcx
	movq	%rcx, %rsi
	movq	%rdi, 8(%rsp)
	movabsq	$-4294967296, %rdi
	salq	$32, %rsi
	movl	%edx, %edx
	orq	%rsi, %rdx
	movl	%eax, %esi
	andq	%rdi, %rdx
	orq	%rsi, %rdx
	movq	%rdx, (%rsp)
	movdqa	(%rsp), %xmm1
	pextrw	$6, %xmm1, %edx
	orl	%edx, %eax
	orl	%ecx, %eax
	je	.L9
	movdqa	(%rsp), %xmm0
	jmp	.L5
	.size	__GI___wcstof128_nan, .-__GI___wcstof128_nan
	.globl	__wcstof128_nan
	.set	__wcstof128_nan,__GI___wcstof128_nan
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.hidden	____wcstoull_l_internal
	.hidden	_nl_C_locobj
