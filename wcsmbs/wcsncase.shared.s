	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcsncasecmp
	.type	__wcsncasecmp, @function
__wcsncasecmp:
	cmpq	%rsi, %rdi
	je	.L10
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rdx, %r13
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$4, %rbp
	movl	-4(%rbp), %edi
	addq	$4, %r12
	call	__GI_towlower
	movl	-4(%r12), %edi
	movl	%eax, %ebx
	call	__GI_towlower
	testl	%ebx, %ebx
	je	.L14
	cmpl	%eax, %ebx
	jne	.L14
	subq	$1, %r13
	jne	.L5
.L14:
	subl	%eax, %ebx
	movl	%ebx, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	ret
	.size	__wcsncasecmp, .-__wcsncasecmp
	.weak	wcsncasecmp
	.set	wcsncasecmp,__wcsncasecmp
