	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"mbsnrtowcs.c"
.LC1:
	.string	"result > 0"
.LC2:
	.string	"__mbsinit (data.__statep)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
	.text
	.p2align 4,,15
	.globl	__mbsnrtowcs
	.hidden	__mbsnrtowcs
	.type	__mbsnrtowcs, @function
__mbsnrtowcs:
	pushq	%r15
	pushq	%r14
	leaq	state(%rip), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$360, %rsp
	testq	%r8, %r8
	cmove	%rax, %r8
	testq	%rdx, %rdx
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	movq	%r8, 80(%rsp)
	je	.L19
	movq	(%rsi), %rbx
	movq	%rsi, %r14
	leaq	-1(%rdx), %rsi
	movq	%rdi, %rbp
	movq	%rcx, %r15
	movq	%rbx, %rdi
	call	__strnlen
	leaq	1(%rbx,%rax), %r13
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L26
.L5:
	movq	(%rax), %r12
	cmpq	$0, (%r12)
	movq	40(%r12), %rbx
	je	.L6
#APP
# 70 "mbsnrtowcs.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L6:
	testq	%rbp, %rbp
	je	.L27
	leaq	0(%rbp,%r15,4), %rax
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)
	movq	%rax, 56(%rsp)
	call	_dl_mcount_wrapper_check
	leaq	48(%rsp), %rsi
	pushq	$1
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	leaq	40(%rsp), %r9
	movq	%r12, %rdi
	call	*%rbx
	movq	64(%rsp), %rdx
	popq	%rsi
	popq	%rdi
	subq	%rbp, %rdx
	movq	%rdx, %rcx
	sarq	$2, %rcx
	testl	$-5, %eax
	movq	%rcx, %r15
	jne	.L12
	testq	%rcx, %rcx
	je	.L28
	movl	-4(%rbp,%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L16
	movq	80(%rsp), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L29
	movq	$0, (%r14)
.L24:
	subq	$1, %r15
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%r14), %rax
	leaq	96(%rsp), %rbp
	leaq	32(%rsp), %r14
	xorl	%r15d, %r15d
	movq	%rax, 32(%rsp)
	movq	80(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	352(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	24(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)
	call	_dl_mcount_wrapper_check
	pushq	$1
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rcx
	movq	16(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	*%rbx
	movq	64(%rsp), %rcx
	popq	%r9
	popq	%r10
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	sarq	$2, %rdx
	addq	%rdx, %r15
	cmpl	$5, %eax
	je	.L8
	testl	$-5, %eax
	je	.L30
.L12:
	leal	-5(%rax), %edx
	cmpl	$2, %edx
	ja	.L31
.L16:
	movl	$177, %edx
	btq	%rax, %rdx
	jc	.L1
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r15
	movl	$84, %fs:(%rax)
.L1:
	addq	$360, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%r15d, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movl	-4(%rcx), %r8d
	testl	%r8d, %r8d
	jne	.L16
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L20
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L5
.L20:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L5
.L29:
	leaq	__PRETTY_FUNCTION__.9885(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$121, %edx
	call	__assert_fail
.L28:
	leaq	__PRETTY_FUNCTION__.9885(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$118, %edx
	call	__assert_fail
.L31:
	leaq	__PRETTY_FUNCTION__.9885(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$132, %edx
	call	__assert_fail
	.size	__mbsnrtowcs, .-__mbsnrtowcs
	.weak	mbsnrtowcs
	.set	mbsnrtowcs,__mbsnrtowcs
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9885, @object
	.size	__PRETTY_FUNCTION__.9885, 13
__PRETTY_FUNCTION__.9885:
	.string	"__mbsnrtowcs"
	.local	state
	.comm	state,8,8
	.hidden	__assert_fail
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
	.hidden	_dl_mcount_wrapper_check
	.hidden	_nl_current_LC_CTYPE
	.hidden	__strnlen
