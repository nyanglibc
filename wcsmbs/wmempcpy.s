	.text
	.p2align 4,,15
	.globl	__wmempcpy
	.hidden	__wmempcpy
	.type	__wmempcpy, @function
__wmempcpy:
	salq	$2, %rdx
	jmp	__mempcpy@PLT
	.size	__wmempcpy, .-__wmempcpy
	.weak	wmempcpy
	.set	wmempcpy,__wmempcpy
