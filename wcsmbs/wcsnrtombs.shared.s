	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wcsnrtombs.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"data.__outbuf != (unsigned char *) dst"
	.section	.rodata.str1.1
.LC2:
	.string	"__mbsinit (data.__statep)"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__wcsnrtombs
	.hidden	__wcsnrtombs
	.type	__wcsnrtombs, @function
__wcsnrtombs:
	pushq	%r15
	pushq	%r14
	leaq	state(%rip), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$360, %rsp
	testq	%r8, %r8
	cmove	%rax, %r8
	testq	%rdx, %rdx
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	movq	%r8, 80(%rsp)
	je	.L16
	movq	(%rsi), %rbx
	movq	%rsi, %r14
	leaq	-1(%rdx), %rsi
	movq	%rdi, %rbp
	movq	%rcx, %r15
	movq	%rbx, %rdi
	call	__wcsnlen@PLT
	leaq	4(%rbx,%rax,4), %r13
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L29
.L5:
	movq	16(%rax), %r12
	cmpq	$0, (%r12)
	movq	40(%r12), %rbx
	je	.L6
#APP
# 68 "wcsnrtombs.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L6:
	testq	%rbp, %rbp
	je	.L30
	addq	%rbp, %r15
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)
	movq	%r15, 56(%rsp)
	call	__GI__dl_mcount_wrapper_check
	leaq	48(%rsp), %rsi
	pushq	$1
	pushq	$0
	xorl	%r8d, %r8d
	movq	%r13, %rcx
	movq	%r14, %rdx
	leaq	112(%rsp), %r9
	movq	%r12, %rdi
	call	*%rbx
	movq	64(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	movq	%rdx, %r15
	subq	%rbp, %r15
	testl	$-5, %eax
	jne	.L10
	cmpb	$0, -1(%rdx)
	jne	.L10
	cmpq	%rbp, %rdx
	je	.L31
	movq	80(%rsp), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L32
	movq	$0, (%r14)
.L27:
	subq	$1, %r15
.L10:
	testl	%eax, %eax
	leal	-4(%rax), %edx
	je	.L14
	cmpl	$3, %edx
	jbe	.L14
	leaq	__PRETTY_FUNCTION__.9861(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$137, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%r14), %rax
	leaq	96(%rsp), %rbp
	leaq	24(%rsp), %r14
	xorl	%r15d, %r15d
	movq	%rax, 24(%rsp)
	movq	80(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	352(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)
	call	__GI__dl_mcount_wrapper_check
	pushq	$1
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r13, %rcx
	movq	16(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rdi
	call	*%rbx
	movq	64(%rsp), %rdx
	popq	%rdi
	popq	%r8
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	addq	%rcx, %r15
	cmpl	$5, %eax
	je	.L8
	testl	$-5, %eax
	jne	.L10
	cmpb	$0, -1(%rdx)
	jne	.L10
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%eax, %eax
	je	.L1
	cmpl	$1, %edx
	jbe	.L1
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r15
	movl	$84, %fs:(%rax)
.L1:
	addq	$360, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r15d, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L17
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L5
.L17:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L5
.L32:
	leaq	__PRETTY_FUNCTION__.9861(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$126, %edx
	call	__GI___assert_fail
.L31:
	leaq	__PRETTY_FUNCTION__.9861(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$125, %edx
	call	__GI___assert_fail
	.size	__wcsnrtombs, .-__wcsnrtombs
	.weak	wcsnrtombs
	.set	wcsnrtombs,__wcsnrtombs
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9861, @object
	.size	__PRETTY_FUNCTION__.9861, 13
__PRETTY_FUNCTION__.9861:
	.string	"__wcsnrtombs"
	.local	state
	.comm	state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
