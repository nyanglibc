	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"mbrtoc16.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
	.section	.rodata.str1.1
.LC3:
	.string	"__mbsinit (data.__statep)"
#NO_APP
	.text
	.p2align 4,,15
	.globl	mbrtoc16
	.type	mbrtoc16, @function
mbrtoc16:
	pushq	%r15
	pushq	%r14
	leaq	state(%rip), %rax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbx
	subq	$104, %rsp
	testq	%rcx, %rcx
	cmove	%rax, %rbx
	movl	(%rbx), %eax
	testl	%eax, %eax
	js	.L43
	testq	%rsi, %rsi
	movq	%rsi, %rbp
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	movq	%rbx, 80(%rsp)
	je	.L19
	testq	%rdx, %rdx
	jne	.L5
.L16:
	movq	$-2, %rax
.L1:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC0(%rip), %rbp
	movl	$1, %edx
	xorl	%r14d, %r14d
.L5:
	leaq	28(%rsp), %r15
	leaq	4(%r15), %rax
	movq	%r15, 48(%rsp)
	movq	%rax, 56(%rsp)
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %r12
	movq	40(%r12), %r13
	testq	%r13, %r13
	je	.L44
.L7:
	movq	%rdx, %rcx
	movq	%rbp, 40(%rsp)
	addq	%rbp, %rcx
	jc	.L45
.L8:
	movq	0(%r13), %rdx
	cmpq	$0, (%rdx)
	movq	40(%rdx), %r12
	je	.L9
#APP
# 102 "mbrtoc16.c" 1
	ror $2*8+1, %r12
xor %fs:48, %r12
# 0 "" 2
#NO_APP
.L9:
	movq	%r12, %rdi
	movq	%rcx, 8(%rsp)
	call	__GI__dl_mcount_wrapper_check
	leaq	40(%rsp), %rdx
	leaq	48(%rsp), %rsi
	pushq	$1
	pushq	$0
	xorl	%r8d, %r8d
	movq	24(%rsp), %rcx
	movq	0(%r13), %rdi
	leaq	48(%rsp), %r9
	call	*%r12
	testl	$-5, %eax
	popq	%rdx
	popq	%rcx
	je	.L10
	leal	-5(%rax), %edx
	cmpl	$2, %edx
	ja	.L46
	cmpl	$5, %eax
	je	.L10
	cmpl	$7, %eax
	je	.L16
.L17:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	addq	$104, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	40(%rsp), %rax
	movl	28(%rsp), %edx
	subq	%rbp, %rax
	cmpl	$65535, %edx
	jg	.L13
	testq	%r14, %r14
	je	.L14
	movw	%dx, (%r14)
.L14:
	cmpq	%r15, 48(%rsp)
	je	.L1
	testl	%edx, %edx
	jne	.L1
	movq	80(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L47
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L43:
	andl	$2147483647, %eax
	movl	%eax, (%rbx)
	movl	4(%rbx), %eax
	movw	%ax, (%rdi)
	movl	$0, 4(%rbx)
	addq	$104, %rsp
	popq	%rbx
	movq	$-3, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%r14, %r14
	je	.L15
	movl	%edx, %ecx
	sarl	$10, %ecx
	subw	$10304, %cx
	movw	%cx, (%r14)
.L15:
	andl	$1023, %edx
	orl	$-2147483648, (%rbx)
	addl	$56320, %edx
	movl	%edx, 4(%rbx)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %r12
	je	.L20
	movq	%r12, %rdi
	movq	%rdx, 8(%rsp)
	call	__wcsmbs_load_conv
	movq	40(%r12), %r13
	movq	8(%rsp), %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L45:
	movq	$-1, %rcx
	cmpq	%rcx, %rbp
	jne	.L8
	jmp	.L17
.L20:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %r13
	jmp	.L7
.L46:
	leaq	__PRETTY_FUNCTION__.9877(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$115, %edx
	call	__GI___assert_fail
.L47:
	leaq	__PRETTY_FUNCTION__.9877(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$130, %edx
	call	__GI___assert_fail
	.size	mbrtoc16, .-mbrtoc16
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9877, @object
	.size	__PRETTY_FUNCTION__.9877, 9
__PRETTY_FUNCTION__.9877:
	.string	"mbrtoc16"
	.local	state
	.comm	state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
