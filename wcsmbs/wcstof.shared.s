	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstof_internal
	.hidden	__GI___wcstof_internal
	.type	__GI___wcstof_internal, @function
__GI___wcstof_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstof_l_internal
	.size	__GI___wcstof_internal, .-__GI___wcstof_internal
	.globl	__wcstof_internal
	.set	__wcstof_internal,__GI___wcstof_internal
	.p2align 4,,15
	.weak	__GI_wcstof
	.hidden	__GI_wcstof
	.type	__GI_wcstof, @function
__GI_wcstof:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstof_l_internal
	.size	__GI_wcstof, .-__GI_wcstof
	.globl	wcstof
	.set	wcstof,__GI_wcstof
	.weak	wcstof32
	.set	wcstof32,wcstof
	.hidden	____wcstof_l_internal
