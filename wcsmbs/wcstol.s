	.text
	.p2align 4,,15
	.globl	__wcstol_internal
	.hidden	__wcstol_internal
	.type	__wcstol_internal, @function
__wcstol_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	____wcstol_l_internal
	.size	__wcstol_internal, .-__wcstol_internal
	.globl	__wcstoll_internal
	.set	__wcstoll_internal,__wcstol_internal
	.p2align 4,,15
	.globl	__wcstol
	.type	__wcstol, @function
__wcstol:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	____wcstol_l_internal
	.size	__wcstol, .-__wcstol
	.weak	wcstol
	.hidden	wcstol
	.set	wcstol,__wcstol
	.weak	wcstoimax
	.set	wcstoimax,wcstol
	.weak	wcstoq
	.set	wcstoq,wcstol
	.weak	wcstoll
	.set	wcstoll,wcstol
	.hidden	____wcstol_l_internal
