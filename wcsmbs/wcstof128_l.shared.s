	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	round_away, @function
round_away:
	cmpl	$1024, %r8d
	je	.L3
	jle	.L18
	cmpl	$2048, %r8d
	je	.L6
	cmpl	$3072, %r8d
	jne	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r8d, %r8d
	jne	.L2
	orl	%esi, %ecx
	movl	$0, %eax
	testb	%dl, %dl
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmovne	%ecx, %eax
	ret
.L2:
	subq	$8, %rsp
	call	__GI_abort
	.size	round_away, .-round_away
	.globl	__multf3
	.globl	__addtf3
	.p2align 4,,15
	.type	round_and_return, @function
round_and_return:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 46(%rsp)
# 0 "" 2
#NO_APP
	movzwl	46(%rsp), %eax
	andw	$3072, %ax
	cmpw	$1024, %ax
	je	.L21
	jbe	.L70
	cmpw	$2048, %ax
	je	.L24
	cmpw	$3072, %ax
	jne	.L20
	movl	$3072, %r11d
.L23:
	cmpq	$-16382, %rbx
	jge	.L26
.L71:
	cmpq	$-16495, %rbx
	jge	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	je	.L28
	movdqa	.LC0(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$-16382, %rbx
	movl	$2048, %r11d
	jl	.L71
.L26:
	cmpq	$16383, %rbx
	jle	.L72
.L43:
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	je	.L52
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	testw	%ax, %ax
	jne	.L20
	xorl	%r11d, %r11d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$-1, %rdx
	movl	%r8d, %ecx
	movq	$-16382, %rax
	salq	%cl, %rdx
	subq	%rbx, %rax
	notq	%rdx
	testq	%r15, %rdx
	setne	%dl
	movzbl	%dl, %edx
	orl	%edx, %r9d
	cmpq	$113, %rax
	je	.L73
	movq	$-16383, %r14
	movq	%r14, %r13
	subq	%rbx, %r13
	cmpq	$63, %rax
	jle	.L32
	movq	%r13, %rdx
	movq	%r13, %rcx
	sarq	$6, %rdx
	andl	$63, %ecx
	cmpq	$1, %rdx
	movq	0(%rbp,%rdx,8), %r15
	jne	.L33
	xorl	%edx, %edx
	cmpq	$0, 0(%rbp)
	setne	%dl
	orl	%edx, %r9d
.L33:
	movq	$-1, %rdx
	movl	%ecx, (%rsp)
	salq	%cl, %rdx
	notq	%rdx
	testq	%r15, %rdx
	setne	%dl
	movzbl	%dl, %edx
	orl	%edx, %r9d
	andl	$63, %eax
	jne	.L34
	movq	8(%rbp), %r10
	movq	%r10, 0(%rbp)
.L35:
	movl	%r9d, %ebx
	movl	%r10d, %esi
	movq	$0, 8(%rbp)
	andl	$1, %ebx
	andl	$1, %esi
.L31:
	movl	(%rsp), %edi
	movq	%r15, %r13
	movl	%edi, %ecx
	shrq	%cl, %r13
	andl	$1, %r13d
	jne	.L41
	testb	%bl, %bl
	je	.L74
.L41:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r9d, 28(%rsp)
	movdqa	.LC1(%rip), %xmm1
	movq	%r10, 16(%rsp)
	movl	%r11d, 24(%rsp)
	movdqa	%xmm1, %xmm0
	movl	$34, %fs:(%rax)
	movl	%esi, 8(%rsp)
	call	__multf3@PLT
	movl	8(%rsp), %esi
	movl	24(%rsp), %r11d
	movq	$-16383, %rbx
	movq	16(%rsp), %r10
	movl	28(%rsp), %r9d
.L40:
	testl	%r9d, %r9d
	movl	$1, %r14d
	jne	.L44
	movzbl	(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r15, %rax
.L42:
	testq	%rax, %rax
	setne	%r14b
	movzbl	%r14b, %r9d
.L44:
	movzbl	%r13b, %edx
	movl	%r11d, %r8d
	movl	%r9d, %ecx
	movl	%r12d, %edi
	movq	%r10, (%rsp)
	call	round_away
	testb	%al, %al
	je	.L50
	movq	(%rsp), %r10
	movq	8(%rbp), %rax
	addq	$1, %r10
	movq	%r10, 0(%rbp)
	jnc	.L48
	addq	$1, %rax
	movq	%rax, 8(%rbp)
.L48:
	btq	$49, %rax
	jc	.L75
	cmpq	$-16383, %rbx
	jne	.L50
	movabsq	$281474976710656, %rdx
	xorl	%ebx, %ebx
	testq	%rdx, %rax
	setne	%bl
	subl	$16383, %ebx
	.p2align 4,,10
	.p2align 3
.L50:
	testb	%r14b, %r14b
	jne	.L59
	testb	%r13b, %r13b
	jne	.L59
.L53:
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	__mpn_construct_float128
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %r11d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	0(%rbp), %rdi
	movl	%r9d, %ebx
	movl	%r13d, (%rsp)
	andl	$1, %ebx
	cmpq	$1, %rax
	movq	%rdi, 8(%rsp)
	je	.L36
	movq	%rbp, %rsi
	movl	%eax, %ecx
	movl	$2, %edx
	movq	%rbp, %rdi
	movl	%r9d, 16(%rsp)
	movl	%r11d, 24(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r10
	movq	8(%rsp), %r15
	movl	24(%rsp), %r11d
	movl	16(%rsp), %r9d
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L59:
	movdqa	.LC1(%rip), %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__addtf3@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L72:
	movq	0(%rbp), %r10
	movq	%r15, %r13
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movl	%r8d, (%rsp)
	andl	$1, %r13d
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$1, %rbx
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mpn_rshift
	movabsq	$281474976710656, %rax
	orq	%rax, 8(%rbp)
	cmpq	$16384, %rbx
	je	.L43
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L74:
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r15, %rax
	jne	.L41
	movq	$-16383, %rbx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	cmpq	$0, 0(%rbp)
	movq	8(%rbp), %r15
	movq	$0, 0(%rbp)
	movq	$0, 8(%rbp)
	movl	$48, (%rsp)
	setne	%al
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	orl	%eax, %r9d
	movl	%r9d, %ebx
	andl	$1, %ebx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r15, %rdx
	movq	8(%rsp), %r15
	movl	%r11d, %r8d
	shrq	%cl, %rdx
	movl	%r12d, %edi
	movl	%r9d, %ecx
	andl	$1, %edx
	movl	%r11d, 16(%rsp)
	movl	%r9d, 24(%rsp)
	movq	%r15, %rsi
	andl	$1, %esi
	call	round_away
	testb	%al, %al
	movl	24(%rsp), %r9d
	movl	16(%rsp), %r11d
	jne	.L37
	movq	%rbp, %rsi
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rbp, %rdi
	movl	%r11d, 8(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r10
	movl	8(%rsp), %r11d
	movl	24(%rsp), %r9d
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	8(%rbp), %rsi
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%rbp, %rdi
	movl	%r9d, 24(%rsp)
	movl	%r11d, 8(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r10
	movl	8(%rsp), %r11d
	movl	24(%rsp), %r9d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%r15d, %r15d
	cmpq	$-1, 8(%rsp)
	movl	$1, %ecx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	movl	$2, %edx
	movl	%r9d, 16(%rsp)
	movl	%r11d, 24(%rsp)
	sete	%r15b
	addq	8(%rbp), %r15
	call	__mpn_rshift
	movq	8(%rsp), %rax
	movq	0(%rbp), %r10
	movl	%r13d, %ecx
	movl	24(%rsp), %r11d
	movl	16(%rsp), %r9d
	movq	%rax, %rdi
	movl	%r10d, %esi
	shrq	%cl, %rdi
	andl	$1, %esi
	movq	%rdi, %r13
	andl	$1, %r13d
	btq	$49, %r15
	jnc	.L39
	movq	%rax, %r15
	movq	%r14, %rbx
	jmp	.L40
.L20:
	call	__GI_abort
.L39:
	movq	8(%rsp), %r15
	jmp	.L31
	.size	round_and_return, .-round_and_return
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"../stdlib/strtod_l.c"
.LC6:
	.string	"digcnt > 0"
.LC7:
	.string	"*nsize < MPNSIZE"
	.text
	.p2align 4,,15
	.type	str_to_mpn, @function
str_to_mpn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testl	%esi, %esi
	movq	$0, (%rcx)
	jle	.L110
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rcx, %r14
	movq	%r8, %r13
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%r15, %rdi
.L77:
	movslq	(%rdi), %rax
	leaq	4(%rdi), %r15
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L86
	movslq	4(%rdi), %rax
	leaq	8(%rdi), %r15
.L86:
	leaq	(%rbx,%rbx,4), %rcx
	addl	$1, %edx
	subl	$1, %ebp
	leaq	-48(%rax,%rcx,2), %rbx
	je	.L111
	cmpl	$19, %edx
	jne	.L78
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L79
	movq	%rbx, (%r12)
	movq	$1, (%r14)
	xorl	%ebx, %ebx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%r12, %rsi
	movabsq	$-8446744073709551616, %rcx
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%edx, %edx
	addq	(%r12), %rbx
	movq	(%r14), %rsi
	setc	%dl
	movq	%rbx, (%r12)
	testq	%rdx, %rdx
	jne	.L112
.L82:
	testq	%rax, %rax
	je	.L99
	movq	(%r14), %rdx
	cmpq	$861, %rdx
	jg	.L113
	movq	%rax, (%r12,%rdx,8)
	xorl	%ebx, %ebx
	addq	$1, (%r14)
	xorl	%edx, %edx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L111:
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	jle	.L88
	movl	$19, %eax
	subl	%edx, %eax
	cltq
	cmpq	%rax, %rcx
	jle	.L114
.L88:
	movq	_tens_in_limb@GOTPCREL(%rip), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rcx
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L90
.L115:
	movq	%rbx, (%r12)
	movq	$1, (%r14)
.L76:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L112:
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	movq	8(%r12,%rdx,8), %rbx
	leaq	1(%rbx), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L82
.L83:
	cmpq	%rdx, %rsi
	jne	.L84
	addq	$1, %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L114:
	movq	_tens_in_limb@GOTPCREL(%rip), %rax
	movslq	%edx, %rdx
	movq	$0, 0(%r13)
	imulq	(%rax,%rcx,8), %rbx
	addq	%rdx, %rcx
	movq	(%r14), %rdx
	movq	(%rax,%rcx,8), %rcx
	testq	%rdx, %rdx
	je	.L115
.L90:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%edx, %edx
	addq	(%r12), %rbx
	movq	(%r14), %rsi
	setc	%dl
	movq	%rbx, (%r12)
	testq	%rdx, %rdx
	je	.L94
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	movq	8(%r12,%rdx,8), %rbx
	leaq	1(%rbx), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L94
.L95:
	cmpq	%rsi, %rdx
	jne	.L96
	addq	$1, %rax
.L94:
	testq	%rax, %rax
	je	.L76
	movq	(%r14), %rdx
	cmpq	$861, %rdx
	jg	.L116
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movq	%rax, (%r12,%rdx,8)
	jmp	.L76
.L116:
	leaq	__PRETTY_FUNCTION__.12759(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$453, %edx
	call	__GI___assert_fail
.L113:
	leaq	__PRETTY_FUNCTION__.12759(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$397, %edx
	call	__GI___assert_fail
.L110:
	leaq	__PRETTY_FUNCTION__.12759(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$380, %edx
	call	__GI___assert_fail
	.size	str_to_mpn, .-str_to_mpn
	.section	.rodata.str1.1
.LC13:
	.string	"decimal != L'\\0'"
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC14:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC15:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"i"
	.string	""
	.string	""
	.string	"t"
	.string	""
	.string	""
	.string	"y"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC16:
	.string	"n"
	.string	""
	.string	""
	.string	"a"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"dig_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC18:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_EXP - MANT_DIG) / 4"
	.align 8
.LC19:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX / 4"
	.align 8
.LC20:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_EXP - 3) / 4"
	.align 8
.LC21:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_10_EXP - MANT_DIG)"
	.align 8
.LC22:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC23:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_10_EXP - 1)"
	.section	.rodata.str1.1
.LC24:
	.string	"dig_no >= int_no"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"lead_zero <= (base == 16 ? (uintmax_t) INTMAX_MAX / 4 : (uintmax_t) INTMAX_MAX)"
	.align 8
.LC26:
	.string	"lead_zero <= (base == 16 ? ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN) / 4 : ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN))"
	.section	.rodata.str1.1
.LC27:
	.string	"bits != 0"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"int_no <= (uintmax_t) (exponent < 0 ? (INTMAX_MAX - bits + 1) / 4 : (INTMAX_MAX - exponent - bits + 1) / 4)"
	.align 8
.LC29:
	.string	"dig_no > int_no && exponent <= 0 && exponent >= MIN_10_EXP - (DIG + 2)"
	.section	.rodata.str1.1
.LC30:
	.string	"numsize < RETURN_LIMB_SIZE"
.LC31:
	.string	"int_no > 0 && exponent == 0"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"int_no == 0 && *startp != L_('0')"
	.section	.rodata.str1.1
.LC33:
	.string	"need_frac_digits > 0"
.LC34:
	.string	"numsize == 1 && n < d"
.LC35:
	.string	"empty == 1"
.LC36:
	.string	"numsize == densize"
.LC37:
	.string	"cy != 0"
	.text
	.p2align 4,,15
	.globl	____wcstof128_l_internal
	.hidden	____wcstof128_l_internal
	.type	____wcstof128_l_internal, @function
____wcstof128_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$13928, %rsp
	testl	%edx, %edx
	movq	8(%rcx), %rax
	movq	%rdi, 32(%rsp)
	movq	%rsi, 24(%rsp)
	movq	%rcx, (%rsp)
	jne	.L590
.L118:
	movl	88(%rax), %eax
	testl	%eax, %eax
	movl	%eax, 16(%rsp)
	je	.L591
	movq	32(%rsp), %rax
	movq	$0, 104(%rsp)
	movq	(%rsp), %r15
	leaq	-4(%rax), %rbx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L378:
	movq	%rbp, %rbx
.L120:
	movl	4(%rbx), %r12d
	movq	%r15, %rsi
	leaq	4(%rbx), %rbp
	movl	%r12d, %edi
	call	__GI___iswspace_l
	testl	%eax, %eax
	jne	.L378
	cmpl	$45, %r12d
	movl	%eax, %r15d
	je	.L592
	cmpl	$43, %r12d
	movl	$0, 88(%rsp)
	je	.L593
.L122:
	cmpl	%r12d, 16(%rsp)
	je	.L594
.L123:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	ja	.L595
.L124:
	cmpl	$48, %r12d
	movl	$10, 8(%rsp)
	je	.L596
.L134:
	testl	%r13d, %r13d
	movq	%rbp, %r9
	setne	%al
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$4, %r9
	movl	(%r9), %r12d
.L135:
	cmpl	$48, %r12d
	je	.L136
	cmpl	%r13d, %r12d
	sete	%bl
	andb	%al, %bl
	jne	.L136
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r9, 40(%rsp)
	call	__GI___towlower_l
	leal	-48(%r12), %ecx
	movq	40(%rsp), %r9
	cmpl	$9, %ecx
	ja	.L597
.L137:
	testl	%r13d, %r13d
	movq	%r9, %r10
	sete	%dl
	xorl	%r11d, %r11d
	cmpl	$9, %ecx
	ja	.L146
.L149:
	addq	$1, %r11
.L147:
	addq	$4, %r10
	movl	(%r10), %r12d
	leal	-48(%r12), %ecx
	cmpl	$9, %ecx
	jbe	.L149
.L146:
	cmpl	$16, 8(%rsp)
	je	.L598
.L148:
	cmpl	%r13d, %r12d
	setne	%al
	orb	%dl, %al
	movb	%al, 56(%rsp)
	je	.L147
	testq	%r14, %r14
	je	.L152
	cmpq	%r10, %rbp
	jb	.L599
.L152:
	xorl	%r14d, %r14d
	testq	%r11, %r11
	movq	%r11, %rbp
	sete	%r14b
	negq	%r14
	cmpl	%r12d, 16(%rsp)
	je	.L600
.L161:
	testq	%rbp, %rbp
	js	.L364
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r11, 48(%rsp)
	movq	%r10, 40(%rsp)
	movq	%r9, 32(%rsp)
	call	__GI___towlower_l
	cmpl	$16, 8(%rsp)
	movq	32(%rsp), %r9
	movq	40(%rsp), %r10
	movq	48(%rsp), %r11
	jne	.L362
	cmpl	$112, %eax
	jne	.L362
	movl	4(%r10), %ecx
	cmpl	$45, %ecx
	je	.L601
.L170:
	cmpl	$43, %ecx
	je	.L602
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L603
.L390:
	movq	%r10, %r13
.L169:
	cmpq	%rbp, %r11
	jnb	.L200
	cmpl	$48, -4(%r10)
	jne	.L201
	.p2align 4,,10
	.p2align 3
.L202:
	subq	$4, %r10
	subq	$1, %rbp
	cmpl	$48, -4(%r10)
	je	.L202
	cmpq	%rbp, %r11
	ja	.L604
.L200:
	cmpq	%rbp, %r11
	jne	.L160
	testq	%rbp, %rbp
	je	.L160
	cmpq	$0, 104(%rsp)
	js	.L605
.L201:
	cmpq	$0, 24(%rsp)
	jne	.L367
.L210:
	testq	%r14, %r14
	je	.L211
	movl	16(%rsp), %eax
	cmpl	%eax, (%r9)
	je	.L212
	.p2align 4,,10
	.p2align 3
.L213:
	addq	$4, %r9
	cmpl	%eax, (%r9)
	jne	.L213
.L212:
	cmpl	$16, 8(%rsp)
	leaq	4(%r9,%r14,4), %r9
	je	.L606
	testq	%r14, %r14
	movq	%r14, %rcx
	js	.L369
	movq	104(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	cmpq	%r14, %rdx
	jb	.L217
.L218:
	subq	%rcx, %rax
	subq	%r14, %rbp
	movq	%rax, 104(%rsp)
.L211:
	cmpl	$16, 8(%rsp)
	je	.L607
	movq	104(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L608
	movq	%rbp, %rax
	subq	%r11, %rax
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
.L242:
	leaq	(%rax,%r11), %rbx
	subq	%rax, %rcx
	movl	$4933, %eax
	movq	%rcx, 104(%rsp)
	subq	%rbx, %rax
	cmpq	%rax, %rcx
	jg	.L583
	cmpq	$-4966, %rcx
	jl	.L609
	testq	%rbx, %rbx
	jne	.L247
	testq	%rbp, %rbp
	je	.L250
	leaq	4966(%rcx), %rax
	cmpq	$4966, %rax
	ja	.L250
	cmpl	$48, (%r9)
	je	.L277
	movl	$1, %eax
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movl	$16496, %esi
	shrq	%rdx
	leal	114(%rdx), %eax
	movl	%ecx, %edx
	cmpl	$16496, %eax
	cmovg	%esi, %eax
	addl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L610
	leaq	104(%rsp), %rax
	movl	$0, (%rsp)
	movq	$0, 40(%rsp)
	movq	%rax, 72(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 64(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 8(%rsp)
.L279:
	movslq	%ecx, %rax
	movq	%rbp, %rsi
	movl	%ebp, %ecx
	subl	%ebx, %ecx
	subq	%rbx, %rsi
	cmpq	%rsi, %rax
	movslq	%ecx, %rcx
	cmovg	%rcx, %rax
	addq	%rbx, %rax
	cmpq	%rax, %rbp
	jle	.L281
	movq	%rax, %rbp
	movl	$1, %r15d
.L281:
	movl	%ebp, %eax
	movl	%r15d, 48(%rsp)
	leaq	_fpioconst_pow10(%rip), %r12
	subl	%ebx, %eax
	xorl	%r14d, %r14d
	movl	$1, %ebx
	movl	%eax, %ebp
	movl	%eax, 32(%rsp)
	movq	8(%rsp), %rax
	subl	%edx, %ebp
	movq	%r9, 56(%rsp)
	movq	%rax, 24(%rsp)
	leaq	7024(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	%rax, %r15
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L612:
	leaq	0(,%r13,8), %rdx
	movq	%r15, %rdi
	movq	%r13, %r14
	call	__GI_memcpy@PLT
.L282:
	addl	%ebx, %ebx
	addq	$24, %r12
	testl	%ebp, %ebp
	je	.L611
.L284:
	testl	%ebp, %ebx
	je	.L282
	movq	8(%r12), %rax
	leaq	__tens(%rip), %rsi
	xorl	%ebx, %ebp
	testq	%r14, %r14
	leaq	-1(%rax), %r13
	movq	(%r12), %rax
	leaq	8(%rsi,%rax,8), %rsi
	je	.L612
	movq	%r13, %rdx
	movq	24(%rsp), %r13
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%r13, %rdi
	call	__mpn_mul
	movq	8(%r12), %rdx
	testq	%rax, %rax
	leaq	-1(%r14,%rdx), %r14
	jne	.L401
	movq	%r15, %rax
	subq	$1, %r14
	movq	%r13, %r15
	movq	%rax, 24(%rsp)
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L597:
	leal	-97(%rax), %edx
	cmpl	$5, %edx
	ja	.L412
	cmpl	$16, 8(%rsp)
	je	.L137
.L412:
	cmpl	%r12d, 16(%rsp)
	je	.L613
	cmpl	$16, 8(%rsp)
	je	.L614
	cmpl	$101, %eax
	je	.L137
.L140:
	movq	%r9, %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rbp, %rdi
	movq	%r9, (%rsp)
	call	__correctly_grouped_prefixwc
	cmpq	$0, 24(%rsp)
	movq	(%rsp), %r9
	je	.L144
	cmpq	%rax, %rbp
	je	.L615
.L145:
	movq	24(%rsp), %rdi
	movq	%rax, (%rdi)
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L593:
	movl	4(%rbp), %r12d
	cmpl	%r12d, 16(%rsp)
	leaq	8(%rbx), %rbp
	movl	%eax, 88(%rsp)
	jne	.L123
.L594:
	movl	4(%rbp), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L124
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L124
.L595:
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r12d, %edi
	call	__GI___towlower_l
	cmpl	$105, %eax
	je	.L616
	cmpl	$110, %eax
	je	.L617
.L153:
	cmpq	$0, 24(%rsp)
	jne	.L618
.L133:
	pxor	%xmm0, %xmm0
.L117:
	addq	$13928, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L600:
	movl	%r15d, 32(%rsp)
	movb	%bl, 48(%rsp)
	movq	%r14, %r15
	leaq	4(%r10), %r13
	movl	4(%r10), %r12d
	movq	%r9, 40(%rsp)
	movq	%r11, %r14
	movl	8(%rsp), %ebp
	movq	%r11, %rbx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L620:
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	call	__GI___towlower_l
	subl	$97, %eax
	cmpl	$5, %eax
	ja	.L619
.L164:
	cmpl	$48, %r12d
	je	.L163
	cmpq	$-1, %r15
	movq	%r14, %rax
	sete	%dl
	subq	%rbx, %rax
	testb	%dl, %dl
	cmovne	%rax, %r15
.L163:
	addq	$4, %r13
	movl	0(%r13), %r12d
	addq	$1, %r14
.L162:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L164
	cmpl	$16, %ebp
	je	.L620
	movq	%r14, %rbp
	movq	%rbx, %r11
	movq	%r15, %r14
	testq	%rbp, %rbp
	movq	40(%rsp), %r9
	movl	32(%rsp), %r15d
	movzbl	48(%rsp), %ebx
	js	.L364
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r11, 40(%rsp)
	movq	%r9, 32(%rsp)
	call	__GI___towlower_l
	movq	32(%rsp), %r9
	movq	40(%rsp), %r11
	movq	%r13, %r10
.L362:
	cmpl	$16, 8(%rsp)
	je	.L390
	cmpl	$101, %eax
	jne	.L390
	movl	4(%r10), %ecx
	cmpl	$45, %ecx
	jne	.L170
.L601:
	movl	8(%r10), %ecx
	leaq	8(%r10), %r13
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L390
	cmpl	$16, 8(%rsp)
	je	.L621
	movabsq	$9223372036854770763, %rax
	cmpq	%rax, %r11
	ja	.L622
	leaq	5044(%r11), %rsi
.L581:
	movq	%rsi, %rax
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	movq	%rsi, %rdi
	movl	$1, %esi
.L177:
	movq	104(%rsp), %rax
	movzbl	56(%rsp), %r8d
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L187:
	je	.L623
.L189:
	leaq	(%rax,%rax,4), %rax
	addq	$4, %r13
	movl	%r8d, %ebx
	leaq	(%rcx,%rax,2), %rax
	movl	0(%r13), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L624
.L191:
	cmpq	%rdx, %rax
	jle	.L187
.L190:
	testb	%bl, %bl
	jne	.L625
.L188:
	cmpq	$-1, %r14
	je	.L626
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%esi, %esi
	movl	$34, %fs:(%rax)
	je	.L195
	movl	88(%rsp), %r10d
	testl	%r10d, %r10d
	je	.L196
	movdqa	.LC0(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__multf3@PLT
	.p2align 4,,10
	.p2align 3
.L198:
	addq	$4, %r13
	movl	0(%r13), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L198
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L117
	movq	%r13, (%rax)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L592:
	movl	4(%rbp), %r12d
	movl	$1, 88(%rsp)
	leaq	8(%rbx), %rbp
	jmp	.L122
.L599:
	movl	%r13d, %edx
	movq	%r10, %rsi
	movq	%r14, %rcx
	movq	%rbp, %rdi
	movq	%r11, 64(%rsp)
	movq	%r9, 48(%rsp)
	movq	%r10, 40(%rsp)
	call	__correctly_grouped_prefixwc
	movq	40(%rsp), %r10
	movq	%rax, %r13
	movq	48(%rsp), %r9
	movq	64(%rsp), %r11
	cmpq	%rax, %r10
	je	.L152
	cmpq	%rax, %rbp
	je	.L153
	cmpq	%rax, %r9
	ja	.L563
	movq	%r9, %rax
	movl	$0, %ebp
	jnb	.L563
.L155:
	movl	(%rax), %esi
	leal	-48(%rsi), %edx
	cmpl	$10, %edx
	adcq	$0, %rbp
	addq	$4, %rax
	cmpq	%rax, %r13
	ja	.L155
	movq	%rbp, %r11
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L160:
	cmpq	$0, 24(%rsp)
	je	.L209
.L367:
	movq	24(%rsp), %rax
	movq	%r13, (%rax)
.L209:
	testq	%rbp, %rbp
	jne	.L210
.L144:
	movl	88(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L133
	movdqa	.LC10(%rip), %xmm0
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L603:
	cmpl	$16, 8(%rsp)
	leaq	4(%r10), %r13
	je	.L174
.L175:
	testq	%r11, %r11
	je	.L183
	testq	%r14, %r14
	jne	.L414
	testq	%r11, %r11
	js	.L414
	movl	$4933, %esi
	subq	%r11, %rsi
.L180:
	testq	%rsi, %rsi
	movl	$0, %eax
	cmovs	%rax, %rsi
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L613:
	cmpl	$16, 8(%rsp)
	jne	.L137
	cmpq	%r9, %rbp
	jne	.L137
	movl	4(%r9), %edi
	leal	-48(%rdi), %eax
	cmpl	$9, %eax
	jbe	.L137
	movq	(%rsp), %rsi
	movq	%r9, 40(%rsp)
	movl	%ecx, 48(%rsp)
	call	__GI___towlower_l
	subl	$97, %eax
	movq	40(%rsp), %r9
	cmpl	$5, %eax
	ja	.L140
	movl	48(%rsp), %ecx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L605:
	movl	8(%rsp), %edi
	xorl	%eax, %eax
	movq	%r13, 40(%rsp)
	movq	%r14, 48(%rsp)
	leaq	-4(%r10), %r12
	movq	%r9, 32(%rsp)
	movq	%r11, %r13
	cmpl	$16, %edi
	sete	%al
	leaq	1(%rax,%rax,2), %rbx
	movq	%rbx, %r14
	movl	%edi, %ebx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	subl	$48, %edi
	cmpl	$9, %edi
	seta	%al
.L206:
	testb	%al, %al
	jne	.L207
	cmpl	$48, (%r12)
	jne	.L573
	movq	104(%rsp), %rax
	subq	$1, %r13
	addq	%r14, %rax
	subq	$1, %rbp
	movq	%rax, 104(%rsp)
	je	.L573
	testq	%rax, %rax
	jns	.L573
.L207:
	subq	$4, %r12
.L204:
	cmpl	$16, %ebx
	movl	(%r12), %edi
	jne	.L205
	movq	(%rsp), %rsi
	call	__GI___iswxdigit_l
	testl	%eax, %eax
	sete	%al
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L607:
	movq	%r9, %rbx
	movq	%r11, %r13
	movq	(%rsp), %r12
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L394:
	movq	%rdx, %rbx
.L220:
	movl	(%rbx), %edi
	movq	%r12, %rsi
	call	__GI___iswxdigit_l
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	movq	%rdx, %r15
	je	.L394
	movl	(%rbx), %edi
	movq	%rbx, %r9
	movq	%r13, %r11
	cmpl	$48, %edi
	je	.L222
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L395:
	movq	%rdx, %r9
	addq	$4, %rdx
.L222:
	movl	(%rdx), %edi
	cmpl	$48, %edi
	je	.L395
	leaq	8(%r9), %r15
.L221:
	leal	-48(%rdi), %esi
	cmpl	$9, %esi
	ja	.L223
	movslq	%esi, %rsi
.L224:
	leaq	nbits.12849(%rip), %rax
	movl	(%rax,%rsi,4), %eax
	testl	%eax, %eax
	je	.L627
	movl	$49, %ecx
	movl	$48, %edx
	movslq	%eax, %rdi
	subl	%eax, %ecx
	subl	%eax, %edx
	salq	%cl, %rsi
	movl	%edx, %r14d
	movq	%rsi, 120(%rsp)
	movq	104(%rsp), %rsi
	testq	%rsi, %rsi
	js	.L628
	movabsq	$9223372036854775807, %rcx
	subq	%rsi, %rcx
	subq	%rdi, %rcx
	leaq	4(%rcx), %rdi
	addq	$1, %rcx
	cmovs	%rdi, %rcx
	sarq	$2, %rcx
.L227:
	cmpq	%r11, %rcx
	jb	.L629
	subl	$1, %eax
	movl	$1, %ebx
	movq	%r15, %r12
	cltq
	leaq	-4(%rax,%r11,4), %rax
	addq	%rax, %rsi
	movq	%rsi, 104(%rsp)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L631:
	leal	-3(%r14), %ecx
	subl	$4, %r14d
	salq	%cl, %rax
	orq	%rsi, %rax
	movq	%rax, 112(%rsp,%rdx,8)
.L234:
	movq	%r13, %rbp
	movq	%r15, %r12
.L229:
	movq	%rbp, %r13
	subq	$1, %r13
	je	.L630
	movq	(%rsp), %rsi
	movl	(%r12), %edi
	leaq	4(%r12), %r15
	call	__GI___iswxdigit_l
	testl	%eax, %eax
	jne	.L230
	leaq	8(%r12), %rax
	movq	%r15, %r12
	movq	%rax, %r15
.L230:
	movl	(%r12), %edi
	leal	-48(%rdi), %eax
	cmpl	$9, %eax
	ja	.L231
	cltq
.L232:
	movslq	%ebx, %rdx
	cmpl	$2, %r14d
	movq	112(%rsp,%rdx,8), %rsi
	jg	.L631
	movl	$3, %ecx
	movq	%rax, %rdi
	subl	%r14d, %ecx
	shrq	%cl, %rdi
	leal	61(%r14), %ecx
	orq	%rdi, %rsi
	salq	%cl, %rax
	testl	%ebx, %ebx
	movq	%rsi, 112(%rsp,%rdx,8)
	je	.L632
	movq	%rax, 112(%rsp)
	addl	$60, %r14d
	xorl	%ebx, %ebx
	jmp	.L234
.L617:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	jne	.L153
	cmpl	$40, 12(%rbp)
	leaq	12(%rbp), %rbx
	movdqa	.LC8(%rip), %xmm0
	je	.L633
.L130:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L132
	movq	%rbx, (%rax)
.L132:
	movl	88(%rsp), %ebp
	testl	%ebp, %ebp
	je	.L117
	pxor	.LC10(%rip), %xmm0
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L231:
	movq	(%rsp), %rsi
	call	__GI___towlower_l
	subl	$87, %eax
	jmp	.L232
.L618:
	movq	24(%rsp), %rax
	movq	32(%rsp), %rdi
	pxor	%xmm0, %xmm0
	movq	%rdi, (%rax)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L598:
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movb	%dl, 64(%rsp)
	movq	%r11, 56(%rsp)
	movq	%r10, 48(%rsp)
	movq	%r9, 40(%rsp)
	call	__GI___towlower_l
	subl	$97, %eax
	movq	40(%rsp), %r9
	movq	48(%rsp), %r10
	cmpl	$5, %eax
	movq	56(%rsp), %r11
	movzbl	64(%rsp), %edx
	jbe	.L149
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L596:
	movl	4(%rbp), %edi
	movq	(%rsp), %rsi
	call	__GI___towlower_l
	cmpl	$120, %eax
	jne	.L134
	movl	8(%rbp), %r12d
	xorl	%r14d, %r14d
	addq	$8, %rbp
	movl	$16, 8(%rsp)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L623:
	cmpq	%rdi, %rcx
	jle	.L189
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L602:
	movl	8(%r10), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L390
	cmpl	$16, 8(%rsp)
	leaq	8(%r10), %r13
	jne	.L175
.L174:
	testq	%r11, %r11
	je	.L634
	testq	%r14, %r14
	jne	.L413
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r11
	ja	.L413
	movl	$4096, %eax
	subq	%r11, %rax
	leaq	3(,%rax,4), %rsi
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L619:
	movq	%rbx, %r11
	movq	%r14, %rbp
	movq	40(%rsp), %r9
	movq	%r15, %r14
	movzbl	48(%rsp), %ebx
	movl	32(%rsp), %r15d
	movq	%r13, %r10
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L590:
	movq	80(%rax), %r14
	movzbl	(%r14), %esi
	leal	-1(%rsi), %edx
	cmpb	$125, %dl
	ja	.L376
	movl	96(%rax), %r13d
	movl	$0, %edx
	testl	%r13d, %r13d
	cmove	%rdx, %r14
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r13, %r11
	movq	32(%rsp), %r9
	movq	40(%rsp), %r13
	movq	48(%rsp), %r14
	jmp	.L160
.L624:
	movq	%rax, %rdx
	negq	%rdx
	testl	%esi, %esi
	cmovne	%rdx, %rax
	movq	%rax, 104(%rsp)
	jmp	.L169
.L630:
	cmpl	$1, %ebx
	jne	.L240
	movq	$0, 112(%rsp)
.L240:
	movl	88(%rsp), %edx
	movq	104(%rsp), %rsi
	leaq	112(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L117
.L616:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	jne	.L153
	movq	24(%rsp), %r15
	testq	%r15, %r15
	je	.L127
	leaq	12(%rbp), %rbx
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	addq	$32, %rbp
	movq	%rbx, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	cmove	%rbp, %rbx
	movq	%rbx, (%r15)
.L127:
	movl	88(%rsp), %r12d
	testl	%r12d, %r12d
	je	.L380
	movdqa	.LC12(%rip), %xmm0
	jmp	.L117
.L626:
	movl	88(%rsp), %r11d
	pxor	%xmm0, %xmm0
	testl	%r11d, %r11d
	je	.L198
	movdqa	.LC10(%rip), %xmm0
	jmp	.L198
.L608:
	movq	%r11, %rax
	negq	%rax
	cmpq	%rcx, %rax
	cmovl	%rcx, %rax
	jmp	.L242
.L195:
	movl	88(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L197
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__multf3@PLT
	jmp	.L198
.L614:
	cmpl	$112, %eax
	jne	.L140
	cmpq	%r9, %rbp
	jne	.L137
	jmp	.L140
.L376:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L118
.L183:
	cmpq	$-1, %r14
	je	.L392
	movabsq	$9223372036854770874, %rax
	cmpq	%rax, %r14
	ja	.L635
	leaq	4933(%r14), %rsi
.L580:
	movq	%rsi, %rax
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	movq	%rsi, %rdi
	xorl	%esi, %esi
	jmp	.L177
.L196:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L198
.L197:
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L198
.L634:
	cmpq	$-1, %r14
	je	.L391
	movabsq	$2305843009213689855, %rax
	cmpq	%rax, %r14
	ja	.L636
	leaq	16387(,%r14,4), %rsi
	jmp	.L580
	.p2align 4,,10
	.p2align 3
.L611:
	movq	%r15, %r10
	cmpq	8(%rsp), %r10
	movq	56(%rsp), %r9
	movl	48(%rsp), %r15d
	je	.L637
.L285:
	movq	72(%rsp), %r8
	movq	64(%rsp), %rcx
	movq	%r9, %rdi
	movq	8(%rsp), %rdx
	movl	32(%rsp), %esi
	call	str_to_mpn
	leaq	-1(%r14), %rax
	bsrq	7024(%rsp,%rax,8), %rbx
	movq	%rax, 32(%rsp)
	xorq	$63, %rbx
	testl	%ebx, %ebx
	jne	.L286
.L585:
	movq	96(%rsp), %rdx
.L287:
	movq	40(%rsp), %rax
	cmpq	$1, %r14
	movq	%rax, 104(%rsp)
	je	.L290
	cmpq	$2, %r14
	jne	.L638
	cmpq	$1, %rdx
	movq	7024(%rsp), %r11
	movq	7032(%rsp), %r13
	movq	128(%rsp), %r8
	jg	.L300
	cmpq	%r8, %r13
	jbe	.L402
	movl	(%rsp), %esi
	testl	%esi, %esi
	je	.L639
	cmpl	$49, (%rsp)
	jle	.L640
	movl	$113, %ebp
	subl	(%rsp), %ebp
	jne	.L641
	leaq	112(%rsp), %r14
	movq	%r8, %rbx
.L305:
	addl	$64, (%rsp)
	movl	(%rsp), %eax
	cmpl	$113, %eax
	jle	.L404
	xorl	%r12d, %r12d
	xorl	%r8d, %r8d
.L306:
	movq	104(%rsp), %rax
	orq	%r8, %rbx
	movl	88(%rsp), %edx
	setne	%r9b
	movl	$63, %r8d
	movq	%r12, %rcx
	orl	%r15d, %r9d
	subl	%ebp, %r8d
	movq	%r14, %rdi
	leaq	-1(%rax), %rsi
	andl	$1, %r9d
	movslq	%r8d, %r8
	call	round_and_return
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%r15, %rax
	movq	24(%rsp), %r15
	movq	%rax, 24(%rsp)
	jmp	.L282
.L286:
	movq	16(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	8(%rsp), %rdi
	movq	96(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L585
	movq	96(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, 128(%rsp,%rcx,8)
	movq	%rdx, 96(%rsp)
	jmp	.L287
.L247:
	leaq	128(%rsp), %rax
	leaq	104(%rsp), %r8
	leaq	96(%rsp), %rcx
	movq	%r9, %rdi
	movl	%ebx, %esi
	movq	%rax, %rdx
	movq	%r8, 72(%rsp)
	movq	%rcx, 64(%rsp)
	movq	%rax, 8(%rsp)
	call	str_to_mpn
	movq	104(%rsp), %rdx
	movq	%rax, %r9
	movq	96(%rsp), %r12
	testq	%rdx, %rdx
	jle	.L252
	leaq	7024(%rsp), %rax
	leaq	_fpioconst_pow10(%rip), %r14
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	%rbx, %r12
	movq	%rax, 16(%rsp)
	movq	%rax, %r10
	movq	8(%rsp), %rax
	movq	%rax, (%rsp)
.L253:
	movslq	%r13d, %rax
	testq	%rdx, %rax
	je	.L254
.L643:
	movq	8(%r14), %rsi
	xorq	%rdx, %rax
	movq	%r9, 32(%rsp)
	movq	%rax, 104(%rsp)
	movq	(%r14), %rax
	leaq	-1(%rsi), %rbx
	leaq	__tens(%rip), %rsi
	cmpq	%rdi, %rbx
	leaq	8(%rsi,%rax,8), %rsi
	jg	.L255
	movq	%rsi, %rcx
	movq	(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%rbx, %r8
	movq	%r10, %rdi
	movq	%r10, 24(%rsp)
	call	__mpn_mul
	movq	24(%rsp), %r10
	movq	32(%rsp), %r9
.L256:
	movq	96(%rsp), %rdi
	movq	104(%rsp), %rdx
	addq	%rbx, %rdi
	testq	%rax, %rax
	movq	%rdi, 96(%rsp)
	jne	.L257
	subq	$1, %rdi
	movq	%rdi, 96(%rsp)
.L257:
	addl	%r13d, %r13d
	addq	$24, %r14
	testq	%rdx, %rdx
	je	.L642
	movq	(%rsp), %rax
	movq	%r10, (%rsp)
	movq	%rax, %r10
	movslq	%r13d, %rax
	testq	%rdx, %rax
	jne	.L643
.L254:
	addl	%r13d, %r13d
	addq	$24, %r14
	jmp	.L253
.L255:
	movq	(%rsp), %rcx
	movq	%rdi, %r8
	movq	%rbx, %rdx
	movq	%r10, %rdi
	movq	%r10, 24(%rsp)
	call	__mpn_mul
	movq	32(%rsp), %r9
	movq	24(%rsp), %r10
	jmp	.L256
.L642:
	cmpq	16(%rsp), %r10
	movq	%r12, %rbx
	movq	%rdi, %r12
	jne	.L252
	leaq	0(,%rdi,8), %rdx
	movq	8(%rsp), %rdi
	movq	%r10, %rsi
	movq	%r9, (%rsp)
	call	__GI_memcpy@PLT
	movq	(%rsp), %r9
.L252:
	leaq	-1(%r12), %rdx
	movl	%r12d, %r13d
	sall	$6, %r13d
	bsrq	128(%rsp,%rdx,8), %rax
	xorq	$63, %rax
	subl	%eax, %r13d
	cmpl	$16384, %r13d
	movl	%r13d, (%rsp)
	jg	.L583
	cmpl	$113, %r13d
	jle	.L261
	leal	-113(%r13), %eax
	movl	%eax, %r12d
	sarl	$6, %r12d
	andl	$63, %eax
	movslq	%r12d, %rdi
	movq	%rdi, %r9
	jne	.L262
	movdqu	128(%rsp,%r9,8), %xmm0
	subq	$1, %rdi
	movl	$63, %r8d
	movq	128(%rsp,%rdi,8), %r10
	movaps	%xmm0, 112(%rsp)
.L263:
	cmpq	$0, 128(%rsp)
	jne	.L265
	movq	8(%rsp), %rdx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L266:
	movl	%eax, %r15d
	addq	$1, %rax
	cmpq	$0, -8(%rdx,%rax,8)
	je	.L266
.L265:
	cmpq	%rbx, %rbp
	movl	$1, %r9d
	ja	.L267
	movslq	%r15d, %rax
	xorl	%r9d, %r9d
	cmpq	%rdi, %rax
	setl	%r9b
.L267:
	leal	-1(%r13), %esi
	movl	88(%rsp), %edx
	leaq	112(%rsp), %rdi
	movq	%r10, %rcx
	movslq	%esi, %rsi
	call	round_and_return
	jmp	.L117
.L606:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L369
	movq	104(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	cmpq	%r14, %rdx
	jb	.L217
	leaq	0(,%r14,4), %rcx
	jmp	.L218
.L621:
	movabsq	$2305843009213689828, %rax
	cmpq	%rax, %r11
	ja	.L644
	leaq	16494(,%r11,4), %rsi
	jmp	.L581
.L638:
	movq	32(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	7024(%rsp,%rax,8), %r10
	leaq	-2(%r14), %rax
	movq	%rax, 72(%rsp)
	movq	7024(%rsp,%rax,8), %r13
	movq	%r14, %rax
	subq	%rdx, %rax
	movq	%r10, 24(%rsp)
	leaq	(%rdi,%rax,8), %rsi
	movq	8(%rsp), %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	movq	24(%rsp), %r10
	js	.L645
	movq	96(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	$0, 128(%rsp,%rdx,8)
	movq	%rax, 96(%rsp)
.L325:
	cmpq	%rax, %r14
	jle	.L326
	movq	%r14, %rbx
	movl	(%rsp), %ecx
	subq	%rax, %rbx
	movq	%rbx, %rdx
	salq	$6, %rdx
	testl	%ecx, %ecx
	je	.L646
	addq	40(%rsp), %rdx
	cmpq	$113, %rdx
	jg	.L329
	cmpq	$1, %rbx
	jne	.L330
	movq	112(%rsp), %rdx
	movq	$0, 112(%rsp)
	movq	%rdx, 120(%rsp)
.L331:
	movl	%ebx, %edx
	sall	$6, %edx
	addl	%edx, (%rsp)
.L328:
	testl	%eax, %eax
	movslq	%eax, %rcx
	jle	.L336
	leal	-1(%rax), %edi
	movq	8(%rsp), %rax
	addq	%rbx, %rcx
	movslq	%edi, %rdx
	movl	%edi, %edi
	leaq	0(,%rdx,8), %rsi
	salq	$3, %rdi
	subq	%rdx, %rcx
	addq	%rsi, %rax
	leaq	120(%rsp,%rsi), %rsi
	subq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L335:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L335
.L336:
	movq	8(%rsp), %rax
	movq	%rax, %rdx
	addq	$8, %rax
	leaq	(%rax,%rbx,8), %rcx
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L647:
	addq	$8, %rax
.L334:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L647
	cmpl	$113, (%rsp)
	movq	$0, 7024(%rsp,%r14,8)
	movq	128(%rsp,%r14,8), %rbx
	jg	.L408
	leaq	120(%rsp), %rsi
.L340:
	leaq	1(%r14), %rax
	movq	8(%rsp), %rdi
	movl	%r15d, 92(%rsp)
	movl	%ebp, 64(%rsp)
	movq	%r14, %r15
	movq	%rax, 40(%rsp)
	leal	-2(%r14), %eax
	movslq	%eax, %rdx
	movl	%eax, %eax
	leaq	0(,%rdx,8), %rcx
	salq	$3, %rax
	addq	%rcx, %rdi
	addq	%rsi, %rcx
	subq	%rax, %rcx
	leal	-1(%r14), %eax
	movq	%rdi, 48(%rsp)
	movq	%r13, %r14
	movq	%rcx, %rbp
	movq	%r10, %r13
	movslq	%eax, %r8
	movl	%eax, 56(%rsp)
	leaq	112(%rsp), %rax
	subq	%rdx, %r8
	movq	%rax, 80(%rsp)
	.p2align 4,,10
	.p2align 3
.L344:
	cmpq	%rbx, %r13
	movq	$-1, %r12
	je	.L345
	movq	32(%rsp), %rax
	movq	%rbx, %rdx
	movq	72(%rsp), %rsi
	movq	128(%rsp,%rax,8), %rax
	movq	%rax, 24(%rsp)
#APP
# 1727 "../stdlib/strtod_l.c" 1
	divq %r13
# 0 "" 2
#NO_APP
	movq	%rax, %r12
	movq	%rdx, %rbx
	movq	%r14, %rax
#APP
# 1728 "../stdlib/strtod_l.c" 1
	mulq %r12
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L648:
	xorl	%edx, %edx
	cmpq	%r14, %rax
	setb	%dl
	subq	%r14, %rax
	subq	%rdx, %rcx
.L346:
	cmpq	%rbx, %rcx
	ja	.L349
	jne	.L345
	cmpq	%rax, 128(%rsp,%rsi,8)
	jnb	.L345
.L349:
	subq	$1, %r12
	addq	%r13, %rbx
	jnc	.L648
.L345:
	movq	8(%rsp), %rbx
	movq	40(%rsp), %rdx
	movq	%r12, %rcx
	movq	16(%rsp), %rsi
	movq	%r8, 24(%rsp)
	movq	%rbx, %rdi
	call	__mpn_submul_1
	cmpq	%rax, 128(%rsp,%r15,8)
	movq	24(%rsp), %r8
	je	.L350
	movq	16(%rsp), %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	__mpn_add_n
	testq	%rax, %rax
	movq	24(%rsp), %r8
	je	.L649
	subq	$1, %r12
.L350:
	movq	32(%rsp), %rax
	movl	56(%rsp), %edx
	movq	128(%rsp,%rax,8), %rbx
	testl	%edx, %edx
	movq	48(%rsp), %rax
	movq	%rbx, 128(%rsp,%r15,8)
	jle	.L355
	.p2align 4,,10
	.p2align 3
.L352:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%r8,8)
	subq	$8, %rax
	cmpq	%rax, %rbp
	jne	.L352
.L355:
	movl	(%rsp), %eax
	movq	$0, 128(%rsp)
	testl	%eax, %eax
	jne	.L650
	testq	%r12, %r12
	jne	.L356
	subq	$64, 104(%rsp)
	movq	$0, 120(%rsp)
	movq	$0, 112(%rsp)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L650:
	movl	(%rsp), %eax
	cmpl	$49, %eax
	leal	64(%rax), %r10d
	jle	.L651
	movl	$113, %eax
	subl	(%rsp), %eax
	movl	%eax, 64(%rsp)
	je	.L359
	movq	80(%rsp), %rdi
	movl	%eax, %ecx
	movl	$2, %edx
	movq	%r8, 24(%rsp)
	movl	%r10d, (%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	$64, %ecx
	subl	64(%rsp), %ecx
	movq	%r12, %rax
	movq	24(%rsp), %r8
	movl	(%rsp), %r10d
	shrq	%cl, %rax
	orq	%rax, 112(%rsp)
.L359:
	cmpl	$113, %r10d
	jg	.L652
	movl	%r10d, (%rsp)
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L356:
	bsrq	%r12, %rax
	movl	$64, %edx
	movq	$0, 120(%rsp)
	xorq	$63, %rax
	movq	%r12, 112(%rsp)
	subl	%eax, %edx
	cltq
	subq	%rax, 104(%rsp)
	movl	%edx, (%rsp)
	jmp	.L344
.L651:
	movq	112(%rsp), %rax
	movq	%r12, 112(%rsp)
	movq	%rax, 120(%rsp)
	jmp	.L359
.L652:
	movq	%r15, %r14
	movl	64(%rsp), %ebp
	movl	92(%rsp), %r15d
.L343:
	testl	%r14d, %r14d
	movl	%r14d, %edx
	js	.L360
	movslq	%r14d, %rax
	cmpq	$0, 128(%rsp,%rax,8)
	jne	.L360
	leal	-1(%r14), %eax
	movl	%r14d, %r14d
	movq	8(%rsp), %rsi
	cltq
	movq	%rax, %rcx
	subq	%r14, %rcx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L653:
	subq	$1, %rax
	cmpq	$0, 8(%rsi,%rax,8)
	jne	.L360
.L361:
	cmpq	%rax, %rcx
	movl	%eax, %edx
	jne	.L653
.L360:
	movl	%r15d, %r9d
	movq	104(%rsp), %rax
	notl	%edx
	shrl	$31, %edx
	andl	$1, %r9d
	movl	$63, %r8d
	orl	%edx, %r9d
	movl	88(%rsp), %edx
	subl	%ebp, %r8d
	leaq	-1(%rax), %rsi
	leaq	112(%rsp), %rdi
	andl	$1, %r9d
	movslq	%r8d, %r8
	movq	%r12, %rcx
	call	round_and_return
	jmp	.L117
.L290:
	cmpq	$1, %rdx
	movq	128(%rsp), %r13
	movq	7024(%rsp), %r10
	jne	.L415
	cmpq	%r10, %r13
	jnb	.L415
	leaq	112(%rsp), %r14
	movl	(%rsp), %ecx
	movl	%r15d, 16(%rsp)
	xorl	%r9d, %r9d
	movq	%r14, %r15
	movq	%r10, %r14
.L292:
	movq	%r13, %rdx
	movq	%r9, %rax
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %r14
# 0 "" 2
#NO_APP
	testl	%ecx, %ecx
	movq	%rdx, %r12
	movq	%rax, %rbx
	movq	%rdx, %r13
	je	.L654
	cmpl	$49, %ecx
	leal	64(%rcx), %r10d
	jle	.L655
	movl	$113, %ebp
	subl	%ecx, %ebp
	je	.L299
	movl	%ebp, %ecx
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r15, %rdi
	movq	%r9, 8(%rsp)
	movl	%r10d, (%rsp)
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%rbx, %rax
	movq	8(%rsp), %r9
	subl	%ebp, %ecx
	movl	(%rsp), %r10d
	shrq	%cl, %rax
	orq	%rax, 112(%rsp)
.L299:
	cmpl	$113, %r10d
	jg	.L656
.L297:
	movl	%r10d, %ecx
	jmp	.L292
.L654:
	testq	%rax, %rax
	jne	.L295
.L657:
	movq	%r13, %rdx
	subq	$64, 104(%rsp)
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %r14
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	movq	%rdx, %r13
	je	.L657
.L295:
	bsrq	%rax, %rdx
	movl	$64, %r10d
	movq	%r9, 120(%rsp)
	xorq	$63, %rdx
	movq	%rax, 112(%rsp)
	subl	%edx, %r10d
	movslq	%edx, %rdx
	subq	%rdx, 104(%rsp)
	jmp	.L297
.L656:
	movq	%r15, %r14
	movq	104(%rsp), %rax
	movl	16(%rsp), %r15d
	testq	%r12, %r12
	movl	$63, %r8d
	movl	88(%rsp), %edx
	setne	%r9b
	subl	%ebp, %r8d
	movq	%rbx, %rcx
	orl	%r15d, %r9d
	leaq	-1(%rax), %rsi
	movslq	%r8d, %r8
	andl	$1, %r9d
	movq	%r14, %rdi
	call	round_and_return
	jmp	.L117
.L628:
	movabsq	$-9223372036854775808, %rcx
	subq	%rdi, %rcx
	sarq	$2, %rcx
	jmp	.L227
.L223:
	movq	(%rsp), %rsi
	movq	%r11, 8(%rsp)
	call	__GI___towlower_l
	movq	8(%rsp), %r11
	leal	-87(%rax), %esi
	jmp	.L224
.L583:
	movl	88(%rsp), %r8d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r8d, %r8d
	movl	$34, %fs:(%rax)
	je	.L244
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__multf3@PLT
	jmp	.L117
.L261:
	cmpq	%rbx, %rbp
	jne	.L268
	subl	$1, %r13d
	movl	%r13d, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	0(%r13,%rdx), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$48, %eax
	je	.L658
	cmpl	$47, %eax
	jle	.L659
	cmpq	$1, %r12
	jg	.L660
	leal	-48(%rax), %ecx
	leaq	112(%rsp), %r14
	movl	$2, %eax
	subq	%r12, %rax
	movq	8(%rsp), %rsi
	movq	%r12, %rdx
	leaq	(%r14,%rax,8), %rdi
	call	__mpn_rshift
	movl	$1, %edx
	subq	96(%rsp), %rdx
	testq	%rdx, %rdx
	movq	%rax, 112(%rsp,%rdx,8)
	jle	.L271
	cmpq	$1, %rdx
	movq	$0, 112(%rsp)
	je	.L271
.L584:
	movq	$0, 120(%rsp)
.L271:
	movl	88(%rsp), %edx
	movslq	%r13d, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	call	round_and_return
	jmp	.L117
.L655:
	movq	112(%rsp), %rax
	movq	%rbx, 112(%rsp)
	movq	%rax, 120(%rsp)
	jmp	.L299
.L637:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	leaq	0(,%r14,8), %rdx
	movq	%r9, 24(%rsp)
	call	__GI_memcpy@PLT
	movq	24(%rsp), %r9
	jmp	.L285
.L326:
	jne	.L661
	testl	%r14d, %r14d
	jle	.L662
	leal	-1(%r14), %edx
	movq	8(%rsp), %rax
	leaq	120(%rsp), %rsi
	movslq	%edx, %rcx
	movl	%edx, %edx
	salq	$3, %rcx
	salq	$3, %rdx
	addq	%rcx, %rax
	addq	%rsi, %rcx
	subq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L341:
	movq	(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rax, %rcx
	jne	.L341
.L342:
	movq	$0, 128(%rsp)
	movq	$0, 7024(%rsp,%r14,8)
	movq	128(%rsp,%r14,8), %rbx
	jmp	.L340
.L380:
	movdqa	.LC11(%rip), %xmm0
	jmp	.L117
.L300:
	movq	136(%rsp), %rbx
	leaq	112(%rsp), %r14
.L303:
	movl	(%rsp), %ecx
	movl	$113, %r10d
.L307:
	cmpq	%r13, %rbx
	jne	.L308
.L663:
	addq	%r8, %rbx
	jnc	.L311
	subq	%r11, %rbx
	xorl	%r8d, %r8d
	movq	$-1, %r12
#APP
# 1607 "../stdlib/strtod_l.c" 1
	addq %r11,%r8
	adcq $0,%rbx
# 0 "" 2
#NO_APP
	testl	%ecx, %ecx
	jne	.L313
	movl	$64, %ecx
	xorl	%eax, %eax
	movq	$-1, %r12
.L320:
	subq	%rax, 104(%rsp)
	cmpq	%r13, %rbx
	movq	$0, 120(%rsp)
	movq	%r12, 112(%rsp)
	je	.L663
.L308:
	movq	%rbx, %rdx
	movq	%r8, %rax
#APP
# 1615 "../stdlib/strtod_l.c" 1
	divq %r13
# 0 "" 2
#NO_APP
	movq	%rax, %r12
	movq	%rdx, %rbx
	movq	%r11, %rax
#APP
# 1616 "../stdlib/strtod_l.c" 1
	mulq %r12
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L319:
	cmpq	%rbx, %rdx
	ja	.L315
	jne	.L316
	testq	%rax, %rax
	je	.L316
.L315:
	subq	$1, %r12
#APP
# 1625 "../stdlib/strtod_l.c" 1
	subq %r11,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%r13, %rbx
	jnc	.L319
.L316:
	xorl	%r8d, %r8d
#APP
# 1630 "../stdlib/strtod_l.c" 1
	subq %rax,%r8
	sbbq %rdx,%rbx
# 0 "" 2
#NO_APP
	testl	%ecx, %ecx
	jne	.L313
	testq	%r12, %r12
	movl	$64, %eax
	je	.L320
	bsrq	%r12, %rdx
	movl	$64, %ecx
	xorq	$63, %rdx
	movslq	%edx, %rax
	subl	%edx, %ecx
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L313:
	cmpl	$49, %ecx
	leal	64(%rcx), %r9d
	jle	.L664
	movl	%r10d, %ebp
	subl	%ecx, %ebp
	je	.L323
	movl	%ebp, %ecx
	movl	$2, %edx
	movq	%r14, %rsi
	movq	%r14, %rdi
	movq	%r11, 24(%rsp)
	movq	%r8, 16(%rsp)
	movl	%r10d, 8(%rsp)
	movl	%r9d, (%rsp)
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%r12, %rax
	movq	24(%rsp), %r11
	subl	%ebp, %ecx
	movq	16(%rsp), %r8
	movl	8(%rsp), %r10d
	shrq	%cl, %rax
	orq	%rax, 112(%rsp)
	movl	(%rsp), %r9d
.L323:
	cmpl	$113, %r9d
	jg	.L306
	movl	%r9d, %ecx
	jmp	.L307
.L311:
	xorl	%eax, %eax
	testq	%r11, %r11
	movq	%r11, %rdx
	setne	%al
	movq	$-1, %r12
	subq	%rax, %rdx
	movq	%r11, %rax
	negq	%rax
	jmp	.L319
.L664:
	movq	112(%rsp), %rax
	movq	%r12, 112(%rsp)
	movq	%rax, 120(%rsp)
	jmp	.L323
.L632:
	cmpq	$2, %rbp
	movq	%rax, %rdi
	movq	%r15, %r12
	je	.L236
	cmpl	$48, (%r15)
	jne	.L397
	subq	$3, %rbp
	xorl	%eax, %eax
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L238:
	addq	$1, %rax
	cmpl	$48, (%r12,%rax,4)
	jne	.L397
.L237:
	cmpq	%rax, %rbp
	jne	.L238
.L236:
	leaq	112(%rsp), %rax
	movl	88(%rsp), %edx
	movq	104(%rsp), %rsi
	movq	%rdi, %rcx
	movl	%ebx, %r9d
	movl	$63, %r8d
	movq	%rax, %rdi
	call	round_and_return
	jmp	.L117
.L609:
	movl	88(%rsp), %edi
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%edi, %edi
	movl	$34, %fs:(%rax)
	je	.L246
	movdqa	.LC0(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__multf3@PLT
	jmp	.L117
.L392:
	xorl	%esi, %esi
	movl	$3, %edi
	movl	$493, %edx
	jmp	.L177
.L645:
	movq	96(%rsp), %rax
	jmp	.L325
.L615:
	cmpl	$16, 8(%rsp)
	leaq	-4(%r9), %rax
	cmovne	32(%rsp), %rax
	jmp	.L145
.L244:
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L117
.L268:
	leaq	112(%rsp), %r14
	movq	8(%rsp), %rsi
	leaq	0(,%r12,8), %rdx
	movq	%r9, 16(%rsp)
	movq	%r14, %rdi
	call	__GI_memcpy@PLT
	cmpq	$1, %r12
	movq	16(%rsp), %r9
	jg	.L276
	movq	$0, 112(%rsp,%r12,8)
.L276:
	cmpq	%rbx, %rbp
	jbe	.L250
	movq	104(%rsp), %rax
	leaq	4966(%rax), %rdx
	cmpq	$4966, %rdx
	ja	.L250
	testl	%r13d, %r13d
	jle	.L277
	testq	%rax, %rax
	jne	.L665
	movl	$114, %ecx
	movslq	%r13d, %rax
	xorl	%edx, %edx
	subl	%r13d, %ecx
	movq	%rax, 40(%rsp)
	jmp	.L279
.L262:
	movq	128(%rsp,%rdi,8), %r10
	movslq	%eax, %r8
	subq	$1, %r8
	cmpq	%rdi, %rdx
	movq	%r10, %rsi
	jle	.L373
	leal	1(%r12), %r11d
	movl	%eax, %ecx
	movq	%r10, %rsi
	shrq	%cl, %rsi
	movl	$64, %r14d
	movslq	%r11d, %rcx
	movq	%rsi, (%rsp)
	subl	%eax, %r14d
	movq	128(%rsp,%rcx,8), %rsi
	movq	%rcx, 16(%rsp)
	movl	%r14d, %ecx
	movq	%rsi, %r11
	salq	%cl, %r11
	movq	(%rsp), %rcx
	orq	%r11, %rcx
	cmpq	16(%rsp), %rdx
	movq	%rcx, 112(%rsp)
	jle	.L264
	movl	%eax, %ecx
	shrq	%cl, %rsi
	leal	2(%r12), %ecx
	movq	%rsi, %r11
	movslq	%ecx, %rcx
	movq	128(%rsp,%rcx,8), %rsi
	movl	%r14d, %ecx
	movq	%rsi, %r12
	salq	%cl, %r12
	orq	%r12, %r11
	movq	%r11, 120(%rsp)
.L264:
	subq	%r9, %rdx
	cmpq	$1, %rdx
	jg	.L263
.L373:
	movl	%eax, %ecx
	shrq	%cl, %rsi
	movq	%rsi, 120(%rsp)
	jmp	.L263
.L402:
	xorl	%ebx, %ebx
	leaq	112(%rsp), %r14
	jmp	.L303
.L397:
	movl	$1, %ebx
	jmp	.L236
.L391:
	xorl	%esi, %esi
	movl	$7, %edi
	movl	$1638, %edx
	jmp	.L177
.L246:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L117
.L646:
	subq	%rdx, 104(%rsp)
	xorl	%ebp, %ebp
	jmp	.L328
.L563:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L144
	movq	%r13, (%rax)
	jmp	.L144
.L633:
	leaq	7024(%rsp), %rsi
	leaq	16(%rbp), %rdi
	movl	$41, %edx
	call	__GI___wcstof128_nan
	movq	7024(%rsp), %rax
	cmpl	$41, (%rax)
	leaq	4(%rax), %rdx
	cmove	%rdx, %rbx
	jmp	.L130
.L329:
	movl	$113, %ebp
	subl	(%rsp), %ebp
	cmpl	$63, %ebp
	jg	.L666
	testl	%ebp, %ebp
	je	.L331
	leaq	112(%rsp), %rdi
	movl	%ebp, %ecx
	movl	$2, %edx
	movq	%r10, 24(%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	96(%rsp), %rax
	movq	24(%rsp), %r10
	jmp	.L331
.L639:
	movq	40(%rsp), %rax
	movq	%r8, %rbx
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	leaq	112(%rsp), %r14
	subq	$64, %rax
	movq	%rax, 104(%rsp)
	jmp	.L303
.L659:
	movl	$2, %ebx
	movl	$48, %ecx
	leaq	112(%rsp), %r14
	subl	%eax, %ecx
	movq	%rbx, %rax
	movq	8(%rsp), %rsi
	subq	%r12, %rax
	movq	%r12, %rdx
	leaq	(%r14,%rax,8), %rdi
	call	__mpn_lshift
	subq	96(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.L271
.L589:
	cmpq	$1, %rbx
	movq	$0, 112(%rsp)
	jne	.L584
	jmp	.L271
.L658:
	movl	$2, %ebx
	leaq	112(%rsp), %r14
	movq	8(%rsp), %rsi
	subq	%r12, %rbx
	leaq	0(,%r12,8), %rdx
	leaq	(%r14,%rbx,8), %rdi
	call	__GI_memcpy@PLT
	testq	%rbx, %rbx
	jg	.L589
	jmp	.L271
.L641:
	leaq	112(%rsp), %r14
	movl	%ebp, %ecx
	movl	$2, %edx
	movq	%r11, 8(%rsp)
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	__mpn_lshift
	movq	128(%rsp), %rbx
	movq	8(%rsp), %r11
	jmp	.L305
.L408:
	xorl	%r12d, %r12d
	jmp	.L343
.L662:
	leaq	120(%rsp), %rsi
	jmp	.L342
.L277:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$1376, %edx
	call	__GI___assert_fail
.L591:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$593, %edx
	call	__GI___assert_fail
.L640:
	movq	112(%rsp), %rax
	movq	%r8, %rbx
	movq	$0, 112(%rsp)
	leaq	112(%rsp), %r14
	movq	%rax, 120(%rsp)
	jmp	.L305
.L666:
	leaq	112(%rsp), %rsi
	movl	%ebp, %ecx
	movl	$1, %edx
	andl	$63, %ecx
	movq	%r10, 24(%rsp)
	leaq	8(%rsi), %rdi
	call	__mpn_lshift
	movq	$0, 112(%rsp)
	movq	96(%rsp), %rax
	movq	24(%rsp), %r10
	jmp	.L331
.L250:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$1360, %edx
	call	__GI___assert_fail
.L364:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$875, %edx
	call	__GI___assert_fail
.L415:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$1497, %edx
	call	__GI___assert_fail
.L644:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$906, %edx
	call	__GI___assert_fail
.L604:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	movl	$1021, %edx
	call	__GI___assert_fail
.L636:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$926, %edx
	call	__GI___assert_fail
.L649:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC37(%rip), %rdi
	movl	$1750, %edx
	call	__GI___assert_fail
.L661:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$1708, %edx
	call	__GI___assert_fail
.L414:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$946, %edx
	call	__GI___assert_fail
.L413:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$914, %edx
	call	__GI___assert_fail
.L610:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	movl	$1397, %edx
	call	__GI___assert_fail
.L627:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$1100, %edx
	call	__GI___assert_fail
.L660:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$1316, %edx
	call	__GI___assert_fail
.L629:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$1121, %edx
	call	__GI___assert_fail
.L217:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$1076, %edx
	call	__GI___assert_fail
.L625:
	movq	%rax, 104(%rsp)
	jmp	.L188
.L369:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$1072, %edx
	call	__GI___assert_fail
.L665:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC31(%rip), %rdi
	movl	$1370, %edx
	call	__GI___assert_fail
.L330:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$1671, %edx
	call	__GI___assert_fail
.L404:
	xorl	%r8d, %r8d
	jmp	.L303
.L635:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$958, %edx
	call	__GI___assert_fail
.L622:
	leaq	__PRETTY_FUNCTION__.12803(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$938, %edx
	call	__GI___assert_fail
	.size	____wcstof128_l_internal, .-____wcstof128_l_internal
	.p2align 4,,15
	.weak	__GI___wcstof128_l
	.hidden	__GI___wcstof128_l
	.type	__GI___wcstof128_l, @function
__GI___wcstof128_l:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	jmp	____wcstof128_l_internal
	.size	__GI___wcstof128_l, .-__GI___wcstof128_l
	.globl	__wcstof128_l
	.set	__wcstof128_l,__GI___wcstof128_l
	.weak	wcstof128_l
	.set	wcstof128_l,__wcstof128_l
	.globl	__GI_wcstof128_l
	.set	__GI_wcstof128_l,__wcstof128_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12759, @object
	.size	__PRETTY_FUNCTION__.12759, 11
__PRETTY_FUNCTION__.12759:
	.string	"str_to_mpn"
	.section	.rodata
	.align 32
	.type	nbits.12849, @object
	.size	nbits.12849, 64
nbits.12849:
	.long	0
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12803, @object
	.size	__PRETTY_FUNCTION__.12803, 25
__PRETTY_FUNCTION__.12803:
	.string	"____wcstof128_l_internal"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147418112
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	-65537
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC12:
	.long	0
	.long	0
	.long	0
	.long	-65536
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
	.hidden	__mpn_cmp
	.hidden	__mpn_lshift
	.hidden	_nl_C_locobj
	.hidden	__correctly_grouped_prefixwc
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	__mpn_mul_1
	.hidden	__mpn_rshift
	.hidden	__mpn_construct_float128
