	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstold_internal
	.hidden	__GI___wcstold_internal
	.type	__GI___wcstold_internal, @function
__GI___wcstold_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstold_l_internal
	.size	__GI___wcstold_internal, .-__GI___wcstold_internal
	.globl	__wcstold_internal
	.set	__wcstold_internal,__GI___wcstold_internal
	.p2align 4,,15
	.weak	__GI_wcstold
	.hidden	__GI_wcstold
	.type	__GI_wcstold, @function
__GI_wcstold:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstold_l_internal
	.size	__GI_wcstold, .-__GI_wcstold
	.globl	wcstold
	.set	wcstold,__GI_wcstold
	.weak	wcstof64x
	.set	wcstof64x,wcstold
	.hidden	____wcstold_l_internal
