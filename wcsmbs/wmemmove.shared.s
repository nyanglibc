	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wmemmove
	.hidden	__wmemmove
	.type	__wmemmove, @function
__wmemmove:
	salq	$2, %rdx
	jmp	__GI_memmove
	.size	__wmemmove, .-__wmemmove
	.weak	wmemmove
	.set	wmemmove,__wmemmove
