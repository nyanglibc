	.text
	.p2align 4,,15
	.globl	__mbsrtowcs
	.hidden	__mbsrtowcs
	.type	__mbsrtowcs, @function
__mbsrtowcs:
	leaq	state(%rip), %rax
	testq	%rcx, %rcx
	cmove	%rax, %rcx
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__mbsrtowcs_l
	.size	__mbsrtowcs, .-__mbsrtowcs
	.weak	mbsrtowcs
	.set	mbsrtowcs,__mbsrtowcs
	.local	state
	.comm	state,8,8
	.hidden	__mbsrtowcs_l
