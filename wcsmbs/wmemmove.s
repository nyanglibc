	.text
	.p2align 4,,15
	.globl	__wmemmove
	.hidden	__wmemmove
	.type	__wmemmove, @function
__wmemmove:
	salq	$2, %rdx
	jmp	memmove
	.size	__wmemmove, .-__wmemmove
	.weak	wmemmove
	.set	wmemmove,__wmemmove
	.hidden	memmove
