	.text
	.p2align 4,,15
	.globl	__wcstof_nan
	.hidden	__wcstof_nan
	.type	__wcstof_nan, @function
__wcstof_nan:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$4, %rbx
.L2:
	movl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpl	$25, %eax
	jbe	.L3
	leal	-48(%rcx), %eax
	cmpl	$9, %eax
	jbe	.L3
	cmpl	$95, %ecx
	je	.L3
	cmpl	%edx, %ecx
	je	.L4
.L6:
	movss	.LC0(%rip), %xmm0
.L5:
	testq	%rbp, %rbp
	je	.L1
	movq	%rbx, 0(%rbp)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L4:
	leaq	24(%rsp), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	____wcstoull_l_internal
	cmpq	%rbx, 24(%rsp)
	jne	.L6
	andl	$4194303, %eax
	orl	$2143289344, %eax
	movl	%eax, 12(%rsp)
	movss	12(%rsp), %xmm0
	jmp	.L5
	.size	__wcstof_nan, .-__wcstof_nan
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.hidden	____wcstoull_l_internal
	.hidden	_nl_C_locobj
