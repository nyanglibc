	.text
	.p2align 4,,15
	.globl	__wcstod_internal
	.hidden	__wcstod_internal
	.type	__wcstod_internal, @function
__wcstod_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstod_l_internal
	.size	__wcstod_internal, .-__wcstod_internal
	.p2align 4,,15
	.weak	wcstod
	.hidden	wcstod
	.type	wcstod, @function
wcstod:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstod_l_internal
	.size	wcstod, .-wcstod
	.weak	wcstof32x
	.set	wcstof32x,wcstod
	.weak	wcstof64
	.set	wcstof64,wcstod
	.hidden	____wcstod_l_internal
