	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wcrtomb.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__wcrtomb
	.hidden	__wcrtomb
	.type	__wcrtomb, @function
__wcrtomb:
	pushq	%r12
	pushq	%rbp
	leaq	state(%rip), %rax
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$96, %rsp
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	testq	%rdi, %rdi
	movl	%esi, 12(%rsp)
	movq	$1, 64(%rsp)
	movl	$1, 72(%rsp)
	movq	%rdx, 80(%rsp)
	je	.L23
.L3:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%rbp, 48(%rsp)
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movl	168(%rbx), %eax
	movq	40(%rbx), %r12
	addq	%rbp, %rax
	testq	%r12, %r12
	movq	%rax, 56(%rsp)
	je	.L24
.L5:
	movq	16(%r12), %rax
	cmpq	$0, (%rax)
	movq	40(%rax), %rbx
	je	.L6
#APP
# 70 "wcrtomb.c" 1
	ror $2*8+1, %rbx
xor %fs:48, %rbx
# 0 "" 2
#NO_APP
.L6:
	movl	12(%rsp), %r8d
	testl	%r8d, %r8d
	jne	.L7
	movq	%rbx, %rdi
	call	__GI__dl_mcount_wrapper_check
	leaq	48(%rsp), %rsi
	movq	16(%r12), %rdi
	pushq	$1
	pushq	$1
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	32(%rsp), %r9
	call	*%rbx
	testl	$-5, %eax
	popq	%rsi
	popq	%rdi
	jne	.L8
	movq	48(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 48(%rsp)
	movb	$0, (%rax)
.L9:
	movq	48(%rsp), %rax
	addq	$96, %rsp
	popq	%rbx
	subq	%rbp, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	12(%rsp), %rax
	movq	%rbx, %rdi
	movq	%rax, 24(%rsp)
	call	__GI__dl_mcount_wrapper_check
	movq	24(%rsp), %rax
	leaq	24(%rsp), %rdx
	leaq	48(%rsp), %rsi
	movq	16(%r12), %rdi
	pushq	$1
	xorl	%r8d, %r8d
	pushq	$0
	leaq	4(%rax), %rcx
	leaq	32(%rsp), %r9
	call	*%rbx
	testl	$-5, %eax
	popq	%rdx
	popq	%rcx
	je	.L9
.L8:
	leal	-5(%rax), %edx
	cmpl	$2, %edx
	ja	.L25
	cmpl	$5, %eax
	je	.L9
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	addq	$96, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$0, 12(%rsp)
	leaq	32(%rsp), %rbp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L15
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %r12
	jmp	.L5
.L25:
	leaq	__PRETTY_FUNCTION__.9853(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$101, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %r12
	jmp	.L5
	.size	__wcrtomb, .-__wcrtomb
	.weak	__GI_wcrtomb
	.hidden	__GI_wcrtomb
	.set	__GI_wcrtomb,__wcrtomb
	.weak	wcrtomb
	.set	wcrtomb,__GI_wcrtomb
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9853, @object
	.size	__PRETTY_FUNCTION__.9853, 10
__PRETTY_FUNCTION__.9853:
	.string	"__wcrtomb"
	.local	state
	.comm	state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
