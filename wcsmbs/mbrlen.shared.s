	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___mbrlen
	.hidden	__GI___mbrlen
	.type	__GI___mbrlen, @function
__GI___mbrlen:
	leaq	internal(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	__GI___mbrtowc
	.size	__GI___mbrlen, .-__GI___mbrlen
	.weak	mbrlen
	.set	mbrlen,__GI___mbrlen
	.globl	__mbrlen
	.set	__mbrlen,__GI___mbrlen
	.local	internal
	.comm	internal,8,8
