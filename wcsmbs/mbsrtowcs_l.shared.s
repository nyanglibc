	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"mbsrtowcs_l.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"((wchar_t *) data.__outbuf)[-1] == L'\\0'"
	.section	.rodata.str1.1
.LC2:
	.string	"result > 0"
.LC3:
	.string	"__mbsinit (data.__statep)"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"status == __GCONV_OK || status == __GCONV_EMPTY_INPUT || status == __GCONV_ILLEGAL_INPUT || status == __GCONV_INCOMPLETE_INPUT || status == __GCONV_FULL_OUTPUT"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__mbsrtowcs_l
	.hidden	__mbsrtowcs_l
	.type	__mbsrtowcs_l, @function
__mbsrtowcs_l:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$376, %rsp
	movq	(%r8), %rbx
	movq	%rsi, 16(%rsp)
	movq	%rdi, 8(%rsp)
	movq	%rdx, %rsi
	movq	$1, 80(%rsp)
	movl	$1, 88(%rsp)
	movq	40(%rbx), %rax
	movq	%rcx, 96(%rsp)
	testq	%rax, %rax
	je	.L40
.L3:
	movq	(%rax), %r12
	cmpq	$0, (%r12)
	movq	40(%r12), %rbp
	je	.L4
#APP
# 64 "mbsrtowcs_l.c" 1
	ror $2*8+1, %rbp
xor %fs:48, %rbp
# 0 "" 2
#NO_APP
.L4:
	cmpq	$0, 8(%rsp)
	movq	16(%rsp), %rax
	movq	(%rax), %rbx
	je	.L41
	movq	8(%rsp), %rax
	testq	%rsi, %rsi
	movq	%rbx, 112(%rsp)
	movq	%rax, 64(%rsp)
	leaq	(%rax,%rsi,4), %rax
	movq	%rax, 72(%rsp)
	je	.L35
	leaq	112(%rsp), %r13
	leaq	40(%rsp), %r15
	leaq	64(%rsp), %r14
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L24:
	movq	112(%rsp), %rcx
	movq	64(%rsp), %rdx
	cmpq	%rbx, %rcx
	jne	.L23
	cmpb	$0, -1(%rbx)
	je	.L14
	movq	72(%rsp), %rsi
	subq	%rdx, %rsi
	sarq	$2, %rsi
	testq	%rsi, %rsi
	je	.L14
.L10:
	movq	%rbx, %rdi
	call	__GI___strnlen
	leaq	1(%rbx,%rax), %rbx
	movq	%rbp, %rdi
	call	__GI__dl_mcount_wrapper_check
	pushq	$1
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r12, %rdi
	movq	%r15, %r9
	movq	%rbx, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	*%rbp
	cmpl	$4, %eax
	popq	%rdi
	popq	%r8
	je	.L24
	cmpl	$7, %eax
	je	.L24
	movq	112(%rsp), %rbx
	movq	64(%rsp), %rdx
.L14:
	movq	%rdx, %rcx
	subq	8(%rsp), %rcx
	movq	16(%rsp), %rdi
	movq	%rbx, (%rdi)
	sarq	$2, %rcx
	testl	$-5, %eax
	movq	%rcx, %rbx
	jne	.L42
	movl	-4(%rdx), %esi
	testl	%esi, %esi
	jne	.L37
	testq	%rcx, %rcx
	je	.L43
	movq	96(%rsp), %rdx
	movl	(%rdx), %edx
	testl	%edx, %edx
	jne	.L44
	movq	16(%rsp), %rdi
	movq	$0, (%rdi)
.L38:
	subq	$1, %rbx
.L37:
	leal	-4(%rax), %edx
	cmpl	$3, %edx
	setbe	%dl
	testl	%eax, %eax
	sete	%cl
	orl	%ecx, %edx
	testb	%dl, %dl
	je	.L45
.L20:
	cmpl	$7, %eax
	jbe	.L46
.L21:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbx
	movl	$84, %fs:(%rax)
.L1:
	addq	$376, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leal	-4(%rax), %edx
	cmpl	$3, %edx
	setbe	%dl
	testb	%dl, %dl
	jne	.L20
.L45:
	leaq	__PRETTY_FUNCTION__.9859(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$153, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$177, %edx
	btq	%rax, %rdx
	jc	.L1
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rdi
	movq	%rbx, 48(%rsp)
	leaq	112(%rsp), %r13
	call	__GI_strlen
	leaq	1(%rbx,%rax), %rax
	leaq	40(%rsp), %r15
	leaq	64(%rsp), %r14
	xorl	%ebx, %ebx
	movq	%rax, 8(%rsp)
	movq	96(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 56(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	368(%rsp), %rax
	movq	%rax, 72(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbp, %rdi
	movq	%r13, 64(%rsp)
	call	__GI__dl_mcount_wrapper_check
	pushq	$1
	xorl	%r8d, %r8d
	pushq	$0
	movq	%r15, %r9
	movq	24(%rsp), %rcx
	movq	32(%rsp), %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	*%rbp
	movq	80(%rsp), %rcx
	popq	%r10
	popq	%r11
	movq	%rcx, %rdx
	subq	%r13, %rdx
	sarq	$2, %rdx
	addq	%rdx, %rbx
	cmpl	$5, %eax
	je	.L6
	testl	$-5, %eax
	jne	.L37
	movl	-4(%rcx), %r9d
	testl	%r9d, %r9d
	je	.L38
	leaq	__PRETTY_FUNCTION__.9859(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$94, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rcx, %rbx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L22
	movq	%rbx, %rdi
	movq	%rdx, 24(%rsp)
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	movq	24(%rsp), %rsi
	jmp	.L3
.L35:
	xorl	%ebx, %ebx
	jmp	.L1
.L22:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L3
.L44:
	leaq	__PRETTY_FUNCTION__.9859(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$142, %edx
	call	__GI___assert_fail
.L43:
	leaq	__PRETTY_FUNCTION__.9859(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$141, %edx
	call	__GI___assert_fail
	.size	__mbsrtowcs_l, .-__mbsrtowcs_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9859, @object
	.size	__PRETTY_FUNCTION__.9859, 14
__PRETTY_FUNCTION__.9859:
	.string	"__mbsrtowcs_l"
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
