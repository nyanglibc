	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstoul_internal
	.hidden	__GI___wcstoul_internal
	.type	__GI___wcstoul_internal, @function
__GI___wcstoul_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	____wcstoul_l_internal
	.size	__GI___wcstoul_internal, .-__GI___wcstoul_internal
	.globl	__wcstoul_internal
	.set	__wcstoul_internal,__GI___wcstoul_internal
	.globl	__GI___wcstoull_internal
	.set	__GI___wcstoull_internal,__wcstoul_internal
	.globl	__wcstoull_internal
	.set	__wcstoull_internal,__wcstoul_internal
	.p2align 4,,15
	.globl	__wcstoul
	.type	__wcstoul, @function
__wcstoul:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	____wcstoul_l_internal
	.size	__wcstoul, .-__wcstoul
	.weak	__GI_wcstoul
	.hidden	__GI_wcstoul
	.set	__GI_wcstoul,__wcstoul
	.weak	wcstoul
	.set	wcstoul,__GI_wcstoul
	.weak	wcstoumax
	.set	wcstoumax,wcstoul
	.weak	wcstouq
	.set	wcstouq,wcstoul
	.weak	wcstoull
	.set	wcstoull,wcstoul
	.hidden	____wcstoul_l_internal
