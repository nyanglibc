	.text
	.p2align 4,,15
	.globl	__wcstof128_internal
	.hidden	__wcstof128_internal
	.type	__wcstof128_internal, @function
__wcstof128_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstof128_l_internal
	.size	__wcstof128_internal, .-__wcstof128_internal
	.p2align 4,,15
	.weak	wcstof128
	.hidden	wcstof128
	.type	wcstof128, @function
wcstof128:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstof128_l_internal
	.size	wcstof128, .-wcstof128
	.hidden	____wcstof128_l_internal
