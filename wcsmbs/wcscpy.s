	.text
	.p2align 4,,15
	.globl	__wcscpy
	.hidden	__wcscpy
	.type	__wcscpy, @function
__wcscpy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__wcslen@PLT
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	leaq	1(%rax), %rdx
	jmp	__wmemcpy
	.size	__wcscpy, .-__wcscpy
	.weak	wcscpy
	.set	wcscpy,__wcscpy
	.hidden	__wmemcpy
