	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	round_away, @function
round_away:
	cmpl	$1024, %r8d
	je	.L3
	jle	.L18
	cmpl	$2048, %r8d
	je	.L6
	cmpl	$3072, %r8d
	jne	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r8d, %r8d
	jne	.L2
	orl	%esi, %ecx
	movl	$0, %eax
	testb	%dl, %dl
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmovne	%ecx, %eax
	ret
.L2:
	subq	$8, %rsp
	call	__GI_abort
	.size	round_away, .-round_away
	.p2align 4,,15
	.type	round_and_return, @function
round_and_return:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %rdx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 46(%rsp)
# 0 "" 2
#NO_APP
	movzwl	46(%rsp), %eax
	andw	$3072, %ax
	cmpw	$1024, %ax
	je	.L21
	jbe	.L61
	cmpw	$2048, %ax
	je	.L24
	cmpw	$3072, %ax
	jne	.L20
	movl	$3072, %r10d
.L23:
	cmpq	$-1022, %rbx
	jge	.L26
.L62:
	cmpq	$-1075, %rbx
	jge	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	movl	$34, %fs:(%rax)
	je	.L28
	movsd	.LC1(%rip), %xmm0
	mulsd	.LC0(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$-1022, %rbx
	movl	$2048, %r10d
	jl	.L62
.L26:
	cmpq	$1023, %rbx
	jle	.L63
.L38:
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	movl	$34, %fs:(%rax)
	je	.L44
	movsd	.LC3(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movsd	.LC2(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	mulsd	%xmm0, %xmm0
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	testw	%ax, %ax
	jne	.L20
	xorl	%r10d, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$-1, %rax
	movl	%r8d, %ecx
	movq	$-1022, %rsi
	salq	%cl, %rax
	subq	%rbx, %rsi
	movq	0(%rbp), %r11
	notq	%rax
	testq	%rdx, %rax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, %r9d
	movl	%r9d, %eax
	andl	$1, %eax
	cmpq	$53, %rsi
	movb	%al, 11(%rsp)
	je	.L64
	movq	$-1023, %r14
	movq	%r14, %rcx
	subq	%rbx, %rcx
	movq	%r11, %rbx
	shrq	%cl, %rbx
	movl	%ecx, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	cmpq	$1, %rsi
	je	.L32
	movl	%esi, %ecx
	movl	%r9d, 24(%rsp)
	movq	%r11, 16(%rsp)
	movl	%r10d, 12(%rsp)
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
.L59:
	call	__mpn_rshift
	movq	0(%rbp), %r12
	movl	12(%rsp), %r10d
	movq	16(%rsp), %r11
	movl	24(%rsp), %r9d
	movl	%r12d, %esi
	andl	$1, %esi
.L31:
	andl	$1, %ebx
	jne	.L36
	cmpb	$0, 11(%rsp)
	je	.L65
.L36:
	movq	__libc_errno@gottpoff(%rip), %rax
	movsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
.L60:
	movq	$-1023, %rbx
.L35:
	testl	%r9d, %r9d
	movl	$1, %r14d
	jne	.L39
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
.L37:
	testq	%rax, %rax
	setne	%r14b
	movzbl	%r14b, %r9d
.L39:
	movzbl	%r13b, %edx
	movl	%r10d, %r8d
	movl	%r9d, %ecx
	movl	%r15d, %edi
	call	round_away
	testb	%al, %al
	je	.L40
	addq	$1, %r12
	btq	$53, %r12
	movq	%r12, 0(%rbp)
	jc	.L66
	cmpq	$-1023, %rbx
	je	.L43
.L40:
	movl	%ebx, %esi
.L42:
	testb	%r14b, %r14b
	jne	.L51
	testb	%r13b, %r13b
	jne	.L51
.L45:
	movl	%r15d, %edx
	movq	%rbp, %rdi
	call	__mpn_construct_double
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movsd	.LC0(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	mulsd	%xmm0, %xmm0
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%r8d, %ecx
	movq	%r11, %rsi
	movl	%r10d, %r8d
	shrq	%cl, %rdx
	andl	$1, %esi
	movl	%r9d, %ecx
	andl	$1, %edx
	movl	%r15d, %edi
	movq	%r11, 24(%rsp)
	movl	%r10d, 16(%rsp)
	movl	%r9d, 12(%rsp)
	call	round_away
	movl	12(%rsp), %r9d
	movl	16(%rsp), %r10d
	testb	%al, %al
	movq	24(%rsp), %r11
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	movl	%r9d, 24(%rsp)
	movl	%r10d, 12(%rsp)
	movq	%r11, 16(%rsp)
	je	.L59
	call	__mpn_rshift
	movq	16(%rsp), %r11
	movq	0(%rbp), %r12
	movl	12(%rsp), %r10d
	movl	24(%rsp), %r9d
	leaq	1(%r11), %rax
	movl	%r12d, %esi
	andl	$1, %esi
	btq	$53, %rax
	jc	.L60
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L51:
	movsd	.LC0(%rip), %xmm0
	addsd	.LC4(%rip), %xmm0
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L63:
	movq	0(%rbp), %r12
	movq	%rdx, %r13
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movl	%r8d, 4(%rsp)
	movq	%rdx, %r11
	andl	$1, %r13d
	movl	%r12d, %esi
	andl	$1, %esi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$1, %rbx
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mpn_rshift
	movabsq	$4503599627370496, %rax
	orq	%rax, 0(%rbp)
	cmpq	$1024, %rbx
	je	.L38
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movabsq	$4503599627370496, %rax
	xorl	%esi, %esi
	testq	%rax, %r12
	setne	%sil
	subl	$1023, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
	jne	.L36
	movq	$-1023, %rbx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r11, %rbx
	movq	$0, 0(%rbp)
	xorl	%esi, %esi
	shrq	$52, %rbx
	xorl	%r12d, %r12d
	movl	$52, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	jmp	.L31
.L20:
	call	__GI_abort
	.size	round_and_return, .-round_and_return
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"../stdlib/strtod_l.c"
.LC6:
	.string	"digcnt > 0"
.LC7:
	.string	"*nsize < MPNSIZE"
	.text
	.p2align 4,,15
	.type	str_to_mpn, @function
str_to_mpn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testl	%esi, %esi
	movq	$0, (%rcx)
	jle	.L101
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rcx, %r14
	movq	%r8, %r13
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%r15, %rdi
.L68:
	movslq	(%rdi), %rax
	leaq	4(%rdi), %r15
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L77
	movslq	4(%rdi), %rax
	leaq	8(%rdi), %r15
.L77:
	leaq	(%rbx,%rbx,4), %rcx
	addl	$1, %edx
	subl	$1, %ebp
	leaq	-48(%rax,%rcx,2), %rbx
	je	.L102
	cmpl	$19, %edx
	jne	.L69
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L70
	movq	%rbx, (%r12)
	movq	$1, (%r14)
	xorl	%ebx, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r12, %rsi
	movabsq	$-8446744073709551616, %rcx
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%edx, %edx
	addq	(%r12), %rbx
	movq	(%r14), %rsi
	setc	%dl
	movq	%rbx, (%r12)
	testq	%rdx, %rdx
	jne	.L103
.L73:
	testq	%rax, %rax
	je	.L90
	movq	(%r14), %rdx
	cmpq	$58, %rdx
	jg	.L104
	movq	%rax, (%r12,%rdx,8)
	xorl	%ebx, %ebx
	addq	$1, (%r14)
	xorl	%edx, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L102:
	movq	0(%r13), %rcx
	testq	%rcx, %rcx
	jle	.L79
	movl	$19, %eax
	subl	%edx, %eax
	cltq
	cmpq	%rax, %rcx
	jle	.L105
.L79:
	movq	_tens_in_limb@GOTPCREL(%rip), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rcx
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	jne	.L81
.L106:
	movq	%rbx, (%r12)
	movq	$1, (%r14)
.L67:
	addq	$8, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%ebx, %ebx
	xorl	%edx, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L103:
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%r12,%rdx,8), %rbx
	leaq	1(%rbx), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L73
.L74:
	cmpq	%rdx, %rsi
	jne	.L75
	addq	$1, %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L105:
	movq	_tens_in_limb@GOTPCREL(%rip), %rax
	movslq	%edx, %rdx
	movq	$0, 0(%r13)
	imulq	(%rax,%rcx,8), %rbx
	addq	%rdx, %rcx
	movq	(%r14), %rdx
	movq	(%rax,%rcx,8), %rcx
	testq	%rdx, %rdx
	je	.L106
.L81:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%edx, %edx
	addq	(%r12), %rbx
	movq	(%r14), %rsi
	setc	%dl
	movq	%rbx, (%r12)
	testq	%rdx, %rdx
	je	.L85
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%r12,%rdx,8), %rbx
	leaq	1(%rbx), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L85
.L86:
	cmpq	%rsi, %rdx
	jne	.L87
	addq	$1, %rax
.L85:
	testq	%rax, %rax
	je	.L67
	movq	(%r14), %rdx
	cmpq	$58, %rdx
	jg	.L107
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r14)
	movq	%rax, (%r12,%rdx,8)
	jmp	.L67
.L107:
	leaq	__PRETTY_FUNCTION__.11984(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$453, %edx
	call	__GI___assert_fail
.L104:
	leaq	__PRETTY_FUNCTION__.11984(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$397, %edx
	call	__GI___assert_fail
.L101:
	leaq	__PRETTY_FUNCTION__.11984(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$380, %edx
	call	__GI___assert_fail
	.size	str_to_mpn, .-str_to_mpn
	.section	.rodata.str1.1
.LC13:
	.string	"decimal != L'\\0'"
	.section	.rodata.str4.4,"aMS",@progbits,4
	.align 4
.LC14:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC15:
	.string	"i"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	"i"
	.string	""
	.string	""
	.string	"t"
	.string	""
	.string	""
	.string	"y"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.align 4
.LC16:
	.string	"n"
	.string	""
	.string	""
	.string	"a"
	.string	""
	.string	""
	.string	"n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"dig_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC19:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_EXP - MANT_DIG) / 4"
	.align 8
.LC20:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX / 4"
	.align 8
.LC21:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_EXP - 3) / 4"
	.align 8
.LC22:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_10_EXP - MANT_DIG)"
	.align 8
.LC23:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC24:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_10_EXP - 1)"
	.section	.rodata.str1.1
.LC25:
	.string	"dig_no >= int_no"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"lead_zero <= (base == 16 ? (uintmax_t) INTMAX_MAX / 4 : (uintmax_t) INTMAX_MAX)"
	.align 8
.LC27:
	.string	"lead_zero <= (base == 16 ? ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN) / 4 : ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN))"
	.section	.rodata.str1.1
.LC28:
	.string	"bits != 0"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"int_no <= (uintmax_t) (exponent < 0 ? (INTMAX_MAX - bits + 1) / 4 : (INTMAX_MAX - exponent - bits + 1) / 4)"
	.align 8
.LC30:
	.string	"dig_no > int_no && exponent <= 0 && exponent >= MIN_10_EXP - (DIG + 2)"
	.section	.rodata.str1.1
.LC31:
	.string	"int_no > 0 && exponent == 0"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"int_no == 0 && *startp != L_('0')"
	.section	.rodata.str1.1
.LC33:
	.string	"need_frac_digits > 0"
.LC34:
	.string	"numsize == 1 && n < d"
.LC35:
	.string	"numsize == densize"
.LC36:
	.string	"cy != 0"
	.text
	.p2align 4,,15
	.globl	____wcstod_l_internal
	.hidden	____wcstod_l_internal
	.type	____wcstod_l_internal, @function
____wcstod_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$1096, %rsp
	testl	%edx, %edx
	movq	8(%rcx), %rax
	movq	%rdi, 32(%rsp)
	movq	%rsi, 24(%rsp)
	movq	%rcx, (%rsp)
	jne	.L565
.L109:
	movl	88(%rax), %eax
	testl	%eax, %eax
	movl	%eax, 16(%rsp)
	je	.L566
	movq	32(%rsp), %rax
	movq	$0, 112(%rsp)
	movq	(%rsp), %r15
	leaq	-4(%rax), %rbx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L363:
	movq	%rbp, %rbx
.L111:
	movl	4(%rbx), %r12d
	movq	%r15, %rsi
	leaq	4(%rbx), %rbp
	movl	%r12d, %edi
	call	__GI___iswspace_l
	testl	%eax, %eax
	jne	.L363
	cmpl	$45, %r12d
	movl	%eax, %r15d
	je	.L567
	cmpl	$43, %r12d
	movl	$0, 44(%rsp)
	je	.L568
.L113:
	cmpl	%r12d, 16(%rsp)
	je	.L569
.L114:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	ja	.L570
.L115:
	cmpl	$48, %r12d
	movl	$10, 8(%rsp)
	je	.L571
.L125:
	testl	%r13d, %r13d
	movq	%rbp, %r9
	setne	%al
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L127:
	addq	$4, %r9
	movl	(%r9), %r12d
.L126:
	cmpl	$48, %r12d
	je	.L127
	cmpl	%r13d, %r12d
	sete	%bl
	andb	%al, %bl
	jne	.L127
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r9, 48(%rsp)
	call	__GI___towlower_l
	leal	-48(%r12), %edx
	movq	48(%rsp), %r9
	cmpl	$9, %edx
	ja	.L572
.L128:
	testl	%r13d, %r13d
	movq	%r9, %r10
	sete	%cl
	xorl	%r11d, %r11d
	cmpl	$9, %edx
	ja	.L137
.L140:
	addq	$1, %r11
.L138:
	addq	$4, %r10
	movl	(%r10), %r12d
	leal	-48(%r12), %edx
	cmpl	$9, %edx
	jbe	.L140
.L137:
	cmpl	$16, 8(%rsp)
	je	.L573
.L139:
	cmpl	%r13d, %r12d
	setne	%al
	orb	%cl, %al
	movb	%al, 64(%rsp)
	je	.L138
	testq	%r14, %r14
	je	.L143
	cmpq	%r10, %rbp
	jb	.L574
.L143:
	xorl	%r14d, %r14d
	testq	%r11, %r11
	movq	%r11, %rbp
	sete	%r14b
	negq	%r14
	cmpl	%r12d, 16(%rsp)
	je	.L575
.L152:
	testq	%rbp, %rbp
	js	.L346
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r11, 56(%rsp)
	movq	%r10, 48(%rsp)
	movq	%r9, 32(%rsp)
	call	__GI___towlower_l
	cmpl	$16, 8(%rsp)
	movq	32(%rsp), %r9
	movq	48(%rsp), %r10
	movq	56(%rsp), %r11
	jne	.L344
	cmpl	$112, %eax
	jne	.L344
	movl	4(%r10), %ecx
	cmpl	$45, %ecx
	je	.L576
.L161:
	cmpl	$43, %ecx
	je	.L577
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L578
.L375:
	movq	%r10, %r13
.L160:
	cmpq	%rbp, %r11
	jnb	.L191
	cmpl	$48, -4(%r10)
	jne	.L192
	.p2align 4,,10
	.p2align 3
.L193:
	subq	$4, %r10
	subq	$1, %rbp
	cmpl	$48, -4(%r10)
	je	.L193
	cmpq	%rbp, %r11
	ja	.L579
.L191:
	cmpq	%rbp, %r11
	jne	.L151
	testq	%rbp, %rbp
	je	.L151
	cmpq	$0, 112(%rsp)
	js	.L580
.L192:
	cmpq	$0, 24(%rsp)
	je	.L201
.L349:
	movq	24(%rsp), %rax
	movq	%r13, (%rax)
.L200:
	testq	%rbp, %rbp
	jne	.L201
.L135:
	movl	44(%rsp), %r13d
	movsd	.LC10(%rip), %xmm0
	testl	%r13d, %r13d
	jne	.L108
.L124:
	pxor	%xmm0, %xmm0
.L108:
	addq	$1096, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L572:
	leal	-97(%rax), %ecx
	cmpl	$5, %ecx
	ja	.L391
	cmpl	$16, 8(%rsp)
	je	.L128
.L391:
	cmpl	%r12d, 16(%rsp)
	je	.L581
	cmpl	$16, 8(%rsp)
	je	.L582
	cmpl	$101, %eax
	je	.L128
.L131:
	movq	%r9, %rsi
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%rbp, %rdi
	movq	%r9, (%rsp)
	call	__correctly_grouped_prefixwc
	cmpq	$0, 24(%rsp)
	movq	(%rsp), %r9
	je	.L135
	cmpq	%rax, %rbp
	je	.L583
.L136:
	movq	24(%rsp), %rsi
	movq	%rax, (%rsi)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L568:
	movl	4(%rbp), %r12d
	cmpl	%r12d, 16(%rsp)
	leaq	8(%rbx), %rbp
	movl	%eax, 44(%rsp)
	jne	.L114
.L569:
	movl	4(%rbp), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L115
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L115
.L570:
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r12d, %edi
	call	__GI___towlower_l
	cmpl	$105, %eax
	je	.L584
	cmpl	$110, %eax
	je	.L585
.L144:
	cmpq	$0, 24(%rsp)
	je	.L124
	movq	24(%rsp), %rax
	movq	32(%rsp), %rsi
	pxor	%xmm0, %xmm0
	movq	%rsi, (%rax)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L575:
	movl	%r15d, 32(%rsp)
	movb	%bl, 56(%rsp)
	movq	%r14, %r15
	leaq	4(%r10), %r13
	movl	4(%r10), %r12d
	movq	%r9, 48(%rsp)
	movq	%r11, %r14
	movl	8(%rsp), %ebp
	movq	%r11, %rbx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L587:
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	call	__GI___towlower_l
	subl	$97, %eax
	cmpl	$5, %eax
	ja	.L586
.L155:
	cmpl	$48, %r12d
	je	.L154
	cmpq	$-1, %r15
	movq	%r14, %rax
	sete	%dl
	subq	%rbx, %rax
	testb	%dl, %dl
	cmovne	%rax, %r15
.L154:
	addq	$4, %r13
	movl	0(%r13), %r12d
	addq	$1, %r14
.L153:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L155
	cmpl	$16, %ebp
	je	.L587
	movq	%r14, %rbp
	movq	%rbx, %r11
	movq	%r15, %r14
	testq	%rbp, %rbp
	movq	48(%rsp), %r9
	movl	32(%rsp), %r15d
	movzbl	56(%rsp), %ebx
	js	.L346
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movq	%r11, 48(%rsp)
	movq	%r9, 32(%rsp)
	call	__GI___towlower_l
	movq	32(%rsp), %r9
	movq	48(%rsp), %r11
	movq	%r13, %r10
.L344:
	cmpl	$16, 8(%rsp)
	je	.L375
	cmpl	$101, %eax
	jne	.L375
	movl	4(%r10), %ecx
	cmpl	$45, %ecx
	jne	.L161
.L576:
	movl	8(%r10), %ecx
	leaq	8(%r10), %r13
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L375
	cmpl	$16, 8(%rsp)
	je	.L588
	movabsq	$9223372036854775447, %rax
	cmpq	%rax, %r11
	ja	.L589
	leaq	360(%r11), %rsi
.L560:
	movq	%rsi, %rax
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	movq	%rsi, %rdi
	movl	$1, %esi
.L168:
	movq	112(%rsp), %rax
	movzbl	64(%rsp), %r8d
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L178:
	je	.L590
.L180:
	leaq	(%rax,%rax,4), %rax
	addq	$4, %r13
	movl	%r8d, %ebx
	leaq	(%rcx,%rax,2), %rax
	movl	0(%r13), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L591
.L182:
	cmpq	%rax, %rdx
	jge	.L178
	testb	%bl, %bl
	jne	.L592
.L179:
	cmpq	$-1, %r14
	je	.L593
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%esi, %esi
	movl	$34, %fs:(%rax)
	je	.L186
	movl	44(%rsp), %ebp
	testl	%ebp, %ebp
	je	.L187
	movsd	.LC1(%rip), %xmm0
	mulsd	.LC0(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L189:
	addq	$4, %r13
	movl	0(%r13), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L189
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L108
	movq	%r13, (%rax)
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L201:
	testq	%r14, %r14
	je	.L202
	movl	16(%rsp), %eax
	cmpl	(%r9), %eax
	je	.L203
	.p2align 4,,10
	.p2align 3
.L204:
	addq	$4, %r9
	cmpl	%eax, (%r9)
	jne	.L204
.L203:
	cmpl	$16, 8(%rsp)
	leaq	4(%r9,%r14,4), %r9
	je	.L594
	testq	%r14, %r14
	movq	%r14, %rcx
	js	.L351
	movq	112(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	cmpq	%r14, %rdx
	jb	.L208
.L209:
	subq	%rcx, %rax
	subq	%r14, %rbp
	movq	%rax, 112(%rsp)
.L202:
	cmpl	$16, 8(%rsp)
	je	.L595
	movq	112(%rsp), %rax
	testq	%rax, %rax
	js	.L596
	movq	%rbp, %rdx
	subq	%r11, %rdx
	cmpq	%rax, %rdx
	cmovg	%rax, %rdx
.L230:
	subq	%rdx, %rax
	leaq	(%rdx,%r11), %rbx
	movq	%rax, %rcx
	movq	%rax, 112(%rsp)
	movl	$309, %eax
	subq	%rbx, %rax
	cmpq	%rax, %rcx
	jg	.L561
	cmpq	$-324, %rcx
	jl	.L597
	testq	%rbx, %rbx
	jne	.L235
	testq	%rbp, %rbp
	je	.L238
	leaq	324(%rcx), %rax
	cmpq	$324, %rax
	ja	.L238
	cmpl	$48, (%r9)
	je	.L260
	movl	$1, %eax
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movl	$1076, %esi
	shrq	%rdx
	leal	54(%rdx), %eax
	movl	%ecx, %edx
	cmpl	$1076, %eax
	cmovg	%esi, %eax
	addl	%ecx, %eax
	testl	%eax, %eax
	jle	.L598
	leaq	112(%rsp), %rsi
	leaq	104(%rsp), %rdi
	movl	$0, (%rsp)
	movq	$0, 48(%rsp)
	movq	%rsi, 80(%rsp)
	leaq	128(%rsp), %rsi
	movq	%rdi, 72(%rsp)
	movq	%rsi, 8(%rsp)
.L262:
	movslq	%eax, %rcx
	movq	%rbp, %rsi
	movl	%ebp, %eax
	subl	%ebx, %eax
	subq	%rbx, %rsi
	cmpq	%rsi, %rcx
	cltq
	cmovg	%rax, %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rbp
	jle	.L264
	movq	%rcx, %rbp
	movl	$1, %r15d
.L264:
	movl	%ebp, %eax
	xorl	%ebp, %ebp
	movl	%r15d, 56(%rsp)
	subl	%ebx, %eax
	leaq	_fpioconst_pow10(%rip), %r14
	movl	$1, %r12d
	movl	%eax, %ebx
	movl	%eax, 32(%rsp)
	movq	8(%rsp), %rax
	subl	%edx, %ebx
	movq	%r9, 64(%rsp)
	movq	%rbp, %r13
	movq	%rax, 24(%rsp)
	leaq	608(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	%rax, %r15
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L600:
	leaq	0(,%rbp,8), %rdx
	movq	%r15, %rdi
	movq	%rbp, %r13
	call	__GI_memcpy@PLT
.L265:
	addl	%r12d, %r12d
	addq	$24, %r14
	testl	%ebx, %ebx
	je	.L599
.L267:
	testl	%ebx, %r12d
	je	.L265
	movq	8(%r14), %rax
	leaq	__tens(%rip), %rsi
	xorl	%r12d, %ebx
	testq	%r13, %r13
	leaq	-1(%rax), %rbp
	movq	(%r14), %rax
	leaq	8(%rsi,%rax,8), %rsi
	je	.L600
	movq	%rbp, %rdx
	movq	24(%rsp), %rbp
	movq	%r13, %r8
	movq	%r15, %rcx
	movq	%rbp, %rdi
	call	__mpn_mul
	movq	8(%r14), %rdx
	testq	%rax, %rax
	leaq	-1(%r13,%rdx), %r13
	jne	.L386
	movq	%r15, %rax
	subq	$1, %r13
	movq	%rbp, %r15
	movq	%rax, 24(%rsp)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L567:
	movl	4(%rbp), %r12d
	movl	$1, 44(%rsp)
	leaq	8(%rbx), %rbp
	jmp	.L113
.L574:
	movl	%r13d, %edx
	movq	%r10, %rsi
	movq	%r14, %rcx
	movq	%rbp, %rdi
	movq	%r11, 72(%rsp)
	movq	%r9, 56(%rsp)
	movq	%r10, 48(%rsp)
	call	__correctly_grouped_prefixwc
	movq	48(%rsp), %r10
	movq	%rax, %r13
	movq	56(%rsp), %r9
	movq	72(%rsp), %r11
	cmpq	%rax, %r10
	je	.L143
	cmpq	%rax, %rbp
	je	.L144
	cmpq	%rax, %r9
	ja	.L539
	movq	%r9, %rax
	movl	$0, %ebp
	jnb	.L539
.L146:
	movl	(%rax), %esi
	leal	-48(%rsi), %edx
	cmpl	$10, %edx
	adcq	$0, %rbp
	addq	$4, %rax
	cmpq	%rax, %r13
	ja	.L146
	movq	%rbp, %r11
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$0, 24(%rsp)
	jne	.L349
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L578:
	cmpl	$16, 8(%rsp)
	leaq	4(%r10), %r13
	je	.L165
.L166:
	testq	%r11, %r11
	je	.L174
	testq	%r14, %r14
	jne	.L393
	testq	%r11, %r11
	js	.L393
	movl	$309, %esi
	subq	%r11, %rsi
.L171:
	testq	%rsi, %rsi
	movl	$0, %eax
	movabsq	$-3689348814741910323, %rdx
	cmovs	%rax, %rsi
	movq	%rsi, %rax
	movq	%rsi, %rdi
	xorl	%esi, %esi
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L581:
	cmpl	$16, 8(%rsp)
	jne	.L128
	cmpq	%r9, %rbp
	jne	.L128
	movl	4(%r9), %edi
	leal	-48(%rdi), %eax
	cmpl	$9, %eax
	jbe	.L128
	movq	(%rsp), %rsi
	movq	%r9, 48(%rsp)
	movl	%edx, 56(%rsp)
	call	__GI___towlower_l
	subl	$97, %eax
	movq	48(%rsp), %r9
	cmpl	$5, %eax
	ja	.L131
	movl	56(%rsp), %edx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L580:
	movl	8(%rsp), %esi
	xorl	%eax, %eax
	movq	%r13, 48(%rsp)
	movq	%r14, 56(%rsp)
	leaq	-4(%r10), %r12
	movq	%r9, 32(%rsp)
	movq	%r11, %r13
	cmpl	$16, %esi
	sete	%al
	leaq	1(%rax,%rax,2), %rbx
	movq	%rbx, %r14
	movl	%esi, %ebx
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L196:
	subl	$48, %edi
	cmpl	$9, %edi
	seta	%al
.L197:
	testb	%al, %al
	jne	.L198
	cmpl	$48, (%r12)
	jne	.L550
	movq	112(%rsp), %rax
	subq	$1, %r13
	addq	%r14, %rax
	subq	$1, %rbp
	movq	%rax, 112(%rsp)
	je	.L551
	testq	%rax, %rax
	jns	.L551
.L198:
	subq	$4, %r12
.L195:
	cmpl	$16, %ebx
	movl	(%r12), %edi
	jne	.L196
	movq	(%rsp), %rsi
	call	__GI___iswxdigit_l
	testl	%eax, %eax
	sete	%al
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L595:
	movq	%r9, %r12
	movq	%r11, %r14
	movq	(%rsp), %r13
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L379:
	movq	%rdx, %r12
.L211:
	movl	(%r12), %edi
	movq	%r13, %rsi
	call	__GI___iswxdigit_l
	leaq	4(%r12), %rdx
	testl	%eax, %eax
	movq	%rdx, %rbx
	je	.L379
	movl	(%r12), %edi
	movq	%r12, %r9
	movq	%r14, %r11
	cmpl	$48, %edi
	je	.L213
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L380:
	movq	%rdx, %r9
	addq	$4, %rdx
.L213:
	movl	(%rdx), %edi
	cmpl	$48, %edi
	je	.L380
	leaq	8(%r9), %rbx
.L212:
	leal	-48(%rdi), %edx
	cmpl	$9, %edx
	ja	.L214
	movslq	%edx, %rdx
.L215:
	leaq	nbits.12074(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	je	.L601
	movl	$53, %ecx
	movl	$52, %r13d
	movslq	%eax, %rsi
	subl	%eax, %ecx
	subl	%eax, %r13d
	salq	%cl, %rdx
	movq	112(%rsp), %rcx
	movq	%rdx, 120(%rsp)
	testq	%rcx, %rcx
	js	.L602
	movabsq	$9223372036854775807, %rdx
	subq	%rcx, %rdx
	subq	%rsi, %rdx
	leaq	4(%rdx), %rsi
	addq	$1, %rdx
	cmovs	%rsi, %rdx
	sarq	$2, %rdx
.L218:
	cmpq	%r11, %rdx
	jb	.L603
	subl	$1, %eax
	cltq
	leaq	-4(%rax,%r11,4), %rsi
	addq	%rcx, %rsi
	subq	$1, %rbp
	movq	%rsi, 112(%rsp)
	je	.L220
	movq	(%rsp), %r12
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L223:
	cmpl	$2, %r13d
	movq	120(%rsp), %rdi
	leaq	-1(%rbp), %rdx
	jle	.L224
	leal	-3(%r13), %ecx
	movq	%rsi, %rax
	subl	$4, %r13d
	movq	%r14, %rbx
	movq	%rdx, %rbp
	salq	%cl, %rax
	orq	%rdi, %rax
	testq	%rdx, %rdx
	movq	%rax, 120(%rsp)
	je	.L604
.L225:
	movl	(%rbx), %edi
	movq	%r12, %rsi
	leaq	4(%rbx), %r14
	call	__GI___iswxdigit_l
	testl	%eax, %eax
	jne	.L221
	leaq	8(%rbx), %rax
	movq	%r14, %rbx
	movq	%rax, %r14
.L221:
	movl	(%rbx), %edi
	leal	-48(%rdi), %eax
	cmpl	$9, %eax
	movslq	%eax, %rsi
	jbe	.L223
	movq	%r12, %rsi
	call	__GI___towlower_l
	leal	-87(%rax), %esi
	jmp	.L223
.L585:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	jne	.L144
	cmpl	$40, 12(%rbp)
	leaq	12(%rbp), %rbx
	movsd	.LC8(%rip), %xmm0
	je	.L605
.L121:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L123
	movq	%rbx, (%rax)
.L123:
	movl	44(%rsp), %r14d
	testl	%r14d, %r14d
	je	.L108
	xorpd	.LC17(%rip), %xmm0
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L573:
	movq	(%rsp), %rsi
	movl	%r12d, %edi
	movb	%cl, 72(%rsp)
	movq	%r11, 64(%rsp)
	movq	%r10, 56(%rsp)
	movq	%r9, 48(%rsp)
	call	__GI___towlower_l
	subl	$97, %eax
	movq	48(%rsp), %r9
	movq	56(%rsp), %r10
	cmpl	$5, %eax
	movq	64(%rsp), %r11
	movzbl	72(%rsp), %ecx
	jbe	.L140
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L571:
	movl	4(%rbp), %edi
	movq	(%rsp), %rsi
	call	__GI___towlower_l
	cmpl	$120, %eax
	jne	.L125
	movl	8(%rbp), %r12d
	xorl	%r14d, %r14d
	addq	$8, %rbp
	movl	$16, 8(%rsp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L590:
	cmpq	%rdi, %rcx
	jle	.L180
	testb	%bl, %bl
	je	.L179
	movq	%rdx, 112(%rsp)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L577:
	movl	8(%r10), %ecx
	subl	$48, %ecx
	cmpl	$9, %ecx
	ja	.L375
	cmpl	$16, 8(%rsp)
	leaq	8(%r10), %r13
	jne	.L166
.L165:
	testq	%r11, %r11
	je	.L606
	testq	%r14, %r14
	jne	.L392
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r11
	ja	.L392
	movl	$256, %eax
	subq	%r11, %rax
	leaq	3(,%rax,4), %rsi
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L586:
	movq	%rbx, %r11
	movq	%r14, %rbp
	movq	48(%rsp), %r9
	movq	%r15, %r14
	movzbl	56(%rsp), %ebx
	movl	32(%rsp), %r15d
	movq	%r13, %r10
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L565:
	movq	80(%rax), %r14
	movzbl	(%r14), %edi
	leal	-1(%rdi), %edx
	cmpb	$125, %dl
	ja	.L361
	movl	96(%rax), %r13d
	movl	$0, %edx
	testl	%r13d, %r13d
	cmove	%rdx, %r14
	jmp	.L109
.L591:
	movq	%rax, %rdx
	negq	%rdx
	testl	%esi, %esi
	cmovne	%rdx, %rax
	movq	%rax, 112(%rsp)
	jmp	.L160
.L584:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$3, %edx
	movq	%rbp, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	jne	.L144
	movq	24(%rsp), %r15
	testq	%r15, %r15
	je	.L118
	leaq	12(%rbp), %rbx
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	addq	$32, %rbp
	movq	%rbx, %rdi
	call	__GI___wcsncasecmp_l
	testl	%eax, %eax
	cmove	%rbp, %rbx
	movq	%rbx, (%r15)
.L118:
	movl	44(%rsp), %r15d
	movsd	.LC11(%rip), %xmm0
	testl	%r15d, %r15d
	je	.L108
	movsd	.LC12(%rip), %xmm0
	jmp	.L108
.L593:
	movl	44(%rsp), %r12d
	pxor	%xmm0, %xmm0
	testl	%r12d, %r12d
	je	.L189
	movsd	.LC10(%rip), %xmm0
	jmp	.L189
.L596:
	movq	%r11, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	cmovl	%rax, %rdx
	jmp	.L230
.L186:
	movl	44(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L188
	movsd	.LC3(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	jmp	.L189
.L582:
	cmpl	$112, %eax
	jne	.L131
	cmpq	%r9, %rbp
	jne	.L128
	jmp	.L131
.L361:
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L109
.L174:
	cmpq	$-1, %r14
	je	.L377
	movabsq	$9223372036854775498, %rax
	cmpq	%rax, %r14
	ja	.L607
	leaq	309(%r14), %rsi
.L559:
	movq	%rsi, %rax
	movabsq	$-3689348814741910323, %rdx
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	movq	%rsi, %rdi
	xorl	%esi, %esi
	jmp	.L168
.L188:
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L189
.L187:
	movsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L189
.L606:
	cmpq	$-1, %r14
	je	.L376
	movabsq	$2305843009213693695, %rax
	cmpq	%rax, %r14
	ja	.L608
	leaq	1027(,%r14,4), %rsi
	jmp	.L559
	.p2align 4,,10
	.p2align 3
.L599:
	movq	%r13, %rbp
	movq	%r15, %r13
	cmpq	8(%rsp), %r13
	movq	64(%rsp), %r9
	movl	56(%rsp), %r15d
	je	.L609
.L268:
	movq	80(%rsp), %r8
	movq	72(%rsp), %rcx
	movq	%r9, %rdi
	movq	8(%rsp), %rdx
	movl	32(%rsp), %esi
	call	str_to_mpn
	leaq	-1(%rbp), %rax
	bsrq	608(%rsp,%rax,8), %r12
	movq	%rax, 32(%rsp)
	xorq	$63, %r12
	testl	%r12d, %r12d
	jne	.L269
.L562:
	movq	104(%rsp), %rdx
.L270:
	movq	48(%rsp), %rax
	cmpq	$1, %rbp
	movq	%rax, 112(%rsp)
	je	.L273
	cmpq	$2, %rbp
	jne	.L610
	cmpq	$1, %rdx
	movq	608(%rsp), %r8
	movq	616(%rsp), %rdi
	movq	128(%rsp), %r13
	jg	.L284
	cmpq	%r13, %rdi
	jbe	.L387
	movl	(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L611
	movl	$53, %eax
	subl	(%rsp), %eax
	leaq	120(%rsp), %rdi
	movq	%rdi, 48(%rsp)
	movl	%eax, %ecx
	je	.L288
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	128(%rsp), %r13
.L288:
	movl	(%rsp), %r8d
	movq	112(%rsp), %rax
	movq	%r13, %rbp
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	addl	$10, %r8d
	movslq	%r8d, %r8
.L289:
	orq	%r13, %rbp
	movl	44(%rsp), %edx
	movq	48(%rsp), %rdi
	setne	%r9b
	leaq	-1(%rax), %rsi
	movq	%r12, %rcx
	orl	%r15d, %r9d
	andl	$1, %r9d
	call	round_and_return
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L386:
	movq	%r15, %rax
	movq	24(%rsp), %r15
	movq	%rax, 24(%rsp)
	jmp	.L265
.L269:
	movq	16(%rsp), %rdi
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	8(%rsp), %rdi
	movq	104(%rsp), %rdx
	movl	%r12d, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L562
	movq	104(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, 128(%rsp,%rcx,8)
	movq	%rdx, 104(%rsp)
	jmp	.L270
.L550:
	movq	%r13, %r11
	movq	32(%rsp), %r9
	movq	48(%rsp), %r13
	movq	56(%rsp), %r14
	jmp	.L192
.L604:
	movq	112(%rsp), %rsi
.L220:
	movl	44(%rsp), %edx
	leaq	120(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L108
.L235:
	leaq	128(%rsp), %rax
	leaq	112(%rsp), %r8
	leaq	104(%rsp), %rcx
	movq	%r9, %rdi
	movl	%ebx, %esi
	movq	%rax, %rdx
	movq	%r8, 80(%rsp)
	movq	%rcx, 72(%rsp)
	movq	%rax, 8(%rsp)
	call	str_to_mpn
	movq	112(%rsp), %rdx
	movq	%rax, %r9
	movq	104(%rsp), %r12
	testq	%rdx, %rdx
	jle	.L240
	leaq	608(%rsp), %rax
	leaq	_fpioconst_pow10(%rip), %r14
	movq	%r12, %rdi
	movl	$1, %r13d
	movq	%rbx, %r12
	movq	%rax, 16(%rsp)
	movq	%rax, %r10
	movq	8(%rsp), %rax
	movq	%rax, (%rsp)
.L241:
	movslq	%r13d, %rax
	testq	%rdx, %rax
	je	.L242
.L613:
	movq	8(%r14), %rsi
	xorq	%rdx, %rax
	movq	%r9, 32(%rsp)
	movq	%rax, 112(%rsp)
	movq	(%r14), %rax
	leaq	-1(%rsi), %rbx
	leaq	__tens(%rip), %rsi
	cmpq	%rdi, %rbx
	leaq	8(%rsi,%rax,8), %rsi
	jg	.L243
	movq	%rsi, %rcx
	movq	(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%rbx, %r8
	movq	%r10, %rdi
	movq	%r10, 24(%rsp)
	call	__mpn_mul
	movq	24(%rsp), %r10
	movq	32(%rsp), %r9
.L244:
	movq	104(%rsp), %rdi
	movq	112(%rsp), %rdx
	addq	%rbx, %rdi
	testq	%rax, %rax
	movq	%rdi, 104(%rsp)
	jne	.L245
	subq	$1, %rdi
	movq	%rdi, 104(%rsp)
.L245:
	addl	%r13d, %r13d
	addq	$24, %r14
	testq	%rdx, %rdx
	je	.L612
	movq	(%rsp), %rax
	movq	%r10, (%rsp)
	movq	%rax, %r10
	movslq	%r13d, %rax
	testq	%rdx, %rax
	jne	.L613
.L242:
	addl	%r13d, %r13d
	addq	$24, %r14
	jmp	.L241
.L243:
	movq	(%rsp), %rcx
	movq	%rdi, %r8
	movq	%rbx, %rdx
	movq	%r10, %rdi
	movq	%r10, 24(%rsp)
	call	__mpn_mul
	movq	32(%rsp), %r9
	movq	24(%rsp), %r10
	jmp	.L244
.L612:
	cmpq	16(%rsp), %r10
	movq	%r12, %rbx
	movq	%rdi, %r12
	jne	.L240
	leaq	0(,%rdi,8), %rdx
	movq	8(%rsp), %rdi
	movq	%r10, %rsi
	movq	%r9, (%rsp)
	call	__GI_memcpy@PLT
	movq	(%rsp), %r9
.L240:
	leaq	-1(%r12), %rdx
	movl	%r12d, %r13d
	sall	$6, %r13d
	bsrq	128(%rsp,%rdx,8), %rax
	xorq	$63, %rax
	subl	%eax, %r13d
	cmpl	$1024, %r13d
	movl	%r13d, (%rsp)
	jg	.L561
	cmpl	$53, %r13d
	jle	.L249
	leal	-53(%r13), %ecx
	movl	%ecx, %eax
	sarl	$6, %eax
	andl	$63, %ecx
	movslq	%eax, %rsi
	movq	128(%rsp,%rsi,8), %r10
	jne	.L250
	subq	$1, %rsi
	movq	%r10, 120(%rsp)
	movl	$63, %r8d
	movq	128(%rsp,%rsi,8), %r10
.L251:
	cmpq	$0, 128(%rsp)
	jne	.L252
	movq	8(%rsp), %rdx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L253:
	movl	%eax, %r15d
	addq	$1, %rax
	cmpq	$0, -8(%rdx,%rax,8)
	je	.L253
.L252:
	cmpq	%rbx, %rbp
	movl	$1, %r9d
	ja	.L254
	movslq	%r15d, %rax
	xorl	%r9d, %r9d
	cmpq	%rsi, %rax
	setl	%r9b
.L254:
	leal	-1(%r13), %esi
	movl	44(%rsp), %edx
	leaq	120(%rsp), %rdi
	movq	%r10, %rcx
	movslq	%esi, %rsi
	call	round_and_return
	jmp	.L108
.L551:
	movq	%r13, %r11
	movq	32(%rsp), %r9
	movq	48(%rsp), %r13
	movq	56(%rsp), %r14
	jmp	.L151
.L594:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r14
	ja	.L351
	movq	112(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	cmpq	%r14, %rdx
	jb	.L208
	leaq	0(,%r14,4), %rcx
	jmp	.L209
.L588:
	movabsq	$2305843009213693683, %rax
	cmpq	%rax, %r11
	ja	.L614
	leaq	1074(,%r11,4), %rsi
	jmp	.L560
.L224:
	movl	$3, %ecx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	subl	%r13d, %ecx
	shrq	%cl, %rdi
	leal	61(%r13), %ecx
	orq	%rdi, %rax
	salq	%cl, %rsi
	testq	%rdx, %rdx
	movq	%rax, 120(%rsp)
	movq	%rsi, %rcx
	je	.L226
	cmpl	$48, (%r14)
	jne	.L382
	leaq	-2(%rbp), %rdx
	xorl	%eax, %eax
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L228:
	addq	$1, %rax
	cmpl	$48, (%r14,%rax,4)
	jne	.L382
.L227:
	cmpq	%rdx, %rax
	jne	.L228
.L226:
	movl	44(%rsp), %edx
	movq	112(%rsp), %rsi
	leaq	120(%rsp), %rdi
	movl	%r15d, %r9d
	movl	$63, %r8d
	call	round_and_return
	jmp	.L108
.L610:
	movq	32(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	608(%rsp,%rax,8), %r14
	leaq	-2(%rbp), %rax
	movq	608(%rsp,%rax,8), %r10
	movq	%rax, 80(%rsp)
	movq	%rbp, %rax
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rsi
	movq	8(%rsp), %rdi
	movq	%r10, 24(%rsp)
	call	__mpn_cmp
	testl	%eax, %eax
	movq	24(%rsp), %r10
	js	.L615
	movq	104(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	$0, 128(%rsp,%rdx,8)
	movq	%rax, 104(%rsp)
.L309:
	cmpq	%rax, %rbp
	jle	.L310
	movq	%rbp, %r12
	movl	(%rsp), %ecx
	subq	%rax, %r12
	movq	%r12, %rdx
	salq	$6, %rdx
	testl	%ecx, %ecx
	je	.L616
	movl	$53, %ebx
	subl	(%rsp), %ebx
	leaq	120(%rsp), %rsi
	movq	%rsi, 48(%rsp)
	jne	.L617
.L313:
	movl	%r12d, %edx
	sall	$6, %edx
	addl	%edx, (%rsp)
.L312:
	testl	%eax, %eax
	movslq	%eax, %rcx
	jle	.L317
	leal	-1(%rax), %edi
	movq	8(%rsp), %rax
	addq	%r12, %rcx
	movslq	%edi, %rdx
	movl	%edi, %edi
	leaq	0(,%rdx,8), %rsi
	salq	$3, %rdi
	subq	%rdx, %rcx
	addq	%rsi, %rax
	addq	48(%rsp), %rsi
	subq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L316:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L316
.L317:
	movq	8(%rsp), %rax
	movq	%rax, %rdx
	addq	$8, %rax
	leaq	(%rax,%r12,8), %rcx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L618:
	addq	$8, %rax
.L315:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L618
	cmpl	$53, (%rsp)
	movq	$0, 608(%rsp,%rbp,8)
	movq	128(%rsp,%rbp,8), %r12
	jg	.L619
.L321:
	leaq	1(%rbp), %rax
	movq	8(%rsp), %rdi
	movl	%r15d, 92(%rsp)
	movl	%ebx, 88(%rsp)
	movq	%rbp, %r15
	movq	%rax, 56(%rsp)
	leal	-2(%rbp), %eax
	movslq	%eax, %rcx
	movl	%eax, %eax
	leaq	0(,%rcx,8), %rdx
	salq	$3, %rax
	leaq	(%rdi,%rdx), %rsi
	movq	%rsi, 64(%rsp)
	movq	48(%rsp), %rsi
	leaq	(%rsi,%rdx), %r8
	subq	%rax, %r8
	leal	-1(%rbp), %eax
	movq	%r10, %rbp
	movq	%r8, %rbx
	movslq	%eax, %r9
	movl	%eax, 72(%rsp)
	subq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L325:
	cmpq	%r14, %r12
	movq	$-1, %r13
	je	.L326
	movq	32(%rsp), %rax
	movq	%r12, %rdx
	movq	80(%rsp), %rsi
	movq	128(%rsp,%rax,8), %rax
	movq	%rax, 24(%rsp)
#APP
# 1727 "../stdlib/strtod_l.c" 1
	divq %r14
# 0 "" 2
#NO_APP
	movq	%rax, %r13
	movq	%rdx, %r12
	movq	%rbp, %rax
#APP
# 1728 "../stdlib/strtod_l.c" 1
	mulq %r13
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	jmp	.L327
	.p2align 4,,10
	.p2align 3
.L620:
	xorl	%edx, %edx
	cmpq	%rbp, %rax
	setb	%dl
	subq	%rbp, %rax
	subq	%rdx, %rcx
.L327:
	cmpq	%r12, %rcx
	ja	.L330
	jne	.L326
	cmpq	%rax, 128(%rsp,%rsi,8)
	jnb	.L326
.L330:
	subq	$1, %r13
	addq	%r14, %r12
	jnc	.L620
.L326:
	movq	8(%rsp), %r12
	movq	56(%rsp), %rdx
	movq	%r13, %rcx
	movq	16(%rsp), %rsi
	movq	%r9, 24(%rsp)
	movq	%r12, %rdi
	call	__mpn_submul_1
	cmpq	%rax, 128(%rsp,%r15,8)
	movq	24(%rsp), %r9
	je	.L331
	movq	16(%rsp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_add_n
	testq	%rax, %rax
	movq	24(%rsp), %r9
	je	.L621
	subq	$1, %r13
.L331:
	movq	32(%rsp), %rax
	movl	72(%rsp), %edx
	movq	128(%rsp,%rax,8), %r12
	testl	%edx, %edx
	movq	64(%rsp), %rax
	movq	%r12, 128(%rsp,%r15,8)
	jle	.L336
	.p2align 4,,10
	.p2align 3
.L333:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%r9,8)
	subq	$8, %rax
	cmpq	%rax, %rbx
	jne	.L333
.L336:
	movl	(%rsp), %eax
	movq	$0, 128(%rsp)
	testl	%eax, %eax
	jne	.L622
	testq	%r13, %r13
	movq	112(%rsp), %rsi
	je	.L337
	bsrq	%r13, %rax
	xorq	$63, %rax
	movslq	%eax, %rcx
	movl	%eax, %edx
	subq	%rcx, %rsi
	movl	$64, %ecx
	movl	%ecx, %edi
	movq	%rsi, 112(%rsp)
	subl	%eax, %edi
	cmpl	$53, %edi
	jle	.L338
	leal	53(%rdx), %ebx
	movq	%r15, %rbp
	movq	%r13, %rax
	movl	92(%rsp), %r15d
	subl	%ebx, %ecx
	shrq	%cl, %rax
	movq	%rax, 120(%rsp)
.L324:
	testl	%ebp, %ebp
	movl	%ebp, %edx
	js	.L342
	movslq	%ebp, %rax
	cmpq	$0, 128(%rsp,%rax,8)
	jne	.L342
	leal	-1(%rbp), %eax
	movl	%ebp, %ebp
	movq	8(%rsp), %rdi
	cltq
	movq	%rax, %rcx
	subq	%rbp, %rcx
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L623:
	subq	$1, %rax
	cmpq	$0, 8(%rdi,%rax,8)
	jne	.L342
.L343:
	cmpq	%rax, %rcx
	movl	%eax, %edx
	jne	.L623
.L342:
	movl	%r15d, %r9d
	notl	%edx
	movl	$63, %r8d
	shrl	$31, %edx
	andl	$1, %r9d
	movq	48(%rsp), %rdi
	orl	%edx, %r9d
	movl	44(%rsp), %edx
	subl	%ebx, %r8d
	subq	$1, %rsi
	andl	$1, %r9d
	movslq	%r8d, %r8
	movq	%r13, %rcx
	call	round_and_return
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L622:
	movl	(%rsp), %esi
	cmpl	$-10, %esi
	leal	64(%rsi), %eax
	jge	.L340
	movq	%r13, 120(%rsp)
	movl	%eax, (%rsp)
.L339:
	cmpl	$53, (%rsp)
	jle	.L325
	movq	%r15, %rbp
	movl	88(%rsp), %ebx
	movl	92(%rsp), %r15d
	movq	112(%rsp), %rsi
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L337:
	subq	$64, %rsi
	movq	$0, 120(%rsp)
	movq	%rsi, 112(%rsp)
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L340:
	movl	$53, %edx
	subl	(%rsp), %edx
	movl	%eax, (%rsp)
	testl	%edx, %edx
	movl	%edx, 88(%rsp)
	je	.L339
	movq	48(%rsp), %rdi
	movl	%edx, %ecx
	movl	$1, %edx
	movq	%r9, 24(%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	$64, %ecx
	subl	88(%rsp), %ecx
	movq	%r13, %rax
	movq	24(%rsp), %r9
	shrq	%cl, %rax
	orq	%rax, 120(%rsp)
	jmp	.L339
.L273:
	cmpq	$1, %rdx
	movq	128(%rsp), %rbp
	movq	608(%rsp), %rsi
	jne	.L394
	cmpq	%rsi, %rbp
	jnb	.L394
	movl	(%rsp), %r8d
	xorl	%edi, %edi
	movl	$64, %ecx
	movq	%rbp, %rdx
.L275:
	movq	%rdi, %rax
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	je	.L353
	movq	%rax, %r12
	movl	$53, %ebx
	leaq	120(%rsp), %rax
	subl	%r8d, %ebx
	movq	%rdx, %rbp
	movq	%rax, 48(%rsp)
	jne	.L624
.L282:
	movq	112(%rsp), %rdx
.L280:
	testq	%rbp, %rbp
	leaq	-1(%rdx), %rsi
	movl	$63, %r8d
	setne	%r9b
	movl	44(%rsp), %edx
	movq	48(%rsp), %rdi
	orl	%r15d, %r9d
	subl	%ebx, %r8d
	movq	%r12, %rcx
	andl	$1, %r9d
	movslq	%r8d, %r8
	call	round_and_return
	jmp	.L108
.L278:
	subq	$64, %r8
	movq	%r8, 112(%rsp)
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
.L353:
	testq	%rax, %rax
	movq	112(%rsp), %r8
	je	.L278
	bsrq	%rax, %r9
	movl	%ecx, %r11d
	xorq	$63, %r9
	movslq	%r9d, %r10
	subl	%r9d, %r11d
	movl	%r9d, %ebx
	subq	%r10, %r8
	cmpl	$53, %r11d
	movq	%r8, 112(%rsp)
	jle	.L279
	addl	$53, %ebx
	movq	%rax, %r12
	movq	%rdx, %rbp
	subl	%ebx, %ecx
	movq	%r8, %rdx
	shrq	%cl, %rax
	movq	%rax, 120(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 48(%rsp)
	jmp	.L280
.L602:
	movabsq	$-9223372036854775808, %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	jmp	.L218
.L214:
	movq	(%rsp), %rsi
	movq	%r11, 8(%rsp)
	call	__GI___towlower_l
	movq	8(%rsp), %r11
	leal	-87(%rax), %edx
	jmp	.L215
.L624:
	movl	%ebx, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%r12, %rax
	subl	%ebx, %ecx
	shrq	%cl, %rax
	orq	%rax, 120(%rsp)
	jmp	.L282
.L561:
	movl	44(%rsp), %r11d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r11d, %r11d
	movl	$34, %fs:(%rax)
	je	.L232
	movsd	.LC3(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	jmp	.L108
.L382:
	movl	$1, %r15d
	jmp	.L226
.L249:
	cmpq	%rbx, %rbp
	jne	.L255
	subl	$1, %r13d
	movl	$1, %ebx
	leaq	120(%rsp), %rdi
	movl	%r13d, %edx
	movq	%rbx, %rbp
	sarl	$31, %edx
	subq	%r12, %rbp
	movq	%rdi, 48(%rsp)
	shrl	$26, %edx
	leaq	(%rdi,%rbp,8), %rdi
	leal	0(%r13,%rdx), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$52, %eax
	je	.L625
	movq	8(%rsp), %rsi
	movl	$52, %ecx
	movq	%r12, %rdx
	subl	%eax, %ecx
	call	__mpn_lshift
	movq	%rbx, %rax
	subq	104(%rsp), %rax
	testq	%rax, %rax
	jle	.L258
.L257:
	movq	$0, 120(%rsp)
.L258:
	movl	44(%rsp), %edx
	movq	48(%rsp), %rdi
	movslq	%r13d, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L108
.L609:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	leaq	0(,%rbp,8), %rdx
	movq	%r9, 24(%rsp)
	call	__GI_memcpy@PLT
	movq	24(%rsp), %r9
	jmp	.L268
.L284:
	movq	136(%rsp), %rbp
.L287:
	movq	%r8, %r10
	xorl	%r9d, %r9d
	movl	$64, %ecx
	negq	%r10
.L290:
	cmpq	%rbp, %rdi
	jne	.L291
	addq	%r13, %rbp
	jnc	.L294
	movl	(%rsp), %edi
	subq	%r8, %rbp
	xorl	%r13d, %r13d
#APP
# 1607 "../stdlib/strtod_l.c" 1
	addq %r8,%r13
	adcq $0,%rbp
# 0 "" 2
#NO_APP
	movq	$-1, %r12
	testl	%edi, %edi
	jne	.L296
	movq	112(%rsp), %rax
	movl	$10, %r8d
	movabsq	$9007199254740991, %rdx
	movq	$-1, %r12
.L358:
	leaq	120(%rsp), %rsi
	movq	%rdx, 120(%rsp)
	movq	%rsi, 48(%rsp)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rbp, %rdx
	movq	%r13, %rax
#APP
# 1615 "../stdlib/strtod_l.c" 1
	divq %rdi
# 0 "" 2
#NO_APP
	movq	%rax, %r12
	movq	%rdx, %rbp
	movq	%r8, %rax
#APP
# 1616 "../stdlib/strtod_l.c" 1
	mulq %r12
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L302:
	cmpq	%rbp, %rdx
	ja	.L298
	jne	.L299
	testq	%rax, %rax
	je	.L299
.L298:
	subq	$1, %r12
#APP
# 1625 "../stdlib/strtod_l.c" 1
	subq %r8,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%rdi, %rbp
	jnc	.L302
.L299:
	movl	(%rsp), %esi
	movq	%r9, %r13
#APP
# 1630 "../stdlib/strtod_l.c" 1
	subq %rax,%r13
	sbbq %rdx,%rbp
# 0 "" 2
#NO_APP
	testl	%esi, %esi
	jne	.L296
	testq	%r12, %r12
	movq	112(%rsp), %rax
	jne	.L626
	subq	$64, %rax
	movq	%r9, 120(%rsp)
	movq	%rax, 112(%rsp)
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L296:
	movl	$53, %ebx
	subl	(%rsp), %ebx
	leaq	120(%rsp), %rax
	jne	.L359
	movq	%rax, 48(%rsp)
.L306:
	movl	$63, %r8d
	movq	112(%rsp), %rax
	subl	%ebx, %r8d
	movslq	%r8d, %r8
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L294:
	xorl	%eax, %eax
	testq	%r8, %r8
	movq	%r8, %rdx
	setne	%al
	movq	$-1, %r12
	subq	%rax, %rdx
	movq	%r10, %rax
	jmp	.L302
.L359:
	movl	%ebx, %ecx
	movq	%rax, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, 48(%rsp)
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%r12, %rax
	subl	%ebx, %ecx
	shrq	%cl, %rax
	orq	%rax, 120(%rsp)
	jmp	.L306
.L310:
	jne	.L627
	testl	%ebp, %ebp
	jle	.L628
	leal	-1(%rbp), %ecx
	movq	8(%rsp), %rax
	leaq	120(%rsp), %rsi
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	movq	%rsi, 48(%rsp)
	salq	$3, %rdx
	salq	$3, %rcx
	addq	%rdx, %rax
	addq	%rsi, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L322:
	movq	(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rax, %rcx
	jne	.L322
.L323:
	movq	$0, 128(%rsp)
	movq	$0, 608(%rsp,%rbp,8)
	movq	128(%rsp,%rbp,8), %r12
	jmp	.L321
.L626:
	bsrq	%r12, %rdx
	movl	%ecx, %ebx
	xorq	$63, %rdx
	movslq	%edx, %r11
	subl	%edx, %ebx
	movl	%edx, %esi
	subq	%r11, %rax
	cmpl	$53, %ebx
	movq	%rax, 112(%rsp)
	jle	.L304
	addl	$53, %esi
	movl	$63, %r8d
	movq	%r12, %rdx
	subl	%esi, %ecx
	subl	%esi, %r8d
	shrq	%cl, %rdx
	movslq	%r8d, %r8
	jmp	.L358
.L597:
	movl	44(%rsp), %r10d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r10d, %r10d
	movl	$34, %fs:(%rax)
	je	.L234
	movsd	.LC1(%rip), %xmm0
	mulsd	.LC0(%rip), %xmm0
	jmp	.L108
.L377:
	xorl	%esi, %esi
	movl	$9, %edi
	movl	$30, %edx
	jmp	.L168
.L615:
	movq	104(%rsp), %rax
	jmp	.L309
.L583:
	cmpl	$16, 8(%rsp)
	leaq	-4(%r9), %rax
	cmovne	32(%rsp), %rax
	jmp	.L136
.L232:
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L108
.L250:
	movslq	%ecx, %r8
	movq	%r10, %rdi
	subq	$1, %r8
	shrq	%cl, %rdi
	cmpq	%rsi, %rdx
	jg	.L355
	movq	%rdi, 120(%rsp)
	jmp	.L251
.L255:
	movq	8(%rsp), %rsi
	leaq	120(%rsp), %rdi
	leaq	0(,%r12,8), %rdx
	movq	%r9, 16(%rsp)
	call	__GI_memcpy@PLT
	cmpq	%rbx, %rbp
	jbe	.L238
	movq	112(%rsp), %rax
	leaq	324(%rax), %rdx
	cmpq	$324, %rdx
	ja	.L238
	testl	%r13d, %r13d
	jle	.L260
	testq	%rax, %rax
	movq	16(%rsp), %r9
	jne	.L629
	movslq	(%rsp), %rsi
	movl	$54, %eax
	xorl	%edx, %edx
	subl	%esi, %eax
	movq	%rsi, 48(%rsp)
	jmp	.L262
.L387:
	xorl	%ebp, %ebp
	jmp	.L287
.L376:
	xorl	%esi, %esi
	movl	$7, %edi
	movl	$102, %edx
	jmp	.L168
.L234:
	movsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L108
.L605:
	leaq	608(%rsp), %rsi
	leaq	16(%rbp), %rdi
	movl	$41, %edx
	call	__GI___wcstod_nan
	movq	608(%rsp), %rax
	cmpl	$41, (%rax)
	leaq	4(%rax), %rdx
	cmove	%rdx, %rbx
	jmp	.L121
.L539:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.L135
	movq	%r13, (%rax)
	jmp	.L135
.L616:
	leaq	120(%rsp), %rsi
	subq	%rdx, 112(%rsp)
	xorl	%ebx, %ebx
	movq	%rsi, 48(%rsp)
	jmp	.L312
.L355:
	addl	$1, %eax
	movslq	%eax, %rdx
	movl	$64, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movq	128(%rsp,%rdx,8), %rax
	salq	%cl, %rax
	orq	%rdi, %rax
	movq	%rax, 120(%rsp)
	jmp	.L251
.L625:
	movq	8(%rsp), %rsi
	leaq	0(,%r12,8), %rdx
	call	__GI_memcpy@PLT
	testq	%rbp, %rbp
	jle	.L258
	jmp	.L257
.L611:
	movq	48(%rsp), %rax
	movq	%r13, %rbp
	xorl	%r13d, %r13d
	subq	$64, %rax
	movq	%rax, 112(%rsp)
	jmp	.L287
.L628:
	leaq	120(%rsp), %rax
	movq	%rax, 48(%rsp)
	jmp	.L323
.L619:
	movq	112(%rsp), %rsi
	xorl	%r13d, %r13d
	jmp	.L324
.L617:
	movl	%ebx, %ecx
	movl	$1, %edx
	movq	%rsi, %rdi
	movq	%r10, 24(%rsp)
	call	__mpn_lshift
	movq	104(%rsp), %rax
	movq	24(%rsp), %r10
	jmp	.L313
.L260:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$1376, %edx
	call	__GI___assert_fail
.L304:
	movl	%ebx, (%rsp)
	movq	%r12, 120(%rsp)
	jmp	.L290
.L566:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$593, %edx
	call	__GI___assert_fail
.L279:
	movl	%r11d, %r8d
	movq	%rax, 120(%rsp)
	jmp	.L275
.L238:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$1360, %edx
	call	__GI___assert_fail
.L614:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$906, %edx
	call	__GI___assert_fail
.L351:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$1072, %edx
	call	__GI___assert_fail
.L589:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$938, %edx
	call	__GI___assert_fail
.L346:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$875, %edx
	call	__GI___assert_fail
.L394:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$1497, %edx
	call	__GI___assert_fail
.L592:
	movq	%rax, 112(%rsp)
	jmp	.L179
.L601:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$1100, %edx
	call	__GI___assert_fail
.L629:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC31(%rip), %rdi
	movl	$1370, %edx
	call	__GI___assert_fail
.L608:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$926, %edx
	call	__GI___assert_fail
.L598:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	movl	$1397, %edx
	call	__GI___assert_fail
.L208:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$1076, %edx
	call	__GI___assert_fail
.L392:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$914, %edx
	call	__GI___assert_fail
.L627:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$1708, %edx
	call	__GI___assert_fail
.L621:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$1750, %edx
	call	__GI___assert_fail
.L338:
	movl	%edi, (%rsp)
	movq	%r13, 120(%rsp)
	jmp	.L339
.L607:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	movl	$958, %edx
	call	__GI___assert_fail
.L393:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$946, %edx
	call	__GI___assert_fail
.L579:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$1021, %edx
	call	__GI___assert_fail
.L603:
	leaq	__PRETTY_FUNCTION__.12028(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$1121, %edx
	call	__GI___assert_fail
	.size	____wcstod_l_internal, .-____wcstod_l_internal
	.p2align 4,,15
	.weak	__GI___wcstod_l
	.hidden	__GI___wcstod_l
	.type	__GI___wcstod_l, @function
__GI___wcstod_l:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	jmp	____wcstod_l_internal
	.size	__GI___wcstod_l, .-__GI___wcstod_l
	.globl	__wcstod_l
	.set	__wcstod_l,__GI___wcstod_l
	.weak	wcstod_l
	.set	wcstod_l,__wcstod_l
	.weak	wcstof32x_l
	.set	wcstof32x_l,wcstod_l
	.weak	wcstof64_l
	.set	wcstof64_l,wcstod_l
	.globl	__GI_wcstod_l
	.set	__GI_wcstod_l,__wcstod_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11984, @object
	.size	__PRETTY_FUNCTION__.11984, 11
__PRETTY_FUNCTION__.11984:
	.string	"str_to_mpn"
	.section	.rodata
	.align 32
	.type	nbits.12074, @object
	.size	nbits.12074, 64
nbits.12074:
	.long	0
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12028, @object
	.size	__PRETTY_FUNCTION__.12028, 22
__PRETTY_FUNCTION__.12028:
	.string	"____wcstod_l_internal"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1048576
	.align 8
.LC1:
	.long	0
	.long	-2146435072
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.align 8
.LC3:
	.long	4294967295
	.long	-1048577
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC8:
	.long	0
	.long	2146959360
	.align 8
.LC10:
	.long	0
	.long	-2147483648
	.align 8
.LC11:
	.long	0
	.long	2146435072
	.align 8
.LC12:
	.long	0
	.long	-1048576
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC17:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
	.hidden	__mpn_cmp
	.hidden	__mpn_lshift
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	_nl_C_locobj
	.hidden	__correctly_grouped_prefixwc
	.hidden	__mpn_mul_1
	.hidden	__mpn_construct_double
	.hidden	__mpn_rshift
