	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcstof128_internal
	.hidden	__GI___wcstof128_internal
	.type	__GI___wcstof128_internal, @function
__GI___wcstof128_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstof128_l_internal
	.size	__GI___wcstof128_internal, .-__GI___wcstof128_internal
	.globl	__wcstof128_internal
	.set	__wcstof128_internal,__GI___wcstof128_internal
	.p2align 4,,15
	.weak	__GI_wcstof128
	.hidden	__GI_wcstof128
	.type	__GI_wcstof128, @function
__GI_wcstof128:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstof128_l_internal
	.size	__GI_wcstof128, .-__GI_wcstof128
	.globl	wcstof128
	.set	wcstof128,__GI_wcstof128
	.hidden	____wcstof128_l_internal
