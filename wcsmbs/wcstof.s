	.text
	.p2align 4,,15
	.globl	__wcstof_internal
	.hidden	__wcstof_internal
	.type	__wcstof_internal, @function
__wcstof_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstof_l_internal
	.size	__wcstof_internal, .-__wcstof_internal
	.p2align 4,,15
	.weak	wcstof
	.hidden	wcstof
	.type	wcstof, @function
wcstof:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstof_l_internal
	.size	wcstof, .-wcstof
	.weak	wcstof32
	.set	wcstof32,wcstof
	.hidden	____wcstof_l_internal
