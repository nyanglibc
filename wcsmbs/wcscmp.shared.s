 .text
.globl __wcscmp
.type __wcscmp,@function
.align 1<<4
__wcscmp:
 mov %esi, %eax
 mov %edi, %edx
 pxor %xmm0, %xmm0
 mov %al, %ch
 mov %dl, %cl
 and $63, %eax
 and $63, %edx
 and $15, %cl
 jz .Lcontinue_00
 cmp $16, %edx
 jb .Lcontinue_0
 cmp $32, %edx
 jb .Lcontinue_16
 cmp $48, %edx
 jb .Lcontinue_32
.Lcontinue_48:
 and $15, %ch
 jz .Lcontinue_48_00
 cmp $16, %eax
 jb .Lcontinue_0_48
 cmp $32, %eax
 jb .Lcontinue_16_48
 cmp $48, %eax
 jb .Lcontinue_32_48
 .p2align 4
.Lcontinue_48_48:
 mov (%rsi), %ecx
 cmp %ecx, (%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 4(%rsi), %ecx
 cmp %ecx, 4(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 8(%rsi), %ecx
 cmp %ecx, 8(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 12(%rsi), %ecx
 cmp %ecx, 12(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 movdqu 16(%rdi), %xmm1
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rdi), %xmm1
 movdqu 32(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 movdqu 48(%rdi), %xmm1
 movdqu 48(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_48
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_48_48
.Lcontinue_0:
 and $15, %ch
 jz .Lcontinue_0_00
 cmp $16, %eax
 jb .Lcontinue_0_0
 cmp $32, %eax
 jb .Lcontinue_0_16
 cmp $48, %eax
 jb .Lcontinue_0_32
 .p2align 4
.Lcontinue_0_48:
 mov (%rsi), %ecx
 cmp %ecx, (%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 4(%rsi), %ecx
 cmp %ecx, 4(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 8(%rsi), %ecx
 cmp %ecx, 8(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 12(%rsi), %ecx
 cmp %ecx, 12(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 movdqu 16(%rdi), %xmm1
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rdi), %xmm1
 movdqu 32(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 mov 48(%rsi), %ecx
 cmp %ecx, 48(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 52(%rsi), %ecx
 cmp %ecx, 52(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 56(%rsi), %ecx
 cmp %ecx, 56(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 60(%rsi), %ecx
 cmp %ecx, 60(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_0_48
 .p2align 4
.Lcontinue_00:
 and $15, %ch
 jz .Lcontinue_00_00
 cmp $16, %eax
 jb .Lcontinue_00_0
 cmp $32, %eax
 jb .Lcontinue_00_16
 cmp $48, %eax
 jb .Lcontinue_00_32
 .p2align 4
.Lcontinue_00_48:
 pcmpeqd (%rdi), %xmm0
 mov (%rdi), %eax
 pmovmskb %xmm0, %ecx
 test %ecx, %ecx
 jnz .Lless4_double_words1
 cmp (%rsi), %eax
 jne .Lnequal
 mov 4(%rdi), %eax
 cmp 4(%rsi), %eax
 jne .Lnequal
 mov 8(%rdi), %eax
 cmp 8(%rsi), %eax
 jne .Lnequal
 mov 12(%rdi), %eax
 cmp 12(%rsi), %eax
 jne .Lnequal
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd 16(%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd 32(%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 movdqu 48(%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd 48(%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_48
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_00_48
 .p2align 4
.Lcontinue_32:
 and $15, %ch
 jz .Lcontinue_32_00
 cmp $16, %eax
 jb .Lcontinue_0_32
 cmp $32, %eax
 jb .Lcontinue_16_32
 cmp $48, %eax
 jb .Lcontinue_32_32
 .p2align 4
.Lcontinue_32_48:
 mov (%rsi), %ecx
 cmp %ecx, (%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 4(%rsi), %ecx
 cmp %ecx, 4(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 8(%rsi), %ecx
 cmp %ecx, 8(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 12(%rsi), %ecx
 cmp %ecx, 12(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 16(%rsi), %ecx
 cmp %ecx, 16(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 20(%rsi), %ecx
 cmp %ecx, 20(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 24(%rsi), %ecx
 cmp %ecx, 24(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 28(%rsi), %ecx
 cmp %ecx, 28(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 movdqu 32(%rdi), %xmm1
 movdqu 32(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 movdqu 48(%rdi), %xmm1
 movdqu 48(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_48
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_32_48
 .p2align 4
.Lcontinue_16:
 and $15, %ch
 jz .Lcontinue_16_00
 cmp $16, %eax
 jb .Lcontinue_0_16
 cmp $32, %eax
 jb .Lcontinue_16_16
 cmp $48, %eax
 jb .Lcontinue_16_32
 .p2align 4
.Lcontinue_16_48:
 mov (%rsi), %ecx
 cmp %ecx, (%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 4(%rsi), %ecx
 cmp %ecx, 4(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 8(%rsi), %ecx
 cmp %ecx, 8(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 12(%rsi), %ecx
 cmp %ecx, 12(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 movdqu 16(%rdi), %xmm1
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 mov 32(%rsi), %ecx
 cmp %ecx, 32(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 36(%rsi), %ecx
 cmp %ecx, 36(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 40(%rsi), %ecx
 cmp %ecx, 40(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 44(%rsi), %ecx
 cmp %ecx, 44(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 movdqu 48(%rdi), %xmm1
 movdqu 48(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_48
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_16_48
 .p2align 4
.Lcontinue_00_00:
 movdqa (%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd (%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqa 16(%rdi), %xmm3
 pcmpeqd %xmm3, %xmm0
 pcmpeqd 16(%rsi), %xmm3
 psubb %xmm0, %xmm3
 pmovmskb %xmm3, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqa 32(%rdi), %xmm5
 pcmpeqd %xmm5, %xmm0
 pcmpeqd 32(%rsi), %xmm5
 psubb %xmm0, %xmm5
 pmovmskb %xmm5, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 movdqa 48(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 48(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_48
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_00_00
 .p2align 4
.Lcontinue_00_32:
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd (%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 add $16, %rsi
 add $16, %rdi
 jmp .Lcontinue_00_48
 .p2align 4
.Lcontinue_00_16:
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd (%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd 16(%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 add $32, %rsi
 add $32, %rdi
 jmp .Lcontinue_00_48
 .p2align 4
.Lcontinue_00_0:
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd (%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd 16(%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rsi), %xmm2
 pcmpeqd %xmm2, %xmm0
 pcmpeqd 32(%rdi), %xmm2
 psubb %xmm0, %xmm2
 pmovmskb %xmm2, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 add $48, %rsi
 add $48, %rdi
 jmp .Lcontinue_00_48
 .p2align 4
.Lcontinue_48_00:
 pcmpeqd (%rsi), %xmm0
 mov (%rdi), %eax
 pmovmskb %xmm0, %ecx
 test %ecx, %ecx
 jnz .Lless4_double_words1
 cmp (%rsi), %eax
 jne .Lnequal
 mov 4(%rdi), %eax
 cmp 4(%rsi), %eax
 jne .Lnequal
 mov 8(%rdi), %eax
 cmp 8(%rsi), %eax
 jne .Lnequal
 mov 12(%rdi), %eax
 cmp 12(%rsi), %eax
 jne .Lnequal
 movdqu 16(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 16(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 32(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 movdqu 48(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 48(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_48
 add $64, %rsi
 add $64, %rdi
 jmp .Lcontinue_48_00
 .p2align 4
.Lcontinue_32_00:
 movdqu (%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd (%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 add $16, %rsi
 add $16, %rdi
 jmp .Lcontinue_48_00
 .p2align 4
.Lcontinue_16_00:
 movdqu (%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd (%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 16(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 add $32, %rsi
 add $32, %rdi
 jmp .Lcontinue_48_00
 .p2align 4
.Lcontinue_0_00:
 movdqu (%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd (%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 16(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rdi), %xmm1
 pcmpeqd %xmm1, %xmm0
 pcmpeqd 32(%rsi), %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 add $48, %rsi
 add $48, %rdi
 jmp .Lcontinue_48_00
 .p2align 4
.Lcontinue_32_32:
 movdqu (%rdi), %xmm1
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 add $16, %rsi
 add $16, %rdi
 jmp .Lcontinue_48_48
 .p2align 4
.Lcontinue_16_16:
 movdqu (%rdi), %xmm1
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rdi), %xmm3
 movdqu 16(%rsi), %xmm4
 pcmpeqd %xmm3, %xmm0
 pcmpeqd %xmm4, %xmm3
 psubb %xmm0, %xmm3
 pmovmskb %xmm3, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 add $32, %rsi
 add $32, %rdi
 jmp .Lcontinue_48_48
 .p2align 4
.Lcontinue_0_0:
 movdqu (%rdi), %xmm1
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rdi), %xmm3
 movdqu 16(%rsi), %xmm4
 pcmpeqd %xmm3, %xmm0
 pcmpeqd %xmm4, %xmm3
 psubb %xmm0, %xmm3
 pmovmskb %xmm3, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 movdqu 32(%rdi), %xmm1
 movdqu 32(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_32
 add $48, %rsi
 add $48, %rdi
 jmp .Lcontinue_48_48
 .p2align 4
.Lcontinue_0_16:
 movdqu (%rdi), %xmm1
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 movdqu 16(%rdi), %xmm1
 movdqu 16(%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words_16
 add $32, %rsi
 add $32, %rdi
 jmp .Lcontinue_32_48
 .p2align 4
.Lcontinue_0_32:
 movdqu (%rdi), %xmm1
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 add $16, %rsi
 add $16, %rdi
 jmp .Lcontinue_16_48
 .p2align 4
.Lcontinue_16_32:
 movdqu (%rdi), %xmm1
 movdqu (%rsi), %xmm2
 pcmpeqd %xmm1, %xmm0
 pcmpeqd %xmm2, %xmm1
 psubb %xmm0, %xmm1
 pmovmskb %xmm1, %edx
 sub $0xffff, %edx
 jnz .Lless4_double_words
 add $16, %rsi
 add $16, %rdi
 jmp .Lcontinue_32_48
 .p2align 4
.Lless4_double_words1:
 cmp (%rsi), %eax
 jne .Lnequal
 test %eax, %eax
 jz .Lequal
 mov 4(%rsi), %ecx
 cmp %ecx, 4(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 8(%rsi), %ecx
 cmp %ecx, 8(%rdi)
 jne .Lnequal
 test %ecx, %ecx
 jz .Lequal
 mov 12(%rsi), %ecx
 cmp %ecx, 12(%rdi)
 jne .Lnequal
 xor %eax, %eax
 ret
 .p2align 4
.Lless4_double_words:
 xor %eax, %eax
 test %dl, %dl
 jz .Lnext_two_double_words
 and $15, %dl
 jz .Lsecond_double_word
 mov (%rdi), %eax
 cmp (%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lsecond_double_word:
 mov 4(%rdi), %eax
 cmp 4(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lnext_two_double_words:
 and $15, %dh
 jz .Lfourth_double_word
 mov 8(%rdi), %eax
 cmp 8(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lfourth_double_word:
 mov 12(%rdi), %eax
 cmp 12(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lless4_double_words_16:
 xor %eax, %eax
 test %dl, %dl
 jz .Lnext_two_double_words_16
 and $15, %dl
 jz .Lsecond_double_word_16
 mov 16(%rdi), %eax
 cmp 16(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lsecond_double_word_16:
 mov 20(%rdi), %eax
 cmp 20(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lnext_two_double_words_16:
 and $15, %dh
 jz .Lfourth_double_word_16
 mov 24(%rdi), %eax
 cmp 24(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lfourth_double_word_16:
 mov 28(%rdi), %eax
 cmp 28(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lless4_double_words_32:
 xor %eax, %eax
 test %dl, %dl
 jz .Lnext_two_double_words_32
 and $15, %dl
 jz .Lsecond_double_word_32
 mov 32(%rdi), %eax
 cmp 32(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lsecond_double_word_32:
 mov 36(%rdi), %eax
 cmp 36(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lnext_two_double_words_32:
 and $15, %dh
 jz .Lfourth_double_word_32
 mov 40(%rdi), %eax
 cmp 40(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lfourth_double_word_32:
 mov 44(%rdi), %eax
 cmp 44(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lless4_double_words_48:
 xor %eax, %eax
 test %dl, %dl
 jz .Lnext_two_double_words_48
 and $15, %dl
 jz .Lsecond_double_word_48
 mov 48(%rdi), %eax
 cmp 48(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lsecond_double_word_48:
 mov 52(%rdi), %eax
 cmp 52(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lnext_two_double_words_48:
 and $15, %dh
 jz .Lfourth_double_word_48
 mov 56(%rdi), %eax
 cmp 56(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lfourth_double_word_48:
 mov 60(%rdi), %eax
 cmp 60(%rsi), %eax
 jne .Lnequal
 ret
 .p2align 4
.Lnequal:
 mov $1, %eax
 jg .Lnequal_bigger
 neg %eax
.Lnequal_bigger:
 ret
 .p2align 4
.Lequal:
 xor %rax, %rax
 ret
.size __wcscmp,.-__wcscmp
.globl __GI___wcscmp
.set __GI___wcscmp,__wcscmp
.weak wcscmp
wcscmp = __wcscmp
