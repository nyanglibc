	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___wcscpy
	.hidden	__GI___wcscpy
	.type	__GI___wcscpy, @function
__GI___wcscpy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__wcslen@PLT
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	leaq	1(%rax), %rdx
	jmp	__wmemcpy
	.size	__GI___wcscpy, .-__GI___wcscpy
	.weak	wcscpy
	.set	wcscpy,__GI___wcscpy
	.globl	__wcscpy
	.set	__wcscpy,__GI___wcscpy
	.hidden	__wmemcpy
