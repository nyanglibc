	.text
	.p2align 4,,15
	.globl	__wcstold_internal
	.hidden	__wcstold_internal
	.type	__wcstold_internal, @function
__wcstold_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____wcstold_l_internal
	.size	__wcstold_internal, .-__wcstold_internal
	.p2align 4,,15
	.weak	wcstold
	.hidden	wcstold
	.type	wcstold, @function
wcstold:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____wcstold_l_internal
	.size	wcstold, .-wcstold
	.weak	wcstof64x
	.set	wcstof64x,wcstold
	.hidden	____wcstold_l_internal
