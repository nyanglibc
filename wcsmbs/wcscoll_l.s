	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../string/strcoll_l.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"((uintptr_t) table) % __alignof__ (table[0]) == 0"
	.align 8
.LC2:
	.string	"((uintptr_t) weights) % __alignof__ (weights[0]) == 0"
	.align 8
.LC3:
	.string	"((uintptr_t) extra) % __alignof__ (extra[0]) == 0"
	.align 8
.LC4:
	.string	"((uintptr_t) indirect) % __alignof__ (indirect[0]) == 0"
	.text
	.p2align 4,,15
	.globl	__wcscoll_l
	.hidden	__wcscoll_l
	.type	__wcscoll_l, @function
__wcscoll_l:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$232, %rsp
	movq	24(%rdx), %rax
	movl	64(%rax), %ebx
	testq	%rbx, %rbx
	movl	%ebx, 12(%rsp)
	movq	%rbx, 200(%rsp)
	je	.L214
	movl	(%rdi), %ecx
	movl	(%rsi), %edx
	testl	%ecx, %ecx
	je	.L3
	testl	%edx, %edx
	je	.L3
	movq	72(%rax), %rbx
	movq	%rsi, 216(%rsp)
	movq	144(%rax), %r14
	movq	152(%rax), %rsi
	movq	%rdi, 208(%rsp)
	movq	%rbx, 56(%rsp)
	movq	136(%rax), %rbx
	movq	160(%rax), %rax
	movq	%rsi, 24(%rsp)
	movq	%rbx, (%rsp)
	andl	$3, %ebx
	movq	%rax, 96(%rsp)
	jne	.L215
	testb	$3, %r14b
	jne	.L216
	testb	$3, 24(%rsp)
	jne	.L217
	testb	$3, 96(%rsp)
	jne	.L218
	movq	$0, 176(%rsp)
	movq	$0, 72(%rsp)
	xorl	%eax, %eax
	movq	$0, 64(%rsp)
	movb	$0, 198(%rsp)
.L109:
	imulq	200(%rsp), %rax
	movq	176(%rsp), %rbx
	movq	56(%rsp), %rdx
	xorl	%r8d, %r8d
	movq	216(%rsp), %rcx
	xorl	%ebp, %ebp
	movq	$-1, 48(%rsp)
	movq	$0, 168(%rsp)
	movl	$0, 164(%rsp)
	addq	%rbx, %rdx
	movl	%ebx, %r15d
	xorl	%ebx, %ebx
	movq	%rcx, %r13
	movl	%r8d, %ecx
	movq	$0, 152(%rsp)
	movl	%ebp, 160(%rsp)
	movzbl	(%rdx,%rax), %eax
	movq	208(%rsp), %rdx
	andl	$4, %eax
	testl	%ecx, %ecx
	movb	%al, 199(%rsp)
	movq	$-1, %rax
	movq	%rax, 32(%rsp)
	movq	%rax, 40(%rsp)
	movq	%rax, 16(%rsp)
	jne	.L113
.L236:
	movq	%r13, 120(%rsp)
	movq	152(%rsp), %r13
	movq	%rdx, %r12
	movq	%rdx, 112(%rsp)
	movq	$0, 88(%rsp)
	.p2align 4,,10
	.p2align 3
.L56:
	movq	40(%rsp), %rbp
	addq	$1, 88(%rsp)
	cmpq	$-1, %rbp
	je	.L114
	cmpq	%rbp, 16(%rsp)
	je	.L13
	movq	128(%rsp), %r12
	movl	%ebx, %edi
	jbe	.L15
	movq	%r14, 80(%rsp)
	movq	%r13, 104(%rsp)
	movq	%rbp, %r14
	movq	%r12, %r13
	movq	(%rsp), %rbp
	movq	16(%rsp), %r12
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%rbx, %r13
.L17:
	addq	$1, %r14
	cmpq	%r14, %r12
	je	.L219
.L14:
	movl	0(%r13), %esi
	movq	%rbp, %rdi
	leaq	4(%r13), %rbx
	call	__collidx_table_lookup
	testl	%eax, %eax
	jns	.L117
	movq	24(%rsp), %rsi
	negl	%eax
	cltq
	leaq	(%rsi,%rax,4), %rdx
.L18:
	movl	(%rdx), %eax
	leaq	8(%rdx), %r8
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	js	.L220
.L19:
	cmpq	$-2, %rcx
	movq	$-2, %rsi
	cmovbe	%rcx, %rsi
	testq	%rcx, %rcx
	je	.L23
	movl	4(%r13), %edi
	cmpl	%edi, 8(%rdx)
	jne	.L24
	xorl	%edi, %edi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L27:
	movl	4(%r13,%rdi,4), %edx
	cmpl	%edx, (%r8,%rdi,4)
	jne	.L26
.L25:
	addq	$1, %rdi
	cmpq	%rsi, %rdi
	jb	.L27
.L26:
	cmpq	%rdi, %rcx
	leaq	0(,%rcx,4), %rdx
	je	.L221
.L28:
	addq	%r8, %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %r8
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	jns	.L19
.L220:
	movq	%rcx, %rsi
	subq	$1, %rsi
	je	.L20
	movl	8(%rdx), %edi
	cmpl	%edi, 4(%r13)
	jne	.L21
	xorl	%edx, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L30:
	movl	4(%r13,%rdx,4), %edi
	cmpl	%edi, (%r8,%rdx,4)
	jne	.L21
.L22:
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L30
.L20:
	leaq	0(,%rcx,4), %r10
	movl	0(%r13,%rcx,4), %edx
	salq	$3, %rcx
	movl	-4(%r8,%r10), %esi
	cmpl	%edx, %esi
	jg	.L209
	cmpl	-4(%r8,%rcx), %edx
	jg	.L209
	subl	%esi, %edx
	negl	%eax
	leaq	(%rbx,%r10), %r13
	movq	96(%rsp), %rbx
	movslq	%edx, %rdx
	cltq
	addq	$1, %r14
	addq	%rdx, %rax
	cmpq	%r14, %r12
	movl	(%rbx,%rax,4), %eax
	jne	.L14
	.p2align 4,,10
	.p2align 3
.L219:
	movq	80(%rsp), %r14
	movq	104(%rsp), %r13
	andl	$16777215, %eax
	movl	%eax, %edi
.L15:
	subq	$1, 16(%rsp)
	movq	112(%rsp), %r12
.L16:
	movslq	%edi, %rax
	testl	%r15d, %r15d
	leal	1(%rdi), %ebx
	movl	(%r14,%rax,4), %ecx
	je	.L54
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L55:
	addl	%ecx, %ebx
	addl	$1, %eax
	movslq	%ebx, %rdx
	addl	$1, %ebx
	cmpl	%r15d, %eax
	movl	(%r14,%rdx,4), %ecx
	jne	.L55
.L54:
	testl	%ecx, %ecx
	je	.L56
	movq	%r13, 152(%rsp)
	movq	120(%rsp), %r13
	movq	%r12, %rdx
.L11:
	movl	164(%rsp), %eax
	testl	%eax, %eax
	jne	.L122
.L237:
	movq	%rdx, 184(%rsp)
	movq	168(%rsp), %rbp
	movl	160(%rsp), %edx
	movl	%ebx, 192(%rsp)
	movq	%r13, %rbx
	movq	%r13, 120(%rsp)
	movq	$0, 80(%rsp)
	movl	%ecx, 164(%rsp)
	.p2align 4,,10
	.p2align 3
.L102:
	movq	48(%rsp), %rax
	addq	$1, 80(%rsp)
	cmpq	$-1, %rax
	je	.L123
	cmpq	%rax, 32(%rsp)
	je	.L59
	movq	136(%rsp), %r13
	movslq	%edx, %rcx
	jbe	.L61
	movq	%r14, 104(%rsp)
	movq	%rbp, 112(%rsp)
	movq	%r13, %r14
	movq	%rax, %rbp
	movq	(%rsp), %r12
	movq	32(%rsp), %r13
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rbx, %r14
.L64:
	addq	$1, %rbp
	cmpq	%r13, %rbp
	je	.L222
.L60:
	movl	(%r14), %esi
	movq	%r12, %rdi
	leaq	4(%r14), %rbx
	call	__collidx_table_lookup
	testl	%eax, %eax
	jns	.L126
	movq	24(%rsp), %rsi
	negl	%eax
	cltq
	leaq	(%rsi,%rax,4), %rdx
.L65:
	movl	(%rdx), %eax
	leaq	8(%rdx), %r8
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	js	.L223
.L66:
	cmpq	$-2, %rcx
	movq	$-2, %rsi
	cmovbe	%rcx, %rsi
	testq	%rcx, %rcx
	je	.L70
	movl	8(%rdx), %edi
	cmpl	%edi, 4(%r14)
	jne	.L71
	xorl	%edi, %edi
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L74:
	movl	4(%r14,%rdi,4), %edx
	cmpl	%edx, (%r8,%rdi,4)
	jne	.L73
.L72:
	addq	$1, %rdi
	cmpq	%rsi, %rdi
	jb	.L74
.L73:
	cmpq	%rdi, %rcx
	leaq	0(,%rcx,4), %rdx
	je	.L224
.L75:
	addq	%r8, %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %r8
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	jns	.L66
.L223:
	movq	%rcx, %rsi
	subq	$1, %rsi
	je	.L67
	movl	8(%rdx), %edi
	cmpl	%edi, 4(%r14)
	jne	.L68
	xorl	%edx, %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L77:
	movl	4(%r14,%rdx,4), %edi
	cmpl	%edi, (%r8,%rdx,4)
	jne	.L68
.L69:
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L77
.L67:
	leaq	0(,%rcx,4), %r10
	movl	(%r14,%rcx,4), %edx
	salq	$3, %rcx
	movl	-4(%r8,%r10), %esi
	cmpl	%edx, %esi
	jg	.L211
	cmpl	-4(%r8,%rcx), %edx
	jg	.L211
	subl	%esi, %edx
	negl	%eax
	leaq	(%rbx,%r10), %r14
	movq	96(%rsp), %rbx
	movslq	%edx, %rdx
	cltq
	addq	$1, %rbp
	addq	%rdx, %rax
	cmpq	%r13, %rbp
	movl	(%rbx,%rax,4), %eax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L222:
	movq	104(%rsp), %r14
	movq	112(%rsp), %rbp
	andl	$16777215, %eax
	movslq	%eax, %rcx
.L61:
	subq	$1, 32(%rsp)
	movq	120(%rsp), %rbx
.L62:
	testl	%r15d, %r15d
	leal	1(%rcx), %edx
	movl	(%r14,%rcx,4), %ecx
	je	.L100
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L101:
	addl	%ecx, %edx
	addl	$1, %eax
	movslq	%edx, %rcx
	addl	$1, %edx
	cmpl	%r15d, %eax
	movl	(%r14,%rcx,4), %ecx
	jne	.L101
.L100:
	testl	%ecx, %ecx
	je	.L102
	movl	%ecx, %r12d
	movl	164(%rsp), %ecx
	movq	%rbx, %r13
	movl	%edx, 160(%rsp)
	movq	%rbp, 168(%rsp)
	movq	184(%rsp), %rdx
	movl	192(%rsp), %ebx
	testl	%ecx, %ecx
	je	.L225
.L197:
	cmpb	$0, 199(%rsp)
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
	setne	%r9b
	cmpq	%rdi, %rsi
	je	.L104
	testb	%r9b, %r9b
	je	.L104
	cmpq	%rdi, %rsi
	movl	$-1, %eax
	ja	.L112
.L1:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	(%r8,%rcx,8), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L209:
	leaq	(%r8,%rcx), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	(%r8,%rcx,8), %rdx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	(%r8,%rcx), %rdx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L59:
	cmpq	%rbp, 72(%rsp)
	jbe	.L226
	movslq	148(%rsp), %rcx
	movq	$-1, 48(%rsp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L13:
	cmpq	%r13, 64(%rsp)
	jbe	.L227
	movl	144(%rsp), %edi
	movq	$-1, 40(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L114:
	movq	64(%rsp), %r8
	movq	%r14, 40(%rsp)
	movl	%ebx, %edi
	movq	%r12, %r14
	movzbl	198(%rsp), %ebp
	movq	%r8, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%r13, %r14
.L33:
	movl	%eax, %esi
	movl	%eax, %edx
	movq	56(%rsp), %rcx
	sarl	$24, %edx
	andl	$16777215, %esi
	testq	%r12, %r12
	cmove	%edx, %ebp
	shrl	$24, %eax
	leaq	1(%r12), %r13
	imull	12(%rsp), %eax
	movl	%ebx, %edi
	addl	%r15d, %eax
	cltq
	testb	$2, (%rcx,%rax)
	je	.L119
	movl	%esi, %ebx
	movq	%r13, %r12
.L12:
	movl	(%r14), %esi
	testl	%esi, %esi
	je	.L228
	movq	(%rsp), %rdi
	leaq	4(%r14), %r13
	call	__collidx_table_lookup
	testl	%eax, %eax
	jns	.L118
	movq	24(%rsp), %rsi
	negl	%eax
	cltq
	leaq	(%rsi,%rax,4), %rdx
.L34:
	movl	(%rdx), %eax
	leaq	8(%rdx), %r8
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	js	.L229
.L35:
	cmpq	$-2, %rcx
	movq	$-2, %rsi
	cmovbe	%rcx, %rsi
	testq	%rcx, %rcx
	je	.L39
	movl	4(%r14), %edi
	cmpl	%edi, 8(%rdx)
	jne	.L40
	xorl	%edi, %edi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	movl	4(%r14,%rdi,4), %edx
	cmpl	%edx, (%r8,%rdi,4)
	jne	.L42
.L41:
	addq	$1, %rdi
	cmpq	%rdi, %rsi
	ja	.L43
.L42:
	cmpq	%rdi, %rcx
	leaq	0(,%rcx,4), %rdx
	je	.L230
.L44:
	addq	%r8, %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %r8
	movslq	4(%rdx), %rcx
	testl	%eax, %eax
	jns	.L35
.L229:
	movq	%rcx, %rsi
	subq	$1, %rsi
	je	.L36
	movl	8(%rdx), %edi
	cmpl	%edi, 4(%r14)
	jne	.L37
	xorl	%edx, %edx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L46:
	movl	4(%r14,%rdx,4), %edi
	cmpl	%edi, (%r8,%rdx,4)
	jne	.L37
.L38:
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L46
.L36:
	leaq	0(,%rcx,4), %r10
	movl	(%r14,%rcx,4), %edx
	salq	$3, %rcx
	movl	-4(%r8,%r10), %esi
	cmpl	%edx, %esi
	jg	.L210
	cmpl	-4(%r8,%rcx), %edx
	jg	.L210
	subl	%esi, %edx
	negl	%eax
	movq	96(%rsp), %rsi
	movslq	%edx, %rdx
	cltq
	leaq	0(%r13,%r10), %r14
	addq	%rdx, %rax
	movl	(%rsi,%rax,4), %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	(%r8,%rcx,8), %rdx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	(%r8,%rcx), %rdx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L123:
	movq	72(%rsp), %r13
	movq	%r14, 48(%rsp)
	movq	%rbx, %r14
	movq	56(%rsp), %rbx
	movq	%r13, %r12
	movl	%edx, %r13d
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rbp, %r14
.L80:
	movl	%eax, %esi
	shrl	$24, %eax
	leaq	1(%r12), %rbp
	imull	12(%rsp), %eax
	andl	$16777215, %esi
	movl	%r13d, %edx
	addl	%r15d, %eax
	cltq
	testb	$2, (%rbx,%rax)
	je	.L128
	movl	%esi, %r13d
	movq	%rbp, %r12
.L58:
	movl	(%r14), %esi
	testl	%esi, %esi
	je	.L231
	movq	(%rsp), %rdi
	leaq	4(%r14), %rbp
	call	__collidx_table_lookup
	testl	%eax, %eax
	jns	.L127
	movq	24(%rsp), %rsi
	negl	%eax
	cltq
	leaq	(%rsi,%rax,4), %rdx
.L81:
	movl	(%rdx), %eax
	leaq	8(%rdx), %rdi
	movslq	4(%rdx), %rsi
	testl	%eax, %eax
	js	.L232
.L82:
	cmpq	$-2, %rsi
	movq	$-2, %r8
	cmovbe	%rsi, %r8
	testq	%rsi, %rsi
	je	.L86
	movl	8(%rdx), %ecx
	cmpl	%ecx, 4(%r14)
	jne	.L87
	xorl	%ecx, %ecx
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L90:
	movl	4(%r14,%rcx,4), %edx
	cmpl	%edx, (%rdi,%rcx,4)
	jne	.L89
.L88:
	addq	$1, %rcx
	cmpq	%rcx, %r8
	ja	.L90
.L89:
	cmpq	%rcx, %rsi
	leaq	0(,%rsi,4), %rdx
	je	.L233
.L91:
	addq	%rdi, %rdx
	movl	(%rdx), %eax
	leaq	8(%rdx), %rdi
	movslq	4(%rdx), %rsi
	testl	%eax, %eax
	jns	.L82
.L232:
	movq	%rsi, %rcx
	subq	$1, %rcx
	je	.L83
	movl	8(%rdx), %edx
	cmpl	%edx, 4(%r14)
	jne	.L84
	xorl	%edx, %edx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L93:
	movl	4(%r14,%rdx,4), %r9d
	cmpl	%r9d, (%rdi,%rdx,4)
	jne	.L84
.L85:
	addq	$1, %rdx
	cmpq	%rcx, %rdx
	jne	.L93
.L83:
	leaq	0(,%rsi,4), %rdx
	movl	(%r14,%rsi,4), %r10d
	leaq	0(,%rsi,8), %rcx
	movl	-4(%rdi,%rdx), %r11d
	cmpl	%r10d, %r11d
	jg	.L212
	cmpl	-4(%rdi,%rcx), %r10d
	jg	.L212
	subl	%r11d, %r10d
	negl	%eax
	movq	96(%rsp), %rsi
	leaq	0(%rbp,%rdx), %r14
	cltq
	movslq	%r10d, %rdx
	addq	%rdx, %rax
	movl	(%rsi,%rax,4), %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	leaq	(%rdi,%rsi,8), %rdx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	(%rdi,%rcx), %rdx
	jmp	.L81
.L221:
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	(%rbx,%rcx), %r13
	jmp	.L17
.L224:
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	(%rbx,%rcx), %r14
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	0(,%rcx,4), %rdx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	0(,%rcx,4), %rdx
	jmp	.L28
.L231:
	movq	%r14, %rbx
	movq	48(%rsp), %r14
	movslq	%r13d, %rcx
	movq	%r12, %r13
.L96:
	movq	72(%rsp), %rax
	cmpq	%rax, %rbp
	ja	.L98
	cmpq	%r13, %rbp
	je	.L129
	cmpq	%rax, %rbp
	jb	.L129
	movq	%r13, 72(%rsp)
	movq	$-1, 48(%rsp)
	jmp	.L62
.L228:
	movq	%r12, %r8
	movq	%r14, %r12
	movq	40(%rsp), %r14
	movb	%bpl, 198(%rsp)
.L50:
	movq	64(%rsp), %rax
	cmpq	%r13, %rax
	jb	.L52
	cmpq	%r8, %r13
	je	.L120
	cmpq	%r13, %rax
	ja	.L120
	movl	%ebx, %edi
	movq	%r8, 64(%rsp)
	movq	$-1, 40(%rsp)
	jmp	.L16
.L230:
	movq	%rdx, %rcx
.L39:
	leaq	0(%r13,%rcx), %r14
	jmp	.L33
.L233:
	movq	%rdx, %rsi
.L86:
	leaq	0(%rbp,%rsi), %r14
	jmp	.L80
.L52:
	leaq	-1(%r13), %rax
	cmpq	%r8, %r13
	movq	%rax, 16(%rsp)
	jnb	.L53
	cmpq	%rax, 64(%rsp)
	jb	.L234
	movq	112(%rsp), %rax
	movl	%ebx, 144(%rsp)
	movq	%r12, 112(%rsp)
	movq	%rax, 128(%rsp)
	movq	64(%rsp), %rax
	movq	%r8, 64(%rsp)
	movq	%rax, 40(%rsp)
	jmp	.L16
.L98:
	leaq	-1(%rbp), %rax
	cmpq	%r13, %rbp
	movq	%rax, 32(%rsp)
	jnb	.L99
	cmpq	%rax, 72(%rsp)
	jb	.L235
	movq	120(%rsp), %rax
	movl	%ecx, 148(%rsp)
	movslq	%edx, %rcx
	movq	%rbx, 120(%rsp)
	movq	%rax, 136(%rsp)
	movq	72(%rsp), %rax
	movq	%r13, 72(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L62
.L119:
	movq	%r12, %r8
	movq	%r13, %rax
	movq	%r14, %r12
	movq	%r8, %r13
	movb	%bpl, 198(%rsp)
	movq	40(%rsp), %r14
	movl	%esi, %ebx
	movq	%rax, %r8
	jmp	.L50
.L128:
	movq	%rbp, %rax
	movq	%r14, %rbx
	movslq	%esi, %rcx
	movq	48(%rsp), %r14
	movq	%r12, %rbp
	movq	%rax, %r13
	jmp	.L96
.L40:
	leaq	0(,%rcx,4), %rdx
	jmp	.L44
.L87:
	leaq	0(,%rsi,4), %rdx
	jmp	.L91
.L235:
	leaq	-2(%rbp), %rax
	movl	%ecx, 148(%rsp)
	movslq	%edx, %rcx
	movq	%rax, 32(%rsp)
.L99:
	movq	120(%rsp), %rax
	movq	%rbx, 120(%rsp)
	movq	%rax, 136(%rsp)
	movq	72(%rsp), %rax
	movq	%r13, 72(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L62
.L234:
	leaq	-2(%r13), %rax
	movl	%ebx, 144(%rsp)
	movl	%edi, %ebx
	movq	%rax, 16(%rsp)
.L53:
	movq	112(%rsp), %rax
	movl	%ebx, %edi
	movq	%r12, 112(%rsp)
	movq	%rax, 128(%rsp)
	movq	64(%rsp), %rax
	movq	%r8, 64(%rsp)
	movq	%rax, 40(%rsp)
	jmp	.L16
.L104:
	movslq	160(%rsp), %rsi
	movslq	%ebx, %rax
	subq	%rsi, %rax
	leaq	(%r14,%rax,4), %r8
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L105:
	subl	$1, %ecx
	addl	$1, %ebx
	leal	1(%rsi), %ebp
	subl	$1, %r12d
	addq	$1, %rsi
	testl	%ecx, %ecx
	jle	.L133
	testl	%r12d, %r12d
	jle	.L133
.L107:
	movl	(%r8,%rsi,4), %eax
	movl	(%r14,%rsi,4), %edi
	cmpl	%edi, %eax
	je	.L105
	subl	%edi, %eax
	movl	%esi, 160(%rsp)
	jne	.L1
.L106:
	testl	%ecx, %ecx
	movl	%r12d, 164(%rsp)
	je	.L236
.L113:
	movl	164(%rsp), %eax
	movq	$0, 88(%rsp)
	testl	%eax, %eax
	je	.L237
.L122:
	testl	%ecx, %ecx
	movl	164(%rsp), %r12d
	movq	$0, 80(%rsp)
	jne	.L197
.L225:
	orl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L226:
	movl	164(%rsp), %r13d
.L63:
	testl	%r13d, %r13d
	jne	.L112
	cmpq	$0, 176(%rsp)
	je	.L238
.L103:
	addq	$1, 176(%rsp)
	movzbl	198(%rsp), %eax
	movq	176(%rsp), %rbx
	cmpq	%rbx, 200(%rsp)
	jne	.L109
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L227:
	movq	16(%rsp), %rax
	movq	%r13, 152(%rsp)
	movq	%r12, %rdx
	movq	120(%rsp), %r13
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	movq	%rax, 40(%rsp)
	jmp	.L11
.L133:
	cmpl	%r12d, %ecx
	movl	%ebp, 160(%rsp)
	je	.L106
	testb	%r9b, %r9b
	je	.L106
	movl	%ecx, %eax
	subl	%r12d, %eax
	jmp	.L1
.L112:
	movl	$1, %eax
	jmp	.L1
.L129:
	movq	%r13, %r8
	movl	164(%rsp), %r13d
	movq	%r8, 72(%rsp)
	jmp	.L63
.L120:
	movq	64(%rsp), %rax
	movq	%r13, 152(%rsp)
	movq	%r12, %rdx
	movq	120(%rsp), %r13
	xorl	%ecx, %ecx
	movq	%r8, 64(%rsp)
	movq	%rax, 40(%rsp)
	jmp	.L11
.L214:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__wcscmp
.L3:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setne	%al
	testl	%edx, %edx
	setne	%dl
	movzbl	%dl, %edx
	subl	%edx, %eax
	jmp	.L1
.L215:
	leaq	__PRETTY_FUNCTION__.8768(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$288, %edx
	call	__assert_fail
.L218:
	leaq	__PRETTY_FUNCTION__.8768(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$291, %edx
	call	__assert_fail
.L238:
	movq	216(%rsp), %rsi
	movq	208(%rsp), %rdi
	call	__wcscmp
	testl	%eax, %eax
	jne	.L103
	jmp	.L1
.L216:
	leaq	__PRETTY_FUNCTION__.8768(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$289, %edx
	call	__assert_fail
.L217:
	leaq	__PRETTY_FUNCTION__.8768(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$290, %edx
	call	__assert_fail
	.size	__wcscoll_l, .-__wcscoll_l
	.weak	wcscoll_l
	.set	wcscoll_l,__wcscoll_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8768, @object
	.size	__PRETTY_FUNCTION__.8768, 12
__PRETTY_FUNCTION__.8768:
	.string	"__wcscoll_l"
	.hidden	__assert_fail
	.hidden	__wcscmp
	.hidden	__collidx_table_lookup
