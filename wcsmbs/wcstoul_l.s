	.text
	.p2align 4,,15
	.globl	____wcstoul_l_internal
	.hidden	____wcstoul_l_internal
	.type	____wcstoul_l_internal, @function
____wcstoul_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %ebp
	movq	%r8, %r14
	subq	$56, %rsp
	testl	%ecx, %ecx
	movq	%rdi, 8(%rsp)
	movq	%rsi, 16(%rsp)
	jne	.L73
.L2:
	cmpl	$1, %ebp
	sete	%r12b
	cmpl	$36, %ebp
	seta	%al
	orb	%al, %r12b
	je	.L32
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r15d, %r15d
	movl	$22, %fs:(%rax)
.L1:
	addq	$56, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	8(%rsp), %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$4, %rbx
.L3:
	movl	(%rbx), %edi
	movq	%r14, %rsi
	call	__iswspace_l
	testl	%eax, %eax
	jne	.L5
	movl	(%rbx), %edx
	movl	%eax, %r8d
	testl	%edx, %edx
	je	.L33
	cmpl	$45, %edx
	je	.L74
	cmpl	$43, %edx
	movl	$0, 44(%rsp)
	jne	.L8
	movl	4(%rbx), %edx
	movl	%eax, 44(%rsp)
	addq	$4, %rbx
.L8:
	cmpl	$48, %edx
	je	.L75
	testl	%ebp, %ebp
	je	.L13
.L10:
	cmpl	$10, %ebp
	je	.L13
	leal	-2(%rbp), %eax
	xorl	%esi, %esi
.L12:
	leaq	__strtol_ul_max_tab(%rip), %rcx
	cltq
	testl	%edx, %edx
	movq	(%rcx,%rax,8), %rdi
	leaq	__strtol_ul_rem_tab(%rip), %rcx
	movzbl	(%rcx,%rax), %eax
	movq	%rdi, (%rsp)
	movl	%eax, 40(%rsp)
	je	.L6
	cmpq	%rbx, %rsi
	je	.L6
	movq	%rbx, %r14
	xorl	%r15d, %r15d
	movb	%r12b, 31(%rsp)
	movq	%rbx, 32(%rsp)
	movl	%ebp, %r12d
	movq	%r15, %rbx
	movl	%r13d, 24(%rsp)
	movq	%r14, %r15
	movq	%rsi, %rbp
	movl	%edx, %r14d
	movl	%r8d, %r13d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	24(%rsp), %r14d
	jne	.L41
	cmpb	$0, 31(%rsp)
	je	.L41
.L19:
	addq	$4, %r15
	movl	(%r15), %r14d
	cmpq	%r15, %rbp
	je	.L21
	testl	%r14d, %r14d
	je	.L21
.L24:
	leal	-48(%r14), %eax
	cmpl	$9, %eax
	ja	.L76
.L18:
	cmpl	%r12d, %eax
	jge	.L21
	cmpq	%rbx, (%rsp)
	jb	.L39
	jne	.L23
	cmpl	40(%rsp), %eax
	jbe	.L23
.L39:
	movl	$1, %r13d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L74:
	movl	4(%rbx), %edx
	movl	$1, 44(%rsp)
	addq	$4, %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r14d, %edi
	call	__iswalpha_l
	testl	%eax, %eax
	je	.L21
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r14d, %edi
	call	__towupper_l
	subl	$55, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L23:
	movslq	%r12d, %rdx
	imulq	%rbx, %rdx
	leaq	(%rax,%rdx), %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r15, %r14
	movq	%rbx, %r15
	movq	32(%rsp), %rbx
	movl	%r13d, %r8d
	cmpq	%rbx, %r14
	je	.L6
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.L26
	movq	%r14, (%rax)
.L26:
	testl	%r8d, %r8d
	jne	.L77
	movl	44(%rsp), %edx
	movq	%r15, %rax
	negq	%rax
	testl	%edx, %edx
	cmovne	%rax, %r15
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%r15, %r15
	jne	.L78
	movl	$8, %eax
	movl	$10, %ebp
	xorl	%esi, %esi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L33:
	movq	8(%rsp), %rbx
.L6:
	xorl	%r15d, %r15d
	cmpq	$0, 16(%rsp)
	je	.L1
	movq	%rbx, %rax
	subq	8(%rsp), %rax
	cmpq	$4, %rax
	jg	.L79
.L28:
	movq	16(%rsp), %rax
	movq	8(%rsp), %rcx
	xorl	%r15d, %r15d
	movq	%rcx, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L73:
	movq	8(%r8), %rdx
	movq	80(%rdx), %r15
	movzbl	(%r15), %eax
	subl	$1, %eax
	cmpb	$125, %al
	ja	.L30
	movl	96(%rdx), %r13d
	movl	$0, %eax
	testl	%r13d, %r13d
	cmove	%rax, %r15
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L75:
	testl	$-17, %ebp
	jne	.L10
	movl	4(%rbx), %edi
	leaq	_nl_C_locobj(%rip), %rsi
	movl	%r8d, (%rsp)
	call	__towupper_l
	cmpl	$88, %eax
	movl	(%rsp), %r8d
	je	.L11
	testl	%ebp, %ebp
	movl	(%rbx), %edx
	jne	.L10
	movl	$6, %eax
	movl	$8, %ebp
	xorl	%esi, %esi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	%edx, %r13d
	je	.L6
	testl	%edx, %edx
	movl	%edx, %r12d
	movq	%rbx, %r14
	je	.L14
	leaq	_nl_C_locobj(%rip), %rbp
	movq	%rbx, (%rsp)
	movl	%r8d, %r14d
	.p2align 4,,10
	.p2align 3
.L16:
	leal	-48(%r12), %eax
	cmpl	$9, %eax
	jbe	.L17
	cmpl	%r13d, %r12d
	je	.L17
	movq	%rbp, %rsi
	movl	%r12d, %edi
	call	__iswalpha_l
	testl	%eax, %eax
	je	.L70
	movq	%rbp, %rsi
	movl	%r12d, %edi
	call	__towupper_l
	subl	$55, %eax
	cmpl	$9, %eax
	jg	.L70
.L17:
	addq	$4, %rbx
	movl	(%rbx), %r12d
	testl	%r12d, %r12d
	jne	.L16
.L70:
	movl	%r14d, %r8d
	movq	%rbx, %r14
	movq	(%rsp), %rbx
.L14:
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r15, %rcx
	movq	%rbx, %rdi
	movl	%r8d, (%rsp)
	movl	$10, %ebp
	call	__correctly_grouped_prefixwc
	movl	$1, %r12d
	movq	%rax, %rsi
	movl	(%rbx), %edx
	movl	$8, %eax
	movl	(%rsp), %r8d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L79:
	movl	-4(%rbx), %edi
	leaq	_nl_C_locobj(%rip), %rsi
	call	__towupper_l
	cmpl	$88, %eax
	jne	.L28
	cmpl	$48, -8(%rbx)
	jne	.L28
	movq	16(%rsp), %rax
	subq	$4, %rbx
	movq	%rbx, (%rax)
	jmp	.L1
.L77:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r15
	movl	$34, %fs:(%rax)
	jmp	.L1
.L11:
	movl	8(%rbx), %edx
	movl	$14, %eax
	addq	$8, %rbx
	movl	$16, %ebp
	xorl	%esi, %esi
	jmp	.L12
	.size	____wcstoul_l_internal, .-____wcstoul_l_internal
	.globl	____wcstoull_l_internal
	.set	____wcstoull_l_internal,____wcstoul_l_internal
	.p2align 4,,15
	.weak	__wcstoul_l
	.hidden	__wcstoul_l
	.type	__wcstoul_l, @function
__wcstoul_l:
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	____wcstoul_l_internal
	.size	__wcstoul_l, .-__wcstoul_l
	.weak	wcstoull_l
	.set	wcstoull_l,__wcstoul_l
	.weak	__wcstoull_l
	.set	__wcstoull_l,__wcstoul_l
	.weak	wcstoul_l
	.set	wcstoul_l,__wcstoul_l
	.hidden	__correctly_grouped_prefixwc
	.hidden	__towupper_l
	.hidden	__iswalpha_l
	.hidden	_nl_C_locobj
	.hidden	__strtol_ul_rem_tab
	.hidden	__strtol_ul_max_tab
	.hidden	__iswspace_l
