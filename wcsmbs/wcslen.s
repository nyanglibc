 .text
.globl __wcslen
.type __wcslen,@function
.align 1<<4
__wcslen:
 cmpl $0, (%rdi)
 jz .Lexit_tail0
 cmpl $0, 4(%rdi)
 jz .Lexit_tail1
 cmpl $0, 8(%rdi)
 jz .Lexit_tail2
 cmpl $0, 12(%rdi)
 jz .Lexit_tail3
 cmpl $0, 16(%rdi)
 jz .Lexit_tail4
 cmpl $0, 20(%rdi)
 jz .Lexit_tail5
 cmpl $0, 24(%rdi)
 jz .Lexit_tail6
 cmpl $0, 28(%rdi)
 jz .Lexit_tail7
 pxor %xmm0, %xmm0
 lea 32(%rdi), %rax
 lea 16(%rdi), %rcx
 and $-16, %rax
 pcmpeqd (%rax), %xmm0
 pmovmskb %xmm0, %edx
 pxor %xmm1, %xmm1
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm1
 pmovmskb %xmm1, %edx
 pxor %xmm2, %xmm2
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm2
 pmovmskb %xmm2, %edx
 pxor %xmm3, %xmm3
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm0
 pmovmskb %xmm0, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm1
 pmovmskb %xmm1, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm2
 pmovmskb %xmm2, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm0
 pmovmskb %xmm0, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm1
 pmovmskb %xmm1, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm2
 pmovmskb %xmm2, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 pcmpeqd (%rax), %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea 16(%rax), %rax
 jnz .Lexit
 and $-0x40, %rax
 .p2align 4
.Laligned_64_loop:
 movaps (%rax), %xmm0
 movaps 16(%rax), %xmm1
 movaps 32(%rax), %xmm2
 movaps 48(%rax), %xmm6
 pminub %xmm1, %xmm0
 pminub %xmm6, %xmm2
 pminub %xmm0, %xmm2
 pcmpeqd %xmm3, %xmm2
 pmovmskb %xmm2, %edx
 test %edx, %edx
 lea 64(%rax), %rax
 jz .Laligned_64_loop
 pcmpeqd -64(%rax), %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea 48(%rcx), %rcx
 jnz .Lexit
 pcmpeqd %xmm1, %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea -16(%rcx), %rcx
 jnz .Lexit
 pcmpeqd -32(%rax), %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea -16(%rcx), %rcx
 jnz .Lexit
 pcmpeqd %xmm6, %xmm3
 pmovmskb %xmm3, %edx
 test %edx, %edx
 lea -16(%rcx), %rcx
 jnz .Lexit
 jmp .Laligned_64_loop
 .p2align 4
.Lexit:
 sub %rcx, %rax
 shr $2, %rax
 test %dl, %dl
 jz .Lexit_high
 mov %dl, %cl
 and $15, %cl
 jz .Lexit_1
 ret
 .p2align 4
.Lexit_high:
 mov %dh, %ch
 and $15, %ch
 jz .Lexit_3
 add $2, %rax
 ret
 .p2align 4
.Lexit_1:
 add $1, %rax
 ret
 .p2align 4
.Lexit_3:
 add $3, %rax
 ret
 .p2align 4
.Lexit_tail0:
 xor %rax, %rax
 ret
 .p2align 4
.Lexit_tail1:
 mov $1, %rax
 ret
 .p2align 4
.Lexit_tail2:
 mov $2, %rax
 ret
 .p2align 4
.Lexit_tail3:
 mov $3, %rax
 ret
 .p2align 4
.Lexit_tail4:
 mov $4, %rax
 ret
 .p2align 4
.Lexit_tail5:
 mov $5, %rax
 ret
 .p2align 4
.Lexit_tail6:
 mov $6, %rax
 ret
 .p2align 4
.Lexit_tail7:
 mov $7, %rax
 ret
.size __wcslen,.-__wcslen
.weak wcslen
wcslen = __wcslen
