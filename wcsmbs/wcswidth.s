	.text
	.p2align 4,,15
	.globl	wcswidth
	.type	wcswidth, @function
wcswidth:
	testq	%rsi, %rsi
	je	.L6
	movl	(%rdi), %edx
	testl	%edx, %edx
	je	.L6
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	160(%rax), %r9
	movl	%edx, %eax
	movl	(%r9), %r11d
	movl	4(%r9), %r10d
	movl	%r11d, %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	movl	$-1, %eax
	cmpl	%ecx, %r10d
	jbe	.L18
	addl	$5, %ecx
	movl	(%r9,%rcx,4), %r8d
	testl	%r8d, %r8d
	je	.L18
	pushq	%r14
	subq	$1, %rsi
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movl	8(%r9), %ebp
	movl	12(%r9), %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	addl	$5, %ecx
	movl	(%r9,%rcx,4), %r8d
	testl	%r8d, %r8d
	je	.L12
.L3:
	movl	%ebp, %ecx
	movl	%edx, %r14d
	shrl	%cl, %r14d
	movl	%r14d, %ecx
	andl	%ebx, %ecx
	leaq	(%r9,%rcx,4), %rcx
	movl	(%rcx,%r8), %ecx
	testl	%ecx, %ecx
	je	.L12
	andl	16(%r9), %edx
	addq	%r9, %rdx
	movzbl	(%rdx,%rcx), %edx
	cmpl	$255, %edx
	je	.L12
	subq	$1, %rsi
	addl	%edx, %eax
	addq	$4, %rdi
	cmpq	$-1, %rsi
	je	.L1
	movl	(%rdi), %edx
	testl	%edx, %edx
	je	.L1
	movl	%r11d, %ecx
	movl	%edx, %r8d
	shrl	%cl, %r8d
	cmpl	%r10d, %r8d
	movl	%r8d, %ecx
	jb	.L22
.L12:
	movl	$-1, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	rep ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.size	wcswidth, .-wcswidth
	.hidden	_nl_current_LC_CTYPE
