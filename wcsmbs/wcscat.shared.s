	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcscat
	.type	__wcscat, @function
__wcscat:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	__wcslen@PLT
	leaq	(%rbx,%rax,4), %rdi
	movq	%rbp, %rsi
	call	__GI___wcscpy
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__wcscat, .-__wcscat
	.weak	wcscat
	.set	wcscat,__wcscat
