	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mbsinit
	.type	__mbsinit, @function
__mbsinit:
	testq	%rdi, %rdi
	movl	$1, %eax
	je	.L1
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
.L1:
	rep ret
	.size	__mbsinit, .-__mbsinit
	.weak	mbsinit
	.set	mbsinit,__mbsinit
