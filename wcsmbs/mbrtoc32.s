	.text
	.p2align 4,,15
	.globl	mbrtoc32
	.type	mbrtoc32, @function
mbrtoc32:
	leaq	state(%rip), %rax
	testq	%rcx, %rcx
	cmove	%rax, %rcx
	jmp	mbrtowc
	.size	mbrtoc32, .-mbrtoc32
	.local	state
	.comm	state,8,8
	.hidden	mbrtowc
