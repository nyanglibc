.macro double_vector offset value
.if .-__svml_d_trig_data != \offset
.err
.endif
.rept 8
.quad \value
.endr
.endm
 .section .rodata, "a"
 .align 64
 .globl __svml_d_trig_data
__svml_d_trig_data:
double_vector 0 0x7fffffffffffffff
double_vector 64 0x4160000000000000
double_vector 64*2 0x4170000000000000
double_vector 64*3 0x3ff921fb54442d18
double_vector 64*4 0x3fd45f306dc9c883
double_vector 64*5 0x4338000000000000
double_vector 64*6 0x0000000000000000
double_vector 64*7 0x8000000000000000
double_vector 64*8 0x3fe0000000000000
double_vector 64*9 0x400921fb40000000
double_vector 64*10 0x3e84442d00000000
double_vector 64*11 0x3d08469880000000
double_vector 64*12 0x3b88cc51701b839a
double_vector 64*13 0x400921fb54442d18
double_vector 64*14 0x3ca1a62633145c06
double_vector 64*15 0x395c1cd129024e09
double_vector 64*16 0x3ff921fc00000000
double_vector 64*17 0xbea5777a00000000
double_vector 64*18 0xbd473dcc00000000
double_vector 64*19 0x3bf898cc51701b84
double_vector 64*20 0xbfc55555555554a7
double_vector 64*21 0x3f8111111110a4a8
double_vector 64*22 0xbf2a01a019a5b86d
double_vector 64*23 0x3ec71de38030fea0
double_vector 64*24 0xbe5ae63546002231
double_vector 64*25 0x3de60e6857a2f220
double_vector 64*26 0xbd69f0d60811aac8
double_vector 64*27 0xbfc55555555554a8
double_vector 64*28 0x3f8111111110a573
double_vector 64*29 0xbf2a01a019a659dd
double_vector 64*30 0x3ec71de3806add1a
double_vector 64*31 0xbe5ae6355aaa4a53
double_vector 64*32 0x3de60e6bee01d83e
double_vector 64*33 0xbd69f1517e9f65f0
double_vector 64*34 0x4330000000000000
double_vector 64*35 0x432fffffffffffff
double_vector 64*36 0x43300000007ffffe
 .type __svml_d_trig_data,@object
 .size __svml_d_trig_data,.-__svml_d_trig_data
