.macro float_vector offset value
.if .-__svml_s_trig_data != \offset
.err
.endif
.rept 16
.long \value
.endr
.endm
 .section .rodata, "a"
 .align 64
 .globl __svml_s_trig_data
__svml_s_trig_data:
float_vector 0 0x7fffffff
float_vector 64 0x461c4000
float_vector 64*2 0x7f800000
float_vector 64*3 0xbe2aaaab
float_vector 64*4 0x3c08885c
float_vector 64*5 0xbf000000
float_vector 64*6 0x3d2aaa7c
float_vector 64*7 0x40490000
float_vector 64*8 0x3a7da000
float_vector 64*9 0x34222000
float_vector 64*10 0x2cb4611a
float_vector 64*11 0x40490fdb
float_vector 64*12 0xb3bbbd2e
float_vector 64*13 0xa7772ced
float_vector 64*14 0xbe2aaaa6
float_vector 64*15 0x3c08876a
float_vector 64*16 0xb94fb7ff
float_vector 64*17 0x362edef8
float_vector 64*18 0x3c088768
float_vector 64*19 0xb94fb6cf
float_vector 64*20 0x362ec335
float_vector 64*21 0x3ea2f983
float_vector 64*22 0x4b400000
float_vector 64*23 0x3fc90fdb
float_vector 64*24 0x3f000000
float_vector 64*25 0x000000ff
float_vector 64*26 0x00000040
float_vector 64*27 0x80000000
 .type __svml_s_trig_data,@object
 .size __svml_s_trig_data,.-__svml_s_trig_data
