.macro WRAPPER_IMPL_SSE2 callee
        subq $40, %rsp
        movaps %xmm0, (%rsp)
        call \callee
        movsd %xmm0, 16(%rsp)
        movsd 8(%rsp), %xmm0
        call \callee
        movsd 16(%rsp), %xmm1
        movsd %xmm0, 24(%rsp)
        unpcklpd %xmm0, %xmm1
        movaps %xmm1, %xmm0
        addq $40, %rsp
        ret
.endm
.macro WRAPPER_IMPL_SSE2_ff callee
        subq $56, %rsp
        movaps %xmm0, (%rsp)
        movaps %xmm1, 16(%rsp)
        call \callee
        movsd %xmm0, 32(%rsp)
        movsd 8(%rsp), %xmm0
        movsd 24(%rsp), %xmm1
        call \callee
        movsd 32(%rsp), %xmm1
        movsd %xmm0, 40(%rsp)
        unpcklpd %xmm0, %xmm1
        movaps %xmm1, %xmm0
        addq $56, %rsp
        ret
.endm
.macro WRAPPER_IMPL_SSE2_fFF callee
        pushq %rbp
        pushq %rbx
        movq %rdi, %rbp
        movq %rsi, %rbx
        subq $40, %rsp
        leaq 16(%rsp), %rsi
        leaq 24(%rsp), %rdi
        movaps %xmm0, (%rsp)
        call \callee
        leaq 16(%rsp), %rsi
        leaq 24(%rsp), %rdi
        movsd 24(%rsp), %xmm0
        movapd (%rsp), %xmm1
        movsd %xmm0, 0(%rbp)
        unpckhpd %xmm1, %xmm1
        movsd 16(%rsp), %xmm0
        movsd %xmm0, (%rbx)
        movapd %xmm1, %xmm0
        call \callee
        movsd 24(%rsp), %xmm0
        movsd %xmm0, 8(%rbp)
        movsd 16(%rsp), %xmm0
        movsd %xmm0, 8(%rbx)
        addq $40, %rsp
        popq %rbx
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        subq $32, %rsp
        vextractf128 $1, %ymm0, (%rsp)
        vzeroupper
        call \callee
        vmovapd %xmm0, 16(%rsp)
        vmovaps (%rsp), %xmm0
        call \callee
        vmovapd %xmm0, %xmm1
        vmovapd 16(%rsp), %xmm0
        vinsertf128 $1, %xmm1, %ymm0, %ymm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX_ff callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        subq $64, %rsp
        vextractf128 $1, %ymm0, 16(%rsp)
        vextractf128 $1, %ymm1, (%rsp)
        vzeroupper
        call \callee
        vmovaps %xmm0, 32(%rsp)
        vmovaps 16(%rsp), %xmm0
        vmovaps (%rsp), %xmm1
        call \callee
        vmovaps %xmm0, %xmm1
        vmovaps 32(%rsp), %xmm0
        vinsertf128 $1, %xmm1, %ymm0, %ymm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX_fFF callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        pushq %r13
        pushq %r14
        subq $48, %rsp
        movq %rsi, %r14
        movq %rdi, %r13
        vextractf128 $1, %ymm0, 32(%rsp)
        vzeroupper
        call \callee
        vmovaps 32(%rsp), %xmm0
        lea (%rsp), %rdi
        lea 16(%rsp), %rsi
        call \callee
        vmovapd (%rsp), %xmm0
        vmovapd 16(%rsp), %xmm1
        vmovapd %xmm0, 16(%r13)
        vmovapd %xmm1, 16(%r14)
        addq $48, %rsp
        popq %r14
        popq %r13
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX512 callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-64, %rsp
        subq $128, %rsp
        vmovups %zmm0, (%rsp)
        vmovupd (%rsp), %ymm0
        call \callee
        vmovupd %ymm0, 64(%rsp)
        vmovupd 32(%rsp), %ymm0
        call \callee
        vmovupd %ymm0, 96(%rsp)
        vmovups 64(%rsp), %zmm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX512_ff callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-64, %rsp
        subq $192, %rsp
        vmovups %zmm0, (%rsp)
        vmovups %zmm1, 64(%rsp)
        vmovupd (%rsp), %ymm0
        vmovupd 64(%rsp), %ymm1
        call \callee
        vmovupd %ymm0, 128(%rsp)
        vmovupd 32(%rsp), %ymm0
        vmovupd 96(%rsp), %ymm1
        call \callee
        vmovupd %ymm0, 160(%rsp)
        vmovups 128(%rsp), %zmm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX512_fFF callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-64, %rsp
        pushq %r12
        pushq %r13
        subq $176, %rsp
        movq %rsi, %r13
        vmovups %zmm0, (%rsp)
        movq %rdi, %r12
        vmovupd (%rsp), %ymm0
        call \callee
        vmovupd 32(%rsp), %ymm0
        lea 64(%rsp), %rdi
        lea 96(%rsp), %rsi
        call \callee
        vmovupd 64(%rsp), %ymm0
        vmovupd 96(%rsp), %ymm1
        vmovupd %ymm0, 32(%r12)
        vmovupd %ymm1, 32(%r13)
        vzeroupper
        addq $176, %rsp
        popq %r13
        popq %r12
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
 .text
.globl _ZGVcN4vv_pow
 .type _ZGVcN4vv_pow,@function
 .align 1<<4
 _ZGVcN4vv_pow: 
 

WRAPPER_IMPL_AVX_ff _ZGVbN2vv_pow
 .size _ZGVcN4vv_pow,.-_ZGVcN4vv_pow

