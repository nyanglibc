.macro float_vector offset value
.if .-__svml_sexp_data != \offset
.err
.endif
.rept 16
.long \value
.endr
.endm
 .section .rodata, "a"
 .align 64
 .globl __svml_sexp_data
__svml_sexp_data:
float_vector 0 0x3fb8aa3b
float_vector 64 0x4b400000
float_vector 128 0x3f317200
float_vector 192 0x35bfbe8e
float_vector 256 0x0000007f
float_vector 320 0x3f800000
float_vector 384 0x3f7ffffe
float_vector 448 0x3effff34
float_vector 512 0x3e2aacac
float_vector 576 0x3d2b8392
float_vector 640 0x3c07d9fe
float_vector 704 0x7fffffff
float_vector 768 0x42aeac4f
 .type __svml_sexp_data,@object
 .size __svml_sexp_data,.-__svml_sexp_data
