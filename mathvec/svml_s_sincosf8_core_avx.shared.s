.macro WRAPPER_IMPL_SSE2 callee
        subq $40, %rsp
        movaps %xmm0, (%rsp)
        call \ callee@PLT
        movss %xmm0, 16(%rsp)
        movss 4(%rsp), %xmm0
        call \ callee@PLT
        movss %xmm0, 20(%rsp)
        movss 8(%rsp), %xmm0
        call \ callee@PLT
        movss %xmm0, 24(%rsp)
        movss 12(%rsp), %xmm0
        call \ callee@PLT
        movss 16(%rsp), %xmm3
        movss 20(%rsp), %xmm2
        movss 24(%rsp), %xmm1
        movss %xmm0, 28(%rsp)
        unpcklps %xmm1, %xmm3
        unpcklps %xmm0, %xmm2
        unpcklps %xmm2, %xmm3
        movaps %xmm3, %xmm0
        addq $40, %rsp
        ret
.endm
.macro WRAPPER_IMPL_SSE2_ff callee
        subq $56, %rsp
        movaps %xmm0, (%rsp)
        movaps %xmm1, 16(%rsp)
        call \ callee@PLT
        movss %xmm0, 32(%rsp)
        movss 4(%rsp), %xmm0
        movss 20(%rsp), %xmm1
        call \ callee@PLT
        movss %xmm0, 36(%rsp)
        movss 8(%rsp), %xmm0
        movss 24(%rsp), %xmm1
        call \ callee@PLT
        movss %xmm0, 40(%rsp)
        movss 12(%rsp), %xmm0
        movss 28(%rsp), %xmm1
        call \ callee@PLT
        movss 32(%rsp), %xmm3
        movss 36(%rsp), %xmm2
        movss 40(%rsp), %xmm1
        movss %xmm0, 44(%rsp)
        unpcklps %xmm1, %xmm3
        unpcklps %xmm0, %xmm2
        unpcklps %xmm2, %xmm3
        movaps %xmm3, %xmm0
        addq $56, %rsp
        ret
.endm
.macro WRAPPER_IMPL_SSE2_fFF callee
        pushq %rbp
        pushq %rbx
        movq %rdi, %rbp
        movq %rsi, %rbx
        subq $40, %rsp
        leaq 24(%rsp), %rsi
        leaq 28(%rsp), %rdi
        movaps %xmm0, (%rsp)
        call \ callee@PLT
        leaq 24(%rsp), %rsi
        leaq 28(%rsp), %rdi
        movss 28(%rsp), %xmm0
        movss %xmm0, 0(%rbp)
        movaps (%rsp), %xmm1
        movss 24(%rsp), %xmm0
        movss %xmm0, (%rbx)
        movaps %xmm1, %xmm0
        shufps $85, %xmm1, %xmm0
        call \ callee@PLT
        movss 28(%rsp), %xmm0
        leaq 24(%rsp), %rsi
        movss %xmm0, 4(%rbp)
        leaq 28(%rsp), %rdi
        movaps (%rsp), %xmm1
        movss 24(%rsp), %xmm0
        movss %xmm0, 4(%rbx)
        movaps %xmm1, %xmm0
        unpckhps %xmm1, %xmm0
        call \ callee@PLT
        movaps (%rsp), %xmm1
        leaq 24(%rsp), %rsi
        leaq 28(%rsp), %rdi
        movss 28(%rsp), %xmm0
        shufps $255, %xmm1, %xmm1
        movss %xmm0, 8(%rbp)
        movss 24(%rsp), %xmm0
        movss %xmm0, 8(%rbx)
        movaps %xmm1, %xmm0
        call \ callee@PLT
        movss 28(%rsp), %xmm0
        movss %xmm0, 12(%rbp)
        movss 24(%rsp), %xmm0
        movss %xmm0, 12(%rbx)
        addq $40, %rsp
        popq %rbx
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        subq $32, %rsp
        vextractf128 $1, %ymm0, (%rsp)
        vzeroupper
        call __GI_\callee
        vmovaps %xmm0, 16(%rsp)
        vmovaps (%rsp), %xmm0
        call __GI_\callee
        vmovaps %xmm0, %xmm1
        vmovaps 16(%rsp), %xmm0
        vinsertf128 $1, %xmm1, %ymm0, %ymm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX_ff callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        subq $64, %rsp
        vextractf128 $1, %ymm0, 16(%rsp)
        vextractf128 $1, %ymm1, (%rsp)
        vzeroupper
        call __GI_\callee
        vmovaps %xmm0, 32(%rsp)
        vmovaps 16(%rsp), %xmm0
        vmovaps (%rsp), %xmm1
        call __GI_\callee
        vmovaps %xmm0, %xmm1
        vmovaps 32(%rsp), %xmm0
        vinsertf128 $1, %xmm1, %ymm0, %ymm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX_fFF callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        pushq %r13
        pushq %r14
        subq $48, %rsp
        movq %rsi, %r14
        vmovaps %ymm0, (%rsp)
        movq %rdi, %r13
        vmovaps 16(%rsp), %xmm1
        vmovaps %xmm1, 32(%rsp)
        vzeroupper
        vmovaps (%rsp), %xmm0
        call __GI_\callee
        vmovaps 32(%rsp), %xmm0
        lea (%rsp), %rdi
        lea 16(%rsp), %rsi
        call __GI_\callee
        vmovaps (%rsp), %xmm0
        vmovaps 16(%rsp), %xmm1
        vmovaps %xmm0, 16(%r13)
        vmovaps %xmm1, 16(%r14)
        addq $48, %rsp
        popq %r14
        popq %r13
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX512 callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-64, %rsp
        subq $128, %rsp
        vmovups %zmm0, (%rsp)
        vmovupd (%rsp), %ymm0
        call __GI_\callee
        vmovupd %ymm0, 64(%rsp)
        vmovupd 32(%rsp), %ymm0
        call __GI_\callee
        vmovupd %ymm0, 96(%rsp)
        vmovups 64(%rsp), %zmm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX512_ff callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-64, %rsp
        subq $192, %rsp
        vmovups %zmm0, (%rsp)
        vmovups %zmm1, 64(%rsp)
        vmovups (%rsp), %ymm0
        vmovups 64(%rsp), %ymm1
        call __GI_\callee
        vmovups %ymm0, 128(%rsp)
        vmovups 32(%rsp), %ymm0
        vmovups 96(%rsp), %ymm1
        call __GI_\callee
        vmovups %ymm0, 160(%rsp)
        vmovups 128(%rsp), %zmm0
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.macro WRAPPER_IMPL_AVX512_fFF callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-64, %rsp
        pushq %r12
        pushq %r13
        subq $176, %rsp
        movq %rsi, %r13
        vmovaps %zmm0, (%rsp)
        movq %rdi, %r12
        vmovaps (%rsp), %ymm0
        call __GI_\callee
        vmovaps 32(%rsp), %ymm0
        lea 64(%rsp), %rdi
        lea 96(%rsp), %rsi
        call __GI_\callee
        vmovaps 64(%rsp), %ymm0
        vmovaps 96(%rsp), %ymm1
        vmovaps %ymm0, 32(%r12)
        vmovaps %ymm1, 32(%r13)
        addq $176, %rsp
        popq %r13
        popq %r12
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
        .text
.globl _ZGVcN8vl4l4_sincosf
 .type _ZGVcN8vl4l4_sincosf,@function
 .align 1<<4
 _ZGVcN8vl4l4_sincosf: 
 

WRAPPER_IMPL_AVX_fFF _ZGVbN4vl4l4_sincosf
 .size _ZGVcN8vl4l4_sincosf,.-_ZGVcN8vl4l4_sincosf

.macro WRAPPER_IMPL_AVX_fFF_vvv callee
        pushq %rbp
        movq %rsp, %rbp
        andq $-32, %rsp
        subq $224, %rsp
        vmovups %ymm0, 64(%rsp)
        lea (%rsp), %rdi
        vmovdqu %xmm1, 96(%rdi)
        vmovdqu %xmm2, 112(%rdi)
        vmovdqu %xmm3, 128(%rdi)
        vmovdqu %xmm4, 144(%rdi)
        vmovdqu %xmm5, 160(%rdi)
        lea 32(%rsp), %rsi
        vmovdqu %xmm6, 144(%rsi)
        vmovdqu %xmm7, 160(%rsi)
        vzeroupper
        call __GI_\callee
        vmovdqu 80(%rsp), %xmm0
        lea 16(%rsp), %rdi
        lea 48(%rsp), %rsi
        call __GI_\callee
        movq 96(%rsp), %rdx
        movq 104(%rsp), %rsi
        movq 112(%rsp), %r8
        movq 120(%rsp), %r10
        movl (%rsp), %eax
        movl 4(%rsp), %ecx
        movl 8(%rsp), %edi
        movl 12(%rsp), %r9d
        movl %eax, (%rdx)
        movl %ecx, (%rsi)
        movq 128(%rsp), %rax
        movq 136(%rsp), %rcx
        movl %edi, (%r8)
        movl %r9d, (%r10)
        movq 144(%rsp), %rdi
        movq 152(%rsp), %r9
        movl 16(%rsp), %r11d
        movl 20(%rsp), %edx
        movl 24(%rsp), %esi
        movl 28(%rsp), %r8d
        movl %r11d, (%rax)
        movl %edx, (%rcx)
        movq 160(%rsp), %r11
        movq 168(%rsp), %rdx
        movl %esi, (%rdi)
        movl %r8d, (%r9)
        movq 176(%rsp), %rsi
        movq 184(%rsp), %r8
        movl 32(%rsp), %r10d
        movl 36(%rsp), %eax
        movl 40(%rsp), %ecx
        movl 44(%rsp), %edi
        movl %r10d, (%r11)
        movl %eax, (%rdx)
        movq 192(%rsp), %r10
        movq 200(%rsp), %rax
        movl %ecx, (%rsi)
        movl %edi, (%r8)
        movq 16(%rbp), %rcx
        movq 24(%rbp), %rdi
        movl 48(%rsp), %r9d
        movl 52(%rsp), %r11d
        movl 56(%rsp), %edx
        movl 60(%rsp), %esi
        movl %r9d, (%r10)
        movl %r11d, (%rax)
        movl %edx, (%rcx)
        movl %esi, (%rdi)
        movq %rbp, %rsp
        popq %rbp
        ret
.endm
.globl _ZGVcN8vvv_sincosf
 .type _ZGVcN8vvv_sincosf,@function
 .align 1<<4
 _ZGVcN8vvv_sincosf: 
 

WRAPPER_IMPL_AVX_fFF_vvv _ZGVbN4vl4l4_sincosf
 .size _ZGVcN8vvv_sincosf,.-_ZGVcN8vvv_sincosf

