.macro float_vector offset value
.if .-__svml_slog_data != \offset
.err
.endif
.rept 16
.long \value
.endr
.endm
 .section .rodata, "a"
 .align 64
 .globl __svml_slog_data
__svml_slog_data:
float_vector 0 0xbf000000
float_vector 64 0x3eaaaee7
float_vector 128 0xbe80061d
float_vector 192 0x3e4afb81
float_vector 256 0xbe289358
float_vector 320 0x3e2db86b
float_vector 384 0xbe1b6a22
float_vector 448 0x00800000
float_vector 512 0x01000000
float_vector 576 0x3f2aaaab
float_vector 640 0x007fffff
float_vector 704 0x3f800000
float_vector 768 0x3f317218
.if .-__svml_slog_data != 832
.err
.endif
 .long 0x7f800000
 .long 0xff800000
 .rept 56
 .byte 0
 .endr
.if .-__svml_slog_data != 896
.err
.endif
 .long 0x3f800000
 .long 0xbf800000
 .rept 56
 .byte 0
 .endr
.if .-__svml_slog_data != 960
.err
.endif
 .long 0x00000000
 .long 0x80000000
 .rept 56
 .byte 0
 .endr
 .type __svml_slog_data,@object
 .size __svml_slog_data,.-__svml_slog_data
