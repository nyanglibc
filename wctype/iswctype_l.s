	.text
	.p2align 4,,15
	.globl	__iswctype_l
	.type	__iswctype_l, @function
__iswctype_l:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1
	movl	(%rsi), %ecx
	movl	%edi, %edx
	shrl	%cl, %edx
	cmpl	4(%rsi), %edx
	jnb	.L1
	addl	$5, %edx
	movl	(%rsi,%rdx,4), %edx
	testl	%edx, %edx
	je	.L1
	movl	8(%rsi), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rsi), %ecx
	leaq	(%rsi,%rcx,4), %rcx
	movl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L1
	movl	%edi, %eax
	movl	%edi, %ecx
	shrl	$5, %eax
	andl	16(%rsi), %eax
	leaq	(%rsi,%rax,4), %rax
	movl	(%rax,%rdx), %eax
	shrl	%cl, %eax
	andl	$1, %eax
.L1:
	rep ret
	.size	__iswctype_l, .-__iswctype_l
	.weak	iswctype_l
	.set	iswctype_l,__iswctype_l
