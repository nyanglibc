	.text
	.p2align 4,,15
	.globl	__iswalnum
	.hidden	__iswalnum
	.type	__iswalnum, @function
__iswalnum:
	testl	$-128, %edi
	je	.L10
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$11, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L1
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L1
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L1
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$8, %eax
	movzwl	%ax, %eax
	ret
	.size	__iswalnum, .-__iswalnum
	.weak	iswalnum
	.hidden	iswalnum
	.set	iswalnum,__iswalnum
	.p2align 4,,15
	.globl	__iswalpha
	.type	__iswalpha, @function
__iswalpha:
	testl	$-128, %edi
	je	.L19
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$2, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L11
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L11
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L11
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L11:
	rep ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$1024, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswalpha, .-__iswalpha
	.weak	iswalpha
	.hidden	iswalpha
	.set	iswalpha,__iswalpha
	.p2align 4,,15
	.globl	__iswblank
	.type	__iswblank, @function
__iswblank:
	testl	$-128, %edi
	je	.L28
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$8, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L20
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L20
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L20
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L20:
	rep ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$1, %eax
	ret
	.size	__iswblank, .-__iswblank
	.weak	iswblank
	.set	iswblank,__iswblank
	.p2align 4,,15
	.globl	__iswcntrl
	.type	__iswcntrl, @function
__iswcntrl:
	testl	$-128, %edi
	je	.L37
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$9, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L29
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L29
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L29
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L29:
	rep ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$2, %eax
	movzwl	%ax, %eax
	ret
	.size	__iswcntrl, .-__iswcntrl
	.weak	iswcntrl
	.set	iswcntrl,__iswcntrl
	.p2align 4,,15
	.globl	__iswdigit
	.type	__iswdigit, @function
__iswdigit:
	testl	$-128, %edi
	je	.L46
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$3, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L38
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L38
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L38
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L38:
	rep ret
	.p2align 4,,10
	.p2align 3
.L46:
	subl	$48, %edi
	xorl	%eax, %eax
	cmpl	$9, %edi
	setbe	%al
	ret
	.size	__iswdigit, .-__iswdigit
	.weak	iswdigit
	.hidden	iswdigit
	.set	iswdigit,__iswdigit
	.p2align 4,,15
	.globl	__iswlower
	.hidden	__iswlower
	.type	__iswlower, @function
__iswlower:
	testl	$-128, %edi
	je	.L55
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$1, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L47
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L47
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L47
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L47:
	rep ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$512, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswlower, .-__iswlower
	.weak	iswlower
	.hidden	iswlower
	.set	iswlower,__iswlower
	.p2align 4,,15
	.globl	__iswgraph
	.type	__iswgraph, @function
__iswgraph:
	testl	$-128, %edi
	je	.L64
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$7, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L56
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L56
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L56
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L56:
	rep ret
	.p2align 4,,10
	.p2align 3
.L64:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$-32768, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswgraph, .-__iswgraph
	.weak	iswgraph
	.set	iswgraph,__iswgraph
	.p2align 4,,15
	.globl	__iswprint
	.type	__iswprint, @function
__iswprint:
	testl	$-128, %edi
	je	.L73
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$6, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L65
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L65
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L65
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L65:
	rep ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$16384, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswprint, .-__iswprint
	.weak	iswprint
	.set	iswprint,__iswprint
	.p2align 4,,15
	.globl	__iswpunct
	.type	__iswpunct, @function
__iswpunct:
	testl	$-128, %edi
	je	.L82
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$10, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L74
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L74
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L74
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L74:
	rep ret
	.p2align 4,,10
	.p2align 3
.L82:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$4, %eax
	movzwl	%ax, %eax
	ret
	.size	__iswpunct, .-__iswpunct
	.weak	iswpunct
	.set	iswpunct,__iswpunct
	.p2align 4,,15
	.globl	__iswspace
	.type	__iswspace, @function
__iswspace:
	testl	$-128, %edi
	je	.L91
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$5, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L83
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L83
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L83
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L83:
	rep ret
	.p2align 4,,10
	.p2align 3
.L91:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$8192, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswspace, .-__iswspace
	.weak	iswspace
	.hidden	iswspace
	.set	iswspace,__iswspace
	.p2align 4,,15
	.globl	__iswupper
	.type	__iswupper, @function
__iswupper:
	testl	$-128, %edi
	je	.L100
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movzwl	200(%rax), %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L92
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L92
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L92
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L92:
	rep ret
	.p2align 4,,10
	.p2align 3
.L100:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$256, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswupper, .-__iswupper
	.weak	iswupper
	.set	iswupper,__iswupper
	.p2align 4,,15
	.globl	__iswxdigit
	.type	__iswxdigit, @function
__iswxdigit:
	testl	$-128, %edi
	je	.L109
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	movl	200(%rdx), %eax
	addl	$4, %eax
	movzwl	%ax, %eax
	movq	64(%rdx,%rax,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L101
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L101
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L101
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L101:
	rep ret
	.p2align 4,,10
	.p2align 3
.L109:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movl	%edi, %edi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$4096, %ax
	movzwl	%ax, %eax
	ret
	.size	__iswxdigit, .-__iswxdigit
	.weak	iswxdigit
	.hidden	iswxdigit
	.set	iswxdigit,__iswxdigit
	.p2align 4,,15
	.globl	__towlower
	.hidden	__towlower
	.type	__towlower, @function
__towlower:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rdx
	movl	%edi, %eax
	movq	%fs:(%rdx), %rdx
	movq	(%rdx), %rcx
	movl	208(%rcx), %esi
	leal	1(%rsi), %edx
	movzwl	%dx, %edx
	movq	64(%rcx,%rdx,8), %rdx
	movl	(%rdx), %ecx
	shrl	%cl, %edi
	cmpl	4(%rdx), %edi
	movl	%edi, %ecx
	jnb	.L111
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L111
	movl	8(%rdx), %ecx
	movl	%eax, %edi
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L111
	movl	%eax, %esi
	andl	16(%rdx), %esi
	leaq	(%rdx,%rsi,4), %rdx
	addl	(%rdx,%rcx), %eax
.L111:
	rep ret
	.size	__towlower, .-__towlower
	.weak	towlower
	.hidden	towlower
	.set	towlower,__towlower
	.p2align 4,,15
	.globl	__towupper
	.hidden	__towupper
	.type	__towupper, @function
__towupper:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rdx
	movl	%edi, %eax
	movq	%fs:(%rdx), %rdx
	movq	(%rdx), %rdx
	movzwl	208(%rdx), %ecx
	movq	64(%rdx,%rcx,8), %rdx
	movl	(%rdx), %ecx
	shrl	%cl, %edi
	cmpl	4(%rdx), %edi
	movl	%edi, %ecx
	jnb	.L119
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L119
	movl	8(%rdx), %ecx
	movl	%eax, %edi
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L119
	movl	%eax, %esi
	andl	16(%rdx), %esi
	leaq	(%rdx,%rsi,4), %rdx
	addl	(%rdx,%rcx), %eax
.L119:
	rep ret
	.size	__towupper, .-__towupper
	.weak	towupper
	.hidden	towupper
	.set	towupper,__towupper
	.hidden	_nl_current_LC_CTYPE
