	.text
	.p2align 4,,15
	.globl	__wctrans
	.type	__wctrans, @function
__wctrans:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %r13
	movq	152(%r13), %rbx
	cmpb	$0, (%rbx)
	je	.L5
	movq	%rdi, %r12
	xorl	%ebp, %ebp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rdi
	addq	$1, %rbp
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
	cmpb	$0, (%rbx)
	je	.L5
.L4:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L9
	movl	208(%r13), %eax
	leaq	8(%rbp,%rax), %rax
	movq	0(%r13,%rax,8), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__wctrans, .-__wctrans
	.weak	wctrans
	.set	wctrans,__wctrans
	.hidden	strcmp
	.hidden	_nl_current_LC_CTYPE
