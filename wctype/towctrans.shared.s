	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___towctrans
	.hidden	__GI___towctrans
	.type	__GI___towctrans, @function
__GI___towctrans:
	testq	%rsi, %rsi
	movl	%edi, %eax
	je	.L2
	movl	(%rsi), %ecx
	movl	%edi, %edx
	shrl	%cl, %edx
	cmpl	4(%rsi), %edx
	jnb	.L2
	addl	$5, %edx
	movl	(%rsi,%rdx,4), %edx
	testl	%edx, %edx
	je	.L2
	movl	8(%rsi), %ecx
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	12(%rsi), %ecx
	leaq	(%rsi,%rcx,4), %rcx
	movl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L2
	movl	%eax, %ecx
	andl	16(%rsi), %ecx
	leaq	(%rsi,%rcx,4), %rcx
	addl	(%rcx,%rdx), %eax
.L2:
	rep ret
	.size	__GI___towctrans, .-__GI___towctrans
	.globl	__towctrans
	.set	__towctrans,__GI___towctrans
	.weak	towctrans
	.set	towctrans,__towctrans
