	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___iswalnum_l
	.hidden	__GI___iswalnum_l
	.type	__GI___iswalnum_l, @function
__GI___iswalnum_l:
	testl	$-128, %edi
	je	.L10
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$11, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L1
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L1
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L1
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andl	$8, %eax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswalnum_l, .-__GI___iswalnum_l
	.globl	__iswalnum_l
	.set	__iswalnum_l,__GI___iswalnum_l
	.weak	iswalnum_l
	.set	iswalnum_l,__iswalnum_l
	.p2align 4,,15
	.globl	__GI___iswalpha_l
	.hidden	__GI___iswalpha_l
	.type	__GI___iswalpha_l, @function
__GI___iswalpha_l:
	testl	$-128, %edi
	je	.L19
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$2, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L11
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L11
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L11
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L11:
	rep ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$1024, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswalpha_l, .-__GI___iswalpha_l
	.globl	__iswalpha_l
	.set	__iswalpha_l,__GI___iswalpha_l
	.weak	iswalpha_l
	.set	iswalpha_l,__iswalpha_l
	.p2align 4,,15
	.globl	__GI___iswblank_l
	.hidden	__GI___iswblank_l
	.type	__GI___iswblank_l, @function
__GI___iswblank_l:
	testl	$-128, %edi
	je	.L28
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$8, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L20
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L20
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L20
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L20:
	rep ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andl	$1, %eax
	ret
	.size	__GI___iswblank_l, .-__GI___iswblank_l
	.globl	__iswblank_l
	.set	__iswblank_l,__GI___iswblank_l
	.weak	iswblank_l
	.set	iswblank_l,__iswblank_l
	.p2align 4,,15
	.globl	__GI___iswcntrl_l
	.hidden	__GI___iswcntrl_l
	.type	__GI___iswcntrl_l, @function
__GI___iswcntrl_l:
	testl	$-128, %edi
	je	.L37
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$9, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L29
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L29
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L29
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L29:
	rep ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andl	$2, %eax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswcntrl_l, .-__GI___iswcntrl_l
	.globl	__iswcntrl_l
	.set	__iswcntrl_l,__GI___iswcntrl_l
	.weak	iswcntrl_l
	.set	iswcntrl_l,__iswcntrl_l
	.p2align 4,,15
	.globl	__GI___iswdigit_l
	.hidden	__GI___iswdigit_l
	.type	__GI___iswdigit_l, @function
__GI___iswdigit_l:
	testl	$-128, %edi
	je	.L46
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$3, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L38
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L38
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L38
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L38:
	rep ret
	.p2align 4,,10
	.p2align 3
.L46:
	subl	$48, %edi
	xorl	%eax, %eax
	cmpl	$9, %edi
	setbe	%al
	ret
	.size	__GI___iswdigit_l, .-__GI___iswdigit_l
	.globl	__iswdigit_l
	.set	__iswdigit_l,__GI___iswdigit_l
	.weak	iswdigit_l
	.set	iswdigit_l,__iswdigit_l
	.p2align 4,,15
	.globl	__GI___iswlower_l
	.hidden	__GI___iswlower_l
	.type	__GI___iswlower_l, @function
__GI___iswlower_l:
	testl	$-128, %edi
	je	.L55
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$1, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L47
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L47
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L47
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L47:
	rep ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$512, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswlower_l, .-__GI___iswlower_l
	.globl	__iswlower_l
	.set	__iswlower_l,__GI___iswlower_l
	.weak	iswlower_l
	.set	iswlower_l,__iswlower_l
	.p2align 4,,15
	.globl	__GI___iswgraph_l
	.hidden	__GI___iswgraph_l
	.type	__GI___iswgraph_l, @function
__GI___iswgraph_l:
	testl	$-128, %edi
	je	.L64
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$7, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L56
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L56
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L56
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L56:
	rep ret
	.p2align 4,,10
	.p2align 3
.L64:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$-32768, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswgraph_l, .-__GI___iswgraph_l
	.globl	__iswgraph_l
	.set	__iswgraph_l,__GI___iswgraph_l
	.weak	iswgraph_l
	.set	iswgraph_l,__iswgraph_l
	.p2align 4,,15
	.globl	__GI___iswprint_l
	.hidden	__GI___iswprint_l
	.type	__GI___iswprint_l, @function
__GI___iswprint_l:
	testl	$-128, %edi
	je	.L73
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$6, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L65
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L65
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L65
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L65:
	rep ret
	.p2align 4,,10
	.p2align 3
.L73:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$16384, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswprint_l, .-__GI___iswprint_l
	.globl	__iswprint_l
	.set	__iswprint_l,__GI___iswprint_l
	.weak	iswprint_l
	.set	iswprint_l,__iswprint_l
	.p2align 4,,15
	.globl	__GI___iswpunct_l
	.hidden	__GI___iswpunct_l
	.type	__GI___iswpunct_l, @function
__GI___iswpunct_l:
	testl	$-128, %edi
	je	.L82
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$10, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L74
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L74
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L74
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L74:
	rep ret
	.p2align 4,,10
	.p2align 3
.L82:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andl	$4, %eax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswpunct_l, .-__GI___iswpunct_l
	.globl	__iswpunct_l
	.set	__iswpunct_l,__GI___iswpunct_l
	.weak	iswpunct_l
	.set	iswpunct_l,__iswpunct_l
	.p2align 4,,15
	.globl	__GI___iswspace_l
	.hidden	__GI___iswspace_l
	.type	__GI___iswspace_l, @function
__GI___iswspace_l:
	testl	$-128, %edi
	je	.L91
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$5, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L83
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L83
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L83
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L83:
	rep ret
	.p2align 4,,10
	.p2align 3
.L91:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$8192, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswspace_l, .-__GI___iswspace_l
	.globl	__iswspace_l
	.set	__iswspace_l,__GI___iswspace_l
	.weak	iswspace_l
	.set	iswspace_l,__iswspace_l
	.p2align 4,,15
	.globl	__GI___iswupper_l
	.hidden	__GI___iswupper_l
	.type	__GI___iswupper_l, @function
__GI___iswupper_l:
	testl	$-128, %edi
	je	.L100
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L92
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L92
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L92
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L92:
	rep ret
	.p2align 4,,10
	.p2align 3
.L100:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$256, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswupper_l, .-__GI___iswupper_l
	.globl	__iswupper_l
	.set	__iswupper_l,__GI___iswupper_l
	.weak	iswupper_l
	.set	iswupper_l,__iswupper_l
	.p2align 4,,15
	.globl	__GI___iswxdigit_l
	.hidden	__GI___iswxdigit_l
	.type	__GI___iswxdigit_l, @function
__GI___iswxdigit_l:
	testl	$-128, %edi
	je	.L109
	movq	(%rsi), %rax
	movl	200(%rax), %edx
	addl	$4, %edx
	movq	64(%rax,%rdx,8), %rdx
	movl	%edi, %eax
	movl	(%rdx), %ecx
	shrl	%cl, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpl	4(%rdx), %ecx
	jnb	.L101
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L101
	movl	8(%rdx), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L101
	movl	%edi, %eax
	shrl	$5, %eax
	andl	16(%rdx), %eax
	leaq	(%rdx,%rax,4), %rax
	movl	(%rax,%rcx), %eax
	movl	%edi, %ecx
	shrl	%cl, %eax
	andl	$1, %eax
.L101:
	rep ret
	.p2align 4,,10
	.p2align 3
.L109:
	movq	104(%rsi), %rax
	movl	%edi, %edi
	movzwl	(%rax,%rdi,2), %eax
	andw	$4096, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI___iswxdigit_l, .-__GI___iswxdigit_l
	.globl	__iswxdigit_l
	.set	__iswxdigit_l,__GI___iswxdigit_l
	.weak	iswxdigit_l
	.set	iswxdigit_l,__iswxdigit_l
	.p2align 4,,15
	.globl	__GI___towlower_l
	.hidden	__GI___towlower_l
	.type	__GI___towlower_l, @function
__GI___towlower_l:
	movq	(%rsi), %rdx
	movl	%edi, %eax
	movl	208(%rdx), %ecx
	addl	$1, %ecx
	movq	64(%rdx,%rcx,8), %rdx
	movl	(%rdx), %ecx
	shrl	%cl, %edi
	cmpl	4(%rdx), %edi
	movl	%edi, %ecx
	jnb	.L111
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L111
	movl	8(%rdx), %ecx
	movl	%eax, %edi
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L111
	movl	%eax, %esi
	andl	16(%rdx), %esi
	leaq	(%rdx,%rsi,4), %rdx
	addl	(%rdx,%rcx), %eax
.L111:
	rep ret
	.size	__GI___towlower_l, .-__GI___towlower_l
	.globl	__towlower_l
	.set	__towlower_l,__GI___towlower_l
	.weak	towlower_l
	.set	towlower_l,__towlower_l
	.p2align 4,,15
	.globl	__GI___towupper_l
	.hidden	__GI___towupper_l
	.type	__GI___towupper_l, @function
__GI___towupper_l:
	movq	(%rsi), %rdx
	movl	%edi, %eax
	movl	208(%rdx), %ecx
	movq	64(%rdx,%rcx,8), %rdx
	movl	(%rdx), %ecx
	shrl	%cl, %edi
	cmpl	4(%rdx), %edi
	movl	%edi, %ecx
	jnb	.L119
	addl	$5, %ecx
	movl	(%rdx,%rcx,4), %esi
	testl	%esi, %esi
	je	.L119
	movl	8(%rdx), %ecx
	movl	%eax, %edi
	shrl	%cl, %edi
	movl	%edi, %ecx
	andl	12(%rdx), %ecx
	leaq	(%rdx,%rcx,4), %rcx
	movl	(%rcx,%rsi), %ecx
	testl	%ecx, %ecx
	je	.L119
	movl	%eax, %esi
	andl	16(%rdx), %esi
	leaq	(%rdx,%rsi,4), %rdx
	addl	(%rdx,%rcx), %eax
.L119:
	rep ret
	.size	__GI___towupper_l, .-__GI___towupper_l
	.globl	__towupper_l
	.set	__towupper_l,__GI___towupper_l
	.weak	towupper_l
	.set	towupper_l,__towupper_l
