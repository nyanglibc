	.text
	.p2align 4,,15
	.globl	__wctype_l
	.type	__wctype_l, @function
__wctype_l:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	xorl	%r12d, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	strlen
	movq	(%rbx), %r14
	movq	%rax, %r13
	movq	144(%r14), %rbx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	1(%rbx,%rbp), %rbx
	cmpb	$0, (%rbx)
	je	.L6
	addl	$1, %r12d
.L5:
	movq	%rbx, %rdi
	call	strlen
	cmpq	%rax, %r13
	movq	%rax, %rbp
	jne	.L2
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2
	addl	200(%r14), %r12d
	movq	64(%r14,%r12,8), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__wctype_l, .-__wctype_l
	.weak	wctype_l
	.set	wctype_l,__wctype_l
	.hidden	strlen
