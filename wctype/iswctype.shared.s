	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___iswctype
	.hidden	__GI___iswctype
	.type	__GI___iswctype, @function
__GI___iswctype:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1
	movl	(%rsi), %ecx
	movl	%edi, %edx
	shrl	%cl, %edx
	cmpl	4(%rsi), %edx
	jnb	.L1
	addl	$5, %edx
	movl	(%rsi,%rdx,4), %edx
	testl	%edx, %edx
	je	.L1
	movl	8(%rsi), %ecx
	movl	%edi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rsi), %ecx
	leaq	(%rsi,%rcx,4), %rcx
	movl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L1
	movl	%edi, %eax
	movl	%edi, %ecx
	shrl	$5, %eax
	andl	16(%rsi), %eax
	leaq	(%rsi,%rax,4), %rax
	movl	(%rax,%rdx), %eax
	shrl	%cl, %eax
	andl	$1, %eax
.L1:
	rep ret
	.size	__GI___iswctype, .-__GI___iswctype
	.globl	__iswctype
	.set	__iswctype,__GI___iswctype
	.weak	iswctype
	.set	iswctype,__iswctype
