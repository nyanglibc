	.text
	.section	.ctors,"aw",@progbits
	.align 8
	.type	__CTOR_END__, @object
__CTOR_END__:
	.zero	8
	.hidden	__DTOR_END__
	.globl	__DTOR_END__
	.section	.dtors,"aw",@progbits
	.align 8
	.type	__DTOR_END__, @object
__DTOR_END__:
	.zero	8
	.section	.eh_frame,"a",@progbits
	.align 4
	.type	__FRAME_END__, @object
__FRAME_END__:
	.zero	4
	.hidden	__TMC_END__
	.globl	__TMC_END__
	.section	.tm_clone_table,"aw",@progbits
	.align 8
	.type	__TMC_END__, @object
__TMC_END__:
	.text
	.p2align 4,,15
	.type	__do_global_ctors_aux, @function
__do_global_ctors_aux:
	movq	-8+__CTOR_END__(%rip), %rax
	cmpq	$-1, %rax
	je	.L9
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%rbx
	leaq	-8+__CTOR_END__(%rip), %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$8, %rbx
	call	*%rax
	movq	(%rbx), %rax
	cmpq	$-1, %rax
	jne	.L3
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	rep ret
#APP
	.section	.init
	call __do_global_ctors_aux
