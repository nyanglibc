	.text
	.section	.ctors,"aw",@progbits
	.align 8
	.type	__CTOR_LIST__, @object
__CTOR_LIST__:
	.quad	-1
	.section	.dtors,"aw",@progbits
	.align 8
	.type	__DTOR_LIST__, @object
__DTOR_LIST__:
	.quad	-1
	.section	.tm_clone_table,"aw",@progbits
	.align 8
	.type	__TMC_LIST__, @object
__TMC_LIST__:
	.text
	.p2align 4,,15
	.type	deregister_tm_clones, @function
deregister_tm_clones:
	leaq	__TMC_LIST__(%rip), %rdi
	pushq	%rbp
	leaq	__TMC_END__(%rip), %rax
	cmpq	%rdi, %rax
	movq	%rsp, %rbp
	je	.L1
	movq	_ITM_deregisterTMCloneTable@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L1
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbp
	ret
	.p2align 4,,15
	.type	register_tm_clones, @function
register_tm_clones:
	leaq	__TMC_LIST__(%rip), %rdi
	leaq	__TMC_END__(%rip), %rsi
	pushq	%rbp
	subq	%rdi, %rsi
	movq	%rsp, %rbp
	sarq	$3, %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	addq	%rax, %rsi
	sarq	%rsi
	je	.L8
	movq	_ITM_registerTMCloneTable@GOTPCREL(%rip), %rax
	testq	%rax, %rax
	je	.L8
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L8:
	popq	%rbp
	ret
	.hidden	__dso_handle
	.globl	__dso_handle
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__dso_handle, @object
__dso_handle:
	.quad	__dso_handle
	.text
	.p2align 4,,15
	.type	__do_global_dtors_aux, @function
__do_global_dtors_aux:
	cmpb	$0, completed.4253(%rip)
	jne	.L27
	cmpq	$0, __cxa_finalize@GOTPCREL(%rip)
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%rbx
	je	.L19
	movq	__dso_handle(%rip), %rdi
	call	__cxa_finalize@PLT
.L19:
	leaq	__DTOR_LIST__(%rip), %rax
	leaq	__DTOR_END__(%rip), %rbx
	movq	%rax, %r12
	subq	%rax, %rbx
	movq	dtor_idx.4255(%rip), %rax
	sarq	$3, %rbx
	subq	$1, %rbx
	cmpq	%rbx, %rax
	jnb	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$1, %rax
	movq	%rax, dtor_idx.4255(%rip)
	call	*(%r12,%rax,8)
	movq	dtor_idx.4255(%rip), %rax
	cmpq	%rbx, %rax
	jb	.L21
.L20:
	call	deregister_tm_clones
	popq	%rbx
	movb	$1, completed.4253(%rip)
	popq	%r12
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	rep ret
#APP
	.section	.fini
	call __do_global_dtors_aux
	.text
#NO_APP
	.p2align 4,,15
	.type	frame_dummy, @function
frame_dummy:
	pushq	%rbp
	movq	%rsp, %rbp
	popq	%rbp
	jmp	register_tm_clones
#APP
	.section	.init
	call frame_dummy
	.text
#NO_APP
	.local	completed.4253
	.comm	completed.4253,1,1
	.local	dtor_idx.4255
	.comm	dtor_idx.4255,8,8
	.weak	__cxa_finalize
	.weak	_ITM_registerTMCloneTable
	.weak	_ITM_deregisterTMCloneTable
	.hidden	__DTOR_END__
	.hidden	__TMC_END__
