	.text
	.p2align 4,,15
	.globl	__getf2
	.type	__getf2, @function
__getf2:
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 42 "/root/nyanlinux/src/glibc-2.33/soft-fp/getf2.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	movq	8(%rsp), %rdx
	movq	(%rsp), %r9
	movq	16(%rsp), %rsi
	movq	%rdx, %r11
	movq	%rdx, %rdi
	shrq	$63, %rdx
	movq	%rdx, %r8
	movq	24(%rsp), %rdx
	andq	%rax, %r11
	shrq	$48, %rdi
	andq	%rdx, %rax
	movq	%rax, %rcx
	movq	%rdx, %rax
	shrq	$63, %rdx
	shrq	$48, %rax
	andl	$32767, %eax
	andl	$32767, %edi
	jne	.L2
	movq	%r11, %rbx
	orq	%r9, %rbx
	je	.L83
	movl	$2, %r10d
.L3:
	cmpq	$32767, %rax
	jne	.L12
.L28:
	movq	%rcx, %rbx
	orq	%rsi, %rbx
	jne	.L8
.L10:
	testq	%rdi, %rdi
	jne	.L29
.L12:
	movq	%r11, %rbx
	orq	%r9, %rbx
	testq	%rax, %rax
	jne	.L15
	movq	%rcx, %r14
	orq	%rsi, %r14
	je	.L84
.L15:
	testq	%rbx, %rbx
	jne	.L85
	cmpq	$1, %rdx
	sbbq	%rax, %rax
	orq	$1, %rax
.L16:
	testl	%r10d, %r10d
	jne	.L11
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rax, %rax
	jne	.L5
	movq	%rcx, %rbx
	xorl	%r10d, %r10d
	orq	%rsi, %rbx
	setne	%r10b
	addl	%r10d, %r10d
	cmpq	$32767, %rdi
	jne	.L10
.L27:
	movq	%r11, %rbx
	orq	%r9, %rbx
	jne	.L8
	cmpq	$32767, %rax
	je	.L28
.L29:
	testq	%rax, %rax
	jne	.L13
	movq	%rcx, %rbx
	orq	%rsi, %rbx
	jne	.L13
.L20:
	cmpq	$1, %r8
	sbbq	%rax, %rax
	andl	$2, %eax
	subq	$1, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%r10d, %r10d
	cmpq	$32767, %rdi
	je	.L27
	cmpq	$32767, %rax
	je	.L28
.L13:
	cmpq	%rdx, %r8
	jne	.L20
	cmpq	%rax, %rdi
	jg	.L20
.L18:
	cmpq	%rax, %rdi
	jl	.L22
	cmpq	%rcx, %r11
	ja	.L20
	sete	%al
	cmpq	%rsi, %r9
	jbe	.L21
	testb	%al, %al
	jne	.L20
.L21:
	cmpq	%rcx, %r11
	jb	.L22
	cmpq	%rsi, %r9
	jnb	.L40
	testb	%al, %al
	je	.L40
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	$1, %r8
	sbbq	%rax, %rax
	orq	$1, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%r10d, %r10d
	testq	%rax, %rax
	jne	.L3
	movq	%rcx, %rbx
	xorl	%r10d, %r10d
	orq	%rsi, %rbx
	setne	%r10b
	addl	%r10d, %r10d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L84:
	testq	%rbx, %rbx
	je	.L16
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L8:
	orl	$1, %r10d
	movq	$-2, %rax
.L11:
	movl	%r10d, %edi
	movq	%rax, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movq	(%rsp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	ret
.L40:
	xorl	%eax, %eax
	jmp	.L16
.L85:
	cmpq	%rdx, %r8
	jne	.L20
	jmp	.L18
	.size	__getf2, .-__getf2
	.globl	__gttf2
	.set	__gttf2,__getf2
