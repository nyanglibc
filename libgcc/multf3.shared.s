	.text
	.p2align 4,,15
	.globl	__multf3
	.type	__multf3, @function
__multf3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 43 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	stmxcsr	60(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	movq	8(%rsp), %rsi
	movq	(%rsp), %r9
	movq	%rsi, %r8
	andq	%rsi, %rax
	shrq	$63, %rsi
	shrq	$48, %r8
	movzbl	%sil, %edi
	movl	%esi, %ebp
	andw	$32767, %r8w
	movq	%rdi, (%rsp)
	je	.L3
	cmpw	$32767, %r8w
	movq	%rax, %r11
	je	.L4
	movq	%r9, %rax
	movabsq	$2251799813685248, %r10
	salq	$3, %r11
	shrq	$61, %rax
	movzwl	%r8w, %r8d
	xorl	%r12d, %r12d
	orq	%r10, %rax
	leaq	0(,%r9,8), %r10
	subq	$16383, %r8
	orq	%rax, %r11
	xorl	%r14d, %r14d
	xorl	%edi, %edi
.L5:
	movq	24(%rsp), %rax
	movabsq	$281474976710655, %rdx
	movq	16(%rsp), %r9
	movq	%rax, %rcx
	andq	%rax, %rdx
	shrq	$63, %rax
	shrq	$48, %rcx
	movzbl	%al, %r15d
	movq	%rdx, %rbx
	andw	$32767, %cx
	movq	%r15, 16(%rsp)
	je	.L11
	cmpw	$32767, %cx
	je	.L12
	movq	%r9, %r13
	movabsq	$2251799813685248, %rdx
	salq	$3, %rbx
	shrq	$61, %r13
	salq	$3, %r9
	movzwl	%cx, %ecx
	orq	%r13, %rdx
	leaq	-16383(%rcx,%r8), %r8
	orq	%rdx, %rbx
	cmpq	%r9, %r10
	setnb	47(%rsp)
	xorl	%r15d, %r15d
.L13:
	xorl	%eax, %esi
	cmpq	$15, %r12
	leaq	1(%r8), %rcx
	movzbl	%sil, %r13d
	ja	.L20
	leaq	.L22(%rip), %rdx
	movslq	(%rdx,%r12,4), %r12
	addq	%r12, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L20-.L22
	.long	.L21-.L22
	.long	.L21-.L22
	.long	.L23-.L22
	.long	.L24-.L22
	.long	.L24-.L22
	.long	.L25-.L22
	.long	.L23-.L22
	.long	.L24-.L22
	.long	.L25-.L22
	.long	.L24-.L22
	.long	.L23-.L22
	.long	.L26-.L22
	.long	.L26-.L22
	.long	.L26-.L22
	.long	.L27-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rax, %r10
	orq	%r9, %r10
	jne	.L158
	xorl	%r11d, %r11d
	movl	$8, %r12d
	movl	$32767, %r8d
	movl	$2, %r14d
	xorl	%edi, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rbx, %r11
	movq	%r9, %r10
	movq	%r15, %r14
.L24:
	movl	%r13d, %ebp
	andl	$1, %ebp
	cmpq	$2, %r14
	jne	.L159
	movl	$32767, %r11d
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
.L43:
	movabsq	$281474976710655, %rax
	movq	$0, 8(%rsp)
	movabsq	$-281474976710656, %rdx
	andq	%rax, %rcx
	movq	8(%rsp), %rax
	salq	$48, %r11
	salq	$63, %rbp
	movq	%r10, (%rsp)
	andq	%rdx, %rax
	movabsq	$-9223090561878065153, %rdx
	orq	%rcx, %rax
	andq	%rdx, %rax
	orq	%rax, %r11
	movabsq	$9223372036854775807, %rax
	andq	%rax, %r11
	orq	%rbp, %r11
	testl	%edi, %edi
	movq	%r11, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jne	.L84
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rax, %r10
	orq	%r9, %r10
	je	.L86
	testq	%rax, %rax
	je	.L6
	bsrq	%rax, %rdx
	xorq	$63, %rdx
	movslq	%edx, %rdx
.L7:
	leaq	-15(%rdx), %rdi
	cmpq	$60, %rdi
	jg	.L8
	leal	3(%rdi), %r8d
	movq	%r9, %r11
	movl	%r8d, %ecx
	salq	%cl, %rax
	movl	$61, %ecx
	subl	%edi, %ecx
	shrq	%cl, %r11
	movl	%r8d, %ecx
	salq	%cl, %r9
	orq	%rax, %r11
	movq	%r9, %r10
.L9:
	movq	$-16367, %r8
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	subq	%rdx, %r8
	movl	$2, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r9, %rcx
	addq	$32767, %r8
	orq	%rdx, %rcx
	je	.L160
	cmpq	%r9, %r10
	movl	$3, %r15d
	setnb	47(%rsp)
	orq	$3, %r12
	btq	$47, %rdx
	jc	.L13
	orl	$1, %edi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	orq	%r9, %rbx
	je	.L161
	testq	%rdx, %rdx
	je	.L15
	bsrq	%rdx, %r13
	xorq	$63, %r13
	movslq	%r13d, %r13
.L16:
	leaq	-15(%r13), %rbx
	cmpq	$60, %rbx
	jg	.L17
	leal	3(%rbx), %r15d
	movl	%r15d, %ecx
	salq	%cl, %rdx
	movl	$61, %ecx
	subl	%ebx, %ecx
	movq	%r9, %rbx
	shrq	%cl, %rbx
	movl	%r15d, %ecx
	salq	%cl, %r9
	orq	%rdx, %rbx
	cmpq	%r9, %r10
	setnb	47(%rsp)
.L18:
	subq	%r13, %r8
	orl	$2, %edi
	xorl	%r15d, %r15d
	subq	$16367, %r8
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L159:
	cmpq	$3, %r14
	je	.L44
	cmpq	$1, %r14
	jne	.L148
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L86:
	xorl	%r11d, %r11d
	movl	$4, %r12d
	xorl	%r8d, %r8d
	movl	$1, %r14d
	xorl	%edi, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rax, %rdi
	movq	%r9, %r10
	movl	$12, %r12d
	shrq	$47, %rdi
	movl	$32767, %r8d
	movl	$3, %r14d
	xorq	$1, %rdi
	andl	$1, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L160:
	orq	$2, %r12
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movb	$1, 47(%rsp)
	movl	$2, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L161:
	orq	$1, %r12
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	movb	$1, 47(%rsp)
	movl	$1, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r10, %rax
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	mulq %r9
# 0 "" 2
#NO_APP
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	%r10, %rax
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	mulq %rbx
# 0 "" 2
#NO_APP
	movq	%rax, %r10
	movq	%rdx, %rbp
	movq	%r11, %rax
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	mulq %r9
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
	movq	%rax, %r15
	movq	%r11, %rax
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	mulq %rbx
# 0 "" 2
#NO_APP
	addq	%r14, %r10
	setc	%r14b
	xorl	%ebx, %ebx
	addq	%rax, %rbp
	movzbl	%r14b, %r14d
	setc	%bl
	xorl	%r11d, %r11d
	addq	%r14, %rbp
	setc	%r11b
	orq	%rbx, %r11
	addq	%r11, %rdx
	addq	%r10, %r15
	setc	%r10b
	xorl	%eax, %eax
	addq	%rbp, %r9
	movzbl	%r10b, %r10d
	setc	%al
	xorl	%r11d, %r11d
	addq	%r10, %r9
	setc	%r11b
	xorl	%r10d, %r10d
	orq	%rax, %r11
	movq	%r9, %rax
	addq	%rdx, %r11
	shrq	$51, %rax
	salq	$13, %r11
	orq	%rax, %r11
	movq	%r15, %rax
	salq	$13, %rax
	orq	%r12, %rax
	setne	%r10b
	shrq	$51, %r15
	salq	$13, %r9
	orq	%r10, %r15
	orq	%r9, %r15
	btq	$52, %r11
	movq	%r15, %r10
	jnc	.L89
	movq	%r15, %rax
	andl	$1, %r10d
	shrq	%rax
	orq	%rax, %r10
	movq	%r11, %rax
	shrq	%r11
	salq	$63, %rax
	orq	%rax, %r10
.L40:
	leaq	16383(%rcx), %rdx
	testq	%rdx, %rdx
	jle	.L46
	testb	$7, %r10b
	je	.L47
	movl	60(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L48
	cmpl	$16384, %eax
	je	.L49
	testl	%eax, %eax
	jne	.L47
	movq	%r10, %rax
	andl	$15, %eax
	cmpq	$4, %rax
	je	.L47
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $4,%r10
	adcq $0,%r11
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L47:
	btq	$52, %r11
	jnc	.L51
	movabsq	$-4503599627370497, %rax
	leaq	16384(%rcx), %rdx
	andq	%rax, %r11
.L51:
	cmpq	$32766, %rdx
	jg	.L52
	movq	%r11, %rax
	salq	$13, %r11
	shrq	$3, %r10
	movq	%r11, %rcx
	salq	$61, %rax
	movl	%edx, %r11d
	orq	%rax, %r10
	shrq	$16, %rcx
	andw	$32767, %r11w
	movl	%esi, %ebp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L25:
	orl	$1, %edi
	movl	$1, %ebp
	movabsq	$140737488355328, %rcx
	xorl	%r10d, %r10d
.L42:
	movl	$32767, %r11d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%rsp), %r13
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rbx, %r11
	movq	%r9, %r10
	movq	16(%rsp), %r13
	movq	%r15, %r14
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L27:
	cmpq	%rbx, %r11
	movabsq	$140737488355328, %rcx
	ja	.L41
	orq	%rbx, %rcx
	movabsq	$281474976710655, %rdx
	andq	%rdx, %rcx
	cmpq	%rbx, %r11
	sete	%dl
	andb	47(%rsp), %dl
	cmove	%eax, %ebp
	cmove	%r9, %r10
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L52:
	movl	60(%rsp), %r8d
	andl	$24576, %r8d
	cmpl	$8192, %r8d
	je	.L54
	cmpl	$16384, %r8d
	je	.L55
	testl	%r8d, %r8d
	movl	$0, %r10d
	movabsq	$281474976710655, %rax
	cmove	%r10, %rax
	cmpl	$1, %r8d
	sbbl	%edx, %edx
	movq	%rax, %rcx
	notl	%edx
	addw	$32767, %dx
	cmpl	$1, %r8d
	sbbq	%r10, %r10
	notq	%r10
.L53:
	orl	$40, %edi
.L57:
	movq	%r10, (%rsp)
	movabsq	$281474976710655, %r10
	movq	$0, 8(%rsp)
	andq	%rcx, %r10
	movq	8(%rsp), %rcx
	movabsq	$-281474976710656, %rax
	salq	$63, %rsi
	andq	%rax, %rcx
	movabsq	$-9223090561878065153, %rax
	orq	%r10, %rcx
	movq	%rdx, %r10
	andl	$32767, %r10d
	andq	%rax, %rcx
	movabsq	$9223372036854775807, %rax
	salq	$48, %r10
	orq	%rcx, %r10
	andq	%rax, %r10
	orq	%rsi, %r10
	movq	%r10, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L84:
	movaps	%xmm0, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movdqa	(%rsp), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leal	-61(%rbx), %ecx
	movq	%r9, %rbx
	movb	$1, 47(%rsp)
	xorl	%r9d, %r9d
	salq	%cl, %rbx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L15:
	bsrq	%r9, %r13
	xorq	$63, %r13
	movslq	%r13d, %r13
	addq	$64, %r13
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L8:
	leal	-61(%rdi), %ecx
	xorl	%r10d, %r10d
	salq	%cl, %r9
	movq	%r9, %r11
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	bsrq	%r9, %rdx
	xorq	$63, %rdx
	movslq	%edx, %rdx
	addq	$64, %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$1, %r9d
	movq	%r9, %r8
	subq	%rdx, %r8
	testq	%rdx, %rdx
	jne	.L58
	testb	$7, %r10b
	movq	%r11, %rax
	je	.L59
	movl	60(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L60
	cmpl	$16384, %eax
	je	.L61
	testl	%eax, %eax
	je	.L162
	movq	%r11, %rax
	.p2align 4,,10
	.p2align 3
.L59:
	shrq	$52, %rax
	xorq	$1, %rax
	andl	$1, %eax
.L63:
	movl	$64, %edx
	movq	%r11, %r9
	movq	%r10, %rbx
	subl	%r8d, %edx
	movl	%edx, %ecx
	salq	%cl, %r9
	movl	%r8d, %ecx
	shrq	%cl, %rbx
	movl	%edx, %ecx
	salq	%cl, %r10
	orq	%rbx, %r9
	movl	%r8d, %ecx
	testq	%r10, %r10
	setne	%r10b
	shrq	%cl, %r11
	movzbl	%r10b, %r10d
	orq	%r9, %r10
	testb	$7, %r10b
	je	.L67
	movl	60(%rsp), %edx
	orl	$32, %edi
	andl	$24576, %edx
	cmpl	$8192, %edx
	je	.L71
	cmpl	$16384, %edx
	je	.L72
	testl	%edx, %edx
	je	.L73
	.p2align 4,,10
	.p2align 3
.L67:
	btq	$51, %r11
	jnc	.L163
	orl	$32, %edi
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	testl	%eax, %eax
	movl	$1, %edx
	je	.L57
.L76:
	orl	$16, %edi
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%r8, %rcx
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	orq	%r11, %rcx
	movabsq	$281474976710655, %rax
	andq	%rax, %rcx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L58:
	cmpq	$116, %r8
	jle	.L164
	xorl	%ecx, %ecx
	orq	%r11, %r10
	je	.L78
	movl	60(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L80
	cmpl	$16384, %eax
	je	.L81
	xorl	%r10d, %r10d
	testl	%eax, %eax
	jne	.L78
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $4,%r9
	adcq $0,%rcx
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	shrq	$3, %r9
	movq	%r9, %r10
	andq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L78:
	orl	$16, %edi
	xorl	%edx, %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L54:
	testq	%r13, %r13
	movabsq	$281474976710655, %r10
	movl	$0, %eax
	cmove	%r10, %rax
	cmpq	$1, %r13
	sbbl	%edx, %edx
	movq	%rax, %rcx
	addw	$32767, %dx
	cmpq	$1, %r13
	sbbq	%r10, %r10
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L48:
	testq	%r13, %r13
	je	.L47
.L153:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $8,%r10
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L148:
	movl	%ebp, %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L49:
	testq	%r13, %r13
	jne	.L47
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L55:
	testq	%r13, %r13
	movabsq	$281474976710655, %r10
	movl	$0, %eax
	movl	%r13d, %edx
	cmovne	%r10, %rax
	movq	%r13, %r10
	negl	%edx
	movq	%rax, %rcx
	negq	%r10
	addw	$32767, %dx
	jmp	.L53
.L163:
	movq	%r11, %rcx
	shrq	$3, %r10
	salq	$61, %r11
	salq	$13, %rcx
	orq	%r11, %r10
	shrq	$16, %rcx
	testl	%eax, %eax
	jne	.L75
.L103:
	movl	%esi, %ebp
	xorl	%r11d, %r11d
	jmp	.L43
.L110:
	xorl	%r11d, %r11d
	movl	$1, %eax
.L72:
	testq	%r13, %r13
	jne	.L67
.L155:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $8,%r10
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L67
.L109:
	xorl	%r11d, %r11d
	movl	$1, %eax
.L71:
	testq	%r13, %r13
	je	.L67
	jmp	.L155
.L108:
	xorl	%r11d, %r11d
	movl	$1, %eax
.L73:
	movq	%r10, %rdx
	andl	$15, %edx
	cmpq	$4, %rdx
	je	.L67
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $4,%r10
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L67
.L81:
	xorl	%r10d, %r10d
	testq	%r13, %r13
	jne	.L78
.L156:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $8,%r9
	adcq $0,%rcx
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	shrq	$3, %r9
	movq	%r9, %r10
	andq	%rax, %rcx
	jmp	.L78
.L80:
	xorl	%r10d, %r10d
	testq	%r13, %r13
	je	.L78
	jmp	.L156
.L162:
	movq	%r10, %rdx
	movq	%r11, %rax
	andl	$15, %edx
	cmpq	$4, %rdx
	je	.L59
	movq	%r10, %rdx
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $4,%rdx
	adcq $0,%rax
# 0 "" 2
#NO_APP
	jmp	.L59
.L61:
	testq	%r13, %r13
	movq	%r11, %rax
	jne	.L59
.L154:
	movq	%r10, %rdx
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/multf3.c" 1
	addq $8,%rdx
	adcq $0,%rax
# 0 "" 2
#NO_APP
	jmp	.L59
.L60:
	testq	%r13, %r13
	movq	%r11, %rax
	je	.L59
	jmp	.L154
.L44:
	movabsq	$140737488355328, %rcx
	movabsq	$281474976710655, %rax
	orq	%r11, %rcx
	andq	%rax, %rcx
	jmp	.L42
.L164:
	cmpq	$63, %r8
	jle	.L165
	leal	-64(%r8), %ecx
	movq	%r11, %rdx
	shrq	%cl, %rdx
	cmpq	$64, %r8
	je	.L68
	movl	$128, %ecx
	subl	%r8d, %ecx
	salq	%cl, %r11
	orq	%r11, %r10
.L68:
	testq	%r10, %r10
	setne	%r10b
	movzbl	%r10b, %r10d
	orq	%rdx, %r10
	testb	$7, %r10b
	je	.L166
	movl	60(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L109
	cmpl	$16384, %eax
	je	.L110
	testl	%eax, %eax
	je	.L108
	shrq	$3, %r10
	xorl	%ecx, %ecx
.L75:
	testb	$32, %dil
	jne	.L102
.L77:
	testb	$8, 61(%rsp)
	jne	.L103
.L102:
	xorl	%edx, %edx
	jmp	.L76
.L166:
	shrq	$3, %r10
	xorl	%ecx, %ecx
	jmp	.L77
.L165:
	movl	$1, %eax
	jmp	.L63
	.size	__multf3, .-__multf3
