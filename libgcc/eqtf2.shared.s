	.text
	.p2align 4,,15
	.globl	__eqtf2
	.type	__eqtf2, @function
__eqtf2:
	pushq	%rbx
	subq	$48, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 42 "/root/nyanlinux/src/glibc-2.33/soft-fp/eqtf2.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rsi
	movq	8(%rsp), %rdx
	movq	(%rsp), %r8
	movq	16(%rsp), %r11
	movq	%rdx, %rbx
	movq	%rdx, %rax
	movq	%rdx, %rcx
	movq	24(%rsp), %rdx
	andq	%rsi, %rbx
	shrq	$48, %rax
	shrq	$63, %rcx
	andq	%rdx, %rsi
	movq	%rdx, %r9
	shrq	$48, %r9
	movq	%rsi, %r10
	movq	%rdx, %rsi
	andl	$32767, %r9d
	shrq	$63, %rsi
	andl	$32767, %eax
	movq	%rax, %rdx
	jne	.L2
	movq	%rbx, %rax
	orq	%r8, %rax
	je	.L57
	movl	$2, %edi
.L3:
	cmpq	$32767, %r9
	je	.L22
.L9:
	cmpq	%r9, %rdx
	movl	$1, %eax
	je	.L21
.L13:
	testl	%edi, %edi
	jne	.L14
.L1:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%r9, %r9
	jne	.L5
	movq	%r10, %rax
	xorl	%edi, %edi
	orq	%r11, %rax
	setne	%dil
	addl	%edi, %edi
	cmpq	$32767, %rdx
	jne	.L9
	movq	%rbx, %rax
	orq	%r8, %rax
	jne	.L7
.L59:
	cmpq	$32767, %r9
	jne	.L29
	movq	%r10, %rax
	orq	%r11, %rax
	je	.L9
.L10:
	orq	%rbx, %r8
	je	.L23
	.p2align 4,,10
	.p2align 3
.L7:
	movabsq	$140737488355328, %rax
	testq	%rax, %rbx
	jne	.L58
.L12:
	orl	$1, %edi
	movl	$1, %eax
.L14:
	movq	%rax, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movq	(%rsp), %rax
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%edi, %edi
	cmpq	$32767, %rax
	jne	.L3
	movq	%rbx, %rax
	orq	%r8, %rax
	jne	.L7
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L57:
	testq	%r9, %r9
	jne	.L60
	movq	%r10, %rax
	orq	%r11, %rax
	jne	.L34
	xorl	%edi, %edi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$2, %edi
.L21:
	cmpq	%r10, %rbx
	jne	.L29
	cmpq	%r11, %r8
	jne	.L29
	xorl	%eax, %eax
	cmpb	%sil, %cl
	je	.L13
	testq	%rdx, %rdx
	movl	$1, %eax
	jne	.L13
	xorl	%eax, %eax
	orq	%r8, %rbx
	setne	%al
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	$32767, %r9
	movl	$1, %eax
	jne	.L1
	xorl	%edi, %edi
.L22:
	movq	%r10, %rax
	orq	%r11, %rax
	je	.L9
	cmpq	$32767, %rdx
	je	.L10
.L23:
	movabsq	$140737488355328, %rax
	testq	%rax, %r10
	je	.L12
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$1, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L58:
	cmpq	$32767, %r9
	movl	$1, %eax
	jne	.L13
	orq	%r10, %r11
	je	.L13
	jmp	.L23
	.size	__eqtf2, .-__eqtf2
	.globl	__netf2
	.set	__netf2,__eqtf2
