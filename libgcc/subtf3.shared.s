	.text
	.p2align 4,,15
	.globl	__subtf3
	.type	__subtf3, @function
__subtf3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 43 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rbx
	movq	8(%rsp), %rdx
	movq	(%rsp), %rcx
	movq	16(%rsp), %r14
	movq	%rdx, %rsi
	movq	%rdx, %rbp
	movq	%rcx, %rax
	andq	%rbx, %rdx
	shrq	$61, %rax
	shrq	$48, %rsi
	salq	$3, %rdx
	shrq	$63, %rbp
	andw	$32767, %si
	orq	%rax, %rdx
	movq	24(%rsp), %rax
	movzwl	%si, %r9d
	movl	%ebp, %r10d
	movzbl	%bpl, %r13d
	leaq	0(,%rcx,8), %r8
	movq	%rax, %rdi
	movq	%rax, %r11
	andq	%rbx, %rax
	shrq	$48, %rdi
	movq	%r14, %rbx
	salq	$3, %rax
	andw	$32767, %di
	shrq	$61, %rbx
	shrq	$63, %r11
	movzwl	%di, %r12d
	orq	%rbx, %rax
	leaq	0(,%r14,8), %rbx
	cmpq	$32767, %r12
	je	.L310
.L2:
	xorq	$1, %r11
.L3:
	movzwl	%si, %esi
	movzwl	%di, %edi
	subl	%edi, %esi
	cmpq	%r13, %r11
	je	.L311
	cmpl	$0, %esi
	jle	.L62
	testq	%r12, %r12
	jne	.L63
	movq	%rax, %rcx
	orq	%rbx, %rcx
	je	.L307
	subl	$1, %esi
	jne	.L65
	movl	$2, %edi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %rbx,%r8
	sbbq %rax,%rdx
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L66:
	btq	$51, %rdx
	jnc	.L110
	movabsq	$2251799813685247, %rsi
	andq	%rdx, %rsi
.L102:
	testq	%rsi, %rsi
	je	.L103
	bsrq	%rsi, %rcx
	xorl	$63, %ecx
.L104:
	leal	-12(%rcx), %r11d
	cmpl	$63, %r11d
	jg	.L105
	movl	%r11d, %ecx
	movq	%r8, %rdx
	salq	%cl, %rsi
	movl	$64, %ecx
	subl	%r11d, %ecx
	shrq	%cl, %rdx
	movl	%r11d, %ecx
	orq	%rdx, %rsi
	salq	%cl, %r8
.L106:
	movslq	%r11d, %rax
	cmpq	%r9, %rax
	jl	.L107
	subl	%r9d, %r11d
	leal	1(%r11), %eax
	cmpl	$63, %eax
	jg	.L108
	movl	$64, %edx
	movq	%rsi, %r9
	movq	%r8, %rbx
	subl	%eax, %edx
	movl	%edx, %ecx
	salq	%cl, %r9
	movl	%eax, %ecx
	shrq	%cl, %rbx
	movl	%edx, %ecx
	salq	%cl, %r8
	orq	%rbx, %r9
	movl	%eax, %ecx
	testq	%r8, %r8
	setne	%r8b
	shrq	%cl, %rsi
	movzbl	%r8b, %r8d
	movq	%rsi, %rdx
	orq	%r9, %r8
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L62:
	jne	.L312
	leaq	1(%r9), %rdi
	andl	$32767, %edi
	cmpq	$1, %rdi
	jle	.L313
	movq	%rdx, %rsi
	movq	%r8, %rcx
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %rbx,%rcx
	sbbq %rax,%rsi
# 0 "" 2
#NO_APP
	btq	$51, %rsi
	jc	.L314
	movq	%rsi, %rax
	orq	%rcx, %rax
	je	.L87
	movq	%rcx, %r8
	xorl	%edi, %edi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L311:
	cmpl	$0, %esi
	jle	.L5
	testq	%r12, %r12
	je	.L315
	cmpq	$32767, %r9
	je	.L316
	movabsq	$2251799813685248, %rcx
	xorl	%edi, %edi
	orq	%rcx, %rax
.L13:
	cmpl	$116, %esi
	jle	.L317
	orq	%rbx, %rax
	setne	%bl
	xorl	%eax, %eax
	movzbl	%bl, %ebx
.L21:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq %rbx,%r8
	adcq %rax,%rdx
# 0 "" 2
#NO_APP
.L12:
	btq	$51, %rdx
	jnc	.L110
	addq	$1, %r9
	cmpq	$32767, %r9
	je	.L58
	movabsq	$-2251799813685249, %rax
	xorl	%esi, %esi
	andq	%rax, %rdx
	movq	%r8, %rax
	andl	$1, %r8d
	shrq	%rax
	orq	%rax, %r8
	movq	%rdx, %rax
	shrq	%rdx
	salq	$63, %rax
	orq	%rax, %r8
	movq	%r8, %rbp
	andl	$7, %ebp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%rax, %r15
	orq	%rbx, %r15
	je	.L2
	movzbl	%r11b, %r11d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$32767, %r9
	je	.L318
	movabsq	$2251799813685248, %rcx
	xorl	%edi, %edi
	orq	%rcx, %rax
.L67:
	cmpl	$116, %esi
	jg	.L69
	cmpl	$63, %esi
	jle	.L319
	leal	-64(%rsi), %ecx
	movq	%rax, %rbp
	shrq	%cl, %rbp
	cmpl	$64, %esi
	je	.L72
	movl	$128, %ecx
	subl	%esi, %ecx
	salq	%cl, %rax
	orq	%rax, %rbx
.L72:
	xorl	%r11d, %r11d
	testq	%rbx, %rbx
	setne	%r11b
	xorl	%eax, %eax
	orq	%rbp, %r11
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rax, %rdi
	orq	%rbx, %rdi
	je	.L307
	subl	$1, %esi
	je	.L320
	cmpq	$32767, %r9
	movl	$2, %edi
	jne	.L13
	movq	%rdx, %rax
	orq	%r8, %rax
	je	.L14
.L300:
	movabsq	$1125899906842624, %rax
	xorl	%esi, %esi
	andq	%rdx, %rax
	cmpq	$1, %rax
	sbbl	%edi, %edi
	notl	%edi
	addl	$3, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L307:
	xorl	%edi, %edi
	cmpq	$32767, %r9
	je	.L321
.L8:
.L110:
	testq	%r9, %r9
	jne	.L322
.L42:
	movq	%r8, %rbp
	orq	%rdx, %rbp
	je	.L166
	movq	%r8, %rbp
	andl	$7, %ebp
.L135:
	movq	%r8, %rax
	shrq	$63, %rax
	leaq	(%rax,%rdx,2), %rsi
	leaq	(%r8,%r8), %rax
	testb	$6, %al
	je	.L111
	movl	44(%rsp), %ecx
	orl	$32, %edi
	andl	$24576, %ecx
	cmpl	$8192, %ecx
	je	.L112
	cmpl	$16384, %ecx
	je	.L113
	testl	%ecx, %ecx
	je	.L323
	.p2align 4,,10
	.p2align 3
.L111:
	shrq	$52, %rsi
	xorl	%r9d, %r9d
	xorq	$1, %rsi
	andl	$1, %esi
.L43:
	testq	%rbp, %rbp
	je	.L10
	movl	44(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L116
	cmpl	$16384, %eax
	je	.L117
	testl	%eax, %eax
	je	.L324
.L121:
	movabsq	$2251799813685248, %rax
	andq	%rdx, %rax
	testl	%esi, %esi
	je	.L120
.L119:
	orl	$16, %edi
.L120:
	testq	%rax, %rax
	je	.L325
.L122:
	leaq	1(%r9), %rax
	cmpq	$32767, %rax
	je	.L326
	movabsq	$-2251799813685249, %rcx
	shrq	$3, %r8
	andw	$32767, %ax
	andq	%rcx, %rdx
	movq	%rdx, %rbp
	salq	$13, %rdx
	salq	$61, %rbp
	shrq	$16, %rdx
	orq	%r8, %rbp
.L130:
	movabsq	$281474976710655, %rcx
	movq	$0, 8(%rsp)
	movabsq	$-281474976710656, %rsi
	andq	%rdx, %rcx
	movq	8(%rsp), %rdx
	salq	$48, %rax
	movzbl	%r10b, %r10d
	movq	%rbp, (%rsp)
	salq	$63, %r10
	andq	%rsi, %rdx
	orq	%rcx, %rdx
	movabsq	$-9223090561878065153, %rcx
	andq	%rcx, %rdx
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	orq	%r10, %rax
	testl	%edi, %edi
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jne	.L139
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	movl	%r11d, %r10d
	andl	$1, %r10d
	testq	%r9, %r9
	je	.L327
	cmpq	$32767, %r12
	je	.L328
	movabsq	$2251799813685248, %rcx
	negl	%esi
	xorl	%edi, %edi
	orq	%rcx, %rdx
.L78:
	cmpl	$116, %esi
	jg	.L80
	cmpl	$63, %esi
	jg	.L81
	movl	$64, %ebp
	movq	%rdx, %r9
	movq	%r8, %r15
	subl	%esi, %ebp
	movl	%ebp, %ecx
	salq	%cl, %r9
	movl	%esi, %ecx
	shrq	%cl, %r15
	movl	%ebp, %ecx
	salq	%cl, %r8
	orq	%r15, %r9
	xorl	%ecx, %ecx
	testq	%r8, %r8
	setne	%cl
	orq	%rcx, %r9
	movl	%esi, %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
.L82:
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%r11, %r13
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %r9,%r8
	sbbq %rcx,%rdx
# 0 "" 2
#NO_APP
	movq	%r12, %r9
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%rdx, %rax
	orq	%r8, %rax
	je	.L18
.L296:
	movq	%rdx, %rdi
	xorl	%esi, %esi
	shrq	$50, %rdi
	xorq	$1, %rdi
	andl	$1, %edi
.L10:
	movabsq	$2251799813685248, %rax
	andq	%rdx, %rax
	testl	%esi, %esi
	je	.L120
	testb	$32, %dil
	jne	.L119
	testb	$8, 45(%rsp)
	je	.L119
	testq	%rax, %rax
	jne	.L122
.L325:
	movq	%rdx, %rbp
	shrq	$3, %r8
	shrq	$3, %rdx
	salq	$61, %rbp
	orq	%r8, %rbp
	cmpq	$32767, %r9
	jne	.L41
.L123:
	movq	%rdx, %rax
	orq	%rbp, %rax
	je	.L329
.L97:
	movabsq	$140737488355328, %rax
	orq	%rax, %rdx
	movabsq	$281474976710655, %rax
	andq	%rax, %rdx
	movl	$32767, %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%rdx, %rax
	orq	%r8, %rax
	jne	.L296
.L303:
	movl	%r11d, %r10d
	andl	$1, %r10d
.L18:
	movq	$0, 8(%rsp)
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rdx
	movzbl	%r10b, %r10d
	movq	$0, (%rsp)
	salq	$63, %r10
	andq	%rdx, %rax
	movabsq	$9223090561878065152, %rdx
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	orq	%r10, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L166:
	xorl	%edx, %edx
	xorl	%r9d, %r9d
.L41:
	movabsq	$281474976710655, %rax
	andq	%rax, %rdx
	movl	%r9d, %eax
	andw	$32767, %ax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L326:
	movl	44(%rsp), %eax
	andl	$24576, %eax
	je	.L127
	cmpl	$16384, %eax
	je	.L330
	orl	$40, %edi
	testq	%r13, %r13
	je	.L301
	cmpl	$8192, %eax
	jne	.L301
.L30:
	movq	$0, 8(%rsp)
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rdx
	movzbl	%r10b, %r10d
	movq	$0, (%rsp)
	salq	$63, %r10
	andq	%rdx, %rax
	movabsq	$9223090561878065152, %rdx
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	orq	%r10, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L139:
	movaps	%xmm0, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movdqa	(%rsp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L174:
	movq	$-1, %rdx
	movl	$32766, %r9d
	xorl	%esi, %esi
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L116:
	testq	%r13, %r13
	je	.L121
.L302:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq $8,%r8
	adcq $0,%rdx
# 0 "" 2
#NO_APP
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L58:
	movl	44(%rsp), %eax
	orl	$40, %edi
	andl	$24576, %eax
	je	.L14
	cmpl	$16384, %eax
	je	.L331
	cmpl	$8192, %eax
	jne	.L57
	testb	%bpl, %bpl
	jne	.L154
.L57:
	cmpl	$8192, %eax
	je	.L174
	cmpl	$16384, %eax
	jne	.L292
.L173:
	movq	$-1, %rdx
	movl	$32766, %r9d
	xorl	%esi, %esi
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L117:
	testq	%r13, %r13
	je	.L302
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L5:
	jne	.L332
	leaq	1(%r9), %rdi
	movq	%rdi, %rbp
	andl	$32767, %ebp
	cmpq	$1, %rbp
	jle	.L333
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq %rbx,%r8
	adcq %rax,%rdx
# 0 "" 2
#NO_APP
	cmpq	$32767, %rdi
	je	.L53
	movq	%r8, %rax
	andl	$1, %r8d
	movq	%rdi, %r9
	shrq	%rax
	xorl	%edi, %edi
	orq	%rax, %r8
	movq	%rdx, %rax
	shrq	%rdx
	salq	$63, %rax
	orq	%rax, %r8
	movq	%r8, %rbp
	andl	$7, %ebp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L324:
	movq	%r8, %rax
	andl	$15, %eax
	cmpq	$4, %rax
	je	.L121
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq $4,%r8
	adcq $0,%rdx
# 0 "" 2
#NO_APP
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%r8, %rbp
	xorl	%esi, %esi
	andl	$7, %ebp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L107:
	movabsq	$-2251799813685249, %rdx
	subq	%rax, %r9
	andq	%rsi, %rdx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r8, %rsi
	subl	$76, %ecx
	xorl	%r8d, %r8d
	salq	%cl, %rsi
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L103:
	bsrq	%r8, %rcx
	xorq	$63, %rcx
	addl	$64, %ecx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L127:
	orl	$40, %edi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%r11d, %r11d
	orq	%rbx, %rax
	setne	%r11b
	xorl	%eax, %eax
.L71:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %r11,%r8
	sbbq %rax,%rdx
# 0 "" 2
#NO_APP
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L313:
	movq	%rdx, %r15
	movq	%rax, %rbp
	orq	%r8, %r15
	orq	%rbx, %rbp
	testq	%r9, %r9
	je	.L334
	cmpq	$32767, %r9
	je	.L335
	cmpq	$32767, %r12
	je	.L168
	testq	%r15, %r15
	je	.L133
	xorl	%edi, %edi
.L93:
	testq	%rbp, %rbp
	je	.L299
.L98:
	movq	%rdx, %r8
	movabsq	$2305843009213693951, %r9
	shrq	$3, %rdx
	salq	$61, %r8
	andq	%r9, %rcx
	orq	%r8, %rcx
	movq	%rax, %r8
	shrq	$3, %r8
	cmpq	%r8, %rdx
	ja	.L99
	andq	%r9, %r14
	salq	$61, %rax
	orq	%r14, %rax
	cmpq	%r8, %rdx
	jne	.L176
	cmpq	%rax, %rcx
	movq	%r8, %rdx
	jbe	.L176
.L99:
	movq	%rcx, %rax
	salq	$3, %rdx
	movl	%r13d, %r10d
	shrq	$61, %rax
	leaq	0(,%rcx,8), %r8
	andl	$1, %r10d
	orq	%rax, %rdx
	movl	$32767, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L321:
	movq	%rdx, %rax
	orq	%r8, %rax
	jne	.L296
.L9:
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L65:
	cmpq	$32767, %r9
	movl	$2, %edi
	jne	.L67
	movq	%rdx, %rax
	orq	%r8, %rax
	je	.L30
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L108:
	leal	-64(%rax), %ecx
	movq	%rsi, %r9
	shrq	%cl, %r9
	cmpl	$64, %eax
	je	.L109
	movl	$128, %ecx
	subl	%eax, %ecx
	salq	%cl, %rsi
	orq	%rsi, %r8
.L109:
	testq	%r8, %r8
	setne	%r8b
	xorl	%edx, %edx
	movzbl	%r8b, %r8d
	orq	%r9, %r8
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L314:
	movl	%r11d, %r10d
	movq	%r11, %r13
	xorl	%edi, %edi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %r8,%rbx
	sbbq %rdx,%rax
# 0 "" 2
#NO_APP
	andl	$1, %r10d
	movq	%rax, %rsi
	movq	%rbx, %r8
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L317:
	cmpl	$63, %esi
	jg	.L20
	movl	$64, %r14d
	movq	%rax, %r12
	movq	%rbx, %r15
	subl	%esi, %r14d
	movl	%r14d, %ecx
	salq	%cl, %r12
	movl	%esi, %ecx
	shrq	%cl, %r15
	movl	%r14d, %ecx
	salq	%cl, %rbx
	orq	%r15, %r12
	movl	%esi, %ecx
	testq	%rbx, %rbx
	setne	%bl
	shrq	%cl, %rax
	movzbl	%bl, %ebx
	orq	%r12, %rbx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L319:
	movl	$64, %ebp
	movq	%rax, %r11
	movq	%rbx, %r15
	subl	%esi, %ebp
	movl	%ebp, %ecx
	salq	%cl, %r11
	movl	%esi, %ecx
	shrq	%cl, %r15
	movl	%ebp, %ecx
	salq	%cl, %rbx
	orq	%r15, %r11
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	setne	%cl
	orq	%rcx, %r11
	movl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%rdx, %rdi
	orq	%r8, %rdi
	jne	.L75
	cmpq	$32767, %r12
	je	.L336
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%r12, %r9
	movq	%r11, %r13
	xorl	%edi, %edi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L334:
	testq	%r15, %r15
	jne	.L86
	testq	%rbp, %rbp
	je	.L87
	movl	%r11d, %r10d
	xorl	%ebp, %ebp
	movq	%rax, %rdx
	andl	$1, %r10d
	movq	%rbx, %r8
	movq	%r11, %r13
.L40:
	movl	$2, %edi
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L320:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq %rbx,%r8
	adcq %rax,%rdx
# 0 "" 2
#NO_APP
	movl	$2, %edi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L323:
	movq	%rax, %rcx
	andl	$15, %ecx
	cmpq	$4, %rcx
	je	.L111
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq $4,%rax
	adcq $0,%rsi
# 0 "" 2
#NO_APP
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L53:
	movl	44(%rsp), %eax
	andl	$24576, %eax
	jne	.L54
	movl	%r11d, %r10d
	movl	$40, %edi
	andl	$1, %r10d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L330:
	testq	%r13, %r13
	je	.L127
	orl	$40, %edi
.L301:
	movabsq	$2305843009213693951, %rdx
	movq	$-1, %rbp
	movl	$32766, %r9d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L332:
	testq	%r9, %r9
	jne	.L24
	movq	%rdx, %rcx
	orq	%r8, %rcx
	jne	.L25
	cmpq	$32767, %r12
	je	.L337
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%r12, %r9
	xorl	%edi, %edi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L87:
	movl	44(%rsp), %eax
	andl	$24576, %eax
	cmpl	$8192, %eax
	sete	%r10b
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	xorl	%r9d, %r9d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L113:
	testq	%r13, %r13
	jne	.L111
.L294:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq $8,%rax
	adcq $0,%rsi
# 0 "" 2
#NO_APP
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L112:
	testq	%r13, %r13
	je	.L111
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L333:
	movq	%rdx, %r15
	orq	%r8, %r15
	testq	%r9, %r9
	jne	.L38
	movq	%rax, %rbp
	orq	%rbx, %rbp
	testq	%r15, %r15
	jne	.L39
	testq	%rbp, %rbp
	jne	.L144
	movl	%r11d, %r10d
	xorl	%edx, %edx
	xorl	%edi, %edi
	andl	$1, %r10d
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L20:
	leal	-64(%rsi), %ecx
	movq	%rax, %r12
	shrq	%cl, %r12
	cmpl	$64, %esi
	je	.L22
	movl	$128, %ecx
	subl	%esi, %ecx
	salq	%cl, %rax
	orq	%rax, %rbx
.L22:
	testq	%rbx, %rbx
	setne	%bl
	xorl	%eax, %eax
	movzbl	%bl, %ebx
	orq	%r12, %rbx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$32767, %r12
	je	.L338
	movabsq	$2251799813685248, %rcx
	negl	%esi
	xorl	%edi, %edi
	orq	%rcx, %rdx
.L28:
	cmpl	$116, %esi
	jg	.L33
	cmpl	$63, %esi
	jg	.L34
	movl	$64, %r14d
	movq	%rdx, %r9
	movq	%r8, %r15
	subl	%esi, %r14d
	movl	%r14d, %ecx
	salq	%cl, %r9
	movl	%esi, %ecx
	shrq	%cl, %r15
	movl	%r14d, %ecx
	salq	%cl, %r8
	orq	%r15, %r9
	xorl	%ecx, %ecx
	testq	%r8, %r8
	setne	%cl
	orq	%rcx, %r9
	movl	%esi, %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
.L35:
	movq	%rax, %rdx
	movq	%rbx, %r8
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq %r9,%r8
	adcq %rcx,%rdx
# 0 "" 2
#NO_APP
	movq	%r12, %r9
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$-1, %esi
	je	.L339
	cmpq	$32767, %r12
	je	.L77
	notl	%esi
	movl	$2, %edi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%r9d, %r9d
	orq	%r8, %rdx
	setne	%r9b
	xorl	%ecx, %ecx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r11d, %r10d
	andl	$1, %r10d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L328:
	movq	%rax, %rcx
	orq	%rbx, %rcx
	je	.L18
.L295:
	movq	%rax, %rdi
	movq	%rax, %rdx
	movq	%rbx, %r8
	shrq	$50, %rdi
	movl	$32767, %r9d
	movq	%r11, %r13
	xorq	$1, %rdi
	xorl	%esi, %esi
	andl	$1, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L38:
	cmpq	$32767, %r9
	je	.L340
	cmpq	$32767, %r12
	je	.L341
	testq	%r15, %r15
	je	.L147
	xorl	%edi, %edi
.L47:
	orq	%rax, %rbx
	je	.L298
.L134:
	movq	%rdx, %r8
	movabsq	$2305843009213693951, %r9
	shrq	$3, %rdx
	andq	%r9, %rcx
	salq	$61, %r8
	orq	%rcx, %r8
	movq	%rax, %rcx
	shrq	$3, %rcx
	cmpq	%rcx, %rdx
	ja	.L51
	andq	%r14, %r9
	salq	$61, %rax
	orq	%r9, %rax
	cmpq	%rcx, %rdx
	jne	.L175
	cmpq	%rax, %r8
	movq	%rcx, %rdx
	jbe	.L175
.L51:
	movq	%r8, %rax
	salq	$3, %rdx
	movl	%r13d, %r10d
	shrq	$61, %rax
	salq	$3, %r8
	andl	$1, %r10d
	orq	%rax, %rdx
	movl	$32767, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L86:
	testq	%rbp, %rbp
	je	.L40
	movq	%rdx, %rcx
	movq	%r8, %rsi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %rbx,%rsi
	sbbq %rax,%rcx
# 0 "" 2
#NO_APP
	btq	$51, %rcx
	jnc	.L88
	movl	%r11d, %r10d
	movq	%r11, %r13
	movl	$2, %edi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %r8,%rbx
	sbbq %rdx,%rax
# 0 "" 2
#NO_APP
	andl	$1, %r10d
	movq	%rax, %rdx
	movq	%rbx, %r8
	jmp	.L42
.L340:
	testq	%r15, %r15
	je	.L45
	movq	%rdx, %rdi
	shrq	$50, %rdi
	xorq	$1, %rdi
	andl	$1, %edi
	cmpq	$32767, %r12
	jne	.L47
	movq	%rax, %r9
	orq	%rbx, %r9
	je	.L298
.L136:
	movabsq	$1125899906842624, %r8
	testq	%r8, %rax
	movl	$1, %r8d
	cmove	%r8d, %edi
	testq	%r15, %r15
	jne	.L134
	movq	%rax, %rdx
	movq	%rbx, %r8
.L298:
	movq	%r11, %r13
.L299:
	movl	$32767, %r9d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$-1, %esi
	je	.L342
	cmpq	$32767, %r12
	je	.L27
	notl	%esi
	movl	$2, %edi
	jmp	.L28
.L33:
	xorl	%r9d, %r9d
	orq	%r8, %rdx
	setne	%r9b
	xorl	%ecx, %ecx
	jmp	.L35
.L81:
	leal	-64(%rsi), %ecx
	movq	%rdx, %rbp
	shrq	%cl, %rbp
	cmpl	$64, %esi
	je	.L83
	movl	$128, %ecx
	subl	%esi, %ecx
	salq	%cl, %rdx
	orq	%rdx, %r8
.L83:
	xorl	%r9d, %r9d
	testq	%r8, %r8
	setne	%r9b
	xorl	%ecx, %ecx
	orq	%rbp, %r9
	jmp	.L82
.L335:
	testq	%r15, %r15
	je	.L91
	movq	%rdx, %rdi
	shrq	$50, %rdi
	xorq	$1, %rdi
	andl	$1, %edi
	cmpq	$32767, %r12
	jne	.L93
.L92:
	testq	%rbp, %rbp
	je	.L95
	movabsq	$1125899906842624, %r8
	testq	%r8, %rax
	movl	$1, %r8d
	cmove	%r8d, %edi
	testq	%r15, %r15
	jne	.L98
	movl	%r11d, %r10d
	movq	%rax, %rdx
	movq	%rbx, %r8
	andl	$1, %r10d
	movq	%r11, %r13
	movl	$32767, %r9d
	jmp	.L10
.L91:
	cmpq	$32767, %r12
	je	.L168
.L133:
	testq	%rbp, %rbp
	je	.L171
	movl	%r11d, %r10d
	movq	%rax, %rdx
	movq	%rbx, %r8
	andl	$1, %r10d
	movq	%r11, %r13
	xorl	%edi, %edi
	movl	$32767, %r9d
	jmp	.L10
.L54:
	cmpl	$16384, %eax
	je	.L343
	cmpl	$8192, %eax
	movl	$40, %edi
	jne	.L57
	xorl	%r10d, %r10d
	testq	%r13, %r13
	jne	.L154
.L56:
	movq	$-1, %rdx
	movl	$32766, %r9d
	movl	$40, %edi
	movq	%rdx, %r8
	jmp	.L122
.L168:
	xorl	%edi, %edi
	jmp	.L92
.L338:
	movq	%rax, %rcx
	orq	%rbx, %rcx
	je	.L303
.L32:
	movq	%rax, %rdi
	movq	%rax, %rdx
	movq	%rbx, %r8
	shrq	$50, %rdi
	movl	$32767, %r9d
	xorl	%esi, %esi
	xorq	$1, %rdi
	andl	$1, %edi
	jmp	.L10
.L339:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	subq %r8,%rbx
	sbbq %rdx,%rax
# 0 "" 2
#NO_APP
	movq	%r12, %r9
	movq	%rax, %rdx
	movq	%rbx, %r8
	movq	%r11, %r13
	movl	$2, %edi
	jmp	.L66
.L336:
	movq	%rax, %rcx
	orq	%rbx, %rcx
	je	.L9
	jmp	.L295
.L39:
	testq	%rbp, %rbp
	je	.L40
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq %rbx,%r8
	adcq %rax,%rdx
# 0 "" 2
#NO_APP
	btq	$51, %rdx
	jnc	.L145
	movabsq	$-2251799813685249, %rax
	movq	%r8, %rbp
	movl	$1, %r9d
	andq	%rax, %rdx
	andl	$7, %ebp
	movl	$2, %edi
	jmp	.L43
.L95:
	testq	%r15, %r15
	jne	.L299
.L171:
	movl	$1, %r10d
	movabsq	$140737488355328, %rdx
	movl	$1, %edi
	jmp	.L97
.L34:
	leal	-64(%rsi), %ecx
	movq	%rdx, %r14
	shrq	%cl, %r14
	cmpl	$64, %esi
	je	.L36
	movl	$128, %ecx
	subl	%esi, %ecx
	salq	%cl, %rdx
	orq	%rdx, %r8
.L36:
	xorl	%r9d, %r9d
	testq	%r8, %r8
	setne	%r9b
	xorl	%ecx, %ecx
	orq	%r14, %r9
	jmp	.L35
.L77:
	movq	%rax, %rcx
	orq	%rbx, %rcx
	je	.L160
	movabsq	$1125899906842624, %rcx
	movq	%rax, %rdx
	movq	%rbx, %r8
	andq	%rax, %rcx
	movl	$32767, %r9d
	movq	%r11, %r13
.L297:
	xorl	%esi, %esi
	cmpq	$1, %rcx
	sbbl	%edi, %edi
	notl	%edi
	addl	$3, %edi
	jmp	.L10
.L331:
	testq	%r13, %r13
	jne	.L173
	xorl	%r10d, %r10d
	jmp	.L30
.L342:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/subtf3.c" 1
	addq %r8,%rbx
	adcq %rdx,%rax
# 0 "" 2
#NO_APP
	movq	%r12, %r9
	movq	%rax, %rdx
	movq	%rbx, %r8
	movl	$2, %edi
	jmp	.L12
.L337:
	movq	%rax, %rcx
	orq	%rbx, %rcx
	je	.L9
	jmp	.L32
.L144:
	xorl	%ebp, %ebp
	movq	%rax, %rdx
	movq	%rbx, %r8
	jmp	.L40
.L88:
	movq	%rcx, %rbp
	orq	%rsi, %rbp
	je	.L89
	movq	%rsi, %rbp
	movq	%rcx, %rdx
	movq	%rsi, %r8
	andl	$7, %ebp
	jmp	.L40
.L147:
	movq	%rax, %rdx
	movq	%rbx, %r8
	xorl	%edi, %edi
	movl	$32767, %r9d
	jmp	.L10
.L341:
	movq	%rax, %rdi
	orq	%rbx, %rdi
	jne	.L170
	xorl	%edi, %edi
	testq	%r15, %r15
	jne	.L298
	jmp	.L9
.L292:
	movq	$-1, %rdx
	movl	$32766, %r9d
	xorl	%esi, %esi
	movq	%rdx, %r8
	jmp	.L121
.L343:
	testq	%r13, %r13
	movl	$1, %r10d
	jne	.L56
	xorl	%r10d, %r10d
	movl	$40, %edi
	jmp	.L30
.L154:
	movl	$1, %r10d
	jmp	.L30
.L27:
	movq	%rax, %rcx
	orq	%rbx, %rcx
	jne	.L29
	movl	%r11d, %r10d
	movl	$2, %edi
	andl	$1, %r10d
	jmp	.L30
.L160:
	movl	$2, %edi
	jmp	.L30
.L45:
	cmpq	$32767, %r12
	je	.L132
	movq	%rax, %rdx
	movq	%rbx, %r8
	xorl	%edi, %edi
	jmp	.L10
.L132:
	movq	%rax, %rdi
	orq	%rbx, %rdi
	je	.L9
.L170:
	xorl	%edi, %edi
	jmp	.L136
.L89:
	movl	44(%rsp), %eax
	movl	$2, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	sete	%r10b
	xorl	%edx, %edx
	jmp	.L41
.L145:
	movl	$2, %edi
	jmp	.L42
.L176:
	movq	%r8, %rdx
	movq	%rax, %rcx
	movq	%r11, %r13
	jmp	.L99
.L29:
	movabsq	$1125899906842624, %rcx
	movq	%rax, %rdx
	movq	%rbx, %r8
	andq	%rax, %rcx
	movl	$32767, %r9d
	jmp	.L297
.L175:
	movq	%rcx, %rdx
	movq	%rax, %r8
	movq	%r11, %r13
	jmp	.L51
.L329:
	xorl	%ebp, %ebp
	movl	$32767, %eax
	xorl	%edx, %edx
	jmp	.L130
	.size	__subtf3, .-__subtf3
