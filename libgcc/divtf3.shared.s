	.text
	.p2align 4,,15
	.globl	__divtf3
	.type	__divtf3, @function
__divtf3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 43 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%rdx, %r8
	andq	%rdx, %rax
	shrq	$63, %rdx
	shrq	$48, %r8
	movzbl	%dl, %r14d
	andw	$32767, %r8w
	je	.L3
	cmpw	$32767, %r8w
	je	.L4
	movq	%rsi, %rbx
	movabsq	$2251799813685248, %rcx
	salq	$3, %rax
	shrq	$61, %rbx
	movzwl	%r8w, %r8d
	salq	$3, %rsi
	orq	%rcx, %rbx
	subq	$16383, %r8
	movq	$0, (%rsp)
	orq	%rax, %rbx
	xorl	%edi, %edi
	xorl	%eax, %eax
.L5:
	movq	24(%rsp), %r10
	movabsq	$281474976710655, %rbp
	movq	16(%rsp), %r9
	movq	%r10, %rcx
	andq	%r10, %rbp
	shrq	$63, %r10
	shrq	$48, %rcx
	movq	%rbp, %r11
	movzbl	%r10b, %r13d
	andw	$32767, %cx
	je	.L11
	cmpw	$32767, %cx
	je	.L12
	movq	%r9, %rbp
	movabsq	$2251799813685248, %r12
	movzwl	%cx, %ecx
	shrq	$61, %rbp
	salq	$3, %r11
	subq	$16383, %rcx
	orq	%r12, %rbp
	salq	$3, %r9
	subq	%rcx, %r8
	orq	%rbp, %r11
	xorl	%r15d, %r15d
.L13:
	movl	%r10d, %ecx
	xorl	%edx, %ecx
	cmpq	$15, %rax
	movzbl	%cl, %ecx
	movq	%rcx, %r12
	ja	.L20
	leaq	.L22(%rip), %rbp
	movslq	0(%rbp,%rax,4), %rax
	addq	%rax, %rbp
	jmp	*%rbp
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L20-.L22
	.long	.L21-.L22
	.long	.L101-.L22
	.long	.L24-.L22
	.long	.L101-.L22
	.long	.L25-.L22
	.long	.L101-.L22
	.long	.L24-.L22
	.long	.L26-.L22
	.long	.L26-.L22
	.long	.L25-.L22
	.long	.L24-.L22
	.long	.L27-.L22
	.long	.L27-.L22
	.long	.L27-.L22
	.long	.L28-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rax, %rbx
	orq	%rsi, %rbx
	jne	.L217
	xorl	%esi, %esi
	movl	$8, %eax
	movl	$32767, %r8d
	movq	$2, (%rsp)
	xorl	%edi, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rsp), %r15
	movq	%rbx, %r11
	movq	%rsi, %r9
	movq	%r14, %r13
.L24:
	cmpq	$2, %r15
	je	.L51
	cmpq	$3, %r15
	je	.L52
	cmpq	$1, %r15
	jne	.L218
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rcx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rax, %rbx
	orq	%rsi, %rbx
	je	.L98
	testq	%rax, %rax
	je	.L6
	bsrq	%rax, %rdi
	xorq	$63, %rdi
	movslq	%edi, %rdi
.L7:
	leaq	-15(%rdi), %r8
	cmpq	$60, %r8
	jg	.L8
	leal	3(%r8), %r9d
	movq	%rsi, %rbx
	movl	%r9d, %ecx
	salq	%cl, %rax
	movl	$61, %ecx
	subl	%r8d, %ecx
	shrq	%cl, %rbx
	movl	%r9d, %ecx
	orq	%rax, %rbx
	salq	%cl, %rsi
.L9:
	movq	$-16367, %r8
	xorl	%eax, %eax
	movq	$0, (%rsp)
	subq	%rdi, %r8
	movl	$2, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbp, %rcx
	subq	$32767, %r8
	orq	%r9, %rcx
	je	.L219
	orq	$3, %rax
	btq	$47, %rbp
	movl	$3, %r15d
	jc	.L13
	orl	$1, %edi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	orq	%r9, %r11
	je	.L220
	testq	%rbp, %rbp
	je	.L15
	bsrq	%rbp, %r12
	xorq	$63, %r12
	movslq	%r12d, %r12
.L16:
	leaq	-15(%r12), %r11
	cmpq	$60, %r11
	jg	.L17
	leal	3(%r11), %r15d
	movl	%r15d, %ecx
	salq	%cl, %rbp
	movl	$61, %ecx
	subl	%r11d, %ecx
	movq	%r9, %r11
	shrq	%cl, %r11
	movl	%r15d, %ecx
	orq	%rbp, %r11
	salq	%cl, %r9
.L18:
	orl	$2, %edi
	leaq	16367(%r8,%r12), %r8
	xorl	%r15d, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L217:
	movq	%rax, %rdi
	movq	%rax, %rbx
	movl	$32767, %r8d
	shrq	$47, %rdi
	movl	$12, %eax
	movq	$3, (%rsp)
	xorl	$1, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L220:
	orq	$1, %rax
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	movl	$1, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L219:
	orq	$2, %rax
	xorl	%r11d, %r11d
	xorl	%r9d, %r9d
	movl	$2, %r15d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%esi, %esi
	movl	$4, %eax
	xorl	%r8d, %r8d
	movq	$1, (%rsp)
	xorl	%edi, %edi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
.L23:
	movabsq	$281474976710655, %rax
	movq	$0, 8(%rsp)
	movq	%rsi, (%rsp)
	andq	%rax, %r8
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rsi
	movq	%rdx, %r11
	movabsq	$-9223090561878065153, %rdx
	salq	$63, %rcx
	salq	$48, %r11
	andq	%rsi, %rax
	orq	%r8, %rax
	andq	%rdx, %rax
	orq	%rax, %r11
	movabsq	$9223372036854775807, %rax
	andq	%rax, %r11
	orq	%rcx, %r11
	testl	%edi, %edi
	movq	%r11, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jne	.L95
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	orl	$1, %edi
	movl	$1, %ecx
	movabsq	$140737488355328, %r8
	xorl	%esi, %esi
.L46:
	movl	$32767, %edx
	movzbl	%cl, %ecx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	orl	$4, %edi
	xorl	%r8d, %r8d
	movl	$32767, %edx
	xorl	%esi, %esi
.L47:
	movabsq	$281474976710655, %r11
	movq	%r8, %rax
	movq	$0, 8(%rsp)
	andq	%r11, %rax
	movq	8(%rsp), %r11
	movq	%rsi, (%rsp)
	movabsq	$-281474976710656, %rsi
	salq	$63, %rcx
	andq	%rsi, %r11
	orq	%rax, %r11
	movq	%rdx, %rax
	movabsq	$-9223090561878065153, %rdx
	andl	$32767, %eax
	andq	%rdx, %r11
	movabsq	$9223372036854775807, %rdx
	salq	$48, %rax
	orq	%r11, %rax
	andq	%rdx, %rax
	orq	%rcx, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L95:
	movaps	%xmm0, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movdqa	(%rsp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$32767, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	%r11, %rbx
	ja	.L29
	jne	.L30
	cmpq	%rsi, %r9
	jbe	.L29
.L30:
	movq	%rsi, %rax
	subq	$1, %r8
	movq	%rbx, %r13
	xorl	%esi, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L28:
	cmpq	%r11, %rbx
	ja	.L45
	movabsq	$140737488355328, %rax
	orq	%r11, %rax
	movq	%rax, %r8
	movabsq	$281474976710655, %rax
	andq	%rax, %r8
	cmpq	%r11, %rbx
	jne	.L104
	cmpq	%rsi, %r9
	jb	.L214
.L104:
	movl	%r10d, %ecx
	movq	%r9, %rsi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, %rax
	movq	%rbx, %r13
	salq	$63, %rbx
	shrq	%rax
	shrq	%r13
	salq	$63, %rsi
	orq	%rbx, %rax
.L31:
	movq	%r9, %rbp
	salq	$12, %r11
	movq	%r9, %r14
	shrq	$52, %rbp
	movq	%r13, %rdx
	salq	$12, %r14
	orq	%r11, %rbp
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	divq %rbp
# 0 "" 2
#NO_APP
	movq	%rdx, %r9
	movq	%rax, %rbx
	movq	%rdx, %r13
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	mulq %r14
# 0 "" 2
#NO_APP
	cmpq	%rdx, %r9
	jb	.L32
	jne	.L102
	cmpq	%rax, %rsi
	jb	.L32
.L102:
	movq	%rbx, %r11
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rbp, %r10
	movq	%r14, %r15
	leaq	-1(%rbx), %r11
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq %rsi,%r15
	adcq %r9,%r10
# 0 "" 2
#NO_APP
	cmpq	%r10, %rbp
	movq	%r10, %r13
	movq	%r15, %rsi
	jb	.L34
	jne	.L33
	cmpq	%r15, %r14
	ja	.L33
.L34:
	cmpq	%r10, %rdx
	jbe	.L221
.L35:
	leaq	-2(%rbx), %r11
	movq	%rbp, %r13
	movq	%r14, %rsi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq %r15,%rsi
	adcq %r10,%r13
# 0 "" 2
#NO_APP
.L33:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	subq %rax,%rsi
	sbbq %rdx,%r13
# 0 "" 2
#NO_APP
	cmpq	%r13, %rbp
	leaq	16383(%r8), %r10
	je	.L36
	movq	%rsi, %rax
	movq	%r13, %rdx
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	divq %rbp
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	movq	%rax, %rsi
	movq	%rdx, %r13
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	mulq %r14
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rcx
	jb	.L37
	testq	%rax, %rax
	setne	%bl
	cmpq	%rdx, %rcx
	jne	.L103
	testb	%bl, %bl
	jne	.L37
.L103:
	movq	%rsi, %r9
.L38:
	cmpq	%rdx, %r13
	jne	.L130
	testb	%bl, %bl
	je	.L43
.L130:
	orq	$1, %r9
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbp, %rbx
	movq	%r14, %r15
	leaq	-1(%rsi), %r9
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $0,%r15
	adcq %rcx,%rbx
# 0 "" 2
#NO_APP
	cmpq	%rbx, %rbp
	movq	%rbx, %r13
	jnb	.L222
.L39:
	cmpq	%rbx, %rdx
	ja	.L41
	jne	.L129
	cmpq	%r15, %rax
	jbe	.L129
.L41:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq %r15,%r14
	adcq %rbx,%rbp
# 0 "" 2
#NO_APP
	cmpq	%r14, %rax
	leaq	-2(%rsi), %r9
	movq	%rbp, %r13
	setne	%bl
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L8:
	leal	-61(%r8), %ecx
	movq	%rsi, %rbx
	xorl	%esi, %esi
	salq	%cl, %rbx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L6:
	bsrq	%rsi, %rdi
	xorq	$63, %rdi
	movslq	%edi, %rdi
	addq	$64, %rdi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	leal	-61(%r11), %ecx
	salq	%cl, %r9
	movq	%r9, %r11
	xorl	%r9d, %r9d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L15:
	bsrq	%r9, %r12
	xorq	$63, %r12
	movslq	%r12d, %r12
	addq	$64, %r12
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L45:
	movabsq	$140737488355328, %r11
	movabsq	$281474976710655, %rax
	orq	%r11, %rbx
	movq	%rbx, %r8
	andq	%rax, %r8
.L214:
	movl	%edx, %ecx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	16383(%r8), %r10
	movq	%r13, %r12
.L43:
	cmpq	$0, %r10
	jle	.L54
	testb	$7, %r9b
	jne	.L55
.L57:
	movq	%r12, %rcx
	andl	$1, %ecx
.L56:
	btq	$52, %r11
	jnc	.L62
	movabsq	$-4503599627370497, %rax
	leaq	16384(%r8), %r10
	andq	%rax, %r11
.L62:
	cmpq	$32766, %r10
	jg	.L63
	movq	%r11, %rsi
	salq	$13, %r11
	shrq	$3, %r9
	salq	$61, %rsi
	movq	%r11, %r8
	movl	%r10d, %edx
	orq	%r9, %rsi
	shrq	$16, %r8
	andw	$32767, %dx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L63:
	movl	44(%rsp), %esi
	andl	$24576, %esi
	cmpl	$8192, %esi
	je	.L65
	cmpl	$16384, %esi
	je	.L66
	testl	%esi, %esi
	movabsq	$281474976710655, %rax
	movl	$0, %r11d
	cmove	%r11, %rax
	cmpl	$1, %esi
	sbbl	%edx, %edx
	movq	%rax, %r8
	notl	%edx
	addw	$32767, %dx
	cmpl	$1, %esi
	sbbq	%rsi, %rsi
	notq	%rsi
.L64:
	orl	$40, %edi
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L54:
	jne	.L68
	testb	$7, %r9b
	movq	%r11, %rdx
	jne	.L93
.L69:
	movq	%r11, %r10
	movl	$1, %r8d
	shrq	$52, %r10
	xorq	$1, %r10
	andl	$1, %r10d
.L73:
	movl	$64, %eax
	movq	%rdx, %rsi
	movq	%r9, %rbx
	subl	%r8d, %eax
	movl	%eax, %ecx
	salq	%cl, %rsi
	movl	%r8d, %ecx
	shrq	%cl, %rbx
	movl	%eax, %ecx
	xorl	%eax, %eax
	salq	%cl, %r9
	orq	%rbx, %rsi
	movl	%r8d, %ecx
	testq	%r9, %r9
	setne	%al
	shrq	%cl, %rdx
	movl	%r12d, %ecx
	orq	%rax, %rsi
	andl	$1, %ecx
	testb	$7, %sil
	je	.L77
.L76:
	movl	44(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L80
	cmpl	$16384, %eax
	je	.L81
	testl	%eax, %eax
	je	.L223
.L77:
	btq	$51, %rdx
	jnc	.L224
	orl	$32, %edi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	testl	%r10d, %r10d
	movl	$1, %edx
	je	.L215
.L85:
	orl	$16, %edi
.L215:
	movzbl	%cl, %ecx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L221:
	jne	.L33
	cmpq	%r15, %rax
	ja	.L35
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L222:
	jne	.L129
	cmpq	%r15, %r14
	jbe	.L39
.L129:
	cmpq	%r15, %rax
	setne	%bl
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L68:
	movl	$1, %ecx
	subq	%r10, %rcx
	cmpq	$116, %rcx
	movq	%rcx, %r8
	jle	.L96
	movq	%r9, %rsi
	xorl	%r8d, %r8d
	orq	%r11, %rsi
	jne	.L97
.L87:
	movq	%r12, %rcx
	orl	$16, %edi
	xorl	%edx, %edx
	andl	$1, %ecx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L52:
	movabsq	$140737488355328, %rax
	movq	%r11, %r8
	movl	%r13d, %ecx
	orq	%rax, %r8
	movabsq	$281474976710655, %rax
	andl	$1, %ecx
	andq	%rax, %r8
	movq	%r9, %rsi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L65:
	testq	%r12, %r12
	movabsq	$281474976710655, %r11
	movl	$0, %eax
	cmove	%r11, %rax
	cmpq	$1, %r12
	sbbl	%edx, %edx
	movq	%rax, %r8
	addw	$32767, %dx
	cmpq	$1, %r12
	sbbq	%rsi, %rsi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L36:
	cmpq	$0, %r10
	jle	.L225
	movl	44(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L125
	cmpl	$16384, %eax
	je	.L126
	testl	%eax, %eax
	movq	$-1, %r9
	jne	.L57
	movq	$-1, %r9
.L61:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $4,%r9
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L55:
	movl	44(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L58
	cmpl	$16384, %eax
	je	.L59
	testl	%eax, %eax
	jne	.L57
	movq	%r9, %rax
	movq	%r12, %rcx
	andl	$15, %eax
	andl	$1, %ecx
	cmpq	$4, %rax
	je	.L56
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L97:
	movl	44(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L89
	cmpl	$16384, %eax
	je	.L90
	testl	%eax, %eax
	je	.L226
.L121:
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L225:
	jne	.L227
	movq	%r11, %rdx
	movq	$-1, %r9
.L93:
	movl	44(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	je	.L70
	cmpl	$16384, %eax
	je	.L71
	testl	%eax, %eax
	jne	.L69
	movq	%r9, %rax
	andl	$15, %eax
	cmpq	$4, %rax
	je	.L69
	movq	%r9, %rax
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $4,%rax
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L69
.L126:
	movq	$-1, %r9
.L59:
	testq	%r12, %r12
	movl	$1, %ecx
	jne	.L56
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $8,%r9
	adcq $0,%r11
# 0 "" 2
#NO_APP
	xorl	%ecx, %ecx
	jmp	.L56
.L125:
	movq	$-1, %r9
.L58:
	xorl	%ecx, %ecx
	testq	%r12, %r12
	je	.L56
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $8,%r9
	adcq $0,%r11
# 0 "" 2
#NO_APP
	movl	$1, %ecx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L66:
	testq	%r12, %r12
	movabsq	$281474976710655, %r11
	movl	$0, %eax
	cmovne	%r11, %rax
	cmpq	$1, %r12
	sbbl	%edx, %edx
	movq	%rax, %r8
	notl	%edx
	addw	$32767, %dx
	cmpq	$1, %r12
	sbbq	%rsi, %rsi
	notq	%rsi
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$32767, %edx
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%r13, %rcx
	jmp	.L23
.L224:
	movq	%rdx, %r11
	shrq	$3, %rsi
	salq	$61, %rdx
	salq	$13, %r11
	orq	%rdx, %rsi
	movq	%r11, %r8
	shrq	$16, %r8
	testl	%r10d, %r10d
	jne	.L84
	xorl	%edx, %edx
	movzbl	%cl, %ecx
	jmp	.L23
.L227:
	movl	$1, %ecx
	subq	%r10, %rcx
	cmpq	$116, %rcx
	movq	%rcx, %r8
	jg	.L97
	movq	$-1, %r9
.L96:
	cmpq	$63, %r8
	jle	.L228
	leal	-64(%r8), %ecx
	movq	%r11, %rax
	shrq	%cl, %rax
	cmpq	$64, %r8
	je	.L78
	movl	$128, %ecx
	subl	%r8d, %ecx
	salq	%cl, %r11
	orq	%r11, %r9
.L78:
	xorl	%esi, %esi
	testq	%r9, %r9
	movl	%r12d, %ecx
	setne	%sil
	andl	$1, %ecx
	orq	%rax, %rsi
	testb	$7, %sil
	je	.L79
	movl	$1, %r10d
	xorl	%edx, %edx
	jmp	.L76
.L79:
	shrq	$3, %rsi
	xorl	%r8d, %r8d
.L86:
	xorl	%edx, %edx
	testb	$8, 45(%rsp)
	je	.L85
	movzbl	%cl, %ecx
	jmp	.L23
.L81:
	testq	%r12, %r12
	jne	.L77
.L212:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $8,%rsi
	adcq $0,%rdx
# 0 "" 2
#NO_APP
	jmp	.L77
.L80:
	testq	%r12, %r12
	je	.L77
	jmp	.L212
.L71:
	testq	%r12, %r12
	jne	.L69
.L211:
	movq	%r9, %rax
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $8,%rax
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L69
.L70:
	testq	%r12, %r12
	je	.L69
	jmp	.L211
.L223:
	movq	%rsi, %rax
	andl	$15, %eax
	cmpq	$4, %rax
	je	.L77
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $4,%rsi
	adcq $0,%rdx
# 0 "" 2
#NO_APP
	jmp	.L77
.L226:
	xorl	%r11d, %r11d
	movl	$1, %esi
	movabsq	$281474976710655, %rax
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $4,%rsi
	adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	%r11, %r8
	shrq	$3, %rsi
	andq	%rax, %r8
	jmp	.L87
.L90:
	testq	%r12, %r12
	movq	%r12, %r11
	jne	.L121
.L213:
	movl	$1, %esi
	movabsq	$281474976710655, %rax
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/divtf3.c" 1
	addq $8,%rsi
	adcq $0,%r11
# 0 "" 2
#NO_APP
	movq	%r11, %r8
	shrq	$3, %rsi
	andq	%rax, %r8
	jmp	.L87
.L89:
	testq	%r12, %r12
	je	.L121
	xorl	%r11d, %r11d
	jmp	.L213
.L228:
	movq	%r11, %rdx
	movl	$1, %r10d
	jmp	.L73
.L84:
	testb	$32, %dil
	je	.L86
	xorl	%edx, %edx
	jmp	.L85
	.size	__divtf3, .-__divtf3
