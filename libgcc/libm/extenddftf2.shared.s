	.text
	.p2align 4,,15
	.globl	__extenddftf2
	.type	__extenddftf2, @function
__extenddftf2:
	subq	$40, %rsp
#APP
# 44 "/root/nyanlinux/src/glibc-2.33/soft-fp/extenddftf2.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movq	%xmm0, %rcx
	movq	%xmm0, %rax
	movabsq	$4503599627370495, %rdx
	shrq	$52, %rcx
	andq	%rax, %rdx
	shrq	$63, %rax
	andl	$2047, %ecx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	leaq	1(%rcx), %rax
	andl	$2047, %eax
	cmpq	$1, %rax
	jle	.L2
	shrq	$4, %rdx
	leal	15360(%rcx), %eax
	salq	$60, %rdi
	movq	%rdx, %r8
.L3:
	movq	$0, 8(%rsp)
	movq	8(%rsp), %rdx
	movabsq	$-281474976710656, %rcx
	salq	$48, %rax
	salq	$63, %rsi
	movq	%rdi, (%rsp)
	andq	%rcx, %rdx
	movabsq	$-9223090561878065153, %rcx
	orq	%r8, %rdx
	andq	%rcx, %rdx
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	orq	%rsi, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rcx, %rcx
	jne	.L4
	testq	%rdx, %rdx
	je	.L9
	bsrq	%rdx, %rdi
	xorq	$63, %rdi
	cmpl	$14, %edi
	movl	%edi, %r8d
	jle	.L13
	leal	-15(%rdi), %ecx
	movq	%rdx, %rax
	xorl	%edx, %edx
	salq	%cl, %rax
.L6:
	movq	%rdx, (%rsp)
	movabsq	$281474976710655, %rdx
	movq	$0, 8(%rsp)
	andq	%rax, %rdx
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rcx
	movl	$2, %edi
	andq	%rcx, %rax
	movabsq	$-9223090561878065153, %rcx
	orq	%rdx, %rax
	movl	$15372, %edx
	subl	%r8d, %edx
	andq	%rcx, %rax
	movabsq	$9223372036854775807, %rcx
	andl	$32767, %edx
	salq	$48, %rdx
	orq	%rax, %rdx
	movq	%rsi, %rax
	salq	$63, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L7:
	movaps	%xmm0, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	testq	%rdx, %rdx
	je	.L10
	movq	%rdx, %rax
	movq	%rdx, %rcx
	movq	$0, 8(%rsp)
	salq	$60, %rax
	shrq	$4, %rcx
	movabsq	$-281474976710656, %rdi
	movq	%rax, (%rsp)
	movabsq	$140737488355328, %rax
	orq	%rcx, %rax
	movq	8(%rsp), %rcx
	andq	%rdi, %rcx
	orq	%rax, %rcx
	movabsq	$9223090561878065152, %rax
	orq	%rax, %rcx
	movq	%rsi, %rax
	movabsq	$9223372036854775807, %rsi
	salq	$63, %rax
	andq	%rsi, %rcx
	orq	%rax, %rcx
	movabsq	$2251799813685248, %rax
	movq	%rcx, 8(%rsp)
	testq	%rax, %rdx
	movdqa	(%rsp), %xmm0
	jne	.L1
	movl	$1, %edi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$32767, %eax
	xorl	%r8d, %r8d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$15, %ecx
	movq	%rdx, %rax
	subl	%edi, %ecx
	shrq	%cl, %rax
	leal	49(%rdi), %ecx
	salq	%cl, %rdx
	jmp	.L6
	.size	__extenddftf2, .-__extenddftf2
