	.text
	.p2align 4,,15
	.globl	__mulxc3
	.type	__mulxc3, @function
__mulxc3:
	fldt	8(%rsp)
	fldt	40(%rsp)
	fld	%st(1)
	fmul	%st(1), %st
	fldt	24(%rsp)
	fldt	56(%rsp)
	fmul	%st, %st(1)
	fld	%st(1)
	fstpt	-56(%rsp)
	fmul	%st(4), %st
	fldt	24(%rsp)
	fmul	%st(4), %st
	fld	%st(0)
	fstpt	-40(%rsp)
	fld	%st(3)
	fsubp	%st, %st(3)
	fld	%st(1)
	faddp	%st, %st(1)
	fucomi	%st(0), %st
	fxch	%st(2)
	setp	%al
	fucomi	%st(0), %st
	setp	%dl
	andb	%al, %dl
	jne	.L152
	fstp	%st(4)
	fstp	%st(0)
	fstp	%st(3)
	fstp	%st(0)
	jmp	.L115
.L170:
	fstp	%st(0)
	fstp	%st(2)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L171:
	fstp	%st(2)
	fstp	%st(0)
	fxch	%st(1)
.L115:
	rep ret
.L152:
	fld	%st(5)
	fsub	%st(6), %st
	fstpt	-24(%rsp)
	fldt	24(%rsp)
	fucomi	%st(0), %st
	fsub	%st(0), %st
	setnp	%cl
	fucomip	%st(0), %st
	fxch	%st(5)
	setp	%al
	andl	%eax, %ecx
	fucomi	%st(0), %st
	jp	.L74
	fldt	-24(%rsp)
	fucomip	%st(0), %st
	jp	.L153
.L74:
	testb	%cl, %cl
	jne	.L154
.L14:
	fld	%st(4)
	fsub	%st(5), %st
	fstpt	-24(%rsp)
	fldt	56(%rsp)
	fucomi	%st(0), %st
	fld	%st(0)
	setnp	%dl
	fsubp	%st, %st(1)
	fucomip	%st(0), %st
	fxch	%st(4)
	setp	%al
	andl	%eax, %edx
	fucomi	%st(0), %st
	jp	.L77
	fldt	-24(%rsp)
	fucomip	%st(0), %st
	jp	.L155
.L77:
	testb	%dl, %dl
	jne	.L156
	testb	%cl, %cl
	jne	.L163
	fld	%st(3)
	fsub	%st(4), %st
	fxch	%st(4)
	fucomip	%st(0), %st
	jp	.L164
	fxch	%st(3)
	fucomip	%st(0), %st
	jp	.L165
	jmp	.L80
.L164:
	fstp	%st(3)
.L80:
	fldt	-56(%rsp)
	fld	%st(0)
	fsub	%st(1), %st
	fxch	%st(1)
	fucomip	%st(0), %st
	jp	.L166
	fucomip	%st(0), %st
	jp	.L167
	jmp	.L81
.L166:
	fstp	%st(0)
.L81:
	fld	%st(0)
	fsub	%st(1), %st
	fxch	%st(1)
	fucomip	%st(0), %st
	jp	.L168
	fucomip	%st(0), %st
	jp	.L169
	jmp	.L82
.L168:
	fstp	%st(0)
.L82:
	fldt	-40(%rsp)
	fld	%st(0)
	fsub	%st(1), %st
	fxch	%st(1)
	fucomip	%st(0), %st
	jp	.L170
	fucomip	%st(0), %st
	jnp	.L171
	fstp	%st(0)
	fstp	%st(2)
	jmp	.L42
.L165:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(2)
	jmp	.L42
.L167:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(2)
	jmp	.L42
.L169:
	fstp	%st(0)
	fstp	%st(2)
.L42:
	fucomi	%st(0), %st
	jp	.L157
.L58:
	fldt	24(%rsp)
	fucomi	%st(0), %st
	jp	.L158
	fstp	%st(0)
	fxch	%st(1)
.L61:
	fucomi	%st(0), %st
	jp	.L159
.L64:
	fldt	56(%rsp)
	fucomi	%st(0), %st
	jp	.L160
	fstp	%st(0)
	jmp	.L39
.L163:
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L39
.L172:
	fstp	%st(0)
	fxch	%st(1)
.L39:
	fld	%st(1)
	fmul	%st(1), %st
	fldt	24(%rsp)
	fldt	56(%rsp)
	fmul	%st, %st(1)
	fxch	%st(2)
	fsubp	%st, %st(1)
	flds	.LC4(%rip)
	fmul	%st, %st(1)
	fxch	%st(4)
	fmulp	%st, %st(2)
	fldt	24(%rsp)
	fmulp	%st, %st(3)
	fxch	%st(1)
	faddp	%st, %st(2)
	fxch	%st(2)
	fmulp	%st, %st(1)
	fxch	%st(1)
	jmp	.L115
.L154:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L69
	fstp	%st(0)
	fldz
	fchs
.L69:
	fld1
.L16:
	fldt	24(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fabs
	je	.L17
	fchs
.L17:
	fstpt	24(%rsp)
	fxch	%st(4)
	fucomi	%st(0), %st
	jp	.L161
.L18:
	fldt	56(%rsp)
	fucomip	%st(0), %st
	jp	.L76
	fxch	%st(4)
	movl	%edx, %ecx
	jmp	.L14
.L156:
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L70
	fstp	%st(0)
	fldz
	fchs
.L70:
.L150:
	fld1
.L34:
	fldt	56(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fabs
	je	.L35
	fchs
.L35:
	fstpt	56(%rsp)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L162
.L36:
	fldt	24(%rsp)
	fucomi	%st(0), %st
	jnp	.L172
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L41
	fstp	%st(0)
	fldz
	fchs
.L41:
	fstpt	24(%rsp)
	fxch	%st(1)
	jmp	.L39
.L162:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L36
	fstp	%st(0)
	fldz
	fchs
	jmp	.L36
.L76:
	fldt	56(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L22
	fstp	%st(0)
	fldz
	fchs
.L22:
	fstpt	56(%rsp)
	fxch	%st(4)
	movl	%edx, %ecx
	jmp	.L14
.L161:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L18
	fstp	%st(0)
	fldz
	fchs
	jmp	.L18
.L160:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L68
	fstp	%st(0)
	fldz
	fchs
.L68:
	fstpt	56(%rsp)
	jmp	.L39
.L159:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L66
	fstp	%st(0)
	fldz
	fchs
.L66:
	jmp	.L64
.L158:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L63
	fstp	%st(0)
	fldz
	fchs
.L63:
	fstpt	24(%rsp)
	fxch	%st(1)
	jmp	.L61
.L157:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L58
	fstp	%st(0)
	fldz
	fchs
	jmp	.L58
.L153:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fld1
	je	.L15
	fstp	%st(0)
	fld1
	fchs
.L15:
	testb	%cl, %cl
	jne	.L69
	fldz
	jmp	.L16
.L155:
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fld1
	je	.L33
	fstp	%st(0)
	fld1
	fchs
.L33:
	testb	%dl, %dl
	jne	.L150
	fldz
	jmp	.L34
	.size	__mulxc3, .-__mulxc3
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC4:
	.long	2139095040
