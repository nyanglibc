	.text
	.p2align 4,,15
	.globl	__fixtfsi
	.type	__fixtfsi, @function
__fixtfsi:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
#APP
# 41 "/root/nyanlinux/src/glibc-2.33/soft-fp/fixtfsi.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	movq	%rsi, %rcx
	andq	%rsi, %rax
	shrq	$63, %rsi
	shrq	$48, %rcx
	movq	%rax, %rdx
	andw	$32767, %cx
	movzwl	%cx, %r8d
	cmpq	$16382, %r8
	jg	.L2
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L21
.L11:
	movl	$32, %edi
.L3:
	movl	%eax, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movl	(%rsp), %eax
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$16413, %r8
	jle	.L5
	cmpq	$16414, %r8
	leal	2147483647(%rsi), %eax
	jne	.L10
	testb	%sil, %sil
	je	.L10
	movq	%rdx, %rcx
	shrq	$17, %rcx
	testq	%rcx, %rcx
	jne	.L10
	salq	$47, %rdx
	orq	%rdi, %rdx
	jne	.L11
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	orq	%rdi, %rdx
	je	.L1
	movl	$34, %edi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movabsq	$281474976710656, %rax
	orq	%rdx, %rax
	movzwl	%cx, %edx
	leal	-16367(%rdx), %ecx
	movq	%rax, %r8
	salq	%cl, %r8
	movl	$16431, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rax
	movl	%eax, %edx
	negl	%edx
	testb	%sil, %sil
	cmovne	%edx, %eax
	orq	%rdi, %r8
	jne	.L11
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$1, %edi
	jmp	.L3
	.size	__fixtfsi, .-__fixtfsi
