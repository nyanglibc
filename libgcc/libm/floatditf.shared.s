	.text
	.p2align 4,,15
	.globl	__floatditf
	.type	__floatditf, @function
__floatditf:
	testq	%rdi, %rdi
	je	.L5
	movq	%rdi, %rax
	shrq	$63, %rax
	movq	%rax, %rdx
	movq	%rdi, %rax
	negq	%rax
	testq	%rdi, %rdi
	cmovs	%rax, %rdi
	movl	$16446, %eax
	bsrq	%rdi, %rcx
	xorq	$63, %rcx
	subl	%ecx, %eax
	movl	$16495, %ecx
	movslq	%eax, %r8
	movl	%eax, %esi
	subq	%r8, %rcx
	andw	$32767, %si
	cmpq	$63, %rcx
	jle	.L7
	movl	$16431, %ecx
	xorl	%r8d, %r8d
	subl	%eax, %ecx
	movabsq	$281474976710655, %rax
	salq	%cl, %rdi
	andq	%rax, %rdi
.L2:
	movabsq	$281474976710655, %rax
	movq	$0, -16(%rsp)
	movabsq	$-281474976710656, %rcx
	andq	%rdi, %rax
	movq	-16(%rsp), %rdi
	salq	$48, %rsi
	movzbl	%dl, %edx
	movq	%r8, -24(%rsp)
	salq	$63, %rdx
	andq	%rcx, %rdi
	movabsq	$9223372036854775807, %rcx
	orq	%rax, %rdi
	movabsq	$-9223090561878065153, %rax
	andq	%rax, %rdi
	movq	%rdi, %rax
	orq	%rsi, %rax
	andq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movdqa	-24(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$16495, %ecx
	movq	%rdi, %r8
	subl	%eax, %ecx
	salq	%cl, %r8
	leal	-16431(%rax), %ecx
	movabsq	$281474976710655, %rax
	shrq	%cl, %rdi
	andq	%rax, %rdi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%r8d, %r8d
	jmp	.L2
	.size	__floatditf, .-__floatditf
