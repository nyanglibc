	.text
	.p2align 4,,15
	.globl	__trunctfdf2
	.type	__trunctfdf2, @function
__trunctfdf2:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
#APP
# 43 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfdf2.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rdi
	movq	8(%rsp), %rax
	movq	(%rsp), %rcx
	movq	%rax, %rdx
	movq	%rax, %r8
	andq	%rdi, %rax
	shrq	$48, %rdx
	movq	%rcx, %r9
	salq	$3, %rax
	andl	$32767, %edx
	shrq	$61, %r9
	shrq	$63, %r8
	leaq	1(%rdx), %rdi
	orq	%r9, %rax
	movzbl	%r8b, %esi
	leaq	0(,%rcx,8), %r9
	andl	$32767, %edi
	cmpq	$1, %rdi
	jle	.L2
	leaq	-15360(%rdx), %r10
	cmpq	$2046, %r10
	jg	.L93
	testq	%r10, %r10
	jle	.L94
	salq	$7, %rcx
	xorl	%edx, %edx
	testq	%rcx, %rcx
	setne	%dl
	shrq	$60, %r9
	salq	$4, %rax
	orq	%r9, %rdx
	xorl	%edi, %edi
	orq	%rdx, %rax
	xorl	%edx, %edx
	movq	%rax, %r9
	andl	$7, %r9d
.L14:
	testq	%r9, %r9
	je	.L22
	movl	28(%rsp), %ecx
.L6:
	andl	$24576, %ecx
	orl	$32, %edi
	cmpl	$8192, %ecx
	je	.L24
	cmpl	$16384, %ecx
	je	.L25
	testl	%ecx, %ecx
	je	.L95
.L29:
	movabsq	$36028797018963968, %rcx
	andq	%rax, %rcx
	testl	%edx, %edx
	je	.L28
.L27:
	orl	$16, %edi
.L28:
	testq	%rcx, %rcx
	je	.L40
	leaq	1(%r10), %rdx
	cmpq	$2047, %rdx
	je	.L96
	shrq	$3, %rax
	movabsq	$2301339409586323455, %rcx
	andw	$2047, %dx
	andq	%rcx, %rax
.L33:
	movabsq	$4503599627370495, %rcx
	salq	$52, %rdx
	salq	$63, %rsi
	andq	%rcx, %rax
	orq	%rax, %rdx
	orq	%rsi, %rdx
	testl	%edi, %edi
	movq	%rdx, (%rsp)
	jne	.L37
	movsd	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movl	28(%rsp), %ecx
	movl	%ecx, %eax
	andl	$24576, %eax
	je	.L41
	cmpl	$16384, %eax
	je	.L97
	cmpl	$8192, %eax
	jne	.L8
	testb	%r8b, %r8b
	movl	$2047, %edx
	movl	$40, %edi
	jne	.L4
.L8:
	cmpl	$8192, %eax
	je	.L55
	cmpl	$16384, %eax
	jne	.L98
	movl	$40, %edi
	movl	$2046, %r10d
	movq	$-1, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	8(%rax), %rcx
	testq	%rsi, %rsi
	cmove	%rcx, %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rax, %rcx
	orq	%r9, %rcx
	testq	%rdx, %rdx
	jne	.L15
	testq	%rcx, %rcx
	movl	$34, %edi
	je	.L91
.L10:
	movl	$1, %r9d
	movl	$1, %eax
	movl	$2, %edx
.L39:
	movl	28(%rsp), %ecx
	andl	$24576, %ecx
	cmpl	$8192, %ecx
	je	.L19
	cmpl	$16384, %ecx
	je	.L20
	testl	%ecx, %ecx
	jne	.L18
	movq	%rdx, %r10
	leaq	4(%rdx), %rcx
	andl	$15, %r10d
	cmpq	$4, %r10
	cmovne	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L18:
	shrq	$56, %rdx
	xorl	%r10d, %r10d
	xorq	$1, %rdx
	andl	$1, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	testq	%rcx, %rcx
	je	.L46
	movq	%rax, %rdi
	movl	$2047, %r10d
	shrq	$50, %rdi
	xorl	$1, %edi
	cmpq	$32767, %rdx
	movl	$0, %edx
	cmovne	%edx, %edi
	shrq	$60, %r9
	salq	$4, %rax
	orq	%r9, %rax
	movabsq	$18014398509481984, %rdx
	andq	$-8, %rax
	orq	%rdx, %rax
.L40:
	movl	%r10d, %edx
	shrq	$3, %rax
	andw	$2047, %dx
	cmpq	$2047, %r10
	jne	.L33
	testq	%rax, %rax
	je	.L33
	movabsq	$2251799813685248, %rdx
	orq	%rdx, %rax
	movl	$2047, %edx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L97:
	testb	%r8b, %r8b
	jne	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$2047, %edx
	movl	$40, %edi
.L4:
	andw	$2047, %dx
	xorl	%eax, %eax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rax, %r9
	leaq	4(%rax), %rcx
	andl	$15, %r9d
	cmpq	$4, %r9
	cmovne	%rcx, %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$2047, %edx
.L91:
	xorl	%edi, %edi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L96:
	movl	28(%rsp), %eax
	andl	$24576, %eax
	je	.L50
	cmpl	$16384, %eax
	je	.L99
	cmpl	$8192, %eax
	jne	.L52
	testb	%r8b, %r8b
	jne	.L50
.L52:
	movl	$2046, %ecx
	movabsq	$2305843009213693951, %rax
	jmp	.L35
.L55:
	movl	$40, %edi
	movl	$2046, %r10d
	movq	$-1, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	(%rax,%rsi,8), %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	$-52, %r10
	jl	.L44
	movabsq	$2251799813685248, %rdx
	movl	%r10d, %edi
	orq	%rdx, %rax
	movl	$61, %edx
	subq	%r10, %rdx
	cmpq	$63, %rdx
	jle	.L100
	movl	$-3, %ecx
	subl	%r10d, %ecx
	movq	%rax, %r10
	shrq	%cl, %r10
	cmpq	$64, %rdx
	je	.L13
	leal	67(%rdi), %ecx
	salq	%cl, %rax
	orq	%rax, %r9
.L13:
	xorl	%eax, %eax
	testq	%r9, %r9
	setne	%al
	orq	%r10, %rax
.L12:
	movq	%rax, %r9
	andl	$7, %r9d
	testq	%rax, %rax
	je	.L48
	leaq	(%rax,%rax), %rdx
	xorl	%edi, %edi
	testb	$6, %dl
	je	.L18
	movl	$32, %edi
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$2047, %ecx
	xorl	%eax, %eax
.L35:
	andl	$2047, %ecx
	movabsq	$4503599627370495, %rdx
	salq	$63, %rsi
	salq	$52, %rcx
	andq	%rdx, %rax
	orl	$40, %edi
	orq	%rcx, %rax
	orq	%rsi, %rax
	movq	%rax, (%rsp)
.L37:
	call	__sfp_handle_exceptions@PLT
	movsd	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	8(%rdx), %rcx
	testq	%rsi, %rsi
	cmove	%rcx, %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	(%rdx,%rsi,8), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$32, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L100:
	leal	3(%r10), %edi
	movl	$61, %ecx
	movq	%r9, %rdx
	subl	%r10d, %ecx
	shrq	%cl, %rdx
	movl	%edi, %ecx
	salq	%cl, %r9
	xorl	%ecx, %ecx
	testq	%r9, %r9
	setne	%cl
	orq	%rcx, %rdx
	movl	%edi, %ecx
	salq	%cl, %rax
	orq	%rdx, %rax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L42:
	movq	$-1, %rax
	movl	$2046, %r10d
	xorl	%edx, %edx
	movl	$40, %edi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L22:
	movabsq	$36028797018963968, %rcx
	andq	%rax, %rcx
	testl	%edx, %edx
	je	.L28
	testb	$32, %dil
	jne	.L27
	testb	$8, 29(%rsp)
	jne	.L28
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L99:
	movl	%esi, %ecx
	movabsq	$2305843009213693951, %rdx
	movl	$0, %eax
	negl	%ecx
	addw	$2047, %cx
	testq	%rsi, %rsi
	cmovne	%rdx, %rax
	jmp	.L35
.L98:
	movl	$40, %edi
	movl	$2046, %r10d
	movq	$-1, %rax
	xorl	%edx, %edx
	jmp	.L29
.L48:
	xorl	%r10d, %r10d
	xorl	%edi, %edi
	jmp	.L40
	.size	__trunctfdf2, .-__trunctfdf2
