	.text
	.p2align 4,,15
	.globl	__floatunsitf
	.type	__floatunsitf, @function
__floatunsitf:
	testl	%edi, %edi
	je	.L3
	movl	%edi, %edi
	movl	$16446, %eax
	movl	$16431, %ecx
	bsrq	%rdi, %rdx
	xorq	$63, %rdx
	subl	%edx, %eax
	movabsq	$281474976710655, %rdx
	subl	%eax, %ecx
	salq	%cl, %rdi
	andq	%rdx, %rdi
	movl	%eax, %edx
	andw	$32767, %dx
.L2:
	movabsq	$281474976710655, %rax
	movq	$0, -16(%rsp)
	movabsq	$-281474976710656, %rcx
	andq	%rdi, %rax
	movq	-16(%rsp), %rdi
	movq	$0, -24(%rsp)
	andq	%rcx, %rdi
	orq	%rax, %rdi
	movq	%rdx, %rax
	movabsq	$-9223090561878065153, %rdx
	andq	%rdx, %rdi
	salq	$48, %rax
	movabsq	$9223372036854775807, %rdx
	orq	%rdi, %rax
	andq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movdqa	-24(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%edx, %edx
	xorl	%edi, %edi
	jmp	.L2
	.size	__floatunsitf, .-__floatunsitf
