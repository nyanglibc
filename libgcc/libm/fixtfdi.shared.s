	.text
	.p2align 4,,15
	.globl	__fixtfdi
	.type	__fixtfdi, @function
__fixtfdi:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
#APP
# 41 "/root/nyanlinux/src/glibc-2.33/soft-fp/fixtfdi.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	movq	8(%rsp), %rcx
	movq	(%rsp), %r9
	movq	%rcx, %rdx
	movq	%rcx, %rsi
	andq	%rcx, %rax
	shrq	$48, %rdx
	shrq	$63, %rsi
	movq	%rax, %rdi
	andw	$32767, %dx
	movzwl	%dx, %r8d
	cmpq	$16382, %r8
	jg	.L2
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L27
.L14:
	movl	$32, %edi
.L3:
	movq	%rax, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movq	(%rsp), %rax
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$16445, %r8
	jle	.L5
	movabsq	$9223372036854775807, %rax
	addq	%rsi, %rax
	cmpq	$16446, %r8
	jne	.L13
	testb	%sil, %sil
	je	.L13
	movq	%r9, %rdx
	salq	$15, %rdi
	shrq	$49, %rdx
	orq	%rdx, %rdi
	jne	.L13
	salq	$15, %r9
	testq	%r9, %r9
	jne	.L14
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	orq	%r9, %rdi
	je	.L1
	movl	$34, %edi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$16495, %ecx
	movabsq	$281474976710656, %rax
	movzwl	%dx, %edx
	subq	%r8, %rcx
	orq	%rdi, %rax
	cmpq	$63, %rcx
	jle	.L28
	cmpq	$64, %rcx
	je	.L8
	leal	-16367(%rdx), %ecx
	movq	%rax, %rdi
	salq	%cl, %rdi
	orq	%rdi, %r9
.L8:
	xorl	%r8d, %r8d
	movl	$16431, %ecx
	testq	%r9, %r9
	setne	%r8b
	subl	%edx, %ecx
	shrq	%cl, %rax
.L7:
	movq	%rax, %rdx
	negq	%rdx
	testb	%sil, %sil
	cmovne	%rdx, %rax
	testl	%r8d, %r8d
	jne	.L14
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leal	-16431(%rdx), %r10d
	movq	%r9, %rdi
	xorl	%r8d, %r8d
	movl	%r10d, %ecx
	salq	%cl, %rdi
	movl	$16495, %ecx
	testq	%rdi, %rdi
	setne	%r8b
	subl	%edx, %ecx
	shrq	%cl, %r9
	movl	%r10d, %ecx
	salq	%cl, %rax
	orq	%r9, %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %edi
	jmp	.L3
	.size	__fixtfdi, .-__fixtfdi
