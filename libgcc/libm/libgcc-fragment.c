/*
 *__LIBGCC_* ARE HARDCODED IN GCC AND ENABLED WITH SPECIAL COMMAND LINE FLAG
 * -fbuilding-libgcc
 */
#if defined(L_mulhc3) || defined(L_divhc3)
typedef		float HFtype	__attribute__ ((mode (HF)));
typedef _Complex float HCtype	__attribute__ ((mode (HC)));
# define MTYPE	HFtype
# define CTYPE	HCtype
# define MODE	hc
# define CEXT	__LIBGCC_HF_FUNC_EXT__
# define NOTRUNC (!__LIBGCC_HF_EXCESS_PRECISION__)
#elif defined(L_mulsc3) || defined(L_divsc3)
typedef 	float SFtype	__attribute__ ((mode (SF)));
typedef _Complex float SCtype	__attribute__ ((mode (SC)));
# define MTYPE	SFtype
# define CTYPE	SCtype
# define MODE	sc
# define CEXT	__LIBGCC_SF_FUNC_EXT__
# define NOTRUNC (!__LIBGCC_SF_EXCESS_PRECISION__)
#elif defined(L_muldc3) || defined(L_divdc3)
typedef		float DFtype	__attribute__ ((mode (DF)));
typedef _Complex float DCtype	__attribute__ ((mode (DC)));
# define MTYPE	DFtype
# define CTYPE	DCtype
# define MODE	dc
# define CEXT	__LIBGCC_DF_FUNC_EXT__
# define NOTRUNC (!__LIBGCC_DF_EXCESS_PRECISION__)
#elif defined(L_mulxc3) || defined(L_divxc3)
typedef		float XFtype	__attribute__ ((mode (XF)));
typedef _Complex float XCtype	__attribute__ ((mode (XC)));
# define MTYPE	XFtype
# define CTYPE	XCtype
# define MODE	xc
# define CEXT	__LIBGCC_XF_FUNC_EXT__
# define NOTRUNC (!__LIBGCC_XF_EXCESS_PRECISION__)
#elif defined(L_multc3) || defined(L_divtc3)
typedef		float TFtype	__attribute__ ((mode (TF)));
typedef _Complex float TCtype	__attribute__ ((mode (TC)));
# define MTYPE	TFtype
# define CTYPE	TCtype
# define MODE	tc
# define CEXT	__LIBGCC_TF_FUNC_EXT__
# define NOTRUNC (!__LIBGCC_TF_EXCESS_PRECISION__)
#else
# error
#endif

#define CONCAT3(A,B,C)	_CONCAT3(A,B,C)
#define _CONCAT3(A,B,C)	A##B##C

#define CONCAT2(A,B)	_CONCAT2(A,B)
#define _CONCAT2(A,B)	A##B

/* All of these would be present in a full C99 implementation of <math.h>
   and <complex.h>.  Our problem is that only a few systems have such full
   implementations.  Further, libgcc_s.so isn't currently linked against
   libm.so, and even for systems that do provide full C99, the extra overhead
   of all programs using libgcc having to link against libm.  So avoid it.  */

#define isnan(x)	__builtin_expect ((x) != (x), 0)
#define isfinite(x)	__builtin_expect (!isnan((x) - (x)), 1)
#define isinf(x)	__builtin_expect (!isnan(x) & !isfinite(x), 0)

#define INFINITY	CONCAT2(__builtin_huge_val, CEXT) ()
#define I		1i

/* Helpers to make the following code slightly less gross.  */
#define COPYSIGN	CONCAT2(__builtin_copysign, CEXT)
#define FABS		CONCAT2(__builtin_fabs, CEXT)

/* Verify that MTYPE matches up with CEXT.  */
extern void *compile_type_assert[sizeof(INFINITY) == sizeof(MTYPE) ? 1 : -1];

/* Ensure that we've lost any extra precision.  */
#if NOTRUNC
# define TRUNC(x)
#else
# define TRUNC(x)	__asm__ ("" : "=m"(x) : "m"(x))
#endif

#if defined(L_mulhc3) || defined(L_mulsc3) || defined(L_muldc3) \
    || defined(L_mulxc3) || defined(L_multc3)

CTYPE
CONCAT3(__mul,MODE,3) (MTYPE a, MTYPE b, MTYPE c, MTYPE d)
{
  MTYPE ac, bd, ad, bc, x, y;
  CTYPE res;

  ac = a * c;
  bd = b * d;
  ad = a * d;
  bc = b * c;

  TRUNC (ac);
  TRUNC (bd);
  TRUNC (ad);
  TRUNC (bc);

  x = ac - bd;
  y = ad + bc;

  if (isnan (x) && isnan (y))
    {
      /* Recover infinities that computed as NaN + iNaN.  */
      _Bool recalc = 0;
      if (isinf (a) || isinf (b))
	{
	  /* z is infinite.  "Box" the infinity and change NaNs in
	     the other factor to 0.  */
	  a = COPYSIGN (isinf (a) ? 1 : 0, a);
	  b = COPYSIGN (isinf (b) ? 1 : 0, b);
	  if (isnan (c)) c = COPYSIGN (0, c);
	  if (isnan (d)) d = COPYSIGN (0, d);
          recalc = 1;
	}
     if (isinf (c) || isinf (d))
	{
	  /* w is infinite.  "Box" the infinity and change NaNs in
	     the other factor to 0.  */
	  c = COPYSIGN (isinf (c) ? 1 : 0, c);
	  d = COPYSIGN (isinf (d) ? 1 : 0, d);
	  if (isnan (a)) a = COPYSIGN (0, a);
	  if (isnan (b)) b = COPYSIGN (0, b);
	  recalc = 1;
	}
     if (!recalc
	  && (isinf (ac) || isinf (bd)
	      || isinf (ad) || isinf (bc)))
	{
	  /* Recover infinities from overflow by changing NaNs to 0.  */
	  if (isnan (a)) a = COPYSIGN (0, a);
	  if (isnan (b)) b = COPYSIGN (0, b);
	  if (isnan (c)) c = COPYSIGN (0, c);
	  if (isnan (d)) d = COPYSIGN (0, d);
	  recalc = 1;
	}
      if (recalc)
	{
	  x = INFINITY * (a * c - b * d);
	  y = INFINITY * (a * d + b * c);
	}
    }

  __real__ res = x;
  __imag__ res = y;
  return res;
}
#endif /* complex multiply */

