	.text
	.globl	__multf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__netf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__multc3
	.type	__multc3, @function
__multc3:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$192, %rsp
	movaps	%xmm1, (%rsp)
	movdqa	%xmm2, %xmm1
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm3, 16(%rsp)
	movaps	%xmm0, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 144(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	setne	%bl
	movdqa	32(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	144(%rsp), %xmm2
	setne	%al
	andb	%al, %bl
	jne	.L98
.L4:
	movdqa	32(%rsp), %xmm5
	movq	%rbp, %rax
	movaps	%xmm5, 0(%rbp)
	movaps	%xmm2, 16(%rbp)
	addq	$192, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L98:
	movdqa	48(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	movaps	%xmm2, 160(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 176(%rsp)
	movdqa	(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm7
	movaps	%xmm0, 144(%rsp)
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__eqtf2@PLT
	movdqa	144(%rsp), %xmm3
	testq	%rax, %rax
	sete	%r12b
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__netf2@PLT
	movdqa	48(%rsp), %xmm6
	testq	%rax, %rax
	setne	%al
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	andl	%eax, %r12d
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	160(%rsp), %xmm2
	jne	.L52
	movdqa	176(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 144(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	144(%rsp), %xmm2
	je	.L52
	testb	%r12b, %r12b
	movdqa	48(%rsp), %xmm6
	movdqa	.LC2(%rip), %xmm3
	pand	%xmm3, %xmm6
	movdqa	.LC0(%rip), %xmm0
	por	%xmm0, %xmm6
	movaps	%xmm6, 48(%rsp)
	jne	.L13
	pxor	%xmm0, %xmm0
.L13:
	movaps	%xmm2, 160(%rsp)
	pand	.LC3(%rip), %xmm0
	movaps	%xmm3, 144(%rsp)
	movdqa	(%rsp), %xmm6
	pand	%xmm3, %xmm6
	por	%xmm6, %xmm0
	movaps	%xmm0, (%rsp)
	movdqa	64(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	144(%rsp), %xmm3
	movdqa	160(%rsp), %xmm2
	jne	.L99
.L14:
	movl	%ebx, %r12d
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 160(%rsp)
	movaps	%xmm2, 144(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	144(%rsp), %xmm2
	movdqa	160(%rsp), %xmm3
	jne	.L100
.L12:
	movdqa	64(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	movaps	%xmm2, 160(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 176(%rsp)
	movdqa	16(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm6
	movaps	%xmm0, 144(%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__eqtf2@PLT
	movdqa	144(%rsp), %xmm3
	testq	%rax, %rax
	sete	%bl
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__netf2@PLT
	movdqa	64(%rsp), %xmm7
	testq	%rax, %rax
	setne	%al
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	andl	%eax, %ebx
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	160(%rsp), %xmm2
	jne	.L55
	movdqa	176(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 144(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	144(%rsp), %xmm2
	je	.L55
	testb	%bl, %bl
	movdqa	64(%rsp), %xmm7
	movdqa	.LC2(%rip), %xmm3
	pand	%xmm3, %xmm7
	movdqa	.LC0(%rip), %xmm0
	por	%xmm0, %xmm7
	movaps	%xmm7, 64(%rsp)
	jne	.L25
	pxor	%xmm0, %xmm0
.L25:
	movaps	%xmm3, 32(%rsp)
	pand	.LC3(%rip), %xmm0
	movdqa	16(%rsp), %xmm7
	pand	%xmm3, %xmm7
	por	%xmm7, %xmm0
	movaps	%xmm0, 16(%rsp)
	movdqa	48(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm3
	jne	.L101
.L26:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 32(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm3
	jne	.L102
.L28:
	movdqa	64(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L4
.L55:
	testb	%bl, %bl
	jne	.L103
	testb	%r12b, %r12b
	movaps	%xmm2, 144(%rsp)
	jne	.L28
	movdqa	80(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm6
	movaps	%xmm0, 160(%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	144(%rsp), %xmm2
	jne	.L58
	movdqa	160(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 80(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	je	.L58
.L30:
	movdqa	48(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	jne	.L104
.L42:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	jne	.L105
.L44:
	movdqa	64(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	jne	.L106
.L46:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	je	.L28
	movdqa	16(%rsp), %xmm7
	pand	.LC2(%rip), %xmm7
	movaps	%xmm7, 16(%rsp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L52:
	testb	%r12b, %r12b
	je	.L12
	movdqa	48(%rsp), %xmm6
	movdqa	.LC2(%rip), %xmm3
	pand	%xmm3, %xmm6
	movdqa	.LC0(%rip), %xmm0
	movaps	%xmm6, 48(%rsp)
	jmp	.L13
.L58:
	movdqa	96(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	movaps	%xmm2, 80(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 144(%rsp)
	movdqa	96(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	jne	.L59
	movdqa	144(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	jne	.L30
.L59:
	movdqa	112(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	movaps	%xmm2, 80(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 96(%rsp)
	movdqa	112(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	jne	.L60
	movdqa	96(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	jne	.L30
.L60:
	movdqa	128(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	movaps	%xmm2, 80(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 96(%rsp)
	movdqa	128(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	jne	.L4
	movdqa	96(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	80(%rsp), %xmm2
	je	.L4
	jmp	.L30
.L103:
	movdqa	64(%rsp), %xmm7
	movdqa	.LC2(%rip), %xmm3
	pand	%xmm3, %xmm7
	movdqa	.LC0(%rip), %xmm0
	movaps	%xmm7, 64(%rsp)
	jmp	.L25
.L100:
	movdqa	16(%rsp), %xmm7
	pand	%xmm3, %xmm7
	movaps	%xmm7, 16(%rsp)
	jmp	.L12
.L105:
	movdqa	(%rsp), %xmm7
	pand	.LC2(%rip), %xmm7
	movaps	%xmm7, (%rsp)
	jmp	.L44
.L104:
	movdqa	48(%rsp), %xmm6
	pand	.LC2(%rip), %xmm6
	movaps	%xmm6, 48(%rsp)
	jmp	.L42
.L102:
	movdqa	(%rsp), %xmm7
	pand	%xmm3, %xmm7
	movaps	%xmm7, (%rsp)
	jmp	.L28
.L101:
	movdqa	48(%rsp), %xmm6
	pand	%xmm3, %xmm6
	movaps	%xmm6, 48(%rsp)
	jmp	.L26
.L99:
	movdqa	64(%rsp), %xmm6
	pand	%xmm3, %xmm6
	movaps	%xmm6, 64(%rsp)
	jmp	.L14
.L106:
	movdqa	64(%rsp), %xmm6
	pand	.LC2(%rip), %xmm6
	movaps	%xmm6, 64(%rsp)
	jmp	.L46
	.size	__multc3, .-__multc3
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	2147418112
