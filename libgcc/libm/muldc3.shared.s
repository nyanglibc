	.text
	.p2align 4,,15
	.globl	__muldc3
	.type	__muldc3, @function
__muldc3:
	movapd	%xmm0, %xmm6
	movapd	%xmm2, %xmm7
	movapd	%xmm0, %xmm4
	mulsd	%xmm3, %xmm6
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm7
	movapd	%xmm1, %xmm11
	mulsd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm5
	movapd	%xmm6, %xmm1
	addsd	%xmm7, %xmm1
	movapd	%xmm4, %xmm10
	subsd	%xmm5, %xmm10
	ucomisd	%xmm1, %xmm1
	setp	%al
	ucomisd	%xmm10, %xmm10
	setp	%dl
	andb	%dl, %al
	jne	.L97
.L87:
	movapd	%xmm10, %xmm0
	ret
.L97:
	movapd	%xmm11, %xmm8
	ucomisd	%xmm11, %xmm11
	subsd	%xmm11, %xmm8
	movapd	%xmm0, %xmm9
	subsd	%xmm0, %xmm9
	setnp	%dl
	ucomisd	%xmm8, %xmm8
	setp	%cl
	andl	%ecx, %edx
	ucomisd	%xmm0, %xmm0
	jp	.L60
	ucomisd	%xmm9, %xmm9
	jp	.L98
.L60:
	testb	%dl, %dl
	jne	.L99
.L14:
	movapd	%xmm3, %xmm8
	movapd	%xmm2, %xmm9
	ucomisd	%xmm3, %xmm3
	subsd	%xmm3, %xmm8
	subsd	%xmm2, %xmm9
	setnp	%al
	ucomisd	%xmm8, %xmm8
	setp	%cl
	andl	%ecx, %eax
	ucomisd	%xmm2, %xmm2
	jp	.L63
	ucomisd	%xmm9, %xmm9
	jp	.L100
.L63:
	testb	%al, %al
	jne	.L101
	testb	%dl, %dl
	jne	.L32
	ucomisd	%xmm4, %xmm4
	movapd	%xmm4, %xmm8
	subsd	%xmm4, %xmm8
	jp	.L66
	ucomisd	%xmm8, %xmm8
	jp	.L34
.L66:
	ucomisd	%xmm5, %xmm5
	movapd	%xmm5, %xmm4
	subsd	%xmm5, %xmm4
	jp	.L67
	ucomisd	%xmm4, %xmm4
	jp	.L34
.L67:
	ucomisd	%xmm6, %xmm6
	movapd	%xmm6, %xmm4
	subsd	%xmm6, %xmm4
	jp	.L68
	ucomisd	%xmm4, %xmm4
	jp	.L34
.L68:
	ucomisd	%xmm7, %xmm7
	movapd	%xmm7, %xmm4
	subsd	%xmm7, %xmm4
	jp	.L87
	ucomisd	%xmm4, %xmm4
	jnp	.L87
.L34:
	ucomisd	%xmm0, %xmm0
	jp	.L102
.L50:
	ucomisd	%xmm11, %xmm11
	jp	.L103
.L52:
	ucomisd	%xmm2, %xmm2
	jp	.L104
.L54:
	ucomisd	%xmm3, %xmm3
	jp	.L105
.L32:
	movapd	%xmm0, %xmm10
	movapd	%xmm11, %xmm1
	movsd	.LC5(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm10
	mulsd	%xmm0, %xmm3
	subsd	%xmm1, %xmm10
	movapd	%xmm11, %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm10
	addsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	jmp	.L87
.L99:
	movq	.LC3(%rip), %xmm8
	movsd	.LC0(%rip), %xmm9
	andpd	%xmm8, %xmm0
.L15:
	movapd	%xmm11, %xmm12
	andpd	.LC4(%rip), %xmm9
	ucomisd	%xmm2, %xmm2
	andpd	%xmm8, %xmm12
	orpd	%xmm12, %xmm9
	movapd	%xmm9, %xmm11
	jp	.L106
.L16:
	ucomisd	%xmm3, %xmm3
	jp	.L62
	movl	%eax, %edx
	jmp	.L14
.L101:
	movq	.LC3(%rip), %xmm8
	movsd	.LC0(%rip), %xmm1
	andpd	%xmm8, %xmm2
.L29:
	movapd	%xmm3, %xmm7
	andpd	.LC4(%rip), %xmm1
	ucomisd	%xmm0, %xmm0
	andpd	%xmm8, %xmm7
	orpd	%xmm7, %xmm1
	movapd	%xmm1, %xmm3
	jp	.L107
.L30:
	ucomisd	%xmm11, %xmm11
	jnp	.L32
	andpd	%xmm8, %xmm11
	jmp	.L32
.L62:
	andpd	%xmm8, %xmm3
	movl	%eax, %edx
	jmp	.L14
.L105:
	andpd	.LC3(%rip), %xmm3
	jmp	.L32
.L104:
	andpd	.LC3(%rip), %xmm2
	jmp	.L54
.L103:
	andpd	.LC3(%rip), %xmm11
	jmp	.L52
.L102:
	andpd	.LC3(%rip), %xmm0
	jmp	.L50
.L106:
	andpd	%xmm8, %xmm2
	jmp	.L16
.L107:
	andpd	%xmm8, %xmm0
	jmp	.L30
.L98:
	movq	.LC3(%rip), %xmm8
	testb	%dl, %dl
	pxor	%xmm9, %xmm9
	andpd	%xmm8, %xmm0
	orpd	.LC2(%rip), %xmm0
	je	.L15
	movsd	.LC0(%rip), %xmm9
	jmp	.L15
.L100:
	movq	.LC3(%rip), %xmm8
	testb	%al, %al
	pxor	%xmm1, %xmm1
	andpd	%xmm8, %xmm2
	orpd	.LC2(%rip), %xmm2
	je	.L29
	movsd	.LC0(%rip), %xmm1
	jmp	.L29
	.size	__muldc3, .-__muldc3
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	2146435072
