	.text
	.p2align 4,,15
	.globl	__trunctfsf2
	.type	__trunctfsf2, @function
__trunctfsf2:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
#APP
# 43 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfsf2.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rsi
	movq	8(%rsp), %rax
	movq	(%rsp), %rcx
	movq	%rax, %rdx
	movq	%rax, %r8
	andq	%rsi, %rax
	shrq	$48, %rdx
	movq	%rcx, %rsi
	salq	$3, %rax
	shrq	$61, %rsi
	andl	$32767, %edx
	shrq	$63, %r8
	orq	%rsi, %rax
	leaq	0(,%rcx,8), %rsi
	leaq	1(%rdx), %rcx
	movzbl	%r8b, %r9d
	andl	$32767, %ecx
	cmpq	$1, %rcx
	jle	.L2
	subq	$16256, %rdx
	cmpq	$254, %rdx
	jg	.L83
	testq	%rdx, %rdx
	jle	.L84
	movq	%rax, %rcx
	salq	$39, %rcx
	orq	%rsi, %rcx
	setne	%cl
	shrq	$25, %rax
	xorl	%edi, %edi
	movzbl	%cl, %ecx
	orq	%rcx, %rax
	xorl	%ecx, %ecx
	movq	%rax, %r10
	andl	$7, %r10d
.L13:
	testq	%r10, %r10
	je	.L20
	movl	28(%rsp), %esi
.L6:
	andl	$24576, %esi
	orl	$32, %edi
	cmpl	$8192, %esi
	je	.L22
	cmpl	$16384, %esi
	je	.L23
	testl	%esi, %esi
	je	.L85
.L27:
	movq	%rax, %r10
	andl	$67108864, %r10d
	testl	%ecx, %ecx
	je	.L26
.L25:
	orl	$16, %edi
.L26:
	testq	%r10, %r10
	je	.L28
	addq	$1, %rdx
	cmpq	$255, %rdx
	je	.L86
	shrq	$3, %rax
	movabsq	$2305843009205305343, %rcx
	andq	%rcx, %rax
.L16:
	movzbl	%dl, %edx
	andl	$8388607, %eax
	sall	$31, %r8d
	sall	$23, %edx
	orl	%eax, %edx
	orl	%r8d, %edx
	testl	%edi, %edi
	movl	%edx, (%rsp)
	jne	.L32
	movss	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movl	28(%rsp), %esi
	movl	%esi, %eax
	andl	$24576, %eax
	je	.L34
	cmpl	$16384, %eax
	je	.L87
	cmpl	$8192, %eax
	jne	.L8
	testb	%r8b, %r8b
	movl	$255, %edx
	movl	$40, %edi
	je	.L8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L28:
	shrq	$3, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L2:
	orq	%rax, %rsi
	testq	%rdx, %rdx
	jne	.L14
	testq	%rsi, %rsi
	movl	$34, %edi
	je	.L80
.L10:
	movl	$1, %r10d
	movl	$1, %eax
	movl	$2, %edx
.L11:
	movl	28(%rsp), %esi
	andl	$24576, %esi
	cmpl	$8192, %esi
	je	.L17
	cmpl	$16384, %esi
	je	.L18
	testl	%esi, %esi
	jne	.L12
	movq	%rdx, %rsi
	leaq	4(%rdx), %rcx
	andl	$15, %esi
	cmpq	$4, %rsi
	cmovne	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	shrq	$27, %rcx
	xorq	$1, %rcx
	andl	$1, %ecx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rsi, %rsi
	je	.L40
	movq	%rax, %rdi
	shrq	$50, %rdi
	xorl	$1, %edi
	cmpq	$32767, %rdx
	movl	$0, %edx
	cmovne	%edx, %edi
	shrq	$28, %rax
	movl	$-1, %edx
	orq	$4194304, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L87:
	testb	%r8b, %r8b
	jne	.L35
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$255, %edx
	movl	$40, %edi
	xorl	%eax, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %r10
	leaq	4(%rax), %rsi
	andl	$15, %r10d
	cmpq	$4, %r10
	cmovne	%rsi, %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$255, %edx
.L80:
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L86:
	movl	28(%rsp), %ecx
	xorl	%edx, %edx
	movl	$-1, %eax
	andl	$24576, %ecx
	je	.L30
	cmpl	$16384, %ecx
	je	.L88
	cmpl	$8192, %ecx
	jne	.L44
	testb	%r8b, %r8b
	jne	.L30
.L44:
	movl	$-2, %eax
	movabsq	$2305843009213693951, %rdx
.L30:
	movzbl	%al, %eax
	andl	$8388607, %edx
	sall	$31, %r8d
	sall	$23, %eax
	orl	$40, %edi
	orl	%edx, %eax
	orl	%r8d, %eax
	movl	%eax, (%rsp)
.L32:
	call	__sfp_handle_exceptions@PLT
	movss	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$8192, %eax
	je	.L47
	cmpl	$16384, %eax
	jne	.L89
	movl	$254, %edx
	movq	$-1, %rax
	xorl	%ecx, %ecx
	movl	$40, %edi
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	8(%rax), %rsi
	testq	%r9, %r9
	cmove	%rsi, %rax
	jmp	.L27
.L47:
	movl	$254, %edx
	movq	$-1, %rax
	xorl	%ecx, %ecx
	movl	$40, %edi
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	(%rax,%r9,8), %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L84:
	cmpq	$-23, %rdx
	jge	.L90
	movl	$32, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L90:
	movabsq	$2251799813685248, %rcx
	orq	%rcx, %rax
	leal	38(%rdx), %ecx
	movq	%rax, %rdi
	salq	%cl, %rdi
	movq	%rdi, %rcx
	orq	%rsi, %rcx
	movl	$26, %ecx
	setne	%sil
	subl	%edx, %ecx
	xorl	%edi, %edi
	shrq	%cl, %rax
	movzbl	%sil, %esi
	orq	%rsi, %rax
	leaq	(%rax,%rax), %rdx
	movq	%rax, %r10
	andl	$7, %r10d
	testb	$6, %dl
	je	.L12
	movl	$32, %edi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	(%rdx,%r9,8), %rdx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	8(%rdx), %rcx
	testq	%r9, %r9
	cmove	%rcx, %rdx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L35:
	movq	$-1, %rax
	movl	$254, %edx
	xorl	%ecx, %ecx
	movl	$40, %edi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L20:
	testl	%ecx, %ecx
	je	.L28
	testb	$32, %dil
	jne	.L25
	testb	$8, 29(%rsp)
	je	.L25
	shrq	$3, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L88:
	cmpq	$1, %r9
	movabsq	$2305843009213693951, %rcx
	sbbl	%eax, %eax
	orl	$-2, %eax
	testq	%r9, %r9
	cmovne	%rcx, %rdx
	jmp	.L30
.L89:
	movl	$254, %edx
	movq	$-1, %rax
	xorl	%ecx, %ecx
	movl	$40, %edi
	jmp	.L27
	.size	__trunctfsf2, .-__trunctfsf2
