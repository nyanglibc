	.text
	.p2align 4,,15
	.globl	__mulsc3
	.type	__mulsc3, @function
__mulsc3:
	movaps	%xmm0, %xmm10
	movaps	%xmm2, %xmm11
	movaps	%xmm0, %xmm5
	mulss	%xmm3, %xmm10
	movaps	%xmm1, %xmm6
	mulss	%xmm1, %xmm11
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm6
	movaps	%xmm10, %xmm4
	addss	%xmm11, %xmm4
	movaps	%xmm5, %xmm9
	subss	%xmm6, %xmm9
	ucomiss	%xmm4, %xmm4
	setp	%al
	ucomiss	%xmm9, %xmm9
	setp	%dl
	andb	%dl, %al
	jne	.L97
.L87:
	movss	%xmm9, -8(%rsp)
	movss	%xmm4, -4(%rsp)
	movq	-8(%rsp), %xmm0
	ret
.L97:
	movaps	%xmm1, %xmm7
	ucomiss	%xmm1, %xmm1
	subss	%xmm1, %xmm7
	movaps	%xmm0, %xmm8
	subss	%xmm0, %xmm8
	setnp	%dl
	ucomiss	%xmm7, %xmm7
	setp	%cl
	andl	%ecx, %edx
	ucomiss	%xmm0, %xmm0
	jp	.L60
	ucomiss	%xmm8, %xmm8
	jp	.L98
.L60:
	testb	%dl, %dl
	jne	.L99
.L14:
	movaps	%xmm3, %xmm7
	movaps	%xmm2, %xmm8
	ucomiss	%xmm3, %xmm3
	subss	%xmm3, %xmm7
	subss	%xmm2, %xmm8
	setnp	%al
	ucomiss	%xmm7, %xmm7
	setp	%cl
	andl	%ecx, %eax
	ucomiss	%xmm2, %xmm2
	jp	.L63
	ucomiss	%xmm8, %xmm8
	jp	.L100
.L63:
	testb	%al, %al
	jne	.L101
	testb	%dl, %dl
	jne	.L32
	ucomiss	%xmm5, %xmm5
	movaps	%xmm5, %xmm7
	subss	%xmm5, %xmm7
	jp	.L66
	ucomiss	%xmm7, %xmm7
	jp	.L34
.L66:
	ucomiss	%xmm6, %xmm6
	movaps	%xmm6, %xmm5
	subss	%xmm6, %xmm5
	jp	.L67
	ucomiss	%xmm5, %xmm5
	jp	.L34
.L67:
	ucomiss	%xmm10, %xmm10
	movaps	%xmm10, %xmm5
	subss	%xmm10, %xmm5
	jp	.L68
	ucomiss	%xmm5, %xmm5
	jp	.L34
.L68:
	ucomiss	%xmm11, %xmm11
	movaps	%xmm11, %xmm5
	subss	%xmm11, %xmm5
	jp	.L87
	ucomiss	%xmm5, %xmm5
	jnp	.L87
.L34:
	ucomiss	%xmm0, %xmm0
	jp	.L102
.L50:
	ucomiss	%xmm1, %xmm1
	jp	.L103
.L52:
	ucomiss	%xmm2, %xmm2
	jp	.L104
.L54:
	ucomiss	%xmm3, %xmm3
	jp	.L105
.L32:
	movaps	%xmm0, %xmm9
	movaps	%xmm1, %xmm4
	mulss	%xmm3, %xmm0
	movss	.LC5(%rip), %xmm5
	mulss	%xmm3, %xmm4
	mulss	%xmm2, %xmm9
	mulss	%xmm2, %xmm1
	subss	%xmm4, %xmm9
	movaps	%xmm0, %xmm4
	addss	%xmm1, %xmm4
	mulss	%xmm5, %xmm9
	mulss	%xmm5, %xmm4
	jmp	.L87
.L99:
	movss	.LC3(%rip), %xmm7
	movss	.LC0(%rip), %xmm8
	andps	%xmm7, %xmm0
.L15:
	movaps	%xmm1, %xmm12
	andps	.LC4(%rip), %xmm8
	ucomiss	%xmm2, %xmm2
	andps	%xmm7, %xmm12
	orps	%xmm12, %xmm8
	movaps	%xmm8, %xmm1
	jp	.L106
.L16:
	ucomiss	%xmm3, %xmm3
	jp	.L62
	movl	%eax, %edx
	jmp	.L14
.L101:
	movss	.LC3(%rip), %xmm7
	movss	.LC0(%rip), %xmm4
	andps	%xmm7, %xmm2
.L29:
	movaps	%xmm3, %xmm6
	andps	.LC4(%rip), %xmm4
	ucomiss	%xmm0, %xmm0
	andps	%xmm7, %xmm6
	orps	%xmm6, %xmm4
	movaps	%xmm4, %xmm3
	jp	.L107
.L30:
	ucomiss	%xmm1, %xmm1
	jnp	.L32
	andps	%xmm7, %xmm1
	jmp	.L32
.L62:
	andps	%xmm7, %xmm3
	movl	%eax, %edx
	jmp	.L14
.L105:
	andps	.LC3(%rip), %xmm3
	jmp	.L32
.L104:
	andps	.LC3(%rip), %xmm2
	jmp	.L54
.L103:
	andps	.LC3(%rip), %xmm1
	jmp	.L52
.L102:
	andps	.LC3(%rip), %xmm0
	jmp	.L50
.L106:
	andps	%xmm7, %xmm2
	jmp	.L16
.L107:
	andps	%xmm7, %xmm0
	jmp	.L30
.L98:
	movss	.LC3(%rip), %xmm7
	testb	%dl, %dl
	pxor	%xmm8, %xmm8
	andps	%xmm7, %xmm0
	orps	.LC2(%rip), %xmm0
	je	.L15
	movss	.LC0(%rip), %xmm8
	jmp	.L15
.L100:
	movss	.LC3(%rip), %xmm7
	testb	%al, %al
	pxor	%xmm4, %xmm4
	andps	%xmm7, %xmm2
	orps	.LC2(%rip), %xmm2
	je	.L29
	movss	.LC0(%rip), %xmm4
	jmp	.L29
	.size	__mulsc3, .-__mulsc3
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	1065353216
	.long	0
	.long	0
	.long	0
	.align 16
.LC3:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	2139095040
