	.text
	.p2align 4,,15
	.globl	__trunctfxf2
	.type	__trunctfxf2, @function
__trunctfxf2:
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
#APP
# 42 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfxf2.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rax
	movq	8(%rsp), %rcx
	movq	(%rsp), %rsi
	movq	%rcx, %rdx
	movq	%rcx, %r8
	andq	%rax, %rcx
	shrq	$48, %rdx
	movq	%rsi, %rax
	salq	$3, %rcx
	andl	$32767, %edx
	shrq	$61, %rax
	shrq	$63, %r8
	leaq	1(%rdx), %rdi
	orq	%rax, %rcx
	movzbl	%r8b, %r10d
	leaq	0(,%rsi,8), %r9
	andl	$32767, %edi
	cmpq	$1, %rdi
	jle	.L2
	cmpq	$32767, %rdx
	je	.L102
	testq	%rdx, %rdx
	je	.L103
	salq	$18, %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	shrq	$49, %r9
	xorl	%esi, %esi
	orq	%rax, %r9
	movq	%rcx, %rax
	xorl	%edi, %edi
	salq	$15, %rax
	shrq	$49, %rcx
	orq	%rax, %r9
	movq	%r9, %rax
	andl	$7, %eax
.L11:
	testq	%rax, %rax
	je	.L21
	movl	28(%rsp), %r11d
	orl	$32, %edi
	andl	$24576, %r11d
	cmpl	$8192, %r11d
	je	.L22
	cmpl	$16384, %r11d
	je	.L23
	testl	%r11d, %r11d
	je	.L104
.L21:
	testl	%esi, %esi
	je	.L43
	testb	$32, %dil
	jne	.L25
	testb	$8, 29(%rsp)
	jne	.L43
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L102:
	movl	28(%rsp), %eax
	andl	$24576, %eax
	jne	.L105
.L46:
	movl	$40, %edi
.L4:
	movl	$32767, %edx
	movabsq	$-9223372036854775808, %rcx
.L39:
	movzwl	40(%rsp), %eax
	sall	$7, %r8d
	movq	%rcx, 32(%rsp)
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 40(%rsp)
	movzbl	%ah, %eax
	andl	$127, %eax
	orl	%r8d, %eax
	testl	%edi, %edi
	movb	%al, 41(%rsp)
	fldt	32(%rsp)
	jne	.L42
.L1:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rcx, %rax
	orq	%r9, %rax
	testq	%rdx, %rdx
	jne	.L12
	testq	%rax, %rax
	je	.L13
	salq	$18, %rsi
	xorl	%eax, %eax
	movl	$2, %edi
	testq	%rsi, %rsi
	setne	%al
	shrq	$49, %r9
	orq	%rax, %r9
	movq	%rcx, %rax
	shrq	$49, %rcx
	salq	$15, %rax
	orq	%rax, %r9
.L10:
	movq	%r9, %rax
	orq	%rcx, %rax
	je	.L16
	movq	%r9, %rax
	shrq	$63, %rax
	leaq	(%rax,%rcx,2), %rsi
	leaq	(%r9,%r9), %rax
	testb	$6, %al
	je	.L17
	movl	28(%rsp), %edx
	orl	$32, %edi
	andl	$24576, %edx
	cmpl	$8192, %edx
	je	.L18
	cmpl	$16384, %edx
	je	.L19
	testl	%edx, %edx
	je	.L106
	.p2align 4,,10
	.p2align 3
.L17:
	shrq	$3, %rsi
	movq	%r9, %rax
	xorl	%edx, %edx
	xorq	$1, %rsi
	andl	$7, %eax
	andl	$1, %esi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	testq	%rax, %rax
	je	.L47
	movq	%rcx, %rdi
	movl	$0, %eax
	shrq	$50, %rdi
	xorl	$1, %edi
	cmpq	$32767, %rdx
	movl	$32767, %edx
	cmovne	%eax, %edi
	movq	%rcx, %rax
	shrq	$49, %r9
	salq	$15, %rax
	shrq	$49, %rcx
	orq	%rax, %r9
	orq	$2, %rcx
	andq	$-8, %r9
.L43:
	movq	%rcx, %rsi
	movq	%r9, %rax
	shrq	$3, %rcx
	shrq	$3, %rax
	salq	$61, %rsi
	orq	%rsi, %rax
	cmpq	$32767, %rdx
	je	.L107
	testq	%rdx, %rdx
	jne	.L41
	.p2align 4,,10
	.p2align 3
.L16:
	movabsq	$9223372036854775807, %rcx
	xorl	%edx, %edx
	andq	%rax, %rcx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$16384, %eax
	je	.L108
	cmpl	$8192, %eax
	jne	.L8
	testb	%r8b, %r8b
	jne	.L46
.L8:
	cmpl	$8192, %eax
	je	.L52
	cmpl	$16384, %eax
	jne	.L109
	movq	$-1, %rcx
	movl	$32766, %edx
	xorl	%esi, %esi
	movq	%rcx, %r9
	movl	$40, %edi
.L23:
	testq	%r10, %r10
	je	.L100
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r9, %rax
	andl	$15, %eax
	cmpq	$4, %rax
	je	.L6
	movq	%r9, %rax
#APP
# 49 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfxf2.c" 1
	addq $4,%rax
	adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%esi, %esi
	je	.L26
.L25:
	orl	$16, %edi
.L26:
	testb	$4, %cl
	je	.L43
	addq	$1, %rdx
	cmpq	$32767, %rdx
	je	.L110
	movq	%r9, %rax
	salq	$61, %rcx
	movabsq	$6917529027641081856, %rsi
	shrq	$3, %rax
	andq	%rsi, %rcx
	orq	%rcx, %rax
.L41:
	movabsq	$-9223372036854775808, %rcx
	andw	$32767, %dx
	orq	%rax, %rcx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L110:
	movl	28(%rsp), %eax
	andl	$24576, %eax
	je	.L34
	cmpl	$16384, %eax
	je	.L111
	orl	$40, %edi
	cmpl	$8192, %eax
	jne	.L37
	testb	%r8b, %r8b
	jne	.L32
.L37:
	movl	$32766, %edx
	movq	$-1, %rax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L52:
	movq	$-1, %rcx
	movl	$32766, %edx
	xorl	%esi, %esi
	movq	%rcx, %r9
	movl	$40, %edi
.L22:
	testq	%r10, %r10
	je	.L6
.L100:
	movq	%r9, %rax
#APP
# 49 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfxf2.c" 1
	addq $8,%rax
	adcq $0,%rcx
# 0 "" 2
#NO_APP
	movq	%rax, %r9
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L103:
	movabsq	$2251799813685248, %rax
	salq	$17, %rsi
	orq	%rax, %rcx
	xorl	%eax, %eax
	testq	%rsi, %rsi
	setne	%al
	shrq	$50, %r9
	xorl	%edi, %edi
	orq	%rax, %r9
	movq	%rcx, %rax
	shrq	$50, %rcx
	salq	$14, %rax
	orq	%rax, %r9
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L34:
	orl	$40, %edi
.L32:
	movzwl	40(%rsp), %edx
	sall	$7, %r8d
	movabsq	$-9223372036854775808, %rax
	movq	%rax, 32(%rsp)
	orw	$32767, %dx
	movw	%dx, 40(%rsp)
	movzbl	%dh, %edx
	andl	$127, %edx
	orl	%edx, %r8d
	movb	%r8b, 41(%rsp)
	fldt	32(%rsp)
.L42:
	fstpt	(%rsp)
	call	__sfp_handle_exceptions@PLT
	fldt	(%rsp)
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	xorl	%edi, %edi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L13:
	movzwl	40(%rsp), %eax
	sall	$7, %r8d
	movq	$0, 32(%rsp)
	andw	$-32768, %ax
	movw	%ax, 40(%rsp)
	movb	%r8b, 41(%rsp)
	fldt	32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L108:
	testb	%r8b, %r8b
	je	.L46
	movq	$-1, %rcx
	movl	$32766, %edx
	movl	$40, %edi
	movq	%rcx, %r9
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rax, %rdx
	andl	$15, %edx
	cmpq	$4, %rdx
	je	.L17
#APP
# 49 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfxf2.c" 1
	addq $4,%rax
	adcq $0,%rsi
# 0 "" 2
#NO_APP
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	testq	%r10, %r10
	jne	.L17
.L99:
#APP
# 49 "/root/nyanlinux/src/glibc-2.33/soft-fp/trunctfxf2.c" 1
	addq $8,%rax
	adcq $0,%rsi
# 0 "" 2
#NO_APP
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%r10, %r10
	je	.L17
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L111:
	testq	%r10, %r10
	je	.L34
	orl	$40, %edi
	jmp	.L37
.L109:
	orq	$-1, %rcx
	movl	$32766, %edx
	movl	$40, %edi
	movq	%rcx, %r9
	jmp	.L26
.L107:
	orq	%rax, %rcx
	je	.L4
	movabsq	$-4611686018427387904, %rcx
	movl	$32767, %edx
	orq	%rax, %rcx
	jmp	.L39
	.size	__trunctfxf2, .-__trunctfxf2
