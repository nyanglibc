	.text
	.p2align 4,,15
	.globl	__sfp_handle_exceptions
	.type	__sfp_handle_exceptions, @function
__sfp_handle_exceptions:
	testb	$1, %dil
	je	.L2
	pxor	%xmm0, %xmm0
#APP
# 52 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	divss	%xmm0, %xmm0
# 0 "" 2
#NO_APP
	movss	%xmm0, -48(%rsp)
.L2:
	testb	$2, %dil
	je	.L3
#APP
# 62 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fnstenv	-40(%rsp)
# 0 "" 2
#NO_APP
	orw	$2, -36(%rsp)
#APP
# 64 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fldenv	-40(%rsp)
# 0 "" 2
# 65 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fwait
# 0 "" 2
#NO_APP
.L3:
	testb	$4, %dil
	je	.L4
	movss	.LC1(%rip), %xmm0
#APP
# 72 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	divss	.LC0(%rip), %xmm0
# 0 "" 2
#NO_APP
	movss	%xmm0, -44(%rsp)
.L4:
	testb	$8, %dil
	je	.L5
#APP
# 82 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fnstenv	-40(%rsp)
# 0 "" 2
#NO_APP
	orw	$8, -36(%rsp)
#APP
# 84 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fldenv	-40(%rsp)
# 0 "" 2
# 85 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fwait
# 0 "" 2
#NO_APP
.L5:
	testb	$16, %dil
	je	.L6
#APP
# 90 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fnstenv	-40(%rsp)
# 0 "" 2
#NO_APP
	orw	$16, -36(%rsp)
#APP
# 92 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fldenv	-40(%rsp)
# 0 "" 2
# 93 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	fwait
# 0 "" 2
#NO_APP
.L6:
	andl	$32, %edi
	je	.L1
	movss	.LC1(%rip), %xmm0
#APP
# 100 "/root/wip/nyanglibc/fork/libgcc/sfp-exceptions.c" 1
	divss	.LC2(%rip), %xmm0
# 0 "" 2
#NO_APP
	movss	%xmm0, -40(%rsp)
.L1:
	rep ret
	.size	__sfp_handle_exceptions, .-__sfp_handle_exceptions
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	1065353216
	.align 4
.LC2:
	.long	1077936128
