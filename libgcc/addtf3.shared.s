	.text
	.p2align 4,,15
	.globl	__addtf3
	.type	__addtf3, @function
__addtf3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 43 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rbx
	movq	8(%rsp), %rdx
	movq	24(%rsp), %r10
	movq	(%rsp), %rcx
	movq	16(%rsp), %r15
	movq	%rdx, %rax
	movq	%r10, %rdi
	movq	%rdx, %r11
	shrq	$48, %rax
	shrq	$48, %rdi
	movq	%r10, %r12
	andw	$32767, %ax
	shrq	$63, %r11
	andq	%rbx, %rdx
	andw	$32767, %di
	andq	%rbx, %r10
	shrq	$63, %r12
	movq	%rcx, %rsi
	movq	%r15, %rbx
	salq	$3, %rdx
	shrq	$61, %rsi
	shrq	$61, %rbx
	salq	$3, %r10
	movzwl	%ax, %r8d
	movzbl	%r11b, %ebp
	movzwl	%di, %r14d
	movzbl	%r12b, %r13d
	movzwl	%ax, %eax
	movzwl	%di, %edi
	orq	%rsi, %rdx
	orq	%rbx, %r10
	subl	%edi, %eax
	cmpq	%r13, %rbp
	movzbl	%r11b, %r9d
	leaq	0(,%rcx,8), %rsi
	leaq	0(,%r15,8), %rbx
	je	.L284
	cmpl	$0, %eax
	jle	.L53
	testq	%r14, %r14
	jne	.L54
	movq	%r10, %rdi
	orq	%rbx, %rdi
	je	.L285
	subl	$1, %eax
	jne	.L56
	movl	$2, %edi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %rbx,%rsi
	sbbq %r10,%rdx
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L57:
	btq	$51, %rdx
	jnc	.L100
	movabsq	$2251799813685247, %rax
	andq	%rdx, %rax
.L91:
	testq	%rax, %rax
	je	.L93
	bsrq	%rax, %rcx
	xorl	$63, %ecx
.L94:
	leal	-12(%rcx), %r11d
	cmpl	$63, %r11d
	jg	.L95
	movl	%r11d, %ecx
	movq	%rsi, %rdx
	salq	%cl, %rax
	movl	$64, %ecx
	subl	%r11d, %ecx
	shrq	%cl, %rdx
	movl	%r11d, %ecx
	orq	%rdx, %rax
	salq	%cl, %rsi
.L96:
	movslq	%r11d, %rdx
	cmpq	%r8, %rdx
	jl	.L97
	subl	%r8d, %r11d
	leal	1(%r11), %edx
	cmpl	$63, %edx
	jg	.L98
	movl	$64, %r8d
	movq	%rax, %r10
	movq	%rsi, %rbx
	subl	%edx, %r8d
	movl	%r8d, %ecx
	salq	%cl, %r10
	movl	%edx, %ecx
	shrq	%cl, %rbx
	movl	%r8d, %ecx
	salq	%cl, %rsi
	orq	%rbx, %r10
	movl	%edx, %ecx
	testq	%rsi, %rsi
	setne	%sil
	shrq	%cl, %rax
	movzbl	%sil, %esi
	movq	%rax, %rdx
	orq	%r10, %rsi
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rsi, %r11
	orq	%rdx, %r11
	je	.L165
	movq	%rsi, %r11
	andl	$7, %r11d
.L125:
	leaq	(%rsi,%rsi), %rcx
	movq	%rsi, %rax
	shrq	$63, %rax
	testb	$6, %cl
	leaq	(%rax,%rdx,2), %rax
	je	.L101
	movl	44(%rsp), %r8d
	orl	$32, %edi
	andl	$24576, %r8d
	cmpl	$8192, %r8d
	je	.L102
	cmpl	$16384, %r8d
	je	.L103
	testl	%r8d, %r8d
	je	.L286
	.p2align 4,,10
	.p2align 3
.L101:
	shrq	$52, %rax
	xorl	%r8d, %r8d
	xorq	$1, %rax
	andl	$1, %eax
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L53:
	jne	.L287
	leaq	1(%r8), %rdi
	andl	$32767, %edi
	cmpq	$1, %rdi
	jle	.L288
	movq	%rdx, %rax
	movq	%rsi, %rcx
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %rbx,%rcx
	sbbq %r10,%rax
# 0 "" 2
#NO_APP
	btq	$51, %rax
	jc	.L289
	movq	%rax, %rbx
	orq	%rcx, %rbx
	je	.L92
	movq	%rcx, %rsi
	xorl	%edi, %edi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L284:
	cmpl	$0, %eax
	jle	.L3
	testq	%r14, %r14
	je	.L290
	cmpq	$32767, %r8
	je	.L291
	movabsq	$2251799813685248, %rcx
	xorl	%edi, %edi
	orq	%rcx, %r10
.L11:
	cmpl	$116, %eax
	jle	.L292
	orq	%rbx, %r10
	setne	%bl
	xorl	%r10d, %r10d
	movzbl	%bl, %ebx
.L18:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq %rbx,%rsi
	adcq %r10,%rdx
# 0 "" 2
#NO_APP
.L10:
	btq	$51, %rdx
	jnc	.L100
	addq	$1, %r8
	cmpq	$32767, %r8
	je	.L50
	movabsq	$-2251799813685249, %rax
	andq	%rax, %rdx
	movq	%rsi, %rax
	andl	$1, %esi
	shrq	%rax
	orq	%rax, %rsi
	movq	%rdx, %rax
	shrq	%rdx
	salq	$63, %rax
	orq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rsi, %r11
	andl	$7, %r11d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	$32767, %r8
	je	.L293
	movabsq	$2251799813685248, %rcx
	xorl	%edi, %edi
	orq	%rcx, %r10
.L58:
	cmpl	$116, %eax
	jg	.L60
	cmpl	$63, %eax
	jle	.L294
	leal	-64(%rax), %ecx
	movq	%r10, %r12
	shrq	%cl, %r12
	cmpl	$64, %eax
	je	.L63
	movl	$128, %ecx
	subl	%eax, %ecx
	salq	%cl, %r10
	orq	%r10, %rbx
.L63:
	xorl	%r11d, %r11d
	testq	%rbx, %rbx
	setne	%r11b
	xorl	%r10d, %r10d
	orq	%r12, %r11
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L290:
	movq	%r10, %rdi
	orq	%rbx, %rdi
	jne	.L5
	xorl	%edi, %edi
	cmpq	$32767, %r8
	jne	.L100
	movq	%rdx, %rax
	orq	%rsi, %rax
	je	.L7
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	$50, %rdi
	xorl	$1, %edi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L285:
	xorl	%edi, %edi
	cmpq	$32767, %r8
	je	.L295
.L6:
	.p2align 4,,10
	.p2align 3
.L100:
	testq	%r8, %r8
	je	.L36
	movq	%rsi, %r11
	xorl	%eax, %eax
	andl	$7, %r11d
.L37:
	testq	%r11, %r11
	je	.L8
	movl	44(%rsp), %ecx
	orl	$32, %edi
	andl	$24576, %ecx
	cmpl	$8192, %ecx
	je	.L106
	cmpl	$16384, %ecx
	je	.L107
	testl	%ecx, %ecx
	je	.L296
.L111:
	movabsq	$2251799813685248, %rcx
	andq	%rdx, %rcx
	testl	%eax, %eax
	je	.L110
.L109:
	orl	$16, %edi
.L110:
	testq	%rcx, %rcx
	je	.L297
.L112:
	addq	$1, %r8
	cmpq	$32767, %r8
	je	.L298
	movabsq	$-2251799813685249, %rax
	shrq	$3, %rsi
	andw	$32767, %r8w
	andq	%rax, %rdx
	movq	%rdx, %r11
	salq	$13, %rdx
	salq	$61, %r11
	shrq	$16, %rdx
	orq	%rsi, %r11
.L120:
	movabsq	$281474976710655, %rax
	movq	$0, 8(%rsp)
	movabsq	$-281474976710656, %rcx
	andq	%rdx, %rax
	movq	8(%rsp), %rdx
	salq	$48, %r8
	movzbl	%r9b, %r9d
	movq	%r11, (%rsp)
	salq	$63, %r9
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movabsq	$-9223090561878065153, %rax
	andq	%rax, %rdx
	movabsq	$9223372036854775807, %rax
	orq	%rdx, %r8
	andq	%rax, %r8
	orq	%r9, %r8
	testl	%edi, %edi
	movq	%r8, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jne	.L129
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	testq	%r8, %r8
	je	.L299
	cmpq	$32767, %r14
	je	.L300
	movabsq	$2251799813685248, %rcx
	negl	%eax
	xorl	%edi, %edi
	orq	%rcx, %rdx
.L69:
	cmpl	$116, %eax
	jg	.L71
	cmpl	$63, %eax
	jg	.L72
	movl	$64, %r9d
	movq	%rdx, %r8
	movq	%rsi, %r15
	subl	%eax, %r9d
	movl	%r9d, %ecx
	salq	%cl, %r8
	movl	%eax, %ecx
	shrq	%cl, %r15
	movl	%r9d, %ecx
	salq	%cl, %rsi
	orq	%r15, %r8
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	setne	%cl
	orq	%rcx, %r8
	movl	%eax, %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rax
.L73:
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movl	%r12d, %r9d
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %r8,%rsi
	sbbq %rax,%rdx
# 0 "" 2
#NO_APP
	movq	%r13, %rbp
	movq	%r14, %r8
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%rdx, %rax
	orq	%rsi, %rax
	je	.L15
.L275:
	movq	%rdx, %rdi
	xorl	%eax, %eax
	shrq	$50, %rdi
	xorq	$1, %rdi
	andl	$1, %edi
.L8:
	movabsq	$2251799813685248, %rcx
	andq	%rdx, %rcx
	testl	%eax, %eax
	je	.L110
	testb	$32, %dil
	jne	.L109
	testb	$8, 45(%rsp)
	je	.L109
	testq	%rcx, %rcx
	jne	.L112
.L297:
	movq	%rdx, %r11
	shrq	$3, %rsi
	shrq	$3, %rdx
	salq	$61, %r11
	orq	%rsi, %r11
	cmpq	$32767, %r8
	jne	.L35
.L113:
	movq	%r11, %rax
	orq	%rdx, %rax
	je	.L301
.L87:
	movabsq	$140737488355328, %r8
	movabsq	$281474976710655, %rax
	orq	%r8, %rdx
	movl	$32767, %r8d
	andq	%rax, %rdx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rdx, %rax
	orq	%rsi, %rax
	jne	.L275
.L159:
	movzbl	%r12b, %r9d
.L15:
	movq	$0, 8(%rsp)
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rdx
	salq	$63, %r9
	movq	$0, (%rsp)
	andq	%rdx, %rax
	movabsq	$9223090561878065152, %rdx
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	orq	%r9, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
.L35:
	movabsq	$281474976710655, %rax
	andw	$32767, %r8w
	andq	%rax, %rdx
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L298:
	movl	44(%rsp), %eax
	andl	$24576, %eax
	je	.L117
	cmpl	$16384, %eax
	je	.L302
	orl	$40, %edi
	testq	%rbp, %rbp
	je	.L279
	cmpl	$8192, %eax
	jne	.L279
.L12:
	movq	$0, 8(%rsp)
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rdx
	movzbl	%r9b, %r9d
	movq	$0, (%rsp)
	salq	$63, %r9
	andq	%rdx, %rax
	movabsq	$9223090561878065152, %rdx
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	orq	%r9, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L129:
	movaps	%xmm0, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movdqa	(%rsp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L174:
	movq	$-1, %rdx
	movl	$32766, %r8d
	xorl	%eax, %eax
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L106:
	testq	%rbp, %rbp
	je	.L111
.L280:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq $8,%rsi
	adcq $0,%rdx
# 0 "" 2
#NO_APP
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L3:
	jne	.L303
	leaq	1(%r8), %rdi
	movq	%rdi, %r11
	andl	$32767, %r11d
	cmpq	$1, %r11
	jle	.L304
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq %rbx,%rsi
	adcq %r10,%rdx
# 0 "" 2
#NO_APP
	cmpq	$32767, %rdi
	jne	.L305
	movl	44(%rsp), %eax
	andl	$24576, %eax
	je	.L144
	cmpl	$16384, %eax
	je	.L306
	cmpl	$8192, %eax
	movl	$40, %edi
	je	.L307
.L49:
	cmpl	$8192, %eax
	je	.L174
	cmpl	$16384, %eax
	jne	.L271
.L173:
	movq	$-1, %rdx
	movl	$32766, %r8d
	xorl	%eax, %eax
	movq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L107:
	testq	%rbp, %rbp
	je	.L280
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rsi, %rcx
	andl	$15, %ecx
	cmpq	$4, %rcx
	je	.L111
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq $4,%rsi
	adcq $0,%rdx
# 0 "" 2
#NO_APP
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L97:
	subq	%rdx, %r8
	movabsq	$-2251799813685249, %rdx
	andq	%rax, %rdx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rsi, %rax
	subl	$76, %ecx
	xorl	%esi, %esi
	salq	%cl, %rax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L93:
	bsrq	%rsi, %rcx
	xorq	$63, %rcx
	addl	$64, %ecx
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L117:
	orl	$40, %edi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%r11d, %r11d
	orq	%rbx, %r10
	setne	%r11b
	xorl	%r10d, %r10d
.L62:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %r11,%rsi
	sbbq %r10,%rdx
# 0 "" 2
#NO_APP
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L288:
	movq	%rdx, %rdi
	movq	%r10, %r11
	orq	%rsi, %rdi
	orq	%rbx, %r11
	testq	%r8, %r8
	movq	%rdi, (%rsp)
	je	.L308
	cmpq	$32767, %r8
	je	.L309
	cmpq	$32767, %r14
	je	.L167
	cmpq	$0, (%rsp)
	je	.L123
	xorl	%edi, %edi
.L83:
	testq	%r11, %r11
	je	.L277
.L88:
	movq	%rdx, %rsi
	movabsq	$2305843009213693951, %r8
	shrq	$3, %rdx
	salq	$61, %rsi
	andq	%r8, %rcx
	orq	%rsi, %rcx
	movq	%r10, %rsi
	shrq	$3, %rsi
	cmpq	%rsi, %rdx
	ja	.L89
	salq	$61, %r10
	andq	%r15, %r8
	orq	%r10, %r8
	cmpq	%rsi, %rdx
	movq	%rsi, %rdx
	sete	%r9b
	cmpq	%r8, %rcx
	setnb	%r10b
	andb	%r10b, %r9b
	cmove	%r8, %rcx
	cmove	%r13, %rbp
.L89:
	movq	%rcx, %rsi
	salq	$3, %rdx
	movl	%ebp, %r9d
	shrq	$61, %rsi
	andl	$1, %r9d
	movl	$32767, %r8d
	orq	%rsi, %rdx
	leaq	0(,%rcx,8), %rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	subl	$1, %eax
	je	.L310
	cmpq	$32767, %r8
	movl	$2, %edi
	jne	.L11
	movq	%rdx, %rax
	orq	%rsi, %rax
	je	.L148
.L281:
	movabsq	$1125899906842624, %rcx
	andq	%rdx, %rcx
.L276:
	xorl	%eax, %eax
	cmpq	$1, %rcx
	sbbl	%edi, %edi
	notl	%edi
	addl	$3, %edi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L56:
	cmpq	$32767, %r8
	movl	$2, %edi
	jne	.L58
	movq	%rdx, %rax
	orq	%rsi, %rax
	je	.L12
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L98:
	leal	-64(%rdx), %ecx
	movq	%rax, %r8
	shrq	%cl, %r8
	cmpl	$64, %edx
	je	.L99
	movl	$128, %ecx
	subl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %rsi
.L99:
	testq	%rsi, %rsi
	setne	%sil
	xorl	%edx, %edx
	movzbl	%sil, %esi
	orq	%r8, %rsi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L289:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %rsi,%rbx
	sbbq %rdx,%r10
# 0 "" 2
#NO_APP
	movl	%r12d, %r9d
	movq	%r10, %rax
	movq	%rbx, %rsi
	movq	%r13, %rbp
	xorl	%edi, %edi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L292:
	cmpl	$63, %eax
	jg	.L17
	movl	$64, %r14d
	movq	%r10, %r13
	movq	%rbx, %r15
	subl	%eax, %r14d
	movl	%r14d, %ecx
	salq	%cl, %r13
	movl	%eax, %ecx
	shrq	%cl, %r15
	movl	%r14d, %ecx
	salq	%cl, %rbx
	orq	%r15, %r13
	movl	%eax, %ecx
	testq	%rbx, %rbx
	setne	%bl
	shrq	%cl, %r10
	movzbl	%bl, %ebx
	orq	%r13, %rbx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L294:
	movl	$64, %r12d
	movq	%r10, %r11
	movq	%rbx, %r14
	subl	%eax, %r12d
	movl	%r12d, %ecx
	salq	%cl, %r11
	movl	%eax, %ecx
	shrq	%cl, %r14
	movl	%r12d, %ecx
	salq	%cl, %rbx
	orq	%r14, %r11
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	setne	%cl
	orq	%rcx, %r11
	movl	%eax, %ecx
	shrq	%cl, %r10
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	jne	.L66
	cmpq	$32767, %r14
	je	.L311
	movl	%r12d, %r9d
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r14, %r8
	movq	%r13, %rbp
	xorl	%edi, %edi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L305:
	movq	%rsi, %rcx
	andl	$1, %esi
	movq	%rdi, %r8
	shrq	%rcx
	xorl	%edi, %edi
	orq	%rcx, %rsi
	movq	%rdx, %rcx
	shrq	%rdx
	salq	$63, %rcx
	orq	%rcx, %rsi
	movq	%rsi, %r11
	andl	$7, %r11d
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L308:
	testq	%rdi, %rdi
	jne	.L77
	testq	%r11, %r11
	je	.L92
	movl	%r12d, %r9d
	xorl	%r11d, %r11d
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rbp
.L34:
	movl	$2, %edi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L310:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq %rbx,%rsi
	adcq %r10,%rdx
# 0 "" 2
#NO_APP
	movl	$2, %edi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%rcx, %r8
	andl	$15, %r8d
	cmpq	$4, %r8
	je	.L101
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq $4,%rcx
	adcq $0,%rax
# 0 "" 2
#NO_APP
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L302:
	testq	%rbp, %rbp
	je	.L117
	orl	$40, %edi
.L279:
	movabsq	$2305843009213693951, %rdx
	movq	$-1, %r11
	movl	$32766, %r8d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L303:
	testq	%r8, %r8
	jne	.L21
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	jne	.L22
	cmpq	$32767, %r14
	je	.L312
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r14, %r8
	xorl	%edi, %edi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rdx, %rax
	orq	%rsi, %rax
	jne	.L275
.L7:
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L92:
	movl	44(%rsp), %eax
	andl	$24576, %eax
	cmpl	$8192, %eax
	sete	%r9b
	xorl	%edi, %edi
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L103:
	testq	%rbp, %rbp
	jne	.L101
.L273:
#APP
# 47 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq $8,%rcx
	adcq $0,%rax
# 0 "" 2
#NO_APP
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%rbp, %rbp
	je	.L101
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L304:
	movq	%rdx, %rdi
	orq	%rsi, %rdi
	testq	%r8, %r8
	movq	%rdi, %r11
	jne	.L32
	movq	%r10, %r11
	orq	%rbx, %r11
	testq	%rdi, %rdi
	jne	.L33
	testq	%r11, %r11
	jne	.L138
	xorl	%edx, %edx
	movl	%r12d, %r9d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L50:
	movl	44(%rsp), %eax
	orl	$40, %edi
	andl	$24576, %eax
	je	.L148
	cmpl	$16384, %eax
	je	.L313
	cmpl	$8192, %eax
	jne	.L49
	testb	%r11b, %r11b
	je	.L49
.L150:
	movl	$1, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	leal	-64(%rax), %ecx
	movq	%r10, %r13
	shrq	%cl, %r13
	cmpl	$64, %eax
	je	.L19
	movl	$128, %ecx
	subl	%eax, %ecx
	salq	%cl, %r10
	orq	%r10, %rbx
.L19:
	testq	%rbx, %rbx
	setne	%bl
	xorl	%r10d, %r10d
	movzbl	%bl, %ebx
	orq	%r13, %rbx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L21:
	cmpq	$32767, %r14
	je	.L314
	movabsq	$2251799813685248, %rcx
	negl	%eax
	xorl	%edi, %edi
	orq	%rcx, %rdx
.L25:
	cmpl	$116, %eax
	jg	.L27
	cmpl	$63, %eax
	jg	.L28
	movl	$64, %r13d
	movq	%rdx, %r8
	movq	%rsi, %r15
	subl	%eax, %r13d
	movl	%r13d, %ecx
	salq	%cl, %r8
	movl	%eax, %ecx
	shrq	%cl, %r15
	movl	%r13d, %ecx
	salq	%cl, %rsi
	orq	%r15, %r8
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	setne	%cl
	orq	%rcx, %r8
	movl	%eax, %ecx
	shrq	%cl, %rdx
	movq	%rdx, %rax
.L29:
	movq	%r10, %rdx
	movq	%rbx, %rsi
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq %r8,%rsi
	adcq %rax,%rdx
# 0 "" 2
#NO_APP
	movq	%r14, %r8
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	$-1, %eax
	je	.L315
	cmpq	$32767, %r14
	je	.L68
	notl	%eax
	movl	$2, %edi
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	xorl	%r8d, %r8d
	orq	%rsi, %rdx
	setne	%r8b
	xorl	%eax, %eax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L148:
	movl	%r12d, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%r10, %rax
	orq	%rbx, %rax
	je	.L159
.L274:
	movq	%r10, %rdi
	movq	%r10, %rdx
	movq	%rbx, %rsi
	shrq	$50, %rdi
	movl	$32767, %r8d
	movq	%r13, %rbp
	xorq	$1, %rdi
	movl	%r12d, %r9d
	xorl	%eax, %eax
	andl	$1, %edi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L32:
	cmpq	$32767, %r8
	je	.L316
	cmpq	$32767, %r14
	je	.L317
	testq	%rdi, %rdi
	je	.L141
	xorl	%edi, %edi
.L41:
	orq	%r10, %rbx
	je	.L127
.L124:
	movq	%rdx, %rsi
	movabsq	$2305843009213693951, %r8
	movq	%r10, %r9
	andq	%r8, %rcx
	salq	$61, %rsi
	shrq	$3, %rdx
	shrq	$3, %r9
	orq	%rcx, %rsi
	cmpq	%r9, %rdx
	ja	.L45
	movq	%r15, %rcx
	salq	$61, %r10
	andq	%r8, %rcx
	orq	%r10, %rcx
	cmpq	%r9, %rdx
	movq	%r9, %rdx
	sete	%r8b
	cmpq	%rcx, %rsi
	setnb	%r10b
	andb	%r10b, %r8b
	cmove	%rcx, %rsi
	cmove	%r13, %rbp
.L45:
	movq	%rsi, %rcx
	salq	$3, %rdx
	movl	%ebp, %r9d
	shrq	$61, %rcx
	salq	$3, %rsi
	andl	$1, %r9d
	orq	%rcx, %rdx
	movl	$32767, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L77:
	testq	%r11, %r11
	je	.L34
	movq	%rdx, %rax
	movq	%rsi, %rcx
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %rbx,%rcx
	sbbq %r10,%rax
# 0 "" 2
#NO_APP
	btq	$51, %rax
	jnc	.L78
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %rsi,%rbx
	sbbq %rdx,%r10
# 0 "" 2
#NO_APP
	movl	%r12d, %r9d
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rbp
	movl	$2, %edi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$-1, %eax
	je	.L318
	cmpq	$32767, %r14
	je	.L24
	notl	%eax
	movl	$2, %edi
	jmp	.L25
.L27:
	xorl	%r8d, %r8d
	orq	%rsi, %rdx
	setne	%r8b
	xorl	%eax, %eax
	jmp	.L29
.L72:
	leal	-64(%rax), %ecx
	movq	%rdx, %r9
	shrq	%cl, %r9
	cmpl	$64, %eax
	je	.L74
	movl	$128, %ecx
	subl	%eax, %ecx
	salq	%cl, %rdx
	orq	%rdx, %rsi
.L74:
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	setne	%r8b
	xorl	%eax, %eax
	orq	%r9, %r8
	jmp	.L73
.L309:
	cmpq	$0, (%rsp)
	je	.L81
	movq	%rdx, %rdi
	shrq	$50, %rdi
	xorq	$1, %rdi
	andl	$1, %edi
	cmpq	$32767, %r14
	jne	.L83
.L82:
	testq	%r11, %r11
	je	.L85
	movabsq	$1125899906842624, %rsi
	testq	%rsi, %r10
	movl	$1, %esi
	cmove	%esi, %edi
	cmpq	$0, (%rsp)
	jne	.L88
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rbp
	movl	%r12d, %r9d
.L277:
	movl	$32767, %r8d
	jmp	.L8
.L81:
	cmpq	$32767, %r14
	je	.L167
.L123:
	testq	%r11, %r11
	je	.L171
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rbp
	movl	%r12d, %r9d
	xorl	%edi, %edi
	movl	$32767, %r8d
	jmp	.L8
.L144:
	movl	%r12d, %r9d
	movl	$40, %edi
	jmp	.L12
.L167:
	xorl	%edi, %edi
	jmp	.L82
.L314:
	movq	%r10, %rax
	orq	%rbx, %rax
	je	.L159
.L278:
	movq	%r10, %rdi
	movq	%r10, %rdx
	movq	%rbx, %rsi
	shrq	$50, %rdi
	movl	$32767, %r8d
	xorl	%eax, %eax
	xorq	$1, %rdi
	andl	$1, %edi
	jmp	.L8
.L311:
	movq	%r10, %rax
	orq	%rbx, %rax
	jne	.L274
	movl	%r12d, %r9d
	jmp	.L7
.L315:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	subq %rsi,%rbx
	sbbq %rdx,%r10
# 0 "" 2
#NO_APP
	movl	%r12d, %r9d
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r14, %r8
	movq	%r13, %rbp
	movl	$2, %edi
	jmp	.L57
.L33:
	testq	%r11, %r11
	je	.L34
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq %rbx,%rsi
	adcq %r10,%rdx
# 0 "" 2
#NO_APP
	btq	$51, %rdx
	jnc	.L139
	movabsq	$-2251799813685249, %rcx
	movq	%rsi, %r11
	movl	$1, %r8d
	andq	%rcx, %rdx
	andl	$7, %r11d
	movl	$2, %edi
	jmp	.L37
.L85:
	cmpq	$0, (%rsp)
	jne	.L277
.L171:
	movl	$1, %edi
	movl	$1, %r9d
	movabsq	$140737488355328, %rdx
	jmp	.L87
.L68:
	movq	%r10, %rax
	orq	%rbx, %rax
	je	.L157
	movabsq	$1125899906842624, %rcx
	movq	%r10, %rdx
	movq	%rbx, %rsi
	andq	%r10, %rcx
	movl	$32767, %r8d
	movq	%r13, %rbp
	movl	%r12d, %r9d
	jmp	.L276
.L316:
	testq	%rdi, %rdi
	je	.L39
	movq	%rdx, %rdi
	shrq	$50, %rdi
	xorq	$1, %rdi
	andl	$1, %edi
	cmpq	$32767, %r14
	jne	.L41
	movq	%r10, %r14
	orq	%rbx, %r14
	je	.L127
.L126:
	movabsq	$1125899906842624, %rsi
	testq	%rsi, %r10
	movl	$1, %esi
	cmove	%esi, %edi
	testq	%r11, %r11
	jne	.L124
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rbp
	movl	$32767, %r8d
	jmp	.L8
.L28:
	leal	-64(%rax), %ecx
	movq	%rdx, %r13
	shrq	%cl, %r13
	cmpl	$64, %eax
	je	.L30
	movl	$128, %ecx
	subl	%eax, %ecx
	salq	%cl, %rdx
	orq	%rdx, %rsi
.L30:
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	setne	%r8b
	xorl	%eax, %eax
	orq	%r13, %r8
	jmp	.L29
.L313:
	testq	%rbp, %rbp
	jne	.L173
	xorl	%r9d, %r9d
	jmp	.L12
.L157:
	movl	%r12d, %r9d
	movl	$2, %edi
	jmp	.L12
.L312:
	movq	%r10, %rax
	orq	%rbx, %rax
	je	.L7
	jmp	.L278
.L318:
#APP
# 46 "/root/nyanlinux/src/glibc-2.33/soft-fp/addtf3.c" 1
	addq %rsi,%rbx
	adcq %rdx,%r10
# 0 "" 2
#NO_APP
	movq	%r14, %r8
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	jmp	.L10
.L138:
	xorl	%r11d, %r11d
	movq	%r10, %rdx
	movq	%rbx, %rsi
	jmp	.L34
.L78:
	movq	%rax, %r11
	orq	%rcx, %r11
	je	.L79
	movq	%rcx, %r11
	movq	%rax, %rdx
	movq	%rcx, %rsi
	andl	$7, %r11d
	jmp	.L34
.L317:
	movq	%r10, %rdi
	orq	%rbx, %rdi
	jne	.L170
	testq	%r11, %r11
	je	.L7
	xorl	%edi, %edi
.L127:
	movq	%r13, %rbp
	movl	$32767, %r8d
	jmp	.L8
.L141:
	movq	%r10, %rdx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movl	$32767, %r8d
	jmp	.L8
.L271:
	movq	$-1, %rdx
	movl	$32766, %r8d
	xorl	%eax, %eax
	movq	%rdx, %rsi
	jmp	.L111
.L306:
	testq	%rbp, %rbp
	movl	$1, %r9d
	jne	.L48
	xorl	%r9d, %r9d
	movl	$40, %edi
	jmp	.L12
.L307:
	xorl	%r9d, %r9d
	testq	%rbp, %rbp
	jne	.L150
.L48:
	movq	$-1, %rdx
	movl	$32766, %r8d
	movl	$40, %edi
	movq	%rdx, %rsi
	jmp	.L112
.L24:
	movq	%r10, %rax
	orq	%rbx, %rax
	je	.L157
	movabsq	$1125899906842624, %rcx
	movq	%r10, %rdx
	movq	%rbx, %rsi
	andq	%r10, %rcx
	movl	$32767, %r8d
	jmp	.L276
.L39:
	cmpq	$32767, %r14
	je	.L122
	movq	%r10, %rdx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	jmp	.L8
.L79:
	movl	44(%rsp), %eax
	movl	$2, %edi
	andl	$24576, %eax
	cmpl	$8192, %eax
	sete	%r9b
	xorl	%edx, %edx
	jmp	.L35
.L122:
	movq	%r10, %rsi
	orq	%rbx, %rsi
	je	.L7
.L170:
	xorl	%edi, %edi
	jmp	.L126
.L139:
	movl	$2, %edi
	jmp	.L36
.L301:
	xorl	%r11d, %r11d
	movl	$32767, %r8d
	xorl	%edx, %edx
	jmp	.L120
	.size	__addtf3, .-__addtf3
