	.text
	.p2align 4,,15
	.globl	__unordtf2
	.type	__unordtf2, @function
__unordtf2:
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 41 "/root/nyanlinux/src/glibc-2.33/soft-fp/unordtf2.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rdx
	movq	8(%rsp), %rcx
	movq	24(%rsp), %rax
	movq	(%rsp), %rsi
	movq	16(%rsp), %r9
	movq	%rcx, %r10
	shrq	$48, %rcx
	andq	%rdx, %r10
	andq	%rax, %rdx
	shrq	$48, %rax
	andl	$32767, %eax
	andl	$32767, %ecx
	movq	%rdx, %r8
	movq	%rax, %rdx
	jne	.L2
	orq	%rsi, %r10
	je	.L3
	xorl	%eax, %eax
	cmpq	$32767, %rdx
	movl	$2, %edi
	je	.L45
.L4:
	movq	%rax, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movq	(%rsp), %rax
.L1:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rax, %rax
	jne	.L6
	movq	%r8, %rax
	xorl	%edi, %edi
	orq	%r9, %rax
	setne	%dil
	addl	%edi, %edi
	cmpq	$32767, %rcx
	jne	.L12
.L8:
	orq	%r10, %rsi
	jne	.L11
	cmpq	$32767, %rdx
	jne	.L12
	movq	%r8, %rax
	orq	%r9, %rax
	je	.L12
.L13:
	orq	%r8, %r9
	movl	$1, %eax
	je	.L15
	movabsq	$140737488355328, %rdx
	testq	%rdx, %r8
	jne	.L15
.L14:
	orl	$1, %edi
	movl	$1, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%edi, %edi
	cmpq	$32767, %rcx
	je	.L8
.L42:
	xorl	%eax, %eax
	cmpq	$32767, %rdx
	jne	.L1
	orq	%r8, %r9
	je	.L1
	movabsq	$140737488355328, %rax
	testq	%rax, %r8
	je	.L27
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%rax, %rax
	jne	.L42
	orq	%r9, %r8
	jne	.L25
	xorl	%edi, %edi
.L12:
	xorl	%eax, %eax
.L15:
	testl	%edi, %edi
	jne	.L4
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movabsq	$140737488355328, %rax
	testq	%rax, %r10
	je	.L14
	cmpq	$32767, %rdx
	movl	$1, %eax
	jne	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L45:
	orq	%r8, %r9
	je	.L4
	movabsq	$140737488355328, %rax
	testq	%rax, %r8
	je	.L14
	movl	$1, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
	movl	$2, %edi
	jmp	.L4
.L27:
	xorl	%edi, %edi
	jmp	.L14
	.size	__unordtf2, .-__unordtf2
