	.text
	.p2align 4,,15
	.globl	__letf2
	.type	__letf2, @function
__letf2:
	pushq	%r14
	pushq	%rbx
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movaps	%xmm1, 16(%rsp)
#APP
# 42 "/root/nyanlinux/src/glibc-2.33/soft-fp/letf2.c" 1
	stmxcsr	44(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rdx
	movq	%rdx, %rcx
	movq	8(%rsp), %rax
	movq	(%rsp), %r9
	movq	16(%rsp), %rsi
	movq	%rax, %r11
	movq	%rax, %rdi
	shrq	$63, %rax
	movq	%rax, %r8
	movq	24(%rsp), %rax
	andq	%rdx, %r11
	shrq	$48, %rdi
	movq	%rax, %rdx
	andq	%rax, %rcx
	shrq	$63, %rax
	shrq	$48, %rdx
	andl	$32767, %edx
	andl	$32767, %edi
	jne	.L2
	movq	%r11, %rbx
	orq	%r9, %rbx
	je	.L83
	movl	$2, %r10d
.L3:
	cmpq	$32767, %rdx
	jne	.L12
.L28:
	movq	%rcx, %rbx
	orq	%rsi, %rbx
	jne	.L8
.L10:
	testq	%rdi, %rdi
	jne	.L29
.L12:
	movq	%r11, %rbx
	orq	%r9, %rbx
	testq	%rdx, %rdx
	jne	.L15
	movq	%rcx, %r14
	orq	%rsi, %r14
	je	.L84
.L15:
	testq	%rbx, %rbx
	jne	.L85
	cmpq	$1, %rax
	sbbq	%rax, %rax
	orq	$1, %rax
.L16:
	testl	%r10d, %r10d
	jne	.L11
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rdx, %rdx
	jne	.L5
	movq	%rcx, %rbx
	xorl	%r10d, %r10d
	orq	%rsi, %rbx
	setne	%r10b
	addl	%r10d, %r10d
	cmpq	$32767, %rdi
	jne	.L10
.L27:
	movq	%r11, %rbx
	orq	%r9, %rbx
	jne	.L8
	cmpq	$32767, %rdx
	je	.L28
.L29:
	testq	%rdx, %rdx
	jne	.L13
	movq	%rcx, %rbx
	orq	%rsi, %rbx
	je	.L20
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%r10d, %r10d
	cmpq	$32767, %rdi
	je	.L27
	cmpq	$32767, %rdx
	je	.L28
.L13:
	cmpq	%rax, %r8
	je	.L17
.L20:
	cmpq	$1, %r8
	sbbq	%rax, %rax
	andl	$2, %eax
	subq	$1, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L83:
	xorl	%r10d, %r10d
	testq	%rdx, %rdx
	jne	.L3
	movq	%rcx, %rbx
	xorl	%r10d, %r10d
	orq	%rsi, %rbx
	setne	%r10b
	addl	%r10d, %r10d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L16
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L8:
	orl	$1, %r10d
	movl	$2, %eax
.L11:
	movl	%r10d, %edi
	movq	%rax, (%rsp)
	call	__sfp_handle_exceptions@PLT
	movq	(%rsp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpq	%rdx, %rdi
	jg	.L20
.L18:
	cmpq	%rdx, %rdi
	jl	.L22
	cmpq	%rcx, %r11
	ja	.L20
	sete	%al
	cmpq	%rsi, %r9
	jbe	.L21
	testb	%al, %al
	jne	.L20
.L21:
	cmpq	%rcx, %r11
	jb	.L22
	cmpq	%rsi, %r9
	jnb	.L40
	testb	%al, %al
	je	.L40
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	$1, %r8
	sbbq	%rax, %rax
	orq	$1, %rax
	jmp	.L16
.L40:
	xorl	%eax, %eax
	jmp	.L16
.L85:
	cmpq	%rax, %r8
	jne	.L20
	jmp	.L18
	.size	__letf2, .-__letf2
	.globl	__lttf2
	.set	__lttf2,__letf2
