	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	semop
	.type	semop, @function
semop:
	xorl	%ecx, %ecx
	jmp	__semtimedop
	.size	semop, .-semop
	.hidden	__semtimedop
