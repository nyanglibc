	.text
	.p2align 4,,15
	.globl	__semtimedop
	.hidden	__semtimedop
	.type	__semtimedop, @function
__semtimedop:
	movq	%rcx, %r10
	movl	$220, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/semtimedop.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__semtimedop, .-__semtimedop
	.weak	semtimedop
	.set	semtimedop,__semtimedop
