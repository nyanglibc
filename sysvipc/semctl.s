	.text
	.p2align 4,,15
	.globl	__semctl
	.type	__semctl, @function
__semctl:
	cmpl	$20, %edx
	movq	%rcx, -24(%rsp)
	ja	.L2
	movl	$1, %eax
	movl	%edx, %ecx
	salq	%cl, %rax
	testl	$55297, %eax
	jne	.L9
	testl	$2039822, %eax
	je	.L2
	leaq	8(%rsp), %rax
	movl	$24, -72(%rsp)
	movq	-24(%rsp), %r10
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movq	%rax, -56(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%r10d, %r10d
.L3:
	movl	$66, %eax
#APP
# 126 "../sysdeps/unix/sysv/linux/semctl.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__semctl, .-__semctl
	.weak	semctl
	.set	semctl,__semctl
