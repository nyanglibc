	.text
	.p2align 4,,15
	.globl	__msgctl
	.type	__msgctl, @function
__msgctl:
	cmpl	$13, %esi
	ja	.L2
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	testl	$14350, %eax
	je	.L10
.L4:
	movl	$71, %eax
#APP
# 79 "../sysdeps/unix/sysv/linux/msgctl.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%esi, %esi
	jne	.L2
	xorl	%edx, %edx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__msgctl, .-__msgctl
	.weak	msgctl
	.set	msgctl,__msgctl
