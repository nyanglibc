	.text
	.p2align 4,,15
	.globl	semop
	.type	semop, @function
semop:
	xorl	%ecx, %ecx
	jmp	__semtimedop
	.size	semop, .-semop
	.hidden	__semtimedop
