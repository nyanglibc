	.text
	.p2align 4,,15
	.globl	ftok
	.type	ftok, @function
ftok:
	pushq	%rbx
	movl	%esi, %ebx
	subq	$144, %rsp
	movq	%rsp, %rsi
	call	__stat64
	testl	%eax, %eax
	js	.L3
	movl	(%rsp), %eax
	movzwl	8(%rsp), %edx
	sall	$24, %ebx
	sall	$16, %eax
	andl	$16711680, %eax
	orl	%edx, %eax
	orl	%ebx, %eax
.L1:
	addq	$144, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	ftok, .-ftok
	.hidden	__stat64
