	.text
	.p2align 4,,15
	.globl	shmdt
	.type	shmdt, @function
shmdt:
	movl	$67, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/shmdt.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	shmdt, .-shmdt
