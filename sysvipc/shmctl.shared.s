	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __shmctl,shmctl@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__shmctl
	.type	__shmctl, @function
__shmctl:
	cmpl	$15, %esi
	ja	.L2
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	testl	$40966, %eax
	jne	.L4
	testl	$6145, %eax
	jne	.L8
	testl	$16392, %eax
	je	.L2
.L4:
	movl	$31, %eax
#APP
# 78 "../sysdeps/unix/sysv/linux/shmctl.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%edx, %edx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__shmctl, .-__shmctl
