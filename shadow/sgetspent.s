	.text
	.p2align 4,,15
	.globl	sgetspent
	.type	sgetspent, @function
sgetspent:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
#APP
# 41 "sgetspent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	buffer.7715(%rip), %rdx
	movq	buffer_size.7716(%rip), %rbx
	testq	%rdx, %rdx
	je	.L20
.L5:
	leaq	8(%rsp), %r14
	leaq	resbuf.7717(%rip), %r13
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, buffer.7715(%rip)
.L13:
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__sgetspent_r
	cmpl	$34, %eax
	jne	.L21
	movq	buffer_size.7716(%rip), %rax
	movq	buffer.7715(%rip), %rbp
	leaq	1024(%rax), %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%rbx, buffer_size.7716(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L7
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%rbp, %rdi
	movl	%fs:(%rbx), %r12d
	call	free@PLT
	movq	$0, buffer.7715(%rip)
	movl	%r12d, %fs:(%rbx)
.L14:
	movq	$0, 8(%rsp)
.L10:
#APP
# 73 "sgetspent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L11
	subl	$1, lock(%rip)
.L12:
	movl	%r12d, %fs:(%rbx)
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	cmpq	$0, buffer.7715(%rip)
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	%fs:(%rbx), %r12d
	jne	.L10
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1024, %edi
	movq	$1024, buffer_size.7716(%rip)
	movl	$1024, %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, buffer.7715(%rip)
	jne	.L5
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	%fs:(%rbx), %r12d
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
#APP
# 73 "sgetspent.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L12
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 73 "sgetspent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.size	sgetspent, .-sgetspent
	.local	resbuf.7717
	.comm	resbuf.7717,72,32
	.local	buffer_size.7716
	.comm	buffer_size.7716,8,8
	.local	buffer.7715
	.comm	buffer.7715,8,8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__sgetspent_r
