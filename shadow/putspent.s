	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%s:%s:"
.LC2:
	.string	"%ld:"
.LC3:
	.string	"%ld"
	.text
	.p2align 4,,15
	.globl	putspent
	.type	putspent, @function
putspent:
.LFB64:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L4
	movq	%rdi, %rbp
	movq	%rax, %rdi
	movq	%rsi, %rbx
	call	__nss_valid_field
	testb	%al, %al
	je	.L4
	movq	8(%rbp), %rdi
	call	__nss_valid_field
	testb	%al, %al
	je	.L4
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L5
	movq	136(%rbx), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L6
#APP
# 43 "putspent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L8:
	movq	136(%rbx), %rdi
	movq	%r12, 8(%rdi)
.L6:
	addl	$1, 4(%rdi)
.L5:
	movq	8(%rbp), %rcx
	leaq	.LC0(%rip), %rax
	movq	0(%rbp), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	testq	%rcx, %rcx
	cmove	%rax, %rcx
	xorl	%eax, %eax
	call	fprintf
	movq	16(%rbp), %rdx
	shrl	$31, %eax
	movl	%eax, %r12d
	movl	%eax, %r13d
	cmpq	$-1, %rdx
	jne	.L10
.L14:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L93
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L15:
	movq	24(%rbp), %rdx
	cmpq	$-1, %rdx
	jne	.L16
.L20:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L94
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L21:
	movq	32(%rbp), %rdx
	cmpq	$-1, %rdx
	jne	.L22
.L26:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L95
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L27:
	movq	40(%rbp), %rdx
	cmpq	$-1, %rdx
	jne	.L28
.L32:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L96
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L33:
	movq	48(%rbp), %rdx
	cmpq	$-1, %rdx
	jne	.L34
.L38:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L97
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L39:
	movq	56(%rbp), %rdx
	cmpq	$-1, %rdx
	jne	.L40
.L44:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L98
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L45:
	movq	64(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L46
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	cmpl	$-2147483648, %eax
	sbbl	$-1, %r13d
.L46:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L99
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$10, (%rax)
.L49:
	testl	$32768, (%rbx)
	jne	.L51
.L50:
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L51
	movq	$0, 8(%rdi)
#APP
# 91 "putspent.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L52
	subl	$1, (%rdi)
.L51:
	xorl	%eax, %eax
	testl	%r13d, %r13d
	setne	%al
	negl	%eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	testl	%eax, %eax
	js	.L13
	cmpq	$-1, 16(%rbp)
	je	.L14
	movq	24(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L20
.L16:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	testl	%eax, %eax
	js	.L19
	cmpq	$-1, 24(%rbp)
	je	.L20
	movq	32(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L26
.L22:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	testl	%eax, %eax
	js	.L25
	cmpq	$-1, 32(%rbp)
	je	.L26
	movq	40(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L32
.L28:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	testl	%eax, %eax
	js	.L31
	cmpq	$-1, 40(%rbp)
	je	.L32
	movq	48(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L38
.L34:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	testl	%eax, %eax
	js	.L37
	cmpq	$-1, 48(%rbp)
	je	.L38
	movq	56(%rbp), %rdx
	cmpq	$-1, %rdx
	je	.L44
.L40:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	fprintf
	testl	%eax, %eax
	js	.L43
	cmpq	$-1, 56(%rbp)
	jne	.L45
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L93:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L15
	.p2align 4,,10
	.p2align 3
.L13:
	leal	1(%r12), %r13d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L25:
	addl	$1, %r13d
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L33
	.p2align 4,,10
	.p2align 3
.L31:
	addl	$1, %r13d
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	addl	$1, %r13d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L98:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L45
	.p2align 4,,10
	.p2align 3
.L43:
	addl	$1, %r13d
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L21
	.p2align 4,,10
	.p2align 3
.L19:
	addl	$1, %r13d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$10, %esi
	movq	%rbx, %rdi
	call	__overflow
	cmpl	$-1, %eax
	jne	.L49
	addl	$1, %r13d
	testl	$32768, (%rbx)
	jne	.L1
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L8
	call	__lll_lock_wait_private
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L52:
#APP
# 91 "putspent.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L51
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 91 "putspent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L51
.LFE64:
	.size	putspent, .-putspent
	.hidden	__lll_lock_wait_private
	.hidden	__overflow
	.hidden	fprintf
	.hidden	__nss_valid_field
