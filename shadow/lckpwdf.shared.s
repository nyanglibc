	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	noop_handler, @function
noop_handler:
	rep ret
	.size	noop_handler, .-noop_handler
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/.pwd.lock"
	.text
	.p2align 4,,15
	.globl	__lckpwdf
	.type	__lckpwdf, @function
__lckpwdf:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$608, %rsp
	movl	lock_fd(%rip), %ebp
	cmpl	$-1, %ebp
	jne	.L10
#APP
# 98 "lckpwdf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L7:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movl	$384, %edx
	movl	$524353, %esi
	call	__GI___open
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movl	%eax, lock_fd(%rip)
	jne	.L8
#APP
# 104 "lckpwdf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
.L20:
	subl	$1, lock(%rip)
.L3:
	addq	$608, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	448(%rsp), %rsi
	xorl	%eax, %eax
	movl	$17, %ecx
	leaq	288(%rsp), %r12
	movq	$-1, %r13
	leaq	16(%rsi), %rdx
	movq	%r13, 456(%rsp)
	movq	%rdx, %rdi
	movq	%r12, %rdx
	rep stosq
	leaq	noop_handler(%rip), %rax
	movl	$14, %edi
	movq	%rax, 448(%rsp)
	call	__GI___sigaction
	testl	%eax, %eax
	jns	.L11
.L28:
	movl	lock_fd(%rip), %edi
	testl	%edi, %edi
	jns	.L29
.L15:
#APP
# 128 "lckpwdf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L16
	subl	$1, lock(%rip)
	movl	%ebp, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	32(%rsp), %r14
	leaq	160(%rsp), %rsi
	movl	$1, %edi
	movq	$8192, 160(%rsp)
	movq	%r14, %rdx
	call	__GI___sigprocmask
	testl	%eax, %eax
	js	.L30
	movl	$15, %edi
	call	__GI_alarm
	pxor	%xmm0, %xmm0
	movl	lock_fd(%rip), %edi
	movq	%rsp, %rdx
	xorl	%eax, %eax
	movl	$1, %ecx
	movl	$7, %esi
	movw	%cx, (%rsp)
	movups	%xmm0, 2(%rsp)
	movw	%ax, 30(%rdx)
	movq	$0, 18(%rdx)
	xorl	%eax, %eax
	movl	$0, 26(%rdx)
	call	__GI___fcntl
	xorl	%edi, %edi
	movl	%eax, %ebx
	call	__GI_alarm
	xorl	%edx, %edx
	movq	%r14, %rsi
	movl	$2, %edi
	call	__GI___sigprocmask
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$14, %edi
	call	__GI___sigaction
	testl	%ebx, %ebx
	js	.L31
.L17:
#APP
# 140 "lckpwdf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L20
	xorl	%eax, %eax
#APP
# 140 "lckpwdf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L3
.L21:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 140 "lckpwdf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L29:
	call	__GI___close
	movl	%r13d, lock_fd(%rip)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$14, %edi
	call	__GI___sigaction
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
#APP
# 104 "lckpwdf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L21
.L10:
	movl	$-1, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L7
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L31:
	movl	lock_fd(%rip), %edi
	testl	%edi, %edi
	js	.L17
	call	__GI___close
	movl	%r13d, lock_fd(%rip)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
#APP
# 128 "lckpwdf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L10
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 128 "lckpwdf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%ebp, %ebx
	jmp	.L3
	.size	__lckpwdf, .-__lckpwdf
	.weak	lckpwdf
	.set	lckpwdf,__lckpwdf
	.p2align 4,,15
	.globl	__ulckpwdf
	.type	__ulckpwdf, @function
__ulckpwdf:
	movl	lock_fd(%rip), %r8d
	cmpl	$-1, %r8d
	je	.L41
	subq	$8, %rsp
#APP
# 156 "lckpwdf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L34
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L35:
	movl	lock_fd(%rip), %edi
	call	__GI___close
	movl	$-1, lock_fd(%rip)
	movl	%eax, %r8d
#APP
# 164 "lckpwdf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L36
	subl	$1, lock(%rip)
.L32:
	movl	%r8d, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L35
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
#APP
# 164 "lckpwdf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L32
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 164 "lckpwdf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%r8d, %eax
	ret
	.size	__ulckpwdf, .-__ulckpwdf
	.weak	ulckpwdf
	.set	ulckpwdf,__ulckpwdf
	.local	lock
	.comm	lock,4,4
	.data
	.align 4
	.type	lock_fd, @object
	.size	lock_fd, 4
lock_fd:
	.long	-1
	.hidden	__lll_lock_wait_private
