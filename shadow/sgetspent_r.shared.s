	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__nss_files_parse_spent
	.hidden	__GI__nss_files_parse_spent
	.type	__GI__nss_files_parse_spent, @function
__GI__nss_files_parse_spent:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$10, %esi
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	__GI_strchr@PLT
	testq	%rax, %rax
	je	.L2
	movb	$0, (%rax)
.L2:
	movq	%rbx, 0(%rbp)
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L84
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L3
.L84:
	cmpb	$58, %al
	jne	.L87
.L3:
	testb	%al, %al
	je	.L6
	movb	$0, (%rbx)
	addq	$1, %rbx
.L6:
	cmpb	$0, (%rbx)
	jne	.L7
	movq	0(%rbp), %rax
	movzbl	(%rax), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L88
.L7:
	movq	%rbx, 8(%rbp)
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L9
	testb	%al, %al
	jne	.L10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L89:
	cmpb	$58, %al
	je	.L9
.L10:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L89
.L9:
	testb	%al, %al
	je	.L12
	movb	$0, (%rbx)
	addq	$1, %rbx
.L12:
	cmpb	$0, (%rbx)
	jne	.L90
.L39:
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	8(%rsp), %r12
	movl	$10, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	__GI_strtoul
	movl	$4294967295, %edx
	movq	8(%rsp), %r13
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cltq
	cmpq	%rbx, %r13
	movq	%rax, 16(%rbp)
	je	.L91
.L14:
	movzbl	0(%r13), %eax
	cmpb	$58, %al
	je	.L92
	testb	%al, %al
	jne	.L39
.L16:
	cmpb	$0, 0(%r13)
	je	.L39
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	movq	8(%rsp), %rbx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cltq
	cmpq	%r13, %rbx
	movq	%rax, 24(%rbp)
	je	.L93
.L19:
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L94
	testb	%al, %al
	jne	.L39
.L21:
	cmpb	$0, (%rbx)
	je	.L39
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	movq	8(%rsp), %r13
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cltq
	cmpq	%rbx, %r13
	movq	%rax, 32(%rbp)
	je	.L95
.L24:
	movzbl	0(%r13), %eax
	cmpb	$58, %al
	je	.L96
	testb	%al, %al
	jne	.L39
.L26:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	0(%r13), %rdx
	movq	%fs:(%rax), %rcx
	movq	%rdx, %rax
	testb	$32, 1(%rcx,%rdx,2)
	je	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$1, %r13
	movsbq	0(%r13), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	movq	%rdx, %rax
	jne	.L27
.L28:
	testb	%al, %al
	je	.L85
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	movq	8(%rsp), %rbx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cltq
	cmpq	%r13, %rbx
	movq	%rax, 40(%rbp)
	je	.L97
.L30:
	movzbl	(%rbx), %edx
	cmpb	$58, %dl
	je	.L98
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L1
.L32:
	cmpb	$0, (%rbx)
	je	.L39
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	movq	8(%rsp), %r13
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cltq
	cmpq	%rbx, %r13
	movq	%rax, 48(%rbp)
	jne	.L36
	movq	$-1, 48(%rbp)
.L36:
	movzbl	0(%r13), %eax
	cmpb	$58, %al
	je	.L99
	testb	%al, %al
	jne	.L39
.L38:
	cmpb	$0, 0(%r13)
	je	.L39
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	movq	8(%rsp), %rbx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cltq
	cmpq	%r13, %rbx
	movq	%rax, 56(%rbp)
	jne	.L41
	movq	$-1, 56(%rbp)
.L41:
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L100
	testb	%al, %al
	jne	.L39
.L43:
	cmpb	$0, (%rbx)
	jne	.L101
	movq	$-1, 64(%rbp)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L88:
	movq	$0, 8(%rbp)
	movq	$0, 16(%rbp)
	movq	$0, 24(%rbp)
	movq	$0, 32(%rbp)
.L85:
	movq	$-1, %rax
	movq	%rax, 40(%rbp)
	movq	%rax, 48(%rbp)
	movq	%rax, 56(%rbp)
	movq	%rax, 64(%rbp)
	addq	$24, %rsp
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$1, %r13
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L91:
	movq	$-1, 16(%rbp)
	jmp	.L14
.L94:
	addq	$1, %rbx
	jmp	.L21
.L93:
	movq	$-1, 24(%rbp)
	jmp	.L19
.L96:
	addq	$1, %r13
	jmp	.L26
.L95:
	movq	$-1, 32(%rbp)
	jmp	.L24
.L97:
	movq	$-1, 40(%rbp)
	jmp	.L30
.L98:
	addq	$1, %rbx
	jmp	.L32
.L99:
	addq	$1, %r13
	movq	%r13, 8(%rsp)
	jmp	.L38
.L101:
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_strtoul
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	movq	%rax, 64(%rbp)
	movq	8(%rsp), %rax
	cmpq	%rbx, %rax
	jne	.L46
	movq	$-1, 64(%rbp)
.L46:
	cmpb	$0, (%rax)
	sete	%al
	movzbl	%al, %eax
	jmp	.L1
.L100:
	addq	$1, %rbx
	movq	%rbx, 8(%rsp)
	jmp	.L43
	.size	__GI__nss_files_parse_spent, .-__GI__nss_files_parse_spent
	.globl	_nss_files_parse_spent
	.set	_nss_files_parse_spent,__GI__nss_files_parse_spent
	.p2align 4,,15
	.globl	__sgetspent_r
	.hidden	__sgetspent_r
	.type	__sgetspent_r, @function
__sgetspent_r:
	movq	%rdi, %rax
	movq	%rdx, %rdi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leaq	-1(%rdi,%rcx), %rbx
	movq	%rcx, %rdx
	movq	%rsi, %rbp
	movq	%rax, %rsi
	movb	$0, (%rbx)
	movq	%r8, %r12
	call	__GI_strncpy
	cmpb	$0, (%rbx)
	movl	$34, %edx
	jne	.L102
	movq	__libc_errno@gottpoff(%rip), %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movq	%rbx, %r8
	addq	%fs:0, %r8
	call	__GI__nss_files_parse_spent
	testl	%eax, %eax
	jle	.L109
	xorl	%edx, %edx
	testq	%rbp, %rbp
	movq	%rbp, (%r12)
	je	.L105
.L102:
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movq	$0, (%r12)
.L105:
	movl	%fs:(%rbx), %edx
	popq	%rbx
	popq	%rbp
	movl	%edx, %eax
	popq	%r12
	ret
	.size	__sgetspent_r, .-__sgetspent_r
	.weak	sgetspent_r
	.set	sgetspent_r,__sgetspent_r
