	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_svcraw_create, svcraw_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	svcraw_stat, @function
svcraw_stat:
	movl	$2, %eax
	ret
	.size	svcraw_stat, .-svcraw_stat
	.p2align 4,,15
	.type	svcraw_destroy, @function
svcraw_destroy:
	rep ret
	.size	svcraw_destroy, .-svcraw_destroy
	.p2align 4,,15
	.type	svcraw_freeargs, @function
svcraw_freeargs:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__rpc_thread_variables
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	je	.L5
	movl	$2, 9136(%rdi)
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rcx
	addq	$9136, %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	svcraw_freeargs, .-svcraw_freeargs
	.p2align 4,,15
	.type	svcraw_getargs, @function
svcraw_getargs:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__rpc_thread_variables
	movq	240(%rax), %rdi
	testq	%rdi, %rdi
	je	.L8
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rcx
	popq	%rbx
	popq	%rbp
	addq	$9136, %rdi
	xorl	%eax, %eax
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	svcraw_getargs, .-svcraw_getargs
	.p2align 4,,15
	.type	svcraw_reply, @function
svcraw_reply:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	call	__rpc_thread_variables
	movq	240(%rax), %rbx
	testq	%rbx, %rbx
	je	.L13
	movq	9144(%rbx), %rax
	leaq	9136(%rbx), %rbp
	xorl	%esi, %esi
	movl	$0, 9136(%rbx)
	movq	%rbp, %rdi
	call	*40(%rax)
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	je	.L13
	movq	9144(%rbx), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	svcraw_reply, .-svcraw_reply
	.p2align 4,,15
	.type	svcraw_recv, @function
svcraw_recv:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	__rpc_thread_variables
	movq	240(%rax), %rax
	testq	%rax, %rax
	je	.L20
	leaq	9136(%rax), %rbx
	movl	$1, 9136(%rax)
	movq	9144(%rax), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_callmsg
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	jmp	.L18
	.size	svcraw_recv, .-svcraw_recv
	.p2align 4,,15
	.globl	__GI_svcraw_create
	.hidden	__GI_svcraw_create
	.type	__GI_svcraw_create, @function
__GI_svcraw_create:
	pushq	%rbx
	call	__rpc_thread_variables
	movq	240(%rax), %rbx
	testq	%rbx, %rbx
	je	.L27
.L23:
	xorl	%eax, %eax
	leaq	9136(%rbx), %rdi
	movl	$0, 8800(%rbx)
	movw	%ax, 8804(%rbx)
	leaq	server_ops(%rip), %rax
	movq	%rbx, %rsi
	movl	$2, %ecx
	movl	$8800, %edx
	movq	%rax, 8808(%rbx)
	leaq	9184(%rbx), %rax
	movq	%rax, 8848(%rbx)
	call	__GI_xdrmem_create
	leaq	8800(%rbx), %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$9584, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L23
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__GI_svcraw_create, .-__GI_svcraw_create
	.globl	__EI_svcraw_create
	.set	__EI_svcraw_create,__GI_svcraw_create
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	server_ops, @object
	.size	server_ops, 48
server_ops:
	.quad	svcraw_recv
	.quad	svcraw_stat
	.quad	svcraw_getargs
	.quad	svcraw_reply
	.quad	svcraw_freeargs
	.quad	svcraw_destroy
	.hidden	__rpc_thread_variables
