	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_user2netname, user2netname@GLIBC_2.2.5
	.symver __EI_getnetname, getnetname@GLIBC_2.2.5
	.symver __EI_netname2host, netname2host@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s.%d@%s"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_user2netname
	.hidden	__GI_user2netname
	.type	__GI_user2netname, @function
__GI_user2netname:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$256, %rsp
	testq	%rdx, %rdx
	je	.L13
	movq	%rsp, %r12
	movq	%rdx, %rsi
	movl	$255, %edx
	movq	%r12, %rdi
	call	__GI_strncpy
	movb	$0, 255(%rsp)
.L3:
	movq	%r12, %rdx
.L5:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L5
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	movl	%eax, %edi
	cmove	%rcx, %rdx
	addb	%al, %dil
	sbbq	$3, %rdx
	subq	%r12, %rdx
	addq	$18, %rdx
	cmpq	$255, %rdx
	jbe	.L14
.L7:
	xorl	%eax, %eax
.L1:
	addq	$256, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	OPSYS(%rip), %rdx
	leaq	.LC0(%rip), %rsi
	movq	%r12, %r8
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	__GI_sprintf
	movq	%rbx, %rdi
	call	__GI_strlen
	leaq	-1(%rbx,%rax), %rdx
	movl	$1, %eax
	cmpb	$46, (%rdx)
	jne	.L1
	movb	$0, (%rdx)
	addq	$256, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rsp, %r12
	movl	$256, %esi
	movq	%r12, %rdi
	call	__GI_getdomainname
	testl	%eax, %eax
	jns	.L3
	jmp	.L7
	.size	__GI_user2netname, .-__GI_user2netname
	.globl	__EI_user2netname
	.set	__EI_user2netname,__GI_user2netname
	.section	.rodata.str1.1
.LC1:
	.string	"%s.%s@%s"
	.text
	.p2align 4,,15
	.globl	__GI_host2netname
	.hidden	__GI_host2netname
	.type	__GI_host2netname, @function
__GI_host2netname:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$160, %rsp
	testq	%rsi, %rsi
	movb	$0, (%rdi)
	movq	%rsp, %rbx
	je	.L42
	movl	$64, %edx
	movq	%rbx, %rdi
	call	__GI_strncpy
	movb	$0, 64(%rsp)
.L17:
	movl	$46, %esi
	movq	%rbx, %rdi
	call	__GI_strchr
	testq	%r14, %r14
	movq	%rax, %r13
	je	.L43
	leaq	80(%rsp), %r12
	movl	$64, %edx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	__GI_strncpy
	movb	$0, 144(%rsp)
.L20:
	movq	%r12, %rax
.L21:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L21
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	movl	%edx, %edi
	cmove	%rcx, %rax
	addb	%dl, %dil
	sbbq	$3, %rax
	subq	%r12, %rax
	je	.L31
	subq	$1, %rax
	cmpb	$46, 80(%rsp,%rax)
	je	.L44
	testq	%r13, %r13
	je	.L26
.L46:
	movb	$0, 0(%r13)
.L26:
	movq	%r12, %rdx
.L27:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L27
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	movl	%eax, %edi
	cmove	%rcx, %rdx
	addb	%al, %dil
	movq	%rbx, %rcx
	sbbq	$3, %rdx
	subq	%r12, %rdx
.L29:
	movl	(%rcx), %esi
	addq	$4, %rcx
	leal	-16843009(%rsi), %eax
	notl	%esi
	andl	%esi, %eax
	andl	$-2139062144, %eax
	je	.L29
	movl	%eax, %esi
	shrl	$16, %esi
	testl	$32896, %eax
	cmove	%esi, %eax
	leaq	2(%rcx), %rsi
	movl	%eax, %edi
	cmove	%rsi, %rcx
	addb	%al, %dil
	sbbq	$3, %rcx
	subq	%rbx, %rcx
	leaq	7(%rdx,%rcx), %rax
	cmpq	$255, %rax
	jbe	.L45
.L31:
	addq	$160, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	testq	%rax, %rax
	je	.L19
	leaq	80(%rsp), %r12
	leaq	1(%rax), %rsi
	movl	$64, %edx
	movq	%r12, %rdi
	call	__GI_strncpy
	movb	$0, 144(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$64, %esi
	movq	%rbx, %rdi
	call	__gethostname
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L44:
	testq	%r13, %r13
	movb	$0, 80(%rsp,%rax)
	jne	.L46
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	OPSYS(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %r8
	movq	%rbx, %rcx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	__GI_sprintf
	addq	$160, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	80(%rsp), %r12
	movl	$64, %esi
	movb	$0, 80(%rsp)
	movq	%r12, %rdi
	call	__GI_getdomainname
	jmp	.L20
	.size	__GI_host2netname, .-__GI_host2netname
	.globl	host2netname
	.set	host2netname,__GI_host2netname
	.p2align 4,,15
	.globl	__GI_getnetname
	.hidden	__GI_getnetname
	.type	__GI_getnetname, @function
__GI_getnetname:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__geteuid
	xorl	%edx, %edx
	testl	%eax, %eax
	je	.L50
	movq	%rbx, %rdi
	movl	%eax, %esi
	popq	%rbx
	jmp	__GI_user2netname
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	jmp	__GI_host2netname
	.size	__GI_getnetname, .-__GI_getnetname
	.globl	__EI_getnetname
	.set	__EI_getnetname,__GI_getnetname
	.section	.rodata.str1.1
.LC2:
	.string	"netname2user"
	.text
	.p2align 4,,15
	.globl	__GI_netname2user
	.hidden	__GI_netname2user
	.type	__GI_netname2user, @function
__GI_netname2user:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%edx, %edx
	movq	%r8, %r15
	subq	$40, %rsp
	leaq	24(%rsp), %rbp
	leaq	16(%rsp), %r12
	movq	%rsi, 8(%rsp)
	leaq	.LC2(%rip), %rsi
	movq	%rdi, (%rsp)
	movq	%rbp, %rcx
	movq	%r12, %rdi
	call	__GI___nss_publickey_lookup2
	testl	%eax, %eax
	jne	.L54
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	call	*24(%rsp)
	leaq	.LC2(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%rbp, %rcx
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	__GI___nss_next2
	testl	%eax, %eax
	je	.L53
	xorl	%eax, %eax
	cmpl	$1, %ebx
	sete	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI_netname2user, .-__GI_netname2user
	.globl	netname2user
	.set	netname2user,__GI_netname2user
	.p2align 4,,15
	.globl	__GI_netname2host
	.hidden	__GI_netname2host
	.type	__GI_netname2host, @function
__GI_netname2host:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movl	$46, %esi
	movslq	%edx, %rbp
	call	__GI_strchr
	testq	%rax, %rax
	je	.L61
	leaq	1(%rax), %rbx
	movl	$64, %esi
	movq	%rbx, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L61
	cmpl	$255, %ebp
	movb	$0, (%rax)
	jle	.L66
.L61:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__GI_strncpy
	movb	$0, (%r12,%rbp)
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI_netname2host, .-__GI_netname2host
	.globl	__EI_netname2host
	.set	__EI_netname2host,__GI_netname2host
	.section	.rodata.str1.1
	.type	OPSYS, @object
	.size	OPSYS, 5
OPSYS:
	.string	"unix"
	.hidden	__geteuid
	.hidden	__gethostname
