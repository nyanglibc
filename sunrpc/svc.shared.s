	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xprt_register, xprt_register@GLIBC_2.2.5
	.symver __EI_xprt_unregister, xprt_unregister@GLIBC_2.2.5
	.symver __EI_svc_register, svc_register@GLIBC_2.2.5
	.symver __EI_svc_unregister, svc_unregister@GLIBC_2.2.5
	.symver __EI_svc_sendreply, svc_sendreply@GLIBC_2.2.5
	.symver __EI_svcerr_noproc, svcerr_noproc@GLIBC_2.2.5
	.symver __EI_svcerr_decode, svcerr_decode@GLIBC_2.2.5
	.symver __EI_svcerr_systemerr, svcerr_systemerr@GLIBC_2.2.5
	.symver __EI_svcerr_auth, svcerr_auth@GLIBC_2.2.5
	.symver __EI_svcerr_weakauth, svcerr_weakauth@GLIBC_2.2.5
	.symver __EI_svcerr_noprog, svcerr_noprog@GLIBC_2.2.5
	.symver __EI_svcerr_progvers, svcerr_progvers@GLIBC_2.2.5
	.symver __EI_svc_getreq, svc_getreq@GLIBC_2.2.5
	.symver __EI_svc_getreqset, svc_getreqset@GLIBC_2.2.5
	.symver __EI_svc_getreq_poll, svc_getreq_poll@GLIBC_2.2.5
	.symver __EI_svc_getreq_common, svc_getreq_common@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xprt_register
	.hidden	__GI_xprt_register
	.type	__GI_xprt_register, @function
__GI_xprt_register:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	(%rdi), %ebp
	call	__rpc_thread_variables
	cmpq	$0, 224(%rax)
	movq	%rax, %rbx
	je	.L2
.L5:
	call	__GI__rpc_dtablesize
	cmpl	%ebp, %eax
	jle	.L1
	movq	224(%rbx), %rdx
	movslq	%ebp, %rax
	cmpl	$1023, %ebp
	movq	%r12, (%rdx,%rax,8)
	jg	.L7
	call	__GI___rpc_thread_svc_fdset
	leal	63(%rbp), %edx
	testl	%ebp, %ebp
	movl	%ebp, %esi
	cmovns	%ebp, %edx
	sarl	$31, %esi
	shrl	$26, %esi
	sarl	$6, %edx
	leal	0(%rbp,%rsi), %ecx
	movslq	%edx, %rdx
	andl	$63, %ecx
	subl	%esi, %ecx
	movl	$1, %esi
	salq	%cl, %rsi
	orq	%rsi, (%rax,%rdx,8)
.L7:
	call	__GI___rpc_thread_svc_max_pollfd
	movq	%rax, %r12
	movl	(%rax), %ebx
	call	__GI___rpc_thread_svc_pollfd
	movq	(%rax), %rdi
	movq	%rax, %r13
	xorl	%edx, %edx
	movq	%rdi, %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rax, %rcx
	addq	$8, %rax
	cmpl	$-1, -8(%rax)
	je	.L16
	addl	$1, %edx
.L8:
	cmpl	%edx, %ebx
	jg	.L10
	leal	1(%rbx), %esi
	movslq	%esi, %rsi
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1
	movl	(%r12), %edi
	movq	%rax, 0(%r13)
	movl	$195, %ecx
	leal	1(%rdi), %edx
	movl	%edx, (%r12)
	movslq	%edx, %rdx
	movl	%ebp, -8(%rax,%rdx,8)
	movslq	(%r12), %rdx
	movw	%cx, -4(%rax,%rdx,8)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	call	__GI__rpc_dtablesize
	movl	$8, %esi
	movslq	%eax, %rdi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 224(%rbx)
	jne	.L5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$195, %esi
	movl	%ebp, (%rcx)
	movw	%si, 4(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI_xprt_register, .-__GI_xprt_register
	.globl	__EI_xprt_register
	.set	__EI_xprt_register,__GI_xprt_register
	.p2align 4,,15
	.globl	__GI_xprt_unregister
	.hidden	__GI_xprt_unregister
	.type	__GI_xprt_unregister, @function
__GI_xprt_unregister:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %ebp
	call	__GI__rpc_dtablesize
	cmpl	%ebp, %eax
	jle	.L17
	call	__rpc_thread_variables
	movq	224(%rax), %rcx
	movslq	%ebp, %rdx
	leaq	(%rcx,%rdx,8), %rax
	cmpq	%rbx, (%rax)
	je	.L25
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$1023, %ebp
	movq	$0, (%rax)
	jg	.L20
	call	__GI___rpc_thread_svc_fdset
	leal	63(%rbp), %edx
	testl	%ebp, %ebp
	movl	%ebp, %esi
	cmovns	%ebp, %edx
	sarl	$31, %esi
	shrl	$26, %esi
	sarl	$6, %edx
	leal	0(%rbp,%rsi), %ecx
	movslq	%edx, %rdx
	andl	$63, %ecx
	subl	%esi, %ecx
	movq	$-2, %rsi
	rolq	%cl, %rsi
	andq	%rsi, (%rax,%rdx,8)
.L20:
	call	__GI___rpc_thread_svc_max_pollfd
	xorl	%ebx, %ebx
	movq	%rax, %r13
	movl	(%rax), %r12d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	call	__GI___rpc_thread_svc_pollfd
	movq	(%rax), %rax
	leaq	(%rax,%rbx,8), %rax
	cmpl	%ebp, (%rax)
	jne	.L22
	movl	$-1, (%rax)
	movl	0(%r13), %r12d
.L22:
	addq	$1, %rbx
.L21:
	cmpl	%ebx, %r12d
	jg	.L23
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI_xprt_unregister, .-__GI_xprt_unregister
	.globl	__EI_xprt_unregister
	.set	__EI_xprt_unregister,__GI_xprt_unregister
	.p2align 4,,15
	.globl	__GI_svc_register
	.hidden	__GI_svc_register
	.type	__GI_svc_register, @function
__GI_svc_register:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%rdi, 8(%rsp)
	movq	%r8, (%rsp)
	call	__rpc_thread_variables
	movq	232(%rax), %r14
	movq	%rax, %r15
	testq	%r14, %r14
	je	.L27
	movq	%r14, %rbx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L27
.L30:
	cmpq	8(%rbx), %rbp
	jne	.L28
	cmpq	16(%rbx), %r12
	jne	.L28
	cmpq	%r13, 24(%rbx)
	je	.L31
.L34:
	xorl	%r13d, %r13d
.L26:
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$40, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L34
	movq	%rbp, 8(%rbx)
	movq	%r12, 16(%rbx)
	movq	%r13, 24(%rbx)
	movq	%r14, (%rbx)
	movl	$0, 32(%rbx)
	movq	%rbx, 232(%r15)
.L31:
	cmpq	$0, (%rsp)
	movl	$1, %r13d
	je	.L26
	movq	8(%rsp), %rax
	movl	(%rsp), %edx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movzwl	4(%rax), %ecx
	call	__GI_pmap_set
	testl	%eax, %eax
	je	.L34
	movl	$1, 32(%rbx)
	jmp	.L26
	.size	__GI_svc_register, .-__GI_svc_register
	.globl	__EI_svc_register
	.set	__EI_svc_register,__GI_svc_register
	.p2align 4,,15
	.globl	__GI_svc_unregister
	.hidden	__GI_svc_unregister
	.type	__GI_svc_unregister, @function
__GI_svc_unregister:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	call	__rpc_thread_variables
	movq	232(%rax), %rdi
	xorl	%ecx, %ecx
	testq	%rdi, %rdi
	jne	.L48
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%rdi), %rdx
	movq	%rdi, %rcx
	testq	%rdx, %rdx
	je	.L47
	movq	%rdx, %rdi
.L48:
	cmpq	8(%rdi), %rbx
	jne	.L50
	cmpq	16(%rdi), %rbp
	jne	.L50
	testq	%rcx, %rcx
	movl	32(%rdi), %r12d
	movq	(%rdi), %rdx
	je	.L65
	movq	%rdx, (%rcx)
.L53:
	call	free@PLT
	testl	%r12d, %r12d
	je	.L47
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__GI_pmap_unset
	.p2align 4,,10
	.p2align 3
.L47:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rdx, 232(%rax)
	jmp	.L53
	.size	__GI_svc_unregister, .-__GI_svc_unregister
	.globl	__EI_svc_unregister
	.set	__EI_svc_unregister,__GI_svc_unregister
	.p2align 4,,15
	.globl	__GI_svc_sendreply
	.hidden	__GI_svc_sendreply
	.type	__GI_svc_sendreply, @function
__GI_svc_sendreply:
	subq	$104, %rsp
	movq	56(%rdi), %rax
	movdqu	40(%rdi), %xmm0
	movq	%rsi, 64(%rsp)
	movl	$1, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rax, 40(%rsp)
	movq	8(%rdi), %rax
	movl	$0, 16(%rsp)
	movl	$0, 48(%rsp)
	movups	%xmm0, 24(%rsp)
	movq	%rdx, 56(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svc_sendreply, .-__GI_svc_sendreply
	.globl	__EI_svc_sendreply
	.set	__EI_svc_sendreply,__GI_svc_sendreply
	.p2align 4,,15
	.globl	__GI_svcerr_noproc
	.hidden	__GI_svcerr_noproc
	.type	__GI_svcerr_noproc, @function
__GI_svcerr_noproc:
	subq	$104, %rsp
	movq	56(%rdi), %rax
	movdqu	40(%rdi), %xmm0
	movq	%rsp, %rsi
	movl	$1, 8(%rsp)
	movl	$0, 16(%rsp)
	movq	%rax, 40(%rsp)
	movq	8(%rdi), %rax
	movups	%xmm0, 24(%rsp)
	movl	$3, 48(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_noproc, .-__GI_svcerr_noproc
	.globl	__EI_svcerr_noproc
	.set	__EI_svcerr_noproc,__GI_svcerr_noproc
	.p2align 4,,15
	.globl	__GI_svcerr_decode
	.hidden	__GI_svcerr_decode
	.type	__GI_svcerr_decode, @function
__GI_svcerr_decode:
	subq	$104, %rsp
	movq	56(%rdi), %rax
	movdqu	40(%rdi), %xmm0
	movq	%rsp, %rsi
	movl	$1, 8(%rsp)
	movl	$0, 16(%rsp)
	movq	%rax, 40(%rsp)
	movq	8(%rdi), %rax
	movups	%xmm0, 24(%rsp)
	movl	$4, 48(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_decode, .-__GI_svcerr_decode
	.globl	__EI_svcerr_decode
	.set	__EI_svcerr_decode,__GI_svcerr_decode
	.p2align 4,,15
	.globl	__GI_svcerr_systemerr
	.hidden	__GI_svcerr_systemerr
	.type	__GI_svcerr_systemerr, @function
__GI_svcerr_systemerr:
	subq	$104, %rsp
	movq	56(%rdi), %rax
	movdqu	40(%rdi), %xmm0
	movq	%rsp, %rsi
	movl	$1, 8(%rsp)
	movl	$0, 16(%rsp)
	movq	%rax, 40(%rsp)
	movq	8(%rdi), %rax
	movups	%xmm0, 24(%rsp)
	movl	$5, 48(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_systemerr, .-__GI_svcerr_systemerr
	.globl	__EI_svcerr_systemerr
	.set	__EI_svcerr_systemerr,__GI_svcerr_systemerr
	.p2align 4,,15
	.globl	__GI_svcerr_auth
	.hidden	__GI_svcerr_auth
	.type	__GI_svcerr_auth, @function
__GI_svcerr_auth:
	subq	$104, %rsp
	movq	8(%rdi), %rax
	movl	%esi, 32(%rsp)
	movl	$1, 8(%rsp)
	movq	%rsp, %rsi
	movl	$1, 16(%rsp)
	movl	$1, 24(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_auth, .-__GI_svcerr_auth
	.globl	__EI_svcerr_auth
	.set	__EI_svcerr_auth,__GI_svcerr_auth
	.p2align 4,,15
	.globl	__GI_svcerr_weakauth
	.hidden	__GI_svcerr_weakauth
	.type	__GI_svcerr_weakauth, @function
__GI_svcerr_weakauth:
	subq	$104, %rsp
	movq	8(%rdi), %rax
	movl	$1, 8(%rsp)
	movl	$1, 16(%rsp)
	movq	%rsp, %rsi
	movl	$1, 24(%rsp)
	movl	$5, 32(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_weakauth, .-__GI_svcerr_weakauth
	.globl	__EI_svcerr_weakauth
	.set	__EI_svcerr_weakauth,__GI_svcerr_weakauth
	.p2align 4,,15
	.globl	__GI_svcerr_noprog
	.hidden	__GI_svcerr_noprog
	.type	__GI_svcerr_noprog, @function
__GI_svcerr_noprog:
	subq	$104, %rsp
	movq	56(%rdi), %rax
	movdqu	40(%rdi), %xmm0
	movq	%rsp, %rsi
	movl	$1, 8(%rsp)
	movl	$0, 16(%rsp)
	movq	%rax, 40(%rsp)
	movq	8(%rdi), %rax
	movups	%xmm0, 24(%rsp)
	movl	$1, 48(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_noprog, .-__GI_svcerr_noprog
	.globl	__EI_svcerr_noprog
	.set	__EI_svcerr_noprog,__GI_svcerr_noprog
	.p2align 4,,15
	.globl	__GI_svcerr_progvers
	.hidden	__GI_svcerr_progvers
	.type	__GI_svcerr_progvers, @function
__GI_svcerr_progvers:
	subq	$104, %rsp
	movq	56(%rdi), %rax
	movdqu	40(%rdi), %xmm0
	movq	%rsi, 56(%rsp)
	movl	$1, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rax, 40(%rsp)
	movq	8(%rdi), %rax
	movl	$0, 16(%rsp)
	movl	$2, 48(%rsp)
	movups	%xmm0, 24(%rsp)
	movq	%rdx, 64(%rsp)
	call	*24(%rax)
	addq	$104, %rsp
	ret
	.size	__GI_svcerr_progvers, .-__GI_svcerr_progvers
	.globl	__EI_svcerr_progvers
	.set	__EI_svcerr_progvers,__GI_svcerr_progvers
	.p2align 4,,15
	.globl	__GI_svc_getreq_common
	.hidden	__GI_svc_getreq_common
	.type	__GI_svc_getreq_common, @function
__GI_svc_getreq_common:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movslq	%edi, %rbp
	subq	$1464, %rsp
	leaq	656(%rsp), %rax
	leaq	256(%rsp), %rbx
	movq	%rax, 144(%rsp)
	movq	%rbx, 120(%rsp)
	call	__rpc_thread_variables
	movq	%rax, %r12
	movq	224(%rax), %rax
	movq	(%rax,%rbp,8), %r15
	testq	%r15, %r15
	je	.L82
	leaq	64(%rsp), %rbp
	leaq	160(%rsp), %r13
	addq	$800, %rbx
	movq	%rsp, %r14
	.p2align 4,,10
	.p2align 3
.L94:
	movq	8(%r15), %rax
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	*(%rax)
	testl	%eax, %eax
	je	.L84
	movq	88(%rsp), %rax
	movq	%rbx, 48(%rsp)
	movdqa	112(%rsp), %xmm0
	movq	%r15, 56(%rsp)
	movq	%rax, (%rsp)
	movq	96(%rsp), %rax
	movups	%xmm0, 24(%rsp)
	movq	%rax, 8(%rsp)
	movq	104(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	128(%rsp), %rax
	movq	%rax, 40(%rsp)
	movl	112(%rsp), %eax
	testl	%eax, %eax
	jne	.L85
	movl	__GI__null_auth(%rip), %eax
	movl	$0, 56(%r15)
	movl	%eax, 40(%r15)
.L86:
	movq	232(%r12), %rax
	testq	%rax, %rax
	je	.L88
	movq	(%rsp), %rcx
	movq	8(%rsp), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	$-1, %rdi
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L111
.L91:
	cmpq	%rcx, 8(%rax)
	jne	.L89
	movq	16(%rax), %rdx
	cmpq	%r8, %rdx
	je	.L112
	movq	(%rax), %rax
	cmpq	%rdx, %rdi
	cmova	%rdx, %rdi
	cmpq	%rdx, %rsi
	cmovb	%rdx, %rsi
	movl	$1, %edx
	testq	%rax, %rax
	jne	.L91
.L111:
	movq	8(%r15), %rax
	testl	%edx, %edx
	movq	24(%rax), %rax
	je	.L92
	movq	56(%r15), %rdx
	movl	$1, 168(%rsp)
	movdqu	40(%r15), %xmm0
	movl	$0, 176(%rsp)
	movq	%rdi, 216(%rsp)
	movq	%r15, %rdi
	movq	%rsi, 224(%rsp)
	movq	%rdx, 200(%rsp)
	movq	%r13, %rsi
	movups	%xmm0, 184(%rsp)
	movl	$2, 208(%rsp)
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r15, %rdi
	movq	8(%r15), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L113
.L93:
	cmpl	$1, %eax
	je	.L94
.L82:
	addq	$1464, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	__GI__authenticate
	testl	%eax, %eax
	je	.L86
	movl	%eax, 192(%rsp)
	movq	8(%r15), %rax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	$1, 168(%rsp)
	movl	$1, 176(%rsp)
	movl	$1, 184(%rsp)
	call	*24(%rax)
	movq	8(%r15), %rax
	movq	%r15, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	jne	.L93
.L113:
	movq	8(%r15), %rax
	movq	%r15, %rdi
	call	*40(%rax)
	jmp	.L82
.L88:
	movq	8(%r15), %rax
	movq	24(%rax), %rax
.L92:
	movq	56(%r15), %rdx
	movl	$1, 168(%rsp)
	movq	%r13, %rsi
	movdqu	40(%r15), %xmm0
	movq	%r15, %rdi
	movl	$0, 176(%rsp)
	movl	$1, 208(%rsp)
	movups	%xmm0, 184(%rsp)
	movq	%rdx, 200(%rsp)
	call	*%rax
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*24(%rax)
	jmp	.L84
	.size	__GI_svc_getreq_common, .-__GI_svc_getreq_common
	.globl	__EI_svc_getreq_common
	.set	__EI_svc_getreq_common,__GI_svc_getreq_common
	.p2align 4,,15
	.globl	__GI_svc_getreqset
	.hidden	__GI_svc_getreqset
	.type	__GI_svc_getreqset, @function
__GI_svc_getreqset:
	pushq	%r15
	pushq	%r14
	movl	$1024, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	call	__GI__rpc_dtablesize
	cmpl	$1024, %eax
	cmovle	%eax, %r14d
	testl	%eax, %eax
	jle	.L114
	xorl	%ebp, %ebp
	movq	$-1, %r12
	.p2align 4,,10
	.p2align 3
.L118:
	addq	$8, %r13
	movq	-8(%r13), %r15
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	leal	-1(%rax,%rbp), %edi
	call	__GI_svc_getreq_common
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	xorq	%rax, %r15
.L122:
	bsfq	%r15, %rax
	cmove	%r12, %rax
	addq	$1, %rax
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L123
	addl	$64, %ebp
	cmpl	%r14d, %ebp
	jl	.L118
.L114:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI_svc_getreqset, .-__GI_svc_getreqset
	.globl	__EI_svc_getreqset
	.set	__EI_svc_getreqset,__GI_svc_getreqset
	.p2align 4,,15
	.globl	__GI_svc_getreq
	.hidden	__GI_svc_getreq
	.type	__GI_svc_getreq, @function
__GI_svc_getreq:
	subq	$136, %rsp
	movl	%edi, %ecx
	leaq	128(%rsp), %rdx
	movq	%rsp, %rdi
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L125:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L125
	movslq	%ecx, %rax
	movq	%rax, (%rsp)
	call	__GI_svc_getreqset
	addq	$136, %rsp
	ret
	.size	__GI_svc_getreq, .-__GI_svc_getreq
	.globl	__EI_svc_getreq
	.set	__EI_svc_getreq,__GI_svc_getreq
	.p2align 4,,15
	.globl	__GI_svc_getreq_poll
	.hidden	__GI_svc_getreq_poll
	.type	__GI_svc_getreq_poll, @function
__GI_svc_getreq_poll:
	testl	%esi, %esi
	je	.L143
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	subq	$8, %rsp
	call	__GI___rpc_thread_svc_max_pollfd
	movq	%rax, %r14
	movl	(%rax), %edx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L146:
	call	__rpc_thread_variables
	movq	224(%rax), %rax
	addl	$1, %r15d
	movq	(%rax,%rbp,8), %rdi
	call	__GI_xprt_unregister
	cmpl	%r15d, %r13d
	jle	.L128
.L147:
	movl	(%r14), %edx
.L131:
	addq	$1, %rbx
.L130:
	cmpl	%ebx, %edx
	jle	.L128
	movslq	(%r12,%rbx,8), %rbp
	cmpl	$-1, %ebp
	je	.L131
	movzwl	6(%r12,%rbx,8), %eax
	testw	%ax, %ax
	je	.L131
	testb	$32, %al
	jne	.L146
	movl	%ebp, %edi
	addl	$1, %r15d
	call	__GI_svc_getreq_common
	cmpl	%r15d, %r13d
	jg	.L147
.L128:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	rep ret
	.size	__GI_svc_getreq_poll, .-__GI_svc_getreq_poll
	.globl	__EI_svc_getreq_poll
	.set	__EI_svc_getreq_poll,__GI_svc_getreq_poll
	.p2align 4,,15
	.globl	__svc_wait_on_error
	.hidden	__svc_wait_on_error
	.type	__svc_wait_on_error, @function
__svc_wait_on_error:
	subq	$24, %rsp
	xorl	%esi, %esi
	movq	%rsp, %rdi
	movq	$0, (%rsp)
	movq	$50000000, 8(%rsp)
	call	__GI___nanosleep
	addq	$24, %rsp
	ret
	.size	__svc_wait_on_error, .-__svc_wait_on_error
	.p2align 4,,15
	.globl	__svc_accept_failed
	.hidden	__svc_accept_failed
	.type	__svc_accept_failed, @function
__svc_accept_failed:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$24, %fs:(%rax)
	je	.L156
	rep ret
	.p2align 4,,10
	.p2align 3
.L156:
	subq	$24, %rsp
	xorl	%esi, %esi
	movq	%rsp, %rdi
	movq	$0, (%rsp)
	movq	$50000000, 8(%rsp)
	call	__GI___nanosleep
	addq	$24, %rsp
	ret
	.size	__svc_accept_failed, .-__svc_accept_failed
	.p2align 4,,15
	.globl	__rpc_thread_svc_cleanup
	.hidden	__rpc_thread_svc_cleanup
	.type	__rpc_thread_svc_cleanup, @function
__rpc_thread_svc_cleanup:
	pushq	%rbx
	call	__rpc_thread_variables
	movq	%rax, %rbx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L159:
	movq	16(%rax), %rsi
	movq	8(%rax), %rdi
	call	__GI_svc_unregister
.L158:
	movq	232(%rbx), %rax
	testq	%rax, %rax
	jne	.L159
	popq	%rbx
	ret
	.size	__rpc_thread_svc_cleanup, .-__rpc_thread_svc_cleanup
	.hidden	__rpc_thread_variables
