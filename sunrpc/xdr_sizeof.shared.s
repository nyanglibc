	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_sizeof, xdr_sizeof@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	x_putlong, @function
x_putlong:
	addl	$4, 40(%rdi)
	movl	$1, %eax
	ret
	.size	x_putlong, .-x_putlong
	.p2align 4,,15
	.type	x_putbytes, @function
x_putbytes:
	addl	%edx, 40(%rdi)
	movl	$1, %eax
	ret
	.size	x_putbytes, .-x_putbytes
	.p2align 4,,15
	.type	x_getpostn, @function
x_getpostn:
	movl	40(%rdi), %eax
	ret
	.size	x_getpostn, .-x_getpostn
	.p2align 4,,15
	.type	x_setpostn, @function
x_setpostn:
	xorl	%eax, %eax
	ret
	.size	x_setpostn, .-x_setpostn
	.p2align 4,,15
	.type	harmless, @function
harmless:
	xorl	%eax, %eax
	ret
	.size	harmless, .-harmless
	.p2align 4,,15
	.type	x_destroy, @function
x_destroy:
	movq	24(%rdi), %rax
	movl	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	testq	%rax, %rax
	je	.L13
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rax, %rdi
	call	free@PLT
	movq	$0, 24(%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	rep ret
	.size	x_destroy, .-x_destroy
	.p2align 4,,15
	.type	x_inline, @function
x_inline:
	testl	%esi, %esi
	je	.L21
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L21
	cmpl	%esi, 32(%rdi)
	movq	24(%rdi), %rax
	jbe	.L18
	addl	%esi, 40(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rax, %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rbx)
	je	.L25
	addl	%r12d, 40(%rbx)
	movq	%r12, 32(%rbx)
.L16:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	$0, 32(%rbx)
	jmp	.L16
	.size	x_inline, .-x_inline
	.p2align 4,,15
	.type	x_putint32, @function
x_putint32:
	addl	$4, 40(%rdi)
	movl	$1, %eax
	ret
	.size	x_putint32, .-x_putint32
	.p2align 4,,15
	.globl	__GI_xdr_sizeof
	.hidden	__GI_xdr_sizeof
	.type	__GI_xdr_sizeof, @function
__GI_xdr_sizeof:
	pushq	%rbx
	leaq	x_putlong(%rip), %rax
	movq	%rdi, %rdx
	addq	$-128, %rsp
	movq	%rax, 56(%rsp)
	leaq	x_putbytes(%rip), %rax
	movq	%rsp, %rdi
	movl	$0, (%rsp)
	movl	$0, 40(%rsp)
	movq	%rax, 72(%rsp)
	leaq	x_inline(%rip), %rax
	movq	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	%rax, 96(%rsp)
	leaq	x_getpostn(%rip), %rax
	movq	%rax, 80(%rsp)
	leaq	x_setpostn(%rip), %rax
	movq	%rax, 88(%rsp)
	leaq	x_destroy(%rip), %rax
	movq	%rax, 104(%rsp)
	leaq	x_putint32(%rip), %rax
	movq	%rax, 120(%rsp)
	leaq	harmless(%rip), %rax
	movq	%rax, 48(%rsp)
	movq	%rax, 64(%rsp)
	movq	%rax, 112(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 8(%rsp)
	xorl	%eax, %eax
	call	*%rdx
	movq	24(%rsp), %rdi
	movl	%eax, %ebx
	call	free@PLT
	xorl	%eax, %eax
	cmpl	$1, %ebx
	jne	.L27
	movl	40(%rsp), %eax
.L27:
	subq	$-128, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_sizeof, .-__GI_xdr_sizeof
	.globl	__EI_xdr_sizeof
	.set	__EI_xdr_sizeof,__GI_xdr_sizeof
