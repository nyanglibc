	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_svcauth_unix
	.type	_svcauth_unix, @function
_svcauth_unix:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movl	$1, %ecx
	pushq	%rbx
	movq	%rdi, %r13
	subq	$48, %rsp
	movq	48(%rdi), %rbx
	movl	64(%rsi), %r12d
	movq	56(%rsi), %rsi
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	leaq	40(%rbx), %rax
	movl	%r12d, %edx
	movq	%rax, 8(%rbx)
	leaq	296(%rbx), %rax
	movq	%rax, 32(%rbx)
	call	__GI_xdrmem_create
	movq	8(%rsp), %rax
	movl	%r12d, %esi
	movq	%rbp, %rdi
	call	*48(%rax)
	testq	%rax, %rax
	je	.L2
	movl	(%rax), %edx
	bswap	%edx
	movl	%edx, %edi
	movl	4(%rax), %edx
	movq	%rdi, (%rbx)
	bswap	%edx
	cmpl	$255, %edx
	jbe	.L3
.L30:
	movl	$1, %ebx
.L4:
	movq	8(%rsp), %rdx
	movq	56(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L1
	movq	%rbp, %rdi
	call	*%rdx
.L1:
	addq	$48, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$8, %edx
	leaq	8(%rax), %r8
	movl	%edx, %r9d
	movq	8(%rbx), %rcx
	jnb	.L5
	testb	$4, %dl
	jne	.L32
	testl	%edx, %edx
	je	.L6
	movzbl	8(%rax), %eax
	testb	$2, %dl
	movb	%al, (%rcx)
	jne	.L27
.L29:
	movq	8(%rbx), %rcx
.L6:
	addl	$3, %edx
	movb	$0, (%rcx,%r9)
	andl	$-4, %edx
	movl	%edx, %eax
	addq	%r8, %rax
	movl	(%rax), %ecx
	bswap	%ecx
	movl	%ecx, 16(%rbx)
	movl	4(%rax), %ecx
	bswap	%ecx
	movl	%ecx, 20(%rbx)
	movl	8(%rax), %edi
	bswap	%edi
	cmpl	$16, %edi
	ja	.L30
	testl	%edi, %edi
	movl	%edi, 24(%rbx)
	je	.L10
	leaq	12(%rax), %rsi
	movq	32(%rbx), %r9
	leal	-1(%rdi), %eax
	leaq	4(,%rax,4), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L11:
	movl	(%rsi,%rax), %ecx
	bswap	%ecx
	movl	%ecx, (%r9,%rax)
	addq	$4, %rax
	cmpq	%rax, %r8
	jne	.L11
.L10:
	leal	20(%rdx,%rdi,4), %eax
	cmpl	%r12d, %eax
	ja	.L30
.L12:
	movl	88(%r14), %edx
	movq	56(%r13), %rax
	testl	%edx, %edx
	je	.L13
	movl	72(%r14), %ecx
	xorl	%ebx, %ebx
	movl	%ecx, 40(%rax)
	movq	80(%r14), %rcx
	movl	%edx, 56(%rax)
	movq	%rcx, 48(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rax), %rax
	leaq	8(%rcx), %rdi
	movq	%r8, %rsi
	andq	$-8, %rdi
	movq	%rax, (%rcx)
	movq	-8(%r8,%r9), %rax
	movq	%rax, -8(%rcx,%r9)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%edx, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_authunix_parms
	testl	%eax, %eax
	jne	.L12
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movl	$2, (%rsp)
	call	__GI_xdr_authunix_parms
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$0, 40(%rax)
	movl	$0, 56(%rax)
	xorl	%ebx, %ebx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L32:
	movl	8(%rax), %eax
	movl	%eax, (%rcx)
	movl	-4(%r8,%r9), %eax
	movl	%eax, -4(%rcx,%r9)
	movq	8(%rbx), %rcx
	jmp	.L6
.L27:
	movzwl	-2(%r8,%r9), %eax
	movw	%ax, -2(%rcx,%r9)
	movq	8(%rbx), %rcx
	jmp	.L6
	.size	_svcauth_unix, .-_svcauth_unix
	.p2align 4,,15
	.globl	_svcauth_short
	.type	_svcauth_short, @function
_svcauth_short:
	movl	$2, %eax
	ret
	.size	_svcauth_short, .-_svcauth_short
