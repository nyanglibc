	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_svcunix_create, svcunix_create@GLIBC_2.2.5
	.symver __EI_svcunixfd_create, svcunixfd_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	rendezvous_stat, @function
rendezvous_stat:
	movl	$2, %eax
	ret
	.size	rendezvous_stat, .-rendezvous_stat
	.p2align 4,,15
	.type	svcunix_getargs, @function
svcunix_getargs:
	movq	64(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	addq	$16, %rdi
	jmp	*%rcx
	.size	svcunix_getargs, .-svcunix_getargs
	.p2align 4,,15
	.type	svcunix_freeargs, @function
svcunix_freeargs:
	movq	64(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	$2, 16(%rdi)
	addq	$16, %rdi
	jmp	*%rcx
	.size	svcunix_freeargs, .-svcunix_freeargs
	.p2align 4,,15
	.type	svcunix_destroy, @function
svcunix_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rbp
	call	__GI_xprt_unregister
	movl	(%rbx), %edi
	call	__GI___close
	cmpw	$0, 4(%rbx)
	jne	.L6
	movq	24(%rbp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L6
	leaq	16(%rbp), %rdi
	call	*%rax
.L6:
	movq	%rbp, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.size	svcunix_destroy, .-svcunix_destroy
	.p2align 4,,15
	.type	svcunix_rendezvous_abort, @function
svcunix_rendezvous_abort:
	subq	$8, %rsp
	call	__GI_abort
	.size	svcunix_rendezvous_abort, .-svcunix_rendezvous_abort
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"svc_unix: makefd_xprt"
.LC2:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.type	makefd_xprt, @function
makefd_xprt:
	pushq	%r14
	pushq	%r13
	movl	%edx, %r14d
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	movl	$336, %edi
	movl	%esi, %r13d
	call	malloc@PLT
	movl	$464, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rbx, %rbx
	movq	%rax, %rbp
	je	.L17
	testq	%rax, %rax
	je	.L17
	leaq	16(%rax), %rdi
	leaq	writeunix(%rip), %r9
	leaq	readunix(%rip), %r8
	movl	$2, (%rax)
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	__GI_xdrrec_create
	leaq	svcunix_op(%rip), %rax
	movq	%rbp, 64(%rbx)
	addq	$64, %rbp
	movq	$0, 72(%rbx)
	movq	%rbp, 48(%rbx)
	movq	%rbx, %rdi
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movl	$0, 16(%rbx)
	movw	%ax, 4(%rbx)
	movl	%r12d, (%rbx)
	call	__GI_xprt_register
.L13:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L13
	.size	makefd_xprt, .-makefd_xprt
	.p2align 4,,15
	.type	rendezvous_request, @function
rendezvous_request:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$152, %rsp
	movq	64(%rdi), %r13
	leaq	32(%rsp), %r12
	leaq	12(%rsp), %rbp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L26
.L20:
	movl	(%rbx), %edi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movl	$110, 12(%rsp)
	call	__GI_accept
	testl	%eax, %eax
	js	.L27
	xorl	%edx, %edx
	movl	0(%r13), %esi
	movl	$1, %ecx
	movw	%dx, 30(%rsp)
	movl	4(%r13), %edx
	movl	%eax, %edi
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	movw	%cx, 16(%rsp)
	call	makefd_xprt
	testq	%rax, %rax
	je	.L28
	movdqa	16(%rsp), %xmm0
	movl	12(%rsp), %edx
	movups	%xmm0, 20(%rax)
	movl	%edx, 16(%rax)
.L22:
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	call	__svc_accept_failed
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	call	__svc_wait_on_error
	jmp	.L22
	.size	rendezvous_request, .-rendezvous_request
	.p2align 4,,15
	.type	svcunix_reply, @function
svcunix_reply:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	64(%rdi), %rax
	leaq	16(%rax), %rbx
	movl	$0, 16(%rax)
	movq	8(%rax), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rsi)
	call	__GI_xdr_replymsg
	movq	%rbx, %rdi
	movl	$1, %esi
	movl	%eax, %ebp
	call	__GI_xdrrec_endofrecord
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	svcunix_reply, .-svcunix_reply
	.p2align 4,,15
	.type	svcunix_stat, @function
svcunix_stat:
	movq	64(%rdi), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L41
	rep ret
	.p2align 4,,10
	.p2align 3
.L41:
	subq	$8, %rsp
	addq	$16, %rdi
	call	__GI_xdrrec_eof
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	addl	$1, %eax
	ret
	.size	svcunix_stat, .-svcunix_stat
	.p2align 4,,15
	.type	svcunix_recv, @function
svcunix_recv:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	64(%rdi), %rbx
	leaq	16(%rbx), %r12
	movl	$1, 16(%rbx)
	movq	%r12, %rdi
	call	__GI_xdrrec_skiprecord
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__GI_xdr_callmsg
	testl	%eax, %eax
	jne	.L46
	movl	$0, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	0(%rbp), %rax
	movq	%rax, 8(%rbx)
	leaq	cm(%rip), %rax
	movl	$1, 72(%rbp)
	movl	$40, 88(%rbp)
	movq	%rax, 80(%rbp)
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	svcunix_recv, .-svcunix_recv
	.p2align 4,,15
	.type	writeunix, @function
writeunix:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	testl	%edx, %edx
	movq	%rdi, 8(%rsp)
	movl	%edx, 28(%rsp)
	jle	.L48
	leaq	32(%rsp), %rax
	leaq	48(%rsp), %rbp
	movq	%rsi, %r13
	movl	%edx, %r12d
	movq	%rax, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L53:
	movq	8(%rsp), %rax
	movl	(%rax), %ebx
	call	__GI___getpid
	movl	%eax, %r14d
	call	__geteuid
	movl	%eax, %r15d
	call	__getegid
	movl	%eax, 24+cm(%rip)
	movabsq	$8589934593, %rax
	movl	%r14d, 16+cm(%rip)
	movq	%rax, 8+cm(%rip)
	movslq	%r12d, %rax
	movl	%r15d, 20+cm(%rip)
	movq	%rax, 40(%rsp)
	movq	16(%rsp), %rax
	movq	$28, cm(%rip)
	movq	%r13, 32(%rsp)
	movq	$1, 72(%rsp)
	movq	$0, 48(%rsp)
	movq	%rax, 64(%rsp)
	leaq	cm(%rip), %rax
	movl	$0, 56(%rsp)
	movq	$32, 88(%rsp)
	movl	$0, 96(%rsp)
	movq	%rax, 80(%rsp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L59:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L58
.L49:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	call	__sendmsg
	testl	%eax, %eax
	movslq	%eax, %rdx
	js	.L59
	subl	%edx, %r12d
	addq	%rdx, %r13
	testl	%r12d, %r12d
	jg	.L53
.L48:
	movl	28(%rsp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rsp), %rax
	movq	64(%rax), %rax
	movl	$0, (%rax)
	addq	$120, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	writeunix, .-writeunix
	.p2align 4,,15
	.type	readunix, @function
readunix:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movslq	%edx, %r13
	subq	$96, %rsp
	movl	(%rdi), %ebx
	leaq	8(%rsp), %rbp
.L65:
	movl	$1, %eax
	movl	$35000, %edx
	movl	$1, %esi
	movq	%rbp, %rdi
	movl	%ebx, 8(%rsp)
	movw	%ax, 12(%rsp)
	call	__GI___poll
	cmpl	$-1, %eax
	je	.L62
	testl	%eax, %eax
	je	.L63
	movzwl	14(%rsp), %eax
	testb	$56, %al
	jne	.L63
.L64:
	testb	$1, %al
	je	.L65
	leaq	16(%rsp), %rax
	leaq	4(%rsp), %rcx
	movl	$4, %r8d
	movl	$16, %edx
	movl	$1, %esi
	movl	%ebx, %edi
	movq	%rax, 48(%rsp)
	leaq	cm(%rip), %rax
	movq	%r14, 16(%rsp)
	movq	%r13, 24(%rsp)
	movq	$1, 56(%rsp)
	movq	$0, 32(%rsp)
	movl	$0, 40(%rsp)
	movq	%rax, 64(%rsp)
	movq	$40, 72(%rsp)
	movl	$0, 80(%rsp)
	movl	$1, 4(%rsp)
	call	__setsockopt
	testl	%eax, %eax
	jne	.L63
	leaq	32(%rsp), %rbp
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L63
.L67:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	call	__recvmsg
	testl	%eax, %eax
	js	.L68
	testb	$8, 80(%rsp)
	jne	.L63
	testl	%eax, %eax
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	movq	64(%r12), %rax
	movl	$0, (%rax)
	movl	$-1, %eax
.L60:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L63
	movzwl	14(%rsp), %eax
	jmp	.L64
	.size	readunix, .-readunix
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"svc_unix.c - AF_UNIX socket creation problem"
	.align 8
.LC4:
	.string	"svc_unix.c - cannot getsockname or listen"
	.text
	.p2align 4,,15
	.globl	__GI_svcunix_create
	.hidden	__GI_svcunix_create
	.type	__GI_svcunix_create, @function
__GI_svcunix_create:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	xorl	%r12d, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movl	%edi, %ebx
	subq	$152, %rsp
	cmpl	$-1, %edi
	movl	%esi, 8(%rsp)
	movl	%edx, 12(%rsp)
	movl	$16, 28(%rsp)
	je	.L99
.L84:
	leaq	34(%rsp), %r14
	leaq	40(%rsp), %rdi
	xorl	%eax, %eax
	movq	$0, 34(%rsp)
	movq	$0, 134(%rsp)
	movl	$1, %edx
	movq	%r14, %rcx
	leaq	32(%rsp), %r15
	subq	%rdi, %rcx
	addl	$108, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rbp, %rdi
	movw	%dx, 32(%rsp)
	call	__GI_strlen
	leal	1(%rax), %edx
	movq	%rax, %r13
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	__GI_memcpy@PLT
	leal	3(%r13), %edx
	movq	%r15, %rsi
	movl	%ebx, %edi
	movl	%edx, 28(%rsp)
	call	__bind
	leaq	28(%rsp), %rdx
	movq	%r15, %rsi
	movl	%ebx, %edi
	call	__getsockname
	testl	%eax, %eax
	je	.L86
.L88:
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	testl	%r12d, %r12d
	jne	.L87
.L98:
	xorl	%ebp, %ebp
.L83:
	addq	$152, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$4096, %esi
	movl	%ebx, %edi
	call	__listen
	testl	%eax, %eax
	jne	.L88
	movl	$8, %edi
	call	malloc@PLT
	movl	$336, %edi
	movq	%rax, %r12
	call	malloc@PLT
	testq	%r12, %r12
	movq	%rax, %rbp
	je	.L94
	testq	%rax, %rax
	je	.L94
	movl	8(%rsp), %eax
	movq	$0, 72(%rbp)
	movq	%rbp, %rdi
	movdqu	__GI__null_auth(%rip), %xmm0
	movq	%r12, 64(%rbp)
	movl	%ebx, 0(%rbp)
	movl	%eax, (%r12)
	movl	12(%rsp), %eax
	movups	%xmm0, 40(%rbp)
	movl	%eax, 4(%r12)
	movq	16+__GI__null_auth(%rip), %rax
	movq	%rax, 56(%rbp)
	leaq	svcunix_rendezvous_op(%rip), %rax
	movq	%rax, 8(%rbp)
	movl	$-1, %eax
	movw	%ax, 4(%rbp)
	call	__GI_xprt_register
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movl	%ebx, %edi
	call	__GI___close
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	xorl	%edx, %edx
	movl	$1, %esi
	movl	$1, %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %ebx
	movl	$1, %r12d
	jns	.L84
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	xorl	%ebp, %ebp
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L94:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.11185(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%r12, %rdi
	call	free@PLT
	movq	%rbp, %rdi
	xorl	%ebp, %ebp
	call	free@PLT
	jmp	.L83
	.size	__GI_svcunix_create, .-__GI_svcunix_create
	.globl	__EI_svcunix_create
	.set	__EI_svcunix_create,__GI_svcunix_create
	.p2align 4,,15
	.globl	__GI_svcunixfd_create
	.hidden	__GI_svcunixfd_create
	.type	__GI_svcunixfd_create, @function
__GI_svcunixfd_create:
	jmp	makefd_xprt
	.size	__GI_svcunixfd_create, .-__GI_svcunixfd_create
	.globl	__EI_svcunixfd_create
	.set	__EI_svcunixfd_create,__GI_svcunixfd_create
	.section	.rodata.str1.8
	.align 8
	.type	__func__.11185, @object
	.size	__func__.11185, 15
__func__.11185:
	.string	"svcunix_create"
	.local	cm
	.comm	cm,40,32
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	svcunix_rendezvous_op, @object
	.size	svcunix_rendezvous_op, 48
svcunix_rendezvous_op:
	.quad	rendezvous_request
	.quad	rendezvous_stat
	.quad	svcunix_rendezvous_abort
	.quad	svcunix_rendezvous_abort
	.quad	svcunix_rendezvous_abort
	.quad	svcunix_destroy
	.align 32
	.type	svcunix_op, @object
	.size	svcunix_op, 48
svcunix_op:
	.quad	svcunix_recv
	.quad	svcunix_stat
	.quad	svcunix_getargs
	.quad	svcunix_reply
	.quad	svcunix_freeargs
	.quad	svcunix_destroy
	.hidden	__listen
	.hidden	__getsockname
	.hidden	__bind
	.hidden	__recvmsg
	.hidden	__setsockopt
	.hidden	__sendmsg
	.hidden	__getegid
	.hidden	__geteuid
	.hidden	__svc_wait_on_error
	.hidden	__svc_accept_failed
	.hidden	__fxprintf
