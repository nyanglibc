	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_callmsg, xdr_callmsg@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_callmsg
	.hidden	__GI_xdr_callmsg
	.type	__GI_xdr_callmsg, @function
__GI_xdr_callmsg:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	(%rdi), %eax
	movq	%rsi, %rbx
	testl	%eax, %eax
	jne	.L2
	movl	64(%rsi), %eax
	cmpl	$400, %eax
	ja	.L5
	movl	88(%rsi), %edx
	cmpl	$400, %edx
	ja	.L5
	addl	$3, %eax
	addl	$3, %edx
	andl	$-4, %eax
	andl	$-4, %edx
	leal	40(%rax,%rdx), %esi
	movq	8(%rdi), %rax
	call	*48(%rax)
	testq	%rax, %rax
	je	.L62
	movq	(%rbx), %rdx
	bswap	%edx
	movl	%edx, (%rax)
	movl	8(%rbx), %edx
	movl	%edx, %ecx
	testl	%edx, %edx
	bswap	%ecx
	movl	%ecx, 4(%rax)
	jne	.L5
	movq	16(%rbx), %rdx
	bswap	%edx
	movl	%edx, 8(%rax)
	cmpq	$2, 16(%rbx)
	je	.L63
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movl	0(%rbp), %eax
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1, %eax
	jne	.L9
	movq	8(%rbp), %rax
	movl	$32, %esi
	movq	%rbp, %rdi
	call	*48(%rax)
	testq	%rax, %rax
	je	.L9
	movl	(%rax), %edx
	bswap	%edx
	movl	%edx, %ecx
	movl	4(%rax), %edx
	movq	%rcx, (%rbx)
	bswap	%edx
	testl	%edx, %edx
	movl	%edx, 8(%rbx)
	jne	.L5
	movl	8(%rax), %edx
	bswap	%edx
	movl	%edx, %ecx
	cmpl	$2, %edx
	movq	%rcx, 16(%rbx)
	jne	.L5
	movl	12(%rax), %edx
	bswap	%edx
	movl	%edx, %ecx
	movq	%rcx, 24(%rbx)
	movl	16(%rax), %edx
	bswap	%edx
	movl	%edx, %ecx
	movq	%rcx, 32(%rbx)
	movl	20(%rax), %edx
	bswap	%edx
	movl	%edx, %edi
	movq	%rdi, 40(%rbx)
	movl	24(%rax), %edx
	bswap	%edx
	movl	%edx, 48(%rbx)
	movl	28(%rax), %r12d
	bswap	%r12d
	testl	%r12d, %r12d
	movl	%r12d, 64(%rbx)
	jne	.L64
.L11:
	movq	8(%rbp), %rax
	movl	$8, %esi
	movq	%rbp, %rdi
	call	*48(%rax)
	testq	%rax, %rax
	je	.L65
	movl	(%rax), %edx
	bswap	%edx
	movl	%edx, 72(%rbx)
	movl	4(%rax), %r12d
	bswap	%r12d
	movl	%r12d, 88(%rbx)
.L16:
	testl	%r12d, %r12d
	je	.L60
	cmpl	$400, %r12d
	ja	.L5
	cmpq	$0, 80(%rbx)
	je	.L66
.L18:
	movq	8(%rbp), %rax
	leal	3(%r12), %esi
	movq	%rbp, %rdi
	andl	$-4, %esi
	call	*48(%rax)
	testq	%rax, %rax
	je	.L67
	movl	88(%rbx), %edx
	movq	80(%rbx), %rdi
	movq	%rax, %rsi
	call	__GI_memcpy@PLT
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L5
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L5
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L5
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L5
	cmpq	$2, 16(%rbx)
	jne	.L5
	leaq	24(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L5
	leaq	32(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L5
	leaq	40(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L5
	leaq	48(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_opaque_auth
	testl	%eax, %eax
	je	.L5
	leaq	72(%rbx), %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__GI_xdr_opaque_auth
	.p2align 4,,10
	.p2align 3
.L63:
	movq	24(%rbx), %rdx
	leaq	32(%rax), %rcx
	bswap	%edx
	movl	%edx, 12(%rax)
	movq	32(%rbx), %rdx
	bswap	%edx
	movl	%edx, 16(%rax)
	movq	40(%rbx), %rdx
	bswap	%edx
	movl	%edx, 20(%rax)
	movl	48(%rbx), %edx
	bswap	%edx
	movl	%edx, 24(%rax)
	movl	64(%rbx), %edx
	bswap	%edx
	movl	%edx, 28(%rax)
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jne	.L68
.L7:
	movl	72(%rbx), %eax
	bswap	%eax
	movl	%eax, (%rcx)
	movl	88(%rbx), %eax
	bswap	%eax
	movl	%eax, 4(%rcx)
	movl	88(%rbx), %eax
	testl	%eax, %eax
	je	.L60
	movq	80(%rbx), %rsi
	leaq	8(%rcx), %rdi
	movl	%eax, %edx
	call	__GI_memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	72(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L5
	leaq	88(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L5
	movl	88(%rbx), %r12d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L68:
	movq	56(%rbx), %rsi
	movq	%rcx, %rdi
	movl	%eax, %edx
	call	__GI_memcpy@PLT
	movq	%rax, %rcx
	movl	64(%rbx), %eax
	addl	$3, %eax
	andl	$-4, %eax
	addq	%rax, %rcx
	jmp	.L7
.L64:
	cmpl	$400, %r12d
	ja	.L5
	cmpq	$0, 56(%rbx)
	je	.L69
.L12:
	movq	8(%rbp), %rax
	leal	3(%r12), %esi
	movq	%rbp, %rdi
	andl	$-4, %esi
	call	*48(%rax)
	testq	%rax, %rax
	je	.L70
	movl	64(%rbx), %edx
	movq	56(%rbx), %rdi
	movq	%rax, %rsi
	call	__GI_memcpy@PLT
	jmp	.L11
.L67:
	movl	88(%rbx), %edx
	movq	80(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_opaque
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1
.L66:
	movl	%r12d, %edi
	call	malloc@PLT
	movq	%rax, 80(%rbx)
	jmp	.L18
.L70:
	movl	64(%rbx), %edx
	movq	56(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_opaque
	testl	%eax, %eax
	jne	.L11
	jmp	.L5
.L69:
	movl	%r12d, %edi
	call	malloc@PLT
	movq	%rax, 56(%rbx)
	jmp	.L12
	.size	__GI_xdr_callmsg, .-__GI_xdr_callmsg
	.globl	__EI_xdr_callmsg
	.set	__EI_xdr_callmsg,__GI_xdr_callmsg
