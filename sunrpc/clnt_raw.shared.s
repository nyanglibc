	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_clntraw_create, clntraw_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	clntraw_geterr, @function
clntraw_geterr:
	rep ret
	.size	clntraw_geterr, .-clntraw_geterr
	.p2align 4,,15
	.type	clntraw_abort, @function
clntraw_abort:
	rep ret
	.size	clntraw_abort, .-clntraw_abort
	.p2align 4,,15
	.type	clntraw_control, @function
clntraw_control:
	xorl	%eax, %eax
	ret
	.size	clntraw_control, .-clntraw_control
	.p2align 4,,15
	.type	clntraw_destroy, @function
clntraw_destroy:
	rep ret
	.size	clntraw_destroy, .-clntraw_destroy
	.p2align 4,,15
	.type	clntraw_freeres, @function
clntraw_freeres:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__rpc_thread_variables
	movq	184(%rax), %rax
	testq	%rax, %rax
	je	.L7
	movl	$2, 24(%rax)
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rcx
	leaq	24(%rax), %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rsp
	movl	$16, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	clntraw_freeres, .-clntraw_freeres
	.p2align 4,,15
	.type	clntraw_call, @function
clntraw_call:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	movq	%rsi, 56(%rsp)
	movq	%rcx, 24(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 40(%rsp)
	call	__rpc_thread_variables
	movq	184(%rax), %rbx
	testq	%rbx, %rbx
	je	.L17
	leaq	8872(%rbx), %rax
	leaq	24(%rbx), %rbp
	leaq	96(%rsp), %r12
	movq	%rax, 8(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	64(%rsp), %rax
	movq	%rax, 48(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movq	32(%rbx), %rax
	movq	16(%rsp), %rsi
	movq	%rbp, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	je	.L13
	movq	0(%r13), %rdi
	movq	%rbp, %rsi
	movq	56(%rdi), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L13
	xorl	%eax, %eax
	movq	24(%rsp), %rsi
	movq	%rbp, %rdi
	call	*%r14
	testl	%eax, %eax
	je	.L13
	movq	32(%rbx), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	$1, %edi
	call	__GI_svc_getreq
	movq	32(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movl	$1, 24(%rbx)
	call	*40(%rax)
	movq	16+__GI__null_auth(%rip), %rax
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movdqu	__GI__null_auth(%rip), %xmm0
	movq	%rax, 136(%rsp)
	movq	40(%rsp), %rax
	movups	%xmm0, 120(%rsp)
	movq	%rax, 152(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 160(%rsp)
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	je	.L18
	movq	48(%rsp), %rsi
	movq	%r12, %rdi
	call	__GI__seterr_reply
	movl	64(%rsp), %r15d
	movq	0(%r13), %rdi
	testl	%r15d, %r15d
	je	.L33
	movq	56(%rdi), %rax
	call	*24(%rax)
	testl	%eax, %eax
	je	.L9
.L11:
	movq	32(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movl	$0, 24(%rbx)
	call	*40(%rax)
	movq	32(%rbx), %rax
	addq	$1, 8872(%rbx)
	movq	%rbp, %rdi
	movl	8896(%rbx), %edx
	movq	8(%rsp), %rsi
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L12
.L13:
	movl	$1, %r15d
.L9:
	addq	$200, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$16, %r15d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$2, %r15d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L33:
	movq	56(%rdi), %rax
	addq	$24, %r12
	movq	%r12, %rsi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L34
	movq	0(%r13), %rdi
	movq	%r12, %rsi
	movq	56(%rdi), %rax
	call	*16(%rax)
	testl	%eax, %eax
	movl	$7, %eax
	cmove	%eax, %r15d
	cmpq	$0, 128(%rsp)
	je	.L9
	movl	$2, 24(%rbx)
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_opaque_auth
	jmp	.L9
.L34:
	movl	$7, %r15d
	jmp	.L9
	.size	clntraw_call, .-clntraw_call
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"clnt_raw.c: fatal header serialization error"
	.text
	.p2align 4,,15
	.globl	__GI_clntraw_create
	.hidden	__GI_clntraw_create
	.type	__GI_clntraw_create, @function
__GI_clntraw_create:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	subq	$96, %rsp
	call	__rpc_thread_variables
	movq	184(%rax), %rbx
	testq	%rbx, %rbx
	je	.L45
.L36:
	leaq	24(%rbx), %rbp
	leaq	8872(%rbx), %rsi
	xorl	%ecx, %ecx
	movl	$24, %edx
	movl	$0, 8(%rsp)
	movq	$2, 16(%rsp)
	movq	%rbp, %rdi
	movq	%r14, 24(%rsp)
	movq	%r13, 32(%rsp)
	call	__GI_xdrmem_create
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	movq	%rbx, %r12
	call	__GI_xdr_callhdr
	testl	%eax, %eax
	je	.L46
.L38:
	movq	32(%rbx), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	%eax, 8896(%rbx)
	movq	32(%rbx), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L39
	movq	%rbp, %rdi
	call	*%rax
.L39:
	leaq	72(%rbx), %rsi
	movl	$2, %ecx
	movl	$8800, %edx
	movq	%rbp, %rdi
	call	__GI_xdrmem_create
	leaq	client_ops(%rip), %rax
	movq	%rax, 8(%rbx)
	call	__GI_authnone_create
	movq	%rax, (%rbx)
.L35:
	addq	$96, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$8904, %esi
	movl	$1, %edi
	movq	%rax, %rbp
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L40
	movq	%rax, 184(%rbp)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r12d, %r12d
	jmp	.L35
	.size	__GI_clntraw_create, .-__GI_clntraw_create
	.globl	__EI_clntraw_create
	.set	__EI_clntraw_create,__GI_clntraw_create
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	client_ops, @object
	.size	client_ops, 48
client_ops:
	.quad	clntraw_call
	.quad	clntraw_abort
	.quad	clntraw_geterr
	.quad	clntraw_freeres
	.quad	clntraw_destroy
	.quad	clntraw_control
	.hidden	__rpc_thread_variables
