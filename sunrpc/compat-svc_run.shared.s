	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_svc_exit, svc_exit@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_svc_exit
	.hidden	__GI_svc_exit
	.type	__GI_svc_exit, @function
__GI_svc_exit:
	pushq	%rbx
	call	__GI___rpc_thread_svc_pollfd
	movq	(%rax), %rdi
	movq	%rax, %rbx
	call	free@PLT
	movq	$0, (%rbx)
	call	__GI___rpc_thread_svc_max_pollfd
	movl	$0, (%rax)
	popq	%rbx
	ret
	.size	__GI_svc_exit, .-__GI_svc_exit
	.globl	__EI_svc_exit
	.set	__EI_svc_exit,__GI_svc_exit
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"svc_run: - out of memory"
.LC1:
	.string	"svc_run: - poll failed"
	.text
	.p2align 4,,15
	.globl	__GI_svc_run
	.hidden	__GI_svc_run
	.type	__GI_svc_run, @function
__GI_svc_run:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	movl	$-1, %r14d
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$8, %rsp
	call	__GI___rpc_thread_svc_max_pollfd
	movq	%rax, %r13
.L5:
	movl	0(%r13), %ebx
	testl	%ebx, %ebx
	jne	.L7
	call	__GI___rpc_thread_svc_pollfd
	cmpq	$0, (%rax)
	je	.L8
	testl	%r15d, %r15d
	je	.L17
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L28
.L15:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
.L8:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	%ebx, %r15d
	movslq	%ebx, %r12
	jne	.L29
.L10:
	testl	%ebx, %ebx
	jle	.L11
	call	__GI___rpc_thread_svc_pollfd
	movq	(%rax), %rax
	leal	-1(%rbx), %ecx
	movq	%rbp, %rdx
	leaq	8(%rax,%rcx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L12:
	movl	(%rax), %ecx
	addq	$8, %rax
	addq	$8, %rdx
	movl	%ecx, -8(%rdx)
	movzwl	-4(%rax), %ecx
	movw	%cx, -4(%rdx)
	xorl	%ecx, %ecx
	movw	%cx, -2(%rdx)
	cmpq	%rsi, %rax
	jne	.L12
.L11:
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__GI___poll
	cmpl	$-1, %eax
	je	.L14
	testl	%eax, %eax
	je	.L6
	movl	%eax, %esi
	movq	%rbp, %rdi
	call	__GI_svc_getreq_poll
.L6:
	movl	%ebx, %r15d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	je	.L6
	leaq	.LC1(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	0(,%r12,8), %rsi
	movq	%rbp, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rbp
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%r12d, %r12d
	jmp	.L11
.L28:
	movq	%rax, %rbp
	xorl	%r12d, %r12d
	jmp	.L11
	.size	__GI_svc_run, .-__GI_svc_run
	.globl	svc_run
	.set	svc_run,__GI_svc_run
