	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_svcfd_create, svcfd_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	rendezvous_stat, @function
rendezvous_stat:
	movl	$2, %eax
	ret
	.size	rendezvous_stat, .-rendezvous_stat
	.p2align 4,,15
	.type	svctcp_getargs, @function
svctcp_getargs:
	movq	64(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	addq	$16, %rdi
	jmp	*%rcx
	.size	svctcp_getargs, .-svctcp_getargs
	.p2align 4,,15
	.type	svctcp_freeargs, @function
svctcp_freeargs:
	movq	64(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	$2, 16(%rdi)
	addq	$16, %rdi
	jmp	*%rcx
	.size	svctcp_freeargs, .-svctcp_freeargs
	.p2align 4,,15
	.type	svctcp_destroy, @function
svctcp_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rbp
	call	__GI_xprt_unregister
	movl	(%rbx), %edi
	call	__GI___close
	cmpw	$0, 4(%rbx)
	jne	.L6
	movq	24(%rbp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L6
	leaq	16(%rbp), %rdi
	call	*%rax
.L6:
	movq	%rbp, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.size	svctcp_destroy, .-svctcp_destroy
	.p2align 4,,15
	.type	svctcp_rendezvous_abort, @function
svctcp_rendezvous_abort:
	subq	$8, %rsp
	call	__GI_abort
	.size	svctcp_rendezvous_abort, .-svctcp_rendezvous_abort
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"svc_tcp: makefd_xprt"
.LC2:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.type	makefd_xprt, @function
makefd_xprt:
	pushq	%r14
	pushq	%r13
	movl	%edx, %r14d
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	movl	$336, %edi
	movl	%esi, %r13d
	call	malloc@PLT
	movl	$464, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rbx, %rbx
	movq	%rax, %rbp
	je	.L17
	testq	%rax, %rax
	je	.L17
	leaq	16(%rax), %rdi
	leaq	writetcp(%rip), %r9
	leaq	readtcp(%rip), %r8
	movl	$2, (%rax)
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movl	%r13d, %esi
	call	__GI_xdrrec_create
	leaq	svctcp_op(%rip), %rax
	movq	%rbp, 64(%rbx)
	addq	$64, %rbp
	movq	$0, 72(%rbx)
	movq	%rbp, 48(%rbx)
	movq	%rbx, %rdi
	movq	%rax, 8(%rbx)
	xorl	%eax, %eax
	movl	$0, 16(%rbx)
	movw	%ax, 4(%rbx)
	movl	%r12d, (%rbx)
	call	__GI_xprt_register
.L13:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L13
	.size	makefd_xprt, .-makefd_xprt
	.p2align 4,,15
	.type	rendezvous_request, @function
rendezvous_request:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	64(%rdi), %r13
	leaq	16(%rsp), %r12
	leaq	12(%rsp), %rbp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L26
.L20:
	movl	(%rbx), %edi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movl	$16, 12(%rsp)
	call	__GI_accept
	testl	%eax, %eax
	js	.L27
	movl	4(%r13), %edx
	movl	0(%r13), %esi
	movl	%eax, %edi
	call	makefd_xprt
	testq	%rax, %rax
	je	.L28
	movdqa	16(%rsp), %xmm0
	movl	12(%rsp), %edx
	movups	%xmm0, 20(%rax)
	movl	%edx, 16(%rax)
.L22:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	call	__svc_accept_failed
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	call	__svc_wait_on_error
	jmp	.L22
	.size	rendezvous_request, .-rendezvous_request
	.p2align 4,,15
	.type	svctcp_reply, @function
svctcp_reply:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	64(%rdi), %rax
	leaq	16(%rax), %rbx
	movl	$0, 16(%rax)
	movq	8(%rax), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rsi)
	call	__GI_xdr_replymsg
	movq	%rbx, %rdi
	movl	$1, %esi
	movl	%eax, %ebp
	call	__GI_xdrrec_endofrecord
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	svctcp_reply, .-svctcp_reply
	.p2align 4,,15
	.type	svctcp_stat, @function
svctcp_stat:
	movq	64(%rdi), %rdi
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L41
	rep ret
	.p2align 4,,10
	.p2align 3
.L41:
	subq	$8, %rsp
	addq	$16, %rdi
	call	__GI_xdrrec_eof
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	addl	$1, %eax
	ret
	.size	svctcp_stat, .-svctcp_stat
	.p2align 4,,15
	.type	svctcp_recv, @function
svctcp_recv:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	64(%rdi), %rbx
	leaq	16(%rbx), %rbp
	movl	$1, 16(%rbx)
	movq	%rbp, %rdi
	call	__GI_xdrrec_skiprecord
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_callmsg
	testl	%eax, %eax
	jne	.L46
	movl	$0, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	(%r12), %rax
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	svctcp_recv, .-svctcp_recv
	.p2align 4,,15
	.type	writetcp, @function
writetcp:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testl	%edx, %edx
	jle	.L48
	movq	%rdi, %r12
	movq	%rsi, %rbp
	movl	%edx, %ebx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L49:
	subl	%eax, %ebx
	cltq
	addq	%rax, %rbp
	testl	%ebx, %ebx
	jle	.L48
.L51:
	movl	(%r12), %edi
	movslq	%ebx, %rdx
	movq	%rbp, %rsi
	call	__GI___write
	testl	%eax, %eax
	jns	.L49
	movq	64(%r12), %rax
	movl	$0, (%rax)
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	writetcp, .-writetcp
	.p2align 4,,15
	.type	readtcp, @function
readtcp:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	%edx, %r13d
	subq	$16, %rsp
	movl	(%rdi), %ebx
	leaq	8(%rsp), %rbp
.L61:
	movl	$1, %eax
	movl	$35000, %edx
	movl	$1, %esi
	movq	%rbp, %rdi
	movl	%ebx, 8(%rsp)
	movw	%ax, 12(%rsp)
	call	__GI___poll
	cmpl	$-1, %eax
	je	.L56
	testl	%eax, %eax
	je	.L57
	movzwl	14(%rsp), %eax
	testb	$56, %al
	jne	.L57
.L59:
	testb	$1, %al
	je	.L61
	movslq	%r13d, %rdx
	movq	%r14, %rsi
	movl	%ebx, %edi
	call	__GI___read
	testl	%eax, %eax
	jg	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	movq	64(%r12), %rax
	movl	$0, (%rax)
	movl	$-1, %eax
.L54:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L57
	movzwl	14(%rsp), %eax
	jmp	.L59
	.size	readtcp, .-readtcp
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"svc_tcp.c - tcp socket creation problem"
	.align 8
.LC4:
	.string	"svc_tcp.c - cannot getsockname or listen"
	.text
	.p2align 4,,15
	.globl	__GI_svctcp_create
	.hidden	__GI_svctcp_create
	.type	__GI_svctcp_create, @function
__GI_svctcp_create:
	pushq	%r14
	pushq	%r13
	xorl	%r14d, %r14d
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r13d
	pushq	%rbx
	movl	%edx, %r12d
	movl	%edi, %ebx
	subq	$32, %rsp
	cmpl	$-1, %edi
	movl	$16, 12(%rsp)
	je	.L90
.L71:
	leaq	16(%rsp), %rbp
	xorl	%edx, %edx
	movq	$0, 18(%rsp)
	movl	$2, %ecx
	movl	%ebx, %edi
	movl	$0, 10(%rbp)
	movw	%dx, 14(%rbp)
	movq	%rbp, %rsi
	movw	%cx, 16(%rsp)
	call	__GI_bindresvport
	testl	%eax, %eax
	jne	.L91
.L73:
	leaq	12(%rsp), %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	call	__getsockname
	testl	%eax, %eax
	je	.L74
.L76:
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	testl	%r14d, %r14d
	jne	.L75
.L89:
	xorl	%ebp, %ebp
.L70:
	addq	$32, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$4096, %esi
	movl	%ebx, %edi
	call	__listen
	testl	%eax, %eax
	jne	.L76
	movl	$8, %edi
	call	malloc@PLT
	movl	$336, %edi
	movq	%rax, %r14
	call	malloc@PLT
	testq	%r14, %r14
	movq	%rax, %rbp
	je	.L82
	testq	%rax, %rax
	je	.L82
	movdqu	__GI__null_auth(%rip), %xmm0
	movq	%rbp, %rdi
	movq	%r14, 64(%rax)
	movq	$0, 72(%rax)
	movups	%xmm0, 40(%rax)
	movq	16+__GI__null_auth(%rip), %rax
	movl	%r13d, (%r14)
	movq	%rax, 56(%rbp)
	leaq	svctcp_rendezvous_op(%rip), %rax
	movl	%r12d, 4(%r14)
	movl	%ebx, 0(%rbp)
	movq	%rax, 8(%rbp)
	movzwl	18(%rsp), %eax
	rolw	$8, %ax
	movw	%ax, 4(%rbp)
	call	__GI_xprt_register
	addq	$32, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movl	%ebx, %edi
	call	__GI___close
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$6, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %ebx
	movl	$1, %r14d
	jns	.L71
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	xorl	%ebp, %ebp
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L91:
	movl	12(%rsp), %edx
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movw	%ax, 18(%rsp)
	call	__bind
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L82:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.11235(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%r14, %rdi
	call	free@PLT
	movq	%rbp, %rdi
	xorl	%ebp, %ebp
	call	free@PLT
	jmp	.L70
	.size	__GI_svctcp_create, .-__GI_svctcp_create
	.globl	svctcp_create
	.set	svctcp_create,__GI_svctcp_create
	.p2align 4,,15
	.globl	__GI_svcfd_create
	.hidden	__GI_svcfd_create
	.type	__GI_svcfd_create, @function
__GI_svcfd_create:
	jmp	makefd_xprt
	.size	__GI_svcfd_create, .-__GI_svcfd_create
	.globl	__EI_svcfd_create
	.set	__EI_svcfd_create,__GI_svcfd_create
	.section	.rodata.str1.8
	.align 8
	.type	__func__.11235, @object
	.size	__func__.11235, 14
__func__.11235:
	.string	"svctcp_create"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	svctcp_rendezvous_op, @object
	.size	svctcp_rendezvous_op, 48
svctcp_rendezvous_op:
	.quad	rendezvous_request
	.quad	rendezvous_stat
	.quad	svctcp_rendezvous_abort
	.quad	svctcp_rendezvous_abort
	.quad	svctcp_rendezvous_abort
	.quad	svctcp_destroy
	.align 32
	.type	svctcp_op, @object
	.size	svctcp_op, 48
svctcp_op:
	.quad	svctcp_recv
	.quad	svctcp_stat
	.quad	svctcp_getargs
	.quad	svctcp_reply
	.quad	svctcp_freeargs
	.quad	svctcp_destroy
	.hidden	__bind
	.hidden	__listen
	.hidden	__getsockname
	.hidden	__svc_wait_on_error
	.hidden	__svc_accept_failed
	.hidden	__fxprintf
