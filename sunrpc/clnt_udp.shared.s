	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI___libc_clntudp_bufcreate, __libc_clntudp_bufcreate@GLIBC_PRIVATE
	.symver __EI_clntudp_bufcreate, clntudp_bufcreate@GLIBC_2.2.5
	.symver __EI_clntudp_create, clntudp_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	clntudp_geterr, @function
clntudp_geterr:
	movq	16(%rdi), %rax
	movdqu	64(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movq	80(%rax), %rax
	movq	%rax, 16(%rsi)
	ret
	.size	clntudp_geterr, .-clntudp_geterr
	.p2align 4,,15
	.type	clntudp_freeres, @function
clntudp_freeres:
	movq	16(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	$2, 88(%rdi)
	addq	$88, %rdi
	jmp	*%rcx
	.size	clntudp_freeres, .-clntudp_freeres
	.p2align 4,,15
	.type	clntudp_abort, @function
clntudp_abort:
	rep ret
	.size	clntudp_abort, .-clntudp_abort
	.p2align 4,,15
	.type	clntudp_control, @function
clntudp_control:
	cmpl	$15, %esi
	movq	16(%rdi), %rdi
	ja	.L22
	leaq	.L8(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L8:
	.long	.L22-.L8
	.long	.L7-.L8
	.long	.L9-.L8
	.long	.L10-.L8
	.long	.L11-.L8
	.long	.L12-.L8
	.long	.L13-.L8
	.long	.L22-.L8
	.long	.L14-.L8
	.long	.L15-.L8
	.long	.L16-.L8
	.long	.L17-.L8
	.long	.L18-.L8
	.long	.L19-.L8
	.long	.L20-.L8
	.long	.L21-.L8
	.text
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rdx), %rax
	movq	144(%rdi), %rdx
	bswap	%eax
	movl	%eax, 12(%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movdqu	(%rdx), %xmm0
	movl	$1, %eax
	movups	%xmm0, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movdqu	48(%rdi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqu	8(%rdi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movdqu	(%rdx), %xmm0
	movl	$1, %eax
	movups	%xmm0, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movdqu	32(%rdi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	(%rdi), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$1, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$0, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	144(%rdi), %rax
	movl	(%rax), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%rdx), %rax
	movq	144(%rdi), %rdx
	subl	$1, %eax
	bswap	%eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	144(%rdi), %rax
	movl	16(%rax), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	(%rdx), %rax
	movq	144(%rdi), %rdx
	bswap	%eax
	movl	%eax, 16(%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	144(%rdi), %rax
	movl	12(%rax), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%eax, %eax
	ret
	.size	clntudp_control, .-clntudp_control
	.p2align 4,,15
	.type	clntudp_destroy, @function
clntudp_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	16(%rdi), %rbx
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jne	.L30
.L24:
	movq	96(%rbx), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L25
	leaq	88(%rbx), %rdi
	call	*%rax
.L25:
	movq	%rbx, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	movl	(%rbx), %edi
	call	__GI___close
	jmp	.L24
	.size	clntudp_destroy, .-clntudp_destroy
	.p2align 4,,15
	.type	clntudp_call, @function
clntudp_call:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$440, %rsp
	movq	16(%rdi), %r15
	movq	504(%rsp), %rax
	movq	%rsi, 152(%rsp)
	movq	%rcx, (%rsp)
	movq	%r8, 16(%rsp)
	movq	%r9, 24(%rsp)
	movq	496(%rsp), %rbp
	movq	%rax, 8(%rsp)
	call	__deadline_current_time
	testq	%rbx, %rbx
	movq	%rax, %r14
	movq	%rdx, %r13
	je	.L32
	movq	56(%r15), %rcx
	cmpq	$-1, %rcx
	je	.L168
	movq	48(%r15), %rdx
.L34:
	testq	%rdx, %rdx
	js	.L48
	cmpq	$999999, %rcx
	jbe	.L169
.L48:
	movl	$5, 64(%r15)
	movl	$5, %eax
.L31:
	addq	$440, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	__deadline_from_timeval
	movq	%rax, 48(%rsp)
	movq	%rdx, 56(%rsp)
.L32:
	cmpq	$0, 32(%r15)
	js	.L48
	cmpq	$999999, 40(%r15)
	ja	.L48
	leaq	156(%r15), %rax
	leaq	88(%r15), %rbp
	movl	$3, 116(%rsp)
	movl	$0, 8(%rsp)
	movq	%rax, 88(%rsp)
	leaq	152(%rsp), %rax
	movq	%rbp, 120(%rsp)
	movq	%rax, 32(%rsp)
	leaq	168(%rsp), %rax
	movq	%rax, 40(%rsp)
.L39:
	testq	%rbx, %rbx
	je	.L40
	movq	96(%r15), %rax
	movl	136(%r15), %esi
	movq	%rbp, %rdi
	movl	$0, 88(%r15)
	call	*40(%rax)
	movq	144(%r15), %rax
	movq	32(%rsp), %rsi
	movq	%rbp, %rdi
	addl	$1, (%rax)
	movq	96(%r15), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L43
	movq	(%r12), %rdi
	movq	%rbp, %rsi
	movq	56(%rdi), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L43
	xorl	%eax, %eax
	movq	(%rsp), %rsi
	movq	%rbp, %rdi
	call	*%rbx
	testl	%eax, %eax
	je	.L43
	movq	96(%r15), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	%eax, 8(%rsp)
.L44:
	movslq	8(%rsp), %r13
	movq	144(%r15), %rsi
	leaq	8(%r15), %r8
	movl	24(%r15), %r9d
	movl	(%r15), %edi
	xorl	%ecx, %ecx
	movq	%r13, %rdx
	call	__sendto
	cmpq	%r13, %rax
	jne	.L170
	call	__deadline_current_time
	movq	%rax, %r14
	movq	%rdx, %r13
.L40:
	movq	40(%r15), %rcx
	movq	32(%r15), %rdx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	__deadline_from_timeval
	movq	%rax, 80(%rsp)
	movq	16+__GI__null_auth(%rip), %rax
	movl	$1, %ecx
	movdqu	__GI__null_auth(%rip), %xmm0
	movq	%r12, 136(%rsp)
	movq	%rbx, 72(%rsp)
	movq	%rax, 376(%rsp)
	movq	24(%rsp), %rax
	movups	%xmm0, 360(%rsp)
	movq	48(%rsp), %rbx
	movq	56(%rsp), %r12
	movq	%rax, 392(%rsp)
	movq	16(%rsp), %rax
	movq	%rbp, 144(%rsp)
	movq	%rdx, 64(%rsp)
	movl	$0, 112(%rsp)
	movq	%rax, 400(%rsp)
	movl	(%r15), %eax
	movw	%cx, 172(%rsp)
	movl	%eax, 168(%rsp)
	leaq	176(%rsp), %rax
	movq	%rax, 128(%rsp)
	leaq	164(%rsp), %rax
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L80:
	cmpq	$0, 72(%rsp)
	je	.L46
	testq	%r12, %r12
	movq	%rbx, %rdx
	movq	%r12, %rcx
	js	.L47
	cmpq	%rbx, %r14
	jg	.L48
	cmpq	%r12, %r13
	jl	.L47
	cmpq	%rbx, %r14
	je	.L48
.L47:
	cmpq	$0, 64(%rsp)
	js	.L50
	cmpq	80(%rsp), %rbx
	jl	.L50
	movq	80(%rsp), %rax
	cmpq	%rax, %rbx
	jne	.L92
	cmpq	64(%rsp), %r12
	movq	%rax, %rdx
	jge	.L92
.L50:
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	__deadline_to_ms
	testl	%eax, %eax
	je	.L171
.L52:
	movq	40(%rsp), %rdi
	movl	%eax, %edx
	movl	$1, %esi
	call	__GI___poll
	cmpl	$-1, %eax
	je	.L54
	testl	%eax, %eax
	jne	.L151
	movl	112(%rsp), %edx
	testl	%edx, %edx
	je	.L56
.L64:
	movl	$1, 112(%rsp)
.L57:
	call	__deadline_current_time
	movq	%rax, %r14
	movq	%rdx, %r13
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L168:
	movq	%rbp, %rdx
	movq	8(%rsp), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L54:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	je	.L57
.L155:
	movl	%eax, 72(%r15)
	.p2align 4,,10
	.p2align 3
.L166:
	movl	$4, 64(%r15)
.L167:
	movl	$4, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L151:
	testb	$8, 174(%rsp)
	jne	.L172
.L159:
	movq	%r12, %rax
	movq	%r14, 96(%rsp)
	movq	%r13, 104(%rsp)
	movq	%rbx, %r12
	movq	88(%rsp), %r14
	movq	%r15, %rbx
	movq	128(%rsp), %r13
	movq	%rax, %r15
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L174:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	jne	.L173
.L77:
	movslq	152(%rbx), %rdx
	movl	(%rbx), %edi
	movq	%rbp, %r9
	movq	%r13, %r8
	movl	$64, %ecx
	movq	%r14, %rsi
	movl	$16, 164(%rsp)
	call	__recvfrom
	testl	%eax, %eax
	js	.L174
	movq	%r15, %rcx
	cmpl	$3, %eax
	movq	%rbx, %r15
	movq	96(%rsp), %r14
	movq	%r12, %rbx
	movq	104(%rsp), %r13
	movq	%rcx, %r12
	jle	.L57
	cmpq	$0, 72(%rsp)
	je	.L79
	movq	144(%r15), %rdx
	movl	156(%r15), %ecx
	cmpl	%ecx, (%rdx)
	jne	.L57
.L79:
	leaq	224(%rsp), %r8
	movq	88(%rsp), %rsi
	movl	%eax, %edx
	movl	$1, %ecx
	movq	72(%rsp), %rbx
	movq	136(%rsp), %r12
	movq	%r8, %rdi
	movq	%r8, 72(%rsp)
	movq	144(%rsp), %rbp
	call	__GI_xdrmem_create
	movq	72(%rsp), %r8
	leaq	336(%rsp), %rax
	movq	%rax, %rsi
	movq	%rax, 64(%rsp)
	movq	%r8, %rdi
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	je	.L81
	movq	64(%rsp), %rdi
	leaq	64(%r15), %rsi
	call	__GI__seterr_reply
	movl	64(%r15), %eax
	testl	%eax, %eax
	je	.L175
	subl	$1, 116(%rsp)
	je	.L85
	movq	(%r12), %rdi
	movq	56(%rdi), %rax
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L39
.L85:
	movl	64(%r15), %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L172:
	movl	8(%rsp), %eax
	leal	256(%rax), %edi
	movslq	%edi, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L176
	leaq	256(%rax), %r9
	movslq	8(%rsp), %rax
	movl	(%r15), %edi
	leaq	272(%rsp), %rsi
	movl	$8192, %edx
	movq	%rcx, 304(%rsp)
	movq	%r9, 208(%rsp)
	movq	%r9, 104(%rsp)
	movq	%rcx, 96(%rsp)
	movl	$16, 280(%rsp)
	movq	%rax, 216(%rsp)
	leaq	192(%rsp), %rax
	movq	$1, 296(%rsp)
	movl	$0, 320(%rsp)
	movq	$256, 312(%rsp)
	movq	%rax, 272(%rsp)
	leaq	208(%rsp), %rax
	movq	%rax, 288(%rsp)
	call	__recvmsg
	testl	%eax, %eax
	movq	96(%rsp), %rcx
	movq	104(%rsp), %r9
	js	.L69
	movq	144(%r15), %rsi
	movslq	%eax, %rdx
	movq	%r9, %rdi
	movq	%rcx, 96(%rsp)
	movq	%rax, 104(%rsp)
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	movq	96(%rsp), %rcx
	jne	.L69
	testb	$32, 321(%rsp)
	je	.L69
	movl	280(%rsp), %eax
	testl	%eax, %eax
	jne	.L70
	movq	104(%rsp), %r8
	cmpl	$11, %r8d
	jg	.L71
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rcx, %rdi
	call	free@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L46:
	movq	80(%rsp), %rdx
	movq	64(%rsp), %rcx
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	__deadline_to_ms
	testl	%eax, %eax
	jne	.L52
	movl	$3, 64(%r15)
	movl	$3, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%r15, %rcx
	cmpl	$11, %eax
	movq	%rbx, %r15
	movq	%r12, %rbx
	movq	%rcx, %r12
	je	.L57
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	272(%rsp), %rdi
	call	__GI_getifaddrs
	testl	%eax, %eax
	jne	.L166
	movq	272(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L60
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L63:
	testb	$1, 16(%rax)
	je	.L61
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L61
	cmpw	$2, (%rdx)
	je	.L62
.L61:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L63
.L60:
	call	__GI_freeifaddrs
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$1, 64(%r15)
	movl	$1, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L92:
	movq	64(%rsp), %rcx
	movq	80(%rsp), %rdx
	jmp	.L50
.L170:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$3, 64(%r15)
	movl	%fs:(%rax), %eax
	movl	%eax, 72(%r15)
	movl	$3, %eax
	jmp	.L31
.L70:
	cmpl	$16, %eax
	jne	.L69
	cmpw	$2, 192(%rsp)
	jne	.L69
	movl	12(%r15), %eax
	cmpl	%eax, 196(%rsp)
	jne	.L69
	movzwl	10(%r15), %eax
	cmpw	%ax, 194(%rsp)
	jne	.L69
.L71:
	movq	312(%rsp), %rdi
	cmpq	$15, %rdi
	jbe	.L69
	movq	304(%rsp), %rax
	testq	%rax, %rax
	je	.L69
	movabsq	$47244640256, %rsi
	cmpq	%rsi, 8(%rax)
	je	.L91
	movq	(%rax), %rdx
	cmpq	$15, %rdx
	jbe	.L69
	addq	$7, %rdx
	addq	%rax, %rdi
	andq	$-8, %rdx
	addq	%rax, %rdx
	leaq	16(%rdx), %rax
	cmpq	%rax, %rdi
	jnb	.L73
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	$15, %rsi
	jbe	.L69
	movq	%rax, %rdx
	leaq	16(%rax), %rax
	cmpq	%rax, %rdi
	jb	.L69
.L73:
	movq	(%rdx), %rsi
	leaq	7(%rsi), %rax
	andq	$-8, %rax
	addq	%rdx, %rax
	cmpq	%rdi, %rax
	ja	.L69
	movabsq	$47244640256, %r10
	cmpq	%r10, 8(%rdx)
	jne	.L74
.L72:
	movl	16(%rdx), %eax
	movq	%rcx, %rdi
	movl	%eax, 72(%r15)
	call	free@PLT
	movl	$4, 64(%r15)
	jmp	.L167
.L171:
	movq	136(%rsp), %r12
	movq	72(%rsp), %rbx
	movq	144(%rsp), %rbp
	jmp	.L44
.L62:
	call	__GI_freeifaddrs
	jmp	.L64
.L176:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$4, 64(%r15)
	movl	%fs:(%rax), %eax
	movl	%eax, 72(%r15)
	jmp	.L167
.L91:
	movq	%rax, %rdx
	jmp	.L72
.L81:
	movl	$2, 64(%r15)
	jmp	.L85
.L175:
	movq	(%r12), %rdi
	movq	64(%rsp), %rbp
	movq	56(%rdi), %rax
	leaq	24(%rbp), %rsi
	call	*16(%rax)
	testl	%eax, %eax
	jne	.L83
	movl	$7, 64(%r15)
	movl	$6, 72(%r15)
.L83:
	cmpq	$0, 368(%rsp)
	je	.L85
	movq	120(%rsp), %rdi
	leaq	24(%rbp), %rsi
	movl	$2, 88(%r15)
	call	__GI_xdr_opaque_auth
	jmp	.L85
	.size	clntudp_call, .-clntudp_call
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"clntudp_create"
.LC2:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI___libc_clntudp_bufcreate
	.hidden	__GI___libc_clntudp_bufcreate
	.type	__GI___libc_clntudp_bufcreate, @function
__GI___libc_clntudp_bufcreate:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	$24, %edi
	subq	$168, %rsp
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rcx, 32(%rsp)
	movq	%r8, 40(%rsp)
	movq	%r9, 24(%rsp)
	call	malloc@PLT
	movq	%rax, %r12
	movl	224(%rsp), %eax
	leal	3(%rax), %ebx
	movl	232(%rsp), %eax
	andl	$-4, %ebx
	leal	3(%rax), %ebp
	movl	%ebx, %eax
	andl	$-4, %ebp
	movl	%ebp, %r14d
	leaq	160(%r14,%rax), %rdi
	call	malloc@PLT
	testq	%r12, %r12
	movq	%rax, %r15
	je	.L188
	testq	%rax, %rax
	je	.L188
	cmpw	$0, 2(%r13)
	leaq	156(%rax,%r14), %rax
	movq	%rax, 144(%r15)
	jne	.L182
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	movl	$17, %ecx
	movq	%r13, %rdi
	call	__GI_pmap_getport
	testw	%ax, %ax
	jne	.L199
.L185:
	movq	%r15, %rdi
	call	free@PLT
.L181:
	testq	%r12, %r12
	je	.L177
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	free@PLT
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L199:
	rolw	$8, %ax
	movw	%ax, 2(%r13)
.L182:
	leaq	udp_ops(%rip), %rax
	movdqu	0(%r13), %xmm0
	movq	%r15, 16(%r12)
	movl	%ebp, 152(%r15)
	leaq	88(%r15), %rbp
	movq	%rax, 8(%r12)
	movq	32(%rsp), %rax
	movups	%xmm0, 8(%r15)
	movl	$16, 24(%r15)
	movl	%ebx, 140(%r15)
	movq	%rax, 32(%r15)
	movq	40(%rsp), %rax
	movq	%rax, 40(%r15)
	movq	$-1, %rax
	movq	%rax, 48(%r15)
	movq	%rax, 56(%r15)
	call	_create_xid@PLT
	movq	%rax, 64(%rsp)
	movq	8(%rsp), %rax
	xorl	%ecx, %ecx
	movq	144(%r15), %rsi
	movl	%ebx, %edx
	movq	%rbp, %rdi
	movl	$0, 72(%rsp)
	movq	$2, 80(%rsp)
	movq	%rax, 88(%rsp)
	movq	16(%rsp), %rax
	movq	%rax, 96(%rsp)
	call	__GI_xdrmem_create
	leaq	64(%rsp), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_callhdr
	testl	%eax, %eax
	je	.L185
	movq	96(%r15), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	%eax, 136(%r15)
	movq	24(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	js	.L200
	movl	$0, 4(%r15)
.L186:
	movl	%eax, (%r15)
	call	__GI_authnone_create
	movq	%rax, (%r12)
.L177:
	addq	$168, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	call	__GI___rpc_thread_createerr
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rbx
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	testq	%r15, %r15
	movl	$12, (%rbx)
	movl	$12, 16(%rbx)
	je	.L181
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L200:
	movl	240(%rsp), %esi
	movl	$17, %edx
	movl	$2, %edi
	orl	$2050, %esi
	call	__GI___socket
	movq	24(%rsp), %rcx
	testl	%eax, %eax
	movl	%eax, (%rcx)
	js	.L201
	xorl	%esi, %esi
	movl	%eax, %edi
	call	__GI_bindresvport
	movq	24(%rsp), %rbx
	leaq	60(%rsp), %rcx
	movl	$4, %r8d
	movl	$11, %edx
	xorl	%esi, %esi
	movl	$1, 60(%rsp)
	movl	(%rbx), %edi
	call	__setsockopt
	movl	$1, 4(%r15)
	movl	(%rbx), %eax
	jmp	.L186
.L201:
	call	__GI___rpc_thread_createerr
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$12, (%rax)
	movl	%fs:(%rdx), %edx
	movl	%edx, 16(%rax)
	jmp	.L185
	.size	__GI___libc_clntudp_bufcreate, .-__GI___libc_clntudp_bufcreate
	.globl	__EI___libc_clntudp_bufcreate
	.set	__EI___libc_clntudp_bufcreate,__GI___libc_clntudp_bufcreate
	.p2align 4,,15
	.globl	__GI_clntudp_bufcreate
	.hidden	__GI_clntudp_bufcreate
	.type	__GI_clntudp_bufcreate, @function
__GI_clntudp_bufcreate:
	subq	$16, %rsp
	pushq	$0
	movl	40(%rsp), %eax
	pushq	%rax
	movl	40(%rsp), %eax
	pushq	%rax
	call	__GI___libc_clntudp_bufcreate
	addq	$40, %rsp
	ret
	.size	__GI_clntudp_bufcreate, .-__GI_clntudp_bufcreate
	.globl	__EI_clntudp_bufcreate
	.set	__EI_clntudp_bufcreate,__GI_clntudp_bufcreate
	.p2align 4,,15
	.globl	__GI_clntudp_create
	.hidden	__GI_clntudp_create
	.type	__GI_clntudp_create, @function
__GI_clntudp_create:
	subq	$16, %rsp
	pushq	$0
	pushq	$8800
	pushq	$8800
	call	__GI___libc_clntudp_bufcreate
	addq	$40, %rsp
	ret
	.size	__GI_clntudp_create, .-__GI_clntudp_create
	.globl	__EI_clntudp_create
	.set	__EI_clntudp_create,__GI_clntudp_create
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	udp_ops, @object
	.size	udp_ops, 48
udp_ops:
	.quad	clntudp_call
	.quad	clntudp_abort
	.quad	clntudp_geterr
	.quad	clntudp_freeres
	.quad	clntudp_destroy
	.quad	clntudp_control
	.hidden	__setsockopt
	.hidden	__fxprintf
	.hidden	__recvmsg
	.hidden	__recvfrom
	.hidden	__deadline_to_ms
	.hidden	__sendto
	.hidden	__deadline_from_timeval
	.hidden	__deadline_current_time
