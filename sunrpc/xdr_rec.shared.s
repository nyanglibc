	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdrrec_create, xdrrec_create@GLIBC_2.2.5
	.symver __EI_xdrrec_skiprecord, xdrrec_skiprecord@GLIBC_2.2.5
	.symver __EI_xdrrec_eof, xdrrec_eof@GLIBC_2.2.5
	.symver __EI_xdrrec_endofrecord, xdrrec_endofrecord@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	xdrrec_inline, @function
xdrrec_inline:
	movl	(%rdi), %eax
	movq	24(%rdi), %rdx
	testl	%eax, %eax
	je	.L3
	cmpl	$1, %eax
	jne	.L8
	movq	104(%rdx), %rcx
	movl	%esi, %esi
	cmpq	%rcx, %rsi
	jg	.L8
	movq	88(%rdx), %rax
	leaq	(%rax,%rsi), %rdi
	cmpq	96(%rdx), %rdi
	ja	.L8
	subq	%rsi, %rcx
	movq	%rdi, 88(%rdx)
	movq	%rcx, 104(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	32(%rdx), %rax
	movl	%esi, %esi
	addq	%rax, %rsi
	cmpq	40(%rdx), %rsi
	ja	.L8
	movq	%rsi, 32(%rdx)
	ret
	.size	xdrrec_inline, .-xdrrec_inline
	.p2align 4,,15
	.type	skip_input_bytes, @function
skip_input_bytes:
	testq	%rsi, %rsi
	jle	.L20
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	96(%rdi), %rax
	movq	%rsi, %rbx
.L12:
	movq	88(%rbp), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	testl	%edx, %edx
	jne	.L14
.L23:
	movq	80(%rbp), %r12
	movl	72(%rbp), %edx
	andl	$3, %eax
	movq	0(%rbp), %rdi
	addq	%rax, %r12
	subl	%eax, %edx
	movq	%r12, %rsi
	call	*64(%rbp)
	cmpl	$-1, %eax
	je	.L17
	movq	%r12, 88(%rbp)
	cltq
	movq	88(%rbp), %rcx
	addq	%r12, %rax
	movq	%rax, %rdx
	movq	%rax, 96(%rbp)
	subq	%rcx, %rdx
	testl	%edx, %edx
	je	.L23
.L14:
	movslq	%edx, %rdx
	cmpq	%rbx, %rdx
	cmovg	%rbx, %rdx
	subq	%rdx, %rbx
	addq	%rdx, %rcx
	testq	%rbx, %rbx
	movq	%rcx, 88(%rbp)
	jg	.L12
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, %eax
	ret
	.size	skip_input_bytes, .-skip_input_bytes
	.p2align 4,,15
	.type	xdrrec_destroy, @function
xdrrec_destroy:
	pushq	%rbx
	movq	24(%rdi), %rbx
	movq	8(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.size	xdrrec_destroy, .-xdrrec_destroy
	.p2align 4,,15
	.type	xdrrec_getpos, @function
xdrrec_getpos:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$1, %edx
	xorl	%esi, %esi
	subq	$8, %rsp
	movq	24(%rdi), %rbx
	movl	(%rbx), %edi
	call	__GI___lseek
	movq	%rax, %rdx
	movl	$-1, %eax
	cmpq	$-1, %rdx
	je	.L26
	movl	0(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L28
	cmpl	$1, %ecx
	jne	.L26
	movq	96(%rbx), %rax
	subq	88(%rbx), %rax
	movl	%edx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	32(%rbx), %rax
	subq	24(%rbx), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	addl	%edx, %eax
	ret
	.size	xdrrec_getpos, .-xdrrec_getpos
	.p2align 4,,15
	.type	xdrrec_setpos, @function
xdrrec_setpos:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	24(%rdi), %rbp
	movq	%rdi, %rbx
	call	xdrrec_getpos
	movl	%eax, %edx
	xorl	%eax, %eax
	cmpl	$-1, %edx
	je	.L34
	movl	(%rbx), %ecx
	subl	%r12d, %edx
	testl	%ecx, %ecx
	je	.L36
	cmpl	$1, %ecx
	jne	.L34
	movq	104(%rbp), %rsi
	cmpl	%esi, %edx
	jge	.L34
	movq	88(%rbp), %rcx
	movslq	%edx, %rdx
	subq	%rdx, %rcx
	cmpq	%rcx, 96(%rbp)
	jb	.L34
	cmpq	%rcx, 80(%rbp)
	ja	.L34
	subq	%rdx, %rsi
	movq	%rcx, 88(%rbp)
	movl	$1, %eax
	movq	%rsi, 104(%rbp)
.L34:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	32(%rbp), %rsi
	movslq	%edx, %rdx
	subq	%rdx, %rsi
	cmpq	%rsi, 48(%rbp)
	jnb	.L34
	cmpq	%rsi, 40(%rbp)
	jbe	.L34
	movq	%rsi, 32(%rbp)
	movl	$1, %eax
	jmp	.L34
	.size	xdrrec_setpos, .-xdrrec_setpos
	.p2align 4,,15
	.type	get_input_bytes, @function
get_input_bytes:
	testl	%edx, %edx
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	jle	.L49
.L48:
	movq	96(%rbx), %rax
	movq	88(%rbx), %rsi
	movq	%rax, %rdx
	subq	%rsi, %rdx
	testl	%edx, %edx
	jne	.L50
	movq	80(%rbx), %rbp
	movl	72(%rbx), %edx
	andl	$3, %eax
	movq	(%rbx), %rdi
	addq	%rax, %rbp
	subl	%eax, %edx
	movq	%rbp, %rsi
	call	*64(%rbx)
	cmpl	$-1, %eax
	je	.L53
	cltq
	movq	%rbp, 88(%rbx)
	addq	%rax, %rbp
	movq	%rbp, 96(%rbx)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L53:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	%r12d, %edx
	movl	%edx, %ebp
	movq	%r13, %rdi
	cmovg	%r12d, %ebp
	movslq	%ebp, %r14
	subl	%ebp, %r12d
	movq	%r14, %rdx
	addq	%r14, %r13
	call	__GI_memcpy@PLT
	addq	%r14, 88(%rbx)
	testl	%r12d, %r12d
	jg	.L48
.L49:
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	get_input_bytes, .-get_input_bytes
	.p2align 4,,15
	.type	set_input_fragment, @function
set_input_fragment:
	pushq	%rbx
	movl	$4, %edx
	movq	%rdi, %rbx
	subq	$16, %rsp
	leaq	12(%rsp), %rsi
	call	get_input_bytes
	testl	%eax, %eax
	je	.L59
	movl	12(%rsp), %eax
	bswap	%eax
	movl	%eax, %edx
	shrl	$31, %edx
	testl	%eax, %eax
	movl	%edx, 112(%rbx)
	je	.L59
	andl	$2147483647, %eax
	movq	%rax, 104(%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	set_input_fragment, .-set_input_fragment
	.p2align 4,,15
	.type	xdrrec_getbytes, @function
xdrrec_getbytes:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r12d
	subq	$8, %rsp
	testl	%edx, %edx
	movq	24(%rdi), %rbp
	je	.L66
.L65:
	movq	104(%rbp), %rdx
	testl	%edx, %edx
	jne	.L67
	movl	112(%rbp), %eax
	testl	%eax, %eax
	jne	.L70
	movq	%rbp, %rdi
	call	set_input_fragment
	testl	%eax, %eax
	jne	.L65
.L70:
	xorl	%eax, %eax
.L64:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	%r12d, %edx
	movl	%edx, %ebx
	movq	%r13, %rsi
	cmova	%r12d, %ebx
	movq	%rbp, %rdi
	movl	%ebx, %edx
	call	get_input_bytes
	testl	%eax, %eax
	je	.L70
	movl	%ebx, %eax
	subq	%rax, 104(%rbp)
	addq	%rax, %r13
	subl	%ebx, %r12d
	jne	.L65
.L66:
	movl	$1, %eax
	jmp	.L64
	.size	xdrrec_getbytes, .-xdrrec_getbytes
	.p2align 4,,15
	.type	flush_out, @function
flush_out:
	pushq	%rbp
	pushq	%rbx
	movl	$-2147483648, %edx
	movl	$0, %eax
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	32(%rdi), %rbp
	movq	48(%rdi), %rcx
	cmpl	$1, %esi
	movq	24(%rdi), %rsi
	movq	(%rdi), %rdi
	cmove	%edx, %eax
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	subq	%rsi, %rbp
	subq	$4, %rdx
	orl	%edx, %eax
	movl	%ebp, %edx
	bswap	%eax
	movl	%eax, (%rcx)
	call	*16(%rbx)
	xorl	%edx, %edx
	cmpl	%eax, %ebp
	jne	.L80
	movq	24(%rbx), %rax
	movl	$1, %edx
	movq	%rax, 48(%rbx)
	addq	$4, %rax
	movq	%rax, 32(%rbx)
.L80:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	flush_out, .-flush_out
	.p2align 4,,15
	.type	xdrrec_putint32, @function
xdrrec_putint32:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	24(%rdi), %rbx
	movq	32(%rbx), %rax
	leaq	4(%rax), %rdx
	cmpq	40(%rbx), %rdx
	movq	%rdx, 32(%rbx)
	ja	.L94
.L88:
	movl	0(%rbp), %edx
	bswap	%edx
	movl	%edx, (%rax)
	movl	$1, %eax
.L87:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%esi, %esi
	movq	%rax, 32(%rbx)
	movl	$1, 56(%rbx)
	movq	%rbx, %rdi
	call	flush_out
	testl	%eax, %eax
	je	.L87
	movq	32(%rbx), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, 32(%rbx)
	jmp	.L88
	.size	xdrrec_putint32, .-xdrrec_putint32
	.p2align 4,,15
	.type	xdrrec_putlong, @function
xdrrec_putlong:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	24(%rdi), %rbx
	movq	32(%rbx), %rax
	leaq	4(%rax), %rdx
	cmpq	40(%rbx), %rdx
	movq	%rdx, 32(%rbx)
	ja	.L102
.L96:
	movq	0(%rbp), %rdx
	bswap	%edx
	movl	%edx, (%rax)
	movl	$1, %eax
.L95:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	xorl	%esi, %esi
	movq	%rax, 32(%rbx)
	movl	$1, 56(%rbx)
	movq	%rbx, %rdi
	call	flush_out
	testl	%eax, %eax
	je	.L95
	movq	32(%rbx), %rax
	leaq	4(%rax), %rdx
	movq	%rdx, 32(%rbx)
	jmp	.L96
	.size	xdrrec_putlong, .-xdrrec_putlong
	.p2align 4,,15
	.type	xdrrec_putbytes, @function
xdrrec_putbytes:
	testl	%edx, %edx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	24(%rdi), %r13
	je	.L108
	movq	40(%r13), %rax
	movq	32(%r13), %rdi
	movq	%rsi, %r14
	movl	%edx, %ebp
	.p2align 4,,10
	.p2align 3
.L106:
	subq	%rdi, %rax
	movq	%r14, %rsi
	cmpl	%ebp, %eax
	movl	%eax, %ebx
	cmova	%ebp, %ebx
	movl	%ebx, %r12d
	subl	%ebx, %ebp
	movq	%r12, %rdx
	addq	%r12, %r14
	call	__GI_memcpy@PLT
	movq	32(%r13), %rdi
	movq	40(%r13), %rax
	addq	%r12, %rdi
	cmpq	%rax, %rdi
	movq	%rdi, 32(%r13)
	je	.L119
	testl	%ebp, %ebp
	jne	.L106
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$1, %eax
.L103:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	testl	%ebp, %ebp
	je	.L108
	xorl	%esi, %esi
	movl	$1, 56(%r13)
	movq	%r13, %rdi
	call	flush_out
	testl	%eax, %eax
	je	.L103
	movq	40(%r13), %rax
	movq	32(%r13), %rdi
	jmp	.L106
	.size	xdrrec_putbytes, .-xdrrec_putbytes
	.p2align 4,,15
	.type	xdrrec_getint32, @function
xdrrec_getint32:
	movq	24(%rdi), %rax
	movq	104(%rax), %rdx
	cmpq	$3, %rdx
	jle	.L121
	movq	88(%rax), %r8
	movq	96(%rax), %rcx
	subq	%r8, %rcx
	cmpq	$3, %rcx
	jg	.L130
.L121:
	pushq	%rbx
	movq	%rsi, %rbx
	movl	$4, %edx
	subq	$16, %rsp
	leaq	12(%rsp), %rsi
	call	xdrrec_getbytes
	testl	%eax, %eax
	je	.L120
	movl	12(%rsp), %eax
	bswap	%eax
	movl	%eax, (%rbx)
	movl	$1, %eax
.L120:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	movl	(%r8), %ecx
	subq	$4, %rdx
	addq	$4, %r8
	bswap	%ecx
	movl	%ecx, (%rsi)
	movq	%rdx, 104(%rax)
	movq	%r8, 88(%rax)
	movl	$1, %eax
	ret
	.size	xdrrec_getint32, .-xdrrec_getint32
	.p2align 4,,15
	.type	xdrrec_getlong, @function
xdrrec_getlong:
	movq	24(%rdi), %rax
	cmpq	$3, 104(%rax)
	jle	.L132
	movq	88(%rax), %rcx
	movq	96(%rax), %rdx
	subq	%rcx, %rdx
	cmpq	$3, %rdx
	jg	.L141
.L132:
	pushq	%rbx
	movq	%rsi, %rbx
	movl	$4, %edx
	subq	$16, %rsp
	leaq	12(%rsp), %rsi
	call	xdrrec_getbytes
	testl	%eax, %eax
	je	.L131
	movl	12(%rsp), %eax
	bswap	%eax
	cltq
	movq	%rax, (%rbx)
	movl	$1, %eax
.L131:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movl	(%rcx), %edx
	addq	$4, %rcx
	bswap	%edx
	movslq	%edx, %rdx
	movq	%rdx, (%rsi)
	movq	%rcx, 88(%rax)
	subq	$4, 104(%rax)
	movl	$1, %eax
	ret
	.size	xdrrec_getlong, .-xdrrec_getlong
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI_xdrrec_create
	.hidden	__GI_xdrrec_create
	.type	__GI_xdrrec_create, @function
__GI_xdrrec_create:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movl	$128, %edi
	movq	%r8, %r14
	subq	$24, %rsp
	movl	%edx, (%rsp)
	movq	%r9, 8(%rsp)
	call	malloc@PLT
	cmpl	$99, %ebp
	movq	%rax, %rbx
	movl	(%rsp), %edx
	jbe	.L147
	addl	$3, %ebp
	andl	$-4, %ebp
	leal	4(%rbp), %edi
.L143:
	cmpl	$99, %edx
	movl	$4000, %r13d
	jbe	.L144
	leal	3(%rdx), %ecx
	andl	$-4, %ecx
	movl	%ecx, %r13d
.L144:
	addl	%r13d, %edi
	call	malloc@PLT
	testq	%rbx, %rbx
	movq	%rax, %r8
	je	.L149
	testq	%rax, %rax
	je	.L149
	movl	%ebp, 116(%rbx)
	movq	%rax, 8(%rbx)
	addq	%rax, %rbp
	movq	%rax, 24(%rbx)
	leaq	xdrrec_ops(%rip), %rax
	movl	%r13d, %ecx
	movq	%rbp, 80(%rbx)
	movq	%r8, 48(%rbx)
	addq	$4, %r8
	movq	%rax, 8(%r12)
	movq	8(%rsp), %rax
	movq	%rbp, 40(%rbx)
	addq	%rcx, %rbp
	movl	%r13d, 120(%rbx)
	movq	%rbx, 24(%r12)
	movq	%r15, (%rbx)
	movq	%r14, 64(%rbx)
	movq	%rbp, 96(%rbx)
	movq	%rbp, 88(%rbx)
	movq	%rax, 16(%rbx)
	movq	%r8, 32(%rbx)
	movl	$0, 56(%rbx)
	movq	%rcx, 72(%rbx)
	movq	$0, 104(%rbx)
	movl	$1, 112(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$4004, %edi
	movl	$4000, %ebp
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%r8, (%rsp)
	call	__GI___dcgettext
	leaq	__func__.11235(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%rbx, %rdi
	call	free@PLT
	movq	(%rsp), %r8
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	movq	%r8, %rdi
	jmp	free@PLT
	.size	__GI_xdrrec_create, .-__GI_xdrrec_create
	.globl	__EI_xdrrec_create
	.set	__EI_xdrrec_create,__GI_xdrrec_create
	.p2align 4,,15
	.globl	__GI_xdrrec_skiprecord
	.hidden	__GI_xdrrec_skiprecord
	.type	__GI_xdrrec_skiprecord, @function
__GI_xdrrec_skiprecord:
	pushq	%rbx
	movq	24(%rdi), %rbx
.L152:
	movq	104(%rbx), %rsi
	testq	%rsi, %rsi
	jle	.L162
.L157:
	movq	%rbx, %rdi
	call	skip_input_bytes
	testl	%eax, %eax
	je	.L156
	movl	112(%rbx), %eax
	movq	$0, 104(%rbx)
	testl	%eax, %eax
	je	.L163
.L155:
	movl	$0, 112(%rbx)
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rbx, %rdi
	call	set_input_fragment
	testl	%eax, %eax
	jne	.L152
.L156:
	xorl	%eax, %eax
	popq	%rbx
	ret
.L162:
	cmpl	$0, 112(%rbx)
	je	.L157
	jmp	.L155
	.size	__GI_xdrrec_skiprecord, .-__GI_xdrrec_skiprecord
	.globl	__EI_xdrrec_skiprecord
	.set	__EI_xdrrec_skiprecord,__GI_xdrrec_skiprecord
	.p2align 4,,15
	.globl	__GI_xdrrec_eof
	.hidden	__GI_xdrrec_eof
	.type	__GI_xdrrec_eof, @function
__GI_xdrrec_eof:
	pushq	%rbx
	movq	24(%rdi), %rbx
.L165:
	movq	104(%rbx), %rsi
	testq	%rsi, %rsi
	jle	.L175
.L170:
	movq	%rbx, %rdi
	call	skip_input_bytes
	testl	%eax, %eax
	je	.L169
	movl	112(%rbx), %eax
	movq	$0, 104(%rbx)
	testl	%eax, %eax
	je	.L176
.L168:
	movq	96(%rbx), %rax
	cmpq	%rax, 88(%rbx)
	popq	%rbx
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L176:
	movq	%rbx, %rdi
	call	set_input_fragment
	testl	%eax, %eax
	jne	.L165
.L169:
	movl	$1, %eax
	popq	%rbx
	ret
.L175:
	cmpl	$0, 112(%rbx)
	je	.L170
	jmp	.L168
	.size	__GI_xdrrec_eof, .-__GI_xdrrec_eof
	.globl	__EI_xdrrec_eof
	.set	__EI_xdrrec_eof,__GI_xdrrec_eof
	.p2align 4,,15
	.globl	__GI_xdrrec_endofrecord
	.hidden	__GI_xdrrec_endofrecord
	.type	__GI_xdrrec_endofrecord, @function
__GI_xdrrec_endofrecord:
	testl	%esi, %esi
	movq	24(%rdi), %rdi
	jne	.L178
	movl	56(%rdi), %eax
	testl	%eax, %eax
	jne	.L178
	movq	32(%rdi), %rdx
	leaq	4(%rdx), %rcx
	cmpq	40(%rdi), %rcx
	jnb	.L178
	movq	48(%rdi), %rsi
	movq	%rdx, %rax
	subq	%rsi, %rax
	subq	$4, %rax
	orl	$-2147483648, %eax
	bswap	%eax
	movl	%eax, (%rsi)
	movq	%rdx, 48(%rdi)
	movl	$1, %eax
	movq	%rcx, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	movl	$0, 56(%rdi)
	movl	$1, %esi
	jmp	flush_out
	.size	__GI_xdrrec_endofrecord, .-__GI_xdrrec_endofrecord
	.globl	__EI_xdrrec_endofrecord
	.set	__EI_xdrrec_endofrecord,__GI_xdrrec_endofrecord
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__func__.11235, @object
	.size	__func__.11235, 14
__func__.11235:
	.string	"xdrrec_create"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	xdrrec_ops, @object
	.size	xdrrec_ops, 80
xdrrec_ops:
	.quad	xdrrec_getlong
	.quad	xdrrec_putlong
	.quad	xdrrec_getbytes
	.quad	xdrrec_putbytes
	.quad	xdrrec_getpos
	.quad	xdrrec_setpos
	.quad	xdrrec_inline
	.quad	xdrrec_destroy
	.quad	xdrrec_getint32
	.quad	xdrrec_putint32
	.hidden	__fxprintf
