	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_float, xdr_float@GLIBC_2.2.5
	.symver __EI_xdr_double, xdr_double@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_float
	.hidden	__GI_xdr_float
	.type	__GI_xdr_float, @function
__GI_xdr_float:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L3
	jb	.L4
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L1
	movq	8(%rsp), %rax
	movl	%eax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movslq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_float, .-__GI_xdr_float
	.globl	__EI_xdr_float
	.set	__EI_xdr_float,__GI_xdr_float
	.p2align 4,,15
	.globl	__GI_xdr_double
	.hidden	__GI_xdr_double
	.type	__GI_xdr_double, @function
__GI_xdr_double:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L15
	jb	.L16
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L13:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	8(%rdi), %rax
	movq	%rsp, %r12
	leaq	8(%r12), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L19
	movq	8(%rbx), %rax
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*(%rax)
	testl	%eax, %eax
	jne	.L29
.L19:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movslq	4(%rsi), %rax
	movq	%rsp, %rbp
	movq	%rax, (%rsp)
	movslq	(%rsi), %rax
	movq	%rbp, %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L19
	movq	8(%rbx), %rax
	leaq	8(%rbp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	setne	%al
	addq	$16, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rsp), %rax
	movl	%eax, 0(%rbp)
	movq	8(%rsp), %rax
	movl	%eax, 4(%rbp)
	movl	$1, %eax
	jmp	.L13
	.size	__GI_xdr_double, .-__GI_xdr_double
	.globl	__EI_xdr_double
	.set	__EI_xdr_double,__GI_xdr_double
