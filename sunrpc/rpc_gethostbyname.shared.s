	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_rpc_gethostbyname
	.hidden	__libc_rpc_gethostbyname
	.type	__libc_rpc_gethostbyname, @function
__libc_rpc_gethostbyname:
	pushq	%r15
	pushq	%r14
	movl	$1024, %r8d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	subq	$1112, %rsp
	leaq	64(%rsp), %rbx
	movq	%rsi, 8(%rsp)
	movq	$0, 24(%rsp)
	movq	$1024, 72(%rsp)
	leaq	24(%rsp), %r13
	leaq	32(%rsp), %r12
	leaq	16(%rbx), %rcx
	leaq	20(%rsp), %rbp
	movq	%rcx, 64(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	movq	72(%rsp), %r8
	movq	64(%rsp), %rcx
.L2:
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%r13, %r9
	pushq	%rbp
	movl	$2, %esi
	movq	%r14, %rdi
	call	__gethostbyname2_r
	movl	%eax, %r15d
	testl	%r15d, %r15d
	popq	%rax
	popq	%rdx
	jne	.L8
	movq	24(%rsp), %rax
	testq	%rax, %rax
	jne	.L17
.L8:
	movl	20(%rsp), %r15d
	cmpl	$-1, %r15d
	jne	.L3
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L3
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L7
	call	__GI___rpc_thread_createerr
	movl	$12, (%rax)
	movl	$12, 16(%rax)
.L1:
	addq	$1112, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	call	__GI___rpc_thread_createerr
	movq	64(%rsp), %rdi
	addq	$16, %rbx
	movl	$13, (%rax)
	cmpq	%rbx, %rdi
	je	.L15
	call	free@PLT
.L15:
	movl	$-1, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movabsq	$17179869186, %rcx
	cmpq	%rcx, 16(%rax)
	movq	64(%rsp), %rbp
	je	.L9
	addq	$16, %rbx
	call	__GI___rpc_thread_createerr
	cmpq	%rbx, %rbp
	movl	$12, (%rax)
	movl	$97, 16(%rax)
	je	.L15
	movq	%rbp, %rdi
	movl	$-1, %r15d
	call	free@PLT
	jmp	.L1
.L9:
	movq	24(%rax), %rax
	movq	8(%rsp), %rdx
	addq	$16, %rbx
	cmpq	%rbx, %rbp
	movq	(%rax), %rax
	movl	$2, (%rdx)
	movl	(%rax), %eax
	movl	%eax, 4(%rdx)
	je	.L1
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L1
	.size	__libc_rpc_gethostbyname, .-__libc_rpc_gethostbyname
	.hidden	__gethostbyname2_r
