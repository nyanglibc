	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"unix"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_clnt_create
	.hidden	__GI_clnt_create
	.type	__GI_clnt_create, @function
__GI_clnt_create:
	pushq	%rbp
	movq	%rdi, %r8
	leaq	.LC0(%rip), %rdi
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbx
	movq	%rcx, %rbx
	movl	$5, %ecx
	movq	%rdx, %r15
	subq	$168, %rsp
	movq	%rsi, -200(%rbp)
	movq	%rbx, %rsi
	repz cmpsb
	jne	.L2
	leaq	-160(%rbp), %rbx
	xorl	%eax, %eax
	movq	$0, -158(%rbp)
	movq	$0, -58(%rbp)
	movq	%r8, %rsi
	leaq	2(%rbx), %rdx
	leaq	8(%rbx), %rdi
	movq	%rdx, %rcx
	subq	%rdi, %rcx
	addl	$108, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rdx, %rdi
	movl	$1, %eax
	movw	%ax, -160(%rbp)
	call	__GI_strcpy
	leaq	-188(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	movl	$-1, -188(%rbp)
	call	__GI_clntunix_create
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	-176(%rbp), %rax
	movq	%r8, %rdi
	movq	%rax, %rsi
	movq	%rax, -208(%rbp)
	call	__libc_rpc_gethostbyname
	testl	%eax, %eax
	jne	.L10
	subq	$1040, %rsp
	movl	$1024, %r14d
	leaq	-160(%rbp), %r13
	leaq	15(%rsp), %rdx
	leaq	-184(%rbp), %r12
	andq	$-16, %rdx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	addq	%r14, %r14
	leaq	30(%r14), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
.L4:
	movq	%r12, %r8
	movq	%r14, %rcx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__getprotobyname_r
	testl	%eax, %eax
	jne	.L6
	movq	-184(%rbp), %rax
	testq	%rax, %rax
	jne	.L17
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	je	.L5
	call	__GI___rpc_thread_createerr
	movl	$17, (%rax)
	movl	$96, 16(%rax)
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	16(%rax), %eax
	movl	$-1, -188(%rbp)
	cmpl	$6, %eax
	je	.L8
	cmpl	$17, %eax
	jne	.L14
	movq	-200(%rbp), %rsi
	movq	-208(%rbp), %rdi
	leaq	-188(%rbp), %r9
	movl	$5, %ecx
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	call	__GI_clntudp_create
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	-200(%rbp), %rsi
	movq	-208(%rbp), %rdi
	leaq	-188(%rbp), %rcx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	call	__GI_clnttcp_create
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	call	__GI___rpc_thread_createerr
	movl	$12, (%rax)
	movl	$96, 16(%rax)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	jmp	.L1
	.size	__GI_clnt_create, .-__GI_clnt_create
	.globl	clnt_create
	.set	clnt_create,__GI_clnt_create
	.hidden	__getprotobyname_r
	.hidden	__libc_rpc_gethostbyname
