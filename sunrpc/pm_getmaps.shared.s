	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_pmap_getmaps, pmap_getmaps@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"pmap_getmaps.c: rpc problem"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_pmap_getmaps
	.hidden	__GI_pmap_getmaps
	.type	__GI_pmap_getmaps, @function
__GI_pmap_getmaps:
	pushq	%r14
	pushq	%r13
	movl	$28416, %r8d
	pushq	%rbp
	pushq	%rbx
	xorl	%r14d, %r14d
	movq	%rdi, %rbx
	movl	$60, %r13d
	subq	$24, %rsp
	movw	%r8w, 2(%rdi)
	movq	$0, 8(%rsp)
	call	__get_socket
	cmpl	$-1, %eax
	movl	%eax, 4(%rsp)
	je	.L22
	leaq	4(%rsp), %rcx
	movl	$500, %r9d
	movl	$50, %r8d
	movl	$2, %edx
	movl	$100000, %esi
	movq	%rbx, %rdi
	call	__GI_clnttcp_create
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L11
	movq	8(%rax), %rax
	pushq	%r14
	xorl	%ecx, %ecx
	pushq	%r13
	leaq	__GI_xdr_void(%rip), %rdx
	leaq	__GI_xdr_pmaplist(%rip), %r8
	movl	$4, %esi
	movq	%rbp, %rdi
	leaq	24(%rsp), %r9
	call	*(%rax)
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L23
.L19:
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
.L11:
	movl	4(%rsp), %edi
	call	__GI___close_nocancel
.L4:
	xorl	%eax, %eax
	movw	%ax, 2(%rbx)
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rbp, %rdi
	movq	%rax, %rsi
	call	__GI_clnt_perror
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	4(%rsp), %rcx
	movl	$500, %r9d
	movl	$50, %r8d
	movl	$2, %edx
	movl	$100000, %esi
	movq	%rbx, %rdi
	call	__GI_clnttcp_create
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L4
	movq	8(%rax), %rax
	pushq	%r14
	xorl	%ecx, %ecx
	pushq	%r13
	movl	$4, %esi
	movq	%rbp, %rdi
	leaq	__GI_xdr_pmaplist(%rip), %r8
	leaq	__GI_xdr_void(%rip), %rdx
	leaq	24(%rsp), %r9
	call	*(%rax)
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	jne	.L24
.L20:
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rbp, %rdi
	movq	%rax, %rsi
	call	__GI_clnt_perror
	jmp	.L20
	.size	__GI_pmap_getmaps, .-__GI_pmap_getmaps
	.globl	__EI_pmap_getmaps
	.set	__EI_pmap_getmaps,__GI_pmap_getmaps
	.hidden	__get_socket
