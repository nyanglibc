	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver getrpcport,getrpcport@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	getrpcport
	.type	getrpcport, @function
getrpcport:
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	subq	$24, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__libc_rpc_gethostbyname
	xorl	%esi, %esi
	testl	%eax, %eax
	jne	.L1
	movq	%rbp, %rsi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rdi
	call	__GI_pmap_getport
	movzwl	%ax, %esi
.L1:
	addq	$24, %rsp
	movl	%esi, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	getrpcport, .-getrpcport
	.hidden	__libc_rpc_gethostbyname
