	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	authdes_nextverf, @function
authdes_nextverf:
	rep ret
	.size	authdes_nextverf, .-authdes_nextverf
	.p2align 4,,15
	.type	authdes_destroy, @function
authdes_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	64(%rdi), %rbx
	movq	(%rbx), %rdi
	call	free@PLT
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.size	authdes_destroy, .-authdes_destroy
	.p2align 4,,15
	.type	authdes_validate, @function
authdes_validate:
	cmpl	$12, 16(%rsi)
	je	.L6
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	pushq	%rbx
	addq	$48, %rdi
	movl	$1, %ecx
	subq	$16, %rsp
	movq	8(%rsi), %rax
	movq	16(%rdi), %rbx
	leaq	4(%rsp), %rsi
	movl	(%rax), %edx
	movl	%edx, 4(%rsp)
	movl	4(%rax), %edx
	movl	8(%rax), %eax
	movl	%edx, 8(%rsp)
	movl	$8, %edx
	movl	%eax, 12(%rsp)
	call	__GI_ecb_crypt
	cmpl	$1, %eax
	jle	.L12
.L8:
	xorl	%eax, %eax
.L5:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	4(%rsp), %eax
	bswap	%eax
	addl	$1, %eax
	movl	%eax, 4(%rsp)
	movl	8(%rsp), %eax
	bswap	%eax
	movl	%eax, 8(%rsp)
	movq	116(%rbx), %rax
	cmpq	%rax, 4(%rsp)
	jne	.L8
	movl	12(%rsp), %eax
	movl	$1, 64(%rbx)
	movl	%eax, 60(%rbx)
	movl	$1, %eax
	jmp	.L5
	.size	authdes_validate, .-authdes_validate
	.p2align 4,,15
	.type	authdes_marshal, @function
authdes_marshal:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$48, %rsp
	movq	64(%rdi), %r12
	xorl	%edi, %edi
	leaq	32(%rsp), %rsi
	call	__GI___clock_gettime
	movq	40(%rsp), %rsi
	movabsq	$2361183241434822607, %rdx
	movl	52(%r12), %ecx
	addl	32(%rsp), %ecx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	movl	%ecx, 116(%r12)
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addl	56(%r12), %edx
	cmpl	$999999, %edx
	ja	.L14
	movl	%edx, 120(%r12)
.L15:
	bswap	%edx
	bswap	%ecx
	leaq	48(%rbp), %rdi
	movl	%edx, 20(%rsp)
	movl	64(%r12), %edx
	movl	%ecx, 16(%rsp)
	testl	%edx, %edx
	jne	.L16
	movl	28(%r12), %eax
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %r8
	xorl	%ecx, %ecx
	movq	$0, 8(%rsp)
	movl	%eax, %edx
	subl	$1, %eax
	bswap	%edx
	bswap	%eax
	movl	%edx, 24(%rsp)
	movl	$16, %edx
	movl	%eax, 28(%rsp)
	call	__GI_cbc_crypt
	cmpl	$1, %eax
	jle	.L18
.L24:
	addq	$48, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	16(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$8, %edx
	call	__GI_ecb_crypt
	cmpl	$1, %eax
	jg	.L24
.L18:
	movq	16(%rsp), %rax
	movq	%rax, 104(%r12)
	movl	64(%r12), %eax
	testl	%eax, %eax
	je	.L43
	movl	60(%r12), %eax
	movl	$0, 112(%r12)
	movl	$8, 4(%rsp)
	movl	%eax, 96(%r12)
.L21:
	movq	8(%rbx), %rax
	movl	$8, %esi
	movq	%rbx, %rdi
	call	*48(%rax)
	testq	%rax, %rax
	jne	.L44
	movq	8(%rbx), %rax
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	je	.L24
	movq	8(%rbx), %rax
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	je	.L24
.L23:
	leaq	64(%r12), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_authdes_cred
	testl	%eax, %eax
	je	.L24
	movq	8(%rbx), %rax
	movl	$12, 4(%rsp)
	movl	$8, %esi
	movq	%rbx, %rdi
	call	*48(%rax)
	testq	%rax, %rax
	jne	.L45
	movq	8(%rbx), %rax
	leaq	24(%rbp), %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	je	.L24
	movq	8(%rbx), %rax
	leaq	4(%rsp), %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	je	.L24
.L26:
	leaq	104(%r12), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_authdes_verf
	testl	%eax, %eax
	setne	%al
	addq	$48, %rsp
	popq	%rbx
	movzbl	%al, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	subl	$1000000, %edx
	addl	$1, %ecx
	movl	%edx, 120(%r12)
	movl	%ecx, 116(%r12)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L43:
	movl	24(%rsp), %eax
	movl	%eax, 88(%r12)
	movl	28(%rsp), %eax
	movl	%eax, 112(%r12)
	movl	8(%r12), %eax
	addl	$20, %eax
	movl	%eax, 4(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$50331648, (%rax)
	movl	4(%rsp), %edx
	bswap	%edx
	movl	%edx, 4(%rax)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$50331648, (%rax)
	movl	4(%rsp), %edx
	bswap	%edx
	movl	%edx, 4(%rax)
	jmp	.L26
	.size	authdes_marshal, .-authdes_marshal
	.p2align 4,,15
	.type	authdes_refresh, @function
authdes_refresh:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$32, %rsp
	movq	64(%rdi), %rbx
	leaq	16(%rsp), %r12
	movl	32(%rbx), %eax
	testl	%eax, %eax
	jne	.L54
.L47:
	movq	48(%rbp), %rax
	leaq	132(%rbx), %rdi
	movq	%rax, 124(%rbx)
	movq	%rdi, 24(%rsp)
	call	__GI_strlen
	movq	16(%rbx), %rdi
	leaq	124(%rbx), %rdx
	addl	$1, %eax
	movq	%r12, %rsi
	movl	%eax, 16(%rsp)
	call	__GI_key_encryptsession_pk
	xorl	%edx, %edx
	testl	%eax, %eax
	js	.L46
	movq	124(%rbx), %rax
	movl	$0, 64(%rbx)
	movl	$1, %edx
	movq	%rax, 80(%rbx)
	movq	(%rbx), %rax
	movq	%rax, 72(%rbx)
.L46:
	addq	$32, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	8(%rsp), %rdx
	leaq	52(%rbx), %rsi
	leaq	36(%rbx), %rdi
	movq	$5, 8(%rsp)
	call	__GI_rtime
	testl	%eax, %eax
	js	.L55
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	__GI___clock_gettime
	movq	24(%rsp), %rsi
	movabsq	$2361183241434822607, %rdx
	movl	52(%rbx), %ecx
	subl	16(%rsp), %ecx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	movl	%ecx, 52(%rbx)
	sarq	$7, %rdx
	subq	%rsi, %rdx
	movl	56(%rbx), %esi
	cmpq	%rsi, %rdx
	movq	%rsi, %rax
	jle	.L49
	subl	$1, %ecx
	addl	$1000000, %eax
	movl	%ecx, 52(%rbx)
.L49:
	subl	%edx, %eax
	movl	%eax, 56(%rbx)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L55:
	movq	$0, 52(%rbx)
	jmp	.L47
	.size	authdes_refresh, .-authdes_refresh
	.p2align 4,,15
	.globl	__GI_authdes_pk_create
	.hidden	__GI_authdes_pk_create
	.type	__GI_authdes_pk_create, @function
__GI_authdes_pk_create:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$72, %edi
	movq	%r8, %rbp
	subq	$312, %rsp
	movl	%edx, 12(%rsp)
	call	malloc@PLT
	movl	$1, %esi
	movq	%rax, %r15
	movl	$1160, %edi
	call	calloc@PLT
	testq	%r15, %r15
	movq	%rax, %r13
	je	.L57
	testq	%rax, %rax
	jne	.L84
.L57:
	testq	%r15, %r15
	jne	.L62
.L59:
	xorl	%r15d, %r15d
	testq	%r13, %r13
	je	.L56
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L68
	call	free@PLT
.L68:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.L69
	call	free@PLT
.L69:
	movq	%r13, %rdi
	xorl	%r15d, %r15d
	call	free@PLT
.L56:
	addq	$312, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movl	(%r14), %edx
	movq	8(%r14), %rsi
	leaq	132(%rax), %rdi
	leaq	48(%rsp), %r14
	call	__GI_memcpy@PLT
	movq	%r14, %rdi
	call	__GI_getnetname
	testl	%eax, %eax
	jne	.L58
.L62:
	movq	%r15, %rdi
	call	free@PLT
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r14, %rdx
.L60:
	movl	(%rdx), %esi
	addq	$4, %rdx
	leal	-16843009(%rsi), %eax
	notl	%esi
	andl	%esi, %eax
	andl	$-2139062144, %eax
	je	.L60
	movl	%eax, %esi
	shrl	$16, %esi
	testl	$32896, %eax
	cmove	%esi, %eax
	leaq	2(%rdx), %rsi
	movl	%eax, %ecx
	cmove	%rsi, %rdx
	addb	%al, %cl
	sbbl	%r14d, %edx
	andl	$-4, %edx
	movl	%edx, 8(%r13)
	addl	$1, %edx
	movq	%rdx, %rdi
	movq	%rdx, 32(%rsp)
	call	malloc@PLT
	movq	%rbx, %rdi
	movq	%rax, 0(%r13)
	movq	%rax, 40(%rsp)
	call	__GI_strlen
	movl	%eax, 24(%r13)
	addl	$1, %eax
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r13)
	movq	%rax, 24(%rsp)
	je	.L62
	movq	40(%rsp), %r10
	testq	%r10, %r10
	je	.L62
	movq	32(%rsp), %rdx
	movq	%r10, %rdi
	movq	%r14, %rsi
	call	__GI_memcpy@PLT
	movq	24(%rsp), %r9
	movq	16(%rsp), %rdx
	movq	%rbx, %rsi
	movq	%r9, %rdi
	call	__GI_memcpy@PLT
	testq	%r12, %r12
	movq	$0, 52(%r13)
	je	.L63
	movdqu	(%r12), %xmm0
	movl	$1, 32(%r13)
	movups	%xmm0, 36(%r13)
.L64:
	movl	12(%rsp), %eax
	testq	%rbp, %rbp
	movl	%eax, 28(%r13)
	je	.L85
	movq	0(%rbp), %rax
	movq	%rax, 48(%r15)
.L66:
	leaq	authdes_ops(%rip), %rax
	movl	$3, (%r15)
	movl	$3, 24(%r15)
	movq	%r13, 64(%r15)
	movq	%r15, %rdi
	movq	%rax, 56(%r15)
	call	authdes_refresh
	testl	%eax, %eax
	je	.L62
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$0, 32(%r13)
	jmp	.L64
.L85:
	leaq	48(%r15), %rdi
	call	__GI_key_gendes
	testl	%eax, %eax
	jns	.L66
	jmp	.L62
	.size	__GI_authdes_pk_create, .-__GI_authdes_pk_create
	.globl	authdes_pk_create
	.set	authdes_pk_create,__GI_authdes_pk_create
	.p2align 4,,15
	.globl	__GI_authdes_create
	.hidden	__GI_authdes_create
	.type	__GI_authdes_create, @function
__GI_authdes_create:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r12
	pushq	%rbx
	movl	%esi, %ebx
	movq	%rdx, %rbp
	subq	$1040, %rsp
	leaq	16(%rsp), %r13
	movq	%r13, %rsi
	call	__GI_getpublickey
	testl	%eax, %eax
	je	.L90
	movq	%r13, 8(%rsp)
	movq	%r13, %rsi
.L88:
	movl	(%rsi), %edi
	addq	$4, %rsi
	leal	-16843009(%rdi), %eax
	notl	%edi
	andl	%edi, %eax
	andl	$-2139062144, %eax
	je	.L88
	movl	%eax, %edx
	movq	%r12, %r8
	movq	%r14, %rdi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rsi), %rdx
	movl	%eax, %ecx
	cmove	%rdx, %rsi
	addb	%al, %cl
	movl	%ebx, %edx
	sbbq	$3, %rsi
	movq	%rbp, %rcx
	subq	%r13, %rsi
	addl	$1, %esi
	movl	%esi, (%rsp)
	movq	%rsp, %rsi
	call	__GI_authdes_pk_create
	addq	$1040, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1040, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI_authdes_create, .-__GI_authdes_create
	.globl	authdes_create
	.set	authdes_create,__GI_authdes_create
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	authdes_ops, @object
	.size	authdes_ops, 40
authdes_ops:
	.quad	authdes_nextverf
	.quad	authdes_marshal
	.quad	authdes_validate
	.quad	authdes_refresh
	.quad	authdes_destroy
