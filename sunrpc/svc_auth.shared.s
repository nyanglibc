	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI__authenticate, _authenticate@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	_svcauth_null, @function
_svcauth_null:
	xorl	%eax, %eax
	ret
	.size	_svcauth_null, .-_svcauth_null
	.p2align 4,,15
	.globl	__GI__authenticate
	.hidden	__GI__authenticate
	.type	__GI__authenticate, @function
__GI__authenticate:
	movq	64(%rsi), %rax
	movl	__GI__null_auth(%rip), %edx
	movdqu	48(%rsi), %xmm0
	movq	%rax, 40(%rdi)
	movq	56(%rdi), %rax
	movups	%xmm0, 24(%rdi)
	movl	%edx, 40(%rax)
	movl	$0, 56(%rax)
	movslq	24(%rdi), %rax
	cmpl	$3, %eax
	ja	.L4
	leaq	svcauthsw(%rip), %rdx
	jmp	*(%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$2, %eax
	ret
	.size	__GI__authenticate, .-__GI__authenticate
	.globl	__EI__authenticate
	.set	__EI__authenticate,__GI__authenticate
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	svcauthsw, @object
	.size	svcauthsw, 32
svcauthsw:
	.quad	_svcauth_null
	.quad	_svcauth_unix
	.quad	_svcauth_short
	.quad	_svcauth_des
