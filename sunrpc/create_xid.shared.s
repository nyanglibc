	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_create_xid
	.type	_create_xid, @function
_create_xid:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
#APP
# 37 "create_xid.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, createxid_lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	call	__GI_getpid
	cmpl	%eax, is_initialized(%rip)
	movl	%eax, %ebx
	movq	%rsp, %rbp
	je	.L4
	movq	%rbp, %rsi
	xorl	%edi, %edi
	call	__GI___clock_gettime
	movq	(%rsp), %rdi
	xorq	8(%rsp), %rdi
	movslq	%ebx, %rax
	leaq	__rpc_lrand48_data(%rip), %rsi
	xorq	%rax, %rdi
	call	__srand48_r
	movl	%ebx, is_initialized(%rip)
.L4:
	leaq	__rpc_lrand48_data(%rip), %rdi
	movq	%rbp, %rsi
	call	__GI_lrand48_r
#APP
# 52 "create_xid.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	subl	$1, createxid_lock(%rip)
.L6:
	movq	(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, createxid_lock(%rip)
	je	.L3
	leaq	createxid_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
#APP
# 52 "create_xid.c" 1
	xchgl %eax, createxid_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L6
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	createxid_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 52 "create_xid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L6
	.size	_create_xid, .-_create_xid
	.local	__rpc_lrand48_data
	.comm	__rpc_lrand48_data,24,16
	.local	is_initialized
	.comm	is_initialized,4,4
	.local	createxid_lock
	.comm	createxid_lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__srand48_r
