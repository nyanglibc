	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_keystatus, xdr_keystatus@GLIBC_2.2.5
	.symver __EI_xdr_keybuf, xdr_keybuf@GLIBC_2.2.5
	.symver __EI_xdr_netnamestr, xdr_netnamestr@GLIBC_2.2.5
	.symver __EI_xdr_cryptkeyarg, xdr_cryptkeyarg@GLIBC_2.2.5
	.symver __EI_xdr_cryptkeyarg2, xdr_cryptkeyarg2@GLIBC_2.2.5
	.symver __EI_xdr_cryptkeyres, xdr_cryptkeyres@GLIBC_2.2.5
	.symver __EI_xdr_unixcred, xdr_unixcred@GLIBC_2.2.5
	.symver __EI_xdr_getcredres, xdr_getcredres@GLIBC_2.2.5
	.symver __EI_xdr_key_netstarg, xdr_key_netstarg@GLIBC_2.2.5
	.symver __EI_xdr_key_netstres, xdr_key_netstres@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_keystatus
	.hidden	__GI_xdr_keystatus
	.type	__GI_xdr_keystatus, @function
__GI_xdr_keystatus:
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	__GI_xdr_keystatus, .-__GI_xdr_keystatus
	.globl	__EI_xdr_keystatus
	.set	__EI_xdr_keystatus,__GI_xdr_keystatus
	.p2align 4,,15
	.globl	__GI_xdr_keybuf
	.hidden	__GI_xdr_keybuf
	.type	__GI_xdr_keybuf, @function
__GI_xdr_keybuf:
	subq	$8, %rsp
	movl	$48, %edx
	call	__GI_xdr_opaque
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	__GI_xdr_keybuf, .-__GI_xdr_keybuf
	.globl	__EI_xdr_keybuf
	.set	__EI_xdr_keybuf,__GI_xdr_keybuf
	.p2align 4,,15
	.globl	__GI_xdr_netnamestr
	.hidden	__GI_xdr_netnamestr
	.type	__GI_xdr_netnamestr, @function
__GI_xdr_netnamestr:
	subq	$8, %rsp
	movl	$255, %edx
	call	__GI_xdr_string
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	__GI_xdr_netnamestr, .-__GI_xdr_netnamestr
	.globl	__EI_xdr_netnamestr
	.set	__EI_xdr_netnamestr,__GI_xdr_netnamestr
	.p2align 4,,15
	.globl	__GI_xdr_cryptkeyarg
	.hidden	__GI_xdr_cryptkeyarg
	.type	__GI_xdr_cryptkeyarg, @function
__GI_xdr_cryptkeyarg:
	pushq	%rbp
	pushq	%rbx
	movl	$255, %edx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_string
	testl	%eax, %eax
	je	.L8
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_des_block
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_cryptkeyarg, .-__GI_xdr_cryptkeyarg
	.globl	__EI_xdr_cryptkeyarg
	.set	__EI_xdr_cryptkeyarg,__GI_xdr_cryptkeyarg
	.p2align 4,,15
	.globl	__GI_xdr_cryptkeyarg2
	.hidden	__GI_xdr_cryptkeyarg2
	.type	__GI_xdr_cryptkeyarg2, @function
__GI_xdr_cryptkeyarg2:
	pushq	%rbp
	pushq	%rbx
	movl	$255, %edx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_string
	testl	%eax, %eax
	jne	.L15
.L17:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_netobj
	testl	%eax, %eax
	je	.L17
	leaq	24(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_des_block
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_cryptkeyarg2, .-__GI_xdr_cryptkeyarg2
	.globl	__EI_xdr_cryptkeyarg2
	.set	__EI_xdr_cryptkeyarg2,__GI_xdr_cryptkeyarg2
	.p2align 4,,15
	.globl	__GI_xdr_cryptkeyres
	.hidden	__GI_xdr_cryptkeyres
	.type	__GI_xdr_cryptkeyres, @function
__GI_xdr_cryptkeyres:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L22
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L31
	movl	$1, %eax
.L22:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	4(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_des_block
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_cryptkeyres, .-__GI_xdr_cryptkeyres
	.globl	__EI_xdr_cryptkeyres
	.set	__EI_xdr_cryptkeyres,__GI_xdr_cryptkeyres
	.p2align 4,,15
	.globl	__GI_xdr_unixcred
	.hidden	__GI_xdr_unixcred
	.type	__GI_xdr_unixcred, @function
__GI_xdr_unixcred:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_u_int
	testl	%eax, %eax
	jne	.L33
.L35:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	4(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L35
	leaq	8(%rbx), %rdx
	leaq	16(%rbx), %rsi
	leaq	__GI_xdr_u_int(%rip), %r9
	movq	%rbp, %rdi
	movl	$4, %r8d
	movl	$16, %ecx
	call	__GI_xdr_array
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_unixcred, .-__GI_xdr_unixcred
	.globl	__EI_xdr_unixcred
	.set	__EI_xdr_unixcred,__GI_xdr_unixcred
	.p2align 4,,15
	.globl	__GI_xdr_getcredres
	.hidden	__GI_xdr_getcredres
	.type	__GI_xdr_getcredres, @function
__GI_xdr_getcredres:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L40
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L49
	movl	$1, %eax
.L40:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_unixcred
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_getcredres, .-__GI_xdr_getcredres
	.globl	__EI_xdr_getcredres
	.set	__EI_xdr_getcredres,__GI_xdr_getcredres
	.p2align 4,,15
	.globl	__GI_xdr_key_netstarg
	.hidden	__GI_xdr_key_netstarg
	.type	__GI_xdr_key_netstarg, @function
__GI_xdr_key_netstarg:
	pushq	%rbp
	pushq	%rbx
	movl	$48, %edx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_opaque
	testl	%eax, %eax
	jne	.L51
.L53:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	48(%rbx), %rsi
	movl	$48, %edx
	movq	%rbp, %rdi
	call	__GI_xdr_opaque
	testl	%eax, %eax
	je	.L53
	leaq	96(%rbx), %rsi
	movq	%rbp, %rdi
	movl	$255, %edx
	call	__GI_xdr_string
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_key_netstarg, .-__GI_xdr_key_netstarg
	.globl	__EI_xdr_key_netstarg
	.set	__EI_xdr_key_netstarg,__GI_xdr_key_netstarg
	.p2align 4,,15
	.globl	__GI_xdr_key_netstres
	.hidden	__GI_xdr_key_netstres
	.type	__GI_xdr_key_netstres, @function
__GI_xdr_key_netstres:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L58
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L67
	movl	$1, %eax
.L58:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_key_netstarg
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_key_netstres, .-__GI_xdr_key_netstres
	.globl	__EI_xdr_key_netstres
	.set	__EI_xdr_key_netstres,__GI_xdr_key_netstres
