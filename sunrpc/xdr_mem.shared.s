	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdrmem_create, xdrmem_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	xdrmem_destroy, @function
xdrmem_destroy:
	rep ret
	.size	xdrmem_destroy, .-xdrmem_destroy
	.p2align 4,,15
	.type	xdrmem_getpos, @function
xdrmem_getpos:
	movq	24(%rdi), %rax
	subq	32(%rdi), %rax
	ret
	.size	xdrmem_getpos, .-xdrmem_getpos
	.p2align 4,,15
	.type	xdrmem_setpos, @function
xdrmem_setpos:
	movl	40(%rdi), %eax
	movq	32(%rdi), %rdx
	movl	%esi, %esi
	addq	24(%rdi), %rax
	addq	%rdx, %rsi
	cmpq	%rax, %rsi
	ja	.L6
	cmpq	%rsi, %rdx
	jbe	.L8
.L6:
	xorl	%eax, %eax
.L4:
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	subq	%rsi, %rax
	movq	%rax, %rdx
	movl	%eax, %ecx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jne	.L4
	movq	%rsi, 24(%rdi)
	movl	%edx, 40(%rdi)
	movl	$1, %eax
	ret
	.size	xdrmem_setpos, .-xdrmem_setpos
	.p2align 4,,15
	.type	xdrmem_inline, @function
xdrmem_inline:
	movl	40(%rdi), %eax
	cmpl	%esi, %eax
	jb	.L11
	subl	%esi, %eax
	movl	%esi, %esi
	movl	%eax, 40(%rdi)
	movq	24(%rdi), %rax
	addq	%rax, %rsi
	movq	%rsi, 24(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	ret
	.size	xdrmem_inline, .-xdrmem_inline
	.p2align 4,,15
	.type	xdrmem_putint32, @function
xdrmem_putint32:
	movl	40(%rdi), %edx
	xorl	%eax, %eax
	cmpl	$3, %edx
	jbe	.L12
	subl	$4, %edx
	movq	24(%rdi), %rax
	movl	%edx, 40(%rdi)
	movl	(%rsi), %edx
	addq	$4, %rax
	bswap	%edx
	movl	%edx, -4(%rax)
	movq	%rax, 24(%rdi)
	movl	$1, %eax
.L12:
	rep ret
	.size	xdrmem_putint32, .-xdrmem_putint32
	.p2align 4,,15
	.type	xdrmem_getint32, @function
xdrmem_getint32:
	movl	40(%rdi), %edx
	xorl	%eax, %eax
	cmpl	$3, %edx
	jbe	.L15
	movq	24(%rdi), %rax
	subl	$4, %edx
	movl	%edx, 40(%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	bswap	%edx
	movl	%edx, (%rsi)
	movq	%rax, 24(%rdi)
	movl	$1, %eax
.L15:
	rep ret
	.size	xdrmem_getint32, .-xdrmem_getint32
	.p2align 4,,15
	.type	xdrmem_putlong, @function
xdrmem_putlong:
	movl	40(%rdi), %edx
	xorl	%eax, %eax
	cmpl	$3, %edx
	jbe	.L18
	subl	$4, %edx
	movq	24(%rdi), %rax
	movl	%edx, 40(%rdi)
	movq	(%rsi), %rdx
	addq	$4, %rax
	bswap	%edx
	movl	%edx, -4(%rax)
	movq	%rax, 24(%rdi)
	movl	$1, %eax
.L18:
	rep ret
	.size	xdrmem_putlong, .-xdrmem_putlong
	.p2align 4,,15
	.type	xdrmem_getlong, @function
xdrmem_getlong:
	movl	40(%rdi), %edx
	xorl	%eax, %eax
	cmpl	$3, %edx
	jbe	.L21
	subl	$4, %edx
	movl	%edx, 40(%rdi)
	movq	24(%rdi), %rdx
	movl	(%rdx), %eax
	addq	$4, %rdx
	bswap	%eax
	cltq
	movq	%rax, (%rsi)
	movq	%rdx, 24(%rdi)
	movl	$1, %eax
.L21:
	rep ret
	.size	xdrmem_getlong, .-xdrmem_getlong
	.p2align 4,,15
	.type	xdrmem_putbytes, @function
xdrmem_putbytes:
	movl	40(%rdi), %ecx
	cmpl	%edx, %ecx
	jnb	.L32
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	pushq	%rbp
	pushq	%rbx
	subl	%edx, %ecx
	movq	%rdi, %rbp
	movl	%edx, %ebx
	subq	$8, %rsp
	movl	%ecx, 40(%rdi)
	movq	24(%rdi), %rdi
	movq	%rbx, %rdx
	call	__GI_memcpy@PLT
	addq	%rbx, 24(%rbp)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	xdrmem_putbytes, .-xdrmem_putbytes
	.p2align 4,,15
	.type	xdrmem_getbytes, @function
xdrmem_getbytes:
	movl	40(%rdi), %ecx
	cmpl	%edx, %ecx
	jnb	.L41
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rax
	subl	%edx, %ecx
	movl	%edx, %ebx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	24(%rdi), %rsi
	movl	%ecx, 40(%rdi)
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	addq	%rbx, 24(%rbp)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	xdrmem_getbytes, .-xdrmem_getbytes
	.p2align 4,,15
	.globl	__GI_xdrmem_create
	.hidden	__GI_xdrmem_create
	.type	__GI_xdrmem_create, @function
__GI_xdrmem_create:
	leaq	xdrmem_ops(%rip), %rax
	movl	%ecx, (%rdi)
	movq	%rsi, 32(%rdi)
	movq	%rsi, 24(%rdi)
	movl	%edx, 40(%rdi)
	movq	%rax, 8(%rdi)
	ret
	.size	__GI_xdrmem_create, .-__GI_xdrmem_create
	.globl	__EI_xdrmem_create
	.set	__EI_xdrmem_create,__GI_xdrmem_create
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	xdrmem_ops, @object
	.size	xdrmem_ops, 80
xdrmem_ops:
	.quad	xdrmem_getlong
	.quad	xdrmem_putlong
	.quad	xdrmem_getbytes
	.quad	xdrmem_putbytes
	.quad	xdrmem_getpos
	.quad	xdrmem_setpos
	.quad	xdrmem_inline
	.quad	xdrmem_destroy
	.quad	xdrmem_getint32
	.quad	xdrmem_putint32
