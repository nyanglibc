	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_reference, xdr_reference@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"%s: %s"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_xdr_reference
	.hidden	__GI_xdr_reference
	.type	__GI_xdr_reference, @function
__GI_xdr_reference:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$16, %rsp
	movq	(%rsi), %rbx
	testq	%rbx, %rbx
	je	.L9
.L2:
	xorl	%eax, %eax
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	*%rcx
	cmpl	$2, 0(%rbp)
	je	.L10
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rbx, %rdi
	movl	%eax, 8(%rsp)
	call	free@PLT
	movq	$0, (%r12)
	movl	8(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	(%rdi), %esi
	cmpl	$1, %esi
	je	.L3
	cmpl	$2, %esi
	movl	$1, %eax
	je	.L1
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%edx, %esi
	movl	$1, %edi
	movq	%rcx, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, (%r12)
	movq	8(%rsp), %rcx
	jne	.L2
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.7361(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	xorl	%eax, %eax
	jmp	.L1
	.size	__GI_xdr_reference, .-__GI_xdr_reference
	.globl	__EI_xdr_reference
	.set	__EI_xdr_reference,__GI_xdr_reference
	.p2align 4,,15
	.globl	__GI_xdr_pointer
	.hidden	__GI_xdr_pointer
	.type	__GI_xdr_pointer, @function
__GI_xdr_pointer:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	%edx, %r12d
	movq	%rcx, %r13
	subq	$24, %rsp
	cmpq	$0, (%rsi)
	leaq	12(%rsp), %rsi
	setne	%al
	movl	%eax, 12(%rsp)
	call	__GI_xdr_bool
	testl	%eax, %eax
	je	.L11
	movl	12(%rsp), %eax
	testl	%eax, %eax
	jne	.L13
	movq	$0, (%rbx)
	movl	$1, %eax
.L11:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_reference
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI_xdr_pointer, .-__GI_xdr_pointer
	.globl	xdr_pointer
	.set	xdr_pointer,__GI_xdr_pointer
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__func__.7361, @object
	.size	__func__.7361, 14
__func__.7361:
	.string	"xdr_reference"
	.hidden	__fxprintf
