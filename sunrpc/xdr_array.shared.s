	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_array, xdr_array@GLIBC_2.2.5
	.symver __EI_xdr_vector, xdr_vector@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"%s: %s"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_xdr_array
	.hidden	__GI_xdr_array
	.type	__GI_xdr_array, @function
__GI_xdr_array:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%r8d, %r12d
	movq	%r9, %r14
	subq	$24, %rsp
	movq	(%rsi), %rbx
	movq	%rdx, %rsi
	movl	%ecx, 8(%rsp)
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L1
	movl	0(%r13), %r13d
	movl	8(%rsp), %ecx
	cmpl	%ecx, %r13d
	ja	.L3
	movl	%r13d, %eax
	mull	%r12d
	jo	.L3
	testq	%rbx, %rbx
	je	.L33
.L9:
	movl	%r12d, %eax
	testl	%r13d, %r13d
	movq	%rax, 8(%rsp)
	movl	$1, %eax
	je	.L11
.L10:
	xorl	%r12d, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L34:
	testl	%eax, %eax
	je	.L11
.L13:
	addl	$1, %r12d
	movq	%rbx, %rsi
	xorl	%eax, %eax
	movl	$-1, %edx
	movq	%rbp, %rdi
	call	*%r14
	addq	8(%rsp), %rbx
	cmpl	%r12d, %r13d
	ja	.L34
.L11:
	cmpl	$2, 0(%rbp)
	je	.L15
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	cmpl	$2, 0(%rbp)
	jne	.L1
	testq	%rbx, %rbx
	je	.L19
	movl	%r12d, %eax
	testl	%r13d, %r13d
	movq	%rax, 8(%rsp)
	movl	$1, %eax
	jne	.L10
.L15:
	movq	(%r15), %rdi
	movl	%eax, 8(%rsp)
	call	free@PLT
	movq	$0, (%r15)
	movl	8(%rsp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movl	0(%rbp), %eax
	cmpl	$1, %eax
	jne	.L35
	testl	%r13d, %r13d
	movl	$1, %eax
	je	.L1
	movl	%r12d, %eax
	movl	%r13d, %edi
	movq	%rax, %rsi
	movq	%rax, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, (%r15)
	jne	.L10
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.7233(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	$2, %eax
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %eax
	jmp	.L1
	.size	__GI_xdr_array, .-__GI_xdr_array
	.globl	__EI_xdr_array
	.set	__EI_xdr_array,__GI_xdr_array
	.p2align 4,,15
	.globl	__GI_xdr_vector
	.hidden	__GI_xdr_vector
	.type	__GI_xdr_vector, @function
__GI_xdr_vector:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testl	%edx, %edx
	movl	%ecx, 12(%rsp)
	je	.L37
	movq	%rdi, %r12
	movq	%rsi, %rbx
	movl	%edx, %ebp
	movq	%r8, %r13
	xorl	%r15d, %r15d
	movl	$-1, %r14d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L48:
	movl	12(%rsp), %eax
	addl	$1, %r15d
	addq	%rax, %rbx
	cmpl	%r15d, %ebp
	je	.L37
.L39:
	xorl	%eax, %eax
	movl	%r14d, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*%r13
	testl	%eax, %eax
	jne	.L48
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI_xdr_vector, .-__GI_xdr_vector
	.globl	__EI_xdr_vector
	.set	__EI_xdr_vector,__GI_xdr_vector
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__func__.7233, @object
	.size	__func__.7233, 10
__func__.7233:
	.string	"xdr_array"
	.hidden	__fxprintf
