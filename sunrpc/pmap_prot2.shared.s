	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_pmaplist, xdr_pmaplist@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_pmaplist
	.hidden	__GI_xdr_pmaplist
	.type	__GI_xdr_pmaplist, @function
__GI_xdr_pmaplist:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %r13d
	leaq	4(%rsp), %r12
	leaq	8(%rsp), %r14
	movq	$0, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	cmpq	$0, (%rbx)
	movq	%r12, %rsi
	movq	%rbp, %rdi
	setne	%al
	movl	%eax, 4(%rsp)
	call	__GI_xdr_bool
	testl	%eax, %eax
	je	.L3
	movl	4(%rsp), %eax
	testl	%eax, %eax
	je	.L8
	cmpl	$2, %r13d
	je	.L21
	leaq	__GI_xdr_pmap(%rip), %rcx
	movl	$40, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_reference
	testl	%eax, %eax
	je	.L3
	movq	(%rbx), %rbx
	addq	$32, %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rbx), %rax
	leaq	__GI_xdr_pmap(%rip), %rcx
	movl	$40, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movq	32(%rax), %rax
	movq	%rax, 8(%rsp)
	call	__GI_xdr_reference
	testl	%eax, %eax
	je	.L3
	movq	%r14, %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI_xdr_pmaplist, .-__GI_xdr_pmaplist
	.globl	__EI_xdr_pmaplist
	.set	__EI_xdr_pmaplist,__GI_xdr_pmaplist
