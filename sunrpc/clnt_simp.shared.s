	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_callrpc, callrpc@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_callrpc
	.hidden	__GI_callrpc
	.type	__GI_callrpc, @function
__GI_callrpc:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rcx, (%rsp)
	movq	%r8, 8(%rsp)
	call	__rpc_thread_variables
	movq	192(%rax), %r13
	testq	%r13, %r13
	je	.L29
	cmpq	$0, 40(%r13)
	je	.L30
.L4:
	cmpq	$0, 32(%r13)
	je	.L5
	cmpq	%rbp, 16(%r13)
	je	.L31
.L5:
	movl	8(%r13), %edi
	movq	$0, 32(%r13)
	cmpl	$-1, %edi
	jne	.L32
.L7:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	8(%rdi), %rax
	call	*32(%rax)
	movq	$0, 0(%r13)
.L8:
	leaq	16(%rsp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	__libc_rpc_gethostbyname
	testl	%eax, %eax
	je	.L9
.L27:
	call	__GI___rpc_thread_createerr
	movl	(%rax), %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	call	__GI___close
	movl	$-1, 8(%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	8(%r13), %r9
	xorl	%r8d, %r8d
	movl	$5, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	__GI_clntudp_create
	testq	%rax, %rax
	movq	%rax, 0(%r13)
	je	.L27
	movq	40(%r13), %rdi
	movq	$1, 32(%r13)
	movl	$255, %edx
	movq	%rbp, 16(%r13)
	movq	%r12, 24(%r13)
	movq	%rbx, %rsi
	call	__GI_strncpy
	movq	40(%r13), %rax
	movb	$0, 255(%rax)
.L6:
	movq	0(%r13), %rdi
	movq	%r15, %rcx
	movq	8(%rdi), %rax
	pushq	$0
	pushq	$25
	movq	120(%rsp), %r9
	movq	112(%rsp), %r8
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L1
	movq	$0, 32(%r13)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%r12, 24(%r13)
	jne	.L5
	movq	40(%r13), %rdi
	movq	%rbx, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L6
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$48, %esi
	movl	$1, %edi
	movq	%rax, %r14
	call	calloc@PLT
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1
	cmpq	$0, 40(%r13)
	movq	%r13, 192(%r14)
	jne	.L4
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$256, %edi
	call	malloc@PLT
	movq	%rax, 40(%r13)
	movb	$0, (%rax)
	movl	$-1, 8(%r13)
	jmp	.L4
	.size	__GI_callrpc, .-__GI_callrpc
	.globl	__EI_callrpc
	.set	__EI_callrpc,__GI_callrpc
	.p2align 4,,15
	.globl	__rpc_thread_clnt_cleanup
	.hidden	__rpc_thread_clnt_cleanup
	.type	__rpc_thread_clnt_cleanup, @function
__rpc_thread_clnt_cleanup:
	pushq	%rbx
	call	__rpc_thread_variables
	movq	192(%rax), %rbx
	testq	%rbx, %rbx
	je	.L33
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L35
	movq	8(%rdi), %rax
	call	*32(%rax)
.L35:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	popq	%rbx
	ret
	.size	__rpc_thread_clnt_cleanup, .-__rpc_thread_clnt_cleanup
	.hidden	__libc_rpc_gethostbyname
	.hidden	__rpc_thread_variables
