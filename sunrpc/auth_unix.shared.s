	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_authunix_create, authunix_create@GLIBC_2.2.5
	.symver __EI_authunix_create_default, authunix_create_default@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	authunix_nextverf, @function
authunix_nextverf:
.LFB65:
	rep ret
.LFE65:
	.size	authunix_nextverf, .-authunix_nextverf
	.p2align 4,,15
	.type	authunix_marshal, @function
authunix_marshal:
.LFB66:
	movq	%rsi, %rax
	movq	64(%rdi), %rsi
	movq	8(%rax), %rcx
	movq	%rax, %rdi
	movl	456(%rsi), %edx
	addq	$56, %rsi
	movq	24(%rcx), %rcx
	jmp	*%rcx
.LFE66:
	.size	authunix_marshal, .-authunix_marshal
	.p2align 4,,15
	.type	authunix_destroy, @function
authunix_destroy:
.LFB69:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	64(%rdi), %rbp
	movq	8(%rbp), %rdi
	call	free@PLT
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	free@PLT
.L5:
	movq	64(%rbx), %rdi
	call	free@PLT
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L6
	call	free@PLT
.L6:
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
.LFE69:
	.size	authunix_destroy, .-authunix_destroy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"auth_unix.c: Fatal marshalling problem"
	.text
	.p2align 4,,15
	.type	marshal_new_auth, @function
marshal_new_auth:
.LFB70:
	pushq	%r12
	pushq	%rbp
	xorl	%ecx, %ecx
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$400, %edx
	subq	$48, %rsp
	movq	64(%rdi), %r12
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	leaq	56(%r12), %rsi
	call	__GI_xdrmem_create
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_opaque_auth
	testl	%eax, %eax
	je	.L17
	leaq	24(%rbp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_opaque_auth
	testl	%eax, %eax
	je	.L17
	movq	8(%rsp), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	%eax, 456(%r12)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
.L16:
	movq	8(%rsp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L18
	movq	%rbx, %rdi
	call	*%rax
.L18:
	addq	$48, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.LFE70:
	.size	marshal_new_auth, .-marshal_new_auth
	.p2align 4,,15
	.type	authunix_refresh, @function
authunix_refresh:
.LFB68:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$112, %rsp
	movq	64(%rdi), %rbx
	movq	8(%rbx), %rsi
	cmpq	%rsi, 8(%rdi)
	je	.L26
	addq	$1, 48(%rbx)
	movl	16(%rbx), %edx
	leaq	64(%rsp), %r13
	leaq	16(%rsp), %r14
	movl	$1, %ecx
	movq	%rdi, %rbp
	movq	%r13, %rdi
	movq	$0, 24(%rsp)
	movq	$0, 48(%rsp)
	call	__GI_xdrmem_create
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__GI_xdr_authunix_parms
	testl	%eax, %eax
	movl	%eax, %r12d
	jne	.L40
.L28:
	movq	%r14, %rsi
	movq	%r13, %rdi
	movl	$2, 64(%rsp)
	call	__GI_xdr_authunix_parms
	movq	72(%rsp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L26
	movq	%r13, %rdi
	call	*%rax
.L26:
	addq	$112, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%edi, %edi
	movq	%rsp, %rsi
	call	__GI___clock_gettime
	movq	(%rsp), %rax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	$0, 64(%rsp)
	movq	%rax, 16(%rsp)
	movq	72(%rsp), %rax
	call	*40(%rax)
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__GI_xdr_authunix_parms
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L28
	movdqu	(%rbx), %xmm0
	movq	%rbp, %rdi
	movq	16(%rbx), %rax
	movups	%xmm0, 0(%rbp)
	movq	%rax, 16(%rbp)
	call	marshal_new_auth
	jmp	.L28
.LFE68:
	.size	authunix_refresh, .-authunix_refresh
	.p2align 4,,15
	.type	authunix_validate, @function
authunix_validate:
.LFB67:
	cmpl	$2, (%rsi)
	je	.L53
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	pushq	%r13
	pushq	%r12
	movl	$1, %ecx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$56, %rsp
	movq	64(%rdi), %rbx
	movl	16(%rsi), %edx
	movq	8(%rsi), %rsi
	movq	%rsp, %r12
	movq	%r12, %rdi
	call	__GI_xdrmem_create
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L43
	call	free@PLT
	movq	$0, 32(%rbx)
.L43:
	leaq	24(%rbx), %r13
	movq	%r12, %rdi
	movq	%r13, %rsi
	call	__GI_xdr_opaque_auth
	testl	%eax, %eax
	je	.L44
	movdqu	24(%rbx), %xmm0
	movq	40(%rbx), %rax
	movups	%xmm0, 0(%rbp)
	movq	%rax, 16(%rbp)
.L45:
	movq	%rbp, %rdi
	call	marshal_new_auth
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$2, (%rsp)
	call	__GI_xdr_opaque_auth
	movq	16(%rbx), %rax
	movq	$0, 32(%rbx)
	movdqu	(%rbx), %xmm0
	movq	%rax, 16(%rbp)
	movups	%xmm0, 0(%rbp)
	jmp	.L45
.LFE67:
	.size	authunix_validate, .-authunix_validate
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"out of memory\n"
.LC2:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI_authunix_create
	.hidden	__GI_authunix_create
	.type	__GI_authunix_create, @function
__GI_authunix_create:
.LFB63:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	$72, %edi
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %r14d
	movl	%ecx, %r12d
	movq	%r8, %rbp
	subq	$536, %rsp
	movl	%edx, 12(%rsp)
	call	malloc@PLT
	movl	$464, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rbx, %rbx
	movq	%rax, %r13
	je	.L59
	testq	%rax, %rax
	je	.L59
	leaq	auth_unix_ops(%rip), %rax
	movdqu	__GI__null_auth(%rip), %xmm0
	leaq	16(%rsp), %rsi
	xorl	%edi, %edi
	movq	%r13, 64(%rbx)
	movq	%rax, 56(%rbx)
	movq	16+__GI__null_auth(%rip), %rax
	movups	%xmm0, 24(%rbx)
	movq	$0, 48(%r13)
	movups	%xmm0, 24(%r13)
	movq	%rax, 40(%rbx)
	movq	%rax, 40(%r13)
	call	__GI___clock_gettime
	movq	16(%rsp), %rax
	movl	%r12d, 56(%rsp)
	leaq	128(%rsp), %r12
	movq	%rbp, 64(%rsp)
	leaq	80(%rsp), %rbp
	xorl	%ecx, %ecx
	movl	$400, %edx
	movq	%r12, %rsi
	movq	%r15, 40(%rsp)
	movq	%rax, 32(%rsp)
	movl	12(%rsp), %eax
	movq	%rbp, %rdi
	movl	%r14d, 48(%rsp)
	movl	%eax, 52(%rsp)
	call	__GI_xdrmem_create
	leaq	32(%rsp), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_authunix_parms
	testl	%eax, %eax
	je	.L64
	movq	88(%rsp), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	%eax, %ebp
	movl	%eax, 16(%r13)
	movl	$1, 0(%r13)
	movq	%rbp, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r13)
	je	.L59
	movq	%rax, %rdi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	call	__GI_memcpy@PLT
	movdqu	0(%r13), %xmm0
	movq	%rbx, %rdi
	movq	16(%r13), %rax
	movups	%xmm0, (%rbx)
	movq	%rax, 16(%rbx)
	call	marshal_new_auth
.L54:
	addq	$536, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	.LC1(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.8733(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L54
.L64:
	call	__GI_abort
.LFE63:
	.size	__GI_authunix_create, .-__GI_authunix_create
	.globl	__EI_authunix_create
	.set	__EI_authunix_create,__GI_authunix_create
	.p2align 4,,15
	.globl	__GI_authunix_create_default
	.hidden	__GI_authunix_create_default
	.type	__GI_authunix_create_default, @function
__GI_authunix_create_default:
.LFB64:
	pushq	%rbp
	movl	$255, %esi
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	leaq	-304(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	movq	%r13, %rdi
	subq	$280, %rsp
	call	__gethostname
	cmpl	$-1, %eax
	je	.L72
	movb	$0, -49(%rbp)
	xorl	%r12d, %r12d
	call	__geteuid
	movl	%eax, -312(%rbp)
	call	__getegid
	movl	%eax, %r15d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L90:
	cmpb	$1, %r12b
	je	.L68
	cltq
	leaq	30(,%rax,4), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
.L69:
	movq	%rbx, %rsi
	movl	%r14d, %edi
	call	__getgroups
	cmpl	$-1, %eax
	jne	.L71
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$22, %fs:(%rax)
	jne	.L72
	cmpl	$255, %r14d
	seta	%al
	orl	%r12d, %eax
	movl	$1, %r12d
	testb	%al, %al
	jne	.L89
.L73:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__getgroups
	cmpl	$255, %eax
	movl	%eax, %r14d
	jbe	.L90
.L68:
	movslq	%r14d, %rdi
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L69
	xorl	%eax, %eax
.L65:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	-312(%rbp), %esi
	cmpl	$16, %eax
	movl	$16, %ecx
	cmovle	%eax, %ecx
	movq	%rbx, %r8
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	__GI_authunix_create
	cmpl	$255, %r14d
	ja	.L76
	testb	%r12b, %r12b
	je	.L65
.L76:
	movq	%rbx, %rdi
	movq	%rax, -312(%rbp)
	call	free@PLT
	movq	-312(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L89:
.L67:
	movq	%rbx, %rdi
	movb	%al, -313(%rbp)
	call	free@PLT
	movzbl	-313(%rbp), %eax
	movl	%eax, %r12d
	jmp	.L73
.L72:
	call	__GI_abort
.LFE64:
	.size	__GI_authunix_create_default, .-__GI_authunix_create_default
	.globl	__EI_authunix_create_default
	.set	__EI_authunix_create_default,__GI_authunix_create_default
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__func__.8733, @object
	.size	__func__.8733, 16
__func__.8733:
	.string	"authunix_create"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	auth_unix_ops, @object
	.size	auth_unix_ops, 40
auth_unix_ops:
	.quad	authunix_nextverf
	.quad	authunix_marshal
	.quad	authunix_validate
	.quad	authunix_refresh
	.quad	authunix_destroy
	.hidden	__getgroups
	.hidden	__getegid
	.hidden	__geteuid
	.hidden	__gethostname
	.hidden	__fxprintf
