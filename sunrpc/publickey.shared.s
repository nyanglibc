	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_getpublickey, getpublickey@GLIBC_2.2.5
	.symver __EI_getsecretkey, getsecretkey@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getpublickey"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_getpublickey
	.hidden	__GI_getpublickey
	.type	__GI_getpublickey, @function
__GI_getpublickey:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbp
	pushq	%rbx
	xorl	%edx, %edx
	movq	%rdi, %r14
	subq	$24, %rsp
	leaq	8(%rsp), %r12
	movq	%rsp, %r13
	movq	%r13, %rdi
	movq	%r12, %rcx
	call	__GI___nss_publickey_lookup2
	testl	%eax, %eax
	jne	.L4
	movq	__libc_errno@gottpoff(%rip), %rbp
	addq	%fs:0, %rbp
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	*8(%rsp)
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%r12, %rcx
	movq	%r13, %rdi
	movl	%eax, %ebx
	call	__GI___nss_next2
	testl	%eax, %eax
	je	.L3
	xorl	%eax, %eax
	cmpl	$1, %ebx
	sete	%al
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI_getpublickey, .-__GI_getpublickey
	.globl	__EI_getpublickey
	.set	__EI_getpublickey,__GI_getpublickey
	.section	.rodata.str1.1
.LC1:
	.string	"getsecretkey"
	.text
	.p2align 4,,15
	.globl	__GI_getsecretkey
	.hidden	__GI_getsecretkey
	.type	__GI_getsecretkey, @function
__GI_getsecretkey:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	leaq	.LC1(%rip), %rsi
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rdx, %r15
	xorl	%edx, %edx
	subq	$40, %rsp
	leaq	24(%rsp), %rbp
	leaq	16(%rsp), %r12
	movq	%rbp, %rcx
	movq	%r12, %rdi
	call	__GI___nss_publickey_lookup2
	testl	%eax, %eax
	jne	.L12
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L11:
	movq	8(%rsp), %rcx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*24(%rsp)
	leaq	.LC1(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%rbp, %rcx
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	__GI___nss_next2
	testl	%eax, %eax
	je	.L11
	xorl	%eax, %eax
	cmpl	$1, %ebx
	sete	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI_getsecretkey, .-__GI_getsecretkey
	.globl	__EI_getsecretkey
	.set	__EI_getsecretkey,__GI_getsecretkey
