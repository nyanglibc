	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI__rpc_dtablesize, _rpc_dtablesize@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI__rpc_dtablesize
	.hidden	__GI__rpc_dtablesize
	.type	__GI__rpc_dtablesize, @function
__GI__rpc_dtablesize:
	movl	size.7750(%rip), %eax
	testl	%eax, %eax
	je	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	subq	$8, %rsp
	call	__getdtablesize
	movl	%eax, size.7750(%rip)
	addq	$8, %rsp
	ret
	.size	__GI__rpc_dtablesize, .-__GI__rpc_dtablesize
	.globl	__EI__rpc_dtablesize
	.set	__EI__rpc_dtablesize,__GI__rpc_dtablesize
	.local	size.7750
	.comm	size.7750,4,4
	.hidden	__getdtablesize
