	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_clnt_sperror, clnt_sperror@GLIBC_2.2.5
	.symver __EI_clnt_spcreateerror, clnt_spcreateerror@GLIBC_2.2.5
#NO_APP
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
.LFB73:
	subq	$8, %rsp
	call	__rpc_thread_variables
	movq	176(%rax), %rdi
	addq	$8, %rsp
	jmp	free@PLT
.LFE73:
	.size	free_mem, .-free_mem
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"RPC: (unknown error code)"
	.text
	.p2align 4,,15
	.globl	__GI_clnt_sperrno
	.hidden	__GI_clnt_sperrno
	.type	__GI_clnt_sperrno, @function
__GI_clnt_sperrno:
.LFB68:
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	rpc_errlist(%rip), %rcx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$1, %rax
	cmpq	$18, %rax
	je	.L6
	movl	(%rcx,%rax,8), %edx
.L7:
	cmpl	%edx, %edi
	jne	.L5
	leaq	rpc_errlist(%rip), %rdx
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	4(%rdx,%rax,8), %esi
	leaq	rpc_errstr(%rip), %rax
	movl	$5, %edx
	addq	%rax, %rsi
	jmp	__GI___dcgettext
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	jmp	__GI___dcgettext
.LFE68:
	.size	__GI_clnt_sperrno, .-__GI_clnt_sperrno
	.globl	clnt_sperrno
	.set	clnt_sperrno,__GI_clnt_sperrno
	.section	.rodata.str1.1
.LC1:
	.string	"%s: %s\n"
.LC2:
	.string	"%s: %s; errno = %s\n"
.LC3:
	.string	"%s: %s; why = %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"%s: %s; why = (unknown authentication error - %d)\n"
	.align 8
.LC5:
	.string	"%s: %s; low version = %lu, high version = %lu"
	.section	.rodata.str1.1
.LC6:
	.string	"%s: %s; s1 = %lu, s2 = %lu"
	.text
	.p2align 4,,15
	.globl	__GI_clnt_sperror
	.hidden	__GI_clnt_sperror
	.type	__GI_clnt_sperror, @function
__GI_clnt_sperror:
.LFB66:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$1080, %rsp
	movq	8(%rdi), %rax
	leaq	16(%rsp), %rsi
	call	*16(%rax)
	movl	16(%rsp), %edi
	call	__GI_clnt_sperrno
	cmpl	$17, 16(%rsp)
	movq	%rax, %rbp
	ja	.L9
	movl	16(%rsp), %eax
	leaq	.L11(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L11:
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L12-.L11
	.long	.L12-.L11
	.long	.L10-.L11
	.long	.L13-.L11
	.long	.L14-.L11
	.long	.L10-.L11
	.long	.L13-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.long	.L10-.L11
	.text
	.p2align 4,,10
	.p2align 3
.L13:
	movq	32(%rsp), %r13
	movq	24(%rsp), %r12
	leaq	.LC5(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movq	%r13, %r9
	movq	%r12, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
.L15:
	testl	%eax, %eax
	js	.L22
.L28:
	call	__rpc_thread_variables
	movq	8(%rsp), %rdx
	movq	176(%rax), %rdi
	movq	%rdx, 176(%rax)
	call	free@PLT
	movq	8(%rsp), %rax
	addq	$1080, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	24(%rsp), %edi
	leaq	48(%rsp), %rsi
	movl	$1024, %edx
	call	__GI___strerror_r
	leaq	8(%rsp), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, %r8
	movq	%rbp, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L28
.L22:
	addq	$1080, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	24(%rsp), %r12d
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	auth_errlist(%rip), %rcx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	movl	(%rcx,%rax,8), %edx
.L20:
	cmpl	%edx, %r12d
	je	.L29
	addq	$1, %rax
	cmpq	$8, %rax
	jne	.L30
.L19:
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movl	%r12d, %r8d
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	8(%rsp), %rdi
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L9:
	movq	32(%rsp), %r9
	movq	24(%rsp), %r8
	leaq	8(%rsp), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	auth_errlist(%rip), %rdx
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	4(%rdx,%rax,8), %esi
	leaq	auth_errstr(%rip), %rax
	movl	$5, %edx
	addq	%rax, %rsi
	call	__GI___dcgettext
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L31
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movq	%r12, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
	jmp	.L15
.L31:
	movl	24(%rsp), %r12d
	jmp	.L19
.LFE66:
	.size	__GI_clnt_sperror, .-__GI_clnt_sperror
	.globl	__EI_clnt_sperror
	.set	__EI_clnt_sperror,__GI_clnt_sperror
	.section	.rodata.str1.1
.LC7:
	.string	"%s"
	.text
	.p2align 4,,15
	.globl	__GI_clnt_perror
	.hidden	__GI_clnt_perror
	.type	__GI_clnt_perror, @function
__GI_clnt_perror:
.LFB67:
	subq	$8, %rsp
	call	__GI_clnt_sperror
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	jmp	__fxprintf
.LFE67:
	.size	__GI_clnt_perror, .-__GI_clnt_perror
	.globl	clnt_perror
	.set	clnt_perror,__GI_clnt_perror
	.p2align 4,,15
	.globl	__GI_clnt_perrno
	.hidden	__GI_clnt_perrno
	.type	__GI_clnt_perrno, @function
__GI_clnt_perrno:
.LFB69:
	subq	$8, %rsp
	call	__GI_clnt_sperrno
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	jmp	__fxprintf
.LFE69:
	.size	__GI_clnt_perrno, .-__GI_clnt_perrno
	.globl	clnt_perrno
	.set	clnt_perrno,__GI_clnt_perrno
	.section	.rodata.str1.1
.LC8:
	.string	" - "
.LC9:
	.string	""
.LC10:
	.string	"%s: %s%s%s\n"
	.text
	.p2align 4,,15
	.globl	__GI_clnt_spcreateerror
	.hidden	__GI_clnt_spcreateerror
	.type	__GI_clnt_spcreateerror, @function
__GI_clnt_spcreateerror:
.LFB70:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	subq	$1040, %rsp
	call	__GI___rpc_thread_createerr
	movl	(%rax), %edi
	movq	%rax, %rbx
	cmpl	$12, %edi
	je	.L38
	cmpl	$14, %edi
	je	.L45
	leaq	.LC9(%rip), %rbp
	movq	%rbp, %rbx
.L37:
	call	__GI_clnt_sperrno
	leaq	8(%rsp), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rcx
	movq	%rbp, %r9
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%r12, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L42
	call	__rpc_thread_variables
	movq	8(%rsp), %rdx
	movq	176(%rax), %rdi
	movq	%rdx, 176(%rax)
	call	free@PLT
	movq	8(%rsp), %rax
	addq	$1040, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movl	8(%rax), %edi
	call	__GI_clnt_sperrno
	movl	(%rbx), %edi
	movq	%rax, %rbp
	leaq	.LC8(%rip), %rbx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movl	16(%rax), %edi
	leaq	16(%rsp), %rsi
	movl	$1024, %edx
	call	__GI___strerror_r
	movl	(%rbx), %edi
	movq	%rax, %rbp
	leaq	.LC8(%rip), %rbx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$1040, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.LFE70:
	.size	__GI_clnt_spcreateerror, .-__GI_clnt_spcreateerror
	.globl	__EI_clnt_spcreateerror
	.set	__EI_clnt_spcreateerror,__GI_clnt_spcreateerror
	.p2align 4,,15
	.globl	__GI_clnt_pcreateerror
	.hidden	__GI_clnt_pcreateerror
	.type	__GI_clnt_pcreateerror, @function
__GI_clnt_pcreateerror:
.LFB71:
	subq	$8, %rsp
	call	__GI_clnt_spcreateerror
	leaq	.LC7(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	jmp	__fxprintf
.LFE71:
	.size	__GI_clnt_pcreateerror, .-__GI_clnt_pcreateerror
	.globl	clnt_pcreateerror
	.set	clnt_pcreateerror,__GI_clnt_pcreateerror
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.section	.rodata
	.align 32
	.type	auth_errlist, @object
	.size	auth_errlist, 64
auth_errlist:
	.long	0
	.long	0
	.long	1
	.long	18
	.long	2
	.long	44
	.long	3
	.long	71
	.long	4
	.long	95
	.long	5
	.long	120
	.long	6
	.long	147
	.long	7
	.long	171
	.align 32
	.type	auth_errstr, @object
	.size	auth_errstr, 198
auth_errstr:
	.string	"Authentication OK"
	.string	"Invalid client credential"
	.string	"Server rejected credential"
	.string	"Invalid client verifier"
	.string	"Server rejected verifier"
	.string	"Client credential too weak"
	.string	"Invalid server verifier"
	.string	"Failed (unspecified error)"
	.align 32
	.type	rpc_errlist, @object
	.size	rpc_errlist, 144
rpc_errlist:
	.long	0
	.long	0
	.long	1
	.long	13
	.long	2
	.long	41
	.long	3
	.long	66
	.long	4
	.long	86
	.long	5
	.long	109
	.long	6
	.long	124
	.long	7
	.long	158
	.long	8
	.long	184
	.long	9
	.long	209
	.long	10
	.long	239
	.long	11
	.long	266
	.long	12
	.long	301
	.long	13
	.long	326
	.long	17
	.long	344
	.long	14
	.long	366
	.long	15
	.long	391
	.long	16
	.long	419
	.align 32
	.type	rpc_errstr, @object
	.size	rpc_errstr, 451
rpc_errstr:
	.string	"RPC: Success"
	.string	"RPC: Can't encode arguments"
	.string	"RPC: Can't decode result"
	.string	"RPC: Unable to send"
	.string	"RPC: Unable to receive"
	.string	"RPC: Timed out"
	.string	"RPC: Incompatible versions of RPC"
	.string	"RPC: Authentication error"
	.string	"RPC: Program unavailable"
	.string	"RPC: Program/version mismatch"
	.string	"RPC: Procedure unavailable"
	.string	"RPC: Server can't decode arguments"
	.string	"RPC: Remote system error"
	.string	"RPC: Unknown host"
	.string	"RPC: Unknown protocol"
	.string	"RPC: Port mapper failure"
	.string	"RPC: Program not registered"
	.string	"RPC: Failed (unspecified error)"
	.hidden	__fxprintf
	.hidden	__rpc_thread_variables
