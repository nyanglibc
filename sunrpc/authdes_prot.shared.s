	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_authdes_cred, xdr_authdes_cred@GLIBC_2.2.5
	.symver __EI_xdr_authdes_verf, xdr_authdes_verf@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_authdes_cred
	.hidden	__GI_xdr_authdes_cred
	.type	__GI_xdr_authdes_cred, @function
__GI_xdr_authdes_cred:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L14
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L4
	cmpl	$1, %eax
	jne	.L14
	leaq	32(%rbx), %rsi
.L16:
	movq	%rbp, %rdi
	movl	$4, %edx
	call	__GI_xdr_opaque
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	8(%rbx), %rsi
	movl	$255, %edx
	movq	%rbp, %rdi
	call	__GI_xdr_string
	testl	%eax, %eax
	je	.L14
	leaq	16(%rbx), %rsi
	movl	$8, %edx
	movq	%rbp, %rdi
	call	__GI_xdr_opaque
	testl	%eax, %eax
	leaq	24(%rbx), %rsi
	jne	.L16
.L14:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_authdes_cred, .-__GI_xdr_authdes_cred
	.globl	__EI_xdr_authdes_cred
	.set	__EI_xdr_authdes_cred,__GI_xdr_authdes_cred
	.p2align 4,,15
	.globl	__GI_xdr_authdes_verf
	.hidden	__GI_xdr_authdes_verf
	.type	__GI_xdr_authdes_verf, @function
__GI_xdr_authdes_verf:
	pushq	%rbp
	pushq	%rbx
	movl	$8, %edx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_opaque
	testl	%eax, %eax
	je	.L18
	leaq	8(%rbx), %rsi
	movl	$4, %edx
	movq	%rbp, %rdi
	call	__GI_xdr_opaque
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_authdes_verf, .-__GI_xdr_authdes_verf
	.globl	__EI_xdr_authdes_verf
	.set	__EI_xdr_authdes_verf,__GI_xdr_authdes_verf
