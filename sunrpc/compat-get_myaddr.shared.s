	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"get_myaddress: getifaddrs"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_get_myaddress
	.hidden	__GI_get_myaddress
	.type	__GI_get_myaddress, @function
__GI_get_myaddress:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	leaq	8(%rsp), %rdi
	call	__GI_getifaddrs
	testl	%eax, %eax
	jne	.L25
.L2:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L3
.L7:
	movl	%eax, %r8d
	movq	%rdi, %rdx
	andl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L6:
	movl	16(%rdx), %ecx
	testb	$1, %cl
	je	.L4
	movq	24(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L4
	cmpw	$2, (%rsi)
	je	.L26
.L4:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L6
	testl	%eax, %eax
	jne	.L3
	movl	$1, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L26:
	andl	$8, %ecx
	je	.L9
	testb	%r8b, %r8b
	je	.L4
.L9:
	movdqu	(%rsi), %xmm0
	movl	$28416, %eax
	movups	%xmm0, (%rbx)
	movw	%ax, 2(%rbx)
.L3:
	call	__GI_freeifaddrs
	addq	$16, %rsp
	popq	%rbx
	ret
.L25:
	leaq	.LC0(%rip), %rdi
	call	__GI_perror
	movl	$1, %edi
	call	__GI_exit
	.size	__GI_get_myaddress, .-__GI_get_myaddress
	.globl	get_myaddress
	.set	get_myaddress,__GI_get_myaddress
