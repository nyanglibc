	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_key_setsecret, key_setsecret@GLIBC_2.2.5
	.symver __EI_key_secretkey_is_set, key_secretkey_is_set@GLIBC_2.2.5
	.symver __EI_key_encryptsession, key_encryptsession@GLIBC_2.2.5
	.symver __EI_key_decryptsession, key_decryptsession@GLIBC_2.2.5
	.symver __EI_key_encryptsession_pk, key_encryptsession_pk@GLIBC_2.2.5
	.symver __EI_key_decryptsession_pk, key_decryptsession_pk@GLIBC_2.2.5
	.symver __EI_key_gendes, key_gendes@GLIBC_2.2.5
	.symver __EI_key_setnet, key_setnet@GLIBC_2.2.5
	.symver __EI_key_get_conv, key_get_conv@GLIBC_2.2.5
	.symver __key_encryptsession_pk_LOCAL,__key_encryptsession_pk_LOCAL@GLIBC_2.2.5
	.symver __key_decryptsession_pk_LOCAL,__key_decryptsession_pk_LOCAL@GLIBC_2.2.5
	.symver __key_gendes_LOCAL,__key_gendes_LOCAL@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"unix"
.LC2:
	.string	"/var/run/keyservsock"
#NO_APP
	.text
	.p2align 4,,15
	.type	getkeyserv_handle, @function
getkeyserv_handle:
.LFB73:
	pushq	%rbp
	pushq	%rbx
	subq	$168, %rsp
	movl	%edi, 12(%rsp)
	call	__rpc_thread_variables
	movq	200(%rax), %rbx
	movl	$110, 28(%rsp)
	testq	%rbx, %rbx
	je	.L28
	cmpq	$0, (%rbx)
	je	.L6
	movl	8(%rbx), %ebp
	call	__GI___getpid
	cmpl	%eax, %ebp
	je	.L7
.L24:
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movq	56(%rdi), %rax
	call	*32(%rax)
	movq	(%rbx), %rdi
	movq	8(%rdi), %rax
	call	*32(%rax)
	movq	$0, (%rbx)
.L6:
	movslq	12(%rsp), %rdx
	leaq	.LC1(%rip), %rcx
	leaq	.LC2(%rip), %rdi
	movl	$100029, %esi
	call	__GI_clnt_create
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L25
	call	__geteuid
	movl	%eax, 12(%rbx)
	call	__GI___getpid
	movl	12(%rbx), %esi
	leaq	.LC0(%rip), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%eax, 8(%rbx)
	movq	(%rbx), %rbp
	call	__GI_authunix_create
	movq	(%rbx), %rdi
	movq	%rax, 0(%rbp)
	cmpq	$0, (%rdi)
	je	.L26
	movq	8(%rdi), %rax
	leaq	32(%rsp), %rdx
	movl	$4, %esi
	movq	$6, 32(%rsp)
	movq	$0, 40(%rsp)
	call	*40(%rax)
	movq	(%rbx), %rdi
	leaq	24(%rsp), %rdx
	movl	$6, %esi
	movq	8(%rdi), %rax
	call	*40(%rax)
	testl	%eax, %eax
	jne	.L29
.L13:
	movq	(%rbx), %rax
.L1:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	24(%rsp), %edi
	movl	$1, %edx
	movl	$2, %esi
	xorl	%eax, %eax
	call	__GI___fcntl
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L30:
	call	__geteuid
	movl	%eax, 12(%rbx)
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movq	56(%rdi), %rax
	call	*32(%rax)
	movl	12(%rbx), %esi
	leaq	.LC0(%rip), %rdi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	(%rbx), %rbp
	call	__GI_authunix_create
	movq	(%rbx), %rdi
	movq	%rax, 0(%rbp)
	cmpq	$0, (%rdi)
	jne	.L10
.L26:
	movq	8(%rdi), %rax
	call	*32(%rax)
	movq	$0, (%rbx)
.L25:
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$16, %edi
	movq	%rax, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L25
	movq	%rax, 200(%rbp)
	movq	$0, (%rax)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L6
	movq	8(%rdi), %rax
	leaq	24(%rsp), %rdx
	movl	$6, %esi
	call	*40(%rax)
	movl	24(%rsp), %edi
	leaq	48(%rsp), %rsi
	leaq	28(%rsp), %rdx
	call	__getpeername
	cmpl	$-1, %eax
	je	.L24
	cmpq	$0, (%rbx)
	je	.L6
	movl	12(%rbx), %ebp
	call	__geteuid
	cmpl	%eax, %ebp
	jne	.L30
	movq	(%rbx), %rdi
.L10:
	movq	8(%rdi), %rax
	leaq	12(%rsp), %rdx
	movl	$13, %esi
	call	*40(%rax)
	movq	(%rbx), %rax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE73:
	.size	getkeyserv_handle, .-getkeyserv_handle
	.p2align 4,,15
	.type	key_call_socket, @function
key_call_socket:
.LFB74:
	pushq	%r14
	movq	%r8, %r14
	pushq	%r13
	movq	%rcx, %r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 497 "key_call.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L32
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, keycall_lock(%rip)
# 0 "" 2
#NO_APP
.L33:
	leaq	-6(%rbx), %rax
	cmpq	$4, %rax
	jbe	.L34
	movl	$1, %edi
	call	getkeyserv_handle
.L35:
	testq	%rax, %rax
	je	.L39
	movq	8(%rax), %r10
	movq	%r13, %r8
	pushq	$0
	movq	%r12, %rcx
	pushq	$30
	movq	%rbp, %rdx
	movq	%r14, %r9
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	*(%r10)
	popq	%rdx
	popq	%rcx
	xorl	%r8d, %r8d
	testl	%eax, %eax
	sete	%r8b
.L36:
#APP
# 515 "key_call.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L37
	subl	$1, keycall_lock(%rip)
.L31:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$2, %edi
	call	getkeyserv_handle
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, keycall_lock(%rip)
	je	.L33
	leaq	keycall_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
#APP
# 515 "key_call.c" 1
	xchgl %eax, keycall_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L31
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	keycall_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 515 "key_call.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r8d, %r8d
	jmp	.L36
.LFE74:
	.size	key_call_socket, .-key_call_socket
	.p2align 4,,15
	.globl	__GI_key_setsecret
	.hidden	__GI_key_setsecret
	.type	__GI_key_setsecret, @function
__GI_key_setsecret:
.LFB64:
	subq	$24, %rsp
	leaq	__GI_xdr_keystatus(%rip), %rcx
	leaq	__GI_xdr_keybuf(%rip), %rsi
	leaq	12(%rsp), %r8
	movq	%rdi, %rdx
	movl	$1, %edi
	call	key_call_socket
	testl	%eax, %eax
	je	.L43
	movl	12(%rsp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	negl	%eax
.L41:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$-1, %eax
	jmp	.L41
.LFE64:
	.size	__GI_key_setsecret, .-__GI_key_setsecret
	.globl	__EI_key_setsecret
	.set	__EI_key_setsecret,__GI_key_setsecret
	.p2align 4,,15
	.globl	__GI_key_secretkey_is_set
	.hidden	__GI_key_secretkey_is_set
	.type	__GI_key_secretkey_is_set, @function
__GI_key_secretkey_is_set:
.LFB65:
	subq	$120, %rsp
	xorl	%eax, %eax
	movl	$14, %ecx
	movq	%rsp, %r8
	leaq	__GI_xdr_void(%rip), %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	rep stosq
	leaq	__GI_xdr_key_netstres(%rip), %rcx
	movl	$9, %edi
	call	key_call_socket
	testl	%eax, %eax
	je	.L45
	movl	(%rsp), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jne	.L45
	xorl	%eax, %eax
	cmpb	$0, 8(%rsp)
	setne	%al
.L45:
	addq	$120, %rsp
	ret
.LFE65:
	.size	__GI_key_secretkey_is_set, .-__GI_key_secretkey_is_set
	.globl	__EI_key_secretkey_is_set
	.set	__EI_key_secretkey_is_set,__GI_key_secretkey_is_set
	.p2align 4,,15
	.globl	__GI_key_encryptsession
	.hidden	__GI_key_encryptsession
	.type	__GI_key_encryptsession, @function
__GI_key_encryptsession:
.LFB66:
	pushq	%rbx
	leaq	__GI_xdr_cryptkeyres(%rip), %rcx
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	(%rsi), %rax
	leaq	__GI_xdr_cryptkeyarg(%rip), %rsi
	leaq	16(%rsp), %rdx
	leaq	4(%rsp), %r8
	movq	%rdi, 16(%rsp)
	movl	$2, %edi
	movq	%rax, 24(%rsp)
	call	key_call_socket
	testl	%eax, %eax
	je	.L55
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L55
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L52:
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$-1, %eax
	jmp	.L52
.LFE66:
	.size	__GI_key_encryptsession, .-__GI_key_encryptsession
	.globl	__EI_key_encryptsession
	.set	__EI_key_encryptsession,__GI_key_encryptsession
	.p2align 4,,15
	.globl	__GI_key_decryptsession
	.hidden	__GI_key_decryptsession
	.type	__GI_key_decryptsession, @function
__GI_key_decryptsession:
.LFB67:
	pushq	%rbx
	leaq	__GI_xdr_cryptkeyres(%rip), %rcx
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	(%rsi), %rax
	leaq	__GI_xdr_cryptkeyarg(%rip), %rsi
	leaq	16(%rsp), %rdx
	leaq	4(%rsp), %r8
	movq	%rdi, 16(%rsp)
	movl	$3, %edi
	movq	%rax, 24(%rsp)
	call	key_call_socket
	testl	%eax, %eax
	je	.L60
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L60
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L57:
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$-1, %eax
	jmp	.L57
.LFE67:
	.size	__GI_key_decryptsession, .-__GI_key_decryptsession
	.globl	__EI_key_decryptsession
	.set	__EI_key_decryptsession,__GI_key_decryptsession
	.p2align 4,,15
	.globl	__GI_key_encryptsession_pk
	.hidden	__GI_key_encryptsession_pk
	.type	__GI_key_encryptsession_pk, @function
__GI_key_encryptsession_pk:
.LFB68:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	(%rdx), %rax
	movdqu	(%rsi), %xmm0
	movq	%rdi, 16(%rsp)
	movq	%rax, 40(%rsp)
	movq	__key_encryptsession_pk_LOCAL@GOTPCREL(%rip), %rax
	movups	%xmm0, 24(%rsp)
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	jne	.L68
	leaq	16(%rsp), %rdx
	leaq	4(%rsp), %r8
	leaq	__GI_xdr_cryptkeyres(%rip), %rcx
	leaq	__GI_xdr_cryptkeyarg2(%rip), %rsi
	movl	$6, %edi
	call	key_call_socket
	testl	%eax, %eax
	jne	.L64
.L66:
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	call	__geteuid
	leaq	16(%rsp), %rsi
	movl	%eax, %edi
	call	*%rbp
	movq	(%rax), %rdx
	movq	%rdx, 4(%rsp)
	movl	8(%rax), %eax
	movl	%eax, 12(%rsp)
.L64:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L66
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
.LFE68:
	.size	__GI_key_encryptsession_pk, .-__GI_key_encryptsession_pk
	.globl	__EI_key_encryptsession_pk
	.set	__EI_key_encryptsession_pk,__GI_key_encryptsession_pk
	.p2align 4,,15
	.globl	__GI_key_decryptsession_pk
	.hidden	__GI_key_decryptsession_pk
	.type	__GI_key_decryptsession_pk, @function
__GI_key_decryptsession_pk:
.LFB69:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$56, %rsp
	movq	(%rdx), %rax
	movdqu	(%rsi), %xmm0
	movq	%rdi, 16(%rsp)
	movq	%rax, 40(%rsp)
	movq	__key_decryptsession_pk_LOCAL@GOTPCREL(%rip), %rax
	movups	%xmm0, 24(%rsp)
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	jne	.L75
	leaq	16(%rsp), %rdx
	leaq	4(%rsp), %r8
	leaq	__GI_xdr_cryptkeyres(%rip), %rcx
	leaq	__GI_xdr_cryptkeyarg2(%rip), %rsi
	movl	$7, %edi
	call	key_call_socket
	testl	%eax, %eax
	jne	.L71
.L73:
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	call	__geteuid
	leaq	16(%rsp), %rsi
	movl	%eax, %edi
	call	*%rbp
	movq	(%rax), %rdx
	movq	%rdx, 4(%rsp)
	movl	8(%rax), %eax
	movl	%eax, 12(%rsp)
.L71:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L73
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
.LFE69:
	.size	__GI_key_decryptsession_pk, .-__GI_key_decryptsession_pk
	.globl	__EI_key_decryptsession_pk
	.set	__EI_key_decryptsession_pk,__GI_key_decryptsession_pk
	.p2align 4,,15
	.globl	__GI_key_gendes
	.hidden	__GI_key_gendes
	.type	__GI_key_gendes, @function
__GI_key_gendes:
.LFB70:
	pushq	%rbp
	pushq	%rbx
	movabsq	$72058139498774530, %rax
	movq	%rdi, %rbp
	movl	$100029, %esi
	movl	$1, %edx
	subq	$40, %rsp
	movq	trytimeout(%rip), %rcx
	movq	8+trytimeout(%rip), %r8
	movq	%rax, 16(%rsp)
	leaq	16(%rsp), %rdi
	movq	$0, 24(%rsp)
	movl	$-1, 12(%rsp)
	pushq	$400
	pushq	$400
	leaq	28(%rsp), %r9
	call	__GI_clntudp_bufcreate
	testq	%rax, %rax
	popq	%rcx
	popq	%rsi
	je	.L78
	movq	%rax, %rbx
	movq	8(%rax), %rax
	movq	%rbp, %r9
	pushq	8+tottimeout(%rip)
	xorl	%ecx, %ecx
	pushq	tottimeout(%rip)
	leaq	__GI_xdr_des_block(%rip), %r8
	movl	$4, %esi
	leaq	__GI_xdr_void(%rip), %rdx
	movq	%rbx, %rdi
	call	*(%rax)
	movl	%eax, %ebp
	movq	%rbx, %rdi
	popq	%rax
	movq	8(%rbx), %rax
	popq	%rdx
	call	*32(%rax)
	movl	12(%rsp), %edi
	call	__GI___close
	xorl	%eax, %eax
	testl	%ebp, %ebp
	setne	%al
	negl	%eax
.L76:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L78:
	movl	$-1, %eax
	jmp	.L76
.LFE70:
	.size	__GI_key_gendes, .-__GI_key_gendes
	.globl	__EI_key_gendes
	.set	__EI_key_gendes,__GI_key_gendes
	.p2align 4,,15
	.globl	__GI_key_setnet
	.hidden	__GI_key_setnet
	.type	__GI_key_setnet, @function
__GI_key_setnet:
.LFB71:
	subq	$24, %rsp
	leaq	__GI_xdr_keystatus(%rip), %rcx
	leaq	__GI_xdr_key_netstarg(%rip), %rsi
	leaq	12(%rsp), %r8
	movq	%rdi, %rdx
	movl	$8, %edi
	call	key_call_socket
	testl	%eax, %eax
	je	.L82
	cmpl	$1, 12(%rsp)
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
.L80:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$-1, %eax
	jmp	.L80
.LFE71:
	.size	__GI_key_setnet, .-__GI_key_setnet
	.globl	__EI_key_setnet
	.set	__EI_key_setnet,__GI_key_setnet
	.p2align 4,,15
	.globl	__GI_key_get_conv
	.hidden	__GI_key_get_conv
	.type	__GI_key_get_conv, @function
__GI_key_get_conv:
.LFB72:
	pushq	%rbx
	leaq	__GI_xdr_cryptkeyres(%rip), %rcx
	movq	%rsi, %rbx
	leaq	__GI_xdr_keybuf(%rip), %rsi
	movq	%rdi, %rdx
	movl	$10, %edi
	subq	$16, %rsp
	leaq	4(%rsp), %r8
	call	key_call_socket
	testl	%eax, %eax
	je	.L88
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L88
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L85:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$-1, %eax
	jmp	.L85
.LFE72:
	.size	__GI_key_get_conv, .-__GI_key_get_conv
	.globl	__EI_key_get_conv
	.set	__EI_key_get_conv,__GI_key_get_conv
	.p2align 4,,15
	.globl	__rpc_thread_key_cleanup
	.hidden	__rpc_thread_key_cleanup
	.type	__rpc_thread_key_cleanup, @function
__rpc_thread_key_cleanup:
.LFB76:
	pushq	%rbx
	call	__rpc_thread_variables
	movq	200(%rax), %rbx
	testq	%rbx, %rbx
	je	.L90
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L92
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L93
	movq	56(%rdi), %rax
	call	*32(%rax)
	movq	(%rbx), %rax
.L93:
	movq	8(%rax), %rdx
	movq	%rax, %rdi
	call	*32(%rdx)
.L92:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L90:
	popq	%rbx
	ret
.LFE76:
	.size	__rpc_thread_key_cleanup, .-__rpc_thread_key_cleanup
	.local	keycall_lock
	.comm	keycall_lock,4,4
	.globl	__key_gendes_LOCAL
	.bss
	.align 8
	.type	__key_gendes_LOCAL, @object
	.size	__key_gendes_LOCAL, 8
__key_gendes_LOCAL:
	.zero	8
	.globl	__key_decryptsession_pk_LOCAL
	.align 8
	.type	__key_decryptsession_pk_LOCAL, @object
	.size	__key_decryptsession_pk_LOCAL, 8
__key_decryptsession_pk_LOCAL:
	.zero	8
	.globl	__key_encryptsession_pk_LOCAL
	.align 8
	.type	__key_encryptsession_pk_LOCAL, @object
	.size	__key_encryptsession_pk_LOCAL, 8
__key_encryptsession_pk_LOCAL:
	.zero	8
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	tottimeout, @object
	.size	tottimeout, 16
tottimeout:
	.quad	60
	.quad	0
	.align 16
	.type	trytimeout, @object
	.size	trytimeout, 16
trytimeout:
	.quad	5
	.quad	0
	.hidden	__lll_lock_wait_private
	.hidden	__getpeername
	.hidden	__geteuid
	.hidden	__rpc_thread_variables
