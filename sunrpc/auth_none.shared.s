	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_authnone_create, authnone_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	authnone_marshal, @function
authnone_marshal:
	testq	%rdi, %rdi
	movq	%rsi, %rax
	je	.L2
	movq	8(%rax), %rcx
	movl	92(%rdi), %edx
	leaq	72(%rdi), %rsi
	movq	%rax, %rdi
	movq	24(%rcx), %rcx
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.size	authnone_marshal, .-authnone_marshal
	.p2align 4,,15
	.type	authnone_verf, @function
authnone_verf:
	rep ret
	.size	authnone_verf, .-authnone_verf
	.p2align 4,,15
	.type	authnone_validate, @function
authnone_validate:
	movl	$1, %eax
	ret
	.size	authnone_validate, .-authnone_validate
	.p2align 4,,15
	.type	authnone_refresh, @function
authnone_refresh:
	xorl	%eax, %eax
	ret
	.size	authnone_refresh, .-authnone_refresh
	.p2align 4,,15
	.type	authnone_create_once, @function
authnone_create_once:
	pushq	%rbx
	leaq	72+authnone_private(%rip), %rsi
	xorl	%ecx, %ecx
	movl	$20, %edx
	subq	$48, %rsp
	movq	16+__GI__null_auth(%rip), %rax
	movdqu	__GI__null_auth(%rip), %xmm0
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	movq	%rax, 40+authnone_private(%rip)
	movq	%rax, 16+authnone_private(%rip)
	leaq	ops(%rip), %rax
	movups	%xmm0, 24+authnone_private(%rip)
	movq	%rax, 56+authnone_private(%rip)
	movaps	%xmm0, authnone_private(%rip)
	call	__GI_xdrmem_create
	leaq	authnone_private(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_opaque_auth
	leaq	24+authnone_private(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_opaque_auth
	movq	8(%rsp), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	%eax, 92+authnone_private(%rip)
	movq	8(%rsp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L7
	movq	%rbx, %rdi
	call	*%rax
.L7:
	addq	$48, %rsp
	popq	%rbx
	ret
	.size	authnone_create_once, .-authnone_create_once
	.p2align 4,,15
	.type	authnone_destroy, @function
authnone_destroy:
	rep ret
	.size	authnone_destroy, .-authnone_destroy
	.p2align 4,,15
	.globl	__GI_authnone_create
	.hidden	__GI_authnone_create
	.type	__GI_authnone_create, @function
__GI_authnone_create:
	subq	$8, %rsp
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L15
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	authnone_create_once(%rip), %rsi
	leaq	authnone_private_guard(%rip), %rdi
#APP
# 96 "auth_none.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L16:
	leaq	authnone_private(%rip), %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	authnone_private_guard(%rip), %eax
	testl	%eax, %eax
	jne	.L16
	call	authnone_create_once
	orl	$2, authnone_private_guard(%rip)
	jmp	.L16
	.size	__GI_authnone_create, .-__GI_authnone_create
	.globl	__EI_authnone_create
	.set	__EI_authnone_create,__GI_authnone_create
	.local	authnone_private_guard
	.comm	authnone_private_guard,4,4
	.local	authnone_private
	.comm	authnone_private,96,32
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	ops, @object
	.size	ops, 40
ops:
	.quad	authnone_verf
	.quad	authnone_marshal
	.quad	authnone_validate
	.quad	authnone_refresh
	.quad	authnone_destroy
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
