	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver passwd2des_internal,passwd2des@GLIBC_2.2.5
	.symver __EI_xencrypt, xencrypt@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	hex2bin, @function
hex2bin:
	testl	%edi, %edi
	jle	.L1
	leal	-1(%rdi), %eax
	addq	$1, %rsi
	movl	$-16, %r10d
	movl	$-1, %r9d
	leaq	1(%rdx,%rax), %r8
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	sall	$4, %eax
	movl	%eax, %edi
.L4:
	movsbq	(%rsi), %rcx
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	jbe	.L6
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movl	(%rax,%rcx,4), %ecx
	leal	-65(%rcx), %eax
	subl	$55, %ecx
	cmpl	$26, %eax
	movl	%ecx, %eax
	cmovnb	%r9d, %eax
.L6:
	addq	$1, %rdx
	addl	%edi, %eax
	addq	$2, %rsi
	cmpq	%r8, %rdx
	movb	%al, -1(%rdx)
	je	.L1
.L8:
	movsbq	-1(%rsi), %rax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L13
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	movl	%r10d, %edi
	movq	%fs:(%rcx), %rcx
	movl	(%rcx,%rax,4), %eax
	leal	-65(%rax), %ecx
	cmpl	$25, %ecx
	ja	.L4
	subl	$55, %eax
	movl	%eax, %edi
	sall	$4, %edi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	hex2bin, .-hex2bin
	.p2align 4,,15
	.globl	__GI_passwd2des_internal
	.hidden	__GI_passwd2des_internal
	.type	__GI_passwd2des_internal, @function
__GI_passwd2des_internal:
	movq	$0, (%rsi)
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.L15
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L16:
	addl	%edx, %edx
	xorb	%dl, -1(%rsi,%rax)
	cmpl	$8, %eax
	movzbl	(%rdi,%rax), %edx
	setne	%r8b
	testb	%dl, %dl
	setne	%cl
	addq	$1, %rax
	testb	%cl, %r8b
	jne	.L16
.L15:
	movq	%rsi, %rdi
	jmp	__GI_des_setparity
	.size	__GI_passwd2des_internal, .-__GI_passwd2des_internal
	.globl	passwd2des_internal
	.set	passwd2des_internal,__GI_passwd2des_internal
	.p2align 4,,15
	.globl	__GI_xencrypt
	.hidden	__GI_xencrypt
	.type	__GI_xencrypt, @function
__GI_xencrypt:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$16, %rsp
	call	__GI_strlen
	shrq	%rax
	movq	%rsp, %r13
	movq	%rax, %rbx
	movl	%eax, %edi
	call	malloc@PLT
	movq	%rbp, %rsi
	movq	%rax, %rdx
	movl	%ebx, %edi
	movq	%rax, %r12
	call	hex2bin
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__GI_passwd2des_internal
	leaq	8(%rsp), %r8
	xorl	%ecx, %ecx
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, 8(%rsp)
	call	__GI_cbc_crypt
	cmpl	$1, %eax
	jg	.L22
	testl	%ebx, %ebx
	jle	.L24
	leal	-1(%rbx), %edi
	leaq	hex(%rip), %rax
	xorl	%edx, %edx
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	(%r12,%rdx), %ecx
	movq	%rcx, %rsi
	andl	$15, %ecx
	shrq	$4, %rsi
	movzbl	(%rax,%rcx), %ecx
	andl	$15, %esi
	movzbl	(%rax,%rsi), %esi
	movb	%cl, 1(%rbp,%rdx,2)
	movb	%sil, 0(%rbp,%rdx,2)
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L26
.L24:
	addl	%ebx, %ebx
	movq	%r12, %rdi
	movslq	%ebx, %rbx
	movb	$0, 0(%rbp,%rbx)
	call	free@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r12, %rdi
	call	free@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI_xencrypt, .-__GI_xencrypt
	.globl	__EI_xencrypt
	.set	__EI_xencrypt,__GI_xencrypt
	.p2align 4,,15
	.globl	__GI_xdecrypt
	.hidden	__GI_xdecrypt
	.type	__GI_xdecrypt, @function
__GI_xdecrypt:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$16, %rsp
	call	__GI_strlen
	shrq	%rax
	movq	%rsp, %r13
	movq	%rax, %rbx
	movl	%eax, %edi
	call	malloc@PLT
	movq	%rbp, %rsi
	movq	%rax, %rdx
	movl	%ebx, %edi
	movq	%rax, %r12
	call	hex2bin
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__GI_passwd2des_internal
	leaq	8(%rsp), %r8
	movl	$1, %ecx
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	$0, 8(%rsp)
	call	__GI_cbc_crypt
	cmpl	$1, %eax
	jg	.L30
	testl	%ebx, %ebx
	jle	.L32
	leal	-1(%rbx), %edi
	leaq	hex(%rip), %rax
	xorl	%edx, %edx
	addq	$1, %rdi
	.p2align 4,,10
	.p2align 3
.L34:
	movzbl	(%r12,%rdx), %ecx
	movq	%rcx, %rsi
	andl	$15, %ecx
	shrq	$4, %rsi
	movzbl	(%rax,%rcx), %ecx
	andl	$15, %esi
	movzbl	(%rax,%rsi), %esi
	movb	%cl, 1(%rbp,%rdx,2)
	movb	%sil, 0(%rbp,%rdx,2)
	addq	$1, %rdx
	cmpq	%rdx, %rdi
	jne	.L34
.L32:
	addl	%ebx, %ebx
	movq	%r12, %rdi
	movslq	%ebx, %rbx
	movb	$0, 0(%rbp,%rbx)
	call	free@PLT
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r12, %rdi
	call	free@PLT
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI_xdecrypt, .-__GI_xdecrypt
	.globl	xdecrypt
	.set	xdecrypt,__GI_xdecrypt
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	hex, @object
	.size	hex, 16
hex:
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
