	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __registerrpc,registerrpc@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"xxx\n"
.LC1:
	.string	"trouble replying to prog %d\n"
.LC2:
	.string	"never registered prog %d\n"
.LC3:
	.string	"%s"
#NO_APP
	.text
	.p2align 4,,15
	.type	universal, @function
universal:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8816, %rsp
	movq	16(%rdi), %r12
	movq	$0, 8(%rsp)
	testq	%r12, %r12
	je	.L25
	movq	(%rdi), %r14
	call	__rpc_thread_variables
	movq	248(%rax), %rbx
	movl	%r14d, %ebp
	testq	%rbx, %rbx
	jne	.L13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L4
.L13:
	cmpl	%ebp, 8(%rbx)
	jne	.L5
	cmpl	%r12d, 12(%rbx)
	jne	.L5
	leaq	16(%rsp), %rbp
	xorl	%esi, %esi
	movl	$8800, %edx
	movq	%rbp, %rdi
	call	__GI_memset@PLT
	movq	8(%r13), %rax
	movq	16(%rbx), %rsi
	movq	%rbp, %rdx
	movq	%r13, %rdi
	call	*16(%rax)
	testl	%eax, %eax
	je	.L26
	movq	%rbp, %rdi
	call	*(%rbx)
	testq	%rax, %rax
	je	.L8
	movq	24(%rbx), %rsi
.L9:
	movq	%rax, %rdx
	movq	%r13, %rdi
	call	__GI_svc_sendreply
	testl	%eax, %eax
	je	.L27
	movq	8(%r13), %rax
	movq	16(%rbx), %rsi
	movq	%rbp, %rdx
	movq	%r13, %rdi
	call	*32(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movl	%r14d, %edx
.L23:
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L28
	movq	8(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L12
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	8(%rsp), %rdi
	call	free@PLT
	movl	$1, %edi
	call	__GI_exit
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	__GI_xdr_void(%rip), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	__GI_svc_sendreply
	testl	%eax, %eax
	je	.L29
.L1:
	addq	$8816, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	$0, 8(%rsp)
.L12:
	movl	$1, %edi
	call	__GI_exit
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	__GI_xdr_void(%rip), %rsi
	cmpq	%rsi, 24(%rbx)
	jne	.L1
	jmp	.L9
.L26:
	movq	%r13, %rdi
	call	__GI_svcerr_decode
	jmp	.L1
.L27:
	movl	8(%rbx), %ebx
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movl	%ebx, %edx
	jmp	.L23
.L29:
	leaq	.LC0(%rip), %rsi
	movl	$2, %edi
	movl	$4, %edx
	call	__GI___write
	movl	$1, %edi
	call	__GI_exit
	.size	universal, .-universal
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"can't reassign procedure number %ld\n"
	.align 8
.LC5:
	.string	"couldn't create an rpc server\n"
	.align 8
.LC6:
	.string	"couldn't register prog %ld vers %ld\n"
	.section	.rodata.str1.1
.LC7:
	.string	"registerrpc: out of memory\n"
	.text
	.p2align 4,,15
	.globl	__registerrpc
	.type	__registerrpc, @function
__registerrpc:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	testq	%rdx, %rdx
	movq	%r9, 8(%rsp)
	je	.L46
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %r13
	movq	%rcx, %r15
	movq	%r8, %r14
	call	__rpc_thread_variables
	cmpq	$0, 256(%rax)
	movq	%rax, %rbp
	je	.L47
.L34:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_pmap_unset
	movq	256(%rbp), %rdi
	leaq	universal(%rip), %rcx
	movl	$17, %r8d
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	__GI_svc_register
	testl	%eax, %eax
	jne	.L36
	leaq	.LC6(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	24(%rsp), %rdi
	movq	%rax, %rsi
	movq	%r12, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L39
.L43:
	movq	24(%rsp), %rdx
.L35:
	testq	%rdx, %rdx
	je	.L39
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	24(%rsp), %rdi
	call	free@PLT
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$40, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L48
	movq	248(%rbp), %rdx
	movq	8(%rsp), %rcx
	movq	%r15, (%rax)
	movl	%ebx, 8(%rax)
	movl	%r13d, 12(%rax)
	movq	%r14, 16(%rax)
	movq	%rcx, 24(%rax)
	movq	%rdx, 32(%rax)
	movq	%rax, 248(%rbp)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$-1, %edi
	call	__GI_svcudp_create
	testq	%rax, %rax
	movq	%rax, 256(%rbp)
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jne	.L34
.L45:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI___strdup
	movq	%rax, %rdx
	movq	%rax, 24(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	24(%rsp), %rdi
	xorl	%edx, %edx
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L43
.L39:
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$5, %edx
	leaq	.LC7(%rip), %rsi
	jmp	.L45
	.size	__registerrpc, .-__registerrpc
	.hidden	__fxprintf
	.hidden	__rpc_thread_variables
