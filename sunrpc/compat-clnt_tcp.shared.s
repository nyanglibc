	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	clnttcp_geterr, @function
clnttcp_geterr:
	movq	16(%rdi), %rax
	movdqu	48(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movq	64(%rax), %rax
	movq	%rax, 16(%rsi)
	ret
	.size	clnttcp_geterr, .-clnttcp_geterr
	.p2align 4,,15
	.type	clnttcp_freeres, @function
clnttcp_freeres:
	movq	16(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	$2, 104(%rdi)
	addq	$104, %rdi
	jmp	*%rcx
	.size	clnttcp_freeres, .-clnttcp_freeres
	.p2align 4,,15
	.type	clnttcp_abort, @function
clnttcp_abort:
	rep ret
	.size	clnttcp_abort, .-clnttcp_abort
	.p2align 4,,15
	.type	clnttcp_destroy, @function
clnttcp_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	16(%rdi), %rbx
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jne	.L12
.L6:
	movq	112(%rbx), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L7
	leaq	104(%rbx), %rdi
	call	*%rax
.L7:
	movq	%rbx, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	movl	(%rbx), %edi
	call	__GI___close
	jmp	.L6
	.size	clnttcp_destroy, .-clnttcp_destroy
	.p2align 4,,15
	.type	clnttcp_control, @function
clnttcp_control:
	cmpl	$15, %esi
	movq	16(%rdi), %rdi
	ja	.L28
	leaq	.L16(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L16:
	.long	.L28-.L16
	.long	.L15-.L16
	.long	.L17-.L16
	.long	.L18-.L16
	.long	.L28-.L16
	.long	.L28-.L16
	.long	.L19-.L16
	.long	.L28-.L16
	.long	.L20-.L16
	.long	.L21-.L16
	.long	.L22-.L16
	.long	.L23-.L16
	.long	.L24-.L16
	.long	.L25-.L16
	.long	.L26-.L16
	.long	.L27-.L16
	.text
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rdx), %rax
	bswap	%eax
	movl	%eax, 84(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movdqu	(%rdx), %xmm0
	movl	$1, %eax
	movl	$1, 24(%rdi)
	movups	%xmm0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movdqu	8(%rdi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	28(%rdi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	(%rdi), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$0, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	72(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rdx), %rax
	subl	$1, %eax
	bswap	%eax
	movl	%eax, 72(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	88(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rdx), %rax
	bswap	%eax
	movl	%eax, 88(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	84(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	ret
	.size	clnttcp_control, .-clnttcp_control
	.p2align 4,,15
	.type	clnttcp_call, @function
clnttcp_call:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$184, %rsp
	movq	16(%rdi), %r12
	movq	%rcx, 48(%rsp)
	movq	%rsi, 72(%rsp)
	movq	%rdx, 40(%rsp)
	movq	%r8, 32(%rsp)
	movl	24(%r12), %ecx
	leaq	72(%r12), %rax
	movq	%r9, 64(%rsp)
	leaq	104(%r12), %rbx
	movq	%rax, 16(%rsp)
	testl	%ecx, %ecx
	jne	.L30
	movdqu	240(%rsp), %xmm0
	movups	%xmm0, 8(%r12)
.L30:
	cmpq	$0, 32(%rsp)
	movl	$1, 56(%rsp)
	je	.L74
.L31:
	leaq	72(%rsp), %rax
	movl	$3, 60(%rsp)
	movq	%rax, 24(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 8(%rsp)
.L32:
	movl	72(%r12), %eax
	movl	$0, 104(%r12)
	movq	%rbx, %rdi
	movl	$0, 48(%r12)
	movl	96(%r12), %edx
	movq	16(%rsp), %rsi
	leal	-1(%rax), %ebp
	movq	112(%r12), %rax
	movl	%ebp, 72(%r12)
	bswap	%ebp
	call	*24(%rax)
	testl	%eax, %eax
	je	.L36
	movq	112(%r12), %rax
	movq	24(%rsp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	je	.L36
	movq	(%r15), %rdi
	movq	%rbx, %rsi
	movq	56(%rdi), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L36
	xorl	%eax, %eax
	movq	48(%rsp), %rsi
	movq	%rbx, %rdi
	movq	40(%rsp), %rcx
	call	*%rcx
	testl	%eax, %eax
	je	.L36
	movl	56(%rsp), %esi
	movq	%rbx, %rdi
	call	__GI_xdrrec_endofrecord
	testl	%eax, %eax
	je	.L75
	movl	56(%rsp), %eax
	testl	%eax, %eax
	je	.L51
	cmpq	$0, 8(%r12)
	je	.L76
.L40:
	leaq	__GI__null_auth(%rip), %r14
	movl	$1, 104(%r12)
	leaq	16(%r14), %r13
.L62:
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	movq	$0, 136(%rsp)
	movdqu	(%r14), %xmm0
	movq	%rax, 120(%rsp)
	leaq	__GI_xdr_void(%rip), %rax
	movups	%xmm0, 104(%rsp)
	movq	%rax, 144(%rsp)
	call	__GI_xdrrec_skiprecord
	testl	%eax, %eax
	je	.L47
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	jne	.L42
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L62
.L29:
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	cmpl	80(%rsp), %ebp
	jne	.L62
	movq	8(%rsp), %rdi
	leaq	48(%r12), %rsi
	call	__GI__seterr_reply
	movl	48(%r12), %eax
	testl	%eax, %eax
	je	.L77
	subl	$1, 60(%rsp)
	je	.L29
	movq	(%r15), %rdi
	movq	56(%rdi), %rax
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L32
	.p2align 4,,10
	.p2align 3
.L47:
	movl	48(%r12), %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L76:
	cmpq	$0, 16(%r12)
	jne	.L40
	movl	$5, 48(%r12)
	movl	$5, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	$0, 8(%r12)
	jne	.L31
	xorl	%eax, %eax
	cmpq	$0, 16(%r12)
	setne	%al
	movl	%eax, 56(%rsp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	movl	48(%r12), %edx
	testl	%edx, %edx
	jne	.L35
	movl	$1, 48(%r12)
.L35:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__GI_xdrrec_endofrecord
	movl	48(%r12), %eax
	jmp	.L29
.L75:
	movl	$3, 48(%r12)
	movl	$3, %eax
	jmp	.L29
.L51:
	xorl	%eax, %eax
	jmp	.L29
.L77:
	movq	8(%rsp), %rax
	movq	(%r15), %rdi
	leaq	24(%rax), %rsi
	movq	56(%rdi), %rax
	call	*16(%rax)
	testl	%eax, %eax
	je	.L78
	xorl	%eax, %eax
	movq	64(%rsp), %rsi
	movq	%rbx, %rdi
	movq	32(%rsp), %rcx
	call	*%rcx
	testl	%eax, %eax
	jne	.L46
	cmpl	$0, 48(%r12)
	jne	.L46
	movl	$2, 48(%r12)
.L46:
	cmpq	$0, 112(%rsp)
	je	.L47
	movq	8(%rsp), %rsi
	movl	$2, 104(%r12)
	movq	%rbx, %rdi
	addq	$24, %rsi
	call	__GI_xdr_opaque_auth
	movl	48(%r12), %eax
	jmp	.L29
.L78:
	movl	$7, 48(%r12)
	movl	$6, 56(%r12)
	jmp	.L46
	.size	clnttcp_call, .-clnttcp_call
	.p2align 4,,15
	.type	writetcp, @function
writetcp:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testl	%edx, %edx
	jle	.L80
	movq	%rdi, %r12
	movq	%rsi, %rbp
	movl	%edx, %ebx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L81:
	subl	%eax, %ebx
	cltq
	addq	%rax, %rbp
	testl	%ebx, %ebx
	jle	.L80
.L83:
	movl	(%r12), %edi
	movslq	%ebx, %rdx
	movq	%rbp, %rsi
	call	__GI___write
	cmpl	$-1, %eax
	movl	%eax, %edx
	jne	.L81
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$3, 48(%r12)
	movl	%fs:(%rax), %eax
	movl	%eax, 56(%r12)
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$8, %rsp
	movl	%r13d, %edx
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	writetcp, .-writetcp
	.p2align 4,,15
	.type	readtcp, @function
readtcp:
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L100
	pushq	%r14
	pushq	%r13
	movabsq	$2361183241434822607, %rcx
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	movq	16(%rdi), %rsi
	imull	$1000, 8(%rdi), %r13d
	leaq	8(%rsp), %r14
	movl	%eax, 8(%rsp)
	movl	$1, %eax
	movw	%ax, 12(%rsp)
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rcx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addl	%edx, %r13d
.L91:
	movl	%r13d, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	__GI___poll
	cmpl	$-1, %eax
	je	.L89
	testl	%eax, %eax
	jne	.L98
	movl	$5, 48(%rbx)
	movl	$-1, %eax
.L86:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	movl	(%rbx), %edi
	movslq	%ebp, %rdx
	movq	%r12, %rsi
	call	__GI___read
	cmpl	$-1, %eax
	je	.L92
	testl	%eax, %eax
	jne	.L86
	movl	$104, 56(%rbx)
	movl	$4, 48(%rbx)
	movl	$-1, %eax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L100:
	rep ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%fs:(%rdx), %edx
	cmpl	$4, %edx
	je	.L91
	movl	$4, 48(%rbx)
	movl	%edx, 56(%rbx)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L92:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$4, 48(%rbx)
	movl	%fs:(%rdx), %edx
	movl	%edx, 56(%rbx)
	jmp	.L86
	.size	readtcp, .-readtcp
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI_clnttcp_create
	.hidden	__GI_clnttcp_create
	.type	__GI_clnttcp_create, @function
__GI_clnttcp_create:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	$24, %edi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdx, %r12
	movq	%rcx, %rbp
	subq	$120, %rsp
	movl	%r8d, 8(%rsp)
	movl	%r9d, 12(%rsp)
	call	malloc@PLT
	movl	$152, %edi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rbx, %rbx
	movq	%rax, %r14
	je	.L119
	testq	%rax, %rax
	je	.L119
	cmpw	$0, 2(%r15)
	je	.L126
	movl	0(%rbp), %eax
	testl	%eax, %eax
	js	.L127
.L110:
	movl	$0, 4(%r14)
.L116:
	movdqu	(%r15), %xmm0
	movl	%eax, (%r14)
	movq	$0, 16(%r14)
	movups	%xmm0, 28(%r14)
	movl	$0, 24(%r14)
	call	_create_xid@PLT
	leaq	72(%r14), %rsi
	xorl	%ecx, %ecx
	movl	$24, %edx
	movq	%r12, 48(%rsp)
	leaq	104(%r14), %r12
	movq	%rax, 16(%rsp)
	movl	$0, 24(%rsp)
	movq	$2, 32(%rsp)
	movq	%r12, %rdi
	movq	%r13, 40(%rsp)
	call	__GI_xdrmem_create
	leaq	16(%rsp), %rsi
	movq	%r12, %rdi
	call	__GI_xdr_callhdr
	testl	%eax, %eax
	je	.L128
	movq	112(%r14), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	movl	%eax, 96(%r14)
	movq	112(%r14), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L118
	movq	%r12, %rdi
	call	*%rax
.L118:
	movl	12(%rsp), %edx
	movl	8(%rsp), %esi
	leaq	writetcp(%rip), %r9
	leaq	readtcp(%rip), %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	__GI_xdrrec_create
	leaq	tcp_ops(%rip), %rax
	movq	%r14, 16(%rbx)
	movq	%rax, 8(%rbx)
	call	__GI_authnone_create
	movq	%rax, (%rbx)
.L103:
	addq	$120, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	movl	$6, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	__GI_pmap_getport
	testw	%ax, %ax
	je	.L106
	rolw	$8, %ax
	movw	%ax, 2(%r15)
	movl	0(%rbp), %eax
	testl	%eax, %eax
	jns	.L110
.L127:
	movl	$6, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	__GI___socket
	xorl	%esi, %esi
	movl	%eax, %edi
	movl	%eax, 0(%rbp)
	call	__GI_bindresvport
	movl	0(%rbp), %edi
	testl	%edi, %edi
	js	.L114
	movl	$16, %edx
	movq	%r15, %rsi
	call	__GI___connect
	testl	%eax, %eax
	js	.L114
	movl	$1, 4(%r14)
	movl	0(%rbp), %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L114:
	call	__GI___rpc_thread_createerr
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$12, (%rax)
	movl	%fs:(%rdx), %edx
	movl	%edx, 16(%rax)
	movl	0(%rbp), %edi
	testl	%edi, %edi
	js	.L106
.L125:
	call	__GI___close
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r14, %rdi
	call	free@PLT
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L119:
	call	__GI___rpc_thread_createerr
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rbp
	call	__GI___dcgettext
	leaq	__func__.14334(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movl	$12, 0(%rbp)
	movl	$12, 16(%rbp)
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L128:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L106
	movl	0(%rbp), %edi
	jmp	.L125
	.size	__GI_clnttcp_create, .-__GI_clnttcp_create
	.globl	clnttcp_create
	.set	clnttcp_create,__GI_clnttcp_create
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__func__.14334, @object
	.size	__func__.14334, 15
__func__.14334:
	.string	"clnttcp_create"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	tcp_ops, @object
	.size	tcp_ops, 48
tcp_ops:
	.quad	clnttcp_call
	.quad	clnttcp_abort
	.quad	clnttcp_geterr
	.quad	clnttcp_freeres
	.quad	clnttcp_destroy
	.quad	clnttcp_control
	.hidden	__fxprintf
