	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_rtime, rtime@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_rtime
	.hidden	__GI_rtime
	.type	__GI_rtime, @function
__GI_rtime:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$56, %rsp
	testq	%rdx, %rdx
	je	.L28
	movq	%rdx, %r14
	movl	$2, %esi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %r13d
	jns	.L29
.L26:
	movl	$-1, %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	16(%rsp), %r15
	xorl	%ecx, %ecx
	movl	$620756994, (%rbx)
	movl	$16, %r9d
	movq	%rbx, %r8
	movl	$4, %edx
	movq	%r15, %rsi
	movl	%r13d, %edi
	call	__sendto
	testl	%eax, %eax
	js	.L30
	movl	4(%r14), %edx
	movl	$274877907, %ecx
	movl	%r13d, 24(%rsp)
	imull	$1000, (%r14), %ebp
	movq	__libc_errno@gottpoff(%rip), %rbx
	leaq	24(%rsp), %r14
	movl	%edx, %eax
	mull	%ecx
	movl	$1, %eax
	movw	%ax, 28(%rsp)
	shrl	$6, %edx
	addl	%edx, %ebp
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%fs:(%rbx), %edx
	cmpl	$4, %edx
	jne	.L7
.L6:
	movl	%ebp, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	__GI___poll
	testl	%eax, %eax
	js	.L31
	je	.L32
	leaq	20(%rsp), %r9
	leaq	32(%rsp), %r8
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movl	$4, %edx
	movl	%r13d, %edi
	movl	$16, 20(%rsp)
	call	__recvfrom
	movl	%fs:(%rbx), %r15d
	movq	%rax, %rbp
	movl	%r13d, %edi
	movl	%eax, %r14d
	call	__GI___close
	testl	%ebp, %ebp
	movl	%r15d, %fs:(%rbx)
	jns	.L9
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%edx, %edx
	movl	$1, %esi
	movl	$2, %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %ebp
	js	.L26
	movl	$620756994, (%rbx)
	movl	$16, %edx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	__GI___connect
	testl	%eax, %eax
	js	.L33
	leaq	16(%rsp), %rsi
	movl	$4, %edx
	movl	%ebp, %edi
	call	__GI___read
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%rax, %r13
	movl	%ebp, %edi
	movl	%eax, %r14d
	movl	%fs:(%rbx), %r15d
	call	__GI___close
	testl	%r13d, %r13d
	movl	%r15d, %fs:(%rbx)
	js	.L26
.L9:
	cmpl	$4, %r14d
	jne	.L34
	movl	16(%rsp), %eax
	movl	$0, 4(%r12)
	bswap	%eax
	addl	$2085978496, %eax
	movl	%eax, (%r12)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$5, %fs:(%rbx)
	jmp	.L26
.L32:
	movl	$110, %fs:(%rbx)
	movl	$110, %edx
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%r13d, %edi
	movl	%edx, 12(%rsp)
	call	__GI___close
	movl	12(%rsp), %edx
	movl	$-1, %eax
	movl	%edx, %fs:(%rbx)
	jmp	.L1
.L33:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	%ebp, %edi
	movl	%fs:(%rbx), %r12d
	call	__GI___close
	movl	$-1, %eax
	movl	%r12d, %fs:(%rbx)
	jmp	.L1
.L30:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	%r13d, %edi
	movl	%fs:(%rbx), %ebp
	call	__GI___close
	movl	$-1, %eax
	movl	%ebp, %fs:(%rbx)
	jmp	.L1
	.size	__GI_rtime, .-__GI_rtime
	.globl	__EI_rtime
	.set	__EI_rtime,__GI_rtime
	.hidden	__recvfrom
	.hidden	__sendto
