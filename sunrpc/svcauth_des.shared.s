	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver svcauthdes_stats,svcauthdes_stats@GLIBC_2.2.5
	.symver __EI_authdes_getucred, authdes_getucred@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_svcauth_des
	.type	_svcauth_des, @function
_svcauth_des:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$1128, %rsp
	call	__rpc_thread_variables
	cmpq	$0, 208(%rax)
	movq	%rax, %rbp
	je	.L77
.L2:
	movl	64(%rbx), %eax
	subl	$1, %eax
	cmpl	$399, %eax
	ja	.L71
	movq	56(%rbx), %rdx
	movq	48(%r13), %r12
	movl	(%rdx), %eax
	bswap	%eax
	testl	%eax, %eax
	movl	%eax, (%r12)
	je	.L6
	cmpl	$1, %eax
	jne	.L71
	movl	88(%rbx), %eax
	movl	4(%rdx), %r15d
	subl	$1, %eax
	movl	%r15d, 32(%r12)
	cmpl	$399, %eax
	ja	.L71
	movq	80(%rbx), %rax
	movl	(%rax), %r8d
	movl	4(%rax), %ecx
	movl	8(%rax), %r10d
.L43:
	cmpl	$63, %r15d
	ja	.L71
	movl	%r15d, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	208(%rbp), %rax
	leaq	(%rax,%rdx,8), %r14
	cmpq	$0, 8(%r14)
	je	.L71
	leaq	80(%rsp), %rax
	movq	%rax, 8(%rsp)
.L19:
	movl	(%r12), %edx
	movl	%r8d, 80(%rsp)
	movl	%ecx, 84(%rsp)
	testl	%edx, %edx
	jne	.L20
	movl	24(%r12), %eax
	movq	8(%rsp), %rsi
	leaq	72(%rsp), %r8
	movl	$1, %ecx
	movl	$16, %edx
	movq	%r14, %rdi
	movl	%r10d, 92(%rsp)
	movq	$0, 72(%rsp)
	movl	%eax, 88(%rsp)
	call	__GI_cbc_crypt
.L21:
	cmpl	$1, %eax
	jg	.L22
	movl	84(%rsp), %r11d
	movl	80(%rsp), %r10d
	movl	%r11d, %eax
	bswap	%r10d
	bswap	%eax
	movl	%eax, 20(%rsp)
	movl	(%r12), %eax
	testl	%eax, %eax
	jne	.L23
	movl	88(%rsp), %eax
	bswap	%eax
	movl	%eax, %ecx
	movl	%eax, 40(%rsp)
	movl	92(%rsp), %eax
	leal	-1(%rcx), %edx
	bswap	%eax
	cmpl	%eax, %edx
	jne	.L71
	movq	8(%r12), %rax
	movl	(%r14), %r9d
	xorl	%r15d, %r15d
	movq	208(%rbp), %r8
	movq	%rbx, 32(%rsp)
	movq	%rax, 24(%rsp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L24:
	addl	$1, %r15d
	addq	$40, %r8
	cmpl	$64, %r15d
	je	.L78
.L29:
	cmpl	(%r8), %r9d
	jne	.L24
	movl	4(%r14), %eax
	cmpl	%eax, 4(%r8)
	jne	.L24
	movq	8(%r8), %rbx
	testq	%rbx, %rbx
	je	.L24
	movq	24(%rsp), %rdi
	movq	%r8, 56(%rsp)
	movl	%r9d, 52(%rsp)
	movl	%r10d, 48(%rsp)
	movl	%r11d, 44(%rsp)
	call	__GI_strlen
	movq	24(%rsp), %rsi
	leaq	1(%rax), %rdx
	movq	%rbx, %rdi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	movl	44(%rsp), %r11d
	movl	48(%rsp), %r10d
	movl	52(%rsp), %r9d
	movq	56(%rsp), %r8
	jne	.L24
	cmpl	20(%r8), %r10d
	movq	32(%rsp), %rbx
	je	.L79
	setb	%al
.L26:
	testb	%al, %al
	movq	svcauthdes_stats@GOTPCREL(%rip), %rax
	jne	.L80
	addq	$1, (%rax)
	movl	%r15d, %ecx
.L28:
	cmpw	$64, %cx
	movl	$2, %eax
	ja	.L1
	cmpl	$999999, 20(%rsp)
	movl	$3, %eax
	ja	.L1
	movswl	%cx, %r15d
	xorl	%ecx, %ecx
.L30:
	leaq	96(%rsp), %rsi
	xorl	%edi, %edi
	movl	%ecx, 44(%rsp)
	movl	%r10d, 32(%rsp)
	movl	%r11d, 24(%rsp)
	call	__GI___clock_gettime
	movl	40(%rsp), %eax
	movl	32(%rsp), %edi
	movq	96(%rsp), %rcx
	movq	104(%rsp), %rsi
	movl	24(%rsp), %r11d
	subq	%rax, %rcx
	movq	%rdi, %r10
	cmpq	%rdi, %rcx
	setge	%dl
	cmpq	%rdi, %rcx
	movl	44(%rsp), %ecx
	jne	.L36
	movq	%rsi, %rax
	movabsq	$2361183241434822607, %rdx
	sarq	$63, %rsi
	imulq	%rdx
	movq	%rdx, %rax
	movl	20(%rsp), %edx
	sarq	$7, %rax
	subq	%rsi, %rax
	cmpq	%rdx, %rax
	setge	%dl
.L36:
	testb	%dl, %dl
	je	.L37
	testl	%ecx, %ecx
	je	.L71
.L34:
	movl	$4, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %eax
.L1:
	addq	$1128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	4(%rdx), %eax
	bswap	%eax
	cmpl	$255, %eax
	ja	.L71
	leaq	40(%r12), %rcx
	cmpl	$8, %eax
	leaq	8(%rdx), %r8
	movl	%eax, %r9d
	movq	%rcx, 8(%r12)
	jnb	.L9
	testb	$4, %al
	jne	.L81
	testl	%eax, %eax
	je	.L10
	movzbl	8(%rdx), %edx
	testb	$2, %al
	movb	%dl, 40(%r12)
	jne	.L82
.L10:
	addq	$3, %rax
	movb	$0, 40(%r12,%r9)
	andl	$4294967292, %eax
	addq	%rax, %r8
	movl	(%r8), %eax
	movl	%eax, 16(%r12)
	movl	4(%r8), %eax
	movl	%eax, 20(%r12)
	movl	8(%r8), %eax
	movl	%eax, 24(%r12)
	movl	88(%rbx), %eax
	subl	$1, %eax
	cmpl	$399, %eax
	ja	.L71
	movl	(%r12), %esi
	movq	80(%rbx), %rax
	testl	%esi, %esi
	movl	(%rax), %r8d
	movl	4(%rax), %ecx
	movl	8(%rax), %r10d
	jne	.L15
	leaq	96(%rsp), %r15
	movq	8(%r12), %rdi
	movl	%r8d, 40(%rsp)
	movl	%ecx, 20(%rsp)
	movl	%r10d, 8(%rsp)
	movq	%r15, %rsi
	call	__GI_getpublickey
	testl	%eax, %eax
	movl	8(%rsp), %r10d
	movl	20(%rsp), %ecx
	movl	40(%rsp), %r8d
	je	.L71
	leaq	16(%r12), %r14
	movq	%r15, 88(%rsp)
	movq	%r15, %rdx
.L17:
	movl	(%rdx), %esi
	addq	$4, %rdx
	leal	-16843009(%rsi), %eax
	notl	%esi
	andl	%esi, %eax
	andl	$-2139062144, %eax
	je	.L17
	movl	%eax, %esi
	movl	%ecx, 40(%rsp)
	movq	8(%r12), %rdi
	shrl	$16, %esi
	testl	$32896, %eax
	movl	%r10d, 24(%rsp)
	cmove	%esi, %eax
	leaq	2(%rdx), %rsi
	movl	%r8d, 20(%rsp)
	movl	%eax, %ecx
	cmove	%rsi, %rdx
	addb	%al, %cl
	leaq	80(%rsp), %rax
	sbbq	$3, %rdx
	subq	%r15, %rdx
	movq	%rax, %rsi
	movq	%rax, 8(%rsp)
	addl	$1, %edx
	movl	%edx, 80(%rsp)
	movq	%r14, %rdx
	call	__GI_key_decryptsession_pk
	testl	%eax, %eax
	js	.L71
	xorl	%r15d, %r15d
	movl	20(%rsp), %r8d
	movl	40(%rsp), %ecx
	movl	24(%rsp), %r10d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$1, %esi
	movl	$2560, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 208(%rbp)
	je	.L22
	movl	$256, %edi
	call	malloc@PLT
	xorl	%edx, %edx
	movq	%rax, 216(%rbp)
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%edx, (%rax,%rdx,4)
	addq	$1, %rdx
	cmpq	$64, %rdx
	jne	.L5
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$7, %eax
	jmp	.L1
.L9:
	movq	8(%rdx), %rdx
	leaq	48(%r12), %rdi
	movq	%r8, %rsi
	andq	$-8, %rdi
	movq	%rdx, 40(%r12)
	movq	-8(%r8,%r9), %rdx
	movq	%rdx, -8(%rcx,%r9)
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%eax, %ecx
	shrl	$3, %ecx
	rep movsq
	jmp	.L10
.L20:
	movq	8(%rsp), %rsi
	movl	$1, %ecx
	movl	$8, %edx
	movq	%r14, %rdi
	call	__GI_ecb_crypt
	jmp	.L21
.L23:
	cmpl	$999999, 20(%rsp)
	ja	.L34
	movl	%r15d, %eax
	leaq	(%rax,%rax,4), %rdx
	movq	208(%rbp), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	20(%rax), %ecx
	cmpl	%r10d, %ecx
	seta	%dl
	cmpl	%r10d, %ecx
	jne	.L33
	movl	20(%rsp), %ecx
	cmpl	%ecx, 24(%rax)
	seta	%dl
.L33:
	testb	%dl, %dl
	jne	.L34
	movl	16(%rax), %eax
	movl	$1, %ecx
	movl	%eax, 40(%rsp)
	jmp	.L30
.L81:
	movl	8(%rdx), %edx
	movl	%edx, 40(%r12)
	movl	-4(%r8,%r9), %edx
	movl	%edx, -4(%rcx,%r9)
	jmp	.L10
.L78:
	movq	svcauthdes_stats@GOTPCREL(%rip), %rax
	movq	32(%rsp), %rbx
	addq	$1, 16(%rax)
	movq	216(%rbp), %rax
	movzwl	252(%rax), %ecx
	jmp	.L28
.L82:
	movzwl	-2(%r8,%r9), %edx
	movw	%dx, -2(%rcx,%r9)
	jmp	.L10
.L37:
	leal	-1(%r10), %eax
	movq	8(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$8, %edx
	movq	%r14, %rdi
	movl	%r10d, 24(%rsp)
	bswap	%eax
	movl	%r11d, 84(%rsp)
	movl	%eax, 80(%rsp)
	call	__GI_ecb_crypt
	cmpl	$1, %eax
	movl	24(%rsp), %r10d
	jg	.L22
	movl	84(%rsp), %edx
	movl	80(%rsp), %ecx
	movq	80(%rbx), %rax
	movl	%ecx, (%rax)
	movl	%edx, 4(%rax)
	addq	$12, %rax
	movl	%r15d, -4(%rax)
	movq	80(%rbx), %rcx
	movl	%r15d, %ebx
	movq	56(%r13), %rdx
	imulq	$40, %rbx, %rbx
	addq	208(%rbp), %rbx
	subq	%rcx, %rax
	movl	%eax, 56(%rdx)
	movl	20(%rsp), %eax
	movl	$3, 40(%rdx)
	movq	%rcx, 48(%rdx)
	movl	%eax, 24(%rbx)
	movq	216(%rbp), %rax
	movl	%r10d, 20(%rbx)
	movl	(%rax), %edx
	movl	%r15d, (%rax)
	cmpl	%edx, %r15d
	je	.L38
	addq	$4, %rax
.L39:
	movl	(%rax), %ecx
	movl	%edx, (%rax)
	addq	$4, %rax
	cmpl	%ecx, %r15d
	movl	%ecx, %edx
	jne	.L39
.L38:
	cmpl	$0, (%r12)
	movq	8(%rbx), %rdi
	jne	.L40
	movl	40(%rsp), %eax
	testq	%rdi, %rdi
	movl	%r15d, 32(%r12)
	movl	%eax, 24(%r12)
	je	.L41
	call	free@PLT
.L41:
	movq	8(%r12), %r13
	orq	$-1, %r12
	xorl	%eax, %eax
	movq	%r12, %rcx
	movq	%r13, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	movl	%eax, %edi
	movq	%rax, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	je	.L22
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movl	40(%rsp), %eax
	movl	%eax, 16(%rbx)
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L47
	movl	%r12d, 8(%rax)
	xorl	%eax, %eax
	jmp	.L1
.L40:
	movq	(%rbx), %rax
	movl	$0, (%r12)
	movq	%rdi, 8(%r12)
	movq	%rax, 16(%r12)
	movl	16(%rbx), %eax
	movl	%eax, 24(%r12)
	xorl	%eax, %eax
	jmp	.L1
.L79:
	movl	20(%rsp), %eax
	cmpl	24(%r8), %eax
	setb	%al
	jmp	.L26
.L80:
	addq	$1, 8(%rax)
	movl	$2, %eax
	jmp	.L1
.L15:
	movl	32(%r12), %r15d
	jmp	.L43
.L47:
	xorl	%eax, %eax
	jmp	.L1
	.size	_svcauth_des, .-_svcauth_des
	.p2align 4,,15
	.globl	__GI_authdes_getucred
	.hidden	__GI_authdes_getucred
	.type	__GI_authdes_getucred, @function
__GI_authdes_getucred:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	32(%rdi), %ebx
	cmpl	$63, %ebx
	ja	.L84
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %rbp
	call	__rpc_thread_variables
	leaq	(%rbx,%rbx,4), %r10
	movq	%rax, %r9
	movq	208(%rax), %rax
	salq	$3, %r10
	movq	32(%rax,%r10), %rbx
	testq	%rbx, %rbx
	je	.L85
	movl	8(%rbx), %eax
	cmpl	$-1, %eax
	je	.L109
	cmpl	$-2, %eax
	je	.L84
	movl	(%rbx), %eax
	movl	%eax, (%r12)
	movl	4(%rbx), %eax
	movl	%eax, 0(%r13)
	cmpl	$32767, 8(%rbx)
	movl	$32767, %eax
	cmovle	8(%rbx), %eax
	movw	%ax, (%r14)
	subl	$1, %eax
	movl	%eax, %ecx
	js	.L93
	movslq	%eax, %rdx
	movl	%ecx, %ecx
	leaq	-1(%rdx), %rax
	movq	%rax, %rsi
	subq	%rcx, %rsi
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L110:
	subq	$1, %rax
.L92:
	movl	16(%rbx,%rdx,4), %ecx
	cmpq	%rax, %rsi
	movl	%ecx, (%r15,%rdx,4)
	movq	%rax, %rdx
	jne	.L110
.L93:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movq	8(%rbp), %rdi
	leaq	28(%rsp), %rcx
	leaq	24(%rsp), %rdx
	leaq	20(%rsp), %rsi
	movq	%r15, %r8
	movq	%r10, 8(%rsp)
	movq	%r9, (%rsp)
	call	__GI_netname2user
	testl	%eax, %eax
	movq	(%rsp), %r9
	movq	8(%rsp), %r10
	je	.L84
.L94:
	cmpl	$65536, 28(%rsp)
	movl	$65536, %ebp
	movq	%r10, 8(%rsp)
	cmovge	28(%rsp), %ebp
	movq	%r9, (%rsp)
	movslq	%ebp, %rax
	leaq	16(,%rax,4), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L111
	.p2align 4,,10
	.p2align 3
.L84:
	xorl	%eax, %eax
.L83:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movq	8(%rbp), %rdi
	leaq	28(%rsp), %rcx
	leaq	24(%rsp), %rdx
	leaq	20(%rsp), %rsi
	movq	%r15, %r8
	movq	%r10, 8(%rsp)
	movq	%r9, (%rsp)
	call	__GI_netname2user
	testl	%eax, %eax
	movq	(%rsp), %r9
	movq	8(%rsp), %r10
	jne	.L87
	movl	$-2, 8(%rbx)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L87:
	movl	28(%rsp), %esi
	cmpl	%esi, 12(%rbx)
	jl	.L112
.L96:
	movl	20(%rsp), %eax
	movl	%esi, %ecx
	subl	$1, %ecx
	movl	%eax, (%rbx)
	movl	%eax, (%r12)
	movl	24(%rsp), %eax
	movl	%eax, 4(%rbx)
	movl	%eax, 0(%r13)
	movl	%esi, 8(%rbx)
	js	.L89
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	leaq	-1(%rdx), %rax
	movq	%rax, %rdi
	subq	%rcx, %rdi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L113:
	subq	$1, %rax
.L90:
	movl	(%r15,%rdx,4), %ecx
	cmpq	%rax, %rdi
	movl	%ecx, 16(%rbx,%rdx,4)
	movq	%rax, %rdx
	jne	.L113
.L89:
	cmpl	$32767, %esi
	movl	$32767, %eax
	cmovg	%eax, %esi
	movl	$1, %eax
	movw	%si, (%r14)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rsp), %r9
	movq	8(%rsp), %r10
	movl	28(%rsp), %esi
	movq	208(%r9), %rax
	movq	%rbx, 32(%rax,%r10)
	movl	$-1, 8(%rbx)
	movl	%ebp, 12(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%rbx, %rdi
	movq	%r10, 8(%rsp)
	movq	%r9, (%rsp)
	call	free@PLT
	movq	(%rsp), %r9
	movq	8(%rsp), %r10
	movq	208(%r9), %rax
	movq	$0, 32(%rax,%r10)
	jmp	.L94
	.size	__GI_authdes_getucred, .-__GI_authdes_getucred
	.globl	__EI_authdes_getucred
	.set	__EI_authdes_getucred,__GI_authdes_getucred
	.globl	svcauthdes_stats
	.bss
	.align 16
	.type	svcauthdes_stats, @object
	.size	svcauthdes_stats, 24
svcauthdes_stats:
	.zero	24
	.hidden	__rpc_thread_variables
