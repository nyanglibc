	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_des_setparity, des_setparity@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_des_setparity
	.hidden	__GI_des_setparity
	.type	__GI_des_setparity, @function
__GI_des_setparity:
	leaq	8(%rdi), %rcx
	leaq	partab(%rip), %rdx
	.p2align 4,,10
	.p2align 3
.L2:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	andl	$127, %eax
	movzbl	(%rdx,%rax), %eax
	movb	%al, -1(%rdi)
	cmpq	%rcx, %rdi
	jne	.L2
	rep ret
	.size	__GI_des_setparity, .-__GI_des_setparity
	.globl	__EI_des_setparity
	.set	__EI_des_setparity,__GI_des_setparity
	.section	.rodata
	.align 32
	.type	partab, @object
	.size	partab, 128
partab:
	.byte	1
	.byte	1
	.byte	2
	.byte	2
	.byte	4
	.byte	4
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	11
	.byte	11
	.byte	13
	.byte	13
	.byte	14
	.byte	14
	.byte	16
	.byte	16
	.byte	19
	.byte	19
	.byte	21
	.byte	21
	.byte	22
	.byte	22
	.byte	25
	.byte	25
	.byte	26
	.byte	26
	.byte	28
	.byte	28
	.byte	31
	.byte	31
	.byte	32
	.byte	32
	.byte	35
	.byte	35
	.byte	37
	.byte	37
	.byte	38
	.byte	38
	.byte	41
	.byte	41
	.byte	42
	.byte	42
	.byte	44
	.byte	44
	.byte	47
	.byte	47
	.byte	49
	.byte	49
	.byte	50
	.byte	50
	.byte	52
	.byte	52
	.byte	55
	.byte	55
	.byte	56
	.byte	56
	.byte	59
	.byte	59
	.byte	61
	.byte	61
	.byte	62
	.byte	62
	.byte	64
	.byte	64
	.byte	67
	.byte	67
	.byte	69
	.byte	69
	.byte	70
	.byte	70
	.byte	73
	.byte	73
	.byte	74
	.byte	74
	.byte	76
	.byte	76
	.byte	79
	.byte	79
	.byte	81
	.byte	81
	.byte	82
	.byte	82
	.byte	84
	.byte	84
	.byte	87
	.byte	87
	.byte	88
	.byte	88
	.byte	91
	.byte	91
	.byte	93
	.byte	93
	.byte	94
	.byte	94
	.byte	97
	.byte	97
	.byte	98
	.byte	98
	.byte	100
	.byte	100
	.byte	103
	.byte	103
	.byte	104
	.byte	104
	.byte	107
	.byte	107
	.byte	109
	.byte	109
	.byte	110
	.byte	110
	.byte	112
	.byte	112
	.byte	115
	.byte	115
	.byte	117
	.byte	117
	.byte	118
	.byte	118
	.byte	121
	.byte	121
	.byte	122
	.byte	122
	.byte	124
	.byte	124
	.byte	127
	.byte	127
