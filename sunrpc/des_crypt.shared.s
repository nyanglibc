	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_cbc_crypt, cbc_crypt@GLIBC_2.2.5
	.symver __EI_ecb_crypt, ecb_crypt@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_cbc_crypt
	.hidden	__GI_cbc_crypt
	.type	__GI_cbc_crypt, @function
__GI_cbc_crypt:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %rax
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %esi
	subq	$72, %rsp
	movzbl	(%r8), %r13d
	movzbl	1(%r8), %r12d
	movzbl	2(%r8), %ebp
	movzbl	3(%r8), %ebx
	testb	$7, %sil
	movzbl	4(%r8), %r11d
	movzbl	5(%r8), %r10d
	movzbl	6(%r8), %r9d
	movzbl	7(%r8), %edx
	movl	$0, 28(%rsp)
	movb	%r13b, 32(%rsp)
	movb	%r12b, 33(%rsp)
	movb	%bpl, 34(%rsp)
	movb	%bl, 35(%rsp)
	movb	%r11b, 36(%rsp)
	movb	%r10b, 37(%rsp)
	movb	%r9b, 38(%rsp)
	movb	%dl, 39(%rsp)
	jne	.L4
	cmpl	$8192, %esi
	ja	.L4
	movl	%ecx, %edx
	movq	%r8, 8(%rsp)
	movl	%ecx, 4(%rsp)
	andl	$1, %edx
	movl	%edx, 24(%rsp)
	movzbl	(%rdi), %edx
	movb	%dl, 16(%rsp)
	movzbl	1(%rdi), %edx
	movb	%dl, 17(%rsp)
	movzbl	2(%rdi), %edx
	movb	%dl, 18(%rsp)
	movzbl	3(%rdi), %edx
	movb	%dl, 19(%rsp)
	movzbl	4(%rdi), %edx
	movb	%dl, 20(%rsp)
	movzbl	5(%rdi), %edx
	movb	%dl, 21(%rsp)
	movzbl	6(%rdi), %edx
	movb	%dl, 22(%rsp)
	movzbl	7(%rdi), %edx
	movq	%rax, %rdi
	movb	%dl, 23(%rsp)
	leaq	16(%rsp), %rdx
	call	_des_crypt@PLT
	testl	%eax, %eax
	movl	4(%rsp), %ecx
	movq	8(%rsp), %r8
	je	.L7
	xorl	%eax, %eax
	andl	$2, %ecx
	movzbl	32(%rsp), %r13d
	sete	%al
	movzbl	33(%rsp), %r12d
	movzbl	34(%rsp), %ebp
	movzbl	35(%rsp), %ebx
	movzbl	36(%rsp), %r11d
	movzbl	37(%rsp), %r10d
	movzbl	38(%rsp), %r9d
	movzbl	39(%rsp), %edx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$3, %eax
.L2:
	movb	%r13b, (%r8)
	movb	%r12b, 1(%r8)
	movb	%bpl, 2(%r8)
	movb	%bl, 3(%r8)
	movb	%r11b, 4(%r8)
	movb	%r10b, 5(%r8)
	movb	%r9b, 6(%r8)
	movb	%dl, 7(%r8)
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movzbl	32(%rsp), %r13d
	movzbl	33(%rsp), %r12d
	movl	$2, %eax
	movzbl	34(%rsp), %ebp
	movzbl	35(%rsp), %ebx
	movzbl	36(%rsp), %r11d
	movzbl	37(%rsp), %r10d
	movzbl	38(%rsp), %r9d
	movzbl	39(%rsp), %edx
	jmp	.L2
	.size	__GI_cbc_crypt, .-__GI_cbc_crypt
	.globl	__EI_cbc_crypt
	.set	__EI_cbc_crypt,__GI_cbc_crypt
	.p2align 4,,15
	.globl	__GI_ecb_crypt
	.hidden	__GI_ecb_crypt
	.type	__GI_ecb_crypt, @function
__GI_ecb_crypt:
	pushq	%rbx
	subq	$48, %rsp
	testb	$7, %dl
	movl	$1, 12(%rsp)
	jne	.L10
	cmpl	$8192, %edx
	ja	.L10
	movl	%edx, %r8d
	movl	%ecx, %edx
	movzbl	7(%rdi), %eax
	andl	$1, %edx
	movq	%rsi, %r9
	movl	%r8d, %esi
	movl	%edx, 8(%rsp)
	movzbl	(%rdi), %edx
	movl	%ecx, %ebx
	movb	%al, 7(%rsp)
	movb	%dl, (%rsp)
	movzbl	1(%rdi), %edx
	movb	%dl, 1(%rsp)
	movzbl	2(%rdi), %edx
	movb	%dl, 2(%rsp)
	movzbl	3(%rdi), %edx
	movb	%dl, 3(%rsp)
	movzbl	4(%rdi), %edx
	movb	%dl, 4(%rsp)
	movzbl	5(%rdi), %edx
	movb	%dl, 5(%rsp)
	movzbl	6(%rdi), %edx
	movq	%r9, %rdi
	movb	%dl, 6(%rsp)
	movq	%rsp, %rdx
	call	_des_crypt@PLT
	movl	%eax, %edx
	movl	$2, %eax
	testl	%edx, %edx
	je	.L8
	xorl	%eax, %eax
	andl	$2, %ebx
	sete	%al
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$3, %eax
.L8:
	addq	$48, %rsp
	popq	%rbx
	ret
	.size	__GI_ecb_crypt, .-__GI_ecb_crypt
	.globl	__EI_ecb_crypt
	.set	__EI_ecb_crypt,__GI_ecb_crypt
