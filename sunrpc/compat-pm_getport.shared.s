	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_pmap_getport, pmap_getport@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__get_socket
	.hidden	__get_socket
	.type	__get_socket, @function
__get_socket:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$6, %edx
	movl	$1, %esi
	movl	$2, %edi
	subq	$24, %rsp
	call	__GI___socket
	testl	%eax, %eax
	js	.L5
	movq	%rsp, %rsi
	movl	$16, %edx
	movl	%eax, %edi
	movl	%eax, %ebx
	movq	$2, (%rsp)
	call	__bind
	testl	%eax, %eax
	js	.L4
	movl	$16, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	call	__GI___connect
	testl	%eax, %eax
	js	.L4
.L1:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ebx, %edi
	movl	$-1, %ebx
	call	__GI___close
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %ebx
	jmp	.L1
	.size	__get_socket, .-__get_socket
	.p2align 4,,15
	.globl	__GI___libc_rpc_getport
	.hidden	__GI___libc_rpc_getport
	.type	__GI___libc_rpc_getport, @function
__GI___libc_rpc_getport:
	pushq	%r15
	pushq	%r14
	movl	$28416, %r10d
	pushq	%r13
	pushq	%r12
	movq	%r9, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%r9d, %r9d
	xorl	%r14d, %r14d
	movq	%rdi, %rbx
	movq	%rsi, %r15
	subq	$88, %rsp
	cmpl	$6, %ecx
	movl	%ecx, %ebp
	movq	%rdx, 16(%rsp)
	movw	%r9w, 42(%rsp)
	movl	$-1, 44(%rsp)
	movw	%r10w, 2(%rdi)
	je	.L21
	pushq	$400
	pushq	$400
	movq	%r8, %rcx
	movl	$100000, %esi
	xorl	%r8d, %r8d
	movl	$2, %edx
	leaq	60(%rsp), %r9
	call	__GI_clntudp_bufcreate
	popq	%rsi
	popq	%rdi
	testq	%rax, %rax
	movq	%rax, %r12
	movb	$0, 15(%rsp)
	je	.L12
.L10:
	call	__GI___rpc_thread_createerr
	movq	%rax, 24(%rsp)
	movq	16(%rsp), %rax
	leaq	48(%rsp), %rcx
	movq	%r15, 48(%rsp)
	movq	$0, 72(%rsp)
	leaq	__GI_xdr_pmap(%rip), %rdx
	leaq	__GI_xdr_u_short(%rip), %r8
	movl	$3, %esi
	movq	%r12, %rdi
	movq	%rax, 56(%rsp)
	movl	%ebp, %eax
	movq	%rax, 64(%rsp)
	movq	8(%r12), %rax
	pushq	%r14
	pushq	%r13
	leaq	58(%rsp), %r9
	call	*(%rax)
	popq	%rdx
	testl	%eax, %eax
	popq	%rcx
	movq	24(%rsp), %r11
	je	.L13
	movq	8(%r12), %rax
	movl	$14, (%r11)
	leaq	8(%r11), %rsi
	movq	%r12, %rdi
	call	*16(%rax)
.L14:
	movq	8(%r12), %rax
	movq	%r12, %rdi
	call	*32(%rax)
.L11:
	cmpb	$0, 15(%rsp)
	je	.L12
	movl	44(%rsp), %edi
	call	__GI___close
.L12:
	xorl	%eax, %eax
	movw	%ax, 2(%rbx)
	movzwl	42(%rsp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	call	__get_socket
	leaq	44(%rsp), %rcx
	cmpl	$-1, %eax
	movl	$400, %r9d
	movl	$400, %r8d
	movl	$2, %edx
	movl	$100000, %esi
	movq	%rbx, %rdi
	setne	15(%rsp)
	movl	%eax, 44(%rsp)
	call	__GI_clnttcp_create
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	cmpw	$0, 42(%rsp)
	jne	.L14
	movl	$15, (%r11)
	jmp	.L14
	.size	__GI___libc_rpc_getport, .-__GI___libc_rpc_getport
	.globl	__libc_rpc_getport
	.set	__libc_rpc_getport,__GI___libc_rpc_getport
	.p2align 4,,15
	.globl	__GI_pmap_getport
	.hidden	__GI_pmap_getport
	.type	__GI_pmap_getport, @function
__GI_pmap_getport:
	movl	$60, %r9d
	movl	$5, %r8d
	jmp	__GI___libc_rpc_getport
	.size	__GI_pmap_getport, .-__GI_pmap_getport
	.globl	__EI_pmap_getport
	.set	__EI_pmap_getport,__GI_pmap_getport
	.hidden	__bind
