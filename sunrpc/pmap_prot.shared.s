	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_pmap, xdr_pmap@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_pmap
	.hidden	__GI_xdr_pmap
	.type	__GI_xdr_pmap, @function
__GI_xdr_pmap:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_u_long
	testl	%eax, %eax
	jne	.L13
.L3:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L3
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L3
	addq	$8, %rsp
	leaq	24(%rbx), %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_u_long
	.size	__GI_xdr_pmap, .-__GI_xdr_pmap
	.globl	__EI_xdr_pmap
	.set	__EI_xdr_pmap,__GI_xdr_pmap
