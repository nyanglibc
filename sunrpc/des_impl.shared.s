	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	des_encrypt, @function
des_encrypt:
	movq	8(%rdi), %r8
	movq	(%rdi), %rax
	pushq	%rbx
	movq	%r8, %r10
	shrq	$4, %r10
	xorq	%rax, %r10
	andl	$252645135, %r10d
	xorq	%r10, %rax
	salq	$4, %r10
	movq	%rax, %r9
	xorq	%r10, %r8
	shrq	$16, %r9
	xorq	%r8, %r9
	movzwl	%r9w, %ecx
	xorq	%rcx, %r8
	salq	$16, %rcx
	movq	%r8, %r9
	xorq	%rcx, %rax
	movq	%r8, %r10
	shrq	$2, %r9
	xorq	%rax, %r9
	andl	$858993459, %r9d
	leaq	0(,%r9,4), %r8
	xorq	%r9, %rax
	movq	%rax, %rcx
	shrq	$8, %rax
	xorq	%r10, %r8
	xorq	%r8, %rax
	andl	$16711935, %eax
	xorq	%rax, %r8
	salq	$8, %rax
	xorq	%rcx, %rax
	movq	%r8, %rcx
	shrq	%rcx
	xorq	%rax, %rcx
	andl	$1431655765, %ecx
	xorq	%rcx, %rax
	addq	%rcx, %rcx
	xorq	%rcx, %r8
	leaq	(%rax,%rax), %rcx
	shrq	$31, %rax
	orq	%rcx, %rax
	leaq	(%r8,%r8), %rcx
	shrq	$31, %r8
	orq	%rcx, %r8
	testl	%edx, %edx
	movl	%eax, %ecx
	movl	%r8d, %r8d
	je	.L2
	leaq	256(%rsi), %r9
	leaq	des_SPtrans(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rsi), %r10
	movq	(%rsi), %r11
	xorq	%rcx, %r10
	xorq	%rcx, %r11
	movq	%r10, %rdx
	salq	$28, %r10
	movq	%r11, %rbx
	shrq	$4, %rdx
	shrq	$16, %rbx
	addq	%rdx, %r10
	movq	%r11, %rdx
	andl	$63, %ebx
	shrq	$8, %rdx
	andl	$63, %edx
	movl	512(%rax,%rdx,4), %edx
	orl	1024(%rax,%rbx,4), %edx
	movq	%r11, %rbx
	andl	$63, %ebx
	shrq	$24, %r11
	orl	(%rax,%rbx,4), %edx
	andl	$63, %r11d
	orl	1536(%rax,%r11,4), %edx
	movq	%r10, %r11
	andl	$63, %r11d
	orl	256(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$8, %r11
	andl	$63, %r11d
	orl	768(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$24, %r10
	shrq	$16, %r11
	andl	$63, %r10d
	andl	$63, %r11d
	orl	1280(%rax,%r11,4), %edx
	movq	16(%rsi), %r11
	orl	1792(%rax,%r10,4), %edx
	movq	24(%rsi), %r10
	xorq	%rdx, %r8
	xorq	%r8, %r10
	xorq	%r8, %r11
	movq	%r10, %rdx
	salq	$28, %r10
	movq	%r11, %rbx
	shrq	$4, %rdx
	addq	%rdx, %r10
	movq	%r11, %rdx
	shrq	$8, %rdx
	shrq	$16, %rbx
	addq	$32, %rsi
	andl	$63, %ebx
	andl	$63, %edx
	movl	512(%rax,%rdx,4), %edx
	orl	1024(%rax,%rbx,4), %edx
	movq	%r11, %rbx
	andl	$63, %ebx
	shrq	$24, %r11
	andl	$63, %r11d
	orl	(%rax,%rbx,4), %edx
	orl	1536(%rax,%r11,4), %edx
	movq	%r10, %r11
	andl	$63, %r11d
	orl	256(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$8, %r11
	andl	$63, %r11d
	orl	768(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$24, %r10
	shrq	$16, %r11
	andl	$63, %r10d
	andl	$63, %r11d
	orl	1280(%rax,%r11,4), %edx
	orl	1792(%rax,%r10,4), %edx
	xorq	%rdx, %rcx
	cmpq	%rsi, %r9
	jne	.L3
.L4:
	movq	%rcx, %rax
	salq	$31, %rcx
	movq	%r8, %rsi
	shrq	%rax
	shrq	%rsi
	salq	$31, %r8
	orq	%rax, %rcx
	orq	%rsi, %r8
	movl	%ecx, %ecx
	movl	%r8d, %r8d
	movq	%rcx, %rdx
	movq	%r8, %rsi
	shrq	%rdx
	xorq	%r8, %rdx
	andl	$1431655765, %edx
	xorq	%rdx, %rsi
	leaq	(%rdx,%rdx), %rax
	movq	%rsi, %r8
	xorq	%rcx, %rax
	shrq	$8, %r8
	xorq	%rax, %r8
	andl	$16711935, %r8d
	xorq	%r8, %rax
	salq	$8, %r8
	movq	%rax, %rdx
	xorq	%rsi, %r8
	movq	%rax, %rcx
	shrq	$2, %rdx
	xorq	%r8, %rdx
	andl	$858993459, %edx
	xorq	%rdx, %r8
	leaq	0(,%rdx,4), %rax
	movq	%r8, %rdx
	xorq	%rcx, %rax
	shrq	$16, %rdx
	xorq	%rax, %rdx
	movzwl	%dx, %edx
	xorq	%rdx, %rax
	salq	$16, %rdx
	xorq	%rdx, %r8
	movq	%rax, %rdx
	shrq	$4, %rdx
	xorq	%r8, %rdx
	andl	$252645135, %edx
	xorq	%rdx, %r8
	salq	$4, %rdx
	xorq	%rdx, %rax
	movq	%r8, (%rdi)
	movq	%rax, 8(%rdi)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	des_SPtrans(%rip), %rax
	leaq	224(%rsi), %r9
	subq	$32, %rsi
	.p2align 4,,10
	.p2align 3
.L5:
	movq	24(%r9), %r10
	movq	16(%r9), %r11
	xorq	%rcx, %r10
	xorq	%rcx, %r11
	movq	%r10, %rdx
	salq	$28, %r10
	movq	%r11, %rbx
	shrq	$4, %rdx
	shrq	$16, %rbx
	addq	%rdx, %r10
	movq	%r11, %rdx
	andl	$63, %ebx
	shrq	$8, %rdx
	andl	$63, %edx
	movl	512(%rax,%rdx,4), %edx
	orl	1024(%rax,%rbx,4), %edx
	movq	%r11, %rbx
	andl	$63, %ebx
	shrq	$24, %r11
	orl	(%rax,%rbx,4), %edx
	andl	$63, %r11d
	orl	1536(%rax,%r11,4), %edx
	movq	%r10, %r11
	andl	$63, %r11d
	orl	256(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$8, %r11
	andl	$63, %r11d
	orl	768(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$24, %r10
	shrq	$16, %r11
	andl	$63, %r10d
	andl	$63, %r11d
	orl	1280(%rax,%r11,4), %edx
	movq	(%r9), %r11
	orl	1792(%rax,%r10,4), %edx
	movq	8(%r9), %r10
	xorq	%rdx, %r8
	xorq	%r8, %r10
	xorq	%r8, %r11
	movq	%r10, %rdx
	salq	$28, %r10
	movq	%r11, %rbx
	shrq	$4, %rdx
	addq	%rdx, %r10
	movq	%r11, %rdx
	shrq	$8, %rdx
	shrq	$16, %rbx
	subq	$32, %r9
	andl	$63, %ebx
	andl	$63, %edx
	movl	512(%rax,%rdx,4), %edx
	orl	1024(%rax,%rbx,4), %edx
	movq	%r11, %rbx
	andl	$63, %ebx
	shrq	$24, %r11
	andl	$63, %r11d
	orl	(%rax,%rbx,4), %edx
	orl	1536(%rax,%r11,4), %edx
	movq	%r10, %r11
	andl	$63, %r11d
	orl	256(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$8, %r11
	andl	$63, %r11d
	orl	768(%rax,%r11,4), %edx
	movq	%r10, %r11
	shrq	$24, %r10
	shrq	$16, %r11
	andl	$63, %r10d
	andl	$63, %r11d
	orl	1280(%rax,%r11,4), %edx
	orl	1792(%rax,%r10,4), %edx
	xorq	%rdx, %rcx
	cmpq	%r9, %rsi
	jne	.L5
	jmp	.L4
	.size	des_encrypt, .-des_encrypt
	.p2align 4,,15
	.globl	_des_crypt
	.type	_des_crypt, @function
_des_crypt:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	1+shifts2(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$344, %rsp
	movl	(%rdx), %r8d
	movl	12(%rdx), %r14d
	movl	4(%rdx), %edx
	leaq	80(%rsp), %rbx
	movq	%rbx, 8(%rsp)
	movq	%rdx, %r11
	shrq	$4, %r11
	xorq	%r8, %r11
	andl	$252645135, %r11d
	xorq	%r11, %r8
	salq	$4, %r11
	xorq	%rdx, %r11
	movq	%r8, %rax
	movq	%r8, %r10
	movq	%r11, %rdx
	salq	$18, %rax
	salq	$18, %rdx
	xorq	%r8, %rax
	xorq	%r11, %rdx
	andl	$3435921408, %eax
	andl	$3435921408, %edx
	xorq	%rax, %r10
	shrq	$18, %rax
	xorq	%rdx, %r11
	shrq	$18, %rdx
	movq	%rax, %r8
	xorq	%rdx, %r11
	xorq	%r10, %r8
	movq	%r11, %rax
	shrq	%rax
	xorq	%r8, %rax
	andl	$1431655765, %eax
	xorq	%rax, %r8
	leaq	(%rax,%rax), %rdx
	movq	%r8, %r10
	xorq	%r11, %rdx
	shrq	$8, %r10
	leaq	des_skb(%rip), %r11
	xorq	%rdx, %r10
	andl	$16711935, %r10d
	xorq	%r10, %rdx
	salq	$8, %r10
	movq	%rdx, %rax
	xorq	%r8, %r10
	movq	%rdx, %rcx
	shrq	%rax
	xorq	%r10, %rax
	andl	$1431655765, %eax
	leaq	(%rax,%rax), %rdx
	xorq	%rax, %r10
	xorq	%rcx, %rdx
	movq	%rdx, %rax
	movq	%rdx, %r8
	salq	$16, %rdx
	shrq	$16, %rax
	andl	$65280, %r8d
	andl	$16711680, %edx
	movzbl	%al, %ecx
	movq	%r10, %rax
	andl	$268435455, %r10d
	shrq	$4, %rax
	andl	$251658240, %eax
	orq	%r8, %rax
	orq	%rax, %rdx
	movq	%rcx, %rax
	orq	%rdx, %rax
	xorl	%edx, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rax, %r8
	shrq	$2, %rcx
	salq	$26, %r10
	shrq	$2, %r8
	salq	$26, %rax
	orq	%r10, %rcx
	orq	%rax, %r8
.L12:
	movq	%rcx, %r10
	andl	$63, %ecx
	movq	%r8, %rax
	andl	$268435455, %r10d
	andl	$268435455, %eax
	andl	$63, %r8d
	movq	%r10, %rdx
	movq	%r10, %r13
	shrq	$6, %rdx
	shrq	$14, %r13
	andl	$3, %edx
	andl	$48, %r13d
	movq	%rdx, %r9
	movq	%r10, %rdx
	shrq	$7, %rdx
	andl	$60, %edx
	orq	%r9, %rdx
	movq	%r10, %r9
	shrq	$13, %r9
	movl	256(%r11,%rdx,4), %edx
	andl	$15, %r9d
	orq	%r13, %r9
	orl	512(%r11,%r9,4), %edx
	movq	%r10, %r9
	orl	(%r11,%rcx,4), %edx
	movq	%r10, %rcx
	shrq	$21, %r9
	shrq	$20, %rcx
	andl	$6, %r9d
	andl	$1, %ecx
	orq	%r9, %rcx
	movq	%r10, %r9
	shrq	$22, %r9
	andl	$56, %r9d
	orq	%r9, %rcx
	movq	%rax, %r9
	orl	768(%r11,%rcx,4), %edx
	movq	%rax, %rcx
	shrq	$7, %r9
	shrq	$8, %rcx
	andl	$3, %r9d
	andl	$60, %ecx
	orq	%rcx, %r9
	movq	%rax, %rcx
	shrq	$15, %rcx
	andl	$63, %ecx
	movl	1536(%r11,%rcx,4), %ecx
	orl	1024(%r11,%r8,4), %ecx
	movq	%rax, %r8
	orl	1280(%r11,%r9,4), %ecx
	movq	%rax, %r9
	shrq	$21, %r8
	shrq	$22, %r9
	andl	$15, %r8d
	addq	$16, %rbx
	andl	$48, %r9d
	orq	%r9, %r8
	movzwl	%dx, %r9d
	shrq	$16, %rdx
	orl	1792(%r11,%r8,4), %ecx
	movl	%ecx, %r8d
	andl	$-65536, %ecx
	orq	%rcx, %rdx
	salq	$16, %r8
	movq	%rdx, %rcx
	orq	%r9, %r8
	shrq	$28, %rdx
	salq	$4, %rcx
	andl	$4294967295, %r8d
	orq	%rcx, %rdx
	movq	%r8, -16(%rbx)
	andl	$4294967295, %edx
	movq	%rdx, -8(%rbx)
	leaq	336(%rsp), %rdx
	cmpq	%rdx, %rbx
	je	.L13
	movzbl	(%r12), %edx
	addq	$1, %r12
.L14:
	testb	%dl, %dl
	movq	%r10, %rcx
	jne	.L29
	movq	%rax, %r8
	shrq	%rcx
	salq	$27, %r10
	shrq	%r8
	salq	$27, %rax
	orq	%r10, %rcx
	orq	%rax, %r8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	movl	16(%rbp), %r8d
	movl	8(%rbp), %r9d
	movl	20(%rbp), %ecx
	testl	%r9d, %r9d
	movq	%r8, %rdx
	jne	.L15
	testl	%esi, %esi
	je	.L16
	leaq	64(%rsp), %rax
	movq	%rbp, 24(%rsp)
	movq	%rdi, %r12
	movq	%rax, 16(%rsp)
	leal	(%rsi,%rdi), %eax
	movl	%eax, %ebp
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	8(%r12), %rbx
	testl	%r14d, %r14d
	movl	(%r12), %edx
	movl	4(%r12), %eax
	movq	%rbx, %r12
	jne	.L17
	xorq	%r8, %rdx
	xorq	%rcx, %rax
.L17:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	movq	%rdx, 64(%rsp)
	movl	$1, %edx
	movq	%rax, 72(%rsp)
	call	des_encrypt
	movq	64(%rsp), %r8
	movq	72(%rsp), %rcx
	movq	%r8, %rax
	movl	%r8d, %edx
	movb	%r8b, -8(%rbx)
	shrq	$8, %rax
	movl	%ecx, %r15d
	movb	%cl, -4(%rbx)
	movl	%eax, %r11d
	movb	%al, -7(%rbx)
	movq	%r8, %rax
	shrq	$16, %rax
	movl	%eax, %r10d
	movb	%al, -6(%rbx)
	movq	%r8, %rax
	shrq	$24, %rax
	movl	%eax, %r13d
	movb	%al, -5(%rbx)
	movq	%rcx, %rax
	shrq	$8, %rax
	movl	%eax, %r9d
	movb	%al, -3(%rbx)
	movq	%rcx, %rax
	shrq	$16, %rax
	movl	%eax, %edi
	movb	%al, -2(%rbx)
	movq	%rcx, %rax
	shrq	$24, %rax
	cmpl	%ebx, %ebp
	movl	%eax, %esi
	movb	%al, -1(%rbx)
	jne	.L18
	movq	24(%rsp), %rbp
.L19:
	movb	%dl, 16(%rbp)
	movb	%r11b, 17(%rbp)
	movb	%r10b, 18(%rbp)
	movb	%r13b, 19(%rbp)
	movb	%r15b, 20(%rbp)
	movb	%r9b, 21(%rbp)
	movb	%dil, 22(%rbp)
	movb	%sil, 23(%rbp)
.L20:
	addq	$344, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	testl	%esi, %esi
	je	.L25
	leaq	64(%rsp), %rax
	movq	%rbp, 56(%rsp)
	movl	%r14d, 36(%rsp)
	movq	%rcx, 40(%rsp)
	movq	%r8, 48(%rsp)
	movq	%rdi, %rbp
	movq	%rax, 16(%rsp)
	leal	(%rsi,%rdi), %eax
	movl	%eax, 24(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	movq	48(%rsp), %rsi
	movq	40(%rsp), %rdx
	xorq	64(%rsp), %rsi
	xorq	72(%rsp), %rdx
	movq	%r14, 40(%rsp)
	movq	%r12, 48(%rsp)
.L23:
	movq	%rsi, %rdi
	movb	%sil, -8(%r15)
	movb	%dl, -4(%r15)
	shrq	$8, %rdi
	movb	%dil, -7(%r15)
	movq	%rsi, %rdi
	shrq	$24, %rsi
	movb	%sil, -5(%r15)
	movq	%rdx, %rsi
	shrq	$16, %rdi
	shrq	$8, %rsi
	movb	%dil, -6(%r15)
	movb	%sil, -3(%r15)
	movq	%rdx, %rsi
	shrq	$24, %rdx
	shrq	$16, %rsi
	cmpl	%r15d, 24(%rsp)
	movb	%dl, -1(%r15)
	movb	%sil, -2(%r15)
	je	.L30
.L24:
	movl	0(%rbp), %r12d
	movl	4(%rbp), %r14d
	xorl	%edx, %edx
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	leaq	8(%rbp), %r15
	movq	%r15, %rbp
	movq	%r12, 64(%rsp)
	movq	%r14, 72(%rsp)
	movq	%r12, %r13
	call	des_encrypt
	movl	36(%rsp), %eax
	movq	%r14, %rbx
	testl	%eax, %eax
	je	.L31
	movq	64(%rsp), %rsi
	movq	72(%rsp), %rdx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r12, %rax
	movq	%r14, %r15
	movq	56(%rsp), %rbp
	movq	%r12, %r11
	movq	%r12, %rdx
	movzbl	%ah, %ecx
	shrq	$24, %r15
	movq	%r14, %rax
	shrq	$16, %rdx
	movzbl	%ah, %esi
	shrq	$24, %r11
	movl	%ebx, %r12d
	shrq	$16, %rax
	movq	%r15, %r10
.L21:
	movb	%r13b, 16(%rbp)
	movb	%cl, 17(%rbp)
	movb	%dl, 18(%rbp)
	movb	%r11b, 19(%rbp)
	movb	%r12b, 20(%rbp)
	movb	%sil, 21(%rbp)
	movb	%al, 22(%rbp)
	movb	%r10b, 23(%rbp)
	jmp	.L20
.L25:
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%r12d, %r12d
	xorl	%r11d, %r11d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	jmp	.L21
.L16:
	movzbl	%dh, %edi
	movq	%r8, %r10
	movzbl	%ch, %eax
	movq	%rdi, %r11
	shrq	$24, %r8
	movq	%rcx, %rdi
	movl	%ecx, %r15d
	shrq	$24, %rcx
	shrq	$16, %r10
	movl	%r8d, %r13d
	movq	%rax, %r9
	shrq	$16, %rdi
	movl	%ecx, %esi
	jmp	.L19
	.size	_des_crypt, .-_des_crypt
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	shifts2, @object
	.size	shifts2, 16
shifts2:
	.byte	0
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	1
	.byte	0
	.section	.rodata
	.align 32
	.type	des_skb, @object
	.size	des_skb, 2048
des_skb:
	.long	0
	.long	16
	.long	536870912
	.long	536870928
	.long	65536
	.long	65552
	.long	536936448
	.long	536936464
	.long	2048
	.long	2064
	.long	536872960
	.long	536872976
	.long	67584
	.long	67600
	.long	536938496
	.long	536938512
	.long	32
	.long	48
	.long	536870944
	.long	536870960
	.long	65568
	.long	65584
	.long	536936480
	.long	536936496
	.long	2080
	.long	2096
	.long	536872992
	.long	536873008
	.long	67616
	.long	67632
	.long	536938528
	.long	536938544
	.long	524288
	.long	524304
	.long	537395200
	.long	537395216
	.long	589824
	.long	589840
	.long	537460736
	.long	537460752
	.long	526336
	.long	526352
	.long	537397248
	.long	537397264
	.long	591872
	.long	591888
	.long	537462784
	.long	537462800
	.long	524320
	.long	524336
	.long	537395232
	.long	537395248
	.long	589856
	.long	589872
	.long	537460768
	.long	537460784
	.long	526368
	.long	526384
	.long	537397280
	.long	537397296
	.long	591904
	.long	591920
	.long	537462816
	.long	537462832
	.long	0
	.long	33554432
	.long	8192
	.long	33562624
	.long	2097152
	.long	35651584
	.long	2105344
	.long	35659776
	.long	4
	.long	33554436
	.long	8196
	.long	33562628
	.long	2097156
	.long	35651588
	.long	2105348
	.long	35659780
	.long	1024
	.long	33555456
	.long	9216
	.long	33563648
	.long	2098176
	.long	35652608
	.long	2106368
	.long	35660800
	.long	1028
	.long	33555460
	.long	9220
	.long	33563652
	.long	2098180
	.long	35652612
	.long	2106372
	.long	35660804
	.long	268435456
	.long	301989888
	.long	268443648
	.long	301998080
	.long	270532608
	.long	304087040
	.long	270540800
	.long	304095232
	.long	268435460
	.long	301989892
	.long	268443652
	.long	301998084
	.long	270532612
	.long	304087044
	.long	270540804
	.long	304095236
	.long	268436480
	.long	301990912
	.long	268444672
	.long	301999104
	.long	270533632
	.long	304088064
	.long	270541824
	.long	304096256
	.long	268436484
	.long	301990916
	.long	268444676
	.long	301999108
	.long	270533636
	.long	304088068
	.long	270541828
	.long	304096260
	.long	0
	.long	1
	.long	262144
	.long	262145
	.long	16777216
	.long	16777217
	.long	17039360
	.long	17039361
	.long	2
	.long	3
	.long	262146
	.long	262147
	.long	16777218
	.long	16777219
	.long	17039362
	.long	17039363
	.long	512
	.long	513
	.long	262656
	.long	262657
	.long	16777728
	.long	16777729
	.long	17039872
	.long	17039873
	.long	514
	.long	515
	.long	262658
	.long	262659
	.long	16777730
	.long	16777731
	.long	17039874
	.long	17039875
	.long	134217728
	.long	134217729
	.long	134479872
	.long	134479873
	.long	150994944
	.long	150994945
	.long	151257088
	.long	151257089
	.long	134217730
	.long	134217731
	.long	134479874
	.long	134479875
	.long	150994946
	.long	150994947
	.long	151257090
	.long	151257091
	.long	134218240
	.long	134218241
	.long	134480384
	.long	134480385
	.long	150995456
	.long	150995457
	.long	151257600
	.long	151257601
	.long	134218242
	.long	134218243
	.long	134480386
	.long	134480387
	.long	150995458
	.long	150995459
	.long	151257602
	.long	151257603
	.long	0
	.long	1048576
	.long	256
	.long	1048832
	.long	8
	.long	1048584
	.long	264
	.long	1048840
	.long	4096
	.long	1052672
	.long	4352
	.long	1052928
	.long	4104
	.long	1052680
	.long	4360
	.long	1052936
	.long	67108864
	.long	68157440
	.long	67109120
	.long	68157696
	.long	67108872
	.long	68157448
	.long	67109128
	.long	68157704
	.long	67112960
	.long	68161536
	.long	67113216
	.long	68161792
	.long	67112968
	.long	68161544
	.long	67113224
	.long	68161800
	.long	131072
	.long	1179648
	.long	131328
	.long	1179904
	.long	131080
	.long	1179656
	.long	131336
	.long	1179912
	.long	135168
	.long	1183744
	.long	135424
	.long	1184000
	.long	135176
	.long	1183752
	.long	135432
	.long	1184008
	.long	67239936
	.long	68288512
	.long	67240192
	.long	68288768
	.long	67239944
	.long	68288520
	.long	67240200
	.long	68288776
	.long	67244032
	.long	68292608
	.long	67244288
	.long	68292864
	.long	67244040
	.long	68292616
	.long	67244296
	.long	68292872
	.long	0
	.long	268435456
	.long	65536
	.long	268500992
	.long	4
	.long	268435460
	.long	65540
	.long	268500996
	.long	536870912
	.long	805306368
	.long	536936448
	.long	805371904
	.long	536870916
	.long	805306372
	.long	536936452
	.long	805371908
	.long	1048576
	.long	269484032
	.long	1114112
	.long	269549568
	.long	1048580
	.long	269484036
	.long	1114116
	.long	269549572
	.long	537919488
	.long	806354944
	.long	537985024
	.long	806420480
	.long	537919492
	.long	806354948
	.long	537985028
	.long	806420484
	.long	4096
	.long	268439552
	.long	69632
	.long	268505088
	.long	4100
	.long	268439556
	.long	69636
	.long	268505092
	.long	536875008
	.long	805310464
	.long	536940544
	.long	805376000
	.long	536875012
	.long	805310468
	.long	536940548
	.long	805376004
	.long	1052672
	.long	269488128
	.long	1118208
	.long	269553664
	.long	1052676
	.long	269488132
	.long	1118212
	.long	269553668
	.long	537923584
	.long	806359040
	.long	537989120
	.long	806424576
	.long	537923588
	.long	806359044
	.long	537989124
	.long	806424580
	.long	0
	.long	134217728
	.long	8
	.long	134217736
	.long	1024
	.long	134218752
	.long	1032
	.long	134218760
	.long	131072
	.long	134348800
	.long	131080
	.long	134348808
	.long	132096
	.long	134349824
	.long	132104
	.long	134349832
	.long	1
	.long	134217729
	.long	9
	.long	134217737
	.long	1025
	.long	134218753
	.long	1033
	.long	134218761
	.long	131073
	.long	134348801
	.long	131081
	.long	134348809
	.long	132097
	.long	134349825
	.long	132105
	.long	134349833
	.long	33554432
	.long	167772160
	.long	33554440
	.long	167772168
	.long	33555456
	.long	167773184
	.long	33555464
	.long	167773192
	.long	33685504
	.long	167903232
	.long	33685512
	.long	167903240
	.long	33686528
	.long	167904256
	.long	33686536
	.long	167904264
	.long	33554433
	.long	167772161
	.long	33554441
	.long	167772169
	.long	33555457
	.long	167773185
	.long	33555465
	.long	167773193
	.long	33685505
	.long	167903233
	.long	33685513
	.long	167903241
	.long	33686529
	.long	167904257
	.long	33686537
	.long	167904265
	.long	0
	.long	256
	.long	524288
	.long	524544
	.long	16777216
	.long	16777472
	.long	17301504
	.long	17301760
	.long	16
	.long	272
	.long	524304
	.long	524560
	.long	16777232
	.long	16777488
	.long	17301520
	.long	17301776
	.long	2097152
	.long	2097408
	.long	2621440
	.long	2621696
	.long	18874368
	.long	18874624
	.long	19398656
	.long	19398912
	.long	2097168
	.long	2097424
	.long	2621456
	.long	2621712
	.long	18874384
	.long	18874640
	.long	19398672
	.long	19398928
	.long	512
	.long	768
	.long	524800
	.long	525056
	.long	16777728
	.long	16777984
	.long	17302016
	.long	17302272
	.long	528
	.long	784
	.long	524816
	.long	525072
	.long	16777744
	.long	16778000
	.long	17302032
	.long	17302288
	.long	2097664
	.long	2097920
	.long	2621952
	.long	2622208
	.long	18874880
	.long	18875136
	.long	19399168
	.long	19399424
	.long	2097680
	.long	2097936
	.long	2621968
	.long	2622224
	.long	18874896
	.long	18875152
	.long	19399184
	.long	19399440
	.long	0
	.long	67108864
	.long	262144
	.long	67371008
	.long	2
	.long	67108866
	.long	262146
	.long	67371010
	.long	8192
	.long	67117056
	.long	270336
	.long	67379200
	.long	8194
	.long	67117058
	.long	270338
	.long	67379202
	.long	32
	.long	67108896
	.long	262176
	.long	67371040
	.long	34
	.long	67108898
	.long	262178
	.long	67371042
	.long	8224
	.long	67117088
	.long	270368
	.long	67379232
	.long	8226
	.long	67117090
	.long	270370
	.long	67379234
	.long	2048
	.long	67110912
	.long	264192
	.long	67373056
	.long	2050
	.long	67110914
	.long	264194
	.long	67373058
	.long	10240
	.long	67119104
	.long	272384
	.long	67381248
	.long	10242
	.long	67119106
	.long	272386
	.long	67381250
	.long	2080
	.long	67110944
	.long	264224
	.long	67373088
	.long	2082
	.long	67110946
	.long	264226
	.long	67373090
	.long	10272
	.long	67119136
	.long	272416
	.long	67381280
	.long	10274
	.long	67119138
	.long	272418
	.long	67381282
	.align 32
	.type	des_SPtrans, @object
	.size	des_SPtrans, 2048
des_SPtrans:
	.long	8520192
	.long	131072
	.long	-2139095040
	.long	-2138963456
	.long	8388608
	.long	-2147352064
	.long	-2147352576
	.long	-2139095040
	.long	-2147352064
	.long	8520192
	.long	8519680
	.long	-2147483136
	.long	-2139094528
	.long	8388608
	.long	0
	.long	-2147352576
	.long	131072
	.long	-2147483648
	.long	8389120
	.long	131584
	.long	-2138963456
	.long	8519680
	.long	-2147483136
	.long	8389120
	.long	-2147483648
	.long	512
	.long	131584
	.long	-2138963968
	.long	512
	.long	-2139094528
	.long	-2138963968
	.long	0
	.long	0
	.long	-2138963456
	.long	8389120
	.long	-2147352576
	.long	8520192
	.long	131072
	.long	-2147483136
	.long	8389120
	.long	-2138963968
	.long	512
	.long	131584
	.long	-2139095040
	.long	-2147352064
	.long	-2147483648
	.long	-2139095040
	.long	8519680
	.long	-2138963456
	.long	131584
	.long	8519680
	.long	-2139094528
	.long	8388608
	.long	-2147483136
	.long	-2147352576
	.long	0
	.long	131072
	.long	8388608
	.long	-2139094528
	.long	8520192
	.long	-2147483648
	.long	-2138963968
	.long	512
	.long	-2147352064
	.long	268705796
	.long	0
	.long	270336
	.long	268697600
	.long	268435460
	.long	8196
	.long	268443648
	.long	270336
	.long	8192
	.long	268697604
	.long	4
	.long	268443648
	.long	262148
	.long	268705792
	.long	268697600
	.long	4
	.long	262144
	.long	268443652
	.long	268697604
	.long	8192
	.long	270340
	.long	268435456
	.long	0
	.long	262148
	.long	268443652
	.long	270340
	.long	268705792
	.long	268435460
	.long	268435456
	.long	262144
	.long	8196
	.long	268705796
	.long	262148
	.long	268705792
	.long	268443648
	.long	270340
	.long	268705796
	.long	262148
	.long	268435460
	.long	0
	.long	268435456
	.long	8196
	.long	262144
	.long	268697604
	.long	8192
	.long	268435456
	.long	270340
	.long	268443652
	.long	268705792
	.long	8192
	.long	0
	.long	268435460
	.long	4
	.long	268705796
	.long	270336
	.long	268697600
	.long	268697604
	.long	262144
	.long	8196
	.long	268443648
	.long	268443652
	.long	4
	.long	268697600
	.long	270336
	.long	1090519040
	.long	16842816
	.long	64
	.long	1090519104
	.long	1073807360
	.long	16777216
	.long	1090519104
	.long	65600
	.long	16777280
	.long	65536
	.long	16842752
	.long	1073741824
	.long	1090584640
	.long	1073741888
	.long	1073741824
	.long	1090584576
	.long	0
	.long	1073807360
	.long	16842816
	.long	64
	.long	1073741888
	.long	1090584640
	.long	65536
	.long	1090519040
	.long	1090584576
	.long	16777280
	.long	1073807424
	.long	16842752
	.long	65600
	.long	0
	.long	16777216
	.long	1073807424
	.long	16842816
	.long	64
	.long	1073741824
	.long	65536
	.long	1073741888
	.long	1073807360
	.long	16842752
	.long	1090519104
	.long	0
	.long	16842816
	.long	65600
	.long	1090584576
	.long	1073807360
	.long	16777216
	.long	1090584640
	.long	1073741824
	.long	1073807424
	.long	1090519040
	.long	16777216
	.long	1090584640
	.long	65536
	.long	16777280
	.long	1090519104
	.long	65600
	.long	16777280
	.long	0
	.long	1090584576
	.long	1073741888
	.long	1090519040
	.long	1073807424
	.long	64
	.long	16842752
	.long	1049602
	.long	67109888
	.long	2
	.long	68158466
	.long	0
	.long	68157440
	.long	67109890
	.long	1048578
	.long	68158464
	.long	67108866
	.long	67108864
	.long	1026
	.long	67108866
	.long	1049602
	.long	1048576
	.long	67108864
	.long	68157442
	.long	1049600
	.long	1024
	.long	2
	.long	1049600
	.long	67109890
	.long	68157440
	.long	1024
	.long	1026
	.long	0
	.long	1048578
	.long	68158464
	.long	67109888
	.long	68157442
	.long	68158466
	.long	1048576
	.long	68157442
	.long	1026
	.long	1048576
	.long	67108866
	.long	1049600
	.long	67109888
	.long	2
	.long	68157440
	.long	67109890
	.long	0
	.long	1024
	.long	1048578
	.long	0
	.long	68157442
	.long	68158464
	.long	1024
	.long	67108864
	.long	68158466
	.long	1049602
	.long	1048576
	.long	68158466
	.long	2
	.long	67109888
	.long	1049602
	.long	1048578
	.long	1049600
	.long	68157440
	.long	67109890
	.long	1026
	.long	67108864
	.long	67108866
	.long	68158464
	.long	33554432
	.long	16384
	.long	256
	.long	33571080
	.long	33570824
	.long	33554688
	.long	16648
	.long	33570816
	.long	16384
	.long	8
	.long	33554440
	.long	16640
	.long	33554696
	.long	33570824
	.long	33571072
	.long	0
	.long	16640
	.long	33554432
	.long	16392
	.long	264
	.long	33554688
	.long	16648
	.long	0
	.long	33554440
	.long	8
	.long	33554696
	.long	33571080
	.long	16392
	.long	33570816
	.long	256
	.long	264
	.long	33571072
	.long	33571072
	.long	33554696
	.long	16392
	.long	33570816
	.long	16384
	.long	8
	.long	33554440
	.long	33554688
	.long	33554432
	.long	16640
	.long	33571080
	.long	0
	.long	16648
	.long	33554432
	.long	256
	.long	16392
	.long	33554696
	.long	256
	.long	0
	.long	33571080
	.long	33570824
	.long	33571072
	.long	264
	.long	16384
	.long	16640
	.long	33570824
	.long	33554688
	.long	264
	.long	8
	.long	16648
	.long	33570816
	.long	33554440
	.long	536870928
	.long	524304
	.long	0
	.long	537397248
	.long	524304
	.long	2048
	.long	536872976
	.long	524288
	.long	2064
	.long	537397264
	.long	526336
	.long	536870912
	.long	536872960
	.long	536870928
	.long	537395200
	.long	526352
	.long	524288
	.long	536872976
	.long	537395216
	.long	0
	.long	2048
	.long	16
	.long	537397248
	.long	537395216
	.long	537397264
	.long	537395200
	.long	536870912
	.long	2064
	.long	16
	.long	526336
	.long	526352
	.long	536872960
	.long	2064
	.long	536870912
	.long	536872960
	.long	526352
	.long	537397248
	.long	524304
	.long	0
	.long	536872960
	.long	536870912
	.long	2048
	.long	537395216
	.long	524288
	.long	524304
	.long	537397264
	.long	526336
	.long	16
	.long	537397264
	.long	526336
	.long	524288
	.long	536872976
	.long	536870928
	.long	537395200
	.long	526352
	.long	0
	.long	2048
	.long	536870928
	.long	536872976
	.long	537397248
	.long	537395200
	.long	2064
	.long	16
	.long	537395216
	.long	4096
	.long	128
	.long	4194432
	.long	4194305
	.long	4198529
	.long	4097
	.long	4224
	.long	0
	.long	4194304
	.long	4194433
	.long	129
	.long	4198400
	.long	1
	.long	4198528
	.long	4198400
	.long	129
	.long	4194433
	.long	4096
	.long	4097
	.long	4198529
	.long	0
	.long	4194432
	.long	4194305
	.long	4224
	.long	4198401
	.long	4225
	.long	4198528
	.long	1
	.long	4225
	.long	4198401
	.long	128
	.long	4194304
	.long	4225
	.long	4198400
	.long	4198401
	.long	129
	.long	4096
	.long	128
	.long	4194304
	.long	4198401
	.long	4194433
	.long	4225
	.long	4224
	.long	0
	.long	128
	.long	4194305
	.long	1
	.long	4194432
	.long	0
	.long	4194433
	.long	4194432
	.long	4224
	.long	129
	.long	4096
	.long	4198529
	.long	4194304
	.long	4198528
	.long	1
	.long	4097
	.long	4198529
	.long	4194305
	.long	4198528
	.long	4198400
	.long	4097
	.long	136314912
	.long	136347648
	.long	32800
	.long	0
	.long	134250496
	.long	2097184
	.long	136314880
	.long	136347680
	.long	32
	.long	134217728
	.long	2129920
	.long	32800
	.long	2129952
	.long	134250528
	.long	134217760
	.long	136314880
	.long	32768
	.long	2129952
	.long	2097184
	.long	134250496
	.long	136347680
	.long	134217760
	.long	0
	.long	2129920
	.long	134217728
	.long	2097152
	.long	134250528
	.long	136314912
	.long	2097152
	.long	32768
	.long	136347648
	.long	32
	.long	2097152
	.long	32768
	.long	134217760
	.long	136347680
	.long	32800
	.long	134217728
	.long	0
	.long	2129920
	.long	136314912
	.long	134250528
	.long	134250496
	.long	2097184
	.long	136347648
	.long	32
	.long	2097184
	.long	134250496
	.long	136347680
	.long	2097152
	.long	136314880
	.long	134217760
	.long	2129920
	.long	32800
	.long	134250528
	.long	136314880
	.long	32
	.long	136347648
	.long	2129952
	.long	0
	.long	134217728
	.long	136314912
	.long	32768
	.long	2129952
