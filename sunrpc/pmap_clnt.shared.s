	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_pmap_set, pmap_set@GLIBC_2.2.5
	.symver __EI_pmap_unset, pmap_unset@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"get_myaddress: getifaddrs"
#NO_APP
	.text
	.p2align 4,,15
	.type	__get_myaddress, @function
__get_myaddress:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	leaq	8(%rsp), %rdi
	call	__GI_getifaddrs
	testl	%eax, %eax
	jne	.L23
.L2:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L8
	movl	$1, %esi
.L7:
	movl	%esi, %r8d
	movq	%rdi, %rax
	xorl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L6:
	movl	16(%rax), %edx
	testb	$1, %dl
	je	.L4
	movq	24(%rax), %rcx
	testq	%rcx, %rcx
	je	.L4
	cmpw	$2, (%rcx)
	je	.L24
.L4:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L6
	cmpl	$1, %esi
	jne	.L8
	xorl	%esi, %esi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L24:
	andl	$8, %edx
	jne	.L10
	testb	%r8b, %r8b
	je	.L4
.L10:
	movdqu	(%rcx), %xmm0
	movl	$28416, %eax
	movaps	%xmm0, (%rbx)
	movw	%ax, 2(%rbx)
	movl	$1, %ebx
	call	__GI_freeifaddrs
	addq	$16, %rsp
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
.L3:
	call	__GI_freeifaddrs
	xorl	%ebx, %ebx
	addq	$16, %rsp
	movl	%ebx, %eax
	popq	%rbx
	ret
.L23:
	leaq	.LC0(%rip), %rdi
	call	__GI_perror
	movl	$1, %edi
	call	__GI_exit
	.size	__get_myaddress, .-__get_myaddress
	.section	.rodata.str1.1
.LC1:
	.string	"Cannot register service"
	.text
	.p2align 4,,15
	.globl	__GI_pmap_set
	.hidden	__GI_pmap_set
	.type	__GI_pmap_set, @function
__GI_pmap_set:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	movslq	%edx, %r12
	movzwl	%cx, %ebx
	subq	$64, %rsp
	leaq	16(%rsp), %rbp
	movl	$-1, 8(%rsp)
	movq	%rbp, %rdi
	call	__get_myaddress
	testl	%eax, %eax
	jne	.L26
.L28:
	addq	$64, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	pushq	$400
	pushq	$400
	movq	%rbp, %rdi
	movq	timeout(%rip), %rcx
	movq	8+timeout(%rip), %r8
	movl	$100000, %esi
	leaq	24(%rsp), %r9
	movl	$2, %edx
	call	__GI_clntudp_bufcreate
	testq	%rax, %rax
	movq	%rax, %rbp
	popq	%rsi
	popq	%rdi
	je	.L28
	movq	8(%rax), %rax
	movq	%r14, 32(%rsp)
	leaq	32(%rsp), %rcx
	movq	%r13, 40(%rsp)
	movq	%r12, 48(%rsp)
	leaq	__GI_xdr_pmap(%rip), %rdx
	movq	%rbx, 56(%rsp)
	pushq	8+tottimeout(%rip)
	leaq	__GI_xdr_bool(%rip), %r8
	pushq	tottimeout(%rip)
	movl	$1, %esi
	movq	%rbp, %rdi
	leaq	28(%rsp), %r9
	call	*(%rax)
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L37
.L29:
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	12(%rsp), %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	.LC1(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rbp, %rdi
	movq	%rax, %rsi
	call	__GI_clnt_perror
	movl	$0, 12(%rsp)
	jmp	.L29
	.size	__GI_pmap_set, .-__GI_pmap_set
	.globl	__EI_pmap_set
	.set	__EI_pmap_set,__GI_pmap_set
	.p2align 4,,15
	.globl	__GI_pmap_unset
	.hidden	__GI_pmap_unset
	.type	__GI_pmap_unset, @function
__GI_pmap_unset:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$64, %rsp
	leaq	16(%rsp), %rbx
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	call	__get_myaddress
	testl	%eax, %eax
	jne	.L39
.L41:
	addq	$64, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pushq	$400
	pushq	$400
	movq	%rbx, %rdi
	movq	timeout(%rip), %rcx
	movq	8+timeout(%rip), %r8
	movl	$100000, %esi
	leaq	24(%rsp), %r9
	movl	$2, %edx
	call	__GI_clntudp_bufcreate
	testq	%rax, %rax
	movq	%rax, %rbx
	popq	%rsi
	popq	%rdi
	je	.L41
	movq	8(%rax), %rax
	movq	%r12, 32(%rsp)
	leaq	32(%rsp), %rcx
	movq	%rbp, 40(%rsp)
	movq	$0, 48(%rsp)
	leaq	__GI_xdr_pmap(%rip), %rdx
	movq	$0, 56(%rsp)
	pushq	8+tottimeout(%rip)
	movq	%rbx, %rdi
	pushq	tottimeout(%rip)
	leaq	__GI_xdr_bool(%rip), %r8
	movl	$2, %esi
	leaq	28(%rsp), %r9
	call	*(%rax)
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	28(%rsp), %eax
	popq	%rdx
	popq	%rcx
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI_pmap_unset, .-__GI_pmap_unset
	.globl	__EI_pmap_unset
	.set	__EI_pmap_unset,__GI_pmap_unset
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	tottimeout, @object
	.size	tottimeout, 16
tottimeout:
	.quad	60
	.quad	0
	.align 16
	.type	timeout, @object
	.size	timeout, 16
timeout:
	.quad	5
	.quad	0
