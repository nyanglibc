	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	xdrstdio_inline, @function
xdrstdio_inline:
	xorl	%eax, %eax
	ret
	.size	xdrstdio_inline, .-xdrstdio_inline
	.p2align 4,,15
	.type	xdrstdio_putint32, @function
xdrstdio_putint32:
	subq	$24, %rsp
	movl	(%rsi), %eax
	movq	24(%rdi), %rcx
	leaq	12(%rsp), %rdi
	movl	$1, %edx
	movl	$4, %esi
	bswap	%eax
	movl	%eax, 12(%rsp)
	call	__GI__IO_fwrite
	cmpq	$1, %rax
	sete	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	ret
	.size	xdrstdio_putint32, .-xdrstdio_putint32
	.p2align 4,,15
	.type	xdrstdio_putlong, @function
xdrstdio_putlong:
	subq	$24, %rsp
	movq	(%rsi), %rax
	movq	24(%rdi), %rcx
	leaq	12(%rsp), %rdi
	movl	$1, %edx
	movl	$4, %esi
	bswap	%eax
	movl	%eax, 12(%rsp)
	call	__GI__IO_fwrite
	cmpq	$1, %rax
	sete	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	ret
	.size	xdrstdio_putlong, .-xdrstdio_putlong
	.p2align 4,,15
	.type	xdrstdio_getint32, @function
xdrstdio_getint32:
	pushq	%rbx
	movl	$1, %edx
	movq	%rsi, %rbx
	movl	$4, %esi
	subq	$16, %rsp
	movq	24(%rdi), %rcx
	leaq	12(%rsp), %rdi
	call	__GI__IO_fread
	xorl	%edx, %edx
	cmpq	$1, %rax
	jne	.L7
	movl	12(%rsp), %eax
	movl	$1, %edx
	bswap	%eax
	movl	%eax, (%rbx)
.L7:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	ret
	.size	xdrstdio_getint32, .-xdrstdio_getint32
	.p2align 4,,15
	.type	xdrstdio_getlong, @function
xdrstdio_getlong:
	pushq	%rbx
	movl	$1, %edx
	movq	%rsi, %rbx
	movl	$4, %esi
	subq	$16, %rsp
	movq	24(%rdi), %rcx
	leaq	12(%rsp), %rdi
	call	__GI__IO_fread
	xorl	%edx, %edx
	cmpq	$1, %rax
	jne	.L12
	movl	12(%rsp), %eax
	movl	$1, %edx
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rbx)
.L12:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	ret
	.size	xdrstdio_getlong, .-xdrstdio_getlong
	.p2align 4,,15
	.type	xdrstdio_destroy, @function
xdrstdio_destroy:
	movq	24(%rdi), %rdi
	jmp	__GI__IO_fflush
	.size	xdrstdio_destroy, .-xdrstdio_destroy
	.p2align 4,,15
	.type	xdrstdio_setpos, @function
xdrstdio_setpos:
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	movl	%esi, %esi
	xorl	%edx, %edx
	call	__GI_fseek
	notl	%eax
	addq	$8, %rsp
	shrl	$31, %eax
	ret
	.size	xdrstdio_setpos, .-xdrstdio_setpos
	.p2align 4,,15
	.type	xdrstdio_getpos, @function
xdrstdio_getpos:
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	call	__GI__IO_ftell
	addq	$8, %rsp
	ret
	.size	xdrstdio_getpos, .-xdrstdio_getpos
	.p2align 4,,15
	.type	xdrstdio_putbytes, @function
xdrstdio_putbytes:
	testl	%edx, %edx
	jne	.L31
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	subq	$8, %rsp
	movq	24(%rdi), %rcx
	movq	%rsi, %rax
	movq	%rax, %rdi
	movslq	%edx, %rsi
	movl	$1, %edx
	call	__GI__IO_fwrite
	cmpq	$1, %rax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	xdrstdio_putbytes, .-xdrstdio_putbytes
	.p2align 4,,15
	.type	xdrstdio_getbytes, @function
xdrstdio_getbytes:
	testl	%edx, %edx
	jne	.L41
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	subq	$8, %rsp
	movq	24(%rdi), %rcx
	movq	%rsi, %rax
	movq	%rax, %rdi
	movslq	%edx, %rsi
	movl	$1, %edx
	call	__GI__IO_fread
	cmpq	$1, %rax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	xdrstdio_getbytes, .-xdrstdio_getbytes
	.p2align 4,,15
	.globl	__GI_xdrstdio_create
	.hidden	__GI_xdrstdio_create
	.type	__GI_xdrstdio_create, @function
__GI_xdrstdio_create:
	leaq	xdrstdio_ops(%rip), %rax
	movl	%edx, (%rdi)
	movq	%rsi, 24(%rdi)
	movl	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	%rax, 8(%rdi)
	ret
	.size	__GI_xdrstdio_create, .-__GI_xdrstdio_create
	.globl	xdrstdio_create
	.set	xdrstdio_create,__GI_xdrstdio_create
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	xdrstdio_ops, @object
	.size	xdrstdio_ops, 80
xdrstdio_ops:
	.quad	xdrstdio_getlong
	.quad	xdrstdio_putlong
	.quad	xdrstdio_getbytes
	.quad	xdrstdio_putbytes
	.quad	xdrstdio_getpos
	.quad	xdrstdio_setpos
	.quad	xdrstdio_inline
	.quad	xdrstdio_destroy
	.quad	xdrstdio_getint32
	.quad	xdrstdio_putint32
