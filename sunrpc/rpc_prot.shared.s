	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_opaque_auth, xdr_opaque_auth@GLIBC_2.2.5
	.symver __EI_xdr_des_block, xdr_des_block@GLIBC_2.2.5
	.symver __EI_xdr_accepted_reply, xdr_accepted_reply@GLIBC_2.2.5
	.symver __EI_xdr_rejected_reply, xdr_rejected_reply@GLIBC_2.2.5
	.symver __EI_xdr_replymsg, xdr_replymsg@GLIBC_2.2.5
	.symver __EI_xdr_callhdr, xdr_callhdr@GLIBC_2.2.5
	.symver __EI__seterr_reply, _seterr_reply@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_rejected_reply
	.hidden	__GI_xdr_rejected_reply
	.type	__GI_xdr_rejected_reply, @function
__GI_xdr_rejected_reply:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L1
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L4
	cmpl	$1, %eax
	jne	.L1
	addq	$8, %rsp
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_enum
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L1
	addq	$8, %rsp
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_u_long
	.size	__GI_xdr_rejected_reply, .-__GI_xdr_rejected_reply
	.globl	__EI_xdr_rejected_reply
	.set	__EI_xdr_rejected_reply,__GI_xdr_rejected_reply
	.p2align 4,,15
	.globl	__GI_xdr_opaque_auth
	.hidden	__GI_xdr_opaque_auth
	.type	__GI_xdr_opaque_auth, @function
__GI_xdr_opaque_auth:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_enum
	testl	%eax, %eax
	jne	.L16
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$8, %rsp
	leaq	16(%rbx), %rdx
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	movl	$400, %ecx
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_bytes
	.size	__GI_xdr_opaque_auth, .-__GI_xdr_opaque_auth
	.globl	__EI_xdr_opaque_auth
	.set	__EI_xdr_opaque_auth,__GI_xdr_opaque_auth
	.p2align 4,,15
	.globl	__GI_xdr_accepted_reply
	.hidden	__GI_xdr_accepted_reply
	.type	__GI_xdr_accepted_reply, @function
__GI_xdr_accepted_reply:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_opaque_auth
	testl	%eax, %eax
	jne	.L18
.L20:
	xorl	%eax, %eax
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	24(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L20
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.L21
	cmpl	$2, %eax
	jne	.L32
	leaq	32(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L20
	addq	$8, %rsp
	leaq	40(%rbx), %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_u_long
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	addq	$8, %rsp
	movq	%rbp, %rdi
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	jmp	*%rdx
	.size	__GI_xdr_accepted_reply, .-__GI_xdr_accepted_reply
	.globl	__EI_xdr_accepted_reply
	.set	__EI_xdr_accepted_reply,__GI_xdr_accepted_reply
	.p2align 4,,15
	.globl	__GI_xdr_des_block
	.hidden	__GI_xdr_des_block
	.type	__GI_xdr_des_block, @function
__GI_xdr_des_block:
	movl	$8, %edx
	jmp	__GI_xdr_opaque
	.size	__GI_xdr_des_block, .-__GI_xdr_des_block
	.globl	__EI_xdr_des_block
	.set	__EI_xdr_des_block,__GI_xdr_des_block
	.p2align 4,,15
	.globl	__GI_xdr_replymsg
	.hidden	__GI_xdr_replymsg
	.type	__GI_xdr_replymsg, @function
__GI_xdr_replymsg:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_u_long
	testl	%eax, %eax
	jne	.L42
.L36:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L36
	cmpl	$1, 8(%rbx)
	jne	.L36
	addq	$8, %rsp
	leaq	24(%rbx), %rdx
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdi
	leaq	reply_dscrm(%rip), %rcx
	xorl	%r8d, %r8d
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_union
	.size	__GI_xdr_replymsg, .-__GI_xdr_replymsg
	.globl	__EI_xdr_replymsg
	.set	__EI_xdr_replymsg,__GI_xdr_replymsg
	.p2align 4,,15
	.globl	__GI_xdr_callhdr
	.hidden	__GI_xdr_callhdr
	.type	__GI_xdr_callhdr, @function
__GI_xdr_callhdr:
	movl	(%rdi), %eax
	movl	$0, 8(%rsi)
	movq	$2, 16(%rsi)
	testl	%eax, %eax
	je	.L63
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_u_long
	testl	%eax, %eax
	jne	.L64
.L45:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L45
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L45
	leaq	24(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L45
	addq	$8, %rsp
	leaq	32(%rbx), %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI_xdr_u_long
	.size	__GI_xdr_callhdr, .-__GI_xdr_callhdr
	.globl	__EI_xdr_callhdr
	.set	__EI_xdr_callhdr,__GI_xdr_callhdr
	.p2align 4,,15
	.globl	__GI__seterr_reply
	.hidden	__GI__seterr_reply
	.type	__GI__seterr_reply, @function
__GI__seterr_reply:
	movl	16(%rdi), %eax
	testl	%eax, %eax
	je	.L67
	cmpl	$1, %eax
	jne	.L84
	movl	24(%rdi), %eax
	testl	%eax, %eax
	je	.L80
	cmpl	$1, %eax
	jne	.L85
	movl	32(%rdi), %eax
	movl	$7, (%rsi)
	movl	%eax, 8(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$16, (%rsi)
	movq	%rax, 8(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movl	48(%rdi), %eax
	testl	%eax, %eax
	jne	.L69
	movl	$0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movl	$16, (%rsi)
	movq	$1, 8(%rsi)
	movq	%rax, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movq	32(%rdi), %rax
	movl	$6, (%rsi)
	movq	%rax, 8(%rsi)
	movq	40(%rdi), %rax
	movq	%rax, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$5, %eax
	ja	.L71
	leaq	.L73(%rip), %r8
	movl	%eax, %ecx
	movslq	(%r8,%rcx,4), %rdx
	addq	%r8, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L73:
	.long	.L71-.L73
	.long	.L72-.L73
	.long	.L74-.L73
	.long	.L75-.L73
	.long	.L76-.L73
	.long	.L77-.L73
	.text
	.p2align 4,,10
	.p2align 3
.L74:
	movq	56(%rdi), %rax
	movl	$9, (%rsi)
	movq	%rax, 8(%rsi)
	movq	64(%rdi), %rax
	movq	%rax, 16(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$11, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$10, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$8, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$12, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$16, (%rsi)
	movq	$0, 8(%rsi)
	movq	%rax, 16(%rsi)
	ret
	.size	__GI__seterr_reply, .-__GI__seterr_reply
	.globl	__EI__seterr_reply
	.set	__EI__seterr_reply,__GI__seterr_reply
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	reply_dscrm, @object
	.size	reply_dscrm, 48
reply_dscrm:
	.long	0
	.zero	4
	.quad	__GI_xdr_accepted_reply
	.long	1
	.zero	4
	.quad	__GI_xdr_rejected_reply
	.long	-1
	.zero	4
	.quad	0
