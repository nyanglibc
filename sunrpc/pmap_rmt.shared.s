	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_pmap_rmtcall, pmap_rmtcall@GLIBC_2.2.5
	.symver __EI_xdr_rmtcall_args, xdr_rmtcall_args@GLIBC_2.2.5
	.symver __EI_xdr_rmtcallres, xdr_rmtcallres@GLIBC_2.2.5
	.symver __EI_clnt_broadcast, clnt_broadcast@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_rmtcallres
	.hidden	__GI_xdr_rmtcallres
	.type	__GI_xdr_rmtcallres, @function
__GI_xdr_rmtcallres:
.LFB65:
	pushq	%rbp
	pushq	%rbx
	leaq	__GI_xdr_u_long(%rip), %rcx
	movq	%rsi, %rbx
	movl	$8, %edx
	movq	%rdi, %rbp
	subq	$24, %rsp
	movq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	call	__GI_xdr_reference
	testl	%eax, %eax
	jne	.L2
.L4:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L4
	movq	8(%rsp), %rax
	movq	16(%rbx), %rsi
	movq	%rbp, %rdi
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	call	*24(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE65:
	.size	__GI_xdr_rmtcallres, .-__GI_xdr_rmtcallres
	.globl	__EI_xdr_rmtcallres
	.set	__EI_xdr_rmtcallres,__GI_xdr_rmtcallres
	.p2align 4,,15
	.globl	__GI_xdr_rmtcall_args
	.hidden	__GI_xdr_rmtcall_args
	.type	__GI_xdr_rmtcall_args, @function
__GI_xdr_rmtcall_args:
.LFB64:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	__GI_xdr_u_long
	testl	%eax, %eax
	jne	.L11
.L16:
	xorl	%eax, %eax
.L10:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	8(%rbp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L16
	leaq	16(%rbp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L16
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	movq	$0, 8(%rsp)
	call	*32(%rax)
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	movl	%eax, %r13d
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L16
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movq	32(%rbp), %rsi
	movl	%eax, %r12d
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	*40(%rbp)
	testl	%eax, %eax
	je	.L16
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	movl	%eax, %eax
	movl	%r13d, %esi
	movq	%rbx, %rdi
	movq	%rax, %r14
	subq	%r12, %rax
	movq	%rax, 24(%rbp)
	movq	8(%rbx), %rax
	call	*40(%rax)
	leaq	24(%rbp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_u_long
	testl	%eax, %eax
	je	.L16
	movq	8(%rbx), %rax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	*40(%rax)
	movl	$1, %eax
	jmp	.L10
.LFE64:
	.size	__GI_xdr_rmtcall_args, .-__GI_xdr_rmtcall_args
	.globl	__EI_xdr_rmtcall_args
	.set	__EI_xdr_rmtcall_args,__GI_xdr_rmtcall_args
	.p2align 4,,15
	.globl	__GI_pmap_rmtcall
	.hidden	__GI_pmap_rmtcall
	.type	__GI_pmap_rmtcall, @function
__GI_pmap_rmtcall:
.LFB63:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	movl	$28416, %esi
	movq	%r9, %r12
	movq	%rdx, %r14
	subq	$120, %rsp
	movq	timeout(%rip), %rcx
	movq	8+timeout(%rip), %r8
	leaq	28(%rsp), %r9
	movw	%si, 2(%rdi)
	movl	$2, %edx
	movl	$100000, %esi
	movq	%rdi, %rbx
	movl	$-1, 28(%rsp)
	call	__GI_clntudp_create
	testq	%rax, %rax
	je	.L32
	movq	%rax, %r10
	movq	208(%rsp), %rax
	movq	%rbp, 104(%rsp)
	movq	%r15, 64(%rsp)
	movq	%r14, 72(%rsp)
	leaq	64(%rsp), %rcx
	movq	%r13, 80(%rsp)
	movq	%r12, 96(%rsp)
	movq	%r10, %rdi
	movq	%rax, 32(%rsp)
	movq	184(%rsp), %rax
	leaq	__GI_xdr_rmtcall_args(%rip), %rdx
	leaq	__GI_xdr_rmtcallres(%rip), %r8
	movl	$5, %esi
	movq	%rax, 48(%rsp)
	movq	176(%rsp), %rax
	movq	%rax, 56(%rsp)
	movq	8(%r10), %rax
	pushq	200(%rsp)
	pushq	200(%rsp)
	movq	%r10, 24(%rsp)
	leaq	48(%rsp), %r9
	call	*(%rax)
	popq	%rdx
	popq	%rcx
	movq	8(%rsp), %r10
	movl	%eax, %ebp
	movq	8(%r10), %rax
	movq	%r10, %rdi
	call	*32(%rax)
.L31:
	xorl	%eax, %eax
	movw	%ax, 2(%rbx)
	addq	$120, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$16, %ebp
	jmp	.L31
.LFE63:
	.size	__GI_pmap_rmtcall, .-__GI_pmap_rmtcall
	.globl	__EI_pmap_rmtcall
	.set	__EI_pmap_rmtcall,__GI_pmap_rmtcall
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Cannot create socket for broadcast rpc"
	.align 8
.LC1:
	.string	"Cannot set socket option SO_BROADCAST"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"broadcast: getifaddrs"
.LC3:
	.string	"Cannot send broadcast packet"
.LC4:
	.string	"Broadcast poll problem"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"Cannot receive reply to broadcast"
	.text
	.p2align 4,,15
	.globl	__GI_clnt_broadcast
	.hidden	__GI_clnt_broadcast
	.type	__GI_clnt_broadcast, @function
__GI_clnt_broadcast:
.LFB67:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	subq	$10712, %rsp
	movq	%rdi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r9, (%rsp)
	call	__GI_authunix_create_default
	movl	$17, %edx
	movl	$2, %esi
	movl	$2, %edi
	movq	%rax, %rbx
	movl	$1, 140(%rsp)
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %r13d
	movl	$5, %edx
	leaq	.LC0(%rip), %rsi
	js	.L100
	leaq	140(%rsp), %rcx
	movl	$4, %r8d
	movl	$6, %edx
	movl	$1, %esi
	movl	%eax, %edi
	call	__setsockopt
	testl	%eax, %eax
	js	.L102
	leaq	1904(%rsp), %rax
	movl	$1, %esi
	movl	%r13d, 152(%rsp)
	movw	%si, 156(%rsp)
	movq	%rax, %rdi
	movq	%rax, 32(%rsp)
	call	__GI_getifaddrs
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L103
	movq	1904(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L40
	movq	%rdi, %rax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L40
.L104:
	cmpl	$19, %r15d
	jg	.L40
.L42:
	movl	16(%rax), %edx
	andl	$3, %edx
	cmpl	$3, %edx
	jne	.L41
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L41
	cmpw	$2, (%rdx)
	jne	.L41
	movq	40(%rax), %rcx
	movq	(%rax), %rax
	movslq	%r15d, %rdx
	addl	$1, %r15d
	movl	4(%rcx), %ecx
	testq	%rax, %rax
	movl	%ecx, 320(%rsp,%rdx,4)
	jne	.L104
	.p2align 4,,10
	.p2align 3
.L40:
	call	__GI_freeifaddrs
.L39:
	leaq	160(%rsp), %rax
	movq	$0, 164(%rsp)
	movl	$0, 172(%rsp)
	movl	$1862270978, 160(%rsp)
	movq	%rax, 24(%rsp)
	call	_create_xid@PLT
	movq	%rax, 48(%rsp)
	movq	%rax, 400(%rsp)
	xorl	%ecx, %ecx
	movq	16(%rbx), %rax
	movl	$1400, %edx
	movl	$0, 408(%rsp)
	movdqu	(%rbx), %xmm0
	movq	%r14, 280(%rsp)
	leaq	496(%rsp), %r14
	movq	$2, 416(%rsp)
	movq	%rax, 464(%rsp)
	movq	40(%rbx), %rax
	movaps	%xmm0, 448(%rsp)
	movq	%r14, %rsi
	movq	$100000, 424(%rsp)
	movq	$2, 432(%rsp)
	movdqu	24(%rbx), %xmm0
	movq	%rax, 488(%rsp)
	movq	8(%rsp), %rax
	movq	%rbp, 304(%rsp)
	leaq	224(%rsp), %rbp
	movq	$5, 440(%rsp)
	movups	%xmm0, 472(%rsp)
	movq	%rax, 272(%rsp)
	movq	16(%rsp), %rax
	movq	%rbp, %rdi
	movq	%rax, 288(%rsp)
	leaq	144(%rsp), %rax
	movq	%r12, 312(%rsp)
	movq	%rbp, 8(%rsp)
	movq	%rax, 192(%rsp)
	movq	(%rsp), %rax
	movq	%rax, 216(%rsp)
	movq	10768(%rsp), %rax
	movq	%rax, 208(%rsp)
	call	__GI_xdrmem_create
	leaq	400(%rsp), %rax
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	%rax, 40(%rsp)
	call	__GI_xdr_callmsg
	testl	%eax, %eax
	jne	.L105
.L44:
	movl	$1, %ebp
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	.LC1(%rip), %rsi
	movl	$5, %edx
.L100:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$3, %ebp
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
.L36:
	movl	%r13d, %edi
	call	__GI___close
	movq	56(%rbx), %rax
	movq	%rbx, %rdi
	call	*32(%rax)
	addq	$10712, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	movq	8(%rsp), %rbp
	leaq	272(%rsp), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_rmtcall_args
	testl	%eax, %eax
	je	.L44
	movq	232(%rsp), %rax
	movq	%rbp, %rdi
	call	*32(%rax)
	movl	%eax, 64(%rsp)
	movq	232(%rsp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L45
	movq	%rbp, %rdi
	call	*%rax
.L45:
	leaq	320(%rsp), %rax
	leaq	192(%rsp), %rsi
	movq	24(%rsp), %r12
	movl	%r15d, 68(%rsp)
	movl	$4000, 16(%rsp)
	movq	%rax, %rcx
	movq	%rax, 56(%rsp)
	leal	-1(%r15), %eax
	movq	%rsi, 72(%rsp)
	movq	%rbx, 80(%rsp)
	leaq	4(%rcx,%rax,4), %rax
	movq	%rax, %r15
.L53:
	movl	68(%rsp), %eax
	movslq	64(%rsp), %rbp
	movq	56(%rsp), %rbx
	testl	%eax, %eax
	jne	.L47
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$4, %rbx
	cmpq	%rbx, %r15
	je	.L60
.L47:
	movl	(%rbx), %eax
	xorl	%ecx, %ecx
	movl	$16, %r9d
	movq	%r12, %r8
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movl	%r13d, %edi
	movl	%eax, 164(%rsp)
	call	__sendto
	cmpq	%rax, %rbp
	je	.L46
	movq	80(%rsp), %rbx
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	.LC2(%rip), %rdi
	xorl	%r15d, %r15d
	call	__GI_perror
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L60:
	cmpq	$0, 10776(%rsp)
	je	.L99
	leaq	152(%rsp), %rax
	leaq	__GI__null_auth(%rip), %rbp
	movq	%rax, 24(%rsp)
	leaq	176(%rsp), %rax
	leaq	16(%rbp), %rbx
	movq	%rax, 88(%rsp)
	leaq	136(%rsp), %rax
	movq	%rax, 96(%rsp)
.L49:
	movq	(%rbx), %rax
	movl	16(%rsp), %edx
	movl	$1, %esi
	movq	24(%rsp), %rdi
	movdqu	0(%rbp), %xmm0
	movq	%rax, 440(%rsp)
	movq	72(%rsp), %rax
	movups	%xmm0, 424(%rsp)
	movq	%rax, 456(%rsp)
	leaq	__GI_xdr_rmtcallres(%rip), %rax
	movq	%rax, 464(%rsp)
	call	__GI___poll
	cmpl	$-1, %eax
	je	.L51
	testl	%eax, %eax
	jne	.L96
	addl	$2000, 16(%rsp)
	movl	16(%rsp), %eax
	cmpl	$16000, %eax
	jne	.L53
	movq	80(%rsp), %rbx
	movl	$5, %ebp
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L51:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	je	.L49
	movq	80(%rsp), %rbx
	leaq	.LC4(%rip), %rsi
	movl	$5, %edx
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$4, %ebp
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%r15, 104(%rsp)
	movq	%r14, 112(%rsp)
	movq	%r12, 120(%rsp)
	movq	88(%rsp), %r14
	movq	32(%rsp), %r12
	movq	96(%rsp), %r15
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L107:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L106
.L50:
	xorl	%ecx, %ecx
	movq	%r15, %r9
	movq	%r14, %r8
	movl	$8800, %edx
	movq	%r12, %rsi
	movl	%r13d, %edi
	movl	$16, 136(%rsp)
	call	__recvfrom
	testl	%eax, %eax
	js	.L107
	cmpl	$7, %eax
	movq	104(%rsp), %r15
	movq	112(%rsp), %r14
	movq	120(%rsp), %r12
	jbe	.L49
	movq	32(%rsp), %rsi
	movq	8(%rsp), %rdi
	movl	$1, %ecx
	movl	%eax, %edx
	call	__GI_xdrmem_create
	movq	40(%rsp), %rsi
	movq	8(%rsp), %rdi
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	je	.L56
	movl	48(%rsp), %eax
	cmpl	%eax, 400(%rsp)
	je	.L108
.L56:
	movq	40(%rsp), %rsi
	movq	8(%rsp), %rdi
	leaq	__GI_xdr_void(%rip), %rax
	movl	$2, 224(%rsp)
	movq	%rax, 464(%rsp)
	call	__GI_xdr_replymsg
	xorl	%eax, %eax
	movq	10768(%rsp), %rsi
	movq	8(%rsp), %rdi
	movq	(%rsp), %rcx
	call	*%rcx
	movq	232(%rsp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L49
	movq	8(%rsp), %rdi
	call	*%rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L106:
	movq	80(%rsp), %rbx
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L101
.L108:
	movl	416(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L56
	movl	448(%rsp), %edx
	testl	%edx, %edx
	jne	.L56
	movzwl	144(%rsp), %eax
	movq	88(%rsp), %rsi
	movq	10768(%rsp), %rdi
	rolw	$8, %ax
	movw	%ax, 178(%rsp)
	call	*10776(%rsp)
	movq	40(%rsp), %rsi
	movq	8(%rsp), %rdi
	movl	%eax, 104(%rsp)
	leaq	__GI_xdr_void(%rip), %rax
	movl	$2, 224(%rsp)
	movq	%rax, 464(%rsp)
	call	__GI_xdr_replymsg
	xorl	%eax, %eax
	movq	10768(%rsp), %rsi
	movq	8(%rsp), %rdi
	movq	(%rsp), %rcx
	call	*%rcx
	movq	232(%rsp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L57
	movq	8(%rsp), %rdi
	call	*%rax
.L57:
	cmpl	$0, 104(%rsp)
	je	.L49
.L99:
	movq	80(%rsp), %rbx
	xorl	%ebp, %ebp
	jmp	.L36
.LFE67:
	.size	__GI_clnt_broadcast, .-__GI_clnt_broadcast
	.globl	__EI_clnt_broadcast
	.set	__EI_clnt_broadcast,__GI_clnt_broadcast
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	timeout, @object
	.size	timeout, 16
timeout:
	.quad	3
	.quad	0
	.hidden	__recvfrom
	.hidden	__sendto
	.hidden	__setsockopt
