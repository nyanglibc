	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI__null_auth, _null_auth@GLIBC_2.2.5
	.symver svc_fdset,svc_fdset@GLIBC_2.2.5
	.symver rpc_createerr,rpc_createerr@GLIBC_2.2.5
	.symver svc_pollfd,svc_pollfd@GLIBC_2.2.5
	.symver svc_max_pollfd,svc_max_pollfd@GLIBC_2.2.5
#NO_APP
	.globl	svc_max_pollfd
	.bss
	.align 4
	.type	svc_max_pollfd, @object
	.size	svc_max_pollfd, 4
svc_max_pollfd:
	.zero	4
	.globl	svc_pollfd
	.align 8
	.type	svc_pollfd, @object
	.size	svc_pollfd, 8
svc_pollfd:
	.zero	8
	.globl	rpc_createerr
	.align 32
	.type	rpc_createerr, @object
	.size	rpc_createerr, 32
rpc_createerr:
	.zero	32
	.globl	svc_fdset
	.align 32
	.type	svc_fdset, @object
	.size	svc_fdset, 128
svc_fdset:
	.zero	128
	.hidden	__GI__null_auth
	.globl	__GI__null_auth
	.align 16
	.type	__GI__null_auth, @object
	.size	__GI__null_auth, 24
__GI__null_auth:
	.zero	24
	.globl	__EI__null_auth
	.set	__EI__null_auth,__GI__null_auth
