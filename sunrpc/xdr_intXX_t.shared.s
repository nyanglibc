	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_int64_t, xdr_int64_t@GLIBC_2.2.5
	.symver __EI_xdr_quad_t, xdr_quad_t@GLIBC_2.3.4
	.symver __EI_xdr_uint64_t, xdr_uint64_t@GLIBC_2.2.5
	.symver __EI_xdr_u_quad_t, xdr_u_quad_t@GLIBC_2.3.4
	.symver __EI_xdr_int32_t, xdr_int32_t@GLIBC_2.2.5
	.symver __EI_xdr_uint32_t, xdr_uint32_t@GLIBC_2.2.5
	.symver __EI_xdr_int16_t, xdr_int16_t@GLIBC_2.2.5
	.symver __EI_xdr_uint16_t, xdr_uint16_t@GLIBC_2.2.5
	.symver __EI_xdr_int8_t, xdr_int8_t@GLIBC_2.2.5
	.symver __EI_xdr_uint8_t, xdr_uint8_t@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_int64_t
	.hidden	__GI_xdr_int64_t
	.type	__GI_xdr_int64_t, @function
__GI_xdr_int64_t:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L3
	jb	.L4
	cmpl	$2, %eax
	sete	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*64(%rax)
	testl	%eax, %eax
	je	.L10
	movq	8(%rbx), %rax
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	call	*64(%rax)
	testl	%eax, %eax
	jne	.L16
.L10:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, %rdx
	movl	%eax, 12(%rsp)
	movq	8(%rdi), %rax
	sarq	$32, %rdx
	movl	%edx, 8(%rsp)
	call	*72(%rax)
	testl	%eax, %eax
	je	.L10
	movq	8(%rbx), %rax
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movslq	8(%rsp), %rax
	movl	12(%rsp), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movq	%rax, 0(%rbp)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_int64_t, .-__GI_xdr_int64_t
	.globl	__EI_xdr_int64_t
	.set	__EI_xdr_int64_t,__GI_xdr_int64_t
	.p2align 4,,15
	.globl	__GI_xdr_quad_t
	.hidden	__GI_xdr_quad_t
	.type	__GI_xdr_quad_t, @function
__GI_xdr_quad_t:
	jmp	__GI_xdr_int64_t
	.size	__GI_xdr_quad_t, .-__GI_xdr_quad_t
	.globl	__EI_xdr_quad_t
	.set	__EI_xdr_quad_t,__GI_xdr_quad_t
	.p2align 4,,15
	.globl	__GI_xdr_uint64_t
	.hidden	__GI_xdr_uint64_t
	.type	__GI_xdr_uint64_t, @function
__GI_xdr_uint64_t:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L20
	jb	.L21
	cmpl	$2, %eax
	sete	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*64(%rax)
	testl	%eax, %eax
	je	.L27
	movq	8(%rbx), %rax
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	call	*64(%rax)
	testl	%eax, %eax
	jne	.L32
.L27:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, %rdx
	movl	%eax, 12(%rsp)
	movq	8(%rdi), %rax
	shrq	$32, %rdx
	movl	%edx, 8(%rsp)
	call	*72(%rax)
	testl	%eax, %eax
	je	.L27
	movq	8(%rbx), %rax
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	call	*72(%rax)
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	8(%rsp), %eax
	movl	12(%rsp), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movq	%rax, 0(%rbp)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_xdr_uint64_t, .-__GI_xdr_uint64_t
	.globl	__EI_xdr_uint64_t
	.set	__EI_xdr_uint64_t,__GI_xdr_uint64_t
	.p2align 4,,15
	.globl	__GI_xdr_u_quad_t
	.hidden	__GI_xdr_u_quad_t
	.type	__GI_xdr_u_quad_t, @function
__GI_xdr_u_quad_t:
	jmp	__GI_xdr_uint64_t
	.size	__GI_xdr_u_quad_t, .-__GI_xdr_u_quad_t
	.globl	__EI_xdr_u_quad_t
	.set	__EI_xdr_u_quad_t,__GI_xdr_u_quad_t
	.p2align 4,,15
	.globl	__GI_xdr_int32_t
	.hidden	__GI_xdr_int32_t
	.type	__GI_xdr_int32_t, @function
__GI_xdr_int32_t:
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L36
	jb	.L42
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	8(%rdi), %rax
	jmp	*64(%rax)
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%rdi), %rax
	jmp	*72(%rax)
	.size	__GI_xdr_int32_t, .-__GI_xdr_int32_t
	.globl	__EI_xdr_int32_t
	.set	__EI_xdr_int32_t,__GI_xdr_int32_t
	.p2align 4,,15
	.globl	__GI_xdr_uint32_t
	.hidden	__GI_xdr_uint32_t
	.type	__GI_xdr_uint32_t, @function
__GI_xdr_uint32_t:
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L45
	jb	.L51
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	8(%rdi), %rax
	jmp	*64(%rax)
	.p2align 4,,10
	.p2align 3
.L51:
	movq	8(%rdi), %rax
	jmp	*72(%rax)
	.size	__GI_xdr_uint32_t, .-__GI_xdr_uint32_t
	.globl	__EI_xdr_uint32_t
	.set	__EI_xdr_uint32_t,__GI_xdr_uint32_t
	.p2align 4,,15
	.globl	__GI_xdr_int16_t
	.hidden	__GI_xdr_int16_t
	.type	__GI_xdr_int16_t, @function
__GI_xdr_int16_t:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L54
	jb	.L55
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L52:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rdi), %rax
	leaq	12(%rsp), %rsi
	call	*64(%rax)
	testl	%eax, %eax
	je	.L52
	movl	12(%rsp), %eax
	movw	%ax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movswl	(%rsi), %eax
	leaq	12(%rsp), %rsi
	movl	%eax, 12(%rsp)
	movq	8(%rdi), %rax
	call	*72(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_int16_t, .-__GI_xdr_int16_t
	.globl	__EI_xdr_int16_t
	.set	__EI_xdr_int16_t,__GI_xdr_int16_t
	.p2align 4,,15
	.globl	__GI_xdr_uint16_t
	.hidden	__GI_xdr_uint16_t
	.type	__GI_xdr_uint16_t, @function
__GI_xdr_uint16_t:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L65
	jb	.L66
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L63:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	8(%rdi), %rax
	leaq	12(%rsp), %rsi
	call	*64(%rax)
	testl	%eax, %eax
	je	.L63
	movl	12(%rsp), %eax
	movw	%ax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movzwl	(%rsi), %eax
	leaq	12(%rsp), %rsi
	movl	%eax, 12(%rsp)
	movq	8(%rdi), %rax
	call	*72(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_uint16_t, .-__GI_xdr_uint16_t
	.globl	__EI_xdr_uint16_t
	.set	__EI_xdr_uint16_t,__GI_xdr_uint16_t
	.p2align 4,,15
	.globl	__GI_xdr_int8_t
	.hidden	__GI_xdr_int8_t
	.type	__GI_xdr_int8_t, @function
__GI_xdr_int8_t:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L76
	jb	.L77
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L74:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movq	8(%rdi), %rax
	leaq	12(%rsp), %rsi
	call	*64(%rax)
	testl	%eax, %eax
	je	.L74
	movl	12(%rsp), %eax
	movb	%al, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movsbl	(%rsi), %eax
	leaq	12(%rsp), %rsi
	movl	%eax, 12(%rsp)
	movq	8(%rdi), %rax
	call	*72(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_int8_t, .-__GI_xdr_int8_t
	.globl	__EI_xdr_int8_t
	.set	__EI_xdr_int8_t,__GI_xdr_int8_t
	.p2align 4,,15
	.globl	__GI_xdr_uint8_t
	.hidden	__GI_xdr_uint8_t
	.type	__GI_xdr_uint8_t, @function
__GI_xdr_uint8_t:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L87
	jb	.L88
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L85:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%rdi), %rax
	leaq	12(%rsp), %rsi
	call	*64(%rax)
	testl	%eax, %eax
	je	.L85
	movl	12(%rsp), %eax
	movb	%al, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movzbl	(%rsi), %eax
	leaq	12(%rsp), %rsi
	movl	%eax, 12(%rsp)
	movq	8(%rdi), %rax
	call	*72(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_uint8_t, .-__GI_xdr_uint8_t
	.globl	__EI_xdr_uint8_t
	.set	__EI_xdr_uint8_t,__GI_xdr_uint8_t
