	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI___rpc_thread_svc_fdset, __rpc_thread_svc_fdset@GLIBC_2.2.5
	.symver __EI___rpc_thread_createerr, __rpc_thread_createerr@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	rpc_thread_multi, @function
rpc_thread_multi:
	movq	thread_rpc_vars@gottpoff(%rip), %rax
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rdx
	movq	%rdx, %fs:(%rax)
	ret
	.size	rpc_thread_multi, .-rpc_thread_multi
	.p2align 4,,15
	.globl	__rpc_thread_destroy
	.hidden	__rpc_thread_destroy
	.type	__rpc_thread_destroy, @function
__rpc_thread_destroy:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	thread_rpc_vars@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L3
	call	__rpc_thread_svc_cleanup
	call	__rpc_thread_clnt_cleanup
	call	__rpc_thread_key_cleanup
	movq	176(%rbx), %rdi
	call	free@PLT
	movq	184(%rbx), %rdi
	call	free@PLT
	movq	240(%rbx), %rdi
	call	free@PLT
	movq	208(%rbx), %rdi
	call	free@PLT
	movq	216(%rbx), %rdi
	call	free@PLT
	movq	224(%rbx), %rdi
	call	free@PLT
	movq	160(%rbx), %rdi
	call	free@PLT
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rax
	cmpq	%rax, %rbx
	je	.L5
	movq	%rbx, %rdi
	call	free@PLT
.L5:
	movq	$0, %fs:0(%rbp)
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__rpc_thread_destroy, .-__rpc_thread_destroy
	.p2align 4,,15
	.globl	__rpc_thread_variables
	.hidden	__rpc_thread_variables
	.type	__rpc_thread_variables, @function
__rpc_thread_variables:
	pushq	%rbx
	movq	thread_rpc_vars@gottpoff(%rip), %rbx
	movq	%fs:(%rbx), %rax
	testq	%rax, %rax
	je	.L21
.L10:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L12
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	rpc_thread_multi(%rip), %rsi
	leaq	once.9545(%rip), %rdi
#APP
# 59 "rpc_thread.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	movq	%fs:(%rbx), %rax
	testq	%rax, %rax
	jne	.L10
.L13:
	movl	$264, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	je	.L10
	movq	%rax, %fs:(%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	once.9545(%rip), %eax
	testl	%eax, %eax
	jne	.L13
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rax
	movl	$2, once.9545(%rip)
	movq	%rax, %fs:(%rbx)
	popq	%rbx
	ret
	.size	__rpc_thread_variables, .-__rpc_thread_variables
	.p2align 4,,15
	.globl	__GI___rpc_thread_svc_fdset
	.hidden	__GI___rpc_thread_svc_fdset
	.type	__GI___rpc_thread_svc_fdset, @function
__GI___rpc_thread_svc_fdset:
	subq	$8, %rsp
	call	__rpc_thread_variables
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L26
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	svc_fdset@GOTPCREL(%rip), %rax
	addq	$8, %rsp
	ret
	.size	__GI___rpc_thread_svc_fdset, .-__GI___rpc_thread_svc_fdset
	.globl	__EI___rpc_thread_svc_fdset
	.set	__EI___rpc_thread_svc_fdset,__GI___rpc_thread_svc_fdset
	.p2align 4,,15
	.globl	__GI___rpc_thread_createerr
	.hidden	__GI___rpc_thread_createerr
	.type	__GI___rpc_thread_createerr, @function
__GI___rpc_thread_createerr:
	subq	$8, %rsp
	call	__rpc_thread_variables
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L29
	subq	$-128, %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	rpc_createerr@GOTPCREL(%rip), %rax
	addq	$8, %rsp
	ret
	.size	__GI___rpc_thread_createerr, .-__GI___rpc_thread_createerr
	.globl	__EI___rpc_thread_createerr
	.set	__EI___rpc_thread_createerr,__GI___rpc_thread_createerr
	.p2align 4,,15
	.globl	__GI___rpc_thread_svc_pollfd
	.hidden	__GI___rpc_thread_svc_pollfd
	.type	__GI___rpc_thread_svc_pollfd, @function
__GI___rpc_thread_svc_pollfd:
	subq	$8, %rsp
	call	__rpc_thread_variables
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L33
	addq	$160, %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	svc_pollfd@GOTPCREL(%rip), %rax
	addq	$8, %rsp
	ret
	.size	__GI___rpc_thread_svc_pollfd, .-__GI___rpc_thread_svc_pollfd
	.globl	__rpc_thread_svc_pollfd
	.set	__rpc_thread_svc_pollfd,__GI___rpc_thread_svc_pollfd
	.p2align 4,,15
	.globl	__GI___rpc_thread_svc_max_pollfd
	.hidden	__GI___rpc_thread_svc_max_pollfd
	.type	__GI___rpc_thread_svc_max_pollfd, @function
__GI___rpc_thread_svc_max_pollfd:
	subq	$8, %rsp
	call	__rpc_thread_variables
	leaq	__libc_tsd_RPC_VARS_mem(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L37
	addq	$168, %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	svc_max_pollfd@GOTPCREL(%rip), %rax
	addq	$8, %rsp
	ret
	.size	__GI___rpc_thread_svc_max_pollfd, .-__GI___rpc_thread_svc_max_pollfd
	.globl	__rpc_thread_svc_max_pollfd
	.set	__rpc_thread_svc_max_pollfd,__GI___rpc_thread_svc_max_pollfd
	.local	once.9545
	.comm	once.9545,4,4
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element___rpc_thread_destroy__, @object
	.size	__elf_set___libc_subfreeres_element___rpc_thread_destroy__, 8
__elf_set___libc_subfreeres_element___rpc_thread_destroy__:
	.quad	__rpc_thread_destroy
	.section	.tbss,"awT",@nobits
	.align 8
	.type	thread_rpc_vars, @object
	.size	thread_rpc_vars, 8
thread_rpc_vars:
	.zero	8
	.local	__libc_tsd_RPC_VARS_mem
	.comm	__libc_tsd_RPC_VARS_mem,264,32
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__rpc_thread_key_cleanup
	.hidden	__rpc_thread_clnt_cleanup
	.hidden	__rpc_thread_svc_cleanup
