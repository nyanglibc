	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_clntunix_create, clntunix_create@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	clntunix_geterr, @function
clntunix_geterr:
	movq	16(%rdi), %rax
	movdqu	144(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movq	160(%rax), %rax
	movq	%rax, 16(%rsi)
	ret
	.size	clntunix_geterr, .-clntunix_geterr
	.p2align 4,,15
	.type	clntunix_freeres, @function
clntunix_freeres:
	movq	16(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	$2, 200(%rdi)
	addq	$200, %rdi
	jmp	*%rcx
	.size	clntunix_freeres, .-clntunix_freeres
	.p2align 4,,15
	.type	clntunix_abort, @function
clntunix_abort:
	rep ret
	.size	clntunix_abort, .-clntunix_abort
	.p2align 4,,15
	.type	clntunix_destroy, @function
clntunix_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	16(%rdi), %rbx
	movl	4(%rbx), %eax
	testl	%eax, %eax
	jne	.L12
.L6:
	movq	208(%rbx), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L7
	leaq	200(%rbx), %rdi
	call	*%rax
.L7:
	movq	%rbx, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	movl	(%rbx), %edi
	call	__GI___close
	jmp	.L6
	.size	clntunix_destroy, .-clntunix_destroy
	.p2align 4,,15
	.type	clntunix_control, @function
clntunix_control:
	cmpl	$15, %esi
	movq	16(%rdi), %rdi
	ja	.L28
	leaq	.L16(%rip), %rcx
	movl	%esi, %esi
	movslq	(%rcx,%rsi,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L16:
	.long	.L28-.L16
	.long	.L15-.L16
	.long	.L17-.L16
	.long	.L18-.L16
	.long	.L28-.L16
	.long	.L28-.L16
	.long	.L19-.L16
	.long	.L28-.L16
	.long	.L20-.L16
	.long	.L21-.L16
	.long	.L22-.L16
	.long	.L23-.L16
	.long	.L24-.L16
	.long	.L25-.L16
	.long	.L26-.L16
	.long	.L27-.L16
	.text
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rdx), %rax
	bswap	%eax
	movl	%eax, 180(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movdqu	(%rdx), %xmm0
	movl	$1, %eax
	movups	%xmm0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movdqu	8(%rdi), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqu	28(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	movdqu	44(%rdi), %xmm0
	movups	%xmm0, 16(%rdx)
	movdqu	60(%rdi), %xmm0
	movups	%xmm0, 32(%rdx)
	movdqu	76(%rdi), %xmm0
	movups	%xmm0, 48(%rdx)
	movdqu	92(%rdi), %xmm0
	movups	%xmm0, 64(%rdx)
	movdqu	108(%rdi), %xmm0
	movups	%xmm0, 80(%rdx)
	movq	124(%rdi), %rax
	movq	%rax, 96(%rdx)
	movl	132(%rdi), %eax
	movl	%eax, 104(%rdx)
	movzwl	136(%rdi), %eax
	movw	%ax, 108(%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	(%rdi), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$1, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$0, 4(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movl	168(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rdx), %rax
	subl	$1, %eax
	bswap	%eax
	movl	%eax, 168(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	184(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rdx), %rax
	bswap	%eax
	movl	%eax, 184(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	180(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	movq	%rax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	ret
	.size	clntunix_control, .-clntunix_control
	.p2align 4,,15
	.type	clntunix_call, @function
clntunix_call:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$184, %rsp
	movq	16(%rdi), %r12
	movq	%rcx, 48(%rsp)
	movq	%rsi, 72(%rsp)
	movq	%rdx, 40(%rsp)
	movq	%r8, 32(%rsp)
	movl	24(%r12), %ecx
	leaq	168(%r12), %rax
	movq	%r9, 64(%rsp)
	leaq	200(%r12), %rbx
	movq	%rax, 16(%rsp)
	testl	%ecx, %ecx
	jne	.L30
	movdqu	240(%rsp), %xmm0
	movups	%xmm0, 8(%r12)
.L30:
	cmpq	$0, 32(%rsp)
	movl	$1, 56(%rsp)
	je	.L74
.L31:
	leaq	72(%rsp), %rax
	movl	$3, 60(%rsp)
	movq	%rax, 24(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 8(%rsp)
.L32:
	movl	168(%r12), %eax
	movl	$0, 200(%r12)
	movq	%rbx, %rdi
	movl	$0, 144(%r12)
	movl	192(%r12), %edx
	movq	16(%rsp), %rsi
	leal	-1(%rax), %ebp
	movq	208(%r12), %rax
	movl	%ebp, 168(%r12)
	bswap	%ebp
	call	*24(%rax)
	testl	%eax, %eax
	movl	%ebp, %ebp
	je	.L36
	movq	208(%r12), %rax
	movq	24(%rsp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	je	.L36
	movq	(%r15), %rdi
	movq	%rbx, %rsi
	movq	56(%rdi), %rax
	call	*8(%rax)
	testl	%eax, %eax
	je	.L36
	xorl	%eax, %eax
	movq	48(%rsp), %rsi
	movq	%rbx, %rdi
	movq	40(%rsp), %rcx
	call	*%rcx
	testl	%eax, %eax
	je	.L36
	movl	56(%rsp), %esi
	movq	%rbx, %rdi
	call	__GI_xdrrec_endofrecord
	testl	%eax, %eax
	je	.L75
	movl	56(%rsp), %eax
	testl	%eax, %eax
	je	.L51
	cmpq	$0, 8(%r12)
	je	.L76
.L40:
	leaq	__GI__null_auth(%rip), %r14
	movl	$1, 200(%r12)
	leaq	16(%r14), %r13
.L62:
	movq	0(%r13), %rax
	movq	%rbx, %rdi
	movq	$0, 136(%rsp)
	movdqu	(%r14), %xmm0
	movq	%rax, 120(%rsp)
	leaq	__GI_xdr_void(%rip), %rax
	movups	%xmm0, 104(%rsp)
	movq	%rax, 144(%rsp)
	call	__GI_xdrrec_skiprecord
	testl	%eax, %eax
	je	.L47
	movq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	jne	.L42
	movl	144(%r12), %eax
	testl	%eax, %eax
	je	.L62
.L29:
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	cmpq	%rbp, 80(%rsp)
	jne	.L62
	movq	8(%rsp), %rdi
	leaq	144(%r12), %rsi
	call	__GI__seterr_reply
	movl	144(%r12), %eax
	testl	%eax, %eax
	je	.L77
	subl	$1, 60(%rsp)
	je	.L29
	movq	(%r15), %rdi
	movq	56(%rdi), %rax
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L32
	.p2align 4,,10
	.p2align 3
.L47:
	movl	144(%r12), %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L76:
	cmpq	$0, 16(%r12)
	jne	.L40
	movl	$5, 144(%r12)
	movl	$5, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	$0, 8(%r12)
	jne	.L31
	xorl	%eax, %eax
	cmpq	$0, 16(%r12)
	setne	%al
	movl	%eax, 56(%rsp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	movl	144(%r12), %edx
	testl	%edx, %edx
	jne	.L35
	movl	$1, 144(%r12)
.L35:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__GI_xdrrec_endofrecord
	movl	144(%r12), %eax
	jmp	.L29
.L75:
	movl	$3, 144(%r12)
	movl	$3, %eax
	jmp	.L29
.L51:
	xorl	%eax, %eax
	jmp	.L29
.L77:
	movq	8(%rsp), %rax
	movq	(%r15), %rdi
	leaq	24(%rax), %rsi
	movq	56(%rdi), %rax
	call	*16(%rax)
	testl	%eax, %eax
	je	.L78
	xorl	%eax, %eax
	movq	64(%rsp), %rsi
	movq	%rbx, %rdi
	movq	32(%rsp), %rcx
	call	*%rcx
	testl	%eax, %eax
	jne	.L46
	cmpl	$0, 144(%r12)
	jne	.L46
	movl	$2, 144(%r12)
.L46:
	cmpq	$0, 112(%rsp)
	je	.L47
	movq	8(%rsp), %rsi
	movl	$2, 200(%r12)
	movq	%rbx, %rdi
	addq	$24, %rsi
	call	__GI_xdr_opaque_auth
	movl	144(%r12), %eax
	jmp	.L29
.L78:
	movl	$7, 144(%r12)
	movl	$6, 152(%r12)
	jmp	.L46
	.size	clntunix_call, .-clntunix_call
	.p2align 4,,15
	.type	__msgwrite, @function
__msgwrite:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbx
	movq	%rsi, %r14
	movl	%edi, %r12d
	subq	$152, %rsp
	leaq	15(%rsp), %rbx
	call	__GI___getpid
	andq	$-16, %rbx
	movl	%eax, -132(%rbp)
	call	__geteuid
	movl	%eax, %r15d
	call	__getegid
	movl	-132(%rbp), %edx
	movl	%eax, 24(%rbx)
	movabsq	$8589934593, %rax
	movq	%rax, 8(%rbx)
	leaq	-128(%rbp), %rax
	movl	%r15d, 20(%rbx)
	movq	$28, (%rbx)
	movq	%rbx, -80(%rbp)
	movl	%edx, 16(%rbx)
	movq	%r14, -128(%rbp)
	leaq	-112(%rbp), %rbx
	movq	%r13, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	$1, -88(%rbp)
	movq	$0, -112(%rbp)
	movl	$0, -104(%rbp)
	movq	$32, -72(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L85:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L84
.L80:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	__sendmsg
	testl	%eax, %eax
	movl	%eax, %edx
	js	.L85
.L79:
	leaq	-40(%rbp), %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$-1, %edx
	jmp	.L79
	.size	__msgwrite, .-__msgwrite
	.p2align 4,,15
	.type	writeunix, @function
writeunix:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testl	%edx, %edx
	jle	.L87
	movq	%rdi, %r12
	movq	%rsi, %rbp
	movl	%edx, %ebx
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L88:
	subl	%eax, %ebx
	cltq
	addq	%rax, %rbp
	testl	%ebx, %ebx
	jle	.L87
.L90:
	movl	(%r12), %edi
	movslq	%ebx, %rdx
	movq	%rbp, %rsi
	call	__msgwrite
	cmpl	$-1, %eax
	jne	.L88
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$3, 144(%r12)
	movl	%fs:(%rdx), %edx
	movl	%edx, 152(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	writeunix, .-writeunix
	.p2align 4,,15
	.type	readunix, @function
readunix:
	xorl	%ecx, %ecx
	testl	%edx, %edx
	je	.L117
	pushq	%r14
	pushq	%r13
	movabsq	$2361183241434822607, %rcx
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movslq	%edx, %rbp
	movq	%rdi, %rbx
	subq	$96, %rsp
	movl	(%rdi), %eax
	movq	16(%rdi), %rsi
	imull	$1000, 8(%rdi), %r13d
	leaq	8(%rsp), %r14
	movl	%eax, 8(%rsp)
	movl	$1, %eax
	movw	%ax, 12(%rsp)
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rcx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addl	%edx, %r13d
.L98:
	movl	%r13d, %edx
	movl	$1, %esi
	movq	%r14, %rdi
	call	__GI___poll
	cmpl	$-1, %eax
	movl	%eax, %ecx
	je	.L96
	testl	%eax, %eax
	je	.L120
	movl	(%rbx), %r13d
	leaq	16(%rsp), %rax
	leaq	4(%rsp), %rcx
	movl	$4, %r8d
	movl	$16, %edx
	movl	$1, %esi
	movq	%rax, 48(%rsp)
	leaq	cm.14493(%rip), %rax
	movq	%r12, 16(%rsp)
	movl	%r13d, %edi
	movq	%rbp, 24(%rsp)
	movq	$1, 56(%rsp)
	movq	$0, 32(%rsp)
	movl	$0, 40(%rsp)
	movq	%rax, 64(%rsp)
	movq	$32, 72(%rsp)
	movl	$0, 80(%rsp)
	movl	$1, 4(%rsp)
	call	__setsockopt
	testl	%eax, %eax
	jne	.L121
	leaq	32(%rsp), %rbp
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	jne	.L100
.L101:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	%r13d, %edi
	call	__recvmsg
	testl	%eax, %eax
	movl	%eax, %ecx
	js	.L102
	testb	$8, 80(%rsp)
	jne	.L103
	testl	%eax, %eax
	jne	.L93
.L103:
	movl	$104, 152(%rbx)
	movl	$4, 144(%rbx)
	movl	$-1, %ecx
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$5, 144(%rbx)
	movl	$-1, %ecx
.L93:
	addq	$96, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	je	.L98
	movl	$4, 144(%rbx)
	movl	%eax, 152(%rbx)
	jmp	.L93
.L121:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	.p2align 4,,10
	.p2align 3
.L100:
	movl	%eax, 152(%rbx)
	movl	$4, 144(%rbx)
	movl	$-1, %ecx
	jmp	.L93
	.size	readunix, .-readunix
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI_clntunix_create
	.hidden	__GI_clntunix_create
	.type	__GI_clntunix_create, @function
__GI_clntunix_create:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	$248, %edi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdx, %r12
	movq	%rcx, %rbp
	subq	$120, %rsp
	movl	%r8d, 4(%rsp)
	movl	%r9d, 8(%rsp)
	call	malloc@PLT
	movl	$24, %edi
	movq	%rax, %r14
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L136
	testq	%r14, %r14
	je	.L136
	movl	0(%rbp), %eax
	testl	%eax, %eax
	js	.L143
	movl	$0, 4(%r14)
.L132:
	movdqu	(%r15), %xmm0
	movl	%eax, (%r14)
	movq	96(%r15), %rax
	movups	%xmm0, 28(%r14)
	movq	$0, 16(%r14)
	movl	$0, 24(%r14)
	movdqu	16(%r15), %xmm0
	movq	%rax, 124(%r14)
	movl	104(%r15), %eax
	movups	%xmm0, 44(%r14)
	movdqu	32(%r15), %xmm0
	movl	%eax, 132(%r14)
	movzwl	108(%r15), %eax
	movups	%xmm0, 60(%r14)
	movdqu	48(%r15), %xmm0
	movw	%ax, 136(%r14)
	movups	%xmm0, 76(%r14)
	movdqu	64(%r15), %xmm0
	movups	%xmm0, 92(%r14)
	movdqu	80(%r15), %xmm0
	movups	%xmm0, 108(%r14)
	call	_create_xid@PLT
	leaq	168(%r14), %rsi
	xorl	%ecx, %ecx
	movl	$24, %edx
	movq	%r12, 48(%rsp)
	leaq	200(%r14), %r12
	movq	%rax, 16(%rsp)
	movl	$0, 24(%rsp)
	movq	$2, 32(%rsp)
	movq	%r12, %rdi
	movq	%r13, 40(%rsp)
	call	__GI_xdrmem_create
	leaq	16(%rsp), %rsi
	movq	%r12, %rdi
	call	__GI_xdr_callhdr
	testl	%eax, %eax
	je	.L144
	movq	208(%r14), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	movl	%eax, 192(%r14)
	movq	208(%r14), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L134
	movq	%r12, %rdi
	call	*%rax
.L134:
	movl	8(%rsp), %edx
	movl	4(%rsp), %esi
	leaq	writeunix(%rip), %r9
	leaq	readunix(%rip), %r8
	movq	%r14, %rcx
	movq	%r12, %rdi
	call	__GI_xdrrec_create
	leaq	unix_ops(%rip), %rax
	movq	%r14, 16(%rbx)
	movq	%rax, 8(%rbx)
	call	__GI_authnone_create
	movq	%rax, (%rbx)
.L122:
	addq	$120, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%edx, %edx
	movl	$1, %esi
	movl	$1, %edi
	call	__GI___socket
	leaq	2(%r15), %rdi
	movl	%eax, 0(%rbp)
	movl	%eax, 12(%rsp)
	call	__GI_strlen
	movl	12(%rsp), %ecx
	leal	3(%rax), %edx
	testl	%ecx, %ecx
	js	.L130
	movq	%r15, %rsi
	movl	%ecx, %edi
	call	__GI___connect
	testl	%eax, %eax
	js	.L130
	movl	$1, 4(%r14)
	movl	0(%rbp), %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L130:
	call	__GI___rpc_thread_createerr
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$12, (%rax)
	movl	%fs:(%rdx), %edx
	movl	%edx, 16(%rax)
	movl	0(%rbp), %edi
	cmpl	$-1, %edi
	je	.L125
.L142:
	call	__GI___close
.L125:
	movq	%r14, %rdi
	call	free@PLT
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L136:
	call	__GI___rpc_thread_createerr
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rbp
	call	__GI___dcgettext
	leaq	__func__.14415(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movl	$12, 0(%rbp)
	movl	$12, 16(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L144:
	movl	4(%r14), %eax
	testl	%eax, %eax
	je	.L125
	movl	0(%rbp), %edi
	jmp	.L142
	.size	__GI_clntunix_create, .-__GI_clntunix_create
	.globl	__EI_clntunix_create
	.set	__EI_clntunix_create,__GI_clntunix_create
	.local	cm.14493
	.comm	cm.14493,32,32
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__func__.14415, @object
	.size	__func__.14415, 16
__func__.14415:
	.string	"clntunix_create"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	unix_ops, @object
	.size	unix_ops, 48
unix_ops:
	.quad	clntunix_call
	.quad	clntunix_abort
	.quad	clntunix_geterr
	.quad	clntunix_freeres
	.quad	clntunix_destroy
	.quad	clntunix_control
	.hidden	__fxprintf
	.hidden	__recvmsg
	.hidden	__setsockopt
	.hidden	__sendmsg
	.hidden	__getegid
	.hidden	__geteuid
