	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_authunix_parms, xdr_authunix_parms@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI_xdr_authunix_parms
	.hidden	__GI_xdr_authunix_parms
	.type	__GI_xdr_authunix_parms, @function
__GI_xdr_authunix_parms:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__GI_xdr_u_long
	testl	%eax, %eax
	jne	.L2
.L4:
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	8(%rbx), %rsi
	movl	$255, %edx
	movq	%rbp, %rdi
	call	__GI_xdr_string
	testl	%eax, %eax
	je	.L4
	leaq	16(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L4
	leaq	20(%rbx), %rsi
	movq	%rbp, %rdi
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L4
	leaq	24(%rbx), %rdx
	leaq	32(%rbx), %rsi
	leaq	__GI_xdr_u_int(%rip), %r9
	movl	$4, %r8d
	movl	$16, %ecx
	movq	%rbp, %rdi
	call	__GI_xdr_array
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1
	.size	__GI_xdr_authunix_parms, .-__GI_xdr_authunix_parms
	.globl	__EI_xdr_authunix_parms
	.set	__EI_xdr_authunix_parms,__GI_xdr_authunix_parms
