	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_xdr_union, xdr_union@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	__GI_xdr_opaque.part.10, @function
__GI_xdr_opaque.part.10:
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %ebx
	andl	$3, %ebx
	movl	$4, %eax
	movq	%rdi, %rbp
	subl	%ebx, %eax
	subq	$8, %rsp
	testl	%ebx, %ebx
	cmovne	%eax, %ebx
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L4
	jb	.L5
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%rdi), %rax
	call	*16(%rax)
	testl	%eax, %eax
	je	.L9
	testl	%ebx, %ebx
	je	.L10
	movq	8(%rbp), %rax
	movl	%ebx, %edx
	movq	%rbp, %rdi
	leaq	crud.7359(%rip), %rsi
	movq	16(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rdi), %rax
	call	*24(%rax)
	testl	%eax, %eax
	jne	.L23
.L9:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	testl	%ebx, %ebx
	je	.L10
	movq	8(%rbp), %rax
	movl	%ebx, %edx
	movq	%rbp, %rdi
	leaq	xdr_zero(%rip), %rsi
	movq	24(%rax), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$1, %eax
	jmp	.L1
	.size	__GI_xdr_opaque.part.10, .-__GI_xdr_opaque.part.10
	.p2align 4,,15
	.globl	__GI_xdr_free
	.hidden	__GI_xdr_free
	.type	__GI_xdr_free, @function
__GI_xdr_free:
	subq	$56, %rsp
	movq	%rdi, %rdx
	xorl	%eax, %eax
	movl	$2, (%rsp)
	movq	%rsp, %rdi
	call	*%rdx
	addq	$56, %rsp
	ret
	.size	__GI_xdr_free, .-__GI_xdr_free
	.globl	xdr_free
	.set	xdr_free,__GI_xdr_free
	.p2align 4,,15
	.globl	__GI_xdr_void
	.hidden	__GI_xdr_void
	.type	__GI_xdr_void, @function
__GI_xdr_void:
	movl	$1, %eax
	ret
	.size	__GI_xdr_void, .-__GI_xdr_void
	.globl	xdr_void
	.set	xdr_void,__GI_xdr_void
	.p2align 4,,15
	.globl	__GI_xdr_int
	.hidden	__GI_xdr_int
	.type	__GI_xdr_int, @function
__GI_xdr_int:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L29
	jb	.L30
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L27:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L27
	movq	8(%rsp), %rax
	movl	%eax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movslq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_int, .-__GI_xdr_int
	.globl	xdr_int
	.set	xdr_int,__GI_xdr_int
	.p2align 4,,15
	.globl	__GI_xdr_u_int
	.hidden	__GI_xdr_u_int
	.type	__GI_xdr_u_int, @function
__GI_xdr_u_int:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L40
	jb	.L41
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L38:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L38
	movq	8(%rsp), %rax
	movl	%eax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movl	(%rsi), %eax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_u_int, .-__GI_xdr_u_int
	.globl	xdr_u_int
	.set	xdr_u_int,__GI_xdr_u_int
	.p2align 4,,15
	.globl	__GI_xdr_long
	.hidden	__GI_xdr_long
	.type	__GI_xdr_long, @function
__GI_xdr_long:
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L50
	movq	(%rsi), %rdx
	movslq	%edx, %rcx
	cmpq	%rcx, %rdx
	jne	.L51
	movq	8(%rdi), %rax
	jmp	*8(%rax)
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	$1, %eax
	jne	.L51
	movq	8(%rdi), %rax
	jmp	*(%rax)
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	ret
	.size	__GI_xdr_long, .-__GI_xdr_long
	.globl	xdr_long
	.set	xdr_long,__GI_xdr_long
	.p2align 4,,15
	.globl	__GI_xdr_u_long
	.hidden	__GI_xdr_u_long
	.type	__GI_xdr_u_long, @function
__GI_xdr_u_long:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L54
	jb	.L55
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L52:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L52
	movl	8(%rsp), %eax
	movq	%rax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rsi), %rdx
	xorl	%eax, %eax
	movl	%edx, %ecx
	cmpq	%rcx, %rdx
	jne	.L52
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_u_long, .-__GI_xdr_u_long
	.globl	xdr_u_long
	.set	xdr_u_long,__GI_xdr_u_long
	.p2align 4,,15
	.globl	__GI_xdr_hyper
	.hidden	__GI_xdr_hyper
	.type	__GI_xdr_hyper, @function
__GI_xdr_hyper:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L65
	movq	(%rsi), %rax
	movq	%rsp, %rsi
	movq	%rax, %rdx
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	sarq	$32, %rdx
	movq	%rdx, (%rsp)
	call	*8(%rax)
	testl	%eax, %eax
	je	.L71
	movq	8(%rbx), %rax
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L64:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	cmpl	$1, %eax
	jne	.L67
	movq	8(%rdi), %rax
	movq	%rsp, %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L71
	movq	8(%rbx), %rax
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*(%rax)
	testl	%eax, %eax
	jne	.L76
.L71:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rsp), %rax
	movl	8(%rsp), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movq	%rax, 0(%rbp)
	movl	$1, %eax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L64
	.size	__GI_xdr_hyper, .-__GI_xdr_hyper
	.globl	xdr_hyper
	.set	xdr_hyper,__GI_xdr_hyper
	.p2align 4,,15
	.globl	__GI_xdr_u_hyper
	.hidden	__GI_xdr_u_hyper
	.type	__GI_xdr_u_hyper, @function
__GI_xdr_u_hyper:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
	testl	%eax, %eax
	jne	.L78
	movq	(%rsi), %rax
	movq	%rsp, %rsi
	movq	%rax, %rdx
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	shrq	$32, %rdx
	movq	%rdx, (%rsp)
	call	*8(%rax)
	testl	%eax, %eax
	je	.L84
	movq	8(%rbx), %rax
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*8(%rax)
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L77:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$1, %eax
	jne	.L80
	movq	8(%rdi), %rax
	movq	%rsp, %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L84
	movq	8(%rbx), %rax
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*(%rax)
	testl	%eax, %eax
	jne	.L89
.L84:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%rsp), %rax
	movl	8(%rsp), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movq	%rax, 0(%rbp)
	movl	$1, %eax
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L77
	.size	__GI_xdr_u_hyper, .-__GI_xdr_u_hyper
	.globl	xdr_u_hyper
	.set	xdr_u_hyper,__GI_xdr_u_hyper
	.p2align 4,,15
	.globl	__GI_xdr_longlong_t
	.hidden	__GI_xdr_longlong_t
	.type	__GI_xdr_longlong_t, @function
__GI_xdr_longlong_t:
	jmp	__GI_xdr_hyper
	.size	__GI_xdr_longlong_t, .-__GI_xdr_longlong_t
	.globl	xdr_longlong_t
	.set	xdr_longlong_t,__GI_xdr_longlong_t
	.p2align 4,,15
	.globl	__GI_xdr_u_longlong_t
	.hidden	__GI_xdr_u_longlong_t
	.type	__GI_xdr_u_longlong_t, @function
__GI_xdr_u_longlong_t:
	jmp	__GI_xdr_u_hyper
	.size	__GI_xdr_u_longlong_t, .-__GI_xdr_u_longlong_t
	.globl	xdr_u_longlong_t
	.set	xdr_u_longlong_t,__GI_xdr_u_longlong_t
	.p2align 4,,15
	.globl	__GI_xdr_short
	.hidden	__GI_xdr_short
	.type	__GI_xdr_short, @function
__GI_xdr_short:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L94
	jb	.L95
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L92:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L92
	movq	8(%rsp), %rax
	movw	%ax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movswq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_short, .-__GI_xdr_short
	.globl	xdr_short
	.set	xdr_short,__GI_xdr_short
	.p2align 4,,15
	.globl	__GI_xdr_u_short
	.hidden	__GI_xdr_u_short
	.type	__GI_xdr_u_short, @function
__GI_xdr_u_short:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L105
	jb	.L106
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L103:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L103
	movq	8(%rsp), %rax
	movw	%ax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	movzwl	(%rsi), %eax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_u_short, .-__GI_xdr_u_short
	.globl	xdr_u_short
	.set	xdr_u_short,__GI_xdr_u_short
	.p2align 4,,15
	.globl	__GI_xdr_char
	.hidden	__GI_xdr_char
	.type	__GI_xdr_char, @function
__GI_xdr_char:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movsbl	(%rsi), %eax
	leaq	12(%rsp), %rsi
	movl	%eax, 12(%rsp)
	call	__GI_xdr_int
	testl	%eax, %eax
	je	.L114
	movl	12(%rsp), %eax
	movb	%al, (%rbx)
	movl	$1, %eax
.L114:
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_char, .-__GI_xdr_char
	.globl	xdr_char
	.set	xdr_char,__GI_xdr_char
	.p2align 4,,15
	.globl	__GI_xdr_u_char
	.hidden	__GI_xdr_u_char
	.type	__GI_xdr_u_char, @function
__GI_xdr_u_char:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movzbl	(%rsi), %eax
	leaq	12(%rsp), %rsi
	movl	%eax, 12(%rsp)
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L120
	movl	12(%rsp), %eax
	movb	%al, (%rbx)
	movl	$1, %eax
.L120:
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_u_char, .-__GI_xdr_u_char
	.globl	xdr_u_char
	.set	xdr_u_char,__GI_xdr_u_char
	.p2align 4,,15
	.globl	__GI_xdr_bool
	.hidden	__GI_xdr_bool
	.type	__GI_xdr_bool, @function
__GI_xdr_bool:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L128
	jb	.L129
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L126:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L126
	xorl	%eax, %eax
	cmpq	$0, 8(%rsp)
	setne	%al
	movl	%eax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	movl	(%rsi), %edx
	xorl	%eax, %eax
	leaq	8(%rsp), %rsi
	testl	%edx, %edx
	setne	%al
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_bool, .-__GI_xdr_bool
	.globl	xdr_bool
	.set	xdr_bool,__GI_xdr_bool
	.p2align 4,,15
	.globl	__GI_xdr_enum
	.hidden	__GI_xdr_enum
	.type	__GI_xdr_enum, @function
__GI_xdr_enum:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L139
	jb	.L140
	cmpl	$2, %eax
	sete	%al
	movzbl	%al, %eax
.L137:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	movq	8(%rdi), %rax
	leaq	8(%rsp), %rsi
	call	*(%rax)
	testl	%eax, %eax
	je	.L137
	movq	8(%rsp), %rax
	movl	%eax, (%rbx)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movslq	(%rsi), %rax
	leaq	8(%rsp), %rsi
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rax
	call	*8(%rax)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_xdr_enum, .-__GI_xdr_enum
	.globl	xdr_enum
	.set	xdr_enum,__GI_xdr_enum
	.p2align 4,,15
	.globl	__GI_xdr_opaque
	.hidden	__GI_xdr_opaque
	.type	__GI_xdr_opaque, @function
__GI_xdr_opaque:
	testl	%edx, %edx
	je	.L149
	jmp	__GI_xdr_opaque.part.10
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$1, %eax
	ret
	.size	__GI_xdr_opaque, .-__GI_xdr_opaque
	.globl	xdr_opaque
	.set	xdr_opaque,__GI_xdr_opaque
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"out of memory\n"
.LC1:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI_xdr_bytes
	.hidden	__GI_xdr_bytes
	.type	__GI_xdr_bytes, @function
__GI_xdr_bytes:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r13d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rsi), %r14
	movq	%rdx, %rsi
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L150
	movl	0(%rbp), %edx
	movl	(%rbx), %ecx
	cmpl	%r13d, %edx
	jbe	.L152
	xorl	%eax, %eax
	cmpl	$2, %ecx
	je	.L153
.L150:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	cmpl	$1, %ecx
	je	.L154
	jb	.L155
	cmpl	$2, %ecx
	je	.L153
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L153:
	testq	%r14, %r14
	movl	$1, %eax
	je	.L150
	movq	%r14, %rdi
	movl	%eax, 12(%rsp)
	call	free@PLT
	movq	$0, (%r12)
	movl	12(%rsp), %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L154:
	testl	%edx, %edx
	je	.L159
	testq	%r14, %r14
	je	.L172
.L156:
	addq	$16, %rsp
	movq	%r14, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	__GI_xdr_opaque.part.10
	.p2align 4,,10
	.p2align 3
.L155:
	testl	%edx, %edx
	movl	$1, %eax
	je	.L150
	jmp	.L156
.L172:
	movl	%edx, %edi
	movl	%edx, 12(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, (%r12)
	movl	12(%rsp), %edx
	jne	.L156
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.7374(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	xorl	%eax, %eax
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L159:
	movl	$1, %eax
	jmp	.L150
	.size	__GI_xdr_bytes, .-__GI_xdr_bytes
	.globl	xdr_bytes
	.set	xdr_bytes,__GI_xdr_bytes
	.p2align 4,,15
	.globl	__GI_xdr_netobj
	.hidden	__GI_xdr_netobj
	.type	__GI_xdr_netobj, @function
__GI_xdr_netobj:
	movq	%rsi, %rdx
	leaq	8(%rsi), %rsi
	movl	$1024, %ecx
	jmp	__GI_xdr_bytes
	.size	__GI_xdr_netobj, .-__GI_xdr_netobj
	.globl	xdr_netobj
	.set	xdr_netobj,__GI_xdr_netobj
	.p2align 4,,15
	.globl	__GI_xdr_union
	.hidden	__GI_xdr_union
	.type	__GI_xdr_union, @function
__GI_xdr_union:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r13
	movq	%rcx, %rbx
	call	__GI_xdr_enum
	testl	%eax, %eax
	je	.L174
	movq	8(%rbx), %rcx
	movl	0(%rbp), %eax
	testq	%rcx, %rcx
	jne	.L179
	.p2align 4,,10
	.p2align 3
.L176:
	testq	%r14, %r14
	je	.L174
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%r14, %rcx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	movl	$-1, %edx
	xorl	%eax, %eax
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L178:
	addq	$16, %rbx
	movq	8(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L176
.L179:
	cmpl	%eax, (%rbx)
	jne	.L178
	popq	%rbx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movl	$-1, %edx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	xorl	%eax, %eax
	jmp	*%rcx
	.p2align 4,,10
	.p2align 3
.L174:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI_xdr_union, .-__GI_xdr_union
	.globl	__EI_xdr_union
	.set	__EI_xdr_union,__GI_xdr_union
	.p2align 4,,15
	.globl	__GI_xdr_string
	.hidden	__GI_xdr_string
	.type	__GI_xdr_string, @function
__GI_xdr_string:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	(%rdi), %eax
	movq	(%rsi), %rbp
	movl	$0, 28(%rsp)
	testl	%eax, %eax
	je	.L194
	cmpl	$2, %eax
	jne	.L193
	testq	%rbp, %rbp
	je	.L216
.L196:
	movq	%rbp, %rdi
	call	__GI_strlen
	movl	%eax, 28(%rsp)
.L193:
	leaq	28(%rsp), %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_u_int
	testl	%eax, %eax
	je	.L214
	movl	28(%rsp), %edx
	cmpl	%r13d, %edx
	ja	.L214
	movl	%edx, %ecx
	addl	$1, %ecx
	je	.L214
	movl	(%rbx), %eax
	cmpl	$1, %eax
	je	.L199
	jb	.L200
	cmpl	$2, %eax
	je	.L201
	.p2align 4,,10
	.p2align 3
.L214:
	xorl	%eax, %eax
.L192:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	testq	%rbp, %rbp
	jne	.L196
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%rbp, %rdi
	call	free@PLT
	movq	$0, (%r12)
.L216:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L199:
	testq	%rbp, %rbp
	je	.L217
.L202:
	movb	$0, 0(%rbp,%rdx)
	movl	28(%rsp), %edx
.L200:
	testl	%edx, %edx
	je	.L216
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_xdr_opaque.part.10
	jmp	.L192
.L217:
	movl	%ecx, %edi
	movl	%edx, 12(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, (%r12)
	movl	12(%rsp), %edx
	jne	.L202
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	__func__.7410(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	xorl	%eax, %eax
	jmp	.L192
	.size	__GI_xdr_string, .-__GI_xdr_string
	.globl	xdr_string
	.set	xdr_string,__GI_xdr_string
	.p2align 4,,15
	.globl	__GI_xdr_wrapstring
	.hidden	__GI_xdr_wrapstring
	.type	__GI_xdr_wrapstring, @function
__GI_xdr_wrapstring:
	subq	$8, %rsp
	movl	$-1, %edx
	call	__GI_xdr_string
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	__GI_xdr_wrapstring, .-__GI_xdr_wrapstring
	.globl	xdr_wrapstring
	.set	xdr_wrapstring,__GI_xdr_wrapstring
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__func__.7410, @object
	.size	__func__.7410, 11
__func__.7410:
	.string	"xdr_string"
	.align 8
	.type	__func__.7374, @object
	.size	__func__.7374, 10
__func__.7374:
	.string	"xdr_bytes"
	.local	crud.7359
	.comm	crud.7359,4,1
	.section	.rodata
	.type	xdr_zero, @object
	.size	xdr_zero, 4
xdr_zero:
	.zero	4
	.hidden	__fxprintf
