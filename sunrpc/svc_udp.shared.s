	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __EI_svcudp_bufcreate, svcudp_bufcreate@GLIBC_2.2.5
	.symver __EI_svcudp_create, svcudp_create@GLIBC_2.2.5
	.symver __EI_svcudp_enablecache, svcudp_enablecache@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	svcudp_stat, @function
svcudp_stat:
	movl	$2, %eax
	ret
	.size	svcudp_stat, .-svcudp_stat
	.p2align 4,,15
	.type	svcudp_getargs, @function
svcudp_getargs:
	movq	72(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	addq	$16, %rdi
	jmp	*%rcx
	.size	svcudp_getargs, .-svcudp_getargs
	.p2align 4,,15
	.type	svcudp_freeargs, @function
svcudp_freeargs:
	movq	72(%rdi), %rdi
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movq	%rdx, %rsi
	movl	$2, 16(%rdi)
	addq	$16, %rdi
	jmp	*%rcx
	.size	svcudp_freeargs, .-svcudp_freeargs
	.p2align 4,,15
	.type	svcudp_destroy, @function
svcudp_destroy:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	72(%rdi), %rbp
	call	__GI_xprt_unregister
	movl	(%rbx), %edi
	call	__GI___close
	movq	24(%rbp), %rax
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L6
	leaq	16(%rbp), %rdi
	call	*%rax
.L6:
	movq	64(%rbx), %rdi
	call	free@PLT
	movq	%rbp, %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.size	svcudp_destroy, .-svcudp_destroy
	.p2align 4,,15
	.type	svcudp_recv, @function
svcudp_recv:
	pushq	%r15
	pushq	%r14
	leaq	80(%rdi), %r14
	pushq	%r13
	pushq	%r12
	leaq	96(%rdi), %r12
	pushq	%rbp
	pushq	%rbx
	leaq	20(%rdi), %rbp
	movq	%rdi, %r13
	movq	%rsi, %r15
	subq	$40, %rsp
	movq	72(%rdi), %rbx
	leaq	16(%rbx), %rax
	movq	%rax, 8(%rsp)
	leaq	28(%rsp), %rax
	movq	%rax, (%rsp)
	.p2align 4,,10
	.p2align 3
.L12:
	cmpq	$0, 120(%r13)
	movl	$16, 28(%rsp)
	movl	(%rbx), %edx
	movq	64(%r13), %rsi
	movl	0(%r13), %edi
	je	.L13
	movl	%edx, %eax
	movq	%rsi, 80(%r13)
	xorl	%edx, %edx
	movq	%rax, 88(%r13)
	leaq	152(%r13), %rax
	movq	%r14, 112(%r13)
	movq	$1, 120(%r13)
	movq	%rbp, 96(%r13)
	movq	%r12, %rsi
	movl	$16, 104(%r13)
	movq	%rax, 128(%r13)
	movq	$184, 136(%r13)
	call	__recvmsg
	testl	%eax, %eax
	movl	%eax, %edx
	js	.L14
	movq	136(%r13), %rcx
	movl	104(%r13), %eax
	cmpq	$15, %rcx
	movl	%eax, 28(%rsp)
	jbe	.L15
	movq	128(%r13), %rsi
	testq	%rsi, %rsi
	je	.L15
	movq	(%rsi), %r8
	cmpq	$15, %r8
	ja	.L45
	.p2align 4,,10
	.p2align 3
.L15:
	movq	$0, 128(%r13)
	movq	$0, 136(%r13)
.L19:
	movl	%eax, 16(%r13)
.L20:
	cmpl	$15, %edx
	jle	.L23
	movq	8(%rsp), %r14
	movq	24(%rbx), %rax
	xorl	%esi, %esi
	movl	$1, 16(%rbx)
	movq	%r14, %rdi
	call	*40(%rax)
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__GI_xdr_callmsg
	testl	%eax, %eax
	je	.L23
	cmpq	$0, 464(%rbx)
	movq	(%r15), %rax
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	je	.L11
	movq	72(%r13), %rax
	xorl	%edx, %edx
	movq	464(%rax), %rsi
	movq	8(%rax), %rcx
	movq	(%rsi), %rax
	leaq	0(,%rax,4), %rdi
	movq	%rcx, %rax
	divq	%rdi
	movq	8(%rsi), %rax
	movl	%edx, %edx
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L24
	leaq	56(%rsi), %rdx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L25:
	movq	64(%rax), %rax
	testq	%rax, %rax
	je	.L24
.L30:
	cmpq	(%rax), %rcx
	jne	.L25
	movq	48(%rsi), %rbx
	cmpq	%rbx, 8(%rax)
	jne	.L25
	movq	40(%rsi), %rbx
	cmpq	%rbx, 16(%rax)
	jne	.L25
	movq	32(%rsi), %rbx
	cmpq	%rbx, 24(%rax)
	jne	.L25
	movq	32(%rax), %rdi
	movq	40(%rax), %r8
	xorq	(%rdx), %rdi
	xorq	8(%rdx), %r8
	orq	%rdi, %r8
	jne	.L25
	cmpq	$0, 120(%r13)
	movq	48(%rax), %rsi
	movl	0(%r13), %edi
	movq	56(%rax), %rax
	jne	.L46
	movl	28(%rsp), %r9d
	movslq	%eax, %rdx
	movq	%rbp, %r8
	xorl	%ecx, %ecx
	call	__sendto
	movl	$1, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rsp), %r9
	movslq	%edx, %rdx
	movq	%rbp, %r8
	xorl	%ecx, %ecx
	call	__recvfrom
	movl	%eax, %edx
.L14:
	movl	28(%rsp), %eax
	cmpl	$-1, %edx
	movl	%eax, 16(%r13)
	jne	.L20
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	je	.L12
	call	__svc_accept_failed
	xorl	%eax, %eax
.L11:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	7(%r8), %rdi
	addq	%rsi, %rcx
	andq	$-8, %rdi
	addq	%rsi, %rdi
	leaq	16(%rdi), %r9
	cmpq	%r9, %rcx
	jnb	.L47
.L16:
	movabsq	$34359738368, %rcx
	cmpq	%rcx, 8(%rsi)
	jne	.L15
	cmpq	$27, %r8
	jbe	.L15
	movl	$0, 16(%rsi)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L24:
	movq	40(%r15), %rax
	movq	%rax, 48(%rsi)
	movq	32(%r15), %rax
	movq	%rax, 40(%rsi)
	movq	24(%r15), %rax
	movq	%rax, 32(%rsi)
	movl	$1, %eax
	movdqu	20(%r13), %xmm0
	movups	%xmm0, 56(%rsi)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rdi), %r10
	leaq	7(%r10), %r9
	andq	$-8, %r9
	addq	%r9, %rdi
	cmpq	%rdi, %rcx
	jnb	.L15
	jmp	.L16
.L46:
	movq	%rsi, 80(%r13)
	movq	%rax, 88(%r13)
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	__sendmsg
	movl	$1, %eax
	jmp	.L11
	.size	svcudp_recv, .-svcudp_recv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cache_set: victim alloc failed"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%s\n"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"cache_set: could not allocate new rpc_buffer"
	.section	.rodata.str1.1
.LC3:
	.string	"cache_set: victim not found"
	.text
	.p2align 4,,15
	.type	svcudp_reply, @function
svcudp_reply:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	72(%rdi), %rbp
	movq	24(%rbp), %rax
	leaq	16(%rbp), %r12
	movl	$0, 16(%rbp)
	movq	%r12, %rdi
	call	*40(%rax)
	movq	8(%rbp), %rax
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, 0(%r13)
	call	__GI_xdr_replymsg
	testl	%eax, %eax
	je	.L53
	movq	24(%rbp), %rax
	movq	%r12, %rdi
	call	*32(%rax)
	cmpq	$0, 120(%rbx)
	movslq	%eax, %r12
	movq	%r12, %r13
	jne	.L74
	movq	64(%rbx), %rsi
	movl	16(%rbx), %r9d
	leaq	20(%rbx), %r8
	movl	(%rbx), %edi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	call	__sendto
.L52:
	cmpl	%r13d, %eax
	jne	.L53
	cmpq	$0, 464(%rbp)
	je	.L72
	testl	%eax, %eax
	js	.L72
	movq	72(%rbx), %r14
	movq	464(%r14), %r13
	movq	24(%r13), %rdx
	movq	16(%r13), %rax
	movq	(%rax,%rdx,8), %rbp
	testq	%rbp, %rbp
	je	.L54
	movq	0(%r13), %rax
	xorl	%edx, %edx
	leaq	0(,%rax,4), %rcx
	movq	0(%rbp), %rax
	divq	%rcx
	movq	8(%r13), %rax
	movl	%edx, %edx
	leaq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L55
	cmpq	%rbp, %rdx
	jne	.L57
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L76:
	cmpq	%rax, %rbp
	je	.L75
	movq	%rax, %rdx
.L57:
	movq	64(%rdx), %rax
	testq	%rax, %rax
	jne	.L76
.L55:
	leaq	.LC3(%rip), %rsi
	movl	$5, %edx
.L73:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
.L72:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%eax, %eax
.L48:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movq	64(%rbx), %rax
	movl	(%rbx), %edi
	leaq	96(%rbx), %rsi
	movq	%r12, 88(%rbx)
	xorl	%edx, %edx
	movq	%rax, 80(%rbx)
	call	__sendmsg
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	64(%rdx), %rax
.L56:
	movq	64(%rbp), %rdx
	movq	48(%rbp), %rsi
	movq	%rdx, (%rax)
	movl	(%r14), %edx
.L59:
	movq	64(%rbx), %rax
	leaq	16(%r14), %rdi
	movq	%r12, 56(%rbp)
	xorl	%ecx, %ecx
	movq	%rax, 48(%rbp)
	movq	%rsi, 64(%rbx)
	call	__GI_xdrmem_create
	movq	48(%r13), %rdx
	movq	8(%r14), %rax
	movdqu	56(%r13), %xmm0
	movq	%rdx, 8(%rbp)
	movq	40(%r13), %rdx
	movq	%rax, 0(%rbp)
	movq	%rdx, 16(%rbp)
	movq	32(%r13), %rdx
	movups	%xmm0, 32(%rbp)
	movq	%rdx, 24(%rbp)
	movq	72(%rbx), %rdx
	movq	464(%rdx), %rdx
	movq	(%rdx), %rcx
	xorl	%edx, %edx
	salq	$2, %rcx
	divq	%rcx
	movl	%edx, %eax
	movq	8(%r13), %rdx
	leaq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, 64(%rbp)
	movq	%rbp, (%rax)
	movq	16(%r13), %rdx
	movq	24(%r13), %rax
	movq	%rbp, (%rdx,%rax,8)
	addq	$1, %rax
	xorl	%edx, %edx
	divq	0(%r13)
	movl	$1, %eax
	movq	%rdx, 24(%r13)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$72, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	%rax, %rbp
	movl	$5, %edx
	leaq	.LC0(%rip), %rsi
	je	.L73
	movl	(%r14), %edi
	movl	%edi, 12(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rsi
	movl	12(%rsp), %edx
	jne	.L59
	movq	%r15, %rdi
	call	free@PLT
	movl	$5, %edx
	leaq	.LC2(%rip), %rsi
	jmp	.L73
	.size	svcudp_reply, .-svcudp_reply
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"svcudp_create: socket creation problem"
	.align 8
.LC5:
	.string	"svcudp_create - cannot getsockname"
	.section	.rodata.str1.1
.LC6:
	.string	"out of memory\n"
.LC7:
	.string	"svcudp_create"
.LC8:
	.string	"%s: %s"
	.text
	.p2align 4,,15
	.globl	__GI_svcudp_bufcreate
	.hidden	__GI_svcudp_bufcreate
	.type	__GI_svcudp_bufcreate, @function
__GI_svcudp_bufcreate:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%esi, %r14d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r13d
	movl	%edi, %r12d
	subq	$40, %rsp
	cmpl	$-1, %edi
	movl	$16, 8(%rsp)
	je	.L96
.L78:
	leaq	16(%rsp), %rbx
	xorl	%edx, %edx
	movq	$0, 18(%rsp)
	movl	$2, %ecx
	movl	%r12d, %edi
	movl	$0, 10(%rbx)
	movw	%dx, 14(%rbx)
	movq	%rbx, %rsi
	movw	%cx, 16(%rsp)
	call	__GI_bindresvport
	testl	%eax, %eax
	jne	.L97
.L80:
	leaq	8(%rsp), %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	__getsockname
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L98
	movl	$336, %edi
	call	malloc@PLT
	movl	$472, %edi
	movq	%rax, %rbp
	call	malloc@PLT
	cmpl	%r14d, %r13d
	movq	%rax, %r15
	cmovb	%r14d, %r13d
	addl	$3, %r13d
	andl	$-4, %r13d
	movl	%r13d, %edi
	call	malloc@PLT
	testq	%rbp, %rbp
	movq	%rax, %r14
	sete	%dl
	testq	%r15, %r15
	sete	%al
	orb	%al, %dl
	jne	.L89
	testq	%r14, %r14
	je	.L89
	leaq	16(%r15), %rdi
	movl	%r13d, (%r15)
	movl	$1, %ecx
	movl	%r13d, %edx
	movq	%r14, %rsi
	movq	%r14, 64(%rbp)
	call	__GI_xdrmem_create
	leaq	svcudp_op(%rip), %rax
	movq	$0, 464(%r15)
	movq	%r15, 72(%rbp)
	leaq	12(%rsp), %rcx
	addq	$64, %r15
	xorl	%esi, %esi
	movq	%rax, 8(%rbp)
	movzwl	18(%rsp), %eax
	movl	$4, %r8d
	movq	%r15, 48(%rbp)
	movl	%r12d, 0(%rbp)
	movl	$8, %edx
	movl	%r12d, %edi
	movl	$1, 12(%rsp)
	rolw	$8, %ax
	movw	%ax, 4(%rbp)
	call	__setsockopt
	testl	%eax, %eax
	jne	.L84
	movl	$255, 12(%rsp)
	movl	$255, %ebx
.L85:
	movzbl	%bl, %ecx
	movabsq	$72340172838076673, %rdi
	movq	%rcx, %rax
	movq	%rcx, %rsi
	mulq	%rdi
	imulq	%rdi, %rsi
	movq	%rax, 80(%rbp)
	movq	%rcx, %rax
	addq	%rdx, %rsi
	mulq	%rdi
	movq	%rsi, 88(%rbp)
	movq	%rsi, 104(%rbp)
	movq	%rsi, 120(%rbp)
	movq	%rsi, 136(%rbp)
	movq	%rsi, 152(%rbp)
	movq	%rsi, 168(%rbp)
	movq	%rsi, 184(%rbp)
	movq	%rsi, 200(%rbp)
	movq	%rax, 96(%rbp)
	movq	%rcx, %rax
	movq	%rsi, 216(%rbp)
	mulq	%rdi
	movq	%rsi, 232(%rbp)
	movq	%rsi, 248(%rbp)
	movq	%rsi, 264(%rbp)
	movq	%rsi, 280(%rbp)
	movq	%rsi, 296(%rbp)
	movq	%rsi, 312(%rbp)
	movq	%rsi, 328(%rbp)
	movq	%rax, 112(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 128(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 144(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 160(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 176(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 192(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 208(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 224(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 240(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 256(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 272(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 288(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rax, 304(%rbp)
	movq	%rcx, %rax
	mulq	%rdi
	movq	%rbp, %rdi
	movq	%rax, 320(%rbp)
	call	__GI_xprt_register
.L77:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	.LC5(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	xorl	%ebp, %ebp
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	testl	%r15d, %r15d
	je	.L77
	movl	%r12d, %edi
	call	__GI___close
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$17, %edx
	movl	$2, %esi
	movl	$2, %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %r12d
	movl	$1, %r15d
	jns	.L78
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	xorl	%ebp, %ebp
	call	__GI___dcgettext
	movq	%rax, %rdi
	call	__GI_perror
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L97:
	movl	8(%rsp), %edx
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movl	%r12d, %edi
	movw	%ax, 18(%rsp)
	call	__bind
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L84:
	movl	$0, 12(%rsp)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L89:
	leaq	.LC6(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	.LC7(%rip), %rdx
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%rbp, %rdi
	xorl	%ebp, %ebp
	call	free@PLT
	movq	%r15, %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	free@PLT
	jmp	.L77
	.size	__GI_svcudp_bufcreate, .-__GI_svcudp_bufcreate
	.globl	__EI_svcudp_bufcreate
	.set	__EI_svcudp_bufcreate,__GI_svcudp_bufcreate
	.p2align 4,,15
	.globl	__GI_svcudp_create
	.hidden	__GI_svcudp_create
	.type	__GI_svcudp_create, @function
__GI_svcudp_create:
	movl	$8800, %edx
	movl	$8800, %esi
	jmp	__GI_svcudp_bufcreate
	.size	__GI_svcudp_create, .-__GI_svcudp_create
	.globl	__EI_svcudp_create
	.set	__EI_svcudp_create,__GI_svcudp_create
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"enablecache: cache already enabled"
	.align 8
.LC10:
	.string	"enablecache: could not allocate cache"
	.align 8
.LC11:
	.string	"enablecache: could not allocate cache data"
	.align 8
.LC12:
	.string	"enablecache: could not allocate cache fifo"
	.text
	.p2align 4,,15
	.globl	__GI_svcudp_enablecache
	.hidden	__GI_svcudp_enablecache
	.type	__GI_svcudp_enablecache, @function
__GI_svcudp_enablecache:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	72(%rdi), %rbx
	cmpq	$0, 464(%rbx)
	je	.L101
	leaq	.LC9(%rip), %rsi
	movl	$5, %edx
.L107:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$72, %edi
	movq	%rsi, %r12
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movl	$5, %edx
	leaq	.LC10(%rip), %rsi
	je	.L107
	leaq	0(,%r12,4), %rsi
	movq	%r12, (%rax)
	movq	$0, 24(%rax)
	movl	$8, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, 8(%rbp)
	je	.L108
	movq	%r12, %rsi
	movl	$8, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rbp)
	je	.L109
	movq	%rbp, 464(%rbx)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movq	%rbp, %rdi
	call	free@PLT
	movl	$5, %edx
	leaq	.LC11(%rip), %rsi
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%r13, %rdi
	call	free@PLT
	movq	%rbp, %rdi
	call	free@PLT
	movl	$5, %edx
	leaq	.LC12(%rip), %rsi
	jmp	.L107
	.size	__GI_svcudp_enablecache, .-__GI_svcudp_enablecache
	.globl	__EI_svcudp_enablecache
	.set	__EI_svcudp_enablecache,__GI_svcudp_enablecache
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	svcudp_op, @object
	.size	svcudp_op, 48
svcudp_op:
	.quad	svcudp_recv
	.quad	svcudp_stat
	.quad	svcudp_getargs
	.quad	svcudp_reply
	.quad	svcudp_freeargs
	.quad	svcudp_destroy
	.hidden	__bind
	.hidden	__setsockopt
	.hidden	__getsockname
	.hidden	__fxprintf
	.hidden	__sendmsg
	.hidden	__svc_accept_failed
	.hidden	__recvfrom
	.hidden	__sendto
	.hidden	__recvmsg
