	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"exec"
.LC1:
	.string	"w"
.LC2:
	.string	"r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	_openchild
	.type	_openchild, @function
_openchild:
.LFB63:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdx, %rbp
	subq	$24, %rsp
	movq	%rsp, %rdi
	call	__GI___pipe
	testl	%eax, %eax
	js	.L9
	leaq	8(%rsp), %rdi
	call	__GI___pipe
	testl	%eax, %eax
	js	.L3
	call	__GI___fork
	cmpl	$-1, %eax
	movl	%eax, %ebx
	je	.L5
	testl	%eax, %eax
	je	.L14
	movl	4(%rsp), %edi
	leaq	.LC1(%rip), %rsi
	call	__GI__IO_fdopen
	movl	(%rsp), %edi
	movq	%rax, (%r12)
	call	__GI___close
	movl	8(%rsp), %edi
	leaq	.LC2(%rip), %rsi
	call	__GI__IO_fdopen
	movl	12(%rsp), %edi
	movq	%rax, 0(%rbp)
	call	__GI___close
.L1:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	8(%rsp), %edi
	call	__GI___close
	movl	12(%rsp), %edi
	call	__GI___close
.L3:
	movl	(%rsp), %edi
	movl	$-1, %ebx
	call	__GI___close
	movl	4(%rsp), %edi
	call	__GI___close
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$-1, %ebx
	jmp	.L1
.L14:
	xorl	%edi, %edi
	call	__GI___close
	movl	(%rsp), %edi
	call	__GI___dup
	movl	$1, %edi
	call	__GI___close
	movl	12(%rsp), %edi
	call	__GI___dup
	movq	stderr@GOTPCREL(%rip), %rbp
	movq	0(%rbp), %rdi
	call	__GI__IO_fflush
	call	__GI__rpc_dtablesize
	leal	-1(%rax), %ebx
	cmpl	$2, %ebx
	jle	.L7
.L8:
	movl	%ebx, %edi
	subl	$1, %ebx
	call	__GI___close
	cmpl	$2, %ebx
	jne	.L8
.L7:
	movq	0(%rbp), %rdi
	call	__GI__IO_fflush
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__GI_execlp
	leaq	.LC0(%rip), %rdi
	call	__GI_perror
	orl	$-1, %edi
	call	__GI__exit
.LFE63:
	.size	_openchild, .-_openchild
