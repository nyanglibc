	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___dl_iterate_phdr
	.hidden	__GI___dl_iterate_phdr
	.type	__GI___dl_iterate_phdr, @function
__GI___dl_iterate_phdr:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movq	_rtld_global@GOTPCREL(%rip), %r12
	leaq	2480(%r12), %rdi
	call	*3984(%r12)
	movq	2432(%r12), %rax
	movl	8(%r12), %ebp
	movq	152(%rsp), %r15
	leaq	-1(%rax), %rcx
	testq	%rcx, %rcx
	jle	.L11
	leaq	(%rax,%rax,8), %rdx
	xorl	%r8d, %r8d
	leaq	(%rax,%rdx,2), %rax
	leaq	-152(%r12,%rax,8), %rdx
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rdx), %rbx
	testq	%rbx, %rbx
	je	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	movl	8(%rdx), %eax
	addq	%rax, %rbp
	cmpq	856(%rbx), %r15
	jb	.L4
	cmpq	864(%rbx), %r15
	jnb	.L4
	testb	$64, 797(%rbx)
	je	.L29
	movq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L4:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L5
.L3:
	subq	$152, %rdx
	subq	$1, %rcx
	jne	.L6
.L2:
	leaq	(%r8,%r8,8), %rax
	leaq	(%r8,%rax,2), %rax
	movq	(%r12,%rax,8), %r15
	testq	%r15, %r15
	je	.L7
	leaq	32(%rsp), %rbx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.L7
.L10:
	movq	40(%r15), %rdi
	movq	(%rdi), %rax
	movq	%rax, 32(%rsp)
	movq	8(%rdi), %rax
	movq	$0, 88(%rsp)
	movq	%rax, 40(%rsp)
	movq	680(%rdi), %rax
	movq	%rax, 48(%rsp)
	movzwl	696(%rdi), %eax
	movw	%ax, 56(%rsp)
	movq	2520(%r12), %rax
	movq	%rax, 64(%rsp)
	subq	%rbp, %rax
	movq	%rax, 72(%rsp)
	movq	1120(%rdi), %rax
	testq	%rax, %rax
	movq	%rax, 80(%rsp)
	je	.L8
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	call	*776(%rax)
	movq	%rax, 88(%rsp)
.L8:
	movq	%r14, %rdx
	movl	$64, %esi
	movq	%rbx, %rdi
	call	*%r13
	testl	%eax, %eax
	je	.L30
.L9:
	movl	%eax, 8(%rsp)
	leaq	2480(%r12), %rdi
	call	*3992(%r12)
	movl	8(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r8, 8(%rsp)
	call	_dl_addr_inside_object
	movq	8(%rsp), %r8
	movq	24(%rsp), %rcx
	testl	%eax, %eax
	movq	16(%rsp), %rdx
	cmovne	%rcx, %r8
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	jmp	.L9
.L11:
	xorl	%r8d, %r8d
	jmp	.L2
	.size	__GI___dl_iterate_phdr, .-__GI___dl_iterate_phdr
	.globl	__dl_iterate_phdr
	.set	__dl_iterate_phdr,__GI___dl_iterate_phdr
	.weak	dl_iterate_phdr
	.set	dl_iterate_phdr,__dl_iterate_phdr
	.hidden	_dl_addr_inside_object
