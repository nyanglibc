	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__thread_gscope_wait
	.hidden	__thread_gscope_wait
	.type	__thread_gscope_wait, @function
__thread_gscope_wait:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movl	$1, %edx
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	lock cmpxchgl	%edx, 4144+_rtld_local(%rip)
	jne	.L34
.L2:
	movq	4112+_rtld_local(%rip), %r8
	leaq	4112+_rtld_local(%rip), %r12
	movq	%fs:16, %rbp
	cmpq	%r12, %r8
	je	.L3
	movl	$1, %r15d
	movl	$2, %r14d
	movl	$202, %ebx
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	-704(%r8), %r9
	cmpq	%r9, %rbp
	je	.L4
	movl	-676(%r8), %edx
	testl	%edx, %edx
	je	.L4
	addq	$28, %r9
	movl	%r15d, %eax
	lock cmpxchgl	%r14d, (%r9)
	je	.L8
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$2, -676(%r8)
	jne	.L4
.L8:
	xorl	%r10d, %r10d
	movl	$2, %edx
	movl	$128, %esi
	movq	%r9, %rdi
	movl	%ebx, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L5
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L7
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L5
.L7:
	leaq	.LC0(%rip), %rdi
	call	__GI___libc_fatal
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%r8), %r8
	cmpq	%r12, %r8
	jne	.L9
.L3:
	movq	4128+_rtld_local(%rip), %r8
	leaq	4128+_rtld_local(%rip), %r12
	cmpq	%r12, %r8
	je	.L10
	movl	$1, %r15d
	movl	$2, %r14d
	movl	$202, %ebx
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	-704(%r8), %r9
	cmpq	%r9, %rbp
	je	.L11
	movl	-676(%r8), %eax
	testl	%eax, %eax
	je	.L11
	addq	$28, %r9
	movl	%r15d, %eax
	lock cmpxchgl	%r14d, (%r9)
	je	.L13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$2, -676(%r8)
	jne	.L11
.L13:
	xorl	%r10d, %r10d
	movl	$2, %edx
	movl	$128, %esi
	movq	%r9, %rdi
	movl	%ebx, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L12
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L7
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	je	.L7
	cmpl	$2, -676(%r8)
	je	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%r8), %r8
	cmpq	%r12, %r8
	jne	.L14
.L10:
	xorl	%eax, %eax
#APP
# 79 "../sysdeps/nptl/dl-thread_gscope_wait.c" 1
	xchgl %eax, 4144+_rtld_local(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L35
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L34:
	leaq	4144+_rtld_local(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L2
.L35:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	4144+_rtld_local(%rip), %rdi
	movl	$202, %eax
#APP
# 79 "../sysdeps/nptl/dl-thread_gscope_wait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	__thread_gscope_wait, .-__thread_gscope_wait
	.hidden	__lll_lock_wait_private
	.hidden	_rtld_local
