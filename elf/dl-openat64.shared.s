	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/dl-openat64.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"!__OPEN_NEEDS_MODE (oflag)"
	.text
	.p2align 4,,15
	.globl	openat64
	.type	openat64, @function
openat64:
	testb	$64, %dl
	jne	.L2
	movl	%edx, %eax
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
	movl	$257, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/dl-openat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movl	$-1, %eax
	ret
.L2:
	leaq	__PRETTY_FUNCTION__.7351(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$28, %edx
	call	__GI___assert_fail
	.size	openat64, .-openat64
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.7351, @object
	.size	__PRETTY_FUNCTION__.7351, 9
__PRETTY_FUNCTION__.7351:
	.string	"openat64"
	.hidden	rtld_errno
