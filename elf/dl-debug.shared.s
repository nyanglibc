	.text
	.p2align 4,,15
	.globl	__GI__dl_debug_state
	.hidden	__GI__dl_debug_state
	.type	__GI__dl_debug_state, @function
__GI__dl_debug_state:
	rep ret
	.size	__GI__dl_debug_state, .-__GI__dl_debug_state
	.globl	_dl_debug_state
	.set	_dl_debug_state,__GI__dl_debug_state
	.p2align 4,,15
	.globl	_dl_debug_initialize
	.hidden	_dl_debug_initialize
	.type	_dl_debug_initialize, @function
_dl_debug_initialize:
	testq	%rsi, %rsi
	je	.L9
	leaq	(%rsi,%rsi,8), %rax
	leaq	(%rsi,%rax,2), %rdx
	leaq	_rtld_local(%rip), %rax
	leaq	112(%rax,%rdx,8), %rax
	cmpq	$0, 8(%rax)
	je	.L5
.L11:
	testq	%rdi, %rdi
	je	.L10
	movl	$1, (%rax)
.L8:
	leaq	(%rsi,%rsi,8), %rdx
	movq	%rdi, 32(%rax)
	leaq	(%rsi,%rdx,2), %rcx
	leaq	_rtld_local(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	leaq	__GI__dl_debug_state(%rip), %rcx
	movq	%rcx, 16(%rax)
	movq	%rdx, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	_r_debug@GOTPCREL(%rip), %rax
	cmpq	$0, 8(%rax)
	jne	.L11
.L5:
	testq	%rdi, %rdi
	movl	$1, (%rax)
	jne	.L8
	movq	_r_debug@GOTPCREL(%rip), %rdx
	movq	32(%rdx), %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	rep ret
	.size	_dl_debug_initialize, .-_dl_debug_initialize
	.comm	_r_debug,40,32
	.hidden	_rtld_local
