	.text
	.p2align 4,,15
	.globl	__GI__dl_make_stack_executable
	.hidden	__GI__dl_make_stack_executable
	.type	__GI__dl_make_stack_executable, @function
__GI__dl_make_stack_executable:
	movq	24+_rtld_local_ro(%rip), %rsi
	pushq	%rbx
	movq	%rdi, %rbx
	movl	__stack_prot(%rip), %edx
	movq	%rsi, %rdi
	negq	%rdi
	andq	(%rbx), %rdi
	call	__mprotect
	testl	%eax, %eax
	jne	.L6
.L2:
.L3:
	movq	$0, (%rbx)
	orl	$1, 4016+_rtld_local(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	rtld_errno(%rip), %eax
	popq	%rbx
	ret
	.size	__GI__dl_make_stack_executable, .-__GI__dl_make_stack_executable
	.globl	_dl_make_stack_executable
	.set	_dl_make_stack_executable,__GI__dl_make_stack_executable
	.hidden	rtld_errno
	.hidden	_rtld_local
	.hidden	__mprotect
	.hidden	__stack_prot
	.hidden	_rtld_local_ro
