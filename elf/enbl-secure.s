	.text
	.p2align 4,,15
	.globl	__libc_init_secure
	.hidden	__libc_init_secure
	.type	__libc_init_secure, @function
__libc_init_secure:
	movl	__libc_enable_secure_decided(%rip), %eax
	testl	%eax, %eax
	je	.L10
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	pushq	%rbx
	call	__geteuid
	movl	%eax, %ebx
	call	__getuid
	cmpl	%ebx, %eax
	movl	$1, %edx
	je	.L11
.L3:
	movl	%edx, __libc_enable_secure(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	call	__getegid
	movl	%eax, %ebx
	call	__getgid
	xorl	%edx, %edx
	cmpl	%ebx, %eax
	setne	%dl
	jmp	.L3
	.size	__libc_init_secure, .-__libc_init_secure
	.hidden	__libc_enable_secure_decided
	.comm	__libc_enable_secure_decided,4,4
	.hidden	__libc_enable_secure
	.globl	__libc_enable_secure
	.section	.data.rel.ro,"aw",@progbits
	.align 4
	.type	__libc_enable_secure, @object
	.size	__libc_enable_secure, 4
__libc_enable_secure:
	.long	1
	.hidden	__getgid
	.hidden	__getegid
	.hidden	__getuid
	.hidden	__geteuid
