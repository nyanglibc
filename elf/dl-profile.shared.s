	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: cannot open file: %s\n"
.LC1:
	.string	"%s: cannot stat file: %s\n"
.LC2:
	.string	"%s: cannot map file: %s\n"
.LC3:
	.string	"%s: cannot create file: %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"%s: file is no correct profile data file for `%s'\n"
	.align 8
.LC5:
	.string	"Out of memory while initializing profiler\n"
	.text
	.p2align 4,,15
	.globl	_dl_start_profile
	.hidden	_dl_start_profile
	.type	_dl_start_profile, @function
_dl_start_profile:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$680, %rsp
	movq	2536+_rtld_local(%rip), %r10
	movzwl	696(%r10), %ecx
	movq	680(%r10), %rax
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rdi
	cmpq	%rdi, %rax
	jnb	.L34
	movq	24+_rtld_local_ro(%rip), %r11
	movq	$-1, %rcx
	xorl	%esi, %esi
	movabsq	$8589934591, %r9
	movabsq	$4294967297, %r8
	movq	%r11, %rbx
	subq	$1, %r11
	negq	%rbx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rax), %rdx
	andq	%r9, %rdx
	cmpq	%r8, %rdx
	jne	.L3
	movq	16(%rax), %rdx
	movq	%rdx, %r12
	andq	%rbx, %r12
	cmpq	%r12, %rcx
	cmova	%r12, %rcx
	addq	40(%rax), %rdx
	addq	%r11, %rdx
	andq	%rbx, %rdx
	cmpq	%rdx, %rsi
	cmovb	%rdx, %rsi
.L3:
	addq	$56, %rax
	cmpq	%rdi, %rax
	jb	.L4
	movq	%rcx, %rdi
	movq	%rsi, %r8
.L2:
	movq	(%r10), %rax
	movl	$0, running(%rip)
	movl	$5, log_hashfraction(%rip)
	leaq	3(%rax,%rsi), %r12
	addq	%rax, %rcx
	andq	$-4, %rcx
	andq	$-4, %r12
	movq	%rcx, lowpc(%rip)
	movq	%r12, %r13
	subq	%rcx, %r13
	movabsq	$2951479051793528259, %rcx
	leaq	0(%r13,%r13,2), %rdx
	movq	%r13, textsize(%rip)
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	shrq	$2, %rdx
	cmpl	$49, %edx
	ja	.L5
	movl	$50, fromlimit(%rip)
	movq	$800, -688(%rbp)
	movq	$16072, -680(%rbp)
.L6:
	movq	%r13, %rax
	movq	%r8, -632(%rbp)
	movq	%rdi, -640(%rbp)
	shrq	$2, %rax
	movl	$1852796263, -672(%rbp)
	movl	$131071, -668(%rbp)
	movq	$0, -664(%rbp)
	movl	$0, -656(%rbp)
	movabsq	$32480047799690611, %rbx
	movl	%eax, -624(%rbp)
	call	__profile_frequency@PLT
	movq	640+_rtld_local_ro(%rip), %r15
	movl	%eax, -620(%rbp)
	leaq	-640(%rbp), %rax
	xorl	%esi, %esi
	movq	%rbx, -616(%rbp)
	movl	$0, -608(%rbp)
	movw	%si, -604(%rbp)
	movq	%rax, -696(%rbp)
	movq	%r15, %rdi
	movb	$0, -602(%rbp)
	movb	$115, -601(%rbp)
	call	strlen
	movq	632+_rtld_local_ro(%rip), %r14
	movq	%rax, %rbx
	movq	%r14, %rdi
	call	strlen
	leaq	40(%rbx,%rax), %rax
	movq	%r15, %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdi
	call	__stpcpy@PLT
	leaq	1(%rax), %rdi
	movq	%r14, %rsi
	movb	$47, (%rax)
	call	__stpcpy@PLT
	movabsq	$7308332183992823854, %rsi
	movb	$0, 8(%rax)
	movl	$438, %edx
	movq	%rsi, (%rax)
	movq	%rbx, %rdi
	xorl	%eax, %eax
	movl	$131138, %esi
	call	__GI___open64_nocancel
	cmpl	$-1, %eax
	movl	%eax, %r15d
	jne	.L50
	movl	rtld_errno(%rip), %r12d
	leaq	.LC0(%rip), %r14
.L33:
	leaq	-448(%rbp), %rsi
	movl	$400, %edx
	movl	%r12d, %edi
	call	__strerror_r
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	_dl_error_printf
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1048576, %edx
	jbe	.L51
	movl	$1048576, fromlimit(%rip)
	movq	$16777216, -688(%rbp)
	movq	$335544392, -680(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	-592(%rbp), %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L35
	movl	-568(%rbp), %eax
	leaq	.LC1(%rip), %r14
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L52
.L11:
	movl	%r15d, %edi
	movl	rtld_errno(%rip), %r12d
	call	__GI___close_nocancel
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC1(%rip), %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L34:
	movq	$-1, %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rdi, %rcx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-680(%rbp), %r14
	movq	%r13, %rax
	shrq	%rax
	movq	%rax, -704(%rbp)
	addq	%rax, %r14
	movq	-544(%rbp), %rax
	testq	%rax, %rax
	je	.L53
	cmpq	%r14, %rax
	jne	.L54
.L16:
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r15d, %r8d
	movl	$1, %ecx
	movl	$3, %edx
	movq	%r14, %rsi
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, -680(%rbp)
	je	.L37
	movl	%r15d, %edi
	call	__GI___close_nocancel
	movq	-680(%rbp), %rdx
	movq	-704(%rbp), %rax
	cmpq	$0, -544(%rbp)
	leaq	64(%rdx), %r15
	leaq	4(%r15,%rax), %rax
	leaq	4(%rax), %rcx
	movq	%rax, narcsp(%rip)
	movq	%rcx, data(%rip)
	jne	.L19
	movdqa	-672(%rbp), %xmm0
	movl	-656(%rbp), %ecx
	movl	$0, 20(%rdx)
	movups	%xmm0, (%rdx)
	movdqa	-640(%rbp), %xmm0
	movl	%ecx, 16(%rdx)
	movq	-608(%rbp), %rcx
	movups	%xmm0, 24(%rdx)
	movdqa	-624(%rbp), %xmm0
	movq	%rcx, 56(%rdx)
	movups	%xmm0, 40(%rdx)
	movl	$1, -4(%rax)
.L20:
	movl	$1, %esi
	movq	-688(%rbp), %rdi
	addq	-704(%rbp), %rdi
	movq	%rdx, -680(%rbp)
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	movq	%rax, tos(%rip)
	movq	-680(%rbp), %rdx
	je	.L55
	movq	narcsp(%rip), %rcx
	movq	-704(%rbp), %rbx
	movl	fromlimit(%rip), %edx
	movl	$0, fromidx(%rip)
	movl	(%rcx), %esi
	leaq	(%rax,%rbx), %r11
	movq	%r11, froms(%rip)
	cmpl	%edx, %esi
	jnb	.L26
	movl	(%rcx), %edx
.L26:
	testq	%rdx, %rdx
	movl	%edx, narcs(%rip)
	je	.L27
	movq	data(%rip), %r9
	leaq	(%rdx,%rdx,4), %rdx
	leaq	-20(%r9,%rdx,4), %rcx
	subq	$20, %r9
	.p2align 4,,10
	.p2align 3
.L28:
	movq	8(%rcx), %rdi
	movl	fromidx(%rip), %esi
	shrq	$2, %rdi
	leal	1(%rsi), %edx
	leaq	(%rax,%rdi,2), %rdi
	movl	%edx, fromidx(%rip)
	movl	%esi, %edx
	salq	$4, %rdx
	movzwl	(%rdi), %ebx
	addq	%r11, %rdx
	movq	%rcx, (%rdx)
	subq	$20, %rcx
	cmpq	%rcx, %r9
	movw	%bx, 8(%rdx)
	movw	%si, (%rdi)
	jne	.L28
.L27:
	movq	lowpc(%rip), %rsi
	movq	-704(%rbp), %rbx
	movl	$65536, %ecx
	subq	%rsi, %r12
	cmpq	%rbx, %r12
	jbe	.L29
	xorl	%edx, %edx
	movq	%r12, %rax
	movl	$1, %ecx
	divq	%rbx
	cmpq	$65535, %rax
	movq	%rax, %rdi
	ja	.L29
	cmpq	$255, %rax
	jbe	.L30
	movl	$65536, %eax
	xorl	%edx, %edx
	divq	%rdi
	movl	%eax, %ecx
.L29:
	movq	%rsi, %rdx
	movq	-704(%rbp), %rsi
	movq	%r15, %rdi
	call	__profil
	movl	$1, running(%rip)
	jmp	.L1
.L54:
	movl	%r15d, %edi
	call	__GI___close_nocancel
.L17:
	movq	632+_rtld_local_ro(%rip), %rdx
	leaq	.LC4(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L1
.L37:
	leaq	.LC2(%rip), %r14
	jmp	.L11
.L53:
	movq	24+_rtld_local_ro(%rip), %rcx
	movq	%rsp, -712(%rbp)
	xorl	%esi, %esi
	leaq	15(%rcx), %rax
	movq	%rcx, %rdx
	movq	%rcx, -720(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	movq	%rsp, -680(%rbp)
	call	memset@PLT
	movq	-720(%rbp), %rcx
	xorl	%edx, %edx
	movl	%r15d, %edi
	negq	%rcx
	andq	%r14, %rcx
	movq	%rcx, %rsi
	call	__lseek
	cmpq	$-1, %rax
	jne	.L13
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	$4, rtld_errno(%rip)
	jne	.L15
.L13:
	movq	24+_rtld_local_ro(%rip), %rax
	movq	-680(%rbp), %rsi
	movl	%r15d, %edi
	leaq	-1(%rax), %rdx
	andq	%r14, %rdx
	call	__GI___write_nocancel
	cmpq	$-1, %rax
	je	.L56
	testq	%rax, %rax
	js	.L15
	movq	-712(%rbp), %rsp
	jmp	.L16
.L15:
	leaq	.LC3(%rip), %r14
	movq	-712(%rbp), %rsp
	jmp	.L11
.L19:
	movq	(%rdx), %rcx
	movq	8(%rdx), %rsi
	xorq	-672(%rbp), %rcx
	xorq	-664(%rbp), %rsi
	orq	%rcx, %rsi
	jne	.L18
	movl	-656(%rbp), %ecx
	cmpl	%ecx, 16(%rdx)
	je	.L57
.L18:
	movq	%r14, %rsi
	movq	%rdx, %rdi
	call	__munmap
	jmp	.L17
.L57:
	movl	20(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L18
	movq	24(%rdx), %rsi
	movq	32(%rdx), %rdi
	xorq	-640(%rbp), %rsi
	xorq	-632(%rbp), %rdi
	orq	%rsi, %rdi
	jne	.L18
	movq	40(%rdx), %rsi
	movq	48(%rdx), %rdi
	xorq	-624(%rbp), %rsi
	xorq	-616(%rbp), %rdi
	orq	%rsi, %rdi
	jne	.L18
	movq	-696(%rbp), %rsi
	movq	32(%rsi), %rsi
	cmpq	%rsi, 56(%rdx)
	jne	.L18
	movl	-4(%rax), %eax
	subl	$1, %eax
	jne	.L18
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	movabsq	$72057594037927935, %rax
	cmpq	%rax, %r12
	jbe	.L31
	movq	%r13, %r10
	movq	%r12, %rax
	xorl	%edx, %edx
	shrq	$9, %r10
	divq	%r10
	xorl	%edx, %edx
	movq	%rax, %r12
	movl	$16777216, %eax
	divq	%r12
	movl	%eax, %ecx
	jmp	.L29
.L31:
	salq	$8, %r12
	xorl	%edx, %edx
	movq	%r12, %rax
	divq	-704(%rbp)
	xorl	%edx, %edx
	movq	%rax, %rcx
	movl	$16777216, %eax
	divq	%rcx
	movl	%eax, %ecx
	jmp	.L29
.L55:
	movq	%rdx, %rdi
	movq	%r14, %rsi
	call	__munmap
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L51:
	movl	%edx, fromlimit(%rip)
	movl	%edx, %edx
	salq	$4, %rdx
	imulq	$20, %rdx, %r15
	movq	%rdx, -688(%rbp)
	leaq	72(%r15), %rax
	movq	%rax, -680(%rbp)
	jmp	.L6
	.size	_dl_start_profile, .-_dl_start_profile
	.p2align 4,,15
	.globl	__GI__dl_mcount
	.hidden	__GI__dl_mcount
	.type	__GI__dl_mcount, @function
__GI__dl_mcount:
	movl	running(%rip), %ecx
	testl	%ecx, %ecx
	je	.L86
	movq	lowpc(%rip), %rdx
	movq	textsize(%rip), %rax
	movl	$0, %ecx
	subq	%rdx, %rdi
	cmpq	%rax, %rdi
	cmovnb	%rcx, %rdi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L86
	movl	log_hashfraction(%rip), %ecx
	movq	%rsi, %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	shrq	%cl, %rax
	movq	tos(%rip), %rcx
	leaq	(%rcx,%rax,2), %r10
	movzwl	(%r10), %eax
	testq	%rax, %rax
	jne	.L89
.L63:
	movq	narcsp(%rip), %r11
	movl	narcs(%rip), %eax
	movl	(%r11), %edx
	cmpl	%edx, %eax
	je	.L68
	cmpl	%eax, fromlimit(%rip)
	jbe	.L68
	movq	froms(%rip), %r8
	movq	data(%rip), %r9
	movl	$1, %ebx
.L69:
	leaq	(%rax,%rax,4), %rax
	movq	8(%r9,%rax,4), %rbp
	movl	%ebx, %eax
	shrq	$2, %rbp
#APP
# 552 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %eax, fromidx(%rip)
# 0 "" 2
#NO_APP
	movl	narcs(%rip), %r12d
	movl	%eax, -12(%rsp)
	leaq	(%rcx,%rbp,2), %rbp
	movl	-12(%rsp), %edx
	leaq	(%r12,%r12,4), %r12
	leal	1(%rdx), %eax
	leaq	(%r9,%r12,4), %r12
	movq	%rax, %rdx
	salq	$4, %rax
	addq	%r8, %rax
	movq	%r12, (%rax)
	movzwl	0(%rbp), %r12d
	movw	%r12w, 8(%rax)
	movw	%dx, 0(%rbp)
#APP
# 556 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incl narcs(%rip)
# 0 "" 2
#NO_APP
	movl	narcs(%rip), %eax
	movl	(%r11), %edx
	cmpl	%edx, %eax
	je	.L68
	cmpl	fromlimit(%rip), %eax
	jb	.L69
.L68:
	movzwl	(%r10), %eax
	testw	%ax, %ax
	jne	.L70
	movl	$1, %eax
	movl	%eax, %edx
#APP
# 562 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %edx, (%r11)
# 0 "" 2
#NO_APP
	movl	%edx, -8(%rsp)
	movl	-8(%rsp), %edx
	cmpl	fromlimit(%rip), %edx
	jnb	.L58
#APP
# 569 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %eax, fromidx(%rip)
# 0 "" 2
#NO_APP
	movl	%eax, -4(%rsp)
	movl	-4(%rsp), %eax
	leaq	(%rdx,%rdx,4), %rcx
	movq	data(%rip), %rdx
	addl	$1, %eax
	movw	%ax, (%r10)
	movzwl	(%r10), %eax
	leaq	(%rdx,%rcx,4), %rdx
	salq	$4, %rax
	addq	froms(%rip), %rax
	movq	%rdx, (%rax)
	movq	%rdi, (%rdx)
	movq	%rsi, 8(%rdx)
	movl	$0, 16(%rdx)
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
#APP
# 577 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incl narcs(%rip)
# 0 "" 2
#NO_APP
	movq	(%rax), %r9
.L67:
#APP
# 590 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incl 16(%r9)
# 0 "" 2
.L60:
.L72:
#NO_APP
.L58:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	rep ret
	.p2align 4,,10
	.p2align 3
.L70:
	movzwl	(%r10), %eax
.L89:
	movq	froms(%rip), %r8
	salq	$4, %rax
	addq	%r8, %rax
	movq	(%rax), %r9
	movq	(%r9), %rdx
	cmpq	%rdx, %rdi
	je	.L67
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	jne	.L66
.L65:
	movq	(%r9), %rdx
	cmpq	%rdx, %rdi
	je	.L67
	leaq	8(%rax), %r10
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L90:
	movq	(%r9), %r10
	cmpq	%rdi, %r10
	je	.L65
.L66:
	salq	$4, %rdx
	leaq	(%r8,%rdx), %rax
	movzwl	8(%rax), %edx
	movq	(%rax), %r9
	testw	%dx, %dx
	jne	.L90
	jmp	.L65
	.size	__GI__dl_mcount, .-__GI__dl_mcount
	.globl	_dl_mcount
	.set	_dl_mcount,__GI__dl_mcount
	.local	log_hashfraction
	.comm	log_hashfraction,4,4
	.local	textsize
	.comm	textsize,8,8
	.local	lowpc
	.comm	lowpc,8,8
	.local	fromidx
	.comm	fromidx,4,4
	.local	fromlimit
	.comm	fromlimit,4,4
	.local	froms
	.comm	froms,8,8
	.local	tos
	.comm	tos,8,8
	.local	narcsp
	.comm	narcsp,8,8
	.local	narcs
	.comm	narcs,4,4
	.local	running
	.comm	running,4,4
	.local	data
	.comm	data,8,8
	.hidden	__munmap
	.hidden	__lseek
	.hidden	__profil
	.hidden	__rtld_calloc
	.hidden	__mmap
	.hidden	_dl_error_printf
	.hidden	__strerror_r
	.hidden	rtld_errno
	.hidden	strlen
	.hidden	_rtld_local_ro
	.hidden	_rtld_local
