	.text
	.p2align 4,,15
	.type	_dl_build_local_scope, @function
_dl_build_local_scope:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, (%rdi)
	leaq	8(%rdi), %rbp
	movzbl	796(%rsi), %eax
	movq	976(%rsi), %rbx
	andl	$-97, %eax
	orl	$32, %eax
	testq	%rbx, %rbx
	movb	%al, 796(%rsi)
	je	.L2
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2
	addq	$8, %rbx
.L4:
	testb	$96, 796(%rsi)
	je	.L14
.L3:
	addq	$8, %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L4
.L2:
	movq	%rbp, %rax
	subq	%r12, %rax
	popq	%rbx
	sarq	$3, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rbp, %rdi
	call	_dl_build_local_scope
	leaq	0(%rbp,%rax,8), %rbp
	jmp	.L3
	.size	_dl_build_local_scope, .-_dl_build_local_scope
	.p2align 4,,15
	.type	openaux, @function
openaux:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	8(%rbx), %ecx
	movq	24(%rbx), %rsi
	movl	12(%rbx), %r8d
	movzbl	796(%rdi), %eax
	movq	48(%rdi), %r9
	andl	$3, %eax
	testb	%al, %al
	movzbl	%al, %edx
	movl	$1, %eax
	cmove	%eax, %edx
	call	_dl_map_object
	movq	%rax, 32(%rbx)
	popq	%rbx
	ret
	.size	openaux, .-openaux
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot allocate dependency buffer"
	.align 8
.LC2:
	.string	"DST not allowed in SUID/SGID programs"
	.section	.rodata.str1.1
.LC3:
	.string	"dl-deps.c"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"(l)->l_name[0] == '\\0' || IS_RTLD (l)"
	.align 8
.LC5:
	.string	"cannot load auxiliary `%s' because of empty dynamic string token substitution\n"
	.align 8
.LC6:
	.string	"empty dynamic string token substitution"
	.align 8
.LC7:
	.string	"load auxiliary object=%s requested by file=%s\n"
	.align 8
.LC8:
	.string	"cannot allocate dependency list"
	.align 8
.LC9:
	.string	"map->l_searchlist.r_list == NULL"
	.align 8
.LC10:
	.string	"cannot allocate symbol search list"
	.align 8
.LC11:
	.string	"Filters not supported with LD_TRACE_PRELINKING"
	.section	.rodata.str1.1
.LC12:
	.string	"cnt <= nlist"
.LC13:
	.string	"map_index < nlist"
	.text
	.p2align 4,,15
	.globl	_dl_map_object_deps
	.hidden	_dl_map_object_deps
	.type	_dl_map_object_deps, @function
_dl_map_object_deps:
	leal	2(%rdx), %eax
	pushq	%rbp
	leaq	(%rax,%rax,2), %rax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	leaq	30(,%rax,8), %rax
	pushq	%r12
	pushq	%rbx
	shrq	$4, %rax
	subq	$1240, %rsp
	movl	%r8d, -1260(%rbp)
	salq	$4, %rax
	movl	%ecx, -1240(%rbp)
	movq	%rdi, -1216(%rbp)
	subq	%rax, %rsp
	movzbl	796(%rdi), %eax
	leaq	15(%rsp), %r8
	andq	$-16, %r8
	leaq	24(%r8), %rcx
	movb	%al, -1176(%rbp)
	andl	$-97, %eax
	orl	$32, %eax
	testl	%edx, %edx
	movq	%r8, %r15
	movl	$0, (%r8)
	movq	%rdi, 8(%r8)
	movq	%rcx, 16(%r8)
	movb	%al, 796(%rdi)
	je	.L130
	addl	$1, %edx
	movl	$1, %r9d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L131:
	movl	%r10d, %r9d
.L21:
	leal	1(%r9), %eax
	movq	(%rsi), %rdi
	movl	$0, (%rcx)
	addq	$8, %rsi
	addq	$24, %rcx
	movq	%rax, %r10
	leaq	(%rax,%rax,2), %rax
	movq	%rdi, -16(%rcx)
	leaq	(%r8,%rax,8), %rax
	movq	%rax, -8(%rcx)
	movzbl	796(%rdi), %eax
	andl	$-97, %eax
	orl	$32, %eax
	cmpl	%r10d, %edx
	movb	%al, 796(%rdi)
	jne	.L131
	leaq	(%r9,%r9,2), %rax
	movq	8(%r8), %r13
	movl	%r10d, -1236(%rbp)
	leaq	(%r8,%rax,8), %rax
	movq	%rax, -1192(%rbp)
.L20:
	movq	-1192(%rbp), %rax
	movq	$1024, -1080(%rbp)
	movq	%r8, -1248(%rbp)
	movq	%r15, -1280(%rbp)
	movq	$0, 16(%rax)
	leaq	-1088(%rbp), %rax
	movq	%rax, -1272(%rbp)
	addq	$16, %rax
	movq	%rax, -1088(%rbp)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, -1264(%rbp)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	leaq	-1136(%rbp), %rax
	movq	%rax, -1208(%rbp)
	leaq	-1168(%rbp), %rax
	movq	%rax, -1200(%rbp)
.L22:
	movq	704(%r13), %r15
	movq	-1248(%rbp), %rax
	testq	%r15, %r15
	movl	$1, (%rax)
	je	.L268
	xorl	%r15d, %r15d
.L23:
	cmpq	$0, 72(%r13)
	je	.L269
.L25:
	movq	104(%r13), %rax
	movq	16(%r13), %r12
	movq	%r13, -1136(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -1184(%rbp)
	movq	%rax, -1120(%rbp)
	movl	-1240(%rbp), %eax
	movl	%eax, -1128(%rbp)
	movl	-1260(%rbp), %eax
	movl	%eax, -1124(%rbp)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L133
	movq	-1248(%rbp), %rbx
	movl	$0, -1176(%rbp)
	movq	%rbx, -1256(%rbp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L273:
	movl	__libc_enable_secure(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L270
	movq	%r14, %rdi
	call	strlen
	movq	848(%r13), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	testq	%rdi, %rdi
	je	.L271
	cmpq	$-1, %rdi
	je	.L35
	movq	%rax, -1232(%rbp)
	movq	%rax, -1224(%rbp)
	call	strlen
	movq	-1232(%rbp), %rcx
	movq	-1224(%rbp), %rdx
.L34:
	cmpq	%rax, _dl_platformlen(%rip)
	cmovnb	_dl_platformlen(%rip), %rax
	cmpq	$4, %rax
	jbe	.L36
	subq	$4, %rax
	imulq	%rax, %rbx
	leaq	(%rcx,%rbx), %rdx
.L36:
	leaq	30(%rdx), %rbx
	movq	%r14, %rsi
	movq	%r13, %rdi
	andq	$-16, %rbx
	subq	%rbx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	call	_dl_dst_substitute
	cmpb	$0, (%rax)
	je	.L272
.L29:
	movq	-1208(%rbp), %rdx
	movq	-1200(%rbp), %rdi
	leaq	openaux(%rip), %rsi
	movq	%rax, -1112(%rbp)
	call	_dl_catch_exception
	cmpq	$0, -1160(%rbp)
	jne	.L262
	movq	-1104(%rbp), %rax
	testb	$96, 796(%rax)
	jne	.L40
	subq	$48, %rsp
	movq	-1192(%rbp), %rbx
	addl	$1, -1236(%rbp)
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rax, 8(%rdx)
	movl	$0, (%rdx)
	movq	$0, 16(%rdx)
	movq	%rdx, 16(%rbx)
	movzbl	796(%rax), %ecx
	movq	%rdx, -1192(%rbp)
	andl	$-97, %ecx
	orl	$32, %ecx
	movb	%cl, 796(%rax)
.L40:
	testq	%r15, %r15
	je	.L37
	movl	-1176(%rbp), %edx
	movq	%rax, (%r15,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, -1176(%rbp)
.L37:
	addq	$16, %r12
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L26
.L68:
	cmpq	$1, %rax
	jne	.L28
	movq	-1184(%rbp), %r14
	addq	8(%r12), %r14
	movq	%r14, %rdi
	call	_dl_dst_count
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L273
	movq	%r14, %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L272:
	testb	$1, _dl_debug_mask(%rip)
	je	.L37
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	addq	$16, %r12
	movq	%r14, %rsi
	call	_dl_debug_printf
	movq	(%r12), %rax
	testq	%rax, %rax
	jne	.L68
	.p2align 4,,10
	.p2align 3
.L26:
	testq	%r15, %r15
	je	.L70
	movl	-1176(%rbp), %eax
	leal	1(%rax), %ebx
	movq	$0, (%r15,%rax,8)
	leal	1(%rbx,%rbx), %edi
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L274
	salq	$3, %rbx
	leaq	8(%rax), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r13, (%rax)
	call	memcpy@PLT
	movl	-1176(%rbp), %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	addl	$2, %eax
	leaq	(%r12,%rax,8), %rdi
	call	memcpy@PLT
	orb	$1, 798(%r13)
	movq	%r12, 976(%r13)
.L70:
	movq	-1248(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L73
.L74:
	movq	-1248(%rbp), %rax
	movq	8(%rax), %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L271:
	movq	8(%r13), %rax
	cmpb	$0, (%rax)
	jne	.L275
	movq	%rcx, -1232(%rbp)
	movq	%rcx, -1224(%rbp)
	call	_dl_get_origin
	leaq	-1(%rax), %rsi
	movq	%rax, 848(%r13)
	movq	-1224(%rbp), %rdx
	movq	-1232(%rbp), %rcx
	cmpq	$-3, %rsi
	ja	.L35
	movq	%rax, %rdi
	movq	%rcx, -1232(%rbp)
	movq	%rdx, -1224(%rbp)
	call	strlen
	movq	-1224(%rbp), %rdx
	movq	-1232(%rbp), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L28:
	andq	$-3, %rax
	cmpq	$2147483645, %rax
	jne	.L37
	movq	-1184(%rbp), %rbx
	addq	8(%r12), %rbx
	movq	%rbx, %rdi
	call	_dl_dst_count
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L41
	movl	__libc_enable_secure(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L276
	movq	%rbx, %rdi
	call	strlen
	movq	848(%r13), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	testq	%rdi, %rdi
	je	.L277
	cmpq	$-1, %rdi
	je	.L47
	movq	%rax, -1232(%rbp)
	movq	%rax, -1224(%rbp)
	call	strlen
	movq	-1232(%rbp), %rcx
	movq	-1224(%rbp), %rdx
.L46:
	cmpq	%rax, _dl_platformlen(%rip)
	cmovnb	_dl_platformlen(%rip), %rax
	cmpq	$4, %rax
	jbe	.L48
	subq	$4, %rax
	imulq	%r14, %rax
	leaq	(%rcx,%rax), %rdx
.L48:
	leaq	30(%rdx), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	call	_dl_dst_substitute
	cmpb	$0, (%rax)
	jne	.L136
	cmpq	$2147483645, (%r12)
	je	.L278
	testb	$1, _dl_debug_mask(%rip)
	je	.L37
	leaq	.LC5(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L262:
	testl	%eax, %eax
	movl	%eax, %ebx
	movq	-1280(%rbp), %r15
	je	.L279
.L69:
	movq	-1272(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L75
	call	free@PLT
.L75:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %esi
	testl	%esi, %esi
	jne	.L76
	movl	-1264(%rbp), %eax
	testl	%eax, %eax
	jne	.L280
.L76:
	movq	-1216(%rbp), %rsi
	movq	976(%rsi), %rax
	testq	%rax, %rax
	movq	%rax, -1176(%rbp)
	je	.L77
	movzbl	796(%rsi), %eax
	movb	%al, -1184(%rbp)
	andl	$3, %eax
	cmpb	$2, %al
	je	.L281
	movq	$0, -1176(%rbp)
.L77:
	movl	-1236(%rbp), %eax
	leal	1(%rax,%rax), %edi
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L91
	movl	-1236(%rbp), %ecx
	movq	-1216(%rbp), %rdi
	xorl	%r12d, %r12d
	movl	-1240(%rbp), %r8d
	leal	1(%rcx), %eax
	movl	%ecx, 712(%rdi)
	movl	$-1, %ecx
	leaq	(%r14,%rax,8), %rsi
	movq	%rsi, 704(%rdi)
	.p2align 4,,10
	.p2align 3
.L82:
	testl	%r8d, %r8d
	movq	8(%r15), %rax
	jne	.L282
.L79:
	movl	%r12d, %edx
	cmpq	%rax, %rdi
	movq	%rax, (%rsi,%rdx,8)
	movq	8(%r15), %rax
	cmove	%r12d, %ecx
	addl	$1, %r12d
.L80:
	andb	$-97, 796(%rax)
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.L82
	testb	$8, 1+_dl_debug_mask(%rip)
	jne	.L283
.L83:
	movq	-1216(%rbp), %rax
	movq	984(%rax), %r8
	testq	%r8, %r8
	je	.L98
	testl	%r12d, %r12d
	movl	(%r8), %r10d
	leaq	8(%r8), %r9
	je	.L99
	leal	-1(%r12), %eax
	movl	%eax, -1184(%rbp)
.L127:
	movq	-1216(%rbp), %rax
	movl	-1184(%rbp), %r13d
	movq	704(%rax), %rdi
	leaq	8(%rdi,%r13,8), %r11
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L100:
	movq	(%rdx), %rsi
	addq	$8, %rdx
	movzbl	796(%rsi), %eax
	andl	$-97, %eax
	orl	$32, %eax
	cmpq	%rdx, %r11
	movb	%al, 796(%rsi)
	jne	.L100
	movq	-1216(%rbp), %rax
	andb	$-97, 796(%rax)
	testl	%r10d, %r10d
	je	.L284
.L124:
	leaq	8(%r9), %rax
	movl	%ebx, -1184(%rbp)
	movl	%r12d, -1208(%rbp)
	xorl	%r11d, %r11d
	xorl	%r13d, %r13d
	movq	%r8, %rbx
	movq	%rax, -1224(%rbp)
	movq	%r9, %r12
	movl	%ecx, -1192(%rbp)
	movq	%r14, -1200(%rbp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	%r14d, (%rbx)
	movl	%r14d, %r13d
	jbe	.L285
.L107:
	movl	%r13d, %eax
	leal	1(%r13), %r14d
	leaq	0(,%rax,8), %r15
	movq	(%r12,%rax,8), %rax
	testb	$96, 796(%rax)
	je	.L103
	movq	-1216(%rbp), %rax
	movl	992(%rax), %eax
	leaq	8(,%rax,8), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r11
	je	.L103
	leaq	8(%rax), %rdi
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%rax, -1232(%rbp)
	call	memcpy@PLT
	movl	(%rbx), %ecx
	movq	-1232(%rbp), %r11
	cmpl	%r14d, %ecx
	jbe	.L143
	leal	-2(%rcx), %edx
	movl	%r14d, %esi
	leaq	(%r12,%rsi,8), %rax
	subl	%r13d, %edx
	addq	%rsi, %rdx
	movq	-1224(%rbp), %rsi
	leaq	(%rsi,%rdx,8), %rdi
	movl	%r13d, %esi
	.p2align 4,,10
	.p2align 3
.L106:
	movq	(%rax), %rdx
	testb	$96, 796(%rdx)
	jne	.L105
	movl	%esi, %r8d
	addl	$1, %esi
	movq	%rdx, 8(%r11,%r8,8)
.L105:
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L106
	leal	-1(%rcx,%r14), %ecx
	subl	%r13d, %ecx
	movl	%ecx, %r14d
.L104:
	addl	$1, %r14d
	movl	%esi, (%r11)
	cmpl	%r14d, (%rbx)
	movl	%r14d, %r13d
	ja	.L107
	.p2align 4,,10
	.p2align 3
.L285:
	movl	-1208(%rbp), %r12d
	movl	-1184(%rbp), %ebx
	movl	-1192(%rbp), %ecx
	movq	-1200(%rbp), %r14
	testl	%r12d, %r12d
	je	.L102
	movq	-1216(%rbp), %rax
	movq	%r11, %r8
	movq	704(%rax), %rdi
.L125:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L109:
	movq	(%rdi,%rax,8), %rdx
	addq	$1, %rax
	andb	$-97, 796(%rdx)
	cmpl	%eax, %r12d
	ja	.L109
.L98:
	cmpl	%r12d, %ecx
	jnb	.L102
	movq	-1216(%rbp), %rax
	movl	%r12d, %r13d
	testl	%ecx, %ecx
	leaq	0(,%r13,8), %rdx
	movq	704(%rax), %rsi
	je	.L110
	movl	%ecx, %eax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, (%r14)
	leal	-1(%rcx), %eax
	leaq	8(,%rax,8), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rsi,%rax), %rdx
	movq	%rdx, 8(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L111
	leal	1(%rcx), %eax
	cmpl	%r12d, %eax
	jnb	.L113
	leal	-2(%r12), %edi
	movl	%eax, %edx
	leaq	0(,%rdx,8), %rax
	movl	%edi, %r11d
	subl	%ecx, %r11d
	leaq	1(%rdx,%r11), %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rsi,%rax), %rdx
	movq	%rdx, (%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L114
.L113:
	movq	(%r14), %rdx
	movq	%r8, -1184(%rbp)
	movq	48(%rdx), %rax
	leaq	(%rax,%rax,8), %rcx
	leaq	(%rax,%rcx,2), %rcx
	leaq	_dl_ns(%rip), %rax
	cmpq	32(%rax,%rcx,8), %rdx
	je	.L286
	leal	-1(%r12), %esi
	leaq	8(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_dl_sort_maps
	movq	-1184(%rbp), %r8
.L117:
	movq	$0, (%r14,%r13,8)
	movq	-1216(%rbp), %rax
	orb	$1, 798(%rax)
	testq	%r8, %r8
	movq	%r14, 976(%rax)
	je	.L118
	movq	984(%rax), %rdi
	movq	%r8, 984(%rax)
	call	_dl_scope_free
.L118:
	movq	-1176(%rbp), %rax
	testq	%rax, %rax
	je	.L119
	movq	%rax, %rdi
	call	_dl_scope_free
.L119:
	testl	%ebx, %ebx
	jne	.L287
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	cmpq	$0, 488(%r13)
	jne	.L25
	cmpq	$0, 472(%r13)
	jne	.L25
.L133:
	movl	$0, -1176(%rbp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L282:
	testb	$2, 797(%rax)
	je	.L79
	subl	$1, 712(%rdi)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L279:
	movl	$-1, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L280:
	movq	__libc_errno@gottpoff(%rip), %rsi
	movl	%eax, %fs:(%rsi)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L268:
	cmpq	$0, 976(%r13)
	jne	.L23
	cmpq	%r13, -1216(%rbp)
	je	.L23
	movzwl	698(%r13), %eax
	testw	%ax, %ax
	je	.L23
	movq	-1272(%rbp), %rdi
	movzwl	%ax, %esi
	movl	$8, %edx
	call	__libc_scratch_buffer_set_array_size
	testb	%al, %al
	je	.L288
	movq	-1088(%rbp), %r15
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L290:
	movl	(%rax), %edi
	testl	%edi, %edi
	je	.L289
.L73:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L290
	movq	-1280(%rbp), %r15
	xorl	%ebx, %ebx
	jmp	.L69
.L136:
	movq	%rax, %rbx
.L41:
	testb	$1, _dl_debug_mask(%rip)
	movq	%rbx, -1112(%rbp)
	jne	.L291
.L50:
	movq	-1208(%rbp), %rdx
	movq	-1200(%rbp), %rdi
	leaq	openaux(%rip), %rsi
	call	_dl_catch_exception
	cmpq	$0, -1160(%rbp)
	jne	.L292
	movq	-1256(%rbp), %rbx
	subq	$48, %rsp
	leaq	15(%rsp), %rcx
	movdqu	(%rbx), %xmm0
	andq	$-16, %rcx
	testq	%r15, %r15
	movaps	%xmm0, (%rcx)
	movq	16(%rbx), %rax
	movq	%rax, 16(%rcx)
	movq	-1104(%rbp), %rax
	movl	$0, (%rbx)
	movq	%rax, 8(%rbx)
	je	.L55
	movl	-1176(%rbp), %edx
	movq	%rdx, %rbx
	movq	%rax, (%r15,%rdx,8)
	movq	-1104(%rbp), %rax
	addl	$1, %ebx
	movl	%ebx, -1176(%rbp)
.L55:
	testb	$96, 796(%rax)
	jne	.L293
	movq	-1256(%rbp), %rbx
	movq	32(%rax), %rsi
	addl	$1, -1236(%rbp)
	movq	%rcx, 16(%rbx)
	movzbl	796(%rax), %edx
	andl	$-97, %edx
	orl	$32, %edx
	testq	%rsi, %rsi
	movb	%dl, 796(%rax)
	movq	24(%rax), %rdx
	je	.L65
.L267:
	movq	%rdx, 24(%rsi)
	movq	24(%rax), %rdx
.L65:
	testq	%rdx, %rdx
	je	.L66
	movq	%rsi, 32(%rdx)
.L66:
	movq	8(%rcx), %rdx
	movq	32(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movq	%rax, 32(%rdx)
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L67
	movq	%rax, 24(%rsi)
.L67:
	movq	-1192(%rbp), %rbx
	cmpq	%rbx, -1256(%rbp)
	movq	%rdx, 24(%rax)
	je	.L139
	movq	%rcx, -1256(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r14, %rdi
	movq	%r8, -1184(%rbp)
	call	memcpy@PLT
	movq	-1184(%rbp), %r8
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L286:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_dl_sort_maps
	movq	-1184(%rbp), %r8
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L281:
	cmpq	$0, 704(%rsi)
	je	.L77
	leaq	__PRETTY_FUNCTION__.10217(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$472, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L293:
	movq	%rcx, %rsi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L59:
	cmpq	8(%rdx), %rax
	je	.L58
	movq	%rdx, %rsi
.L56:
	movq	16(%rsi), %rdx
	testq	%rdx, %rdx
	jne	.L59
	movq	-1256(%rbp), %rbx
	movdqa	(%rcx), %xmm0
	movups	%xmm0, (%rbx)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L37
.L277:
	movq	8(%r13), %rax
	cmpb	$0, (%rax)
	jne	.L294
	movq	%rcx, -1232(%rbp)
	movq	%rcx, -1224(%rbp)
	call	_dl_get_origin
	leaq	-1(%rax), %rsi
	movq	%rax, 848(%r13)
	movq	-1224(%rbp), %rdx
	movq	-1232(%rbp), %rcx
	cmpq	$-3, %rsi
	ja	.L47
	movq	%rax, %rdi
	movq	%rcx, -1232(%rbp)
	movq	%rdx, -1224(%rbp)
	call	strlen
	movq	-1224(%rbp), %rdx
	movq	-1232(%rbp), %rcx
	jmp	.L46
.L47:
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L130:
	movq	-1216(%rbp), %r13
	movq	%r8, -1192(%rbp)
	movl	$1, -1236(%rbp)
	jmp	.L20
.L84:
	movq	-1216(%rbp), %rax
	movq	984(%rax), %r8
	testq	%r8, %r8
	je	.L102
	movl	(%r8), %r10d
	leaq	8(%r8), %r9
.L99:
	movq	-1216(%rbp), %rax
	andb	$-97, 796(%rax)
	testl	%r10d, %r10d
	jne	.L124
.L102:
	leaq	__PRETTY_FUNCTION__.10217(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$596, %edx
	call	__assert_fail
.L283:
	movq	-1216(%rbp), %rax
	cmpq	%rax, _dl_ns(%rip)
	jne	.L83
	testl	%r12d, %r12d
	je	.L84
	leal	-1(%r12), %eax
	movq	-1216(%rbp), %r15
	xorl	%r8d, %r8d
	movl	%ebx, -1224(%rbp)
	movl	%ecx, -1232(%rbp)
	movq	%r8, %rbx
	movl	%eax, -1184(%rbp)
	leaq	8(,%rax,8), %rax
	movl	%r12d, -1208(%rbp)
	movq	%rax, -1192(%rbp)
	.p2align 4,,10
	.p2align 3
.L96:
	movq	704(%r15), %rax
	movq	(%rax,%rbx), %r12
	cmpq	%r12, %r15
	je	.L85
	movq	928(%r12), %rax
	testq	%rax, %rax
	je	.L86
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L85
.L86:
	cmpq	$0, 488(%r12)
	jne	.L87
	cmpq	$0, 472(%r12)
	jne	.L87
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_dl_build_local_scope
	cmpl	-1208(%rbp), %eax
	movq	%rax, %r13
	movl	%eax, %edi
	ja	.L89
	testl	%eax, %eax
	je	.L95
	movq	(%r14), %rax
	xorl	%edx, %edx
	andb	$-97, 796(%rax)
	leaq	8(%r14), %rax
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rax), %rcx
	andb	$-97, 796(%rcx)
	movq	(%rax), %rcx
	cmpq	$0, 192(%rcx)
	jne	.L295
.L93:
	addq	$8, %rax
.L126:
	addl	$1, %edx
	cmpl	%edx, %edi
	jne	.L94
.L95:
	movl	%r13d, %eax
	leaq	16(,%rax,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -1200(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 928(%r12)
	movq	-1200(%rbp), %rdx
	je	.L91
	leaq	16(%rax), %rdi
	movl	%r13d, 8(%rax)
	subq	$16, %rdx
	movq	%r14, %rsi
	movq	%rdi, (%rax)
	call	memcpy@PLT
.L85:
	addq	$8, %rbx
	cmpq	%rbx, -1192(%rbp)
	jne	.L96
	movq	-1216(%rbp), %rax
	movl	-1224(%rbp), %ebx
	movl	-1232(%rbp), %ecx
	movl	-1208(%rbp), %r12d
	movq	984(%rax), %r8
	testq	%r8, %r8
	je	.L98
	movl	(%r8), %r10d
	leaq	8(%r8), %r9
	jmp	.L127
.L58:
	movq	-1256(%rbp), %rbx
	movq	%rcx, 16(%rbx)
	movq	-1192(%rbp), %rbx
	movq	16(%rsi), %rdx
	cmpq	%rbx, %rdx
	movq	16(%rdx), %rdx
	cmove	%rsi, %rbx
	movq	%rbx, -1192(%rbp)
	movq	%rdx, 16(%rsi)
	movq	32(%rax), %rsi
	movq	24(%rax), %rdx
	testq	%rsi, %rsi
	jne	.L267
	jmp	.L65
.L284:
	xorl	%r8d, %r8d
	jmp	.L125
.L139:
	movq	%rcx, -1256(%rbp)
	movq	%rcx, -1192(%rbp)
	jmp	.L37
.L143:
	movl	%r13d, %esi
	jmp	.L104
.L289:
	movq	%rax, -1248(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L295:
	orb	$-128, 797(%r12)
	jmp	.L93
.L292:
	cmpq	$2147483645, (%r12)
	jne	.L262
	movq	-1200(%rbp), %rdi
	call	_dl_exception_free@PLT
	jmp	.L37
.L291:
	movq	8(%r13), %rdx
	cmpb	$0, (%rdx)
	jne	.L51
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rdx
	leaq	.LC0(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L51:
	leaq	.LC7(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L50
.L270:
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	_dl_signal_error
.L288:
	movq	-1216(%rbp), %rax
	leaq	.LC1(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	movq	8(%rax), %rsi
	call	_dl_signal_error
.L275:
	leaq	__PRETTY_FUNCTION__.10217(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$244, %edx
	call	__assert_fail
.L91:
	movq	-1216(%rbp), %rax
	leaq	.LC10(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	movq	8(%rax), %rsi
	call	_dl_signal_error
.L287:
	cmpl	$-1, %ebx
	jne	.L121
	xorl	%ebx, %ebx
.L121:
	leaq	-1168(%rbp), %rsi
	xorl	%edx, %edx
	movl	%ebx, %edi
	call	_dl_signal_exception
.L276:
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_dl_signal_error
.L274:
	movq	-1272(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L72
	call	free@PLT
.L72:
	movq	-1216(%rbp), %rax
	leaq	.LC8(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	movq	8(%rax), %rsi
	call	_dl_signal_error
.L278:
	leaq	.LC6(%rip), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_dl_signal_error
.L294:
	leaq	__PRETTY_FUNCTION__.10217(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$288, %edx
	call	__assert_fail
.L89:
	leaq	__PRETTY_FUNCTION__.10217(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$533, %edx
	call	__assert_fail
.L87:
	movq	8(%r12), %rsi
	leaq	.LC11(%rip), %rcx
	xorl	%edx, %edx
	movl	$22, %edi
	call	_dl_signal_error
	.size	_dl_map_object_deps, .-_dl_map_object_deps
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10217, @object
	.size	__PRETTY_FUNCTION__.10217, 20
__PRETTY_FUNCTION__.10217:
	.string	"_dl_map_object_deps"
	.hidden	_dl_signal_exception
	.hidden	_dl_signal_error
	.hidden	__assert_fail
	.hidden	__libc_scratch_buffer_set_array_size
	.hidden	_dl_scope_free
	.hidden	_dl_sort_maps
	.hidden	_dl_get_origin
	.hidden	_dl_debug_printf
	.hidden	_dl_dst_count
	.hidden	_dl_catch_exception
	.hidden	_dl_dst_substitute
	.hidden	strlen
	.hidden	_dl_map_object
