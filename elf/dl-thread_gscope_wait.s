	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__thread_gscope_wait
	.hidden	__thread_gscope_wait
	.type	__thread_gscope_wait, @function
__thread_gscope_wait:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
#APP
# 28 "../sysdeps/nptl/dl-thread_gscope_wait.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	_dl_stack_used(%rip), %r8
	leaq	_dl_stack_used(%rip), %r12
	movq	%fs:16, %rbp
	cmpq	%r12, %r8
	je	.L4
	movl	$1, %r15d
	movl	$2, %r14d
	movl	$202, %ebx
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	-704(%r8), %r9
	cmpq	%r9, %rbp
	je	.L5
	movl	-676(%r8), %edx
	testl	%edx, %edx
	je	.L5
	addq	$28, %r9
	movl	%r15d, %eax
	lock cmpxchgl	%r14d, (%r9)
	je	.L9
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$2, -676(%r8)
	jne	.L5
.L9:
	xorl	%r10d, %r10d
	movl	$2, %edx
	movl	$128, %esi
	movq	%r9, %rdi
	movl	%ebx, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L6
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L8
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L6
.L8:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%r8), %r8
	cmpq	%r12, %r8
	jne	.L10
.L4:
	movq	_dl_stack_user(%rip), %r8
	leaq	_dl_stack_user(%rip), %r12
	cmpq	%r12, %r8
	je	.L11
	movl	$1, %r15d
	movl	$2, %r14d
	movl	$202, %ebx
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	-704(%r8), %r9
	cmpq	%r9, %rbp
	je	.L12
	movl	-676(%r8), %eax
	testl	%eax, %eax
	je	.L12
	addq	$28, %r9
	movl	%r15d, %eax
	lock cmpxchgl	%r14d, (%r9)
	je	.L14
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	$2, -676(%r8)
	jne	.L12
.L14:
	xorl	%r10d, %r10d
	movl	$2, %edx
	movl	$128, %esi
	movq	%r9, %rdi
	movl	%ebx, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L13
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L8
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	je	.L8
	cmpl	$2, -676(%r8)
	je	.L14
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%r8), %r8
	cmpq	%r12, %r8
	jne	.L15
.L11:
#APP
# 79 "../sysdeps/nptl/dl-thread_gscope_wait.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L16
	subl	$1, _dl_stack_cache_lock(%rip)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	je	.L3
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
.L16:
	xorl	%eax, %eax
#APP
# 79 "../sysdeps/nptl/dl-thread_gscope_wait.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 79 "../sysdeps/nptl/dl-thread_gscope_wait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	__thread_gscope_wait, .-__thread_gscope_wait
	.hidden	__lll_lock_wait_private
	.hidden	__libc_fatal
