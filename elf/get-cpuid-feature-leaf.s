	.text
	.p2align 4,,15
	.globl	__x86_get_cpuid_feature_leaf
	.type	__x86_get_cpuid_feature_leaf, @function
__x86_get_cpuid_feature_leaf:
	cmpl	$7, %edi
	leaq	feature.9014(%rip), %rax
	ja	.L1
	movl	%edi, %edi
	leaq	_dl_x86_cpu_features(%rip), %rax
	salq	$5, %rdi
	leaq	20(%rax,%rdi), %rax
.L1:
	rep ret
	.size	__x86_get_cpuid_feature_leaf, .-__x86_get_cpuid_feature_leaf
	.section	.rodata
	.align 32
	.type	feature.9014, @object
	.size	feature.9014, 32
feature.9014:
	.zero	32
