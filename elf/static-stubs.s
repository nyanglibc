	.text
	.p2align 4,,15
	.globl	__libc_message
	.type	__libc_message, @function
__libc_message:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	16(%rbp), %rax
	pushq	%rbx
	subq	$104, %rsp
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%r8, -64(%rbp)
	movq	%r9, -56(%rbp)
	movzbl	(%rsi), %r13d
	movq	%rax, -112(%rbp)
	leaq	-96(%rbp), %rax
	movl	%edi, -132(%rbp)
	movl	$16, -120(%rbp)
	movq	%rax, -104(%rbp)
	testb	%r13b, %r13b
	je	.L45
	movq	%rsi, %r12
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%r13d, %edx
	movq	%r12, %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	1(%rax), %rdi
	movl	$37, %esi
	call	__strchrnul@PLT
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L4
.L11:
	cmpb	$37, %dl
	jne	.L5
	cmpb	$115, 1(%rax)
	jne	.L5
.L4:
	cmpb	$37, %r13b
	je	.L46
.L6:
	movq	%rax, %rsi
	movq	%r12, %rcx
	subq	%r12, %rsi
	movq	%rax, %r12
.L9:
	subq	$48, %rsp
	leal	1(%rbx), %r15d
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rcx, (%rdx)
	movq	%rsi, 8(%rdx)
	movq	%r14, 16(%rdx)
	movzbl	(%r12), %r13d
	testb	%r13b, %r13b
	je	.L10
	movl	%r15d, %ebx
	movq	%rdx, %r14
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L46:
	cmpb	$115, 1(%r12)
	jne	.L6
	movl	-120(%rbp), %edx
	cmpl	$47, %edx
	ja	.L7
	movl	%edx, %eax
	addq	-104(%rbp), %rax
	addl	$8, %edx
	movl	%edx, -120(%rbp)
.L8:
	movq	(%rax), %rcx
	addq	$2, %r12
	movq	%rcx, %rdi
	movq	%rcx, -144(%rbp)
	call	strlen@PLT
	movq	-144(%rbp), %rcx
	movq	%rax, %rsi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movslq	%r15d, %rax
	movslq	%ebx, %rdx
	salq	$4, %rax
	salq	$4, %rdx
	addq	$16, %rax
	subq	%rax, %rsp
	xorl	%eax, %eax
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	addq	%r12, %rdx
	movq	%r12, %r13
	leaq	-16(%r12), %rdi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%r14), %rcx
	movq	8(%r14), %rsi
	movq	16(%r14), %r14
.L13:
	movq	%rcx, (%rdx)
	movq	%rsi, 8(%rdx)
	leaq	(%rax,%rsi), %rbx
	subq	$16, %rdx
	cmpq	%rdi, %rdx
	movq	%rbx, %rax
	jne	.L47
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L48:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L14
.L12:
	movl	%r15d, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	call	__writev@PLT
	cmpq	$-1, %rax
	je	.L48
.L14:
	testb	$1, -132(%rbp)
	jne	.L49
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L49:
	movq	_dl_pagesize(%rip), %rax
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$34, %ecx
	movl	$3, %edx
	addq	%rax, %rbx
	negq	%rax
	andq	%rax, %rbx
	movq	%rbx, %rsi
	call	__mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, %r14
	je	.L19
	leal	-1(%r15), %edx
	movl	%ebx, (%rax)
	leaq	4(%rax), %rax
	salq	$4, %rdx
	leaq	16(%r12,%rdx), %rbx
	.p2align 4,,10
	.p2align 3
.L17:
	movq	8(%r13), %rdx
	movq	0(%r13), %rsi
	movq	%rax, %rdi
	addq	$16, %r13
	call	__mempcpy@PLT
	cmpq	%rbx, %r13
	jne	.L17
	movb	$0, (%rax)
#APP
# 143 "../sysdeps/posix/libc_fatal.c" 1
	xchgq %r14, __abort_msg(%rip)
# 0 "" 2
#NO_APP
	testq	%r14, %r14
	je	.L19
	movl	(%r14), %esi
	movq	%r14, %rdi
	call	__munmap@PLT
.L19:
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L45:
	testb	$1, -132(%rbp)
	jne	.L19
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L7:
	movq	-112(%rbp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, -112(%rbp)
	jmp	.L8
	.size	__libc_message, .-__libc_message
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s"
	.text
	.p2align 4,,15
	.globl	__libc_fatal
	.type	__libc_fatal, @function
__libc_fatal:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__libc_message
	jmp	.L51
	.size	__libc_fatal, .-__libc_fatal
	.p2align 4,,15
	.globl	_Unwind_Resume
	.type	_Unwind_Resume, @function
_Unwind_Resume:
	subq	$8, %rsp
	call	abort@PLT
	.size	_Unwind_Resume, .-_Unwind_Resume
	.p2align 4,,15
	.globl	__gcc_personality_v0
	.type	__gcc_personality_v0, @function
__gcc_personality_v0:
	subq	$8, %rsp
	call	abort@PLT
	.size	__gcc_personality_v0, .-__gcc_personality_v0
