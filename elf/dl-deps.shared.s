	.text
	.p2align 4,,15
	.type	_dl_build_local_scope, @function
_dl_build_local_scope:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, (%rdi)
	leaq	8(%rdi), %rbp
	movzbl	796(%rsi), %eax
	movq	976(%rsi), %rbx
	andl	$-97, %eax
	orl	$32, %eax
	testq	%rbx, %rbx
	movb	%al, 796(%rsi)
	je	.L2
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L2
	addq	$8, %rbx
.L4:
	testb	$96, 796(%rsi)
	je	.L14
.L3:
	addq	$8, %rbx
	movq	(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L4
.L2:
	movq	%rbp, %rax
	subq	%r12, %rax
	popq	%rbx
	sarq	$3, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rbp, %rdi
	call	_dl_build_local_scope
	leaq	0(%rbp,%rax,8), %rbp
	jmp	.L3
	.size	_dl_build_local_scope, .-_dl_build_local_scope
	.p2align 4,,15
	.type	openaux, @function
openaux:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	movl	8(%rbx), %ecx
	movq	24(%rbx), %rsi
	movl	12(%rbx), %r8d
	movzbl	796(%rdi), %eax
	movq	48(%rdi), %r9
	andl	$3, %eax
	testb	%al, %al
	movzbl	%al, %edx
	movl	$1, %eax
	cmove	%eax, %edx
	call	_dl_map_object
	movq	%rax, 32(%rbx)
	popq	%rbx
	ret
	.size	openaux, .-openaux
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot allocate dependency buffer"
	.align 8
.LC2:
	.string	"DST not allowed in SUID/SGID programs"
	.section	.rodata.str1.1
.LC3:
	.string	"dl-deps.c"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"(l)->l_name[0] == '\\0' || IS_RTLD (l)"
	.align 8
.LC5:
	.string	"cannot load auxiliary `%s' because of empty dynamic string token substitution\n"
	.align 8
.LC6:
	.string	"empty dynamic string token substitution"
	.align 8
.LC7:
	.string	"load auxiliary object=%s requested by file=%s\n"
	.align 8
.LC8:
	.string	"cannot allocate dependency list"
	.align 8
.LC9:
	.string	"map->l_searchlist.r_list == NULL"
	.align 8
.LC10:
	.string	"cannot allocate symbol search list"
	.align 8
.LC11:
	.string	"Filters not supported with LD_TRACE_PRELINKING"
	.section	.rodata.str1.1
.LC12:
	.string	"cnt <= nlist"
.LC13:
	.string	"map_index < nlist"
	.text
	.p2align 4,,15
	.globl	_dl_map_object_deps
	.hidden	_dl_map_object_deps
	.type	_dl_map_object_deps, @function
_dl_map_object_deps:
	leal	2(%rdx), %eax
	pushq	%rbp
	leaq	(%rax,%rax,2), %rax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r15
	leaq	30(,%rax,8), %rax
	pushq	%r12
	pushq	%rbx
	shrq	$4, %rax
	subq	$1240, %rsp
	movl	%r8d, -1260(%rbp)
	salq	$4, %rax
	movl	%ecx, -1232(%rbp)
	subq	%rax, %rsp
	movzbl	796(%rdi), %eax
	leaq	15(%rsp), %r8
	andq	$-16, %r8
	andl	$-97, %eax
	leaq	24(%r8), %rcx
	movq	%r8, -1280(%rbp)
	orl	$32, %eax
	testl	%edx, %edx
	movl	$0, (%r8)
	movq	%rdi, 8(%r8)
	movq	%rcx, 16(%r8)
	movb	%al, 796(%rdi)
	je	.L129
	addl	$1, %edx
	movl	$1, %r9d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L130:
	movl	%r10d, %r9d
.L21:
	leal	1(%r9), %eax
	movq	(%rsi), %rdi
	movl	$0, (%rcx)
	addq	$8, %rsi
	addq	$24, %rcx
	movq	%rax, %r10
	leaq	(%rax,%rax,2), %rax
	movq	%rdi, -16(%rcx)
	leaq	(%r8,%rax,8), %rax
	movq	%rax, -8(%rcx)
	movzbl	796(%rdi), %eax
	andl	$-97, %eax
	orl	$32, %eax
	cmpl	%edx, %r10d
	movb	%al, 796(%rdi)
	jne	.L130
	leaq	(%r9,%r9,2), %rax
	movq	8(%r8), %r13
	movl	%r10d, -1228(%rbp)
	leaq	(%r8,%rax,8), %rax
	movq	%rax, -1192(%rbp)
.L20:
	movq	-1192(%rbp), %rax
	movq	$1024, -1080(%rbp)
	movq	%r8, -1240(%rbp)
	movq	%r15, -1256(%rbp)
	movq	$0, 16(%rax)
	leaq	-1088(%rbp), %rax
	movq	%rax, -1272(%rbp)
	addq	$16, %rax
	movq	%rax, -1088(%rbp)
	movl	rtld_errno(%rip), %eax
	movl	$0, rtld_errno(%rip)
	movl	%eax, -1264(%rbp)
	leaq	-1136(%rbp), %rax
	movq	%rax, -1208(%rbp)
.L22:
	movq	704(%r13), %r15
	movq	-1240(%rbp), %rax
	testq	%r15, %r15
	movl	$1, (%rax)
	je	.L256
	xorl	%r15d, %r15d
.L23:
	cmpq	$0, 72(%r13)
	je	.L257
.L25:
	movq	104(%r13), %rax
	movq	16(%r13), %r12
	movq	%r13, -1136(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -1184(%rbp)
	movq	%rax, -1120(%rbp)
	movl	-1232(%rbp), %eax
	movl	%eax, -1128(%rbp)
	movl	-1260(%rbp), %eax
	movl	%eax, -1124(%rbp)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L132
	movq	-1240(%rbp), %rbx
	movl	$0, -1176(%rbp)
	movq	%rbx, -1248(%rbp)
	leaq	-1168(%rbp), %rbx
	movq	%rbx, -1200(%rbp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L261:
	movl	__GI___libc_enable_secure(%rip), %r10d
	testl	%r10d, %r10d
	jne	.L258
	movq	%r14, %rdi
	call	strlen
	movq	848(%r13), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	testq	%rdi, %rdi
	je	.L259
	cmpq	$-1, %rdi
	je	.L35
	movq	%rax, -1224(%rbp)
	movq	%rax, -1216(%rbp)
	call	strlen
	movq	-1224(%rbp), %rcx
	movq	-1216(%rbp), %rdx
.L34:
	cmpq	%rax, 16+_rtld_local_ro(%rip)
	cmovnb	16+_rtld_local_ro(%rip), %rax
	cmpq	$4, %rax
	jbe	.L36
	subq	$4, %rax
	imulq	%rax, %rbx
	leaq	(%rcx,%rbx), %rdx
.L36:
	leaq	30(%rdx), %rbx
	movq	%r14, %rsi
	movq	%r13, %rdi
	andq	$-16, %rbx
	subq	%rbx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	call	_dl_dst_substitute
	cmpb	$0, (%rax)
	je	.L260
.L29:
	movq	-1208(%rbp), %rdx
	movq	-1200(%rbp), %rdi
	leaq	openaux(%rip), %rsi
	movq	%rax, -1112(%rbp)
	call	_dl_catch_exception@PLT
	cmpq	$0, -1160(%rbp)
	jne	.L254
	movq	-1104(%rbp), %rax
	testb	$96, 796(%rax)
	jne	.L40
	subq	$48, %rsp
	movq	-1192(%rbp), %rbx
	addl	$1, -1228(%rbp)
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rax, 8(%rdx)
	movl	$0, (%rdx)
	movq	$0, 16(%rdx)
	movq	%rdx, 16(%rbx)
	movzbl	796(%rax), %ecx
	movq	%rdx, -1192(%rbp)
	andl	$-97, %ecx
	orl	$32, %ecx
	movb	%cl, 796(%rax)
.L40:
	testq	%r15, %r15
	je	.L37
	movl	-1176(%rbp), %edx
	movq	%rax, (%r15,%rdx,8)
	leal	1(%rdx), %eax
	movl	%eax, -1176(%rbp)
.L37:
	addq	$16, %r12
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L26
.L68:
	cmpq	$1, %rax
	jne	.L28
	movq	-1184(%rbp), %r14
	addq	8(%r12), %r14
	movq	%r14, %rdi
	call	_dl_dst_count
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L261
	movq	%r14, %rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L260:
	testb	$1, _rtld_local_ro(%rip)
	je	.L37
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	addq	$16, %r12
	movq	%r14, %rsi
	call	_dl_debug_printf
	movq	(%r12), %rax
	testq	%rax, %rax
	jne	.L68
	.p2align 4,,10
	.p2align 3
.L26:
	testq	%r15, %r15
	je	.L70
	movl	-1176(%rbp), %eax
	leal	1(%rax), %ebx
	movq	$0, (%r15,%rax,8)
	leal	1(%rbx,%rbx), %edi
	salq	$3, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L262
	salq	$3, %rbx
	leaq	8(%rax), %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%r13, (%rax)
	call	memcpy@PLT
	movl	-1176(%rbp), %eax
	movq	%rbx, %rdx
	movq	%r12, %rsi
	addl	$2, %eax
	leaq	(%r12,%rax,8), %rdi
	call	memcpy@PLT
	orb	$1, 798(%r13)
	movq	%r12, 976(%r13)
.L70:
	movq	-1240(%rbp), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L73
.L74:
	movq	-1240(%rbp), %rax
	movq	8(%rax), %r13
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L259:
	movq	8(%r13), %rax
	cmpb	$0, (%rax)
	je	.L32
	leaq	2568+_rtld_local(%rip), %rax
	cmpq	%rax, %r13
	jne	.L263
.L32:
	movq	%rcx, -1224(%rbp)
	movq	%rdx, -1216(%rbp)
	call	_dl_get_origin
	leaq	-1(%rax), %rsi
	movq	%rax, 848(%r13)
	movq	-1216(%rbp), %rdx
	movq	-1224(%rbp), %rcx
	cmpq	$-3, %rsi
	ja	.L35
	movq	%rax, %rdi
	movq	%rcx, -1224(%rbp)
	movq	%rdx, -1216(%rbp)
	call	strlen
	movq	-1216(%rbp), %rdx
	movq	-1224(%rbp), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L28:
	andq	$-3, %rax
	cmpq	$2147483645, %rax
	jne	.L37
	movq	-1184(%rbp), %rbx
	addq	8(%r12), %rbx
	movq	%rbx, %rdi
	call	_dl_dst_count
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L41
	movl	__GI___libc_enable_secure(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L264
	movq	%rbx, %rdi
	call	strlen
	movq	848(%r13), %rdi
	movq	%rax, %rcx
	movq	%rax, %rdx
	testq	%rdi, %rdi
	je	.L265
	cmpq	$-1, %rdi
	je	.L47
	movq	%rax, -1224(%rbp)
	movq	%rax, -1216(%rbp)
	call	strlen
	movq	-1224(%rbp), %rcx
	movq	-1216(%rbp), %rdx
.L46:
	cmpq	%rax, 16+_rtld_local_ro(%rip)
	cmovnb	16+_rtld_local_ro(%rip), %rax
	cmpq	$4, %rax
	jbe	.L48
	subq	$4, %rax
	imulq	%r14, %rax
	leaq	(%rcx,%rax), %rdx
.L48:
	leaq	30(%rdx), %rax
	movq	%rbx, %rsi
	movq	%r13, %rdi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	call	_dl_dst_substitute
	cmpb	$0, (%rax)
	jne	.L135
	cmpq	$2147483645, (%r12)
	je	.L266
	testb	$1, _rtld_local_ro(%rip)
	je	.L37
	leaq	.LC5(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L254:
	testl	%eax, %eax
	movl	%eax, %ebx
	movq	-1256(%rbp), %r15
	je	.L267
.L69:
	movq	-1272(%rbp), %rax
	movq	-1088(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L75
	call	*__rtld_free(%rip)
.L75:
	movl	rtld_errno(%rip), %edi
	testl	%edi, %edi
	jne	.L76
	movl	-1264(%rbp), %eax
	testl	%eax, %eax
	jne	.L268
.L76:
	movq	976(%r15), %rax
	testq	%rax, %rax
	movq	%rax, -1192(%rbp)
	jne	.L269
.L77:
	movl	-1228(%rbp), %eax
	leal	1(%rax,%rax), %edi
	salq	$3, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L91
	movl	-1228(%rbp), %ecx
	movq	-1280(%rbp), %rdx
	xorl	%r12d, %r12d
	movl	-1232(%rbp), %r8d
	leal	1(%rcx), %eax
	movl	%ecx, 712(%r15)
	movl	$-1, %ecx
	leaq	(%r14,%rax,8), %rsi
	movq	%rsi, 704(%r15)
	.p2align 4,,10
	.p2align 3
.L82:
	testl	%r8d, %r8d
	movq	8(%rdx), %rax
	jne	.L270
.L79:
	movl	%r12d, %edi
	cmpq	%rax, %r15
	movq	%rax, (%rsi,%rdi,8)
	movq	8(%rdx), %rax
	cmove	%r12d, %ecx
	addl	$1, %r12d
.L80:
	andb	$-97, 796(%rax)
	movq	16(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L82
	testb	$8, 1+_rtld_local_ro(%rip)
	movl	%ecx, -1184(%rbp)
	jne	.L271
.L83:
	movq	984(%r15), %rsi
	testq	%rsi, %rsi
	je	.L98
	testl	%r12d, %r12d
	je	.L99
	leal	-1(%r12), %eax
	movl	%eax, -1176(%rbp)
.L128:
	movq	704(%r15), %rax
	movl	-1176(%rbp), %r13d
	leaq	8(%rax), %rdx
	leaq	(%rdx,%r13,8), %r8
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L272:
	addq	$8, %rdx
.L100:
	movq	(%rax), %rcx
	movzbl	796(%rcx), %eax
	andl	$-97, %eax
	orl	$32, %eax
	cmpq	%rdx, %r8
	movb	%al, 796(%rcx)
	movq	%rdx, %rax
	jne	.L272
.L99:
	andb	$-97, 796(%r15)
	leaq	8(%rsi), %rax
	xorl	%r8d, %r8d
	movl	(%rsi), %ecx
	movq	%rax, -1176(%rbp)
	testl	%ecx, %ecx
	je	.L101
	leaq	16(%rsi), %rax
	xorl	%r13d, %r13d
	movq	%r14, -1216(%rbp)
	movl	%r12d, -1224(%rbp)
	movl	%ebx, -1208(%rbp)
	movq	%r8, %r14
	movq	%rax, -1200(%rbp)
	movl	%r13d, %r12d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L255:
	movq	984(%r15), %rdi
	movl	%ebx, %r12d
	cmpl	(%rdi), %r12d
	jnb	.L273
.L108:
	movq	-1176(%rbp), %rsi
	movl	%r12d, %eax
	leal	1(%r12), %ebx
	leaq	0(,%rax,8), %r13
	movq	(%rsi,%rax,8), %rax
	testb	$96, 796(%rax)
	je	.L255
	movl	992(%r15), %eax
	leaq	8(,%rax,8), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L255
	movq	-1176(%rbp), %rsi
	leaq	8(%rax), %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	984(%r15), %rdi
	movl	(%rdi), %r13d
	cmpl	%ebx, %r13d
	jbe	.L143
	movq	-1176(%rbp), %rax
	leal	-2(%r13), %edx
	movl	%ebx, %esi
	subl	%r12d, %edx
	addq	%rsi, %rdx
	leaq	(%rax,%rsi,8), %rax
	movq	-1200(%rbp), %rsi
	leaq	(%rsi,%rdx,8), %r10
	movl	%r12d, %esi
	.p2align 4,,10
	.p2align 3
.L107:
	movq	(%rax), %rdx
	testb	$96, 796(%rdx)
	jne	.L106
	movl	%esi, %ecx
	addl	$1, %esi
	movq	%rdx, 8(%r14,%rcx,8)
.L106:
	addq	$8, %rax
	cmpq	%rax, %r10
	jne	.L107
	leal	-1(%rbx,%r13), %r9d
	movl	%r9d, %ebx
	subl	%r12d, %ebx
.L105:
	movl	%esi, (%r14)
	leal	1(%rbx), %r12d
	cmpl	(%rdi), %r12d
	jb	.L108
.L273:
	movq	%r14, %r8
	movl	-1208(%rbp), %ebx
	movq	-1216(%rbp), %r14
	movl	-1224(%rbp), %r12d
.L101:
	testl	%r12d, %r12d
	je	.L112
	movq	704(%r15), %rdx
	leal	-1(%r12), %esi
	leaq	8(%rdx), %rax
	leaq	(%rax,%rsi,8), %rsi
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L274:
	addq	$8, %rax
.L110:
	movq	(%rdx), %rdx
	andb	$-97, 796(%rdx)
	cmpq	%rax, %rsi
	movq	%rax, %rdx
	jne	.L274
.L111:
	movl	-1184(%rbp), %ecx
	cmpl	%r12d, %ecx
	jnb	.L112
	movl	%r12d, %r13d
	testl	%ecx, %ecx
	movq	704(%r15), %rsi
	leaq	0(,%r13,8), %rdx
	je	.L113
	movl	%ecx, %eax
	movq	(%rsi,%rax,8), %rax
	movq	%rax, (%r14)
	leal	-1(%rcx), %eax
	leaq	8(,%rax,8), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L114:
	movq	(%rsi,%rax), %rdx
	movq	%rdx, 8(%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L114
	movl	-1184(%rbp), %r11d
	leal	1(%r11), %eax
	cmpl	%r12d, %eax
	jnb	.L116
	leal	-2(%r12), %edi
	movl	%eax, %edx
	leaq	0(,%rdx,8), %rax
	movl	%edi, %ecx
	subl	%r11d, %ecx
	leaq	1(%rdx,%rcx), %rcx
	salq	$3, %rcx
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rsi,%rax), %rdx
	movq	%rdx, (%r14,%rax)
	addq	$8, %rax
	cmpq	%rax, %rcx
	jne	.L117
.L116:
	movq	(%r14), %rdx
	movq	%r8, -1176(%rbp)
	movq	48(%rdx), %rax
	leaq	(%rax,%rax,8), %rcx
	leaq	(%rax,%rcx,2), %rcx
	leaq	_rtld_local(%rip), %rax
	cmpq	32(%rax,%rcx,8), %rdx
	je	.L275
	leal	-1(%r12), %esi
	leaq	8(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	_dl_sort_maps
	movq	-1176(%rbp), %r8
.L120:
	movq	$0, (%r14,%r13,8)
	orb	$1, 798(%r15)
	testq	%r8, %r8
	movq	%r14, 976(%r15)
	je	.L121
	movq	984(%r15), %rdi
	movq	%r8, 984(%r15)
	call	_dl_scope_free
.L121:
	movq	-1192(%rbp), %rax
	testq	%rax, %rax
	je	.L122
	movq	%rax, %rdi
	call	_dl_scope_free
.L122:
	testl	%ebx, %ebx
	jne	.L276
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	cmpq	$0, 488(%r13)
	jne	.L25
	cmpq	$0, 472(%r13)
	jne	.L25
.L132:
	movl	$0, -1176(%rbp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L270:
	testb	$2, 797(%rax)
	je	.L79
	subl	$1, 712(%r15)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$-1, %ebx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L268:
	movl	%eax, rtld_errno(%rip)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L256:
	cmpq	$0, 976(%r13)
	jne	.L23
	cmpq	%r13, -1256(%rbp)
	je	.L23
	movzwl	698(%r13), %eax
	testw	%ax, %ax
	je	.L23
	movq	-1272(%rbp), %rdi
	movzwl	%ax, %esi
	movl	$8, %edx
	call	__libc_scratch_buffer_set_array_size@PLT
	testb	%al, %al
	je	.L277
	movq	-1088(%rbp), %r15
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L279:
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	je	.L278
.L73:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L279
	movq	-1256(%rbp), %r15
	xorl	%ebx, %ebx
	jmp	.L69
.L135:
	movq	%rax, %rbx
.L41:
	testb	$1, _rtld_local_ro(%rip)
	movq	%rbx, -1112(%rbp)
	jne	.L280
.L50:
	movq	-1208(%rbp), %rdx
	movq	-1200(%rbp), %rdi
	leaq	openaux(%rip), %rsi
	call	_dl_catch_exception@PLT
	cmpq	$0, -1160(%rbp)
	jne	.L281
	movq	-1248(%rbp), %rbx
	subq	$48, %rsp
	leaq	15(%rsp), %rcx
	movdqu	(%rbx), %xmm0
	andq	$-16, %rcx
	testq	%r15, %r15
	movaps	%xmm0, (%rcx)
	movq	16(%rbx), %rax
	movq	%rax, 16(%rcx)
	movq	-1104(%rbp), %rax
	movl	$0, (%rbx)
	movq	%rax, 8(%rbx)
	je	.L55
	movl	-1176(%rbp), %edx
	movq	%rdx, %rbx
	movq	%rax, (%r15,%rdx,8)
	movq	-1104(%rbp), %rax
	addl	$1, %ebx
	movl	%ebx, -1176(%rbp)
.L55:
	testb	$96, 796(%rax)
	jne	.L282
	movq	-1248(%rbp), %rbx
	movq	32(%rax), %rsi
	addl	$1, -1228(%rbp)
	movq	%rcx, 16(%rbx)
	movzbl	796(%rax), %edx
	andl	$-97, %edx
	orl	$32, %edx
	testq	%rsi, %rsi
	movb	%dl, 796(%rax)
	movq	24(%rax), %rdx
	je	.L65
	movq	%rdx, 24(%rsi)
	movq	24(%rax), %rdx
.L65:
	testq	%rdx, %rdx
	je	.L66
	movq	%rsi, 32(%rdx)
.L66:
	movq	8(%rcx), %rdx
	movq	32(%rdx), %rsi
	movq	%rsi, 32(%rax)
	movq	%rax, 32(%rdx)
	movq	32(%rax), %rsi
	testq	%rsi, %rsi
	je	.L67
	movq	%rax, 24(%rsi)
.L67:
	movq	%rdx, 24(%rax)
.L64:
	movq	-1248(%rbp), %rbx
	cmpq	%rbx, -1192(%rbp)
	je	.L138
	movq	%rcx, -1248(%rbp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L113:
	movq	%r14, %rdi
	movq	%r8, -1176(%rbp)
	call	memcpy@PLT
	movq	-1176(%rbp), %r8
	jmp	.L116
.L271:
	cmpq	%r15, _rtld_local(%rip)
	jne	.L83
	testl	%r12d, %r12d
	je	.L84
	leal	-1(%r12), %eax
	xorl	%r8d, %r8d
	movl	%ebx, -1224(%rbp)
	movl	%r12d, -1216(%rbp)
	movq	%r8, %rbx
	movl	%eax, -1176(%rbp)
	leaq	8(,%rax,8), %rax
	movq	%rax, -1200(%rbp)
	.p2align 4,,10
	.p2align 3
.L96:
	movq	704(%r15), %rax
	movq	(%rax,%rbx), %r12
	cmpq	%r12, %r15
	je	.L85
	movq	928(%r12), %rax
	testq	%rax, %rax
	je	.L86
	movl	8(%rax), %esi
	testl	%esi, %esi
	jne	.L85
.L86:
	cmpq	$0, 488(%r12)
	jne	.L87
	cmpq	$0, 472(%r12)
	jne	.L87
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	_dl_build_local_scope
	cmpl	-1216(%rbp), %eax
	movq	%rax, %r13
	movl	%eax, %edi
	ja	.L89
	testl	%eax, %eax
	je	.L95
	movq	(%r14), %rax
	xorl	%edx, %edx
	andb	$-97, 796(%rax)
	leaq	8(%r14), %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rax), %rcx
	andb	$-97, 796(%rcx)
	movq	(%rax), %rcx
	cmpq	$0, 192(%rcx)
	jne	.L283
.L93:
	addq	$8, %rax
.L127:
	addl	$1, %edx
	cmpl	%edx, %edi
	jne	.L94
.L95:
	movl	%r13d, %eax
	leaq	16(,%rax,8), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -1208(%rbp)
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, 928(%r12)
	movq	-1208(%rbp), %rdx
	je	.L91
	leaq	16(%rax), %rdi
	movl	%r13d, 8(%rax)
	subq	$16, %rdx
	movq	%r14, %rsi
	movq	%rdi, (%rax)
	call	memcpy@PLT
.L85:
	addq	$8, %rbx
	cmpq	%rbx, -1200(%rbp)
	jne	.L96
	movq	984(%r15), %rsi
	movl	-1224(%rbp), %ebx
	movl	-1216(%rbp), %r12d
	testq	%rsi, %rsi
	jne	.L128
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%r8d, %r8d
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	_dl_sort_maps
	movq	-1176(%rbp), %r8
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L269:
	movzbl	796(%r15), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L284
	movq	$0, -1192(%rbp)
	jmp	.L77
.L282:
	movq	%rcx, %rdi
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L59:
	movq	8(%rdx), %rsi
	cmpq	%rax, %rsi
	je	.L58
	movq	%rdx, %rdi
.L56:
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L59
	movq	-1248(%rbp), %rbx
	movdqa	(%rcx), %xmm0
	movups	%xmm0, (%rbx)
	movq	16(%rcx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L37
.L284:
	cmpq	$0, 704(%r15)
	je	.L77
	leaq	__PRETTY_FUNCTION__.10023(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$472, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L265:
	movq	8(%r13), %rax
	cmpb	$0, (%rax)
	je	.L44
	leaq	2568+_rtld_local(%rip), %rax
	cmpq	%rax, %r13
	je	.L44
	leaq	__PRETTY_FUNCTION__.10023(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$288, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rcx, -1224(%rbp)
	movq	%rdx, -1216(%rbp)
	call	_dl_get_origin
	leaq	-1(%rax), %rsi
	movq	%rax, 848(%r13)
	movq	-1216(%rbp), %rdx
	movq	-1224(%rbp), %rcx
	cmpq	$-3, %rsi
	ja	.L47
	movq	%rax, %rdi
	movq	%rcx, -1224(%rbp)
	movq	%rdx, -1216(%rbp)
	call	strlen
	movq	-1216(%rbp), %rdx
	movq	-1224(%rbp), %rcx
	jmp	.L46
.L47:
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L143:
	movl	%r12d, %esi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%rdi, %r13
	movq	%r8, -1192(%rbp)
	movl	$1, -1228(%rbp)
	jmp	.L20
.L84:
	movq	984(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L99
.L112:
	leaq	__PRETTY_FUNCTION__.10023(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$596, %edx
	call	__GI___assert_fail
.L58:
	movq	-1248(%rbp), %rbx
	movq	%rcx, 16(%rbx)
	movq	-1192(%rbp), %rbx
	movq	16(%rdi), %rdx
	cmpq	%rbx, %rdx
	movq	16(%rdx), %rdx
	cmove	%rdi, %rbx
	movq	%rbx, -1192(%rbp)
	movq	%rdx, 16(%rdi)
	movq	32(%rax), %rdi
	movq	24(%rax), %rdx
	testq	%rdi, %rdi
	je	.L61
	movq	%rdx, 24(%rdi)
	movq	24(%rax), %rdx
.L61:
	testq	%rdx, %rdx
	je	.L62
	movq	%rdi, 32(%rdx)
.L62:
	movq	8(%rcx), %rax
	movq	32(%rax), %rdx
	movq	%rdx, 32(%rsi)
	movq	%rsi, 32(%rax)
	movq	32(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L63
	movq	%rsi, 24(%rdx)
.L63:
	movq	%rax, 24(%rsi)
	jmp	.L64
.L138:
	movq	%rcx, -1248(%rbp)
	movq	%rcx, -1192(%rbp)
	jmp	.L37
.L278:
	movq	%rax, -1240(%rbp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L283:
	orb	$-128, 797(%r12)
	jmp	.L93
.L280:
	movq	8(%r13), %rdx
	cmpb	$0, (%rdx)
	jne	.L51
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rdx
	leaq	.LC0(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L51:
	leaq	.LC7(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L50
.L281:
	cmpq	$2147483645, (%r12)
	jne	.L254
	movq	-1200(%rbp), %rdi
	call	__GI__dl_exception_free
	jmp	.L37
.L258:
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	movq	%r14, %rsi
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
.L277:
	movq	-1256(%rbp), %r15
	leaq	.LC1(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	movq	8(%r15), %rsi
	call	_dl_signal_error@PLT
.L89:
	leaq	__PRETTY_FUNCTION__.10023(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$533, %edx
	call	__GI___assert_fail
.L87:
	movq	8(%r12), %rsi
	leaq	.LC11(%rip), %rcx
	xorl	%edx, %edx
	movl	$22, %edi
	call	_dl_signal_error@PLT
.L266:
	leaq	.LC6(%rip), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
.L276:
	cmpl	$-1, %ebx
	jne	.L124
	xorl	%ebx, %ebx
.L124:
	leaq	-1168(%rbp), %rsi
	xorl	%edx, %edx
	movl	%ebx, %edi
	call	_dl_signal_exception@PLT
.L263:
	leaq	__PRETTY_FUNCTION__.10023(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$244, %edx
	call	__GI___assert_fail
.L264:
	leaq	.LC2(%rip), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
.L262:
	movq	-1272(%rbp), %rax
	movq	-1088(%rbp), %rdi
	movq	-1256(%rbp), %r15
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L72
	call	*__rtld_free(%rip)
.L72:
	movq	8(%r15), %rsi
	leaq	.LC8(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error@PLT
.L91:
	movq	8(%r15), %rsi
	leaq	.LC10(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error@PLT
	.size	_dl_map_object_deps, .-_dl_map_object_deps
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10023, @object
	.size	__PRETTY_FUNCTION__.10023, 20
__PRETTY_FUNCTION__.10023:
	.string	"_dl_map_object_deps"
	.hidden	_dl_scope_free
	.hidden	_dl_sort_maps
	.hidden	__rtld_free
	.hidden	_dl_get_origin
	.hidden	_rtld_local
	.hidden	__rtld_malloc
	.hidden	_dl_debug_printf
	.hidden	_dl_dst_count
	.hidden	_dl_dst_substitute
	.hidden	_rtld_local_ro
	.hidden	strlen
	.hidden	rtld_errno
	.hidden	_dl_map_object
