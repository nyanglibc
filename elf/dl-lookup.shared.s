	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-lookup.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"version->filename == NULL || ! _dl_name_match_p (version->filename, map)"
	.text
	.p2align 4,,15
	.type	check_match, @function
check_match:
.LFB65:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movzbl	4(%r9), %eax
	movzwl	6(%r9), %r10d
	movq	64(%rsp), %rbp
	andl	$15, %eax
	cmpq	$0, 8(%r9)
	je	.L36
.L2:
	testw	%r10w, %r10w
	sete	%r10b
	movzbl	%r10b, %r10d
	testl	%r8d, %r10d
	jne	.L34
	movl	$1127, %r8d
	btl	%eax, %r8d
	jnc	.L34
	movq	%rsi, %rax
	movq	%r9, %rbx
	movl	%ecx, %r13d
	cmpq	%rax, %r9
	movq	%rdx, %r12
	movq	%rdi, %rsi
	je	.L5
	movl	(%r9), %edi
	addq	56(%rsp), %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L34
.L5:
	testq	%r12, %r12
	movq	840(%rbp), %rax
	je	.L6
	testq	%rax, %rax
	je	.L37
	movl	48(%rsp), %edx
	movzwl	(%rax,%rdx,2), %r13d
	movq	%r13, %rax
	andl	$32767, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	744(%rbp), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	8(%rax), %ebp
	cmpl	8(%r12), %ebp
	je	.L38
.L9:
	orl	12(%r12), %ebp
	jne	.L34
	testw	%r13w, %r13w
	js	.L34
.L8:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	testq	%rax, %rax
	je	.L8
	movl	48(%rsp), %edx
	movzwl	(%rax,%rdx,2), %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$32767, %ecx
	andl	$2, %r13d
	sete	%al
	addl	$2, %eax
	cmpl	%eax, %ecx
	jl	.L8
	testw	%dx, %dx
	js	.L34
	movq	80(%rsp), %rax
	movq	80(%rsp), %rcx
	movl	(%rax), %eax
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, (%rcx)
	jne	.L34
	movq	72(%rsp), %rax
	movq	%rbx, (%rax)
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
.L39:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	%rbp, %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L8
	leaq	__PRETTY_FUNCTION__.10027(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$106, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%r12), %rsi
	movq	(%rax), %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L36:
	cmpb	$6, %al
	je	.L2
	cmpw	$-15, %r10w
	je	.L2
	xorl	%eax, %eax
	jmp	.L39
.LFE65:
	.size	check_match, .-check_match
	.section	.rodata.str1.1
.LC2:
	.string	"<main program>"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"symbol=%s;  lookup in file=%s [%lu]\n"
	.section	.rodata.str1.1
.LC4:
	.string	"out of memory\n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"GLRO(dl_debug_mask) & DL_DEBUG_PRELINK"
	.align 8
.LC6:
	.string	"marking %s [%lu] as NODELETE due to unique symbol\n"
	.text
	.p2align 4,,15
	.type	do_lookup_x, @function
do_lookup_x:
.LFB70:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	movl	8(%r9), %eax
	movq	%rdi, 24(%rsp)
	movq	%rdx, 64(%rsp)
	movq	%rcx, 48(%rsp)
	movq	%r8, 56(%rsp)
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r10
	movl	208(%rsp), %r13d
	movq	%rax, %r14
	movq	%rsi, %rax
	movq	(%r9), %r12
	movq	%rsi, 32(%rsp)
	shrq	$6, %rax
	movq	%r10, %r15
	movq	%rax, 40(%rsp)
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L43:
	movl	756(%rbx), %esi
	testl	%esi, %esi
	je	.L101
	movq	112(%rbx), %rax
	movl	$0, 100(%rsp)
	movq	$0, 104(%rsp)
	movq	8(%rax), %r10
	movq	104(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 16(%rsp)
	movq	768(%rbx), %rax
	testq	%rax, %rax
	je	.L45
	movl	40(%rsp), %edx
	andl	760(%rbx), %edx
	movq	32(%rsp), %rdi
	movl	764(%rbx), %ecx
	movq	(%rax,%rdx,8), %rax
	movq	%rdi, %rdx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movl	%edi, %ecx
	shrq	%cl, %rax
	andq	%rdx, %rax
	testb	$1, %al
	jne	.L190
.L101:
	addq	$1, %rbp
	cmpq	%rbp, %r14
	jbe	.L191
.L103:
	movq	(%r12,%rbp,8), %rax
	movq	40(%rax), %rbx
	cmpq	%r15, %rbx
	je	.L101
	movl	%r13d, %eax
	andl	$2, %eax
	movl	%eax, 8(%rsp)
	je	.L42
	testb	$3, 796(%rbx)
	je	.L101
.L42:
	testb	$32, 797(%rbx)
	jne	.L101
	testb	$8, _rtld_local_ro(%rip)
	je	.L43
	movq	8(%rbx), %rdx
	movq	48(%rbx), %rcx
	cmpb	$0, (%rdx)
	je	.L192
.L44:
	movq	24(%rsp), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L192:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rdx
	leaq	.LC2(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L191:
	addq	$120, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	64(%rsp), %rax
	movl	$4294967295, %edi
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	je	.L51
	xorl	%edx, %edx
	divq	%rsi
	salq	$2, %rdx
.L52:
	movq	784(%rbx), %rax
	movl	(%rax,%rdx), %eax
	testl	%eax, %eax
	je	.L101
	leaq	100(%rsp), %rdi
	leaq	104(%rsp), %rsi
	movq	%r12, 72(%rsp)
	movq	%rbp, 176(%rsp)
	movq	%r14, 80(%rsp)
	movq	%r10, %rbp
	movq	%rbx, %r14
	movq	%r15, 200(%rsp)
	movq	%rdi, %r12
	movq	%rsi, %rbx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L194:
	movq	776(%r14), %rax
	movl	(%rax,%r15,4), %eax
	testl	%eax, %eax
	je	.L193
.L60:
	movl	%eax, %r15d
	subq	$8, %rsp
	movl	%r13d, %r8d
	leaq	(%r15,%r15,2), %rdx
	pushq	%r12
	pushq	%rbx
	pushq	%r14
	pushq	48(%rsp)
	leaq	0(%rbp,%rdx,8), %r9
	pushq	%rax
	movl	240(%rsp), %ecx
	movq	232(%rsp), %rdx
	movq	96(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	check_match
	addq	$48, %rsp
	testq	%rax, %rax
	je	.L194
	movq	%r14, %rbx
	movq	%rbp, %r10
	movq	72(%rsp), %r12
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	%rax, %r8
	movq	80(%rsp), %r14
	.p2align 4,,10
	.p2align 3
.L48:
	cmpq	$0, 216(%rsp)
	je	.L195
.L62:
	movzbl	5(%r8), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L101
	movzbl	4(%r8), %eax
	shrb	$4, %al
	cmpb	$2, %al
	je	.L68
	cmpb	$10, %al
	je	.L69
	cmpb	$1, %al
	jne	.L101
.L70:
	movq	56(%rsp), %rax
	movq	%r8, (%rax)
	movq	%rbx, 8(%rax)
.L67:
.L71:
	addq	$120, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	movq	776(%rbx), %rax
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	je	.L101
	movq	784(%rbx), %rdx
	leaq	104(%rsp), %rdi
	movq	%r12, 80(%rsp)
	movq	%rbp, 176(%rsp)
	movq	%r15, 200(%rsp)
	movq	%r14, 88(%rsp)
	movq	%rdi, 72(%rsp)
	movq	%rbx, %r14
	leaq	(%rdx,%rax,4), %r11
	leaq	100(%rsp), %rax
	movq	32(%rsp), %r12
	movq	%r10, %rbx
	movq	%r11, %rbp
	movq	%rax, %r15
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$4, %rbp
	andl	$1, %edx
	jne	.L196
.L49:
	movl	0(%rbp), %eax
	movq	%rax, %rdx
	xorq	%r12, %rax
	shrq	%rax
	jne	.L47
	movq	%rbp, %rax
	subq	784(%r14), %rax
	subq	$8, %rsp
	pushq	%r15
	pushq	88(%rsp)
	movl	%r13d, %r8d
	pushq	%r14
	pushq	48(%rsp)
	sarq	$2, %rax
	movl	%eax, %edx
	pushq	%rax
	movl	240(%rsp), %ecx
	leaq	(%rdx,%rdx,2), %rdx
	movq	96(%rsp), %rsi
	movq	72(%rsp), %rdi
	leaq	(%rbx,%rdx,8), %r9
	movq	232(%rsp), %rdx
	call	check_match
	addq	$48, %rsp
	testq	%rax, %rax
	jne	.L184
	movl	0(%rbp), %edx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L195:
	testb	$3, 796(%rbx)
	jne	.L62
	cmpl	$4, %r13d
	jne	.L62
	movq	120(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L62
	movq	128(%rbx), %rax
	testq	%rax, %rax
	je	.L62
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L62
	movabsq	$-6148914691236517205, %rsi
	movq	8(%rdx), %r11
	mulq	%rsi
	movq	%rdx, %rax
	shrq	$4, %rax
	testl	%eax, %eax
	je	.L62
	subl	$1, %eax
	movq	%r12, 72(%rsp)
	movq	%rbx, 88(%rsp)
	leaq	(%rax,%rax,2), %rax
	movq	%rbp, 176(%rsp)
	movq	%r8, 80(%rsp)
	movq	%r11, %rbx
	movq	%r10, %rbp
	leaq	24(%r11,%rax,8), %r9
	movq	%r9, %r12
	jmp	.L66
.L111:
	xorl	%eax, %eax
.L63:
	cmpq	$6, %rdx
	movl	$4, %esi
	jne	.L65
.L104:
	orl	%esi, %eax
	cmpl	$2, %eax
	jne	.L65
	shrq	$32, %rcx
	movq	24(%rsp), %rsi
	leaq	(%rcx,%rcx,2), %rax
	movl	0(%rbp,%rax,8), %edi
	addq	16(%rsp), %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L186
.L65:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	je	.L197
.L66:
	movq	8(%rbx), %rcx
	movl	%ecx, %edx
	cmpq	$36, %rdx
	ja	.L111
	movabsq	$68719935616, %rax
	shrq	%cl, %rax
	andl	$1, %eax
	cmpq	$5, %rdx
	jne	.L63
	orl	$2, %eax
	xorl	%esi, %esi
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L68:
	movl	76+_rtld_local_ro(%rip), %ecx
	testl	%ecx, %ecx
	je	.L70
	movq	56(%rsp), %rax
	cmpq	$0, (%rax)
	jne	.L101
	movq	%r8, (%rax)
	movq	%rbx, 8(%rax)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L69:
	movq	48(%rbx), %rsi
	leaq	_rtld_local(%rip), %r14
	movq	%r8, 72(%rsp)
	movq	32(%rsp), %r15
	leaq	(%rsi,%rsi,8), %rax
	movq	%rsi, 40(%rsp)
	leaq	(%rsi,%rax,2), %rbp
	salq	$3, %rbp
	leaq	40(%r14,%rbp), %rax
	addq	%r14, %rbp
	movq	%rax, 64(%rsp)
	movq	%rax, %rdi
	call	*3984+_rtld_local(%rip)
	movq	80(%rbp), %rax
	movq	88(%rbp), %r14
	movq	72(%rsp), %r8
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	je	.L72
	movq	%r15, %rax
	xorl	%edx, %edx
	leaq	-2(%r14), %rcx
	divq	%r14
	movq	%r15, %rax
	movq	%rbx, 80(%rsp)
	movq	%rdx, %r12
	xorl	%edx, %edx
	divq	%rcx
	movq	%r12, %r13
	movq	%r12, 88(%rsp)
	movq	%r15, %r12
	movq	%r14, %r15
	leaq	1(%rdx), %r9
	movq	%r9, %rdx
	movq	%r9, %rbx
	salq	$5, %rdx
	movq	%rdx, %r14
.L79:
	movq	%r13, %rbp
	salq	$5, %rbp
	addq	32(%rsp), %rbp
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L73:
	testq	%rdi, %rdi
	je	.L77
.L74:
	addq	%rbx, %r13
	addq	%r14, %rbp
	cmpq	%r13, %r15
	jbe	.L198
.L78:
	movl	0(%rbp), %eax
	movq	8(%rbp), %rdi
	cmpq	%rax, %r12
	jne	.L73
	movq	24(%rsp), %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L74
	movl	8(%rsp), %edx
	movq	72(%rsp), %r8
	movq	80(%rsp), %rbx
	testl	%edx, %edx
	je	.L75
	movq	56(%rsp), %rax
	movq	%r8, (%rax)
	movq	%rbx, 8(%rax)
.L76:
	movq	64(%rsp), %rdi
	call	*3992+_rtld_local(%rip)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%r14, %rbx
	movq	%rbp, %r10
	movq	72(%rsp), %r12
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	80(%rsp), %r14
.L50:
	cmpl	$1, 100(%rsp)
	jne	.L101
	movq	104(%rsp), %r8
	testq	%r8, %r8
	jne	.L48
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L77:
	movq	40(%rsp), %rsi
	leaq	_rtld_local(%rip), %rdi
	movq	%r15, %r14
	leaq	(%r14,%r14,2), %rdx
	movq	%rbx, %r9
	movq	%r12, %r15
	movq	72(%rsp), %r8
	movq	80(%rsp), %rbx
	leaq	(%rsi,%rsi,8), %rax
	movq	88(%rsp), %r12
	leaq	(%rsi,%rax,2), %rax
	movq	96(%rdi,%rax,8), %rax
	salq	$2, %rax
	cmpq	%rax, %rdx
	jbe	.L199
.L80:
	movl	8(%rsp), %eax
	movl	(%r8), %ecx
	movq	%r9, %rdx
	addq	16(%rsp), %rcx
	salq	$5, %rdx
	movq	32(%rsp), %rsi
	testl	%eax, %eax
	je	.L92
.L93:
	movq	%r12, %rax
	salq	$5, %rax
	addq	%rsi, %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	addq	%r9, %r12
	addq	%rdx, %rax
	cmpq	%r12, %r14
	jbe	.L200
.L94:
	cmpq	$0, 8(%rax)
	jne	.L95
	movq	48(%rsp), %rdi
	movq	216(%rsp), %rsi
	movl	%r15d, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rdi, 16(%rax)
	movq	%rsi, 24(%rax)
.L96:
	movq	40(%rsp), %rdi
	leaq	(%rdi,%rdi,8), %rax
	leaq	(%rdi,%rax,2), %rax
	leaq	_rtld_local(%rip), %rdi
	addq	$1, 96(%rdi,%rax,8)
.L90:
	movq	%r8, 8(%rsp)
	movq	64(%rsp), %rdi
	call	*3992+_rtld_local(%rip)
	movq	56(%rsp), %rax
	movq	8(%rsp), %r8
	movq	%rbx, 8(%rax)
	movq	%r8, (%rax)
	jmp	.L71
.L201:
	subq	%r14, %r12
.L92:
	movq	%r12, %rax
	salq	$5, %rax
	addq	%rsi, %rax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	addq	%r9, %r12
	addq	%rdx, %rax
	cmpq	%r12, %r14
	jbe	.L201
.L97:
	cmpq	$0, 8(%rax)
	jne	.L98
	movl	%r15d, (%rax)
	movq	%rcx, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%rbx, 24(%rax)
	movzbl	796(%rbx), %eax
	andl	$3, %eax
	cmpb	$2, %al
	jne	.L96
	cmpb	$0, 799(%rbx)
	jne	.L96
	testb	$8, 192(%rsp)
	je	.L99
	cmpb	$0, 800(%rbx)
	jne	.L96
	testb	$4, _rtld_local_ro(%rip)
	jne	.L202
.L106:
	movb	$1, 800(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L198:
	subq	%r15, %r13
	jmp	.L79
.L200:
	subq	%r14, %r12
	jmp	.L93
.L196:
	movq	%rbx, %r10
	movq	80(%rsp), %r12
	movq	%r14, %rbx
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	88(%rsp), %r14
	jmp	.L50
.L51:
	movq	24(%rsp), %rax
	xorl	%edx, %edx
	movzbl	(%rax), %ecx
	testq	%rcx, %rcx
	je	.L53
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L189
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	movzbl	2(%rax), %eax
	testb	%al, %al
	je	.L189
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	movzbl	3(%rax), %eax
	testb	%al, %al
	je	.L189
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	movzbl	4(%rax), %eax
	testb	%al, %al
	je	.L189
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	leaq	5(%rax), %rdx
	movzbl	5(%rax), %eax
	testb	%al, %al
	je	.L58
.L59:
	salq	$4, %rcx
	addq	$1, %rdx
	addq	%rcx, %rax
	movq	%rax, %rcx
	shrq	$24, %rcx
	andl	$240, %ecx
	xorq	%rax, %rcx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L59
.L58:
	andl	$268435455, %ecx
.L189:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%rsi
	salq	$2, %rdx
.L53:
	movq	64(%rsp), %rax
	movq	%rcx, (%rax)
	jmp	.L52
.L72:
	testq	%r14, %r14
	jne	.L203
	movq	%r8, 24(%rsp)
	movl	$31, %esi
	movl	$32, %edi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	je	.L81
	movq	%rax, 80(%rbp)
	movq	__rtld_free(%rip), %rax
	movabsq	$595056260442243601, %rdx
	movq	%r15, %r12
	movq	%r15, %rsi
	movq	$31, 88(%rbp)
	movl	$31, %r14d
	movq	24(%rsp), %r8
	movq	%rax, 104(%rbp)
	movq	%r15, %rax
	mulq	%rdx
	subq	%rdx, %r12
	shrq	%r12
	addq	%rdx, %r12
	movabsq	$5088756985850910791, %rdx
	shrq	$4, %r12
	movq	%r12, %rax
	salq	$5, %rax
	subq	%r12, %rax
	movq	%r15, %r12
	subq	%rax, %r12
	movq	%r15, %rax
	imulq	%rdx
	movq	%rdx, %rax
	movq	%r15, %rdx
	sarq	$63, %rdx
	sarq	$3, %rax
	subq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rax,%rdx,4), %rax
	subq	%rax, %rsi
	leaq	1(%rsi), %r9
	jmp	.L80
.L199:
	leaq	1(%r14), %rdi
	call	_dl_higher_prime_number
	movl	$32, %edi
	movq	%rax, %rbp
	movq	%rax, %rsi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L81
	leaq	-2(%rbp), %rax
	testq	%r14, %r14
	movq	72(%rsp), %r8
	movq	%rax, 24(%rsp)
	je	.L83
	movq	32(%rsp), %rax
	salq	$5, %r14
	movq	%rax, %rsi
	addq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L88:
	movq	8(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L84
	movl	(%rsi), %r12d
	xorl	%edx, %edx
	movq	24(%rsi), %r10
	movq	16(%rsi), %r11
	movq	%r12, %rax
	movq	%r12, %r9
	divq	%rbp
	movq	%r12, %rax
	movq	%rdx, %rcx
	xorl	%edx, %edx
	divq	24(%rsp)
	addq	$1, %rdx
	movq	%rdx, %r12
	salq	$5, %r12
.L85:
	movq	%rcx, %rax
	salq	$5, %rax
	addq	%r13, %rax
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	addq	%rdx, %rcx
	addq	%r12, %rax
	cmpq	%rcx, %rbp
	jbe	.L204
.L86:
	cmpq	$0, 8(%rax)
	jne	.L87
	movl	%r9d, (%rax)
	movq	%rdi, 8(%rax)
	movq	%r11, 16(%rax)
	movq	%r10, 24(%rax)
.L84:
	addq	$32, %rsi
	cmpq	%rsi, %r14
	jne	.L88
.L83:
	movq	40(%rsp), %rdi
	leaq	_rtld_local(%rip), %rsi
	movq	%r8, 72(%rsp)
	movq	%rbp, %r14
	leaq	(%rdi,%rdi,8), %rax
	leaq	(%rdi,%rax,2), %rax
	movq	32(%rsp), %rdi
	leaq	(%rsi,%rax,8), %r12
	call	*104(%r12)
	movq	__rtld_free(%rip), %rax
	xorl	%edx, %edx
	movq	%rbp, 88(%r12)
	movq	%r13, 80(%r12)
	movq	%r13, 32(%rsp)
	movq	72(%rsp), %r8
	movq	%rax, 104(%r12)
	movq	%r15, %rax
	divq	%rbp
	movq	%r15, %rax
	movq	%rdx, %r12
	xorl	%edx, %edx
	divq	24(%rsp)
	leaq	1(%rdx), %r9
	jmp	.L80
.L204:
	subq	%rbp, %rcx
	jmp	.L85
.L184:
	movq	%rbx, %r10
	movq	%rax, %r8
	movq	%r14, %rbx
	movq	80(%rsp), %r12
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	88(%rsp), %r14
	jmp	.L48
.L75:
	movq	16(%rbp), %rax
	movq	56(%rsp), %rdi
	movq	%rax, (%rdi)
	movq	24(%rbp), %rax
	movq	%rax, 8(%rdi)
	jmp	.L76
.L99:
	testb	$4, _rtld_local_ro(%rip)
	jne	.L205
.L108:
	movb	$1, 799(%rbx)
	jmp	.L96
.L203:
	testb	$8, 1+_rtld_local_ro(%rip)
	jne	.L90
	leaq	__PRETTY_FUNCTION__.10076(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$306, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L197:
	movq	80(%rsp), %r8
	movq	72(%rsp), %r12
	movq	88(%rsp), %rbx
	movq	176(%rsp), %rbp
	jmp	.L62
.L186:
	movq	72(%rsp), %r12
	movq	176(%rsp), %rbp
	jmp	.L101
.L202:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, 8(%rsp)
	call	_dl_debug_printf
	movq	8(%rsp), %r8
	jmp	.L106
.L81:
	movq	64(%rsp), %rdi
	call	*3992+_rtld_local(%rip)
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L205:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, 8(%rsp)
	call	_dl_debug_printf
	movq	8(%rsp), %r8
	jmp	.L108
.LFE70:
	.size	do_lookup_x, .-do_lookup_x
	.section	.rodata.str1.1
.LC7:
	.string	", version "
.LC8:
	.string	""
.LC9:
	.string	"protected"
.LC10:
	.string	"normal"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"version == NULL || !(flags & DL_LOOKUP_RETURN_NEWEST)"
	.section	.rodata.str1.1
.LC12:
	.string	"undefined symbol: %s%s%s"
.LC13:
	.string	"symbol lookup error"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"marking %s [%lu] as NODELETE due to reference to main program\n"
	.align 8
.LC15:
	.string	"marking %s [%lu] as NODELETE due to reference to %s [%lu]\n"
	.align 8
.LC16:
	.string	"\nfile=%s [%lu];  needed by %s [%lu] (relocation dependency)\n\n"
	.align 8
.LC17:
	.string	"binding file %s [%lu] to %s [%lu]: %s symbol `%s'"
	.section	.rodata.str1.1
.LC18:
	.string	" [%s]\n"
.LC19:
	.string	"\n"
.LC20:
	.string	"x 0x%0*Zx 0x%0*Zx "
.LC21:
	.string	"/%x %s\n"
	.section	.rodata.str1.8
	.align 8
.LC22:
	.string	"marking %s [%lu] as NODELETE due to memory allocation failure\n"
	.section	.rodata.str1.1
.LC23:
	.string	"lookup"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"%s 0x%0*Zx 0x%0*Zx -> 0x%0*Zx 0x%0*Zx "
	.section	.rodata.str1.1
.LC25:
	.string	"conflict"
	.text
	.p2align 4,,15
	.globl	_dl_lookup_symbol_x
	.hidden	_dl_lookup_symbol_x
	.type	_dl_lookup_symbol_x, @function
_dl_lookup_symbol_x:
.LFB73:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%r8, %r12
	subq	$152, %rsp
	movzbl	(%rdi), %edx
	movq	%rcx, 24(%rsp)
	movl	%r9d, 48(%rsp)
	testb	%dl, %dl
	je	.L348
	movq	%rdi, %rcx
	movl	$5381, %ebx
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rbx, %rax
	addq	$1, %rcx
	salq	$5, %rax
	addq	%rax, %rbx
	addq	%rdx, %rbx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L208
	movl	%ebx, %ebx
.L207:
	movl	$4294967295, %eax
	addq	$1, 2544+_rtld_local(%rip)
	testq	%r12, %r12
	movq	%rax, 64(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 88(%rsp)
	je	.L209
	testb	$2, 208(%rsp)
	jne	.L498
.L209:
	cmpq	$0, 216(%rsp)
	movq	24(%rsp), %rax
	movq	(%rax), %r9
	jne	.L499
	testq	%r9, %r9
	movq	0(%rbp), %rcx
	je	.L213
	movq	$0, 32(%rsp)
	xorl	%eax, %eax
.L341:
	leaq	80(%rsp), %rdi
	leaq	64(%rsp), %rsi
	movq	24(%rsp), %r14
	movq	%rdi, 16(%rsp)
	movq	%rsi, 8(%rsp)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L500:
	addq	$8, %r14
	movq	(%r14), %r9
	xorl	%eax, %eax
	movq	0(%rbp), %rcx
	testq	%r9, %r9
	je	.L216
.L215:
	pushq	%r13
	movl	56(%rsp), %edi
	pushq	%rdi
	pushq	232(%rsp)
	movq	%r15, %rdi
	movl	232(%rsp), %esi
	pushq	%rsi
	pushq	%r12
	movq	%rbx, %rsi
	pushq	%rax
	movq	64(%rsp), %r8
	movq	56(%rsp), %rdx
	call	do_lookup_x
	addq	$48, %rsp
	testl	%eax, %eax
	je	.L500
	movq	0(%rbp), %rcx
.L216:
	cmpq	$0, 80(%rsp)
	je	.L213
	testq	%rcx, %rcx
	je	.L226
	movzbl	5(%rcx), %eax
	andl	$3, %eax
	cmpb	$3, %al
	je	.L501
.L226:
	movq	88(%rsp), %r14
	xorl	%ebx, %ebx
.L225:
	movzbl	796(%r14), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L502
.L237:
	movl	996(%r14), %esi
	testl	%esi, %esi
	je	.L503
.L289:
	movl	_rtld_local_ro(%rip), %edx
	movq	80(%rsp), %rax
	testl	$2052, %edx
	jne	.L504
.L290:
	movq	%rax, 0(%rbp)
.L206:
	addq	$152, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L501:
	cmpl	$1, 48(%rsp)
	je	.L505
	movq	24(%rsp), %rax
	movq	$0, 112(%rsp)
	movq	$0, 120(%rsp)
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L236
	cmpl	$4, 48(%rsp)
	movq	24(%rsp), %r10
	leaq	112(%rsp), %rax
	movq	32(%rsp), %rdx
	movq	%r13, 32(%rsp)
	movq	%rax, 16(%rsp)
	movq	%r10, %r13
	sete	%r14b
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L506:
	testb	%r14b, %r14b
	movl	$4, %eax
	je	.L363
.L231:
	pushq	$0
	pushq	%rax
	movq	%rbx, %rsi
	pushq	232(%rsp)
	movl	232(%rsp), %eax
	movq	%r15, %rdi
	pushq	%rax
	pushq	%r12
	pushq	%rdx
	movq	64(%rsp), %r8
	movq	56(%rsp), %rdx
	call	do_lookup_x
	addq	$48, %rsp
	testl	%eax, %eax
	jne	.L233
	addq	$8, %r13
	movq	0(%r13), %r9
	testq	%r9, %r9
	je	.L233
	movq	0(%rbp), %rcx
	xorl	%edx, %edx
.L234:
	movzbl	4(%rcx), %eax
	andl	$15, %eax
	cmpb	$1, %al
	je	.L506
.L363:
	movl	$1, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L348:
	movl	$5381, %ebx
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L213:
	testq	%rcx, %rcx
	je	.L218
	movzbl	4(%rcx), %eax
	shrb	$4, %al
	cmpb	$2, %al
	je	.L219
.L218:
	testb	$1, 1+_rtld_local_ro(%rip)
	jne	.L219
	testq	%r13, %r13
	je	.L220
	testq	%r12, %r12
	movq	8(%r13), %rsi
	je	.L507
	movq	(%r12), %r9
	leaq	.LC8(%rip), %rax
	leaq	.LC7(%rip), %r8
	testq	%r9, %r9
	cmove	%rax, %r9
.L320:
	cmpb	$0, (%rsi)
	je	.L330
.L222:
	leaq	112(%rsp), %rbx
	leaq	.LC12(%rip), %rdx
	movq	%r15, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	__GI__dl_exception_create_format
	leaq	.LC13(%rip), %rdx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	call	_dl_signal_cexception
	movq	%rbx, %rdi
	call	__GI__dl_exception_free
.L219:
	movq	$0, 0(%rbp)
	xorl	%r14d, %r14d
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L499:
	movq	(%r9), %rdx
	movq	(%rdx), %rax
	cmpq	%rax, 216(%rsp)
	movl	$0, %eax
	je	.L211
	movq	216(%rsp), %rcx
	.p2align 4,,10
	.p2align 3
.L212:
	addq	$1, %rax
	cmpq	%rcx, (%rdx,%rax,8)
	jne	.L212
.L211:
	movq	0(%rbp), %rcx
	movq	%rax, 32(%rsp)
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L504:
	testb	$4, %dl
	je	.L291
	movq	8(%r14), %rcx
	leaq	.LC10(%rip), %r9
	leaq	.LC9(%rip), %rax
	testl	%ebx, %ebx
	movq	48(%r14), %r8
	cmovne	%rax, %r9
	cmpb	$0, (%rcx)
	jne	.L293
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rcx
	leaq	.LC2(%rip), %rax
	testq	%rcx, %rcx
	cmove	%rax, %rcx
.L293:
	movq	8(%r13), %rsi
	movq	48(%r13), %rdx
	cmpb	$0, (%rsi)
	jne	.L294
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC2(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L294:
	subq	$8, %rsp
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	pushq	%r15
	call	_dl_debug_printf
	testq	%r12, %r12
	popq	%rdx
	popq	%rcx
	je	.L295
	movq	(%r12), %rsi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf_c
	movl	_rtld_local_ro(%rip), %edx
.L291:
	andb	$8, %dh
	je	.L508
	movq	656+_rtld_local_ro(%rip), %rdx
	movq	$0, 96(%rsp)
	movq	$0, 104(%rsp)
	movq	_rtld_local(%rip), %rax
	testq	%rdx, %rdx
	je	.L297
	cmpq	%rax, %rdx
	je	.L297
.L298:
	movq	80(%rsp), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L509
.L307:
	movzbl	4(%rax), %edx
	andl	$3, 48(%rsp)
	andl	$15, %edx
	cmpb	$6, %dl
	je	.L343
	xorl	%edi, %edi
.L344:
	movl	48(%rsp), %ebx
	movl	%ebx, %ecx
	orl	$8, %ecx
	cmpb	$10, %dl
	cmovne	%ebx, %ecx
	testl	%edi, %edi
	movl	%ecx, 48(%rsp)
	jne	.L346
.L308:
	movq	656+_rtld_local_ro(%rip), %rdx
	cmpq	%rdx, %r13
	sete	%cl
	testq	%rdx, %rdx
	sete	%dl
	orb	%dl, %cl
	jne	.L318
	cmpl	$3, 48(%rsp)
	jle	.L319
.L318:
	movq	856(%r13), %rcx
	movq	0(%rbp), %r9
	subq	%rcx, %r9
	testq	%rsi, %rsi
	je	.L486
	movq	88(%rsp), %rax
	movq	8(%rsi), %rdx
	movq	856(%rax), %rax
.L333:
	pushq	%rdx
	leaq	.LC23(%rip), %rsi
	pushq	$16
	leaq	.LC24(%rip), %rdi
	pushq	%rax
	pushq	$16
	movl	$16, %r8d
	movl	$16, %edx
	xorl	%eax, %eax
	call	_dl_printf
	addq	$32, %rsp
.L326:
	movl	48(%rsp), %esi
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	movq	%r15, %rdx
	call	_dl_printf
	movq	80(%rsp), %rax
.L319:
	movq	88(%rsp), %r14
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L503:
	movl	$1, 996(%r14)
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L502:
	cmpq	%r14, %r13
	je	.L237
	testb	$1, 208(%rsp)
	je	.L237
	cmpb	$0, 799(%r14)
	jne	.L237
	movl	208(%rsp), %eax
	andl	$8, %eax
	movl	%eax, 16(%rsp)
	je	.L238
	cmpb	$0, 800(%r14)
	jne	.L237
.L238:
	movq	984(%r13), %rax
	movq	%rax, 32(%rsp)
	movq	976(%r13), %r8
	testq	%r8, %r8
	je	.L321
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	je	.L321
	xorl	%eax, %eax
	cmpq	%rdx, %r14
	jne	.L239
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L240:
	cmpq	%r14, %rdx
	je	.L266
.L239:
	leal	1(%rax), %edx
	movq	%rdx, %rax
	movq	(%r8,%rdx,8), %rdx
	testq	%rdx, %rdx
	jne	.L240
.L321:
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L352
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	je	.L241
	cmpq	%r14, 8(%rax)
	je	.L266
	movq	32(%rsp), %rax
	leal	-1(%r9), %edx
	addq	$16, %rax
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L243
.L244:
	addq	$8, %rax
	cmpq	%r14, -8(%rax)
	je	.L266
.L243:
	cmpq	%rax, %rdx
	jne	.L244
.L241:
	movq	1152(%r14), %rax
	movq	%rax, 40(%rsp)
	movl	208(%rsp), %eax
	andl	$4, %eax
	movl	%eax, 8(%rsp)
	jne	.L510
	movl	%r9d, 32(%rsp)
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	movl	32(%rsp), %r9d
.L252:
	movq	48(%r13), %r8
	leaq	(%r8,%r8,8), %rax
	leaq	(%r8,%rax,2), %rdx
	leaq	_rtld_local(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	cmpq	%r14, %rax
	je	.L259
	testq	%rax, %rax
	jne	.L260
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L511:
	cmpq	%r14, %rax
	je	.L259
.L260:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L511
.L259:
	testq	%rax, %rax
	je	.L262
	movq	40(%rsp), %rax
	cmpq	1152(%r14), %rax
	je	.L512
	leaq	2440+_rtld_local(%rip), %rdi
	movl	$-1, %r14d
	call	*3992+_rtld_local(%rip)
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.L287
.L342:
#APP
# 806 "dl-lookup.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	addl	$1, %r14d
	jne	.L266
.L288:
	movq	920(%r13), %rax
	movq	%rax, 24(%rsp)
.L287:
	pushq	216(%rsp)
	movl	216(%rsp), %eax
	movq	%r12, %r8
	movq	%r15, %rdi
	movq	%rbp, %rdx
	movq	%r13, %rsi
	pushq	%rax
	movl	64(%rsp), %r9d
	movq	40(%rsp), %rcx
	call	_dl_lookup_symbol_x
	popq	%rdi
	movq	%rax, %r14
	popq	%r8
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L233:
	cmpq	$0, 112(%rsp)
	movq	32(%rsp), %r13
	je	.L236
	cmpq	%r13, 120(%rsp)
	je	.L236
	movq	0(%rbp), %rax
	movq	%r13, 88(%rsp)
	movq	%r13, %r14
	movq	%rax, 80(%rsp)
.L230:
	movl	$1, %ebx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L505:
	cmpq	%r13, 88(%rsp)
	je	.L228
	movq	%rcx, 80(%rsp)
	movq	%r13, 88(%rsp)
.L228:
	movq	%r13, %r14
	movl	$1, %ebx
	jmp	.L237
.L508:
	movq	88(%rsp), %r14
	movq	80(%rsp), %rax
	jmp	.L290
.L236:
	movq	88(%rsp), %r14
	jmp	.L230
.L297:
	cmpq	%rax, %r13
	je	.L298
	movzbl	(%r15), %edx
	testb	%dl, %dl
	je	.L359
	movq	%r15, %rcx
	movl	$5381, %ebx
	.p2align 4,,10
	.p2align 3
.L300:
	movq	%rbx, %rax
	addq	$1, %rcx
	salq	$5, %rax
	addq	%rax, %rbx
	addq	%rdx, %rbx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L300
	movl	%ebx, %ebx
.L299:
	movl	$4294967295, %eax
	movq	928(%r13), %r9
	movq	0(%rbp), %rcx
	movq	%rax, 72(%rsp)
	leaq	72(%rsp), %rax
	movq	%r15, %rdi
	movq	80+_rtld_local(%rip), %r14
	movq	$0, 80+_rtld_local(%rip)
	movq	%rax, 16(%rsp)
	pushq	%r13
	movq	%rax, %rdx
	movl	56(%rsp), %esi
	pushq	%rsi
	pushq	$0
	movq	%rbx, %rsi
	pushq	$0
	pushq	%r12
	pushq	$0
	leaq	144(%rsp), %r8
	call	do_lookup_x
	movq	128(%rsp), %rax
	movq	144(%rsp), %rdx
	addq	$48, %rsp
	cmpq	%rax, %rdx
	movq	%rax, %rsi
	je	.L301
.L488:
	testq	%rax, %rax
	movq	%r14, 80+_rtld_local(%rip)
	je	.L513
	movzbl	4(%rax), %edx
	andl	$3, 48(%rsp)
	andl	$15, %edx
	cmpb	$6, %dl
	je	.L345
	movl	$1, %edi
	jmp	.L344
.L220:
	leaq	.LC8(%rip), %r8
	testq	%r12, %r12
	movq	%r8, %r9
	je	.L330
	movq	(%r12), %r9
	leaq	.LC7(%rip), %r8
	testq	%r9, %r9
	jne	.L330
	leaq	.LC8(%rip), %r9
.L330:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC2(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L222
.L295:
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf_c
	movl	_rtld_local_ro(%rip), %edx
	jmp	.L291
.L509:
	cmpq	%rdx, %r13
	sete	%cl
	testq	%rdx, %rdx
	sete	%dl
	orb	%dl, %cl
	jne	.L339
.L497:
	cmpl	$3, 48(%rsp)
	jle	.L319
.L339:
	movq	856(%r13), %rcx
	movq	0(%rbp), %r9
	subq	%rcx, %r9
.L486:
	xorl	%edx, %edx
	xorl	%eax, %eax
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L346:
	movq	8(%rax), %rdx
	movq	88(%rsp), %rax
	movq	856(%r13), %rcx
	movq	0(%rbp), %r9
	movq	856(%rax), %rax
	subq	%rcx, %r9
.L340:
	pushq	%rdx
	leaq	.LC25(%rip), %rsi
	pushq	$16
	leaq	.LC24(%rip), %rdi
	pushq	%rax
	pushq	$16
	xorl	%eax, %eax
	movl	$16, %r8d
	movl	$16, %edx
	call	_dl_printf
	movq	128(%rsp), %rax
	xorl	%r8d, %r8d
	addq	$32, %rsp
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L328
	movq	8(%rax), %r8
	movq	104(%rsp), %rax
	movq	856(%rax), %rdx
.L328:
	leaq	.LC20(%rip), %rdi
	movl	$16, %ecx
	movl	$16, %esi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L326
.L512:
	cmpb	$0, 799(%r14)
	jne	.L494
	cmpl	$0, 16(%rsp)
	je	.L267
	cmpb	$0, 800(%r14)
	jne	.L494
	movzbl	796(%r13), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L335
	testb	$4, _rtld_local_ro(%rip)
	jne	.L270
.L280:
	movb	$1, 800(%r14)
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	cmpl	$0, 8(%rsp)
	je	.L266
.L281:
#APP
# 806 "dl-lookup.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
.L266:
	movq	88(%rsp), %r14
	jmp	.L237
.L507:
	leaq	.LC8(%rip), %r8
	movq	%r8, %r9
	jmp	.L320
.L513:
	movq	856(%r13), %rcx
	movq	0(%rbp), %r9
	xorl	%edx, %edx
	xorl	%eax, %eax
	subq	%rcx, %r9
	jmp	.L340
.L301:
	movq	104(%rsp), %rcx
	cmpq	88(%rsp), %rcx
	jne	.L488
	cmpb	$0, 797(%r13)
	jns	.L364
	testq	%rax, %rax
	jne	.L305
.L364:
	testq	%rax, %rax
	movq	%r14, 80+_rtld_local(%rip)
	jne	.L307
	movq	%rdx, %rsi
	jmp	.L308
.L343:
	movq	856(%r13), %rcx
	movq	0(%rbp), %r9
	movq	8(%rax), %rdx
	movq	88(%rsp), %rax
	movl	$4, 48(%rsp)
	subq	%rcx, %r9
	movq	856(%rax), %rax
	jmp	.L333
.L359:
	movl	$5381, %ebx
	jmp	.L299
.L352:
	xorl	%r9d, %r9d
	jmp	.L241
.L262:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	movl	8(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L287
#APP
# 806 "dl-lookup.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	jmp	.L288
.L510:
	xorl	%eax, %eax
#APP
# 640 "dl-lookup.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	je	.L514
.L246:
	movl	%r9d, 52(%rsp)
	movq	%r8, 56(%rsp)
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	movq	976(%r13), %rdx
	movq	%r14, %rax
	movl	52(%rsp), %r9d
	movq	%rax, %r14
	testq	%rdx, %rdx
	je	.L247
	movq	56(%rsp), %r8
	cmpq	%r8, %rdx
	je	.L247
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L247
	cmpq	%rcx, %rax
	je	.L248
	xorl	%ecx, %ecx
.L249:
	leal	1(%rcx), %esi
	movq	%rsi, %rcx
	movq	(%rdx,%rsi,8), %rsi
	testq	%rsi, %rsi
	je	.L247
	cmpq	%rsi, %rax
	jne	.L249
.L490:
	movq	1152(%rax), %rax
.L250:
	cmpq	%rax, 40(%rsp)
	leaq	2440+_rtld_local(%rip), %rdi
	setne	%al
	movzbl	%al, %eax
	negl	%eax
	movl	%eax, %r14d
	call	*3992+_rtld_local(%rip)
	jmp	.L342
.L267:
	movzbl	796(%r13), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L335
	testb	$4, _rtld_local_ro(%rip)
	jne	.L270
.L337:
	movb	$1, 799(%r14)
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	cmpl	$0, 8(%rsp)
	je	.L266
	jmp	.L281
.L335:
	movl	992(%r13), %eax
	cmpl	%r9d, %eax
	jbe	.L515
	movq	984(%r13), %rax
	movl	%r9d, %edx
	movq	%r14, 8(%rax,%rdx,8)
	movq	984(%r13), %rax
	addl	$1, %r9d
	movl	%r9d, (%rax)
.L283:
	testb	$64, _rtld_local_ro(%rip)
	je	.L494
	movq	8(%r13), %rcx
	movq	48(%r13), %r8
	cmpb	$0, (%rcx)
	jne	.L284
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L284
	leaq	.LC2(%rip), %rcx
.L284:
	movq	8(%r14), %rsi
	movq	48(%r14), %rdx
	cmpb	$0, (%rsi)
	jne	.L285
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L285
	leaq	.LC2(%rip), %rsi
.L285:
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
.L494:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	cmpl	$0, 8(%rsp)
	je	.L266
	jmp	.L281
.L247:
	movq	984(%r13), %rdx
	testq	%rdx, %rdx
	je	.L252
	cmpq	%rdx, 32(%rsp)
	je	.L253
	movl	(%rdx), %r9d
	testl	%r9d, %r9d
	je	.L252
	cmpq	%rax, 8(%rdx)
	je	.L490
	leal	-1(%r9), %ecx
	addq	$16, %rdx
	leaq	(%rdx,%rcx,8), %rcx
.L255:
	cmpq	%rdx, %rcx
	je	.L252
	addq	$8, %rdx
	cmpq	-8(%rdx), %rax
	jne	.L255
	jmp	.L490
.L305:
	movzbl	4(%rax), %edx
	shrb	$4, %dl
	cmpb	$10, %dl
	je	.L309
	movq	%r14, 80+_rtld_local(%rip)
	jmp	.L307
.L498:
	leaq	__PRETTY_FUNCTION__.10235(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$845, %edx
	call	__GI___assert_fail
.L248:
	movq	1152(%rax), %rax
	jmp	.L250
.L514:
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 640 "dl-lookup.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L246
.L253:
	movq	32(%rsp), %rsi
	movl	(%rsi), %ecx
	cmpl	%r9d, %ecx
	jbe	.L252
	movl	%r9d, %edx
	leaq	8(%rsi,%rdx,8), %rsi
	xorl	%edx, %edx
.L258:
	cmpq	(%rsi,%rdx,8), %rax
	je	.L490
	addq	$1, %rdx
	leal	(%r9,%rdx), %edi
	cmpl	%edi, %ecx
	ja	.L258
	movl	%ecx, %r9d
	jmp	.L252
.L270:
	movq	8(%r13), %rcx
	movq	48(%r14), %rdx
	movq	8(%r14), %rsi
	cmpb	$0, (%rcx)
	jne	.L516
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
.L272:
	cmpl	$0, 16(%rsp)
	jne	.L280
	jmp	.L337
.L345:
	movq	856(%r13), %rcx
	movq	0(%rbp), %r9
	movq	8(%rax), %rdx
	movq	88(%rsp), %rax
	movl	$4, 48(%rsp)
	subq	%rcx, %r9
	movq	856(%rax), %rax
	jmp	.L340
.L515:
	testl	%eax, %eax
	movl	$10, 24(%rsp)
	je	.L275
	addl	%eax, %eax
	movl	%eax, 24(%rsp)
.L275:
	movl	24(%rsp), %eax
	movl	%r9d, 32(%rsp)
	leaq	8(,%rax,8), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rcx
	movl	32(%rsp), %r9d
	je	.L517
	testl	%r9d, %r9d
	movl	%r9d, %r8d
	je	.L282
	leaq	8(%rax), %rdi
	movq	%rax, 32(%rsp)
	movq	984(%r13), %rax
	leaq	0(,%r8,8), %rdx
	movl	%r9d, 40(%rsp)
	movq	%r8, 16(%rsp)
	leaq	8(%rax), %rsi
	call	memcpy@PLT
	movl	40(%rsp), %r9d
	movq	32(%rsp), %rcx
	movq	16(%rsp), %r8
.L282:
	addl	$1, %r9d
	movq	%r14, 8(%rcx,%r8,8)
	movl	%r9d, (%rcx)
	movq	984(%r13), %rdi
	movl	24(%rsp), %eax
	movq	%rcx, 984(%r13)
	testq	%rdi, %rdi
	movl	%eax, 992(%r13)
	je	.L283
	call	_dl_scope_free
	jmp	.L283
.L517:
	testb	$4, _rtld_local_ro(%rip)
	jne	.L518
.L277:
	cmpl	$0, 16(%rsp)
	jne	.L280
	movb	$1, 799(%r14)
	jmp	.L494
.L518:
	cmpb	$0, 799(%r14)
	jne	.L277
	cmpl	$0, 16(%rsp)
	je	.L278
	cmpb	$0, 800(%r14)
	jne	.L280
.L278:
	movq	48(%r14), %rdx
	movq	8(%r14), %rsi
	leaq	.LC22(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L277
.L309:
	movq	928(%r13), %r11
	movq	$0, 112(%rsp)
	xorl	%eax, %eax
	movq	$0, 120(%rsp)
	movl	8(%r11), %edx
.L310:
	cmpq	%rdx, %rax
	leaq	1(%rax), %r10
	jnb	.L311
	movq	(%r11), %rsi
	movq	(%rsi,%rax,8), %rsi
	movq	%r10, %rax
	cmpq	%rsi, %rcx
	jne	.L310
.L311:
	leaq	0(,%r10,8), %rax
	movq	%rax, 8(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 24(%rsp)
.L313:
	cmpq	%rdx, %r10
	jnb	.L519
	movq	(%r11), %rax
	movq	8(%rsp), %rsi
	movq	(%rax,%rsi), %rax
	cmpq	$0, 192(%rax)
	je	.L314
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	leaq	720(%rax), %r9
	pushq	%r13
	movl	56(%rsp), %esi
	movq	%r15, %rdi
	movq	0(%rbp), %rcx
	pushq	%rsi
	pushq	$0
	movq	%rbx, %rsi
	pushq	$0
	pushq	%r12
	pushq	$0
	movq	72(%rsp), %r8
	movq	64(%rsp), %rdx
	call	do_lookup_x
	addq	$48, %rsp
	testl	%eax, %eax
	jg	.L478
	movq	32(%rsp), %r11
	movq	40(%rsp), %r10
	movl	8(%r11), %edx
.L314:
	addq	$1, %r10
	addq	$8, 8(%rsp)
	jmp	.L313
.L516:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L272
.L519:
	movq	80(%rsp), %rax
	movq	%r14, 80+_rtld_local(%rip)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L307
	movq	656+_rtld_local_ro(%rip), %rdx
	testq	%rdx, %rdx
	sete	%cl
	cmpq	%rdx, %r13
	sete	%dl
	orb	%dl, %cl
	jne	.L339
	jmp	.L497
.L478:
	movq	80(%rsp), %rax
	movdqa	112(%rsp), %xmm0
	movq	%rax, %rsi
	movaps	%xmm0, 96(%rsp)
	jmp	.L488
.LFE73:
	.size	_dl_lookup_symbol_x, .-_dl_lookup_symbol_x
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"(bitmask_nwords & (bitmask_nwords - 1)) == 0"
	.text
	.p2align 4,,15
	.globl	_dl_setup_hash
	.hidden	_dl_setup_hash
	.type	_dl_setup_hash, @function
_dl_setup_hash:
.LFB74:
	movq	672(%rdi), %rax
	testq	%rax, %rax
	je	.L521
	movq	8(%rax), %rcx
	movl	(%rcx), %edx
	movl	%edx, 756(%rdi)
	movl	8(%rcx), %eax
	movl	4(%rcx), %esi
	leal	-1(%rax), %r8d
	testl	%eax, %r8d
	jne	.L530
	movl	%r8d, 760(%rdi)
	leaq	16(%rcx), %r8
	addl	%eax, %eax
	movl	12(%rcx), %ecx
	subq	%rsi, %rdx
	leaq	(%r8,%rax,4), %rax
	movq	%r8, 768(%rdi)
	movq	%rax, 776(%rdi)
	leaq	(%rax,%rdx,4), %rax
	movl	%ecx, 764(%rdi)
	movq	%rax, 784(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L521:
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.L520
	movq	8(%rax), %rax
	movl	(%rax), %edx
	addq	$8, %rax
	movq	%rax, 784(%rdi)
	leaq	(%rax,%rdx,4), %rax
	movl	%edx, 756(%rdi)
	movq	%rax, 776(%rdi)
.L520:
	rep ret
.L530:
	leaq	__PRETTY_FUNCTION__.10260(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	subq	$8, %rsp
	movl	$966, %edx
	call	__GI___assert_fail
.LFE74:
	.size	_dl_setup_hash, .-_dl_setup_hash
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10260, @object
	.size	__PRETTY_FUNCTION__.10260, 15
__PRETTY_FUNCTION__.10260:
	.string	"_dl_setup_hash"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10076, @object
	.size	__PRETTY_FUNCTION__.10076, 17
__PRETTY_FUNCTION__.10076:
	.string	"do_lookup_unique"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10027, @object
	.size	__PRETTY_FUNCTION__.10027, 12
__PRETTY_FUNCTION__.10027:
	.string	"check_match"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10235, @object
	.size	__PRETTY_FUNCTION__.10235, 20
__PRETTY_FUNCTION__.10235:
	.string	"_dl_lookup_symbol_x"
	.hidden	_dl_scope_free
	.hidden	__rtld_malloc
	.hidden	_dl_printf
	.hidden	_dl_debug_printf_c
	.hidden	_dl_signal_cexception
	.hidden	_dl_higher_prime_number
	.hidden	__rtld_free
	.hidden	__rtld_calloc
	.hidden	_rtld_local
	.hidden	_dl_debug_printf
	.hidden	_rtld_local_ro
	.hidden	_dl_name_match_p
	.hidden	strcmp
