	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_dl_addr_inside_object
	.type	_dl_addr_inside_object, @function
_dl_addr_inside_object:
	movzwl	696(%rdi), %edx
	subq	(%rdi), %rsi
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	leaq	-56(,%rax,8), %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$56, %rax
.L2:
	cmpq	$-56, %rax
	je	.L8
	movq	680(%rdi), %rdx
	addq	%rax, %rdx
	cmpl	$1, (%rdx)
	jne	.L3
	movq	%rsi, %rcx
	subq	16(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L3
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.size	_dl_addr_inside_object, .-_dl_addr_inside_object
