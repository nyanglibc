	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-misc.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"pid >= 0 && sizeof (pid_t) <= 4"
	.section	.rodata.str1.1
.LC2:
	.string	"niov < NIOVMAX"
.LC3:
	.string	"! \"invalid format specifier\""
	.text
	.p2align 4,,15
	.type	_dl_debug_vdprintf, @function
_dl_debug_vdprintf:
	pushq	%rbp
	xorl	%r8d, %r8d
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-1084(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdx, %rbx
	xorl	%r12d, %r12d
	leaq	10(%r14), %rax
	movl	%esi, %r13d
	movl	$-1, %r15d
	subq	$1096, %rsp
	movl	%edi, -1132(%rbp)
	movq	%rcx, -1104(%rbp)
	movq	%rax, -1120(%rbp)
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	cmpl	$1, %r13d
	jne	.L3
	testl	%r8d, %r8d
	je	.L122
.L4:
	cmpl	$63, %r12d
	jg	.L123
	movslq	%r12d, %rax
	movl	%r15d, %r13d
	addl	$1, %r12d
	salq	$4, %rax
	movq	$12, -1064(%rbp,%rax)
	movq	%r14, -1072(%rbp,%rax)
	movzbl	(%rbx), %eax
.L3:
	testb	%al, %al
	je	.L59
	cmpb	$37, %al
	je	.L59
	testl	%r13d, %r13d
	movq	%rbx, %rdx
	sete	%cl
	cmpb	$10, %al
	je	.L119
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L11
	cmpb	$37, %al
	je	.L11
	cmpb	$10, %al
	jne	.L97
.L119:
	testb	%cl, %cl
	jne	.L97
.L11:
	cmpl	$63, %r12d
	jg	.L124
	movq	%rdx, %rsi
	movslq	%r12d, %rcx
	subq	%rbx, %rsi
	salq	$4, %rcx
	testq	%rsi, %rsi
	movq	%rsi, -1064(%rbp,%rcx)
	je	.L14
	movq	%rbx, -1072(%rbp,%rcx)
	addl	$1, %r12d
.L14:
	cmpb	$37, %al
	je	.L125
	cmpb	$10, %al
	je	.L126
.L50:
	movq	%rdx, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L55
.L54:
	leaq	-1072(%rbp), %rsi
	movslq	%r12d, %rdx
	movl	-1132(%rbp), %edi
	movl	$20, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/dl-writev.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	1(%rdx), %eax
	cmpb	$48, %al
	je	.L16
	cmpb	$42, %al
	leaq	1(%rdx), %rcx
	movl	$32, %r11d
	movl	%r15d, %r10d
	je	.L127
.L18:
	cmpb	$46, %al
	movl	%r15d, %edx
	je	.L128
.L21:
	cmpb	$108, %al
	je	.L68
	cmpb	$90, %al
	je	.L68
	cmpb	$115, %al
	je	.L66
	jg	.L56
	cmpb	$37, %al
	movq	%rcx, %rbx
	jne	.L129
.L29:
	movslq	%r12d, %rax
	salq	$4, %rax
	movq	%rbx, -1072(%rbp,%rax)
	movq	$1, -1064(%rbp,%rax)
.L117:
	addl	$1, %r12d
.L45:
	leaq	1(%rbx), %rdx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L126:
	cmpq	%rdx, %rbx
	je	.L130
	leal	-1(%r12), %eax
	cltq
	salq	$4, %rax
	addq	$1, -1064(%rbp,%rax)
.L52:
	cmpb	$0, 1(%rdx)
	leaq	1(%rdx), %rbx
	je	.L54
	testl	%r8d, %r8d
	jne	.L4
.L122:
	call	__getpid
	testl	%eax, %eax
	js	.L131
	movq	-1120(%rbp), %rsi
	xorl	%ecx, %ecx
	movslq	%eax, %rdi
	movl	$10, %edx
	movl	%eax, -1096(%rbp)
	call	_itoa_word
	cmpq	%r14, %rax
	movl	-1096(%rbp), %r8d
	jbe	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$1, %rax
	movb	$32, (%rax)
	cmpq	%r14, %rax
	jne	.L7
.L6:
	movl	$2362, %eax
	movw	%ax, -1074(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L68:
	movzbl	1(%rcx), %eax
	leaq	1(%rcx), %rbx
	cmpb	$115, %al
	je	.L27
	jg	.L28
	cmpb	$37, %al
	je	.L29
	cmpb	$100, %al
	jne	.L22
.L30:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %edx
	cmpl	$47, %edx
	ja	.L31
	movl	%edx, %eax
	addq	16(%rdi), %rax
	addl	$8, %edx
	movl	%edx, (%rdi)
.L32:
	movq	(%rax), %rdi
	movzbl	1(%rcx), %eax
	cmpb	$100, %al
	jne	.L34
	movq	%rdi, %rax
	shrq	$63, %rax
	movq	%rax, %r9
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L128:
	cmpb	$42, 1(%rcx)
	je	.L132
.L22:
	leaq	__PRETTY_FUNCTION__.10410(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$243, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	2(%rdx), %eax
	leaq	2(%rdx), %rcx
	movl	$48, %r11d
	movl	%r15d, %r10d
	cmpb	$42, %al
	jne	.L18
.L127:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %edx
	cmpl	$47, %edx
	ja	.L19
	movl	%edx, %eax
	addq	16(%rdi), %rax
	addl	$8, %edx
	movl	%edx, (%rdi)
.L20:
	movl	(%rax), %r10d
	addq	$1, %rcx
	movzbl	(%rcx), %eax
	jmp	.L18
.L66:
	movq	%rcx, %rbx
.L27:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %eax
	cmpl	$47, %eax
	ja	.L46
	movl	%eax, %ecx
	addq	16(%rdi), %rcx
	addl	$8, %eax
	movl	%eax, (%rdi)
.L47:
	movq	(%rcx), %rdi
	movslq	%r12d, %rcx
	movl	%r8d, -1112(%rbp)
	salq	$4, %rcx
	movl	%edx, -1108(%rbp)
	movq	%rcx, -1096(%rbp)
	movq	%rdi, -1072(%rbp,%rcx)
	call	strlen
	movslq	-1108(%rbp), %rdx
	movq	-1096(%rbp), %rcx
	movl	-1112(%rbp), %r8d
	cmpl	$-1, %edx
	je	.L133
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -1064(%rbp,%rcx)
	jmp	.L117
.L59:
	movq	%rbx, %rdx
	jmp	.L11
.L130:
	movslq	%r12d, %rax
	addl	$1, %r12d
	salq	$4, %rax
	movq	%rdx, -1072(%rbp,%rax)
	movq	$1, -1064(%rbp,%rax)
	jmp	.L52
.L133:
	movq	%rax, -1064(%rbp,%rcx)
	jmp	.L117
.L28:
	cmpb	$117, %al
	je	.L30
	cmpb	$120, %al
	je	.L30
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L56:
	cmpb	$117, %al
	je	.L35
	cmpb	$120, %al
	jne	.L22
.L35:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %edx
	cmpl	$47, %edx
	ja	.L36
	movl	%edx, %eax
	addl	$8, %edx
	addq	16(%rdi), %rax
	movl	%edx, (%rdi)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	$100, %al
	je	.L35
	jmp	.L22
.L36:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
.L37:
	movl	(%rax), %edi
	movzbl	(%rcx), %eax
	movq	%rcx, %rbx
	cmpb	$100, %al
	movq	%rdi, %r9
	je	.L134
.L34:
	subq	$48, %rsp
	movl	$16, %edx
	movl	%r8d, -1112(%rbp)
	leaq	15(%rsp), %r9
	movl	%r10d, -1108(%rbp)
	movb	%r11b, -1121(%rbp)
	andq	$-16, %r9
	addq	$25, %r9
	cmpb	$120, %al
	movl	$10, %eax
	cmovne	%eax, %edx
	movq	%r9, %rsi
	xorl	%ecx, %ecx
	movq	%r9, -1096(%rbp)
	call	_itoa_word
	movslq	-1108(%rbp), %r10
	movq	-1096(%rbp), %r9
	movl	-1112(%rbp), %r8d
	movq	%r9, %rcx
	cmpl	$-1, %r10d
	je	.L44
	movq	%r9, %rdx
	xorl	%r9d, %r9d
	movzbl	-1121(%rbp), %r11d
	subq	%rax, %rdx
	cmpq	%r10, %rdx
	jge	.L44
.L58:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L43:
	subq	$1, %rdx
	movq	%rcx, %rax
	movb	%r11b, (%rdx)
	subq	%rdx, %rax
	cmpq	%r10, %rax
	jl	.L43
	testb	%r9b, %r9b
	je	.L64
.L135:
	leaq	-1(%rdx), %rax
	movb	$45, -1(%rdx)
.L44:
	movslq	%r12d, %rdx
	subq	%rax, %rcx
	addl	$1, %r12d
	salq	$4, %rdx
	movq	%rax, -1072(%rbp,%rdx)
	movq	%rcx, -1064(%rbp,%rdx)
	jmp	.L45
.L134:
	shrl	$31, %r9d
.L39:
	subq	$48, %rsp
	xorl	%ecx, %ecx
	movl	$10, %edx
	leaq	15(%rsp), %rax
	movl	%r8d, -1128(%rbp)
	movb	%r9b, -1121(%rbp)
	movl	%r10d, -1112(%rbp)
	movb	%r11b, -1108(%rbp)
	andq	$-16, %rax
	addq	$25, %rax
	movq	%rax, %rsi
	movq	%rax, -1096(%rbp)
	call	_itoa_word
	movslq	-1112(%rbp), %r10
	movq	-1096(%rbp), %rcx
	movzbl	-1108(%rbp), %r11d
	movzbl	-1121(%rbp), %r9d
	movl	-1128(%rbp), %r8d
	cmpl	$-1, %r10d
	je	.L63
	movq	%rcx, %rdx
	subq	%rax, %rdx
	cmpq	%r10, %rdx
	jl	.L58
.L63:
	testb	%r9b, %r9b
	movq	%rax, %rdx
	jne	.L135
.L64:
	movq	%rdx, %rax
	jmp	.L44
.L46:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rdi)
	jmp	.L47
.L124:
	leaq	__PRETTY_FUNCTION__.10410(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$120, %edx
	call	__assert_fail
.L123:
	leaq	__PRETTY_FUNCTION__.10410(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$107, %edx
	call	__assert_fail
.L19:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
	jmp	.L20
.L132:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %edx
	cmpl	$47, %edx
	ja	.L23
	movl	%edx, %eax
	addq	16(%rdi), %rax
	addl	$8, %edx
	movl	%edx, (%rdi)
.L24:
	movl	(%rax), %edx
	addq	$2, %rcx
	movzbl	(%rcx), %eax
	jmp	.L21
.L131:
	leaq	__PRETTY_FUNCTION__.10410(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$98, %edx
	call	__assert_fail
.L23:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
	jmp	.L24
.L31:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 8(%rdi)
	jmp	.L32
	.size	_dl_debug_vdprintf, .-_dl_debug_vdprintf
	.p2align 4,,15
	.globl	_dl_sysdep_read_whole_file
	.hidden	_dl_sysdep_read_whole_file
	.type	_dl_sysdep_read_whole_file, @function
_dl_sysdep_read_whole_file:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movl	$524288, %esi
	movl	%edx, %r13d
	movq	$-1, %rbp
	subq	$152, %rsp
	call	__open64_nocancel
	testl	%eax, %eax
	js	.L136
	movq	%rsp, %rsi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	__fstat64
	testl	%eax, %eax
	js	.L140
	movq	48(%rsp), %rsi
	testq	%rsi, %rsi
	movq	%rsi, (%r12)
	jne	.L147
.L140:
	movq	$-1, %rbp
.L139:
	movl	%ebx, %edi
	call	__close_nocancel
.L136:
	addq	$152, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movl	$2, %ecx
	movl	%r13d, %edx
	xorl	%edi, %edi
	call	__mmap
	movq	%rax, %rbp
	jmp	.L139
	.size	_dl_sysdep_read_whole_file, .-_dl_sysdep_read_whole_file
	.p2align 4,,15
	.globl	_dl_debug_printf
	.hidden	_dl_debug_printf
	.type	_dl_debug_printf, @function
_dl_debug_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L150
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L150:
	leaq	224(%rsp), %rax
	movq	%rdi, %rdx
	leaq	8(%rsp), %rcx
	movl	$1, %esi
	movl	_dl_debug_fd(%rip), %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_debug_printf, .-_dl_debug_printf
	.p2align 4,,15
	.globl	_dl_debug_printf_c
	.hidden	_dl_debug_printf_c
	.type	_dl_debug_printf_c, @function
_dl_debug_printf_c:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L154
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L154:
	leaq	224(%rsp), %rax
	movq	%rdi, %rdx
	leaq	8(%rsp), %rcx
	movl	$-1, %esi
	movl	_dl_debug_fd(%rip), %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_debug_printf_c, .-_dl_debug_printf_c
	.p2align 4,,15
	.globl	_dl_dprintf
	.hidden	_dl_dprintf
	.type	_dl_dprintf, @function
_dl_dprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L158
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L158:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_dprintf, .-_dl_dprintf
	.p2align 4,,15
	.globl	_dl_printf
	.hidden	_dl_printf
	.type	_dl_printf, @function
_dl_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L162
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L162:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movl	$1, %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_printf, .-_dl_printf
	.p2align 4,,15
	.globl	_dl_error_printf
	.hidden	_dl_error_printf
	.type	_dl_error_printf, @function
_dl_error_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L166
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L166:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_error_printf, .-_dl_error_printf
	.p2align 4,,15
	.globl	_dl_fatal_printf
	.type	_dl_fatal_printf, @function
_dl_fatal_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L170
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L170:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	movl	$127, %edi
	call	_exit
	.size	_dl_fatal_printf, .-_dl_fatal_printf
	.p2align 4,,15
	.globl	_dl_name_match_p
	.hidden	_dl_name_match_p
	.type	_dl_name_match_p, @function
_dl_name_match_p:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	call	strcmp
	testl	%eax, %eax
	movl	$1, %edx
	je	.L172
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L174
	.p2align 4,,10
	.p2align 3
.L176:
	xorl	%edx, %edx
.L172:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L176
.L174:
	movq	(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L181
	addq	$8, %rsp
	movl	$1, %edx
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_dl_name_match_p, .-_dl_name_match_p
	.p2align 4,,15
	.globl	_dl_higher_prime_number
	.hidden	_dl_higher_prime_number
	.type	_dl_higher_prime_number, @function
_dl_higher_prime_number:
	leaq	primes.10475(%rip), %rcx
	leaq	120(%rcx), %r8
.L183:
	movq	%r8, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%rcx,%rax,4), %rsi
	movl	(%rsi), %eax
	cmpq	%rax, %rdi
	jbe	.L187
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L185:
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %edx
	cmpq	%rdi, %rdx
	jb	.L188
	movq	%rax, %rsi
.L187:
	cmpq	%rsi, %rcx
	jne	.L185
.L186:
	movl	(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%rsi, %r8
.L184:
	leaq	4(%rax), %rcx
	cmpq	%r8, %rcx
	jne	.L183
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rsi, %rax
	jmp	.L184
	.size	_dl_higher_prime_number, .-_dl_higher_prime_number
	.p2align 4,,15
	.globl	_dl_strtoul
	.hidden	_dl_strtoul
	.type	_dl_strtoul, @function
_dl_strtoul:
	movzbl	(%rdi), %eax
	pushq	%rbx
	cmpb	$32, %al
	jne	.L222
	.p2align 4,,10
	.p2align 3
.L216:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	cmpb	$32, %al
	je	.L216
.L222:
	cmpb	$9, %al
	je	.L216
	cmpb	$45, %al
	je	.L224
	cmpb	$43, %al
	movl	$1, %ebx
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %rdi
.L196:
	movzbl	(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L197
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	je	.L191
.L223:
	movq	%rdi, (%rsi)
.L191:
	movq	%r8, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	cmpb	$48, %al
	movl	$10, %r10d
	movl	$9, %r11d
	je	.L225
.L199:
	xorl	%r8d, %r8d
	addl	$48, %r11d
	movslq	%r10d, %r9
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L227:
	movsbl	%al, %ecx
	cmpl	%r11d, %ecx
	ja	.L200
	subl	$48, %ecx
.L201:
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	notq	%rax
	divq	%r9
	cmpq	%r8, %rax
	jbe	.L226
	imulq	%r9, %r8
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	addq	%rcx, %r8
.L205:
	cmpb	$47, %al
	jg	.L227
.L200:
	cmpl	$16, %r10d
	jne	.L202
	leal	-97(%rax), %edx
	cmpb	$5, %dl
	ja	.L203
	movsbl	%al, %ecx
	subl	$87, %ecx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L203:
	leal	-65(%rax), %edx
	cmpb	$5, %dl
	ja	.L202
	movsbl	%al, %ecx
	subl	$55, %ecx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L224:
	addq	$1, %rdi
	xorl	%ebx, %ebx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L202:
	testq	%rsi, %rsi
	je	.L206
	movq	%rdi, (%rsi)
.L206:
	movq	%r8, %rax
	negq	%rax
	testb	%bl, %bl
	cmove	%rax, %r8
	movq	%r8, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	testq	%rsi, %rsi
	movq	$-1, %r8
	jne	.L223
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L225:
	movzbl	1(%rdi), %edx
	andl	$-33, %edx
	cmpb	$88, %dl
	jne	.L210
	movzbl	2(%rdi), %eax
	movl	$16, %r10d
	addq	$2, %rdi
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L210:
	movl	$8, %r10d
	movl	$7, %r11d
	jmp	.L199
	.size	_dl_strtoul, .-_dl_strtoul
	.section	.rodata
	.align 32
	.type	primes.10475, @object
	.size	primes.10475, 120
primes.10475:
	.long	7
	.long	13
	.long	31
	.long	61
	.long	127
	.long	251
	.long	509
	.long	1021
	.long	2039
	.long	4093
	.long	8191
	.long	16381
	.long	32749
	.long	65521
	.long	131071
	.long	262139
	.long	524287
	.long	1048573
	.long	2097143
	.long	4194301
	.long	8388593
	.long	16777213
	.long	33554393
	.long	67108859
	.long	134217689
	.long	268435399
	.long	536870909
	.long	1073741789
	.long	2147483647
	.long	-5
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10410, @object
	.size	__PRETTY_FUNCTION__.10410, 19
__PRETTY_FUNCTION__.10410:
	.string	"_dl_debug_vdprintf"
	.hidden	strcmp
	.hidden	_exit
	.hidden	__mmap
	.hidden	__close_nocancel
	.hidden	__fstat64
	.hidden	__open64_nocancel
	.hidden	strlen
	.hidden	__assert_fail
	.hidden	_itoa_word
	.hidden	__getpid
