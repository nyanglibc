	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-open.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"new_nlist < ns->_ns_global_scope_alloc"
	.align 8
.LC2:
	.string	"\nadd %s [%lu] to global scope\n"
	.align 8
.LC3:
	.string	"added <= ns->_ns_global_scope_pending_adds"
	.text
	.p2align 4,,15
	.type	add_to_global_update, @function
add_to_global_update:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	_rtld_local(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	48(%rdi), %r15
	leaq	(%r15,%r15,8), %rax
	leaq	(%r15,%rax,2), %r14
	leaq	0(%r13,%r14,8), %rax
	movq	16(%rax), %rdx
	movl	8(%rdx), %r12d
	movl	712(%rdi), %edx
	testl	%edx, %edx
	je	.L2
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L5:
	movq	704(%rbp), %rdx
	movl	%ebx, %eax
	movq	(%rdx,%rax,8), %rcx
	movzbl	796(%rcx), %eax
	testb	$16, %al
	jne	.L3
	orl	$16, %eax
	movb	%al, 796(%rcx)
	leaq	0(%r13,%r14,8), %rax
	cmpl	%r12d, 24(%rax)
	jbe	.L13
	movq	16(%rax), %rax
	movl	%r12d, %edx
	addl	$1, %r12d
	testb	$2, 1+_rtld_local_ro(%rip)
	movq	(%rax), %rax
	movq	%rcx, (%rax,%rdx,8)
	jne	.L14
.L3:
	addl	$1, %ebx
	cmpl	%ebx, 712(%rbp)
	ja	.L5
	leaq	(%r15,%r15,8), %rax
	movl	%r12d, %esi
	leaq	(%r15,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	movq	16(%rax), %rdx
	movl	28(%rax), %eax
	subl	8(%rdx), %esi
	cmpl	%esi, %eax
	movl	%esi, %edx
	jb	.L15
.L6:
	leaq	(%r15,%r15,8), %rcx
	subl	%edx, %eax
	leaq	(%r15,%rcx,2), %rcx
	leaq	0(%r13,%rcx,8), %rcx
	movl	%eax, 28(%rcx)
	movq	16(%rcx), %rax
	movl	%r12d, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	48(%rcx), %rdx
	movq	8(%rcx), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L3
.L2:
	movl	28(%rax), %eax
	jmp	.L6
.L13:
	leaq	__PRETTY_FUNCTION__.10854(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$183, %edx
	call	__GI___assert_fail
.L15:
	leaq	__PRETTY_FUNCTION__.10854(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$197, %edx
	call	__GI___assert_fail
	.size	add_to_global_update, .-add_to_global_update
	.p2align 4,,15
	.type	call_dl_init, @function
call_dl_init:
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rdx
	movl	8(%rdi), %esi
	movq	(%rdi), %rdi
	jmp	_dl_init
	.size	call_dl_init, .-call_dl_init
	.section	.rodata.str1.1
.LC4:
	.string	"cannot extend global scope"
	.section	.text.unlikely,"ax",@progbits
	.type	add_to_global_resize_failure.isra.2, @function
add_to_global_resize_failure.isra.2:
	subq	$8, %rsp
	movq	(%rdi), %rsi
	leaq	.LC4(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error@PLT
	.size	add_to_global_resize_failure.isra.2, .-add_to_global_resize_failure.isra.2
	.text
	.p2align 4,,15
	.type	add_to_global_resize, @function
add_to_global_resize:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	712(%rdi), %eax
	movq	48(%rdi), %rbx
	testl	%eax, %eax
	je	.L20
	movq	704(%rdi), %rdx
	subl	$1, %eax
	leaq	8(%rdx), %rcx
	leaq	(%rcx,%rax,8), %rsi
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$8, %rcx
.L22:
	movq	(%rdx), %rdx
	movzbl	796(%rdx), %edx
	andl	$16, %edx
	cmpb	$1, %dl
	movq	%rcx, %rdx
	adcl	$0, %eax
	cmpq	%rsi, %rcx
	jne	.L46
.L20:
	leaq	(%rbx,%rbx,8), %rdx
	leaq	_rtld_local(%rip), %r12
	leaq	(%rbx,%rdx,2), %rcx
	xorl	%edx, %edx
	addl	28(%r12,%rcx,8), %eax
	leaq	(%rbx,%rbx,8), %rcx
	leaq	(%rbx,%rcx,2), %rcx
	setc	%dl
	leaq	(%r12,%rcx,8), %rcx
	testl	%edx, %edx
	movl	%eax, 28(%rcx)
	jne	.L45
	movq	16(%rcx), %rsi
	movl	%eax, %eax
	movl	8(%rsi), %edx
	addq	%rdx, %rax
	movl	24(%rcx), %edx
	testl	%edx, %edx
	jne	.L26
	addq	$8, %rax
	movl	%eax, %edx
	movl	%eax, %r13d
	cmpq	%rdx, %rax
	jne	.L45
	xorl	%r14d, %r14d
.L29:
	testl	%r13d, %r13d
	je	.L19
	movl	%r13d, %edi
	salq	$3, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L45
	leaq	(%rbx,%rbx,8), %rax
	movq	%rcx, %rdi
	leaq	(%rbx,%rax,2), %rax
	leaq	(%r12,%rax,8), %rbx
	movq	16(%rbx), %rax
	movl	8(%rax), %edx
	movq	(%rax), %rsi
	salq	$3, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	16(%rbx), %rax
	movl	%r13d, 24(%rbx)
	movq	%rcx, (%rax)
#APP
# 158 "dl-open.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L47
.L33:
	popq	%rbx
	movq	%r14, %rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	*__rtld_free(%rip)
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	%rdx, %rax
	jbe	.L19
	addq	%rax, %rax
	movl	%eax, %edx
	movl	%eax, %r13d
	cmpq	%rdx, %rax
	jne	.L45
	movq	(%rsi), %r14
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L19:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	call	__thread_gscope_wait
	jmp	.L33
.L45:
	movq	56(%rbp), %rdi
	call	add_to_global_resize_failure.isra.2
	.size	add_to_global_resize, .-add_to_global_resize
	.section	.rodata.str1.1
.LC5:
	.string	"ns == l->l_ns"
	.text
	.p2align 4,,15
	.globl	__GI__dl_find_dso_for_object
	.hidden	__GI__dl_find_dso_for_object
	.type	__GI__dl_find_dso_for_object, @function
__GI__dl_find_dso_for_object:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$0, 2432+_rtld_local(%rip)
	je	.L49
	leaq	_rtld_local(%rip), %r13
	movq	%rdi, %rbp
	xorl	%r12d, %r12d
.L57:
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	je	.L50
	.p2align 4,,10
	.p2align 3
.L56:
	cmpq	%rbp, 856(%rbx)
	ja	.L51
	cmpq	%rbp, 864(%rbx)
	jbe	.L51
	testb	$64, 797(%rbx)
	jne	.L55
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_dl_addr_inside_object
	testl	%eax, %eax
	jne	.L55
.L51:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L56
.L50:
	addq	$1, %r12
	addq	$152, %r13
	cmpq	%r12, 2432+_rtld_local(%rip)
	ja	.L57
.L49:
	addq	$8, %rsp
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	cmpq	%r12, 48(%rbx)
	jne	.L65
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L65:
	leaq	__PRETTY_FUNCTION__.10864(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$219, %edx
	call	__GI___assert_fail
	.size	__GI__dl_find_dso_for_object, .-__GI__dl_find_dso_for_object
	.globl	_dl_find_dso_for_object
	.set	_dl_find_dso_for_object,__GI__dl_find_dso_for_object
	.section	.rodata.str1.1
.LC6:
	.string	"invalid mode for dlopen()"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"no more namespaces available for dlmopen()"
	.align 8
.LC8:
	.string	"invalid target namespace in dlmopen()"
	.align 8
.LC9:
	.string	"_dl_debug_initialize (0, args.nsid)->r_state == RT_CONSISTENT"
	.text
	.p2align 4,,15
	.globl	_dl_open
	.hidden	_dl_open
	.type	_dl_open, @function
_dl_open:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$136, %rsp
	testb	$3, %sil
	je	.L100
	leaq	_rtld_local(%rip), %r13
	movq	%rcx, %rbx
	movq	%r9, 8(%rsp)
	movl	%esi, %ebp
	movq	%rdx, %r14
	movl	%r8d, %r15d
	leaq	2440(%r13), %rdi
	call	*3984+_rtld_local(%rip)
	cmpq	$-1, %rbx
	movq	8(%rsp), %r9
	je	.L101
	leaq	2(%rbx), %rax
	testq	$-3, %rax
	jne	.L102
.L74:
	movq	%r12, 48(%rsp)
	movq	192(%rsp), %rax
	leaq	16(%rsp), %r12
	leaq	dl_open_worker(%rip), %rsi
	leaq	48(%rsp), %rdx
	movq	%r14, 64(%rsp)
	movq	%r12, %rdi
	movq	%r9, 104(%rsp)
	movl	%ebp, 56(%rsp)
	movq	$0, 72(%rsp)
	movq	%rbx, 80(%rsp)
	movl	%r15d, 96(%rsp)
	movq	%rax, 112(%rsp)
	call	_dl_catch_exception@PLT
	movl	%eax, %r14d
	call	_dl_unload_cache
	movq	80(%rsp), %rsi
	testq	%rsi, %rsi
	js	.L76
	leaq	(%rsi,%rsi,8), %rax
	movl	88(%rsp), %edx
	leaq	(%rsi,%rax,2), %rax
	movl	%edx, 28(%r13,%rax,8)
.L76:
	cmpq	$0, 24(%rsp)
	jne	.L103
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L104
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	movq	72(%rsp), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movq	2432+_rtld_local(%rip), %rdx
	cmpq	$1, %rdx
	jbe	.L84
	cmpq	$0, 152+_rtld_local(%rip)
	je	.L85
	leaq	304(%r13), %rax
	movl	$1, %ebx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$152, %rax
	cmpq	$0, -152(%rax)
	je	.L72
.L71:
	addq	$1, %rbx
	cmpq	%rbx, %rdx
	movq	%rbx, %rcx
	jne	.L73
	cmpq	$16, %rdx
	je	.L83
.L69:
	cmpq	%rcx, %rdx
	jne	.L70
	leaq	(%rbx,%rbx,8), %rax
	pxor	%xmm0, %xmm0
	addq	$1, %rdx
	leaq	(%rbx,%rax,2), %rcx
	leaq	32+_rtld_local(%rip), %rax
	movq	%rdx, 2432+_rtld_local(%rip)
	salq	$3, %rcx
	addq	%rcx, %rax
	movups	%xmm0, 8(%rax)
	movq	$0, 40(%rax)
	movups	%xmm0, 24(%rax)
	movl	$1, 56(%r13,%rcx)
.L70:
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	%r9, 8(%rsp)
	call	_dl_debug_initialize
	movq	8(%rsp), %r9
	movl	$0, 24(%rax)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L103:
	cmpb	$0, 92(%rsp)
	je	.L105
.L78:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L79
	andl	$134217728, %ebp
	je	.L106
.L80:
	movl	$1, %esi
	call	_dl_close_worker
	movq	80(%rsp), %rsi
.L79:
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	24(%rax), %edx
	testl	%edx, %edx
	jne	.L107
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%r14d, %edi
	call	_dl_signal_exception@PLT
	.p2align 4,,10
	.p2align 3
.L102:
	testq	%rbx, %rbx
	js	.L75
	cmpq	%rbx, 2432+_rtld_local(%rip)
	jbe	.L75
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	movl	8(%rax), %ecx
	testl	%ecx, %ecx
	je	.L75
	movq	(%rax), %rax
	testb	$8, 797(%rax)
	je	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	leaq	.LC8(%rip), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$22, %edi
	call	_dl_signal_error@PLT
	.p2align 4,,10
	.p2align 3
.L105:
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rax
	movq	$0, 32(%r13,%rax,8)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L106:
	movb	$1, 4020+_rtld_local(%rip)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L72:
	cmpq	$16, %rbx
	jne	.L70
.L83:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	leaq	.LC7(%rip), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$22, %edi
	call	_dl_signal_error@PLT
.L84:
	movl	$1, %ecx
	movl	$1, %ebx
	jmp	.L69
.L107:
	leaq	__PRETTY_FUNCTION__.11012(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$907, %edx
	call	__GI___assert_fail
.L85:
	movl	$1, %ebx
	jmp	.L70
.L104:
	leaq	__PRETTY_FUNCTION__.11012(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$916, %edx
	call	__GI___assert_fail
.L100:
	leaq	.LC6(%rip), %rcx
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movl	$22, %edi
	call	_dl_signal_error@PLT
	.size	_dl_open, .-_dl_open
	.section	.rodata.str1.1
.LC10:
	.string	"<main program>"
.LC11:
	.string	"<program name unknown>"
.LC12:
	.string	"object=%s [%lu]\n"
.LC13:
	.string	" scope %u:"
.LC14:
	.string	" %s"
.LC15:
	.string	"\n"
.LC16:
	.string	" no scope\n"
	.text
	.p2align 4,,15
	.globl	_dl_show_scope
	.hidden	_dl_show_scope
	.type	_dl_show_scope, @function
_dl_show_scope:
	pushq	%r14
	pushq	%r13
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r14
	pushq	%rbx
	movq	8(%rdi), %rsi
	movq	48(%rdi), %rdx
	cmpb	$0, (%rsi)
	jne	.L109
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC10(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L109:
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	920(%r14), %rax
	testq	%rax, %rax
	je	.L110
	movslq	%r13d, %rdx
	leaq	.LC11(%rip), %r12
	cmpq	$0, (%rax,%rdx,8)
	leaq	0(,%rdx,8), %rbp
	je	.L112
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	movl	%r13d, %esi
	call	_dl_debug_printf
	movq	920(%r14), %rax
	movq	(%rax,%rbp), %rdx
	movl	8(%rdx), %eax
	testl	%eax, %eax
	je	.L113
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rdx), %rdx
	movl	%ebx, %eax
	movq	(%rdx,%rax,8), %rax
	movq	8(%rax), %rsi
	cmpb	$0, (%rsi)
	jne	.L123
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	cmove	%r12, %rsi
.L123:
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	addl	$1, %ebx
	call	_dl_debug_printf_c
	movq	920(%r14), %rax
	movq	(%rax,%rbp), %rdx
	cmpl	%ebx, 8(%rdx)
	ja	.L117
.L113:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	addq	$8, %rbp
	addl	$1, %r13d
	call	_dl_debug_printf_c
	movq	920(%r14), %rax
	cmpq	$0, (%rax,%rbp)
	jne	.L111
.L112:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	jmp	_dl_debug_printf
.L110:
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L112
	.size	_dl_show_scope, .-_dl_show_scope
	.section	.rodata.str1.1
.LC17:
	.string	"mode & RTLD_NOLOAD"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"opening file=%s [%lu]; direct_opencount=%u\n\n"
	.section	.rodata.str1.1
.LC19:
	.string	"marking %s [%lu] as NODELETE\n"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"_dl_debug_initialize (0, args->nsid)->r_state == RT_CONSISTENT"
	.align 8
.LC21:
	.string	"CPU ISA level is lower than required"
	.section	.rodata.str1.1
.LC22:
	.string	"dlopen"
.LC23:
	.string	"cannot create scope list"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"activating NODELETE for %s [%lu]\n"
	.section	.rodata.str1.1
.LC25:
	.string	"cnt + 1 < imap->l_scope_max"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"TLS generation counter wrapped!  Please report this."
	.section	.rodata.str1.1
.LC27:
	.string	"imap->l_need_tls_init == 0"
	.text
	.p2align 4,,15
	.type	dl_open_worker, @function
dl_open_worker:
	pushq	%r15
	pushq	%r14
	movl	$36, %esi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$72, %rsp
	movq	(%rdi), %r15
	movl	8(%rdi), %r12d
	movq	%r15, %rdi
	call	strchr
	testq	%rax, %rax
	je	.L282
.L125:
	movq	16(%rbp), %rdi
	movq	_rtld_local(%rip), %rbx
	call	__GI__dl_find_dso_for_object
	testq	%rax, %rax
	movq	%rax, %r14
	cmove	%rbx, %r14
	movq	32(%rbp), %rbx
	cmpq	$-2, %rbx
	jne	.L126
	movq	48(%r14), %rbx
	movq	%rbx, 32(%rbp)
.L126:
	leaq	(%rbx,%rbx,8), %rax
	leaq	_rtld_local(%rip), %r13
	movq	%rbx, %rsi
	leaq	(%rbx,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	cmpq	$0, 32(%rax)
	movl	28(%rax), %eax
	movl	%eax, 40(%rbp)
	setne	44(%rbp)
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movq	32(%rbp), %r9
	movl	%r12d, %r8d
	xorl	%ecx, %ecx
	orl	$268435456, %r8d
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_dl_map_object
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, 24(%rbp)
	je	.L283
	testl	$1073741824, %r12d
	jne	.L124
	movl	792(%rax), %eax
	movl	%r12d, %r14d
	andl	$4096, %r14d
	cmpq	$0, 704(%rbx)
	leal	1(%rax), %ecx
	movl	%ecx, 792(%rbx)
	jne	.L284
	testl	%r14d, %r14d
	jne	.L285
.L142:
	movl	%r12d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	andl	$-2013265912, %r8d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_map_object_deps
	movl	712(%rbx), %edi
	testl	%edi, %edi
	je	.L143
	xorl	%r14d, %r14d
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L144:
	addl	$1, %r14d
	cmpl	%r14d, 712(%rbx)
	jbe	.L143
.L145:
	movq	704(%rbx), %rdx
	movl	%r14d, %eax
	movq	(%rdx,%rax,8), %rax
	movq	40(%rax), %rdi
	cmpq	$0, 744(%rdi)
	jne	.L144
	xorl	%edx, %edx
	xorl	%esi, %esi
	addl	$1, %r14d
	call	_dl_check_map_versions
	cmpl	%r14d, 712(%rbx)
	ja	.L145
	.p2align 4,,10
	.p2align 3
.L143:
	movl	800+_rtld_local_ro(%rip), %esi
	testl	%esi, %esi
	jne	.L286
.L146:
	movq	32(%rbp), %rsi
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	$0, 24(%rax)
	call	__GI__dl_debug_state
	movl	712(%rbx), %eax
	movl	384+_rtld_local_ro(%rip), %r8d
	leaq	2568+_rtld_local(%rip), %rsi
	leal	-1(%rax), %r15d
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L153:
	movq	976(%rbx), %rcx
	movl	%r15d, %edx
	movq	(%rcx,%rdx,8), %rdx
	testb	$8, 796(%rdx)
	jne	.L152
	cmpq	%rsi, %rdx
	je	.L152
	cmpq	%rsi, 40(%rdx)
	je	.L152
	movl	808(%rdx), %ecx
	movl	%ecx, %edi
	andl	%r8d, %edi
	cmpl	%edi, %ecx
	jne	.L287
.L152:
	subl	$1, %r15d
.L151:
	cmpl	$-1, %r15d
	jne	.L153
	testb	$2, 1+_rtld_local_ro(%rip)
	jne	.L288
.L154:
	movl	68+_rtld_local_ro(%rip), %ecx
	movl	%r12d, %eax
	andl	$134217729, %eax
	movl	%eax, 8(%rsp)
	testl	%ecx, %ecx
	je	.L289
.L156:
	movq	976(%rbx), %r8
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movq	(%r8), %rsi
	.p2align 4,,10
	.p2align 3
.L159:
	movq	40(%rsi), %rsi
	leal	1(%rdi), %edx
	testb	$4, 796(%rsi)
	jne	.L157
	cmpl	$-1, %r15d
	movl	%edx, %ecx
	cmove	%edi, %r15d
.L157:
	movl	%edx, %esi
	movl	%edx, %edi
	movq	(%r8,%rsi,8), %rsi
	testq	%rsi, %rsi
	jne	.L159
	movl	8(%rsp), %eax
	orl	$1, %eax
	movl	%eax, 16(%rsp)
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L165:
	movq	976(%rbx), %rcx
	movl	%r14d, %edx
	movq	(%rcx,%rdx,8), %rdi
	movq	40(%rdi), %rdx
	testb	$4, 796(%rdx)
	jne	.L162
	cmpq	$0, 632+_rtld_local_ro(%rip)
	movq	920(%rdi), %rsi
	jne	.L290
	movl	8(%rsp), %edx
	xorl	%ecx, %ecx
	call	_dl_relocate_object
.L162:
	movl	%r14d, %ecx
.L160:
	cmpl	%ecx, %r15d
	leal	-1(%rcx), %r14d
	jb	.L165
	movl	712(%rbx), %edx
	testl	%edx, %edx
	je	.L212
	xorl	%r15d, %r15d
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L167:
	movl	712(%rbx), %eax
	addl	$1, %r15d
	cmpl	%eax, %r15d
	jnb	.L291
.L176:
	movq	704(%rbx), %rdx
	movl	%r15d, %eax
	movq	(%rdx,%rax,8), %r14
	movzbl	796(%r14), %eax
	andl	$11, %eax
	cmpb	$10, %al
	jne	.L167
	movq	920(%r14), %rsi
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L213
	leaq	704(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L167
	leaq	8(%rsi), %rax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$8, %rax
	cmpq	%rdi, %rdx
	je	.L167
.L169:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L170
	leaq	1(%rdx), %rax
	cmpq	$0, (%rsi,%rax,8)
	je	.L292
	.p2align 4,,10
	.p2align 3
.L214:
	movq	%rax, %rdx
	leaq	1(%rdx), %rax
	cmpq	$0, (%rsi,%rax,8)
	jne	.L214
.L292:
	addq	$2, %rdx
.L168:
	movq	912(%r14), %rdi
	cmpq	%rdx, %rdi
	ja	.L167
	leaq	880(%r14), %rax
	cmpq	$3, %rdi
	movq	%rax, 8(%rsp)
	ja	.L221
	cmpq	%rax, %rsi
	jne	.L215
.L221:
	leaq	(%rdi,%rdi), %rax
	movq	%rdx, 24(%rsp)
	salq	$4, %rdi
	movq	%rax, 16(%rsp)
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L174
	movq	920(%r14), %rsi
	movq	24(%rsp), %rdx
.L172:
	salq	$3, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
	movq	920(%r14), %rdi
	cmpq	%rdi, 8(%rsp)
	movq	%rax, 920(%r14)
	je	.L175
	call	_dl_scope_free
.L175:
	movq	16(%rsp), %rax
	movq	%rax, 912(%r14)
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L184:
	testb	%r15b, %r15b
	jne	.L293
.L194:
	cmpb	$0, 44(%rbp)
	je	.L294
.L204:
	movl	48(%rbp), %eax
	leaq	32(%rsp), %rdx
	leaq	call_dl_init(%rip), %rsi
	xorl	%edi, %edi
	movq	%rbx, 32(%rsp)
	movl	%eax, 40(%rsp)
	movq	56(%rbp), %rax
	movq	%rax, 48(%rsp)
	movq	64(%rbp), %rax
	movq	%rax, 56(%rsp)
	call	_dl_catch_exception@PLT
	testl	%r12d, %r12d
	jne	.L295
.L205:
	testb	$64, _rtld_local_ro(%rip)
	je	.L124
	movl	792(%rbx), %ecx
	movq	48(%rbx), %rdx
	leaq	.LC18(%rip), %rdi
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	call	_dl_debug_printf
.L124:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	movq	32(%rbp), %rbx
	cmpq	$-2, %rbx
	je	.L125
	movl	$47, %esi
	movq	%r15, %rdi
	xorl	%r14d, %r14d
	call	strchr
	testq	%rax, %rax
	jne	.L126
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L291:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	je	.L166
	xorl	%r14d, %r14d
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L177:
	addl	$1, %r14d
	cmpl	712(%rbx), %r14d
	jnb	.L166
.L178:
	movq	704(%rbx), %rdx
	movl	%r14d, %eax
	movq	(%rdx,%rax,8), %rdi
	testb	$8, 796(%rdi)
	jne	.L177
	cmpq	$0, 1088(%rdi)
	je	.L177
	xorl	%esi, %esi
	addl	$1, %r14d
	movl	$1, %r15d
	call	_dl_add_to_slotinfo
	cmpl	712(%rbx), %r14d
	jb	.L178
	.p2align 4,,10
	.p2align 3
.L166:
	andl	$256, %r12d
	jne	.L296
.L179:
	movq	48(%rbx), %rax
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	0(%r13,%rax,8), %r14
	testq	%r14, %r14
	je	.L180
	leaq	.LC24(%rip), %rcx
	.p2align 4,,10
	.p2align 3
.L183:
	cmpb	$0, 800(%r14)
	je	.L181
	testb	$64, _rtld_local_ro(%rip)
	jne	.L297
.L182:
	movb	$1, 799(%r14)
	movb	$0, 800(%r14)
.L181:
	movq	24(%r14), %r14
	testq	%r14, %r14
	jne	.L183
.L180:
	movl	712(%rbx), %ecx
	testl	%ecx, %ecx
	movl	%ecx, %edx
	je	.L184
	xorl	%r14d, %r14d
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L185:
	testb	$2, 1+_rtld_local_ro(%rip)
	jne	.L276
.L280:
	movl	712(%rbx), %ecx
.L189:
	addl	$1, %r14d
	movl	%ecx, %edx
	cmpl	%ecx, %r14d
	jnb	.L184
.L193:
	movq	704(%rbx), %rdx
	movl	%r14d, %eax
	xorl	%esi, %esi
	movq	(%rdx,%rax,8), %rdi
	movzbl	796(%rdi), %eax
	andl	$11, %eax
	cmpb	$10, %al
	jne	.L185
	movq	920(%rdi), %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L218
	leaq	704(%rbx), %rsi
	cmpq	%rax, %rsi
	je	.L189
	leaq	8(%r8), %rax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L190:
	addq	$8, %rax
	cmpq	%rdx, %rsi
	je	.L189
.L188:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L190
	xorl	%eax, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%rsi, %rax
.L191:
	leaq	1(%rax), %rsi
	cmpq	$0, (%r8,%rsi,8)
	leaq	0(,%rsi,8), %rdx
	jne	.L219
	addq	$2, %rax
.L186:
	cmpq	%rax, 912(%rdi)
	jbe	.L298
	movq	$0, (%r8,%rax,8)
	testb	$2, 1+_rtld_local_ro(%rip)
	movq	920(%rdi), %rax
	leaq	704(%rbx), %rcx
	movq	%rcx, (%rax,%rdx)
	je	.L280
	.p2align 4,,10
	.p2align 3
.L276:
	call	_dl_show_scope
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L283:
	andl	$4, %r12d
	jne	.L124
	leaq	__PRETTY_FUNCTION__.10966(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$533, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L289:
	movl	%r12d, %eax
	andl	$134217728, %eax
	movl	%eax, 8(%rsp)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L287:
	movq	8(%rdx), %rsi
	leaq	.LC21(%rip), %rcx
	leaq	.LC22(%rip), %rdx
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
	.p2align 4,,10
	.p2align 3
.L290:
	movq	2536+_rtld_local(%rip), %r9
	movl	16(%rsp), %edx
	movl	$1, %ecx
	movq	%r9, 24(%rsp)
	call	_dl_relocate_object
	movq	24(%rsp), %r9
	testq	%r9, %r9
	jne	.L162
	cmpq	$0, 2536+_rtld_local(%rip)
	je	.L162
	call	_dl_start_profile
	movq	2536+_rtld_local(%rip), %rdx
	movb	$1, 799(%rdx)
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L293:
	movl	712(%rbx), %eax
	testl	%eax, %eax
	je	.L195
	xorl	%r14d, %r14d
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L281:
	movl	712(%rbx), %eax
.L197:
	addl	$1, %r14d
	cmpl	%eax, %r14d
	jnb	.L195
.L200:
	movq	704(%rbx), %rcx
	movl	%r14d, %eax
	movq	(%rcx,%rax,8), %r15
	testb	$8, 796(%r15)
	jne	.L281
	cmpq	$0, 1088(%r15)
	je	.L281
	movl	$1, %esi
	movq	%r15, %rdi
	movl	%edx, 8(%rsp)
	call	_dl_add_to_slotinfo
	testb	$4, 797(%r15)
	movl	8(%rsp), %edx
	movl	712(%rbx), %eax
	je	.L197
	cmpl	%edx, %eax
	cmove	%r14d, %edx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L195:
	addq	$1, 4088+_rtld_local(%rip)
	je	.L201
	cmpl	%edx, 712(%rbx)
	ja	.L202
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L203:
	addl	$1, %edx
	cmpl	712(%rbx), %edx
	jnb	.L194
.L202:
	movq	704(%rbx), %rcx
	movl	%edx, %eax
	movq	(%rcx,%rax,8), %r14
	movzwl	796(%r14), %eax
	andw	$1032, %ax
	cmpw	$1024, %ax
	jne	.L203
	cmpq	$0, 1088(%r14)
	je	.L203
	movl	%edx, 8(%rsp)
	andb	$-5, 797(%r14)
	movq	1120(%r14), %rdi
	call	_dl_update_slotinfo
	movq	%r14, %rdi
	call	*4096+_rtld_local(%rip)
	testb	$4, 797(%r14)
	movl	8(%rsp), %edx
	je	.L203
	leaq	__PRETTY_FUNCTION__.10935(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$430, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rbx, %rdi
	call	add_to_global_resize
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L295:
	movq	%rbx, %rdi
	call	add_to_global_update
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L294:
	movq	32(%rbp), %rax
	xorl	%esi, %esi
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	32(%r13,%rax,8), %rdi
	cmpq	$0, 48(%rdi)
	sete	%sil
	call	_dl_call_libc_early_init
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L297:
	movq	48(%r14), %rdx
	movq	8(%r14), %rsi
	movq	%rcx, %rdi
	xorl	%eax, %eax
	movq	%rcx, 8(%rsp)
	call	_dl_debug_printf
	movq	8(%rsp), %rcx
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L213:
	movl	$1, %edx
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$1, %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L284:
	testb	$64, _rtld_local_ro(%rip)
	jne	.L299
.L133:
	andl	$256, %r12d
	je	.L134
	testb	$16, 796(%rbx)
	je	.L300
	testl	%r14d, %r14d
	je	.L141
.L136:
	testb	$64, _rtld_local_ro(%rip)
	jne	.L207
	movb	$1, 799(%rbx)
.L140:
	movzbl	796(%rbx), %eax
	andl	$16, %eax
	testb	%al, %al
	jne	.L141
	movq	%rbx, %rdi
	call	add_to_global_update
	jmp	.L141
.L285:
	movb	$1, 800(%rbx)
	jmp	.L142
.L286:
	movq	48(%rbx), %rax
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	0(%r13,%rax,8), %r8
	testb	$8, 797(%r8)
	jne	.L146
	movq	792+_rtld_local_ro(%rip), %r15
	xorl	%r14d, %r14d
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L149:
	movq	%r8, 8(%rsp)
	xorl	%esi, %esi
	call	*%rdx
	movq	8(%rsp), %r8
.L147:
	addl	$1, %r14d
	cmpl	%r14d, 800+_rtld_local_ro(%rip)
	movq	64(%r15), %r15
	jbe	.L146
.L150:
	movq	(%r15), %rdx
	testq	%rdx, %rdx
	je	.L147
	movl	%r14d, %ecx
	leaq	2568+_rtld_local(%rip), %rax
	movq	%rcx, %rsi
	salq	$4, %rsi
	cmpq	%rax, %r8
	leaq	1160(%r8,%rsi), %rdi
	jne	.L149
	addq	$233, %rcx
	salq	$4, %rcx
	leaq	0(%r13,%rcx), %rdi
	jmp	.L149
.L134:
	testl	%r14d, %r14d
	jne	.L301
.L141:
	movq	32(%rbp), %rsi
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	24(%rax), %r8d
	testl	%r8d, %r8d
	je	.L124
	leaq	__PRETTY_FUNCTION__.10966(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$573, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L288:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_show_scope
	jmp	.L154
.L212:
	xorl	%r15d, %r15d
	jmp	.L166
.L201:
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L300:
	movq	%rbx, %rdi
	call	add_to_global_resize
	testl	%r14d, %r14d
	je	.L140
	jmp	.L136
.L215:
	movq	8(%rsp), %r9
	movq	$4, 16(%rsp)
	jmp	.L172
.L299:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L133
.L301:
	testb	$64, _rtld_local_ro(%rip)
	jne	.L207
	movb	$1, 799(%rbx)
	jmp	.L141
.L207:
	cmpb	$0, 799(%rbx)
	je	.L302
.L139:
	testl	%r12d, %r12d
	movb	$1, 799(%rbx)
	je	.L141
	jmp	.L140
.L174:
	leaq	.LC23(%rip), %rcx
	leaq	.LC22(%rip), %rsi
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error@PLT
.L298:
	leaq	__PRETTY_FUNCTION__.10911(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$334, %edx
	call	__GI___assert_fail
.L302:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L139
	.size	dl_open_worker, .-dl_open_worker
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10854, @object
	.size	__PRETTY_FUNCTION__.10854, 21
__PRETTY_FUNCTION__.10854:
	.string	"add_to_global_update"
	.align 16
	.type	__PRETTY_FUNCTION__.10935, @object
	.size	__PRETTY_FUNCTION__.10935, 20
__PRETTY_FUNCTION__.10935:
	.string	"update_tls_slotinfo"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10911, @object
	.size	__PRETTY_FUNCTION__.10911, 14
__PRETTY_FUNCTION__.10911:
	.string	"update_scopes"
	.align 8
	.type	__PRETTY_FUNCTION__.10966, @object
	.size	__PRETTY_FUNCTION__.10966, 15
__PRETTY_FUNCTION__.10966:
	.string	"dl_open_worker"
	.align 8
	.type	__PRETTY_FUNCTION__.11012, @object
	.size	__PRETTY_FUNCTION__.11012, 9
__PRETTY_FUNCTION__.11012:
	.string	"_dl_open"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10864, @object
	.size	__PRETTY_FUNCTION__.10864, 24
__PRETTY_FUNCTION__.10864:
	.string	"_dl_find_dso_for_object"
	.hidden	_dl_call_libc_early_init
	.hidden	_dl_update_slotinfo
	.hidden	_dl_start_profile
	.hidden	_dl_add_to_slotinfo
	.hidden	_dl_scope_free
	.hidden	_dl_relocate_object
	.hidden	_dl_check_map_versions
	.hidden	_dl_map_object_deps
	.hidden	_dl_map_object
	.hidden	strchr
	.hidden	_dl_debug_printf_c
	.hidden	_dl_close_worker
	.hidden	_dl_debug_initialize
	.hidden	_dl_unload_cache
	.hidden	_dl_addr_inside_object
	.hidden	__thread_gscope_wait
	.hidden	__rtld_free
	.hidden	__rtld_malloc
	.hidden	_dl_init
	.hidden	_dl_debug_printf
	.hidden	_rtld_local_ro
	.hidden	_rtld_local
