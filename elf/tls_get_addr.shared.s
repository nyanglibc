.globl __tls_get_addr
.type __tls_get_addr,@function
.align 1<<4
__tls_get_addr:
 mov %fs:8, %rdx
 mov 4088 +_rtld_local(%rip), %rax
 cmp %rax, (%rdx)
 jne 1f
 mov 0(%rdi), %rax
 salq $4, %rax
 movq (%rdx,%rax), %rax
 cmp $-1, %rax
 je 1f
 add 8(%rdi), %rax
 ret
1:
 pushq %rbp
 mov %rsp, %rbp
 and $-16, %rsp
 call __tls_get_addr_slow
 mov %rbp, %rsp
 popq %rbp
 ret
.size __tls_get_addr,.-__tls_get_addr
