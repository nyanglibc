	.text
#APP
	
.text
	.align 16
.globl _start
.globl _dl_start_user
_start:
	movq %rsp, %rdi
	call _dl_start
_dl_start_user:
	# Save the user entry point address in %r12.
	movq %rax, %r12
	# See if we were run as a command with the executable file
	# name as an extra leading argument.
	movl _dl_skip_args(%rip), %eax
	# Pop the original argument count.
	popq %rdx
	# Adjust the stack pointer to skip _dl_skip_args words.
	leaq (%rsp,%rax,8), %rsp
	# Subtract _dl_skip_args from argc.
	subl %eax, %edx
	# Push argc back on the stack.
	pushq %rdx
	# Call _dl_init (struct link_map *main_map, int argc, char **argv, char **env)
	# argc -> rsi
	movq %rdx, %rsi
	# Save %rsp value in %r13.
	movq %rsp, %r13
	# And align stack for the _dl_init call. 
	andq $-16, %rsp
	# _dl_loaded -> rdi
	movq _rtld_local(%rip), %rdi
	# env -> rcx
	leaq 16(%r13,%rdx,8), %rcx
	# argv -> rdx
	leaq 8(%r13), %rdx
	# Clear %rbp to mark outermost frame obviously even for constructors.
	xorl %ebp, %ebp
	# Call the function to run the initializers.
	call _dl_init
	# Pass our finalizer function to the user in %rdx, as per ELF ABI.
	leaq _dl_fini(%rip), %rdx
	# And make sure %rsp points to argc stored on the stack.
	movq %r13, %rsp
	# Jump to the user's entry point.
	jmp *%r12
.previous

#NO_APP
	.p2align 4,,15
	.type	rtld_lock_default_lock_recursive, @function
rtld_lock_default_lock_recursive:
	addl	$1, 4(%rdi)
	ret
	.size	rtld_lock_default_lock_recursive, .-rtld_lock_default_lock_recursive
	.p2align 4,,15
	.type	rtld_lock_default_unlock_recursive, @function
rtld_lock_default_unlock_recursive:
	subl	$1, 4(%rdi)
	ret
	.size	rtld_lock_default_unlock_recursive, .-rtld_lock_default_unlock_recursive
	.p2align 4,,15
	.type	notify_audit_modules_of_loaded_object, @function
notify_audit_modules_of_loaded_object:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	800+_rtld_local_ro(%rip), %eax
	movq	792+_rtld_local_ro(%rip), %r14
	testl	%eax, %eax
	je	.L4
	leaq	2568+_rtld_local(%rip), %rbx
	movq	%rdi, %r12
	xorl	%r13d, %r13d
	leaq	-2568(%rbx), %rbp
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	*%rax
	movzbl	797(%r12), %ecx
	movl	%eax, 8(%r15)
	movl	%ecx, %edx
	shrb	$4, %dl
	andl	$1, %edx
	testl	%eax, %eax
	setne	%al
	andl	$-17, %ecx
	orl	%edx, %eax
	sall	$4, %eax
	orl	%ecx, %eax
	movb	%al, 797(%r12)
.L6:
	addl	$1, %r13d
	cmpl	%r13d, 800+_rtld_local_ro(%rip)
	movq	64(%r14), %r14
	jbe	.L4
.L9:
	movq	16(%r14), %rax
	testq	%rax, %rax
	je	.L6
	movl	%r13d, %edx
	movq	%rdx, %rcx
	salq	$4, %rcx
	cmpq	%rbx, %r12
	leaq	1160(%r12,%rcx), %r15
	jne	.L8
	addq	$233, %rdx
	salq	$4, %rdx
	leaq	(%rdx,%rbp), %r15
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	notify_audit_modules_of_loaded_object, .-notify_audit_modules_of_loaded_object
	.p2align 4,,15
	.type	dlmopen_doit, @function
dlmopen_doit:
	movq	__environ@GOTPCREL(%rip), %rax
	pushq	%rbx
	movq	%rdi, %rbx
	movq	__GI__dl_argv(%rip), %r9
	movl	_dl_argc(%rip), %r8d
	leaq	dl_main(%rip), %rdx
	movq	(%rdi), %rdi
	subq	$8, %rsp
	movq	$-1, %rcx
	pushq	(%rax)
	movl	$-1946157055, %esi
	call	_dl_open
	movq	%rax, 8(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	dlmopen_doit, .-dlmopen_doit
	.p2align 4,,15
	.type	lookup_doit, @function
lookup_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	subq	$16, %rsp
	movq	8(%rdi), %rsi
	movq	$0, 16(%rdi)
	movq	(%rdi), %rdi
	leaq	8(%rsp), %rdx
	movq	$0, 8(%rsp)
	pushq	$0
	pushq	$2
	leaq	928(%rsi), %rcx
	call	_dl_lookup_symbol_x
	movq	24(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	testq	%rdx, %rdx
	je	.L17
	cmpw	$-15, 6(%rdx)
	je	.L20
	testq	%rax, %rax
	je	.L20
	movq	(%rax), %rax
.L19:
	addq	8(%rdx), %rax
	movq	%rax, 16(%rbx)
.L17:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	jmp	.L19
	.size	lookup_doit, .-lookup_doit
	.p2align 4,,15
	.type	map_doit, @function
map_doit:
	pushq	%rbx
	movl	16(%rdi), %r8d
	movq	%rdi, %rbx
	movq	(%rbx), %rsi
	movq	8(%rdi), %rdi
	xorl	%edx, %edx
	cmpl	$536870912, %r8d
	setne	%dl
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	call	_dl_map_object
	movq	%rax, 24(%rbx)
	popq	%rbx
	ret
	.size	map_doit, .-map_doit
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: %s cycles (%s%%)\n"
	.text
	.p2align 4,,15
	.type	print_statistics_item, @function
print_statistics_item:
	pushq	%r14
	pushq	%r12
	leaq	__GI__itoa_lower_digits(%rip), %r10
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %r8
	movabsq	$-3689348814741910323, %r11
	subq	$72, %rsp
	leaq	32(%rsp), %rcx
	leaq	25(%rcx), %rbx
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r8, %rax
	subq	$1, %rbx
	mulq	%r11
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %r9
	addq	%r9, %r9
	subq	%r9, %r8
	testq	%rdx, %rdx
	movzbl	(%r10,%r8), %eax
	movq	%rdx, %r8
	movb	%al, (%rbx)
	jne	.L34
	leaq	25(%rcx), %rax
	movl	$25, %edx
	movq	%rsp, %r11
	movq	%r11, %r12
	subq	%rbx, %rax
	cmpq	$25, %rax
	cmova	%rdx, %rax
	movq	%rbx, %rdx
	cmpl	$8, %eax
	movl	%eax, %r9d
	jnb	.L63
.L35:
	xorl	%r8d, %r8d
	testb	$4, %r9b
	jne	.L64
	testb	$2, %r9b
	jne	.L65
.L39:
	andl	$1, %r9d
	jne	.L66
.L40:
	imulq	$1000, %rsi, %rsi
	movb	$0, -1(%rsp,%rax)
	xorl	%edx, %edx
	leaq	26(%rcx), %r8
	movabsq	$-3689348814741910323, %rbx
	movq	%rsi, %rax
	divq	%rbp
	movq	%rax, %rsi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r9, %r8
.L41:
	movq	%rsi, %rax
	leaq	-1(%r8), %r9
	mulq	%rbx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movzbl	(%r10,%rsi), %eax
	movq	%rdx, %rsi
	movb	%al, -1(%r8)
	jne	.L46
	leaq	26(%rcx), %rdx
	subq	%r9, %rdx
	cmpq	$2, %rdx
	je	.L49
	cmpq	$3, %rdx
	je	.L44
	cmpq	$1, %rdx
	movq	%rcx, %rax
	je	.L67
.L42:
	movq	%rdi, %rsi
	leaq	.LC0(%rip), %rdi
	movb	$0, (%rax)
	movq	%r11, %rdx
	xorl	%eax, %eax
	call	_dl_debug_printf
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movzbl	(%rdx,%r8), %edx
	movb	%dl, (%r12,%r8)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L65:
	movzwl	(%rdx,%r8), %ebx
	movw	%bx, (%r12,%r8)
	addq	$2, %r8
	andl	$1, %r9d
	je	.L40
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L64:
	movl	(%rdx), %r8d
	testb	$2, %r9b
	movl	%r8d, (%r12)
	movl	$4, %r8d
	je	.L39
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rcx, %rdx
.L45:
	movb	$46, (%rdx)
	movzbl	(%r9), %esi
	leaq	2(%rdx), %rax
	movb	%sil, 1(%rdx)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%eax, %r12d
	xorl	%edx, %edx
	andl	$-8, %r12d
.L36:
	movl	%edx, %r8d
	addl	$8, %edx
	movq	(%rbx,%r8), %r14
	cmpl	%r12d, %edx
	movq	%r14, (%r11,%r8)
	jb	.L36
	leaq	(%r11,%rdx), %r12
	addq	%rbx, %rdx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L44:
	movb	%al, 32(%rsp)
	movzbl	(%r8), %eax
	leaq	1(%rcx), %rsi
	movq	%r8, %r9
.L43:
	addq	$1, %r9
	leaq	1(%rsi), %rdx
	movb	%al, (%rsi)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rcx, %rsi
	jmp	.L43
	.size	print_statistics_item, .-print_statistics_item
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"\nruntime linker statistics:\n  total startup time in dynamic loader: %s cycles\n"
	.align 8
.LC2:
	.string	"            time needed for relocation"
	.align 8
.LC3:
	.string	"                 number of relocations: %lu\n      number of relocations from cache: %lu\n        number of relative relocations: %lu\n"
	.align 8
.LC4:
	.string	"           time needed to load objects"
	.text
	.p2align 4,,15
	.type	print_statistics, @function
print_statistics:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	leaq	__GI__itoa_lower_digits(%rip), %rsi
	movabsq	$-3689348814741910323, %rcx
	subq	$72, %rsp
	movq	(%rdi), %rbx
	leaq	32(%rsp), %r8
	leaq	25(%r8), %rdi
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rbx, %rax
	subq	$1, %rdi
	mulq	%rcx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rbx
	testq	%rdx, %rdx
	movzbl	(%rsi,%rbx), %eax
	movq	%rdx, %rbx
	movb	%al, (%rdi)
	jne	.L69
	leaq	25(%r8), %rax
	movl	$25, %edx
	movq	%rsp, %rsi
	movq	%rsi, %r8
	subq	%rdi, %rax
	cmpq	$25, %rax
	cmova	%rdx, %rax
	movq	%rdi, %rdx
	cmpl	$8, %eax
	movl	%eax, %ecx
	jnb	.L112
.L70:
	xorl	%edi, %edi
	testb	$4, %cl
	jne	.L113
	testb	$2, %cl
	jne	.L114
.L74:
	andl	$1, %ecx
	jne	.L115
.L75:
	leaq	.LC1(%rip), %rdi
	movb	$0, -1(%rsp,%rax)
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	0(%rbp), %rdx
	movq	relocate_time(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	print_statistics_item
	movq	2432+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L83
	leaq	(%rax,%rax,8), %rdx
	leaq	_rtld_local(%rip), %rdi
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rdi,%rax,8), %r8
	.p2align 4,,10
	.p2align 3
.L82:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L77
	movl	712(%rax), %ecx
	testl	%ecx, %ecx
	je	.L77
	movq	704(%rax), %rdx
	subl	$1, %ecx
	leaq	8(%rdx), %rax
	leaq	(%rax,%rcx,8), %rsi
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L116:
	movq	384(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L79
	addq	8(%rcx), %rbx
.L79:
	movq	392(%rdx), %rdx
	testq	%rdx, %rdx
	je	.L80
	addq	8(%rdx), %rbx
.L80:
	cmpq	%rax, %rsi
	movq	%rax, %rdx
	je	.L77
.L117:
	addq	$8, %rax
.L81:
	movq	(%rdx), %rdx
	cmpq	$0, (%rdx)
	jne	.L116
	cmpq	$0, 576(%rdx)
	je	.L79
	cmpq	%rax, %rsi
	movq	%rax, %rdx
	jne	.L117
.L77:
	addq	$152, %rdi
	cmpq	%rdi, %r8
	jne	.L82
.L76:
	movq	2552+_rtld_local(%rip), %rdx
	movq	2544+_rtld_local(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movq	%rbx, %rcx
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	0(%rbp), %rdx
	movq	load_time(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	print_statistics_item
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	movzbl	(%rdx,%rdi), %edx
	movb	%dl, (%r8,%rdi)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L114:
	movzwl	(%rdx,%rdi), %r9d
	movw	%r9w, (%r8,%rdi)
	addq	$2, %rdi
	andl	$1, %ecx
	je	.L75
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L113:
	movl	(%rdx), %edi
	testb	$2, %cl
	movl	%edi, (%r8)
	movl	$4, %edi
	je	.L74
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L112:
	movl	%eax, %r9d
	xorl	%edx, %edx
	andl	$-8, %r9d
.L71:
	movl	%edx, %r8d
	addl	$8, %edx
	movq	(%rdi,%r8), %r10
	cmpl	%r9d, %edx
	movq	%r10, (%rsi,%r8)
	jb	.L71
	leaq	(%rsi,%rdx), %r8
	addq	%rdi, %rdx
	jmp	.L70
.L83:
	xorl	%ebx, %ebx
	jmp	.L76
	.size	print_statistics, .-print_statistics
	.p2align 4,,15
	.type	relocate_doit, @function
relocate_doit:
	movq	(%rdi), %rax
	movl	8(%rdi), %edx
	xorl	%ecx, %ecx
	movq	920(%rax), %rsi
	movq	%rax, %rdi
	jmp	_dl_relocate_object
	.size	relocate_doit, .-relocate_doit
	.section	.rodata.str1.1
.LC5:
	.string	"<program name unknown>"
.LC6:
	.string	"%s\t(%s)\n"
	.text
	.p2align 4,,15
	.type	print_unresolved, @function
print_unresolved:
	cmpb	$0, (%rsi)
	movq	%rdx, %rax
	jne	.L120
	movq	__GI__dl_argv(%rip), %rdx
	movq	(%rdx), %rsi
	leaq	.LC5(%rip), %rdx
	testq	%rsi, %rsi
	cmove	%rdx, %rsi
.L120:
	leaq	.LC6(%rip), %rdi
	movq	%rsi, %rdx
	movq	%rax, %rsi
	xorl	%eax, %eax
	jmp	_dl_error_printf
	.size	print_unresolved, .-print_unresolved
	.section	.rodata.str1.1
.LC7:
	.string	"%s: %s: %s\n"
	.text
	.p2align 4,,15
	.type	print_missing_version, @function
print_missing_version:
	movq	__GI__dl_argv(%rip), %rcx
	movq	%rsi, %rax
	leaq	.LC7(%rip), %rdi
	movq	(%rcx), %rsi
	leaq	.LC5(%rip), %rcx
	testq	%rsi, %rsi
	cmove	%rcx, %rsi
	movq	%rdx, %rcx
	movq	%rax, %rdx
	xorl	%eax, %eax
	jmp	_dl_error_printf
	.size	print_missing_version, .-print_missing_version
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"ERROR: ld.so: object '%s' from %s cannot be preloaded (%s): ignored.\n"
	.text
	.p2align 4,,15
	.type	do_preload, @function
do_preload:
	pushq	%r12
	pushq	%rbp
	leaq	map_doit(%rip), %rcx
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rdi, %rbx
	subq	$64, %rsp
	movl	8+_rtld_local(%rip), %ebp
	movq	%rdi, 32(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	15(%rsp), %rdx
	leaq	24(%rsp), %rsi
	leaq	16(%rsp), %rdi
	leaq	32(%rsp), %r8
	movq	$0, 24(%rsp)
	movl	$67108864, 48(%rsp)
	call	_dl_catch_error@PLT
	movq	24(%rsp), %rcx
	testq	%rcx, %rcx
	jne	.L127
	xorl	%eax, %eax
	cmpl	%ebp, 8+_rtld_local(%rip)
	setne	%al
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	.LC8(%rip), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	addq	$64, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	do_preload, .-do_preload
	.p2align 4,,15
	.type	dso_name_valid_for_suid, @function
dso_name_valid_for_suid:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	__GI___libc_enable_secure(%rip), %eax
	testl	%eax, %eax
	jne	.L134
.L129:
	cmpb	$0, (%rbx)
	setne	%bpl
.L128:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	call	strlen
	xorl	%ebp, %ebp
	cmpq	$254, %rax
	ja	.L128
	movq	%rax, %rdx
	movl	$47, %esi
	movq	%rbx, %rdi
	call	memchr
	testq	%rax, %rax
	jne	.L128
	jmp	.L129
	.size	dso_name_valid_for_suid, .-dso_name_valid_for_suid
	.section	.rodata.str1.1
.LC9:
	.string	"rtld.c"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"GL(dl_ns)[LM_ID_BASE + 1]._ns_loaded == NULL"
	.section	.rodata.str1.1
.LC11:
	.string	"i == GL(dl_tls_max_dtv_idx)"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"cannot allocate TLS data structures for initial thread\n"
	.align 8
.LC13:
	.string	"cannot set %fs base address for thread-local storage"
	.align 8
.LC14:
	.string	"cannot set up thread-local storage: %s\n"
	.text
	.p2align 4,,15
	.type	init_tls, @function
init_tls:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$0, 4080+_rtld_local(%rip)
	movq	4024+_rtld_local(%rip), %rbp
	movq	%rbp, 4040+_rtld_local(%rip)
	jne	.L144
	movq	%rdi, %rbx
	leaq	64(%rbp), %rdi
	movl	$1, %esi
	addq	$63, %rbp
	salq	$4, %rdi
	call	*__rtld_calloc(%rip)
	cmpq	$0, 152+_rtld_local(%rip)
	movq	%rax, 4032+_rtld_local(%rip)
	leaq	16(%rax), %rdi
	movq	%rbp, (%rax)
	movq	$0, 8(%rax)
	jne	.L148
	movq	_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L145
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L140:
	cmpq	$0, 1088(%rax)
	movq	%rdx, %rcx
	je	.L139
	salq	$4, %rdx
	addl	$1, %esi
	movq	%rax, 8(%rdi,%rdx)
	movslq	%esi, %rdx
	movq	%rdx, %rcx
.L139:
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L140
.L138:
	cmpq	%rcx, 4024+_rtld_local(%rip)
	jne	.L149
	movq	%rbx, %rdi
	call	_dl_tls_static_surplus_init
	call	_dl_determine_tlsoffset
	call	_dl_allocate_tls_storage
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L150
	movq	8(%rax), %rax
	movl	$4098, %edi
	movq	%rax, 4080+_rtld_local(%rip)
	movq	%rsi, (%rsi)
	movl	$158, %eax
	movq	%rsi, 16(%rsi)
#APP
# 804 "rtld.c" 1
	syscall
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L151
	movq	%fs:16, %rax
	movq	4128+_rtld_local(%rip), %rcx
	leaq	4128+_rtld_local(%rip), %rbx
	leaq	704(%rax), %rdx
	movq	%rcx, 704(%rax)
	movq	%rbx, 712(%rax)
	movq	4128+_rtld_local(%rip), %rax
	movq	%rdx, 8(%rax)
	movb	$1, tls_init_tp_called(%rip)
	movq	%rdx, 4128+_rtld_local(%rip)
.L135:
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%ecx, %ecx
	jmp	.L138
.L144:
	xorl	%esi, %esi
	jmp	.L135
.L148:
	leaq	__PRETTY_FUNCTION__.10944(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$769, %edx
	call	__GI___assert_fail
.L150:
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L149:
	leaq	__PRETTY_FUNCTION__.10944(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$781, %edx
	call	__GI___assert_fail
	.size	init_tls, .-init_tls
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"GL(dl_ns)[ns]._ns_loaded == NULL"
	.align 8
.LC16:
	.string	"GL(dl_ns)[ns]._ns_nloaded == 0"
	.text
	.p2align 4,,15
	.type	unload_audit_module, @function
unload_audit_module:
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rbp
	subq	$8, %rsp
	movq	48(%rdi), %rbx
	call	_dl_close
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rdx
	leaq	_rtld_local(%rip), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpq	$0, (%rax)
	jne	.L156
	movl	8(%rax), %eax
	testl	%eax, %eax
	jne	.L157
	movq	%rbp, 4024+_rtld_local(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L156:
	leaq	__PRETTY_FUNCTION__.11009(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$938, %edx
	call	__GI___assert_fail
.L157:
	leaq	__PRETTY_FUNCTION__.11009(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$939, %edx
	call	__GI___assert_fail
	.size	unload_audit_module, .-unload_audit_module
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"Fatal glibc error: Too many audit modules requested\n"
	.text
	.p2align 4,,15
	.type	audit_list_add_string.part.1, @function
audit_list_add_string.part.1:
	movq	128(%rdi), %rax
	cmpq	$16, %rax
	je	.L164
	leaq	1(%rax), %rdx
	cmpq	$1, %rdx
	movq	%rdx, 128(%rdi)
	movq	%rsi, (%rdi,%rax,8)
	je	.L165
	rep ret
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%rsi, 144(%rdi)
	ret
.L164:
	leaq	.LC17(%rip), %rdi
	subq	$8, %rsp
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.size	audit_list_add_string.part.1, .-audit_list_add_string.part.1
	.p2align 4,,15
	.type	version_check_doit, @function
version_check_doit:
	pushq	%rbx
	movq	%rdi, %rbx
	movl	4(%rdi), %edx
	movq	_rtld_local(%rip), %rdi
	movl	$1, %esi
	call	_dl_check_all_versions
	testl	%eax, %eax
	je	.L166
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L172
.L166:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	popq	%rbx
	movl	$1, %edi
	jmp	_exit
	.size	version_check_doit, .-version_check_doit
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"Valid options for the LD_DEBUG environment variable are:\n\n"
	.section	.rodata.str1.1
.LC19:
	.string	"         "
.LC20:
	.string	"  %.*s%s%s\n"
	.section	.rodata.str1.8
	.align 8
.LC21:
	.string	"\nTo direct the debugging output into a file instead of standard output\na filename can be specified using the LD_DEBUG_OUTPUT environment variable.\n"
	.align 8
.LC22:
	.string	"warning: debug option `%s' unknown; try LD_DEBUG=help\n"
	.text
	.p2align 4,,15
	.type	process_dl_debug.isra.3, @function
process_dl_debug.isra.3:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movabsq	$288247972632723457, %r13
	pushq	%rbx
	movabsq	$288247972632723456, %r12
	movq	%rsi, %rbx
	subq	$40, %rsp
	movzbl	(%rsi), %eax
	movq	%rdi, -72(%rbp)
	testb	%al, %al
	je	.L203
	.p2align 4,,10
	.p2align 3
.L186:
	movl	%eax, %edx
	leaq	1(%rbx), %r14
	movzbl	1(%rbx), %eax
	cmpb	$58, %dl
	jbe	.L175
.L176:
	cmpb	$58, %al
	ja	.L178
	btq	%rax, %r13
	movl	$1, %r8d
	jc	.L179
.L178:
	leaq	2(%rbx), %rax
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L180:
	movzbl	(%rax), %edx
	addq	$1, %r8
	movq	%rax, %r14
	addq	$1, %rax
	cmpb	$58, %dl
	ja	.L180
	btq	%rdx, %r13
	jnc	.L180
.L179:
	leaq	1+debopts.11288(%rip), %r15
	movl	$4, %eax
	xorl	%ecx, %ecx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L183:
	addq	$1, %rcx
	addq	$54, %r15
	cmpq	$11, %rcx
	je	.L184
	movzbl	-1(%r15), %eax
.L185:
	cmpq	%r8, %rax
	jne	.L183
	movq	%r8, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rcx, -64(%rbp)
	movq	%r8, -56(%rbp)
	call	memcmp@PLT
	testl	%eax, %eax
	movq	-56(%rbp), %r8
	movq	-64(%rbp), %rcx
	jne	.L183
	leaq	(%rcx,%rcx,2), %rax
	leaq	debopts.11288(%rip), %rdi
	leaq	(%rax,%rax,8), %rax
	movzwl	52(%rdi,%rax,2), %eax
	orl	%eax, _rtld_local_ro(%rip)
	movq	-72(%rbp), %rax
	movb	$1, (%rax)
	movzbl	(%r14), %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L175:
	btq	%rdx, %r12
	jnc	.L176
.L177:
	testb	%al, %al
	movq	%r14, %rbx
	jne	.L186
.L203:
	movl	_rtld_local_ro(%rip), %eax
	testb	$1, %ah
	je	.L187
	movl	$0, 68+_rtld_local_ro(%rip)
.L187:
	testb	$4, %ah
	jne	.L204
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	movq	%r8, %rsi
	movq	%rbx, %rdi
	call	strnlen
	leaq	31(%rax), %rdx
	movq	%rbx, %rsi
	andq	$-16, %rdx
	subq	%rdx, %rsp
	movq	%rax, %rdx
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movb	$0, (%rdi,%rax)
	call	memcpy@PLT
	leaq	.LC22(%rip), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	movzbl	(%r14), %eax
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L204:
	leaq	.LC18(%rip), %rdi
	leaq	1+debopts.11288(%rip), %rbx
	xorl	%eax, %eax
	leaq	.LC19(%rip), %r13
	leaq	.LC20(%rip), %r12
	call	_dl_printf
	leaq	594(%rbx), %r14
	movl	$4, %esi
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L205:
	movzbl	-1(%rbx), %esi
.L190:
	movzbl	%sil, %eax
	leaq	10(%rbx), %r8
	movq	%rbx, %rdx
	leaq	-3(%r13,%rax), %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$54, %rbx
	call	_dl_printf
	cmpq	%rbx, %r14
	jne	.L205
	leaq	.LC21(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	xorl	%edi, %edi
	call	_exit
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.size	process_dl_debug.isra.3, .-process_dl_debug.isra.3
	.section	.rodata.str1.1
.LC23:
	.string	":"
	.text
	.p2align 4,,15
	.type	audit_list_next.part.5, @function
audit_list_next.part.5:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	leaq	152(%rdi), %r12
	pushq	%rbx
	movq	144(%rdi), %rbx
	movq	%rdi, %rbp
	.p2align 4,,10
	.p2align 3
.L207:
	cmpb	$0, (%rbx)
	je	.L210
.L217:
	leaq	.LC23(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r12, %r13
	call	strcspn@PLT
	movq	%rax, %r14
	leaq	-1(%rax), %rax
	cmpq	$253, %rax
	jbe	.L216
	movb	$0, 152(%rbp)
.L212:
	addq	%r14, %rbx
	movq	%rbx, 144(%rbp)
	cmpb	$58, (%rbx)
	jne	.L213
	addq	$1, %rbx
	movq	%rbx, 144(%rbp)
.L213:
	movq	%r12, %rdi
	call	dso_name_valid_for_suid
	testb	%al, %al
	jne	.L206
	movq	144(%rbp), %rbx
	cmpb	$0, (%rbx)
	jne	.L217
.L210:
	movq	136(%rbp), %rax
	addq	$1, %rax
	cmpq	128(%rbp), %rax
	movq	%rax, 136(%rbp)
	je	.L218
	movq	0(%rbp,%rax,8), %rbx
	movq	%rbx, 144(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movb	$0, 152(%rbp,%r14)
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L218:
	movq	$0, 144(%rbp)
	xorl	%r13d, %r13d
.L206:
	popq	%rbx
	movq	%r13, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	audit_list_next.part.5, .-audit_list_next.part.5
	.section	.rodata.str1.1
.LC24:
	.string	"get-dynamic-info.h"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"info[DT_PLTREL]->d_un.d_val == DT_RELA"
	.align 8
.LC26:
	.string	"info[DT_RELAENT]->d_un.d_val == sizeof (ElfW(Rela))"
	.align 8
.LC27:
	.string	"info[VERSYMIDX (DT_FLAGS_1)] == NULL || (info[VERSYMIDX (DT_FLAGS_1)]->d_un.d_val & ~DF_1_NOW) == 0"
	.align 8
.LC28:
	.string	"info[DT_FLAGS] == NULL || (info[DT_FLAGS]->d_un.d_val & ~DF_BIND_NOW) == 0"
	.section	.rodata.str1.1
.LC29:
	.string	"info[DT_RUNPATH] == NULL"
.LC30:
	.string	"info[DT_RPATH] == NULL"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"../sysdeps/x86_64/dl-machine.h"
	.align 8
.LC32:
	.string	"ELFW(R_TYPE) (reloc->r_info) == R_X86_64_RELATIVE"
	.section	.rodata.str1.1
.LC33:
	.string	"do-rel.h"
	.section	.rodata.str1.8
	.align 8
.LC34:
	.string	"map->l_info[VERSYMIDX (DT_VERSYM)] != NULL"
	.text
	.p2align 4,,15
	.type	_dl_start, @function
_dl_start:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	leaq	_DYNAMIC(%rip), %rdx
	movq	%rax, start_time(%rip)
	movq	_DYNAMIC(%rip), %rax
	movq	%rdx, %r14
	subq	_GLOBAL_OFFSET_TABLE_(%rip), %r14
	movq	%rdx, 2584+_rtld_local(%rip)
	testq	%rax, %rax
	movq	%r14, 2568+_rtld_local(%rip)
	je	.L220
	leaq	2632+_rtld_local(%rip), %rcx
	movl	$1879048191, %edi
	movl	$1879047679, %r10d
	movl	$1879047935, %r12d
	movl	$1879048001, %r13d
	movl	$1879047733, %r11d
	movl	$50, %r9d
	movl	$1879048226, %r8d
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L342:
	movq	%r8, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
.L221:
	movq	%rdx, (%rcx,%rax,8)
.L225:
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L220
.L226:
	cmpq	$34, %rax
	jbe	.L221
	movq	%rdi, %rsi
	subq	%rax, %rsi
	cmpq	$15, %rsi
	jbe	.L342
	leal	(%rax,%rax), %esi
	sarl	%esi
	cmpl	$-4, %esi
	jbe	.L223
	movl	%r9d, %eax
	subl	%esi, %eax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L220:
	testq	%r14, %r14
	je	.L228
	movq	2664+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L229
	addq	%r14, 8(%rax)
.L229:
	movq	2656+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L230
	addq	%r14, 8(%rax)
.L230:
	movq	2672+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L231
	addq	%r14, 8(%rax)
.L231:
	movq	2680+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L232
	addq	%r14, 8(%rax)
.L232:
	movq	2688+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L233
	addq	%r14, 8(%rax)
.L233:
	movq	2816+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L234
	addq	%r14, 8(%rax)
.L234:
	movq	3032+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L235
	addq	%r14, 8(%rax)
.L235:
	movq	3240+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L228
	addq	%r14, 8(%rax)
.L228:
	movq	2792+_rtld_local(%rip), %rdx
	testq	%rdx, %rdx
	je	.L237
	cmpq	$7, 8(%rdx)
	jne	.L343
.L237:
	movq	2688+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L238
	movq	2704+_rtld_local(%rip), %rcx
	cmpq	$24, 8(%rcx)
	jne	.L344
.L238:
	movq	2944+_rtld_local(%rip), %rcx
	testq	%rcx, %rcx
	je	.L239
	testq	$-2, 8(%rcx)
	jne	.L345
.L239:
	movq	2872+_rtld_local(%rip), %rcx
	testq	%rcx, %rcx
	je	.L240
	testq	$-9, 8(%rcx)
	jne	.L346
.L240:
	cmpq	$0, 2864+_rtld_local(%rip)
	jne	.L347
	cmpq	$0, 2752+_rtld_local(%rip)
	jne	.L348
	testq	%r14, %r14
	jne	.L243
	cmpq	$0, 3144+_rtld_local(%rip)
	je	.L243
.L244:
	orb	$4, 3364+_rtld_local(%rip)
	call	__rtld_malloc_init_stubs
	rdtsc
	leaq	2568+_rtld_local(%rip), %rdi
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, start_time(%rip)
	call	_dl_setup_hash
	leaq	2568+_rtld_local(%rip), %rax
	leaq	dl_main(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rbp, __GI___libc_stack_end(%rip)
	movq	%rax, 2608+_rtld_local(%rip)
	leaq	_begin(%rip), %rax
	movq	%rax, 3424+_rtld_local(%rip)
	leaq	_end(%rip), %rax
	movq	%rax, 3432+_rtld_local(%rip)
	leaq	_etext(%rip), %rax
	movq	%rax, 3440+_rtld_local(%rip)
	call	_dl_sysdep_start
	testb	$-128, _rtld_local_ro(%rip)
	movq	%rax, %rbx
	jne	.L349
.L219:
	addq	$72, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L243:
	testq	%rax, %rax
	je	.L266
	movq	8(%rax), %rdi
	movq	2696+_rtld_local(%rip), %rax
	movq	2960+_rtld_local(%rip), %r8
	movq	8(%rax), %rcx
	movq	%rdi, %r13
	movq	%rdi, %rax
	testq	%r8, %r8
	leaq	(%rdi,%rcx), %rsi
	je	.L245
	movq	8(%r8), %r8
	leaq	(%r8,%r8,2), %r8
	leaq	(%rdi,%r8,8), %r13
.L245:
	testq	%rdx, %rdx
	je	.L246
	movq	2816+_rtld_local(%rip), %r8
	movq	2648+_rtld_local(%rip), %rdx
	movq	8(%r8), %r9
	movq	8(%rdx), %rdx
	movq	%rcx, %r8
	addq	%rdx, %r9
	subq	%rdx, %r8
	cmpq	%rsi, %r9
	leaq	(%rdx,%rdi), %rsi
	cmove	%r8, %rcx
	addq	%rcx, %rsi
.L246:
	movq	2680+_rtld_local(%rip), %rdx
	cmpq	%rax, %r13
	movq	8(%rdx), %rdi
	jbe	.L254
	.p2align 4,,10
	.p2align 3
.L253:
	movq	(%rax), %rcx
	addq	%r14, %rcx
	cmpl	$8, 8(%rax)
	jne	.L350
	movq	16(%rax), %rdx
	addq	$24, %rax
	addq	%r14, %rdx
	cmpq	%r13, %rax
	movq	%rdx, (%rcx)
	jb	.L253
.L254:
	cmpq	$0, 3032+_rtld_local(%rip)
	je	.L351
	cmpq	%r13, %rsi
	leaq	.L260(%rip), %r8
	leaq	_dl_tlsdesc_return(%rip), %r9
	jbe	.L244
	.p2align 4,,10
	.p2align 3
.L255:
	movq	8(%r13), %rax
	movq	0(%r13), %rcx
	movq	%rax, %rdx
	addq	%r14, %rcx
	shrq	$32, %rdx
	andl	$4294967295, %eax
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rax, %r12
	leaq	(%rdi,%rdx,8), %r15
	je	.L256
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L257
	movzwl	6(%r15), %r10d
	movzbl	4(%r15), %edx
	movq	8(%r15), %rax
	andl	$15, %edx
	cmpw	$-15, %r10w
	je	.L258
	addq	2568+_rtld_local(%rip), %rax
	cmpb	$10, %dl
	je	.L352
.L257:
	leaq	-6(%r12), %rdx
	cmpq	$30, %rdx
	ja	.L256
	movslq	(%r8,%rdx,4), %rdx
	addq	%r8, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L260:
	.long	.L259-.L260
	.long	.L259-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L261-.L260
	.long	.L256-.L260
	.long	.L262-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L256-.L260
	.long	.L263-.L260
	.text
	.p2align 4,,10
	.p2align 3
.L223:
	movq	%r10, %rsi
	subq	%rax, %rsi
	cmpq	$11, %rsi
	jbe	.L353
	movq	%r12, %rsi
	subq	%rax, %rsi
	cmpq	$10, %rsi
	ja	.L225
	movq	%r13, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L262:
	movq	16(%r13), %rax
	subq	3680+_rtld_local(%rip), %rax
	addq	8(%r15), %rax
	movq	%rax, (%rcx)
	.p2align 4,,10
	.p2align 3
.L256:
	addq	$24, %r13
	cmpq	%r13, %rsi
	ja	.L255
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L259:
	addq	16(%r13), %rax
	movq	%rax, (%rcx)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L263:
	movq	16(%r13), %rax
	subq	3680+_rtld_local(%rip), %rax
	addq	8(%r15), %rax
	movq	%rax, 8(%rcx)
	movq	%r9, (%rcx)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L261:
	movq	$1, (%rcx)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L353:
	movq	%r11, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L258:
	cmpb	$10, %dl
	jne	.L257
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r9, -104(%rbp)
	movq	%r8, -96(%rbp)
	movq	%rsi, -88(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%rdi, -72(%rbp)
	call	*%rax
	movq	-104(%rbp), %r9
	movq	-96(%rbp), %r8
	movq	-88(%rbp), %rsi
	movq	-80(%rbp), %rcx
	movq	-72(%rbp), %rdi
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L352:
	testw	%r10w, %r10w
	jne	.L265
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L266:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L349:
	rdtsc
	salq	$32, %rdx
	leaq	-56(%rbp), %rdi
	orq	%rdx, %rax
	subq	start_time(%rip), %rax
	movq	%rax, -56(%rbp)
	call	print_statistics
	jmp	.L219
.L350:
	leaq	__PRETTY_FUNCTION__.10780(%rip), %rcx
	leaq	.LC31(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$546, %edx
	call	__GI___assert_fail
.L348:
	leaq	__PRETTY_FUNCTION__.10838(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$142, %edx
	call	__GI___assert_fail
.L347:
	leaq	__PRETTY_FUNCTION__.10838(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$141, %edx
	call	__GI___assert_fail
.L346:
	leaq	__PRETTY_FUNCTION__.10838(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$138, %edx
	call	__GI___assert_fail
.L345:
	leaq	__PRETTY_FUNCTION__.10838(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$135, %edx
	call	__GI___assert_fail
.L344:
	leaq	__PRETTY_FUNCTION__.10838(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$126, %edx
	call	__GI___assert_fail
.L343:
	leaq	__PRETTY_FUNCTION__.10838(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$118, %edx
	call	__GI___assert_fail
.L351:
	leaq	__PRETTY_FUNCTION__.10855(%rip), %rcx
	leaq	.LC33(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$116, %edx
	call	__GI___assert_fail
	.size	_dl_start, .-_dl_start
	.section	.rodata.str1.1
.LC35:
	.string	" :"
	.text
	.p2align 4,,15
	.globl	handle_preload_list
	.type	handle_preload_list, @function
handle_preload_list:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbp
	pushq	%rbx
	xorl	%r13d, %r13d
	movq	%rdi, %rbx
	subq	$4104, %rsp
	movq	%rsp, %r12
.L355:
	cmpb	$0, (%rbx)
	je	.L365
.L360:
	leaq	.LC35(%rip), %rsi
	movq	%rbx, %rdi
	call	strcspn@PLT
	movq	%rax, %rbp
	leaq	-1(%rax), %rax
	cmpq	$4094, %rax
	jbe	.L366
	movb	$0, (%rsp)
.L357:
	addq	%rbp, %rbx
	movq	%r12, %rdi
	cmpb	$1, (%rbx)
	sbbq	$-1, %rbx
	call	dso_name_valid_for_suid
	testb	%al, %al
	je	.L355
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	do_preload
	addl	%eax, %r13d
	cmpb	$0, (%rbx)
	jne	.L360
.L365:
	addq	$4104, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L366:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movb	$0, (%rsp,%rbp)
	jmp	.L357
	.size	handle_preload_list, .-handle_preload_list
	.section	.rodata
.LC36:
	.string	"/var/tmp"
	.string	"/var/profile"
	.section	.rodata.str1.1
.LC37:
	.string	"<main program>"
.LC38:
	.string	"not found"
.LC39:
	.string	"[WEAK] "
.LC40:
	.string	""
.LC41:
	.string	"ok"
.LC42:
	.string	"failed"
.LC43:
	.string	"DEBUG"
.LC44:
	.string	"AUDIT"
.LC45:
	.string	"VERBOSE"
.LC46:
	.string	"PRELOAD"
.LC47:
	.string	"PROFILE"
.LC48:
	.string	"SHOW_AUXV"
.LC49:
	.string	"ORIGIN_PATH"
.LC50:
	.string	"LIBRARY_PATH"
.LC51:
	.string	"LD_LIBRARY_PATH"
.LC52:
	.string	"DEBUG_OUTPUT"
.LC53:
	.string	"DYNAMIC_WEAK"
.LC54:
	.string	"ASSUME_KERNEL"
.LC55:
	.string	"USE_LOAD_BIAS"
.LC56:
	.string	"PROFILE_OUTPUT"
.LC57:
	.string	"TRACE_PRELINKING"
.LC58:
	.string	"TRACE_LOADED_OBJECTS"
.LC59:
	.string	"PREFER_MAP_32BIT_EXEC"
.LC60:
	.string	"/etc/suid-debug"
.LC61:
	.string	"--list"
.LC62:
	.string	"--verify"
.LC63:
	.string	"--inhibit-cache"
.LC64:
	.string	"--library-path"
.LC65:
	.string	"--inhibit-rpath"
.LC66:
	.string	"--audit"
.LC67:
	.string	"--preload"
.LC68:
	.string	"--argv0"
.LC69:
	.string	"--glibc-hwcaps-prepend"
.LC70:
	.string	"--glibc-hwcaps-mask"
.LC71:
	.string	"--list-tunables"
.LC72:
	.string	"--help"
.LC73:
	.string	"--version"
.LC74:
	.string	"loader cannot load itself\n"
.LC75:
	.string	"main_map != NULL"
	.section	.rodata.str1.8
	.align 8
.LC76:
	.string	"main_map == GL(dl_ns)[LM_ID_BASE]._ns_loaded"
	.section	.rodata.str1.1
.LC77:
	.string	"GL(dl_rtld_map).l_libname"
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"GL(dl_rtld_map).l_libname->next == NULL"
	.section	.rodata.str1.1
.LC79:
	.string	"GL(dl_rtld_map).l_relocated"
	.section	.rodata.str1.8
	.align 8
.LC80:
	.string	"\nWARNING: Unsupported flag value(s) of 0x%x in DT_FLAGS_1.\n"
	.section	.rodata.str1.1
.LC81:
	.string	"libc.so.6"
.LC82:
	.string	"./setup-vdso.h"
.LC83:
	.string	"ph->p_type != PT_TLS"
.LC84:
	.string	"out of memory\n"
.LC85:
	.string	"l->l_next == NULL"
.LC86:
	.string	"l->l_prev == main_map"
.LC87:
	.string	"LINUX_2.6"
.LC88:
	.string	"__vdso_clock_gettime"
.LC89:
	.string	"__vdso_gettimeofday"
.LC90:
	.string	"__vdso_time"
.LC91:
	.string	"__vdso_getcpu"
.LC92:
	.string	"__vdso_clock_getres"
.LC93:
	.string	"FATAL: kernel too old\n"
	.section	.rodata.str1.8
	.align 8
.LC94:
	.string	"FATAL: cannot determine kernel version\n"
	.align 8
.LC95:
	.string	"rtld_ehdr->e_ehsize == sizeof *rtld_ehdr"
	.align 8
.LC96:
	.string	"rtld_ehdr->e_phentsize == sizeof (ElfW(Phdr))"
	.section	.rodata.str1.1
.LC97:
	.string	"list->current_index == 0"
	.section	.rodata.str1.8
	.align 8
.LC98:
	.string	"ERROR: ld.so: object '%s' cannot be loaded as audit interface: %s; ignored.\n"
	.section	.rodata.str1.1
.LC99:
	.string	"la_version"
.LC100:
	.string	"laversion != NULL"
	.section	.rodata.str1.8
	.align 8
.LC101:
	.string	"file=%s [%lu]; audit interface function la_version returned zero; ignored.\n"
	.align 8
.LC102:
	.string	"ERROR: audit interface '%s' requires version %d (maximum supported version %d); ignored.\n"
	.align 8
.LC103:
	.string	"Out of memory while loading audit modules\n"
	.section	.rodata.str1.1
.LC104:
	.string	"cnt == naudit_ifaces"
.LC105:
	.string	"GLRO(dl_naudit) <= naudit"
.LC106:
	.string	"*first_preload == NULL"
.LC107:
	.string	"LD_PRELOAD"
.LC108:
	.string	": \t\n"
.LC109:
	.string	"i == npreloads"
	.section	.rodata.str1.8
	.align 8
.LC110:
	.string	"GL(dl_rtld_map).l_prev->l_next == GL(dl_rtld_map).l_next"
	.align 8
.LC111:
	.string	"GL(dl_rtld_map).l_next->l_prev == GL(dl_rtld_map).l_prev"
	.section	.rodata.str1.1
.LC112:
	.string	"\t%s => not found\n"
.LC113:
	.string	"\t%s => %s (0x%0*Zx, 0x%0*Zx)"
.LC114:
	.string	" TLS(0x%Zx, 0x%0*Zx)\n"
.LC115:
	.string	"\n"
.LC116:
	.string	"Unused direct dependencies:\n"
.LC117:
	.string	"\t%s\n"
.LC118:
	.string	"\tstatically linked\n"
.LC119:
	.string	"\t%s (0x%0*Zx)\n"
.LC120:
	.string	"\t%s => %s (0x%0*Zx)\n"
	.section	.rodata.str1.8
	.align 8
.LC121:
	.string	"%s found at 0x%0*Zd in object at 0x%0*Zd\n"
	.section	.rodata.str1.1
.LC122:
	.string	"\n\tVersion information:\n"
.LC123:
	.string	"\t%s:\n"
.LC124:
	.string	"\t\t%s (%s) %s=> %s\n"
	.section	.rodata.str1.8
	.align 8
.LC125:
	.string	"main_map->l_info[VALIDX (DT_GNU_LIBLISTSZ)] != NULL"
	.section	.rodata.str1.1
.LC126:
	.string	"\nprelink checking: %s\n"
.LC127:
	.string	"\nInitial object scopes\n"
	.section	.rodata.str1.8
	.align 8
.LC128:
	.string	"%s: CPU ISA level is lower than required\n"
	.align 8
.LC129:
	.string	"CPU ISA level is lower than required"
	.section	.rodata.str1.1
.LC130:
	.string	"dlopen"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"main_map->l_info [VALIDX (DT_GNU_CONFLICTSZ)] != NULL"
	.align 8
.LC132:
	.string	"GLRO(dl_init_all_dirs) == GL(dl_all_dirs)"
	.text
	.p2align 4,,15
	.type	dl_main, @function
dl_main:
	pushq	%rbp
	leaq	_dl_nothread_init_static_tls(%rip), %rax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	-512(%rbp), %r14
	pushq	%rbx
	leaq	.L374(%rip), %r12
	movq	%rdi, %rbx
	subq	$664, %rsp
	movq	%rax, 4096+_rtld_local(%rip)
	leaq	rtld_lock_default_lock_recursive(%rip), %rax
	movq	%rdx, -632(%rbp)
	movl	%esi, -624(%rbp)
	movq	%rax, 3984+_rtld_local(%rip)
	leaq	rtld_lock_default_unlock_recursive(%rip), %rax
	movq	%rcx, -640(%rbp)
	movq	$0, -384(%rbp)
	movq	$0, -376(%rbp)
	movq	%rax, 3992+_rtld_local(%rip)
	leaq	4112+_rtld_local(%rip), %rax
	movq	$0, -368(%rbp)
	movq	$0, -104(%rbp)
	movq	$0, -96(%rbp)
	movq	%rax, 4120+_rtld_local(%rip)
	movq	%rax, 4112+_rtld_local(%rip)
	addq	$16, %rax
	movq	%rax, 4136+_rtld_local(%rip)
	movq	%rax, 4128+_rtld_local(%rip)
	leaq	__GI__dl_make_stack_executable(%rip), %rax
	movq	$0, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	%rax, 4008+_rtld_local(%rip)
	movq	_environ(%rip), %rax
	movq	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movl	$0, -56(%rbp)
	movb	$0, -52(%rbp)
	movq	%rax, -576(%rbp)
	movb	$0, -51(%rbp)
	leaq	9+.LC36(%rip), %rax
	movl	__GI___libc_enable_secure(%rip), %r9d
	leaq	-9(%rax), %rdx
	testl	%r9d, %r9d
	cmove	%rdx, %rax
	xorl	%r13d, %r13d
	movq	%rax, 640+_rtld_local_ro(%rip)
	leaq	-576(%rbp), %rax
	movq	%rax, -616(%rbp)
	leaq	460(%r14), %rax
	movq	%rax, -648(%rbp)
	.p2align 4,,10
	.p2align 3
.L369:
	movq	-616(%rbp), %rdi
	call	_dl_next_ld_env_entry
	testq	%rax, %rax
	je	.L431
	movzbl	(%rax), %ecx
	testb	%cl, %cl
	je	.L369
	xorl	%edx, %edx
	cmpb	$61, %cl
	jne	.L370
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L1316:
	cmpb	$61, %cl
	je	.L859
.L370:
	addq	$1, %rdx
	movzbl	(%rax,%rdx), %ecx
	testb	%cl, %cl
	jne	.L1316
.L859:
	cmpb	$61, %cl
	jne	.L369
	subq	$4, %rdx
	cmpq	$17, %rdx
	ja	.L369
	movslq	(%r12,%rdx,4), %rdx
	addq	%r12, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L374:
	.long	.L373-.L374
	.long	.L375-.L374
	.long	.L369-.L374
	.long	.L376-.L374
	.long	.L377-.L374
	.long	.L378-.L374
	.long	.L369-.L374
	.long	.L379-.L374
	.long	.L380-.L374
	.long	.L381-.L374
	.long	.L382-.L374
	.long	.L369-.L374
	.long	.L383-.L374
	.long	.L369-.L374
	.long	.L369-.L374
	.long	.L369-.L374
	.long	.L384-.L374
	.long	.L385-.L374
	.text
	.p2align 4,,10
	.p2align 3
.L431:
	movl	__GI___libc_enable_secure(%rip), %r12d
	movq	%rax, %r15
	testl	%r12d, %r12d
	jne	.L1317
	testq	%r13, %r13
	je	.L437
	cmpb	$0, -52(%rbp)
	jne	.L1318
.L437:
	movq	__GI__dl_argv(%rip), %rax
	movq	-632(%rbp), %rsi
	movq	(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -656(%rbp)
	leaq	_start(%rip), %rax
	cmpq	%rax, (%rsi)
	jne	.L442
	movq	%rdi, 2576+_rtld_local(%rip)
	movl	_dl_argc(%rip), %edx
	leaq	.LC61(%rip), %r12
	movq	$0, -648(%rbp)
	leaq	.LC62(%rip), %r13
	leaq	.LC63(%rip), %r14
	leaq	.LC64(%rip), %r10
	jmp	.L443
	.p2align 4,,10
	.p2align 3
.L1320:
	cmpl	$5, -56(%rbp)
	je	.L1306
	movl	$1, -56(%rbp)
	movl	$-1, 68+_rtld_local_ro(%rip)
.L1306:
	addl	$1, _dl_skip_args(%rip)
	subl	$1, %edx
	addq	$8, %rax
	movl	%edx, _dl_argc(%rip)
	movq	%rax, __GI__dl_argv(%rip)
.L443:
	cmpl	$1, %edx
	jle	.L1319
	movq	__GI__dl_argv(%rip), %rax
	movl	$7, %ecx
	movq	%r12, %rdi
	movq	8(%rax), %r8
	movq	%r8, %rsi
	repz cmpsb
	je	.L1320
	movl	$9, %ecx
	movq	%r8, %rsi
	movq	%r13, %rdi
	repz cmpsb
	jne	.L447
	cmpl	$5, -56(%rbp)
	je	.L1306
	movl	$2, -56(%rbp)
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L447:
	movl	$16, %ecx
	movq	%r8, %rsi
	movq	%r14, %rdi
	repz cmpsb
	jne	.L449
	movl	$1, 32+_rtld_local_ro(%rip)
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L449:
	movl	$15, %ecx
	movq	%r8, %rsi
	movq	%r10, %rdi
	repz cmpsb
	seta	%sil
	setb	%cl
	cmpl	$2, %edx
	setne	%r9b
	cmpb	%cl, %sil
	jne	.L450
	testb	%r9b, %r9b
	jne	.L1321
.L450:
	leaq	.LC65(%rip), %rdi
	movl	$16, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L451
	testb	%r9b, %r9b
	jne	.L1322
.L451:
	leaq	.LC66(%rip), %rdi
	movl	$8, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L452
	testb	%r9b, %r9b
	jne	.L1323
.L452:
	leaq	.LC67(%rip), %rdi
	movl	$10, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L454
	testb	%r9b, %r9b
	jne	.L1324
.L454:
	leaq	.LC68(%rip), %rdi
	movl	$8, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L455
	testb	%r9b, %r9b
	jne	.L1325
.L455:
	leaq	.LC69(%rip), %rdi
	movl	$23, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L456
	testb	%r9b, %r9b
	jne	.L1326
.L456:
	leaq	.LC70(%rip), %rdi
	movl	$20, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L457
	testb	%r9b, %r9b
	jne	.L1327
.L457:
	leaq	.LC71(%rip), %rdi
	movl	$16, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L458
	movl	$4, -56(%rbp)
	jmp	.L1306
	.p2align 4,,10
	.p2align 3
.L1319:
	movl	-56(%rbp), %eax
	cmpl	$4, %eax
	jne	.L811
.L810:
	call	__GI___tunables_print
	xorl	%edi, %edi
	call	_exit
	movl	_dl_argc(%rip), %edx
	cmpl	$1, %edx
	jle	.L1328
	movq	__GI__dl_argv(%rip), %rax
	movl	-56(%rbp), %ecx
.L812:
	movl	-624(%rbp), %esi
	subl	$1, %edx
	addl	$1, _dl_skip_args(%rip)
	movl	%edx, _dl_argc(%rip)
	leaq	8(%rax), %rdx
	movq	%rdx, __GI__dl_argv(%rip)
	leaq	0(,%rsi,8), %rdx
	subq	%rsi, %rdx
	leaq	(%rbx,%rdx,8), %rdx
	cmpq	%rdx, %rbx
	jb	.L469
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L468:
	addq	$56, %rbx
	cmpq	%rdx, %rbx
	jnb	.L466
.L469:
	cmpl	$1685382481, (%rbx)
	jne	.L468
	movl	4(%rbx), %edx
	movl	%edx, 4016+_rtld_local(%rip)
.L466:
	cmpl	$2, %ecx
	je	.L861
	cmpl	$5, %ecx
	je	.L861
	rdtsc
	xorl	%r9d, %r9d
	movl	$536870912, %r8d
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	movq	__GI__dl_argv(%rip), %rax
	salq	$32, %rdx
	orq	%rdx, %rbx
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	(%rax), %rsi
	call	_dl_map_object
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	%rbx, %rax
	movq	%rax, load_time(%rip)
.L474:
	movl	-56(%rbp), %r10d
	movq	_rtld_local(%rip), %r13
	testl	%r10d, %r10d
	jne	.L475
	movq	2744+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L475
	movq	176(%r13), %rdx
	testq	%rdx, %rdx
	je	.L475
	movq	104(%r13), %rcx
	movq	8(%rax), %rdi
	movq	2672+_rtld_local(%rip), %rax
	movq	8(%rdx), %rsi
	addq	8(%rcx), %rsi
	addq	8(%rax), %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L1329
.L475:
	movzwl	696(%r13), %eax
	movq	-632(%rbp), %rdi
	movq	-640(%rbp), %rsi
	movq	680(%r13), %rbx
	movq	%rax, %rdx
	movl	%eax, -624(%rbp)
	leaq	.LC40(%rip), %rax
	movq	%rax, 8(%r13)
	movq	688(%r13), %rax
	movq	%rax, (%rdi)
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L476
	movq	__GI__dl_argv(%rip), %rcx
	jmp	.L483
	.p2align 4,,10
	.p2align 3
.L479:
	cmpq	$9, %rax
	je	.L481
	cmpq	$31, %rax
	jne	.L477
	movq	(%rcx), %rax
	movq	%rax, 8(%rsi)
.L477:
	addq	$16, %rsi
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L476
.L483:
	cmpq	$5, %rax
	je	.L478
	ja	.L479
	cmpq	$3, %rax
	jne	.L477
	movq	%rbx, 8(%rsi)
	addq	$16, %rsi
	movq	(%rsi), %rax
	testq	%rax, %rax
	jne	.L483
	.p2align 4,,10
	.p2align 3
.L476:
	movq	-648(%rbp), %rdi
	movb	$1, -632(%rbp)
	testq	%rdi, %rdi
	je	.L484
	movq	__GI__dl_argv(%rip), %rax
	movq	%rdi, (%rax)
	jmp	.L484
.L1318:
	movq	%r13, %rdi
	movq	%rsp, -648(%rbp)
	call	strlen
	movq	%rax, %r12
	leaq	27(%rax), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movb	$0, 11(%rsp,%r12)
	leaq	11(%rsp,%r12), %r14
	call	__getpid
	leaq	__GI__itoa_lower_digits(%rip), %r8
	movslq	%eax, %rcx
	movabsq	$-3689348814741910323, %rdi
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L820:
	movq	%rsi, %r14
.L439:
	movq	%rcx, %rax
	leaq	-1(%r14), %rsi
	mulq	%rdi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	testq	%rdx, %rdx
	movzbl	(%r8,%rcx), %eax
	movq	%rdx, %rcx
	movb	%al, -1(%r14)
	jne	.L820
	subq	%r12, %r14
	movb	$46, -1(%rsi)
	movq	%r12, %rdx
	leaq	-2(%r14), %rdi
	movq	%r13, %rsi
	call	memcpy@PLT
	movl	$438, %edx
	movq	%rax, %rdi
	movl	$132161, %esi
	xorl	%eax, %eax
	call	__GI___open64_nocancel
	movl	$1, %edx
	cmpl	$-1, %eax
	cmove	%edx, %eax
	movl	%eax, 64+_rtld_local_ro(%rip)
	movq	-648(%rbp), %rsp
	jmp	.L437
.L385:
	movl	__GI___libc_enable_secure(%rip), %r15d
	testl	%r15d, %r15d
	jne	.L369
	movabsq	$6073458355863507009, %rdx
	movabsq	$5575265321193132624, %rcx
	xorq	8(%rax), %rdx
	xorq	(%rax), %rcx
	orq	%rcx, %rdx
	jne	.L369
	cmpl	$1163412831, 16(%rax)
	jne	.L369
	cmpb	$67, 20(%rax)
	jne	.L369
	orl	$512, 380+_rtld_local_ro(%rip)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L384:
	movabsq	$5350926577855448129, %rcx
	movabsq	$5714046778312053332, %rdx
	xorq	8(%rax), %rcx
	xorq	(%rax), %rdx
	orq	%rdx, %rcx
	jne	.L369
	cmpl	$1398031173, 16(%rax)
	jne	.L369
	movl	$3, -56(%rbp)
	jmp	.L369
.L383:
	movabsq	$5138124812661115973, %rcx
	movabsq	$5931345460332679764, %rdx
	xorq	8(%rax), %rcx
	xorq	(%rax), %rdx
	orq	%rdx, %rcx
	jne	.L369
	addq	$17, %rax
	movl	$3, -56(%rbp)
	movl	$1, 60+_rtld_local_ro(%rip)
	orl	$2048, _rtld_local_ro(%rip)
	movq	%rax, 648+_rtld_local_ro(%rip)
	jmp	.L369
.L382:
	movl	__GI___libc_enable_secure(%rip), %edx
	testl	%edx, %edx
	jne	.L369
	movabsq	$6864977084592116304, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpl	$1347704143, 8(%rax)
	jne	.L369
	cmpw	$21589, 12(%rax)
	jne	.L369
	cmpb	$0, 15(%rax)
	je	.L369
	addq	$15, %rax
	movq	%rax, 640+_rtld_local_ro(%rip)
	jmp	.L369
.L381:
	movabsq	$5431135874078430017, %rdx
	cmpq	%rdx, (%rax)
	je	.L1330
.L413:
	movl	__GI___libc_enable_secure(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L369
	movabsq	$4918299457499779925, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpl	$1095320159, 8(%rax)
	jne	.L369
	cmpb	$83, 12(%rax)
	jne	.L369
	cmpb	$49, 14(%rax)
	sete	%al
	movzbl	%al, %eax
	negq	%rax
	movq	%rax, 616+_rtld_local_ro(%rip)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L380:
	movl	__GI___libc_enable_secure(%rip), %esi
	testl	%esi, %esi
	jne	.L404
	movabsq	$6870613147036830028, %rdx
	cmpq	%rdx, (%rax)
	je	.L1331
.L405:
	movabsq	$6147236776361739588, %rdx
	cmpq	%rdx, (%rax)
	je	.L1332
.L408:
	movabsq	$6864410853199731012, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpl	$1262568791, 8(%rax)
	jne	.L369
	movl	$1, 76+_rtld_local_ro(%rip)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L379:
	movl	__GI___libc_enable_secure(%rip), %edi
	testl	%edi, %edi
	jne	.L369
	movabsq	$5791433722457313871, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpw	$21569, 8(%rax)
	jne	.L369
	cmpb	$72, 10(%rax)
	jne	.L369
	addq	$12, %rax
	movq	%rax, 608+_rtld_local_ro(%rip)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L378:
	movl	__GI___libc_enable_secure(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L369
	movabsq	$6365065526100576339, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpb	$86, 8(%rax)
	jne	.L369
	call	_dl_show_auxv
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L377:
	movabsq	$6291333375534713154, %rdx
	cmpq	%rdx, (%rax)
	je	.L1333
	movabsq	$6075160593420929346, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpb	$0, 9(%rax)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 72+_rtld_local_ro(%rip)
	jmp	.L369
.L376:
	cmpl	$1112687958, (%rax)
	je	.L1334
.L391:
	cmpl	$1279611472, (%rax)
	je	.L1335
.L394:
	cmpl	$1179603536, (%rax)
	jne	.L369
	cmpw	$19529, 4(%rax)
	jne	.L369
	cmpb	$69, 6(%rax)
	jne	.L369
	cmpb	$0, 8(%rax)
	je	.L369
	addq	$8, %rax
	movq	%rax, 632+_rtld_local_ro(%rip)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L375:
	cmpl	$1430406468, (%rax)
	je	.L1336
.L386:
	cmpl	$1229215041, (%rax)
	jne	.L369
	cmpb	$84, 4(%rax)
	jne	.L369
	cmpb	$0, 6(%rax)
	je	.L369
	leaq	6(%rax), %rsi
	movq	%r14, %rdi
	call	audit_list_add_string.part.1
	jmp	.L369
.L373:
	cmpl	$1314013527, (%rax)
	jne	.L369
	cmpb	$0, 5(%rax)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 60+_rtld_local_ro(%rip)
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L442:
	leaq	.LC40(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$536870912, %r8d
	movq	%rsi, %rdi
	call	_dl_new_object
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1337
	movq	%rbx, 680(%rax)
	movzwl	-624(%rbp), %eax
	xorl	%esi, %esi
	movq	%r13, %rdi
	movw	%ax, 696(%r13)
	movq	-632(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 688(%r13)
	call	_dl_add_to_namespace_list
	cmpq	%r13, _rtld_local(%rip)
	jne	.L1338
	movb	$0, -632(%rbp)
.L484:
	movl	-624(%rbp), %edx
	addl	$1, 792(%r13)
	movq	$0, 864(%r13)
	movq	$0, 872(%r13)
	movq	$-1, 856(%r13)
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	leaq	(%rbx,%rax,8), %r12
	cmpq	%r12, %rbx
	jnb	.L486
	movq	24+_rtld_local_ro(%rip), %rsi
	movq	%rbx, %rax
	movb	$0, -640(%rbp)
	leaq	_dl_rtld_libname(%rip), %rdi
	leaq	_dl_rtld_libname2(%rip), %r8
	negq	%rsi
	jmp	.L503
	.p2align 4,,10
	.p2align 3
.L1340:
	cmpl	$2, %edx
	je	.L490
	cmpl	$3, %edx
	je	.L491
	cmpl	$1, %edx
	je	.L1339
	.p2align 4,,10
	.p2align 3
.L487:
	addq	$56, %rax
	cmpq	%r12, %rax
	jnb	.L504
.L503:
	movl	(%rax), %edx
	cmpl	$6, %edx
	je	.L488
	jbe	.L1340
	cmpl	$1685382481, %edx
	je	.L493
	cmpl	$1685382482, %edx
	je	.L494
	cmpl	$7, %edx
	jne	.L487
	movq	40(%rax), %rdx
	testq	%rdx, %rdx
	je	.L487
	movq	%rdx, 1088(%r13)
	movq	48(%rax), %rdx
	testq	%rdx, %rdx
	movq	%rdx, 1096(%r13)
	movq	16(%rax), %rcx
	je	.L1341
	subq	$1, %rdx
	andq	%rcx, %rdx
	movq	%rdx, 1104(%r13)
.L502:
	movq	32(%rax), %rdx
	addq	$56, %rax
	movq	%rcx, 1072(%r13)
	cmpq	%r12, %rax
	movq	$1, 1120(%r13)
	movq	%rdx, 1080(%r13)
	movq	$1, 4024+_rtld_local(%rip)
	jb	.L503
	.p2align 4,,10
	.p2align 3
.L504:
	leaq	-56(%r12), %r14
	movabsq	$988218432520154551, %rdx
	movq	%r15, -648(%rbp)
	movq	%r14, %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	imulq	%rdx, %rax
	movabsq	$2305843009213693951, %rdx
	andq	%rdx, %rax
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rdx, %rax
	leaq	(%r12,%rax,8), %rax
	movq	%rax, -624(%rbp)
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L1342:
	cmpl	$1685382483, %eax
	jne	.L506
	movq	%r14, %rdx
	movl	$-1, %esi
	movq	%r13, %rdi
	call	_dl_process_pt_gnu_property@PLT
.L506:
	cmpq	-624(%rbp), %r14
	je	.L1279
	subq	$56, %r14
.L521:
	movl	-56(%r12), %eax
	movq	%r14, %r12
	cmpl	$4, %eax
	jne	.L1342
	testb	$3, 801(%r13)
	jne	.L506
	cmpq	$8, 48(%r14)
	jne	.L506
	movq	0(%r13), %r10
	movq	40(%r14), %rbx
	addq	16(%r14), %r10
	cmpq	$12, %rbx
	movq	%r10, %rcx
	leaq	12(%r10), %rax
	jbe	.L511
	movl	$0, -672(%rbp)
	xorl	%edi, %edi
	xorl	%esi, %esi
	movq	%r14, -664(%rbp)
	jmp	.L520
	.p2align 4,,10
	.p2align 3
.L512:
	movl	4(%rcx), %r8d
.L514:
	addq	$19, %rdx
	andq	$-8, %rdx
	leaq	7(%r8,%rdx), %rax
	andq	$-8, %rax
	addq	%rax, %rcx
	leaq	12(%rcx), %rax
	movq	%rax, %rdx
	subq	%r10, %rdx
	cmpq	%rdx, %rbx
	jbe	.L1343
.L520:
	movl	(%rcx), %edx
	cmpl	$4, %edx
	jne	.L512
	cmpl	$5, 8(%rcx)
	jne	.L512
	cmpl	$5590599, (%rax)
	jne	.L512
	movzbl	801(%r13), %eax
	testb	$3, %al
	jne	.L1294
	andl	$-4, %eax
	orl	$1, %eax
	movb	%al, 801(%r13)
	movl	4(%rcx), %r8d
	cmpl	$7, %r8d
	jbe	.L1294
	testb	$7, %r8b
	jne	.L1294
	leaq	16(%rcx), %rax
	movl	%edx, -680(%rbp)
	movl	%esi, %edx
	leaq	(%rax,%r8), %r12
	jmp	.L519
	.p2align 4,,10
	.p2align 3
.L1346:
	cmpl	$4, %r11d
	jne	.L1294
	cmpl	$-1073741822, %r9d
	movl	8(%rax), %eax
	jne	.L1344
	movl	%eax, %edx
.L518:
	addq	$7, %rsi
	andq	$-8, %rsi
	leaq	(%rdi,%rsi), %rax
	movq	%r12, %rsi
	movl	%r9d, %edi
	subq	%rax, %rsi
	cmpq	$7, %rsi
	jle	.L1345
.L519:
	movl	(%rax), %r9d
	movl	4(%rax), %r11d
	cmpl	%edi, %r9d
	jb	.L1294
	leaq	8(%rax), %rdi
	movl	%r11d, %esi
	leaq	(%rdi,%rsi), %r15
	cmpq	%r15, %r12
	jb	.L1294
	movl	%r9d, %r15d
	andl	$-32769, %r15d
	cmpl	$-1073741822, %r15d
	je	.L1346
	cmpl	$-1073709054, %r9d
	jbe	.L518
	movl	%edx, %esi
	movl	%r9d, %edi
	movl	-680(%rbp), %edx
	jmp	.L514
	.p2align 4,,10
	.p2align 3
.L1279:
	movq	1072(%r13), %rax
	movq	-648(%rbp), %r15
	movq	864(%r13), %rcx
	movq	872(%r13), %rdx
	testq	%rax, %rax
	je	.L522
.L816:
	addq	0(%r13), %rax
	movq	%rax, 1072(%r13)
.L522:
	testq	%rcx, %rcx
	je	.L817
.L523:
	testq	%rdx, %rdx
	jne	.L524
	movq	$-1, 872(%r13)
.L524:
	cmpq	$0, 2624+_rtld_local(%rip)
	je	.L1347
.L525:
	movq	2744+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L527
	movq	8(%rax), %rbx
	movq	2672+_rtld_local(%rip), %rax
	movq	2624+_rtld_local(%rip), %r12
	addq	8(%rax), %rbx
	movq	(%r12), %rdi
	movq	%rbx, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L527
	movq	$0, 8+newname.11121(%rip)
	cmpq	$0, 8(%r12)
	movq	%rbx, newname.11121(%rip)
	movl	$1, 16+newname.11121(%rip)
	jne	.L1348
	leaq	newname.11121(%rip), %rax
	movq	%rax, 8(%r12)
.L527:
	testb	$4, 3364+_rtld_local(%rip)
	je	.L1349
	cmpb	$0, -632(%rbp)
	jne	.L530
	movq	16(%r13), %rdx
	testq	%rdx, %rdx
	je	.L532
	movq	(%rdx), %rax
	leaq	64(%r13), %rcx
	testq	%rax, %rax
	je	.L533
	movl	$1879048191, %edi
	movl	$1879047679, %r10d
	movl	$1879047935, %r11d
	movl	$1879048001, %r12d
	movl	$1879047733, %ebx
	movl	$50, %r9d
	movl	$1879048226, %r8d
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L1350:
	movq	%r8, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
.L534:
	movq	%rdx, (%rcx,%rax,8)
.L538:
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L533
.L539:
	cmpq	$34, %rax
	jbe	.L534
	movq	%rdi, %rsi
	subq	%rax, %rsi
	cmpq	$15, %rsi
	jbe	.L1350
	leal	(%rax,%rax), %esi
	sarl	%esi
	cmpl	$-4, %esi
	jbe	.L536
	movl	%r9d, %eax
	subl	%esi, %eax
	jmp	.L534
.L533:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L541
	movq	96(%r13), %rdx
	testq	%rdx, %rdx
	je	.L542
	addq	%rax, 8(%rdx)
.L542:
	movq	88(%r13), %rdx
	testq	%rdx, %rdx
	je	.L543
	addq	%rax, 8(%rdx)
.L543:
	movq	104(%r13), %rdx
	testq	%rdx, %rdx
	je	.L544
	addq	%rax, 8(%rdx)
.L544:
	movq	112(%r13), %rdx
	testq	%rdx, %rdx
	je	.L545
	addq	%rax, 8(%rdx)
.L545:
	movq	120(%r13), %rdx
	testq	%rdx, %rdx
	je	.L546
	addq	%rax, 8(%rdx)
.L546:
	movq	248(%r13), %rdx
	testq	%rdx, %rdx
	je	.L547
	addq	%rax, 8(%rdx)
.L547:
	movq	464(%r13), %rdx
	testq	%rdx, %rdx
	je	.L548
	addq	%rax, 8(%rdx)
.L548:
	movq	672(%r13), %rdx
	testq	%rdx, %rdx
	je	.L541
	addq	%rax, 8(%rdx)
.L541:
	movq	224(%r13), %rax
	testq	%rax, %rax
	je	.L550
	cmpq	$7, 8(%rax)
	jne	.L596
.L550:
	cmpq	$0, 120(%r13)
	je	.L551
	movq	136(%r13), %rax
	cmpq	$24, 8(%rax)
	jne	.L598
.L551:
	movq	304(%r13), %rax
	testq	%rax, %rax
	je	.L553
	movq	8(%rax), %rdx
	testb	$2, %dl
	movl	%edx, 1008(%r13)
	je	.L554
	movq	%rax, 192(%r13)
.L554:
	testb	$4, %dl
	je	.L555
	movq	%rax, 240(%r13)
.L555:
	andl	$8, %edx
	je	.L553
	movq	%rax, 256(%r13)
.L553:
	movq	376(%r13), %rax
	testq	%rax, %rax
	je	.L558
	movq	8(%rax), %rax
	testb	$8, %al
	movl	%eax, %edx
	movl	%eax, 1004(%r13)
	je	.L559
	movb	$1, 800(%r13)
.L559:
	testb	$64, _rtld_local_ro(%rip)
	je	.L560
	movl	%eax, %esi
	andl	$-134220010, %esi
	je	.L560
	leaq	.LC80(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movl	1004(%r13), %edx
.L560:
	andl	$1, %edx
	je	.L558
	movq	376(%r13), %rax
	movq	%rax, 256(%r13)
.L558:
	cmpq	$0, 296(%r13)
	je	.L532
	movq	$0, 184(%r13)
.L532:
	movq	176(%r13), %rax
	testq	%rax, %rax
	je	.L563
	movq	104(%r13), %rdx
	movq	8(%rax), %rsi
	leaq	.LC81(%rip), %rdi
	movl	$10, %ecx
	addq	8(%rdx), %rsi
	repz cmpsb
	jne	.L563
	movq	%r13, 32+_rtld_local(%rip)
.L563:
	movq	%r13, %rdi
	call	_dl_setup_hash
.L530:
	cmpl	$2, -56(%rbp)
	je	.L1351
.L564:
	cmpq	$0, 672+_rtld_local_ro(%rip)
	je	.L567
	leaq	.LC40(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rsi, %rdi
	call	_dl_new_object
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L567
	movq	672+_rtld_local_ro(%rip), %rcx
	movzwl	56(%rcx), %edi
	movq	32(%rcx), %rax
	addq	%rcx, %rax
	testq	%rdi, %rdi
	movq	%rax, 680(%r14)
	movw	%di, 696(%r14)
	je	.L568
	xorl	%esi, %esi
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L569:
	cmpl	$1, %edx
	je	.L1352
	cmpl	$7, %edx
	je	.L1353
.L570:
	addq	$1, %rsi
	addq	$56, %rax
	cmpq	%rsi, %rdi
	je	.L568
.L575:
	movl	(%rax), %edx
	cmpl	$2, %edx
	jne	.L569
	movq	16(%rax), %rdx
	addq	$1, %rsi
	addq	$56, %rax
	movq	%rdx, 16(%r14)
	movq	-16(%rax), %rdx
	shrq	$4, %rdx
	cmpq	%rsi, %rdi
	movw	%dx, 698(%r14)
	jne	.L575
.L568:
	movq	%rcx, 856(%r14)
	subq	(%r14), %rcx
	movq	16(%r14), %rdx
	addq	%rcx, 864(%r14)
	addq	%rcx, 872(%r14)
	addq	%rcx, %rdx
	movq	%rcx, (%r14)
	testq	%rdx, %rdx
	movq	%rdx, 16(%r14)
	je	.L577
	movq	(%rdx), %rax
	leaq	64(%r14), %rsi
	testq	%rax, %rax
	je	.L578
	movl	$1879048191, %r8d
	movl	$1879047679, %r11d
	movl	$1879047935, %ebx
	movl	$1879047733, %r12d
	movl	$50, %r10d
	movl	$1879048226, %r9d
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L1354:
	movq	%r9, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
.L579:
	movq	%rdx, (%rsi,%rax,8)
.L583:
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L578
.L584:
	cmpq	$34, %rax
	jbe	.L579
	movq	%r8, %rdi
	subq	%rax, %rdi
	cmpq	$15, %rdi
	jbe	.L1354
	leal	(%rax,%rax), %edi
	sarl	%edi
	cmpl	$-4, %edi
	jbe	.L581
	movl	%r10d, %eax
	subl	%edi, %eax
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L1352:
	cmpq	$0, (%r14)
	movq	16(%rax), %rdx
	jne	.L572
	movq	%rdx, (%r14)
.L572:
	addq	40(%rax), %rdx
	cmpq	864(%r14), %rdx
	jb	.L573
	movq	%rdx, 864(%r14)
.L573:
	testb	$1, 4(%rax)
	je	.L570
	cmpq	872(%r14), %rdx
	jb	.L570
	movq	%rdx, 872(%r14)
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L488:
	movq	%rbx, %rdx
	subq	16(%rax), %rdx
	movq	%rdx, 0(%r13)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L1339:
	movq	16(%rax), %rdx
	movq	0(%r13), %r9
	movq	%rdx, %rcx
	andq	%rsi, %rcx
	addq	%r9, %rcx
	cmpq	%rcx, 856(%r13)
	jbe	.L499
	movq	%rcx, 856(%r13)
.L499:
	addq	40(%rax), %rdx
	addq	%r9, %rdx
	cmpq	%rdx, 864(%r13)
	jnb	.L500
	movq	%rdx, 864(%r13)
.L500:
	testb	$1, 4(%rax)
	je	.L487
	cmpq	%rdx, 872(%r13)
	jnb	.L487
	movq	%rdx, 872(%r13)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L491:
	movq	0(%r13), %rdx
	addq	16(%rax), %rdx
	cmpq	$0, 2584+_rtld_local(%rip)
	movq	%rdi, 2624+_rtld_local(%rip)
	movq	%rdx, (%rdi)
	je	.L823
	movb	$1, -640(%rbp)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L490:
	movq	0(%r13), %rdx
	addq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L494:
	movq	16(%rax), %rdx
	movq	%rdx, 1136(%r13)
	movq	40(%rax), %rdx
	movq	%rdx, 1144(%r13)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L493:
	movl	4(%rax), %edx
	movl	%edx, 4016+_rtld_local(%rip)
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L578:
	testq	%rcx, %rcx
	je	.L586
	movq	96(%r14), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L587
	movq	(%rdx), %rax
	movq	%rax, dyn_temp.10982(%rip)
	movq	8(%rdx), %rax
	addq	%rcx, %rax
	movq	%rax, 8+dyn_temp.10982(%rip)
	leaq	dyn_temp.10982(%rip), %rax
	movq	%rax, 96(%r14)
	movl	$1, %eax
.L587:
	movq	88(%r14), %rsi
	testq	%rsi, %rsi
	je	.L588
	movslq	%eax, %rdx
	leaq	dyn_temp.10982(%rip), %rdi
	movq	8(%rsi), %rbx
	salq	$4, %rdx
	addl	$1, %eax
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	addq	%rcx, %rbx
	movq	%rbx, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 88(%r14)
.L588:
	movq	104(%r14), %rsi
	testq	%rsi, %rsi
	je	.L589
	movslq	%eax, %rdx
	leaq	dyn_temp.10982(%rip), %rdi
	movq	8(%rsi), %rbx
	salq	$4, %rdx
	addl	$1, %eax
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	addq	%rcx, %rbx
	movq	%rbx, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 104(%r14)
.L589:
	movq	112(%r14), %rsi
	testq	%rsi, %rsi
	je	.L590
	movslq	%eax, %rdx
	leaq	dyn_temp.10982(%rip), %rdi
	movq	8(%rsi), %rbx
	salq	$4, %rdx
	addl	$1, %eax
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	addq	%rcx, %rbx
	movq	%rbx, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 112(%r14)
.L590:
	movq	120(%r14), %rsi
	testq	%rsi, %rsi
	je	.L591
	movslq	%eax, %rdx
	leaq	dyn_temp.10982(%rip), %rdi
	movq	8(%rsi), %rbx
	salq	$4, %rdx
	addl	$1, %eax
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	addq	%rcx, %rbx
	movq	%rbx, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 120(%r14)
.L591:
	movq	248(%r14), %rsi
	testq	%rsi, %rsi
	je	.L592
	movslq	%eax, %rdx
	leaq	dyn_temp.10982(%rip), %rdi
	movq	8(%rsi), %rbx
	salq	$4, %rdx
	addl	$1, %eax
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	addq	%rcx, %rbx
	movq	%rbx, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 248(%r14)
.L592:
	movq	464(%r14), %rsi
	testq	%rsi, %rsi
	je	.L593
	movslq	%eax, %rdx
	leaq	dyn_temp.10982(%rip), %rdi
	movq	8(%rsi), %rbx
	salq	$4, %rdx
	addl	$1, %eax
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	addq	%rcx, %rbx
	movq	%rbx, 8(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 464(%r14)
.L593:
	movq	672(%r14), %rdx
	testq	%rdx, %rdx
	je	.L586
	leaq	dyn_temp.10982(%rip), %rsi
	cltq
	addq	8(%rdx), %rcx
	salq	$4, %rax
	addq	%rsi, %rax
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rax)
	movq	%rsi, (%rax)
	movq	%rax, 672(%r14)
.L586:
	movq	224(%r14), %rax
	testq	%rax, %rax
	je	.L595
	cmpq	$7, 8(%rax)
	jne	.L596
.L595:
	cmpq	$0, 120(%r14)
	je	.L597
	movq	136(%r14), %rax
	cmpq	$24, 8(%rax)
	jne	.L598
.L597:
	movq	304(%r14), %rax
	testq	%rax, %rax
	je	.L600
	movq	8(%rax), %rdx
	testb	$2, %dl
	movl	%edx, 1008(%r14)
	je	.L601
	movq	%rax, 192(%r14)
.L601:
	testb	$4, %dl
	je	.L602
	movq	%rax, 240(%r14)
.L602:
	andl	$8, %edx
	je	.L600
	movq	%rax, 256(%r14)
.L600:
	movq	376(%r14), %rax
	testq	%rax, %rax
	je	.L605
	movq	8(%rax), %rax
	testb	$8, %al
	movl	%eax, %edx
	movl	%eax, 1004(%r14)
	je	.L606
	movb	$1, 800(%r14)
.L606:
	testb	$64, _rtld_local_ro(%rip)
	jne	.L1355
.L607:
	andl	$1, %edx
	je	.L605
	movq	376(%r14), %rax
	movq	%rax, 256(%r14)
.L605:
	cmpq	$0, 296(%r14)
	je	.L577
	movq	$0, 184(%r14)
.L577:
	movq	%r14, %rdi
	call	_dl_setup_hash
	movq	928(%r14), %rax
	leaq	40(%r14), %rdx
	orb	$4, 796(%r14)
	movl	$1, 996(%r14)
	movl	$1, 8(%rax)
	movq	%rdx, (%rax)
	movq	176(%r14), %rax
	testq	%rax, %rax
	je	.L610
	movq	104(%r14), %rdx
	movq	8(%rax), %rbx
	addq	8(%rdx), %rbx
	movq	%rbx, %rdi
	call	strlen
	leaq	1(%rax), %r12
	movq	%r12, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	je	.L1356
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	%rax, 8(%r14)
	movq	56(%r14), %rax
	movq	%rcx, (%rax)
.L610:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_dl_add_to_namespace_list
	cmpq	$0, 24(%r14)
	jne	.L1357
	cmpq	32(%r14), %r13
	jne	.L1358
	leaq	2568+_rtld_local(%rip), %rax
	movq	%r14, 2592+_rtld_local(%rip)
	movq	%rax, 32(%r14)
	leaq	24(%r14), %rax
	movq	%r14, 680+_rtld_local_ro(%rip)
	movq	%rax, -648(%rbp)
.L614:
	movabsq	$4356732406, %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -536(%rbp)
	movq	-616(%rbp), %rax
	leaq	.LC87(%rip), %r12
	leaq	-544(%rbp), %rbx
	movq	$0, -560(%rbp)
	movq	$0, -528(%rbp)
	movaps	%xmm0, -576(%rbp)
	movq	%rax, -584(%rbp)
	leaq	-584(%rbp), %rax
	movb	$32, -572(%rbp)
	xorl	%r9d, %r9d
	movq	%rbx, %r8
	leaq	928(%r14), %rcx
	movq	%rax, %rdx
	movq	%r14, %rsi
	leaq	.LC88(%rip), %rdi
	movq	%r12, -544(%rbp)
	movq	%rax, -624(%rbp)
	pushq	$0
	pushq	$0
	call	*752+_rtld_local_ro(%rip)
	movq	-584(%rbp), %rdx
	popq	%r8
	popq	%r9
	testq	%rdx, %rdx
	je	.L1359
	cmpw	$-15, 6(%rdx)
	je	.L830
	testq	%rax, %rax
	je	.L830
	movq	(%rax), %rax
.L618:
	addq	8(%rdx), %rax
	movq	680+_rtld_local_ro(%rip), %rsi
.L617:
	testq	%rsi, %rsi
	movq	%rax, 688+_rtld_local_ro(%rip)
	je	.L619
	movabsq	$4356732406, %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -536(%rbp)
	movq	-616(%rbp), %rax
	leaq	928(%rsi), %rcx
	movq	$0, -560(%rbp)
	movq	%r12, -544(%rbp)
	xorl	%r9d, %r9d
	movaps	%xmm0, -576(%rbp)
	movq	$0, -528(%rbp)
	movb	$32, -572(%rbp)
	leaq	.LC89(%rip), %rdi
	movq	%rbx, %r8
	movq	%rax, -584(%rbp)
	movq	-624(%rbp), %rdx
	pushq	$0
	pushq	$0
	call	*752+_rtld_local_ro(%rip)
	movq	-584(%rbp), %rdx
	popq	%rsi
	popq	%rdi
	testq	%rdx, %rdx
	je	.L1360
	cmpw	$-15, 6(%rdx)
	je	.L831
	testq	%rax, %rax
	je	.L831
	movq	(%rax), %rax
.L622:
	addq	8(%rdx), %rax
	movq	680+_rtld_local_ro(%rip), %rsi
.L621:
	testq	%rsi, %rsi
	movq	%rax, 696+_rtld_local_ro(%rip)
	je	.L623
	movabsq	$4356732406, %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -536(%rbp)
	movq	-616(%rbp), %rax
	leaq	928(%rsi), %rcx
	movq	$0, -560(%rbp)
	movq	%r12, -544(%rbp)
	xorl	%r9d, %r9d
	movaps	%xmm0, -576(%rbp)
	movq	$0, -528(%rbp)
	movb	$32, -572(%rbp)
	movq	%rbx, %r8
	leaq	.LC90(%rip), %rdi
	movq	%rax, -584(%rbp)
	movq	-624(%rbp), %rdx
	pushq	$0
	pushq	$0
	call	*752+_rtld_local_ro(%rip)
	movq	-584(%rbp), %rdx
	popq	%r14
	popq	%rcx
	testq	%rdx, %rdx
	je	.L1361
	cmpw	$-15, 6(%rdx)
	je	.L832
	testq	%rax, %rax
	je	.L832
	movq	(%rax), %rax
.L626:
	addq	8(%rdx), %rax
	movq	680+_rtld_local_ro(%rip), %rsi
.L625:
	testq	%rsi, %rsi
	movq	%rax, 704+_rtld_local_ro(%rip)
	je	.L627
	movabsq	$4356732406, %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -536(%rbp)
	movq	-616(%rbp), %rax
	xorl	%r9d, %r9d
	movq	$0, -560(%rbp)
	movq	%r12, -544(%rbp)
	leaq	928(%rsi), %rcx
	movaps	%xmm0, -576(%rbp)
	movq	$0, -528(%rbp)
	movb	$32, -572(%rbp)
	movq	%rbx, %r8
	leaq	.LC91(%rip), %rdi
	movq	%rax, -584(%rbp)
	movq	-624(%rbp), %rdx
	pushq	$0
	pushq	$0
	call	*752+_rtld_local_ro(%rip)
	movq	-584(%rbp), %rdx
	popq	%r10
	popq	%r11
	testq	%rdx, %rdx
	je	.L1362
	cmpw	$-15, 6(%rdx)
	je	.L833
	testq	%rax, %rax
	je	.L833
	movq	(%rax), %rax
.L630:
	addq	8(%rdx), %rax
	movq	680+_rtld_local_ro(%rip), %rsi
.L629:
	testq	%rsi, %rsi
	movq	%rax, 712+_rtld_local_ro(%rip)
	je	.L633
	movabsq	$4356732406, %rax
	pxor	%xmm0, %xmm0
	movq	%rax, -536(%rbp)
	movq	-616(%rbp), %rax
	xorl	%r9d, %r9d
	movq	$0, -560(%rbp)
	movq	%r12, -544(%rbp)
	movq	%rbx, %r8
	movaps	%xmm0, -576(%rbp)
	movq	$0, -528(%rbp)
	movb	$32, -572(%rbp)
	leaq	928(%rsi), %rcx
	leaq	.LC92(%rip), %rdi
	movq	%rax, -584(%rbp)
	movq	-624(%rbp), %rdx
	pushq	$0
	pushq	$0
	call	*752+_rtld_local_ro(%rip)
	movq	-584(%rbp), %rdx
	popq	%r8
	popq	%r9
	testq	%rdx, %rdx
	je	.L633
	cmpw	$-15, 6(%rdx)
	je	.L834
	testq	%rax, %rax
	je	.L834
	movq	(%rax), %rax
.L634:
	addq	8(%rdx), %rax
.L632:
	movq	%rax, 720+_rtld_local_ro(%rip)
	call	_dl_discover_osversion
	testl	%eax, %eax
	js	.L635
	movl	4+_rtld_local_ro(%rip), %edx
	testl	%edx, %edx
	jne	.L1363
.L636:
	movl	%eax, 4+_rtld_local_ro(%rip)
.L637:
	cmpl	$197119, %eax
	jle	.L1364
	movq	-64(%rbp), %rcx
	movq	-72(%rbp), %rdx
	movq	-96(%rbp), %rsi
	movq	-104(%rbp), %rdi
	call	_dl_init_paths
	movq	2568+_rtld_local(%rip), %rdi
	xorl	%esi, %esi
	call	_dl_debug_initialize
	cmpq	$0, 2576+_rtld_local(%rip)
	movq	%rax, -632(%rbp)
	movl	$0, 24(%rax)
	je	.L1365
.L639:
	movzbl	3364+_rtld_local(%rip), %eax
	addl	$1, 8+_rtld_local(%rip)
	andl	$-4, %eax
	orl	$1, %eax
	movb	%al, 3364+_rtld_local(%rip)
	leaq	2568+_rtld_local(%rip), %rax
	movq	%rax, 24(%r13)
	addq	$1, 2520+_rtld_local(%rip)
	cmpq	$-2, 616+_rtld_local_ro(%rip)
	movq	%r13, 2600+_rtld_local(%rip)
	jne	.L640
	xorl	%eax, %eax
	cmpq	$0, 0(%r13)
	sete	%al
	negq	%rax
	movq	%rax, 616+_rtld_local_ro(%rip)
.L640:
	cmpw	$64, 52+__ehdr_start(%rip)
	jne	.L1366
	cmpw	$56, 54+__ehdr_start(%rip)
	jne	.L1367
	movzwl	56+__ehdr_start(%rip), %esi
	leaq	__ehdr_start(%rip), %rax
	addq	32+__ehdr_start(%rip), %rax
	leaq	0(,%rsi,8), %rdx
	movq	%rax, 3248+_rtld_local(%rip)
	movw	%si, 3264+_rtld_local(%rip)
	subq	%rsi, %rdx
	leaq	-56(%rax,%rdx,8), %rdx
	xorl	%eax, %eax
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L645:
	movq	%rdx, %rcx
	addq	$1, %rax
	subq	$56, %rdx
	cmpl	$1685382482, (%rcx)
	je	.L1368
.L643:
	cmpq	%rax, %rsi
	jne	.L645
	cmpq	$0, 3656+_rtld_local(%rip)
	jne	.L1369
.L646:
	movq	616(%r13), %rdx
	movq	104(%r13), %rax
	testq	%rdx, %rdx
	movq	8(%rax), %rax
	je	.L647
	movq	8(%rdx), %rsi
	addq	%rax, %rsi
	cmpb	$0, (%rsi)
	je	.L647
	leaq	-512(%rbp), %rbx
	movq	%rbx, %rdi
	call	audit_list_add_string.part.1
	movq	104(%r13), %rax
	movq	8(%rax), %rax
.L647:
	movq	624(%r13), %rdx
	testq	%rdx, %rdx
	je	.L648
	addq	8(%rdx), %rax
	cmpb	$0, (%rax)
	movq	%rax, %rsi
	je	.L648
	leaq	-512(%rbp), %rbx
	movq	%rbx, %rdi
	call	audit_list_add_string.part.1
.L648:
	cmpl	$5, -56(%rbp)
	je	.L1315
	cmpq	$0, -384(%rbp)
	je	.L835
	movq	-376(%rbp), %r14
	movq	-368(%rbp), %r12
	testq	%r14, %r14
	movq	%r14, -664(%rbp)
	jne	.L651
	testq	%r12, %r12
	je	.L652
	leaq	-512(%rbp), %rbx
	jmp	.L653
	.p2align 4,,10
	.p2align 3
.L654:
	addq	$1, %r14
	cmpq	$0, -368(%rbp)
	je	.L1295
.L653:
	movq	%rbx, %rdi
	call	audit_list_next.part.5
	testq	%rax, %rax
	jne	.L654
.L1295:
	movq	%r14, -664(%rbp)
.L652:
	movq	-664(%rbp), %rdi
	movq	%r12, -368(%rbp)
	movq	$0, -376(%rbp)
	call	init_tls
	movq	_dl_random(%rip), %rdx
	movq	%rax, -656(%rbp)
	movq	(%rdx), %rax
	xorb	%al, %al
#APP
# 868 "rtld.c" 1
	movq %rax,%fs:40
# 0 "" 2
#NO_APP
	movq	8(%rdx), %rax
#APP
# 877 "rtld.c" 1
	movq %rax,%fs:48
# 0 "" 2
#NO_APP
	movq	%rax, __pointer_chk_guard_local(%rip)
	leaq	-512(%rbp), %rax
	movq	$0, _dl_random(%rip)
	movq	$0, -696(%rbp)
	movq	%r13, -680(%rbp)
	movq	%rax, -624(%rbp)
	movq	%r15, -688(%rbp)
.L655:
	cmpq	$0, -368(%rbp)
	je	.L656
.L1371:
	movq	-624(%rbp), %rdi
	call	audit_list_next.part.5
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L656
	leaq	-593(%rbp), %r12
	leaq	-584(%rbp), %r14
	leaq	-592(%rbp), %r13
	movq	-616(%rbp), %r8
	leaq	dlmopen_doit(%rip), %rcx
	movq	4024+_rtld_local(%rip), %r15
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -576(%rbp)
	movq	$0, -568(%rbp)
	movq	$0, -584(%rbp)
	call	_dl_catch_error@PLT
	movq	-584(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L1370
	leaq	.LC99(%rip), %rax
	leaq	lookup_doit(%rip), %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%rax, -544(%rbp)
	movq	-568(%rbp), %rax
	movq	%rax, -536(%rbp)
	leaq	-544(%rbp), %rax
	movq	%rax, %r8
	movq	%rax, -640(%rbp)
	call	_dl_catch_error@PLT
	cmpq	$0, -584(%rbp)
	je	.L660
	movq	-568(%rbp), %rdi
	movl	%r15d, %esi
	call	unload_audit_module
	movzbl	-593(%rbp), %r13d
	movq	-584(%rbp), %r12
	leaq	.LC98(%rip), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r12, %rdx
	call	_dl_error_printf
	testb	%r13b, %r13b
	je	.L655
	movq	%r12, %rdi
	call	*__rtld_free(%rip)
	cmpq	$0, -368(%rbp)
	jne	.L1371
.L656:
	movl	800+_rtld_local_ro(%rip), %edi
	movq	-680(%rbp), %r13
	movq	-688(%rbp), %r15
	movb	$0, -624(%rbp)
	testl	%edi, %edi
	jne	.L1372
.L650:
	call	_dl_count_modids
	movq	%rax, -664(%rbp)
	movq	232(%r13), %rax
	testq	%rax, %rax
	je	.L672
	movq	-632(%rbp), %rdi
	movq	%rdi, 8(%rax)
.L672:
	movq	2800+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L673
	movq	-632(%rbp), %rdi
	movq	%rdi, 8(%rax)
.L673:
	movq	-632(%rbp), %rax
	movl	$1, 24(%rax)
	call	__GI__dl_debug_state
	movl	800+_rtld_local_ro(%rip), %esi
	testl	%esi, %esi
	jne	.L1373
.L674:
	movq	-648(%rbp), %rax
	cmpq	$0, (%rax)
	jne	.L1374
	xorl	%r12d, %r12d
	cmpq	$0, -88(%rbp)
	jne	.L1375
.L680:
	cmpq	$0, -80(%rbp)
	jne	.L1376
.L681:
	leaq	preload_file.11144(%rip), %rdi
	movl	$4, %esi
	call	__access
	testl	%eax, %eax
	je	.L1377
.L682:
	movq	-648(%rbp), %rax
	xorl	%esi, %esi
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1378
.L701:
	rdtsc
	xorl	%ecx, %ecx
	movq	%rax, %rbx
	salq	$32, %rdx
	movq	%r13, %rdi
	orq	%rdx, %rbx
	cmpl	$3, -56(%rbp)
	movl	%r12d, %edx
	sete	%cl
	xorl	%r8d, %r8d
	call	_dl_map_object_deps
	rdtsc
	movl	712(%r13), %esi
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	%rbx, %rax
	addq	%rax, load_time(%rip)
	testl	%esi, %esi
	je	.L703
	movq	704(%r13), %rcx
	leal	-1(%rsi), %eax
	leaq	(%rcx,%rax,8), %rax
	subq	$8, %rcx
	.p2align 4,,10
	.p2align 3
.L704:
	movq	(%rax), %rdx
	subq	$8, %rax
	orb	$16, 796(%rdx)
	cmpq	%rax, %rcx
	jne	.L704
	movq	2592+_rtld_local(%rip), %rax
	movq	2600+_rtld_local(%rip), %rdx
	movl	-56(%rbp), %edi
	movq	%rax, 24(%rdx)
	movq	2592+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L705
.L813:
	movq	%rdx, 32(%rax)
.L705:
	cmpl	$1, %esi
	jbe	.L1311
	movq	704(%r13), %r8
	leaq	2568+_rtld_local(%rip), %rax
	movl	$1, %ecx
	cmpq	%rax, 8(%r8)
	leaq	16(%r8), %rdx
	jne	.L709
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L710:
	addq	$8, %rdx
	leaq	2568+_rtld_local(%rip), %rbx
	cmpq	%rbx, -8(%rdx)
	je	.L1380
	movl	%eax, %ecx
.L709:
	leal	1(%rcx), %eax
	cmpl	%eax, %esi
	jne	.L710
.L1311:
	movb	$0, -632(%rbp)
.L706:
	xorl	%eax, %eax
	testl	%edi, %edi
	leaq	-544(%rbp), %rbx
	sete	%al
	leaq	version_check_doit(%rip), %rsi
	movl	%eax, -544(%rbp)
	xorl	%eax, %eax
	cmpl	$3, %edi
	leaq	print_missing_version(%rip), %rdi
	sete	%al
	movq	%rbx, %rdx
	movl	%eax, -540(%rbp)
	call	_dl_receive_error
	movzbl	tls_init_tp_called(%rip), %eax
	cmpq	$0, -656(%rbp)
	movb	%al, -640(%rbp)
	je	.L1381
.L716:
	cmpb	$0, -624(%rbp)
	je	.L717
	movq	_dl_random(%rip), %rdx
	movq	(%rdx), %rax
	xorb	%al, %al
#APP
# 868 "rtld.c" 1
	movq %rax,%fs:40
# 0 "" 2
#NO_APP
	movq	8(%rdx), %rax
#APP
# 877 "rtld.c" 1
	movq %rax,%fs:48
# 0 "" 2
#NO_APP
	movq	$0, _dl_random(%rip)
	movq	%rax, __pointer_chk_guard_local(%rip)
.L717:
	movl	-56(%rbp), %eax
	testl	%eax, %eax
	jne	.L1382
.L718:
	movq	640(%r13), %rax
	testq	%rax, %rax
	je	.L766
	cmpq	$0, 632+_rtld_local_ro(%rip)
	jne	.L766
	movl	76+_rtld_local_ro(%rip), %esi
	testl	%esi, %esi
	jne	.L766
	movq	104(%r13), %rdx
	movq	8(%rdx), %rbx
	movq	560(%r13), %rdx
	testq	%rdx, %rdx
	movq	%rbx, -616(%rbp)
	je	.L1383
	movq	8(%rax), %r14
	movq	8(%rdx), %r12
	movl	712(%r13), %eax
	movq	704(%r13), %r15
	addq	%r14, %r12
	cmpq	%r12, %r14
	leaq	(%r15,%rax,8), %rbx
	jnb	.L768
	cmpq	%rbx, %r15
	jnb	.L768
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L1384:
	movq	552(%rsi), %rax
	testq	%rax, %rax
	je	.L1303
	movl	8(%r15), %ecx
	cmpq	%rcx, 8(%rax)
	jne	.L1303
	movq	576(%rsi), %rax
	testq	%rax, %rax
	je	.L1303
	movl	4(%r15), %ecx
	cmpq	%rcx, 8(%rax)
	jne	.L1303
	movl	(%r15), %edi
	addq	-616(%rbp), %rdi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L1303
	addq	$20, %r15
.L769:
	addq	$8, %r14
	cmpq	%r14, %rbx
	jbe	.L1303
	cmpq	%r12, %r15
	jnb	.L1303
.L770:
	movq	(%r14), %rsi
	cmpq	%rsi, %r13
	je	.L769
	cmpq	$0, (%rsi)
	je	.L1384
	.p2align 4,,10
	.p2align 3
.L1303:
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
.L768:
	movl	_rtld_local_ro(%rip), %eax
	cmpq	%r15, %rbx
	sete	%bl
	cmpq	%r14, %r12
	sete	%dl
	movl	%eax, %ecx
	andl	$1, %ecx
	andb	%dl, %bl
	je	.L772
	testl	%ecx, %ecx
	je	.L765
	leaq	.LC41(%rip), %rsi
.L773:
	leaq	.LC126(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movl	_rtld_local_ro(%rip), %eax
	jmp	.L765
.L1402:
	xorl	%edx, %edx
	movb	$0, -640(%rbp)
	.p2align 4,,10
	.p2align 3
.L817:
	movq	$-1, 864(%r13)
	jmp	.L523
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	16(%rcx), %rax
	cmpq	$0, 3656+_rtld_local(%rip)
	movq	%rax, 3704+_rtld_local(%rip)
	movq	40(%rcx), %rax
	movq	%rax, 3712+_rtld_local(%rip)
	je	.L646
.L1369:
	call	_dl_next_tls_modid
	movq	%rax, 3688+_rtld_local(%rip)
	jmp	.L646
	.p2align 4,,10
	.p2align 3
.L835:
	movb	$1, -624(%rbp)
	movq	$0, -656(%rbp)
	jmp	.L650
	.p2align 4,,10
	.p2align 3
.L481:
	movq	(%rdi), %rax
	movq	%rax, 8(%rsi)
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L478:
	movq	%rdx, 8(%rsi)
	jmp	.L477
.L567:
	movq	680+_rtld_local_ro(%rip), %r14
	testq	%r14, %r14
	je	.L615
	leaq	2592+_rtld_local(%rip), %rax
	movq	%rax, -648(%rbp)
	jmp	.L614
.L831:
	xorl	%eax, %eax
	jmp	.L622
.L830:
	xorl	%eax, %eax
	jmp	.L618
.L834:
	xorl	%eax, %eax
	jmp	.L634
.L833:
	xorl	%eax, %eax
	jmp	.L630
.L832:
	xorl	%eax, %eax
	jmp	.L626
.L766:
	movl	_rtld_local_ro(%rip), %eax
	xorl	%ebx, %ebx
.L765:
	leaq	704(%r13), %rdx
	movdqu	704(%r13), %xmm0
	testb	$2, %ah
	movq	%rdx, 16+_rtld_local(%rip)
	movq	2560+_rtld_local(%rip), %rdx
	movups	%xmm0, 40+_rtld_local_ro(%rip)
	movq	%rdx, 664+_rtld_local_ro(%rip)
	jne	.L1385
.L774:
	movl	712(%r13), %eax
	movl	384+_rtld_local_ro(%rip), %edi
	subl	$1, %eax
	jmp	.L776
	.p2align 4,,10
	.p2align 3
.L780:
	movq	976(%r13), %rcx
	movl	%eax, %edx
	movq	(%rcx,%rdx,8), %rdx
	testb	$8, 796(%rdx)
	jne	.L777
	leaq	2568+_rtld_local(%rip), %rsi
	cmpq	%rsi, %rdx
	je	.L777
	cmpq	%rsi, 40(%rdx)
	je	.L777
	movl	808(%rdx), %ecx
	movl	%ecx, %esi
	andl	%edi, %esi
	cmpl	%esi, %ecx
	jne	.L1386
.L777:
	subl	$1, %eax
.L776:
	cmpl	$-1, %eax
	jne	.L780
	testb	%bl, %bl
	je	.L781
	cmpq	$0, 648(%r13)
	je	.L782
	rdtsc
	movq	%rax, %r12
	movq	568(%r13), %rax
	salq	$32, %rdx
	orq	%rdx, %r12
	testq	%rax, %rax
	je	.L1387
	movq	648(%r13), %rdx
	movq	%r13, %rdi
	movq	8(%rdx), %rsi
	movq	8(%rax), %rdx
	addq	%rsi, %rdx
	call	_dl_resolve_conflicts
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	%r12, %rax
	movq	%rax, relocate_time(%rip)
.L782:
	movq	%r13, %rdi
	movq	%r13, %r12
	call	__rtld_malloc_init_real
	jmp	.L786
	.p2align 4,,10
	.p2align 3
.L784:
	cmpq	$0, 1088(%r12)
	je	.L785
	cmpb	$0, tls_init_tp_called(%rip)
	jne	.L1388
.L785:
	movq	24(%r12), %r12
	testq	%r12, %r12
	je	.L787
.L786:
	orb	$4, 796(%r12)
	cmpq	$0, 1144(%r12)
	je	.L784
	movq	%r12, %rdi
	call	_dl_protect_relro
	jmp	.L784
.L1398:
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	-616(%rbp), %rax
	cmpq	$0, 2536+_rtld_local(%rip)
	movq	%rax, relocate_time(%rip)
	je	.L787
	call	_dl_start_profile
	.p2align 4,,10
	.p2align 3
.L787:
	cmpb	$0, -640(%rbp)
	jne	.L795
	cmpq	$0, 4024+_rtld_local(%rip)
	je	.L795
.L797:
	addq	$1, 4088+_rtld_local(%rip)
.L796:
	movq	-656(%rbp), %r15
	movq	%r15, %rdi
	call	__GI__dl_allocate_tls_init
	cmpb	$0, tls_init_tp_called(%rip)
	jne	.L798
	movq	%r15, %rsi
	movl	$158, %eax
	movl	$4098, %edi
	movq	%r15, (%rsi)
	movq	%r15, 16(%rsi)
#APP
# 2439 "rtld.c" 1
	syscall
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1389
	movq	%fs:16, %rax
	movq	4128+_rtld_local(%rip), %rcx
	leaq	4128+_rtld_local(%rip), %rdi
	leaq	704(%rax), %rdx
	movq	%rcx, 704(%rax)
	movq	%rdi, 712(%rax)
	movq	4128+_rtld_local(%rip), %rax
	movq	%rdx, 8(%rax)
	movq	%rdx, 4128+_rtld_local(%rip)
.L798:
	movq	2560+_rtld_local(%rip), %rax
	cmpq	%rax, 664+_rtld_local_ro(%rip)
	jne	.L1390
	testb	%bl, %bl
	jne	.L801
	cmpb	$0, -632(%rbp)
	jne	.L1391
.L801:
	movq	32+_rtld_local(%rip), %rdi
	movl	$1, %esi
	call	_dl_call_libc_early_init
	call	_dl_sysdep_start_cleanup
	movl	800+_rtld_local_ro(%rip), %eax
	testl	%eax, %eax
	jne	.L1392
.L802:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	$0, 24(%rax)
	call	__GI__dl_debug_state
	call	_dl_unload_cache
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L795:
	call	_dl_count_modids
	cmpq	-664(%rbp), %rax
	jne	.L797
	jmp	.L796
.L1380:
	leaq	(%r8,%rcx,8), %rdx
.L707:
	movq	(%rdx), %rdx
	testl	%edi, %edi
	movq	%rdx, 2600+_rtld_local(%rip)
	jne	.L1393
	addl	$1, %eax
	xorl	%ecx, %ecx
	cmpl	%esi, %eax
	jnb	.L711
	movq	(%r8,%rax,8), %rcx
.L711:
	movq	680+_rtld_local_ro(%rip), %rax
	movq	%rcx, 2592+_rtld_local(%rip)
	movq	24(%rdx), %rsi
	testq	%rax, %rax
	je	.L713
	cmpq	%rsi, %rax
	jne	.L713
	cmpq	%rcx, %rax
	jne	.L1394
.L713:
	cmpq	%rcx, %rsi
	jne	.L1395
	leaq	2568+_rtld_local(%rip), %rax
	movq	%rax, 24(%rdx)
	movq	2592+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L845
	cmpq	%rdx, 32(%rax)
	jne	.L1396
	leaq	2568+_rtld_local(%rip), %rbx
	movb	$1, -632(%rbp)
	movq	%rbx, 32(%rax)
	jmp	.L706
.L1347:
	movq	2576+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L526
	movq	%rax, _dl_rtld_libname(%rip)
	leaq	_dl_rtld_libname(%rip), %rax
	movq	%rax, 2624+_rtld_local(%rip)
	jmp	.L525
.L581:
	movq	%r11, %rdi
	subq	%rax, %rdi
	cmpq	$11, %rdi
	jbe	.L1397
	movq	%rbx, %rdi
	subq	%rax, %rdi
	cmpq	$10, %rdi
	ja	.L583
	movl	$1879048001, %edi
	subq	%rax, %rdi
	movq	%rdi, %rax
	jmp	.L579
.L781:
	xorl	%r12d, %r12d
	cmpq	$0, 632+_rtld_local_ro(%rip)
	setne	%r12b
	orl	%r12d, 68+_rtld_local_ro(%rip)
	rdtsc
	movl	712(%r13), %r15d
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, -616(%rbp)
	.p2align 4,,10
	.p2align 3
.L788:
	subl	$1, %r15d
	cmpl	$-1, %r15d
	je	.L1398
	movq	976(%r13), %rdx
	movl	%r15d, %eax
	movq	(%rdx,%rax,8), %r14
	movq	56(%r14), %rax
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L790
.L789:
	leaq	2568+_rtld_local(%rip), %rax
	andb	$-2, 798(%r14)
	cmpq	%rax, %r14
	je	.L791
	movl	68+_rtld_local_ro(%rip), %ecx
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	920(%r14), %rsi
	testl	%ecx, %ecx
	movl	%r12d, %ecx
	setne	%dl
	call	_dl_relocate_object
.L791:
	cmpq	$0, 1088(%r14)
	je	.L788
	cmpb	$0, tls_init_tp_called(%rip)
	je	.L788
	movl	$1, %esi
	movq	%r14, %rdi
	call	_dl_add_to_slotinfo
	jmp	.L788
.L1365:
	movq	2624+_rtld_local(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 2576+_rtld_local(%rip)
	jmp	.L639
.L1388:
	movl	$1, %esi
	movq	%r12, %rdi
	call	_dl_add_to_slotinfo
	jmp	.L785
.L536:
	movq	%r10, %rsi
	subq	%rax, %rsi
	cmpq	$11, %rsi
	jbe	.L1399
	movq	%r11, %rsi
	subq	%rax, %rsi
	cmpq	$10, %rsi
	ja	.L538
	movq	%r12, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	jmp	.L534
.L1321:
	movq	16(%rax), %rcx
	movq	%r10, -96(%rbp)
	movq	%rcx, -104(%rbp)
.L1307:
	subl	$2, %edx
	addq	$16, %rax
	addl	$2, _dl_skip_args(%rip)
	movl	%edx, _dl_argc(%rip)
	movq	%rax, __GI__dl_argv(%rip)
	jmp	.L443
.L772:
	testl	%ecx, %ecx
	leaq	.LC42(%rip), %rsi
	je	.L765
	jmp	.L773
.L1328:
	movl	-56(%rbp), %eax
.L811:
	cmpl	$5, %eax
	je	.L1315
	movq	-656(%rbp), %rdi
	xorl	%esi, %esi
	call	_dl_usage
.L1341:
	movq	$0, 1104(%r13)
	jmp	.L502
.L1391:
	movq	%r13, %rdi
	call	__rtld_malloc_init_real
	rdtsc
	andb	$-5, 3364+_rtld_local(%rip)
	leaq	2568+_rtld_local(%rip), %rdi
	xorl	%ecx, %ecx
	movq	920(%r13), %rsi
	movq	%rax, %rbx
	salq	$32, %rdx
	orq	%rdx, %rbx
	xorl	%edx, %edx
	call	_dl_relocate_object
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	%rbx, %rax
	addq	%rax, relocate_time(%rip)
	jmp	.L801
.L1397:
	movq	%r12, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
	jmp	.L579
.L404:
	movabsq	$6147236776361739588, %rdx
	cmpq	%rdx, (%rax)
	jne	.L369
	cmpl	$1414877268, 8(%rax)
	jne	.L369
.L809:
	leaq	13(%rax), %r13
	jmp	.L369
.L1333:
	cmpb	$0, 9(%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, 68+_rtld_local_ro(%rip)
	jmp	.L369
.L1323:
	movq	16(%rax), %rsi
	cmpb	$0, (%rsi)
	je	.L1307
	leaq	-512(%rbp), %rdi
	movq	%r10, -664(%rbp)
	call	audit_list_add_string.part.1
	movl	_dl_argc(%rip), %edx
	movq	__GI__dl_argv(%rip), %rax
	movq	-664(%rbp), %r10
	jmp	.L1307
.L1399:
	movq	%rbx, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	jmp	.L534
.L1324:
	movq	16(%rax), %rcx
	movq	%rcx, -80(%rbp)
	jmp	.L1307
.L1394:
	movq	%rax, 2600+_rtld_local(%rip)
	movq	24(%rax), %rsi
	movq	%rax, %rdx
	jmp	.L713
.L1336:
	cmpb	$71, 4(%rax)
	jne	.L386
	movq	-648(%rbp), %rdi
	leaq	6(%rax), %rsi
	call	process_dl_debug.isra.3
	jmp	.L369
.L1334:
	cmpw	$21327, 4(%rax)
	jne	.L391
	cmpb	$69, 6(%rax)
	jne	.L391
	cmpb	$0, 8(%rax)
	setne	-51(%rbp)
	jmp	.L369
.L1330:
	cmpl	$1162760773, 8(%rax)
	jne	.L413
	cmpb	$76, 12(%rax)
	jne	.L413
	leaq	14(%rax), %rcx
	leaq	-544(%rbp), %rax
	movl	$2, %r15d
	movq	%r13, -656(%rbp)
	movq	$0, -664(%rbp)
	movq	%r15, %r13
	movq	%rax, -672(%rbp)
	movq	%r14, %r15
	movq	%rbx, %r14
	movq	%rcx, %rbx
.L420:
	movq	-672(%rbp), %rsi
	movq	%rbx, %rdi
	call	_dl_strtoul
	cmpq	$254, %rax
	ja	.L1285
	movq	-544(%rbp), %rdx
	cmpq	%rbx, %rdx
	je	.L1285
	testq	%r13, %r13
	je	.L417
	movzbl	(%rdx), %esi
	cmpb	$46, %sil
	je	.L860
	testb	%sil, %sil
	jne	.L1285
.L860:
	leal	0(,%r13,8), %ecx
	salq	%cl, %rax
	orq	%rax, -664(%rbp)
	testb	%sil, %sil
	je	.L1286
	leaq	1(%rdx), %rbx
	subq	$1, %r13
	jmp	.L420
.L790:
	movl	$1, 16(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L789
	jmp	.L790
	.p2align 4,,10
	.p2align 3
.L1370:
	movzbl	-593(%rbp), %r12d
	leaq	.LC98(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	movq	%rbx, %rsi
	movq	%rcx, -640(%rbp)
	call	_dl_error_printf
	testb	%r12b, %r12b
	je	.L655
	movq	-640(%rbp), %rcx
	movq	%rcx, %rdi
	call	*__rtld_free(%rip)
	jmp	.L655
.L1386:
	movq	__GI__dl_argv(%rip), %rax
	movq	8(%rdx), %rsi
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L778
	cmpb	$0, (%rsi)
	leaq	.LC128(%rip), %rdi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L845:
	movb	$1, -632(%rbp)
	jmp	.L706
.L660:
	movq	-528(%rbp), %rax
	testq	%rax, %rax
	je	.L1400
	movl	$1, %edi
	call	*%rax
	testl	%eax, %eax
	jne	.L662
	testb	$64, _rtld_local_ro(%rip)
	movq	-568(%rbp), %rdi
	je	.L663
	movq	48(%rdi), %rdx
	movq	8(%rdi), %rsi
	leaq	.LC101(%rip), %rdi
	call	_dl_debug_printf
	movq	-568(%rbp), %rdi
.L663:
	movl	%r15d, %esi
	call	unload_audit_module
	jmp	.L655
.L1294:
	movq	-664(%rbp), %r12
	jmp	.L506
.L1322:
	movq	16(%rax), %rcx
	movq	%rcx, 600+_rtld_local_ro(%rip)
	jmp	.L1307
.L1343:
	movl	-672(%rbp), %edi
	movl	%esi, %eax
	movq	-664(%rbp), %r12
	orl	%edi, %eax
	je	.L511
	movzbl	801(%r13), %eax
	movl	%edi, 808(%r13)
	movl	%esi, 804(%r13)
	andl	$-4, %eax
	orl	$2, %eax
	movb	%al, 801(%r13)
	jmp	.L506
.L1315:
	movq	-656(%rbp), %rdi
	leaq	-512(%rbp), %rsi
	call	_dl_help
.L486:
	jne	.L1401
	movq	1072(%r13), %rax
	testq	%rax, %rax
	je	.L1402
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$0, -640(%rbp)
	jmp	.L816
.L1372:
	movq	%r13, %rdi
	call	notify_audit_modules_of_loaded_object
	leaq	2568+_rtld_local(%rip), %rdi
	call	notify_audit_modules_of_loaded_object
	movl	800+_rtld_local_ro(%rip), %eax
	cmpq	-664(%rbp), %rax
	jbe	.L650
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC105(%rip), %rdi
	movl	$1770, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L1381:
	xorl	%edi, %edi
	call	init_tls
	movq	%rax, -656(%rbp)
	jmp	.L716
.L823:
	xorl	%r9d, %r9d
	jmp	.L496
	.p2align 4,,10
	.p2align 3
.L498:
	addq	$1, %rdx
	cmpb	$47, %cl
	cmove	%rdx, %r9
.L496:
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L498
	testq	%r9, %r9
	movb	$1, -640(%rbp)
	je	.L487
	movq	%r9, (%r8)
	movq	%r8, 8+_dl_rtld_libname(%rip)
	jmp	.L487
.L1317:
	leaq	unsecure_envvars.11336(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L434:
	movq	%r12, %rdi
	call	unsetenv
	movq	%r12, %rdi
	call	strlen@PLT
	addq	%r12, %rax
	cmpb	$0, 1(%rax)
	leaq	1(%rax), %r12
	jne	.L434
	leaq	.LC60(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	je	.L435
	movl	$0, _rtld_local_ro(%rip)
.L435:
	movl	-56(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L437
	movl	$5, %edi
	call	_exit
	jmp	.L437
.L703:
	movq	2592+_rtld_local(%rip), %rax
	movq	2600+_rtld_local(%rip), %rdx
	movl	-56(%rbp), %edi
	movq	%rax, 24(%rdx)
	movq	2592+_rtld_local(%rip), %rax
	testq	%rax, %rax
	jne	.L813
	jmp	.L1311
.L662:
	cmpl	$1, %eax
	ja	.L1403
	movl	$72, %edi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, -672(%rbp)
	je	.L1404
	leaq	audit_iface_names.11036(%rip), %r15
	xorl	%ebx, %ebx
	jmp	.L665
	.p2align 4,,10
	.p2align 3
.L666:
	movq	-672(%rbp), %rdi
	movl	%ebx, %eax
	movq	$0, (%rdi,%rax,8)
.L667:
	xorl	%esi, %esi
	movq	%r15, %rdi
	addl	$1, %ebx
	call	rawmemchr
	cmpb	$0, 1(%rax)
	leaq	1(%rax), %r15
	je	.L1405
.L665:
	movq	-640(%rbp), %r8
	leaq	lookup_doit(%rip), %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r15, -544(%rbp)
	call	_dl_catch_error@PLT
	cmpq	$0, -584(%rbp)
	jne	.L666
	movq	-528(%rbp), %rax
	testq	%rax, %rax
	je	.L666
	movq	-672(%rbp), %rdi
	movl	%ebx, %edx
	movq	%rax, (%rdi,%rdx,8)
	jmp	.L667
.L511:
	movzbl	801(%r13), %eax
	andl	$-4, %eax
	orl	$1, %eax
	movb	%al, 801(%r13)
	jmp	.L506
.L1351:
	cmpq	$0, 16(%r13)
	je	.L1406
.L565:
	movzbl	-640(%rbp), %edi
	xorl	$1, %edi
	movzbl	%dil, %edi
	addl	%edi, %edi
	call	_exit
	jmp	.L564
.L635:
	leaq	.LC94(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L1405:
	cmpl	$8, %ebx
	jne	.L1407
	cmpq	$0, -696(%rbp)
	movq	-672(%rbp), %rax
	movq	$0, 64(%rax)
	je	.L1408
	movq	-696(%rbp), %rax
	movq	-672(%rbp), %rbx
	movq	%rbx, 64(%rax)
.L670:
	movl	800+_rtld_local_ro(%rip), %eax
	leaq	_rtld_local(%rip), %rbx
	leaq	2568(%rbx), %rdi
	movq	%rax, %rdx
	addq	$233, %rax
	salq	$4, %rax
	addl	$1, %edx
	movq	%rdi, (%rbx,%rax)
	movq	-568(%rbp), %rax
	movl	%edx, 800+_rtld_local_ro(%rip)
	orb	$8, 797(%rax)
	movq	-672(%rbp), %rax
	movq	%rax, -696(%rbp)
	jmp	.L655
.L1363:
	cmpl	%eax, %edx
	jbe	.L637
	jmp	.L636
.L1378:
	movl	%r12d, %edx
	leaq	30(,%rdx,8), %rdx
	shrq	$4, %rdx
	salq	$4, %rdx
	subq	%rdx, %rsp
	xorl	%edx, %edx
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L702:
	movl	%edx, %ecx
	addl	$1, %edx
	movq	%rax, (%rdi,%rcx,8)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L702
	cmpl	%edx, %r12d
	je	.L701
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC109(%rip), %rdi
	movl	$1933, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L1377:
	movq	-616(%rbp), %rsi
	leaq	preload_file.11144(%rip), %rdi
	movl	$3, %edx
	call	_dl_sysdep_read_whole_file
	cmpq	$-1, %rax
	movq	%rax, %r14
	je	.L682
	movq	-576(%rbp), %rbx
	movq	%rax, -544(%rbp)
	testq	%rbx, %rbx
	movq	%rbx, %rsi
	je	.L683
	movq	%rax, %rcx
.L686:
	movq	%rbx, %rdx
	movl	$35, %esi
	movq	%rcx, %rdi
	movq	%rcx, -616(%rbp)
	call	memchr
	testq	%rax, %rax
	je	.L684
	movq	-616(%rbp), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	subq	%rdx, %rbx
	jmp	.L685
.L1410:
	addq	$1, %rax
	cmpb	$10, (%rax)
	je	.L1409
.L685:
	subq	$1, %rbx
	movb	$32, (%rax)
	jne	.L1410
.L684:
	movq	-576(%rbp), %rsi
	leaq	-1(%r14,%rsi), %rax
	movzbl	(%rax), %edx
	cmpb	$58, %dl
	jbe	.L1411
.L687:
	leaq	(%r14,%rsi), %r15
	cmpq	%r15, %r14
	jnb	.L689
	movzbl	-1(%r15), %eax
	cmpb	$58, %al
	ja	.L690
	movabsq	$288230380446680576, %rdx
	btq	%rax, %rdx
	jc	.L691
.L690:
	movabsq	$288230380446680576, %rdx
.L692:
	subq	$1, %r15
	cmpq	%r15, %r14
	je	.L693
	movzbl	-1(%r15), %eax
	cmpb	$58, %al
	ja	.L692
	btq	%rax, %rdx
	jnc	.L692
.L691:
	movb	$0, -1(%r15)
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, -616(%rbp)
.L695:
	movq	%r14, -544(%rbp)
	leaq	-544(%rbp), %rbx
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L700:
	cmpb	$0, (%rax)
	jne	.L1412
.L699:
	leaq	.LC108(%rip), %rsi
	movq	%rbx, %rdi
	call	strsep
	testq	%rax, %rax
	jne	.L700
	movq	-576(%rbp), %rsi
.L697:
	testq	%r15, %r15
	je	.L696
	movq	%r15, %rax
	movq	%r15, %rdi
	subq	%r14, %rax
	subq	%rax, %rsi
	call	strnlen
	leaq	31(%rax), %rdx
	movq	%r15, %rsi
	andq	$-16, %rdx
	subq	%rdx, %rsp
	movq	%rax, %rdx
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movb	$0, (%rdi,%rax)
	call	memcpy@PLT
	leaq	preload_file.11144(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	do_preload
	addl	%eax, %r12d
.L1310:
	movq	-576(%rbp), %rsi
.L696:
	rdtsc
	salq	$32, %rdx
	movq	%r14, %rdi
	orq	%rdx, %rax
	subq	-616(%rbp), %rax
	addq	%rax, load_time(%rip)
	call	__munmap
	jmp	.L682
.L1376:
	rdtsc
	movq	-80(%rbp), %rdi
	salq	$32, %rdx
	movq	%rax, %rbx
	movq	%r13, %rsi
	orq	%rdx, %rbx
	leaq	.LC67(%rip), %rdx
	call	handle_preload_list@PLT
	addl	%eax, %r12d
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	%rbx, %rax
	addq	%rax, load_time(%rip)
	jmp	.L681
.L1382:
	movl	_rtld_local_ro(%rip), %edx
	movl	%edx, %r12d
	andl	$2048, %r12d
	je	.L719
	movl	712(%r13), %ecx
	testl	%ecx, %ecx
	je	.L720
	xorl	%r12d, %r12d
	leaq	.LC37(%rip), %r15
	jmp	.L727
.L1414:
	movq	1112(%r14), %rcx
	leaq	.LC114(%rip), %rdi
	movl	$16, %edx
	xorl	%eax, %eax
	call	_dl_printf
.L722:
	addl	$1, %r12d
	cmpl	%r12d, 712(%r13)
	jbe	.L1312
.L727:
	movq	704(%r13), %rdx
	movl	%r12d, %eax
	movq	(%rdx,%rax,8), %r14
	testb	$2, 797(%r14)
	jne	.L1413
	movq	648+_rtld_local_ro(%rip), %rdi
	movq	%r14, %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L723
	movq	%r14, 656+_rtld_local_ro(%rip)
.L723:
	movq	8(%r14), %rdx
	movq	(%r14), %rax
	movq	856(%r14), %r8
	cmpb	$0, (%rdx)
	jne	.L724
	movq	__GI__dl_argv(%rip), %rdx
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	cmove	%r15, %rdx
.L724:
	movq	56(%r14), %rcx
	movq	(%rcx), %rsi
	cmpb	$0, (%rsi)
	jne	.L725
	movq	__GI__dl_argv(%rip), %rcx
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	cmove	%r15, %rsi
.L725:
	subq	$8, %rsp
	leaq	.LC113(%rip), %rdi
	movl	$16, %r9d
	pushq	%rax
	movl	$16, %ecx
	xorl	%eax, %eax
	call	_dl_printf
	movq	1120(%r14), %rsi
	popq	%rax
	popq	%rdx
	testq	%rsi, %rsi
	jne	.L1414
	leaq	.LC115(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L722
.L1373:
	movq	792+_rtld_local_ro(%rip), %r14
	xorl	%r12d, %r12d
	leaq	_rtld_local(%rip), %rbx
	jmp	.L678
.L677:
	movl	$1, %esi
	call	*%rax
.L675:
	addl	$1, %r12d
	cmpl	%r12d, 800+_rtld_local_ro(%rip)
	movq	64(%r14), %r14
	jbe	.L674
.L678:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L675
	movl	%r12d, %edx
	movq	%rdx, %rcx
	salq	$4, %rcx
	leaq	1160(%r13,%rcx), %rdi
	leaq	2568+_rtld_local(%rip), %rcx
	cmpq	%rcx, %r13
	jne	.L677
	addq	$233, %rdx
	salq	$4, %rdx
	leaq	(%rbx,%rdx), %rdi
	jmp	.L677
.L1375:
	rdtsc
	movq	-88(%rbp), %rdi
	salq	$32, %rdx
	movq	%rax, %rbx
	movq	%r13, %rsi
	orq	%rdx, %rbx
	leaq	.LC107(%rip), %rdx
	call	handle_preload_list@PLT
	movl	%eax, %r12d
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	subq	%rbx, %rax
	addq	%rax, load_time(%rip)
	jmp	.L680
.L1385:
	leaq	.LC127(%rip), %rdi
	xorl	%eax, %eax
	movq	%r13, %r12
	call	_dl_debug_printf
	.p2align 4,,10
	.p2align 3
.L775:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	_dl_show_scope
	movq	24(%r12), %r12
	testq	%r12, %r12
	jne	.L775
	jmp	.L774
.L1325:
	movq	16(%rax), %rdi
	movq	%rdi, -648(%rbp)
	jmp	.L1307
.L1393:
	movq	24(%rdx), %rcx
	movq	%rcx, 2592+_rtld_local(%rip)
	movq	24(%rdx), %rsi
	jmp	.L713
.L458:
	leaq	.LC72(%rip), %rdi
	movl	$7, %ecx
	movq	%r8, %rsi
	repz cmpsb
	jne	.L459
	subl	$1, %edx
	addq	$8, %rax
	movl	$5, -56(%rbp)
	movl	%edx, _dl_argc(%rip)
	movq	%rax, __GI__dl_argv(%rip)
	jmp	.L443
.L1345:
	movl	%edx, %esi
	movl	-680(%rbp), %edx
	jmp	.L514
.L1389:
	leaq	.LC13(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L526:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC77(%rip), %rdi
	movl	$1606, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L1335:
	cmpw	$16719, 4(%rax)
	jne	.L394
	cmpb	$68, 6(%rax)
	jne	.L394
	addq	$8, %rax
	movq	%rax, -88(%rbp)
	jmp	.L369
.L719:
	andb	$1, %dh
	je	.L728
	movl	68+_rtld_local_ro(%rip), %r11d
	xorl	%eax, %eax
	leaq	relocate_doit(%rip), %rsi
	leaq	print_unresolved(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r13, -544(%rbp)
	testl	%r11d, %r11d
	setne	%al
	addl	$33554432, %eax
	movl	%eax, -536(%rbp)
	call	_dl_receive_error
	movq	16(%r13), %r14
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L730
	movq	%r13, %r15
	movl	$1, %eax
	leaq	.LC117(%rip), %r12
	jmp	.L734
.L731:
	addq	$16, %r14
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L1415
.L734:
	cmpq	$1, %rdx
	jne	.L731
	movq	24(%r15), %r15
	cmpq	%r15, 680+_rtld_local_ro(%rip)
	jne	.L732
	movq	24(%r15), %r15
.L732:
	movl	996(%r15), %r10d
	testl	%r10d, %r10d
	jne	.L731
	testb	%al, %al
	jne	.L1416
.L733:
	movq	8(%r15), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_dl_printf
	xorl	%eax, %eax
	jmp	.L731
.L1415:
	xorl	$1, %eax
	movzbl	%al, %r12d
.L730:
	movl	%r12d, %edi
	call	_exit
	movl	-56(%rbp), %eax
.L720:
	cmpl	$3, %eax
	jne	.L1417
	movl	68+_rtld_local_ro(%rip), %eax
	testl	%eax, %eax
	js	.L744
	movl	60+_rtld_local_ro(%rip), %edi
	testl	%edi, %edi
	je	.L744
	testl	%eax, %eax
	movl	712(%r13), %r14d
	leaq	relocate_doit(%rip), %r12
	setne	%al
	movzbl	%al, %eax
	addl	$33554432, %eax
	movl	%eax, -536(%rbp)
.L746:
	subl	$1, %r14d
	cmpl	$-1, %r14d
	je	.L1418
	movq	976(%r13), %rdx
	movl	%r14d, %eax
	leaq	2568+_rtld_local(%rip), %rdi
	movq	(%rdx,%rax,8), %rax
	cmpq	%rdi, %rax
	je	.L746
	testb	$2, 797(%rax)
	jne	.L746
	leaq	print_unresolved(%rip), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rax, -544(%rbp)
	call	_dl_receive_error
	jmp	.L746
.L1418:
	testb	$8, 1+_rtld_local_ro(%rip)
	je	.L744
	cmpb	$0, -632(%rbp)
	je	.L744
	andb	$-5, 3364+_rtld_local(%rip)
	leaq	2568+_rtld_local(%rip), %rdi
	xorl	%ecx, %ecx
	movq	920(%r13), %rsi
	movl	$33554432, %edx
	call	_dl_relocate_object
.L744:
	cmpb	$0, -51(%rbp)
	je	.L741
	movq	%r13, -648(%rbp)
	movl	$1, %edx
	movq	%r13, -672(%rbp)
.L763:
	movq	-648(%rbp), %rbx
	movq	352(%rbx), %rax
	testq	%rax, %rax
	je	.L750
	movq	104(%rbx), %rcx
	movq	8(%rax), %r15
	addq	(%rbx), %r15
	testl	%edx, %edx
	movq	8(%rcx), %rdi
	movq	%r15, -624(%rbp)
	movq	%rdi, -616(%rbp)
	jne	.L1419
.L751:
	movq	-648(%rbp), %rax
	movq	8(%rax), %rsi
	cmpb	$0, (%rsi)
	jne	.L752
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC37(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L752:
	leaq	.LC123(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	.p2align 4,,10
	.p2align 3
.L762:
	movq	-624(%rbp), %rax
	movl	4(%rax), %r12d
	leaq	_rtld_local(%rip), %rax
	addq	-616(%rbp), %r12
	movq	(%rax), %r14
	movl	712(%r14), %eax
	leal	-1(%rax), %ebx
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L755:
	movq	704(%r14), %rdx
	movl	%ebx, %eax
	movq	%r12, %rdi
	leaq	0(,%rax,8), %r13
	subl	$1, %ebx
	movq	(%rdx,%rax,8), %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	jne	.L1420
.L753:
	cmpl	$-1, %ebx
	jne	.L755
	xorl	%r14d, %r14d
.L754:
	movq	-624(%rbp), %rax
	movl	8(%rax), %r12d
	.p2align 4,,10
	.p2align 3
.L1313:
	addq	%rax, %r12
	leaq	.LC38(%rip), %r8
	movl	8(%r12), %ebx
	addq	-616(%rbp), %rbx
	testq	%r14, %r14
	je	.L756
	movq	368(%r14), %rax
	testq	%rax, %rax
	je	.L756
	movq	104(%r14), %rdx
	movq	8(%rdx), %r13
	movq	(%r14), %rdx
	addq	8(%rax), %rdx
	movq	%rdx, %r15
	jmp	.L758
	.p2align 4,,10
	.p2align 3
.L1421:
	movl	16(%r15), %eax
	testl	%eax, %eax
	je	.L854
	addq	%rax, %r15
.L758:
	movl	12(%r15), %eax
	movq	%rbx, %rdi
	movl	(%r15,%rax), %esi
	addq	%r13, %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L1421
	movq	8(%r14), %r8
	leaq	.LC38(%rip), %rax
	testq	%r8, %r8
	cmove	%rax, %r8
.L756:
	testb	$2, 4(%r12)
	leaq	.LC40(%rip), %rax
	leaq	.LC39(%rip), %rcx
	leaq	.LC124(%rip), %rdi
	movq	%rbx, %rdx
	cmove	%rax, %rcx
	movq	-624(%rbp), %rax
	movl	4(%rax), %esi
	xorl	%eax, %eax
	addq	-616(%rbp), %rsi
	call	_dl_printf
	movl	12(%r12), %eax
	testl	%eax, %eax
	jne	.L1313
	movq	-624(%rbp), %rbx
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.L856
	addq	%rax, %rbx
	movq	%rbx, -624(%rbp)
	jmp	.L762
	.p2align 4,,10
	.p2align 3
.L854:
	leaq	.LC38(%rip), %r8
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L1420:
	movq	704(%r14), %rax
	movq	(%rax,%r13), %r14
	jmp	.L754
.L856:
	xorl	%edx, %edx
.L750:
	movq	-648(%rbp), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	movq	%rax, -648(%rbp)
	jne	.L763
	movq	-672(%rbp), %r13
.L741:
	xorl	%edi, %edi
	call	_exit
	jmp	.L718
.L1413:
	movq	56(%r14), %rax
	leaq	.LC112(%rip), %rdi
	movq	(%rax), %rsi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L722
.L417:
	orq	%rax, -664(%rbp)
	movq	-656(%rbp), %r13
	movq	%r14, %rbx
	movq	%r15, %r14
.L419:
	movq	-664(%rbp), %rax
	testq	%rax, %rax
	je	.L369
	movl	%eax, 4+_rtld_local_ro(%rip)
	jmp	.L369
.L1379:
	movq	%r8, %rdx
	movl	$1, %eax
	jmp	.L707
.L1392:
	movq	_rtld_local(%rip), %r14
	testb	$8, 797(%r14)
	jne	.L802
	movq	792+_rtld_local_ro(%rip), %r13
	xorl	%r12d, %r12d
	leaq	_rtld_local(%rip), %rbx
	jmp	.L806
.L805:
	xorl	%esi, %esi
	call	*%rdx
.L803:
	addl	$1, %r12d
	cmpl	%r12d, 800+_rtld_local_ro(%rip)
	movq	64(%r13), %r13
	jbe	.L802
.L806:
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	je	.L803
	movl	%r12d, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	leaq	1160(%r14,%rcx), %rdi
	leaq	2568+_rtld_local(%rip), %rcx
	cmpq	%rcx, %r14
	jne	.L805
	addq	$233, %rax
	salq	$4, %rax
	leaq	(%rax,%rbx), %rdi
	jmp	.L805
.L1326:
	movq	16(%rax), %rcx
	movq	%rcx, -72(%rbp)
	jmp	.L1307
.L1419:
	leaq	.LC122(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L751
.L1331:
	cmpl	$1213481296, 8(%rax)
	jne	.L405
	addq	$13, %rax
	movq	%rax, -104(%rbp)
	leaq	.LC51(%rip), %rax
	movq	%rax, -96(%rbp)
	jmp	.L369
.L1312:
	movl	-56(%rbp), %eax
	jmp	.L720
.L615:
	leaq	2592+_rtld_local(%rip), %rax
	movq	$0, 688+_rtld_local_ro(%rip)
	movq	$0, 696+_rtld_local_ro(%rip)
	movq	%rax, -648(%rbp)
.L623:
	movq	$0, 704+_rtld_local_ro(%rip)
	movq	$0, 712+_rtld_local_ro(%rip)
.L633:
	xorl	%eax, %eax
	jmp	.L632
.L778:
	leaq	.LC129(%rip), %rcx
	leaq	.LC130(%rip), %rdx
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
.L1327:
	movq	16(%rax), %rcx
	movq	%rcx, -64(%rbp)
	jmp	.L1307
.L1359:
	movq	680+_rtld_local_ro(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L617
.L861:
	movq	$0, -576(%rbp)
	movq	-616(%rbp), %rsi
	leaq	-592(%rbp), %rdx
	movq	8(%rax), %rax
	leaq	-584(%rbp), %rdi
	leaq	-544(%rbp), %r8
	leaq	map_doit(%rip), %rcx
	movq	$0, -536(%rbp)
	movl	$536870912, -528(%rbp)
	movq	%rax, -544(%rbp)
	call	_dl_catch_error@PLT
	cmpq	$0, -576(%rbp)
	je	.L474
	cmpl	$5, -56(%rbp)
	je	.L1315
	movl	$1, %edi
	call	_exit
	jmp	.L474
.L1362:
	movq	680+_rtld_local_ro(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L629
.L1360:
	movq	680+_rtld_local_ro(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L621
.L1361:
	movq	680+_rtld_local_ro(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L625
.L1344:
	movl	%edx, %esi
	movl	%r9d, %edi
	movl	-680(%rbp), %edx
	movl	%eax, -672(%rbp)
	jmp	.L514
.L1355:
	movl	%eax, %esi
	andl	$-134220010, %esi
	je	.L607
	leaq	.LC80(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movl	1004(%r14), %edx
	jmp	.L607
.L1408:
	movq	%rax, 792+_rtld_local_ro(%rip)
	jmp	.L670
.L1409:
	movq	-544(%rbp), %rcx
	jmp	.L686
.L1332:
	cmpl	$1414877268, 8(%rax)
	jne	.L408
	jmp	.L809
.L1406:
	movl	$1, %edi
	call	_exit
	jmp	.L565
.L728:
	cmpq	$0, 72(%r13)
	je	.L1422
	movq	24(%r13), %r12
	testq	%r12, %r12
	jne	.L739
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L736:
	movq	8(%r12), %r15
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	strcmp
	testl	%eax, %eax
	movq	856(%r12), %r8
	jne	.L738
	leaq	.LC119(%rip), %rdi
	movq	%r8, %rcx
	movl	$16, %edx
	movq	%r14, %rsi
	call	_dl_printf
.L737:
	movq	24(%r12), %r12
	testq	%r12, %r12
	je	.L1312
.L739:
	testb	$2, 797(%r12)
	movq	56(%r12), %rax
	movq	(%rax), %r14
	je	.L736
	leaq	.LC112(%rip), %rdi
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L737
.L738:
	leaq	.LC120(%rip), %rdi
	movl	$16, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L737
.L1403:
	leaq	.LC102(%rip), %rdi
	movl	%eax, %edx
	movq	%rbx, %rsi
	movl	$1, %ecx
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	-568(%rbp), %rdi
	movl	%r15d, %esi
	call	unload_audit_module
	jmp	.L655
.L1411:
	movabsq	$288230380446680576, %rdi
	btq	%rdx, %rdi
	jnc	.L687
.L688:
	movb	$0, (%rax)
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	testq	%r14, %r14
	movq	%rax, -616(%rbp)
	jne	.L695
	jmp	.L1310
	.p2align 4,,10
	.p2align 3
.L1417:
	cmpl	$1, _dl_argc(%rip)
	jbe	.L741
	leaq	.LC121(%rip), %r12
	movl	$1, %r15d
.L743:
	movq	__GI__dl_argv(%rip), %rax
	movl	%r15d, %r14d
	movq	920(%r13), %rcx
	movq	$0, -544(%rbp)
	xorl	%r8d, %r8d
	movl	$1, %r9d
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	(%rax,%r14,8), %rdi
	pushq	$0
	pushq	$1
	call	_dl_lookup_symbol_x
	popq	%r8
	popq	%r9
	xorl	%r9d, %r9d
	testq	%rax, %rax
	je	.L742
	movq	(%rax), %r9
.L742:
	movq	-544(%rbp), %rax
	movl	$16, %r8d
	movl	$16, %edx
	movq	%r12, %rdi
	addl	$1, %r15d
	movq	8(%rax), %rcx
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax,%r14,8), %rsi
	xorl	%eax, %eax
	call	_dl_printf
	cmpl	%r15d, _dl_argc(%rip)
	ja	.L743
	jmp	.L741
.L1285:
	movq	%r14, %rbx
	movq	-656(%rbp), %r13
	movq	%r15, %r14
	jmp	.L369
.L689:
	rdtsc
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpq	%r15, %r14
	movq	%rax, -616(%rbp)
	jne	.L695
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L1412:
	leaq	preload_file.11144(%rip), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	do_preload
	addl	%eax, %r12d
	jmp	.L699
.L459:
	leaq	.LC73(%rip), %rdi
	movl	$10, %ecx
	movq	%r8, %rsi
	repz cmpsb
	je	.L1423
	cmpb	$45, (%r8)
	je	.L1424
.L461:
	movl	-56(%rbp), %ecx
	cmpl	$4, %ecx
	jne	.L812
	jmp	.L810
	.p2align 4,,10
	.p2align 3
.L1286:
	movq	%r14, %rbx
	movq	-656(%rbp), %r13
	movq	%r15, %r14
	jmp	.L419
.L1422:
	leaq	.LC118(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	movl	-56(%rbp), %eax
	jmp	.L720
.L1416:
	leaq	.LC116(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L733
.L1329:
	leaq	.LC74(%rip), %rdi
	call	__GI__dl_fatal_printf
.L1404:
	leaq	.LC103(%rip), %rdi
	call	__GI__dl_fatal_printf
.L683:
	movzbl	-1(%r14), %edx
	leaq	-1(%rax), %rax
	cmpb	$58, %dl
	ja	.L693
	movabsq	$288230380446680576, %rdi
	btq	%rdx, %rdi
	jc	.L688
.L693:
	rdtsc
	salq	$32, %rdx
	movq	%r14, %r15
	orq	%rdx, %rax
	movq	%rax, -616(%rbp)
	jmp	.L697
.L1401:
	movb	$0, -640(%rbp)
	jmp	.L504
.L1387:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC131(%rip), %rdi
	movl	$2342, %edx
	call	__GI___assert_fail
.L1407:
	leaq	__PRETTY_FUNCTION__.11027(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC104(%rip), %rdi
	movl	$1061, %edx
	call	__GI___assert_fail
.L1424:
	cmpb	$45, 1(%r8)
	jne	.L461
	movq	-656(%rbp), %rdi
	movq	%r8, %rsi
	call	_dl_usage
.L1423:
	call	_dl_version
.L1400:
	leaq	__PRETTY_FUNCTION__.11027(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC100(%rip), %rdi
	movl	$993, %edx
	call	__GI___assert_fail
.L1337:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC75(%rip), %rdi
	movl	$1438, %edx
	call	__GI___assert_fail
.L619:
	movq	$0, 696+_rtld_local_ro(%rip)
	movq	$0, 704+_rtld_local_ro(%rip)
.L627:
	movq	$0, 712+_rtld_local_ro(%rip)
	xorl	%eax, %eax
	jmp	.L632
.L1356:
	leaq	.LC84(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L1357:
	leaq	__PRETTY_FUNCTION__.10985(%rip), %rcx
	leaq	.LC82(%rip), %rsi
	leaq	.LC85(%rip), %rdi
	movl	$104, %edx
	call	__GI___assert_fail
.L1358:
	leaq	__PRETTY_FUNCTION__.10985(%rip), %rcx
	leaq	.LC82(%rip), %rsi
	leaq	.LC86(%rip), %rdi
	movl	$105, %edx
	call	__GI___assert_fail
.L1390:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC132(%rip), %rdi
	movl	$2449, %edx
	call	__GI___assert_fail
.L596:
	leaq	__PRETTY_FUNCTION__.10117(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$118, %edx
	call	__GI___assert_fail
.L598:
	leaq	__PRETTY_FUNCTION__.10117(%rip), %rcx
	leaq	.LC24(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$126, %edx
	call	__GI___assert_fail
.L1396:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC111(%rip), %rdi
	movl	$1994, %edx
	call	__GI___assert_fail
.L1395:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC110(%rip), %rdi
	movl	$1990, %edx
	call	__GI___assert_fail
.L1348:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC78(%rip), %rdi
	movl	$1621, %edx
	call	__GI___assert_fail
.L1349:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC79(%rip), %rdi
	movl	$1626, %edx
	call	__GI___assert_fail
.L1364:
	leaq	.LC93(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L1366:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC95(%rip), %rdi
	movl	$1717, %edx
	call	__GI___assert_fail
.L1367:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC96(%rip), %rdi
	movl	$1718, %edx
	call	__GI___assert_fail
.L1353:
	leaq	__PRETTY_FUNCTION__.10985(%rip), %rcx
	leaq	.LC82(%rip), %rsi
	leaq	.LC83(%rip), %rdi
	movl	$61, %edx
	call	__GI___assert_fail
.L651:
	leaq	__PRETTY_FUNCTION__.10700(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC97(%rip), %rdi
	movl	$279, %edx
	call	__GI___assert_fail
.L1338:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC76(%rip), %rdi
	movl	$1446, %edx
	call	__GI___assert_fail
.L1383:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC125(%rip), %rdi
	movl	$2262, %edx
	call	__GI___assert_fail
.L1374:
	leaq	__PRETTY_FUNCTION__.11096(%rip), %rcx
	leaq	.LC9(%rip), %rsi
	leaq	.LC106(%rip), %rdi
	movl	$1817, %edx
	call	__GI___assert_fail
	.size	dl_main, .-dl_main
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10944, @object
	.size	__PRETTY_FUNCTION__.10944, 9
__PRETTY_FUNCTION__.10944:
	.string	"init_tls"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11009, @object
	.size	__PRETTY_FUNCTION__.11009, 20
__PRETTY_FUNCTION__.11009:
	.string	"unload_audit_module"
	.section	.rodata
	.align 32
	.type	audit_iface_names.11036, @object
	.size	audit_iface_names.11036, 118
audit_iface_names.11036:
	.string	"la_activity"
	.string	"la_objsearch"
	.string	"la_objopen"
	.string	"la_preinit"
	.string	"la_symbind64"
	.string	"la_x86_64_gnu_pltenter"
	.string	"la_x86_64_gnu_pltexit"
	.string	"la_objclose"
	.string	""
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.11027, @object
	.size	__PRETTY_FUNCTION__.11027, 18
__PRETTY_FUNCTION__.11027:
	.string	"load_audit_module"
	.align 16
	.type	__PRETTY_FUNCTION__.10700, @object
	.size	__PRETTY_FUNCTION__.10700, 17
__PRETTY_FUNCTION__.10700:
	.string	"audit_list_count"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10985, @object
	.size	__PRETTY_FUNCTION__.10985, 11
__PRETTY_FUNCTION__.10985:
	.string	"setup_vdso"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10117, @object
	.size	__PRETTY_FUNCTION__.10117, 21
__PRETTY_FUNCTION__.10117:
	.string	"elf_get_dynamic_info"
	.set	__PRETTY_FUNCTION__.10838,__PRETTY_FUNCTION__.10117
	.section	.rodata
	.align 32
	.type	debopts.11288, @object
	.size	debopts.11288, 594
debopts.11288:
	.byte	4
	.string	"libs"
	.zero	5
	.string	"display library search paths"
	.zero	12
	.value	3
	.byte	5
	.string	"reloc"
	.zero	4
	.string	"display relocation processing"
	.zero	11
	.value	34
	.byte	5
	.string	"files"
	.zero	4
	.string	"display progress for input file"
	.zero	9
	.value	66
	.byte	7
	.string	"symbols"
	.zero	2
	.string	"display symbol table processing"
	.zero	9
	.value	10
	.byte	8
	.string	"bindings"
	.zero	1
	.string	"display information about symbol binding"
	.value	6
	.byte	8
	.string	"versions"
	.zero	1
	.string	"display version dependencies"
	.zero	12
	.value	18
	.byte	6
	.string	"scopes"
	.zero	3
	.string	"display scope information"
	.zero	15
	.value	512
	.byte	3
	.string	"all"
	.zero	6
	.string	"all previous options combined"
	.zero	11
	.value	639
	.byte	10
	.ascii	"statistics"
	.string	"display relocation statistics"
	.zero	11
	.value	128
	.byte	6
	.string	"unused"
	.zero	3
	.string	"determined unused DSOs"
	.zero	18
	.value	256
	.byte	4
	.string	"help"
	.zero	5
	.string	"display this help message and exit"
	.zero	6
	.value	1024
	.align 32
	.type	unsecure_envvars.11336, @object
	.size	unsecure_envvars.11336, 300
unsecure_envvars.11336:
	.string	"LD_PREFER_MAP_32BIT_EXEC"
	.string	"GCONV_PATH"
	.string	"GETCONF_DIR"
	.string	"HOSTALIASES"
	.string	"LD_AUDIT"
	.string	"LD_DEBUG"
	.string	"LD_DEBUG_OUTPUT"
	.string	"LD_DYNAMIC_WEAK"
	.string	"LD_HWCAP_MASK"
	.string	"LD_LIBRARY_PATH"
	.string	"LD_ORIGIN_PATH"
	.string	"LD_PRELOAD"
	.string	"LD_PROFILE"
	.string	"LD_SHOW_AUXV"
	.string	"LD_USE_LOAD_BIAS"
	.string	"LOCALDOMAIN"
	.string	"LOCPATH"
	.string	"MALLOC_TRACE"
	.string	"NIS_PATH"
	.string	"NLSPATH"
	.string	"RESOLV_HOST_CONF"
	.string	"RES_OPTIONS"
	.string	"TMPDIR"
	.string	"TZDIR"
	.string	""
	.section	.rodata.str1.16
	.align 16
	.type	preload_file.11144, @object
	.size	preload_file.11144, 19
preload_file.11144:
	.string	"/etc/ld.so.preload"
	.local	newname.11121
	.comm	newname.11121,24,16
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11096, @object
	.size	__PRETTY_FUNCTION__.11096, 8
__PRETTY_FUNCTION__.11096:
	.string	"dl_main"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.10780, @object
	.size	__PRETTY_FUNCTION__.10780, 26
__PRETTY_FUNCTION__.10780:
	.string	"elf_machine_rela_relative"
	.align 16
	.type	__PRETTY_FUNCTION__.10855, @object
	.size	__PRETTY_FUNCTION__.10855, 20
__PRETTY_FUNCTION__.10855:
	.string	"elf_dynamic_do_Rela"
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	dyn_temp.10982, @object
	.size	dyn_temp.10982, 128
dyn_temp.10982:
	.zero	128
	.local	tls_init_tp_called
	.comm	tls_init_tp_called,1,1
	.align 8
	.type	start_time, @object
	.size	start_time, 8
start_time:
	.zero	8
	.align 8
	.type	load_time, @object
	.size	load_time, 8
load_time:
	.zero	8
	.local	relocate_time
	.comm	relocate_time,8,8
	.local	_dl_rtld_libname2
	.comm	_dl_rtld_libname2,24,16
	.local	_dl_rtld_libname
	.comm	_dl_rtld_libname,24,16
	.globl	_rtld_global
	.data
	.align 32
	.type	_rtld_global, @object
	.size	_rtld_global, 4152
_rtld_global:
	.zero	40
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.zero	32
	.zero	40
	.zero	2280
	.quad	1
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.zero	1480
	.long	0
	.byte	0
	.zero	3
	.zero	8
	.long	7
	.zero	132
	.globl	_rtld_local
	.hidden	_rtld_local
	.set	_rtld_local,_rtld_global
	.hidden	__pointer_chk_guard_local
	.globl	__pointer_chk_guard_local
	.section	.data.rel.ro
	.align 8
	.type	__pointer_chk_guard_local, @object
	.size	__pointer_chk_guard_local, 8
__pointer_chk_guard_local:
	.zero	8
	.hidden	_dl_skip_args
	.globl	_dl_skip_args
	.align 4
	.type	_dl_skip_args, @object
	.size	_dl_skip_args, 4
_dl_skip_args:
	.zero	4
	.hidden	__GI__dl_argv
	.globl	__GI__dl_argv
	.align 8
	.type	__GI__dl_argv, @object
	.size	__GI__dl_argv, 8
__GI__dl_argv:
	.zero	8
	.globl	_dl_argv
	.set	_dl_argv,__GI__dl_argv
	.hidden	_dl_argc
	.globl	_dl_argc
	.align 4
	.type	_dl_argc, @object
	.size	_dl_argc, 4
_dl_argc:
	.zero	4
	.globl	_rtld_global_ro
	.align 32
	.type	_rtld_global_ro, @object
	.size	_rtld_global_ro, 808
_rtld_global_ro:
	.zero	24
	.quad	4096
	.long	0
	.zero	28
	.long	2
	.long	1
	.zero	8
	.value	895
	.zero	2
	.long	771
	.zero	16
	.zero	432
	.string	"sse2"
	.zero	4
	.string	"x86_64"
	.zero	2
	.string	"avx512_1"
	.string	"i586"
	.zero	4
	.string	"i686"
	.zero	4
	.string	"haswell"
	.zero	1
	.string	"xeon_phi"
	.zero	17
	.quad	-2
	.zero	112
	.quad	_dl_debug_printf
	.quad	__GI__dl_mcount
	.quad	_dl_lookup_symbol_x
	.quad	_dl_open
	.quad	_dl_close
	.quad	_dl_tls_get_addr_soft
	.quad	_dl_discover_osversion
	.zero	16
	.globl	_rtld_local_ro
	.hidden	_rtld_local_ro
	.set	_rtld_local_ro,_rtld_global_ro
	.hidden	_dl_tls_get_addr_soft
	.hidden	_dl_version
	.hidden	_dl_show_scope
	.hidden	__munmap
	.hidden	strsep
	.hidden	_dl_sysdep_read_whole_file
	.hidden	rawmemchr
	.hidden	unsetenv
	.hidden	_dl_help
	.hidden	_dl_strtoul
	.hidden	_dl_usage
	.hidden	_dl_add_to_slotinfo
	.hidden	_dl_unload_cache
	.hidden	_dl_sysdep_start_cleanup
	.hidden	_dl_call_libc_early_init
	.hidden	_dl_start_profile
	.hidden	_dl_protect_relro
	.hidden	__rtld_malloc_init_real
	.hidden	_dl_resolve_conflicts
	.hidden	_dl_next_tls_modid
	.hidden	_dl_name_match_p
	.hidden	_dl_receive_error
	.hidden	_dl_map_object_deps
	.hidden	__access
	.hidden	_dl_count_modids
	.hidden	__rtld_free
	.hidden	_dl_random
	.hidden	__ehdr_start
	.hidden	_dl_debug_initialize
	.hidden	_dl_init_paths
	.hidden	_dl_discover_osversion
	.hidden	__rtld_malloc
	.hidden	_dl_add_to_namespace_list
	.hidden	_dl_new_object
	.hidden	_dl_show_auxv
	.hidden	__getpid
	.hidden	strcmp
	.hidden	_start
	.hidden	_dl_next_ld_env_entry
	.hidden	_environ
	.hidden	_dl_nothread_init_static_tls
	.hidden	_dl_tlsdesc_return
	.hidden	_dl_sysdep_start
	.hidden	_etext
	.hidden	_end
	.hidden	_begin
	.hidden	_dl_setup_hash
	.hidden	__rtld_malloc_init_stubs
	.hidden	_GLOBAL_OFFSET_TABLE_
	.hidden	_DYNAMIC
	.hidden	_dl_printf
	.hidden	strnlen
	.hidden	_dl_check_all_versions
	.hidden	_dl_close
	.hidden	_dl_allocate_tls_storage
	.hidden	_dl_determine_tlsoffset
	.hidden	_dl_tls_static_surplus_init
	.hidden	__rtld_calloc
	.hidden	memchr
	.hidden	strlen
	.hidden	_dl_error_printf
	.hidden	_dl_relocate_object
	.hidden	_dl_debug_printf
	.hidden	_dl_map_object
	.hidden	_dl_lookup_symbol_x
	.hidden	_dl_open
