	.text
	.p2align 4,,15
	.globl	_dl_next_ld_env_entry
	.hidden	_dl_next_ld_env_entry
	.type	_dl_next_ld_env_entry, @function
_dl_next_ld_env_entry:
	movq	(%rdi), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$8, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L1
.L4:
	cmpb	$76, (%rax)
	jne	.L3
	cmpb	$68, 1(%rax)
	jne	.L3
	cmpb	$95, 2(%rax)
	jne	.L3
	addq	$8, %rdx
	addq	$3, %rax
	movq	%rdx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	_dl_next_ld_env_entry, .-_dl_next_ld_env_entry
	.p2align 4,,15
	.globl	unsetenv
	.hidden	unsetenv
	.type	unsetenv, @function
unsetenv:
	movq	__environ(%rip), %r8
.L11:
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	je	.L17
.L23:
	movzbl	(%rsi), %edx
	movzbl	(%rdi), %ecx
	cmpb	%cl, %dl
	jne	.L12
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %rax
	movzbl	(%rsi,%rax), %edx
	movzbl	(%rdi,%rax), %ecx
	cmpb	%cl, %dl
	jne	.L12
.L22:
	testb	%dl, %dl
	jne	.L13
.L14:
	addq	$8, %r8
	movq	(%r8), %rsi
	testq	%rsi, %rsi
	jne	.L23
.L17:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	testb	%cl, %cl
	jne	.L14
	cmpb	$61, %dl
	jne	.L14
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L15:
	movq	8(%rax), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	testq	%rdx, %rdx
	jne	.L15
	jmp	.L11
	.size	unsetenv, .-unsetenv
	.hidden	__environ
