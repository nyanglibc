	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: cannot open file: %s\n"
.LC1:
	.string	"%s: cannot stat file: %s\n"
.LC2:
	.string	"%s: cannot map file: %s\n"
.LC3:
	.string	"%s: cannot create file: %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"%s: file is no correct profile data file for `%s'\n"
	.align 8
.LC5:
	.string	"Out of memory while initializing profiler\n"
	.text
	.p2align 4,,15
	.globl	_dl_start_profile
	.hidden	_dl_start_profile
	.type	_dl_start_profile, @function
_dl_start_profile:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$680, %rsp
	movq	_dl_profile_map(%rip), %r10
	movzwl	696(%r10), %ecx
	movq	680(%r10), %rax
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rdi
	cmpq	%rdi, %rax
	jnb	.L34
	movq	_dl_pagesize(%rip), %r11
	movq	$-1, %rcx
	xorl	%esi, %esi
	movabsq	$8589934591, %r9
	movabsq	$4294967297, %r8
	movq	%r11, %rbx
	subq	$1, %r11
	negq	%rbx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rax), %rdx
	andq	%r9, %rdx
	cmpq	%r8, %rdx
	jne	.L3
	movq	16(%rax), %rdx
	movq	%rdx, %r12
	andq	%rbx, %r12
	cmpq	%r12, %rcx
	cmova	%r12, %rcx
	addq	40(%rax), %rdx
	addq	%r11, %rdx
	andq	%rbx, %rdx
	cmpq	%rdx, %rsi
	cmovb	%rdx, %rsi
.L3:
	addq	$56, %rax
	cmpq	%rdi, %rax
	jb	.L4
	movq	%rcx, %rdi
	movq	%rsi, %r8
.L2:
	movq	(%r10), %rax
	movl	$0, running(%rip)
	movl	$5, log_hashfraction(%rip)
	leaq	3(%rax,%rsi), %r13
	addq	%rax, %rcx
	andq	$-4, %rcx
	andq	$-4, %r13
	movq	%rcx, lowpc(%rip)
	movq	%r13, %rbx
	subq	%rcx, %rbx
	movabsq	$2951479051793528259, %rcx
	leaq	(%rbx,%rbx,2), %rdx
	movq	%rbx, textsize(%rip)
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rcx
	shrq	$2, %rdx
	cmpl	$49, %edx
	ja	.L5
	movl	$50, fromlimit(%rip)
	movq	$800, -696(%rbp)
	movq	$16072, -688(%rbp)
.L6:
	movq	%rbx, %rax
	movq	%r8, -632(%rbp)
	movq	%rdi, -640(%rbp)
	shrq	$2, %rax
	movl	$1852796263, -672(%rbp)
	movl	$131071, -668(%rbp)
	movq	$0, -664(%rbp)
	movl	$0, -656(%rbp)
	leaq	-640(%rbp), %r14
	movl	%eax, -624(%rbp)
	call	__profile_frequency
	movq	_dl_profile_output(%rip), %rsi
	xorl	%edx, %edx
	movl	$0, -608(%rbp)
	movl	%eax, -620(%rbp)
	movw	%dx, 36(%r14)
	movabsq	$32480047799690611, %rax
	movb	$0, 38(%r14)
	movq	%rax, -616(%rbp)
	movq	%rsi, %rdi
	movq	%rsi, -680(%rbp)
	movb	$115, -601(%rbp)
	call	strlen
	movq	_dl_profile(%rip), %r15
	movq	%rax, %r12
	movq	%r15, %rdi
	call	strlen
	leaq	40(%r12,%rax), %rax
	movq	-680(%rbp), %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	movq	%r12, %rdi
	call	__stpcpy@PLT
	leaq	1(%rax), %rdi
	movq	%r15, %rsi
	movb	$47, (%rax)
	call	__stpcpy@PLT
	movabsq	$7308332183992823854, %rcx
	movb	$0, 8(%rax)
	movl	$438, %edx
	movq	%rcx, (%rax)
	movl	$131138, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	__open64_nocancel
	cmpl	$-1, %eax
	movl	%eax, %r15d
	jne	.L53
	movq	__libc_errno@gottpoff(%rip), %rax
	leaq	.LC0(%rip), %r13
	movl	%fs:(%rax), %ebx
.L33:
	leaq	-448(%rbp), %rsi
	movl	$400, %edx
	movl	%ebx, %edi
	call	__strerror_r
	movq	%r12, %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_dl_error_printf
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1048576, %edx
	jbe	.L54
	movl	$1048576, fromlimit(%rip)
	movq	$16777216, -696(%rbp)
	movq	$335544392, -688(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	-592(%rbp), %rsi
	movl	%eax, %edi
	call	__fstat64
	testl	%eax, %eax
	js	.L36
	movl	-568(%rbp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L55
.L36:
	movq	__libc_errno@gottpoff(%rip), %rax
	leaq	.LC1(%rip), %r13
.L11:
	movl	%r15d, %edi
	movl	%fs:(%rax), %ebx
	call	__close_nocancel
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	movq	$-1, %rdi
	xorl	%r8d, %r8d
	xorl	%esi, %esi
	movq	%rdi, %rcx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L55:
	movq	-688(%rbp), %rcx
	movq	%rbx, %rax
	shrq	%rax
	movq	%rax, -704(%rbp)
	addq	%rax, %rcx
	movq	-544(%rbp), %rax
	movq	%rcx, -680(%rbp)
	testq	%rax, %rax
	je	.L56
	cmpq	-680(%rbp), %rax
	jne	.L57
.L16:
	movq	-680(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r15d, %r8d
	movl	$1, %ecx
	movl	$3, %edx
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, -688(%rbp)
	je	.L37
	movl	%r15d, %edi
	call	__close_nocancel
	movq	-688(%rbp), %rdx
	movq	-704(%rbp), %rax
	cmpq	$0, -544(%rbp)
	leaq	64(%rdx), %r15
	leaq	4(%r15,%rax), %r9
	leaq	4(%r9), %rax
	movq	%r9, narcsp(%rip)
	movq	%rax, data(%rip)
	jne	.L19
	movdqa	-672(%rbp), %xmm0
	movl	-656(%rbp), %eax
	movl	$0, 20(%rdx)
	movups	%xmm0, (%rdx)
	movdqa	-640(%rbp), %xmm0
	movl	%eax, 16(%rdx)
	movq	-608(%rbp), %rax
	movups	%xmm0, 24(%rdx)
	movdqa	-624(%rbp), %xmm0
	movq	%rax, 56(%rdx)
	movups	%xmm0, 40(%rdx)
	movl	$1, -4(%r9)
.L20:
	movl	$1, %esi
	movq	-696(%rbp), %rdi
	addq	-704(%rbp), %rdi
	movq	%rdx, -712(%rbp)
	movq	%r9, -688(%rbp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, tos(%rip)
	movq	-688(%rbp), %r9
	movq	-712(%rbp), %rdx
	je	.L58
	movq	-704(%rbp), %rcx
	movl	fromlimit(%rip), %edx
	movl	$0, fromidx(%rip)
	leaq	(%rax,%rcx), %r11
	movl	(%r9), %ecx
	movq	%r11, froms(%rip)
	cmpl	%edx, %ecx
	jnb	.L26
	movl	(%r9), %edx
.L26:
	testq	%rdx, %rdx
	movl	%edx, narcs(%rip)
	je	.L27
	leaq	(%rdx,%rdx,4), %rdx
	leaq	-16(%r9,%rdx,4), %rcx
	subq	$16, %r9
	.p2align 4,,10
	.p2align 3
.L28:
	movq	8(%rcx), %rdi
	movl	fromidx(%rip), %esi
	shrq	$2, %rdi
	leal	1(%rsi), %edx
	leaq	(%rax,%rdi,2), %rdi
	movl	%edx, fromidx(%rip)
	movl	%esi, %edx
	salq	$4, %rdx
	movzwl	(%rdi), %r8d
	addq	%r11, %rdx
	movq	%rcx, (%rdx)
	subq	$20, %rcx
	cmpq	%rcx, %r9
	movw	%r8w, 8(%rdx)
	movw	%si, (%rdi)
	jne	.L28
.L27:
	movq	lowpc(%rip), %rsi
	movq	-704(%rbp), %rdi
	movl	$65536, %ecx
	subq	%rsi, %r13
	cmpq	%rdi, %r13
	jbe	.L29
	xorl	%edx, %edx
	movq	%r13, %rax
	movl	$1, %ecx
	divq	%rdi
	cmpq	$65535, %rax
	movq	%rax, %rdi
	ja	.L29
	cmpq	$255, %rax
	jbe	.L30
	movl	$65536, %eax
	xorl	%edx, %edx
	divq	%rdi
	movl	%eax, %ecx
.L29:
	movq	%rsi, %rdx
	movq	-704(%rbp), %rsi
	movq	%r15, %rdi
	call	__profil
	movl	$1, running(%rip)
	jmp	.L1
.L57:
	movl	%r15d, %edi
	call	__close_nocancel
.L17:
	movq	_dl_profile(%rip), %rdx
	leaq	.LC4(%rip), %rdi
	movq	%r12, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L1
.L37:
	leaq	.LC2(%rip), %r13
	movq	__libc_errno@gottpoff(%rip), %rax
	jmp	.L11
.L56:
	movq	_dl_pagesize(%rip), %rcx
	movq	%rsp, -712(%rbp)
	xorl	%esi, %esi
	leaq	15(%rcx), %rax
	movq	%rcx, %rdx
	movq	%rcx, -720(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	movq	%rsp, -688(%rbp)
	call	memset@PLT
	movq	-720(%rbp), %rcx
	xorl	%edx, %edx
	movl	%r15d, %edi
	negq	%rcx
	andq	-680(%rbp), %rcx
	movq	%rcx, %rsi
	call	__lseek
	cmpq	$-1, %rax
	jne	.L13
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L15
.L13:
	movq	_dl_pagesize(%rip), %rax
	movq	-688(%rbp), %rsi
	movl	%r15d, %edi
	leaq	-1(%rax), %rdx
	andq	-680(%rbp), %rdx
	call	__write_nocancel
	cmpq	$-1, %rax
	je	.L59
	testq	%rax, %rax
	js	.L51
	movq	-712(%rbp), %rsp
	jmp	.L16
.L51:
	movq	__libc_errno@gottpoff(%rip), %rax
.L15:
	leaq	.LC3(%rip), %r13
	movq	-712(%rbp), %rsp
	jmp	.L11
.L19:
	movq	(%rdx), %rax
	movq	8(%rdx), %rcx
	xorq	-672(%rbp), %rax
	xorq	-664(%rbp), %rcx
	orq	%rax, %rcx
	jne	.L18
	movl	-656(%rbp), %eax
	cmpl	%eax, 16(%rdx)
	je	.L60
.L18:
	movq	-680(%rbp), %rsi
	movq	%rdx, %rdi
	call	__munmap
	jmp	.L17
.L60:
	movl	20(%rdx), %eax
	testl	%eax, %eax
	jne	.L18
	movq	24(%rdx), %rcx
	movq	32(%rdx), %rsi
	xorq	-640(%rbp), %rcx
	xorq	-632(%rbp), %rsi
	orq	%rcx, %rsi
	jne	.L18
	movq	40(%rdx), %rcx
	movq	48(%rdx), %rsi
	xorq	-624(%rbp), %rcx
	xorq	-616(%rbp), %rsi
	orq	%rcx, %rsi
	jne	.L18
	movq	32(%r14), %rcx
	cmpq	%rcx, 56(%rdx)
	jne	.L18
	movl	-4(%r9), %eax
	subl	$1, %eax
	jne	.L18
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	movabsq	$72057594037927935, %rax
	cmpq	%rax, %r13
	jbe	.L31
	movq	%r13, %rax
	shrq	$9, %rbx
	xorl	%edx, %edx
	divq	%rbx
	xorl	%edx, %edx
	movq	%rax, %r13
	movl	$16777216, %eax
	divq	%r13
	movl	%eax, %ecx
	jmp	.L29
.L31:
	salq	$8, %r13
	xorl	%edx, %edx
	movq	%r13, %rax
	divq	-704(%rbp)
	xorl	%edx, %edx
	movq	%rax, %rcx
	movl	$16777216, %eax
	divq	%rcx
	movl	%eax, %ecx
	jmp	.L29
.L58:
	movq	-680(%rbp), %rsi
	movq	%rdx, %rdi
	call	__munmap
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
.L54:
	movl	%edx, fromlimit(%rip)
	movl	%edx, %edx
	salq	$4, %rdx
	imulq	$20, %rdx, %r15
	movq	%rdx, -696(%rbp)
	leaq	72(%r15), %rax
	movq	%rax, -688(%rbp)
	jmp	.L6
	.size	_dl_start_profile, .-_dl_start_profile
	.p2align 4,,15
	.globl	_dl_mcount
	.type	_dl_mcount, @function
_dl_mcount:
	movl	running(%rip), %ecx
	testl	%ecx, %ecx
	je	.L88
	movq	lowpc(%rip), %rdx
	movq	textsize(%rip), %rax
	movl	$0, %ecx
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	cmovbe	%rcx, %rdi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L88
	movl	log_hashfraction(%rip), %ecx
	movq	%rsi, %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	shrq	%cl, %rax
	movq	tos(%rip), %rcx
	leaq	(%rcx,%rax,2), %r10
	movzwl	(%r10), %eax
	testq	%rax, %rax
	jne	.L91
.L66:
	movq	narcsp(%rip), %r11
	movl	narcs(%rip), %eax
	movl	(%r11), %edx
	cmpl	%eax, %edx
	je	.L71
	cmpl	%eax, fromlimit(%rip)
	jbe	.L71
	movq	froms(%rip), %r8
	movq	data(%rip), %r9
	movl	$1, %ebx
.L72:
	leaq	(%rax,%rax,4), %rax
	movq	8(%r9,%rax,4), %rbp
	movl	%ebx, %eax
	shrq	$2, %rbp
#APP
# 552 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %eax, fromidx(%rip)
# 0 "" 2
#NO_APP
	movl	narcs(%rip), %r12d
	movl	%eax, -12(%rsp)
	leaq	(%rcx,%rbp,2), %rbp
	movl	-12(%rsp), %edx
	leaq	(%r12,%r12,4), %r12
	leal	1(%rdx), %eax
	leaq	(%r9,%r12,4), %r12
	movq	%rax, %rdx
	salq	$4, %rax
	addq	%r8, %rax
	movq	%r12, (%rax)
	movzwl	0(%rbp), %r12d
	movw	%r12w, 8(%rax)
	movw	%dx, 0(%rbp)
#APP
# 556 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incl narcs(%rip)
# 0 "" 2
#NO_APP
	movl	narcs(%rip), %eax
	movl	(%r11), %edx
	cmpl	%eax, %edx
	je	.L71
	cmpl	fromlimit(%rip), %eax
	jb	.L72
.L71:
	movzwl	(%r10), %eax
	testw	%ax, %ax
	jne	.L73
	movl	$1, %eax
	movl	%eax, %edx
#APP
# 562 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %edx, (%r11)
# 0 "" 2
#NO_APP
	movl	%edx, -8(%rsp)
	movl	-8(%rsp), %edx
	cmpl	%edx, fromlimit(%rip)
	jbe	.L61
#APP
# 569 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %eax, fromidx(%rip)
# 0 "" 2
#NO_APP
	movl	%eax, -4(%rsp)
	movl	-4(%rsp), %eax
	leaq	(%rdx,%rdx,4), %rcx
	movq	data(%rip), %rdx
	addl	$1, %eax
	movw	%ax, (%r10)
	movzwl	(%r10), %eax
	leaq	(%rdx,%rcx,4), %rdx
	salq	$4, %rax
	addq	froms(%rip), %rax
	movq	%rdx, (%rax)
	movq	%rdi, (%rdx)
	movq	%rsi, 8(%rdx)
	movl	$0, 16(%rdx)
	xorl	%edx, %edx
	movw	%dx, 8(%rax)
#APP
# 577 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incl narcs(%rip)
# 0 "" 2
#NO_APP
	movq	(%rax), %r9
.L70:
#APP
# 590 "dl-profile.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incl 16(%r9)
# 0 "" 2
.L63:
#NO_APP
.L61:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	rep ret
	.p2align 4,,10
	.p2align 3
.L73:
	movzwl	(%r10), %eax
.L91:
	movq	froms(%rip), %r8
	salq	$4, %rax
	addq	%r8, %rax
	movq	(%rax), %r9
	movq	(%r9), %rdx
	cmpq	%rdi, %rdx
	je	.L70
	movzwl	8(%rax), %edx
	testw	%dx, %dx
	jne	.L69
.L68:
	movq	(%r9), %rdx
	cmpq	%rdi, %rdx
	je	.L70
	leaq	8(%rax), %r10
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L92:
	movq	(%r9), %r10
	cmpq	%rdi, %r10
	je	.L68
.L69:
	salq	$4, %rdx
	leaq	(%r8,%rdx), %rax
	movzwl	8(%rax), %edx
	movq	(%rax), %r9
	testw	%dx, %dx
	jne	.L92
	jmp	.L68
	.size	_dl_mcount, .-_dl_mcount
	.local	log_hashfraction
	.comm	log_hashfraction,4,4
	.local	textsize
	.comm	textsize,8,8
	.local	lowpc
	.comm	lowpc,8,8
	.local	fromidx
	.comm	fromidx,4,4
	.local	fromlimit
	.comm	fromlimit,4,4
	.local	froms
	.comm	froms,8,8
	.local	tos
	.comm	tos,8,8
	.local	narcsp
	.comm	narcsp,8,8
	.local	narcs
	.comm	narcs,4,4
	.local	running
	.comm	running,4,4
	.local	data
	.comm	data,8,8
	.hidden	__munmap
	.hidden	__write_nocancel
	.hidden	__lseek
	.hidden	__profil
	.hidden	__mmap
	.hidden	__close_nocancel
	.hidden	__fstat64
	.hidden	_dl_error_printf
	.hidden	__strerror_r
	.hidden	__open64_nocancel
	.hidden	strlen
	.hidden	__profile_frequency
