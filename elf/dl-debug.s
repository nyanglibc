	.text
	.p2align 4,,15
	.globl	_dl_debug_state
	.type	_dl_debug_state, @function
_dl_debug_state:
	rep ret
	.size	_dl_debug_state, .-_dl_debug_state
	.p2align 4,,15
	.globl	_dl_debug_initialize
	.hidden	_dl_debug_initialize
	.type	_dl_debug_initialize, @function
_dl_debug_initialize:
	testq	%rsi, %rsi
	leaq	_r_debug(%rip), %rax
	je	.L4
	leaq	(%rsi,%rsi,8), %rax
	leaq	(%rsi,%rax,2), %rdx
	leaq	_dl_ns(%rip), %rax
	leaq	112(%rax,%rdx,8), %rax
.L4:
	cmpq	$0, 8(%rax)
	je	.L5
	testq	%rdi, %rdi
	je	.L11
	movl	$1, (%rax)
.L8:
	leaq	(%rsi,%rsi,8), %rdx
	movq	%rdi, 32(%rax)
	leaq	(%rsi,%rdx,2), %rcx
	leaq	_dl_ns(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	leaq	_dl_debug_state(%rip), %rcx
	movq	%rcx, 16(%rax)
	movq	%rdx, 8(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	testq	%rdi, %rdi
	movl	$1, (%rax)
	jne	.L8
	movq	32+_r_debug(%rip), %rdi
	jmp	.L8
	.size	_dl_debug_initialize, .-_dl_debug_initialize
	.comm	_r_debug,40,32
