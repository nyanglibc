	.text
	.p2align 4,,15
	.globl	_dl_scope_free
	.hidden	_dl_scope_free
	.type	_dl_scope_free, @function
_dl_scope_free:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
#APP
# 30 "dl-scope.c" 1
	movl %fs:24,%ebx
# 0 "" 2
#NO_APP
	testl	%ebx, %ebx
	jne	.L2
	call	*__rtld_free(%rip)
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	4104+_rtld_local(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L4
	movl	$408, %edi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, 4104+_rtld_local(%rip)
	je	.L15
	movq	%rbp, 8(%rax)
	movq	$1, (%rax)
	xorl	%ebx, %ebx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rbx), %rax
	cmpq	$49, %rax
	ja	.L6
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movq	%rdi, 8(%rbx,%rax,8)
	xorl	%ebx, %ebx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	call	__thread_gscope_wait
	movl	$1, %ebx
	movq	%rbp, %rdi
	call	*__rtld_free(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	call	__thread_gscope_wait
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	subq	$1, %rax
	movq	%rax, (%rbx)
	movq	8(%rbx,%rax,8), %rdi
	call	*__rtld_free(%rip)
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L8
.L7:
	movl	$1, %ebx
	jmp	.L1
	.size	_dl_scope_free, .-_dl_scope_free
	.hidden	__thread_gscope_wait
	.hidden	__rtld_malloc
	.hidden	_rtld_local
	.hidden	__rtld_free
