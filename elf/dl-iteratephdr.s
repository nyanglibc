	.text
	.p2align 4,,15
	.globl	__dl_iterate_phdr
	.hidden	__dl_iterate_phdr
	.type	__dl_iterate_phdr, @function
__dl_iterate_phdr:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$72, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	_dl_load_write_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	movq	_dl_ns(%rip), %r14
	movl	8+_dl_ns(%rip), %ebx
	testq	%r14, %r14
	je	.L3
	movq	%rsp, %r13
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r12, %rdx
	movl	$64, %esi
	movq	%r13, %rdi
	call	*%rbp
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L5
	movq	24(%r14), %r14
	testq	%r14, %r14
	je	.L3
.L6:
	movq	40(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rax, (%rsp)
	movq	8(%rdi), %rax
	movq	$0, 56(%rsp)
	movq	%rax, 8(%rsp)
	movq	680(%rdi), %rax
	movq	%rax, 16(%rsp)
	movzwl	696(%rdi), %eax
	movw	%ax, 24(%rsp)
	movq	_dl_load_adds(%rip), %rax
	movq	%rax, 32(%rsp)
	subq	%rbx, %rax
	movq	%rax, 40(%rsp)
	movq	1120(%rdi), %rax
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L4
	call	_dl_tls_get_addr_soft
	movq	%rax, 56(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%r15d, %r15d
.L5:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	_dl_load_write_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__dl_iterate_phdr, .-__dl_iterate_phdr
	.weak	dl_iterate_phdr
	.set	dl_iterate_phdr,__dl_iterate_phdr
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	_dl_tls_get_addr_soft
