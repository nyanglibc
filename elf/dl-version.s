	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"unsupported version %s of Verneed record"
	.section	.rodata.str1.1
.LC2:
	.string	"dl-version.c"
.LC3:
	.string	"needed != NULL"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"checking for version `%s' in file %s [%lu] required by file %s [%lu]\n"
	.align 8
.LC5:
	.string	"no version information available (required by %s)"
	.section	.rodata.str1.1
.LC6:
	.string	"def_offset != 0"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"unsupported version %s of Verdef record"
	.align 8
.LC8:
	.string	"weak version `%s' not found (required by %s)"
	.align 8
.LC9:
	.string	"version `%s' not found (required by %s)"
	.section	.rodata.str1.1
.LC10:
	.string	"version lookup error"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"cannot allocate version reference table"
	.text
	.p2align 4,,15
	.globl	_dl_check_map_versions
	.hidden	_dl_check_map_versions
	.type	_dl_check_map_versions, @function
_dl_check_map_versions:
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L104
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$152, %rsp
	movq	8(%rax), %r15
	movq	352(%rdi), %rax
	movq	368(%rdi), %rdi
	movl	%edx, 64(%rsp)
	movl	%esi, 68(%rsp)
	testq	%rax, %rax
	movq	%rax, 72(%rsp)
	movq	%rdi, 56(%rsp)
	je	.L3
	movq	8(%rax), %rax
	addq	0(%rbp), %rax
	xorl	%r14d, %r14d
	cmpw	$1, (%rax)
	movq	%rax, 32(%rsp)
	jne	.L109
	movq	%r15, 16(%rsp)
	movl	%r14d, %r15d
.L4:
	movq	32(%rsp), %rax
	leaq	_dl_ns(%rip), %rdi
	movl	4(%rax), %r12d
	movq	48(%rbp), %rax
	addq	16(%rsp), %r12
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	(%rdi,%rax,8), %rbx
	testq	%rbx, %rbx
	jne	.L9
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L110:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L7
.L9:
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L110
.L8:
	movl	64(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L111
.L13:
	movq	32(%rsp), %rax
	movq	%rbx, 24(%rsp)
	movl	8(%rax), %r12d
	addq	%rax, %r12
.L31:
	movzwl	4(%r12), %eax
	movq	8(%rbp), %r14
	movl	(%r12), %ebx
	movw	%ax, 46(%rsp)
	movq	24(%rsp), %rax
	movq	40(%rax), %r13
	movl	8(%r12), %eax
	addq	16(%rsp), %rax
	cmpb	$0, (%r14)
	movq	%rax, (%rsp)
	jne	.L15
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %r14
	leaq	.LC0(%rip), %rax
	testq	%r14, %r14
	cmove	%rax, %r14
.L15:
	movq	104(%r13), %rax
	testb	$16, _dl_debug_mask(%rip)
	movq	8(%rax), %rax
	movq	%rax, 8(%rsp)
	jne	.L112
.L16:
	movq	368(%r13), %rax
	testq	%rax, %rax
	je	.L113
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L114
	addq	0(%r13), %rdx
	cmpw	$1, (%rdx)
	jne	.L27
	movq	%r14, 48(%rsp)
	movq	%rbp, %r14
	movl	%ebx, %ebp
	movq	%rdx, %rbx
.L23:
	cmpl	8(%rbx), %ebp
	je	.L115
.L25:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	je	.L26
	addq	%rax, %rbx
	cmpw	$1, (%rbx)
	je	.L23
	movq	%rbx, %rdx
.L27:
	movb	$0, 99(%rsp)
	movzwl	(%rdx), %edi
	leaq	99(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	call	_itoa_word
	movq	8(%r13), %rsi
	cmpb	$0, (%rsi)
	jne	.L24
	movq	_dl_argv(%rip), %rdx
	movq	(%rdx), %rsi
	leaq	.LC0(%rip), %rdx
	testq	%rsi, %rsi
	cmove	%rdx, %rsi
.L24:
	leaq	112(%rsp), %rbx
	leaq	.LC7(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_dl_exception_create_format@PLT
.L21:
	leaq	.LC10(%rip), %rdx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_dl_signal_exception
	.p2align 4,,10
	.p2align 3
.L115:
	movl	12(%rbx), %eax
	movq	(%rsp), %rdi
	movl	(%rbx,%rax), %esi
	addq	8(%rsp), %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L25
	movq	%r14, %rbp
.L19:
	movzwl	6(%r12), %eax
	andl	$32767, %eax
	cmpl	%eax, %r15d
	cmovb	%eax, %r15d
	movl	12(%r12), %eax
	testl	%eax, %eax
	je	.L14
	addq	%rax, %r12
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L7:
	movl	712(%rbp), %esi
	testl	%esi, %esi
	je	.L10
	xorl	%r13d, %r13d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	addl	$1, %r13d
	cmpl	712(%rbp), %r13d
	jnb	.L10
.L12:
	movq	704(%rbp), %rdx
	movl	%r13d, %eax
	movq	%r12, %rdi
	leaq	0(,%rax,8), %rbx
	movq	(%rdx,%rax,8), %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L11
	movq	704(%rbp), %rax
	movq	(%rax,%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	__PRETTY_FUNCTION__.9166(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$205, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L111:
	testb	$2, 797(%rbx)
	je	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movq	32(%rsp), %rcx
	movl	12(%rcx), %eax
	testl	%eax, %eax
	je	.L32
	addq	%rax, %rcx
	movq	%rcx, 32(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%r14d, %r14d
	cmpq	$0, 56(%rsp)
	je	.L2
.L33:
	movq	56(%rsp), %rax
	movq	8(%rax), %rdx
	addq	0(%rbp), %rdx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L116:
	addq	%rax, %rdx
.L107:
	movzwl	4(%rdx), %eax
	andl	$32767, %eax
	cmpl	%eax, %r14d
	cmovb	%eax, %r14d
	movl	16(%rdx), %eax
	testl	%eax, %eax
	jne	.L116
.L34:
	testl	%r14d, %r14d
	je	.L2
	leal	1(%r14), %edi
	movl	$24, %esi
	movq	%rdi, %rbx
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 744(%rbp)
	je	.L117
	movq	464(%rbp), %rdx
	movq	72(%rsp), %rdi
	movl	%ebx, 752(%rbp)
	movq	8(%rdx), %rdx
	testq	%rdi, %rdi
	movq	%rdx, 840(%rbp)
	je	.L38
	movq	8(%rdi), %rdi
	addq	0(%rbp), %rdi
	.p2align 4,,10
	.p2align 3
.L42:
	movl	8(%rdi), %ecx
	addq	%rdi, %rcx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L118:
	addq	%rdx, %rcx
.L41:
	movzwl	6(%rcx), %edx
	movl	%edx, %esi
	andw	$32767, %si
	movzwl	%si, %r8d
	cmpl	%r8d, %ebx
	jbe	.L39
	movzwl	%si, %esi
	andw	$-32768, %dx
	movl	(%rcx), %r8d
	leaq	(%rsi,%rsi,2), %rsi
	movzwl	%dx, %edx
	leaq	(%rax,%rsi,8), %rsi
	movl	%edx, 12(%rsi)
	movl	8(%rcx), %edx
	movl	%r8d, 8(%rsi)
	addq	%r15, %rdx
	movq	%rdx, (%rsi)
	movl	4(%rdi), %edx
	addq	%r15, %rdx
	movq	%rdx, 16(%rsi)
.L39:
	movl	12(%rcx), %edx
	testl	%edx, %edx
	jne	.L118
	movl	12(%rdi), %edx
	testl	%edx, %edx
	je	.L38
	addq	%rdx, %rdi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L38:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L2
	movq	8(%rdi), %rdx
	addq	0(%rbp), %rdx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L119:
	addq	%rcx, %rdx
.L44:
	testb	$1, 2(%rdx)
	jne	.L43
	movl	12(%rdx), %esi
	movzwl	4(%rdx), %ecx
	movl	8(%rdx), %edi
	movl	(%rdx,%rsi), %esi
	andl	$32767, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	addq	%r15, %rsi
	movl	%edi, 8(%rcx)
	movq	%rsi, (%rcx)
	movq	$0, 16(%rcx)
.L43:
	movl	16(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L119
.L2:
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movb	$0, 99(%rsp)
	movzwl	(%rax), %edi
	leaq	99(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	call	_itoa_word
	movq	8(%rbp), %rsi
	cmpb	$0, (%rsi)
	je	.L120
.L5:
	leaq	112(%rsp), %rbx
	leaq	.LC1(%rip), %rdx
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_dl_exception_create_format@PLT
	xorl	%edi, %edi
.L6:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_dl_signal_exception
	.p2align 4,,10
	.p2align 3
.L113:
	movl	68(%rsp), %edx
	testl	%edx, %edx
	je	.L19
	movq	8(%r13), %rsi
	movq	%r14, %r10
	cmpb	$0, (%rsi)
	jne	.L20
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L20
	leaq	.LC0(%rip), %rsi
.L20:
	leaq	112(%rsp), %rbx
	leaq	.LC5(%rip), %rdx
	movq	%r10, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_dl_exception_create_format@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L112:
	movq	8(%r13), %rdx
	movq	48(%r13), %rcx
	cmpb	$0, (%rdx)
	jne	.L17
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rdx
	leaq	.LC0(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L17:
	movq	48(%rbp), %r9
	movq	(%rsp), %rsi
	leaq	.LC4(%rip), %rdi
	movq	%r14, %r8
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L26:
	testb	$2, 46(%rsp)
	movq	%r14, %rbp
	movq	48(%rsp), %r14
	je	.L28
	movl	68(%rsp), %eax
	testl	%eax, %eax
	je	.L19
	movq	8(%r13), %rsi
	movq	%r14, %r10
	cmpb	$0, (%rsi)
	jne	.L29
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L29
	leaq	.LC0(%rip), %rsi
.L29:
	leaq	112(%rsp), %rbx
	movq	(%rsp), %rcx
	leaq	.LC8(%rip), %rdx
	movq	%r10, %r8
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_dl_exception_create_format@PLT
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%eax, %eax
	ret
.L120:
	movq	_dl_argv(%rip), %rdx
	movq	(%rdx), %rsi
	leaq	.LC0(%rip), %rdx
	testq	%rsi, %rsi
	cmove	%rdx, %rsi
	jmp	.L5
.L32:
	cmpq	$0, 56(%rsp)
	movl	%r15d, %r14d
	movq	16(%rsp), %r15
	jne	.L33
	jmp	.L34
.L28:
	movq	8(%r13), %rsi
	movq	%r14, %r10
	cmpb	$0, (%rsi)
	jne	.L30
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L30:
	leaq	112(%rsp), %rbx
	movq	(%rsp), %rcx
	leaq	.LC9(%rip), %rdx
	movq	%r10, %r8
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_dl_exception_create_format@PLT
	jmp	.L21
.L117:
	movq	8(%rbp), %rsi
	cmpb	$0, (%rsi)
	jne	.L37
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L37:
	leaq	112(%rsp), %rbx
	leaq	.LC11(%rip), %rdx
	movq	%rbx, %rdi
	call	_dl_exception_create@PLT
	movl	$12, %edi
	jmp	.L6
.L114:
	leaq	__PRETTY_FUNCTION__.9144(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$88, %edx
	call	__assert_fail
	.size	_dl_check_map_versions, .-_dl_check_map_versions
	.p2align 4,,15
	.globl	_dl_check_all_versions
	.hidden	_dl_check_all_versions
	.type	_dl_check_all_versions, @function
_dl_check_all_versions:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L125
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	%edx, %r13d
	xorl	%ebp, %ebp
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L123:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L121
.L124:
	testb	$2, 797(%rbx)
	jne	.L123
	movq	%rbx, %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	call	_dl_check_map_versions
	movq	24(%rbx), %rbx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, %ebp
	testq	%rbx, %rbx
	jne	.L124
.L121:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	addq	$8, %rsp
	xorl	%ebp, %ebp
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	_dl_check_all_versions, .-_dl_check_all_versions
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9144, @object
	.size	__PRETTY_FUNCTION__.9144, 13
__PRETTY_FUNCTION__.9144:
	.string	"match_symbol"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9166, @object
	.size	__PRETTY_FUNCTION__.9166, 23
__PRETTY_FUNCTION__.9166:
	.string	"_dl_check_map_versions"
	.hidden	_dl_debug_printf
	.hidden	__assert_fail
	.hidden	strcmp
	.hidden	_dl_signal_exception
	.hidden	_itoa_word
	.hidden	_dl_name_match_p
