	.text
	.p2align 4,,15
	.globl	__sbrk
	.hidden	__sbrk
	.type	__sbrk, @function
__sbrk:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	__curbrk(%rip), %rbx
	testq	%rbx, %rbx
	je	.L15
.L2:
	cmpq	$0, %rbp
	je	.L1
	jle	.L5
	movq	%rbx, %rax
	addq	%rbp, %rax
	setc	%al
	testb	%al, %al
	je	.L7
.L16:
	movl	$12, rtld_errno(%rip)
.L13:
	movq	$-1, %rbx
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rbp, %rax
	negq	%rax
	cmpq	%rbx, %rax
	seta	%al
	testb	%al, %al
	jne	.L16
.L7:
	leaq	(%rbx,%rbp), %rdi
	call	__brk
	testl	%eax, %eax
	js	.L13
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%edi, %edi
	call	__brk
	testl	%eax, %eax
	movq	__curbrk(%rip), %rbx
	jns	.L2
	jmp	.L13
	.size	__sbrk, .-__sbrk
	.weak	sbrk
	.set	sbrk,__sbrk
	.hidden	__brk
	.hidden	rtld_errno
	.hidden	__curbrk
