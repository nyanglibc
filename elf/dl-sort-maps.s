	.text
	.p2align 4,,15
	.globl	_dl_sort_maps
	.hidden	_dl_sort_maps
	.type	_dl_sort_maps, @function
_dl_sort_maps:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	cmpl	$1, %esi
	movb	%cl, -49(%rbp)
	ja	.L44
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L44:
	movq	%rdx, -152(%rbp)
	movl	%esi, %edx
	movq	%rsp, %rax
	movq	%rdx, %rbx
	movl	%edx, -56(%rbp)
	addq	%rdx, %rdx
	movq	%rax, -160(%rbp)
	leaq	16(%rdx), %rax
	movq	%rdi, %r14
	movq	%rdi, -72(%rbp)
	xorl	%esi, %esi
	movl	$1, %r13d
	shrq	$4, %rax
	xorl	%r15d, %r15d
	salq	$4, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	movq	%rdi, -64(%rbp)
	call	memset@PLT
	leal	-1(%rbx), %ecx
	movl	%ebx, %eax
	subl	$2, %eax
	leaq	(%r14,%rcx,8), %rbx
	movl	%ecx, -116(%rbp)
	xorl	%r14d, %r14d
	movq	%rcx, -128(%rbp)
	subq	$1, %rcx
	movq	%r14, %r10
	movq	%rbx, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movl	%r13d, %r14d
	movl	%eax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L3:
	movq	-64(%rbp), %rax
	leaq	(%rax,%r10,2), %r9
	movzwl	(%r9), %eax
	addl	$1, %eax
	cmpb	$0, -49(%rbp)
	movw	%ax, -52(%rbp)
	movw	%ax, (%r9)
	movq	-72(%rbp), %rax
	leaq	(%rax,%r10,8), %rdi
	movq	(%rdi), %rbx
	jne	.L45
.L4:
	cmpl	-116(%rbp), %r15d
	jnb	.L5
	movl	-120(%rbp), %eax
	movq	-144(%rbp), %rdx
	movq	-128(%rbp), %r12
	movq	-136(%rbp), %r13
	subl	%r15d, %eax
	subq	%rax, %rdx
.L18:
	movq	0(%r13), %rax
	movl	%r12d, %ecx
	movq	976(%rax), %rsi
	testq	%rsi, %rsi
	je	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rsi), %r8
	testq	%r8, %r8
	je	.L7
	addq	$8, %rsi
	cmpq	%rbx, %r8
	jne	.L6
.L8:
	movl	%ecx, %eax
	movl	%ecx, -88(%rbp)
	movq	-72(%rbp), %rcx
	subl	%r15d, %eax
	movl	%r14d, %r11d
	movq	%r10, -104(%rbp)
	leaq	0(,%rax,8), %rdx
	movq	%r9, -96(%rbp)
	movq	%r11, -80(%rbp)
	leaq	(%rcx,%r11,8), %rsi
	movq	%rax, -112(%rbp)
	call	memmove
	movq	%rbx, 0(%r13)
	movq	-152(%rbp), %r13
	movq	-80(%rbp), %r11
	movl	-88(%rbp), %ecx
	movq	-96(%rbp), %r9
	movq	-104(%rbp), %r10
	testq	%r13, %r13
	je	.L9
	leaq	0(%r13,%r10), %rdi
	movq	-112(%rbp), %rdx
	leaq	0(%r13,%r11), %rsi
	movq	%r9, -104(%rbp)
	movl	%ecx, -96(%rbp)
	movzbl	(%rdi), %ebx
	movq	%r10, -88(%rbp)
	call	memmove
	movq	-104(%rbp), %r9
	movl	-96(%rbp), %ecx
	movq	-88(%rbp), %r10
	movq	-80(%rbp), %r11
	movb	%bl, 0(%r13,%r12)
.L9:
	movq	-64(%rbp), %rax
	movzwl	(%rax,%r11,2), %edx
	leaq	(%rax,%r11,2), %rdi
	movl	-56(%rbp), %eax
	subl	%r15d, %eax
	cmpl	%eax, %edx
	jbe	.L46
.L10:
	movl	-56(%rbp), %edx
	xorl	%esi, %esi
	movl	%r14d, %r15d
	subl	%r14d, %edx
	addq	%rdx, %rdx
	call	memset@PLT
	movl	%r14d, %r10d
	addl	$1, %r14d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	$0, -49(%rbp)
	jne	.L47
.L13:
	subq	$1, %r12
	subq	$8, %r13
	cmpq	%r12, %rdx
	jne	.L18
.L5:
	cmpl	%r14d, -56(%rbp)
	je	.L19
.L48:
	movq	-64(%rbp), %rcx
	movl	%r14d, %eax
	leaq	(%rcx,%rax,2), %rdi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L46:
	movq	-112(%rbp), %rdx
	movq	%rdi, %rsi
	movq	%r9, %rdi
	movq	%r10, -88(%rbp)
	movl	%ecx, -80(%rbp)
	addq	%rdx, %rdx
	call	memmove
	movl	-80(%rbp), %ecx
	movzwl	-52(%rbp), %ebx
	movq	-64(%rbp), %rax
	movq	-88(%rbp), %r10
	movw	%bx, (%rax,%rcx,2)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L47:
	movq	984(%rax), %rsi
	testq	%rsi, %rsi
	je	.L13
	leaq	8(%rsi), %r11
	movl	(%rsi), %esi
	subl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$-1, %esi
	je	.L13
	movl	%esi, %r8d
	subl	$1, %esi
	cmpq	%rbx, (%r11,%r8,8)
	jne	.L14
	movq	976(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.L15
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$8, %rsi
	cmpq	%r8, %rax
	je	.L13
.L15:
	movq	(%rsi), %r8
	testq	%r8, %r8
	jne	.L16
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	%rbx, 40(%rbx)
	jne	.L5
	cmpl	$-1, 1012(%rbx)
	jne	.L4
	cmpl	%r14d, -56(%rbp)
	jne	.L48
.L19:
	movq	-160(%rbp), %rsp
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.size	_dl_sort_maps, .-_dl_sort_maps
	.hidden	memmove
