	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	": "
.LC1:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"error while loading shared libraries"
	.section	.rodata.str1.1
.LC3:
	.string	"<program name unknown>"
.LC4:
	.string	"%s: %s: %s%s%s%s%s\n"
	.text
	.p2align 4,,15
	.type	fatal_error, @function
fatal_error:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC1(%rip), %rax
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rcx, %r9
	subq	$1048, %rsp
	testl	%edi, %edi
	movq	%rax, %rdx
	jne	.L12
.L2:
	cmpb	$0, 0(%rbp)
	leaq	.LC1(%rip), %rcx
	leaq	.LC0(%rip), %r8
	leaq	.LC4(%rip), %rdi
	cmove	%rcx, %r8
	leaq	.LC2(%rip), %rcx
	testq	%rbx, %rbx
	cmove	%rcx, %rbx
	movq	_dl_argv(%rip), %rcx
	movq	(%rcx), %rsi
	leaq	.LC3(%rip), %rcx
	pushq	%rax
	pushq	%rdx
	movq	%rbx, %rdx
	testq	%rsi, %rsi
	cmove	%rcx, %rsi
	movq	%rbp, %rcx
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
.L12:
	leaq	16(%rsp), %rsi
	movl	$1024, %edx
	movq	%rcx, 8(%rsp)
	call	__strerror_r
	leaq	.LC0(%rip), %rdx
	movq	8(%rsp), %r9
	jmp	.L2
	.size	fatal_error, .-fatal_error
	.p2align 4,,15
	.globl	_dl_signal_exception
	.hidden	_dl_signal_exception
	.type	_dl_signal_exception, @function
_dl_signal_exception:
	subq	$8, %rsp
	movq	catch_hook@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	testq	%rax, %rax
	je	.L14
	movq	(%rax), %rdx
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movq	16(%rsi), %rcx
	movl	$1, %esi
	movq	%rcx, 16(%rdx)
	movq	8(%rax), %rdx
	movl	%edi, (%rdx)
	leaq	16(%rax), %rdi
	call	__longjmp
.L14:
	movq	8(%rsi), %rcx
	movq	(%rsi), %rsi
	call	fatal_error
	.size	_dl_signal_exception, .-_dl_signal_exception
	.section	.rodata.str1.1
.LC5:
	.string	"DYNAMIC LINKER BUG!!!"
	.text
	.p2align 4,,15
	.globl	_dl_signal_error
	.hidden	_dl_signal_error
	.type	_dl_signal_error, @function
_dl_signal_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$8, %rsp
	movq	catch_hook@gottpoff(%rip), %rax
	testq	%rcx, %rcx
	movq	%fs:(%rax), %rbx
	leaq	.LC5(%rip), %rax
	cmove	%rax, %rcx
	testq	%rbx, %rbx
	je	.L18
	movq	(%rbx), %rdi
	movq	%rcx, %rdx
	call	_dl_exception_create@PLT
	movq	8(%rbx), %rax
	leaq	16(%rbx), %rdi
	movl	$1, %esi
	movl	%ebp, (%rax)
	call	__longjmp
.L18:
	call	fatal_error
	.size	_dl_signal_error, .-_dl_signal_error
	.p2align 4,,15
	.globl	_dl_catch_exception
	.hidden	_dl_catch_exception
	.type	_dl_catch_exception, @function
_dl_catch_exception:
	pushq	%r14
	pushq	%rbx
	subq	$296, %rsp
	movq	catch_hook@gottpoff(%rip), %rbx
	testq	%rdi, %rdi
	movq	%rdi, 8(%rsp)
	movq	%rsi, 24(%rsp)
	movq	%rdx, 32(%rsp)
	movq	%fs:(%rbx), %r14
	movq	%r14, 16(%rsp)
	jne	.L21
	movq	$0, %fs:(%rbx)
	movq	%rdx, %rdi
	call	*%rsi
	movq	%r14, %fs:(%rbx)
	xorl	%eax, %eax
.L20:
	addq	$296, %rsp
	popq	%rbx
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	8(%rsp), %rax
	leaq	64(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rdi, %fs:(%rbx)
	leaq	80(%rsp), %rdi
	movq	%rax, 64(%rsp)
	leaq	60(%rsp), %rax
	movq	%rax, 72(%rsp)
	call	__sigsetjmp
	testl	%eax, %eax
	movl	%eax, 44(%rsp)
	jne	.L24
	movq	24(%rsp), %rcx
	movq	32(%rsp), %rdi
	call	*%rcx
	movq	8(%rsp), %rax
	movq	catch_hook@gottpoff(%rip), %rdx
	movq	16(%rsp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rcx, %fs:(%rdx)
	movl	44(%rsp), %eax
	addq	$296, %rsp
	popq	%rbx
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	catch_hook@gottpoff(%rip), %rax
	movq	16(%rsp), %rcx
	movq	%rcx, %fs:(%rax)
	movl	60(%rsp), %eax
	jmp	.L20
	.size	_dl_catch_exception, .-_dl_catch_exception
	.p2align 4,,15
	.globl	_dl_catch_error
	.hidden	_dl_catch_error
	.type	_dl_catch_error, @function
_dl_catch_error:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	movq	%rcx, %rsi
	movq	%r8, %rdx
	subq	$32, %rsp
	movq	%rsp, %rdi
	call	_dl_catch_exception
	movq	(%rsp), %rdx
	movq	%rdx, (%r12)
	movq	8(%rsp), %rdx
	cmpq	%rdx, 16(%rsp)
	movq	%rdx, 0(%rbp)
	sete	(%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	_dl_catch_error, .-_dl_catch_error
	.section	.tbss,"awT",@nobits
	.align 8
	.type	catch_hook, @object
	.size	catch_hook, 8
catch_hook:
	.zero	8
	.hidden	__sigsetjmp
	.hidden	__longjmp
	.hidden	__strerror_r
