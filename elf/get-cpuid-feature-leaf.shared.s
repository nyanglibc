	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__x86_get_cpuid_feature_leaf
	.type	__x86_get_cpuid_feature_leaf, @function
__x86_get_cpuid_feature_leaf:
	cmpl	$7, %edi
	leaq	feature.9045(%rip), %rax
	ja	.L1
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movl	%edi, %edi
	salq	$5, %rdi
	leaq	124(%rdi,%rax), %rax
.L1:
	rep ret
	.size	__x86_get_cpuid_feature_leaf, .-__x86_get_cpuid_feature_leaf
	.section	.rodata
	.align 32
	.type	feature.9045, @object
	.size	feature.9045, 32
feature.9045:
	.zero	32
