	.text
	.p2align 4,,15
	.type	call_dl_lookup, @function
call_dl_lookup:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rsi
	movl	24(%rbx), %eax
	movq	40(%rdi), %rdx
	xorl	%r9d, %r9d
	movq	8(%rdi), %rdi
	movq	16(%rbx), %r8
	movq	920(%rsi), %rcx
	pushq	$0
	pushq	%rax
	call	_dl_lookup_symbol_x
	movq	%rax, (%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	call_dl_lookup, .-call_dl_lookup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"RTLD_NEXT used in code not dynamically loaded"
	.text
	.p2align 4,,15
	.type	do_sym, @function
do_sym:
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rsi, %rbx
	subq	$120, %rsp
	testq	%rdi, %rdi
	movq	$0, 24(%rsp)
	je	.L34
	cmpq	$-1, %rdi
	je	.L35
	leaq	24(%rsp), %rdx
	leaq	928(%rdi), %rcx
	pushq	$0
	pushq	%r8
	movq	%rdi, %rsi
	xorl	%r9d, %r9d
	movq	%rbp, %r8
	movq	%rbx, %rdi
	call	_dl_lookup_symbol_x
	popq	%rdx
	popq	%rcx
.L8:
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L20
	cmpw	$-15, 6(%rdx)
	je	.L21
	testq	%rax, %rax
	je	.L21
	movq	(%rax), %rax
.L17:
	addq	8(%rdx), %rax
	movzbl	4(%rdx), %edx
	andl	$15, %edx
	cmpb	$10, %dl
	je	.L36
.L4:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rdx, %rdi
	movq	%rdx, 8(%rsp)
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	movq	8(%rsp), %rdx
	je	.L37
	cmpq	_dl_ns(%rip), %rax
	je	.L18
.L14:
	movq	%rax, %rdi
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rdx, %rdi
.L15:
	movq	736(%rdi), %rdx
	testq	%rdx, %rdx
	jne	.L19
	leaq	24(%rsp), %rdx
	leaq	928(%rdi), %rcx
	pushq	%rax
	pushq	$0
.L33:
	movq	%rax, %rsi
	movq	%rbx, %rdi
	xorl	%r9d, %r9d
	movq	%rbp, %r8
	call	_dl_lookup_symbol_x
	popq	%rsi
	popq	%rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L37:
	movq	_dl_ns(%rip), %rax
	testq	%rax, %rax
	je	.L13
.L18:
	cmpq	%rdx, 856(%rax)
	ja	.L13
	cmpq	%rdx, 864(%rax)
	ja	.L14
.L13:
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rdx, %rdi
	movl	%r8d, 8(%rsp)
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	cmove	_dl_ns(%rip), %rax
#APP
# 104 "dl-sym.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	movl	8(%rsp), %r8d
	jne	.L7
	orl	$1, %r8d
	movq	920(%rax), %rcx
	leaq	24(%rsp), %rdx
	pushq	$0
	pushq	%r8
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	call	*%rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, 64(%rsp)
	leaq	24(%rsp), %rax
	orl	$5, %r8d
	movq	%rbx, 72(%rsp)
	movq	%rbp, 80(%rsp)
	movl	%r8d, 88(%rsp)
	movq	%rax, 104(%rsp)
#APP
# 119 "dl-sym.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	leaq	32(%rsp), %rbx
	leaq	64(%rsp), %rdx
	leaq	call_dl_lookup(%rip), %rsi
	movq	%rbx, %rdi
	call	_dl_catch_exception
	movl	%eax, %r8d
	xorl	%eax, %eax
#APP
# 122 "dl-sym.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	jne	.L9
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 122 "dl-sym.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L9:
	cmpq	$0, 40(%rsp)
	movq	64(%rsp), %rax
	je	.L8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%r8d, %edi
	call	_dl_signal_exception
	.size	do_sym, .-do_sym
	.p2align 4,,15
	.globl	_dl_vsym
	.type	_dl_vsym, @function
_dl_vsym:
	subq	$40, %rsp
	movzbl	(%rdx), %r8d
	xorl	%eax, %eax
	movq	%rcx, %r9
	movq	%rdx, (%rsp)
	movl	$1, 12(%rsp)
	testq	%r8, %r8
	je	.L39
	movzbl	1(%rdx), %ecx
	testb	%cl, %cl
	je	.L53
	movzbl	%cl, %eax
	movzbl	2(%rdx), %ecx
	salq	$4, %r8
	addq	%r8, %rax
	testb	%cl, %cl
	je	.L39
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	3(%rdx), %ecx
	testb	%cl, %cl
	je	.L39
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	4(%rdx), %ecx
	testb	%cl, %cl
	je	.L39
	salq	$4, %rax
	addq	%rcx, %rax
	leaq	5(%rdx), %rcx
	movzbl	5(%rdx), %edx
	testb	%dl, %dl
	je	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	salq	$4, %rax
	addq	$1, %rcx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$240, %edx
	xorq	%rdx, %rax
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L45
.L44:
	andl	$268435455, %eax
.L39:
	movq	%rsp, %rcx
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	movl	%eax, 8(%rsp)
	movq	$0, 16(%rsp)
	call	do_sym
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movzbl	%r8b, %eax
	jmp	.L39
	.size	_dl_vsym, .-_dl_vsym
	.p2align 4,,15
	.globl	_dl_sym
	.type	_dl_sym, @function
_dl_sym:
	movl	$2, %r8d
	xorl	%ecx, %ecx
	jmp	do_sym
	.size	_dl_sym, .-_dl_sym
	.hidden	_dl_signal_exception
	.hidden	_dl_catch_exception
	.hidden	_dl_signal_error
	.hidden	_dl_lookup_symbol_x
