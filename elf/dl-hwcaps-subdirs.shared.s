	.text
	.p2align 4,,15
	.globl	_dl_hwcaps_subdirs_active
	.hidden	_dl_hwcaps_subdirs_active
	.type	_dl_hwcaps_subdirs_active, @function
_dl_hwcaps_subdirs_active:
	movl	152+_rtld_local_ro(%rip), %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$33024, %ecx
	cmpl	$33024, %ecx
	je	.L27
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	136+_rtld_local_ro(%rip), %eax
	andl	$1, %eax
	je	.L1
	andl	$125829120, %edx
	xorl	%eax, %eax
	cmpl	$125829120, %edx
	jne	.L1
	movl	148+_rtld_local_ro(%rip), %edx
	movl	%edx, %eax
	andl	$8192, %eax
	je	.L1
	movl	212+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %eax
	andl	$1, %eax
	je	.L1
	movl	%edx, %esi
	xorl	%eax, %eax
	andl	$9961985, %esi
	cmpl	$9961985, %esi
	jne	.L1
	testl	$268435456, %edx
	movl	$4, %eax
	je	.L1
	movl	176+_rtld_local_ro(%rip), %esi
	testb	$32, %sil
	je	.L1
	movl	%edx, %edi
	andl	$536875008, %edi
	cmpl	$536875008, %edi
	jne	.L1
	andl	$32, %ecx
	je	.L1
	andl	$4194304, %edx
	je	.L1
	movl	%esi, %edx
	movl	$6, %eax
	andl	$1342373888, %edx
	cmpl	$1342373888, %edx
	jne	.L1
	movl	%esi, %eax
	shrl	$31, %eax
	addl	$6, %eax
	ret
	.size	_dl_hwcaps_subdirs_active, .-_dl_hwcaps_subdirs_active
	.hidden	_dl_hwcaps_subdirs
	.globl	_dl_hwcaps_subdirs
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	_dl_hwcaps_subdirs, @object
	.size	_dl_hwcaps_subdirs, 30
_dl_hwcaps_subdirs:
	.string	"x86-64-v4:x86-64-v3:x86-64-v2"
	.hidden	_rtld_local_ro
