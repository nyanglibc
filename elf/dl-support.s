	.text
	.p2align 4,,15
	.globl	_dl_aux_init
	.hidden	_dl_aux_init
	.type	_dl_aux_init, @function
_dl_aux_init:
	movq	(%rdi), %rax
	movq	%rdi, _dl_auxv(%rip)
	testq	%rax, %rax
	je	.L75
	pushq	%r15
	pushq	%r14
	leaq	.L6(%rip), %rdx
	pushq	%r13
	pushq	%r12
	xorl	%r14d, %r14d
	pushq	%rbp
	pushq	%rbx
	xorl	%r12d, %r12d
	movq	_dl_sysinfo_dso(%rip), %rbx
	movq	_dl_hwcap2(%rip), %rsi
	xorl	%r9d, %r9d
	movl	__libc_enable_secure_decided(%rip), %ecx
	movq	_dl_random(%rip), %r15
	xorl	%r8d, %r8d
	movl	_dl_clktck(%rip), %r13d
	movq	_dl_pagesize(%rip), %r10
	movq	%rbx, -24(%rsp)
	movl	__libc_enable_secure(%rip), %ebx
	movq	%rsi, -32(%rsp)
	movzwl	_dl_fpu_control(%rip), %esi
	movl	%ecx, -8(%rsp)
	movq	_dl_platform(%rip), %rcx
	movl	%ebx, -12(%rsp)
	movq	_dl_hwcap(%rip), %rbx
	movq	_dl_phnum(%rip), %r11
	movq	_dl_phdr(%rip), %rbp
	movw	%si, -16(%rsp)
	movq	%rcx, -56(%rsp)
	xorl	%esi, %esi
	movq	%rbx, -48(%rsp)
	movb	$0, -1(%rsp)
	xorl	%ebx, %ebx
	movb	$0, -2(%rsp)
	movb	$0, -3(%rsp)
	xorl	%ecx, %ecx
	movb	$0, -4(%rsp)
	movb	$0, -13(%rsp)
	movb	$0, -14(%rsp)
	movb	$0, -33(%rsp)
	movb	$0, -34(%rsp)
	.p2align 4,,10
	.p2align 3
.L21:
	subq	$3, %rax
	cmpq	$30, %rax
	ja	.L4
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L5-.L6
	.long	.L4-.L6
	.long	.L7-.L6
	.long	.L8-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L9-.L6
	.long	.L10-.L6
	.long	.L11-.L6
	.long	.L12-.L6
	.long	.L13-.L6
	.long	.L14-.L6
	.long	.L15-.L6
	.long	.L16-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L17-.L6
	.long	.L4-.L6
	.long	.L18-.L6
	.long	.L19-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L4-.L6
	.long	.L20-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	cmovne	%rax, %r10
	movl	$1, %eax
	cmovne	%eax, %r9d
.L4:
	addq	$16, %rdi
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	.p2align 4,,10
	.p2align 3
.L80:
	testb	%r9b, %r9b
	jne	.L79
.L22:
	testb	%r14b, %r14b
	je	.L23
	movl	%r13d, _dl_clktck(%rip)
.L23:
	testb	%r12b, %r12b
	je	.L24
	movq	%rbp, _dl_phdr(%rip)
.L24:
	testb	%bl, %bl
	je	.L25
	movq	%r11, _dl_phnum(%rip)
.L25:
	cmpb	$0, -34(%rsp)
	je	.L26
	movq	-56(%rsp), %rax
	movq	%rax, _dl_platform(%rip)
.L26:
	cmpb	$0, -33(%rsp)
	je	.L27
	movq	-48(%rsp), %rax
	movq	%rax, _dl_hwcap(%rip)
.L27:
	cmpb	$0, -14(%rsp)
	je	.L28
	movq	-32(%rsp), %rax
	movq	%rax, _dl_hwcap2(%rip)
.L28:
	cmpb	$0, -13(%rsp)
	je	.L29
	movzwl	-16(%rsp), %eax
	movw	%ax, _dl_fpu_control(%rip)
.L29:
	cmpb	$0, -4(%rsp)
	je	.L30
	movq	-24(%rsp), %rax
	movq	%rax, _dl_sysinfo_dso(%rip)
.L30:
	cmpb	$0, -3(%rsp)
	je	.L31
	movl	-12(%rsp), %eax
	movl	%eax, __libc_enable_secure(%rip)
.L31:
	cmpb	$0, -2(%rsp)
	je	.L32
	movl	-8(%rsp), %eax
	movl	%eax, __libc_enable_secure_decided(%rip)
.L32:
	cmpb	$0, -1(%rsp)
	je	.L33
	movq	%r15, _dl_random(%rip)
.L33:
	cmpl	$15, %ecx
	jne	.L1
	xorl	%eax, %eax
	orl	%esi, %r8d
	movl	$1, __libc_enable_secure_decided(%rip)
	setne	%al
	movl	%eax, __libc_enable_secure(%rip)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rdi), %rax
	addq	$16, %rdi
	movb	$1, -4(%rsp)
	movq	%rax, -24(%rsp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L19:
	movq	8(%rdi), %rax
	addq	$16, %rdi
	movb	$1, -14(%rsp)
	movq	%rax, -32(%rsp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%rdi), %r15
	addq	$16, %rdi
	movq	(%rdi), %rax
	movb	$1, -1(%rsp)
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L17:
	movl	8(%rdi), %eax
	addq	$16, %rdi
	movb	$1, -2(%rsp)
	movl	$1, -8(%rsp)
	movb	$1, -3(%rsp)
	movl	$-1, %ecx
	movl	%eax, -12(%rsp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L16:
	movzwl	8(%rdi), %eax
	addq	$16, %rdi
	movb	$1, -13(%rsp)
	movw	%ax, -16(%rsp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L15:
	movl	8(%rdi), %r13d
	addq	$16, %rdi
	movq	(%rdi), %rax
	movl	$1, %r14d
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%rdi), %rax
	addq	$16, %rdi
	movb	$1, -33(%rsp)
	movq	%rax, -48(%rsp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L13:
	movq	8(%rdi), %rax
	addq	$16, %rdi
	movb	$1, -34(%rsp)
	movq	%rax, -56(%rsp)
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	8(%rdi), %esi
	addq	$16, %rdi
	orl	$8, %ecx
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	8(%rdi), %esi
	addq	$16, %rdi
	orl	$4, %ecx
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	8(%rdi), %r8d
	addq	$16, %rdi
	orl	$2, %ecx
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	8(%rdi), %r8d
	addq	$16, %rdi
	orl	$1, %ecx
	movq	(%rdi), %rax
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L7:
	movq	8(%rdi), %r11
	addq	$16, %rdi
	movq	(%rdi), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rdi), %rbp
	addq	$16, %rdi
	movq	(%rdi), %rax
	movl	$1, %r12d
	testq	%rax, %rax
	jne	.L21
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L75:
	rep ret
.L79:
	movq	%r10, _dl_pagesize(%rip)
	jmp	.L22
	.size	_dl_aux_init, .-_dl_aux_init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.rodata
.LC1:
	.string	"/var/tmp"
	.string	"/var/profile"
	.section	.rodata.str1.1
.LC2:
	.string	"LD_WARN"
.LC3:
	.string	"setup-vdso.h"
.LC4:
	.string	"ph->p_type != PT_TLS"
.LC5:
	.string	"get-dynamic-info.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC6:
	.string	"info[DT_PLTREL]->d_un.d_val == DT_RELA"
	.align 8
.LC7:
	.string	"info[DT_RELAENT]->d_un.d_val == sizeof (ElfW(Rela))"
	.align 8
.LC8:
	.string	"\nWARNING: Unsupported flag value(s) of 0x%x in DT_FLAGS_1.\n"
	.section	.rodata.str1.1
.LC9:
	.string	"out of memory\n"
.LC10:
	.string	"LINUX_2.6"
.LC11:
	.string	"__vdso_clock_gettime"
.LC12:
	.string	"__vdso_gettimeofday"
.LC13:
	.string	"__vdso_time"
.LC14:
	.string	"__vdso_getcpu"
.LC15:
	.string	"__vdso_clock_getres"
.LC16:
	.string	"LD_LIBRARY_PATH"
.LC17:
	.string	"LD_BIND_NOW"
.LC18:
	.string	"LD_BIND_NOT"
.LC19:
	.string	"LD_DYNAMIC_WEAK"
.LC20:
	.string	"LD_PROFILE_OUTPUT"
.LC21:
	.string	"LD_ASSUME_KERNEL"
	.text
	.p2align 4,,15
	.globl	_dl_non_dynamic_init
	.hidden	_dl_non_dynamic_init
	.type	_dl_non_dynamic_init, @function
_dl_non_dynamic_init:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$80, %rsp
	call	_dl_get_origin
	movq	%rax, 848+_dl_main_map(%rip)
	movq	_dl_phdr(%rip), %rax
	leaq	.LC2(%rip), %rdi
	movq	%rax, 680+_dl_main_map(%rip)
	movq	_dl_phnum(%rip), %rax
	movw	%ax, 696+_dl_main_map(%rip)
	call	getenv
	leaq	.LC0(%rip), %rdi
	testq	%rax, %rax
	cmove	%rdi, %rax
	cmpb	$0, (%rax)
	setne	%al
	cmpq	$0, _dl_sysinfo_dso(%rip)
	movzbl	%al, %eax
	movl	%eax, _dl_verbose(%rip)
	je	.L83
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rdi, %rsi
	call	_dl_new_object
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L83
	movq	_dl_sysinfo_dso(%rip), %rdi
	movzwl	56(%rdi), %esi
	movq	32(%rdi), %rax
	addq	%rdi, %rax
	testq	%rsi, %rsi
	movq	%rax, 680(%rbx)
	movw	%si, 696(%rbx)
	je	.L84
	xorl	%ecx, %ecx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L85:
	cmpl	$1, %edx
	je	.L325
	cmpl	$7, %edx
	je	.L326
.L86:
	addq	$1, %rcx
	addq	$56, %rax
	cmpq	%rsi, %rcx
	je	.L84
.L91:
	movl	(%rax), %edx
	cmpl	$2, %edx
	jne	.L85
	movq	16(%rax), %rdx
	addq	$1, %rcx
	addq	$56, %rax
	movq	%rdx, 16(%rbx)
	movq	-16(%rax), %rdx
	shrq	$4, %rdx
	cmpq	%rsi, %rcx
	movw	%dx, 698(%rbx)
	jne	.L91
.L84:
	movq	%rdi, %rcx
	subq	(%rbx), %rcx
	movq	16(%rbx), %rdx
	addq	%rcx, 864(%rbx)
	addq	%rcx, 872(%rbx)
	movq	%rdi, 856(%rbx)
	addq	%rcx, %rdx
	testq	%rdx, %rdx
	movq	%rcx, (%rbx)
	movq	%rdx, 16(%rbx)
	je	.L93
	movq	(%rdx), %rax
	leaq	64(%rbx), %rsi
	testq	%rax, %rax
	je	.L94
	movl	$1879048191, %r8d
	movl	$1879047679, %r11d
	movl	$1879047935, %r12d
	movl	$1879048001, %r13d
	movl	$1879047733, %ebp
	movl	$50, %r10d
	movl	$1879048226, %r9d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L327:
	movq	%r9, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
.L95:
	movq	%rdx, (%rsi,%rax,8)
.L99:
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L94
.L100:
	cmpq	$34, %rax
	jbe	.L95
	movq	%r8, %rdi
	subq	%rax, %rdi
	cmpq	$15, %rdi
	jbe	.L327
	leal	(%rax,%rax), %edi
	sarl	%edi
	cmpl	$-4, %edi
	jbe	.L97
	movl	%r10d, %eax
	subl	%edi, %eax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L325:
	cmpq	$0, (%rbx)
	movq	16(%rax), %rdx
	jne	.L88
	movq	%rdx, (%rbx)
.L88:
	addq	40(%rax), %rdx
	cmpq	864(%rbx), %rdx
	jb	.L89
	movq	%rdx, 864(%rbx)
.L89:
	testb	$1, 4(%rax)
	je	.L86
	cmpq	872(%rbx), %rdx
	jb	.L86
	movq	%rdx, 872(%rbx)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L94:
	testq	%rcx, %rcx
	je	.L102
	movq	96(%rbx), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L103
	movq	(%rdx), %rax
	movq	%rax, dyn_temp.10402(%rip)
	movq	8(%rdx), %rax
	addq	%rcx, %rax
	movq	%rax, 8+dyn_temp.10402(%rip)
	leaq	dyn_temp.10402(%rip), %rax
	movq	%rax, 96(%rbx)
	movl	$1, %eax
.L103:
	movq	88(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L104
	movslq	%eax, %rdx
	leaq	dyn_temp.10402(%rip), %rdi
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 8(%rdx)
	movq	%rdx, 88(%rbx)
.L104:
	movq	104(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L105
	movslq	%eax, %rdx
	leaq	dyn_temp.10402(%rip), %rdi
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 8(%rdx)
	movq	%rdx, 104(%rbx)
.L105:
	movq	112(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L106
	movslq	%eax, %rdx
	leaq	dyn_temp.10402(%rip), %rdi
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 8(%rdx)
	movq	%rdx, 112(%rbx)
.L106:
	movq	120(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L107
	movslq	%eax, %rdx
	leaq	dyn_temp.10402(%rip), %rdi
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 8(%rdx)
	movq	%rdx, 120(%rbx)
.L107:
	movq	248(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L108
	movslq	%eax, %rdx
	leaq	dyn_temp.10402(%rip), %rdi
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 8(%rdx)
	movq	%rdx, 248(%rbx)
.L108:
	movq	464(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L109
	movslq	%eax, %rdx
	leaq	dyn_temp.10402(%rip), %rdi
	addl	$1, %eax
	salq	$4, %rdx
	addq	%rdi, %rdx
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	8(%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 8(%rdx)
	movq	%rdx, 464(%rbx)
.L109:
	movq	672(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L102
	leaq	dyn_temp.10402(%rip), %rsi
	cltq
	addq	8(%rdx), %rcx
	salq	$4, %rax
	addq	%rsi, %rax
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rax)
	movq	%rsi, (%rax)
	movq	%rax, 672(%rbx)
.L102:
	movq	224(%rbx), %rax
	testq	%rax, %rax
	je	.L111
	cmpq	$7, 8(%rax)
	jne	.L328
.L111:
	cmpq	$0, 120(%rbx)
	je	.L112
	movq	136(%rbx), %rax
	cmpq	$24, 8(%rax)
	jne	.L329
.L112:
	movq	304(%rbx), %rax
	testq	%rax, %rax
	je	.L114
	movq	8(%rax), %rdx
	testb	$2, %dl
	movl	%edx, 1008(%rbx)
	je	.L115
	movq	%rax, 192(%rbx)
.L115:
	testb	$4, %dl
	je	.L116
	movq	%rax, 240(%rbx)
.L116:
	andl	$8, %edx
	je	.L114
	movq	%rax, 256(%rbx)
.L114:
	movq	376(%rbx), %rax
	testq	%rax, %rax
	je	.L119
	movq	8(%rax), %rsi
	testb	$8, %sil
	movl	%esi, %eax
	movl	%esi, 1004(%rbx)
	je	.L120
	movb	$1, 800(%rbx)
.L120:
	testb	$64, _dl_debug_mask(%rip)
	jne	.L330
.L121:
	testb	$1, %al
	je	.L119
	movq	376(%rbx), %rax
	movq	%rax, 256(%rbx)
.L119:
	cmpq	$0, 296(%rbx)
	je	.L93
	movq	$0, 184(%rbx)
.L93:
	movq	%rbx, %rdi
	call	_dl_setup_hash
	movq	928(%rbx), %rax
	leaq	40(%rbx), %rdx
	orb	$4, 796(%rbx)
	movl	$1, 996(%rbx)
	movl	$1, 8(%rax)
	movq	%rdx, (%rax)
	movq	176(%rbx), %rax
	testq	%rax, %rax
	je	.L124
	movq	104(%rbx), %rdx
	movq	8(%rax), %rbp
	addq	8(%rdx), %rbp
	movq	%rbp, %rdi
	call	strlen
	leaq	1(%rax), %r12
	movq	%r12, %rdi
	call	malloc
	testq	%rax, %rax
	je	.L331
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	56(%rbx), %rdx
	movq	%rax, 8(%rbx)
	movq	%rax, (%rdx)
.L124:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_add_to_namespace_list
	movq	$1, _dl_nns(%rip)
	movq	%rbx, _dl_sysinfo_map(%rip)
.L126:
	pxor	%xmm0, %xmm0
	leaq	16(%rsp), %r13
	leaq	.LC10(%rip), %r14
	leaq	48(%rsp), %rbp
	leaq	8(%rsp), %r12
	movabsq	$4356732406, %rax
	movq	$0, 32(%rsp)
	movq	%r14, 48(%rsp)
	leaq	928(%rbx), %rcx
	movaps	%xmm0, 16(%rsp)
	movq	%rax, 56(%rsp)
	movb	$32, 20(%rsp)
	leaq	.LC11(%rip), %rdi
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %r8
	movq	$0, 64(%rsp)
	movq	%r13, 8(%rsp)
	pushq	$0
	pushq	$0
	call	_dl_lookup_symbol_x
	movq	24(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	testq	%rdx, %rdx
	je	.L332
	cmpw	$-15, 6(%rdx)
	je	.L174
	testq	%rax, %rax
	je	.L174
	movq	(%rax), %rax
.L130:
	addq	8(%rdx), %rax
	movq	_dl_sysinfo_map(%rip), %rsi
.L129:
	testq	%rsi, %rsi
	movq	%rax, _dl_vdso_clock_gettime64(%rip)
	je	.L131
	pxor	%xmm0, %xmm0
	movabsq	$4356732406, %rax
	movq	$0, 32(%rsp)
	movq	%r14, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	928(%rsi), %rcx
	movq	$0, 64(%rsp)
	movq	%r13, 8(%rsp)
	leaq	.LC12(%rip), %rdi
	movaps	%xmm0, 16(%rsp)
	movb	$32, 20(%rsp)
	pushq	$0
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movq	%rbp, %r8
	pushq	$0
	call	_dl_lookup_symbol_x
	movq	24(%rsp), %rdx
	popq	%r11
	popq	%rbx
	testq	%rdx, %rdx
	je	.L333
	cmpw	$-15, 6(%rdx)
	je	.L175
	testq	%rax, %rax
	je	.L175
	movq	(%rax), %rax
.L134:
	addq	8(%rdx), %rax
	movq	_dl_sysinfo_map(%rip), %rsi
.L133:
	testq	%rsi, %rsi
	movq	%rax, _dl_vdso_gettimeofday(%rip)
	je	.L135
	pxor	%xmm0, %xmm0
	movabsq	$4356732406, %rax
	movq	$0, 32(%rsp)
	movq	%r14, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	928(%rsi), %rcx
	movq	$0, 64(%rsp)
	movq	%r13, 8(%rsp)
	leaq	.LC13(%rip), %rdi
	movaps	%xmm0, 16(%rsp)
	movb	$32, 20(%rsp)
	pushq	$0
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movq	%rbp, %r8
	pushq	$0
	call	_dl_lookup_symbol_x
	movq	24(%rsp), %rdx
	popq	%r9
	popq	%r10
	testq	%rdx, %rdx
	je	.L334
	cmpw	$-15, 6(%rdx)
	je	.L176
	testq	%rax, %rax
	je	.L176
	movq	(%rax), %rax
.L138:
	addq	8(%rdx), %rax
	movq	_dl_sysinfo_map(%rip), %rsi
.L137:
	testq	%rsi, %rsi
	movq	%rax, _dl_vdso_time(%rip)
	je	.L139
	pxor	%xmm0, %xmm0
	movabsq	$4356732406, %rax
	movq	$0, 32(%rsp)
	movq	%r14, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	.LC14(%rip), %rdi
	movq	$0, 64(%rsp)
	movq	%r13, 8(%rsp)
	leaq	928(%rsi), %rcx
	movaps	%xmm0, 16(%rsp)
	movb	$32, 20(%rsp)
	pushq	$0
	xorl	%r9d, %r9d
	movq	%rbp, %r8
	movq	%r12, %rdx
	pushq	$0
	call	_dl_lookup_symbol_x
	movq	24(%rsp), %rdx
	popq	%rdi
	popq	%r8
	testq	%rdx, %rdx
	je	.L335
	cmpw	$-15, 6(%rdx)
	je	.L177
	testq	%rax, %rax
	je	.L177
	movq	(%rax), %rax
.L142:
	addq	8(%rdx), %rax
	movq	_dl_sysinfo_map(%rip), %rsi
.L141:
	testq	%rsi, %rsi
	movq	%rax, _dl_vdso_getcpu(%rip)
	je	.L145
	pxor	%xmm0, %xmm0
	movabsq	$4356732406, %rax
	movq	$0, 32(%rsp)
	movq	%r14, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	928(%rsi), %rcx
	movq	$0, 64(%rsp)
	movq	%r13, 8(%rsp)
	leaq	.LC15(%rip), %rdi
	movaps	%xmm0, 16(%rsp)
	movb	$32, 20(%rsp)
	pushq	$0
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movq	%rbp, %r8
	pushq	$0
	call	_dl_lookup_symbol_x
	movq	24(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	testq	%rdx, %rdx
	je	.L145
	cmpw	$-15, 6(%rdx)
	je	.L178
	testq	%rax, %rax
	je	.L178
	movq	(%rax), %rax
.L146:
	addq	8(%rdx), %rax
.L144:
	leaq	.LC16(%rip), %rdi
	movq	%rax, _dl_vdso_clock_getres_time64(%rip)
	leaq	.LC0(%rip), %rbx
	call	getenv
	leaq	.LC16(%rip), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %rdi
	call	_dl_init_paths
	movq	_dl_all_dirs(%rip), %rax
	leaq	.LC17(%rip), %rdi
	movq	%rax, _dl_init_all_dirs(%rip)
	call	getenv
	testq	%rax, %rax
	leaq	.LC18(%rip), %rdi
	cmove	%rbx, %rax
	cmpb	$0, (%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, _dl_lazy(%rip)
	call	getenv
	testq	%rax, %rax
	leaq	.LC19(%rip), %rdi
	cmove	%rbx, %rax
	cmpb	$0, (%rax)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, _dl_bind_not(%rip)
	call	getenv
	testq	%rax, %rax
	leaq	.LC20(%rip), %rdi
	cmove	%rbx, %rax
	cmpb	$0, (%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, _dl_dynamic_weak(%rip)
	call	getenv
	testq	%rax, %rax
	movq	%rax, _dl_profile_output(%rip)
	movl	__libc_enable_secure(%rip), %edx
	je	.L151
	cmpb	$0, (%rax)
	je	.L151
.L152:
	testl	%edx, %edx
	je	.L154
	leaq	unsecure_envvars.10458(%rip), %rbx
	leaq	300(%rbx), %rbp
	.p2align 4,,10
	.p2align 3
.L155:
	movq	%rbx, %rdi
	call	__unsetenv
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__rawmemchr
	leaq	1(%rax), %rbx
	cmpq	%rbp, %rbx
	jb	.L155
.L154:
	movq	_dl_platform(%rip), %rax
	testq	%rax, %rax
	je	.L156
	cmpb	$0, (%rax)
	je	.L336
.L156:
	leaq	.LC21(%rip), %rdi
	call	getenv
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L157
	leaq	48(%rsp), %rbp
	movl	$2, %r12d
	xorl	%r13d, %r13d
.L162:
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_dl_strtoul
	cmpq	$254, %rax
	ja	.L157
	movq	48(%rsp), %rdx
	cmpq	%rbx, %rdx
	je	.L157
	testq	%r12, %r12
	je	.L159
	movzbl	(%rdx), %esi
	cmpb	$46, %sil
	je	.L183
	testb	%sil, %sil
	jne	.L157
.L183:
	leal	0(,%r12,8), %ecx
	salq	%cl, %rax
	orq	%rax, %r13
	testb	%sil, %sil
	je	.L161
	leaq	1(%rdx), %rbx
	subq	$1, %r12
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L151:
	leaq	.LC1(%rip), %rax
	testl	%edx, %edx
	leaq	9(%rax), %rcx
	cmovne	%rcx, %rax
	movq	%rax, _dl_profile_output(%rip)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L159:
	orq	%rax, %r13
.L161:
	testq	%r13, %r13
	je	.L157
	movl	%r13d, _dl_osversion(%rip)
.L157:
	movq	_dl_platform(%rip), %rdi
	testq	%rdi, %rdi
	je	.L163
	call	strlen
	movq	%rax, _dl_platformlen(%rip)
.L163:
	movq	_dl_phdr(%rip), %rax
	movq	1144+_dl_main_map(%rip), %rsi
	testq	%rax, %rax
	je	.L164
	movq	_dl_phnum(%rip), %rcx
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rcx
	cmpq	%rcx, %rax
	jnb	.L164
	movq	1136+_dl_main_map(%rip), %r10
	movl	_dl_stack_flags(%rip), %ebx
	movq	%rsi, %r8
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	xorl	%edi, %edi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L338:
	cmpl	$1685382482, %edx
	jne	.L165
	movq	16(%rax), %r10
	movq	40(%rax), %r8
	movl	$1, %r9d
	movl	$1, %r11d
.L165:
	addq	$56, %rax
	cmpq	%rcx, %rax
	jnb	.L337
.L168:
	movl	(%rax), %edx
	cmpl	$1685382481, %edx
	jne	.L338
	movl	4(%rax), %ebx
	addq	$56, %rax
	movl	$1, %edi
	cmpq	%rcx, %rax
	jb	.L168
.L337:
	testb	%dil, %dil
	jne	.L339
.L169:
	testb	%r11b, %r11b
	je	.L170
	movq	%r10, 1136+_dl_main_map(%rip)
.L170:
	testb	%r9b, %r9b
	je	.L164
	movq	%r8, 1144+_dl_main_map(%rip)
	movq	%r8, %rsi
	.p2align 4,,10
	.p2align 3
.L164:
	testq	%rsi, %rsi
	je	.L81
	leaq	_dl_main_map(%rip), %rdi
	call	_dl_protect_relro
.L81:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	movq	$0, _dl_platform(%rip)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L83:
	movq	_dl_sysinfo_map(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L126
	movq	$0, _dl_vdso_clock_gettime64(%rip)
	movq	$0, _dl_vdso_gettimeofday(%rip)
.L135:
	movq	$0, _dl_vdso_time(%rip)
	movq	$0, _dl_vdso_getcpu(%rip)
.L145:
	xorl	%eax, %eax
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L174:
	xorl	%eax, %eax
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%eax, %eax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L177:
	xorl	%eax, %eax
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L176:
	xorl	%eax, %eax
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L175:
	xorl	%eax, %eax
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r11, %rdi
	subq	%rax, %rdi
	cmpq	$11, %rdi
	jbe	.L340
	movq	%r12, %rdi
	subq	%rax, %rdi
	cmpq	$10, %rdi
	ja	.L99
	movq	%r13, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L340:
	movq	%rbp, %rdi
	subq	%rax, %rdi
	movq	%rdi, %rax
	jmp	.L95
.L334:
	movq	_dl_sysinfo_map(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L137
.L335:
	movq	_dl_sysinfo_map(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L141
.L333:
	movq	_dl_sysinfo_map(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L133
.L332:
	movq	_dl_sysinfo_map(%rip), %rsi
	xorl	%eax, %eax
	jmp	.L129
.L330:
	andl	$-134220010, %esi
	je	.L121
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movl	1004(%rbx), %eax
	jmp	.L121
.L326:
	leaq	__PRETTY_FUNCTION__.10405(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$61, %edx
	call	__assert_fail
.L131:
	movq	$0, _dl_vdso_gettimeofday(%rip)
	movq	$0, _dl_vdso_time(%rip)
.L139:
	movq	$0, _dl_vdso_getcpu(%rip)
	xorl	%eax, %eax
	jmp	.L144
.L331:
	leaq	.LC9(%rip), %rdi
	call	_dl_fatal_printf
.L329:
	leaq	__PRETTY_FUNCTION__.10396(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$126, %edx
	call	__assert_fail
.L339:
	movl	%ebx, _dl_stack_flags(%rip)
	jmp	.L169
.L328:
	leaq	__PRETTY_FUNCTION__.10396(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$118, %edx
	call	__assert_fail
	.size	_dl_non_dynamic_init, .-_dl_non_dynamic_init
	.p2align 4,,15
	.globl	_dl_get_dl_main_map
	.hidden	_dl_get_dl_main_map
	.type	_dl_get_dl_main_map, @function
_dl_get_dl_main_map:
	leaq	_dl_main_map(%rip), %rax
	ret
	.size	_dl_get_dl_main_map, .-_dl_get_dl_main_map
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10396, @object
	.size	__PRETTY_FUNCTION__.10396, 21
__PRETTY_FUNCTION__.10396:
	.string	"elf_get_dynamic_info"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10405, @object
	.size	__PRETTY_FUNCTION__.10405, 11
__PRETTY_FUNCTION__.10405:
	.string	"setup_vdso"
	.section	.rodata
	.align 32
	.type	unsecure_envvars.10458, @object
	.size	unsecure_envvars.10458, 300
unsecure_envvars.10458:
	.string	"GCONV_PATH"
	.string	"GETCONF_DIR"
	.string	"HOSTALIASES"
	.string	"LD_AUDIT"
	.string	"LD_DEBUG"
	.string	"LD_DEBUG_OUTPUT"
	.string	"LD_DYNAMIC_WEAK"
	.string	"LD_HWCAP_MASK"
	.string	"LD_LIBRARY_PATH"
	.string	"LD_ORIGIN_PATH"
	.string	"LD_PRELOAD"
	.string	"LD_PROFILE"
	.string	"LD_SHOW_AUXV"
	.string	"LD_USE_LOAD_BIAS"
	.string	"LOCALDOMAIN"
	.string	"LOCPATH"
	.string	"MALLOC_TRACE"
	.string	"NIS_PATH"
	.string	"NLSPATH"
	.string	"RESOLV_HOST_CONF"
	.string	"RES_OPTIONS"
	.string	"TMPDIR"
	.string	"TZDIR"
	.string	"LD_PREFER_MAP_32BIT_EXEC"
	.string	""
	.hidden	_dl_clktck
	.comm	_dl_clktck,4,4
	.hidden	_dl_load_write_lock
	.globl	_dl_load_write_lock
	.data
	.align 32
	.type	_dl_load_write_lock, @object
	.size	_dl_load_write_lock, 40
_dl_load_write_lock:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.hidden	_dl_load_lock
	.globl	_dl_load_lock
	.align 32
	.type	_dl_load_lock, @object
	.size	_dl_load_lock, 40
_dl_load_lock:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	dyn_temp.10402, @object
	.size	dyn_temp.10402, 128
dyn_temp.10402:
	.zero	128
	.hidden	_dl_sysinfo_map
	.comm	_dl_sysinfo_map,8,8
	.hidden	_dl_sysinfo_dso
	.comm	_dl_sysinfo_dso,8,8
	.hidden	_dl_scope_free_list
	.comm	_dl_scope_free_list,8,8
	.hidden	_dl_stack_cache_lock
	.comm	_dl_stack_cache_lock,4,4
	.hidden	_dl_stack_user
	.comm	_dl_stack_user,16,16
	.hidden	_dl_stack_used
	.comm	_dl_stack_used,16,16
	.hidden	_dl_make_stack_executable_hook
	.globl	_dl_make_stack_executable_hook
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	_dl_make_stack_executable_hook, @object
	.size	_dl_make_stack_executable_hook, 8
_dl_make_stack_executable_hook:
	.quad	_dl_make_stack_executable
	.hidden	_dl_stack_flags
	.globl	_dl_stack_flags
	.data
	.align 4
	.type	_dl_stack_flags, @object
	.size	_dl_stack_flags, 4
_dl_stack_flags:
	.long	7
	.hidden	_dl_fpu_control
	.globl	_dl_fpu_control
	.align 2
	.type	_dl_fpu_control, @object
	.size	_dl_fpu_control, 2
_dl_fpu_control:
	.value	895
	.hidden	_dl_hwcap2
	.globl	_dl_hwcap2
	.bss
	.align 8
	.type	_dl_hwcap2, @object
	.size	_dl_hwcap2, 8
_dl_hwcap2:
	.zero	8
	.hidden	_dl_hwcap
	.globl	_dl_hwcap
	.align 8
	.type	_dl_hwcap, @object
	.size	_dl_hwcap, 8
_dl_hwcap:
	.zero	8
	.hidden	_dl_phnum
	.comm	_dl_phnum,8,8
	.hidden	_dl_phdr
	.comm	_dl_phdr,8,8
	.hidden	_dl_auxv
	.comm	_dl_auxv,8,8
	.hidden	_dl_correct_cache_id
	.globl	_dl_correct_cache_id
	.data
	.align 4
	.type	_dl_correct_cache_id, @object
	.size	_dl_correct_cache_id, 4
_dl_correct_cache_id:
	.long	771
	.hidden	_dl_debug_fd
	.globl	_dl_debug_fd
	.align 4
	.type	_dl_debug_fd, @object
	.size	_dl_debug_fd, 4
_dl_debug_fd:
	.long	2
	.hidden	_dl_initfirst
	.comm	_dl_initfirst,8,8
	.hidden	_dl_init_all_dirs
	.comm	_dl_init_all_dirs,8,8
	.hidden	_dl_all_dirs
	.comm	_dl_all_dirs,8,8
	.hidden	_dl_osversion
	.comm	_dl_osversion,4,4
	.hidden	_dl_inhibit_cache
	.comm	_dl_inhibit_cache,4,4
	.hidden	_dl_pagesize
	.globl	_dl_pagesize
	.align 8
	.type	_dl_pagesize, @object
	.size	_dl_pagesize, 8
_dl_pagesize:
	.quad	4096
	.hidden	_dl_init_static_tls
	.globl	_dl_init_static_tls
	.section	.data.rel.local
	.align 8
	.type	_dl_init_static_tls, @object
	.size	_dl_init_static_tls, 8
_dl_init_static_tls:
	.quad	_dl_nothread_init_static_tls
	.hidden	_dl_x86_platforms
	.globl	_dl_x86_platforms
	.section	.rodata
	.align 32
	.type	_dl_x86_platforms, @object
	.size	_dl_x86_platforms, 36
_dl_x86_platforms:
	.string	"i586"
	.zero	4
	.string	"i686"
	.zero	4
	.string	"haswell"
	.zero	1
	.string	"xeon_phi"
	.hidden	_dl_x86_hwcap_flags
	.globl	_dl_x86_hwcap_flags
	.align 16
	.type	_dl_x86_hwcap_flags, @object
	.size	_dl_x86_hwcap_flags, 27
_dl_x86_hwcap_flags:
	.string	"sse2"
	.zero	4
	.string	"x86_64"
	.zero	2
	.string	"avx512_1"
	.hidden	_dl_x86_cpu_features
	.globl	_dl_x86_cpu_features
	.bss
	.align 32
	.type	_dl_x86_cpu_features, @object
	.size	_dl_x86_cpu_features, 432
_dl_x86_cpu_features:
	.zero	432
	.hidden	_dl_x86_feature_control
	.globl	_dl_x86_feature_control
	.align 4
	.type	_dl_x86_feature_control, @object
	.size	_dl_x86_feature_control, 4
_dl_x86_feature_control:
	.zero	4
	.hidden	_dl_x86_feature_1
	.globl	_dl_x86_feature_1
	.align 4
	.type	_dl_x86_feature_1, @object
	.size	_dl_x86_feature_1, 4
_dl_x86_feature_1:
	.zero	4
	.hidden	_dl_initial_searchlist
	.globl	_dl_initial_searchlist
	.section	.data.rel.local
	.align 16
	.type	_dl_initial_searchlist, @object
	.size	_dl_initial_searchlist, 16
_dl_initial_searchlist:
	.quad	__compound_literal.3
	.long	1
	.zero	4
	.align 8
	.type	__compound_literal.3, @object
	.size	__compound_literal.3, 8
__compound_literal.3:
	.quad	_dl_main_map
	.hidden	_dl_load_adds
	.globl	_dl_load_adds
	.data
	.align 8
	.type	_dl_load_adds, @object
	.size	_dl_load_adds, 8
_dl_load_adds:
	.quad	1
	.hidden	_dl_nns
	.globl	_dl_nns
	.align 8
	.type	_dl_nns, @object
	.size	_dl_nns, 8
_dl_nns:
	.quad	1
	.hidden	_dl_ns
	.globl	_dl_ns
	.section	.data.rel.local
	.align 32
	.type	_dl_ns, @object
	.size	_dl_ns, 152
_dl_ns:
	.quad	_dl_main_map
	.long	1
	.zero	4
	.quad	_dl_main_map+704
	.zero	128
	.align 32
	.type	_dl_main_map, @object
	.size	_dl_main_map, 1160
_dl_main_map:
	.zero	8
	.quad	.LC0
	.zero	24
	.quad	_dl_main_map
	.quad	0
	.quad	__compound_literal.0
	.zero	640
	.quad	__compound_literal.1
	.long	1
	.zero	4
	.quad	__compound_literal.2
	.zero	8
	.zero	60
	.byte	0
	.zero	83
	.quad	_dl_main_map+704
	.zero	24
	.quad	4
	.quad	_dl_main_map+880
	.quad	_dl_main_map+704
	.zero	8
	.zero	52
	.long	1
	.zero	112
	.quad	0
	.zero	32
	.quad	1
	.local	__compound_literal.2
	.comm	__compound_literal.2,8,8
	.align 8
	.type	__compound_literal.1, @object
	.size	__compound_literal.1, 8
__compound_literal.1:
	.quad	_dl_main_map
	.align 16
	.type	__compound_literal.0, @object
	.size	__compound_literal.0, 24
__compound_literal.0:
	.quad	.LC0
	.zero	8
	.long	1
	.zero	4
	.hidden	_dl_bind_not
	.comm	_dl_bind_not,4,4
	.hidden	_dl_origin_path
	.comm	_dl_origin_path,8,8
	.hidden	_dl_profile_map
	.comm	_dl_profile_map,8,8
	.hidden	_dl_inhibit_rpath
	.comm	_dl_inhibit_rpath,8,8
	.hidden	_dl_profile_output
	.comm	_dl_profile_output,8,8
	.hidden	_dl_profile
	.comm	_dl_profile,8,8
	.hidden	_dl_verbose
	.comm	_dl_verbose,4,4
	.hidden	_dl_dynamic_weak
	.comm	_dl_dynamic_weak,4,4
	.hidden	_dl_use_load_bias
	.globl	_dl_use_load_bias
	.data
	.align 8
	.type	_dl_use_load_bias, @object
	.size	_dl_use_load_bias, 8
_dl_use_load_bias:
	.quad	-2
	.hidden	_dl_lazy
	.comm	_dl_lazy,4,4
	.hidden	_dl_debug_mask
	.comm	_dl_debug_mask,4,4
	.hidden	_dl_platformlen
	.comm	_dl_platformlen,8,8
	.hidden	_dl_platform
	.comm	_dl_platform,8,8
	.hidden	_dl_random
	.globl	_dl_random
	.section	.data.rel.ro
	.align 8
	.type	_dl_random, @object
	.size	_dl_random, 8
_dl_random:
	.zero	8
	.hidden	_dl_argv
	.globl	_dl_argv
	.align 8
	.type	_dl_argv, @object
	.size	_dl_argv, 8
_dl_argv:
	.quad	__progname
	.hidden	__libc_stack_end
	.globl	__libc_stack_end
	.align 8
	.type	__libc_stack_end, @object
	.size	__libc_stack_end, 8
__libc_stack_end:
	.zero	8
	.hidden	_dl_vdso_clock_getres_time64
	.globl	_dl_vdso_clock_getres_time64
	.align 8
	.type	_dl_vdso_clock_getres_time64, @object
	.size	_dl_vdso_clock_getres_time64, 8
_dl_vdso_clock_getres_time64:
	.zero	8
	.hidden	_dl_vdso_getcpu
	.globl	_dl_vdso_getcpu
	.align 8
	.type	_dl_vdso_getcpu, @object
	.size	_dl_vdso_getcpu, 8
_dl_vdso_getcpu:
	.zero	8
	.hidden	_dl_vdso_time
	.globl	_dl_vdso_time
	.align 8
	.type	_dl_vdso_time, @object
	.size	_dl_vdso_time, 8
_dl_vdso_time:
	.zero	8
	.hidden	_dl_vdso_gettimeofday
	.globl	_dl_vdso_gettimeofday
	.align 8
	.type	_dl_vdso_gettimeofday, @object
	.size	_dl_vdso_gettimeofday, 8
_dl_vdso_gettimeofday:
	.zero	8
	.hidden	_dl_vdso_clock_gettime64
	.globl	_dl_vdso_clock_gettime64
	.align 8
	.type	_dl_vdso_clock_gettime64, @object
	.size	_dl_vdso_clock_gettime64, 8
_dl_vdso_clock_gettime64:
	.zero	8
	.hidden	__progname
	.hidden	_dl_nothread_init_static_tls
	.hidden	_dl_make_stack_executable
	.hidden	_dl_fatal_printf
	.hidden	__assert_fail
	.hidden	_dl_debug_printf
	.hidden	_dl_protect_relro
	.hidden	_dl_strtoul
	.hidden	__rawmemchr
	.hidden	__unsetenv
	.hidden	_dl_init_paths
	.hidden	_dl_lookup_symbol_x
	.hidden	_dl_add_to_namespace_list
	.hidden	malloc
	.hidden	strlen
	.hidden	_dl_setup_hash
	.hidden	_dl_new_object
	.hidden	getenv
	.hidden	_dl_get_origin
	.hidden	__libc_enable_secure
	.hidden	__libc_enable_secure_decided
