	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\t%s ("
.LC1:
	.string	"unknown"
.LC2:
	.string	",64bit"
.LC3:
	.string	",IA-64"
.LC4:
	.string	",x86-64"
.LC5:
	.string	",N32"
.LC6:
	.string	",x32"
.LC7:
	.string	",hard-float"
.LC8:
	.string	",AArch64"
.LC9:
	.string	",soft-float"
.LC10:
	.string	",nan2008"
.LC11:
	.string	",N32,nan2008"
.LC12:
	.string	",64bit,nan2008"
.LC13:
	.string	",double-float"
.LC14:
	.string	",%d"
.LC15:
	.string	", hwcap: \"%s\""
.LC16:
	.string	", hwcap: %#.16lx"
.LC17:
	.string	", OS ABI: %s %d.%d.%d"
.LC18:
	.string	") => %s\n"
	.text
	.p2align 4,,15
	.type	print_entry, @function
print_entry:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r14
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebx
	movq	%rdi, %rsi
	leaq	.LC0(%rip), %rdi
	movl	%edx, %ebp
	subq	$8, %rsp
	movq	%r8, %r12
	movq	%r9, %r13
	call	printf@PLT
	movzbl	%bl, %eax
	cmpl	$3, %eax
	ja	.L2
	leaq	flag_descr(%rip), %rdx
	movq	stdout(%rip), %rsi
	movq	(%rdx,%rax,8), %rdi
	call	fputs@PLT
.L4:
	andl	$65280, %ebx
	cmpl	$2048, %ebx
	je	.L6
	jle	.L56
	cmpl	$3072, %ebx
	je	.L15
	jle	.L57
	cmpl	$3584, %ebx
	je	.L20
	jle	.L58
	cmpl	$3840, %ebx
	je	.L18
	cmpl	$4096, %ebx
	jne	.L5
	movq	stdout(%rip), %rcx
	leaq	.LC13(%rip), %rdi
	movl	$13, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L2:
	movq	stdout(%rip), %r15
	leaq	.LC1(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	fputs@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	$768, %ebx
	je	.L8
	jle	.L59
	cmpl	$1280, %ebx
	je	.L10
	jle	.L60
	cmpl	$1536, %ebx
	je	.L14
	cmpl	$1792, %ebx
	jne	.L5
.L10:
	movq	stdout(%rip), %rcx
	leaq	.LC2(%rip), %rdi
	movl	$6, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L12:
	testq	%r12, %r12
	je	.L24
	leaq	.LC15(%rip), %rdi
	movq	%r12, %rsi
	xorl	%eax, %eax
	call	printf@PLT
.L25:
	testl	%ebp, %ebp
	je	.L26
	movl	%ebp, %eax
	movl	$6, %edx
	leaq	_libc_intl_domainname(%rip), %rdi
	shrl	$24, %eax
	cmpl	$6, %eax
	cmova	%rdx, %rax
	leaq	abi_tag_os.10101(%rip), %rdx
	movq	(%rdx,%rax,8), %rsi
	movl	$5, %edx
	call	__dcgettext@PLT
	leaq	.LC17(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%rax, %rbx
	call	__dcgettext@PLT
	movl	%ebp, %edx
	movl	%ebp, %ecx
	movq	%rax, %rdi
	shrl	$16, %edx
	movzbl	%ch, %ecx
	movzbl	%bpl, %r8d
	movzbl	%dl, %edx
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	printf@PLT
.L26:
	addq	$8, %rsp
	movq	%r13, %rsi
	leaq	.LC18(%rip), %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	xorl	%eax, %eax
	jmp	printf@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	testq	%r14, %r14
	je	.L25
	leaq	.LC16(%rip), %rdi
	movq	%r14, %rsi
	xorl	%eax, %eax
	call	printf@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L20:
	movq	stdout(%rip), %rcx
	leaq	.LC12(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	$256, %ebx
	je	.L10
	cmpl	$512, %ebx
	je	.L11
	testl	%ebx, %ebx
	je	.L12
.L5:
	leaq	.LC14(%rip), %rdi
	movl	%ebx, %esi
	xorl	%eax, %eax
	call	printf@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$2560, %ebx
	je	.L17
	cmpl	$2816, %ebx
	je	.L18
	cmpl	$2304, %ebx
	jne	.L5
	movq	stdout(%rip), %rcx
	leaq	.LC7(%rip), %rdi
	movl	$11, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L18:
	movq	stdout(%rip), %rcx
	leaq	.LC9(%rip), %rdi
	movl	$11, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	$3328, %ebx
	jne	.L5
	movq	stdout(%rip), %rcx
	leaq	.LC11(%rip), %rdi
	movl	$12, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L60:
	cmpl	$1024, %ebx
	je	.L10
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
	movq	stdout(%rip), %rcx
	leaq	.LC8(%rip), %rdi
	movl	$8, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L8:
	movq	stdout(%rip), %rcx
	leaq	.LC4(%rip), %rdi
	movl	$7, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L15:
	movq	stdout(%rip), %rcx
	leaq	.LC10(%rip), %rdi
	movl	$8, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	movq	stdout(%rip), %rcx
	leaq	.LC5(%rip), %rdi
	movl	$4, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L6:
	movq	stdout(%rip), %rcx
	leaq	.LC6(%rip), %rdi
	movl	$4, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	movq	stdout(%rip), %rcx
	leaq	.LC3(%rip), %rdi
	movl	$6, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L12
	.size	print_entry, .-print_entry
	.p2align 4,,15
	.type	assign_glibc_hwcaps_indices_compare, @function
assign_glibc_hwcaps_indices_compare:
	movq	(%rdi), %rax
	movq	8(%rax), %rdi
	movq	(%rsi), %rax
	movq	8(%rax), %rsi
	addq	$16, %rdi
	addq	$16, %rsi
	jmp	strcmp@PLT
	.size	assign_glibc_hwcaps_indices_compare, .-assign_glibc_hwcaps_indices_compare
	.p2align 4,,15
	.type	insert_to_aux_cache, @function
insert_to_aux_cache:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r13d
	movl	%esi, %r14d
	movl	%r9d, %ebp
	subq	$40, %rsp
	movq	(%rdi), %rdi
	movq	8(%r15), %rcx
	movq	16(%r15), %rsi
	movq	24(%r15), %r9
	leaq	(%rdi,%rdi,4), %rax
	leaq	(%rdi,%rax,2), %rax
	addq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	addq	%rsi, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rax,%r9), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	xorl	%edx, %eax
	xorl	%edx, %edx
	divq	aux_hash_size(%rip)
	movq	aux_hash(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	leaq	0(,%rdx,8), %rbx
	testq	%rax, %rax
	jne	.L65
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L64:
	movq	56(%rax), %rax
	testq	%rax, %rax
	je	.L63
.L65:
	cmpq	%rdi, (%rax)
	jne	.L64
	cmpq	%rcx, 8(%rax)
	jne	.L64
	cmpq	%rsi, 16(%rax)
	jne	.L64
	cmpq	%r9, 24(%rax)
	jne	.L64
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	testq	%r8, %r8
	je	.L66
	movq	%r8, %rdi
	movq	%r8, 24(%rsp)
	call	strlen@PLT
	leaq	65(%rax), %rdi
	movq	%rax, 16(%rsp)
	call	xmalloc@PLT
	movdqu	(%r15), %xmm0
	leaq	64(%rax), %rdi
	movq	16(%rsp), %rdx
	movq	24(%rsp), %r8
	movups	%xmm0, (%rax)
	movl	%r14d, 32(%rax)
	movl	%r13d, 36(%rax)
	addq	$1, %rdx
	movq	%r8, %rsi
	movdqu	16(%r15), %xmm0
	movl	%r12d, 40(%rax)
	movl	%ebp, 44(%rax)
	movups	%xmm0, 16(%rax)
	movq	%rax, 8(%rsp)
	call	memcpy@PLT
	movq	8(%rsp), %rcx
	movq	%rax, 48(%rcx)
.L67:
	addq	aux_hash(%rip), %rbx
	movq	(%rbx), %rax
	movq	%rax, 56(%rcx)
	movq	%rcx, (%rbx)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movl	$64, %edi
	call	xmalloc@PLT
	movdqu	(%r15), %xmm0
	movq	%rax, %rcx
	movl	%r14d, 32(%rax)
	movl	%r13d, 36(%rax)
	movups	%xmm0, (%rax)
	movl	%r12d, 40(%rax)
	movl	%ebp, 44(%rax)
	movdqu	16(%r15), %xmm0
	movq	$0, 48(%rax)
	movups	%xmm0, 16(%rax)
	jmp	.L67
	.size	insert_to_aux_cache, .-insert_to_aux_cache
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC19:
	.string	"Cache file has wrong endianness.\n"
	.section	.text.unlikely,"ax",@progbits
	.type	check_new_cache.part.3, @function
check_new_cache.part.3:
	leaq	.LC19(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	subq	$8, %rsp
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.size	check_new_cache.part.3, .-check_new_cache.part.3
	.text
	.p2align 4,,15
	.globl	glibc_hwcaps_subdirectory_name
	.type	glibc_hwcaps_subdirectory_name, @function
glibc_hwcaps_subdirectory_name:
	movq	8(%rdi), %rax
	addq	$16, %rax
	ret
	.size	glibc_hwcaps_subdirectory_name, .-glibc_hwcaps_subdirectory_name
	.p2align 4,,15
	.globl	new_glibc_hwcaps_subdirectory
	.type	new_glibc_hwcaps_subdirectory, @function
new_glibc_hwcaps_subdirectory:
	pushq	%rbx
	movq	%rdi, %rsi
	leaq	strings(%rip), %rdi
	call	stringtable_add@PLT
	movq	%rax, %rbx
	movq	hwcaps(%rip), %rax
	testq	%rax, %rax
	je	.L78
	cmpq	8(%rax), %rbx
	jne	.L80
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L81:
	cmpq	%rbx, 8(%rax)
	je	.L77
.L80:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L81
.L78:
	movl	$24, %edi
	call	xmalloc@PLT
	movq	hwcaps(%rip), %rdx
	movq	%rbx, 8(%rax)
	movl	$0, 16(%rax)
	movb	$0, 20(%rax)
	movq	%rax, hwcaps(%rip)
	movq	%rdx, (%rax)
.L77:
	popq	%rbx
	ret
	.size	new_glibc_hwcaps_subdirectory, .-new_glibc_hwcaps_subdirectory
	.section	.rodata.str1.1
.LC20:
	.string	"Can't open cache file %s\n"
.LC21:
	.string	"mmap of cache file failed.\n"
.LC22:
	.string	"File is not a cache file.\n"
.LC23:
	.string	"ld.so-1.7.0"
.LC24:
	.string	"glibc-ld.so.cache"
.LC25:
	.string	"1.1"
.LC26:
	.string	"%d libs found in cache `%s'\n"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"Malformed extension data in cache file %s\n"
	.section	.rodata.str1.1
.LC28:
	.string	"Cache generated by: "
	.text
	.p2align 4,,15
	.globl	print_cache
	.type	print_cache, @function
print_cache:
	pushq	%r15
	pushq	%r14
	xorl	%esi, %esi
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$216, %rsp
	call	open@PLT
	testl	%eax, %eax
	js	.L161
	leaq	64(%rsp), %rsi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	__fstat64@PLT
	testl	%eax, %eax
	js	.L160
	movq	112(%rsp), %rsi
	testq	%rsi, %rsi
	jne	.L89
.L160:
	movl	%ebx, %edi
	call	close@PLT
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%ebx, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	call	mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L162
	movq	112(%rsp), %rbp
	cmpq	$15, %rbp
	jbe	.L98
	movabsq	$3328491556300547180, %rax
	cmpq	%rax, (%r15)
	je	.L163
.L93:
	movabsq	$7521962881247572782, %rdx
	movabsq	$7236208606153632871, %rax
	xorq	8(%r15), %rdx
	xorq	(%r15), %rax
	orq	%rax, %rdx
	je	.L164
.L98:
	leaq	.LC22(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.p2align 4,,10
	.p2align 3
.L164:
	cmpb	$101, 16(%r15)
	jne	.L98
	cmpw	$11825, 17(%r15)
	jne	.L98
	cmpb	$49, 19(%r15)
	jne	.L98
	movzbl	28(%r15), %eax
	testb	%al, %al
	je	.L127
	andl	$3, %eax
	cmpb	$2, %al
	je	.L127
.L102:
	call	check_new_cache.part.3
	.p2align 4,,10
	.p2align 3
.L163:
	cmpw	$11831, 8(%r15)
	jne	.L93
	cmpb	$48, 10(%r15)
	jne	.L93
	leaq	-16(%rbp), %rdx
	movabsq	$-6148914691236517205, %rsi
	movl	12(%r15), %ecx
	movq	%rdx, %rax
	mulq	%rsi
	movl	%ecx, (%rsp)
	shrq	$3, %rdx
	cmpq	%rcx, %rdx
	jb	.L98
	leaq	(%rcx,%rcx,2), %rax
	salq	$2, %rax
	leaq	23(%rax), %r10
	leaq	16(%r15,%rax), %r13
	andq	$-8, %r10
	leaq	48(%r10), %rax
	cmpq	%rbp, %rax
	jnb	.L103
	leaq	(%r15,%r10), %r14
	movabsq	$7521962881247572782, %rdx
	movabsq	$7236208606153632871, %rax
	xorq	8(%r14), %rdx
	xorq	(%r14), %rax
	orq	%rax, %rdx
	jne	.L103
	cmpb	$101, 16(%r14)
	je	.L165
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	.LC26(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movl	(%rsp), %esi
	movq	%r12, %rdx
	movq	%rax, %rdi
	xorl	%eax, %eax
	xorl	%r12d, %r12d
	call	printf@PLT
	movl	12(%r15), %ecx
	testl	%ecx, %ecx
	je	.L109
	.p2align 4,,10
	.p2align 3
.L108:
	movl	%r12d, %eax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	leaq	(%rax,%rax,2), %rax
	addl	$1, %r12d
	leaq	(%r15,%rax,4), %rax
	movl	24(%rax), %edx
	movl	20(%rax), %edi
	movl	16(%rax), %esi
	leaq	0(%r13,%rdx), %r9
	addq	%r13, %rdi
	xorl	%edx, %edx
	call	print_entry
	cmpl	%r12d, 12(%r15)
	ja	.L108
.L109:
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	munmap@PLT
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%r15, %r14
.L101:
	movl	32(%r14), %eax
	pxor	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)
	testl	%eax, %eax
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 48(%rsp)
	je	.L111
	testb	$3, %al
	jne	.L112
	leaq	8(%rax), %rdx
	cmpq	%rdx, %rbp
	jb	.L112
	addq	%r15, %rax
	cmpl	$-358342284, (%rax)
	je	.L166
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	.LC27(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%r12, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.p2align 4,,10
	.p2align 3
.L111:
	movl	20(%r14), %r13d
	leaq	.LC26(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%r12, %rdx
	movq	%rax, %rdi
	movl	%r13d, %esi
	xorl	%eax, %eax
	call	printf@PLT
	movl	20(%r14), %eax
	movq	$0, 8(%rsp)
	testl	%eax, %eax
	je	.L109
.L125:
	movq	40(%rsp), %rax
	testq	%rax, %rax
	movq	%rax, (%rsp)
	setne	%r12b
	xorl	%r13d, %r13d
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%r8d, %r8d
.L122:
	leaq	(%rdx,%rdx,2), %rdx
	addl	$1, %r13d
	leaq	(%r14,%rdx,8), %rsi
	movl	56(%rsi), %edx
	movl	52(%rsi), %edi
	movq	64(%rsi), %rcx
	leaq	(%r14,%rdx), %r9
	movl	60(%rsi), %edx
	movl	48(%rsi), %esi
	addq	%r14, %rdi
	call	print_entry
	cmpl	%r13d, 20(%r14)
	jbe	.L121
.L123:
	movl	%r13d, %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	64(%r14,%rcx,8), %rsi
	movq	%rsi, %rcx
	shrq	$32, %rcx
	andq	$-1024, %rcx
	cmpq	$1073741824, %rcx
	jne	.L129
	testb	%r12b, %r12b
	je	.L129
	movq	48(%rsp), %rcx
	movl	%esi, %esi
	xorl	%r8d, %r8d
	shrq	$2, %rcx
	cmpq	%rcx, %rsi
	jnb	.L122
	movq	(%rsp), %rax
	movl	(%rax,%rsi,4), %ecx
	movl	$0, %eax
	leaq	(%r15,%rcx), %r8
	cmpq	%rcx, %rbp
	cmovbe	%rax, %r8
	jmp	.L122
.L166:
	movl	4(%rax), %ecx
	movq	%rcx, %rdi
	salq	$4, %rcx
	addq	%rcx, %rdx
	cmpq	%rdx, %rbp
	jb	.L112
	testl	%edi, %edi
	je	.L119
	movl	16(%rax), %ecx
	movl	20(%rax), %esi
	leaq	(%rsi,%rcx), %rdx
	cmpq	%rdx, %rbp
	jb	.L112
	subl	$1, %edi
	addq	$8, %rax
	salq	$4, %rdi
	addq	%rax, %rdi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L118:
	movl	24(%rax), %ecx
	movl	28(%rax), %esi
	addq	$16, %rax
	leaq	(%rcx,%rsi), %rdx
	cmpq	%rdx, %rbp
	jb	.L112
.L116:
	movl	(%rax), %edx
	cmpl	$1, %edx
	ja	.L117
	leaq	(%rdx,%rdx,2), %rdx
	addq	%r15, %rcx
	salq	$3, %rdx
	movq	%rcx, 16(%rsp,%rdx)
	movl	4(%rax), %ecx
	movq	%rsi, 24(%rsp,%rdx)
	movl	%ecx, 32(%rsp,%rdx)
.L117:
	cmpq	%rdi, %rax
	jne	.L118
.L119:
	movq	48(%rsp), %rax
	movq	16(%rsp), %rcx
	testq	%rax, %rax
	movq	%rcx, 8(%rsp)
	je	.L114
	testb	$3, 40(%rsp)
	jne	.L114
	testb	$3, %al
	je	.L120
.L114:
	movq	$0, 40(%rsp)
	movq	$0, 48(%rsp)
	movl	$0, 56(%rsp)
.L120:
	movl	20(%r14), %r13d
	leaq	.LC26(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%r12, %rdx
	movq	%rax, %rdi
	movl	%r13d, %esi
	xorl	%eax, %eax
	call	printf@PLT
	movl	20(%r14), %edx
	testl	%edx, %edx
	jne	.L125
	.p2align 4,,10
	.p2align 3
.L121:
	movq	8(%rsp), %r14
	testq	%r14, %r14
	je	.L109
	movq	stdout(%rip), %r12
	leaq	.LC28(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	fputs@PLT
	movq	stdout(%rip), %rcx
	movq	24(%rsp), %rdx
	movq	%r14, %rdi
	movl	$1, %esi
	call	fwrite@PLT
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	call	putc@PLT
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L165:
	cmpw	$11825, 17(%r14)
	jne	.L103
	cmpb	$49, 19(%r14)
	jne	.L103
	movzbl	28(%r14), %eax
	testb	%al, %al
	je	.L101
	andl	$3, %eax
	cmpb	$2, %al
	jne	.L102
	jmp	.L101
.L161:
	leaq	.LC20(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %esi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L162:
	leaq	.LC21(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %esi
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.size	print_cache, .-print_cache
	.p2align 4,,15
	.globl	init_cache
	.type	init_cache, @function
init_cache:
	movq	$0, entries(%rip)
	ret
	.size	init_cache, .-init_cache
	.section	.rodata.str1.1
.LC29:
	.string	"cache.c"
.LC30:
	.string	"i == count"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"%s: ISA level is too high (%d > %d)"
	.section	.rodata.str1.1
.LC32:
	.string	"%s~"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"Can't create temporary cache file %s"
	.section	.rodata.str1.1
.LC34:
	.string	"Writing of cache data failed"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"(unsigned long long int) (extension_offset - old_offset) < 4"
	.align 8
.LC36:
	.string	"Writing of cache extension data failed"
	.align 8
.LC37:
	.string	"ldconfig (GNU libc) release release version 2.33"
	.align 8
.LC38:
	.string	"Changing access rights of %s to %#o failed"
	.section	.rodata.str1.1
.LC39:
	.string	"Renaming of %s to %s failed"
	.text
	.p2align 4,,15
	.globl	save_cache
	.type	save_cache, @function
save_cache:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	xorl	%ebx, %ebx
	subq	$72, %rsp
	movq	hwcaps(%rip), %rax
	movq	%rdi, -88(%rbp)
	testq	%rax, %rax
	je	.L281
	.p2align 4,,10
	.p2align 3
.L169:
	cmpb	$0, 20(%rax)
	je	.L172
	addq	$1, %rbx
.L172:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L169
	leaq	0(,%rbx,8), %rdi
	call	xmalloc@PLT
	movq	hwcaps(%rip), %rdx
	movq	%rax, %r12
	testq	%rdx, %rdx
	je	.L173
.L170:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L175:
	cmpb	$0, 20(%rdx)
	je	.L174
	movq	%rdx, (%r12,%rax,8)
	addq	$1, %rax
.L174:
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L175
	cmpq	%rax, %rbx
	jne	.L227
	leaq	assign_glibc_hwcaps_indices_compare(%rip), %rcx
	movl	$8, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	qsort@PLT
	testq	%rbx, %rbx
	je	.L226
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%r12,%rdx,8), %rax
	movl	%edx, 16(%rax)
	addq	$1, %rdx
	cmpq	%rdx, %rbx
	jne	.L178
.L226:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	call	free@PLT
	movq	entries(%rip), %rax
	testq	%rax, %rax
	je	.L179
	.p2align 4,,10
	.p2align 3
.L181:
	addl	$1, %ebx
	cmpq	$1, 32(%rax)
	movq	56(%rax), %rax
	adcl	$0, %r14d
	testq	%rax, %rax
	jne	.L181
.L179:
	leaq	-64(%rbp), %rsi
	leaq	strings(%rip), %rdi
	call	stringtable_finalize@PLT
	movl	opt_format(%rip), %eax
	cmpl	$2, %eax
	je	.L233
	testl	%eax, %eax
	jne	.L282
.L183:
	movslq	%r14d, %rax
	leaq	(%rax,%rax,2), %r15
	leaq	0(,%r15,4), %r12
	leaq	16(%r12), %r13
	movq	%r13, %rdi
	call	xmalloc@PLT
	xorl	%ecx, %ecx
	movl	$11831, %esi
	movq	%rax, -80(%rbp)
	movw	%cx, 10(%rax)
	movabsq	$3328491556300547180, %rcx
	movw	%si, 8(%rax)
	movq	%rcx, (%rax)
	movb	$48, 10(%rax)
	movl	%r14d, 12(%rax)
	movl	opt_format(%rip), %esi
	leaq	23(%r12), %rax
	andq	$-8, %rax
	subq	%r13, %rax
	testl	%esi, %esi
	movq	%rax, -72(%rbp)
	jne	.L182
	movq	entries(%rip), %rax
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	movq	$0, -96(%rbp)
	testq	%rax, %rax
	je	.L235
.L292:
	movl	-72(%rbp), %ebx
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	-80(%rbp), %r8
	leal	(%rcx,%rbx), %r10d
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L187:
	cmpq	$0, 32(%rax)
	jne	.L189
	movslq	%edi, %rbx
	movl	16(%rax), %r9d
	movq	8(%rax), %r12
	leaq	(%rbx,%rbx,2), %rbx
	leaq	(%r8,%rbx,4), %rbx
	movl	%r9d, 16(%rbx)
	movl	%r10d, 20(%rbx)
	movq	(%rax), %rbx
	movl	12(%rbx), %r11d
	addl	%ecx, %r11d
	movl	%r11d, 20(%r8,%rdx)
	movl	12(%r12), %ebx
	addl	%ecx, %ebx
	testl	%esi, %esi
	movl	%ebx, 24(%r8,%rdx)
	jne	.L188
.L194:
	addl	$1, %edi
.L190:
	movq	56(%rax), %rax
	addq	$12, %rdx
	testq	%rax, %rax
	je	.L186
.L195:
	cmpl	$2, %esi
	jne	.L187
.L277:
	movl	16(%rax), %r9d
	movq	8(%rax), %r12
.L188:
	movl	%r9d, 48(%r15,%rdx,2)
	movl	20(%rax), %r9d
	movl	%r9d, 60(%r15,%rdx,2)
	movq	48(%rax), %r9
	testq	%r9, %r9
	je	.L283
	movl	24(%rax), %ebx
	cmpl	$1023, %ebx
	ja	.L284
	movl	16(%r9), %r9d
	salq	$32, %rbx
	movabsq	$4611686018427387904, %r11
	orq	%r9, %rbx
	orq	%r11, %rbx
	movq	%rbx, 64(%r15,%rdx,2)
	movq	32(%rax), %r9
.L192:
	movq	(%rax), %rbx
	movl	12(%rbx), %r11d
	movl	12(%r12), %ebx
	addl	%ecx, %r11d
	addl	%ecx, %ebx
	testq	%r9, %r9
	movl	%r11d, 52(%r15,%rdx,2)
	movl	%ebx, 56(%r15,%rdx,2)
	je	.L194
	movq	56(%rax), %rax
	addq	$12, %rdx
	testq	%rax, %rax
	jne	.L195
	.p2align 4,,10
	.p2align 3
.L186:
	movl	-56(%rbp), %eax
	cmpl	$2, %esi
	leal	3(%rax), %ebx
	je	.L236
.L293:
	cmpl	%r14d, %edi
	jl	.L285
	testl	%esi, %esi
	jne	.L224
.L291:
	addl	%r13d, %ebx
	andl	$-4, %ebx
.L225:
	movq	-88(%rbp), %r12
	movl	%ecx, -104(%rbp)
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	2(%rax), %rdi
	call	xmalloc@PLT
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	movq	%rax, %r14
	xorl	%eax, %eax
	call	sprintf@PLT
	xorl	%eax, %eax
	movl	$384, %edx
	movl	$131649, %esi
	movq	%r14, %rdi
	call	open@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	movl	-104(%rbp), %ecx
	js	.L286
	cmpl	$2, opt_format(%rip)
	jne	.L287
.L200:
	movq	-96(%rbp), %r13
	movq	%r15, %rsi
	movl	%r12d, %edi
	movl	%ecx, -72(%rbp)
	movq	%r13, %rdx
	call	write@PLT
	cmpq	%r13, %rax
	movl	-72(%rbp), %ecx
	jne	.L220
.L202:
	movq	-56(%rbp), %rdx
	movq	-64(%rbp), %rsi
	movl	%r12d, %edi
	movl	%ecx, -72(%rbp)
	call	write@PLT
	cmpq	-56(%rbp), %rax
	movl	-72(%rbp), %ecx
	jne	.L220
	movl	opt_format(%rip), %eax
	movl	%ecx, -72(%rbp)
	testl	%eax, %eax
	je	.L206
	movl	%ebx, %r13d
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%r13, %rsi
	call	lseek64@PLT
	subq	%rax, %r13
	movl	-72(%rbp), %ecx
	cmpq	$3, %r13
	ja	.L288
	movq	hwcaps(%rip), %rax
	testq	%rax, %rax
	je	.L208
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L210:
	cmpb	$0, 20(%rax)
	je	.L209
	addq	$1, %r13
.L209:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L210
	leal	0(,%r13,4), %eax
	movl	%ecx, -108(%rbp)
	movl	%r13d, -96(%rbp)
	movq	%rax, %rdi
	movl	%eax, -72(%rbp)
	movq	%rax, -104(%rbp)
	call	xmalloc@PLT
	movq	%rax, %r13
	movq	hwcaps(%rip), %rax
	movl	-96(%rbp), %r9d
	movl	-108(%rbp), %ecx
	testq	%rax, %rax
	je	.L211
	.p2align 4,,10
	.p2align 3
.L213:
	cmpb	$0, 20(%rax)
	je	.L212
	movq	8(%rax), %rsi
	movl	16(%rax), %edx
	movl	12(%rsi), %edi
	addl	%ecx, %edi
	movl	%edi, 0(%r13,%rdx,4)
.L212:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L213
.L211:
	testl	%r9d, %r9d
	jne	.L214
.L229:
	addl	$24, %ebx
	movl	$40, %edi
	call	xmalloc@PLT
	movl	%ebx, 16(%rax)
	movq	%rax, %rcx
	movl	$-358342284, (%rax)
	movq	$0, 8(%rax)
	movl	$48, 20(%rax)
	movl	$24, %ebx
	movl	$1, %eax
.L215:
	movl	%eax, 4(%rcx)
	movq	%rcx, %rsi
	movq	%rbx, %rdx
	movl	%r12d, %edi
	movq	%rcx, -96(%rbp)
	call	write@PLT
	cmpq	%rbx, %rax
	movq	-96(%rbp), %rcx
	jne	.L217
	movq	-104(%rbp), %rdx
	movq	%r13, %rsi
	movl	%r12d, %edi
	movq	%rcx, -96(%rbp)
	call	write@PLT
	movl	-72(%rbp), %r13d
	cmpq	%r13, %rax
	jne	.L217
	leaq	.LC37(%rip), %rsi
	movl	$48, %edx
	movl	%r12d, %edi
	call	write@PLT
	cmpq	$48, %rax
	jne	.L217
	movq	-96(%rbp), %rcx
	movq	%rcx, %rdi
	call	free@PLT
.L206:
	movl	$420, %esi
	movq	%r14, %rdi
	call	chmod@PLT
	testl	%eax, %eax
	jne	.L289
	movl	%r12d, %edi
	call	fsync@PLT
	testl	%eax, %eax
	jne	.L220
	movl	%r12d, %edi
	call	close@PLT
	testl	%eax, %eax
	jne	.L220
	movq	-88(%rbp), %rsi
	movq	%r14, %rdi
	call	rename@PLT
	testl	%eax, %eax
	jne	.L290
	movq	%r15, %rdi
	call	free@PLT
	movq	-80(%rbp), %rdi
	call	free@PLT
	movq	-64(%rbp), %rdi
	call	free@PLT
	movq	entries(%rip), %rdi
	testq	%rdi, %rdi
	je	.L168
	.p2align 4,,10
	.p2align 3
.L223:
	movq	56(%rdi), %rbx
	movq	%rbx, entries(%rip)
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L223
.L168:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	testl	%esi, %esi
	je	.L190
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L283:
	movq	32(%rax), %r9
	movq	%r9, 64(%r15,%rdx,2)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L285:
	movslq	%edi, %rax
	subl	$1, %edi
	movq	-80(%rbp), %r10
	movslq	%edi, %rdi
	leaq	(%rax,%rax,2), %rax
	testl	%esi, %esi
	leaq	(%rdi,%rdi,2), %rdx
	leaq	(%r10,%rax,4), %rax
	leaq	(%r10,%rdx,4), %rdx
	movq	16(%rdx), %rdi
	movl	24(%rdx), %edx
	movq	%rdi, 16(%rax)
	movl	%edx, 24(%rax)
	je	.L291
.L224:
	movl	-72(%rbp), %eax
	addl	%r13d, %eax
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L282:
	addl	$1, %r14d
	andl	$-2, %r14d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L287:
	movq	-80(%rbp), %rsi
	movq	%r13, %rdx
	movl	%eax, %edi
	movl	%ecx, -104(%rbp)
	call	write@PLT
	cmpq	%r13, %rax
	movl	-104(%rbp), %ecx
	jne	.L220
	movl	opt_format(%rip), %eax
	testl	%eax, %eax
	je	.L202
	cmpl	$2, %eax
	je	.L200
	movq	-72(%rbp), %rsi
	movq	%rsp, %r13
	movl	%ecx, -104(%rbp)
	leaq	15(%rsi), %rax
	movq	%rsi, %rdx
	xorl	%esi, %esi
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memset@PLT
	movq	-72(%rbp), %rdx
	movq	%rsp, %rsi
	movl	%r12d, %edi
	call	write@PLT
	cmpq	%rax, -72(%rbp)
	movl	-104(%rbp), %ecx
	jne	.L220
	movq	%r13, %rsp
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L233:
	movq	$0, -72(%rbp)
	xorl	%r13d, %r13d
	movq	$0, -80(%rbp)
.L182:
	movslq	%ebx, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	48(,%rax,8), %r12
	movq	%r12, %rdi
	movq	%r12, -96(%rbp)
	call	xmalloc@PLT
	pxor	%xmm0, %xmm0
	movl	$11825, %edx
	movq	%rax, %r15
	movl	$0, 44(%rax)
	movb	$101, 16(%rax)
	movw	%dx, 17(%rax)
	movb	$49, 19(%rax)
	movl	$0, %ecx
	movups	%xmm0, 28(%rax)
	movl	%ebx, 20(%rax)
	movl	opt_format(%rip), %esi
	movdqa	.LC40(%rip), %xmm0
	testl	%esi, %esi
	cmovne	%r12d, %ecx
	movups	%xmm0, (%rax)
	movq	-56(%rbp), %rax
	movb	$2, 28(%r15)
	movl	%eax, 24(%r15)
	movq	entries(%rip), %rax
	testq	%rax, %rax
	jne	.L292
.L235:
	movl	-56(%rbp), %eax
	xorl	%edi, %edi
	cmpl	$2, %esi
	leal	3(%rax), %ebx
	jne	.L293
	.p2align 4,,10
	.p2align 3
.L236:
	xorl	%eax, %eax
.L196:
	addl	-96(%rbp), %ebx
	addl	%eax, %ebx
	andl	$-4, %ebx
	movl	%ebx, 32(%r15)
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L214:
	movl	-72(%rbp), %eax
	addl	$40, %ebx
	movl	$40, %edi
	leal	(%rax,%rbx), %edx
	movl	%edx, -96(%rbp)
	call	xmalloc@PLT
	movl	-96(%rbp), %edx
	movabsq	$4294967344, %rsi
	movl	%ebx, 32(%rax)
	movq	%rax, %rcx
	movl	$-358342284, (%rax)
	movq	$0, 8(%rax)
	movq	%rsi, 20(%rax)
	movl	$0, 28(%rax)
	movl	$40, %ebx
	movl	%edx, 16(%rax)
	movl	-72(%rbp), %eax
	movl	%eax, 36(%rcx)
	movl	$2, %eax
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L173:
	testq	%rbx, %rbx
	jne	.L227
.L171:
	leaq	assign_glibc_hwcaps_indices_compare(%rip), %rcx
	movl	$8, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	qsort@PLT
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L281:
	xorl	%edi, %edi
	call	xmalloc@PLT
	movq	hwcaps(%rip), %rdx
	movq	%rax, %r12
	testq	%rdx, %rdx
	jne	.L170
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L208:
	xorl	%edi, %edi
	movl	%ecx, -72(%rbp)
	call	xmalloc@PLT
	movq	%rax, %r13
	movq	hwcaps(%rip), %rax
	xorl	%r9d, %r9d
	movl	-72(%rbp), %ecx
	movq	$0, -104(%rbp)
	movl	$0, -72(%rbp)
	testq	%rax, %rax
	jne	.L213
	jmp	.L229
.L220:
	leaq	.LC34(%rip), %rsi
	movl	$5, %edx
.L280:
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %esi
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L286:
	leaq	.LC33(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %esi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L227:
	leaq	__PRETTY_FUNCTION__.10051(%rip), %rcx
	leaq	.LC29(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$131, %edx
	call	__assert_fail@PLT
.L290:
	leaq	.LC39(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movq	-88(%rbp), %r8
	movl	(%rax), %esi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L289:
	leaq	.LC38(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %esi
	movl	$420, %r8d
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L217:
	movl	$5, %edx
	leaq	.LC36(%rip), %rsi
	jmp	.L280
.L284:
	leaq	.LC31(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	leaq	16(%r12), %rcx
	movq	%rax, %rdx
	movl	$1023, %r9d
	movl	%ebx, %r8d
	xorl	%esi, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L288:
	leaq	__PRETTY_FUNCTION__.10197(%rip), %rcx
	leaq	.LC29(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$758, %edx
	call	__assert_fail@PLT
	.size	save_cache, .-save_cache
	.section	.rodata.str1.1
.LC41:
	.string	"%s/%s"
.LC42:
	.string	"Could not create library path"
.LC43:
	.string	"hwcap == 0"
	.text
	.p2align 4,,15
	.globl	add_to_cache
	.type	add_to_cache, @function
add_to_cache:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	$64, %edi
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %r13d
	movl	%r8d, %r12d
	movl	%r9d, %ebp
	subq	$40, %rsp
	movq	%rsi, 8(%rsp)
	movq	%rdx, (%rsp)
	movq	96(%rsp), %rbx
	call	xmalloc@PLT
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdi
	movq	%rax, %r14
	movq	%r15, %rdx
	xorl	%eax, %eax
	movq	%rsi, %rcx
	leaq	.LC41(%rip), %rsi
	call	asprintf@PLT
	testl	%eax, %eax
	js	.L331
	movq	24(%rsp), %rsi
	leaq	strings(%rip), %rdi
	call	stringtable_add@PLT
	movq	24(%rsp), %rdi
	movq	%rax, %r15
	call	free@PLT
	movq	(%rsp), %rsi
	leaq	strings(%rip), %rdi
	call	stringtable_add@PLT
	movq	104(%rsp), %rcx
	movq	%rax, (%r14)
	movq	%r15, 8(%r14)
	movl	%r13d, 16(%r14)
	movl	%r12d, 20(%r14)
	movl	%ebp, 24(%r14)
	testq	%rcx, %rcx
	movq	%rbx, 32(%r14)
	movq	%rcx, 48(%r14)
	movl	$0, 40(%r14)
	je	.L296
	testq	%rbx, %rbx
	jne	.L332
	movq	104(%rsp), %rsi
	movb	$1, 20(%rsi)
.L298:
	movq	entries(%rip), %rbp
	testq	%rbp, %rbp
	je	.L313
	movq	%rbp, %rbx
	.p2align 4,,10
	.p2align 3
.L310:
	movq	(%rbx), %rdi
	leaq	16(%rdi), %rsi
	leaq	16(%rax), %rdi
	call	_dl_cache_libcmp@PLT
	testl	%eax, %eax
	jne	.L304
	movl	16(%r14), %eax
	cmpl	%eax, 16(%rbx)
	jl	.L305
	jg	.L306
	movq	48(%rbx), %rax
	movq	48(%r14), %rdx
	testq	%rax, %rax
	je	.L307
	testq	%rdx, %rdx
	je	.L306
	movq	8(%rax), %rdi
	movq	8(%rdx), %rsi
	addq	$16, %rdi
	addq	$16, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L304
.L309:
	movl	40(%rbx), %eax
	cmpl	%eax, 40(%r14)
	jg	.L305
	jl	.L306
	movq	32(%rbx), %rax
	cmpq	%rax, 32(%r14)
	ja	.L305
	jb	.L306
	movl	20(%rbx), %eax
	cmpl	%eax, 20(%r14)
	ja	.L305
	.p2align 4,,10
	.p2align 3
.L306:
	movq	56(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L314
	movq	%rbx, %rbp
	movq	(%r14), %rax
	movq	%rdx, %rbx
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L314:
	movq	%rbx, %rbp
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L305:
	cmpq	%rbx, entries(%rip)
	je	.L303
.L311:
	movq	56(%rbp), %rax
	movq	%rax, 56(%r14)
	movq	%r14, 56(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L304:
	testl	%eax, %eax
	jle	.L306
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L307:
	testq	%rdx, %rdx
	je	.L309
	cmpq	%rbx, entries(%rip)
	jne	.L311
.L303:
	movq	%rbx, 56(%r14)
	movq	%r14, entries(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L296:
	testq	%rbx, %rbx
	je	.L298
	xorl	%ecx, %ecx
	movl	$1, %esi
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L333:
	cmpq	$64, %rcx
	je	.L298
.L301:
	btq	%rcx, %rbx
	jnc	.L300
	addl	$1, 40(%r14)
.L300:
	addq	$1, %rcx
	movq	%rsi, %rdx
	salq	%cl, %rdx
	negq	%rdx
	testq	%rbx, %rdx
	jne	.L333
	jmp	.L298
.L313:
	xorl	%ebx, %ebx
	jmp	.L303
.L332:
	leaq	__PRETTY_FUNCTION__.10214(%rip), %rcx
	leaq	.LC29(%rip), %rsi
	leaq	.LC43(%rip), %rdi
	movl	$820, %edx
	call	__assert_fail@PLT
.L331:
	leaq	.LC42(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movq	%rax, %rbx
	call	__errno_location@PLT
	movl	(%rax), %esi
	movq	%rbx, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.size	add_to_cache, .-add_to_cache
	.p2align 4,,15
	.globl	init_aux_cache
	.type	init_aux_cache, @function
init_aux_cache:
	subq	$8, %rsp
	movl	$8, %esi
	movl	$8191, %edi
	movq	$8191, aux_hash_size(%rip)
	call	xcalloc@PLT
	movq	%rax, aux_hash(%rip)
	addq	$8, %rsp
	ret
	.size	init_aux_cache, .-init_aux_cache
	.p2align 4,,15
	.globl	search_aux_cache
	.type	search_aux_cache, @function
search_aux_cache:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$8, %rsp
	movq	8(%rdi), %r9
	movq	104(%rdi), %r10
	movq	48(%rdi), %r11
	movq	(%rdi), %rdi
	leaq	(%r9,%r9,4), %rax
	leaq	(%r9,%rax,2), %rax
	addq	%r10, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	addq	%r11, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rax,%rdx,2), %rax
	leaq	(%rax,%rdi), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	xorl	%edx, %eax
	xorl	%edx, %edx
	divq	aux_hash_size(%rip)
	movq	aux_hash(%rip), %rax
	movq	(%rax,%rdx,8), %rbx
	testq	%rbx, %rbx
	jne	.L341
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L338:
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L342
.L341:
	cmpq	(%rbx), %r9
	jne	.L338
	cmpq	8(%rbx), %r10
	jne	.L338
	cmpq	16(%rbx), %r11
	jne	.L338
	cmpq	24(%rbx), %rdi
	jne	.L338
	movl	32(%rbx), %eax
	movq	48(%rbx), %rdi
	movl	%eax, (%rsi)
	movl	36(%rbx), %eax
	testq	%rdi, %rdi
	movl	%eax, 0(%rbp)
	movl	40(%rbx), %eax
	movl	%eax, (%rcx)
	je	.L339
	movq	%r8, %rbp
	call	xstrdup@PLT
	movq	%rax, 0(%rbp)
.L340:
	movl	$1, 44(%rbx)
	movl	$1, %eax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%eax, %eax
.L336:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L339:
	movq	$0, (%r8)
	jmp	.L340
	.size	search_aux_cache, .-search_aux_cache
	.p2align 4,,15
	.globl	add_to_aux_cache
	.type	add_to_aux_cache, @function
add_to_aux_cache:
	subq	$40, %rsp
	movq	8(%rdi), %rax
	movl	$1, %r9d
	movq	%rax, (%rsp)
	movq	104(%rdi), %rax
	movq	%rax, 8(%rsp)
	movq	48(%rdi), %rax
	movq	%rax, 16(%rsp)
	movq	(%rdi), %rax
	movq	%rsp, %rdi
	movq	%rax, 24(%rsp)
	call	insert_to_aux_cache
	addq	$40, %rsp
	ret
	.size	add_to_aux_cache, .-add_to_aux_cache
	.section	.rodata.str1.1
.LC44:
	.string	"glibc-ld.so.auxcache-1.0"
	.text
	.p2align 4,,15
	.globl	load_aux_cache
	.type	load_aux_cache, @function
load_aux_cache:
	pushq	%r14
	pushq	%r13
	xorl	%esi, %esi
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	subq	$144, %rsp
	call	open@PLT
	testl	%eax, %eax
	js	.L372
	movq	%rsp, %rsi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	__fstat64@PLT
	testl	%eax, %eax
	js	.L352
	movq	48(%rsp), %rbp
	cmpq	$31, %rbp
	ja	.L351
.L352:
	movl	%ebx, %edi
	call	close@PLT
.L372:
	movl	$8, %esi
	movl	$8191, %edi
	movq	$8191, aux_hash_size(%rip)
	call	xcalloc@PLT
	movq	%rax, aux_hash(%rip)
.L347:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L351:
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%ebx, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	call	mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, %r12
	je	.L352
	movabsq	$7167607867211477806, %rdx
	xorq	8(%rax), %rdx
	movabsq	$7236208606153632871, %rax
	xorq	(%r12), %rax
	orq	%rax, %rdx
	jne	.L352
	movabsq	$3471766433793860449, %rax
	cmpq	%rax, 16(%r12)
	jne	.L352
	movl	24(%r12), %edx
	movl	28(%r12), %ecx
	leaq	(%rdx,%rdx,2), %rax
	salq	$4, %rax
	leaq	32(%rcx,%rax), %rax
	cmpq	%rax, %rbp
	jne	.L352
	leaq	4+primes(%rip), %rax
	movl	$1021, %edi
	leaq	84(%rax), %rcx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L373:
	cmpq	%rcx, %rax
	je	.L361
	movl	(%rax), %edi
	addq	$4, %rax
.L355:
	cmpq	%rdi, %rdx
	ja	.L373
.L356:
	movl	$8, %esi
	movq	%rdi, aux_hash_size(%rip)
	call	xcalloc@PLT
	movl	24(%r12), %edx
	movq	%rax, aux_hash(%rip)
	leaq	(%rdx,%rdx,2), %r13
	salq	$4, %r13
	addq	$32, %r13
	testl	%edx, %edx
	je	.L357
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L359:
	movl	%r14d, %eax
	xorl	%r8d, %r8d
	leaq	(%rax,%rax,2), %rdx
	salq	$4, %rdx
	movl	68(%r12,%rdx), %edx
	testl	%edx, %edx
	je	.L358
	leaq	(%rdx,%r13), %r8
	addq	%r12, %r8
.L358:
	leaq	(%rax,%rax,2), %rax
	xorl	%r9d, %r9d
	addl	$1, %r14d
	salq	$4, %rax
	leaq	(%r12,%rax), %rsi
	leaq	32(%r12,%rax), %rdi
	movl	76(%rsi), %ecx
	movl	72(%rsi), %edx
	movl	64(%rsi), %esi
	call	insert_to_aux_cache
	cmpl	%r14d, 24(%r12)
	ja	.L359
.L357:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	call	munmap@PLT
	movl	%ebx, %edi
	call	close@PLT
	jmp	.L347
.L361:
	movq	%rdx, %rdi
	jmp	.L356
	.size	load_aux_cache, .-load_aux_cache
	.p2align 4,,15
	.globl	save_aux_cache
	.type	save_aux_cache, @function
save_aux_cache:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$200, %rsp
	movq	aux_hash_size(%rip), %rax
	movq	%rdi, -232(%rbp)
	testq	%rax, %rax
	je	.L394
	movq	aux_hash(%rip), %r12
	xorl	%r15d, %r15d
	movl	$1, %r13d
	leaq	(%r12,%rax,8), %r14
	.p2align 4,,10
	.p2align 3
.L379:
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.L376
	.p2align 4,,10
	.p2align 3
.L378:
	movl	44(%rbx), %edx
	testl	%edx, %edx
	je	.L377
	movq	48(%rbx), %rdi
	addl	$1, %r15d
	testq	%rdi, %rdi
	je	.L377
	call	strlen@PLT
	leaq	1(%r13,%rax), %r13
.L377:
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L378
.L376:
	addq	$8, %r12
	cmpq	%r12, %r14
	jne	.L379
	movslq	%r15d, %rax
	movl	%r13d, %ebx
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	leaq	32(%rax), %r12
	leaq	0(%r13,%r12), %rax
	movq	%rax, -224(%rbp)
.L375:
	movq	-224(%rbp), %rdi
	call	xmalloc@PLT
	movdqa	.LC45(%rip), %xmm0
	movq	%rax, %r14
	cmpq	$0, aux_hash_size(%rip)
	movups	%xmm0, (%rax)
	movabsq	$3471766433793860449, %rax
	movl	%r15d, 24(%r14)
	movl	%ebx, 28(%r14)
	movq	%rax, 16(%r14)
	leaq	(%r14,%r12), %rax
	leaq	1(%rax), %rcx
	movb	$0, (%rax)
	movq	%rcx, -200(%rbp)
	je	.L380
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L386:
	movq	aux_hash(%rip), %rax
	movq	(%rax,%r12,8), %rbx
	testq	%rbx, %rbx
	jne	.L385
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L383:
	movq	%rsi, %rdi
	movl	%r13d, 68(%rax)
	movq	%rsi, -216(%rbp)
	call	strlen@PLT
	movq	-216(%rbp), %rsi
	leaq	1(%rax), %r10
	movq	-200(%rbp), %rdi
	movq	%r10, %rdx
	movq	%r10, -208(%rbp)
	call	__mempcpy@PLT
	movq	-208(%rbp), %r10
	movq	%rax, -200(%rbp)
	addl	%r10d, %r13d
.L384:
	leaq	(%r15,%r15,2), %rax
	movl	36(%rbx), %edx
	addq	$1, %r15
	salq	$4, %rax
	addq	%r14, %rax
	movl	%edx, 72(%rax)
	movl	40(%rbx), %edx
	movl	%edx, 76(%rax)
.L382:
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L381
.L385:
	movl	44(%rbx), %eax
	testl	%eax, %eax
	je	.L382
	leaq	(%r15,%r15,2), %rax
	movdqu	(%rbx), %xmm0
	movq	48(%rbx), %rsi
	movl	32(%rbx), %edx
	salq	$4, %rax
	addq	%r14, %rax
	movups	%xmm0, 32(%rax)
	testq	%rsi, %rsi
	movdqu	16(%rbx), %xmm0
	movl	%edx, 64(%rax)
	movups	%xmm0, 48(%rax)
	jne	.L383
	movl	$0, 68(%rax)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L381:
	addq	$1, %r12
	cmpq	%r12, aux_hash_size(%rip)
	ja	.L386
.L380:
	movq	-232(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	2(%rax), %rdi
	call	xmalloc@PLT
	leaq	.LC32(%rip), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %rbx
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	addq	$31, %rax
	movq	%r15, %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	call	memcpy@PLT
	movq	%rax, %rdi
	call	dirname@PLT
	leaq	-192(%rbp), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	stat64@PLT
	testl	%eax, %eax
	js	.L387
.L390:
	xorl	%eax, %eax
	movl	$384, %edx
	movl	$131649, %esi
	movq	%rbx, %rdi
	call	open@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L391
	movq	-224(%rbp), %r15
	movq	%r14, %rsi
	movl	%eax, %edi
	movq	%r15, %rdx
	call	write@PLT
	cmpq	%r15, %rax
	je	.L392
.L413:
	movq	%rbx, %rdi
	call	unlink@PLT
.L391:
	movq	%rbx, %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	free@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	movl	$448, %esi
	movq	%r12, %rdi
	call	mkdir@PLT
	testl	%eax, %eax
	jns	.L390
	jmp	.L391
.L392:
	movl	%r12d, %edi
	call	fdatasync@PLT
	testl	%eax, %eax
	jne	.L413
	movl	%r12d, %edi
	call	close@PLT
	testl	%eax, %eax
	jne	.L413
	movq	-232(%rbp), %rsi
	movq	%rbx, %rdi
	call	rename@PLT
	testl	%eax, %eax
	je	.L391
	jmp	.L413
	.p2align 4,,10
	.p2align 3
.L394:
	movl	$32, %r12d
	movl	$1, %ebx
	xorl	%r15d, %r15d
	movq	$33, -224(%rbp)
	jmp	.L375
	.size	save_aux_cache, .-save_aux_cache
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10214, @object
	.size	__PRETTY_FUNCTION__.10214, 13
__PRETTY_FUNCTION__.10214:
	.string	"add_to_cache"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10051, @object
	.size	__PRETTY_FUNCTION__.10051, 28
__PRETTY_FUNCTION__.10051:
	.string	"assign_glibc_hwcaps_indices"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10197, @object
	.size	__PRETTY_FUNCTION__.10197, 11
__PRETTY_FUNCTION__.10197:
	.string	"save_cache"
	.section	.rodata.str1.1
.LC46:
	.string	"Linux"
.LC47:
	.string	"Hurd"
.LC48:
	.string	"Solaris"
.LC49:
	.string	"FreeBSD"
.LC50:
	.string	"kNetBSD"
.LC51:
	.string	"Syllable"
.LC52:
	.string	"Unknown OS"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	abi_tag_os.10101, @object
	.size	abi_tag_os.10101, 56
abi_tag_os.10101:
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.local	aux_hash
	.comm	aux_hash,8,8
	.local	aux_hash_size
	.comm	aux_hash_size,8,8
	.section	.rodata
	.align 32
	.type	primes, @object
	.size	primes, 88
primes:
	.long	1021
	.long	2039
	.long	4093
	.long	8191
	.long	16381
	.long	32749
	.long	65521
	.long	131071
	.long	262139
	.long	524287
	.long	1048573
	.long	2097143
	.long	4194301
	.long	8388593
	.long	16777213
	.long	33554393
	.long	67108859
	.long	134217689
	.long	268435399
	.long	536870909
	.long	1073741789
	.long	2147483647
	.section	.rodata.str1.1
.LC53:
	.string	"libc4"
.LC54:
	.string	"ELF"
.LC55:
	.string	"libc5"
.LC56:
	.string	"libc6"
	.section	.data.rel.ro.local
	.align 32
	.type	flag_descr, @object
	.size	flag_descr, 32
flag_descr:
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.local	entries
	.comm	entries,8,8
	.local	hwcaps
	.comm	hwcaps,8,8
	.local	strings
	.comm	strings,16,16
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC40:
	.quad	7236208606153632871
	.quad	7521962881247572782
	.align 16
.LC45:
	.quad	7236208606153632871
	.quad	7167607867211477806
