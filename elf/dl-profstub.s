	.text
	.p2align 4,,15
	.globl	_dl_mcount_wrapper
	.type	_dl_mcount_wrapper, @function
_dl_mcount_wrapper:
	movq	%rdi, %rsi
	movq	(%rsp), %rdi
	jmp	_dl_mcount@PLT
	.size	_dl_mcount_wrapper, .-_dl_mcount_wrapper
	.p2align 4,,15
	.globl	_dl_mcount_wrapper_check
	.hidden	_dl_mcount_wrapper_check
	.type	_dl_mcount_wrapper_check, @function
_dl_mcount_wrapper_check:
	cmpq	$0, _dl_profile_map(%rip)
	je	.L3
	movq	%rdi, %rsi
	movq	(%rsp), %rdi
	jmp	_dl_mcount@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	rep ret
	.size	_dl_mcount_wrapper_check, .-_dl_mcount_wrapper_check
