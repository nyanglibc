	.text
	.p2align 4,,15
	.type	do_dlopen, @function
do_dlopen:
	pushq	%rbx
	movq	__libc_argv(%rip), %r9
	movq	%rdi, %rbx
	movq	16(%rdi), %rdx
	movl	8(%rdi), %esi
	movq	$-2, %rcx
	movl	__libc_argc(%rip), %r8d
	movq	(%rdi), %rdi
	subq	$8, %rsp
	pushq	__environ(%rip)
	call	_dl_open
	movq	%rax, 24(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	do_dlopen, .-do_dlopen
	.p2align 4,,15
	.type	dlerror_run, @function
dlerror_run:
	subq	$56, %rsp
	movq	%rdi, %rcx
	movq	%rsi, %r8
	leaq	31(%rsp), %rdx
	leaq	40(%rsp), %rsi
	leaq	32(%rsp), %rdi
	movq	$0, 40(%rsp)
	call	_dl_catch_error
	testl	%eax, %eax
	jne	.L5
	cmpq	$0, 40(%rsp)
	jne	.L8
.L4:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L5:
	cmpb	$0, 31(%rsp)
	je	.L4
	movq	40(%rsp), %rdi
	movl	%eax, 12(%rsp)
	call	free@PLT
	movl	12(%rsp), %eax
	addq	$56, %rsp
	ret
	.size	dlerror_run, .-dlerror_run
	.p2align 4,,15
	.globl	__libc_dlclose
	.hidden	__libc_dlclose
	.type	__libc_dlclose, @function
__libc_dlclose:
	movq	%rdi, %rsi
	leaq	do_dlclose(%rip), %rdi
	jmp	dlerror_run
	.size	__libc_dlclose, .-__libc_dlclose
	.p2align 4,,15
	.globl	__libc_dlsym
	.hidden	__libc_dlsym
	.type	__libc_dlsym, @function
__libc_dlsym:
	subq	$40, %rsp
	movq	%rdi, (%rsp)
	leaq	do_dlsym(%rip), %rdi
	movq	%rsi, 8(%rsp)
	movq	%rsp, %rsi
	call	dlerror_run
	testl	%eax, %eax
	jne	.L14
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L14
	cmpw	$-15, 6(%rdx)
	je	.L15
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L12
	movq	(%rcx), %rax
.L12:
	addq	8(%rdx), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	jmp	.L12
	.size	__libc_dlsym, .-__libc_dlsym
	.p2align 4,,15
	.globl	__libc_dlvsym
	.hidden	__libc_dlvsym
	.type	__libc_dlvsym, @function
__libc_dlvsym:
	subq	$72, %rsp
	xorl	%eax, %eax
	movq	%rsi, 8(%rsp)
	movzbl	(%rdx), %esi
	movq	%rdi, (%rsp)
	movq	%rdx, 32(%rsp)
	movl	$1, 44(%rsp)
	testq	%rsi, %rsi
	je	.L20
	movzbl	1(%rdx), %ecx
	testb	%cl, %cl
	je	.L41
	movzbl	%cl, %eax
	movzbl	2(%rdx), %ecx
	salq	$4, %rsi
	addq	%rsi, %rax
	testb	%cl, %cl
	je	.L20
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	3(%rdx), %ecx
	testb	%cl, %cl
	je	.L20
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	4(%rdx), %ecx
	testb	%cl, %cl
	je	.L20
	salq	$4, %rax
	addq	%rcx, %rax
	leaq	5(%rdx), %rcx
	movzbl	5(%rdx), %edx
	testb	%dl, %dl
	je	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	salq	$4, %rax
	addq	$1, %rcx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$240, %edx
	xorq	%rdx, %rax
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L26
.L25:
	andl	$268435455, %eax
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	do_dlvsym(%rip), %rdi
	movq	%rsp, %rsi
	movl	%eax, 40(%rsp)
	movq	$0, 48(%rsp)
	call	dlerror_run
	testl	%eax, %eax
	jne	.L31
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L31
	cmpw	$-15, 6(%rdx)
	je	.L32
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L28
	movq	(%rcx), %rax
.L28:
	addq	8(%rdx), %rax
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movzbl	%sil, %eax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%eax, %eax
	jmp	.L28
	.size	__libc_dlvsym, .-__libc_dlvsym
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"GLIBC_PRIVATE"
	.text
	.p2align 4,,15
	.type	do_dlsym_private, @function
do_dlsym_private:
	pushq	%rbx
	leaq	.LC0(%rip), %rax
	movq	%rdi, %rbx
	leaq	24(%rdi), %rdx
	xorl	%r9d, %r9d
	subq	$32, %rsp
	movq	(%rdi), %rsi
	movq	$0, 24(%rdi)
	movq	%rax, (%rsp)
	movabsq	$4452503429, %rax
	movq	$0, 16(%rsp)
	movq	%rax, 8(%rsp)
	movq	8(%rdi), %rdi
	movq	920(%rsi), %rcx
	pushq	$0
	pushq	$0
	leaq	16(%rsp), %r8
	call	_dl_lookup_symbol_x
	movq	%rax, 16(%rbx)
	addq	$48, %rsp
	popq	%rbx
	ret
	.size	do_dlsym_private, .-do_dlsym_private
	.p2align 4,,15
	.type	do_dlsym, @function
do_dlsym:
	pushq	%rbx
	movq	(%rdi), %rsi
	movq	%rdi, %rbx
	movq	$0, 24(%rdi)
	leaq	24(%rdi), %rdx
	movq	8(%rdi), %rdi
	pushq	$0
	pushq	$2
	xorl	%r9d, %r9d
	leaq	928(%rsi), %rcx
	xorl	%r8d, %r8d
	call	_dl_lookup_symbol_x
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	do_dlsym, .-do_dlsym
	.p2align 4,,15
	.type	do_dlvsym, @function
do_dlvsym:
	pushq	%rbx
	movq	(%rdi), %rsi
	movq	%rdi, %rbx
	movq	$0, 24(%rdi)
	leaq	24(%rdi), %rdx
	movq	8(%rdi), %rdi
	leaq	32(%rbx), %r8
	pushq	$0
	pushq	$0
	leaq	928(%rsi), %rcx
	xorl	%r9d, %r9d
	call	_dl_lookup_symbol_x
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	do_dlvsym, .-do_dlvsym
	.p2align 4,,15
	.type	do_dlclose, @function
do_dlclose:
	jmp	_dl_close
	.size	do_dlclose, .-do_dlclose
	.p2align 4,,15
	.globl	__libc_dlsym_private
	.hidden	__libc_dlsym_private
	.type	__libc_dlsym_private, @function
__libc_dlsym_private:
	subq	$40, %rsp
	movq	%rdi, (%rsp)
	leaq	do_dlsym_private(%rip), %rdi
	movq	%rsi, 8(%rsp)
	movq	%rsp, %rsi
	call	dlerror_run
	testl	%eax, %eax
	jne	.L53
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L53
	cmpw	$-15, 6(%rdx)
	je	.L54
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L51
	movq	(%rcx), %rax
.L51:
	addq	8(%rdx), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%eax, %eax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	xorl	%eax, %eax
	jmp	.L51
	.size	__libc_dlsym_private, .-__libc_dlsym_private
	.section	.rodata.str1.1
.LC1:
	.string	"_dl_open_hook"
.LC2:
	.string	"_dl_open_hook2"
	.text
	.p2align 4,,15
	.globl	__libc_register_dl_open_hook
	.hidden	__libc_register_dl_open_hook
	.type	__libc_register_dl_open_hook, @function
__libc_register_dl_open_hook:
	pushq	%rbx
	leaq	.LC1(%rip), %rsi
	movq	%rdi, %rbx
	call	__libc_dlsym_private
	testq	%rax, %rax
	je	.L59
	leaq	_dl_open_hook(%rip), %rcx
	movq	%rcx, (%rax)
.L59:
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	__libc_dlsym_private
	testq	%rax, %rax
	je	.L58
	leaq	_dl_open_hook(%rip), %rdx
	movq	%rdx, (%rax)
.L58:
	popq	%rbx
	ret
	.size	__libc_register_dl_open_hook, .-__libc_register_dl_open_hook
	.p2align 4,,15
	.globl	__libc_dlopen_mode
	.hidden	__libc_dlopen_mode
	.type	__libc_dlopen_mode, @function
__libc_dlopen_mode:
	subq	$40, %rsp
	movq	40(%rsp), %rax
	movq	%rdi, (%rsp)
	leaq	do_dlopen(%rip), %rdi
	movl	%esi, 8(%rsp)
	movq	%rsp, %rsi
	movq	%rax, 16(%rsp)
	call	dlerror_run
	testl	%eax, %eax
	jne	.L70
	movq	24(%rsp), %rdi
	call	__libc_register_dl_open_hook
	movq	24(%rsp), %rdi
	call	__libc_register_dlfcn_hook
	movq	24(%rsp), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%eax, %eax
	addq	$40, %rsp
	ret
	.size	__libc_dlopen_mode, .-__libc_dlopen_mode
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_slotinfo, @function
free_slotinfo:
	movq	(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L91
	rep ret
	.p2align 4,,10
	.p2align 3
.L91:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	8(%rdx), %rdi
	subq	$16, %rsp
	call	free_slotinfo
	testb	%al, %al
	je	.L72
	movq	(%rbx), %rdi
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L74
	cmpq	$0, 24(%rdi)
	jne	.L79
	leaq	40(%rdi), %rcx
	xorl	%edx, %edx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L76:
	addq	$16, %rcx
	cmpq	$0, -16(%rcx)
	jne	.L79
.L75:
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jne	.L76
.L74:
	movb	%al, 15(%rsp)
	call	free@PLT
	movzbl	15(%rsp), %eax
	movq	$0, (%rbx)
.L72:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%eax, %eax
	jmp	.L72
	.size	free_slotinfo, .-free_slotinfo
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	_dl_all_dirs(%rip), %rdi
	cmpq	_dl_init_all_dirs(%rip), %rdi
	je	.L93
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rdi), %rbx
	call	free@PLT
	cmpq	%rbx, _dl_init_all_dirs(%rip)
	movq	%rbx, %rdi
	jne	.L94
.L93:
	cmpq	$0, _dl_nns(%rip)
	je	.L95
	movq	_dl_ns(%rip), %rbp
	testq	%rbp, %rbp
	je	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	movq	56(%rbp), %rax
	movq	8(%rax), %rdi
	movq	$0, 8(%rax)
	testq	%rdi, %rdi
	jne	.L99
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	je	.L97
.L99:
	movl	16(%rdi), %edx
	movq	8(%rdi), %rbx
	testl	%edx, %edx
	jne	.L98
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L99
.L97:
	testb	$1, 798(%rbp)
	jne	.L116
.L100:
	movq	$0, 976(%rbp)
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.L101
.L96:
	movl	24+_dl_ns(%rip), %eax
	testl	%eax, %eax
	jne	.L117
.L95:
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rax
	leaq	8(%rax), %rdi
	call	free_slotinfo
	movq	_dl_scope_free_list(%rip), %rdi
	movq	$0, _dl_scope_free_list(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L116:
	movq	976(%rbp), %rdi
	call	free@PLT
	jmp	.L100
.L117:
	movq	16+_dl_ns(%rip), %rax
	movl	8+_dl_initial_searchlist(%rip), %ecx
	cmpl	%ecx, 8(%rax)
	jne	.L95
	movq	_dl_initial_searchlist(%rip), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	movl	$0, 24+_dl_ns(%rip)
	call	free@PLT
	jmp	.L95
	.size	free_mem, .-free_mem
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	_dl_open_hook, @object
	.size	_dl_open_hook, 32
_dl_open_hook:
	.quad	__libc_dlopen_mode
	.quad	__libc_dlsym
	.quad	__libc_dlclose
	.quad	__libc_dlvsym
	.hidden	__libc_register_dlfcn_hook
	.hidden	_dl_close
	.hidden	_dl_lookup_symbol_x
	.hidden	_dl_catch_error
	.hidden	_dl_open
	.hidden	__libc_argc
	.hidden	__libc_argv
