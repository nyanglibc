	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal error: length accounting in _dl_exception_create_format\n"
	.text
	.p2align 4,,15
	.type	length_mismatch, @function
length_mismatch:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
	.size	length_mismatch, .-length_mismatch
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.text
	.p2align 4,,15
	.globl	_dl_exception_create
	.type	_dl_exception_create, @function
_dl_exception_create:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L8
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen
	leaq	1(%rax), %r12
.L5:
	movq	%r14, %rdi
	call	strlen
	leaq	1(%rax), %rbx
	leaq	(%rbx,%r12), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L10
	leaq	.LC1(%rip), %rax
	movq	$0, 16(%rbp)
	movq	%rax, 0(%rbp)
	leaq	_dl_out_of_memory(%rip), %rax
	movq	%rax, 8(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r15, 8(%rbp)
	movq	%rax, 0(%rbp)
	movq	%r15, 16(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %r12d
	leaq	.LC1(%rip), %r13
	jmp	.L5
	.size	_dl_exception_create, .-_dl_exception_create
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"Fatal error: invalid format in exception string\n"
	.text
	.p2align 4,,15
	.globl	_dl_exception_create_format
	.type	_dl_exception_create_format, @function
_dl_exception_create_format:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$104, %rsp
	testq	%rsi, %rsi
	movq	%rcx, 72(%rsp)
	movq	%r8, 80(%rsp)
	movq	%r9, 88(%rsp)
	je	.L12
	movq	%rsi, %rdi
	movq	%rsi, %r12
	call	strlen
	movzbl	(%r15), %edx
	leaq	1(%rax), %rsi
	leaq	2(%rax), %r14
	leaq	160(%rsp), %rax
	movl	$24, 24(%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	testb	%dl, %dl
	movq	%rax, 40(%rsp)
	je	.L13
.L48:
	movq	40(%rsp), %r13
	movq	%r15, %rbx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$1, %r14
	movq	%rcx, %rbx
	movl	%eax, %edx
.L22:
	testb	%dl, %dl
	je	.L13
.L23:
	cmpb	$37, %dl
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %rcx
	jne	.L14
	cmpb	$115, %al
	je	.L16
	jle	.L81
	cmpb	$120, %al
	je	.L19
	cmpb	$122, %al
	jne	.L15
.L18:
	cmpb	$120, 2(%rbx)
	je	.L82
.L19:
	movzbl	2(%rbx), %edx
	addq	$8, %r14
	addq	$2, %rbx
	testb	%dl, %dl
	jne	.L23
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%r14, %r14
	jns	.L24
.L26:
	leaq	.LC1(%rip), %rax
	movq	$0, 16(%rbp)
	movq	%rax, 0(%rbp)
	leaq	_dl_out_of_memory(%rip), %rax
	movq	%rax, 8(%rbp)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movzbl	(%rdx), %edx
	leaq	160(%rsp), %rax
	movl	$24, 24(%rsp)
	movl	$2, %r14d
	movq	$1, 8(%rsp)
	leaq	.LC1(%rip), %r12
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	testb	%dl, %dl
	movq	%rax, 40(%rsp)
	jne	.L48
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L26
	movq	%rax, 8(%rbp)
	movq	%rax, 16(%rbp)
	leaq	(%rax,%r14), %r13
	leaq	160(%rsp), %rax
	movl	$24, 24(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	movzbl	(%r15), %eax
	testb	%al, %al
	jne	.L47
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	cmpq	%rbx, %r13
	je	.L39
	movb	%al, (%rbx)
	movq	%r15, %r14
	addq	$1, %rbx
.L38:
	movzbl	1(%r14), %eax
	leaq	1(%r14), %r15
	testb	%al, %al
	je	.L27
.L47:
	cmpb	$37, %al
	jne	.L28
	movzbl	1(%r15), %eax
	leaq	1(%r15), %r14
	cmpb	$115, %al
	je	.L30
	jg	.L31
	cmpb	$37, %al
	jne	.L83
	cmpq	%rbx, %r13
	je	.L39
	movzbl	1(%r14), %eax
	movb	$37, (%rbx)
	addq	$1, %rbx
	leaq	1(%r14), %r15
	testb	%al, %al
	jne	.L47
	.p2align 4,,10
	.p2align 3
.L27:
	cmpq	%rbx, %r13
	je	.L39
	leaq	1(%rbx), %rdi
	movq	8(%rsp), %rax
	movq	%r13, %r8
	movb	$0, (%rbx)
	subq	%rdi, %r8
	cmpq	%rax, %r8
	jne	.L39
	movq	%r12, %rsi
	movq	%rax, %rdx
	call	memcpy@PLT
	movq	%rax, 0(%rbp)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpb	$120, %al
	jne	.L84
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L40
	movl	%edx, %eax
	addl	$8, %edx
	addq	40(%rsp), %rax
	movl	%edx, 24(%rsp)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L81:
	cmpb	$108, %al
	je	.L18
.L15:
	movzbl	2(%rbx), %edx
	addq	$1, %r14
	addq	$2, %rbx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L83:
	cmpb	$108, %al
	jne	.L29
.L33:
	cmpb	$120, 2(%r15)
	jne	.L29
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L43
	movl	%edx, %eax
	addq	40(%rsp), %rax
	addl	$8, %edx
	movl	%edx, 24(%rsp)
.L44:
	leaq	16(%rbx), %r9
	movq	(%rax), %rdi
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r9, %rsi
	movq	%r9, (%rsp)
	call	_itoa_word
	cmpq	%rbx, %rax
	movq	(%rsp), %r9
	je	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rbx, %rax
	jne	.L46
.L45:
	leaq	2(%r15), %r14
	movq	%r9, %rbx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L84:
	cmpb	$122, %al
	je	.L33
.L29:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
	.p2align 4,,10
	.p2align 3
.L82:
	movzbl	3(%rbx), %edx
	addq	$16, %r14
	addq	$3, %rbx
	jmp	.L22
.L39:
	call	length_mismatch
.L30:
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L35
	movl	%edx, %eax
	addq	40(%rsp), %rax
	addl	$8, %edx
	movl	%edx, 24(%rsp)
.L36:
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	strlen
	movq	%r13, %rdx
	subq	%rbx, %rdx
	cmpq	%rax, %rdx
	jb	.L39
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%r15, %rsi
	call	__mempcpy@PLT
	movq	%rax, %rbx
	jmp	.L38
.L43:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
	jmp	.L44
.L16:
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L20
	movl	%edx, %eax
	addl	$8, %edx
	addq	%r13, %rax
	movl	%edx, 24(%rsp)
.L21:
	movq	(%rax), %rdi
	addq	$2, %rbx
	call	strlen
	movzbl	(%rbx), %edx
	addq	%rax, %r14
	jmp	.L22
.L40:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
.L41:
	movl	(%rax), %edi
	leaq	8(%rbx), %r15
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r15, %rsi
	call	_itoa_word
	cmpq	%rbx, %rax
	je	.L50
	.p2align 4,,10
	.p2align 3
.L42:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rbx, %rax
	jne	.L42
.L50:
	movq	%r15, %rbx
	jmp	.L38
.L20:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
	jmp	.L21
.L35:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
	jmp	.L36
	.size	_dl_exception_create_format, .-_dl_exception_create_format
	.p2align 4,,15
	.globl	_dl_exception_free
	.type	_dl_exception_free, @function
_dl_exception_free:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	popq	%rbx
	ret
	.size	_dl_exception_free, .-_dl_exception_free
	.section	.rodata.str1.8
	.align 8
	.type	_dl_out_of_memory, @object
	.size	_dl_out_of_memory, 14
_dl_out_of_memory:
	.string	"out of memory"
	.hidden	_itoa_word
	.hidden	strlen
