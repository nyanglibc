	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-runtime.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ELFW(R_TYPE)(reloc->r_info) == ELF_MACHINE_JMP_SLOT"
	.text
	.p2align 4,,15
	.globl	_dl_fixup
	.hidden	_dl_fixup
	.type	_dl_fixup, @function
_dl_fixup:
.LFB81:
	pushq	%rbx
	movq	%rdi, %r10
	movl	%esi, %esi
	leaq	(%rsi,%rsi,2), %rdx
	subq	$16, %rsp
	movq	104(%rdi), %rax
	movq	8(%rax), %rdi
	movq	248(%r10), %rax
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,8), %r8
	movq	112(%r10), %rax
	movq	8(%r8), %rcx
	movq	(%r8), %rbx
	movq	8(%rax), %rax
	movq	%rcx, %rdx
	shrq	$32, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	movq	(%r10), %rax
	movq	%rsi, 8(%rsp)
	addq	%rax, %rbx
	cmpl	$7, %ecx
	jne	.L33
	testb	$3, 5(%rsi)
	jne	.L3
	movq	464(%r10), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L4
	movq	8(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	andl	$32767, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	744(%r10), %rax
	leaq	(%rax,%rdx,8), %r8
	movl	$0, %eax
	movl	8(%r8), %r9d
	testl	%r9d, %r9d
	cmove	%rax, %r8
.L4:
#APP
# 99 "dl-runtime.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	movl	$1, %eax
	jne	.L34
.L5:
	movl	(%rsi), %esi
	movq	920(%r10), %rcx
	leaq	8(%rsp), %rdx
	pushq	$0
	pushq	%rax
	movl	$1, %r9d
	addq	%rsi, %rdi
	movq	%r10, %rsi
	call	_dl_lookup_symbol_x
	movq	%rax, %r8
#APP
# 113 "dl-runtime.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%rcx
	popq	%rsi
	jne	.L35
.L6:
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L7
	cmpw	$-15, 6(%rsi)
	je	.L17
	testq	%r8, %r8
	je	.L17
	movq	(%r8), %rax
.L31:
	movzbl	4(%rsi), %edx
	addq	8(%rsi), %rax
	andl	$15, %edx
	cmpb	$10, %dl
	je	.L36
.L7:
	movl	_dl_bind_not(%rip), %edx
	testl	%edx, %edx
	jne	.L1
	movq	%rax, (%rbx)
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	call	*%rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L3:
	cmpw	$-15, 6(%rsi)
	movl	$0, %edx
	cmove	%rdx, %rax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
#APP
# 114 "dl-runtime.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	jne	.L6
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 114 "dl-runtime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
#APP
# 101 "dl-runtime.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	movl	$5, %eax
	jmp	.L5
.L33:
	leaq	__PRETTY_FUNCTION__.11059(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$77, %edx
	call	__assert_fail
.LFE81:
	.size	_dl_fixup, .-_dl_fixup
	.p2align 4,,15
	.globl	_dl_profile_fixup
	.type	_dl_profile_fixup, @function
_dl_profile_fixup:
.LFB82:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	subq	$24, %rsp
	movq	832(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L72
	movl	%esi, %esi
	movq	%rdx, %r12
	movq	%rsi, %rax
	salq	$5, %rax
	addq	%rax, %rbx
	movl	28(%rbx), %eax
	testl	%eax, %eax
	jne	.L40
	movq	248(%rdi), %rdx
	movq	104(%rdi), %rax
	movq	8(%rdx), %r8
	movq	8(%rax), %r9
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r8,%rax,8), %rax
	movq	8(%rax), %rcx
	movq	112(%rdi), %rax
	movq	%rcx, %rdx
	movq	8(%rax), %rax
	shrq	$32, %rdx
	cmpl	$7, %ecx
	leaq	(%rdx,%rdx,2), %rsi
	leaq	(%rax,%rsi,8), %rax
	movq	%rax, 8(%rsp)
	jne	.L73
	testb	$3, 5(%rax)
	jne	.L42
	movq	464(%rdi), %rcx
	xorl	%r8d, %r8d
	testq	%rcx, %rcx
	je	.L43
	movq	8(%rcx), %rcx
	movzwl	(%rcx,%rdx,2), %edx
	andl	$32767, %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	744(%rdi), %rdx
	leaq	(%rdx,%rcx,8), %r8
	movl	$0, %edx
	movl	8(%r8), %esi
	testl	%esi, %esi
	cmove	%rdx, %r8
.L43:
#APP
# 254 "dl-runtime.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	movl	$1, %esi
	jne	.L74
.L44:
	movl	(%rax), %eax
	movq	920(%rdi), %rcx
	leaq	8(%rsp), %rdx
	pushq	$0
	pushq	%rsi
	movq	%rdi, %rsi
	addq	%r9, %rax
	movl	$1, %r9d
	movq	%rax, %rdi
	call	_dl_lookup_symbol_x
	movq	%rax, %r8
#APP
# 265 "dl-runtime.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L75
.L45:
	movq	8(%rsp), %rax
	xorl	%r13d, %r13d
	testq	%rax, %rax
	je	.L46
	cmpw	$-15, 6(%rax)
	je	.L56
	testq	%r8, %r8
	je	.L56
	movq	(%r8), %r13
.L48:
	addq	8(%rax), %r13
	movzbl	4(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	je	.L76
.L46:
	movl	_dl_bind_not(%rip), %eax
	testl	%eax, %eax
	jne	.L50
	movq	%r13, (%rbx)
	movl	$1, 28(%rbx)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L40:
	movq	(%rbx), %r13
.L50:
	movq	$-1, 0(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_dl_mcount@PLT
	addq	$24, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	$-1, (%r8)
	call	_dl_fixup
	addq	$24, %rsp
	movq	%rax, %r13
	popq	%rbx
	movq	%r13, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%r13d, %r13d
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L76:
	call	*%r13
	movq	%rax, %r13
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L42:
	cmpw	$-15, 6(%rax)
	je	.L56
	movq	(%rdi), %r13
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%eax, %eax
#APP
# 266 "dl-runtime.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	jne	.L45
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 266 "dl-runtime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L74:
#APP
# 256 "dl-runtime.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	movl	$5, %esi
	jmp	.L44
.L73:
	leaq	__PRETTY_FUNCTION__.11121(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$232, %edx
	call	__assert_fail
.LFE82:
	.size	_dl_profile_fixup, .-_dl_profile_fixup
	.p2align 4,,15
	.globl	_dl_call_pltexit
	.type	_dl_call_pltexit, @function
_dl_call_pltexit:
.LFB83:
	rep ret
.LFE83:
	.size	_dl_call_pltexit, .-_dl_call_pltexit
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11121, @object
	.size	__PRETTY_FUNCTION__.11121, 18
__PRETTY_FUNCTION__.11121:
	.string	"_dl_profile_fixup"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11059, @object
	.size	__PRETTY_FUNCTION__.11059, 10
__PRETTY_FUNCTION__.11059:
	.string	"_dl_fixup"
	.hidden	__assert_fail
	.hidden	_dl_lookup_symbol_x
