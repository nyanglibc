	.text
	.p2align 4,,15
	.globl	_dl_scope_free
	.hidden	_dl_scope_free
	.type	_dl_scope_free, @function
_dl_scope_free:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
#APP
# 30 "dl-scope.c" 1
	movl %fs:24,%ebx
# 0 "" 2
#NO_APP
	testl	%ebx, %ebx
	jne	.L2
	call	free@PLT
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	_dl_scope_free_list(%rip), %rbx
	testq	%rbx, %rbx
	je	.L15
	movq	(%rbx), %rax
	cmpq	$49, %rax
	ja	.L6
	leaq	1(%rax), %rdx
	movq	%rdx, (%rbx)
	movq	%rdi, 8(%rbx,%rax,8)
	xorl	%ebx, %ebx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	call	__thread_gscope_wait
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	subq	$1, %rax
	movq	8(%rbx,%rax,8), %rdi
	movq	%rax, (%rbx)
	call	free@PLT
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L8
.L7:
	movl	$1, %ebx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$408, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, _dl_scope_free_list(%rip)
	je	.L16
	movq	%rbp, 8(%rax)
	movq	$1, (%rax)
	xorl	%ebx, %ebx
	jmp	.L1
.L16:
	call	__thread_gscope_wait
	movq	%rbp, %rdi
	movl	$1, %ebx
	call	free@PLT
	jmp	.L1
	.size	_dl_scope_free, .-_dl_scope_free
	.hidden	__thread_gscope_wait
