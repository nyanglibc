	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
.LC1:
	.string	"dl-init.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"l->l_real->l_relocated || l->l_real->l_type == lt_executable"
	.section	.rodata.str1.1
.LC3:
	.string	"\ncalling init: %s\n\n"
	.text
	.p2align 4,,15
	.type	call_init, @function
call_init:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	40(%rdi), %rax
	movq	%rdi, %rbx
	movl	%esi, %ebp
	movzbl	796(%rax), %eax
	testb	$4, %al
	jne	.L2
	testb	$3, %al
	jne	.L27
.L2:
	movzbl	796(%rbx), %eax
	testb	$8, %al
	jne	.L1
	movq	8(%rbx), %rsi
	orl	$8, %eax
	movb	%al, 796(%rbx)
	cmpb	$0, (%rsi)
	je	.L28
	testb	$2, _dl_debug_mask(%rip)
	jne	.L6
.L10:
	movq	160(%rbx), %rax
	testq	%rax, %rax
	je	.L7
	movq	8(%rax), %rax
	addq	(%rbx), %rax
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	*%rax
.L7:
	movq	264(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L1
	movq	280(%rbx), %rax
	movq	8(%rax), %rdx
	movq	(%rbx), %rax
	addq	8(%rcx), %rax
	shrq	$3, %rdx
	testl	%edx, %edx
	je	.L1
	leaq	8(%rax), %rbx
	subl	$1, %edx
	leaq	(%rbx,%rdx,8), %r14
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$8, %rbx
.L8:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	*(%rax)
	cmpq	%rbx, %r14
	movq	%rbx, %rax
	jne	.L29
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	testb	$3, %al
	je	.L1
	testb	$2, _dl_debug_mask(%rip)
	je	.L10
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L6:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L10
.L27:
	leaq	__PRETTY_FUNCTION__.9053(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$35, %edx
	call	__assert_fail
	.size	call_init, .-call_init
	.section	.rodata.str1.1
.LC4:
	.string	"\ncalling preinit: %s\n\n"
	.text
	.p2align 4,,15
	.globl	_dl_init
	.hidden	_dl_init
	.type	_dl_init, @function
_dl_init:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdx, %r12
	subq	$8, %rsp
	movq	320(%rdi), %r15
	movq	328(%rdi), %rbx
	movq	_dl_initfirst(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L56
.L31:
	testq	%rbx, %rbx
	je	.L32
	testq	%r15, %r15
	jne	.L57
.L32:
	movl	712(%r14), %eax
	testl	%eax, %eax
	leal	-1(%rax), %ebx
	je	.L30
	salq	$3, %rbx
	.p2align 4,,10
	.p2align 3
.L37:
	movq	976(%r14), %rax
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebp, %esi
	movq	(%rax,%rbx), %rdi
	subq	$8, %rbx
	call	call_init
	cmpq	$-8, %rbx
	jne	.L37
.L30:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movq	8(%rbx), %rbx
	shrq	$3, %rbx
	testl	%ebx, %ebx
	je	.L32
	testb	$2, _dl_debug_mask(%rip)
	jne	.L58
.L33:
	movq	(%r14), %rax
	addq	8(%r15), %rax
	leal	-1(%rbx), %edx
	leaq	8(%rax), %rbx
	leaq	(%rbx,%rdx,8), %r15
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$8, %rbx
.L35:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	*(%rax)
	cmpq	%rbx, %r15
	movq	%rbx, %rax
	jne	.L59
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L56:
	call	call_init
	movq	$0, _dl_initfirst(%rip)
	jmp	.L31
.L58:
	movq	8(%r14), %rsi
	cmpb	$0, (%rsi)
	jne	.L34
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L34:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L33
	.size	_dl_init, .-_dl_init
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9053, @object
	.size	__PRETTY_FUNCTION__.9053, 10
__PRETTY_FUNCTION__.9053:
	.string	"call_init"
	.hidden	__assert_fail
	.hidden	_dl_debug_printf
