	.text
	.p2align 4,,15
	.type	rtld_calloc, @function
rtld_calloc:
	movq	%rdi, %rcx
	movl	$4294967295, %eax
	movq	%rcx, %rdx
	orq	%rsi, %rdx
	imulq	%rsi, %rdi
	cmpq	%rax, %rdx
	jbe	.L2
	testq	%rsi, %rsi
	jne	.L11
.L2:
	jmp	*__rtld_malloc(%rip)
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	cmpq	%rcx, %rax
	je	.L2
	xorl	%eax, %eax
	ret
	.size	rtld_calloc, .-rtld_calloc
	.p2align 4,,15
	.type	rtld_malloc, @function
rtld_malloc:
	movq	alloc_end(%rip), %rcx
	testq	%rcx, %rcx
	je	.L13
	movq	alloc_ptr(%rip), %rdx
.L14:
	addq	$15, %rdx
	andq	$-16, %rdx
	leaq	(%rdx,%rdi), %rax
	movq	%rdx, alloc_ptr(%rip)
	cmpq	%rcx, %rax
	jnb	.L15
	movq	%rdx, %rcx
	negq	%rcx
	cmpq	%rdi, %rcx
	ja	.L32
.L15:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	24+_rtld_local_ro(%rip), %rax
	leaq	-1(%rax,%rdi), %rbx
	movq	%rax, %rdx
	negq	%rdx
	andq	%rdx, %rbx
	jne	.L17
	testq	%rdi, %rdi
	jne	.L19
.L17:
	addq	%rax, %rbx
	xorl	%r9d, %r9d
	movq	%rdi, %rbp
	movl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	movq	%rbx, %rsi
	call	__mmap
	cmpq	$-1, %rax
	je	.L19
	cmpq	%rax, alloc_end(%rip)
	movq	%rax, %rdx
	je	.L35
.L20:
	addq	%rbx, %rax
	movq	%rdx, alloc_last_block(%rip)
	movq	%rax, alloc_end(%rip)
	leaq	(%rdx,%rbp), %rax
	movq	%rax, alloc_ptr(%rip)
	addq	$8, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rax, alloc_ptr(%rip)
	movq	%rdx, alloc_last_block(%rip)
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	24+_rtld_local_ro(%rip), %rax
	leaq	_end(%rip), %rdx
	leaq	-1(%rax,%rdx), %rcx
	negq	%rax
	andq	%rax, %rcx
	movq	%rcx, alloc_end(%rip)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L35:
	movq	alloc_ptr(%rip), %rdx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$8, %rsp
	xorl	%edx, %edx
	movq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	rtld_malloc, .-rtld_malloc
	.p2align 4,,15
	.type	rtld_free, @function
rtld_free:
	movq	alloc_last_block(%rip), %rcx
	cmpq	%rdi, %rcx
	je	.L42
	rep ret
	.p2align 4,,10
	.p2align 3
.L42:
	subq	$8, %rsp
	movq	alloc_ptr(%rip), %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	subq	%rcx, %rdx
	call	memset@PLT
	movq	%rax, alloc_ptr(%rip)
	addq	$8, %rsp
	ret
	.size	rtld_free, .-rtld_free
	.p2align 4,,15
	.globl	__rtld_malloc_init_stubs
	.hidden	__rtld_malloc_init_stubs
	.type	__rtld_malloc_init_stubs, @function
__rtld_malloc_init_stubs:
	leaq	rtld_calloc(%rip), %rax
	movq	%rax, __rtld_calloc(%rip)
	leaq	rtld_free(%rip), %rax
	movq	%rax, __rtld_free(%rip)
	leaq	rtld_malloc(%rip), %rax
	movq	%rax, __rtld_malloc(%rip)
	leaq	rtld_realloc(%rip), %rax
	movq	%rax, __rtld_realloc(%rip)
	ret
	.size	__rtld_malloc_init_stubs, .-__rtld_malloc_init_stubs
	.p2align 4,,15
	.weak	__sigjmp_save
	.type	__sigjmp_save, @function
__sigjmp_save:
	movl	$0, 64(%rdi)
	xorl	%eax, %eax
	ret
	.size	__sigjmp_save, .-__sigjmp_save
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Cannot allocate memory"
.LC1:
	.string	"Invalid argument"
.LC2:
	.string	"No such file or directory"
.LC3:
	.string	"Operation not permitted"
.LC4:
	.string	"Input/output error"
.LC5:
	.string	"Permission denied"
	.text
	.p2align 4,,15
	.weak	__strerror_r
	.hidden	__strerror_r
	.type	__strerror_r, @function
__strerror_r:
	cmpl	$22, %edi
	ja	.L46
	leaq	.L48(%rip), %r8
	movl	%edi, %ecx
	movslq	(%r8,%rcx,4), %rax
	addq	%r8, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L48:
	.long	.L46-.L48
	.long	.L47-.L48
	.long	.L49-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L50-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L55-.L48
	.long	.L52-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L46-.L48
	.long	.L53-.L48
	.text
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC0(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	.LC5(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	.LC1(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	.LC3(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	.LC2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	.LC4(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	leaq	-1(%rsi,%rdx), %rcx
	movslq	%edi, %rdi
	leaq	__GI__itoa_lower_digits(%rip), %r9
	movabsq	$-3689348814741910323, %r8
	movb	$0, (%rcx)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%rsi, %rcx
.L54:
	movq	%rdi, %rax
	leaq	-1(%rcx), %rsi
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	testq	%rdx, %rdx
	movzbl	(%r9,%rdi), %eax
	movq	%rdx, %rdi
	movb	%al, -1(%rcx)
	jne	.L56
	movl	$8306, %eax
	movl	$1869771333, -7(%rcx)
	movw	%ax, -3(%rcx)
	leaq	-7(%rcx), %rax
	ret
	.size	__strerror_r, .-__strerror_r
	.section	.rodata.str1.1
.LC6:
	.string	"%s"
	.text
	.p2align 4,,15
	.globl	__GI___libc_fatal
	.hidden	__GI___libc_fatal
	.type	__GI___libc_fatal, @function
__GI___libc_fatal:
	movq	%rdi, %rsi
	leaq	.LC6(%rip), %rdi
	subq	$8, %rsp
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.size	__GI___libc_fatal, .-__GI___libc_fatal
	.globl	__libc_fatal
	.set	__libc_fatal,__GI___libc_fatal
	.p2align 4,,15
	.globl	__GI___chk_fail
	.hidden	__GI___chk_fail
	.type	__GI___chk_fail, @function
__GI___chk_fail:
	subq	$8, %rsp
	movl	$127, %edi
	call	__GI__exit
	.size	__GI___chk_fail, .-__GI___chk_fail
	.globl	__chk_fail
	.set	__chk_fail,__GI___chk_fail
	.section	.rodata.str1.1
.LC7:
	.string	": "
.LC8:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"Inconsistency detected by ld.so: %s: %u: %s%sAssertion `%s' failed!\n"
	.text
	.p2align 4,,15
	.weak	__GI___assert_fail
	.hidden	__GI___assert_fail
	.type	__GI___assert_fail, @function
__GI___assert_fail:
	subq	$8, %rsp
	testq	%rcx, %rcx
	movq	%rdi, %r9
	leaq	.LC7(%rip), %r8
	je	.L65
.L62:
	leaq	.LC9(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L65:
	leaq	.LC8(%rip), %r8
	movq	%r8, %rcx
	jmp	.L62
	.size	__GI___assert_fail, .-__GI___assert_fail
	.weak	__assert_fail
	.set	__assert_fail,__GI___assert_fail
	.section	.rodata.str1.1
.LC10:
	.string	"dl-minimal.c"
.LC11:
	.string	"ptr == alloc_last_block"
	.text
	.p2align 4,,15
	.type	rtld_realloc, @function
rtld_realloc:
	testq	%rdi, %rdi
	jne	.L67
	movq	%rsi, %rdi
	jmp	*__rtld_malloc(%rip)
	.p2align 4,,10
	.p2align 3
.L67:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rax
	movq	%rsi, %rdi
	subq	$8, %rsp
	movq	alloc_last_block(%rip), %rbx
	cmpq	%rax, %rbx
	jne	.L72
	movq	alloc_ptr(%rip), %rbp
	movq	%rbx, alloc_ptr(%rip)
	call	*__rtld_malloc(%rip)
	cmpq	%rax, %rbx
	movq	%rax, %rcx
	je	.L66
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	subq	%rbx, %rdx
	call	memcpy@PLT
	movq	%rax, %rcx
.L66:
	addq	$8, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%rbp
	ret
.L72:
	leaq	__PRETTY_FUNCTION__.11032(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$205, %edx
	call	__GI___assert_fail
	.size	rtld_realloc, .-rtld_realloc
	.section	.rodata.str1.1
.LC12:
	.string	"__rtld_malloc != NULL"
	.text
	.p2align 4,,15
	.globl	__rtld_malloc_is_complete
	.hidden	__rtld_malloc_is_complete
	.type	__rtld_malloc_is_complete, @function
__rtld_malloc_is_complete:
	movq	__rtld_malloc(%rip), %rax
	testq	%rax, %rax
	je	.L78
	leaq	rtld_malloc(%rip), %rdx
	cmpq	%rdx, %rax
	setne	%al
	ret
.L78:
	leaq	__PRETTY_FUNCTION__.10992(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	subq	$8, %rsp
	movl	$66, %edx
	call	__GI___assert_fail
	.size	__rtld_malloc_is_complete, .-__rtld_malloc_is_complete
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"ELFW(ST_TYPE) (ref->st_info) != STT_TLS"
	.text
	.p2align 4,,15
	.type	lookup_malloc_symbol, @function
lookup_malloc_symbol:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r8
	pushq	%r13
	pushq	%r12
	xorl	%r9d, %r9d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rdi
	movq	%rbp, %rsi
	subq	$88, %rsp
	movq	920(%rbp), %rcx
	leaq	40(%rsp), %rdx
	movq	$0, 40(%rsp)
	pushq	$0
	pushq	$0
	call	_dl_lookup_symbol_x
	movq	56(%rsp), %rbx
	popq	%rcx
	popq	%rsi
	movzbl	4(%rbx), %edx
	andl	$15, %edx
	cmpb	$6, %dl
	je	.L110
	cmpw	$-15, 6(%rbx)
	movq	%rax, %r12
	je	.L92
	testq	%rax, %rax
	je	.L92
	movq	(%rax), %rax
	addq	8(%rbx), %rax
	cmpb	$10, %dl
	je	.L111
.L82:
	movl	800+_rtld_local_ro(%rip), %edx
	testl	%edx, %edx
	jne	.L112
.L79:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%eax, %eax
	addq	8(%rbx), %rax
	cmpb	$10, %dl
	jne	.L82
.L111:
	call	*%rax
	movl	800+_rtld_local_ro(%rip), %edx
	testl	%edx, %edx
	je	.L79
	.p2align 4,,10
	.p2align 3
.L112:
	movzbl	797(%r12), %edx
	orb	797(%rbp), %dl
	andl	$16, %edx
	je	.L79
	movq	104(%r12), %rdx
	movq	792+_rtld_local_ro(%rip), %r14
	xorl	%r13d, %r13d
	movdqu	(%rbx), %xmm0
	leaq	2568+_rtld_local(%rip), %r15
	movl	$0, 8(%rsp)
	movq	8(%rdx), %rsi
	movq	112(%r12), %rdx
	movq	8(%rdx), %rdx
	movq	%rsi, (%rsp)
	movaps	%xmm0, 48(%rsp)
	movq	%rax, 56(%rsp)
	movq	%rbx, %rax
	movq	16(%rbx), %rcx
	subq	%rdx, %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	movq	%rcx, 64(%rsp)
	movl	%eax, 12(%rsp)
	leaq	36(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	%rbp, %rax
	movq	%rbx, %rbp
	movq	%rax, %rbx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	%r15, %r12
	leaq	1160(%r12,%rcx), %rcx
	je	.L113
.L87:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L88
	testb	$2, 8(%rdx)
	je	.L114
.L89:
	movl	8(%rsp), %esi
	movl	0(%rbp), %r9d
	addq	(%rsp), %r9
	movq	16(%rsp), %r8
	movq	24(%rsp), %rdi
	orl	$8, %esi
	movl	%esi, 36(%rsp)
	movl	12(%rsp), %esi
	call	*%rax
	cmpq	56(%rsp), %rax
	je	.L88
	movq	%rax, 56(%rsp)
	movl	$16, 8(%rsp)
.L88:
	addl	$1, %r13d
	cmpl	800+_rtld_local_ro(%rip), %r13d
	movq	64(%r14), %r14
	jnb	.L115
.L91:
	movl	%r13d, %eax
	movq	%rax, %rdx
	salq	$4, %rdx
	cmpq	%r15, %rbx
	leaq	1160(%rbx,%rdx), %rdx
	jne	.L85
	leaq	233(%rax), %rdx
	leaq	_rtld_local(%rip), %rsi
	salq	$4, %rdx
	addq	%rsi, %rdx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L113:
	addq	$233, %rax
	leaq	_rtld_local(%rip), %rsi
	salq	$4, %rax
	leaq	(%rax,%rsi), %rcx
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L115:
	movq	56(%rsp), %rax
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L114:
	testb	$1, 8(%rcx)
	je	.L88
	jmp	.L89
.L110:
	leaq	__PRETTY_FUNCTION__.11000(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$81, %edx
	call	__GI___assert_fail
	.size	lookup_malloc_symbol, .-lookup_malloc_symbol
	.section	.rodata.str1.1
.LC14:
	.string	"GLIBC_2.2.5"
.LC15:
	.string	"calloc"
.LC16:
	.string	"free"
.LC17:
	.string	"malloc"
.LC18:
	.string	"realloc"
	.text
	.p2align 4,,15
	.globl	__rtld_malloc_init_real
	.hidden	__rtld_malloc_init_real
	.type	__rtld_malloc_init_real, @function
__rtld_malloc_init_real:
	pushq	%r14
	pushq	%r13
	leaq	.LC14(%rip), %rax
	pushq	%r12
	pushq	%rbp
	movl	$95, %edx
	pushq	%rbx
	leaq	5(%rax), %rcx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rax, (%rsp)
	movl	$0, 12(%rsp)
	movl	$4984163, %eax
.L117:
	salq	$4, %rax
	addq	$1, %rcx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$240, %edx
	xorq	%rdx, %rax
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L117
	movq	%rsp, %rbp
	leaq	.LC15(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	andl	$268435455, %eax
	movq	$0, 16(%rsp)
	movl	%eax, 8(%rsp)
	call	lookup_malloc_symbol
	leaq	.LC16(%rip), %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	lookup_malloc_symbol
	leaq	.LC17(%rip), %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	lookup_malloc_symbol
	leaq	.LC18(%rip), %rsi
	movq	%rax, %r12
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	call	lookup_malloc_symbol
	movq	%r14, __rtld_calloc(%rip)
	movq	%r13, __rtld_free(%rip)
	movq	%r12, __rtld_malloc(%rip)
	movq	%rax, __rtld_realloc(%rip)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__rtld_malloc_init_real, .-__rtld_malloc_init_real
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"Inconsistency detected by ld.so: %s: %u: %s%sUnexpected error: %s.\n"
	.text
	.p2align 4,,15
	.weak	__GI___assert_perror_fail
	.hidden	__GI___assert_perror_fail
	.type	__GI___assert_perror_fail, @function
__GI___assert_perror_fail:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rcx, %rbx
	movl	$400, %edx
	subq	$400, %rsp
	movq	%rsp, %rsi
	call	__strerror_r
	testq	%rbx, %rbx
	movq	%rax, %r9
	leaq	.LC7(%rip), %r8
	je	.L124
.L121:
	leaq	.LC19(%rip), %rdi
	movq	%rbx, %rcx
	movl	%r12d, %edx
	movq	%rbp, %rsi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L124:
	leaq	.LC8(%rip), %r8
	movq	%r8, %rbx
	jmp	.L121
	.size	__GI___assert_perror_fail, .-__GI___assert_perror_fail
	.weak	__assert_perror_fail
	.set	__assert_perror_fail,__GI___assert_perror_fail
	.section	.rodata.str1.1
.LC20:
	.string	"! upper_case"
	.text
	.p2align 4,,15
	.globl	_itoa
	.hidden	_itoa
	.type	_itoa, @function
_itoa:
	testl	%ecx, %ecx
	jne	.L132
	movq	%rdi, %rax
	leaq	__GI__itoa_lower_digits(%rip), %rdi
	movl	%edx, %ecx
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%edx, %edx
	subq	$1, %rsi
	divq	%rcx
	movzbl	(%rdi,%rdx), %edx
	testq	%rax, %rax
	movb	%dl, (%rsi)
	jne	.L127
	movq	%rsi, %rax
	ret
.L132:
	leaq	__PRETTY_FUNCTION__.11086(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	subq	$8, %rsp
	movl	$324, %edx
	call	__GI___assert_fail
	.size	_itoa, .-_itoa
	.section	.rodata.str1.1
.LC21:
	.string	"delim[0] != '\\0'"
	.text
	.p2align 4,,15
	.globl	__strsep
	.type	__strsep, @function
__strsep:
	movzbl	(%rsi), %r10d
	testb	%r10b, %r10b
	je	.L151
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L133
	movzbl	(%rax), %r8d
	testb	%r8b, %r8b
	je	.L141
	movq	%rax, %r9
	.p2align 4,,10
	.p2align 3
.L140:
	movl	%r10d, %edx
	movq	%rsi, %rcx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L153:
	addq	$1, %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L152
.L138:
	cmpb	%r8b, %dl
	jne	.L153
	leaq	1(%r9), %rdx
	movb	$0, (%r9)
.L136:
	movq	%rdx, (%rdi)
.L133:
	rep ret
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$1, %r9
	movzbl	(%r9), %r8d
	testb	%r8b, %r8b
	jne	.L140
.L141:
	xorl	%edx, %edx
	jmp	.L136
.L151:
	leaq	__PRETTY_FUNCTION__.11097(%rip), %rcx
	leaq	.LC10(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	subq	$8, %rsp
	movl	$348, %edx
	call	__GI___assert_fail
	.size	__strsep, .-__strsep
	.globl	__strsep_g
	.hidden	__strsep_g
	.set	__strsep_g,__strsep
	.weak	strsep
	.hidden	strsep
	.set	strsep,__strsep
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11097, @object
	.size	__PRETTY_FUNCTION__.11097, 9
__PRETTY_FUNCTION__.11097:
	.string	"__strsep"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.11086, @object
	.size	__PRETTY_FUNCTION__.11086, 6
__PRETTY_FUNCTION__.11086:
	.string	"_itoa"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11000, @object
	.size	__PRETTY_FUNCTION__.11000, 21
__PRETTY_FUNCTION__.11000:
	.string	"lookup_malloc_symbol"
	.align 16
	.type	__PRETTY_FUNCTION__.10992, @object
	.size	__PRETTY_FUNCTION__.10992, 26
__PRETTY_FUNCTION__.10992:
	.string	"__rtld_malloc_is_complete"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11032, @object
	.size	__PRETTY_FUNCTION__.11032, 13
__PRETTY_FUNCTION__.11032:
	.string	"rtld_realloc"
	.hidden	__GI__itoa_lower_digits
	.globl	__GI__itoa_lower_digits
	.section	.rodata
	.align 16
	.type	__GI__itoa_lower_digits, @object
	.size	__GI__itoa_lower_digits, 16
__GI__itoa_lower_digits:
	.ascii	"0123456789abcdef"
	.globl	_itoa_lower_digits
	.set	_itoa_lower_digits,__GI__itoa_lower_digits
	.local	alloc_last_block
	.comm	alloc_last_block,8,8
	.local	alloc_end
	.comm	alloc_end,8,8
	.local	alloc_ptr
	.comm	alloc_ptr,8,8
	.hidden	__rtld_realloc
	.globl	__rtld_realloc
	.section	.data.rel.ro,"aw",@progbits
	.align 8
	.type	__rtld_realloc, @object
	.size	__rtld_realloc, 8
__rtld_realloc:
	.zero	8
	.hidden	__rtld_malloc
	.globl	__rtld_malloc
	.align 8
	.type	__rtld_malloc, @object
	.size	__rtld_malloc, 8
__rtld_malloc:
	.zero	8
	.hidden	__rtld_free
	.globl	__rtld_free
	.align 8
	.type	__rtld_free, @object
	.size	__rtld_free, 8
__rtld_free:
	.zero	8
	.hidden	__rtld_calloc
	.globl	__rtld_calloc
	.align 8
	.type	__rtld_calloc, @object
	.size	__rtld_calloc, 8
__rtld_calloc:
	.zero	8
	.hidden	_rtld_local
	.hidden	_dl_lookup_symbol_x
	.hidden	_end
	.hidden	__mmap
	.hidden	_rtld_local_ro
