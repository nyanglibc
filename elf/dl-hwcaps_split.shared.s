	.text
	.p2align 4,,15
	.type	_dl_hwcaps_split.part.0, @function
_dl_hwcaps_split.part.0:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rbx
	movzbl	(%rbx), %edx
	cmpb	$58, %dl
	jne	.L2
	leaq	1(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rax, %rbx
	movq	%rax, (%rdi)
	addq	$1, %rax
	movzbl	-1(%rax), %edx
	cmpb	$58, %dl
	je	.L3
.L2:
	xorl	%eax, %eax
	testb	%dl, %dl
	je	.L1
	movq	%rdi, %rbp
	movl	$58, %esi
	movq	%rbx, %rdi
	call	strchr
	testq	%rax, %rax
	je	.L12
	subq	%rbx, %rax
	movq	%rax, 8(%rbp)
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rdi
	call	strlen
	movq	%rax, 8(%rbp)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_dl_hwcaps_split.part.0, .-_dl_hwcaps_split.part.0
	.p2align 4,,15
	.globl	_dl_hwcaps_split
	.hidden	_dl_hwcaps_split
	.type	_dl_hwcaps_split, @function
_dl_hwcaps_split:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L14
	addq	8(%rdi), %rax
	movq	%rax, (%rdi)
	jmp	_dl_hwcaps_split.part.0
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	ret
	.size	_dl_hwcaps_split, .-_dl_hwcaps_split
	.p2align 4,,15
	.globl	_dl_hwcaps_contains
	.hidden	_dl_hwcaps_contains
	.type	_dl_hwcaps_contains, @function
_dl_hwcaps_contains:
	testq	%rdi, %rdi
	je	.L30
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r13
	pushq	%rbx
	xorl	%ebp, %ebp
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	$0, 8(%rsp)
	movq	%rsp, %r12
	.p2align 4,,10
	.p2align 3
.L18:
	addq	%rbp, %rbx
	movq	%r12, %rdi
	movq	%rbx, (%rsp)
	call	_dl_hwcaps_split.part.0
	testb	%al, %al
	je	.L15
	movq	8(%rsp), %rbp
	movq	(%rsp), %rbx
	cmpq	%r13, %rbp
	je	.L31
	testq	%rbx, %rbx
	jne	.L18
	xorl	%eax, %eax
.L15:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L18
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$1, %eax
	ret
	.size	_dl_hwcaps_contains, .-_dl_hwcaps_contains
	.p2align 4,,15
	.globl	_dl_hwcaps_split_masked
	.hidden	_dl_hwcaps_split_masked
	.type	_dl_hwcaps_split_masked, @function
_dl_hwcaps_split_masked:
	pushq	%rbx
	movq	%rdi, %rbx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L47:
	addq	8(%rbx), %rax
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	call	_dl_hwcaps_split.part.0
	testb	%al, %al
	je	.L33
	movl	24(%rbx), %eax
	movl	%eax, %edx
	shrl	%edx
	testb	$1, %al
	movl	%edx, 24(%rbx)
	jne	.L46
.L34:
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L47
.L33:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rdi
	movq	(%rbx), %rsi
	call	_dl_hwcaps_contains
	testb	%al, %al
	je	.L34
	popq	%rbx
	ret
	.size	_dl_hwcaps_split_masked, .-_dl_hwcaps_split_masked
	.hidden	strlen
	.hidden	strchr
