	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_dl_mcount_wrapper
	.type	_dl_mcount_wrapper, @function
_dl_mcount_wrapper:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	%rdi, %rsi
	movq	(%rsp), %rdi
	movq	744(%rax), %rax
	jmp	*%rax
	.size	_dl_mcount_wrapper, .-_dl_mcount_wrapper
	.p2align 4,,15
	.globl	__GI__dl_mcount_wrapper_check
	.hidden	__GI__dl_mcount_wrapper_check
	.type	__GI__dl_mcount_wrapper_check, @function
__GI__dl_mcount_wrapper_check:
	movq	_rtld_global@GOTPCREL(%rip), %rax
	cmpq	$0, 2536(%rax)
	je	.L3
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	%rdi, %rsi
	movq	(%rsp), %rdi
	movq	744(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L3:
	rep ret
	.size	__GI__dl_mcount_wrapper_check, .-__GI__dl_mcount_wrapper_check
	.globl	_dl_mcount_wrapper_check
	.set	_dl_mcount_wrapper_check,__GI__dl_mcount_wrapper_check
