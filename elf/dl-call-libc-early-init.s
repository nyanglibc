	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"GLIBC_PRIVATE"
.LC1:
	.string	"__libc_early_init"
.LC2:
	.string	"dl-call-libc-early-init.c"
.LC3:
	.string	"sym != NULL"
	.text
	.p2align 4,,15
	.globl	_dl_call_libc_early_init
	.hidden	_dl_call_libc_early_init
	.type	_dl_call_libc_early_init, @function
_dl_call_libc_early_init:
	testq	%rdi, %rdi
	je	.L1
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	leaq	.LC0(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$157536133, %r8d
	subq	$8, %rsp
	movl	$110527148, %edx
	movq	%rdi, %rbx
	call	_dl_lookup_direct
	testq	%rax, %rax
	je	.L10
	cmpw	$-15, 6(%rax)
	je	.L5
	movq	(%rbx), %rdx
.L4:
	addq	8(%rax), %rdx
	addq	$8, %rsp
	movzbl	%bpl, %edi
	popq	%rbx
	popq	%rbp
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%edx, %edx
	jmp	.L4
.L10:
	leaq	__PRETTY_FUNCTION__.9053(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$37, %edx
	call	__assert_fail
	.size	_dl_call_libc_early_init, .-_dl_call_libc_early_init
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9053, @object
	.size	__PRETTY_FUNCTION__.9053, 25
__PRETTY_FUNCTION__.9053:
	.string	"_dl_call_libc_early_init"
	.hidden	__assert_fail
	.hidden	_dl_lookup_direct
