 .text
 .globl _dl_runtime_profile_avx512
 .hidden _dl_runtime_profile_avx512
 .type _dl_runtime_profile_avx512, @function
 .align 16
_dl_runtime_profile_avx512:

 sub $32, %rsp # Allocate the local storage.
 movq %rbx, (%rsp)
 movq %rax, 8(%rsp)
 mov %rsp, %rbx
 and $-64, %rsp
 sub $(768 + 16*8), %rsp
 movq %rsp, 24(%rbx)
 movq %rdx, 0(%rsp)
 movq %r8, 8(%rsp)
 movq %r9, 16(%rsp)
 movq %rcx, 24(%rsp)
 movq %rsi, 32(%rsp)
 movq %rdi, 40(%rsp)
 movq %rbp, 48(%rsp)
 lea 48(%rbx), %rax
 movq %rax, 56(%rsp)
 movaps %xmm0, (64)(%rsp)
 movaps %xmm1, (64 + 16)(%rsp)
 movaps %xmm2, (64 + 16*2)(%rsp)
 movaps %xmm3, (64 + 16*3)(%rsp)
 movaps %xmm4, (64 + 16*4)(%rsp)
 movaps %xmm5, (64 + 16*5)(%rsp)
 movaps %xmm6, (64 + 16*6)(%rsp)
 movaps %xmm7, (64 + 16*7)(%rsp)
 bndmov %bnd0, (704)(%rsp) # Preserve bound
 bndmov %bnd1, (704 + 16)(%rsp) # registers. Nops if
 bndmov %bnd2, (704 + 16*2)(%rsp) # MPX not available
 bndmov %bnd3, (704 + 16*3)(%rsp) # or disabled.
 vmovdqa64 %zmm0, (192)(%rsp)
 vmovdqa64 %zmm1, (192 + 64)(%rsp)
 vmovdqa64 %zmm2, (192 + 64*2)(%rsp)
 vmovdqa64 %zmm3, (192 + 64*3)(%rsp)
 vmovdqa64 %zmm4, (192 + 64*4)(%rsp)
 vmovdqa64 %zmm5, (192 + 64*5)(%rsp)
 vmovdqa64 %zmm6, (192 + 64*6)(%rsp)
 vmovdqa64 %zmm7, (192 + 64*7)(%rsp)
 vmovdqa %xmm0, (768)(%rsp)
 vmovdqa %xmm1, (768 + 16)(%rsp)
 vmovdqa %xmm2, (768 + 16*2)(%rsp)
 vmovdqa %xmm3, (768 + 16*3)(%rsp)
 vmovdqa %xmm4, (768 + 16*4)(%rsp)
 vmovdqa %xmm5, (768 + 16*5)(%rsp)
 vmovdqa %xmm6, (768 + 16*6)(%rsp)
 vmovdqa %xmm7, (768 + 16*7)(%rsp)
 mov %rsp, %rcx # La_x86_64_regs pointer to %rcx.
 mov 48(%rbx), %rdx # Load return address if needed.
 mov 40(%rbx), %rsi # Copy args pushed by PLT in register.
 mov 32(%rbx), %rdi # %rdi: link_map, %rsi: reloc_index
 lea 16(%rbx), %r8 # Address of framesize
 call _dl_profile_fixup # Call resolver.
 mov %rax, %r11 # Save return value.
 movq 8(%rbx), %rax # Get back register content.
 movq 0(%rsp), %rdx
 movq 8(%rsp), %r8
 movq 16(%rsp), %r9
 movaps (64)(%rsp), %xmm0
 movaps (64 + 16)(%rsp), %xmm1
 movaps (64 + 16*2)(%rsp), %xmm2
 movaps (64 + 16*3)(%rsp), %xmm3
 movaps (64 + 16*4)(%rsp), %xmm4
 movaps (64 + 16*5)(%rsp), %xmm5
 movaps (64 + 16*6)(%rsp), %xmm6
 movaps (64 + 16*7)(%rsp), %xmm7
 vpcmpeqq (768)(%rsp), %xmm0, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm0, (192)(%rsp)
 jmp 1f
2: vmovdqa64 (192)(%rsp), %zmm0
 vmovdqa %xmm0, (64)(%rsp)
1: vpcmpeqq (768 + 16)(%rsp), %xmm1, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm1, (192 + 64)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64)(%rsp), %zmm1
 vmovdqa %xmm1, (64 + 16)(%rsp)
1: vpcmpeqq (768 + 16*2)(%rsp), %xmm2, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm2, (192 + 64*2)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64*2)(%rsp), %zmm2
 vmovdqa %xmm2, (64 + 16*2)(%rsp)
1: vpcmpeqq (768 + 16*3)(%rsp), %xmm3, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm3, (192 + 64*3)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64*3)(%rsp), %zmm3
 vmovdqa %xmm3, (64 + 16*3)(%rsp)
1: vpcmpeqq (768 + 16*4)(%rsp), %xmm4, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm4, (192 + 64*4)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64*4)(%rsp), %zmm4
 vmovdqa %xmm4, (64 + 16*4)(%rsp)
1: vpcmpeqq (768 + 16*5)(%rsp), %xmm5, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm5, (192 + 64*5)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64*5)(%rsp), %zmm5
 vmovdqa %xmm5, (64 + 16*5)(%rsp)
1: vpcmpeqq (768 + 16*6)(%rsp), %xmm6, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm6, (192 + 64*6)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64*6)(%rsp), %zmm6
 vmovdqa %xmm6, (64 + 16*6)(%rsp)
1: vpcmpeqq (768 + 16*7)(%rsp), %xmm7, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm7, (192 + 64*7)(%rsp)
 jmp 1f
2: vmovdqa64 (192 + 64*7)(%rsp), %zmm7
 vmovdqa %xmm7, (64 + 16*7)(%rsp)
1:
 bndmov (704)(%rsp), %bnd0 # Restore bound
 bndmov (704 + 16)(%rsp), %bnd1 # registers.
 bndmov (704 + 16*2)(%rsp), %bnd2
 bndmov (704 + 16*3)(%rsp), %bnd3
 mov 16(%rbx), %r10 # Anything in framesize?
 test %r10, %r10
 bnd
 jns 3f
 movq 24(%rsp), %rcx
 movq 32(%rsp), %rsi
 movq 40(%rsp), %rdi
 mov %rbx, %rsp
 movq (%rsp), %rbx
 add $48, %rsp # Adjust the stack to the return value
    # (eats the reloc index and link_map)
 bnd
 jmp *%r11 # Jump to function address.
3:
 lea 56(%rbx), %rsi # stack
 add $8, %r10
 and $-16, %r10
 mov %r10, %rcx
 sub %r10, %rsp
 mov %rsp, %rdi
 shr $3, %rcx
 rep
 movsq
 movq 24(%rdi), %rcx # Get back register content.
 movq 32(%rdi), %rsi
 movq 40(%rdi), %rdi
 bnd
 call *%r11
 mov 24(%rbx), %rsp # Drop the copied stack content
 sub $(240 + 16*2), %rsp
 mov %rsp, %rcx # La_x86_64_retval argument to %rcx.
 movq %rax, 0(%rcx)
 movq %rdx, 8(%rcx)
 movaps %xmm0, 16(%rcx)
 movaps %xmm1, 32(%rcx)
 vmovdqa64 %zmm0, 80(%rcx)
 vmovdqa64 %zmm1, 144(%rcx)
 vmovdqa %xmm0, (240)(%rcx)
 vmovdqa %xmm1, (240 + 16)(%rcx)
 bndmov %bnd0, 208(%rcx) # Preserve returned bounds.
 bndmov %bnd1, 224(%rcx)
 fstpt 48(%rcx)
 fstpt 64(%rcx)
 movq 24(%rbx), %rdx # La_x86_64_regs argument to %rdx.
 movq 40(%rbx), %rsi # Copy args pushed by PLT in register.
 movq 32(%rbx), %rdi # %rdi: link_map, %rsi: reloc_index
 call _dl_call_pltexit
 movq 0(%rsp), %rax
 movq 8(%rsp), %rdx
 movaps 16(%rsp), %xmm0
 movaps 32(%rsp), %xmm1
 vpcmpeqq (240)(%rsp), %xmm0, %xmm2
 vpmovmskb %xmm2, %esi
 cmpl $0xffff, %esi
 jne 1f
 vmovdqa64 80(%rsp), %zmm0
1: vpcmpeqq (240 + 16)(%rsp), %xmm1, %xmm2
 vpmovmskb %xmm2, %esi
 cmpl $0xffff, %esi
 jne 1f
 vmovdqa64 144(%rsp), %zmm1
1:
 bndmov 208(%rsp), %bnd0 # Restore bound registers.
 bndmov 224(%rsp), %bnd1
 fldt 64(%rsp)
 fldt 48(%rsp)
 mov %rbx, %rsp
 movq (%rsp), %rbx
 add $48, %rsp # Adjust the stack to the return value
    # (eats the reloc index and link_map)
 bnd
 retq
 .size _dl_runtime_profile_avx512, .-_dl_runtime_profile_avx512
 .text
 .globl _dl_runtime_profile_avx
 .hidden _dl_runtime_profile_avx
 .type _dl_runtime_profile_avx, @function
 .align 16
_dl_runtime_profile_avx:

 sub $32, %rsp # Allocate the local storage.
 movq %rbx, (%rsp)
 movq %rax, 8(%rsp)
 mov %rsp, %rbx
 and $-32, %rsp
 sub $(768 + 16*8), %rsp
 movq %rsp, 24(%rbx)
 movq %rdx, 0(%rsp)
 movq %r8, 8(%rsp)
 movq %r9, 16(%rsp)
 movq %rcx, 24(%rsp)
 movq %rsi, 32(%rsp)
 movq %rdi, 40(%rsp)
 movq %rbp, 48(%rsp)
 lea 48(%rbx), %rax
 movq %rax, 56(%rsp)
 movaps %xmm0, (64)(%rsp)
 movaps %xmm1, (64 + 16)(%rsp)
 movaps %xmm2, (64 + 16*2)(%rsp)
 movaps %xmm3, (64 + 16*3)(%rsp)
 movaps %xmm4, (64 + 16*4)(%rsp)
 movaps %xmm5, (64 + 16*5)(%rsp)
 movaps %xmm6, (64 + 16*6)(%rsp)
 movaps %xmm7, (64 + 16*7)(%rsp)
 bndmov %bnd0, (704)(%rsp) # Preserve bound
 bndmov %bnd1, (704 + 16)(%rsp) # registers. Nops if
 bndmov %bnd2, (704 + 16*2)(%rsp) # MPX not available
 bndmov %bnd3, (704 + 16*3)(%rsp) # or disabled.
 vmovdqa %ymm0, (192)(%rsp)
 vmovdqa %ymm1, (192 + 64)(%rsp)
 vmovdqa %ymm2, (192 + 64*2)(%rsp)
 vmovdqa %ymm3, (192 + 64*3)(%rsp)
 vmovdqa %ymm4, (192 + 64*4)(%rsp)
 vmovdqa %ymm5, (192 + 64*5)(%rsp)
 vmovdqa %ymm6, (192 + 64*6)(%rsp)
 vmovdqa %ymm7, (192 + 64*7)(%rsp)
 vmovdqa %xmm0, (768)(%rsp)
 vmovdqa %xmm1, (768 + 16)(%rsp)
 vmovdqa %xmm2, (768 + 16*2)(%rsp)
 vmovdqa %xmm3, (768 + 16*3)(%rsp)
 vmovdqa %xmm4, (768 + 16*4)(%rsp)
 vmovdqa %xmm5, (768 + 16*5)(%rsp)
 vmovdqa %xmm6, (768 + 16*6)(%rsp)
 vmovdqa %xmm7, (768 + 16*7)(%rsp)
 mov %rsp, %rcx # La_x86_64_regs pointer to %rcx.
 mov 48(%rbx), %rdx # Load return address if needed.
 mov 40(%rbx), %rsi # Copy args pushed by PLT in register.
 mov 32(%rbx), %rdi # %rdi: link_map, %rsi: reloc_index
 lea 16(%rbx), %r8 # Address of framesize
 call _dl_profile_fixup # Call resolver.
 mov %rax, %r11 # Save return value.
 movq 8(%rbx), %rax # Get back register content.
 movq 0(%rsp), %rdx
 movq 8(%rsp), %r8
 movq 16(%rsp), %r9
 movaps (64)(%rsp), %xmm0
 movaps (64 + 16)(%rsp), %xmm1
 movaps (64 + 16*2)(%rsp), %xmm2
 movaps (64 + 16*3)(%rsp), %xmm3
 movaps (64 + 16*4)(%rsp), %xmm4
 movaps (64 + 16*5)(%rsp), %xmm5
 movaps (64 + 16*6)(%rsp), %xmm6
 movaps (64 + 16*7)(%rsp), %xmm7
 vpcmpeqq (768)(%rsp), %xmm0, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm0, (192)(%rsp)
 jmp 1f
2: vmovdqa (192)(%rsp), %ymm0
 vmovdqa %xmm0, (64)(%rsp)
1: vpcmpeqq (768 + 16)(%rsp), %xmm1, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm1, (192 + 64)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64)(%rsp), %ymm1
 vmovdqa %xmm1, (64 + 16)(%rsp)
1: vpcmpeqq (768 + 16*2)(%rsp), %xmm2, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm2, (192 + 64*2)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64*2)(%rsp), %ymm2
 vmovdqa %xmm2, (64 + 16*2)(%rsp)
1: vpcmpeqq (768 + 16*3)(%rsp), %xmm3, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm3, (192 + 64*3)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64*3)(%rsp), %ymm3
 vmovdqa %xmm3, (64 + 16*3)(%rsp)
1: vpcmpeqq (768 + 16*4)(%rsp), %xmm4, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm4, (192 + 64*4)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64*4)(%rsp), %ymm4
 vmovdqa %xmm4, (64 + 16*4)(%rsp)
1: vpcmpeqq (768 + 16*5)(%rsp), %xmm5, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm5, (192 + 64*5)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64*5)(%rsp), %ymm5
 vmovdqa %xmm5, (64 + 16*5)(%rsp)
1: vpcmpeqq (768 + 16*6)(%rsp), %xmm6, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm6, (192 + 64*6)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64*6)(%rsp), %ymm6
 vmovdqa %xmm6, (64 + 16*6)(%rsp)
1: vpcmpeqq (768 + 16*7)(%rsp), %xmm7, %xmm8
 vpmovmskb %xmm8, %esi
 cmpl $0xffff, %esi
 je 2f
 vmovdqa %xmm7, (192 + 64*7)(%rsp)
 jmp 1f
2: vmovdqa (192 + 64*7)(%rsp), %ymm7
 vmovdqa %xmm7, (64 + 16*7)(%rsp)
1:
 bndmov (704)(%rsp), %bnd0 # Restore bound
 bndmov (704 + 16)(%rsp), %bnd1 # registers.
 bndmov (704 + 16*2)(%rsp), %bnd2
 bndmov (704 + 16*3)(%rsp), %bnd3
 mov 16(%rbx), %r10 # Anything in framesize?
 test %r10, %r10
 bnd
 jns 3f
 movq 24(%rsp), %rcx
 movq 32(%rsp), %rsi
 movq 40(%rsp), %rdi
 mov %rbx, %rsp
 movq (%rsp), %rbx
 add $48, %rsp # Adjust the stack to the return value
    # (eats the reloc index and link_map)
 bnd
 jmp *%r11 # Jump to function address.
3:
 lea 56(%rbx), %rsi # stack
 add $8, %r10
 and $-16, %r10
 mov %r10, %rcx
 sub %r10, %rsp
 mov %rsp, %rdi
 shr $3, %rcx
 rep
 movsq
 movq 24(%rdi), %rcx # Get back register content.
 movq 32(%rdi), %rsi
 movq 40(%rdi), %rdi
 bnd
 call *%r11
 mov 24(%rbx), %rsp # Drop the copied stack content
 sub $(240 + 16*2), %rsp
 mov %rsp, %rcx # La_x86_64_retval argument to %rcx.
 movq %rax, 0(%rcx)
 movq %rdx, 8(%rcx)
 movaps %xmm0, 16(%rcx)
 movaps %xmm1, 32(%rcx)
 vmovdqa %ymm0, 80(%rcx)
 vmovdqa %ymm1, 144(%rcx)
 vmovdqa %xmm0, (240)(%rcx)
 vmovdqa %xmm1, (240 + 16)(%rcx)
 bndmov %bnd0, 208(%rcx) # Preserve returned bounds.
 bndmov %bnd1, 224(%rcx)
 fstpt 48(%rcx)
 fstpt 64(%rcx)
 movq 24(%rbx), %rdx # La_x86_64_regs argument to %rdx.
 movq 40(%rbx), %rsi # Copy args pushed by PLT in register.
 movq 32(%rbx), %rdi # %rdi: link_map, %rsi: reloc_index
 call _dl_call_pltexit
 movq 0(%rsp), %rax
 movq 8(%rsp), %rdx
 movaps 16(%rsp), %xmm0
 movaps 32(%rsp), %xmm1
 vpcmpeqq (240)(%rsp), %xmm0, %xmm2
 vpmovmskb %xmm2, %esi
 cmpl $0xffff, %esi
 jne 1f
 vmovdqa 80(%rsp), %ymm0
1: vpcmpeqq (240 + 16)(%rsp), %xmm1, %xmm2
 vpmovmskb %xmm2, %esi
 cmpl $0xffff, %esi
 jne 1f
 vmovdqa 144(%rsp), %ymm1
1:
 bndmov 208(%rsp), %bnd0 # Restore bound registers.
 bndmov 224(%rsp), %bnd1
 fldt 64(%rsp)
 fldt 48(%rsp)
 mov %rbx, %rsp
 movq (%rsp), %rbx
 add $48, %rsp # Adjust the stack to the return value
    # (eats the reloc index and link_map)
 bnd
 retq
 .size _dl_runtime_profile_avx, .-_dl_runtime_profile_avx
 .text
 .globl _dl_runtime_profile_sse
 .hidden _dl_runtime_profile_sse
 .type _dl_runtime_profile_sse, @function
 .align 16
_dl_runtime_profile_sse:

 sub $32, %rsp # Allocate the local storage.
 movq %rbx, (%rsp)
 movq %rax, 8(%rsp)
 mov %rsp, %rbx
 and $-16, %rsp
 sub $(768 + 16*8), %rsp
 movq %rsp, 24(%rbx)
 movq %rdx, 0(%rsp)
 movq %r8, 8(%rsp)
 movq %r9, 16(%rsp)
 movq %rcx, 24(%rsp)
 movq %rsi, 32(%rsp)
 movq %rdi, 40(%rsp)
 movq %rbp, 48(%rsp)
 lea 48(%rbx), %rax
 movq %rax, 56(%rsp)
 movaps %xmm0, (64)(%rsp)
 movaps %xmm1, (64 + 16)(%rsp)
 movaps %xmm2, (64 + 16*2)(%rsp)
 movaps %xmm3, (64 + 16*3)(%rsp)
 movaps %xmm4, (64 + 16*4)(%rsp)
 movaps %xmm5, (64 + 16*5)(%rsp)
 movaps %xmm6, (64 + 16*6)(%rsp)
 movaps %xmm7, (64 + 16*7)(%rsp)
 bndmov %bnd0, (704)(%rsp) # Preserve bound
 bndmov %bnd1, (704 + 16)(%rsp) # registers. Nops if
 bndmov %bnd2, (704 + 16*2)(%rsp) # MPX not available
 bndmov %bnd3, (704 + 16*3)(%rsp) # or disabled.
 mov %rsp, %rcx # La_x86_64_regs pointer to %rcx.
 mov 48(%rbx), %rdx # Load return address if needed.
 mov 40(%rbx), %rsi # Copy args pushed by PLT in register.
 mov 32(%rbx), %rdi # %rdi: link_map, %rsi: reloc_index
 lea 16(%rbx), %r8 # Address of framesize
 call _dl_profile_fixup # Call resolver.
 mov %rax, %r11 # Save return value.
 movq 8(%rbx), %rax # Get back register content.
 movq 0(%rsp), %rdx
 movq 8(%rsp), %r8
 movq 16(%rsp), %r9
 movaps (64)(%rsp), %xmm0
 movaps (64 + 16)(%rsp), %xmm1
 movaps (64 + 16*2)(%rsp), %xmm2
 movaps (64 + 16*3)(%rsp), %xmm3
 movaps (64 + 16*4)(%rsp), %xmm4
 movaps (64 + 16*5)(%rsp), %xmm5
 movaps (64 + 16*6)(%rsp), %xmm6
 movaps (64 + 16*7)(%rsp), %xmm7
 bndmov (704)(%rsp), %bnd0 # Restore bound
 bndmov (704 + 16)(%rsp), %bnd1 # registers.
 bndmov (704 + 16*2)(%rsp), %bnd2
 bndmov (704 + 16*3)(%rsp), %bnd3
 mov 16(%rbx), %r10 # Anything in framesize?
 test %r10, %r10
 bnd
 jns 3f
 movq 24(%rsp), %rcx
 movq 32(%rsp), %rsi
 movq 40(%rsp), %rdi
 mov %rbx, %rsp
 movq (%rsp), %rbx
 add $48, %rsp # Adjust the stack to the return value
    # (eats the reloc index and link_map)
 bnd
 jmp *%r11 # Jump to function address.
3:
 lea 56(%rbx), %rsi # stack
 add $8, %r10
 and $-16, %r10
 mov %r10, %rcx
 sub %r10, %rsp
 mov %rsp, %rdi
 shr $3, %rcx
 rep
 movsq
 movq 24(%rdi), %rcx # Get back register content.
 movq 32(%rdi), %rsi
 movq 40(%rdi), %rdi
 bnd
 call *%r11
 mov 24(%rbx), %rsp # Drop the copied stack content
 sub $240, %rsp # sizeof(La_x86_64_retval)
 mov %rsp, %rcx # La_x86_64_retval argument to %rcx.
 movq %rax, 0(%rcx)
 movq %rdx, 8(%rcx)
 movaps %xmm0, 16(%rcx)
 movaps %xmm1, 32(%rcx)
 bndmov %bnd0, 208(%rcx) # Preserve returned bounds.
 bndmov %bnd1, 224(%rcx)
 fstpt 48(%rcx)
 fstpt 64(%rcx)
 movq 24(%rbx), %rdx # La_x86_64_regs argument to %rdx.
 movq 40(%rbx), %rsi # Copy args pushed by PLT in register.
 movq 32(%rbx), %rdi # %rdi: link_map, %rsi: reloc_index
 call _dl_call_pltexit
 movq 0(%rsp), %rax
 movq 8(%rsp), %rdx
 movaps 16(%rsp), %xmm0
 movaps 32(%rsp), %xmm1
 bndmov 208(%rsp), %bnd0 # Restore bound registers.
 bndmov 224(%rsp), %bnd1
 fldt 64(%rsp)
 fldt 48(%rsp)
 mov %rbx, %rsp
 movq (%rsp), %rbx
 add $48, %rsp # Adjust the stack to the return value
    # (eats the reloc index and link_map)
 bnd
 retq
 .size _dl_runtime_profile_sse, .-_dl_runtime_profile_sse
 .text
 .globl _dl_runtime_resolve_fxsave
 .hidden _dl_runtime_resolve_fxsave
 .type _dl_runtime_resolve_fxsave, @function
 .align 16
_dl_runtime_resolve_fxsave:

 pushq %rbx # push subtracts stack by 8.
 mov %rsp, %rbx
 and $-16, %rsp
 sub $(512 + (8 * 7 + 8)), %rsp
 # Preserve registers otherwise clobbered.
 movq %rax, 0(%rsp)
 movq %rcx, (0 + 8)(%rsp)
 movq %rdx, ((0 + 8) + 8)(%rsp)
 movq %rsi, (((0 + 8) + 8) + 8)(%rsp)
 movq %rdi, ((((0 + 8) + 8) + 8) + 8)(%rsp)
 movq %r8, (((((0 + 8) + 8) + 8) + 8) + 8)(%rsp)
 movq %r9, ((((((0 + 8) + 8) + 8) + 8) + 8) + 8)(%rsp)
 fxsave (8 * 7 + 8)(%rsp)
 # Copy args pushed by PLT in register.
 # %rdi: link_map, %rsi: reloc_index
 mov (8 + 8)(%rbx), %rsi
 mov 8(%rbx), %rdi
 call _dl_fixup # Call resolver.
 mov %rax, %r11 # Save return value
 # Get register content back.
 fxrstor (8 * 7 + 8)(%rsp)
 movq ((((((0 + 8) + 8) + 8) + 8) + 8) + 8)(%rsp), %r9
 movq (((((0 + 8) + 8) + 8) + 8) + 8)(%rsp), %r8
 movq ((((0 + 8) + 8) + 8) + 8)(%rsp), %rdi
 movq (((0 + 8) + 8) + 8)(%rsp), %rsi
 movq ((0 + 8) + 8)(%rsp), %rdx
 movq (0 + 8)(%rsp), %rcx
 movq 0(%rsp), %rax
 mov %rbx, %rsp
 movq (%rsp), %rbx
 # Adjust stack(PLT did 2 pushes)
 add $(8 + 16), %rsp
 # Preserve bound registers.
 bnd
 jmp *%r11 # Jump to function address.
 .size _dl_runtime_resolve_fxsave, .-_dl_runtime_resolve_fxsave
 .text
 .globl _dl_runtime_resolve_xsave
 .hidden _dl_runtime_resolve_xsave
 .type _dl_runtime_resolve_xsave, @function
 .align 16
_dl_runtime_resolve_xsave:

 pushq %rbx # push subtracts stack by 8.
 mov %rsp, %rbx
 and $-64, %rsp
 # Allocate stack space of the required size to save the state.
 sub _rtld_local_ro+104 +288(%rip), %rsp
 # Preserve registers otherwise clobbered.
 movq %rax, 0(%rsp)
 movq %rcx, (0 + 8)(%rsp)
 movq %rdx, ((0 + 8) + 8)(%rsp)
 movq %rsi, (((0 + 8) + 8) + 8)(%rsp)
 movq %rdi, ((((0 + 8) + 8) + 8) + 8)(%rsp)
 movq %r8, (((((0 + 8) + 8) + 8) + 8) + 8)(%rsp)
 movq %r9, ((((((0 + 8) + 8) + 8) + 8) + 8) + 8)(%rsp)
 movl $((1 << 1) | (1 << 2) | (1 << 3) | (1 << 5) | (1 << 6) | (1 << 7)), %eax
 xorl %edx, %edx
 # Clear the XSAVE Header.
 movq %rdx, ((8 * 7 + 8) + 512)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 2)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 3)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 4)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 5)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 6)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 7)(%rsp)
 xsave (8 * 7 + 8)(%rsp)
 # Copy args pushed by PLT in register.
 # %rdi: link_map, %rsi: reloc_index
 mov (8 + 8)(%rbx), %rsi
 mov 8(%rbx), %rdi
 call _dl_fixup # Call resolver.
 mov %rax, %r11 # Save return value
 # Get register content back.
 movl $((1 << 1) | (1 << 2) | (1 << 3) | (1 << 5) | (1 << 6) | (1 << 7)), %eax
 xorl %edx, %edx
 xrstor (8 * 7 + 8)(%rsp)
 movq ((((((0 + 8) + 8) + 8) + 8) + 8) + 8)(%rsp), %r9
 movq (((((0 + 8) + 8) + 8) + 8) + 8)(%rsp), %r8
 movq ((((0 + 8) + 8) + 8) + 8)(%rsp), %rdi
 movq (((0 + 8) + 8) + 8)(%rsp), %rsi
 movq ((0 + 8) + 8)(%rsp), %rdx
 movq (0 + 8)(%rsp), %rcx
 movq 0(%rsp), %rax
 mov %rbx, %rsp
 movq (%rsp), %rbx
 # Adjust stack(PLT did 2 pushes)
 add $(8 + 16), %rsp
 # Preserve bound registers.
 bnd
 jmp *%r11 # Jump to function address.
 .size _dl_runtime_resolve_xsave, .-_dl_runtime_resolve_xsave
 .text
 .globl _dl_runtime_resolve_xsavec
 .hidden _dl_runtime_resolve_xsavec
 .type _dl_runtime_resolve_xsavec, @function
 .align 16
_dl_runtime_resolve_xsavec:

 pushq %rbx # push subtracts stack by 8.
 mov %rsp, %rbx
 and $-64, %rsp
 # Allocate stack space of the required size to save the state.
 sub _rtld_local_ro+104 +288(%rip), %rsp
 # Preserve registers otherwise clobbered.
 movq %rax, 0(%rsp)
 movq %rcx, (0 + 8)(%rsp)
 movq %rdx, ((0 + 8) + 8)(%rsp)
 movq %rsi, (((0 + 8) + 8) + 8)(%rsp)
 movq %rdi, ((((0 + 8) + 8) + 8) + 8)(%rsp)
 movq %r8, (((((0 + 8) + 8) + 8) + 8) + 8)(%rsp)
 movq %r9, ((((((0 + 8) + 8) + 8) + 8) + 8) + 8)(%rsp)
 movl $((1 << 1) | (1 << 2) | (1 << 3) | (1 << 5) | (1 << 6) | (1 << 7)), %eax
 xorl %edx, %edx
 # Clear the XSAVE Header.
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 2)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 3)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 4)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 5)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 6)(%rsp)
 movq %rdx, ((8 * 7 + 8) + 512 + 8 * 7)(%rsp)
 xsavec (8 * 7 + 8)(%rsp)
 # Copy args pushed by PLT in register.
 # %rdi: link_map, %rsi: reloc_index
 mov (8 + 8)(%rbx), %rsi
 mov 8(%rbx), %rdi
 call _dl_fixup # Call resolver.
 mov %rax, %r11 # Save return value
 # Get register content back.
 movl $((1 << 1) | (1 << 2) | (1 << 3) | (1 << 5) | (1 << 6) | (1 << 7)), %eax
 xorl %edx, %edx
 xrstor (8 * 7 + 8)(%rsp)
 movq ((((((0 + 8) + 8) + 8) + 8) + 8) + 8)(%rsp), %r9
 movq (((((0 + 8) + 8) + 8) + 8) + 8)(%rsp), %r8
 movq ((((0 + 8) + 8) + 8) + 8)(%rsp), %rdi
 movq (((0 + 8) + 8) + 8)(%rsp), %rsi
 movq ((0 + 8) + 8)(%rsp), %rdx
 movq (0 + 8)(%rsp), %rcx
 movq 0(%rsp), %rax
 mov %rbx, %rsp
 movq (%rsp), %rbx
 # Adjust stack(PLT did 2 pushes)
 add $(8 + 16), %rsp
 # Preserve bound registers.
 bnd
 jmp *%r11 # Jump to function address.
 .size _dl_runtime_resolve_xsavec, .-_dl_runtime_resolve_xsavec
