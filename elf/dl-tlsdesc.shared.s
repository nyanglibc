 .text
 .hidden _dl_tlsdesc_return
 .global _dl_tlsdesc_return
 .type _dl_tlsdesc_return,@function
 .align 16
_dl_tlsdesc_return:

 movq 8(%rax), %rax
 ret
 .size _dl_tlsdesc_return, .-_dl_tlsdesc_return
 .hidden _dl_tlsdesc_undefweak
 .global _dl_tlsdesc_undefweak
 .type _dl_tlsdesc_undefweak,@function
 .align 16
_dl_tlsdesc_undefweak:

 movq 8(%rax), %rax
 subq %fs:0, %rax
 ret
 .size _dl_tlsdesc_undefweak, .-_dl_tlsdesc_undefweak
 .hidden _dl_tlsdesc_dynamic
 .global _dl_tlsdesc_dynamic
 .type _dl_tlsdesc_dynamic,@function
 .align 16
_dl_tlsdesc_dynamic:

 movq %rsi, -16(%rsp)
 movq %fs:8, %rsi
 movq %rdi, -8(%rsp)
 movq 8(%rax), %rdi
 movq (%rsi), %rax
 cmpq %rax, 16(%rdi)
 ja .Lslow
 movq 0(%rdi), %rax
 salq $4, %rax
 movq (%rax,%rsi), %rax
 cmpq $-1, %rax
 je .Lslow
 addq 8(%rdi), %rax
.Lret:
 movq -16(%rsp), %rsi
 subq %fs:0, %rax
 movq -8(%rsp), %rdi
 ret
.Lslow:
 subq $72, %rsp
 movq %rdx, 8(%rsp)
 movq %rcx, 16(%rsp)
 movq %r8, 24(%rsp)
 movq %r9, 32(%rsp)
 movq %r10, 40(%rsp)
 movq %r11, 48(%rsp)
 call __GI___tls_get_addr
 movq 8(%rsp), %rdx
 movq 16(%rsp), %rcx
 movq 24(%rsp), %r8
 movq 32(%rsp), %r9
 movq 40(%rsp), %r10
 movq 48(%rsp), %r11
 addq $72, %rsp
 jmp .Lret
 .size _dl_tlsdesc_dynamic, .-_dl_tlsdesc_dynamic
 .hidden _dl_tlsdesc_resolve_rela
 .global _dl_tlsdesc_resolve_rela
 .type _dl_tlsdesc_resolve_rela,@function
 .align 16
_dl_tlsdesc_resolve_rela:

 subq $80, %rsp
 movq %rax, (%rsp)
 movq %rdi, 8(%rsp)
 movq %rax, %rdi
 movq %rsi, 16(%rsp)
 movq 80(%rsp), %rsi
 movq %r8, 24(%rsp)
 movq %r9, 32(%rsp)
 movq %r10, 40(%rsp)
 movq %r11, 48(%rsp)
 movq %rdx, 56(%rsp)
 movq %rcx, 64(%rsp)
 call _dl_tlsdesc_resolve_rela_fixup
 movq (%rsp), %rax
 movq 8(%rsp), %rdi
 movq 16(%rsp), %rsi
 movq 24(%rsp), %r8
 movq 32(%rsp), %r9
 movq 40(%rsp), %r10
 movq 48(%rsp), %r11
 movq 56(%rsp), %rdx
 movq 64(%rsp), %rcx
 addq $88, %rsp
 jmp *(%rax)
 .size _dl_tlsdesc_resolve_rela, .-_dl_tlsdesc_resolve_rela
 .hidden _dl_tlsdesc_resolve_hold
 .global _dl_tlsdesc_resolve_hold
 .type _dl_tlsdesc_resolve_hold,@function
 .align 16
_dl_tlsdesc_resolve_hold:
0:

 subq $72, %rsp
 movq %rax, (%rsp)
 movq %rdi, 8(%rsp)
 movq %rax, %rdi
 movq %rsi, 16(%rsp)
 leaq . - _dl_tlsdesc_resolve_hold(%rip), %rsi
 movq %r8, 24(%rsp)
 movq %r9, 32(%rsp)
 movq %r10, 40(%rsp)
 movq %r11, 48(%rsp)
 movq %rdx, 56(%rsp)
 movq %rcx, 64(%rsp)
 call _dl_tlsdesc_resolve_hold_fixup
1:
 movq (%rsp), %rax
 movq 8(%rsp), %rdi
 movq 16(%rsp), %rsi
 movq 24(%rsp), %r8
 movq 32(%rsp), %r9
 movq 40(%rsp), %r10
 movq 48(%rsp), %r11
 movq 56(%rsp), %rdx
 movq 64(%rsp), %rcx
 addq $72, %rsp
 jmp *(%rax)
 .size _dl_tlsdesc_resolve_hold, .-_dl_tlsdesc_resolve_hold
