	.text
	.p2align 4,,15
	.globl	_dl_add_to_namespace_list
	.hidden	_dl_add_to_namespace_list
	.type	_dl_add_to_namespace_list, @function
_dl_add_to_namespace_list:
	pushq	%r12
	pushq	%rbp
	leaq	_rtld_local(%rip), %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %r12
	leaq	2480(%rbp), %rdi
	call	*3984+_rtld_local(%rip)
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rax
	leaq	0(%rbp,%rax,8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L3
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rax, %rdx
.L3:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L5
	movq	%rdx, 32(%r12)
	movq	%r12, 24(%rdx)
.L4:
	leaq	(%rbx,%rbx,8), %rax
	leaq	2480+_rtld_local(%rip), %rdi
	leaq	(%rbx,%rax,2), %rax
	addl	$1, 8(%rbp,%rax,8)
	movq	2520+_rtld_local(%rip), %rax
	popq	%rbx
	popq	%rbp
	movq	%rax, 1152(%r12)
	addq	$1, %rax
	popq	%r12
	movq	%rax, 2520+_rtld_local(%rip)
	jmp	*3992+_rtld_local(%rip)
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, (%rax)
	jmp	.L4
	.size	_dl_add_to_namespace_list, .-_dl_add_to_namespace_list
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"dl-object.c"
.LC2:
	.string	"type == lt_executable"
.LC3:
	.string	"nsid == LM_ID_BASE"
	.text
	.p2align 4,,15
	.globl	_dl_new_object
	.hidden	_dl_new_object
	.type	_dl_new_object, @function
_dl_new_object:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %eax
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %r13
	movq	%rdi, %rbx
	subq	$56, %rsp
	andl	$536870912, %eax
	movq	%rsi, 8(%rsp)
	movl	%edx, 16(%rsp)
	movl	%r8d, 44(%rsp)
	movl	%eax, 40(%rsp)
	jne	.L70
	movq	8(%rsp), %rdi
	movl	800+_rtld_local_ro(%rip), %r12d
	call	strlen
	movl	%r12d, %r10d
	leaq	1(%rax), %rdx
	movq	%rax, %rbp
	salq	$4, %r10
	leaq	1193(%r10,%rax), %rdi
.L12:
	movq	%rdx, 32(%rsp)
	movq	%r10, 24(%rsp)
	movl	$1, %esi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L9
	movq	24(%rsp), %r10
	movq	%rax, 40(%r14)
	movq	32(%rsp), %rdx
	movq	8(%rsp), %rsi
	leaq	1160(%rax,%r10), %r10
	movq	%r10, 720(%rax)
	leaq	8(%r10), %rax
	leaq	32(%r10), %rdi
	movq	%r10, 24(%rsp)
	movq	%rax, 56(%r14)
	call	memcpy@PLT
	movq	24(%rsp), %r10
	movq	%rax, 8(%r10)
	movl	$1, 24(%r10)
	cmpb	$0, (%rbx)
	je	.L14
	movl	40(%rsp), %edx
	testl	%edx, %edx
	je	.L71
.L14:
	addq	%rbp, %rax
	movq	%rax, 8(%r14)
.L15:
	movzbl	796(%r14), %eax
	movzbl	16(%rsp), %ebp
	andl	$-4, %eax
	andl	$3, %ebp
	orl	%eax, %ebp
	testb	$1, 1+_rtld_local_ro(%rip)
	movb	%bpl, 796(%r14)
	je	.L72
.L16:
	testl	%r12d, %r12d
	movq	%r15, 736(%r14)
	movq	%r13, 48(%r14)
	je	.L17
	leal	-1(%r12), %r11d
	leaq	2568+_rtld_local(%rip), %rdi
	leaq	1160(%r14), %rsi
	xorl	%eax, %eax
	addq	$1, %r11
	leaq	1160(%rdi), %rbp
	salq	$4, %r11
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	(%rsi,%rax), %rdx
	leaq	0(%rbp,%rax), %r10
	cmpq	%rdi, %r14
	cmove	%r10, %rdx
	addq	$16, %rax
	cmpq	%rax, %r11
	movq	%r14, (%rdx)
	jne	.L20
.L17:
	leaq	0(%r13,%r13,8), %rax
	leaq	880(%r14), %rdx
	movq	$4, 912(%r14)
	leaq	0(%r13,%rax,2), %rsi
	leaq	_rtld_local(%rip), %rax
	movq	%rdx, 920(%r14)
	movq	(%rax,%rsi,8), %rax
	testq	%rax, %rax
	je	.L73
	addq	$704, %rax
	testq	%r15, %r15
	movl	$1, %esi
	movq	%rax, 880(%r14)
	jne	.L25
	movq	%r14, %r15
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rax, %r15
.L25:
	movq	736(%r15), %rax
	testq	%rax, %rax
	jne	.L41
	testl	%esi, %esi
	jne	.L74
.L22:
	leaq	704(%r15), %rcx
.L36:
	movq	%rcx, (%rdx)
.L27:
	leaq	704(%r14), %rax
	movq	%rax, 928(%r14)
	movzbl	(%rbx), %ebp
	testb	%bpl, %bpl
	je	.L9
	movq	%rbx, %rdi
	call	strlen
	movq	%rax, %r13
	leaq	1(%rax), %rax
	cmpb	$47, %bpl
	movq	%rax, 16(%rsp)
	jne	.L42
	movq	%rax, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r12
	movq	%rax, %rdi
	je	.L75
.L30:
	movq	16(%rsp), %rdx
	movq	%rbx, %rsi
	call	__mempcpy@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rdx, %rax
.L33:
	cmpb	$47, -1(%rax)
	leaq	-1(%rax), %rdx
	jne	.L44
	cmpq	%rdx, %r12
	cmove	%rax, %rdx
	movb	$0, (%rdx)
.L29:
	movq	%r12, 848(%r14)
.L9:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$1, 996(%r14)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L74:
	movq	880(%r14), %rax
.L24:
	leaq	704(%r15), %rcx
	cmpq	%rax, %rcx
	je	.L27
	testb	$8, 44(%rsp)
	je	.L76
	movq	%rax, 888(%r14)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rbx, 8(%r14)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%esi, %esi
	testq	%r15, %r15
	jne	.L25
	movq	%r14, %r15
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L42:
	movq	16(%rsp), %r15
	movq	$0, 8(%rsp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	127(%r15), %rsi
	movq	%rax, %rdi
	subq	%r13, %rsi
	call	__getcwd
	testq	%rax, %rax
	jne	.L32
	cmpl	$34, rtld_errno(%rip)
	movq	%r12, 8(%rsp)
	movq	%rbp, %r15
	jne	.L31
.L28:
	leaq	128(%r15), %rbp
	movq	8(%rsp), %rdi
	movq	%rbp, %rsi
	call	*__rtld_realloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L77
.L31:
	movq	8(%rsp), %rdi
	movq	$-1, %r12
	call	*__rtld_free(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L75:
	movq	$-1, %r12
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	888(%r14), %rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L70:
	testl	%edx, %edx
	jne	.L78
	testq	%r9, %r9
	jne	.L79
	leaq	.LC0(%rip), %rax
	movl	$256, %r10d
	movl	$1, %edx
	movl	$1449, %edi
	xorl	%ebp, %ebp
	movl	$16, %r12d
	movq	%rax, 8(%rsp)
	jmp	.L12
.L32:
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	(%r12,%rax), %rdi
	cmpb	$47, -1(%rdi)
	je	.L30
	movb	$47, (%rdi)
	addq	$1, %rdi
	jmp	.L30
.L79:
	leaq	__PRETTY_FUNCTION__.9866(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$65, %edx
	call	__GI___assert_fail
.L78:
	leaq	__PRETTY_FUNCTION__.9866(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$64, %edx
	call	__GI___assert_fail
	.size	_dl_new_object, .-_dl_new_object
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.9866, @object
	.size	__PRETTY_FUNCTION__.9866, 15
__PRETTY_FUNCTION__.9866:
	.string	"_dl_new_object"
	.hidden	__rtld_free
	.hidden	__rtld_realloc
	.hidden	rtld_errno
	.hidden	__getcwd
	.hidden	__rtld_malloc
	.hidden	__rtld_calloc
	.hidden	strlen
	.hidden	_rtld_local_ro
	.hidden	_rtld_local
