	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
.LC1:
	.string	"\nconflict processing: %s\n"
.LC2:
	.string	"dl-conflict.c"
.LC3:
	.string	"l->l_ns == LM_ID_BASE"
	.text
	.p2align 4,,15
	.globl	_dl_resolve_conflicts
	.hidden	_dl_resolve_conflicts
	.type	_dl_resolve_conflicts, @function
_dl_resolve_conflicts:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	testb	$32, _rtld_local_ro(%rip)
	jne	.L29
.L2:
	cmpq	$0, 48(%r14)
	jne	.L30
	movq	%r13, %rax
	movabsq	$-6148914691236517205, %rdx
	subq	%rbx, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	addq	%rax, 2552+_rtld_local(%rip)
	cmpq	%rbx, %r13
	jbe	.L1
	leaq	.L12(%rip), %r12
	movl	$4294967295, %r15d
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%rbx), %rsi
	movq	(%rbx), %rbp
	movl	%esi, %eax
	cmpq	$8, %rax
	je	.L27
	cmpq	$38, %rax
	je	.L27
	testq	%rax, %rax
	je	.L7
	cmpq	$37, %rax
	ja	.L10
	movslq	(%r12,%rax,4), %rax
	addq	%r12, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L12:
	.long	.L10-.L12
	.long	.L11-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L11-.L12
	.long	.L11-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L14-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L15-.L12
	.long	.L15-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L10-.L12
	.long	.L17-.L12
	.text
	.p2align 4,,10
	.p2align 3
.L11:
	movq	16(%rbx), %rax
	movq	%rax, 0(%rbp)
.L7:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	ja	.L18
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	16, %rax
	ud2
	.p2align 4,,10
	.p2align 3
.L14:
	movq	16(%rbx), %rax
	cmpq	%r15, %rax
	movl	%eax, 0(%rbp)
	jbe	.L7
	movl	0, %eax
	ud2
	.p2align 4,,10
	.p2align 3
.L17:
	movq	16(%rbx), %rax
	addq	(%r14), %rax
	call	*%rax
	movq	%rax, 0(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L27:
	movq	16(%rbx), %rax
	addq	(%r14), %rax
	movq	%rax, 0(%rbp)
	jmp	.L7
.L29:
	movq	8(%rdi), %rsi
	cmpb	$0, (%rsi)
	jne	.L3
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L3:
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L2
.L10:
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	_dl_reloc_bad_type
.L30:
	leaq	__PRETTY_FUNCTION__.10028(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$55, %edx
	call	__GI___assert_fail
	.size	_dl_resolve_conflicts, .-_dl_resolve_conflicts
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10028, @object
	.size	__PRETTY_FUNCTION__.10028, 22
__PRETTY_FUNCTION__.10028:
	.string	"_dl_resolve_conflicts"
	.hidden	_dl_reloc_bad_type
	.hidden	_dl_debug_printf
	.hidden	_rtld_local
	.hidden	_rtld_local_ro
