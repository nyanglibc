	.text
	.p2align 4,,15
	.type	allocate_dtv, @function
allocate_dtv:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$16, %esi
	subq	$8, %rsp
	movq	_dl_tls_max_dtv_idx(%rip), %rdi
	leaq	14(%rdi), %rbp
	addq	$16, %rdi
	call	calloc@PLT
	testq	%rax, %rax
	je	.L3
	leaq	16(%rax), %rdx
	movq	%rbp, (%rax)
	movq	%rbx, %rax
	movq	%rdx, 8(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	jmp	.L1
	.size	allocate_dtv, .-allocate_dtv
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Failed loading %lu audit modules, %lu are supported.\n"
	.text
	.p2align 4,,15
	.globl	_dl_tls_static_surplus_init
	.hidden	_dl_tls_static_surplus_init
	.type	_dl_tls_static_surplus_init, @function
_dl_tls_static_surplus_init:
	pushq	%r12
	pushq	%rbp
	xorl	%edx, %edx
	pushq	%rbx
	movq	%rdi, %rbp
	xorl	%edi, %edi
	subq	$16, %rsp
	leaq	8(%rsp), %r12
	movq	%r12, %rsi
	call	__tunable_get_val@PLT
	movq	8(%rsp), %rbx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$28, %edi
	call	__tunable_get_val@PLT
	testq	%rbx, %rbx
	movl	$1, %edx
	movq	8(%rsp), %rcx
	cmovne	%rdx, %rbx
	subq	%rbx, %rdx
	cmpq	%rbp, %rdx
	jb	.L9
	addq	%rbp, %rbx
	movq	%rcx, _dl_tls_static_optional(%rip)
	leal	-1(%rbx,%rbx), %eax
	leal	(%rax,%rax,8), %eax
	sall	$4, %eax
	leal	144(%rax,%rcx), %eax
	cltq
	movq	%rax, _dl_tls_static_surplus(%rip)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L9:
	leaq	.LC0(%rip), %rdi
	movq	%rbp, %rsi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
	.size	_dl_tls_static_surplus_init, .-_dl_tls_static_surplus_init
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"../elf/dl-tls.c"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"result <= GL(dl_tls_max_dtv_idx) + 1"
	.align 8
.LC3:
	.string	"result == GL(dl_tls_max_dtv_idx) + 1"
	.text
	.p2align 4,,15
	.globl	_dl_next_tls_modid
	.hidden	_dl_next_tls_modid
	.type	_dl_next_tls_modid, @function
_dl_next_tls_modid:
	cmpb	$0, _dl_tls_dtv_gaps(%rip)
	movq	_dl_tls_max_dtv_idx(%rip), %rsi
	jne	.L11
	leaq	1(%rsi), %rax
	movq	%rax, _dl_tls_max_dtv_idx(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$8, %rsp
	movq	_dl_tls_static_nelem(%rip), %rax
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rdx
	ja	.L13
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rcx
	leaq	1(%rsi), %r9
	movq	%rdx, %rax
	xorl	%edi, %edi
	movq	(%rcx), %r8
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%r8, %rdx
	jnb	.L27
	addq	$1, %rdx
	salq	$4, %rdx
	cmpq	$0, 8(%rcx,%rdx)
	je	.L15
	addq	$1, %rax
	cmpq	%rax, %r9
	jnb	.L14
	leaq	__PRETTY_FUNCTION__.10223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$152, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L27:
	movq	8(%rcx), %rcx
	addq	%r8, %rdi
	testq	%rcx, %rcx
	je	.L15
	movq	(%rcx), %r8
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	%rsi, %rax
	jbe	.L10
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	1(%rsi), %rax
	cmpq	%rdx, %rax
	jne	.L28
.L12:
	movb	$0, _dl_tls_dtv_gaps(%rip)
	movq	%rax, _dl_tls_max_dtv_idx(%rip)
.L10:
	addq	$8, %rsp
	ret
.L28:
	leaq	__PRETTY_FUNCTION__.10223(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$166, %edx
	call	__assert_fail
	.size	_dl_next_tls_modid, .-_dl_next_tls_modid
	.p2align 4,,15
	.globl	_dl_count_modids
	.hidden	_dl_count_modids
	.type	_dl_count_modids, @function
_dl_count_modids:
	cmpb	$0, _dl_tls_dtv_gaps(%rip)
	jne	.L30
	movq	_dl_tls_max_dtv_idx(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L44
	.p2align 4,,10
	.p2align 3
.L32:
	movq	(%rdi), %rsi
	xorl	%edx, %edx
	leaq	24(%rdi), %rcx
	testq	%rsi, %rsi
	je	.L36
	.p2align 4,,10
	.p2align 3
.L34:
	cmpq	$1, (%rcx)
	sbbq	$-1, %rax
	addq	$1, %rdx
	addq	$16, %rcx
	cmpq	%rsi, %rdx
	jne	.L34
.L36:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L32
	rep ret
.L44:
	rep ret
	.size	_dl_count_modids, .-_dl_count_modids
	.p2align 4,,15
	.globl	_dl_get_tls_static_info
	.type	_dl_get_tls_static_info, @function
_dl_get_tls_static_info:
	movq	_dl_tls_static_size(%rip), %rax
	movq	%rax, (%rdi)
	movq	_dl_tls_static_align(%rip), %rax
	movq	%rax, (%rsi)
	ret
	.size	_dl_get_tls_static_info, .-_dl_get_tls_static_info
	.p2align 4,,15
	.globl	_dl_allocate_tls_storage
	.hidden	_dl_allocate_tls_storage
	.type	_dl_allocate_tls_storage, @function
_dl_allocate_tls_storage:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	movq	_dl_tls_static_size(%rip), %r12
	movq	_dl_tls_static_align(%rip), %rbx
	leaq	8(%r12,%rbx), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L48
	movq	%rax, %rbp
	leaq	-1(%rax,%rbx), %rax
	xorl	%edx, %edx
	divq	%rbx
	imulq	%rbx, %rax
	leaq	-2496(%r12,%rax), %rdx
	xorl	%eax, %eax
	leaq	8(%rdx), %rdi
	movq	%rdx, %rcx
	movq	$0, (%rdx)
	movq	$0, 2488(%rdx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2496, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rbp, 2496(%rdx)
	movq	%rdx, %rdi
	call	allocate_dtv
	testq	%rax, %rax
	je	.L50
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rbp, %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	_dl_allocate_tls_storage, .-_dl_allocate_tls_storage
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"cannot allocate memory for thread-local data: ABORT\n"
	.align 8
.LC5:
	.string	"listp->slotinfo[cnt].gen <= GL(dl_tls_generation)"
	.align 8
.LC6:
	.string	"map->l_tls_modid == total + cnt"
	.align 8
.LC7:
	.string	"map->l_tls_blocksize >= map->l_tls_initimage_size"
	.align 8
.LC8:
	.string	"(size_t) map->l_tls_offset >= map->l_tls_blocksize"
	.section	.rodata.str1.1
.LC9:
	.string	"listp != NULL"
	.text
	.p2align 4,,15
	.globl	_dl_allocate_tls_init
	.type	_dl_allocate_tls_init, @function
_dl_allocate_tls_init:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	testq	%rdi, %rdi
	movq	%rdi, 40(%rsp)
	je	.L52
	movq	8(%rdi), %rax
	movq	_dl_tls_max_dtv_idx(%rip), %rsi
	cmpq	%rsi, -16(%rax)
	movq	%rax, 32(%rsp)
	jb	.L83
.L53:
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rbp
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%ecx, %ecx
	testq	%rax, %rax
	sete	%cl
	cmpq	0(%rbp), %rcx
	leaq	(%rax,%rcx), %rbx
	movq	%rcx, 16(%rsp)
	movq	%rbx, 8(%rsp)
	jnb	.L68
	cmpq	%rsi, %rbx
	ja	.L59
	movq	%rcx, %rdx
	leaq	1(%rax,%rcx), %r13
	salq	$4, %rdx
	leaq	24(%rbp,%rdx), %r12
	subq	%rbx, %r13
	movq	%r13, 24(%rsp)
	movq	%r12, %r14
	movq	%rbx, %r12
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$16, %r14
	cmpq	%rsi, %r12
	ja	.L59
.L60:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L61
	movq	-8(%r14), %rdx
	cmpq	_dl_tls_generation(%rip), %rdx
	ja	.L84
	movq	1120(%rax), %r10
	cmpq	%rdx, %r15
	movq	1112(%rax), %rdi
	cmovb	%rdx, %r15
	movq	%r10, %rdx
	leaq	1(%rdi), %r11
	salq	$4, %rdx
	addq	32(%rsp), %rdx
	cmpq	$1, %r11
	movq	$-1, (%rdx)
	movq	$0, 8(%rdx)
	jbe	.L61
	cmpq	%r12, %r10
	jne	.L85
	movq	1088(%rax), %rbx
	movq	1080(%rax), %r13
	cmpq	%r13, %rbx
	jb	.L86
	cmpq	%rbx, %rdi
	jb	.L87
	movq	40(%rsp), %rcx
	movq	1072(%rax), %rsi
	subq	%r13, %rbx
	subq	%rdi, %rcx
	movq	%rcx, %rdi
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	_dl_tls_max_dtv_idx(%rip), %rsi
.L61:
	movq	16(%rsp), %rax
	addq	$1, %rax
	subq	8(%rsp), %rax
	addq	%r12, %rax
	addq	24(%rsp), %r12
	cmpq	0(%rbp), %rax
	jb	.L66
	movq	%r12, %rax
.L58:
	cmpq	%rsi, %rax
	jnb	.L59
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.L67
	leaq	__PRETTY_FUNCTION__.10282(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$597, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L68:
	movq	8(%rsp), %rax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L59:
	movq	32(%rsp), %rax
	movq	%r15, (%rax)
.L52:
	movq	40(%rsp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L83:
	movq	_dl_tls_max_dtv_idx(%rip), %rax
	movq	32(%rsp), %rsi
	leaq	16+_dl_static_dtv(%rip), %rdx
	leaq	14(%rax), %rbx
	addq	$16, %rax
	movq	-16(%rsi), %rbp
	salq	$4, %rax
	cmpq	%rdx, %rsi
	je	.L88
	movq	32(%rsp), %rdi
	movq	%rax, %rsi
	subq	$16, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L57
	leaq	2(%rbp), %r12
	salq	$4, %r12
.L56:
	movq	%rbx, 0(%r13)
	subq	%rbp, %rbx
	leaq	0(%r13,%r12), %rdi
	movq	%rbx, %rdx
	xorl	%esi, %esi
	salq	$4, %rdx
	call	memset@PLT
	movq	40(%rsp), %rsi
	leaq	16(%r13), %rax
	movq	%rax, 32(%rsp)
	movq	%rax, 8(%rsi)
	movq	_dl_tls_max_dtv_idx(%rip), %rsi
	jmp	.L53
.L84:
	leaq	__PRETTY_FUNCTION__.10282(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$561, %edx
	call	__assert_fail
.L88:
	movq	%rax, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L57
	leaq	2(%rbp), %r12
	leaq	_dl_static_dtv(%rip), %rsi
	movq	%rax, %rdi
	salq	$4, %r12
	movq	%r12, %rdx
	call	memcpy@PLT
	jmp	.L56
.L86:
	leaq	__PRETTY_FUNCTION__.10282(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$572, %edx
	call	__assert_fail
.L87:
	leaq	__PRETTY_FUNCTION__.10282(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$574, %edx
	call	__assert_fail
.L85:
	leaq	__PRETTY_FUNCTION__.10282(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$571, %edx
	call	__assert_fail
.L57:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
	.size	_dl_allocate_tls_init, .-_dl_allocate_tls_init
	.p2align 4,,15
	.globl	_dl_allocate_tls
	.type	_dl_allocate_tls, @function
_dl_allocate_tls:
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L93
	call	allocate_dtv
	addq	$8, %rsp
	movq	%rax, %rdi
	jmp	_dl_allocate_tls_init
	.p2align 4,,10
	.p2align 3
.L93:
	call	_dl_allocate_tls_storage
	addq	$8, %rsp
	movq	%rax, %rdi
	jmp	_dl_allocate_tls_init
	.size	_dl_allocate_tls, .-_dl_allocate_tls
	.p2align 4,,15
	.globl	_dl_deallocate_tls
	.type	_dl_deallocate_tls, @function
_dl_deallocate_tls:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rbp
	cmpq	$0, -16(%rbp)
	je	.L95
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$1, %rbx
	movq	%rbx, %rax
	salq	$4, %rax
	movq	8(%rbp,%rax), %rdi
	call	free@PLT
	cmpq	-16(%rbp), %rbx
	jb	.L96
.L95:
	leaq	16+_dl_static_dtv(%rip), %rax
	cmpq	%rax, %rbp
	je	.L97
	leaq	-16(%rbp), %rdi
	call	free@PLT
.L97:
	testb	%r13b, %r13b
	jne	.L101
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movq	2496(%r12), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	free@PLT
	.size	_dl_deallocate_tls, .-_dl_deallocate_tls
	.p2align 4,,15
	.globl	_dl_tls_get_addr_soft
	.hidden	_dl_tls_get_addr_soft
	.type	_dl_tls_get_addr_soft, @function
_dl_tls_get_addr_soft:
	movq	1120(%rdi), %rax
	testq	%rax, %rax
	je	.L110
#APP
# 930 "../elf/dl-tls.c" 1
	movq %fs:8,%rdx
# 0 "" 2
#NO_APP
	movq	(%rdx), %rcx
	cmpq	_dl_tls_generation(%rip), %rcx
	jne	.L112
.L104:
	salq	$4, %rax
	movq	(%rdx,%rax), %rax
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	cmpq	-16(%rdx), %rax
	jnb	.L110
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %r8
	cmpq	%r8, %rax
	jb	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	movq	8(%rdi), %rdi
	subq	%r8, %rsi
	movq	(%rdi), %r8
	cmpq	%rsi, %r8
	jbe	.L106
.L105:
	addq	$1, %rsi
	salq	$4, %rsi
	cmpq	(%rdi,%rsi), %rcx
	jnb	.L104
.L110:
	xorl	%eax, %eax
	ret
	.size	_dl_tls_get_addr_soft, .-_dl_tls_get_addr_soft
	.section	.rodata.str1.1
.LC10:
	.string	"idx == 0"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"cannot create TLS data structures"
	.section	.rodata.str1.1
.LC12:
	.string	"dlopen"
	.text
	.p2align 4,,15
	.globl	_dl_add_to_slotinfo
	.hidden	_dl_add_to_slotinfo
	.type	_dl_add_to_slotinfo, @function
_dl_add_to_slotinfo:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	movq	1120(%rdi), %rbx
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rbp
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L126:
	subq	%rax, %rbx
	movq	8(%rbp), %rax
	testq	%rax, %rax
	je	.L125
	movq	%rax, %rbp
.L115:
	movq	0(%rbp), %rax
	cmpq	%rbx, %rax
	jbe	.L126
.L114:
	testb	%sil, %sil
	je	.L113
	movq	_dl_tls_generation(%rip), %rax
	salq	$4, %rbx
	addq	%rbp, %rbx
	movq	%r12, 24(%rbx)
	addq	$1, %rax
	movq	%rax, 16(%rbx)
.L113:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	testq	%rbx, %rbx
	jne	.L127
	movl	$1008, %edi
	movl	%esi, 12(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, 8(%rbp)
	movl	12(%rsp), %esi
	je	.L128
	leaq	16(%rax), %r8
	movq	$62, (%rax)
	movq	$0, 8(%rax)
	movl	$124, %ecx
	movq	%rbx, %rax
	movq	%rdx, %rbp
	movq	%r8, %rdi
	rep stosq
	jmp	.L114
.L127:
	leaq	__PRETTY_FUNCTION__.10322(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$994, %edx
	call	__assert_fail
.L128:
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	movl	$12, %edi
	addq	$1, _dl_tls_generation(%rip)
	call	_dl_signal_error
	.size	_dl_add_to_slotinfo, .-_dl_add_to_slotinfo
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10322, @object
	.size	__PRETTY_FUNCTION__.10322, 20
__PRETTY_FUNCTION__.10322:
	.string	"_dl_add_to_slotinfo"
	.align 16
	.type	__PRETTY_FUNCTION__.10282, @object
	.size	__PRETTY_FUNCTION__.10282, 22
__PRETTY_FUNCTION__.10282:
	.string	"_dl_allocate_tls_init"
	.align 16
	.type	__PRETTY_FUNCTION__.10223, @object
	.size	__PRETTY_FUNCTION__.10223, 19
__PRETTY_FUNCTION__.10223:
	.string	"_dl_next_tls_modid"
	.hidden	_dl_signal_error
	.hidden	__assert_fail
