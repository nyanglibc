	.text
	.p2align 4,,15
	.globl	_dl_make_tlsdesc_dynamic
	.hidden	_dl_make_tlsdesc_dynamic
	.type	_dl_make_tlsdesc_dynamic, @function
_dl_make_tlsdesc_dynamic:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	2440+_rtld_local(%rip), %rdi
	subq	$24, %rsp
	call	*3984+_rtld_local(%rip)
	movq	1032(%r13), %r14
	testq	%r14, %r14
	je	.L40
.L2:
	movq	8(%r14), %rbp
	movq	16(%r14), %rdi
	movq	1120(%r13), %r8
	movq	(%r14), %r9
	leaq	0(%rbp,%rbp,2), %rdx
	leaq	0(,%rdi,4), %rax
	cmpq	%rax, %rdx
	jbe	.L41
.L6:
	movslq	%r15d, %rsi
	xorl	%edx, %edx
	movq	%rsi, %rax
	divq	%rbp
	movl	%edx, %eax
	leaq	(%r9,%rax,8), %r12
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.L14
	cmpq	8(%rbx), %r15
	je	.L15
	leaq	-2(%rbp), %rdi
	movl	%edx, %ecx
	movq	%rsi, %rax
	xorl	%edx, %edx
	divq	%rdi
	addl	$1, %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L42:
	cmpq	8(%rbx), %r15
	je	.L15
.L17:
	leal	(%rcx,%rdx), %esi
	cmpq	%rbp, %rsi
	movq	%rsi, %rcx
	jb	.L16
	subl	%ebp, %ecx
	movl	%ecx, %esi
.L16:
	leaq	(%r9,%rsi,8), %r12
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	jne	.L42
.L14:
	addq	$1, 16(%r14)
	movq	%r8, (%rsp)
	movl	$24, %edi
	call	*__rtld_malloc(%rip)
	movq	1120(%r13), %rdx
	movq	%rax, %rbx
	movq	%rax, (%r12)
	movq	(%rsp), %r8
	movq	4032+_rtld_local(%rip), %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%rax), %rax
	subq	%rcx, %rdx
	testq	%rax, %rax
	je	.L19
.L21:
	movq	(%rax), %rcx
	cmpq	%rdx, %rcx
	jbe	.L18
	salq	$4, %rdx
	addq	%rdx, %rax
	cmpq	24(%rax), %r13
	je	.L43
.L19:
	movq	4088+_rtld_local(%rip), %rax
	addq	$1, %rax
.L20:
	movq	%rax, 16(%rbx)
	movq	%r8, (%rbx)
	leaq	2440+_rtld_local(%rip), %rdi
	movq	%r15, 8(%rbx)
	call	*3992+_rtld_local(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
.L1:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$32, %edi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L8
	movq	$3, 8(%rax)
	movl	$24, %edi
	call	*__rtld_malloc(%rip)
	movq	__rtld_free(%rip), %rdx
	testq	%rax, %rax
	movq	%rax, (%r14)
	movq	%rdx, 24(%r14)
	je	.L44
	movq	8(%r14), %rdi
	movq	$0, 16(%r14)
	xorl	%esi, %esi
	leaq	0(,%rdi,8), %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	%r14, 1032(%r13)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r14, %rdi
	call	*%rdx
.L8:
	leaq	2440+_rtld_local(%rip), %rdi
	xorl	%ebx, %ebx
	call	*3992+_rtld_local(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L41:
	addq	%rdi, %rdi
	movq	%rbp, %rbx
	cmpq	%rdi, %rbp
	jb	.L45
.L7:
	movq	%r9, 8(%rsp)
	movq	%r8, (%rsp)
	movq	%rbx, %rsi
	movl	$8, %edi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	je	.L8
	movq	8(%rsp), %r9
	movq	(%rsp), %r8
	leaq	-2(%rbx), %r11
	movq	%rax, (%r14)
	movq	%rbx, 8(%r14)
	leaq	(%r9,%rbp,8), %rbp
	movq	%r9, %rdi
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rdi), %r10
	testq	%r10, %r10
	je	.L9
	movslq	8(%r10), %r12
	xorl	%edx, %edx
	movq	(%r14), %rsi
	movq	%r12, %rax
	divq	%rbx
	movl	%edx, %eax
	leaq	(%rsi,%rax,8), %rax
	cmpq	$0, (%rax)
	je	.L10
	movl	%edx, %ecx
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%r11
	addl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L12:
	leal	(%rcx,%rdx), %eax
	cmpq	%rax, %rbx
	movq	%rax, %rcx
	ja	.L11
	subl	%ebx, %ecx
	movl	%ecx, %eax
.L11:
	leaq	(%rsi,%rax,8), %rax
	cmpq	$0, (%rax)
	jne	.L12
.L10:
	movq	%r10, (%rax)
.L9:
	addq	$8, %rdi
	cmpq	%rdi, %rbp
	ja	.L13
	movq	%r8, (%rsp)
	movq	%r9, %rdi
	call	*24(%r14)
	movq	__rtld_free(%rip), %rax
	movq	8(%r14), %rbp
	movq	(%r14), %r9
	movq	(%rsp), %r8
	movq	%rax, 24(%r14)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L43:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L20
	jmp	.L19
.L45:
	movq	%r9, 8(%rsp)
	movq	%r8, (%rsp)
	call	_dl_higher_prime_number
	movq	8(%rsp), %r9
	movq	%rax, %rbx
	movq	(%rsp), %r8
	jmp	.L7
	.size	_dl_make_tlsdesc_dynamic, .-_dl_make_tlsdesc_dynamic
	.p2align 4,,15
	.globl	_dl_tlsdesc_resolve_rela_fixup
	.hidden	_dl_tlsdesc_resolve_rela_fixup
	.type	_dl_tlsdesc_resolve_rela_fixup, @function
_dl_tlsdesc_resolve_rela_fixup:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	664(%rsi), %rax
	movq	(%rsi), %r12
	movq	8(%rdi), %r13
	addq	8(%rax), %r12
	movq	(%rdi), %rax
	cmpq	%rax, %r12
	jne	.L46
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	movq	0(%rbp), %rax
	cmpq	%rax, %r12
	je	.L48
.L51:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
.L46:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	_dl_tlsdesc_resolve_hold(%rip), %rax
	movq	%rax, 0(%rbp)
	movq	104(%rbx), %rax
	movl	12(%r13), %ecx
	movq	8(%rax), %rdi
	movq	112(%rbx), %rax
	leaq	(%rcx,%rcx,2), %rdx
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, 8(%rsp)
	movzbl	4(%rax), %edx
	shrb	$4, %dl
	testb	%dl, %dl
	jne	.L66
.L49:
	movq	1112(%rbx), %rcx
	cmpq	$-1, %rcx
	je	.L52
	testq	%rcx, %rcx
	je	.L67
.L53:
	movq	16(%r13), %rdx
	addq	8(%rax), %rdx
	movq	%rdx, %rax
	subq	%rcx, %rax
	movq	%rax, 8(%rbp)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, 0(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L66:
	testb	$3, 5(%rax)
	jne	.L49
	movq	464(%rbx), %rdx
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L50
	movq	8(%rdx), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	andl	$32767, %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	744(%rbx), %rdx
	leaq	(%rdx,%rcx,8), %r8
	movl	$0, %edx
	movl	8(%r8), %esi
	testl	%esi, %esi
	cmove	%rdx, %r8
.L50:
	movl	(%rax), %eax
	movq	920(%rbx), %rcx
	leaq	8(%rsp), %rdx
	pushq	$0
	pushq	$1
	movq	%rbx, %rsi
	movl	$1, %r9d
	addq	%rax, %rdi
	call	_dl_lookup_symbol_x
	movq	%rax, %rbx
	movq	24(%rsp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	jne	.L49
	movq	16(%r13), %rax
	movq	%rax, 8(%rbp)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, 0(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L52:
	movq	16(%r13), %rsi
	addq	8(%rax), %rsi
	movq	%rbx, %rdi
	call	_dl_make_tlsdesc_dynamic
	movq	%rax, 8(%rbp)
	leaq	_dl_tlsdesc_dynamic(%rip), %rax
	movq	%rax, 0(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L67:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_dl_try_allocate_static_tls
	testl	%eax, %eax
	movq	8(%rsp), %rax
	jne	.L52
	movq	1112(%rbx), %rcx
	jmp	.L53
	.size	_dl_tlsdesc_resolve_rela_fixup, .-_dl_tlsdesc_resolve_rela_fixup
	.p2align 4,,15
	.globl	_dl_tlsdesc_resolve_hold_fixup
	.hidden	_dl_tlsdesc_resolve_hold_fixup
	.type	_dl_tlsdesc_resolve_hold_fixup, @function
_dl_tlsdesc_resolve_hold_fixup:
	movq	(%rdi), %rax
	cmpq	%rsi, %rax
	jne	.L68
	subq	$8, %rsp
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	leaq	2440+_rtld_local(%rip), %rdi
	addq	$8, %rsp
	jmp	*3992+_rtld_local(%rip)
	.p2align 4,,10
	.p2align 3
.L68:
	rep ret
	.size	_dl_tlsdesc_resolve_hold_fixup, .-_dl_tlsdesc_resolve_hold_fixup
	.p2align 4,,15
	.globl	_dl_unmap
	.hidden	_dl_unmap
	.type	_dl_unmap, @function
_dl_unmap:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	856(%rdi), %rdi
	movq	864(%rbx), %rsi
	subq	%rdi, %rsi
	call	__munmap
	movq	1032(%rbx), %r12
	testq	%r12, %r12
	je	.L73
	movl	8(%r12), %ebx
	subl	$1, %ebx
	js	.L75
	movslq	%ebx, %rbp
	salq	$3, %rbp
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%r12), %rax
	subl	$1, %ebx
	movq	(%rax,%rbp), %rdi
	subq	$8, %rbp
	call	*__rtld_free(%rip)
	cmpl	$-1, %ebx
	jne	.L76
.L75:
	movq	(%r12), %rdi
	call	*24(%r12)
	popq	%rbx
	movq	%r12, %rdi
	popq	%rbp
	popq	%r12
	jmp	*__rtld_free(%rip)
	.p2align 4,,10
	.p2align 3
.L73:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	_dl_unmap, .-_dl_unmap
	.hidden	__munmap
	.hidden	_dl_try_allocate_static_tls
	.hidden	_dl_tlsdesc_dynamic
	.hidden	_dl_tlsdesc_undefweak
	.hidden	_dl_lookup_symbol_x
	.hidden	_dl_tlsdesc_return
	.hidden	_dl_tlsdesc_resolve_hold
	.hidden	_dl_higher_prime_number
	.hidden	__rtld_calloc
	.hidden	__rtld_free
	.hidden	__rtld_malloc
	.hidden	_rtld_local
