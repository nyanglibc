	.text
	.p2align 4,,15
	.type	match_file.part.0, @function
match_file.part.0:
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rdx, %rbp
	subq	$8, %rsp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	1(%rax), %rbx
.L4:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L2
	movzbl	(%rbx,%rbp), %eax
	cmpb	$58, %al
	je	.L5
	testb	%al, %al
	je	.L5
.L2:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L9
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	match_file.part.0, .-match_file.part.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"cannot handle interface version %u"
	.section	.rodata.str1.1
.LC2:
	.string	"SOTRUSS_FROMLIST"
.LC3:
	.string	"SOTRUSS_TOLIST"
.LC4:
	.string	"SOTRUSS_EXIT"
.LC5:
	.string	"SOTRUSS_WHICH"
.LC6:
	.string	"SOTRUSS_OUTNAME"
.LC7:
	.string	".%ld"
.LC8:
	.string	"w"
	.text
	.p2align 4,,15
	.globl	la_version
	.type	la_version, @function
la_version:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$1, %edi
	jne	.L47
	leaq	.LC2(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	movq	%rax, fromlist(%rip)
	je	.L12
	cmpb	$0, (%rax)
	je	.L48
.L12:
	leaq	.LC3(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	movq	%rax, tolist(%rip)
	je	.L13
	cmpb	$0, (%rax)
	je	.L49
.L13:
	leaq	.LC4(%rip), %rdi
	call	getenv@PLT
	leaq	.LC0(%rip), %rdx
	testq	%rax, %rax
	leaq	.LC5(%rip), %rdi
	cmove	%rdx, %rax
	cmpb	$0, (%rax)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, do_exit(%rip)
	call	getenv@PLT
	movq	%rax, %rbx
	call	getpid@PLT
	testq	%rbx, %rbx
	movl	%eax, %r13d
	je	.L15
	cmpb	$0, (%rbx)
	jne	.L16
.L15:
	movl	$1, print_pid(%rip)
.L17:
	leaq	.LC6(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L20
	cmpb	$0, (%rax)
	jne	.L50
.L20:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movl	$1000, %edx
	movl	$2, %edi
	call	fcntl@PLT
	cmpl	$-1, %eax
	je	.L51
.L24:
	leaq	.LC8(%rip), %rsi
	movl	%eax, %edi
	call	fdopen@PLT
	testq	%rax, %rax
	movq	%rax, out_file(%rip)
	je	.L33
	movq	%rax, %rdi
	call	setlinebuf@PLT
.L33:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movq	$0, tolist(%rip)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L48:
	movq	$0, fromlist(%rip)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	-56(%rbp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	call	strtoul@PLT
	movq	-56(%rbp), %rdx
	cmpb	$0, (%rdx)
	jne	.L20
	movslq	%r13d, %rdx
	cmpq	%rdx, %rax
	jne	.L20
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rax, %rdi
	movq	%rsp, %r14
	call	strlen@PLT
	addq	$28, %rax
	movq	%r12, %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	movq	%rsp, %r15
	call	__stpcpy@PLT
	testq	%rbx, %rbx
	je	.L21
	cmpb	$0, (%rbx)
	je	.L21
.L22:
	xorl	%eax, %eax
	movl	$438, %edx
	movl	$578, %esi
	movq	%r15, %rdi
	call	open@PLT
	cmpl	$-1, %eax
	je	.L23
	movl	$0, print_pid(%rip)
	movq	%r14, %rsp
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC7(%rip), %rdx
	movq	%rax, %rdi
	movslq	%r13d, %rcx
	movl	$13, %esi
	xorl	%eax, %eax
	call	snprintf@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$2, %edi
	call	dup@PLT
	cmpl	$-1, %eax
	je	.L33
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r14, %rsp
	jmp	.L20
.L47:
	leaq	.LC1(%rip), %rdx
	movl	%edi, %ecx
	xorl	%esi, %esi
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.size	la_version, .-la_version
	.p2align 4,,15
	.globl	la_objopen
	.type	la_objopen, @function
la_objopen:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	xorl	%r13d, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	cmpq	$0, out_file(%rip)
	je	.L52
	movq	8(%rdi), %rax
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L54
	cmpb	$0, (%rax)
	je	.L54
.L55:
	movq	8(%rsp), %rbx
	movq	%rdi, 16(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	%rbx, %rdi
	movq	%rax, 32(%rsp)
	call	basename@PLT
	cmpb	$0, (%rax)
	movq	%rax, (%rsp)
	je	.L88
.L56:
	movq	(%rsp), %rdi
	call	strlen@PLT
	movq	%rax, 40(%rsp)
	movq	16(%rsp), %rax
	movq	fromlist(%rip), %r12
	movq	tolist(%rip), %rbp
	movq	56(%rax), %r15
	testq	%r15, %r15
	je	.L75
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L58:
	cmpb	$47, (%rbx)
	jne	.L60
	movq	(%r15), %rax
	cmpb	$47, (%rax)
	cmovne	%rax, %rbx
.L60:
	testq	%r12, %r12
	je	.L61
	movq	(%r15), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	cmpb	$0, (%r12)
	jne	.L89
.L61:
	testq	%rbp, %rbp
	je	.L63
	movq	(%r15), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	cmpb	$0, 0(%rbp)
	jne	.L90
.L63:
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.L91
.L65:
	testq	%rbx, %rbx
	jne	.L58
	movq	(%r15), %rbx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L91:
	testq	%rbx, %rbx
	cmove	(%rsp), %rbx
.L57:
	cmpb	$0, (%rbx)
	jne	.L66
	movq	__progname@GOTPCREL(%rip), %rax
	movq	(%rax), %rbx
.L66:
	movq	24(%rsp), %rax
	testq	%r12, %r12
	movq	%rbx, (%rax)
	je	.L92
	cmpb	$0, (%r12)
	je	.L69
	movq	32(%rsp), %rdx
	movq	8(%rsp), %rsi
	movl	$2, %ecx
	movq	%r12, %rdi
	call	match_file.part.0
	movq	40(%rsp), %rdx
	movq	(%rsp), %rsi
	movl	%eax, %ebx
	movl	$2, %ecx
	movq	%r12, %rdi
	call	match_file.part.0
	orl	%eax, %ebx
	orl	%ebx, %r13d
.L69:
	testq	%rbp, %rbp
	je	.L93
	cmpb	$0, 0(%rbp)
	je	.L52
	movq	32(%rsp), %rdx
	movq	8(%rsp), %rsi
	movl	$1, %ecx
	movq	%rbp, %rdi
	call	match_file.part.0
	movq	40(%rsp), %rdx
	movq	(%rsp), %rsi
	movl	%eax, %ebx
	movl	$1, %ecx
	movq	%rbp, %rdi
	call	match_file.part.0
	orl	%ebx, %eax
	orl	%eax, %r13d
.L52:
	addq	$56, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movl	$2, %ecx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	match_file.part.0
	orl	%eax, %r13d
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$1, %ecx
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	match_file.part.0
	orl	%eax, %r13d
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L88:
	movq	__progname@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L54:
	movq	__progname_full@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%rsp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L93:
	orl	$1, %r13d
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L92:
	movq	16(%rsp), %rax
	movq	8(%rax), %rdx
	movl	%r13d, %eax
	orl	$2, %eax
	cmpb	$0, (%rdx)
	cmove	%eax, %r13d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rsp), %rbx
	xorl	%r13d, %r13d
	jmp	.L57
	.size	la_objopen, .-la_objopen
	.p2align 4,,15
	.globl	la_symbind64
	.type	la_symbind64, @function
la_symbind64:
	movl	do_exit(%rip), %eax
	testl	%eax, %eax
	jne	.L95
	movl	$2, (%r8)
.L95:
	movq	8(%rdi), %rax
	ret
	.size	la_symbind64, .-la_symbind64
	.section	.rodata.str1.1
.LC9:
	.string	"*"
.LC10:
	.string	" "
.LC11:
	.string	"%5ld: "
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"%s%15s -> %-15s:%s%s(0x%lx, 0x%lx, 0x%lx)\n"
	.text
	.p2align 4,,15
	.globl	la_x86_64_gnu_pltenter
	.type	la_x86_64_gnu_pltenter, @function
la_x86_64_gnu_pltenter:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %r12
	subq	$40, %rsp
	movl	print_pid(%rip), %eax
	movl	(%r9), %r10d
	movq	32(%r8), %r15
	movq	(%r8), %r9
	leaq	17(%rsp), %rbx
	movq	40(%r8), %r14
	movb	$0, 17(%rsp)
	testl	%eax, %eax
	jne	.L102
.L97:
	pushq	%r9
	leaq	.LC10(%rip), %rax
	movq	0(%r13), %r8
	movq	(%r12), %rcx
	leaq	.LC9(%rip), %r9
	movq	out_file(%rip), %rdi
	pushq	%r15
	andl	$2, %r10d
	pushq	%r14
	leaq	.LC12(%rip), %rsi
	pushq	120(%rsp)
	cmove	%rax, %r9
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	movq	136(%rsp), %rax
	movq	$0, (%rax)
	movq	8(%rbp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%r9, 8(%rsp)
	movl	%r10d, 4(%rsp)
	call	getpid@PLT
	leaq	.LC11(%rip), %rdx
	movslq	%eax, %rcx
	movl	$15, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	snprintf@PLT
	movq	8(%rsp), %r9
	movl	4(%rsp), %r10d
	jmp	.L97
	.size	la_x86_64_gnu_pltenter, .-la_x86_64_gnu_pltenter
	.section	.rodata.str1.1
.LC13:
	.string	"%s%15s -> %-15s:%s%s - 0x%lx\n"
	.text
	.p2align 4,,15
	.globl	la_x86_64_gnu_pltexit
	.type	la_x86_64_gnu_pltexit, @function
la_x86_64_gnu_pltexit:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$24, %rsp
	movl	print_pid(%rip), %eax
	movq	(%r9), %r13
	movb	$0, 1(%rsp)
	leaq	1(%rsp), %rbx
	testl	%eax, %eax
	je	.L104
	call	getpid@PLT
	leaq	.LC11(%rip), %rdx
	movslq	%eax, %rcx
	movl	$15, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	snprintf@PLT
.L104:
	pushq	%r13
	movq	(%r12), %r8
	leaq	.LC10(%rip), %r9
	movq	0(%rbp), %rcx
	movq	out_file(%rip), %rdi
	leaq	.LC13(%rip), %rsi
	pushq	72(%rsp)
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	la_x86_64_gnu_pltexit, .-la_x86_64_gnu_pltexit
	.local	out_file
	.comm	out_file,8,8
	.local	print_pid
	.comm	print_pid,4,4
	.local	do_exit
	.comm	do_exit,4,4
	.local	tolist
	.comm	tolist,8,8
	.local	fromlist
	.comm	fromlist,8,8
