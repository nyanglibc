	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"unsupported version %s of Verneed record"
	.section	.rodata.str1.1
.LC2:
	.string	"dl-version.c"
.LC3:
	.string	"needed != NULL"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"checking for version `%s' in file %s [%lu] required by file %s [%lu]\n"
	.align 8
.LC5:
	.string	"no version information available (required by %s)"
	.section	.rodata.str1.1
.LC6:
	.string	"def_offset != 0"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"unsupported version %s of Verdef record"
	.align 8
.LC8:
	.string	"weak version `%s' not found (required by %s)"
	.align 8
.LC9:
	.string	"version `%s' not found (required by %s)"
	.section	.rodata.str1.1
.LC10:
	.string	"version lookup error"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"cannot allocate version reference table"
	.text
	.p2align 4,,15
	.globl	_dl_check_map_versions
	.hidden	_dl_check_map_versions
	.type	_dl_check_map_versions, @function
_dl_check_map_versions:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$168, %rsp
	movq	104(%rdi), %rax
	testq	%rax, %rax
	je	.L47
	movq	8(%rax), %rax
	movq	%rdi, %r15
	movl	%edx, 76(%rsp)
	movl	%esi, 72(%rsp)
	movq	%rax, 24(%rsp)
	movq	352(%rdi), %rax
	movq	368(%rdi), %rdi
	testq	%rax, %rax
	movq	%rax, 88(%rsp)
	movq	%rdi, 80(%rsp)
	je	.L3
	movq	8(%rax), %rax
	addq	(%r15), %rax
	leaq	__GI__itoa_lower_digits(%rip), %r14
	movl	$0, (%rsp)
	movl	$0, 68(%rsp)
	movq	%r15, %r13
	cmpw	$1, (%rax)
	movq	%rax, 56(%rsp)
	jne	.L115
	.p2align 4,,10
	.p2align 3
.L4:
	movq	56(%rsp), %rax
	leaq	_rtld_local(%rip), %rdi
	movl	4(%rax), %ebx
	movq	48(%r13), %rax
	addq	24(%rsp), %rbx
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	(%rdi,%rax,8), %r15
	testq	%r15, %r15
	jne	.L10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L116:
	movq	24(%r15), %r15
	testq	%r15, %r15
	je	.L8
.L10:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L116
.L9:
	movl	76(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L117
.L14:
	movq	56(%rsp), %rax
	movq	%r15, 32(%rsp)
	movl	(%rsp), %r15d
	movq	%r13, 16(%rsp)
	movl	8(%rax), %ebx
	addq	%rax, %rbx
	leaq	128(%rsp), %rax
	movq	%rax, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L33:
	movzwl	4(%rbx), %eax
	movl	8(%rbx), %r12d
	addq	24(%rsp), %r12
	movl	(%rbx), %r13d
	movw	%ax, 66(%rsp)
	movq	32(%rsp), %rax
	movq	%r12, (%rsp)
	movq	40(%rax), %rbp
	movq	16(%rsp), %rax
	movq	8(%rax), %r12
	cmpb	$0, (%r12)
	jne	.L16
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %r12
	leaq	.LC0(%rip), %rax
	testq	%r12, %r12
	cmove	%rax, %r12
.L16:
	movq	104(%rbp), %rax
	movq	8(%rax), %rax
	movq	%rax, 8(%rsp)
	leaq	_rtld_local_ro(%rip), %rax
	testb	$16, (%rax)
	jne	.L118
.L17:
	movq	368(%rbp), %rax
	testq	%rax, %rax
	je	.L119
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L120
	addq	0(%rbp), %rdx
	cmpw	$1, (%rdx)
	jne	.L29
	movq	%r12, 40(%rsp)
	movq	%rbx, %r12
	movl	%r13d, %ebx
	movq	%rdx, %r13
.L24:
	cmpl	8(%r13), %ebx
	je	.L121
.L27:
	movl	16(%r13), %eax
	testl	%eax, %eax
	je	.L28
	addq	%rax, %r13
	cmpw	$1, 0(%r13)
	je	.L24
	movq	%r12, %rbx
	movq	%r13, %rdx
.L29:
	movb	$0, 115(%rsp)
	movzwl	(%rdx), %esi
	leaq	115(%rsp), %rcx
	.p2align 4,,10
	.p2align 3
.L25:
	movabsq	$-3689348814741910323, %rax
	subq	$1, %rcx
	mulq	%rsi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movzbl	(%r14,%rsi), %eax
	movq	%rdx, %rsi
	movb	%al, (%rcx)
	jne	.L25
	movq	8(%rbp), %rsi
	cmpb	$0, (%rsi)
	jne	.L26
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L26:
	movq	48(%rsp), %rdi
	leaq	.LC7(%rip), %rdx
	xorl	%eax, %eax
	call	__GI__dl_exception_create_format
	movl	$1, 68(%rsp)
.L22:
	movq	48(%rsp), %rbp
	leaq	.LC10(%rip), %rdx
	xorl	%edi, %edi
	movq	%rbp, %rsi
	call	_dl_signal_cexception
	movq	%rbp, %rdi
	call	__GI__dl_exception_free
.L20:
	movzwl	6(%rbx), %eax
	andl	$32767, %eax
	cmpl	%eax, %r15d
	cmovb	%eax, %r15d
	movl	12(%rbx), %eax
	testl	%eax, %eax
	je	.L111
	addq	%rax, %rbx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L121:
	movl	12(%r13), %eax
	movq	(%rsp), %rdi
	movl	0(%r13,%rax), %esi
	addq	8(%rsp), %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L27
	movq	%r12, %rbx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L111:
	movq	16(%rsp), %r13
	movl	%r15d, (%rsp)
.L15:
	movq	56(%rsp), %rdi
	movl	12(%rdi), %eax
	testl	%eax, %eax
	je	.L34
	addq	%rax, %rdi
	movq	%rdi, 56(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L119:
	movl	72(%rsp), %edx
	testl	%edx, %edx
	je	.L20
	movq	8(%rbp), %rsi
	cmpb	$0, (%rsi)
	jne	.L21
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L21:
	movq	48(%rsp), %rdi
	leaq	.LC5(%rip), %rdx
	movq	%r12, %rcx
	xorl	%eax, %eax
	call	__GI__dl_exception_create_format
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L118:
	movq	8(%rbp), %rdx
	movq	48(%rbp), %rcx
	cmpb	$0, (%rdx)
	jne	.L18
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rdx
	leaq	.LC0(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L18:
	movq	16(%rsp), %rax
	movq	(%rsp), %rsi
	leaq	.LC4(%rip), %rdi
	movq	%r12, %r8
	movq	48(%rax), %r9
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L8:
	movl	712(%r13), %esi
	testl	%esi, %esi
	je	.L11
	xorl	%r12d, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L12:
	addl	$1, %r12d
	cmpl	712(%r13), %r12d
	jnb	.L11
.L13:
	movq	704(%r13), %rdx
	movl	%r12d, %eax
	movq	%rbx, %rdi
	leaq	0(,%rax,8), %rbp
	movq	(%rdx,%rax,8), %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L12
	movq	704(%r13), %rax
	movq	(%rax,%rbp), %r15
	testq	%r15, %r15
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	__PRETTY_FUNCTION__.9011(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$205, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L28:
	testb	$2, 66(%rsp)
	movq	%r12, %rbx
	movq	40(%rsp), %r12
	je	.L30
	movl	72(%rsp), %eax
	testl	%eax, %eax
	je	.L20
	movq	8(%rbp), %rsi
	cmpb	$0, (%rsi)
	jne	.L31
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L31:
	movq	(%rsp), %rcx
	movq	48(%rsp), %rdi
	leaq	.LC8(%rip), %rdx
	movq	%r12, %r8
	xorl	%eax, %eax
	call	__GI__dl_exception_create_format
	jmp	.L22
.L117:
	testb	$2, 797(%r15)
	je	.L14
	jmp	.L15
.L34:
	cmpq	$0, 80(%rsp)
	movq	%r13, %r15
	je	.L36
.L35:
	movq	80(%rsp), %rax
	movl	(%rsp), %edi
	movq	8(%rax), %rdx
	addq	(%r15), %rdx
	movzwl	4(%rdx), %eax
	andl	$32767, %eax
	cmpl	%eax, %edi
	cmovnb	%edi, %eax
	movl	%eax, %ecx
	movl	%eax, (%rsp)
	movl	16(%rdx), %eax
	testl	%eax, %eax
	je	.L36
	.p2align 4,,10
	.p2align 3
.L37:
	addq	%rax, %rdx
	movzwl	4(%rdx), %eax
	andl	$32767, %eax
	cmpl	%eax, %ecx
	cmovb	%eax, %ecx
	movl	16(%rdx), %eax
	testl	%eax, %eax
	jne	.L37
	movl	%ecx, (%rsp)
.L36:
	movl	(%rsp), %eax
	testl	%eax, %eax
	je	.L1
	leal	1(%rax), %edi
	movl	$24, %esi
	movq	%rdi, %rbx
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	movq	%rax, 744(%r15)
	je	.L122
	movq	464(%r15), %rdx
	movq	88(%rsp), %rdi
	movl	%ebx, 752(%r15)
	movq	8(%rdx), %rdx
	testq	%rdi, %rdi
	movq	%rdx, 840(%r15)
	je	.L40
	movq	8(%rdi), %rdi
	addq	(%r15), %rdi
	movq	24(%rsp), %r8
	.p2align 4,,10
	.p2align 3
.L44:
	movl	8(%rdi), %ecx
	addq	%rdi, %rcx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L123:
	addq	%rdx, %rcx
.L43:
	movzwl	6(%rcx), %edx
	movl	%edx, %esi
	andw	$32767, %si
	movzwl	%si, %r9d
	cmpl	%r9d, %ebx
	jbe	.L41
	movzwl	%si, %esi
	andw	$-32768, %dx
	movl	(%rcx), %r9d
	leaq	(%rsi,%rsi,2), %rsi
	movzwl	%dx, %edx
	leaq	(%rax,%rsi,8), %rsi
	movl	%edx, 12(%rsi)
	movl	8(%rcx), %edx
	movl	%r9d, 8(%rsi)
	addq	%r8, %rdx
	movq	%rdx, (%rsi)
	movl	4(%rdi), %edx
	addq	%r8, %rdx
	movq	%rdx, 16(%rsi)
.L41:
	movl	12(%rcx), %edx
	testl	%edx, %edx
	jne	.L123
	movl	12(%rdi), %edx
	testl	%edx, %edx
	je	.L40
	addq	%rdx, %rdi
	jmp	.L44
.L47:
	movl	$0, 68(%rsp)
.L1:
	movl	68(%rsp), %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L40:
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L1
	movq	8(%rdi), %rdx
	movq	24(%rsp), %r8
	addq	(%r15), %rdx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L124:
	addq	%rcx, %rdx
.L46:
	testb	$1, 2(%rdx)
	jne	.L45
	movl	12(%rdx), %esi
	movzwl	4(%rdx), %ecx
	movl	8(%rdx), %edi
	movl	(%rdx,%rsi), %esi
	andl	$32767, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	addq	%r8, %rsi
	movl	%edi, 8(%rcx)
	movq	%rsi, (%rcx)
	movq	$0, 16(%rcx)
.L45:
	movl	16(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L124
	jmp	.L1
.L30:
	movq	8(%rbp), %rsi
	cmpb	$0, (%rsi)
	je	.L125
.L32:
	movq	(%rsp), %rcx
	movq	48(%rsp), %rdi
	leaq	.LC9(%rip), %rdx
	movq	%r12, %r8
	xorl	%eax, %eax
	call	__GI__dl_exception_create_format
	movl	$1, 68(%rsp)
	jmp	.L22
.L3:
	cmpq	$0, 80(%rsp)
	movl	$0, (%rsp)
	movl	$0, 68(%rsp)
	jne	.L35
	jmp	.L1
.L125:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L32
.L115:
	movb	$0, 115(%rsp)
	movzwl	(%rax), %esi
	leaq	115(%rsp), %rcx
	movq	%r14, %r8
	movabsq	$-3689348814741910323, %rdi
.L5:
	movq	%rsi, %rax
	subq	$1, %rcx
	mulq	%rdi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movzbl	(%r8,%rsi), %eax
	movq	%rdx, %rsi
	movb	%al, (%rcx)
	jne	.L5
	movq	8(%r15), %rsi
	cmpb	$0, (%rsi)
	je	.L126
.L6:
	leaq	128(%rsp), %rbx
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	__GI__dl_exception_create_format
	xorl	%edi, %edi
.L7:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	_dl_signal_exception@PLT
.L122:
	movq	8(%r15), %rsi
	cmpb	$0, (%rsi)
	je	.L127
.L39:
	leaq	128(%rsp), %rbx
	leaq	.LC11(%rip), %rdx
	movq	%rbx, %rdi
	call	__GI__dl_exception_create
	movl	$12, %edi
	jmp	.L7
.L127:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L39
.L126:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L6
.L120:
	leaq	__PRETTY_FUNCTION__.8989(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$88, %edx
	call	__GI___assert_fail
	.size	_dl_check_map_versions, .-_dl_check_map_versions
	.p2align 4,,15
	.globl	_dl_check_all_versions
	.hidden	_dl_check_all_versions
	.type	_dl_check_all_versions, @function
_dl_check_all_versions:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L132
	movq	%rdi, %rbx
	movl	%esi, %r12d
	movl	%edx, %r13d
	xorl	%ebp, %ebp
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L130:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L128
.L131:
	testb	$2, 797(%rbx)
	jne	.L130
	movq	%rbx, %rdi
	movl	%r13d, %edx
	movl	%r12d, %esi
	call	_dl_check_map_versions
	movq	24(%rbx), %rbx
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, %ebp
	testq	%rbx, %rbx
	jne	.L131
.L128:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$8, %rsp
	xorl	%ebp, %ebp
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	_dl_check_all_versions, .-_dl_check_all_versions
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8989, @object
	.size	__PRETTY_FUNCTION__.8989, 13
__PRETTY_FUNCTION__.8989:
	.string	"match_symbol"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9011, @object
	.size	__PRETTY_FUNCTION__.9011, 23
__PRETTY_FUNCTION__.9011:
	.string	"_dl_check_map_versions"
	.hidden	__rtld_calloc
	.hidden	_dl_debug_printf
	.hidden	strcmp
	.hidden	_dl_signal_cexception
	.hidden	_rtld_local_ro
	.hidden	_dl_name_match_p
	.hidden	_rtld_local
