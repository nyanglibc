	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../elf/dl-tls.c"
.LC1:
	.string	"result.to_free != NULL"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"cannot allocate memory for thread-local data: ABORT\n"
	.text
	.p2align 4,,15
	.type	tls_get_addr_tail, @function
tls_get_addr_tail:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L32
	cmpq	$-1, 1112(%r14)
	jne	.L33
.L5:
	movq	1096(%r14), %rbx
	movq	1088(%r14), %rdi
	leaq	-1(%rbx), %rbp
	testq	%rbp, %rbx
	jne	.L9
	cmpq	$16, %rbx
	ja	.L9
	call	*__rtld_malloc(%rip)
	movq	%rax, %rbx
	movq	%rax, %r15
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	addq	%rbx, %rdi
	jc	.L14
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L14
	leaq	(%rax,%rbp), %rax
	xorl	%edx, %edx
	divq	%rbx
	imulq	%rax, %rbx
.L10:
	testq	%rbx, %rbx
	je	.L14
	movq	1080(%r14), %rdx
	movq	1088(%r14), %rbp
	movq	%rbx, %rdi
	movq	1072(%r14), %rsi
	subq	%rdx, %rbp
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%rbp, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	0(%r13), %rax
	salq	$4, %rax
	addq	%rax, %r12
	testq	%r15, %r15
	movq	%rbx, (%r12)
	movq	%r15, 8(%r12)
	je	.L34
	movq	8(%r13), %rax
	addq	%rbx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.p2align 4,,10
	.p2align 3
.L32:
	movq	4032+_rtld_local(%rip), %rdx
	movq	(%rdi), %rax
	movq	(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%rdx), %rdx
	subq	%rcx, %rax
	movq	(%rdx), %rcx
	cmpq	%rax, %rcx
	jbe	.L4
.L3:
	addq	$1, %rax
	salq	$4, %rax
	movq	8(%rdx,%rax), %r14
	cmpq	$-1, 1112(%r14)
	je	.L5
.L33:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	movq	1112(%r14), %rax
	testq	%rax, %rax
	jne	.L6
	movq	$-1, 1112(%r14)
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	jmp	.L5
.L6:
	cmpq	$-1, %rax
	movq	3992+_rtld_local(%rip), %rdx
	je	.L7
	movq	%fs:16, %rbx
	leaq	2440+_rtld_local(%rip), %rdi
	subq	%rax, %rbx
	call	*%rdx
	movq	0(%r13), %rax
	salq	$4, %rax
	addq	%rax, %r12
	movq	8(%r13), %rax
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
	addq	%rbx, %rax
	jmp	.L1
.L7:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*%rdx
	jmp	.L5
.L34:
	leaq	__PRETTY_FUNCTION__.10191(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$869, %edx
	call	__GI___assert_fail
	.size	tls_get_addr_tail, .-tls_get_addr_tail
	.p2align 4,,15
	.type	_dl_resize_dtv, @function
_dl_resize_dtv:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r14
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	4024+_rtld_local(%rip), %rsi
	cmpq	%rdi, 4080+_rtld_local(%rip)
	movq	-16(%rdi), %r13
	leaq	14(%rsi), %rbp
	je	.L44
	addq	$16, %rsi
	leaq	-16(%rdi), %rdi
	salq	$4, %rsi
	call	*__rtld_realloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L39
	leaq	2(%r13), %r12
	salq	$4, %r12
.L38:
	movq	%rbp, %rdx
	leaq	(%rbx,%r12), %rdi
	movq	%rbp, (%rbx)
	subq	%r13, %rdx
	xorl	%esi, %esi
	salq	$4, %rdx
	call	memset@PLT
	leaq	16(%rbx), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	16(%rsi), %rdi
	salq	$4, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L39
	leaq	2(%r13), %r12
	leaq	-16(%r14), %rsi
	movq	%rax, %rdi
	salq	$4, %r12
	movq	%r12, %rdx
	call	memcpy@PLT
	jmp	.L38
.L39:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.size	_dl_resize_dtv, .-_dl_resize_dtv
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"Failed loading %lu audit modules, %lu are supported.\n"
	.text
	.p2align 4,,15
	.globl	_dl_tls_static_surplus_init
	.hidden	_dl_tls_static_surplus_init
	.type	_dl_tls_static_surplus_init, @function
_dl_tls_static_surplus_init:
	pushq	%r12
	pushq	%rbp
	xorl	%edx, %edx
	pushq	%rbx
	movq	%rdi, %rbp
	xorl	%edi, %edi
	subq	$16, %rsp
	leaq	8(%rsp), %r12
	movq	%r12, %rsi
	call	__GI___tunable_get_val
	movq	8(%rsp), %rbx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$28, %edi
	call	__GI___tunable_get_val
	cmpq	$16, %rbx
	movl	$16, %edx
	movq	8(%rsp), %rcx
	cmova	%rdx, %rbx
	subq	%rbx, %rdx
	cmpq	%rbp, %rdx
	jb	.L48
	addq	%rbp, %rbx
	movq	%rcx, 4072+_rtld_local(%rip)
	leal	-1(%rbx,%rbx), %eax
	leal	(%rax,%rax,8), %eax
	sall	$4, %eax
	leal	144(%rax,%rcx), %eax
	cltq
	movq	%rax, 624+_rtld_local_ro(%rip)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L48:
	leaq	.LC3(%rip), %rdi
	movq	%rbp, %rsi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.size	_dl_tls_static_surplus_init, .-_dl_tls_static_surplus_init
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"result <= GL(dl_tls_max_dtv_idx) + 1"
	.align 8
.LC5:
	.string	"result == GL(dl_tls_max_dtv_idx) + 1"
	.text
	.p2align 4,,15
	.globl	_dl_next_tls_modid
	.hidden	_dl_next_tls_modid
	.type	_dl_next_tls_modid, @function
_dl_next_tls_modid:
	cmpb	$0, 4020+_rtld_local(%rip)
	movq	4024+_rtld_local(%rip), %rsi
	jne	.L50
	leaq	1(%rsi), %rax
	movq	%rax, 4024+_rtld_local(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	subq	$8, %rsp
	movq	4040+_rtld_local(%rip), %rax
	leaq	1(%rax), %rdx
	cmpq	%rsi, %rdx
	ja	.L52
	movq	4032+_rtld_local(%rip), %rcx
	leaq	1(%rsi), %r9
	movq	%rdx, %rax
	xorl	%edi, %edi
	movq	(%rcx), %r8
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rax, %rdx
	subq	%rdi, %rdx
	cmpq	%r8, %rdx
	jnb	.L66
	addq	$1, %rdx
	salq	$4, %rdx
	cmpq	$0, 8(%rcx,%rdx)
	je	.L54
	addq	$1, %rax
	cmpq	%rax, %r9
	jnb	.L53
	leaq	__PRETTY_FUNCTION__.10038(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$152, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L66:
	movq	8(%rcx), %rcx
	addq	%r8, %rdi
	testq	%rcx, %rcx
	je	.L54
	movq	(%rcx), %r8
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	%rsi, %rax
	jbe	.L49
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L52:
	leaq	1(%rsi), %rax
	cmpq	%rdx, %rax
	jne	.L67
.L51:
	movb	$0, 4020+_rtld_local(%rip)
	movq	%rax, 4024+_rtld_local(%rip)
.L49:
	addq	$8, %rsp
	ret
.L67:
	leaq	__PRETTY_FUNCTION__.10038(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$166, %edx
	call	__GI___assert_fail
	.size	_dl_next_tls_modid, .-_dl_next_tls_modid
	.p2align 4,,15
	.globl	_dl_count_modids
	.hidden	_dl_count_modids
	.type	_dl_count_modids, @function
_dl_count_modids:
	cmpb	$0, 4020+_rtld_local(%rip)
	jne	.L69
	movq	4024+_rtld_local(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movq	4032+_rtld_local(%rip), %rdi
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L83
	.p2align 4,,10
	.p2align 3
.L71:
	movq	(%rdi), %rsi
	xorl	%edx, %edx
	leaq	24(%rdi), %rcx
	testq	%rsi, %rsi
	je	.L75
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	$1, (%rcx)
	sbbq	$-1, %rax
	addq	$1, %rdx
	addq	$16, %rcx
	cmpq	%rsi, %rdx
	jne	.L73
.L75:
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L71
	rep ret
.L83:
	rep ret
	.size	_dl_count_modids, .-_dl_count_modids
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"GL(dl_tls_dtv_slotinfo_list) != NULL"
	.align 8
.LC7:
	.string	"GL(dl_tls_dtv_slotinfo_list)->next == NULL"
	.align 8
.LC8:
	.string	"cnt < GL(dl_tls_dtv_slotinfo_list)->len"
	.text
	.p2align 4,,15
	.globl	_dl_determine_tlsoffset
	.hidden	_dl_determine_tlsoffset
	.type	_dl_determine_tlsoffset, @function
_dl_determine_tlsoffset:
	movq	4032+_rtld_local(%rip), %r10
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	testq	%r10, %r10
	je	.L98
	cmpq	$0, 8(%r10)
	jne	.L99
	movq	24(%r10), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	(%r10), %r13
	testq	%r13, %r13
	je	.L89
	addq	$40, %r10
	xorl	%ebx, %ebx
	xorl	%r11d, %r11d
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	movl	$64, %r9d
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-1(%rcx,%r11), %rax
	xorl	%edx, %edx
	addq	%r8, %rsi
	addq	%r11, %rsi
	addq	%r8, %rax
	subq	%r14, %rax
	divq	%rcx
	imulq	%rcx, %rax
	addq	%r14, %rax
	cmpq	%rax, %rsi
	jnb	.L93
	movq	%rax, %r12
	movq	%r11, %rbp
	subq	%r8, %r12
.L93:
	movq	%rax, 1112(%rdi)
	movq	(%r10), %rdi
	addq	$1, %rbx
	movq	%rax, %r11
	testq	%rdi, %rdi
	je	.L87
.L94:
	addq	$16, %r10
	cmpq	%r13, %rbx
	je	.L89
.L90:
	movq	1096(%rdi), %rcx
	movq	1104(%rdi), %rax
	movq	%r12, %rsi
	movq	1088(%rdi), %r8
	leaq	-1(%rcx), %r14
	negq	%rax
	andq	%rax, %r14
	cmpq	%rcx, %r9
	cmovb	%rcx, %r9
	subq	%rbp, %rsi
	cmpq	%r8, %rsi
	jb	.L91
	leaq	-1(%rcx,%rbp), %rax
	xorl	%edx, %edx
	addq	%r8, %rax
	subq	%r14, %rax
	divq	%rcx
	imulq	%rcx, %rax
	addq	%r14, %rax
	cmpq	%r12, %rax
	ja	.L91
	movq	%rax, 1112(%rdi)
	movq	(%r10), %rdi
	addq	$1, %rbx
	movq	%rax, %rbp
	testq	%rdi, %rdi
	jne	.L94
.L87:
	movq	624+_rtld_local_ro(%rip), %rax
	xorl	%edx, %edx
	movq	%r11, 4056+_rtld_local(%rip)
	popq	%rbx
	movq	%r9, 4064+_rtld_local(%rip)
	leaq	-1(%r9,%rax), %rax
	popq	%rbp
	addq	%r11, %rax
	divq	%r9
	popq	%r12
	popq	%r13
	popq	%r14
	imulq	%r9, %rax
	addq	$2496, %rax
	movq	%rax, 4048+_rtld_local(%rip)
	ret
.L95:
	xorl	%r11d, %r11d
	movl	$64, %r9d
	jmp	.L87
.L89:
	leaq	__PRETTY_FUNCTION__.10062(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$261, %edx
	call	__GI___assert_fail
.L99:
	leaq	__PRETTY_FUNCTION__.10062(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$223, %edx
	call	__GI___assert_fail
.L98:
	leaq	__PRETTY_FUNCTION__.10062(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$220, %edx
	call	__GI___assert_fail
	.size	_dl_determine_tlsoffset, .-_dl_determine_tlsoffset
	.p2align 4,,15
	.globl	_dl_get_tls_static_info
	.type	_dl_get_tls_static_info, @function
_dl_get_tls_static_info:
	movq	4048+_rtld_local(%rip), %rax
	movq	%rax, (%rdi)
	movq	4064+_rtld_local(%rip), %rax
	movq	%rax, (%rsi)
	ret
	.size	_dl_get_tls_static_info, .-_dl_get_tls_static_info
	.p2align 4,,15
	.globl	_dl_allocate_tls_storage
	.hidden	_dl_allocate_tls_storage
	.type	_dl_allocate_tls_storage, @function
_dl_allocate_tls_storage:
	pushq	%r12
	movq	4048+_rtld_local(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	4064+_rtld_local(%rip), %rbx
	leaq	8(%r12,%rbx), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	je	.L104
	movq	%rax, %rbp
	leaq	-1(%rax,%rbx), %rax
	xorl	%edx, %edx
	movl	$16, %esi
	divq	%rbx
	imulq	%rbx, %rax
	leaq	-2496(%r12,%rax), %rbx
	xorl	%eax, %eax
	leaq	8(%rbx), %rdi
	movq	%rbx, %rcx
	movq	$0, (%rbx)
	movq	$0, 2488(%rbx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2496, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rbp, 2496(%rbx)
	movq	4024+_rtld_local(%rip), %rdi
	leaq	14(%rdi), %r12
	addq	$16, %rdi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	je	.L106
	movq	%r12, (%rax)
	addq	$16, %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	xorl	%ebx, %ebx
	movq	%rbp, %rdi
	call	*__rtld_free(%rip)
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	_dl_allocate_tls_storage, .-_dl_allocate_tls_storage
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"listp->slotinfo[cnt].gen <= GL(dl_tls_generation)"
	.align 8
.LC10:
	.string	"map->l_tls_modid == total + cnt"
	.align 8
.LC11:
	.string	"map->l_tls_blocksize >= map->l_tls_initimage_size"
	.align 8
.LC12:
	.string	"(size_t) map->l_tls_offset >= map->l_tls_blocksize"
	.section	.rodata.str1.1
.LC13:
	.string	"listp != NULL"
	.text
	.p2align 4,,15
	.globl	__GI__dl_allocate_tls_init
	.hidden	__GI__dl_allocate_tls_init
	.type	__GI__dl_allocate_tls_init, @function
__GI__dl_allocate_tls_init:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	testq	%rdi, %rdi
	movq	%rdi, 40(%rsp)
	je	.L108
	movq	8(%rdi), %rax
	movq	4024+_rtld_local(%rip), %rsi
	cmpq	%rsi, -16(%rax)
	movq	%rax, 32(%rsp)
	jnb	.L109
	movq	%rax, %rdi
	call	_dl_resize_dtv
	movq	40(%rsp), %rsi
	movq	%rax, 32(%rsp)
	movq	%rax, 8(%rsi)
	movq	4024+_rtld_local(%rip), %rsi
.L109:
	movq	4032+_rtld_local(%rip), %rbp
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%ecx, %ecx
	testq	%rax, %rax
	sete	%cl
	cmpq	0(%rbp), %rcx
	leaq	(%rax,%rcx), %rbx
	movq	%rcx, 16(%rsp)
	movq	%rbx, 8(%rsp)
	jnb	.L120
	cmpq	%rsi, %rbx
	ja	.L111
	leaq	1(%rax,%rcx), %r13
	movq	%rcx, %rdx
	movq	%r15, %rax
	salq	$4, %rdx
	movq	%rbx, %r15
	movq	%rax, %r14
	subq	%rbx, %r13
	leaq	24(%rbp,%rdx), %r12
	movq	%r13, 24(%rsp)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L118:
	addq	$16, %r12
	cmpq	%rsi, %r15
	ja	.L129
.L112:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L113
	movq	-8(%r12), %rdx
	cmpq	4088+_rtld_local(%rip), %rdx
	ja	.L132
	movq	1120(%rax), %r10
	cmpq	%rdx, %r14
	movq	1112(%rax), %rdi
	cmovb	%rdx, %r14
	movq	%r10, %rdx
	leaq	1(%rdi), %r11
	salq	$4, %rdx
	addq	32(%rsp), %rdx
	cmpq	$1, %r11
	movq	$-1, (%rdx)
	movq	$0, 8(%rdx)
	jbe	.L113
	cmpq	%r15, %r10
	jne	.L133
	movq	1088(%rax), %rbx
	movq	1080(%rax), %r13
	cmpq	%r13, %rbx
	jb	.L134
	cmpq	%rbx, %rdi
	jb	.L135
	movq	40(%rsp), %rcx
	movq	1072(%rax), %rsi
	subq	%r13, %rbx
	subq	%rdi, %rcx
	movq	%rcx, %rdi
	movq	%rcx, (%rdx)
	movq	%r13, %rdx
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	4024+_rtld_local(%rip), %rsi
.L113:
	movq	16(%rsp), %rax
	addq	$1, %rax
	subq	8(%rsp), %rax
	addq	%r15, %rax
	addq	24(%rsp), %r15
	cmpq	%rax, 0(%rbp)
	ja	.L118
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
	movq	%r14, %rax
.L110:
	cmpq	%rax, %rsi
	jbe	.L111
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.L119
	leaq	__PRETTY_FUNCTION__.10112(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$597, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%r14, %r15
.L111:
	movq	32(%rsp), %rax
	movq	%r15, (%rax)
.L108:
	movq	40(%rsp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L120:
	movq	8(%rsp), %rax
	jmp	.L110
.L132:
	leaq	__PRETTY_FUNCTION__.10112(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$561, %edx
	call	__GI___assert_fail
.L133:
	leaq	__PRETTY_FUNCTION__.10112(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$571, %edx
	call	__GI___assert_fail
.L134:
	leaq	__PRETTY_FUNCTION__.10112(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$572, %edx
	call	__GI___assert_fail
.L135:
	leaq	__PRETTY_FUNCTION__.10112(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$574, %edx
	call	__GI___assert_fail
	.size	__GI__dl_allocate_tls_init, .-__GI__dl_allocate_tls_init
	.globl	_dl_allocate_tls_init
	.set	_dl_allocate_tls_init,__GI__dl_allocate_tls_init
	.p2align 4,,15
	.globl	__GI__dl_allocate_tls
	.hidden	__GI__dl_allocate_tls
	.type	__GI__dl_allocate_tls, @function
__GI__dl_allocate_tls:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L141
	movq	4024+_rtld_local(%rip), %rbx
	movq	%rdi, %rbp
	movl	$16, %esi
	leaq	16(%rbx), %rdi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	je	.L139
	addq	$14, %rbx
	addq	$16, %rax
	movq	%rbx, -16(%rax)
	movq	%rax, 8(%rbp)
.L138:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI__dl_allocate_tls_init
	.p2align 4,,10
	.p2align 3
.L139:
	xorl	%ebp, %ebp
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI__dl_allocate_tls_init
	.p2align 4,,10
	.p2align 3
.L141:
	call	_dl_allocate_tls_storage
	movq	%rax, %rbp
	jmp	.L138
	.size	__GI__dl_allocate_tls, .-__GI__dl_allocate_tls
	.globl	_dl_allocate_tls
	.set	_dl_allocate_tls,__GI__dl_allocate_tls
	.p2align 4,,15
	.globl	__GI__dl_deallocate_tls
	.hidden	__GI__dl_deallocate_tls
	.type	__GI__dl_deallocate_tls, @function
__GI__dl_deallocate_tls:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$8, %rsp
	movq	8(%rdi), %rbp
	cmpq	$0, -16(%rbp)
	je	.L143
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L144:
	addq	$1, %rbx
	movq	%rbx, %rax
	salq	$4, %rax
	movq	8(%rbp,%rax), %rdi
	call	*__rtld_free(%rip)
	cmpq	-16(%rbp), %rbx
	jb	.L144
.L143:
	cmpq	%rbp, 4080+_rtld_local(%rip)
	je	.L145
	leaq	-16(%rbp), %rdi
	call	*__rtld_free(%rip)
.L145:
	testb	%r13b, %r13b
	je	.L142
	movq	2496(%r12), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	*__rtld_free(%rip)
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI__dl_deallocate_tls, .-__GI__dl_deallocate_tls
	.globl	_dl_deallocate_tls
	.set	_dl_deallocate_tls,__GI__dl_deallocate_tls
	.section	.rodata.str1.1
.LC14:
	.string	"total + cnt == modid"
.LC15:
	.string	"modid <= dtv[-1].counter"
	.text
	.p2align 4,,15
	.globl	_dl_update_slotinfo
	.hidden	_dl_update_slotinfo
	.type	_dl_update_slotinfo, @function
_dl_update_slotinfo:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
#APP
# 705 "../elf/dl-tls.c" 1
	movq %fs:8,%r10
# 0 "" 2
#NO_APP
	movq	4032+_rtld_local(%rip), %r14
	movq	%r10, %r15
	movq	%rdi, %rdx
	movq	(%r14), %rsi
	movq	%r14, %rcx
	cmpq	%rsi, %rdi
	jb	.L150
	movq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rcx), %rcx
	subq	%r8, %rdx
	movq	(%rcx), %r8
	cmpq	%rdx, %r8
	jbe	.L151
.L150:
	addq	$1, %rdx
	salq	$4, %rdx
	movq	(%rcx,%rdx), %rax
	cmpq	%rax, (%r10)
	movq	%rax, (%rsp)
	jnb	.L163
	movq	%rdi, 40(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L161:
	movq	24(%rsp), %rax
	xorl	%ebp, %ebp
	testq	%rax, %rax
	sete	%bpl
	cmpq	%rbp, %rsi
	jbe	.L153
	salq	$4, %rax
	movq	%rbp, %rdx
	movq	%rax, %r12
	salq	$4, %rdx
	subq	%r14, %r12
	leaq	24(%r14,%rdx), %r13
	leaq	-24(%r12), %rax
	movq	%rax, 32(%rsp)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L155:
	movq	1120(%r12), %rbx
	cmpq	%rsi, %rbx
	jne	.L168
	cmpq	%rbx, -16(%r15)
	jb	.L169
.L157:
	movq	32(%rsp), %rax
	leaq	(%rax,%r13), %rsi
	addq	%r15, %rsi
	movq	8(%rsi), %rdi
	movq	%rsi, 16(%rsp)
	call	*__rtld_free(%rip)
	cmpq	%rbx, 40(%rsp)
	movq	16(%rsp), %rsi
	cmovne	8(%rsp), %r12
	movq	$-1, (%rsi)
	movq	$0, 8(%rsi)
	movq	%r12, 8(%rsp)
.L154:
	movq	(%r14), %rsi
	addq	$1, %rbp
	addq	$16, %r13
	cmpq	%rbp, %rsi
	jbe	.L153
.L159:
	movq	-8(%r13), %rsi
	cmpq	%rsi, (%rsp)
	jb	.L154
	cmpq	%rsi, (%r15)
	jnb	.L154
	movq	0(%r13), %r12
	movq	24(%rsp), %rax
	testq	%r12, %r12
	leaq	(%rax,%rbp), %rsi
	jne	.L155
	cmpq	%rsi, -16(%r15)
	jb	.L154
	movq	32(%rsp), %rax
	addq	$1, %rbp
	leaq	(%rax,%r13), %rsi
	addq	$16, %r13
	leaq	(%r15,%rsi), %rbx
	movq	8(%rbx), %rdi
	call	*__rtld_free(%rip)
	movq	(%r14), %rsi
	movq	$-1, (%rbx)
	movq	$0, 8(%rbx)
	cmpq	%rbp, %rsi
	ja	.L159
	.p2align 4,,10
	.p2align 3
.L153:
	movq	8(%r14), %r14
	addq	%rsi, 24(%rsp)
	testq	%r14, %r14
	je	.L160
	movq	(%r14), %rsi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%r15, %rdi
	call	_dl_resize_dtv
	cmpq	%rbx, -16(%rax)
	movq	%rax, %r15
	jb	.L170
#APP
# 787 "../elf/dl-tls.c" 1
	movq %rax,%fs:8
# 0 "" 2
#NO_APP
	jmp	.L157
.L160:
	movq	(%rsp), %rax
	movq	%rax, (%r15)
.L149:
	movq	8(%rsp), %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L163:
	movq	$0, 8(%rsp)
	jmp	.L149
.L168:
	leaq	__PRETTY_FUNCTION__.10172(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$777, %edx
	call	__GI___assert_fail
.L170:
	leaq	__PRETTY_FUNCTION__.10172(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$783, %edx
	call	__GI___assert_fail
	.size	_dl_update_slotinfo, .-_dl_update_slotinfo
	.p2align 4,,15
	.type	update_get_addr, @function
update_get_addr:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	_dl_update_slotinfo
#APP
# 880 "../elf/dl-tls.c" 1
	movq %fs:8,%rsi
# 0 "" 2
#NO_APP
	movq	(%rbx), %rcx
	salq	$4, %rcx
	movq	(%rsi,%rcx), %rcx
	cmpq	$-1, %rcx
	je	.L174
	movq	8(%rbx), %rax
	popq	%rbx
	addq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rbx, %rdi
	movq	%rax, %rdx
	popq	%rbx
	jmp	tls_get_addr_tail
	.size	update_get_addr, .-update_get_addr
	.p2align 4,,15
	.globl	___tls_get_addr
	.type	___tls_get_addr, @function
___tls_get_addr:
#APP
# 906 "../elf/dl-tls.c" 1
	movq %fs:8,%rsi
# 0 "" 2
#NO_APP
	movq	4088+_rtld_local(%rip), %rax
	cmpq	%rax, (%rsi)
	jne	.L178
	movq	(%rdi), %rax
	salq	$4, %rax
	movq	(%rsi,%rax), %rax
	cmpq	$-1, %rax
	je	.L179
	addq	8(%rdi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	jmp	update_get_addr
	.p2align 4,,10
	.p2align 3
.L179:
	xorl	%edx, %edx
	jmp	tls_get_addr_tail
	.size	___tls_get_addr, .-___tls_get_addr
	.globl	__GI___tls_get_addr
	.set	__GI___tls_get_addr,___tls_get_addr
	.p2align 4,,15
	.globl	_dl_tls_get_addr_soft
	.hidden	_dl_tls_get_addr_soft
	.type	_dl_tls_get_addr_soft, @function
_dl_tls_get_addr_soft:
	movq	1120(%rdi), %rax
	testq	%rax, %rax
	je	.L188
#APP
# 930 "../elf/dl-tls.c" 1
	movq %fs:8,%rdx
# 0 "" 2
#NO_APP
	movq	(%rdx), %rcx
	cmpq	4088+_rtld_local(%rip), %rcx
	jne	.L190
.L182:
	salq	$4, %rax
	movq	(%rdx,%rax), %rax
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	cmpq	-16(%rdx), %rax
	jnb	.L188
	movq	4032+_rtld_local(%rip), %rdi
	movq	%rax, %rsi
	movq	(%rdi), %r8
	cmpq	%r8, %rax
	jb	.L183
	.p2align 4,,10
	.p2align 3
.L184:
	movq	8(%rdi), %rdi
	subq	%r8, %rsi
	movq	(%rdi), %r8
	cmpq	%rsi, %r8
	jbe	.L184
.L183:
	addq	$1, %rsi
	salq	$4, %rsi
	cmpq	(%rdi,%rsi), %rcx
	jnb	.L182
.L188:
	xorl	%eax, %eax
	ret
	.size	_dl_tls_get_addr_soft, .-_dl_tls_get_addr_soft
	.section	.rodata.str1.1
.LC16:
	.string	"idx == 0"
	.section	.rodata.str1.8
	.align 8
.LC17:
	.string	"cannot create TLS data structures"
	.section	.rodata.str1.1
.LC18:
	.string	"dlopen"
	.text
	.p2align 4,,15
	.globl	_dl_add_to_slotinfo
	.hidden	_dl_add_to_slotinfo
	.type	_dl_add_to_slotinfo, @function
_dl_add_to_slotinfo:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	subq	$16, %rsp
	movq	1120(%rdi), %rbx
	movq	4032+_rtld_local(%rip), %rbp
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L204:
	subq	%rax, %rbx
	movq	8(%rbp), %rax
	testq	%rax, %rax
	je	.L203
	movq	%rax, %rbp
.L193:
	movq	0(%rbp), %rax
	cmpq	%rbx, %rax
	jbe	.L204
.L192:
	testb	%sil, %sil
	je	.L191
	movq	4088+_rtld_local(%rip), %rax
	salq	$4, %rbx
	addq	%rbp, %rbx
	movq	%r12, 24(%rbx)
	addq	$1, %rax
	movq	%rax, 16(%rbx)
.L191:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	testq	%rbx, %rbx
	jne	.L205
	movl	%esi, 12(%rsp)
	movl	$1008, %edi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, 8(%rbp)
	movl	12(%rsp), %esi
	je	.L206
	leaq	24(%rax), %rdi
	movq	$62, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	movq	$0, 1000(%rax)
	movq	%rdx, %rbp
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	1008(%rax), %ecx
	movq	%rbx, %rax
	shrl	$3, %ecx
	rep stosq
	jmp	.L192
.L205:
	leaq	__PRETTY_FUNCTION__.10234(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$994, %edx
	call	__GI___assert_fail
.L206:
	leaq	.LC17(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	movl	$12, %edi
	addq	$1, 4088+_rtld_local(%rip)
	call	_dl_signal_error@PLT
	.size	_dl_add_to_slotinfo, .-_dl_add_to_slotinfo
	.p2align 4,,15
	.globl	__tls_get_addr_slow
	.hidden	__tls_get_addr_slow
	.type	__tls_get_addr_slow, @function
__tls_get_addr_slow:
#APP
# 41 "../sysdeps/x86_64/dl-tls.c" 1
	movq %fs:8,%rsi
# 0 "" 2
#NO_APP
	movq	4088+_rtld_local(%rip), %rax
	cmpq	%rax, (%rsi)
	jne	.L209
	xorl	%edx, %edx
	jmp	tls_get_addr_tail
	.p2align 4,,10
	.p2align 3
.L209:
	jmp	update_get_addr
	.size	__tls_get_addr_slow, .-__tls_get_addr_slow
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10234, @object
	.size	__PRETTY_FUNCTION__.10234, 20
__PRETTY_FUNCTION__.10234:
	.string	"_dl_add_to_slotinfo"
	.align 16
	.type	__PRETTY_FUNCTION__.10191, @object
	.size	__PRETTY_FUNCTION__.10191, 18
__PRETTY_FUNCTION__.10191:
	.string	"tls_get_addr_tail"
	.align 16
	.type	__PRETTY_FUNCTION__.10172, @object
	.size	__PRETTY_FUNCTION__.10172, 20
__PRETTY_FUNCTION__.10172:
	.string	"_dl_update_slotinfo"
	.align 16
	.type	__PRETTY_FUNCTION__.10112, @object
	.size	__PRETTY_FUNCTION__.10112, 22
__PRETTY_FUNCTION__.10112:
	.string	"_dl_allocate_tls_init"
	.align 16
	.type	__PRETTY_FUNCTION__.10062, @object
	.size	__PRETTY_FUNCTION__.10062, 24
__PRETTY_FUNCTION__.10062:
	.string	"_dl_determine_tlsoffset"
	.align 16
	.type	__PRETTY_FUNCTION__.10038, @object
	.size	__PRETTY_FUNCTION__.10038, 19
__PRETTY_FUNCTION__.10038:
	.string	"_dl_next_tls_modid"
	.hidden	__rtld_free
	.hidden	__rtld_calloc
	.hidden	_rtld_local_ro
	.hidden	__rtld_realloc
	.hidden	_rtld_local
	.hidden	__rtld_malloc
