	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"AVX"
.LC1:
	.string	"CX8"
.LC2:
	.string	"FMA"
.LC3:
	.string	"HTT"
.LC4:
	.string	"IBT"
.LC5:
	.string	"RTM"
.LC6:
	.string	"LZCNT"
.LC7:
	.string	"MOVBE"
.LC8:
	.string	"SHSTK"
.LC9:
	.string	"SSSE3"
.LC10:
	.string	"POPCNT"
.LC11:
	.string	"SSE4_1"
.LC12:
	.string	"SSE4_2"
.LC13:
	.string	"XSAVEC"
.LC14:
	.string	"AVX512F"
.LC15:
	.string	"OSXSAVE"
.LC16:
	.string	"Prefer_ERMS"
.LC17:
	.string	"Prefer_FSRM"
.LC18:
	.string	"Slow_SSE4_2"
.LC19:
	.string	"Fast_Rep_String"
.LC20:
	.string	"Prefer_No_AVX512"
.LC21:
	.string	"Fast_Copy_Backward"
.LC22:
	.string	"Fast_Unaligned_Load"
.LC23:
	.string	"Fast_Unaligned_Copy"
.LC24:
	.string	"Prefer_No_VZEROUPPER"
.LC25:
	.string	"Prefer_MAP_32BIT_EXEC"
.LC26:
	.string	"AVX_Fast_Unaligned_Load"
.LC27:
	.string	"MathVec_Prefer_No_AVX512"
.LC28:
	.string	"Prefer_PMINUB_for_stringop"
	.text
	.p2align 4,,15
	.globl	_dl_tunable_set_hwcaps
	.hidden	_dl_tunable_set_hwcaps
	.type	_dl_tunable_set_hwcaps, @function
_dl_tunable_set_hwcaps:
	pushq	%r15
	pushq	%r14
	leaq	.L128(%rip), %r8
	pushq	%r13
	pushq	%r12
	movabsq	$6287643216692401729, %r13
	pushq	%rbp
	pushq	%rbx
	movabsq	$5855860602418255425, %r12
	movq	(%rdi), %rax
	movl	400+_rtld_local_ro(%rip), %r14d
	leaq	.L122(%rip), %rdi
	movabsq	$4918830404948481601, %rbx
	movabsq	$5928199671432894017, %rbp
	movabsq	$5067466983515057235, %r11
	movabsq	$5791473425656934992, %r10
	movzbl	(%rax), %edx
	.p2align 4,,10
	.p2align 3
.L121:
	cmpb	$44, %dl
	je	.L129
	testb	%dl, %dl
	je	.L129
	movq	%rax, %rcx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L176:
	cmpb	$44, %sil
	je	.L143
.L3:
	addq	$1, %rcx
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	jne	.L176
.L143:
	subq	%rax, %rcx
	cmpb	$45, %dl
	leaq	1(%rcx), %rsi
	je	.L177
	subq	$5, %rcx
	cmpq	$21, %rcx
	ja	.L2
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L122:
	.long	.L132-.L122
	.long	.L133-.L122
	.long	.L134-.L122
	.long	.L123-.L122
	.long	.L2-.L122
	.long	.L2-.L122
	.long	.L135-.L122
	.long	.L2-.L122
	.long	.L2-.L122
	.long	.L2-.L122
	.long	.L136-.L122
	.long	.L137-.L122
	.long	.L2-.L122
	.long	.L138-.L122
	.long	.L139-.L122
	.long	.L140-.L122
	.long	.L141-.L122
	.long	.L2-.L122
	.long	.L142-.L122
	.long	.L131-.L122
	.long	.L2-.L122
	.long	.L130-.L122
	.text
.L7:
	cmpw	$22081, (%r9)
	je	.L178
.L8:
	cmpw	$22595, (%r9)
	je	.L179
.L11:
	cmpw	$19782, (%r9)
	je	.L180
.L14:
	cmpw	$21576, (%r9)
	je	.L181
.L17:
	cmpw	$16969, (%r9)
	je	.L182
.L20:
	cmpw	$21586, (%r9)
	jne	.L2
	cmpb	$77, 2(%r9)
	jne	.L2
	andl	$-2049, 176+_rtld_local_ro(%rip)
	.p2align 4,,10
	.p2align 3
.L2:
	addq	%rsi, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L121
.L183:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	subq	$4, %rcx
	leaq	1(%rax), %r9
	cmpq	$23, %rcx
	ja	.L2
	movslq	(%r8,%rcx,4), %rcx
	addq	%r8, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L128:
	.long	.L7-.L128
	.long	.L125-.L128
	.long	.L151-.L128
	.long	.L152-.L128
	.long	.L153-.L128
	.long	.L62-.L128
	.long	.L2-.L128
	.long	.L2-.L128
	.long	.L69-.L128
	.long	.L2-.L128
	.long	.L2-.L128
	.long	.L2-.L128
	.long	.L81-.L128
	.long	.L85-.L128
	.long	.L2-.L128
	.long	.L89-.L128
	.long	.L93-.L128
	.long	.L101-.L128
	.long	.L105-.L128
	.long	.L2-.L128
	.long	.L109-.L128
	.long	.L113-.L128
	.long	.L2-.L128
	.long	.L117-.L128
	.text
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$1, %esi
	addq	%rsi, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L121
	jmp	.L183
.L123:
	cmpq	%r11, (%rax)
	jne	.L2
	orl	$64, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L134:
	cmpb	$45, %dl
	movq	%rax, %r9
	jne	.L2
.L153:
	cmpl	$894981697, (%r9)
	je	.L184
.L57:
	cmpl	$1398297423, (%r9)
	jne	.L2
	cmpw	$22081, 4(%r9)
	jne	.L2
	cmpb	$69, 6(%r9)
	jne	.L2
	andl	$-134217729, 148+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$45, %dl
	movq	%rax, %r9
	jne	.L2
.L152:
	cmpl	$1129336656, (%r9)
	je	.L185
.L45:
	cmpl	$876958547, (%r9)
	je	.L186
.L48:
	cmpl	$876958547, (%r9)
	je	.L187
.L51:
	cmpl	$1447121752, (%r9)
	jne	.L2
	cmpw	$17221, 4(%r9)
	jne	.L2
	movq	%r14, 392+_rtld_local_ro(%rip)
	andl	$-3, 236+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L132:
	cmpb	$45, %dl
	movq	%rax, %r9
	jne	.L2
.L151:
	cmpl	$1313036876, (%r9)
	je	.L188
.L33:
	cmpl	$1112952653, (%r9)
	je	.L189
.L36:
	cmpl	$1414744147, (%r9)
	je	.L190
.L39:
	cmpl	$1163088723, (%r9)
	jne	.L2
	cmpb	$51, 4(%r9)
	jne	.L2
	andl	$-513, 148+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rax, %r9
.L113:
	movabsq	$5647358237581079120, %r15
	movabsq	$6873448878091559245, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	movabsq	$3616730629275737967, %rcx
	cmpq	%rcx, 16(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L191
	testb	$1, 178+_rtld_local_ro(%rip)
	je	.L2
	orl	$32768, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L130:
	movq	%rax, %r9
.L117:
	movabsq	$8027208124338161997, %rcx
	movq	(%r9), %r15
	xorq	8(%r9), %rcx
	xorq	%r10, %r15
	orq	%r15, %rcx
	jne	.L2
	movabsq	$7453010373645655922, %rcx
	cmpq	%rcx, 16(%r9)
	jne	.L2
	cmpw	$28783, 24(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L192
	testb	$4, 155+_rtld_local_ro(%rip)
	je	.L2
	orl	$1024, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L142:
	movq	%rax, %r9
.L109:
	movabsq	$7955443180985275743, %r15
	movabsq	$8391157485596595777, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpl	$1281320037, 16(%r9)
	jne	.L2
	cmpw	$24943, 20(%r9)
	jne	.L2
	cmpb	$100, 22(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L193
	testb	$16, 151+_rtld_local_ro(%rip)
	je	.L2
	orl	$256, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L141:
	movq	%rax, %r9
.L105:
	movabsq	$6073458355863507009, %r15
	movabsq	$5575300643543151184, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpl	$1163412831, 16(%r9)
	jne	.L2
	cmpb	$67, 20(%r9)
	jne	.L2
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andb	$-3, %ch
	orl	$512, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%rax, %r9
.L101:
	movabsq	$6147222474205847407, %r15
	movabsq	$5647358237581079120, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpl	$1380274256, 16(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L194
	testb	$16, 151+_rtld_local_ro(%rip)
	je	.L2
	orl	$2048, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L139:
	movq	%rax, %r9
.L93:
	movabsq	$5503227656476780908, %r15
	movabsq	$7020642737581154630, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L94
	cmpw	$24943, 16(%r9)
	je	.L195
.L94:
	movabsq	$4854709310135429484, %r15
	movabsq	$7020642737581154630, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpw	$28783, 16(%r9)
	jne	.L2
	cmpb	$121, 18(%r9)
	jne	.L2
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-33, %ecx
	orl	$32, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rax, %r9
.L89:
	movabsq	$7023200218485251961, %r15
	movabsq	$8101768331917484358, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpw	$25714, 16(%r9)
	jne	.L2
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-9, %ecx
	orl	$8, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L136:
	movq	%rax, %r9
.L81:
	movabsq	$8098970074824794438, %rcx
	cmpq	%rcx, (%r9)
	jne	.L2
	cmpl	$1920226143, 8(%r9)
	jne	.L2
	cmpw	$28265, 12(%r9)
	jne	.L2
	cmpb	$103, 14(%r9)
	jne	.L2
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-5, %ecx
	orl	$4, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rax, %r9
.L69:
	movabsq	$4998839891239727696, %rcx
	cmpq	%rcx, (%r9)
	je	.L196
.L70:
	movabsq	$5070897485277655632, %rcx
	cmpq	%rcx, (%r9)
	je	.L197
.L74:
	movabsq	$4995428081174801491, %rcx
	cmpq	%rcx, (%r9)
	jne	.L2
	cmpw	$24372, 8(%r9)
	jne	.L2
	cmpb	$50, 10(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L198
	testb	$16, 150+_rtld_local_ro(%rip)
	je	.L2
	orl	$128, 380+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rax, %r9
.L85:
	movabsq	$3616730629275737967, %r15
	movabsq	$5647358237581079120, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpb	$45, %dl
	je	.L199
	testb	$1, 178+_rtld_local_ro(%rip)
	je	.L2
	orl	$16384, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L125:
	cmpl	$844650049, 1(%rax)
	jne	.L200
	andl	$-33, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L62:
	cmpq	%rbx, 1(%rax)
	jne	.L63
	andl	$-268435457, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L195:
	cmpb	$100, 18(%r9)
	jne	.L94
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-17, %ecx
	orl	$16, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L199:
	andl	$-16385, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L196:
	cmpw	$19794, 8(%r9)
	jne	.L70
	cmpb	$83, 10(%r9)
	jne	.L70
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andb	$-17, %ch
	orl	$4096, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L197:
	cmpw	$21075, 8(%r9)
	jne	.L74
	cmpb	$77, 10(%r9)
	jne	.L74
	movl	380+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %r9d
	andb	$-33, %ch
	orl	$8192, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L200:
	cmpl	$826887490, 1(%rax)
	je	.L201
	cmpl	$843664706, 1(%rax)
	jne	.L26
	andl	$-257, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L63:
	cmpq	%r13, 1(%rax)
	je	.L202
	cmpq	%r12, 1(%rax)
	jne	.L65
	andl	$-131073, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L191:
	andl	$-32769, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L194:
	andl	$-2049, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L184:
	cmpw	$12849, 4(%r9)
	jne	.L57
	cmpb	$70, 6(%r9)
	jne	.L57
	andl	$-65537, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L178:
	cmpb	$88, 2(%r9)
	jne	.L8
	andl	$-268435457, 148+_rtld_local_ro(%rip)
	jmp	.L2
.L188:
	cmpb	$84, 4(%r9)
	jne	.L33
	andl	$-33, 212+_rtld_local_ro(%rip)
	jmp	.L2
.L185:
	cmpw	$21582, 4(%r9)
	jne	.L45
	andl	$-8388609, 148+_rtld_local_ro(%rip)
	jmp	.L2
.L202:
	andl	$-1073741825, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L201:
	andl	$-9, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L187:
	cmpw	$12895, 4(%r9)
	jne	.L51
	andl	$-1048577, 148+_rtld_local_ro(%rip)
	jmp	.L2
.L190:
	cmpb	$75, 4(%r9)
	jne	.L39
	andl	$-129, 180+_rtld_local_ro(%rip)
	jmp	.L2
.L180:
	cmpb	$65, 2(%r9)
	jne	.L14
	andl	$-4097, 148+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L192:
	andl	$-1025, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L193:
	andl	$-257, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L186:
	cmpw	$12639, 4(%r9)
	jne	.L48
	andl	$-524289, 148+_rtld_local_ro(%rip)
	jmp	.L2
.L179:
	cmpb	$56, 2(%r9)
	jne	.L11
	andl	$-257, 152+_rtld_local_ro(%rip)
	jmp	.L2
.L189:
	cmpb	$69, 4(%r9)
	jne	.L36
	andl	$-4194305, 148+_rtld_local_ro(%rip)
	jmp	.L2
.L65:
	cmpq	%rbp, 1(%rax)
	jne	.L66
	andl	$-134217729, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L26:
	cmpl	$1448037699, 1(%rax)
	jne	.L27
	andl	$-32769, 152+_rtld_local_ro(%rip)
	jmp	.L2
.L198:
	andl	$-129, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L66:
	movabsq	$5066604767721576001, %rdx
	cmpq	%rdx, 1(%rax)
	jne	.L67
	andl	$-67108865, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L27:
	cmpl	$1397576261, 1(%rax)
	jne	.L28
	andl	$-513, 176+_rtld_local_ro(%rip)
	jmp	.L2
.L67:
	movabsq	$5500639181809407553, %rdx
	cmpq	%rdx, 1(%rax)
	jne	.L68
	andl	$2147483647, 176+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	$876694854, 1(%rax)
	jne	.L29
	andl	$-65537, 212+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L182:
	cmpb	$84, 2(%r9)
	jne	.L20
	andl	$-1048577, 184+_rtld_local_ro(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L181:
	cmpb	$84, 2(%r9)
	jne	.L17
	andl	$-268435457, 152+_rtld_local_ro(%rip)
	jmp	.L2
.L68:
	cmpq	%r11, 1(%rax)
	jne	.L2
	andl	$-65, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L29:
	cmpl	$843404115, 1(%rax)
	jne	.L30
	andl	$-67108865, 152+_rtld_local_ro(%rip)
	jmp	.L2
.L30:
	cmpl	$909653321, 1(%rax)
	jne	.L31
	andl	$-2, 380+_rtld_local_ro(%rip)
	jmp	.L2
.L31:
	cmpl	$909653577, 1(%rax)
	jne	.L2
	andl	$-3, 380+_rtld_local_ro(%rip)
	jmp	.L2
	.size	_dl_tunable_set_hwcaps, .-_dl_tunable_set_hwcaps
	.p2align 4,,15
	.globl	_dl_sysdep_start
	.hidden	_dl_sysdep_start
	.type	_dl_sysdep_start, @function
_dl_sysdep_start:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	(%rdi), %rax
	movq	%rdi, __GI___libc_stack_end(%rip)
	addq	$8, %rdi
	movq	%rdi, __GI__dl_argv(%rip)
	movl	%eax, _dl_argc(%rip)
	cltq
	leaq	8(%rdi,%rax,8), %rdi
	movq	%rdi, _environ(%rip)
	cmpq	$0, (%rdi)
	movq	%rdi, %rax
	je	.L204
.L205:
	addq	$8, %rax
	cmpq	$0, (%rax)
	jne	.L205
.L204:
	leaq	8(%rax), %rdx
	movq	8(%rax), %rax
	leaq	_start(%rip), %rsi
	movq	$0, 8+_rtld_local_ro(%rip)
	movq	%rdx, 96+_rtld_local_ro(%rip)
	movq	%rsi, 56(%rsp)
	testq	%rax, %rax
	je	.L236
	movq	672+_rtld_local_ro(%rip), %rbx
	movq	88+_rtld_local_ro(%rip), %rcx
	xorl	%r10d, %r10d
	movl	__GI___libc_enable_secure(%rip), %r11d
	movq	24+_rtld_local_ro(%rip), %r15
	xorl	%r9d, %r9d
	movb	$0, 47(%rsp)
	movb	$0, 46(%rsp)
	xorl	%r13d, %r13d
	movq	%rbx, 24(%rsp)
	movq	728+_rtld_local_ro(%rip), %rbx
	xorl	%r14d, %r14d
	movq	%rcx, (%rsp)
	leaq	.L209(%rip), %rcx
	movb	$0, 45(%rsp)
	movb	$0, 44(%rsp)
	movb	$0, 41(%rsp)
	xorl	%r8d, %r8d
	movq	%rbx, 8(%rsp)
	movq	_dl_random(%rip), %rbx
	xorl	%ebp, %ebp
	movb	$0, 19(%rsp)
	movq	%rbx, 32(%rsp)
	movzwl	80+_rtld_local_ro(%rip), %ebx
	movw	%bx, 42(%rsp)
	movl	56+_rtld_local_ro(%rip), %ebx
	movl	%ebx, 20(%rsp)
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L221:
	subq	$3, %rax
	cmpq	$30, %rax
	ja	.L207
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L209:
	.long	.L208-.L209
	.long	.L207-.L209
	.long	.L210-.L209
	.long	.L211-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L212-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L213-.L209
	.long	.L214-.L209
	.long	.L215-.L209
	.long	.L216-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L217-.L209
	.long	.L207-.L209
	.long	.L218-.L209
	.long	.L219-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L207-.L209
	.long	.L220-.L209
	.text
	.p2align 4,,10
	.p2align 3
.L208:
	movq	8(%rdx), %rbx
.L207:
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	.p2align 4,,10
	.p2align 3
.L274:
	testb	%r8b, %r8b
	jne	.L273
.L222:
	testb	%r14b, %r14b
	je	.L223
	movq	%rsi, 56(%rsp)
.L223:
	testb	%r13b, %r13b
	je	.L224
	movl	%r11d, __GI___libc_enable_secure(%rip)
.L224:
	testb	%r10b, %r10b
	je	.L225
	movq	%r9, 8+_rtld_local_ro(%rip)
.L225:
	cmpb	$0, 19(%rsp)
	je	.L226
	movq	(%rsp), %rax
	movq	%rax, 88+_rtld_local_ro(%rip)
.L226:
	cmpb	$0, 41(%rsp)
	je	.L227
	movq	8(%rsp), %rax
	movq	%rax, 728+_rtld_local_ro(%rip)
.L227:
	cmpb	$0, 44(%rsp)
	je	.L228
	movl	20(%rsp), %eax
	movl	%eax, 56+_rtld_local_ro(%rip)
.L228:
	cmpb	$0, 45(%rsp)
	je	.L229
	movzwl	42(%rsp), %eax
	movw	%ax, 80+_rtld_local_ro(%rip)
.L229:
	cmpb	$0, 46(%rsp)
	je	.L230
	movq	24(%rsp), %rax
	movq	%rax, 672+_rtld_local_ro(%rip)
.L230:
	cmpb	$0, 47(%rsp)
	je	.L206
	movq	32(%rsp), %rax
	movq	%rax, _dl_random(%rip)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L220:
	movq	8(%rdx), %rax
	addq	$16, %rdx
	movb	$1, 46(%rsp)
	movq	%rax, 24(%rsp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L219:
	movq	8(%rdx), %rax
	addq	$16, %rdx
	movb	$1, 41(%rsp)
	movq	%rax, 8(%rsp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L218:
	movq	8(%rdx), %rax
	addq	$16, %rdx
	movb	$1, 47(%rsp)
	movq	%rax, 32(%rsp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L217:
	movl	8(%rdx), %r11d
	addq	$16, %rdx
	movq	(%rdx), %rax
	movl	$1, %r13d
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L216:
	movzwl	8(%rdx), %eax
	addq	$16, %rdx
	movb	$1, 45(%rsp)
	movw	%ax, 42(%rsp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L215:
	movl	8(%rdx), %eax
	addq	$16, %rdx
	movb	$1, 44(%rsp)
	movl	%eax, 20(%rsp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L214:
	movq	8(%rdx), %rax
	addq	$16, %rdx
	movb	$1, 19(%rsp)
	movq	%rax, (%rsp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L213:
	movq	8(%rdx), %r9
	addq	$16, %rdx
	movq	(%rdx), %rax
	movl	$1, %r10d
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L212:
	movq	8(%rdx), %rsi
	addq	$16, %rdx
	movq	(%rdx), %rax
	movl	$1, %r14d
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L211:
	movq	8(%rdx), %r15
	addq	$16, %rdx
	movq	(%rdx), %rax
	movl	$1, %r8d
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L210:
	movl	8(%rdx), %ebp
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L221
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L236:
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
.L206:
	call	__GI___tunables_init
	xorl	%edi, %edi
	call	__brk
	call	_dl_x86_init_cpu_features
	movq	8+_rtld_local_ro(%rip), %rdi
	testq	%rdi, %rdi
	je	.L232
	call	strlen
	movq	%rax, 16+_rtld_local_ro(%rip)
.L232:
	xorl	%edi, %edi
	call	__sbrk
	leaq	_end(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L275
	movl	__GI___libc_enable_secure(%rip), %eax
	testl	%eax, %eax
	jne	.L276
.L234:
	leaq	56(%rsp), %rdx
	movl	%ebp, %esi
	movq	%rbx, %rdi
	movq	96+_rtld_local_ro(%rip), %rcx
	call	*%r12
	movq	56(%rsp), %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L275:
	movq	24+_rtld_local_ro(%rip), %rdi
	leaq	-1(%rdi), %rdx
	andq	%rdx, %rax
	subq	%rax, %rdi
	call	__sbrk
	movl	__GI___libc_enable_secure(%rip), %eax
	testl	%eax, %eax
	je	.L234
	.p2align 4,,10
	.p2align 3
.L276:
	call	__libc_check_standard_fds
	jmp	.L234
.L273:
	movq	%r15, 24+_rtld_local_ro(%rip)
	jmp	.L222
	.size	_dl_sysdep_start, .-_dl_sysdep_start
	.p2align 4,,15
	.globl	_dl_sysdep_start_cleanup
	.hidden	_dl_sysdep_start_cleanup
	.type	_dl_sysdep_start_cleanup, @function
_dl_sysdep_start_cleanup:
	rep ret
	.size	_dl_sysdep_start_cleanup, .-_dl_sysdep_start_cleanup
	.section	.rodata.str1.1
.LC29:
	.string	"AT_%s%s\n"
.LC30:
	.string	"AT_??? (0x%s): 0x%s\n"
	.text
	.p2align 4,,15
	.globl	_dl_show_auxv
	.hidden	_dl_show_auxv
	.type	_dl_show_auxv, @function
_dl_show_auxv:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$96, %rsp
	movq	96+_rtld_local_ro(%rip), %r14
	movb	$0, 95(%rsp)
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L278
	leaq	__GI__itoa_lower_digits(%rip), %r13
	leaq	auxvars.10484(%rip), %rbp
	movq	%rsp, %r12
	movabsq	$-3689348814741910323, %rbx
	.p2align 4,,10
	.p2align 3
.L289:
	cmpl	$1, %eax
	leal	-2(%rax), %esi
	jbe	.L288
	cmpl	$45, %esi
	ja	.L281
	leaq	(%rsi,%rsi,2), %rax
	movzbl	22(%rbp,%rax,8), %eax
	cmpb	$4, %al
	je	.L288
	testb	%al, %al
	je	.L281
	cmpq	$1, %rax
	movq	8(%r14), %rcx
	leaq	95(%rsp), %rdi
	jne	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	movq	%rcx, %rax
	subq	$1, %rdi
	mulq	%rbx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	testq	%rdx, %rdx
	movzbl	0(%r13,%rcx), %eax
	movq	%rdx, %rcx
	movb	%al, (%rdi)
	jne	.L283
.L284:
	leaq	(%rsi,%rsi,2), %rax
	movq	%rdi, %rdx
	leaq	.LC29(%rip), %rdi
	leaq	0(%rbp,%rax,8), %rsi
	xorl	%eax, %eax
	call	_dl_printf
.L288:
	addq	$16, %r14
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L289
.L278:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L281:
	movb	$0, 16(%rsp)
	movq	8(%r14), %rax
	leaq	16(%r12), %rdx
	.p2align 4,,10
	.p2align 3
.L286:
	movq	%rax, %rcx
	shrq	$4, %rax
	subq	$1, %rdx
	andl	$15, %ecx
	testq	%rax, %rax
	movzbl	0(%r13,%rcx), %ecx
	movb	%cl, (%rdx)
	jne	.L286
	movq	(%r14), %rax
	leaq	95(%rsp), %rsi
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%rax, %rcx
	shrq	$4, %rax
	subq	$1, %rsi
	andl	$15, %ecx
	testq	%rax, %rax
	movzbl	0(%r13,%rcx), %ecx
	movb	%cl, (%rsi)
	jne	.L287
	leaq	.LC30(%rip), %rdi
	call	_dl_printf
	jmp	.L288
.L282:
	cmpq	$2, %rax
	jne	.L306
	.p2align 4,,10
	.p2align 3
.L285:
	movq	%rcx, %rax
	shrq	$4, %rcx
	subq	$1, %rdi
	andl	$15, %eax
	testq	%rcx, %rcx
	movzbl	0(%r13,%rax), %eax
	movb	%al, (%rdi)
	jne	.L285
	jmp	.L284
.L306:
	movq	%rcx, %rdi
	jmp	.L284
	.size	_dl_show_auxv, .-_dl_show_auxv
	.section	.rodata.str1.1
.LC31:
	.string	"/proc/sys/kernel/osrelease"
	.text
	.p2align 4,,15
	.globl	_dl_discover_osversion
	.hidden	_dl_discover_osversion
	.type	_dl_discover_osversion, @function
_dl_discover_osversion:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$464, %rsp
	movq	680+_rtld_local_ro(%rip), %rbx
	testq	%rbx, %rbx
	je	.L308
	movzwl	696(%rbx), %r11d
	movq	680(%rbx), %rsi
	testq	%r11, %r11
	je	.L308
	xorl	%edi, %edi
	movl	$12, %ebp
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$1, %rdi
	addq	$56, %rsi
	cmpq	%r11, %rdi
	je	.L308
.L315:
	cmpl	$4, (%rsi)
	jne	.L309
	movq	(%rbx), %r8
	movq	40(%rsi), %r9
	addq	16(%rsi), %r8
	cmpq	%r9, %rbp
	movq	%r8, %rax
	jnb	.L309
	leaq	expected_note.10501(%rip), %r10
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L336:
	movl	(%rax), %ecx
	movl	4(%rax), %edx
	addq	$3, %rcx
	addq	$3, %rdx
	andq	$-4, %rcx
	andq	$-4, %rdx
	leaq	12(%rcx,%rdx), %rdx
	addq	%rdx, %rax
	leaq	12(%rax), %rdx
	subq	%r8, %rdx
	cmpq	%r9, %rdx
	jnb	.L309
.L314:
	movq	(%rax), %rdx
	movq	8(%rax), %rcx
	xorq	(%r10), %rdx
	xorq	8+expected_note.10501(%rip), %rcx
	orq	%rdx, %rcx
	jne	.L336
	movl	16+expected_note.10501(%rip), %ecx
	cmpl	%ecx, 16(%rax)
	jne	.L336
	movl	20(%rax), %eax
.L307:
	addq	$464, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	call	__uname
	testl	%eax, %eax
	leaq	130(%rbx), %rsi
	jne	.L347
.L316:
	xorl	%edi, %edi
	xorl	%eax, %eax
.L320:
	movsbl	(%rsi), %ecx
	leal	-48(%rcx), %edx
	cmpb	$9, %dl
	ja	.L326
	movsbl	1(%rsi), %edx
	leaq	1(%rsi), %r8
	subl	$48, %ecx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L321
	.p2align 4,,10
	.p2align 3
.L322:
	leal	(%rcx,%rcx,4), %ecx
	addq	$1, %r8
	leal	-48(%rdx,%rcx,2), %ecx
	movsbl	(%r8), %edx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	jbe	.L322
.L321:
	sall	$8, %eax
	addl	$1, %edi
	leaq	1(%r8), %rsi
	orl	%ecx, %eax
	cmpb	$46, %dl
	jne	.L323
	cmpl	$3, %edi
	jne	.L320
	addq	$464, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L323:
	cmpl	$3, %edi
	je	.L307
.L326:
	movl	$3, %ecx
	addq	$464, %rsp
	subl	%edi, %ecx
	sall	$3, %ecx
	popq	%rbx
	sall	%cl, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L347:
	leaq	.LC31(%rip), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__GI___open64_nocancel
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L318
	movq	%rsp, %rbx
	movl	$64, %edx
	movl	%eax, %edi
	movq	%rbx, %rsi
	call	__GI___read_nocancel
	movl	%r12d, %edi
	movq	%rax, %rbp
	call	__GI___close_nocancel
	testq	%rbp, %rbp
	jle	.L318
	cmpq	$63, %rbp
	movl	$63, %eax
	movq	%rbx, %rsi
	cmovge	%rax, %rbp
	movb	$0, (%rsp,%rbp)
	jmp	.L316
.L318:
	movl	$-1, %eax
	jmp	.L307
	.size	_dl_discover_osversion, .-_dl_discover_osversion
	.section	.rodata
	.align 16
	.type	expected_note.10501, @object
	.size	expected_note.10501, 20
expected_note.10501:
	.long	6
	.long	4
	.long	0
	.string	"Linux"
	.zero	2
	.align 32
	.type	auxvars.10484, @object
	.size	auxvars.10484, 1104
auxvars.10484:
	.string	"EXECFD:            "
	.zero	2
	.byte	1
	.zero	1
	.string	"PHDR:              0x"
	.byte	2
	.zero	1
	.string	"PHENT:             "
	.zero	2
	.byte	1
	.zero	1
	.string	"PHNUM:             "
	.zero	2
	.byte	1
	.zero	1
	.string	"PAGESZ:            "
	.zero	2
	.byte	1
	.zero	1
	.string	"BASE:              0x"
	.byte	2
	.zero	1
	.string	"FLAGS:             0x"
	.byte	2
	.zero	1
	.string	"ENTRY:             0x"
	.byte	2
	.zero	1
	.string	"NOTELF:            "
	.zero	2
	.byte	2
	.zero	1
	.string	"UID:               "
	.zero	2
	.byte	1
	.zero	1
	.string	"EUID:              "
	.zero	2
	.byte	1
	.zero	1
	.string	"GID:               "
	.zero	2
	.byte	1
	.zero	1
	.string	"EGID:              "
	.zero	2
	.byte	1
	.zero	1
	.string	"PLATFORM:          "
	.zero	2
	.byte	3
	.zero	1
	.string	"HWCAP:             "
	.zero	2
	.byte	2
	.zero	1
	.string	"CLKTCK:            "
	.zero	2
	.byte	1
	.zero	1
	.string	"FPUCW:             "
	.zero	2
	.byte	2
	.zero	1
	.string	"DCACHEBSIZE:       0x"
	.byte	2
	.zero	1
	.string	"ICACHEBSIZE:       0x"
	.byte	2
	.zero	1
	.string	"UCACHEBSIZE:       0x"
	.byte	2
	.zero	1
	.string	"IGNOREPPC"
	.zero	12
	.byte	4
	.zero	1
	.string	"SECURE:            "
	.zero	2
	.byte	1
	.zero	1
	.string	"BASE_PLATFORM:     "
	.zero	2
	.byte	3
	.zero	1
	.string	"RANDOM:            0x"
	.byte	2
	.zero	1
	.string	"HWCAP2:            0x"
	.byte	2
	.zero	1
	.zero	96
	.string	"EXECFN:            "
	.zero	2
	.byte	3
	.zero	1
	.string	"SYSINFO:           0x"
	.byte	2
	.zero	1
	.string	"SYSINFO_EHDR:      0x"
	.byte	2
	.zero	1
	.zero	144
	.string	"L1I_CACHESIZE:     "
	.zero	2
	.byte	1
	.zero	1
	.string	"L1I_CACHEGEOMETRY: 0x"
	.byte	2
	.zero	1
	.string	"L1D_CACHESIZE:     "
	.zero	2
	.byte	1
	.zero	1
	.string	"L1D_CACHEGEOMETRY: 0x"
	.byte	2
	.zero	1
	.string	"L2_CACHESIZE:      "
	.zero	2
	.byte	1
	.zero	1
	.string	"L2_CACHEGEOMETRY:  0x"
	.byte	2
	.zero	1
	.string	"L3_CACHESIZE:      "
	.zero	2
	.byte	1
	.zero	1
	.string	"L3_CACHEGEOMETRY:  0x"
	.byte	2
	.zero	1
	.hidden	_dl_random
	.globl	_dl_random
	.section	.data.rel.ro,"aw",@progbits
	.align 8
	.type	_dl_random, @object
	.size	_dl_random, 8
_dl_random:
	.zero	8
	.hidden	__GI___libc_stack_end
	.globl	__GI___libc_stack_end
	.align 8
	.type	__GI___libc_stack_end, @object
	.size	__GI___libc_stack_end, 8
__GI___libc_stack_end:
	.zero	8
	.globl	__libc_stack_end
	.set	__libc_stack_end,__GI___libc_stack_end
	.hidden	__GI___libc_enable_secure
	.globl	__GI___libc_enable_secure
	.align 4
	.type	__GI___libc_enable_secure, @object
	.size	__GI___libc_enable_secure, 4
__GI___libc_enable_secure:
	.zero	4
	.globl	__libc_enable_secure
	.set	__libc_enable_secure,__GI___libc_enable_secure
	.hidden	__uname
	.hidden	_dl_printf
	.hidden	__libc_check_standard_fds
	.hidden	_end
	.hidden	__sbrk
	.hidden	strlen
	.hidden	_dl_x86_init_cpu_features
	.hidden	__brk
	.hidden	_start
	.hidden	_environ
	.hidden	_dl_argc
	.hidden	_rtld_local_ro
