	.text
	.p2align 4,,15
	.globl	__brk
	.hidden	__brk
	.type	__brk, @function
__brk:
	movl	$12, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/brk.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	%rdi, %rax
	movq	%rax, __curbrk(%rip)
	jb	.L5
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$12, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.size	__brk, .-__brk
	.weak	brk
	.set	brk,__brk
	.hidden	__curbrk
	.globl	__curbrk
	.bss
	.align 8
	.type	__curbrk, @object
	.size	__curbrk, 8
__curbrk:
	.zero	8
	.hidden	rtld_errno
