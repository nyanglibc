	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
.LC1:
	.string	"dl-fini.c"
.LC2:
	.string	"i < nloaded"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"ns != LM_ID_BASE || i == nloaded"
	.align 8
.LC4:
	.string	"ns == LM_ID_BASE || i == nloaded || i == nloaded - 1"
	.section	.rodata.str1.1
.LC5:
	.string	"\ncalling fini: %s [%lu]\n\n"
	.text
	.p2align 4,,15
	.globl	_dl_fini
	.hidden	_dl_fini
	.type	_dl_fini, @function
_dl_fini:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	movq	_dl_nns(%rip), %rax
	movq	%rax, %r13
	subq	$1, %r13
	js	.L1
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rdx
	leaq	_dl_ns(%rip), %rax
	leaq	-152(%rax,%rdx,8), %rax
	movq	%rax, -56(%rbp)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L55:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L5
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L5:
	subq	$1, %r13
	subq	$152, -56(%rbp)
	cmpq	$-1, %r13
	je	.L1
.L26:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L3
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L3:
	movq	-56(%rbp), %rax
	movl	8(%rax), %esi
	testl	%esi, %esi
	je	.L55
	movl	%esi, %eax
	movq	%rsp, -72(%rbp)
	xorl	%r12d, %r12d
	leaq	22(,%rax,8), %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	movq	-56(%rbp), %rax
	movq	%rsp, %rbx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L9
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L6
.L9:
	cmpq	%rax, 40(%rax)
	jne	.L7
	cmpl	%r12d, %esi
	jbe	.L56
	movl	%r12d, %edx
	movq	%rax, (%rbx,%rdx,8)
	movl	%r12d, 1012(%rax)
	addl	$1, %r12d
	addl	$1, 792(%rax)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L9
.L6:
	cmpl	%r12d, %esi
	sete	%dl
	testq	%r13, %r13
	jne	.L10
	testb	%dl, %dl
	je	.L57
.L10:
	testq	%r13, %r13
	sete	%al
	testb	%dl, %dl
	jne	.L11
	testb	%al, %al
	jne	.L11
	subl	$1, %esi
	cmpl	%r12d, %esi
	jne	.L58
	movq	%rbx, %rdi
.L12:
	xorl	%edx, %edx
	movl	$1, %ecx
	call	_dl_sort_maps
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L13
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L13:
	testl	%r12d, %r12d
	je	.L14
	leal	-1(%r12), %eax
	leaq	8(%rbx,%rax,8), %rax
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rbx), %r15
	movzbl	796(%r15), %eax
	testb	$8, %al
	je	.L16
	andl	$-9, %eax
	movb	%al, 796(%r15)
	movq	272(%r15), %rax
	testq	%rax, %rax
	je	.L59
	testb	$2, _dl_debug_mask(%rip)
	jne	.L27
.L29:
	movq	8(%rax), %r12
	movq	288(%r15), %rax
	addq	(%r15), %r12
	movq	8(%rax), %rax
	shrq	$3, %rax
	testl	%eax, %eax
	leal	-1(%rax), %edx
	je	.L21
	leaq	(%r12,%rdx,8), %r14
	subq	$8, %r12
	.p2align 4,,10
	.p2align 3
.L24:
	call	*(%r14)
	subq	$8, %r14
	cmpq	%r12, %r14
	jne	.L24
.L21:
	movq	168(%r15), %rax
	testq	%rax, %rax
	je	.L16
.L28:
	movq	8(%rax), %rax
	addq	(%r15), %rax
	call	*%rax
.L16:
	subl	$1, 792(%r15)
	addq	$8, %rbx
	cmpq	%rbx, -64(%rbp)
	jne	.L25
.L14:
	movq	-72(%rbp), %rsp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L59:
	movq	168(%r15), %rax
	testq	%rax, %rax
	je	.L16
	testb	$2, _dl_debug_mask(%rip)
	je	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	movq	8(%r15), %rsi
	cmpb	$0, (%rsi)
	jne	.L20
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L20:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%r13, %rdx
	call	_dl_debug_printf
	movq	272(%r15), %rax
	testq	%rax, %rax
	je	.L21
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L11:
	movzbl	%al, %eax
	movl	%r12d, %esi
	leaq	8(%rbx), %rdi
	subl	%eax, %esi
	testq	%r13, %r13
	cmovne	%rbx, %rdi
	jmp	.L12
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L56:
	leaq	__PRETTY_FUNCTION__.9057(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$78, %edx
	call	__assert_fail
.L58:
	leaq	__PRETTY_FUNCTION__.9057(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$89, %edx
	call	__assert_fail
.L57:
	leaq	__PRETTY_FUNCTION__.9057(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$88, %edx
	call	__assert_fail
	.size	_dl_fini, .-_dl_fini
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9057, @object
	.size	__PRETTY_FUNCTION__.9057, 9
__PRETTY_FUNCTION__.9057:
	.string	"_dl_fini"
	.weak	__pthread_mutex_lock
	.weak	__pthread_mutex_unlock
	.hidden	__assert_fail
	.hidden	_dl_debug_printf
	.hidden	_dl_sort_maps
