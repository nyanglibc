	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal error: length accounting in _dl_exception_create_format\n"
	.text
	.p2align 4,,15
	.type	length_mismatch, @function
length_mismatch:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.size	length_mismatch, .-length_mismatch
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.text
	.p2align 4,,15
	.globl	__GI__dl_exception_create
	.hidden	__GI__dl_exception_create
	.type	__GI__dl_exception_create, @function
__GI__dl_exception_create:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L10
	movq	%rsi, %rdi
	movq	%rsi, %r13
	call	strlen
	leaq	1(%rax), %r12
.L5:
	movq	%r14, %rdi
	call	strlen
	leaq	1(%rax), %rbx
	leaq	(%rbx,%r12), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L6
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, 0(%rbp)
	movq	_rtld_local(%rip), %rax
	movq	%r15, 8(%rbp)
	testq	%rax, %rax
	je	.L7
	testb	$4, 796(%rax)
	jne	.L8
.L7:
	movq	$0, 16(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r15, 16(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC1(%rip), %rax
	movq	$0, 16(%rbp)
	movq	%rax, 0(%rbp)
	leaq	_dl_out_of_memory(%rip), %rax
	movq	%rax, 8(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$1, %r12d
	leaq	.LC1(%rip), %r13
	jmp	.L5
	.size	__GI__dl_exception_create, .-__GI__dl_exception_create
	.globl	_dl_exception_create
	.set	_dl_exception_create,__GI__dl_exception_create
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"Fatal error: invalid format in exception string\n"
	.text
	.p2align 4,,15
	.globl	__GI__dl_exception_create_format
	.hidden	__GI__dl_exception_create_format
	.type	__GI__dl_exception_create_format, @function
__GI__dl_exception_create_format:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$104, %rsp
	testq	%rsi, %rsi
	movq	%rsi, (%rsp)
	movq	%rcx, 72(%rsp)
	movq	%r8, 80(%rsp)
	movq	%r9, 88(%rsp)
	je	.L16
	movq	%rsi, %rdi
	call	strlen
	movzbl	(%r15), %edx
	leaq	1(%rax), %rcx
	leaq	2(%rax), %r14
	leaq	160(%rsp), %rax
	movl	$24, 24(%rsp)
	movq	%rcx, 8(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	testb	%dl, %dl
	movq	%rax, 40(%rsp)
	je	.L17
.L57:
	movq	40(%rsp), %r12
	movq	%r15, %rbx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %r14
	movq	%rsi, %rbx
	movl	%eax, %edx
.L26:
	testb	%dl, %dl
	je	.L17
.L27:
	cmpb	$37, %dl
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %rsi
	jne	.L18
	cmpb	$115, %al
	je	.L20
	jle	.L96
	cmpb	$120, %al
	je	.L23
	cmpb	$122, %al
	jne	.L19
.L22:
	cmpb	$120, 2(%rbx)
	je	.L97
.L23:
	movzbl	2(%rbx), %edx
	addq	$8, %r14
	addq	$2, %rbx
	testb	%dl, %dl
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%r14, %r14
	js	.L30
.L28:
	movq	%r14, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L30
	movq	%rax, 8(%rbp)
	movq	_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L31
	testb	$4, 796(%rax)
	je	.L31
	movq	%rbx, 16(%rbp)
.L56:
	leaq	160(%rsp), %rax
	leaq	(%rbx,%r14), %r13
	movl	$24, 24(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L33
	leaq	__GI__itoa_lower_digits(%rip), %r12
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L34:
	cmpq	%rbx, %r13
	je	.L45
	movb	%al, (%rbx)
	movq	%r15, %r14
	addq	$1, %rbx
.L44:
	movzbl	1(%r14), %eax
	leaq	1(%r14), %r15
	testb	%al, %al
	je	.L33
.L55:
	cmpb	$37, %al
	jne	.L34
	movzbl	1(%r15), %eax
	leaq	1(%r15), %r14
	cmpb	$115, %al
	je	.L36
	jg	.L37
	cmpb	$37, %al
	jne	.L98
	cmpq	%rbx, %r13
	je	.L45
	movb	$37, (%rbx)
	movzbl	1(%r14), %eax
	addq	$1, %rbx
	leaq	1(%r14), %r15
	testb	%al, %al
	jne	.L55
	.p2align 4,,10
	.p2align 3
.L33:
	cmpq	%rbx, %r13
	je	.L45
	leaq	1(%rbx), %rdi
	movq	8(%rsp), %rax
	movq	%r13, %rcx
	movb	$0, (%rbx)
	subq	%rdi, %rcx
	cmpq	%rax, %rcx
	jne	.L45
	movq	(%rsp), %rsi
	movq	%rax, %rdx
	call	memcpy@PLT
	movq	%rax, 0(%rbp)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	$0, 16(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L37:
	cmpb	$120, %al
	jne	.L99
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L46
	movl	%edx, %eax
	addl	$8, %edx
	addq	40(%rsp), %rax
	movl	%edx, 24(%rsp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L98:
	cmpb	$108, %al
	jne	.L35
.L39:
	cmpb	$120, 2(%r15)
	jne	.L35
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L50
	movl	%edx, %eax
	addq	40(%rsp), %rax
	addl	$8, %edx
	movl	%edx, 24(%rsp)
.L51:
	leaq	16(%rbx), %rdi
	movq	(%rax), %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L52:
	movq	%rdx, %rsi
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %esi
	testq	%rdx, %rdx
	movzbl	(%r12,%rsi), %esi
	movb	%sil, (%rax)
	jne	.L52
	cmpq	%rbx, %rax
	je	.L100
	.p2align 4,,10
	.p2align 3
.L54:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rbx, %rax
	jne	.L54
.L100:
	leaq	2(%r15), %r14
	movq	%rdi, %rbx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L99:
	cmpb	$122, %al
	je	.L39
.L35:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.p2align 4,,10
	.p2align 3
.L96:
	cmpb	$108, %al
	je	.L22
.L19:
	movzbl	2(%rbx), %edx
	addq	$1, %r14
	addq	$2, %rbx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC1(%rip), %rax
	movq	$0, 16(%rbp)
	movq	%rax, 0(%rbp)
	leaq	_dl_out_of_memory(%rip), %rax
	movq	%rax, 8(%rbp)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	160(%rsp), %rax
	movzbl	(%rdx), %edx
	movl	$24, 24(%rsp)
	movl	$2, %r14d
	movq	$1, 8(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	leaq	.LC1(%rip), %rax
	testb	%dl, %dl
	movq	%rax, (%rsp)
	jne	.L57
	jmp	.L28
.L97:
	movzbl	3(%rbx), %edx
	addq	$16, %r14
	addq	$3, %rbx
	jmp	.L26
.L45:
	call	length_mismatch
.L20:
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L24
	movl	%edx, %eax
	addl	$8, %edx
	addq	%r12, %rax
	movl	%edx, 24(%rsp)
.L25:
	movq	(%rax), %rdi
	addq	$2, %rbx
	call	strlen
	movzbl	(%rbx), %edx
	addq	%rax, %r14
	jmp	.L26
.L50:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
	jmp	.L51
.L36:
	movl	24(%rsp), %edx
	cmpl	$47, %edx
	ja	.L41
	movl	%edx, %eax
	addq	40(%rsp), %rax
	addl	$8, %edx
	movl	%edx, 24(%rsp)
.L42:
	movq	(%rax), %r15
	movq	%r15, %rdi
	call	strlen
	movq	%r13, %rdx
	subq	%rbx, %rdx
	cmpq	%rax, %rdx
	jb	.L45
	movq	%rbx, %rdi
	movq	%rax, %rdx
	movq	%r15, %rsi
	call	__mempcpy@PLT
	movq	%rax, %rbx
	jmp	.L44
.L46:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
.L47:
	leaq	8(%rbx), %rdi
	movl	(%rax), %edx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rdx, %rsi
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %esi
	testq	%rdx, %rdx
	movzbl	(%r12,%rsi), %esi
	movb	%sil, (%rax)
	jne	.L48
	cmpq	%rbx, %rax
	je	.L59
	.p2align 4,,10
	.p2align 3
.L49:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rbx, %rax
	jne	.L49
.L59:
	movq	%rdi, %rbx
	jmp	.L44
.L41:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
	jmp	.L42
.L24:
	movq	32(%rsp), %rax
	leaq	8(%rax), %rdx
	movq	%rdx, 32(%rsp)
	jmp	.L25
	.size	__GI__dl_exception_create_format, .-__GI__dl_exception_create_format
	.globl	_dl_exception_create_format
	.set	_dl_exception_create_format,__GI__dl_exception_create_format
	.p2align 4,,15
	.globl	__GI__dl_exception_free
	.hidden	__GI__dl_exception_free
	.type	__GI__dl_exception_free, @function
__GI__dl_exception_free:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	*__rtld_free(%rip)
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	popq	%rbx
	ret
	.size	__GI__dl_exception_free, .-__GI__dl_exception_free
	.globl	_dl_exception_free
	.set	_dl_exception_free,__GI__dl_exception_free
	.section	.rodata.str1.8
	.align 8
	.type	_dl_out_of_memory, @object
	.size	_dl_out_of_memory, 14
_dl_out_of_memory:
	.string	"out of memory"
	.hidden	__rtld_free
	.hidden	_rtld_local
	.hidden	__rtld_malloc
	.hidden	strlen
