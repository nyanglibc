	.text
	.p2align 4,,15
	.globl	_dl_addr
	.hidden	_dl_addr
	.type	_dl_addr, @function
_dl_addr:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rbp
	movq	%rcx, %rbx
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	movq	%r12, %rdi
	xorl	%r14d, %r14d
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	je	.L3
	movq	8(%rax), %rdx
	movq	856(%rax), %rcx
	movq	%rdx, 0(%r13)
	movq	%rcx, 8(%r13)
	cmpb	$0, (%rdx)
	je	.L84
.L4:
	movq	112(%rax), %rdx
	cmpq	$0, 672(%rax)
	movq	8(%rdx), %r8
	movq	104(%rax), %rdx
	movq	8(%rdx), %rsi
	movq	144(%rax), %rdx
	movq	%rsi, (%rsp)
	movl	8(%rdx), %esi
	movl	%esi, 12(%rsp)
	je	.L5
	movl	756(%rax), %ecx
	testl	%ecx, %ecx
	je	.L36
	movq	776(%rax), %rdx
	subl	$1, %ecx
	xorl	%r11d, %r11d
	leaq	4(%rdx), %r9
	leaq	(%r9,%rcx,4), %r15
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L7:
	cmpq	%r9, %r15
	movq	%r9, %rdx
	je	.L6
	addq	$4, %r9
.L17:
	movl	(%rdx), %edx
	testl	%edx, %edx
	je	.L7
	movq	784(%rax), %rsi
	leaq	(%rsi,%rdx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	sarq	$2, %rdx
	movl	%edx, %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r8,%rdx,8), %rdx
	movzwl	6(%rdx), %edi
	testw	%di, %di
	jne	.L8
	cmpq	$0, 8(%rdx)
	je	.L10
.L9:
	movzbl	4(%rdx), %r10d
	andl	$15, %r10d
	cmpb	$6, %r10b
	je	.L10
	movq	8(%rdx), %r14
	movq	(%rax), %r10
	addq	%r14, %r10
	cmpq	%r10, %r12
	jb	.L10
	testw	%di, %di
	je	.L11
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.L13
	cmpq	%r10, %r12
	je	.L14
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$4, %rcx
	testb	$1, -4(%rcx)
	je	.L16
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L6:
	testq	%rbp, %rbp
	je	.L28
	movq	%rax, 0(%rbp)
.L28:
	testq	%rbx, %rbx
	je	.L29
	movq	%r11, (%rbx)
.L29:
	testq	%r11, %r11
	je	.L30
	movl	(%r11), %edx
	addq	(%rsp), %rdx
	cmpw	$-15, 6(%r11)
	movq	%rdx, 16(%r13)
	je	.L41
	movq	(%rax), %rax
.L31:
	addq	8(%r11), %rax
	movl	$1, %r14d
	movq	%rax, 24(%r13)
.L3:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$24, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	96(%rax), %rdx
	movq	(%rsp), %rsi
	testq	%rdx, %rdx
	je	.L18
	movq	8(%rdx), %rdx
	movl	4(%rdx), %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r8,%rdx,8), %rsi
.L18:
	xorl	%r11d, %r11d
	cmpq	%rsi, %r8
	jb	.L27
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	%r10, %r12
	je	.L14
	movq	16(%rdx), %rdi
.L13:
	addq	%rdi, %r10
	cmpq	%r10, %r12
	jnb	.L10
.L14:
	testq	%r11, %r11
	je	.L15
	cmpq	8(%r11), %r14
	jbe	.L10
.L15:
	movl	12(%rsp), %edi
	cmpl	(%rdx), %edi
	cmova	%rdx, %r11
	addq	$4, %rcx
	testb	$1, -4(%rcx)
	je	.L16
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L86:
	movzbl	5(%r8), %edx
	andl	$3, %edx
	subl	$1, %edx
	cmpl	$1, %edx
	jbe	.L19
	andl	$15, %ecx
	cmpb	$6, %cl
	je	.L19
	movzwl	6(%r8), %edx
	testw	%dx, %dx
	je	.L85
	cmpw	$-15, %dx
	je	.L19
	movq	8(%r8), %rdx
	movq	(%rax), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, %r12
	jb	.L19
	movq	16(%r8), %rdi
	testq	%rdi, %rdi
	jne	.L24
	cmpq	%rcx, %r12
	jne	.L19
.L25:
	testq	%r11, %r11
	je	.L26
	cmpq	%rdx, 8(%r11)
	jnb	.L19
.L26:
	movl	12(%rsp), %edi
	cmpl	(%r8), %edi
	cmova	%r8, %r11
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$24, %r8
	cmpq	%rsi, %r8
	jnb	.L6
.L27:
	movzbl	4(%r8), %ecx
	movl	%ecx, %edx
	shrb	$4, %dl
	subl	$1, %edx
	cmpb	$1, %dl
	ja	.L19
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L8:
	cmpw	$-15, %di
	jne	.L9
	addq	$4, %rcx
	testb	$1, -4(%rcx)
	je	.L16
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movl	$1, %r14d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L85:
	movq	8(%r8), %rdx
	testq	%rdx, %rdx
	je	.L19
	movq	(%rax), %rcx
	addq	%rdx, %rcx
	cmpq	%rcx, %r12
	jb	.L19
	je	.L25
	movq	16(%r8), %rdi
.L24:
	addq	%rdi, %rcx
	cmpq	%rcx, %r12
	jb	.L25
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L84:
	testb	$3, 796(%rax)
	jne	.L4
	movq	_dl_argv(%rip), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 0(%r13)
	jmp	.L4
.L41:
	xorl	%eax, %eax
	jmp	.L31
	.size	_dl_addr, .-_dl_addr
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
