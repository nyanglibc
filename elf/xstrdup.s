	.text
	.p2align 4,,15
	.globl	xstrdup
	.type	xstrdup, @function
xstrdup:
	pushq	%rbx
	movq	%rdi, %rbx
	call	strlen@PLT
	leaq	1(%rax), %rdi
	call	xmalloc@PLT
	movq	%rbx, %rsi
	movq	%rax, %rdi
	popq	%rbx
	jmp	strcpy@PLT
	.size	xstrdup, .-xstrdup
