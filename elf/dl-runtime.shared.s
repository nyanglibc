	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-runtime.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ELFW(R_TYPE)(reloc->r_info) == ELF_MACHINE_JMP_SLOT"
	.text
	.p2align 4,,15
	.globl	_dl_fixup
	.hidden	_dl_fixup
	.type	_dl_fixup, @function
_dl_fixup:
.LFB85:
	pushq	%rbx
	movq	%rdi, %r10
	movl	%esi, %esi
	leaq	(%rsi,%rsi,2), %rdx
	subq	$16, %rsp
	movq	104(%rdi), %rax
	movq	8(%rax), %rdi
	movq	248(%r10), %rax
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,8), %r8
	movq	112(%r10), %rax
	movq	8(%r8), %rcx
	movq	(%r8), %rbx
	movq	8(%rax), %rax
	movq	%rcx, %rdx
	shrq	$32, %rdx
	leaq	(%rdx,%rdx,2), %rsi
	leaq	(%rax,%rsi,8), %rsi
	movq	(%r10), %rax
	movq	%rsi, 8(%rsp)
	addq	%rax, %rbx
	cmpl	$7, %ecx
	jne	.L33
	testb	$3, 5(%rsi)
	jne	.L3
	movq	464(%r10), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L4
	movq	8(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	andl	$32767, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	744(%r10), %rax
	leaq	(%rax,%rdx,8), %r8
	movl	$0, %eax
	movl	8(%r8), %r9d
	testl	%r9d, %r9d
	cmove	%rax, %r8
.L4:
#APP
# 99 "dl-runtime.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	movl	$1, %eax
	jne	.L34
.L5:
	movl	(%rsi), %esi
	movq	920(%r10), %rcx
	leaq	8(%rsp), %rdx
	pushq	$0
	pushq	%rax
	movl	$1, %r9d
	addq	%rsi, %rdi
	movq	%r10, %rsi
	call	_dl_lookup_symbol_x
	movq	%rax, %r8
#APP
# 113 "dl-runtime.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%rcx
	popq	%rsi
	jne	.L35
.L6:
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L7
	cmpw	$-15, 6(%rsi)
	je	.L17
	testq	%r8, %r8
	je	.L17
	movq	(%r8), %rax
.L31:
	movzbl	4(%rsi), %edx
	addq	8(%rsi), %rax
	andl	$15, %edx
	cmpb	$10, %dl
	je	.L36
.L7:
	movl	72+_rtld_local_ro(%rip), %edx
	testl	%edx, %edx
	jne	.L1
	movq	%rax, (%rbx)
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	call	*%rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L3:
	cmpw	$-15, 6(%rsi)
	movl	$0, %edx
	cmove	%rdx, %rax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
#APP
# 114 "dl-runtime.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	jne	.L6
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 114 "dl-runtime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
#APP
# 101 "dl-runtime.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	movl	$5, %eax
	jmp	.L5
.L33:
	leaq	__PRETTY_FUNCTION__.10795(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$77, %edx
	call	__GI___assert_fail
.LFE85:
	.size	_dl_fixup, .-_dl_fixup
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"DL_FIXUP_VALUE_CODE_ADDR (value) != 0"
	.text
	.p2align 4,,15
	.globl	_dl_profile_fixup
	.type	_dl_profile_fixup, @function
_dl_profile_fixup:
.LFB86:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$120, %rsp
	movq	832(%rdi), %rbx
	movq	%rdx, 8(%rsp)
	movq	%rcx, 16(%rsp)
	testq	%rbx, %rbx
	je	.L120
	movl	%esi, %esi
	movq	%rsi, %rax
	salq	$5, %rax
	addq	%rax, %rbx
	movl	28(%rbx), %eax
	testl	%eax, %eax
	je	.L121
	movq	(%rbx), %r13
.L63:
	movl	800+_rtld_local_ro(%rip), %r8d
	testl	%r8d, %r8d
	je	.L84
	movl	20(%rbx), %edx
	andl	$1, %edx
	jne	.L84
	testq	%r13, %r13
	je	.L122
	movq	8(%rbx), %rcx
	movl	16(%rbx), %eax
	movq	$-1, %r10
	movq	792+_rtld_local_ro(%rip), %r12
	leaq	2568+_rtld_local(%rip), %r11
	movq	%r15, 48(%rsp)
	movq	%r10, %r15
	movq	112(%rcx), %rsi
	leaq	(%rax,%rax,2), %rax
	movq	8(%rsi), %rdi
	leaq	(%rdi,%rax,8), %rax
	movdqu	(%rax), %xmm0
	movaps	%xmm0, 80(%rsp)
	movq	16(%rax), %rax
	movq	%r13, 88(%rsp)
	movq	%rax, 96(%rsp)
	movq	104(%rcx), %rax
	movl	80(%rsp), %ecx
	addq	8(%rax), %rcx
	movl	24(%rbx), %eax
	movl	%eax, 68(%rsp)
	leaq	68(%rsp), %rax
	movq	%rcx, 32(%rsp)
	movq	%rax, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L74:
	movq	40(%r12), %rax
	leal	1(%rdx), %r13d
	testq	%rax, %rax
	je	.L66
	leal	(%r13,%r13), %r14d
	movl	$1, %esi
	movl	%r14d, %ecx
	sall	%cl, %esi
	testl	%esi, 20(%rbx)
	jne	.L66
	movl	%edx, %esi
	movq	$-1, 72(%rsp)
	movq	%rsi, %rdx
	salq	$4, %rdx
	cmpq	%r11, %rbp
	leaq	1160(%rbp,%rdx), %rdx
	je	.L123
.L68:
	movq	8(%rbx), %rdi
	movq	%rsi, %rcx
	salq	$4, %rcx
	cmpq	%r11, %rdi
	leaq	1160(%rdi,%rcx), %rcx
	je	.L124
.L70:
	leaq	72(%rsp), %r8
	movq	%r11, 24(%rsp)
	leaq	80(%rsp), %rdi
	movl	16(%rbx), %esi
	pushq	%r8
	pushq	40(%rsp)
	movq	56(%rsp), %r9
	movq	32(%rsp), %r8
	call	*%rax
	popq	%rdx
	popq	%rcx
	cmpq	%rax, 88(%rsp)
	movl	68(%rsp), %edx
	movq	24(%rsp), %r11
	je	.L72
	orl	$16, %edx
	movq	%rax, 88(%rsp)
	movl	%edx, 68(%rsp)
.L72:
	movl	20(%rbx), %eax
	andl	$3, %edx
	movl	%r14d, %ecx
	sall	%cl, %edx
	orl	%edx, %eax
	movl	$2, %edx
	sall	%cl, %edx
	movl	%eax, 20(%rbx)
	testl	%eax, %edx
	jne	.L66
	movq	72(%rsp), %rax
	cmpq	$-1, %rax
	je	.L66
	cmpq	$-2, %r15
	je	.L66
	cmpq	$-1, %r15
	je	.L85
	cmpq	%rax, %r15
	je	.L66
	cmovl	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	%r13d, 800+_rtld_local_ro(%rip)
	movq	64(%r12), %r12
	movl	%r13d, %edx
	ja	.L74
	movq	%r15, %r10
	movq	88(%rsp), %r13
	movq	48(%rsp), %r15
.L64:
	movq	8(%rsp), %rdi
	movq	%r10, (%r15)
	movq	%r13, %rsi
	call	__GI__dl_mcount
.L37:
	addq	$120, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movq	248(%rbp), %rdx
	movq	104(%rdi), %rax
	movq	8(%rdx), %rcx
	movq	8(%rax), %rdi
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	112(%rbp), %rcx
	movq	8(%rax), %rdx
	movq	8(%rcx), %rcx
	movq	%rdx, %rax
	shrq	$32, %rax
	cmpl	$7, %edx
	leaq	(%rax,%rax,2), %rsi
	leaq	(%rcx,%rsi,8), %rsi
	movq	%rsi, 72(%rsp)
	jne	.L125
	testb	$3, 5(%rsi)
	jne	.L42
	movq	464(%rbp), %rdx
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L43
	movq	8(%rdx), %rdx
	movzwl	(%rdx,%rax,2), %eax
	andl	$32767, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	744(%rbp), %rax
	leaq	(%rax,%rdx,8), %r8
	movl	$0, %eax
	movl	8(%r8), %r14d
	testl	%r14d, %r14d
	cmove	%rax, %r8
.L43:
#APP
# 254 "dl-runtime.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	movl	$1, %eax
	jne	.L126
.L44:
	movl	(%rsi), %esi
	movq	920(%rbp), %rcx
	leaq	72(%rsp), %rdx
	pushq	$0
	pushq	%rax
	movl	$1, %r9d
	addq	%rsi, %rdi
	movq	%rbp, %rsi
	call	_dl_lookup_symbol_x
	movq	%rax, %r12
#APP
# 265 "dl-runtime.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%r11
	popq	%r13
	jne	.L127
.L45:
	movq	72(%rsp), %rsi
	xorl	%r13d, %r13d
	testq	%rsi, %rsi
	je	.L46
	cmpw	$-15, 6(%rsi)
	je	.L79
	testq	%r12, %r12
	je	.L79
	movq	(%r12), %r13
.L47:
	movzbl	4(%rsi), %eax
	addq	8(%rsi), %r13
	andl	$15, %eax
	cmpb	$10, %al
	je	.L119
.L48:
	movl	800+_rtld_local_ro(%rip), %r10d
	testl	%r10d, %r10d
	je	.L46
	movq	112(%r12), %rax
	movq	%rsi, %rdi
	movabsq	$-6148914691236517205, %rdx
	movq	%r12, 8(%rbx)
	subq	8(%rax), %rdi
	movq	%rdi, %rax
	sarq	$3, %rax
	imulq	%rdx, %rax
	movl	%eax, 16(%rbx)
	movzbl	797(%r12), %eax
	orb	797(%rbp), %al
	testb	$16, %al
	je	.L51
	movl	$0, 68(%rsp)
	movq	792+_rtld_local_ro(%rip), %r14
	leaq	2568+_rtld_local(%rip), %r10
	movdqu	(%rsi), %xmm0
	movl	$3, %r11d
	movaps	%xmm0, 80(%rsp)
	movq	16(%rsi), %rax
	movq	%r13, 88(%rsp)
	movl	$1, %r13d
	movq	%rax, 96(%rsp)
	movq	104(%r12), %rax
	movl	$3, 20(%rbx)
	movq	8(%rax), %rax
	movq	%rax, 40(%rsp)
	leaq	68(%rsp), %rax
	movq	%rax, 48(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 56(%rsp)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L132:
	testb	$1, 8(%rcx)
	je	.L56
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L128
	movq	72(%rsp), %rsi
	movq	40(%rsp), %r9
	movl	%r11d, 32(%rsp)
	movq	%r10, 24(%rsp)
	movq	48(%rsp), %r8
	movl	(%rsi), %edi
	movl	16(%rbx), %esi
	addq	%rdi, %r9
	movq	56(%rsp), %rdi
	call	*%rax
	cmpq	%rax, 88(%rsp)
	movl	68(%rsp), %edx
	movq	24(%rsp), %r10
	movl	32(%rsp), %r11d
	je	.L58
	orl	$16, %edx
	movq	%rax, 88(%rsp)
	movl	%edx, 68(%rsp)
.L58:
	movl	%edx, %eax
	movl	20(%rbx), %edx
	leal	(%r13,%r13), %ecx
	andl	$3, %eax
	andl	%eax, %edx
	sall	%cl, %eax
	orl	%edx, %eax
	movl	%eax, 20(%rbx)
.L60:
	cmpl	%r13d, 800+_rtld_local_ro(%rip)
	movq	64(%r14), %r14
	leal	1(%r13), %eax
	jbe	.L129
	movl	%eax, %r13d
.L61:
	leal	-1(%r13), %eax
	movq	%rax, %rdx
	salq	$4, %rdx
	cmpq	%r10, %rbp
	leaq	1160(%rbp,%rdx), %rdx
	je	.L130
.L53:
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	%r10, %r12
	leaq	1160(%r12,%rcx), %rcx
	je	.L131
.L55:
	testb	$2, 8(%rdx)
	jne	.L132
.L56:
	leal	(%r13,%r13), %ecx
	movl	%r11d, %eax
	sall	%cl, %eax
	orl	%eax, 20(%rbx)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L84:
	movq	$-1, %r10
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$65535, 20(%rbx)
	.p2align 4,,10
	.p2align 3
.L46:
	movl	72+_rtld_local_ro(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L63
	movq	%r13, (%rbx)
	movl	$1, 28(%rbx)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L120:
	movq	$-1, (%r8)
	call	_dl_fixup
	movq	%rax, %r13
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%r13d, %r13d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L124:
	leaq	233(%rsi), %rcx
	leaq	_rtld_local(%rip), %rdi
	salq	$4, %rcx
	leaq	(%rcx,%rdi), %rcx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	233(%rsi), %rdx
	leaq	_rtld_local(%rip), %rdi
	salq	$4, %rdx
	addq	%rdi, %rdx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$233, %rax
	leaq	_rtld_local(%rip), %rdi
	salq	$4, %rax
	leaq	(%rax,%rdi), %rcx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	233(%rax), %rdx
	leaq	_rtld_local(%rip), %rdi
	salq	$4, %rdx
	addq	%rdi, %rdx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L129:
	movl	68(%rsp), %eax
	movq	88(%rsp), %r13
	movl	%eax, 24(%rbx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L42:
	cmpw	$-15, 6(%rsi)
	je	.L80
	movq	0(%rbp), %r13
.L50:
	movzbl	4(%rsi), %eax
	addq	8(%rsi), %r13
	movq	%rbp, %r12
	andl	$15, %eax
	cmpb	$10, %al
	jne	.L48
	.p2align 4,,10
	.p2align 3
.L119:
	call	*%r13
	movq	72(%rsp), %rsi
	movq	%rax, %r13
	testq	%rsi, %rsi
	jne	.L48
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rax, %r15
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L126:
#APP
# 256 "dl-runtime.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	movl	$5, %eax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L127:
	xorl	%eax, %eax
#APP
# 266 "dl-runtime.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	jne	.L45
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 266 "dl-runtime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L45
.L128:
	movl	68(%rsp), %edx
	jmp	.L58
.L80:
	xorl	%r13d, %r13d
	jmp	.L50
.L122:
	leaq	__PRETTY_FUNCTION__.10857(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$405, %edx
	call	__GI___assert_fail
.L125:
	leaq	__PRETTY_FUNCTION__.10857(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$232, %edx
	call	__GI___assert_fail
.LFE86:
	.size	_dl_profile_fixup, .-_dl_profile_fixup
	.p2align 4,,15
	.globl	_dl_call_pltexit
	.type	_dl_call_pltexit, @function
_dl_call_pltexit:
.LFB87:
	pushq	%r15
	pushq	%r14
	movl	%esi, %esi
	pushq	%r13
	pushq	%r12
	salq	$5, %rsi
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	addq	832(%rdi), %rsi
	movl	800+_rtld_local_ro(%rip), %r10d
	movq	792+_rtld_local_ro(%rip), %rbp
	movq	%rsi, %r14
	movq	8(%rsi), %rsi
	movl	16(%r14), %eax
	movq	112(%rsi), %r8
	leaq	(%rax,%rax,2), %rax
	movq	8(%r8), %r9
	leaq	(%r9,%rax,8), %rax
	movdqu	(%rax), %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	16(%rax), %rax
	movl	32(%rsp), %r12d
	movq	%rax, 48(%rsp)
	movq	(%r14), %rax
	movq	%rax, 40(%rsp)
	movq	104(%rsi), %rax
	addq	8(%rax), %r12
	testl	%r10d, %r10d
	je	.L133
	leaq	32(%rsp), %rax
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rdi, %r15
	xorl	%ebx, %ebx
	movl	$2, %r13d
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L140:
	movq	48(%rbp), %rax
	testq	%rax, %rax
	je	.L135
	leal	(%rbx,%rbx), %ecx
	movl	%r13d, %edx
	sarl	%cl, %edx
	testl	%edx, 20(%r14)
	jne	.L135
	movl	%ebx, %esi
	leaq	2568+_rtld_local(%rip), %rdi
	movq	%rsi, %rdx
	salq	$4, %rdx
	cmpq	%rdi, %r15
	leaq	1160(%r15,%rdx), %rdx
	je	.L146
.L137:
	movq	8(%r14), %rdi
	leaq	2568+_rtld_local(%rip), %r10
	movq	%rsi, %rcx
	salq	$4, %rcx
	cmpq	%r10, %rdi
	leaq	1160(%rdi,%rcx), %rcx
	je	.L147
.L139:
	subq	$8, %rsp
	movl	16(%r14), %esi
	pushq	%r12
	movq	40(%rsp), %r9
	movq	32(%rsp), %r8
	movq	24(%rsp), %rdi
	call	*%rax
	popq	%rax
	popq	%rdx
.L135:
	addl	$1, %ebx
	cmpl	%ebx, 800+_rtld_local_ro(%rip)
	movq	64(%rbp), %rbp
	ja	.L140
.L133:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$233, %rsi
	leaq	-2568(%r10), %rdi
	salq	$4, %rsi
	leaq	(%rsi,%rdi), %rcx
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	233(%rsi), %rdx
	subq	$2568, %rdi
	salq	$4, %rdx
	addq	%rdi, %rdx
	jmp	.L137
.LFE87:
	.size	_dl_call_pltexit, .-_dl_call_pltexit
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10857, @object
	.size	__PRETTY_FUNCTION__.10857, 18
__PRETTY_FUNCTION__.10857:
	.string	"_dl_profile_fixup"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10795, @object
	.size	__PRETTY_FUNCTION__.10795, 10
__PRETTY_FUNCTION__.10795:
	.string	"_dl_fixup"
	.hidden	_rtld_local
	.hidden	_rtld_local_ro
	.hidden	_dl_lookup_symbol_x
