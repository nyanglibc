	.text
	.p2align 4,,15
	.type	update_hwcaps_counts, @function
update_hwcaps_counts:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsi, (%rsp)
	movq	$0, 8(%rsp)
	movq	%rsp, %rbp
	movl	%edx, 24(%rsp)
	movq	%rcx, 16(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%rsp), %rax
	addq	$1, (%rbx)
	addq	%rax, 8(%rbx)
	cmpq	16(%rbx), %rax
	ja	.L8
.L3:
	movq	%rbp, %rdi
	call	_dl_hwcaps_split_masked
	testb	%al, %al
	jne	.L4
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rax, 16(%rbx)
	jmp	.L3
	.size	update_hwcaps_counts, .-update_hwcaps_counts
	.p2align 4,,15
	.type	copy_hwcaps, @function
copy_hwcaps:
	pushq	%r12
	pushq	%rbp
	movabsq	$8604176992967421031, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	%rsi, (%rsp)
	movq	$0, 8(%rsp)
	movq	%rsp, %rbp
	movl	%edx, 24(%rsp)
	movq	%rcx, 16(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movq	8(%rbx), %rdi
	movq	(%rbx), %rax
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%rdi, (%rax)
	movq	%r12, (%rdi)
	addq	$13, %rdi
	movl	$1936744803, -5(%rdi)
	movb	$47, -1(%rdi)
	call	__mempcpy@PLT
	movb	$47, (%rax)
	movq	8(%rsp), %rcx
	addq	$1, %rax
	movq	(%rbx), %rdx
	addq	$14, %rcx
	movq	%rcx, 8(%rdx)
	addq	$16, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, (%rbx)
.L10:
	movq	%rbp, %rdi
	call	_dl_hwcaps_split_masked
	testb	%al, %al
	jne	.L11
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	copy_hwcaps, .-copy_hwcaps
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot create HWCAP priorities"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"dl-hwcaps.c"
.LC2:
	.string	"i == total_count"
.LC3:
	.string	"tls"
.LC4:
	.string	"m == cnt"
.LC5:
	.string	"cannot create capability list"
	.text
	.p2align 4,,15
	.globl	_dl_important_hwcaps
	.hidden	_dl_important_hwcaps
	.type	_dl_important_hwcaps, @function
_dl_important_hwcaps:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-80(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rsi, %r14
	leaq	-112(%rbp), %rbx
	subq	$200, %rsp
	movq	%rdi, -176(%rbp)
	movq	%rsi, -184(%rbp)
	movl	$15, %edi
	movq	%rax, %rsi
	movq	%rdx, -192(%rbp)
	xorl	%edx, %edx
	movq	%rcx, -224(%rbp)
	movq	%rax, -144(%rbp)
	call	__GI___tunable_get_val
	movq	-80(%rbp), %r12
	xorl	%eax, %eax
	andq	88+_rtld_local_ro(%rip), %r12
	cmpq	$0, 8+_rtld_local_ro(%rip)
	movq	%r12, -200(%rbp)
	setne	%al
	movq	%rax, -152(%rbp)
	call	_dl_hwcaps_subdirs_active
	pxor	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	$-1, %edx
	movq	%r13, %rsi
	movl	%eax, %r15d
	movl	%eax, -168(%rbp)
	movq	$0, -96(%rbp)
	movaps	%xmm0, -112(%rbp)
	call	update_hwcaps_counts
	leaq	_dl_hwcaps_subdirs(%rip), %rsi
	movq	%rbx, %rdi
	movq	%r14, %rcx
	movl	%r15d, %edx
	call	update_hwcaps_counts
	movq	-112(%rbp), %rbx
	movq	%rbx, %rdi
	salq	$4, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, _dl_hwcaps_priorities(%rip)
	je	.L97
	movq	-176(%rbp), %rax
	movl	%ebx, _dl_hwcaps_priorities_length(%rip)
	xorl	%r13d, %r13d
	movq	$0, -72(%rbp)
	movq	-144(%rbp), %r12
	movq	%rax, -80(%rbp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r13, %rax
	movq	-80(%rbp), %rdx
	salq	$4, %rax
	addq	_dl_hwcaps_priorities(%rip), %rax
	movq	%rdx, (%rax)
	movq	-72(%rbp), %rdx
	movl	%edx, 8(%rax)
	leal	1(%r13), %edx
	addq	$1, %r13
	movl	%edx, 12(%rax)
.L15:
	movq	%r12, %rdi
	call	_dl_hwcaps_split
	testb	%al, %al
	jne	.L16
	leaq	_dl_hwcaps_subdirs(%rip), %rax
	movq	%r13, %r14
	movq	$0, -72(%rbp)
	salq	$4, %r14
	movq	-144(%rbp), %r12
	movq	%rax, -80(%rbp)
	movl	-168(%rbp), %eax
	movl	%eax, -56(%rbp)
	movq	-184(%rbp), %rax
	movq	%rax, -64(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_dl_hwcaps_priorities(%rip), %rax
	movq	-80(%rbp), %rcx
	movq	%rdx, %r13
	addq	%r14, %rax
	addq	$16, %r14
	movq	%rcx, (%rax)
	movq	-72(%rbp), %rcx
	movl	%edx, 12(%rax)
	movl	%ecx, 8(%rax)
.L17:
	movq	%r12, %rdi
	call	_dl_hwcaps_split_masked
	testb	%al, %al
	leaq	1(%r13), %rdx
	jne	.L18
	cmpq	%r13, %rbx
	jne	.L19
	movl	_dl_hwcaps_priorities_length(%rip), %eax
	cmpq	$1, %rax
	movq	%rax, -136(%rbp)
	jbe	.L21
	movq	_dl_hwcaps_priorities(%rip), %rax
	movq	$1, -120(%rbp)
	addq	$16, %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-128(%rbp), %rbx
	movq	-120(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L24:
	movl	8(%rbx), %r14d
	movl	-8(%rbx), %r13d
	movq	-16(%rbx), %r15
	movq	(%rbx), %rdi
	movl	%r13d, %edx
	cmpl	%r13d, %r14d
	movq	%r15, %rsi
	cmovbe	%r14, %rdx
	call	memcmp
	cmpl	$0, %eax
	jg	.L22
	jne	.L67
	cmpl	%r13d, %r14d
	jnb	.L22
.L67:
	movl	-4(%rbx), %eax
	subq	$16, %rbx
	movdqu	16(%rbx), %xmm0
	movq	%r15, 16(%rbx)
	movl	%r13d, 24(%rbx)
	movups	%xmm0, (%rbx)
	movl	%eax, 28(%rbx)
	subq	$1, %r12
	jne	.L24
.L22:
	addq	$1, -120(%rbp)
	addq	$16, -128(%rbp)
	movq	-120(%rbp), %rax
	cmpq	-136(%rbp), %rax
	jne	.L25
.L21:
	movq	-112(%rbp), %rcx
	movq	-104(%rbp), %rdx
	movq	-200(%rbp), %rdi
	addq	$14, -96(%rbp)
	leaq	0(,%rcx,8), %rax
	movq	%rcx, -208(%rbp)
	subq	%rcx, %rax
	testq	%rdi, %rdi
	leaq	(%rdx,%rax,2), %rax
	movq	%rax, -216(%rbp)
	je	.L26
	movq	-152(%rbp), %rsi
	xorl	%ecx, %ecx
	movl	$1, %edx
	.p2align 4,,10
	.p2align 3
.L28:
	btq	%rcx, %rdi
	leaq	1(%rsi), %rbx
	jnc	.L27
	leaq	2(%rsi), %rax
	movq	%rbx, %rsi
	movq	%rax, %rbx
.L27:
	addq	$1, %rcx
	movq	%rdx, %rax
	salq	%cl, %rax
	negq	%rax
	testq	%rdi, %rax
	jne	.L28
	movq	%rbx, %rax
	movl	$1, %r15d
	movq	%rbx, -232(%rbp)
	salq	$4, %rax
	movq	%r15, %rbx
	movq	-200(%rbp), %r15
	addq	$16, %rax
	leaq	_rtld_local_ro(%rip), %r12
	movq	%rsi, -152(%rbp)
	subq	%rax, %rsp
	xorl	%edx, %edx
	xorl	%r13d, %r13d
	movq	%rsp, %rax
	movq	%rax, -160(%rbp)
	movq	%rax, -128(%rbp)
.L60:
	btq	%r13, %r15
	movq	%rbx, -120(%rbp)
	jnc	.L30
.L99:
	movslq	%r13d, %rax
	movq	-160(%rbp), %rsi
	movq	%rdx, -136(%rbp)
	leaq	536(%rax,%rax,8), %rdi
	movq	%rdx, %rax
	salq	$4, %rax
	leaq	(%rsi,%rax), %r14
	addq	%r12, %rdi
	movq	%rdi, (%r14)
	call	strlen
	movl	%r13d, %ecx
	movq	%rax, 8(%r14)
	movq	-136(%rbp), %rdx
	movl	$1, %eax
	addq	$1, %r13
	salq	%cl, %rax
	movq	%rax, %rcx
	addq	$2, %rdx
	xorq	%r15, %rcx
	cmpq	%r15, %rax
	je	.L98
	movq	%rcx, %r15
	movq	%rbx, %rdx
	addq	$1, %rbx
	btq	%r13, %r15
	movq	%rbx, -120(%rbp)
	jc	.L99
.L30:
	addq	$1, %r13
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%rbx, %r14
	movq	-232(%rbp), %rbx
.L61:
	movq	8+_rtld_local_ro(%rip), %rcx
	testq	%rcx, %rcx
	je	.L63
	movq	-128(%rbp), %rax
	movq	%r14, %r9
	salq	$4, %r9
	addq	%r9, %rax
	movq	%rcx, (%rax)
	movq	16+_rtld_local_ro(%rip), %rcx
	movq	%rcx, 8(%rax)
	leaq	1(%rdx), %rax
.L32:
	salq	$4, %rdx
	addq	-128(%rbp), %rdx
	leaq	.LC3(%rip), %rsi
	cmpq	%rax, %rbx
	movq	%rsi, (%rdx)
	movq	$3, 8(%rdx)
	jne	.L100
	movq	-128(%rbp), %rax
	cmpq	$1, %rbx
	movq	8(%rax), %rax
	movq	%rax, -120(%rbp)
	movq	-216(%rbp), %rax
	movq	-120(%rbp), %rdi
	je	.L101
	leaq	2(%rax,%rdi), %rdi
	movq	-152(%rbp), %rax
	movq	-128(%rbp), %rsi
	movl	%ebx, %r12d
	salq	$4, %rax
	addq	8(%rsi,%rax), %rdi
	cmpq	$2, %rbx
	ja	.L102
.L35:
	movl	$1, %r13d
	movl	%r12d, %ecx
	movq	-208(%rbp), %r15
	movl	%r13d, %eax
	sall	%cl, %eax
	movl	%eax, -216(%rbp)
	movslq	%eax, %r14
	movq	-192(%rbp), %rax
	addq	%r14, %r15
	movq	%r15, (%rax)
	salq	$4, %r15
	addq	%r15, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rsi
	movq	%rax, -200(%rbp)
	je	.L39
	movq	%rax, -80(%rbp)
	movq	-192(%rbp), %rax
	xorl	%ecx, %ecx
	movq	-144(%rbp), %r15
	movl	$-1, %edx
	movq	(%rax), %rax
	movq	%r15, %rdi
	salq	$4, %rax
	addq	%rsi, %rax
	movq	-176(%rbp), %rsi
	movq	%rax, -72(%rbp)
	call	copy_hwcaps
	movq	-184(%rbp), %rcx
	movl	-168(%rbp), %edx
	leaq	_dl_hwcaps_subdirs(%rip), %rsi
	movq	%r15, %rdi
	call	copy_hwcaps
	movq	-80(%rbp), %rax
	cmpq	$1, %rbx
	movq	-72(%rbp), %rdi
	movq	%rax, -160(%rbp)
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, -144(%rbp)
	je	.L103
	movq	-160(%rbp), %rax
	movq	-128(%rbp), %rsi
	movq	%rdi, (%rax)
	movq	%rdi, 16(%rax)
	leal	-1(%r12), %eax
	movl	%eax, %ecx
	movl	%eax, -184(%rbp)
	movl	$1, %eax
	sall	%cl, %r13d
	movl	%r12d, %ecx
	salq	%cl, %rax
	movl	%r13d, -176(%rbp)
	movq	%rax, -192(%rbp)
	movq	-152(%rbp), %rax
	salq	$4, %rax
	cmpq	$2, %rbx
	leaq	(%rsi,%rax), %rdx
	movq	8(%rdx), %rsi
	movq	%rsi, -136(%rbp)
	je	.L104
	movq	-128(%rbp), %rcx
	movq	-152(%rbp), %r15
	movl	$1, %r13d
	movq	(%rdx), %rsi
	movslq	-176(%rbp), %r12
	movq	%r14, -208(%rbp)
	movq	%rbx, -232(%rbp)
	leaq	-16(%rcx,%rax), %rax
	subq	$1, %r15
	movq	%rsi, -168(%rbp)
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L47:
	movq	-136(%rbp), %rdx
	movq	-168(%rbp), %rsi
	subq	$2, %r12
	movq	%r15, %rbx
	call	__mempcpy@PLT
	movq	-152(%rbp), %r14
	movb	$47, (%rax)
	leaq	1(%rax), %rdi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L45:
	subq	$16, %r14
	subq	$1, %rbx
	je	.L105
.L46:
	movl	%r13d, %edx
	movl	%ebx, %ecx
	sall	%cl, %edx
	movslq	%edx, %rdx
	testq	%r12, %rdx
	je	.L45
	movq	8(%r14), %rdx
	movq	(%r14), %rsi
	subq	$16, %r14
	call	__mempcpy@PLT
	subq	$1, %rbx
	movb	$47, (%rax)
	leaq	1(%rax), %rdi
	jne	.L46
.L105:
	movq	-120(%rbp), %rdx
	movq	-144(%rbp), %rsi
	call	__mempcpy@PLT
	testq	%r12, %r12
	movb	$47, (%rax)
	leaq	1(%rax), %rdi
	jne	.L47
	movq	-208(%rbp), %r14
	movq	-232(%rbp), %rbx
.L44:
	movq	-160(%rbp), %rax
	movq	-192(%rbp), %rcx
	leaq	8(%rax), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$1, %rax
	movq	$0, (%rdx)
	addq	$16, %rdx
	cmpq	%rcx, %rax
	jb	.L48
	movq	-128(%rbp), %rdi
	movq	%rbx, %rax
	movq	-160(%rbp), %r10
	salq	$4, %rax
	movl	$1, %r8d
	movq	%rbx, %rcx
	leaq	-8(%rdi,%rax), %rdi
	.p2align 4,,10
	.p2align 3
.L53:
	subq	$1, %rcx
	movl	%r8d, %r9d
	movq	%r10, %rdx
	sall	%cl, %r9d
	testq	%r14, %r14
	movq	%r14, %rax
	movslq	%r9d, %r9
	je	.L50
.L49:
	subq	$1, %rax
	testq	%rax, %r9
	je	.L51
.L106:
	movq	(%rdi), %rsi
	subq	$1, %rax
	addq	$1, %rsi
	addq	%rsi, 8(%rdx)
	addq	$16, %rdx
	testq	%rax, %r9
	jne	.L106
.L51:
	addq	$16, %rdx
	testq	%rax, %rax
	jne	.L49
.L50:
	subq	$16, %rdi
	testq	%rcx, %rcx
	jne	.L53
	movl	-216(%rbp), %ecx
	movq	-160(%rbp), %rax
	movl	$1, %edi
	subl	$2, %ecx
	leaq	32(%rax), %rdx
	movslq	%ecx, %r8
	movzbl	-184(%rbp), %ecx
	salq	%cl, %rdi
	cmpq	%r8, %rdi
	je	.L66
	movq	%rdx, %rcx
	movq	%r8, %rax
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L108:
	movq	-24(%rcx), %rsi
	addq	-32(%rcx), %rsi
	addq	$16, %rcx
	movq	%rsi, -16(%rcx)
	cmpq	%rax, %rdi
	je	.L107
.L57:
	subq	$1, %rax
	testb	$1, %al
	jne	.L108
	movq	-16(%rcx), %rsi
	addq	$16, %rcx
	movq	%rsi, -16(%rcx)
	cmpq	%rax, %rdi
	jne	.L57
.L107:
	movq	%r8, %rcx
	subq	%rax, %rcx
	salq	$4, %rcx
	addq	%rcx, %rdx
.L54:
	movl	-176(%rbp), %r13d
	movq	-136(%rbp), %rcx
	negl	%r13d
	addq	$1, %rcx
	movslq	%r13d, %r13
	salq	$4, %r13
	.p2align 4,,10
	.p2align 3
.L58:
	movq	(%rdx,%r13), %rsi
	addq	$16, %rdx
	addq	%rcx, %rsi
	movq	%rsi, -16(%rdx)
	subq	$1, %rax
	jne	.L58
	movq	-160(%rbp), %rax
	movq	8(%rax), %rdx
.L96:
	movq	-96(%rbp), %rax
	movq	-224(%rbp), %rdi
	cmpq	%rax, %rdx
	cmova	%rdx, %rax
	movq	%rax, (%rdi)
	movq	-200(%rbp), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L102:
	movq	-128(%rbp), %rax
	leaq	-2(%rbx), %rsi
	addq	%rdi, %rdi
	leaq	24(%rax), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rdx), %rcx
	addq	$1, %rax
	addq	$16, %rdx
	cmpq	%rax, %rsi
	leaq	1(%rdi,%rcx), %rdi
	jne	.L37
	cmpq	$3, %rbx
	je	.L64
	cmpq	$63, %rbx
	ja	.L39
	leaq	128(%rdi), %rax
	movl	$67, %ecx
	movl	%ebx, %r12d
	subl	%ebx, %ecx
	shrq	%cl, %rax
	testq	%rax, %rax
	je	.L38
.L39:
	leaq	.LC5(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$12, %edi
	call	_dl_signal_error@PLT
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	1(%rax,%rdi), %rdi
	movl	$1, %r12d
	jmp	.L35
.L63:
	movq	%rdx, %rax
	movq	%r14, %rdx
	jmp	.L32
.L103:
	movq	-120(%rbp), %rdx
	movq	-160(%rbp), %rbx
	movq	-144(%rbp), %rsi
	leaq	1(%rdx), %rax
	movq	%rdi, (%rbx)
	movq	%rdi, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	%rax, 8(%rbx)
	call	__mempcpy@PLT
	movb	$47, (%rax)
	movq	8(%rbx), %rdx
	jmp	.L96
.L64:
	movl	$3, %r12d
.L38:
	leal	-3(%r12), %ecx
	salq	%cl, %rdi
	jmp	.L35
.L104:
	movq	-128(%rbp), %rax
	movq	24(%rax), %rdx
	movq	16(%rax), %rsi
	call	__mempcpy@PLT
	movq	-120(%rbp), %rdx
	movq	-144(%rbp), %rsi
	leaq	1(%rax), %rdi
	movb	$47, (%rax)
	call	__mempcpy@PLT
	movb	$47, (%rax)
	jmp	.L44
.L26:
	movq	-152(%rbp), %rax
	movl	$1, %edx
	xorl	%r14d, %r14d
	leaq	1(%rax), %rbx
	movq	%rbx, %rax
	salq	$4, %rax
	addq	$16, %rax
	subq	%rax, %rsp
	movq	%rsp, -128(%rbp)
	jmp	.L61
.L66:
	movq	%rdi, %rax
	jmp	.L54
.L97:
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$12, %edi
	call	_dl_signal_error@PLT
.L19:
	leaq	__PRETTY_FUNCTION__.10090(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$133, %edx
	call	__GI___assert_fail
.L100:
	leaq	__PRETTY_FUNCTION__.10134(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$229, %edx
	call	__GI___assert_fail
	.size	_dl_important_hwcaps, .-_dl_important_hwcaps
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10090, @object
	.size	__PRETTY_FUNCTION__.10090, 19
__PRETTY_FUNCTION__.10090:
	.string	"compute_priorities"
	.align 16
	.type	__PRETTY_FUNCTION__.10134, @object
	.size	__PRETTY_FUNCTION__.10134, 21
__PRETTY_FUNCTION__.10134:
	.string	"_dl_important_hwcaps"
	.hidden	_dl_hwcaps_priorities_length
	.comm	_dl_hwcaps_priorities_length,4,4
	.hidden	_dl_hwcaps_priorities
	.comm	_dl_hwcaps_priorities,8,8
	.hidden	strlen
	.hidden	memcmp
	.hidden	_dl_hwcaps_split
	.hidden	__rtld_malloc
	.hidden	_dl_hwcaps_subdirs
	.hidden	_dl_hwcaps_subdirs_active
	.hidden	_rtld_local_ro
	.hidden	_dl_hwcaps_split_masked
