	.text
	.p2align 4,,15
	.type	check_match, @function
check_match:
	pushq	%r14
	pushq	%r13
	movl	%r8d, %r14d
	pushq	%r12
	pushq	%rbp
	leaq	(%r14,%r14,2), %r8
	pushq	%rbx
	movq	112(%rdi), %rax
	movq	8(%rax), %rax
	leaq	(%rax,%r8,8), %rbx
	movzbl	4(%rbx), %eax
	andl	$15, %eax
	cmpq	$0, 8(%rbx)
	je	.L15
.L2:
	movl	$1127, %r8d
	btl	%eax, %r8d
	jnc	.L8
	movq	%rdx, %r12
	movq	104(%rdi), %rdx
	movl	(%rbx), %eax
	movq	%rdi, %r13
	movl	%ecx, %ebp
	addq	8(%rdx), %rax
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L8
	movq	840(%r13), %rax
	movzwl	(%rax,%r14,2), %eax
	andl	$32767, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	744(%r13), %rax
	leaq	(%rax,%rdx,8), %rax
	cmpl	%ebp, 8(%rax)
	jne	.L8
	movq	(%rax), %rdi
	movq	%r12, %rsi
	call	strcmp
	testl	%eax, %eax
	movl	$0, %eax
	cmovne	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpw	$-15, 6(%rbx)
	je	.L2
	cmpb	$6, %al
	je	.L2
.L8:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	check_match, .-check_match
	.p2align 4,,15
	.globl	_dl_lookup_direct
	.hidden	_dl_lookup_direct
	.type	_dl_lookup_direct, @function
_dl_lookup_direct:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$24, %rsp
	cmpq	$0, 768(%rdi)
	movq	%rcx, (%rsp)
	movl	%r8d, 12(%rsp)
	movl	756(%rdi), %ecx
	je	.L17
	movl	%edx, %eax
	movl	%edx, %r14d
	xorl	%edx, %edx
	divl	%ecx
	movq	776(%rdi), %rax
	movl	%edx, %edx
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	je	.L30
	movq	784(%rdi), %r15
	leaq	(%r15,%rax,4), %rbx
	.p2align 4,,10
	.p2align 3
.L20:
	movl	(%rbx), %ebp
	movl	%ebp, %eax
	xorl	%r14d, %eax
	shrl	%eax
	jne	.L19
	movq	%rbx, %r8
	movl	12(%rsp), %ecx
	movq	(%rsp), %rdx
	subq	%r15, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	sarq	$2, %r8
	call	check_match
	testq	%rax, %rax
	jne	.L16
.L19:
	addq	$4, %rbx
	andl	$1, %ebp
	je	.L20
.L30:
	xorl	%eax, %eax
.L16:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	(%rsi), %eax
	testq	%rax, %rax
	je	.L21
	movzbl	1(%rsi), %esi
	testb	%sil, %sil
	je	.L43
	salq	$4, %rax
	movzbl	%sil, %edx
	addq	%rdx, %rax
	movzbl	2(%r12), %edx
	testb	%dl, %dl
	je	.L42
	salq	$4, %rax
	addq	%rdx, %rax
	movzbl	3(%r12), %edx
	testb	%dl, %dl
	je	.L42
	salq	$4, %rax
	addq	%rdx, %rax
	movzbl	4(%r12), %edx
	testb	%dl, %dl
	je	.L42
	salq	$4, %rax
	movq	%rax, %rsi
	movzbl	%dl, %eax
	movzbl	5(%r12), %edx
	addq	%rsi, %rax
	leaq	5(%r12), %rsi
	testb	%dl, %dl
	je	.L26
.L27:
	salq	$4, %rax
	addq	$1, %rsi
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$240, %edx
	xorq	%rdx, %rax
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	jne	.L27
.L26:
	andl	$268435455, %eax
.L42:
	xorl	%edx, %edx
	divl	%ecx
	movl	%edx, %eax
	salq	$2, %rax
.L21:
	movq	784(%r13), %rdx
	movl	(%rdx,%rax), %ebx
	testl	%ebx, %ebx
	jne	.L28
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L44:
	movq	776(%r13), %rdx
	movl	(%rdx,%rbx,4), %ebx
	testl	%ebx, %ebx
	je	.L16
.L28:
	movl	12(%rsp), %ecx
	movq	(%rsp), %rdx
	movl	%ebx, %r8d
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	check_match
	testq	%rax, %rax
	je	.L44
	jmp	.L16
.L43:
	movzbl	%al, %eax
	xorl	%edx, %edx
	divl	%ecx
	movl	%edx, %eax
	salq	$2, %rax
	jmp	.L21
	.size	_dl_lookup_direct, .-_dl_lookup_direct
	.hidden	strcmp
