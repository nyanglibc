	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	".so."
	.text
	.p2align 4,,15
	.globl	implicit_soname
	.type	implicit_soname, @function
implicit_soname:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	subq	$8, %rsp
	call	xstrdup@PLT
	testb	%bpl, %bpl
	movq	%rax, %rbx
	jne	.L1
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	strstr@PLT
	testq	%rax, %rax
	je	.L1
	leaq	4(%rax), %rdi
	movl	$46, %esi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L1
	movb	$0, (%rax)
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	implicit_soname, .-implicit_soname
	.section	.rodata.str1.1
.LC1:
	.string	"/lib/ld-linux-x86-64.so.2"
.LC2:
	.string	"%s is a 64 bit ELF file.\n"
.LC3:
	.string	"Unknown ELFCLASS in file %s.\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"%s is not a shared object file (Type: %d).\n"
	.section	.rodata.str1.1
.LC5:
	.string	"file %s is truncated\n"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"more than one dynamic segment\n"
	.text
	.p2align 4,,15
	.globl	process_elf32_file
	.type	process_elf32_file, @function
process_elf32_file:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movl	$0, (%rcx)
	movq	144(%rsp), %rbx
	movzbl	4(%rbx), %eax
	cmpb	$1, %al
	je	.L12
	movl	opt_verbose(%rip), %ecx
	testl	%ecx, %ecx
	je	.L109
	cmpb	$2, %al
	movl	$5, %edx
	leaq	.LC2(%rip), %rsi
	je	.L108
	leaq	.LC3(%rip), %rsi
.L108:
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext@PLT
	movq	%r13, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
.L109:
	movl	$1, %eax
.L11:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movzwl	16(%rbx), %ebp
	cmpw	$3, %bp
	jne	.L111
	movl	28(%rbx), %eax
	addq	%rbx, %rax
	movq	%rax, 16(%rsp)
	jc	.L110
	movq	%rcx, %r12
	movq	152(%rsp), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rax
	movq	%rcx, 32(%rsp)
	ja	.L110
	cmpw	$0, 44(%rbx)
	movl	$1, (%rdx)
	movl	$0, (%r8)
	je	.L109
	movq	16(%rsp), %rbp
	xorl	%ecx, %ecx
	movq	$0, 24(%rsp)
	movl	$0, 8(%rsp)
	movq	%rdi, 48(%rsp)
	movl	%ecx, %r15d
	movq	%rdx, 56(%rsp)
	movq	%r12, 40(%rsp)
	movq	%rbp, %r13
	movq	%r8, 72(%rsp)
	movq	32(%rsp), %rbp
	movq	%r9, 64(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$4, %eax
	je	.L25
	cmpl	$1685382483, %eax
	jne	.L21
	cmpl	$4, 28(%r13)
	je	.L112
	.p2align 4,,10
	.p2align 3
.L21:
	movzwl	44(%rbx), %eax
	addl	$1, %r15d
	addq	$32, %r13
	cmpl	%eax, %r15d
	jge	.L113
	cmpq	%r13, %rbx
	ja	.L63
	cmpq	%r13, %rbp
	jb	.L63
.L19:
	movl	0(%r13), %eax
	cmpl	$3, %eax
	je	.L22
	ja	.L23
	cmpl	$2, %eax
	jne	.L21
	movl	8(%rsp), %edx
	testl	%edx, %edx
	jne	.L114
.L27:
	movl	4(%r13), %eax
	movl	%eax, 8(%rsp)
	movl	16(%r13), %eax
	movq	%rax, 24(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	.LC4(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movzwl	%bp, %r8d
	movq	%r13, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	xorl	%edi, %edi
	call	error@PLT
	addq	$88, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L65:
	movq	8(%rsp), %r13
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L25:
	movq	40(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L21
	movl	16(%r13), %edi
	cmpl	$31, %edi
	jbe	.L21
	movl	28(%r13), %r10d
	cmpl	$3, %r10d
	jbe	.L21
	leal	-4(%r10), %eax
	andl	$-5, %eax
	jne	.L21
	movl	4(%r13), %esi
	movl	%r10d, %r11d
	leal	11(%r10), %r12d
	negl	%r11d
	subl	$1, %r10d
	addq	%rbx, %rsi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	leal	(%r12,%rdx), %eax
	andl	%r11d, %eax
	movl	%eax, %edx
	leal	(%r10,%rcx), %eax
	addl	%edx, %eax
	leal	-32(%rdi), %edx
	andl	%r11d, %eax
	cmpl	%edx, %eax
	ja	.L21
	testl	%eax, %eax
	je	.L21
	subl	%eax, %edi
	addq	%rax, %rsi
.L31:
	movl	(%rsi), %edx
	movl	4(%rsi), %ecx
	cmpl	$4, %edx
	jne	.L32
	cmpl	$16, %ecx
	jne	.L32
	cmpl	$1, 8(%rsi)
	jne	.L32
	cmpl	$5590599, 12(%rsi)
	jne	.L32
	testl	%edi, %edi
	je	.L21
	movzbl	28(%rsi), %edx
	movl	16(%rsi), %eax
	sall	$24, %eax
	orl	%edx, %eax
	movl	20(%rsi), %edx
	sall	$16, %edx
	andl	$16711680, %edx
	orl	%edx, %eax
	movl	24(%rsi), %edx
	movq	40(%rsp), %rsi
	sall	$8, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movl	%eax, (%rsi)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	movl	4(%r13), %r14d
	addq	%rbx, %r14
	jc	.L63
	cmpq	%r14, %rbp
	jb	.L63
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	xorl	%r12d, %r12d
	movq	%rbx, 144(%rsp)
	movq	%r12, %rbx
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L115
.L30:
	addq	$1, %rbx
	cmpq	$4, %rbx
	je	.L99
	leaq	interpreters(%rip), %rdi
	movq	%rbx, %rax
	salq	$4, %rax
	movq	(%rdi,%rax), %rsi
	movq	%r14, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L30
.L115:
	movl	%ebx, %r12d
	leaq	interpreters(%rip), %rax
	movq	56(%rsp), %rsi
	salq	$4, %r12
	movq	144(%rsp), %rbx
	movl	8(%rax,%r12), %eax
	movl	%eax, (%rsi)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L99:
	movq	144(%rsp), %rbx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L63:
	movq	48(%rsp), %r13
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	leaq	.LC6(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L112:
	movl	4(%r13), %edx
	movl	16(%r13), %esi
	addq	%rbx, %rdx
	cmpl	$12, %esi
	movl	%edx, %edi
	leaq	12(%rdx), %r10
	ja	.L37
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L33:
	movl	4(%rdx), %ecx
	addq	$15, %rax
	andq	$-4, %rax
	leaq	3(%rax,%rcx), %rax
	andq	$-4, %rax
	addq	%rax, %rdx
	leaq	12(%rdx), %r10
	movl	%r10d, %eax
	subl	%edi, %eax
	cmpl	%eax, %esi
	jbe	.L21
.L37:
	movl	(%rdx), %eax
	cmpl	$4, %eax
	jne	.L33
	cmpl	$5, 8(%rdx)
	jne	.L33
	cmpl	$5590599, (%r10)
	jne	.L33
	movl	4(%rdx), %esi
	cmpl	$7, %esi
	jbe	.L21
	testb	$3, %sil
	jne	.L21
	leaq	16(%rdx), %rax
	movl	20(%rdx), %r10d
	leaq	(%rax,%rsi), %r12
	movl	16(%rdx), %esi
	jmp	.L34
.L35:
	addq	$3, %rdx
	andq	$-4, %rdx
	leaq	(%rcx,%rdx), %rax
	movq	%r12, %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jle	.L21
	movl	(%rax), %edx
	movl	4(%rax), %r10d
	cmpl	%esi, %edx
	jb	.L21
	movl	%edx, %esi
.L34:
	leaq	8(%rax), %rcx
	movl	%r10d, %edx
	leaq	(%rcx,%rdx), %rdi
	cmpq	%rdi, %r12
	jb	.L21
	cmpl	$-1073709054, %esi
	ja	.L21
	cmpl	$-1073709054, %esi
	jne	.L35
	cmpl	$4, %r10d
	jne	.L21
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L21
	movq	72(%rsp), %rcx
#APP
# 50 "../sysdeps/unix/sysv/linux/x86/elf-read-prop.h" 1
	bsr %eax, %eax
# 0 "" 2
#NO_APP
	movl	%eax, (%rcx)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L113:
	cmpq	$0, 24(%rsp)
	movq	48(%rsp), %r13
	movq	56(%rsp), %r15
	movq	64(%rsp), %r9
	je	.L109
	movl	8(%rsp), %r12d
	movq	32(%rsp), %rdi
	addq	%rbx, %r12
	cmpq	%r12, %rdi
	jb	.L110
	cmpq	%r12, %rbx
	ja	.L110
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L109
	cmpl	$5, %esi
	movq	%r12, %rdx
	jne	.L41
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L46:
	cmpq	%rdx, %rbx
	ja	.L110
	cmpq	%rdx, %rdi
	jb	.L110
	cmpl	$5, %ecx
	je	.L40
.L41:
	addq	$8, %rdx
	movl	(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L46
	jmp	.L109
.L40:
	testl	%eax, %eax
	movl	4(%rdx), %edx
	je	.L62
	subl	$1, %eax
	movq	16(%rsp), %rdi
	addq	$1, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$32, %rdi
	cmpq	%rdi, %rax
	je	.L62
.L44:
	cmpl	$1, (%rdi)
	jne	.L43
	movl	8(%rdi), %ecx
	cmpl	%edx, %ecx
	ja	.L43
	movl	%edx, %r8d
	subl	%ecx, %r8d
	cmpl	16(%rdi), %r8d
	jnb	.L43
	subl	4(%rdi), %ecx
	cmpl	$-1, %ecx
	je	.L62
.L42:
	movl	%edx, %eax
	subq	%rcx, %rax
	leaq	(%rbx,%rax), %rbp
	cmpq	%rbp, 32(%rsp)
	jb	.L110
	cmpq	%rbp, %rbx
	ja	.L110
	testq	%rbp, %rbp
	je	.L109
	movq	%r13, 8(%rsp)
	movq	%rbx, %rcx
	movq	%r15, %r13
	movq	%r9, %rbx
	movq	%rbp, %r15
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$14, %esi
	je	.L64
.L47:
	addq	$8, %r12
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L57
.L56:
	cmpl	$1, %esi
	jne	.L116
.L64:
	movl	4(%r12), %r14d
	addq	%r15, %r14
	cmpq	%r14, %rcx
	ja	.L65
	cmpq	%r14, 32(%rsp)
	jb	.L65
	cmpl	$1, %esi
	je	.L117
	movq	%r14, %rdi
	movq	%rcx, 16(%rsp)
	call	xstrdup@PLT
	movq	16(%rsp), %rcx
	movq	%rax, (%rbx)
.L54:
	cmpq	$0, (%rbx)
	je	.L47
	cmpl	$1, 0(%r13)
	je	.L47
.L57:
	xorl	%eax, %eax
	jmp	.L11
.L117:
	cmpl	$1, 0(%r13)
	je	.L118
	cmpq	$0, (%rbx)
	je	.L47
	xorl	%eax, %eax
	jmp	.L11
.L62:
	xorl	%ecx, %ecx
	jmp	.L42
.L118:
	leaq	known_libs(%rip), %r8
	xorl	%ebp, %ebp
.L55:
	movq	(%r8), %rsi
	movq	%r14, %rdi
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	strcmp@PLT
	testl	%eax, %eax
	movq	16(%rsp), %r8
	movq	24(%rsp), %rcx
	je	.L119
	addl	$1, %ebp
	addq	$16, %r8
	cmpl	$4, %ebp
	jne	.L55
	jmp	.L47
.L119:
	movl	%ebp, %eax
	leaq	known_libs(%rip), %rsi
	salq	$4, %rax
	movl	8(%rsi,%rax), %eax
	movl	%eax, 0(%r13)
	jmp	.L54
	.size	process_elf32_file, .-process_elf32_file
	.section	.rodata.str1.1
.LC7:
	.string	"%s is a 32 bit ELF file.\n"
	.text
	.p2align 4,,15
	.globl	process_elf64_file
	.type	process_elf64_file, @function
process_elf64_file:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movl	$0, (%rcx)
	movq	144(%rsp), %rbx
	movzbl	4(%rbx), %eax
	cmpb	$2, %al
	je	.L121
	movl	opt_verbose(%rip), %ecx
	testl	%ecx, %ecx
	je	.L218
	cmpb	$1, %al
	movl	$5, %edx
	leaq	.LC7(%rip), %rsi
	je	.L217
	leaq	.LC3(%rip), %rsi
.L217:
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext@PLT
	movq	%r13, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
.L218:
	movl	$1, %eax
.L120:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movzwl	16(%rbx), %ebp
	cmpw	$3, %bp
	jne	.L220
	movq	%rbx, %rax
	addq	32(%rbx), %rax
	movq	%rax, 16(%rsp)
	jc	.L219
	movq	%rcx, %r12
	movq	152(%rsp), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %rax
	movq	%rcx, 32(%rsp)
	ja	.L219
	cmpw	$0, 56(%rbx)
	movl	$1, (%rdx)
	movl	$0, (%r8)
	je	.L218
	movq	16(%rsp), %rbp
	xorl	%r15d, %r15d
	movq	$0, 24(%rsp)
	movl	$0, 8(%rsp)
	movq	%rdi, 48(%rsp)
	movl	%r15d, %r14d
	movq	%rdx, 56(%rsp)
	movq	%r12, 40(%rsp)
	movq	%rbp, %r13
	movq	%r8, 72(%rsp)
	movq	32(%rsp), %rbp
	movq	%r9, 64(%rsp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L132:
	cmpl	$4, %eax
	je	.L134
	cmpl	$1685382483, %eax
	jne	.L130
	cmpq	$8, 48(%r13)
	je	.L221
	.p2align 4,,10
	.p2align 3
.L130:
	movzwl	56(%rbx), %eax
	addl	$1, %r14d
	addq	$56, %r13
	cmpl	%eax, %r14d
	jge	.L222
	cmpq	%r13, %rbx
	ja	.L172
	cmpq	%r13, %rbp
	jb	.L172
.L128:
	movl	0(%r13), %eax
	cmpl	$3, %eax
	je	.L131
	ja	.L132
	cmpl	$2, %eax
	jne	.L130
	movl	8(%rsp), %edx
	testl	%edx, %edx
	jne	.L223
.L136:
	movl	8(%r13), %eax
	movl	%eax, 8(%rsp)
	movq	32(%r13), %rax
	movq	%rax, 24(%rsp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC4(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movzwl	%bp, %r8d
	movq	%r13, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	xorl	%edi, %edi
	call	error@PLT
	addq	$88, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L174:
	movq	8(%rsp), %r13
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$5, %edx
	leaq	.LC5(%rip), %rsi
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L134:
	movq	40(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L130
	movq	32(%r13), %rsi
	cmpq	$31, %rsi
	jbe	.L130
	movq	48(%r13), %r10
	cmpq	$3, %r10
	jbe	.L130
	leaq	-4(%r10), %rax
	testq	$-5, %rax
	jne	.L130
	movq	8(%r13), %rdi
	movq	%r10, %r11
	leaq	11(%r10), %r12
	negq	%r11
	subq	$1, %r10
	addq	%rbx, %rdi
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	addq	%r12, %rax
	addq	%r10, %rdx
	andq	%r11, %rax
	addq	%rdx, %rax
	leaq	-32(%rsi), %rdx
	andq	%r11, %rax
	cmpq	%rdx, %rax
	ja	.L130
	testq	%rax, %rax
	je	.L130
	subq	%rax, %rsi
	addq	%rax, %rdi
.L140:
	movl	(%rdi), %eax
	movl	4(%rdi), %edx
	cmpl	$4, %eax
	jne	.L141
	cmpl	$16, %edx
	jne	.L141
	cmpl	$1, 8(%rdi)
	jne	.L141
	cmpl	$5590599, 12(%rdi)
	jne	.L141
	testq	%rsi, %rsi
	je	.L130
	movzbl	28(%rdi), %edx
	movl	16(%rdi), %eax
	movq	40(%rsp), %rsi
	sall	$24, %eax
	orl	%edx, %eax
	movl	20(%rdi), %edx
	sall	$16, %edx
	andl	$16711680, %edx
	orl	%edx, %eax
	movl	24(%rdi), %edx
	sall	$8, %edx
	movzwl	%dx, %edx
	orl	%edx, %eax
	movl	%eax, (%rsi)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rbx, %r12
	addq	8(%r13), %r12
	jc	.L172
	cmpq	%r12, %rbp
	jb	.L172
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	xorl	%r15d, %r15d
	movq	%rbx, 144(%rsp)
	movq	%r15, %rbx
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L224
.L139:
	addq	$1, %rbx
	cmpq	$4, %rbx
	je	.L208
	leaq	interpreters(%rip), %rcx
	movq	%rbx, %rax
	movq	%r12, %rdi
	salq	$4, %rax
	movq	(%rcx,%rax), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L139
.L224:
	movl	%ebx, %r15d
	leaq	interpreters(%rip), %rax
	movq	56(%rsp), %rdi
	salq	$4, %r15
	movq	144(%rsp), %rbx
	movl	8(%rax,%r15), %eax
	movl	%eax, (%rdi)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L208:
	movq	144(%rsp), %rbx
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L172:
	movq	48(%rsp), %r13
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	.LC6(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L221:
	movq	8(%r13), %rdx
	movq	32(%r13), %rsi
	addq	%rbx, %rdx
	cmpq	$12, %rsi
	movq	%rdx, %rdi
	leaq	12(%rdx), %r10
	ja	.L146
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L142:
	movl	4(%rdx), %ecx
	addq	$19, %rax
	andq	$-8, %rax
	leaq	7(%rax,%rcx), %rax
	andq	$-8, %rax
	addq	%rax, %rdx
	leaq	12(%rdx), %r10
	movq	%r10, %rax
	subq	%rdi, %rax
	cmpq	%rax, %rsi
	jbe	.L130
.L146:
	movl	(%rdx), %eax
	cmpl	$4, %eax
	jne	.L142
	cmpl	$5, 8(%rdx)
	jne	.L142
	cmpl	$5590599, (%r10)
	jne	.L142
	movl	4(%rdx), %esi
	cmpl	$7, %esi
	jbe	.L130
	testb	$7, %sil
	jne	.L130
	leaq	16(%rdx), %rax
	movl	20(%rdx), %r10d
	leaq	(%rax,%rsi), %r12
	movl	16(%rdx), %esi
	jmp	.L143
.L144:
	addq	$7, %rdx
	andq	$-8, %rdx
	leaq	(%rcx,%rdx), %rax
	movq	%r12, %rdx
	subq	%rax, %rdx
	cmpq	$7, %rdx
	jle	.L130
	movl	(%rax), %edx
	movl	4(%rax), %r10d
	cmpl	%esi, %edx
	jb	.L130
	movl	%edx, %esi
.L143:
	leaq	8(%rax), %rcx
	movl	%r10d, %edx
	leaq	(%rcx,%rdx), %rdi
	cmpq	%rdi, %r12
	jb	.L130
	cmpl	$-1073709054, %esi
	ja	.L130
	cmpl	$-1073709054, %esi
	jne	.L144
	cmpl	$4, %r10d
	jne	.L130
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L130
	movq	72(%rsp), %rsi
#APP
# 50 "../sysdeps/unix/sysv/linux/x86/elf-read-prop.h" 1
	bsr %eax, %eax
# 0 "" 2
#NO_APP
	movl	%eax, (%rsi)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L222:
	cmpq	$0, 24(%rsp)
	movq	48(%rsp), %r13
	movq	56(%rsp), %r14
	movq	64(%rsp), %r9
	je	.L218
	movl	8(%rsp), %r12d
	movq	32(%rsp), %rdi
	addq	%rbx, %r12
	cmpq	%r12, %rdi
	jb	.L219
	cmpq	%r12, %rbx
	ja	.L219
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L218
	cmpq	$5, %rsi
	movq	%r12, %rdx
	jne	.L150
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	cmpq	%rdx, %rbx
	ja	.L219
	cmpq	%rdx, %rdi
	jb	.L219
	cmpq	$5, %rcx
	je	.L149
.L150:
	addq	$16, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L155
	jmp	.L218
.L149:
	testl	%eax, %eax
	movq	8(%rdx), %rcx
	je	.L171
	subl	$1, %eax
	movq	16(%rsp), %rdi
	addq	$1, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rdi,%rdx,8), %rdx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$56, %rdi
	cmpq	%rdi, %rdx
	je	.L171
.L153:
	cmpl	$1, (%rdi)
	jne	.L152
	movq	16(%rdi), %rax
	cmpq	%rcx, %rax
	ja	.L152
	movq	%rcx, %r8
	subq	%rax, %r8
	cmpq	32(%rdi), %r8
	jnb	.L152
	subq	8(%rdi), %rax
	cmpq	$-1, %rax
	je	.L171
.L151:
	subq	%rax, %rcx
	leaq	(%rbx,%rcx), %rbp
	cmpq	%rbp, 32(%rsp)
	jb	.L219
	cmpq	%rbp, %rbx
	ja	.L219
	testq	%rbp, %rbp
	je	.L218
	movq	%r13, 8(%rsp)
	movq	%rbx, %rcx
	movq	%r14, %r13
	movq	%rbp, %r14
	movq	%r9, %rbp
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L225:
	cmpq	$14, %rsi
	je	.L173
.L156:
	addq	$16, %r12
	movq	(%r12), %rsi
	testq	%rsi, %rsi
	je	.L166
.L165:
	cmpq	$1, %rsi
	jne	.L225
.L173:
	movq	8(%r12), %r15
	addq	%r14, %r15
	cmpq	%r15, %rcx
	ja	.L174
	cmpq	%r15, 32(%rsp)
	jb	.L174
	cmpq	$1, %rsi
	je	.L226
	movq	%r15, %rdi
	movq	%rcx, 16(%rsp)
	call	xstrdup@PLT
	movq	16(%rsp), %rcx
	movq	%rax, 0(%rbp)
.L163:
	cmpq	$0, 0(%rbp)
	je	.L156
	cmpl	$1, 0(%r13)
	je	.L156
.L166:
	xorl	%eax, %eax
	jmp	.L120
.L226:
	cmpl	$1, 0(%r13)
	je	.L227
	cmpq	$0, 0(%rbp)
	je	.L156
	xorl	%eax, %eax
	jmp	.L120
.L171:
	xorl	%eax, %eax
	jmp	.L151
.L227:
	leaq	known_libs(%rip), %r8
	xorl	%ebx, %ebx
.L164:
	movq	(%r8), %rsi
	movq	%r15, %rdi
	movq	%rcx, 24(%rsp)
	movq	%r8, 16(%rsp)
	call	strcmp@PLT
	testl	%eax, %eax
	movq	16(%rsp), %r8
	movq	24(%rsp), %rcx
	je	.L228
	addl	$1, %ebx
	addq	$16, %r8
	cmpl	$4, %ebx
	jne	.L164
	jmp	.L156
.L228:
	movl	%ebx, %eax
	leaq	known_libs(%rip), %rsi
	salq	$4, %rax
	movl	8(%rsi,%rax), %eax
	movl	%eax, 0(%r13)
	jmp	.L163
	.size	process_elf64_file, .-process_elf64_file
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"%s is for unknown machine %d.\n"
	.text
	.p2align 4,,15
	.globl	process_elf_file
	.type	process_elf_file, @function
process_elf_file:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	48(%rsp), %rax
	movzwl	18(%rax), %ebx
	cmpw	$3, %bx
	je	.L231
	cmpw	$62, %bx
	jne	.L230
	movzbl	4(%rax), %edx
	cmpb	$2, %dl
	je	.L249
	cmpb	$1, %dl
	movl	$2051, %ebx
	movl	$1, %r13d
	je	.L235
.L233:
	pushq	56(%rsp)
	pushq	%rax
	movq	%r12, %rdx
	movq	%rbp, %rdi
	movl	$1, %r13d
	call	process_elf64_file
	popq	%rdx
	popq	%rcx
.L237:
	testl	%eax, %eax
	jne	.L229
	testb	%r13b, %r13b
	je	.L229
	movl	%ebx, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	cmpb	$1, 4(%rax)
	je	.L239
.L230:
	leaq	.LC8(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movzwl	%bx, %r8d
	movq	%rax, %rdx
	movq	%rbp, %rcx
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	error@PLT
	movl	$1, %eax
.L229:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
.L235:
	pushq	56(%rsp)
	pushq	%rax
	movq	%rbp, %rdi
	movq	%r12, %rdx
	call	process_elf32_file
	popq	%rsi
	popq	%rdi
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L249:
	movl	$771, %ebx
	jmp	.L233
	.size	process_elf_file, .-process_elf_file
	.section	.rodata.str1.1
.LC9:
	.string	"rb"
.LC10:
	.string	".so"
.LC11:
	.string	"Input file %s not found.\n"
.LC12:
	.string	"Cannot fstat file %s.\n"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"File %s is empty, not checked."
	.section	.rodata.str1.1
.LC14:
	.string	"\177ELF"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"File %s is too small, not checked."
	.section	.rodata.str1.1
.LC16:
	.string	"Cannot mmap file %s.\n"
.LC17:
	.string	"GROUP"
.LC18:
	.string	"GNU ld script"
.LC19:
	.string	"-gdb.py"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"%s is not an ELF file - it has the wrong magic bytes at the start.\n"
	.text
	.p2align 4,,15
	.globl	process_file
	.type	process_file, @function
process_file:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	.LC9(%rip), %rsi
	movq	%rdx, %r13
	movq	%rcx, %rbp
	subq	$184, %rsp
	movl	$-1, (%rcx)
	movq	240(%rsp), %rax
	movq	%r8, (%rsp)
	movq	$0, (%rax)
	call	fopen@PLT
	testq	%rax, %rax
	je	.L284
	movq	%rax, %rdi
	movq	%rax, %r14
	call	fileno@PLT
	leaq	32(%rsp), %rsi
	movl	%eax, %edi
	call	fstat64@PLT
	testl	%eax, %eax
	movl	$5, %edx
	leaq	.LC12(%rip), %rsi
	js	.L281
	movq	80(%rsp), %rax
	cmpq	$63, %rax
	ja	.L255
	testq	%rax, %rax
	movl	$5, %edx
	leaq	.LC13(%rip), %rsi
	jne	.L285
.L281:
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext@PLT
	movq	%r12, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
.L257:
	movq	%r14, %rdi
	movl	$1, %ebx
	call	fclose@PLT
.L250:
	addq	$184, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	movl	248(%rsp), %esi
	testl	%esi, %esi
	je	.L252
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	movl	$1, %ebx
	call	strstr@PLT
	testq	%rax, %rax
	jne	.L250
.L252:
	leaq	.LC11(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movl	$1, %ebx
	call	__dcgettext@PLT
	movq	%r12, %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L285:
	cmpq	$4, %rax
	movl	$4, %ebx
	leaq	28(%rsp), %rbp
	cmovle	%rax, %rbx
	movq	%r14, %rcx
	movl	$1, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	fread@PLT
	cmpq	$1, %rax
	jne	.L257
	leaq	.LC14(%rip), %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L257
	movl	$5, %edx
	leaq	.LC15(%rip), %rsi
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r14, %rdi
	call	fileno@PLT
	movq	80(%rsp), %rsi
	xorl	%r9d, %r9d
	movl	$1, %edx
	xorl	%edi, %edi
	movl	%eax, %r8d
	movl	$1, %ecx
	call	mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, %r11
	movl	$5, %edx
	leaq	.LC16(%rip), %rsi
	je	.L281
	movzwl	(%rax), %eax
	cmpq	$267, %rax
	je	.L270
	cmpq	$204, %rax
	je	.L270
	cmpl	$1179403647, (%r11)
	movq	80(%rsp), %rbx
	je	.L264
	cmpq	$512, %rbx
	movl	$512, %ebp
	leaq	.LC17(%rip), %rdx
	cmovle	%rbx, %rbp
	movq	%r11, %rdi
	movl	$5, %ecx
	movq	%rbp, %rsi
	movq	%r11, (%rsp)
	call	memmem@PLT
	testq	%rax, %rax
	movq	(%rsp), %r11
	je	.L265
.L282:
	movq	%rbx, %rsi
	movl	$1, %ebx
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L264:
	cmpw	$3, 16(%r11)
	movq	%rbx, %rsi
	movl	$1, %ebx
	je	.L286
.L263:
	movq	%r11, %rdi
	call	munmap@PLT
	movq	%r14, %rdi
	call	fclose@PLT
	movq	256(%rsp), %rax
	movdqa	32(%rsp), %xmm0
	movups	%xmm0, (%rax)
	movdqa	48(%rsp), %xmm0
	movups	%xmm0, 16(%rax)
	movdqa	64(%rsp), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	80(%rsp), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	96(%rsp), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	112(%rsp), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	128(%rsp), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	144(%rsp), %xmm0
	movups	%xmm0, 112(%rax)
	movdqa	160(%rsp), %xmm0
	movups	%xmm0, 128(%rax)
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r13, %rdi
	movq	%r11, (%rsp)
	call	xstrdup@PLT
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	strstr@PLT
	testq	%rax, %rax
	movq	(%rsp), %r11
	je	.L262
	leaq	4(%rax), %rdi
	movl	$46, %esi
	call	strchr@PLT
	testq	%rax, %rax
	movq	(%rsp), %r11
	je	.L262
	movb	$0, (%rax)
.L262:
	movq	240(%rsp), %rax
	movq	80(%rsp), %rsi
	xorl	%ebx, %ebx
	movq	%r12, (%rax)
	movl	$0, 0(%rbp)
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L265:
	leaq	.LC18(%rip), %rdx
	movq	%r11, %rdi
	movl	$13, %ecx
	movq	%rbp, %rsi
	movq	%r11, (%rsp)
	call	memmem@PLT
	testq	%rax, %rax
	movq	(%rsp), %r11
	jne	.L282
	movq	%r12, %rdi
	movq	%r11, (%rsp)
	call	strlen@PLT
	cmpq	$7, %rax
	movq	(%rsp), %r11
	jbe	.L267
	leaq	-7(%r12,%rax), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$8, %ecx
	repz cmpsb
	je	.L282
.L267:
	leaq	.LC20(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	%r11, (%rsp)
	movl	$1, %ebx
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	movq	%r12, %rcx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	error@PLT
	movq	80(%rsp), %rsi
	movq	(%rsp), %r11
	jmp	.L263
.L286:
	pushq	%rsi
	pushq	%r11
	movq	%rbp, %rdx
	movq	256(%rsp), %r9
	movq	16(%rsp), %rcx
	movq	%r13, %rsi
	movq	%r15, %r8
	movq	%r12, %rdi
	movq	%r11, 24(%rsp)
	xorl	%ebx, %ebx
	call	process_elf_file
	popq	%rdx
	testl	%eax, %eax
	popq	%rcx
	setne	%bl
	movq	80(%rsp), %rsi
	movq	8(%rsp), %r11
	jmp	.L263
	.size	process_file, .-process_file
	.section	.rodata.str1.1
.LC21:
	.string	"libc.so.6"
.LC22:
	.string	"libm.so.6"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	known_libs, @object
	.size	known_libs, 64
known_libs:
	.quad	.LC21
	.long	3
	.zero	4
	.quad	.LC22
	.long	3
	.zero	4
	.quad	.LC21
	.long	3
	.zero	4
	.quad	.LC22
	.long	3
	.zero	4
	.section	.rodata.str1.1
.LC23:
	.string	"/lib/ld-linux.so.2"
.LC24:
	.string	"/libx32/ld-linux-x32.so.2"
.LC25:
	.string	"/lib64/ld-linux-x86-64.so.2"
	.section	.data.rel.ro.local
	.align 32
	.type	interpreters, @object
	.size	interpreters, 64
interpreters:
	.quad	.LC1
	.long	3
	.zero	4
	.quad	.LC23
	.long	3
	.zero	4
	.quad	.LC24
	.long	3
	.zero	4
	.quad	.LC25
	.long	3
	.zero	4
