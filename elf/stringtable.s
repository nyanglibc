	.text
	.p2align 4,,15
	.type	finalize_compare, @function
finalize_compare:
	pushq	%rbp
	pushq	%rbx
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	movl	8(%rdi), %r11d
	movl	8(%rsi), %r10d
	cmpl	%r10d, %r11d
	jnb	.L2
	movl	%r11d, %r8d
	testq	%r8, %r8
	je	.L9
	movl	%r10d, %ebx
	movq	%r8, %rbp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%r10d, %r8d
	testq	%r8, %r8
	je	.L5
	movl	%r11d, %ebp
	movq	%r8, %rbx
.L4:
	movzbl	15(%rdi,%rbp), %ecx
	movzbl	15(%rsi,%rbx), %edx
	cmpb	%cl, %dl
	jne	.L6
	xorl	%eax, %eax
	movl	$2, %r9d
	addq	%rbp, %rdi
	addq	%rbx, %rsi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movzbl	14(%rdi,%rax), %ecx
	movzbl	14(%rsi,%rax), %edx
	subq	$1, %rax
	cmpb	%dl, %cl
	jne	.L6
.L7:
	movq	%r9, %rdx
	subq	%rax, %rdx
	cmpq	%rdx, %r8
	jnb	.L8
.L5:
	xorl	%eax, %eax
	cmpl	%r10d, %r11d
	je	.L1
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
.L1:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	%dl, %eax
	subl	%ecx, %eax
	popq	%rbx
	popq	%rbp
	ret
.L9:
	movl	$1, %eax
	jmp	.L1
	.size	finalize_compare, .-finalize_compare
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"String table string is too long"
	.align 8
.LC1:
	.string	"String table has too many entries"
	.text
	.p2align 4,,15
	.globl	stringtable_add
	.type	stringtable_add, @function
stringtable_add:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	12(%rdi), %eax
	testl	%eax, %eax
	je	.L58
.L19:
	movq	%rbp, %rdi
	call	strlen@PLT
	cmpq	$1073741824, %rax
	movq	%rax, %r13
	ja	.L20
	testq	%r13, %r13
	movq	%rbp, %rax
	leaq	0(%rbp,%r13), %rcx
	movl	$-2128831035, %r12d
	je	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	movzbl	(%rax), %edx
	addq	$1, %rax
	xorl	%edx, %r12d
	imull	$16777619, %r12d, %r12d
	cmpq	%rax, %rcx
	jne	.L23
.L22:
	movl	12(%rbx), %r14d
	movq	(%rbx), %rdx
	leal	-1(%r14), %eax
	andl	%r12d, %eax
	movq	(%rdx,%rax,8), %r15
	testq	%r15, %r15
	jne	.L27
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.L24
.L27:
	movl	8(%r15), %eax
	cmpq	%r13, %rax
	jne	.L25
	leaq	16(%r15), %rdi
	movq	%r13, %rdx
	movq	%rbp, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L25
.L18:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	8(%rbx), %eax
	cmpl	$1073741823, %eax
	ja	.L59
	leal	(%rax,%rax,2), %edx
	addl	%r14d, %r14d
	cmpl	%r14d, %edx
	ja	.L60
.L29:
	addl	$1, %eax
	leaq	17(%r13), %rdi
	movl	%eax, 8(%rbx)
	call	xmalloc@PLT
	movq	%rax, %r15
	movl	12(%rbx), %eax
	movq	%rbp, %rsi
	leaq	16(%r15), %rdi
	subl	$1, %eax
	andl	%eax, %r12d
	movq	(%rbx), %rax
	leaq	(%rax,%r12,8), %rax
	movq	(%rax), %rdx
	movq	%rdx, (%r15)
	leaq	1(%r13), %rdx
	movq	%r15, (%rax)
	movl	%r13d, 8(%r15)
	movl	$0, 12(%r15)
	call	memcpy@PLT
	jmp	.L18
.L58:
	movabsq	$549755813888, %rax
	movl	$8, %esi
	movq	%rax, 8(%rdi)
	movl	$128, %edi
	call	xcalloc@PLT
	movq	%rax, (%rbx)
	jmp	.L19
.L60:
	movl	%r14d, %edi
	movl	$8, %esi
	call	xcalloc@PLT
	movl	12(%rbx), %edx
	movq	%rax, %r15
	leal	-1(%r14), %eax
	testl	%edx, %edx
	je	.L30
	movq	(%rbx), %rdi
	subl	$1, %edx
	movq	%rbx, 8(%rsp)
	movl	$-2128831035, %r10d
	movq	%rdi, (%rsp)
	movq	%rdi, %r11
	leaq	8(%rdi,%rdx,8), %rdi
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r11), %r8
	testq	%r8, %r8
	je	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	movl	8(%r8), %esi
	movq	(%r8), %rdi
	leaq	16(%r8), %rcx
	movl	%r10d, %edx
	testq	%rsi, %rsi
	je	.L32
	addq	%rcx, %rsi
	.p2align 4,,10
	.p2align 3
.L33:
	movzbl	(%rcx), %r9d
	addq	$1, %rcx
	xorl	%r9d, %edx
	imull	$16777619, %edx, %edx
	cmpq	%rcx, %rsi
	jne	.L33
.L32:
	andl	%eax, %edx
	testq	%rdi, %rdi
	leaq	(%r15,%rdx,8), %rdx
	movq	(%rdx), %rcx
	movq	%rcx, (%r8)
	movq	%r8, (%rdx)
	movq	%rdi, %r8
	jne	.L34
.L31:
	addq	$8, %r11
	cmpq	%r11, %rbx
	jne	.L35
	movq	8(%rsp), %rbx
.L36:
	movq	(%rsp), %rdi
	call	free@PLT
	movq	%r15, (%rbx)
	movl	%r14d, 12(%rbx)
	movl	8(%rbx), %eax
	jmp	.L29
.L30:
	movq	(%rbx), %rax
	movq	%rax, (%rsp)
	jmp	.L36
.L59:
	leaq	.LC1(%rip), %rsi
	movl	$5, %edx
.L57:
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
.L20:
	movl	$5, %edx
	leaq	.LC0(%rip), %rsi
	jmp	.L57
	.size	stringtable_add, .-stringtable_add
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	""
.LC3:
	.string	"stringtable.c"
.LC4:
	.string	"j == table->count"
.LC5:
	.string	"String table is too large"
	.text
	.p2align 4,,15
	.globl	stringtable_finalize
	.type	stringtable_finalize, @function
stringtable_finalize:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$40, %rsp
	movl	8(%rdi), %edi
	testl	%edi, %edi
	je	.L96
	movl	$8, %esi
	call	xcalloc@PLT
	movl	12(%r12), %edx
	movq	%rax, %r15
	testl	%edx, %edx
	je	.L80
	movq	(%r12), %rax
	leal	-1(%rdx), %ecx
	xorl	%esi, %esi
	leaq	8(%rax), %rdx
	leaq	(%rdx,%rcx,8), %rcx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rax, (%r15,%rsi,8)
	addq	$1, %rsi
.L95:
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L66
	cmpq	%rdx, %rcx
	movq	%rdx, %rax
	je	.L64
	addq	$8, %rdx
	jmp	.L95
.L80:
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L64:
	movl	8(%r12), %eax
	cmpq	%rsi, %rax
	jne	.L97
	leaq	finalize_compare(%rip), %rcx
	movl	$8, %edx
	movq	%r15, %rdi
	call	qsort@PLT
	movl	8(%r12), %ecx
	movq	(%r15), %rax
	cmpl	$1, %ecx
	movl	$0, 12(%rax)
	movl	%ecx, 28(%rsp)
	jbe	.L69
	leal	-2(%rcx), %edx
	leaq	8(%r15), %r13
	xorl	%r14d, %r14d
	leaq	8(%r15,%rdx,8), %rcx
	movq	%rcx, 8(%rsp)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L98:
	movl	%ebx, %edi
	leaq	16(%rcx), %rsi
	movl	%r10d, %edx
	subl	%r10d, %edi
	movq	%rcx, 16(%rsp)
	movl	%r10d, 24(%rsp)
	leaq	16(%rax,%rdi), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	movq	16(%rsp), %rcx
	jne	.L70
	movl	24(%rsp), %r10d
	addl	%r14d, %ebx
	subl	%r10d, %ebx
	movl	%ebx, 12(%rcx)
.L71:
	cmpq	8(%rsp), %r13
	je	.L69
	movl	12(%rcx), %r14d
	addq	$8, %r13
	movq	%rcx, %rax
.L74:
	movq	0(%r13), %rcx
	movl	8(%rax), %ebx
	movl	8(%rcx), %r10d
	cmpl	%r10d, %ebx
	jnb	.L98
.L70:
	addl	$1, %ebx
	addl	%r14d, %ebx
	movl	%ebx, 12(%rcx)
	jnc	.L71
	leaq	.LC5(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	xorl	%esi, %esi
	movq	%rax, %rdx
	movl	$1, %edi
	xorl	%eax, %eax
	call	error@PLT
	.p2align 4,,10
	.p2align 3
.L69:
	movl	28(%rsp), %eax
	movq	%r15, %rdi
	subl	$1, %eax
	movq	(%r15,%rax,8), %rax
	movl	8(%rax), %edx
	movl	12(%rax), %eax
	leal	1(%rdx), %ebx
	addq	%rax, %rbx
	movq	%rbx, 8(%rbp)
	call	free@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	xcalloc@PLT
	movq	%rax, 0(%rbp)
	movl	12(%r12), %eax
	testl	%eax, %eax
	je	.L61
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L79:
	movq	(%r12), %rdx
	movl	%r13d, %eax
	movq	(%rdx,%rax,8), %rbx
	testq	%rbx, %rbx
	jne	.L78
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L76
.L78:
	movl	12(%rbx), %edi
	addq	0(%rbp), %rdi
	cmpb	$0, (%rdi)
	jne	.L77
	movl	8(%rbx), %eax
	leaq	16(%rbx), %rsi
	leal	1(%rax), %edx
	call	memcpy@PLT
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L78
.L76:
	addl	$1, %r13d
	cmpl	%r13d, 12(%r12)
	ja	.L79
.L61:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	.LC2(%rip), %rdi
	call	xstrdup@PLT
	movq	$0, 8(%rbp)
	movq	%rax, 0(%rbp)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L97:
	leaq	__PRETTY_FUNCTION__.4199(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$170, %edx
	call	__assert_fail@PLT
	.size	stringtable_finalize, .-stringtable_finalize
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.4199, @object
	.size	__PRETTY_FUNCTION__.4199, 21
__PRETTY_FUNCTION__.4199:
	.string	"stringtable_finalize"
