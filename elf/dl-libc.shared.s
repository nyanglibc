	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	do_dlopen, @function
do_dlopen:
	movq	__environ@GOTPCREL(%rip), %rax
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdx
	movl	8(%rdi), %esi
	movq	$-2, %rcx
	subq	$8, %rsp
	movq	__libc_argv(%rip), %r9
	movl	__libc_argc(%rip), %r8d
	pushq	(%rax)
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	(%rdi), %rdi
	call	*760(%rax)
	movq	%rax, 24(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	do_dlopen, .-do_dlopen
	.p2align 4,,15
	.type	do_dlsym, @function
do_dlsym:
	pushq	%rbx
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	%rdi, %rbx
	movq	(%rdi), %rsi
	movq	$0, 24(%rdi)
	leaq	24(%rdi), %rdx
	xorl	%r9d, %r9d
	movq	8(%rdi), %rdi
	xorl	%r8d, %r8d
	pushq	$0
	pushq	$2
	leaq	928(%rsi), %rcx
	call	*752(%rax)
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	do_dlsym, .-do_dlsym
	.p2align 4,,15
	.type	do_dlvsym, @function
do_dlvsym:
	pushq	%rbx
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	%rdi, %rbx
	movq	(%rdi), %rsi
	movq	$0, 24(%rdi)
	leaq	24(%rdi), %rdx
	leaq	32(%rbx), %r8
	movq	8(%rdi), %rdi
	xorl	%r9d, %r9d
	pushq	$0
	pushq	$0
	leaq	928(%rsi), %rcx
	call	*752(%rax)
	movq	%rax, 16(%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	do_dlvsym, .-do_dlvsym
	.p2align 4,,15
	.type	do_dlclose, @function
do_dlclose:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	jmp	*768(%rax)
	.size	do_dlclose, .-do_dlclose
	.p2align 4,,15
	.type	dlerror_run, @function
dlerror_run:
	subq	$56, %rsp
	movq	%rdi, %rcx
	movq	%rsi, %r8
	leaq	31(%rsp), %rdx
	leaq	40(%rsp), %rsi
	leaq	32(%rsp), %rdi
	movq	$0, 40(%rsp)
	call	__GI__dl_catch_error
	testl	%eax, %eax
	jne	.L10
	cmpq	$0, 40(%rsp)
	jne	.L13
.L9:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L10:
	cmpb	$0, 31(%rsp)
	je	.L9
	movq	40(%rsp), %rdi
	movl	%eax, 12(%rsp)
	call	free@PLT
	movl	12(%rsp), %eax
	addq	$56, %rsp
	ret
	.size	dlerror_run, .-dlerror_run
	.p2align 4,,15
	.globl	__GI___libc_dlopen_mode
	.hidden	__GI___libc_dlopen_mode
	.type	__GI___libc_dlopen_mode, @function
__GI___libc_dlopen_mode:
	subq	$40, %rsp
	movq	40(%rsp), %rax
	movq	%rdi, (%rsp)
	movl	%esi, 8(%rsp)
	movq	%rax, 16(%rsp)
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L15
	movq	__GI__dl_open_hook(%rip), %rax
	call	*(%rax)
.L14:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	do_dlopen(%rip), %rdi
	movq	%rsp, %rsi
	call	dlerror_run
	testl	%eax, %eax
	jne	.L17
	movq	24(%rsp), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	jmp	.L14
	.size	__GI___libc_dlopen_mode, .-__GI___libc_dlopen_mode
	.globl	__libc_dlopen_mode
	.set	__libc_dlopen_mode,__GI___libc_dlopen_mode
	.p2align 4,,15
	.globl	__GI___libc_dlsym
	.hidden	__GI___libc_dlsym
	.type	__GI___libc_dlsym, @function
__GI___libc_dlsym:
	subq	$40, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	cmpq	$0, 664(%rax)
	jne	.L20
	movq	__GI__dl_open_hook(%rip), %rax
	call	*8(%rax)
.L19:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	do_dlsym(%rip), %rdi
	movq	%rsp, %rsi
	call	dlerror_run
	testl	%eax, %eax
	jne	.L24
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L24
	cmpw	$-15, 6(%rdx)
	je	.L25
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L22
	movq	(%rcx), %rax
.L22:
	addq	8(%rdx), %rax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
	jmp	.L22
	.size	__GI___libc_dlsym, .-__GI___libc_dlsym
	.globl	__libc_dlsym
	.set	__libc_dlsym,__GI___libc_dlsym
	.p2align 4,,15
	.globl	__GI___libc_dlvsym
	.hidden	__GI___libc_dlvsym
	.type	__GI___libc_dlvsym, @function
__GI___libc_dlvsym:
	subq	$72, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	je	.L55
	movq	%rsi, 8(%rsp)
	movzbl	(%rdx), %esi
	xorl	%eax, %eax
	movq	%rdi, (%rsp)
	movq	%rdx, 32(%rsp)
	movl	$1, 44(%rsp)
	testq	%rsi, %rsi
	je	.L33
	movzbl	1(%rdx), %ecx
	movzbl	%sil, %eax
	testb	%cl, %cl
	je	.L33
	movzbl	%cl, %eax
	movzbl	2(%rdx), %ecx
	salq	$4, %rsi
	addq	%rsi, %rax
	testb	%cl, %cl
	je	.L33
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	3(%rdx), %ecx
	testb	%cl, %cl
	je	.L33
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	4(%rdx), %ecx
	testb	%cl, %cl
	je	.L33
	salq	$4, %rax
	addq	%rcx, %rax
	leaq	5(%rdx), %rcx
	movzbl	5(%rdx), %edx
	testb	%dl, %dl
	je	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	salq	$4, %rax
	addq	$1, %rcx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$240, %edx
	xorq	%rdx, %rax
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L39
.L38:
	andl	$268435455, %eax
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	do_dlvsym(%rip), %rdi
	movq	%rsp, %rsi
	movl	%eax, 40(%rsp)
	movq	$0, 48(%rsp)
	call	dlerror_run
	testl	%eax, %eax
	jne	.L40
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L40
	cmpw	$-15, 6(%rdx)
	je	.L43
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L41
	movq	(%rcx), %rax
.L41:
	addq	8(%rdx), %rax
.L29:
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	__GI__dl_open_hook2(%rip), %rax
	testq	%rax, %rax
	je	.L40
	call	*24(%rax)
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%eax, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%eax, %eax
	jmp	.L41
	.size	__GI___libc_dlvsym, .-__GI___libc_dlvsym
	.globl	__libc_dlvsym
	.set	__libc_dlvsym,__GI___libc_dlvsym
	.p2align 4,,15
	.globl	__GI___libc_dlclose
	.hidden	__GI___libc_dlclose
	.type	__GI___libc_dlclose, @function
__GI___libc_dlclose:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$0, 664(%rax)
	jne	.L57
	movq	__GI__dl_open_hook(%rip), %rax
	jmp	*16(%rax)
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rdi, %rsi
	leaq	do_dlclose(%rip), %rdi
	jmp	dlerror_run
	.size	__GI___libc_dlclose, .-__GI___libc_dlclose
	.globl	__libc_dlclose
	.set	__libc_dlclose,__GI___libc_dlclose
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_slotinfo, @function
free_slotinfo:
	movq	(%rdi), %rdx
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L77
	rep ret
	.p2align 4,,10
	.p2align 3
.L77:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	8(%rdx), %rdi
	subq	$16, %rsp
	call	free_slotinfo
	testb	%al, %al
	je	.L58
	movq	(%rbx), %rdi
	movq	(%rdi), %rsi
	testq	%rsi, %rsi
	je	.L60
	cmpq	$0, 24(%rdi)
	jne	.L65
	leaq	40(%rdi), %rcx
	xorl	%edx, %edx
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L62:
	addq	$16, %rcx
	cmpq	$0, -16(%rcx)
	jne	.L65
.L61:
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	jne	.L62
.L60:
	movb	%al, 15(%rsp)
	call	free@PLT
	movzbl	15(%rsp), %eax
	movq	$0, (%rbx)
.L58:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	xorl	%eax, %eax
	jmp	.L58
	.size	free_slotinfo, .-free_slotinfo
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	_rtld_global@GOTPCREL(%rip), %r13
	movq	_rtld_global_ro@GOTPCREL(%rip), %r14
	movq	2560(%r13), %rdi
	movq	664(%r14), %rbp
	cmpq	%rbp, %rdi
	je	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	movq	(%rdi), %rbx
	call	free@PLT
	cmpq	%rbp, %rbx
	movq	%rbx, %rdi
	jne	.L80
.L79:
	cmpq	$0, 2432(%r13)
	je	.L81
	movq	%r13, %rbp
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L89:
	movq	0(%rbp), %r15
	testq	%r15, %r15
	je	.L82
	.p2align 4,,10
	.p2align 3
.L87:
	movq	56(%r15), %rax
	movq	8(%rax), %rdi
	movq	$0, 8(%rax)
	testq	%rdi, %rdi
	jne	.L85
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	je	.L83
.L85:
	movl	16(%rdi), %edx
	movq	8(%rdi), %rbx
	testl	%edx, %edx
	jne	.L84
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L85
.L83:
	testb	$1, 798(%r15)
	jne	.L106
.L86:
	movq	$0, 976(%r15)
	movq	24(%r15), %r15
	testq	%r15, %r15
	jne	.L87
.L82:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	jne	.L107
.L88:
	addq	$1, %r12
	addq	$152, %rbp
	cmpq	%r12, 2432(%r13)
	ja	.L89
.L81:
	cmpq	$0, 4080(%r13)
	je	.L108
	movq	4032(%r13), %rax
	leaq	8(%rax), %rdi
	call	free_slotinfo
.L91:
	movq	4104(%r13), %rdi
	movq	$0, 4104(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L106:
	movq	976(%r15), %rdi
	call	free@PLT
	jmp	.L86
.L107:
	movq	16(%rbp), %rax
	movl	48(%r14), %ecx
	cmpl	%ecx, 8(%rax)
	jne	.L88
	movq	40(%r14), %rdx
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	movl	$0, 24(%rbp)
	call	free@PLT
	jmp	.L88
.L108:
	leaq	4032(%r13), %rdi
	call	free_slotinfo
	jmp	.L91
	.size	free_mem, .-free_mem
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.hidden	__GI__dl_open_hook2
	.globl	__GI__dl_open_hook2
	.bss
	.align 8
	.type	__GI__dl_open_hook2, @object
	.size	__GI__dl_open_hook2, 8
__GI__dl_open_hook2:
	.zero	8
	.globl	_dl_open_hook2
	.set	_dl_open_hook2,__GI__dl_open_hook2
	.hidden	__GI__dl_open_hook
	.globl	__GI__dl_open_hook
	.align 8
	.type	__GI__dl_open_hook, @object
	.size	__GI__dl_open_hook, 8
__GI__dl_open_hook:
	.zero	8
	.globl	_dl_open_hook
	.set	_dl_open_hook,__GI__dl_open_hook
	.hidden	__libc_argc
	.hidden	__libc_argv
