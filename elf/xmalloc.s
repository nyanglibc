	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"memory exhausted"
	.text
	.p2align 4,,15
	.type	fixup_null_alloc, @function
fixup_null_alloc:
	subq	$8, %rsp
	testq	%rdi, %rdi
	jne	.L4
	movl	$1, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L4
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	.LC0(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__dcgettext@PLT
	movl	xmalloc_exit_failure(%rip), %edi
	movq	%rax, %rdx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	error@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	fixup_null_alloc, .-fixup_null_alloc
	.p2align 4,,15
	.globl	xmalloc
	.type	xmalloc, @function
xmalloc:
	pushq	%rbx
	movq	%rdi, %rbx
	call	malloc@PLT
	testq	%rax, %rax
	je	.L13
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fixup_null_alloc
	.size	xmalloc, .-xmalloc
	.p2align 4,,15
	.globl	xcalloc
	.type	xcalloc, @function
xcalloc:
	pushq	%rbx
	movq	%rdi, %rbx
	call	calloc@PLT
	testq	%rax, %rax
	je	.L17
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fixup_null_alloc
	.size	xcalloc, .-xcalloc
	.p2align 4,,15
	.globl	xrealloc
	.type	xrealloc, @function
xrealloc:
	testq	%rdi, %rdi
	je	.L23
	pushq	%rbx
	movq	%rsi, %rbx
	call	realloc@PLT
	testq	%rax, %rax
	je	.L24
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, %rdi
	jmp	xmalloc
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fixup_null_alloc
	.size	xrealloc, .-xrealloc
	.globl	xmalloc_exit_failure
	.data
	.align 4
	.type	xmalloc_exit_failure, @object
	.size	xmalloc_exit_failure, 4
xmalloc_exit_failure:
	.long	1
