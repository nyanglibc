	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"<main program>"
.LC1:
	.string	"dl-fini.c"
.LC2:
	.string	"i < nloaded"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"ns != LM_ID_BASE || i == nloaded"
	.align 8
.LC4:
	.string	"ns == LM_ID_BASE || i == nloaded || i == nloaded - 1"
	.section	.rodata.str1.1
.LC5:
	.string	"\ncalling fini: %s [%lu]\n\n"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"\nruntime linker statistics:\n           final number of relocations: %lu\nfinal number of relocations from cache: %lu\n"
	.text
	.p2align 4,,15
	.globl	_dl_fini
	.hidden	_dl_fini
	.type	_dl_fini, @function
_dl_fini:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$40, %rsp
	movq	2432+_rtld_local(%rip), %r15
	subq	$1, %r15
	js	.L2
	movl	$0, -56(%rbp)
.L32:
	leaq	(%r15,%r15,8), %rax
	leaq	_rtld_local(%rip), %rcx
	leaq	(%r15,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rbx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
.L5:
	subq	$1, %r15
	subq	$152, %rbx
	cmpq	$-1, %r15
	je	.L62
.L28:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	movl	8(%rbx), %edx
	testl	%edx, %edx
	je	.L3
	movq	(%rbx), %rax
	movzbl	797(%rax), %r12d
	shrb	$3, %r12b
	movl	%r12d, %esi
	andl	$1, %esi
	cmpl	-56(%rbp), %esi
	movl	%esi, -52(%rbp)
	jne	.L3
	movl	%edx, %ecx
	movq	%rsp, -72(%rbp)
	xorl	%r14d, %r14d
	leaq	22(,%rcx,8), %rcx
	shrq	$4, %rcx
	salq	$4, %rcx
	subq	%rcx, %rsp
	movq	%rsp, %r13
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L63
.L8:
	cmpq	%rax, 40(%rax)
	jne	.L6
	cmpl	%r14d, %edx
	jbe	.L64
	movl	%r14d, %ecx
	movq	%rax, 0(%r13,%rcx,8)
	movl	%r14d, 1012(%rax)
	addl	$1, %r14d
	addl	$1, 792(%rax)
	movq	24(%rax), %rax
	testq	%rax, %rax
	jne	.L8
.L63:
	cmpl	%edx, %r14d
	sete	%cl
	testq	%r15, %r15
	jne	.L9
	testb	%cl, %cl
	je	.L65
.L9:
	testq	%r15, %r15
	sete	%al
	testb	%cl, %cl
	jne	.L10
	testb	%al, %al
	jne	.L10
	subl	$1, %edx
	cmpl	%edx, %r14d
	jne	.L66
	movq	%r13, %rdi
	movl	%r14d, %esi
.L11:
	xorl	%edx, %edx
	movl	$1, %ecx
	call	_dl_sort_maps
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	testl	%r14d, %r14d
	je	.L12
	leal	-1(%r14), %eax
	movq	%r15, -64(%rbp)
	movq	%rbx, -80(%rbp)
	leaq	8(%r13,%rax,8), %r14
	.p2align 4,,10
	.p2align 3
.L27:
	movq	0(%r13), %rbx
	movzbl	796(%rbx), %eax
	testb	$8, %al
	je	.L13
	andl	$-9, %eax
	movb	%al, 796(%rbx)
	movq	272(%rbx), %rax
	testq	%rax, %rax
	je	.L67
	testb	$2, _rtld_local_ro(%rip)
	jne	.L33
.L34:
	movq	8(%rax), %r12
	movq	288(%rbx), %rax
	addq	(%rbx), %r12
	movq	8(%rax), %rax
	shrq	$3, %rax
	testl	%eax, %eax
	leal	-1(%rax), %edx
	je	.L18
	leaq	(%r12,%rdx,8), %r15
	subq	$8, %r12
	.p2align 4,,10
	.p2align 3
.L21:
	call	*(%r15)
	subq	$8, %r15
	cmpq	%r12, %r15
	jne	.L21
.L18:
	movq	168(%rbx), %rax
	testq	%rax, %rax
	je	.L16
.L35:
	movq	8(%rax), %rax
	addq	(%rbx), %rax
	call	*%rax
.L16:
	movl	-52(%rbp), %esi
	testl	%esi, %esi
	jne	.L13
	movl	800+_rtld_local_ro(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L68
.L13:
	addq	$8, %r13
	subl	$1, 792(%rbx)
	cmpq	%r13, %r14
	jne	.L27
	movq	-64(%rbp), %r15
	movq	-80(%rbp), %rbx
.L12:
	movq	-72(%rbp), %rsp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L62:
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jne	.L2
	movl	800+_rtld_local_ro(%rip), %eax
	testl	%eax, %eax
	jne	.L69
.L2:
	testb	$-128, _rtld_local_ro(%rip)
	jne	.L70
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	%al, %eax
	movl	%r14d, %esi
	leaq	8(%r13), %rdi
	subl	%eax, %esi
	testq	%r15, %r15
	cmovne	%r13, %rdi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L67:
	movq	168(%rbx), %rax
	testq	%rax, %rax
	je	.L16
	testb	$2, _rtld_local_ro(%rip)
	je	.L35
	.p2align 4,,10
	.p2align 3
.L33:
	movq	8(%rbx), %rsi
	cmpb	$0, (%rsi)
	je	.L71
.L17:
	movq	-64(%rbp), %rdx
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	272(%rbx), %rax
	testq	%rax, %rax
	je	.L18
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L71:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L17
.L69:
	movq	2432+_rtld_local(%rip), %r15
	movl	$1, -56(%rbp)
	subq	$1, %r15
	jns	.L32
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L68:
	movq	792+_rtld_local_ro(%rip), %rdx
	xorl	%r12d, %r12d
	movq	%rdx, %r15
	.p2align 4,,10
	.p2align 3
.L26:
	movq	56(%r15), %rax
	testq	%rax, %rax
	je	.L23
	movl	%r12d, %edx
	movq	%rdx, %rcx
	salq	$4, %rcx
	leaq	1160(%rbx,%rcx), %rdi
	leaq	2568+_rtld_local(%rip), %rcx
	cmpq	%rcx, %rbx
	je	.L72
.L25:
	call	*%rax
.L23:
	addl	$1, %r12d
	cmpl	%r12d, 800+_rtld_local_ro(%rip)
	movq	64(%r15), %r15
	ja	.L26
	jmp	.L13
.L72:
	addq	$233, %rdx
	subq	$2568, %rcx
	salq	$4, %rdx
	leaq	(%rcx,%rdx), %rdi
	jmp	.L25
.L70:
	movq	2552+_rtld_local(%rip), %rdx
	movq	2544+_rtld_local(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L64:
	leaq	__PRETTY_FUNCTION__.8902(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$78, %edx
	call	__GI___assert_fail
.L66:
	leaq	__PRETTY_FUNCTION__.8902(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$89, %edx
	call	__GI___assert_fail
.L65:
	leaq	__PRETTY_FUNCTION__.8902(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$88, %edx
	call	__GI___assert_fail
	.size	_dl_fini, .-_dl_fini
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8902, @object
	.size	__PRETTY_FUNCTION__.8902, 9
__PRETTY_FUNCTION__.8902:
	.string	"_dl_fini"
	.hidden	_dl_debug_printf
	.hidden	_rtld_local_ro
	.hidden	_dl_sort_maps
	.hidden	_rtld_local
