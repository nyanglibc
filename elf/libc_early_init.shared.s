	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_early_init
	.type	__libc_early_init, @function
__libc_early_init:
	pushq	%rbx
	movl	%edi, %ebx
	call	__GI___ctype_init
	movq	__libc_single_threaded@GOTPCREL(%rip), %rax
	movb	%bl, __libc_initial(%rip)
	movb	%bl, (%rax)
	popq	%rbx
	ret
	.size	__libc_early_init, .-__libc_early_init
	.hidden	__libc_initial
	.comm	__libc_initial,1,1
