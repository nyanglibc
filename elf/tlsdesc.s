	.text
	.p2align 4,,15
	.globl	_dl_tlsdesc_resolve_rela_fixup
	.hidden	_dl_tlsdesc_resolve_rela_fixup
	.type	_dl_tlsdesc_resolve_rela_fixup, @function
_dl_tlsdesc_resolve_rela_fixup:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$32, %rsp
	movq	664(%rsi), %rax
	movq	(%rsi), %rbp
	movq	8(%rdi), %r12
	addq	8(%rax), %rbp
	movq	(%rdi), %rax
	cmpq	%rax, %rbp
	je	.L26
.L1:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, %rbx
	je	.L3
	leaq	_dl_load_lock(%rip), %rdi
	movq	%rsi, 8(%rsp)
	call	__pthread_mutex_lock@PLT
	movq	8(%rsp), %rsi
.L3:
	movq	(%rbx), %rax
	cmpq	%rax, %rbp
	je	.L4
.L24:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	_dl_tlsdesc_resolve_hold(%rip), %rax
	movq	%rax, (%rbx)
	movq	104(%rsi), %rax
	movl	12(%r12), %ecx
	movq	8(%rax), %rdi
	movq	112(%rsi), %rax
	leaq	(%rcx,%rcx,2), %rdx
	movq	8(%rax), %rax
	leaq	(%rax,%rdx,8), %rax
	movq	%rax, 24(%rsp)
	movzbl	4(%rax), %edx
	shrb	$4, %dl
	testb	%dl, %dl
	je	.L6
	testb	$3, 5(%rax)
	jne	.L6
	movq	464(%rsi), %rdx
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.L7
	movq	8(%rdx), %rdx
	movzwl	(%rdx,%rcx,2), %edx
	andl	$32767, %edx
	leaq	(%rdx,%rdx,2), %rcx
	movq	744(%rsi), %rdx
	leaq	(%rdx,%rcx,8), %r8
	movl	$0, %edx
	movl	8(%r8), %r9d
	testl	%r9d, %r9d
	cmove	%rdx, %r8
.L7:
	movl	(%rax), %eax
	movq	920(%rsi), %rcx
	leaq	24(%rsp), %rdx
	pushq	$0
	pushq	$1
	movl	$1, %r9d
	addq	%rax, %rdi
	call	_dl_lookup_symbol_x
	movq	%rax, %rsi
	movq	40(%rsp), %rax
	popq	%rdx
	popq	%rcx
	testq	%rax, %rax
	jne	.L6
	movq	16(%r12), %rax
	movq	%rax, 8(%rbx)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L6:
	movq	1112(%rsi), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L27
.L9:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%r12), %rax
	movq	%rax, 8(%rbx)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	_dl_allocate_static_tls
	movq	8(%rsp), %rsi
	movq	24(%rsp), %rax
	movq	1112(%rsi), %rdx
	jmp	.L9
	.size	_dl_tlsdesc_resolve_rela_fixup, .-_dl_tlsdesc_resolve_rela_fixup
	.p2align 4,,15
	.globl	_dl_tlsdesc_resolve_hold_fixup
	.hidden	_dl_tlsdesc_resolve_hold_fixup
	.type	_dl_tlsdesc_resolve_hold_fixup, @function
_dl_tlsdesc_resolve_hold_fixup:
	movq	(%rdi), %rax
	cmpq	%rsi, %rax
	je	.L45
.L40:
	rep ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L41
	leaq	_dl_load_lock(%rip), %rdi
	subq	$8, %rsp
	call	__pthread_mutex_lock@PLT
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L28
	leaq	_dl_load_lock(%rip), %rdi
	addq	$8, %rsp
	jmp	__pthread_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L40
	leaq	_dl_load_lock(%rip), %rdi
	jmp	__pthread_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$8, %rsp
	ret
	.size	_dl_tlsdesc_resolve_hold_fixup, .-_dl_tlsdesc_resolve_hold_fixup
	.p2align 4,,15
	.globl	_dl_unmap
	.hidden	_dl_unmap
	.type	_dl_unmap, @function
_dl_unmap:
	movq	856(%rdi), %rax
	movq	864(%rdi), %rsi
	movq	%rax, %rdi
	subq	%rax, %rsi
	jmp	__munmap
	.size	_dl_unmap, .-_dl_unmap
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__munmap
	.hidden	_dl_allocate_static_tls
	.hidden	_dl_tlsdesc_return
	.hidden	_dl_tlsdesc_undefweak
	.hidden	_dl_lookup_symbol_x
	.hidden	_dl_tlsdesc_resolve_hold
