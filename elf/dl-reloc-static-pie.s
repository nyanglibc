	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%s: Symbol `%s' causes overflow in R_X86_64_32 relocation\n"
	.align 8
.LC1:
	.string	"%s: Symbol `%s' causes overflow in R_X86_64_PC32 relocation\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	"<program name unknown>"
.LC3:
	.string	"get-dynamic-info.h"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"info[DT_PLTREL]->d_un.d_val == DT_RELA"
	.align 8
.LC5:
	.string	"info[DT_RELAENT]->d_un.d_val == sizeof (ElfW(Rela))"
	.section	.rodata.str1.1
.LC6:
	.string	"info[DT_RUNPATH] == NULL"
.LC7:
	.string	"info[DT_RPATH] == NULL"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"../sysdeps/x86_64/dl-machine.h"
	.align 8
.LC9:
	.string	"ELFW(R_TYPE) (reloc->r_info) == R_X86_64_RELATIVE"
	.text
	.p2align 4,,15
	.globl	_dl_relocate_static_pie
	.hidden	_dl_relocate_static_pie
	.type	_dl_relocate_static_pie, @function
_dl_relocate_static_pie:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	call	_dl_get_dl_main_map
	leaq	_DYNAMIC(%rip), %rdx
	movq	%rax, %r12
	leaq	64(%rax), %rcx
	movq	%rdx, %r13
	subq	_GLOBAL_OFFSET_TABLE_(%rip), %r13
	movq	%rdx, 16(%rax)
	movq	%r13, (%rax)
	movq	_DYNAMIC(%rip), %rax
	testq	%rax, %rax
	je	.L2
	movl	$1879048191, %edi
	movl	$1879047679, %r10d
	movl	$1879047935, %ebx
	movl	$1879048001, %ebp
	movl	$1879047733, %r11d
	movl	$50, %r9d
	movl	$1879048226, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L229:
	movq	%r8, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
.L3:
	movq	%rdx, (%rcx,%rax,8)
.L7:
	addq	$16, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L2
.L8:
	cmpq	$34, %rax
	jbe	.L3
	movq	%rdi, %rsi
	subq	%rax, %rsi
	cmpq	$15, %rsi
	jbe	.L229
	leal	(%rax,%rax), %esi
	sarl	%esi
	cmpl	$-4, %esi
	jbe	.L5
	movl	%r9d, %eax
	subl	%esi, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%r13, %r13
	je	.L10
	movq	96(%r12), %rax
	testq	%rax, %rax
	je	.L11
	addq	%r13, 8(%rax)
.L11:
	movq	88(%r12), %rax
	testq	%rax, %rax
	je	.L12
	addq	%r13, 8(%rax)
.L12:
	movq	104(%r12), %rax
	testq	%rax, %rax
	je	.L13
	addq	%r13, 8(%rax)
.L13:
	movq	112(%r12), %rax
	testq	%rax, %rax
	je	.L14
	addq	%r13, 8(%rax)
.L14:
	movq	120(%r12), %rax
	testq	%rax, %rax
	je	.L15
	addq	%r13, 8(%rax)
.L15:
	movq	248(%r12), %rax
	testq	%rax, %rax
	je	.L16
	addq	%r13, 8(%rax)
.L16:
	movq	464(%r12), %rax
	testq	%rax, %rax
	je	.L17
	addq	%r13, 8(%rax)
.L17:
	movq	672(%r12), %rax
	testq	%rax, %rax
	je	.L10
	addq	%r13, 8(%rax)
.L10:
	movq	224(%r12), %rsi
	testq	%rsi, %rsi
	je	.L19
	cmpq	$7, 8(%rsi)
	jne	.L230
.L19:
	movq	120(%r12), %rax
	testq	%rax, %rax
	je	.L20
	movq	136(%r12), %rdx
	cmpq	$24, 8(%rdx)
	jne	.L231
.L20:
	cmpq	$0, 296(%r12)
	jne	.L232
	cmpq	$0, 184(%r12)
	jne	.L233
	testq	%rax, %rax
	je	.L103
	movq	8(%rax), %rcx
	movq	128(%r12), %rax
	movq	392(%r12), %rdi
	movq	8(%rax), %rdx
	movq	%rcx, %rbx
	movq	%rcx, %rax
	testq	%rdi, %rdi
	leaq	(%rcx,%rdx), %r15
	je	.L23
	movq	8(%rdi), %rdi
	leaq	(%rdi,%rdi,2), %rdi
	leaq	(%rcx,%rdi,8), %rbx
.L23:
	testq	%rsi, %rsi
	je	.L24
	movq	248(%r12), %rdi
	movq	80(%r12), %rsi
	movq	8(%rdi), %r8
	movq	8(%rsi), %rsi
	movq	%rdx, %rdi
	addq	%rsi, %r8
	subq	%rsi, %rdi
	cmpq	%r15, %r8
	leaq	(%rsi,%rcx), %r15
	cmove	%rdi, %rdx
	addq	%rdx, %r15
.L24:
	cmpq	_dl_rtld_map@GOTPCREL(%rip), %r12
	movq	112(%r12), %rdx
	movq	8(%rdx), %r9
	je	.L26
	testq	%r13, %r13
	je	.L234
.L27:
	cmpq	%rax, %rbx
	jbe	.L26
	.p2align 4,,10
	.p2align 3
.L31:
	movl	8(%rax), %edx
	movq	(%rax), %rcx
	addq	%r13, %rcx
	cmpq	$38, %rdx
	je	.L30
	cmpq	$8, %rdx
	jne	.L235
.L30:
	movq	16(%rax), %rdx
	addq	$24, %rax
	addq	%r13, %rdx
	cmpq	%rbx, %rax
	movq	%rdx, (%rcx)
	jb	.L31
.L26:
	cmpq	$0, 464(%r12)
	je	.L236
.L32:
	cmpq	%rbx, %r15
	jbe	.L34
	xorl	%r14d, %r14d
	leaq	.L44(%rip), %r8
	xorl	%r10d, %r10d
	movq	%r14, %r11
	movq	%r12, %rcx
	movq	%r13, %r14
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L61:
	movq	8(%rbx), %rsi
	movl	%esi, %ebp
	cmpq	$37, %rbp
	je	.L237
	movq	(%rbx), %r12
	addq	%r14, %r12
	cmpq	$8, %rbp
	je	.L221
	cmpq	$38, %rbp
	je	.L221
	testq	%rbp, %rbp
	je	.L36
	movq	%rsi, %rax
	shrq	$32, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r9,%rax,8), %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L40
	movzwl	6(%r15), %edi
	movzbl	4(%r15), %edx
	movq	8(%r15), %rax
	andl	$15, %edx
	cmpw	$-15, %di
	je	.L41
	addq	(%rcx), %rax
	cmpb	$10, %dl
	je	.L238
.L40:
	cmpq	$37, %rbp
	ja	.L42
	movslq	(%r8,%rbp,4), %rdx
	addq	%r8, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L44:
	.long	.L42-.L44
	.long	.L43-.L44
	.long	.L45-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L46-.L44
	.long	.L43-.L44
	.long	.L43-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L48-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L49-.L44
	.long	.L50-.L44
	.long	.L51-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L52-.L44
	.long	.L53-.L44
	.long	.L42-.L44
	.long	.L42-.L44
	.long	.L54-.L44
	.long	.L55-.L44
	.text
	.p2align 4,,10
	.p2align 3
.L53:
	movq	16(%r15), %rax
.L43:
	addq	16(%rbx), %rax
	movq	%rax, (%r12)
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	ja	.L61
	testq	%r11, %r11
	movq	%r14, %r13
	movq	%rcx, %r12
	movq	%r11, %r14
	je	.L34
	cmpq	%r10, %r11
	ja	.L34
	movq	%r9, %rbp
	movq	%r10, %r15
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L62:
	addq	$24, %r14
	cmpq	%r15, %r14
	ja	.L34
.L66:
	movq	8(%r14), %rax
	cmpl	$37, %eax
	jne	.L62
	shrq	$32, %rax
	movq	(%r14), %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	0(%rbp,%rax,8), %rax
	addq	%r13, %rbx
	testq	%rax, %rax
	je	.L223
	movzwl	6(%rax), %ecx
	movzbl	4(%rax), %edx
	movq	8(%rax), %rsi
	andl	$15, %edx
	cmpw	$-15, %cx
	je	.L65
	cmpb	$10, %dl
	movq	(%r12), %rax
	je	.L239
.L64:
	addq	16(%r14), %rax
	addq	$24, %r14
	call	*%rax
	cmpq	%r15, %r14
	movq	%rax, (%rbx)
	jbe	.L66
	.p2align 4,,10
	.p2align 3
.L34:
	orb	$4, 796(%r12)
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movq	232(%r12), %rdx
	movl	$0, 24(%rax)
	testq	%rdx, %rdx
	je	.L1
	movq	%rax, 8(%rdx)
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movq	16(%r15), %rax
.L48:
	addq	16(%rbx), %rax
	movl	$4294967295, %edi
	cmpq	%rdi, %rax
	movl	%eax, (%r12)
	jbe	.L36
	leaq	.LC0(%rip), %rdi
.L59:
	movq	104(%rcx), %rax
	movl	(%r15), %edx
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	addq	8(%rax), %rdx
	movq	_dl_argv(%rip), %rax
	movq	%rcx, (%rsp)
	movq	(%rax), %rsi
	leaq	.LC2(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L237:
	testq	%r11, %r11
	movq	%rbx, %r10
	jne	.L36
	movq	%rbx, %r11
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L221:
	movq	16(%rbx), %rax
	addq	(%rcx), %rax
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r10, %rsi
	subq	%rax, %rsi
	cmpq	$11, %rsi
	jbe	.L240
	movq	%rbx, %rsi
	subq	%rax, %rsi
	cmpq	$10, %rsi
	ja	.L7
	movq	%rbp, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L54:
	testq	%r15, %r15
	je	.L241
	movq	1112(%rcx), %rdx
	leaq	1(%rdx), %rax
	cmpq	$1, %rax
	jbe	.L242
.L57:
	movq	16(%rbx), %rax
	addq	8(%r15), %rax
	subq	%rdx, %rax
	movq	%rax, 8(%r12)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L51:
	testq	%r15, %r15
	je	.L36
	movq	1112(%rcx), %rdx
	leaq	1(%rdx), %rax
	cmpq	$1, %rax
	jbe	.L243
.L58:
	movq	16(%rbx), %rax
	addq	8(%r15), %rax
	subq	%rdx, %rax
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L50:
	testq	%r15, %r15
	je	.L36
	movq	16(%rbx), %rax
	addq	8(%r15), %rax
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L49:
	movq	1120(%rcx), %rax
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L55:
	movq	16(%rbx), %rax
	addq	(%rcx), %rax
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%rcx, (%rsp)
	call	*%rax
	movq	%rax, (%r12)
.L222:
	movq	(%rsp), %rcx
	movq	8(%rsp), %r8
	movq	16(%rsp), %r9
	movq	24(%rsp), %r11
	movq	32(%rsp), %r10
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L46:
	testq	%r15, %r15
	je	.L36
	movq	16(%r15), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%rcx, 8(%rsp)
	movq	%r8, (%rsp)
	call	memcpy@PLT
	movq	(%rsp), %r8
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r9
	movq	24(%rsp), %r11
	movq	32(%rsp), %r10
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L45:
	movq	16(%rbx), %rsi
	subq	%r12, %rsi
	addq	%rsi, %rax
	movslq	%eax, %rdx
	movl	%eax, (%r12)
	cmpq	%rdx, %rax
	je	.L36
	leaq	.LC1(%rip), %rdi
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L240:
	movq	%r11, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L234:
	cmpq	$0, 576(%r12)
	je	.L27
	cmpq	$0, 464(%r12)
	jne	.L32
.L236:
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	cmpq	%rbx, %r15
	leaq	.L76(%rip), %r8
	jbe	.L34
	movq	%r14, %r11
	movq	%r12, %rcx
	movq	%r13, %r14
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L33:
	movq	8(%rbx), %r15
	movl	%r15d, %ebp
	cmpq	$37, %rbp
	je	.L244
	movq	(%rbx), %r12
	addq	%r14, %r12
	cmpq	$8, %rbp
	je	.L224
	cmpq	$38, %rbp
	je	.L224
	testq	%rbp, %rbp
	je	.L68
	movq	%r15, %rax
	shrq	$32, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r9,%rax,8), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L72
	movzwl	6(%rdx), %edi
	movzbl	4(%rdx), %esi
	movq	8(%rdx), %rax
	andl	$15, %esi
	cmpw	$-15, %di
	je	.L73
	addq	(%rcx), %rax
	cmpb	$10, %sil
	je	.L245
.L72:
	cmpq	$37, %rbp
	ja	.L74
	movslq	(%r8,%rbp,4), %rsi
	addq	%r8, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L76:
	.long	.L74-.L76
	.long	.L75-.L76
	.long	.L77-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L78-.L76
	.long	.L75-.L76
	.long	.L75-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L80-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L81-.L76
	.long	.L82-.L76
	.long	.L83-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L84-.L76
	.long	.L85-.L76
	.long	.L74-.L76
	.long	.L74-.L76
	.long	.L86-.L76
	.long	.L87-.L76
	.text
.L239:
	testw	%cx, %cx
	je	.L64
	addq	%rax, %rsi
.L100:
	call	*%rsi
	.p2align 4,,10
	.p2align 3
.L223:
	movq	(%r12), %rax
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L41:
	cmpb	$10, %dl
	jne	.L40
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rsi, 40(%rsp)
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%rcx, 8(%rsp)
	movq	%r8, (%rsp)
	call	*%rax
	movq	40(%rsp), %rsi
	movq	32(%rsp), %r10
	movq	24(%rsp), %r11
	movq	16(%rsp), %r9
	movq	8(%rsp), %rcx
	movq	(%rsp), %r8
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L238:
	testw	%di, %di
	jne	.L99
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L103:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L85:
	movq	16(%rdx), %rax
.L75:
	addq	16(%rbx), %rax
	movq	%rax, (%r12)
	.p2align 4,,10
	.p2align 3
.L68:
	addq	$24, %rbx
	cmpq	%rbx, %r13
	ja	.L33
	testq	%r11, %r11
	movq	%r14, %r13
	movq	%rcx, %r12
	movq	%r11, %r14
	je	.L34
	cmpq	%r10, %r11
	ja	.L34
	movq	%r9, %rbp
	movq	%r10, %r15
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$24, %r14
	cmpq	%r15, %r14
	ja	.L34
.L97:
	movq	8(%r14), %rax
	cmpl	$37, %eax
	jne	.L93
	shrq	$32, %rax
	movq	(%r14), %rbx
	leaq	(%rax,%rax,2), %rax
	leaq	0(%rbp,%rax,8), %rax
	addq	%r13, %rbx
	testq	%rax, %rax
	je	.L226
	movzwl	6(%rax), %ecx
	movzbl	4(%rax), %edx
	movq	8(%rax), %rsi
	andl	$15, %edx
	cmpw	$-15, %cx
	je	.L96
	cmpb	$10, %dl
	movq	(%r12), %rax
	je	.L246
.L95:
	addq	16(%r14), %rax
	call	*%rax
	movq	%rax, (%rbx)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L84:
	movq	16(%rdx), %rax
.L80:
	addq	16(%rbx), %rax
	movl	$4294967295, %edi
	cmpq	%rdi, %rax
	movl	%eax, (%r12)
	jbe	.L68
	leaq	.LC0(%rip), %rdi
.L91:
	movq	104(%rcx), %rax
	movl	(%rdx), %edx
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	addq	8(%rax), %rdx
	movq	_dl_argv(%rip), %rax
	movq	%rcx, (%rsp)
	movq	(%rax), %rsi
	leaq	.LC2(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L244:
	testq	%r11, %r11
	movq	%rbx, %r10
	jne	.L68
	movq	%rbx, %r11
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L224:
	movq	16(%rbx), %rax
	addq	(%rcx), %rax
	movq	%rax, (%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L86:
	testq	%rdx, %rdx
	je	.L247
	movq	1112(%rcx), %rsi
	leaq	1(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L248
.L89:
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	subq	%rsi, %rax
	movq	%rax, 8(%r12)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L83:
	testq	%rdx, %rdx
	je	.L68
	movq	1112(%rcx), %rsi
	leaq	1(%rsi), %rax
	cmpq	$1, %rax
	jbe	.L249
.L90:
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	subq	%rsi, %rax
	movq	%rax, (%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L82:
	testq	%rdx, %rdx
	je	.L68
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	movq	%rax, (%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L81:
	movq	1120(%rcx), %rax
	movq	%rax, (%r12)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L87:
	movq	16(%rbx), %rax
	addq	(%rcx), %rax
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%rcx, (%rsp)
	call	*%rax
	movq	%rax, (%r12)
.L225:
	movq	(%rsp), %rcx
	movq	8(%rsp), %r8
	movq	16(%rsp), %r9
	movq	24(%rsp), %r11
	movq	32(%rsp), %r10
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L78:
	testq	%rdx, %rdx
	je	.L68
	movq	16(%rdx), %rdx
	movq	%rax, %rsi
	movq	%r12, %rdi
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%rcx, 8(%rsp)
	movq	%r8, (%rsp)
	call	memcpy@PLT
	movq	(%rsp), %r8
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r9
	movq	24(%rsp), %r11
	movq	32(%rsp), %r10
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L77:
	movq	16(%rbx), %rsi
	subq	%r12, %rsi
	addq	%rsi, %rax
	movslq	%eax, %rsi
	movl	%eax, (%r12)
	cmpq	%rsi, %rax
	je	.L68
	leaq	.LC1(%rip), %rdi
	jmp	.L91
.L246:
	testw	%cx, %cx
	je	.L95
	addq	%rax, %rsi
.L102:
	call	*%rsi
	.p2align 4,,10
	.p2align 3
.L226:
	movq	(%r12), %rax
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L241:
	movq	16(%rbx), %rax
	movq	%rax, 8(%r12)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L73:
	cmpb	$10, %sil
	jne	.L72
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	movq	%r9, 24(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%rdx, (%rsp)
	call	*%rax
	movq	40(%rsp), %r10
	movq	32(%rsp), %r11
	movq	24(%rsp), %r9
	movq	16(%rsp), %rcx
	movq	8(%rsp), %r8
	movq	(%rsp), %rdx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L245:
	testw	%di, %di
	jne	.L101
	jmp	.L72
.L65:
	cmpb	$10, %dl
	jne	.L223
	jmp	.L100
.L42:
	xorl	%edx, %edx
	movq	%rcx, %rdi
	call	_dl_reloc_bad_type
	.p2align 4,,10
	.p2align 3
.L247:
	movq	16(%rbx), %rax
	movq	%rax, 8(%r12)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%r12)
	jmp	.L68
.L242:
	movq	%rcx, %rdi
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_dl_allocate_static_tls
	movq	(%rsp), %rcx
	movq	32(%rsp), %r10
	movq	24(%rsp), %r11
	movq	16(%rsp), %r9
	movq	8(%rsp), %r8
	movq	1112(%rcx), %rdx
	jmp	.L57
.L243:
	movq	%rcx, %rdi
	movq	%r10, 32(%rsp)
	movq	%r11, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_dl_allocate_static_tls
	movq	(%rsp), %rcx
	movq	32(%rsp), %r10
	movq	24(%rsp), %r11
	movq	16(%rsp), %r9
	movq	8(%rsp), %r8
	movq	1112(%rcx), %rdx
	jmp	.L58
.L96:
	cmpb	$10, %dl
	jne	.L226
	jmp	.L102
.L249:
	movq	%rcx, %rdi
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	movq	%r9, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_dl_allocate_static_tls
	movq	(%rsp), %rcx
	movq	40(%rsp), %r10
	movq	32(%rsp), %r11
	movq	24(%rsp), %r9
	movq	16(%rsp), %r8
	movq	8(%rsp), %rdx
	movq	1112(%rcx), %rsi
	jmp	.L90
.L248:
	movq	%rcx, %rdi
	movq	%r10, 40(%rsp)
	movq	%r11, 32(%rsp)
	movq	%r9, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_dl_allocate_static_tls
	movq	(%rsp), %rcx
	movq	40(%rsp), %r10
	movq	32(%rsp), %r11
	movq	24(%rsp), %r9
	movq	16(%rsp), %r8
	movq	8(%rsp), %rdx
	movq	1112(%rcx), %rsi
	jmp	.L89
.L235:
	leaq	__PRETTY_FUNCTION__.10198(%rip), %rcx
	leaq	.LC8(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$546, %edx
	call	__assert_fail
.L74:
	movq	%r15, %rsi
	xorl	%edx, %edx
	movq	%rcx, %rdi
	call	_dl_reloc_bad_type
.L230:
	leaq	__PRETTY_FUNCTION__.10256(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$118, %edx
	call	__assert_fail
.L232:
	leaq	__PRETTY_FUNCTION__.10256(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$141, %edx
	call	__assert_fail
.L231:
	leaq	__PRETTY_FUNCTION__.10256(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$126, %edx
	call	__assert_fail
.L233:
	leaq	__PRETTY_FUNCTION__.10256(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$142, %edx
	call	__assert_fail
	.size	_dl_relocate_static_pie, .-_dl_relocate_static_pie
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10198, @object
	.size	__PRETTY_FUNCTION__.10198, 26
__PRETTY_FUNCTION__.10198:
	.string	"elf_machine_rela_relative"
	.align 16
	.type	__PRETTY_FUNCTION__.10256, @object
	.size	__PRETTY_FUNCTION__.10256, 21
__PRETTY_FUNCTION__.10256:
	.string	"elf_get_dynamic_info"
	.weak	_dl_rtld_map
	.hidden	__assert_fail
	.hidden	_dl_allocate_static_tls
	.hidden	_dl_reloc_bad_type
	.hidden	_dl_tlsdesc_undefweak
	.hidden	_dl_tlsdesc_return
	.hidden	_dl_error_printf
	.hidden	_dl_argv
	.hidden	_dl_debug_initialize
	.hidden	_dl_rtld_map
	.hidden	_GLOBAL_OFFSET_TABLE_
	.hidden	_DYNAMIC
	.hidden	_dl_get_dl_main_map
