	.text
	.p2align 4,,15
	.globl	chroot_canon
	.type	chroot_canon, @function
chroot_canon:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	movq	%rsi, %r14
	subq	$200, %rsp
	call	strlen@PLT
	testq	%rax, %rax
	je	.L66
	leaq	4096(%rax), %rbx
	movq	%rax, %r13
	movq	%rbx, %rdi
	call	xmalloc@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	addq	%rax, %rbx
	movq	%rax, %r12
	call	__mempcpy@PLT
	cmpb	$47, -1(%rax)
	je	.L67
	movb	$47, (%rax)
.L5:
	movzbl	(%r14), %esi
	addq	$1, %rax
	movl	$0, -220(%rbp)
	movq	%rax, -232(%rbp)
	movq	%rax, %r15
	leaq	-192(%rbp), %rax
	movq	$0, -200(%rbp)
	movq	%rax, -208(%rbp)
	testb	%sil, %sil
	je	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	cmpb	$47, %sil
	movq	%r14, %r13
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %r14
	movzbl	(%r14), %esi
	cmpb	$47, %sil
	je	.L8
	testb	%sil, %sil
	movq	%r14, %r13
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	%r13, %r14
	je	.L32
	movq	%r14, %rdx
	subq	%r13, %rdx
	cmpq	$1, %rdx
	je	.L68
	cmpq	$2, %rdx
	jne	.L15
	cmpb	$46, %sil
	je	.L69
.L15:
	cmpb	$47, -1(%r15)
	je	.L17
	movb	$47, (%r15)
	addq	$1, %r15
.L17:
	leaq	(%r15,%rdx), %rax
	cmpq	%rax, %rbx
	ja	.L18
	subq	%r12, %rbx
	subq	%r12, %r15
	movq	%r12, %rdi
	leaq	1(%rdx,%rbx), %rax
	addq	$4096, %rbx
	cmpq	$4096, %rdx
	movq	%rdx, -216(%rbp)
	cmovge	%rax, %rbx
	movq	%rbx, %rsi
	call	xrealloc@PLT
	movq	-216(%rbp), %rdx
	movq	%rax, %r12
	addq	%rax, %rbx
	addq	%rax, %r15
.L18:
	movq	%r15, %rdi
	movq	%r13, %rsi
	call	__mempcpy@PLT
	movq	-208(%rbp), %rsi
	movb	$0, (%rax)
	movq	%r12, %rdi
	movq	%rax, %r15
	call	lstat64@PLT
	testl	%eax, %eax
	js	.L62
	movl	-168(%rbp), %eax
	andl	$61440, %eax
	cmpl	$40960, %eax
	je	.L70
.L14:
	movzbl	(%r14), %esi
	testb	%sil, %sil
	jne	.L6
.L32:
	cmpq	-232(%rbp), %r15
	jbe	.L7
	cmpb	$47, -1(%r15)
	je	.L71
.L7:
	movb	$0, (%r15)
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	cmpb	$47, %al
	je	.L9
.L10:
	addq	$1, %r14
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L72
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L68:
	cmpb	$46, %sil
	jne	.L15
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L70:
	subq	$4112, %rsp
	movq	sysconf_symloop_max.5361(%rip), %rax
	addl	$1, -220(%rbp)
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	testq	%rax, %rax
	je	.L73
.L24:
	testq	%rax, %rax
	movl	$40, %edx
	jle	.L25
	cmpl	$40, %eax
	cmovnb	%eax, %edx
.L25:
	cmpl	%edx, -220(%rbp)
	ja	.L74
	movl	$4095, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	readlink@PLT
	testq	%rax, %rax
	movq	%rax, %r9
	js	.L62
	cmpq	$0, -200(%rbp)
	movb	$0, 0(%r13,%rax)
	je	.L75
.L29:
	movq	%r14, %rdi
	movq	%r9, -216(%rbp)
	call	strlen@PLT
	movq	-216(%rbp), %r9
	movl	$4096, %edx
	subq	%r9, %rdx
	cmpq	%rax, %rdx
	jbe	.L76
	leaq	1(%rax), %rdx
	movq	-200(%rbp), %rax
	movq	%r14, %rsi
	movq	%r9, -216(%rbp)
	leaq	(%rax,%r9), %rdi
	call	memmove@PLT
	movq	-216(%rbp), %r9
	movq	-200(%rbp), %rdi
	movq	%r13, %rsi
	movq	%r9, %rdx
	call	memcpy@PLT
	cmpb	$47, 0(%r13)
	movq	%rax, %r14
	je	.L37
	cmpq	%r15, -232(%rbp)
	jnb	.L14
	.p2align 4,,10
	.p2align 3
.L31:
	subq	$1, %r15
	cmpb	$47, -1(%r15)
	je	.L14
	subq	$1, %r15
	cmpb	$47, -1(%r15)
	jne	.L31
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L69:
	cmpb	$46, 1(%r13)
	jne	.L15
	cmpq	%r15, -232(%rbp)
	jnb	.L14
	.p2align 4,,10
	.p2align 3
.L16:
	subq	$1, %r15
	cmpb	$47, -1(%r15)
	je	.L14
	subq	$1, %r15
	cmpb	$47, -1(%r15)
	jne	.L16
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$173, %edi
	call	__sysconf@PLT
	movq	%rax, sysconf_symloop_max.5361(%rip)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L37:
	movq	-232(%rbp), %r15
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L75:
	subq	$4112, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -200(%rbp)
	jmp	.L29
.L67:
	subq	$1, %rax
	jmp	.L5
.L62:
	cmpb	$0, (%r14)
	je	.L32
.L27:
	movq	%r12, %rdi
	xorl	%r12d, %r12d
	call	free@PLT
	jmp	.L1
.L71:
	subq	$1, %r15
	jmp	.L7
.L66:
	call	__errno_location@PLT
	xorl	%r12d, %r12d
	movl	$22, (%rax)
	jmp	.L1
.L74:
	call	__errno_location@PLT
	movl	$40, (%rax)
	jmp	.L27
.L76:
	call	__errno_location@PLT
	movl	$36, (%rax)
	jmp	.L27
	.size	chroot_canon, .-chroot_canon
	.local	sysconf_symloop_max.5361
	.comm	sysconf_symloop_max.5361,8,8
