	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-misc.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"pid >= 0 && sizeof (pid_t) <= 4"
	.section	.rodata.str1.1
.LC2:
	.string	"niov < NIOVMAX"
.LC3:
	.string	"! \"invalid format specifier\""
	.text
	.p2align 4,,15
	.type	_dl_debug_vdprintf, @function
_dl_debug_vdprintf:
	pushq	%rbp
	xorl	%r11d, %r11d
	movabsq	$-3689348814741910323, %r8
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	xorl	%r12d, %r12d
	pushq	%rbx
	movq	%rdx, %rbx
	movl	%esi, %r13d
	leaq	-1084(%rbp), %r14
	leaq	__GI__itoa_lower_digits(%rip), %r15
	subq	$1096, %rsp
	movzbl	(%rbx), %eax
	movl	%edi, -1124(%rbp)
	movq	%rcx, -1104(%rbp)
	testb	%al, %al
	je	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	$1, %r13d
	jne	.L3
	testl	%r11d, %r11d
	je	.L137
.L4:
	cmpl	$63, %r12d
	jg	.L138
	movslq	%r12d, %rax
	movl	$-1, %r13d
	addl	$1, %r12d
	salq	$4, %rax
	movq	$12, -1064(%rbp,%rax)
	movq	%r14, -1072(%rbp,%rax)
	movzbl	(%rbx), %eax
.L3:
	testb	%al, %al
	je	.L62
	cmpb	$37, %al
	je	.L62
	testl	%r13d, %r13d
	movq	%rbx, %rdx
	sete	%cl
	cmpb	$10, %al
	je	.L134
	.p2align 4,,10
	.p2align 3
.L106:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L12
	cmpb	$37, %al
	je	.L12
	cmpb	$10, %al
	jne	.L106
.L134:
	testb	%cl, %cl
	jne	.L106
.L12:
	cmpl	$63, %r12d
	jg	.L139
	movq	%rdx, %rsi
	movslq	%r12d, %rcx
	subq	%rbx, %rsi
	salq	$4, %rcx
	testq	%rsi, %rsi
	movq	%rsi, -1064(%rbp,%rcx)
	je	.L15
	movq	%rbx, -1072(%rbp,%rcx)
	addl	$1, %r12d
.L15:
	cmpb	$37, %al
	je	.L140
	cmpb	$10, %al
	je	.L141
.L53:
	movq	%rdx, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L58
.L57:
	leaq	-1072(%rbp), %rsi
	movslq	%r12d, %rdx
	movl	-1124(%rbp), %edi
	movl	$20, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/dl-writev.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movzbl	1(%rdx), %ecx
	cmpb	$48, %cl
	je	.L17
	cmpb	$42, %cl
	leaq	1(%rdx), %rax
	movl	$32, %r10d
	movq	$-1, %r9
	je	.L142
.L19:
	cmpb	$46, %cl
	movl	$-1, %edx
	je	.L143
.L22:
	cmpb	$108, %cl
	je	.L71
	cmpb	$90, %cl
	je	.L71
	cmpb	$115, %cl
	je	.L68
	jg	.L59
	cmpb	$37, %cl
	movq	%rax, %rbx
	jne	.L144
.L30:
	movslq	%r12d, %rax
	salq	$4, %rax
	movq	%rbx, -1072(%rbp,%rax)
	movq	$1, -1064(%rbp,%rax)
.L132:
	addl	$1, %r12d
.L48:
	leaq	1(%rbx), %rdx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L141:
	cmpq	%rdx, %rbx
	je	.L145
	leal	-1(%r12), %eax
	cltq
	salq	$4, %rax
	addq	$1, -1064(%rbp,%rax)
.L55:
	cmpb	$0, 1(%rdx)
	leaq	1(%rdx), %rbx
	je	.L57
	testl	%r11d, %r11d
	jne	.L4
.L137:
	movq	%r8, -1096(%rbp)
	call	__getpid
	testl	%eax, %eax
	movl	%eax, %r11d
	movq	-1096(%rbp), %r8
	js	.L146
	leaq	10(%r14), %rcx
	movslq	%eax, %rsi
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rsi, %rax
	subq	$1, %rcx
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movzbl	(%r15,%rsi), %eax
	movq	%rdx, %rsi
	movb	%al, (%rcx)
	jne	.L6
	cmpq	%r14, %rcx
	jbe	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	subq	$1, %rcx
	movb	$32, (%rcx)
	cmpq	%r14, %rcx
	jne	.L8
.L7:
	movl	$2362, %eax
	movw	%ax, -1074(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L71:
	movzbl	1(%rax), %ecx
	leaq	1(%rax), %rbx
	cmpb	$115, %cl
	je	.L28
	jg	.L29
	cmpb	$37, %cl
	je	.L30
	cmpb	$100, %cl
	jne	.L23
.L31:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %ecx
	cmpl	$47, %ecx
	ja	.L32
	movl	%ecx, %edx
	addq	16(%rdi), %rdx
	addl	$8, %ecx
	movl	%ecx, (%rdi)
.L33:
	movq	(%rdx), %rsi
	movzbl	1(%rax), %edx
	movq	%rbx, %rax
	cmpb	$100, %dl
	jne	.L35
	movq	%rsi, %rax
	shrq	$63, %rax
	movq	%rax, -1096(%rbp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L143:
	cmpb	$42, 1(%rax)
	je	.L147
.L23:
	leaq	__PRETTY_FUNCTION__.10207(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$243, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L17:
	movzbl	2(%rdx), %ecx
	leaq	2(%rdx), %rax
	movl	$48, %r10d
	movq	$-1, %r9
	cmpb	$42, %cl
	jne	.L19
.L142:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %ecx
	cmpl	$47, %ecx
	ja	.L20
	movl	%ecx, %edx
	addq	16(%rdi), %rdx
	addl	$8, %ecx
	movl	%ecx, (%rdi)
.L21:
	movzbl	1(%rax), %ecx
	movslq	(%rdx), %r9
	addq	$1, %rax
	jmp	.L19
.L68:
	movq	%rax, %rbx
.L28:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %eax
	cmpl	$47, %eax
	ja	.L49
	movl	%eax, %ecx
	addq	16(%rdi), %rcx
	addl	$8, %eax
	movl	%eax, (%rdi)
.L50:
	movq	(%rcx), %rdi
	movslq	%r12d, %rcx
	movl	%r11d, -1112(%rbp)
	salq	$4, %rcx
	movq	%r8, -1120(%rbp)
	movl	%edx, -1108(%rbp)
	movq	%rcx, -1096(%rbp)
	movq	%rdi, -1072(%rbp,%rcx)
	call	strlen
	movslq	-1108(%rbp), %rdx
	movq	-1096(%rbp), %rcx
	movq	-1120(%rbp), %r8
	movl	-1112(%rbp), %r11d
	cmpl	$-1, %edx
	je	.L148
	cmpq	%rax, %rdx
	cmova	%rax, %rdx
	movq	%rdx, -1064(%rbp,%rcx)
	jmp	.L132
.L62:
	movq	%rbx, %rdx
	jmp	.L12
.L145:
	movslq	%r12d, %rax
	addl	$1, %r12d
	salq	$4, %rax
	movq	%rdx, -1072(%rbp,%rax)
	movq	$1, -1064(%rbp,%rax)
	jmp	.L55
.L148:
	movq	%rax, -1064(%rbp,%rcx)
	jmp	.L132
.L29:
	cmpb	$117, %cl
	je	.L31
	cmpb	$120, %cl
	je	.L31
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L59:
	cmpb	$117, %cl
	je	.L36
	cmpb	$120, %cl
	jne	.L23
.L36:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %ecx
	cmpl	$47, %ecx
	ja	.L37
	movl	%ecx, %edx
	addl	$8, %ecx
	addq	16(%rdi), %rdx
	movl	%ecx, (%rdi)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L144:
	cmpb	$100, %cl
	je	.L36
	jmp	.L23
.L37:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, 8(%rdi)
.L38:
	movl	(%rdx), %esi
	movzbl	(%rax), %edx
	cmpb	$100, %dl
	movq	%rsi, %rcx
	je	.L149
.L35:
	subq	$48, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	addq	$25, %rdi
	cmpb	$120, %dl
	movq	%rdi, %rcx
	je	.L41
	movq	%rax, %rbx
	movb	$0, -1096(%rbp)
.L60:
	movq	%rdi, %rcx
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rsi, %rax
	subq	$1, %rcx
	mulq	%r8
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rsi
	testq	%rdx, %rdx
	movzbl	(%r15,%rsi), %eax
	movq	%rdx, %rsi
	movb	%al, (%rcx)
	jne	.L42
	cmpl	$-1, %r9d
	jne	.L131
.L45:
	cmpb	$0, -1096(%rbp)
	je	.L47
	movb	$45, -1(%rcx)
	subq	$1, %rcx
.L47:
	movslq	%r12d, %rax
	subq	%rcx, %rdi
	addl	$1, %r12d
	salq	$4, %rax
	movq	%rcx, -1072(%rbp,%rax)
	movq	%rdi, -1064(%rbp,%rax)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rsi, %rdx
	shrq	$4, %rsi
	subq	$1, %rcx
	andl	$15, %edx
	testq	%rsi, %rsi
	movzbl	(%r15,%rdx), %edx
	movb	%dl, (%rcx)
	jne	.L41
	cmpl	$-1, %r9d
	movq	%rax, %rbx
	je	.L47
	movq	%rdi, %rax
	movb	$0, -1096(%rbp)
	subq	%rcx, %rax
	cmpq	%r9, %rax
	jge	.L47
	.p2align 4,,10
	.p2align 3
.L46:
	subq	$1, %rcx
	movb	%r10b, (%rcx)
.L131:
	movq	%rdi, %rax
	subq	%rcx, %rax
	cmpq	%r9, %rax
	jl	.L46
	jmp	.L45
.L149:
	shrl	$31, %ecx
	movq	%rax, %rbx
	movl	%ecx, -1096(%rbp)
.L40:
	subq	$48, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	addq	$25, %rdi
	jmp	.L60
.L49:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rcx
	leaq	8(%rcx), %rax
	movq	%rax, 8(%rdi)
	jmp	.L50
.L139:
	leaq	__PRETTY_FUNCTION__.10207(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$120, %edx
	call	__GI___assert_fail
.L138:
	leaq	__PRETTY_FUNCTION__.10207(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$107, %edx
	call	__GI___assert_fail
.L20:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, 8(%rdi)
	jmp	.L21
.L147:
	movq	-1104(%rbp), %rdi
	movl	(%rdi), %ecx
	cmpl	$47, %ecx
	ja	.L24
	movl	%ecx, %edx
	addq	16(%rdi), %rdx
	addl	$8, %ecx
	movl	%ecx, (%rdi)
.L25:
	movzbl	2(%rax), %ecx
	movl	(%rdx), %edx
	addq	$2, %rax
	jmp	.L22
.L146:
	leaq	__PRETTY_FUNCTION__.10207(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$98, %edx
	call	__GI___assert_fail
.L24:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, 8(%rdi)
	jmp	.L25
.L32:
	movq	-1104(%rbp), %rdi
	movq	8(%rdi), %rdx
	leaq	8(%rdx), %rcx
	movq	%rcx, 8(%rdi)
	jmp	.L33
	.size	_dl_debug_vdprintf, .-_dl_debug_vdprintf
	.p2align 4,,15
	.globl	_dl_sysdep_read_whole_file
	.hidden	_dl_sysdep_read_whole_file
	.type	_dl_sysdep_read_whole_file, @function
_dl_sysdep_read_whole_file:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movl	$524288, %esi
	movl	%edx, %r13d
	movq	$-1, %rbp
	subq	$152, %rsp
	call	__GI___open64_nocancel
	testl	%eax, %eax
	js	.L150
	movq	%rsp, %rsi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L154
	movq	48(%rsp), %rsi
	testq	%rsi, %rsi
	movq	%rsi, (%r12)
	jne	.L161
.L154:
	movq	$-1, %rbp
.L153:
	movl	%ebx, %edi
	call	__GI___close_nocancel
.L150:
	addq	$152, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	movl	$2, %ecx
	movl	%r13d, %edx
	xorl	%edi, %edi
	call	__mmap
	movq	%rax, %rbp
	jmp	.L153
	.size	_dl_sysdep_read_whole_file, .-_dl_sysdep_read_whole_file
	.p2align 4,,15
	.globl	_dl_debug_printf
	.hidden	_dl_debug_printf
	.type	_dl_debug_printf, @function
_dl_debug_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L164
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L164:
	leaq	224(%rsp), %rax
	movq	%rdi, %rdx
	leaq	8(%rsp), %rcx
	movl	$1, %esi
	movl	64+_rtld_local_ro(%rip), %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_debug_printf, .-_dl_debug_printf
	.p2align 4,,15
	.globl	_dl_debug_printf_c
	.hidden	_dl_debug_printf_c
	.type	_dl_debug_printf_c, @function
_dl_debug_printf_c:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L168
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L168:
	leaq	224(%rsp), %rax
	movq	%rdi, %rdx
	leaq	8(%rsp), %rcx
	movl	$-1, %esi
	movl	64+_rtld_local_ro(%rip), %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_debug_printf_c, .-_dl_debug_printf_c
	.p2align 4,,15
	.globl	_dl_dprintf
	.hidden	_dl_dprintf
	.type	_dl_dprintf, @function
_dl_dprintf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L172
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L172:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_dprintf, .-_dl_dprintf
	.p2align 4,,15
	.globl	_dl_printf
	.hidden	_dl_printf
	.type	_dl_printf, @function
_dl_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L176
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L176:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movl	$1, %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_printf, .-_dl_printf
	.p2align 4,,15
	.globl	_dl_error_printf
	.hidden	_dl_error_printf
	.type	_dl_error_printf, @function
_dl_error_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L180
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L180:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	addq	$216, %rsp
	ret
	.size	_dl_error_printf, .-_dl_error_printf
	.p2align 4,,15
	.globl	__GI__dl_fatal_printf
	.hidden	__GI__dl_fatal_printf
	.type	__GI__dl_fatal_printf, @function
__GI__dl_fatal_printf:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L184
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L184:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%rdi, %rdx
	xorl	%esi, %esi
	movl	$2, %edi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	_dl_debug_vdprintf
	movl	$127, %edi
	call	__GI__exit
	.size	__GI__dl_fatal_printf, .-__GI__dl_fatal_printf
	.globl	_dl_fatal_printf
	.set	_dl_fatal_printf,__GI__dl_fatal_printf
	.p2align 4,,15
	.globl	_dl_name_match_p
	.hidden	_dl_name_match_p
	.type	_dl_name_match_p, @function
_dl_name_match_p:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	8(%rsi), %rsi
	call	strcmp
	testl	%eax, %eax
	movl	$1, %edx
	je	.L186
	movq	56(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L188
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%edx, %edx
.L186:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L190
.L188:
	movq	(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L195
	addq	$8, %rsp
	movl	$1, %edx
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_dl_name_match_p, .-_dl_name_match_p
	.p2align 4,,15
	.globl	_dl_higher_prime_number
	.hidden	_dl_higher_prime_number
	.type	_dl_higher_prime_number, @function
_dl_higher_prime_number:
	leaq	primes.10274(%rip), %rcx
	leaq	120(%rcx), %r8
.L197:
	movq	%r8, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%rcx,%rax,4), %rsi
	movl	(%rsi), %eax
	cmpq	%rax, %rdi
	jbe	.L201
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%rsi, %rax
	subq	%rcx, %rax
	sarq	$2, %rax
	movq	%rax, %rdx
	shrq	$63, %rax
	addq	%rdx, %rax
	sarq	%rax
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %edx
	cmpq	%rdi, %rdx
	jb	.L202
	movq	%rax, %rsi
.L201:
	cmpq	%rsi, %rcx
	jne	.L199
.L200:
	movl	(%rcx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rsi, %r8
.L198:
	leaq	4(%rax), %rcx
	cmpq	%r8, %rcx
	jne	.L197
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L204:
	movq	%rsi, %rax
	jmp	.L198
	.size	_dl_higher_prime_number, .-_dl_higher_prime_number
	.p2align 4,,15
	.globl	_dl_strtoul
	.hidden	_dl_strtoul
	.type	_dl_strtoul, @function
_dl_strtoul:
	movzbl	(%rdi), %eax
	pushq	%rbx
	cmpb	$32, %al
	jne	.L236
	.p2align 4,,10
	.p2align 3
.L230:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	cmpb	$32, %al
	je	.L230
.L236:
	cmpb	$9, %al
	je	.L230
	cmpb	$45, %al
	je	.L238
	cmpb	$43, %al
	movl	$1, %ebx
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %rdi
.L210:
	movzbl	(%rdi), %eax
	leal	-48(%rax), %edx
	cmpb	$9, %dl
	jbe	.L211
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	je	.L205
.L237:
	movq	%rdi, (%rsi)
.L205:
	movq	%r8, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	cmpb	$48, %al
	movl	$10, %r10d
	movl	$9, %r11d
	je	.L239
.L213:
	xorl	%r8d, %r8d
	addl	$48, %r11d
	movslq	%r10d, %r9
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L241:
	movsbl	%al, %ecx
	cmpl	%r11d, %ecx
	ja	.L214
	subl	$48, %ecx
.L215:
	movslq	%ecx, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	notq	%rax
	divq	%r9
	cmpq	%r8, %rax
	jbe	.L240
	imulq	%r9, %r8
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	addq	%rcx, %r8
.L219:
	cmpb	$47, %al
	jg	.L241
.L214:
	cmpl	$16, %r10d
	jne	.L216
	leal	-97(%rax), %edx
	cmpb	$5, %dl
	ja	.L217
	movsbl	%al, %ecx
	subl	$87, %ecx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L217:
	leal	-65(%rax), %edx
	cmpb	$5, %dl
	ja	.L216
	movsbl	%al, %ecx
	subl	$55, %ecx
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L238:
	addq	$1, %rdi
	xorl	%ebx, %ebx
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L216:
	testq	%rsi, %rsi
	je	.L220
	movq	%rdi, (%rsi)
.L220:
	movq	%r8, %rax
	negq	%rax
	testb	%bl, %bl
	cmove	%rax, %r8
	movq	%r8, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L240:
	testq	%rsi, %rsi
	movq	$-1, %r8
	jne	.L237
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L239:
	movzbl	1(%rdi), %edx
	andl	$-33, %edx
	cmpb	$88, %dl
	jne	.L224
	movzbl	2(%rdi), %eax
	movl	$16, %r10d
	addq	$2, %rdi
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$8, %r10d
	movl	$7, %r11d
	jmp	.L213
	.size	_dl_strtoul, .-_dl_strtoul
	.section	.rodata
	.align 32
	.type	primes.10274, @object
	.size	primes.10274, 120
primes.10274:
	.long	7
	.long	13
	.long	31
	.long	61
	.long	127
	.long	251
	.long	509
	.long	1021
	.long	2039
	.long	4093
	.long	8191
	.long	16381
	.long	32749
	.long	65521
	.long	131071
	.long	262139
	.long	524287
	.long	1048573
	.long	2097143
	.long	4194301
	.long	8388593
	.long	16777213
	.long	33554393
	.long	67108859
	.long	134217689
	.long	268435399
	.long	536870909
	.long	1073741789
	.long	2147483647
	.long	-5
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10207, @object
	.size	__PRETTY_FUNCTION__.10207, 19
__PRETTY_FUNCTION__.10207:
	.string	"_dl_debug_vdprintf"
	.hidden	strcmp
	.hidden	_rtld_local_ro
	.hidden	__mmap
	.hidden	strlen
	.hidden	__getpid
