	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-open.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"new_nlist < ns->_ns_global_scope_alloc"
	.align 8
.LC2:
	.string	"\nadd %s [%lu] to global scope\n"
	.align 8
.LC3:
	.string	"added <= ns->_ns_global_scope_pending_adds"
	.text
	.p2align 4,,15
	.type	add_to_global_update, @function
add_to_global_update:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	_dl_ns(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	48(%rdi), %r15
	leaq	(%r15,%r15,8), %rax
	leaq	(%r15,%rax,2), %r14
	leaq	0(%r13,%r14,8), %rax
	movq	16(%rax), %rdx
	movl	8(%rdx), %r12d
	movl	712(%rdi), %edx
	testl	%edx, %edx
	je	.L2
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L5:
	movq	704(%rbp), %rdx
	movl	%ebx, %eax
	movq	(%rdx,%rax,8), %rcx
	movzbl	796(%rcx), %eax
	testb	$16, %al
	jne	.L3
	orl	$16, %eax
	movb	%al, 796(%rcx)
	leaq	0(%r13,%r14,8), %rax
	cmpl	%r12d, 24(%rax)
	jbe	.L13
	movq	16(%rax), %rax
	movl	%r12d, %edx
	addl	$1, %r12d
	testb	$2, 1+_dl_debug_mask(%rip)
	movq	(%rax), %rax
	movq	%rcx, (%rax,%rdx,8)
	jne	.L14
.L3:
	addl	$1, %ebx
	cmpl	%ebx, 712(%rbp)
	ja	.L5
	leaq	(%r15,%r15,8), %rax
	movl	%r12d, %esi
	leaq	(%r15,%rax,2), %rax
	leaq	0(%r13,%rax,8), %rax
	movq	16(%rax), %rdx
	movl	28(%rax), %eax
	subl	8(%rdx), %esi
	cmpl	%esi, %eax
	movl	%esi, %edx
	jb	.L15
.L6:
	leaq	(%r15,%r15,8), %rcx
	subl	%edx, %eax
	leaq	(%r15,%rcx,2), %rcx
	leaq	0(%r13,%rcx,8), %rcx
	movl	%eax, 28(%rcx)
	movq	16(%rcx), %rax
	movl	%r12d, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	48(%rcx), %rdx
	movq	8(%rcx), %rsi
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L3
.L2:
	movl	28(%rax), %eax
	jmp	.L6
.L13:
	leaq	__PRETTY_FUNCTION__.11125(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$183, %edx
	call	__assert_fail
.L15:
	leaq	__PRETTY_FUNCTION__.11125(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$197, %edx
	call	__assert_fail
	.size	add_to_global_update, .-add_to_global_update
	.p2align 4,,15
	.type	call_dl_init, @function
call_dl_init:
	movq	24(%rdi), %rcx
	movq	16(%rdi), %rdx
	movl	8(%rdi), %esi
	movq	(%rdi), %rdi
	jmp	_dl_init
	.size	call_dl_init, .-call_dl_init
	.section	.rodata.str1.1
.LC4:
	.string	"cannot extend global scope"
	.section	.text.unlikely,"ax",@progbits
	.type	add_to_global_resize_failure.isra.2, @function
add_to_global_resize_failure.isra.2:
	subq	$8, %rsp
	movq	(%rdi), %rsi
	leaq	.LC4(%rip), %rcx
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error
	.size	add_to_global_resize_failure.isra.2, .-add_to_global_resize_failure.isra.2
	.text
	.p2align 4,,15
	.type	add_to_global_resize, @function
add_to_global_resize:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$24, %rsp
	movl	712(%rdi), %eax
	movq	48(%rdi), %rbx
	testl	%eax, %eax
	je	.L20
	movq	704(%rdi), %rdx
	subl	$1, %eax
	leaq	8(%rdx), %rcx
	leaq	(%rcx,%rax,8), %rsi
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$8, %rcx
.L22:
	movq	(%rdx), %rdx
	movzbl	796(%rdx), %edx
	andl	$16, %edx
	cmpb	$1, %dl
	movq	%rcx, %rdx
	adcl	$0, %eax
	cmpq	%rsi, %rcx
	jne	.L46
.L20:
	leaq	(%rbx,%rbx,8), %rdx
	leaq	_dl_ns(%rip), %r12
	leaq	(%rbx,%rdx,2), %rcx
	xorl	%edx, %edx
	addl	28(%r12,%rcx,8), %eax
	leaq	(%rbx,%rbx,8), %rcx
	leaq	(%rbx,%rcx,2), %rcx
	setc	%dl
	leaq	(%r12,%rcx,8), %rcx
	testl	%edx, %edx
	movl	%eax, 28(%rcx)
	jne	.L45
	movq	16(%rcx), %r13
	movl	24(%rcx), %ecx
	movl	%eax, %eax
	movl	8(%r13), %r14d
	addq	%r14, %rax
	testl	%ecx, %ecx
	jne	.L26
	addq	$8, %rax
	movl	%eax, %edx
	movl	%eax, %r15d
	cmpq	%rdx, %rax
	jne	.L45
	testl	%r15d, %r15d
	movq	$0, 8(%rsp)
	jne	.L47
.L19:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	%rcx, %rax
	jbe	.L19
	addq	%rax, %rax
	movl	%eax, %edx
	movl	%eax, %r15d
	cmpq	%rdx, %rax
	jne	.L45
	movq	0(%r13), %rax
	testl	%r15d, %r15d
	movq	%rax, 8(%rsp)
	je	.L19
.L47:
	movl	%r15d, %edi
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L45
	movq	0(%r13), %rsi
	leaq	0(,%r14,8), %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rax, %r8
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rax
	movl	%r15d, 24(%r12,%rax,8)
	movq	%r8, 0(%r13)
#APP
# 158 "dl-open.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L48
.L33:
	movq	8(%rsp), %rdi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free@PLT
.L48:
	call	__thread_gscope_wait
	jmp	.L33
.L45:
	movq	56(%rbp), %rdi
	call	add_to_global_resize_failure.isra.2
	.size	add_to_global_resize, .-add_to_global_resize
	.section	.rodata.str1.1
.LC5:
	.string	"ns == l->l_ns"
	.text
	.p2align 4,,15
	.globl	_dl_find_dso_for_object
	.type	_dl_find_dso_for_object, @function
_dl_find_dso_for_object:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$0, _dl_nns(%rip)
	je	.L50
	movq	_dl_ns(%rip), %rbx
	movq	%rdi, %rbp
	testq	%rbx, %rbx
	je	.L50
	.p2align 4,,10
	.p2align 3
.L57:
	cmpq	%rbp, 856(%rbx)
	ja	.L52
	cmpq	%rbp, 864(%rbx)
	jbe	.L52
	testb	$64, 797(%rbx)
	jne	.L56
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_dl_addr_inside_object
	testl	%eax, %eax
	jne	.L56
.L52:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L57
.L50:
	xorl	%ebx, %ebx
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	cmpq	$0, 48(%rbx)
	jne	.L64
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
.L64:
	leaq	__PRETTY_FUNCTION__.11135(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$219, %edx
	call	__assert_fail
	.size	_dl_find_dso_for_object, .-_dl_find_dso_for_object
	.section	.rodata.str1.1
.LC6:
	.string	"invalid mode for dlopen()"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"no more namespaces available for dlmopen()"
	.align 8
.LC8:
	.string	"invalid target namespace in dlmopen()"
	.align 8
.LC9:
	.string	"_dl_debug_initialize (0, args.nsid)->r_state == RT_CONSISTENT"
	.text
	.p2align 4,,15
	.globl	_dl_open
	.hidden	_dl_open
	.type	_dl_open, @function
_dl_open:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	testb	$3, %sil
	je	.L96
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movl	%esi, %ebp
	movq	%rdx, %r15
	movq	%rcx, %rbx
	movl	%r8d, %r14d
	movq	%r9, %r13
	je	.L67
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L67:
	cmpq	$-1, %rbx
	je	.L97
	leaq	2(%rbx), %rax
	testq	$-3, %rax
	jne	.L98
	movq	176(%rsp), %rax
	leaq	dl_open_worker(%rip), %rsi
	movq	%r12, 32(%rsp)
	leaq	32(%rsp), %rdx
	movq	%rsp, %r12
	movq	%r13, 88(%rsp)
	movq	%r12, %rdi
	movl	%ebp, 40(%rsp)
	movq	%r15, 48(%rsp)
	movq	$0, 56(%rsp)
	movq	%rbx, 64(%rsp)
	movl	%r14d, 80(%rsp)
	movq	%rax, 96(%rsp)
	call	_dl_catch_exception
	movl	%eax, %r13d
	call	_dl_unload_cache
	movq	64(%rsp), %rsi
	testq	%rsi, %rsi
	js	.L71
	leaq	(%rsi,%rsi,8), %rax
	movl	72(%rsp), %ecx
	leaq	(%rsi,%rax,2), %rdx
	leaq	_dl_ns(%rip), %rax
	movl	%ecx, 28(%rax,%rdx,8)
.L71:
	cmpq	$0, 8(%rsp)
	jne	.L99
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L100
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L79
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L79:
	movq	56(%rsp), %rax
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L69
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L69:
	leaq	.LC7(%rip), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$22, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L98:
	leaq	.LC8(%rip), %rcx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$22, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L99:
	cmpb	$0, 76(%rsp)
	je	.L101
.L73:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L74
	andl	$134217728, %ebp
	je	.L102
.L75:
	movl	$1, %esi
	call	_dl_close_worker
	movq	64(%rsp), %rsi
.L74:
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	24(%rax), %edx
	testl	%edx, %edx
	jne	.L103
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L77
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L77:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	_dl_signal_exception
	.p2align 4,,10
	.p2align 3
.L101:
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rdx
	leaq	_dl_ns(%rip), %rax
	movq	$0, 32(%rax,%rdx,8)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L102:
	movb	$1, _dl_tls_dtv_gaps(%rip)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L103:
	leaq	__PRETTY_FUNCTION__.11275(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$907, %edx
	call	__assert_fail
.L96:
	leaq	.LC6(%rip), %rcx
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movl	$22, %edi
	call	_dl_signal_error
.L100:
	leaq	__PRETTY_FUNCTION__.11275(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$916, %edx
	call	__assert_fail
	.size	_dl_open, .-_dl_open
	.section	.rodata.str1.1
.LC10:
	.string	"<main program>"
.LC11:
	.string	"<program name unknown>"
.LC12:
	.string	"object=%s [%lu]\n"
.LC13:
	.string	" scope %u:"
.LC14:
	.string	" %s"
.LC15:
	.string	"\n"
.LC16:
	.string	" no scope\n"
	.text
	.p2align 4,,15
	.globl	_dl_show_scope
	.hidden	_dl_show_scope
	.type	_dl_show_scope, @function
_dl_show_scope:
	pushq	%r14
	pushq	%r13
	movl	%esi, %r13d
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r14
	pushq	%rbx
	movq	8(%rdi), %rsi
	movq	48(%rdi), %rdx
	cmpb	$0, (%rsi)
	jne	.L105
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC10(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L105:
	leaq	.LC12(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	920(%r14), %rax
	testq	%rax, %rax
	je	.L106
	movslq	%r13d, %rdx
	leaq	.LC11(%rip), %r12
	cmpq	$0, (%rax,%rdx,8)
	leaq	0(,%rdx,8), %rbp
	je	.L108
	.p2align 4,,10
	.p2align 3
.L107:
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	movl	%r13d, %esi
	call	_dl_debug_printf
	movq	920(%r14), %rax
	movq	(%rax,%rbp), %rdx
	movl	8(%rdx), %eax
	testl	%eax, %eax
	je	.L109
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%rdx), %rdx
	movl	%ebx, %eax
	movq	(%rdx,%rax,8), %rax
	movq	8(%rax), %rsi
	cmpb	$0, (%rsi)
	jne	.L119
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	cmove	%r12, %rsi
.L119:
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	addl	$1, %ebx
	call	_dl_debug_printf_c
	movq	920(%r14), %rax
	movq	(%rax,%rbp), %rdx
	cmpl	%ebx, 8(%rdx)
	ja	.L113
.L109:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	addq	$8, %rbp
	addl	$1, %r13d
	call	_dl_debug_printf_c
	movq	920(%r14), %rax
	cmpq	$0, (%rax,%rbp)
	jne	.L107
.L108:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	jmp	_dl_debug_printf
.L106:
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L108
	.size	_dl_show_scope, .-_dl_show_scope
	.section	.rodata.str1.1
.LC17:
	.string	"mode & RTLD_NOLOAD"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"opening file=%s [%lu]; direct_opencount=%u\n\n"
	.section	.rodata.str1.1
.LC19:
	.string	"marking %s [%lu] as NODELETE\n"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"_dl_debug_initialize (0, args->nsid)->r_state == RT_CONSISTENT"
	.align 8
.LC21:
	.string	"CPU ISA level is lower than required"
	.section	.rodata.str1.1
.LC22:
	.string	"dlopen"
.LC23:
	.string	"cannot create scope list"
	.section	.rodata.str1.8
	.align 8
.LC24:
	.string	"activating NODELETE for %s [%lu]\n"
	.section	.rodata.str1.1
.LC25:
	.string	"cnt + 1 < imap->l_scope_max"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"TLS generation counter wrapped!  Please report this."
	.section	.rodata.str1.1
.LC27:
	.string	"imap->l_need_tls_init == 0"
	.text
	.p2align 4,,15
	.type	dl_open_worker, @function
dl_open_worker:
	pushq	%r15
	pushq	%r14
	movl	$36, %esi
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	(%rdi), %r15
	movl	8(%rdi), %r13d
	movq	%r15, %rdi
	call	strchr
	testq	%rax, %rax
	je	.L265
.L121:
	movq	16(%r12), %rdi
	movq	_dl_ns(%rip), %rbx
	call	_dl_find_dso_for_object
	testq	%rax, %rax
	movq	%rax, %rbp
	cmove	%rbx, %rbp
	movq	32(%r12), %rbx
	cmpq	$-2, %rbx
	jne	.L122
	movq	48(%rbp), %rbx
	movq	%rbx, 32(%r12)
.L122:
	leaq	(%rbx,%rbx,8), %rax
	leaq	_dl_ns(%rip), %r14
	movq	%rbx, %rsi
	leaq	(%rbx,%rax,2), %rax
	leaq	(%r14,%rax,8), %rax
	cmpq	$0, 32(%rax)
	movl	28(%rax), %eax
	movl	%eax, 40(%r12)
	setne	44(%r12)
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movq	32(%r12), %r9
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	orl	$268435456, %r8d
	movl	$2, %edx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	_dl_map_object
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, 24(%r12)
	je	.L266
	testl	$1073741824, %r13d
	jne	.L120
	movl	792(%rax), %eax
	movl	%r13d, %ebp
	andl	$4096, %ebp
	cmpq	$0, 704(%rbx)
	leal	1(%rax), %ecx
	movl	%ecx, 792(%rbx)
	jne	.L267
	testl	%ebp, %ebp
	jne	.L268
.L138:
	movl	%r13d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	andl	$-2013265912, %r8d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_map_object_deps
	movl	712(%rbx), %edi
	testl	%edi, %edi
	je	.L139
	xorl	%ebp, %ebp
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L140:
	addl	$1, %ebp
	cmpl	%ebp, 712(%rbx)
	jbe	.L139
.L141:
	movq	704(%rbx), %rdx
	movl	%ebp, %eax
	movq	(%rdx,%rax,8), %rax
	movq	40(%rax), %rdi
	cmpq	$0, 744(%rdi)
	jne	.L140
	xorl	%edx, %edx
	xorl	%esi, %esi
	addl	$1, %ebp
	call	_dl_check_map_versions
	cmpl	%ebp, 712(%rbx)
	ja	.L141
	.p2align 4,,10
	.p2align 3
.L139:
	movq	32(%r12), %rsi
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	$0, 24(%rax)
	call	_dl_debug_state@PLT
	movl	712(%rbx), %eax
	movl	280+_dl_x86_cpu_features(%rip), %esi
	leal	-1(%rax), %ebp
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L144:
	movq	976(%rbx), %rdx
	movl	%ebp, %eax
	movq	(%rdx,%rax,8), %rax
	testb	$8, 796(%rax)
	jne	.L143
	movl	808(%rax), %edx
	movl	%edx, %ecx
	andl	%esi, %ecx
	cmpl	%ecx, %edx
	jne	.L269
.L143:
	subl	$1, %ebp
.L142:
	cmpl	$-1, %ebp
	jne	.L144
	testb	$2, 1+_dl_debug_mask(%rip)
	jne	.L270
.L145:
	movl	_dl_lazy(%rip), %esi
	movl	%r13d, %eax
	andl	$134217729, %eax
	movl	%eax, (%rsp)
	testl	%esi, %esi
	je	.L271
.L147:
	movq	976(%rbx), %rdi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L150:
	movq	40(%rcx), %rcx
	leal	1(%rsi), %eax
	testb	$4, 796(%rcx)
	jne	.L148
	cmpl	$-1, %ebp
	movl	%eax, %edx
	cmove	%esi, %ebp
.L148:
	movl	%eax, %ecx
	movl	%eax, %esi
	movq	(%rdi,%rcx,8), %rcx
	testq	%rcx, %rcx
	jne	.L150
	cmpl	%edx, %ebp
	leal	-1(%rdx), %r15d
	jnb	.L272
	.p2align 4,,10
	.p2align 3
.L153:
	movq	976(%rbx), %rdx
	movl	%r15d, %eax
	movq	(%rdx,%rax,8), %rdi
	movq	40(%rdi), %rax
	testb	$4, 796(%rax)
	jne	.L152
	movq	920(%rdi), %rsi
	movl	(%rsp), %edx
	xorl	%ecx, %ecx
	call	_dl_relocate_object
.L152:
	movl	%r15d, %edx
	cmpl	%edx, %ebp
	leal	-1(%rdx), %r15d
	jb	.L153
.L272:
	movl	712(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L199
	xorl	%r15d, %r15d
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L155:
	movl	712(%rbx), %eax
	addl	$1, %r15d
	cmpl	%eax, %r15d
	jnb	.L273
.L163:
	movq	704(%rbx), %rdx
	movl	%r15d, %eax
	movq	(%rdx,%rax,8), %rbp
	movzbl	796(%rbp), %eax
	andl	$11, %eax
	cmpb	$10, %al
	jne	.L155
	movq	920(%rbp), %rsi
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L200
	leaq	704(%rbx), %rdi
	cmpq	%rdi, %rax
	je	.L155
	leaq	8(%rsi), %rax
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$8, %rax
	cmpq	%rdi, %rdx
	je	.L155
.L157:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L158
	leaq	1(%rdx), %rax
	cmpq	$0, (%rsi,%rax,8)
	je	.L274
	.p2align 4,,10
	.p2align 3
.L201:
	movq	%rax, %rdx
	leaq	1(%rdx), %rax
	cmpq	$0, (%rsi,%rax,8)
	jne	.L201
.L274:
	addq	$2, %rdx
.L156:
	movq	912(%rbp), %rdi
	cmpq	%rdx, %rdi
	ja	.L155
	leaq	880(%rbp), %rax
	cmpq	$3, %rdi
	movq	%rax, 8(%rsp)
	ja	.L208
	cmpq	%rax, %rsi
	movq	%rax, %r9
	movq	$4, (%rsp)
	je	.L208
.L160:
	salq	$3, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
	movq	920(%rbp), %rdi
	cmpq	%rdi, 8(%rsp)
	movq	%rax, 920(%rbp)
	je	.L162
	call	_dl_scope_free
.L162:
	movq	(%rsp), %rax
	movq	%rax, 912(%rbp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L171:
	testb	%r15b, %r15b
	jne	.L275
.L181:
	cmpb	$0, 44(%r12)
	je	.L276
.L191:
	movl	48(%r12), %eax
	leaq	32(%rsp), %rdx
	leaq	call_dl_init(%rip), %rsi
	xorl	%edi, %edi
	movq	%rbx, 32(%rsp)
	movl	%eax, 40(%rsp)
	movq	56(%r12), %rax
	movq	%rax, 48(%rsp)
	movq	64(%r12), %rax
	movq	%rax, 56(%rsp)
	call	_dl_catch_exception
	movl	(%rsp), %eax
	testl	%eax, %eax
	jne	.L277
.L192:
	testb	$64, _dl_debug_mask(%rip)
	je	.L120
	movl	792(%rbx), %ecx
	movq	48(%rbx), %rdx
	leaq	.LC18(%rip), %rdi
	movq	8(%rbx), %rsi
	xorl	%eax, %eax
	call	_dl_debug_printf
.L120:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	movq	32(%r12), %rbx
	cmpq	$-2, %rbx
	je	.L121
	movl	$47, %esi
	movq	%r15, %rdi
	xorl	%ebp, %ebp
	call	strchr
	testq	%rax, %rax
	jne	.L122
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	je	.L154
	xorl	%ebp, %ebp
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L164:
	addl	$1, %ebp
	cmpl	712(%rbx), %ebp
	jnb	.L154
.L165:
	movq	704(%rbx), %rdx
	movl	%ebp, %eax
	movq	(%rdx,%rax,8), %rdi
	testb	$8, 796(%rdi)
	jne	.L164
	cmpq	$0, 1088(%rdi)
	je	.L164
	xorl	%esi, %esi
	addl	$1, %ebp
	movl	$1, %r15d
	call	_dl_add_to_slotinfo
	cmpl	712(%rbx), %ebp
	jb	.L165
	.p2align 4,,10
	.p2align 3
.L154:
	andl	$256, %r13d
	movl	%r13d, (%rsp)
	jne	.L278
.L166:
	movq	48(%rbx), %rax
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	(%r14,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.L167
	leaq	.LC24(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L170:
	cmpb	$0, 800(%rbp)
	je	.L168
	testb	$64, _dl_debug_mask(%rip)
	jne	.L279
.L169:
	movb	$1, 799(%rbp)
	movb	$0, 800(%rbp)
.L168:
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.L170
.L167:
	movl	712(%rbx), %ecx
	testl	%ecx, %ecx
	movl	%ecx, %r13d
	je	.L171
	xorl	%ebp, %ebp
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L172:
	testb	$2, 1+_dl_debug_mask(%rip)
	jne	.L259
.L263:
	movl	712(%rbx), %ecx
.L176:
	addl	$1, %ebp
	movl	%ecx, %r13d
	cmpl	%ecx, %ebp
	jnb	.L171
.L180:
	movq	704(%rbx), %rdx
	movl	%ebp, %eax
	xorl	%esi, %esi
	movq	(%rdx,%rax,8), %rdi
	movzbl	796(%rdi), %eax
	andl	$11, %eax
	cmpb	$10, %al
	jne	.L172
	movq	920(%rdi), %r8
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L205
	leaq	704(%rbx), %rsi
	cmpq	%rsi, %rax
	je	.L176
	leaq	8(%r8), %rax
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L177:
	addq	$8, %rax
	cmpq	%rdx, %rsi
	je	.L176
.L175:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L177
	xorl	%eax, %eax
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%rsi, %rax
.L178:
	leaq	1(%rax), %rsi
	cmpq	$0, (%r8,%rsi,8)
	leaq	0(,%rsi,8), %rdx
	jne	.L206
	addq	$2, %rax
.L173:
	cmpq	%rax, 912(%rdi)
	jbe	.L280
	movq	$0, (%r8,%rax,8)
	testb	$2, 1+_dl_debug_mask(%rip)
	movq	920(%rdi), %rax
	leaq	704(%rbx), %rcx
	movq	%rcx, (%rax,%rdx)
	je	.L263
	.p2align 4,,10
	.p2align 3
.L259:
	call	_dl_show_scope
	jmp	.L263
	.p2align 4,,10
	.p2align 3
.L266:
	andl	$4, %r13d
	jne	.L120
	leaq	__PRETTY_FUNCTION__.11235(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$533, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L269:
	movq	8(%rax), %rsi
	leaq	.LC21(%rip), %rcx
	leaq	.LC22(%rip), %rdx
	xorl	%edi, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L271:
	movl	%r13d, %eax
	andl	$134217728, %eax
	movl	%eax, (%rsp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L278:
	movq	%rbx, %rdi
	call	add_to_global_resize
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L275:
	movl	712(%rbx), %edx
	testl	%edx, %edx
	je	.L182
	xorl	%ebp, %ebp
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L264:
	movl	712(%rbx), %edx
.L184:
	addl	$1, %ebp
	cmpl	%edx, %ebp
	jnb	.L182
.L187:
	movq	704(%rbx), %rcx
	movl	%ebp, %edx
	movq	(%rcx,%rdx,8), %r15
	testb	$8, 796(%r15)
	jne	.L264
	cmpq	$0, 1088(%r15)
	je	.L264
	movl	$1, %esi
	movq	%r15, %rdi
	call	_dl_add_to_slotinfo
	testb	$4, 797(%r15)
	movl	712(%rbx), %edx
	je	.L184
	cmpl	%r13d, %edx
	cmove	%ebp, %r13d
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L182:
	addq	$1, _dl_tls_generation(%rip)
	je	.L188
	cmpl	%r13d, 712(%rbx)
	ja	.L189
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L190:
	addl	$1, %r13d
	cmpl	712(%rbx), %r13d
	jnb	.L181
.L189:
	movq	704(%rbx), %rcx
	movl	%r13d, %edx
	movq	(%rcx,%rdx,8), %rbp
	movzwl	796(%rbp), %edx
	andw	$1032, %dx
	cmpw	$1024, %dx
	jne	.L190
	cmpq	$0, 1088(%rbp)
	je	.L190
	andb	$-5, 797(%rbp)
	movq	%rbp, %rdi
	call	*_dl_init_static_tls(%rip)
	testb	$4, 797(%rbp)
	je	.L190
	leaq	__PRETTY_FUNCTION__.11204(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$430, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L277:
	movq	%rbx, %rdi
	call	add_to_global_update
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L276:
	movq	32(%r12), %rax
	xorl	%esi, %esi
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,2), %rax
	movq	32(%r14,%rax,8), %rdi
	call	_dl_call_libc_early_init
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L279:
	movq	48(%rbp), %rdx
	movq	8(%rbp), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L205:
	movl	$1, %eax
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L200:
	movl	$1, %edx
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L267:
	testb	$64, _dl_debug_mask(%rip)
	jne	.L281
.L129:
	andl	$256, %r13d
	je	.L130
	testb	$16, 796(%rbx)
	je	.L282
	testl	%ebp, %ebp
	je	.L137
.L132:
	testb	$64, _dl_debug_mask(%rip)
	jne	.L194
	movb	$1, 799(%rbx)
.L136:
	movzbl	796(%rbx), %eax
	andl	$16, %eax
	testb	%al, %al
	jne	.L137
	movq	%rbx, %rdi
	call	add_to_global_update
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L268:
	movb	$1, 800(%rbx)
	jmp	.L138
.L130:
	testl	%ebp, %ebp
	jne	.L283
.L137:
	movq	32(%r12), %rsi
	xorl	%edi, %edi
	call	_dl_debug_initialize
	movl	24(%rax), %r8d
	testl	%r8d, %r8d
	je	.L120
	leaq	__PRETTY_FUNCTION__.11235(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$573, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L270:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_show_scope
	jmp	.L145
.L208:
	leaq	(%rdi,%rdi), %rax
	salq	$4, %rdi
	movq	%rdx, 24(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rax, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r9
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdx
	jne	.L160
	leaq	.LC23(%rip), %rcx
	leaq	.LC22(%rip), %rsi
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L199:
	xorl	%r15d, %r15d
	jmp	.L154
.L188:
	leaq	.LC26(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
.L282:
	movq	%rbx, %rdi
	call	add_to_global_resize
	testl	%ebp, %ebp
	je	.L136
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L281:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L129
.L283:
	testb	$64, _dl_debug_mask(%rip)
	jne	.L194
	movb	$1, 799(%rbx)
	jmp	.L137
.L194:
	cmpb	$0, 799(%rbx)
	je	.L284
.L135:
	testl	%r13d, %r13d
	movb	$1, 799(%rbx)
	je	.L137
	jmp	.L136
.L284:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L135
.L280:
	leaq	__PRETTY_FUNCTION__.11180(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$334, %edx
	call	__assert_fail
	.size	dl_open_worker, .-dl_open_worker
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11125, @object
	.size	__PRETTY_FUNCTION__.11125, 21
__PRETTY_FUNCTION__.11125:
	.string	"add_to_global_update"
	.align 16
	.type	__PRETTY_FUNCTION__.11204, @object
	.size	__PRETTY_FUNCTION__.11204, 20
__PRETTY_FUNCTION__.11204:
	.string	"update_tls_slotinfo"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11180, @object
	.size	__PRETTY_FUNCTION__.11180, 14
__PRETTY_FUNCTION__.11180:
	.string	"update_scopes"
	.align 8
	.type	__PRETTY_FUNCTION__.11235, @object
	.size	__PRETTY_FUNCTION__.11235, 15
__PRETTY_FUNCTION__.11235:
	.string	"dl_open_worker"
	.align 8
	.type	__PRETTY_FUNCTION__.11275, @object
	.size	__PRETTY_FUNCTION__.11275, 9
__PRETTY_FUNCTION__.11275:
	.string	"_dl_open"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.11135, @object
	.size	__PRETTY_FUNCTION__.11135, 24
__PRETTY_FUNCTION__.11135:
	.string	"_dl_find_dso_for_object"
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	_dl_call_libc_early_init
	.hidden	_dl_add_to_slotinfo
	.hidden	_dl_scope_free
	.hidden	_dl_relocate_object
	.hidden	_dl_check_map_versions
	.hidden	_dl_map_object_deps
	.hidden	_dl_map_object
	.hidden	strchr
	.hidden	_dl_debug_printf_c
	.hidden	_dl_signal_exception
	.hidden	_dl_close_worker
	.hidden	_dl_debug_initialize
	.hidden	_dl_unload_cache
	.hidden	_dl_catch_exception
	.hidden	_dl_addr_inside_object
	.hidden	__thread_gscope_wait
	.hidden	_dl_signal_error
	.hidden	_dl_init
	.hidden	__assert_fail
	.hidden	_dl_debug_printf
