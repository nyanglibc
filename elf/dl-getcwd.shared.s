	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"."
.LC1:
	.string	"/"
.LC2:
	.string	".."
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../sysdeps/unix/sysv/linux/getcwd.c"
	.align 8
.LC4:
	.string	"errno != ERANGE || buf != NULL || size != 0"
	.text
	.p2align 4,,15
	.globl	__getcwd
	.hidden	__getcwd
	.type	__getcwd, @function
__getcwd:
	pushq	%r15
	pushq	%r14
	movl	$79, %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	subq	$248, %rsp
	movq	%rsi, 16(%rsp)
#APP
# 81 "../sysdeps/unix/sysv/linux/getcwd.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L88
	cmpl	$0, %eax
	jle	.L4
	cmpb	$47, (%rdi)
	movq	%rdi, %r15
	je	.L1
.L8:
	cmpq	$0, 16(%rsp)
	jne	.L89
	testq	%r14, %r14
	movq	$4096, 24(%rsp)
	jne	.L90
.L10:
	movq	24(%rsp), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1
.L12:
	movq	24(%rsp), %rax
	leaq	96(%rsp), %rsi
	leaq	.LC0(%rip), %rdi
	movq	%rsi, (%rsp)
	leaq	(%r15,%rax), %rbx
	leaq	-1(%rbx), %rax
	movb	$0, -1(%rbx)
	movq	%rax, 56(%rsp)
	call	__GI___lstat64
	testl	%eax, %eax
	js	.L36
	movq	96(%rsp), %rax
	movq	(%rsp), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rax, 8(%rsp)
	movq	104(%rsp), %rax
	movq	%rax, %rbp
	call	__GI___lstat64
	testl	%eax, %eax
	js	.L36
	movq	96(%rsp), %rax
	cmpq	%rax, 8(%rsp)
	movq	%rbp, %rdi
	movq	104(%rsp), %rcx
	movq	%rax, 64(%rsp)
	setne	%dl
	cmpq	%rcx, %rbp
	movq	%rcx, 72(%rsp)
	setne	%al
	orb	%al, %dl
	movb	%dl, 55(%rsp)
	je	.L16
	movq	%r15, 32(%rsp)
	xorl	%ebp, %ebp
	movl	$-100, %r13d
	movq	%rdi, %r15
	movq	%r14, 40(%rsp)
.L70:
	leaq	.LC2(%rip), %rsi
	xorl	%edx, %edx
	movl	%r13d, %edi
	xorl	%eax, %eax
	call	__openat64@PLT
	testl	%eax, %eax
	movl	%eax, %r13d
	js	.L17
	movq	(%rsp), %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	jne	.L18
	testq	%rbp, %rbp
	je	.L22
	movq	%rbp, %rdi
	call	__closedir
	testl	%eax, %eax
	jne	.L83
.L22:
	movq	104(%rsp), %rax
	movl	%r13d, %edi
	movq	96(%rsp), %r14
	movq	%rax, 80(%rsp)
	call	__fdopendir
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L83
	movzbl	55(%rsp), %r12d
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rbp, %rdi
	movl	$0, rtld_errno(%rip)
	call	__readdir64@PLT
	testq	%rax, %rax
	je	.L91
	cmpb	$46, 19(%rax)
	je	.L43
.L27:
	testb	%r12b, %r12b
	je	.L29
	cmpq	%r15, (%rax)
	sete	%dl
	cmpq	8(%rsp), %r14
	setne	%cl
	orb	%cl, %dl
	je	.L21
	movl	%edx, %r12d
.L29:
	leaq	19(%rax), %rbx
	movq	(%rsp), %rdx
	movl	$256, %ecx
	movl	%r13d, %edi
	movq	%rbx, %rsi
	call	__GI___fstatat64
	testl	%eax, %eax
	jne	.L21
	movl	120(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L21
	movq	8(%rsp), %rax
	cmpq	%rax, 96(%rsp)
	jne	.L21
	cmpq	%r15, 104(%rsp)
	jne	.L21
	movq	56(%rsp), %r12
	subq	32(%rsp), %r12
	movq	%rbx, %rdi
	call	strlen
	movq	%rax, %rcx
	cmpq	%rax, %r12
	ja	.L30
	cmpq	$0, 16(%rsp)
	jne	.L92
	movq	24(%rsp), %rax
	movq	%rcx, 88(%rsp)
	cmpq	%rax, %rcx
	movq	%rax, %r15
	cmovnb	%rcx, %r15
	addq	%rax, %r15
	jc	.L34
	movq	%r15, %rsi
	movq	32(%rsp), %rdi
	call	*__rtld_realloc(%rip)
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L34
	movq	24(%rsp), %rax
	movq	%r12, %rdi
	leaq	(%r8,%r12), %rsi
	movq	%r8, 8(%rsp)
	subq	%rax, %rdi
	subq	%r12, %rax
	addq	%r15, %rdi
	movq	%rax, %rdx
	addq	%r8, %rdi
	call	memcpy@PLT
	movq	8(%rsp), %r8
	movq	88(%rsp), %rcx
	movq	%rax, 56(%rsp)
	movq	%r15, 24(%rsp)
	movq	%r8, 32(%rsp)
.L30:
	movq	56(%rsp), %r8
	movq	%rcx, %rdx
	movq	%rbx, %rsi
	subq	%rcx, %r8
	movq	%r8, %rdi
	call	memcpy@PLT
	cmpq	%r14, 64(%rsp)
	movq	80(%rsp), %rcx
	movq	%rax, %r8
	leaq	-1(%rax), %rax
	movb	$47, -1(%r8)
	movq	%r14, 8(%rsp)
	movq	%rax, 56(%rsp)
	movq	%rcx, %r15
	jne	.L70
	cmpq	%rcx, 72(%rsp)
	jne	.L70
	movq	%rbp, %rdi
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	call	__closedir
	testl	%eax, %eax
	jne	.L36
	movq	24(%rsp), %rax
	leaq	(%r15,%rax), %rbx
.L16:
	movq	24(%rsp), %rax
	leaq	-1(%r15,%rax), %rax
	cmpq	%rax, 56(%rsp)
	je	.L93
.L37:
	movq	56(%rsp), %rsi
	movq	%r15, %rdi
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	call	memmove
	cmpq	$0, 16(%rsp)
	jne	.L38
	cmpq	24(%rsp), %rbx
	jnb	.L1
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*__rtld_realloc(%rip)
	movq	%rax, %r14
.L38:
	testq	%r14, %r14
	cmovne	%r14, %r15
.L1:
	addq	$248, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	16(%rsp), %rax
	testq	%r14, %r14
	movq	%rax, 24(%rsp)
	je	.L10
	movq	%r14, %r15
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L91:
	movl	rtld_errno(%rip), %ebx
	testl	%ebx, %ebx
	jne	.L85
	testb	%r12b, %r12b
	je	.L85
	movq	%rbp, %rdi
	call	__rewinddir
	movq	%rbp, %rdi
	call	__readdir64@PLT
	testq	%rax, %rax
	je	.L94
	xorl	%r12d, %r12d
	cmpb	$46, 19(%rax)
	jne	.L29
.L43:
	cmpb	$0, 20(%rax)
	je	.L21
	cmpw	$46, 20(%rax)
	jne	.L27
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L85:
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
.L24:
	testl	%ebx, %ebx
	je	.L95
.L26:
	movq	%rbp, %rdi
	call	__closedir
.L14:
	testq	%r14, %r14
	jne	.L40
	movq	%r15, %rdi
	call	*__rtld_free(%rip)
.L40:
	movl	%ebx, rtld_errno(%rip)
	xorl	%r15d, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	je	.L8
	movl	rtld_errno(%rip), %eax
.L3:
	cmpl	$36, %eax
	je	.L8
	cmpl	$34, %eax
	jne	.L48
	testq	%r14, %r14
	jne	.L48
	cmpq	$0, 16(%rsp)
	je	.L96
.L48:
	xorl	%r15d, %r15d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$2, rtld_errno(%rip)
	movl	$2, %ebx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L88:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L36:
	movl	rtld_errno(%rip), %ebx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$22, rtld_errno(%rip)
	xorl	%r15d, %r15d
	jmp	.L1
.L94:
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	movl	rtld_errno(%rip), %ebx
	jmp	.L24
.L83:
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	movl	rtld_errno(%rip), %ebx
.L39:
	movl	%r13d, %edi
	call	__GI___close_nocancel
	jmp	.L14
.L17:
	testq	%rbp, %rbp
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	movl	rtld_errno(%rip), %ebx
	je	.L14
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%rbp, %rbp
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	movl	rtld_errno(%rip), %ebx
	je	.L39
	movq	%rbp, %rdi
	call	__closedir
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L93:
	movq	56(%rsp), %rax
	movb	$47, -1(%rax)
	subq	$1, %rax
	movq	%rax, 56(%rsp)
	jmp	.L37
.L34:
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	movl	$12, %ebx
	movl	$12, rtld_errno(%rip)
	jmp	.L26
.L96:
	leaq	__PRETTY_FUNCTION__.8887(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$124, %edx
	call	__GI___assert_fail
.L92:
	movq	32(%rsp), %r15
	movq	40(%rsp), %r14
	movl	$34, %ebx
	movl	$34, rtld_errno(%rip)
	jmp	.L26
	.size	__getcwd, .-__getcwd
	.weak	getcwd
	.set	getcwd,__getcwd
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.8887, @object
	.size	__PRETTY_FUNCTION__.8887, 9
__PRETTY_FUNCTION__.8887:
	.string	"__getcwd"
	.hidden	__rtld_free
	.hidden	__rewinddir
	.hidden	memmove
	.hidden	__rtld_realloc
	.hidden	strlen
	.hidden	rtld_errno
	.hidden	__fdopendir
	.hidden	__closedir
	.hidden	__rtld_malloc
