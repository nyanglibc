	.text
	.p2align 4,,15
	.globl	__alloc_dir
	.hidden	__alloc_dir
	.type	__alloc_dir, @function
__alloc_dir:
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	subq	$16, %rsp
	testb	%sil, %sil
	je	.L13
	cmpq	$32768, 56(%rcx)
	movl	$32768, %ebx
	movl	$1048576, %eax
	cmovnb	56(%rcx), %rbx
	cmpq	$1048576, %rbx
	cmova	%rax, %rbx
	leaq	96(%rbx), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L14
.L6:
	movl	%r12d, 0(%rbp)
	movq	%rbx, 48(%rbp)
	movq	$0, 56(%rbp)
	movq	$0, 64(%rbp)
	movq	$0, 72(%rbp)
	movl	$0, 80(%rbp)
.L1:
	addq	$16, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	movl	$1, %edx
	movl	$2, %esi
	movq	%rcx, 8(%rsp)
	call	__GI___fcntl64_nocancel
	testl	%eax, %eax
	movq	8(%rsp), %rcx
	js	.L5
	cmpq	$32768, 56(%rcx)
	movl	$32768, %ebx
	movl	$1048576, %eax
	cmovnb	56(%rcx), %rbx
	cmpq	$1048576, %rbx
	cmova	%rax, %rbx
	leaq	96(%rbx), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L6
.L5:
	addq	$16, %rsp
	xorl	%ebp, %ebp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r12d, %edi
	call	__GI___close_nocancel
	jmp	.L1
	.size	__alloc_dir, .-__alloc_dir
	.p2align 4,,15
	.globl	__opendir
	.hidden	__opendir
	.type	__opendir, @function
__opendir:
	cmpb	$0, (%rdi)
	je	.L16
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movl	$591872, %esi
	subq	$152, %rsp
	call	__GI___open_nocancel
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L28
	movq	%rsp, %rbp
	movl	%eax, %edi
	movq	%rbp, %rsi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L20
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L29
	movq	%rbp, %rcx
	xorl	%edx, %edx
	movl	$1, %esi
	movl	%ebx, %edi
	call	__alloc_dir
.L15:
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$20, rtld_errno(%rip)
.L20:
	movl	%ebx, %edi
	call	__GI___close_nocancel
	addq	$152, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$2, rtld_errno(%rip)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	jmp	.L15
	.size	__opendir, .-__opendir
	.weak	opendir
	.set	opendir,__opendir
	.hidden	rtld_errno
	.hidden	__rtld_malloc
