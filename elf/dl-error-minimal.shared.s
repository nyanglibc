	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	": "
.LC1:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"error while loading shared libraries"
	.section	.rodata.str1.1
.LC3:
	.string	"<program name unknown>"
.LC4:
	.string	"%s: %s: %s%s%s%s%s\n"
	.text
	.p2align 4,,15
	.type	fatal_error, @function
fatal_error:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC1(%rip), %rax
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movq	%rcx, %r9
	subq	$1048, %rsp
	testl	%edi, %edi
	movq	%rax, %rdx
	jne	.L12
.L2:
	cmpb	$0, 0(%rbp)
	leaq	.LC1(%rip), %rcx
	leaq	.LC0(%rip), %r8
	leaq	.LC4(%rip), %rdi
	cmove	%rcx, %r8
	leaq	.LC2(%rip), %rcx
	testq	%rbx, %rbx
	cmove	%rcx, %rbx
	movq	__GI__dl_argv(%rip), %rcx
	movq	(%rcx), %rsi
	leaq	.LC3(%rip), %rcx
	pushq	%rax
	pushq	%rdx
	movq	%rbx, %rdx
	testq	%rsi, %rsi
	cmove	%rcx, %rsi
	movq	%rbp, %rcx
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
.L12:
	leaq	16(%rsp), %rsi
	movl	$1024, %edx
	movq	%rcx, 8(%rsp)
	call	__strerror_r
	leaq	.LC0(%rip), %rdx
	movq	8(%rsp), %r9
	jmp	.L2
	.size	fatal_error, .-fatal_error
	.p2align 4,,15
	.globl	_dl_signal_exception
	.type	_dl_signal_exception, @function
_dl_signal_exception:
	subq	$8, %rsp
	movq	catch_hook(%rip), %rax
	testq	%rax, %rax
	je	.L14
	movq	(%rax), %rdx
	movdqu	(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movq	16(%rsi), %rcx
	movl	$1, %esi
	movq	%rcx, 16(%rdx)
	movq	8(%rax), %rdx
	movl	%edi, (%rdx)
	leaq	16(%rax), %rdi
	call	__longjmp
.L14:
	movq	8(%rsi), %rcx
	movq	(%rsi), %rsi
	call	fatal_error
	.size	_dl_signal_exception, .-_dl_signal_exception
	.section	.rodata.str1.1
.LC5:
	.string	"DYNAMIC LINKER BUG!!!"
	.text
	.p2align 4,,15
	.globl	_dl_signal_error
	.type	_dl_signal_error, @function
_dl_signal_error:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC5(%rip), %rax
	movl	%edi, %ebp
	subq	$8, %rsp
	movq	catch_hook(%rip), %rbx
	testq	%rcx, %rcx
	cmove	%rax, %rcx
	testq	%rbx, %rbx
	je	.L18
	movq	(%rbx), %rdi
	movq	%rcx, %rdx
	call	__GI__dl_exception_create
	movq	8(%rbx), %rax
	leaq	16(%rbx), %rdi
	movl	$1, %esi
	movl	%ebp, (%rax)
	call	__longjmp
.L18:
	call	fatal_error
	.size	_dl_signal_error, .-_dl_signal_error
	.section	.rodata.str1.1
.LC6:
	.string	"continued"
.LC7:
	.string	"fatal"
.LC8:
	.string	"%s: error: %s: %s (%s)\n"
	.text
	.p2align 4,,15
	.globl	_dl_signal_cexception
	.hidden	_dl_signal_cexception
	.type	_dl_signal_cexception, @function
_dl_signal_cexception:
	testl	$-2177, _rtld_local_ro(%rip)
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movq	receiver(%rip), %rax
	movl	%edi, %ebp
	pushq	%rbx
	movq	%rsi, %rbx
	jne	.L29
.L21:
	testq	%rax, %rax
	je	.L23
	movq	8(%rbx), %rdx
	movq	(%rbx), %rsi
	movl	%ebp, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L29:
	testq	%rax, %rax
	movq	8(%rsi), %rcx
	leaq	.LC7(%rip), %rax
	leaq	.LC6(%rip), %r8
	movq	(%rsi), %rsi
	leaq	.LC8(%rip), %rdi
	cmove	%rax, %r8
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	receiver(%rip), %rax
	jmp	.L21
.L23:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	_dl_signal_exception@PLT
	.size	_dl_signal_cexception, .-_dl_signal_cexception
	.p2align 4,,15
	.globl	_dl_signal_cerror
	.hidden	_dl_signal_cerror
	.type	_dl_signal_cerror, @function
_dl_signal_cerror:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %r12d
	movq	%rsi, %rbx
	movq	%rcx, %rbp
	subq	$8, %rsp
	testl	$-2177, _rtld_local_ro(%rip)
	movq	receiver(%rip), %rax
	jne	.L39
.L31:
	testq	%rax, %rax
	je	.L33
	addq	$8, %rsp
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L39:
	testq	%rax, %rax
	leaq	.LC6(%rip), %r8
	leaq	.LC7(%rip), %rax
	leaq	.LC8(%rip), %rdi
	cmove	%rax, %r8
	xorl	%eax, %eax
	call	_dl_debug_printf
	movq	receiver(%rip), %rax
	jmp	.L31
.L33:
	movq	%rbp, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	_dl_signal_error@PLT
	.size	_dl_signal_cerror, .-_dl_signal_cerror
	.p2align 4,,15
	.globl	_dl_catch_exception
	.type	_dl_catch_exception, @function
_dl_catch_exception:
	pushq	%rbx
	subq	$288, %rsp
	movq	catch_hook(%rip), %rbx
	testq	%rdi, %rdi
	movq	%rdi, 8(%rsp)
	movq	%rsi, 24(%rsp)
	movq	%rdx, 32(%rsp)
	movq	%rbx, 16(%rsp)
	jne	.L41
	movq	$0, catch_hook(%rip)
	movq	%rdx, %rdi
	call	*%rsi
	movq	%rbx, catch_hook(%rip)
	xorl	%eax, %eax
.L40:
	addq	$288, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movq	8(%rsp), %rax
	leaq	64(%rsp), %rdi
	xorl	%esi, %esi
	movq	%rdi, catch_hook(%rip)
	leaq	80(%rsp), %rdi
	movq	%rax, 64(%rsp)
	leaq	60(%rsp), %rax
	movq	%rax, 72(%rsp)
	call	__sigsetjmp
	testl	%eax, %eax
	movl	%eax, 44(%rsp)
	jne	.L44
	movq	24(%rsp), %rcx
	movq	32(%rsp), %rdi
	call	*%rcx
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx
	pxor	%xmm0, %xmm0
	movq	$0, 16(%rax)
	movq	%rcx, catch_hook(%rip)
	movups	%xmm0, (%rax)
	movl	44(%rsp), %eax
	addq	$288, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	16(%rsp), %rax
	movq	%rax, catch_hook(%rip)
	movl	60(%rsp), %eax
	jmp	.L40
	.size	_dl_catch_exception, .-_dl_catch_exception
	.p2align 4,,15
	.globl	_dl_catch_error
	.type	_dl_catch_error, @function
_dl_catch_error:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	movq	%rcx, %rsi
	movq	%r8, %rdx
	subq	$32, %rsp
	movq	%rsp, %rdi
	call	_dl_catch_exception@PLT
	movq	(%rsp), %rdx
	movq	%rdx, (%r12)
	movq	8(%rsp), %rdx
	cmpq	%rdx, 16(%rsp)
	movq	%rdx, 0(%rbp)
	sete	(%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	_dl_catch_error, .-_dl_catch_error
	.p2align 4,,15
	.globl	_dl_receive_error
	.hidden	_dl_receive_error
	.type	_dl_receive_error, @function
_dl_receive_error:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	catch_hook(%rip), %rbp
	movq	receiver(%rip), %rbx
	movq	$0, catch_hook(%rip)
	movq	%rdi, receiver(%rip)
	movq	%rdx, %rdi
	call	*%rsi
	movq	%rbp, catch_hook(%rip)
	movq	%rbx, receiver(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	_dl_receive_error, .-_dl_receive_error
	.local	receiver
	.comm	receiver,8,8
	.local	catch_hook
	.comm	catch_hook,8,8
	.hidden	__sigsetjmp
	.hidden	_dl_debug_printf
	.hidden	_rtld_local_ro
	.hidden	__longjmp
	.hidden	__strerror_r
