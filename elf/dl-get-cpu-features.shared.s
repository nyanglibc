	.text
	.p2align 4,,15
	.type	get_common_cache_info, @function
get_common_cache_info:
	pushq	%r15
	pushq	%r14
	xorl	%r8d, %r8d
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	(%rdi), %r9
	movl	(%rsi), %ebp
	movl	$3, -20(%rsp)
	testq	%r9, %r9
	jg	.L2
	movq	%rdx, %r9
	movl	$2, -20(%rsp)
	movl	$-1, %r8d
.L2:
	testb	$16, 139+_rtld_local_ro(%rip)
	je	.L3
	movl	112+_rtld_local_ro(%rip), %ebx
	movl	108+_rtld_local_ro(%rip), %eax
	movl	104+_rtld_local_ro(%rip), %r15d
	movl	%ebx, -12(%rsp)
	movl	116+_rtld_local_ro(%rip), %ebx
	cmpl	$3, %eax
	movl	%eax, -8(%rsp)
	movl	%ebx, -4(%rsp)
	jg	.L91
	movb	$1, -13(%rsp)
	xorl	%r14d, %r14d
.L4:
	movzbl	130+_rtld_local_ro(%rip), %ebp
.L89:
	testl	%ebp, %ebp
	setne	%al
.L26:
	testq	%r9, %r9
	jle	.L28
	testb	%al, %al
	je	.L28
	movq	%r9, %rax
	movl	%ebp, %ecx
	cqto
	idivq	%rcx
	movq	%rax, %r9
.L28:
	cmpb	$0, -13(%rsp)
	jne	.L3
	testl	%r14d, %r14d
	je	.L29
	movq	%r12, %rax
	movslq	%r14d, %r14
	cqto
	idivq	%r14
	movq	%rax, %r12
.L29:
	addq	%r12, %r9
.L3:
	movq	%r9, (%rdi)
	movl	%ebp, (%rsi)
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	testl	%r8d, %r8d
	movl	$3, %r11d
	jne	.L92
.L5:
	movq	%r9, %r10
	xorl	%ecx, %ecx
	movb	$1, -13(%rsp)
	xorl	%r14d, %r14d
	movl	$4, %r13d
	movq	%rdi, %r9
	.p2align 4,,10
	.p2align 3
.L6:
	leal	1(%rcx), %edi
	movl	%r13d, %eax
#APP
# 538 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$1, %r15d
	je	.L93
.L8:
	movl	%eax, %ecx
	shrl	$5, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	je	.L9
	cmpl	$3, %ecx
	jne	.L7
	testb	$2, %r11b
	je	.L7
	shrl	$14, %eax
	shrl	%edx
	andl	$-3, %r11d
	andl	$1023, %eax
	movl	%eax, %r8d
	movl	%edx, %eax
	andl	$1, %eax
	testl	%r11d, %r11d
	movb	%al, -13(%rsp)
	je	.L94
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%edi, %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L93:
	testb	$31, %al
	jne	.L8
	movq	%r9, %rdi
	movq	%r10, %r9
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	testb	$1, %r11b
	je	.L7
	shrl	$14, %eax
	andl	$-2, %r11d
	andl	$1023, %eax
	testl	%r11d, %r11d
	movl	%eax, %r14d
	jne	.L7
.L94:
	cmpl	$6, -12(%rsp)
	movq	%r9, %rdi
	movq	%r10, %r9
	sete	-12(%rsp)
	movzbl	-12(%rsp), %ebx
	cmpl	$3, %r15d
	sete	%al
	testb	%al, %bl
	jne	.L12
	cmpl	$10, -8(%rsp)
	jle	.L12
	testl	%r14d, %r14d
	movl	$2, %eax
	setg	%dl
	xorl	%r11d, %r11d
	cmpl	$3, -20(%rsp)
	sete	%r11b
	andl	%edx, %r11d
	testl	%r8d, %r8d
	jg	.L13
	xorl	%eax, %eax
	cmpl	$2, -20(%rsp)
	sete	%al
	andl	%edx, %eax
	addl	%eax, %eax
.L13:
	orl	%eax, %r11d
	je	.L12
	xorl	%ecx, %ecx
	movl	$11, %eax
#APP
# 595 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	andl	$65280, %ecx
	movzbl	%bl, %ebx
	je	.L12
	testl	%ebx, %ebx
	je	.L12
	movl	$1, %edx
	movl	$11, %r13d
	movq	%r9, %r10
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$512, %ecx
	je	.L95
.L16:
	leal	1(%rdx), %r9d
	movl	%r13d, %eax
	movl	%edx, %ecx
#APP
# 595 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	andl	$65280, %ecx
	andl	$255, %ebx
	je	.L86
	testl	%ecx, %ecx
	je	.L86
	movl	%r9d, %edx
.L14:
	cmpl	$256, %ecx
	jne	.L15
	testb	$1, %r11b
	je	.L16
#APP
# 609 "../sysdeps/x86/dl-cacheinfo.h" 1
	bsr %r14d, %ecx
# 0 "" 2
#NO_APP
	movl	$-1, %r14d
	addl	$1, %ecx
	subl	$1, %ebx
	sall	%cl, %r14d
	andl	$-2, %r11d
	notl	%r14d
	andl	%ebx, %r14d
.L17:
	testl	%r11d, %r11d
	jne	.L16
.L86:
	movq	%r10, %r9
.L12:
	cmpl	$1, %r14d
	sbbl	$-1, %r14d
	testl	%r8d, %r8d
	jle	.L21
	cmpl	$2, -20(%rsp)
	jne	.L83
.L22:
	testl	%r14d, %r14d
	je	.L89
	cmpl	$1, %r15d
	movl	%r14d, %ebp
	movl	$1, %eax
	jne	.L26
	cmpl	$2, %r14d
	seta	%al
	andb	-12(%rsp), %al
	je	.L36
	movl	-4(%rsp), %ecx
	subl	$55, %ecx
	cmpl	$38, %ecx
	ja	.L26
	movl	$1, %edx
	movl	$2, %ebp
	salq	%cl, %rdx
	movabsq	$309242363905, %rcx
	testq	%rcx, %rdx
	cmove	%r14d, %ebp
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$1, %r11d
	movl	$-1, %r8d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L95:
	testb	$2, %r11b
	je	.L16
	subl	$1, %ebx
	cmpl	$2, -20(%rsp)
	je	.L96
#APP
# 626 "../sysdeps/x86/dl-cacheinfo.h" 1
	bsr %r8d, %ecx
# 0 "" 2
#NO_APP
	movl	$-1, %r8d
	addl	$1, %ecx
	sall	%cl, %r8d
	notl	%r8d
	andl	%ebx, %r8d
.L30:
	andl	$-3, %r11d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L21:
	cmpl	$2, -20(%rsp)
	je	.L22
	testl	%r8d, %r8d
	je	.L89
	orl	$-1, %ebp
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L83:
	leal	1(%r8), %ebp
.L36:
	movl	$1, %eax
	jmp	.L26
.L96:
#APP
# 626 "../sysdeps/x86/dl-cacheinfo.h" 1
	bsr %r14d, %ecx
# 0 "" 2
#NO_APP
	movl	$-1, %r14d
	addl	$1, %ecx
	sall	%cl, %r14d
	notl	%r14d
	andl	%ebx, %r14d
	jmp	.L30
	.size	get_common_cache_info, .-get_common_cache_info
	.p2align 4,,15
	.type	handle_zhaoxin, @function
handle_zhaoxin:
	subl	$185, %edi
	movl	$-1431655765, %edx
	xorl	%ecx, %ecx
	movl	%edi, %eax
	pushq	%rbx
	mull	%edx
	movl	$4, %eax
	shrl	%edx
	leal	(%rdx,%rdx,2), %r8d
#APP
# 443 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L108
	xorl	%esi, %esi
	movl	$4, %r9d
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L138:
	testb	%r10b, %r10b
	je	.L99
	cmpl	$3, %r8d
	je	.L100
.L101:
	cmpl	$3, %eax
	jne	.L103
	cmpl	$9, %r8d
	je	.L100
.L103:
	addl	$1, %esi
	movl	%r9d, %eax
	movl	%esi, %ecx
#APP
# 443 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L108
.L107:
	shrl	$5, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sete	%r10b
	cmpl	$1, %edx
	je	.L138
.L99:
	cmpl	$2, %edx
	jne	.L102
	testb	%r10b, %r10b
	je	.L102
	testl	%r8d, %r8d
	jne	.L103
.L100:
	movl	%edi, %eax
	movl	$-1431655765, %edx
	mull	%edx
	shrl	%edx
	leal	(%rdx,%rdx,2), %eax
	subl	%eax, %edi
	je	.L139
	movl	%ebx, %ecx
	shrl	$22, %ebx
	andl	$4095, %ecx
	leal	1(%rbx), %eax
	addq	$1, %rcx
	cmpl	$1, %edi
	cmovne	%rcx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L139:
	movl	%ebx, %eax
	movl	%ebx, %edx
	addl	$1, %ecx
	shrl	$22, %eax
	andl	$4095, %edx
	shrl	$12, %ebx
	addl	$1, %eax
	addl	$1, %edx
	andl	$1023, %ebx
	imull	%edx, %eax
	imull	%eax, %ecx
	leal	1(%rbx), %eax
	popq	%rbx
	imull	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%eax, %eax
	popq	%rbx
	ret
.L102:
	cmpl	$2, %eax
	jne	.L101
	cmpl	$6, %r8d
	je	.L100
	jmp	.L101
	.size	handle_zhaoxin, .-handle_zhaoxin
	.p2align 4,,15
	.type	handle_amd, @function
handle_amd:
	movl	$-2147483648, %esi
	pushq	%rbx
	movl	%esi, %eax
#APP
# 320 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	xorl	%edx, %edx
	cmpl	$190, %edi
	movl	%eax, %esi
	setg	%dl
	xorl	%eax, %eax
	subl	$2147483643, %edx
	cmpl	%esi, %edx
	ja	.L140
	movl	%edx, %eax
#APP
# 330 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$187, %edi
	jle	.L186
.L143:
	subl	$189, %edi
	cmpl	$7, %edi
	ja	.L144
	leaq	.L146(%rip), %rsi
	movslq	(%rsi,%rdi,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L146:
	.long	.L145-.L146
	.long	.L147-.L146
	.long	.L148-.L146
	.long	.L149-.L146
	.long	.L150-.L146
	.long	.L151-.L146
	.long	.L152-.L146
	.long	.L153-.L146
	.text
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%eax, %eax
	testb	$-16, %ch
	je	.L140
	movl	%ecx, %eax
	shrl	$6, %eax
	andl	$67107840, %eax
.L140:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L186:
	addl	$3, %edi
	movl	%edx, %ecx
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L151:
	xorl	%eax, %eax
	testb	$-16, %dh
	je	.L140
	leal	(%rdx,%rdx), %eax
	popq	%rbx
	andl	$2146959360, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L152:
	movl	%edx, %eax
	leaq	.L167(%rip), %rsi
	shrl	$12, %eax
	andl	$15, %eax
	movslq	(%rsi,%rax,4), %rcx
	addq	%rsi, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L167:
	.long	.L140-.L167
	.long	.L140-.L167
	.long	.L140-.L167
	.long	.L165-.L167
	.long	.L140-.L167
	.long	.L165-.L167
	.long	.L178-.L167
	.long	.L165-.L167
	.long	.L169-.L167
	.long	.L165-.L167
	.long	.L170-.L167
	.long	.L171-.L167
	.long	.L172-.L167
	.long	.L173-.L167
	.long	.L181-.L167
	.long	.L174-.L167
	.text
	.p2align 4,,10
	.p2align 3
.L145:
	movl	%ecx, %eax
	shrl	$16, %eax
	movzbl	%al, %edx
	sall	$2, %eax
	movl	%eax, %ecx
	movl	%edx, %eax
	andl	$261120, %ecx
	cmpl	$255, %edx
	cmove	%rcx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	movzbl	%cl, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	movl	%ecx, %eax
	leaq	.L157(%rip), %rsi
	shrl	$12, %eax
	movl	%eax, %edx
	andl	$15, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L157:
	.long	.L156-.L157
	.long	.L156-.L157
	.long	.L156-.L157
	.long	.L165-.L157
	.long	.L156-.L157
	.long	.L165-.L157
	.long	.L178-.L157
	.long	.L165-.L157
	.long	.L169-.L157
	.long	.L165-.L157
	.long	.L170-.L157
	.long	.L171-.L157
	.long	.L172-.L157
	.long	.L173-.L157
	.long	.L181-.L157
	.long	.L164-.L157
	.text
	.p2align 4,,10
	.p2align 3
.L150:
	movzbl	%cl, %edx
	movl	$0, %eax
	andb	$-16, %ch
	cmovne	%rdx, %rax
	popq	%rbx
	ret
.L178:
	movl	$8, %eax
	popq	%rbx
	ret
.L181:
	movl	$128, %eax
	popq	%rbx
	ret
.L173:
	movl	$96, %eax
	popq	%rbx
	ret
.L172:
	movl	$64, %eax
	popq	%rbx
	ret
.L171:
	movl	$48, %eax
	popq	%rbx
	ret
.L170:
	movl	$32, %eax
	popq	%rbx
	ret
.L169:
	movl	$16, %eax
	popq	%rbx
	ret
.L164:
	movl	%ecx, %edx
	movzbl	%cl, %ecx
	shrl	$6, %edx
	movl	%edx, %eax
	xorl	%edx, %edx
	andl	$67107840, %eax
	divl	%ecx
	popq	%rbx
	movl	%eax, %eax
	ret
.L156:
	movq	%rdx, %rax
	popq	%rbx
	ret
.L174:
	leal	(%rdx,%rdx), %eax
	movzbl	%dl, %ecx
	xorl	%edx, %edx
	popq	%rbx
	andl	$2146959360, %eax
	divl	%ecx
	movl	%eax, %eax
	ret
.L165:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	movl	%ecx, %eax
	shrl	$14, %eax
	andl	$261120, %eax
	popq	%rbx
	ret
.L153:
	movzbl	%dl, %eax
	andb	$-16, %dh
	movl	$0, %edx
	cmove	%rdx, %rax
	popq	%rbx
	ret
	.size	handle_amd, .-handle_amd
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../sysdeps/x86/dl-cacheinfo.h"
.LC1:
	.string	"offset == 2"
	.text
	.p2align 4,,15
	.type	intel_check_word.isra.0, @function
intel_check_word.isra.0:
	testl	%esi, %esi
	js	.L272
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	leal	-185(%rdi), %edx
	movl	$-1431655765, %r10d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %eax
	mull	%r10d
	subq	$8, %rsp
	shrl	%edx
	testl	%esi, %esi
	leal	(%rdx,%rdx,2), %ebp
	je	.L188
	leaq	intel_02_known(%rip), %r13
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L276:
	cmpl	$9, %ebp
	movb	$1, (%rcx)
	je	.L188
.L190:
	shrl	$8, %esi
	testl	%esi, %esi
	je	.L188
.L216:
	movzbl	%sil, %eax
	cmpl	$64, %eax
	je	.L276
	cmpl	$255, %eax
	je	.L277
	cmpl	$73, %eax
	jne	.L207
	cmpl	$9, %ebp
	jne	.L207
	cmpl	$15, (%r8)
	jne	.L217
	cmpl	$6, (%r9)
	jne	.L217
	subl	$3, %edi
	movl	$6, %ebp
.L207:
	movl	%esi, %ebx
	movl	$68, %r10d
	xorl	%edx, %edx
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%rax, %r10
.L211:
	cmpq	%rdx, %r10
	jbe	.L190
.L212:
	leaq	(%rdx,%r10), %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r11
	cmpb	(%r11), %bl
	je	.L278
	jb	.L218
	leaq	1(%rax), %rdx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L278:
	movzbl	3(%r11), %edx
	cmpl	%ebp, %edx
	je	.L279
	cmpb	$6, %dl
	jne	.L190
	shrl	$8, %esi
	movb	$1, (%r12)
	testl	%esi, %esi
	jne	.L216
	.p2align 4,,10
	.p2align 3
.L188:
	xorl	%eax, %eax
.L187:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$9, %ebp
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L272:
	xorl	%eax, %eax
	ret
.L277:
	xorl	%ecx, %ecx
	movl	$4, %eax
#APP
# 155 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L188
	xorl	%esi, %esi
	movl	$4, %r8d
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L280:
	testb	%r9b, %r9b
	je	.L192
	cmpl	$3, %ebp
	je	.L197
.L193:
	cmpl	$3, %eax
	jne	.L198
	cmpl	$9, %ebp
	je	.L197
.L198:
	cmpl	$4, %eax
	jne	.L202
	cmpl	$12, %ebp
	je	.L197
.L202:
	addl	$1, %esi
	movl	%r8d, %eax
	movl	%esi, %ecx
#APP
# 155 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L188
.L206:
	shrl	$5, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sete	%r9b
	cmpl	$1, %edx
	je	.L280
.L192:
	cmpl	$2, %edx
	jne	.L196
	testb	%r9b, %r9b
	je	.L196
	testl	%ebp, %ebp
	jne	.L198
.L197:
	subl	$185, %edi
	subl	%ebp, %edi
	je	.L281
	cmpl	$1, %edi
	je	.L282
	cmpl	$2, %edi
	jne	.L283
	andl	$4095, %ebx
	leaq	1(%rbx), %rax
	jmp	.L187
.L279:
	subl	$185, %edi
	subl	%ebp, %edi
	jne	.L213
	movl	4(%r11), %eax
	jmp	.L187
.L281:
	movl	%ebx, %eax
	movl	%ebx, %edx
	addl	$1, %ecx
	shrl	$22, %eax
	andl	$4095, %edx
	shrl	$12, %ebx
	addl	$1, %edx
	addl	$1, %eax
	imull	%edx, %eax
	movl	%ebx, %edx
	andl	$1023, %edx
	addl	$1, %edx
	imull	%ecx, %eax
	imull	%edx, %eax
	jmp	.L187
.L213:
	cmpl	$1, %edi
	je	.L284
	cmpl	$2, %edi
	jne	.L285
	movzbl	2(%r11), %eax
	jmp	.L187
.L282:
	movl	%ebx, %eax
	shrl	$22, %eax
	addl	$1, %eax
	jmp	.L187
.L284:
	movzbl	1(%r11), %eax
	jmp	.L187
.L196:
	cmpl	$2, %eax
	jne	.L193
	cmpl	$6, %ebp
	je	.L197
	jmp	.L193
.L283:
	leaq	__PRETTY_FUNCTION__.10001(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$183, %edx
	call	__GI___assert_fail
.L285:
	leaq	__PRETTY_FUNCTION__.10001(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$231, %edx
	call	__GI___assert_fail
	.size	intel_check_word.isra.0, .-intel_check_word.isra.0
	.p2align 4,,15
	.type	get_common_indices.constprop.2, @function
get_common_indices.constprop.2:
	pushq	%rbx
	movq	%rdx, %r8
	movq	%rcx, %r9
	movl	$1, %eax
#APP
# 324 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%edx, 136+_rtld_local_ro(%rip)
	movl	%eax, %edx
	movl	%ebx, 128+_rtld_local_ro(%rip)
	shrl	$8, %edx
	movl	%ecx, 132+_rtld_local_ro(%rip)
	movl	%eax, 124+_rtld_local_ro(%rip)
	andl	$15, %edx
	movl	%edx, (%rdi)
	movl	%eax, %edx
	shrl	$4, %edx
	andl	$15, %edx
	movl	%edx, (%rsi)
	movl	%eax, %edx
	shrl	$12, %edx
	andl	$240, %edx
	movl	%edx, (%r8)
	movl	%eax, %edx
	andl	$15, %edx
	movl	%edx, (%r9)
	cmpl	$15, (%rdi)
	je	.L291
.L287:
	cmpl	$6, 108+_rtld_local_ro(%rip)
	jle	.L286
	movl	$7, %r8d
	xorl	%esi, %esi
	movl	$1, %edi
	movl	%r8d, %eax
	movl	%esi, %ecx
#APP
# 342 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 156+_rtld_local_ro(%rip)
	movl	%ebx, 160+_rtld_local_ro(%rip)
	movl	%r8d, %eax
	movl	%ecx, 164+_rtld_local_ro(%rip)
	movl	%edx, 168+_rtld_local_ro(%rip)
	movl	%edi, %ecx
#APP
# 347 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$12, 108+_rtld_local_ro(%rip)
	movl	%eax, 316+_rtld_local_ro(%rip)
	movl	%ebx, 320+_rtld_local_ro(%rip)
	movl	%ecx, 324+_rtld_local_ro(%rip)
	movl	%edx, 328+_rtld_local_ro(%rip)
	jle	.L286
	movl	$13, %eax
	movl	%edi, %ecx
#APP
# 355 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$24, 108+_rtld_local_ro(%rip)
	movl	%eax, 220+_rtld_local_ro(%rip)
	movl	%ebx, 224+_rtld_local_ro(%rip)
	movl	%ecx, 228+_rtld_local_ro(%rip)
	movl	%edx, 232+_rtld_local_ro(%rip)
	jle	.L286
	movl	$25, %eax
	movl	%esi, %ecx
#APP
# 362 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 348+_rtld_local_ro(%rip)
	movl	%ebx, 352+_rtld_local_ro(%rip)
	movl	%ecx, 356+_rtld_local_ro(%rip)
	movl	%edx, 360+_rtld_local_ro(%rip)
.L286:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L291:
	shrl	$20, %eax
	movzbl	%al, %eax
	addl	$15, %eax
	movl	%eax, (%rdi)
	movl	(%r8), %eax
	addl	%eax, (%rsi)
	jmp	.L287
	.size	get_common_indices.constprop.2, .-get_common_indices.constprop.2
	.p2align 4,,15
	.type	handle_intel.constprop.5, @function
handle_intel.constprop.5:
	cmpl	$1, 108+_rtld_local_ro(%rip)
	jbe	.L302
	pushq	%r15
	pushq	%r14
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$40, %rsp
	movb	$0, 30(%rsp)
	movb	$0, 31(%rsp)
	leaq	30(%rsp), %r13
	movl	$1, 12(%rsp)
	leaq	31(%rsp), %r12
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L298:
	movl	%edx, %r14d
.L296:
	movl	$2, %eax
#APP
# 272 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$1, %r14d
	movl	%edx, 8(%rsp)
	movl	%ecx, %r15d
	movl	%eax, %esi
	jne	.L295
	movzbl	%al, %eax
	xorb	%sil, %sil
	movl	%eax, 12(%rsp)
.L295:
	leaq	116+_rtld_local_ro(%rip), %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L292
	leaq	116+_rtld_local_ro(%rip), %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebx, %esi
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L292
	leaq	116+_rtld_local_ro(%rip), %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, %esi
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L292
	leaq	116+_rtld_local_ro(%rip), %r9
	movl	8(%rsp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L292
	cmpl	%r14d, 12(%rsp)
	leal	1(%r14), %edx
	ja	.L298
	subl	$191, %ebp
	cmpl	$5, %ebp
	ja	.L292
	cmpb	$0, 30(%rsp)
	jne	.L297
.L292:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L297:
	movq	$-1, %rax
	jmp	.L292
.L302:
	orq	$-1, %rax
	ret
	.size	handle_intel.constprop.5, .-handle_intel.constprop.5
	.p2align 4,,15
	.type	update_usable.constprop.3, @function
update_usable.constprop.3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$160, %rsp
	movl	132+_rtld_local_ro(%rip), %r9d
	movl	164+_rtld_local_ro(%rip), %edi
	movl	136+_rtld_local_ro(%rip), %r12d
	movl	160+_rtld_local_ro(%rip), %r14d
	movl	168+_rtld_local_ro(%rip), %r13d
	movl	%r9d, %esi
	movl	%r9d, %eax
	movl	%edi, %edx
	andl	$47718915, %esi
	orl	148+_rtld_local_ro(%rip), %esi
	andl	$134217728, %eax
	andl	$4194737, %edx
	movl	%edi, %ebx
	orl	180+_rtld_local_ro(%rip), %edx
	movl	%r9d, %ecx
	andl	$16, %ebx
	movl	%r12d, %r8d
	andl	$1073741824, %ecx
	movl	%ebx, -116(%rsp)
	movl	196+_rtld_local_ro(%rip), %ebx
	orl	%eax, %esi
	movl	%r14d, %r10d
	movl	%r13d, %ebp
	orl	%ecx, %esi
	movl	%edi, %ecx
	andl	$394821904, %r8d
	andl	$436207616, %ecx
	movl	%ebx, %r11d
	orl	152+_rtld_local_ro(%rip), %r8d
	orl	%edx, %ecx
	movl	200+_rtld_local_ro(%rip), %edx
	andl	$562826008, %r10d
	andl	$1130512, %ebp
	orl	176+_rtld_local_ro(%rip), %r10d
	orl	184+_rtld_local_ro(%rip), %ebp
	andl	$2097505, %r11d
	orl	212+_rtld_local_ro(%rip), %r11d
	movl	%ebx, -108(%rsp)
	andl	$134217728, %edx
	orl	%edx, 216+_rtld_local_ro(%rip)
	movl	288+_rtld_local_ro(%rip), %edx
	movl	%r8d, 152+_rtld_local_ro(%rip)
	movl	%esi, 148+_rtld_local_ro(%rip)
	movl	%r10d, 176+_rtld_local_ro(%rip)
	movl	%ecx, -112(%rsp)
	andl	$512, %edx
	movl	%ecx, 180+_rtld_local_ro(%rip)
	movl	%ebp, 184+_rtld_local_ro(%rip)
	movl	%r11d, 212+_rtld_local_ro(%rip)
	orl	%edx, 304+_rtld_local_ro(%rip)
	movl	316+_rtld_local_ro(%rip), %r15d
	movl	%r15d, %ebx
	andl	$7168, %ebx
	orl	332+_rtld_local_ro(%rip), %ebx
	testl	%eax, %eax
	movl	%ebx, 332+_rtld_local_ro(%rip)
	je	.L305
	xorl	%ecx, %ecx
#APP
# 105 "../sysdeps/x86/cpu-features.c" 1
	xgetbv
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$6, %edx
	cmpl	$6, %edx
	je	.L376
	andl	$393216, %eax
	cmpl	$393216, %eax
	je	.L377
.L311:
	orl	$67108864, %esi
	movl	%esi, 148+_rtld_local_ro(%rip)
	movl	220+_rtld_local_ro(%rip), %esi
	movl	%esi, %ebp
	andl	$23, %ebp
	orl	236+_rtld_local_ro(%rip), %ebp
	cmpl	$12, 108+_rtld_local_ro(%rip)
	movl	%ebp, 236+_rtld_local_ro(%rip)
	jg	.L378
.L305:
	movl	-116(%rsp), %eax
	testl	%eax, %eax
	je	.L322
	orl	$8, 180+_rtld_local_ro(%rip)
.L322:
	movl	352+_rtld_local_ro(%rip), %eax
	testb	$1, %al
	je	.L323
	movl	368+_rtld_local_ro(%rip), %edx
	andl	$8388608, %edi
	orl	%edi, 180+_rtld_local_ro(%rip)
	andl	$4, %eax
	orl	$1, %edx
	orl	%edx, %eax
	movl	%eax, 368+_rtld_local_ro(%rip)
.L323:
	movl	%r8d, %edx
	xorl	%eax, %eax
	andl	$33024, %edx
	cmpl	$33024, %edx
	je	.L379
.L324:
	movl	%eax, 384+_rtld_local_ro(%rip)
	addq	$160, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%ecx, %ecx
	movl	$13, %eax
#APP
# 214 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	testl	%ebx, %ebx
	je	.L305
	addl	$127, %ebx
	andl	$-64, %ebx
	andl	$2, %esi
	movl	%ebx, %eax
	movl	%ebx, 400+_rtld_local_ro(%rip)
	movq	%rax, 392+_rtld_local_ro(%rip)
	je	.L305
	movabsq	$687194767360, %rax
	movl	$576, -96(%rsp)
	leaq	-96(%rsp), %r10
	movq	%rax, -104(%rsp)
	movabsq	$1099511627936, %rax
	leaq	32(%rsp), %r9
	movq	%rax, 24(%rsp)
	movl	$3, %r11d
	movl	$2, %esi
	movl	$238, %r13d
	movl	$13, %r14d
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L381:
	movl	%r14d, %eax
	movl	%esi, %ecx
#APP
# 242 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$2, %esi
	movl	%eax, (%r9)
	je	.L320
	movl	-4(%r9), %eax
	addl	-4(%r10), %eax
	andl	$2, %ecx
	je	.L375
	addl	$63, %eax
	andl	$-64, %eax
.L375:
	movl	%eax, (%r10)
.L319:
	cmpl	$32, %r11d
	je	.L380
.L320:
	addl	$1, %esi
	addl	$1, %r11d
	addq	$4, %r10
	addq	$4, %r9
.L313:
	btl	%esi, %r13d
	jc	.L381
	cmpl	$2, %esi
	movl	$0, (%r9)
	je	.L320
	movl	-4(%r9), %eax
	addl	-4(%r10), %eax
	movl	%eax, (%r10)
	jmp	.L319
	.p2align 4,,10
	.p2align 3
.L379:
	movl	%r12d, %eax
	andl	$1, %eax
	je	.L324
	andl	$125829120, %r8d
	cmpl	$125829120, %r8d
	je	.L382
	xorl	%eax, %eax
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L377:
	andl	$54525952, %r13d
	orl	%r13d, 184+_rtld_local_ro(%rip)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L376:
	testl	$268435456, %r9d
	je	.L307
	orl	$268435456, %esi
	testb	$32, %r14b
	jne	.L383
.L308:
	movl	%r15d, %edx
	andl	$536875008, %r9d
	andl	$16, %edx
	orl	%r9d, %esi
	orl	%edx, %ebx
	movl	%edi, %edx
	movl	%esi, 148+_rtld_local_ro(%rip)
	andl	$1536, %edx
	orl	-112(%rsp), %edx
	movl	%ebx, 332+_rtld_local_ro(%rip)
	movl	%edx, 180+_rtld_local_ro(%rip)
	movl	-108(%rsp), %edx
	andl	$2048, %edx
	orl	%edx, %r11d
	movl	%r11d, 212+_rtld_local_ro(%rip)
.L307:
	movl	%eax, %edx
	andl	$224, %edx
	cmpl	$224, %edx
	je	.L309
.L374:
	andl	$393216, %eax
	movl	148+_rtld_local_ro(%rip), %esi
	cmpl	$393216, %eax
	jne	.L311
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L382:
	movl	148+_rtld_local_ro(%rip), %edx
	testb	$32, %dh
	je	.L324
	movl	212+_rtld_local_ro(%rip), %ecx
	movl	%ecx, %esi
	andl	$1, %esi
	je	.L324
	movl	%edx, %edi
	movl	%esi, %eax
	andl	$9961985, %edi
	cmpl	$9961985, %edi
	jne	.L324
	testl	$268435456, %edx
	movl	$3, %eax
	je	.L324
	movl	176+_rtld_local_ro(%rip), %esi
	testb	$32, %sil
	je	.L324
	movl	%edx, %edi
	andl	$536875008, %edi
	cmpl	$536875008, %edi
	jne	.L324
	andl	$32, %ecx
	je	.L324
	andl	$4194304, %edx
	je	.L324
	movl	%esi, %edx
	movl	$7, %eax
	andl	$1342373888, %edx
	cmpl	$1342373888, %edx
	jne	.L324
	movl	%esi, %eax
	sarl	$31, %eax
	andl	$8, %eax
	addl	$7, %eax
	jmp	.L324
	.p2align 4,,10
	.p2align 3
.L380:
	movl	148(%rsp), %eax
	addl	20(%rsp), %eax
	je	.L305
	addl	$127, %eax
	orl	$2, %ebp
	andl	$-64, %eax
	movl	%ebp, 236+_rtld_local_ro(%rip)
	movq	%rax, 392+_rtld_local_ro(%rip)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L309:
	testl	$65536, %r14d
	je	.L374
	movl	176+_rtld_local_ro(%rip), %edx
	movl	%r14d, %ecx
	andl	$32, %r15d
	andl	$268435456, %ecx
	orl	%r15d, 332+_rtld_local_ro(%rip)
	orl	$65536, %edx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$134217728, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$67108864, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$-2147483648, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$131072, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$2097152, %r14d
	andl	$1073741824, %ecx
	orl	%ecx, %edx
	orl	%edx, %r14d
	movl	%edi, %edx
	andl	$22594, %edx
	orl	180+_rtld_local_ro(%rip), %edx
	movl	%r14d, 176+_rtld_local_ro(%rip)
	movl	%edx, 180+_rtld_local_ro(%rip)
	movl	%r13d, %edx
	andl	$8388876, %edx
	orl	%edx, %ebp
	movl	%ebp, 184+_rtld_local_ro(%rip)
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L383:
	orl	$32, %r10d
	orl	$256, 380+_rtld_local_ro(%rip)
	movl	%r10d, 176+_rtld_local_ro(%rip)
	jmp	.L308
	.size	update_usable.constprop.3, .-update_usable.constprop.3
	.section	.rodata.str1.1
.LC2:
	.string	"haswell"
.LC3:
	.string	"xeon_phi"
	.text
	.p2align 4,,15
	.type	init_cpu_features.constprop.1, @function
init_cpu_features.constprop.1:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
#APP
# 398 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	subq	$248, %rsp
	cmpl	$1970169159, %ebx
	movl	%eax, 108+_rtld_local_ro(%rip)
	movl	$0, 96(%rsp)
	movl	$0, 100(%rsp)
	movl	$0, 104(%rsp)
	jne	.L385
	cmpl	$1818588270, %ecx
	jne	.L385
	cmpl	$1231384169, %edx
	je	.L547
.L386:
	cmpl	$1953391939, %ebx
	jne	.L408
	cmpl	$1936487777, %ecx
	jne	.L408
	cmpl	$1215460705, %edx
	jne	.L409
.L415:
	leaq	224(%rsp), %rax
	leaq	232(%rsp), %r15
	leaq	100(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movq	%rax, %rdx
	movq	%r15, %rcx
	movq	%rax, 48(%rsp)
	call	get_common_indices.constprop.2
	movl	$-2147483648, %esi
	movl	%esi, %eax
#APP
# 295 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483648, %eax
	movl	%eax, %esi
	jbe	.L417
	movl	$-2147483647, %eax
#APP
# 297 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483642, %esi
	movl	%eax, 188+_rtld_local_ro(%rip)
	movl	%ebx, 192+_rtld_local_ro(%rip)
	movl	%ecx, 196+_rtld_local_ro(%rip)
	movl	%edx, 200+_rtld_local_ro(%rip)
	jbe	.L417
	movl	$-2147483641, %eax
#APP
# 303 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483641, %esi
	movl	%eax, 252+_rtld_local_ro(%rip)
	movl	%ebx, 256+_rtld_local_ro(%rip)
	movl	%ecx, 260+_rtld_local_ro(%rip)
	movl	%edx, 264+_rtld_local_ro(%rip)
	je	.L417
	movl	$-2147483640, %eax
#APP
# 309 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 284+_rtld_local_ro(%rip)
	movl	%ebx, 288+_rtld_local_ro(%rip)
	movl	%ecx, 292+_rtld_local_ro(%rip)
	movl	%edx, 296+_rtld_local_ro(%rip)
.L417:
	call	update_usable.constprop.3
	movl	224(%rsp), %ecx
	movl	96(%rsp), %ebp
	addl	100(%rsp), %ecx
	cmpl	$6, %ebp
	movl	%ecx, 100(%rsp)
	je	.L548
	cmpl	$7, %ebp
	je	.L549
.L420:
	movl	104(%rsp), %esi
	movl	$3, %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L385:
	cmpl	$1752462657, %ebx
	jne	.L406
	cmpl	$1145913699, %ecx
	jne	.L406
	cmpl	$1769238117, %edx
	je	.L407
.L408:
	cmpl	$1750278176, %ebx
	sete	%sil
	cmpl	$538995041, %ecx
	sete	%al
	testb	%al, %sil
	je	.L409
	cmpl	$1751608929, %edx
	je	.L415
	.p2align 4,,10
	.p2align 3
.L409:
	cmpl	$6, 108+_rtld_local_ro(%rip)
	jle	.L424
	movl	$7, %r8d
	xorl	%esi, %esi
	movl	$1, %edi
	movl	%r8d, %eax
	movl	%esi, %ecx
#APP
# 342 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 156+_rtld_local_ro(%rip)
	movl	%ebx, 160+_rtld_local_ro(%rip)
	movl	%r8d, %eax
	movl	%ecx, 164+_rtld_local_ro(%rip)
	movl	%edx, 168+_rtld_local_ro(%rip)
	movl	%edi, %ecx
#APP
# 347 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$12, 108+_rtld_local_ro(%rip)
	movl	%eax, 316+_rtld_local_ro(%rip)
	movl	%ebx, 320+_rtld_local_ro(%rip)
	movl	%ecx, 324+_rtld_local_ro(%rip)
	movl	%edx, 328+_rtld_local_ro(%rip)
	jle	.L424
	movl	$13, %eax
	movl	%edi, %ecx
#APP
# 355 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$24, 108+_rtld_local_ro(%rip)
	movl	%eax, 220+_rtld_local_ro(%rip)
	movl	%ebx, 224+_rtld_local_ro(%rip)
	movl	%ecx, 228+_rtld_local_ro(%rip)
	movl	%edx, 232+_rtld_local_ro(%rip)
	jle	.L424
	movl	$25, %eax
	movl	%esi, %ecx
#APP
# 362 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 348+_rtld_local_ro(%rip)
	movl	%ebx, 352+_rtld_local_ro(%rip)
	movl	%ecx, 356+_rtld_local_ro(%rip)
	movl	%edx, 360+_rtld_local_ro(%rip)
.L424:
	call	update_usable.constprop.3
	leaq	224(%rsp), %rbx
	movl	96(%rsp), %ebp
	movl	100(%rsp), %ecx
	movl	104(%rsp), %esi
	leaq	232(%rsp), %r15
	movl	$4, %eax
	movq	%rbx, 48(%rsp)
.L405:
	movl	136+_rtld_local_ro(%rip), %edx
	testb	$1, %dh
	je	.L425
	orl	$1, 380+_rtld_local_ro(%rip)
.L425:
	andb	$-128, %dh
	je	.L426
	orl	$2, 380+_rtld_local_ro(%rip)
.L426:
	movq	$-1, %r12
	cmpl	$1, %eax
	movl	%eax, 104+_rtld_local_ro(%rip)
	movl	%ebp, 112+_rtld_local_ro(%rip)
	movl	%ecx, 116+_rtld_local_ro(%rip)
	movl	%esi, 120+_rtld_local_ro(%rip)
	movq	%r12, 112(%rsp)
	movl	$0, 108(%rsp)
	je	.L550
	cmpl	$3, %eax
	je	.L551
	cmpl	$2, %eax
	je	.L552
	leaq	112(%rsp), %rax
	xorl	%ebp, %ebp
	movq	%r12, %r9
	movq	%r12, %r8
	movq	%r12, 40(%rsp)
	movq	%r12, %r10
	movq	%r12, %r11
	movq	%r12, %r14
	movq	%r12, 32(%rsp)
	movq	%r12, 24(%rsp)
	movq	%r12, 16(%rsp)
	movq	%r12, %r13
	movq	%r12, 8(%rsp)
	movq	%rax, 56(%rsp)
.L428:
	movq	8(%rsp), %rax
	testb	$1, 178+_rtld_local_ro(%rip)
	movq	%r14, 480+_rtld_local_ro(%rip)
	movq	%r11, 488+_rtld_local_ro(%rip)
	movq	%r10, 496+_rtld_local_ro(%rip)
	movq	%r8, 512+_rtld_local_ro(%rip)
	movq	%rax, 448+_rtld_local_ro(%rip)
	movq	16(%rsp), %rax
	movq	%r9, 520+_rtld_local_ro(%rip)
	movq	%r12, 528+_rtld_local_ro(%rip)
	movq	%rax, 456+_rtld_local_ro(%rip)
	movq	24(%rsp), %rax
	movq	%rax, 464+_rtld_local_ro(%rip)
	movq	32(%rsp), %rax
	movq	%rax, 472+_rtld_local_ro(%rip)
	movq	40(%rsp), %rax
	movq	%rax, 504+_rtld_local_ro(%rip)
	movl	380+_rtld_local_ro(%rip), %eax
	je	.L436
	testb	$64, %ah
	jne	.L436
	movl	$512, %r14d
	movl	$8192, %ebx
.L437:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$24, %edi
	call	__GI___tunable_get_val
	movq	232(%rsp), %r12
	movq	%r15, %rsi
	movl	$4, %edi
	testq	%r12, %r12
	cmove	%r13, %r12
	xorl	%edx, %edx
	call	__GI___tunable_get_val
	movq	232(%rsp), %rax
	testq	%rax, %rax
	je	.L439
	movq	%rax, 112(%rsp)
.L439:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	$13, %edi
	call	__GI___tunable_get_val
	movq	232(%rsp), %rax
	movq	%r15, %rsi
	movl	$8, %edi
	movq	$-1, %r13
	testq	%rax, %rax
	cmovne	%rax, %rbp
	xorl	%edx, %edx
	call	__GI___tunable_get_val
	movq	232(%rsp), %rax
	movq	%r15, %rsi
	movl	$12, %edi
	cmpq	%r14, %rax
	cmovg	%eax, %ebx
	xorl	%edx, %edx
	call	__GI___tunable_get_val
	movq	232(%rsp), %rax
	leaq	120(%rsp), %rcx
	leaq	128(%rsp), %rdx
	leaq	136(%rsp), %rsi
	movl	$24, %edi
	movq	%r13, 120(%rsp)
	movq	$0, 128(%rsp)
	movq	%r12, 136(%rsp)
	movq	%rax, 8(%rsp)
	call	__GI___tunable_set_val
	movq	112(%rsp), %rax
	leaq	144(%rsp), %rcx
	leaq	152(%rsp), %rdx
	leaq	160(%rsp), %rsi
	movl	$4, %edi
	movq	%r13, 144(%rsp)
	movq	$0, 152(%rsp)
	movq	%rax, 160(%rsp)
	call	__GI___tunable_set_val
	leaq	168(%rsp), %rcx
	leaq	176(%rsp), %rdx
	leaq	184(%rsp), %rsi
	movl	$13, %edi
	movq	%r13, 168(%rsp)
	movq	$0, 176(%rsp)
	movq	%rbp, 184(%rsp)
	call	__GI___tunable_set_val
	movl	%ebx, %edi
	leaq	192(%rsp), %rcx
	leaq	200(%rsp), %rdx
	leaq	208(%rsp), %rsi
	movq	%rdi, 208(%rsp)
	movl	$8, %edi
	movq	%r14, 200(%rsp)
	movq	%r13, 192(%rsp)
	call	__GI___tunable_set_val
	movq	8(%rsp), %r14
	movq	48(%rsp), %rdx
	leaq	216(%rsp), %rcx
	movq	%r15, %rsi
	movl	$12, %edi
	movq	%r13, 216(%rsp)
	movq	$1, 224(%rsp)
	movq	%r14, 232(%rsp)
	call	__GI___tunable_set_val
	movq	112(%rsp), %rax
	movq	56(%rsp), %rsi
	leaq	_dl_tunable_set_hwcaps(%rip), %rdx
	movl	$20, %edi
	movq	%r12, 408+_rtld_local_ro(%rip)
	movq	%rbp, 424+_rtld_local_ro(%rip)
	movq	%r14, 440+_rtld_local_ro(%rip)
	movq	%rax, 416+_rtld_local_ro(%rip)
	movl	%ebx, %eax
	movq	%rax, 432+_rtld_local_ro(%rip)
	call	__GI___tunable_get_val
	cmpl	$1, 104+_rtld_local_ro(%rip)
	movq	$2, 88+_rtld_local_ro(%rip)
	je	.L553
.L384:
	addq	$248, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L436:
	andl	$256, %eax
	cmpl	$1, %eax
	sbbq	%r14, %r14
	andq	$-128, %r14
	addq	$256, %r14
	cmpl	$1, %eax
	sbbl	%ebx, %ebx
	andl	$-2048, %ebx
	addl	$4096, %ebx
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L553:
	movl	176+_rtld_local_ro(%rip), %eax
	testl	$268435456, %eax
	je	.L444
	testl	$134217728, %eax
	je	.L445
	testl	$67108864, %eax
	je	.L444
	leaq	.LC3(%rip), %rax
.L446:
	movq	%rax, 8+_rtld_local_ro(%rip)
	jmp	.L384
	.p2align 4,,10
	.p2align 3
.L445:
	movl	%eax, %edx
	andl	$1073872896, %edx
	cmpl	$1073872896, %edx
	jne	.L444
	testl	%eax, %eax
	jns	.L444
	movq	$6, 88+_rtld_local_ro(%rip)
	.p2align 4,,10
	.p2align 3
.L444:
	testb	$32, %al
	je	.L384
	movl	148+_rtld_local_ro(%rip), %edx
	testb	$16, %dh
	je	.L384
	andl	$264, %eax
	cmpl	$264, %eax
	jne	.L384
	testb	$32, 212+_rtld_local_ro(%rip)
	je	.L384
	andl	$12582912, %edx
	cmpl	$12582912, %edx
	jne	.L384
	leaq	.LC2(%rip), %rax
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L406:
	cmpl	$1869052232, %ebx
	jne	.L386
	cmpl	$1701734773, %ecx
	jne	.L386
	cmpl	$1852131182, %edx
	jne	.L409
.L407:
	leaq	232(%rsp), %r15
	leaq	104(%rsp), %rcx
	leaq	100(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movq	%r15, %rdx
	call	get_common_indices.constprop.2
	movl	$-2147483648, %esi
	movl	%esi, %eax
#APP
# 295 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483648, %eax
	movl	%eax, %esi
	jbe	.L411
	movl	$-2147483647, %eax
#APP
# 297 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483642, %esi
	movl	%eax, 188+_rtld_local_ro(%rip)
	movl	%ebx, 192+_rtld_local_ro(%rip)
	movl	%ecx, 196+_rtld_local_ro(%rip)
	movl	%edx, 200+_rtld_local_ro(%rip)
	jbe	.L411
	movl	$-2147483641, %eax
#APP
# 303 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483641, %esi
	movl	%eax, 252+_rtld_local_ro(%rip)
	movl	%ebx, 256+_rtld_local_ro(%rip)
	movl	%ecx, 260+_rtld_local_ro(%rip)
	movl	%edx, 264+_rtld_local_ro(%rip)
	je	.L411
	movl	$-2147483640, %eax
#APP
# 309 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 284+_rtld_local_ro(%rip)
	movl	%ebx, 288+_rtld_local_ro(%rip)
	movl	%ecx, 292+_rtld_local_ro(%rip)
	movl	%edx, 296+_rtld_local_ro(%rip)
.L411:
	call	update_usable.constprop.3
	testb	$16, 151+_rtld_local_ro(%rip)
	je	.L413
	movl	196+_rtld_local_ro(%rip), %eax
	andl	$65536, %eax
	orl	%eax, 212+_rtld_local_ro(%rip)
.L413:
	movl	96(%rsp), %ebp
	movl	100(%rsp), %ecx
	cmpl	$21, %ebp
	je	.L554
.L414:
	leaq	224(%rsp), %rbx
	movl	104(%rsp), %esi
	movl	$2, %eax
	movq	%rbx, 48(%rsp)
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L550:
	movl	$188, %edi
	call	handle_intel.constprop.5
	movl	$191, %edi
	movq	%rax, %r13
	call	handle_intel.constprop.5
	movl	$194, %edi
	movq	%rax, %r14
	call	handle_intel.constprop.5
	movl	$185, %edi
	movq	%rax, 112(%rsp)
	call	handle_intel.constprop.5
	movl	$189, %edi
	movq	%rax, 8(%rsp)
	movq	%r13, 16(%rsp)
	call	handle_intel.constprop.5
	movl	$190, %edi
	movq	%rax, 24(%rsp)
	call	handle_intel.constprop.5
	movl	$192, %edi
	movq	%rax, 32(%rsp)
	call	handle_intel.constprop.5
	movl	$193, %edi
	movq	%rax, 88(%rsp)
	call	handle_intel.constprop.5
	movq	%rax, 80(%rsp)
	movq	112(%rsp), %rax
	movl	$195, %edi
	movq	%rax, 40(%rsp)
	call	handle_intel.constprop.5
	movl	$196, %edi
	movq	%rax, 72(%rsp)
	call	handle_intel.constprop.5
	movl	$197, %edi
	movq	%rax, 64(%rsp)
	call	handle_intel.constprop.5
	movq	%rax, %r12
.L546:
	leaq	112(%rsp), %rax
	leaq	108(%rsp), %rsi
	movq	%r14, %rdx
	movq	%rax, %rdi
	movq	%rax, 56(%rsp)
	call	get_common_cache_info
	movq	112(%rsp), %rax
	movq	64(%rsp), %r9
	movq	72(%rsp), %r8
	movq	80(%rsp), %r10
	movq	88(%rsp), %r11
	leaq	(%rax,%rax,2), %rax
	leaq	3(%rax), %rbp
	testq	%rax, %rax
	cmovns	%rax, %rbp
	sarq	$2, %rbp
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	232(%rsp), %r15
	leaq	104(%rsp), %rcx
	leaq	100(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movq	%r15, %rdx
	call	get_common_indices.constprop.2
	movl	$-2147483648, %esi
	movl	%esi, %eax
#APP
# 295 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483648, %eax
	movl	%eax, %esi
	jbe	.L388
	movl	$-2147483647, %eax
#APP
# 297 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483642, %esi
	movl	%eax, 188+_rtld_local_ro(%rip)
	movl	%ebx, 192+_rtld_local_ro(%rip)
	movl	%ecx, 196+_rtld_local_ro(%rip)
	movl	%edx, 200+_rtld_local_ro(%rip)
	jbe	.L388
	movl	$-2147483641, %eax
#APP
# 303 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483641, %esi
	movl	%eax, 252+_rtld_local_ro(%rip)
	movl	%ebx, 256+_rtld_local_ro(%rip)
	movl	%ecx, 260+_rtld_local_ro(%rip)
	movl	%edx, 264+_rtld_local_ro(%rip)
	je	.L388
	movl	$-2147483640, %eax
#APP
# 309 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 284+_rtld_local_ro(%rip)
	movl	%ebx, 288+_rtld_local_ro(%rip)
	movl	%ecx, 292+_rtld_local_ro(%rip)
	movl	%edx, 296+_rtld_local_ro(%rip)
.L388:
	call	update_usable.constprop.3
	movl	96(%rsp), %ebp
	cmpl	$6, %ebp
	je	.L390
	movl	104(%rsp), %esi
	movl	380+_rtld_local_ro(%rip), %eax
.L391:
	movl	%eax, %edx
	orb	$8, %ah
	leaq	224(%rsp), %rbx
	orb	$64, %dh
	testb	$8, 163+_rtld_local_ro(%rip)
	movl	100(%rsp), %ecx
	movq	%rbx, 48(%rsp)
	cmove	%edx, %eax
	movl	%eax, 380+_rtld_local_ro(%rip)
	movl	$1, %eax
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L554:
	leal	-96(%rcx), %eax
	cmpl	$31, %eax
	ja	.L414
	movl	380+_rtld_local_ro(%rip), %eax
	andb	$-2, %ah
	orl	$24, %eax
	movl	%eax, 380+_rtld_local_ro(%rip)
	jmp	.L414
	.p2align 4,,10
	.p2align 3
.L551:
	movl	$188, %edi
	call	handle_zhaoxin
	movl	$191, %edi
	movq	%rax, %r13
	call	handle_zhaoxin
	movl	$194, %edi
	movq	%rax, %r14
	call	handle_zhaoxin
	movl	$185, %edi
	movq	%rax, %rbx
	movq	%rax, 112(%rsp)
	call	handle_zhaoxin
	movl	$189, %edi
	movq	%rax, 8(%rsp)
	movq	%r13, 16(%rsp)
	call	handle_zhaoxin
	movl	$190, %edi
	movq	%rax, 24(%rsp)
	call	handle_zhaoxin
	movl	$192, %edi
	movq	%rax, 32(%rsp)
	call	handle_zhaoxin
	movl	$193, %edi
	movq	%rax, 88(%rsp)
	call	handle_zhaoxin
	movl	$195, %edi
	movq	%rax, 80(%rsp)
	movq	%rbx, 40(%rsp)
	call	handle_zhaoxin
	movl	$196, %edi
	movq	%rax, 72(%rsp)
	call	handle_zhaoxin
	movq	%rax, 64(%rsp)
	jmp	.L546
	.p2align 4,,10
	.p2align 3
.L552:
	movl	$188, %edi
	call	handle_amd
	movl	$191, %edi
	movq	%rax, %r13
	call	handle_amd
	movl	$194, %edi
	movq	%rax, %r14
	movq	%rax, 88(%rsp)
	call	handle_amd
	movl	$185, %edi
	movq	%rax, %rbx
	movq	%rax, 56(%rsp)
	call	handle_amd
	movl	$189, %edi
	movq	%rax, 8(%rsp)
	movq	%r13, 16(%rsp)
	call	handle_amd
	movl	$190, %edi
	movq	%rax, 24(%rsp)
	call	handle_amd
	movl	$192, %edi
	movq	%rax, 32(%rsp)
	call	handle_amd
	movl	$193, %edi
	movq	%rax, 80(%rsp)
	call	handle_amd
	movl	$195, %edi
	movq	%rbx, %r10
	movq	%rbx, 40(%rsp)
	movq	%rax, 72(%rsp)
	call	handle_amd
	movl	$196, %edi
	movq	%rax, 64(%rsp)
	call	handle_amd
	movq	%rax, %r9
	movl	$-2147483648, %eax
	movq	64(%rsp), %r8
#APP
# 786 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	testq	%r10, %r10
	movq	80(%rsp), %r11
	movq	72(%rsp), %r10
	jle	.L555
	cmpl	$-2147483641, %eax
	jbe	.L431
	movl	$-2147483640, %eax
#APP
# 797 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	shrl	$12, %ecx
	movl	$1, %ebx
	andl	$15, %ecx
	sall	%cl, %ebx
	cmpl	$22, %ebp
	movl	%ebx, 108(%rsp)
	ja	.L431
.L432:
	movq	56(%rsp), %rax
	cqto
	idivq	%rbx
	movq	%rax, 56(%rsp)
.L434:
	cmpl	$22, %ebp
	jbe	.L435
	movl	$-2147483619, %eax
	movl	$3, %ecx
	movq	$-1, %r12
#APP
# 822 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	shrl	$14, %eax
	andl	$4095, %eax
	addq	$1, %rax
	imulq	56(%rsp), %rax
	leaq	(%rax,%rax,2), %rbp
	movq	%rax, 112(%rsp)
	leaq	112(%rsp), %rax
	sarq	$2, %rbp
	movq	%rax, 56(%rsp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L431:
	movl	$1, %eax
#APP
# 805 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	andl	$268435456, %edx
	je	.L556
	shrl	$16, %ebx
	movzbl	%bl, %ebx
	movl	%ebx, 108(%rsp)
.L433:
	testl	%ebx, %ebx
	je	.L434
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L548:
	cmpl	$15, %ecx
	je	.L422
	cmpl	$25, %ecx
	jne	.L420
.L422:
	movl	380+_rtld_local_ro(%rip), %eax
	andl	$-268435457, 148+_rtld_local_ro(%rip)
	andl	$-33, 176+_rtld_local_ro(%rip)
	andb	$-2, %ah
	orb	$-128, %al
	movl	%eax, 380+_rtld_local_ro(%rip)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L556:
	movl	108(%rsp), %ebx
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L555:
	leaq	(%r14,%r14,2), %rax
	movq	%r14, 112(%rsp)
	leaq	3(%rax), %rbp
	testq	%rax, %rax
	cmovns	%rax, %rbp
	leaq	112(%rsp), %rax
	sarq	$2, %rbp
	movq	%rax, 56(%rsp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L549:
	cmpl	$27, %ecx
	je	.L422
	cmpl	$59, %ecx
	jne	.L420
	andl	$-268435457, 148+_rtld_local_ro(%rip)
	andl	$-33, 176+_rtld_local_ro(%rip)
	andl	$-257, 380+_rtld_local_ro(%rip)
	jmp	.L420
	.p2align 4,,10
	.p2align 3
.L435:
	movq	88(%rsp), %rax
	addq	56(%rsp), %rax
	movq	$-1, %r12
	movq	%rax, 112(%rsp)
	leaq	(%rax,%rax,2), %rax
	leaq	3(%rax), %rbp
	testq	%rax, %rax
	cmovns	%rax, %rbp
	leaq	112(%rsp), %rax
	sarq	$2, %rbp
	movq	%rax, 56(%rsp)
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L390:
	movl	232(%rsp), %edx
	addl	100(%rsp), %edx
	movl	104(%rsp), %esi
	leal	-26(%rdx), %eax
	movl	%edx, 100(%rsp)
	cmpl	$130, %eax
	ja	.L392
	leaq	.L394(%rip), %rcx
	movslq	(%rcx,%rax,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L394:
	.long	.L393-.L394
	.long	.L392-.L394
	.long	.L395-.L394
	.long	.L392-.L394
	.long	.L393-.L394
	.long	.L393-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L393-.L394
	.long	.L395-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L393-.L394
	.long	.L392-.L394
	.long	.L393-.L394
	.long	.L393-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L396-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L397-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L397-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L392-.L394
	.long	.L397-.L394
	.text
.L392:
	testb	$16, 135+_rtld_local_ro(%rip)
	movl	380+_rtld_local_ro(%rip), %eax
	je	.L398
.L393:
	movl	380+_rtld_local_ro(%rip), %eax
	orl	$1076, %eax
.L398:
	cmpl	$63, %edx
	je	.L400
	ja	.L401
	cmpl	$60, %edx
	jne	.L391
.L402:
	andl	$-2049, 176+_rtld_local_ro(%rip)
	jmp	.L391
.L397:
	movl	380+_rtld_local_ro(%rip), %eax
	orl	$1204, %eax
	jmp	.L398
.L396:
	movl	380+_rtld_local_ro(%rip), %eax
	orl	$1200, %eax
	jmp	.L398
.L395:
	movl	380+_rtld_local_ro(%rip), %eax
	orl	$64, %eax
	jmp	.L391
	.p2align 4,,10
	.p2align 3
.L400:
	cmpl	$3, %esi
	ja	.L391
	jmp	.L402
.L401:
	subl	$69, %edx
	cmpl	$1, %edx
	jbe	.L402
	jmp	.L391
	.size	init_cpu_features.constprop.1, .-init_cpu_features.constprop.1
	.p2align 4,,15
	.type	__x86_cpu_features_ifunc, @function
__x86_cpu_features_ifunc:
	movl	104+_rtld_local_ro(%rip), %eax
	testl	%eax, %eax
	je	.L563
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	subq	$8, %rsp
	call	init_cpu_features.constprop.1
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__x86_cpu_features_ifunc, .-__x86_cpu_features_ifunc
	.globl	__x86_cpu_features
	.hidden	__x86_cpu_features
	.type	__x86_cpu_features, @gnu_indirect_function
	.set	__x86_cpu_features,__x86_cpu_features_ifunc
	.p2align 4,,15
	.globl	_dl_x86_init_cpu_features
	.hidden	_dl_x86_init_cpu_features
	.type	_dl_x86_init_cpu_features, @function
_dl_x86_init_cpu_features:
	movl	104+_rtld_local_ro(%rip), %eax
	testl	%eax, %eax
	je	.L566
	rep ret
	.p2align 4,,10
	.p2align 3
.L566:
	jmp	init_cpu_features.constprop.1
	.size	_dl_x86_init_cpu_features, .-_dl_x86_init_cpu_features
	.p2align 4,,15
	.globl	_dl_x86_get_cpu_features
	.type	_dl_x86_get_cpu_features, @function
_dl_x86_get_cpu_features:
	leaq	104+_rtld_local_ro(%rip), %rax
	ret
	.size	_dl_x86_get_cpu_features, .-_dl_x86_get_cpu_features
	.section	.rodata
	.align 16
	.type	__PRETTY_FUNCTION__.10001, @object
	.size	__PRETTY_FUNCTION__.10001, 17
__PRETTY_FUNCTION__.10001:
	.string	"intel_check_word"
	.hidden	__x86_cpu_features_p
	.globl	__x86_cpu_features_p
	.section	.data.rel.ro,"aw",@progbits
	.align 8
	.type	__x86_cpu_features_p, @object
	.size	__x86_cpu_features_p, 8
__x86_cpu_features_p:
	.quad	__x86_cpu_features
	.section	.rodata
	.align 32
	.type	intel_02_known, @object
	.size	intel_02_known, 544
intel_02_known:
	.byte	6
	.byte	4
	.byte	32
	.byte	0
	.long	8192
	.byte	8
	.byte	4
	.byte	32
	.byte	0
	.long	16384
	.byte	9
	.byte	4
	.byte	32
	.byte	0
	.long	32768
	.byte	10
	.byte	2
	.byte	32
	.byte	3
	.long	8192
	.byte	12
	.byte	4
	.byte	32
	.byte	3
	.long	16384
	.byte	13
	.byte	4
	.byte	64
	.byte	3
	.long	16384
	.byte	14
	.byte	6
	.byte	64
	.byte	3
	.long	24576
	.byte	33
	.byte	8
	.byte	64
	.byte	6
	.long	262144
	.byte	34
	.byte	4
	.byte	64
	.byte	9
	.long	524288
	.byte	35
	.byte	8
	.byte	64
	.byte	9
	.long	1048576
	.byte	37
	.byte	8
	.byte	64
	.byte	9
	.long	2097152
	.byte	41
	.byte	8
	.byte	64
	.byte	9
	.long	4194304
	.byte	44
	.byte	8
	.byte	64
	.byte	3
	.long	32768
	.byte	48
	.byte	8
	.byte	64
	.byte	0
	.long	32768
	.byte	57
	.byte	4
	.byte	64
	.byte	6
	.long	131072
	.byte	58
	.byte	6
	.byte	64
	.byte	6
	.long	196608
	.byte	59
	.byte	2
	.byte	64
	.byte	6
	.long	131072
	.byte	60
	.byte	4
	.byte	64
	.byte	6
	.long	262144
	.byte	61
	.byte	6
	.byte	64
	.byte	6
	.long	393216
	.byte	62
	.byte	4
	.byte	64
	.byte	6
	.long	524288
	.byte	63
	.byte	2
	.byte	64
	.byte	6
	.long	262144
	.byte	65
	.byte	4
	.byte	32
	.byte	6
	.long	131072
	.byte	66
	.byte	4
	.byte	32
	.byte	6
	.long	262144
	.byte	67
	.byte	4
	.byte	32
	.byte	6
	.long	524288
	.byte	68
	.byte	4
	.byte	32
	.byte	6
	.long	1048576
	.byte	69
	.byte	4
	.byte	32
	.byte	6
	.long	2097152
	.byte	70
	.byte	4
	.byte	64
	.byte	9
	.long	4194304
	.byte	71
	.byte	8
	.byte	64
	.byte	9
	.long	8388608
	.byte	72
	.byte	12
	.byte	64
	.byte	6
	.long	3145728
	.byte	73
	.byte	16
	.byte	64
	.byte	6
	.long	4194304
	.byte	74
	.byte	12
	.byte	64
	.byte	9
	.long	6291456
	.byte	75
	.byte	16
	.byte	64
	.byte	9
	.long	8388608
	.byte	76
	.byte	12
	.byte	64
	.byte	9
	.long	12582912
	.byte	77
	.byte	16
	.byte	64
	.byte	9
	.long	16777216
	.byte	78
	.byte	24
	.byte	64
	.byte	6
	.long	6291456
	.byte	96
	.byte	8
	.byte	64
	.byte	3
	.long	16384
	.byte	102
	.byte	4
	.byte	64
	.byte	3
	.long	8192
	.byte	103
	.byte	4
	.byte	64
	.byte	3
	.long	16384
	.byte	104
	.byte	4
	.byte	64
	.byte	3
	.long	32768
	.byte	120
	.byte	8
	.byte	64
	.byte	6
	.long	1048576
	.byte	121
	.byte	8
	.byte	64
	.byte	6
	.long	131072
	.byte	122
	.byte	8
	.byte	64
	.byte	6
	.long	262144
	.byte	123
	.byte	8
	.byte	64
	.byte	6
	.long	524288
	.byte	124
	.byte	8
	.byte	64
	.byte	6
	.long	1048576
	.byte	125
	.byte	8
	.byte	64
	.byte	6
	.long	2097152
	.byte	127
	.byte	2
	.byte	64
	.byte	6
	.long	524288
	.byte	-128
	.byte	8
	.byte	64
	.byte	6
	.long	524288
	.byte	-126
	.byte	8
	.byte	32
	.byte	6
	.long	262144
	.byte	-125
	.byte	8
	.byte	32
	.byte	6
	.long	524288
	.byte	-124
	.byte	8
	.byte	32
	.byte	6
	.long	1048576
	.byte	-123
	.byte	8
	.byte	32
	.byte	6
	.long	2097152
	.byte	-122
	.byte	4
	.byte	64
	.byte	6
	.long	524288
	.byte	-121
	.byte	8
	.byte	64
	.byte	6
	.long	1048576
	.byte	-48
	.byte	4
	.byte	64
	.byte	9
	.long	524288
	.byte	-47
	.byte	4
	.byte	64
	.byte	9
	.long	1048576
	.byte	-46
	.byte	4
	.byte	64
	.byte	9
	.long	2097152
	.byte	-42
	.byte	8
	.byte	64
	.byte	9
	.long	1048576
	.byte	-41
	.byte	8
	.byte	64
	.byte	9
	.long	2097152
	.byte	-40
	.byte	8
	.byte	64
	.byte	9
	.long	4194304
	.byte	-36
	.byte	12
	.byte	64
	.byte	9
	.long	2097152
	.byte	-35
	.byte	12
	.byte	64
	.byte	9
	.long	4194304
	.byte	-34
	.byte	12
	.byte	64
	.byte	9
	.long	8388608
	.byte	-30
	.byte	16
	.byte	64
	.byte	9
	.long	2097152
	.byte	-29
	.byte	16
	.byte	64
	.byte	9
	.long	4194304
	.byte	-28
	.byte	16
	.byte	64
	.byte	9
	.long	8388608
	.byte	-22
	.byte	24
	.byte	64
	.byte	9
	.long	12582912
	.byte	-21
	.byte	24
	.byte	64
	.byte	9
	.long	18874368
	.byte	-20
	.byte	24
	.byte	64
	.byte	9
	.long	25165824
	.hidden	_dl_tunable_set_hwcaps
	.hidden	_rtld_local_ro
