	.text
	.p2align 4,,15
	.globl	__libc_early_init
	.type	__libc_early_init, @function
__libc_early_init:
	pushq	%rbx
	movl	%edi, %ebx
	call	__ctype_init
	movb	%bl, __libc_single_threaded(%rip)
	popq	%rbx
	ret
	.size	__libc_early_init, .-__libc_early_init
	.hidden	__ctype_init
