	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/self/exe"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../sysdeps/unix/sysv/linux/dl-origin.c"
	.section	.rodata.str1.1
.LC2:
	.string	"linkval[0] == '/'"
	.text
	.p2align 4,,15
	.globl	_dl_get_origin
	.hidden	_dl_get_origin
	.type	_dl_get_origin, @function
_dl_get_origin:
	pushq	%r12
	pushq	%rbp
	movl	$4096, %edx
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	movl	$89, %eax
	subq	$4096, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rsi
#APP
# 41 "../sysdeps/unix/sysv/linux/dl-origin.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L2
	testl	%eax, %eax
	jle	.L2
	movzbl	(%rsp), %edx
	cmpb	$91, %dl
	je	.L2
	cmpb	$47, %dl
	jne	.L39
	cmpl	$1, %eax
	je	.L4
	leal	-1(%rax), %edx
	movl	%eax, %r12d
	movslq	%edx, %rdx
	cmpb	$47, (%rsp,%rdx)
	je	.L5
	subl	$2, %eax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$1, %rdx
	cmpb	$47, 0(%rbp,%rdx)
	je	.L5
.L6:
	cmpq	%rax, %rdx
	movl	%edx, %r12d
	jne	.L7
.L4:
	movl	$2, %edi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L10
	movl	$47, %eax
	movw	%ax, (%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movq	608+_rtld_local_ro(%rip), %rdi
	testq	%rdi, %rdi
	je	.L10
	call	strlen
	movq	%rax, %rbp
	leaq	1(%rax), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L10
	movq	608+_rtld_local_ro(%rip), %rsi
	movq	%rbp, %rdx
	movq	%rax, %rdi
	call	__mempcpy@PLT
	leaq	1(%rbx), %rdx
	cmpq	%rdx, %rax
	ja	.L37
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	subq	$1, %rax
	cmpq	%rdx, %rax
	je	.L11
.L37:
	cmpb	$47, -1(%rax)
	je	.L40
.L11:
	movb	$0, (%rax)
	addq	$4096, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	$-1, %rbx
.L1:
	addq	$4096, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leal	1(%r12), %edi
	movslq	%edi, %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L10
	leal	-1(%r12), %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movslq	%edx, %rdx
	call	__mempcpy@PLT
	movb	$0, (%rax)
	addq	$4096, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L39:
	leaq	__PRETTY_FUNCTION__.9863(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$46, %edx
	call	__GI___assert_fail
	.size	_dl_get_origin, .-_dl_get_origin
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9863, @object
	.size	__PRETTY_FUNCTION__.9863, 15
__PRETTY_FUNCTION__.9863:
	.string	"_dl_get_origin"
	.hidden	strlen
	.hidden	_rtld_local_ro
	.hidden	__rtld_malloc
