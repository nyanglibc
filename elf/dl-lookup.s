	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-lookup.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"version->filename == NULL || ! _dl_name_match_p (version->filename, map)"
	.text
	.p2align 4,,15
	.type	check_match, @function
check_match:
.LFB61:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movzbl	4(%r9), %eax
	movzwl	6(%r9), %r10d
	movq	64(%rsp), %rbp
	andl	$15, %eax
	cmpq	$0, 8(%r9)
	je	.L36
.L2:
	testw	%r10w, %r10w
	sete	%r10b
	movzbl	%r10b, %r10d
	testl	%r8d, %r10d
	jne	.L34
	movl	$1127, %r8d
	btl	%eax, %r8d
	jnc	.L34
	movq	%rsi, %rax
	movq	%r9, %rbx
	movl	%ecx, %r13d
	cmpq	%rax, %r9
	movq	%rdx, %r12
	movq	%rdi, %rsi
	je	.L5
	movl	(%r9), %edi
	addq	56(%rsp), %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L34
.L5:
	testq	%r12, %r12
	movq	840(%rbp), %rax
	je	.L6
	testq	%rax, %rax
	je	.L37
	movl	48(%rsp), %edx
	movzwl	(%rax,%rdx,2), %r13d
	movq	%r13, %rax
	andl	$32767, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	744(%rbp), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	8(%rax), %ebp
	cmpl	8(%r12), %ebp
	je	.L38
.L9:
	orl	12(%r12), %ebp
	jne	.L34
	testw	%r13w, %r13w
	js	.L34
.L8:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	testq	%rax, %rax
	je	.L8
	movl	48(%rsp), %edx
	movzwl	(%rax,%rdx,2), %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	andl	$32767, %ecx
	andl	$2, %r13d
	sete	%al
	addl	$2, %eax
	cmpl	%eax, %ecx
	jl	.L8
	testw	%dx, %dx
	js	.L34
	movq	80(%rsp), %rax
	movq	80(%rsp), %rcx
	movl	(%rax), %eax
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, (%rcx)
	jne	.L34
	movq	72(%rsp), %rax
	movq	%rbx, (%rax)
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
.L39:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%r12), %rdi
	testq	%rdi, %rdi
	je	.L8
	movq	%rbp, %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L8
	leaq	__PRETTY_FUNCTION__.10212(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$106, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%r12), %rsi
	movq	(%rax), %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L36:
	cmpb	$6, %al
	je	.L2
	cmpw	$-15, %r10w
	je	.L2
	xorl	%eax, %eax
	jmp	.L39
.LFE61:
	.size	check_match, .-check_match
	.section	.rodata.str1.1
.LC2:
	.string	"<main program>"
	.section	.rodata.str1.8
	.align 8
.LC3:
	.string	"symbol=%s;  lookup in file=%s [%lu]\n"
	.section	.rodata.str1.1
.LC4:
	.string	"out of memory\n"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"marking %s [%lu] as NODELETE due to unique symbol\n"
	.text
	.p2align 4,,15
	.type	do_lookup_x, @function
do_lookup_x:
.LFB66:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	movl	8(%r9), %eax
	movq	%rdi, 24(%rsp)
	movq	%rdx, 64(%rsp)
	movq	%rcx, 48(%rsp)
	movq	%r8, 56(%rsp)
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r10
	movl	208(%rsp), %r13d
	movq	%rax, %r15
	movq	%rsi, %rax
	movq	%r15, %r14
	movq	(%r9), %r12
	shrq	$6, %rax
	movq	%rsi, 32(%rsp)
	movq	%r10, %r15
	movq	%rax, 40(%rsp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L43:
	movl	756(%rbx), %esi
	testl	%esi, %esi
	je	.L102
	movq	112(%rbx), %rax
	movl	$0, 100(%rsp)
	movq	$0, 104(%rsp)
	movq	8(%rax), %r10
	movq	104(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rax, 16(%rsp)
	movq	768(%rbx), %rax
	testq	%rax, %rax
	je	.L45
	movl	40(%rsp), %edx
	andl	760(%rbx), %edx
	movq	32(%rsp), %rdi
	movl	764(%rbx), %ecx
	movq	(%rax,%rdx,8), %rax
	movq	%rdi, %rdx
	shrq	%cl, %rdx
	movq	%rdx, %rcx
	movq	%rax, %rdx
	shrq	%cl, %rdx
	movl	%edi, %ecx
	shrq	%cl, %rax
	andq	%rdx, %rax
	testb	$1, %al
	jne	.L203
.L102:
	addq	$1, %rbp
	cmpq	%rbp, %r14
	jbe	.L204
.L104:
	movq	(%r12,%rbp,8), %rax
	movq	40(%rax), %rbx
	cmpq	%r15, %rbx
	je	.L102
	movl	%r13d, %eax
	andl	$2, %eax
	movl	%eax, 8(%rsp)
	je	.L42
	testb	$3, 796(%rbx)
	je	.L102
.L42:
	testb	$32, 797(%rbx)
	jne	.L102
	testb	$8, _dl_debug_mask(%rip)
	je	.L43
	movq	8(%rbx), %rdx
	movq	48(%rbx), %rcx
	cmpb	$0, (%rdx)
	je	.L205
.L44:
	movq	24(%rsp), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L205:
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rdx
	leaq	.LC2(%rip), %rax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L204:
	addq	$120, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	64(%rsp), %rax
	movl	$4294967295, %edi
	movq	(%rax), %rax
	cmpq	%rdi, %rax
	je	.L51
	xorl	%edx, %edx
	divq	%rsi
	salq	$2, %rdx
.L52:
	movq	784(%rbx), %rax
	movl	(%rax,%rdx), %eax
	testl	%eax, %eax
	je	.L102
	leaq	100(%rsp), %rsi
	leaq	104(%rsp), %rdi
	movq	%r12, 72(%rsp)
	movq	%rbp, 176(%rsp)
	movq	%r14, 80(%rsp)
	movq	%r10, %rbp
	movq	%rbx, %r14
	movq	%r15, 200(%rsp)
	movq	%rsi, %r12
	movq	%rdi, %rbx
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L207:
	movq	776(%r14), %rax
	movl	(%rax,%r15,4), %eax
	testl	%eax, %eax
	je	.L206
.L60:
	movl	%eax, %r15d
	subq	$8, %rsp
	movl	%r13d, %r8d
	leaq	(%r15,%r15,2), %rdx
	pushq	%r12
	pushq	%rbx
	pushq	%r14
	pushq	48(%rsp)
	leaq	0(%rbp,%rdx,8), %r9
	pushq	%rax
	movl	240(%rsp), %ecx
	movq	232(%rsp), %rdx
	movq	96(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	check_match
	addq	$48, %rsp
	testq	%rax, %rax
	je	.L207
	movq	%r14, %rbx
	movq	%rbp, %r10
	movq	72(%rsp), %r12
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	%rax, %r8
	movq	80(%rsp), %r14
	.p2align 4,,10
	.p2align 3
.L48:
	cmpq	$0, 216(%rsp)
	je	.L208
.L62:
	movzbl	5(%r8), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L102
	movzbl	4(%r8), %eax
	shrb	$4, %al
	cmpb	$2, %al
	je	.L68
	cmpb	$10, %al
	je	.L69
	cmpb	$1, %al
	jne	.L102
.L101:
	movq	56(%rsp), %rax
	movq	%r8, (%rax)
	movq	%rbx, 8(%rax)
.L67:
.L71:
	addq	$120, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rsi
	movq	776(%rbx), %rax
	movl	(%rax,%rdx,4), %eax
	testl	%eax, %eax
	je	.L102
	movq	784(%rbx), %rdx
	leaq	104(%rsp), %rdi
	movq	%r12, 80(%rsp)
	movq	%rbp, 176(%rsp)
	movq	%r15, 200(%rsp)
	movq	%r14, 88(%rsp)
	movq	%rdi, 72(%rsp)
	movq	%rbx, %r14
	leaq	(%rdx,%rax,4), %r11
	leaq	100(%rsp), %rax
	movq	32(%rsp), %r12
	movq	%r10, %rbx
	movq	%r11, %rbp
	movq	%rax, %r15
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$4, %rbp
	andl	$1, %edx
	jne	.L209
.L49:
	movl	0(%rbp), %eax
	movq	%rax, %rdx
	xorq	%r12, %rax
	shrq	%rax
	jne	.L47
	movq	%rbp, %rax
	subq	784(%r14), %rax
	subq	$8, %rsp
	pushq	%r15
	pushq	88(%rsp)
	movl	%r13d, %r8d
	pushq	%r14
	pushq	48(%rsp)
	sarq	$2, %rax
	movl	%eax, %edx
	pushq	%rax
	movl	240(%rsp), %ecx
	leaq	(%rdx,%rdx,2), %rdx
	movq	96(%rsp), %rsi
	movq	72(%rsp), %rdi
	leaq	(%rbx,%rdx,8), %r9
	movq	232(%rsp), %rdx
	call	check_match
	addq	$48, %rsp
	testq	%rax, %rax
	jne	.L197
	movl	0(%rbp), %edx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L208:
	testb	$3, 796(%rbx)
	jne	.L62
	cmpl	$4, %r13d
	jne	.L62
	movq	120(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L62
	movq	128(%rbx), %rax
	testq	%rax, %rax
	je	.L62
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L62
	movabsq	$-6148914691236517205, %rsi
	movq	8(%rdx), %r11
	mulq	%rsi
	movq	%rdx, %rax
	shrq	$4, %rax
	testl	%eax, %eax
	je	.L62
	subl	$1, %eax
	movq	%r12, 72(%rsp)
	movq	%rbx, 88(%rsp)
	leaq	(%rax,%rax,2), %rax
	movq	%rbp, 176(%rsp)
	movq	%r8, 80(%rsp)
	movq	%r11, %rbx
	movq	%r10, %rbp
	leaq	24(%r11,%rax,8), %r9
	movq	%r9, %r12
	jmp	.L66
.L112:
	xorl	%eax, %eax
.L63:
	cmpq	$6, %rdx
	movl	$4, %esi
	jne	.L65
.L105:
	orl	%esi, %eax
	cmpl	$2, %eax
	jne	.L65
	shrq	$32, %rcx
	movq	24(%rsp), %rsi
	leaq	(%rcx,%rcx,2), %rax
	movl	0(%rbp,%rax,8), %edi
	addq	16(%rsp), %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L199
.L65:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	je	.L210
.L66:
	movq	8(%rbx), %rcx
	movl	%ecx, %edx
	cmpq	$36, %rdx
	ja	.L112
	movabsq	$68719935616, %rax
	shrq	%cl, %rax
	andl	$1, %eax
	cmpq	$5, %rdx
	jne	.L63
	orl	$2, %eax
	xorl	%esi, %esi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L68:
	movl	_dl_dynamic_weak(%rip), %ecx
	testl	%ecx, %ecx
	je	.L101
	movq	56(%rsp), %rax
	cmpq	$0, (%rax)
	jne	.L102
	movq	%r8, (%rax)
	movq	%rbx, 8(%rax)
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L69:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	32(%rsp), %r14
	movq	48(%rbx), %rbp
	je	.L72
	leaq	0(%rbp,%rbp,8), %rax
	leaq	_dl_ns(%rip), %rdi
	movq	%r8, 32(%rsp)
	leaq	0(%rbp,%rax,2), %rax
	leaq	40(%rdi,%rax,8), %rdi
	call	__pthread_mutex_lock@PLT
	movq	32(%rsp), %r8
.L72:
	leaq	0(%rbp,%rbp,8), %rax
	leaq	_dl_ns(%rip), %rsi
	leaq	0(%rbp,%rax,2), %rax
	leaq	(%rsi,%rax,8), %r12
	movq	80(%r12), %rax
	movq	88(%r12), %r9
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	je	.L73
	movq	%r14, %rax
	xorl	%edx, %edx
	leaq	-2(%r9), %rcx
	divq	%r9
	movq	%r14, %rax
	movq	%rbx, 64(%rsp)
	movq	%rbp, 72(%rsp)
	movq	%r14, %r13
	movq	%r8, 40(%rsp)
	movq	%rdx, %r15
	xorl	%edx, %edx
	divq	%rcx
	movq	%r15, %rbp
	movq	%r15, 80(%rsp)
	movq	%r9, %r15
	addq	$1, %rdx
	movq	%rdx, %rcx
	movq	%rdx, %rbx
	salq	$5, %rcx
	movq	%rcx, %r14
.L80:
	movq	%rbp, %r12
	salq	$5, %r12
	addq	32(%rsp), %r12
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L74:
	testq	%rdi, %rdi
	je	.L78
.L75:
	addq	%rbx, %rbp
	addq	%r14, %r12
	cmpq	%rbp, %r15
	jbe	.L211
.L79:
	movl	(%r12), %eax
	movq	8(%r12), %rdi
	cmpq	%rax, %r13
	jne	.L74
	movq	24(%rsp), %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L75
	movl	8(%rsp), %edx
	movq	40(%rsp), %r8
	movq	64(%rsp), %rbx
	movq	72(%rsp), %rbp
	testl	%edx, %edx
	je	.L76
	movq	56(%rsp), %rax
	movq	%r8, (%rax)
	movq	%rbx, 8(%rax)
.L77:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L71
	leaq	0(%rbp,%rbp,8), %rax
	leaq	_dl_ns(%rip), %rdi
	leaq	0(%rbp,%rax,2), %rax
	leaq	40(%rdi,%rax,8), %rdi
	call	__pthread_mutex_unlock@PLT
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L206:
	movq	%r14, %rbx
	movq	%rbp, %r10
	movq	72(%rsp), %r12
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	80(%rsp), %r14
.L50:
	cmpl	$1, 100(%rsp)
	jne	.L102
	movq	104(%rsp), %r8
	testq	%r8, %r8
	jne	.L48
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L78:
	movq	72(%rsp), %rbp
	leaq	_dl_ns(%rip), %rdi
	movq	%r15, %r9
	leaq	(%r9,%r9,2), %rcx
	movq	%rbx, %rdx
	movq	40(%rsp), %r8
	movq	64(%rsp), %rbx
	movq	80(%rsp), %r15
	movq	%r13, %r14
	leaq	0(%rbp,%rbp,8), %rax
	leaq	0(%rbp,%rax,2), %rax
	movq	96(%rdi,%rax,8), %rax
	salq	$2, %rax
	cmpq	%rax, %rcx
	jbe	.L212
.L81:
	movl	8(%rsp), %eax
	movl	(%r8), %ecx
	movq	%rdx, %rsi
	addq	16(%rsp), %rcx
	salq	$5, %rsi
	movq	32(%rsp), %rdi
	testl	%eax, %eax
	je	.L92
.L93:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L95:
	addq	%rdx, %r15
	addq	%rsi, %rax
	cmpq	%r15, %r9
	jbe	.L213
.L94:
	cmpq	$0, 8(%rax)
	jne	.L95
	movq	48(%rsp), %rsi
	movq	216(%rsp), %rdi
	movl	%r14d, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rsi, 16(%rax)
	movq	%rdi, 24(%rax)
.L96:
	leaq	0(%rbp,%rbp,8), %rax
	leaq	_dl_ns(%rip), %rsi
	leaq	0(%rbp,%rax,2), %rax
	salq	$3, %rax
	addq	$1, 96(%rsi,%rax)
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L101
	leaq	40(%rsi,%rax), %rdi
	movq	%r8, 8(%rsp)
	call	__pthread_mutex_unlock@PLT
	movq	8(%rsp), %r8
	jmp	.L101
.L214:
	subq	%r9, %r15
.L92:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rdi, %rax
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	addq	%rdx, %r15
	addq	%rsi, %rax
	cmpq	%r15, %r9
	jbe	.L214
.L97:
	cmpq	$0, 8(%rax)
	jne	.L98
	movl	%r14d, (%rax)
	movq	%rcx, 8(%rax)
	movq	%r8, 16(%rax)
	movq	%rbx, 24(%rax)
	movzbl	796(%rbx), %eax
	andl	$3, %eax
	cmpb	$2, %al
	jne	.L96
	cmpb	$0, 799(%rbx)
	jne	.L96
	testb	$8, 192(%rsp)
	je	.L99
	cmpb	$0, 800(%rbx)
	jne	.L96
	testb	$4, _dl_debug_mask(%rip)
	jne	.L215
.L107:
	movb	$1, 800(%rbx)
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L211:
	subq	%r15, %rbp
	jmp	.L80
.L213:
	subq	%r9, %r15
	jmp	.L93
.L209:
	movq	%rbx, %r10
	movq	80(%rsp), %r12
	movq	%r14, %rbx
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	88(%rsp), %r14
	jmp	.L50
.L51:
	movq	24(%rsp), %rax
	xorl	%edx, %edx
	movzbl	(%rax), %ecx
	testq	%rcx, %rcx
	je	.L53
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L202
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	movzbl	2(%rax), %eax
	testb	%al, %al
	je	.L202
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	movzbl	3(%rax), %eax
	testb	%al, %al
	je	.L202
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	movzbl	4(%rax), %eax
	testb	%al, %al
	je	.L202
	salq	$4, %rcx
	addq	%rax, %rcx
	movq	24(%rsp), %rax
	leaq	5(%rax), %rdx
	movzbl	5(%rax), %eax
	testb	%al, %al
	je	.L58
.L59:
	salq	$4, %rcx
	addq	$1, %rdx
	addq	%rcx, %rax
	movq	%rax, %rcx
	shrq	$24, %rcx
	andl	$240, %ecx
	xorq	%rax, %rcx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L59
.L58:
	andl	$268435455, %ecx
.L202:
	movq	%rcx, %rax
	xorl	%edx, %edx
	divq	%rsi
	salq	$2, %rdx
.L53:
	movq	64(%rsp), %rax
	movq	%rcx, (%rax)
	jmp	.L52
.L212:
	leaq	1(%r9), %rdi
	movq	%r8, 72(%rsp)
	movq	%r9, 64(%rsp)
	call	_dl_higher_prime_number
	movl	$32, %edi
	movq	%rax, %rsi
	movq	%rax, %r12
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 40(%rsp)
	je	.L82
	movq	64(%rsp), %r9
	leaq	-2(%r12), %rax
	movq	72(%rsp), %r8
	movq	%rax, 24(%rsp)
	testq	%r9, %r9
	je	.L84
	movq	32(%rsp), %rdi
	movq	%r9, %rax
	movq	%r8, 64(%rsp)
	salq	$5, %rax
	movq	40(%rsp), %r8
	leaq	(%rax,%rdi), %r15
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L86
	movl	(%rsi), %r13d
	xorl	%edx, %edx
	movq	24(%rsi), %r10
	movq	16(%rsi), %r11
	movq	%r13, %rax
	movq	%r13, %r9
	divq	%r12
	movq	%r13, %rax
	movq	%rdx, %rcx
	xorl	%edx, %edx
	divq	24(%rsp)
	addq	$1, %rdx
	movq	%rdx, %r13
	salq	$5, %r13
.L87:
	movq	%rcx, %rax
	salq	$5, %rax
	addq	%r8, %rax
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	addq	%rdx, %rcx
	addq	%r13, %rax
	cmpq	%rcx, %r12
	jbe	.L216
.L88:
	cmpq	$0, 8(%rax)
	jne	.L89
	movl	%r9d, (%rax)
	movq	%rdi, 8(%rax)
	movq	%r11, 16(%rax)
	movq	%r10, 24(%rax)
.L86:
	addq	$32, %rsi
	cmpq	%rsi, %r15
	jne	.L90
	movq	64(%rsp), %r8
.L84:
	leaq	0(%rbp,%rbp,8), %rax
	leaq	_dl_ns(%rip), %rdi
	movq	%r8, 64(%rsp)
	leaq	0(%rbp,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	32(%rsp), %rdi
	call	*104(%r13)
	movq	free@GOTPCREL(%rip), %rax
	xorl	%edx, %edx
	movq	40(%rsp), %rsi
	movq	%r12, 88(%r13)
	movq	%r12, %r9
	movq	64(%rsp), %r8
	movq	%rax, 104(%r13)
	movq	%r14, %rax
	movq	%rsi, 80(%r13)
	divq	%r12
	movq	%r14, %rax
	movq	%rsi, 32(%rsp)
	movq	%rdx, %r15
	xorl	%edx, %edx
	divq	24(%rsp)
	addq	$1, %rdx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L216:
	subq	%r12, %rcx
	jmp	.L87
.L73:
	movl	$31, %esi
	movl	$32, %edi
	movq	%r8, 24(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	je	.L82
	movq	%rax, 80(%r12)
	movq	free@GOTPCREL(%rip), %rax
	movabsq	$595056260442243601, %rdx
	movq	%r14, %r15
	movq	$31, 88(%r12)
	movl	$31, %r9d
	movq	24(%rsp), %r8
	movq	%rax, 104(%r12)
	movq	%r14, %rax
	mulq	%rdx
	subq	%rdx, %r15
	shrq	%r15
	addq	%rdx, %r15
	movabsq	$5088756985850910791, %rdx
	shrq	$4, %r15
	movq	%r15, %rax
	salq	$5, %rax
	subq	%r15, %rax
	movq	%r14, %r15
	subq	%rax, %r15
	movq	%r14, %rax
	imulq	%rdx
	movq	%rdx, %rax
	movq	%r14, %rdx
	sarq	$63, %rdx
	sarq	$3, %rax
	subq	%rdx, %rax
	leaq	0(,%rax,8), %rdx
	subq	%rax, %rdx
	leaq	(%rax,%rdx,4), %rax
	movq	%r14, %rdx
	subq	%rax, %rdx
	addq	$1, %rdx
	jmp	.L81
.L197:
	movq	%rbx, %r10
	movq	%rax, %r8
	movq	%r14, %rbx
	movq	80(%rsp), %r12
	movq	176(%rsp), %rbp
	movq	200(%rsp), %r15
	movq	88(%rsp), %r14
	jmp	.L48
.L76:
	movq	16(%r12), %rax
	movq	56(%rsp), %rsi
	movq	%rax, (%rsi)
	movq	24(%r12), %rax
	movq	%rax, 8(%rsi)
	jmp	.L77
.L99:
	testb	$4, _dl_debug_mask(%rip)
	jne	.L217
.L109:
	movb	$1, 799(%rbx)
	jmp	.L96
.L210:
	movq	80(%rsp), %r8
	movq	72(%rsp), %r12
	movq	88(%rsp), %rbx
	movq	176(%rsp), %rbp
	jmp	.L62
.L199:
	movq	72(%rsp), %r12
	movq	176(%rsp), %rbp
	jmp	.L102
.L215:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, 8(%rsp)
	call	_dl_debug_printf
	movq	8(%rsp), %r8
	jmp	.L107
.L82:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L85
	imulq	$152, %rbp, %rbp
	leaq	_dl_ns(%rip), %rax
	leaq	40(%rax,%rbp), %rdi
	call	__pthread_mutex_unlock@PLT
.L85:
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
.L217:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, 8(%rsp)
	call	_dl_debug_printf
	movq	8(%rsp), %r8
	jmp	.L109
.LFE66:
	.size	do_lookup_x, .-do_lookup_x
	.section	.rodata.str1.1
.LC6:
	.string	", version "
.LC7:
	.string	""
.LC8:
	.string	"protected"
.LC9:
	.string	"normal"
	.section	.rodata.str1.8
	.align 8
.LC10:
	.string	"version == NULL || !(flags & DL_LOOKUP_RETURN_NEWEST)"
	.section	.rodata.str1.1
.LC11:
	.string	"undefined symbol: %s%s%s"
.LC12:
	.string	"symbol lookup error"
	.section	.rodata.str1.8
	.align 8
.LC13:
	.string	"marking %s [%lu] as NODELETE due to reference to main program\n"
	.align 8
.LC14:
	.string	"marking %s [%lu] as NODELETE due to reference to %s [%lu]\n"
	.align 8
.LC15:
	.string	"\nfile=%s [%lu];  needed by %s [%lu] (relocation dependency)\n\n"
	.align 8
.LC16:
	.string	"binding file %s [%lu] to %s [%lu]: %s symbol `%s'"
	.section	.rodata.str1.1
.LC17:
	.string	" [%s]\n"
.LC18:
	.string	"\n"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"marking %s [%lu] as NODELETE due to memory allocation failure\n"
	.text
	.p2align 4,,15
	.globl	_dl_lookup_symbol_x
	.hidden	_dl_lookup_symbol_x
	.type	_dl_lookup_symbol_x, @function
_dl_lookup_symbol_x:
.LFB69:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%r8, %r12
	movl	%r9d, %r14d
	subq	$136, %rsp
	movzbl	(%rdi), %edx
	movq	%rcx, 24(%rsp)
	testb	%dl, %dl
	je	.L327
	movq	%rdi, %rcx
	movl	$5381, %ebx
	.p2align 4,,10
	.p2align 3
.L220:
	movq	%rbx, %rax
	addq	$1, %rcx
	salq	$5, %rax
	addq	%rax, %rbx
	addq	%rdx, %rbx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L220
	movl	%ebx, %eax
	movq	%rax, (%rsp)
.L219:
	movl	$4294967295, %eax
	testq	%r12, %r12
	movq	$0, 80(%rsp)
	movq	%rax, 72(%rsp)
	movq	$0, 88(%rsp)
	je	.L221
	testb	$2, 192(%rsp)
	jne	.L499
.L221:
	cmpq	$0, 200(%rsp)
	movq	24(%rsp), %rax
	movq	(%rax), %r9
	jne	.L500
	testq	%r9, %r9
	movq	0(%rbp), %rcx
	je	.L225
	movq	$0, 32(%rsp)
	xorl	%eax, %eax
.L326:
	leaq	80(%rsp), %rsi
	movq	24(%rsp), %rbx
	movq	%rsi, 16(%rsp)
	leaq	72(%rsp), %rsi
	movq	%rsi, 8(%rsp)
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L501:
	addq	$8, %rbx
	movq	(%rbx), %r9
	xorl	%eax, %eax
	movq	0(%rbp), %rcx
	testq	%r9, %r9
	je	.L228
.L227:
	pushq	%r13
	pushq	%r14
	pushq	216(%rsp)
	movl	216(%rsp), %edi
	pushq	%rdi
	pushq	%r12
	movq	%r15, %rdi
	pushq	%rax
	movq	64(%rsp), %r8
	movq	56(%rsp), %rdx
	movq	48(%rsp), %rsi
	call	do_lookup_x
	addq	$48, %rsp
	testl	%eax, %eax
	je	.L501
	movq	0(%rbp), %rcx
.L228:
	cmpq	$0, 80(%rsp)
	je	.L225
	testq	%rcx, %rcx
	je	.L238
	movzbl	5(%rcx), %eax
	andl	$3, %eax
	cmpb	$3, %al
	je	.L502
.L238:
	movq	88(%rsp), %rbx
	xorl	%r9d, %r9d
.L237:
	movzbl	796(%rbx), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L503
.L249:
	movl	996(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L504
.L306:
	movl	_dl_debug_mask(%rip), %eax
	testl	$2052, %eax
	jne	.L505
.L307:
	movq	80(%rsp), %rax
	movq	%rax, 0(%rbp)
.L218:
	addq	$136, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L502:
	cmpl	$1, %r14d
	je	.L506
	movq	24(%rsp), %rax
	movq	$0, 96(%rsp)
	movq	$0, 104(%rsp)
	movq	(%rax), %r9
	testq	%r9, %r9
	je	.L248
	movq	24(%rsp), %r10
	leaq	96(%rsp), %rax
	cmpl	$4, %r14d
	movq	32(%rsp), %rdx
	movl	%r14d, 40(%rsp)
	sete	%bl
	movq	%r13, 32(%rsp)
	movq	%rax, 16(%rsp)
	movq	%r10, %r13
	movl	192(%rsp), %r14d
	jmp	.L246
	.p2align 4,,10
	.p2align 3
.L507:
	testb	%bl, %bl
	movl	$4, %eax
	je	.L342
.L243:
	pushq	$0
	pushq	%rax
	movq	%r15, %rdi
	pushq	216(%rsp)
	pushq	%r14
	pushq	%r12
	pushq	%rdx
	movq	64(%rsp), %r8
	movq	56(%rsp), %rdx
	movq	48(%rsp), %rsi
	call	do_lookup_x
	addq	$48, %rsp
	testl	%eax, %eax
	jne	.L245
	addq	$8, %r13
	movq	0(%r13), %r9
	testq	%r9, %r9
	je	.L245
	movq	0(%rbp), %rcx
	xorl	%edx, %edx
.L246:
	movzbl	4(%rcx), %eax
	andl	$15, %eax
	cmpb	$1, %al
	je	.L507
.L342:
	movl	$1, %eax
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L327:
	movq	$5381, (%rsp)
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L225:
	testq	%rcx, %rcx
	je	.L230
	movzbl	4(%rcx), %eax
	shrb	$4, %al
	cmpb	$2, %al
	je	.L231
.L230:
	testb	$1, 1+_dl_debug_mask(%rip)
	je	.L508
.L231:
	movq	$0, 0(%rbp)
	xorl	%ebx, %ebx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L500:
	movq	(%r9), %rdx
	movq	200(%rsp), %rax
	cmpq	%rax, (%rdx)
	movl	$0, %eax
	je	.L223
	movq	200(%rsp), %rcx
	.p2align 4,,10
	.p2align 3
.L224:
	addq	$1, %rax
	cmpq	%rcx, (%rdx,%rax,8)
	jne	.L224
.L223:
	movq	0(%rbp), %rcx
	movq	%rax, 32(%rsp)
	jmp	.L326
	.p2align 4,,10
	.p2align 3
.L505:
	testb	$4, %al
	je	.L307
	movq	8(%rbx), %rcx
	testl	%r9d, %r9d
	leaq	.LC8(%rip), %rax
	leaq	.LC9(%rip), %r9
	movq	48(%rbx), %r8
	cmovne	%rax, %r9
	cmpb	$0, (%rcx)
	jne	.L309
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rcx
	leaq	.LC2(%rip), %rax
	testq	%rcx, %rcx
	cmove	%rax, %rcx
.L309:
	movq	8(%r13), %rsi
	movq	48(%r13), %rdx
	cmpb	$0, (%rsi)
	jne	.L310
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC2(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
.L310:
	subq	$8, %rsp
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	pushq	%r15
	call	_dl_debug_printf
	testq	%r12, %r12
	popq	%rax
	popq	%rdx
	je	.L311
	movq	(%r12), %rsi
	leaq	.LC17(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf_c
	movq	88(%rsp), %rbx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L504:
	movl	$1, 996(%rbx)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L503:
	testb	$1, 192(%rsp)
	je	.L249
	cmpq	%rbx, %r13
	je	.L249
	cmpb	$0, 799(%rbx)
	jne	.L249
	movl	192(%rsp), %eax
	andl	$8, %eax
	movl	%eax, 16(%rsp)
	je	.L250
	cmpb	$0, 800(%rbx)
	jne	.L249
.L250:
	movq	984(%r13), %rax
	movq	%rax, 32(%rsp)
	movq	976(%r13), %r8
	testq	%r8, %r8
	je	.L313
	movq	(%r8), %rdx
	testq	%rdx, %rdx
	je	.L313
	xorl	%eax, %eax
	jmp	.L487
.L251:
	leal	1(%rax), %edx
	movq	%rdx, %rax
	movq	(%r8,%rdx,8), %rdx
	testq	%rdx, %rdx
	je	.L313
.L487:
	cmpq	%rbx, %rdx
	jne	.L251
.L301:
	movq	88(%rsp), %rbx
	jmp	.L249
.L245:
	cmpq	$0, 96(%rsp)
	movq	32(%rsp), %r13
	movl	40(%rsp), %r14d
	je	.L248
	cmpq	%r13, 104(%rsp)
	je	.L248
	movq	0(%rbp), %rax
	movq	%r13, 88(%rsp)
	movq	%r13, %rbx
	movq	%rax, 80(%rsp)
.L242:
	movl	$1, %r9d
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L506:
	cmpq	%r13, 88(%rsp)
	je	.L240
	movq	%rcx, 80(%rsp)
	movq	%r13, 88(%rsp)
.L240:
	movq	%r13, %rbx
	movl	$1, %r9d
	jmp	.L249
.L248:
	movq	88(%rsp), %rbx
	jmp	.L242
.L311:
	leaq	.LC18(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf_c
	movq	88(%rsp), %rbx
	jmp	.L307
.L313:
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L331
	movl	(%rax), %esi
	testl	%esi, %esi
	movl	%esi, (%rsp)
	je	.L253
	cmpq	%rbx, 8(%rax)
	je	.L301
	movl	(%rsp), %esi
	movq	32(%rsp), %rax
	leal	-1(%rsi), %edx
	addq	$16, %rax
	leaq	(%rax,%rdx,8), %rdx
	jmp	.L255
.L256:
	addq	$8, %rax
	cmpq	%rbx, -8(%rax)
	je	.L301
.L255:
	cmpq	%rax, %rdx
	jne	.L256
.L253:
	movq	1152(%rbx), %rax
	movq	%rax, 40(%rsp)
	movl	192(%rsp), %eax
	andl	$4, %eax
	movl	%eax, 8(%rsp)
	jne	.L509
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L265
	leaq	_dl_load_lock(%rip), %rdi
	movl	%r9d, 32(%rsp)
	call	__pthread_mutex_lock@PLT
	movl	32(%rsp), %r9d
.L265:
	movq	48(%r13), %r8
	leaq	(%r8,%r8,8), %rax
	leaq	(%r8,%rax,2), %rdx
	leaq	_dl_ns(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	jne	.L480
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L510:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L272
.L480:
	cmpq	%rbx, %rax
	jne	.L510
.L272:
	testq	%rax, %rax
	je	.L511
	movq	1152(%rbx), %rdx
	cmpq	%rdx, 40(%rsp)
	je	.L512
.L263:
	xorl	%ebx, %ebx
	cmpq	%rdx, 40(%rsp)
	setne	%bl
	negl	%ebx
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L305
	leaq	_dl_load_lock(%rip), %rdi
	movl	%r9d, 16(%rsp)
	movq	%rdx, (%rsp)
	call	__pthread_mutex_unlock@PLT
	movl	16(%rsp), %r9d
	movq	(%rsp), %rdx
.L305:
	movl	8(%rsp), %r8d
	testl	%r8d, %r8d
	jne	.L325
	cmpq	%rdx, 40(%rsp)
	je	.L301
.L277:
	pushq	200(%rsp)
	movl	200(%rsp), %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	movl	%r14d, %r9d
	movq	%r12, %r8
	movq	%rbp, %rdx
	pushq	%rax
	movq	40(%rsp), %rcx
	call	_dl_lookup_symbol_x
	popq	%rsi
	movq	%rax, %rbx
	popq	%rdi
	jmp	.L218
.L331:
	movl	$0, (%rsp)
	jmp	.L253
.L512:
	cmpb	$0, 799(%rbx)
	jne	.L498
	cmpl	$0, 16(%rsp)
	je	.L282
	cmpb	$0, 800(%rbx)
	jne	.L498
	movzbl	796(%r13), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L321
	testb	$4, _dl_debug_mask(%rip)
	jne	.L285
.L288:
	movb	$1, 800(%rbx)
.L498:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L281
	movl	%r9d, (%rsp)
.L493:
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
	movl	8(%rsp), %r9d
	testl	%r9d, %r9d
	movl	(%rsp), %r9d
	je	.L301
#APP
# 806 "dl-lookup.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L511:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L276
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
	cmpl	$0, 8(%rsp)
	je	.L277
.L485:
#APP
# 806 "dl-lookup.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
.L278:
	movq	920(%r13), %rax
	movq	%rax, 24(%rsp)
	jmp	.L277
.L509:
	xorl	%eax, %eax
#APP
# 640 "dl-lookup.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	je	.L513
.L258:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L259
	leaq	_dl_load_lock(%rip), %rdi
	movq	%r8, 56(%rsp)
	movl	%r9d, 52(%rsp)
	call	__pthread_mutex_lock@PLT
	movq	56(%rsp), %r8
	movl	52(%rsp), %r9d
.L259:
	movq	976(%r13), %rdx
	movq	%rbx, %rax
	movq	%rax, %rbx
	cmpq	%r8, %rdx
	je	.L260
	testq	%rdx, %rdx
	je	.L260
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L260
	cmpq	%rcx, %rax
	je	.L261
	xorl	%ecx, %ecx
.L262:
	leal	1(%rcx), %esi
	movq	%rsi, %rcx
	movq	(%rdx,%rsi,8), %rsi
	testq	%rsi, %rsi
	je	.L260
	cmpq	%rsi, %rax
	jne	.L262
.L484:
	movq	1152(%rax), %rdx
	jmp	.L263
.L282:
	movzbl	796(%r13), %eax
	andl	$3, %eax
	cmpb	$2, %al
	je	.L321
	testb	$4, _dl_debug_mask(%rip)
	jne	.L285
.L495:
	movb	$1, 799(%rbx)
	jmp	.L498
.L321:
	movl	992(%r13), %eax
	cmpl	(%rsp), %eax
	jbe	.L514
	movl	(%rsp), %edx
	movq	984(%r13), %rax
	movq	%rdx, %rsi
	movq	%rbx, 8(%rax,%rdx,8)
	movq	984(%r13), %rdx
	leal	1(%rsi), %eax
	movl	%eax, (%rdx)
.L298:
	testb	$64, _dl_debug_mask(%rip)
	je	.L498
	movq	8(%r13), %rcx
	movq	48(%r13), %r8
	cmpb	$0, (%rcx)
	jne	.L299
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.L299
	leaq	.LC2(%rip), %rcx
.L299:
	movq	8(%rbx), %rsi
	movq	48(%rbx), %rdx
	cmpb	$0, (%rsi)
	jne	.L300
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L300
	leaq	.LC2(%rip), %rsi
.L300:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	movl	%r9d, (%rsp)
	call	_dl_debug_printf
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	(%rsp), %r9d
	jne	.L493
.L281:
	cmpl	$0, 8(%rsp)
	je	.L301
	xorl	%ebx, %ebx
.L325:
#APP
# 806 "dl-lookup.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	addl	$1, %ebx
	je	.L278
	jmp	.L301
.L276:
	cmpl	$0, 8(%rsp)
	je	.L277
	jmp	.L485
.L260:
	movq	984(%r13), %rdx
	testq	%rdx, %rdx
	je	.L265
	cmpq	%rdx, 32(%rsp)
	je	.L266
	movl	(%rdx), %esi
	testl	%esi, %esi
	movl	%esi, (%rsp)
	je	.L265
	cmpq	%rax, 8(%rdx)
	je	.L484
	movl	(%rsp), %esi
	addq	$16, %rdx
	leal	-1(%rsi), %ecx
	leaq	(%rdx,%rcx,8), %rcx
.L268:
	cmpq	%rcx, %rdx
	je	.L265
	addq	$8, %rdx
	cmpq	-8(%rdx), %rax
	jne	.L268
	jmp	.L484
.L513:
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 640 "dl-lookup.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L258
.L285:
	movq	8(%r13), %rcx
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	cmpb	$0, (%rcx)
	jne	.L515
	leaq	.LC13(%rip), %rdi
	xorl	%eax, %eax
	movl	%r9d, (%rsp)
	call	_dl_debug_printf
	movl	(%rsp), %r9d
.L292:
	cmpl	$0, 16(%rsp)
	jne	.L288
	jmp	.L495
.L515:
	leaq	.LC14(%rip), %rdi
	xorl	%eax, %eax
	movl	%r9d, (%rsp)
	call	_dl_debug_printf
	movl	(%rsp), %r9d
	jmp	.L292
.L508:
	testq	%r13, %r13
	je	.L232
	testq	%r12, %r12
	movq	8(%r13), %rsi
	je	.L516
	movq	(%r12), %r9
	leaq	.LC6(%rip), %r8
	testq	%r9, %r9
	jne	.L312
	leaq	.LC7(%rip), %r9
.L312:
	cmpb	$0, (%rsi)
	jne	.L234
.L319:
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L234
	leaq	.LC2(%rip), %rsi
.L234:
	leaq	96(%rsp), %rbx
	leaq	.LC11(%rip), %rdx
	movq	%r15, %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	_dl_exception_create_format@PLT
	leaq	.LC12(%rip), %rdx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	_dl_signal_exception
.L516:
	leaq	.LC7(%rip), %r8
	movq	%r8, %r9
	jmp	.L312
.L232:
	testq	%r12, %r12
	je	.L517
	movq	(%r12), %r9
	leaq	.LC6(%rip), %r8
	testq	%r9, %r9
	jne	.L319
	leaq	.LC7(%rip), %r9
	jmp	.L319
.L499:
	leaq	__PRETTY_FUNCTION__.10432(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$845, %edx
	call	__assert_fail
.L517:
	leaq	.LC7(%rip), %r8
	movq	%r8, %r9
	jmp	.L319
.L514:
	testl	%eax, %eax
	movl	$10, 24(%rsp)
	je	.L290
	addl	%eax, %eax
	movl	%eax, 24(%rsp)
.L290:
	movl	24(%rsp), %eax
	movl	%r9d, 32(%rsp)
	leaq	8(,%rax,8), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movl	32(%rsp), %r9d
	je	.L518
	movl	(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L297
	movq	984(%r13), %rax
	leaq	8(%rcx), %rdi
	leaq	0(,%r8,8), %rdx
	movl	%r9d, 40(%rsp)
	movq	%rcx, 32(%rsp)
	movq	%r8, 16(%rsp)
	leaq	8(%rax), %rsi
	call	memcpy@PLT
	movl	40(%rsp), %r9d
	movq	32(%rsp), %rcx
	movq	16(%rsp), %r8
.L297:
	movl	(%rsp), %eax
	movq	%rbx, 8(%rcx,%r8,8)
	addl	$1, %eax
	movl	%eax, (%rcx)
	movq	984(%r13), %rdi
	movl	24(%rsp), %eax
	movq	%rcx, 984(%r13)
	testq	%rdi, %rdi
	movl	%eax, 992(%r13)
	je	.L298
	movl	%r9d, (%rsp)
	call	_dl_scope_free
	movl	(%rsp), %r9d
	jmp	.L298
.L518:
	testb	$4, _dl_debug_mask(%rip)
	je	.L292
	cmpl	$0, 16(%rsp)
	je	.L293
	cmpb	$0, 800(%rbx)
	jne	.L288
.L293:
	movq	48(%rbx), %rdx
	movq	8(%rbx), %rsi
	leaq	.LC19(%rip), %rdi
	xorl	%eax, %eax
	movl	%r9d, (%rsp)
	call	_dl_debug_printf
	movl	(%rsp), %r9d
	jmp	.L292
.L261:
	movq	1152(%rax), %rdx
	jmp	.L263
.L266:
	movq	32(%rsp), %rsi
	movl	(%rsp), %edi
	movl	(%rsi), %ecx
	cmpl	%edi, %ecx
	jbe	.L265
	movl	%edi, %edx
	leaq	8(%rsi,%rdx,8), %rsi
	xorl	%edx, %edx
.L271:
	cmpq	(%rsi,%rdx,8), %rax
	je	.L484
	movl	(%rsp), %edi
	addq	$1, %rdx
	addl	%edx, %edi
	cmpl	%edi, %ecx
	ja	.L271
	movl	%ecx, (%rsp)
	jmp	.L265
.LFE69:
	.size	_dl_lookup_symbol_x, .-_dl_lookup_symbol_x
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"(bitmask_nwords & (bitmask_nwords - 1)) == 0"
	.text
	.p2align 4,,15
	.globl	_dl_setup_hash
	.hidden	_dl_setup_hash
	.type	_dl_setup_hash, @function
_dl_setup_hash:
.LFB70:
	movq	672(%rdi), %rax
	testq	%rax, %rax
	je	.L520
	movq	8(%rax), %rcx
	movl	(%rcx), %edx
	movl	%edx, 756(%rdi)
	movl	8(%rcx), %eax
	movl	4(%rcx), %esi
	leal	-1(%rax), %r8d
	testl	%eax, %r8d
	jne	.L529
	movl	%r8d, 760(%rdi)
	leaq	16(%rcx), %r8
	addl	%eax, %eax
	movl	12(%rcx), %ecx
	subq	%rsi, %rdx
	leaq	(%r8,%rax,4), %rax
	movq	%r8, 768(%rdi)
	movq	%rax, 776(%rdi)
	leaq	(%rax,%rdx,4), %rax
	movl	%ecx, 764(%rdi)
	movq	%rax, 784(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L520:
	movq	96(%rdi), %rax
	testq	%rax, %rax
	je	.L519
	movq	8(%rax), %rax
	movl	(%rax), %edx
	addq	$8, %rax
	movq	%rax, 784(%rdi)
	leaq	(%rax,%rdx,4), %rax
	movl	%edx, 756(%rdi)
	movq	%rax, 776(%rdi)
.L519:
	rep ret
.L529:
	leaq	__PRETTY_FUNCTION__.10457(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	subq	$8, %rsp
	movl	$966, %edx
	call	__assert_fail
.LFE70:
	.size	_dl_setup_hash, .-_dl_setup_hash
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10457, @object
	.size	__PRETTY_FUNCTION__.10457, 15
__PRETTY_FUNCTION__.10457:
	.string	"_dl_setup_hash"
	.align 8
	.type	__PRETTY_FUNCTION__.10212, @object
	.size	__PRETTY_FUNCTION__.10212, 12
__PRETTY_FUNCTION__.10212:
	.string	"check_match"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10432, @object
	.size	__PRETTY_FUNCTION__.10432, 20
__PRETTY_FUNCTION__.10432:
	.string	"_dl_lookup_symbol_x"
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	_dl_scope_free
	.hidden	_dl_signal_exception
	.hidden	_dl_debug_printf_c
	.hidden	_dl_higher_prime_number
	.hidden	_dl_debug_printf
	.hidden	__assert_fail
	.hidden	_dl_name_match_p
	.hidden	strcmp
