	.text
	.p2align 4,,15
	.type	tunable_initialize, @function
tunable_initialize:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$16, %rsp
	cmpl	$3, 48(%rbx)
	jne	.L34
	movb	$1, 80(%rbx)
.L4:
	movq	%rdi, 72(%rbx)
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%esi, %esi
	call	_dl_strtoul
	movl	48(%rbx), %edx
	cmpl	$3, %edx
	je	.L35
	cmpl	$1, %edx
	je	.L6
	jnb	.L6
	cmpq	%rax, 56(%rbx)
	jg	.L1
	cmpq	%rax, 64(%rbx)
	jge	.L31
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	56(%rbx), %rax
	jb	.L1
	cmpq	64(%rbx), %rax
	ja	.L1
.L31:
	movq	%rax, 72(%rbx)
	movb	$1, 80(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
.L35:
	leaq	8(%rsp), %rdi
	jmp	.L4
	.size	tunable_initialize, .-tunable_initialize
	.p2align 4,,15
	.globl	__GI___tunable_set_val
	.hidden	__GI___tunable_set_val
	.type	__GI___tunable_set_val, @function
__GI___tunable_set_val:
	movl	%edi, %edi
	leaq	tunable_list(%rip), %r9
	leaq	0(,%rdi,8), %rax
	subq	%rdi, %rax
	salq	$4, %rax
	addq	%r9, %rax
	movl	48(%rax), %r8d
	cmpl	$3, %r8d
	je	.L37
	cmpl	$1, %r8d
	movq	(%rsi), %rsi
	movq	64(%rax), %r10
	movq	56(%rax), %r8
	je	.L39
	jnb	.L39
	testq	%rdx, %rdx
	je	.L42
	testq	%rcx, %rcx
	movq	(%rdx), %rdx
	je	.L43
	movq	(%rcx), %rcx
	cmpq	%rcx, %rdx
	jg	.L44
	cmpq	%r10, %rcx
	jg	.L44
	cmpq	%r8, %rdx
	jl	.L44
	movq	%rdx, 56(%rax)
	movq	%rcx, 64(%rax)
	movq	%rcx, %r10
	movq	%rdx, %r8
	.p2align 4,,10
	.p2align 3
.L44:
	cmpq	%r8, %rsi
	jl	.L36
	cmpq	%r10, %rsi
	jle	.L82
.L36:
	rep ret
	.p2align 4,,10
	.p2align 3
.L39:
	testq	%rdx, %rdx
	je	.L50
	testq	%rcx, %rcx
	movq	(%rdx), %rdx
	je	.L51
	movq	(%rcx), %rcx
	cmpq	%rcx, %rdx
	jbe	.L105
.L52:
	cmpq	%r8, %rsi
	jb	.L36
	cmpq	%r10, %rsi
	ja	.L36
.L82:
	leaq	0(,%rdi,8), %rax
	subq	%rdi, %rax
	salq	$4, %rax
	addq	%r9, %rax
	movq	%rsi, 72(%rax)
	movb	$1, 80(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rsi, 72(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	cmpq	%r10, %rcx
	ja	.L52
	cmpq	%r8, %rdx
	jb	.L52
	movq	%rdx, 56(%rax)
	movq	%rcx, 64(%rax)
	movq	%rcx, %r10
	movq	%rdx, %r8
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L42:
	testq	%rcx, %rcx
	je	.L44
	movq	(%rcx), %rdx
	cmpq	%r10, %rdx
	jge	.L44
	cmpq	%r8, %rdx
	jl	.L44
	movq	%rdx, 64(%rax)
	movq	%rdx, %r10
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L50:
	testq	%rcx, %rcx
	je	.L52
	movq	(%rcx), %rdx
	cmpq	%r10, %rdx
	jnb	.L52
	cmpq	%r8, %rdx
	jb	.L52
	movq	%rdx, 64(%rax)
	movq	%rdx, %r10
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L51:
	cmpq	%r8, %rdx
	jbe	.L52
	cmpq	%r10, %rdx
	ja	.L52
	movq	%rdx, 56(%rax)
	movq	%rdx, %r8
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L43:
	cmpq	%r8, %rdx
	jle	.L44
	cmpq	%r10, %rdx
	jg	.L44
	movq	%rdx, 56(%rax)
	movq	%rdx, %r8
	jmp	.L44
	.size	__GI___tunable_set_val, .-__GI___tunable_set_val
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"GLIBC_TUNABLES"
.LC1:
	.string	"/etc/suid-debug"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"sbrk() failure while processing tunables\n"
	.text
	.p2align 4,,15
	.globl	__GI___tunables_init
	.hidden	__GI___tunables_init
	.type	__GI___tunables_init, @function
__GI___tunables_init:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	__GI___libc_enable_secure(%rip), %edx
	testl	%edx, %edx
	je	.L108
	xorl	%esi, %esi
	leaq	.LC1(%rip), %rdi
	movl	$21, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/not-errno.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L178
	testl	%eax, %eax
	jne	.L108
.L178:
	movl	$2, 3444+tunable_list(%rip)
.L108:
	leaq	tunable_list(%rip), %rdi
	movq	$-88, %rax
	subq	%rdi, %rax
	movq	%rax, %r14
.L111:
	testq	%rbx, %rbx
	je	.L106
.L288:
	movq	(%rbx), %rbp
	testq	%rbp, %rbp
	je	.L106
	movzbl	0(%rbp), %r8d
	leaq	8(%rbx), %r12
	cmpb	$61, %r8b
	je	.L174
.L290:
	testb	%r8b, %r8b
	je	.L174
	movl	$1, %eax
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L284:
	cmpb	$61, %dl
	je	.L157
	movq	%rcx, %rax
.L158:
	movzbl	0(%rbp,%rax), %edx
	leaq	1(%rax), %rcx
	testb	%dl, %dl
	jne	.L284
.L157:
	testb	%dl, %dl
	je	.L160
	leaq	1(%rax), %r15
	testq	%r12, %r12
	leaq	0(%rbp,%r15), %r13
	je	.L106
	cmpb	$71, %r8b
	leaq	1(%rbp), %rax
	leaq	1+.LC0(%rip), %rsi
	movl	$76, %edx
	jne	.L113
	.p2align 4,,10
	.p2align 3
.L165:
	movzbl	(%rax), %ecx
	cmpb	%dl, %cl
	jne	.L113
	testb	%cl, %cl
	je	.L113
	addq	$1, %rsi
	movzbl	(%rsi), %edx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L165
	cmpb	$61, (%rax)
	je	.L285
.L113:
	leaq	88+tunable_list(%rip), %rdi
	movl	__GI___libc_enable_secure(%rip), %r10d
	leaq	3472(%rdi), %r8
	.p2align 4,,10
	.p2align 3
.L155:
	cmpb	$0, -8(%rdi)
	jne	.L142
	cmpb	$0, (%rdi)
	je	.L142
	movzbl	(%rdi), %edx
	leaq	(%r14,%rdi), %r9
	movzbl	0(%rbp), %eax
	testb	%dl, %dl
	je	.L143
	testb	%al, %al
	je	.L142
	cmpb	%al, %dl
	jne	.L142
	movq	%rdi, %rcx
	movq	%rbp, %rdx
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L286:
	cmpb	%al, %sil
	jne	.L142
	testb	%al, %al
	je	.L142
.L144:
	addq	$1, %rcx
	movzbl	(%rcx), %esi
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%sil, %sil
	jne	.L286
.L143:
	cmpb	$61, %al
	je	.L287
	.p2align 4,,10
	.p2align 3
.L142:
	addq	$112, %rdi
	cmpq	%rdi, %r8
	jne	.L155
	movq	%r12, %rbx
	testq	%rbx, %rbx
	jne	.L288
.L106:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	testl	%r10d, %r10d
	je	.L145
	movl	-4(%rdi), %eax
	testl	%eax, %eax
	je	.L289
.L147:
	cmpl	$2, %eax
	jne	.L142
.L145:
	leaq	tunable_list(%rip), %rax
	movq	%r13, %rsi
	movq	%r12, %rbx
	leaq	(%rax,%r9), %rdi
	call	tunable_initialize
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L160:
	testq	%r12, %r12
	je	.L106
	movq	(%r12), %rbp
	movq	%r12, %rbx
	testq	%rbp, %rbp
	je	.L106
	movzbl	0(%rbp), %r8d
	leaq	8(%rbx), %r12
	cmpb	$61, %r8b
	jne	.L290
.L174:
	movl	%r8d, %edx
	xorl	%eax, %eax
	jmp	.L157
.L289:
	movq	%rbx, %r11
.L146:
	movq	(%r11), %rax
	testq	%rax, %rax
	je	.L291
.L153:
	movzbl	(%rdi), %ecx
	movzbl	(%rax), %edx
	testb	%cl, %cl
	je	.L148
	cmpb	%dl, %cl
	jne	.L154
	testb	%dl, %dl
	je	.L154
	movq	%rdi, %rcx
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L292:
	cmpb	%dl, %sil
	jne	.L154
	testb	%dl, %dl
	je	.L154
.L149:
	addq	$1, %rcx
	movzbl	(%rcx), %esi
	addq	$1, %rax
	movzbl	(%rax), %edx
	testb	%sil, %sil
	jne	.L292
.L148:
	cmpb	$61, %dl
	je	.L293
.L154:
	addq	$8, %r11
	movq	(%r11), %rax
	testq	%rax, %rax
	jne	.L153
.L291:
	movl	-4(%rdi), %eax
	movq	%rbx, %r12
	jmp	.L147
.L293:
	movq	%r11, %rax
	.p2align 4,,10
	.p2align 3
.L152:
	movq	8(%rax), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	testq	%rdx, %rdx
	jne	.L152
	jmp	.L146
.L285:
	xorl	%edx, %edx
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L294:
	movzbl	1(%rbp,%rdx), %r8d
	movq	%rdi, %rdx
.L116:
	testb	%r8b, %r8b
	leaq	1(%rdx), %rdi
	jne	.L294
	movq	%rdx, 8(%rsp)
	call	__sbrk
	cmpq	$-1, %rax
	movq	%rax, (%rsp)
	je	.L117
	movq	8(%rsp), %rdx
	movq	(%rsp), %rcx
	testq	%rdx, %rdx
	leaq	-1(%rdx), %rax
	je	.L295
	.p2align 4,,10
	.p2align 3
.L118:
	movzbl	0(%rbp,%rax), %edx
	movb	%dl, (%rcx,%rax)
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L118
.L120:
	addq	(%rsp), %r15
	movq	%r15, 8(%rsp)
	je	.L138
	movzbl	(%r15), %r9d
	movq	%r15, %rbp
	testb	%r9b, %r9b
	je	.L138
	.p2align 4,,10
	.p2align 3
.L122:
	cmpb	$58, %r9b
	je	.L166
	cmpb	$61, %r9b
	je	.L166
	testb	%r9b, %r9b
	je	.L138
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L124:
	addq	$1, %rdx
	movzbl	0(%rbp,%rdx), %eax
	cmpb	$61, %al
	je	.L123
	cmpb	$58, %al
	je	.L123
	testb	%al, %al
	jne	.L124
.L138:
	movq	(%rsp), %rax
	movq	%rax, (%rbx)
	movq	%r12, %rbx
	jmp	.L111
.L166:
	movl	%r9d, %eax
	xorl	%edx, %edx
.L123:
	testb	%al, %al
	je	.L138
	leaq	1(%rbp,%rdx), %r11
	cmpb	$58, %al
	movzbl	(%r11), %r10d
	je	.L167
	xorl	%r15d, %r15d
	testb	%r10b, %r10b
	je	.L126
	cmpb	$58, %r10b
	jne	.L127
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L296:
	testb	%r10b, %r10b
	je	.L126
.L127:
	addq	$1, %r15
	movzbl	(%r11,%r15), %r10d
	cmpb	$58, %r10b
	jne	.L296
.L126:
	leaq	tunable_list(%rip), %rdi
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L134:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L169
	testb	%r9b, %r9b
	je	.L130
	cmpb	%r9b, %al
	jne	.L130
	movq	%rdi, %rdx
	movq	%rbp, %rax
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L297:
	cmpb	%sil, %cl
	jne	.L130
	testb	%sil, %sil
	je	.L130
.L131:
	addq	$1, %rdx
	movzbl	(%rdx), %ecx
	addq	$1, %rax
	movzbl	(%rax), %esi
	testb	%cl, %cl
	jne	.L297
	cmpb	$61, %sil
	je	.L298
.L130:
	addq	$1, %r8
	addq	$112, %rdi
	cmpq	$31, %r8
	jne	.L134
.L135:
	cmpb	$0, (%r11,%r15)
	je	.L138
	leaq	1(%r11,%r15), %rbp
	movzbl	0(%rbp), %r9d
	jmp	.L122
.L169:
	movl	%r9d, %esi
	cmpb	$61, %sil
	jne	.L130
.L298:
	movl	__GI___libc_enable_secure(%rip), %eax
	testl	%eax, %eax
	je	.L299
	leaq	0(,%r8,8), %rax
	leaq	tunable_list(%rip), %rcx
	subq	%r8, %rax
	salq	$4, %rax
	movl	84(%rcx,%rax), %eax
	testl	%eax, %eax
	je	.L300
	movq	%r11, %rbp
.L136:
	cmpl	$2, %eax
	je	.L133
	movq	%rbp, %r11
	jmp	.L135
.L167:
	movl	%r10d, %r9d
	movq	%r11, %rbp
	jmp	.L122
.L299:
	movq	%r11, %rbp
.L133:
	movq	%r11, %rsi
	subq	8(%rsp), %rsi
	addq	%r13, %rsi
	movb	$0, (%rsi,%r15)
	call	tunable_initialize
	movq	%rbp, %r11
	jmp	.L135
.L300:
	testb	%r10b, %r10b
	je	.L301
	leaq	1(%r11,%r15), %rdx
	movq	%rbp, %rax
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L139
.L140:
	addq	$1, %rdx
	addq	$1, %rax
	movb	%cl, -1(%rax)
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	jne	.L140
.L139:
	movb	$0, (%rax)
	leaq	0(,%r8,8), %rax
	leaq	tunable_list(%rip), %rcx
	xorl	%r15d, %r15d
	subq	%r8, %rax
	salq	$4, %rax
	movl	84(%rcx,%rax), %eax
	jmp	.L136
.L295:
	cmpq	$0, (%rsp)
	jne	.L120
	movq	(%rsp), %rax
	movq	%rax, (%rbx)
	movq	%r12, %rbx
	jmp	.L111
.L301:
	movb	$0, 0(%rbp)
	jmp	.L138
.L117:
	leaq	.LC2(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.size	__GI___tunables_init, .-__GI___tunables_init
	.section	.rodata.str1.1
.LC3:
	.string	"%s:\n"
.LC4:
	.string	"%s: "
.LC5:
	.string	"%d (min: %d, max: %d)\n"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"0x%lx (min: 0x%lx, max: 0x%lx)\n"
	.align 8
.LC7:
	.string	"0x%Zx (min: 0x%Zx, max: 0x%Zx)\n"
	.section	.rodata.str1.1
.LC8:
	.string	"%s\n"
	.text
	.p2align 4,,15
	.globl	__GI___tunables_print
	.hidden	__GI___tunables_print
	.type	__GI___tunables_print, @function
__GI___tunables_print:
	pushq	%r12
	pushq	%rbp
	leaq	.LC4(%rip), %r12
	pushq	%rbx
	leaq	tunable_list(%rip), %rbx
	leaq	3472(%rbx), %rbp
	jmp	.L310
	.p2align 4,,10
	.p2align 3
.L315:
	jb	.L307
	cmpl	$2, %eax
	je	.L308
	movq	72(%rbx), %rsi
	leaq	.LC8(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
.L304:
	addq	$112, %rbx
	cmpq	%rbp, %rbx
	je	.L313
.L310:
	cmpl	$3, 48(%rbx)
	movq	%rbx, %rsi
	jne	.L303
	cmpq	$0, 72(%rbx)
	je	.L314
.L303:
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	_dl_printf
	movl	48(%rbx), %eax
	cmpl	$1, %eax
	jne	.L315
	movq	64(%rbx), %rcx
	movq	56(%rbx), %rdx
	leaq	.LC6(%rip), %rdi
	movq	72(%rbx), %rsi
	xorl	%eax, %eax
	addq	$112, %rbx
	call	_dl_printf
	cmpq	%rbp, %rbx
	jne	.L310
.L313:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	movq	64(%rbx), %rcx
	movq	56(%rbx), %rdx
	leaq	.LC7(%rip), %rdi
	movq	72(%rbx), %rsi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L307:
	movl	64(%rbx), %ecx
	movl	56(%rbx), %edx
	leaq	.LC5(%rip), %rdi
	movl	72(%rbx), %esi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L314:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_printf
	jmp	.L304
	.size	__GI___tunables_print, .-__GI___tunables_print
	.p2align 4,,15
	.globl	__GI___tunable_get_val
	.hidden	__GI___tunable_get_val
	.type	__GI___tunable_get_val, @function
__GI___tunable_get_val:
	movl	%edi, %edi
	leaq	tunable_list(%rip), %rcx
	leaq	0(,%rdi,8), %rax
	subq	%rdi, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	movl	48(%rax), %r8d
	movq	72(%rax), %rax
	cmpl	$2, %r8d
	ja	.L319
	cmpl	$1, %r8d
	jnb	.L319
	movl	%eax, (%rsi)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L319:
	movq	%rax, (%rsi)
.L322:
	leaq	0(,%rdi,8), %rax
	subq	%rdi, %rax
	salq	$4, %rax
	cmpb	$0, 80(%rcx,%rax)
	je	.L316
	testq	%rdx, %rdx
	je	.L316
	leaq	72(%rcx,%rax), %rdi
	jmp	*%rdx
	.p2align 4,,10
	.p2align 3
.L316:
	rep ret
	.size	__GI___tunable_get_val, .-__GI___tunable_get_val
	.globl	__tunable_get_val
	.set	__tunable_get_val,__GI___tunable_get_val
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	tunable_list, @object
	.size	tunable_list, 3472
tunable_list:
	.string	"glibc.rtld.nns"
	.zero	27
	.zero	6
	.long	2
	.zero	4
	.quad	1
	.quad	16
	.quad	4
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.elision.skip_lock_after_retries"
	.zero	4
	.zero	6
	.long	0
	.zero	4
	.quad	-2147483648
	.quad	2147483647
	.quad	3
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.trim_threshold"
	.zero	14
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_TRIM_THRESHOLD_"
	.zero	1
	.string	"glibc.malloc.perturb"
	.zero	21
	.zero	6
	.long	0
	.zero	4
	.quad	0
	.quad	255
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_PERTURB_"
	.zero	7
	.zero	1
	.string	"glibc.cpu.x86_shared_cache_size"
	.zero	10
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.mem.tagging"
	.zero	24
	.zero	6
	.long	0
	.zero	4
	.quad	0
	.quad	255
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.elision.tries"
	.zero	22
	.zero	6
	.long	0
	.zero	4
	.quad	-2147483648
	.quad	2147483647
	.quad	3
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.elision.enable"
	.zero	21
	.zero	6
	.long	0
	.zero	4
	.quad	0
	.quad	1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.cpu.x86_rep_movsb_threshold"
	.zero	8
	.zero	6
	.long	2
	.zero	4
	.quad	1
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.mxfast"
	.zero	22
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.elision.skip_lock_busy"
	.zero	13
	.zero	6
	.long	0
	.zero	4
	.quad	-2147483648
	.quad	2147483647
	.quad	3
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.top_pad"
	.zero	21
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_TOP_PAD_"
	.zero	7
	.zero	1
	.string	"glibc.cpu.x86_rep_stosb_threshold"
	.zero	8
	.zero	6
	.long	2
	.zero	4
	.quad	1
	.quad	-1
	.quad	2048
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.cpu.x86_non_temporal_threshold"
	.zero	5
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.cpu.x86_shstk"
	.zero	22
	.zero	6
	.long	3
	.zero	4
	.quad	0
	.quad	0
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.cpu.hwcap_mask"
	.zero	21
	.zero	6
	.long	1
	.zero	4
	.quad	0
	.quad	-1
	.quad	6
	.byte	0
	.zero	3
	.long	0
	.string	"LD_HWCAP_MASK"
	.zero	9
	.zero	1
	.string	"glibc.malloc.mmap_max"
	.zero	20
	.zero	6
	.long	0
	.zero	4
	.quad	-2147483648
	.quad	2147483647
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_MMAP_MAX_"
	.zero	6
	.zero	1
	.string	"glibc.elision.skip_trylock_internal_abort"
	.zero	6
	.long	0
	.zero	4
	.quad	-2147483648
	.quad	2147483647
	.quad	3
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.tcache_unsorted_limit"
	.zero	7
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.cpu.x86_ibt"
	.zero	24
	.zero	6
	.long	3
	.zero	4
	.quad	0
	.quad	0
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.cpu.hwcaps"
	.zero	25
	.zero	6
	.long	3
	.zero	4
	.quad	0
	.quad	0
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.elision.skip_lock_internal_abort"
	.zero	3
	.zero	6
	.long	0
	.zero	4
	.quad	-2147483648
	.quad	2147483647
	.quad	3
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.arena_max"
	.zero	19
	.zero	6
	.long	2
	.zero	4
	.quad	1
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_ARENA_MAX"
	.zero	6
	.zero	1
	.string	"glibc.malloc.mmap_threshold"
	.zero	14
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_MMAP_THRESHOLD_"
	.zero	1
	.string	"glibc.cpu.x86_data_cache_size"
	.zero	12
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.tcache_count"
	.zero	16
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.arena_test"
	.zero	18
	.zero	6
	.long	2
	.zero	4
	.quad	1
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	1
	.string	"MALLOC_ARENA_TEST"
	.zero	5
	.zero	1
	.string	"glibc.pthread.mutex_spin_count"
	.zero	11
	.zero	6
	.long	0
	.zero	4
	.quad	0
	.quad	32767
	.quad	100
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.rtld.optional_static_tls"
	.zero	11
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.quad	512
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.tcache_max"
	.zero	18
	.zero	6
	.long	2
	.zero	4
	.quad	0
	.quad	-1
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.byte	0
	.zero	22
	.zero	1
	.string	"glibc.malloc.check"
	.zero	23
	.zero	6
	.long	0
	.zero	4
	.quad	0
	.quad	3
	.zero	8
	.byte	0
	.zero	3
	.long	0
	.string	"MALLOC_CHECK_"
	.zero	9
	.zero	1
	.hidden	_dl_printf
	.hidden	__sbrk
	.hidden	_dl_strtoul
