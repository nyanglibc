	.text
	.p2align 4,,15
	.globl	_dl_add_to_namespace_list
	.hidden	_dl_add_to_namespace_list
	.type	_dl_add_to_namespace_list, @function
_dl_add_to_namespace_list:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	_dl_load_write_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	leaq	(%rbx,%rbx,8), %rax
	leaq	_dl_ns(%rip), %rcx
	leaq	(%rbx,%rax,2), %rax
	leaq	(%rcx,%rax,8), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L4
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, %rdx
.L4:
	movq	24(%rdx), %rax
	testq	%rax, %rax
	jne	.L7
	movq	%rdx, 32(%rbp)
	movq	%rbp, 24(%rdx)
.L5:
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rax
	addl	$1, 8(%rcx,%rax,8)
	movq	_dl_load_adds(%rip), %rax
	movq	%rax, 1152(%rbp)
	addq	$1, %rax
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movq	%rax, _dl_load_adds(%rip)
	je	.L1
	addq	$8, %rsp
	leaq	_dl_load_write_lock(%rip), %rdi
	popq	%rbx
	popq	%rbp
	jmp	__pthread_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rbp, (%rax)
	jmp	.L5
	.size	_dl_add_to_namespace_list, .-_dl_add_to_namespace_list
	.p2align 4,,15
	.globl	_dl_new_object
	.hidden	_dl_new_object
	.type	_dl_new_object, @function
_dl_new_object:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rcx, %rbp
	movq	%r9, %r12
	subq	$40, %rsp
	movl	%r8d, 24(%rsp)
	movq	%rsi, 16(%rsp)
	call	strlen
	leaq	1(%rax), %rdx
	leaq	1193(%rax), %rdi
	movl	$1, %esi
	movq	%rax, %r14
	movq	%rdx, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L14
	movq	%rax, 40(%rbx)
	movq	16(%rsp), %r10
	leaq	1160(%rax), %rax
	movq	8(%rsp), %rdx
	leaq	1192(%rbx), %rdi
	movq	%rax, 720(%rbx)
	leaq	1168(%rbx), %rax
	movq	%r10, %rsi
	movq	%rax, 56(%rbx)
	call	memcpy@PLT
	movzbl	0(%r13), %ecx
	movq	%rax, 1168(%rbx)
	addq	%r14, %rax
	movl	$1, 1184(%rbx)
	testb	%cl, %cl
	cmovne	%r13, %rax
	andl	$3, %r15d
	movq	%rax, 8(%rbx)
	movzbl	796(%rbx), %eax
	andl	$-4, %eax
	orl	%eax, %r15d
	testb	$1, 1+_dl_debug_mask(%rip)
	movb	%r15b, 796(%rbx)
	je	.L62
.L18:
	leaq	(%r12,%r12,8), %rax
	leaq	880(%rbx), %rdx
	movq	%rbp, 736(%rbx)
	movq	%r12, 48(%rbx)
	movq	$4, 912(%rbx)
	leaq	(%r12,%rax,2), %rsi
	leaq	_dl_ns(%rip), %rax
	movq	%rdx, 920(%rbx)
	movq	(%rax,%rsi,8), %rax
	testq	%rax, %rax
	je	.L63
	addq	$704, %rax
	testq	%rbp, %rbp
	movl	$1, %esi
	movq	%rax, 880(%rbx)
	jne	.L23
	movq	%rbx, %rbp
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rax, %rbp
.L23:
	movq	736(%rbp), %rax
	testq	%rax, %rax
	jne	.L40
	testl	%esi, %esi
	jne	.L64
.L20:
	addq	$704, %rbp
.L33:
	movq	%rbp, (%rdx)
.L25:
	leaq	704(%rbx), %rax
	testb	%cl, %cl
	movb	%cl, 16(%rsp)
	movq	%rax, 928(%rbx)
	je	.L14
	movq	%r13, %rdi
	xorl	%r12d, %r12d
	call	strlen
	movzbl	16(%rsp), %ecx
	movq	%rax, 8(%rsp)
	addq	$1, %rax
	movq	%rax, 24(%rsp)
	movq	%rax, %r14
	cmpb	$47, %cl
	jne	.L26
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	127(%r14), %rsi
	subq	8(%rsp), %rsi
	movq	%rax, %rdi
	call	__getcwd
	testq	%rax, %rax
	jne	.L30
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r15, %r12
	movq	%rbp, %r14
	cmpl	$34, %fs:(%rax)
	jne	.L29
.L26:
	leaq	128(%r14), %rbp
	movq	%r12, %rdi
	movq	%rbp, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L66
.L29:
	movq	%r12, %rdi
	movq	$-1, %r15
	call	free@PLT
.L27:
	movq	%r15, 848(%rbx)
.L14:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, 996(%rbx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L64:
	movq	880(%rbx), %rax
.L22:
	addq	$704, %rbp
	cmpq	%rax, %rbp
	je	.L25
	testb	$8, 24(%rsp)
	je	.L67
	movq	%rax, 888(%rbx)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r15, %rdi
.L35:
	movl	(%rdi), %edx
	addq	$4, %rdi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L35
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rdi), %rdx
	movl	%eax, %ecx
	cmove	%rdx, %rdi
	addb	%al, %cl
	sbbq	$3, %rdi
	cmpb	$47, -1(%rdi)
	je	.L28
	movb	$47, (%rdi)
	addq	$1, %rdi
.L28:
	movq	24(%rsp), %rdx
	movq	%r13, %rsi
	call	__mempcpy@PLT
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rdx, %rax
.L31:
	cmpb	$47, -1(%rax)
	leaq	-1(%rax), %rdx
	jne	.L43
	cmpq	%rdx, %r15
	cmove	%rax, %rdx
	movb	$0, (%rdx)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%esi, %esi
	testq	%rbp, %rbp
	jne	.L23
	movq	%rbx, %rbp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	888(%rbx), %rdx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rax, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	%rax, %rdi
	jne	.L28
	movq	$-1, %r15
	jmp	.L27
	.size	_dl_new_object, .-_dl_new_object
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__getcwd
	.hidden	strlen
