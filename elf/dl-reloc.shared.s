	.text
	.p2align 4,,15
	.globl	_dl_try_allocate_static_tls
	.hidden	_dl_try_allocate_static_tls
	.type	_dl_try_allocate_static_tls, @function
_dl_try_allocate_static_tls:
	cmpq	$-1, 1112(%rdi)
	je	.L10
	movq	1096(%rdi), %r9
	cmpq	4064+_rtld_local(%rip), %r9
	ja	.L10
	movq	4056+_rtld_local(%rip), %r8
	movq	4048+_rtld_local(%rip), %rcx
	subq	%r8, %rcx
	cmpq	$2495, %rcx
	jbe	.L10
	movq	1104(%rdi), %r10
	movq	1088(%rdi), %rax
	subq	$2496, %rcx
	addq	%r10, %rax
	cmpq	%rax, %rcx
	jb	.L10
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r9
	imulq	%r9, %rax
	subq	%rax, %rcx
	subq	%r10, %rcx
	testb	%sil, %sil
	je	.L3
	movq	4072+_rtld_local(%rip), %rax
	cmpq	%rcx, %rax
	jb	.L10
	subq	%rcx, %rax
	movq	%rax, 4072+_rtld_local(%rip)
.L3:
	movq	40(%rdi), %rax
	addq	%r8, %rcx
	movq	%rcx, 4056+_rtld_local(%rip)
	movq	%rcx, 1112(%rdi)
	testb	$4, 796(%rax)
	je	.L4
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 114 "dl-reloc.c" 1
	movq %fs:8,%rax
# 0 "" 2
#NO_APP
	movq	4088+_rtld_local(%rip), %rsi
	cmpq	%rsi, (%rax)
	jne	.L16
.L5:
	movq	%rbx, %rdi
	call	*4096+_rtld_local(%rip)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	orb	$4, 797(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
.L2:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	1120(%rdi), %rdi
	call	_dl_update_slotinfo
	jmp	.L5
	.size	_dl_try_allocate_static_tls, .-_dl_try_allocate_static_tls
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot allocate memory in static TLS block"
	.text
	.p2align 4,,15
	.globl	_dl_allocate_static_tls
	.hidden	_dl_allocate_static_tls
	.type	_dl_allocate_static_tls, @function
_dl_allocate_static_tls:
	pushq	%rbx
	cmpq	$-1, 1112(%rdi)
	movq	%rdi, %rbx
	je	.L19
	xorl	%esi, %esi
	call	_dl_try_allocate_static_tls
	testl	%eax, %eax
	jne	.L19
	popq	%rbx
	ret
.L19:
	movq	8(%rbx), %rsi
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
	.size	_dl_allocate_static_tls, .-_dl_allocate_static_tls
	.p2align 4,,15
	.globl	_dl_nothread_init_static_tls
	.hidden	_dl_nothread_init_static_tls
	.type	_dl_nothread_init_static_tls, @function
_dl_nothread_init_static_tls:
	pushq	%rbx
	movq	%fs:16, %rax
	subq	1112(%rdi), %rax
	movq	1080(%rdi), %rdx
	movq	1088(%rdi), %rbx
	movq	1072(%rdi), %rsi
	subq	%rdx, %rbx
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	popq	%rbx
	jmp	memset@PLT
	.size	_dl_nothread_init_static_tls, .-_dl_nothread_init_static_tls
	.p2align 4,,15
	.globl	_dl_protect_relro
	.hidden	_dl_protect_relro
	.type	_dl_protect_relro, @function
_dl_protect_relro:
	movq	1136(%rdi), %rax
	addq	(%rdi), %rax
	movq	24+_rtld_local_ro(%rip), %rdx
	movq	%rax, %rcx
	addq	1144(%rdi), %rax
	negq	%rdx
	andq	%rdx, %rcx
	movq	%rax, %rsi
	andq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L26
	pushq	%rbx
	subq	%rcx, %rsi
	movq	%rdi, %rbx
	movl	$1, %edx
	movq	%rcx, %rdi
	call	__mprotect
	testl	%eax, %eax
	js	.L29
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	rep ret
.L29:
	movq	8(%rbx), %rsi
	movl	rtld_errno(%rip), %edi
	leaq	errstring.10259(%rip), %rcx
	xorl	%edx, %edx
	call	_dl_signal_error@PLT
	.size	_dl_protect_relro, .-_dl_protect_relro
	.p2align 4,,15
	.globl	_dl_reloc_bad_type
	.hidden	_dl_reloc_bad_type
	.type	_dl_reloc_bad_type, @function
_dl_reloc_bad_type:
	movslq	%edx, %rdx
	pushq	%r12
	pushq	%rbp
	leaq	(%rdx,%rdx,8), %rax
	pushq	%rbx
	movl	%esi, %ebx
	movq	%rdi, %r12
	leaq	(%rdx,%rax,2), %rdx
	leaq	msg.10265(%rip), %rax
	subq	$48, %rsp
	movq	%rsp, %rbp
	leaq	(%rax,%rdx,2), %rsi
	movq	%rbp, %rdi
	call	__stpcpy@PLT
	cmpl	$255, %ebx
	leaq	__GI__itoa_lower_digits(%rip), %rsi
	ja	.L34
.L31:
	movl	%ebx, %edx
	andl	$15, %ebx
	movb	$0, 2(%rax)
	shrl	$4, %edx
	movq	%rbp, %rcx
	xorl	%edi, %edi
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, (%rax)
	movzbl	(%rsi,%rbx), %edx
	movq	8(%r12), %rsi
	movb	%dl, 1(%rax)
	xorl	%edx, %edx
	call	_dl_signal_error@PLT
.L34:
	movl	%ebx, %edx
	addq	$6, %rax
	shrl	$28, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -6(%rax)
	movl	%ebx, %edx
	shrl	$24, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -5(%rax)
	movl	%ebx, %edx
	shrl	$20, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -4(%rax)
	movl	%ebx, %edx
	shrl	$16, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -3(%rax)
	movl	%ebx, %edx
	shrl	$12, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -2(%rax)
	movl	%ebx, %edx
	shrl	$8, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -1(%rax)
	jmp	.L31
	.size	_dl_reloc_bad_type, .-_dl_reloc_bad_type
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" (lazy)"
.LC2:
	.string	""
.LC3:
	.string	"<main program>"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"cannot make segment writable for relocation"
	.align 8
.LC5:
	.string	"cannot restore segment prot after reloc"
	.section	.rodata.str1.1
.LC6:
	.string	"<program name unknown>"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"%s: Symbol `%s' causes overflow in R_X86_64_32 relocation\n"
	.align 8
.LC8:
	.string	"%s: Symbol `%s' causes overflow in R_X86_64_PC32 relocation\n"
	.align 8
.LC9:
	.string	"%s: Symbol `%s' has different size in shared object, consider re-linking\n"
	.section	.rodata.str1.1
.LC10:
	.string	"\nrelocation processing: %s%s\n"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"../sysdeps/x86_64/dl-machine.h"
	.align 8
.LC12:
	.string	"ELFW(R_TYPE) (reloc->r_info) == R_X86_64_RELATIVE"
	.align 8
.LC13:
	.string	"%s: IFUNC symbol '%s' referenced in '%s' is defined in the executable and creates an unsatisfiable circular dependency.\n"
	.align 8
.LC14:
	.string	"%s: Relink `%s' with `%s' for IFUNC symbol `%s'\n"
	.align 8
.LC15:
	.string	"%s: out of memory to store relocation results for %s\n"
	.text
	.p2align 4,,15
	.globl	_dl_relocate_object
	.hidden	_dl_relocate_object
	.type	_dl_relocate_object, @function
_dl_relocate_object:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rdi, %r15
	subq	$216, %rsp
	andl	$134217728, %edx
	movq	%rsi, -184(%rbp)
	movl	%ecx, -216(%rbp)
	jne	.L36
	xorl	%eax, %eax
	cmpq	$0, 792+_rtld_local_ro(%rip)
	setne	%al
	orl	%eax, %ecx
	movl	%ecx, -216(%rbp)
.L36:
	testb	$4, 796(%r15)
	jne	.L35
	movl	_rtld_local_ro(%rip), %eax
	movl	-216(%rbp), %esi
	andl	$32, %eax
	testl	%esi, %esi
	jne	.L39
	cmpq	$0, 256(%r15)
	jne	.L422
.L39:
	movl	%r12d, %r13d
	andl	$1, %r13d
	testl	%eax, %eax
	jne	.L423
.L41:
	cmpq	$0, 240(%r15)
	movq	$0, -240(%rbp)
	jne	.L424
.L43:
	movq	104(%r15), %rax
	cmpq	$0, 248(%r15)
	movq	8(%rax), %rax
	movq	%rax, -200(%rbp)
	je	.L52
	testl	%r13d, %r13d
	jne	.L425
.L54:
	movq	120(%r15), %rax
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	je	.L228
	movq	8(%rax), %rdx
	movq	128(%r15), %rax
	movq	8(%rax), %r8
	movq	392(%r15), %rax
	movq	%rdx, -112(%rbp)
	testq	%rax, %rax
	movq	%r8, -104(%rbp)
	leaq	(%rdx,%r8), %rsi
	je	.L65
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
.L65:
	cmpq	$0, 224(%r15)
	je	.L66
	movq	248(%r15), %rax
	movq	80(%r15), %rcx
	movq	8(%rax), %rax
	movq	8(%rcx), %rcx
	leaq	(%rax,%rcx), %rdi
	cmpq	%rsi, %rdi
	jne	.L67
	subq	%rcx, %r8
	leaq	(%r8,%rdx), %rsi
	movq	%r8, -104(%rbp)
.L67:
	testl	%r13d, %r13d
	je	.L426
.L68:
	movq	%rax, -80(%rbp)
	movq	%rcx, -72(%rbp)
	movl	%r13d, -56(%rbp)
.L66:
	andl	$33554432, %r12d
	movq	$0, -208(%rbp)
	xorl	%ecx, %ecx
	movl	%r12d, -212(%rbp)
.L209:
	movq	(%r15), %rdi
	leaq	(%rdx,%r8), %rsi
	testl	%ecx, %ecx
	movq	%rdx, %rax
	movq	%rsi, -152(%rbp)
	movq	%rdi, -160(%rbp)
	jne	.L427
	movq	112(%r15), %rcx
	leaq	2568+_rtld_local(%rip), %rsi
	cmpq	%rsi, %r15
	movq	8(%rcx), %rdi
	movq	-208(%rbp), %rcx
	movq	-96(%rbp,%rcx), %rcx
	movq	%rdi, -168(%rbp)
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rdx,%rcx,8), %rbx
	je	.L85
	cmpq	$0, -160(%rbp)
	jne	.L84
	cmpq	$0, 576(%r15)
	je	.L84
.L85:
	movq	464(%r15), %rax
	testq	%rax, %rax
	je	.L428
	cmpq	%rbx, -152(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	jbe	.L70
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L135:
	movq	8(%rbx), %r14
	movl	%r14d, %r12d
	cmpq	$37, %r12
	je	.L429
	movq	-176(%rbp), %rcx
	movq	%r14, %rax
	movq	-160(%rbp), %r10
	shrq	$32, %rax
	addq	(%rbx), %r10
	cmpq	$8, %r12
	movq	744(%r15), %rsi
	movzwl	(%rcx,%rax,2), %edx
	movq	-168(%rbp), %rcx
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,8), %r13
	movq	%r13, -128(%rbp)
	je	.L418
	cmpq	$38, %r12
	je	.L418
	testq	%r12, %r12
	je	.L92
	movzbl	4(%r13), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L233
	movzbl	5(%r13), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L233
	cmpq	1040(%r15), %r13
	je	.L430
	cmpq	$7, %r12
	je	.L237
	cmpq	$16, %r12
	je	.L237
.L217:
	leaq	-17(%r12), %rax
	cmpq	$1, %rax
	setbe	%r9b
	cmpq	$36, %r12
	sete	%al
	orl	%eax, %r9d
	cmpq	$5, %r12
	movzbl	%r9b, %eax
	je	.L104
.L439:
	xorl	%r9d, %r9d
	cmpq	$6, %r12
	sete	%r9b
	sall	$2, %r9d
.L105:
	andl	$32767, %edx
	orl	%eax, %r9d
	movq	%r13, 1040(%r15)
	leaq	(%rdx,%rdx,2), %rdx
	movl	%r9d, 1048(%r15)
	leaq	(%rsi,%rdx,8), %r8
	testq	%r8, %r8
	je	.L106
	movl	8(%r8), %r11d
	movl	$0, %eax
	testl	%r11d, %r11d
	cmove	%rax, %r8
.L106:
	movl	0(%r13), %edi
	movq	-184(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	addq	-200(%rbp), %rdi
	pushq	$0
	movq	%r15, %rsi
	pushq	$9
	movq	%r10, -192(%rbp)
	call	_dl_lookup_symbol_x
	movq	%rax, %r11
	movq	-128(%rbp), %rax
	movq	-192(%rbp), %r10
	movq	%r11, 1056(%r15)
	movq	%rax, 1064(%r15)
	popq	%rdi
	popq	%r8
.L102:
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L96
.L107:
	cmpq	$37, %r12
	ja	.L113
	leaq	.L115(%rip), %rdi
	movslq	(%rdi,%r12,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L115:
	.long	.L113-.L115
	.long	.L114-.L115
	.long	.L116-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L117-.L115
	.long	.L114-.L115
	.long	.L114-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L119-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L120-.L115
	.long	.L121-.L115
	.long	.L122-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L123-.L115
	.long	.L124-.L115
	.long	.L113-.L115
	.long	.L113-.L115
	.long	.L125-.L115
	.long	.L126-.L115
	.text
	.p2align 4,,10
	.p2align 3
.L208:
	movl	-216(%rbp), %eax
	testl	%eax, %eax
	jne	.L431
.L210:
	orb	$4, 796(%r15)
	cmpq	$0, -240(%rbp)
	jne	.L432
.L213:
	cmpq	$0, 1144(%r15)
	je	.L35
	movq	%r15, %rdi
	call	_dl_protect_relro
.L35:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L446:
	testb	$1, 178+_rtld_local_ro(%rip)
	je	.L57
	leaq	_dl_runtime_profile_avx512(%rip), %rsi
	movq	%rsi, 16(%rax)
.L58:
	movq	632+_rtld_local_ro(%rip), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	%r15, %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L52
	movq	%r15, 2536+_rtld_local(%rip)
	.p2align 4,,10
	.p2align 3
.L52:
	movq	656(%r15), %rax
	testq	%rax, %rax
	je	.L54
	testl	%r13d, %r13d
	je	.L54
	movq	8(%rax), %rdx
	movq	(%r15), %rax
	leaq	_dl_tlsdesc_resolve_rela(%rip), %rcx
	movq	%rcx, (%rdx,%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L124:
	movq	-128(%rbp), %rax
	movq	16(%rax), %r9
.L114:
	addq	16(%rbx), %r9
	movq	%r9, (%r10)
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$24, %rbx
	cmpq	%rbx, -152(%rbp)
	ja	.L135
	movq	-224(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L70
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	ja	.L70
	leaq	-120(%rbp), %r13
	xorl	%r14d, %r14d
	movq	%r13, -192(%rbp)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	ja	.L70
.L150:
	movq	8(%rbx), %rax
	cmpl	$37, %eax
	jne	.L136
	movq	-176(%rbp), %rdi
	shrq	$32, %rax
	movq	-160(%rbp), %r13
	addq	(%rbx), %r13
	movq	744(%r15), %rcx
	movzwl	(%rdi,%rax,2), %edx
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r10
	movq	%r10, -120(%rbp)
	movzbl	4(%r10), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L249
	movzbl	5(%r10), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L249
	cmpq	1040(%r15), %r10
	je	.L433
.L138:
	andl	$32767, %edx
	movl	$0, 1048(%r15)
	movq	%r10, 1040(%r15)
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %r8
	testq	%r8, %r8
	je	.L140
	movl	8(%r8), %r11d
	testl	%r11d, %r11d
	cmove	%r14, %r8
.L140:
	movl	(%r10), %edi
	movq	-192(%rbp), %rdx
	xorl	%r9d, %r9d
	addq	-200(%rbp), %rdi
	movq	-184(%rbp), %rcx
	movq	%r15, %rsi
	pushq	$0
	pushq	$9
	movq	%r10, -152(%rbp)
	call	_dl_lookup_symbol_x
	movq	-120(%rbp), %rdx
	movq	%rax, 1056(%r15)
	movq	-152(%rbp), %r10
	movq	%rdx, 1064(%r15)
	popq	%r8
	popq	%r9
.L139:
	testq	%rdx, %rdx
	jne	.L137
.L141:
	movl	-212(%rbp), %esi
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%esi, %esi
	jne	.L149
.L148:
	call	*%rax
.L149:
	addq	$24, %rbx
	movq	%rax, 0(%r13)
	cmpq	%r12, %rbx
	jbe	.L150
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$32, -208(%rbp)
	movq	-208(%rbp), %rsi
	cmpq	$64, %rsi
	je	.L208
	movl	-88(%rbp,%rsi), %ecx
	movq	-104(%rbp,%rsi), %r8
	movq	-112(%rbp,%rsi), %rdx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L123:
	movq	-128(%rbp), %rax
	movq	16(%rax), %r9
.L119:
	addq	16(%rbx), %r9
	movl	$4294967295, %eax
	leaq	.LC7(%rip), %rdi
	cmpq	%rax, %r9
	movl	%r9d, (%r10)
	jbe	.L92
.L132:
	movq	104(%r15), %rax
	movl	0(%r13), %edx
	addq	8(%rax), %rdx
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L434
	movq	1112(%r11), %rcx
	cmpq	$-1, %rcx
	je	.L128
	testq	%rcx, %rcx
	je	.L435
.L129:
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	subq	%rcx, %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L126:
	movl	-212(%rbp), %edx
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%edx, %edx
	jne	.L134
	movq	%r10, -192(%rbp)
	call	*%rax
	movq	-192(%rbp), %r10
.L134:
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-128(%rbp), %r12
	testq	%r12, %r12
	je	.L92
	movq	16(%r12), %rdx
	cmpq	%rdx, 16(%r13)
	movq	%r9, %rsi
	cmovbe	16(%r13), %rdx
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	16(%r13), %rax
	cmpq	%rax, 16(%r12)
	ja	.L247
	jnb	.L92
	movl	60+_rtld_local_ro(%rip), %ecx
	testl	%ecx, %ecx
	je	.L92
.L247:
	leaq	.LC9(%rip), %rdi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L116:
	movq	16(%rbx), %rax
	subq	%r10, %rax
	addq	%rax, %r9
	movslq	%r9d, %rax
	movl	%r9d, (%r10)
	cmpq	%rax, %r9
	je	.L92
	leaq	.LC8(%rip), %rdi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L92
	movq	1112(%r11), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L436
.L131:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%rbx), %rax
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L92
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L120:
	testq	%r11, %r11
	je	.L92
	movq	1120(%r11), %rax
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L233:
	movq	%r13, %rax
	movq	%r15, %r11
.L96:
	movzwl	6(%rax), %edx
	cmpw	$-15, %dx
	je	.L241
	movq	(%r11), %r9
.L108:
	addq	8(%rax), %r9
	movzbl	4(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	jne	.L107
	testw	%dx, %dx
	je	.L107
	movl	-212(%rbp), %esi
	testl	%esi, %esi
	jne	.L107
	cmpq	%r11, %r15
	jne	.L437
.L109:
	movq	%r11, -248(%rbp)
	movq	%r10, -192(%rbp)
	call	*%r9
	movq	-248(%rbp), %r11
	movq	%rax, %r9
	movq	-192(%rbp), %r10
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L429:
	cmpq	$0, -224(%rbp)
	je	.L438
	movq	%rbx, -232(%rbp)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L418:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L237:
	cmpq	$5, %r12
	movl	$1, %eax
	jne	.L439
.L104:
	orl	$2, %eax
	xorl	%r9d, %r9d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L249:
	movq	%r10, %rdx
	movq	%r15, %rax
.L137:
	movzwl	6(%rdx), %esi
	movzbl	4(%rdx), %ecx
	movq	8(%rdx), %r9
	andl	$15, %ecx
	cmpw	$-15, %si
	je	.L142
	cmpb	$10, %cl
	movq	(%rax), %rdx
	jne	.L141
	testw	%si, %si
	je	.L141
	addq	%rdx, %r9
.L218:
	movl	-212(%rbp), %edi
	testl	%edi, %edi
	jne	.L143
	cmpq	%r15, %rax
	jne	.L440
.L144:
	call	*%r9
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L438:
	movq	%rbx, -232(%rbp)
	movq	%rbx, -224(%rbp)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L241:
	xorl	%r9d, %r9d
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L427:
	cmpq	%rsi, %rdx
	movq	%rsi, %r8
	jnb	.L70
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L71:
	movq	(%rax), %rcx
	addq	%rdi, %rcx
	cmpq	$7, %rdx
	jne	.L74
	movq	1016(%r15), %rdx
	testq	%rdx, %rdx
	jne	.L75
	addq	%rdi, (%rcx)
.L72:
	addq	$24, %rax
	cmpq	%rax, %r8
	jbe	.L441
.L77:
	movq	8(%rax), %rsi
	movl	%esi, %edx
	cmpq	$37, %rdx
	jne	.L71
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.L72
	movq	%rax, %rbx
	addq	$24, %rax
	cmpq	%rax, %r8
	ja	.L77
	.p2align 4,,10
	.p2align 3
.L441:
	testq	%rbx, %rbx
	je	.L70
	cmpq	%r12, %rbx
	ja	.L70
	movq	-160(%rbp), %r14
	movq	%r15, %r13
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L78:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	ja	.L442
.L80:
	cmpl	$37, 8(%rbx)
	jne	.L78
	movq	(%rbx), %r15
	movl	-212(%rbp), %edx
	movq	16(%rbx), %rax
	addq	0(%r13), %rax
	addq	%r14, %r15
	testl	%edx, %edx
	jne	.L79
	call	*%rax
.L79:
	addq	$24, %rbx
	movq	%rax, (%r15)
	cmpq	%r12, %rbx
	jbe	.L80
.L442:
	movq	%r13, %r15
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L74:
	cmpq	$36, %rdx
	jne	.L76
	movq	664(%r15), %rsi
	movq	(%r15), %rdx
	movq	%rax, 8(%rcx)
	addq	8(%rsi), %rdx
	movq	%rdx, (%rcx)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rcx, %rsi
	subq	1024(%r15), %rsi
	leaq	(%rdx,%rsi,2), %rdx
	movq	%rdx, (%rcx)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L430:
	xorl	%eax, %eax
	cmpq	$36, %r12
	ja	.L98
	movabsq	$68719935616, %rax
	movl	%r14d, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
.L98:
	cmpq	$5, %r12
	movl	1048(%r15), %ecx
	je	.L99
	cmpq	$6, %r12
	movl	$4, %edi
	je	.L100
	cmpl	%ecx, %eax
	je	.L216
	cmpq	$16, %r12
	je	.L276
	cmpq	$7, %r12
	jne	.L217
.L276:
	xorl	%r9d, %r9d
	movl	$1, %eax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L84:
	cmpq	%rbx, %rdx
	movq	-160(%rbp), %rcx
	jb	.L89
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L88:
	movq	16(%rax), %rdx
	addq	$24, %rax
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	movq	%rdx, (%rsi)
	jbe	.L85
.L89:
	movl	8(%rax), %edx
	movq	(%rax), %rsi
	addq	%rcx, %rsi
	cmpq	$38, %rdx
	je	.L88
	cmpq	$8, %rdx
	je	.L88
	leaq	__PRETTY_FUNCTION__.10132(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$546, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L428:
	xorl	%r10d, %r10d
	cmpq	%rbx, -152(%rbp)
	movq	$0, -192(%rbp)
	jbe	.L70
	movq	%r10, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L90:
	movq	8(%rbx), %r14
	movl	%r14d, %r12d
	cmpq	$37, %r12
	je	.L443
	movq	%r14, %rax
	movq	-168(%rbp), %rdi
	movq	-160(%rbp), %r10
	shrq	$32, %rax
	addq	(%rbx), %r10
	cmpq	$8, %r12
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	%r13, -136(%rbp)
	je	.L419
	cmpq	$38, %r12
	je	.L419
	testq	%r12, %r12
	je	.L152
	movzbl	4(%r13), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L255
	movzbl	5(%r13), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L255
	cmpq	1040(%r15), %r13
	je	.L444
	cmpq	$7, %r12
	je	.L259
	cmpq	$16, %r12
	je	.L259
.L220:
	leaq	-17(%r12), %rax
	cmpq	$1, %rax
	setbe	%r9b
	cmpq	$36, %r12
	sete	%al
	orl	%eax, %r9d
	cmpq	$5, %r12
	movzbl	%r9b, %eax
	je	.L164
.L455:
	xorl	%r9d, %r9d
	cmpq	$6, %r12
	sete	%r9b
	sall	$2, %r9d
.L165:
	movl	0(%r13), %edi
	orl	%eax, %r9d
	movq	-184(%rbp), %rcx
	addq	-200(%rbp), %rdi
	movl	%r9d, 1048(%r15)
	leaq	-136(%rbp), %rdx
	movq	%r13, 1040(%r15)
	pushq	$0
	xorl	%r8d, %r8d
	pushq	$9
	movq	%r15, %rsi
	movq	%r10, -224(%rbp)
	call	_dl_lookup_symbol_x
	movq	%rax, %r11
	movq	-136(%rbp), %rax
	movq	-224(%rbp), %r10
	movq	%r11, 1056(%r15)
	movq	%rax, 1064(%r15)
	popq	%rdx
	popq	%rcx
.L162:
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L156
.L166:
	cmpq	$37, %r12
	ja	.L172
	leaq	.L174(%rip), %rsi
	movslq	(%rsi,%r12,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L174:
	.long	.L172-.L174
	.long	.L173-.L174
	.long	.L175-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L176-.L174
	.long	.L173-.L174
	.long	.L173-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L178-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L179-.L174
	.long	.L180-.L174
	.long	.L181-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L182-.L174
	.long	.L183-.L174
	.long	.L172-.L174
	.long	.L172-.L174
	.long	.L184-.L174
	.long	.L185-.L174
	.text
	.p2align 4,,10
	.p2align 3
.L183:
	movq	-136(%rbp), %rax
	movq	16(%rax), %r9
.L173:
	addq	16(%rbx), %r9
	movq	%r9, (%r10)
	.p2align 4,,10
	.p2align 3
.L152:
	addq	$24, %rbx
	cmpq	%rbx, -152(%rbp)
	ja	.L90
	movq	-176(%rbp), %r10
	testq	%r10, %r10
	je	.L70
	movq	-192(%rbp), %r12
	cmpq	%r12, %r10
	ja	.L70
	leaq	-144(%rbp), %r13
	leaq	.LC14(%rip), %r14
	movq	%r10, %rbx
	movq	%r13, -176(%rbp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L194:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	ja	.L70
.L207:
	movq	8(%rbx), %rax
	cmpl	$37, %eax
	jne	.L194
	shrq	$32, %rax
	movq	-168(%rbp), %rdi
	movq	-160(%rbp), %r13
	leaq	(%rax,%rax,2), %rax
	addq	(%rbx), %r13
	leaq	(%rdi,%rax,8), %r10
	movq	%r10, -144(%rbp)
	movzbl	4(%r10), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L270
	movzbl	5(%r10), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L270
	cmpq	1040(%r15), %r10
	je	.L445
.L196:
	movl	(%r10), %edi
	movq	-176(%rbp), %rdx
	movq	%r15, %rsi
	addq	-200(%rbp), %rdi
	movq	-184(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%r10, 1040(%r15)
	movl	$0, 1048(%r15)
	xorl	%r8d, %r8d
	pushq	$0
	pushq	$9
	movq	%r10, -152(%rbp)
	call	_dl_lookup_symbol_x
	movq	-144(%rbp), %rdx
	movq	%rax, 1056(%r15)
	movq	-152(%rbp), %r10
	movq	%rdx, 1064(%r15)
	popq	%rsi
	popq	%rdi
.L197:
	testq	%rdx, %rdx
	jne	.L195
.L198:
	movl	-212(%rbp), %edx
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%edx, %edx
	jne	.L206
.L205:
	call	*%rax
.L206:
	movq	%rax, 0(%r13)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L426:
	cmpq	%rsi, %rax
	jne	.L68
	addq	%rcx, %r8
	movq	%r8, -104(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L425:
	movq	88(%r15), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L55
	addq	(%r15), %rdx
	movq	%rdx, 1016(%r15)
	leaq	24(%rax), %rdx
	movq	%rdx, 1024(%r15)
.L55:
	movl	-216(%rbp), %ecx
	movq	%r15, 8(%rax)
	testl	%ecx, %ecx
	jne	.L446
	cmpq	$0, 392+_rtld_local_ro(%rip)
	je	.L61
	testb	$2, 236+_rtld_local_ro(%rip)
	leaq	_dl_runtime_resolve_xsave(%rip), %rdx
	leaq	_dl_runtime_resolve_xsavec(%rip), %rcx
	cmovne	%rcx, %rdx
	movq	%rdx, 16(%rax)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L182:
	movq	-136(%rbp), %rax
	movq	16(%rax), %r9
.L178:
	addq	16(%rbx), %r9
	movl	$4294967295, %eax
	leaq	.LC7(%rip), %rdi
	cmpq	%rax, %r9
	movl	%r9d, (%r10)
	jbe	.L152
.L191:
	movq	104(%r15), %rax
	movl	0(%r13), %edx
	addq	8(%rax), %rdx
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L185:
	movl	-212(%rbp), %r9d
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%r9d, %r9d
	jne	.L193
	movq	%r10, -224(%rbp)
	call	*%rax
	movq	-224(%rbp), %r10
.L193:
	movq	%rax, (%r10)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L152
	movq	16(%r12), %rax
	cmpq	%rax, 16(%r13)
	movq	%r9, %rsi
	cmovbe	16(%r13), %rax
	movq	%r10, %rdi
	movq	%rax, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rax
	cmpq	%rax, 16(%r12)
	ja	.L268
	jnb	.L152
	movl	60+_rtld_local_ro(%rip), %r10d
	testl	%r10d, %r10d
	je	.L152
.L268:
	leaq	.LC9(%rip), %rdi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L175:
	movq	16(%rbx), %rax
	subq	%r10, %rax
	addq	%rax, %r9
	movslq	%r9d, %rax
	movl	%r9d, (%r10)
	cmpq	%rax, %r9
	je	.L152
	leaq	.LC8(%rip), %rdi
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L184:
	movq	-136(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L447
	movq	1112(%r11), %rcx
	cmpq	$-1, %rcx
	je	.L187
	testq	%rcx, %rcx
	je	.L448
.L188:
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	subq	%rcx, %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L181:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L152
	movq	1112(%r11), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L449
.L190:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%rbx), %rax
	movq	%rax, (%r10)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L180:
	movq	-136(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L152
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	movq	%rax, (%r10)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L179:
	testq	%r11, %r11
	je	.L152
	movq	1120(%r11), %rax
	movq	%rax, (%r10)
	jmp	.L152
.L99:
	orl	$2, %eax
	xorl	%edi, %edi
.L100:
	orl	%edi, %eax
	cmpl	%eax, %ecx
	jne	.L217
.L216:
	movq	1064(%r15), %rax
	addq	$1, 2552+_rtld_local(%rip)
	movq	1056(%r15), %r11
	movq	%rax, -128(%rbp)
	jmp	.L102
.L424:
	movzwl	696(%r15), %edx
	movq	680(%r15), %rcx
	movq	%rcx, %rbx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	%rax, %rcx
	jnb	.L43
	movl	%r13d, -152(%rbp)
	movl	%r12d, -160(%rbp)
	movabsq	$12884901887, %r14
	movq	%r15, %r13
	xorl	%r12d, %r12d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	0(,%rdx,8), %rax
	addq	$56, %rbx
	subq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	%rax, %rbx
	jnb	.L450
.L50:
	movq	(%rbx), %rax
	andq	%r14, %rax
	cmpq	$1, %rax
	jne	.L44
	movq	16(%rbx), %rsi
	movq	24+_rtld_local_ro(%rip), %rax
	subq	$48, %rsp
	leaq	15(%rsp), %rcx
	movq	%rax, %rdx
	leaq	-1(%rsi,%rax), %rax
	addq	40(%rbx), %rax
	negq	%rdx
	movq	%rsi, %rdi
	andq	$-16, %rcx
	andq	%rdx, %rdi
	movl	$0, 16(%rcx)
	movq	%rcx, %r15
	movq	%rax, %rsi
	movl	4(%rbx), %eax
	andq	%rdx, %rsi
	xorl	%edx, %edx
	subq	%rdi, %rsi
	addq	0(%r13), %rdi
	testb	$4, %al
	movq	%rsi, 8(%rcx)
	movq	%rdi, (%rcx)
	je	.L45
	movl	$1, 16(%rcx)
	movl	$1, %edx
.L45:
	testb	$2, %al
	je	.L46
	orl	$2, %edx
	movl	%edx, 16(%r15)
.L46:
	testb	$1, %al
	je	.L47
	orl	$4, %edx
	movl	%edx, 16(%r15)
.L47:
	orl	$2, %edx
	call	__mprotect
	testl	%eax, %eax
	js	.L451
	movq	%r12, 24(%r15)
	movq	680(%r13), %rcx
	movq	%r15, %r12
	movzwl	696(%r13), %edx
	jmp	.L44
.L423:
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rax
	testl	%r13d, %r13d
	cmove	%rax, %rdx
.L40:
	movq	8(%r15), %rsi
	cmpb	$0, (%rsi)
	je	.L452
.L42:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L443:
	cmpq	$0, -176(%rbp)
	je	.L453
	movq	%rbx, -192(%rbp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L255:
	movq	%r13, %rax
	movq	%r15, %r11
.L156:
	movzwl	6(%rax), %edx
	cmpw	$-15, %dx
	je	.L262
	movq	(%r11), %r9
.L167:
	addq	8(%rax), %r9
	movzbl	4(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	jne	.L166
	testw	%dx, %dx
	je	.L166
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	jne	.L166
	cmpq	%r11, %r15
	jne	.L454
.L168:
	movq	%r11, -232(%rbp)
	movq	%r10, -224(%rbp)
	call	*%r9
	movq	-232(%rbp), %r11
	movq	%rax, %r9
	movq	-224(%rbp), %r10
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L419:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	movq	%rax, (%r10)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L259:
	cmpq	$5, %r12
	movl	$1, %eax
	jne	.L455
.L164:
	orl	$2, %eax
	xorl	%r9d, %r9d
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%rbx, -192(%rbp)
	movq	%rbx, -176(%rbp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L270:
	movq	%r10, %rdx
	movq	%r15, %rax
.L195:
	movzwl	6(%rdx), %ecx
	movq	8(%rdx), %r9
	movzbl	4(%rdx), %edx
	andl	$15, %edx
	cmpw	$-15, %cx
	je	.L199
	cmpb	$10, %dl
	movq	(%rax), %rsi
	jne	.L198
	testw	%cx, %cx
	je	.L198
	addq	%rsi, %r9
.L221:
	movl	-212(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L200
	cmpq	%rax, %r15
	jne	.L456
.L201:
	call	*%r9
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L228:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L434:
	movq	16(%rbx), %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L262:
	xorl	%r9d, %r9d
	jmp	.L167
.L61:
	leaq	_dl_runtime_resolve_fxsave(%rip), %rdi
	movq	%rdi, 16(%rax)
	jmp	.L52
.L444:
	xorl	%eax, %eax
	cmpq	$36, %r12
	jbe	.L457
.L158:
	cmpq	$5, %r12
	movl	1048(%r15), %ecx
	je	.L159
	cmpq	$6, %r12
	movl	$4, %edx
	jne	.L458
.L160:
	orl	%edx, %eax
	cmpl	%eax, %ecx
	jne	.L220
.L219:
	movq	1064(%r15), %rax
	addq	$1, 2552+_rtld_local(%rip)
	movq	1056(%r15), %r11
	movq	%rax, -136(%rbp)
	jmp	.L162
.L457:
	movabsq	$68719935616, %rax
	movl	%r14d, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
	jmp	.L158
.L437:
	movzbl	796(%r11), %eax
	testb	$4, %al
	jne	.L109
	movq	104(%r15), %rdx
	movq	__GI__dl_argv(%rip), %rcx
	movl	0(%r13), %r8d
	addq	8(%rdx), %r8
	testb	$3, %al
	movq	8(%r15), %rdx
	movq	(%rcx), %rsi
	je	.L421
	movq	8(%r11), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	leaq	.LC14(%rip), %rdi
	movq	%r9, -256(%rbp)
	movq	%r10, -248(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	movq	%r11, -192(%rbp)
	call	_dl_error_printf
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %r10
	movq	-192(%rbp), %r11
	jmp	.L109
.L142:
	cmpb	$10, %cl
	jne	.L141
	jmp	.L218
.L432:
	movq	-240(%rbp), %rbx
.L214:
	movl	16(%rbx), %edx
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	__mprotect
	testl	%eax, %eax
	js	.L275
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L213
	jmp	.L214
.L431:
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.L210
	movq	224(%r15), %rdx
	xorl	%ecx, %ecx
	movq	8(%rax), %rax
	movl	$32, %edi
	cmpq	$7, 8(%rdx)
	sete	%cl
	xorl	%edx, %edx
	leaq	16(,%rcx,8), %rcx
	divq	%rcx
	movq	%rax, %rsi
	call	*__rtld_calloc(%rip)
	testq	%rax, %rax
	movq	%rax, 832(%r15)
	jne	.L210
	movq	__GI__dl_argv(%rip), %rax
	movq	8(%r15), %rdx
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L212
	leaq	.LC6(%rip), %rsi
.L212:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	__GI__dl_fatal_printf
	.p2align 4,,10
	.p2align 3
.L450:
	movq	%r12, -240(%rbp)
	movq	%r13, %r15
	movl	-160(%rbp), %r12d
	movl	-152(%rbp), %r13d
	jmp	.L43
.L452:
	movq	__GI__dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC3(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L42
.L422:
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L41
	leaq	.LC2(%rip), %rdx
	jmp	.L40
.L458:
	cmpl	%eax, %ecx
	je	.L219
	cmpq	$16, %r12
	je	.L277
	cmpq	$7, %r12
	jne	.L220
.L277:
	xorl	%r9d, %r9d
	movl	$1, %eax
	jmp	.L165
.L440:
	movzbl	796(%rax), %ecx
	testb	$4, %cl
	jne	.L144
	movq	104(%r15), %rdx
	movq	__GI__dl_argv(%rip), %rsi
	movl	(%r10), %r8d
	addq	8(%rdx), %r8
	andl	$3, %ecx
	movq	8(%r15), %rdx
	movq	(%rsi), %rsi
	je	.L421
	movq	8(%rax), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	leaq	.LC14(%rip), %rdi
	movq	%r9, -152(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	movq	-152(%rbp), %r9
	jmp	.L144
.L113:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_dl_reloc_bad_type
	.p2align 4,,10
	.p2align 3
.L433:
	movl	1048(%r15), %eax
	testl	%eax, %eax
	jne	.L138
	movq	1064(%r15), %rdx
	addq	$1, 2552+_rtld_local(%rip)
	movq	1056(%r15), %rax
	movq	%rdx, -120(%rbp)
	jmp	.L139
.L447:
	movq	16(%rbx), %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L152
.L128:
	movq	16(%rbx), %rsi
	addq	8(%rdx), %rsi
	movq	%r11, %rdi
	movq	%r10, -192(%rbp)
	call	_dl_make_tlsdesc_dynamic
	movq	-192(%rbp), %r10
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_dynamic(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L92
.L199:
	cmpb	$10, %dl
	jne	.L198
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L436:
	movq	%r11, %rdi
	movq	%r10, -248(%rbp)
	movq	%r11, -192(%rbp)
	call	_dl_allocate_static_tls
	movq	-192(%rbp), %r11
	movq	-128(%rbp), %rax
	movq	-248(%rbp), %r10
	movq	1112(%r11), %rdx
	jmp	.L131
.L454:
	movzbl	796(%r11), %eax
	testb	$4, %al
	jne	.L168
	movq	104(%r15), %rdx
	movq	__GI__dl_argv(%rip), %rcx
	movl	0(%r13), %r8d
	addq	8(%rdx), %r8
	testb	$3, %al
	movq	8(%r15), %rdx
	movq	(%rcx), %rsi
	je	.L421
	movq	8(%r11), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	leaq	.LC14(%rip), %rdi
	movq	%r9, -248(%rbp)
	movq	%r10, -232(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	movq	%r11, -224(%rbp)
	call	_dl_error_printf
	movq	-248(%rbp), %r9
	movq	-232(%rbp), %r10
	movq	-224(%rbp), %r11
	jmp	.L168
.L159:
	orl	$2, %eax
	xorl	%edx, %edx
	jmp	.L160
.L435:
	movq	%r11, %rdi
	movl	$1, %esi
	movq	%r10, -248(%rbp)
	movq	%r11, -192(%rbp)
	call	_dl_try_allocate_static_tls
	testl	%eax, %eax
	movq	-128(%rbp), %rdx
	movq	-192(%rbp), %r11
	movq	-248(%rbp), %r10
	jne	.L128
	movq	1112(%r11), %rcx
	jmp	.L129
.L76:
	movl	$1, %edx
	movq	%r15, %rdi
	call	_dl_reloc_bad_type
.L445:
	movl	1048(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L196
	movq	1064(%r15), %rdx
	addq	$1, 2552+_rtld_local(%rip)
	movq	1056(%r15), %rax
	movq	%rdx, -144(%rbp)
	jmp	.L197
.L187:
	movq	16(%rbx), %rsi
	addq	8(%rdx), %rsi
	movq	%r11, %rdi
	movq	%r10, -224(%rbp)
	call	_dl_make_tlsdesc_dynamic
	movq	-224(%rbp), %r10
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_dynamic(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L152
.L449:
	movq	%r11, %rdi
	movq	%r10, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_dl_allocate_static_tls
	movq	-224(%rbp), %r11
	movq	-136(%rbp), %rax
	movq	-232(%rbp), %r10
	movq	1112(%r11), %rdx
	jmp	.L190
.L448:
	movq	%r11, %rdi
	movl	$1, %esi
	movq	%r10, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_dl_try_allocate_static_tls
	testl	%eax, %eax
	movq	-136(%rbp), %rdx
	movq	-224(%rbp), %r11
	movq	-232(%rbp), %r10
	jne	.L187
	movq	1112(%r11), %rcx
	jmp	.L188
.L57:
	testb	$16, 151+_rtld_local_ro(%rip)
	je	.L59
	leaq	_dl_runtime_profile_avx(%rip), %rcx
	movq	%rcx, 16(%rax)
	jmp	.L58
.L456:
	movzbl	796(%rax), %ecx
	testb	$4, %cl
	jne	.L201
	movq	104(%r15), %rdx
	movq	__GI__dl_argv(%rip), %rsi
	movl	(%r10), %r8d
	addq	8(%rdx), %r8
	andl	$3, %ecx
	movq	8(%r15), %rdx
	movq	(%rsi), %rsi
	je	.L421
	movq	8(%rax), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	movq	%r14, %rdi
	movq	%r9, -152(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	movq	-152(%rbp), %r9
	jmp	.L201
.L143:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L149
.L59:
	leaq	_dl_runtime_profile_sse(%rip), %rsi
	movq	%rsi, 16(%rax)
	jmp	.L58
.L200:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L206
.L172:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_dl_reloc_bad_type
.L275:
	leaq	.LC5(%rip), %rcx
.L49:
	movq	8(%r15), %rsi
	movl	rtld_errno(%rip), %edi
	xorl	%edx, %edx
	call	_dl_signal_error@PLT
.L451:
	movq	%r13, %r15
	leaq	.LC4(%rip), %rcx
	jmp	.L49
.L421:
	testq	%rsi, %rsi
	jne	.L203
	leaq	.LC6(%rip), %rsi
.L203:
	leaq	.LC13(%rip), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	movq	%r8, %rdx
	call	__GI__dl_fatal_printf
	.size	_dl_relocate_object, .-_dl_relocate_object
	.section	.rodata
	.align 32
	.type	msg.10265, @object
	.size	msg.10265, 76
msg.10265:
	.string	"unexpected reloc type 0x"
	.zero	13
	.string	"unexpected PLT reloc type 0x"
	.zero	9
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	errstring.10259, @object
	.size	errstring.10259, 59
errstring.10259:
	.string	"cannot apply additional memory protection after relocation"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10132, @object
	.size	__PRETTY_FUNCTION__.10132, 26
__PRETTY_FUNCTION__.10132:
	.string	"elf_machine_rela_relative"
	.hidden	_dl_runtime_profile_sse
	.hidden	_dl_runtime_profile_avx
	.hidden	_dl_tlsdesc_dynamic
	.hidden	_dl_make_tlsdesc_dynamic
	.hidden	__rtld_calloc
	.hidden	_dl_runtime_resolve_fxsave
	.hidden	_dl_tlsdesc_undefweak
	.hidden	_dl_debug_printf
	.hidden	_dl_runtime_resolve_xsavec
	.hidden	_dl_runtime_resolve_xsave
	.hidden	_dl_tlsdesc_return
	.hidden	_dl_error_printf
	.hidden	_dl_tlsdesc_resolve_rela
	.hidden	_dl_name_match_p
	.hidden	_dl_runtime_profile_avx512
	.hidden	_dl_lookup_symbol_x
	.hidden	rtld_errno
	.hidden	__mprotect
	.hidden	_rtld_local_ro
	.hidden	_dl_update_slotinfo
	.hidden	_rtld_local
