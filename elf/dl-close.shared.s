	.text
	.p2align 4,,15
	.type	call_destructors, @function
call_destructors:
	movq	272(%rdi), %rax
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	testq	%rax, %rax
	je	.L2
	movq	8(%rax), %rbp
	movq	288(%rdi), %rax
	addq	(%rdi), %rbp
	movq	8(%rax), %rax
	shrq	$3, %rax
	testl	%eax, %eax
	leal	-1(%rax), %edx
	je	.L2
	leaq	0(%rbp,%rdx,8), %rbx
	subq	$8, %rbp
	.p2align 4,,10
	.p2align 3
.L3:
	call	*(%rbx)
	subq	$8, %rbx
	cmpq	%rbx, %rbp
	jne	.L3
.L2:
	movq	168(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1
	movq	(%r12), %rax
	addq	8(%rdx), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	call_destructors, .-call_destructors
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-close.c"
.LC1:
	.string	"! should_be_there"
.LC2:
	.string	"old_map->l_tls_modid == idx"
	.text
	.p2align 4,,15
	.type	remove_slotinfo, @function
remove_slotinfo:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rax
	subq	%rdx, %rax
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	(%rsi), %rdx
	cmpq	%rdx, %rax
	jb	.L12
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L13
	testb	%cl, %cl
	jne	.L29
.L17:
	movq	4040+_rtld_local(%rip), %rax
	testq	%rbx, %rbx
	leaq	1(%rax), %rcx
	movl	$0, %eax
	cmovne	%rax, %rcx
	movq	%rdi, %rax
	subq	%rbx, %rax
	salq	$4, %rax
	leaq	8(%rbp,%rax), %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	subq	$16, %rax
	subq	$1, %rdi
	cmpq	$0, 16(%rax)
	jne	.L30
.L21:
	movq	%rdi, %rdx
	subq	%rbx, %rdx
	cmpq	%rcx, %rdx
	ja	.L22
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	salq	$4, %rax
	addq	%rsi, %rax
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L18
	cmpq	%rdi, 1120(%rdx)
	jne	.L31
	movq	4088+_rtld_local(%rip), %rsi
	movq	$0, 24(%rax)
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rax)
.L18:
	cmpq	%rdi, 4024+_rtld_local(%rip)
	je	.L17
.L28:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rdi, 4024+_rtld_local(%rip)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L13:
	addq	%rbx, %rdx
	movzbl	%cl, %ecx
	call	remove_slotinfo
	testb	%al, %al
	jne	.L28
	movq	0(%rbp), %rdi
	addq	%rbx, %rdi
	jmp	.L17
.L29:
	leaq	__PRETTY_FUNCTION__.10749(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$59, %edx
	call	__GI___assert_fail
.L31:
	leaq	__PRETTY_FUNCTION__.10749(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$80, %edx
	call	__GI___assert_fail
	.size	remove_slotinfo, .-remove_slotinfo
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"\nclosing file=%s; direct_opencount=%u\n"
	.section	.rodata.str1.1
.LC4:
	.string	"idx == nloaded"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(*lp)->l_idx >= 0 && (*lp)->l_idx < nloaded"
	.align 8
.LC6:
	.string	"jmap->l_idx >= 0 && jmap->l_idx < nloaded"
	.section	.rodata.str1.1
.LC7:
	.string	"imap->l_ns == nsid"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"imap->l_type == lt_loaded && !imap->l_nodelete_active"
	.section	.rodata.str1.1
.LC9:
	.string	"\ncalling fini: %s [%lu]\n\n"
.LC10:
	.string	"tmap->l_ns == nsid"
.LC11:
	.string	"cannot create scope list"
.LC12:
	.string	"dlclose"
.LC13:
	.string	"imap->l_type == lt_loaded"
.LC14:
	.string	"nsid != LM_ID_BASE"
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"\nfile=%s [%lu];  destroying link map\n"
	.align 8
.LC16:
	.string	"TLS generation counter wrapped!  Please report as described in <https://www.gnu.org/software/libc/bugs.html>.\n"
	.text
	.p2align 4,,15
	.globl	_dl_close_worker
	.hidden	_dl_close_worker
	.type	_dl_close_worker, @function
_dl_close_worker:
	pushq	%rbp
	movabsq	$8589934592, %rcx
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$136, %rsp
	movl	792(%rdi), %eax
	movb	%sil, -113(%rbp)
	leal	-1(%rax), %edx
	movabsq	$17179869183, %rax
	movl	%edx, 792(%rdi)
	movq	792(%rdi), %rsi
	andq	%rax, %rsi
	cmpq	%rcx, %rsi
	je	.L299
.L33:
	testb	$64, _rtld_local_ro(%rip)
	jne	.L300
.L32:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L299:
	movl	dl_close_state.10771(%rip), %r14d
	testl	%r14d, %r14d
	jne	.L301
	movq	48(%rdi), %r15
	movq	%rax, -88(%rbp)
	cmpq	$1, %r15
	leaq	(%r15,%r15,8), %rdx
	sbbq	%rbx, %rbx
	andl	$8, %ebx
	leaq	(%r15,%rdx,2), %rdx
	movq	%rbx, -176(%rbp)
	leaq	_rtld_local(%rip), %rbx
	salq	$3, %rdx
	leaq	40(%rbx,%rdx), %rcx
	addq	%rdx, %rbx
	movq	%rbx, -144(%rbp)
	movq	%rcx, -112(%rbp)
.L36:
	movq	-144(%rbp), %rsi
	movq	%rsp, -160(%rbp)
	xorl	%edx, %edx
	movl	$1, dl_close_state.10771(%rip)
	movl	8(%rsi), %r13d
	leaq	15(%r13), %rax
	movq	%r13, %r11
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	movq	%rsp, %r10
	subq	%rax, %rsp
	leaq	22(,%r13,8), %rax
	movq	%rsp, %r12
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	movq	(%rsi), %rax
	leaq	7(%rsp), %rbx
	shrq	$3, %rbx
	testq	%rax, %rax
	leaq	0(,%rbx,8), %rcx
	movq	%rcx, -56(%rbp)
	je	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%edx, 1012(%rax)
	movq	%rax, (%rcx)
	addl	$1, %edx
	movq	24(%rax), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.L38
.L37:
	cmpl	%edx, %r11d
	jne	.L302
	movq	%r10, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	movl	%r11d, -72(%rbp)
	movq	%r10, -64(%rbp)
	call	memset@PLT
	movq	%r12, %rdi
	movq	%r13, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	-72(%rbp), %r11d
	movq	-64(%rbp), %r10
	movl	$-1, %edi
	movabsq	$8589934592, %r9
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$1, %edi
	cmpl	%edi, %r11d
	jbe	.L303
.L54:
	movslq	%edi, %rax
	cmpb	$0, (%r12,%rax)
	jne	.L40
	movq	-56(%rbp), %rcx
	movq	-88(%rbp), %rdx
	movq	(%rcx,%rax,8), %rcx
	andq	792(%rcx), %rdx
	cmpq	%r9, %rdx
	je	.L42
.L45:
	movb	$1, (%r10,%rax)
	movb	$1, (%r12,%rax)
	movq	976(%rcx), %rax
	movl	$-1, 1012(%rcx)
	testq	%rax, %rax
	je	.L44
	leaq	8(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movslq	1012(%rax), %rax
	cmpl	$-1, %eax
	je	.L46
	testl	%eax, %eax
	js	.L47
	cmpl	%eax, %r11d
	jbe	.L47
	cmpb	$0, (%r10,%rax)
	jne	.L46
	movb	$1, (%r10,%rax)
	movq	(%rdx), %rax
	movl	1012(%rax), %eax
	leal	-1(%rax), %esi
	cmpl	%edi, %eax
	cmovle	%esi, %edi
.L46:
	addq	$8, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L49
.L44:
	movq	984(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L40
	movl	(%rsi), %r13d
	testl	%r13d, %r13d
	je	.L40
	xorl	%edx, %edx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L304:
	cmpl	%eax, %r11d
	jbe	.L51
	cmpb	$0, (%r10,%rax)
	jne	.L50
	movb	$1, (%r10,%rax)
	movl	1012(%r8), %eax
	cmpl	%edi, %eax
	jg	.L294
	leal	-1(%rax), %edi
.L294:
	movq	984(%rcx), %rsi
.L50:
	addl	$1, %edx
	cmpl	(%rsi), %edx
	jnb	.L40
.L53:
	movl	%edx, %eax
	movq	8(%rsi,%rax,8), %r8
	movslq	1012(%r8), %rax
	cmpl	$-1, %eax
	je	.L50
	testl	%eax, %eax
	jns	.L304
.L51:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$248, %edx
	call	__GI___assert_fail
.L301:
	movl	$2, dl_close_state.10771(%rip)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L42:
	cmpb	$0, 799(%rcx)
	jne	.L45
	movq	1128(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L45
	cmpb	$0, (%r10,%rax)
	jne	.L45
	addl	$1, %edi
	cmpl	%edi, %r11d
	ja	.L54
.L303:
	testq	%r15, %r15
	movq	-56(%rbp), %rdi
	movl	%r11d, %esi
	sete	%al
	addq	-176(%rbp), %rdi
	movl	$1, %ecx
	movzbl	%al, %edx
	movzbl	%al, %eax
	movq	%r10, -72(%rbp)
	addq	%r10, %rdx
	subl	%eax, %esi
	movl	%r11d, -64(%rbp)
	call	_dl_sort_maps
	movl	800+_rtld_local_ro(%rip), %r10d
	movl	$0, -80(%rbp)
	movl	-64(%rbp), %r11d
	testl	%r10d, %r10d
	movq	-72(%rbp), %r10
	je	.L55
	movq	-144(%rbp), %rax
	movq	(%rax), %rax
	movzbl	797(%rax), %eax
	shrb	$3, %al
	xorl	$1, %eax
	andl	$1, %eax
	movl	%eax, -80(%rbp)
.L55:
	testl	%r11d, %r11d
	je	.L57
	movq	0(,%rbx,8), %rbx
	cmpq	48(%rbx), %r15
	jne	.L58
	leal	-1(%r11), %eax
	xorl	%r14d, %r14d
	movb	$0, -76(%rbp)
	movb	$0, -114(%rbp)
	movl	$-1, %r13d
	movl	$0, -96(%rbp)
	movl	%eax, -164(%rbp)
	movq	%rax, -72(%rbp)
	movl	%r11d, -120(%rbp)
	movq	%r10, -64(%rbp)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L308:
	cmpb	$2, %dl
	jne	.L61
	cmpb	$0, 799(%rbx)
	jne	.L61
	testb	$8, %al
	je	.L64
	testb	$2, _rtld_local_ro(%rip)
	jne	.L305
.L65:
	cmpq	$0, 272(%rbx)
	je	.L306
.L66:
	leaq	call_destructors(%rip), %rsi
	movq	%rbx, %rdx
	xorl	%edi, %edi
	call	_dl_catch_exception@PLT
.L64:
	movl	-80(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L68
.L71:
	movzbl	796(%rbx), %eax
	orb	$32, 797(%rbx)
	movb	$1, -76(%rbp)
	andl	$16, %eax
	cmpb	$1, %al
	sbbl	$-1, -96(%rbp)
	cmpl	%r12d, %r13d
	cmova	%r12d, %r13d
.L76:
	cmpq	%r14, -72(%rbp)
	je	.L307
.L98:
	movq	-56(%rbp), %rax
	movq	8(%rax,%r14,8), %rbx
	addq	$1, %r14
	cmpq	48(%rbx), %r15
	jne	.L58
.L59:
	movzbl	796(%rbx), %eax
	movq	-64(%rbp), %rcx
	movl	%r14d, %r12d
	movl	%eax, %edx
	andl	$3, %edx
	cmpb	$0, (%rcx,%r14)
	je	.L308
	cmpb	$2, %dl
	jne	.L76
	cmpq	$0, 704(%rbx)
	je	.L77
.L295:
	movq	920(%rbx), %r10
	movl	$1, %r8d
	movq	(%r10), %rcx
	movq	$0, -104(%rbp)
	testq	%rcx, %rcx
	je	.L79
.L78:
	leaq	8(%r10), %rsi
	leaq	720(%rbx), %rdx
	movq	%rcx, %rax
	xorl	%r9d, %r9d
	movq	%rsi, %rdi
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L311:
	cmpq	-656(%rax), %r15
	jne	.L309
	cmpl	$-1, 308(%rax)
	je	.L84
	addq	$8, %rdi
	movq	-8(%rdi), %rax
	movl	$1, %r9d
	testq	%rax, %rax
	je	.L310
.L87:
	cmpq	%rax, %rdx
	jne	.L311
.L84:
	addq	$8, %rdi
	movq	-8(%rdi), %rax
	addq	$1, %r8
	testq	%rax, %rax
	jne	.L87
.L310:
	testb	%r9b, %r9b
	je	.L88
	leaq	880(%rbx), %rax
	cmpq	%r10, %rax
	movq	%rax, -128(%rbp)
	je	.L184
	cmpq	$3, %r8
	jbe	.L175
.L184:
	movq	912(%rbx), %rax
	movq	%rdx, -152(%rbp)
	movb	%r9b, -115(%rbp)
	movq	%rax, -136(%rbp)
	leaq	0(,%rax,8), %rdi
	call	*__rtld_malloc(%rip)
	testq	%rax, %rax
	movzbl	-115(%rbp), %r9d
	movq	-152(%rbp), %rdx
	je	.L312
	movq	920(%rbx), %r10
	movq	(%r10), %rcx
	testq	%rcx, %rcx
	je	.L176
	leaq	8(%r10), %rsi
.L89:
	movb	%r9b, -115(%rbp)
	xorl	%r8d, %r8d
	movq	-104(%rbp), %r9
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L314:
	testq	%r9, %r9
	je	.L94
	movq	%r9, (%rdi)
	leaq	8(%rax,%r11), %rdi
	addq	$1, %r8
	xorl	%r9d, %r9d
.L94:
	addq	$8, %rsi
	movq	-8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L313
.L95:
	leaq	0(,%r8,8), %r11
	cmpq	%rcx, %rdx
	leaq	(%rax,%r11), %rdi
	je	.L93
	cmpl	$-1, 308(%rcx)
	jne	.L314
.L93:
	addq	$8, %rsi
	movq	%rcx, (%rdi)
	addq	$1, %r8
	movq	-8(%rsi), %rcx
	leaq	8(%rax,%r11), %rdi
	testq	%rcx, %rcx
	jne	.L95
.L313:
	movzbl	-115(%rbp), %r9d
.L92:
	cmpq	%r10, -128(%rbp)
	movq	$0, (%rdi)
	movq	%rax, 920(%rbx)
	je	.L177
	movq	%r10, %rdi
	call	_dl_scope_free
	testl	%eax, %eax
	movzbl	-114(%rbp), %eax
	movl	$0, %ecx
	cmovne	%ecx, %eax
	movb	%al, -114(%rbp)
.L96:
	movq	-136(%rbp), %rax
	movq	%rax, 912(%rbx)
.L79:
	movq	736(%rbx), %rax
	testq	%rax, %rax
	je	.L97
	cmpl	$-1, 1012(%rax)
	je	.L97
	movq	$0, 736(%rbx)
.L97:
	cmpl	%r12d, %r13d
	cmova	%r12d, %r13d
	cmpq	%r14, -72(%rbp)
	jne	.L98
.L307:
	cmpb	$0, -76(%rbp)
	movq	-64(%rbp), %r10
	movl	-120(%rbp), %r11d
	movl	%r13d, -64(%rbp)
	jne	.L315
.L57:
	cmpl	$2, dl_close_state.10771(%rip)
	jne	.L316
	movq	-160(%rbp), %rsp
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L306:
	cmpq	$0, 168(%rbx)
	jne	.L66
	movl	-80(%rbp), %r9d
	testl	%r9d, %r9d
	je	.L71
.L68:
	movl	800+_rtld_local_ro(%rip), %r8d
	movq	792+_rtld_local_ro(%rip), %rcx
	testl	%r8d, %r8d
	je	.L71
	xorl	%edx, %edx
	movl	%r13d, -76(%rbp)
	movl	%r12d, -104(%rbp)
	movq	%rcx, %r13
	movq	%rbx, %r12
	movl	%edx, %ebx
	.p2align 4,,10
	.p2align 3
.L75:
	movq	56(%r13), %rax
	testq	%rax, %rax
	je	.L72
	movl	%ebx, %edx
	leaq	2568+_rtld_local(%rip), %rcx
	movq	%rdx, %rdi
	salq	$4, %rdi
	cmpq	%rcx, %r12
	leaq	1160(%r12,%rdi), %rdi
	je	.L317
.L74:
	call	*%rax
.L72:
	addl	$1, %ebx
	cmpl	800+_rtld_local_ro(%rip), %ebx
	movq	64(%r13), %r13
	jb	.L75
	movq	%r12, %rbx
	movl	-76(%rbp), %r13d
	movl	-104(%rbp), %r12d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L317:
	addq	$233, %rdx
	subq	$2568, %rcx
	salq	$4, %rdx
	leaq	(%rcx,%rdx), %rdi
	jmp	.L74
.L88:
	cmpq	$0, -104(%rbp)
	je	.L79
.L83:
	movq	$0, 704(%rbx)
	movl	$0, 712(%rbx)
	jmp	.L79
.L77:
	movq	976(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L295
	cmpq	$0, 8(%rdx)
	je	.L172
	movl	$1, %ecx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L173:
	movl	%eax, %ecx
.L82:
	leal	1(%rcx), %esi
	cmpq	$0, (%rdx,%rsi,8)
	movq	%rsi, %rax
	jne	.L173
	addl	$2, %ecx
	salq	$3, %rcx
.L81:
	movq	920(%rbx), %r10
	addq	%rcx, %rdx
	movl	%eax, 712(%rbx)
	leaq	704(%rbx), %rax
	movq	%rdx, 704(%rbx)
	movq	(%r10), %rcx
	movq	%rax, -104(%rbp)
	testq	%rcx, %rcx
	je	.L83
	movl	$2, %r8d
	jmp	.L78
.L305:
	movq	8(%rbx), %rsi
	leaq	.LC9(%rip), %rdi
	movq	%r15, %rdx
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L65
.L177:
	movb	%r9b, -114(%rbp)
	jmp	.L96
.L175:
	movq	-128(%rbp), %rax
	movq	$4, -136(%rbp)
	jmp	.L89
.L315:
	movl	-80(%rbp), %edi
	testl	%edi, %edi
	jne	.L100
.L103:
	movq	%r15, %rsi
	xorl	%edi, %edi
	movq	%r10, -128(%rbp)
	movl	%r11d, -72(%rbp)
	call	_dl_debug_initialize
	movl	$2, 24(%rax)
	movq	%rax, -104(%rbp)
	call	__GI__dl_debug_state
	movl	-96(%rbp), %esi
	movl	-72(%rbp), %r11d
	movq	-128(%rbp), %r10
	testl	%esi, %esi
	jne	.L318
.L102:
#APP
# 530 "dl-close.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L115
.L119:
	movq	%r10, -96(%rbp)
	movl	%r11d, -72(%rbp)
	leaq	2480+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	movl	-72(%rbp), %r11d
	cmpl	-64(%rbp), %r11d
	movq	-96(%rbp), %r10
	jbe	.L319
	movl	-64(%rbp), %eax
	movq	-56(%rbp), %rsi
	movl	-164(%rbp), %r14d
	movb	$0, -64(%rbp)
	movq	$0, -72(%rbp)
	movq	$0, -96(%rbp)
	movq	%r15, -128(%rbp)
	movq	%rax, %rcx
	leaq	(%r10,%rax), %rbx
	leaq	(%rsi,%rax,8), %r13
	leaq	1(%r10,%rax), %rax
	subl	%ecx, %r14d
	addq	%r14, %rax
	movq	-144(%rbp), %r14
	movq	%rax, -56(%rbp)
	.p2align 4,,10
	.p2align 3
.L148:
	cmpb	$0, (%rbx)
	jne	.L121
	movq	0(%r13), %r12
	movzbl	796(%r12), %eax
	movl	%eax, %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L320
	cmpq	$0, 1088(%r12)
	jne	.L321
.L123:
	cmpb	$0, -113(%rbp)
	je	.L130
	movq	-112(%rbp), %rdi
	call	*3984+_rtld_local(%rip)
	movq	80(%r14), %rax
	testq	%rax, %rax
	je	.L132
	movq	88(%r14), %rdx
	testq	%rdx, %rdx
	je	.L132
	salq	$5, %rdx
	addq	%rax, %rdx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L133:
	addq	$32, %rax
	cmpq	%rax, %rdx
	je	.L132
.L134:
	cmpq	$0, 8(%rax)
	je	.L133
	cmpq	24(%rax), %r12
	jne	.L133
	movq	$0, 8(%rax)
	movl	$0, (%rax)
	addq	$32, %rax
	subq	$1, 96(%r14)
	cmpq	%rax, %rdx
	jne	.L134
	.p2align 4,,10
	.p2align 3
.L132:
	movq	-112(%rbp), %rdi
	call	*3992+_rtld_local(%rip)
.L130:
	movq	%r12, %rdi
	call	_dl_unmap
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.L322
	movq	24(%r12), %rdx
	movq	%rdx, 24(%rax)
	movq	24(%r12), %rdx
.L137:
	subl	$1, 8(%r14)
	testq	%rdx, %rdx
	je	.L138
	movq	%rax, 32(%rdx)
.L138:
	movq	744(%r12), %rdi
	call	*__rtld_free(%rip)
	movq	848(%r12), %rdi
	cmpq	$-1, %rdi
	je	.L139
	call	*__rtld_free(%rip)
.L139:
	movq	984(%r12), %rdi
	call	*__rtld_free(%rip)
	testb	$64, _rtld_local_ro(%rip)
	jne	.L323
.L140:
	movq	8(%r12), %rdi
	call	*__rtld_free(%rip)
	movq	56(%r12), %rdi
	movq	__rtld_free(%rip), %rax
	.p2align 4,,10
	.p2align 3
.L142:
	movl	16(%rdi), %edx
	movq	8(%rdi), %r15
	testl	%edx, %edx
	jne	.L141
	call	*%rax
	movq	__rtld_free(%rip), %rax
.L141:
	testq	%r15, %r15
	movq	%r15, %rdi
	jne	.L142
	movq	976(%r12), %rdi
	call	*%rax
	movq	920(%r12), %rdi
	leaq	880(%r12), %rax
	cmpq	%rax, %rdi
	je	.L143
	call	*__rtld_free(%rip)
.L143:
	cmpb	$0, 796(%r12)
	movq	__rtld_free(%rip), %rax
	jns	.L144
	movq	680(%r12), %rdi
	call	*%rax
	movq	__rtld_free(%rip), %rax
.L144:
	movq	816(%r12), %rdi
	cmpq	$-1, %rdi
	je	.L145
	call	*%rax
	movq	__rtld_free(%rip), %rax
.L145:
	movq	960(%r12), %rdi
	cmpq	$-1, %rdi
	je	.L146
	call	*%rax
	movq	__rtld_free(%rip), %rax
.L146:
	cmpq	2528+_rtld_local(%rip), %r12
	je	.L324
.L147:
	movq	%r12, %rdi
	call	*%rax
.L121:
	addq	$1, %rbx
	addq	$8, %r13
	cmpq	%rbx, -56(%rbp)
	jne	.L148
	movq	-128(%rbp), %r15
	leaq	2480+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	cmpb	$0, -64(%rbp)
	je	.L150
	movq	4088+_rtld_local(%rip), %rax
	addq	$1, %rax
	testq	%rax, %rax
	movq	%rax, 4088+_rtld_local(%rip)
	je	.L325
	movq	-72(%rbp), %rax
	cmpq	%rax, 4056+_rtld_local(%rip)
	jne	.L150
	movq	-96(%rbp), %rax
	movq	%rax, 4056+_rtld_local(%rip)
.L150:
	movq	-144(%rbp), %rax
	movq	(%rax), %r12
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L326
.L154:
	testq	%r12, %r12
	je	.L164
.L159:
	movq	-104(%rbp), %rax
	movl	$0, 24(%rax)
	call	__GI__dl_debug_state
	jmp	.L57
.L322:
	cmpq	$0, -128(%rbp)
	je	.L327
	movq	24(%r12), %rdx
	movq	-104(%rbp), %rcx
	movq	%rdx, (%r14)
	movq	%rdx, 8(%rcx)
	jmp	.L137
.L324:
	movq	$0, 2528+_rtld_local(%rip)
	jmp	.L147
.L323:
	movq	48(%r12), %rdx
	movq	8(%r12), %rsi
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L140
.L321:
	movq	4032+_rtld_local(%rip), %rsi
	testq	%rsi, %rsi
	je	.L125
	shrb	$3, %al
	movq	1120(%r12), %rdi
	xorl	%edx, %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	call	remove_slotinfo
	testb	%al, %al
	jne	.L125
	movq	4040+_rtld_local(%rip), %rax
	movq	%rax, 4024+_rtld_local(%rip)
.L125:
	movq	1112(%r12), %rax
	movzbl	-76(%rbp), %ecx
	leaq	1(%rax), %rdx
	movb	%cl, -64(%rbp)
	cmpq	$1, %rdx
	jbe	.L123
	movq	-96(%rbp), %rcx
	movq	%rax, %rdx
	cmpq	%rcx, %rax
	sete	%sil
	testq	%rcx, %rcx
	sete	%cl
	subq	1088(%r12), %rdx
	orb	%cl, %sil
	movb	%sil, -64(%rbp)
	je	.L127
	cmpq	$0, -72(%rbp)
	jne	.L297
	movq	%rax, -72(%rbp)
.L297:
	movq	%rdx, -96(%rbp)
	jmp	.L123
.L176:
	movq	%rax, %rdi
	jmp	.L92
.L300:
	movq	8(%rdi), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L32
.L47:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$223, %edx
	call	__GI___assert_fail
.L318:
	movq	-144(%rbp), %rax
	movq	16(%rax), %r8
	movl	8(%r8), %esi
	testl	%esi, %esi
	je	.L179
	movq	(%r8), %rdx
	leal	-1(%rsi), %ecx
	movq	%rcx, %rax
	movq	(%rdx,%rcx,8), %rcx
	testb	$32, 797(%rcx)
	jne	.L109
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L111:
	leal	-1(%rax), %edi
	movq	%rdi, %rcx
	movq	(%rdx,%rdi,8), %rdi
	testb	$32, 797(%rdi)
	je	.L110
	movl	%ecx, %eax
.L109:
	testl	%eax, %eax
	jne	.L111
.L108:
	movl	%eax, 8(%r8)
	jmp	.L102
.L172:
	movl	$16, %ecx
	movl	$1, %eax
	jmp	.L81
.L127:
	movq	-72(%rbp), %rsi
	cmpq	%rsi, %rdx
	je	.L182
	movq	4056+_rtld_local(%rip), %rcx
	cmpq	%rsi, %rcx
	je	.L329
	cmpq	%rcx, %rax
	je	.L330
	cmpq	-72(%rbp), %rax
	jbe	.L296
	movq	%rax, -72(%rbp)
	movq	%rdx, -96(%rbp)
.L296:
	movzbl	-76(%rbp), %eax
	movb	%al, -64(%rbp)
	jmp	.L123
.L328:
	movl	%esi, %eax
.L110:
	movl	-96(%rbp), %ebx
	leal	(%rax,%rbx), %ecx
	cmpl	%ecx, %esi
	je	.L108
	leal	-1(%rax), %r9d
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	addq	$1, %r9
.L114:
	movq	(%rdx,%rcx,8), %rsi
	testb	$32, 797(%rsi)
	jne	.L112
	cmpl	%ecx, %eax
	je	.L113
	movl	%eax, %edi
	movq	%rsi, (%rdx,%rdi,8)
.L113:
	addl	$1, %eax
.L112:
	addq	$1, %rcx
	cmpq	%rcx, %r9
	jne	.L114
	jmp	.L108
.L325:
	leaq	.LC16(%rip), %rdi
	call	__GI__dl_fatal_printf
.L329:
	movq	%rax, -72(%rbp)
	movzbl	-76(%rbp), %eax
	movq	-96(%rbp), %rcx
	movq	%rdx, -96(%rbp)
	movq	%rcx, 4056+_rtld_local(%rip)
	movb	%al, -64(%rbp)
	jmp	.L123
.L182:
	movq	%rax, -72(%rbp)
	movzbl	-76(%rbp), %eax
	movb	%al, -64(%rbp)
	jmp	.L123
.L164:
	movq	2432+_rtld_local(%rip), %rdx
	leaq	-1(%rdx), %rax
	cmpq	%r15, %rax
	jne	.L159
	imulq	$152, %rdx, %rdx
	leaq	_rtld_local(%rip), %rbx
	leaq	-304(%rbx,%rdx), %rdx
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L331:
	subq	$1, %rax
.L165:
	subq	$152, %rdx
	cmpq	$0, 152(%rdx)
	je	.L331
	movq	%rax, 2432+_rtld_local(%rip)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L326:
	testq	%r12, %r12
	je	.L164
	testb	$8, 797(%r12)
	jne	.L159
	cmpl	$0, 800+_rtld_local_ro(%rip)
	movq	792+_rtld_local_ro(%rip), %r14
	je	.L159
	xorl	%r13d, %r13d
	leaq	2568+_rtld_local(%rip), %rbx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L162:
	xorl	%esi, %esi
	call	*%rdx
.L160:
	addl	$1, %r13d
	cmpl	800+_rtld_local_ro(%rip), %r13d
	movq	64(%r14), %r14
	jnb	.L332
.L163:
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L160
	movl	%r13d, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	%rbx, %r12
	leaq	1160(%r12,%rcx), %rdi
	jne	.L162
	addq	$233, %rax
	leaq	_rtld_local(%rip), %rcx
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdi
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L115:
	movl	-96(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L118
	cmpb	$0, -114(%rbp)
	jne	.L118
	movq	4104+_rtld_local(%rip), %rax
	testq	%rax, %rax
	je	.L119
	cmpq	$0, (%rax)
	je	.L119
.L118:
	movq	%r10, -96(%rbp)
	movl	%r11d, -72(%rbp)
	call	__thread_gscope_wait
	movq	4104+_rtld_local(%rip), %rbx
	movl	-72(%rbp), %r11d
	movq	-96(%rbp), %r10
	testq	%rbx, %rbx
	je	.L119
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L119
	movl	%r11d, %r12d
	movq	%r10, %r13
.L120:
	subq	$1, %rax
	movq	%rax, (%rbx)
	movq	8(%rbx,%rax,8), %rdi
	call	*__rtld_free(%rip)
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L120
	movl	%r12d, %r11d
	movq	%r13, %r10
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L319:
	leaq	2480+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	jmp	.L150
.L309:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$372, %edx
	call	__GI___assert_fail
.L61:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$282, %edx
	call	__GI___assert_fail
.L58:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$278, %edx
	call	__GI___assert_fail
.L316:
	movl	$0, dl_close_state.10771(%rip)
	movq	-160(%rbp), %rsp
	jmp	.L32
.L100:
	movq	-144(%rbp), %rax
	movq	(%rax), %r13
	testb	$8, 797(%r13)
	jne	.L103
	cmpl	$0, 800+_rtld_local_ro(%rip)
	je	.L103
	movq	792+_rtld_local_ro(%rip), %r12
	leaq	2568+_rtld_local(%rip), %r14
	xorl	%ebx, %ebx
.L107:
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L104
	movl	%ebx, %eax
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	%r14, %r13
	leaq	1160(%r13,%rcx), %rdi
	jne	.L106
	addq	$233, %rax
	leaq	_rtld_local(%rip), %rcx
	salq	$4, %rax
	leaq	(%rcx,%rax), %rdi
.L106:
	movq	%r10, -104(%rbp)
	movl	%r11d, -72(%rbp)
	movl	$2, %esi
	call	*%rdx
	movq	-104(%rbp), %r10
	movl	-72(%rbp), %r11d
.L104:
	addl	$1, %ebx
	cmpl	800+_rtld_local_ro(%rip), %ebx
	movq	64(%r12), %r12
	jb	.L107
	jmp	.L103
.L332:
	imulq	$152, %r15, %rax
	leaq	_rtld_local(%rip), %rbx
	movq	(%rbx,%rax), %r12
	jmp	.L154
.L302:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$181, %edx
	call	__GI___assert_fail
.L312:
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error@PLT
.L330:
	movzbl	-76(%rbp), %eax
	movq	%rdx, 4056+_rtld_local(%rip)
	movb	%al, -64(%rbp)
	jmp	.L123
.L327:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$704, %edx
	call	__GI___assert_fail
.L179:
	xorl	%eax, %eax
	jmp	.L108
.L320:
	leaq	__PRETTY_FUNCTION__.10785(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$559, %edx
	call	__GI___assert_fail
	.size	_dl_close_worker, .-_dl_close_worker
	.section	.rodata.str1.1
.LC17:
	.string	"shared object not open"
	.text
	.p2align 4,,15
	.globl	_dl_close
	.hidden	_dl_close
	.type	_dl_close, @function
_dl_close:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3984+_rtld_local(%rip)
	cmpb	$0, 799(%rbx)
	jne	.L337
	movl	792(%rbx), %eax
	testl	%eax, %eax
	je	.L338
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_close_worker
.L337:
	popq	%rbx
	leaq	2440+_rtld_local(%rip), %rdi
	jmp	*3992+_rtld_local(%rip)
	.p2align 4,,10
	.p2align 3
.L338:
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	movq	8(%rbx), %rsi
	leaq	.LC17(%rip), %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	_dl_signal_error@PLT
	.size	_dl_close, .-_dl_close
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10749, @object
	.size	__PRETTY_FUNCTION__.10749, 16
__PRETTY_FUNCTION__.10749:
	.string	"remove_slotinfo"
	.align 16
	.type	__PRETTY_FUNCTION__.10785, @object
	.size	__PRETTY_FUNCTION__.10785, 17
__PRETTY_FUNCTION__.10785:
	.string	"_dl_close_worker"
	.local	dl_close_state.10771
	.comm	dl_close_state.10771,4,4
	.hidden	__thread_gscope_wait
	.hidden	__rtld_free
	.hidden	_dl_unmap
	.hidden	_dl_debug_initialize
	.hidden	_dl_debug_printf
	.hidden	_dl_scope_free
	.hidden	__rtld_malloc
	.hidden	_dl_sort_maps
	.hidden	_rtld_local_ro
	.hidden	_rtld_local
