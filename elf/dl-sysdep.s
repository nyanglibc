	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"AVX"
.LC1:
	.string	"CX8"
.LC2:
	.string	"FMA"
.LC3:
	.string	"HTT"
.LC4:
	.string	"IBT"
.LC5:
	.string	"RTM"
.LC6:
	.string	"LZCNT"
.LC7:
	.string	"MOVBE"
.LC8:
	.string	"SHSTK"
.LC9:
	.string	"SSSE3"
.LC10:
	.string	"POPCNT"
.LC11:
	.string	"SSE4_1"
.LC12:
	.string	"SSE4_2"
.LC13:
	.string	"XSAVEC"
.LC14:
	.string	"AVX512F"
.LC15:
	.string	"OSXSAVE"
.LC16:
	.string	"Prefer_ERMS"
.LC17:
	.string	"Prefer_FSRM"
.LC18:
	.string	"Slow_SSE4_2"
.LC19:
	.string	"Fast_Rep_String"
.LC20:
	.string	"Prefer_No_AVX512"
.LC21:
	.string	"Fast_Copy_Backward"
.LC22:
	.string	"Fast_Unaligned_Load"
.LC23:
	.string	"Fast_Unaligned_Copy"
.LC24:
	.string	"Prefer_No_VZEROUPPER"
.LC25:
	.string	"Prefer_MAP_32BIT_EXEC"
.LC26:
	.string	"AVX_Fast_Unaligned_Load"
.LC27:
	.string	"MathVec_Prefer_No_AVX512"
.LC28:
	.string	"Prefer_PMINUB_for_stringop"
	.text
	.p2align 4,,15
	.globl	_dl_tunable_set_hwcaps
	.hidden	_dl_tunable_set_hwcaps
	.type	_dl_tunable_set_hwcaps, @function
_dl_tunable_set_hwcaps:
	pushq	%r15
	pushq	%r14
	leaq	.L128(%rip), %r8
	pushq	%r13
	pushq	%r12
	movabsq	$6287643216692401729, %r13
	pushq	%rbp
	pushq	%rbx
	movabsq	$5855860602418255425, %r12
	movq	(%rdi), %rax
	movl	296+_dl_x86_cpu_features(%rip), %r14d
	leaq	.L122(%rip), %rdi
	movabsq	$4918830404948481601, %rbx
	movabsq	$5928199671432894017, %rbp
	movabsq	$5067466983515057235, %r11
	movabsq	$5791473425656934992, %r10
	movzbl	(%rax), %edx
	.p2align 4,,10
	.p2align 3
.L121:
	cmpb	$44, %dl
	je	.L129
	testb	%dl, %dl
	je	.L129
	movq	%rax, %rcx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L176:
	cmpb	$44, %sil
	je	.L143
.L3:
	addq	$1, %rcx
	movzbl	(%rcx), %esi
	testb	%sil, %sil
	jne	.L176
.L143:
	subq	%rax, %rcx
	cmpb	$45, %dl
	leaq	1(%rcx), %rsi
	je	.L177
	subq	$5, %rcx
	cmpq	$21, %rcx
	ja	.L2
	movslq	(%rdi,%rcx,4), %rcx
	addq	%rdi, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L122:
	.long	.L132-.L122
	.long	.L133-.L122
	.long	.L134-.L122
	.long	.L123-.L122
	.long	.L2-.L122
	.long	.L2-.L122
	.long	.L135-.L122
	.long	.L2-.L122
	.long	.L2-.L122
	.long	.L2-.L122
	.long	.L136-.L122
	.long	.L137-.L122
	.long	.L2-.L122
	.long	.L138-.L122
	.long	.L139-.L122
	.long	.L140-.L122
	.long	.L141-.L122
	.long	.L2-.L122
	.long	.L142-.L122
	.long	.L131-.L122
	.long	.L2-.L122
	.long	.L130-.L122
	.text
.L7:
	cmpw	$22081, (%r9)
	je	.L178
.L8:
	cmpw	$22595, (%r9)
	je	.L179
.L11:
	cmpw	$19782, (%r9)
	je	.L180
.L14:
	cmpw	$21576, (%r9)
	je	.L181
.L17:
	cmpw	$16969, (%r9)
	je	.L182
.L20:
	cmpw	$21586, (%r9)
	jne	.L2
	cmpb	$77, 2(%r9)
	jne	.L2
	andl	$-2049, 72+_dl_x86_cpu_features(%rip)
	.p2align 4,,10
	.p2align 3
.L2:
	addq	%rsi, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L121
.L183:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	subq	$4, %rcx
	leaq	1(%rax), %r9
	cmpq	$23, %rcx
	ja	.L2
	movslq	(%r8,%rcx,4), %rcx
	addq	%r8, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L128:
	.long	.L7-.L128
	.long	.L125-.L128
	.long	.L151-.L128
	.long	.L152-.L128
	.long	.L153-.L128
	.long	.L62-.L128
	.long	.L2-.L128
	.long	.L2-.L128
	.long	.L69-.L128
	.long	.L2-.L128
	.long	.L2-.L128
	.long	.L2-.L128
	.long	.L81-.L128
	.long	.L85-.L128
	.long	.L2-.L128
	.long	.L89-.L128
	.long	.L93-.L128
	.long	.L101-.L128
	.long	.L105-.L128
	.long	.L2-.L128
	.long	.L109-.L128
	.long	.L113-.L128
	.long	.L2-.L128
	.long	.L117-.L128
	.text
	.p2align 4,,10
	.p2align 3
.L129:
	movl	$1, %esi
	addq	%rsi, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L121
	jmp	.L183
.L123:
	cmpq	%r11, (%rax)
	jne	.L2
	orl	$64, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L134:
	cmpb	$45, %dl
	movq	%rax, %r9
	jne	.L2
.L153:
	cmpl	$894981697, (%r9)
	je	.L184
.L57:
	cmpl	$1398297423, (%r9)
	jne	.L2
	cmpw	$22081, 4(%r9)
	jne	.L2
	cmpb	$69, 6(%r9)
	jne	.L2
	andl	$-134217729, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$45, %dl
	movq	%rax, %r9
	jne	.L2
.L152:
	cmpl	$1129336656, (%r9)
	je	.L185
.L45:
	cmpl	$876958547, (%r9)
	je	.L186
.L48:
	cmpl	$876958547, (%r9)
	je	.L187
.L51:
	cmpl	$1447121752, (%r9)
	jne	.L2
	cmpw	$17221, 4(%r9)
	jne	.L2
	movq	%r14, 288+_dl_x86_cpu_features(%rip)
	andl	$-3, 132+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L132:
	cmpb	$45, %dl
	movq	%rax, %r9
	jne	.L2
.L151:
	cmpl	$1313036876, (%r9)
	je	.L188
.L33:
	cmpl	$1112952653, (%r9)
	je	.L189
.L36:
	cmpl	$1414744147, (%r9)
	je	.L190
.L39:
	cmpl	$1163088723, (%r9)
	jne	.L2
	cmpb	$51, 4(%r9)
	jne	.L2
	andl	$-513, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%rax, %r9
.L113:
	movabsq	$5647358237581079120, %r15
	movabsq	$6873448878091559245, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	movabsq	$3616730629275737967, %rcx
	cmpq	%rcx, 16(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L191
	testb	$1, 74+_dl_x86_cpu_features(%rip)
	je	.L2
	orl	$32768, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L130:
	movq	%rax, %r9
.L117:
	movabsq	$8027208124338161997, %rcx
	movq	(%r9), %r15
	xorq	8(%r9), %rcx
	xorq	%r10, %r15
	orq	%r15, %rcx
	jne	.L2
	movabsq	$7453010373645655922, %rcx
	cmpq	%rcx, 16(%r9)
	jne	.L2
	cmpw	$28783, 24(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L192
	testb	$4, 51+_dl_x86_cpu_features(%rip)
	je	.L2
	orl	$1024, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L142:
	movq	%rax, %r9
.L109:
	movabsq	$7955443180985275743, %r15
	movabsq	$8391157485596595777, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpl	$1281320037, 16(%r9)
	jne	.L2
	cmpw	$24943, 20(%r9)
	jne	.L2
	cmpb	$100, 22(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L193
	testb	$16, 47+_dl_x86_cpu_features(%rip)
	je	.L2
	orl	$256, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L141:
	movq	%rax, %r9
.L105:
	movabsq	$6073458355863507009, %r15
	movabsq	$5575300643543151184, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpl	$1163412831, 16(%r9)
	jne	.L2
	cmpb	$67, 20(%r9)
	jne	.L2
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andb	$-3, %ch
	orl	$512, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L140:
	movq	%rax, %r9
.L101:
	movabsq	$6147222474205847407, %r15
	movabsq	$5647358237581079120, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpl	$1380274256, 16(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L194
	testb	$16, 47+_dl_x86_cpu_features(%rip)
	je	.L2
	orl	$2048, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L139:
	movq	%rax, %r9
.L93:
	movabsq	$5503227656476780908, %r15
	movabsq	$7020642737581154630, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L94
	cmpw	$24943, 16(%r9)
	je	.L195
.L94:
	movabsq	$4854709310135429484, %r15
	movabsq	$7020642737581154630, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpw	$28783, 16(%r9)
	jne	.L2
	cmpb	$121, 18(%r9)
	jne	.L2
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-33, %ecx
	orl	$32, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rax, %r9
.L89:
	movabsq	$7023200218485251961, %r15
	movabsq	$8101768331917484358, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpw	$25714, 16(%r9)
	jne	.L2
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-9, %ecx
	orl	$8, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L136:
	movq	%rax, %r9
.L81:
	movabsq	$8098970074824794438, %rcx
	cmpq	%rcx, (%r9)
	jne	.L2
	cmpl	$1920226143, 8(%r9)
	jne	.L2
	cmpw	$28265, 12(%r9)
	jne	.L2
	cmpb	$103, 14(%r9)
	jne	.L2
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-5, %ecx
	orl	$4, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L135:
	movq	%rax, %r9
.L69:
	movabsq	$4998839891239727696, %rcx
	cmpq	%rcx, (%r9)
	je	.L196
.L70:
	movabsq	$5070897485277655632, %rcx
	cmpq	%rcx, (%r9)
	je	.L197
.L74:
	movabsq	$4995428081174801491, %rcx
	cmpq	%rcx, (%r9)
	jne	.L2
	cmpw	$24372, 8(%r9)
	jne	.L2
	cmpb	$50, 10(%r9)
	jne	.L2
	cmpb	$45, %dl
	je	.L198
	testb	$16, 46+_dl_x86_cpu_features(%rip)
	je	.L2
	orl	$128, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L137:
	movq	%rax, %r9
.L85:
	movabsq	$3616730629275737967, %r15
	movabsq	$5647358237581079120, %rcx
	xorq	8(%r9), %r15
	xorq	(%r9), %rcx
	orq	%rcx, %r15
	jne	.L2
	cmpb	$45, %dl
	je	.L199
	testb	$1, 74+_dl_x86_cpu_features(%rip)
	je	.L2
	orl	$16384, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L125:
	cmpl	$844650049, 1(%rax)
	jne	.L200
	andl	$-33, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L62:
	cmpq	%rbx, 1(%rax)
	jne	.L63
	andl	$-268435457, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L195:
	cmpb	$100, 18(%r9)
	jne	.L94
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andl	$-17, %ecx
	orl	$16, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L199:
	andl	$-16385, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L196:
	cmpw	$19794, 8(%r9)
	jne	.L70
	cmpb	$83, 10(%r9)
	jne	.L70
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andb	$-17, %ch
	orl	$4096, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L197:
	cmpw	$21075, 8(%r9)
	jne	.L74
	cmpb	$77, 10(%r9)
	jne	.L74
	movl	276+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %r9d
	andb	$-33, %ch
	orl	$8192, %r9d
	cmpb	$45, %dl
	movl	%r9d, %edx
	cmove	%ecx, %edx
	movl	%edx, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L200:
	cmpl	$826887490, 1(%rax)
	je	.L201
	cmpl	$843664706, 1(%rax)
	jne	.L26
	andl	$-257, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L63:
	cmpq	%r13, 1(%rax)
	je	.L202
	cmpq	%r12, 1(%rax)
	jne	.L65
	andl	$-131073, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L191:
	andl	$-32769, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L194:
	andl	$-2049, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L184:
	cmpw	$12849, 4(%r9)
	jne	.L57
	cmpb	$70, 6(%r9)
	jne	.L57
	andl	$-65537, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L178:
	cmpb	$88, 2(%r9)
	jne	.L8
	andl	$-268435457, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L188:
	cmpb	$84, 4(%r9)
	jne	.L33
	andl	$-33, 108+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L185:
	cmpw	$21582, 4(%r9)
	jne	.L45
	andl	$-8388609, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L202:
	andl	$-1073741825, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L201:
	andl	$-9, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L187:
	cmpw	$12895, 4(%r9)
	jne	.L51
	andl	$-1048577, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L190:
	cmpb	$75, 4(%r9)
	jne	.L39
	andl	$-129, 76+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L180:
	cmpb	$65, 2(%r9)
	jne	.L14
	andl	$-4097, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L192:
	andl	$-1025, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L193:
	andl	$-257, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L186:
	cmpw	$12639, 4(%r9)
	jne	.L48
	andl	$-524289, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L179:
	cmpb	$56, 2(%r9)
	jne	.L11
	andl	$-257, 48+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L189:
	cmpb	$69, 4(%r9)
	jne	.L36
	andl	$-4194305, 44+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L65:
	cmpq	%rbp, 1(%rax)
	jne	.L66
	andl	$-134217729, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L26:
	cmpl	$1448037699, 1(%rax)
	jne	.L27
	andl	$-32769, 48+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L198:
	andl	$-129, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L66:
	movabsq	$5066604767721576001, %rdx
	cmpq	%rdx, 1(%rax)
	jne	.L67
	andl	$-67108865, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L27:
	cmpl	$1397576261, 1(%rax)
	jne	.L28
	andl	$-513, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L67:
	movabsq	$5500639181809407553, %rdx
	cmpq	%rdx, 1(%rax)
	jne	.L68
	andl	$2147483647, 72+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	$876694854, 1(%rax)
	jne	.L29
	andl	$-65537, 108+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L182:
	cmpb	$84, 2(%r9)
	jne	.L20
	andl	$-1048577, 80+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L181:
	cmpb	$84, 2(%r9)
	jne	.L17
	andl	$-268435457, 48+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L68:
	cmpq	%r11, 1(%rax)
	jne	.L2
	andl	$-65, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L29:
	cmpl	$843404115, 1(%rax)
	jne	.L30
	andl	$-67108865, 48+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L30:
	cmpl	$909653321, 1(%rax)
	jne	.L31
	andl	$-2, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
.L31:
	cmpl	$909653577, 1(%rax)
	jne	.L2
	andl	$-3, 276+_dl_x86_cpu_features(%rip)
	jmp	.L2
	.size	_dl_tunable_set_hwcaps, .-_dl_tunable_set_hwcaps
	.section	.rodata.str1.1
.LC29:
	.string	"/proc/sys/kernel/osrelease"
	.text
	.p2align 4,,15
	.globl	_dl_discover_osversion
	.hidden	_dl_discover_osversion
	.type	_dl_discover_osversion, @function
_dl_discover_osversion:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$464, %rsp
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	call	__uname
	testl	%eax, %eax
	leaq	130(%rbx), %rsi
	jne	.L227
.L204:
	xorl	%edi, %edi
	xorl	%eax, %eax
.L209:
	movsbl	(%rsi), %ecx
	leal	-48(%rcx), %edx
	cmpb	$9, %dl
	ja	.L215
	movsbl	1(%rsi), %edx
	leaq	1(%rsi), %r8
	subl	$48, %ecx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	ja	.L210
	.p2align 4,,10
	.p2align 3
.L211:
	leal	(%rcx,%rcx,4), %ecx
	addq	$1, %r8
	leal	-48(%rdx,%rcx,2), %ecx
	movsbl	(%r8), %edx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	jbe	.L211
.L210:
	sall	$8, %eax
	addl	$1, %edi
	leaq	1(%r8), %rsi
	orl	%ecx, %eax
	cmpb	$46, %dl
	jne	.L212
	cmpl	$3, %edi
	jne	.L209
.L203:
	addq	$464, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L212:
	cmpl	$3, %edi
	je	.L203
.L215:
	movl	$3, %ecx
	addq	$464, %rsp
	subl	%edi, %ecx
	sall	$3, %ecx
	popq	%rbx
	sall	%cl, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L227:
	leaq	.LC29(%rip), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__open64_nocancel
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L207
	movq	%rsp, %rbx
	movl	$64, %edx
	movl	%eax, %edi
	movq	%rbx, %rsi
	call	__read_nocancel
	movl	%r12d, %edi
	movq	%rax, %rbp
	call	__close_nocancel
	testq	%rbp, %rbp
	jle	.L207
	cmpq	$63, %rbp
	movl	$63, %eax
	movq	%rbx, %rsi
	cmovge	%rax, %rbp
	movb	$0, (%rsp,%rbp)
	jmp	.L204
.L207:
	movl	$-1, %eax
	jmp	.L203
	.size	_dl_discover_osversion, .-_dl_discover_osversion
	.hidden	__close_nocancel
	.hidden	__read_nocancel
	.hidden	__open64_nocancel
	.hidden	__uname
