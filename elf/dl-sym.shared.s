	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	call_dl_lookup, @function
call_dl_lookup:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rsi
	movl	24(%rbx), %eax
	movq	40(%rdi), %rdx
	xorl	%r9d, %r9d
	movq	8(%rdi), %rdi
	movq	16(%rbx), %r8
	movq	920(%rsi), %rcx
	pushq	$0
	pushq	%rax
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	call	*752(%rax)
	movq	%rax, (%rbx)
	popq	%rax
	popq	%rdx
	popq	%rbx
	ret
	.size	call_dl_lookup, .-call_dl_lookup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"RTLD_NEXT used in code not dynamically loaded"
	.text
	.p2align 4,,15
	.type	do_sym, @function
do_sym:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rsi, %rbx
	subq	$168, %rsp
	testq	%rdi, %rdi
	movq	$0, 72(%rsp)
	je	.L58
	cmpq	$-1, %rdi
	je	.L59
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	leaq	72(%rsp), %rdx
	leaq	928(%rdi), %rcx
	pushq	$0
	movq	%rdi, %rsi
	pushq	%r8
	xorl	%r9d, %r9d
	movq	%rbp, %r8
	movq	%rbx, %rdi
	call	*752(%rax)
	movq	%rax, %rbp
	xorl	%r12d, %r12d
	popq	%rax
	popq	%rdx
.L8:
	movq	72(%rsp), %rbx
	testq	%rbx, %rbx
	je	.L32
	movzbl	4(%rbx), %edx
	movq	8(%rbx), %rax
	andl	$15, %edx
	cmpb	$6, %dl
	je	.L60
	cmpw	$-15, 6(%rbx)
	je	.L19
	testq	%rbp, %rbp
	je	.L19
	addq	0(%rbp), %rax
.L19:
	movq	%rax, %r14
.L18:
	cmpb	$10, %dl
	je	.L61
.L20:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	movl	800(%rdx), %r15d
	testl	%r15d, %r15d
	jne	.L62
.L4:
	addq	$168, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movq	1120(%rbp), %rdx
	leaq	112(%rsp), %rdi
	movq	%rax, 120(%rsp)
	movq	%rdx, 112(%rsp)
	call	__tls_get_addr@PLT
	movq	72(%rsp), %rbx
	movq	%rax, %r14
	movzbl	4(%rbx), %edx
	andl	$15, %edx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rdx, %rdi
	movl	%r8d, (%rsp)
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	movl	(%rsp), %r8d
	je	.L63
.L6:
#APP
# 104 "dl-sym.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	orl	$1, %r8d
	movq	920(%r12), %rcx
	leaq	72(%rsp), %rdx
	pushq	$0
	pushq	%r8
.L57:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	%rbp, %r8
	movq	%r12, %rsi
	xorl	%r9d, %r9d
	movq	%rbx, %rdi
	call	*752(%rax)
	popq	%rcx
	movq	%rax, %rbp
	popq	%rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L62:
	movq	104(%rbp), %rax
	testq	%r12, %r12
	movq	8(%rax), %rax
	movq	%rax, 32(%rsp)
	movq	112(%rbp), %rax
	movq	8(%rax), %rcx
	je	.L64
.L21:
	movzbl	797(%rbp), %eax
	orb	797(%r12), %al
	testb	$16, %al
	je	.L4
	movdqu	(%rbx), %xmm0
	movl	$3728, %r13d
	movq	_rtld_global@GOTPCREL(%rip), %r11
	movq	792(%rdx), %r10
	movaps	%xmm0, 112(%rsp)
	movq	16(%rbx), %rax
	movq	%r14, 120(%rsp)
	leaq	2568(%r11), %r14
	movq	%rax, 128(%rsp)
	leal	-1(%r15), %eax
	leaq	-2568(%r12), %r15
	movq	%rbx, 56(%rsp)
	movl	$0, (%rsp)
	addq	$234, %rax
	movq	%r15, 24(%rsp)
	movq	%r11, %r15
	salq	$4, %rax
	movq	%rax, 8(%rsp)
	movq	%rbx, %rax
	movq	%r10, %rbx
	subq	%rcx, %rax
	sarq	$3, %rax
	imull	$-1431655765, %eax, %eax
	movl	%eax, 44(%rsp)
	leaq	-2568(%rbp), %rax
	movq	%rax, 16(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L29:
	movq	24(%rsp), %rax
	leaq	0(%r13,%r15), %rdx
	leaq	0(%r13,%r15), %rcx
	addq	%r13, %rax
	cmpq	%r14, %r12
	cmovne	%rax, %rdx
	movq	16(%rsp), %rax
	addq	%r13, %rax
	cmpq	%r14, %rbp
	cmovne	%rax, %rcx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L26
	testb	$2, 8(%rdx)
	je	.L65
.L27:
	movl	(%rsp), %esi
	movq	32(%rsp), %r10
	leaq	112(%rsp), %rdi
	movq	48(%rsp), %r8
	orl	$8, %esi
	movl	%esi, 80(%rsp)
	movq	56(%rsp), %rsi
	movl	(%rsi), %esi
	leaq	(%r10,%rsi), %r9
	movl	44(%rsp), %esi
	call	*%rax
	cmpq	120(%rsp), %rax
	je	.L26
	movq	%rax, 120(%rsp)
	movl	$16, (%rsp)
.L26:
	addq	$16, %r13
	cmpq	%r13, 8(%rsp)
	movq	64(%rbx), %rbx
	jne	.L29
	movq	120(%rsp), %r14
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L61:
	call	*%r14
	movq	%rax, %r14
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L63:
	movq	_rtld_global@GOTPCREL(%rip), %r11
	movq	(%r11), %r12
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%r14d, %r14d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L65:
	testb	$1, 8(%rcx)
	je	.L26
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rdx, %rdi
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	movq	_rtld_global@GOTPCREL(%rip), %r11
	je	.L66
	cmpq	(%r11), %rax
	je	.L30
.L14:
	movq	%r12, %rcx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rax, %rcx
.L15:
	movq	736(%rcx), %rax
	testq	%rax, %rax
	jne	.L31
	leaq	72(%rsp), %rdx
	addq	$928, %rcx
	pushq	%r12
	pushq	$0
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	72(%rsp), %rax
	orl	$5, %r8d
	movq	%rbx, 120(%rsp)
	movq	%r12, 112(%rsp)
	movq	%rbp, 128(%rsp)
	movl	%r8d, 136(%rsp)
	movq	%rax, 152(%rsp)
#APP
# 119 "dl-sym.c" 1
	movl $1,%fs:28
# 0 "" 2
#NO_APP
	leaq	80(%rsp), %rbx
	leaq	112(%rsp), %rdx
	leaq	call_dl_lookup(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI__dl_catch_exception
	movl	%eax, %r8d
	xorl	%eax, %eax
#APP
# 122 "dl-sym.c" 1
	xchgl %eax, %fs:28
# 0 "" 2
#NO_APP
	cmpl	$2, %eax
	jne	.L9
	movq	%fs:16, %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	28(%rax), %rdi
	movl	$202, %eax
#APP
# 122 "dl-sym.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L9:
	cmpq	$0, 88(%rsp)
	movq	112(%rsp), %rbp
	je	.L8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%r8d, %edi
	call	__GI__dl_signal_exception
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r13, %rdi
	movq	%rdx, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	jne	.L21
	movq	_rtld_global@GOTPCREL(%rip), %r11
	movq	(%r11), %r12
	jmp	.L21
.L66:
	movq	(%r11), %r12
	testq	%r12, %r12
	je	.L13
.L30:
	cmpq	%r13, 856(%r12)
	ja	.L13
	cmpq	%r13, 864(%r12)
	ja	.L14
.L13:
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__GI__dl_signal_error
	.size	do_sym, .-do_sym
	.p2align 4,,15
	.globl	_dl_vsym
	.type	_dl_vsym, @function
_dl_vsym:
	subq	$40, %rsp
	movzbl	(%rdx), %r8d
	xorl	%eax, %eax
	movq	%rcx, %r9
	movq	%rdx, (%rsp)
	movl	$1, 12(%rsp)
	testq	%r8, %r8
	je	.L68
	movzbl	1(%rdx), %ecx
	testb	%cl, %cl
	je	.L82
	movzbl	%cl, %eax
	movzbl	2(%rdx), %ecx
	salq	$4, %r8
	addq	%r8, %rax
	testb	%cl, %cl
	je	.L68
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	3(%rdx), %ecx
	testb	%cl, %cl
	je	.L68
	salq	$4, %rax
	addq	%rcx, %rax
	movzbl	4(%rdx), %ecx
	testb	%cl, %cl
	je	.L68
	salq	$4, %rax
	addq	%rcx, %rax
	leaq	5(%rdx), %rcx
	movzbl	5(%rdx), %edx
	testb	%dl, %dl
	je	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	salq	$4, %rax
	addq	$1, %rcx
	addq	%rdx, %rax
	movq	%rax, %rdx
	shrq	$24, %rdx
	andl	$240, %edx
	xorq	%rdx, %rax
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L74
.L73:
	andl	$268435455, %eax
.L68:
	movq	%rsp, %rcx
	xorl	%r8d, %r8d
	movq	%r9, %rdx
	movl	%eax, 8(%rsp)
	movq	$0, 16(%rsp)
	call	do_sym
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movzbl	%r8b, %eax
	jmp	.L68
	.size	_dl_vsym, .-_dl_vsym
	.p2align 4,,15
	.globl	_dl_sym
	.type	_dl_sym, @function
_dl_sym:
	movl	$2, %r8d
	xorl	%ecx, %ecx
	jmp	do_sym
	.size	_dl_sym, .-_dl_sym
