	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI__dl_addr
	.hidden	__GI__dl_addr
	.type	__GI__dl_addr, @function
__GI__dl_addr:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	_rtld_global@GOTPCREL(%rip), %r15
	movq	%rcx, 8(%rsp)
	leaq	2440(%r15), %rdi
	call	*3984(%r15)
	movq	%rbp, %rdi
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	je	.L2
	movq	%rax, %r10
	movq	8(%rax), %rax
	movq	856(%r10), %rdx
	movq	%rax, 0(%r13)
	movq	%rdx, 8(%r13)
	cmpb	$0, (%rax)
	je	.L77
.L3:
	movq	112(%r10), %rax
	cmpq	$0, 672(%r10)
	movq	8(%rax), %r9
	movq	104(%r10), %rax
	movq	8(%rax), %rax
	movq	%rax, 16(%rsp)
	movq	144(%r10), %rax
	movl	8(%rax), %eax
	movl	%eax, 28(%rsp)
	je	.L4
	movl	756(%r10), %eax
	testl	%eax, %eax
	je	.L34
	movq	776(%r10), %rdx
	leal	-1(%rax), %ecx
	xorl	%r12d, %r12d
	leaq	4(%rdx), %rax
	leaq	(%rax,%rcx,4), %rsi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	%rax, %rsi
	movq	%rax, %rdx
	je	.L5
	addq	$4, %rax
.L16:
	movl	(%rdx), %edx
	testl	%edx, %edx
	je	.L6
	movq	784(%r10), %rdi
	leaq	(%rdi,%rdx,4), %rcx
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rcx, %rdx
	subq	%rdi, %rdx
	sarq	$2, %rdx
	movl	%edx, %edx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r9,%rdx,8), %rdx
	movzwl	6(%rdx), %r8d
	testw	%r8w, %r8w
	jne	.L7
	cmpq	$0, 8(%rdx)
	je	.L9
.L8:
	movzbl	4(%rdx), %r11d
	andl	$15, %r11d
	cmpb	$6, %r11b
	je	.L9
	movq	8(%rdx), %rbx
	movq	(%r10), %r11
	addq	%rbx, %r11
	cmpq	%r11, %rbp
	jb	.L9
	testw	%r8w, %r8w
	je	.L10
	movq	16(%rdx), %r8
	testq	%r8, %r8
	jne	.L12
	cmpq	%r11, %rbp
	je	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$4, %rcx
	testb	$1, -4(%rcx)
	je	.L15
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L5:
	testq	%r14, %r14
	je	.L27
	movq	%r10, (%r14)
.L27:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L28
	movq	%r12, (%rax)
.L28:
	testq	%r12, %r12
	je	.L29
	movl	(%r12), %eax
	addq	16(%rsp), %rax
	cmpw	$-15, 6(%r12)
	movq	%rax, 16(%r13)
	je	.L39
	movq	(%r10), %rax
.L30:
	addq	8(%r12), %rax
	movl	$1, %ebx
	movq	%rax, 24(%r13)
.L2:
	leaq	2440(%r15), %rdi
	call	*3992(%r15)
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%r11, %rbp
	je	.L13
	movq	16(%rdx), %r8
.L12:
	addq	%r8, %r11
	cmpq	%r11, %rbp
	jnb	.L9
.L13:
	testq	%r12, %r12
	je	.L14
	cmpq	8(%r12), %rbx
	jbe	.L9
.L14:
	movl	28(%rsp), %ebx
	cmpl	(%rdx), %ebx
	cmova	%rdx, %r12
	addq	$4, %rcx
	testb	$1, -4(%rcx)
	je	.L15
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	cmpw	$-15, %r8w
	jne	.L8
	addq	$4, %rcx
	testb	$1, -4(%rcx)
	je	.L15
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L29:
	movq	$0, 16(%r13)
	movq	$0, 24(%r13)
	movl	$1, %ebx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movq	96(%r10), %rax
	movq	16(%rsp), %rcx
	testq	%rax, %rax
	je	.L17
	movq	8(%rax), %rax
	movl	4(%rax), %eax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r9,%rax,8), %rcx
.L17:
	xorl	%r12d, %r12d
	cmpq	%rcx, %r9
	jb	.L26
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L79:
	movzbl	5(%r9), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L18
	andl	$15, %edx
	cmpb	$6, %dl
	je	.L18
	movzwl	6(%r9), %eax
	testw	%ax, %ax
	je	.L78
	cmpw	$-15, %ax
	je	.L18
	movq	8(%r9), %rax
	movq	(%r10), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rbp
	jb	.L18
	movq	16(%r9), %rsi
	testq	%rsi, %rsi
	jne	.L23
	cmpq	%rdx, %rbp
	jne	.L18
.L24:
	testq	%r12, %r12
	je	.L25
	cmpq	%rax, 8(%r12)
	jnb	.L18
.L25:
	movl	28(%rsp), %eax
	cmpl	(%r9), %eax
	cmova	%r9, %r12
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$24, %r9
	cmpq	%rcx, %r9
	jnb	.L5
.L26:
	movzbl	4(%r9), %edx
	movl	%edx, %eax
	shrb	$4, %al
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L18
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L78:
	movq	8(%r9), %rax
	testq	%rax, %rax
	je	.L18
	movq	(%r10), %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rbp
	jb	.L18
	je	.L24
	movq	16(%r9), %rsi
.L23:
	addq	%rsi, %rdx
	cmpq	%rdx, %rbp
	jb	.L24
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L77:
	testb	$3, 796(%r10)
	jne	.L3
	movq	_dl_argv@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 0(%r13)
	jmp	.L3
.L39:
	xorl	%eax, %eax
	jmp	.L30
	.size	__GI__dl_addr, .-__GI__dl_addr
	.globl	_dl_addr
	.set	_dl_addr,__GI__dl_addr
