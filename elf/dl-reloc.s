	.text
	.p2align 4,,15
	.globl	_dl_try_allocate_static_tls
	.hidden	_dl_try_allocate_static_tls
	.type	_dl_try_allocate_static_tls, @function
_dl_try_allocate_static_tls:
	cmpq	$-1, 1112(%rdi)
	je	.L9
	movq	1096(%rdi), %r9
	cmpq	_dl_tls_static_align(%rip), %r9
	ja	.L9
	movq	_dl_tls_static_used(%rip), %r8
	movq	_dl_tls_static_size(%rip), %rcx
	subq	%r8, %rcx
	cmpq	$2495, %rcx
	jbe	.L9
	movq	1104(%rdi), %r10
	movq	1088(%rdi), %rax
	subq	$2496, %rcx
	addq	%r10, %rax
	cmpq	%rax, %rcx
	jb	.L9
	movq	%rcx, %rdx
	subq	%rax, %rdx
	movq	%rdx, %rax
	xorl	%edx, %edx
	divq	%r9
	imulq	%r9, %rax
	subq	%rax, %rcx
	subq	%r10, %rcx
	testb	%sil, %sil
	je	.L3
	movq	_dl_tls_static_optional(%rip), %rax
	cmpq	%rcx, %rax
	jb	.L9
	subq	%rcx, %rax
	movq	%rax, _dl_tls_static_optional(%rip)
.L3:
	movq	40(%rdi), %rax
	addq	%r8, %rcx
	movq	%rcx, 1112(%rdi)
	movq	%rcx, _dl_tls_static_used(%rip)
	testb	$4, 796(%rax)
	je	.L4
	subq	$8, %rsp
	call	*_dl_init_static_tls(%rip)
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	orb	$4, 797(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
.L2:
	movl	$-1, %eax
	ret
	.size	_dl_try_allocate_static_tls, .-_dl_try_allocate_static_tls
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot allocate memory in static TLS block"
	.text
	.p2align 4,,15
	.globl	_dl_allocate_static_tls
	.hidden	_dl_allocate_static_tls
	.type	_dl_allocate_static_tls, @function
_dl_allocate_static_tls:
	pushq	%rbx
	cmpq	$-1, 1112(%rdi)
	movq	%rdi, %rbx
	je	.L17
	xorl	%esi, %esi
	call	_dl_try_allocate_static_tls
	testl	%eax, %eax
	jne	.L17
	popq	%rbx
	ret
.L17:
	movq	8(%rbx), %rsi
	leaq	.LC0(%rip), %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	_dl_signal_error
	.size	_dl_allocate_static_tls, .-_dl_allocate_static_tls
	.p2align 4,,15
	.globl	_dl_nothread_init_static_tls
	.hidden	_dl_nothread_init_static_tls
	.type	_dl_nothread_init_static_tls, @function
_dl_nothread_init_static_tls:
	pushq	%rbx
	movq	%fs:16, %rax
	subq	1112(%rdi), %rax
	movq	1080(%rdi), %rdx
	movq	1088(%rdi), %rbx
	movq	1072(%rdi), %rsi
	subq	%rdx, %rbx
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	popq	%rbx
	jmp	memset@PLT
	.size	_dl_nothread_init_static_tls, .-_dl_nothread_init_static_tls
	.p2align 4,,15
	.globl	_dl_protect_relro
	.hidden	_dl_protect_relro
	.type	_dl_protect_relro, @function
_dl_protect_relro:
	movq	1136(%rdi), %rax
	addq	(%rdi), %rax
	movq	_dl_pagesize(%rip), %rdx
	movq	%rax, %rcx
	addq	1144(%rdi), %rax
	negq	%rdx
	andq	%rdx, %rcx
	movq	%rax, %rsi
	andq	%rdx, %rsi
	cmpq	%rsi, %rcx
	je	.L24
	pushq	%rbx
	subq	%rcx, %rsi
	movq	%rdi, %rbx
	movl	$1, %edx
	movq	%rcx, %rdi
	call	__mprotect
	testl	%eax, %eax
	js	.L27
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	rep ret
.L27:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	8(%rbx), %rsi
	leaq	errstring.10440(%rip), %rcx
	xorl	%edx, %edx
	movl	%fs:(%rax), %edi
	call	_dl_signal_error
	.size	_dl_protect_relro, .-_dl_protect_relro
	.p2align 4,,15
	.globl	_dl_reloc_bad_type
	.hidden	_dl_reloc_bad_type
	.type	_dl_reloc_bad_type, @function
_dl_reloc_bad_type:
	movslq	%edx, %rdx
	pushq	%r12
	pushq	%rbp
	leaq	(%rdx,%rdx,8), %rax
	pushq	%rbx
	movl	%esi, %ebx
	movq	%rdi, %r12
	leaq	(%rdx,%rax,2), %rdx
	leaq	msg.10446(%rip), %rax
	subq	$48, %rsp
	movq	%rsp, %rbp
	leaq	(%rax,%rdx,2), %rsi
	movq	%rbp, %rdi
	call	__stpcpy@PLT
	cmpl	$255, %ebx
	leaq	_itoa_lower_digits(%rip), %rsi
	ja	.L32
.L29:
	movl	%ebx, %edx
	andl	$15, %ebx
	movb	$0, 2(%rax)
	shrl	$4, %edx
	movq	%rbp, %rcx
	xorl	%edi, %edi
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, (%rax)
	movzbl	(%rsi,%rbx), %edx
	movq	8(%r12), %rsi
	movb	%dl, 1(%rax)
	xorl	%edx, %edx
	call	_dl_signal_error
.L32:
	movl	%ebx, %edx
	addq	$6, %rax
	shrl	$28, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -6(%rax)
	movl	%ebx, %edx
	shrl	$24, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -5(%rax)
	movl	%ebx, %edx
	shrl	$20, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -4(%rax)
	movl	%ebx, %edx
	shrl	$16, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -3(%rax)
	movl	%ebx, %edx
	shrl	$12, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -2(%rax)
	movl	%ebx, %edx
	shrl	$8, %edx
	andl	$15, %edx
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, -1(%rax)
	jmp	.L29
	.size	_dl_reloc_bad_type, .-_dl_reloc_bad_type
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	" (lazy)"
.LC2:
	.string	""
.LC3:
	.string	"<main program>"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"cannot make segment writable for relocation"
	.align 8
.LC5:
	.string	"cannot restore segment prot after reloc"
	.section	.rodata.str1.1
.LC6:
	.string	"<program name unknown>"
	.section	.rodata.str1.8
	.align 8
.LC7:
	.string	"%s: Symbol `%s' causes overflow in R_X86_64_32 relocation\n"
	.align 8
.LC8:
	.string	"%s: Symbol `%s' causes overflow in R_X86_64_PC32 relocation\n"
	.align 8
.LC9:
	.string	"%s: Symbol `%s' has different size in shared object, consider re-linking\n"
	.section	.rodata.str1.1
.LC10:
	.string	"\nrelocation processing: %s%s\n"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"../sysdeps/x86_64/dl-machine.h"
	.align 8
.LC12:
	.string	"ELFW(R_TYPE) (reloc->r_info) == R_X86_64_RELATIVE"
	.align 8
.LC13:
	.string	"%s: IFUNC symbol '%s' referenced in '%s' is defined in the executable and creates an unsatisfiable circular dependency.\n"
	.align 8
.LC14:
	.string	"%s: Relink `%s' with `%s' for IFUNC symbol `%s'\n"
	.align 8
.LC15:
	.string	"%s: out of memory to store relocation results for %s\n"
	.text
	.p2align 4,,15
	.globl	_dl_relocate_object
	.hidden	_dl_relocate_object
	.type	_dl_relocate_object, @function
_dl_relocate_object:
	testb	$4, 796(%rdi)
	jne	.L405
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	movl	%edx, %r12d
	subq	$216, %rsp
	movl	_dl_debug_mask(%rip), %eax
	movl	%ecx, -216(%rbp)
	movq	%rsi, -200(%rbp)
	andl	$32, %eax
	testl	%ecx, %ecx
	jne	.L36
	cmpq	$0, 256(%rdi)
	jne	.L410
.L36:
	movl	%r12d, %r13d
	andl	$1, %r13d
	testl	%eax, %eax
	jne	.L411
.L38:
	cmpq	$0, 240(%r15)
	movq	$0, -240(%rbp)
	jne	.L412
.L40:
	movq	104(%r15), %rax
	cmpq	$0, 248(%r15)
	movq	8(%rax), %rax
	movq	%rax, -192(%rbp)
	je	.L49
	testl	%r13d, %r13d
	jne	.L413
.L51:
	movq	120(%r15), %rax
	pxor	%xmm0, %xmm0
	testq	%rax, %rax
	movaps	%xmm0, -112(%rbp)
	movaps	%xmm0, -96(%rbp)
	movaps	%xmm0, -80(%rbp)
	movaps	%xmm0, -64(%rbp)
	je	.L221
	movq	8(%rax), %rcx
	movq	128(%r15), %rax
	movq	8(%rax), %r8
	movq	392(%r15), %rax
	movq	%rcx, -112(%rbp)
	testq	%rax, %rax
	movq	%r8, -104(%rbp)
	leaq	(%rcx,%r8), %rsi
	je	.L62
	movq	8(%rax), %rax
	movq	%rax, -96(%rbp)
.L62:
	cmpq	$0, 224(%r15)
	je	.L63
	movq	248(%r15), %rax
	movq	80(%r15), %rdx
	movq	8(%rax), %rax
	movq	8(%rdx), %rdx
	leaq	(%rax,%rdx), %rdi
	cmpq	%rsi, %rdi
	jne	.L64
	subq	%rdx, %r8
	leaq	(%r8,%rcx), %rsi
	movq	%r8, -104(%rbp)
.L64:
	testl	%r13d, %r13d
	je	.L414
.L65:
	movq	%rax, -80(%rbp)
	movq	%rdx, -72(%rbp)
	movl	%r13d, -56(%rbp)
.L63:
	andl	$33554432, %r12d
	movq	$0, -208(%rbp)
	xorl	%edx, %edx
	movl	%r12d, -212(%rbp)
.L202:
	leaq	(%rcx,%r8), %rdi
	testl	%edx, %edx
	movq	%rcx, %rax
	movq	%rdi, -152(%rbp)
	movq	(%r15), %rdi
	movq	%rdi, -160(%rbp)
	jne	.L415
	movq	112(%r15), %rdx
	movq	-208(%rbp), %rdi
	cmpq	_dl_rtld_map@GOTPCREL(%rip), %r15
	movq	8(%rdx), %rsi
	movq	-96(%rbp,%rdi), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movq	%rsi, -168(%rbp)
	leaq	(%rcx,%rdx,8), %rbx
	je	.L82
	cmpq	$0, -160(%rbp)
	jne	.L81
	cmpq	$0, 576(%r15)
	je	.L81
.L82:
	movq	464(%r15), %rax
	testq	%rax, %rax
	je	.L416
	cmpq	%rbx, -152(%rbp)
	movq	8(%rax), %rax
	movq	%rax, -176(%rbp)
	jbe	.L67
	movq	$0, -232(%rbp)
	movq	$0, -224(%rbp)
	.p2align 4,,10
	.p2align 3
.L130:
	movq	8(%rbx), %r14
	movl	%r14d, %r12d
	cmpq	$37, %r12
	je	.L417
	movq	-176(%rbp), %rdi
	movq	%r14, %rax
	movq	-160(%rbp), %r10
	shrq	$32, %rax
	addq	(%rbx), %r10
	cmpq	$8, %r12
	movq	744(%r15), %rsi
	movzwl	(%rdi,%rax,2), %edx
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	%r13, -128(%rbp)
	je	.L408
	cmpq	$38, %r12
	je	.L408
	testq	%r12, %r12
	je	.L89
	movzbl	4(%r13), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L226
	movzbl	5(%r13), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L226
	cmpq	1040(%r15), %r13
	je	.L418
	cmpq	$7, %r12
	je	.L230
	cmpq	$16, %r12
	je	.L230
.L210:
	leaq	-17(%r12), %rax
	cmpq	$1, %rax
	setbe	%r9b
	cmpq	$36, %r12
	sete	%al
	orl	%eax, %r9d
	cmpq	$5, %r12
	movzbl	%r9b, %eax
	je	.L101
.L425:
	xorl	%r9d, %r9d
	cmpq	$6, %r12
	sete	%r9b
	sall	$2, %r9d
.L102:
	andl	$32767, %edx
	orl	%eax, %r9d
	movq	%r13, 1040(%r15)
	leaq	(%rdx,%rdx,2), %rdx
	movl	%r9d, 1048(%r15)
	leaq	(%rsi,%rdx,8), %r8
	testq	%r8, %r8
	je	.L103
	movl	8(%r8), %r11d
	movl	$0, %eax
	testl	%r11d, %r11d
	cmove	%rax, %r8
.L103:
	movl	0(%r13), %edi
	movq	-200(%rbp), %rcx
	leaq	-128(%rbp), %rdx
	addq	-192(%rbp), %rdi
	pushq	$0
	movq	%r15, %rsi
	pushq	$9
	movq	%r10, -184(%rbp)
	call	_dl_lookup_symbol_x
	movq	%rax, %r11
	movq	-128(%rbp), %rax
	movq	-184(%rbp), %r10
	movq	%r11, 1056(%r15)
	movq	%rax, 1064(%r15)
	popq	%rdi
	popq	%r8
.L99:
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L93
.L104:
	cmpq	$37, %r12
	ja	.L110
	leaq	.L112(%rip), %rcx
	movslq	(%rcx,%r12,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L112:
	.long	.L110-.L112
	.long	.L111-.L112
	.long	.L113-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L114-.L112
	.long	.L111-.L112
	.long	.L111-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L116-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L117-.L112
	.long	.L118-.L112
	.long	.L119-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L120-.L112
	.long	.L121-.L112
	.long	.L110-.L112
	.long	.L110-.L112
	.long	.L122-.L112
	.long	.L123-.L112
	.text
.L434:
	testb	$1, 74+_dl_x86_cpu_features(%rip)
	je	.L54
	leaq	_dl_runtime_profile_avx512(%rip), %rsi
	movq	%rsi, 16(%rax)
.L55:
	movq	_dl_profile(%rip), %rdi
	testq	%rdi, %rdi
	je	.L49
	movq	%r15, %rsi
	call	_dl_name_match_p
	testl	%eax, %eax
	je	.L49
	movq	%r15, _dl_profile_map(%rip)
	.p2align 4,,10
	.p2align 3
.L49:
	movq	656(%r15), %rax
	testq	%rax, %rax
	je	.L51
	testl	%r13d, %r13d
	je	.L51
	movq	8(%rax), %rdx
	movq	(%r15), %rax
	leaq	_dl_tlsdesc_resolve_rela(%rip), %rdi
	movq	%rdi, (%rdx,%rax)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L121:
	movq	-128(%rbp), %rax
	movq	16(%rax), %r9
.L111:
	addq	16(%rbx), %r9
	movq	%r9, (%r10)
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$24, %rbx
	cmpq	%rbx, -152(%rbp)
	ja	.L130
	movq	-224(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L67
	movq	-232(%rbp), %r12
	cmpq	%r12, %rbx
	ja	.L67
	leaq	-120(%rbp), %r13
	xorl	%r14d, %r14d
	movq	%r13, -184(%rbp)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	ja	.L67
.L145:
	movq	8(%rbx), %rax
	cmpl	$37, %eax
	jne	.L131
	movq	-176(%rbp), %rdi
	shrq	$32, %rax
	movq	-160(%rbp), %r13
	addq	(%rbx), %r13
	movq	744(%r15), %rcx
	movzwl	(%rdi,%rax,2), %edx
	movq	-168(%rbp), %rdi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r10
	movq	%r10, -120(%rbp)
	movzbl	4(%r10), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L242
	movzbl	5(%r10), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L242
	cmpq	1040(%r15), %r10
	je	.L419
.L133:
	andl	$32767, %edx
	movl	$0, 1048(%r15)
	movq	%r10, 1040(%r15)
	leaq	(%rdx,%rdx,2), %rax
	leaq	(%rcx,%rax,8), %r8
	testq	%r8, %r8
	je	.L135
	movl	8(%r8), %r11d
	testl	%r11d, %r11d
	cmove	%r14, %r8
.L135:
	movl	(%r10), %edi
	movq	-184(%rbp), %rdx
	xorl	%r9d, %r9d
	addq	-192(%rbp), %rdi
	movq	-200(%rbp), %rcx
	movq	%r15, %rsi
	pushq	$0
	pushq	$9
	movq	%r10, -152(%rbp)
	call	_dl_lookup_symbol_x
	movq	-120(%rbp), %rdx
	movq	%rax, 1056(%r15)
	movq	-152(%rbp), %r10
	movq	%rdx, 1064(%r15)
	popq	%r8
	popq	%r9
.L134:
	testq	%rdx, %rdx
	jne	.L132
.L136:
	movl	-212(%rbp), %esi
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%esi, %esi
	jne	.L144
.L143:
	call	*%rax
.L144:
	addq	$24, %rbx
	movq	%rax, 0(%r13)
	cmpq	%r12, %rbx
	jbe	.L145
	.p2align 4,,10
	.p2align 3
.L67:
	addq	$32, -208(%rbp)
	movq	-208(%rbp), %rsi
	cmpq	$64, %rsi
	je	.L201
	movl	-88(%rbp,%rsi), %edx
	movq	-104(%rbp,%rsi), %r8
	movq	-112(%rbp,%rsi), %rcx
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-128(%rbp), %rax
	movq	16(%rax), %r9
.L116:
	addq	16(%rbx), %r9
	movl	$4294967295, %eax
	leaq	.LC7(%rip), %rdi
	cmpq	%rax, %r9
	movl	%r9d, (%r10)
	jbe	.L89
.L127:
	movq	104(%r15), %rax
	movl	0(%r13), %edx
	addq	8(%rax), %rdx
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L123:
	movl	-212(%rbp), %edx
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%edx, %edx
	jne	.L129
	movq	%r10, -184(%rbp)
	call	*%rax
	movq	-184(%rbp), %r10
.L129:
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L114:
	movq	-128(%rbp), %r12
	testq	%r12, %r12
	je	.L89
	movq	16(%r12), %rdx
	cmpq	%rdx, 16(%r13)
	movq	%r9, %rsi
	cmovbe	16(%r13), %rdx
	movq	%r10, %rdi
	call	memcpy@PLT
	movq	16(%r13), %rax
	cmpq	%rax, 16(%r12)
	ja	.L240
	jnb	.L89
	movl	_dl_verbose(%rip), %ecx
	testl	%ecx, %ecx
	je	.L89
.L240:
	leaq	.LC9(%rip), %rdi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%rbx), %rax
	subq	%r10, %rax
	addq	%rax, %r9
	movslq	%r9d, %rax
	movl	%r9d, (%r10)
	cmpq	%rax, %r9
	je	.L89
	leaq	.LC8(%rip), %rdi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L122:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L420
	movq	1112(%r11), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L421
.L125:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%rbx), %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L89
	movq	1112(%r11), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L422
.L126:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%rbx), %rax
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L118:
	movq	-128(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L89
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L117:
	testq	%r11, %r11
	je	.L89
	movq	1120(%r11), %rax
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%r13, %rax
	movq	%r15, %r11
.L93:
	movzwl	6(%rax), %edx
	cmpw	$-15, %dx
	je	.L234
	movq	(%r11), %r9
.L105:
	addq	8(%rax), %r9
	movzbl	4(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	jne	.L104
	testw	%dx, %dx
	je	.L104
	movl	-212(%rbp), %esi
	testl	%esi, %esi
	jne	.L104
	cmpq	%r11, %r15
	jne	.L423
.L106:
	movq	%r11, -248(%rbp)
	movq	%r10, -184(%rbp)
	call	*%r9
	movq	-248(%rbp), %r11
	movq	%rax, %r9
	movq	-184(%rbp), %r10
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L417:
	cmpq	$0, -224(%rbp)
	je	.L424
	movq	%rbx, -232(%rbp)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L408:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L230:
	cmpq	$5, %r12
	movl	$1, %eax
	jne	.L425
.L101:
	orl	$2, %eax
	xorl	%r9d, %r9d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r10, %rdx
	movq	%r15, %rax
.L132:
	movzwl	6(%rdx), %esi
	movzbl	4(%rdx), %ecx
	movq	8(%rdx), %r9
	andl	$15, %ecx
	cmpw	$-15, %si
	je	.L137
	cmpb	$10, %cl
	movq	(%rax), %rdx
	jne	.L136
	testw	%si, %si
	je	.L136
	addq	%rdx, %r9
.L211:
	movl	-212(%rbp), %edi
	testl	%edi, %edi
	jne	.L138
	cmpq	%rax, %r15
	jne	.L426
.L139:
	call	*%r9
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L424:
	movq	%rbx, -232(%rbp)
	movq	%rbx, -224(%rbp)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L201:
	movl	-216(%rbp), %eax
	testl	%eax, %eax
	jne	.L427
.L203:
	orb	$4, 796(%r15)
	cmpq	$0, -240(%rbp)
	jne	.L428
.L206:
	cmpq	$0, 1144(%r15)
	je	.L33
	movq	%r15, %rdi
	call	_dl_protect_relro
.L33:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	rep ret
	.p2align 4,,10
	.p2align 3
.L234:
	xorl	%r9d, %r9d
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L415:
	movq	-152(%rbp), %r8
	cmpq	%r8, %rcx
	jnb	.L67
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%rax), %rcx
	addq	%rdi, %rcx
	cmpq	$7, %rdx
	jne	.L71
	movq	1016(%r15), %rdx
	testq	%rdx, %rdx
	jne	.L72
	addq	%rdi, (%rcx)
.L69:
	addq	$24, %rax
	cmpq	%rax, %r8
	jbe	.L429
.L74:
	movq	8(%rax), %rsi
	movl	%esi, %edx
	cmpq	$37, %rdx
	jne	.L68
	testq	%rbx, %rbx
	movq	%rax, %r12
	jne	.L69
	movq	%rax, %rbx
	addq	$24, %rax
	cmpq	%rax, %r8
	ja	.L74
	.p2align 4,,10
	.p2align 3
.L429:
	testq	%rbx, %rbx
	je	.L67
	cmpq	%r12, %rbx
	ja	.L67
	movq	-160(%rbp), %r14
	movq	%r15, %r13
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	ja	.L430
.L77:
	cmpl	$37, 8(%rbx)
	jne	.L75
	movq	(%rbx), %r15
	movl	-212(%rbp), %edx
	movq	16(%rbx), %rax
	addq	0(%r13), %rax
	addq	%r14, %r15
	testl	%edx, %edx
	jne	.L76
	call	*%rax
.L76:
	addq	$24, %rbx
	movq	%rax, (%r15)
	cmpq	%r12, %rbx
	jbe	.L77
.L430:
	movq	%r13, %r15
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L71:
	cmpq	$36, %rdx
	jne	.L73
	movq	664(%r15), %rsi
	movq	(%r15), %rdx
	movq	%rax, 8(%rcx)
	addq	8(%rsi), %rdx
	movq	%rdx, (%rcx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rcx, %rsi
	subq	1024(%r15), %rsi
	leaq	(%rdx,%rsi,2), %rdx
	movq	%rdx, (%rcx)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L418:
	xorl	%eax, %eax
	cmpq	$36, %r12
	ja	.L95
	movabsq	$68719935616, %rax
	movl	%r14d, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
.L95:
	cmpq	$5, %r12
	movl	1048(%r15), %ecx
	je	.L96
	cmpq	$6, %r12
	movl	$4, %edi
	je	.L97
	cmpl	%ecx, %eax
	je	.L209
	cmpq	$7, %r12
	je	.L269
	cmpq	$16, %r12
	jne	.L210
.L269:
	xorl	%r9d, %r9d
	movl	$1, %eax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L81:
	cmpq	%rbx, %rcx
	jnb	.L82
	movq	-160(%rbp), %rcx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L85:
	movq	16(%rax), %rdx
	addq	$24, %rax
	addq	%rcx, %rdx
	cmpq	%rax, %rbx
	movq	%rdx, (%rsi)
	jbe	.L82
.L86:
	movl	8(%rax), %edx
	movq	(%rax), %rsi
	addq	%rcx, %rsi
	cmpq	$38, %rdx
	je	.L85
	cmpq	$8, %rdx
	je	.L85
	leaq	__PRETTY_FUNCTION__.10313(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$546, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L416:
	xorl	%r10d, %r10d
	cmpq	%rbx, -152(%rbp)
	movq	$0, -184(%rbp)
	jbe	.L67
	movq	%r10, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%rbx), %r14
	movl	%r14d, %r12d
	cmpq	$37, %r12
	je	.L431
	movq	%r14, %rax
	movq	-168(%rbp), %rdi
	movq	-160(%rbp), %r10
	shrq	$32, %rax
	addq	(%rbx), %r10
	cmpq	$8, %r12
	leaq	(%rax,%rax,2), %rax
	leaq	(%rdi,%rax,8), %r13
	movq	%r13, -136(%rbp)
	je	.L409
	cmpq	$38, %r12
	je	.L409
	testq	%r12, %r12
	je	.L147
	movzbl	4(%r13), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L248
	movzbl	5(%r13), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L248
	cmpq	1040(%r15), %r13
	je	.L432
	cmpq	$7, %r12
	je	.L252
	cmpq	$16, %r12
	je	.L252
.L213:
	leaq	-17(%r12), %rax
	cmpq	$1, %rax
	setbe	%r9b
	cmpq	$36, %r12
	sete	%al
	orl	%eax, %r9d
	cmpq	$5, %r12
	movzbl	%r9b, %eax
	je	.L159
.L443:
	xorl	%r9d, %r9d
	cmpq	$6, %r12
	sete	%r9b
	sall	$2, %r9d
.L160:
	movl	0(%r13), %edi
	orl	%eax, %r9d
	movq	-200(%rbp), %rcx
	addq	-192(%rbp), %rdi
	movl	%r9d, 1048(%r15)
	leaq	-136(%rbp), %rdx
	movq	%r13, 1040(%r15)
	pushq	$0
	xorl	%r8d, %r8d
	pushq	$9
	movq	%r15, %rsi
	movq	%r10, -224(%rbp)
	call	_dl_lookup_symbol_x
	movq	%rax, %r11
	movq	-136(%rbp), %rax
	movq	-224(%rbp), %r10
	movq	%r11, 1056(%r15)
	movq	%rax, 1064(%r15)
	popq	%rdx
	popq	%rcx
.L157:
	xorl	%r9d, %r9d
	testq	%rax, %rax
	jne	.L151
.L161:
	cmpq	$37, %r12
	ja	.L167
	leaq	.L169(%rip), %rsi
	movslq	(%rsi,%r12,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L169:
	.long	.L167-.L169
	.long	.L168-.L169
	.long	.L170-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L171-.L169
	.long	.L168-.L169
	.long	.L168-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L173-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L174-.L169
	.long	.L175-.L169
	.long	.L176-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L177-.L169
	.long	.L178-.L169
	.long	.L167-.L169
	.long	.L167-.L169
	.long	.L179-.L169
	.long	.L180-.L169
	.text
	.p2align 4,,10
	.p2align 3
.L178:
	movq	-136(%rbp), %rax
	movq	16(%rax), %r9
.L168:
	addq	16(%rbx), %r9
	movq	%r9, (%r10)
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$24, %rbx
	cmpq	%rbx, -152(%rbp)
	ja	.L87
	movq	-176(%rbp), %r10
	testq	%r10, %r10
	je	.L67
	movq	-184(%rbp), %r12
	cmpq	%r12, %r10
	ja	.L67
	leaq	-144(%rbp), %r13
	leaq	.LC14(%rip), %r14
	movq	%r10, %rbx
	movq	%r13, -176(%rbp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L187:
	addq	$24, %rbx
	cmpq	%r12, %rbx
	ja	.L67
.L200:
	movq	8(%rbx), %rax
	cmpl	$37, %eax
	jne	.L187
	shrq	$32, %rax
	movq	-168(%rbp), %rdi
	movq	-160(%rbp), %r13
	leaq	(%rax,%rax,2), %rax
	addq	(%rbx), %r13
	leaq	(%rdi,%rax,8), %r10
	movq	%r10, -144(%rbp)
	movzbl	4(%r10), %eax
	shrb	$4, %al
	testb	%al, %al
	je	.L263
	movzbl	5(%r10), %eax
	andl	$3, %eax
	subl	$1, %eax
	cmpl	$1, %eax
	jbe	.L263
	cmpq	1040(%r15), %r10
	je	.L433
.L189:
	movl	(%r10), %edi
	movq	-176(%rbp), %rdx
	movq	%r15, %rsi
	addq	-192(%rbp), %rdi
	movq	-200(%rbp), %rcx
	xorl	%r9d, %r9d
	movq	%r10, 1040(%r15)
	movl	$0, 1048(%r15)
	xorl	%r8d, %r8d
	pushq	$0
	pushq	$9
	movq	%r10, -152(%rbp)
	call	_dl_lookup_symbol_x
	movq	-144(%rbp), %rdx
	movq	%rax, 1056(%r15)
	movq	-152(%rbp), %r10
	movq	%rdx, 1064(%r15)
	popq	%rsi
	popq	%rdi
.L190:
	testq	%rdx, %rdx
	jne	.L188
.L191:
	movl	-212(%rbp), %edx
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%edx, %edx
	jne	.L199
.L198:
	call	*%rax
.L199:
	movq	%rax, 0(%r13)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L414:
	cmpq	%rsi, %rax
	jne	.L65
	addq	%rdx, %r8
	movq	%r8, -104(%rbp)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L413:
	movq	88(%r15), %rax
	movq	8(%rax), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L52
	addq	(%r15), %rdx
	movq	%rdx, 1016(%r15)
	leaq	24(%rax), %rdx
	movq	%rdx, 1024(%r15)
.L52:
	movl	-216(%rbp), %ecx
	movq	%r15, 8(%rax)
	testl	%ecx, %ecx
	jne	.L434
	cmpq	$0, 288+_dl_x86_cpu_features(%rip)
	je	.L58
	testb	$2, 132+_dl_x86_cpu_features(%rip)
	leaq	_dl_runtime_resolve_xsave(%rip), %rdx
	leaq	_dl_runtime_resolve_xsavec(%rip), %rcx
	cmovne	%rcx, %rdx
	movq	%rdx, 16(%rax)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L177:
	movq	-136(%rbp), %rax
	movq	16(%rax), %r9
.L173:
	addq	16(%rbx), %r9
	movl	$4294967295, %eax
	leaq	.LC7(%rip), %rdi
	cmpq	%rax, %r9
	movl	%r9d, (%r10)
	jbe	.L147
.L184:
	movq	104(%r15), %rax
	movl	0(%r13), %edx
	addq	8(%rax), %rdx
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L435
	movq	1112(%r11), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L436
.L182:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%rbx), %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_return(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L180:
	movl	-212(%rbp), %r9d
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	testl	%r9d, %r9d
	jne	.L186
	movq	%r10, -224(%rbp)
	call	*%rax
	movq	-224(%rbp), %r10
.L186:
	movq	%rax, (%r10)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L171:
	movq	-136(%rbp), %r12
	testq	%r12, %r12
	je	.L147
	movq	16(%r12), %rax
	cmpq	%rax, 16(%r13)
	movq	%r9, %rsi
	cmovbe	16(%r13), %rax
	movq	%r10, %rdi
	movq	%rax, %rdx
	call	memcpy@PLT
	movq	16(%r13), %rax
	cmpq	%rax, 16(%r12)
	ja	.L261
	jnb	.L147
	movl	_dl_verbose(%rip), %r10d
	testl	%r10d, %r10d
	je	.L147
.L261:
	leaq	.LC9(%rip), %rdi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L170:
	movq	16(%rbx), %rax
	subq	%r10, %rax
	addq	%rax, %r9
	movslq	%r9d, %rax
	movl	%r9d, (%r10)
	cmpq	%rax, %r9
	je	.L147
	leaq	.LC8(%rip), %rdi
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L176:
	movq	-136(%rbp), %rax
	testq	%rax, %rax
	je	.L147
	movq	1112(%r11), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L437
.L183:
	movq	8(%rax), %rax
	subq	%rdx, %rax
	addq	16(%rbx), %rax
	movq	%rax, (%r10)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L175:
	movq	-136(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L147
	movq	16(%rbx), %rax
	addq	8(%rdx), %rax
	movq	%rax, (%r10)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L174:
	testq	%r11, %r11
	je	.L147
	movq	1120(%r11), %rax
	movq	%rax, (%r10)
	jmp	.L147
.L96:
	orl	$2, %eax
	xorl	%edi, %edi
.L97:
	orl	%edi, %eax
	cmpl	%ecx, %eax
	jne	.L210
.L209:
	movq	1064(%r15), %rax
	movq	1056(%r15), %r11
	movq	%rax, -128(%rbp)
	jmp	.L99
.L412:
	movzwl	696(%r15), %edx
	movq	680(%r15), %rcx
	movq	%rcx, %rbx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	%rax, %rcx
	jnb	.L40
	movl	%r13d, -152(%rbp)
	movl	%r12d, -160(%rbp)
	movabsq	$12884901887, %r14
	movq	%r15, %r13
	xorl	%r12d, %r12d
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	0(,%rdx,8), %rax
	addq	$56, %rbx
	subq	%rdx, %rax
	leaq	(%rcx,%rax,8), %rax
	cmpq	%rax, %rbx
	jnb	.L438
.L47:
	movq	(%rbx), %rax
	andq	%r14, %rax
	cmpq	$1, %rax
	jne	.L41
	movq	16(%rbx), %rsi
	movq	_dl_pagesize(%rip), %rax
	subq	$48, %rsp
	leaq	15(%rsp), %rcx
	movq	%rax, %rdx
	leaq	-1(%rsi,%rax), %rax
	addq	40(%rbx), %rax
	negq	%rdx
	movq	%rsi, %rdi
	andq	$-16, %rcx
	andq	%rdx, %rdi
	movl	$0, 16(%rcx)
	movq	%rcx, %r15
	movq	%rax, %rsi
	movl	4(%rbx), %eax
	andq	%rdx, %rsi
	xorl	%edx, %edx
	subq	%rdi, %rsi
	addq	0(%r13), %rdi
	testb	$4, %al
	movq	%rsi, 8(%rcx)
	movq	%rdi, (%rcx)
	je	.L42
	movl	$1, 16(%rcx)
	movl	$1, %edx
.L42:
	testb	$2, %al
	je	.L43
	orl	$2, %edx
	movl	%edx, 16(%r15)
.L43:
	testb	$1, %al
	je	.L44
	orl	$4, %edx
	movl	%edx, 16(%r15)
.L44:
	orl	$2, %edx
	call	__mprotect
	testl	%eax, %eax
	js	.L439
	movq	%r12, 24(%r15)
	movq	680(%r13), %rcx
	movq	%r15, %r12
	movzwl	696(%r13), %edx
	jmp	.L41
.L411:
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rax
	testl	%r13d, %r13d
	cmove	%rax, %rdx
.L37:
	movq	8(%r15), %rsi
	cmpb	$0, (%rsi)
	je	.L440
.L39:
	leaq	.LC10(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L431:
	cmpq	$0, -176(%rbp)
	je	.L441
	movq	%rbx, -184(%rbp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L248:
	movq	%r13, %rax
	movq	%r15, %r11
.L151:
	movzwl	6(%rax), %edx
	cmpw	$-15, %dx
	je	.L255
	movq	(%r11), %r9
.L162:
	addq	8(%rax), %r9
	movzbl	4(%rax), %eax
	andl	$15, %eax
	cmpb	$10, %al
	jne	.L161
	testw	%dx, %dx
	je	.L161
	movl	-212(%rbp), %eax
	testl	%eax, %eax
	jne	.L161
	cmpq	%r11, %r15
	jne	.L442
.L163:
	movq	%r10, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	*%r9
	movq	-232(%rbp), %r10
	movq	%rax, %r9
	movq	-224(%rbp), %r11
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L409:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	movq	%rax, (%r10)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L252:
	cmpq	$5, %r12
	movl	$1, %eax
	jne	.L443
.L159:
	orl	$2, %eax
	xorl	%r9d, %r9d
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%rbx, -184(%rbp)
	movq	%rbx, -176(%rbp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L263:
	movq	%r10, %rdx
	movq	%r15, %rax
.L188:
	movzwl	6(%rdx), %ecx
	movq	8(%rdx), %r9
	movzbl	4(%rdx), %edx
	andl	$15, %edx
	cmpw	$-15, %cx
	je	.L192
	cmpb	$10, %dl
	movq	(%rax), %rsi
	jne	.L191
	testw	%cx, %cx
	je	.L191
	addq	%rsi, %r9
.L214:
	movl	-212(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L193
	cmpq	%rax, %r15
	jne	.L444
.L194:
	call	*%r9
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L221:
	xorl	%esi, %esi
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L420:
	movq	16(%rbx), %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L255:
	xorl	%r9d, %r9d
	jmp	.L162
.L58:
	leaq	_dl_runtime_resolve_fxsave(%rip), %rsi
	movq	%rsi, 16(%rax)
	jmp	.L49
.L432:
	xorl	%eax, %eax
	cmpq	$36, %r12
	jbe	.L445
.L153:
	cmpq	$5, %r12
	movl	1048(%r15), %ecx
	je	.L154
	cmpq	$6, %r12
	movl	$4, %edx
	jne	.L446
.L155:
	orl	%edx, %eax
	cmpl	%ecx, %eax
	jne	.L213
.L212:
	movq	1064(%r15), %rax
	movq	1056(%r15), %r11
	movq	%rax, -136(%rbp)
	jmp	.L157
.L445:
	movabsq	$68719935616, %rax
	movl	%r14d, %ecx
	shrq	%cl, %rax
	andl	$1, %eax
	jmp	.L153
.L423:
	movzbl	796(%r11), %eax
	testb	$4, %al
	jne	.L106
	movq	104(%r15), %rdx
	movq	_dl_argv(%rip), %rcx
	movl	0(%r13), %r8d
	addq	8(%rdx), %r8
	testb	$3, %al
	movq	8(%r15), %rdx
	movq	(%rcx), %rsi
	je	.L447
	movq	8(%r11), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	leaq	.LC14(%rip), %rdi
	movq	%r9, -256(%rbp)
	movq	%r10, -248(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	movq	%r11, -184(%rbp)
	call	_dl_error_printf
	movq	-256(%rbp), %r9
	movq	-248(%rbp), %r10
	movq	-184(%rbp), %r11
	jmp	.L106
.L137:
	cmpb	$10, %cl
	jne	.L136
	jmp	.L211
.L427:
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.L203
	movq	224(%r15), %rdx
	xorl	%ecx, %ecx
	movq	8(%rax), %rax
	movl	$32, %edi
	cmpq	$7, 8(%rdx)
	sete	%cl
	xorl	%edx, %edx
	leaq	16(,%rcx,8), %rcx
	divq	%rcx
	movq	%rax, %rsi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 832(%r15)
	jne	.L203
	movq	_dl_argv(%rip), %rax
	movq	8(%r15), %rdx
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L205
	leaq	.LC6(%rip), %rsi
.L205:
	leaq	.LC15(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_fatal_printf@PLT
	.p2align 4,,10
	.p2align 3
.L428:
	movq	-240(%rbp), %rbx
.L207:
	movl	16(%rbx), %edx
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	call	__mprotect
	testl	%eax, %eax
	js	.L268
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L206
	jmp	.L207
.L438:
	movq	%r12, -240(%rbp)
	movq	%r13, %r15
	movl	-160(%rbp), %r12d
	movl	-152(%rbp), %r13d
	jmp	.L40
.L440:
	movq	_dl_argv(%rip), %rax
	movq	(%rax), %rsi
	leaq	.LC3(%rip), %rax
	testq	%rsi, %rsi
	cmove	%rax, %rsi
	jmp	.L39
.L410:
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L38
	leaq	.LC2(%rip), %rdx
	jmp	.L37
.L446:
	cmpl	%ecx, %eax
	je	.L212
	cmpq	$16, %r12
	je	.L270
	cmpq	$7, %r12
	jne	.L213
.L270:
	xorl	%r9d, %r9d
	movl	$1, %eax
	jmp	.L160
.L426:
	movzbl	796(%rax), %ecx
	testb	$4, %cl
	jne	.L139
	movq	104(%r15), %rdx
	movq	_dl_argv(%rip), %rsi
	movl	(%r10), %r8d
	addq	8(%rdx), %r8
	andl	$3, %ecx
	movq	8(%r15), %rdx
	movq	(%rsi), %rsi
	je	.L448
	movq	8(%rax), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	leaq	.LC14(%rip), %rdi
	movq	%r9, -152(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	movq	-152(%rbp), %r9
	jmp	.L139
.L110:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_dl_reloc_bad_type
	.p2align 4,,10
	.p2align 3
.L419:
	movl	1048(%r15), %eax
	testl	%eax, %eax
	jne	.L133
	movq	1064(%r15), %rdx
	movq	1056(%r15), %rax
	movq	%rdx, -120(%rbp)
	jmp	.L134
.L435:
	movq	16(%rbx), %rax
	movq	%rax, 8(%r10)
	leaq	_dl_tlsdesc_undefweak(%rip), %rax
	movq	%rax, (%r10)
	jmp	.L147
.L192:
	cmpb	$10, %dl
	jne	.L191
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r11, %rdi
	movq	%r10, -248(%rbp)
	movq	%r11, -184(%rbp)
	call	_dl_allocate_static_tls
	movq	-184(%rbp), %r11
	movq	-128(%rbp), %rax
	movq	-248(%rbp), %r10
	movq	1112(%r11), %rdx
	jmp	.L125
.L422:
	movq	%r11, %rdi
	movq	%r10, -248(%rbp)
	movq	%r11, -184(%rbp)
	call	_dl_allocate_static_tls
	movq	-184(%rbp), %r11
	movq	-128(%rbp), %rax
	movq	-248(%rbp), %r10
	movq	1112(%r11), %rdx
	jmp	.L126
.L442:
	movzbl	796(%r11), %eax
	testb	$4, %al
	jne	.L163
	movq	104(%r15), %rdx
	movq	_dl_argv(%rip), %rcx
	movl	0(%r13), %r8d
	addq	8(%rdx), %r8
	testb	$3, %al
	movq	8(%r15), %rdx
	movq	(%rcx), %rsi
	je	.L449
	movq	8(%r11), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	leaq	.LC14(%rip), %rdi
	movq	%r9, -248(%rbp)
	movq	%r10, -232(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	movq	%r11, -224(%rbp)
	call	_dl_error_printf
	movq	-248(%rbp), %r9
	movq	-232(%rbp), %r10
	movq	-224(%rbp), %r11
	jmp	.L163
.L154:
	orl	$2, %eax
	xorl	%edx, %edx
	jmp	.L155
.L73:
	movl	$1, %edx
	movq	%r15, %rdi
	call	_dl_reloc_bad_type
.L433:
	movl	1048(%r15), %r8d
	testl	%r8d, %r8d
	jne	.L189
	movq	1064(%r15), %rdx
	movq	1056(%r15), %rax
	movq	%rdx, -144(%rbp)
	jmp	.L190
.L436:
	movq	%r11, %rdi
	movq	%r10, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_dl_allocate_static_tls
	movq	-224(%rbp), %r11
	movq	-136(%rbp), %rax
	movq	-232(%rbp), %r10
	movq	1112(%r11), %rdx
	jmp	.L182
.L437:
	movq	%r11, %rdi
	movq	%r10, -232(%rbp)
	movq	%r11, -224(%rbp)
	call	_dl_allocate_static_tls
	movq	-224(%rbp), %r11
	movq	-136(%rbp), %rax
	movq	-232(%rbp), %r10
	movq	1112(%r11), %rdx
	jmp	.L183
.L54:
	testb	$16, 47+_dl_x86_cpu_features(%rip)
	je	.L56
	leaq	_dl_runtime_profile_avx(%rip), %rsi
	movq	%rsi, 16(%rax)
	jmp	.L55
.L444:
	movzbl	796(%rax), %ecx
	testb	$4, %cl
	jne	.L194
	movq	104(%r15), %rdx
	movq	_dl_argv(%rip), %rsi
	movl	(%r10), %r8d
	addq	8(%rdx), %r8
	andl	$3, %ecx
	movq	8(%r15), %rdx
	movq	(%rsi), %rsi
	je	.L450
	movq	8(%rax), %rcx
	leaq	.LC6(%rip), %rax
	testq	%rsi, %rsi
	movq	%r14, %rdi
	movq	%r9, -152(%rbp)
	cmove	%rax, %rsi
	xorl	%eax, %eax
	call	_dl_error_printf
	movq	-152(%rbp), %r9
	jmp	.L194
.L138:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L144
.L56:
	leaq	_dl_runtime_profile_sse(%rip), %rdi
	movq	%rdi, 16(%rax)
	jmp	.L55
.L193:
	movq	16(%rbx), %rax
	addq	(%r15), %rax
	jmp	.L199
.L167:
	xorl	%edx, %edx
	movl	%r14d, %esi
	movq	%r15, %rdi
	call	_dl_reloc_bad_type
.L449:
	testq	%rsi, %rsi
	jne	.L165
	leaq	.LC6(%rip), %rsi
.L165:
	leaq	.LC13(%rip), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	movq	%r8, %rdx
	call	_dl_fatal_printf@PLT
.L450:
	testq	%rsi, %rsi
	jne	.L196
	leaq	.LC6(%rip), %rsi
.L196:
	leaq	.LC13(%rip), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	movq	%r8, %rdx
	call	_dl_fatal_printf@PLT
.L439:
	leaq	.LC4(%rip), %rcx
	movq	%r13, %r15
.L46:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	8(%r15), %rsi
	xorl	%edx, %edx
	movl	%fs:(%rax), %edi
	call	_dl_signal_error
.L448:
	testq	%rsi, %rsi
	jne	.L141
	leaq	.LC6(%rip), %rsi
.L141:
	leaq	.LC13(%rip), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	movq	%r8, %rdx
	call	_dl_fatal_printf@PLT
.L268:
	leaq	.LC5(%rip), %rcx
	jmp	.L46
.L447:
	testq	%rsi, %rsi
	jne	.L108
	leaq	.LC6(%rip), %rsi
.L108:
	leaq	.LC13(%rip), %rdi
	movq	%rdx, %rcx
	xorl	%eax, %eax
	movq	%r8, %rdx
	call	_dl_fatal_printf@PLT
	.size	_dl_relocate_object, .-_dl_relocate_object
	.section	.rodata
	.align 32
	.type	msg.10446, @object
	.size	msg.10446, 76
msg.10446:
	.string	"unexpected reloc type 0x"
	.zero	13
	.string	"unexpected PLT reloc type 0x"
	.zero	9
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	errstring.10440, @object
	.size	errstring.10440, 59
errstring.10440:
	.string	"cannot apply additional memory protection after relocation"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10313, @object
	.size	__PRETTY_FUNCTION__.10313, 26
__PRETTY_FUNCTION__.10313:
	.string	"elf_machine_rela_relative"
	.weak	_dl_rtld_map
	.hidden	_dl_runtime_profile_sse
	.hidden	_dl_runtime_profile_avx
	.hidden	_dl_runtime_resolve_fxsave
	.hidden	_dl_tlsdesc_undefweak
	.hidden	_dl_debug_printf
	.hidden	_dl_runtime_resolve_xsavec
	.hidden	_dl_runtime_resolve_xsave
	.hidden	__assert_fail
	.hidden	_dl_tlsdesc_return
	.hidden	_dl_error_printf
	.hidden	_dl_tlsdesc_resolve_rela
	.hidden	_dl_name_match_p
	.hidden	_dl_runtime_profile_avx512
	.hidden	_dl_lookup_symbol_x
	.hidden	_itoa_lower_digits
	.hidden	__mprotect
	.hidden	_dl_signal_error
