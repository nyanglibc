	.text
	.p2align 4,,15
	.type	call_destructors, @function
call_destructors:
	movq	272(%rdi), %rax
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	testq	%rax, %rax
	je	.L2
	movq	8(%rax), %rbp
	movq	288(%rdi), %rax
	addq	(%rdi), %rbp
	movq	8(%rax), %rax
	shrq	$3, %rax
	testl	%eax, %eax
	leal	-1(%rax), %edx
	je	.L2
	leaq	0(%rbp,%rdx,8), %rbx
	subq	$8, %rbp
	.p2align 4,,10
	.p2align 3
.L3:
	call	*(%rbx)
	subq	$8, %rbx
	cmpq	%rbx, %rbp
	jne	.L3
.L2:
	movq	168(%r12), %rdx
	testq	%rdx, %rdx
	je	.L1
	movq	(%r12), %rax
	addq	8(%rdx), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	call_destructors, .-call_destructors
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dl-close.c"
.LC1:
	.string	"! should_be_there"
.LC2:
	.string	"old_map->l_tls_modid == idx"
	.text
	.p2align 4,,15
	.type	remove_slotinfo, @function
remove_slotinfo:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rax
	subq	%rdx, %rax
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	(%rsi), %rdx
	cmpq	%rdx, %rax
	jb	.L12
	movq	8(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L13
	testb	%cl, %cl
	jne	.L29
.L17:
	movq	_dl_tls_static_nelem(%rip), %rax
	testq	%rbx, %rbx
	leaq	1(%rax), %rcx
	movl	$0, %eax
	cmovne	%rax, %rcx
	movq	%rdi, %rax
	subq	%rbx, %rax
	salq	$4, %rax
	leaq	8(%rbp,%rax), %rax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	subq	$16, %rax
	subq	$1, %rdi
	cmpq	$0, 16(%rax)
	jne	.L30
.L21:
	movq	%rdi, %rdx
	subq	%rbx, %rdx
	cmpq	%rcx, %rdx
	ja	.L22
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	salq	$4, %rax
	addq	%rsi, %rax
	movq	24(%rax), %rdx
	testq	%rdx, %rdx
	je	.L18
	cmpq	%rdi, 1120(%rdx)
	jne	.L31
	movq	_dl_tls_generation(%rip), %rsi
	movq	$0, 24(%rax)
	leaq	1(%rsi), %rdx
	movq	%rdx, 16(%rax)
.L18:
	cmpq	%rdi, _dl_tls_max_dtv_idx(%rip)
	je	.L17
.L28:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rdi, _dl_tls_max_dtv_idx(%rip)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L13:
	addq	%rbx, %rdx
	movzbl	%cl, %ecx
	call	remove_slotinfo
	testb	%al, %al
	jne	.L28
	movq	0(%rbp), %rdi
	addq	%rbx, %rdi
	jmp	.L17
.L29:
	leaq	__PRETTY_FUNCTION__.11020(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$59, %edx
	call	__assert_fail
.L31:
	leaq	__PRETTY_FUNCTION__.11020(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$80, %edx
	call	__assert_fail
	.size	remove_slotinfo, .-remove_slotinfo
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"\nclosing file=%s; direct_opencount=%u\n"
	.section	.rodata.str1.1
.LC4:
	.string	"idx == nloaded"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"(*lp)->l_idx >= 0 && (*lp)->l_idx < nloaded"
	.align 8
.LC6:
	.string	"jmap->l_idx >= 0 && jmap->l_idx < nloaded"
	.section	.rodata.str1.1
.LC7:
	.string	"imap->l_ns == nsid"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"imap->l_type == lt_loaded && !imap->l_nodelete_active"
	.section	.rodata.str1.1
.LC9:
	.string	"\ncalling fini: %s [%lu]\n\n"
.LC10:
	.string	"tmap->l_ns == nsid"
.LC11:
	.string	"cannot create scope list"
.LC12:
	.string	"dlclose"
.LC13:
	.string	"imap->l_type == lt_loaded"
.LC14:
	.string	"nsid == LM_ID_BASE"
.LC15:
	.string	"imap->l_prev != NULL"
	.section	.rodata.str1.8
	.align 8
.LC16:
	.string	"\nfile=%s [%lu];  destroying link map\n"
	.align 8
.LC17:
	.string	"TLS generation counter wrapped!  Please report as described in <https://www.gnu.org/software/libc/bugs.html>.\n"
	.text
	.p2align 4,,15
	.globl	_dl_close_worker
	.hidden	_dl_close_worker
	.type	_dl_close_worker, @function
_dl_close_worker:
	pushq	%rbp
	movabsq	$8589934592, %rcx
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$168, %rsp
	movl	792(%rdi), %eax
	movb	%sil, -114(%rbp)
	leal	-1(%rax), %edx
	movabsq	$17179869183, %rax
	movl	%edx, 792(%rdi)
	movq	792(%rdi), %rsi
	andq	%rax, %rsi
	cmpq	%rcx, %rsi
	je	.L266
	testb	$64, _dl_debug_mask(%rip)
	jne	.L267
.L32:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L266:
	movl	dl_close_state.11042(%rip), %r9d
	testl	%r9d, %r9d
	jne	.L268
	movq	48(%rdi), %r15
	movq	%rax, -80(%rbp)
	cmpq	$1, %r15
	leaq	(%r15,%r15,8), %rdx
	sbbq	%rcx, %rcx
	andl	$8, %ecx
	leaq	(%r15,%rdx,2), %rdx
	movq	%rcx, -192(%rbp)
	leaq	_dl_ns(%rip), %rcx
	salq	$3, %rdx
	leaq	40(%rcx,%rdx), %rbx
	addq	%rdx, %rcx
	movq	%rcx, -144(%rbp)
	movq	%rbx, -200(%rbp)
.L36:
	movq	-144(%rbp), %rcx
	movq	%rsp, -152(%rbp)
	movl	$1, dl_close_state.11042(%rip)
	movl	8(%rcx), %r14d
	leaq	15(%r14), %rax
	movq	%r14, %r11
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	movq	%rsp, %rbx
	subq	%rax, %rsp
	leaq	22(,%r14,8), %rax
	movq	%rsp, %r13
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	movq	(%rcx), %rax
	leaq	7(%rsp), %r12
	shrq	$3, %r12
	testq	%rax, %rax
	leaq	0(,%r12,8), %r9
	je	.L144
	movq	%r9, %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L38:
	movl	%edx, 1012(%rax)
	movq	%rax, (%rcx)
	addl	$1, %edx
	movq	24(%rax), %rax
	addq	$8, %rcx
	testq	%rax, %rax
	jne	.L38
.L37:
	cmpl	%edx, %r11d
	jne	.L269
	movq	%r14, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r9, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	memset@PLT
	movq	%r13, %rdi
	movq	%r14, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movl	-56(%rbp), %r11d
	movq	-64(%rbp), %r9
	movl	$-1, %edi
	movabsq	$8589934592, %r10
	.p2align 4,,10
	.p2align 3
.L40:
	addl	$1, %edi
	cmpl	%edi, %r11d
	jbe	.L270
.L54:
	movslq	%edi, %rax
	cmpb	$0, 0(%r13,%rax)
	jne	.L40
	movq	(%r9,%rax,8), %rcx
	movq	-80(%rbp), %rdx
	andq	792(%rcx), %rdx
	cmpq	%r10, %rdx
	je	.L42
.L45:
	movb	$1, (%rbx,%rax)
	movb	$1, 0(%r13,%rax)
	movq	976(%rcx), %rax
	movl	$-1, 1012(%rcx)
	testq	%rax, %rax
	je	.L44
	leaq	8(%rax), %rdx
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movslq	1012(%rax), %rax
	cmpl	$-1, %eax
	je	.L46
	testl	%eax, %eax
	js	.L47
	cmpl	%eax, %r11d
	jbe	.L47
	cmpb	$0, (%rbx,%rax)
	jne	.L46
	movb	$1, (%rbx,%rax)
	movq	(%rdx), %rax
	movl	1012(%rax), %eax
	leal	-1(%rax), %esi
	cmpl	%edi, %eax
	cmovle	%esi, %edi
.L46:
	addq	$8, %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L49
.L44:
	movq	984(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L40
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	je	.L40
	xorl	%edx, %edx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L271:
	cmpl	%eax, %r11d
	jbe	.L51
	cmpb	$0, (%rbx,%rax)
	jne	.L50
	movb	$1, (%rbx,%rax)
	movl	1012(%r8), %eax
	cmpl	%edi, %eax
	jg	.L262
	leal	-1(%rax), %edi
.L262:
	movq	984(%rcx), %rsi
.L50:
	addl	$1, %edx
	cmpl	(%rsi), %edx
	jnb	.L40
.L53:
	movl	%edx, %eax
	movq	8(%rsi,%rax,8), %r8
	movslq	1012(%r8), %rax
	cmpl	$-1, %eax
	je	.L50
	testl	%eax, %eax
	jns	.L271
.L51:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$248, %edx
	call	__assert_fail
.L268:
	testb	$64, _dl_debug_mask(%rip)
	movl	$2, dl_close_state.11042(%rip)
	je	.L32
.L267:
	movq	8(%rdi), %rsi
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L42:
	cmpb	$0, 799(%rcx)
	jne	.L45
	movq	1128(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L45
	cmpb	$0, (%rbx,%rax)
	jne	.L45
	addl	$1, %edi
	cmpl	%edi, %r11d
	ja	.L54
.L270:
	testq	%r15, %r15
	movl	%r11d, %esi
	movl	$1, %ecx
	sete	%al
	movl	%r11d, -56(%rbp)
	movq	%r9, -64(%rbp)
	movzbl	%al, %edx
	movzbl	%al, %eax
	subl	%eax, %esi
	movq	-192(%rbp), %rax
	addq	%rbx, %rdx
	leaq	(%r9,%rax), %rdi
	call	_dl_sort_maps
	movl	-56(%rbp), %r11d
	testl	%r11d, %r11d
	je	.L56
	movq	0(,%r12,8), %r13
	cmpq	48(%r13), %r15
	jne	.L57
	leal	-1(%r11), %eax
	xorl	%r8d, %r8d
	movl	$-1, %r10d
	movq	%r8, %r12
	movb	$0, -72(%rbp)
	movq	%rbx, %r8
	movl	%eax, -180(%rbp)
	movq	%rax, -56(%rbp)
	movl	%r10d, %ebx
	movb	$0, -113(%rbp)
	movl	$0, -88(%rbp)
	movl	%r11d, -120(%rbp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L275:
	cmpb	$2, %dl
	jne	.L60
	cmpb	$0, 799(%r13)
	jne	.L60
	testb	$8, %al
	je	.L62
	testb	$2, _dl_debug_mask(%rip)
	jne	.L272
.L63:
	cmpq	$0, 272(%r13)
	je	.L273
.L64:
	leaq	call_destructors(%rip), %rsi
	movq	%r13, %rdx
	xorl	%edi, %edi
	movq	%r8, -72(%rbp)
	call	_dl_catch_exception
	movzbl	796(%r13), %eax
	movq	-72(%rbp), %r8
.L62:
	orb	$32, 797(%r13)
	andl	$16, %eax
	movb	$1, -72(%rbp)
	cmpb	$1, %al
	sbbl	$-1, -88(%rbp)
	cmpl	%r14d, %ebx
	cmova	%r14d, %ebx
.L66:
	cmpq	%r12, -56(%rbp)
	je	.L274
.L86:
	movq	-64(%rbp), %rax
	movq	8(%rax,%r12,8), %r13
	addq	$1, %r12
	cmpq	48(%r13), %r15
	jne	.L57
.L58:
	movzbl	796(%r13), %eax
	movl	%r12d, %r14d
	movl	%eax, %edx
	andl	$3, %edx
	cmpb	$0, (%r8,%r12)
	je	.L275
	cmpb	$2, %dl
	jne	.L66
	cmpq	$0, 704(%r13)
	je	.L67
.L263:
	movq	920(%r13), %rax
	movl	$1, %edi
	movq	(%rax), %rcx
	movq	%rax, -96(%rbp)
	movq	$0, -104(%rbp)
	testq	%rcx, %rcx
	je	.L69
.L68:
	movq	-96(%rbp), %rax
	leaq	720(%r13), %rdx
	xorl	%r10d, %r10d
	leaq	8(%rax), %rsi
	movq	%rcx, %rax
	movq	%rsi, %r9
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L278:
	cmpq	-656(%rax), %r15
	jne	.L276
	cmpl	$-1, 308(%rax)
	je	.L74
	addq	$8, %r9
	movq	-8(%r9), %rax
	movl	$1, %r10d
	testq	%rax, %rax
	je	.L277
.L77:
	cmpq	%rax, %rdx
	jne	.L278
.L74:
	addq	$8, %r9
	movq	-8(%r9), %rax
	addq	$1, %rdi
	testq	%rax, %rax
	jne	.L77
.L277:
	testb	%r10b, %r10b
	je	.L78
	leaq	880(%r13), %rax
	cmpq	%rax, -96(%rbp)
	movq	%rax, -128(%rbp)
	je	.L158
	cmpq	$3, %rdi
	movq	$4, -112(%rbp)
	ja	.L158
.L79:
	movq	%r8, -136(%rbp)
	xorl	%r9d, %r9d
	movq	-104(%rbp), %r8
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L280:
	testq	%r8, %r8
	je	.L82
	movq	%r8, (%rdi)
	leaq	8(%rax,%r11), %rdi
	addq	$1, %r9
	xorl	%r8d, %r8d
.L82:
	addq	$8, %rsi
	movq	-8(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L279
.L83:
	leaq	0(,%r9,8), %r11
	cmpq	%rcx, %rdx
	leaq	(%rax,%r11), %rdi
	je	.L81
	cmpl	$-1, 308(%rcx)
	jne	.L280
.L81:
	addq	$8, %rsi
	movq	%rcx, (%rdi)
	addq	$1, %r9
	movq	-8(%rsi), %rcx
	leaq	8(%rax,%r11), %rdi
	testq	%rcx, %rcx
	jne	.L83
.L279:
	movq	$0, (%rdi)
	movq	%rax, 920(%r13)
	movq	-96(%rbp), %rdi
	movq	-128(%rbp), %rax
	movq	-136(%rbp), %r8
	cmpq	%rax, %rdi
	je	.L151
	movq	%r8, -96(%rbp)
	call	_dl_scope_free
	testl	%eax, %eax
	movzbl	-113(%rbp), %eax
	movl	$0, %ecx
	movq	-96(%rbp), %r8
	cmovne	%ecx, %eax
	movb	%al, -113(%rbp)
.L84:
	movq	-112(%rbp), %rax
	movq	%rax, 912(%r13)
.L69:
	movq	736(%r13), %rax
	testq	%rax, %rax
	je	.L85
	cmpl	$-1, 1012(%rax)
	je	.L85
	movq	$0, 736(%r13)
.L85:
	cmpl	%r14d, %ebx
	cmova	%r14d, %ebx
	cmpq	%r12, -56(%rbp)
	jne	.L86
.L274:
	cmpb	$0, -72(%rbp)
	movl	%ebx, %r10d
	movl	-120(%rbp), %r11d
	movq	%r8, %rbx
	movq	-64(%rbp), %r9
	je	.L56
	xorl	%edi, %edi
	movq	%r15, %rsi
	movq	%r9, -96(%rbp)
	movl	%r10d, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	_dl_debug_initialize
	movl	$2, 24(%rax)
	movq	%rax, -104(%rbp)
	call	_dl_debug_state@PLT
	movl	-88(%rbp), %ecx
	movl	-56(%rbp), %r11d
	movl	-64(%rbp), %r10d
	movq	-96(%rbp), %r9
	testl	%ecx, %ecx
	jne	.L281
.L88:
#APP
# 530 "dl-close.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L96
.L100:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L98
	leaq	_dl_load_write_lock(%rip), %rdi
	movq	%r9, -88(%rbp)
	movl	%r10d, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	__pthread_mutex_lock@PLT
	movl	-56(%rbp), %r11d
	movl	-64(%rbp), %r10d
	movq	-88(%rbp), %r9
.L98:
	cmpl	%r10d, %r11d
	jbe	.L282
	movl	%r10d, %eax
	movb	$0, -56(%rbp)
	movq	$0, -64(%rbp)
	leaq	(%rbx,%rax), %r12
	leaq	(%r9,%rax,8), %r13
	leaq	1(%rbx,%rax), %rax
	movl	-180(%rbp), %ebx
	movq	$0, -96(%rbp)
	movq	%r15, -88(%rbp)
	subl	%r10d, %ebx
	addq	%rax, %rbx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$1, %r12
	addq	$8, %r13
	cmpq	%r12, %rbx
	je	.L283
.L132:
	cmpb	$0, (%r12)
	jne	.L102
	movq	0(%r13), %r14
	movzbl	796(%r14), %eax
	movl	%eax, %edx
	andl	$3, %edx
	cmpb	$2, %dl
	jne	.L284
	cmpq	$0, 1088(%r14)
	jne	.L285
.L104:
	cmpb	$0, -114(%rbp)
	je	.L112
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L113
	movq	-200(%rbp), %rdi
	call	__pthread_mutex_lock@PLT
.L113:
	movq	-144(%rbp), %rax
	movq	80(%rax), %rax
	testq	%rax, %rax
	je	.L117
	movq	-144(%rbp), %rcx
	movq	88(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L117
	salq	$5, %rdx
	addq	%rax, %rdx
	jmp	.L119
.L118:
	addq	$32, %rax
	cmpq	%rax, %rdx
	je	.L117
.L119:
	cmpq	$0, 8(%rax)
	je	.L118
	cmpq	24(%rax), %r14
	jne	.L118
	movq	$0, 8(%rax)
	movl	$0, (%rax)
	addq	$32, %rax
	subq	$1, 96(%rcx)
	cmpq	%rax, %rdx
	jne	.L119
.L117:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L112
	movq	-200(%rbp), %rdi
	call	__pthread_mutex_unlock@PLT
.L112:
	movq	%r14, %rdi
	call	_dl_unmap
	cmpq	$0, -88(%rbp)
	jne	.L286
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L287
	movq	24(%r14), %rdx
	subl	$1, 8+_dl_ns(%rip)
	movq	%rdx, 24(%rax)
	movq	24(%r14), %rdx
	testq	%rdx, %rdx
	je	.L122
	movq	%rax, 32(%rdx)
.L122:
	movq	744(%r14), %rdi
	call	free@PLT
	movq	848(%r14), %rdi
	cmpq	$-1, %rdi
	je	.L123
	call	free@PLT
.L123:
	movq	984(%r14), %rdi
	call	free@PLT
	testb	$64, _dl_debug_mask(%rip)
	jne	.L288
.L124:
	movq	8(%r14), %rdi
	call	free@PLT
	movq	56(%r14), %rdi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L125:
	testq	%r15, %r15
	movq	%r15, %rdi
	je	.L289
.L126:
	movl	16(%rdi), %eax
	movq	8(%rdi), %r15
	testl	%eax, %eax
	jne	.L125
	call	free@PLT
	testq	%r15, %r15
	movq	%r15, %rdi
	jne	.L126
.L289:
	movq	976(%r14), %rdi
	call	free@PLT
	movq	920(%r14), %rdi
	leaq	880(%r14), %rax
	cmpq	%rax, %rdi
	je	.L127
	call	free@PLT
.L127:
	cmpb	$0, 796(%r14)
	js	.L290
.L128:
	movq	816(%r14), %rdi
	cmpq	$-1, %rdi
	je	.L129
	call	free@PLT
.L129:
	movq	960(%r14), %rdi
	cmpq	$-1, %rdi
	je	.L130
	call	free@PLT
.L130:
	cmpq	_dl_initfirst(%rip), %r14
	je	.L291
.L131:
	movq	%r14, %rdi
	addq	$1, %r12
	addq	$8, %r13
	call	free@PLT
	cmpq	%r12, %rbx
	jne	.L132
.L283:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movq	-88(%rbp), %r15
	je	.L136
	leaq	_dl_load_write_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L136:
	cmpb	$0, -56(%rbp)
	je	.L138
	movq	_dl_tls_generation(%rip), %rax
	addq	$1, %rax
	testq	%rax, %rax
	movq	%rax, _dl_tls_generation(%rip)
	je	.L292
	movq	-64(%rbp), %rax
	cmpq	%rax, _dl_tls_static_used(%rip)
	je	.L293
.L138:
	movq	-144(%rbp), %rax
	cmpq	$0, (%rax)
	je	.L294
.L141:
	movq	-104(%rbp), %rax
	movl	$0, 24(%rax)
	call	_dl_debug_state@PLT
.L56:
	cmpl	$2, dl_close_state.11042(%rip)
	jne	.L295
	movq	-152(%rbp), %rsp
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L273:
	cmpq	$0, 168(%r13)
	jne	.L64
	movzbl	796(%r13), %eax
	jmp	.L62
.L78:
	cmpq	$0, -104(%rbp)
	je	.L69
.L73:
	movq	$0, 704(%r13)
	movl	$0, 712(%r13)
	jmp	.L69
.L67:
	movq	976(%r13), %rdx
	testq	%rdx, %rdx
	je	.L263
	cmpq	$0, 8(%rdx)
	je	.L147
	movl	$1, %ecx
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L148:
	movl	%eax, %ecx
.L72:
	leal	1(%rcx), %esi
	cmpq	$0, (%rdx,%rsi,8)
	movq	%rsi, %rax
	jne	.L148
	addl	$2, %ecx
	salq	$3, %rcx
.L71:
	movl	%eax, 712(%r13)
	leaq	704(%r13), %rax
	addq	%rcx, %rdx
	movq	%rdx, 704(%r13)
	movq	%rax, -104(%rbp)
	movq	920(%r13), %rax
	movq	(%rax), %rcx
	movq	%rax, -96(%rbp)
	testq	%rcx, %rcx
	je	.L73
	movl	$2, %edi
	jmp	.L68
.L158:
	movq	912(%r13), %rax
	movq	%r8, -176(%rbp)
	movb	%r10b, -115(%rbp)
	movq	%rsi, -168(%rbp)
	movq	%rcx, -160(%rbp)
	movq	%rdx, -136(%rbp)
	leaq	0(,%rax,8), %rdi
	movq	%rax, -112(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	-136(%rbp), %rdx
	movq	-160(%rbp), %rcx
	movq	-168(%rbp), %rsi
	movzbl	-115(%rbp), %r10d
	movq	-176(%rbp), %r8
	jne	.L79
	leaq	.LC11(%rip), %rcx
	leaq	.LC12(%rip), %rsi
	xorl	%edx, %edx
	movl	$12, %edi
	call	_dl_signal_error
	.p2align 4,,10
	.p2align 3
.L272:
	movq	8(%r13), %rsi
	leaq	.LC9(%rip), %rdi
	movq	%r15, %rdx
	xorl	%eax, %eax
	movq	%r8, -72(%rbp)
	call	_dl_debug_printf
	movq	-72(%rbp), %r8
	jmp	.L63
.L151:
	movb	%r10b, -113(%rbp)
	jmp	.L84
.L291:
	movq	$0, _dl_initfirst(%rip)
	jmp	.L131
.L290:
	movq	680(%r14), %rdi
	call	free@PLT
	jmp	.L128
.L144:
	xorl	%edx, %edx
	jmp	.L37
.L288:
	movq	48(%r14), %rdx
	movq	8(%r14), %rsi
	leaq	.LC16(%rip), %rdi
	xorl	%eax, %eax
	call	_dl_debug_printf
	jmp	.L124
.L285:
	movq	_dl_tls_dtv_slotinfo_list(%rip), %rsi
	testq	%rsi, %rsi
	je	.L106
	shrb	$3, %al
	movq	1120(%r14), %rdi
	xorl	%edx, %edx
	movl	%eax, %ecx
	andl	$1, %ecx
	call	remove_slotinfo
	testb	%al, %al
	je	.L296
.L106:
	movq	1112(%r14), %rax
	movzbl	-72(%rbp), %ecx
	leaq	1(%rax), %rdx
	movb	%cl, -56(%rbp)
	cmpq	$1, %rdx
	jbe	.L104
	movq	-96(%rbp), %rcx
	movq	%rax, %rdx
	testq	%rcx, %rcx
	sete	%sil
	cmpq	%rcx, %rax
	sete	%cl
	subq	1088(%r14), %rdx
	orb	%cl, %sil
	movb	%sil, -56(%rbp)
	je	.L108
	cmpq	$0, -64(%rbp)
	jne	.L265
	movq	%rax, -64(%rbp)
.L265:
	movq	%rdx, -96(%rbp)
	jmp	.L104
.L47:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$223, %edx
	call	__assert_fail
.L147:
	movl	$16, %ecx
	movl	$1, %eax
	jmp	.L71
.L281:
	movq	-144(%rbp), %rax
	movq	16(%rax), %r8
	movl	8(%r8), %esi
	testl	%esi, %esi
	je	.L153
	movq	(%r8), %rdi
	leal	-1(%rsi), %edx
	movq	%rdx, %rax
	movq	(%rdi,%rdx,8), %rdx
	testb	$32, 797(%rdx)
	jne	.L90
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L92:
	leal	-1(%rax), %ecx
	movq	%rcx, %rdx
	movq	(%rdi,%rcx,8), %rcx
	testb	$32, 797(%rcx)
	je	.L91
	movl	%edx, %eax
.L90:
	testl	%eax, %eax
	jne	.L92
.L89:
	movl	%eax, 8(%r8)
	jmp	.L88
.L296:
	movq	_dl_tls_static_nelem(%rip), %rax
	movq	%rax, _dl_tls_max_dtv_idx(%rip)
	jmp	.L106
.L108:
	movq	-64(%rbp), %rdi
	cmpq	%rdi, %rdx
	je	.L156
	movq	_dl_tls_static_used(%rip), %rcx
	cmpq	%rdi, %rcx
	je	.L298
	cmpq	%rcx, %rax
	je	.L299
	cmpq	-64(%rbp), %rax
	jbe	.L264
	movq	%rax, -64(%rbp)
	movq	%rdx, -96(%rbp)
.L264:
	movzbl	-72(%rbp), %eax
	movb	%al, -56(%rbp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%rax, -64(%rbp)
	movzbl	-72(%rbp), %eax
	movb	%al, -56(%rbp)
	jmp	.L104
.L294:
	movq	_dl_nns(%rip), %rax
	subq	$1, %rax
	cmpq	%r15, %rax
	jne	.L141
	movq	%r15, _dl_nns(%rip)
	jmp	.L141
.L293:
	movq	-96(%rbp), %rax
	movq	%rax, _dl_tls_static_used(%rip)
	jmp	.L138
.L96:
	movl	-88(%rbp), %edx
	testl	%edx, %edx
	jne	.L99
	cmpb	$0, -113(%rbp)
	jne	.L99
	movq	_dl_scope_free_list(%rip), %rax
	testq	%rax, %rax
	je	.L100
	cmpq	$0, (%rax)
	je	.L100
.L99:
	movq	%r9, -88(%rbp)
	movl	%r10d, -64(%rbp)
	movl	%r11d, -56(%rbp)
	call	__thread_gscope_wait
	movq	_dl_scope_free_list(%rip), %r12
	movl	-56(%rbp), %r11d
	movl	-64(%rbp), %r10d
	movq	-88(%rbp), %r9
	testq	%r12, %r12
	je	.L100
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L100
	movl	%r11d, %r13d
	movl	%r10d, %r14d
.L101:
	subq	$1, %rax
	movq	%r9, -56(%rbp)
	movq	8(%r12,%rax,8), %rdi
	movq	%rax, (%r12)
	call	free@PLT
	movq	(%r12), %rax
	movq	-56(%rbp), %r9
	testq	%rax, %rax
	jne	.L101
	movl	%r13d, %r11d
	movl	%r14d, %r10d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L295:
	movl	$0, dl_close_state.11042(%rip)
	movq	-152(%rbp), %rsp
	jmp	.L32
.L276:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$372, %edx
	call	__assert_fail
.L60:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$282, %edx
	call	__assert_fail
.L57:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$278, %edx
	call	__assert_fail
.L297:
	movl	%esi, %eax
.L91:
	movl	-88(%rbp), %ecx
	leal	(%rax,%rcx), %edx
	cmpl	%edx, %esi
	je	.L89
	subl	$1, %eax
	xorl	%edx, %edx
	leaq	1(%rax), %r12
	xorl	%eax, %eax
.L95:
	movq	(%rdi,%rdx,8), %rcx
	testb	$32, 797(%rcx)
	jne	.L93
	cmpl	%eax, %edx
	je	.L94
	movl	%eax, %esi
	movq	%rcx, (%rdi,%rsi,8)
.L94:
	addl	$1, %eax
.L93:
	addq	$1, %rdx
	cmpq	%rdx, %r12
	jne	.L95
	jmp	.L89
.L269:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$181, %edx
	call	__assert_fail
.L153:
	xorl	%eax, %eax
	jmp	.L89
.L299:
	movzbl	-72(%rbp), %eax
	movq	%rdx, _dl_tls_static_used(%rip)
	movb	%al, -56(%rbp)
	jmp	.L104
.L298:
	movq	%rax, -64(%rbp)
	movzbl	-72(%rbp), %eax
	movq	-96(%rbp), %rcx
	movq	%rdx, -96(%rbp)
	movq	%rcx, _dl_tls_static_used(%rip)
	movb	%al, -56(%rbp)
	jmp	.L104
.L287:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$700, %edx
	call	__assert_fail
.L286:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$699, %edx
	call	__assert_fail
.L284:
	leaq	__PRETTY_FUNCTION__.11056(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$559, %edx
	call	__assert_fail
.L282:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L138
	leaq	_dl_load_write_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
	jmp	.L138
.L292:
	leaq	.LC17(%rip), %rdi
	call	_dl_fatal_printf@PLT
	.size	_dl_close_worker, .-_dl_close_worker
	.section	.rodata.str1.1
.LC18:
	.string	"shared object not open"
	.text
	.p2align 4,,15
	.globl	_dl_close
	.hidden	_dl_close
	.type	_dl_close, @function
_dl_close:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	movq	%rdi, %rbx
	je	.L301
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L301:
	cmpb	$0, 799(%rbx)
	jne	.L315
	movl	792(%rbx), %eax
	testl	%eax, %eax
	je	.L316
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_close_worker
.L315:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L317
	popq	%rbx
	leaq	_dl_load_lock(%rip), %rdi
	jmp	__pthread_mutex_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L317:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L306
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L306:
	movq	8(%rbx), %rsi
	leaq	.LC18(%rip), %rcx
	xorl	%edx, %edx
	xorl	%edi, %edi
	call	_dl_signal_error
	.size	_dl_close, .-_dl_close
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11020, @object
	.size	__PRETTY_FUNCTION__.11020, 16
__PRETTY_FUNCTION__.11020:
	.string	"remove_slotinfo"
	.align 16
	.type	__PRETTY_FUNCTION__.11056, @object
	.size	__PRETTY_FUNCTION__.11056, 17
__PRETTY_FUNCTION__.11056:
	.string	"_dl_close_worker"
	.local	dl_close_state.11042
	.comm	dl_close_state.11042,4,4
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__thread_gscope_wait
	.hidden	_dl_signal_error
	.hidden	_dl_unmap
	.hidden	_dl_debug_initialize
	.hidden	_dl_scope_free
	.hidden	_dl_catch_exception
	.hidden	_dl_sort_maps
	.hidden	_dl_debug_printf
	.hidden	__assert_fail
