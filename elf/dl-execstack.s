	.text
	.p2align 4,,15
	.globl	_dl_make_stack_executable
	.type	_dl_make_stack_executable, @function
_dl_make_stack_executable:
	movq	_dl_pagesize(%rip), %rsi
	pushq	%rbx
	movq	%rdi, %rbx
	movl	__stack_prot(%rip), %edx
	movq	%rsi, %rdi
	negq	%rdi
	andq	(%rbx), %rdi
	call	__mprotect
	testl	%eax, %eax
	jne	.L6
.L2:
.L3:
	movq	$0, (%rbx)
	orl	$1, _dl_stack_flags(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	popq	%rbx
	movl	%fs:(%rax), %eax
	ret
	.size	_dl_make_stack_executable, .-_dl_make_stack_executable
	.hidden	__mprotect
	.hidden	__stack_prot
