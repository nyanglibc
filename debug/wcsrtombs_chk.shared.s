	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcsrtombs_chk
	.type	__wcsrtombs_chk, @function
__wcsrtombs_chk:
	cmpq	%rdx, %r8
	jb	.L7
	jmp	__wcsrtombs
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__wcsrtombs_chk, .-__wcsrtombs_chk
	.hidden	__wcsrtombs
