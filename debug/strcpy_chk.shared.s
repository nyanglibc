	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__strcpy_chk
	.type	__strcpy_chk, @function
__strcpy_chk:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	__GI_strlen
	cmpq	%rbp, %rax
	jnb	.L5
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	1(%rax), %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__GI_memcpy@PLT
.L5:
	call	__GI___chk_fail
	.size	__strcpy_chk, .-__strcpy_chk
