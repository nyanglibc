	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vfwprintf_chk
	.type	__vfwprintf_chk, @function
__vfwprintf_chk:
.LFB68:
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	%rax, %rsi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vfwprintf_internal
.LFE68:
	.size	__vfwprintf_chk, .-__vfwprintf_chk
	.hidden	__vfwprintf_internal
