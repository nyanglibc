	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__recv_chk
	.type	__recv_chk, @function
__recv_chk:
.LFB16:
	cmpq	%rcx, %rdx
	ja	.L7
	movl	%r8d, %ecx
	jmp	__GI___recv
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
.LFE16:
	.size	__recv_chk, .-__recv_chk
