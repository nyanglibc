	.file	"segfault.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/debug
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/debug/libSegFault/segfault.shared.v.d
# -MF /run/asm/debug/libSegFault/segfault.os.dt -MP
# -MT /run/asm/debug/libSegFault/segfault.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libSegFault -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h ../sysdeps/unix/sysv/linux/segfault.c
# -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/debug/libSegFault/segfault.shared.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*** "
.LC1:
	.string	"signal "
.LC2:
	.string	"\n"
.LC3:
	.string	"Register dump:\n\n RAX: "
.LC4:
	.string	"   RBX: "
.LC5:
	.string	"   RCX: "
.LC6:
	.string	"\n RDX: "
.LC7:
	.string	"   RSI: "
.LC8:
	.string	"   RDI: "
.LC9:
	.string	"\n RBP: "
.LC10:
	.string	"   R8 : "
.LC11:
	.string	"   R9 : "
.LC12:
	.string	"\n R10: "
.LC13:
	.string	"   R11: "
.LC14:
	.string	"   R12: "
.LC15:
	.string	"\n R13: "
.LC16:
	.string	"   R14: "
.LC17:
	.string	"   R15: "
.LC18:
	.string	"\n RSP: "
.LC19:
	.string	"\n\n RIP: "
.LC20:
	.string	"   EFLAGS: "
.LC21:
	.string	"\n\n CS: "
.LC22:
	.string	"   FS: "
.LC23:
	.string	"   GS: "
.LC24:
	.string	"\n\n Trap: "
.LC25:
	.string	"   Error: "
.LC26:
	.string	"   OldMask: "
.LC27:
	.string	"   CR2: "
.LC28:
	.string	"\n\n FPUCW: "
.LC29:
	.string	"   FPUSW: "
.LC30:
	.string	"   TAG: "
.LC31:
	.string	"\n RIP: "
.LC32:
	.string	"   RDP: "
.LC33:
	.string	"\n\n ST(0) "
.LC34:
	.string	" "
.LC35:
	.string	"   ST(1) "
.LC36:
	.string	"\n ST(2) "
.LC37:
	.string	"   ST(3) "
.LC38:
	.string	"\n ST(4) "
.LC39:
	.string	"   ST(5) "
.LC40:
	.string	"\n ST(6) "
.LC41:
	.string	"   ST(7) "
.LC42:
	.string	"\n mxcsr: "
.LC43:
	.string	"\n XMM0:  "
.LC44:
	.string	" XMM1:  "
.LC45:
	.string	"\n XMM2:  "
.LC46:
	.string	" XMM3:  "
.LC47:
	.string	"\n XMM4:  "
.LC48:
	.string	" XMM5:  "
.LC49:
	.string	"\n XMM6:  "
.LC50:
	.string	" XMM7:  "
.LC51:
	.string	"\n XMM8:  "
.LC52:
	.string	" XMM9:  "
.LC53:
	.string	"\n XMM10: "
.LC54:
	.string	" XMM11: "
.LC55:
	.string	"\n XMM12: "
.LC56:
	.string	" XMM13: "
.LC57:
	.string	"\n XMM14: "
.LC58:
	.string	" XMM15: "
.LC59:
	.string	"\nBacktrace:\n"
.LC60:
	.string	"/proc/self/maps"
.LC61:
	.string	"\nMemory map:\n\n"
	.text
	.p2align 4,,15
	.type	catch_segfault, @function
catch_segfault:
.LFB78:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%r15	#
	pushq	%r14	#
	pushq	%r13	#
	pushq	%r12	#
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movl	%edi, %r13d	# signal, signal
	pushq	%rbx	#
	movq	%rdx, %r15	# ctx, ctx
	subq	$3528, %rsp	#,
	.cfi_offset 3, -56
# ../debug/segfault.c:79:   if (fname != NULL)
	movq	fname(%rip), %rdi	# fname, fname.0_1
	testq	%rdi, %rdi	# fname.0_1
	je	.L4	#,
# ../debug/segfault.c:81:       fd = open (fname, O_TRUNC | O_WRONLY | O_CREAT, 0666);
	xorl	%eax, %eax	#
	movl	$438, %edx	#,
	movl	$577, %esi	#,
	call	open@PLT	#
# ../debug/segfault.c:82:       if (fd == -1)
	cmpl	$-1, %eax	#, fd
# ../debug/segfault.c:81:       fd = open (fname, O_TRUNC | O_WRONLY | O_CREAT, 0666);
	movl	%eax, %r12d	#, fd
# ../debug/segfault.c:82:       if (fd == -1)
	je	.L4	#,
.L3:
# ../debug/segfault.c:86:   WRITE_STRING ("*** ");
	leaq	.LC0(%rip), %rsi	#,
	movl	$4, %edx	#,
	movl	%r12d, %edi	# fd,
# ../debug/segfault.c:59:   char *ptr = _itoa_word (signal, &buf[sizeof (buf)], 10, 0);
	leaq	-2400(%rbp), %rbx	#, tmp1262
# ../debug/segfault.c:86:   WRITE_STRING ("*** ");
	call	write@PLT	#
	movq	_itoa_lower_digits@GOTPCREL(%rip), %r14	#, tmp1261
# ../debug/segfault.c:59:   char *ptr = _itoa_word (signal, &buf[sizeof (buf)], 10, 0);
	leaq	30(%rbx), %r8	#, buflim
	movslq	%r13d, %rcx	# signal, value
# ../sysdeps/generic/_itoa.h:76:       SPECIAL (10);
	movabsq	$-3689348814741910323, %rsi	#, tmp622
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rcx, %rax	# value, tmp1267
	subq	$1, %r8	#, buflim
	mulq	%rsi	# tmp622
	shrq	$3, %rdx	#, tmp620
	leaq	(%rdx,%rdx,4), %rax	#, tmp625
	addq	%rax, %rax	# tmp626
	subq	%rax, %rcx	# tmp626, tmp627
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %eax	# *_777, *_777
	movq	%rdx, %rcx	# tmp620, value
	movb	%al, (%r8)	# *_777, MEM[base: buflim_779, offset: 0B]
	jne	.L5	#,
# ../debug/segfault.c:60:   WRITE_STRING ("signal ");
	leaq	.LC1(%rip), %rsi	#,
	movl	$7, %edx	#,
	movl	%r12d, %edi	# fd,
	movq	%r8, -3560(%rbp)	# buflim, %sfp
	call	write@PLT	#
# ../debug/segfault.c:61:   write (fd, buf, &buf[sizeof (buf)] - ptr);
	movq	-3560(%rbp), %r8	# %sfp, buflim
	leaq	30(%rbx), %rdx	#, tmp634
	movq	%rbx, %rsi	# tmp1262,
	movl	%r12d, %edi	# fd,
	subq	%r8, %rdx	# buflim, tmp635
	call	write@PLT	#
# ../debug/segfault.c:88:   WRITE_STRING ("\n");
	leaq	.LC2(%rip), %rsi	#,
	movl	$1, %edx	#,
	movl	%r12d, %edi	# fd,
	call	write@PLT	#
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:86:   hexvalue (ctx->uc_mcontext.gregs[REG_RAX], regs[0], 16);
	leaq	-3312(%rbp), %rax	#, tmp1265
	movq	144(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	16(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L6:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp637
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp637
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_64, *_64
	movb	%sil, (%rdx)	# *_64, MEM[base: buflim_66, offset: 0B]
	jne	.L6	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rax, %rdx	# tmp1265, cp
	jbe	.L7	#,
	.p2align 4,,10
	.p2align 3
.L8:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_70, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rax, %rdx	# tmp1265, cp
	jne	.L8	#,
.L7:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:87:   hexvalue (ctx->uc_mcontext.gregs[REG_RBX], regs[1], 16);
	movq	128(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	32(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L9:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp642
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp642
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_75, *_75
	movb	%sil, (%rdx)	# *_75, MEM[base: buflim_77, offset: 0B]
	jne	.L9	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	16(%rax), %rcx	#, tmp646
	cmpq	%rcx, %rdx	# tmp646, cp
	jbe	.L10	#,
	.p2align 4,,10
	.p2align 3
.L11:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_81, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp648, cp
	jne	.L11	#,
.L10:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:88:   hexvalue (ctx->uc_mcontext.gregs[REG_RCX], regs[2], 16);
	movq	152(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	48(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L12:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp649
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp649
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_86, *_86
	movb	%sil, (%rdx)	# *_86, MEM[base: buflim_88, offset: 0B]
	jne	.L12	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	32(%rax), %rcx	#, tmp653
	cmpq	%rcx, %rdx	# tmp653, cp
	jbe	.L13	#,
	.p2align 4,,10
	.p2align 3
.L14:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_92, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp655, cp
	jne	.L14	#,
.L13:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:89:   hexvalue (ctx->uc_mcontext.gregs[REG_RDX], regs[3], 16);
	movq	136(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	64(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L15:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp656
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp656
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_97, *_97
	movb	%sil, (%rdx)	# *_97, MEM[base: buflim_99, offset: 0B]
	jne	.L15	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	48(%rax), %rcx	#, tmp660
	cmpq	%rcx, %rdx	# tmp660, cp
	jbe	.L16	#,
	.p2align 4,,10
	.p2align 3
.L17:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_103, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp662, cp
	jne	.L17	#,
.L16:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:90:   hexvalue (ctx->uc_mcontext.gregs[REG_RSI], regs[4], 16);
	movq	112(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	80(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L18:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp663
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp663
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_108, *_108
	movb	%sil, (%rdx)	# *_108, MEM[base: buflim_110, offset: 0B]
	jne	.L18	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	64(%rax), %rcx	#, tmp667
	cmpq	%rcx, %rdx	# tmp667, cp
	jbe	.L19	#,
	.p2align 4,,10
	.p2align 3
.L20:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_114, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp669, cp
	jne	.L20	#,
.L19:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:91:   hexvalue (ctx->uc_mcontext.gregs[REG_RDI], regs[5], 16);
	movq	104(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	96(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L21:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp670
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp670
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_119, *_119
	movb	%sil, (%rdx)	# *_119, MEM[base: buflim_121, offset: 0B]
	jne	.L21	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	80(%rax), %rcx	#, tmp674
	cmpq	%rcx, %rdx	# tmp674, cp
	jbe	.L22	#,
	.p2align 4,,10
	.p2align 3
.L23:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_125, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp676, cp
	jne	.L23	#,
.L22:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:92:   hexvalue (ctx->uc_mcontext.gregs[REG_RBP], regs[6], 16);
	movq	120(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	112(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L24:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp677
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp677
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_130, *_130
	movb	%sil, (%rdx)	# *_130, MEM[base: buflim_132, offset: 0B]
	jne	.L24	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	96(%rax), %rcx	#, tmp681
	cmpq	%rcx, %rdx	# tmp681, cp
	jbe	.L25	#,
	.p2align 4,,10
	.p2align 3
.L26:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_136, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp683, cp
	jne	.L26	#,
.L25:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:93:   hexvalue (ctx->uc_mcontext.gregs[REG_R8], regs[7], 16);
	movq	40(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	128(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L27:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp684
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp684
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_141, *_141
	movb	%sil, (%rdx)	# *_141, MEM[base: buflim_143, offset: 0B]
	jne	.L27	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	112(%rax), %rcx	#, tmp688
	cmpq	%rcx, %rdx	# tmp688, cp
	jbe	.L28	#,
	.p2align 4,,10
	.p2align 3
.L29:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_147, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp690, cp
	jne	.L29	#,
.L28:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:94:   hexvalue (ctx->uc_mcontext.gregs[REG_R9], regs[8], 16);
	movq	48(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	144(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L30:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp691
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp691
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_152, *_152
	movb	%sil, (%rdx)	# *_152, MEM[base: buflim_154, offset: 0B]
	jne	.L30	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	128(%rax), %rcx	#, tmp695
	cmpq	%rcx, %rdx	# tmp695, cp
	jbe	.L31	#,
	.p2align 4,,10
	.p2align 3
.L32:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_158, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp697, cp
	jne	.L32	#,
.L31:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:95:   hexvalue (ctx->uc_mcontext.gregs[REG_R10], regs[9], 16);
	movq	56(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	160(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L33:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp698
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp698
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_163, *_163
	movb	%sil, (%rdx)	# *_163, MEM[base: buflim_165, offset: 0B]
	jne	.L33	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	144(%rax), %rcx	#, tmp702
	cmpq	%rcx, %rdx	# tmp702, cp
	jbe	.L34	#,
	.p2align 4,,10
	.p2align 3
.L35:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_169, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp704, cp
	jne	.L35	#,
.L34:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:96:   hexvalue (ctx->uc_mcontext.gregs[REG_R11], regs[10], 16);
	movq	64(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	176(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L36:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp705
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp705
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_174, *_174
	movb	%sil, (%rdx)	# *_174, MEM[base: buflim_176, offset: 0B]
	jne	.L36	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	160(%rax), %rcx	#, tmp709
	cmpq	%rcx, %rdx	# tmp709, cp
	jbe	.L37	#,
	.p2align 4,,10
	.p2align 3
.L38:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_180, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp711, cp
	jne	.L38	#,
.L37:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:97:   hexvalue (ctx->uc_mcontext.gregs[REG_R12], regs[11], 16);
	movq	72(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	192(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L39:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp712
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp712
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_185, *_185
	movb	%sil, (%rdx)	# *_185, MEM[base: buflim_187, offset: 0B]
	jne	.L39	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	176(%rax), %rcx	#, tmp716
	cmpq	%rcx, %rdx	# tmp716, cp
	jbe	.L40	#,
	.p2align 4,,10
	.p2align 3
.L41:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_191, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp718, cp
	jne	.L41	#,
.L40:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:98:   hexvalue (ctx->uc_mcontext.gregs[REG_R13], regs[12], 16);
	movq	80(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	208(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L42:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp719
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp719
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_196, *_196
	movb	%sil, (%rdx)	# *_196, MEM[base: buflim_198, offset: 0B]
	jne	.L42	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	192(%rax), %rcx	#, tmp723
	cmpq	%rcx, %rdx	# tmp723, cp
	jbe	.L43	#,
	.p2align 4,,10
	.p2align 3
.L44:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_202, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp725, cp
	jne	.L44	#,
.L43:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:99:   hexvalue (ctx->uc_mcontext.gregs[REG_R14], regs[13], 16);
	movq	88(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	224(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L45:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp726
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp726
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_207, *_207
	movb	%sil, (%rdx)	# *_207, MEM[base: buflim_209, offset: 0B]
	jne	.L45	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	208(%rax), %rcx	#, tmp730
	cmpq	%rcx, %rdx	# tmp730, cp
	jbe	.L46	#,
	.p2align 4,,10
	.p2align 3
.L47:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_213, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp732, cp
	jne	.L47	#,
.L46:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:100:   hexvalue (ctx->uc_mcontext.gregs[REG_R15], regs[14], 16);
	movq	96(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	240(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L48:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp733
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp733
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_218, *_218
	movb	%sil, (%rdx)	# *_218, MEM[base: buflim_220, offset: 0B]
	jne	.L48	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	224(%rax), %rcx	#, tmp737
	cmpq	%rcx, %rdx	# tmp737, cp
	jbe	.L49	#,
	.p2align 4,,10
	.p2align 3
.L50:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_224, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp739, cp
	jne	.L50	#,
.L49:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:101:   hexvalue (ctx->uc_mcontext.gregs[REG_RSP], regs[15], 16);
	movq	160(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	256(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L51:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp740
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp740
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_229, *_229
	movb	%sil, (%rdx)	# *_229, MEM[base: buflim_231, offset: 0B]
	jne	.L51	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	240(%rax), %rcx	#, tmp744
	cmpq	%rcx, %rdx	# tmp744, cp
	jbe	.L52	#,
	.p2align 4,,10
	.p2align 3
.L53:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_235, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp746, cp
	jne	.L53	#,
.L52:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:102:   hexvalue (ctx->uc_mcontext.gregs[REG_RIP], regs[16], 16);
	movq	168(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	272(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L54:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp747
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp747
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_240, *_240
	movb	%sil, (%rdx)	# *_240, MEM[base: buflim_242, offset: 0B]
	jne	.L54	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	256(%rax), %rcx	#, tmp751
	cmpq	%rcx, %rdx	# tmp751, cp
	jbe	.L55	#,
	.p2align 4,,10
	.p2align 3
.L56:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_246, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp753, cp
	jne	.L56	#,
.L55:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:104:   hexvalue (ctx->uc_mcontext.gregs[REG_EFL], regs[17], 8);
	movq	176(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	280(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L57:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp754
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp754
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_251, *_251
	movb	%sil, (%rdx)	# *_251, MEM[base: buflim_253, offset: 0B]
	jne	.L57	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	272(%rax), %rcx	#, tmp758
	cmpq	%rcx, %rdx	# tmp758, cp
	jbe	.L58	#,
	.p2align 4,,10
	.p2align 3
.L59:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_257, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp760, cp
	jne	.L59	#,
.L58:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:105:   hexvalue (ctx->uc_mcontext.gregs[REG_CSGSFS] & 0xffff, regs[18], 4);
	movq	184(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, _258
	leaq	292(%rax), %rdx	#, cp
	movzwl	%cx, %esi	# _258, value
	.p2align 4,,10
	.p2align 3
.L60:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rsi, %rdi	# value, tmp761
	shrq	$4, %rsi	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %edi	#, tmp761
	testq	%rsi, %rsi	# value
	movzbl	(%rdi,%r14), %edi	# *_263, *_263
	movb	%dil, (%rdx)	# *_263, MEM[base: buflim_265, offset: 0B]
	jne	.L60	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	288(%rax), %rsi	#, tmp765
	cmpq	%rsi, %rdx	# tmp765, cp
	jbe	.L61	#,
	.p2align 4,,10
	.p2align 3
.L62:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_269, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rsi, %rdx	# tmp767, cp
	jne	.L62	#,
.L61:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:106:   hexvalue ((ctx->uc_mcontext.gregs[REG_CSGSFS] >> 16) & 0xffff, regs[19], 4);
	movq	%rcx, %rsi	# _258, tmp768
	leaq	308(%rax), %rdx	#, cp
	sarq	$16, %rsi	#, tmp768
	movzwl	%si, %esi	# tmp768, value
	.p2align 4,,10
	.p2align 3
.L63:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rsi, %rdi	# value, tmp769
	shrq	$4, %rsi	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %edi	#, tmp769
	testq	%rsi, %rsi	# value
	movzbl	(%rdi,%r14), %edi	# *_275, *_275
	movb	%dil, (%rdx)	# *_275, MEM[base: buflim_277, offset: 0B]
	jne	.L63	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	304(%rax), %rsi	#, tmp773
	cmpq	%rsi, %rdx	# tmp773, cp
	jbe	.L64	#,
	.p2align 4,,10
	.p2align 3
.L65:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_281, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rsi, %rdx	# tmp775, cp
	jne	.L65	#,
.L64:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:107:   hexvalue ((ctx->uc_mcontext.gregs[REG_CSGSFS] >> 32) & 0xffff, regs[20], 4);
	sarq	$32, %rcx	#, tmp776
	leaq	324(%rax), %rdx	#, cp
	movzwl	%cx, %ecx	# tmp776, value
	.p2align 4,,10
	.p2align 3
.L66:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp777
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp777
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_287, *_287
	movb	%sil, (%rdx)	# *_287, MEM[base: buflim_289, offset: 0B]
	jne	.L66	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	320(%rax), %rcx	#, tmp781
	cmpq	%rcx, %rdx	# tmp781, cp
	jbe	.L67	#,
	.p2align 4,,10
	.p2align 3
.L68:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_293, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp783, cp
	jne	.L68	#,
.L67:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:109:   hexvalue (ctx->uc_mcontext.gregs[REG_TRAPNO], regs[21], 8);
	movq	200(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	344(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L69:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp784
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp784
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_298, *_298
	movb	%sil, (%rdx)	# *_298, MEM[base: buflim_300, offset: 0B]
	jne	.L69	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	336(%rax), %rcx	#, tmp788
	cmpq	%rcx, %rdx	# tmp788, cp
	jbe	.L70	#,
	.p2align 4,,10
	.p2align 3
.L71:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_304, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp790, cp
	jne	.L71	#,
.L70:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:110:   hexvalue (ctx->uc_mcontext.gregs[REG_ERR], regs[22], 8);
	movq	192(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	360(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L72:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp791
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp791
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_309, *_309
	movb	%sil, (%rdx)	# *_309, MEM[base: buflim_311, offset: 0B]
	jne	.L72	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	352(%rax), %rcx	#, tmp795
	cmpq	%rcx, %rdx	# tmp795, cp
	jbe	.L73	#,
	.p2align 4,,10
	.p2align 3
.L74:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_315, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp797, cp
	jne	.L74	#,
.L73:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:111:   hexvalue (ctx->uc_mcontext.gregs[REG_OLDMASK], regs[23], 8);
	movq	208(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	376(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L75:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp798
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp798
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_320, *_320
	movb	%sil, (%rdx)	# *_320, MEM[base: buflim_322, offset: 0B]
	jne	.L75	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	368(%rax), %rcx	#, tmp802
	cmpq	%rcx, %rdx	# tmp802, cp
	jbe	.L76	#,
	.p2align 4,,10
	.p2align 3
.L77:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_326, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp804, cp
	jne	.L77	#,
.L76:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:112:   hexvalue (ctx->uc_mcontext.gregs[REG_CR2], regs[24], 8);
	movq	216(%r15), %rcx	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.gregs, value
	leaq	392(%rax), %rdx	#, cp
	.p2align 4,,10
	.p2align 3
.L78:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rcx, %rsi	# value, tmp805
	shrq	$4, %rcx	#, value
	subq	$1, %rdx	#, cp
	andl	$15, %esi	#, tmp805
	testq	%rcx, %rcx	# value
	movzbl	(%rsi,%r14), %esi	# *_331, *_331
	movb	%sil, (%rdx)	# *_331, MEM[base: buflim_333, offset: 0B]
	jne	.L78	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	384(%rax), %rcx	#, tmp809
	cmpq	%rcx, %rdx	# tmp809, cp
	jbe	.L79	#,
	.p2align 4,,10
	.p2align 3
.L80:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rdx	#, cp
	movb	$48, (%rdx)	#, MEM[base: cp_337, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rcx, %rdx	# tmp811, cp
	jne	.L80	#,
.L79:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:115:   ADD_STRING ("Register dump:\n\n RAX: ");
	leaq	.LC3(%rip), %rdi	#, tmp1295
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:118:   ADD_MEM (regs[1], 16);
	leaq	16(%rax), %rdx	#, tmp816
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:116:   ADD_MEM (regs[0], 16);
	movq	%rax, -2384(%rbp)	# tmp1265, iov[1].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:115:   ADD_STRING ("Register dump:\n\n RAX: ");
	movq	$22, -2392(%rbp)	#, iov[0].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:116:   ADD_MEM (regs[0], 16);
	movq	$16, -2376(%rbp)	#, iov[1].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:115:   ADD_STRING ("Register dump:\n\n RAX: ");
	movq	%rdi, -2400(%rbp)	# tmp1295, iov[0].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:117:   ADD_STRING ("   RBX: ");
	leaq	.LC4(%rip), %rdi	#, tmp1296
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:118:   ADD_MEM (regs[1], 16);
	movq	%rdx, -2352(%rbp)	# tmp816, iov[3].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:120:   ADD_MEM (regs[2], 16);
	leaq	32(%rax), %rdx	#, tmp819
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:117:   ADD_STRING ("   RBX: ");
	movq	$8, -2360(%rbp)	#, iov[2].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:118:   ADD_MEM (regs[1], 16);
	movq	$16, -2344(%rbp)	#, iov[3].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:117:   ADD_STRING ("   RBX: ");
	movq	%rdi, -2368(%rbp)	# tmp1296, iov[2].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:119:   ADD_STRING ("   RCX: ");
	leaq	.LC5(%rip), %rdi	#, tmp1297
	movq	$8, -2328(%rbp)	#, iov[4].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:120:   ADD_MEM (regs[2], 16);
	movq	%rdx, -2320(%rbp)	# tmp819, iov[5].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:122:   ADD_MEM (regs[3], 16);
	leaq	48(%rax), %rdx	#, tmp822
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:120:   ADD_MEM (regs[2], 16);
	movq	$16, -2312(%rbp)	#, iov[5].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:119:   ADD_STRING ("   RCX: ");
	movq	%rdi, -2336(%rbp)	# tmp1297, iov[4].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:121:   ADD_STRING ("\n RDX: ");
	leaq	.LC6(%rip), %rdi	#, tmp1298
	movq	$7, -2296(%rbp)	#, iov[6].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:122:   ADD_MEM (regs[3], 16);
	movq	%rdx, -2288(%rbp)	# tmp822, iov[7].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:124:   ADD_MEM (regs[4], 16);
	leaq	64(%rax), %rdx	#, tmp825
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:122:   ADD_MEM (regs[3], 16);
	movq	$16, -2280(%rbp)	#, iov[7].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:121:   ADD_STRING ("\n RDX: ");
	movq	%rdi, -2304(%rbp)	# tmp1298, iov[6].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:123:   ADD_STRING ("   RSI: ");
	leaq	.LC7(%rip), %rdi	#, tmp1299
	movq	$8, -2264(%rbp)	#, iov[8].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:124:   ADD_MEM (regs[4], 16);
	movq	%rdx, -2256(%rbp)	# tmp825, iov[9].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:126:   ADD_MEM (regs[5], 16);
	leaq	80(%rax), %rdx	#, tmp828
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:124:   ADD_MEM (regs[4], 16);
	movq	$16, -2248(%rbp)	#, iov[9].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:123:   ADD_STRING ("   RSI: ");
	movq	%rdi, -2272(%rbp)	# tmp1299, iov[8].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:125:   ADD_STRING ("   RDI: ");
	leaq	.LC8(%rip), %rdi	#, tmp1300
	movq	$8, -2232(%rbp)	#, iov[10].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:126:   ADD_MEM (regs[5], 16);
	movq	%rdx, -2224(%rbp)	# tmp828, iov[11].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:128:   ADD_MEM (regs[6], 16);
	leaq	96(%rax), %rdx	#, tmp831
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:126:   ADD_MEM (regs[5], 16);
	movq	$16, -2216(%rbp)	#, iov[11].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:125:   ADD_STRING ("   RDI: ");
	movq	%rdi, -2240(%rbp)	# tmp1300, iov[10].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:127:   ADD_STRING ("\n RBP: ");
	leaq	.LC9(%rip), %rdi	#, tmp1301
	movq	$7, -2200(%rbp)	#, iov[12].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:128:   ADD_MEM (regs[6], 16);
	movq	%rdx, -2192(%rbp)	# tmp831, iov[13].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:130:   ADD_MEM (regs[7], 16);
	leaq	112(%rax), %rdx	#, tmp834
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:128:   ADD_MEM (regs[6], 16);
	movq	$16, -2184(%rbp)	#, iov[13].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:127:   ADD_STRING ("\n RBP: ");
	movq	%rdi, -2208(%rbp)	# tmp1301, iov[12].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:129:   ADD_STRING ("   R8 : ");
	leaq	.LC10(%rip), %rdi	#, tmp1302
	movq	$8, -2168(%rbp)	#, iov[14].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:130:   ADD_MEM (regs[7], 16);
	movq	%rdx, -2160(%rbp)	# tmp834, iov[15].iov_base
	movq	$16, -2152(%rbp)	#, iov[15].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:132:   ADD_MEM (regs[8], 16);
	leaq	128(%rax), %rdx	#, tmp837
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:129:   ADD_STRING ("   R8 : ");
	movq	%rdi, -2176(%rbp)	# tmp1302, iov[14].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:131:   ADD_STRING ("   R9 : ");
	leaq	.LC11(%rip), %rdi	#, tmp1303
	movq	%rdi, -2144(%rbp)	# tmp1303, iov[16].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:133:   ADD_STRING ("\n R10: ");
	leaq	.LC12(%rip), %rdi	#, tmp1304
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:132:   ADD_MEM (regs[8], 16);
	movq	%rdx, -2128(%rbp)	# tmp837, iov[17].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:134:   ADD_MEM (regs[9], 16);
	leaq	144(%rax), %rdx	#, tmp840
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:131:   ADD_STRING ("   R9 : ");
	movq	$8, -2136(%rbp)	#, iov[16].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:132:   ADD_MEM (regs[8], 16);
	movq	$16, -2120(%rbp)	#, iov[17].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:133:   ADD_STRING ("\n R10: ");
	movq	%rdi, -2112(%rbp)	# tmp1304, iov[18].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:135:   ADD_STRING ("   R11: ");
	leaq	.LC13(%rip), %rdi	#, tmp1305
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:133:   ADD_STRING ("\n R10: ");
	movq	$7, -2104(%rbp)	#, iov[18].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:134:   ADD_MEM (regs[9], 16);
	movq	%rdx, -2096(%rbp)	# tmp840, iov[19].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:136:   ADD_MEM (regs[10], 16);
	leaq	160(%rax), %rdx	#, tmp843
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:134:   ADD_MEM (regs[9], 16);
	movq	$16, -2088(%rbp)	#, iov[19].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:135:   ADD_STRING ("   R11: ");
	movq	%rdi, -2080(%rbp)	# tmp1305, iov[20].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:137:   ADD_STRING ("   R12: ");
	leaq	.LC14(%rip), %rdi	#, tmp1306
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:135:   ADD_STRING ("   R11: ");
	movq	$8, -2072(%rbp)	#, iov[20].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:136:   ADD_MEM (regs[10], 16);
	movq	%rdx, -2064(%rbp)	# tmp843, iov[21].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:138:   ADD_MEM (regs[11], 16);
	leaq	176(%rax), %rdx	#, tmp846
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:136:   ADD_MEM (regs[10], 16);
	movq	$16, -2056(%rbp)	#, iov[21].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:137:   ADD_STRING ("   R12: ");
	movq	%rdi, -2048(%rbp)	# tmp1306, iov[22].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:139:   ADD_STRING ("\n R13: ");
	leaq	.LC15(%rip), %rdi	#, tmp1307
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:137:   ADD_STRING ("   R12: ");
	movq	$8, -2040(%rbp)	#, iov[22].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:138:   ADD_MEM (regs[11], 16);
	movq	%rdx, -2032(%rbp)	# tmp846, iov[23].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:140:   ADD_MEM (regs[12], 16);
	leaq	192(%rax), %rdx	#, tmp849
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:138:   ADD_MEM (regs[11], 16);
	movq	$16, -2024(%rbp)	#, iov[23].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:139:   ADD_STRING ("\n R13: ");
	movq	%rdi, -2016(%rbp)	# tmp1307, iov[24].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:141:   ADD_STRING ("   R14: ");
	leaq	.LC16(%rip), %rdi	#, tmp1308
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:139:   ADD_STRING ("\n R13: ");
	movq	$7, -2008(%rbp)	#, iov[24].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:140:   ADD_MEM (regs[12], 16);
	movq	%rdx, -2000(%rbp)	# tmp849, iov[25].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:142:   ADD_MEM (regs[13], 16);
	leaq	208(%rax), %rdx	#, tmp852
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:140:   ADD_MEM (regs[12], 16);
	movq	$16, -1992(%rbp)	#, iov[25].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:141:   ADD_STRING ("   R14: ");
	movq	%rdi, -1984(%rbp)	# tmp1308, iov[26].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:143:   ADD_STRING ("   R15: ");
	leaq	.LC17(%rip), %rdi	#, tmp1309
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:141:   ADD_STRING ("   R14: ");
	movq	$8, -1976(%rbp)	#, iov[26].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:142:   ADD_MEM (regs[13], 16);
	movq	%rdx, -1968(%rbp)	# tmp852, iov[27].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:144:   ADD_MEM (regs[14], 16);
	leaq	224(%rax), %rdx	#, tmp855
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:142:   ADD_MEM (regs[13], 16);
	movq	$16, -1960(%rbp)	#, iov[27].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:143:   ADD_STRING ("   R15: ");
	movq	%rdi, -1952(%rbp)	# tmp1309, iov[28].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:145:   ADD_STRING ("\n RSP: ");
	leaq	.LC18(%rip), %rdi	#, tmp1310
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:143:   ADD_STRING ("   R15: ");
	movq	$8, -1944(%rbp)	#, iov[28].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:144:   ADD_MEM (regs[14], 16);
	movq	%rdx, -1936(%rbp)	# tmp855, iov[29].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:146:   ADD_MEM (regs[15], 16);
	leaq	240(%rax), %rdx	#, tmp858
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:144:   ADD_MEM (regs[14], 16);
	movq	$16, -1928(%rbp)	#, iov[29].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:145:   ADD_STRING ("\n RSP: ");
	movq	%rdi, -1920(%rbp)	# tmp1310, iov[30].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:147:   ADD_STRING ("\n\n RIP: ");
	leaq	.LC19(%rip), %rdi	#, tmp1311
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:145:   ADD_STRING ("\n RSP: ");
	movq	$7, -1912(%rbp)	#, iov[30].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:146:   ADD_MEM (regs[15], 16);
	movq	%rdx, -1904(%rbp)	# tmp858, iov[31].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:148:   ADD_MEM (regs[16], 16);
	leaq	256(%rax), %rdx	#, tmp861
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:146:   ADD_MEM (regs[15], 16);
	movq	$16, -1896(%rbp)	#, iov[31].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:147:   ADD_STRING ("\n\n RIP: ");
	movq	%rdi, -1888(%rbp)	# tmp1311, iov[32].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:149:   ADD_STRING ("   EFLAGS: ");
	leaq	.LC20(%rip), %rdi	#, tmp1312
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:147:   ADD_STRING ("\n\n RIP: ");
	movq	$8, -1880(%rbp)	#, iov[32].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:148:   ADD_MEM (regs[16], 16);
	movq	%rdx, -1872(%rbp)	# tmp861, iov[33].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:150:   ADD_MEM (regs[17], 8);
	leaq	272(%rax), %rdx	#, tmp864
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:148:   ADD_MEM (regs[16], 16);
	movq	$16, -1864(%rbp)	#, iov[33].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:149:   ADD_STRING ("   EFLAGS: ");
	movq	%rdi, -1856(%rbp)	# tmp1312, iov[34].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:151:   ADD_STRING ("\n\n CS: ");
	leaq	.LC21(%rip), %rdi	#, tmp1313
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:149:   ADD_STRING ("   EFLAGS: ");
	movq	$11, -1848(%rbp)	#, iov[34].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:150:   ADD_MEM (regs[17], 8);
	movq	%rdx, -1840(%rbp)	# tmp864, iov[35].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:152:   ADD_MEM (regs[18], 4);
	leaq	288(%rax), %rdx	#, tmp867
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:150:   ADD_MEM (regs[17], 8);
	movq	$8, -1832(%rbp)	#, iov[35].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:151:   ADD_STRING ("\n\n CS: ");
	movq	%rdi, -1824(%rbp)	# tmp1313, iov[36].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:153:   ADD_STRING ("   FS: ");
	leaq	.LC22(%rip), %rdi	#, tmp1314
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:151:   ADD_STRING ("\n\n CS: ");
	movq	$7, -1816(%rbp)	#, iov[36].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:152:   ADD_MEM (regs[18], 4);
	movq	%rdx, -1808(%rbp)	# tmp867, iov[37].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:154:   ADD_MEM (regs[19], 4);
	leaq	304(%rax), %rdx	#, tmp870
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:152:   ADD_MEM (regs[18], 4);
	movq	$4, -1800(%rbp)	#, iov[37].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:153:   ADD_STRING ("   FS: ");
	movq	%rdi, -1792(%rbp)	# tmp1314, iov[38].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:155:   ADD_STRING ("   GS: ");
	leaq	.LC23(%rip), %rdi	#, tmp1315
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:153:   ADD_STRING ("   FS: ");
	movq	$7, -1784(%rbp)	#, iov[38].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:154:   ADD_MEM (regs[19], 4);
	movq	%rdx, -1776(%rbp)	# tmp870, iov[39].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:156:   ADD_MEM (regs[20], 4);
	leaq	320(%rax), %rdx	#, tmp873
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:154:   ADD_MEM (regs[19], 4);
	movq	$4, -1768(%rbp)	#, iov[39].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:155:   ADD_STRING ("   GS: ");
	movq	%rdi, -1760(%rbp)	# tmp1315, iov[40].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:161:   ADD_STRING ("\n\n Trap: ");
	leaq	.LC24(%rip), %rdi	#, tmp1316
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:155:   ADD_STRING ("   GS: ");
	movq	$7, -1752(%rbp)	#, iov[40].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:156:   ADD_MEM (regs[20], 4);
	movq	%rdx, -1744(%rbp)	# tmp873, iov[41].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:162:   ADD_MEM (regs[21], 8);
	leaq	336(%rax), %rdx	#, tmp876
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:156:   ADD_MEM (regs[20], 4);
	movq	$4, -1736(%rbp)	#, iov[41].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:161:   ADD_STRING ("\n\n Trap: ");
	movq	%rdi, -1728(%rbp)	# tmp1316, iov[42].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:163:   ADD_STRING ("   Error: ");
	leaq	.LC25(%rip), %rdi	#, tmp1317
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:161:   ADD_STRING ("\n\n Trap: ");
	movq	$9, -1720(%rbp)	#, iov[42].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:162:   ADD_MEM (regs[21], 8);
	movq	%rdx, -1712(%rbp)	# tmp876, iov[43].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:164:   ADD_MEM (regs[22], 8);
	leaq	352(%rax), %rdx	#, tmp879
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:162:   ADD_MEM (regs[21], 8);
	movq	$8, -1704(%rbp)	#, iov[43].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:163:   ADD_STRING ("   Error: ");
	movq	%rdi, -1696(%rbp)	# tmp1317, iov[44].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:165:   ADD_STRING ("   OldMask: ");
	leaq	.LC26(%rip), %rdi	#, tmp1318
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:163:   ADD_STRING ("   Error: ");
	movq	$10, -1688(%rbp)	#, iov[44].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:164:   ADD_MEM (regs[22], 8);
	movq	%rdx, -1680(%rbp)	# tmp879, iov[45].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:166:   ADD_MEM (regs[23], 8);
	leaq	368(%rax), %rdx	#, tmp882
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:168:   ADD_MEM (regs[24], 8);
	addq	$384, %rax	#, tmp885
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:165:   ADD_STRING ("   OldMask: ");
	movq	%rdi, -1664(%rbp)	# tmp1318, iov[46].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:167:   ADD_STRING ("   CR2: ");
	leaq	.LC27(%rip), %rdi	#, tmp1319
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:164:   ADD_MEM (regs[22], 8);
	movq	$8, -1672(%rbp)	#, iov[45].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:165:   ADD_STRING ("   OldMask: ");
	movq	$12, -1656(%rbp)	#, iov[46].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:166:   ADD_MEM (regs[23], 8);
	movq	%rdx, -1648(%rbp)	# tmp882, iov[47].iov_base
	movq	$8, -1640(%rbp)	#, iov[47].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:167:   ADD_STRING ("   CR2: ");
	movq	%rdi, -1632(%rbp)	# tmp1319, iov[48].iov_base
	movq	$8, -1624(%rbp)	#, iov[48].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:168:   ADD_MEM (regs[24], 8);
	movq	%rax, -1616(%rbp)	# tmp885, iov[49].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:170:   if (ctx->uc_mcontext.fpregs != NULL)
	movq	224(%r15), %rsi	# MEM[(struct ucontext_t *)ctx_34(D)].uc_mcontext.fpregs, _338
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:168:   ADD_MEM (regs[24], 8);
	movq	$8, -1608(%rbp)	#, iov[49].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:170:   if (ctx->uc_mcontext.fpregs != NULL)
	testq	%rsi, %rsi	# _338
	je	.L186	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:174:       hexvalue (ctx->uc_mcontext.fpregs->cwd, fpregs[0], 8);
	leaq	-3552(%rbp), %r8	#, tmp1263
	movzwl	(%rsi), %edx	# _338->cwd, value
	leaq	8(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L82:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp886
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp886
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_343, *_343
	movb	%cl, (%rax)	# *_343, MEM[base: buflim_345, offset: 0B]
	jne	.L82	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%r8, %rax	# tmp1263, cp
	jbe	.L83	#,
	.p2align 4,,10
	.p2align 3
.L84:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_349, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%r8, %rax	# tmp1263, cp
	jne	.L84	#,
.L83:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:175:       hexvalue (ctx->uc_mcontext.fpregs->swd, fpregs[1], 8);
	movzwl	2(%rsi), %edx	# _338->swd, value
	leaq	16(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L85:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp891
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp891
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_354, *_354
	movb	%cl, (%rax)	# *_354, MEM[base: buflim_356, offset: 0B]
	jne	.L85	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	8(%r8), %rdx	#, tmp895
	cmpq	%rdx, %rax	# tmp895, cp
	jbe	.L86	#,
	.p2align 4,,10
	.p2align 3
.L87:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_360, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp897, cp
	jne	.L87	#,
.L86:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:176:       hexvalue (ctx->uc_mcontext.fpregs->ftw, fpregs[2], 8);
	movzwl	4(%rsi), %edx	# _338->ftw, value
	leaq	24(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L88:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp898
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp898
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_365, *_365
	movb	%cl, (%rax)	# *_365, MEM[base: buflim_367, offset: 0B]
	jne	.L88	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	16(%r8), %rdx	#, tmp902
	cmpq	%rdx, %rax	# tmp902, cp
	jbe	.L89	#,
	.p2align 4,,10
	.p2align 3
.L90:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_371, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp904, cp
	jne	.L90	#,
.L89:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:177:       hexvalue (ctx->uc_mcontext.fpregs->rip, fpregs[3], 8);
	movq	8(%rsi), %rdx	# _338->rip, value
	leaq	32(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L91:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp905
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp905
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_375, *_375
	movb	%cl, (%rax)	# *_375, MEM[base: buflim_377, offset: 0B]
	jne	.L91	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	24(%r8), %rdx	#, tmp909
	cmpq	%rdx, %rax	# tmp909, cp
	jbe	.L92	#,
	.p2align 4,,10
	.p2align 3
.L93:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_381, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp911, cp
	jne	.L93	#,
.L92:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:178:       hexvalue (ctx->uc_mcontext.fpregs->rdp, fpregs[4], 8);
	movq	16(%rsi), %rdx	# _338->rdp, value
	leaq	40(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L94:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp912
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp912
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_385, *_385
	movb	%cl, (%rax)	# *_385, MEM[base: buflim_387, offset: 0B]
	jne	.L94	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	32(%r8), %rdx	#, tmp916
	cmpq	%rdx, %rax	# tmp916, cp
	jbe	.L95	#,
	.p2align 4,,10
	.p2align 3
.L96:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_391, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp918, cp
	jne	.L96	#,
.L95:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:180:       ADD_STRING ("\n\n FPUCW: ");
	leaq	.LC28(%rip), %rax	#, tmp1325
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:192:       hexvalue (ctx->uc_mcontext.fpregs->_st[0].exponent, fpregs[5], 8);
	movzwl	40(%rsi), %edx	# _338->_st[0].exponent, value
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:180:       ADD_STRING ("\n\n FPUCW: ");
	movq	$10, -1592(%rbp)	#, iov[50].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:181:       ADD_MEM (fpregs[0], 8);
	movq	%r8, -1584(%rbp)	# tmp1263, iov[51].iov_base
	movq	$8, -1576(%rbp)	#, iov[51].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:180:       ADD_STRING ("\n\n FPUCW: ");
	movq	%rax, -1600(%rbp)	# tmp1325, iov[50].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:182:       ADD_STRING ("   FPUSW: ");
	leaq	.LC29(%rip), %rax	#, tmp1326
	movq	$10, -1560(%rbp)	#, iov[52].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:183:       ADD_MEM (fpregs[1], 8);
	movq	$8, -1544(%rbp)	#, iov[53].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:184:       ADD_STRING ("   TAG: ");
	movq	$8, -1528(%rbp)	#, iov[54].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:182:       ADD_STRING ("   FPUSW: ");
	movq	%rax, -1568(%rbp)	# tmp1326, iov[52].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:183:       ADD_MEM (fpregs[1], 8);
	leaq	8(%r8), %rax	#, tmp923
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:185:       ADD_MEM (fpregs[2], 8);
	movq	$8, -1512(%rbp)	#, iov[55].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:186:       ADD_STRING ("\n RIP: ");
	movq	$7, -1496(%rbp)	#, iov[56].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:187:       ADD_MEM (fpregs[3], 8);
	movq	$8, -1480(%rbp)	#, iov[57].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:183:       ADD_MEM (fpregs[1], 8);
	movq	%rax, -1552(%rbp)	# tmp923, iov[53].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:184:       ADD_STRING ("   TAG: ");
	leaq	.LC30(%rip), %rax	#, tmp1327
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:188:       ADD_STRING ("   RDP: ");
	movq	$8, -1464(%rbp)	#, iov[58].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:189:       ADD_MEM (fpregs[4], 8);
	movq	$8, -1448(%rbp)	#, iov[59].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:184:       ADD_STRING ("   TAG: ");
	movq	%rax, -1536(%rbp)	# tmp1327, iov[54].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:185:       ADD_MEM (fpregs[2], 8);
	leaq	16(%r8), %rax	#, tmp926
	movq	%rax, -1520(%rbp)	# tmp926, iov[55].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:186:       ADD_STRING ("\n RIP: ");
	leaq	.LC31(%rip), %rax	#, tmp1328
	movq	%rax, -1504(%rbp)	# tmp1328, iov[56].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:187:       ADD_MEM (fpregs[3], 8);
	leaq	24(%r8), %rax	#, tmp929
	movq	%rax, -1488(%rbp)	# tmp929, iov[57].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:188:       ADD_STRING ("   RDP: ");
	leaq	.LC32(%rip), %rax	#, tmp1329
	movq	%rax, -1472(%rbp)	# tmp1329, iov[58].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:189:       ADD_MEM (fpregs[4], 8);
	leaq	32(%r8), %rax	#, tmp932
	movq	%rax, -1456(%rbp)	# tmp932, iov[59].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:192:       hexvalue (ctx->uc_mcontext.fpregs->_st[0].exponent, fpregs[5], 8);
	leaq	48(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L97:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp933
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp933
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_396, *_396
	movb	%cl, (%rax)	# *_396, MEM[base: buflim_398, offset: 0B]
	jne	.L97	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	40(%r8), %rdx	#, tmp937
	cmpq	%rdx, %rax	# tmp937, cp
	jbe	.L98	#,
	.p2align 4,,10
	.p2align 3
.L99:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_402, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp939, cp
	jne	.L99	#,
.L98:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:193:       hexvalue (ctx->uc_mcontext.fpregs->_st[0].significand[3] << 16
	movslq	36(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 36B], value
	leaq	56(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L100:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp941
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp941
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_412, *_412
	movb	%cl, (%rax)	# *_412, MEM[base: buflim_414, offset: 0B]
	jne	.L100	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	48(%r8), %rdx	#, tmp945
	cmpq	%rdx, %rax	# tmp945, cp
	jbe	.L101	#,
	.p2align 4,,10
	.p2align 3
.L102:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_418, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp947, cp
	jne	.L102	#,
.L101:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:196:       hexvalue (ctx->uc_mcontext.fpregs->_st[0].significand[1] << 16
	movslq	32(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 32B], value
	leaq	64(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L103:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp949
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp949
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_428, *_428
	movb	%cl, (%rax)	# *_428, MEM[base: buflim_430, offset: 0B]
	jne	.L103	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	56(%r8), %rdx	#, tmp953
	cmpq	%rdx, %rax	# tmp953, cp
	jbe	.L104	#,
	.p2align 4,,10
	.p2align 3
.L105:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_434, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp955, cp
	jne	.L105	#,
.L104:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:199:       hexvalue (ctx->uc_mcontext.fpregs->_st[1].exponent, fpregs[8], 8);
	movzwl	56(%rsi), %edx	# _338->_st[1].exponent, value
	leaq	72(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L106:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp956
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp956
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_439, *_439
	movb	%cl, (%rax)	# *_439, MEM[base: buflim_441, offset: 0B]
	jne	.L106	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	64(%r8), %rdx	#, tmp960
	cmpq	%rdx, %rax	# tmp960, cp
	jbe	.L107	#,
	.p2align 4,,10
	.p2align 3
.L108:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_445, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp962, cp
	jne	.L108	#,
.L107:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:200:       hexvalue (ctx->uc_mcontext.fpregs->_st[1].significand[3] << 16
	movslq	52(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 52B], value
	leaq	80(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L109:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp964
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp964
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_455, *_455
	movb	%cl, (%rax)	# *_455, MEM[base: buflim_457, offset: 0B]
	jne	.L109	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	72(%r8), %rdx	#, tmp968
	cmpq	%rdx, %rax	# tmp968, cp
	jbe	.L110	#,
	.p2align 4,,10
	.p2align 3
.L111:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_461, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp970, cp
	jne	.L111	#,
.L110:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:203:       hexvalue (ctx->uc_mcontext.fpregs->_st[1].significand[1] << 16
	movslq	48(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 48B], value
	leaq	88(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L112:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp972
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp972
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_471, *_471
	movb	%cl, (%rax)	# *_471, MEM[base: buflim_473, offset: 0B]
	jne	.L112	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	80(%r8), %rdx	#, tmp976
	cmpq	%rdx, %rax	# tmp976, cp
	jbe	.L113	#,
	.p2align 4,,10
	.p2align 3
.L114:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_477, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp978, cp
	jne	.L114	#,
.L113:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:206:       hexvalue (ctx->uc_mcontext.fpregs->_st[2].exponent, fpregs[11], 8);
	movzwl	72(%rsi), %edx	# _338->_st[2].exponent, value
	leaq	96(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L115:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp979
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp979
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_482, *_482
	movb	%cl, (%rax)	# *_482, MEM[base: buflim_484, offset: 0B]
	jne	.L115	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	88(%r8), %rdx	#, tmp983
	cmpq	%rdx, %rax	# tmp983, cp
	jbe	.L116	#,
	.p2align 4,,10
	.p2align 3
.L117:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_488, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp985, cp
	jne	.L117	#,
.L116:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:207:       hexvalue (ctx->uc_mcontext.fpregs->_st[2].significand[3] << 16
	movslq	68(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 68B], value
	leaq	104(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L118:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp987
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp987
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_498, *_498
	movb	%cl, (%rax)	# *_498, MEM[base: buflim_500, offset: 0B]
	jne	.L118	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	96(%r8), %rdx	#, tmp991
	cmpq	%rdx, %rax	# tmp991, cp
	jbe	.L119	#,
	.p2align 4,,10
	.p2align 3
.L120:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_504, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp993, cp
	jne	.L120	#,
.L119:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:210:       hexvalue (ctx->uc_mcontext.fpregs->_st[2].significand[1] << 16
	movslq	64(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 64B], value
	leaq	112(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L121:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp995
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp995
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_514, *_514
	movb	%cl, (%rax)	# *_514, MEM[base: buflim_516, offset: 0B]
	jne	.L121	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	104(%r8), %rdx	#, tmp999
	cmpq	%rdx, %rax	# tmp999, cp
	jbe	.L122	#,
	.p2align 4,,10
	.p2align 3
.L123:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_520, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1001, cp
	jne	.L123	#,
.L122:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:213:       hexvalue (ctx->uc_mcontext.fpregs->_st[3].exponent, fpregs[14], 8);
	movzwl	88(%rsi), %edx	# _338->_st[3].exponent, value
	leaq	120(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L124:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1002
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1002
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_525, *_525
	movb	%cl, (%rax)	# *_525, MEM[base: buflim_527, offset: 0B]
	jne	.L124	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	112(%r8), %rdx	#, tmp1006
	cmpq	%rdx, %rax	# tmp1006, cp
	jbe	.L125	#,
	.p2align 4,,10
	.p2align 3
.L126:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_531, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1008, cp
	jne	.L126	#,
.L125:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:214:       hexvalue (ctx->uc_mcontext.fpregs->_st[3].significand[3] << 16
	movslq	84(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 84B], value
	leaq	128(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L127:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1010
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1010
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_541, *_541
	movb	%cl, (%rax)	# *_541, MEM[base: buflim_543, offset: 0B]
	jne	.L127	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	120(%r8), %rdx	#, tmp1014
	cmpq	%rdx, %rax	# tmp1014, cp
	jbe	.L128	#,
	.p2align 4,,10
	.p2align 3
.L129:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_547, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1016, cp
	jne	.L129	#,
.L128:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:217:       hexvalue (ctx->uc_mcontext.fpregs->_st[3].significand[1] << 16
	movslq	80(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 80B], value
	leaq	136(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L130:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1018
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1018
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_557, *_557
	movb	%cl, (%rax)	# *_557, MEM[base: buflim_559, offset: 0B]
	jne	.L130	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	128(%r8), %rdx	#, tmp1022
	cmpq	%rdx, %rax	# tmp1022, cp
	jbe	.L131	#,
	.p2align 4,,10
	.p2align 3
.L132:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_563, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1024, cp
	jne	.L132	#,
.L131:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:220:       hexvalue (ctx->uc_mcontext.fpregs->_st[4].exponent, fpregs[17], 8);
	movzwl	104(%rsi), %edx	# _338->_st[4].exponent, value
	leaq	144(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L133:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1025
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1025
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_568, *_568
	movb	%cl, (%rax)	# *_568, MEM[base: buflim_570, offset: 0B]
	jne	.L133	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	136(%r8), %rdx	#, tmp1029
	cmpq	%rdx, %rax	# tmp1029, cp
	jbe	.L134	#,
	.p2align 4,,10
	.p2align 3
.L135:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_574, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1031, cp
	jne	.L135	#,
.L134:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:221:       hexvalue (ctx->uc_mcontext.fpregs->_st[4].significand[3] << 16
	movslq	100(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 100B], value
	leaq	152(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L136:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1033
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1033
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_584, *_584
	movb	%cl, (%rax)	# *_584, MEM[base: buflim_586, offset: 0B]
	jne	.L136	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	144(%r8), %rdx	#, tmp1037
	cmpq	%rdx, %rax	# tmp1037, cp
	jbe	.L137	#,
	.p2align 4,,10
	.p2align 3
.L138:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_590, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1039, cp
	jne	.L138	#,
.L137:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:224:       hexvalue (ctx->uc_mcontext.fpregs->_st[4].significand[1] << 16
	movslq	96(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 96B], value
	leaq	160(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L139:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1041
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1041
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_600, *_600
	movb	%cl, (%rax)	# *_600, MEM[base: buflim_602, offset: 0B]
	jne	.L139	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	152(%r8), %rdx	#, tmp1045
	cmpq	%rdx, %rax	# tmp1045, cp
	jbe	.L140	#,
	.p2align 4,,10
	.p2align 3
.L141:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_606, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1047, cp
	jne	.L141	#,
.L140:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:227:       hexvalue (ctx->uc_mcontext.fpregs->_st[5].exponent, fpregs[20], 8);
	movzwl	120(%rsi), %edx	# _338->_st[5].exponent, value
	leaq	168(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L142:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1048
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1048
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_611, *_611
	movb	%cl, (%rax)	# *_611, MEM[base: buflim_613, offset: 0B]
	jne	.L142	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	160(%r8), %rdx	#, tmp1052
	cmpq	%rdx, %rax	# tmp1052, cp
	jbe	.L143	#,
	.p2align 4,,10
	.p2align 3
.L144:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_617, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1054, cp
	jne	.L144	#,
.L143:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:228:       hexvalue (ctx->uc_mcontext.fpregs->_st[5].significand[3] << 16
	movslq	116(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 116B], value
	leaq	176(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L145:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1056
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1056
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_627, *_627
	movb	%cl, (%rax)	# *_627, MEM[base: buflim_629, offset: 0B]
	jne	.L145	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	168(%r8), %rdx	#, tmp1060
	cmpq	%rdx, %rax	# tmp1060, cp
	jbe	.L146	#,
	.p2align 4,,10
	.p2align 3
.L147:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_633, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1062, cp
	jne	.L147	#,
.L146:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:231:       hexvalue (ctx->uc_mcontext.fpregs->_st[5].significand[1] << 16
	movslq	112(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 112B], value
	leaq	184(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L148:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1064
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1064
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_643, *_643
	movb	%cl, (%rax)	# *_643, MEM[base: buflim_645, offset: 0B]
	jne	.L148	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	176(%r8), %rdx	#, tmp1068
	cmpq	%rdx, %rax	# tmp1068, cp
	jbe	.L149	#,
	.p2align 4,,10
	.p2align 3
.L150:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_649, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1070, cp
	jne	.L150	#,
.L149:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:234:       hexvalue (ctx->uc_mcontext.fpregs->_st[6].exponent, fpregs[23], 8);
	movzwl	136(%rsi), %edx	# _338->_st[6].exponent, value
	leaq	192(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L151:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1071
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1071
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_654, *_654
	movb	%cl, (%rax)	# *_654, MEM[base: buflim_656, offset: 0B]
	jne	.L151	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	184(%r8), %rdx	#, tmp1075
	cmpq	%rdx, %rax	# tmp1075, cp
	jbe	.L152	#,
	.p2align 4,,10
	.p2align 3
.L153:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_660, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1077, cp
	jne	.L153	#,
.L152:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:235:       hexvalue (ctx->uc_mcontext.fpregs->_st[6].significand[3] << 16
	movslq	132(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 132B], value
	leaq	200(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L154:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1079
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1079
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_670, *_670
	movb	%cl, (%rax)	# *_670, MEM[base: buflim_672, offset: 0B]
	jne	.L154	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	192(%r8), %rdx	#, tmp1083
	cmpq	%rdx, %rax	# tmp1083, cp
	jbe	.L155	#,
	.p2align 4,,10
	.p2align 3
.L156:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_676, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1085, cp
	jne	.L156	#,
.L155:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:238:       hexvalue (ctx->uc_mcontext.fpregs->_st[6].significand[1] << 16
	movslq	128(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 128B], value
	leaq	208(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L157:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1087
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1087
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_686, *_686
	movb	%cl, (%rax)	# *_686, MEM[base: buflim_688, offset: 0B]
	jne	.L157	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	200(%r8), %rdx	#, tmp1091
	cmpq	%rdx, %rax	# tmp1091, cp
	jbe	.L158	#,
	.p2align 4,,10
	.p2align 3
.L159:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_692, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1093, cp
	jne	.L159	#,
.L158:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:241:       hexvalue (ctx->uc_mcontext.fpregs->_st[7].exponent, fpregs[26], 8);
	movzwl	152(%rsi), %edx	# _338->_st[7].exponent, value
	leaq	216(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L160:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1094
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1094
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_697, *_697
	movb	%cl, (%rax)	# *_697, MEM[base: buflim_699, offset: 0B]
	jne	.L160	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	208(%r8), %rdx	#, tmp1098
	cmpq	%rdx, %rax	# tmp1098, cp
	jbe	.L161	#,
	.p2align 4,,10
	.p2align 3
.L162:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_703, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1100, cp
	jne	.L162	#,
.L161:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:242:       hexvalue (ctx->uc_mcontext.fpregs->_st[7].significand[3] << 16
	movslq	148(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 148B], value
	leaq	224(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L163:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1102
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1102
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_713, *_713
	movb	%cl, (%rax)	# *_713, MEM[base: buflim_715, offset: 0B]
	jne	.L163	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	216(%r8), %rdx	#, tmp1106
	cmpq	%rdx, %rax	# tmp1106, cp
	jbe	.L164	#,
	.p2align 4,,10
	.p2align 3
.L165:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_719, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1108, cp
	jne	.L165	#,
.L164:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:245:       hexvalue (ctx->uc_mcontext.fpregs->_st[7].significand[1] << 16
	movslq	144(%rsi), %rdx	# MEM[(short unsigned int *)_338 + 144B], value
	leaq	232(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L166:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1110
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1110
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_729, *_729
	movb	%cl, (%rax)	# *_729, MEM[base: buflim_731, offset: 0B]
	jne	.L166	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	224(%r8), %rdx	#, tmp1114
	cmpq	%rdx, %rax	# tmp1114, cp
	jbe	.L167	#,
	.p2align 4,,10
	.p2align 3
.L168:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_735, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1116, cp
	jne	.L168	#,
.L167:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:249:       hexvalue (ctx->uc_mcontext.fpregs->mxcsr, fpregs[29], 4);
	movl	24(%rsi), %edx	# _338->mxcsr, value
	leaq	236(%r8), %rax	#, cp
	.p2align 4,,10
	.p2align 3
.L169:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1117
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1117
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_740, *_740
	movb	%cl, (%rax)	# *_740, MEM[base: buflim_742, offset: 0B]
	jne	.L169	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	leaq	232(%r8), %rdx	#, tmp1121
	cmpq	%rdx, %rax	# tmp1121, cp
	jbe	.L170	#,
	.p2align 4,,10
	.p2align 3
.L171:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_746, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rdx, %rax	# tmp1123, cp
	jne	.L171	#,
.L170:
	leaq	-2912(%rbp), %r10	#, tmp1264
	addq	$160, %rsi	#, ivtmp.41
	leaq	512(%r10), %r11	#, _721
	movq	%r10, %r9	# tmp1264, ivtmp.43
	.p2align 4,,10
	.p2align 3
.L175:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:252: 	hexvalue (ctx->uc_mcontext.fpregs->_xmm[i].element[3] << 24
	movl	12(%rsi), %edx	# MEM[base: _708, offset: 12B], tmp1124
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:253: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[2] << 16
	movl	8(%rsi), %eax	# MEM[base: _708, offset: 8B], tmp1126
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:255: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[0], xmmregs[i],
	movq	%r9, %rdi	# ivtmp.43, _748
	addq	$32, %r9	#, ivtmp.43
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:253: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[2] << 16
	sall	$16, %eax	#, tmp1126
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:252: 	hexvalue (ctx->uc_mcontext.fpregs->_xmm[i].element[3] << 24
	sall	$24, %edx	#, tmp1124
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:253: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[2] << 16
	orl	%eax, %edx	# tmp1126, tmp1128
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:254: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[1] << 8
	movl	4(%rsi), %eax	# MEM[base: _708, offset: 4B], tmp1130
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:255: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[0], xmmregs[i],
	orl	(%rsi), %edx	# MEM[base: _708, offset: 0B], tmp1129
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:254: 		  | ctx->uc_mcontext.fpregs->_xmm[i].element[1] << 8
	sall	$8, %eax	#, tmp1130
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:252: 	hexvalue (ctx->uc_mcontext.fpregs->_xmm[i].element[3] << 24
	orl	%eax, %edx	# tmp1130, value
	movq	%r9, %rax	# ivtmp.43, cp
	.p2align 4,,10
	.p2align 3
.L172:
# ../sysdeps/generic/_itoa.h:77:       SPECIAL (16);
	movq	%rdx, %rcx	# value, tmp1133
	shrq	$4, %rdx	#, value
	subq	$1, %rax	#, cp
	andl	$15, %ecx	#, tmp1133
	testq	%rdx, %rdx	# value
	movzbl	(%rcx,%r14), %ecx	# *_763, *_763
	movb	%cl, (%rax)	# *_763, MEM[base: buflim_765, offset: 0B]
	jne	.L172	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rax, %rdi	# cp, _748
	jnb	.L173	#,
	.p2align 4,,10
	.p2align 3
.L174:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:63:     *--cp = '0';
	subq	$1, %rax	#, cp
	movb	$48, (%rax)	#, MEM[base: cp_769, offset: 0B]
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:62:   while (cp > buf)
	cmpq	%rax, %rdi	# cp, _748
	jne	.L174	#,
.L173:
	addq	$16, %rsi	#, ivtmp.41
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:251:       for (i = 0; i < 16; i++)
	cmpq	%r9, %r11	# ivtmp.43, _721
	jne	.L175	#,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:262:       ADD_MEM (fpregs[6], 8);
	leaq	48(%r8), %rdx	#, tmp1141
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:259:       ADD_STRING ("\n\n ST(0) ");
	leaq	.LC33(%rip), %rax	#, tmp1359
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:264:       ADD_STRING ("   ST(1) ");
	leaq	.LC35(%rip), %rdi	#, tmp1360
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:259:       ADD_STRING ("\n\n ST(0) ");
	movq	$9, -1432(%rbp)	#, iov[60].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:260:       ADD_MEM (fpregs[5], 4);
	movq	$4, -1416(%rbp)	#, iov[61].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:262:       ADD_MEM (fpregs[6], 8);
	movq	%rdx, -1392(%rbp)	# tmp1141, iov[63].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:263:       ADD_MEM (fpregs[7], 8);
	leaq	56(%r8), %rdx	#, tmp1143
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:259:       ADD_STRING ("\n\n ST(0) ");
	movq	%rax, -1440(%rbp)	# tmp1359, iov[60].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:264:       ADD_STRING ("   ST(1) ");
	movq	%rdi, -1360(%rbp)	# tmp1360, iov[65].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:260:       ADD_MEM (fpregs[5], 4);
	leaq	40(%r8), %rax	#, tmp1138
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:269:       ADD_STRING ("\n ST(2) ");
	leaq	.LC36(%rip), %rdi	#, tmp1361
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:263:       ADD_MEM (fpregs[7], 8);
	movq	%rdx, -1376(%rbp)	# tmp1143, iov[64].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:265:       ADD_MEM (fpregs[8], 4);
	leaq	64(%r8), %rdx	#, tmp1146
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:261:       ADD_STRING (" ");
	movq	$1, -1400(%rbp)	#, iov[62].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:260:       ADD_MEM (fpregs[5], 4);
	movq	%rax, -1424(%rbp)	# tmp1138, iov[61].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:269:       ADD_STRING ("\n ST(2) ");
	movq	%rdi, -1280(%rbp)	# tmp1361, iov[70].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:261:       ADD_STRING (" ");
	leaq	.LC34(%rip), %rax	#, tmp1139
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:265:       ADD_MEM (fpregs[8], 4);
	movq	%rdx, -1344(%rbp)	# tmp1146, iov[66].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:267:       ADD_MEM (fpregs[9], 8);
	leaq	72(%r8), %rdx	#, tmp1149
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:274:       ADD_STRING ("   ST(3) ");
	leaq	.LC37(%rip), %rdi	#, tmp1362
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:261:       ADD_STRING (" ");
	movq	%rax, -1408(%rbp)	# tmp1139, iov[62].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:266:       ADD_STRING (" ");
	movq	%rax, -1328(%rbp)	# tmp1139, iov[67].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:267:       ADD_MEM (fpregs[9], 8);
	movq	%rdx, -1312(%rbp)	# tmp1149, iov[68].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:268:       ADD_MEM (fpregs[10], 8);
	leaq	80(%r8), %rdx	#, tmp1151
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:271:       ADD_STRING (" ");
	movq	%rax, -1248(%rbp)	# tmp1139, iov[72].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:274:       ADD_STRING ("   ST(3) ");
	movq	%rdi, -1200(%rbp)	# tmp1362, iov[75].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:262:       ADD_MEM (fpregs[6], 8);
	movq	$8, -1384(%rbp)	#, iov[63].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:279:       ADD_STRING ("\n ST(4) ");
	leaq	.LC38(%rip), %rdi	#, tmp1363
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:268:       ADD_MEM (fpregs[10], 8);
	movq	%rdx, -1296(%rbp)	# tmp1151, iov[69].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:270:       ADD_MEM (fpregs[11], 4);
	leaq	88(%r8), %rdx	#, tmp1154
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:263:       ADD_MEM (fpregs[7], 8);
	movq	$8, -1368(%rbp)	#, iov[64].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:264:       ADD_STRING ("   ST(1) ");
	movq	$9, -1352(%rbp)	#, iov[65].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:265:       ADD_MEM (fpregs[8], 4);
	movq	$4, -1336(%rbp)	#, iov[66].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:270:       ADD_MEM (fpregs[11], 4);
	movq	%rdx, -1264(%rbp)	# tmp1154, iov[71].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:272:       ADD_MEM (fpregs[12], 8);
	leaq	96(%r8), %rdx	#, tmp1157
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:266:       ADD_STRING (" ");
	movq	$1, -1320(%rbp)	#, iov[67].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:267:       ADD_MEM (fpregs[9], 8);
	movq	$8, -1304(%rbp)	#, iov[68].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:268:       ADD_MEM (fpregs[10], 8);
	movq	$8, -1288(%rbp)	#, iov[69].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:272:       ADD_MEM (fpregs[12], 8);
	movq	%rdx, -1232(%rbp)	# tmp1157, iov[73].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:273:       ADD_MEM (fpregs[13], 8);
	leaq	104(%r8), %rdx	#, tmp1159
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:269:       ADD_STRING ("\n ST(2) ");
	movq	$8, -1272(%rbp)	#, iov[70].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:270:       ADD_MEM (fpregs[11], 4);
	movq	$4, -1256(%rbp)	#, iov[71].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:271:       ADD_STRING (" ");
	movq	$1, -1240(%rbp)	#, iov[72].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:273:       ADD_MEM (fpregs[13], 8);
	movq	%rdx, -1216(%rbp)	# tmp1159, iov[74].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:275:       ADD_MEM (fpregs[14], 4);
	leaq	112(%r8), %rdx	#, tmp1162
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:272:       ADD_MEM (fpregs[12], 8);
	movq	$8, -1224(%rbp)	#, iov[73].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:273:       ADD_MEM (fpregs[13], 8);
	movq	$8, -1208(%rbp)	#, iov[74].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:274:       ADD_STRING ("   ST(3) ");
	movq	$9, -1192(%rbp)	#, iov[75].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:275:       ADD_MEM (fpregs[14], 4);
	movq	%rdx, -1184(%rbp)	# tmp1162, iov[76].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:277:       ADD_MEM (fpregs[15], 8);
	leaq	120(%r8), %rdx	#, tmp1165
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:279:       ADD_STRING ("\n ST(4) ");
	movq	%rdi, -1120(%rbp)	# tmp1363, iov[80].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:284:       ADD_STRING ("   ST(5) ");
	leaq	.LC39(%rip), %rdi	#, tmp1364
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:276:       ADD_STRING (" ");
	movq	%rax, -1168(%rbp)	# tmp1139, iov[77].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:281:       ADD_STRING (" ");
	movq	%rax, -1088(%rbp)	# tmp1139, iov[82].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:277:       ADD_MEM (fpregs[15], 8);
	movq	%rdx, -1152(%rbp)	# tmp1165, iov[78].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:278:       ADD_MEM (fpregs[16], 8);
	leaq	128(%r8), %rdx	#, tmp1167
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:286:       ADD_STRING (" ");
	movq	%rax, -1008(%rbp)	# tmp1139, iov[87].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:284:       ADD_STRING ("   ST(5) ");
	movq	%rdi, -1040(%rbp)	# tmp1364, iov[85].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:289:       ADD_STRING ("\n ST(6) ");
	leaq	.LC40(%rip), %rdi	#, tmp1365
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:291:       ADD_STRING (" ");
	movq	%rax, -928(%rbp)	# tmp1139, iov[92].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:278:       ADD_MEM (fpregs[16], 8);
	movq	%rdx, -1136(%rbp)	# tmp1167, iov[79].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:280:       ADD_MEM (fpregs[17], 4);
	leaq	136(%r8), %rdx	#, tmp1170
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:275:       ADD_MEM (fpregs[14], 4);
	movq	$4, -1176(%rbp)	#, iov[76].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:289:       ADD_STRING ("\n ST(6) ");
	movq	%rdi, -960(%rbp)	# tmp1365, iov[90].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:276:       ADD_STRING (" ");
	movq	$1, -1160(%rbp)	#, iov[77].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:294:       ADD_STRING ("   ST(7) ");
	leaq	.LC41(%rip), %rdi	#, tmp1366
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:280:       ADD_MEM (fpregs[17], 4);
	movq	%rdx, -1104(%rbp)	# tmp1170, iov[81].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:282:       ADD_MEM (fpregs[18], 8);
	leaq	144(%r8), %rdx	#, tmp1173
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:277:       ADD_MEM (fpregs[15], 8);
	movq	$8, -1144(%rbp)	#, iov[78].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:278:       ADD_MEM (fpregs[16], 8);
	movq	$8, -1128(%rbp)	#, iov[79].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:279:       ADD_STRING ("\n ST(4) ");
	movq	$8, -1112(%rbp)	#, iov[80].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:282:       ADD_MEM (fpregs[18], 8);
	movq	%rdx, -1072(%rbp)	# tmp1173, iov[83].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:283:       ADD_MEM (fpregs[19], 8);
	leaq	152(%r8), %rdx	#, tmp1175
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:280:       ADD_MEM (fpregs[17], 4);
	movq	$4, -1096(%rbp)	#, iov[81].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:281:       ADD_STRING (" ");
	movq	$1, -1080(%rbp)	#, iov[82].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:282:       ADD_MEM (fpregs[18], 8);
	movq	$8, -1064(%rbp)	#, iov[83].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:283:       ADD_MEM (fpregs[19], 8);
	movq	%rdx, -1056(%rbp)	# tmp1175, iov[84].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:285:       ADD_MEM (fpregs[20], 4);
	leaq	160(%r8), %rdx	#, tmp1178
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:283:       ADD_MEM (fpregs[19], 8);
	movq	$8, -1048(%rbp)	#, iov[84].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:284:       ADD_STRING ("   ST(5) ");
	movq	$9, -1032(%rbp)	#, iov[85].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:285:       ADD_MEM (fpregs[20], 4);
	movq	$4, -1016(%rbp)	#, iov[86].iov_len
	movq	%rdx, -1024(%rbp)	# tmp1178, iov[86].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:287:       ADD_MEM (fpregs[21], 8);
	leaq	168(%r8), %rdx	#, tmp1181
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:286:       ADD_STRING (" ");
	movq	$1, -1000(%rbp)	#, iov[87].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:287:       ADD_MEM (fpregs[21], 8);
	movq	$8, -984(%rbp)	#, iov[88].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:288:       ADD_MEM (fpregs[22], 8);
	movq	$8, -968(%rbp)	#, iov[89].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:287:       ADD_MEM (fpregs[21], 8);
	movq	%rdx, -992(%rbp)	# tmp1181, iov[88].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:288:       ADD_MEM (fpregs[22], 8);
	leaq	176(%r8), %rdx	#, tmp1183
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:289:       ADD_STRING ("\n ST(6) ");
	movq	$8, -952(%rbp)	#, iov[90].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:290:       ADD_MEM (fpregs[23], 4);
	movq	$4, -936(%rbp)	#, iov[91].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:288:       ADD_MEM (fpregs[22], 8);
	movq	%rdx, -976(%rbp)	# tmp1183, iov[89].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:290:       ADD_MEM (fpregs[23], 4);
	leaq	184(%r8), %rdx	#, tmp1186
	movq	%rdx, -944(%rbp)	# tmp1186, iov[91].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:291:       ADD_STRING (" ");
	movq	$1, -920(%rbp)	#, iov[92].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:292:       ADD_MEM (fpregs[24], 8);
	leaq	192(%r8), %rdx	#, tmp1189
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:296:       ADD_STRING (" ");
	movq	%rax, -848(%rbp)	# tmp1139, iov[97].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:298:       ADD_MEM (fpregs[28], 8);
	leaq	224(%r8), %rax	#, tmp1199
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:292:       ADD_MEM (fpregs[24], 8);
	movq	$8, -904(%rbp)	#, iov[93].iov_len
	movq	%rdx, -912(%rbp)	# tmp1189, iov[93].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:293:       ADD_MEM (fpregs[25], 8);
	leaq	200(%r8), %rdx	#, tmp1191
	movq	$8, -888(%rbp)	#, iov[94].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:298:       ADD_MEM (fpregs[28], 8);
	movq	%rax, -816(%rbp)	# tmp1199, iov[99].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:300:       ADD_STRING ("\n mxcsr: ");
	leaq	.LC42(%rip), %rax	#, tmp1367
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:294:       ADD_STRING ("   ST(7) ");
	movq	%rdi, -880(%rbp)	# tmp1366, iov[95].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:293:       ADD_MEM (fpregs[25], 8);
	movq	%rdx, -896(%rbp)	# tmp1191, iov[94].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:295:       ADD_MEM (fpregs[27], 4);
	leaq	216(%r8), %rdx	#, tmp1194
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:301:       ADD_MEM (fpregs[29], 4);
	addq	$232, %r8	#, tmp1202
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:300:       ADD_STRING ("\n mxcsr: ");
	movq	%rax, -800(%rbp)	# tmp1367, iov[100].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:303:       ADD_STRING ("\n XMM0:  ");
	leaq	.LC43(%rip), %rax	#, tmp1368
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:294:       ADD_STRING ("   ST(7) ");
	movq	$9, -872(%rbp)	#, iov[95].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:295:       ADD_MEM (fpregs[27], 4);
	movq	%rdx, -864(%rbp)	# tmp1194, iov[96].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:297:       ADD_MEM (fpregs[27], 8);
	movq	%rdx, -832(%rbp)	# tmp1194, iov[98].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:334:       ADD_MEM (xmmregs[0], 32);
	movl	$135, %edx	#, prephitmp_2035
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:303:       ADD_STRING ("\n XMM0:  ");
	movq	%rax, -768(%rbp)	# tmp1368, iov[102].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:305:       ADD_STRING (" XMM1:  ");
	leaq	.LC44(%rip), %rax	#, tmp1369
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:295:       ADD_MEM (fpregs[27], 4);
	movq	$4, -856(%rbp)	#, iov[96].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:296:       ADD_STRING (" ");
	movq	$1, -840(%rbp)	#, iov[97].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:297:       ADD_MEM (fpregs[27], 8);
	movq	$8, -824(%rbp)	#, iov[98].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:305:       ADD_STRING (" XMM1:  ");
	movq	%rax, -736(%rbp)	# tmp1369, iov[104].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:307:       ADD_STRING ("\n XMM2:  ");
	leaq	.LC45(%rip), %rax	#, tmp1370
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:298:       ADD_MEM (fpregs[28], 8);
	movq	$8, -808(%rbp)	#, iov[99].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:300:       ADD_STRING ("\n mxcsr: ");
	movq	$9, -792(%rbp)	#, iov[100].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:301:       ADD_MEM (fpregs[29], 4);
	movq	%r8, -784(%rbp)	# tmp1202, iov[101].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:307:       ADD_STRING ("\n XMM2:  ");
	movq	%rax, -704(%rbp)	# tmp1370, iov[106].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:309:       ADD_STRING (" XMM3:  ");
	leaq	.LC46(%rip), %rax	#, tmp1371
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:301:       ADD_MEM (fpregs[29], 4);
	movq	$4, -776(%rbp)	#, iov[101].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:303:       ADD_STRING ("\n XMM0:  ");
	movq	$9, -760(%rbp)	#, iov[102].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:304:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -752(%rbp)	# tmp1264, iov[103].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:309:       ADD_STRING (" XMM3:  ");
	movq	%rax, -672(%rbp)	# tmp1371, iov[108].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:311:       ADD_STRING ("\n XMM4:  ");
	leaq	.LC47(%rip), %rax	#, tmp1372
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:304:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -744(%rbp)	#, iov[103].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:305:       ADD_STRING (" XMM1:  ");
	movq	$8, -728(%rbp)	#, iov[104].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:306:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -720(%rbp)	# tmp1264, iov[105].iov_base
	movq	$32, -712(%rbp)	#, iov[105].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:307:       ADD_STRING ("\n XMM2:  ");
	movq	$9, -696(%rbp)	#, iov[106].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:308:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -688(%rbp)	# tmp1264, iov[107].iov_base
	movq	$32, -680(%rbp)	#, iov[107].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:309:       ADD_STRING (" XMM3:  ");
	movq	$8, -664(%rbp)	#, iov[108].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:310:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -656(%rbp)	# tmp1264, iov[109].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:311:       ADD_STRING ("\n XMM4:  ");
	movq	%rax, -640(%rbp)	# tmp1372, iov[110].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:313:       ADD_STRING (" XMM5:  ");
	leaq	.LC48(%rip), %rax	#, tmp1373
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:310:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -648(%rbp)	#, iov[109].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:311:       ADD_STRING ("\n XMM4:  ");
	movq	$9, -632(%rbp)	#, iov[110].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:312:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -624(%rbp)	# tmp1264, iov[111].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:313:       ADD_STRING (" XMM5:  ");
	movq	%rax, -608(%rbp)	# tmp1373, iov[112].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:315:       ADD_STRING ("\n XMM6:  ");
	leaq	.LC49(%rip), %rax	#, tmp1374
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:312:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -616(%rbp)	#, iov[111].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:313:       ADD_STRING (" XMM5:  ");
	movq	$8, -600(%rbp)	#, iov[112].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:314:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -592(%rbp)	# tmp1264, iov[113].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:315:       ADD_STRING ("\n XMM6:  ");
	movq	%rax, -576(%rbp)	# tmp1374, iov[114].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:317:       ADD_STRING (" XMM7:  ");
	leaq	.LC50(%rip), %rax	#, tmp1375
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:314:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -584(%rbp)	#, iov[113].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:315:       ADD_STRING ("\n XMM6:  ");
	movq	$9, -568(%rbp)	#, iov[114].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:316:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -560(%rbp)	# tmp1264, iov[115].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:317:       ADD_STRING (" XMM7:  ");
	movq	%rax, -544(%rbp)	# tmp1375, iov[116].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:319:       ADD_STRING ("\n XMM8:  ");
	leaq	.LC51(%rip), %rax	#, tmp1376
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:316:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -552(%rbp)	#, iov[115].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:317:       ADD_STRING (" XMM7:  ");
	movq	$8, -536(%rbp)	#, iov[116].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:318:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -528(%rbp)	# tmp1264, iov[117].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:319:       ADD_STRING ("\n XMM8:  ");
	movq	%rax, -512(%rbp)	# tmp1376, iov[118].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:321:       ADD_STRING (" XMM9:  ");
	leaq	.LC52(%rip), %rax	#, tmp1377
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:318:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -520(%rbp)	#, iov[117].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:319:       ADD_STRING ("\n XMM8:  ");
	movq	$9, -504(%rbp)	#, iov[118].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:320:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -496(%rbp)	# tmp1264, iov[119].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:321:       ADD_STRING (" XMM9:  ");
	movq	%rax, -480(%rbp)	# tmp1377, iov[120].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:323:       ADD_STRING ("\n XMM10: ");
	leaq	.LC53(%rip), %rax	#, tmp1378
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:320:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -488(%rbp)	#, iov[119].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:321:       ADD_STRING (" XMM9:  ");
	movq	$8, -472(%rbp)	#, iov[120].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:322:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -464(%rbp)	# tmp1264, iov[121].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:323:       ADD_STRING ("\n XMM10: ");
	movq	%rax, -448(%rbp)	# tmp1378, iov[122].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:325:       ADD_STRING (" XMM11: ");
	leaq	.LC54(%rip), %rax	#, tmp1379
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:322:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -456(%rbp)	#, iov[121].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:323:       ADD_STRING ("\n XMM10: ");
	movq	$9, -440(%rbp)	#, iov[122].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:324:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -432(%rbp)	# tmp1264, iov[123].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:325:       ADD_STRING (" XMM11: ");
	movq	%rax, -416(%rbp)	# tmp1379, iov[124].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:327:       ADD_STRING ("\n XMM12: ");
	leaq	.LC55(%rip), %rax	#, tmp1380
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:324:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -424(%rbp)	#, iov[123].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:325:       ADD_STRING (" XMM11: ");
	movq	$8, -408(%rbp)	#, iov[124].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:326:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -400(%rbp)	# tmp1264, iov[125].iov_base
	movq	$32, -392(%rbp)	#, iov[125].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:327:       ADD_STRING ("\n XMM12: ");
	movq	%rax, -384(%rbp)	# tmp1380, iov[126].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:329:       ADD_STRING (" XMM13: ");
	leaq	.LC56(%rip), %rax	#, tmp1381
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:327:       ADD_STRING ("\n XMM12: ");
	movq	$9, -376(%rbp)	#, iov[126].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:328:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -368(%rbp)	# tmp1264, iov[127].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:329:       ADD_STRING (" XMM13: ");
	movq	%rax, -352(%rbp)	# tmp1381, iov[128].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:331:       ADD_STRING ("\n XMM14: ");
	leaq	.LC57(%rip), %rax	#, tmp1382
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:328:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -360(%rbp)	#, iov[127].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:329:       ADD_STRING (" XMM13: ");
	movq	$8, -344(%rbp)	#, iov[128].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:330:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -336(%rbp)	# tmp1264, iov[129].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:331:       ADD_STRING ("\n XMM14: ");
	movq	%rax, -320(%rbp)	# tmp1382, iov[130].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:333:       ADD_STRING (" XMM15: ");
	leaq	.LC58(%rip), %rax	#, tmp1383
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:330:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -328(%rbp)	#, iov[129].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:331:       ADD_STRING ("\n XMM14: ");
	movq	$9, -312(%rbp)	#, iov[130].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:332:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -304(%rbp)	# tmp1264, iov[131].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:333:       ADD_STRING (" XMM15: ");
	movq	%rax, -288(%rbp)	# tmp1383, iov[132].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:332:       ADD_MEM (xmmregs[0], 32);
	movq	$32, -296(%rbp)	#, iov[131].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:334:       ADD_MEM (xmmregs[0], 32);
	movl	$134, %eax	#, nr
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:333:       ADD_STRING (" XMM15: ");
	movq	$8, -280(%rbp)	#, iov[132].iov_len
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:334:       ADD_MEM (xmmregs[0], 32);
	movq	%r10, -272(%rbp)	# tmp1264, iov[133].iov_base
	movq	$32, -264(%rbp)	#, iov[133].iov_len
.L81:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:338:   ADD_STRING ("\n");
	leaq	.LC2(%rip), %rdi	#, tmp1384
	salq	$4, %rax	#, tmp1235
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:341:   writev (fd, iov, nr);
	movq	%rbx, %rsi	# tmp1262,
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:338:   ADD_STRING ("\n");
	movq	$1, -2392(%rbp,%rax)	#, iov[nr_771].iov_len
	movq	%rdi, -2400(%rbp,%rax)	# tmp1384, iov[nr_771].iov_base
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:341:   writev (fd, iov, nr);
	movl	%r12d, %edi	# fd,
	call	writev@PLT	#
# ../debug/segfault.c:94:   WRITE_STRING ("\nBacktrace:\n");
	leaq	.LC59(%rip), %rsi	#,
	movl	$12, %edx	#,
	movl	%r12d, %edi	# fd,
	call	write@PLT	#
# ../debug/segfault.c:97:   arr = alloca (256 * sizeof (void *));
	subq	$2064, %rsp	#,
# ../debug/segfault.c:98:   cnt = backtrace (arr, 256);
	movl	$256, %esi	#,
# ../debug/segfault.c:97:   arr = alloca (256 * sizeof (void *));
	leaq	15(%rsp), %r14	#, tmp1250
	andq	$-16, %r14	#, arr
# ../debug/segfault.c:98:   cnt = backtrace (arr, 256);
	movq	%r14, %rdi	# arr,
	call	backtrace@PLT	#
# ../debug/segfault.c:105:   for (i = 0; i < cnt; ++i)
	testl	%eax, %eax	# cnt
# ../sysdeps/unix/sysv/linux/x86_64/sigcontextinfo.h:24:   return ctx->uc_mcontext.gregs[REG_RIP];
	movq	168(%r15), %r8	# MEM[(long long int *)ctx_34(D) + 168B], _59
# ../debug/segfault.c:105:   for (i = 0; i < cnt; ++i)
	jle	.L176	#,
	leaq	-16(%r8), %rsi	#, _2039
	movq	%r14, %rdx	# arr, ivtmp.19
	xorl	%ecx, %ecx	# i
# ../debug/segfault.c:106:     if ((uintptr_t) arr[i] >= pc - 16 && (uintptr_t) arr[i] <= pc + 16)
	addq	$16, %r8	#, tmp1266
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%rdx), %rdi	# MEM[base: _5, offset: 0B], _7
	cmpq	%rsi, %rdi	# _2039, _7
	jb	.L177	#,
# ../debug/segfault.c:106:     if ((uintptr_t) arr[i] >= pc - 16 && (uintptr_t) arr[i] <= pc + 16)
	cmpq	%r8, %rdi	# tmp1266, _7
	jbe	.L311	#,
.L177:
# ../debug/segfault.c:105:   for (i = 0; i < cnt; ++i)
	addl	$1, %ecx	#, i
	addq	$8, %rdx	#, ivtmp.19
	cmpl	%ecx, %eax	# i, cnt
	jne	.L178	#,
.L176:
# ../debug/segfault.c:115:   __backtrace_symbols_fd (arr + i, cnt - i, fd);
	movl	%eax, %esi	# cnt,
	movl	%r12d, %edx	# fd,
	movq	%r14, %rdi	# arr,
	call	__backtrace_symbols_fd@PLT	#
# ../debug/segfault.c:119:   int mapfd = open ("/proc/self/maps", O_RDONLY);
	leaq	.LC60(%rip), %rdi	#,
	xorl	%esi, %esi	#
	xorl	%eax, %eax	#
	call	open@PLT	#
# ../debug/segfault.c:120:   if (mapfd != -1)
	cmpl	$-1, %eax	#, mapfd
# ../debug/segfault.c:119:   int mapfd = open ("/proc/self/maps", O_RDONLY);
	movl	%eax, %r15d	#, mapfd
# ../debug/segfault.c:120:   if (mapfd != -1)
	je	.L179	#,
# ../debug/segfault.c:122:       write (fd, "\nMemory map:\n\n", 14);
	leaq	.LC61(%rip), %rsi	#,
	movl	$14, %edx	#,
	movl	%r12d, %edi	# fd,
	call	write@PLT	#
	.p2align 4,,10
	.p2align 3
.L180:
# ../debug/segfault.c:127:       while ((n = TEMP_FAILURE_RETRY (read (mapfd, buf, sizeof (buf)))) > 0)
	movl	$256, %edx	#,
	movq	%rbx, %rsi	# tmp1262,
	movl	%r15d, %edi	# mapfd,
	call	read@PLT	#
	cmpq	$-1, %rax	#, __result
	movq	%rax, %r14	#, __result
	je	.L312	#,
# ../debug/segfault.c:127:       while ((n = TEMP_FAILURE_RETRY (read (mapfd, buf, sizeof (buf)))) > 0)
	testq	%rax, %rax	# __result
	jle	.L184	#,
	.p2align 4,,10
	.p2align 3
.L182:
# ../debug/segfault.c:128: 	TEMP_FAILURE_RETRY (write (fd, buf, n));
	movq	%r14, %rdx	# __result,
	movq	%rbx, %rsi	# tmp1262,
	movl	%r12d, %edi	# fd,
	call	write@PLT	#
	cmpq	$-1, %rax	#, __result
	jne	.L180	#,
# ../debug/segfault.c:128: 	TEMP_FAILURE_RETRY (write (fd, buf, n));
	call	__errno_location@PLT	#
	cmpl	$4, (%rax)	#, *_16
	je	.L182	#,
	jmp	.L180	#
.L4:
# ../debug/segfault.c:78:   fd = 2;
	movl	$2, %r12d	#, fd
	jmp	.L3	#
	.p2align 4,,10
	.p2align 3
.L312:
# ../debug/segfault.c:127:       while ((n = TEMP_FAILURE_RETRY (read (mapfd, buf, sizeof (buf)))) > 0)
	call	__errno_location@PLT	#
	cmpl	$4, (%rax)	#, *_18
	je	.L180	#,
.L184:
# ../debug/segfault.c:130:       close (mapfd);
	movl	%r15d, %edi	# mapfd,
	call	close@PLT	#
.L179:
# ../debug/segfault.c:136:   sigemptyset (&sa.sa_mask);
	leaq	8(%rbx), %rdi	#, tmp1257
# ../debug/segfault.c:135:   sa.sa_handler = SIG_DFL;
	movq	$0, -2400(%rbp)	#, sa.__sigaction_handler.sa_handler
# ../debug/segfault.c:136:   sigemptyset (&sa.sa_mask);
	call	sigemptyset@PLT	#
# ../debug/segfault.c:138:   sigaction (signal, &sa, NULL);
	movq	%rbx, %rsi	# tmp1262,
	movl	%r13d, %edi	# signal,
	xorl	%edx, %edx	#
# ../debug/segfault.c:137:   sa.sa_flags = 0;
	movl	$0, -2264(%rbp)	#, sa.sa_flags
# ../debug/segfault.c:138:   sigaction (signal, &sa, NULL);
	call	sigaction@PLT	#
# ../debug/segfault.c:139:   raise (signal);
	movl	%r13d, %edi	# signal,
	call	raise@PLT	#
# ../debug/segfault.c:140: }
	leaq	-40(%rbp), %rsp	#,
	popq	%rbx	#
	popq	%r12	#
	popq	%r13	#
	popq	%r14	#
	popq	%r15	#
	popq	%rbp	#
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L311:
	.cfi_restore_state
	subl	%ecx, %eax	# i, cnt
# ../debug/segfault.c:106:     if ((uintptr_t) arr[i] >= pc - 16 && (uintptr_t) arr[i] <= pc + 16)
	movq	%rdx, %r14	# ivtmp.19, arr
	jmp	.L176	#
.L186:
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:170:   if (ctx->uc_mcontext.fpregs != NULL)
	movl	$51, %edx	#, prephitmp_2035
# ../sysdeps/unix/sysv/linux/x86_64/register-dump.h:168:   ADD_MEM (regs[24], 8);
	movl	$50, %eax	#, nr
	jmp	.L81	#
	.cfi_endproc
.LFE78:
	.size	catch_segfault, .-catch_segfault
	.section	.rodata.str1.1
.LC62:
	.string	"SEGFAULT_SIGNALS"
.LC63:
	.string	"SEGFAULT_USE_ALTSTACK"
.LC64:
	.string	"all"
.LC65:
	.string	"segv"
.LC66:
	.string	"SEGFAULT_OUTPUT_NAME"
.LC67:
	.string	"ill"
.LC68:
	.string	"bus"
.LC69:
	.string	"stkflt"
.LC70:
	.string	"abrt"
.LC71:
	.string	"fpe"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	install_handler, @function
install_handler:
.LFB79:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
# ../debug/segfault.c:148:   const char *sigs = getenv ("SEGFAULT_SIGNALS");
	leaq	.LC62(%rip), %rdi	#,
# ../debug/segfault.c:146: {
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	subq	$200, %rsp	#,
	.cfi_def_cfa_offset 240
# ../debug/segfault.c:158:   sigemptyset (&sa.sa_mask);
	leaq	32(%rsp), %rbp	#, tmp247
# ../debug/segfault.c:148:   const char *sigs = getenv ("SEGFAULT_SIGNALS");
	call	getenv@PLT	#
# ../debug/segfault.c:158:   sigemptyset (&sa.sa_mask);
	leaq	8(%rbp), %rdi	#, tmp195
# ../debug/segfault.c:148:   const char *sigs = getenv ("SEGFAULT_SIGNALS");
	movq	%rax, %rbx	#, sigs
# ../debug/segfault.c:152:   sa.sa_sigaction = catch_segfault;
	leaq	catch_segfault(%rip), %rax	#, tmp250
# ../debug/segfault.c:153:   sa.sa_flags = SA_SIGINFO;
	movl	$4, 168(%rsp)	#, sa.sa_flags
# ../debug/segfault.c:152:   sa.sa_sigaction = catch_segfault;
	movq	%rax, 32(%rsp)	# tmp250, sa.__sigaction_handler.sa_sigaction
# ../debug/segfault.c:158:   sigemptyset (&sa.sa_mask);
	call	sigemptyset@PLT	#
# ../debug/segfault.c:162:   if (getenv ("SEGFAULT_USE_ALTSTACK") != 0)
	leaq	.LC63(%rip), %rdi	#,
# ../debug/segfault.c:159:   sa.sa_flags |= SA_RESTART;
	orl	$268435456, 168(%rsp)	#, sa.sa_flags
# ../debug/segfault.c:162:   if (getenv ("SEGFAULT_USE_ALTSTACK") != 0)
	call	getenv@PLT	#
	testq	%rax, %rax	# _3
	je	.L314	#,
# ../debug/segfault.c:164:       void *stack_mem = malloc (2 * SIGSTKSZ);
	movl	$16384, %edi	#,
	call	malloc@PLT	#
# ../debug/segfault.c:167:       if (stack_mem != NULL)
	testq	%rax, %rax	# tmp196
	je	.L314	#,
# ../debug/segfault.c:173: 	  if (sigaltstack (&ss, NULL) == 0)
	xorl	%esi, %esi	#
	movq	%rsp, %rdi	#, tmp197
# ../debug/segfault.c:169: 	  ss.ss_sp = stack_mem;
	movq	%rax, (%rsp)	# tmp196, ss.ss_sp
# ../debug/segfault.c:170: 	  ss.ss_flags = 0;
	movl	$0, 8(%rsp)	#, ss.ss_flags
# ../debug/segfault.c:171: 	  ss.ss_size = 2 * SIGSTKSZ;
	movq	$16384, 16(%rsp)	#, ss.ss_size
# ../debug/segfault.c:173: 	  if (sigaltstack (&ss, NULL) == 0)
	call	sigaltstack@PLT	#
	testl	%eax, %eax	# _4
	je	.L386	#,
	.p2align 4,,10
	.p2align 3
.L314:
# ../debug/segfault.c:178:   if (sigs == NULL)
	testq	%rbx, %rbx	# sigs
	je	.L387	#,
# ../debug/segfault.c:180:   else if (sigs[0] == '\0')
	cmpb	$0, (%rbx)	#, *sigs_109
	je	.L313	#,
# ../debug/segfault.c:186:       int all = __strcasecmp (sigs, "all") == 0;
	leaq	.LC64(%rip), %rsi	#,
	movq	%rbx, %rdi	# sigs,
	call	__strcasecmp@PLT	#
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	leaq	.LC65(%rip), %rsi	#,
# ../debug/segfault.c:186:       int all = __strcasecmp (sigs, "all") == 0;
	movl	%eax, %r13d	#, _8
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	movq	%rbx, %rdi	# sigs,
	call	__strcasestr@PLT	#
	testl	%r13d, %r13d	# _8
	movq	%rax, %r12	#, where
	je	.L321	#,
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	testq	%rax, %rax	# where
	je	.L381	#,
	call	__ctype_b_loc@PLT	#
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	cmpq	%r12, %rbx	# where, sigs
	movq	(%rax), %rax	# *_124, pretmp_103
	je	.L323	#,
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	movsbq	-1(%r12), %rdx	# MEM[(const char *)where_123 + -1B], MEM[(const char *)where_123 + -1B]
	testb	$8, (%rax,%rdx,2)	#, *_14
	jne	.L381	#,
.L323:
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	movsbq	4(%r12), %rdx	# MEM[(const char *)where_123 + 4B], MEM[(const char *)where_123 + 4B]
	testb	$8, (%rax,%rdx,2)	#, *_21
	jne	.L381	#,
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$11, %edi	#,
	call	sigaction@PLT	#
.L381:
# ../debug/segfault.c:196:       INSTALL_FOR_SIG (SIGILL, "ill");
	leaq	.LC67(%rip), %rsi	#,
	movq	%rbx, %rdi	# sigs,
	call	__strcasestr@PLT	#
	movq	%rax, %r12	#, where
	testq	%r12, %r12	# where
	je	.L382	#,
	call	__ctype_b_loc@PLT	#
# ../debug/segfault.c:196:       INSTALL_FOR_SIG (SIGILL, "ill");
	cmpq	%r12, %rbx	# where, sigs
	movq	(%rax), %rax	# *_198, pretmp_125
	je	.L326	#,
# ../debug/segfault.c:196:       INSTALL_FOR_SIG (SIGILL, "ill");
	movsbq	-1(%r12), %rdx	# MEM[(const char *)where_218 + -1B], MEM[(const char *)where_218 + -1B]
	testb	$8, (%rax,%rdx,2)	#, *_28
	jne	.L382	#,
.L326:
# ../debug/segfault.c:196:       INSTALL_FOR_SIG (SIGILL, "ill");
	movsbq	3(%r12), %rdx	# MEM[(const char *)where_218 + 3B], MEM[(const char *)where_218 + 3B]
	testb	$8, (%rax,%rdx,2)	#, *_35
	jne	.L382	#,
# ../debug/segfault.c:196:       INSTALL_FOR_SIG (SIGILL, "ill");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$4, %edi	#,
	call	sigaction@PLT	#
.L382:
# ../debug/segfault.c:198:       INSTALL_FOR_SIG (SIGBUS, "bus");
	leaq	.LC68(%rip), %rsi	#,
	movq	%rbx, %rdi	# sigs,
	call	__strcasestr@PLT	#
	movq	%rax, %r12	#, where
	testq	%r12, %r12	# where
	je	.L383	#,
	call	__ctype_b_loc@PLT	#
# ../debug/segfault.c:198:       INSTALL_FOR_SIG (SIGBUS, "bus");
	cmpq	%r12, %rbx	# where, sigs
	movq	(%rax), %rax	# *_211, pretmp_195
	je	.L329	#,
# ../debug/segfault.c:198:       INSTALL_FOR_SIG (SIGBUS, "bus");
	movsbq	-1(%r12), %rdx	# MEM[(const char *)where_219 + -1B], MEM[(const char *)where_219 + -1B]
	testb	$8, (%rax,%rdx,2)	#, *_42
	jne	.L383	#,
.L329:
# ../debug/segfault.c:198:       INSTALL_FOR_SIG (SIGBUS, "bus");
	movsbq	3(%r12), %rdx	# MEM[(const char *)where_219 + 3B], MEM[(const char *)where_219 + 3B]
	testb	$8, (%rax,%rdx,2)	#, *_158
	jne	.L383	#,
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$7, %edi	#,
	call	sigaction@PLT	#
.L383:
# ../debug/segfault.c:201:       INSTALL_FOR_SIG (SIGSTKFLT, "stkflt");
	leaq	.LC69(%rip), %rsi	#,
	movq	%rbx, %rdi	# sigs,
	call	__strcasestr@PLT	#
	movq	%rax, %r12	#, where
	testq	%r12, %r12	# where
	je	.L384	#,
	call	__ctype_b_loc@PLT	#
# ../debug/segfault.c:201:       INSTALL_FOR_SIG (SIGSTKFLT, "stkflt");
	cmpq	%r12, %rbx	# where, sigs
	movq	(%rax), %rax	# *_101, pretmp_197
	je	.L331	#,
# ../debug/segfault.c:201:       INSTALL_FOR_SIG (SIGSTKFLT, "stkflt");
	movsbq	-1(%r12), %rdx	# MEM[(const char *)where_220 + -1B], MEM[(const char *)where_220 + -1B]
	testb	$8, (%rax,%rdx,2)	#, *_56
	jne	.L384	#,
.L331:
# ../debug/segfault.c:201:       INSTALL_FOR_SIG (SIGSTKFLT, "stkflt");
	movsbq	6(%r12), %rdx	# MEM[(const char *)where_220 + 6B], MEM[(const char *)where_220 + 6B]
	testb	$8, (%rax,%rdx,2)	#, *_166
	jne	.L384	#,
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$16, %edi	#,
	call	sigaction@PLT	#
.L384:
# ../debug/segfault.c:203:       INSTALL_FOR_SIG (SIGABRT, "abrt");
	leaq	.LC70(%rip), %rsi	#,
	movq	%rbx, %rdi	# sigs,
	call	__strcasestr@PLT	#
	movq	%rax, %r12	#, where
	testq	%r12, %r12	# where
	je	.L385	#,
	call	__ctype_b_loc@PLT	#
# ../debug/segfault.c:203:       INSTALL_FOR_SIG (SIGABRT, "abrt");
	cmpq	%r12, %rbx	# where, sigs
	movq	(%rax), %rax	# *_99, pretmp_194
	je	.L333	#,
# ../debug/segfault.c:203:       INSTALL_FOR_SIG (SIGABRT, "abrt");
	movsbq	-1(%r12), %rdx	# MEM[(const char *)where_221 + -1B], MEM[(const char *)where_221 + -1B]
	testb	$8, (%rax,%rdx,2)	#, *_70
	jne	.L385	#,
.L333:
# ../debug/segfault.c:203:       INSTALL_FOR_SIG (SIGABRT, "abrt");
	movsbq	4(%r12), %rdx	# MEM[(const char *)where_221 + 4B], MEM[(const char *)where_221 + 4B]
	testb	$8, (%rax,%rdx,2)	#, *_230
	jne	.L385	#,
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$6, %edi	#,
	call	sigaction@PLT	#
.L385:
# ../debug/segfault.c:204:       INSTALL_FOR_SIG (SIGFPE, "fpe");
	leaq	.LC71(%rip), %rsi	#,
	movq	%rbx, %rdi	# sigs,
	call	__strcasestr@PLT	#
	movq	%rax, %r12	#, where
	testq	%r12, %r12	# where
	je	.L319	#,
	call	__ctype_b_loc@PLT	#
# ../debug/segfault.c:204:       INSTALL_FOR_SIG (SIGFPE, "fpe");
	cmpq	%r12, %rbx	# where, sigs
	movq	(%rax), %rax	# *_200, pretmp_191
	je	.L334	#,
# ../debug/segfault.c:204:       INSTALL_FOR_SIG (SIGFPE, "fpe");
	movsbq	-1(%r12), %rdx	# MEM[(const char *)where_222 + -1B], MEM[(const char *)where_222 + -1B]
	testb	$8, (%rax,%rdx,2)	#, *_84
	jne	.L319	#,
.L334:
# ../debug/segfault.c:204:       INSTALL_FOR_SIG (SIGFPE, "fpe");
	movsbq	3(%r12), %rdx	# MEM[(const char *)where_222 + 3B], MEM[(const char *)where_222 + 3B]
	testb	$8, (%rax,%rdx,2)	#, *_91
	jne	.L319	#,
.L343:
# ../debug/segfault.c:204:       INSTALL_FOR_SIG (SIGFPE, "fpe");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$8, %edi	#,
	call	sigaction@PLT	#
.L319:
# ../debug/segfault.c:208:   name = getenv ("SEGFAULT_OUTPUT_NAME");
	leaq	.LC66(%rip), %rdi	#,
	call	getenv@PLT	#
# ../debug/segfault.c:209:   if (name != NULL && name[0] != '\0')
	testq	%rax, %rax	# name
# ../debug/segfault.c:208:   name = getenv ("SEGFAULT_OUTPUT_NAME");
	movq	%rax, %rbx	#, name
# ../debug/segfault.c:209:   if (name != NULL && name[0] != '\0')
	je	.L313	#,
# ../debug/segfault.c:209:   if (name != NULL && name[0] != '\0')
	cmpb	$0, (%rax)	#, *name_138
	jne	.L388	#,
.L313:
# ../debug/segfault.c:216: }
	addq	$200, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
.L388:
	.cfi_restore_state
# ../debug/segfault.c:211:       int ret = access (name, R_OK | W_OK);
	movl	$6, %esi	#,
	movq	%rax, %rdi	# name,
	call	access@PLT	#
# ../debug/segfault.c:213:       if (ret == 0 || (ret == -1 && errno == ENOENT))
	testl	%eax, %eax	# ret
	je	.L337	#,
# ../debug/segfault.c:213:       if (ret == 0 || (ret == -1 && errno == ENOENT))
	addl	$1, %eax	#, ret
	jne	.L313	#,
# ../debug/segfault.c:213:       if (ret == 0 || (ret == -1 && errno == ENOENT))
	call	__errno_location@PLT	#
	cmpl	$2, (%rax)	#, *_94
	jne	.L313	#,
.L337:
# ../debug/segfault.c:214: 	fname = __strdup (name);
	movq	%rbx, %rdi	# name,
	call	__strdup@PLT	#
	movq	%rax, fname(%rip)	# tmp227, fname
	jmp	.L313	#
.L386:
# ../debug/segfault.c:174: 	    sa.sa_flags |= SA_ONSTACK;
	orl	$134217728, 168(%rsp)	#, sa.sa_flags
	jmp	.L314	#
.L387:
# ../debug/segfault.c:179:     sigaction (SIGSEGV, &sa, NULL);
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$11, %edi	#,
	call	sigaction@PLT	#
	jmp	.L319	#
.L321:
# ../debug/segfault.c:195:       INSTALL_FOR_SIG (SIGSEGV, "segv");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$11, %edi	#,
	call	sigaction@PLT	#
# ../debug/segfault.c:196:       INSTALL_FOR_SIG (SIGILL, "ill");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$4, %edi	#,
	call	sigaction@PLT	#
# ../debug/segfault.c:198:       INSTALL_FOR_SIG (SIGBUS, "bus");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$7, %edi	#,
	call	sigaction@PLT	#
# ../debug/segfault.c:201:       INSTALL_FOR_SIG (SIGSTKFLT, "stkflt");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$16, %edi	#,
	call	sigaction@PLT	#
# ../debug/segfault.c:203:       INSTALL_FOR_SIG (SIGABRT, "abrt");
	xorl	%edx, %edx	#
	movq	%rbp, %rsi	# tmp247,
	movl	$6, %edi	#,
	call	sigaction@PLT	#
	jmp	.L343	#
	.cfi_endproc
.LFE79:
	.size	install_handler, .-install_handler
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	install_handler
	.local	fname
	.comm	fname,8,8
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
