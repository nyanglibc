	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*** "
.LC1:
	.string	"signal "
.LC2:
	.string	"\n"
.LC3:
	.string	"Register dump:\n\n RAX: "
.LC4:
	.string	"   RBX: "
.LC5:
	.string	"   RCX: "
.LC6:
	.string	"\n RDX: "
.LC7:
	.string	"   RSI: "
.LC8:
	.string	"   RDI: "
.LC9:
	.string	"\n RBP: "
.LC10:
	.string	"   R8 : "
.LC11:
	.string	"   R9 : "
.LC12:
	.string	"\n R10: "
.LC13:
	.string	"   R11: "
.LC14:
	.string	"   R12: "
.LC15:
	.string	"\n R13: "
.LC16:
	.string	"   R14: "
.LC17:
	.string	"   R15: "
.LC18:
	.string	"\n RSP: "
.LC19:
	.string	"\n\n RIP: "
.LC20:
	.string	"   EFLAGS: "
.LC21:
	.string	"\n\n CS: "
.LC22:
	.string	"   FS: "
.LC23:
	.string	"   GS: "
.LC24:
	.string	"\n\n Trap: "
.LC25:
	.string	"   Error: "
.LC26:
	.string	"   OldMask: "
.LC27:
	.string	"   CR2: "
.LC28:
	.string	"\n\n FPUCW: "
.LC29:
	.string	"   FPUSW: "
.LC30:
	.string	"   TAG: "
.LC31:
	.string	"\n RIP: "
.LC32:
	.string	"   RDP: "
.LC33:
	.string	"\n\n ST(0) "
.LC34:
	.string	" "
.LC35:
	.string	"   ST(1) "
.LC36:
	.string	"\n ST(2) "
.LC37:
	.string	"   ST(3) "
.LC38:
	.string	"\n ST(4) "
.LC39:
	.string	"   ST(5) "
.LC40:
	.string	"\n ST(6) "
.LC41:
	.string	"   ST(7) "
.LC42:
	.string	"\n mxcsr: "
.LC43:
	.string	"\n XMM0:  "
.LC44:
	.string	" XMM1:  "
.LC45:
	.string	"\n XMM2:  "
.LC46:
	.string	" XMM3:  "
.LC47:
	.string	"\n XMM4:  "
.LC48:
	.string	" XMM5:  "
.LC49:
	.string	"\n XMM6:  "
.LC50:
	.string	" XMM7:  "
.LC51:
	.string	"\n XMM8:  "
.LC52:
	.string	" XMM9:  "
.LC53:
	.string	"\n XMM10: "
.LC54:
	.string	" XMM11: "
.LC55:
	.string	"\n XMM12: "
.LC56:
	.string	" XMM13: "
.LC57:
	.string	"\n XMM14: "
.LC58:
	.string	" XMM15: "
.LC59:
	.string	"\nBacktrace:\n"
.LC60:
	.string	"/proc/self/maps"
.LC61:
	.string	"\nMemory map:\n\n"
	.text
	.p2align 4,,15
	.type	catch_segfault, @function
catch_segfault:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edi, %r13d
	pushq	%rbx
	movq	%rdx, %r15
	subq	$3528, %rsp
	movq	fname(%rip), %rdi
	testq	%rdi, %rdi
	je	.L4
	xorl	%eax, %eax
	movl	$438, %edx
	movl	$577, %esi
	call	open@PLT
	cmpl	$-1, %eax
	movl	%eax, %r12d
	je	.L4
.L3:
	leaq	.LC0(%rip), %rsi
	movl	$4, %edx
	movl	%r12d, %edi
	leaq	-2400(%rbp), %rbx
	call	write@PLT
	movq	_itoa_lower_digits@GOTPCREL(%rip), %r14
	leaq	30(%rbx), %r8
	movslq	%r13d, %rcx
	movabsq	$-3689348814741910323, %rsi
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rcx, %rax
	subq	$1, %r8
	mulq	%rsi
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rcx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %eax
	movq	%rdx, %rcx
	movb	%al, (%r8)
	jne	.L5
	leaq	.LC1(%rip), %rsi
	movl	$7, %edx
	movl	%r12d, %edi
	movq	%r8, -3560(%rbp)
	call	write@PLT
	movq	-3560(%rbp), %r8
	leaq	30(%rbx), %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	subq	%r8, %rdx
	call	write@PLT
	leaq	.LC2(%rip), %rsi
	movl	$1, %edx
	movl	%r12d, %edi
	call	write@PLT
	leaq	-3312(%rbp), %rax
	movq	144(%r15), %rcx
	leaq	16(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L6
	cmpq	%rax, %rdx
	jbe	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rax, %rdx
	jne	.L8
.L7:
	movq	128(%r15), %rcx
	leaq	32(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L9
	leaq	16(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L11
.L10:
	movq	152(%r15), %rcx
	leaq	48(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L12
	leaq	32(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L14
.L13:
	movq	136(%r15), %rcx
	leaq	64(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L15
	leaq	48(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L16
	.p2align 4,,10
	.p2align 3
.L17:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L17
.L16:
	movq	112(%r15), %rcx
	leaq	80(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L18
	leaq	64(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L20
.L19:
	movq	104(%r15), %rcx
	leaq	96(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L21
	leaq	80(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L22
	.p2align 4,,10
	.p2align 3
.L23:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L23
.L22:
	movq	120(%r15), %rcx
	leaq	112(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L24
	leaq	96(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L26
.L25:
	movq	40(%r15), %rcx
	leaq	128(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L27
	leaq	112(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L29
.L28:
	movq	48(%r15), %rcx
	leaq	144(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L30
	leaq	128(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L32
.L31:
	movq	56(%r15), %rcx
	leaq	160(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L33
	leaq	144(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L35
.L34:
	movq	64(%r15), %rcx
	leaq	176(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L36
	leaq	160(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L37
	.p2align 4,,10
	.p2align 3
.L38:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L38
.L37:
	movq	72(%r15), %rcx
	leaq	192(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L39
	leaq	176(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L41
.L40:
	movq	80(%r15), %rcx
	leaq	208(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L42
	leaq	192(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L44
.L43:
	movq	88(%r15), %rcx
	leaq	224(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L45
	leaq	208(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L46
	.p2align 4,,10
	.p2align 3
.L47:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L47
.L46:
	movq	96(%r15), %rcx
	leaq	240(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L48
	leaq	224(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L49
	.p2align 4,,10
	.p2align 3
.L50:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L50
.L49:
	movq	160(%r15), %rcx
	leaq	256(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L51
	leaq	240(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L53
.L52:
	movq	168(%r15), %rcx
	leaq	272(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L54
	leaq	256(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L56
.L55:
	movq	176(%r15), %rcx
	leaq	280(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L57
	leaq	272(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L58
	.p2align 4,,10
	.p2align 3
.L59:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L59
.L58:
	movq	184(%r15), %rcx
	leaq	292(%rax), %rdx
	movzwl	%cx, %esi
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rsi, %rdi
	shrq	$4, %rsi
	subq	$1, %rdx
	andl	$15, %edi
	testq	%rsi, %rsi
	movzbl	(%rdi,%r14), %edi
	movb	%dil, (%rdx)
	jne	.L60
	leaq	288(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L61
	.p2align 4,,10
	.p2align 3
.L62:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rsi, %rdx
	jne	.L62
.L61:
	movq	%rcx, %rsi
	leaq	308(%rax), %rdx
	sarq	$16, %rsi
	movzwl	%si, %esi
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rsi, %rdi
	shrq	$4, %rsi
	subq	$1, %rdx
	andl	$15, %edi
	testq	%rsi, %rsi
	movzbl	(%rdi,%r14), %edi
	movb	%dil, (%rdx)
	jne	.L63
	leaq	304(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L64
	.p2align 4,,10
	.p2align 3
.L65:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rsi, %rdx
	jne	.L65
.L64:
	sarq	$32, %rcx
	leaq	324(%rax), %rdx
	movzwl	%cx, %ecx
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L66
	leaq	320(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L68
.L67:
	movq	200(%r15), %rcx
	leaq	344(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L69
	leaq	336(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L71
.L70:
	movq	192(%r15), %rcx
	leaq	360(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L72
	leaq	352(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L73
	.p2align 4,,10
	.p2align 3
.L74:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L74
.L73:
	movq	208(%r15), %rcx
	leaq	376(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L75:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L75
	leaq	368(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L76
	.p2align 4,,10
	.p2align 3
.L77:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L77
.L76:
	movq	216(%r15), %rcx
	leaq	392(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L78:
	movq	%rcx, %rsi
	shrq	$4, %rcx
	subq	$1, %rdx
	andl	$15, %esi
	testq	%rcx, %rcx
	movzbl	(%rsi,%r14), %esi
	movb	%sil, (%rdx)
	jne	.L78
	leaq	384(%rax), %rcx
	cmpq	%rcx, %rdx
	jbe	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	subq	$1, %rdx
	movb	$48, (%rdx)
	cmpq	%rcx, %rdx
	jne	.L80
.L79:
	leaq	.LC3(%rip), %rdi
	leaq	16(%rax), %rdx
	movq	%rax, -2384(%rbp)
	movq	$22, -2392(%rbp)
	movq	$16, -2376(%rbp)
	movq	%rdi, -2400(%rbp)
	leaq	.LC4(%rip), %rdi
	movq	%rdx, -2352(%rbp)
	leaq	32(%rax), %rdx
	movq	$8, -2360(%rbp)
	movq	$16, -2344(%rbp)
	movq	%rdi, -2368(%rbp)
	leaq	.LC5(%rip), %rdi
	movq	$8, -2328(%rbp)
	movq	%rdx, -2320(%rbp)
	leaq	48(%rax), %rdx
	movq	$16, -2312(%rbp)
	movq	%rdi, -2336(%rbp)
	leaq	.LC6(%rip), %rdi
	movq	$7, -2296(%rbp)
	movq	%rdx, -2288(%rbp)
	leaq	64(%rax), %rdx
	movq	$16, -2280(%rbp)
	movq	%rdi, -2304(%rbp)
	leaq	.LC7(%rip), %rdi
	movq	$8, -2264(%rbp)
	movq	%rdx, -2256(%rbp)
	leaq	80(%rax), %rdx
	movq	$16, -2248(%rbp)
	movq	%rdi, -2272(%rbp)
	leaq	.LC8(%rip), %rdi
	movq	$8, -2232(%rbp)
	movq	%rdx, -2224(%rbp)
	leaq	96(%rax), %rdx
	movq	$16, -2216(%rbp)
	movq	%rdi, -2240(%rbp)
	leaq	.LC9(%rip), %rdi
	movq	$7, -2200(%rbp)
	movq	%rdx, -2192(%rbp)
	leaq	112(%rax), %rdx
	movq	$16, -2184(%rbp)
	movq	%rdi, -2208(%rbp)
	leaq	.LC10(%rip), %rdi
	movq	$8, -2168(%rbp)
	movq	%rdx, -2160(%rbp)
	movq	$16, -2152(%rbp)
	leaq	128(%rax), %rdx
	movq	%rdi, -2176(%rbp)
	leaq	.LC11(%rip), %rdi
	movq	%rdi, -2144(%rbp)
	leaq	.LC12(%rip), %rdi
	movq	%rdx, -2128(%rbp)
	leaq	144(%rax), %rdx
	movq	$8, -2136(%rbp)
	movq	$16, -2120(%rbp)
	movq	%rdi, -2112(%rbp)
	leaq	.LC13(%rip), %rdi
	movq	$7, -2104(%rbp)
	movq	%rdx, -2096(%rbp)
	leaq	160(%rax), %rdx
	movq	$16, -2088(%rbp)
	movq	%rdi, -2080(%rbp)
	leaq	.LC14(%rip), %rdi
	movq	$8, -2072(%rbp)
	movq	%rdx, -2064(%rbp)
	leaq	176(%rax), %rdx
	movq	$16, -2056(%rbp)
	movq	%rdi, -2048(%rbp)
	leaq	.LC15(%rip), %rdi
	movq	$8, -2040(%rbp)
	movq	%rdx, -2032(%rbp)
	leaq	192(%rax), %rdx
	movq	$16, -2024(%rbp)
	movq	%rdi, -2016(%rbp)
	leaq	.LC16(%rip), %rdi
	movq	$7, -2008(%rbp)
	movq	%rdx, -2000(%rbp)
	leaq	208(%rax), %rdx
	movq	$16, -1992(%rbp)
	movq	%rdi, -1984(%rbp)
	leaq	.LC17(%rip), %rdi
	movq	$8, -1976(%rbp)
	movq	%rdx, -1968(%rbp)
	leaq	224(%rax), %rdx
	movq	$16, -1960(%rbp)
	movq	%rdi, -1952(%rbp)
	leaq	.LC18(%rip), %rdi
	movq	$8, -1944(%rbp)
	movq	%rdx, -1936(%rbp)
	leaq	240(%rax), %rdx
	movq	$16, -1928(%rbp)
	movq	%rdi, -1920(%rbp)
	leaq	.LC19(%rip), %rdi
	movq	$7, -1912(%rbp)
	movq	%rdx, -1904(%rbp)
	leaq	256(%rax), %rdx
	movq	$16, -1896(%rbp)
	movq	%rdi, -1888(%rbp)
	leaq	.LC20(%rip), %rdi
	movq	$8, -1880(%rbp)
	movq	%rdx, -1872(%rbp)
	leaq	272(%rax), %rdx
	movq	$16, -1864(%rbp)
	movq	%rdi, -1856(%rbp)
	leaq	.LC21(%rip), %rdi
	movq	$11, -1848(%rbp)
	movq	%rdx, -1840(%rbp)
	leaq	288(%rax), %rdx
	movq	$8, -1832(%rbp)
	movq	%rdi, -1824(%rbp)
	leaq	.LC22(%rip), %rdi
	movq	$7, -1816(%rbp)
	movq	%rdx, -1808(%rbp)
	leaq	304(%rax), %rdx
	movq	$4, -1800(%rbp)
	movq	%rdi, -1792(%rbp)
	leaq	.LC23(%rip), %rdi
	movq	$7, -1784(%rbp)
	movq	%rdx, -1776(%rbp)
	leaq	320(%rax), %rdx
	movq	$4, -1768(%rbp)
	movq	%rdi, -1760(%rbp)
	leaq	.LC24(%rip), %rdi
	movq	$7, -1752(%rbp)
	movq	%rdx, -1744(%rbp)
	leaq	336(%rax), %rdx
	movq	$4, -1736(%rbp)
	movq	%rdi, -1728(%rbp)
	leaq	.LC25(%rip), %rdi
	movq	$9, -1720(%rbp)
	movq	%rdx, -1712(%rbp)
	leaq	352(%rax), %rdx
	movq	$8, -1704(%rbp)
	movq	%rdi, -1696(%rbp)
	leaq	.LC26(%rip), %rdi
	movq	$10, -1688(%rbp)
	movq	%rdx, -1680(%rbp)
	leaq	368(%rax), %rdx
	addq	$384, %rax
	movq	%rdi, -1664(%rbp)
	leaq	.LC27(%rip), %rdi
	movq	$8, -1672(%rbp)
	movq	$12, -1656(%rbp)
	movq	%rdx, -1648(%rbp)
	movq	$8, -1640(%rbp)
	movq	%rdi, -1632(%rbp)
	movq	$8, -1624(%rbp)
	movq	%rax, -1616(%rbp)
	movq	224(%r15), %rsi
	movq	$8, -1608(%rbp)
	testq	%rsi, %rsi
	je	.L186
	leaq	-3552(%rbp), %r8
	movzwl	(%rsi), %edx
	leaq	8(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L82
	cmpq	%r8, %rax
	jbe	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%r8, %rax
	jne	.L84
.L83:
	movzwl	2(%rsi), %edx
	leaq	16(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L85
	leaq	8(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L86
	.p2align 4,,10
	.p2align 3
.L87:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L87
.L86:
	movzwl	4(%rsi), %edx
	leaq	24(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L88
	leaq	16(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L90
.L89:
	movq	8(%rsi), %rdx
	leaq	32(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L91
	leaq	24(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L93
.L92:
	movq	16(%rsi), %rdx
	leaq	40(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L94
	leaq	32(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L96
.L95:
	leaq	.LC28(%rip), %rax
	movzwl	40(%rsi), %edx
	movq	$10, -1592(%rbp)
	movq	%r8, -1584(%rbp)
	movq	$8, -1576(%rbp)
	movq	%rax, -1600(%rbp)
	leaq	.LC29(%rip), %rax
	movq	$10, -1560(%rbp)
	movq	$8, -1544(%rbp)
	movq	$8, -1528(%rbp)
	movq	%rax, -1568(%rbp)
	leaq	8(%r8), %rax
	movq	$8, -1512(%rbp)
	movq	$7, -1496(%rbp)
	movq	$8, -1480(%rbp)
	movq	%rax, -1552(%rbp)
	leaq	.LC30(%rip), %rax
	movq	$8, -1464(%rbp)
	movq	$8, -1448(%rbp)
	movq	%rax, -1536(%rbp)
	leaq	16(%r8), %rax
	movq	%rax, -1520(%rbp)
	leaq	.LC31(%rip), %rax
	movq	%rax, -1504(%rbp)
	leaq	24(%r8), %rax
	movq	%rax, -1488(%rbp)
	leaq	.LC32(%rip), %rax
	movq	%rax, -1472(%rbp)
	leaq	32(%r8), %rax
	movq	%rax, -1456(%rbp)
	leaq	48(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L97
	leaq	40(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L98
	.p2align 4,,10
	.p2align 3
.L99:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L99
.L98:
	movslq	36(%rsi), %rdx
	leaq	56(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L100
	leaq	48(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L102
.L101:
	movslq	32(%rsi), %rdx
	leaq	64(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L103
	leaq	56(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L105
.L104:
	movzwl	56(%rsi), %edx
	leaq	72(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L106
	leaq	64(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L108
.L107:
	movslq	52(%rsi), %rdx
	leaq	80(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L109:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L109
	leaq	72(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L111
.L110:
	movslq	48(%rsi), %rdx
	leaq	88(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L112:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L112
	leaq	80(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L114
.L113:
	movzwl	72(%rsi), %edx
	leaq	96(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L115
	leaq	88(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L116
	.p2align 4,,10
	.p2align 3
.L117:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L117
.L116:
	movslq	68(%rsi), %rdx
	leaq	104(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L118:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L118
	leaq	96(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L119
	.p2align 4,,10
	.p2align 3
.L120:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L120
.L119:
	movslq	64(%rsi), %rdx
	leaq	112(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L121
	leaq	104(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L123
.L122:
	movzwl	88(%rsi), %edx
	leaq	120(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L124
	leaq	112(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L125
	.p2align 4,,10
	.p2align 3
.L126:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L126
.L125:
	movslq	84(%rsi), %rdx
	leaq	128(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L127
	leaq	120(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L129
.L128:
	movslq	80(%rsi), %rdx
	leaq	136(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L130
	leaq	128(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L132
.L131:
	movzwl	104(%rsi), %edx
	leaq	144(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L133:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L133
	leaq	136(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L134
	.p2align 4,,10
	.p2align 3
.L135:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L135
.L134:
	movslq	100(%rsi), %rdx
	leaq	152(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L136
	leaq	144(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L138
.L137:
	movslq	96(%rsi), %rdx
	leaq	160(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L139:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L139
	leaq	152(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L140
	.p2align 4,,10
	.p2align 3
.L141:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L141
.L140:
	movzwl	120(%rsi), %edx
	leaq	168(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L142
	leaq	160(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L143
	.p2align 4,,10
	.p2align 3
.L144:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L144
.L143:
	movslq	116(%rsi), %rdx
	leaq	176(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L145
	leaq	168(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L146
	.p2align 4,,10
	.p2align 3
.L147:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L147
.L146:
	movslq	112(%rsi), %rdx
	leaq	184(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L148
	leaq	176(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L149
	.p2align 4,,10
	.p2align 3
.L150:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L150
.L149:
	movzwl	136(%rsi), %edx
	leaq	192(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L151:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L151
	leaq	184(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L152
	.p2align 4,,10
	.p2align 3
.L153:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L153
.L152:
	movslq	132(%rsi), %rdx
	leaq	200(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L154
	leaq	192(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L155
	.p2align 4,,10
	.p2align 3
.L156:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L156
.L155:
	movslq	128(%rsi), %rdx
	leaq	208(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L157
	leaq	200(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L158
	.p2align 4,,10
	.p2align 3
.L159:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L159
.L158:
	movzwl	152(%rsi), %edx
	leaq	216(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L160:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L160
	leaq	208(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L161
	.p2align 4,,10
	.p2align 3
.L162:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L162
.L161:
	movslq	148(%rsi), %rdx
	leaq	224(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L163
	leaq	216(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L165
.L164:
	movslq	144(%rsi), %rdx
	leaq	232(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L166
	leaq	224(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L167
	.p2align 4,,10
	.p2align 3
.L168:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L168
.L167:
	movl	24(%rsi), %edx
	leaq	236(%r8), %rax
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L169
	leaq	232(%r8), %rdx
	cmpq	%rdx, %rax
	jbe	.L170
	.p2align 4,,10
	.p2align 3
.L171:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rdx, %rax
	jne	.L171
.L170:
	leaq	-2912(%rbp), %r10
	addq	$160, %rsi
	leaq	512(%r10), %r11
	movq	%r10, %r9
	.p2align 4,,10
	.p2align 3
.L175:
	movl	12(%rsi), %edx
	movl	8(%rsi), %eax
	movq	%r9, %rdi
	addq	$32, %r9
	sall	$16, %eax
	sall	$24, %edx
	orl	%eax, %edx
	movl	4(%rsi), %eax
	orl	(%rsi), %edx
	sall	$8, %eax
	orl	%eax, %edx
	movq	%r9, %rax
	.p2align 4,,10
	.p2align 3
.L172:
	movq	%rdx, %rcx
	shrq	$4, %rdx
	subq	$1, %rax
	andl	$15, %ecx
	testq	%rdx, %rdx
	movzbl	(%rcx,%r14), %ecx
	movb	%cl, (%rax)
	jne	.L172
	cmpq	%rax, %rdi
	jnb	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	subq	$1, %rax
	movb	$48, (%rax)
	cmpq	%rax, %rdi
	jne	.L174
.L173:
	addq	$16, %rsi
	cmpq	%r9, %r11
	jne	.L175
	leaq	48(%r8), %rdx
	leaq	.LC33(%rip), %rax
	leaq	.LC35(%rip), %rdi
	movq	$9, -1432(%rbp)
	movq	$4, -1416(%rbp)
	movq	%rdx, -1392(%rbp)
	leaq	56(%r8), %rdx
	movq	%rax, -1440(%rbp)
	movq	%rdi, -1360(%rbp)
	leaq	40(%r8), %rax
	leaq	.LC36(%rip), %rdi
	movq	%rdx, -1376(%rbp)
	leaq	64(%r8), %rdx
	movq	$1, -1400(%rbp)
	movq	%rax, -1424(%rbp)
	movq	%rdi, -1280(%rbp)
	leaq	.LC34(%rip), %rax
	movq	%rdx, -1344(%rbp)
	leaq	72(%r8), %rdx
	leaq	.LC37(%rip), %rdi
	movq	%rax, -1408(%rbp)
	movq	%rax, -1328(%rbp)
	movq	%rdx, -1312(%rbp)
	leaq	80(%r8), %rdx
	movq	%rax, -1248(%rbp)
	movq	%rdi, -1200(%rbp)
	movq	$8, -1384(%rbp)
	leaq	.LC38(%rip), %rdi
	movq	%rdx, -1296(%rbp)
	leaq	88(%r8), %rdx
	movq	$8, -1368(%rbp)
	movq	$9, -1352(%rbp)
	movq	$4, -1336(%rbp)
	movq	%rdx, -1264(%rbp)
	leaq	96(%r8), %rdx
	movq	$1, -1320(%rbp)
	movq	$8, -1304(%rbp)
	movq	$8, -1288(%rbp)
	movq	%rdx, -1232(%rbp)
	leaq	104(%r8), %rdx
	movq	$8, -1272(%rbp)
	movq	$4, -1256(%rbp)
	movq	$1, -1240(%rbp)
	movq	%rdx, -1216(%rbp)
	leaq	112(%r8), %rdx
	movq	$8, -1224(%rbp)
	movq	$8, -1208(%rbp)
	movq	$9, -1192(%rbp)
	movq	%rdx, -1184(%rbp)
	leaq	120(%r8), %rdx
	movq	%rdi, -1120(%rbp)
	leaq	.LC39(%rip), %rdi
	movq	%rax, -1168(%rbp)
	movq	%rax, -1088(%rbp)
	movq	%rdx, -1152(%rbp)
	leaq	128(%r8), %rdx
	movq	%rax, -1008(%rbp)
	movq	%rdi, -1040(%rbp)
	leaq	.LC40(%rip), %rdi
	movq	%rax, -928(%rbp)
	movq	%rdx, -1136(%rbp)
	leaq	136(%r8), %rdx
	movq	$4, -1176(%rbp)
	movq	%rdi, -960(%rbp)
	movq	$1, -1160(%rbp)
	leaq	.LC41(%rip), %rdi
	movq	%rdx, -1104(%rbp)
	leaq	144(%r8), %rdx
	movq	$8, -1144(%rbp)
	movq	$8, -1128(%rbp)
	movq	$8, -1112(%rbp)
	movq	%rdx, -1072(%rbp)
	leaq	152(%r8), %rdx
	movq	$4, -1096(%rbp)
	movq	$1, -1080(%rbp)
	movq	$8, -1064(%rbp)
	movq	%rdx, -1056(%rbp)
	leaq	160(%r8), %rdx
	movq	$8, -1048(%rbp)
	movq	$9, -1032(%rbp)
	movq	$4, -1016(%rbp)
	movq	%rdx, -1024(%rbp)
	leaq	168(%r8), %rdx
	movq	$1, -1000(%rbp)
	movq	$8, -984(%rbp)
	movq	$8, -968(%rbp)
	movq	%rdx, -992(%rbp)
	leaq	176(%r8), %rdx
	movq	$8, -952(%rbp)
	movq	$4, -936(%rbp)
	movq	%rdx, -976(%rbp)
	leaq	184(%r8), %rdx
	movq	%rdx, -944(%rbp)
	movq	$1, -920(%rbp)
	leaq	192(%r8), %rdx
	movq	%rax, -848(%rbp)
	leaq	224(%r8), %rax
	movq	$8, -904(%rbp)
	movq	%rdx, -912(%rbp)
	leaq	200(%r8), %rdx
	movq	$8, -888(%rbp)
	movq	%rax, -816(%rbp)
	leaq	.LC42(%rip), %rax
	movq	%rdi, -880(%rbp)
	movq	%rdx, -896(%rbp)
	leaq	216(%r8), %rdx
	addq	$232, %r8
	movq	%rax, -800(%rbp)
	leaq	.LC43(%rip), %rax
	movq	$9, -872(%rbp)
	movq	%rdx, -864(%rbp)
	movq	%rdx, -832(%rbp)
	movl	$135, %edx
	movq	%rax, -768(%rbp)
	leaq	.LC44(%rip), %rax
	movq	$4, -856(%rbp)
	movq	$1, -840(%rbp)
	movq	$8, -824(%rbp)
	movq	%rax, -736(%rbp)
	leaq	.LC45(%rip), %rax
	movq	$8, -808(%rbp)
	movq	$9, -792(%rbp)
	movq	%r8, -784(%rbp)
	movq	%rax, -704(%rbp)
	leaq	.LC46(%rip), %rax
	movq	$4, -776(%rbp)
	movq	$9, -760(%rbp)
	movq	%r10, -752(%rbp)
	movq	%rax, -672(%rbp)
	leaq	.LC47(%rip), %rax
	movq	$32, -744(%rbp)
	movq	$8, -728(%rbp)
	movq	%r10, -720(%rbp)
	movq	$32, -712(%rbp)
	movq	$9, -696(%rbp)
	movq	%r10, -688(%rbp)
	movq	$32, -680(%rbp)
	movq	$8, -664(%rbp)
	movq	%r10, -656(%rbp)
	movq	%rax, -640(%rbp)
	leaq	.LC48(%rip), %rax
	movq	$32, -648(%rbp)
	movq	$9, -632(%rbp)
	movq	%r10, -624(%rbp)
	movq	%rax, -608(%rbp)
	leaq	.LC49(%rip), %rax
	movq	$32, -616(%rbp)
	movq	$8, -600(%rbp)
	movq	%r10, -592(%rbp)
	movq	%rax, -576(%rbp)
	leaq	.LC50(%rip), %rax
	movq	$32, -584(%rbp)
	movq	$9, -568(%rbp)
	movq	%r10, -560(%rbp)
	movq	%rax, -544(%rbp)
	leaq	.LC51(%rip), %rax
	movq	$32, -552(%rbp)
	movq	$8, -536(%rbp)
	movq	%r10, -528(%rbp)
	movq	%rax, -512(%rbp)
	leaq	.LC52(%rip), %rax
	movq	$32, -520(%rbp)
	movq	$9, -504(%rbp)
	movq	%r10, -496(%rbp)
	movq	%rax, -480(%rbp)
	leaq	.LC53(%rip), %rax
	movq	$32, -488(%rbp)
	movq	$8, -472(%rbp)
	movq	%r10, -464(%rbp)
	movq	%rax, -448(%rbp)
	leaq	.LC54(%rip), %rax
	movq	$32, -456(%rbp)
	movq	$9, -440(%rbp)
	movq	%r10, -432(%rbp)
	movq	%rax, -416(%rbp)
	leaq	.LC55(%rip), %rax
	movq	$32, -424(%rbp)
	movq	$8, -408(%rbp)
	movq	%r10, -400(%rbp)
	movq	$32, -392(%rbp)
	movq	%rax, -384(%rbp)
	leaq	.LC56(%rip), %rax
	movq	$9, -376(%rbp)
	movq	%r10, -368(%rbp)
	movq	%rax, -352(%rbp)
	leaq	.LC57(%rip), %rax
	movq	$32, -360(%rbp)
	movq	$8, -344(%rbp)
	movq	%r10, -336(%rbp)
	movq	%rax, -320(%rbp)
	leaq	.LC58(%rip), %rax
	movq	$32, -328(%rbp)
	movq	$9, -312(%rbp)
	movq	%r10, -304(%rbp)
	movq	%rax, -288(%rbp)
	movq	$32, -296(%rbp)
	movl	$134, %eax
	movq	$8, -280(%rbp)
	movq	%r10, -272(%rbp)
	movq	$32, -264(%rbp)
.L81:
	leaq	.LC2(%rip), %rdi
	salq	$4, %rax
	movq	%rbx, %rsi
	movq	$1, -2392(%rbp,%rax)
	movq	%rdi, -2400(%rbp,%rax)
	movl	%r12d, %edi
	call	writev@PLT
	leaq	.LC59(%rip), %rsi
	movl	$12, %edx
	movl	%r12d, %edi
	call	write@PLT
	subq	$2064, %rsp
	movl	$256, %esi
	leaq	15(%rsp), %r14
	andq	$-16, %r14
	movq	%r14, %rdi
	call	backtrace@PLT
	testl	%eax, %eax
	movq	168(%r15), %r8
	jle	.L176
	leaq	-16(%r8), %rsi
	movq	%r14, %rdx
	xorl	%ecx, %ecx
	addq	$16, %r8
	.p2align 4,,10
	.p2align 3
.L178:
	movq	(%rdx), %rdi
	cmpq	%rsi, %rdi
	jb	.L177
	cmpq	%r8, %rdi
	jbe	.L311
.L177:
	addl	$1, %ecx
	addq	$8, %rdx
	cmpl	%ecx, %eax
	jne	.L178
.L176:
	movl	%eax, %esi
	movl	%r12d, %edx
	movq	%r14, %rdi
	call	__backtrace_symbols_fd@PLT
	leaq	.LC60(%rip), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	open@PLT
	cmpl	$-1, %eax
	movl	%eax, %r15d
	je	.L179
	leaq	.LC61(%rip), %rsi
	movl	$14, %edx
	movl	%r12d, %edi
	call	write@PLT
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$256, %edx
	movq	%rbx, %rsi
	movl	%r15d, %edi
	call	read@PLT
	cmpq	$-1, %rax
	movq	%rax, %r14
	je	.L312
	testq	%rax, %rax
	jle	.L184
	.p2align 4,,10
	.p2align 3
.L182:
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	%r12d, %edi
	call	write@PLT
	cmpq	$-1, %rax
	jne	.L180
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	je	.L182
	jmp	.L180
.L4:
	movl	$2, %r12d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L312:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	je	.L180
.L184:
	movl	%r15d, %edi
	call	close@PLT
.L179:
	leaq	8(%rbx), %rdi
	movq	$0, -2400(%rbp)
	call	sigemptyset@PLT
	movq	%rbx, %rsi
	movl	%r13d, %edi
	xorl	%edx, %edx
	movl	$0, -2264(%rbp)
	call	sigaction@PLT
	movl	%r13d, %edi
	call	raise@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L311:
	subl	%ecx, %eax
	movq	%rdx, %r14
	jmp	.L176
.L186:
	movl	$51, %edx
	movl	$50, %eax
	jmp	.L81
	.size	catch_segfault, .-catch_segfault
	.section	.rodata.str1.1
.LC62:
	.string	"SEGFAULT_SIGNALS"
.LC63:
	.string	"SEGFAULT_USE_ALTSTACK"
.LC64:
	.string	"all"
.LC65:
	.string	"segv"
.LC66:
	.string	"SEGFAULT_OUTPUT_NAME"
.LC67:
	.string	"ill"
.LC68:
	.string	"bus"
.LC69:
	.string	"stkflt"
.LC70:
	.string	"abrt"
.LC71:
	.string	"fpe"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	install_handler, @function
install_handler:
	pushq	%r13
	pushq	%r12
	leaq	.LC62(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	leaq	32(%rsp), %rbp
	call	getenv@PLT
	leaq	8(%rbp), %rdi
	movq	%rax, %rbx
	leaq	catch_segfault(%rip), %rax
	movl	$4, 168(%rsp)
	movq	%rax, 32(%rsp)
	call	sigemptyset@PLT
	leaq	.LC63(%rip), %rdi
	orl	$268435456, 168(%rsp)
	call	getenv@PLT
	testq	%rax, %rax
	je	.L314
	movl	$16384, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L314
	xorl	%esi, %esi
	movq	%rsp, %rdi
	movq	%rax, (%rsp)
	movl	$0, 8(%rsp)
	movq	$16384, 16(%rsp)
	call	sigaltstack@PLT
	testl	%eax, %eax
	je	.L386
	.p2align 4,,10
	.p2align 3
.L314:
	testq	%rbx, %rbx
	je	.L387
	cmpb	$0, (%rbx)
	je	.L313
	leaq	.LC64(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcasecmp@PLT
	leaq	.LC65(%rip), %rsi
	movl	%eax, %r13d
	movq	%rbx, %rdi
	call	__strcasestr@PLT
	testl	%r13d, %r13d
	movq	%rax, %r12
	je	.L321
	testq	%rax, %rax
	je	.L381
	call	__ctype_b_loc@PLT
	cmpq	%r12, %rbx
	movq	(%rax), %rax
	je	.L323
	movsbq	-1(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L381
.L323:
	movsbq	4(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L381
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$11, %edi
	call	sigaction@PLT
.L381:
	leaq	.LC67(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcasestr@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L382
	call	__ctype_b_loc@PLT
	cmpq	%r12, %rbx
	movq	(%rax), %rax
	je	.L326
	movsbq	-1(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L382
.L326:
	movsbq	3(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L382
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$4, %edi
	call	sigaction@PLT
.L382:
	leaq	.LC68(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcasestr@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L383
	call	__ctype_b_loc@PLT
	cmpq	%r12, %rbx
	movq	(%rax), %rax
	je	.L329
	movsbq	-1(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L383
.L329:
	movsbq	3(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L383
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$7, %edi
	call	sigaction@PLT
.L383:
	leaq	.LC69(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcasestr@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L384
	call	__ctype_b_loc@PLT
	cmpq	%r12, %rbx
	movq	(%rax), %rax
	je	.L331
	movsbq	-1(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L384
.L331:
	movsbq	6(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L384
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$16, %edi
	call	sigaction@PLT
.L384:
	leaq	.LC70(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcasestr@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L385
	call	__ctype_b_loc@PLT
	cmpq	%r12, %rbx
	movq	(%rax), %rax
	je	.L333
	movsbq	-1(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L385
.L333:
	movsbq	4(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L385
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$6, %edi
	call	sigaction@PLT
.L385:
	leaq	.LC71(%rip), %rsi
	movq	%rbx, %rdi
	call	__strcasestr@PLT
	movq	%rax, %r12
	testq	%r12, %r12
	je	.L319
	call	__ctype_b_loc@PLT
	cmpq	%r12, %rbx
	movq	(%rax), %rax
	je	.L334
	movsbq	-1(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L319
.L334:
	movsbq	3(%r12), %rdx
	testb	$8, (%rax,%rdx,2)
	jne	.L319
.L343:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$8, %edi
	call	sigaction@PLT
.L319:
	leaq	.LC66(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L313
	cmpb	$0, (%rax)
	jne	.L388
.L313:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L388:
	movl	$6, %esi
	movq	%rax, %rdi
	call	access@PLT
	testl	%eax, %eax
	je	.L337
	addl	$1, %eax
	jne	.L313
	call	__errno_location@PLT
	cmpl	$2, (%rax)
	jne	.L313
.L337:
	movq	%rbx, %rdi
	call	__strdup@PLT
	movq	%rax, fname(%rip)
	jmp	.L313
.L386:
	orl	$134217728, 168(%rsp)
	jmp	.L314
.L387:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$11, %edi
	call	sigaction@PLT
	jmp	.L319
.L321:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$11, %edi
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$4, %edi
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$7, %edi
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$16, %edi
	call	sigaction@PLT
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$6, %edi
	call	sigaction@PLT
	jmp	.L343
	.size	install_handler, .-install_handler
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	install_handler
	.local	fname
	.comm	fname,8,8
