	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcrtomb_chk
	.type	__wcrtomb_chk, @function
__wcrtomb_chk:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	cmpq	%rcx, %rax
	ja	.L7
	jmp	__wcrtomb
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__wcrtomb_chk, .-__wcrtomb_chk
	.hidden	__wcrtomb
