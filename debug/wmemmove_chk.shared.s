	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wmemmove_chk
	.type	__wmemmove_chk, @function
__wmemmove_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	salq	$2, %rdx
	jmp	__GI_memmove
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__wmemmove_chk, .-__wmemmove_chk
