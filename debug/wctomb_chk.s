	.text
	.p2align 4,,15
	.globl	__wctomb_chk
	.type	__wctomb_chk, @function
__wctomb_chk:
	subq	$8, %rsp
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	cmpq	%rdx, %rax
	ja	.L5
	leaq	__wctomb_state(%rip), %rdx
	call	__wcrtomb
	addq	$8, %rsp
	ret
.L5:
	call	__chk_fail
	.size	__wctomb_chk, .-__wctomb_chk
	.hidden	__chk_fail
	.hidden	__wcrtomb
	.hidden	__wctomb_state
	.hidden	_nl_current_LC_CTYPE
