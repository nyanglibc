	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ppoll_chk
	.type	__ppoll_chk, @function
__ppoll_chk:
	shrq	$3, %r8
	cmpq	%rsi, %r8
	jb	.L7
	jmp	__GI_ppoll
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__ppoll_chk, .-__ppoll_chk
