	.text
	.p2align 4,,15
	.globl	__mbstowcs_chk
	.type	__mbstowcs_chk, @function
__mbstowcs_chk:
	subq	$40, %rsp
	cmpq	%rdx, %rcx
	movq	%rsi, 8(%rsp)
	jb	.L5
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %rsi
	movq	$0, 24(%rsp)
	call	__mbsrtowcs
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	call	__chk_fail
	.size	__mbstowcs_chk, .-__mbstowcs_chk
	.hidden	__chk_fail
	.hidden	__mbsrtowcs
