	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*** %s ***: terminated\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___fortify_fail
	.hidden	__GI___fortify_fail
	.type	__GI___fortify_fail, @function
__GI___fortify_fail:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__libc_message
	jmp	.L2
	.size	__GI___fortify_fail, .-__GI___fortify_fail
	.globl	__fortify_fail
	.set	__fortify_fail,__GI___fortify_fail
	.hidden	__libc_message
