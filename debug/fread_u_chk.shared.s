	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fread_unlocked_chk
	.type	__fread_unlocked_chk, @function
__fread_unlocked_chk:
.LFB68:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdx, %rbx
	movl	$4294967295, %eax
	orq	%rcx, %rdx
	subq	$24, %rsp
	imulq	%rcx, %rbx
	cmpq	%rax, %rdx
	jbe	.L2
	testq	%rbp, %rbp
	jne	.L17
.L2:
	cmpq	%rsi, %rbx
	ja	.L3
	testq	%rbx, %rbx
	jne	.L18
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rdi, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rdi
	movq	%rcx, 8(%rsp)
	call	__GI__IO_sgetn
	cmpq	%rax, %rbx
	movq	8(%rsp), %rcx
	je	.L5
	xorl	%edx, %edx
	addq	$24, %rsp
	divq	%rbp
	movq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%edx, %edx
	movq	%rbx, %rax
	divq	%rbp
	cmpq	%rcx, %rax
	je	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	call	__GI___chk_fail
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rcx, %rbx
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
.LFE68:
	.size	__fread_unlocked_chk, .-__fread_unlocked_chk
