	.text
	.p2align 4,,15
	.globl	__longjmp_chk
	.type	__longjmp_chk, @function
__longjmp_chk:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_longjmp_unwind@PLT
	movl	64(%rbp), %eax
	testl	%eax, %eax
	jne	.L6
.L2:
	testl	%ebx, %ebx
	movl	$1, %eax
	movq	%rbp, %rdi
	cmove	%eax, %ebx
	movl	%ebx, %esi
	call	____longjmp_chk
.L6:
	leaq	72(%rbp), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__sigprocmask
	jmp	.L2
	.size	__longjmp_chk, .-__longjmp_chk
	.hidden	__sigprocmask
	.hidden	____longjmp_chk
