	.text
	.p2align 4,,15
	.globl	___snprintf_chk
	.type	___snprintf_chk, @function
___snprintf_chk:
	subq	$216, %rsp
	testb	%al, %al
	movq	%r8, %r10
	movq	%r9, 72(%rsp)
	je	.L6
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L6:
	cmpq	%rsi, %rcx
	jb	.L9
	leaq	224(%rsp), %rax
	xorl	%r8d, %r8d
	testl	%edx, %edx
	setg	%r8b
	leaq	8(%rsp), %rcx
	movq	%r10, %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	addl	%r8d, %r8d
	movl	$40, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vsnprintf_internal
	addq	$216, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	call	__chk_fail
	.size	___snprintf_chk, .-___snprintf_chk
	.globl	__snprintf_chk
	.set	__snprintf_chk,___snprintf_chk
	.hidden	__chk_fail
	.hidden	__vsnprintf_internal
