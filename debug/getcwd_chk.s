	.text
	.p2align 4,,15
	.globl	__getcwd_chk
	.type	__getcwd_chk, @function
__getcwd_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	__getcwd
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__getcwd_chk, .-__getcwd_chk
	.hidden	__chk_fail
	.hidden	__getcwd
