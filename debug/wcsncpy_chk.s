	.text
	.p2align 4,,15
	.globl	__wcsncpy_chk
	.type	__wcsncpy_chk, @function
__wcsncpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	__wcsncpy@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wcsncpy_chk, .-__wcsncpy_chk
	.hidden	__chk_fail
