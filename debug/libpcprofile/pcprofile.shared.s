	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PCPROFILE_OUTPUT"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	install, @function
install:
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	subq	$16, %rsp
	call	getenv@PLT
	testq	%rax, %rax
	je	.L1
	cmpb	$0, (%rax)
	jne	.L18
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
.L18:
	movq	%rax, %rdi
	movl	$438, %edx
	xorl	%eax, %eax
	movl	$66, %esi
	call	open@PLT
	movl	%eax, fd(%rip)
	addl	$1, %eax
	je	.L1
	movl	$1, active(%rip)
	movl	$-558891000, 12(%rsp)
	leaq	12(%rsp), %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	call	__errno_location@PLT
	cmpl	$4, (%rax)
	jne	.L7
.L6:
	movl	fd(%rip), %edi
	movl	$4, %edx
	movq	%rbx, %rsi
	call	write@PLT
	cmpq	$-1, %rax
	je	.L19
	cmpq	$4, %rax
	je	.L1
.L7:
	movl	fd(%rip), %edi
	call	close@PLT
	movl	$-1, fd(%rip)
	movl	$0, active(%rip)
	jmp	.L1
	.size	install, .-install
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	install
	.section	.text.exit,"ax",@progbits
	.p2align 4,,15
	.type	uninstall, @function
uninstall:
	movl	active(%rip), %eax
	testl	%eax, %eax
	jne	.L22
	ret
.L22:
	movl	fd(%rip), %edi
	jmp	close@PLT
	.size	uninstall, .-uninstall
	.section	.dtors,"aw",@progbits
	.align 8
	.quad	uninstall
	.text
	.p2align 4,,15
	.globl	__cyg_profile_func_enter
	.type	__cyg_profile_func_enter, @function
__cyg_profile_func_enter:
	movl	active(%rip), %eax
	testl	%eax, %eax
	jne	.L29
	rep ret
	.p2align 4,,10
	.p2align 3
.L29:
	subq	$24, %rsp
	movl	$16, %edx
	movq	%rdi, (%rsp)
	movl	fd(%rip), %edi
	movq	%rsi, 8(%rsp)
	movq	%rsp, %rsi
	call	write@PLT
	addq	$24, %rsp
	ret
	.size	__cyg_profile_func_enter, .-__cyg_profile_func_enter
	.globl	__cyg_profile_func_exit
	.set	__cyg_profile_func_exit,__cyg_profile_func_enter
	.local	fd
	.comm	fd,4,4
	.local	active
	.comm	active,4,4
