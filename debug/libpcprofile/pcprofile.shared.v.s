	.file	"pcprofile.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/debug
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/debug/libpcprofile/pcprofile.shared.v.d
# -MF /run/asm/debug/libpcprofile/pcprofile.os.dt -MP
# -MT /run/asm/debug/libpcprofile/pcprofile.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libpcprofile -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h pcprofile.c -mtune=generic
# -march=x86-64
# -auxbase-strip /run/asm/debug/libpcprofile/pcprofile.shared.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PCPROFILE_OUTPUT"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	install, @function
install:
.LFB15:
	.cfi_startproc
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
# pcprofile.c:39:   const char *outfile = getenv ("PCPROFILE_OUTPUT");
	leaq	.LC0(%rip), %rdi	#,
# pcprofile.c:36: {
	subq	$16, %rsp	#,
	.cfi_def_cfa_offset 32
# pcprofile.c:39:   const char *outfile = getenv ("PCPROFILE_OUTPUT");
	call	getenv@PLT	#
# pcprofile.c:41:   if (outfile != NULL && *outfile != '\0')
	testq	%rax, %rax	# outfile
	je	.L1	#,
# pcprofile.c:41:   if (outfile != NULL && *outfile != '\0')
	cmpb	$0, (%rax)	#, *outfile_13
	jne	.L18	#,
.L1:
# pcprofile.c:63: }
	addq	$16, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 16
	popq	%rbx	#
	.cfi_def_cfa_offset 8
	ret
.L18:
	.cfi_restore_state
# pcprofile.c:43:       fd = open (outfile, O_RDWR | O_CREAT, 0666);
	movq	%rax, %rdi	# outfile,
	movl	$438, %edx	#,
	xorl	%eax, %eax	#
	movl	$66, %esi	#,
	call	open@PLT	#
	movl	%eax, fd(%rip)	# _2, fd
# pcprofile.c:45:       if (fd != -1)
	addl	$1, %eax	#, _2
	je	.L1	#,
# pcprofile.c:49: 	  active = 1;
	movl	$1, active(%rip)	#, active
# pcprofile.c:53: 	  word = 0xdeb00000 | sizeof (void *);
	movl	$-558891000, 12(%rsp)	#, word
	leaq	12(%rsp), %rbx	#, tmp98
	jmp	.L6	#
	.p2align 4,,10
	.p2align 3
.L19:
# pcprofile.c:54: 	  if (TEMP_FAILURE_RETRY (write (fd, &word, 4)) != 4)
	call	__errno_location@PLT	#
	cmpl	$4, (%rax)	#, *_5
	jne	.L7	#,
.L6:
# pcprofile.c:54: 	  if (TEMP_FAILURE_RETRY (write (fd, &word, 4)) != 4)
	movl	fd(%rip), %edi	# fd,
	movl	$4, %edx	#,
	movq	%rbx, %rsi	# tmp98,
	call	write@PLT	#
	cmpq	$-1, %rax	#, __result
	je	.L19	#,
# pcprofile.c:54: 	  if (TEMP_FAILURE_RETRY (write (fd, &word, 4)) != 4)
	cmpq	$4, %rax	#, __result
	je	.L1	#,
.L7:
# pcprofile.c:57: 	      close (fd);
	movl	fd(%rip), %edi	# fd,
	call	close@PLT	#
# pcprofile.c:58: 	      fd = -1;
	movl	$-1, fd(%rip)	#, fd
# pcprofile.c:59: 	      active = 0;
	movl	$0, active(%rip)	#, active
# pcprofile.c:63: }
	jmp	.L1	#
	.cfi_endproc
.LFE15:
	.size	install, .-install
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	install
	.section	.text.exit,"ax",@progbits
	.p2align 4,,15
	.type	uninstall, @function
uninstall:
.LFB16:
	.cfi_startproc
# pcprofile.c:70:   if (active)
	movl	active(%rip), %eax	# active,
	testl	%eax, %eax	#
	jne	.L22	#,
# pcprofile.c:72: }
	ret
.L22:
# pcprofile.c:71:     close (fd);
	movl	fd(%rip), %edi	# fd,
	jmp	close@PLT	#
	.cfi_endproc
.LFE16:
	.size	uninstall, .-uninstall
	.section	.dtors,"aw",@progbits
	.align 8
	.quad	uninstall
	.text
	.p2align 4,,15
	.globl	__cyg_profile_func_enter
	.type	__cyg_profile_func_enter, @function
__cyg_profile_func_enter:
.LFB17:
	.cfi_startproc
# pcprofile.c:80:   if (! active)
	movl	active(%rip), %eax	# active,
	testl	%eax, %eax	#
	jne	.L29	#,
	rep ret
	.p2align 4,,10
	.p2align 3
.L29:
# pcprofile.c:77: {
	subq	$24, %rsp	#,
	.cfi_def_cfa_offset 32
# pcprofile.c:88:   write (fd, buf, sizeof buf);
	movl	$16, %edx	#,
# pcprofile.c:85:   buf[0] = this_fn;
	movq	%rdi, (%rsp)	# this_fn, buf
# pcprofile.c:88:   write (fd, buf, sizeof buf);
	movl	fd(%rip), %edi	# fd,
# pcprofile.c:86:   buf[1] = call_site;
	movq	%rsi, 8(%rsp)	# call_site, buf
# pcprofile.c:88:   write (fd, buf, sizeof buf);
	movq	%rsp, %rsi	#, tmp91
	call	write@PLT	#
# pcprofile.c:89: }
	addq	$24, %rsp	#,
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE17:
	.size	__cyg_profile_func_enter, .-__cyg_profile_func_enter
	.globl	__cyg_profile_func_exit
	.set	__cyg_profile_func_exit,__cyg_profile_func_enter
	.local	fd
	.comm	fd,4,4
	.local	active
	.comm	active,4,4
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
