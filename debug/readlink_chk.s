	.text
	.p2align 4,,15
	.globl	__readlink_chk
	.type	__readlink_chk, @function
__readlink_chk:
	cmpq	%rcx, %rdx
	ja	.L7
	jmp	__readlink
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__readlink_chk, .-__readlink_chk
	.hidden	__chk_fail
	.hidden	__readlink
