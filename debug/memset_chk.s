        .text
.globl __memset_chk
.type __memset_chk,@function
.align 1<<4
__memset_chk:
 cmpq %rdx, %rcx
 jb __chk_fail
 jmp memset
.size __memset_chk,.-__memset_chk
