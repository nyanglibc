	.text
	.p2align 4,,15
	.globl	__getdomainname_chk
	.type	__getdomainname_chk, @function
__getdomainname_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	getdomainname
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__getdomainname_chk, .-__getdomainname_chk
	.hidden	__chk_fail
	.hidden	getdomainname
