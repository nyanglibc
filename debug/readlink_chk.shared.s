	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__readlink_chk
	.type	__readlink_chk, @function
__readlink_chk:
	cmpq	%rcx, %rdx
	ja	.L7
	jmp	__readlink
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__readlink_chk, .-__readlink_chk
	.hidden	__readlink
