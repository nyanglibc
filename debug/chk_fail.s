	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"buffer overflow detected"
	.text
	.p2align 4,,15
	.globl	__chk_fail
	.hidden	__chk_fail
	.type	__chk_fail, @function
__chk_fail:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__fortify_fail
	.size	__chk_fail, .-__chk_fail
	.hidden	__fortify_fail
