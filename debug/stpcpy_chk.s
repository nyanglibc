	.text
	.p2align 4,,15
	.globl	__stpcpy_chk
	.type	__stpcpy_chk, @function
__stpcpy_chk:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	strlen
	cmpq	%r12, %rax
	jnb	.L5
	leaq	1(%rax), %rdx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	movq	%rax, %rbx
	call	memcpy@PLT
	addq	$8, %rsp
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L5:
	call	__chk_fail
	.size	__stpcpy_chk, .-__stpcpy_chk
	.hidden	__chk_fail
	.hidden	strlen
