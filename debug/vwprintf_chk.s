	.text
	.p2align 4,,15
	.globl	__vwprintf_chk
	.type	__vwprintf_chk, @function
__vwprintf_chk:
.LFB68:
	xorl	%ecx, %ecx
	testl	%edi, %edi
	movq	stdout(%rip), %rdi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vfwprintf_internal
.LFE68:
	.size	__vwprintf_chk, .-__vwprintf_chk
	.hidden	__vfwprintf_internal
