	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcsnrtombs_chk
	.type	__wcsnrtombs_chk, @function
__wcsnrtombs_chk:
	cmpq	%rcx, %r9
	jb	.L7
	jmp	__wcsnrtombs
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__wcsnrtombs_chk, .-__wcsnrtombs_chk
	.hidden	__wcsnrtombs
