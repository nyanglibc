	.text
	.p2align 4,,15
	.type	backtrace_helper, @function
backtrace_helper:
.LFB49:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%rbx
	movl	16(%rsi), %edx
	movq	%rsi, %rbx
	cmpl	$-1, %edx
	jne	.L11
	addl	$1, %edx
	cmpl	20(%rbx), %edx
	movl	%edx, 16(%rbx)
	je	.L5
.L13:
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rdi, %r12
	call	_Unwind_GetIP@PLT
	movslq	16(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rax, (%rdx,%rcx,8)
	call	_Unwind_GetCFA@PLT
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L3
	movq	(%rbx), %rcx
	movslq	%edx, %rsi
	movq	(%rcx,%rsi,8), %rdi
	cmpq	%rdi, -8(%rcx,%rsi,8)
	je	.L12
.L3:
	addl	$1, %edx
	cmpl	20(%rbx), %edx
	movq	%rax, 8(%rbx)
	movl	%edx, 16(%rbx)
	jne	.L13
.L5:
	popq	%rbx
	movl	$5, %eax
	popq	%r12
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	cmpq	%rax, 8(%rbx)
	jne	.L3
	jmp	.L5
.LFE49:
	.size	backtrace_helper, .-backtrace_helper
	.p2align 4,,15
	.globl	__backtrace
	.hidden	__backtrace
	.type	__backtrace, @function
__backtrace:
.LFB50:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$32, %rsp
	testl	%esi, %esi
	movq	%rdi, -32(%rbp)
	movq	$0, -24(%rbp)
	movl	$-1, -16(%rbp)
	movl	%esi, -12(%rbp)
	jle	.L15
	leaq	-32(%rbp), %rsi
	leaq	backtrace_helper(%rip), %rdi
	call	_Unwind_Backtrace@PLT
	movl	-16(%rbp), %eax
	cmpl	$1, %eax
	jle	.L16
	movq	-32(%rbp), %rcx
	movslq	%eax, %rdx
	cmpq	$0, -8(%rcx,%rdx,8)
	je	.L20
.L14:
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$-1, %eax
	jne	.L14
.L15:
	xorl	%eax, %eax
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	subl	$1, %eax
	leave
	ret
.LFE50:
	.size	__backtrace, .-__backtrace
	.weak	backtrace
	.set	backtrace,__backtrace
