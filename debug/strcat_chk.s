	.text
	.p2align 4,,15
	.globl	__strcat_chk
	.type	__strcat_chk, @function
__strcat_chk:
	leaq	(%rdi,%rdx), %r10
	movq	%rdi, %rcx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	1(%rcx), %r8
	cmpb	$0, -1(%r8)
	je	.L11
	movq	%r8, %rcx
	movq	%r9, %rdx
.L3:
	cmpq	%rcx, %r10
	leaq	-1(%rdx), %r9
	jne	.L2
.L5:
	subq	$8, %rsp
	call	__chk_fail
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r8d, %r8d
	subq	$1, %rcx
	movzbl	(%rsi,%r8), %r9d
	testb	%r9b, %r9b
	movb	%r9b, 1(%rcx,%r8)
	je	.L12
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %r8
	cmpq	%r8, %rdx
	je	.L5
	movzbl	(%rsi,%r8), %r9d
	testb	%r9b, %r9b
	movb	%r9b, 1(%rcx,%r8)
	jne	.L6
.L12:
	movq	%rdi, %rax
	ret
	.size	__strcat_chk, .-__strcat_chk
	.hidden	__chk_fail
