	.text
	.p2align 4,,15
	.globl	__mbsnrtowcs_chk
	.type	__mbsnrtowcs_chk, @function
__mbsnrtowcs_chk:
	cmpq	%rcx, %r9
	jb	.L7
	jmp	__mbsnrtowcs
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__mbsnrtowcs_chk, .-__mbsnrtowcs_chk
	.hidden	__chk_fail
	.hidden	__mbsnrtowcs
