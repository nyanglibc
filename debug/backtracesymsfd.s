	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"+0x"
.LC1:
	.string	"-0x"
.LC2:
	.string	")"
.LC3:
	.string	"[0x"
.LC4:
	.string	"]\n"
.LC5:
	.string	"("
	.text
	.p2align 4,,15
	.globl	__backtrace_symbols_fd
	.hidden	__backtrace_symbols_fd
	.type	__backtrace_symbols_fd, @function
__backtrace_symbols_fd:
	testl	%esi, %esi
	jle	.L15
	pushq	%r15
	pushq	%r14
	leaq	.LC4(%rip), %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$312, %rsp
	leaq	96(%rsp), %r14
	movl	%edx, 44(%rsp)
	leaq	16(%r14), %rax
	movq	%rax, (%rsp)
	leal	-1(%rsi), %eax
	leaq	8(%rdi,%rax,8), %rax
	movq	%rax, 8(%rsp)
	leaq	88(%rsp), %rax
	movq	%rax, 24(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	160(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L8:
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	(%rbx), %rdi
	call	_dl_addr
	testl	%eax, %eax
	je	.L12
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L12
	cmpb	$0, (%rdi)
	je	.L12
	movq	%rdi, 160(%rsp)
	call	strlen
	movq	144(%rsp), %rdi
	movq	%rax, 168(%rsp)
	testq	%rdi, %rdi
	je	.L19
	leaq	.LC5(%rip), %rax
	movq	%rdi, 192(%rsp)
	movq	$1, 184(%rsp)
	movl	$8, %r12d
	movl	$7, %ebp
	movl	$9, %r13d
	movq	%rax, 176(%rsp)
	call	strlen
	movq	152(%rsp), %rdi
	movq	%rax, 200(%rsp)
	movl	$6, %r8d
	movq	$5, 64(%rsp)
	movq	$4, 56(%rsp)
	movl	$3, %eax
.L9:
	movq	(%rbx), %rdx
	movq	%rax, %rcx
	salq	$4, %rcx
	cmpq	%rdi, %rdx
	jnb	.L20
	leaq	.LC1(%rip), %rsi
	subq	%rdx, %rdi
	movq	%rsi, 160(%rsp,%rcx)
.L7:
	movq	48(%rsp), %rsi
	salq	$4, %rax
	xorl	%ecx, %ecx
	movl	$16, %edx
	movq	%r8, 72(%rsp)
	movq	$3, 168(%rsp,%rax)
	call	_itoa_word
	movq	56(%rsp), %rdx
	movq	48(%rsp), %rcx
	movq	72(%rsp), %r8
	salq	$4, %rdx
	subq	%rax, %rcx
	movq	%rax, 160(%rsp,%rdx)
	movq	64(%rsp), %rax
	movq	%rcx, 168(%rsp,%rdx)
	leaq	.LC2(%rip), %rcx
	salq	$4, %rax
	movq	%rcx, 160(%rsp,%rax)
	movq	$1, 168(%rsp,%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$2, %r12d
	movl	$1, %ebp
	movl	$3, %r13d
	xorl	%r8d, %r8d
.L3:
	movq	(%rbx), %rdi
	movq	%r8, %rax
	leaq	.LC3(%rip), %rdx
	salq	$4, %rax
	leaq	16(%r14), %rsi
	xorl	%ecx, %ecx
	movq	%rdx, 160(%rsp,%rax)
	movl	$16, %edx
	movq	$3, 168(%rsp,%rax)
	call	_itoa_word
	movq	(%rsp), %rcx
	movq	32(%rsp), %rsi
	salq	$4, %rbp
	movl	44(%rsp), %edi
	salq	$4, %r12
	movl	%r13d, %edx
	movq	%rax, 160(%rsp,%rbp)
	addq	$8, %rbx
	movq	%r15, 160(%rsp,%r12)
	subq	%rax, %rcx
	movq	%rcx, 168(%rsp,%rbp)
	movq	$2, 168(%rsp,%r12)
	call	__writev
	cmpq	8(%rsp), %rbx
	jne	.L8
	addq	$312, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC0(%rip), %rsi
	subq	%rdi, %rdx
	movq	%rdx, %rdi
	movq	%rsi, 160(%rsp,%rcx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L19:
	movq	88(%rsp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L21
	leaq	.LC5(%rip), %rax
	movq	$1, 184(%rsp)
	movq	%rdi, 152(%rsp)
	movl	$7, %r12d
	movl	$6, %ebp
	movl	$8, %r13d
	movq	%rax, 176(%rsp)
	movl	$5, %r8d
	movq	$4, 64(%rsp)
	movq	$3, 56(%rsp)
	movl	$2, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$3, %r12d
	movl	$2, %ebp
	movl	$4, %r13d
	movl	$1, %r8d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	rep ret
	.size	__backtrace_symbols_fd, .-__backtrace_symbols_fd
	.weak	backtrace_symbols_fd
	.set	backtrace_symbols_fd,__backtrace_symbols_fd
	.hidden	__writev
	.hidden	_itoa_word
	.hidden	strlen
	.hidden	_dl_addr
