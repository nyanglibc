	.text
	.p2align 4,,15
	.globl	__vswprintf_chk
	.type	__vswprintf_chk, @function
__vswprintf_chk:
	cmpq	%rsi, %rcx
	jb	.L9
	movq	%r8, %r10
	xorl	%r8d, %r8d
	testl	%edx, %edx
	setg	%r8b
	movq	%r9, %rcx
	movq	%r10, %rdx
	addl	%r8d, %r8d
	jmp	__vswprintf_internal
	.p2align 4,,10
	.p2align 3
.L9:
	subq	$8, %rsp
	call	__chk_fail
	.size	__vswprintf_chk, .-__vswprintf_chk
	.hidden	__chk_fail
	.hidden	__vswprintf_internal
