	.text
	.p2align 4,,15
	.globl	__readlinkat_chk
	.type	__readlinkat_chk, @function
__readlinkat_chk:
	cmpq	%r8, %rcx
	ja	.L7
	jmp	readlinkat
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__readlinkat_chk, .-__readlinkat_chk
	.hidden	__chk_fail
	.hidden	readlinkat
