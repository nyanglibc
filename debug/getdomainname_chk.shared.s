	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getdomainname_chk
	.type	__getdomainname_chk, @function
__getdomainname_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	__GI_getdomainname
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__getdomainname_chk, .-__getdomainname_chk
