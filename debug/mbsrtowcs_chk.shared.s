	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mbsrtowcs_chk
	.type	__mbsrtowcs_chk, @function
__mbsrtowcs_chk:
	cmpq	%rdx, %r8
	jb	.L7
	jmp	__mbsrtowcs
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__mbsrtowcs_chk, .-__mbsrtowcs_chk
	.hidden	__mbsrtowcs
