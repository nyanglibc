	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.__gets_chk
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__gets_chk
	.type	__gets_chk, @function
__gets_chk:
.LFB68:
	testq	%rsi, %rsi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L30
	movq	stdin@GOTPCREL(%rip), %r12
	movq	%rdi, %rbx
	movq	%rsi, %r13
	movq	(%r12), %rbp
	movl	0(%rbp), %edx
	andl	$32768, %edx
	je	.L31
	movq	%rbp, %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L32
.L9:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L11:
	xorl	%edx, %edx
	cmpl	$10, %eax
	je	.L13
	movq	(%r12), %rcx
	leaq	1(%rbx), %rsi
	xorl	%r8d, %r8d
	movl	(%rcx), %edx
	movl	%edx, %r14d
	andl	$-33, %edx
	movl	%edx, (%rcx)
	movb	%al, (%rbx)
	leaq	-1(%r13), %rdx
	movq	(%r12), %rdi
	movl	$10, %ecx
	andl	$32, %r14d
.LEHB0:
	call	__GI__IO_getline
	movq	(%r12), %rcx
	leaq	1(%rax), %rdx
	movl	(%rcx), %eax
	testb	$32, %al
	jne	.L14
	orl	%eax, %r14d
	movl	%r14d, (%rcx)
.L13:
	cmpq	%r13, %rdx
	jnb	.L33
	movb	$0, (%rbx,%rdx)
.L12:
	testl	$32768, 0(%rbp)
	jne	.L1
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L19
	subl	$1, (%rdi)
.L1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	136(%rbp), %rcx
	movq	%fs:16, %r14
	cmpq	%r14, 8(%rcx)
	je	.L24
#APP
# 40 "gets_chk.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L6:
	movq	136(%rbp), %rcx
	movq	(%r12), %rdi
	movq	%r14, 8(%rcx)
.L4:
	addl	$1, 4(%rcx)
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jb	.L9
.L32:
	call	__GI___uflow
	cmpl	$-1, %eax
	jne	.L11
.L14:
	xorl	%ebx, %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rbp, %rdi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rcx)
	je	.L6
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L33:
	call	__GI___chk_fail
.LEHE0:
.L26:
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L21
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L21
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L22
	subl	$1, (%rdi)
.L21:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.L30:
	call	__GI___chk_fail
.LEHE1:
.L22:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L21
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L21
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L26-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	__gets_chk, .-__gets_chk
	.section	.gnu.warning.__gets_chk
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning___gets_chk, @object
	.size	__evoke_link_warning___gets_chk, 57
__evoke_link_warning___gets_chk:
	.string	"the `gets' function is dangerous and should not be used."
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
