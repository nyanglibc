	.text
	.p2align 4,,15
	.globl	__wmemmove_chk
	.type	__wmemmove_chk, @function
__wmemmove_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	salq	$2, %rdx
	jmp	memmove
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wmemmove_chk, .-__wmemmove_chk
	.hidden	__chk_fail
	.hidden	memmove
