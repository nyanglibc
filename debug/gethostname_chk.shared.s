	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gethostname_chk
	.type	__gethostname_chk, @function
__gethostname_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	__gethostname
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__gethostname_chk, .-__gethostname_chk
	.hidden	__gethostname
