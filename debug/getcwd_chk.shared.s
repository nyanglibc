	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getcwd_chk
	.type	__getcwd_chk, @function
__getcwd_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	__GI___getcwd
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__getcwd_chk, .-__getcwd_chk
