	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___vprintf_chk
	.type	___vprintf_chk, @function
___vprintf_chk:
	movq	stdout@GOTPCREL(%rip), %rax
	xorl	%ecx, %ecx
	testl	%edi, %edi
	setg	%cl
	addl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	__vfprintf_internal
	.size	___vprintf_chk, .-___vprintf_chk
	.globl	__vprintf_chk
	.set	__vprintf_chk,___vprintf_chk
	.hidden	__vfprintf_internal
