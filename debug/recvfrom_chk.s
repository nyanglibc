	.text
	.p2align 4,,15
	.globl	__recvfrom_chk
	.type	__recvfrom_chk, @function
__recvfrom_chk:
.LFB16:
	movq	%rcx, %rax
	subq	$8, %rsp
	movl	%r8d, %ecx
	cmpq	%rax, %rdx
	movq	%r9, %r8
	movq	16(%rsp), %r9
	ja	.L5
	addq	$8, %rsp
	jmp	__recvfrom
.L5:
	call	__chk_fail
.LFE16:
	.size	__recvfrom_chk, .-__recvfrom_chk
	.hidden	__chk_fail
	.hidden	__recvfrom
