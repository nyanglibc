	.text
	.p2align 4,,15
	.globl	__stack_chk_fail_local
	.hidden	__stack_chk_fail_local
	.type	__stack_chk_fail_local, @function
__stack_chk_fail_local:
	subq	$8, %rsp
	call	__stack_chk_fail@PLT
	.size	__stack_chk_fail_local, .-__stack_chk_fail_local
