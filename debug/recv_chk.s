	.text
	.p2align 4,,15
	.globl	__recv_chk
	.type	__recv_chk, @function
__recv_chk:
.LFB16:
	cmpq	%rcx, %rdx
	ja	.L7
	movl	%r8d, %ecx
	jmp	__recv
.L7:
	subq	$8, %rsp
	call	__chk_fail
.LFE16:
	.size	__recv_chk, .-__recv_chk
	.hidden	__chk_fail
	.hidden	__recv
