        .text
.globl __wmemset_chk
.type __wmemset_chk,@function
.align 1<<4
__wmemset_chk:
 cmpq %rdx, %rcx
 jb __chk_fail
 jmp wmemset
.size __wmemset_chk,.-__wmemset_chk
