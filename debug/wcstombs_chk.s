	.text
	.p2align 4,,15
	.globl	__wcstombs_chk
	.type	__wcstombs_chk, @function
__wcstombs_chk:
	subq	$40, %rsp
	cmpq	%rdx, %rcx
	movq	%rsi, 8(%rsp)
	jb	.L5
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %rsi
	movq	$0, 24(%rsp)
	call	__wcsrtombs
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	call	__chk_fail
	.size	__wcstombs_chk, .-__wcstombs_chk
	.hidden	__chk_fail
	.hidden	__wcsrtombs
