	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcscpy_chk
	.type	__wcscpy_chk, @function
__wcscpy_chk:
	movq	%rdi, %r8
	xorl	%ecx, %ecx
	subq	%rsi, %r8
	sarq	$2, %r8
	leaq	-4(%rsi,%r8,4), %r9
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	movl	(%rsi,%rcx,4), %r8d
	movl	%r8d, 4(%r9,%rcx,4)
	addq	$1, %rcx
	testl	%r8d, %r8d
	je	.L9
.L3:
	cmpq	%rdx, %rcx
	jne	.L2
	subq	$8, %rsp
	call	__GI___chk_fail
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rdi, %rax
	ret
	.size	__wcscpy_chk, .-__wcscpy_chk
