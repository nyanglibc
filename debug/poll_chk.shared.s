	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__poll_chk
	.type	__poll_chk, @function
__poll_chk:
	shrq	$3, %rcx
	cmpq	%rsi, %rcx
	jb	.L7
	jmp	__GI___poll
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__poll_chk, .-__poll_chk
