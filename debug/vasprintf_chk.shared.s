	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vasprintf_chk
	.type	__vasprintf_chk, @function
__vasprintf_chk:
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	%rax, %rsi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vasprintf_internal
	.size	__vasprintf_chk, .-__vasprintf_chk
	.hidden	__vasprintf_internal
