	.text
	.p2align 4,,15
	.globl	__wcpncpy_chk
	.type	__wcpncpy_chk, @function
__wcpncpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	__wcpncpy@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wcpncpy_chk, .-__wcpncpy_chk
	.hidden	__chk_fail
