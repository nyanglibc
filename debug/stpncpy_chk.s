	.text
	.p2align 4,,15
	.globl	__stpncpy_chk
	.type	__stpncpy_chk, @function
__stpncpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	__stpncpy
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__stpncpy_chk, .-__stpncpy_chk
	.hidden	__chk_fail
	.hidden	__stpncpy
