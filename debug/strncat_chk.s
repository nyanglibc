	.text
	.p2align 4,,15
	.globl	__strncat_chk
	.type	__strncat_chk, @function
__strncat_chk:
	leaq	(%rdi,%rcx), %r11
	movq	%rdi, %r8
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	1(%r8), %r9
	cmpb	$0, -1(%r9)
	je	.L70
	movq	%r10, %rcx
	movq	%r9, %r8
.L3:
	cmpq	%r8, %r11
	leaq	-1(%rcx), %r10
	jne	.L2
.L8:
	subq	$8, %rsp
	call	__chk_fail
	.p2align 4,,10
	.p2align 3
.L70:
	cmpq	$3, %rdx
	leaq	-1(%r8), %rax
	jbe	.L4
	movzbl	(%rsi), %r9d
	testb	%r9b, %r9b
	movb	%r9b, (%r8)
	je	.L34
	cmpq	$1, %rcx
	je	.L8
	movzbl	1(%rsi), %r9d
	testb	%r9b, %r9b
	movb	%r9b, 1(%r8)
	je	.L34
	cmpq	$2, %rcx
	je	.L8
	movzbl	2(%rsi), %r9d
	testb	%r9b, %r9b
	movb	%r9b, 2(%r8)
	je	.L34
	movq	%rdx, %r8
	andq	$-4, %r8
	addq	%rax, %r8
.L11:
	cmpq	$3, %rcx
	leaq	-4(%rcx), %r10
	je	.L8
	addq	$4, %rsi
	movzbl	-1(%rsi), %r9d
	addq	$4, %rax
	testb	%r9b, %r9b
	movb	%r9b, (%rax)
	je	.L34
	cmpq	%rax, %r8
	je	.L71
	testq	%r10, %r10
	je	.L8
	movzbl	(%rsi), %ecx
	testb	%cl, %cl
	movb	%cl, 1(%rax)
	je	.L34
	cmpq	$1, %r10
	je	.L8
	movzbl	1(%rsi), %ecx
	testb	%cl, %cl
	movb	%cl, 2(%rax)
	je	.L34
	cmpq	$2, %r10
	je	.L8
	movzbl	2(%rsi), %r9d
	movq	%r10, %rcx
	testb	%r9b, %r9b
	movb	%r9b, 3(%rax)
	jne	.L11
.L34:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	testq	%rdx, %rdx
	je	.L34
.L16:
	movzbl	(%rsi), %r9d
	leaq	1(%rsi), %rcx
	leaq	1(%rax), %r8
	testb	%r9b, %r9b
	movb	%r9b, 1(%rax)
	je	.L34
	addq	%rdx, %rsi
	leaq	1(%rax,%r10), %rdx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %rcx
	movzbl	-1(%rcx), %eax
	addq	$1, %r8
	testb	%al, %al
	movb	%al, (%r8)
	je	.L34
.L14:
	cmpq	%rsi, %rcx
	je	.L13
	subq	$1, %r10
	cmpq	%rdx, %r8
	jne	.L72
	jmp	.L8
.L13:
	testq	%r10, %r10
	je	.L8
	movb	$0, 1(%r8)
	jmp	.L34
.L71:
	andl	$3, %edx
	je	.L13
	testq	%r10, %r10
	je	.L8
	leaq	-5(%rcx), %r10
	jmp	.L16
	.size	__strncat_chk, .-__strncat_chk
	.hidden	__chk_fail
