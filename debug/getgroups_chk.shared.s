	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getgroups_chk
	.type	__getgroups_chk, @function
__getgroups_chk:
	testl	%edi, %edi
	js	.L6
	movslq	%edi, %rax
	salq	$2, %rax
	cmpq	%rdx, %rax
	ja	.L11
	jmp	__getgroups
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__getgroups_chk, .-__getgroups_chk
	.hidden	__getgroups
