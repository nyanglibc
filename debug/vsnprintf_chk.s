	.text
	.p2align 4,,15
	.globl	___vsnprintf_chk
	.type	___vsnprintf_chk, @function
___vsnprintf_chk:
	cmpq	%rsi, %rcx
	jb	.L9
	movq	%r8, %r10
	xorl	%r8d, %r8d
	testl	%edx, %edx
	setg	%r8b
	movq	%r9, %rcx
	movq	%r10, %rdx
	addl	%r8d, %r8d
	jmp	__vsnprintf_internal
	.p2align 4,,10
	.p2align 3
.L9:
	subq	$8, %rsp
	call	__chk_fail
	.size	___vsnprintf_chk, .-___vsnprintf_chk
	.globl	__vsnprintf_chk
	.set	__vsnprintf_chk,___vsnprintf_chk
	.hidden	__chk_fail
	.hidden	__vsnprintf_internal
