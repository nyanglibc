	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__strncpy_chk
	.type	__strncpy_chk, @function
__strncpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	__GI_strncpy
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__strncpy_chk, .-__strncpy_chk
