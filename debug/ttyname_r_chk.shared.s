	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ttyname_r_chk
	.type	__ttyname_r_chk, @function
__ttyname_r_chk:
	cmpq	%rcx, %rdx
	ja	.L7
	jmp	__ttyname_r
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__ttyname_r_chk, .-__ttyname_r_chk
	.hidden	__ttyname_r
