	.text
	.p2align 4,,15
	.globl	__confstr_chk
	.type	__confstr_chk, @function
__confstr_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	confstr
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__confstr_chk, .-__confstr_chk
	.hidden	__chk_fail
	.hidden	confstr
