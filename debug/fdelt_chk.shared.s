	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fdelt_chk
	.type	__fdelt_chk, @function
__fdelt_chk:
	cmpq	$1023, %rdi
	ja	.L7
	movq	%rdi, %rax
	sarq	$6, %rax
	ret
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__fdelt_chk, .-__fdelt_chk
	.globl	__fdelt_warn
	.set	__fdelt_warn,__fdelt_chk
