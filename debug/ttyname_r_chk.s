	.text
	.p2align 4,,15
	.globl	__ttyname_r_chk
	.type	__ttyname_r_chk, @function
__ttyname_r_chk:
	cmpq	%rcx, %rdx
	ja	.L7
	jmp	__ttyname_r
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__ttyname_r_chk, .-__ttyname_r_chk
	.hidden	__chk_fail
	.hidden	__ttyname_r
