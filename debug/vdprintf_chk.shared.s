	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vdprintf_chk
	.type	__vdprintf_chk, @function
__vdprintf_chk:
.LFB68:
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	%rax, %rsi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vdprintf_internal
.LFE68:
	.size	__vdprintf_chk, .-__vdprintf_chk
	.hidden	__vdprintf_internal
