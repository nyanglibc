	.text
	.p2align 4,,15
	.globl	__strcpy_chk
	.type	__strcpy_chk, @function
__strcpy_chk:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	strlen
	cmpq	%rbp, %rax
	jnb	.L5
	movq	%rbx, %rsi
	movq	%r12, %rdi
	leaq	1(%rax), %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	memcpy@PLT
.L5:
	call	__chk_fail
	.size	__strcpy_chk, .-__strcpy_chk
	.hidden	__chk_fail
	.hidden	strlen
