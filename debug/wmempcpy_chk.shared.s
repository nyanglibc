	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wmempcpy_chk
	.type	__wmempcpy_chk, @function
__wmempcpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	salq	$2, %rdx
	jmp	__GI_mempcpy@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__wmempcpy_chk, .-__wmempcpy_chk
