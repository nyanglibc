	.text
	.p2align 4,,15
	.globl	__pread_chk
	.type	__pread_chk, @function
__pread_chk:
.LFB15:
	cmpq	%r8, %rdx
	ja	.L7
	jmp	__pread
.L7:
	subq	$8, %rsp
	call	__chk_fail
.LFE15:
	.size	__pread_chk, .-__pread_chk
	.hidden	__chk_fail
	.hidden	__pread
