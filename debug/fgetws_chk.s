	.text
	.p2align 4,,15
	.globl	__fgetws_chk
	.type	__fgetws_chk, @function
__fgetws_chk:
.LFB71:
	testl	%edx, %edx
	jle	.L16
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %rbp
	pushq	%rbx
	movl	%edx, %ebx
	movl	(%rcx), %edx
	movq	%rsi, %r12
	movl	%edx, %ecx
	andl	$32768, %ecx
	jne	.L3
	movq	136(%rbp), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L4
#APP
# 31 "fgetws_chk.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L5
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L6:
	movq	136(%rbp), %rdi
	movl	0(%rbp), %edx
	movq	%r13, 8(%rdi)
.L4:
	addl	$1, 4(%rdi)
.L3:
	movl	%edx, %r13d
	andl	$-33, %edx
	movl	$1, %r8d
	movl	%edx, 0(%rbp)
	movslq	%ebx, %rdx
	andl	$32, %r13d
	subq	$1, %rdx
	movl	$10, %ecx
	movq	%r14, %rsi
	cmpq	%r12, %rdx
	movq	%rbp, %rdi
	cmova	%r12, %rdx
.LEHB0:
	call	_IO_getwline@PLT
	testq	%rax, %rax
	movl	0(%rbp), %edx
	jne	.L28
	xorl	%r8d, %r8d
.L9:
	orl	%edx, %r13d
	movl	%r13d, 0(%rbp)
	andl	$32768, %r13d
	jne	.L1
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L12
	subl	$1, (%rdi)
.L1:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	testb	$32, %dl
	je	.L10
	movq	__libc_errno@gottpoff(%rip), %rcx
	xorl	%r8d, %r8d
	cmpl	$11, %fs:(%rcx)
	jne	.L9
.L10:
	cmpq	%rax, %r12
	jbe	.L29
	movl	$0, (%r14,%rax,4)
	movq	%r14, %r8
	movl	0(%rbp), %edx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%r8d, %r8d
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L6
	call	__lll_lock_wait_private
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L29:
	call	__chk_fail
.LEHE0:
.L19:
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L14
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L14
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L15
	subl	$1, (%rdi)
.L14:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L15:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L14
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L14
.LFE71:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA71:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE71-.LLSDACSB71
.LLSDACSB71:
	.uleb128 .LEHB0-.LFB71
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L19-.LFB71
	.uleb128 0
	.uleb128 .LEHB1-.LFB71
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE71:
	.text
	.size	__fgetws_chk, .-__fgetws_chk
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__chk_fail
	.hidden	__lll_lock_wait_private
