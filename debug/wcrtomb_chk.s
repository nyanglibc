	.text
	.p2align 4,,15
	.globl	__wcrtomb_chk
	.type	__wcrtomb_chk, @function
__wcrtomb_chk:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	cmpq	%rcx, %rax
	ja	.L7
	jmp	__wcrtomb
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wcrtomb_chk, .-__wcrtomb_chk
	.hidden	__chk_fail
	.hidden	__wcrtomb
	.hidden	_nl_current_LC_CTYPE
