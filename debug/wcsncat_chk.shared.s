	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcsncat_chk
	.type	__wcsncat_chk, @function
__wcsncat_chk:
	movq	%rdi, %r10
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	4(%r10), %r9
	movl	-4(%r9), %eax
	testl	%eax, %eax
	je	.L71
	movq	%r8, %rcx
	movq	%r9, %r10
.L3:
	testq	%rcx, %rcx
	leaq	-1(%rcx), %r8
	jne	.L2
.L8:
	subq	$8, %rsp
	call	__GI___chk_fail
	.p2align 4,,10
	.p2align 3
.L71:
	cmpq	$3, %rdx
	leaq	-4(%r10), %r11
	jbe	.L4
	movl	(%rsi), %eax
	testl	%eax, %eax
	movl	%eax, (%r10)
	je	.L35
	cmpq	$1, %rcx
	je	.L8
	movl	4(%rsi), %eax
	testl	%eax, %eax
	movl	%eax, 4(%r10)
	je	.L35
	cmpq	$2, %rcx
	je	.L8
	movl	8(%rsi), %eax
	testl	%eax, %eax
	movl	%eax, 8(%r10)
	je	.L35
	movq	%rdx, %rax
	movq	%rcx, %r9
	andq	$-4, %rax
	subq	%rax, %r9
.L11:
	cmpq	$3, %rcx
	leaq	-4(%rcx), %r8
	je	.L8
	addq	$16, %rsi
	movl	-4(%rsi), %eax
	addq	$16, %r11
	testl	%eax, %eax
	movl	%eax, (%r11)
	je	.L35
	cmpq	%r8, %r9
	je	.L72
	testq	%r8, %r8
	je	.L8
	movl	(%rsi), %eax
	testl	%eax, %eax
	movl	%eax, 4(%r11)
	je	.L35
	cmpq	$1, %r8
	je	.L8
	movl	4(%rsi), %eax
	testl	%eax, %eax
	movl	%eax, 8(%r11)
	je	.L35
	cmpq	$2, %r8
	je	.L8
	movl	8(%rsi), %eax
	movq	%r8, %rcx
	testl	%eax, %eax
	movl	%eax, 12(%r11)
	jne	.L11
.L35:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	testq	%rdx, %rdx
	je	.L35
.L16:
	movl	(%rsi), %eax
	leaq	4(%rsi), %r9
	leaq	4(%r11), %rcx
	testl	%eax, %eax
	movl	%eax, 4(%r11)
	je	.L35
	leaq	1(%r8), %rax
	subq	%rdx, %rax
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$4, %r9
	movl	-4(%r9), %edx
	addq	$4, %rcx
	subq	$1, %r8
	testl	%edx, %edx
	movl	%edx, (%rcx)
	je	.L35
.L14:
	cmpq	%r8, %rax
	je	.L13
	testq	%r8, %r8
	jne	.L73
	jmp	.L8
.L18:
	movq	%r11, %rcx
.L13:
	testq	%r8, %r8
	je	.L8
	movl	$0, 4(%rcx)
	jmp	.L35
.L72:
	andl	$3, %edx
	je	.L18
	testq	%r8, %r8
	je	.L8
	leaq	-5(%rcx), %r8
	jmp	.L16
	.size	__wcsncat_chk, .-__wcsncat_chk
