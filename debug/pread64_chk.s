	.text
	.p2align 4,,15
	.globl	__pread64_chk
	.type	__pread64_chk, @function
__pread64_chk:
.LFB15:
	cmpq	%r8, %rdx
	ja	.L7
	jmp	__libc_pread64
.L7:
	subq	$8, %rsp
	call	__chk_fail
.LFE15:
	.size	__pread64_chk, .-__pread64_chk
	.hidden	__chk_fail
	.hidden	__libc_pread64
