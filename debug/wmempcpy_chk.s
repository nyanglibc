	.text
	.p2align 4,,15
	.globl	__wmempcpy_chk
	.type	__wmempcpy_chk, @function
__wmempcpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	salq	$2, %rdx
	jmp	__mempcpy@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wmempcpy_chk, .-__wmempcpy_chk
	.hidden	__chk_fail
