 .section .rodata.str1.1,"aMS",@progbits,1
 .type longjmp_msg,@object
longjmp_msg:
 .string "longjmp causes uninitialized stack frame"
 .size longjmp_msg, .-longjmp_msg
 .text
.globl ____longjmp_chk
.type ____longjmp_chk,@function
.align 1<<4
____longjmp_chk:
 mov (6*8)(%rdi), %r8
 mov (1*8)(%rdi),%r9
 mov (7*8)(%rdi), %rdx
 ror $2*8 +1, %r8; xor %fs:48, %r8
 ror $2*8 +1, %r9; xor %fs:48, %r9
 ror $2*8 +1, %rdx; xor %fs:48, %rdx
 cmp %r8, %rsp
 jbe .Lok
 movq %rdi, %r10
 movl %esi, %ebx
 xorl %edi, %edi
 lea -24(%rsp), %rsi
 movl $131, %eax
 syscall
 testl %eax, %eax
 jne .Lok2
 testl $1, (-24 + 8)(%rsp)
 jz .Lfail
 mov (-24 + 0)(%rsp), %rax
 add (-24 + 16)(%rsp), %rax
 sub %r8, %rax
 cmp (-24 + 16)(%rsp), %rax
 jae .Lok2
.Lfail: sub $8, %rsp
lea longjmp_msg(%rip), %rdi
call __fortify_fail
nop
.Lok2: movq %r10, %rdi
 movl %ebx, %esi
.Lok:

 movq (0*8)(%rdi), %rbx
 movq (2*8)(%rdi), %r12
 movq (3*8)(%rdi), %r13
 movq (4*8)(%rdi), %r14
 movq (5*8)(%rdi), %r15
 movl %esi, %eax
 mov %r8, %rsp
 movq %r9,%rbp

 jmpq *%rdx
.size ____longjmp_chk,.-____longjmp_chk
