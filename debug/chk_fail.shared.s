	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"buffer overflow detected"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___chk_fail
	.hidden	__GI___chk_fail
	.type	__GI___chk_fail, @function
__GI___chk_fail:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__GI___fortify_fail
	.size	__GI___chk_fail, .-__GI___chk_fail
	.globl	__chk_fail
	.set	__chk_fail,__GI___chk_fail
