	.text
	.p2align 4,,15
	.globl	__poll_chk
	.type	__poll_chk, @function
__poll_chk:
	shrq	$3, %rcx
	cmpq	%rsi, %rcx
	jb	.L7
	jmp	__poll
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__poll_chk, .-__poll_chk
	.hidden	__chk_fail
	.hidden	__poll
