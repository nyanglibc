	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"stack smashing detected"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__stack_chk_fail
	.type	__stack_chk_fail, @function
__stack_chk_fail:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__GI___fortify_fail
	.size	__stack_chk_fail, .-__stack_chk_fail
	.globl	__stack_chk_fail_local
	.set	__stack_chk_fail_local,__stack_chk_fail
