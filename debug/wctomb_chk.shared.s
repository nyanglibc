	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wctomb_chk
	.type	__wctomb_chk, @function
__wctomb_chk:
	subq	$8, %rsp
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	cmpq	%rdx, %rax
	ja	.L5
	leaq	__wctomb_state(%rip), %rdx
	call	__wcrtomb
	addq	$8, %rsp
	ret
.L5:
	call	__GI___chk_fail
	.size	__wctomb_chk, .-__wctomb_chk
	.hidden	__wcrtomb
	.hidden	__wctomb_state
