	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___sprintf_chk
	.type	___sprintf_chk, @function
___sprintf_chk:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, %r10
	movq	%r8, 64(%rsp)
	movq	%rcx, %rdx
	movq	%r9, 72(%rsp)
	je	.L6
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L6:
	xorl	%r8d, %r8d
	testl	%esi, %esi
	setg	%r8b
	testq	%r10, %r10
	leal	4(%r8,%r8), %r8d
	je	.L9
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	movq	%r10, %rsi
	movl	$32, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	call	__vsprintf_internal
	addq	$216, %rsp
	ret
.L9:
	call	__GI___chk_fail
	.size	___sprintf_chk, .-___sprintf_chk
	.globl	__sprintf_chk
	.set	__sprintf_chk,___sprintf_chk
	.hidden	__vsprintf_internal
