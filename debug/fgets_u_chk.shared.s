	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fgets_unlocked_chk
	.type	__fgets_unlocked_chk, @function
__fgets_unlocked_chk:
.LFB68:
	testl	%edx, %edx
	jle	.L7
	pushq	%r13
	pushq	%r12
	movslq	%edx, %rdx
	pushq	%rbp
	pushq	%rbx
	subq	$1, %rdx
	movq	%rcx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r13
	subq	$8, %rsp
	movl	(%rcx), %eax
	movl	$1, %r8d
	movl	%eax, %ebp
	andl	$-33, %eax
	andl	$32, %ebp
	cmpq	%rsi, %rdx
	movl	%eax, (%rcx)
	cmova	%rsi, %rdx
	movl	$10, %ecx
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	call	__GI__IO_getline
	movq	%rax, %rcx
	xorl	%eax, %eax
	movl	(%rbx), %edx
	testq	%rcx, %rcx
	je	.L4
	testb	$32, %dl
	je	.L5
	movq	__libc_errno@gottpoff(%rip), %rsi
	xorl	%eax, %eax
	cmpl	$11, %fs:(%rsi)
	je	.L5
.L4:
	orl	%edx, %ebp
	movl	%ebp, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	%rcx, %r12
	jbe	.L17
	movb	$0, 0(%r13,%rcx)
	movl	(%rbx), %edx
	movq	%r13, %rax
	orl	%edx, %ebp
	movl	%ebp, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
.L17:
	call	__GI___chk_fail
.LFE68:
	.size	__fgets_unlocked_chk, .-__fgets_unlocked_chk
