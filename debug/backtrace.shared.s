	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	dummy_getcfa, @function
dummy_getcfa:
.LFB49:
	pushq	%rbp
	xorl	%eax, %eax
	movq	%rsp, %rbp
	popq	%rbp
	ret
.LFE49:
	.size	dummy_getcfa, .-dummy_getcfa
	.p2align 4,,15
	.type	backtrace_helper, @function
backtrace_helper:
.LFB51:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%rbx
	movl	16(%rsi), %edx
	movq	%rsi, %rbx
	cmpl	$-1, %edx
	je	.L5
	movq	%rdi, %r12
	call	*unwind_getip(%rip)
	movslq	16(%rbx), %rcx
	movq	(%rbx), %rdx
	movq	%r12, %rdi
	movq	%rax, (%rdx,%rcx,8)
	call	*unwind_getcfa(%rip)
	movl	16(%rbx), %edx
	testl	%edx, %edx
	jle	.L6
	movq	(%rbx), %rcx
	movslq	%edx, %rsi
	movq	(%rcx,%rsi,8), %rdi
	cmpq	%rdi, -8(%rcx,%rsi,8)
	je	.L13
.L6:
	movq	%rax, 8(%rbx)
.L5:
	addl	$1, %edx
	cmpl	20(%rbx), %edx
	movl	%edx, 16(%rbx)
	je	.L8
	popq	%rbx
	xorl	%eax, %eax
	popq	%r12
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	cmpq	%rax, 8(%rbx)
	jne	.L6
.L8:
	popq	%rbx
	movl	$5, %eax
	popq	%r12
	popq	%rbp
	ret
.LFE51:
	.size	backtrace_helper, .-backtrace_helper
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
.LFB53:
	movq	libgcc_handle(%rip), %rdi
	movq	$0, unwind_backtrace(%rip)
	testq	%rdi, %rdi
	je	.L20
	pushq	%rbp
	movq	%rsp, %rbp
	call	__GI___libc_dlclose
	movq	$0, libgcc_handle(%rip)
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	rep ret
.LFE53:
	.size	free_mem, .-free_mem
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libgcc_s.so.1"
.LC1:
	.string	"_Unwind_Backtrace"
.LC2:
	.string	"_Unwind_GetIP"
.LC3:
	.string	"_Unwind_GetCFA"
	.text
	.p2align 4,,15
	.type	init, @function
init:
.LFB50:
	pushq	%rbp
	leaq	.LC0(%rip), %rdi
	movl	$-2147483646, %esi
	movq	%rsp, %rbp
	call	__GI___libc_dlopen_mode
	testq	%rax, %rax
	movq	%rax, libgcc_handle(%rip)
	je	.L23
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	call	__GI___libc_dlsym
	movq	libgcc_handle(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%rax, unwind_backtrace(%rip)
	call	__GI___libc_dlsym
	testq	%rax, %rax
	movq	%rax, unwind_getip(%rip)
	je	.L32
.L25:
	movq	libgcc_handle(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	__GI___libc_dlsym
	leaq	dummy_getcfa(%rip), %rdx
	testq	%rax, %rax
	cmove	%rdx, %rax
	movq	%rax, unwind_getcfa(%rip)
.L23:
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	$0, unwind_backtrace(%rip)
	jmp	.L25
.LFE50:
	.size	init, .-init
	.p2align 4,,15
	.globl	__GI___backtrace
	.hidden	__GI___backtrace
	.type	__GI___backtrace, @function
__GI___backtrace:
.LFB52:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$32, %rsp
	testl	%esi, %esi
	movq	%rdi, -32(%rbp)
	movq	$0, -24(%rbp)
	movl	$-1, -16(%rbp)
	movl	%esi, -12(%rbp)
	jle	.L34
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L35
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	init(%rip), %rsi
	leaq	once.8110(%rip), %rdi
#APP
# 111 "backtrace.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L36:
	movq	unwind_backtrace(%rip), %rax
	testq	%rax, %rax
	je	.L34
	leaq	-32(%rbp), %rsi
	leaq	backtrace_helper(%rip), %rdi
	call	*%rax
	movl	-16(%rbp), %eax
	cmpl	$1, %eax
	jle	.L37
	movq	-32(%rbp), %rcx
	movslq	%eax, %rdx
	cmpq	$0, -8(%rcx,%rdx,8)
	je	.L44
.L33:
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$-1, %eax
	jne	.L33
.L34:
	xorl	%eax, %eax
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	subl	$1, %eax
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	once.8110(%rip), %eax
	testl	%eax, %eax
	jne	.L36
	call	init
	orl	$2, once.8110(%rip)
	jmp	.L36
.LFE52:
	.size	__GI___backtrace, .-__GI___backtrace
	.weak	backtrace
	.set	backtrace,__GI___backtrace
	.globl	__backtrace
	.set	__backtrace,__GI___backtrace
	.local	once.8110
	.comm	once.8110,4,4
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	libgcc_handle
	.comm	libgcc_handle,8,8
	.local	unwind_getcfa
	.comm	unwind_getcfa,8,8
	.local	unwind_getip
	.comm	unwind_getip,8,8
	.local	unwind_backtrace
	.comm	unwind_backtrace,8,8
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
