	.text
	.p2align 4,,15
	.globl	__strncpy_chk
	.type	__strncpy_chk, @function
__strncpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	strncpy
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__strncpy_chk, .-__strncpy_chk
	.hidden	__chk_fail
	.hidden	strncpy
