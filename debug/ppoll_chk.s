	.text
	.p2align 4,,15
	.globl	__ppoll_chk
	.type	__ppoll_chk, @function
__ppoll_chk:
	shrq	$3, %r8
	cmpq	%rsi, %r8
	jb	.L7
	jmp	ppoll
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__ppoll_chk, .-__ppoll_chk
	.hidden	__chk_fail
	.hidden	ppoll
