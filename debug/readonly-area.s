	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"/proc/self/maps"
	.text
	.p2align 4,,15
	.globl	__readonly_area
	.type	__readonly_area, @function
__readonly_area:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	leaq	.LC1(%rip), %rdi
	leaq	.LC0(%rip), %rsi
	subq	$72, %rsp
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	je	.L40
	movq	%rax, %rbx
	movl	(%rax), %eax
	movl	%eax, %edx
	orb	$-128, %dh
	testb	$16, %al
	movl	%edx, (%rbx)
	movq	$0, 32(%rsp)
	movq	$0, 40(%rsp)
	jne	.L6
	leaq	32(%rsp), %rax
	leaq	40(%rsp), %r15
	leaq	(%r12,%rbp), %r13
	movq	%rax, 8(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L41:
	addq	%r12, %rbp
	subq	%rax, %rbp
.L11:
	testq	%rbp, %rbp
	je	.L6
.L8:
	testb	$16, (%rbx)
	jne	.L6
.L13:
	movq	8(%rsp), %rdi
	movq	%rbx, %rcx
	movl	$10, %edx
	movq	%r15, %rsi
	call	_IO_getdelim@PLT
	testq	%rax, %rax
	jle	.L6
	movq	16(%rsp), %rsi
	movq	32(%rsp), %rdi
	movl	$16, %edx
	call	strtoul
	movq	%rax, %r14
	movq	48(%rsp), %rax
	cmpq	32(%rsp), %rax
	je	.L6
	leaq	1(%rax), %rdi
	movq	%rdi, 48(%rsp)
	cmpb	$45, (%rax)
	jne	.L6
	movq	24(%rsp), %rsi
	movl	$16, %edx
	call	strtoul
	movq	56(%rsp), %rdx
	cmpq	48(%rsp), %rdx
	je	.L6
	leaq	1(%rdx), %rsi
	movq	%rsi, 56(%rsp)
	cmpb	$32, (%rdx)
	jne	.L6
	cmpq	%r14, %r13
	jbe	.L8
	cmpq	%rax, %r12
	jnb	.L8
	leaq	2(%rdx), %rsi
	movq	%rsi, 56(%rsp)
	cmpb	$114, 1(%rdx)
	jne	.L6
	leaq	3(%rdx), %rsi
	movq	%rsi, 56(%rsp)
	cmpb	$45, 2(%rdx)
	jne	.L6
	cmpq	%r14, %r12
	jb	.L17
	cmpq	%rax, %r13
	jbe	.L16
.L17:
	cmpq	%r14, %r12
	jnb	.L41
	cmpq	%rax, %r13
	ja	.L12
	subq	%r13, %rbp
	addq	%r14, %rbp
	jmp	.L11
.L16:
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbx, %rdi
	call	_IO_new_fclose@PLT
	movq	32(%rsp), %rdi
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L3
.L14:
	movl	$1, %eax
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	addq	%r14, %rbp
	subq	%rax, %rbp
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$2, %eax
	je	.L14
	cmpl	$13, %eax
	je	.L14
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__readonly_area, .-__readonly_area
	.hidden	strtoul
