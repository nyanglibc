	.text
	.p2align 4,,15
	.globl	__asprintf_chk
	.type	__asprintf_chk, @function
__asprintf_chk:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, %r10
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L5
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L5:
	leaq	224(%rsp), %rax
	xorl	%ecx, %ecx
	testl	%esi, %esi
	setg	%cl
	leaq	8(%rsp), %rdx
	movq	%r10, %rsi
	addl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$24, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vasprintf_internal
	addq	$216, %rsp
	ret
	.size	__asprintf_chk, .-__asprintf_chk
	.hidden	__vasprintf_internal
