	.text
	.p2align 4,,15
	.globl	__wcscat_chk
	.type	__wcscat_chk, @function
__wcscat_chk:
	subq	$1, %rdx
	movq	%rdi, %r8
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	4(%r8), %rcx
	movl	-4(%rcx), %eax
	leaq	-1(%rdx), %r9
	testl	%eax, %eax
	je	.L11
	movq	%r9, %rdx
	movq	%rcx, %r8
.L3:
	cmpq	$-1, %rdx
	jne	.L2
.L5:
	subq	$8, %rsp
	call	__chk_fail
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%ecx, %ecx
	subq	$4, %r8
	addq	$1, %rdx
	movl	(%rsi,%rcx,4), %r9d
	testl	%r9d, %r9d
	movl	%r9d, 4(%r8,%rcx,4)
	je	.L12
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %rcx
	cmpq	%rcx, %rdx
	je	.L5
	movl	(%rsi,%rcx,4), %r9d
	testl	%r9d, %r9d
	movl	%r9d, 4(%r8,%rcx,4)
	jne	.L6
.L12:
	movq	%rdi, %rax
	ret
	.size	__wcscat_chk, .-__wcscat_chk
	.hidden	__chk_fail
