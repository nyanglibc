	.text
	.p2align 4,,15
	.globl	___vprintf_chk
	.type	___vprintf_chk, @function
___vprintf_chk:
	xorl	%ecx, %ecx
	testl	%edi, %edi
	movq	stdout(%rip), %rdi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vfprintf_internal
	.size	___vprintf_chk, .-___vprintf_chk
	.globl	__vprintf_chk
	.set	__vprintf_chk,___vprintf_chk
	.hidden	__vfprintf_internal
