	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pread_chk
	.type	__pread_chk, @function
__pread_chk:
.LFB15:
	cmpq	%r8, %rdx
	ja	.L7
	jmp	__GI___pread
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
.LFE15:
	.size	__pread_chk, .-__pread_chk
