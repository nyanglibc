	.text
	.p2align 4,,15
	.globl	__wmemcpy_chk
	.type	__wmemcpy_chk, @function
__wmemcpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	salq	$2, %rdx
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wmemcpy_chk, .-__wmemcpy_chk
	.hidden	__chk_fail
