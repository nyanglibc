	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wcsncpy_chk
	.type	__wcsncpy_chk, @function
__wcsncpy_chk:
	cmpq	%rdx, %rcx
	jb	.L7
	jmp	__wcsncpy@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__wcsncpy_chk, .-__wcsncpy_chk
