	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pread64_chk
	.type	__pread64_chk, @function
__pread64_chk:
.LFB15:
	cmpq	%r8, %rdx
	ja	.L7
	jmp	__libc_pread64
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
.LFE15:
	.size	__pread64_chk, .-__pread64_chk
	.hidden	__libc_pread64
