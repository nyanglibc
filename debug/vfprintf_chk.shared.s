	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___vfprintf_chk
	.type	___vfprintf_chk, @function
___vfprintf_chk:
.LFB68:
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	%rax, %rsi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vfprintf_internal
.LFE68:
	.size	___vfprintf_chk, .-___vfprintf_chk
	.globl	__vfprintf_chk
	.set	__vfprintf_chk,___vfprintf_chk
	.hidden	__vfprintf_internal
