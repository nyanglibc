	.text
	.p2align 4,,15
	.globl	__wprintf_chk
	.type	__wprintf_chk, @function
__wprintf_chk:
.LFB68:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L5
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L5:
	leaq	224(%rsp), %rax
	xorl	%ecx, %ecx
	testl	%edi, %edi
	setg	%cl
	leaq	8(%rsp), %rdx
	addl	%ecx, %ecx
	movq	stdout(%rip), %rdi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vfwprintf_internal
	addq	$216, %rsp
	ret
.LFE68:
	.size	__wprintf_chk, .-__wprintf_chk
	.hidden	__vfwprintf_internal
