	.text
	.p2align 4,,15
	.globl	__wcpcpy_chk
	.type	__wcpcpy_chk, @function
__wcpcpy_chk:
	subq	%rdi, %rsi
	leaq	-4(%rdi), %rax
	xorl	%ecx, %ecx
	sarq	$2, %rsi
	leaq	4(,%rsi,4), %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	movl	(%rax,%rdi), %esi
	addq	$1, %rcx
	addq	$4, %rax
	testl	%esi, %esi
	movl	%esi, (%rax)
	je	.L9
.L3:
	cmpq	%rdx, %rcx
	jne	.L2
	subq	$8, %rsp
	call	__chk_fail
.L9:
	ret
	.size	__wcpcpy_chk, .-__wcpcpy_chk
	.hidden	__chk_fail
