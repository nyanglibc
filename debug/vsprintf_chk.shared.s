	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___vsprintf_chk
	.type	___vsprintf_chk, @function
___vsprintf_chk:
	movq	%rdx, %rax
	testl	%esi, %esi
	movq	%rcx, %rdx
	movq	%r8, %rcx
	setg	%r8b
	testq	%rax, %rax
	je	.L9
	movzbl	%r8b, %r8d
	movq	%rax, %rsi
	leal	4(%r8,%r8), %r8d
	jmp	__vsprintf_internal
.L9:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	___vsprintf_chk, .-___vsprintf_chk
	.globl	__vsprintf_chk
	.set	__vsprintf_chk,___vsprintf_chk
	.globl	__GI___vsprintf_chk
	.hidden	__GI___vsprintf_chk
	.set	__GI___vsprintf_chk,___vsprintf_chk
	.hidden	__vsprintf_internal
