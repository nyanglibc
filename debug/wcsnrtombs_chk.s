	.text
	.p2align 4,,15
	.globl	__wcsnrtombs_chk
	.type	__wcsnrtombs_chk, @function
__wcsnrtombs_chk:
	cmpq	%rcx, %r9
	jb	.L7
	jmp	__wcsnrtombs
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__wcsnrtombs_chk, .-__wcsnrtombs_chk
	.hidden	__chk_fail
	.hidden	__wcsnrtombs
