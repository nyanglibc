        .text
.globl __mempcpy_chk
.type __mempcpy_chk,@function
.align 1<<4
__mempcpy_chk:
 cmpq %rdx, %rcx
 jb __chk_fail
 jmp mempcpy
.size __mempcpy_chk,.-__mempcpy_chk
