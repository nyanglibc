	.text
	.p2align 4,,15
	.globl	__gethostname_chk
	.type	__gethostname_chk, @function
__gethostname_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	__gethostname
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__gethostname_chk, .-__gethostname_chk
	.hidden	__chk_fail
	.hidden	__gethostname
