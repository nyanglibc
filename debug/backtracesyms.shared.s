	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"%s(%s) [%p]"
.LC2:
	.string	"%s(%s%c%#tx) [%p]"
.LC3:
	.string	"[%p]"
.LC4:
	.string	"backtracesyms.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"last <= (char *) result + size * sizeof (char *) + total"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__backtrace_symbols
	.type	__backtrace_symbols, @function
__backtrace_symbols:
	pushq	%rbp
	movslq	%esi, %rax
	movq	%rax, %rdx
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	salq	$5, %rdx
	pushq	%rbx
	addq	$16, %rdx
	subq	$88, %rsp
	movq	%rdi, -80(%rbp)
	subq	%rdx, %rsp
	leaq	18(,%rax,4), %rdx
	salq	$3, %rax
	movq	%rsp, %rbx
	movq	%rax, -88(%rbp)
	andq	$-16, %rdx
	subq	%rdx, %rsp
	testl	%esi, %esi
	movq	%rsp, %r15
	movq	%r15, -104(%rbp)
	jle	.L2
	leal	-1(%rsi), %eax
	movq	%rbx, %r14
	movq	%rbx, -96(%rbp)
	movq	%rdi, %r13
	movq	%r15, %rbx
	xorl	%r12d, %r12d
	movq	%rax, -112(%rbp)
	leaq	8(%rdi,%rax,8), %rax
	movq	%rax, -72(%rbp)
	leaq	-56(%rbp), %rax
	movq	%rax, %r15
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L3
	cmpb	$0, (%rdi)
	je	.L3
	call	__GI_strlen
	movq	16(%r14), %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L4
	movq	%rdx, -120(%rbp)
	call	__GI_strlen
	movq	-120(%rbp), %rdx
.L4:
	leaq	43(%r12,%rdx), %r12
	addq	%rax, %r12
	movq	-56(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 8(%r14)
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %r13
	addq	$32, %r14
	addq	$4, %rbx
	cmpq	-72(%rbp), %r13
	je	.L36
.L6:
	movq	0(%r13), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	__GI__dl_addr
	testl	%eax, %eax
	movl	%eax, (%rbx)
	jne	.L37
.L3:
	addq	$21, %r12
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L36:
	movq	-88(%rbp), %rax
	movq	-96(%rbp), %rbx
	leaq	(%r12,%rax), %r13
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1
	movq	-88(%rbp), %r12
	movq	-112(%rbp), %rax
	xorl	%r15d, %r15d
	movq	%r13, -88(%rbp)
	addq	%r14, %r12
	addq	$1, %rax
	movq	%r12, %r13
	movq	-104(%rbp), %r12
	movq	%rax, -72(%rbp)
	.p2align 4,,10
	.p2align 3
.L15:
	movl	(%r12,%r15,4), %ecx
	movq	-80(%rbp), %rax
	movq	%r13, (%r14,%r15,8)
	testl	%ecx, %ecx
	movq	(%rax,%r15,8), %rdx
	je	.L8
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L8
	cmpb	$0, (%rax)
	je	.L8
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.L38
	movq	24(%rbx), %r9
.L10:
	cmpq	%rdx, %r9
	jbe	.L39
	subq	%rdx, %r9
	movl	$45, %r8d
.L13:
	leaq	.LC0(%rip), %rsi
	testq	%rcx, %rcx
	movq	%r13, %rdi
	cmove	%rsi, %rcx
	subq	$8, %rsp
	leaq	.LC2(%rip), %rsi
	pushq	%rdx
	movq	%rax, %rdx
	xorl	%eax, %eax
	addq	$1, %r15
	addq	$32, %rbx
	call	__GI_sprintf
	addl	$1, %eax
	cltq
	addq	%rax, %r13
	cmpq	%r15, -72(%rbp)
	popq	%rax
	popq	%rdx
	jne	.L15
.L41:
	movq	%r13, %r12
	movq	-88(%rbp), %r13
	leaq	(%r14,%r13), %rax
	cmpq	%rax, %r12
	ja	.L40
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC3(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__GI_sprintf
	addl	$1, %eax
	cltq
	addq	%rax, %r13
.L11:
	addq	$1, %r15
	addq	$32, %rbx
	cmpq	%r15, -72(%rbp)
	jne	.L15
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rdx, %rsi
	movl	$43, %r8d
	subq	%r9, %rsi
	movq	%rsi, %r9
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L38:
	movq	8(%rbx), %r9
	testq	%r9, %r9
	movq	%r9, 24(%rbx)
	jne	.L10
	leaq	.LC0(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rdx, %r8
	movq	%r13, %rdi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	__GI_sprintf
	addl	$1, %eax
	cltq
	addq	%rax, %r13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L2:
	movq	-88(%rbp), %rdi
	call	malloc@PLT
	movq	%rax, %r14
	jmp	.L1
.L40:
	leaq	__PRETTY_FUNCTION__.10015(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$115, %edx
	call	__GI___assert_fail
	.size	__backtrace_symbols, .-__backtrace_symbols
	.weak	backtrace_symbols
	.set	backtrace_symbols,__backtrace_symbols
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10015, @object
	.size	__PRETTY_FUNCTION__.10015, 20
__PRETTY_FUNCTION__.10015:
	.string	"__backtrace_symbols"
