	.text
	.p2align 4,,15
	.globl	__read_chk
	.type	__read_chk, @function
__read_chk:
.LFB46:
	cmpq	%rcx, %rdx
	ja	.L8
	xorl	%eax, %eax
#APP
# 33 "read_chk.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L8:
	subq	$8, %rsp
	call	__chk_fail
.LFE46:
	.size	__read_chk, .-__read_chk
	.hidden	__chk_fail
