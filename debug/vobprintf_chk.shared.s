	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__obstack_vprintf_chk
	.type	__obstack_vprintf_chk, @function
__obstack_vprintf_chk:
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	%rax, %rsi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__obstack_vprintf_internal
	.size	__obstack_vprintf_chk, .-__obstack_vprintf_chk
	.hidden	__obstack_vprintf_internal
