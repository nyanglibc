	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__realpath_chk
	.type	__realpath_chk, @function
__realpath_chk:
	cmpq	$4095, %rdx
	jbe	.L7
	jmp	__GI___realpath
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__realpath_chk, .-__realpath_chk
