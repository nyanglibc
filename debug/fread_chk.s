	.text
	.p2align 4,,15
	.globl	__fread_chk
	.type	__fread_chk, @function
__fread_chk:
.LFB68:
	pushq	%r15
	pushq	%r14
	movl	$4294967295, %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdx, %rbx
	orq	%rcx, %rdx
	movq	%rcx, %r13
	subq	$8, %rsp
	movq	%r8, %r12
	imulq	%rcx, %rbx
	cmpq	%rax, %rdx
	jbe	.L2
	testq	%rbp, %rbp
	jne	.L31
.L2:
	cmpq	%rsi, %rbx
	ja	.L3
	testq	%rbx, %rbx
	je	.L1
	movl	(%r12), %edx
	andl	$32768, %edx
	jne	.L5
	movq	136(%r12), %rdi
	movq	%fs:16, %r15
	cmpq	%r15, 8(%rdi)
	je	.L6
#APP
# 50 "fread_chk.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L8:
	movq	136(%r12), %rdi
	movq	%r15, 8(%rdi)
.L6:
	addl	$1, 4(%rdi)
.L5:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
.LEHB0:
	call	_IO_sgetn
.LEHE0:
	testl	$32768, (%r12)
	movq	%rax, %r8
	jne	.L12
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L12
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L14
	subl	$1, (%rdi)
.L12:
	cmpq	%r8, %rbx
	je	.L18
	movq	%r8, %rax
	xorl	%edx, %edx
	divq	%rbp
	movq	%rax, %rbx
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%edx, %edx
	movq	%rbx, %rax
	divq	%rbp
	cmpq	%rcx, %rax
	je	.L2
	.p2align 4,,10
	.p2align 3
.L3:
.LEHB1:
	call	__chk_fail
.LEHE1:
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r13, %rbx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L8
.LEHB2:
	call	__lll_lock_wait_private
.LEHE2:
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L12
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L12
.L19:
	testl	$32768, (%r12)
	movq	%rax, %r8
	jne	.L16
	movq	136(%r12), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L16
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L16:
	movq	%r8, %rdi
.LEHB3:
	call	_Unwind_Resume@PLT
.LEHE3:
.L17:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L16
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L16
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L19-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB2-.LFB68
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L19-.LFB68
	.uleb128 0
	.uleb128 .LEHB3-.LFB68
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	__fread_chk, .-__fread_chk
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__chk_fail
	.hidden	_IO_sgetn
