	.text
	.p2align 4,,15
	.globl	__cyg_profile_func_enter
	.type	__cyg_profile_func_enter, @function
__cyg_profile_func_enter:
	rep ret
	.size	__cyg_profile_func_enter, .-__cyg_profile_func_enter
	.globl	__cyg_profile_func_exit
	.set	__cyg_profile_func_exit,__cyg_profile_func_enter
