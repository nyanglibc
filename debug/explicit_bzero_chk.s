	.text
	.p2align 4,,15
	.globl	__explicit_bzero_chk
	.type	__explicit_bzero_chk, @function
__explicit_bzero_chk:
	subq	$8, %rsp
	cmpq	%rsi, %rdx
	jb	.L5
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	call	__chk_fail
	.size	__explicit_bzero_chk, .-__explicit_bzero_chk
	.globl	__explicit_bzero_chk_internal
	.hidden	__explicit_bzero_chk_internal
	.set	__explicit_bzero_chk_internal,__explicit_bzero_chk
	.hidden	__chk_fail
