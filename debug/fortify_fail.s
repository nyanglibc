	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"*** %s ***: terminated\n"
	.text
	.p2align 4,,15
	.globl	__fortify_fail
	.hidden	__fortify_fail
	.type	__fortify_fail, @function
__fortify_fail:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movl	$1, %edi
	xorl	%eax, %eax
	call	__libc_message
	jmp	.L2
	.size	__fortify_fail, .-__fortify_fail
	.hidden	__libc_message
