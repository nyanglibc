        .text
.globl __memcpy_chk
.type __memcpy_chk,@function
.align 1<<4
__memcpy_chk:
 cmpq %rdx, %rcx
 jb __chk_fail
 jmp memcpy
.size __memcpy_chk,.-__memcpy_chk
