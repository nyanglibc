	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.getwd
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__getwd_chk
	.type	__getwd_chk, @function
__getwd_chk:
	subq	$8, %rsp
	call	__GI___getcwd
	testq	%rax, %rax
	je	.L5
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$34, %fs:(%rdx)
	jne	.L1
	call	__GI___chk_fail
	.size	__getwd_chk, .-__getwd_chk
	.section	.gnu.warning.getwd
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getwd, @object
	.size	__evoke_link_warning_getwd, 58
__evoke_link_warning_getwd:
	.string	"the `getwd' function is dangerous and should not be used."
