	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__readlinkat_chk
	.type	__readlinkat_chk, @function
__readlinkat_chk:
	cmpq	%r8, %rcx
	ja	.L7
	jmp	__GI_readlinkat
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__readlinkat_chk, .-__readlinkat_chk
