	.text
	.p2align 4,,15
	.globl	__realpath_chk
	.type	__realpath_chk, @function
__realpath_chk:
	cmpq	$4095, %rdx
	jbe	.L7
	jmp	__realpath
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__realpath_chk, .-__realpath_chk
	.hidden	__chk_fail
	.hidden	__realpath
