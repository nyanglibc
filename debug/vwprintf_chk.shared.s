	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__vwprintf_chk
	.type	__vwprintf_chk, @function
__vwprintf_chk:
.LFB68:
	movq	stdout@GOTPCREL(%rip), %rax
	xorl	%ecx, %ecx
	testl	%edi, %edi
	setg	%cl
	addl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	__vfwprintf_internal
.LFE68:
	.size	__vwprintf_chk, .-__vwprintf_chk
	.hidden	__vfwprintf_internal
