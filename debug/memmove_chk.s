        .text
.globl __memmove_chk
.type __memmove_chk,@function
.align 1<<4
__memmove_chk:
 cmpq %rdx, %rcx
 jb __chk_fail
 jmp memmove
.size __memmove_chk,.-__memmove_chk
