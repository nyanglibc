	.text
	.p2align 4,,15
	.globl	isblank
	.type	isblank, @function
isblank:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$1, %eax
	ret
	.size	isblank, .-isblank
