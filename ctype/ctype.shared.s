	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	isalnum
	.type	isalnum, @function
isalnum:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$8, %eax
	movzwl	%ax, %eax
	ret
	.size	isalnum, .-isalnum
	.p2align 4,,15
	.globl	isalpha
	.type	isalpha, @function
isalpha:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$1024, %ax
	movzwl	%ax, %eax
	ret
	.size	isalpha, .-isalpha
	.p2align 4,,15
	.globl	iscntrl
	.type	iscntrl, @function
iscntrl:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$2, %eax
	movzwl	%ax, %eax
	ret
	.size	iscntrl, .-iscntrl
	.p2align 4,,15
	.globl	isdigit
	.type	isdigit, @function
isdigit:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$2048, %ax
	movzwl	%ax, %eax
	ret
	.size	isdigit, .-isdigit
	.p2align 4,,15
	.globl	islower
	.type	islower, @function
islower:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$512, %ax
	movzwl	%ax, %eax
	ret
	.size	islower, .-islower
	.p2align 4,,15
	.globl	isgraph
	.type	isgraph, @function
isgraph:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$-32768, %ax
	movzwl	%ax, %eax
	ret
	.size	isgraph, .-isgraph
	.p2align 4,,15
	.globl	isprint
	.type	isprint, @function
isprint:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$16384, %ax
	movzwl	%ax, %eax
	ret
	.size	isprint, .-isprint
	.p2align 4,,15
	.globl	ispunct
	.type	ispunct, @function
ispunct:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andl	$4, %eax
	movzwl	%ax, %eax
	ret
	.size	ispunct, .-ispunct
	.p2align 4,,15
	.globl	isspace
	.type	isspace, @function
isspace:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$8192, %ax
	movzwl	%ax, %eax
	ret
	.size	isspace, .-isspace
	.p2align 4,,15
	.globl	isupper
	.type	isupper, @function
isupper:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$256, %ax
	movzwl	%ax, %eax
	ret
	.size	isupper, .-isupper
	.p2align 4,,15
	.globl	isxdigit
	.type	isxdigit, @function
isxdigit:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdi,2), %eax
	andw	$4096, %ax
	movzwl	%ax, %eax
	ret
	.size	isxdigit, .-isxdigit
	.p2align 4,,15
	.globl	__GI_tolower
	.hidden	__GI_tolower
	.type	__GI_tolower, @function
__GI_tolower:
	leal	128(%rdi), %edx
	movslq	%edi, %rax
	cmpl	$383, %edx
	ja	.L13
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	88(%rdx), %rdx
	movl	512(%rdx,%rax,4), %eax
.L13:
	rep ret
	.size	__GI_tolower, .-__GI_tolower
	.globl	tolower
	.set	tolower,__GI_tolower
	.p2align 4,,15
	.globl	__GI_toupper
	.hidden	__GI_toupper
	.type	__GI_toupper, @function
__GI_toupper:
	leal	128(%rdi), %edx
	movslq	%edi, %rax
	cmpl	$383, %edx
	ja	.L16
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	72(%rdx), %rdx
	movl	512(%rdx,%rax,4), %eax
.L16:
	rep ret
	.size	__GI_toupper, .-__GI_toupper
	.globl	toupper
	.set	toupper,__GI_toupper
