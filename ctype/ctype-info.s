	.text
	.p2align 4,,15
	.globl	__ctype_b_loc
	.type	__ctype_b_loc, @function
__ctype_b_loc:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__ctype_b_loc, .-__ctype_b_loc
	.p2align 4,,15
	.globl	__ctype_toupper_loc
	.type	__ctype_toupper_loc, @function
__ctype_toupper_loc:
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__ctype_toupper_loc, .-__ctype_toupper_loc
	.p2align 4,,15
	.globl	__ctype_tolower_loc
	.type	__ctype_tolower_loc, @function
__ctype_tolower_loc:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__ctype_tolower_loc, .-__ctype_tolower_loc
	.p2align 4,,15
	.globl	__ctype_init
	.hidden	__ctype_init
	.type	__ctype_init, @function
__ctype_init:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rcx
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	64(%rax), %rsi
	movq	72(%rax), %rdi
	movq	88(%rax), %rax
	leaq	256(%rsi), %rdx
	addq	$512, %rax
	movq	%rdx, %fs:(%rcx)
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	leaq	512(%rdi), %rdx
	movq	%rdx, %fs:(%rcx)
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%rax, %fs:(%rdx)
	ret
	.size	__ctype_init, .-__ctype_init
	.globl	__libc_tsd_CTYPE_TOLOWER
	.section	.tbss,"awT",@nobits
	.align 8
	.type	__libc_tsd_CTYPE_TOLOWER, @object
	.size	__libc_tsd_CTYPE_TOLOWER, 8
__libc_tsd_CTYPE_TOLOWER:
	.zero	8
	.globl	__libc_tsd_CTYPE_TOUPPER
	.align 8
	.type	__libc_tsd_CTYPE_TOUPPER, @object
	.size	__libc_tsd_CTYPE_TOUPPER, 8
__libc_tsd_CTYPE_TOUPPER:
	.zero	8
	.globl	__libc_tsd_CTYPE_B
	.align 8
	.type	__libc_tsd_CTYPE_B, @object
	.size	__libc_tsd_CTYPE_B, 8
__libc_tsd_CTYPE_B:
	.zero	8
	.hidden	_nl_current_LC_CTYPE
