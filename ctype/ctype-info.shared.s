	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __ctype_b,__ctype_b@GLIBC_2.2.5
	.symver __ctype_tolower,__ctype_tolower@GLIBC_2.2.5
	.symver __ctype_toupper,__ctype_toupper@GLIBC_2.2.5
	.symver __ctype32_b,__ctype32_b@GLIBC_2.2.5
	.symver __ctype32_tolower,__ctype32_tolower@GLIBC_2.2.5
	.symver __ctype32_toupper,__ctype32_toupper@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ctype_b_loc
	.type	__ctype_b_loc, @function
__ctype_b_loc:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__ctype_b_loc, .-__ctype_b_loc
	.p2align 4,,15
	.globl	__ctype_toupper_loc
	.type	__ctype_toupper_loc, @function
__ctype_toupper_loc:
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__ctype_toupper_loc, .-__ctype_toupper_loc
	.p2align 4,,15
	.globl	__ctype_tolower_loc
	.type	__ctype_tolower_loc, @function
__ctype_tolower_loc:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__ctype_tolower_loc, .-__ctype_tolower_loc
	.p2align 4,,15
	.globl	__GI___ctype_init
	.hidden	__GI___ctype_init
	.type	__GI___ctype_init, @function
__GI___ctype_init:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rcx
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	64(%rax), %rsi
	movq	72(%rax), %rdi
	movq	88(%rax), %rax
	leaq	256(%rsi), %rdx
	addq	$512, %rax
	movq	%rdx, %fs:(%rcx)
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	leaq	512(%rdi), %rdx
	movq	%rdx, %fs:(%rcx)
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%rax, %fs:(%rdx)
	ret
	.size	__GI___ctype_init, .-__GI___ctype_init
	.globl	__ctype_init
	.set	__ctype_init,__GI___ctype_init
	.globl	__ctype32_toupper
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__ctype32_toupper, @object
	.size	__ctype32_toupper, 8
__ctype32_toupper:
	.quad	_nl_C_LC_CTYPE_toupper+512
	.globl	__ctype32_tolower
	.align 8
	.type	__ctype32_tolower, @object
	.size	__ctype32_tolower, 8
__ctype32_tolower:
	.quad	_nl_C_LC_CTYPE_tolower+512
	.globl	__ctype_toupper
	.align 8
	.type	__ctype_toupper, @object
	.size	__ctype_toupper, 8
__ctype_toupper:
	.quad	_nl_C_LC_CTYPE_toupper+512
	.globl	__ctype_tolower
	.align 8
	.type	__ctype_tolower, @object
	.size	__ctype_tolower, 8
__ctype_tolower:
	.quad	_nl_C_LC_CTYPE_tolower+512
	.globl	__ctype32_b
	.align 8
	.type	__ctype32_b, @object
	.size	__ctype32_b, 8
__ctype32_b:
	.quad	_nl_C_LC_CTYPE_class32
	.globl	__ctype_b
	.align 8
	.type	__ctype_b, @object
	.size	__ctype_b, 8
__ctype_b:
	.quad	_nl_C_LC_CTYPE_class+256
	.globl	__libc_tsd_CTYPE_TOLOWER
	.section	.tbss,"awT",@nobits
	.align 8
	.type	__libc_tsd_CTYPE_TOLOWER, @object
	.size	__libc_tsd_CTYPE_TOLOWER, 8
__libc_tsd_CTYPE_TOLOWER:
	.zero	8
	.globl	__libc_tsd_CTYPE_TOUPPER
	.align 8
	.type	__libc_tsd_CTYPE_TOUPPER, @object
	.size	__libc_tsd_CTYPE_TOUPPER, 8
__libc_tsd_CTYPE_TOUPPER:
	.zero	8
	.globl	__libc_tsd_CTYPE_B
	.align 8
	.type	__libc_tsd_CTYPE_B, @object
	.size	__libc_tsd_CTYPE_B, 8
__libc_tsd_CTYPE_B:
	.zero	8
	.hidden	_nl_C_LC_CTYPE_class
	.hidden	_nl_C_LC_CTYPE_class32
	.hidden	_nl_C_LC_CTYPE_tolower
	.hidden	_nl_C_LC_CTYPE_toupper
