	.text
	.p2align 4,,15
	.globl	__isalnum_l
	.type	__isalnum_l, @function
__isalnum_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andl	$8, %eax
	movzwl	%ax, %eax
	ret
	.size	__isalnum_l, .-__isalnum_l
	.weak	isalnum_l
	.set	isalnum_l,__isalnum_l
	.p2align 4,,15
	.globl	__isalpha_l
	.type	__isalpha_l, @function
__isalpha_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$1024, %ax
	movzwl	%ax, %eax
	ret
	.size	__isalpha_l, .-__isalpha_l
	.weak	isalpha_l
	.set	isalpha_l,__isalpha_l
	.p2align 4,,15
	.globl	__iscntrl_l
	.type	__iscntrl_l, @function
__iscntrl_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andl	$2, %eax
	movzwl	%ax, %eax
	ret
	.size	__iscntrl_l, .-__iscntrl_l
	.weak	iscntrl_l
	.set	iscntrl_l,__iscntrl_l
	.p2align 4,,15
	.globl	__isdigit_l
	.type	__isdigit_l, @function
__isdigit_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$2048, %ax
	movzwl	%ax, %eax
	ret
	.size	__isdigit_l, .-__isdigit_l
	.weak	isdigit_l
	.set	isdigit_l,__isdigit_l
	.p2align 4,,15
	.globl	__islower_l
	.type	__islower_l, @function
__islower_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$512, %ax
	movzwl	%ax, %eax
	ret
	.size	__islower_l, .-__islower_l
	.weak	islower_l
	.set	islower_l,__islower_l
	.p2align 4,,15
	.globl	__isgraph_l
	.type	__isgraph_l, @function
__isgraph_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$-32768, %ax
	movzwl	%ax, %eax
	ret
	.size	__isgraph_l, .-__isgraph_l
	.weak	isgraph_l
	.set	isgraph_l,__isgraph_l
	.p2align 4,,15
	.globl	__isprint_l
	.type	__isprint_l, @function
__isprint_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$16384, %ax
	movzwl	%ax, %eax
	ret
	.size	__isprint_l, .-__isprint_l
	.weak	isprint_l
	.set	isprint_l,__isprint_l
	.p2align 4,,15
	.globl	__ispunct_l
	.type	__ispunct_l, @function
__ispunct_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andl	$4, %eax
	movzwl	%ax, %eax
	ret
	.size	__ispunct_l, .-__ispunct_l
	.weak	ispunct_l
	.set	ispunct_l,__ispunct_l
	.p2align 4,,15
	.globl	__isspace_l
	.type	__isspace_l, @function
__isspace_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$8192, %ax
	movzwl	%ax, %eax
	ret
	.size	__isspace_l, .-__isspace_l
	.weak	isspace_l
	.set	isspace_l,__isspace_l
	.p2align 4,,15
	.globl	__isupper_l
	.type	__isupper_l, @function
__isupper_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$256, %ax
	movzwl	%ax, %eax
	ret
	.size	__isupper_l, .-__isupper_l
	.weak	isupper_l
	.set	isupper_l,__isupper_l
	.p2align 4,,15
	.globl	__isxdigit_l
	.type	__isxdigit_l, @function
__isxdigit_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andw	$4096, %ax
	movzwl	%ax, %eax
	ret
	.size	__isxdigit_l, .-__isxdigit_l
	.weak	isxdigit_l
	.set	isxdigit_l,__isxdigit_l
	.p2align 4,,15
	.globl	__tolower_l
	.type	__tolower_l, @function
__tolower_l:
	movq	112(%rsi), %rax
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %eax
	ret
	.size	__tolower_l, .-__tolower_l
	.weak	tolower_l
	.set	tolower_l,__tolower_l
	.p2align 4,,15
	.globl	__toupper_l
	.type	__toupper_l, @function
__toupper_l:
	movq	120(%rsi), %rax
	movslq	%edi, %rdi
	movl	(%rax,%rdi,4), %eax
	ret
	.size	__toupper_l, .-__toupper_l
	.weak	toupper_l
	.set	toupper_l,__toupper_l
