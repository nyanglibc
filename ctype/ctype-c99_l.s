	.text
	.p2align 4,,15
	.globl	__isblank_l
	.type	__isblank_l, @function
__isblank_l:
	movq	104(%rsi), %rax
	movslq	%edi, %rdi
	movzwl	(%rax,%rdi,2), %eax
	andl	$1, %eax
	ret
	.size	__isblank_l, .-__isblank_l
	.weak	isblank_l
	.set	isblank_l,__isblank_l
