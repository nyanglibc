	.text
	.p2align 4,,15
	.globl	_tolower
	.type	_tolower, @function
_tolower:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	88(%rax), %rax
	movl	512(%rax,%rdi,4), %eax
	ret
	.size	_tolower, .-_tolower
	.p2align 4,,15
	.globl	_toupper
	.type	_toupper, @function
_toupper:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	72(%rax), %rax
	movl	512(%rax,%rdi,4), %eax
	ret
	.size	_toupper, .-_toupper
	.p2align 4,,15
	.globl	toascii
	.type	toascii, @function
toascii:
	movl	%edi, %eax
	andl	$127, %eax
	ret
	.size	toascii, .-toascii
	.weak	__toascii_l
	.set	__toascii_l,toascii
	.p2align 4,,15
	.globl	isascii
	.type	isascii, @function
isascii:
	xorl	%eax, %eax
	andl	$-128, %edi
	sete	%al
	ret
	.size	isascii, .-isascii
	.weak	__isascii_l
	.set	__isascii_l,isascii
	.hidden	_nl_current_LC_CTYPE
