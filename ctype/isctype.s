	.text
	.p2align 4,,15
	.globl	__isctype
	.type	__isctype, @function
__isctype:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	64(%rax), %rax
	movzwl	256(%rax,%rdi,2), %eax
	andl	%esi, %eax
	ret
	.size	__isctype, .-__isctype
	.weak	isctype
	.set	isctype,__isctype
	.hidden	_nl_current_LC_CTYPE
