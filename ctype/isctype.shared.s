	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__isctype
	.type	__isctype, @function
__isctype:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	64(%rax), %rax
	movzwl	256(%rax,%rdi,2), %eax
	andl	%esi, %eax
	ret
	.size	__isctype, .-__isctype
	.weak	isctype
	.set	isctype,__isctype
