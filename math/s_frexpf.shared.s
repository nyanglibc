	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__frexpf
	.type	__frexpf, @function
__frexpf:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	movl	$0, (%rdi)
	andl	$2147483647, %eax
	cmpl	$2139095039, %eax
	jg	.L7
	testl	%eax, %eax
	je	.L7
	xorl	%ecx, %ecx
	cmpl	$8388607, %eax
	jg	.L5
	mulss	.LC0(%rip), %xmm0
	movl	$-25, %ecx
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	andl	$2147483647, %eax
.L5:
	sarl	$23, %eax
	andl	$-2139095041, %edx
	leal	-126(%rcx,%rax), %eax
	orl	$1056964608, %edx
	movl	%edx, -4(%rsp)
	movl	%eax, (%rdi)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	addss	%xmm0, %xmm0
	ret
	.size	__frexpf, .-__frexpf
	.weak	frexpf32
	.set	frexpf32,__frexpf
	.weak	frexpf
	.set	frexpf,__frexpf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1275068416
