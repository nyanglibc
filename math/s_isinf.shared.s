	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isinf
	.hidden	__GI___isinf
	.type	__GI___isinf, @function
__GI___isinf:
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rax
	movabsq	$9218868437227405312, %rcx
	andq	%rdx, %rax
	sarq	$62, %rdx
	xorq	%rax, %rcx
	movq	%rcx, %rax
	negq	%rax
	orq	%rcx, %rax
	sarq	$63, %rax
	notl	%eax
	andl	%edx, %eax
	ret
	.size	__GI___isinf, .-__GI___isinf
	.globl	__isinf
	.set	__isinf,__GI___isinf
	.weak	isinf
	.set	isinf,__isinf
