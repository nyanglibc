	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__setfpucw
	.hidden	__setfpucw
	.type	__setfpucw, @function
__setfpucw:
#APP
# 28 "setfpucw.c" 1
	fnstcw -2(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-2(%rsp), %eax
	andw	$3903, %di
	andw	$-3904, %ax
	orl	%eax, %edi
	movw	%di, -2(%rsp)
#APP
# 35 "setfpucw.c" 1
	fldcw -2(%rsp)
# 0 "" 2
#NO_APP
	ret
	.size	__setfpucw, .-__setfpucw
