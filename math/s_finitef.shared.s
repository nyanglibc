	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___finitef
	.hidden	__GI___finitef
	.type	__GI___finitef, @function
__GI___finitef:
	movd	%xmm0, %eax
	andl	$2139095040, %eax
	subl	$2139095040, %eax
	shrl	$31, %eax
	ret
	.size	__GI___finitef, .-__GI___finitef
	.globl	__finitef
	.set	__finitef,__GI___finitef
	.weak	finitef
	.set	finitef,__finitef
