	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__modff
	.type	__modff, @function
__modff:
	movd	%xmm0, %ecx
	movd	%xmm0, %edx
	sarl	$23, %ecx
	movzbl	%cl, %ecx
	subl	$127, %ecx
	cmpl	$22, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L9
	movl	$8388607, %eax
	sarl	%cl, %eax
	testl	%eax, %edx
	jne	.L5
	andl	$-2147483648, %edx
	movss	%xmm0, (%rdi)
	movl	%edx, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	notl	%eax
	andl	%edx, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm1
	movss	%xmm1, (%rdi)
	subss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	andl	$-2147483648, %edx
	movl	%edx, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	mulss	.LC0(%rip), %xmm0
	addl	$-128, %ecx
	movss	%xmm0, (%rdi)
	je	.L10
.L6:
	andl	$-2147483648, %edx
	movl	%edx, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	testl	$8388607, %edx
	je	.L6
	rep ret
	.size	__modff, .-__modff
	.weak	modff32
	.set	modff32,__modff
	.weak	modff
	.set	modff,__modff
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
