	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__modfl
	.type	__modfl, @function
__modfl:
	movq	16(%rsp), %rax
	movq	8(%rsp), %rsi
	movl	%eax, %edx
	movq	%rsi, %r8
	andl	$32767, %edx
	shrq	$32, %r8
	leal	-16383(%rdx), %ecx
	cmpl	$31, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L12
	movl	$2147483647, %edx
	sarl	%cl, %edx
	movl	%edx, %ecx
	andl	%r8d, %ecx
	orl	%esi, %ecx
	jne	.L5
	fldt	8(%rsp)
	andw	$-32768, %ax
	movq	$0, -24(%rsp)
	movw	%ax, -16(%rsp)
	fstpt	(%rdi)
	fldt	-24(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$63, %ecx
	jg	.L13
	leal	-16415(%rdx), %ecx
	movl	$2147483647, %edx
	shrl	%cl, %edx
	testl	%esi, %edx
	je	.L14
	notl	%edx
	movw	%ax, -16(%rsp)
	movl	%r8d, -20(%rsp)
	andl	%edx, %esi
	movl	%esi, -24(%rsp)
	fldt	-24(%rsp)
	fld	%st(0)
	fstpt	(%rdi)
	fldt	8(%rsp)
	fsubp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	fldt	8(%rsp)
	fstpt	(%rdi)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L15:
	fstp	%st(0)
	jmp	.L9
.L16:
	fstp	%st(0)
.L9:
	andw	$-32768, %ax
	movq	$0, -24(%rsp)
	movw	%ax, -16(%rsp)
	fldt	-24(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	notl	%edx
	movw	%ax, -16(%rsp)
	movl	$0, -24(%rsp)
	andl	%r8d, %edx
	movl	%edx, -20(%rsp)
	fldt	-24(%rsp)
	fld	%st(0)
	fstpt	(%rdi)
	fldt	8(%rsp)
	fsubp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	andw	$-32768, %ax
	movq	$0, -24(%rsp)
	movw	%ax, -16(%rsp)
	fldt	-24(%rsp)
	fstpt	(%rdi)
	fldt	8(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	fld1
	cmpl	$16384, %ecx
	fldt	8(%rsp)
	fmulp	%st, %st(1)
	fld	%st(0)
	fstpt	(%rdi)
	jne	.L15
	movl	%r8d, %edx
	andl	$2147483647, %edx
	orl	%esi, %edx
	je	.L16
	rep ret
	.size	__modfl, .-__modfl
	.weak	modff64x
	.set	modff64x,__modfl
	.weak	modfl
	.set	modfl,__modfl
