	.text
	.p2align 4,,15
	.globl	__remquo
	.type	__remquo, @function
__remquo:
	pushq	%r13
	movq	%xmm0, %r13
	pushq	%r12
	movq	%xmm0, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%xmm1, %rbx
	movabsq	$9223372036854775807, %rax
	subq	$24, %rsp
	andq	%rax, %r12
	xorq	%rbx, %r13
	andq	%rax, %rbx
	je	.L27
	movabsq	$9218868437227405311, %rax
	cmpq	%rax, %r12
	ja	.L27
	addq	$1, %rax
	cmpq	%rax, %rbx
	ja	.L27
	movabsq	$9205357638345293823, %rax
	movq	%xmm0, %rbp
	cmpq	%rax, %rbx
	jbe	.L36
.L6:
	cmpq	%r12, %rbx
	je	.L37
	movabsq	$9209861237972664319, %rax
	movq	%rbx, 8(%rsp)
	cmpq	%rax, %rbx
	andpd	.LC2(%rip), %xmm0
	movsd	8(%rsp), %xmm1
	jbe	.L38
	movabsq	$9214364837600034815, %rax
	cmpq	%rax, %rbx
	movl	$0, %eax
	jbe	.L10
.L12:
	movsd	.LC4(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm0
	ja	.L39
.L15:
	pxor	%xmm3, %xmm3
	movl	%eax, %edx
	negl	%edx
	testq	%r13, %r13
	cmovs	%edx, %eax
	ucomisd	%xmm3, %xmm0
	movl	%eax, (%rdi)
	jp	.L21
	movq	%xmm0, %rcx
	movq	%xmm3, %rax
	cmovne	%rcx, %rax
	movq	%rax, 8(%rsp)
	movsd	8(%rsp), %xmm0
.L21:
	testq	%rbp, %rbp
	jns	.L1
	xorpd	.LC5(%rip), %xmm0
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movsd	.LC3(%rip), %xmm2
	xorl	%eax, %eax
	mulsd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm0
	jb	.L10
	subsd	%xmm2, %xmm0
	movl	$4, %eax
.L10:
	movapd	%xmm1, %xmm2
	addsd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm0
	jnb	.L40
.L13:
	movabsq	$9007199254740991, %rdx
	cmpq	%rdx, %rbx
	ja	.L12
	movapd	%xmm0, %xmm2
	addsd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.L15
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm2
	addsd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	jb	.L34
.L19:
	subsd	%xmm1, %xmm0
	addl	$2, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L36:
	mulsd	.LC1(%rip), %xmm1
	movq	%rdi, 8(%rsp)
	call	__ieee754_fmod@PLT
	movq	8(%rsp), %rdi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L40:
	subsd	%xmm2, %xmm0
	addl	$2, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L39:
	subsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	jnb	.L19
.L34:
	addl	$1, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L27:
	mulsd	%xmm1, %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	divsd	%xmm0, %xmm0
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	sarq	$63, %r13
	mulsd	.LC0(%rip), %xmm0
	orl	$1, %r13d
	movl	%r13d, (%rdi)
	jmp	.L1
	.size	__remquo, .-__remquo
	.weak	remquof32x
	.set	remquof32x,__remquo
	.weak	remquof64
	.set	remquof64,__remquo
	.weak	remquo
	.set	remquo,__remquo
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1075838976
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	1074790400
	.align 8
.LC4:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
