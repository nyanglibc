	.text
	.p2align 4,,15
	.globl	__j0
	.type	__j0, @function
__j0:
	jmp	__ieee754_j0@PLT
	.size	__j0, .-__j0
	.weak	j0f32x
	.set	j0f32x,__j0
	.weak	j0f64
	.set	j0f64,__j0
	.weak	j0
	.set	j0,__j0
	.p2align 4,,15
	.globl	__y0
	.type	__y0, @function
__y0:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L11
.L4:
	jmp	__ieee754_y0@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	ucomisd	%xmm1, %xmm0
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y0, .-__y0
	.weak	y0f32x
	.set	y0f32x,__y0
	.weak	y0f64
	.set	y0f64,__y0
	.weak	y0
	.set	y0,__y0
