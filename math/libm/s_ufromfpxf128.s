	.text
	.p2align 4,,15
	.globl	__ufromfpxf128
	.type	__ufromfpxf128, @function
__ufromfpxf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	cmpl	$64, %esi
	movaps	%xmm0, (%rsp)
	ja	.L34
	testl	%esi, %esi
	jne	.L2
.L19:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$64, %esi
.L2:
	movq	8(%rsp), %r9
	movq	(%rsp), %rdx
	movabsq	$9223372036854775807, %rcx
	xorl	%eax, %eax
	andq	%r9, %rcx
	movq	%rcx, %rbx
	orq	%rdx, %rbx
	je	.L1
	shrq	$48, %rcx
	testq	%r9, %r9
	movq	%rcx, %r8
	leal	-16383(%rcx), %r10d
	js	.L4
	leal	-1(%rsi), %r11d
	cmpl	%r11d, %r10d
	jg	.L22
.L6:
	cmpl	$-1, %r10d
	jl	.L7
	movabsq	$281474976710655, %rax
	movl	$112, %ebx
	movabsq	$281474976710656, %rcx
	andq	%r9, %rax
	subl	%r10d, %ebx
	orq	%rcx, %rax
	cmpl	$64, %ebx
	jg	.L8
	movl	$111, %ecx
	movl	$1, %ebp
	subl	%r10d, %ecx
	salq	%cl, %rbp
	testq	%rdx, %rbp
	movq	%rbp, %rcx
	setne	%bpl
	subq	$1, %rcx
	testq	%rdx, %rcx
	leal	-16431(%r8), %ecx
	setne	%r12b
	salq	%cl, %rax
	movl	%ebx, %ecx
	shrq	%cl, %rdx
	orq	%rax, %rdx
	cmpl	$64, %ebx
	cmovne	%rdx, %rax
.L9:
	cmpl	$1, %edi
	je	.L11
	jle	.L67
	cmpl	$3, %edi
	je	.L14
	cmpl	$4, %edi
	jne	.L10
	testb	%bpl, %bpl
	je	.L10
	movq	%rax, %rdx
	orq	%r12, %rdx
	andl	$1, %edx
	addq	%rdx, %rax
.L10:
	testq	%r9, %r9
	jns	.L17
.L16:
	testq	%rax, %rax
	jne	.L19
.L20:
	testb	%r12b, %r12b
	jne	.L31
	testb	%bpl, %bpl
	je	.L1
.L31:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$1, %edi
	je	.L37
	jle	.L68
	cmpl	$3, %edi
	je	.L30
	cmpl	$4, %edi
	jne	.L28
	xorl	%eax, %eax
	movl	$1, %r12d
	xorl	%ebp, %ebp
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
	testq	%r9, %r9
	js	.L31
.L32:
	cmpl	$63, %r11d
	jne	.L40
	xorl	%eax, %eax
	movl	$1, %r12d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	$63, %r10d
	jne	.L20
	testq	%rax, %rax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %edi
	movl	%esi, (%rsp)
	call	feraiseexcept@PLT
	movl	(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	movq	$-1, %rax
	je	.L1
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r10d, %r10d
	movl	$-1, %r11d
	js	.L6
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$47, %ecx
	movl	$1, %r8d
	subl	%r10d, %ecx
	salq	%cl, %r8
	testq	%r8, %rax
	movq	%r8, %rcx
	setne	%bpl
	subq	$1, %rcx
	andq	%rax, %rcx
	orq	%rdx, %rcx
	movl	$48, %ecx
	setne	%r12b
	subl	%r10d, %ecx
	shrq	%cl, %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	%bpl, %edx
	addq	%rdx, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L68:
	testl	%edi, %edi
	jne	.L28
	movl	$1, %r12d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
.L13:
	testq	%r9, %r9
	js	.L16
	movl	%ebp, %edx
	orl	%r12d, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
.L17:
	cmpl	$63, %r11d
	je	.L33
	leal	1(%r11), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rax, %rdx
	jne	.L20
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, %r12d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
.L11:
	testq	%r9, %r9
	jns	.L17
	movl	%ebp, %edx
	orl	%r12d, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L67:
	testl	%edi, %edi
	je	.L13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	testq	%r9, %r9
	jns	.L32
.L40:
	xorl	%eax, %eax
	jmp	.L31
	.size	__ufromfpxf128, .-__ufromfpxf128
	.weak	ufromfpxf128
	.set	ufromfpxf128,__ufromfpxf128
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
