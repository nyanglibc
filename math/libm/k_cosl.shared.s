	.text
	.p2align 4,,15
	.globl	__kernel_cosl
	.type	__kernel_cosl, @function
__kernel_cosl:
	fldt	24(%rsp)
	fldt	8(%rsp)
	fxam
	fnstsw	%ax
	testb	$2, %ah
	je	.L2
	fchs
	fxch	%st(1)
	fchs
	fxch	%st(1)
.L2:
	flds	.LC1(%rip)
	fucomi	%st(1), %st
	jbe	.L14
	fstp	%st(0)
	fstp	%st(1)
	flds	.LC2(%rip)
	fucomip	%st(1), %st
	jbe	.L5
	fnstcw	-10(%rsp)
	movzwl	-10(%rsp), %eax
	orb	$12, %ah
	movw	%ax, -12(%rsp)
	fld	%st(0)
	fldcw	-12(%rsp)
	fistpl	-16(%rsp)
	fldcw	-10(%rsp)
	movl	-16(%rsp), %eax
	testl	%eax, %eax
	jne	.L5
	fstp	%st(0)
	fld1
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	fld	%st(1)
	fnstcw	-10(%rsp)
	movzwl	-10(%rsp), %eax
	pxor	%xmm0, %xmm0
	movq	__sincosl_table@GOTPCREL(%rip), %rcx
	fsubs	.LC11(%rip)
	orb	$12, %ah
	movw	%ax, -12(%rsp)
	fmuls	.LC12(%rip)
	fldcw	-12(%rsp)
	fistpl	-16(%rsp)
	fldcw	-10(%rsp)
	movl	-16(%rsp), %edx
	cvtsi2sd	%edx, %xmm0
	leal	0(,%rdx,4), %eax
	movslq	%eax, %rdx
	salq	$4, %rdx
	mulsd	.LC13(%rip), %xmm0
	movsd	%xmm0, -24(%rsp)
	faddl	-24(%rsp)
	fsubp	%st, %st(1)
	fsubrp	%st, %st(1)
	fld	%st(0)
	fmul	%st(1), %st
	fldt	(%rcx,%rdx)
	leal	1(%rax), %edx
	addl	$2, %eax
	cltq
	salq	$4, %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	fldt	.LC14(%rip)
	fmul	%st(2), %st
	fldt	.LC15(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC16(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC17(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC18(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fld1
	faddp	%st, %st(1)
	fmulp	%st, %st(3)
	fldt	(%rcx,%rax)
	fmulp	%st, %st(3)
	fldt	.LC19(%rip)
	fmul	%st(2), %st
	fldt	.LC20(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fsubs	.LC10(%rip)
	fmulp	%st, %st(2)
	fmul	%st, %st(1)
	fxch	%st(2)
	fsubp	%st, %st(1)
	fldt	(%rcx,%rdx)
	fsubp	%st, %st(1)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	fld	%st(0)
	fmulp	%st, %st(1)
	fldt	.LC3(%rip)
	fmul	%st(1), %st
	fldt	.LC4(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC5(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC6(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC7(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fsubs	.LC10(%rip)
	fmulp	%st, %st(1)
	fld1
	faddp	%st, %st(1)
	ret
	.size	__kernel_cosl, .-__kernel_cosl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1041760256
	.align 4
.LC2:
	.long	788529152
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	3857114492
	.long	3610230987
	.long	16338
	.long	0
	.align 16
.LC4:
	.long	2438246415
	.long	3385566388
	.long	16346
	.long	0
	.align 16
.LC5:
	.long	3289288521
	.long	2406926207
	.long	16354
	.long	0
	.align 16
.LC6:
	.long	3304772929
	.long	2482142651
	.long	16361
	.long	0
	.align 16
.LC7:
	.long	218157069
	.long	3490513104
	.long	16367
	.long	0
	.align 16
.LC8:
	.long	190887435
	.long	3054198966
	.long	16373
	.long	0
	.align 16
.LC9:
	.long	2863311531
	.long	2863311530
	.long	16378
	.long	0
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	1056964608
	.align 4
.LC11:
	.long	1041498112
	.align 4
.LC12:
	.long	1124073472
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC13:
	.long	0
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC14:
	.long	2250501689
	.long	3610388340
	.long	49125
	.long	0
	.align 16
.LC15:
	.long	3056731701
	.long	3102678314
	.long	16364
	.long	0
	.align 16
.LC16:
	.long	218157069
	.long	3490513104
	.long	16370
	.long	0
	.align 16
.LC17:
	.long	2290649225
	.long	2290649224
	.long	16376
	.long	0
	.align 16
.LC18:
	.long	2863311531
	.long	2863311530
	.long	16380
	.long	0
	.align 16
.LC19:
	.long	1931208070
	.long	2482141934
	.long	49129
	.long	0
	.align 16
.LC20:
	.long	217514350
	.long	3490513104
	.long	16367
	.long	0
