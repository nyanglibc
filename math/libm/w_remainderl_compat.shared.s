	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__remainderl
	.type	__remainderl, @function
__remainderl:
	fldt	24(%rsp)
	fldt	8(%rsp)
	fucomi	%st(0), %st
	jp	.L5
	fldz
	movl	$0, %edx
	fucomip	%st(2), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L16
.L5:
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L17
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L4
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L16:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L4
	fstpt	24(%rsp)
	movl	$228, %edi
	fstpt	8(%rsp)
	jmp	__kernel_standard_l@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	fxch	%st(1)
.L4:
	fstpt	24(%rsp)
	fstpt	8(%rsp)
	jmp	__ieee754_remainderl@PLT
	.size	__remainderl, .-__remainderl
	.weak	dreml
	.set	dreml,__remainderl
	.weak	remainderf64x
	.set	remainderf64x,__remainderl
	.weak	remainderl
	.set	remainderl,__remainderl
