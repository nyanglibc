	.text
	.globl	__addtf3
	.globl	__unordtf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__nextupf128
	.type	__nextupf128, @function
__nextupf128:
	pushq	%rbp
	pushq	%rbx
	movabsq	$9223372036854775807, %rax
	movabsq	$9223090561878065151, %rdx
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbx
	movq	(%rsp), %rbp
	andq	%rbx, %rax
	cmpq	%rdx, %rax
	jle	.L2
	movabsq	$-9223090561878065152, %rdx
	addq	%rax, %rdx
	orq	%rbp, %rdx
	jne	.L17
.L2:
	orq	%rbp, %rax
	je	.L8
	testq	%rbx, %rbx
	js	.L4
	movdqa	(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L10
	movdqa	16(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L14
.L10:
	leaq	1(%rbx), %rax
	addq	$1, %rbp
	cmove	%rax, %rbx
.L6:
	movq	%rbp, (%rsp)
	movq	%rbx, 8(%rsp)
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	.LC0(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	-1(%rbx), %rax
	testq	%rbp, %rbp
	cmove	%rax, %rbx
	subq	$1, %rbp
	jmp	.L6
	.size	__nextupf128, .-__nextupf128
	.weak	nextupf128
	.set	nextupf128,__nextupf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	0
	.long	0
	.long	0
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
