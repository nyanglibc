	.text
	.p2align 4,,15
	.globl	__ieee754_jnl
	.type	__ieee754_jnl, @function
__ieee754_jnl:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$176, %rsp
	movq	216(%rsp), %rax
	movq	208(%rsp), %rdx
	movswl	%ax, %esi
	movq	%rdx, %rcx
	andw	$32767, %ax
	shrq	$32, %rcx
	cmpw	$32767, %ax
	je	.L81
.L2:
	testl	%ebx, %ebx
	js	.L82
	fldt	208(%rsp)
	je	.L83
	cmpl	$1, %ebx
	je	.L84
.L6:
	shrl	$15, %esi
	movl	%esi, %ebp
	andl	%ebx, %ebp
	andl	$1, %ebp
	fabs
#APP
# 383 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstcw 96(%rsp)
# 0 "" 2
#NO_APP
	movzwl	96(%rsp), %esi
	movl	%esi, %edx
	movw	%si, 128(%rsp)
	andb	$-16, %dh
	orb	$3, %dh
	cmpw	%dx, %si
	movw	%dx, 112(%rsp)
	jne	.L85
	movb	$0, 160(%rsp)
.L8:
	movq	208(%rsp), %rdx
	cwtl
	orl	%ecx, %edx
	orl	%eax, %edx
	je	.L89
	cmpl	$32767, %eax
	je	.L90
	movl	%ebx, (%rsp)
	fildl	(%rsp)
	fxch	%st(1)
	fucomi	%st(1), %st
	jb	.L75
	fstp	%st(1)
	cmpl	$16684, %eax
	jle	.L15
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__sincosl@PLT
	movl	%ebx, %edi
	popq	%rcx
	popq	%rsi
	andl	$3, %edi
	cmpl	$2, %edi
	fldt	(%rsp)
	je	.L17
	cmpl	$3, %edi
	je	.L18
	cmpl	$1, %edi
	je	.L19
	fldt	112(%rsp)
	fldt	96(%rsp)
	faddp	%st, %st(1)
	fxch	%st(1)
.L20:
	fsqrt
	pxor	%xmm0, %xmm0
	movss	%xmm0, 16(%rsp)
	fldt	.LC3(%rip)
	fmulp	%st, %st(2)
	fdivrp	%st, %st(1)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L83:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_j0l@PLT
	popq	%r9
	popq	%r10
.L1:
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	fldt	208(%rsp)
	negl	%ebx
	xorl	$32768, %esi
	cmpl	$1, %ebx
	fchs
	jne	.L6
.L84:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_j1l@PLT
	popq	%rdi
	popq	%r8
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$16349, %eax
	jg	.L23
	fstp	%st(1)
	cmpl	$399, %ebx
	jg	.L48
	fmuls	.LC4(%rip)
	movl	$2, %eax
	fld	%st(0)
	fld1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L91:
	fxch	%st(1)
.L24:
	movl	%eax, (%rsp)
	addl	$1, %eax
	fildl	(%rsp)
	cmpl	%ebx, %eax
	fmulp	%st, %st(1)
	fxch	%st(1)
	fmul	%st(2), %st
	jle	.L91
	fstp	%st(2)
	fxch	%st(1)
	pxor	%xmm3, %xmm3
	fdivp	%st, %st(1)
	movss	%xmm3, 16(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	leal	(%rbx,%rbx), %r12d
	movl	$1, %eax
	movl	%r12d, (%rsp)
	fildl	(%rsp)
	fdiv	%st(1), %st
	flds	.LC5(%rip)
	fdiv	%st(2), %st
	fld	%st(1)
	fadd	%st(1), %st
	fld	%st(2)
	fmul	%st(1), %st
	fld1
	fsubrp	%st, %st(1)
	fldl	.LC6(%rip)
	fld	%st(0)
	fucomip	%st(2), %st
	jbe	.L92
	fxch	%st(2)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L93:
	fxch	%st(1)
	fxch	%st(4)
	fxch	%st(1)
.L27:
	fadd	%st(3), %st
	addl	$1, %eax
	fld	%st(0)
	fmul	%st(2), %st
	fsubp	%st, %st(5)
	fld	%st(2)
	fucomip	%st(5), %st
	ja	.L93
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L92:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L25:
	leal	(%rax,%rbx), %edx
	cmpl	%ebx, %edx
	leal	(%rdx,%rdx), %eax
	jl	.L50
	pxor	%xmm2, %xmm2
	fldz
	fld1
	movss	%xmm2, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%eax, (%rsp)
	subl	$2, %eax
	cmpl	%eax, %r12d
	fildl	(%rsp)
	fdiv	%st(4), %st
	fsubp	%st, %st(2)
	fdiv	%st, %st(1)
	jle	.L29
	fstp	%st(0)
	fxch	%st(2)
.L28:
	fstpt	48(%rsp)
	fxch	%st(1)
	subq	$16, %rsp
	subl	$2, %r12d
	fstpt	48(%rsp)
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	16(%rsp)
	fabs
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	fldt	16(%rsp)
	popq	%rax
	popq	%rdx
	leal	-1(%rbx), %edi
	movl	%r12d, (%rsp)
	fmulp	%st, %st(1)
	fldt	.LC7(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	32(%rsp)
	fldt	48(%rsp)
	fildl	(%rsp)
	jbe	.L76
	fld	%st(2)
	fld1
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L51:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	fxch	%st(2)
.L32:
	fld	%st(0)
	subl	$1, %edi
	fmul	%st(3), %st
	fdiv	%st(4), %st
	fsubp	%st, %st(2)
	fxch	%st(2)
	fsubs	.LC5(%rip)
	fld	%st(2)
	jne	.L51
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(3)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L94:
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(2)
	fxch	%st(3)
.L33:
	fstpt	64(%rsp)
	fxch	%st(2)
	subq	$16, %rsp
	fstpt	96(%rsp)
	fxch	%st(1)
	fstpt	64(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	__ieee754_j0l@PLT
	fstpt	16(%rsp)
	subq	$16, %rsp
	fldt	64(%rsp)
	fstpt	(%rsp)
	call	__ieee754_j1l@PLT
	fldt	32(%rsp)
	addq	$32, %rsp
	fld	%st(0)
	fabs
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	48(%rsp)
	fldt	64(%rsp)
	jb	.L78
	fstp	%st(3)
	fstp	%st(0)
	fxch	%st(1)
	fmulp	%st, %st(1)
	fldt	80(%rsp)
	fdivrp	%st, %st(1)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L15:
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__ieee754_j0l@PLT
	fstpt	32(%rsp)
	subq	$16, %rsp
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__ieee754_j1l@PLT
	addq	$32, %rsp
	movl	$1, %edx
	fldt	16(%rsp)
	fldt	(%rsp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L47:
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
.L22:
	leal	(%rdx,%rdx), %eax
	addl	$1, %edx
	cmpl	%ebx, %edx
	movl	%eax, (%rsp)
	fildl	(%rsp)
	fdiv	%st(1), %st
	fmul	%st(3), %st
	fsubp	%st, %st(2)
	jl	.L47
	fstp	%st(0)
	fstp	%st(1)
	pxor	%xmm1, %xmm1
	movss	%xmm1, 16(%rsp)
.L21:
	testl	%ebp, %ebp
	je	.L39
	fchs
.L39:
	cmpb	$0, 160(%rsp)
	jne	.L86
.L40:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L41
	je	.L87
.L41:
	fldt	.LC9(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L1
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	fstp	%st(0)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L90:
	fstp	%st(0)
.L53:
	testl	%ebp, %ebp
	fldz
	je	.L11
	fchs
.L11:
	cmpb	$0, 160(%rsp)
	je	.L1
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 128(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L87:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC9(%rip)
	je	.L43
	fstp	%st(0)
	fldt	.LC10(%rip)
.L43:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	fldt	.LC9(%rip)
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	testl	$2147483647, %ecx
	je	.L2
	fldt	208(%rsp)
	fadd	%st(0), %st
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	fstp	%st(0)
	pxor	%xmm4, %xmm4
	fldz
	movss	%xmm4, 16(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L78:
	fstp	%st(2)
	fxch	%st(1)
	fmulp	%st, %st(2)
	fdivrp	%st, %st(1)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L76:
	fld	%st(2)
	fld1
	fldt	.LC8(%rip)
	fld1
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L88:
	fdivr	%st, %st(2)
	fdivrp	%st, %st(6)
	fld	%st(3)
.L34:
	subl	$1, %edi
	je	.L94
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(4)
.L36:
	fld	%st(2)
	fmul	%st(5), %st
	fdiv	%st(6), %st
	fsubp	%st, %st(4)
	fxch	%st(4)
	fsubs	.LC5(%rip)
	fxch	%st(3)
	fucomi	%st(1), %st
	ja	.L88
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L85:
#APP
# 391 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 112(%rsp)
# 0 "" 2
#NO_APP
	movb	$1, 160(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	fldt	96(%rsp)
	fldt	112(%rsp)
	fsubrp	%st, %st(1)
	fxch	%st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L86:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 128(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L18:
	fldt	112(%rsp)
	fldt	96(%rsp)
	fsubrp	%st, %st(1)
	fxch	%st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L17:
	fldt	112(%rsp)
	fchs
	fldt	96(%rsp)
	fsubrp	%st, %st(1)
	fxch	%st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L50:
	pxor	%xmm5, %xmm5
	fldz
	fxch	%st(2)
	movss	%xmm5, 16(%rsp)
	jmp	.L28
	.size	__ieee754_jnl, .-__ieee754_jnl
	.p2align 4,,15
	.globl	__ieee754_ynl
	.type	__ieee754_ynl, @function
__ieee754_ynl:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$120, %rsp
	movq	152(%rsp), %rcx
	movq	144(%rsp), %rax
	movl	%ecx, %edx
	movq	%rax, %rsi
	andw	$32767, %dx
	shrq	$32, %rsi
	cmpw	$32767, %dx
	je	.L150
.L96:
	orl	%esi, %eax
	movswl	%dx, %esi
	orl	%esi, %eax
	je	.L151
	testw	%cx, %cx
	js	.L152
	testl	%ebx, %ebx
	js	.L153
	movl	$1, %ebp
	je	.L154
.L102:
#APP
# 383 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstcw 32(%rsp)
# 0 "" 2
#NO_APP
	movzwl	32(%rsp), %ecx
	movl	%ecx, %eax
	movw	%cx, 64(%rsp)
	andb	$-16, %ah
	orb	$3, %ah
	cmpw	%ax, %cx
	movw	%ax, 48(%rsp)
	jne	.L155
	movb	$0, 96(%rsp)
.L104:
	cmpl	$1, %ebx
	je	.L156
	cmpw	$32767, %dx
	je	.L157
	cmpw	$16684, %dx
	jle	.L110
	leaq	48(%rsp), %rsi
	leaq	32(%rsp), %rdi
	pushq	152(%rsp)
	pushq	152(%rsp)
	andl	$3, %ebx
	call	__sincosl@PLT
	cmpl	$2, %ebx
	popq	%rax
	popq	%rdx
	je	.L112
	cmpl	$3, %ebx
	fldt	32(%rsp)
	je	.L113
	cmpl	$1, %ebx
	jne	.L147
	fchs
.L147:
	fldt	48(%rsp)
	fsubrp	%st, %st(1)
.L115:
	fldt	144(%rsp)
	fsqrt
	fldt	.LC3(%rip)
	fmulp	%st, %st(2)
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L161:
	fstp	%st(0)
.L116:
	fldt	.LC13(%rip)
	fldt	(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L119
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L119:
	cmpl	$1, %ebp
	je	.L121
	fldt	(%rsp)
	fchs
	fstpt	(%rsp)
.L121:
	cmpb	$0, 96(%rsp)
	jne	.L148
.L107:
	fldt	(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	movl	%eax, %edx
	andb	$69, %dh
	cmpb	$5, %dh
	jne	.L95
	testb	$2, %ah
	fldt	.LC13(%rip)
	je	.L123
	fstp	%st(0)
	fldt	.LC14(%rip)
.L123:
	fldt	.LC13(%rip)
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	fldt	(%rsp)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	pushq	152(%rsp)
	pushq	152(%rsp)
	call	__ieee754_y0l@PLT
	fstpt	16(%rsp)
	popq	%rdi
	popq	%r8
.L95:
	fldt	(%rsp)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	negl	%ebx
	movl	$1, %ebp
	leal	(%rbx,%rbx), %eax
	andl	$2, %eax
	subl	%eax, %ebp
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L110:
	pushq	152(%rsp)
	pushq	152(%rsp)
	call	__ieee754_y0l@PLT
	fstpt	16(%rsp)
	pushq	168(%rsp)
	pushq	168(%rsp)
	call	__ieee754_y1l@PLT
	fstpt	48(%rsp)
	movq	56(%rsp), %rax
	addq	$32, %rsp
	cwtl
	cmpl	$-1, %eax
	je	.L125
	movl	$1, %edx
	fldt	(%rsp)
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L158:
	movq	8(%rsp), %rax
	cwtl
	cmpl	$-1, %eax
	je	.L161
	fldt	(%rsp)
	fstpt	16(%rsp)
.L117:
	leal	(%rdx,%rdx), %eax
	addl	$1, %edx
	cmpl	%ebx, %edx
	movl	%eax, (%rsp)
	fildl	(%rsp)
	fldt	144(%rsp)
	fdivrp	%st, %st(1)
	fldt	16(%rsp)
	fmul	%st, %st(1)
	fxch	%st(1)
	fsubp	%st, %st(2)
	fxch	%st(1)
	fstpt	(%rsp)
	jl	.L158
	fstp	%st(0)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L156:
	pushq	152(%rsp)
	pushq	152(%rsp)
	call	__ieee754_y1l@PLT
	movl	%ebp, 16(%rsp)
	fildl	16(%rsp)
	fmulp	%st, %st(1)
	fstpt	16(%rsp)
	popq	%rcx
	popq	%rsi
	cmpb	$0, 96(%rsp)
	je	.L107
.L148:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 64(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L112:
	fldt	48(%rsp)
	fldt	32(%rsp)
	fsubrp	%st, %st(1)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L150:
	testl	$2147483647, %esi
	je	.L96
	fldt	144(%rsp)
	fadd	%st(0), %st
	fstpt	(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L151:
	testl	%ebx, %ebx
	fld1
	js	.L159
.L99:
	fchs
.L149:
	fdivs	.LC0(%rip)
	fstpt	(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L152:
	fldz
	fldt	144(%rsp)
	fmul	%st(1), %st
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L155:
#APP
# 391 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 48(%rsp)
# 0 "" 2
#NO_APP
	movb	$1, 96(%rsp)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L113:
	fldt	48(%rsp)
	faddp	%st, %st(1)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L159:
	andl	$1, %ebx
	je	.L99
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L125:
	fldt	16(%rsp)
	fstpt	(%rsp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L157:
	cmpb	$0, 96(%rsp)
	jne	.L160
.L109:
	fldz
	fstpt	(%rsp)
	jmp	.L95
.L160:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 64(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L109
	.size	__ieee754_ynl, .-__ieee754_ynl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	349923469
	.long	2423175810
	.long	16382
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	1056964608
	.align 4
.LC5:
	.long	1073741824
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	3892314112
	.long	1110919286
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	3520035244
	.long	2977044471
	.long	16396
	.long	0
	.align 16
.LC8:
	.long	2786846552
	.long	2454546732
	.long	16715
	.long	0
	.align 16
.LC9:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC10:
	.long	0
	.long	2147483648
	.long	32769
	.long	0
	.align 16
.LC13:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC14:
	.long	4294967295
	.long	4294967295
	.long	65534
	.long	0
