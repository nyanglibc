 .text
.globl __fmax
.type __fmax,@function
.align 1<<4
__fmax:
 ucomisd %xmm0, %xmm1
 jp 1f
 maxsd %xmm1, %xmm0
 jmp 2f
1: ucomisd %xmm1, %xmm1
 jp 3f
 movsd %xmm0, -8(%rsp)
 testb $0x8, -2(%rsp)
 jz 4f
 movsd %xmm1, %xmm0
 ret
3:
 ucomisd %xmm0, %xmm0
 jp 4f
 movsd %xmm1, -8(%rsp)
 testb $0x8, -2(%rsp)
 jz 4f
 ret
4:
 addsd %xmm1, %xmm0
2: ret
.size __fmax,.-__fmax
.weak fmax
fmax = __fmax
.weak fmaxf64
fmaxf64 = __fmax
.weak fmaxf32x
fmaxf32x = __fmax
