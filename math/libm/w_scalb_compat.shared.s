	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	sysv_scalb, @function
sysv_scalb:
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_scalb@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm3
	movsd	(%rsp), %xmm2
	andpd	%xmm1, %xmm5
	movsd	8(%rsp), %xmm4
	ucomisd	%xmm3, %xmm5
	jbe	.L2
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jnb	.L13
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm0, %xmm2
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	pxor	%xmm1, %xmm1
	movl	$0, %edx
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	movapd	%xmm4, %xmm1
	movl	$33, %edi
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	movapd	%xmm4, %xmm1
	movl	$32, %edi
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.size	sysv_scalb, .-sysv_scalb
	.p2align 4,,15
	.globl	__scalb
	.type	__scalb, @function
__scalb:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L33
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_scalb@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm4
	movsd	(%rsp), %xmm2
	andpd	%xmm1, %xmm5
	movsd	8(%rsp), %xmm3
	ucomisd	%xmm5, %xmm4
	jb	.L16
	ucomisd	.LC2(%rip), %xmm0
	jp	.L14
	je	.L16
.L14:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	ucomisd	%xmm0, %xmm0
	jp	.L34
	ucomisd	%xmm4, %xmm5
	jbe	.L20
	andpd	%xmm1, %xmm2
	ucomisd	%xmm4, %xmm2
	ja	.L14
.L27:
	andpd	%xmm3, %xmm1
	ucomisd	%xmm4, %xmm1
	ja	.L14
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L33:
	jmp	sysv_scalb
	.p2align 4,,10
	.p2align 3
.L20:
	ucomisd	.LC2(%rip), %xmm2
	jp	.L27
	jne	.L27
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L34:
	ucomisd	%xmm3, %xmm2
	jp	.L14
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L14
	.size	__scalb, .-__scalb
	.weak	scalb
	.set	scalb,__scalb
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	0
