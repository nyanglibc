	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ccos
	.type	__ccos, @function
__ccos:
	xorpd	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	jmp	__ccosh@PLT
	.size	__ccos, .-__ccos
	.weak	ccosf32x
	.set	ccosf32x,__ccos
	.weak	ccosf64
	.set	ccosf64,__ccos
	.weak	ccos
	.set	ccos,__ccos
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
