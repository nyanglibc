	.text
	.p2align 4,,15
	.globl	__lroundl
	.type	__lroundl, @function
__lroundl:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	56(%rsp), %rbx
	movq	48(%rsp), %r8
	movl	%ebx, %ecx
	movswl	%bx, %ebx
	movq	%r8, %rsi
	andl	$32767, %ecx
	sarl	$31, %ebx
	shrq	$32, %rsi
	leal	-16383(%rcx), %edx
	orl	$1, %ebx
	cmpl	$30, %edx
	jg	.L3
	testl	%edx, %edx
	js	.L31
	movl	$1073741824, %ebp
	movl	%edx, %ecx
	sarl	%cl, %ebp
	addl	%esi, %ebp
	jc	.L32
.L8:
	movl	$31, %ecx
	subl	%edx, %ecx
	shrl	%cl, %ebp
.L9:
	movslq	%ebx, %rax
	addq	$24, %rsp
	imulq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$62, %edx
	jg	.L10
	leal	-16414(%rcx), %edi
	movl	$-2147483648, %eax
	movl	%esi, %ebp
	movl	%edi, %ecx
	shrl	%cl, %eax
	xorl	%ecx, %ecx
	addl	%r8d, %eax
	setc	%cl
	cmpl	$1, %ecx
	sbbq	$-1, %rbp
	cmpl	$31, %edx
	je	.L9
	movl	$63, %ecx
	subl	%edx, %ecx
	shrl	%cl, %eax
	movl	%edi, %ecx
	salq	%cl, %rbp
	orq	%rax, %rbp
	cmpl	$1, %ebx
	jne	.L9
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rbp
	jne	.L9
	movl	$1, %edi
	call	feraiseexcept@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	fldt	48(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	orb	$12, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	fistpq	(%rsp)
	fldcw	14(%rsp)
	movq	(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movslq	%ebx, %rbx
	cmpl	$-1, %edx
	movl	$0, %eax
	cmove	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	shrl	%ebp
	addl	$1, %edx
	orl	$-2147483648, %ebp
	jmp	.L8
	.size	__lroundl, .-__lroundl
	.weak	lroundf64x
	.set	lroundf64x,__lroundl
	.weak	lroundl
	.set	lroundl,__lroundl
