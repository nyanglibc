	.text
	.p2align 4,,15
	.globl	__cacosh
	.type	__cacosh, @function
__cacosh:
	movq	.LC7(%rip), %xmm4
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm3
	andpd	%xmm4, %xmm2
	andpd	%xmm4, %xmm3
	ucomisd	%xmm2, %xmm2
	jp	.L2
	movsd	.LC8(%rip), %xmm5
	ucomisd	%xmm5, %xmm2
	jbe	.L45
	ucomisd	%xmm3, %xmm3
	jp	.L42
	ucomisd	%xmm5, %xmm3
	ja	.L46
	movmskpd	%xmm0, %eax
	pxor	%xmm2, %xmm2
	testb	$1, %al
	je	.L15
	movsd	.LC3(%rip), %xmm2
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L45:
	movsd	.LC9(%rip), %xmm6
	ucomisd	%xmm6, %xmm2
	jnb	.L4
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L4
	jne	.L4
	ucomisd	%xmm3, %xmm3
	jp	.L47
	ucomisd	%xmm5, %xmm3
	ja	.L27
	ucomisd	%xmm6, %xmm3
	jb	.L48
.L9:
	movapd	%xmm1, %xmm2
	subq	$40, %rsp
	movl	$1, %edi
	movq	.LC10(%rip), %xmm3
	movapd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 24(%rsp)
	xorpd	%xmm3, %xmm0
	movaps	%xmm3, (%rsp)
	call	__kernel_casinh@PLT
	movsd	24(%rsp), %xmm2
	movapd	(%rsp), %xmm3
	movmskpd	%xmm2, %eax
	testb	$1, %al
	jne	.L49
	xorpd	%xmm3, %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	%xmm3, %xmm3
	jp	.L37
	ucomisd	%xmm5, %xmm3
	jbe	.L9
.L27:
	movsd	.LC1(%rip), %xmm2
.L15:
	movapd	%xmm1, %xmm7
	andpd	%xmm4, %xmm2
	movsd	.LC6(%rip), %xmm0
	andpd	.LC10(%rip), %xmm7
	orpd	%xmm7, %xmm2
	movapd	%xmm2, %xmm1
.L39:
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm3, %xmm3
	jp	.L37
	ucomisd	.LC8(%rip), %xmm3
	jbe	.L37
.L42:
	movsd	.LC5(%rip), %xmm1
	movsd	.LC6(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	xorpd	%xmm3, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	ucomisd	%xmm2, %xmm1
	jp	.L9
	jne	.L9
	andpd	.LC10(%rip), %xmm1
	pxor	%xmm0, %xmm0
	orpd	.LC11(%rip), %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movsd	.LC5(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L46:
	pxor	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.L50
	movsd	.LC0(%rip), %xmm2
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L47:
	movsd	.LC5(%rip), %xmm0
	movsd	.LC1(%rip), %xmm1
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L50:
	movsd	.LC2(%rip), %xmm2
	jmp	.L15
	.size	__cacosh, .-__cacosh
	.weak	cacoshf32x
	.set	cacoshf32x,__cacosh
	.weak	cacoshf64
	.set	cacoshf64,__cacosh
	.weak	cacosh
	.set	cacosh,__cacosh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1413754136
	.long	1072243195
	.align 8
.LC1:
	.long	1413754136
	.long	1073291771
	.align 8
.LC2:
	.long	2134057426
	.long	1073928572
	.align 8
.LC3:
	.long	1413754136
	.long	1074340347
	.align 8
.LC5:
	.long	0
	.long	2146959360
	.align 8
.LC6:
	.long	0
	.long	2146435072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC8:
	.long	4294967295
	.long	2146435071
	.align 8
.LC9:
	.long	0
	.long	1048576
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC11:
	.long	1413754136
	.long	1073291771
	.long	0
	.long	0
