	.text
	.p2align 4,,15
	.globl	__dubsin
	.type	__dubsin, @function
__dubsin:
	movsd	big(%rip), %xmm3
	leaq	__sincostab(%rip), %rdx
	movsd	.LC0(%rip), %xmm4
	movapd	%xmm3, %xmm2
	movsd	s7(%rip), %xmm6
	movsd	ss7(%rip), %xmm14
	addsd	%xmm0, %xmm2
	movapd	%xmm6, %xmm10
	movapd	%xmm6, %xmm12
	movq	%xmm2, %rax
	subsd	%xmm3, %xmm2
	sall	$2, %eax
	movslq	%eax, %rcx
	subsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm5
	addsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	movapd	%xmm5, %xmm8
	movapd	%xmm5, %xmm11
	movapd	%xmm0, %xmm7
	movapd	%xmm5, %xmm0
	addsd	%xmm1, %xmm7
	mulsd	%xmm4, %xmm0
	movsd	%xmm7, -24(%rsp)
	mulsd	%xmm5, %xmm7
	subsd	%xmm0, %xmm8
	addsd	%xmm0, %xmm8
	subsd	%xmm8, %xmm11
	movapd	%xmm8, %xmm1
	movapd	%xmm8, %xmm0
	mulsd	%xmm8, %xmm0
	mulsd	%xmm11, %xmm1
	movapd	%xmm11, %xmm9
	mulsd	%xmm11, %xmm9
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	addsd	%xmm7, %xmm1
	movsd	(%rdx,%rcx,8), %xmm7
	leal	1(%rax), %ecx
	addsd	%xmm9, %xmm0
	movapd	%xmm2, %xmm9
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	leal	2(%rax), %ecx
	addl	$3, %eax
	cltq
	addsd	%xmm1, %xmm0
	movslq	%ecx, %rcx
	movsd	%xmm3, -32(%rsp)
	movsd	(%rdx,%rcx,8), %xmm3
	movsd	%xmm3, -16(%rsp)
	addsd	%xmm0, %xmm9
	subsd	%xmm9, %xmm2
	movapd	%xmm9, %xmm1
	movapd	%xmm9, %xmm3
	mulsd	%xmm9, %xmm14
	addsd	%xmm0, %xmm2
	movapd	%xmm9, %xmm0
	mulsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm15
	movsd	%xmm2, -40(%rsp)
	movsd	(%rdx,%rax,8), %xmm2
	subsd	%xmm0, %xmm1
	movsd	%xmm2, -8(%rsp)
	addsd	%xmm1, %xmm0
	movapd	%xmm6, %xmm1
	mulsd	%xmm15, %xmm6
	mulsd	%xmm4, %xmm1
	movsd	ss5(%rip), %xmm15
	subsd	%xmm0, %xmm3
	movapd	%xmm0, %xmm2
	addsd	%xmm14, %xmm6
	subsd	%xmm1, %xmm10
	addsd	%xmm1, %xmm10
	movapd	%xmm0, %xmm1
	subsd	%xmm10, %xmm12
	mulsd	%xmm10, %xmm2
	mulsd	%xmm3, %xmm10
	mulsd	%xmm12, %xmm1
	mulsd	%xmm3, %xmm12
	addsd	%xmm10, %xmm1
	movapd	%xmm2, %xmm10
	addsd	%xmm1, %xmm10
	subsd	%xmm10, %xmm2
	movapd	%xmm10, %xmm13
	addsd	%xmm1, %xmm2
	addsd	%xmm12, %xmm2
	addsd	%xmm6, %xmm2
	movapd	%xmm10, %xmm6
	movsd	s5(%rip), %xmm10
	movapd	%xmm10, %xmm12
	addsd	%xmm2, %xmm6
	movapd	%xmm10, %xmm1
	subsd	%xmm6, %xmm13
	movapd	%xmm6, %xmm14
	addsd	%xmm6, %xmm1
	addsd	%xmm2, %xmm13
	movq	.LC1(%rip), %xmm2
	andpd	%xmm2, %xmm14
	andpd	%xmm2, %xmm12
	ucomisd	%xmm12, %xmm14
	jbe	.L34
	subsd	%xmm1, %xmm6
	addsd	%xmm10, %xmm6
	addsd	%xmm15, %xmm6
	addsd	%xmm6, %xmm13
.L4:
	movapd	%xmm13, %xmm14
	movapd	%xmm0, %xmm12
	addsd	%xmm1, %xmm14
	movapd	%xmm14, %xmm6
	movapd	%xmm14, %xmm10
	movapd	%xmm14, %xmm15
	mulsd	%xmm4, %xmm6
	subsd	%xmm14, %xmm1
	mulsd	-40(%rsp), %xmm14
	subsd	%xmm6, %xmm10
	addsd	%xmm13, %xmm1
	movsd	s3(%rip), %xmm13
	addsd	%xmm10, %xmm6
	movapd	%xmm0, %xmm10
	mulsd	%xmm9, %xmm1
	subsd	%xmm6, %xmm15
	mulsd	%xmm6, %xmm12
	mulsd	%xmm3, %xmm6
	addsd	%xmm1, %xmm14
	mulsd	%xmm15, %xmm10
	mulsd	%xmm3, %xmm15
	addsd	%xmm10, %xmm6
	movapd	%xmm12, %xmm10
	addsd	%xmm6, %xmm10
	subsd	%xmm10, %xmm12
	movapd	%xmm10, %xmm1
	addsd	%xmm6, %xmm12
	movapd	%xmm13, %xmm6
	addsd	%xmm15, %xmm12
	movsd	ss3(%rip), %xmm15
	addsd	%xmm14, %xmm12
	addsd	%xmm12, %xmm1
	subsd	%xmm1, %xmm10
	movapd	%xmm1, %xmm14
	addsd	%xmm1, %xmm6
	andpd	%xmm2, %xmm14
	addsd	%xmm12, %xmm10
	movapd	%xmm13, %xmm12
	andpd	%xmm2, %xmm12
	ucomisd	%xmm12, %xmm14
	jbe	.L35
	subsd	%xmm6, %xmm1
	addsd	%xmm13, %xmm1
	addsd	%xmm15, %xmm1
	movapd	%xmm1, %xmm12
	addsd	%xmm10, %xmm12
.L7:
	movapd	%xmm12, %xmm13
	movapd	%xmm0, %xmm15
	addsd	%xmm6, %xmm13
	movapd	%xmm13, %xmm1
	movapd	%xmm13, %xmm10
	movapd	%xmm13, %xmm14
	mulsd	%xmm4, %xmm1
	subsd	%xmm13, %xmm6
	mulsd	-40(%rsp), %xmm13
	subsd	%xmm1, %xmm10
	addsd	%xmm12, %xmm6
	addsd	%xmm1, %xmm10
	movapd	%xmm0, %xmm1
	mulsd	%xmm9, %xmm6
	subsd	%xmm10, %xmm14
	mulsd	%xmm10, %xmm15
	mulsd	%xmm3, %xmm10
	addsd	%xmm6, %xmm13
	mulsd	%xmm14, %xmm1
	mulsd	%xmm3, %xmm14
	addsd	%xmm1, %xmm10
	movapd	%xmm15, %xmm1
	addsd	%xmm10, %xmm1
	subsd	%xmm1, %xmm15
	movapd	%xmm1, %xmm12
	addsd	%xmm10, %xmm15
	addsd	%xmm14, %xmm15
	addsd	%xmm13, %xmm15
	addsd	%xmm15, %xmm12
	movapd	%xmm12, %xmm6
	movapd	%xmm12, %xmm10
	movapd	%xmm12, %xmm13
	mulsd	%xmm4, %xmm6
	subsd	%xmm12, %xmm1
	subsd	%xmm6, %xmm10
	addsd	%xmm15, %xmm1
	movapd	%xmm5, %xmm15
	addsd	%xmm10, %xmm6
	movapd	%xmm8, %xmm10
	mulsd	%xmm5, %xmm1
	subsd	%xmm6, %xmm13
	mulsd	%xmm6, %xmm10
	mulsd	%xmm11, %xmm6
	mulsd	%xmm13, %xmm8
	mulsd	%xmm13, %xmm11
	movsd	-24(%rsp), %xmm13
	addsd	%xmm6, %xmm8
	movapd	%xmm10, %xmm6
	mulsd	%xmm13, %xmm12
	addsd	%xmm8, %xmm6
	addsd	%xmm1, %xmm12
	movapd	%xmm5, %xmm1
	andpd	%xmm2, %xmm1
	subsd	%xmm6, %xmm10
	movapd	%xmm6, %xmm14
	addsd	%xmm8, %xmm10
	addsd	%xmm11, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm10, %xmm14
	subsd	%xmm14, %xmm6
	addsd	%xmm14, %xmm15
	addsd	%xmm6, %xmm10
	movapd	%xmm14, %xmm6
	andpd	%xmm2, %xmm6
	ucomisd	%xmm1, %xmm6
	jbe	.L36
	subsd	%xmm15, %xmm14
	addsd	%xmm14, %xmm5
	addsd	%xmm13, %xmm5
	addsd	%xmm5, %xmm10
.L10:
	movsd	c8(%rip), %xmm8
	movapd	%xmm0, %xmm5
	movapd	%xmm0, %xmm12
	movapd	%xmm8, %xmm1
	movapd	%xmm8, %xmm11
	movsd	cc8(%rip), %xmm6
	mulsd	%xmm4, %xmm1
	movapd	%xmm10, %xmm14
	mulsd	%xmm9, %xmm6
	addsd	%xmm15, %xmm14
	subsd	%xmm1, %xmm11
	subsd	%xmm14, %xmm15
	addsd	%xmm1, %xmm11
	movapd	%xmm8, %xmm1
	mulsd	-40(%rsp), %xmm8
	addsd	%xmm10, %xmm15
	subsd	%xmm11, %xmm1
	mulsd	%xmm11, %xmm5
	mulsd	%xmm3, %xmm11
	movsd	%xmm15, -24(%rsp)
	addsd	%xmm8, %xmm6
	movsd	c6(%rip), %xmm8
	mulsd	%xmm1, %xmm12
	mulsd	%xmm3, %xmm1
	movapd	%xmm8, %xmm10
	andpd	%xmm2, %xmm10
	addsd	%xmm11, %xmm12
	movapd	%xmm5, %xmm11
	addsd	%xmm12, %xmm11
	subsd	%xmm11, %xmm5
	addsd	%xmm12, %xmm5
	addsd	%xmm1, %xmm5
	movapd	%xmm8, %xmm1
	addsd	%xmm5, %xmm6
	movapd	%xmm11, %xmm5
	addsd	%xmm6, %xmm5
	movapd	%xmm6, %xmm12
	subsd	%xmm5, %xmm11
	addsd	%xmm5, %xmm1
	movapd	%xmm11, %xmm6
	movsd	cc6(%rip), %xmm11
	addsd	%xmm12, %xmm6
	movapd	%xmm5, %xmm12
	andpd	%xmm2, %xmm12
	ucomisd	%xmm10, %xmm12
	jbe	.L37
	subsd	%xmm1, %xmm5
	addsd	%xmm8, %xmm5
	addsd	%xmm11, %xmm5
	movapd	%xmm5, %xmm10
	addsd	%xmm6, %xmm10
.L13:
	movapd	%xmm10, %xmm12
	movapd	%xmm0, %xmm15
	movapd	%xmm0, %xmm8
	addsd	%xmm1, %xmm12
	movapd	%xmm12, %xmm5
	movapd	%xmm12, %xmm6
	movapd	%xmm12, %xmm13
	mulsd	%xmm4, %xmm5
	subsd	%xmm12, %xmm1
	mulsd	-40(%rsp), %xmm12
	subsd	%xmm5, %xmm6
	addsd	%xmm10, %xmm1
	addsd	%xmm6, %xmm5
	mulsd	%xmm9, %xmm1
	subsd	%xmm5, %xmm13
	mulsd	%xmm5, %xmm15
	mulsd	%xmm3, %xmm5
	addsd	%xmm1, %xmm12
	movsd	c4(%rip), %xmm1
	mulsd	%xmm13, %xmm8
	movapd	%xmm15, %xmm11
	mulsd	%xmm3, %xmm13
	addsd	%xmm8, %xmm5
	movapd	%xmm1, %xmm8
	andpd	%xmm2, %xmm8
	addsd	%xmm5, %xmm11
	subsd	%xmm11, %xmm15
	movapd	%xmm11, %xmm10
	addsd	%xmm5, %xmm15
	movapd	%xmm1, %xmm5
	addsd	%xmm13, %xmm15
	addsd	%xmm12, %xmm15
	addsd	%xmm15, %xmm10
	movapd	%xmm10, %xmm12
	subsd	%xmm10, %xmm11
	andpd	%xmm2, %xmm12
	addsd	%xmm10, %xmm5
	ucomisd	%xmm8, %xmm12
	movapd	%xmm11, %xmm6
	movsd	cc4(%rip), %xmm11
	addsd	%xmm15, %xmm6
	jbe	.L38
	subsd	%xmm5, %xmm10
	addsd	%xmm1, %xmm10
	movapd	%xmm10, %xmm8
	addsd	%xmm11, %xmm8
	addsd	%xmm6, %xmm8
.L16:
	movapd	%xmm8, %xmm11
	movapd	%xmm0, %xmm13
	movapd	%xmm0, %xmm10
	addsd	%xmm5, %xmm11
	movapd	%xmm11, %xmm1
	movapd	%xmm11, %xmm6
	movapd	%xmm11, %xmm12
	mulsd	%xmm4, %xmm1
	subsd	%xmm11, %xmm5
	mulsd	-40(%rsp), %xmm11
	subsd	%xmm1, %xmm6
	addsd	%xmm8, %xmm5
	addsd	%xmm6, %xmm1
	mulsd	%xmm9, %xmm5
	subsd	%xmm1, %xmm12
	mulsd	%xmm1, %xmm13
	mulsd	%xmm3, %xmm1
	addsd	%xmm5, %xmm11
	movsd	c2(%rip), %xmm5
	mulsd	%xmm12, %xmm10
	mulsd	%xmm3, %xmm12
	addsd	%xmm10, %xmm1
	movapd	%xmm13, %xmm10
	addsd	%xmm1, %xmm10
	subsd	%xmm10, %xmm13
	movapd	%xmm10, %xmm8
	movapd	%xmm10, %xmm6
	movsd	cc2(%rip), %xmm10
	addsd	%xmm1, %xmm13
	movapd	%xmm5, %xmm1
	addsd	%xmm12, %xmm13
	addsd	%xmm11, %xmm13
	movapd	%xmm5, %xmm11
	andpd	%xmm2, %xmm11
	addsd	%xmm13, %xmm8
	movapd	%xmm8, %xmm12
	subsd	%xmm8, %xmm6
	andpd	%xmm2, %xmm12
	addsd	%xmm8, %xmm1
	ucomisd	%xmm11, %xmm12
	addsd	%xmm13, %xmm6
	jbe	.L39
	subsd	%xmm1, %xmm8
	addsd	%xmm5, %xmm8
	addsd	%xmm8, %xmm10
	addsd	%xmm10, %xmm6
.L19:
	movapd	%xmm6, %xmm12
	addsd	%xmm1, %xmm12
	movapd	%xmm12, %xmm5
	movapd	%xmm12, %xmm10
	movapd	%xmm12, %xmm8
	mulsd	%xmm4, %xmm5
	subsd	%xmm12, %xmm1
	mulsd	-40(%rsp), %xmm12
	subsd	%xmm5, %xmm10
	addsd	%xmm1, %xmm6
	movsd	-16(%rsp), %xmm1
	addsd	%xmm5, %xmm10
	movapd	%xmm0, %xmm5
	mulsd	%xmm6, %xmm9
	movapd	%xmm14, %xmm6
	subsd	%xmm10, %xmm8
	mulsd	%xmm10, %xmm5
	mulsd	%xmm3, %xmm10
	addsd	%xmm12, %xmm9
	movapd	%xmm14, %xmm12
	mulsd	%xmm8, %xmm0
	movapd	%xmm5, %xmm11
	mulsd	%xmm8, %xmm3
	movapd	%xmm1, %xmm8
	addsd	%xmm0, %xmm10
	movapd	%xmm1, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm10, %xmm11
	subsd	%xmm0, %xmm8
	subsd	%xmm11, %xmm5
	addsd	%xmm0, %xmm8
	movapd	%xmm14, %xmm0
	mulsd	-8(%rsp), %xmm14
	mulsd	%xmm4, %xmm0
	addsd	%xmm10, %xmm5
	movapd	%xmm1, %xmm10
	mulsd	-24(%rsp), %xmm1
	subsd	%xmm8, %xmm10
	subsd	%xmm0, %xmm6
	addsd	%xmm3, %xmm5
	movapd	%xmm11, %xmm3
	addsd	%xmm1, %xmm14
	addsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm5
	movapd	%xmm8, %xmm9
	movapd	%xmm14, %xmm1
	subsd	%xmm6, %xmm12
	mulsd	%xmm6, %xmm9
	mulsd	%xmm10, %xmm6
	addsd	%xmm5, %xmm3
	mulsd	%xmm12, %xmm8
	movapd	%xmm9, %xmm0
	subsd	%xmm3, %xmm11
	addsd	%xmm6, %xmm8
	movapd	%xmm9, %xmm6
	movapd	%xmm3, %xmm9
	addsd	%xmm11, %xmm5
	addsd	%xmm8, %xmm6
	mulsd	%xmm7, %xmm5
	subsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm8
	movapd	%xmm10, %xmm0
	movapd	%xmm7, %xmm10
	mulsd	%xmm12, %xmm0
	addsd	%xmm8, %xmm0
	addsd	%xmm0, %xmm1
	movapd	%xmm6, %xmm0
	addsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm14
	movapd	%xmm3, %xmm6
	addsd	%xmm1, %xmm14
	movapd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm6
	mulsd	%xmm7, %xmm4
	mulsd	-32(%rsp), %xmm3
	subsd	%xmm6, %xmm1
	addsd	%xmm6, %xmm1
	movapd	%xmm7, %xmm6
	addsd	%xmm3, %xmm5
	subsd	%xmm4, %xmm6
	subsd	%xmm1, %xmm9
	addsd	%xmm6, %xmm4
	movapd	%xmm1, %xmm6
	subsd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm6
	mulsd	%xmm9, %xmm4
	mulsd	%xmm10, %xmm1
	movapd	%xmm6, %xmm8
	mulsd	%xmm10, %xmm9
	addsd	%xmm4, %xmm1
	addsd	%xmm1, %xmm8
	subsd	%xmm8, %xmm6
	movapd	%xmm8, %xmm3
	addsd	%xmm6, %xmm1
	movapd	%xmm0, %xmm6
	andpd	%xmm2, %xmm6
	addsd	%xmm9, %xmm1
	addsd	%xmm1, %xmm5
	movapd	%xmm0, %xmm1
	addsd	%xmm5, %xmm3
	movapd	%xmm3, %xmm4
	subsd	%xmm3, %xmm8
	andpd	%xmm2, %xmm4
	subsd	%xmm3, %xmm1
	ucomisd	%xmm4, %xmm6
	addsd	%xmm8, %xmm5
	jbe	.L40
	subsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	subsd	%xmm5, %xmm0
	addsd	%xmm14, %xmm0
.L22:
	movapd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm4
	subsd	%xmm3, %xmm1
	andpd	%xmm2, %xmm4
	andpd	%xmm7, %xmm2
	addsd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	ucomisd	%xmm2, %xmm4
	addsd	%xmm3, %xmm1
	ja	.L43
	subsd	%xmm1, %xmm7
	addsd	%xmm3, %xmm7
	addsd	%xmm0, %xmm7
	addsd	-32(%rsp), %xmm7
.L25:
	movapd	%xmm7, %xmm0
	addsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm1
	movsd	%xmm0, (%rdi)
	addsd	%xmm1, %xmm7
	movsd	%xmm7, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	subsd	%xmm1, %xmm10
	addsd	%xmm6, %xmm10
	addsd	%xmm10, %xmm13
	addsd	%xmm15, %xmm13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L40:
	addsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	addsd	%xmm14, %xmm0
	subsd	%xmm5, %xmm0
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L39:
	subsd	%xmm1, %xmm5
	addsd	%xmm8, %xmm5
	addsd	%xmm5, %xmm6
	addsd	%xmm10, %xmm6
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L38:
	subsd	%xmm5, %xmm1
	addsd	%xmm10, %xmm1
	movapd	%xmm1, %xmm8
	addsd	%xmm6, %xmm8
	addsd	%xmm11, %xmm8
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L37:
	subsd	%xmm1, %xmm8
	addsd	%xmm5, %xmm8
	movapd	%xmm8, %xmm10
	addsd	%xmm6, %xmm10
	addsd	%xmm11, %xmm10
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L36:
	subsd	%xmm15, %xmm5
	addsd	%xmm5, %xmm14
	addsd	%xmm14, %xmm10
	addsd	-24(%rsp), %xmm10
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L35:
	subsd	%xmm6, %xmm13
	addsd	%xmm1, %xmm13
	addsd	%xmm10, %xmm13
	addsd	%xmm15, %xmm13
	movapd	%xmm13, %xmm12
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L43:
	subsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm7
	addsd	-32(%rsp), %xmm7
	addsd	%xmm0, %xmm7
	jmp	.L25
	.size	__dubsin, .-__dubsin
	.p2align 4,,15
	.globl	__dubcos
	.type	__dubcos, @function
__dubcos:
	movsd	big(%rip), %xmm3
	leaq	__sincostab(%rip), %rdx
	movsd	.LC0(%rip), %xmm4
	movapd	%xmm3, %xmm2
	movsd	s7(%rip), %xmm7
	movsd	ss7(%rip), %xmm14
	addsd	%xmm0, %xmm2
	movapd	%xmm7, %xmm10
	movapd	%xmm7, %xmm12
	movsd	ss5(%rip), %xmm15
	movq	%xmm2, %rax
	subsd	%xmm3, %xmm2
	sall	$2, %eax
	movslq	%eax, %rcx
	subsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm5
	addsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	movapd	%xmm5, %xmm8
	movapd	%xmm5, %xmm11
	movapd	%xmm0, %xmm6
	movapd	%xmm5, %xmm0
	addsd	%xmm1, %xmm6
	mulsd	%xmm4, %xmm0
	movsd	%xmm6, -16(%rsp)
	mulsd	%xmm5, %xmm6
	subsd	%xmm0, %xmm8
	addsd	%xmm0, %xmm8
	subsd	%xmm8, %xmm11
	movapd	%xmm8, %xmm1
	movapd	%xmm8, %xmm0
	mulsd	%xmm8, %xmm0
	mulsd	%xmm11, %xmm1
	movapd	%xmm11, %xmm9
	mulsd	%xmm11, %xmm9
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm6, %xmm1
	addsd	%xmm6, %xmm1
	movsd	(%rdx,%rcx,8), %xmm6
	leal	1(%rax), %ecx
	addsd	%xmm9, %xmm0
	movapd	%xmm2, %xmm9
	movslq	%ecx, %rcx
	movsd	%xmm6, -32(%rsp)
	movsd	(%rdx,%rcx,8), %xmm6
	leal	2(%rax), %ecx
	addl	$3, %eax
	cltq
	addsd	%xmm1, %xmm0
	movsd	(%rdx,%rax,8), %xmm3
	movslq	%ecx, %rcx
	movsd	%xmm6, -8(%rsp)
	movsd	%xmm3, -24(%rsp)
	movsd	(%rdx,%rcx,8), %xmm6
	addsd	%xmm0, %xmm9
	subsd	%xmm9, %xmm2
	movapd	%xmm9, %xmm1
	movapd	%xmm9, %xmm3
	mulsd	%xmm9, %xmm14
	addsd	%xmm0, %xmm2
	movapd	%xmm9, %xmm0
	mulsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm13
	movsd	%xmm2, -40(%rsp)
	subsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm0
	movapd	%xmm7, %xmm1
	mulsd	%xmm13, %xmm7
	mulsd	%xmm4, %xmm1
	subsd	%xmm0, %xmm3
	movapd	%xmm0, %xmm2
	addsd	%xmm14, %xmm7
	subsd	%xmm1, %xmm10
	addsd	%xmm1, %xmm10
	movapd	%xmm0, %xmm1
	subsd	%xmm10, %xmm12
	mulsd	%xmm10, %xmm2
	mulsd	%xmm3, %xmm10
	mulsd	%xmm12, %xmm1
	mulsd	%xmm3, %xmm12
	addsd	%xmm10, %xmm1
	movapd	%xmm2, %xmm10
	addsd	%xmm1, %xmm10
	subsd	%xmm10, %xmm2
	movapd	%xmm10, %xmm13
	addsd	%xmm1, %xmm2
	addsd	%xmm12, %xmm2
	addsd	%xmm7, %xmm2
	movapd	%xmm10, %xmm7
	movsd	s5(%rip), %xmm10
	movapd	%xmm10, %xmm12
	addsd	%xmm2, %xmm7
	movapd	%xmm10, %xmm1
	subsd	%xmm7, %xmm13
	movapd	%xmm7, %xmm14
	addsd	%xmm7, %xmm1
	addsd	%xmm2, %xmm13
	movq	.LC1(%rip), %xmm2
	andpd	%xmm2, %xmm14
	andpd	%xmm2, %xmm12
	ucomisd	%xmm12, %xmm14
	jbe	.L77
	subsd	%xmm1, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm15, %xmm7
	addsd	%xmm7, %xmm13
.L47:
	movapd	%xmm13, %xmm14
	movapd	%xmm0, %xmm12
	addsd	%xmm1, %xmm14
	movapd	%xmm14, %xmm7
	movapd	%xmm14, %xmm10
	movapd	%xmm14, %xmm15
	mulsd	%xmm4, %xmm7
	subsd	%xmm14, %xmm1
	mulsd	-40(%rsp), %xmm14
	subsd	%xmm7, %xmm10
	addsd	%xmm13, %xmm1
	movsd	s3(%rip), %xmm13
	addsd	%xmm10, %xmm7
	movapd	%xmm0, %xmm10
	mulsd	%xmm9, %xmm1
	subsd	%xmm7, %xmm15
	mulsd	%xmm7, %xmm12
	mulsd	%xmm3, %xmm7
	addsd	%xmm1, %xmm14
	mulsd	%xmm15, %xmm10
	mulsd	%xmm3, %xmm15
	addsd	%xmm10, %xmm7
	movapd	%xmm12, %xmm10
	addsd	%xmm7, %xmm10
	subsd	%xmm10, %xmm12
	movapd	%xmm10, %xmm1
	addsd	%xmm7, %xmm12
	movapd	%xmm13, %xmm7
	addsd	%xmm15, %xmm12
	movsd	ss3(%rip), %xmm15
	addsd	%xmm14, %xmm12
	addsd	%xmm12, %xmm1
	subsd	%xmm1, %xmm10
	movapd	%xmm1, %xmm14
	addsd	%xmm1, %xmm7
	andpd	%xmm2, %xmm14
	addsd	%xmm12, %xmm10
	movapd	%xmm13, %xmm12
	andpd	%xmm2, %xmm12
	ucomisd	%xmm12, %xmm14
	jbe	.L78
	subsd	%xmm7, %xmm1
	addsd	%xmm13, %xmm1
	addsd	%xmm15, %xmm1
	movapd	%xmm1, %xmm12
	addsd	%xmm10, %xmm12
.L50:
	movapd	%xmm12, %xmm13
	movapd	%xmm0, %xmm15
	addsd	%xmm7, %xmm13
	movapd	%xmm13, %xmm1
	movapd	%xmm13, %xmm10
	movapd	%xmm13, %xmm14
	mulsd	%xmm4, %xmm1
	subsd	%xmm13, %xmm7
	mulsd	-40(%rsp), %xmm13
	subsd	%xmm1, %xmm10
	addsd	%xmm12, %xmm7
	addsd	%xmm1, %xmm10
	movapd	%xmm0, %xmm1
	mulsd	%xmm9, %xmm7
	subsd	%xmm10, %xmm14
	mulsd	%xmm10, %xmm15
	mulsd	%xmm3, %xmm10
	addsd	%xmm7, %xmm13
	mulsd	%xmm14, %xmm1
	mulsd	%xmm3, %xmm14
	addsd	%xmm1, %xmm10
	movapd	%xmm15, %xmm1
	addsd	%xmm10, %xmm1
	subsd	%xmm1, %xmm15
	movapd	%xmm1, %xmm12
	addsd	%xmm10, %xmm15
	addsd	%xmm14, %xmm15
	addsd	%xmm13, %xmm15
	addsd	%xmm15, %xmm12
	movapd	%xmm12, %xmm7
	movapd	%xmm12, %xmm10
	movapd	%xmm12, %xmm13
	mulsd	%xmm4, %xmm7
	subsd	%xmm12, %xmm1
	subsd	%xmm7, %xmm10
	addsd	%xmm15, %xmm1
	movapd	%xmm5, %xmm15
	addsd	%xmm10, %xmm7
	movapd	%xmm8, %xmm10
	mulsd	%xmm5, %xmm1
	subsd	%xmm7, %xmm13
	mulsd	%xmm7, %xmm10
	mulsd	%xmm11, %xmm7
	mulsd	%xmm13, %xmm8
	mulsd	%xmm13, %xmm11
	movsd	-16(%rsp), %xmm13
	addsd	%xmm7, %xmm8
	movapd	%xmm10, %xmm7
	mulsd	%xmm13, %xmm12
	addsd	%xmm8, %xmm7
	addsd	%xmm1, %xmm12
	movapd	%xmm5, %xmm1
	andpd	%xmm2, %xmm1
	subsd	%xmm7, %xmm10
	movapd	%xmm7, %xmm14
	addsd	%xmm8, %xmm10
	addsd	%xmm11, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm10, %xmm14
	subsd	%xmm14, %xmm7
	addsd	%xmm14, %xmm15
	addsd	%xmm7, %xmm10
	movapd	%xmm14, %xmm7
	andpd	%xmm2, %xmm7
	ucomisd	%xmm1, %xmm7
	jbe	.L79
	subsd	%xmm15, %xmm14
	addsd	%xmm14, %xmm5
	addsd	%xmm13, %xmm5
	addsd	%xmm5, %xmm10
.L53:
	movsd	c8(%rip), %xmm8
	movapd	%xmm0, %xmm12
	movsd	cc8(%rip), %xmm7
	movapd	%xmm8, %xmm1
	movapd	%xmm8, %xmm11
	movapd	%xmm8, %xmm5
	mulsd	-40(%rsp), %xmm8
	mulsd	%xmm4, %xmm1
	movapd	%xmm10, %xmm14
	mulsd	%xmm9, %xmm7
	addsd	%xmm15, %xmm14
	subsd	%xmm1, %xmm11
	addsd	%xmm8, %xmm7
	movsd	c6(%rip), %xmm8
	subsd	%xmm14, %xmm15
	addsd	%xmm1, %xmm11
	movapd	%xmm0, %xmm1
	addsd	%xmm10, %xmm15
	movapd	%xmm8, %xmm10
	subsd	%xmm11, %xmm5
	mulsd	%xmm11, %xmm1
	mulsd	%xmm3, %xmm11
	andpd	%xmm2, %xmm10
	movsd	%xmm15, -16(%rsp)
	mulsd	%xmm5, %xmm12
	mulsd	%xmm3, %xmm5
	addsd	%xmm11, %xmm12
	movapd	%xmm1, %xmm11
	addsd	%xmm12, %xmm11
	subsd	%xmm11, %xmm1
	addsd	%xmm12, %xmm1
	addsd	%xmm5, %xmm1
	movapd	%xmm8, %xmm5
	addsd	%xmm1, %xmm7
	movapd	%xmm11, %xmm1
	addsd	%xmm7, %xmm1
	movapd	%xmm7, %xmm12
	subsd	%xmm1, %xmm11
	addsd	%xmm1, %xmm5
	movapd	%xmm11, %xmm7
	movsd	cc6(%rip), %xmm11
	addsd	%xmm12, %xmm7
	movapd	%xmm1, %xmm12
	andpd	%xmm2, %xmm12
	ucomisd	%xmm10, %xmm12
	jbe	.L80
	subsd	%xmm5, %xmm1
	addsd	%xmm8, %xmm1
	addsd	%xmm11, %xmm1
	movapd	%xmm1, %xmm10
	addsd	%xmm7, %xmm10
.L56:
	movapd	%xmm10, %xmm12
	movapd	%xmm0, %xmm15
	movapd	%xmm0, %xmm8
	addsd	%xmm5, %xmm12
	movapd	%xmm12, %xmm1
	movapd	%xmm12, %xmm7
	movapd	%xmm12, %xmm13
	mulsd	%xmm4, %xmm1
	subsd	%xmm12, %xmm5
	mulsd	-40(%rsp), %xmm12
	subsd	%xmm1, %xmm7
	addsd	%xmm10, %xmm5
	addsd	%xmm7, %xmm1
	mulsd	%xmm9, %xmm5
	subsd	%xmm1, %xmm13
	mulsd	%xmm1, %xmm15
	mulsd	%xmm3, %xmm1
	addsd	%xmm5, %xmm12
	movsd	c4(%rip), %xmm5
	mulsd	%xmm13, %xmm8
	movapd	%xmm15, %xmm11
	mulsd	%xmm3, %xmm13
	addsd	%xmm8, %xmm1
	addsd	%xmm1, %xmm11
	subsd	%xmm11, %xmm15
	movapd	%xmm11, %xmm10
	movapd	%xmm11, %xmm8
	movsd	cc4(%rip), %xmm11
	addsd	%xmm1, %xmm15
	movapd	%xmm5, %xmm1
	addsd	%xmm13, %xmm15
	addsd	%xmm12, %xmm15
	addsd	%xmm15, %xmm10
	subsd	%xmm10, %xmm8
	movapd	%xmm10, %xmm12
	addsd	%xmm10, %xmm1
	andpd	%xmm2, %xmm12
	movapd	%xmm8, %xmm7
	movapd	%xmm5, %xmm8
	addsd	%xmm15, %xmm7
	andpd	%xmm2, %xmm8
	ucomisd	%xmm8, %xmm12
	jbe	.L81
	subsd	%xmm1, %xmm10
	addsd	%xmm5, %xmm10
	movapd	%xmm10, %xmm8
	addsd	%xmm11, %xmm8
	addsd	%xmm7, %xmm8
.L59:
	movapd	%xmm8, %xmm11
	movapd	%xmm0, %xmm13
	movapd	%xmm0, %xmm10
	addsd	%xmm1, %xmm11
	movapd	%xmm11, %xmm5
	movapd	%xmm11, %xmm7
	movapd	%xmm11, %xmm12
	mulsd	%xmm4, %xmm5
	subsd	%xmm11, %xmm1
	mulsd	-40(%rsp), %xmm11
	subsd	%xmm5, %xmm7
	addsd	%xmm8, %xmm1
	addsd	%xmm7, %xmm5
	mulsd	%xmm9, %xmm1
	subsd	%xmm5, %xmm12
	mulsd	%xmm5, %xmm13
	mulsd	%xmm3, %xmm5
	addsd	%xmm1, %xmm11
	mulsd	%xmm12, %xmm10
	mulsd	%xmm3, %xmm12
	addsd	%xmm10, %xmm5
	movapd	%xmm13, %xmm10
	addsd	%xmm5, %xmm10
	subsd	%xmm10, %xmm13
	movapd	%xmm10, %xmm8
	movapd	%xmm10, %xmm7
	movsd	cc2(%rip), %xmm10
	addsd	%xmm5, %xmm13
	movsd	c2(%rip), %xmm5
	movapd	%xmm5, %xmm1
	addsd	%xmm12, %xmm13
	addsd	%xmm11, %xmm13
	movapd	%xmm5, %xmm11
	andpd	%xmm2, %xmm11
	addsd	%xmm13, %xmm8
	movapd	%xmm8, %xmm12
	subsd	%xmm8, %xmm7
	andpd	%xmm2, %xmm12
	addsd	%xmm8, %xmm1
	ucomisd	%xmm11, %xmm12
	addsd	%xmm13, %xmm7
	jbe	.L82
	subsd	%xmm1, %xmm8
	addsd	%xmm5, %xmm8
	addsd	%xmm8, %xmm10
	addsd	%xmm10, %xmm7
.L62:
	movapd	%xmm7, %xmm11
	movsd	-32(%rsp), %xmm15
	addsd	%xmm1, %xmm11
	movapd	%xmm15, %xmm13
	movapd	%xmm11, %xmm5
	movapd	%xmm11, %xmm8
	movapd	%xmm11, %xmm10
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm8
	addsd	%xmm5, %xmm8
	movapd	%xmm0, %xmm5
	subsd	%xmm8, %xmm10
	mulsd	%xmm8, %xmm5
	mulsd	%xmm3, %xmm8
	mulsd	%xmm10, %xmm0
	mulsd	%xmm10, %xmm3
	addsd	%xmm8, %xmm0
	movapd	%xmm5, %xmm8
	addsd	%xmm0, %xmm8
	subsd	%xmm8, %xmm5
	movapd	%xmm8, %xmm10
	addsd	%xmm0, %xmm5
	movapd	%xmm6, %xmm0
	mulsd	%xmm4, %xmm0
	addsd	%xmm3, %xmm5
	movapd	%xmm1, %xmm3
	movapd	%xmm14, %xmm1
	subsd	%xmm11, %xmm3
	mulsd	-40(%rsp), %xmm11
	addsd	%xmm3, %xmm7
	movapd	%xmm7, %xmm3
	mulsd	%xmm9, %xmm3
	movapd	%xmm6, %xmm9
	subsd	%xmm0, %xmm9
	addsd	%xmm11, %xmm3
	movapd	%xmm6, %xmm11
	addsd	%xmm0, %xmm9
	movapd	%xmm14, %xmm0
	addsd	%xmm5, %xmm3
	mulsd	%xmm4, %xmm0
	movapd	%xmm14, %xmm5
	mulsd	-8(%rsp), %xmm14
	subsd	%xmm9, %xmm11
	addsd	%xmm3, %xmm10
	subsd	%xmm0, %xmm1
	movapd	%xmm10, %xmm7
	addsd	%xmm0, %xmm1
	movapd	%xmm10, %xmm0
	movapd	%xmm10, %xmm12
	mulsd	%xmm4, %xmm0
	subsd	%xmm10, %xmm8
	mulsd	%xmm15, %xmm4
	subsd	%xmm1, %xmm5
	mulsd	-24(%rsp), %xmm10
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm0
	movapd	%xmm15, %xmm7
	subsd	%xmm4, %xmm7
	subsd	%xmm0, %xmm12
	addsd	%xmm7, %xmm4
	movapd	%xmm1, %xmm7
	subsd	%xmm4, %xmm13
	mulsd	%xmm4, %xmm7
	mulsd	%xmm5, %xmm4
	mulsd	%xmm13, %xmm1
	mulsd	%xmm13, %xmm5
	addsd	%xmm1, %xmm4
	movapd	%xmm7, %xmm1
	addsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm7
	movapd	%xmm1, %xmm15
	addsd	%xmm4, %xmm7
	addsd	%xmm7, %xmm5
	movsd	-32(%rsp), %xmm7
	mulsd	-16(%rsp), %xmm7
	addsd	%xmm14, %xmm7
	addsd	%xmm5, %xmm7
	movapd	%xmm9, %xmm5
	mulsd	%xmm12, %xmm9
	mulsd	%xmm0, %xmm5
	mulsd	%xmm11, %xmm0
	mulsd	%xmm12, %xmm11
	addsd	%xmm7, %xmm1
	movapd	%xmm5, %xmm4
	addsd	%xmm0, %xmm9
	subsd	%xmm1, %xmm15
	movapd	%xmm1, %xmm14
	addsd	%xmm9, %xmm4
	addsd	%xmm7, %xmm15
	subsd	%xmm4, %xmm5
	movapd	%xmm4, %xmm13
	addsd	%xmm9, %xmm5
	addsd	%xmm11, %xmm5
	movapd	%xmm8, %xmm11
	addsd	%xmm3, %xmm11
	movapd	%xmm1, %xmm3
	andpd	%xmm2, %xmm3
	mulsd	%xmm6, %xmm11
	addsd	%xmm10, %xmm11
	addsd	%xmm5, %xmm11
	addsd	%xmm11, %xmm13
	movapd	%xmm13, %xmm0
	subsd	%xmm13, %xmm4
	andpd	%xmm2, %xmm0
	addsd	%xmm13, %xmm14
	ucomisd	%xmm0, %xmm3
	addsd	%xmm4, %xmm11
	jbe	.L83
	subsd	%xmm14, %xmm1
	addsd	%xmm1, %xmm13
	addsd	%xmm13, %xmm11
	addsd	%xmm11, %xmm15
.L65:
	movapd	%xmm15, %xmm1
	movapd	%xmm6, %xmm3
	movapd	%xmm6, %xmm0
	addsd	%xmm14, %xmm1
	andpd	%xmm2, %xmm3
	andpd	%xmm1, %xmm2
	subsd	%xmm1, %xmm14
	subsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm3
	addsd	%xmm14, %xmm15
	ja	.L85
	addsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm6
	addsd	-24(%rsp), %xmm6
	subsd	%xmm15, %xmm6
.L68:
	movapd	%xmm6, %xmm1
	addsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm1, (%rdi)
	addsd	%xmm0, %xmm6
	movsd	%xmm6, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	subsd	%xmm1, %xmm10
	addsd	%xmm7, %xmm10
	addsd	%xmm10, %xmm13
	addsd	%xmm15, %xmm13
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L83:
	subsd	%xmm14, %xmm13
	addsd	%xmm13, %xmm1
	addsd	%xmm1, %xmm15
	addsd	%xmm11, %xmm15
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L82:
	subsd	%xmm1, %xmm5
	addsd	%xmm8, %xmm5
	addsd	%xmm5, %xmm7
	addsd	%xmm10, %xmm7
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L81:
	subsd	%xmm1, %xmm5
	addsd	%xmm10, %xmm5
	movapd	%xmm5, %xmm8
	addsd	%xmm7, %xmm8
	addsd	%xmm11, %xmm8
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L80:
	subsd	%xmm5, %xmm8
	addsd	%xmm1, %xmm8
	movapd	%xmm8, %xmm10
	addsd	%xmm7, %xmm10
	addsd	%xmm11, %xmm10
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L79:
	subsd	%xmm15, %xmm5
	addsd	%xmm5, %xmm14
	addsd	%xmm14, %xmm10
	addsd	-16(%rsp), %xmm10
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L78:
	subsd	%xmm7, %xmm13
	addsd	%xmm1, %xmm13
	addsd	%xmm10, %xmm13
	addsd	%xmm15, %xmm13
	movapd	%xmm13, %xmm12
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L85:
	subsd	%xmm0, %xmm6
	subsd	%xmm1, %xmm6
	subsd	%xmm15, %xmm6
	addsd	-24(%rsp), %xmm6
	jmp	.L68
	.size	__dubcos, .-__dubcos
	.p2align 4,,15
	.globl	__docos
	.type	__docos, @function
__docos:
	pxor	%xmm4, %xmm4
	subq	$40, %rsp
	movq	%rdi, %rsi
	ucomisd	%xmm4, %xmm0
	ja	.L87
	movq	.LC3(%rip), %xmm2
	xorpd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm1
.L87:
	movsd	hp0(%rip), %xmm2
	movsd	.LC4(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	ucomisd	%xmm0, %xmm3
	ja	.L102
	movsd	.LC5(%rip), %xmm5
	movsd	hp1(%rip), %xmm3
	mulsd	%xmm2, %xmm5
	ucomisd	%xmm0, %xmm5
	jbe	.L99
	subsd	%xmm0, %xmm2
	leaq	16(%rsp), %rdi
	subsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	addsd	%xmm3, %xmm0
	subsd	%xmm0, %xmm2
	ucomisd	%xmm4, %xmm0
	addsd	%xmm2, %xmm1
	jbe	.L100
	call	__dubsin
	movsd	16(%rsp), %xmm0
	movsd	%xmm0, (%rsi)
	movsd	24(%rsp), %xmm0
	movsd	%xmm0, 8(%rsi)
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	addsd	%xmm2, %xmm2
	leaq	16(%rsp), %rdi
	addsd	%xmm3, %xmm3
	subsd	%xmm0, %xmm2
	subsd	%xmm1, %xmm3
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	addsd	%xmm3, %xmm0
	subsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	call	__dubcos
	movq	.LC3(%rip), %xmm2
	movsd	16(%rsp), %xmm1
	movsd	24(%rsp), %xmm0
	xorpd	%xmm2, %xmm1
	xorpd	%xmm2, %xmm0
	movsd	%xmm1, (%rsi)
	movsd	%xmm0, 8(%rsi)
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	leaq	16(%rsp), %rdi
	call	__dubcos
	movsd	16(%rsp), %xmm0
	movsd	%xmm0, (%rsi)
	movsd	24(%rsp), %xmm0
	movsd	%xmm0, 8(%rsi)
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movq	.LC3(%rip), %xmm2
	xorpd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm1
	movaps	%xmm2, (%rsp)
	call	__dubsin
	movapd	(%rsp), %xmm2
	movsd	16(%rsp), %xmm0
	xorpd	%xmm2, %xmm0
	movsd	%xmm0, (%rsi)
	movsd	24(%rsp), %xmm0
	xorpd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsi)
	addq	$40, %rsp
	ret
	.size	__docos, .-__docos
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	hp1, @object
	.size	hp1, 8
hp1:
	.long	856972295
	.long	1016178214
	.align 8
	.type	hp0, @object
	.size	hp0, 8
hp0:
	.long	1413754136
	.long	1073291771
	.align 8
	.type	big, @object
	.size	big, 8
big:
	.long	0
	.long	1120403456
	.align 8
	.type	cc8, @object
	.size	cc8, 8
cc8:
	.long	1929373132
	.long	997898014
	.align 8
	.type	c8, @object
	.size	c8, 8
c8:
	.long	-2112005753
	.long	-1090911841
	.align 8
	.type	cc6, @object
	.size	cc6, 8
cc6:
	.long	-432771308
	.long	-1143805882
	.align 8
	.type	c6, @object
	.size	c6, 8
c6:
	.long	381774486
	.long	1062650220
	.align 8
	.type	cc4, @object
	.size	cc4, 8
cc4:
	.long	805250430
	.long	-1136307012
	.align 8
	.type	c4, @object
	.size	c4, 8
c4:
	.long	1431655765
	.long	-1079683755
	.align 8
	.type	cc2, @object
	.size	cc2, 8
cc2:
	.long	0
	.long	-1171771432
	.align 8
	.type	c2, @object
	.size	c2, 8
c2:
	.long	0
	.long	1071644672
	.align 8
	.type	ss7, @object
	.size	ss7, 8
ss7:
	.long	1780006698
	.long	1003343561
	.align 8
	.type	s7, @object
	.size	s7, 8
s7:
	.long	1477887885
	.long	-1087766113
	.align 8
	.type	ss5, @object
	.size	ss5, 8
ss5:
	.long	-632780768
	.long	-1138643962
	.align 8
	.type	s5, @object
	.size	s5, 8
s5:
	.long	286330645
	.long	1065423121
	.align 8
	.type	ss3, @object
	.size	ss3, 8
ss3:
	.long	-411114366
	.long	-1134210134
	.align 8
	.type	s3, @object
	.size	s3, 8
s3:
	.long	1431655765
	.long	-1077586603
	.align 8
.LC0:
	.long	33554432
	.long	1101004800
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1071644672
	.align 8
.LC5:
	.long	0
	.long	1073217536
