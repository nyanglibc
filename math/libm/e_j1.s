	.text
	.p2align 4,,15
	.type	pone, @function
pone:
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1102053375, %eax
	jg	.L4
	cmpl	$1075838975, %eax
	jle	.L9
	pxor	%xmm14, %xmm14
	movsd	.LC0(%rip), %xmm2
	movsd	.LC1(%rip), %xmm8
	movsd	.LC2(%rip), %xmm3
	movsd	.LC3(%rip), %xmm11
	movsd	.LC4(%rip), %xmm6
	movsd	.LC5(%rip), %xmm13
	movsd	.LC6(%rip), %xmm1
	movsd	.LC7(%rip), %xmm15
	movsd	.LC8(%rip), %xmm12
	movsd	.LC9(%rip), %xmm9
.L3:
	movsd	.LC44(%rip), %xmm5
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm4
	divsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm1
	mulsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm12
	addsd	%xmm15, %xmm1
	mulsd	%xmm4, %xmm6
	addsd	%xmm11, %xmm3
	mulsd	%xmm4, %xmm9
	mulsd	%xmm4, %xmm2
	movapd	%xmm10, %xmm7
	addsd	%xmm12, %xmm14
	mulsd	%xmm10, %xmm7
	addsd	%xmm6, %xmm13
	mulsd	%xmm10, %xmm1
	addsd	%xmm5, %xmm9
	mulsd	%xmm10, %xmm3
	addsd	%xmm8, %xmm2
	mulsd	%xmm7, %xmm13
	addsd	%xmm14, %xmm1
	addsd	%xmm9, %xmm3
	mulsd	%xmm7, %xmm2
	addsd	%xmm13, %xmm1
	addsd	%xmm3, %xmm2
	divsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$1074933386, %eax
	jg	.L6
	cmpl	$1074191212, %eax
	jg	.L7
	movsd	.LC33(%rip), %xmm2
	movsd	.LC34(%rip), %xmm8
	movsd	.LC35(%rip), %xmm3
	movsd	.LC36(%rip), %xmm11
	movsd	.LC37(%rip), %xmm6
	movsd	.LC38(%rip), %xmm13
	movsd	.LC39(%rip), %xmm1
	movsd	.LC40(%rip), %xmm15
	movsd	.LC41(%rip), %xmm12
	movsd	.LC42(%rip), %xmm9
	movsd	.LC43(%rip), %xmm14
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movsd	.LC44(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movsd	.LC11(%rip), %xmm2
	movsd	.LC12(%rip), %xmm8
	movsd	.LC13(%rip), %xmm3
	movsd	.LC14(%rip), %xmm11
	movsd	.LC15(%rip), %xmm6
	movsd	.LC16(%rip), %xmm13
	movsd	.LC17(%rip), %xmm1
	movsd	.LC18(%rip), %xmm15
	movsd	.LC19(%rip), %xmm12
	movsd	.LC20(%rip), %xmm9
	movsd	.LC21(%rip), %xmm14
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movsd	.LC22(%rip), %xmm2
	movsd	.LC23(%rip), %xmm8
	movsd	.LC24(%rip), %xmm3
	movsd	.LC25(%rip), %xmm11
	movsd	.LC26(%rip), %xmm6
	movsd	.LC27(%rip), %xmm13
	movsd	.LC28(%rip), %xmm1
	movsd	.LC29(%rip), %xmm15
	movsd	.LC30(%rip), %xmm12
	movsd	.LC31(%rip), %xmm9
	movsd	.LC32(%rip), %xmm14
	jmp	.L3
	.size	pone, .-pone
	.p2align 4,,15
	.type	qone, @function
qone:
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1102053375, %eax
	jg	.L17
	cmpl	$1075838975, %eax
	jle	.L18
	pxor	%xmm14, %xmm14
	movsd	.LC45(%rip), %xmm9
	movsd	.LC46(%rip), %xmm11
	movsd	.LC47(%rip), %xmm10
	movsd	.LC48(%rip), %xmm3
	movsd	.LC49(%rip), %xmm12
	movsd	.LC50(%rip), %xmm5
	movsd	.LC51(%rip), %xmm13
	movsd	.LC52(%rip), %xmm1
	movsd	.LC53(%rip), %xmm15
	movsd	.LC54(%rip), %xmm7
	movsd	.LC55(%rip), %xmm6
.L13:
	movapd	%xmm0, %xmm2
	movsd	.LC44(%rip), %xmm4
	mulsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm4
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm7
	movapd	%xmm4, %xmm8
	mulsd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm12
	mulsd	%xmm11, %xmm2
	addsd	.LC44(%rip), %xmm6
	mulsd	%xmm4, %xmm8
	addsd	%xmm7, %xmm14
	addsd	%xmm5, %xmm13
	mulsd	%xmm4, %xmm12
	addsd	%xmm15, %xmm1
	addsd	%xmm2, %xmm10
	mulsd	%xmm8, %xmm13
	mulsd	%xmm4, %xmm1
	addsd	%xmm12, %xmm6
	mulsd	%xmm8, %xmm10
	mulsd	%xmm8, %xmm4
	addsd	%xmm1, %xmm14
	addsd	%xmm6, %xmm10
	mulsd	%xmm4, %xmm9
	addsd	%xmm14, %xmm13
	addsd	%xmm9, %xmm10
	divsd	%xmm10, %xmm13
	addsd	.LC92(%rip), %xmm13
	divsd	%xmm0, %xmm13
	movapd	%xmm13, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$1074933386, %eax
	jg	.L15
	cmpl	$1074191212, %eax
	jg	.L16
	movsd	.LC80(%rip), %xmm9
	movsd	.LC81(%rip), %xmm11
	movsd	.LC82(%rip), %xmm10
	movsd	.LC83(%rip), %xmm3
	movsd	.LC84(%rip), %xmm12
	movsd	.LC85(%rip), %xmm5
	movsd	.LC86(%rip), %xmm13
	movsd	.LC87(%rip), %xmm1
	movsd	.LC88(%rip), %xmm15
	movsd	.LC89(%rip), %xmm7
	movsd	.LC90(%rip), %xmm6
	movsd	.LC91(%rip), %xmm14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	.LC92(%rip), %xmm13
	divsd	%xmm0, %xmm13
	movapd	%xmm13, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movsd	.LC56(%rip), %xmm9
	movsd	.LC57(%rip), %xmm11
	movsd	.LC58(%rip), %xmm10
	movsd	.LC59(%rip), %xmm3
	movsd	.LC60(%rip), %xmm12
	movsd	.LC61(%rip), %xmm5
	movsd	.LC62(%rip), %xmm13
	movsd	.LC63(%rip), %xmm1
	movsd	.LC64(%rip), %xmm15
	movsd	.LC65(%rip), %xmm7
	movsd	.LC66(%rip), %xmm6
	movsd	.LC67(%rip), %xmm14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L16:
	movsd	.LC68(%rip), %xmm9
	movsd	.LC69(%rip), %xmm11
	movsd	.LC70(%rip), %xmm10
	movsd	.LC71(%rip), %xmm3
	movsd	.LC72(%rip), %xmm12
	movsd	.LC73(%rip), %xmm5
	movsd	.LC74(%rip), %xmm13
	movsd	.LC75(%rip), %xmm1
	movsd	.LC76(%rip), %xmm15
	movsd	.LC77(%rip), %xmm7
	movsd	.LC78(%rip), %xmm6
	movsd	.LC79(%rip), %xmm14
	jmp	.L13
	.size	qone, .-qone
	.p2align 4,,15
	.globl	__ieee754_j1
	.type	__ieee754_j1, @function
__ieee754_j1:
	pushq	%rbp
	pushq	%rbx
	movq	%xmm0, %rbx
	movapd	%xmm0, %xmm1
	shrq	$32, %rbx
	subq	$88, %rsp
	movl	%ebx, %ebp
	andl	$2147483647, %ebp
	cmpl	$2146435071, %ebp
	jg	.L44
	cmpl	$1073741823, %ebp
	jg	.L45
	movsd	.LC96(%rip), %xmm0
	cmpl	$1044381695, %ebp
	movsd	.LC44(%rip), %xmm7
	mulsd	%xmm1, %xmm0
	jle	.L46
.L28:
	movapd	%xmm1, %xmm2
	movsd	.LC99(%rip), %xmm4
	movsd	.LC101(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm4
	movapd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm6
	addsd	.LC100(%rip), %xmm4
	movapd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm4
	addsd	%xmm3, %xmm4
	movsd	.LC102(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm4
	movsd	.LC103(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	%xmm4, %xmm1
	movsd	.LC105(%rip), %xmm4
	addsd	.LC104(%rip), %xmm3
	mulsd	%xmm2, %xmm4
	mulsd	.LC106(%rip), %xmm2
	addsd	%xmm7, %xmm4
	mulsd	%xmm6, %xmm3
	addsd	.LC107(%rip), %xmm2
	addsd	%xmm4, %xmm3
	mulsd	%xmm5, %xmm2
	addsd	%xmm3, %xmm2
	divsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
.L19:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	andpd	.LC93(%rip), %xmm1
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movapd	%xmm1, %xmm0
	movsd	%xmm1, (%rsp)
	call	__sincos@PLT
	movsd	64(%rsp), %xmm0
	cmpl	$2145386495, %ebp
	movq	.LC94(%rip), %xmm5
	movapd	%xmm0, %xmm2
	movsd	72(%rsp), %xmm4
	movsd	(%rsp), %xmm1
	xorpd	%xmm5, %xmm2
	subsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm3
	movapd	%xmm0, %xmm2
	subsd	%xmm4, %xmm3
	jle	.L47
.L23:
	cmpl	$1207959552, %ebp
	sqrtsd	%xmm1, %xmm4
	jg	.L48
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 40(%rsp)
	movaps	%xmm5, 48(%rsp)
	movsd	%xmm3, 32(%rsp)
	movsd	%xmm4, 16(%rsp)
	movsd	%xmm1, 8(%rsp)
	call	pone
	movsd	8(%rsp), %xmm1
	movsd	%xmm0, (%rsp)
	movapd	%xmm1, %xmm0
	call	qone
	movsd	40(%rsp), %xmm2
	movapd	%xmm0, %xmm1
	movsd	32(%rsp), %xmm3
	movsd	(%rsp), %xmm0
	mulsd	%xmm1, %xmm3
	movapd	48(%rsp), %xmm5
	mulsd	%xmm2, %xmm0
	movsd	16(%rsp), %xmm4
	subsd	%xmm3, %xmm0
	mulsd	.LC95(%rip), %xmm0
	divsd	%xmm4, %xmm0
.L27:
	testl	%ebx, %ebx
	jns	.L19
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	xorpd	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movsd	.LC95(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	divsd	%xmm4, %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm1, %xmm0
	movaps	%xmm5, 16(%rsp)
	addsd	%xmm1, %xmm0
	movsd	%xmm3, 32(%rsp)
	call	__cos@PLT
	movsd	64(%rsp), %xmm4
	movsd	(%rsp), %xmm1
	mulsd	72(%rsp), %xmm4
	movsd	8(%rsp), %xmm2
	movapd	16(%rsp), %xmm5
	ucomisd	.LC10(%rip), %xmm4
	jbe	.L42
	movsd	32(%rsp), %xmm3
	divsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm2
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L44:
	movsd	.LC44(%rip), %xmm0
	addq	$88, %rsp
	popq	%rbx
	divsd	%xmm1, %xmm0
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movsd	.LC97(%rip), %xmm2
	addsd	%xmm1, %xmm2
	ucomisd	%xmm7, %xmm2
	jbe	.L28
	movapd	%xmm0, %xmm2
	movsd	.LC98(%rip), %xmm3
	andpd	.LC93(%rip), %xmm2
	ucomisd	%xmm2, %xmm3
	jbe	.L30
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
.L30:
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	ucomisd	%xmm2, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L19
	ucomisd	%xmm2, %xmm1
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L19
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L42:
	divsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm3
	jmp	.L23
	.size	__ieee754_j1, .-__ieee754_j1
	.p2align 4,,15
	.globl	__ieee754_y1
	.type	__ieee754_y1, @function
__ieee754_y1:
	movq	%xmm0, %rdx
	pushq	%rbx
	shrq	$32, %rdx
	subq	$64, %rsp
	movl	%edx, %ebx
	andl	$2147483647, %ebx
	cmpl	$2146435071, %ebx
	jg	.L63
	movq	%xmm0, %rax
	orl	%ebx, %eax
	je	.L64
	testl	%edx, %edx
	js	.L65
	cmpl	$1073741823, %ebx
	movapd	%xmm0, %xmm2
	jg	.L66
	cmpl	$1016070144, %ebx
	jle	.L67
	movapd	%xmm0, %xmm1
	movsd	.LC113(%rip), %xmm3
	movsd	%xmm2, 24(%rsp)
	mulsd	%xmm0, %xmm1
	movsd	.LC111(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm4
	subsd	.LC112(%rip), %xmm0
	subsd	.LC114(%rip), %xmm3
	movapd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm0
	addsd	%xmm3, %xmm0
	movsd	.LC115(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm0
	movsd	.LC116(%rip), %xmm3
	mulsd	%xmm1, %xmm3
	addsd	.LC117(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	movsd	.LC118(%rip), %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	.LC119(%rip), %xmm1
	addsd	.LC44(%rip), %xmm4
	addsd	.LC120(%rip), %xmm1
	addsd	%xmm4, %xmm3
	mulsd	%xmm5, %xmm1
	addsd	%xmm3, %xmm1
	divsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm2, %xmm0
	call	__ieee754_j1
	movsd	24(%rsp), %xmm2
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm2, %xmm0
	call	__ieee754_log@PLT
	movsd	.LC44(%rip), %xmm1
	movsd	24(%rsp), %xmm2
	mulsd	16(%rsp), %xmm0
	divsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	mulsd	.LC121(%rip), %xmm0
	addsd	8(%rsp), %xmm0
.L49:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdi
	movsd	%xmm0, 8(%rsp)
	call	__sincos@PLT
	movsd	48(%rsp), %xmm0
	cmpl	$2145386495, %ebx
	movsd	56(%rsp), %xmm3
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm2
	subsd	%xmm3, %xmm0
	xorpd	.LC94(%rip), %xmm1
	subsd	%xmm3, %xmm1
	movapd	%xmm0, %xmm3
	jle	.L68
.L55:
	cmpl	$1207959552, %ebx
	sqrtsd	%xmm2, %xmm4
	jle	.L58
	mulsd	.LC95(%rip), %xmm1
	addq	$64, %rsp
	popq	%rbx
	movapd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movapd	%xmm2, %xmm0
	movsd	%xmm3, 40(%rsp)
	movsd	%xmm1, 32(%rsp)
	movsd	%xmm4, 24(%rsp)
	movsd	%xmm2, 16(%rsp)
	call	pone
	movsd	16(%rsp), %xmm2
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm2, %xmm0
	call	qone
	movsd	32(%rsp), %xmm1
	movsd	40(%rsp), %xmm3
	mulsd	8(%rsp), %xmm1
	mulsd	%xmm3, %xmm0
	movsd	24(%rsp), %xmm4
	addsd	%xmm1, %xmm0
	mulsd	.LC95(%rip), %xmm0
	addq	$64, %rsp
	popq	%rbx
	divsd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm2, %xmm0
	movsd	%xmm1, 24(%rsp)
	addsd	%xmm2, %xmm0
	call	__cos@PLT
	movsd	48(%rsp), %xmm4
	movsd	8(%rsp), %xmm2
	mulsd	56(%rsp), %xmm4
	movsd	16(%rsp), %xmm3
	ucomisd	.LC10(%rip), %xmm4
	jbe	.L61
	movapd	%xmm0, %xmm3
	movsd	24(%rsp), %xmm1
	divsd	%xmm1, %xmm3
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L63:
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm0, %xmm2
	movsd	.LC44(%rip), %xmm0
	addq	$64, %rsp
	popq	%rbx
	divsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movsd	.LC108(%rip), %xmm0
	divsd	.LC10(%rip), %xmm0
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L65:
	pxor	%xmm1, %xmm1
	mulsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L67:
	movsd	.LC109(%rip), %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	andpd	.LC93(%rip), %xmm1
	ucomisd	.LC110(%rip), %xmm1
	jbe	.L49
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L61:
	movapd	%xmm0, %xmm1
	divsd	%xmm3, %xmm1
	jmp	.L55
	.size	__ieee754_y1, .-__ieee754_y1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1769605933
	.long	1088296209
	.align 8
.LC1:
	.long	2995722171
	.long	1089983532
	.align 8
.LC2:
	.long	2546318207
	.long	1088555910
	.align 8
.LC3:
	.long	2521638735
	.long	1085048284
	.align 8
.LC4:
	.long	3274146525
	.long	1086253690
	.align 8
.LC5:
	.long	2745512652
	.long	1085162877
	.align 8
.LC6:
	.long	1697555856
	.long	1081721044
	.align 8
.LC7:
	.long	897548238
	.long	1076525725
	.align 8
.LC8:
	.long	4294966478
	.long	1069416447
	.align 8
.LC9:
	.long	2389011628
	.long	1079807301
	.align 8
.LC10:
	.long	0
	.long	0
	.align 8
.LC11:
	.long	57630289
	.long	1083670576
	.align 8
.LC12:
	.long	3097869077
	.long	1086235824
	.align 8
.LC13:
	.long	1460057851
	.long	1085598020
	.align 8
.LC14:
	.long	453404417
	.long	1083112246
	.align 8
.LC15:
	.long	3145600183
	.long	1082164664
	.align 8
.LC16:
	.long	3495089737
	.long	1082141974
	.align 8
.LC17:
	.long	1160119021
	.long	1079710649
	.align 8
.LC18:
	.long	1851987427
	.long	1075525124
	.align 8
.LC19:
	.long	3804299331
	.long	1069416447
	.align 8
.LC20:
	.long	2830066493
	.long	1078830058
	.align 8
.LC21:
	.long	3672230525
	.long	1034749543
	.align 8
.LC22:
	.long	2083450195
	.long	1079636589
	.align 8
.LC23:
	.long	2737713641
	.long	1082906237
	.align 8
.LC24:
	.long	1345836323
	.long	1083202428
	.align 8
.LC25:
	.long	133277535
	.long	1081412659
	.align 8
.LC26:
	.long	2393390821
	.long	1078478735
	.align 8
.LC27:
	.long	1294735415
	.long	1079427973
	.align 8
.LC28:
	.long	2644955433
	.long	1078038344
	.align 8
.LC29:
	.long	3898518922
	.long	1074755260
	.align 8
.LC30:
	.long	1528942971
	.long	1069416437
	.align 8
.LC31:
	.long	2704541340
	.long	1078027593
	.align 8
.LC32:
	.long	2813173469
	.long	1042938913
	.align 8
.LC33:
	.long	4098773394
	.long	1075886769
	.align 8
.LC34:
	.long	3659039913
	.long	1079864186
	.align 8
.LC35:
	.long	3584220121
	.long	1080887512
	.align 8
.LC36:
	.long	351874261
	.long	1079988883
	.align 8
.LC37:
	.long	2775892478
	.long	1075071817
	.align 8
.LC38:
	.long	394235618
	.long	1076998568
	.align 8
.LC39:
	.long	2138155364
	.long	1076395063
	.align 8
.LC40:
	.long	4186943168
	.long	1073935031
	.align 8
.LC41:
	.long	3195407747
	.long	1069416258
	.align 8
.LC42:
	.long	2329275612
	.long	1077243837
	.align 8
.LC43:
	.long	4132783348
	.long	1048373716
	.align 8
.LC44:
	.long	0
	.long	1072693248
	.align 8
.LC45:
	.long	245738008
	.long	-1055786647
	.align 8
.LC46:
	.long	1998171484
	.long	1092900818
	.align 8
.LC47:
	.long	1921424409
	.long	1093006931
	.align 8
.LC48:
	.long	2964807320
	.long	1090541466
	.align 8
.LC49:
	.long	3503850521
	.long	1086230882
	.align 8
.LC50:
	.long	1708170346
	.long	-1058560304
	.align 8
.LC51:
	.long	1090024469
	.long	-1060690713
	.align 8
.LC52:
	.long	1407497590
	.long	-1064846128
	.align 8
.LC53:
	.long	2724690423
	.long	-1070578287
	.align 8
.LC54:
	.long	4294966771
	.long	-1078312961
	.align 8
.LC55:
	.long	3730558437
	.long	1080306854
	.align 8
.LC56:
	.long	4244611076
	.long	-1062047954
	.align 8
.LC57:
	.long	3481024075
	.long	1088113412
	.align 8
.LC58:
	.long	2864370071
	.long	1088968557
	.align 8
.LC59:
	.long	224710185
	.long	1087442719
	.align 8
.LC60:
	.long	3883661369
	.long	1084170033
	.align 8
.LC61:
	.long	2298324893
	.long	-1062967069
	.align 8
.LC62:
	.long	1764848463
	.long	-1063947066
	.align 8
.LC63:
	.long	1822931376
	.long	-1066994323
	.align 8
.LC64:
	.long	3389238603
	.long	-1071637274
	.align 8
.LC65:
	.long	3411640303
	.long	-1078312961
	.align 8
.LC66:
	.long	4284092850
	.long	1079267762
	.align 8
.LC67:
	.long	446800024
	.long	-1112081853
	.align 8
.LC68:
	.long	688533791
	.long	-1067391376
	.align 8
.LC69:
	.long	232054603
	.long	1084079226
	.align 8
.LC70:
	.long	2786940070
	.long	1085647802
	.align 8
.LC71:
	.long	1150188698
	.long	1084909646
	.align 8
.LC72:
	.long	3224497726
	.long	1082461931
	.align 8
.LC73:
	.long	1599871990
	.long	-1066703175
	.align 8
.LC74:
	.long	1192809823
	.long	-1066629165
	.align 8
.LC75:
	.long	3260880602
	.long	-1068700559
	.align 8
.LC76:
	.long	855824895
	.long	-1072533310
	.align 8
.LC77:
	.long	1370418516
	.long	-1078312981
	.align 8
.LC78:
	.long	3436406756
	.long	1078449443
	.align 8
.LC79:
	.long	3549415503
	.long	-1103769687
	.align 8
.LC80:
	.long	3877365867
	.long	-1072441722
	.align 8
.LC81:
	.long	1010751700
	.long	1080262238
	.align 8
.LC82:
	.long	1221902377
	.long	1082596133
	.align 8
.LC83:
	.long	3460931831
	.long	1082633221
	.align 8
.LC84:
	.long	3682737338
	.long	1081057128
	.align 8
.LC85:
	.long	969895506
	.long	-1070244042
	.align 8
.LC86:
	.long	3725609642
	.long	-1069209181
	.align 8
.LC87:
	.long	3244855423
	.long	-1070355998
	.align 8
.LC88:
	.long	1773883098
	.long	-1073347452
	.align 8
.LC89:
	.long	2437459984
	.long	-1078313330
	.align 8
.LC90:
	.long	2024695039
	.long	1077774474
	.align 8
.LC91:
	.long	1153836754
	.long	-1098387162
	.align 8
.LC92:
	.long	0
	.long	1071120384
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC93:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC94:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC95:
	.long	1346542445
	.long	1071779287
	.align 8
.LC96:
	.long	0
	.long	1071644672
	.align 8
.LC97:
	.long	2281731484
	.long	2117592124
	.align 8
.LC98:
	.long	0
	.long	1048576
	.align 8
.LC99:
	.long	3122042472
	.long	-1091516986
	.align 8
.LC100:
	.long	2554801249
	.long	1062669727
	.align 8
.LC101:
	.long	0
	.long	-1078984704
	.align 8
.LC102:
	.long	1187646425
	.long	1047177978
	.align 8
.LC103:
	.long	859800728
	.long	1051967480
	.align 8
.LC104:
	.long	3117282916
	.long	1059610454
	.align 8
.LC105:
	.long	308510291
	.long	1066638603
	.align 8
.LC106:
	.long	3485040344
	.long	1034627791
	.align 8
.LC107:
	.long	3380477740
	.long	1043704968
	.align 8
.LC108:
	.long	0
	.long	-1074790400
	.align 8
.LC109:
	.long	1841940611
	.long	-1075552464
	.align 8
.LC110:
	.long	4294967295
	.long	2146435071
	.align 8
.LC111:
	.long	2410068110
	.long	1056484099
	.align 8
.LC112:
	.long	1212478735
	.long	1063212517
	.align 8
.LC113:
	.long	1982409937
	.long	1068094407
	.align 8
.LC114:
	.long	339524746
	.long	1070143590
	.align 8
.LC115:
	.long	1452344760
	.long	-1099387904
	.align 8
.LC116:
	.long	2303626406
	.long	1052164186
	.align 8
.LC117:
	.long	1814394724
	.long	1059753097
	.align 8
.LC118:
	.long	1062054384
	.long	1066689805
	.align 8
.LC119:
	.long	3670701866
	.long	1035096121
	.align 8
.LC120:
	.long	1537645190
	.long	1044037405
	.align 8
.LC121:
	.long	1841940611
	.long	1071931184
