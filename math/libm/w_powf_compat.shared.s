	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __powf_compat,powf@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__powf_compat
	.type	__powf_compat, @function
__powf_compat:
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_powf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm5
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm5
	movss	8(%rsp), %xmm2
	movss	12(%rsp), %xmm4
	ucomiss	%xmm5, %xmm3
	jb	.L26
	pxor	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm0
	jp	.L1
	je	.L27
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movaps	%xmm2, %xmm5
	andps	%xmm1, %xmm5
	ucomiss	%xmm5, %xmm3
	jb	.L1
	andps	%xmm4, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.L1
	ucomiss	%xmm0, %xmm0
	jp	.L28
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm2
	jp	.L6
	jne	.L6
	ucomiss	%xmm4, %xmm1
	ja	.L29
.L6:
	movaps	%xmm4, %xmm1
	movaps	%xmm2, %xmm0
	movl	$121, %edi
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	movaps	%xmm2, %xmm6
	andps	%xmm1, %xmm6
	ucomiss	%xmm6, %xmm3
	jb	.L1
	ucomiss	%xmm5, %xmm2
	jp	.L16
	je	.L1
.L16:
	andps	%xmm4, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movaps	%xmm4, %xmm1
	movaps	%xmm2, %xmm0
	movl	$122, %edi
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
.L29:
	movd	%xmm2, %eax
	testl	%eax, %eax
	jns	.L9
	movd	%xmm0, %eax
	testl	%eax, %eax
	js	.L30
.L9:
	movaps	%xmm4, %xmm1
	movaps	%xmm2, %xmm0
	movl	$143, %edi
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
.L30:
	movaps	%xmm4, %xmm1
	movl	$123, %edi
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
.L28:
	movaps	%xmm4, %xmm1
	movl	$124, %edi
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	__powf_compat, .-__powf_compat
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
