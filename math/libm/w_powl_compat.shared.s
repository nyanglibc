	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__powl
	.type	__powl, @function
__powl:
	subq	$72, %rsp
	fldt	80(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	48(%rsp)
	fstpt	16(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__ieee754_powl@PLT
	fld	%st(0)
	addq	$32, %rsp
	fabs
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	fldt	(%rsp)
	jb	.L26
	fldz
	fxch	%st(3)
	fucomi	%st(3), %st
	jp	.L31
	je	.L27
	fstp	%st(2)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L1
.L31:
	fstp	%st(2)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L32:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L39:
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L1
.L40:
	fstp	%st(0)
	fstp	%st(1)
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L32
	fld	%st(0)
	fabs
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jb	.L33
	fldt	16(%rsp)
	fabs
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jb	.L34
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L28
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	jp	.L35
	jne	.L36
	fldt	16(%rsp)
	fxch	%st(3)
	fucomip	%st(3), %st
	ja	.L29
	fstp	%st(1)
	fstp	%st(1)
	jmp	.L6
.L35:
	fstp	%st(1)
	fstp	%st(1)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L36:
	fstp	%st(1)
	fstp	%st(1)
.L6:
	fldt	16(%rsp)
	movl	$221, %edi
	fstpt	64(%rsp)
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	fld	%st(1)
	fabs
	fxch	%st(3)
	fucomip	%st(3), %st
	fstp	%st(2)
	jb	.L37
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L16
	je	.L38
.L16:
	fldt	.LC0(%rip)
	fldt	16(%rsp)
	fld	%st(0)
	fabs
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jb	.L39
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L40
	fstp	%st(1)
	fstpt	64(%rsp)
	movl	$222, %edi
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
.L29:
	fxam
	fnstsw	%ax
	testb	$2, %ah
	je	.L41
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L30
	fstp	%st(1)
	jmp	.L9
.L41:
	fstp	%st(1)
	fstp	%st(1)
.L9:
	fldt	16(%rsp)
	movl	$243, %edi
	fstpt	64(%rsp)
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
.L30:
	fxch	%st(1)
	fstpt	64(%rsp)
	movl	$223, %edi
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
.L28:
	fstp	%st(0)
	fldt	16(%rsp)
	movl	$224, %edi
	fstpt	64(%rsp)
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
	.size	__powl, .-__powl
	.weak	powf64x
	.set	powf64x,__powl
	.weak	powl
	.set	powl,__powl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
