	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__lttf2
	.globl	__subtf3
	.p2align 4,,15
	.globl	__csinhf128
	.type	__csinhf128, @function
__csinhf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movdqa	160(%rsp), %xmm3
	pand	.LC5(%rip), %xmm3
	movdqa	176(%rsp), %xmm6
	movdqa	.LC5(%rip), %xmm4
	movaps	160(%rsp), %xmm5
	pand	%xmm6, %xmm4
	movdqa	%xmm3, %xmm1
	movmskps	%xmm5, %ebp
	movdqa	%xmm3, %xmm0
	andl	$8, %ebp
	movaps	%xmm6, 48(%rsp)
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm3, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L83
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L45
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L45
	movdqa	.LC7(%rip), %xmm7
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm7, %xmm1
	movaps	%xmm7, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L84
.L17:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L71
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	176(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	96(%rsp), %xmm4
	movaps	%xmm4, 48(%rsp)
	movdqa	112(%rsp), %xmm1
.L36:
	movdqa	.LC3(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC3(%rip), %xmm0
	call	__copysignf128@PLT
	testl	%ebp, %ebp
	je	.L85
	movdqa	(%rsp), %xmm5
	pxor	.LC11(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
.L34:
	movq	%rbx, %rax
	movdqa	16(%rsp), %xmm7
	movdqa	(%rsp), %xmm5
	movaps	%xmm7, (%rbx)
	movaps	%xmm5, 16(%rbx)
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movdqa	.LC7(%rip), %xmm6
	movdqa	(%rsp), %xmm0
	movdqa	%xmm6, %xmm1
	movaps	%xmm6, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	160(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	16(%rsp), %xmm0
	testq	%rax, %rax
	movdqa	%xmm0, %xmm1
	jne	.L81
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L11
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L49
.L11:
	testl	%ebp, %ebp
	jne	.L86
	movdqa	.LC1(%rip), %xmm1
.L35:
	pxor	%xmm0, %xmm0
	call	__copysignf128@PLT
	movdqa	176(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
.L81:
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L8
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L49
.L8:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movdqa	.LC4(%rip), %xmm2
	movaps	%xmm2, 16(%rsp)
	movaps	%xmm2, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L49:
	movdqa	.LC9(%rip), %xmm1
	movdqa	.LC10(%rip), %xmm0
	call	__multf3@PLT
	call	__fixtfsi@PLT
	movdqa	32(%rsp), %xmm1
	movl	%eax, %r12d
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L68
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	176(%rsp), %xmm0
	call	__sincosf128@PLT
.L21:
	testl	%ebp, %ebp
	je	.L22
	movdqa	112(%rsp), %xmm0
	pxor	.LC11(%rip), %xmm0
	movaps	%xmm0, 112(%rsp)
.L22:
	movl	%r12d, %edi
	call	__floatsitf@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L69
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC12(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm2
	movdqa	112(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L25
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 112(%rsp)
.L25:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L70
	movdqa	.LC6(%rip), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
.L29:
	movdqa	16(%rsp), %xmm0
	pand	.LC5(%rip), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L30
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L30:
	movdqa	(%rsp), %xmm0
	pand	.LC5(%rip), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L34
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L69:
	movdqa	(%rsp), %xmm0
	call	__ieee754_sinhf128@PLT
	movdqa	112(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_coshf128@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L84:
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L17
	testl	%ebp, %ebp
	jne	.L87
	movdqa	.LC3(%rip), %xmm0
.L46:
	movdqa	176(%rsp), %xmm6
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm6, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L70:
	movdqa	(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	96(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L14
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L14
	movdqa	.LC7(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L14
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L88
	movdqa	.LC4(%rip), %xmm2
	movdqa	176(%rsp), %xmm4
	movaps	%xmm2, 16(%rsp)
	movaps	%xmm4, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L89
	movdqa	.LC4(%rip), %xmm5
	movaps	%xmm5, 16(%rsp)
	movaps	%xmm5, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L68:
	movdqa	176(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm0
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm0, 112(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L89:
	movdqa	.LC4(%rip), %xmm4
	movdqa	176(%rsp), %xmm7
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm7, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L86:
	movdqa	.LC0(%rip), %xmm1
	jmp	.L35
.L87:
	movdqa	.LC2(%rip), %xmm0
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L45:
	movdqa	176(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	.LC3(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L88:
	movdqa	.LC4(%rip), %xmm6
	movaps	%xmm6, 16(%rsp)
	movaps	%xmm6, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L85:
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	jmp	.L34
.L71:
	movdqa	.LC1(%rip), %xmm1
	jmp	.L36
	.size	__csinhf128, .-__csinhf128
	.weak	csinhf128
	.set	csinhf128,__csinhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	-65536
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC6:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1074593784
	.align 16
.LC10:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC12:
	.long	0
	.long	0
	.long	0
	.long	1073610752
