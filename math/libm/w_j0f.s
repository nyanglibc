	.text
	.p2align 4,,15
	.globl	__j0f
	.type	__j0f, @function
__j0f:
	jmp	__ieee754_j0f@PLT
	.size	__j0f, .-__j0f
	.weak	j0f32
	.set	j0f32,__j0f
	.weak	j0f
	.set	j0f,__j0f
	.p2align 4,,15
	.globl	__y0f
	.type	__y0f, @function
__y0f:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L11
.L4:
	jmp	__ieee754_y0f@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	ucomiss	%xmm1, %xmm0
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y0f, .-__y0f
	.weak	y0f32
	.set	y0f32,__y0f
	.weak	y0f
	.set	y0f,__y0f
