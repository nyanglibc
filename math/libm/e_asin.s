	.text
	.p2align 4,,15
	.globl	__ieee754_asin
	.type	__ieee754_asin, @function
__ieee754_asin:
	pushq	%rbx
	movq	%xmm0, %rbx
	sarq	$32, %rbx
	subq	$48, %rsp
	movl	%ebx, %eax
	andl	$2147483647, %eax
	cmpl	$1045430271, %eax
	jg	.L2
	movapd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm2
	movapd	%xmm0, %xmm3
	andpd	.LC0(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jbe	.L1
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
.L1:
	addq	$48, %rsp
	movapd	%xmm3, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1069547519, %eax
	movapd	%xmm0, %xmm2
	jg	.L5
	movapd	%xmm0, %xmm1
	movapd	%xmm2, %xmm3
	movapd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm1
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	.LC3(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC4(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC5(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC6(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC7(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm3
	addsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	.LC8(%rip), %xmm0
	addsd	%xmm3, %xmm0
	ucomisd	%xmm3, %xmm0
	jp	.L115
	je	.L1
.L115:
	movsd	.LC10(%rip), %xmm0
	movapd	%xmm2, %xmm4
	movsd	.LC9(%rip), %xmm3
	mulsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm8
	addsd	%xmm3, %xmm4
	movsd	a2(%rip), %xmm5
	addsd	.LC11(%rip), %xmm0
	subsd	%xmm3, %xmm4
	movsd	a1(%rip), %xmm3
	movapd	%xmm3, %xmm6
	addsd	%xmm5, %xmm3
	movapd	%xmm4, %xmm7
	mulsd	%xmm1, %xmm0
	subsd	%xmm4, %xmm8
	mulsd	%xmm4, %xmm7
	addsd	.LC12(%rip), %xmm0
	mulsd	%xmm8, %xmm3
	mulsd	%xmm4, %xmm7
	mulsd	.LC16(%rip), %xmm4
	mulsd	%xmm8, %xmm3
	mulsd	%xmm7, %xmm6
	mulsd	%xmm1, %xmm0
	mulsd	%xmm7, %xmm5
	mulsd	%xmm2, %xmm4
	addsd	.LC13(%rip), %xmm0
	addsd	%xmm3, %xmm4
	movapd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm0
	mulsd	%xmm4, %xmm8
	addsd	.LC14(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC15(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	addsd	%xmm6, %xmm1
	mulsd	%xmm2, %xmm0
	subsd	%xmm1, %xmm3
	addsd	%xmm8, %xmm0
	addsd	%xmm3, %xmm6
	movapd	%xmm1, %xmm3
	addsd	%xmm5, %xmm0
	addsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm1
	addsd	%xmm0, %xmm1
	mulsd	.LC17(%rip), %xmm1
	addsd	%xmm3, %xmm1
	ucomisd	%xmm3, %xmm1
	jp	.L116
	je	.L1
.L116:
	pxor	%xmm1, %xmm1
	leaq	32(%rsp), %rdi
	movapd	%xmm2, %xmm0
	call	__doasin@PLT
	movsd	32(%rsp), %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1071644671, %eax
	jg	.L8
	cmpl	$1070596095, %eax
	jg	.L9
	sarl	$15, %eax
	andl	$31, %eax
	leal	(%rax,%rax,4), %edx
	leal	(%rax,%rdx,2), %eax
.L10:
	leaq	asncs(%rip), %rdx
	movslq	%eax, %rcx
	testl	%ebx, %ebx
	movapd	%xmm2, %xmm1
	movsd	(%rdx,%rcx,8), %xmm3
	jg	.L180
	xorpd	.LC19(%rip), %xmm1
.L180:
	leal	1(%rax), %ecx
	subsd	%xmm3, %xmm1
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm4
	leal	6(%rax), %ecx
	movapd	%xmm1, %xmm3
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm4
	movsd	(%rdx,%rcx,8), %xmm0
	leal	5(%rax), %ecx
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm0
	movslq	%ecx, %rcx
	movapd	%xmm4, %xmm6
	addsd	(%rdx,%rcx,8), %xmm0
	leal	4(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	3(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	2(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	7(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm3, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	8(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm5
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm6
	movapd	%xmm5, %xmm4
	addsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	.LC20(%rip), %xmm4
	addsd	%xmm3, %xmm4
	ucomisd	%xmm3, %xmm4
	jp	.L13
	je	.L199
.L13:
	leal	9(%rax), %ecx
	movapd	%xmm5, %xmm4
	addl	$10, %eax
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm4
.L242:
	cltq
	subsd	%xmm4, %xmm5
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm3
	addsd	%xmm1, %xmm0
	movsd	.LC21(%rip), %xmm1
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	ucomisd	%xmm3, %xmm1
	jp	.L34
	jne	.L34
.L199:
	testl	%ebx, %ebx
	jg	.L1
.L185:
	xorpd	.LC19(%rip), %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1072168959, %eax
	jg	.L29
	sarl	$11, %eax
	leaq	asncs(%rip), %rdx
	andl	$508, %eax
	testl	%ebx, %ebx
	leal	(%rax,%rax,2), %eax
	movapd	%xmm0, %xmm1
	leal	1056(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	jle	.L30
.L181:
	leal	1057(%rax), %ecx
	subsd	%xmm3, %xmm1
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm4
	leal	1063(%rax), %ecx
	movapd	%xmm1, %xmm3
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm4
	movsd	(%rdx,%rcx,8), %xmm0
	leal	1062(%rax), %ecx
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm0
	movslq	%ecx, %rcx
	movapd	%xmm4, %xmm6
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1061(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1060(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1059(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1058(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1064(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm3, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1065(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm5
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm6
	movapd	%xmm5, %xmm4
	addsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	.LC25(%rip), %xmm4
	addsd	%xmm3, %xmm4
	ucomisd	%xmm3, %xmm4
	jp	.L32
	je	.L199
.L32:
	leal	1066(%rax), %ecx
	movapd	%xmm5, %xmm4
	addl	$1067, %eax
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm4
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L9:
	sarl	$14, %eax
	andl	$63, %eax
	leal	(%rax,%rax,4), %edx
	leal	352(%rax,%rdx,2), %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L29:
	cmpl	$1072529407, %eax
	jle	.L244
	cmpl	$1072594943, %eax
	jg	.L67
	sarl	$13, %eax
	leaq	asncs(%rip), %rdx
	andl	$127, %eax
	movapd	%xmm2, %xmm1
	imull	$14, %eax, %eax
	testl	%ebx, %ebx
	leal	884(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm0
	jg	.L183
	xorpd	.LC19(%rip), %xmm1
.L183:
	leal	885(%rax), %ecx
	subsd	%xmm0, %xmm1
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	leal	893(%rax), %ecx
	movapd	%xmm1, %xmm4
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm3
	movsd	(%rdx,%rcx,8), %xmm0
	leal	892(%rax), %ecx
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm0
	movslq	%ecx, %rcx
	movapd	%xmm3, %xmm6
	addsd	(%rdx,%rcx,8), %xmm0
	leal	891(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	890(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	889(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	888(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	887(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	886(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	894(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm4, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	895(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm5
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm6
	movapd	%xmm5, %xmm4
	addsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	.LC25(%rip), %xmm4
	addsd	%xmm3, %xmm4
	ucomisd	%xmm3, %xmm4
	jp	.L70
	je	.L199
.L70:
	leal	896(%rax), %ecx
	movapd	%xmm5, %xmm4
	addl	$897, %eax
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm4
.L238:
	cltq
	subsd	%xmm4, %xmm5
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm3
	addsd	%xmm1, %xmm0
	movsd	.LC27(%rip), %xmm1
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	ucomisd	%xmm3, %xmm1
	jp	.L91
	je	.L199
.L91:
	mulsd	.LC22(%rip), %xmm0
	movsd	%xmm2, 24(%rsp)
	leaq	32(%rsp), %rdi
	movsd	hp0(%rip), %xmm5
	movsd	hp1(%rip), %xmm1
	subsd	%xmm3, %xmm5
	movsd	%xmm3, 8(%rsp)
	movapd	%xmm0, %xmm4
	addsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm0
	movsd	%xmm4, 16(%rsp)
	subsd	%xmm3, %xmm0
	mulsd	.LC16(%rip), %xmm0
	subsd	%xmm0, %xmm5
	movapd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	subsd	%xmm0, %xmm5
.L233:
	addsd	%xmm5, %xmm1
	call	__dubcos@PLT
.L231:
	movsd	24(%rsp), %xmm2
	movsd	32(%rsp), %xmm0
	andpd	.LC0(%rip), %xmm2
	movsd	8(%rsp), %xmm3
	movsd	16(%rsp), %xmm4
	subsd	%xmm2, %xmm0
	addsd	40(%rsp), %xmm0
	ucomisd	.LC23(%rip), %xmm0
	jbe	.L175
	testl	%ebx, %ebx
	jle	.L95
	minsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L244:
	sarl	$13, %eax
	movapd	%xmm0, %xmm1
	andl	$127, %eax
	testl	%ebx, %ebx
	leal	(%rax,%rax,2), %edx
	leal	(%rax,%rdx,4), %eax
	leaq	asncs(%rip), %rdx
	leal	992(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	jg	.L182
	xorpd	.LC19(%rip), %xmm1
.L182:
	leal	993(%rax), %ecx
	subsd	%xmm3, %xmm1
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	leal	1000(%rax), %ecx
	movapd	%xmm1, %xmm4
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm3
	movsd	(%rdx,%rcx,8), %xmm0
	leal	999(%rax), %ecx
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm0
	movslq	%ecx, %rcx
	movapd	%xmm3, %xmm6
	addsd	(%rdx,%rcx,8), %xmm0
	leal	998(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	997(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	996(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	995(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	994(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1001(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm4, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	1002(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm5
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm6
	movapd	%xmm5, %xmm4
	addsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	.LC25(%rip), %xmm4
	addsd	%xmm3, %xmm4
	ucomisd	%xmm3, %xmm4
	jp	.L51
	je	.L199
.L51:
	leal	1003(%rax), %ecx
	movapd	%xmm5, %xmm4
	addl	$1004, %eax
	movslq	%ecx, %rcx
	cltq
	movsd	(%rdx,%rcx,8), %xmm3
	mulsd	%xmm1, %xmm3
	mulsd	(%rdx,%rax,8), %xmm1
	addsd	%xmm3, %xmm4
	addsd	%xmm1, %xmm0
	movsd	.LC26(%rip), %xmm1
	subsd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm3
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	ucomisd	%xmm3, %xmm1
	jp	.L53
	je	.L199
.L53:
	mulsd	.LC22(%rip), %xmm0
	movsd	%xmm2, 24(%rsp)
	leaq	32(%rsp), %rdi
	movsd	hp0(%rip), %xmm5
	movsd	hp1(%rip), %xmm1
	movsd	%xmm3, 8(%rsp)
	movapd	%xmm0, %xmm4
	movapd	%xmm5, %xmm0
	addsd	%xmm3, %xmm4
	subsd	%xmm3, %xmm0
	movapd	%xmm4, %xmm6
	movsd	%xmm4, 16(%rsp)
	subsd	%xmm0, %xmm5
	subsd	%xmm3, %xmm6
	subsd	%xmm3, %xmm5
	mulsd	.LC16(%rip), %xmm6
	subsd	%xmm6, %xmm1
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L30:
	xorpd	.LC19(%rip), %xmm1
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L34:
	mulsd	.LC22(%rip), %xmm0
	leaq	32(%rsp), %rdi
	movsd	%xmm2, 24(%rsp)
	movsd	%xmm3, 8(%rsp)
	movapd	%xmm0, %xmm4
	movapd	%xmm3, %xmm0
	addsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm1
	movsd	%xmm4, 16(%rsp)
	subsd	%xmm3, %xmm1
	mulsd	.LC16(%rip), %xmm1
	call	__dubsin@PLT
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L175:
	movsd	.LC24(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.L199
	testl	%ebx, %ebx
	jle	.L101
	maxsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	cmpl	$1072627711, %eax
	jg	.L86
	sarl	$13, %eax
	movapd	%xmm2, %xmm1
	andl	$127, %eax
	movl	%eax, %edx
	sall	$4, %edx
	subl	%eax, %edx
	testl	%ebx, %ebx
	movl	%edx, %eax
	leaq	asncs(%rip), %rdx
	leal	768(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm0
	jg	.L184
	xorpd	.LC19(%rip), %xmm1
.L184:
	leal	769(%rax), %ecx
	subsd	%xmm0, %xmm1
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	leal	778(%rax), %ecx
	movapd	%xmm1, %xmm4
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm3
	movsd	(%rdx,%rcx,8), %xmm0
	leal	777(%rax), %ecx
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm0
	movslq	%ecx, %rcx
	movapd	%xmm3, %xmm6
	addsd	(%rdx,%rcx,8), %xmm0
	leal	776(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	775(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	774(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	773(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	772(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	771(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	770(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm1, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	779(%rax), %ecx
	movslq	%ecx, %rcx
	mulsd	%xmm4, %xmm0
	addsd	(%rdx,%rcx,8), %xmm0
	leal	780(%rax), %ecx
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm5
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm6
	movapd	%xmm5, %xmm4
	addsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	.LC25(%rip), %xmm4
	addsd	%xmm3, %xmm4
	ucomisd	%xmm3, %xmm4
	jp	.L89
	je	.L199
.L89:
	leal	781(%rax), %ecx
	movapd	%xmm5, %xmm4
	addl	$782, %eax
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm4
	jmp	.L238
.L86:
	cmpl	$1072693247, %eax
	jg	.L105
	testl	%ebx, %ebx
	movsd	.LC28(%rip), %xmm1
	jle	.L106
	movapd	%xmm1, %xmm7
	movsd	.LC16(%rip), %xmm4
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm6
	mulsd	%xmm4, %xmm6
.L107:
	movq	%xmm6, %rax
	movl	$511, %edx
	movsd	.LC29(%rip), %xmm0
	movapd	%xmm6, %xmm7
	sarq	$32, %rax
	movsd	hp1(%rip), %xmm5
	movl	%eax, %ecx
	sarl	$21, %eax
	subl	%eax, %edx
	sarl	$14, %ecx
	movslq	%edx, %rax
	leaq	inroot(%rip), %rdx
	andl	$127, %ecx
	movsd	(%rdx,%rcx,8), %xmm2
	leaq	powtwo(%rip), %rdx
	mulsd	(%rdx,%rax,8), %xmm2
	movapd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	%xmm6, %xmm3
	subsd	%xmm3, %xmm1
	movapd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm0
	addsd	.LC30(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC31(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LC33(%rip), %xmm1
	addsd	.LC32(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	movapd	%xmm6, %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	.LC34(%rip), %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm4
	subsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	addsd	%xmm4, %xmm0
	mulsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm7
	movapd	%xmm7, %xmm1
	movsd	hp0(%rip), %xmm7
	divsd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	movapd	%xmm1, %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm2
	subsd	%xmm2, %xmm3
	movsd	.LC2(%rip), %xmm2
	mulsd	%xmm6, %xmm2
	addsd	.LC3(%rip), %xmm2
	mulsd	%xmm6, %xmm2
	addsd	.LC4(%rip), %xmm2
	mulsd	%xmm6, %xmm2
	addsd	.LC5(%rip), %xmm2
	mulsd	%xmm6, %xmm2
	addsd	.LC6(%rip), %xmm2
	mulsd	%xmm6, %xmm2
	addsd	.LC7(%rip), %xmm2
	mulsd	%xmm6, %xmm2
	movapd	%xmm0, %xmm6
	addsd	%xmm0, %xmm6
	mulsd	%xmm6, %xmm2
	movapd	%xmm3, %xmm6
	movapd	%xmm7, %xmm3
	subsd	%xmm2, %xmm6
	movapd	%xmm4, %xmm2
	addsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm2
	movapd	%xmm6, %xmm3
	addsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm2
	addsd	%xmm6, %xmm2
	mulsd	.LC35(%rip), %xmm2
	addsd	%xmm3, %xmm2
	ucomisd	%xmm3, %xmm2
	jp	.L108
	je	.L199
.L108:
	subsd	%xmm0, %xmm4
	leaq	32(%rsp), %rdi
	movsd	%xmm7, 16(%rsp)
	movsd	%xmm5, 8(%rsp)
	addsd	%xmm4, %xmm1
	call	__doasin@PLT
	movsd	32(%rsp), %xmm2
	testl	%ebx, %ebx
	movsd	16(%rsp), %xmm7
	addsd	%xmm2, %xmm2
	movsd	40(%rsp), %xmm1
	movapd	%xmm7, %xmm0
	addsd	%xmm1, %xmm1
	movsd	8(%rsp), %xmm5
	subsd	%xmm2, %xmm0
	subsd	%xmm1, %xmm5
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm1
	subsd	%xmm2, %xmm1
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm3
	addsd	%xmm0, %xmm3
	jg	.L1
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L95:
	ucomisd	%xmm4, %xmm3
	jbe	.L185
.L187:
	xorpd	.LC19(%rip), %xmm4
	movapd	%xmm4, %xmm3
	jmp	.L1
.L105:
	cmpl	$1072693248, %eax
	je	.L245
	cmpl	$2146435072, %eax
	jle	.L246
.L112:
	addsd	%xmm2, %xmm2
	movapd	%xmm2, %xmm3
	jmp	.L1
.L101:
	ucomisd	%xmm3, %xmm4
	jbe	.L185
	jmp	.L187
.L106:
	addsd	%xmm1, %xmm2
	movsd	.LC16(%rip), %xmm4
	movapd	%xmm2, %xmm6
	mulsd	%xmm4, %xmm6
	jmp	.L107
.L245:
	movq	%xmm0, %rdx
	testl	%edx, %edx
	jne	.L111
	movsd	hp0(%rip), %xmm3
	jmp	.L199
.L246:
	jne	.L111
	movq	%xmm0, %rax
	testl	%eax, %eax
	jne	.L112
.L111:
	movabsq	$9218868437227405312, %rax
	movq	%rax, 8(%rsp)
	movq	8(%rsp), %xmm0
	movapd	%xmm0, %xmm3
	divsd	%xmm0, %xmm3
	jmp	.L1
	.size	__ieee754_asin, .-__ieee754_asin
	.p2align 4,,15
	.globl	__ieee754_acos
	.type	__ieee754_acos, @function
__ieee754_acos:
	movq	%xmm0, %rdx
	sarq	$32, %rdx
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$1015545855, %eax
	jle	.L399
	subq	$56, %rsp
	cmpl	$1069547519, %eax
	movapd	%xmm0, %xmm2
	jg	.L250
	movapd	%xmm0, %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	hp0(%rip), %xmm3
	mulsd	%xmm0, %xmm5
	movapd	%xmm2, %xmm7
	movapd	%xmm3, %xmm0
	movapd	%xmm3, %xmm6
	movsd	hp1(%rip), %xmm4
	subsd	%xmm2, %xmm0
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm7
	subsd	%xmm0, %xmm6
	addsd	.LC3(%rip), %xmm1
	subsd	%xmm2, %xmm6
	mulsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm6
	addsd	.LC4(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC5(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC6(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC7(%rip), %xmm1
	mulsd	%xmm7, %xmm1
	movapd	%xmm0, %xmm7
	subsd	%xmm1, %xmm6
	addsd	%xmm6, %xmm7
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm0
	mulsd	.LC42(%rip), %xmm0
	addsd	%xmm7, %xmm0
	ucomisd	%xmm7, %xmm0
	jp	.L337
	je	.L247
.L337:
	movsd	.LC9(%rip), %xmm0
	movapd	%xmm2, %xmm6
	movsd	a1(%rip), %xmm1
	addsd	%xmm0, %xmm6
	movsd	a2(%rip), %xmm7
	movapd	%xmm2, %xmm9
	movapd	%xmm1, %xmm10
	addsd	%xmm7, %xmm1
	subsd	%xmm0, %xmm6
	movsd	.LC10(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	movapd	%xmm6, %xmm8
	subsd	%xmm6, %xmm9
	addsd	.LC11(%rip), %xmm0
	mulsd	%xmm6, %xmm8
	mulsd	%xmm9, %xmm1
	mulsd	%xmm6, %xmm8
	mulsd	.LC16(%rip), %xmm6
	mulsd	%xmm5, %xmm0
	mulsd	%xmm9, %xmm1
	mulsd	%xmm8, %xmm10
	mulsd	%xmm8, %xmm7
	addsd	.LC12(%rip), %xmm0
	mulsd	%xmm2, %xmm6
	addsd	%xmm1, %xmm6
	movapd	%xmm3, %xmm1
	mulsd	%xmm5, %xmm0
	mulsd	%xmm6, %xmm9
	movapd	%xmm2, %xmm6
	addsd	.LC13(%rip), %xmm0
	addsd	%xmm10, %xmm6
	mulsd	%xmm5, %xmm0
	addsd	.LC14(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	addsd	.LC15(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	mulsd	%xmm0, %xmm5
	movapd	%xmm3, %xmm0
	subsd	%xmm6, %xmm0
	mulsd	%xmm2, %xmm5
	subsd	%xmm0, %xmm1
	addsd	%xmm9, %xmm5
	subsd	%xmm6, %xmm1
	addsd	%xmm7, %xmm5
	movapd	%xmm2, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm4, %xmm1
	addsd	%xmm7, %xmm10
	movapd	%xmm0, %xmm7
	addsd	%xmm5, %xmm10
	subsd	%xmm10, %xmm1
	addsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	.LC43(%rip), %xmm0
	addsd	%xmm7, %xmm0
	ucomisd	%xmm7, %xmm0
	jp	.L338
	jne	.L338
.L247:
	movapd	%xmm7, %xmm0
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	cmpl	$1071644671, %eax
	jg	.L253
	cmpl	$1070596095, %eax
	jg	.L254
	sarl	$15, %eax
	andl	$31, %eax
	leal	(%rax,%rax,4), %ecx
	leal	(%rax,%rcx,2), %eax
.L255:
	leaq	asncs(%rip), %rcx
	movslq	%eax, %rsi
	testl	%edx, %edx
	movapd	%xmm2, %xmm1
	movsd	(%rcx,%rsi,8), %xmm3
	jg	.L379
	xorpd	.LC19(%rip), %xmm1
.L379:
	leal	1(%rax), %esi
	subsd	%xmm3, %xmm1
	testl	%edx, %edx
	movsd	hp1(%rip), %xmm5
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm3
	leal	6(%rax), %esi
	movapd	%xmm1, %xmm4
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm3
	movsd	(%rcx,%rsi,8), %xmm0
	leal	5(%rax), %esi
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm0
	movslq	%esi, %rsi
	addsd	(%rcx,%rsi,8), %xmm0
	leal	4(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	3(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	2(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	7(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm4, %xmm0
	movsd	hp0(%rip), %xmm4
	addsd	(%rcx,%rsi,8), %xmm0
	leal	8(%rax), %esi
	movslq	%esi, %rsi
	addsd	%xmm0, %xmm3
	movapd	%xmm0, %xmm8
	movsd	(%rcx,%rsi,8), %xmm0
	movapd	%xmm3, %xmm6
	jle	.L258
	movapd	%xmm5, %xmm7
	movapd	%xmm4, %xmm3
	subsd	%xmm6, %xmm7
	subsd	%xmm0, %xmm3
	movapd	%xmm7, %xmm6
.L259:
	movapd	%xmm6, %xmm7
	addsd	%xmm3, %xmm7
	subsd	%xmm7, %xmm3
	addsd	%xmm6, %xmm3
	mulsd	.LC37(%rip), %xmm3
	addsd	%xmm7, %xmm3
	ucomisd	%xmm7, %xmm3
	jp	.L339
	je	.L247
.L339:
	leal	9(%rax), %esi
	addl	$10, %eax
	testl	%edx, %edx
	cltq
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm6
	mulsd	%xmm1, %xmm6
	mulsd	(%rcx,%rax,8), %xmm1
	movapd	%xmm6, %xmm3
	addsd	%xmm0, %xmm3
	addsd	%xmm8, %xmm1
	subsd	%xmm3, %xmm0
	addsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm1
	jle	.L261
	movapd	%xmm4, %xmm0
	subsd	%xmm3, %xmm0
	subsd	%xmm0, %xmm4
	subsd	%xmm3, %xmm4
	subsd	%xmm1, %xmm4
	movapd	%xmm4, %xmm1
	addsd	%xmm5, %xmm1
.L262:
	movapd	%xmm0, %xmm7
	addsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm0
	addsd	%xmm0, %xmm1
	movsd	.LC39(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm7, %xmm0
	ucomisd	%xmm7, %xmm0
	jp	.L352
.L396:
	je	.L247
.L352:
	mulsd	.LC22(%rip), %xmm1
	movsd	%xmm2, 24(%rsp)
	movapd	%xmm1, %xmm3
.L389:
	addsd	%xmm7, %xmm3
	movapd	%xmm7, %xmm0
	leaq	32(%rsp), %rdi
	movsd	%xmm7, 8(%rsp)
	movapd	%xmm3, %xmm1
	movsd	%xmm3, 16(%rsp)
	subsd	%xmm7, %xmm1
	mulsd	.LC16(%rip), %xmm1
	call	__docos@PLT
	movsd	32(%rsp), %xmm0
	movsd	24(%rsp), %xmm2
	movsd	8(%rsp), %xmm7
	subsd	%xmm2, %xmm0
	movsd	16(%rsp), %xmm3
	addsd	40(%rsp), %xmm0
	ucomisd	.LC23(%rip), %xmm0
	jbe	.L375
	maxsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm7
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L399:
	movsd	hp0(%rip), %xmm7
	movapd	%xmm7, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	cmpl	$1072168959, %eax
	jg	.L269
	sarl	$11, %eax
	leaq	asncs(%rip), %rcx
	andl	$508, %eax
	testl	%edx, %edx
	leal	(%rax,%rax,2), %eax
	movapd	%xmm2, %xmm1
	leal	1056(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm0
	jle	.L270
	subsd	%xmm0, %xmm1
	movsd	.LC36(%rip), %xmm4
.L271:
	leal	1057(%rax), %esi
	movapd	%xmm1, %xmm3
	testl	%edx, %edx
	movsd	hp1(%rip), %xmm5
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm3
	movsd	(%rcx,%rsi,8), %xmm6
	leal	1063(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm6
	movsd	(%rcx,%rsi,8), %xmm0
	leal	1062(%rax), %esi
	mulsd	%xmm1, %xmm0
	movslq	%esi, %rsi
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1061(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1060(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1059(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1058(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1064(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm3, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1065(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm8
	movapd	%xmm0, %xmm9
	addsd	%xmm0, %xmm6
	movsd	hp0(%rip), %xmm0
	jle	.L272
	movapd	%xmm5, %xmm7
	movapd	%xmm0, %xmm3
	subsd	%xmm6, %xmm7
	subsd	%xmm8, %xmm3
	movapd	%xmm7, %xmm6
.L273:
	movapd	%xmm6, %xmm7
	addsd	%xmm3, %xmm7
	subsd	%xmm7, %xmm3
	addsd	%xmm6, %xmm3
	mulsd	%xmm4, %xmm3
	addsd	%xmm7, %xmm3
	ucomisd	%xmm7, %xmm3
	jp	.L345
	je	.L247
.L345:
	leal	1066(%rax), %esi
	addl	$1067, %eax
	testl	%edx, %edx
	cltq
	movapd	%xmm8, %xmm3
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm6
	mulsd	%xmm1, %xmm6
	mulsd	(%rcx,%rax,8), %xmm1
	movapd	%xmm6, %xmm4
	addsd	%xmm8, %xmm4
	addsd	%xmm9, %xmm1
	subsd	%xmm4, %xmm3
	addsd	%xmm6, %xmm3
	addsd	%xmm3, %xmm1
	jle	.L275
	movapd	%xmm0, %xmm3
	subsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm0
	subsd	%xmm4, %xmm0
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movsd	.LC38(%rip), %xmm0
	addsd	%xmm5, %xmm1
.L276:
	movapd	%xmm3, %xmm7
	addsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm3
	addsd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm0
	addsd	%xmm7, %xmm0
	ucomisd	%xmm7, %xmm0
	jnp	.L396
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L254:
	sarl	$14, %eax
	andl	$63, %eax
	leal	(%rax,%rax,4), %ecx
	leal	352(%rax,%rcx,2), %eax
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L269:
	cmpl	$1072529407, %eax
	jle	.L400
	cmpl	$1072594943, %eax
	jg	.L297
	sarl	$13, %eax
	leaq	asncs(%rip), %rcx
	andl	$127, %eax
	movapd	%xmm2, %xmm1
	imull	$14, %eax, %eax
	testl	%edx, %edx
	leal	884(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm0
	jle	.L298
	subsd	%xmm0, %xmm1
	movsd	.LC36(%rip), %xmm3
.L299:
	leal	885(%rax), %esi
	movapd	%xmm1, %xmm5
	testl	%edx, %edx
	movsd	hp1(%rip), %xmm8
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm5
	movsd	(%rcx,%rsi,8), %xmm4
	leal	893(%rax), %esi
	movsd	hp0(%rip), %xmm6
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm4
	movsd	(%rcx,%rsi,8), %xmm0
	leal	892(%rax), %esi
	mulsd	%xmm1, %xmm0
	movslq	%esi, %rsi
	addsd	(%rcx,%rsi,8), %xmm0
	leal	891(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	890(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	889(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	888(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	887(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	886(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	894(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm5, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	895(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm9
	addsd	%xmm0, %xmm4
	jle	.L300
	movapd	%xmm8, %xmm7
	movapd	%xmm6, %xmm5
	subsd	%xmm4, %xmm7
	subsd	%xmm9, %xmm5
	movapd	%xmm7, %xmm4
.L301:
	movapd	%xmm4, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm5
	addsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm3
	addsd	%xmm7, %xmm3
	ucomisd	%xmm7, %xmm3
	jp	.L357
	je	.L247
.L357:
	leal	896(%rax), %esi
	addl	$897, %eax
	testl	%edx, %edx
	cltq
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	(%rcx,%rax,8), %xmm1
	movapd	%xmm4, %xmm3
	addsd	%xmm9, %xmm3
	addsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm9
	addsd	%xmm4, %xmm9
	addsd	%xmm9, %xmm0
	jle	.L303
.L398:
	movapd	%xmm6, %xmm1
	subsd	%xmm3, %xmm1
	subsd	%xmm1, %xmm6
	subsd	%xmm3, %xmm6
	movsd	.LC35(%rip), %xmm3
	subsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm0
	addsd	%xmm8, %xmm0
.L318:
	movapd	%xmm1, %xmm7
	addsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm1
	addsd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm1
	addsd	%xmm7, %xmm1
	ucomisd	%xmm7, %xmm1
	jp	.L364
	je	.L247
.L364:
	mulsd	.LC22(%rip), %xmm0
	movsd	%xmm2, 24(%rsp)
	movapd	%xmm0, %xmm3
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L338:
	pxor	%xmm1, %xmm1
	leaq	32(%rsp), %rdi
	movapd	%xmm2, %xmm0
	movsd	%xmm4, 16(%rsp)
	movsd	%xmm3, 8(%rsp)
	call	__doasin@PLT
	movsd	8(%rsp), %xmm3
	movsd	32(%rsp), %xmm0
	movapd	%xmm3, %xmm7
	movsd	16(%rsp), %xmm4
	subsd	%xmm0, %xmm7
	subsd	40(%rsp), %xmm4
	subsd	%xmm7, %xmm3
	subsd	%xmm0, %xmm3
	addsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm7
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L400:
	sarl	$13, %eax
	movapd	%xmm2, %xmm1
	andl	$127, %eax
	testl	%edx, %edx
	leal	(%rax,%rax,2), %ecx
	leal	(%rax,%rcx,4), %eax
	leaq	asncs(%rip), %rcx
	leal	992(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm0
	jle	.L284
	subsd	%xmm0, %xmm1
	movsd	.LC36(%rip), %xmm3
.L285:
	leal	993(%rax), %esi
	movapd	%xmm1, %xmm5
	testl	%edx, %edx
	movsd	hp1(%rip), %xmm8
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm5
	movsd	(%rcx,%rsi,8), %xmm4
	leal	1000(%rax), %esi
	movsd	hp0(%rip), %xmm6
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm4
	movsd	(%rcx,%rsi,8), %xmm0
	leal	999(%rax), %esi
	mulsd	%xmm1, %xmm0
	movslq	%esi, %rsi
	addsd	(%rcx,%rsi,8), %xmm0
	leal	998(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	997(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	996(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	995(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	994(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1001(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm5, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	1002(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm9
	addsd	%xmm0, %xmm4
	jle	.L286
	movapd	%xmm8, %xmm7
	movapd	%xmm6, %xmm5
	subsd	%xmm4, %xmm7
	subsd	%xmm9, %xmm5
	movapd	%xmm7, %xmm4
.L287:
	movapd	%xmm4, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm5
	addsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm3
	addsd	%xmm7, %xmm3
	ucomisd	%xmm7, %xmm3
	jp	.L351
	je	.L247
.L351:
	leal	1003(%rax), %esi
	addl	$1004, %eax
	testl	%edx, %edx
	cltq
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	(%rcx,%rax,8), %xmm1
	movapd	%xmm4, %xmm3
	addsd	%xmm9, %xmm3
	addsd	%xmm0, %xmm1
	subsd	%xmm3, %xmm9
	addsd	%xmm4, %xmm9
	addsd	%xmm9, %xmm1
	jle	.L289
	movapd	%xmm6, %xmm0
	subsd	%xmm3, %xmm0
	subsd	%xmm0, %xmm6
	subsd	%xmm3, %xmm6
	movsd	.LC40(%rip), %xmm3
	subsd	%xmm1, %xmm6
	movapd	%xmm6, %xmm1
	addsd	%xmm8, %xmm1
.L290:
	movapd	%xmm0, %xmm7
	addsd	%xmm1, %xmm7
	subsd	%xmm7, %xmm0
	addsd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	addsd	%xmm7, %xmm0
	ucomisd	%xmm7, %xmm0
	jnp	.L396
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L261:
	movapd	%xmm3, %xmm0
	addsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm4
	addsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm1
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L270:
	xorpd	.LC19(%rip), %xmm1
	movsd	.LC37(%rip), %xmm4
	subsd	%xmm0, %xmm1
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L258:
	movapd	%xmm0, %xmm3
	addsd	%xmm5, %xmm6
	addsd	%xmm4, %xmm3
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L375:
	movsd	.LC24(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.L247
	minsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm7
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L272:
	movapd	%xmm8, %xmm3
	addsd	%xmm5, %xmm6
	addsd	%xmm0, %xmm3
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L297:
	cmpl	$1072627711, %eax
	jg	.L311
	sarl	$13, %eax
	movapd	%xmm2, %xmm1
	andl	$127, %eax
	movl	%eax, %ecx
	sall	$4, %ecx
	subl	%eax, %ecx
	testl	%edx, %edx
	movl	%ecx, %eax
	leaq	asncs(%rip), %rcx
	leal	768(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm0
	jle	.L312
	subsd	%xmm0, %xmm1
	movsd	.LC36(%rip), %xmm3
.L313:
	leal	769(%rax), %esi
	movapd	%xmm1, %xmm5
	testl	%edx, %edx
	movsd	hp1(%rip), %xmm8
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm5
	movsd	(%rcx,%rsi,8), %xmm4
	leal	778(%rax), %esi
	movsd	hp0(%rip), %xmm6
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm4
	movsd	(%rcx,%rsi,8), %xmm0
	leal	777(%rax), %esi
	mulsd	%xmm1, %xmm0
	movslq	%esi, %rsi
	addsd	(%rcx,%rsi,8), %xmm0
	leal	776(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	775(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	774(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	773(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	772(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	771(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	770(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm1, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	779(%rax), %esi
	movslq	%esi, %rsi
	mulsd	%xmm5, %xmm0
	addsd	(%rcx,%rsi,8), %xmm0
	leal	780(%rax), %esi
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm9
	addsd	%xmm0, %xmm4
	jle	.L314
	movapd	%xmm8, %xmm7
	movapd	%xmm6, %xmm5
	subsd	%xmm4, %xmm7
	subsd	%xmm9, %xmm5
	movapd	%xmm7, %xmm4
.L315:
	movapd	%xmm4, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm5
	addsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm3
	addsd	%xmm7, %xmm3
	ucomisd	%xmm7, %xmm3
	jp	.L363
	je	.L247
.L363:
	leal	781(%rax), %esi
	addl	$782, %eax
	testl	%edx, %edx
	cltq
	movslq	%esi, %rsi
	movsd	(%rcx,%rsi,8), %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	(%rcx,%rax,8), %xmm1
	movapd	%xmm4, %xmm3
	addsd	%xmm9, %xmm3
	addsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm9
	addsd	%xmm4, %xmm9
	addsd	%xmm9, %xmm0
	jg	.L398
	movapd	%xmm3, %xmm1
	addsd	%xmm8, %xmm0
	addsd	%xmm6, %xmm1
	subsd	%xmm1, %xmm6
	addsd	%xmm3, %xmm6
	movsd	.LC21(%rip), %xmm3
	addsd	%xmm6, %xmm0
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L275:
	movapd	%xmm4, %xmm3
	addsd	%xmm5, %xmm1
	addsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm1
	movsd	.LC39(%rip), %xmm0
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L286:
	movapd	%xmm9, %xmm5
	addsd	%xmm8, %xmm4
	addsd	%xmm6, %xmm5
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L284:
	xorpd	.LC19(%rip), %xmm1
	movsd	.LC25(%rip), %xmm3
	subsd	%xmm0, %xmm1
	jmp	.L285
.L311:
	cmpl	$1072693247, %eax
	jg	.L325
	testl	%edx, %edx
	movsd	.LC28(%rip), %xmm1
	jle	.L326
	movapd	%xmm1, %xmm4
	movsd	.LC16(%rip), %xmm5
	subsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm2
	mulsd	%xmm5, %xmm2
	movapd	%xmm2, %xmm4
.L327:
	movq	%xmm4, %rax
	movl	$511, %ecx
	movsd	.LC29(%rip), %xmm0
	sarq	$32, %rax
	movl	%eax, %esi
	sarl	$21, %eax
	subl	%eax, %ecx
	sarl	$14, %esi
	movslq	%ecx, %rax
	leaq	inroot(%rip), %rcx
	andl	$127, %esi
	testl	%edx, %edx
	movsd	(%rcx,%rsi,8), %xmm2
	leaq	powtwo(%rip), %rcx
	mulsd	(%rcx,%rax,8), %xmm2
	movapd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm0
	addsd	.LC30(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	.LC31(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LC33(%rip), %xmm1
	addsd	.LC32(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	movapd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	%xmm5, %xmm0
	movapd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	.LC44(%rip), %xmm1
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	.LC2(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	subsd	%xmm1, %xmm3
	addsd	.LC3(%rip), %xmm2
	movapd	%xmm3, %xmm1
	addsd	%xmm3, %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm2
	subsd	%xmm1, %xmm5
	addsd	.LC4(%rip), %xmm2
	movapd	%xmm5, %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm3, %xmm0
	mulsd	%xmm4, %xmm2
	addsd	%xmm1, %xmm0
	addsd	.LC5(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	.LC6(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	.LC7(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	mulsd	%xmm0, %xmm2
	js	.L401
	addsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm7
	movapd	%xmm3, %xmm4
	addsd	%xmm2, %xmm7
	subsd	%xmm7, %xmm4
	addsd	%xmm4, %xmm2
	mulsd	.LC46(%rip), %xmm2
	addsd	%xmm7, %xmm2
	ucomisd	%xmm7, %xmm2
	jp	.L331
	jne	.L331
.L381:
	addsd	%xmm7, %xmm7
	jmp	.L247
.L289:
	movapd	%xmm3, %xmm0
	addsd	%xmm8, %xmm1
	addsd	%xmm6, %xmm0
	subsd	%xmm0, %xmm6
	addsd	%xmm3, %xmm6
	movsd	.LC26(%rip), %xmm3
	addsd	%xmm6, %xmm1
	jmp	.L290
.L300:
	movapd	%xmm9, %xmm5
	addsd	%xmm8, %xmm4
	addsd	%xmm6, %xmm5
	jmp	.L301
.L298:
	xorpd	.LC19(%rip), %xmm1
	movsd	.LC41(%rip), %xmm3
	subsd	%xmm0, %xmm1
	jmp	.L299
.L325:
	cmpl	$1072693248, %eax
	je	.L402
	cmpl	$2146435072, %eax
	jle	.L403
.L335:
	addsd	%xmm2, %xmm2
	movapd	%xmm2, %xmm7
	jmp	.L247
.L303:
	movapd	%xmm3, %xmm1
	addsd	%xmm8, %xmm0
	addsd	%xmm6, %xmm1
	subsd	%xmm1, %xmm6
	addsd	%xmm3, %xmm6
	movsd	.LC21(%rip), %xmm3
	addsd	%xmm6, %xmm0
	jmp	.L318
.L314:
	movapd	%xmm9, %xmm5
	addsd	%xmm8, %xmm4
	addsd	%xmm6, %xmm5
	jmp	.L315
.L312:
	xorpd	.LC19(%rip), %xmm1
	movsd	.LC41(%rip), %xmm3
	subsd	%xmm0, %xmm1
	jmp	.L313
.L326:
	addsd	%xmm1, %xmm2
	movsd	.LC16(%rip), %xmm5
	mulsd	%xmm5, %xmm2
	movapd	%xmm2, %xmm4
	jmp	.L327
.L401:
	movsd	hp1(%rip), %xmm4
	subsd	%xmm1, %xmm4
	subsd	%xmm2, %xmm4
	movsd	hp0(%rip), %xmm2
	movapd	%xmm4, %xmm5
	movapd	%xmm2, %xmm4
	movapd	%xmm5, %xmm7
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm7
	subsd	%xmm7, %xmm4
	addsd	%xmm5, %xmm4
	mulsd	.LC45(%rip), %xmm4
	addsd	%xmm7, %xmm4
	ucomisd	%xmm7, %xmm4
	jp	.L329
	je	.L381
.L329:
	subsd	%xmm0, %xmm3
	leaq	32(%rsp), %rdi
	movsd	%xmm2, 8(%rsp)
	addsd	%xmm3, %xmm1
	call	__doasin@PLT
	movsd	8(%rsp), %xmm2
	movsd	32(%rsp), %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm7
	subsd	%xmm0, %xmm7
	movsd	hp1(%rip), %xmm0
	subsd	40(%rsp), %xmm0
	addsd	%xmm0, %xmm7
	addsd	%xmm1, %xmm7
	addsd	%xmm7, %xmm7
	jmp	.L247
.L402:
	movq	%xmm0, %rcx
	testl	%ecx, %ecx
	jne	.L334
	testl	%edx, %edx
	pxor	%xmm7, %xmm7
	jg	.L247
	movsd	hp0(%rip), %xmm7
	jmp	.L381
.L403:
	jne	.L334
	movq	%xmm0, %rax
	testl	%eax, %eax
	jne	.L335
.L334:
	movabsq	$9218868437227405312, %rax
	movq	%rax, 8(%rsp)
	movq	8(%rsp), %xmm7
	divsd	%xmm7, %xmm7
	jmp	.L247
.L331:
	subsd	%xmm0, %xmm3
	leaq	32(%rsp), %rdi
	addsd	%xmm3, %xmm1
	call	__doasin@PLT
	movsd	32(%rsp), %xmm7
	addsd	%xmm7, %xmm7
	jmp	.L247
	.size	__ieee754_acos, .-__ieee754_acos
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	hp1, @object
	.size	hp1, 8
hp1:
	.long	856972295
	.long	1016178214
	.align 8
	.type	hp0, @object
	.size	hp0, 8
hp0:
	.long	1413754136
	.long	1073291771
	.align 8
	.type	a2, @object
	.size	a2, 8
a2:
	.long	1431642928
	.long	-1093315243
	.align 8
	.type	a1, @object
	.size	a1, 8
a1:
	.long	0
	.long	1069897088
	.section	.rodata
	.align 32
	.type	powtwo, @object
	.size	powtwo, 224
powtwo:
	.long	0
	.long	1072693248
	.long	0
	.long	1073741824
	.long	0
	.long	1074790400
	.long	0
	.long	1075838976
	.long	0
	.long	1076887552
	.long	0
	.long	1077936128
	.long	0
	.long	1078984704
	.long	0
	.long	1080033280
	.long	0
	.long	1081081856
	.long	0
	.long	1082130432
	.long	0
	.long	1083179008
	.long	0
	.long	1084227584
	.long	0
	.long	1085276160
	.long	0
	.long	1086324736
	.long	0
	.long	1087373312
	.long	0
	.long	1088421888
	.long	0
	.long	1089470464
	.long	0
	.long	1090519040
	.long	0
	.long	1091567616
	.long	0
	.long	1092616192
	.long	0
	.long	1093664768
	.long	0
	.long	1094713344
	.long	0
	.long	1095761920
	.long	0
	.long	1096810496
	.long	0
	.long	1097859072
	.long	0
	.long	1098907648
	.long	0
	.long	1099956224
	.long	0
	.long	1101004800
	.align 32
	.type	inroot, @object
	.size	inroot, 1024
inroot:
	.long	2161580064
	.long	1073121823
	.long	720986911
	.long	1073110504
	.long	538786277
	.long	1073099441
	.long	3642487746
	.long	1073088624
	.long	1273358242
	.long	1073078046
	.long	3814811275
	.long	1073067696
	.long	2011998018
	.long	1073057568
	.long	1215545827
	.long	1073047653
	.long	4094520504
	.long	1073037943
	.long	1947370400
	.long	1073028433
	.long	379951157
	.long	1073019115
	.long	2040586588
	.long	1073009982
	.long	2246167062
	.long	1073001029
	.long	1793738238
	.long	1072992250
	.long	2597373050
	.long	1072983639
	.long	3329774677
	.long	1072975191
	.long	3658161570
	.long	1072966901
	.long	4189184459
	.long	1072958764
	.long	2122559886
	.long	1072950776
	.long	4087968535
	.long	1072942931
	.long	330382096
	.long	1072935227
	.long	3322794209
	.long	1072927657
	.long	482282245
	.long	1072920220
	.long	1377827218
	.long	1072912910
	.long	3336049511
	.long	1072905724
	.long	4293712861
	.long	1072898659
	.long	2767300832
	.long	1072891712
	.long	2119394317
	.long	1072884879
	.long	1941851849
	.long	1072878157
	.long	2325477612
	.long	1072871543
	.long	3836198255
	.long	1072865034
	.long	3197648529
	.long	1072858628
	.long	1859941170
	.long	1072852322
	.long	1684731988
	.long	1072846113
	.long	631402940
	.long	1072839999
	.long	1329191515
	.long	1072833977
	.long	2470427718
	.long	1072828045
	.long	3089587406
	.long	1072822201
	.long	2548234226
	.long	1072816443
	.long	520763345
	.long	1072810769
	.long	1275865276
	.long	1072805176
	.long	778825718
	.long	1072799663
	.long	3859259105
	.long	1072794227
	.long	2724758814
	.long	1072788868
	.long	2719772685
	.long	1072783583
	.long	840388492
	.long	1072778371
	.long	2904342629
	.long	1072773229
	.long	2066810875
	.long	1072768157
	.long	286331168
	.long	1072763153
	.long	4021355582
	.long	1072758214
	.long	2747341416
	.long	1072753341
	.long	3308802497
	.long	1072748531
	.long	4142181615
	.long	1072743783
	.long	3858805381
	.long	1072739096
	.long	1238228815
	.long	1072734469
	.long	3811821660
	.long	1072729899
	.long	2086906591
	.long	1072725387
	.long	3900714329
	.long	1072720930
	.long	55122466
	.long	1072716529
	.long	2966084391
	.long	1072712180
	.long	3708913524
	.long	1072707884
	.long	2078232869
	.long	1072703640
	.long	2288396083
	.long	1072699446
	.long	84173815
	.long	1072695302
	.long	2961900962
	.long	1072685103
	.long	3143750511
	.long	1072669095
	.long	976363447
	.long	1072653450
	.long	3011570592
	.long	1072638153
	.long	1589286080
	.long	1072623193
	.long	414438269
	.long	1072608557
	.long	1494295639
	.long	1072594233
	.long	679452101
	.long	1072580211
	.long	397169062
	.long	1072566480
	.long	921261886
	.long	1072553030
	.long	242305518
	.long	1072539852
	.long	2537261180
	.long	1072526936
	.long	877977989
	.long	1072514275
	.long	2897315501
	.long	1072501859
	.long	627979566
	.long	1072489682
	.long	477589628
	.long	1072477735
	.long	1670287495
	.long	1072466011
	.long	463805792
	.long	1072454504
	.long	2961679847
	.long	1072443206
	.long	3570524592
	.long	1072432112
	.long	2116448473
	.long	1072421216
	.long	3785692313
	.long	1072410511
	.long	1889184450
	.long	1072399993
	.long	3580272725
	.long	1072389655
	.long	4036093483
	.long	1072379493
	.long	3591623665
	.long	1072369502
	.long	3401687645
	.long	1072359677
	.long	1105558061
	.long	1072350014
	.long	3673836513
	.long	1072340507
	.long	1602871030
	.long	1072331154
	.long	3240803642
	.long	1072321949
	.long	396086761
	.long	1072312890
	.long	2667290187
	.long	1072303971
	.long	3055124439
	.long	1072295190
	.long	3705587101
	.long	1072286543
	.long	2999880136
	.long	1072278027
	.long	4120546283
	.long	1072269638
	.long	2144060708
	.long	1072261374
	.long	1199405268
	.long	1072253231
	.long	1563005096
	.long	1072245206
	.long	3934599189
	.long	1072237296
	.long	829206031
	.long	1072229500
	.long	2329765232
	.long	1072221813
	.long	1711117204
	.long	1072214234
	.long	1194351305
	.long	1072206760
	.long	3342195010
	.long	1072199388
	.long	2455134457
	.long	1072192117
	.long	1738028189
	.long	1072184944
	.long	402599602
	.long	1072177867
	.long	2245378270
	.long	1072170883
	.long	2751381520
	.long	1072163991
	.long	1968144996
	.long	1072157189
	.long	200398607
	.long	1072150475
	.long	2295162482
	.long	1072143846
	.long	452465797
	.long	1072137302
	.long	4281140012
	.long	1072130839
	.long	2135545654
	.long	1072124458
	.long	4057067092
	.long	1072118155
	.long	3111595747
	.long	1072111930
	.long	1151871943
	.long	1072105781
	.long	220418318
	.long	1072099706
	.long	2542721654
	.long	1072093703
	.long	1930776164
	.long	1072087772
	.long	956713214
	.long	1072081911
	.align 32
	.type	asncs, @object
	.size	asncs, 20544
asncs:
	.long	0
	.long	1069563904
	.long	-2003221468
	.long	1072701801
	.long	-1214664427
	.long	1068541602
	.long	-713815648
	.long	1070001941
	.long	-1353520480
	.long	1068110878
	.long	-1413750175
	.long	1068979601
	.long	-771303281
	.long	1067920209
	.long	1538473597
	.long	1016509845
	.long	-1606539520
	.long	1069566785
	.long	0
	.long	1072709632
	.long	1723587708
	.long	-1082222985
	.long	0
	.long	1069596672
	.long	-102614442
	.long	1072702342
	.long	1627193909
	.long	1068576908
	.long	349268738
	.long	1070008735
	.long	666902241
	.long	1068170966
	.long	-607297640
	.long	1068997743
	.long	1082448556
	.long	1067979850
	.long	953700446
	.long	-1131954030
	.long	1456075648
	.long	1069599829
	.long	0
	.long	1072709632
	.long	499493412
	.long	-1082361594
	.long	0
	.long	1069629440
	.long	-1301200394
	.long	1072702901
	.long	80638976
	.long	1068612376
	.long	1922069314
	.long	1070015764
	.long	200548908
	.long	1068231776
	.long	1974694764
	.long	1069016592
	.long	1369031491
	.long	1068040780
	.long	112553824
	.long	-1131747252
	.long	1039114352
	.long	1069632890
	.long	0
	.long	1072709632
	.long	-1900148132
	.long	-1082504627
	.long	0
	.long	1069662208
	.long	-958492841
	.long	1072703477
	.long	878137825
	.long	1068648011
	.long	-821925883
	.long	1070023032
	.long	1084874966
	.long	1068293336
	.long	-350381000
	.long	1069036162
	.long	-675385221
	.long	1068103056
	.long	60991190
	.long	-1129605272
	.long	-532985744
	.long	1069665968
	.long	0
	.long	1072709632
	.long	561031491
	.long	-1082652103
	.long	0
	.long	1069694976
	.long	1282774934
	.long	1072704071
	.long	-1837688523
	.long	1068683819
	.long	648781933
	.long	1070030543
	.long	1214912818
	.long	1068355675
	.long	1526297045
	.long	1069056471
	.long	-2095387124
	.long	1068166738
	.long	-1390732305
	.long	1017200471
	.long	-924806352
	.long	1069699065
	.long	0
	.long	1072709632
	.long	-1972868709
	.long	-1082804045
	.long	0
	.long	1069727744
	.long	1496796681
	.long	1072704682
	.long	-733049878
	.long	1068719806
	.long	-1830502774
	.long	1070038298
	.long	1248605437
	.long	1068418822
	.long	-1758499321
	.long	1069077534
	.long	-108765335
	.long	1068231885
	.long	-1805893243
	.long	1015783930
	.long	-2084546384
	.long	1069732181
	.long	0
	.long	1072709632
	.long	-927860876
	.long	-1082960474
	.long	0
	.long	1069760512
	.long	64780101
	.long	1072705311
	.long	-1041326239
	.long	1068755978
	.long	1250092754
	.long	1070046302
	.long	431091570
	.long	1068482807
	.long	715736037
	.long	1069099370
	.long	-276062745
	.long	1068298561
	.long	138184470
	.long	-1132248959
	.long	-1653713392
	.long	1069765316
	.long	0
	.long	1072709632
	.long	596163376
	.long	-1083121412
	.long	0
	.long	1069793280
	.long	1675104849
	.long	1072705957
	.long	923496991
	.long	1068792341
	.long	-1568198171
	.long	1070054557
	.long	506485737
	.long	1068523302
	.long	1604652989
	.long	1069121996
	.long	1118584963
	.long	1068366831
	.long	-1461647417
	.long	1017252555
	.long	-1556679200
	.long	1069798471
	.long	0
	.long	1072709632
	.long	1339776348
	.long	-1083394760
	.long	0
	.long	1069826048
	.long	-1856382865
	.long	1072706621
	.long	598963324
	.long	1068828900
	.long	248879051
	.long	1070063068
	.long	2041442564
	.long	1068556178
	.long	1180227909
	.long	1069145432
	.long	346857579
	.long	1068436761
	.long	-547131650
	.long	-1130045171
	.long	589638656
	.long	1069831647
	.long	0
	.long	1072709632
	.long	1280254490
	.long	-1083734819
	.long	0
	.long	1069858816
	.long	-1521429488
	.long	1072707303
	.long	-1928662553
	.long	1068865661
	.long	586389286
	.long	1070071837
	.long	-1592693389
	.long	1068589520
	.long	-1142287064
	.long	1069169697
	.long	1948266486
	.long	1068503682
	.long	-787978526
	.long	1017775633
	.long	-1408735904
	.long	1069864843
	.long	0
	.long	1072709632
	.long	1582817162
	.long	-1084084043
	.long	0
	.long	1069891584
	.long	-1183969398
	.long	1072708003
	.long	-1911805450
	.long	1068902631
	.long	-1831936511
	.long	1070080868
	.long	-75801212
	.long	1068623344
	.long	1908213998
	.long	1069194813
	.long	1602222772
	.long	1068540413
	.long	-284887321
	.long	-1131349632
	.long	-847606336
	.long	1069898061
	.long	0
	.long	1072709632
	.long	1203886201
	.long	-1084657382
	.long	0
	.long	1069924352
	.long	-400073710
	.long	1072708721
	.long	1481386531
	.long	1068939816
	.long	864828747
	.long	1070090166
	.long	1504009688
	.long	1068657668
	.long	-182440598
	.long	1069220800
	.long	-1671141724
	.long	1068578082
	.long	707168627
	.long	1016609946
	.long	400960032
	.long	1069931302
	.long	0
	.long	1072709632
	.long	-987795448
	.long	-1085509442
	.long	0
	.long	1069957120
	.long	1287270825
	.long	1072709458
	.long	885299934
	.long	1068977222
	.long	-60869684
	.long	1070099733
	.long	-96363140
	.long	1068692507
	.long	-1218975702
	.long	1069247682
	.long	-632464327
	.long	1068616728
	.long	771433037
	.long	-1129815340
	.long	478972656
	.long	1069964565
	.long	0
	.long	1072709632
	.long	-1177887809
	.long	-1088047512
	.long	0
	.long	1069989888
	.long	53389455
	.long	1072710213
	.long	-2069711757
	.long	1069014855
	.long	124412149
	.long	1070109576
	.long	-1903979327
	.long	1068727881
	.long	370095218
	.long	1069275482
	.long	964970512
	.long	1068656392
	.long	1770768618
	.long	1016676904
	.long	1837896864
	.long	1069997851
	.long	0
	.long	1072709632
	.long	1967421356
	.long	1061300249
	.long	0
	.long	1070022656
	.long	677022955
	.long	1072710986
	.long	-1038380508
	.long	1069052722
	.long	-1834404862
	.long	1070119696
	.long	1054078342
	.long	1068763807
	.long	1681004488
	.long	1069304223
	.long	1563009387
	.long	1068697114
	.long	1039790807
	.long	-1130122529
	.long	-1645829792
	.long	1070031161
	.long	0
	.long	1072709632
	.long	1781771083
	.long	1062545569
	.long	0
	.long	1070055424
	.long	-639333032
	.long	1072711777
	.long	-2126111754
	.long	1069090830
	.long	14888954
	.long	1070130100
	.long	-1347292507
	.long	1068800303
	.long	-214497835
	.long	1069333931
	.long	2116284041
	.long	1068738938
	.long	-857453503
	.long	1014672658
	.long	1099430624
	.long	1070064496
	.long	0
	.long	1072709632
	.long	-920998127
	.long	1063306163
	.long	0
	.long	1070088192
	.long	910656777
	.long	1072712588
	.long	1895481780
	.long	1069129185
	.long	-626450074
	.long	1070140790
	.long	113514150
	.long	1068837390
	.long	484221764
	.long	1069364634
	.long	2130529440
	.long	1068781909
	.long	900237661
	.long	1010396325
	.long	-313757968
	.long	1070097855
	.long	0
	.long	1072709632
	.long	-1895165532
	.long	1063721068
	.long	0
	.long	1070120960
	.long	1557520968
	.long	1072713417
	.long	1538868227
	.long	1069167794
	.long	-816011462
	.long	1070151773
	.long	-266666562
	.long	1068875085
	.long	1298245545
	.long	1069396357
	.long	12404687
	.long	1068826074
	.long	1920895844
	.long	-1130557159
	.long	923237920
	.long	1070131241
	.long	0
	.long	1072709632
	.long	-1413181237
	.long	1064145593
	.long	0
	.long	1070153728
	.long	1841113616
	.long	1072714265
	.long	680839841
	.long	1069206664
	.long	-1237158634
	.long	1070163053
	.long	-1549245225
	.long	1068913411
	.long	238875604
	.long	1069429130
	.long	2026743416
	.long	1068871480
	.long	-627224097
	.long	1017349149
	.long	-1249204640
	.long	1070164652
	.long	0
	.long	1072709632
	.long	-1121316782
	.long	1064442221
	.long	0
	.long	1070186496
	.long	-1979082352
	.long	1072715132
	.long	-601454235
	.long	1069245801
	.long	-1885357535
	.long	1070174635
	.long	-353558907
	.long	1068952387
	.long	291261539
	.long	1069462982
	.long	1207545594
	.long	1068918179
	.long	61552243
	.long	-1130235651
	.long	11291232
	.long	1070198091
	.long	0
	.long	1072709632
	.long	161058907
	.long	1064664202
	.long	0
	.long	1070219264
	.long	-743840140
	.long	1072716018
	.long	-1719191502
	.long	1069285214
	.long	-2048713495
	.long	1070186524
	.long	652255218
	.long	1068992036
	.long	1022396508
	.long	1069497944
	.long	-761765319
	.long	1068966222
	.long	1046038164
	.long	1016587237
	.long	-1320241712
	.long	1070231556
	.long	0
	.long	1072709632
	.long	-1444514709
	.long	1064891091
	.long	0
	.long	1070252032
	.long	1836261067
	.long	1072716924
	.long	-1555061581
	.long	1069324909
	.long	-287718829
	.long	1070198725
	.long	1444215531
	.long	1069032378
	.long	-1211209329
	.long	1069534048
	.long	-1712117457
	.long	1069015665
	.long	109932700
	.long	-1130872122
	.long	1634156816
	.long	1070265050
	.long	0
	.long	1072709632
	.long	1931397858
	.long	1065122925
	.long	0
	.long	1070284800
	.long	2066000243
	.long	1072717849
	.long	1554032536
	.long	1069364894
	.long	1290853220
	.long	1070211245
	.long	454704945
	.long	1069073437
	.long	2067128393
	.long	1069559424
	.long	1459325591
	.long	1069066564
	.long	-751324292
	.long	-1129529444
	.long	-1408920720
	.long	1070298572
	.long	0
	.long	1072726016
	.long	-615084680
	.long	-1082136956
	.long	0
	.long	1070317568
	.long	560752641
	.long	1072718794
	.long	1244842493
	.long	1069405176
	.long	1350843337
	.long	1070224088
	.long	-1021025142
	.long	1069115235
	.long	-465536871
	.long	1069578669
	.long	375629276
	.long	1069118978
	.long	-969463651
	.long	-1130123837
	.long	760940192
	.long	1070332124
	.long	0
	.long	1072726016
	.long	-1818755418
	.long	-1082378786
	.long	0
	.long	1070350336
	.long	-2048197857
	.long	1072719758
	.long	325970630
	.long	1069445763
	.long	-650477187
	.long	1070237260
	.long	1291258011
	.long	1069157798
	.long	-1280593118
	.long	1069598538
	.long	1445883422
	.long	1069172968
	.long	2126053368
	.long	-1131078082
	.long	-2101204096
	.long	1070365705
	.long	0
	.long	1072726016
	.long	352641235
	.long	-1082625670
	.long	0
	.long	1070383104
	.long	-818402727
	.long	1072720742
	.long	-2088159493
	.long	1069486662
	.long	-143696748
	.long	1070250768
	.long	1880782766
	.long	1069201149
	.long	-1033436474
	.long	1069619049
	.long	617477006
	.long	1069228599
	.long	-494729303
	.long	-1131563366
	.long	1254519184
	.long	1070399317
	.long	0
	.long	1072726016
	.long	-942299463
	.long	-1082877648
	.long	0
	.long	1070415872
	.long	619147391
	.long	1072721747
	.long	-1967928398
	.long	1069527882
	.long	-307458700
	.long	1070264618
	.long	-1537366508
	.long	1069245314
	.long	-1383162044
	.long	1069640222
	.long	1105687709
	.long	1069285937
	.long	584849058
	.long	1014417959
	.long	623637056
	.long	1070432960
	.long	0
	.long	1072726016
	.long	412057931
	.long	-1083134757
	.long	0
	.long	1070448640
	.long	-1349735008
	.long	1072722771
	.long	-1616695863
	.long	1069558475
	.long	842151704
	.long	1070278817
	.long	698937694
	.long	1069290320
	.long	-558731039
	.long	1069662077
	.long	1087796323
	.long	1069345052
	.long	273394184
	.long	1017571702
	.long	-1292410560
	.long	1070466634
	.long	0
	.long	1072726016
	.long	-425410615
	.long	-1083615072
	.long	0
	.long	1070481408
	.long	-1732182055
	.long	1072723816
	.long	1870600944
	.long	1069579418
	.long	1888430843
	.long	1070293370
	.long	270906636
	.long	1069336193
	.long	-1803074246
	.long	1069684636
	.long	-1391873109
	.long	1069406016
	.long	412104896
	.long	-1131597093
	.long	-1770642688
	.long	1070500341
	.long	0
	.long	1072726016
	.long	2113949008
	.long	-1084150066
	.long	0
	.long	1070514176
	.long	187155479
	.long	1072724882
	.long	-400488326
	.long	1069600533
	.long	-1958093684
	.long	1070308285
	.long	1086256578
	.long	1069382961
	.long	-335018635
	.long	1069707920
	.long	810985129
	.long	1069468906
	.long	211494818
	.long	1016980742
	.long	1934002768
	.long	1070534081
	.long	0
	.long	1072726016
	.long	1626317645
	.long	-1085163565
	.long	0
	.long	1070546944
	.long	846446616
	.long	1072725968
	.long	1371045640
	.long	1069621826
	.long	-1650806084
	.long	1070323569
	.long	-2046844635
	.long	1069430653
	.long	-368247211
	.long	1069731953
	.long	-1151214125
	.long	1069533799
	.long	-2002369049
	.long	-1130212477
	.long	-295682240
	.long	1070567854
	.long	0
	.long	1072726016
	.long	536093366
	.long	-1090001210
	.long	0
	.long	1070579712
	.long	996964082
	.long	1072727075
	.long	169736343
	.long	1069643300
	.long	-46631011
	.long	1070339229
	.long	-1789714739
	.long	1069479299
	.long	-2057328282
	.long	1069756759
	.long	-669094998
	.long	1069574149
	.long	457702124
	.long	1015448190
	.long	1460561816
	.long	1070598879
	.long	0
	.long	1072726016
	.long	-1310996906
	.long	1062243565
	.long	0
	.long	1070604288
	.long	1408477563
	.long	1072728203
	.long	-2054871594
	.long	1069664959
	.long	1010282384
	.long	1070355274
	.long	414003498
	.long	1069528930
	.long	-1320039763
	.long	1069782362
	.long	128837677
	.long	1069608726
	.long	-52525242
	.long	1018097476
	.long	-1390398336
	.long	1070615800
	.long	0
	.long	1072726016
	.long	-413993567
	.long	1063327399
	.long	0
	.long	1070620672
	.long	-1425341448
	.long	1072729352
	.long	1328869096
	.long	1069686809
	.long	721267678
	.long	1070371710
	.long	1548694970
	.long	1069563548
	.long	1759453606
	.long	1069808789
	.long	1401175122
	.long	1069644433
	.long	-1027898534
	.long	-1130533036
	.long	-838769992
	.long	1070632739
	.long	0
	.long	1072726016
	.long	369618723
	.long	1063915862
	.long	0
	.long	1070637056
	.long	1893333639
	.long	1072730523
	.long	172321801
	.long	1069708854
	.long	-632801416
	.long	1070388545
	.long	325936688
	.long	1069589396
	.long	-1168023908
	.long	1069836066
	.long	-1564709005
	.long	1069681318
	.long	2132647302
	.long	1017761798
	.long	252452552
	.long	1070649697
	.long	0
	.long	1072726016
	.long	-637892984
	.long	1064409968
	.long	0
	.long	1070653440
	.long	-692857391
	.long	1072731715
	.long	1919161670
	.long	1069731098
	.long	-1656043540
	.long	1070405789
	.long	345779814
	.long	1069615785
	.long	-740391103
	.long	1069864222
	.long	-153467032
	.long	1069719430
	.long	-2104743295
	.long	1018079488
	.long	-966948664
	.long	1070666672
	.long	0
	.long	1072726016
	.long	-1277832935
	.long	1064715222
	.long	0
	.long	1070669824
	.long	253620629
	.long	1072732930
	.long	1553886015
	.long	1069753547
	.long	199370935
	.long	1070423450
	.long	-761920848
	.long	1069642732
	.long	268072857
	.long	1069893287
	.long	-2017831265
	.long	1069758821
	.long	-1722355005
	.long	-1129434536
	.long	1255829488
	.long	1070683667
	.long	0
	.long	1072726016
	.long	502371631
	.long	1065026063
	.long	0
	.long	1070686208
	.long	1305882288
	.long	1072734166
	.long	-1204593179
	.long	1069776205
	.long	81765361
	.long	1070441536
	.long	1441109010
	.long	1069670257
	.long	60077870
	.long	1069923290
	.long	-518556776
	.long	1069799543
	.long	995565735
	.long	1017687176
	.long	-197908568
	.long	1070700680
	.long	0
	.long	1072726016
	.long	-701583483
	.long	1065342541
	.long	0
	.long	1070702592
	.long	-942040481
	.long	1072735424
	.long	-1888765630
	.long	1069799078
	.long	-1335052119
	.long	1070460056
	.long	1126986929
	.long	1069698377
	.long	-1944293690
	.long	1069954263
	.long	29304160
	.long	1069841654
	.long	1198400967
	.long	1017376231
	.long	451770144
	.long	1070717714
	.long	0
	.long	1072742400
	.long	644194598
	.long	-1082441928
	.long	0
	.long	1070718976
	.long	-1284813685
	.long	1072736705
	.long	142205851
	.long	1069822171
	.long	-2104984181
	.long	1070479021
	.long	-508347271
	.long	1069727111
	.long	-559617698
	.long	1069986240
	.long	2089832522
	.long	1069885210
	.long	-1972454136
	.long	1017838297
	.long	408920928
	.long	1070734767
	.long	0
	.long	1072742400
	.long	-1800178404
	.long	-1082769844
	.long	0
	.long	1070735360
	.long	1209761517
	.long	1072738009
	.long	1717648307
	.long	1069845488
	.long	1033887119
	.long	1070498440
	.long	575081458
	.long	1069756481
	.long	-1748650890
	.long	1070019256
	.long	-1688121485
	.long	1069930274
	.long	1972848072
	.long	1017736066
	.long	1186960824
	.long	1070751840
	.long	0
	.long	1072742400
	.long	-461303076
	.long	-1083103561
	.long	0
	.long	1070751744
	.long	-1093738738
	.long	1072739335
	.long	166029722
	.long	1069869036
	.long	-180229075
	.long	1070518322
	.long	-1503099772
	.long	1069786505
	.long	-867030851
	.long	1070053346
	.long	-2081641185
	.long	1069976910
	.long	2092405603
	.long	1017742726
	.long	19078856
	.long	1070768934
	.long	0
	.long	1072742400
	.long	1648485449
	.long	-1083707262
	.long	0
	.long	1070768128
	.long	1371938076
	.long	1072740685
	.long	1921120311
	.long	1069892819
	.long	284270473
	.long	1070538680
	.long	-942634428
	.long	1069817206
	.long	452257254
	.long	1070088549
	.long	1592121400
	.long	1070025185
	.long	-1577473904
	.long	1015687734
	.long	-1551476896
	.long	1070786048
	.long	0
	.long	1072742400
	.long	-410284497
	.long	-1084568904
	.long	0
	.long	1070784512
	.long	1017493953
	.long	1072742058
	.long	1063948755
	.long	1069916844
	.long	1326862555
	.long	1070559522
	.long	-1605409817
	.long	1069848606
	.long	-1094935708
	.long	1070124902
	.long	-2124837897
	.long	1070075169
	.long	-1911565536
	.long	-1130872509
	.long	-1966006264
	.long	1070803184
	.long	0
	.long	1072742400
	.long	-1536953347
	.long	-1087022027
	.long	0
	.long	1070800896
	.long	-1132587750
	.long	1072743454
	.long	815021593
	.long	1069941116
	.long	-938707443
	.long	1070580860
	.long	302715549
	.long	1069880728
	.long	-1566026315
	.long	1070162448
	.long	-1736432883
	.long	1070126936
	.long	-1088977514
	.long	-1130049239
	.long	350011472
	.long	1070820342
	.long	0
	.long	1072742400
	.long	-128686562
	.long	1062238961
	.long	0
	.long	1070817280
	.long	265535797
	.long	1072744875
	.long	667419778
	.long	1069965641
	.long	2024541661
	.long	1070599401
	.long	-803076943
	.long	1069913594
	.long	1984066829
	.long	1070201229
	.long	-668285814
	.long	1070180563
	.long	681261393
	.long	1016558983
	.long	-1602640168
	.long	1070837521
	.long	0
	.long	1072742400
	.long	-1484625395
	.long	1063474719
	.long	0
	.long	1070833664
	.long	1990728297
	.long	1072746319
	.long	700406477
	.long	1069990425
	.long	1350393300
	.long	1070610584
	.long	1740084281
	.long	1069947231
	.long	-1017483105
	.long	1070241289
	.long	737625436
	.long	1070236132
	.long	-198945506
	.long	1017131780
	.long	-1921692864
	.long	1070854723
	.long	0
	.long	1072742400
	.long	1345639080
	.long	1064214253
	.long	0
	.long	1070850048
	.long	847390733
	.long	1072747788
	.long	1598737443
	.long	1070015474
	.long	142020913
	.long	1070622033
	.long	1232082494
	.long	1069981663
	.long	82745866
	.long	1070282676
	.long	1803140120
	.long	1070293726
	.long	44233814
	.long	1018089253
	.long	1017131744
	.long	1070871948
	.long	0
	.long	1072742400
	.long	-2111304408
	.long	1064635442
	.long	0
	.long	1070866432
	.long	-2038970323
	.long	1072749281
	.long	377703102
	.long	1070040795
	.long	-245164239
	.long	1070633753
	.long	-748513361
	.long	1070016916
	.long	-1084737400
	.long	1070325436
	.long	-1852953882
	.long	1070353435
	.long	-213248676
	.long	1017720688
	.long	265558472
	.long	1070889196
	.long	0
	.long	1072742400
	.long	2009607507
	.long	1065017734
	.long	0
	.long	1070882816
	.long	-1221121800
	.long	1072750799
	.long	-1006197910
	.long	1070066393
	.long	-1849842154
	.long	1070645753
	.long	1535558699
	.long	1070053019
	.long	-1714582369
	.long	1070369622
	.long	-150451914
	.long	1070415352
	.long	1418994567
	.long	-1131267212
	.long	1778008296
	.long	1070906467
	.long	0
	.long	1072758784
	.long	-925431877
	.long	-1082183608
	.long	0
	.long	1070899200
	.long	185635512
	.long	1072752343
	.long	71274513
	.long	1070092277
	.long	-1481648405
	.long	1070658038
	.long	983947193
	.long	1070089999
	.long	2035195133
	.long	1070415286
	.long	-1862571523
	.long	1070479576
	.long	-1036878395
	.long	1016920621
	.long	-1357782336
	.long	1070923762
	.long	0
	.long	1072758784
	.long	-278050744
	.long	-1082578700
	.long	0
	.long	1070915584
	.long	-905935536
	.long	1072753911
	.long	-1665536857
	.long	1070118451
	.long	723561533
	.long	1070670616
	.long	-673847200
	.long	1070127885
	.long	-1659693649
	.long	1070462483
	.long	853064815
	.long	1070546209
	.long	-1830514962
	.long	1016883766
	.long	1144442888
	.long	1070941082
	.long	0
	.long	1072758784
	.long	-8736878
	.long	-1082980299
	.long	0
	.long	1070931968
	.long	1035611234
	.long	1072755506
	.long	2105776188
	.long	1070144924
	.long	1339924526
	.long	1070683493
	.long	-1266202681
	.long	1070166709
	.long	-1131346439
	.long	1070511271
	.long	1783848655
	.long	1070605727
	.long	-152414783
	.long	1015540534
	.long	-1884801736
	.long	1070958426
	.long	0
	.long	1072758784
	.long	-1951974294
	.long	-1083597948
	.long	0
	.long	1070948352
	.long	-1313723070
	.long	1072757126
	.long	-1030057235
	.long	1070171702
	.long	-2015631827
	.long	1070696677
	.long	2032941035
	.long	1070206502
	.long	657844429
	.long	1070561711
	.long	1858917568
	.long	1070641617
	.long	-1095891696
	.long	1016398517
	.long	-120616256
	.long	1070975795
	.long	0
	.long	1072758784
	.long	927659852
	.long	-1084627655
	.long	0
	.long	1070964736
	.long	1932120634
	.long	1072758773
	.long	-1251413886
	.long	1070198793
	.long	-2053923406
	.long	1070710176
	.long	42185098
	.long	1070247297
	.long	1928761672
	.long	1070604980
	.long	967641977
	.long	1070678882
	.long	-2138928250
	.long	-1129481880
	.long	-397954768
	.long	1070993190
	.long	0
	.long	1072758784
	.long	1535885441
	.long	-1092281940
	.long	0
	.long	1070981120
	.long	-784689529
	.long	1072760446
	.long	-833667231
	.long	1070226204
	.long	1047938373
	.long	1070723998
	.long	1871742747
	.long	1070289127
	.long	1867022231
	.long	1070631947
	.long	1730567200
	.long	1070717584
	.long	-1722342483
	.long	1018059522
	.long	-941344096
	.long	1071010611
	.long	0
	.long	1072758784
	.long	-363192988
	.long	1062861636
	.long	0
	.long	1070997504
	.long	484579033
	.long	1072762147
	.long	-1237447853
	.long	1070253943
	.long	-310259627
	.long	1070738150
	.long	809944492
	.long	1070332029
	.long	-55402524
	.long	1070659838
	.long	-912637874
	.long	1070757789
	.long	-1021302040
	.long	-1130189026
	.long	45671288
	.long	1071028059
	.long	0
	.long	1072758784
	.long	-1003638101
	.long	1063929401
	.long	0
	.long	1071013888
	.long	-1458673116
	.long	1072763874
	.long	1214196843
	.long	1070282018
	.long	368170700
	.long	1070752643
	.long	351917397
	.long	1070376039
	.long	610154878
	.long	1070688691
	.long	-1723360009
	.long	1070799567
	.long	-1986870744
	.long	-1133310657
	.long	86063912
	.long	1071045533
	.long	0
	.long	1072758784
	.long	241837161
	.long	1064559273
	.long	0
	.long	1071030272
	.long	-894769451
	.long	1072765629
	.long	-1815110877
	.long	1070310436
	.long	-2045744428
	.long	1070767483
	.long	1586923275
	.long	1070421195
	.long	-1720331155
	.long	1070718541
	.long	-1492518748
	.long	1070842990
	.long	1165717340
	.long	-1130580432
	.long	1019772872
	.long	1071063034
	.long	0
	.long	1072758784
	.long	-1427712707
	.long	1065008586
	.long	0
	.long	1071046656
	.long	-659728226
	.long	1072767412
	.long	-580219767
	.long	1070339206
	.long	1512585361
	.long	1070782681
	.long	-810224223
	.long	1070467537
	.long	-816231498
	.long	1070749429
	.long	-1839117271
	.long	1070888135
	.long	1395675625
	.long	-1131873487
	.long	414297384
	.long	1071080563
	.long	0
	.long	1072775168
	.long	1386701270
	.long	-1082242265
	.long	0
	.long	1071063040
	.long	740535114
	.long	1072769224
	.long	-1582914730
	.long	1070368337
	.long	-6706205
	.long	1070798245
	.long	-1097931582
	.long	1070515107
	.long	287341917
	.long	1070781397
	.long	50228728
	.long	1070935083
	.long	-28471821
	.long	-1130106108
	.long	155173360
	.long	1071098120
	.long	0
	.long	1072775168
	.long	-598428126
	.long	-1082705965
	.long	0
	.long	1071079424
	.long	541200179
	.long	1072771064
	.long	-1769599607
	.long	1070397837
	.long	940896914
	.long	1070814187
	.long	669522935
	.long	1070563948
	.long	-1702726830
	.long	1070814486
	.long	-434845985
	.long	1070983917
	.long	918646796
	.long	1015066331
	.long	-2143405016
	.long	1071115705
	.long	0
	.long	1072775168
	.long	-1108292410
	.long	-1083176993
	.long	0
	.long	1071095808
	.long	309435450
	.long	1072772933
	.long	-1382387329
	.long	1070427715
	.long	486198546
	.long	1070830515
	.long	-511996155
	.long	1070605099
	.long	-1304923204
	.long	1070848743
	.long	-1346956571
	.long	1071034729
	.long	197608857
	.long	1016679436
	.long	-253112448
	.long	1071133319
	.long	0
	.long	1072775168
	.long	482839717
	.long	-1084131877
	.long	0
	.long	1071112192
	.long	1650423876
	.long	1072774831
	.long	371038584
	.long	1070457981
	.long	585514442
	.long	1070847240
	.long	-1959868437
	.long	1070630858
	.long	-973727288
	.long	1070884215
	.long	-1601539106
	.long	1071087612
	.long	1853576319
	.long	1017240542
	.long	-805742856
	.long	1071150963
	.long	0
	.long	1072775168
	.long	142329682
	.long	-1087043110
	.long	0
	.long	1071128576
	.long	1913423517
	.long	1072776759
	.long	1059051840
	.long	1070488643
	.long	492914260
	.long	1070864373
	.long	1193300717
	.long	1070657322
	.long	-2012123738
	.long	1070920952
	.long	270604214
	.long	1071142666
	.long	558527272
	.long	-1129615735
	.long	-1817770920
	.long	1071168637
	.long	0
	.long	1072775168
	.long	840594553
	.long	1062788552
	.long	0
	.long	1071144960
	.long	-1512200764
	.long	1072778717
	.long	-640862407
	.long	1070519711
	.long	1120338962
	.long	1070881925
	.long	1237943208
	.long	1070684516
	.long	251129099
	.long	1070959006
	.long	-1135332199
	.long	1071199994
	.long	2066346542
	.long	1018164493
	.long	-1279668400
	.long	1071186341
	.long	0
	.long	1072775168
	.long	-1152677920
	.long	1064024907
	.long	0
	.long	1071161344
	.long	1689026930
	.long	1072780706
	.long	-607489977
	.long	1070551196
	.long	810918255
	.long	1070899908
	.long	-766418745
	.long	1070712466
	.long	-158361338
	.long	1070998430
	.long	503066338
	.long	1071259709
	.long	1730081695
	.long	1017978905
	.long	-1450236136
	.long	1071204076
	.long	0
	.long	1072775168
	.long	-1400802903
	.long	1064673892
	.long	0
	.long	1071177728
	.long	400097026
	.long	1072782726
	.long	-2119500450
	.long	1070583108
	.long	-294471818
	.long	1070918333
	.long	931107903
	.long	1070741201
	.long	-2086310473
	.long	1071039284
	.long	-1016239024
	.long	1071321925
	.long	1981102011
	.long	-1132969499
	.long	-266016760
	.long	1071221842
	.long	0
	.long	1072775168
	.long	-654376482
	.long	1065190935
	.long	0
	.long	1071194112
	.long	727403162
	.long	1072784777
	.long	-1463768890
	.long	1070605776
	.long	-182131530
	.long	1070937214
	.long	-177869379
	.long	1070770747
	.long	-913495998
	.long	1071081626
	.long	-1322732323
	.long	1071386767
	.long	1146267241
	.long	-1131782421
	.long	69441656
	.long	1071239641
	.long	0
	.long	1072791552
	.long	-1531615647
	.long	-1082493228
	.long	0
	.long	1071210496
	.long	232164296
	.long	1072786860
	.long	247811130
	.long	1070622175
	.long	818252306
	.long	1070956564
	.long	-1144271633
	.long	1070801136
	.long	-770116736
	.long	1071125520
	.long	-919134935
	.long	1071454364
	.long	493776583
	.long	1012450241
	.long	1676209944
	.long	1071257471
	.long	0
	.long	1072791552
	.long	695482470
	.long	-1083026446
	.long	0
	.long	1071226880
	.long	816597160
	.long	1072788975
	.long	58435520
	.long	1070638803
	.long	116247832
	.long	1070976395
	.long	2103939464
	.long	1070832398
	.long	-938311357
	.long	1071171032
	.long	968057146
	.long	1071524854
	.long	-955474826
	.long	-1131986816
	.long	-1886214816
	.long	1071275334
	.long	0
	.long	1072791552
	.long	-1485918098
	.long	-1083956834
	.long	0
	.long	1071243264
	.long	135298799
	.long	1072791123
	.long	1080628102
	.long	1070655666
	.long	1537356186
	.long	1070996721
	.long	2146633302
	.long	1070864565
	.long	610419014
	.long	1071218232
	.long	-217225847
	.long	1071598380
	.long	1960643739
	.long	-1131590951
	.long	151623928
	.long	1071293231
	.long	0
	.long	1072791552
	.long	-133097796
	.long	-1086664834
	.long	0
	.long	1071259648
	.long	186546845
	.long	1072793304
	.long	-1451010320
	.long	1070672770
	.long	-1765781032
	.long	1071017557
	.long	1831380125
	.long	1070897671
	.long	-569059818
	.long	1071267191
	.long	324022099
	.long	1071659885
	.long	1255511815
	.long	1017452288
	.long	1410154248
	.long	1071311161
	.long	0
	.long	1072791552
	.long	2045408559
	.long	1062953004
	.long	0
	.long	1071276032
	.long	-1276218747
	.long	1072795518
	.long	1321512768
	.long	1070690122
	.long	-1447922179
	.long	1071038918
	.long	1689814713
	.long	1070931751
	.long	-1653431996
	.long	1071317988
	.long	-417301035
	.long	1071699919
	.long	-1904750158
	.long	-1129656802
	.long	-163615624
	.long	1071329125
	.long	0
	.long	1072791552
	.long	-588969483
	.long	1064238439
	.long	0
	.long	1071292416
	.long	2141975333
	.long	1072797767
	.long	1842437851
	.long	1070707727
	.long	359467277
	.long	1071060820
	.long	256175682
	.long	1070966842
	.long	-228505140
	.long	1071370702
	.long	-1631879853
	.long	1071741716
	.long	88872826
	.long	1017219175
	.long	1999664632
	.long	1071347125
	.long	0
	.long	1072791552
	.long	-1410128517
	.long	1064847231
	.long	0
	.long	1071308800
	.long	-290643738
	.long	1072800050
	.long	1940891892
	.long	1070725592
	.long	-256234684
	.long	1071083277
	.long	-1300798446
	.long	1071002981
	.long	-1907654271
	.long	1071425419
	.long	-560827814
	.long	1071785365
	.long	576644208
	.long	-1129588155
	.long	1617669088
	.long	1071365160
	.long	0
	.long	1072807936
	.long	1390353022
	.long	-1082209007
	.long	0
	.long	1071325184
	.long	-2071274730
	.long	1072802369
	.long	-23909701
	.long	1070743723
	.long	-287840163
	.long	1071106308
	.long	1178644915
	.long	1071040210
	.long	-2117070816
	.long	1071482227
	.long	-1724335815
	.long	1071830963
	.long	920088066
	.long	1017196235
	.long	1032074288
	.long	1071383231
	.long	0
	.long	1072807936
	.long	1965353584
	.long	-1082802565
	.long	0
	.long	1071341568
	.long	-935736916
	.long	1072804723
	.long	-541290195
	.long	1070762128
	.long	1718477770
	.long	1071129930
	.long	-1957214852
	.long	1071078569
	.long	2122208766
	.long	1071541220
	.long	1760311159
	.long	1071878611
	.long	1300922533
	.long	-1130397954
	.long	-1675475040
	.long	1071401338
	.long	0
	.long	1072807936
	.long	-1939035993
	.long	-1083631505
	.long	0
	.long	1071357952
	.long	1143234601
	.long	1072807114
	.long	493795686
	.long	1070780814
	.long	1491050016
	.long	1071154160
	.long	587792260
	.long	1071118103
	.long	485234322
	.long	1071602497
	.long	-273776770
	.long	1071928416
	.long	-1035484543
	.long	-1129535350
	.long	202429480
	.long	1071419483
	.long	0
	.long	1072807936
	.long	-587286794
	.long	-1085690402
	.long	0
	.long	1071374336
	.long	-2042240858
	.long	1072809541
	.long	-184919668
	.long	1070799786
	.long	2046873021
	.long	1071179017
	.long	-1468600412
	.long	1071158856
	.long	-1816446048
	.long	1071655416
	.long	1047958674
	.long	1071980494
	.long	-624456794
	.long	-1134314230
	.long	525044232
	.long	1071437665
	.long	0
	.long	1072807936
	.long	394434779
	.long	1062802969
	.long	0
	.long	1071390720
	.long	541485715
	.long	1072812006
	.long	-589209325
	.long	1070819054
	.long	942776930
	.long	1071204521
	.long	-883738953
	.long	1071200877
	.long	-184327515
	.long	1071688496
	.long	1141851620
	.long	1072034964
	.long	2041057720
	.long	-1130253761
	.long	1779259080
	.long	1071455885
	.long	0
	.long	1072807936
	.long	-1932188016
	.long	1064291392
	.long	0
	.long	1071407104
	.long	-1483202214
	.long	1072814508
	.long	-2028777104
	.long	1070838625
	.long	-991350896
	.long	1071230691
	.long	1815808518
	.long	1071244216
	.long	1134389635
	.long	1071722883
	.long	1145260014
	.long	1072091955
	.long	337476318
	.long	1017013895
	.long	-2099293576
	.long	1071474144
	.long	0
	.long	1072807936
	.long	-1742644659
	.long	1064938663
	.long	0
	.long	1071423488
	.long	-1248495695
	.long	1072817049
	.long	-482090103
	.long	1070858506
	.long	498378107
	.long	1071257550
	.long	-1161246169
	.long	1071288924
	.long	699158537
	.long	1071758636
	.long	1476832190
	.long	1072151603
	.long	-648819734
	.long	1015641146
	.long	44573728
	.long	1071492443
	.long	0
	.long	1072824320
	.long	1787317935
	.long	-1082366390
	.long	0
	.long	1071439872
	.long	-408900322
	.long	1072819629
	.long	559768280
	.long	1070878707
	.long	364016686
	.long	1071285118
	.long	1434204770
	.long	1071335057
	.long	-1698771808
	.long	1071795819
	.long	-144886247
	.long	1072214052
	.long	-1791221983
	.long	-1134517205
	.long	-2068061848
	.long	1071510781
	.long	0
	.long	1072824320
	.long	1599267433
	.long	-1083026920
	.long	0
	.long	1071456256
	.long	-549155871
	.long	1072822249
	.long	-1274004155
	.long	1070899234
	.long	1603334482
	.long	1071313418
	.long	1805200934
	.long	1071382671
	.long	130125616
	.long	1071834501
	.long	-1915844158
	.long	1072279457
	.long	-441130787
	.long	-1131704155
	.long	-1494454544
	.long	1071529160
	.long	0
	.long	1072824320
	.long	1994931694
	.long	-1084216255
	.long	0
	.long	1071472640
	.long	1112962641
	.long	1072824910
	.long	1400987208
	.long	1070920098
	.long	-1743216858
	.long	1071342474
	.long	-518030407
	.long	1071431826
	.long	-1769071556
	.long	1071874751
	.long	441206097
	.long	1072347980
	.long	-966860311
	.long	-1129564083
	.long	161113632
	.long	1071547581
	.long	0
	.long	1072824320
	.long	-1280145538
	.long	1061319186
	.long	0
	.long	1071489024
	.long	-1155935386
	.long	1072827611
	.long	1639712
	.long	1070941307
	.long	1363854295
	.long	1071372311
	.long	1963045454
	.long	1071482586
	.long	1566428305
	.long	1071916646
	.long	-612152390
	.long	1072419793
	.long	1680900068
	.long	-1129961326
	.long	1338410464
	.long	1071566043
	.long	0
	.long	1072824320
	.long	866569472
	.long	1063892854
	.long	0
	.long	1071505408
	.long	-127603912
	.long	1072830354
	.long	91480266
	.long	1070962870
	.long	505256965
	.long	1071402954
	.long	-373097660
	.long	1071535015
	.long	-1569810001
	.long	1071960264
	.long	162046809
	.long	1072495083
	.long	-642799136
	.long	-1129449375
	.long	522433528
	.long	1071584548
	.long	0
	.long	1072824320
	.long	1693136950
	.long	1064801016
	.long	0
	.long	1071521792
	.long	-1379449608
	.long	1072833140
	.long	-42444457
	.long	1070984796
	.long	-1629480134
	.long	1071434429
	.long	816297909
	.long	1071589184
	.long	539888909
	.long	1072005690
	.long	-1628706775
	.long	1072574043
	.long	-487942265
	.long	-1131168958
	.long	539595960
	.long	1071603096
	.long	0
	.long	1072840704
	.long	951781476
	.long	-1082291374
	.long	0
	.long	1071538176
	.long	-1817077494
	.long	1072835969
	.long	-745403121
	.long	1071007097
	.long	-1907367426
	.long	1071466765
	.long	-875303591
	.long	1071644917
	.long	1313208180
	.long	1072053011
	.long	802519246
	.long	1072656884
	.long	-1058952121
	.long	1017687324
	.long	-30949536
	.long	1071621687
	.long	0
	.long	1072840704
	.long	1315370401
	.long	-1083015572
	.long	0
	.long	1071554560
	.long	1738500833
	.long	1072838842
	.long	-938104587
	.long	1071029782
	.long	-814951223
	.long	1071499990
	.long	-47522248
	.long	1071673850
	.long	-1343318480
	.long	1072102321
	.long	1786432386
	.long	1072718537
	.long	903042395
	.long	1014904192
	.long	1733924232
	.long	1071640324
	.long	0
	.long	1072840704
	.long	-2108392593
	.long	-1084418463
	.long	0
	.long	1071570944
	.long	-331194549
	.long	1072841759
	.long	1942972340
	.long	1071052862
	.long	2081853882
	.long	1071534135
	.long	2063079694
	.long	1071703767
	.long	670208120
	.long	1072153720
	.long	330694620
	.long	1072764178
	.long	1609278209
	.long	1013761953
	.long	108880420
	.long	1071651839
	.long	0
	.long	1072840704
	.long	159198172
	.long	1062240177
	.long	0
	.long	1071587328
	.long	-374381261
	.long	1072844722
	.long	-876697596
	.long	1071076347
	.long	-192878799
	.long	1071569230
	.long	275615942
	.long	1071734709
	.long	1390072880
	.long	1072207311
	.long	687517203
	.long	1072812114
	.long	-397806758
	.long	-1130034826
	.long	-777110712
	.long	1071661202
	.long	0
	.long	1072840704
	.long	1590322781
	.long	1064265171
	.long	0
	.long	1071603712
	.long	764266490
	.long	1072847732
	.long	620835957
	.long	1071100250
	.long	-275946617
	.long	1071605309
	.long	-1562424321
	.long	1071766719
	.long	-406657819
	.long	1072263205
	.long	472573099
	.long	1072862480
	.long	-1339409133
	.long	1017432284
	.long	-251813928
	.long	1071670589
	.long	0
	.long	1072840704
	.long	-1916274112
	.long	1065055277
	.long	0
	.long	1071620096
	.long	-1958198286
	.long	1072850788
	.long	955318871
	.long	1071124581
	.long	-1944510287
	.long	1071642406
	.long	1363727588
	.long	1071799845
	.long	543278078
	.long	1072321521
	.long	1536360677
	.long	1072915419
	.long	1649118103
	.long	-1130072204
	.long	-1043668168
	.long	1071680000
	.long	0
	.long	1072857088
	.long	-1212412336
	.long	-1082614924
	.long	0
	.long	1071636480
	.long	-599771406
	.long	1072853892
	.long	695688335
	.long	1071149353
	.long	833062774
	.long	1071662614
	.long	-1779561371
	.long	1071834134
	.long	685789537
	.long	1072382381
	.long	-14813111
	.long	1072971084
	.long	-1162106599
	.long	1017484316
	.long	-1558045668
	.long	1071689435
	.long	0
	.long	1072857088
	.long	2140281895
	.long	-1083640249
	.long	0
	.long	1071648768
	.long	198792
	.long	1072857046
	.long	-2053594699
	.long	1071174578
	.long	1309394773
	.long	1071682234
	.long	1782746860
	.long	1071869638
	.long	1156912329
	.long	1072445888
	.long	415559482
	.long	1073029637
	.long	-1470959051
	.long	1073791441
	.long	-804297544
	.long	1017610370
	.long	-171424008
	.long	1071698894
	.long	0
	.long	1072857088
	.long	2075933151
	.long	-1090191362
	.long	0
	.long	1071656960
	.long	-595466537
	.long	1072860248
	.long	1312551766
	.long	1071200270
	.long	-3783001
	.long	1071702418
	.long	1832154477
	.long	1071906410
	.long	328907921
	.long	1072512239
	.long	-134147648
	.long	1073091256
	.long	879216264
	.long	1073849148
	.long	888509509
	.long	-1131767468
	.long	474469712
	.long	1071708379
	.long	0
	.long	1072857088
	.long	63811135
	.long	1063825849
	.long	0
	.long	1071665152
	.long	1582036755
	.long	1072863502
	.long	-80580962
	.long	1071226441
	.long	-2022098662
	.long	1071723188
	.long	204464933
	.long	1071944507
	.long	-1364532784
	.long	1072581553
	.long	524515261
	.long	1073156128
	.long	478701061
	.long	1073910385
	.long	1899024025
	.long	-1130153295
	.long	2063445776
	.long	1071717888
	.long	0
	.long	1072857088
	.long	1274483399
	.long	1064898142
	.long	0
	.long	1071673344
	.long	2026576443
	.long	1072866807
	.long	-2138978863
	.long	1071253107
	.long	224289945
	.long	1071744564
	.long	-1361220634
	.long	1071983987
	.long	485599904
	.long	1072653989
	.long	1211426217
	.long	1073224450
	.long	-52131578
	.long	1073975398
	.long	1811631237
	.long	-1130148273
	.long	2015800020
	.long	1071727423
	.long	0
	.long	1072873472
	.long	887473524
	.long	-1082521465
	.long	0
	.long	1071681536
	.long	646146267
	.long	1072870165
	.long	1408652727
	.long	1071280281
	.long	-1383058708
	.long	1071766567
	.long	-436695517
	.long	1072024914
	.long	174636540
	.long	1072711480
	.long	788887584
	.long	1073296437
	.long	-622288132
	.long	1074044455
	.long	-89897627
	.long	1015448364
	.long	2079162976
	.long	1071736984
	.long	0
	.long	1072873472
	.long	-114406847
	.long	-1083583054
	.long	0
	.long	1071689728
	.long	1767213726
	.long	1072873576
	.long	1929452388
	.long	1071307978
	.long	-2062478230
	.long	1071789222
	.long	-1653817540
	.long	1072067354
	.long	-713691126
	.long	1072751073
	.long	-1377167385
	.long	1073372317
	.long	-653115780
	.long	1074117842
	.long	-869604892
	.long	-1130217905
	.long	-260486928
	.long	1071746571
	.long	0
	.long	1072873472
	.long	1655136586
	.long	1058675285
	.long	0
	.long	1071697920
	.long	1254363334
	.long	1072877042
	.long	1759919511
	.long	1071336214
	.long	-1079880014
	.long	1071812552
	.long	1375596634
	.long	1072111376
	.long	122931330
	.long	1072792494
	.long	-242085744
	.long	1073452336
	.long	351879868
	.long	1074195870
	.long	-1983414365
	.long	1017280633
	.long	1107061020
	.long	1071756186
	.long	0
	.long	1072873472
	.long	-2011067178
	.long	1064035477
	.long	0
	.long	1071706112
	.long	-600048347
	.long	1072880563
	.long	1430336007
	.long	1071365005
	.long	-1502008335
	.long	1071836583
	.long	1254416944
	.long	1072157053
	.long	-1427715323
	.long	1072835840
	.long	-922579343
	.long	1073536757
	.long	-1274256788
	.long	1074278872
	.long	-1737231228
	.long	1016414191
	.long	-557567808
	.long	1071765827
	.long	0
	.long	1072873472
	.long	1006445729
	.long	1065071580
	.long	0
	.long	1071714304
	.long	929079707
	.long	1072884142
	.long	-219946261
	.long	1071394367
	.long	-1344156360
	.long	1071861341
	.long	-691139802
	.long	1072204462
	.long	743114438
	.long	1072881220
	.long	327685074
	.long	1073625862
	.long	603805202
	.long	1074367213
	.long	-1213264300
	.long	-1131287527
	.long	927527188
	.long	1071775497
	.long	0
	.long	1072889856
	.long	-1621203586
	.long	-1082764856
	.long	0
	.long	1071722496
	.long	2119894827
	.long	1072887778
	.long	-1625908147
	.long	1071424319
	.long	-1881019562
	.long	1071886854
	.long	-1892621897
	.long	1072253686
	.long	-808478585
	.long	1072928745
	.long	1337455199
	.long	1073719952
	.long	-1804313200
	.long	1074461283
	.long	705926032
	.long	1016501596
	.long	-1103185376
	.long	1071785194
	.long	0
	.long	1072889856
	.long	1240574597
	.long	-1084212477
	.long	0
	.long	1071730688
	.long	-601026954
	.long	1072891473
	.long	1631515656
	.long	1071454878
	.long	1240270909
	.long	1071913151
	.long	-1644356061
	.long	1072304810
	.long	850878326
	.long	1072978538
	.long	-1486405817
	.long	1073780588
	.long	-1337545622
	.long	1074561508
	.long	2098981760
	.long	1015521271
	.long	-391726928
	.long	1071794920
	.long	0
	.long	1072889856
	.long	-1271277298
	.long	1062815600
	.long	0
	.long	1071738880
	.long	-2063040819
	.long	1072895229
	.long	-218868848
	.long	1071486062
	.long	1136581125
	.long	1071940262
	.long	174329303
	.long	1072357926
	.long	-693905771
	.long	1073030725
	.long	134844384
	.long	1073833119
	.long	-1658081817
	.long	1074668348
	.long	1314882376
	.long	1017025676
	.long	769736280
	.long	1071804676
	.long	0
	.long	1072889856
	.long	142527770
	.long	1064631685
	.long	0
	.long	1071747072
	.long	-1230357401
	.long	1072899046
	.long	-930527192
	.long	1071517892
	.long	1500611465
	.long	1071968219
	.long	177042978
	.long	1072413129
	.long	-1197069550
	.long	1073085445
	.long	-467048399
	.long	1073888666
	.long	1237366832
	.long	1074782302
	.long	-1055351320
	.long	1017221383
	.long	130055116
	.long	1071814461
	.long	0
	.long	1072906240
	.long	1438882109
	.long	-1082386103
	.long	0
	.long	1071755264
	.long	-1195777525
	.long	1072902926
	.long	437339787
	.long	1071550388
	.long	-219725599
	.long	1071997055
	.long	1536285460
	.long	1072470521
	.long	-183738454
	.long	1073142843
	.long	217760199
	.long	1073947434
	.long	-1991312080
	.long	1074847155
	.long	931387670
	.long	-1129875953
	.long	-224659984
	.long	1071824275
	.long	0
	.long	1072906240
	.long	-1942230401
	.long	-1083579762
	.long	0
	.long	1071763456
	.long	-584188307
	.long	1072906870
	.long	-320134549
	.long	1071583569
	.long	446768078
	.long	1072026807
	.long	-1608555016
	.long	1072530210
	.long	2031936899
	.long	1073203076
	.long	-1051038820
	.long	1074009637
	.long	1680347115
	.long	1074912081
	.long	684355003
	.long	1017185379
	.long	1835318956
	.long	1071834121
	.long	0
	.long	1072906240
	.long	1878222049
	.long	1061402345
	.long	0
	.long	1071771648
	.long	-2135606841
	.long	1072910880
	.long	500767362
	.long	1071617460
	.long	-1776375350
	.long	1072057509
	.long	-967099601
	.long	1072592310
	.long	-1005394529
	.long	1073266309
	.long	38152785
	.long	1074075512
	.long	278906907
	.long	1074981448
	.long	-1619998056
	.long	1016438348
	.long	-105219268
	.long	1071843997
	.long	0
	.long	1072906240
	.long	-1254504798
	.long	1064444032
	.long	0
	.long	1071779840
	.long	186952922
	.long	1072914957
	.long	-1244134474
	.long	1071648376
	.long	-1812485
	.long	1072089201
	.long	1973351302
	.long	1072656942
	.long	-1194268556
	.long	1073332721
	.long	135245934
	.long	1074145309
	.long	-772366857
	.long	1075055602
	.long	-872746113
	.long	-1132164630
	.long	469844412
	.long	1071853906
	.long	0
	.long	1072922624
	.long	-615307673
	.long	-1082264844
	.long	0
	.long	1071788032
	.long	-269764755
	.long	1072919101
	.long	-1051460532
	.long	1071666064
	.long	-481679770
	.long	1072121924
	.long	-1033853952
	.long	1072708740
	.long	-2115563775
	.long	1073402502
	.long	-607869042
	.long	1074219300
	.long	-1950571808
	.long	1075134923
	.long	2078966898
	.long	1016886568
	.long	1534613752
	.long	1071863846
	.long	0
	.long	1072922624
	.long	680600848
	.long	-1083472864
	.long	0
	.long	1071796224
	.long	-1366447673
	.long	1072923316
	.long	-2077447239
	.long	1071684142
	.long	-634035903
	.long	1072155720
	.long	-1126892104
	.long	1072743783
	.long	-1984549838
	.long	1073475855
	.long	414041273
	.long	1074297781
	.long	-988350735
	.long	1075219821
	.long	775411363
	.long	-1131201941
	.long	1112663468
	.long	1071873819
	.long	0
	.long	1072922624
	.long	1833843322
	.long	1061528948
	.long	0
	.long	1071804416
	.long	-752772532
	.long	1072927602
	.long	-734444489
	.long	1071702622
	.long	-1394291596
	.long	1072190634
	.long	159510035
	.long	1072780296
	.long	-2002103502
	.long	1073552998
	.long	-633613937
	.long	1074381066
	.long	-1790431
	.long	1075310745
	.long	2069400254
	.long	-1129392789
	.long	1573622044
	.long	1071883825
	.long	0
	.long	1072922624
	.long	563760041
	.long	1064530643
	.long	0
	.long	1071812608
	.long	-153565508
	.long	1072931961
	.long	286882354
	.long	1071721519
	.long	1945717205
	.long	1072226713
	.long	-293976035
	.long	1072818353
	.long	-1413576664
	.long	1073634164
	.long	-616725572
	.long	1074469500
	.long	1841586432
	.long	1075408185
	.long	-1395067236
	.long	1017608588
	.long	1044919784
	.long	1071893865
	.long	0
	.long	1072939008
	.long	658064273
	.long	-1082423799
	.long	0
	.long	1071820800
	.long	-1064573128
	.long	1072936395
	.long	727496375
	.long	1071740845
	.long	-1125577039
	.long	1072264006
	.long	1760329476
	.long	1072858038
	.long	-709300988
	.long	1073719603
	.long	-875692134
	.long	1074563453
	.long	239779669
	.long	1075512674
	.long	518266947
	.long	1015554038
	.long	2003476620
	.long	1071903939
	.long	0
	.long	1072939008
	.long	-399404922
	.long	-1083938690
	.long	0
	.long	1071828992
	.long	-447750050
	.long	1072940905
	.long	-1407549024
	.long	1071760615
	.long	-1324375258
	.long	1072302566
	.long	-2079365221
	.long	1072899435
	.long	-18223334
	.long	1073775703
	.long	-468425917
	.long	1074663326
	.long	1127519913
	.long	1075624795
	.long	1508935616
	.long	-1130005940
	.long	-1607372376
	.long	1071914048
	.long	0
	.long	1072939008
	.long	1065449277
	.long	1063102357
	.long	0
	.long	1071837184
	.long	689064412
	.long	1072945494
	.long	-1123792183
	.long	1071780845
	.long	1066500008
	.long	1072342448
	.long	-1221064071
	.long	1072942636
	.long	1466628840
	.long	1073823108
	.long	-1163168525
	.long	1074769554
	.long	-384655798
	.long	1075745186
	.long	216826871
	.long	1016092273
	.long	1393832340
	.long	1071924193
	.long	0
	.long	1072939008
	.long	306830276
	.long	1064916521
	.long	0
	.long	1071845376
	.long	1597916949
	.long	1072950162
	.long	821650897
	.long	1071801551
	.long	1246703538
	.long	1072383709
	.long	1271064911
	.long	1072987739
	.long	1425603051
	.long	1073873081
	.long	858807711
	.long	1074836504
	.long	-2106658457
	.long	1075856761
	.long	-529613650
	.long	-1130940512
	.long	773956924
	.long	1071934374
	.long	0
	.long	1072955392
	.long	-1044845820
	.long	-1082888800
	.long	0
	.long	1071853568
	.long	1802123477
	.long	1072954912
	.long	-1926838368
	.long	1071822748
	.long	-701925128
	.long	1072426410
	.long	-879966511
	.long	1073034846
	.long	-661344428
	.long	1073925788
	.long	2046281278
	.long	1074896699
	.long	210664350
	.long	1075926308
	.long	333224576
	.long	1017415120
	.long	-752913076
	.long	1071944591
	.long	0
	.long	1072955392
	.long	1551020519
	.long	-1086457527
	.long	0
	.long	1071861760
	.long	1108269287
	.long	1072959746
	.long	314368690
	.long	1071844455
	.long	1054749790
	.long	1072470617
	.long	2026550374
	.long	1073084069
	.long	1051550446
	.long	1073981409
	.long	-1665555928
	.long	1074960840
	.long	468142512
	.long	1076001140
	.long	-961969377
	.long	1018159759
	.long	-408208832
	.long	1071954846
	.long	0
	.long	1072955392
	.long	249096054
	.long	1064370754
	.long	0
	.long	1071869952
	.long	-381134013
	.long	1072964665
	.long	-918101965
	.long	1071866688
	.long	1858886270
	.long	1072516396
	.long	-802740902
	.long	1073135524
	.long	1917864929
	.long	1074040134
	.long	155474901
	.long	1075029229
	.long	1956518393
	.long	1076081717
	.long	-357894850
	.long	-1136597019
	.long	358416496
	.long	1071965140
	.long	0
	.long	1072971776
	.long	-1213940426
	.long	-1082407402
	.long	0
	.long	1071878144
	.long	2040675363
	.long	1072969673
	.long	-1984992611
	.long	1071889468
	.long	452285370
	.long	1072563820
	.long	371349146
	.long	1073189338
	.long	454602283
	.long	1074102171
	.long	-2000686528
	.long	1075102192
	.long	26887458
	.long	1076168545
	.long	-33764589
	.long	1017430584
	.long	166418680
	.long	1071975472
	.long	0
	.long	1072971776
	.long	-1148732959
	.long	-1084199668
	.long	0
	.long	1071886336
	.long	518942517
	.long	1072974771
	.long	167791901
	.long	1071912814
	.long	60422740
	.long	1072612964
	.long	-254815994
	.long	1073245642
	.long	-389542249
	.long	1074167741
	.long	-640864903
	.long	1075180087
	.long	-939039931
	.long	1076262177
	.long	-1333135441
	.long	1018087254
	.long	2001800596
	.long	1071985843
	.long	0
	.long	1072971776
	.long	-589403761
	.long	1063740989
	.long	0
	.long	1071894528
	.long	422097342
	.long	1072979961
	.long	-370453527
	.long	1071936745
	.long	970654646
	.long	1072663908
	.long	254380935
	.long	1073304582
	.long	-3724618
	.long	1074237086
	.long	-1810606361
	.long	1075263303
	.long	1957109361
	.long	1076363226
	.long	-25918541
	.long	-1129386605
	.long	334794432
	.long	1071996255
	.long	0
	.long	1072971776
	.long	682737151
	.long	1065351449
	.long	0
	.long	1071902720
	.long	-1117335356
	.long	1072985245
	.long	-1070850029
	.long	1071961285
	.long	-1251514164
	.long	1072704992
	.long	-548165699
	.long	1073366307
	.long	-1976208506
	.long	1074310465
	.long	1580803993
	.long	1075352263
	.long	-1753978163
	.long	1076472363
	.long	-1264116156
	.long	1016221450
	.long	-1697565368
	.long	1072006707
	.long	0
	.long	1072988160
	.long	845051742
	.long	-1083784059
	.long	0
	.long	1071910912
	.long	1993786513
	.long	1072990627
	.long	684984361
	.long	1071986456
	.long	-1740305021
	.long	1072732394
	.long	1431465531
	.long	1073430983
	.long	1906267150
	.long	1074388157
	.long	-2141386543
	.long	1075447429
	.long	-1525406157
	.long	1076590330
	.long	-234924292
	.long	1009441191
	.long	-878546364
	.long	1072017201
	.long	0
	.long	1072988160
	.long	-1383522048
	.long	1063470829
	.long	0
	.long	1071919104
	.long	-942959412
	.long	1072996108
	.long	-728585698
	.long	1072012280
	.long	886229634
	.long	1072760831
	.long	-1008755119
	.long	1073498782
	.long	1173718862
	.long	1074470465
	.long	-558291384
	.long	1075549306
	.long	-654060238
	.long	1076717945
	.long	80117598
	.long	1017393713
	.long	1796278692
	.long	1072027738
	.long	0
	.long	1072988160
	.long	-879440870
	.long	1065290951
	.long	0
	.long	1071927296
	.long	1255803888
	.long	1073001692
	.long	-1727035873
	.long	1072038784
	.long	-873695934
	.long	1072790352
	.long	-918749553
	.long	1073569892
	.long	1458794467
	.long	1074557716
	.long	1768838004
	.long	1075658447
	.long	393785117
	.long	1076856113
	.long	837540484
	.long	1014649791
	.long	1122137696
	.long	1072038318
	.long	0
	.long	1073004544
	.long	1273503617
	.long	-1083816086
	.long	0
	.long	1071935488
	.long	-1274653184
	.long	1073007380
	.long	-2106926513
	.long	1072065993
	.long	-1557557939
	.long	1072821012
	.long	1377377460
	.long	1073644513
	.long	97151407
	.long	1074650265
	.long	276415594
	.long	1075775455
	.long	-112980304
	.long	1076946691
	.long	1477955805
	.long	1017419233
	.long	573216928
	.long	1072048942
	.long	0
	.long	1073004544
	.long	212598996
	.long	1063659880
	.long	0
	.long	1071943680
	.long	-771506743
	.long	1073013176
	.long	-436336266
	.long	1072093934
	.long	1011152670
	.long	1072852867
	.long	-1260834927
	.long	1073722858
	.long	1437039048
	.long	1074748495
	.long	-1502972618
	.long	1075869983
	.long	2110676341
	.long	1077027880
	.long	-1773177077
	.long	1018030056
	.long	-578825940
	.long	1072059610
	.long	0
	.long	1073020928
	.long	-62769333
	.long	-1082243283
	.long	0
	.long	1071951872
	.long	-1886923455
	.long	1073019083
	.long	1987453124
	.long	1072122637
	.long	-1859719037
	.long	1072885976
	.long	1945143188
	.long	1073773491
	.long	-515796713
	.long	1074821611
	.long	-1458046946
	.long	1075937378
	.long	1212435950
	.long	1077116011
	.long	-200925688
	.long	1016101441
	.long	1328656240
	.long	1072070325
	.long	0
	.long	1073020928
	.long	-525665659
	.long	-1084436031
	.long	0
	.long	1071960064
	.long	-189632116
	.long	1073025103
	.long	1499830585
	.long	1072152131
	.long	1426254566
	.long	1072920404
	.long	1793109073
	.long	1073816742
	.long	861031703
	.long	1074877051
	.long	2143255085
	.long	1076009798
	.long	-1385076888
	.long	1077211764
	.long	2082492072
	.long	-1130602225
	.long	1468160620
	.long	1072081086
	.long	0
	.long	1073020928
	.long	-1301181439
	.long	1064325108
	.long	0
	.long	1071968256
	.long	680264061
	.long	1073031241
	.long	1047347071
	.long	1072182448
	.long	990332875
	.long	1072956218
	.long	-381242986
	.long	1073862226
	.long	1009749172
	.long	1074936011
	.long	1407525057
	.long	1076087680
	.long	1618723062
	.long	1077315896
	.long	-872073833
	.long	-1132189876
	.long	-589111368
	.long	1072091894
	.long	0
	.long	1073037312
	.long	1946059620
	.long	-1082673449
	.long	0
	.long	1071976448
	.long	1922819859
	.long	1073037498
	.long	2015316386
	.long	1072213621
	.long	1126875395
	.long	1072993490
	.long	-1694493258
	.long	1073910087
	.long	359363817
	.long	1074998759
	.long	-399102264
	.long	1076171504
	.long	-925703944
	.long	1077429246
	.long	-1153774509
	.long	-1131457061
	.long	-869099792
	.long	1072102751
	.long	0
	.long	1073037312
	.long	2095208030
	.long	1059540563
	.long	0
	.long	1071984640
	.long	1016635649
	.long	1073043879
	.long	381883965
	.long	1072245686
	.long	229819469
	.long	1073032297
	.long	256198328
	.long	1073960478
	.long	2131335810
	.long	1075065585
	.long	-2095177094
	.long	1076261801
	.long	-1607746347
	.long	1077552751
	.long	-1364604003
	.long	1017280936
	.long	419547972
	.long	1072113658
	.long	0
	.long	1073037312
	.long	-1734279036
	.long	1064937276
	.long	0
	.long	1071992832
	.long	338934205
	.long	1073050387
	.long	86239154
	.long	1072278679
	.long	908030867
	.long	1073072720
	.long	-1326184411
	.long	1074013563
	.long	1177051173
	.long	1075136807
	.long	2014614964
	.long	1076359153
	.long	168901105
	.long	1077687454
	.long	-1956637222
	.long	-1134744474
	.long	-1109925664
	.long	1072124614
	.long	0
	.long	1073053696
	.long	-1735621025
	.long	-1083581993
	.long	0
	.long	1072001024
	.long	-1392271865
	.long	1073057025
	.long	664351693
	.long	1072312639
	.long	-1237478490
	.long	1073114846
	.long	-667532262
	.long	1074069522
	.long	-230877908
	.long	1075212769
	.long	1708368349
	.long	1076464204
	.long	1589493561
	.long	1077834518
	.long	2057980310
	.long	-1129424020
	.long	-1132646496
	.long	1072135622
	.long	0
	.long	1073053696
	.long	121376390
	.long	1063912282
	.long	0
	.long	1072009216
	.long	-494031434
	.long	1073063798
	.long	2105270965
	.long	1072347607
	.long	1379190953
	.long	1073158769
	.long	-126659252
	.long	1074128547
	.long	-1041023077
	.long	1075293850
	.long	1125123182
	.long	1076577665
	.long	-162855961
	.long	1077965686
	.long	-1280637630
	.long	-1130105957
	.long	508088664
	.long	1072146683
	.long	0
	.long	1073070080
	.long	1917995466
	.long	-1082619619
	.long	0
	.long	1072017408
	.long	-1166864485
	.long	1073070710
	.long	1161897923
	.long	1072383627
	.long	243362523
	.long	1073204587
	.long	-304429001
	.long	1074190846
	.long	34202551
	.long	1075380462
	.long	1464376446
	.long	1076700322
	.long	2139164180
	.long	1078053610
	.long	620324699
	.long	-1132137970
	.long	-193812848
	.long	1072157796
	.long	0
	.long	1073070080
	.long	-1736649181
	.long	1061402067
	.long	0
	.long	1072025600
	.long	1724058844
	.long	1073077765
	.long	595168538
	.long	1072420744
	.long	-1327142952
	.long	1073252405
	.long	1850107402
	.long	1074256644
	.long	-905459035
	.long	1075473054
	.long	1106046820
	.long	1076833046
	.long	-1016121733
	.long	1078149909
	.long	1411749030
	.long	-1130328927
	.long	1483332072
	.long	1072168965
	.long	0
	.long	1073070080
	.long	-1022567463
	.long	1065223526
	.long	0
	.long	1072033792
	.long	1215691267
	.long	1073084967
	.long	1291414883
	.long	1072459006
	.long	1299940115
	.long	1073302338
	.long	-1582283374
	.long	1074326183
	.long	744136071
	.long	1075572123
	.long	466169704
	.long	1076932177
	.long	1751104868
	.long	1078255500
	.long	821977976
	.long	-1129576536
	.long	1815097404
	.long	1072180189
	.long	0
	.long	1073086464
	.long	672658444
	.long	-1084792098
	.long	0
	.long	1072041984
	.long	-233571114
	.long	1073092320
	.long	-1267235585
	.long	1072498464
	.long	-516135161
	.long	1073354505
	.long	-418697614
	.long	1074399727
	.long	-378098983
	.long	1075678208
	.long	-777118040
	.long	1077010106
	.long	1680094239
	.long	1078371411
	.long	-1503838031
	.long	1017200211
	.long	1522170344
	.long	1072191470
	.long	0
	.long	1073086464
	.long	335336980
	.long	1064755442
	.long	0
	.long	1072050176
	.long	713772760
	.long	1073099831
	.long	1006016789
	.long	1072539173
	.long	-76679057
	.long	1073409037
	.long	-890055093
	.long	1074477562
	.long	-1209869306
	.long	1075791906
	.long	-1322960925
	.long	1077094683
	.long	-1354829615
	.long	1078498800
	.long	1911025014
	.long	1018078600
	.long	1482063996
	.long	1072202809
	.long	0
	.long	1073102848
	.long	-379433188
	.long	-1083731542
	.long	0
	.long	1072058368
	.long	-264636957
	.long	1073107502
	.long	-363295148
	.long	1072581188
	.long	2007234724
	.long	1073466073
	.long	912913138
	.long	1074559998
	.long	451564708
	.long	1075876423
	.long	1803970905
	.long	1077186570
	.long	1644573903
	.long	1078638973
	.long	-627986179
	.long	1016366665
	.long	-1558785120
	.long	1072214207
	.long	0
	.long	1073102848
	.long	972415661
	.long	1064447728
	.long	0
	.long	1072066560
	.long	2083475395
	.long	1073115341
	.long	629395296
	.long	1072624572
	.long	1244534404
	.long	1073525761
	.long	-591791877
	.long	1074647370
	.long	1310081907
	.long	1075941897
	.long	-1883147844
	.long	1077286505
	.long	-1536459807
	.long	1078793402
	.long	-1142954406
	.long	-1129599951
	.long	-2092638068
	.long	1072225666
	.long	0
	.long	1073119232
	.long	-1587512713
	.long	-1083284217
	.long	0
	.long	1072074752
	.long	1168784517
	.long	1073123352
	.long	-242238184
	.long	1072669386
	.long	1628181518
	.long	1073588261
	.long	-259484191
	.long	1074740046
	.long	657248771
	.long	1076012260
	.long	1623498990
	.long	1077395313
	.long	-327600867
	.long	1078963753
	.long	1042479882
	.long	1013708219
	.long	1272563800
	.long	1072237187
	.long	0
	.long	1073119232
	.long	-1438874317
	.long	1064310853
	.long	0
	.long	1072082944
	.long	98119246
	.long	1073131541
	.long	-1698881320
	.long	1072704474
	.long	-1681535981
	.long	1073653745
	.long	-1800424773
	.long	1074814412
	.long	-1283279542
	.long	1076087948
	.long	432180910
	.long	1077513915
	.long	-1888089675
	.long	1079068308
	.long	-161400859
	.long	1016910371
	.long	1526625196
	.long	1072248771
	.long	0
	.long	1073135616
	.long	1302553396
	.long	-1083189772
	.long	0
	.long	1072091136
	.long	-1142821077
	.long	1073139913
	.long	-2097423063
	.long	1072728417
	.long	-590085140
	.long	1073722398
	.long	303890467
	.long	1074866670
	.long	-1287562830
	.long	1076169445
	.long	-614667580
	.long	1077643341
	.long	-1598868304
	.long	1079172361
	.long	-655275015
	.long	-1130971363
	.long	445869348
	.long	1072260420
	.long	0
	.long	1073135616
	.long	-504419602
	.long	1064356283
	.long	0
	.long	1072099328
	.long	-1321761073
	.long	1073148476
	.long	2071970100
	.long	1072753184
	.long	466670872
	.long	1073768122
	.long	362727749
	.long	1074922233
	.long	385844874
	.long	1076257285
	.long	-2026274796
	.long	1077784749
	.long	1205176081
	.long	1079287603
	.long	1420892712
	.long	1015907727
	.long	12654668
	.long	1072272135
	.long	0
	.long	1073152000
	.long	-1863163286
	.long	-1083472227
	.long	0
	.long	1072107520
	.long	2118473219
	.long	1073157236
	.long	979723615
	.long	1072778816
	.long	524986273
	.long	1073805924
	.long	-1186543862
	.long	1074981360
	.long	1019558198
	.long	1076352058
	.long	-301436295
	.long	1077937781
	.long	1999715567
	.long	1079415416
	.long	1999466082
	.long	-1129636959
	.long	-1869551500
	.long	1072283917
	.long	0
	.long	1073152000
	.long	1163264723
	.long	1064596606
	.long	0
	.long	1072115712
	.long	259472876
	.long	1073166200
	.long	310948419
	.long	1072805356
	.long	981811109
	.long	1073845633
	.long	1831289829
	.long	1075044336
	.long	162838820
	.long	1076454421
	.long	430284063
	.long	1078022494
	.long	-203081609
	.long	1079557377
	.long	-63665234
	.long	1016472169
	.long	1519405372
	.long	1072295769
	.long	0
	.long	1073168384
	.long	293873475
	.long	-1084157983
	.long	0
	.long	1072123904
	.long	-1431862976
	.long	1073175374
	.long	682923382
	.long	1072832850
	.long	1960208198
	.long	1073887374
	.long	-1540324440
	.long	1075111470
	.long	42491607
	.long	1076565102
	.long	698596396
	.long	1078115397
	.long	-844109851
	.long	1079715288
	.long	-264271244
	.long	1017677357
	.long	-42075176
	.long	1072307691
	.long	0
	.long	1073168384
	.long	-1484701766
	.long	1065045674
	.long	0
	.long	1072132096
	.long	-184811512
	.long	1073184767
	.long	-1377527324
	.long	1072861347
	.long	-14483951
	.long	1073931282
	.long	-677283517
	.long	1075183103
	.long	-1963484433
	.long	1076684911
	.long	1871634310
	.long	1078217418
	.long	701472658
	.long	1079891210
	.long	-981052877
	.long	1017421232
	.long	654491024
	.long	1072319687
	.long	0
	.long	1073184768
	.long	-259888927
	.long	-1100609537
	.long	0
	.long	1072140288
	.long	-113410116
	.long	1073194387
	.long	522795649
	.long	1072890901
	.long	828677796
	.long	1073977505
	.long	-311043554
	.long	1075259609
	.long	2065118922
	.long	1076814752
	.long	57592276
	.long	1078329606
	.long	-1276796769
	.long	1080060392
	.long	1317976194
	.long	-1129866307
	.long	-1804197632
	.long	1072331756
	.long	0
	.long	1073201152
	.long	-1031781284
	.long	-1082495994
	.long	0
	.long	1072148480
	.long	783607094
	.long	1073204243
	.long	-1949147779
	.long	1072921566
	.long	-1524762729
	.long	1074026199
	.long	272298814
	.long	1075341400
	.long	554834990
	.long	1076921592
	.long	-1652789789
	.long	1078453146
	.long	-280192494
	.long	1080170084
	.long	970683135
	.long	1015041325
	.long	330340784
	.long	1072343902
	.long	0
	.long	1073201152
	.long	1774873647
	.long	1063790173
	.long	0
	.long	1072156672
	.long	-2137284147
	.long	1073214342
	.long	-493309205
	.long	1072953403
	.long	1888119486
	.long	1074077538
	.long	152008619
	.long	1075428927
	.long	1813233464
	.long	1076998114
	.long	1711211176
	.long	1078589386
	.long	-628879392
	.long	1080292884
	.long	-1419961772
	.long	-1129857559
	.long	-2081696992
	.long	1072356125
	.long	0
	.long	1073217536
	.long	-927177374
	.long	-1083641090
	.long	0
	.long	1072164864
	.long	1445862948
	.long	1073224695
	.long	1184310491
	.long	1072986477
	.long	-2031716073
	.long	1074131708
	.long	794486146
	.long	1075522690
	.long	1710102809
	.long	1077081349
	.long	1195692576
	.long	1078739855
	.long	-698152451
	.long	1080430595
	.long	-58554220
	.long	1015312584
	.long	-692942884
	.long	1072368428
	.long	0
	.long	1073217536
	.long	773727205
	.long	1065088854
	.long	0
	.long	1072173056
	.long	-1687114979
	.long	1073235311
	.long	1828824692
	.long	1073020855
	.long	1121433191
	.long	1074188913
	.long	98386005
	.long	1075623241
	.long	-460272997
	.long	1077172003
	.long	1204115611
	.long	1078906135
	.long	1030598710
	.long	1080585286
	.long	-319669202
	.long	1082393677
	.long	219626079
	.long	-1132525168
	.long	283192636
	.long	1072380814
	.long	0
	.long	1073233920
	.long	-1028885700
	.long	1062583917
	.long	0
	.long	1072181248
	.long	-893813859
	.long	1073246201
	.long	78060354
	.long	1073056612
	.long	549518190
	.long	1074249374
	.long	-1690148201
	.long	1075731189
	.long	20725998
	.long	1077270872
	.long	571860108
	.long	1079037609
	.long	-1195350374
	.long	1080759403
	.long	-74534658
	.long	1082559638
	.long	533510868
	.long	1017469000
	.long	1270409880
	.long	1072393283
	.long	0
	.long	1073250304
	.long	1183081241
	.long	-1083177419
	.long	0
	.long	1072189440
	.long	-149657153
	.long	1073257376
	.long	678526714
	.long	1073093826
	.long	-1346913545
	.long	1074313332
	.long	-328038693
	.long	1075843093
	.long	-2094422935
	.long	1077378847
	.long	690483421
	.long	1079139913
	.long	1525157081
	.long	1080955740
	.long	1088382205
	.long	1082749796
	.long	517544707
	.long	1015674171
	.long	-1241089348
	.long	1072405838
	.long	0
	.long	1073250304
	.long	342474547
	.long	1065066743
	.long	0
	.long	1072197632
	.long	-677913921
	.long	1073268848
	.long	-566045345
	.long	1073132582
	.long	-1059035822
	.long	1074381052
	.long	-351954681
	.long	1075905516
	.long	1235704487
	.long	1077496938
	.long	-1297836336
	.long	1079253633
	.long	547141267
	.long	1081129710
	.long	676858053
	.long	1082968160
	.long	-1693595917
	.long	-1129419380
	.long	-1788952124
	.long	1072418482
	.long	0
	.long	1073266688
	.long	800423655
	.long	1063313839
	.long	0
	.long	1072205824
	.long	-742063372
	.long	1073280629
	.long	-1547717916
	.long	1073172973
	.long	-566655044
	.long	1074452822
	.long	-1291145974
	.long	1075972768
	.long	371326996
	.long	1077626283
	.long	-326876516
	.long	1079380265
	.long	225737017
	.long	1081255273
	.long	1409807390
	.long	1083199248
	.long	-64579230
	.long	-1134076693
	.long	1198723980
	.long	1072431217
	.long	0
	.long	1073283072
	.long	1979324240
	.long	-1084025768
	.long	0
	.long	1072214016
	.long	286336479
	.long	1073292733
	.long	181580994
	.long	1073215097
	.long	324641736
	.long	1074528959
	.long	-2004517855
	.long	1076045313
	.long	622188814
	.long	1077768171
	.long	-736029602
	.long	1079521530
	.long	-42429116
	.long	1081397718
	.long	1261497329
	.long	1083344225
	.long	2021063122
	.long	1017200251
	.long	1132420724
	.long	1072444045
	.long	0
	.long	1073299456
	.long	-287694684
	.long	-1082506514
	.long	0
	.long	1072222208
	.long	-2118563973
	.long	1073305172
	.long	1744336761
	.long	1073259059
	.long	342253216
	.long	1074609808
	.long	1514231623
	.long	1076123669
	.long	494211520
	.long	1077924065
	.long	32256251
	.long	1079679414
	.long	-578885727
	.long	1081559666
	.long	1546589988
	.long	1083511892
	.long	2001359365
	.long	1015121331
	.long	466922764
	.long	1072456969
	.long	0
	.long	1073299456
	.long	-1186497856
	.long	1064719489
	.long	0
	.long	1072230400
	.long	-184143691
	.long	1073317962
	.long	-1800185203
	.long	1073304975
	.long	-777835747
	.long	1074695750
	.long	183265499
	.long	1076208415
	.long	-48126119
	.long	1078015877
	.long	-177341274
	.long	1079856211
	.long	-123990177
	.long	1081744197
	.long	-919827213
	.long	1083706291
	.long	-21585826
	.long	-1132030232
	.long	2139273420
	.long	1072469991
	.long	0
	.long	1073315840
	.long	207710662
	.long	1063294442
	.long	0
	.long	1072238592
	.long	690733727
	.long	1073331120
	.long	-924592277
	.long	1073352969
	.long	-2005678976
	.long	1074787206
	.long	-564028297
	.long	1076300198
	.long	1519654841
	.long	1078110441
	.long	-1075705001
	.long	1080043933
	.long	-517843919
	.long	1081954947
	.long	-1037746068
	.long	1083932276
	.long	1962120923
	.long	-1131965391
	.long	1008965768
	.long	1072483115
	.long	0
	.long	1073332224
	.long	1358267295
	.long	-1085194405
	.long	0
	.long	1072246784
	.long	-554403779
	.long	1073344660
	.long	-1909455050
	.long	1073403176
	.long	-1294674706
	.long	1074837518
	.long	1060962391
	.long	1076399748
	.long	-771595328
	.long	1078214868
	.long	652298535
	.long	1080155458
	.long	1592868105
	.long	1082163325
	.long	-23161666
	.long	1084195692
	.long	-436363987
	.long	-1132381052
	.long	1070395652
	.long	1072496343
	.long	0
	.long	1073348608
	.long	386893131
	.long	-1083255230
	.long	0
	.long	1072254976
	.long	-277820935
	.long	1073358602
	.long	-786013479
	.long	1073455741
	.long	-398247079
	.long	1074889476
	.long	458086617
	.long	1076507881
	.long	1487549772
	.long	1078330399
	.long	-2014641317
	.long	1080281123
	.long	2139727542
	.long	1082301774
	.long	-1262561971
	.long	1084365594
	.long	-41778220
	.long	1015373158
	.long	-1691715964
	.long	1072509678
	.long	0
	.long	1073364992
	.long	-1892284743
	.long	-1082591984
	.long	0
	.long	1072263168
	.long	1676284543
	.long	1073372965
	.long	341885464
	.long	1073510824
	.long	1024759644
	.long	1074944960
	.long	575265638
	.long	1076625519
	.long	-318050355
	.long	1078458454
	.long	773859536
	.long	1080423039
	.long	1826985365
	.long	1082461077
	.long	415537122
	.long	1084546083
	.long	1991922890
	.long	1015799726
	.long	-2084174508
	.long	1072523124
	.long	0
	.long	1073364992
	.long	-367886509
	.long	1065297251
	.long	0
	.long	1072271360
	.long	-1872638210
	.long	1073387768
	.long	-895355802
	.long	1073568595
	.long	-1457114904
	.long	1075004281
	.long	1105425239
	.long	1076753703
	.long	-673307899
	.long	1078600671
	.long	420594959
	.long	1080583678
	.long	-707898307
	.long	1082644865
	.long	557097762
	.long	1084758318
	.long	1340529352
	.long	-1131324167
	.long	1451368308
	.long	1072536684
	.long	0
	.long	1073381376
	.long	1640955323
	.long	1064892560
	.long	0
	.long	1072279552
	.long	821091020
	.long	1073403034
	.long	-574624144
	.long	1073629244
	.long	-1002339239
	.long	1075067788
	.long	-421009386
	.long	1076890581
	.long	49160206
	.long	1078758938
	.long	-2096920061
	.long	1080765948
	.long	-645405735
	.long	1082857492
	.long	-2010657030
	.long	1085008670
	.long	-500499232
	.long	1017676442
	.long	-1706280496
	.long	1072550361
	.long	0
	.long	1073397760
	.long	-254096506
	.long	1064606256
	.long	0
	.long	1072287744
	.long	-1972968435
	.long	1073418785
	.long	1764606218
	.long	1073692976
	.long	1684150912
	.long	1075135868
	.long	-316007756
	.long	1076967066
	.long	-1140835728
	.long	1078935437
	.long	1177459158
	.long	1080973281
	.long	-985704681
	.long	1083104194
	.long	175236179
	.long	1085290557
	.long	-1092778327
	.long	1016115103
	.long	51181320
	.long	1072564160
	.long	0
	.long	1073414144
	.long	1726221502
	.long	1064444298
	.long	0
	.long	1072295936
	.long	-1902565403
	.long	1073435047
	.long	1477851059
	.long	1073750919
	.long	-172834434
	.long	1075208951
	.long	921354366
	.long	1077050843
	.long	-955409341
	.long	1079058704
	.long	-2000344765
	.long	1081145796
	.long	-1314174904
	.long	1083285150
	.long	-1309903211
	.long	1085466475
	.long	1587507161
	.long	1017783296
	.long	1960655972
	.long	1072578083
	.long	0
	.long	1073430528
	.long	-1725438607
	.long	1064413070
	.long	0
	.long	1072304128
	.long	81852767
	.long	1073451847
	.long	-1399995781
	.long	1073786214
	.long	-1375361664
	.long	1075287521
	.long	697999520
	.long	1077142778
	.long	-1556433060
	.long	1079169197
	.long	970345609
	.long	1081280997
	.long	357315780
	.long	1083452729
	.long	1506892573
	.long	1085676121
	.long	290186597
	.long	-1129330535
	.long	126352688
	.long	1072592136
	.long	0
	.long	1073446912
	.long	-520528245
	.long	1064519428
	.long	0
	.long	1072312320
	.long	-1218496607
	.long	1073469212
	.long	-57669919
	.long	1073823420
	.long	-997381111
	.long	1075372117
	.long	-1027028531
	.long	1077243863
	.long	1637398897
	.long	1079293271
	.long	-1769932369
	.long	1081436044
	.long	-1963374883
	.long	1083648997
	.long	-508218887
	.long	1085926884
	.long	-464358076
	.long	-1136361623
	.long	174173468
	.long	1072606322
	.long	0
	.long	1073463296
	.long	1597481183
	.long	1064770743
	.long	0
	.long	1072320512
	.long	-1515040896
	.long	1073487175
	.long	2126116745
	.long	1073862686
	.long	-549275977
	.long	1075463346
	.long	-1267308718
	.long	1077355237
	.long	-319102992
	.long	1079432942
	.long	-1960920875
	.long	1081614376
	.long	-897318588
	.long	1083879650
	.long	-502109249
	.long	1086227988
	.long	751183074
	.long	-1131570541
	.long	142576928
	.long	1072620646
	.long	0
	.long	1073479680
	.long	-1303412636
	.long	1065174949
	.long	0
	.long	1072328704
	.long	1038713351
	.long	1073505769
	.long	-2128649189
	.long	1073904174
	.long	-232577427
	.long	1075561891
	.long	-1494434263
	.long	1077478208
	.long	-1009477201
	.long	1079590582
	.long	-1383283983
	.long	1081820121
	.long	-1525041130
	.long	1084151676
	.long	1530622011
	.long	1086457866
	.long	-376192035
	.long	1014700157
	.long	-849707800
	.long	1072635112
	.long	0
	.long	1073512448
	.long	377354551
	.long	-1082517822
	.long	0
	.long	1072336896
	.long	-1667043234
	.long	1073525029
	.long	1425820821
	.long	1073948065
	.long	-641335435
	.long	1075668523
	.long	-1902946435
	.long	1077614286
	.long	-521993570
	.long	1079768986
	.long	79486865
	.long	1082058258
	.long	837805041
	.long	1084350636
	.long	-2048981732
	.long	1086677607
	.long	755744878
	.long	-1129488094
	.long	1776988912
	.long	1072649727
	.long	0
	.long	1073528832
	.long	-1172356124
	.long	-1083329338
	.long	0
	.long	1072345088
	.long	-714367147
	.long	1073544995
	.long	-2045470149
	.long	1073994558
	.long	193793453
	.long	1075784115
	.long	1089065383
	.long	1077765219
	.long	1637705337
	.long	1079971465
	.long	-1831911492
	.long	1082232622
	.long	-2075242051
	.long	1084541968
	.long	-206967968
	.long	1086944813
	.long	-280289790
	.long	1017253497
	.long	978042908
	.long	1072664495
	.long	0
	.long	1073545216
	.long	-1944755640
	.long	-1087666862
	.long	0
	.long	1072353280
	.long	1152046322
	.long	1073565710
	.long	1803934568
	.long	1074043875
	.long	-447155375
	.long	1075874315
	.long	-1044593102
	.long	1077933036
	.long	710237765
	.long	1080117616
	.long	-586842317
	.long	1082393774
	.long	576784920
	.long	1084770265
	.long	1014670004
	.long	1087271238
	.long	1405752556
	.long	-1131056688
	.long	-335895736
	.long	1072679421
	.long	0
	.long	1073561600
	.long	-1428884889
	.long	1064308292
	.long	0
	.long	1072361472
	.long	-325133222
	.long	1073587218
	.long	582408456
	.long	1074096262
	.long	-1241140300
	.long	1075942624
	.long	-1825024500
	.long	1078028116
	.long	884040041
	.long	1080249211
	.long	991670172
	.long	1082582280
	.long	-73532427
	.long	1085043829
	.long	-189482239
	.long	1087522622
	.long	-370118916
	.long	1017654065
	.long	-1033814270
	.long	1072693880
	.long	0
	.long	1073594368
	.long	1629726161
	.long	-1082397421
	.long	0
	.long	1072369664
	.long	-549789477
	.long	1073609571
	.long	145569999
	.long	1074151993
	.long	-1441372813
	.long	1076017115
	.long	941690174
	.long	1078132660
	.long	-77380522
	.long	1080399970
	.long	-292602316
	.long	1082803636
	.long	-744854417
	.long	1085324633
	.long	726165480
	.long	1087769805
	.long	482051507
	.long	-1129969281
	.long	892856320
	.long	1072701512
	.long	0
	.long	1073610752
	.long	343708653
	.long	-1085116285
	.long	0
	.long	1072377856
	.long	-1223729294
	.long	1073632823
	.long	-896315134
	.long	1074211374
	.long	1129681432
	.long	1076098526
	.long	-1260282717
	.long	1078249838
	.long	309517731
	.long	1080573282
	.long	-960517395
	.long	1083064629
	.long	161460809
	.long	1085523735
	.long	-718712963
	.long	1088076397
	.long	-920651231
	.long	1017415857
	.long	-1246557728
	.long	1072709232
	.long	0
	.long	1073627136
	.long	257913406
	.long	1064712119
	.long	0
	.long	1072386048
	.long	567588930
	.long	1073657034
	.long	736137157
	.long	1074274751
	.long	1785347024
	.long	1076187704
	.long	-1161230366
	.long	1078381568
	.long	-1809832872
	.long	1080773240
	.long	-33137498
	.long	1083276341
	.long	-388646843
	.long	1085765704
	.long	215428470
	.long	1088440352
	.long	-1665249141
	.long	1017465710
	.long	-467916082
	.long	1072717045
	.long	0
	.long	1073659904
	.long	1452243992
	.long	-1083806788
	.long	0
	.long	1072394240
	.long	2142694182
	.long	1073682268
	.long	-1480942539
	.long	1074342508
	.long	-2131379553
	.long	1076285628
	.long	868054267
	.long	1078530120
	.long	1643962911
	.long	1081004825
	.long	1625250541
	.long	1083460142
	.long	449409397
	.long	1086061308
	.long	-1012351966
	.long	1088680261
	.long	-1492499751
	.long	-1130804387
	.long	-1623881888
	.long	1072724955
	.long	0
	.long	1073676288
	.long	-1226103422
	.long	1064787071
	.long	0
	.long	1072402432
	.long	-1691793108
	.long	1073708598
	.long	473279882
	.long	1074415083
	.long	-935904335
	.long	1076393431
	.long	-2007078170
	.long	1078698193
	.long	137454366
	.long	1081177988
	.long	2136070776
	.long	1083679809
	.long	1836466939
	.long	1086374578
	.long	1497981435
	.long	1088983155
	.long	-1764900479
	.long	-1129980210
	.long	172373838
	.long	1072732966
	.long	0
	.long	1073709056
	.long	1802321157
	.long	-1086548403
	.long	0
	.long	1072410624
	.long	2111418831
	.long	1073736103
	.long	-1179748433
	.long	1074492967
	.long	859320429
	.long	1076512432
	.long	-1597330199
	.long	1078889016
	.long	-2041621598
	.long	1081335227
	.long	-691437683
	.long	1083943664
	.long	-542221672
	.long	1086598895
	.long	-510328358
	.long	1089368090
	.long	-942418315
	.long	1016906996
	.long	-1813231186
	.long	1072741081
	.long	0
	.long	1073741824
	.long	642658573
	.long	-1082763134
	.long	0
	.long	1072418816
	.long	875573051
	.long	1073753347
	.long	2136649485
	.long	1074576722
	.long	207838218
	.long	1076644169
	.long	-1229252814
	.long	1079045585
	.long	-1768838668
	.long	1081519677
	.long	1632120774
	.long	1084244935
	.long	-132204832
	.long	1086877751
	.long	-397417122
	.long	1089665584
	.long	579847197
	.long	-1130880783
	.long	-36393368
	.long	1072749306
	.long	0
	.long	1073750016
	.long	1616803504
	.long	1064961640
	.long	0
	.long	1072427008
	.long	-104249437
	.long	1073768409
	.long	-1586354046
	.long	1074666985
	.long	1107165767
	.long	1076790448
	.long	-607005066
	.long	1079169966
	.long	-527909131
	.long	1081737080
	.long	1691985028
	.long	1084438400
	.long	427096501
	.long	1087226669
	.long	-836336111
	.long	1089983125
	.long	-734539201
	.long	-1129995886
	.long	1702821322
	.long	1072757647
	.long	0
	.long	1073766400
	.long	622759155
	.long	1064265703
	.long	0
	.long	1072435200
	.long	-1308624275
	.long	1073784205
	.long	1316161475
	.long	1074764487
	.long	1587632482
	.long	1076920475
	.long	1410916790
	.long	1079312848
	.long	-1267279969
	.long	1081994624
	.long	938687839
	.long	1084674751
	.long	-1539347345
	.long	1087519793
	.long	-619201622
	.long	1090395718
	.long	1104719190
	.long	-1129845918
	.long	1312480778
	.long	1072766108
	.long	0
	.long	1073782784
	.long	-1461161
	.long	1063663303
	.long	0
	.long	1072443392
	.long	488365254
	.long	1073800794
	.long	889441486
	.long	1074830233
	.long	1971383332
	.long	1077011547
	.long	402907510
	.long	1079477706
	.long	-2074564739
	.long	1082215899
	.long	1813076663
	.long	1084965337
	.long	-675486332
	.long	1087798768
	.long	-179800506
	.long	1090727669
	.long	-211611052
	.long	-1130610624
	.long	-1399435864
	.long	1072774695
	.long	0
	.long	1073799168
	.long	1869813616
	.long	1063872628
	.long	0
	.long	1072451584
	.long	110083252
	.long	1073818242
	.long	2145950303
	.long	1074887545
	.long	-746637161
	.long	1077113718
	.long	-549763166
	.long	1079668814
	.long	-351189943
	.long	1082399619
	.long	-1926090464
	.long	1085300597
	.long	-1964614179
	.long	1088155628
	.long	-780057791
	.long	1091084962
	.long	-81400714
	.long	1017374781
	.long	-209715494
	.long	1072783415
	.long	0
	.long	1073815552
	.long	528050055
	.long	1064633357
	.long	0
	.long	1072459776
	.long	1603229515
	.long	1073836624
	.long	-1251587779
	.long	1074949941
	.long	-1793561027
	.long	1077228789
	.long	-1803723099
	.long	1079891467
	.long	1452039539
	.long	1082621047
	.long	-1570610704
	.long	1085524838
	.long	-1087235763
	.long	1088518918
	.long	290000450
	.long	1091561761
	.long	1400294241
	.long	-1130150872
	.long	594535188
	.long	1072792276
	.long	0
	.long	1073840128
	.long	-514758231
	.long	-1082433728
	.long	0
	.long	1072467968
	.long	-1850423080
	.long	1073856025
	.long	-1100367130
	.long	1075018075
	.long	-848327934
	.long	1077358929
	.long	-664110081
	.long	1080092772
	.long	1365203926
	.long	1082889676
	.long	-797920095
	.long	1085806604
	.long	-911061632
	.long	1088818476
	.long	-1439093585
	.long	1091886070
	.long	-175879083
	.long	-1129800752
	.long	-604310986
	.long	1072801283
	.long	0
	.long	1073856512
	.long	-1284340228
	.long	-1085380892
	.long	0
	.long	1072476160
	.long	391640753
	.long	1073876541
	.long	-1366751680
	.long	1075092713
	.long	-703115424
	.long	1077506772
	.long	-1937580142
	.long	1080246393
	.long	-1398057623
	.long	1083198438
	.long	-1285363878
	.long	1086163609
	.long	949187804
	.long	1089212119
	.long	-899770797
	.long	1092324063
	.long	1243496009
	.long	-1132409913
	.long	1884249382
	.long	1072810447
	.long	0
	.long	1073872896
	.long	-1343397214
	.long	1065122350
	.long	0
	.long	1072484352
	.long	1798542762
	.long	1073898279
	.long	872092775
	.long	1075174758
	.long	-588037252
	.long	1077675536
	.long	-908606342
	.long	1080428494
	.long	-1557078948
	.long	1083400443
	.long	950212939
	.long	1086472372
	.long	-1163483015
	.long	1089602615
	.long	81195420
	.long	1092772102
	.long	367338796
	.long	1017892085
	.long	-13277158
	.long	1072819775
	.long	0
	.long	1073897472
	.long	-1666364183
	.long	1062812505
	.long	0
	.long	1072492544
	.long	1716159312
	.long	1073921364
	.long	-1806593888
	.long	1075265279
	.long	-942910940
	.long	1077869184
	.long	-1989208488
	.long	1080645802
	.long	1339379204
	.long	1083651152
	.long	1289096266
	.long	1086766933
	.long	1056952444
	.long	1089953470
	.long	-1641125503
	.long	1093193788
	.long	-928741121
	.long	-1130857234
	.long	-1889963258
	.long	1072829279
	.long	0
	.long	1073922048
	.long	-1411021826
	.long	-1084924723
	.long	0
	.long	1072500736
	.long	708897372
	.long	1073945938
	.long	1387566205
	.long	1075365555
	.long	1560351854
	.long	1078014382
	.long	23324444
	.long	1080907003
	.long	-281095845
	.long	1083965066
	.long	-1556452608
	.long	1087151144
	.long	341238182
	.long	1090430243
	.long	-58224421
	.long	1093727759
	.long	1873529409
	.long	1017909327
	.long	819493992
	.long	1072838969
	.long	0
	.long	1073946624
	.long	-122872583
	.long	-1084920147
	.long	0
	.long	1072508928
	.long	-1068251613
	.long	1073972164
	.long	1792802604
	.long	1075477121
	.long	-501636530
	.long	1078144091
	.long	821344472
	.long	1081152644
	.long	-1135001026
	.long	1084294745
	.long	1028881125
	.long	1087515657
	.long	-1531455184
	.long	1090802850
	.long	-1051593789
	.long	1094156625
	.long	2006882641
	.long	-1130171866
	.long	625885084
	.long	1072848857
	.long	0
	.long	1073971200
	.long	-1640949109
	.long	1063134722
	.long	0
	.long	1072517120
	.long	-1560911988
	.long	1074000234
	.long	918565006
	.long	1075601839
	.long	1396178813
	.long	1078295682
	.long	2142784425
	.long	1081345955
	.long	1282727591
	.long	1084548214
	.long	-551804636
	.long	1087854136
	.long	-1571459436
	.long	1091261216
	.long	2104011710
	.long	1094748109
	.long	1940421305
	.long	-1129523828
	.long	268532486
	.long	1072858957
	.long	0
	.long	1074003968
	.long	323020873
	.long	-1082316102
	.long	0
	.long	1072525312
	.long	2074716176
	.long	1074030370
	.long	-876132555
	.long	1075741983
	.long	-209217147
	.long	1078474134
	.long	-852918528
	.long	1081584363
	.long	828809040
	.long	1084875720
	.long	-1211386268
	.long	1088312352
	.long	-1219428192
	.long	1091739515
	.long	1372661695
	.long	1095213505
	.long	1188677519
	.long	1017930878
	.long	810115758
	.long	1072869284
	.long	0
	.long	1074028544
	.long	-1499446839
	.long	1064077806
	.long	0
	.long	1072533504
	.long	2056890421
	.long	1074062834
	.long	363864877
	.long	1075869669
	.long	-297367450
	.long	1078685889
	.long	1134576067
	.long	1081881418
	.long	1406320996
	.long	1085290193
	.long	281865662
	.long	1088681905
	.long	-154999456
	.long	1092206944
	.long	1830933007
	.long	1095840221
	.long	784664422
	.long	1099400835
	.long	-787263796
	.long	-1129753194
	.long	-1460359128
	.long	1072879855
	.long	0
	.long	1074061312
	.long	1721815751
	.long	1063766506
	.long	0
	.long	1072541696
	.long	-701023344
	.long	1074097937
	.long	-1015775757
	.long	1075959724
	.long	1778470255
	.long	1078939379
	.long	1348224331
	.long	1082193092
	.long	558462405
	.long	1085574422
	.long	-176424250
	.long	1089121533
	.long	1378199277
	.long	1092756021
	.long	-1397069909
	.long	1096386115
	.long	-564323911
	.long	1100113849
	.long	1120478265
	.long	-1129384388
	.long	-664015498
	.long	1072890690
	.long	0
	.long	1074094080
	.long	1853300552
	.long	1065231276
	.long	0
	.long	1072549888
	.long	1026395377
	.long	1074136053
	.long	-280358662
	.long	1076062852
	.long	1319265711
	.long	1079115251
	.long	-649791946
	.long	1082431925
	.long	938949548
	.long	1085957304
	.long	630925308
	.long	1089608607
	.long	643652511
	.long	1093273193
	.long	1036519138
	.long	1097031054
	.long	917342976
	.long	1100842583
	.long	1073536163
	.long	1017433835
	.long	-962777840
	.long	1072901811
	.long	0
	.long	1074135040
	.long	1818723877
	.long	1063234025
	.long	0
	.long	1072558080
	.long	1607334165
	.long	1074177631
	.long	1748114394
	.long	1076181870
	.long	1886293417
	.long	1079302473
	.long	1248907991
	.long	1082740975
	.long	521527630
	.long	1086403353
	.long	-2073388075
	.long	1090062257
	.long	398692374
	.long	1093866396
	.long	-1132007141
	.long	1097735587
	.long	-638030021
	.long	1101551999
	.long	2098024848
	.long	1009964280
	.long	-1347910828
	.long	1072913243
	.long	0
	.long	1074176000
	.long	937711101
	.long	1063878015
	.long	0
	.long	1072566272
	.long	1104272377
	.long	1074223223
	.long	-175824508
	.long	1076320424
	.long	1707144295
	.long	1079534062
	.long	-757525898
	.long	1083147200
	.long	1139555086
	.long	1086769788
	.long	1904419528
	.long	1090627362
	.long	-1791764474
	.long	1094493382
	.long	-376859261
	.long	1098388151
	.long	-2047964282
	.long	1102362741
	.long	-777064751
	.long	-1130766597
	.long	-1257224338
	.long	1072925015
	.long	0
	.long	1074225152
	.long	-1198515207
	.long	-1083301128
	.long	0
	.long	1072574464
	.long	-507281833
	.long	1074273510
	.long	-740517616
	.long	1076483315
	.long	-485928859
	.long	1079824500
	.long	-977042908
	.long	1083434852
	.long	1018044209
	.long	1087292846
	.long	-1067163848
	.long	1091140176
	.long	-660082082
	.long	1095112806
	.long	-1943075677
	.long	1099160309
	.long	-1458291681
	.long	1103250702
	.long	-1613130308
	.long	1014834622
	.long	-1017302042
	.long	1072937161
	.long	0
	.long	1074274304
	.long	-468891168
	.long	-1084700431
	.long	0
	.long	1072582656
	.long	676741307
	.long	1074329350
	.long	992705745
	.long	1076676971
	.long	-986442968
	.long	1080113887
	.long	384379049
	.long	1083805837
	.long	2092993084
	.long	1087715718
	.long	836582981
	.long	1091755973
	.long	-492750969
	.long	1095865497
	.long	-112974675
	.long	1100020035
	.long	-842184258
	.long	1104201404
	.long	1833005085
	.long	-1130486696
	.long	-1064780504
	.long	1072949721
	.long	0
	.long	1074331648
	.long	1400801792
	.long	-1083051089
	.long	0
	.long	1072590848
	.long	1700975072
	.long	1074391832
	.long	1969558051
	.long	1076898865
	.long	862863815
	.long	1080353841
	.long	-1603937687
	.long	1084275878
	.long	-1854213289
	.long	1088291661
	.long	1097399055
	.long	1092408001
	.long	364607821
	.long	1096613558
	.long	1862260197
	.long	1100889825
	.long	449531292
	.long	1105208551
	.long	-650177217
	.long	-1130616376
	.long	562292752
	.long	1072962743
	.long	0
	.long	1074388992
	.long	-979124168
	.long	1064710346
	.long	0
	.long	1072599040
	.long	-1553720977
	.long	1074462374
	.long	186229483
	.long	1077041395
	.long	622898293
	.long	1080671653
	.long	-1889014294
	.long	1084647868
	.long	-1071199752
	.long	1088804748
	.long	507197122
	.long	1093061812
	.long	-1085945851
	.long	1097395684
	.long	-1966563722
	.long	1101803370
	.long	1286204245
	.long	1106274142
	.long	219407327
	.long	1110665921
	.long	-220294234
	.long	1016798580
	.long	358593070
	.long	1072976283
	.long	0
	.long	1074462720
	.long	-1100411289
	.long	-1085958711
	.long	0
	.long	1072607232
	.long	1272037477
	.long	1074542856
	.long	650354685
	.long	1077218696
	.long	1775495070
	.long	1081092459
	.long	-671159715
	.long	1085198969
	.long	-44313273
	.long	1089499816
	.long	1242086200
	.long	1093848505
	.long	1399581958
	.long	1098286210
	.long	789637261
	.long	1102832127
	.long	-1411662420
	.long	1107413350
	.long	48639450
	.long	1111953425
	.long	1049656945
	.long	-1130330787
	.long	-1884866272
	.long	1072990411
	.long	0
	.long	1074544640
	.long	-1191285281
	.long	-1083449648
	.long	0
	.long	1072615424
	.long	313194921
	.long	1074635833
	.long	1660047625
	.long	1077443910
	.long	1058224703
	.long	1081393977
	.long	1713387257
	.long	1085661507
	.long	1950775345
	.long	1090113284
	.long	1466724264
	.long	1094733328
	.long	-11563384
	.long	1099273353
	.long	-1270259031
	.long	1103988946
	.long	-1750955789
	.long	1108662328
	.long	1828141235
	.long	1113448715
	.long	1601332172
	.long	-1131503961
	.long	-1434947786
	.long	1073005215
	.long	0
	.long	1074634752
	.long	-1410948435
	.long	1063314506
	.long	0
	.long	1072623616
	.long	-1050036945
	.long	1074744885
	.long	703097244
	.long	1077737331
	.long	-65529642
	.long	1081830711
	.long	327579914
	.long	1086334549
	.long	1341627678
	.long	1090865695
	.long	2108696154
	.long	1095635018
	.long	-439873205
	.long	1100364857
	.long	-181762085
	.long	1105264982
	.long	782138046
	.long	1110105462
	.long	1858605921
	.long	1114998443
	.long	-476713306
	.long	1016288912
	.long	777610658
	.long	1073020806
	.long	0
	.long	1074741248
	.long	-748003728
	.long	1065118594
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC1:
	.long	0
	.long	1048576
	.align 8
.LC2:
	.long	256195698
	.long	1066570456
	.align 8
.LC3:
	.long	3357725721
	.long	1066853442
	.align 8
.LC4:
	.long	83144089
	.long	1067392126
	.align 8
.LC5:
	.long	2923610340
	.long	1067899757
	.align 8
.LC6:
	.long	859181693
	.long	1068708659
	.align 8
.LC7:
	.long	1431655673
	.long	1069897045
	.align 8
.LC8:
	.long	1717986918
	.long	1072719462
	.align 8
.LC9:
	.long	0
	.long	1110966272
	.align 8
.LC10:
	.long	2085800548
	.long	1066279031
	.align 8
.LC11:
	.long	1008610459
	.long	1066516456
	.align 8
.LC12:
	.long	1087100500
	.long	1066854603
	.align 8
.LC13:
	.long	2801635634
	.long	1067392113
	.align 8
.LC14:
	.long	3068908137
	.long	1067899757
	.align 8
.LC15:
	.long	858992408
	.long	1068708659
	.align 8
.LC16:
	.long	0
	.long	1071644672
	.align 8
.LC17:
	.long	3438722616
	.long	1072693394
	.section	.rodata.cst16
	.align 16
.LC19:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC20:
	.long	3435973837
	.long	1072745676
	.align 8
.LC21:
	.long	1236950581
	.long	1072693772
	.align 8
.LC22:
	.long	2576980378
	.long	1072798105
	.align 8
.LC23:
	.long	921844752
	.long	978570906
	.align 8
.LC24:
	.long	921844752
	.long	-1168912742
	.align 8
.LC25:
	.long	3264175145
	.long	1072703733
	.align 8
.LC26:
	.long	3697107848
	.long	1072694086
	.align 8
.LC27:
	.long	13743895
	.long	1072693982
	.align 8
.LC28:
	.long	0
	.long	1072693248
	.align 8
.LC29:
	.long	416406201
	.long	1070858339
	.align 8
.LC30:
	.long	1734986010
	.long	1071120457
	.align 8
.LC31:
	.long	4285887236
	.long	1071644671
	.align 8
.LC32:
	.long	4293706205
	.long	1072693247
	.align 8
.LC33:
	.long	0
	.long	1073217536
	.align 8
.LC34:
	.long	0
	.long	1097859072
	.align 8
.LC35:
	.long	3126736191
	.long	1072696393
	.align 8
.LC36:
	.long	171798692
	.long	1072735191
	.align 8
.LC37:
	.long	2233382994
	.long	1072714219
	.align 8
.LC38:
	.long	1848553924
	.long	1072693667
	.align 8
.LC39:
	.long	3071760610
	.long	1072693457
	.align 8
.LC40:
	.long	1903529506
	.long	1072696603
	.align 8
.LC41:
	.long	3779571220
	.long	1072698490
	.align 8
.LC42:
	.long	1305670058
	.long	1072697442
	.align 8
.LC43:
	.long	4050325959
	.long	1072693289
	.align 8
.LC44:
	.long	0
	.long	1101004800
	.align 8
.LC45:
	.long	652835029
	.long	1072695345
	.align 8
.LC46:
	.long	1202590843
	.long	1072724705
