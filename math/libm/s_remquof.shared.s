	.text
	.p2align 4,,15
	.globl	__remquof
	.type	__remquof, @function
__remquof:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movd	%xmm1, %ebp
	pushq	%rbx
	movaps	%xmm1, %xmm2
	subq	$24, %rsp
	andl	$2147483647, %ebp
	je	.L26
	movd	%xmm0, %r13d
	movd	%xmm0, %r12d
	andl	$2147483647, %r13d
	cmpl	$2139095039, %r13d
	jg	.L26
	cmpl	$2139095040, %ebp
	jg	.L26
	cmpl	$2113929215, %ebp
	movd	%xmm1, %ebx
	jle	.L38
.L6:
	xorl	%r12d, %ebx
	andl	$-2147483648, %ebx
	cmpl	%r13d, %ebp
	je	.L39
	movss	.LC2(%rip), %xmm1
	cmpl	$2122317823, %ebp
	andps	%xmm1, %xmm0
	andps	%xmm1, %xmm2
	jg	.L9
	movss	.LC3(%rip), %xmm1
	xorl	%eax, %eax
	mulss	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.L10
	subss	%xmm1, %xmm0
	movl	$4, %eax
.L10:
	movaps	%xmm2, %xmm1
	addss	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm0
	jnb	.L40
.L13:
	cmpl	$16777215, %ebp
	jg	.L12
	movaps	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	ucomiss	%xmm2, %xmm1
	ja	.L41
.L15:
	pxor	%xmm3, %xmm3
	movl	%eax, %edx
	negl	%edx
	testl	%ebx, %ebx
	cmovne	%edx, %eax
	ucomiss	%xmm3, %xmm0
	movl	%eax, (%rdi)
	jp	.L21
	movd	%xmm0, %eax
	movd	%xmm3, %ecx
	cmove	%ecx, %eax
	movl	%eax, 4(%rsp)
	movss	4(%rsp), %xmm0
.L21:
	testl	%r12d, %r12d
	jns	.L1
	xorps	.LC5(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	mulss	%xmm2, %xmm0
	divss	%xmm0, %xmm0
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$1, %ebx
	mulss	.LC0(%rip), %xmm0
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	movl	%eax, (%rdi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	movss	%xmm1, 4(%rsp)
	mulss	.LC1(%rip), %xmm1
	movq	%rdi, 8(%rsp)
	call	__ieee754_fmodf@PLT
	movq	8(%rsp), %rdi
	movss	4(%rsp), %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	cmpl	$2130706431, %ebp
	jle	.L10
.L12:
	movss	.LC4(%rip), %xmm1
	mulss	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.L15
	subss	%xmm2, %xmm0
	ucomiss	%xmm1, %xmm0
	jnb	.L19
.L36:
	addl	$1, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L41:
	subss	%xmm2, %xmm0
	movaps	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	ucomiss	%xmm2, %xmm1
	jb	.L36
.L19:
	subss	%xmm2, %xmm0
	addl	$2, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L40:
	subss	%xmm1, %xmm0
	addl	$2, %eax
	jmp	.L13
	.size	__remquof, .-__remquof
	.weak	remquof32
	.set	remquof32,__remquof
	.weak	remquof
	.set	remquof,__remquof
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	1090519040
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	1082130432
	.align 4
.LC4:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
