 .section .rodata.cst8,"aM",@progbits,8
 .p2align 3
 .type one,@object
one: .double 1.0
 .size one,.-one;
 .type limit,@object
limit: .double 0.29
 .size limit,.-limit;
 .text
.globl __ieee754_log2l
.type __ieee754_log2l,@function
.align 1<<4
__ieee754_log2l:
 fldl one(%rip)
 fldt 8(%rsp)
 fxam
 fnstsw
 fld %st
 testb $1, %ah
 jnz 3f
4: fsub %st(2), %st
 fld %st
 fabs
 fcompl limit(%rip)
 fnstsw
 andb $0x45, %ah
 jz 2f
 fxam
 fnstsw
 andb $0x45, %ah
 cmpb $0x40, %ah
 jne 5f
 fabs
5: fstp %st(1)
 fyl2xp1
 ret
2: fstp %st(0)
 fyl2x
 ret
3: testb $4, %ah
 jnz 4b
 fstp %st(1)
 fstp %st(1)
 fadd %st(0)
 ret
.size __ieee754_log2l,.-__ieee754_log2l
.globl __log2l_finite
.type __log2l_finite,@function
.align 1<<4
__log2l_finite:
 fldl one(%rip)
 fldt 8(%rsp)
 fld %st
 fsub %st(2), %st
 fld %st
 fabs
 fcompl limit(%rip)
 fnstsw
 andb $0x45, %ah
 jz 2b
 fxam
 fnstsw
 andb $0x45, %ah
 cmpb $0x40, %ah
 jne 6f
 fabs
6: fstp %st(1)
 fyl2xp1
 ret
.size __log2l_finite,.-__log2l_finite
