	.text
	.p2align 4,,15
	.globl	__erfl
	.type	__erfl, @function
__erfl:
	pushq	%rbx
	subq	$96, %rsp
	movq	120(%rsp), %rbx
	movl	%ebx, %edx
	andw	$32767, %dx
	cmpw	$32767, %dx
	je	.L18
	movswl	%dx, %eax
	sall	$16, %eax
	movl	%eax, %edx
	movq	112(%rsp), %rax
	shrq	$48, %rax
	orl	%edx, %eax
	cmpl	$1073666047, %eax
	ja	.L4
	cmpl	$1071546367, %eax
	ja	.L5
	cmpl	$524287, %eax
	jg	.L6
	fldt	112(%rsp)
	fmuls	.LC1(%rip)
	fldt	.LC2(%rip)
	fldt	112(%rsp)
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	fmuls	.LC3(%rip)
	fld	%st(0)
	fabs
	fldt	.LC4(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L1
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1073717247, %eax
	ja	.L8
	fldt	112(%rsp)
	testw	%bx, %bx
	fabs
	fld1
	fsubrp	%st, %st(1)
	fldt	.LC18(%rip)
	fmul	%st(1), %st
	fldt	.LC19(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC20(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC21(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC22(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC23(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC24(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC25(%rip)
	fsubrp	%st, %st(1)
	fldt	.LC26(%rip)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC27(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC28(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC29(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC30(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC31(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(2)
	fldt	.LC32(%rip)
	faddp	%st, %st(2)
	fdivp	%st, %st(1)
	js	.L9
	fadds	.LC33(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	shrl	$14, %ebx
	movl	$1, %eax
	andl	$2, %ebx
	subl	%ebx, %eax
	movl	%eax, (%rsp)
	fildl	(%rsp)
	fld1
	fldt	112(%rsp)
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
.L1:
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1073861972, %eax
	jle	.L10
	testw	%bx, %bx
	js	.L11
	fld1
	fldt	.LC35(%rip)
	fsubrp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	fldt	112(%rsp)
	fld	%st(0)
	fmul	%st(0), %st
	fldt	.LC6(%rip)
	fmul	%st(1), %st
	fldt	.LC7(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC9(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC11(%rip)
	faddp	%st, %st(1)
	fldt	.LC12(%rip)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC13(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC14(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC15(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC16(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(2)
	fldt	.LC17(%rip)
	addq	$96, %rsp
	popq	%rbx
	faddp	%st, %st(2)
	fdivp	%st, %st(1)
	fmul	%st(1), %st
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	fldt	112(%rsp)
	cmpl	$1073788634, %eax
	fabs
	fstpt	(%rsp)
	fldt	112(%rsp)
	fmul	%st(0), %st
	fld1
	fdivp	%st, %st(1)
	jle	.L19
	fldt	.LC54(%rip)
	fmul	%st(1), %st
	fldt	.LC55(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC56(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC57(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC58(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC59(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC60(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC61(%rip)
	fsubrp	%st, %st(1)
	fstpt	48(%rsp)
	fldt	.LC62(%rip)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC63(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC64(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC65(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC66(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC67(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC68(%rip)
	faddp	%st, %st(1)
	fstpt	64(%rsp)
.L13:
	movq	8(%rsp), %rax
	movl	$0, 80(%rsp)
	subq	$16, %rsp
	movw	%ax, 104(%rsp)
	movq	16(%rsp), %rax
	shrq	$32, %rax
	movl	%eax, 100(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fchs
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	48(%rsp)
	fsubs	.LC69(%rip)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fstpt	32(%rsp)
	subq	$16, %rsp
	fldt	32(%rsp)
	fldt	64(%rsp)
	fsub	%st, %st(1)
	fldt	32(%rsp)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	80(%rsp)
	fldt	96(%rsp)
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	48(%rsp)
	addq	$32, %rsp
	testw	%bx, %bx
	fmulp	%st, %st(1)
	js	.L14
	fldt	(%rsp)
	fdivrp	%st, %st(1)
	fld1
	fsubp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	fldt	.LC5(%rip)
	fldt	112(%rsp)
	fmul	%st, %st(1)
	faddp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	fldt	.LC36(%rip)
	fmul	%st(1), %st
	fldt	.LC37(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC38(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC39(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC40(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC41(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC42(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC43(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC44(%rip)
	faddp	%st, %st(1)
	fstpt	48(%rsp)
	fldt	.LC45(%rip)
	fsubr	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC46(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC47(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC48(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC49(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC50(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC51(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC52(%rip)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC53(%rip)
	fsubrp	%st, %st(1)
	fstpt	64(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	fldt	.LC35(%rip)
	fld1
	fsubrp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	fsubrs	.LC34(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	fldt	(%rsp)
	fdivrp	%st, %st(1)
	fld1
	fsubrp	%st, %st(1)
	jmp	.L1
	.size	__erfl, .-__erfl
	.weak	erff64x
	.set	erff64x,__erfl
	.weak	erfl
	.set	erfl,__erfl
	.p2align 4,,15
	.globl	__erfcl
	.type	__erfcl, @function
__erfcl:
	pushq	%rbx
	subq	$96, %rsp
	movq	120(%rsp), %rbx
	movl	%ebx, %edx
	andw	$32767, %dx
	cmpw	$32767, %dx
	je	.L38
	movswl	%dx, %eax
	sall	$16, %eax
	movl	%eax, %edx
	movq	112(%rsp), %rax
	shrq	$48, %rax
	orl	%edx, %eax
	cmpl	$1073666047, %eax
	ja	.L23
	cmpl	$1069416447, %eax
	jbe	.L39
	fldt	112(%rsp)
	cmpl	$1073577983, %eax
	fld	%st(0)
	fmul	%st(0), %st
	fldt	.LC6(%rip)
	fmul	%st(1), %st
	fldt	.LC7(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC9(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC11(%rip)
	faddp	%st, %st(1)
	fldt	.LC12(%rip)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC13(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC14(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC15(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC16(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(2)
	fldt	.LC17(%rip)
	faddp	%st, %st(2)
	fdivp	%st, %st(1)
	fmul	%st(1), %st
	jg	.L25
	faddp	%st, %st(1)
	fld1
	fsubp	%st, %st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$1073717247, %eax
	ja	.L26
	fldt	112(%rsp)
	testw	%bx, %bx
	fabs
	fld1
	fsubr	%st, %st(1)
	fldt	.LC18(%rip)
	fmul	%st(2), %st
	fldt	.LC19(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC20(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC21(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC22(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC23(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC24(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC25(%rip)
	fsubrp	%st, %st(1)
	fldt	.LC26(%rip)
	fadd	%st(3), %st
	fmul	%st(3), %st
	fldt	.LC27(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC28(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC29(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC30(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC31(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(3)
	fldt	.LC32(%rip)
	faddp	%st, %st(3)
	fdivp	%st, %st(2)
	js	.L27
	fstp	%st(0)
	fsubrs	.LC72(%rip)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L38:
	shrl	$14, %ebx
	andl	$2, %ebx
	movl	%ebx, (%rsp)
	fildl	(%rsp)
	fld1
	fldt	112(%rsp)
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
.L20:
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$1074124287, %eax
	jg	.L28
	fldt	112(%rsp)
	cmpl	$1073788634, %eax
	fabs
	fstpt	(%rsp)
	fldt	112(%rsp)
	fmul	%st(0), %st
	fld1
	fdivp	%st, %st(1)
	jle	.L40
	cmpl	$1073861972, %eax
	jg	.L31
	fldt	.LC54(%rip)
	fmul	%st(1), %st
	fldt	.LC55(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC56(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC57(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC58(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC59(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC60(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC61(%rip)
	fsubrp	%st, %st(1)
	fstpt	48(%rsp)
	fldt	.LC62(%rip)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC63(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC64(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC65(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC66(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC67(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC68(%rip)
	faddp	%st, %st(1)
	fstpt	64(%rsp)
.L30:
	movq	8(%rsp), %rax
	movl	$0, 80(%rsp)
	subq	$16, %rsp
	movw	%ax, 104(%rsp)
	movq	16(%rsp), %rax
	shrq	$32, %rax
	xorb	%al, %al
	movl	%eax, 100(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fchs
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	48(%rsp)
	fsubs	.LC69(%rip)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fstpt	32(%rsp)
	subq	$16, %rsp
	fldt	32(%rsp)
	fldt	64(%rsp)
	fsub	%st, %st(1)
	fldt	32(%rsp)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	80(%rsp)
	fldt	96(%rsp)
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	48(%rsp)
	addq	$32, %rsp
	testw	%bx, %bx
	fmulp	%st, %st(1)
	fldt	(%rsp)
	fdivrp	%st, %st(1)
	js	.L33
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L20
	jne	.L20
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L39:
	fld1
	fldt	112(%rsp)
	addq	$96, %rsp
	popq	%rbx
	fsubrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	fstp	%st(1)
	flds	.LC71(%rip)
	fldt	112(%rsp)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fsubp	%st, %st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L28:
	testw	%bx, %bx
	js	.L35
	fldt	.LC35(%rip)
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	fmul	%st(0), %st
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L40:
	fldt	.LC36(%rip)
	fmul	%st(1), %st
	fldt	.LC37(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC38(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC39(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC40(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC41(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC42(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC43(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC44(%rip)
	faddp	%st, %st(1)
	fstpt	48(%rsp)
	fldt	.LC45(%rip)
	fsubr	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC46(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC47(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC48(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC49(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC50(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC51(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC52(%rip)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC53(%rip)
	fsubrp	%st, %st(1)
	fstpt	64(%rsp)
	jmp	.L30
.L41:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L35:
	fldt	.LC35(%rip)
	fsubrs	.LC73(%rip)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	fxch	%st(1)
	fadds	.LC33(%rip)
	faddp	%st, %st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L33:
	fsubrs	.LC73(%rip)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	testw	%bx, %bx
	js	.L41
	fldt	.LC74(%rip)
	fmul	%st(1), %st
	fldt	.LC75(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC76(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC77(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC78(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC79(%rip)
	fsubrp	%st, %st(1)
	fstpt	48(%rsp)
	fldt	.LC80(%rip)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC81(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC82(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC83(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC84(%rip)
	faddp	%st, %st(1)
	fstpt	64(%rsp)
	jmp	.L30
	.size	__erfcl, .-__erfcl
	.weak	erfcf64x
	.set	erfcf64x,__erfcl
	.weak	erfcl
	.set	erfcl,__erfcl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1098907648
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2799387756
	.long	2205537296
	.long	16384
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	1031798784
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC5:
	.long	2799387756
	.long	2205537296
	.long	16380
	.long	0
	.align 16
.LC6:
	.long	2601930728
	.long	2777149849
	.long	49158
	.long	0
	.align 16
.LC7:
	.long	2011927833
	.long	2786923847
	.long	16394
	.long	0
	.align 16
.LC8:
	.long	2568852590
	.long	2244176925
	.long	16399
	.long	0
	.align 16
.LC9:
	.long	2110120996
	.long	2715095432
	.long	16401
	.long	0
	.align 16
.LC10:
	.long	1055019513
	.long	2875938101
	.long	16404
	.long	0
	.align 16
.LC11:
	.long	3330329356
	.long	2299394766
	.long	16403
	.long	0
	.align 16
.LC12:
	.long	1177467865
	.long	2296638027
	.long	16390
	.long	0
	.align 16
.LC13:
	.long	1010259806
	.long	2365369345
	.long	16395
	.long	0
	.align 16
.LC14:
	.long	983254633
	.long	2440864925
	.long	16399
	.long	0
	.align 16
.LC15:
	.long	2505349499
	.long	2894380557
	.long	16402
	.long	0
	.align 16
.LC16:
	.long	633466601
	.long	3835943183
	.long	16404
	.long	0
	.align 16
.LC17:
	.long	1056318702
	.long	2238870623
	.long	16406
	.long	0
	.align 16
.LC18:
	.long	2536851545
	.long	3228095511
	.long	16379
	.long	0
	.align 16
.LC19:
	.long	505390227
	.long	3977633740
	.long	16383
	.long	0
	.align 16
.LC20:
	.long	3045715060
	.long	2534284771
	.long	16384
	.long	0
	.align 16
.LC21:
	.long	2882728357
	.long	2259178991
	.long	16387
	.long	0
	.align 16
.LC22:
	.long	1352404112
	.long	2976889265
	.long	16388
	.long	0
	.align 16
.LC23:
	.long	3616091059
	.long	3583045579
	.long	16388
	.long	0
	.align 16
.LC24:
	.long	2810900404
	.long	3162194766
	.long	16390
	.long	0
	.align 16
.LC25:
	.long	2575486127
	.long	2312737123
	.long	16383
	.long	0
	.align 16
.LC26:
	.long	2709548192
	.long	2491763466
	.long	16385
	.long	0
	.align 16
.LC27:
	.long	3312349077
	.long	2789978110
	.long	16387
	.long	0
	.align 16
.LC28:
	.long	2832367916
	.long	4066925157
	.long	16388
	.long	0
	.align 16
.LC29:
	.long	64966662
	.long	2346655809
	.long	16390
	.long	0
	.align 16
.LC30:
	.long	1250757164
	.long	2387457367
	.long	16391
	.long	0
	.align 16
.LC31:
	.long	2611981490
	.long	2759222032
	.long	16391
	.long	0
	.align 16
.LC32:
	.long	2125792576
	.long	3824587613
	.long	16391
	.long	0
	.section	.rodata.cst4
	.align 4
.LC33:
	.long	1062753803
	.align 4
.LC34:
	.long	3210237451
	.section	.rodata.cst16
	.align 16
.LC35:
	.long	3168843697
	.long	3193661164
	.long	2
	.long	0
	.align 16
.LC36:
	.long	968920464
	.long	3726079840
	.long	16390
	.long	0
	.align 16
.LC37:
	.long	1654480531
	.long	2503467293
	.long	16394
	.long	0
	.align 16
.LC38:
	.long	2772926118
	.long	3808656621
	.long	16395
	.long	0
	.align 16
.LC39:
	.long	1509144298
	.long	2340487575
	.long	16396
	.long	0
	.align 16
.LC40:
	.long	4213181831
	.long	2667859651
	.long	16395
	.long	0
	.align 16
.LC41:
	.long	207773359
	.long	2960387134
	.long	16393
	.long	0
	.align 16
.LC42:
	.long	645943422
	.long	3124520529
	.long	16390
	.long	0
	.align 16
.LC43:
	.long	4007677580
	.long	2733218314
	.long	16386
	.long	0
	.align 16
.LC44:
	.long	591933472
	.long	2342589567
	.long	16380
	.long	0
	.align 16
.LC45:
	.long	2980778670
	.long	2352570452
	.long	16390
	.long	0
	.align 16
.LC46:
	.long	451250474
	.long	3014173373
	.long	16394
	.long	0
	.align 16
.LC47:
	.long	4222930349
	.long	3802661462
	.long	16396
	.long	0
	.align 16
.LC48:
	.long	3314547959
	.long	3775227214
	.long	16397
	.long	0
	.align 16
.LC49:
	.long	80294030
	.long	3503659150
	.long	16397
	.long	0
	.align 16
.LC50:
	.long	1667980724
	.long	3267945035
	.long	16396
	.long	0
	.align 16
.LC51:
	.long	1298673777
	.long	3092381553
	.long	16394
	.long	0
	.align 16
.LC52:
	.long	712667591
	.long	2781359446
	.long	16391
	.long	0
	.align 16
.LC53:
	.long	653564961
	.long	3710407819
	.long	16386
	.long	0
	.align 16
.LC54:
	.long	531584038
	.long	3009115771
	.long	49151
	.long	0
	.align 16
.LC55:
	.long	345153970
	.long	3795256038
	.long	16385
	.long	0
	.align 16
.LC56:
	.long	1352846491
	.long	2373584132
	.long	16386
	.long	0
	.align 16
.LC57:
	.long	3587418637
	.long	2294472554
	.long	16385
	.long	0
	.align 16
.LC58:
	.long	136985911
	.long	4002494091
	.long	16382
	.long	0
	.align 16
.LC59:
	.long	902232627
	.long	3241644044
	.long	16379
	.long	0
	.align 16
.LC60:
	.long	303399741
	.long	2215625546
	.long	16375
	.long	0
	.align 16
.LC61:
	.long	2630050001
	.long	3426667463
	.long	16368
	.long	0
	.align 16
.LC62:
	.long	1292118506
	.long	3173066803
	.long	16386
	.long	0
	.align 16
.LC63:
	.long	1501313601
	.long	3851717123
	.long	16387
	.long	0
	.align 16
.LC64:
	.long	3165305670
	.long	3397946273
	.long	16387
	.long	0
	.align 16
.LC65:
	.long	1585116560
	.long	2664922935
	.long	16386
	.long	0
	.align 16
.LC66:
	.long	3017718794
	.long	3974236300
	.long	16383
	.long	0
	.align 16
.LC67:
	.long	4027298389
	.long	2720359483
	.long	16380
	.long	0
	.align 16
.LC68:
	.long	1509338728
	.long	2713734865
	.long	16375
	.long	0
	.section	.rodata.cst4
	.align 4
.LC69:
	.long	1058013184
	.align 4
.LC71:
	.long	1056964608
	.align 4
.LC72:
	.long	1042196436
	.align 4
.LC73:
	.long	1073741824
	.section	.rodata.cst16
	.align 16
.LC74:
	.long	2935883941
	.long	2210627125
	.long	49151
	.long	0
	.align 16
.LC75:
	.long	2164815514
	.long	3790999974
	.long	16383
	.long	0
	.align 16
.LC76:
	.long	428680734
	.long	3230392563
	.long	16382
	.long	0
	.align 16
.LC77:
	.long	103343542
	.long	3922738666
	.long	16379
	.long	0
	.align 16
.LC78:
	.long	1769199140
	.long	3432590466
	.long	16375
	.long	0
	.align 16
.LC79:
	.long	333618457
	.long	2920168319
	.long	16369
	.long	0
	.align 16
.LC80:
	.long	553016326
	.long	2687134331
	.long	16385
	.long	0
	.align 16
.LC81:
	.long	3884659201
	.long	2650418124
	.long	16385
	.long	0
	.align 16
.LC82:
	.long	4069326463
	.long	3519865159
	.long	16383
	.long	0
	.align 16
.LC83:
	.long	2137717972
	.long	3547839410
	.long	16380
	.long	0
	.align 16
.LC84:
	.long	489413003
	.long	2312615001
	.long	16376
	.long	0
