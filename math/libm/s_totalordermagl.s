	.text
	.p2align 4,,15
	.globl	__totalordermagl
	.type	__totalordermagl, @function
__totalordermagl:
	movl	8(%rdi), %r10d
	movq	(%rdi), %r9
	movl	$1, %eax
	movl	8(%rsi), %edi
	movq	(%rsi), %rsi
	movq	%r10, %rcx
	movq	%rdi, %rdx
	andw	$32767, %cx
	andw	$32767, %dx
	cmpw	%dx, %cx
	jb	.L1
	movl	$0, %eax
	je	.L7
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r9, %rcx
	movq	%rsi, %rdx
	movl	$1, %eax
	shrq	$32, %rcx
	shrq	$32, %rdx
	cmpl	%edx, %ecx
	jb	.L1
	sete	%dl
	xorl	%eax, %eax
	cmpl	%esi, %r9d
	setbe	%al
	andl	%edx, %eax
	ret
	.size	__totalordermagl, .-__totalordermagl
	.weak	totalordermagf64x
	.set	totalordermagf64x,__totalordermagl
	.weak	totalordermagl
	.set	totalordermagl,__totalordermagl
