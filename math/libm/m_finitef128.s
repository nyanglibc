	.text
	.p2align 4,,15
	.globl	__finitef128
	.type	__finitef128, @function
__finitef128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rax
	movabsq	$9223090561878065152, %rdx
	andq	%rdx, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__finitef128, .-__finitef128
	.weak	finitef128_do_not_use
	.set	finitef128_do_not_use,__finitef128
