	.text
	.p2align 4,,15
	.globl	__GI___isinff
	.hidden	__GI___isinff
	.type	__GI___isinff, @function
__GI___isinff:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	sarl	$30, %edx
	xorl	$2139095040, %eax
	movl	%eax, %ecx
	negl	%eax
	orl	%ecx, %eax
	sarl	$31, %eax
	notl	%eax
	andl	%edx, %eax
	ret
	.size	__GI___isinff, .-__GI___isinff
	.globl	__isinff
	.set	__isinff,__GI___isinff
	.weak	isinff
	.set	isinff,__isinff
