	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__lttf2
	.p2align 4,,15
	.globl	__cacoshf128
	.type	__cacoshf128, @function
__cacoshf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$64, %rsp
	movdqa	.LC7(%rip), %xmm0
	movdqa	96(%rsp), %xmm3
	movdqa	80(%rsp), %xmm2
	pand	%xmm0, %xmm2
	pand	%xmm3, %xmm0
	movdqa	%xmm2, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	16(%rsp), %xmm2
	movdqa	.LC8(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L53
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L49
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L54
	movaps	80(%rsp), %xmm5
	pxor	%xmm0, %xmm0
	movmskps	%xmm5, %eax
	testb	$8, %al
	je	.L18
	movdqa	.LC3(%rip), %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L53:
	movdqa	16(%rsp), %xmm2
	movdqa	.LC9(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	(%rsp), %xmm0
	testq	%rax, %rax
	movdqa	%xmm0, %xmm1
	jne	.L51
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L55
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L30
	movdqa	.LC9(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L56
.L8:
	movdqa	96(%rsp), %xmm0
	leaq	32(%rsp), %rdi
	movl	$1, %esi
	pushq	88(%rsp)
	pushq	88(%rsp)
	pxor	.LC10(%rip), %xmm0
	subq	$16, %rsp
	movups	%xmm0, (%rsp)
	call	__kernel_casinhf128@PLT
	movaps	128(%rsp), %xmm4
	movmskps	%xmm4, %eax
	movdqa	64(%rsp), %xmm1
	movdqa	80(%rsp), %xmm0
	addq	$32, %rsp
	testb	$8, %al
	jne	.L57
	pxor	.LC10(%rip), %xmm1
.L6:
	movaps	%xmm1, (%rbx)
	movq	%rbx, %rax
	movaps	%xmm0, 16(%rbx)
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
.L51:
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L47
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L8
.L30:
	movdqa	.LC1(%rip), %xmm0
.L18:
	movdqa	96(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	.LC6(%rip), %xmm1
	movq	%rbx, %rax
	movaps	%xmm1, (%rbx)
	movaps	%xmm0, 16(%rbx)
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L47
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L47
.L49:
	movdqa	.LC6(%rip), %xmm1
	movq	%rbx, %rax
	movdqa	.LC5(%rip), %xmm0
	movaps	%xmm1, (%rbx)
	movaps	%xmm0, 16(%rbx)
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	pxor	.LC10(%rip), %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L56:
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L8
	movdqa	96(%rsp), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__copysignf128@PLT
	pxor	%xmm1, %xmm1
	movq	%rbx, %rax
	movaps	%xmm0, 16(%rbx)
	movaps	%xmm1, (%rbx)
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm1, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L58
	movdqa	.LC0(%rip), %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L55:
	movdqa	.LC5(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L58:
	movdqa	.LC2(%rip), %xmm0
	jmp	.L18
	.size	__cacoshf128, .-__cacoshf128
	.weak	cacoshf128
	.set	cacoshf128,__cacoshf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073648159
	.align 16
.LC1:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC2:
	.long	2479964490
	.long	592389929
	.long	3354604061
	.long	1073753495
	.align 16
.LC3:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073779231
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC7:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC8:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
