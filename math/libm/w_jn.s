	.text
	.p2align 4,,15
	.globl	__jn
	.type	__jn, @function
__jn:
	jmp	__ieee754_jn@PLT
	.size	__jn, .-__jn
	.weak	jnf32x
	.set	jnf32x,__jn
	.weak	jnf64
	.set	jnf64,__jn
	.weak	jn
	.set	jn,__jn
	.p2align 4,,15
	.globl	__yn
	.type	__yn, @function
__yn:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L11
.L4:
	jmp	__ieee754_yn@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	ucomisd	%xmm1, %xmm0
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__yn, .-__yn
	.weak	ynf32x
	.set	ynf32x,__yn
	.weak	ynf64
	.set	ynf64,__yn
	.weak	yn
	.set	yn,__yn
