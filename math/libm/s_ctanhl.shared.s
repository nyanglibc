	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ctanhl
	.type	__ctanhl, @function
__ctanhl:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$112, %rsp
	fldt	16(%rbp)
	fldt	32(%rbp)
	fld	%st(1)
	fabs
	fldt	.LC2(%rip)
	fucomi	%st(1), %st
	jb	.L2
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	jb	.L77
	fld	%st(3)
	fnstcw	-34(%rbp)
	movzwl	-34(%rbp), %eax
	fldln2
	fmuls	.LC7(%rip)
	orb	$12, %ah
	movw	%ax, -36(%rbp)
	fmuls	.LC8(%rip)
	fldcw	-36(%rbp)
	fistpl	-64(%rbp)
	fldcw	-34(%rbp)
	fldt	.LC9(%rip)
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jbe	.L71
	fstp	%st(0)
	fxch	%st(2)
	fld	%st(0)
	fstpt	-112(%rbp)
	subq	$16, %rsp
	leaq	-32(%rbp), %rdi
	leaq	-16(%rbp), %rsi
	fstpt	-96(%rbp)
	fxch	%st(1)
	fstpt	-80(%rbp)
	fstpt	(%rsp)
	call	__sincosl@PLT
	popq	%rdi
	popq	%r8
	fldt	-80(%rbp)
	fldt	-96(%rbp)
	fldt	-112(%rbp)
.L19:
	fildl	-64(%rbp)
	fxch	%st(3)
	fucomi	%st(3), %st
	ja	.L78
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fldt	.LC9(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L79
	fstp	%st(1)
	fld1
.L26:
	fldt	-16(%rbp)
	fld	%st(0)
	fmul	%st(1), %st
	fld	%st(1)
	fabs
	fmuls	.LC11(%rip)
	fxch	%st(4)
	fucomip	%st(4), %st
	fstp	%st(3)
	jbe	.L85
	fld	%st(3)
	fmul	%st(4), %st
	faddp	%st, %st(3)
	fxch	%st(3)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L85:
	fxch	%st(3)
.L28:
	fmulp	%st, %st(1)
	fdiv	%st(1), %st
	fld	%st(0)
	fldt	-32(%rbp)
	fmulp	%st, %st(4)
	fxch	%st(3)
	fdivp	%st, %st(2)
	fld	%st(1)
.L25:
	fldt	.LC9(%rip)
	fld	%st(4)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L86
	fxch	%st(3)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L86:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
.L30:
	fldt	.LC9(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L87
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L87:
	fstp	%st(2)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L91:
	fxch	%st(1)
.L1:
	leave
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	fstp	%st(2)
	fstpt	-112(%rbp)
	fxch	%st(1)
	movl	-64(%rbp), %eax
	subq	$16, %rsp
	addl	%eax, %eax
	movl	%eax, -64(%rbp)
	fstpt	-96(%rbp)
	fstpt	-80(%rbp)
	fildl	-64(%rbp)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-112(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fld1
	testb	$2, %ah
	fldt	-80(%rbp)
	fldt	-96(%rbp)
	jne	.L80
.L22:
	popq	%rcx
	popq	%rsi
	fsubr	%st, %st(1)
	fldt	-32(%rbp)
	fmuls	.LC10(%rip)
	fldt	-16(%rbp)
	fmulp	%st, %st(1)
	fdiv	%st(4), %st
	fxch	%st(2)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L73
	fstp	%st(0)
	fdivp	%st, %st(2)
	fld	%st(0)
	fld	%st(2)
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L79:
	fstp	%st(0)
	fstp	%st(1)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	-80(%rbp)
	call	__ieee754_sinhl@PLT
	fstpt	-64(%rbp)
	subq	$16, %rsp
	fldt	-80(%rbp)
	fstpt	(%rsp)
	call	__ieee754_coshl@PLT
	addq	$32, %rsp
	fldt	-64(%rbp)
	fld	%st(0)
	fabs
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L80:
	fstp	%st(2)
	fxch	%st(1)
	fld1
	fchs
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L73:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	fstpt	-80(%rbp)
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	-64(%rbp)
	fadd	%st(0), %st
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-80(%rbp)
	popq	%rax
	popq	%rdx
	fdivp	%st, %st(1)
	fldt	-64(%rbp)
	fld	%st(0)
	fld	%st(2)
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxam
	fnstsw	%ax
	movl	%eax, %edx
	andb	$69, %dh
	cmpb	$5, %dh
	jne	.L5
	fstp	%st(0)
	testb	$2, %ah
	fld1
	jne	.L81
.L6:
	fld	%st(1)
	fabs
	fldt	.LC2(%rip)
	fucomip	%st(1), %st
	jb	.L88
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L82
	fxch	%st(1)
	jmp	.L7
.L88:
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L92:
	fxch	%st(1)
.L7:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L1
	fstp	%st(0)
	fldz
	fchs
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L77:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxam
	fnstsw	%ax
	movl	%eax, %edx
	andb	$69, %dh
	cmpb	$5, %dh
	je	.L83
.L5:
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L11
	je	.L84
.L11:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L89
	jne	.L90
	flds	.LC0(%rip)
	fxch	%st(2)
.L15:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L91
	fxch	%st(1)
	fstpt	-80(%rbp)
	movl	$1, %edi
	fstpt	-64(%rbp)
	call	__GI_feraiseexcept
	fldt	-64(%rbp)
	fldt	-80(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L71:
	fxch	%st(2)
	fstpt	-32(%rbp)
	fld1
	fstpt	-16(%rbp)
	fxch	%st(2)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L83:
	fstp	%st(0)
	testb	$2, %ah
	fld1
	je	.L92
	fstp	%st(0)
	fld1
	fchs
	fxch	%st(1)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L81:
	fstp	%st(0)
	fld1
	fchs
	jmp	.L6
.L89:
	fstp	%st(0)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L90:
	fstp	%st(0)
.L37:
	flds	.LC0(%rip)
	fld	%st(0)
	fxch	%st(2)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L82:
	fstpt	-64(%rbp)
	subq	$16, %rsp
	leaq	-16(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	fldt	-32(%rbp)
	fldt	-16(%rbp)
	fmulp	%st, %st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fldz
	testb	$2, %ah
	fldt	-64(%rbp)
	je	.L13
	fstp	%st(1)
	fldz
	fchs
	fxch	%st(1)
.L13:
	fxch	%st(1)
	popq	%r9
	popq	%r10
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L84:
	fxch	%st(1)
	jmp	.L1
	.size	__ctanhl, .-__ctanhl
	.weak	ctanhf64x
	.set	ctanhf64x,__ctanhl
	.weak	ctanhl
	.set	ctanhl,__ctanhl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1182792704
	.align 4
.LC8:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	1082130432
	.align 4
.LC11:
	.long	536870912
