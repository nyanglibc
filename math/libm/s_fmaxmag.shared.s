	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fmaxmag
	.type	__fmaxmag, @function
__fmaxmag:
	movq	.LC0(%rip), %xmm2
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm3
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm3
	ja	.L21
	ucomisd	%xmm3, %xmm2
	ja	.L19
	ucomisd	%xmm2, %xmm3
	jp	.L5
	je	.L24
.L5:
	subq	$24, %rsp
	movsd	%xmm1, (%rsp)
	movsd	%xmm0, 8(%rsp)
	call	__GI___issignaling
	testl	%eax, %eax
	jne	.L8
	movsd	(%rsp), %xmm0
	call	__GI___issignaling
	testl	%eax, %eax
	jne	.L8
	movsd	(%rsp), %xmm4
	movsd	8(%rsp), %xmm6
	movapd	%xmm4, %xmm1
	cmpordsd	%xmm4, %xmm1
	andpd	%xmm1, %xmm4
	andnpd	%xmm6, %xmm1
	movapd	%xmm4, %xmm0
	orpd	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	rep ret
	.p2align 4,,10
	.p2align 3
.L19:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	maxsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movsd	8(%rsp), %xmm0
	addsd	(%rsp), %xmm0
.L1:
	addq	$24, %rsp
	ret
	.size	__fmaxmag, .-__fmaxmag
	.weak	fmaxmagf32x
	.set	fmaxmagf32x,__fmaxmag
	.weak	fmaxmagf64
	.set	fmaxmagf64,__fmaxmag
	.weak	fmaxmag
	.set	fmaxmag,__fmaxmag
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
