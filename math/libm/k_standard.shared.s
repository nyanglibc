	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"acosl"
.LC1:
	.string	"acos"
.LC2:
	.string	"acosf"
.LC3:
	.string	"asinl"
.LC4:
	.string	"asin"
.LC5:
	.string	"asinf"
.LC6:
	.string	"atan2l"
.LC7:
	.string	"atan2"
.LC8:
	.string	"atan2f"
.LC9:
	.string	"hypotl"
.LC10:
	.string	"hypot"
.LC11:
	.string	"hypotf"
.LC12:
	.string	"coshl"
.LC13:
	.string	"cosh"
.LC14:
	.string	"coshf"
.LC15:
	.string	"expl"
.LC16:
	.string	"exp"
.LC17:
	.string	"expf"
.LC18:
	.string	"y0l"
.LC19:
	.string	"y0"
.LC20:
	.string	"y0f"
.LC21:
	.string	"y1l"
.LC22:
	.string	"y1"
.LC23:
	.string	"y1f"
.LC24:
	.string	"ynl"
.LC25:
	.string	"yn"
.LC26:
	.string	"ynf"
.LC29:
	.string	"lgammal"
.LC30:
	.string	"lgamma"
.LC31:
	.string	"lgammaf"
.LC32:
	.string	"logl"
.LC33:
	.string	"log"
.LC34:
	.string	"logf"
.LC35:
	.string	"log10l"
.LC36:
	.string	"log10"
.LC37:
	.string	"log10f"
.LC38:
	.string	"powl"
.LC39:
	.string	"pow"
.LC40:
	.string	"powf"
.LC41:
	.string	"sinhl"
.LC42:
	.string	"sinh"
.LC43:
	.string	"sinhf"
.LC46:
	.string	"sqrtl"
.LC47:
	.string	"sqrt"
.LC48:
	.string	"sqrtf"
.LC49:
	.string	"fmodl"
.LC50:
	.string	"fmod"
.LC51:
	.string	"fmodf"
.LC52:
	.string	"remainderl"
.LC53:
	.string	"remainder"
.LC54:
	.string	"remainderf"
.LC55:
	.string	"acoshl"
.LC56:
	.string	"acosh"
.LC57:
	.string	"acoshf"
.LC58:
	.string	"atanhl"
.LC59:
	.string	"atanh"
.LC60:
	.string	"atanhf"
.LC61:
	.string	"scalbl"
.LC62:
	.string	"scalb"
.LC63:
	.string	"scalbf"
.LC64:
	.string	"j0l"
.LC65:
	.string	"j0"
.LC66:
	.string	"j0f"
.LC67:
	.string	"j1l"
.LC68:
	.string	"j1"
.LC69:
	.string	"j1f"
.LC70:
	.string	"jnl"
.LC71:
	.string	"jn"
.LC72:
	.string	"jnf"
.LC73:
	.string	"tgammal"
.LC74:
	.string	"tgamma"
.LC75:
	.string	"tgammaf"
.LC76:
	.string	"exp2l"
.LC77:
	.string	"exp2"
.LC78:
	.string	"exp2f"
.LC79:
	.string	"exp10l"
.LC80:
	.string	"exp10"
.LC81:
	.string	"exp10f"
.LC82:
	.string	"log2l"
.LC83:
	.string	"log2"
.LC84:
	.string	"log2f"
.LC87:
	.string	"acos: DOMAIN error\n"
.LC88:
	.string	"asin: DOMAIN error\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC89:
	.string	"../sysdeps/ieee754/k_standard.c"
	.section	.rodata.str1.1
.LC90:
	.string	"_LIB_VERSION == _SVID_"
.LC91:
	.string	"atan2: DOMAIN error\n"
.LC92:
	.string	"y0: DOMAIN error\n"
.LC93:
	.string	"y1: DOMAIN error\n"
.LC94:
	.string	"yn: DOMAIN error\n"
.LC95:
	.string	"lgamma: SING error\n"
.LC96:
	.string	"log: SING error\n"
.LC97:
	.string	"log: DOMAIN error\n"
.LC98:
	.string	"log10: SING error\n"
.LC99:
	.string	"log10: DOMAIN error\n"
.LC104:
	.string	"pow(0,neg): DOMAIN error\n"
	.section	.rodata.str1.8
	.align 8
.LC105:
	.string	"neg**non-integral: DOMAIN error\n"
	.section	.rodata.str1.1
.LC106:
	.string	"sqrt: DOMAIN error\n"
.LC107:
	.string	"fmod:  DOMAIN error\n"
.LC108:
	.string	"remainder: DOMAIN error\n"
.LC109:
	.string	"acosh: DOMAIN error\n"
.LC110:
	.string	"atanh: DOMAIN error\n"
.LC111:
	.string	"atanh: SING error\n"
.LC113:
	.string	": TLOSS error\n"
.LC115:
	.string	"tgamma: SING error\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__kernel_standard
	.type	__kernel_standard, @function
__kernel_standard:
	pushq	%rbx
	subq	$48, %rsp
	cmpl	$250, %edi
	movsd	%xmm0, 16(%rsp)
	movsd	%xmm1, 24(%rsp)
	ja	.L2
	leaq	.L4(%rip), %rdx
	movl	%edi, %eax
	movapd	%xmm0, %xmm2
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L2-.L4
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L6-.L4
	.long	.L7-.L4
	.long	.L8-.L4
	.long	.L9-.L4
	.long	.L10-.L4
	.long	.L11-.L4
	.long	.L12-.L4
	.long	.L13-.L4
	.long	.L14-.L4
	.long	.L15-.L4
	.long	.L16-.L4
	.long	.L17-.L4
	.long	.L18-.L4
	.long	.L19-.L4
	.long	.L20-.L4
	.long	.L21-.L4
	.long	.L22-.L4
	.long	.L2-.L4
	.long	.L23-.L4
	.long	.L24-.L4
	.long	.L25-.L4
	.long	.L26-.L4
	.long	.L27-.L4
	.long	.L28-.L4
	.long	.L29-.L4
	.long	.L30-.L4
	.long	.L31-.L4
	.long	.L32-.L4
	.long	.L33-.L4
	.long	.L34-.L4
	.long	.L35-.L4
	.long	.L36-.L4
	.long	.L37-.L4
	.long	.L38-.L4
	.long	.L39-.L4
	.long	.L40-.L4
	.long	.L41-.L4
	.long	.L42-.L4
	.long	.L43-.L4
	.long	.L2-.L4
	.long	.L44-.L4
	.long	.L45-.L4
	.long	.L46-.L4
	.long	.L47-.L4
	.long	.L48-.L4
	.long	.L49-.L4
	.long	.L50-.L4
	.long	.L51-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L6-.L4
	.long	.L7-.L4
	.long	.L8-.L4
	.long	.L9-.L4
	.long	.L10-.L4
	.long	.L11-.L4
	.long	.L12-.L4
	.long	.L13-.L4
	.long	.L14-.L4
	.long	.L15-.L4
	.long	.L16-.L4
	.long	.L17-.L4
	.long	.L18-.L4
	.long	.L19-.L4
	.long	.L20-.L4
	.long	.L21-.L4
	.long	.L22-.L4
	.long	.L2-.L4
	.long	.L23-.L4
	.long	.L24-.L4
	.long	.L25-.L4
	.long	.L26-.L4
	.long	.L27-.L4
	.long	.L28-.L4
	.long	.L29-.L4
	.long	.L30-.L4
	.long	.L31-.L4
	.long	.L32-.L4
	.long	.L33-.L4
	.long	.L34-.L4
	.long	.L35-.L4
	.long	.L36-.L4
	.long	.L37-.L4
	.long	.L38-.L4
	.long	.L39-.L4
	.long	.L40-.L4
	.long	.L41-.L4
	.long	.L42-.L4
	.long	.L43-.L4
	.long	.L2-.L4
	.long	.L44-.L4
	.long	.L45-.L4
	.long	.L46-.L4
	.long	.L47-.L4
	.long	.L48-.L4
	.long	.L49-.L4
	.long	.L50-.L4
	.long	.L51-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L2-.L4
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L6-.L4
	.long	.L7-.L4
	.long	.L8-.L4
	.long	.L9-.L4
	.long	.L10-.L4
	.long	.L11-.L4
	.long	.L12-.L4
	.long	.L13-.L4
	.long	.L14-.L4
	.long	.L15-.L4
	.long	.L16-.L4
	.long	.L17-.L4
	.long	.L18-.L4
	.long	.L19-.L4
	.long	.L20-.L4
	.long	.L21-.L4
	.long	.L22-.L4
	.long	.L2-.L4
	.long	.L23-.L4
	.long	.L24-.L4
	.long	.L25-.L4
	.long	.L26-.L4
	.long	.L27-.L4
	.long	.L28-.L4
	.long	.L29-.L4
	.long	.L30-.L4
	.long	.L31-.L4
	.long	.L32-.L4
	.long	.L33-.L4
	.long	.L34-.L4
	.long	.L35-.L4
	.long	.L36-.L4
	.long	.L37-.L4
	.long	.L38-.L4
	.long	.L39-.L4
	.long	.L40-.L4
	.long	.L41-.L4
	.long	.L42-.L4
	.long	.L43-.L4
	.long	.L2-.L4
	.long	.L44-.L4
	.long	.L45-.L4
	.long	.L46-.L4
	.long	.L47-.L4
	.long	.L48-.L4
	.long	.L49-.L4
	.long	.L50-.L4
	.long	.L51-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC74(%rip), %rax
	jle	.L272
	leaq	.LC73(%rip), %rax
	leaq	.LC75(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L272:
	andpd	.LC112(%rip), %xmm2
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	cmpl	$2, (%rbx)
	orpd	.LC114(%rip), %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 32(%rsp)
	je	.L442
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L443
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L275
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC115(%rip), %rdi
	movl	$19, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L275:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
.L1:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC83(%rip), %rax
	jle	.L267
	leaq	.LC82(%rip), %rax
	leaq	.LC84(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L267:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L268
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L269:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L271
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC83(%rip), %rax
	jle	.L263
	leaq	.LC82(%rip), %rax
	leaq	.LC84(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L263:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L264
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L265:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L266
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	$99, %edi
	movl	$4, (%rsp)
	leaq	.LC80(%rip), %rax
	jle	.L261
	leaq	.LC79(%rip), %rax
	leaq	.LC81(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L261:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rax)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L262
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L43:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC74(%rip), %rax
	jle	.L245
	leaq	.LC73(%rip), %rax
	leaq	.LC75(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L245:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movsd	.LC85(%rip), %xmm0
	movq	%rax, 8(%rsp)
	movsd	%xmm0, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L270
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L444
	movl	(%rbx), %edx
	testl	%edx, %edx
	je	.L248
	movsd	32(%rsp), %xmm0
.L249:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L42:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC74(%rip), %rax
	jle	.L242
	leaq	.LC73(%rip), %rax
	leaq	.LC75(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L242:
	andpd	.LC112(%rip), %xmm2
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$2, (%rax)
	orpd	.LC114(%rip), %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 32(%rsp)
	je	.L445
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L244
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC80(%rip), %rax
	jle	.L256
	leaq	.LC79(%rip), %rax
	leaq	.LC81(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L256:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L257
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L258:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L260
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L46:
	cmpl	$99, %edi
	movl	$4, (%rsp)
	leaq	.LC77(%rip), %rax
	jle	.L254
	leaq	.LC76(%rip), %rax
	leaq	.LC78(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L254:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rax)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L255
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$99, %edi
	movl	$5, (%rsp)
	leaq	.LC22(%rip), %rax
	jle	.L233
	leaq	.LC21(%rip), %rax
	leaq	.LC23(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L233:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L446
	movl	(%rbx), %edi
	testl	%edi, %edi
	jne	.L235
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	8(%rsp), %rdi
	movq	(%rbx), %rsi
	call	fputs@PLT
	movq	(%rbx), %rcx
	leaq	.LC113(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L235:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC77(%rip), %rax
	jle	.L250
	leaq	.LC76(%rip), %rax
	leaq	.LC78(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L250:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L251
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L252:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L253
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L44:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC39(%rip), %rax
	jle	.L170
	leaq	.LC38(%rip), %rax
	leaq	.LC40(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L170:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L171
	movq	$0x000000000, 32(%rsp)
.L172:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L447
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L174
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC104(%rip), %rdi
	movl	$25, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L174:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$99, %edi
	movl	$5, (%rsp)
	leaq	.LC25(%rip), %rax
	jle	.L239
	leaq	.LC24(%rip), %rax
	leaq	.LC26(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L239:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L448
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L241
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	8(%rsp), %rdi
	movq	(%rbx), %rsi
	call	fputs@PLT
	movq	(%rbx), %rcx
	leaq	.LC113(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L241:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L40:
	cmpl	$99, %edi
	movl	$5, (%rsp)
	leaq	.LC71(%rip), %rax
	jle	.L236
	leaq	.LC70(%rip), %rax
	leaq	.LC72(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L236:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L449
	movl	(%rbx), %esi
	testl	%esi, %esi
	jne	.L238
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	8(%rsp), %rdi
	movq	(%rbx), %rsi
	call	fputs@PLT
	movq	(%rbx), %rcx
	leaq	.LC113(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L238:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$99, %edi
	movl	$5, (%rsp)
	leaq	.LC19(%rip), %rax
	jle	.L226
	leaq	.LC18(%rip), %rax
	leaq	.LC20(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L226:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L450
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jne	.L229
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	8(%rsp), %rdi
	movq	(%rbx), %rsi
	call	fputs@PLT
	movq	(%rbx), %rcx
	leaq	.LC113(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L229:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$99, %edi
	movl	$5, (%rsp)
	leaq	.LC65(%rip), %rax
	jle	.L222
	leaq	.LC64(%rip), %rax
	leaq	.LC66(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L222:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L451
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jne	.L225
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	8(%rsp), %rdi
	movq	(%rbx), %rsi
	call	fputs@PLT
	movq	(%rbx), %rcx
	leaq	.LC113(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L225:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$99, %edi
	movl	$5, (%rsp)
	leaq	.LC68(%rip), %rax
	jle	.L230
	leaq	.LC67(%rip), %rax
	leaq	.LC69(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L230:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rbx)
	je	.L227
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L452
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jne	.L232
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	8(%rsp), %rdi
	movq	(%rbx), %rsi
	call	fputs@PLT
	movq	(%rbx), %rcx
	leaq	.LC113(%rip), %rdi
	movl	$14, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L232:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	$99, %edi
	movl	$4, (%rsp)
	leaq	.LC62(%rip), %rax
	jle	.L219
	leaq	.LC61(%rip), %rax
	leaq	.LC63(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L219:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	andpd	.LC112(%rip), %xmm2
	cmpl	$2, (%rax)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 32(%rsp)
	je	.L453
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L221
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC62(%rip), %rax
	jle	.L214
	leaq	.LC61(%rip), %rax
	leaq	.LC63(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L214:
	ucomisd	.LC86(%rip), %xmm2
	movq	%rax, 8(%rsp)
	movsd	.LC27(%rip), %xmm0
	ja	.L215
	movsd	.LC28(%rip), %xmm0
.L215:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movsd	%xmm0, 32(%rsp)
	cmpl	$2, (%rax)
	je	.L454
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L218
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC33(%rip), %rax
	jle	.L126
	leaq	.LC32(%rip), %rax
	leaq	.LC34(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L126:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L127
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L128:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L455
	movl	(%rbx), %r8d
	testl	%r8d, %r8d
	jne	.L130
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC96(%rip), %rdi
	movl	$16, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L130:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC30(%rip), %rax
	jle	.L121
	leaq	.LC29(%rip), %rax
	leaq	.LC31(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L121:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L122
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L123:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L456
	movl	(%rbx), %r9d
	testl	%r9d, %r9d
	jne	.L125
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC95(%rip), %rdi
	movl	$19, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L125:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC42(%rip), %rax
	jle	.L180
	leaq	.LC41(%rip), %rax
	leaq	.LC43(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L180:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L181
	ucomisd	.LC86(%rip), %xmm2
	movsd	.LC44(%rip), %xmm0
	ja	.L182
	movsd	.LC45(%rip), %xmm0
.L182:
	movsd	%xmm0, 32(%rsp)
.L184:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L187
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC39(%rip), %rax
	jle	.L175
	leaq	.LC38(%rip), %rax
	leaq	.LC40(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L175:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L176
	movq	$0x000000000, 32(%rsp)
.L177:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L457
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L179
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC105(%rip), %rdi
	movl	$32, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L179:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC19(%rip), %rax
	jle	.L84
	leaq	.LC18(%rip), %rax
	leaq	.LC20(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L84:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L85
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L86:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L458
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L88
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC92(%rip), %rdi
	movl	$17, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L88:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$99, %edi
	movl	$4, (%rsp)
	leaq	.LC16(%rip), %rax
	jle	.L81
	leaq	.LC15(%rip), %rax
	leaq	.LC17(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L81:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movq	$0x000000000, 32(%rsp)
	cmpl	$2, (%rax)
	je	.L459
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L83
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC16(%rip), %rax
	jle	.L77
	leaq	.LC15(%rip), %rax
	leaq	.LC17(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L77:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L78
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L79:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L80
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC13(%rip), %rax
	jle	.L72
	leaq	.LC12(%rip), %rax
	leaq	.LC14(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L72:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L73
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L74:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L76
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC56(%rip), %rax
	jle	.L202
	leaq	.LC55(%rip), %rax
	leaq	.LC57(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L202:
	pxor	%xmm0, %xmm0
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	divsd	%xmm0, %xmm0
	cmpl	$2, (%rbx)
	movsd	%xmm0, 32(%rsp)
	je	.L460
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L461
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L205
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC109(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L205:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC53(%rip), %rax
	jle	.L198
	leaq	.LC52(%rip), %rax
	leaq	.LC54(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L198:
	pxor	%xmm0, %xmm0
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	divsd	%xmm0, %xmm0
	cmpl	$2, (%rbx)
	movsd	%xmm0, 32(%rsp)
	je	.L462
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L463
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L201
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC108(%rip), %rdi
	movl	$24, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L201:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC25(%rip), %rax
	jle	.L105
	leaq	.LC24(%rip), %rax
	leaq	.LC26(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L105:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L106
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L107:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L464
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jne	.L111
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC94(%rip), %rdi
	movl	$17, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L111:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC22(%rip), %rax
	jle	.L100
	leaq	.LC21(%rip), %rax
	leaq	.LC23(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L100:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L101
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L102:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L465
	movl	(%rbx), %ebx
	testl	%ebx, %ebx
	jne	.L104
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC93(%rip), %rdi
	movl	$17, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L104:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC22(%rip), %rax
	jle	.L94
	leaq	.LC21(%rip), %rax
	leaq	.LC23(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L94:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L95
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L96:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L466
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L99
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC93(%rip), %rdi
	movl	$17, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L99:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC19(%rip), %rax
	jle	.L89
	leaq	.LC18(%rip), %rax
	leaq	.LC20(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L89:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L90
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L91:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L467
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L93
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC92(%rip), %rdi
	movl	$17, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L93:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC30(%rip), %rax
	jle	.L117
	leaq	.LC29(%rip), %rax
	leaq	.LC31(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L117:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L118
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L119:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L120
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC25(%rip), %rax
	jle	.L112
	leaq	.LC24(%rip), %rax
	leaq	.LC26(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L112:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L113
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L114:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L468
	movl	(%rbx), %r10d
	testl	%r10d, %r10d
	jne	.L116
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC94(%rip), %rdi
	movl	$17, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L116:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC10(%rip), %rax
	jle	.L68
	leaq	.LC9(%rip), %rax
	leaq	.LC11(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L68:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L69
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L70:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L71
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$99, %edi
	movsd	%xmm1, 16(%rsp)
	movl	$1, (%rsp)
	movsd	%xmm0, 24(%rsp)
	leaq	.LC7(%rip), %rax
	jle	.L64
	leaq	.LC6(%rip), %rax
	leaq	.LC8(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L64:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L469
	movq	.LC44(%rip), %rax
	movq	%rsp, %rdi
	movq	%rax, 32(%rsp)
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L470
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L67
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC91(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L67:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$99, %edi
	movl	$3, (%rsp)
	leaq	.LC39(%rip), %rax
	jle	.L146
	leaq	.LC38(%rip), %rax
	leaq	.LC40(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L146:
	movq	%rax, 8(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	mulsd	.LC100(%rip), %xmm1
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L147
	pxor	%xmm0, %xmm0
	movq	.LC44(%rip), %rax
	ucomisd	%xmm2, %xmm0
	movq	%rax, 32(%rsp)
	jbe	.L152
	movsd	.LC101(%rip), %xmm2
	movapd	%xmm1, %xmm4
	movsd	.LC102(%rip), %xmm3
	andpd	%xmm2, %xmm4
	movapd	%xmm1, %xmm0
	ucomisd	%xmm4, %xmm3
	jbe	.L150
	andnpd	%xmm1, %xmm2
	orpd	%xmm2, %xmm3
	addsd	%xmm3, %xmm0
	subsd	%xmm3, %xmm0
	orpd	%xmm2, %xmm0
.L150:
	ucomisd	%xmm1, %xmm0
	jp	.L379
	je	.L152
.L379:
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L152:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L157
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC36(%rip), %rax
	jle	.L141
	leaq	.LC35(%rip), %rax
	leaq	.LC37(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L141:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L142
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L143:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L471
	movl	(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L145
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC99(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L145:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC4(%rip), %rax
	jle	.L58
	leaq	.LC3(%rip), %rax
	leaq	.LC5(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L58:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L59
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L60:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L472
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L63
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC88(%rip), %rdi
	movl	$19, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L63:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC1(%rip), %rax
	jle	.L52
	leaq	.LC0(%rip), %rax
	leaq	.LC2(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L52:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L53
	movq	.LC44(%rip), %rax
	movq	%rax, 32(%rsp)
.L54:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L56
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L57
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC87(%rip), %rdi
	movl	$19, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L57:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC59(%rip), %rax
	jle	.L210
	leaq	.LC58(%rip), %rax
	leaq	.LC60(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L210:
	divsd	.LC86(%rip), %xmm2
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	cmpl	$2, (%rbx)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 32(%rsp)
	je	.L473
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L474
	movl	(%rbx), %r11d
	testl	%r11d, %r11d
	jne	.L213
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC111(%rip), %rdi
	movl	$18, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L213:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC59(%rip), %rax
	jle	.L206
	leaq	.LC58(%rip), %rax
	leaq	.LC60(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L206:
	pxor	%xmm0, %xmm0
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	divsd	%xmm0, %xmm0
	cmpl	$2, (%rbx)
	movsd	%xmm0, 32(%rsp)
	je	.L475
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L476
	movl	(%rbx), %ebx
	testl	%ebx, %ebx
	jne	.L209
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC110(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L209:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC50(%rip), %rax
	jle	.L193
	leaq	.LC49(%rip), %rax
	leaq	.LC51(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L193:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L194
	movsd	%xmm2, 32(%rsp)
.L195:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L477
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L197
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC107(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L197:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC47(%rip), %rax
	jle	.L188
	leaq	.LC46(%rip), %rax
	leaq	.LC48(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L188:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L189
	movq	$0x000000000, 32(%rsp)
.L190:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L478
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L192
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC106(%rip), %rdi
	movl	$19, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L192:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC39(%rip), %rax
	jle	.L165
	leaq	.LC38(%rip), %rax
	leaq	.LC40(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L165:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L166
	movq	$0x000000000, 32(%rsp)
.L167:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L479
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L169
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC104(%rip), %rdi
	movl	$25, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L169:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$99, %edi
	movl	$4, (%rsp)
	leaq	.LC39(%rip), %rax
	jle	.L158
	leaq	.LC38(%rip), %rax
	leaq	.LC40(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L158:
	pxor	%xmm0, %xmm0
	movq	%rax, 8(%rsp)
	movq	$0x000000000, 32(%rsp)
	ucomisd	%xmm2, %xmm0
	jbe	.L159
	mulsd	.LC100(%rip), %xmm1
	movsd	.LC101(%rip), %xmm2
	movsd	.LC102(%rip), %xmm3
	movapd	%xmm1, %xmm4
	movapd	%xmm1, %xmm0
	andpd	%xmm2, %xmm4
	ucomisd	%xmm4, %xmm3
	jbe	.L161
	andnpd	%xmm1, %xmm2
	orpd	%xmm2, %xmm3
	addsd	%xmm3, %xmm0
	subsd	%xmm3, %xmm0
	orpd	%xmm2, %xmm0
.L161:
	ucomisd	%xmm1, %xmm0
	jp	.L383
	je	.L159
.L383:
	movq	.LC103(%rip), %rax
	movq	%rax, 32(%rsp)
.L159:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$2, (%rax)
	je	.L480
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L164
	movsd	32(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	cmpl	$99, %edi
	movl	$2, (%rsp)
	leaq	.LC36(%rip), %rax
	jle	.L136
	leaq	.LC35(%rip), %rax
	leaq	.LC37(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L136:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L137
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L138:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L481
	movl	(%rbx), %esi
	testl	%esi, %esi
	jne	.L140
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC98(%rip), %rdi
	movl	$18, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L140:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	cmpl	$99, %edi
	movl	$1, (%rsp)
	leaq	.LC33(%rip), %rax
	jle	.L131
	leaq	.LC32(%rip), %rax
	leaq	.LC34(%rip), %rdx
	cmpl	$199, %edi
	cmovle	%rdx, %rax
.L131:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rbx
	movq	%rax, 8(%rsp)
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L132
	movq	.LC45(%rip), %rax
	movq	%rax, 32(%rsp)
.L133:
	movq	%rsp, %rdi
	call	matherr@PLT
	testl	%eax, %eax
	jne	.L482
	movl	(%rbx), %edi
	testl	%edi, %edi
	jne	.L135
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC97(%rip), %rdi
	movl	$18, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
.L135:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
.L2:
	.p2align 4,,10
	.p2align 3
.L227:
	movq	errno@gottpoff(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L268:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L269
.L270:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L137:
	movsd	.LC28(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L138
.L97:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L142:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L143
	.p2align 4,,10
	.p2align 3
.L61:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L147:
	pxor	%xmm0, %xmm0
	movq	.LC27(%rip), %rcx
	ucomisd	%xmm2, %xmm0
	movq	%rcx, 32(%rsp)
	jbe	.L153
	movsd	.LC101(%rip), %xmm2
	movapd	%xmm1, %xmm4
	movsd	.LC102(%rip), %xmm3
	andpd	%xmm2, %xmm4
	movapd	%xmm1, %xmm0
	ucomisd	%xmm4, %xmm3
	jbe	.L155
	andnpd	%xmm1, %xmm2
	orpd	%xmm2, %xmm3
	addsd	%xmm3, %xmm0
	subsd	%xmm3, %xmm0
	orpd	%xmm2, %xmm0
.L155:
	ucomisd	%xmm1, %xmm0
	jp	.L381
	je	.L153
.L381:
	movq	.LC28(%rip), %rsi
	movq	%rsi, 32(%rsp)
.L153:
	cmpl	$2, %eax
	jne	.L152
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L113:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L61
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L90:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L61
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L95:
	movsd	.LC28(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L97
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L101:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L61
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L118:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L119
	.p2align 4,,10
	.p2align 3
.L75:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L257:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L258
.L259:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L264:
	movsd	.LC28(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L265
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L176:
	pxor	%xmm0, %xmm0
	cmpl	$2, %eax
	divsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rsp)
	jne	.L177
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L181:
	ucomisd	.LC86(%rip), %xmm2
	movsd	.LC27(%rip), %xmm0
	ja	.L185
	movsd	.LC28(%rip), %xmm0
.L185:
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	jne	.L184
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L122:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L75
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L127:
	movsd	.LC28(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L97
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L171:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L75
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L132:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L61
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L251:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L259
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L166:
	movsd	.LC28(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L97
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L189:
	pxor	%xmm0, %xmm0
	cmpl	$2, %eax
	divsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rsp)
	jne	.L190
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L194:
	pxor	%xmm0, %xmm0
	cmpl	$2, %eax
	divsd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rsp)
	jne	.L195
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L73:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L75
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L53:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L61
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L59:
	movsd	.LC85(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L61
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L69:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L75
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L85:
	movsd	.LC28(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L97
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L106:
	pxor	%xmm0, %xmm0
	ucomisd	%xmm2, %xmm0
	ja	.L483
	movsd	.LC28(%rip), %xmm0
.L108:
	cmpl	$2, %edx
	movsd	%xmm0, 32(%rsp)
	jne	.L107
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L78:
	movsd	.LC27(%rip), %xmm0
	cmpl	$2, %eax
	movsd	%xmm0, 32(%rsp)
	je	.L75
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L459:
	movq	errno@gottpoff(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L462:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
.L460:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
.L473:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
.L445:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
.L442:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
.L475:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
.L453:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
.L454:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
.L480:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L470:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L164:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L157:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L80:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L83:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L71:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L244:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L271:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
.L253:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L76:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L187:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L221:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L218:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L120:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L260:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L266:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	jmp	.L1
.L262:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L255:
	movq	errno@gottpoff(%rip), %rax
	movsd	32(%rsp), %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L1
.L451:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L452:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L455:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L450:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L449:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L446:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L478:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L448:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L447:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L463:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L444:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L482:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L481:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L467:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L465:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L466:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L458:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L474:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L471:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L472:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L457:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L461:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L464:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L477:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L456:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L468:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L479:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L476:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L443:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L56:
	movsd	32(%rsp), %xmm0
	jmp	.L1
.L483:
	cvttsd2si	%xmm2, %eax
	movsd	.LC28(%rip), %xmm0
	testb	$1, %al
	je	.L108
	movsd	.LC27(%rip), %xmm0
	jmp	.L108
.L248:
	movq	stderr@GOTPCREL(%rip), %rax
	leaq	.LC115(%rip), %rdi
	movl	$19, %edx
	movl	$1, %esi
	movq	(%rax), %rcx
	call	fwrite@PLT
	movsd	.LC27(%rip), %xmm0
	jmp	.L249
.L469:
	leaq	__PRETTY_FUNCTION__.5451(%rip), %rcx
	leaq	.LC89(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	movl	$166, %edx
	call	__assert_fail@PLT
	.size	__kernel_standard, .-__kernel_standard
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.5451, @object
	.size	__PRETTY_FUNCTION__.5451, 18
__PRETTY_FUNCTION__.5451:
	.string	"__kernel_standard"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC27:
	.long	0
	.long	2146435072
	.align 8
.LC28:
	.long	0
	.long	-1048576
	.align 8
.LC44:
	.long	3758096384
	.long	1206910975
	.align 8
.LC45:
	.long	3758096384
	.long	-940572673
	.align 8
.LC85:
	.long	0
	.long	2146959360
	.align 8
.LC86:
	.long	0
	.long	0
	.align 8
.LC100:
	.long	0
	.long	1071644672
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC101:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC102:
	.long	0
	.long	1127219200
	.align 8
.LC103:
	.long	0
	.long	-2147483648
	.section	.rodata.cst16
	.align 16
.LC112:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC114:
	.long	0
	.long	2146435072
	.long	0
	.long	0
