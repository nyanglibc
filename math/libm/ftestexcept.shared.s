	.text
	.p2align 4,,15
	.globl	__GI_fetestexcept
	.hidden	__GI_fetestexcept
	.type	__GI_fetestexcept, @function
__GI_fetestexcept:
#APP
# 28 "../sysdeps/x86_64/fpu/ftestexcept.c" 1
	fnstsw -8(%rsp)
stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-8(%rsp), %eax
	orl	-4(%rsp), %eax
	andl	%edi, %eax
	andl	$61, %eax
	ret
	.size	__GI_fetestexcept, .-__GI_fetestexcept
	.globl	fetestexcept
	.set	fetestexcept,__GI_fetestexcept
