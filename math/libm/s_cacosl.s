	.text
	.p2align 4,,15
	.globl	__cacosl
	.type	__cacosl, @function
__cacosl:
	subq	$24, %rsp
	fldt	32(%rsp)
	fldt	48(%rsp)
	fld	%st(1)
	fabs
	fucomi	%st(0), %st
	jp	.L11
	fldt	.LC1(%rip)
	fxch	%st(1)
	movl	$1, %eax
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L27
	fldt	.LC2(%rip)
	fxch	%st(1)
	movl	$4, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L2
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L14
	movl	$2, %eax
	jne	.L14
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L27:
	fstp	%st(0)
.L2:
	fld	%st(0)
	fabs
	fucomi	%st(0), %st
	jp	.L28
.L21:
	fldt	.LC1(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L22
	fstp	%st(0)
	pxor	%xmm0, %xmm0
	movss	%xmm0, 12(%rsp)
.L4:
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__casinl@PLT
	addq	$32, %rsp
	fldt	.LC3(%rip)
	fsubp	%st, %st(1)
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	jp	.L29
	fcmove	%st(1), %st
	fstp	%st(1)
	jmp	.L8
.L29:
	fstp	%st(1)
.L8:
	fxch	%st(1)
	addq	$24, %rsp
	fchs
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L5
	fldz
	fxch	%st(1)
	pxor	%xmm1, %xmm1
	fucomi	%st(1), %st
	fstp	%st(1)
	movss	%xmm1, 12(%rsp)
	jp	.L5
	jne	.L5
	cmpl	$1, %eax
	jle	.L4
	cmpl	$2, %eax
	je	.L4
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1, %eax
	jle	.L26
.L7:
	fchs
	fxch	%st(1)
	subq	$32, %rsp
	movl	$1, %edi
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_casinhl@PLT
	addq	$32, %rsp
	addq	$24, %rsp
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	fld	%st(0)
	movl	$3, %eax
	fabs
	fucomi	%st(0), %st
	jnp	.L21
	fstp	%st(0)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L28:
	fstp	%st(0)
.L25:
	pxor	%xmm3, %xmm3
	movss	%xmm3, 12(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L26:
	pxor	%xmm2, %xmm2
	movss	%xmm2, 12(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	fstp	%st(0)
	xorl	%eax, %eax
	jmp	.L2
	.size	__cacosl, .-__cacosl
	.weak	cacosf64x
	.set	cacosf64x,__cacosl
	.weak	cacosl
	.set	cacosl,__cacosl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC2:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC3:
	.long	560513589
	.long	3373259426
	.long	16383
	.long	0
