	.text
#APP
	.symver __ieee754_acosh,__acosh_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_acosh
	.type	__ieee754_acosh, @function
__ieee754_acosh:
	movq	%xmm0, %rax
	movabsq	$4611686018427387904, %rdx
	cmpq	%rdx, %rax
	jle	.L2
	movabsq	$4733283208366391295, %rdx
	cmpq	%rdx, %rax
	jg	.L12
	movapd	%xmm0, %xmm1
	movsd	.LC2(%rip), %xmm2
	movapd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm1
	addsd	%xmm0, %xmm3
	subsd	%xmm2, %xmm1
	sqrtsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	movapd	%xmm3, %xmm0
	jmp	__ieee754_log@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	movabsq	$4607182418800017408, %rdx
	cmpq	%rdx, %rax
	jle	.L6
	subsd	.LC2(%rip), %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	addsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	sqrtsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	jmp	__log1p@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	movabsq	$9218868437227405311, %rdx
	cmpq	%rdx, %rax
	jle	.L4
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
.L9:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	pxor	%xmm1, %xmm1
	je	.L9
	subsd	%xmm0, %xmm0
	movapd	%xmm0, %xmm1
	divsd	%xmm0, %xmm1
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L4:
	subq	$8, %rsp
	call	__ieee754_log@PLT
	movsd	.LC1(%rip), %xmm1
	addq	$8, %rsp
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.size	__ieee754_acosh, .-__ieee754_acosh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4277811695
	.long	1072049730
	.align 8
.LC2:
	.long	0
	.long	1072693248
