	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__lttf2
	.globl	__multf3
	.globl	__addtf3
	.globl	__divtf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__csqrtf128
	.type	__csqrtf128, @function
__csqrtf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	movdqa	96(%rsp), %xmm6
	movdqa	.LC2(%rip), %xmm3
	movdqa	112(%rsp), %xmm7
	pand	%xmm6, %xmm3
	movdqa	.LC2(%rip), %xmm4
	pand	%xmm7, %xmm4
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm6, 48(%rsp)
	movaps	%xmm7, 16(%rsp)
	movaps	%xmm4, (%rsp)
	movaps	%xmm3, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC3(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L111
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L112
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L113
.L7:
	movdqa	.LC5(%rip), %xmm1
	movdqa	112(%rsp), %xmm0
.L14:
	movq	%rbx, %rax
	movaps	%xmm1, (%rbx)
	movaps	%xmm0, 16(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	movdqa	.LC4(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L5
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L108
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L7
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L48
	pxor	%xmm1, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L48
.L55:
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	96(%rsp), %xmm0
	js	.L114
	call	__ieee754_sqrtf128@PLT
	movaps	%xmm0, (%rsp)
	pxor	%xmm0, %xmm0
	movdqa	112(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm2
	pand	%xmm2, %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	(%rsp), %xmm5
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L108
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L7
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L48
	pxor	%xmm1, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L55
.L48:
	movdqa	.LC9(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L115
	movdqa	.LC9(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L94
	movdqa	.LC10(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L116
	pxor	%xmm2, %xmm2
	movaps	%xmm2, (%rsp)
.L27:
	movl	$-2, %edi
	movdqa	112(%rsp), %xmm0
	call	__scalbnf128@PLT
.L22:
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_hypotf128@PLT
	movdqa	16(%rsp), %xmm5
	pxor	%xmm1, %xmm1
	pand	.LC2(%rip), %xmm5
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	movaps	%xmm5, 48(%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L117
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC11(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	js	.L118
	call	__divtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movl	$1, %ebp
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
.L34:
	movl	%ebp, %edi
	movdqa	(%rsp), %xmm0
	call	__scalbnf128@PLT
	movaps	%xmm0, (%rsp)
	movl	%ebp, %edi
	movdqa	32(%rsp), %xmm0
	call	__scalbnf128@PLT
	movaps	%xmm0, 32(%rsp)
.L33:
	movdqa	(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L35
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L35:
	movdqa	32(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L37
	movdqa	32(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L37:
	movdqa	16(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L113:
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	pxor	%xmm1, %xmm1
	js	.L119
	movdqa	96(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L12
	.p2align 4,,10
	.p2align 3
.L57:
	pxor	%xmm6, %xmm6
	movaps	%xmm6, (%rsp)
.L11:
	movdqa	112(%rsp), %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L5:
	movdqa	(%rsp), %xmm5
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L108
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L7
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L51
	pxor	%xmm1, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L92
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm2
.L19:
	movdqa	112(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L119:
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L57
.L12:
	pxor	%xmm0, %xmm0
	movdqa	112(%rsp), %xmm1
	call	__copysignf128@PLT
.L44:
	movdqa	96(%rsp), %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L94:
	movdqa	.LC7(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L97
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L120
.L97:
	xorl	%ebp, %ebp
.L28:
	movdqa	16(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__ieee754_hypotf128@PLT
	pxor	%xmm1, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jle	.L121
	movdqa	%xmm2, %xmm0
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
.L43:
	testl	%ebp, %ebp
	je	.L33
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L115:
	movdqa	96(%rsp), %xmm0
	movl	$-2, %edi
	call	__scalbnf128@PLT
	movaps	%xmm0, (%rsp)
	movl	$-2, %edi
	movdqa	112(%rsp), %xmm0
	call	__scalbnf128@PLT
	jmp	.L22
.L121:
	movdqa	%xmm2, %xmm0
	movdqa	48(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	pand	.LC2(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L108
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L7
.L108:
	movdqa	.LC0(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L117:
	movdqa	(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC11(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L122
	movdqa	32(%rsp), %xmm1
	movl	$1, %ebp
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	pand	.LC2(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L116:
	movdqa	96(%rsp), %xmm0
	movl	$-2, %edi
	call	__scalbnf128@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L120:
	movdqa	96(%rsp), %xmm0
	movl	$114, %edi
	movl	$-57, %ebp
	call	__scalbnf128@PLT
	movaps	%xmm0, 48(%rsp)
	movl	$114, %edi
	movdqa	112(%rsp), %xmm0
	call	__scalbnf128@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L92:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L118:
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
	movl	$1, %edi
	movdqa	(%rsp), %xmm0
	call	__scalbnf128@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L114:
	pxor	.LC6(%rip), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	112(%rsp), %xmm1
	call	__copysignf128@PLT
	pxor	%xmm1, %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L112:
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L60
	movdqa	.LC0(%rip), %xmm0
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L122:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC2(%rip), %xmm5
	movl	$1, %edi
	pand	%xmm0, %xmm5
	movdqa	32(%rsp), %xmm0
	movaps	%xmm5, (%rsp)
	call	__scalbnf128@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L33
.L60:
	movdqa	.LC0(%rip), %xmm7
	movaps	%xmm7, (%rsp)
	jmp	.L11
	.size	__csqrtf128, .-__csqrtf128
	.weak	csqrtf128
	.set	csqrtf128,__csqrtf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	131072
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC9:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147287039
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	196608
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	1073676288
