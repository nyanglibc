	.text
	.p2align 4,,15
	.globl	__asinhl
	.type	__asinhl, @function
__asinhl:
	subq	$8, %rsp
	movq	24(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16349, %ax
	jle	.L20
	cmpw	$16416, %ax
	jg	.L17
	fldt	16(%rsp)
	fabs
.L6:
	fldt	16(%rsp)
	cmpw	$16384, %ax
	fmul	%st(0), %st
	fld1
	fld	%st(1)
	fadd	%st(1), %st
	fsqrt
	jg	.L21
	faddp	%st, %st(1)
	subq	$16, %rsp
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	popq	%rax
	popq	%rdx
.L8:
	fldt	16(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fabs
	je	.L1
	fchs
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	fstp	%st(1)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	fstp	%st(2)
	fxch	%st(1)
	fadd	%st(2), %st
	subq	$16, %rsp
	fdivrp	%st, %st(1)
	fxch	%st(1)
	fadd	%st(0), %st
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	popq	%rcx
	popq	%rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	fldt	16(%rsp)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	jbe	.L3
	fldt	16(%rsp)
	fmul	%st(0), %st
	fstp	%st(0)
.L3:
	fldt	.LC1(%rip)
	fldt	16(%rsp)
	fadd	%st, %st(1)
	fld1
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	ja	.L23
	fstp	%st(0)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	cmpw	$32767, %ax
	fldt	16(%rsp)
	je	.L22
	subq	$16, %rsp
	fabs
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	fldln2
	popq	%rdi
	faddp	%st, %st(1)
	popq	%r8
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	fadd	%st(0), %st
	jmp	.L1
	.size	__asinhl, .-__asinhl
	.weak	asinhf64x
	.set	asinhf64x,__asinhl
	.weak	asinhl
	.set	asinhl,__asinhl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC1:
	.long	1496818881
	.long	2928804903
	.long	32660
	.long	0
