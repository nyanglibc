	.text
	.p2align 4,,15
	.globl	__GI___fesetenv
	.hidden	__GI___fesetenv
	.type	__GI___fesetenv, @function
__GI___fesetenv:
#APP
# 38 "../sysdeps/x86_64/fpu/fesetenv.c" 1
	fnstenv -40(%rsp)
stmxcsr -12(%rsp)
# 0 "" 2
#NO_APP
	cmpq	$-1, %rdi
	movzwl	-40(%rsp), %eax
	je	.L6
	movzwl	-36(%rsp), %ecx
	andl	$-64, %ecx
	cmpq	$-2, %rdi
	je	.L7
	movzwl	(%rdi), %edx
	andw	$-3904, %ax
	andw	$3903, %dx
	orl	%edx, %eax
	movzwl	18(%rdi), %edx
	movw	%ax, -40(%rsp)
	movzwl	4(%rdi), %eax
	andw	$2047, %dx
	andl	$63, %eax
	orl	%ecx, %eax
	movw	%ax, -36(%rsp)
	movl	12(%rdi), %eax
	movl	%eax, -28(%rsp)
	movzwl	16(%rdi), %eax
	movw	%ax, -24(%rsp)
	movzwl	-22(%rsp), %eax
	andw	$-2048, %ax
	orl	%edx, %eax
	movw	%ax, -22(%rsp)
	movl	20(%rdi), %eax
	movl	%eax, -20(%rsp)
	movzwl	24(%rdi), %eax
	movw	%ax, -16(%rsp)
	movl	28(%rdi), %eax
	movl	%eax, -12(%rsp)
.L3:
#APP
# 106 "../sysdeps/x86_64/fpu/fesetenv.c" 1
	fldenv -40(%rsp)
ldmxcsr -12(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	andb	$-13, %ah
	xorl	%esi, %esi
	xorl	%edi, %edi
	orw	$831, %ax
	movl	$8064, %r8d
	andw	$-64, -36(%rsp)
	movw	%ax, -40(%rsp)
	movl	$0, -28(%rsp)
	movw	%si, -24(%rsp)
	andw	$-2048, -22(%rsp)
	movl	$0, -20(%rsp)
	movw	%di, -16(%rsp)
	movw	%r8w, -12(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	andw	$-3134, %ax
	movw	%cx, -36(%rsp)
	xorl	%edx, %edx
	orw	$770, %ax
	movl	$256, %ecx
	movl	$0, -28(%rsp)
	movw	%ax, -40(%rsp)
	xorl	%eax, %eax
	andw	$-2048, -22(%rsp)
	movw	%ax, -24(%rsp)
	movl	$0, -20(%rsp)
	movw	%dx, -16(%rsp)
	movw	%cx, -12(%rsp)
	jmp	.L3
	.size	__GI___fesetenv, .-__GI___fesetenv
	.globl	__fesetenv
	.set	__fesetenv,__GI___fesetenv
	.weak	__GI_fesetenv
	.hidden	__GI_fesetenv
	.set	__GI_fesetenv,__fesetenv
	.weak	fesetenv
	.set	fesetenv,__GI_fesetenv
