	.text
#APP
	.symver __ieee754_exp2f128,__exp2f128_finite@GLIBC_2.26
	.globl	__unordtf2
	.globl	__getf2
	.globl	__lttf2
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__subtf3
	.globl	__addtf3
	.globl	__multf3
	.globl	__letf2
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_exp2f128
	.type	__ieee754_exp2f128, @function
__ieee754_exp2f128:
	pushq	%rbx
	subq	$16, %rsp
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC1(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L2
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L3
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L3
	movdqa	(%rsp), %xmm0
	call	__fixtfsi@PLT
	movl	%eax, %edi
	movl	%eax, %ebx
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	pand	.LC3(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	js	.L18
	movdqa	%xmm2, %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_expf128@PLT
	movl	%ebx, %edi
	call	__scalbnf128@PLT
	movdqa	%xmm0, %xmm2
.L6:
	movdqa	%xmm2, %xmm0
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm2, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jns	.L1
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
.L1:
	addq	$16, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	%xmm2, %xmm0
	movdqa	.LC5(%rip), %xmm1
	call	__addtf3@PLT
	movl	%ebx, %edi
	call	__scalbnf128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	addq	$16, %rsp
	popq	%rbx
	movdqa	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movdqa	(%rsp), %xmm2
	pand	.LC3(%rip), %xmm2
	movdqa	.LC8(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L13
	movdqa	(%rsp), %xmm2
	movdqa	.LC8(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	pxor	%xmm2, %xmm2
	jg	.L1
.L13:
	movdqa	.LC7(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.size	__ieee754_exp2f128, .-__ieee754_exp2f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1074593792
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	-1072889412
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1066205184
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC6:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC8:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
