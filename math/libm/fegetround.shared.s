	.text
	.p2align 4,,15
	.globl	__GI___fegetround
	.hidden	__GI___fegetround
	.type	__GI___fegetround, @function
__GI___fegetround:
#APP
# 29 "../sysdeps/x86_64/fpu/fegetround.c" 1
	fnstcw -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-4(%rsp), %eax
	andl	$3072, %eax
	ret
	.size	__GI___fegetround, .-__GI___fegetround
	.globl	__fegetround
	.set	__fegetround,__GI___fegetround
	.weak	__GI_fegetround
	.hidden	__GI_fegetround
	.set	__GI_fegetround,__fegetround
	.weak	fegetround
	.set	fegetround,__GI_fegetround
