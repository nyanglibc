	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__eqtf2
	.globl	__letf2
	.globl	__lttf2
	.p2align 4,,15
	.globl	__tgammaf128
	.type	__tgammaf128, @function
__tgammaf128:
	subq	$72, %rsp
	leaq	60(%rsp), %rdi
	movaps	%xmm0, 32(%rsp)
	call	__ieee754_gammaf128_r@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L2
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L2
.L3:
	movl	60(%rsp), %eax
	testl	%eax, %eax
	jns	.L1
	movdqa	(%rsp), %xmm3
	pxor	.LC3(%rip), %xmm3
	movaps	%xmm3, (%rsp)
.L1:
	movdqa	(%rsp), %xmm0
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	32(%rsp), %xmm4
	pand	.LC0(%rip), %xmm4
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L15
	movdqa	.LC1(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L26
.L15:
	movdqa	.LC1(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L3
	movdqa	.LC1(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L3
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L3
	movdqa	32(%rsp), %xmm0
	call	__floorf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L10
.L14:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L26:
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L27
.L10:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	32(%rsp), %xmm0
	call	__floorf128@PLT
	movdqa	32(%rsp), %xmm1
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L10
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L10
	jmp	.L14
	.size	__tgammaf128, .-__tgammaf128
	.weak	tgammaf128
	.set	tgammaf128,__tgammaf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
