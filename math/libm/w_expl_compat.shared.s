	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___expl
	.hidden	__GI___expl
	.type	__GI___expl, @function
__GI___expl:
	subq	$8, %rsp
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__ieee754_expl@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L1
	je	.L2
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fldt	16(%rsp)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	fstp	%st(0)
	fldt	16(%rsp)
	xorl	%edi, %edi
	fxam
	fnstsw	%ax
	testb	$2, %ah
	setne	%dil
	subq	$16, %rsp
	fstpt	(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	addl	$206, %edi
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L1
	.size	__GI___expl, .-__GI___expl
	.globl	__expl
	.set	__expl,__GI___expl
	.weak	expf64x
	.set	expf64x,__expl
	.weak	expl
	.set	expl,__expl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
