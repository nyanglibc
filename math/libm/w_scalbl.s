	.text
	.p2align 4,,15
	.globl	__scalbl
	.type	__scalbl, @function
__scalbl:
	subq	$8, %rsp
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__ieee754_scalbl@PLT
	fld	%st(0)
	addq	$32, %rsp
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L1
	je	.L2
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fucomi	%st(0), %st
	jp	.L10
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L6
	fldt	16(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L1
.L7:
	fldt	32(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	fldz
	fldt	16(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L7
	jne	.L7
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	fldt	16(%rsp)
	fldt	32(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__scalbl, .-__scalbl
	.weak	scalbf64x
	.set	scalbf64x,__scalbl
	.weak	scalbl
	.set	scalbl,__scalbl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
