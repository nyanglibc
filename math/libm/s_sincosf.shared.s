	.text
	.p2align 4,,15
	.globl	__sincosf
	.type	__sincosf, @function
__sincosf:
	movd	%xmm0, %eax
	pxor	%xmm1, %xmm1
	movd	%xmm0, %edx
	shrl	$20, %eax
	andl	$2047, %eax
	cvtss2sd	%xmm0, %xmm1
	cmpl	$1011, %eax
	ja	.L2
	movapd	%xmm1, %xmm2
	cmpl	$919, %eax
	mulsd	%xmm1, %xmm2
	jbe	.L22
	movapd	%xmm1, %xmm4
	movapd	%xmm2, %xmm3
	movapd	64+__sincosf_table(%rip), %xmm0
	unpcklpd	%xmm2, %xmm4
	mulsd	56+__sincosf_table(%rip), %xmm2
	unpcklpd	%xmm3, %xmm3
	mulpd	%xmm3, %xmm4
	addsd	48+__sincosf_table(%rip), %xmm2
	mulpd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm1
	addpd	%xmm0, %xmm1
	movapd	96+__sincosf_table(%rip), %xmm0
	mulpd	%xmm3, %xmm0
	mulpd	%xmm4, %xmm3
	addpd	80+__sincosf_table(%rip), %xmm0
	mulpd	%xmm3, %xmm0
.L20:
	addpd	%xmm0, %xmm1
	cvtpd2ps	%xmm1, %xmm1
	movss	%xmm1, (%rdi)
	shufps	$85, %xmm1, %xmm1
	movss	%xmm1, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1070, %eax
	jbe	.L23
	cmpl	$2039, %eax
	ja	.L9
	movd	%xmm0, %ecx
	movd	%xmm0, %r8d
	leaq	__inv_pio4(%rip), %r11
	shrl	$31, %edx
	andl	$8388607, %ecx
	shrl	$26, %r8d
	movl	%ecx, %eax
	movd	%xmm0, %ecx
	andl	$15, %r8d
	leaq	(%r11,%r8,4), %r9
	orl	$8388608, %eax
	shrl	$23, %ecx
	pxor	%xmm0, %xmm0
	andl	$7, %ecx
	sall	%cl, %eax
	movl	%eax, %r10d
	movl	32(%r9), %eax
	movq	%r10, %rcx
	imull	(%r11,%r8,4), %ecx
	imulq	%r10, %rax
	salq	$32, %rcx
	shrq	$32, %rax
	orq	%rax, %rcx
	movl	16(%r9), %eax
	movabsq	$-4611686018427387904, %r9
	imulq	%r10, %rax
	addq	%rcx, %rax
	movabsq	$2305843009213693952, %rcx
	addq	%rax, %rcx
	movq	%rcx, %r8
	andq	%r9, %rcx
	subq	%rcx, %rax
	shrq	$62, %r8
	leaq	__sincosf_table(%rip), %rcx
	cvtsi2sdq	%rax, %xmm0
	addl	%r8d, %edx
	movl	%edx, %r9d
	leaq	112(%rcx), %rax
	andl	$3, %r9d
	andl	$2, %edx
	cmove	%rcx, %rax
	andl	$1, %r8d
	movapd	96(%rax), %xmm4
	mulsd	.LC1(%rip), %xmm0
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	(%rcx,%r9,8), %xmm0
	movapd	%xmm2, %xmm1
	unpcklpd	%xmm1, %xmm1
	movapd	%xmm0, %xmm3
	mulpd	%xmm1, %xmm4
	unpcklpd	%xmm2, %xmm3
	addpd	80(%rax), %xmm4
	mulpd	%xmm1, %xmm3
	je	.L11
	movq	%rdi, %rdx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
.L11:
	mulsd	56(%rax), %xmm2
	mulpd	%xmm3, %xmm1
	addsd	48(%rax), %xmm2
	mulpd	%xmm4, %xmm1
	unpcklpd	%xmm2, %xmm0
	movapd	64(%rax), %xmm2
	mulpd	%xmm3, %xmm2
	addpd	%xmm2, %xmm0
	addpd	%xmm1, %xmm0
	cvtpd2ps	%xmm0, %xmm0
	movss	%xmm0, (%rdi)
	shufps	$85, %xmm0, %xmm0
	movss	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movsd	32+__sincosf_table(%rip), %xmm0
	leaq	__sincosf_table(%rip), %rcx
	mulsd	%xmm1, %xmm0
	leaq	112(%rcx), %rdx
	cvttsd2si	%xmm0, %eax
	pxor	%xmm0, %xmm0
	addl	$8388608, %eax
	sarl	$24, %eax
	cvtsi2sd	%eax, %xmm0
	movl	%eax, %r8d
	andl	$3, %r8d
	testb	$2, %al
	cmove	%rcx, %rdx
	testb	$1, %al
	movapd	96(%rdx), %xmm4
	mulsd	40+__sincosf_table(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	(%rcx,%r8,8), %xmm1
	movapd	%xmm2, %xmm0
	unpcklpd	%xmm0, %xmm0
	movapd	%xmm1, %xmm3
	mulpd	%xmm0, %xmm4
	unpcklpd	%xmm2, %xmm3
	addpd	80(%rdx), %xmm4
	mulpd	%xmm0, %xmm3
	je	.L8
	movq	%rdi, %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
.L8:
	mulsd	56(%rdx), %xmm2
	mulpd	%xmm3, %xmm0
	addsd	48(%rdx), %xmm2
	mulpd	%xmm4, %xmm0
	unpcklpd	%xmm2, %xmm1
	movapd	64(%rdx), %xmm2
	mulpd	%xmm3, %xmm2
	addpd	%xmm2, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$7, %eax
	jbe	.L24
.L4:
	movss	%xmm0, (%rdi)
	movl	$0x3f800000, (%rsi)
	ret
.L24:
	cvtsd2ss	%xmm2, %xmm2
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	movaps	%xmm0, %xmm1
	subss	%xmm0, %xmm1
	addss	%xmm0, %xmm0
	movss	%xmm1, (%rsi)
	movss	%xmm1, (%rdi)
	jmp	__math_invalidf
	.size	__sincosf, .-__sincosf
	.weak	sincosf32
	.set	sincosf32,__sincosf
	.weak	sincosf
	.set	sincosf,__sincosf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1413754136
	.long	1008280059
	.hidden	__math_invalidf
	.hidden	__inv_pio4
	.hidden	__sincosf_table
