	.text
	.p2align 4,,15
	.globl	__isnanf128
	.type	__isnanf128, @function
__isnanf128:
	movaps	%xmm0, -24(%rsp)
	movq	-24(%rsp), %rax
	movq	-16(%rsp), %rcx
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rcx, %rax
	shrq	$63, %rdx
	orq	%rax, %rdx
	movabsq	$9223090561878065152, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__isnanf128, .-__isnanf128
	.weak	isnanf128_do_not_use
	.set	isnanf128_do_not_use,__isnanf128
