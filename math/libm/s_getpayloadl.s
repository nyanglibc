	.text
	.p2align 4,,15
	.globl	__getpayloadl
	.type	__getpayloadl, @function
__getpayloadl:
	movl	8(%rdi), %edx
	movq	(%rdi), %rax
	movq	%rdx, %rcx
	andw	$32767, %cx
	cmpw	$32767, %cx
	je	.L6
.L4:
	fld1
	fchs
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rax, %rdx
	shrq	$32, %rax
	movl	%eax, %ecx
	andl	$2147483647, %ecx
	orl	%edx, %ecx
	je	.L4
	andl	$1073741823, %eax
	movl	%edx, %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movq	%rax, -16(%rsp)
	fildq	-16(%rsp)
	ret
	.size	__getpayloadl, .-__getpayloadl
	.weak	getpayloadf64x
	.set	getpayloadf64x,__getpayloadl
	.weak	getpayloadl
	.set	getpayloadl,__getpayloadl
