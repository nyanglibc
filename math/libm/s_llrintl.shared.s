.text
.globl __llrintl
.type __llrintl,@function
.align 1<<4
__llrintl:
 fldt 8(%rsp)
 fistpll -8(%rsp)
 fwait
 movq -8(%rsp),%rax
 ret
.size __llrintl,.-__llrintl
.weak llrintl
llrintl = __llrintl
.weak llrintf64x
llrintf64x = __llrintl
.globl __lrintl
.set __lrintl,__llrintl
.weak lrintl
lrintl = __llrintl
.weak lrintf64x
lrintf64x = __llrintl
