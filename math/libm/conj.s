	.text
	.p2align 4,,15
	.globl	__conj
	.type	__conj, @function
__conj:
	xorpd	.LC0(%rip), %xmm1
	ret
	.size	__conj, .-__conj
	.weak	conjf32x
	.set	conjf32x,__conj
	.weak	conjf64
	.set	conjf64,__conj
	.weak	conj
	.set	conj,__conj
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
