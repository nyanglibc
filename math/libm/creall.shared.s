	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__creall
	.type	__creall, @function
__creall:
	fldt	8(%rsp)
	ret
	.size	__creall, .-__creall
	.weak	crealf64x
	.set	crealf64x,__creall
	.weak	creall
	.set	creall,__creall
