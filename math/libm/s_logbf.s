	.text
	.p2align 4,,15
	.globl	__logbf
	.type	__logbf, @function
__logbf:
	movd	%xmm0, %eax
	andl	$2147483647, %eax
	movl	%eax, %edx
	je	.L7
	cmpl	$2139095039, %eax
	jg	.L8
	sarl	$23, %eax
	testl	%eax, %eax
	je	.L9
.L5:
	pxor	%xmm0, %xmm0
	subl	$127, %eax
	cvtsi2ss	%eax, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	mulss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movaps	%xmm0, %xmm1
	movss	.LC1(%rip), %xmm0
	andps	.LC0(%rip), %xmm1
	divss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	bsrl	%edx, %edx
	movl	$9, %eax
	xorl	$31, %edx
	subl	%edx, %eax
	jmp	.L5
	.size	__logbf, .-__logbf
	.weak	logbf32
	.set	logbf32,__logbf
	.weak	logbf
	.set	logbf,__logbf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	3212836864
