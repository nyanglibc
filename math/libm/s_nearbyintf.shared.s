	.text
	.p2align 4,,15
	.globl	__nearbyintf
	.type	__nearbyintf, @function
__nearbyintf:
	movd	%xmm0, %edx
	movd	%xmm0, %ecx
	sarl	$23, %edx
	movzbl	%dl, %edx
	subl	$127, %edx
	cmpl	$22, %edx
	jg	.L2
#APP
# 33 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	shrl	$31, %ecx
	testl	%edx, %edx
	movl	-44(%rsp), %edx
	movl	%edx, -12(%rsp)
	js	.L7
	andl	$-8128, %edx
	orl	$8064, %edx
	movl	%edx, -44(%rsp)
#APP
# 36 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	leaq	TWO23.6265(%rip), %rdx
	movss	%xmm0, -60(%rsp)
	movss	(%rdx,%rcx,4), %xmm1
	movss	-60(%rsp), %xmm0
	addss	%xmm1, %xmm0
	subss	%xmm1, %xmm0
#APP
# 122 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -12(%rsp)
# 0 "" 2
#NO_APP
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movss	%xmm0, -60(%rsp)
	addl	$-128, %edx
	movss	-60(%rsp), %xmm0
	jne	.L1
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andl	$-8128, %edx
	orl	$8064, %edx
	movl	%edx, -44(%rsp)
#APP
# 36 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	leaq	TWO23.6265(%rip), %rdx
	movslq	%ecx, %rsi
	movss	(%rdx,%rsi,4), %xmm1
	addss	%xmm1, %xmm0
	subss	%xmm1, %xmm0
#APP
# 122 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -12(%rsp)
# 0 "" 2
#NO_APP
	movd	%xmm0, %eax
	sall	$31, %ecx
	andl	$2147483647, %eax
	orl	%ecx, %eax
	movl	%eax, -60(%rsp)
	movd	-60(%rsp), %xmm0
	ret
	.size	__nearbyintf, .-__nearbyintf
	.weak	nearbyintf32
	.set	nearbyintf32,__nearbyintf
	.weak	nearbyintf
	.set	nearbyintf,__nearbyintf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	TWO23.6265, @object
	.size	TWO23.6265, 8
TWO23.6265:
	.long	1258291200
	.long	3405774848
