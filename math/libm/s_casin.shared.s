	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__casin
	.type	__casin, @function
__casin:
	ucomisd	%xmm0, %xmm0
	jp	.L2
	ucomisd	%xmm1, %xmm1
	jp	.L2
	subq	$24, %rsp
	movapd	%xmm1, %xmm3
	movq	.LC5(%rip), %xmm2
	movapd	%xmm0, %xmm1
	xorpd	%xmm2, %xmm3
	movaps	%xmm2, (%rsp)
	movapd	%xmm3, %xmm0
	call	__casinh@PLT
	movapd	(%rsp), %xmm2
	addq	$24, %rsp
	xorpd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	.LC1(%rip), %xmm0
	jp	.L10
	je	.L13
.L10:
	movq	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm3
	andpd	%xmm2, %xmm0
	ucomisd	%xmm3, %xmm0
	movsd	.LC0(%rip), %xmm0
	ja	.L6
	andpd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	jbe	.L9
.L6:
	andpd	.LC5(%rip), %xmm1
	orpd	.LC4(%rip), %xmm1
.L13:
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	movapd	%xmm0, %xmm1
	ret
	.size	__casin, .-__casin
	.weak	casinf32x
	.set	casinf32x,__casin
	.weak	casinf64
	.set	casinf64,__casin
	.weak	casin
	.set	casin,__casin
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.align 8
.LC1:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	0
	.long	2146435072
	.long	0
	.long	0
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
