	.text
	.p2align 4,,15
	.globl	__gamma_product
	.type	__gamma_product, @function
__gamma_product:
	movsd	%xmm0, -24(%rsp)
	cmpl	$1, %edi
	fldl	-24(%rsp)
	movsd	%xmm1, -24(%rsp)
	fldl	-24(%rsp)
	faddp	%st, %st(1)
	fld	%st(0)
	jle	.L7
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%eax, -24(%rsp)
	addl	$1, %eax
	fildl	-24(%rsp)
	cmpl	%eax, %edi
	fadd	%st(2), %st
	fmulp	%st, %st(1)
	jne	.L3
	fstp	%st(1)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	fstp	%st(1)
.L2:
	fstl	-24(%rsp)
	fldl	-24(%rsp)
	movsd	-24(%rsp), %xmm0
	fsubr	%st, %st(1)
	fdivrp	%st, %st(1)
	fstpl	(%rsi)
	ret
	.size	__gamma_product, .-__gamma_product
