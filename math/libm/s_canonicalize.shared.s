	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__canonicalize
	.type	__canonicalize, @function
__canonicalize:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movsd	(%rsi), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 8(%rsp)
	call	__GI___issignaling
	testl	%eax, %eax
	movsd	8(%rsp), %xmm1
	jne	.L6
	movsd	%xmm1, (%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	addsd	%xmm1, %xmm1
	xorl	%eax, %eax
	movsd	%xmm1, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__canonicalize, .-__canonicalize
	.weak	canonicalizef32x
	.set	canonicalizef32x,__canonicalize
	.weak	canonicalizef64
	.set	canonicalizef64,__canonicalize
	.weak	canonicalize
	.set	canonicalize,__canonicalize
