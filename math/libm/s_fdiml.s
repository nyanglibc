	.text
	.p2align 4,,15
	.globl	__fdiml
	.type	__fdiml, @function
__fdiml:
	fldt	8(%rsp)
	fldt	24(%rsp)
	fucomi	%st(1), %st
	jnb	.L9
	fld	%st(1)
	fsub	%st(1), %st
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L11
	fxch	%st(2)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L12
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	fstp	%st(0)
	fstp	%st(0)
	fldz
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	fstp	%st(1)
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
.L1:
	rep ret
	.size	__fdiml, .-__fdiml
	.weak	fdimf64x
	.set	fdimf64x,__fdiml
	.weak	fdiml
	.set	fdiml,__fdiml
