	.text
	.p2align 4,,15
	.globl	__hypot
	.type	__hypot, @function
__hypot:
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_hypot@PLT
	movq	.LC0(%rip), %xmm4
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm1
	movsd	(%rsp), %xmm2
	andpd	%xmm4, %xmm5
	movsd	8(%rsp), %xmm3
	ucomisd	%xmm5, %xmm1
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andpd	%xmm4, %xmm2
	ucomisd	%xmm2, %xmm1
	jb	.L1
	andpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm1
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__hypot, .-__hypot
	.weak	hypotf32x
	.set	hypotf32x,__hypot
	.weak	hypotf64
	.set	hypotf64,__hypot
	.weak	hypot
	.set	hypot,__hypot
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
