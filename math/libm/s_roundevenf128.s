	.text
	.globl	__addtf3
	.p2align 4,,15
	.globl	__roundevenf128
	.type	__roundevenf128, @function
__roundevenf128:
	subq	$24, %rsp
	movabsq	$9223372036854775807, %rcx
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	movq	%rdi, %rdx
	shrq	$48, %rdx
	cmpq	$16494, %rdx
	jbe	.L2
	cmpq	$32767, %rdx
	je	.L27
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rsp), %rax
	cmpq	$16431, %rdx
	movq	%rax, %r8
	jbe	.L4
	movl	$1, %edi
	movl	$16494, %ecx
	subl	%edx, %ecx
	movq	%rdi, %r8
	salq	%cl, %r8
	movl	$16495, %ecx
	subl	%edx, %ecx
	leaq	-1(%r8), %rdx
	salq	%cl, %rdi
	orq	%rdi, %rdx
	testq	%rax, %rdx
	jne	.L28
.L5:
	negq	%rdi
	andq	%rdi, %rax
.L8:
	movq	%rsi, 8(%rsp)
	movq	%rax, (%rsp)
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$16431, %edx
	je	.L29
	cmpl	$16382, %edx
	jle	.L10
	movl	$1, %eax
	movl	$16430, %ecx
	subl	%edx, %ecx
	movq	%rax, %rdi
	salq	%cl, %rdi
	movl	$16431, %ecx
	subl	%edx, %ecx
	leaq	-1(%rdi), %rdx
	addq	%rsi, %rdi
	salq	%cl, %rax
	orq	%rax, %rdx
	andq	%rsi, %rdx
	orq	%r8, %rdx
	cmovne	%rdi, %rsi
	negq	%rax
	andq	%rax, %rsi
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%edx, %edx
	addq	%r8, %rax
	setc	%dl
	addq	%rdx, %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rsi, %rdx
	andq	%rax, %rcx
	xorl	%eax, %eax
	andl	$1, %edx
	orq	%rcx, %rdx
	je	.L8
	shrq	$63, %r8
	addq	%r8, %rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movabsq	$-9223372036854775808, %rax
	andq	%rax, %rsi
	xorl	%eax, %eax
	cmpl	$16382, %edx
	jne	.L8
	movabsq	$4611123068473966592, %rdx
	cmpq	%rdx, %rdi
	ja	.L17
	testq	%r8, %r8
	je	.L8
.L17:
	movabsq	$4611404543450677248, %rax
	orq	%rax, %rsi
	xorl	%eax, %eax
	jmp	.L8
	.size	__roundevenf128, .-__roundevenf128
	.weak	roundevenf128
	.set	roundevenf128,__roundevenf128
