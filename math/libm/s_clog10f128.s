	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__divtf3
	.globl	__lttf2
	.globl	__multf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__floatsitf
	.p2align 4,,15
	.globl	__clog10f128
	.type	__clog10f128, @function
__clog10f128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$48, %rsp
	movdqa	80(%rsp), %xmm4
	pand	.LC4(%rip), %xmm4
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC5(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L94
	movdqa	96(%rsp), %xmm5
	pand	.LC4(%rip), %xmm5
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	movaps	%xmm5, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L44
.L7:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L14
	movdqa	(%rsp), %xmm0
	movdqa	16(%rsp), %xmm6
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm6, (%rsp)
.L14:
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L95
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L19
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L96
.L19:
	movdqa	.LC10(%rip), %xmm7
	movdqa	(%rsp), %xmm0
	movdqa	%xmm7, %xmm1
	movaps	%xmm7, 32(%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L88
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jns	.L23
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	.p2align 4,,10
	.p2align 3
.L23:
	movdqa	80(%rsp), %xmm1
	movdqa	96(%rsp), %xmm0
	movaps	%xmm2, (%rsp)
	call	__ieee754_atan2f128@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm0, %xmm3
.L6:
	movaps	%xmm2, (%rbx)
	movq	%rbx, %rax
	movaps	%xmm3, 16(%rbx)
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L97
	movdqa	96(%rsp), %xmm3
	pand	.LC4(%rip), %xmm3
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L45
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L7
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L7
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L7
	movaps	80(%rsp), %xmm7
	pxor	%xmm0, %xmm0
	movmskps	%xmm7, %eax
	testb	$8, %al
	je	.L13
	movdqa	.LC0(%rip), %xmm0
.L13:
	movdqa	96(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC7(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm3
	movdqa	%xmm0, %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L95:
	movdqa	(%rsp), %xmm0
	movl	$-1, %edi
	call	__scalbnf128@PLT
	movdqa	.LC9(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L98
	pxor	%xmm6, %xmm6
	movl	$-1, %ebp
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC10(%rip), %xmm6
	movaps	%xmm6, 32(%rsp)
.L17:
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L25
	movdqa	.LC12(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L25
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L99
.L25:
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L100
.L31:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__ieee754_hypotf128@PLT
	call	__ieee754_log10f128@PLT
	movl	%ebp, %edi
	movaps	%xmm0, (%rsp)
	call	__floatsitf@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	96(%rsp), %xmm6
	pand	.LC4(%rip), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	movaps	%xmm6, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L7
.L45:
	movdqa	.LC2(%rip), %xmm3
	movdqa	%xmm3, %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L97:
	movdqa	96(%rsp), %xmm7
	pand	.LC4(%rip), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	movaps	%xmm7, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L7
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L96:
	movdqa	(%rsp), %xmm0
	movl	$113, %edi
	movl	$113, %ebp
	call	__scalbnf128@PLT
	movaps	%xmm0, (%rsp)
	movl	$113, %edi
	movdqa	16(%rsp), %xmm0
	call	__scalbnf128@PLT
	movdqa	.LC10(%rip), %xmm7
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm7, 32(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L98:
	movdqa	16(%rsp), %xmm0
	movl	$-1, %edi
	movl	$-1, %ebp
	call	__scalbnf128@PLT
	movdqa	.LC10(%rip), %xmm6
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm6, 32(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L88:
	xorl	%ebp, %ebp
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L100:
	movdqa	.LC14(%rip), %xmm1
	testl	%ebp, %ebp
	sete	%r12b
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L33
	movdqa	.LC15(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L101
.L33:
	movdqa	.LC14(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L31
	testb	%r12b, %r12b
	je	.L31
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm3
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L31
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__x2y2m1f128@PLT
	call	__log1pf128@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	96(%rsp), %xmm2
	pand	.LC4(%rip), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L45
	movdqa	(%rsp), %xmm2
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	movdqa	.LC2(%rip), %xmm3
	testq	%rax, %rax
	movdqa	%xmm3, %xmm2
	jle	.L6
.L92:
	movdqa	.LC3(%rip), %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L99:
	testl	%ebp, %ebp
	jne	.L25
	movdqa	.LC10(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC10(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L29
	movdqa	16(%rsp), %xmm3
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
.L29:
	movdqa	(%rsp), %xmm0
	call	__log1pf128@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L44:
	movdqa	.LC2(%rip), %xmm3
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L101:
	testb	%r12b, %r12b
	je	.L33
	movdqa	.LC10(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC10(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L23
	.size	__clog10f128, .-__clog10f128
	.weak	clog10f128
	.set	clog10f128,__clog10f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	302048907
	.long	185521955
	.long	3301650362
	.long	1073700167
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC8:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147352575
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	131072
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC11:
	.long	1432312424
	.long	3811207863
	.long	2974969424
	.long	1073527991
	.align 16
.LC12:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC13:
	.long	0
	.long	0
	.long	0
	.long	1066336256
	.align 16
.LC14:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC15:
	.long	0
	.long	0
	.long	0
	.long	1066270720
	.align 16
.LC16:
	.long	3008894713
	.long	4012973842
	.long	889845663
	.long	1073558593
	.align 16
.LC17:
	.long	1432312424
	.long	3811207863
	.long	2974969424
	.long	1073593527
