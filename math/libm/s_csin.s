	.text
	.p2align 4,,15
	.globl	__csin
	.type	__csin, @function
__csin:
	pushq	%rbp
	pushq	%rbx
	movmskpd	%xmm0, %ebp
	movapd	%xmm0, %xmm2
	subq	$88, %rsp
	andl	$1, %ebp
	movq	.LC4(%rip), %xmm5
	movapd	%xmm1, %xmm4
	movapd	%xmm1, %xmm6
	andpd	%xmm5, %xmm2
	andpd	%xmm5, %xmm4
	ucomisd	%xmm2, %xmm2
	jp	.L3
	ucomisd	.LC5(%rip), %xmm2
	ja	.L3
	ucomisd	.LC6(%rip), %xmm2
	jnb	.L7
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L7
	jne	.L7
	ucomisd	%xmm4, %xmm4
	jp	.L85
	ucomisd	.LC5(%rip), %xmm4
	jbe	.L36
	testl	%ebp, %ebp
	pxor	%xmm0, %xmm0
	jne	.L86
.L27:
	movapd	%xmm6, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	ucomisd	%xmm4, %xmm4
	jp	.L52
	ucomisd	.LC5(%rip), %xmm4
	jbe	.L87
	subsd	%xmm2, %xmm2
	movsd	.LC2(%rip), %xmm1
	movapd	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L87:
	ucomisd	.LC6(%rip), %xmm4
	jnb	.L42
	ucomisd	.LC1(%rip), %xmm6
	jp	.L42
	jne	.L42
	subsd	%xmm2, %xmm2
	movapd	%xmm6, %xmm1
	movapd	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	ucomisd	%xmm4, %xmm4
	jp	.L52
	ucomisd	.LC5(%rip), %xmm4
	ja	.L35
.L36:
	movsd	.LC8(%rip), %xmm3
	ucomisd	.LC6(%rip), %xmm2
	mulsd	.LC7(%rip), %xmm3
	cvttsd2si	%xmm3, %ebx
	jbe	.L67
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm6, 24(%rsp)
	movapd	%xmm2, %xmm0
	movsd	%xmm4, (%rsp)
	movaps	%xmm5, 32(%rsp)
	call	__sincos@PLT
	movsd	(%rsp), %xmm4
	movsd	24(%rsp), %xmm6
	movapd	32(%rsp), %xmm5
.L11:
	testl	%ebp, %ebp
	je	.L12
	movsd	64(%rsp), %xmm0
	xorpd	.LC10(%rip), %xmm0
	movsd	%xmm0, 64(%rsp)
.L12:
	pxor	%xmm3, %xmm3
	movsd	%xmm4, (%rsp)
	cvtsi2sd	%ebx, %xmm3
	ucomisd	%xmm3, %xmm4
	jbe	.L68
	movapd	%xmm3, %xmm0
	movsd	%xmm6, 32(%rsp)
	movaps	%xmm5, 48(%rsp)
	movsd	%xmm3, 24(%rsp)
	call	__ieee754_exp@PLT
	movsd	32(%rsp), %xmm6
	movsd	24(%rsp), %xmm3
	movmskpd	%xmm6, %eax
	movapd	48(%rsp), %xmm5
	testb	$1, %al
	movsd	(%rsp), %xmm4
	jne	.L15
	movsd	72(%rsp), %xmm2
.L16:
	movsd	.LC11(%rip), %xmm1
	subsd	%xmm3, %xmm4
	movsd	64(%rsp), %xmm6
	mulsd	%xmm0, %xmm1
	ucomisd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm6
	mulsd	%xmm2, %xmm1
	movsd	%xmm6, 64(%rsp)
	movsd	%xmm1, 72(%rsp)
	ja	.L88
.L69:
	movapd	%xmm4, %xmm0
	movaps	%xmm5, (%rsp)
	call	__ieee754_exp@PLT
	movsd	64(%rsp), %xmm3
	movsd	72(%rsp), %xmm2
	mulsd	%xmm0, %xmm3
	movapd	(%rsp), %xmm5
	mulsd	%xmm0, %xmm2
	movapd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L21:
	movapd	%xmm3, %xmm4
	movsd	.LC6(%rip), %xmm7
	andpd	%xmm5, %xmm4
	ucomisd	%xmm4, %xmm7
	jbe	.L22
	mulsd	%xmm3, %xmm3
.L22:
	andpd	%xmm2, %xmm5
	movsd	.LC6(%rip), %xmm7
	ucomisd	%xmm5, %xmm7
	jbe	.L1
	mulsd	%xmm2, %xmm2
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movapd	%xmm6, %xmm0
	movsd	%xmm6, 24(%rsp)
	movaps	%xmm5, 32(%rsp)
	call	__ieee754_cosh@PLT
	movsd	64(%rsp), %xmm3
	movsd	24(%rsp), %xmm6
	mulsd	%xmm0, %xmm3
	movapd	%xmm6, %xmm0
	movsd	%xmm3, (%rsp)
	call	__ieee754_sinh@PLT
	movsd	72(%rsp), %xmm2
	movsd	(%rsp), %xmm3
	mulsd	%xmm0, %xmm2
	movapd	32(%rsp), %xmm5
	movapd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L88:
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm1
	ucomisd	%xmm3, %xmm4
	movsd	%xmm6, 64(%rsp)
	movsd	%xmm1, 72(%rsp)
	jbe	.L69
	movsd	.LC5(%rip), %xmm3
	movsd	.LC5(%rip), %xmm2
	mulsd	%xmm6, %xmm3
	mulsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm0
	movapd	%xmm2, %xmm1
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L15:
	movsd	72(%rsp), %xmm1
	xorpd	.LC10(%rip), %xmm1
	movapd	%xmm1, %xmm2
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L67:
	movq	.LC9(%rip), %rax
	movsd	%xmm2, 64(%rsp)
	movq	%rax, 72(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L35:
	ucomisd	.LC6(%rip), %xmm2
	jbe	.L70
	movapd	%xmm2, %xmm0
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm6, (%rsp)
	call	__sincos@PLT
	movq	.LC10(%rip), %xmm4
	movsd	72(%rsp), %xmm1
	movq	.LC12(%rip), %xmm0
	andpd	%xmm4, %xmm1
	movsd	64(%rsp), %xmm2
	movsd	(%rsp), %xmm6
	orpd	%xmm0, %xmm1
	movapd	%xmm1, %xmm3
.L28:
	andpd	%xmm4, %xmm2
	testl	%ebp, %ebp
	orpd	%xmm0, %xmm2
	je	.L79
	xorpd	%xmm4, %xmm2
.L79:
	movmskpd	%xmm6, %eax
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	testb	$1, %al
	je	.L1
	xorpd	%xmm4, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L1
.L86:
	movsd	.LC0(%rip), %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L85:
	testl	%ebp, %ebp
	je	.L48
	movsd	.LC0(%rip), %xmm0
	movsd	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm1
	jmp	.L1
.L70:
	movsd	.LC2(%rip), %xmm3
	movq	.LC10(%rip), %xmm4
	movq	.LC12(%rip), %xmm0
	jmp	.L28
.L52:
	movsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L1
	.size	__csin, .-__csin
	.weak	csinf32x
	.set	csinf32x,__csin
	.weak	csinf64
	.set	csinf64,__csin
	.weak	csin
	.set	csin,__csin
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-2147483648
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC2:
	.long	0
	.long	2146435072
	.align 8
.LC3:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	4294967295
	.long	2146435071
	.align 8
.LC6:
	.long	0
	.long	1048576
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1083176960
	.align 8
.LC9:
	.long	0
	.long	1072693248
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC11:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC12:
	.long	0
	.long	2146435072
	.long	0
	.long	0
