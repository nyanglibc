	.text
	.p2align 4,,15
	.globl	__rintl
	.type	__rintl, @function
__rintl:
	fldt	8(%rsp)
#APP
# 15 "../sysdeps/i386/fpu/s_rintl.c" 1
	frndint
# 0 "" 2
#NO_APP
	ret
	.size	__rintl, .-__rintl
	.weak	rintf64x
	.set	rintf64x,__rintl
	.weak	rintl
	.set	rintl,__rintl
