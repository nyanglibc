	.text
	.p2align 4,,15
	.globl	__log2l
	.type	__log2l, @function
__log2l:
	fldt	8(%rsp)
	fldz
	fucomi	%st(1), %st
	jnb	.L7
	fstp	%st(0)
.L2:
	fstpt	8(%rsp)
	jmp	__ieee754_log2l@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__log2l, .-__log2l
	.weak	log2f64x
	.set	log2f64x,__log2l
	.weak	log2l
	.set	log2l,__log2l
