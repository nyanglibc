	.text
	.p2align 4,,15
	.globl	__csqrt
	.type	__csqrt, @function
__csqrt:
	movq	.LC2(%rip), %xmm4
	movapd	%xmm0, %xmm6
	movapd	%xmm1, %xmm2
	andpd	%xmm4, %xmm6
	andpd	%xmm4, %xmm2
	ucomisd	%xmm6, %xmm6
	jp	.L2
	pushq	%rbx
	subq	$64, %rsp
	movsd	.LC3(%rip), %xmm7
	ucomisd	%xmm7, %xmm6
	jbe	.L107
	ucomisd	%xmm2, %xmm2
	jp	.L108
	ucomisd	%xmm7, %xmm2
	jbe	.L109
.L8:
	movsd	.LC12(%rip), %xmm0
.L1:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	ucomisd	.LC4(%rip), %xmm6
	movapd	%xmm0, %xmm3
	movapd	%xmm1, %xmm5
	jnb	.L4
	pxor	%xmm8, %xmm8
	ucomisd	%xmm8, %xmm0
	movsd	%xmm8, 8(%rsp)
	jp	.L7
	je	.L5
.L7:
	ucomisd	%xmm2, %xmm2
	jp	.L67
	ucomisd	%xmm7, %xmm2
	ja	.L8
	ucomisd	.LC4(%rip), %xmm2
	jnb	.L48
	ucomisd	8(%rsp), %xmm1
	jp	.L48
	jne	.L48
.L55:
	movsd	8(%rsp), %xmm5
	ucomisd	%xmm0, %xmm5
	ja	.L110
	sqrtsd	%xmm0, %xmm2
	andpd	%xmm4, %xmm2
	movapd	%xmm2, %xmm0
.L104:
	andpd	.LC6(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	%xmm2, %xmm2
	jp	.L67
	ucomisd	%xmm7, %xmm2
	ja	.L8
	ucomisd	.LC4(%rip), %xmm2
	pxor	%xmm7, %xmm7
	movsd	%xmm7, 8(%rsp)
	jnb	.L48
	ucomisd	%xmm7, %xmm1
	jp	.L48
	je	.L55
.L48:
	movsd	.LC9(%rip), %xmm7
	ucomisd	%xmm7, %xmm6
	ja	.L111
	ucomisd	%xmm7, %xmm2
	jbe	.L87
	ucomisd	.LC10(%rip), %xmm6
	jnb	.L112
	movsd	8(%rsp), %xmm5
	movsd	%xmm5, 32(%rsp)
.L28:
	movapd	%xmm1, %xmm0
	movl	$-2, %edi
	movaps	%xmm4, 16(%rsp)
	call	__scalbn@PLT
	movapd	%xmm0, %xmm5
	movsd	32(%rsp), %xmm2
	movapd	16(%rsp), %xmm4
.L23:
	movapd	%xmm5, %xmm1
	movapd	%xmm2, %xmm0
	movaps	%xmm4, 48(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm2, 16(%rsp)
	call	__ieee754_hypot@PLT
	movsd	16(%rsp), %xmm2
	movsd	32(%rsp), %xmm5
	ucomisd	8(%rsp), %xmm2
	movapd	%xmm5, %xmm3
	movapd	48(%rsp), %xmm4
	andpd	%xmm4, %xmm3
	jbe	.L113
	addsd	%xmm0, %xmm2
	movsd	.LC8(%rip), %xmm1
	movsd	.LC11(%rip), %xmm0
	ucomisd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm2
	sqrtsd	%xmm2, %xmm2
	ja	.L114
	movapd	%xmm5, %xmm0
	movl	$1, %ebx
	divsd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm1
.L35:
	movapd	%xmm2, %xmm0
	movl	%ebx, %edi
	movsd	%xmm5, 32(%rsp)
	movaps	%xmm4, 48(%rsp)
	movsd	%xmm1, 16(%rsp)
	call	__scalbn@PLT
	movsd	16(%rsp), %xmm1
	movl	%ebx, %edi
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm1, %xmm0
	call	__scalbn@PLT
	movapd	48(%rsp), %xmm4
	movapd	%xmm0, %xmm1
	movsd	32(%rsp), %xmm5
	movsd	8(%rsp), %xmm2
.L34:
	movapd	%xmm2, %xmm0
	movsd	.LC4(%rip), %xmm7
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm7
	jbe	.L36
	movapd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm0
.L36:
	movapd	%xmm1, %xmm0
	movsd	.LC4(%rip), %xmm7
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm7
	jbe	.L38
	movapd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
.L38:
	movapd	%xmm5, %xmm3
	andpd	%xmm4, %xmm1
	movapd	%xmm2, %xmm0
	andpd	.LC6(%rip), %xmm3
	addq	$64, %rsp
	popq	%rbx
	orpd	%xmm3, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	ucomisd	.LC4(%rip), %xmm2
	jb	.L115
	pxor	%xmm3, %xmm3
	ucomisd	%xmm0, %xmm3
	jbe	.L104
.L60:
	pxor	%xmm2, %xmm2
.L12:
	andpd	.LC6(%rip), %xmm1
	movapd	%xmm2, %xmm0
	orpd	.LC5(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	ucomisd	%xmm2, %xmm2
	jp	.L67
	ucomisd	%xmm7, %xmm2
	ja	.L8
	ucomisd	.LC4(%rip), %xmm2
	jnb	.L51
	ucomisd	8(%rsp), %xmm1
	jp	.L51
	je	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	ucomisd	.LC7(%rip), %xmm2
	jb	.L85
	mulsd	.LC8(%rip), %xmm2
	sqrtsd	%xmm2, %xmm2
.L20:
	movapd	%xmm1, %xmm5
	movapd	%xmm2, %xmm0
	andpd	.LC6(%rip), %xmm5
	andpd	%xmm0, %xmm4
	orpd	%xmm5, %xmm4
	movapd	%xmm4, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L115:
	pxor	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm1
	movsd	%xmm7, 8(%rsp)
	jp	.L10
	je	.L116
.L10:
	movsd	8(%rsp), %xmm5
	ucomisd	%xmm0, %xmm5
	jbe	.L104
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L87:
	movsd	.LC7(%rip), %xmm7
	ucomisd	%xmm6, %xmm7
	jbe	.L90
	ucomisd	%xmm2, %xmm7
	ja	.L117
.L90:
	xorl	%ebx, %ebx
.L29:
	movapd	%xmm5, %xmm1
	movapd	%xmm3, %xmm0
	movaps	%xmm4, 48(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm3, 16(%rsp)
	call	__ieee754_hypot@PLT
	movsd	16(%rsp), %xmm3
	movsd	32(%rsp), %xmm5
	ucomisd	8(%rsp), %xmm3
	movapd	48(%rsp), %xmm4
	jbe	.L118
	addsd	%xmm0, %xmm3
	movsd	.LC8(%rip), %xmm1
	movapd	%xmm5, %xmm0
	mulsd	%xmm1, %xmm3
	sqrtsd	%xmm3, %xmm2
	divsd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm1
.L44:
	testl	%ebx, %ebx
	je	.L34
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$-2, %edi
	movsd	%xmm1, 48(%rsp)
	movaps	%xmm4, 32(%rsp)
	call	__scalbn@PLT
	movsd	%xmm0, 16(%rsp)
	movl	$-2, %edi
	movsd	48(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	call	__scalbn@PLT
	movsd	16(%rsp), %xmm2
	movapd	%xmm0, %xmm5
	movapd	32(%rsp), %xmm4
	jmp	.L23
.L118:
	subsd	%xmm3, %xmm0
	movsd	.LC8(%rip), %xmm3
	movapd	%xmm5, %xmm2
	mulsd	%xmm3, %xmm0
	sqrtsd	%xmm0, %xmm1
	divsd	%xmm1, %xmm2
	mulsd	%xmm3, %xmm2
	andpd	%xmm4, %xmm2
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm2, %xmm2
	jp	.L105
	ucomisd	.LC3(%rip), %xmm2
	movsd	.LC12(%rip), %xmm0
	jbe	.L105
	rep ret
	.p2align 4,,10
	.p2align 3
.L105:
	movsd	.LC0(%rip), %xmm0
	movapd	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	subsd	%xmm2, %xmm0
	movsd	.LC8(%rip), %xmm6
	mulsd	%xmm6, %xmm0
	sqrtsd	%xmm0, %xmm1
	movsd	.LC11(%rip), %xmm0
	ucomisd	%xmm3, %xmm0
	ja	.L119
	movapd	%xmm5, %xmm2
	movl	$1, %ebx
	divsd	%xmm1, %xmm2
	mulsd	%xmm6, %xmm2
	andpd	%xmm4, %xmm2
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$-2, %edi
	movsd	%xmm1, 48(%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__scalbn@PLT
	movsd	%xmm0, 32(%rsp)
	movsd	48(%rsp), %xmm1
	movapd	16(%rsp), %xmm4
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$54, %edi
	movsd	%xmm1, 48(%rsp)
	movl	$-27, %ebx
	movaps	%xmm4, 32(%rsp)
	call	__scalbn@PLT
	movsd	%xmm0, 16(%rsp)
	movl	$54, %edi
	movsd	48(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	call	__scalbn@PLT
	movsd	16(%rsp), %xmm3
	movapd	%xmm0, %xmm5
	movapd	32(%rsp), %xmm4
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L85:
	addsd	%xmm2, %xmm2
	sqrtsd	%xmm2, %xmm2
	mulsd	.LC8(%rip), %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L114:
	movapd	%xmm5, %xmm1
	movl	$1, %edi
	movapd	%xmm2, %xmm0
	movsd	%xmm5, 16(%rsp)
	divsd	%xmm2, %xmm1
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm1, 8(%rsp)
	call	__scalbn@PLT
	movsd	8(%rsp), %xmm1
	movapd	%xmm0, %xmm2
	movsd	16(%rsp), %xmm5
	movapd	32(%rsp), %xmm4
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L110:
	movq	.LC6(%rip), %xmm5
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm6
	pxor	%xmm0, %xmm0
	xorpd	%xmm5, %xmm2
	andpd	%xmm5, %xmm6
	sqrtsd	%xmm2, %xmm2
	andnpd	%xmm2, %xmm5
	orpd	%xmm6, %xmm5
	movapd	%xmm5, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	movsd	.LC0(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L108:
	pxor	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	ja	.L63
	movsd	.LC0(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L119:
	movapd	%xmm5, %xmm2
	movl	$1, %edi
	movapd	%xmm1, %xmm0
	movsd	%xmm5, 32(%rsp)
	divsd	%xmm1, %xmm2
	movaps	%xmm4, 16(%rsp)
	andpd	%xmm4, %xmm2
	movsd	%xmm2, 8(%rsp)
	call	__scalbn@PLT
	movsd	8(%rsp), %xmm2
	movapd	%xmm0, %xmm1
	movapd	16(%rsp), %xmm4
	movsd	32(%rsp), %xmm5
	jmp	.L34
.L63:
	movsd	.LC0(%rip), %xmm2
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L116:
	ucomisd	%xmm0, %xmm7
	jbe	.L104
	jmp	.L60
	.size	__csqrt, .-__csqrt
	.weak	csqrtf32x
	.set	csqrtf32x,__csqrt
	.weak	csqrtf64
	.set	csqrtf64,__csqrt
	.weak	csqrt
	.set	csqrt,__csqrt
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.align 8
.LC4:
	.long	0
	.long	1048576
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	2146435072
	.long	0
	.long	0
	.align 16
.LC6:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	2097152
	.align 8
.LC8:
	.long	0
	.long	1071644672
	.align 8
.LC9:
	.long	4294967295
	.long	2144337919
	.align 8
.LC10:
	.long	0
	.long	3145728
	.align 8
.LC11:
	.long	0
	.long	1072693248
	.align 8
.LC12:
	.long	0
	.long	2146435072
