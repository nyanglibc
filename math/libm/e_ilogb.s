	.text
	.p2align 4,,15
	.globl	__ieee754_ilogb
	.type	__ieee754_ilogb, @function
__ieee754_ilogb:
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$32, %rdx
	andl	$2147483647, %edx
	cmpl	$1048575, %edx
	jg	.L2
	movl	%edx, %eax
	movd	%xmm0, %esi
	orl	%ecx, %eax
	je	.L8
	testl	%edx, %edx
	jne	.L4
	testl	%ecx, %ecx
	movl	$-1043, %eax
	jle	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	addl	%esi, %esi
	subl	$1, %eax
	testl	%esi, %esi
	jg	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$2146435071, %edx
	jle	.L14
	xorl	$2146435072, %edx
	xorl	%eax, %eax
	orl	%ecx, %edx
	setne	%al
	addl	$2147483647, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	sarl	$20, %edx
	leal	-1023(%rdx), %eax
	ret
.L8:
	movl	$-2147483648, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	sall	$11, %edx
	movl	$-1022, %eax
	.p2align 4,,10
	.p2align 3
.L6:
	addl	%edx, %edx
	subl	$1, %eax
	testl	%edx, %edx
	jg	.L6
	rep ret
	.size	__ieee754_ilogb, .-__ieee754_ilogb
