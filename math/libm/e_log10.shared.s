	.text
#APP
	.symver __ieee754_log10,__log10_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_log10
	.type	__ieee754_log10, @function
__ieee754_log10:
	movq	%xmm0, %rdx
	movabsq	$4503599627370495, %rax
	cmpq	%rax, %rdx
	jg	.L7
	movabsq	$9223372036854775807, %rax
	testq	%rax, %rdx
	je	.L12
	testq	%rdx, %rdx
	js	.L13
	mulsd	.LC2(%rip), %xmm0
	movl	$-54, %ecx
	movq	%xmm0, %rdx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%ecx, %ecx
.L2:
	movabsq	$9218868437227405311, %rax
	cmpq	%rax, %rdx
	ja	.L14
	movq	%rdx, %rax
	pxor	%xmm1, %xmm1
	sarq	$52, %rax
	subq	$24, %rsp
	leal	-1023(%rcx,%rax), %eax
	cltq
	movq	%rax, %rcx
	shrq	$63, %rcx
	addq	%rcx, %rax
	cvtsi2sdq	%rax, %xmm1
	movl	$1023, %eax
	subq	%rcx, %rax
	movabsq	$4503599627370495, %rcx
	salq	$52, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, (%rsp)
	movsd	(%rsp), %xmm0
	movsd	%xmm1, 8(%rsp)
	call	__ieee754_log@PLT
	movsd	8(%rsp), %xmm1
	movsd	.LC3(%rip), %xmm2
	mulsd	.LC4(%rip), %xmm0
	mulsd	%xmm1, %xmm2
	mulsd	.LC5(%rip), %xmm1
	addq	$24, %rsp
	addsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movapd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm0
	andpd	.LC0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	subsd	%xmm0, %xmm0
	divsd	%xmm0, %xmm0
	ret
	.size	__ieee754_log10, .-__ieee754_log10
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1018167296
	.align 8
.LC2:
	.long	0
	.long	1129316352
	.align 8
.LC3:
	.long	301017910
	.long	1029308147
	.align 8
.LC4:
	.long	354870542
	.long	1071369083
	.align 8
.LC5:
	.long	1352622080
	.long	1070810131
