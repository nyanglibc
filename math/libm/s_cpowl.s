	.text
	.p2align 4,,15
	.globl	__cpowl
	.type	__cpowl, @function
__cpowl:
	subq	$40, %rsp
	fldt	80(%rsp)
	fstpt	(%rsp)
	fldt	96(%rsp)
	fstpt	16(%rsp)
	pushq	72(%rsp)
	pushq	72(%rsp)
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__clogl@PLT
	fxch	%st(1)
	addq	$32, %rsp
	pushq	24(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__mulxc3@PLT
	fstpt	112(%rsp)
	fstpt	128(%rsp)
	addq	$104, %rsp
	jmp	__cexpl@PLT
	.size	__cpowl, .-__cpowl
	.weak	cpowf64x
	.set	cpowf64x,__cpowl
	.weak	cpowl
	.set	cpowl,__cpowl
