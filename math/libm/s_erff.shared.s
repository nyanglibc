	.text
	.p2align 4,,15
	.globl	__erff
	.type	__erff, @function
__erff:
	movd	%xmm0, %eax
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %eax
	subq	$32, %rsp
	cmpl	$2139095039, %eax
	jg	.L18
	cmpl	$1062731775, %eax
	jg	.L4
	cmpl	$830472191, %eax
	jg	.L5
	cmpl	$67108863, %eax
	movaps	%xmm0, %xmm1
	jg	.L6
	mulss	.LC1(%rip), %xmm1
	movss	.LC5(%rip), %xmm2
	mulss	.LC2(%rip), %xmm0
	addss	%xmm1, %xmm0
	mulss	.LC3(%rip), %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC4(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jbe	.L1
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1067450367, %eax
	jg	.L8
	movaps	%xmm0, %xmm1
	movss	.LC0(%rip), %xmm3
	movss	.LC16(%rip), %xmm0
	testl	%ebx, %ebx
	andps	.LC4(%rip), %xmm1
	movss	.LC23(%rip), %xmm2
	subss	%xmm3, %xmm1
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm2
	addss	.LC17(%rip), %xmm0
	addss	.LC24(%rip), %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm2
	subss	.LC18(%rip), %xmm0
	addss	.LC25(%rip), %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm2
	addss	.LC19(%rip), %xmm0
	addss	.LC26(%rip), %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm2
	subss	.LC20(%rip), %xmm0
	addss	.LC27(%rip), %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm2
	addss	.LC21(%rip), %xmm0
	addss	.LC28(%rip), %xmm2
	mulss	%xmm1, %xmm0
	mulss	%xmm2, %xmm1
	subss	.LC22(%rip), %xmm0
	addss	%xmm3, %xmm1
	divss	%xmm1, %xmm0
	js	.L9
	addss	.LC29(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movss	.LC0(%rip), %xmm2
	shrl	$31, %ebx
	divss	%xmm0, %xmm2
	addl	%ebx, %ebx
	movl	$1, %eax
	subl	%ebx, %eax
	pxor	%xmm1, %xmm1
	cvtsi2ss	%eax, %xmm1
	movaps	%xmm2, %xmm0
	addss	%xmm1, %xmm0
.L1:
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1086324735, %eax
	jle	.L10
	testl	%ebx, %ebx
	js	.L11
	movss	.LC0(%rip), %xmm0
	subss	.LC31(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movaps	%xmm0, %xmm3
	movss	.LC11(%rip), %xmm2
	mulss	%xmm0, %xmm3
	movss	.LC7(%rip), %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm1
	addss	.LC12(%rip), %xmm2
	subss	.LC8(%rip), %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm1
	addss	.LC13(%rip), %xmm2
	subss	.LC9(%rip), %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm1
	addss	.LC14(%rip), %xmm2
	subss	.LC10(%rip), %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm3, %xmm1
	addss	.LC15(%rip), %xmm2
	addss	.LC6(%rip), %xmm1
	mulss	%xmm3, %xmm2
	addss	.LC0(%rip), %xmm2
	addq	$32, %rsp
	popq	%rbx
	divss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movaps	%xmm0, %xmm5
	mulss	%xmm0, %xmm0
	movss	.LC0(%rip), %xmm1
	cmpl	$1077336941, %eax
	movaps	%xmm1, %xmm6
	andps	.LC4(%rip), %xmm5
	divss	%xmm0, %xmm6
	movss	%xmm5, 8(%rsp)
	movaps	%xmm6, %xmm0
	movaps	%xmm6, %xmm2
	jle	.L19
	mulss	.LC48(%rip), %xmm2
	subss	.LC49(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC50(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC51(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC52(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC53(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC54(%rip), %xmm2
	movss	%xmm2, 24(%rsp)
	movss	.LC55(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC56(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC57(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC58(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC59(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC60(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC61(%rip), %xmm2
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 28(%rsp)
.L13:
	movl	$-4096, %eax
	andl	8(%rsp), %eax
	movss	%xmm1, 20(%rsp)
	movl	%eax, 12(%rsp)
	movd	12(%rsp), %xmm2
	movaps	%xmm2, %xmm0
	movss	%xmm2, 16(%rsp)
	xorps	.LC62(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC63(%rip), %xmm0
	call	__ieee754_expf@PLT
	movss	24(%rsp), %xmm5
	divss	28(%rsp), %xmm5
	movss	16(%rsp), %xmm2
	movaps	%xmm2, %xmm7
	movss	%xmm0, 12(%rsp)
	movss	8(%rsp), %xmm4
	subss	%xmm4, %xmm7
	addss	%xmm4, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm5, %xmm0
	addss	%xmm2, %xmm0
	call	__ieee754_expf@PLT
	mulss	12(%rsp), %xmm0
	testl	%ebx, %ebx
	movss	20(%rsp), %xmm1
	divss	8(%rsp), %xmm0
	js	.L14
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	mulss	.LC6(%rip), %xmm1
	addss	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	mulss	.LC32(%rip), %xmm2
	subss	.LC33(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC34(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC35(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC36(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC37(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC38(%rip), %xmm2
	mulss	%xmm6, %xmm2
	subss	.LC39(%rip), %xmm2
	movss	%xmm2, 24(%rsp)
	movss	.LC40(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC41(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC42(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC43(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC44(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC45(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC46(%rip), %xmm2
	mulss	%xmm6, %xmm2
	addss	.LC47(%rip), %xmm2
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 28(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movss	.LC31(%rip), %xmm0
	subss	.LC0(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movss	.LC30(%rip), %xmm1
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	subss	%xmm1, %xmm0
	jmp	.L1
	.size	__erff, .-__erff
	.weak	erff32
	.set	erff32,__erff
	.weak	erff
	.set	erff,__erff
	.p2align 4,,15
	.globl	__erfcf
	.type	__erfcf, @function
__erfcf:
	movd	%xmm0, %eax
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %eax
	subq	$32, %rsp
	cmpl	$2139095039, %eax
	jg	.L41
	cmpl	$1062731775, %eax
	jg	.L23
	cmpl	$847249407, %eax
	jle	.L42
	movaps	%xmm0, %xmm1
	movss	.LC11(%rip), %xmm3
	movss	.LC7(%rip), %xmm2
	cmpl	$1048575999, %ebx
	mulss	%xmm0, %xmm1
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm2
	addss	.LC12(%rip), %xmm3
	subss	.LC8(%rip), %xmm2
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm2
	addss	.LC13(%rip), %xmm3
	subss	.LC9(%rip), %xmm2
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm2
	addss	.LC14(%rip), %xmm3
	subss	.LC10(%rip), %xmm2
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm2
	addss	.LC15(%rip), %xmm3
	addss	.LC6(%rip), %xmm2
	mulss	%xmm1, %xmm3
	movss	.LC0(%rip), %xmm1
	addss	%xmm1, %xmm3
	divss	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
	jle	.L40
	movss	.LC64(%rip), %xmm1
	subss	%xmm1, %xmm0
.L40:
	addss	%xmm0, %xmm2
	addq	$32, %rsp
	popq	%rbx
	subss	%xmm2, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$1067450367, %eax
	jg	.L26
	movaps	%xmm0, %xmm1
	movss	.LC0(%rip), %xmm2
	movss	.LC16(%rip), %xmm3
	testl	%ebx, %ebx
	andps	.LC4(%rip), %xmm1
	movss	.LC23(%rip), %xmm0
	subss	%xmm2, %xmm1
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm0
	addss	.LC17(%rip), %xmm3
	addss	.LC24(%rip), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm0
	subss	.LC18(%rip), %xmm3
	addss	.LC25(%rip), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm0
	addss	.LC19(%rip), %xmm3
	addss	.LC26(%rip), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm0
	subss	.LC20(%rip), %xmm3
	addss	.LC27(%rip), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm0
	addss	.LC21(%rip), %xmm3
	addss	.LC28(%rip), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm0, %xmm1
	subss	.LC22(%rip), %xmm3
	addss	%xmm2, %xmm1
	divss	%xmm1, %xmm3
	js	.L27
	movss	.LC65(%rip), %xmm0
	subss	%xmm3, %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L41:
	movss	.LC0(%rip), %xmm2
	shrl	$31, %ebx
	divss	%xmm0, %xmm2
	addl	%ebx, %ebx
	pxor	%xmm1, %xmm1
	cvtsi2ss	%ebx, %xmm1
	movaps	%xmm2, %xmm0
	addss	%xmm1, %xmm0
.L20:
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$1105199103, %eax
	jg	.L28
	movaps	%xmm0, %xmm5
	mulss	%xmm0, %xmm0
	cmpl	$1077336940, %eax
	movss	.LC0(%rip), %xmm1
	andps	.LC4(%rip), %xmm5
	movaps	%xmm1, %xmm2
	divss	%xmm0, %xmm2
	movss	%xmm5, 12(%rsp)
	jg	.L29
	movss	.LC32(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC33(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC34(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC35(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC36(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC37(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC38(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC39(%rip), %xmm0
	movss	%xmm0, 24(%rsp)
	movss	.LC40(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC41(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC42(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC43(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC44(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC45(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC46(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC47(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 28(%rsp)
.L30:
	movl	$-8192, %eax
	andl	12(%rsp), %eax
	movl	%eax, 16(%rsp)
	movd	16(%rsp), %xmm1
	movaps	%xmm1, %xmm0
	movss	%xmm1, 20(%rsp)
	xorps	.LC62(%rip), %xmm0
	mulss	%xmm1, %xmm0
	subss	.LC63(%rip), %xmm0
	call	__ieee754_expf@PLT
	movss	24(%rsp), %xmm7
	divss	28(%rsp), %xmm7
	movss	20(%rsp), %xmm1
	movaps	%xmm1, %xmm6
	movss	%xmm0, 16(%rsp)
	movss	12(%rsp), %xmm4
	subss	%xmm4, %xmm6
	addss	%xmm4, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm7, %xmm0
	addss	%xmm1, %xmm0
	call	__ieee754_expf@PLT
	mulss	16(%rsp), %xmm0
	testl	%ebx, %ebx
	divss	12(%rsp), %xmm0
	jle	.L32
	ucomiss	.LC67(%rip), %xmm0
	jp	.L20
	jne	.L20
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L42:
	movss	.LC0(%rip), %xmm1
	addq	$32, %rsp
	subss	%xmm0, %xmm1
	popq	%rbx
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	testl	%ebx, %ebx
	jle	.L34
	movq	errno@gottpoff(%rip), %rax
	movss	.LC31(%rip), %xmm0
	mulss	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	testl	%ebx, %ebx
	jns	.L31
	cmpl	$1086324735, %eax
	jle	.L31
.L34:
	movss	.LC66(%rip), %xmm0
	subss	.LC31(%rip), %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	addss	.LC29(%rip), %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm3, %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	movss	.LC48(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC49(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC50(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC51(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC52(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC53(%rip), %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC54(%rip), %xmm0
	movss	%xmm0, 24(%rsp)
	movss	.LC55(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC56(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC57(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC58(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC59(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC60(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	.LC61(%rip), %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 28(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L32:
	movss	.LC66(%rip), %xmm1
	subss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L20
	.size	__erfcf, .-__erfcf
	.weak	erfcf32
	.set	erfcf32,__erfcf
	.weak	erfcf
	.set	erfcf,__erfcf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	1098907648
	.align 4
.LC2:
	.long	1073968596
	.align 4
.LC3:
	.long	1031798784
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	8388608
	.align 4
.LC6:
	.long	1040414164
	.align 4
.LC7:
	.long	3083294385
	.align 4
.LC8:
	.long	1002247305
	.align 4
.LC9:
	.long	1021923983
	.align 4
.LC10:
	.long	1051093995
	.align 4
.LC11:
	.long	3062161946
	.align 4
.LC12:
	.long	957017673
	.align 4
.LC13:
	.long	1000767766
	.align 4
.LC14:
	.long	1032137315
	.align 4
.LC15:
	.long	1053539278
	.align 4
.LC16:
	.long	3138255296
	.align 4
.LC17:
	.long	1024545203
	.align 4
.LC18:
	.long	1038294210
	.align 4
.LC19:
	.long	1050869332
	.align 4
.LC20:
	.long	1052676616
	.align 4
.LC21:
	.long	1054107653
	.align 4
.LC22:
	.long	991612358
	.align 4
.LC23:
	.long	1011112611
	.align 4
.LC24:
	.long	1012887059
	.align 4
.LC25:
	.long	1040265991
	.align 4
.LC26:
	.long	1033050855
	.align 4
.LC27:
	.long	1057642373
	.align 4
.LC28:
	.long	1037693745
	.align 4
.LC29:
	.long	1062753803
	.align 4
.LC30:
	.long	3210237451
	.align 4
.LC31:
	.long	228737632
	.align 4
.LC32:
	.long	3239905150
	.align 4
.LC33:
	.long	1117950763
	.align 4
.LC34:
	.long	1127783143
	.align 4
.LC35:
	.long	1126327692
	.align 4
.LC36:
	.long	1115258967
	.align 4
.LC37:
	.long	1093201954
	.align 4
.LC38:
	.long	1060217015
	.align 4
.LC39:
	.long	1008836755
	.align 4
.LC40:
	.long	3178725271
	.align 4
.LC41:
	.long	1087520636
	.align 4
.LC42:
	.long	1121535263
	.align 4
.LC43:
	.long	1138131211
	.align 4
.LC44:
	.long	1143036105
	.align 4
.LC45:
	.long	1138313327
	.align 4
.LC46:
	.long	1124706403
	.align 4
.LC47:
	.long	1100821966
	.align 4
.LC48:
	.long	3287401077
	.align 4
.LC49:
	.long	1149248267
	.align 4
.LC50:
	.long	1142907969
	.align 4
.LC51:
	.long	1126212330
	.align 4
.LC52:
	.long	1099829323
	.align 4
.LC53:
	.long	1061985748
	.align 4
.LC54:
	.long	1008836754
	.align 4
.LC55:
	.long	3249768210
	.align 4
.LC56:
	.long	1139622823
	.align 4
.LC57:
	.long	1159696590
	.align 4
.LC58:
	.long	1162345915
	.align 4
.LC59:
	.long	1153439577
	.align 4
.LC60:
	.long	1134749041
	.align 4
.LC61:
	.long	1106424921
	.section	.rodata.cst16
	.align 16
.LC62:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC63:
	.long	1058013184
	.align 4
.LC64:
	.long	1056964608
	.align 4
.LC65:
	.long	1042196436
	.align 4
.LC66:
	.long	1073741824
	.align 4
.LC67:
	.long	0
