	.text
	.p2align 4,,15
	.globl	__cbrtl
	.type	__cbrtl, @function
__cbrtl:
	subq	$56, %rsp
	fldt	64(%rsp)
	leaq	44(%rsp), %rdi
	subq	$16, %rsp
	fld	%st(0)
	fstpt	32(%rsp)
	fabs
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__frexpl@PLT
	movl	60(%rsp), %ecx
	popq	%rsi
	popq	%rdi
	testl	%ecx, %ecx
	fldt	(%rsp)
	fldt	16(%rsp)
	jne	.L14
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L15
	fldt	.LC0(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L16
	fldt	.LC1(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L2
	je	.L17
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L14:
	fstp	%st(1)
.L2:
	fld	%st(1)
	movl	%ecx, %eax
	movl	$1431655766, %edi
	imull	%edi
	movl	%ecx, %eax
	fmull	.LC3(%rip)
	sarl	$31, %eax
	movl	%edx, %edi
	subl	%eax, %edi
	leal	(%rdi,%rdi,2), %eax
	faddl	.LC4(%rip)
	subl	%eax, %ecx
	leaq	factor(%rip), %rax
	addl	$2, %ecx
	movslq	%ecx, %rcx
	fmul	%st(2), %st
	fsubl	.LC5(%rip)
	fmul	%st(2), %st
	faddl	.LC6(%rip)
	fmulp	%st, %st(2)
	fxch	%st(1)
	faddl	.LC7(%rip)
	fmull	(%rax,%rcx,8)
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jbe	.L13
	fxch	%st(1)
.L6:
	fstpt	(%rsp)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ldexpl@PLT
	fld	%st(0)
	fmul	%st(1), %st
	fldt	16(%rsp)
	popq	%rax
	popq	%rdx
	fdiv	%st, %st(1)
	fxch	%st(1)
	fsubr	%st(2), %st
	fldt	.LC8(%rip)
	addq	$56, %rsp
	fmul	%st, %st(1)
	fxch	%st(1)
	fsubrp	%st, %st(3)
	fld	%st(2)
	fmul	%st(3), %st
	fdivrp	%st, %st(2)
	fxch	%st(1)
	fsubr	%st(2), %st
	fmulp	%st, %st(1)
	fsubrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	fchs
	fxch	%st(1)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L17:
	fstp	%st(1)
.L3:
	fadd	%st(0), %st
	addq	$56, %rsp
	ret
	.size	__cbrtl, .-__cbrtl
	.weak	cbrtf64x
	.set	cbrtf64x,__cbrtl
	.weak	cbrtl
	.set	cbrtl,__cbrtl
	.section	.rodata
	.align 32
	.type	factor, @object
	.size	factor, 40
factor:
	.long	4186796682
	.long	1071917218
	.long	2772266556
	.long	1072260606
	.long	0
	.long	1072693248
	.long	4186796683
	.long	1072965794
	.long	2772266557
	.long	1073309182
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC1:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	946855926
	.long	-1077855085
	.align 8
.LC4:
	.long	3353714008
	.long	1071742495
	.align 8
.LC5:
	.long	2746890168
	.long	1072597580
	.align 8
.LC6:
	.long	3842344762
	.long	1072840046
	.align 8
.LC7:
	.long	555918590
	.long	1071235265
	.section	.rodata.cst16
	.align 16
.LC8:
	.long	2863311531
	.long	2863311530
	.long	16381
	.long	0
