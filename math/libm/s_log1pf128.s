	.text
	.globl	__addtf3
	.globl	__lttf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__getf2
	.globl	__letf2
	.globl	__eqtf2
	.globl	__divtf3
	.globl	__subtf3
	.globl	__floatsitf
	.p2align 4,,15
	.globl	__log1pf128
	.type	__log1pf128, @function
__log1pf128:
	pushq	%rbx
	subq	$80, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$2147418111, %eax
	jg	.L43
	testl	%eax, %eax
	je	.L44
	cmpl	$1066270719, %eax
	jle	.L5
.L6:
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	js	.L45
.L9:
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jg	.L37
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jne	.L38
	pxor	%xmm1, %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__divtf3@PLT
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%rsp), %rax
	movdqa	(%rsp), %xmm0
	movq	%rax, %rcx
	shrq	$32, %rcx
	orl	%ecx, %edx
	orl	%eax, %edx
	je	.L1
.L5:
	movdqa	(%rsp), %xmm0
	pand	.LC0(%rip), %xmm0
	movdqa	.LC1(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L7
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L7:
	movdqa	(%rsp), %xmm0
	call	__fixtfsi@PLT
	testl	%eax, %eax
	movdqa	(%rsp), %xmm0
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L43:
	pand	.LC0(%rip), %xmm0
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movdqa	%xmm2, %xmm0
	movdqa	.LC3(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	76(%rsp), %rdi
	movdqa	%xmm2, %xmm0
	call	__frexpf128@PLT
	movl	76(%rsp), %ebx
	movaps	%xmm0, 16(%rsp)
	leal	2(%rbx), %eax
	cmpl	$4, %eax
	jbe	.L14
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L46
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC6(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
.L17:
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movl	%ebx, %edi
	movaps	%xmm0, (%rsp)
	call	__floatsitf@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC20(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm3
	movaps	%xmm0, (%rsp)
	movdqa	%xmm3, %xmm0
	movdqa	.LC21(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	call	__divtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	.LC7(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L47
	testl	%ebx, %ebx
	je	.L20
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
.L20:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC22(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC23(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC24(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC25(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC26(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC27(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC28(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC29(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC30(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC31(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC32(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC33(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC34(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC35(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC36(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC37(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC38(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC39(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC40(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC41(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC42(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC43(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC44(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC45(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC46(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movl	%ebx, %edi
	movaps	%xmm0, 48(%rsp)
	call	__floatsitf@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC6(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC21(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L47:
	subl	$1, %ebx
	je	.L20
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L46:
	movdqa	.LC6(%rip), %xmm1
	subl	$1, %ebx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	jmp	.L17
	.size	__log1pf128, .-__log1pf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1081081856
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC7:
	.long	325511829
	.long	3372790523
	.long	3865572284
	.long	1073637897
	.align 16
.LC8:
	.long	3838760712
	.long	1749523796
	.long	478629722
	.long	-1073822710
	.align 16
.LC9:
	.long	2735601192
	.long	1448826385
	.long	3040057118
	.long	1074086471
	.align 16
.LC10:
	.long	1165886384
	.long	3062951389
	.long	1353261326
	.long	1074395667
	.align 16
.LC11:
	.long	3693756872
	.long	3927060692
	.long	3417556205
	.long	1074610208
	.align 16
.LC12:
	.long	3892757290
	.long	948854074
	.long	2490895907
	.long	1074749100
	.align 16
.LC13:
	.long	844150013
	.long	1745004283
	.long	3051546817
	.long	1074795770
	.align 16
.LC14:
	.long	2916428899
	.long	628477308
	.long	873499491
	.long	1074125451
	.align 16
.LC15:
	.long	3922856684
	.long	2418196305
	.long	3676626696
	.long	1074459600
	.align 16
.LC16:
	.long	851125885
	.long	3945266192
	.long	3615278364
	.long	1074708762
	.align 16
.LC17:
	.long	2770410685
	.long	2934725590
	.long	4047370030
	.long	1074890438
	.align 16
.LC18:
	.long	3003253112
	.long	4003369444
	.long	1910524869
	.long	1075004755
	.align 16
.LC19:
	.long	3413710640
	.long	470022776
	.long	282352930
	.long	1075027832
	.align 16
.LC20:
	.long	1060072180
	.long	2654681472
	.long	485989052
	.long	1072398205
	.align 16
.LC21:
	.long	0
	.long	0
	.long	0
	.long	1073636068
	.align 16
.LC22:
	.long	8741116
	.long	2504214818
	.long	2698440066
	.long	1072405764
	.align 16
.LC23:
	.long	445394848
	.long	782022121
	.long	3792914380
	.long	1073610711
	.align 16
.LC24:
	.long	1655018215
	.long	1733051370
	.long	1366883990
	.long	1073967969
	.align 16
.LC25:
	.long	3590128992
	.long	4199782165
	.long	2825100328
	.long	1074240371
	.align 16
.LC26:
	.long	951581226
	.long	1590019597
	.long	2041818642
	.long	1074454046
	.align 16
.LC27:
	.long	3664047852
	.long	1667887306
	.long	1325748431
	.long	1074613410
	.align 16
.LC28:
	.long	1153561700
	.long	290522294
	.long	2441227584
	.long	1074735271
	.align 16
.LC29:
	.long	2166468600
	.long	2515288904
	.long	1783230833
	.long	1074814745
	.align 16
.LC30:
	.long	1907052596
	.long	2722646897
	.long	3130992471
	.long	1074861770
	.align 16
.LC31:
	.long	1684703774
	.long	3835374584
	.long	783309735
	.long	1074865575
	.align 16
.LC32:
	.long	2098957012
	.long	397171827
	.long	2736341389
	.long	1074825596
	.align 16
.LC33:
	.long	3665220616
	.long	2683394341
	.long	2348546465
	.long	1074737039
	.align 16
.LC34:
	.long	1299777404
	.long	3879778388
	.long	3403010287
	.long	1074567805
	.align 16
.LC35:
	.long	2894954405
	.long	1243681754
	.long	4225387839
	.long	1074037538
	.align 16
.LC36:
	.long	3346593135
	.long	2667261922
	.long	343375446
	.long	1074317119
	.align 16
.LC37:
	.long	886286085
	.long	4063926484
	.long	870995301
	.long	1074535897
	.align 16
.LC38:
	.long	997651553
	.long	1262749082
	.long	3615157506
	.long	1074705908
	.align 16
.LC39:
	.long	1224732860
	.long	694297421
	.long	3061800703
	.long	1074837275
	.align 16
.LC40:
	.long	3169996515
	.long	3866347283
	.long	1574618040
	.long	1074932588
	.align 16
.LC41:
	.long	2380495985
	.long	886625134
	.long	1067692091
	.long	1074993848
	.align 16
.LC42:
	.long	244691732
	.long	101923003
	.long	658470442
	.long	1075016152
	.align 16
.LC43:
	.long	3876003188
	.long	3816404440
	.long	3902617826
	.long	1075005691
	.align 16
.LC44:
	.long	3928175223
	.long	1093578671
	.long	555124929
	.long	1074953157
	.align 16
.LC45:
	.long	1363910953
	.long	372740050
	.long	2168456170
	.long	1074856072
	.align 16
.LC46:
	.long	974833098
	.long	3983575615
	.long	1478515891
	.long	1074672606
