	.text
	.p2align 4,,15
	.globl	__acosl
	.type	__acosl, @function
__acosl:
	fldt	8(%rsp)
	fld	%st(0)
	fabs
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L4
	fstpt	8(%rsp)
	jmp	__ieee754_acosl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	fstpt	8(%rsp)
	jmp	__ieee754_acosl@PLT
	.size	__acosl, .-__acosl
	.weak	acosf64x
	.set	acosf64x,__acosl
	.weak	acosl
	.set	acosl,__acosl
