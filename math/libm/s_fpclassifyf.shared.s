	.text
	.p2align 4,,15
	.globl	__GI___fpclassifyf
	.hidden	__GI___fpclassifyf
	.type	__GI___fpclassifyf, @function
__GI___fpclassifyf:
	movd	%xmm0, %edx
	movl	$2, %eax
	andl	$2147483647, %edx
	je	.L1
	cmpl	$8388607, %edx
	movl	$3, %eax
	jbe	.L1
	cmpl	$2139095039, %edx
	movl	$4, %eax
	jbe	.L1
	xorl	%eax, %eax
	cmpl	$2139095040, %edx
	setbe	%al
.L1:
	rep ret
	.size	__GI___fpclassifyf, .-__GI___fpclassifyf
	.globl	__fpclassifyf
	.set	__fpclassifyf,__GI___fpclassifyf
