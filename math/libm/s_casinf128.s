	.text
	.globl	__unordtf2
	.globl	__eqtf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__casinf128
	.type	__casinf128, @function
__casinf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$48, %rsp
	movdqa	64(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	80(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	80(%rsp), %xmm0
	leaq	16(%rsp), %rdi
	pushq	72(%rsp)
	pushq	72(%rsp)
	pxor	.LC5(%rip), %xmm0
	subq	$16, %rsp
	movups	%xmm0, (%rsp)
	call	__casinhf128@PLT
	movdqa	48(%rsp), %xmm0
	pxor	.LC5(%rip), %xmm0
	movdqa	64(%rsp), %xmm5
	movaps	%xmm5, 96(%rsp)
	movaps	%xmm0, 112(%rsp)
	addq	$32, %rsp
.L5:
	movq	%rbx, %rax
	movdqa	64(%rsp), %xmm3
	movdqa	80(%rsp), %xmm4
	movaps	%xmm3, (%rbx)
	movaps	%xmm4, 16(%rbx)
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pxor	%xmm1, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L5
	movdqa	64(%rsp), %xmm2
	pand	.LC2(%rip), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L9
	movdqa	(%rsp), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L6
.L9:
	movdqa	80(%rsp), %xmm2
	pand	.LC2(%rip), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L8
	movdqa	(%rsp), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L8
.L6:
	movdqa	80(%rsp), %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	.LC0(%rip), %xmm6
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm6, 64(%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	.LC0(%rip), %xmm7
	movaps	%xmm7, 64(%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L5
	.size	__casinf128, .-__casinf128
	.weak	casinf128
	.set	casinf128,__casinf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
