	.text
	.p2align 4,,15
	.globl	__acoshl
	.type	__acoshl, @function
__acoshl:
	fldt	8(%rsp)
	fld1
	fucomip	%st(1), %st
	ja	.L4
	fstpt	8(%rsp)
	jmp	__ieee754_acoshl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	fstpt	8(%rsp)
	jmp	__ieee754_acoshl@PLT
	.size	__acoshl, .-__acoshl
	.weak	acoshf64x
	.set	acoshf64x,__acoshl
	.weak	acoshl
	.set	acoshl,__acoshl
