	.text
	.p2align 4,,15
	.globl	__scalb
	.type	__scalb, @function
__scalb:
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_scalb@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm3
	movsd	(%rsp), %xmm2
	andpd	%xmm1, %xmm5
	movsd	8(%rsp), %xmm4
	ucomisd	%xmm5, %xmm3
	jb	.L2
	ucomisd	.LC2(%rip), %xmm0
	jp	.L1
	je	.L2
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm0, %xmm0
	jp	.L13
	ucomisd	%xmm3, %xmm5
	jbe	.L6
	andpd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	ja	.L1
.L9:
	andpd	%xmm1, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	ucomisd	.LC2(%rip), %xmm2
	jp	.L9
	jne	.L9
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	ucomisd	%xmm4, %xmm2
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__scalb, .-__scalb
	.weak	scalbf32x
	.set	scalbf32x,__scalb
	.weak	scalbf64
	.set	scalbf64,__scalb
	.weak	scalb
	.set	scalb,__scalb
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	0
