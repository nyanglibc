	.text
	.p2align 4,,15
	.globl	fetestexceptflag
	.type	fetestexceptflag, @function
fetestexceptflag:
	movzwl	(%rdi), %eax
	andl	$61, %esi
	andl	%esi, %eax
	ret
	.size	fetestexceptflag, .-fetestexceptflag
