	.text
	.p2align 4,,15
	.globl	__totalordermagf128
	.type	__totalordermagf128, @function
__totalordermagf128:
	movdqa	(%rdi), %xmm0
	movabsq	$9223372036854775807, %rax
	movdqa	(%rsi), %xmm1
	movaps	%xmm0, -40(%rsp)
	movq	-32(%rsp), %rcx
	movaps	%xmm1, -24(%rsp)
	andq	%rax, %rcx
	movq	-16(%rsp), %rdx
	andq	%rax, %rdx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	jb	.L1
	movq	-40(%rsp), %rcx
	movq	-24(%rsp), %rdx
	sete	%sil
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	setbe	%al
	andl	%esi, %eax
.L1:
	rep ret
	.size	__totalordermagf128, .-__totalordermagf128
	.weak	totalordermagf128
	.set	totalordermagf128,__totalordermagf128
