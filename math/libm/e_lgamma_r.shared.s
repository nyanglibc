	.text
#APP
	.symver __ieee754_lgamma_r,__lgamma_r_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_lgamma_r
	.type	__ieee754_lgamma_r, @function
__ieee754_lgamma_r:
	pushq	%r12
	movq	%xmm0, %r12
	pushq	%rbp
	pushq	%rbx
	movapd	%xmm0, %xmm3
	shrq	$32, %r12
	movl	%r12d, %ebx
	subq	$48, %rsp
	movl	$1, (%rdi)
	andl	$2147483647, %ebx
	cmpl	$2146435071, %ebx
	jg	.L70
	movq	%xmm0, %rbp
	movl	%ebx, %eax
	orl	%ebp, %eax
	je	.L71
	cmpl	$999292927, %ebx
	jle	.L72
	testl	%r12d, %r12d
	js	.L73
	leal	-1072693248(%rbx), %eax
	pxor	%xmm1, %xmm1
	orl	%ebp, %eax
	je	.L1
	leal	-1073741824(%rbx), %eax
	pxor	%xmm1, %xmm1
	orl	%ebp, %eax
	je	.L1
.L49:
	cmpl	$1073741823, %ebx
	jg	.L32
	cmpl	$1072483532, %ebx
	jle	.L74
	cmpl	$1073460418, %ebx
	jle	.L38
	movsd	.LC11(%rip), %xmm4
	pxor	%xmm0, %xmm0
	subsd	%xmm3, %xmm4
.L35:
	movapd	%xmm4, %xmm5
	movsd	.LC15(%rip), %xmm1
	movsd	.LC21(%rip), %xmm3
	mulsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm3
	addsd	.LC16(%rip), %xmm1
	addsd	.LC22(%rip), %xmm3
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm3
	addsd	.LC17(%rip), %xmm1
	addsd	.LC23(%rip), %xmm3
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm3
	addsd	.LC18(%rip), %xmm1
	addsd	.LC24(%rip), %xmm3
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm3
	addsd	.LC19(%rip), %xmm1
	addsd	.LC25(%rip), %xmm3
	mulsd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm3
	addsd	.LC20(%rip), %xmm1
	addsd	.LC26(%rip), %xmm3
	mulsd	%xmm4, %xmm1
	mulsd	%xmm5, %xmm3
	mulsd	.LC8(%rip), %xmm4
	addsd	%xmm3, %xmm1
	subsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	.p2align 4,,10
	.p2align 3
.L40:
	testl	%r12d, %r12d
	js	.L31
.L1:
	addq	$48, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$1127219199, %ebx
	jg	.L75
	movsd	.LC4(%rip), %xmm0
	ucomisd	%xmm3, %xmm0
	jbe	.L10
	ucomisd	.LC5(%rip), %xmm3
	ja	.L76
.L10:
	cmpl	$1070596095, %ebx
	jle	.L77
	movq	.LC3(%rip), %xmm4
	movapd	%xmm3, %xmm0
	movq	.LC2(%rip), %xmm6
	xorpd	%xmm4, %xmm0
	movsd	.LC7(%rip), %xmm1
	movapd	%xmm6, %xmm7
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm5
	andpd	%xmm6, %xmm2
	ucomisd	%xmm2, %xmm1
	jbe	.L15
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC1(%rip), %xmm5
	andnpd	%xmm0, %xmm7
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm8
	cmpnlesd	%xmm0, %xmm8
	andpd	%xmm5, %xmm8
	movapd	%xmm2, %xmm5
	subsd	%xmm8, %xmm5
	orpd	%xmm7, %xmm5
.L15:
	ucomisd	%xmm5, %xmm0
	jp	.L60
	jne	.L60
	subsd	%xmm3, %xmm1
	pxor	%xmm0, %xmm0
	movq	%xmm1, %rax
	andl	$1, %eax
	cvtsi2sd	%eax, %xmm0
	sall	$2, %eax
.L19:
	cmpl	$6, %eax
	ja	.L20
	leaq	.L22(%rip), %rdx
	movl	%eax, %eax
	movq	%rdi, 24(%rsp)
	movaps	%xmm4, 32(%rsp)
	movslq	(%rdx,%rax,4), %rax
	movsd	%xmm3, 16(%rsp)
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L22:
	.long	.L68-.L22
	.long	.L23-.L22
	.long	.L23-.L22
	.long	.L24-.L22
	.long	.L24-.L22
	.long	.L25-.L22
	.long	.L25-.L22
	.text
	.p2align 4,,10
	.p2align 3
.L24:
	movsd	.LC1(%rip), %xmm5
	subsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
.L68:
	movsd	.LC6(%rip), %xmm2
	mulsd	%xmm2, %xmm0
	movsd	%xmm2, (%rsp)
	call	__sin@PLT
	movq	24(%rsp), %rdi
	movsd	(%rsp), %xmm2
	movsd	16(%rsp), %xmm3
	movapd	32(%rsp), %xmm4
.L26:
	xorpd	%xmm0, %xmm4
.L14:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm4
	jp	.L27
	jne	.L27
	cvtsd2ss	%xmm4, %xmm4
	andps	.LC12(%rip), %xmm4
	movsd	.LC1(%rip), %xmm1
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	cvtss2sd	%xmm4, %xmm4
	popq	%r12
	divsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	mulsd	%xmm0, %xmm3
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	movapd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	testl	%r12d, %r12d
	jns	.L5
	movl	$-1, (%rdi)
.L5:
	andpd	.LC2(%rip), %xmm3
	movsd	.LC1(%rip), %xmm1
	divsd	%xmm3, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L72:
	testl	%r12d, %r12d
	js	.L78
	call	__ieee754_log@PLT
	movapd	%xmm0, %xmm1
	xorpd	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movapd	%xmm3, %xmm0
	movsd	%xmm1, 32(%rsp)
	movq	%rdi, 24(%rsp)
	movsd	%xmm3, 16(%rsp)
	mulsd	%xmm4, %xmm0
	movsd	%xmm4, (%rsp)
	andpd	.LC2(%rip), %xmm0
	divsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	call	__ieee754_log@PLT
	movsd	(%rsp), %xmm4
	movq	24(%rsp), %rdi
	movsd	32(%rsp), %xmm1
	movapd	%xmm0, %xmm2
	movsd	16(%rsp), %xmm3
	ucomisd	%xmm4, %xmm1
	ja	.L79
.L29:
	leal	-1072693248(%rbx), %eax
	orl	%ebp, %eax
	jne	.L80
.L31:
	subsd	%xmm1, %xmm2
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	movapd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$1075838975, %ebx
	jg	.L41
	cvttsd2si	%xmm3, %eax
	pxor	%xmm0, %xmm0
	movsd	.LC55(%rip), %xmm1
	movsd	.LC1(%rip), %xmm5
	cvtsi2sd	%eax, %xmm0
	subl	$3, %eax
	cmpl	$4, %eax
	subsd	%xmm0, %xmm3
	movsd	.LC61(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	.LC56(%rip), %xmm1
	addsd	.LC62(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	.LC57(%rip), %xmm1
	addsd	.LC63(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	.LC58(%rip), %xmm1
	addsd	.LC64(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	.LC59(%rip), %xmm1
	addsd	.LC65(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	.LC60(%rip), %xmm1
	addsd	.LC66(%rip), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm0
	subsd	.LC20(%rip), %xmm1
	addsd	%xmm5, %xmm0
	mulsd	%xmm3, %xmm1
	divsd	%xmm0, %xmm1
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm1
	ja	.L40
	leaq	.L43(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L43:
	.long	.L52-.L43
	.long	.L44-.L43
	.long	.L54-.L43
	.long	.L55-.L43
	.long	.L47-.L43
	.text
	.p2align 4,,10
	.p2align 3
.L60:
	mulsd	.LC8(%rip), %xmm0
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm5
	andpd	%xmm6, %xmm2
	ucomisd	%xmm2, %xmm1
	ja	.L81
.L18:
	subsd	%xmm5, %xmm0
	movsd	.LC9(%rip), %xmm1
	addsd	%xmm0, %xmm0
	mulsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %eax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L77:
	movsd	.LC6(%rip), %xmm2
	movq	%rdi, 24(%rsp)
	movapd	%xmm3, %xmm0
	movsd	%xmm3, 16(%rsp)
	movsd	%xmm2, (%rsp)
	mulsd	%xmm2, %xmm0
	call	__sin@PLT
	movsd	(%rsp), %xmm2
	movq	24(%rsp), %rdi
	movapd	%xmm0, %xmm4
	movsd	16(%rsp), %xmm3
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$1133510655, %ebx
	movsd	%xmm2, 16(%rsp)
	movapd	%xmm3, %xmm0
	movsd	%xmm3, (%rsp)
	jg	.L48
	call	__ieee754_log@PLT
	movsd	.LC1(%rip), %xmm5
	movsd	(%rsp), %xmm3
	movapd	%xmm5, %xmm1
	movsd	.LC70(%rip), %xmm4
	subsd	%xmm5, %xmm0
	movsd	16(%rsp), %xmm2
	divsd	%xmm3, %xmm1
	subsd	.LC8(%rip), %xmm3
	movapd	%xmm1, %xmm6
	mulsd	%xmm1, %xmm6
	mulsd	%xmm6, %xmm4
	addsd	.LC71(%rip), %xmm4
	mulsd	%xmm6, %xmm4
	subsd	.LC72(%rip), %xmm4
	mulsd	%xmm6, %xmm4
	addsd	.LC73(%rip), %xmm4
	mulsd	%xmm6, %xmm4
	subsd	.LC74(%rip), %xmm4
	mulsd	%xmm6, %xmm4
	addsd	.LC75(%rip), %xmm4
	mulsd	%xmm1, %xmm4
	movapd	%xmm3, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	.LC76(%rip), %xmm4
	addsd	%xmm4, %xmm1
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L81:
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC1(%rip), %xmm5
	andnpd	%xmm0, %xmm6
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm1
	cmpnlesd	%xmm0, %xmm1
	andpd	%xmm5, %xmm1
	movapd	%xmm2, %xmm5
	subsd	%xmm1, %xmm5
	orpd	%xmm6, %xmm5
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$-1, (%rdi)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L74:
	movapd	%xmm3, %xmm0
	movsd	%xmm2, 16(%rsp)
	movsd	%xmm3, (%rsp)
	call	__ieee754_log@PLT
	cmpl	$1072130371, %ebx
	movsd	(%rsp), %xmm3
	xorpd	.LC3(%rip), %xmm0
	movsd	16(%rsp), %xmm2
	jle	.L34
	movsd	.LC1(%rip), %xmm4
	subsd	%xmm3, %xmm4
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L23:
	movsd	.LC8(%rip), %xmm1
	movsd	.LC6(%rip), %xmm2
	subsd	%xmm0, %xmm1
	movsd	%xmm2, (%rsp)
	movapd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	call	__cos@PLT
	movsd	(%rsp), %xmm2
	movq	24(%rsp), %rdi
	movsd	16(%rsp), %xmm3
	movapd	32(%rsp), %xmm4
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L25:
	subsd	.LC10(%rip), %xmm0
	movsd	.LC6(%rip), %xmm2
	movsd	%xmm2, (%rsp)
	mulsd	%xmm2, %xmm0
	call	__cos@PLT
	movapd	32(%rsp), %xmm4
	movq	24(%rsp), %rdi
	xorpd	%xmm4, %xmm0
	movsd	(%rsp), %xmm2
	movsd	16(%rsp), %xmm3
	jmp	.L26
.L54:
	movapd	%xmm5, %xmm0
.L45:
	movsd	.LC9(%rip), %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm0, %xmm5
.L44:
	movsd	.LC69(%rip), %xmm0
	addsd	%xmm3, %xmm0
	mulsd	%xmm5, %xmm0
.L42:
	addsd	.LC11(%rip), %xmm3
	movsd	%xmm1, 16(%rsp)
	movsd	%xmm2, (%rsp)
	mulsd	%xmm3, %xmm0
	call	__ieee754_log@PLT
	movsd	16(%rsp), %xmm1
	movsd	(%rsp), %xmm2
	addsd	%xmm0, %xmm1
	jmp	.L40
.L52:
	movapd	%xmm5, %xmm0
	jmp	.L42
.L55:
	movapd	%xmm5, %xmm0
.L46:
	movsd	.LC68(%rip), %xmm5
	addsd	%xmm3, %xmm5
	mulsd	%xmm5, %xmm0
	jmp	.L45
.L47:
	movsd	.LC67(%rip), %xmm0
	addsd	%xmm3, %xmm0
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L76:
	addq	$48, %rsp
	movapd	%xmm3, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__lgamma_neg@PLT
	.p2align 4,,10
	.p2align 3
.L78:
	movq	.LC3(%rip), %xmm1
	movl	$-1, (%rdi)
	xorpd	%xmm1, %xmm3
	movaps	%xmm1, (%rsp)
	movapd	%xmm3, %xmm0
	call	__ieee754_log@PLT
	movapd	(%rsp), %xmm1
	xorpd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L75:
	andpd	.LC2(%rip), %xmm3
	movapd	%xmm3, %xmm1
	divsd	.LC0(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$1072936131, %ebx
	jle	.L39
	subsd	.LC14(%rip), %xmm3
	pxor	%xmm0, %xmm0
.L37:
	movapd	%xmm3, %xmm1
	movsd	.LC27(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm5
	mulsd	%xmm3, %xmm5
	mulsd	%xmm5, %xmm4
	subsd	.LC28(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	addsd	.LC29(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	subsd	.LC30(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	addsd	.LC31(%rip), %xmm4
	mulsd	%xmm1, %xmm4
	movsd	.LC32(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	subsd	.LC33(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC34(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	subsd	.LC35(%rip), %xmm1
	mulsd	%xmm5, %xmm1
	addsd	.LC36(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	movsd	.LC37(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	addsd	.LC38(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	subsd	.LC39(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	addsd	.LC40(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	subsd	.LC41(%rip), %xmm3
	addsd	%xmm3, %xmm1
	movsd	.LC42(%rip), %xmm3
	mulsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm1
	subsd	.LC43(%rip), %xmm1
	addsd	%xmm0, %xmm1
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L20:
	subsd	.LC11(%rip), %xmm0
	movsd	.LC6(%rip), %xmm2
	movaps	%xmm4, 32(%rsp)
	movq	%rdi, 24(%rsp)
	movsd	%xmm3, 16(%rsp)
	movsd	%xmm2, (%rsp)
	mulsd	%xmm2, %xmm0
	call	__sin@PLT
	movapd	32(%rsp), %xmm4
	movq	24(%rsp), %rdi
	movsd	16(%rsp), %xmm3
	movsd	(%rsp), %xmm2
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L48:
	call	__ieee754_log@PLT
	subsd	.LC1(%rip), %xmm0
	movsd	(%rsp), %xmm3
	movsd	16(%rsp), %xmm2
	movapd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm1
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L39:
	movsd	.LC1(%rip), %xmm5
	pxor	%xmm0, %xmm0
	subsd	%xmm5, %xmm3
.L36:
	movsd	.LC44(%rip), %xmm1
	movsd	.LC49(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm4
	addsd	.LC45(%rip), %xmm1
	addsd	.LC50(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm4
	addsd	.LC46(%rip), %xmm1
	addsd	.LC51(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm4
	addsd	.LC47(%rip), %xmm1
	addsd	.LC52(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm4
	addsd	.LC48(%rip), %xmm1
	addsd	.LC53(%rip), %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm4
	subsd	.LC20(%rip), %xmm1
	addsd	%xmm4, %xmm5
	mulsd	%xmm3, %xmm1
	mulsd	.LC54(%rip), %xmm3
	divsd	%xmm5, %xmm1
	addsd	%xmm3, %xmm1
	addsd	%xmm0, %xmm1
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$1070442080, %ebx
	movsd	.LC1(%rip), %xmm5
	jle	.L36
	subsd	.LC13(%rip), %xmm3
	jmp	.L37
.L80:
	leal	-1073741824(%rbx), %eax
	xorpd	.LC3(%rip), %xmm3
	orl	%ebp, %eax
	je	.L31
	jmp	.L49
	.size	__ieee754_lgamma_r, .-__ieee754_lgamma_r
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	-1073741824
	.align 8
.LC5:
	.long	0
	.long	-1069809664
	.align 8
.LC6:
	.long	1413754136
	.long	1074340347
	.align 8
.LC7:
	.long	0
	.long	1127219200
	.align 8
.LC8:
	.long	0
	.long	1071644672
	.align 8
.LC9:
	.long	0
	.long	1074790400
	.align 8
.LC10:
	.long	0
	.long	1073217536
	.align 8
.LC11:
	.long	0
	.long	1073741824
	.section	.rodata.cst16
	.align 16
.LC12:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC13:
	.long	2371549436
	.long	1071483745
	.align 8
.LC14:
	.long	1666629183
	.long	1073177304
	.align 8
.LC15:
	.long	1116535378
	.long	1056600180
	.align 8
.LC16:
	.long	3977307469
	.long	1059910380
	.align 8
.LC17:
	.long	292503389
	.long	1062439572
	.align 8
.LC18:
	.long	3062886376
	.long	1065238607
	.align 8
.LC19:
	.long	441803431
	.long	1068580352
	.align 8
.LC20:
	.long	3816665288
	.long	1068745831
	.align 8
.LC21:
	.long	2426689591
	.long	1057457550
	.align 8
.LC22:
	.long	2558393095
	.long	1058820232
	.align 8
.LC23:
	.long	2310642688
	.long	1061205702
	.align 8
.LC24:
	.long	3434582635
	.long	1063759320
	.align 8
.LC25:
	.long	2895271035
	.long	1066734370
	.align 8
.LC26:
	.long	3299217325
	.long	1070900044
	.align 8
.LC27:
	.long	1812904951
	.long	1060417389
	.align 8
.LC28:
	.long	3207404273
	.long	1062665870
	.align 8
.LC29:
	.long	3815826244
	.long	1064893664
	.align 8
.LC30:
	.long	3744839443
	.long	1067501992
	.align 8
.LC31:
	.long	3371055266
	.long	1071576875
	.align 8
.LC32:
	.long	3905082356
	.long	1060502846
	.align 8
.LC33:
	.long	2624839916
	.long	1061266960
	.align 8
.LC34:
	.long	773179669
	.long	1063420627
	.align 8
.LC35:
	.long	3130125418
	.long	1065688991
	.align 8
.LC36:
	.long	2497003931
	.long	1068534594
	.align 8
.LC37:
	.long	3972238392
	.long	-1087078620
	.align 8
.LC38:
	.long	4016154857
	.long	1062002444
	.align 8
.LC39:
	.long	3018396887
	.long	1064185599
	.align 8
.LC40:
	.long	2534078956
	.long	1066559207
	.align 8
.LC41:
	.long	2378614025
	.long	1069736999
	.align 8
.LC42:
	.long	2760546079
	.long	-1135556662
	.align 8
.LC43:
	.long	3166931522
	.long	1069488569
	.align 8
.LC44:
	.long	3207310089
	.long	1066100619
	.align 8
.LC45:
	.long	4127263012
	.long	1070419630
	.align 8
.LC46:
	.long	1156219984
	.long	1072646518
	.align 8
.LC47:
	.long	3508125039
	.long	1073170268
	.align 8
.LC48:
	.long	2332057087
	.long	1071923230
	.align 8
.LC49:
	.long	1473302369
	.long	1063934651
	.align 8
.LC50:
	.long	3595795592
	.long	1069198933
	.align 8
.LC51:
	.long	3830468783
	.long	1072209403
	.align 8
.LC52:
	.long	2754287861
	.long	1073809189
	.align 8
.LC53:
	.long	3267191196
	.long	1073980887
	.align 8
.LC54:
	.long	0
	.long	-1075838976
	.align 8
.LC55:
	.long	3709331781
	.long	1057013740
	.align 8
.LC56:
	.long	1936257593
	.long	1063134902
	.align 8
.LC57:
	.long	2123602273
	.long	1067141148
	.align 8
.LC58:
	.long	3202740983
	.long	1069726620
	.align 8
.LC59:
	.long	1326686041
	.long	1070913935
	.align 8
.LC60:
	.long	920782968
	.long	1070302347
	.align 8
.LC61:
	.long	2780004672
	.long	1054784247
	.align 8
.LC62:
	.long	3393300827
	.long	1061780954
	.align 8
.LC63:
	.long	1949226101
	.long	1066604522
	.align 8
.LC64:
	.long	3439058727
	.long	1069941229
	.align 8
.LC65:
	.long	2480135388
	.long	1072110104
	.align 8
.LC66:
	.long	1657056116
	.long	1073104295
	.align 8
.LC67:
	.long	0
	.long	1075314688
	.align 8
.LC68:
	.long	0
	.long	1075052544
	.align 8
.LC69:
	.long	0
	.long	1074266112
	.align 8
.LC70:
	.long	194921444
	.long	-1084573539
	.align 8
.LC71:
	.long	1289410001
	.long	1061906362
	.align 8
.LC72:
	.long	2349852481
	.long	1061388491
	.align 8
.LC73:
	.long	2563717302
	.long	1061814687
	.align 8
.LC74:
	.long	380644956
	.long	1063698796
	.align 8
.LC75:
	.long	1431655739
	.long	1068848469
	.align 8
.LC76:
	.long	2429123945
	.long	1071304675
