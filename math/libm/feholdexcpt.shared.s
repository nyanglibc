	.text
	.p2align 4,,15
	.globl	__GI___feholdexcept
	.hidden	__GI___feholdexcept
	.type	__GI___feholdexcept, @function
__GI___feholdexcept:
#APP
# 28 "../sysdeps/x86_64/fpu/feholdexcpt.c" 1
	fnstenv (%rdi)
	stmxcsr 28(%rdi)
	fnclex
# 0 "" 2
#NO_APP
	movl	28(%rdi), %eax
	andl	$-8128, %eax
	orl	$8064, %eax
	movl	%eax, -4(%rsp)
#APP
# 35 "../sysdeps/x86_64/fpu/feholdexcpt.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	__GI___feholdexcept, .-__GI___feholdexcept
	.globl	__feholdexcept
	.set	__feholdexcept,__GI___feholdexcept
	.weak	__GI_feholdexcept
	.hidden	__GI_feholdexcept
	.set	__GI_feholdexcept,__feholdexcept
	.weak	feholdexcept
	.set	feholdexcept,__GI_feholdexcept
