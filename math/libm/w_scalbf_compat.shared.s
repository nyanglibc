	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	sysv_scalbf, @function
sysv_scalbf:
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_scalbf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	8(%rsp), %xmm2
	andps	%xmm1, %xmm4
	movss	12(%rsp), %xmm3
	ucomiss	.LC1(%rip), %xmm4
	jbe	.L2
	andps	%xmm2, %xmm1
	movss	.LC1(%rip), %xmm4
	ucomiss	%xmm1, %xmm4
	jnb	.L13
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm0, %xmm2
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	pxor	%xmm1, %xmm1
	movl	$0, %edx
	ucomiss	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	movaps	%xmm3, %xmm1
	movl	$133, %edi
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	movaps	%xmm3, %xmm1
	movl	$132, %edi
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	sysv_scalbf, .-sysv_scalbf
	.p2align 4,,15
	.globl	__scalbf
	.type	__scalbf, @function
__scalbf:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	je	.L33
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_scalbf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm5
	andps	%xmm1, %xmm4
	movss	8(%rsp), %xmm2
	movss	12(%rsp), %xmm3
	ucomiss	%xmm4, %xmm5
	jb	.L16
	ucomiss	.LC2(%rip), %xmm0
	jp	.L14
	je	.L16
.L14:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	ucomiss	%xmm0, %xmm0
	jp	.L34
	ucomiss	.LC1(%rip), %xmm4
	jbe	.L20
	andps	%xmm1, %xmm2
	ucomiss	.LC1(%rip), %xmm2
	ja	.L14
.L27:
	andps	%xmm3, %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L14
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L33:
	jmp	sysv_scalbf
	.p2align 4,,10
	.p2align 3
.L20:
	ucomiss	.LC2(%rip), %xmm2
	jp	.L27
	jne	.L27
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L34:
	ucomiss	%xmm3, %xmm2
	jp	.L14
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L14
	.size	__scalbf, .-__scalbf
	.weak	scalbf
	.set	scalbf,__scalbf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	0
