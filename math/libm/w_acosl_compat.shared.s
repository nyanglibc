	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__acosl
	.type	__acosl, @function
__acosl:
	subq	$24, %rsp
	fldt	32(%rsp)
	fld	%st(0)
	fabs
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L8
.L2:
	fstpt	32(%rsp)
	addq	$24, %rsp
	jmp	__ieee754_acosl@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	fstpt	(%rsp)
	movl	$1, %edi
	call	__GI_feraiseexcept
	subq	$32, %rsp
	movl	$201, %edi
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$56, %rsp
	ret
	.size	__acosl, .-__acosl
	.weak	acosf64x
	.set	acosf64x,__acosl
	.weak	acosl
	.set	acosl,__acosl
