	.text
	.p2align 4,,15
	.globl	__clog
	.type	__clog, @function
__clog:
	movq	.LC4(%rip), %xmm5
	movapd	%xmm0, %xmm3
	andpd	%xmm5, %xmm3
	ucomisd	%xmm3, %xmm3
	jp	.L2
	movsd	.LC5(%rip), %xmm6
	ucomisd	%xmm6, %xmm3
	jbe	.L91
	andpd	%xmm1, %xmm5
	ucomisd	%xmm5, %xmm5
	movapd	%xmm5, %xmm2
	jp	.L43
.L8:
	pushq	%rbx
	subq	$32, %rsp
	ucomisd	%xmm3, %xmm2
	ja	.L15
	movapd	%xmm2, %xmm4
	movapd	%xmm3, %xmm2
	movapd	%xmm4, %xmm3
.L15:
	ucomisd	.LC9(%rip), %xmm2
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	ja	.L92
	movsd	.LC6(%rip), %xmm4
	ucomisd	%xmm2, %xmm4
	jbe	.L20
	ucomisd	%xmm3, %xmm4
	ja	.L93
.L20:
	movsd	.LC11(%rip), %xmm1
	movsd	%xmm4, 16(%rsp)
	ucomisd	%xmm1, %xmm2
	jp	.L48
	jne	.L48
	mulsd	%xmm3, %xmm3
	movapd	%xmm3, %xmm0
	call	__log1p@PLT
	movsd	.LC12(%rip), %xmm2
	movsd	16(%rsp), %xmm4
	mulsd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L24
	movapd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm0
.L24:
	movsd	(%rsp), %xmm1
	movsd	8(%rsp), %xmm0
	movsd	%xmm2, 16(%rsp)
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm2
	addq	$32, %rsp
	movapd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	movsd	.LC6(%rip), %xmm7
	ucomisd	%xmm7, %xmm3
	jnb	.L4
	pxor	%xmm4, %xmm4
	ucomisd	%xmm4, %xmm0
	jp	.L4
	jne	.L4
	movapd	%xmm1, %xmm2
	andpd	%xmm5, %xmm2
	ucomisd	%xmm2, %xmm2
	jp	.L83
	ucomisd	%xmm6, %xmm2
	ja	.L8
	ucomisd	%xmm7, %xmm2
	jnb	.L8
	ucomisd	%xmm4, %xmm1
	jp	.L8
	jne	.L8
	movmskpd	%xmm0, %eax
	testb	$1, %al
	je	.L14
	movsd	.LC0(%rip), %xmm4
.L14:
	movsd	.LC8(%rip), %xmm2
	movapd	%xmm1, %xmm7
	andpd	%xmm5, %xmm4
	divsd	%xmm3, %xmm2
	andpd	.LC7(%rip), %xmm7
	movapd	%xmm2, %xmm0
	orpd	%xmm7, %xmm4
	movapd	%xmm4, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	andpd	%xmm1, %xmm5
	ucomisd	%xmm5, %xmm5
	movapd	%xmm5, %xmm2
	jnp	.L8
.L83:
	movsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movapd	%xmm2, %xmm0
	movl	$-1, %edi
	movsd	%xmm3, 16(%rsp)
	call	__scalbn@PLT
	movsd	16(%rsp), %xmm3
	movapd	%xmm0, %xmm2
	ucomisd	.LC10(%rip), %xmm3
	jnb	.L94
	pxor	%xmm3, %xmm3
	movl	$-1, %ebx
	movsd	.LC11(%rip), %xmm1
.L18:
	ucomisd	%xmm1, %xmm2
	jbe	.L26
	movsd	.LC13(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	jbe	.L26
	ucomisd	%xmm3, %xmm1
	jbe	.L26
	testl	%ebx, %ebx
	je	.L95
	.p2align 4,,10
	.p2align 3
.L26:
	ucomisd	%xmm2, %xmm1
	ja	.L96
.L31:
	movapd	%xmm3, %xmm1
	movapd	%xmm2, %xmm0
	call	__ieee754_hypot@PLT
	call	__ieee754_log@PLT
	pxor	%xmm1, %xmm1
	movapd	%xmm0, %xmm2
	cvtsi2sd	%ebx, %xmm1
	mulsd	.LC16(%rip), %xmm1
	subsd	%xmm1, %xmm2
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L93:
	movapd	%xmm2, %xmm0
	movl	$53, %edi
	movsd	%xmm3, 24(%rsp)
	movl	$53, %ebx
	call	__scalbn@PLT
	movsd	24(%rsp), %xmm3
	movl	$53, %edi
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm3, %xmm0
	call	__scalbn@PLT
	movsd	.LC11(%rip), %xmm1
	movapd	%xmm0, %xmm3
	movsd	16(%rsp), %xmm2
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L94:
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm3, %xmm0
	movl	$-1, %edi
	movl	$-1, %ebx
	call	__scalbn@PLT
	movsd	.LC11(%rip), %xmm1
	movapd	%xmm0, %xmm3
	movsd	16(%rsp), %xmm2
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%ebx, %ebx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L96:
	movsd	.LC12(%rip), %xmm4
	testl	%ebx, %ebx
	sete	%al
	ucomisd	%xmm4, %xmm2
	jb	.L33
	movsd	.LC15(%rip), %xmm0
	ucomisd	%xmm3, %xmm0
	jbe	.L33
	testb	%al, %al
	jne	.L97
.L33:
	ucomisd	.LC12(%rip), %xmm2
	jb	.L31
	testb	%al, %al
	je	.L31
	movapd	%xmm3, %xmm1
	movapd	%xmm2, %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	ucomisd	%xmm4, %xmm0
	jb	.L31
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	movsd	%xmm4, 16(%rsp)
	call	__x2y2m1@PLT
	call	__log1p@PLT
	movsd	16(%rsp), %xmm4
	movapd	%xmm0, %xmm2
	mulsd	%xmm4, %xmm2
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L2:
	andpd	%xmm5, %xmm1
	ucomisd	%xmm1, %xmm1
	jp	.L83
	ucomisd	.LC5(%rip), %xmm1
	jbe	.L83
.L43:
	movsd	.LC3(%rip), %xmm1
	movsd	.LC2(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	movapd	%xmm2, %xmm0
	movsd	%xmm4, 16(%rsp)
	addsd	%xmm1, %xmm2
	subsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	call	__log1p@PLT
	movsd	16(%rsp), %xmm4
	movapd	%xmm0, %xmm2
	mulsd	%xmm4, %xmm2
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L95:
	movapd	%xmm2, %xmm0
	ucomisd	.LC14(%rip), %xmm3
	subsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	jb	.L29
	mulsd	%xmm3, %xmm3
	addsd	%xmm3, %xmm0
.L29:
	call	__log1p@PLT
	movsd	.LC12(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	jmp	.L24
	.size	__clog, .-__clog
	.weak	clogf32x
	.set	clogf32x,__clog
	.weak	clogf64
	.set	clogf64,__clog
	.weak	clog
	.set	clog,__clog
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1413754136
	.long	1074340347
	.align 8
.LC2:
	.long	0
	.long	2146435072
	.align 8
.LC3:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	4294967295
	.long	2146435071
	.align 8
.LC6:
	.long	0
	.long	1048576
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC8:
	.long	0
	.long	-1074790400
	.align 8
.LC9:
	.long	4294967295
	.long	2145386495
	.align 8
.LC10:
	.long	0
	.long	2097152
	.align 8
.LC11:
	.long	0
	.long	1072693248
	.align 8
.LC12:
	.long	0
	.long	1071644672
	.align 8
.LC13:
	.long	0
	.long	1073741824
	.align 8
.LC14:
	.long	0
	.long	1018167296
	.align 8
.LC15:
	.long	0
	.long	1017118720
	.align 8
.LC16:
	.long	4277811695
	.long	1072049730
