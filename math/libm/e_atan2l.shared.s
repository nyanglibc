	.text
#APP
	.symver __ieee754_atan2l,__atan2l_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_atan2l
	.type	__ieee754_atan2l, @function
__ieee754_atan2l:
	fldt	8(%rsp)
	fldt	24(%rsp)
#APP
# 16 "../sysdeps/i386/fpu/e_atan2l.c" 1
	fpatan
# 0 "" 2
#NO_APP
	ret
	.size	__ieee754_atan2l, .-__ieee754_atan2l
