	.text
	.p2align 4,,15
	.globl	__f32xmulf64
	.type	__f32xmulf64, @function
__f32xmulf64:
	movapd	%xmm0, %xmm2
	movq	.LC0(%rip), %xmm4
	mulsd	%xmm1, %xmm2
	movsd	.LC1(%rip), %xmm3
	movapd	%xmm2, %xmm5
	andpd	%xmm4, %xmm5
	ucomisd	%xmm5, %xmm3
	jnb	.L12
	ucomisd	%xmm2, %xmm2
	jp	.L14
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L1
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	pxor	%xmm3, %xmm3
	movl	$1, %ecx
	ucomisd	%xmm3, %xmm1
	setp	%dl
	cmovne	%ecx, %edx
	ucomisd	%xmm3, %xmm0
	setp	%al
	cmovne	%ecx, %eax
	testb	%al, %dl
	je	.L1
	ucomisd	%xmm3, %xmm2
	movl	$0, %edx
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L6
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	ucomisd	%xmm1, %xmm0
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__f32xmulf64, .-__f32xmulf64
	.weak	f32xmulf64
	.set	f32xmulf64,__f32xmulf64
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
