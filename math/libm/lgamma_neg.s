	.text
	.p2align 4,,15
	.type	lg_sinpi, @function
lg_sinpi:
	movsd	.LC0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L7
	movsd	.LC2(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	jmp	__cos@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	mulsd	.LC1(%rip), %xmm0
	jmp	__sin@PLT
	.size	lg_sinpi, .-lg_sinpi
	.p2align 4,,15
	.type	lg_cospi, @function
lg_cospi:
	movsd	.LC0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L13
	movsd	.LC2(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	jmp	__sin@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	mulsd	.LC1(%rip), %xmm0
	jmp	__cos@PLT
	.size	lg_cospi, .-lg_cospi
	.p2align 4,,15
	.globl	__lgamma_neg
	.type	__lgamma_neg, @function
__lgamma_neg:
	movsd	.LC5(%rip), %xmm2
	movq	.LC7(%rip), %xmm7
	mulsd	%xmm0, %xmm2
	movsd	.LC6(%rip), %xmm8
	movapd	%xmm2, %xmm3
	movapd	%xmm2, %xmm1
	andpd	%xmm7, %xmm3
	ucomisd	%xmm3, %xmm8
	jbe	.L15
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm3, %xmm3
	movsd	.LC8(%rip), %xmm4
	movapd	%xmm7, %xmm5
	andnpd	%xmm2, %xmm5
	cvtsi2sdq	%rax, %xmm3
	movapd	%xmm3, %xmm1
	cmpnlesd	%xmm2, %xmm1
	andpd	%xmm4, %xmm1
	subsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm1
	orpd	%xmm5, %xmm1
.L15:
	cvttsd2si	%xmm1, %eax
	testb	$1, %al
	je	.L55
	movl	%eax, %ecx
	pxor	%xmm1, %xmm1
	notl	%ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	cvtsi2sd	%edx, %xmm1
.L20:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leal	-4(%rax), %ebx
	movl	%ebx, %eax
	subq	$192, %rsp
	andl	$2, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	xorl	%r12d, %r12d
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	92(%rsp), %ebp
	orl	$1, %eax
	movl	%eax, (%rdi)
	movl	%ebp, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebp
	movl	%eax, 96(%rsp)
	jne	.L56
.L22:
	movslq	%ebx, %rax
	movapd	%xmm0, %xmm2
	salq	$4, %rax
	movapd	%xmm0, %xmm4
	movq	%rax, %rdx
	leaq	lgamma_zeros(%rip), %rax
	addq	%rdx, %rax
	cmpl	$1, %ebx
	movsd	(%rax), %xmm3
	movsd	8(%rax), %xmm5
	subsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm6
	subsd	%xmm5, %xmm6
	jle	.L57
	movapd	%xmm1, %xmm2
	subsd	%xmm3, %xmm1
	subsd	%xmm0, %xmm2
	movsd	.LC2(%rip), %xmm0
	subsd	%xmm5, %xmm1
	andpd	%xmm7, %xmm2
	mulsd	%xmm2, %xmm0
	andpd	%xmm7, %xmm1
	movapd	%xmm6, %xmm7
	xorpd	.LC12(%rip), %xmm7
	ucomisd	%xmm1, %xmm0
	movsd	%xmm7, 8(%rsp)
	ja	.L58
	testb	$1, %bl
	movsd	.LC2(%rip), %xmm1
	jne	.L59
	mulsd	%xmm6, %xmm1
.L32:
	movapd	%xmm1, %xmm0
	movsd	%xmm4, 72(%rsp)
	movsd	%xmm6, 64(%rsp)
	movsd	%xmm5, 56(%rsp)
	movsd	%xmm3, 48(%rsp)
	movsd	%xmm2, 24(%rsp)
	movsd	%xmm1, 16(%rsp)
	call	lg_sinpi
	movsd	16(%rsp), %xmm1
	movsd	%xmm0, 40(%rsp)
	movapd	%xmm1, %xmm0
	call	lg_cospi
	movsd	24(%rsp), %xmm2
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 32(%rsp)
	call	lg_cospi
	movsd	32(%rsp), %xmm2
	movsd	%xmm0, 24(%rsp)
	movapd	%xmm2, %xmm0
	call	lg_sinpi
	movsd	24(%rsp), %xmm1
	movsd	40(%rsp), %xmm7
	divsd	%xmm0, %xmm1
	movsd	16(%rsp), %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm7, %xmm0
	addsd	%xmm7, %xmm7
	mulsd	%xmm7, %xmm0
	call	__log1p@PLT
	movsd	72(%rsp), %xmm4
	movsd	%xmm0, 32(%rsp)
	movsd	64(%rsp), %xmm6
	movsd	56(%rsp), %xmm5
	movsd	48(%rsp), %xmm3
.L30:
	movsd	.LC8(%rip), %xmm9
	cmpl	$5, %ebx
	movapd	%xmm9, %xmm1
	movapd	%xmm9, %xmm0
	movapd	%xmm9, %xmm8
	subsd	%xmm3, %xmm1
	subsd	%xmm4, %xmm8
	subsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm7
	subsd	%xmm5, %xmm7
	movapd	%xmm9, %xmm5
	subsd	%xmm8, %xmm5
	subsd	%xmm4, %xmm5
	jle	.L60
	pxor	%xmm4, %xmm4
	movsd	%xmm4, 40(%rsp)
.L33:
	movsd	.LC13(%rip), %xmm2
	movapd	%xmm1, %xmm0
	movsd	%xmm9, 72(%rsp)
	subsd	%xmm2, %xmm0
	movsd	%xmm4, 64(%rsp)
	movsd	%xmm1, 56(%rsp)
	movsd	%xmm6, 16(%rsp)
	movsd	%xmm5, 48(%rsp)
	subsd	.LC14(%rip), %xmm0
	movsd	%xmm8, 24(%rsp)
	addsd	%xmm7, %xmm0
	divsd	%xmm2, %xmm0
	call	__log1p@PLT
	movsd	24(%rsp), %xmm8
	movsd	16(%rsp), %xmm6
	movapd	%xmm8, %xmm7
	movsd	48(%rsp), %xmm5
	mulsd	%xmm6, %xmm0
	movsd	%xmm8, 48(%rsp)
	divsd	%xmm8, %xmm6
	subsd	.LC2(%rip), %xmm7
	movsd	%xmm0, 16(%rsp)
	addsd	%xmm5, %xmm7
	movsd	%xmm7, 24(%rsp)
	movapd	%xmm6, %xmm0
	call	__log1p@PLT
	mulsd	24(%rsp), %xmm0
	movl	$1, %edx
	leaq	96(%rsp), %rax
	movsd	72(%rsp), %xmm9
	leaq	lgamma_coeff(%rip), %rcx
	movsd	40(%rsp), %xmm7
	movsd	56(%rsp), %xmm1
	movapd	%xmm9, %xmm2
	movsd	48(%rsp), %xmm8
	movsd	8(%rsp), %xmm4
	addsd	16(%rsp), %xmm0
	divsd	%xmm8, %xmm2
	addsd	%xmm0, %xmm7
	movapd	%xmm9, %xmm0
	divsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm5
	movapd	%xmm2, %xmm3
	mulsd	%xmm8, %xmm1
	mulsd	%xmm2, %xmm5
	movapd	%xmm0, %xmm6
	divsd	%xmm1, %xmm4
	addsd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm6
	movsd	.LC15(%rip), %xmm0
	mulsd	%xmm4, %xmm3
	movapd	%xmm4, %xmm1
	mulsd	%xmm4, %xmm0
	mulsd	%xmm2, %xmm3
	movsd	%xmm0, 96(%rsp)
	movsd	.LC4(%rip), %xmm2
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L61:
	movsd	(%rcx,%rdx,8), %xmm2
.L35:
	mulsd	%xmm6, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	%xmm5, %xmm3
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax,%rdx,8)
	addq	$1, %rdx
	cmpq	$12, %rdx
	jne	.L61
	pxor	%xmm0, %xmm0
	leaq	88(%rsp), %rdx
	addq	$88, %rax
	.p2align 4,,10
	.p2align 3
.L36:
	addsd	(%rax), %xmm0
	subq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L36
	addsd	%xmm7, %xmm0
	addsd	32(%rsp), %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L55:
	pxor	%xmm1, %xmm1
	cvtsi2sd	%eax, %xmm1
	ucomisd	%xmm1, %xmm2
	jp	.L17
	je	.L62
.L17:
	movl	%eax, %edx
	pxor	%xmm1, %xmm1
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	negl	%edx
	cvtsi2sd	%edx, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	movsd	.LC9(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm3
	movapd	%xmm2, %xmm5
	andpd	%xmm7, %xmm3
	ucomisd	%xmm3, %xmm8
	ja	.L63
.L24:
	subsd	.LC10(%rip), %xmm2
	pxor	%xmm0, %xmm0
	movl	$-33, %edx
	movapd	%xmm4, %xmm7
	cvttsd2si	%xmm2, %eax
	leal	(%rax,%rax), %ecx
	cltq
	subl	%ecx, %edx
	cvtsi2sd	%edx, %xmm0
	leaq	poly_deg(%rip), %rdx
	movq	(%rdx,%rax,8), %rcx
	leaq	poly_end(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	leaq	poly_coeff(%rip), %rax
	testq	%rcx, %rcx
	movsd	(%rax,%rdx,8), %xmm2
	mulsd	.LC11(%rip), %xmm0
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm0
	je	.L25
	leaq	-8(%rax,%rdx,8), %rax
	subq	%rcx, %rdx
	leaq	-8+poly_coeff(%rip), %rcx
	leaq	(%rcx,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L26:
	mulsd	%xmm0, %xmm2
	subq	$8, %rax
	addsd	8(%rax), %xmm2
	cmpq	%rax, %rdx
	jne	.L26
.L25:
	mulsd	%xmm6, %xmm2
	subsd	%xmm1, %xmm4
	divsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm0
	call	__log1p@PLT
.L27:
	testb	%r12b, %r12b
	jne	.L64
.L14:
	addq	$192, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movsd	.LC8(%rip), %xmm0
	divsd	.LC3(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	mulsd	8(%rsp), %xmm1
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L63:
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm3, %xmm3
	andnpd	%xmm5, %xmm7
	cvtsi2sdq	%rax, %xmm3
	movapd	%xmm3, %xmm0
	cmpnlesd	%xmm2, %xmm0
	movsd	.LC8(%rip), %xmm2
	andpd	%xmm2, %xmm0
	subsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm2
	orpd	%xmm7, %xmm2
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L60:
	movl	$7, %edi
	pxor	%xmm0, %xmm0
	subl	%ebx, %edi
	movsd	%xmm9, 72(%rsp)
	sarl	%edi
	movsd	%xmm6, 16(%rsp)
	cvtsi2sd	%edi, %xmm0
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm4
	addsd	%xmm1, %xmm3
	addsd	%xmm8, %xmm4
	movapd	%xmm3, %xmm2
	movsd	%xmm3, 64(%rsp)
	movsd	%xmm4, 48(%rsp)
	subsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm7
	movapd	%xmm4, %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm6, %xmm0
	movsd	%xmm7, 56(%rsp)
	subsd	%xmm1, %xmm8
	addsd	%xmm8, %xmm5
	movapd	%xmm5, %xmm2
	movsd	%xmm5, 24(%rsp)
	call	__lgamma_product@PLT
	call	__log1p@PLT
	xorpd	.LC12(%rip), %xmm0
	movsd	48(%rsp), %xmm4
	movsd	64(%rsp), %xmm3
	movapd	%xmm4, %xmm8
	movsd	16(%rsp), %xmm6
	movsd	%xmm0, 40(%rsp)
	movapd	%xmm3, %xmm1
	pxor	%xmm4, %xmm4
	movsd	24(%rsp), %xmm5
	movsd	56(%rsp), %xmm7
	movsd	72(%rsp), %xmm9
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L58:
	movapd	%xmm1, %xmm0
	movsd	%xmm4, 56(%rsp)
	movsd	%xmm6, 48(%rsp)
	movsd	%xmm5, 40(%rsp)
	movsd	%xmm3, 24(%rsp)
	movsd	%xmm2, 32(%rsp)
	call	lg_sinpi
	movsd	32(%rsp), %xmm2
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm2, %xmm0
	call	lg_sinpi
	movsd	16(%rsp), %xmm1
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	call	__ieee754_log@PLT
	movsd	24(%rsp), %xmm3
	movsd	%xmm0, 32(%rsp)
	movsd	40(%rsp), %xmm5
	movsd	48(%rsp), %xmm6
	movsd	56(%rsp), %xmm4
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L64:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	92(%rsp), %eax
	andl	$24576, %ebp
	andb	$-97, %ah
	orl	%eax, %ebp
	movl	%ebp, 92(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L56:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 96(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r12d
	jmp	.L22
	.size	__lgamma_neg, .-__lgamma_neg
	.section	.rodata
	.align 32
	.type	poly_end, @object
	.size	poly_end, 64
poly_end:
	.quad	10
	.quad	22
	.quad	35
	.quad	49
	.quad	63
	.quad	76
	.quad	88
	.quad	100
	.align 32
	.type	poly_deg, @object
	.size	poly_deg, 64
poly_deg:
	.quad	10
	.quad	11
	.quad	12
	.quad	13
	.quad	13
	.quad	12
	.quad	11
	.quad	11
	.align 32
	.type	poly_coeff, @object
	.size	poly_coeff, 808
poly_coeff:
	.long	1549063215
	.long	-1074743524
	.long	3087787755
	.long	-1075255485
	.long	1082464529
	.long	-1078015935
	.long	3024554237
	.long	-1075023981
	.long	2094795462
	.long	-1078969128
	.long	3684443612
	.long	-1074963147
	.long	788635561
	.long	1067672116
	.long	2294381526
	.long	-1074928449
	.long	1661883529
	.long	1069774018
	.long	1133356704
	.long	-1074853891
	.long	841229782
	.long	1070716978
	.long	303758031
	.long	-1074900383
	.long	3154812245
	.long	-1075229793
	.long	2130046158
	.long	1070392560
	.long	3545635289
	.long	-1074779558
	.long	584024776
	.long	1071832374
	.long	783943946
	.long	-1074472909
	.long	1260071776
	.long	1072808137
	.long	3510159182
	.long	-1073964432
	.long	4057252246
	.long	1073577372
	.long	2792052182
	.long	-1073442849
	.long	4221945852
	.long	1074246568
	.long	3629063995
	.long	-1072755237
	.long	2852891696
	.long	-1075119535
	.long	878739755
	.long	-1074998583
	.long	1157953964
	.long	1072044777
	.long	2452138094
	.long	-1074191184
	.long	735011189
	.long	1073564081
	.long	3667421578
	.long	-1073199931
	.long	2946758291
	.long	1074823657
	.long	3585423936
	.long	-1072109637
	.long	1435998681
	.long	1075955144
	.long	2211130701
	.long	-1070982365
	.long	1889125765
	.long	1077076493
	.long	1763396777
	.long	-1069789514
	.long	150913791
	.long	1078244621
	.long	933232425
	.long	-1075385900
	.long	3583034230
	.long	-1074616277
	.long	4163418128
	.long	1073250336
	.long	4162874327
	.long	-1073114820
	.long	155925018
	.long	1075142322
	.long	3121766986
	.long	-1071413923
	.long	655812940
	.long	1076964371
	.long	4200335981
	.long	-1069635251
	.long	3590275782
	.long	1078670768
	.long	2911537593
	.long	-1067961357
	.long	1093142395
	.long	1080393175
	.long	2602248332
	.long	-1066198899
	.long	2800264738
	.long	1082245051
	.long	845592731
	.long	-1064308859
	.long	1175713240
	.long	-1076983800
	.long	3405741359
	.long	1073534295
	.long	1119263591
	.long	1074531546
	.long	783544584
	.long	1075450903
	.long	3433886874
	.long	1076331365
	.long	2748214387
	.long	1077217875
	.long	4258854406
	.long	1078115695
	.long	1383321603
	.long	1079028821
	.long	4127903622
	.long	1079879219
	.long	3158503082
	.long	1080712153
	.long	1649215417
	.long	1081567642
	.long	2965200382
	.long	1082443623
	.long	890460205
	.long	1083397265
	.long	2129079089
	.long	1084347077
	.long	1251326506
	.long	-1076177079
	.long	943563686
	.long	1072860512
	.long	987400638
	.long	1073591889
	.long	2357020161
	.long	1074204856
	.long	1259947218
	.long	1074861060
	.long	1138108131
	.long	1075391462
	.long	746255431
	.long	1075993451
	.long	1821357106
	.long	1076544896
	.long	526653103
	.long	1077115384
	.long	3036526944
	.long	1077696024
	.long	2239321639
	.long	1078237718
	.long	2875807939
	.long	1078906479
	.long	1196853737
	.long	1079422590
	.long	1017015807
	.long	-1075754954
	.long	2808953725
	.long	1072280847
	.long	3586352905
	.long	1072777034
	.long	3104607324
	.long	1073177068
	.long	4283992102
	.long	1073555355
	.long	33397588
	.long	1073902312
	.long	3131782177
	.long	1074178986
	.long	1483258795
	.long	1074538665
	.long	1686579276
	.long	1074874447
	.long	4159953521
	.long	1075140457
	.long	3396227088
	.long	1075491773
	.long	2218500301
	.long	1075875961
	.long	3439103263
	.long	-1075574566
	.long	462946520
	.long	1071833457
	.long	675517082
	.long	1071993159
	.long	650638108
	.long	1072324250
	.long	1127410542
	.long	1072403765
	.long	2246739976
	.long	1072668179
	.long	1705618940
	.long	1072712467
	.long	4011931544
	.long	1072835412
	.long	622881781
	.long	1072874796
	.long	2039131364
	.long	1072998973
	.long	2025129187
	.long	1073071090
	.long	2462856315
	.long	1073209474
	.align 32
	.type	lgamma_coeff, @object
	.size	lgamma_coeff, 96
lgamma_coeff:
	.long	1431655765
	.long	1068848469
	.long	381774871
	.long	-1083784852
	.long	436314138
	.long	1061814688
	.long	327235604
	.long	-1086095048
	.long	723058467
	.long	1061917982
	.long	3650698365
	.long	-1084265808
	.long	440509466
	.long	1064976804
	.long	3406779288
	.long	-1080147322
	.long	941491840
	.long	1070005910
	.long	18358074
	.long	-1074378905
	.long	841801734
	.long	1076547140
	.long	634178587
	.long	-1067214043
	.align 32
	.type	lgamma_zeros, @object
	.size	lgamma_zeros, 832
lgamma_zeros:
	.long	2516645996
	.long	-1073502212
	.long	4186634448
	.long	-1132109980
	.long	169597185
	.long	-1073349823
	.long	1831759749
	.long	1016732073
	.long	3164494255
	.long	-1073142259
	.long	3543508915
	.long	-1129352836
	.long	1417424869
	.long	-1072716687
	.long	1630081591
	.long	-1133048507
	.long	2019136897
	.long	-1072682930
	.long	3924049150
	.long	1018067137
	.long	2054089469
	.long	-1072433321
	.long	2877231823
	.long	1017503326
	.long	1475393241
	.long	-1072428950
	.long	3073157578
	.long	-1131851166
	.long	4119983117
	.long	-1072169326
	.long	3593256242
	.long	-1128800946
	.long	629767309
	.long	-1072168597
	.long	1235532402
	.long	1016233487
	.long	4151019248
	.long	-1071906869
	.long	2470950918
	.long	1013470130
	.long	4260232735
	.long	-1071906765
	.long	3526510200
	.long	1018301367
	.long	2139183567
	.long	-1071644679
	.long	363402630
	.long	-1131522828
	.long	1076409337
	.long	-1071644669
	.long	1022106861
	.long	-1128308533
	.long	2743618512
	.long	-1071513601
	.long	2636835109
	.long	-1129318132
	.long	1551329531
	.long	-1071513600
	.long	268136660
	.long	-1130669215
	.long	4139833280
	.long	-1071382529
	.long	2831428989
	.long	1019320270
	.long	155133815
	.long	-1071382528
	.long	1570103636
	.long	-1128716364
	.long	4280864212
	.long	-1071251457
	.long	2790958214
	.long	1017944356
	.long	14103082
	.long	-1071251456
	.long	758644562
	.long	-1127698741
	.long	4293792039
	.long	-1071120385
	.long	2422943989
	.long	-1130569225
	.long	1175257
	.long	-1071120384
	.long	132250462
	.long	1017224531
	.long	4294876892
	.long	-1070989313
	.long	875364380
	.long	1019760448
	.long	90404
	.long	-1070989312
	.long	874440925
	.long	-1127723517
	.long	4294960839
	.long	-1070858241
	.long	4091207931
	.long	1020078640
	.long	6457
	.long	-1070858240
	.long	1249095523
	.long	-1127405009
	.long	4294966866
	.long	-1070727169
	.long	3487073918
	.long	1020252060
	.long	430
	.long	-1070727168
	.long	3454562832
	.long	-1127231588
	.long	4294967269
	.long	-1070596097
	.long	3427002918
	.long	-1129837544
	.long	13
	.long	-1070596096
	.long	3866526985
	.long	-1126367620
	.long	4294967295
	.long	-1070530561
	.long	1060981233
	.long	-1127566110
	.long	1
	.long	-1070530560
	.long	1060981692
	.long	1019917538
	.long	0
	.long	-1070465024
	.long	1673100698
	.long	1017545336
	.long	0
	.long	-1070465024
	.long	1673100693
	.long	-1129938312
	.long	0
	.long	-1070399488
	.long	1182875991
	.long	1013118107
	.long	0
	.long	-1070399488
	.long	1182875991
	.long	-1134365541
	.long	0
	.long	-1070333952
	.long	2751595045
	.long	1008620587
	.long	0
	.long	-1070333952
	.long	2751595045
	.long	-1138863061
	.long	0
	.long	-1070268416
	.long	4141675890
	.long	1003953038
	.long	0
	.long	-1070268416
	.long	4141675890
	.long	-1143530610
	.long	0
	.long	-1070202880
	.long	1840773203
	.long	999345721
	.long	0
	.long	-1070202880
	.long	1840773203
	.long	-1148137927
	.long	0
	.long	-1070137344
	.long	320223258
	.long	994533812
	.long	0
	.long	-1070137344
	.long	320223258
	.long	-1152949836
	.long	0
	.long	-1070071808
	.long	426964344
	.long	989801712
	.long	0
	.long	-1070071808
	.long	426964344
	.long	-1157681936
	.long	0
	.long	-1070006272
	.long	3709231017
	.long	984871884
	.long	0
	.long	-1070006272
	.long	3709231017
	.long	-1162611764
	.long	0
	.long	-1069940736
	.long	4234825306
	.long	979930757
	.long	0
	.long	-1069940736
	.long	4234825306
	.long	-1167552891
	.long	0
	.long	-1069875200
	.long	3269250723
	.long	974985905
	.long	0
	.long	-1069875200
	.long	3269250723
	.long	-1172497743
	.long	0
	.long	-1069809664
	.long	641009757
	.long	969974154
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1070596096
	.align 8
.LC1:
	.long	1413754136
	.long	1074340347
	.align 8
.LC2:
	.long	0
	.long	1071644672
	.align 8
.LC3:
	.long	0
	.long	0
	.align 8
.LC4:
	.long	381774871
	.long	-1083784852
	.align 8
.LC5:
	.long	0
	.long	-1073741824
	.align 8
.LC6:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	0
	.long	-1071644672
	.align 8
.LC10:
	.long	0
	.long	1076887552
	.align 8
.LC11:
	.long	0
	.long	1068498944
	.section	.rodata.cst16
	.align 16
.LC12:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC13:
	.long	2333366121
	.long	1074118410
	.align 8
.LC14:
	.long	3803251002
	.long	1017435518
	.align 8
.LC15:
	.long	1431655765
	.long	1068848469
