	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__subtf3
	.p2align 4,,15
	.globl	__cacosf128
	.type	__cacosf128, @function
__cacosf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	movdqa	96(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L10
	movdqa	(%rsp), %xmm2
	movl	$1, %ebp
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L20
.L2:
	movdqa	112(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L4
	movdqa	(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L4
	movdqa	(%rsp), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L5
	pxor	%xmm1, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L5
	cmpl	$1, %ebp
	jle	.L4
	cmpl	$2, %ebp
	jne	.L7
.L4:
	leaq	32(%rsp), %rdi
	pushq	120(%rsp)
	pushq	120(%rsp)
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	__casinf128@PLT
	movdqa	80(%rsp), %xmm2
	movdqa	64(%rsp), %xmm1
	movdqa	.LC4(%rip), %xmm0
	movaps	%xmm2, 48(%rsp)
	call	__subtf3@PLT
	pxor	%xmm1, %xmm1
	addq	$32, %rsp
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	movdqa	16(%rsp), %xmm2
	jne	.L8
	pxor	%xmm3, %xmm3
.L8:
	movdqa	.LC5(%rip), %xmm0
	pxor	%xmm2, %xmm0
.L9:
	movaps	%xmm3, (%rbx)
	movq	%rbx, %rax
	movaps	%xmm0, 16(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	(%rsp), %xmm2
	movl	$4, %ebp
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L2
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	xorl	%ebp, %ebp
	call	__eqtf2@PLT
	testq	%rax, %rax
	setne	%bpl
	addl	$2, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1, %ebp
	jle	.L4
.L7:
	movdqa	112(%rsp), %xmm0
	leaq	32(%rsp), %rdi
	movl	$1, %esi
	pushq	104(%rsp)
	pushq	104(%rsp)
	pxor	.LC5(%rip), %xmm0
	subq	$16, %rsp
	movups	%xmm0, (%rsp)
	call	__kernel_casinhf128@PLT
	movdqa	64(%rsp), %xmm0
	movdqa	80(%rsp), %xmm3
	addq	$32, %rsp
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%ebp, %ebp
	jmp	.L2
	.size	__cacosf128, .-__cacosf128
	.weak	cacosf128
	.set	cacosf128,__cacosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC4:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
