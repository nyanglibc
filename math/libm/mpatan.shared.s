	.text
	.p2align 4,,15
	.globl	__mpatan
	.type	__mpatan, @function
__mpatan:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r8
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movl	$41, %ecx
	movl	%edx, %ebx
	subq	$2424, %rsp
	cmpl	$0, (%r8)
	movq	%rdi, 24(%rsp)
	leaq	64(%rsp), %rdi
	movq	%rsi, 40(%rsp)
	movq	%rdi, 8(%rsp)
	rep stosq
	jle	.L2
	leaq	736(%rsp), %r13
	movq	%r8, %rdi
	movl	$1, 64(%rsp)
	movq	$1, 72(%rsp)
	movq	%r13, %rsi
	call	__sqr@PLT
	movq	$128, 32(%rsp)
	movl	$7, 16(%rsp)
.L10:
	leaq	1408(%rsp), %rbp
	leaq	1744(%rsp), %r12
	leaq	2080(%rsp), %r14
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L3:
	movq	__mpone@GOTPCREL(%rip), %rdi
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r13, %rsi
	addl	$1, %r15d
	call	__add@PLT
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__mpsqrt@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__add@PLT
	movq	__mptwo@GOTPCREL(%rip), %rdi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	__add@PLT
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__add@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__dvd@PLT
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__cpy@PLT
	cmpl	16(%rsp), %r15d
	jne	.L3
	leaq	400(%rsp), %rax
	movl	%ebx, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, 16(%rsp)
	call	__mpsqrt@PLT
	movq	24(%rsp), %rax
	movq	8(%rax), %rax
	movq	%rax, 408(%rsp)
.L5:
	leaq	__atan_np(%rip), %rdx
	movslq	%ebx, %rax
	leaq	1072(%rsp), %r14
	movq	8(%rsp), %rsi
	movl	%ebx, %ecx
	movq	%r13, %rdi
	movl	(%rdx,%rax,4), %r15d
	leaq	__atan_twonm1(%rip), %rdx
	cvttsd2siq	(%rdx,%rax,8), %rax
	movq	%r14, %rdx
	movq	%rax, 80(%rsp)
	subl	$1, %r15d
	call	__dvd@PLT
	cmpl	$1, %r15d
	jle	.L8
	leaq	1744(%rsp), %r12
	.p2align 4,,10
	.p2align 3
.L9:
	movq	8(%rsp), %rsi
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r13, %rdi
	subq	$2, 80(%rsp)
	subl	$1, %r15d
	call	__dvd@PLT
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__mul@PLT
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__sub@PLT
	cmpl	$1, %r15d
	jne	.L9
.L8:
	movq	16(%rsp), %r15
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__mul@PLT
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	__sub@PLT
	movq	32(%rsp), %rax
	movq	40(%rsp), %rdx
	movl	%ebx, %ecx
	movq	8(%rsp), %rdi
	movq	%r14, %rsi
	movq	%rax, 80(%rsp)
	call	__mul@PLT
	addq	$2424, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	je	.L4
.L7:
	movq	24(%rsp), %r14
	leaq	736(%rsp), %r13
	movl	%ebx, %edx
	movl	$1, 64(%rsp)
	movq	$1, 72(%rsp)
	leaq	1408(%rsp), %rbp
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	__sqr@PLT
	leaq	400(%rsp), %rax
	movl	%ebx, %edx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rax, 16(%rsp)
	call	__cpy@PLT
	movq	$1, 32(%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movq	24(%rsp), %rdi
	leaq	56(%rsp), %rsi
	call	__mp_dbl@PLT
	movsd	56(%rsp), %xmm0
	andpd	.LC0(%rip), %xmm0
	ucomisd	48+__atan_xm(%rip), %xmm0
	movsd	%xmm0, 56(%rsp)
	ja	.L11
	ucomisd	40+__atan_xm(%rip), %xmm0
	ja	.L12
	ucomisd	32+__atan_xm(%rip), %xmm0
	ja	.L13
	ucomisd	24+__atan_xm(%rip), %xmm0
	ja	.L14
	ucomisd	16+__atan_xm(%rip), %xmm0
	ja	.L15
	ucomisd	8+__atan_xm(%rip), %xmm0
	jbe	.L7
	movl	$1, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	736(%rsp), %r13
	movq	24(%rsp), %rdi
	movl	%ebx, %edx
	movl	$1, 64(%rsp)
	movq	$1, 72(%rsp)
	movq	%r13, %rsi
	call	__sqr@PLT
	movzbl	16(%rsp), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cltq
	movq	%rax, 32(%rsp)
	jmp	.L10
.L11:
	movl	$6, 16(%rsp)
	jmp	.L6
.L12:
	movl	$5, 16(%rsp)
	jmp	.L6
.L13:
	movl	$4, 16(%rsp)
	jmp	.L6
.L14:
	movl	$3, 16(%rsp)
	jmp	.L6
.L15:
	movl	$2, 16(%rsp)
	jmp	.L6
	.size	__mpatan, .-__mpatan
	.hidden	__atan_np
	.globl	__atan_np
	.section	.rodata
	.align 32
	.type	__atan_np, @object
	.size	__atan_np, 132
__atan_np:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	6
	.long	8
	.long	10
	.long	11
	.long	13
	.long	15
	.long	17
	.long	19
	.long	21
	.long	23
	.long	25
	.long	27
	.long	28
	.long	30
	.long	32
	.long	34
	.long	36
	.long	38
	.long	40
	.long	42
	.long	43
	.long	45
	.long	47
	.long	49
	.long	51
	.long	53
	.long	55
	.long	57
	.long	59
	.hidden	__atan_twonm1
	.globl	__atan_twonm1
	.align 32
	.type	__atan_twonm1, @object
	.size	__atan_twonm1, 264
__atan_twonm1:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1076232192
	.long	0
	.long	1076756480
	.long	0
	.long	1077084160
	.long	0
	.long	1077215232
	.long	0
	.long	1077477376
	.long	0
	.long	1077739520
	.long	0
	.long	1077968896
	.long	0
	.long	1078099968
	.long	0
	.long	1078231040
	.long	0
	.long	1078362112
	.long	0
	.long	1078493184
	.long	0
	.long	1078624256
	.long	0
	.long	1078689792
	.long	0
	.long	1078820864
	.long	0
	.long	1078951936
	.long	0
	.long	1079033856
	.long	0
	.long	1079099392
	.long	0
	.long	1079164928
	.long	0
	.long	1079230464
	.long	0
	.long	1079296000
	.long	0
	.long	1079328768
	.long	0
	.long	1079394304
	.long	0
	.long	1079459840
	.long	0
	.long	1079525376
	.long	0
	.long	1079590912
	.long	0
	.long	1079656448
	.long	0
	.long	1079721984
	.long	0
	.long	1079787520
	.long	0
	.long	1079853056
	.hidden	__atan_xm
	.globl	__atan_xm
	.align 32
	.type	__atan_xm, @object
	.size	__atan_xm, 64
__atan_xm:
	.long	0
	.long	0
	.long	0
	.long	1065955518
	.long	0
	.long	1066997383
	.long	0
	.long	1068049314
	.long	0
	.long	1069101246
	.long	0
	.long	1070159888
	.long	0
	.long	1071283961
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
