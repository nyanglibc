	.text
	.globl	__eqtf2
	.globl	__subtf3
	.globl	__trunctfxf2
	.globl	__unordtf2
	.globl	__gttf2
	.p2align 4,,15
	.globl	__f64xsubf128
	.type	__f64xsubf128, @function
__f64xsubf128:
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L18
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	call	__trunctfxf2@PLT
	fld	%st(0)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L1
	fucomi	%st(0), %st
	jp	.L11
.L9:
	movdqa	(%rsp), %xmm2
	fstpt	32(%rsp)
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	32(%rsp)
	jne	.L1
	fstp	%st(0)
	movdqa	(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	fldt	32(%rsp)
	jg	.L1
	movdqa	16(%rsp), %xmm2
	fstpt	(%rsp)
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jne	.L1
	fstp	%st(0)
	movdqa	16(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jle	.L21
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebp
	movl	%ebp, %eax
	andl	$-32704, %eax
	orl	$32640, %eax
	movl	%eax, 76(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %eax
	orl	%ebp, %eax
	movl	%eax, 76(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %ebp
	notl	%ebp
	testl	%edi, %ebp
	jne	.L23
.L6:
	shrl	$5, %ebx
	movq	%xmm0, %rax
	movabsq	$-4294967296, %rdx
	andl	$1, %ebx
	orl	32(%rsp), %ebx
	andq	%rdx, %rax
	orq	%rbx, %rax
	movq	%rax, 32(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__trunctfxf2@PLT
	fld	%st(0)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L24
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L1
	jne	.L1
.L21:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	fucomi	%st(0), %st
	jnp	.L9
	fstpt	32(%rsp)
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	32(%rsp)
	jne	.L1
.L11:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movaps	%xmm0, 48(%rsp)
	call	__feraiseexcept@PLT
	movdqa	48(%rsp), %xmm0
	jmp	.L6
	.size	__f64xsubf128, .-__f64xsubf128
	.weak	f64xsubf128
	.set	f64xsubf128,__f64xsubf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
