	.text
	.globl	__addtf3
	.globl	__lttf2
	.globl	__divtf3
	.globl	__subtf3
	.globl	__multf3
	.globl	__fixtfsi
	.p2align 4,,15
	.globl	__expm1f128
	.type	__expm1f128, @function
__expm1f128:
	pushq	%rbx
	subq	$48, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	shrq	$32, %rax
	movl	%eax, %edx
	andl	$2147483647, %edx
	testl	%eax, %eax
	js	.L2
	cmpl	$1074135039, %edx
	jg	.L26
.L2:
	cmpl	$2147418111, %edx
	movdqa	(%rsp), %xmm3
	jle	.L3
	movq	(%rsp), %rcx
	movq	8(%rsp), %rax
	movzwl	%dx, %edx
	movq	%rcx, %rsi
	shrq	$32, %rsi
	orl	%esi, %eax
	orl	%ecx, %eax
	orl	%edx, %eax
	je	.L11
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__addtf3@PLT
.L1:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	testl	%edx, %edx
	jne	.L5
	movq	(%rsp), %rdx
	movq	8(%rsp), %rax
	movdqa	(%rsp), %xmm0
	movq	%rdx, %rcx
	shrq	$32, %rcx
	orl	%ecx, %eax
	orl	%edx, %eax
	je	.L1
.L5:
	movdqa	.LC1(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L27
	movdqa	(%rsp), %xmm2
	pand	.LC5(%rip), %xmm2
	movdqa	.LC6(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L22
	movdqa	16(%rsp), %xmm2
	movdqa	.LC7(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L23
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$48, %rsp
	popq	%rbx
	jmp	__expf128@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	.LC0(%rip), %xmm0
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__subtf3@PLT
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movdqa	.LC8(%rip), %xmm1
	movdqa	.LC9(%rip), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__addtf3@PLT
	call	__floorf128@PLT
	movaps	%xmm0, 16(%rsp)
	call	__fixtfsi@PLT
	movl	%eax, %ebx
	movdqa	16(%rsp), %xmm0
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC11(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC22(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC23(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC24(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC25(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC26(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC10(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movl	%ebx, %edi
	movdqa	.LC4(%rip), %xmm0
	call	__ldexpf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	jmp	.L1
	.size	__expm1f128, .-__expm1f128
	.weak	expm1f128
	.set	expm1f128,__expm1f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC1:
	.long	2548483849
	.long	2428108701
	.long	984706489
	.long	-1073398765
	.align 16
.LC2:
	.long	3822078837
	.long	3986280503
	.long	1699920125
	.long	2147397209
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1073807360
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1066270720
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC8:
	.long	1060072180
	.long	2654681472
	.long	485989052
	.long	1072398205
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073636068
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC11:
	.long	2030756558
	.long	505022984
	.long	1380592755
	.long	-1073875813
	.align 16
.LC12:
	.long	2042744432
	.long	1555542959
	.long	3455644835
	.long	1074028570
	.align 16
.LC13:
	.long	783501092
	.long	1287556401
	.long	1891541785
	.long	1074375985
	.align 16
.LC14:
	.long	1690732992
	.long	3881698442
	.long	1071666636
	.long	1074685371
	.align 16
.LC15:
	.long	14193826
	.long	2210635428
	.long	1756295773
	.long	1074946091
	.align 16
.LC16:
	.long	3178610853
	.long	1861912952
	.long	3986479846
	.long	1075187959
	.align 16
.LC17:
	.long	3018233795
	.long	832007064
	.long	1550541965
	.long	1075360926
	.align 16
.LC18:
	.long	738884190
	.long	70000300
	.long	1303964020
	.long	1075517623
	.align 16
.LC19:
	.long	432152267
	.long	2890474730
	.long	4160026180
	.long	1074094103
	.align 16
.LC20:
	.long	1555394622
	.long	3316818056
	.long	3773010132
	.long	1074449974
	.align 16
.LC21:
	.long	1959418798
	.long	1582381084
	.long	496033136
	.long	1074755483
	.align 16
.LC22:
	.long	1392169331
	.long	1398470442
	.long	195740534
	.long	1075026654
	.align 16
.LC23:
	.long	3416231977
	.long	4022526443
	.long	3904534055
	.long	1075262509
	.align 16
.LC24:
	.long	3292925736
	.long	2416584422
	.long	3667842153
	.long	1075459123
	.align 16
.LC25:
	.long	1685999643
	.long	4122599258
	.long	3706909899
	.long	1075607108
	.align 16
.LC26:
	.long	1108326606
	.long	105000450
	.long	4103429678
	.long	1075684626
