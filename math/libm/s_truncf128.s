	.text
	.globl	__addtf3
	.p2align 4,,15
	.globl	__truncf128
	.type	__truncf128, @function
__truncf128:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	shrq	$48, %rax
	andl	$32767, %eax
	leal	-16383(%rax), %ecx
	cmpl	$47, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L9
	movabsq	$281474976710655, %rax
	movq	$0, (%rsp)
	sarq	%cl, %rax
	notq	%rax
	andq	%rdx, %rax
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$111, %ecx
	jle	.L5
	cmpl	$16384, %ecx
	movdqa	(%rsp), %xmm0
	jne	.L1
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	leal	-16431(%rax), %ecx
	movq	$-1, %rax
	movq	%rdx, 24(%rsp)
	shrq	%cl, %rax
	notq	%rax
	movq	%rax, %rdx
	movq	(%rsp), %rax
	andq	%rax, %rdx
	movq	%rdx, 16(%rsp)
	movdqa	16(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movabsq	$-9223372036854775808, %rax
	movq	$0, (%rsp)
	andq	%rax, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.size	__truncf128, .-__truncf128
	.weak	truncf128
	.set	truncf128,__truncf128
