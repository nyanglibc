	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ctanh
	.type	__ctanh, @function
__ctanh:
	pushq	%rbx
	movapd	%xmm0, %xmm5
	movapd	%xmm1, %xmm0
	subq	$80, %rsp
	movapd	%xmm5, %xmm6
	movq	.LC2(%rip), %xmm4
	movsd	.LC3(%rip), %xmm2
	andpd	%xmm4, %xmm6
	ucomisd	%xmm6, %xmm2
	jb	.L2
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L52
	movsd	.LC8(%rip), %xmm2
	movapd	%xmm5, %xmm3
	ucomisd	.LC10(%rip), %xmm1
	mulsd	.LC7(%rip), %xmm2
	mulsd	.LC9(%rip), %xmm2
	cvttsd2si	%xmm2, %ebx
	jbe	.L45
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm5, 32(%rsp)
	movaps	%xmm4, 48(%rsp)
	movsd	%xmm5, 16(%rsp)
	movsd	%xmm6, 8(%rsp)
	call	__sincos@PLT
	movsd	8(%rsp), %xmm6
	movsd	16(%rsp), %xmm3
	movsd	32(%rsp), %xmm5
	movapd	48(%rsp), %xmm4
.L16:
	pxor	%xmm1, %xmm1
	cvtsi2sd	%ebx, %xmm1
	ucomisd	%xmm1, %xmm6
	ja	.L53
	ucomisd	.LC10(%rip), %xmm6
	movsd	.LC1(%rip), %xmm0
	ja	.L54
.L22:
	movsd	72(%rsp), %xmm2
	movapd	%xmm2, %xmm5
	movapd	%xmm2, %xmm1
	andpd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm1
	mulsd	.LC12(%rip), %xmm5
	ucomisd	%xmm5, %xmm6
	ja	.L55
.L24:
	mulsd	64(%rsp), %xmm2
	mulsd	%xmm0, %xmm3
	divsd	%xmm1, %xmm3
	divsd	%xmm1, %xmm2
	movapd	%xmm3, %xmm5
	movapd	%xmm2, %xmm1
.L21:
	movapd	%xmm3, %xmm0
	movsd	.LC10(%rip), %xmm7
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm7
	jbe	.L26
	mulsd	%xmm3, %xmm3
.L26:
	andpd	%xmm2, %xmm4
	movsd	.LC10(%rip), %xmm7
	ucomisd	%xmm4, %xmm7
	jbe	.L1
	mulsd	%xmm2, %xmm2
.L1:
	addq	$80, %rsp
	movapd	%xmm5, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movapd	%xmm3, %xmm5
	mulsd	%xmm3, %xmm5
	addsd	%xmm5, %xmm1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L53:
	pxor	%xmm0, %xmm0
	addl	%ebx, %ebx
	movaps	%xmm4, 48(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm1, 16(%rsp)
	cvtsi2sd	%ebx, %xmm0
	movsd	%xmm6, 8(%rsp)
	call	__ieee754_exp@PLT
	movsd	.LC11(%rip), %xmm2
	movsd	16(%rsp), %xmm1
	mulsd	64(%rsp), %xmm2
	movsd	8(%rsp), %xmm6
	movsd	32(%rsp), %xmm5
	subsd	%xmm1, %xmm6
	movapd	48(%rsp), %xmm4
	movapd	%xmm5, %xmm3
	mulsd	72(%rsp), %xmm2
	andpd	.LC5(%rip), %xmm3
	ucomisd	%xmm1, %xmm6
	orpd	.LC4(%rip), %xmm3
	divsd	%xmm0, %xmm2
	jbe	.L47
	movapd	%xmm3, %xmm5
	divsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm1
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L54:
	movapd	%xmm5, %xmm0
	movsd	%xmm5, 16(%rsp)
	movaps	%xmm4, 32(%rsp)
	call	__ieee754_sinh@PLT
	movsd	%xmm0, 8(%rsp)
	movsd	16(%rsp), %xmm5
	movapd	%xmm5, %xmm0
	call	__ieee754_cosh@PLT
	movsd	8(%rsp), %xmm3
	movapd	32(%rsp), %xmm4
	movapd	%xmm3, %xmm6
	andpd	%xmm4, %xmm6
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L47:
	addsd	%xmm6, %xmm6
	movsd	%xmm2, 16(%rsp)
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm3, 8(%rsp)
	movapd	%xmm6, %xmm0
	call	__ieee754_exp@PLT
	movsd	16(%rsp), %xmm2
	movsd	8(%rsp), %xmm3
	divsd	%xmm0, %xmm2
	movapd	32(%rsp), %xmm4
	movapd	%xmm3, %xmm5
	movapd	%xmm2, %xmm1
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm2, %xmm6
	jbe	.L5
	andpd	%xmm0, %xmm4
	movq	.LC5(%rip), %xmm3
	ucomisd	%xmm4, %xmm2
	andpd	%xmm3, %xmm5
	orpd	.LC4(%rip), %xmm5
	jb	.L6
	ucomisd	.LC1(%rip), %xmm4
	ja	.L56
.L6:
	andpd	%xmm3, %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	ucomisd	%xmm2, %xmm6
	ja	.L57
.L5:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L10
	je	.L58
.L10:
	ucomisd	%xmm1, %xmm5
	jp	.L32
	movsd	.LC0(%rip), %xmm1
	je	.L12
.L32:
	movsd	.LC0(%rip), %xmm5
	movapd	%xmm5, %xmm1
.L12:
	andpd	%xmm4, %xmm0
	ucomisd	%xmm2, %xmm0
	jbe	.L1
	movl	$1, %edi
	movsd	%xmm1, 16(%rsp)
	movsd	%xmm5, 8(%rsp)
	call	__GI_feraiseexcept
	movsd	8(%rsp), %xmm5
	movsd	16(%rsp), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L45:
	movq	.LC1(%rip), %rax
	movsd	%xmm0, 64(%rsp)
	movq	%rax, 72(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L57:
	movq	.LC5(%rip), %xmm3
	andpd	%xmm3, %xmm5
	orpd	.LC4(%rip), %xmm5
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm5, 8(%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__sincos@PLT
	movsd	64(%rsp), %xmm1
	movapd	16(%rsp), %xmm3
	mulsd	72(%rsp), %xmm1
	movsd	8(%rsp), %xmm5
	andpd	%xmm3, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L58:
	movapd	%xmm0, %xmm1
	jmp	.L1
	.size	__ctanh, .-__ctanh
	.weak	ctanhf32x
	.set	ctanhf32x,__ctanh
	.weak	ctanhf64
	.set	ctanhf64,__ctanh
	.weak	ctanh
	.set	ctanh,__ctanh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1083176960
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.align 8
.LC10:
	.long	0
	.long	1048576
	.align 8
.LC11:
	.long	0
	.long	1074790400
	.align 8
.LC12:
	.long	0
	.long	1018167296
