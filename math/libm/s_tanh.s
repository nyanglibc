	.text
	.p2align 4,,15
	.globl	__tanh
	.type	__tanh, @function
__tanh:
	pushq	%rbx
	movq	%xmm0, %rbx
	shrq	$32, %rbx
	movl	%ebx, %eax
	andl	$2147483647, %eax
	cmpl	$2146435071, %eax
	jle	.L2
	movsd	.LC0(%rip), %xmm2
	testl	%ebx, %ebx
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	js	.L3
	addsd	%xmm2, %xmm1
.L1:
	movapd	%xmm1, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1077280767, %eax
	jle	.L16
	movsd	.LC0(%rip), %xmm1
	subsd	.LC6(%rip), %xmm1
.L10:
	testl	%ebx, %ebx
	jns	.L1
	xorpd	.LC5(%rip), %xmm1
	popq	%rbx
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movd	%xmm0, %ecx
	movapd	%xmm0, %xmm1
	orl	%eax, %ecx
	je	.L1
	cmpl	$1015021567, %eax
	andpd	.LC1(%rip), %xmm1
	jg	.L6
	movsd	.LC2(%rip), %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.L7
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
.L7:
	movsd	.LC0(%rip), %xmm1
	addsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	subsd	%xmm2, %xmm1
	popq	%rbx
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1072693247, %eax
	jle	.L9
	addsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	call	__expm1@PLT
	movsd	.LC3(%rip), %xmm1
	addsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	movsd	.LC0(%rip), %xmm1
	subsd	%xmm0, %xmm1
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movsd	.LC4(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	call	__expm1@PLT
	movapd	%xmm0, %xmm1
	addsd	.LC3(%rip), %xmm0
	xorpd	.LC5(%rip), %xmm1
	divsd	%xmm0, %xmm1
	jmp	.L10
	.size	__tanh, .-__tanh
	.weak	tanhf32x
	.set	tanhf32x,__tanh
	.weak	tanhf64
	.set	tanhf64,__tanh
	.weak	tanh
	.set	tanh,__tanh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC2:
	.long	0
	.long	1048576
	.align 8
.LC3:
	.long	0
	.long	1073741824
	.align 8
.LC4:
	.long	0
	.long	-1073741824
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	3271095129
	.long	27618847
