	.text
	.globl	__subtf3
	.globl	__divtf3
	.globl	__addtf3
	.globl	__multf3
	.p2align 4,,15
	.globl	__ieee754_acoshf128
	.type	__ieee754_acoshf128, @function
__ieee754_acoshf128:
	subq	$56, %rsp
	movabsq	$4611404543450677247, %rdx
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	cmpq	%rdx, %rax
	jle	.L10
	movabsq	$4626604192193052671, %rdx
	cmpq	%rdx, %rax
	jle	.L4
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rax
	jle	.L5
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsp), %rcx
	movabsq	$-4611404543450677248, %rdx
	addq	%rax, %rdx
	orq	%rcx, %rdx
	je	.L7
	movabsq	$4611686018427387904, %rdx
	cmpq	%rdx, %rax
	jg	.L11
	movdqa	(%rsp), %xmm0
	movdqa	.LC2(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	addq	$56, %rsp
	jmp	__log1pf128@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	%xmm0, %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
.L1:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	pxor	%xmm0, %xmm0
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqa	(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__subtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	addq	$56, %rsp
	jmp	__ieee754_logf128@PLT
	.size	__ieee754_acoshf128, .-__ieee754_acoshf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676288
