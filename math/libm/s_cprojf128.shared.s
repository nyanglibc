	.text
	.globl	__unordtf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__cprojf128
	.type	__cprojf128, @function
__cprojf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movdqa	32(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L6
	movdqa	(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L2
.L6:
	movdqa	48(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L4
	movdqa	(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L2
.L4:
	movdqa	32(%rsp), %xmm3
	movq	%rbx, %rax
	movdqa	48(%rsp), %xmm4
	movaps	%xmm3, (%rbx)
	movaps	%xmm4, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pxor	%xmm0, %xmm0
	movdqa	48(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	.LC3(%rip), %xmm1
	movq	%rbx, %rax
	movaps	%xmm1, (%rbx)
	movaps	%xmm0, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__cprojf128, .-__cprojf128
	.weak	cprojf128
	.set	cprojf128,__cprojf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	2147418112
