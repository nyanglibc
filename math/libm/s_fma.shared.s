	.text
	.p2align 4,,15
	.globl	__fma
	.type	__fma, @function
__fma:
	movq	.LC0(%rip), %xmm3
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm4
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm4
	jb	.L2
	movapd	%xmm1, %xmm5
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm4
	jb	.L2
	andpd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm4
	jb	.L33
	pxor	%xmm4, %xmm4
	movl	$0, %ecx
	ucomisd	%xmm4, %xmm0
	setnp	%dl
	cmovne	%ecx, %edx
	ucomisd	%xmm4, %xmm1
	setnp	%al
	cmovne	%ecx, %eax
	orb	%al, %dl
	je	.L8
	ucomisd	%xmm4, %xmm2
	setnp	%al
	cmove	%eax, %ecx
	testb	%cl, %cl
	jne	.L34
.L8:
	pushq	%rbp
	pushq	%rbx
	subq	$152, %rsp
	leaq	112(%rsp), %rbx
	movsd	%xmm2, (%rsp)
	movsd	%xmm1, 32(%rsp)
	movq	%rbx, %rdi
	movsd	%xmm0, 16(%rsp)
	call	__GI_feholdexcept
	xorl	%edi, %edi
	call	__GI_fesetround
	fldl	16(%rsp)
	fldl	.LC3(%rip)
	fld	%st(1)
	fmul	%st(1), %st
	fldl	32(%rsp)
	fmul	%st, %st(2)
	fld	%st(3)
	fmul	%st(1), %st
	fld	%st(4)
	fsub	%st(3), %st
	faddp	%st, %st(3)
	fld	%st(1)
	fsub	%st(4), %st
	faddp	%st, %st(4)
	fxch	%st(4)
	fsub	%st(2), %st
	fxch	%st(1)
	fsub	%st(3), %st
	fld	%st(2)
	fmul	%st(4), %st
	fsub	%st(5), %st
	fxch	%st(3)
	fmul	%st(1), %st
	faddp	%st, %st(3)
	fxch	%st(3)
	fmul	%st(1), %st
	faddp	%st, %st(2)
	fmulp	%st, %st(2)
	faddp	%st, %st(1)
	fldl	(%rsp)
	fld	%st(0)
	fadd	%st(3), %st
	fld	%st(0)
	fsub	%st(2), %st
	fld	%st(1)
	fstpt	32(%rsp)
	fsubr	%st, %st(1)
	fsubr	%st(4), %st
	fxch	%st(4)
	fstpt	64(%rsp)
	fsubrp	%st, %st(1)
	faddp	%st, %st(2)
	fxch	%st(1)
	fld	%st(0)
	fstpt	48(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fstp	%st(0)
	movl	$32, %edi
	call	__GI_feclearexcept
	movl	$0, %edx
	fldz
	fldt	32(%rsp)
	fucomi	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	fldt	16(%rsp)
	je	.L35
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L9
	fstp	%st(0)
	fstp	%st(0)
	movq	%rbx, %rdi
	call	__GI_feupdateenv
	movsd	(%rsp), %xmm0
	movsd	%xmm0, (%rsp)
	fldl	(%rsp)
	fldt	64(%rsp)
	faddp	%st, %st(1)
	fstpl	88(%rsp)
	movsd	88(%rsp), %xmm0
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	fstp	%st(2)
.L9:
	fstpt	16(%rsp)
	movl	$3072, %edi
	fstpt	(%rsp)
	call	__GI_fesetround
	fldt	48(%rsp)
	fldt	(%rsp)
	faddp	%st, %st(1)
	fldt	16(%rsp)
	faddp	%st, %st(1)
	fld	%st(0)
	fstpt	(%rsp)
	movq	(%rsp), %rbp
	testb	$1, %bpl
	fstpt	96(%rsp)
	jne	.L10
	movq	8(%rsp), %rax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L10
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, %ebp
	movl	%ebp, 96(%rsp)
	fldt	96(%rsp)
	fstpt	(%rsp)
.L10:
	movq	%rbx, %rdi
	call	__GI_feupdateenv
	fldt	(%rsp)
	fstpl	88(%rsp)
	movsd	88(%rsp), %xmm0
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	mulsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	addsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	ret
.L34:
	movapd	%xmm0, %xmm3
	mulsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	ret
	.size	__fma, .-__fma
	.weak	fmaf32x
	.set	fmaf32x,__fma
	.weak	fmaf64
	.set	fmaf64,__fma
	.weak	fma
	.set	fma,__fma
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC3:
	.long	1048576
	.long	1106247680
