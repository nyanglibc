	.text
	.p2align 4,,15
	.globl	__fmal
	.type	__fmal, @function
__fmal:
	pushq	%rbp
	pushq	%rbx
	subq	$168, %rsp
	fldt	192(%rsp)
	fld	%st(0)
	fstpt	80(%rsp)
	movzwl	88(%rsp), %ecx
	fldt	208(%rsp)
	andw	$32767, %cx
	movzwl	%cx, %r8d
	fld	%st(0)
	fstpt	96(%rsp)
	movzwl	104(%rsp), %eax
	fldt	224(%rsp)
	andw	$32767, %ax
	movzwl	%ax, %edx
	addl	%edx, %r8d
	cmpw	$32702, %cx
	seta	%sil
	cmpl	$49085, %r8d
	setg	%dl
	orl	%esi, %edx
	cmpw	$32702, %ax
	seta	%sil
	orb	%dl, %sil
	fstpt	112(%rsp)
	jne	.L95
	movzwl	120(%rsp), %edi
	andw	$32767, %di
	cmpw	$32702, %di
	ja	.L96
	cmpl	$16447, %r8d
	setle	%dil
	jle	.L97
	fldz
	pxor	%xmm0, %xmm0
	fucomi	%st(2), %st
	fstp	%st(2)
	movss	%xmm0, (%rsp)
	setnp	%al
	cmovne	%edi, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%edi, %eax
	xorl	%ebp, %ebp
.L3:
	orb	%al, %dl
	je	.L33
	fldz
	movl	$0, %edx
	fldt	224(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L90
	fstp	%st(0)
.L33:
	leaq	128(%rsp), %rbx
	movq	%rbx, %rdi
	call	__GI_feholdexcept
	xorl	%edi, %edi
	call	__GI_fesetround
	fldl	.LC7(%rip)
	fldt	192(%rsp)
	fmul	%st(1), %st
	fldt	208(%rsp)
	fmul	%st, %st(2)
	fldt	192(%rsp)
	fmul	%st(1), %st
	fldt	192(%rsp)
	fsub	%st(3), %st
	faddp	%st, %st(3)
	fld	%st(1)
	fsub	%st(4), %st
	faddp	%st, %st(4)
	fldt	192(%rsp)
	fsub	%st(3), %st
	fxch	%st(2)
	fsub	%st(4), %st
	fld	%st(3)
	fmul	%st(5), %st
	fsub	%st(2), %st
	fxch	%st(4)
	fmul	%st(1), %st
	faddp	%st, %st(4)
	fxch	%st(4)
	fmul	%st(2), %st
	faddp	%st, %st(3)
	fxch	%st(3)
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	fldt	224(%rsp)
	fadd	%st(2), %st
	fldt	224(%rsp)
	fsubr	%st(1), %st
	fld	%st(1)
	fstpt	32(%rsp)
	fsubr	%st, %st(1)
	fsubr	%st(3), %st
	fxch	%st(3)
	fstpt	64(%rsp)
	fldt	224(%rsp)
	fsubp	%st, %st(1)
	faddp	%st, %st(2)
	fxch	%st(1)
	fld	%st(0)
	fstpt	48(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fstp	%st(0)
	movl	$32, %edi
	call	__GI_feclearexcept
	movl	$0, %edx
	fldz
	fldt	32(%rsp)
	fucomi	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	fldt	16(%rsp)
	je	.L98
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L34
	fstp	%st(0)
	fstp	%st(0)
	movq	%rbx, %rdi
	call	__GI_feupdateenv
	fldt	224(%rsp)
	fldt	64(%rsp)
	faddp	%st, %st(1)
.L1:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	fstp	%st(2)
.L34:
	fstpt	32(%rsp)
	movl	$3072, %edi
	fstpt	16(%rsp)
	call	__GI_fesetround
	fldt	48(%rsp)
	fldt	16(%rsp)
	faddp	%st, %st(1)
	fld	%st(0)
	fstpt	16(%rsp)
	movq	16(%rsp), %rax
	fstpt	80(%rsp)
	andl	$1, %eax
	testl	%ebp, %ebp
	fldt	32(%rsp)
	jne	.L35
	testl	%eax, %eax
	jne	.L36
	movq	24(%rsp), %rax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L36
	fstpt	(%rsp)
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, 80(%rsp)
	fldt	(%rsp)
.L36:
	movq	%rbx, %rdi
	fstpt	(%rsp)
	call	__GI_feupdateenv
	fldt	80(%rsp)
	fldt	(%rsp)
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L96:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L97:
	fstp	%st(0)
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	232(%rsp), %rdi
	andw	$32767, %di
	cmpw	$32767, %di
	je	.L91
	fldz
	movl	$1, %r11d
	movl	$0, %ebx
	pxor	%xmm1, %xmm1
	fldt	208(%rsp)
	movss	%xmm1, (%rsp)
	fucomip	%st(1), %st
	fldt	224(%rsp)
	setp	%r10b
	cmovne	%r11d, %r10d
	fucomip	%st(1), %st
	setnp	%r9b
	cmovne	%ebx, %r9d
	testb	%r9b, %r10b
	je	.L99
	fldt	192(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%r9b
	cmove	%r9d, %r11d
	testb	%r11b, %r11b
	je	.L8
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L101:
	fstp	%st(0)
.L11:
	fldt	192(%rsp)
	fldt	208(%rsp)
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	fstp	%st(0)
.L8:
	movq	200(%rsp), %rbp
	andw	$32767, %bp
	cmpw	$32767, %bp
	je	.L9
	movq	216(%rsp), %r9
	andw	$32767, %r9w
	cmpw	$32767, %r9w
	je	.L9
	fldz
	movl	$0, %r10d
	fldt	192(%rsp)
	fucomip	%st(1), %st
	fldt	208(%rsp)
	setnp	%bl
	cmovne	%r10d, %ebx
	fucomip	%st(1), %st
	setnp	%r11b
	cmove	%r11d, %r10d
	orb	%bl, %r10b
	jne	.L100
	cmpl	$49150, %r8d
	jg	.L101
	movzwl	120(%rsp), %r11d
	andw	$32767, %r11w
	cmpl	$16316, %r8d
	jg	.L12
	fstp	%st(0)
	movzbl	89(%rsp), %edx
	movzbl	105(%rsp), %eax
	shrb	$7, %dl
	shrb	$7, %al
	cmpb	%al, %dl
	je	.L46
	fldt	.LC0(%rip)
.L13:
	cmpw	$2, %r11w
	fldt	224(%rsp)
	ja	.L87
	fmuls	.LC3(%rip)
	faddp	%st, %st(1)
	fld	%st(0)
	fstpt	96(%rsp)
	movzwl	104(%rsp), %eax
	andw	$32767, %ax
	cmpw	$65, %ax
	ja	.L15
	fldt	192(%rsp)
	fldt	208(%rsp)
	fmulp	%st, %st(1)
	fstp	%st(0)
.L15:
	fmuls	.LC4(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	fldz
	movl	$1, %ecx
	movl	$0, %esi
	fldt	208(%rsp)
	fucomip	%st(1), %st
	fldt	224(%rsp)
	setp	%dl
	cmovne	%ecx, %edx
	fucomip	%st(1), %st
	setnp	%al
	cmovne	%esi, %eax
	testb	%al, %dl
	je	.L102
	fldt	192(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%al
	cmove	%eax, %ecx
	testb	%cl, %cl
	jne	.L11
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L100:
	fstp	%st(0)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L102:
	fstp	%st(0)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L103:
	fstp	%st(0)
.L9:
	fldt	192(%rsp)
	fldt	208(%rsp)
	fmulp	%st, %st(1)
	fldt	224(%rsp)
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	movq	200(%rsp), %rax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L5
	movq	216(%rsp), %rax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L6
	fldt	192(%rsp)
	fldt	224(%rsp)
	faddp	%st, %st(1)
	fldt	208(%rsp)
	faddp	%st, %st(1)
	jmp	.L1
.L90:
	fldt	192(%rsp)
	fldt	208(%rsp)
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	fldz
	movl	$0, %eax
	movl	$1, %ecx
	fldt	224(%rsp)
	fucomip	%st(1), %st
	fldt	208(%rsp)
	setnp	%dl
	cmovne	%eax, %edx
	fucomip	%st(1), %st
	setp	%al
	cmovne	%ecx, %eax
	testb	%al, %dl
	je	.L103
	fldt	192(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%al
	cmove	%eax, %ecx
	testb	%cl, %cl
	je	.L9
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	$1, %ebp
	jne	.L37
	testl	%eax, %eax
	jne	.L38
	movq	24(%rsp), %rax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L38
	fstpt	(%rsp)
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, 80(%rsp)
	fldt	(%rsp)
.L38:
	movq	%rbx, %rdi
	fstpt	(%rsp)
	call	__GI_feupdateenv
	fldt	80(%rsp)
	fldt	(%rsp)
	faddp	%st, %st(1)
	fmuls	.LC5(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$49085, %r8d
	jle	.L16
	cmpw	%ax, %cx
	jbe	.L17
	movzwl	88(%rsp), %eax
	subl	$64, %ecx
	movl	%r10d, %edx
	andw	$32767, %cx
	andw	$-32768, %ax
	orl	%ecx, %eax
	movw	%ax, 88(%rsp)
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	setnp	%al
	cmove	%eax, %edx
	xorl	%eax, %eax
.L18:
	cmpw	$64, %r11w
	movl	$1, %ebp
	jbe	.L3
	movzwl	120(%rsp), %ecx
	subl	$64, %r11d
	andw	$32767, %r11w
	andw	$-32768, %cx
	orl	%r11d, %ecx
	movw	%cx, 120(%rsp)
	fldt	112(%rsp)
	fstpt	224(%rsp)
	jmp	.L3
.L16:
	cmpw	$32702, %r11w
	jbe	.L20
	fstp	%st(0)
	cmpl	$16511, %r8d
	jg	.L21
	cmpw	%ax, %cx
	jbe	.L22
	leal	130(%rcx), %eax
.L85:
	andw	$32767, %ax
	movl	%eax, %edx
	movzwl	88(%rsp), %eax
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 88(%rsp)
.L23:
	movzwl	120(%rsp), %eax
	subl	$64, %r11d
	movl	$0, %ecx
	andw	$32767, %r11w
	movl	$1, %ebp
	andw	$-32768, %ax
	orl	%r11d, %eax
	movw	%ax, 120(%rsp)
	fldt	112(%rsp)
	fstpt	224(%rsp)
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fldz
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%ecx, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%ecx, %eax
	jmp	.L3
.L94:
	fldt	80(%rsp)
	faddp	%st, %st(1)
	fld	%st(0)
	fstpt	16(%rsp)
	movq	24(%rsp), %rax
	andw	$32767, %ax
	cmpw	$131, %ax
	je	.L92
	fstp	%st(0)
	flds	(%rsp)
	movl	96(%rsp), %edx
	movzbl	105(%rsp), %ecx
	fstpt	112(%rsp)
	leal	(%rdx,%rdx), %eax
	andl	$-4, %edx
	movl	%edx, 96(%rsp)
	andl	$6, %eax
	andl	$-128, %ecx
	orl	$1, %eax
	movl	%eax, 112(%rsp)
	movzbl	121(%rsp), %eax
	fldt	96(%rsp)
	andl	$127, %eax
	orl	%ecx, %eax
	movb	%al, 121(%rsp)
	fmull	.LC8(%rip)
	fldt	112(%rsp)
	fmuls	.LC9(%rip)
.L87:
	faddp	%st, %st(1)
	jmp	.L1
.L46:
	fldt	.LC1(%rip)
	jmp	.L13
.L17:
	subl	$64, %eax
	andw	$32767, %ax
	movl	%eax, %edx
	movzwl	104(%rsp), %eax
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 104(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	setnp	%al
	cmovne	%r10d, %eax
	xorl	%edx, %edx
	jmp	.L18
.L20:
	testb	%dl, %dl
	je	.L25
	movzwl	88(%rsp), %edx
	subl	$64, %ecx
	andw	$32767, %cx
	andw	$-32768, %dx
	orl	%ecx, %edx
	testw	%r9w, %r9w
	movw	%dx, 88(%rsp)
	je	.L26
	fldt	80(%rsp)
	addl	$64, %eax
	andw	$32767, %ax
	movl	%eax, %edx
	movzwl	104(%rsp), %eax
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 104(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fxch	%st(1)
	fucomip	%st(2), %st
	setnp	%al
	cmovne	%r10d, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%r10d, %eax
	xorl	%ebp, %ebp
	jmp	.L3
.L37:
	testl	%eax, %eax
	jne	.L39
	fstpt	32(%rsp)
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, 80(%rsp)
	fldt	80(%rsp)
	fstpt	16(%rsp)
	fldt	32(%rsp)
.L39:
	fldt	16(%rsp)
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	16(%rsp)
	fld	%st(0)
	fstpt	96(%rsp)
	fstp	%st(0)
	movl	$32, %edi
	call	__GI_fetestexcept
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__GI_feupdateenv
	testl	%ebp, %ebp
	fldt	16(%rsp)
	je	.L104
	movzwl	104(%rsp), %eax
	andw	$32767, %ax
	cmpw	$130, %ax
	ja	.L93
	je	.L94
	fstp	%st(0)
	orl	$1, 96(%rsp)
	jmp	.L88
.L104:
	fstp	%st(0)
.L88:
	fldt	96(%rsp)
	fmull	.LC8(%rip)
	jmp	.L1
.L21:
	cmpw	%ax, %cx
	jbe	.L24
	cmpw	$64, %cx
	jbe	.L23
	leal	-64(%rcx), %eax
	jmp	.L85
.L25:
	fstp	%st(0)
	testb	%sil, %sil
	je	.L27
	subl	$64, %eax
	andw	$32767, %ax
	movl	%eax, %edx
	movzwl	104(%rsp), %eax
	andw	$-32768, %ax
	orl	%edx, %eax
	testw	%bp, %bp
	movw	%ax, 104(%rsp)
	je	.L28
	leal	64(%rcx), %eax
	movl	$0, %ecx
	andw	$32767, %ax
	movl	%eax, %edx
	movzwl	88(%rsp), %eax
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 88(%rsp)
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fldz
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%ecx, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%ecx, %eax
	xorl	%ebp, %ebp
	jmp	.L3
.L22:
	addw	$130, %ax
.L86:
	andw	$32767, %ax
	movl	%eax, %edx
	movzwl	104(%rsp), %eax
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 104(%rsp)
	jmp	.L23
.L27:
	cmpw	%ax, %cx
	jbe	.L29
	movzwl	88(%rsp), %edx
	leal	130(%rcx), %eax
	andw	$32767, %ax
	andw	$-32768, %dx
	orl	%edx, %eax
	movw	%ax, 88(%rsp)
.L30:
	cmpw	$262, %r11w
	jbe	.L31
	fldt	80(%rsp)
	movl	$0, %ecx
	fld	%st(0)
	fstpt	192(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fldz
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%ecx, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%ecx, %eax
	xorl	%ebp, %ebp
	jmp	.L3
.L93:
	fldt	80(%rsp)
	faddp	%st, %st(1)
	fmull	.LC8(%rip)
	jmp	.L1
.L26:
	fldt	208(%rsp)
	fmuls	.LC5(%rip)
	fstpt	208(%rsp)
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fucomip	%st(1), %st
	fldt	208(%rsp)
	setnp	%al
	cmovne	%r10d, %eax
	movl	%eax, %edx
	fucomip	%st(1), %st
	fstp	%st(0)
	setnp	%al
	cmovne	%r10d, %eax
	xorl	%ebp, %ebp
	jmp	.L3
.L24:
	cmpw	$64, %ax
	jbe	.L23
	subl	$64, %eax
	jmp	.L86
.L28:
	fldt	192(%rsp)
	movl	$0, %ecx
	fmuls	.LC5(%rip)
	fld	%st(0)
	fstpt	192(%rsp)
	fld	%st(0)
	fstpt	80(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fldz
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%ecx, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%ecx, %eax
	xorl	%ebp, %ebp
	jmp	.L3
.L31:
	testw	%di, %di
	je	.L32
	movzwl	120(%rsp), %edx
	leal	130(%r11), %eax
	movl	$0, %ecx
	andw	$32767, %ax
	andw	$-32768, %dx
	orl	%edx, %eax
	movw	%ax, 120(%rsp)
	fldt	112(%rsp)
	fstpt	224(%rsp)
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fldz
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%ecx, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%ecx, %eax
	orl	$-1, %ebp
	jmp	.L3
.L29:
	movzwl	104(%rsp), %edx
	addw	$130, %ax
	andw	$32767, %ax
	andw	$-32768, %dx
	orl	%edx, %eax
	movw	%ax, 104(%rsp)
	jmp	.L30
.L32:
	fldt	224(%rsp)
	movl	$0, %ecx
	fmull	.LC6(%rip)
	fstpt	224(%rsp)
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	192(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	208(%rsp)
	fldz
	fucomi	%st(2), %st
	fstp	%st(2)
	setnp	%al
	cmovne	%ecx, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	movl	%eax, %edx
	setnp	%al
	cmovne	%ecx, %eax
	orl	$-1, %ebp
	jmp	.L3
.L92:
	fmull	.LC8(%rip)
	jmp	.L1
	.size	__fmal, .-__fmal
	.weak	fmaf64x
	.set	fmaf64x,__fmal
	.weak	fmal
	.set	fmal,__fmal
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	0
	.long	32768
	.long	0
	.align 16
.LC1:
	.long	1
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	1610612736
	.align 4
.LC4:
	.long	520093696
	.align 4
.LC5:
	.long	1602224128
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	0
	.long	1209008128
	.align 8
.LC7:
	.long	1048576
	.long	1106247680
	.align 8
.LC8:
	.long	0
	.long	936378368
	.section	.rodata.cst4
	.align 4
.LC9:
	.long	1048576000
