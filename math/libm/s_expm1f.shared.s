	.text
	.p2align 4,,15
	.globl	__expm1f
	.type	__expm1f, @function
__expm1f:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	cmpl	$1100331075, %eax
	jbe	.L2
	cmpl	$1118925335, %eax
	jbe	.L3
	cmpl	$2139095040, %eax
	ja	.L35
	je	.L36
	ucomiss	.LC5(%rip), %xmm0
	ja	.L37
	.p2align 4,,10
	.p2align 3
.L3:
	testl	%edx, %edx
	jns	.L38
	movss	.LC7(%rip), %xmm1
	addss	%xmm1, %xmm0
	movaps	%xmm1, %xmm0
	subss	.LC8(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1051816472, %eax
	ja	.L39
	cmpl	$855638015, %eax
	jbe	.L40
	xorl	%ecx, %ecx
	movss	.LC0(%rip), %xmm4
.L13:
	movaps	%xmm0, %xmm6
	testl	%ecx, %ecx
	movaps	%xmm0, %xmm2
	mulss	%xmm4, %xmm6
	movss	.LC13(%rip), %xmm1
	movss	.LC8(%rip), %xmm5
	mulss	%xmm6, %xmm2
	movaps	%xmm6, %xmm7
	movss	.LC18(%rip), %xmm6
	mulss	%xmm2, %xmm1
	addss	.LC14(%rip), %xmm1
	mulss	%xmm2, %xmm1
	subss	.LC15(%rip), %xmm1
	mulss	%xmm2, %xmm1
	addss	.LC16(%rip), %xmm1
	mulss	%xmm2, %xmm1
	subss	.LC17(%rip), %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm1, %xmm7
	subss	%xmm7, %xmm6
	movss	.LC19(%rip), %xmm7
	subss	%xmm6, %xmm1
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm7
	divss	%xmm7, %xmm1
	mulss	%xmm2, %xmm1
	je	.L41
	subss	%xmm3, %xmm1
	cmpl	$-1, %ecx
	mulss	%xmm0, %xmm1
	subss	%xmm3, %xmm1
	subss	%xmm2, %xmm1
	je	.L42
	cmpl	$1, %ecx
	je	.L43
	leal	1(%rcx), %edx
	movl	%ecx, %eax
	sall	$23, %eax
	cmpl	$57, %edx
	ja	.L44
	cmpl	$22, %ecx
	jle	.L45
	movl	$127, %edx
	subl	%ecx, %edx
	sall	$23, %edx
	movl	%edx, -4(%rsp)
	addss	-4(%rsp), %xmm1
	subss	%xmm1, %xmm0
	addss	%xmm5, %xmm0
	movd	%xmm0, %esi
	addl	%esi, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movaps	%xmm0, %xmm1
	movss	.LC12(%rip), %xmm2
	andps	.LC11(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jbe	.L14
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
.L14:
	movss	.LC6(%rip), %xmm1
	addss	%xmm0, %xmm1
	subss	%xmm1, %xmm1
	subss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$1065686417, %eax
	ja	.L9
	testl	%edx, %edx
	movaps	%xmm0, %xmm3
	jns	.L46
	addss	.LC9(%rip), %xmm3
	movl	$-1, %ecx
	movss	.LC2(%rip), %xmm1
	movss	.LC0(%rip), %xmm4
	.p2align 4,,10
	.p2align 3
.L11:
	movaps	%xmm3, %xmm0
	subss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	subss	%xmm1, %xmm3
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L35:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movss	.LC10(%rip), %xmm2
	mulss	%xmm0, %xmm2
.L33:
	movss	.LC0(%rip), %xmm4
	movaps	%xmm4, %xmm1
.L12:
	addss	%xmm2, %xmm1
	movss	.LC9(%rip), %xmm2
	movaps	%xmm0, %xmm3
	cvttss2si	%xmm1, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2ss	%ecx, %xmm1
	mulss	%xmm1, %xmm2
	mulss	.LC3(%rip), %xmm1
	subss	%xmm2, %xmm3
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L41:
	mulss	%xmm0, %xmm1
	subss	%xmm2, %xmm1
	subss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	subss	.LC9(%rip), %xmm3
	movl	$1, %ecx
	movss	.LC3(%rip), %xmm1
	movss	.LC0(%rip), %xmm4
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L42:
	subss	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	subss	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movss	.LC10(%rip), %xmm2
	testl	%edx, %edx
	mulss	%xmm0, %xmm2
	jns	.L33
	movss	.LC1(%rip), %xmm1
	movss	.LC0(%rip), %xmm4
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L43:
	movss	.LC20(%rip), %xmm2
	ucomiss	%xmm0, %xmm2
	jbe	.L31
	addss	%xmm4, %xmm0
	subss	%xmm0, %xmm1
	movss	.LC21(%rip), %xmm0
	mulss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	subss	%xmm0, %xmm1
	movaps	%xmm5, %xmm4
	subss	%xmm1, %xmm4
	movd	%xmm4, %edx
	addl	%edx, %eax
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
	subss	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	errno@gottpoff(%rip), %rax
	movss	.LC6(%rip), %xmm0
	mulss	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	subss	%xmm1, %xmm0
	addss	%xmm0, %xmm0
	addss	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$16777216, %edx
	subss	%xmm0, %xmm1
	sarl	%cl, %edx
	movl	$1065353216, %ecx
	subl	%edx, %ecx
	movl	%ecx, -4(%rsp)
	movss	-4(%rsp), %xmm5
	subss	%xmm1, %xmm5
	movd	%xmm5, %edx
	addl	%edx, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	testl	%edx, %edx
	js	.L25
	rep ret
	.p2align 4,,10
	.p2align 3
.L25:
	movss	.LC4(%rip), %xmm0
	ret
	.size	__expm1f, .-__expm1f
	.weak	expm1f32
	.set	expm1f32,__expm1f
	.weak	expm1f
	.set	expm1f,__expm1f
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1056964608
	.align 4
.LC1:
	.long	3204448256
	.align 4
.LC2:
	.long	3071801297
	.align 4
.LC3:
	.long	924317649
	.align 4
.LC4:
	.long	3212836864
	.align 4
.LC5:
	.long	1118925184
	.align 4
.LC6:
	.long	1900671690
	.align 4
.LC7:
	.long	228737632
	.align 4
.LC8:
	.long	1065353216
	.align 4
.LC9:
	.long	1060204928
	.align 4
.LC10:
	.long	1069066811
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC12:
	.long	8388608
	.align 4
.LC13:
	.long	3025661371
	.align 4
.LC14:
	.long	914783828
	.align 4
.LC15:
	.long	950431949
	.align 4
.LC16:
	.long	986713345
	.align 4
.LC17:
	.long	1023969417
	.align 4
.LC18:
	.long	1077936128
	.align 4
.LC19:
	.long	1086324736
	.align 4
.LC20:
	.long	3196059648
	.align 4
.LC21:
	.long	3221225472
