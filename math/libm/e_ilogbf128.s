	.text
	.p2align 4,,15
	.globl	__ieee754_ilogbf128
	.type	__ieee754_ilogbf128, @function
__ieee754_ilogbf128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rdx
	movabsq	$9223372036854775807, %rax
	movq	-24(%rsp), %rcx
	andq	%rax, %rdx
	movabsq	$281474976710656, %rax
	cmpq	%rax, %rdx
	jg	.L2
	movq	%rcx, %rax
	orq	%rdx, %rax
	je	.L8
	testq	%rdx, %rdx
	jne	.L4
	testq	%rcx, %rcx
	movl	$-16431, %eax
	jle	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	addq	%rcx, %rcx
	subl	$1, %eax
	testq	%rcx, %rcx
	jg	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movabsq	$9223090561878065151, %rax
	cmpq	%rax, %rdx
	jle	.L15
	movabsq	$9223090561878065152, %rax
	xorq	%rax, %rdx
	xorl	%eax, %eax
	orq	%rcx, %rdx
	setne	%al
	addl	$2147483647, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	sarq	$48, %rdx
	leal	-16383(%rdx), %eax
	ret
.L8:
	movl	$-2147483648, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	salq	$15, %rdx
	movl	$-16382, %eax
	testq	%rdx, %rdx
	jle	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	addq	%rdx, %rdx
	subl	$1, %eax
	testq	%rdx, %rdx
	jg	.L6
	rep ret
	.size	__ieee754_ilogbf128, .-__ieee754_ilogbf128
