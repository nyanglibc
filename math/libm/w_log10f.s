	.text
	.p2align 4,,15
	.globl	__log10f
	.type	__log10f, @function
__log10f:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L7
.L2:
	jmp	__ieee754_log10f@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ucomiss	%xmm1, %xmm0
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__log10f, .-__log10f
	.weak	log10f32
	.set	log10f32,__log10f
	.weak	log10f
	.set	log10f,__log10f
