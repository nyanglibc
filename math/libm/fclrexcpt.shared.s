	.text
	.p2align 4,,15
	.globl	__GI_feclearexcept
	.hidden	__GI_feclearexcept
	.type	__GI_feclearexcept, @function
__GI_feclearexcept:
	andl	$61, %edi
	movl	%edi, %eax
#APP
# 32 "../sysdeps/x86_64/fpu/fclrexcpt.c" 1
	fnstenv -40(%rsp)
# 0 "" 2
#NO_APP
	xorl	$61, %eax
	andw	%ax, -36(%rsp)
#APP
# 38 "../sysdeps/x86_64/fpu/fclrexcpt.c" 1
	fldenv -40(%rsp)
# 0 "" 2
# 41 "../sysdeps/x86_64/fpu/fclrexcpt.c" 1
	stmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	notl	%edi
	andl	%edi, -44(%rsp)
#APP
# 47 "../sysdeps/x86_64/fpu/fclrexcpt.c" 1
	ldmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	__GI_feclearexcept, .-__GI_feclearexcept
	.globl	feclearexcept
	.set	feclearexcept,__GI_feclearexcept
