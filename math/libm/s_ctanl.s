	.text
	.p2align 4,,15
	.globl	__ctanl
	.type	__ctanl, @function
__ctanl:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%rbx
	subq	$120, %rsp
	fldt	16(%rbp)
	fldt	32(%rbp)
	fld	%st(1)
	fabs
	fldt	.LC2(%rip)
	fucomi	%st(1), %st
	jb	.L2
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	jb	.L71
	fld	%st(2)
	fnstcw	-50(%rbp)
	movzwl	-50(%rbp), %eax
	fldln2
	fmuls	.LC7(%rip)
	orb	$12, %ah
	movw	%ax, -52(%rbp)
	fmuls	.LC8(%rip)
	fldcw	-52(%rbp)
	fistpl	-80(%rbp)
	fldcw	-50(%rbp)
	fldt	.LC9(%rip)
	fxch	%st(3)
	fucomip	%st(3), %st
	fstp	%st(2)
	jbe	.L64
	fstp	%st(1)
	fxch	%st(1)
	fld	%st(0)
	fstpt	-128(%rbp)
	fxch	%st(1)
	subq	$16, %rsp
	leaq	-48(%rbp), %rdi
	leaq	-32(%rbp), %rsi
	fstpt	-112(%rbp)
	fstpt	-96(%rbp)
	fstpt	(%rsp)
	call	__sincosl@PLT
	popq	%rdi
	popq	%r8
	fldt	-96(%rbp)
	fldt	-112(%rbp)
	fldt	-128(%rbp)
.L19:
	fildl	-80(%rbp)
	fxch	%st(2)
	fucomi	%st(2), %st
	ja	.L72
	fstp	%st(2)
	fxch	%st(1)
	fldt	.LC9(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L73
	fstp	%st(1)
	fld1
.L26:
	fldt	-32(%rbp)
	fld	%st(0)
	fmul	%st(1), %st
	fld	%st(1)
	fabs
	fmuls	.LC11(%rip)
	fxch	%st(4)
	fucomip	%st(4), %st
	fstp	%st(3)
	jbe	.L28
	fld	%st(3)
	fmul	%st(4), %st
	faddp	%st, %st(3)
.L28:
	fldt	-48(%rbp)
	fmulp	%st, %st(1)
	fdiv	%st(2), %st
	fxch	%st(3)
	fmulp	%st, %st(1)
	fdivp	%st, %st(1)
	fld	%st(1)
	fld	%st(1)
.L25:
	fldt	.LC9(%rip)
	fld	%st(4)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L79
	fxch	%st(3)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L79:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
.L30:
	fldt	.LC9(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L80
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L80:
	fstp	%st(2)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L83:
	fxch	%st(1)
.L1:
	movq	-8(%rbp), %rbx
	leave
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	fstpt	-128(%rbp)
	fxch	%st(1)
	movl	-80(%rbp), %eax
	subq	$16, %rsp
	addl	%eax, %eax
	movl	%eax, -80(%rbp)
	fstpt	-112(%rbp)
	fstpt	-96(%rbp)
	fildl	-80(%rbp)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-128(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fld1
	testb	$2, %ah
	fldt	-96(%rbp)
	fldt	-112(%rbp)
	jne	.L74
.L22:
	popq	%rcx
	popq	%rsi
	fsubr	%st, %st(1)
	fldt	-48(%rbp)
	fmuls	.LC10(%rip)
	fldt	-32(%rbp)
	fmulp	%st, %st(1)
	fdiv	%st(4), %st
	fxch	%st(2)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L66
	fstp	%st(0)
	fdivp	%st, %st(2)
	fld	%st(1)
	fld	%st(1)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L73:
	fstp	%st(0)
	fstp	%st(1)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	-96(%rbp)
	call	__ieee754_sinhl@PLT
	fstpt	-80(%rbp)
	subq	$16, %rsp
	fldt	-96(%rbp)
	fstpt	(%rsp)
	call	__ieee754_coshl@PLT
	fldt	-80(%rbp)
	addq	$32, %rsp
	fld	%st(0)
	fabs
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L74:
	fstp	%st(2)
	fxch	%st(1)
	fld1
	fchs
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L66:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	fstpt	-96(%rbp)
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	-80(%rbp)
	fadd	%st(0), %st
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-96(%rbp)
	popq	%rax
	popq	%rdx
	fdivp	%st, %st(1)
	fld	%st(0)
	fldt	-80(%rbp)
	fld	%st(0)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fstp	%st(0)
	fxam
	fnstsw	%ax
	movl	%eax, %ebx
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L81
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L84:
	fstp	%st(1)
.L34:
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L8
	je	.L75
.L8:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	flds	.LC0(%rip)
	jp	.L82
	je	.L76
	fstp	%st(1)
	jmp	.L36
.L82:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L36:
	fld	%st(0)
	fxch	%st(2)
.L15:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L83
	fxch	%st(1)
	fstpt	-96(%rbp)
	movl	$1, %edi
	fstpt	-80(%rbp)
	call	feraiseexcept@PLT
	fldt	-80(%rbp)
	fldt	-96(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L71:
	fstp	%st(0)
	fxch	%st(1)
	fxam
	fnstsw	%ax
	movl	%eax, %ebx
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L84
	fstp	%st(0)
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L9
	subq	$16, %rsp
	leaq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	fldt	-48(%rbp)
	fldt	-32(%rbp)
	fmulp	%st, %st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L11
	fstp	%st(0)
	fldz
	fchs
.L11:
	popq	%r9
	popq	%r10
.L12:
	andb	$2, %bh
	fld1
	jne	.L77
.L78:
	movq	-8(%rbp), %rbx
	leave
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	fxch	%st(3)
	fstpt	-48(%rbp)
	fld1
	fstpt	-32(%rbp)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L81:
	fstp	%st(0)
.L9:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L12
	fstp	%st(0)
	fldz
	fchs
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L77:
	fstp	%st(0)
	fld1
	fchs
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L76:
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L75:
	jmp	.L78
	.size	__ctanl, .-__ctanl
	.weak	ctanf64x
	.set	ctanf64x,__ctanl
	.weak	ctanl
	.set	ctanl,__ctanl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1182792704
	.align 4
.LC8:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	1082130432
	.align 4
.LC11:
	.long	536870912
