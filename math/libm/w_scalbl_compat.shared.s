	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	sysv_scalbl, @function
sysv_scalbl:
	subq	$72, %rsp
	fldt	80(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	48(%rsp)
	fstpt	16(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__ieee754_scalbl@PLT
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	sete	%al
	addq	$32, %rsp
	movzbl	%al, %eax
	testl	%eax, %eax
	fldt	(%rsp)
	je	.L2
	fldt	.LC0(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L13
	fstp	%st(0)
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	fstp	%st(0)
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fucomi	%st(1), %st
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L14
	fldz
	movl	$0, %edx
	fucomip	%st(2), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L15
	fstp	%st(1)
	fldt	16(%rsp)
	movl	$233, %edi
	fstpt	64(%rsp)
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	fstp	%st(1)
	fldt	16(%rsp)
	movl	$232, %edi
	fstpt	64(%rsp)
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
	.size	sysv_scalbl, .-sysv_scalbl
	.p2align 4,,15
	.globl	__scalbl
	.type	__scalbl, @function
__scalbl:
	subq	$40, %rsp
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	fldt	48(%rsp)
	movl	(%rax), %eax
	testl	%eax, %eax
	fldt	64(%rsp)
	fstpt	(%rsp)
	je	.L27
	pushq	8(%rsp)
	pushq	8(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	__ieee754_scalbl@PLT
	fld	%st(0)
	addq	$32, %rsp
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	16(%rsp)
	jb	.L29
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L30
	je	.L31
	fstp	%st(0)
	jmp	.L16
.L30:
	fstp	%st(0)
.L16:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	fxch	%st(1)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L31:
	fxch	%st(1)
.L18:
	fucomi	%st(0), %st
	jp	.L28
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L22
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L16
	fldt	(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L16
.L26:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	fstp	%st(0)
	addq	$40, %rsp
	jmp	sysv_scalbl
	.p2align 4,,10
	.p2align 3
.L22:
	fldz
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jp	.L24
	je	.L16
.L24:
	fldt	(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L26
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L28:
	fldt	(%rsp)
	fucomip	%st(2), %st
	fstp	%st(1)
	jp	.L16
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L16
	.size	__scalbl, .-__scalbl
	.weak	scalbl
	.set	scalbl,__scalbl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
