	.text
#APP
	.symver __totalordermag0,totalordermag@@GLIBC_2.31
	.symver __totalordermag1,totalordermagf64@@GLIBC_2.31
	.symver __totalordermag2,totalordermagf32x@@GLIBC_2.31
	.symver __totalordermag_compat3,totalordermag@GLIBC_2.25
	.symver __totalordermag_compat4,totalordermagf64@GLIBC_2.27
	.symver __totalordermag_compat5,totalordermagf32x@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__totalordermag
	.type	__totalordermag, @function
__totalordermag:
	movq	(%rdi), %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rdx
	andq	(%rsi), %rax
	cmpq	%rax, %rdx
	setbe	%al
	movzbl	%al, %eax
	ret
	.size	__totalordermag, .-__totalordermag
	.globl	__totalordermag2
	.set	__totalordermag2,__totalordermag
	.globl	__totalordermag1
	.set	__totalordermag1,__totalordermag
	.globl	__totalordermag0
	.set	__totalordermag0,__totalordermag
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalordermag_compat
	.type	__totalordermag_compat, @function
__totalordermag_compat:
	subq	$24, %rsp
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	movsd	%xmm0, 8(%rsp)
	movsd	%xmm1, (%rsp)
	call	__totalordermag@PLT
	addq	$24, %rsp
	ret
	.size	__totalordermag_compat, .-__totalordermag_compat
	.globl	__totalordermag_compat5
	.set	__totalordermag_compat5,__totalordermag_compat
	.globl	__totalordermag_compat4
	.set	__totalordermag_compat4,__totalordermag_compat
	.globl	__totalordermag_compat3
	.set	__totalordermag_compat3,__totalordermag_compat
