	.text
	.p2align 4,,15
	.globl	__setpayloadsigf128
	.type	__setpayloadsigf128, @function
__setpayloadsigf128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rdx
	movq	%rdx, %rsi
	shrq	$48, %rsi
	leal	-16383(%rsi), %eax
	cmpl	$110, %eax
	ja	.L6
	movl	$16495, %r8d
	movq	-24(%rsp), %rax
	subl	%esi, %r8d
	cmpl	$63, %r8d
	jg	.L4
	movl	%r8d, %ecx
	movq	$-1, %r9
	salq	%cl, %r9
	notq	%r9
	testq	%rax, %r9
	setne	%cl
.L5:
	testb	%cl, %cl
	jne	.L6
	testq	%rsi, %rsi
	je	.L12
	movabsq	$281474976710655, %rcx
	andq	%rcx, %rdx
	addq	$1, %rcx
	orq	%rdx, %rcx
	cmpl	$63, %r8d
	movq	%rcx, %r9
	jle	.L10
	movl	$16431, %ecx
	movq	%r9, %rax
	movabsq	$9223090561878065152, %rdx
	subl	%esi, %ecx
	shrq	%cl, %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L4:
	testq	%rax, %rax
	je	.L14
.L6:
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movaps	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%r8d, %r8d
	jne	.L11
.L12:
	movabsq	$9223090561878065152, %rcx
	orq	%rcx, %rdx
.L9:
	movq	%rax, -24(%rsp)
	movq	%rdx, -16(%rsp)
	xorl	%eax, %eax
	movdqa	-24(%rsp), %xmm1
	movaps	%xmm1, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$16431, %ecx
	movq	$-1, %r9
	subl	%esi, %ecx
	salq	%cl, %r9
	notq	%r9
	testq	%rdx, %r9
	setne	%cl
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L11:
	leal	-16431(%rsi), %ecx
	movq	%r9, %rdx
	salq	%cl, %rdx
	movl	%r8d, %ecx
	shrq	%cl, %rax
	shrq	%cl, %r9
	orq	%rdx, %rax
	movabsq	$9223090561878065152, %rdx
	orq	%r9, %rdx
	jmp	.L9
	.size	__setpayloadsigf128, .-__setpayloadsigf128
	.weak	setpayloadsigf128
	.set	setpayloadsigf128,__setpayloadsigf128
