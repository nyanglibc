	.text
	.p2align 4,,15
	.globl	__cpowf128
	.type	__cpowf128, @function
__cpowf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$40, %rsp
	movq	%rsp, %rbp
	pushq	88(%rsp)
	pushq	88(%rsp)
	pushq	88(%rsp)
	pushq	88(%rsp)
	movq	%rbp, %rdi
	call	__clogf128@PLT
	addq	$32, %rsp
	movdqa	(%rsp), %xmm0
	movq	%rbp, %rdi
	movdqa	112(%rsp), %xmm3
	movdqa	96(%rsp), %xmm2
	movdqa	16(%rsp), %xmm1
	call	__multc3@PLT
	pushq	24(%rsp)
	pushq	24(%rsp)
	movq	%rbp, %rdi
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__cexpf128@PLT
	movdqa	32(%rsp), %xmm0
	movq	%rbx, %rax
	movaps	%xmm0, (%rbx)
	movdqa	48(%rsp), %xmm0
	movaps	%xmm0, 16(%rbx)
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__cpowf128, .-__cpowf128
	.weak	cpowf128
	.set	cpowf128,__cpowf128
