	.text
	.globl	__addtf3
	.globl	__subtf3
	.globl	__divtf3
	.globl	__multf3
	.p2align 4,,15
	.globl	__ieee754_acosf128
	.type	__ieee754_acosf128, @function
__ieee754_acosf128:
	pushq	%rbx
	subq	$80, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rbx
	movdqa	(%rsp), %xmm3
	shrq	$32, %rbx
	movl	%edx, %esi
	movl	%ebx, %eax
	andl	$2147483647, %eax
	movq	%rax, %rcx
	salq	$32, %rcx
	orq	%rcx, %rsi
	cmpl	$1073676287, %eax
	movaps	%xmm3, 16(%rsp)
	movq	%rsi, 24(%rsp)
	jle	.L2
	cmpl	$1073676288, %eax
	je	.L15
.L3:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1073610751, %eax
	jg	.L5
	cmpl	$1066270719, %eax
	jle	.L16
	cmpl	$1073602559, %eax
	jle	.L17
	movdqa	.LC24(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC25(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	.LC26(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC27(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC28(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC29(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC30(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC31(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC32(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC33(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC34(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC35(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC36(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC37(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC38(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC39(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC40(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC41(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC42(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC43(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC44(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC45(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	testl	%ebx, %ebx
	js	.L18
	movdqa	.LC47(%rip), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1073627135, %eax
	jg	.L9
	movdqa	.LC48(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC49(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	.LC50(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC51(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC52(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC53(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC54(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC55(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC56(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC57(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC58(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC59(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC60(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC61(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC62(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC63(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC64(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC65(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC66(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC67(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC68(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC69(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	testl	%ebx, %ebx
	js	.L19
	movdqa	.LC71(%rip), %xmm1
	call	__addtf3@PLT
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rsp), %rax
	movq	%rax, %rcx
	shrq	$32, %rcx
	orl	%ecx, %edx
	orl	%eax, %edx
	jne	.L3
	testl	%ebx, %ebx
	pxor	%xmm0, %xmm0
	jns	.L1
	movdqa	.LC1(%rip), %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	.LC3(%rip), %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__addtf3@PLT
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movdqa	16(%rsp), %xmm1
	movdqa	.LC72(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC73(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	call	__ieee754_sqrtf128@PLT
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	$0, 16(%rsp)
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm4
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movaps	%xmm2, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC15(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC22(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC23(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	48(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 32(%rsp)
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	testl	%ebx, %ebx
	jns	.L11
	movdqa	%xmm0, %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__addtf3@PLT
.L11:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC22(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC23(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__subtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	%xmm0, %xmm1
	movdqa	.LC70(%rip), %xmm0
	call	__subtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	%xmm0, %xmm1
	movdqa	.LC46(%rip), %xmm0
	call	__subtf3@PLT
	jmp	.L1
	.size	__ieee754_acosf128, .-__ieee754_acosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	549186148
	.long	2793195328
	.long	2418335880
	.long	1066257682
	.align 16
.LC2:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073779231
	.align 16
.LC3:
	.long	549186148
	.long	2793195328
	.long	2418335880
	.long	1066192146
	.align 16
.LC4:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC5:
	.long	2994638726
	.long	3819129570
	.long	3570476991
	.long	1073026253
	.align 16
.LC6:
	.long	2893190845
	.long	725906007
	.long	3565776163
	.long	1073640230
	.align 16
.LC7:
	.long	4225383851
	.long	2864381606
	.long	442664683
	.long	1073963794
	.align 16
.LC8:
	.long	4059658242
	.long	2277313708
	.long	2961860365
	.long	1074192933
	.align 16
.LC9:
	.long	960451199
	.long	4058097474
	.long	2855542377
	.long	1074348328
	.align 16
.LC10:
	.long	3877092939
	.long	2668239585
	.long	4053202364
	.long	1074453802
	.align 16
.LC11:
	.long	4281363685
	.long	4155082959
	.long	2150590390
	.long	1074503485
	.align 16
.LC12:
	.long	3007142356
	.long	3841484484
	.long	2858373782
	.long	1074504875
	.align 16
.LC13:
	.long	712659232
	.long	1330351014
	.long	715739411
	.long	1074449247
	.align 16
.LC14:
	.long	3687091646
	.long	3455006158
	.long	2866287762
	.long	1074307559
	.align 16
.LC15:
	.long	9513418
	.long	1192469894
	.long	2988952859
	.long	1074023943
	.align 16
.LC16:
	.long	328987358
	.long	3111373982
	.long	1937287656
	.long	1074276813
	.align 16
.LC17:
	.long	2814616615
	.long	2507534042
	.long	1041954991
	.long	1074463607
	.align 16
.LC18:
	.long	4202130788
	.long	2294640750
	.long	754160365
	.long	1074593456
	.align 16
.LC19:
	.long	848089937
	.long	2831916931
	.long	1985850255
	.long	1074671422
	.align 16
.LC20:
	.long	626697192
	.long	1918250244
	.long	1059663819
	.long	1074707306
	.align 16
.LC21:
	.long	3802319913
	.long	1465678802
	.long	406921836
	.long	1074693750
	.align 16
.LC22:
	.long	1194376343
	.long	1155061677
	.long	523633905
	.long	1074625482
	.align 16
.LC23:
	.long	617833803
	.long	443770971
	.long	3223457646
	.long	1074477421
	.align 16
.LC24:
	.long	0
	.long	0
	.long	0
	.long	1073594368
	.align 16
.LC25:
	.long	1625524246
	.long	1286158639
	.long	4131703222
	.long	-1073917045
	.align 16
.LC26:
	.long	242427990
	.long	3519844094
	.long	670230183
	.long	1073616636
	.align 16
.LC27:
	.long	4292672910
	.long	4201816116
	.long	1684000152
	.long	1073896950
	.align 16
.LC28:
	.long	1197134870
	.long	2777800964
	.long	677648837
	.long	1073936683
	.align 16
.LC29:
	.long	544745963
	.long	738391317
	.long	721484562
	.long	1074044076
	.align 16
.LC30:
	.long	174777123
	.long	1146986237
	.long	2749335228
	.long	1074102719
	.align 16
.LC31:
	.long	3187003431
	.long	3655272581
	.long	527026084
	.long	1073963870
	.align 16
.LC32:
	.long	4167569405
	.long	3801580388
	.long	2085535822
	.long	1074141188
	.align 16
.LC33:
	.long	1065701261
	.long	122602228
	.long	3570457411
	.long	1074110471
	.align 16
.LC34:
	.long	2174169535
	.long	3011495639
	.long	2917678411
	.long	1073989578
	.align 16
.LC35:
	.long	1073008017
	.long	2234627286
	.long	2371763275
	.long	1073747646
	.align 16
.LC36:
	.long	27783823
	.long	1433508396
	.long	2987917335
	.long	1073619391
	.align 16
.LC37:
	.long	1056041773
	.long	2959348065
	.long	1598969611
	.long	1073945401
	.align 16
.LC38:
	.long	251189526
	.long	2280539282
	.long	3966504523
	.long	1073946902
	.align 16
.LC39:
	.long	1690176825
	.long	56488068
	.long	3657004691
	.long	1074073045
	.align 16
.LC40:
	.long	1178241084
	.long	837900601
	.long	2725839665
	.long	1074111340
	.align 16
.LC41:
	.long	2948840858
	.long	658969282
	.long	3916733909
	.long	1074004099
	.align 16
.LC42:
	.long	3728748066
	.long	3936932366
	.long	2953036882
	.long	1074143434
	.align 16
.LC43:
	.long	2146448032
	.long	1312700038
	.long	1684186354
	.long	1074106274
	.align 16
.LC44:
	.long	3420914003
	.long	3326233169
	.long	469986453
	.long	1073979989
	.align 16
.LC45:
	.long	1560958689
	.long	2047025361
	.long	3641365592
	.long	1073739085
	.align 16
.LC46:
	.long	2503189584
	.long	4291938030
	.long	3213550437
	.long	1073742597
	.align 16
.LC47:
	.long	1606859472
	.long	154109244
	.long	3950165719
	.long	1073684019
	.align 16
.LC48:
	.long	0
	.long	0
	.long	0
	.long	1073618944
	.align 16
.LC49:
	.long	1607178326
	.long	1104419777
	.long	1181518960
	.long	-1073917114
	.align 16
.LC50:
	.long	109461040
	.long	4067765236
	.long	1203914854
	.long	1073609603
	.align 16
.LC51:
	.long	915984218
	.long	949989413
	.long	3651840747
	.long	1073899621
	.align 16
.LC52:
	.long	3111858691
	.long	437513679
	.long	2567089527
	.long	1073921764
	.align 16
.LC53:
	.long	499926289
	.long	697971763
	.long	1867096265
	.long	1074055341
	.align 16
.LC54:
	.long	1002744012
	.long	2235396958
	.long	1487352072
	.long	1074104388
	.align 16
.LC55:
	.long	1206769445
	.long	3545988230
	.long	1203218911
	.long	1074001707
	.align 16
.LC56:
	.long	4127616959
	.long	1272581273
	.long	2010989980
	.long	1074152782
	.align 16
.LC57:
	.long	33346367
	.long	1994054562
	.long	2914874684
	.long	1074136968
	.align 16
.LC58:
	.long	2444275344
	.long	1752709555
	.long	537960493
	.long	1074029783
	.align 16
.LC59:
	.long	576620165
	.long	1480447225
	.long	2166760692
	.long	1073833886
	.align 16
.LC60:
	.long	1766657934
	.long	3510038169
	.long	1685648030
	.long	1073430623
	.align 16
.LC61:
	.long	192519496
	.long	121297041
	.long	1428643199
	.long	1073947244
	.align 16
.LC62:
	.long	24948848
	.long	135483122
	.long	2834575870
	.long	1073911458
	.align 16
.LC63:
	.long	1824090980
	.long	53764142
	.long	358676584
	.long	1074081078
	.align 16
.LC64:
	.long	1422916208
	.long	2719498036
	.long	4093711507
	.long	1074106807
	.align 16
.LC65:
	.long	793263510
	.long	1961376623
	.long	2984325471
	.long	1074036825
	.align 16
.LC66:
	.long	2638174537
	.long	1169991197
	.long	2761225039
	.long	1074154435
	.align 16
.LC67:
	.long	4159980182
	.long	1463222840
	.long	1832287078
	.long	1074129099
	.align 16
.LC68:
	.long	1513243139
	.long	2641248403
	.long	1942888300
	.long	1074017875
	.align 16
.LC69:
	.long	303700078
	.long	3229799768
	.long	123412897
	.long	1073817941
	.align 16
.LC70:
	.long	3776569899
	.long	4131212324
	.long	2866877999
	.long	1073747335
	.align 16
.LC71:
	.long	2415164981
	.long	951121311
	.long	697086598
	.long	1073672800
	.align 16
.LC72:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC73:
	.long	0
	.long	0
	.long	0
	.long	1073610752
