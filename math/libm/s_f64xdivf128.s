	.text
	.globl	__divtf3
	.globl	__trunctfxf2
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__netf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__f64xdivf128
	.type	__f64xdivf128, @function
__f64xdivf128:
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebp
	movaps	%xmm0, 16(%rsp)
	movl	%ebp, %eax
	andl	$-32704, %eax
	orl	$32640, %eax
	movaps	%xmm1, 32(%rsp)
	movl	%eax, 76(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	call	__divtf3@PLT
	movaps	%xmm0, (%rsp)
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %eax
	orl	%ebp, %eax
	movl	%eax, 76(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %ebp
	notl	%ebp
	testl	%edi, %ebp
	jne	.L22
.L2:
	shrl	$5, %ebx
	movq	%xmm0, %rax
	movabsq	$-4294967296, %rdx
	andl	$1, %ebx
	orl	(%rsp), %ebx
	andq	%rdx, %rax
	orq	%rbx, %rax
	movq	%rax, (%rsp)
	movdqa	(%rsp), %xmm0
	call	__trunctfxf2@PLT
	fld	%st(0)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L19
	fld	%st(0)
	fstpt	(%rsp)
	fucomip	%st(0), %st
	jp	.L23
	movdqa	16(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jne	.L1
	fstp	%st(0)
	movdqa	16(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jg	.L1
.L8:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	fldz
	movl	$0, %edx
	fucomip	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	fstpt	(%rsp)
	pxor	%xmm1, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	je	.L1
	fstp	%st(0)
	movdqa	32(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jne	.L8
	fstp	%st(0)
	movdqa	16(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jle	.L8
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movaps	%xmm0, 48(%rsp)
	call	__feraiseexcept@PLT
	movdqa	48(%rsp), %xmm0
	jmp	.L2
	.size	__f64xdivf128, .-__f64xdivf128
	.weak	f64xdivf128
	.set	f64xdivf128,__f64xdivf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
