	.text
	.globl	__subtf3
	.p2align 4,,15
	.globl	__cosf128
	.type	__cosf128, @function
__cosf128:
	subq	$56, %rsp
	movabsq	$9223372036854775807, %rdx
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	andq	%rdx, %rax
	movabsq	$4611283733356757713, %rdx
	cmpq	%rdx, %rax
	jle	.L14
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rax
	jle	.L4
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L15
.L5:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
.L1:
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	16(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__ieee754_rem_pio2f128@PLT
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L7
	cmpl	$2, %eax
	je	.L8
	testl	%eax, %eax
	je	.L16
	movdqa	32(%rsp), %xmm1
	movl	$1, %edi
	movdqa	16(%rsp), %xmm0
	call	__kernel_sinf128@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm1, %xmm1
	call	__kernel_cosf128@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movdqa	32(%rsp), %xmm1
	movl	$1, %edi
	movdqa	16(%rsp), %xmm0
	call	__kernel_sinf128@PLT
	pxor	.LC1(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rsp), %rax
	testq	%rax, %rax
	jne	.L5
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__kernel_cosf128@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__kernel_cosf128@PLT
	pxor	.LC1(%rip), %xmm0
	jmp	.L1
	.size	__cosf128, .-__cosf128
	.weak	cosf128
	.set	cosf128,__cosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
