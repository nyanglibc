	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ccosh
	.type	__ccosh, @function
__ccosh:
	movq	.LC3(%rip), %xmm4
	movapd	%xmm0, %xmm3
	movapd	%xmm1, %xmm6
	andpd	%xmm4, %xmm3
	andpd	%xmm4, %xmm6
	ucomisd	%xmm3, %xmm3
	jp	.L2
	pushq	%rbx
	subq	$80, %rsp
	ucomisd	.LC4(%rip), %xmm3
	movsd	%xmm0, (%rsp)
	jbe	.L75
	ucomisd	%xmm6, %xmm6
	jp	.L41
	ucomisd	.LC4(%rip), %xmm6
	ja	.L41
	movsd	.LC5(%rip), %xmm5
	movapd	%xmm1, %xmm2
	ucomisd	%xmm5, %xmm6
	jnb	.L16
	ucomisd	.LC0(%rip), %xmm1
	jp	.L16
	je	.L17
.L16:
	ucomisd	%xmm5, %xmm6
	jbe	.L65
	movapd	%xmm1, %xmm0
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	call	__sincos@PLT
	movq	.LC9(%rip), %xmm1
	movsd	72(%rsp), %xmm0
	movq	.LC11(%rip), %xmm3
	andpd	%xmm1, %xmm0
	movsd	64(%rsp), %xmm2
	orpd	%xmm3, %xmm0
.L36:
	movsd	(%rsp), %xmm7
	andpd	%xmm1, %xmm2
	andpd	%xmm1, %xmm7
	orpd	%xmm3, %xmm2
	orpd	.LC12(%rip), %xmm7
	movapd	%xmm7, %xmm1
	mulsd	%xmm2, %xmm1
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movsd	.LC5(%rip), %xmm5
	ucomisd	%xmm5, %xmm3
	jnb	.L4
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L51
	jne	.L51
	ucomisd	%xmm6, %xmm6
	jp	.L8
	ucomisd	.LC4(%rip), %xmm6
	ja	.L8
.L45:
	movsd	.LC7(%rip), %xmm2
	ucomisd	%xmm5, %xmm6
	mulsd	.LC6(%rip), %xmm2
	cvttsd2si	%xmm2, %ebx
	jbe	.L62
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm5, 32(%rsp)
	movapd	%xmm1, %xmm0
	movsd	%xmm3, 48(%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__sincos@PLT
	movsd	48(%rsp), %xmm3
	movapd	16(%rsp), %xmm4
	movsd	32(%rsp), %xmm5
.L21:
	pxor	%xmm2, %xmm2
	movsd	%xmm3, 48(%rsp)
	cvtsi2sd	%ebx, %xmm2
	ucomisd	%xmm2, %xmm3
	jbe	.L63
	movapd	%xmm2, %xmm0
	movsd	%xmm5, 56(%rsp)
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm2, 16(%rsp)
	call	__ieee754_exp@PLT
	movsd	(%rsp), %xmm7
	movsd	64(%rsp), %xmm1
	movmskpd	%xmm7, %eax
	movsd	16(%rsp), %xmm2
	movapd	32(%rsp), %xmm4
	testb	$1, %al
	movsd	56(%rsp), %xmm5
	movsd	48(%rsp), %xmm3
	je	.L25
	xorpd	.LC9(%rip), %xmm1
.L25:
	subsd	%xmm2, %xmm3
	movsd	.LC10(%rip), %xmm6
	mulsd	%xmm0, %xmm6
	ucomisd	%xmm2, %xmm3
	mulsd	%xmm6, %xmm1
	mulsd	72(%rsp), %xmm6
	movsd	%xmm1, 64(%rsp)
	movsd	%xmm6, 72(%rsp)
	ja	.L76
.L64:
	movapd	%xmm3, %xmm0
	movsd	%xmm5, 48(%rsp)
	movaps	%xmm4, (%rsp)
	call	__ieee754_exp@PLT
	movapd	%xmm0, %xmm1
	movsd	72(%rsp), %xmm0
	movapd	(%rsp), %xmm4
	mulsd	%xmm1, %xmm0
	mulsd	64(%rsp), %xmm1
	movsd	48(%rsp), %xmm5
	.p2align 4,,10
	.p2align 3
.L30:
	movapd	%xmm0, %xmm2
	andpd	%xmm4, %xmm2
	ucomisd	%xmm2, %xmm5
	jbe	.L31
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
.L31:
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm5
	jbe	.L1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	%xmm6, %xmm6
	jp	.L11
	ucomisd	.LC4(%rip), %xmm6
	jbe	.L45
.L11:
	pxor	%xmm2, %xmm2
	movsd	(%rsp), %xmm7
	ucomisd	%xmm2, %xmm7
	jp	.L49
	je	.L8
.L49:
	movsd	.LC1(%rip), %xmm2
.L8:
	subsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L51:
	ucomisd	%xmm6, %xmm6
	jp	.L49
	ucomisd	.LC4(%rip), %xmm6
	ja	.L49
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L63:
	movsd	(%rsp), %xmm0
	movsd	%xmm5, 32(%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__ieee754_cosh@PLT
	movsd	72(%rsp), %xmm2
	mulsd	%xmm0, %xmm2
	movsd	(%rsp), %xmm0
	movsd	%xmm2, 48(%rsp)
	call	__ieee754_sinh@PLT
	movsd	48(%rsp), %xmm2
	movapd	%xmm0, %xmm1
	movsd	32(%rsp), %xmm5
	movapd	%xmm2, %xmm0
	mulsd	64(%rsp), %xmm1
	movapd	16(%rsp), %xmm4
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L76:
	subsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm1
	mulsd	%xmm6, %xmm0
	ucomisd	%xmm2, %xmm3
	movsd	%xmm1, 64(%rsp)
	movsd	%xmm0, 72(%rsp)
	jbe	.L64
	mulsd	.LC4(%rip), %xmm0
	mulsd	.LC4(%rip), %xmm1
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	.LC0(%rip), %xmm1
	movsd	.LC1(%rip), %xmm0
	jp	.L47
	je	.L69
.L47:
	movapd	%xmm0, %xmm1
.L69:
	rep ret
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	(%rsp), %xmm2
	movsd	.LC2(%rip), %xmm0
	andpd	.LC9(%rip), %xmm2
	orpd	.LC12(%rip), %xmm2
	mulsd	%xmm2, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L62:
	movq	.LC8(%rip), %rax
	movsd	%xmm1, 64(%rsp)
	movq	%rax, 72(%rsp)
	jmp	.L21
.L65:
	movsd	.LC2(%rip), %xmm0
	movq	.LC9(%rip), %xmm1
	movq	.LC11(%rip), %xmm3
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L41:
	movsd	.LC2(%rip), %xmm0
	subsd	%xmm1, %xmm1
	jmp	.L1
	.size	__ccosh, .-__ccosh
	.weak	ccoshf32x
	.set	ccoshf32x,__ccosh
	.weak	ccoshf64
	.set	ccoshf64,__ccosh
	.weak	ccosh
	.set	ccosh,__ccosh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	2146959360
	.align 8
.LC2:
	.long	0
	.long	2146435072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.align 8
.LC5:
	.long	0
	.long	1048576
	.align 8
.LC6:
	.long	4277811695
	.long	1072049730
	.align 8
.LC7:
	.long	0
	.long	1083176960
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC10:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	0
	.long	2146435072
	.long	0
	.long	0
	.align 16
.LC12:
	.long	0
	.long	1072693248
	.long	0
	.long	0
