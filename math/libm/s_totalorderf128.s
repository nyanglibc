	.text
	.p2align 4,,15
	.globl	__totalorderf128
	.type	__totalorderf128, @function
__totalorderf128:
	movdqa	(%rdi), %xmm0
	movdqa	(%rsi), %xmm1
	movaps	%xmm0, -40(%rsp)
	movq	-32(%rsp), %rdx
	movaps	%xmm1, -24(%rsp)
	movq	%rdx, %rdi
	sarq	$63, %rdi
	movq	-16(%rsp), %rax
	movq	%rdi, %rcx
	shrq	%rcx
	xorq	%rdx, %rcx
	movq	%rax, %rsi
	sarq	$63, %rsi
	movq	%rsi, %rdx
	shrq	%rdx
	xorq	%rax, %rdx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	jl	.L1
	movq	-40(%rsp), %rax
	xorq	%rax, %rdi
	movq	-24(%rsp), %rax
	xorq	%rax, %rsi
	cmpq	%rsi, %rdi
	setbe	%sil
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	sete	%al
	andl	%esi, %eax
.L1:
	rep ret
	.size	__totalorderf128, .-__totalorderf128
	.weak	totalorderf128
	.set	totalorderf128,__totalorderf128
