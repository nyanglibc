	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__hypot
	.type	__hypot, @function
__hypot:
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_hypot@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm2
	movsd	(%rsp), %xmm3
	andpd	%xmm1, %xmm5
	movsd	8(%rsp), %xmm4
	ucomisd	%xmm5, %xmm2
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movapd	%xmm3, %xmm5
	andpd	%xmm1, %xmm5
	ucomisd	%xmm5, %xmm2
	jb	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movapd	%xmm4, %xmm1
	movapd	%xmm3, %xmm0
	movl	$4, %edi
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.size	__hypot, .-__hypot
	.weak	hypotf32x
	.set	hypotf32x,__hypot
	.weak	hypotf64
	.set	hypotf64,__hypot
	.weak	hypot
	.set	hypot,__hypot
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
