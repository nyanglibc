	.text
	.p2align 4,,15
	.globl	__j1
	.type	__j1, @function
__j1:
	jmp	__ieee754_j1@PLT
	.size	__j1, .-__j1
	.weak	j1f32x
	.set	j1f32x,__j1
	.weak	j1f64
	.set	j1f64,__j1
	.weak	j1
	.set	j1,__j1
	.p2align 4,,15
	.globl	__y1
	.type	__y1, @function
__y1:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L11
.L4:
	jmp	__ieee754_y1@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	ucomisd	%xmm1, %xmm0
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y1, .-__y1
	.weak	y1f32x
	.set	y1f32x,__y1
	.weak	y1f64
	.set	y1f64,__y1
	.weak	y1
	.set	y1,__y1
