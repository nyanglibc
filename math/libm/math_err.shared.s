	.text
	.p2align 4,,15
	.type	with_errno, @function
with_errno:
	movq	errno@gottpoff(%rip), %rax
	movl	%edi, %fs:(%rax)
	ret
	.size	with_errno, .-with_errno
	.p2align 4,,15
	.type	xflow, @function
xflow:
	testl	%edi, %edi
	movapd	%xmm0, %xmm1
	je	.L4
	xorpd	.LC0(%rip), %xmm1
.L4:
	mulsd	%xmm1, %xmm0
	movl	$34, %edi
	jmp	with_errno
	.size	xflow, .-xflow
	.p2align 4,,15
	.globl	__math_uflow
	.hidden	__math_uflow
	.type	__math_uflow, @function
__math_uflow:
	movsd	.LC1(%rip), %xmm0
	jmp	xflow
	.size	__math_uflow, .-__math_uflow
	.p2align 4,,15
	.globl	__math_may_uflow
	.hidden	__math_may_uflow
	.type	__math_may_uflow, @function
__math_may_uflow:
	movsd	.LC2(%rip), %xmm0
	jmp	xflow
	.size	__math_may_uflow, .-__math_may_uflow
	.p2align 4,,15
	.globl	__math_oflow
	.hidden	__math_oflow
	.type	__math_oflow, @function
__math_oflow:
	movsd	.LC3(%rip), %xmm0
	jmp	xflow
	.size	__math_oflow, .-__math_oflow
	.p2align 4,,15
	.globl	__math_divzero
	.hidden	__math_divzero
	.type	__math_divzero, @function
__math_divzero:
	testl	%edi, %edi
	movsd	.LC4(%rip), %xmm0
	jne	.L10
	movsd	.LC5(%rip), %xmm0
.L10:
	divsd	.LC6(%rip), %xmm0
	movl	$34, %edi
	jmp	with_errno
	.size	__math_divzero, .-__math_divzero
	.p2align 4,,15
	.globl	__math_invalid
	.hidden	__math_invalid
	.type	__math_invalid, @function
__math_invalid:
	movapd	%xmm0, %xmm1
	subsd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm1
	divsd	%xmm0, %xmm0
	jp	.L12
	movl	$33, %edi
	jmp	with_errno
	.p2align 4,,10
	.p2align 3
.L12:
	rep ret
	.size	__math_invalid, .-__math_invalid
	.p2align 4,,15
	.globl	__math_check_uflow
	.hidden	__math_check_uflow
	.type	__math_check_uflow, @function
__math_check_uflow:
	ucomisd	.LC6(%rip), %xmm0
	jp	.L15
	je	.L18
.L15:
	rep ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$34, %edi
	jmp	with_errno
	.size	__math_check_uflow, .-__math_check_uflow
	.p2align 4,,15
	.globl	__math_check_oflow
	.hidden	__math_check_oflow
	.type	__math_check_oflow, @function
__math_check_oflow:
	movapd	%xmm0, %xmm1
	andpd	.LC7(%rip), %xmm1
	ucomisd	.LC8(%rip), %xmm1
	ja	.L21
	rep ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$34, %edi
	jmp	with_errno
	.size	__math_check_oflow, .-__math_check_oflow
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	268435456
	.align 8
.LC2:
	.long	0
	.long	509083648
	.align 8
.LC3:
	.long	0
	.long	1879048192
	.align 8
.LC4:
	.long	0
	.long	-1074790400
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.align 8
.LC6:
	.long	0
	.long	0
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC8:
	.long	4294967295
	.long	2146435071
