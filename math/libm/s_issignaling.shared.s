	.text
	.p2align 4,,15
	.globl	__GI___issignaling
	.hidden	__GI___issignaling
	.type	__GI___issignaling, @function
__GI___issignaling:
	movq	%xmm0, %rax
	movabsq	$2251799813685248, %rdx
	xorq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	movabsq	$9221120237041090560, %rdx
	cmpq	%rdx, %rax
	seta	%al
	movzbl	%al, %eax
	ret
	.size	__GI___issignaling, .-__GI___issignaling
	.globl	__issignaling
	.set	__issignaling,__GI___issignaling
