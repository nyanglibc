	.text
	.p2align 4,,15
	.globl	__catanh
	.type	__catanh, @function
__catanh:
	movq	.LC2(%rip), %xmm7
	movapd	%xmm0, %xmm6
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm9
	andpd	%xmm7, %xmm6
	ucomisd	%xmm6, %xmm6
	jp	.L48
	movsd	.LC3(%rip), %xmm0
	ucomisd	%xmm0, %xmm6
	jbe	.L92
	movl	$1, %eax
.L3:
	movapd	%xmm9, %xmm5
	andpd	%xmm7, %xmm5
	ucomisd	%xmm5, %xmm5
	jp	.L93
.L46:
	ucomisd	%xmm0, %xmm5
	ja	.L45
	movsd	.LC4(%rip), %xmm8
	ucomisd	%xmm8, %xmm5
	jnb	.L6
	ucomisd	.LC0(%rip), %xmm9
	jp	.L6
	je	.L7
.L6:
	cmpl	$1, %eax
	jle	.L9
.L10:
	subq	$88, %rsp
	movsd	.LC7(%rip), %xmm4
	ucomisd	%xmm4, %xmm6
	jnb	.L13
	ucomisd	%xmm4, %xmm5
	jnb	.L13
	movsd	.LC8(%rip), %xmm4
	movsd	.LC11(%rip), %xmm0
	ucomisd	%xmm4, %xmm6
	jp	.L21
	je	.L60
.L21:
	ucomisd	%xmm0, %xmm5
	jb	.L78
	movapd	%xmm9, %xmm0
	mulsd	%xmm9, %xmm0
.L26:
	movapd	%xmm2, %xmm3
	movapd	%xmm4, %xmm1
	movsd	%xmm9, 72(%rsp)
	addsd	%xmm4, %xmm3
	movsd	%xmm4, 64(%rsp)
	subsd	%xmm2, %xmm1
	movsd	%xmm8, 56(%rsp)
	movaps	%xmm7, 32(%rsp)
	mulsd	%xmm3, %xmm3
	movsd	%xmm5, 16(%rsp)
	mulsd	%xmm1, %xmm1
	movsd	%xmm6, 48(%rsp)
	addsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm0
	movsd	.LC9(%rip), %xmm3
	movsd	%xmm3, 8(%rsp)
	divsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm3
	jbe	.L79
	call	__ieee754_log@PLT
	movsd	.LC10(%rip), %xmm2
	movsd	48(%rsp), %xmm6
	mulsd	%xmm0, %xmm2
	movapd	32(%rsp), %xmm7
	movsd	16(%rsp), %xmm5
	movsd	56(%rsp), %xmm8
	movsd	64(%rsp), %xmm4
	movsd	72(%rsp), %xmm9
.L30:
	ucomisd	%xmm6, %xmm5
	jbe	.L31
	movapd	%xmm6, %xmm0
	movapd	%xmm5, %xmm6
	movapd	%xmm0, %xmm5
.L31:
	movsd	.LC15(%rip), %xmm0
	ucomisd	%xmm5, %xmm0
	jbe	.L80
	movapd	%xmm4, %xmm1
	pxor	%xmm3, %xmm3
	subsd	%xmm6, %xmm1
	addsd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm1
	ucomisd	%xmm3, %xmm1
	jp	.L35
	movq	%xmm1, %rdx
	movq	%xmm3, %rax
	cmovne	%rdx, %rax
	movq	%rax, 48(%rsp)
	movsd	48(%rsp), %xmm1
.L35:
	addsd	%xmm9, %xmm9
	movsd	%xmm8, 32(%rsp)
	movaps	%xmm7, 16(%rsp)
	movsd	%xmm2, 48(%rsp)
	movapd	%xmm9, %xmm0
	call	__ieee754_atan2@PLT
	movsd	8(%rsp), %xmm3
	movsd	48(%rsp), %xmm2
	mulsd	%xmm0, %xmm3
	movapd	16(%rsp), %xmm7
	movapd	%xmm2, %xmm0
	movsd	32(%rsp), %xmm8
	movapd	%xmm3, %xmm1
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L92:
	ucomisd	.LC4(%rip), %xmm6
	jnb	.L50
	ucomisd	.LC0(%rip), %xmm2
	jp	.L56
	jne	.L56
	movl	$2, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movapd	%xmm9, %xmm3
	movsd	.LC8(%rip), %xmm4
	andpd	.LC5(%rip), %xmm3
	ucomisd	%xmm5, %xmm4
	orpd	.LC6(%rip), %xmm3
	jnb	.L94
	ucomisd	%xmm6, %xmm4
	jb	.L75
	divsd	%xmm9, %xmm2
	movapd	%xmm3, %xmm1
	divsd	%xmm9, %xmm2
	movapd	%xmm2, %xmm0
.L18:
	movapd	%xmm2, %xmm4
	andpd	%xmm7, %xmm4
	ucomisd	%xmm4, %xmm8
	jbe	.L42
	mulsd	%xmm2, %xmm2
.L42:
	andpd	%xmm3, %xmm7
	ucomisd	%xmm7, %xmm8
	jbe	.L1
	mulsd	%xmm3, %xmm3
.L1:
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	.LC5(%rip), %xmm1
	andpd	%xmm1, %xmm9
	andpd	%xmm1, %xmm2
	orpd	.LC6(%rip), %xmm9
	movapd	%xmm2, %xmm0
	movapd	%xmm9, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$4, %eax
.L2:
	movapd	%xmm9, %xmm5
	andpd	%xmm7, %xmm5
	ucomisd	%xmm5, %xmm5
	jp	.L84
	movsd	.LC3(%rip), %xmm0
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L94:
	divsd	%xmm2, %xmm4
	movapd	%xmm3, %xmm1
	movapd	%xmm4, %xmm2
	movapd	%xmm4, %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$1, %eax
	jle	.L9
	cmpl	$2, %eax
	movapd	%xmm2, %xmm0
	movapd	%xmm9, %xmm1
	jne	.L10
	rep ret
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$3, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L60:
	ucomisd	%xmm5, %xmm0
	jbe	.L21
	andpd	.LC5(%rip), %xmm2
	movapd	%xmm5, %xmm0
	movsd	%xmm9, 72(%rsp)
	movsd	%xmm4, 64(%rsp)
	movsd	%xmm8, 56(%rsp)
	movaps	%xmm7, 32(%rsp)
	movsd	%xmm6, 16(%rsp)
	movsd	%xmm5, 48(%rsp)
	orpd	.LC12(%rip), %xmm2
	movsd	%xmm2, 8(%rsp)
	call	__ieee754_log@PLT
	movsd	.LC13(%rip), %xmm2
	movsd	48(%rsp), %xmm5
	movsd	64(%rsp), %xmm4
	subsd	%xmm0, %xmm2
	movsd	16(%rsp), %xmm6
	ucomisd	%xmm4, %xmm5
	movapd	32(%rsp), %xmm7
	mulsd	8(%rsp), %xmm2
	movsd	56(%rsp), %xmm8
	movsd	72(%rsp), %xmm9
	jbe	.L77
	movapd	%xmm6, %xmm0
	movapd	%xmm5, %xmm6
	movapd	%xmm0, %xmm5
.L88:
	movsd	.LC9(%rip), %xmm3
	movsd	%xmm3, 8(%rsp)
.L82:
	movapd	%xmm4, %xmm1
	mulsd	%xmm5, %xmm5
	subsd	%xmm6, %xmm1
	addsd	%xmm4, %xmm6
	mulsd	%xmm6, %xmm1
	subsd	%xmm5, %xmm1
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L75:
	movsd	.LC9(%rip), %xmm1
	movapd	%xmm2, %xmm0
	movsd	%xmm8, 32(%rsp)
	movaps	%xmm7, 16(%rsp)
	mulsd	%xmm1, %xmm0
	mulsd	%xmm9, %xmm1
	movsd	%xmm3, 48(%rsp)
	movsd	%xmm2, 8(%rsp)
	call	__ieee754_hypot@PLT
	movsd	8(%rsp), %xmm2
	movsd	48(%rsp), %xmm3
	divsd	%xmm0, %xmm2
	movapd	16(%rsp), %xmm7
	movapd	%xmm3, %xmm1
	movsd	32(%rsp), %xmm8
	divsd	%xmm0, %xmm2
	mulsd	.LC10(%rip), %xmm2
	movapd	%xmm2, %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%eax, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L78:
	pxor	%xmm0, %xmm0
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L79:
	mulsd	.LC14(%rip), %xmm2
	divsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	call	__log1p@PLT
	movsd	.LC10(%rip), %xmm2
	movsd	72(%rsp), %xmm9
	mulsd	%xmm0, %xmm2
	movapd	32(%rsp), %xmm7
	movsd	64(%rsp), %xmm4
	movsd	56(%rsp), %xmm8
	movsd	16(%rsp), %xmm5
	movsd	48(%rsp), %xmm6
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L80:
	ucomisd	%xmm4, %xmm6
	jnb	.L82
	ucomisd	.LC16(%rip), %xmm6
	jnb	.L39
	ucomisd	8(%rsp), %xmm5
	jb	.L82
.L39:
	movapd	%xmm5, %xmm1
	movapd	%xmm6, %xmm0
	movsd	%xmm9, 56(%rsp)
	movsd	%xmm8, 32(%rsp)
	movaps	%xmm7, 16(%rsp)
	movsd	%xmm2, 48(%rsp)
	call	__x2y2m1@PLT
	movapd	%xmm0, %xmm1
	movsd	48(%rsp), %xmm2
	xorpd	.LC5(%rip), %xmm1
	movapd	16(%rsp), %xmm7
	movsd	32(%rsp), %xmm8
	movsd	56(%rsp), %xmm9
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%eax, %eax
	jne	.L45
.L84:
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movsd	.LC15(%rip), %xmm0
	ucomisd	%xmm5, %xmm0
	jbe	.L88
	movsd	.LC9(%rip), %xmm5
	pxor	%xmm1, %xmm1
	movsd	%xmm5, 8(%rsp)
	jmp	.L35
.L93:
	andpd	.LC5(%rip), %xmm2
	movsd	.LC1(%rip), %xmm1
	movapd	%xmm2, %xmm0
	ret
	.size	__catanh, .-__catanh
	.weak	catanhf32x
	.set	catanhf32x,__catanh
	.weak	catanhf64
	.set	catanhf64,__catanh
	.weak	catanh
	.set	catanh,__catanh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.align 8
.LC4:
	.long	0
	.long	1048576
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC6:
	.long	1413754136
	.long	1073291771
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1131413504
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.align 8
.LC10:
	.long	0
	.long	1070596096
	.align 8
.LC11:
	.long	0
	.long	963641344
	.section	.rodata.cst16
	.align 16
.LC12:
	.long	0
	.long	1071644672
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC13:
	.long	4277811695
	.long	1072049730
	.align 8
.LC14:
	.long	0
	.long	1074790400
	.align 8
.LC15:
	.long	0
	.long	1017118720
	.align 8
.LC16:
	.long	0
	.long	1072168960
