	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__atanh
	.type	__atanh, @function
__atanh:
	movapd	%xmm0, %xmm2
	movsd	.LC1(%rip), %xmm1
	andpd	.LC0(%rip), %xmm2
	ucomisd	%xmm1, %xmm2
	jnb	.L6
.L2:
	jmp	__ieee754_atanh@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	xorl	%edi, %edi
	ucomisd	%xmm1, %xmm2
	movapd	%xmm0, %xmm1
	setbe	%dil
	addl	$30, %edi
	jmp	__kernel_standard@PLT
	.size	__atanh, .-__atanh
	.weak	atanhf32x
	.set	atanhf32x,__atanh
	.weak	atanhf64
	.set	atanhf64,__atanh
	.weak	atanh
	.set	atanh,__atanh
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
