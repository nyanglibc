	.text
#APP
	.symver __totalordermagf0,totalordermagf@@GLIBC_2.31
	.symver __totalordermagf1,totalordermagf32@@GLIBC_2.31
	.symver __totalordermag_compatf2,totalordermagf@GLIBC_2.25
	.symver __totalordermag_compatf3,totalordermagf32@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__totalordermagf
	.type	__totalordermagf, @function
__totalordermagf:
	movl	(%rdi), %edx
	movl	(%rsi), %eax
	andl	$2147483647, %edx
	andl	$2147483647, %eax
	cmpl	%eax, %edx
	setbe	%al
	movzbl	%al, %eax
	ret
	.size	__totalordermagf, .-__totalordermagf
	.globl	__totalordermagf1
	.set	__totalordermagf1,__totalordermagf
	.globl	__totalordermagf0
	.set	__totalordermagf0,__totalordermagf
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalordermag_compatf
	.type	__totalordermag_compatf, @function
__totalordermag_compatf:
	subq	$24, %rsp
	leaq	8(%rsp), %rsi
	leaq	12(%rsp), %rdi
	movss	%xmm0, 12(%rsp)
	movss	%xmm1, 8(%rsp)
	call	__totalordermagf@PLT
	addq	$24, %rsp
	ret
	.size	__totalordermag_compatf, .-__totalordermag_compatf
	.globl	__totalordermag_compatf3
	.set	__totalordermag_compatf3,__totalordermag_compatf
	.globl	__totalordermag_compatf2
	.set	__totalordermag_compatf2,__totalordermag_compatf
