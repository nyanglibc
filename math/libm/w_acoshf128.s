	.text
	.globl	__unordtf2
	.globl	__getf2
	.p2align 4,,15
	.globl	__acoshf128
	.type	__acoshf128, @function
__acoshf128:
	subq	$24, %rsp
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L5
.L2:
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	jmp	__ieee754_acoshf128@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rax
	movdqa	(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	addq	$24, %rsp
	jmp	__ieee754_acoshf128@PLT
	.size	__acoshf128, .-__acoshf128
	.weak	acoshf128
	.set	acoshf128,__acoshf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
