	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fmodl
	.type	__fmodl, @function
__fmodl:
	fldt	24(%rsp)
	fldt	8(%rsp)
	xorl	%ecx, %ecx
	movl	$0, %edx
	fxam
	fnstsw	%ax
	andb	$69, %ah
	fldz
	cmpb	$5, %ah
	sete	%cl
	fucomip	%st(2), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L4
	testl	%ecx, %ecx
	jne	.L4
	fxch	%st(1)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L15:
	fxch	%st(1)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L16:
	fxch	%st(1)
.L2:
	fstpt	24(%rsp)
	fstpt	8(%rsp)
	jmp	__ieee754_fmodl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L15
	fucomi	%st(1), %st
	jp	.L16
	fxch	%st(1)
	fstpt	24(%rsp)
	movl	$227, %edi
	fstpt	8(%rsp)
	jmp	__kernel_standard_l@PLT
	.size	__fmodl, .-__fmodl
	.weak	fmodf64x
	.set	fmodf64x,__fmodl
	.weak	fmodl
	.set	fmodl,__fmodl
