	.text
	.globl	__addtf3
	.globl	__lttf2
	.globl	__multf3
	.globl	__gttf2
	.globl	__divtf3
	.globl	__subtf3
	.globl	__letf2
	.p2align 4,,15
	.globl	__ieee754_sinhf128
	.type	__ieee754_sinhf128, @function
__ieee754_sinhf128:
	pushq	%rbx
	subq	$64, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	shrq	$32, %rax
	movdqa	(%rsp), %xmm3
	movl	%eax, %ebx
	andl	$2147483647, %ebx
	cmpl	$2147418111, %ebx
	movaps	%xmm3, 16(%rsp)
	ja	.L23
	testl	%eax, %eax
	js	.L15
	movdqa	.LC0(%rip), %xmm4
	movaps	%xmm4, 32(%rsp)
.L4:
	movq	8(%rsp), %rax
	movq	%rbx, %rdx
	salq	$32, %rdx
	movl	%eax, %eax
	orq	%rdx, %rax
	cmpl	$1074020352, %ebx
	movq	%rax, 24(%rsp)
	ja	.L5
	cmpl	$1069940735, %ebx
	ja	.L6
	movdqa	(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC3(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L7
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L7:
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	jg	.L1
	movdqa	16(%rsp), %xmm0
	call	__expm1f128@PLT
	movdqa	%xmm0, %xmm2
.L14:
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	48(%rsp), %xmm2
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
.L1:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1074553571, %ebx
	jbe	.L24
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L25
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movdqa	.LC1(%rip), %xmm5
	movaps	%xmm5, 32(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	movdqa	16(%rsp), %xmm0
	call	__expm1f128@PLT
	cmpl	$1073676287, %ebx
	movdqa	%xmm0, %xmm2
	jbe	.L14
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	.LC0(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	call	__ieee754_expf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	jmp	.L1
	.size	__ieee754_sinhf128, .-__ieee754_sinhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-1073872896
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC4:
	.long	1339676151
	.long	4048017862
	.long	2218929559
	.long	2147178567
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC6:
	.long	3058541734
	.long	2178113829
	.long	3145753437
	.long	1074553577
