	.text
	.p2align 4,,15
	.globl	__getpayloadf128
	.type	__getpayloadf128, @function
__getpayloadf128:
	movdqa	(%rdi), %xmm1
	movabsq	$9223090561878065152, %rax
	movaps	%xmm1, -24(%rsp)
	movq	-16(%rsp), %rdx
	movq	%rdx, %rcx
	andq	%rax, %rcx
	cmpq	%rax, %rcx
	je	.L11
.L8:
	movdqa	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-24(%rsp), %rax
	movabsq	$281474976710655, %rcx
	andq	%rdx, %rcx
	orq	%rax, %rcx
	je	.L8
	movabsq	$140737488355327, %rcx
	andq	%rcx, %rdx
	je	.L12
	bsrq	%rdx, %rsi
	xorl	$63, %esi
.L4:
	leal	-15(%rsi), %edi
	cmpl	$63, %edi
	jle	.L5
	leal	-79(%rsi), %ecx
	salq	%cl, %rax
	movq	%rax, %rdx
	xorl	%eax, %eax
.L6:
	movabsq	$281474976710655, %rcx
	movq	%rax, -24(%rsp)
	andq	%rcx, %rdx
	movl	$16510, %ecx
	subq	%rsi, %rcx
	salq	$48, %rcx
	orq	%rdx, %rcx
	movq	%rcx, -16(%rsp)
	movdqa	-24(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	testq	%rax, %rax
	je	.L9
	bsrq	%rax, %rsi
	xorq	$63, %rsi
	addl	$64, %esi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%edi, %ecx
	movq	%rax, %r8
	salq	%cl, %rdx
	movl	$64, %ecx
	subl	%edi, %ecx
	shrq	%cl, %r8
	movl	%edi, %ecx
	orq	%r8, %rdx
	salq	%cl, %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	pxor	%xmm0, %xmm0
	ret
	.size	__getpayloadf128, .-__getpayloadf128
	.weak	getpayloadf128
	.set	getpayloadf128,__getpayloadf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
