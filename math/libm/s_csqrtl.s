	.text
	.p2align 4,,15
	.globl	__csqrtl
	.type	__csqrtl, @function
__csqrtl:
	pushq	%rbx
	subq	$48, %rsp
	fldt	64(%rsp)
	fldt	80(%rsp)
	fld	%st(1)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L2
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L129
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L130
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L131
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L144:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L149:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L153:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L8:
	flds	.LC4(%rip)
	.p2align 4,,10
	.p2align 3
.L125:
.L1:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	fld	%st(4)
	fld	%st(4)
	fldt	.LC3(%rip)
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jnb	.L4
	fldz
	fxch	%st(7)
	pxor	%xmm0, %xmm0
	fucomi	%st(7), %st
	movss	%xmm0, (%rsp)
	jp	.L142
	je	.L5
	fstp	%st(4)
	fstp	%st(6)
	fxch	%st(2)
	fxch	%st(4)
	fxch	%st(5)
	fxch	%st(1)
	fxch	%st(3)
	jmp	.L7
.L142:
	fstp	%st(4)
	fstp	%st(6)
	fxch	%st(2)
	fxch	%st(4)
	fxch	%st(5)
	fxch	%st(1)
	fxch	%st(3)
	.p2align 4,,10
	.p2align 3
.L7:
	fucomi	%st(0), %st
	jp	.L143
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L144
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L145
	fldz
	fxch	%st(6)
	fucomi	%st(6), %st
	fstp	%st(6)
	jp	.L146
	jne	.L147
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L156:
	fstp	%st(1)
.L61:
	fldz
	fucomip	%st(1), %st
	ja	.L132
	fsqrt
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	jne	.L133
.L21:
	fxch	%st(1)
	fabs
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	fxch	%st(4)
	fucomi	%st(0), %st
	jp	.L148
	fucomi	%st(3), %st
	fstp	%st(3)
	ja	.L149
	fldt	.LC3(%rip)
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jb	.L120
	pxor	%xmm2, %xmm2
	movss	%xmm2, (%rsp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L145:
	fxch	%st(2)
	fxch	%st(1)
	fxch	%st(3)
	fxch	%st(1)
	fxch	%st(4)
	fxch	%st(5)
	fxch	%st(4)
	jmp	.L54
.L146:
	fxch	%st(2)
	fxch	%st(1)
	fxch	%st(3)
	fxch	%st(1)
	fxch	%st(4)
	fxch	%st(5)
	fxch	%st(4)
	jmp	.L54
.L147:
	fxch	%st(2)
	fxch	%st(1)
	fxch	%st(3)
	fxch	%st(1)
	fxch	%st(4)
	fxch	%st(5)
	fxch	%st(4)
	.p2align 4,,10
	.p2align 3
.L54:
	fldt	.LC9(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	ja	.L134
	fxch	%st(3)
	fucomi	%st(2), %st
	fstp	%st(2)
	jbe	.L111
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	fldt	.LC10(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L135
	fstp	%st(1)
	fldz
	fstpt	16(%rsp)
.L33:
	subq	$16, %rsp
	movl	$-2, %edi
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fldt	32(%rsp)
	fxch	%st(1)
	popq	%rdi
	popq	%r8
.L28:
	subq	$32, %rsp
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	64(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	__ieee754_hypotl@PLT
	fldt	64(%rsp)
	addq	$32, %rsp
	fld	%st(0)
	fabs
	fldz
	fldt	16(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L136
	faddp	%st, %st(3)
	flds	.LC8(%rip)
	fmul	%st, %st(3)
	fxch	%st(3)
	fsqrt
	fld1
	fld	%st(3)
	fxch	%st(1)
	fucomip	%st(3), %st
	fstp	%st(2)
	ja	.L137
	fdivr	%st, %st(1)
	fxch	%st(1)
	movl	$1, %ebx
	fmulp	%st, %st(3)
	fxch	%st(1)
.L40:
	fstpt	32(%rsp)
	fxch	%st(1)
	subq	$16, %rsp
	movl	%ebx, %edi
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fstpt	16(%rsp)
	subq	$16, %rsp
	movl	%ebx, %edi
	fldt	48(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	addq	$32, %rsp
	fldt	(%rsp)
	fldt	32(%rsp)
	fxch	%st(2)
	fxch	%st(1)
.L39:
	fldt	.LC3(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L41
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L41:
	fld	%st(1)
	fabs
	fldt	.LC3(%rip)
	fucomip	%st(1), %st
	jbe	.L150
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L150:
	fstp	%st(2)
	fxch	%st(1)
	fxch	%st(2)
.L43:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L125
	fxch	%st(1)
	fchs
	fxch	%st(1)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L131:
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L138
	fldz
	pxor	%xmm4, %xmm4
	fucomip	%st(2), %st
	movss	%xmm4, (%rsp)
	ja	.L151
.L13:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L25
	fstp	%st(0)
	fldz
	fchs
.L25:
	addq	$48, %rsp
	popq	%rbx
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(2)
	fucomi	%st(0), %st
	jp	.L152
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L153
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L154
	fxch	%st(2)
	fucomi	%st(3), %st
	fstp	%st(3)
	jp	.L155
	je	.L156
	fstp	%st(0)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L154:
	fstp	%st(3)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L57
.L155:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L57:
	fldt	.LC7(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L109
	fmuls	.LC8(%rip)
	fsqrt
	fxch	%st(1)
.L24:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fld	%st(0)
	testb	$2, %ah
	fabs
	je	.L25
	fchs
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L138:
	fldz
	fxch	%st(1)
	pxor	%xmm1, %xmm1
	fucomi	%st(1), %st
	movss	%xmm1, (%rsp)
	jp	.L157
	je	.L158
	fstp	%st(1)
	jmp	.L10
.L157:
	fstp	%st(1)
.L10:
	fldz
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L158:
	fxch	%st(1)
.L127:
	fucomip	%st(2), %st
	jbe	.L13
	fstp	%st(1)
	jmp	.L66
.L151:
	fstp	%st(1)
.L66:
	fldz
	fxch	%st(1)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L111:
	fldt	.LC7(%rip)
	fucomi	%st(3), %st
	fstp	%st(3)
	jbe	.L159
	fxch	%st(2)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L139
	fstp	%st(2)
	fstp	%st(2)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L159:
	fstp	%st(4)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(2)
.L114:
	fxch	%st(1)
	xorl	%ebx, %ebx
.L34:
	subq	$32, %rsp
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	64(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	__ieee754_hypotl@PLT
	fldz
	addq	$32, %rsp
	fldt	16(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	fldt	32(%rsp)
	jbe	.L140
	fxch	%st(2)
	faddp	%st, %st(1)
	flds	.LC8(%rip)
	fmul	%st, %st(1)
	fxch	%st(1)
	fsqrt
	fld	%st(2)
	fdiv	%st(1), %st
	fmulp	%st, %st(2)
.L50:
	testl	%ebx, %ebx
	je	.L39
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L134:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstpt	32(%rsp)
	subq	$16, %rsp
	movl	$-2, %edi
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fstpt	32(%rsp)
	subq	$16, %rsp
	movl	$-2, %edi
	fldt	64(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fldt	48(%rsp)
	fxch	%st(1)
	addq	$32, %rsp
	jmp	.L28
.L140:
	fxch	%st(2)
	fsubp	%st, %st(1)
	flds	.LC8(%rip)
	fmul	%st, %st(1)
	fxch	%st(1)
	fsqrt
	fld	%st(2)
	fdiv	%st(1), %st
	fmulp	%st, %st(2)
	fxch	%st(1)
	fabs
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fstp	%st(2)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L160
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L8
	fstp	%st(0)
	jmp	.L124
.L143:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L148:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L124
.L152:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L124
.L160:
	fstp	%st(0)
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L124:
	flds	.LC0(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L136:
	fsubrp	%st, %st(3)
	flds	.LC8(%rip)
	fmul	%st, %st(3)
	fxch	%st(3)
	fsqrt
	fld1
	fucomip	%st(2), %st
	fstp	%st(1)
	ja	.L141
	fld	%st(1)
	movl	$1, %ebx
	fdiv	%st(1), %st
	fmulp	%st, %st(3)
	fxch	%st(2)
	fabs
	fxch	%st(1)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L135:
	fstpt	32(%rsp)
	subq	$16, %rsp
	movl	$-2, %edi
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fstpt	32(%rsp)
	popq	%r9
	popq	%r10
	fldt	32(%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L120:
	fldz
	fxch	%st(5)
	pxor	%xmm3, %xmm3
	fucomi	%st(5), %st
	fstp	%st(5)
	movss	%xmm3, (%rsp)
	jp	.L54
	jne	.L54
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L139:
	fstp	%st(0)
	fstp	%st(0)
	fstpt	32(%rsp)
	subq	$16, %rsp
	movl	$64, %edi
	movl	$-32, %ebx
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fstpt	32(%rsp)
	subq	$16, %rsp
	movl	$64, %edi
	fldt	64(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	addq	$32, %rsp
	fldt	16(%rsp)
	fxch	%st(1)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L133:
	fstp	%st(0)
	fldz
	fchs
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L109:
	fadd	%st(0), %st
	fsqrt
	fmuls	.LC8(%rip)
	fxch	%st(1)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L137:
	fstp	%st(3)
	fxch	%st(1)
	fstpt	16(%rsp)
	subq	$16, %rsp
	movl	$1, %edi
	fdiv	%st(1), %st
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	popq	%rcx
	popq	%rsi
	fldt	(%rsp)
	fldt	16(%rsp)
	fxch	%st(2)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L132:
	fchs
	fsqrt
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fabs
	testb	$2, %ah
	je	.L20
	fchs
.L20:
	fldz
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L130:
	fstp	%st(0)
	fstp	%st(0)
	fldz
	fucomip	%st(2), %st
	ja	.L69
	fstp	%st(0)
	flds	.LC0(%rip)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L141:
	fstp	%st(2)
	fxch	%st(1)
	fld	%st(1)
	subq	$16, %rsp
	movl	$1, %edi
	fstpt	32(%rsp)
	fdivr	%st, %st(1)
	fxch	%st(1)
	fabs
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	popq	%rax
	popq	%rdx
	fldt	(%rsp)
	fldt	16(%rsp)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L39
.L69:
	fstp	%st(1)
	flds	.LC0(%rip)
	fxch	%st(1)
.L12:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	flds	.LC4(%rip)
	je	.L25
	fstp	%st(0)
	flds	.LC5(%rip)
	jmp	.L25
	.size	__csqrtl, .-__csqrtl
	.weak	csqrtf64x
	.set	csqrtf64x,__csqrtl
	.weak	csqrtl
	.set	csqrtl,__csqrtl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC3:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	2139095040
	.align 4
.LC5:
	.long	4286578688
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	0
	.long	2147483648
	.long	2
	.long	0
	.section	.rodata.cst4
	.align 4
.LC8:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	4294967295
	.long	4294967295
	.long	32764
	.long	0
	.align 16
.LC10:
	.long	0
	.long	2147483648
	.long	3
	.long	0
