	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__netf2
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__letf2
	.globl	__lttf2
	.globl	__multf3
	.globl	__divtf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__ieee754_j0f128
	.type	__ieee754_j0f128, @function
__ieee754_j0f128:
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm4
	subq	$168, %rsp
	pand	.LC53(%rip), %xmm4
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC54(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L100
	movdqa	.LC54(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L113
.L100:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	pxor	%xmm0, %xmm0
	je	.L1
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
.L1:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	pxor	%xmm1, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L95
	movdqa	.LC55(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L115
	movdqa	.LC56(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L95
	movdqa	16(%rsp), %xmm0
	leaq	80+J0_2N(%rip), %rbx
	movdqa	%xmm0, %xmm1
	leaq	-96(%rbx), %rbp
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC0(%rip), %xmm7
	movdqa	.LC1(%rip), %xmm0
	movaps	%xmm7, 16(%rsp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L127:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L9:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L127
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	leaq	80+J0_2D(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC57(%rip), %xmm1
	leaq	-96(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm3
	movaps	%xmm3, 16(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L128:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L11:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L128
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC58(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC52(%rip), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L95:
	movdqa	.LC52(%rip), %xmm0
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	128(%rsp), %rsi
	leaq	144(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	144(%rsp), %xmm2
	movdqa	128(%rsp), %xmm3
	movdqa	%xmm2, %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm3, 32(%rsp)
	movaps	%xmm2, 16(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 80(%rsp)
	movdqa	32(%rsp), %xmm3
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	.LC59(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L129
.L12:
	movdqa	.LC61(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L130
	movdqa	(%rsp), %xmm1
	movdqa	.LC52(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC58(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L118
	movdqa	.LC63(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L119
	movdqa	.LC64(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L120
	movdqa	.LC3(%rip), %xmm7
	leaq	128+P16_IN(%rip), %rbx
	movaps	%xmm7, 96(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC4(%rip), %xmm7
	movaps	%xmm7, 64(%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L131:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L25:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L131
	leaq	128+P16_ID(%rip), %rbx
	movdqa	.LC65(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 96(%rsp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L132:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L27:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L132
	movdqa	64(%rsp), %xmm0
	leaq	144+Q16_IN(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC6(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 64(%rsp)
	movdqa	.LC7(%rip), %xmm2
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L133:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L29:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L133
	movaps	%xmm0, 64(%rsp)
	leaq	128+Q16_ID(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC66(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 96(%rsp)
	movdqa	64(%rsp), %xmm2
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L134:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L31:
	subq	$16, %rbx
	movaps	%xmm2, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm2
	jne	.L134
	.p2align 4,,10
	.p2align 3
.L91:
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 96(%rsp)
.L32:
	movdqa	16(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC52(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC63(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC62(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L118:
	movdqa	.LC74(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L122
	movdqa	.LC75(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L123
	movdqa	.LC27(%rip), %xmm7
	leaq	128+P3r2_4N(%rip), %rbx
	movdqa	.LC28(%rip), %xmm3
	leaq	-144(%rbx), %rbp
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L135:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L62:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L135
	leaq	128+P3r2_4D(%rip), %rbx
	movdqa	.LC76(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC29(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L136:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L64:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L136
	movdqa	64(%rsp), %xmm0
	leaq	144+Q3r2_4N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC30(%rip), %xmm6
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 64(%rsp)
	movdqa	.LC31(%rip), %xmm2
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L137:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L66:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L137
	movaps	%xmm0, 64(%rsp)
	leaq	128+Q3r2_4D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC77(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC32(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 96(%rsp)
	movdqa	64(%rsp), %xmm2
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L138:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L68:
	subq	$16, %rbx
	movaps	%xmm2, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm2
	jne	.L138
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L129:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__cosf128@PLT
	pxor	.LC60(%rip), %xmm0
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	144(%rsp), %xmm0
	call	__multf3@PLT
	pxor	%xmm1, %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L139
	movdqa	48(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 80(%rsp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L130:
	movdqa	(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC62(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L119:
	movdqa	.LC69(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L121
	movdqa	.LC15(%rip), %xmm5
	leaq	144+P5_8N(%rip), %rbx
	movaps	%xmm5, 64(%rsp)
	leaq	-160(%rbx), %rbp
	movdqa	.LC16(%rip), %xmm2
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L140:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L43:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L140
	movaps	%xmm0, 64(%rsp)
	leaq	128+P5_8D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC70(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC17(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 96(%rsp)
	movdqa	64(%rsp), %xmm2
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L141:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L45:
	subq	$16, %rbx
	movaps	%xmm2, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm2
	jne	.L141
	movdqa	%xmm2, %xmm0
	leaq	144+Q5_8N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC18(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movdqa	.LC19(%rip), %xmm3
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L142:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L47:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L142
	leaq	144+Q5_8D(%rip), %rbx
	movdqa	.LC71(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC20(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L143:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L49:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L143
.L74:
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 96(%rsp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L122:
	movdqa	.LC80(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L124
	movdqa	.LC39(%rip), %xmm5
	leaq	128+P2r3_2r7N(%rip), %rbx
	movaps	%xmm5, 64(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC40(%rip), %xmm2
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L144:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L79:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L144
	movaps	%xmm0, 96(%rsp)
	leaq	112+P2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC81(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC41(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L145:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L81:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L145
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r3_2r7N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC42(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 64(%rsp)
	movdqa	.LC43(%rip), %xmm2
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L146:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L83:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L146
	movaps	%xmm0, 96(%rsp)
	leaq	112+Q2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC82(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC44(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L147:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L85:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L147
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L139:
	movdqa	80(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 48(%rsp)
	jmp	.L12
.L124:
	movdqa	.LC45(%rip), %xmm5
	leaq	112+P2_2r3N(%rip), %rbx
	movdqa	.LC46(%rip), %xmm6
	leaq	-128(%rbx), %rbp
	movaps	%xmm5, 96(%rsp)
	movaps	%xmm6, 64(%rsp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L148:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L76:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L148
	leaq	112+P2_2r3D(%rip), %rbx
	movdqa	.LC83(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC47(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 96(%rsp)
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L149:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L88:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L149
	movdqa	64(%rsp), %xmm0
	leaq	128+Q2_2r3N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC48(%rip), %xmm3
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm3, 64(%rsp)
	movdqa	.LC49(%rip), %xmm2
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L150:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L90:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L150
	movaps	%xmm0, 96(%rsp)
	leaq	112+Q2_2r3D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC84(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC50(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L151:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L92:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L151
	jmp	.L91
.L120:
	movdqa	.LC9(%rip), %xmm7
	leaq	144+P8_16N(%rip), %rbx
	movdqa	.LC10(%rip), %xmm3
	leaq	-160(%rbx), %rbp
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L152:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L22:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L152
	leaq	144+P8_16D(%rip), %rbx
	movdqa	.LC67(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC11(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L153:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L35:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L153
	movdqa	64(%rsp), %xmm0
	leaq	160+Q8_16N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC12(%rip), %xmm6
	leaq	-176(%rbx), %rbp
	movdqa	.LC13(%rip), %xmm7
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L154:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L37:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L154
	movdqa	.LC68(%rip), %xmm1
	leaq	160+Q8_16D(%rip), %rbx
	movdqa	16(%rsp), %xmm0
	leaq	-176(%rbx), %rbp
	call	__addtf3@PLT
	movdqa	.LC14(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 96(%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L155:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L39:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L155
	jmp	.L74
.L121:
	movdqa	.LC21(%rip), %xmm6
	leaq	128+P4_5N(%rip), %rbx
	movdqa	.LC22(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L156:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L40:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L156
	leaq	128+P4_5D(%rip), %rbx
	movdqa	.LC72(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC23(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 96(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L157:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L52:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L157
	movdqa	64(%rsp), %xmm0
	leaq	144+Q4_5N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC24(%rip), %xmm5
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm5, 64(%rsp)
	movdqa	.LC25(%rip), %xmm2
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L158:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L54:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L158
	movaps	%xmm0, 64(%rsp)
	leaq	128+Q4_5D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC73(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC26(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 96(%rsp)
	movdqa	64(%rsp), %xmm2
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L159:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L56:
	subq	$16, %rbx
	movaps	%xmm2, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm2
	jne	.L159
	jmp	.L91
.L123:
	movdqa	.LC33(%rip), %xmm3
	leaq	128+P2r7_3r2N(%rip), %rbx
	movaps	%xmm3, 64(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC34(%rip), %xmm2
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L160:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L59:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L160
	movaps	%xmm0, 96(%rsp)
	leaq	112+P2r7_3r2D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC78(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC35(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L161:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L71:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L161
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r7_3r2N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC36(%rip), %xmm6
	leaq	-144(%rbx), %rbp
	movdqa	.LC37(%rip), %xmm7
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L162:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L73:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L162
	leaq	128+Q2r7_3r2D(%rip), %rbx
	movdqa	.LC79(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC38(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 96(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L163:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L75:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L163
	jmp	.L74
	.size	__ieee754_j0f128, .-__ieee754_j0f128
	.p2align 4,,15
	.globl	__ieee754_y0f128
	.type	__ieee754_y0f128, @function
__ieee754_y0f128:
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm4
	subq	$168, %rsp
	pand	.LC53(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movdqa	.LC54(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L265
	movdqa	.LC54(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L265
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L295
	movdqa	.LC56(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L296
	movdqa	.LC55(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L284
	movdqa	(%rsp), %xmm0
	leaq	96+Y0_2N(%rip), %rbx
	movdqa	%xmm0, %xmm1
	leaq	-112(%rbx), %rbp
	call	__multf3@PLT
	movdqa	.LC85(%rip), %xmm3
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC86(%rip), %xmm3
	movaps	%xmm3, 16(%rsp)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L297:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 48(%rsp)
.L177:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 16(%rsp)
	jne	.L297
	leaq	96+Y0_2D(%rip), %rbx
	movdqa	.LC91(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC87(%rip), %xmm3
	movaps	%xmm3, 48(%rsp)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L298:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 48(%rsp)
.L179:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L298
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_j0f128
	movdqa	.LC89(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L265:
	movdqa	(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC52(%rip), %xmm0
	call	__divtf3@PLT
.L164:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	pxor	%xmm1, %xmm1
	js	.L299
	movdqa	.LC88(%rip), %xmm0
	call	__divtf3@PLT
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	leaq	128(%rsp), %rsi
	leaq	144(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	144(%rsp), %xmm2
	movdqa	128(%rsp), %xmm3
	movdqa	%xmm2, %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm3, 64(%rsp)
	movaps	%xmm2, 32(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm3
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	.LC59(%rip), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L300
.L180:
	movdqa	.LC61(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L301
	movdqa	16(%rsp), %xmm1
	movdqa	.LC52(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC58(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L287
	movdqa	.LC63(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L288
	movdqa	.LC64(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L289
	movdqa	.LC3(%rip), %xmm6
	leaq	128+P16_IN(%rip), %rbx
	movdqa	.LC4(%rip), %xmm3
	leaq	-144(%rbx), %rbp
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L302:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L193:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L302
	leaq	128+P16_ID(%rip), %rbx
	movdqa	.LC65(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm2
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 96(%rsp)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L303:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L195:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L303
	movdqa	64(%rsp), %xmm0
	leaq	144+Q16_IN(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC6(%rip), %xmm6
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 64(%rsp)
	movdqa	.LC7(%rip), %xmm2
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L304:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L197:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L304
	movaps	%xmm0, 96(%rsp)
	leaq	128+Q16_ID(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC66(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L305:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L199:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L305
.L259:
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 96(%rsp)
.L200:
	movdqa	16(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC63(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC52(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC62(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L296:
	movdqa	(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	.LC89(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC90(%rip), %xmm1
	call	__subtf3@PLT
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L299:
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	call	__divtf3@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L287:
	movdqa	.LC74(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L291
	movdqa	.LC75(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L292
	movdqa	.LC27(%rip), %xmm7
	leaq	128+P3r2_4N(%rip), %rbx
	movdqa	.LC28(%rip), %xmm3
	leaq	-144(%rbx), %rbp
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L306:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L230:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L306
	leaq	128+P3r2_4D(%rip), %rbx
	movdqa	.LC76(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC29(%rip), %xmm2
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 96(%rsp)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L307:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L232:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L307
	movdqa	64(%rsp), %xmm0
	leaq	144+Q3r2_4N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC30(%rip), %xmm6
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 64(%rsp)
	movdqa	.LC31(%rip), %xmm2
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L308:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L234:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L308
	movaps	%xmm0, 96(%rsp)
	leaq	128+Q3r2_4D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC77(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC32(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L309:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L236:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L309
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L300:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__cosf128@PLT
	pxor	.LC60(%rip), %xmm0
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	144(%rsp), %xmm0
	call	__multf3@PLT
	pxor	%xmm1, %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L310
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 48(%rsp)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L301:
	movdqa	(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC62(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L288:
	movdqa	.LC69(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L290
	movdqa	.LC15(%rip), %xmm2
	leaq	144+P5_8N(%rip), %rbx
	movaps	%xmm2, 64(%rsp)
	leaq	-160(%rbx), %rbp
	movdqa	.LC16(%rip), %xmm2
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L311:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L211:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L311
	movaps	%xmm0, 96(%rsp)
	leaq	128+P5_8D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC70(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC17(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L312:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L213:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L312
	movdqa	%xmm2, %xmm0
	leaq	144+Q5_8N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC18(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movdqa	.LC19(%rip), %xmm3
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L215
	.p2align 4,,10
	.p2align 3
.L313:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L215:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L313
	leaq	144+Q5_8D(%rip), %rbx
	movdqa	.LC71(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC20(%rip), %xmm2
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 96(%rsp)
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L314:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L217:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L314
.L242:
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 96(%rsp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L291:
	movdqa	.LC80(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L293
	movdqa	.LC39(%rip), %xmm3
	leaq	128+P2r3_2r7N(%rip), %rbx
	movaps	%xmm3, 64(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC40(%rip), %xmm2
	jmp	.L247
	.p2align 4,,10
	.p2align 3
.L315:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L247:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L315
	movaps	%xmm0, 96(%rsp)
	leaq	112+P2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC81(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC41(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L316:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L249:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L316
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r3_2r7N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC42(%rip), %xmm2
	leaq	-144(%rbx), %rbp
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	.LC43(%rip), %xmm2
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L317:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L251:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L317
	movaps	%xmm0, 96(%rsp)
	leaq	112+Q2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC82(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC44(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L318:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L253:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L318
	jmp	.L259
.L310:
	movdqa	48(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 80(%rsp)
	jmp	.L180
.L293:
	movdqa	.LC45(%rip), %xmm7
	leaq	112+P2_2r3N(%rip), %rbx
	movdqa	.LC46(%rip), %xmm3
	leaq	-128(%rbx), %rbp
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm3, 64(%rsp)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L319:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L244:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L319
	leaq	112+P2_2r3D(%rip), %rbx
	movdqa	.LC83(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC47(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L320:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L256:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L320
	movdqa	64(%rsp), %xmm0
	leaq	128+Q2_2r3N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC48(%rip), %xmm2
	leaq	-144(%rbx), %rbp
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	.LC49(%rip), %xmm2
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L321:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 64(%rsp)
.L258:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L321
	movaps	%xmm0, 96(%rsp)
	leaq	112+Q2_2r3D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC84(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC50(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L322:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L260:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L322
	jmp	.L259
.L289:
	movdqa	.LC9(%rip), %xmm2
	leaq	144+P8_16N(%rip), %rbx
	movdqa	.LC10(%rip), %xmm6
	leaq	-160(%rbx), %rbp
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm6, 64(%rsp)
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L323:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L190:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L323
	leaq	144+P8_16D(%rip), %rbx
	movdqa	.LC67(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC11(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 96(%rsp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L324:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L203:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L324
	movdqa	64(%rsp), %xmm0
	leaq	160+Q8_16N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC12(%rip), %xmm2
	leaq	-176(%rbx), %rbp
	movdqa	.LC13(%rip), %xmm6
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm6, 64(%rsp)
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L325:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L205:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L325
	movdqa	.LC68(%rip), %xmm1
	leaq	160+Q8_16D(%rip), %rbx
	movdqa	16(%rsp), %xmm0
	leaq	-176(%rbx), %rbp
	call	__addtf3@PLT
	movdqa	.LC14(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 96(%rsp)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L326:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L207:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L326
	jmp	.L242
.L290:
	movdqa	.LC21(%rip), %xmm6
	leaq	128+P4_5N(%rip), %rbx
	movdqa	.LC22(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L327:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L208:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L327
	leaq	128+P4_5D(%rip), %rbx
	movdqa	.LC72(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC23(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 96(%rsp)
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L328:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L220:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L328
	movdqa	64(%rsp), %xmm0
	leaq	144+Q4_5N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC24(%rip), %xmm2
	leaq	-160(%rbx), %rbp
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	.LC25(%rip), %xmm2
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L329:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L222:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L329
	movaps	%xmm0, 96(%rsp)
	leaq	128+Q4_5D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC73(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC26(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L330:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L224:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L330
	jmp	.L259
.L292:
	movdqa	.LC33(%rip), %xmm3
	leaq	128+P2r7_3r2N(%rip), %rbx
	movaps	%xmm3, 64(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC34(%rip), %xmm2
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L331:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L227:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L331
	movaps	%xmm0, 96(%rsp)
	leaq	112+P2r7_3r2D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC78(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC35(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L332:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L239:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L332
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r7_3r2N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC36(%rip), %xmm2
	leaq	-144(%rbx), %rbp
	movdqa	.LC37(%rip), %xmm6
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm6, 64(%rsp)
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L333:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 96(%rsp)
.L241:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L333
	leaq	128+Q2r7_3r2D(%rip), %rbx
	movdqa	.LC79(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC38(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 96(%rsp)
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L334:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L243:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L334
	jmp	.L242
	.size	__ieee754_y0f128, .-__ieee754_y0f128
	.section	.rodata
	.align 32
	.type	Y0_2D, @object
	.size	Y0_2D, 128
Y0_2D:
	.long	2063764000
	.long	2911076892
	.long	1907125092
	.long	1078063934
	.long	1945276173
	.long	241432212
	.long	2749470365
	.long	1077648436
	.long	3489946092
	.long	1328156527
	.long	1594995719
	.long	1077172904
	.long	1659038860
	.long	394765382
	.long	4023453667
	.long	1076660257
	.long	2138017168
	.long	2457735150
	.long	3902707481
	.long	1076120180
	.long	1271416741
	.long	1167634560
	.long	4080276094
	.long	1075556162
	.long	701391619
	.long	227881458
	.long	1471092492
	.long	1074966838
	.long	3179536446
	.long	3446825851
	.long	2230135802
	.long	1074346236
	.align 32
	.type	Y0_2N, @object
	.size	Y0_2N, 128
Y0_2N:
	.long	2213492227
	.long	1893587087
	.long	1357639529
	.long	-1069668667
	.long	4154999812
	.long	259459728
	.long	56231127
	.long	1077895366
	.long	3758971386
	.long	3351543981
	.long	803637920
	.long	-1069827948
	.long	3431327617
	.long	3000593462
	.long	3058745458
	.long	1077305864
	.long	109361112
	.long	2223609269
	.long	2840758285
	.long	-1070598429
	.long	2517232470
	.long	2117537947
	.long	1099634963
	.long	1076387535
	.long	3598674202
	.long	4203639049
	.long	1535162916
	.long	-1071654293
	.long	46388386
	.long	3134769428
	.long	4137354046
	.long	1075181150
	.align 32
	.type	Q2_2r3D, @object
	.size	Q2_2r3D, 144
Q2_2r3D:
	.long	3634498394
	.long	2400758381
	.long	306889955
	.long	1072710324
	.long	3098415148
	.long	381176426
	.long	2604688734
	.long	1073122180
	.long	4020091702
	.long	2567950402
	.long	924385081
	.long	1073436230
	.long	726185221
	.long	1549223711
	.long	374887259
	.long	1073680765
	.long	1639320133
	.long	969333074
	.long	778926218
	.long	1073854878
	.long	2815289374
	.long	865344308
	.long	241938540
	.long	1073967418
	.long	1564302232
	.long	1576695079
	.long	4166855519
	.long	1074018734
	.long	1740744028
	.long	3958000533
	.long	2773936285
	.long	1074002639
	.long	120979639
	.long	2026690421
	.long	3749987479
	.long	1073893459
	.align 32
	.type	Q2_2r3N, @object
	.size	Q2_2r3N, 160
Q2_2r3N:
	.long	2462608901
	.long	3099560343
	.long	3683455209
	.long	1072462378
	.long	3884840324
	.long	4205158048
	.long	1990421124
	.long	1072872749
	.long	3676414207
	.long	1036253565
	.long	3468344299
	.long	1073178761
	.long	3595114892
	.long	1844961594
	.long	3998287215
	.long	1073415248
	.long	2259555086
	.long	1135006481
	.long	3701319594
	.long	1073572469
	.long	1241825643
	.long	4069041999
	.long	1985811814
	.long	1073670674
	.long	4118330460
	.long	562273417
	.long	65557366
	.long	1073689150
	.long	933221794
	.long	756425956
	.long	2967982432
	.long	1073624768
	.long	348310698
	.long	2066880296
	.long	1260450407
	.long	1073443260
	.long	1724440725
	.long	3042389129
	.long	2325410045
	.long	1072961228
	.align 32
	.type	Q2r3_2r7D, @object
	.size	Q2r3_2r7D, 144
Q2r3_2r7D:
	.long	1912918804
	.long	2026477594
	.long	1224092366
	.long	1072535602
	.long	3824074977
	.long	3697905046
	.long	2182342532
	.long	1072966274
	.long	2847753074
	.long	709468532
	.long	4172486505
	.long	1073297295
	.long	4116868058
	.long	1850784012
	.long	1163446316
	.long	1073557240
	.long	2848710858
	.long	3738825842
	.long	2192716285
	.long	1073753067
	.long	1221228888
	.long	880479818
	.long	16800536
	.long	1073886543
	.long	3805853541
	.long	2368404701
	.long	3377478167
	.long	1073956260
	.long	521163460
	.long	369095570
	.long	1796796181
	.long	1073956622
	.long	2506826024
	.long	3999150462
	.long	504061892
	.long	1073876549
	.align 32
	.type	Q2r3_2r7N, @object
	.size	Q2r3_2r7N, 160
Q2r3_2r7N:
	.long	2531121608
	.long	2853435410
	.long	3740986992
	.long	1072291418
	.long	158746964
	.long	1168706683
	.long	2163092486
	.long	1072714298
	.long	3844986054
	.long	2937123719
	.long	3602319941
	.long	1073040887
	.long	4120777080
	.long	4073808782
	.long	406145258
	.long	1073293820
	.long	853157144
	.long	3020118791
	.long	526320032
	.long	1073480558
	.long	86020103
	.long	1645626868
	.long	3763738496
	.long	1073593124
	.long	3400082627
	.long	4247345144
	.long	3442887069
	.long	1073635557
	.long	2705901576
	.long	4242475379
	.long	4267543413
	.long	1073597402
	.long	3277216270
	.long	874617592
	.long	3422816221
	.long	1073439305
	.long	3518498702
	.long	3644409867
	.long	543999865
	.long	1072987316
	.align 32
	.type	Q2r7_3r2D, @object
	.size	Q2r7_3r2D, 160
Q2r7_3r2D:
	.long	547759173
	.long	581797360
	.long	4131126653
	.long	1072371196
	.long	2698909913
	.long	3188623939
	.long	2393100565
	.long	1072828684
	.long	583506396
	.long	4109793235
	.long	2265338395
	.long	1073187021
	.long	743737144
	.long	2903693631
	.long	594905541
	.long	1073483412
	.long	644851052
	.long	1703775631
	.long	1052182230
	.long	1073710941
	.long	1516776690
	.long	2479332956
	.long	2179389988
	.long	1073885687
	.long	1504052893
	.long	3150576681
	.long	751835844
	.long	1074004044
	.long	2368601901
	.long	734386341
	.long	2556378690
	.long	1074051016
	.long	4165991345
	.long	1065042528
	.long	3948402601
	.long	1074028362
	.long	915760660
	.long	2866117804
	.long	3336285092
	.long	1073917774
	.align 32
	.type	Q2r7_3r2N, @object
	.size	Q2r7_3r2N, 160
Q2r7_3r2N:
	.long	3242322814
	.long	3318371869
	.long	1870316712
	.long	1072121284
	.long	3340063813
	.long	1544863180
	.long	1978109782
	.long	1072576534
	.long	3230157338
	.long	1069262456
	.long	2503689678
	.long	1072934703
	.long	3545334666
	.long	2850452724
	.long	1776182460
	.long	1073223430
	.long	2743309782
	.long	3159576315
	.long	3243788955
	.long	1073442678
	.long	60436386
	.long	2470792306
	.long	3447737800
	.long	1073607759
	.long	3139653546
	.long	2947796073
	.long	871138306
	.long	1073698210
	.long	1419852819
	.long	1759184554
	.long	2768797806
	.long	1073720931
	.long	564840037
	.long	2058547614
	.long	3640328707
	.long	1073652236
	.long	1913868440
	.long	3028513747
	.long	3306842531
	.long	1073451429
	.align 32
	.type	Q3r2_4D, @object
	.size	Q3r2_4D, 160
Q3r2_4D:
	.long	2858081117
	.long	1486004330
	.long	3491577003
	.long	1071914852
	.long	1039653883
	.long	144518932
	.long	3022634334
	.long	1072399764
	.long	65104359
	.long	1375671224
	.long	2162927840
	.long	1072794521
	.long	1948115726
	.long	1266643107
	.long	173322344
	.long	1073121729
	.long	3192550863
	.long	3344304274
	.long	2945626538
	.long	1073391036
	.long	1623686521
	.long	1021397634
	.long	495601390
	.long	1073608159
	.long	777823374
	.long	4136916284
	.long	619270961
	.long	1073760887
	.long	3721823558
	.long	547003163
	.long	1815700081
	.long	1073860724
	.long	973734188
	.long	4119926453
	.long	3116614566
	.long	1073889772
	.long	3233148619
	.long	2792552983
	.long	1552982972
	.long	1073838152
	.align 32
	.type	Q3r2_4N, @object
	.size	Q3r2_4N, 176
Q3r2_4N:
	.long	338229671
	.long	1118227914
	.long	600947860
	.long	1071665354
	.long	861411491
	.long	2646063602
	.long	2754878962
	.long	1072152673
	.long	3949799155
	.long	1942948389
	.long	2697424944
	.long	1072544369
	.long	3751925582
	.long	2237011582
	.long	4000654915
	.long	1072866646
	.long	1644201199
	.long	3956458641
	.long	2164799077
	.long	1073128991
	.long	906221990
	.long	1810827525
	.long	89405350
	.long	1073333903
	.long	3658185343
	.long	1228319180
	.long	1751074829
	.long	1073480105
	.long	2176850545
	.long	683629495
	.long	2853558713
	.long	1073553387
	.long	55001348
	.long	967481802
	.long	3905372744
	.long	1073550244
	.long	560483644
	.long	3789325098
	.long	955789563
	.long	1073431960
	.long	1495065244
	.long	2164084335
	.long	4246647033
	.long	1073037902
	.align 32
	.type	Q4_5D, @object
	.size	Q4_5D, 160
Q4_5D:
	.long	2929420452
	.long	121834439
	.long	975138130
	.long	1071599449
	.long	461108403
	.long	3642736986
	.long	4033489858
	.long	1072116182
	.long	3965076816
	.long	2363894555
	.long	3694608277
	.long	1072539367
	.long	2854783297
	.long	1134873584
	.long	668349684
	.long	1072898462
	.long	573456784
	.long	2434265404
	.long	2587469216
	.long	1073196726
	.long	3790373152
	.long	2452875532
	.long	1321408353
	.long	1073440385
	.long	3146392695
	.long	1616036271
	.long	4258557177
	.long	1073630250
	.long	733669943
	.long	4078887604
	.long	1679264110
	.long	1073760716
	.long	3622822805
	.long	2454527251
	.long	3995586923
	.long	1073824667
	.long	934612885
	.long	3030291038
	.long	1516158769
	.long	1073810305
	.align 32
	.type	Q4_5N, @object
	.size	Q4_5N, 176
Q4_5N:
	.long	2249038306
	.long	3607015653
	.long	2417762296
	.long	1071352060
	.long	212937621
	.long	3889645869
	.long	1358986491
	.long	1071866277
	.long	3173375403
	.long	2691433938
	.long	888729055
	.long	1072291531
	.long	2689897882
	.long	2853349443
	.long	3918507863
	.long	1072643410
	.long	4288727706
	.long	1481273156
	.long	2007207222
	.long	1072939197
	.long	2226981953
	.long	2716968152
	.long	3569668368
	.long	1073174967
	.long	1591400621
	.long	237240760
	.long	3770712985
	.long	1073355815
	.long	587695224
	.long	399562883
	.long	81902539
	.long	1073468017
	.long	1465175801
	.long	3551694695
	.long	568040946
	.long	1073499221
	.long	2940413244
	.long	2646107406
	.long	2979579488
	.long	1073424619
	.long	3329012593
	.long	2955600171
	.long	1113728081
	.long	1073085690
	.align 32
	.type	Q5_8D, @object
	.size	Q5_8D, 176
Q5_8D:
	.long	2555618141
	.long	1499723625
	.long	1879641147
	.long	1071140952
	.long	3285844014
	.long	1605977710
	.long	1254467860
	.long	1071704760
	.long	2116361762
	.long	4054019263
	.long	99390738
	.long	1072174566
	.long	222603279
	.long	3347356272
	.long	3173041946
	.long	1072578451
	.long	121029665
	.long	808284985
	.long	2153375184
	.long	1072929233
	.long	159110754
	.long	802074055
	.long	885710174
	.long	1073229598
	.long	1579105316
	.long	3571128257
	.long	2568240118
	.long	1073480383
	.long	926755651
	.long	4292816495
	.long	3825099416
	.long	1073672257
	.long	2332965533
	.long	2856279518
	.long	1218624685
	.long	1073805322
	.long	2583019621
	.long	1730326074
	.long	2677427281
	.long	1073866393
	.long	949997511
	.long	2961556009
	.long	1643564695
	.long	1073834732
	.align 32
	.type	Q5_8N, @object
	.size	Q5_8N, 176
Q5_8N:
	.long	4055648085
	.long	3772681626
	.long	2739575274
	.long	1070893607
	.long	1468380895
	.long	1165425246
	.long	3821139121
	.long	1071455524
	.long	2466310127
	.long	4223702523
	.long	3158210762
	.long	1071922902
	.long	244502741
	.long	2586228914
	.long	3651520351
	.long	1072326533
	.long	3934522336
	.long	1641169938
	.long	3931226991
	.long	1072676703
	.long	1947539235
	.long	2618487640
	.long	1538339995
	.long	1072970723
	.long	2586823504
	.long	271486696
	.long	3471891005
	.long	1073214856
	.long	449733154
	.long	2124728746
	.long	3457075800
	.long	1073392674
	.long	4150431487
	.long	3320523937
	.long	4286409828
	.long	1073505809
	.long	3246709568
	.long	3357459288
	.long	2494155339
	.long	1073538829
	.long	3528852740
	.long	1972177484
	.long	1945973386
	.long	1073436520
	.align 32
	.type	Q8_16D, @object
	.size	Q8_16D, 192
Q8_16D:
	.long	1947865514
	.long	3043253961
	.long	2400264657
	.long	1070218096
	.long	805553778
	.long	3057317494
	.long	1890910355
	.long	1070846432
	.long	95871732
	.long	3000689804
	.long	3252775000
	.long	1071387430
	.long	4269966521
	.long	638901261
	.long	1957562695
	.long	1071862962
	.long	881311529
	.long	3175760324
	.long	380598275
	.long	1072293696
	.long	314453399
	.long	3475939239
	.long	4245254135
	.long	1072669276
	.long	2113809170
	.long	1724837446
	.long	137322873
	.long	1073001189
	.long	2065648598
	.long	3897776242
	.long	42407180
	.long	1073286594
	.long	2218986055
	.long	2101003602
	.long	6321635
	.long	1073512653
	.long	3911398386
	.long	1873946617
	.long	2234549333
	.long	1073686291
	.long	225254486
	.long	2137960184
	.long	748553817
	.long	1073787160
	.long	2676610123
	.long	1767692098
	.long	3196685912
	.long	1073801588
	.align 32
	.type	Q8_16N, @object
	.size	Q8_16N, 192
Q8_16N:
	.long	2502124403
	.long	3229266072
	.long	3886551969
	.long	1069969831
	.long	1775374225
	.long	1429478612
	.long	4090791653
	.long	1070600154
	.long	606444416
	.long	1245596951
	.long	2145962788
	.long	1071136555
	.long	2864485545
	.long	358646466
	.long	3629887132
	.long	1071613858
	.long	1356564905
	.long	505027508
	.long	74011154
	.long	1072043077
	.long	2173164943
	.long	3999841374
	.long	3437528626
	.long	1072418998
	.long	504352721
	.long	1179853759
	.long	4181678658
	.long	1072747475
	.long	3094309259
	.long	3508308258
	.long	1210167027
	.long	1073025976
	.long	314941306
	.long	2185889966
	.long	55011141
	.long	1073244514
	.long	445560389
	.long	23012035
	.long	1789532622
	.long	1073405433
	.long	4293069583
	.long	865464412
	.long	4006394503
	.long	1073481791
	.long	1034821207
	.long	4181988469
	.long	421413619
	.long	1073428288
	.align 32
	.type	Q16_ID, @object
	.size	Q16_ID, 160
Q16_ID:
	.long	1600863022
	.long	4248852790
	.long	1479372161
	.long	1070081826
	.long	1671964725
	.long	998116062
	.long	1099794922
	.long	1070745855
	.long	3289647365
	.long	3726093849
	.long	1755766272
	.long	1071324422
	.long	1515423056
	.long	1644088213
	.long	3113293733
	.long	1071837904
	.long	1647161955
	.long	888633351
	.long	2597894884
	.long	1072291931
	.long	2944092067
	.long	1913185110
	.long	3207083801
	.long	1072694420
	.long	240945280
	.long	1715013188
	.long	2042408254
	.long	1073035804
	.long	2817878739
	.long	3142897038
	.long	1699400841
	.long	1073320130
	.long	1481610203
	.long	2948793082
	.long	3241384939
	.long	1073540142
	.long	1688555469
	.long	3041571166
	.long	2251451187
	.long	1073669616
	.align 32
	.type	Q16_IN, @object
	.size	Q16_IN, 176
Q16_IN:
	.long	3083910888
	.long	1422354571
	.long	1062550612
	.long	1069832668
	.long	3521892705
	.long	3753809396
	.long	81118513
	.long	1070497919
	.long	422511562
	.long	678028382
	.long	3225564065
	.long	1071074225
	.long	1484743401
	.long	1424912854
	.long	600110873
	.long	1071587404
	.long	4181072475
	.long	274159147
	.long	3228481337
	.long	1072042652
	.long	2675002082
	.long	4279681999
	.long	3808945574
	.long	1072440453
	.long	3957966131
	.long	2702378549
	.long	623729350
	.long	1072780650
	.long	4228964100
	.long	1230426604
	.long	2581756014
	.long	1073060343
	.long	4284304002
	.long	615634043
	.long	418169149
	.long	1073265680
	.long	4141498020
	.long	2932411532
	.long	1522016451
	.long	1073365449
	.long	111829467
	.long	3686536051
	.long	3067611765
	.long	1073244936
	.align 32
	.type	P2_2r3D, @object
	.size	P2_2r3D, 144
P2_2r3D:
	.long	2612927463
	.long	1407267426
	.long	1122463043
	.long	1073050935
	.long	2336823317
	.long	922076172
	.long	3877448146
	.long	1073444777
	.long	3220064701
	.long	907932423
	.long	302338572
	.long	1073743455
	.long	2457803124
	.long	726222254
	.long	106112771
	.long	1073957641
	.long	1695759067
	.long	104542706
	.long	2570355086
	.long	1074108938
	.long	333198308
	.long	3587099482
	.long	464477157
	.long	1074198942
	.long	3554594110
	.long	2545446259
	.long	1307328952
	.long	1074211639
	.long	3021082174
	.long	3489662270
	.long	3953821064
	.long	1074148361
	.long	2073386439
	.long	1788358638
	.long	17666849
	.long	1073986745
	.align 32
	.type	P2_2r3N, @object
	.size	P2_2r3N, 144
P2_2r3N:
	.long	3180291462
	.long	1359757351
	.long	434322359
	.long	-1074682914
	.long	3910125858
	.long	2045153362
	.long	2107333874
	.long	-1074291672
	.long	924535264
	.long	1264544162
	.long	3620263216
	.long	-1073998908
	.long	3091915914
	.long	2272148069
	.long	2669927876
	.long	-1073789744
	.long	3237983413
	.long	1204617137
	.long	869340806
	.long	-1073649187
	.long	351792524
	.long	2971224715
	.long	1408590437
	.long	-1073579078
	.long	301178285
	.long	97025813
	.long	585191138
	.long	-1073586665
	.long	152693292
	.long	544859148
	.long	260872612
	.long	-1073685175
	.long	406162131
	.long	2735333673
	.long	1421467220
	.long	-1073925316
	.align 32
	.type	P2r3_2r7D, @object
	.size	P2r3_2r7D, 144
P2r3_2r7D:
	.long	1814364873
	.long	2981938518
	.long	587266486
	.long	1072659583
	.long	1372286499
	.long	1251678651
	.long	1646076246
	.long	1073084114
	.long	3399577227
	.long	853544612
	.long	1750897031
	.long	1073404113
	.long	4051783337
	.long	445084139
	.long	1006767602
	.long	1073650809
	.long	1209908873
	.long	2915132349
	.long	2234671604
	.long	1073835014
	.long	2774230633
	.long	2160135489
	.long	2419436668
	.long	1073957789
	.long	3845086207
	.long	1366911366
	.long	1153646736
	.long	1074016291
	.long	2559870469
	.long	2667532431
	.long	2673322041
	.long	1074005086
	.long	1154196708
	.long	1463552930
	.long	556167979
	.long	1073897869
	.align 32
	.type	P2r3_2r7N, @object
	.size	P2r3_2r7N, 160
P2r3_2r7N:
	.long	3330404467
	.long	1538622158
	.long	76477170
	.long	-1075074033
	.long	2742649519
	.long	2242622583
	.long	3477738961
	.long	-1074653824
	.long	928017484
	.long	3999464743
	.long	3592733668
	.long	-1074333332
	.long	3327512709
	.long	3167712090
	.long	1936174403
	.long	-1074093344
	.long	1457558918
	.long	2214618062
	.long	3550994072
	.long	-1073916564
	.long	904301848
	.long	2349650326
	.long	1313266276
	.long	-1073802399
	.long	238581476
	.long	656318129
	.long	1202232402
	.long	-1073765799
	.long	529145361
	.long	1064490111
	.long	3489677431
	.long	-1073805386
	.long	1817494267
	.long	3368359430
	.long	1852076382
	.long	-1073965181
	.long	745458139
	.long	1618034449
	.long	2215684086
	.long	-1074353774
	.align 32
	.type	P2r7_3r2D, @object
	.size	P2r7_3r2D, 144
P2r7_3r2D:
	.long	93086068
	.long	4062326842
	.long	619587738
	.long	1072459262
	.long	2302057666
	.long	1669562632
	.long	1006925769
	.long	1072900567
	.long	2775794254
	.long	609086079
	.long	1106837290
	.long	1073241240
	.long	2716423975
	.long	3461159996
	.long	4053081265
	.long	1073511911
	.long	3134248687
	.long	3725026136
	.long	2994554491
	.long	1073719912
	.long	3181081606
	.long	3888158192
	.long	3662122308
	.long	1073868369
	.long	3399965804
	.long	3572934127
	.long	1522451411
	.long	1073947063
	.long	214828058
	.long	3365730769
	.long	1843106115
	.long	1073954261
	.long	1752450014
	.long	4014862103
	.long	4132839548
	.long	1073878183
	.align 32
	.type	P2r7_3r2N, @object
	.size	P2r7_3r2N, 160
P2r7_3r2N:
	.long	4262329358
	.long	2460462794
	.long	3910121072
	.long	-1075274819
	.long	1112857487
	.long	1170348684
	.long	2766258095
	.long	-1074837008
	.long	2031998443
	.long	185155800
	.long	656035909
	.long	-1074497463
	.long	4065790600
	.long	3101098248
	.long	3516036715
	.long	-1074230443
	.long	1725491255
	.long	1895198958
	.long	1266160675
	.long	-1074029275
	.long	4219976478
	.long	672211413
	.long	1429717248
	.long	-1073893448
	.long	1712328105
	.long	99499753
	.long	1685404999
	.long	-1073828612
	.long	1718586612
	.long	1511339745
	.long	1051713515
	.long	-1073847024
	.long	2408517700
	.long	1496380487
	.long	3821546484
	.long	-1073975245
	.long	3437507012
	.long	3696803776
	.long	1961083538
	.long	-1074328431
	.align 32
	.type	P3r2_4D, @object
	.size	P3r2_4D, 160
P3r2_4D:
	.long	2666282709
	.long	4242722001
	.long	3327909251
	.long	1072270179
	.long	901937649
	.long	486737615
	.long	1702305745
	.long	1072743391
	.long	2071260011
	.long	983728723
	.long	3204130385
	.long	1073118466
	.long	999708756
	.long	1290184306
	.long	3391305100
	.long	1073427731
	.long	2925000449
	.long	1450689423
	.long	470266480
	.long	1073678215
	.long	991281942
	.long	2248713988
	.long	2272380199
	.long	1073865780
	.long	750183760
	.long	3980368674
	.long	178683757
	.long	1073993776
	.long	1656406792
	.long	2779790667
	.long	2654466947
	.long	1074056923
	.long	3482546193
	.long	1437846149
	.long	1407009693
	.long	1074042790
	.long	2352296794
	.long	3210290327
	.long	770996456
	.long	1073938553
	.align 32
	.type	P3r2_4N, @object
	.size	P3r2_4N, 160
P3r2_4N:
	.long	1713759920
	.long	3345372262
	.long	1059075530
	.long	-1075462960
	.long	167930860
	.long	122798471
	.long	1374416040
	.long	-1074989360
	.long	1899391840
	.long	1448429192
	.long	3635924878
	.long	-1074618264
	.long	775048342
	.long	1263778901
	.long	2380249357
	.long	-1074313160
	.long	3254505442
	.long	811706263
	.long	1945817714
	.long	-1074066574
	.long	2228882054
	.long	528993622
	.long	1008012040
	.long	-1073886823
	.long	3607153372
	.long	1130739745
	.long	2822349720
	.long	-1073770897
	.long	3711452467
	.long	3469041287
	.long	3340766671
	.long	-1073724355
	.long	303706496
	.long	2651060674
	.long	2914434958
	.long	-1073767678
	.long	3437058749
	.long	77074259
	.long	860958324
	.long	-1073937935
	.align 32
	.type	P4_5D, @object
	.size	P4_5D, 160
P4_5D:
	.long	1632237097
	.long	3610595494
	.long	2044718816
	.long	1071963770
	.long	929253441
	.long	3707470018
	.long	98675574
	.long	1072460019
	.long	1952568788
	.long	1690155675
	.long	877829553
	.long	1072867485
	.long	3508803886
	.long	3405969750
	.long	3116216491
	.long	1073208012
	.long	1174134035
	.long	2037552779
	.long	3714884149
	.long	1073486161
	.long	2346296544
	.long	2718148331
	.long	1717259544
	.long	1073703308
	.long	3688962077
	.long	3624366041
	.long	208436596
	.long	1073868677
	.long	3516728480
	.long	544770629
	.long	270008989
	.long	1073960486
	.long	1218390132
	.long	1450083072
	.long	1688605491
	.long	1073981503
	.long	122226705
	.long	582061421
	.long	2359704668
	.long	1073903001
	.align 32
	.type	P4_5N, @object
	.size	P4_5N, 160
P4_5N:
	.long	3741599017
	.long	2067131500
	.long	3834498717
	.long	-1075770076
	.long	2708972155
	.long	2646689512
	.long	3449309809
	.long	-1075274826
	.long	3908857828
	.long	3309360185
	.long	503024559
	.long	-1074867299
	.long	4213560308
	.long	1654056626
	.long	2255843613
	.long	-1074528282
	.long	1097351536
	.long	1406415092
	.long	1198805110
	.long	-1074256599
	.long	2665816792
	.long	3418226451
	.long	2083097334
	.long	-1074043736
	.long	3039002330
	.long	3250903562
	.long	1426310017
	.long	-1073889620
	.long	2396603876
	.long	1294281032
	.long	3542730353
	.long	-1073805217
	.long	2008012806
	.long	1154481737
	.long	3971851943
	.long	-1073812204
	.long	2773010042
	.long	556399238
	.long	1725743542
	.long	-1073951445
	.align 32
	.type	P5_8D, @object
	.size	P5_8D, 160
P5_8D:
	.long	3858553339
	.long	2634032874
	.long	2814077417
	.long	1071332831
	.long	244311394
	.long	2614684248
	.long	3806690306
	.long	1071876520
	.long	2804930034
	.long	2296153827
	.long	1983551479
	.long	1072329853
	.long	3645035719
	.long	2310246680
	.long	3332311259
	.long	1072717557
	.long	3957947346
	.long	345409271
	.long	3001280436
	.long	1073047509
	.long	2623231133
	.long	2784549915
	.long	2797347796
	.long	1073323083
	.long	3279081269
	.long	406906148
	.long	3858242631
	.long	1073546539
	.long	4156850689
	.long	2522415495
	.long	443060590
	.long	1073700265
	.long	2034055815
	.long	4179491777
	.long	3083188410
	.long	1073791756
	.long	1459447710
	.long	1480745170
	.long	3412992708
	.long	1073796099
	.align 32
	.type	P5_8N, @object
	.size	P5_8N, 176
P5_8N:
	.long	2707356149
	.long	3048588180
	.long	2628966151
	.long	-1076402789
	.long	1811764121
	.long	4082533938
	.long	2550960184
	.long	-1075857245
	.long	3076037003
	.long	2545514597
	.long	429608610
	.long	-1075405409
	.long	1610540235
	.long	1394355752
	.long	3338395741
	.long	-1075019569
	.long	3431238378
	.long	3587731350
	.long	2107094559
	.long	-1074691415
	.long	1021866422
	.long	3880906522
	.long	476650842
	.long	-1074418483
	.long	3266552229
	.long	1303186198
	.long	1634541845
	.long	-1074200246
	.long	1696731008
	.long	1521662883
	.long	266318894
	.long	-1074054756
	.long	2274034499
	.long	1855033787
	.long	3572974290
	.long	-1073982506
	.long	4098940999
	.long	292626924
	.long	4145755519
	.long	-1074010749
	.long	1298623731
	.long	3493461773
	.long	2601278540
	.long	-1074256822
	.align 32
	.type	P8_16D, @object
	.size	P8_16D, 176
P8_16D:
	.long	1392442415
	.long	2714622358
	.long	4105451379
	.long	1070738211
	.long	1021593647
	.long	1142066261
	.long	1020592666
	.long	1071340262
	.long	3189067280
	.long	4144933876
	.long	1812910921
	.long	1071856160
	.long	925367770
	.long	2196747055
	.long	3521694466
	.long	1072308663
	.long	2092442662
	.long	4204420850
	.long	3939887620
	.long	1072703980
	.long	3135508816
	.long	2143793162
	.long	3623269566
	.long	1073045475
	.long	3746786824
	.long	3316212206
	.long	1991464299
	.long	1073339134
	.long	3787479536
	.long	1117443937
	.long	2498932889
	.long	1073570260
	.long	3996748982
	.long	3607915880
	.long	1308616838
	.long	1073745645
	.long	1894502047
	.long	1623536979
	.long	4243869677
	.long	1073837682
	.long	324228361
	.long	3305710386
	.long	29656116
	.long	1073834480
	.align 32
	.type	P8_16N, @object
	.size	P8_16N, 176
P8_16N:
	.long	3441446777
	.long	368241742
	.long	1934278242
	.long	-1076998008
	.long	2649841020
	.long	211543909
	.long	2677867756
	.long	-1076394698
	.long	3423124576
	.long	2773061380
	.long	1455183486
	.long	-1075880197
	.long	3031211222
	.long	1191296439
	.long	3522201602
	.long	-1075428961
	.long	300392875
	.long	1970239746
	.long	4045586665
	.long	-1075034272
	.long	754415478
	.long	3505154410
	.long	2580034077
	.long	-1074693029
	.long	988369682
	.long	4179627860
	.long	3029284233
	.long	-1074400387
	.long	2816378427
	.long	1818926776
	.long	3739298959
	.long	-1074175329
	.long	3233020400
	.long	3434514718
	.long	512737106
	.long	-1074007934
	.long	1641147472
	.long	1426562695
	.long	3556494238
	.long	-1073931029
	.long	1142716280
	.long	3486216402
	.long	4137820811
	.long	-1073981912
	.align 32
	.type	P16_ID, @object
	.size	P16_ID, 160
P16_ID:
	.long	2302127381
	.long	75247160
	.long	309937960
	.long	1070499271
	.long	174239057
	.long	3844058760
	.long	3185303209
	.long	1071147172
	.long	2547688394
	.long	4240321745
	.long	3336651151
	.long	1071709446
	.long	1123334077
	.long	1439246872
	.long	612462105
	.long	1072194191
	.long	1571159511
	.long	1066169409
	.long	4096124016
	.long	1072629759
	.long	2852322598
	.long	3284574755
	.long	733397546
	.long	1072999035
	.long	1191436839
	.long	4114954460
	.long	2057498477
	.long	1073310618
	.long	3820887290
	.long	648419554
	.long	96842349
	.long	1073557720
	.long	2736443170
	.long	243443225
	.long	3711012714
	.long	1073723073
	.long	1932530817
	.long	2335505160
	.long	2170943808
	.long	1073782179
	.align 32
	.type	P16_IN, @object
	.size	P16_IN, 160
P16_IN:
	.long	2589810063
	.long	84653055
	.long	4106776589
	.long	-1077234049
	.long	2949555925
	.long	501735747
	.long	2047944935
	.long	-1076587254
	.long	3701937743
	.long	142688935
	.long	4208690017
	.long	-1076028143
	.long	2582271709
	.long	4231494496
	.long	1636506783
	.long	-1075541156
	.long	3007573154
	.long	325010739
	.long	3695165274
	.long	-1075108822
	.long	4192040969
	.long	1937156424
	.long	952670179
	.long	-1074736708
	.long	251742492
	.long	2732457166
	.long	207992049
	.long	-1074429117
	.long	2268223248
	.long	3794764461
	.long	2689805311
	.long	-1074187139
	.long	3323914889
	.long	611521125
	.long	1376779870
	.long	-1074034157
	.long	2355030360
	.long	1965733965
	.long	2448638578
	.long	-1074002843
	.align 32
	.type	J0_2D, @object
	.size	J0_2D, 112
J0_2D:
	.long	1139445254
	.long	4057098914
	.long	2510454028
	.long	1077656898
	.long	4140172893
	.long	3519439226
	.long	3889312048
	.long	1077224748
	.long	2542926998
	.long	809870881
	.long	3887434851
	.long	1076723508
	.long	2258128643
	.long	620682462
	.long	1737830267
	.long	1076184484
	.long	1499247191
	.long	2519075985
	.long	853115069
	.long	1075611993
	.long	1941526840
	.long	2590687178
	.long	1552214487
	.long	1075009157
	.long	4240932278
	.long	3532792720
	.long	1240483707
	.long	1074369786
	.align 32
	.type	J0_2N, @object
	.size	J0_2N, 112
J0_2N:
	.long	1139445349
	.long	4057098914
	.long	2510454028
	.long	1077263682
	.long	4089431637
	.long	352650945
	.long	3914247455
	.long	-1070599579
	.long	3561630883
	.long	1516221105
	.long	2815522862
	.long	1076457004
	.long	3389646809
	.long	2106831062
	.long	1221134972
	.long	-1071509828
	.long	939112629
	.long	4081082506
	.long	3119527201
	.long	1075432631
	.long	3520223163
	.long	2954792473
	.long	1288474098
	.long	-1072653410
	.long	888959643
	.long	149342189
	.long	3146367011
	.long	1074149520
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	3520223163
	.long	2954792473
	.long	1288474098
	.long	-1072653410
	.align 16
.LC1:
	.long	888959643
	.long	149342189
	.long	3146367011
	.long	1074149520
	.align 16
.LC2:
	.long	1941526840
	.long	2590687178
	.long	1552214487
	.long	1075009157
	.align 16
.LC3:
	.long	3323914889
	.long	611521125
	.long	1376779870
	.long	-1074034157
	.align 16
.LC4:
	.long	2355030360
	.long	1965733965
	.long	2448638578
	.long	-1074002843
	.align 16
.LC5:
	.long	2736443170
	.long	243443225
	.long	3711012714
	.long	1073723073
	.align 16
.LC6:
	.long	4141498020
	.long	2932411532
	.long	1522016451
	.long	1073365449
	.align 16
.LC7:
	.long	111829467
	.long	3686536051
	.long	3067611765
	.long	1073244936
	.align 16
.LC8:
	.long	1481610203
	.long	2948793082
	.long	3241384939
	.long	1073540142
	.align 16
.LC9:
	.long	1641147472
	.long	1426562695
	.long	3556494238
	.long	-1073931029
	.align 16
.LC10:
	.long	1142716280
	.long	3486216402
	.long	4137820811
	.long	-1073981912
	.align 16
.LC11:
	.long	1894502047
	.long	1623536979
	.long	4243869677
	.long	1073837682
	.align 16
.LC12:
	.long	4293069583
	.long	865464412
	.long	4006394503
	.long	1073481791
	.align 16
.LC13:
	.long	1034821207
	.long	4181988469
	.long	421413619
	.long	1073428288
	.align 16
.LC14:
	.long	225254486
	.long	2137960184
	.long	748553817
	.long	1073787160
	.align 16
.LC15:
	.long	4098940999
	.long	292626924
	.long	4145755519
	.long	-1074010749
	.align 16
.LC16:
	.long	1298623731
	.long	3493461773
	.long	2601278540
	.long	-1074256822
	.align 16
.LC17:
	.long	2034055815
	.long	4179491777
	.long	3083188410
	.long	1073791756
	.align 16
.LC18:
	.long	3246709568
	.long	3357459288
	.long	2494155339
	.long	1073538829
	.align 16
.LC19:
	.long	3528852740
	.long	1972177484
	.long	1945973386
	.long	1073436520
	.align 16
.LC20:
	.long	2583019621
	.long	1730326074
	.long	2677427281
	.long	1073866393
	.align 16
.LC21:
	.long	2008012806
	.long	1154481737
	.long	3971851943
	.long	-1073812204
	.align 16
.LC22:
	.long	2773010042
	.long	556399238
	.long	1725743542
	.long	-1073951445
	.align 16
.LC23:
	.long	1218390132
	.long	1450083072
	.long	1688605491
	.long	1073981503
	.align 16
.LC24:
	.long	2940413244
	.long	2646107406
	.long	2979579488
	.long	1073424619
	.align 16
.LC25:
	.long	3329012593
	.long	2955600171
	.long	1113728081
	.long	1073085690
	.align 16
.LC26:
	.long	3622822805
	.long	2454527251
	.long	3995586923
	.long	1073824667
	.align 16
.LC27:
	.long	303706496
	.long	2651060674
	.long	2914434958
	.long	-1073767678
	.align 16
.LC28:
	.long	3437058749
	.long	77074259
	.long	860958324
	.long	-1073937935
	.align 16
.LC29:
	.long	3482546193
	.long	1437846149
	.long	1407009693
	.long	1074042790
	.align 16
.LC30:
	.long	560483644
	.long	3789325098
	.long	955789563
	.long	1073431960
	.align 16
.LC31:
	.long	1495065244
	.long	2164084335
	.long	4246647033
	.long	1073037902
	.align 16
.LC32:
	.long	973734188
	.long	4119926453
	.long	3116614566
	.long	1073889772
	.align 16
.LC33:
	.long	2408517700
	.long	1496380487
	.long	3821546484
	.long	-1073975245
	.align 16
.LC34:
	.long	3437507012
	.long	3696803776
	.long	1961083538
	.long	-1074328431
	.align 16
.LC35:
	.long	214828058
	.long	3365730769
	.long	1843106115
	.long	1073954261
	.align 16
.LC36:
	.long	564840037
	.long	2058547614
	.long	3640328707
	.long	1073652236
	.align 16
.LC37:
	.long	1913868440
	.long	3028513747
	.long	3306842531
	.long	1073451429
	.align 16
.LC38:
	.long	4165991345
	.long	1065042528
	.long	3948402601
	.long	1074028362
	.align 16
.LC39:
	.long	1817494267
	.long	3368359430
	.long	1852076382
	.long	-1073965181
	.align 16
.LC40:
	.long	745458139
	.long	1618034449
	.long	2215684086
	.long	-1074353774
	.align 16
.LC41:
	.long	2559870469
	.long	2667532431
	.long	2673322041
	.long	1074005086
	.align 16
.LC42:
	.long	3277216270
	.long	874617592
	.long	3422816221
	.long	1073439305
	.align 16
.LC43:
	.long	3518498702
	.long	3644409867
	.long	543999865
	.long	1072987316
	.align 16
.LC44:
	.long	521163460
	.long	369095570
	.long	1796796181
	.long	1073956622
	.align 16
.LC45:
	.long	152693292
	.long	544859148
	.long	260872612
	.long	-1073685175
	.align 16
.LC46:
	.long	406162131
	.long	2735333673
	.long	1421467220
	.long	-1073925316
	.align 16
.LC47:
	.long	3021082174
	.long	3489662270
	.long	3953821064
	.long	1074148361
	.align 16
.LC48:
	.long	348310698
	.long	2066880296
	.long	1260450407
	.long	1073443260
	.align 16
.LC49:
	.long	1724440725
	.long	3042389129
	.long	2325410045
	.long	1072961228
	.align 16
.LC50:
	.long	1740744028
	.long	3958000533
	.long	2773936285
	.long	1074002639
	.align 16
.LC52:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC53:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC54:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC55:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC56:
	.long	0
	.long	0
	.long	0
	.long	1069940736
	.align 16
.LC57:
	.long	4240932278
	.long	3532792720
	.long	1240483707
	.long	1074369786
	.align 16
.LC58:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC59:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147352575
	.align 16
.LC60:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC61:
	.long	0
	.long	0
	.long	0
	.long	1090453504
	.align 16
.LC62:
	.long	352245758
	.long	3508200361
	.long	1963207094
	.long	1073619165
	.align 16
.LC63:
	.long	0
	.long	0
	.long	0
	.long	1073479680
	.align 16
.LC64:
	.long	0
	.long	0
	.long	0
	.long	1073414144
	.align 16
.LC65:
	.long	1932530817
	.long	2335505160
	.long	2170943808
	.long	1073782179
	.align 16
.LC66:
	.long	1688555469
	.long	3041571166
	.long	2251451187
	.long	1073669616
	.align 16
.LC67:
	.long	324228361
	.long	3305710386
	.long	29656116
	.long	1073834480
	.align 16
.LC68:
	.long	2676610123
	.long	1767692098
	.long	3196685912
	.long	1073801588
	.align 16
.LC69:
	.long	0
	.long	0
	.long	0
	.long	1073512448
	.align 16
.LC70:
	.long	1459447710
	.long	1480745170
	.long	3412992708
	.long	1073796099
	.align 16
.LC71:
	.long	949997511
	.long	2961556009
	.long	1643564695
	.long	1073834732
	.align 16
.LC72:
	.long	122226705
	.long	582061421
	.long	2359704668
	.long	1073903001
	.align 16
.LC73:
	.long	934612885
	.long	3030291038
	.long	1516158769
	.long	1073810305
	.align 16
.LC74:
	.long	0
	.long	0
	.long	0
	.long	1073577984
	.align 16
.LC75:
	.long	0
	.long	0
	.long	0
	.long	1073561600
	.align 16
.LC76:
	.long	2352296794
	.long	3210290327
	.long	770996456
	.long	1073938553
	.align 16
.LC77:
	.long	3233148619
	.long	2792552983
	.long	1552982972
	.long	1073838152
	.align 16
.LC78:
	.long	1752450014
	.long	4014862103
	.long	4132839548
	.long	1073878183
	.align 16
.LC79:
	.long	915760660
	.long	2866117804
	.long	3336285092
	.long	1073917774
	.align 16
.LC80:
	.long	0
	.long	0
	.long	0
	.long	1073594368
	.align 16
.LC81:
	.long	1154196708
	.long	1463552930
	.long	556167979
	.long	1073897869
	.align 16
.LC82:
	.long	2506826024
	.long	3999150462
	.long	504061892
	.long	1073876549
	.align 16
.LC83:
	.long	2073386439
	.long	1788358638
	.long	17666849
	.long	1073986745
	.align 16
.LC84:
	.long	120979639
	.long	2026690421
	.long	3749987479
	.long	1073893459
	.align 16
.LC85:
	.long	3598674202
	.long	4203639049
	.long	1535162916
	.long	-1071654293
	.align 16
.LC86:
	.long	46388386
	.long	3134769428
	.long	4137354046
	.long	1075181150
	.align 16
.LC87:
	.long	701391619
	.long	227881458
	.long	1471092492
	.long	1074966838
	.align 16
.LC88:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC89:
	.long	2946755178
	.long	710146126
	.long	115121288
	.long	1073628659
	.align 16
.LC90:
	.long	2520643536
	.long	3901229555
	.long	1771879681
	.long	1073425997
	.align 16
.LC91:
	.long	3179536446
	.long	3446825851
	.long	2230135802
	.long	1074346236
