	.text
	.p2align 4,,15
	.globl	__fmaf
	.type	__fmaf, @function
__fmaf:
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	xorps	.LC0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	jp	.L2
	je	.L18
.L2:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %eax
	movl	%eax, %edx
	andl	$-32704, %edx
	orl	$32640, %edx
	movl	%edx, 28(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	cvtss2sd	%xmm2, %xmm2
	addsd	%xmm2, %xmm1
	movq	%xmm1, %rbx
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %ebp
	movl	%ebp, %edi
	andl	$61, %edi
	movl	%edi, %edx
	orl	%eax, %edx
	movl	%edx, 28(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %eax
	notl	%eax
	testl	%edi, %eax
	jne	.L19
.L5:
	testb	$1, %bl
	jne	.L6
	movq	%rbx, %rax
	shrq	$48, %rax
	andw	$32752, %ax
	cmpw	$32752, %ax
	je	.L6
	shrl	$5, %ebp
	movabsq	$-4294967296, %rax
	andl	$1, %ebp
	orl	%ebx, %ebp
	andq	%rax, %rbx
	orq	%rbp, %rbx
.L6:
	movq	%rbx, 8(%rsp)
	pxor	%xmm0, %xmm0
	movsd	8(%rsp), %xmm3
	addq	$40, %rsp
	popq	%rbx
	cvtsd2ss	%xmm3, %xmm0
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pxor	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	call	__feraiseexcept@PLT
	jmp	.L5
	.size	__fmaf, .-__fmaf
	.weak	fmaf32
	.set	fmaf32,__fmaf
	.weak	fmaf
	.set	fmaf,__fmaf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483648
	.long	0
	.long	0
	.long	0
