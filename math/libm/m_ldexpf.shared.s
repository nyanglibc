	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ldexpf
	.type	__ldexpf, @function
__ldexpf:
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm3
	movss	.LC1(%rip), %xmm2
	andps	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm2
	jb	.L2
	pxor	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm0
	jp	.L3
	jne	.L3
.L2:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$40, %rsp
	movss	%xmm2, 28(%rsp)
	movaps	%xmm1, (%rsp)
	call	__scalbnf@PLT
	movaps	(%rsp), %xmm1
	andps	%xmm0, %xmm1
	movss	28(%rsp), %xmm2
	ucomiss	%xmm1, %xmm2
	jb	.L6
	pxor	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm0
	jp	.L1
	jne	.L1
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$40, %rsp
	ret
	.size	__ldexpf, .-__ldexpf
	.globl	__wrap_scalbnf
	.set	__wrap_scalbnf,__ldexpf
	.weak	scalbnf32
	.set	scalbnf32,__wrap_scalbnf
	.weak	scalbnf
	.set	scalbnf,__wrap_scalbnf
	.weak	ldexpf32
	.set	ldexpf32,__ldexpf
	.weak	ldexpf
	.set	ldexpf,__ldexpf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
