	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__letf2
	.globl	__divtf3
	.globl	__multf3
	.globl	__lttf2
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__catanhf128
	.type	__catanhf128, @function
__catanhf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$88, %rsp
	movdqa	112(%rsp), %xmm7
	movdqa	.LC2(%rip), %xmm3
	pand	%xmm7, %xmm3
	movaps	%xmm7, 16(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L48
	movdqa	.LC3(%rip), %xmm5
	movdqa	(%rsp), %xmm0
	movdqa	%xmm5, %xmm1
	movaps	%xmm5, 48(%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L84
	movl	$1, %ebp
.L3:
	movdqa	128(%rsp), %xmm4
	pand	.LC2(%rip), %xmm4
	movdqa	%xmm4, %xmm0
	movdqa	%xmm4, %xmm1
	movaps	%xmm4, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L85
.L44:
	movdqa	48(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L43
	movdqa	.LC4(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L5
	pxor	%xmm1, %xmm1
	movdqa	128(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L6
.L5:
	cmpl	$1, %ebp
	jle	.L7
.L8:
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L11
	movdqa	.LC6(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L11
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	.LC10(%rip), %xmm2
	je	.L58
.L19:
	movdqa	%xmm2, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L74
	movdqa	128(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
.L24:
	movdqa	.LC7(%rip), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L75
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__ieee754_logf128@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
.L28:
	movdqa	(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L29
	movdqa	(%rsp), %xmm0
	movdqa	32(%rsp), %xmm4
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm4, (%rsp)
.L29:
	movdqa	.LC13(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L76
	movdqa	(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	pxor	%xmm1, %xmm1
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L33
	pxor	%xmm4, %xmm4
	movaps	%xmm4, (%rsp)
.L33:
	movdqa	128(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 48(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L84:
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L50
	pxor	%xmm1, %xmm1
	movdqa	112(%rsp), %xmm0
	movl	$3, %ebp
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L86
.L2:
	movdqa	128(%rsp), %xmm6
	pand	.LC2(%rip), %xmm6
	movdqa	%xmm6, %xmm0
	movdqa	%xmm6, %xmm1
	movaps	%xmm6, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L67
	movdqa	.LC3(%rip), %xmm6
	movaps	%xmm6, 48(%rsp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	128(%rsp), %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L87
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L72
	movdqa	128(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	128(%rsp), %xmm1
	call	__divtf3@PLT
	movaps	%xmm0, 16(%rsp)
.L16:
	movdqa	16(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L40
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L40:
	movdqa	48(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L10
	movdqa	48(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L10:
	movdqa	16(%rsp), %xmm7
	movq	%rbx, %rax
	movdqa	48(%rsp), %xmm2
	movaps	%xmm7, (%rbx)
	movaps	%xmm2, 16(%rbx)
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	pxor	%xmm0, %xmm0
	movdqa	112(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC5(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 48(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$4, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L87:
	movdqa	112(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1, %ebp
	jle	.L7
	movdqa	128(%rsp), %xmm6
	cmpl	$2, %ebp
	movaps	%xmm6, 48(%rsp)
	je	.L10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$2, %ebp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L58:
	movdqa	%xmm2, %xmm1
	movdqa	32(%rsp), %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jns	.L19
	movdqa	112(%rsp), %xmm1
	movdqa	.LC8(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC11(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L73
	movdqa	(%rsp), %xmm0
	movdqa	32(%rsp), %xmm6
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm6, (%rsp)
.L22:
	movdqa	(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm4
	movaps	%xmm0, (%rsp)
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm0
.L81:
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L72:
	movdqa	.LC8(%rip), %xmm1
	movdqa	128(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_hypotf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	112(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__divtf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%ebp, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L74:
	pxor	%xmm6, %xmm6
	movaps	%xmm6, 16(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L75:
	movdqa	.LC12(%rip), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__divtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L76:
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L22
	movdqa	.LC14(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L37
	movdqa	.LC8(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L37
	movdqa	(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%ebp, %ebp
	jne	.L43
	movdqa	.LC1(%rip), %xmm4
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm4, 48(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L37:
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__x2y2m1f128@PLT
	pxor	.LC15(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L73:
	movdqa	.LC13(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L22
	pxor	%xmm6, %xmm6
	movaps	%xmm6, (%rsp)
	jmp	.L33
.L85:
	pxor	%xmm0, %xmm0
	movdqa	112(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	.LC1(%rip), %xmm6
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm6, 48(%rsp)
	jmp	.L10
.L67:
	movdqa	.LC1(%rip), %xmm6
	movaps	%xmm6, 16(%rsp)
	movaps	%xmm6, 48(%rsp)
	jmp	.L10
	.size	__catanhf128, .-__catanhf128
	.weak	catanhf128
	.set	catanhf128,__catanhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC5:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1081278464
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1058996224
	.align 16
.LC11:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC12:
	.long	0
	.long	0
	.long	0
	.long	1073807360
	.align 16
.LC13:
	.long	0
	.long	0
	.long	0
	.long	1066270720
	.align 16
.LC14:
	.long	0
	.long	0
	.long	0
	.long	1073643520
	.align 16
.LC15:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
