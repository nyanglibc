	.text
	.p2align 4,,15
	.globl	__casinh
	.type	__casinh, @function
__casinh:
	movq	.LC3(%rip), %xmm4
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm3
	andpd	%xmm4, %xmm2
	andpd	%xmm4, %xmm3
	ucomisd	%xmm2, %xmm2
	jp	.L2
	movsd	.LC4(%rip), %xmm5
	ucomisd	%xmm5, %xmm2
	jbe	.L37
	ucomisd	%xmm3, %xmm3
	jp	.L32
	ucomisd	%xmm5, %xmm3
	ja	.L38
.L21:
	andpd	.LC7(%rip), %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movsd	.LC5(%rip), %xmm6
	ucomisd	%xmm6, %xmm2
	jnb	.L4
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L4
	jne	.L4
	ucomisd	%xmm3, %xmm3
	jp	.L34
	ucomisd	%xmm5, %xmm3
	ja	.L17
	ucomisd	%xmm6, %xmm3
	jnb	.L18
	ucomisd	%xmm2, %xmm1
	jp	.L18
	jne	.L18
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	%xmm3, %xmm3
	jp	.L34
	ucomisd	%xmm5, %xmm3
	ja	.L17
.L18:
	xorl	%edi, %edi
	jmp	__kernel_casinh@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm3, %xmm3
	jp	.L32
	ucomisd	.LC4(%rip), %xmm3
	jbe	.L39
	andpd	.LC7(%rip), %xmm0
	orpd	.LC8(%rip), %xmm0
.L32:
	movsd	.LC2(%rip), %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	.LC7(%rip), %xmm2
	movsd	.LC1(%rip), %xmm3
	andpd	%xmm2, %xmm0
	orpd	.LC8(%rip), %xmm0
.L16:
	movapd	%xmm1, %xmm7
	andpd	%xmm2, %xmm7
	andnpd	%xmm3, %xmm2
	orpd	%xmm7, %xmm2
	movapd	%xmm2, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movsd	.LC2(%rip), %xmm0
	movapd	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	.LC7(%rip), %xmm2
	movsd	.LC0(%rip), %xmm3
	andpd	%xmm2, %xmm0
	orpd	.LC8(%rip), %xmm0
	jmp	.L16
.L39:
	ucomisd	.LC5(%rip), %xmm3
	jnb	.L32
	ucomisd	.LC6(%rip), %xmm1
	jp	.L32
	je	.L21
	jmp	.L32
	.size	__casinh, .-__casinh
	.weak	casinhf32x
	.set	casinhf32x,__casinh
	.weak	casinhf64
	.set	casinhf64,__casinh
	.weak	casinh
	.set	casinh,__casinh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1413754136
	.long	1072243195
	.align 8
.LC1:
	.long	1413754136
	.long	1073291771
	.align 8
.LC2:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.align 8
.LC5:
	.long	0
	.long	1048576
	.align 8
.LC6:
	.long	0
	.long	0
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC8:
	.long	0
	.long	2146435072
	.long	0
	.long	0
