	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__csqrtf
	.type	__csqrtf, @function
__csqrtf:
	pushq	%rbx
	subq	$80, %rsp
	movq	%xmm0, 72(%rsp)
	movss	.LC2(%rip), %xmm3
	movss	72(%rsp), %xmm0
	movaps	%xmm0, %xmm5
	movss	76(%rsp), %xmm6
	movaps	%xmm6, %xmm1
	andps	%xmm3, %xmm5
	andps	%xmm3, %xmm1
	ucomiss	%xmm5, %xmm5
	jp	.L2
	ucomiss	.LC3(%rip), %xmm5
	jbe	.L102
	ucomiss	%xmm1, %xmm1
	jp	.L103
	ucomiss	.LC3(%rip), %xmm1
	jbe	.L104
.L8:
	movaps	%xmm6, %xmm3
	movss	.LC12(%rip), %xmm1
.L1:
	movss	%xmm1, 64(%rsp)
	movss	%xmm3, 68(%rsp)
	movq	64(%rsp), %xmm0
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	ucomiss	.LC4(%rip), %xmm5
	movaps	%xmm0, %xmm2
	movaps	%xmm6, %xmm4
	jnb	.L4
	pxor	%xmm7, %xmm7
	ucomiss	%xmm7, %xmm0
	movss	%xmm7, 12(%rsp)
	jp	.L7
	je	.L5
.L7:
	ucomiss	%xmm1, %xmm1
	jp	.L100
	ucomiss	.LC3(%rip), %xmm1
	ja	.L8
	ucomiss	.LC4(%rip), %xmm1
	jnb	.L48
	ucomiss	12(%rsp), %xmm6
	jp	.L48
	jne	.L48
.L55:
	movss	12(%rsp), %xmm7
	ucomiss	%xmm0, %xmm7
	ja	.L105
	andps	.LC6(%rip), %xmm6
	sqrtss	%xmm0, %xmm1
	andps	%xmm3, %xmm1
	movaps	%xmm6, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	ucomiss	%xmm1, %xmm1
	jp	.L100
	ucomiss	.LC3(%rip), %xmm1
	ja	.L8
	ucomiss	.LC4(%rip), %xmm1
	pxor	%xmm7, %xmm7
	movss	%xmm7, 12(%rsp)
	jnb	.L48
	ucomiss	%xmm7, %xmm6
	jp	.L48
	je	.L55
.L48:
	ucomiss	.LC9(%rip), %xmm5
	ja	.L106
	ucomiss	.LC9(%rip), %xmm1
	jbe	.L87
	ucomiss	.LC10(%rip), %xmm5
	jnb	.L107
	movss	12(%rsp), %xmm7
	movss	%xmm7, 32(%rsp)
.L28:
	movaps	%xmm6, %xmm0
	movl	$-2, %edi
	movaps	%xmm3, 16(%rsp)
	call	__scalbnf@PLT
	movaps	%xmm0, %xmm4
	movaps	16(%rsp), %xmm3
	movss	32(%rsp), %xmm2
.L23:
	movaps	%xmm4, %xmm1
	movaps	%xmm2, %xmm0
	movaps	%xmm3, 48(%rsp)
	movss	%xmm4, 32(%rsp)
	movss	%xmm2, 16(%rsp)
	call	__ieee754_hypotf@PLT
	movss	16(%rsp), %xmm2
	ucomiss	12(%rsp), %xmm2
	movss	32(%rsp), %xmm4
	movaps	%xmm4, %xmm5
	movaps	48(%rsp), %xmm3
	andps	%xmm3, %xmm5
	jbe	.L108
	addss	%xmm0, %xmm2
	movss	.LC8(%rip), %xmm0
	mulss	%xmm0, %xmm2
	sqrtss	%xmm2, %xmm1
	movss	.LC11(%rip), %xmm2
	ucomiss	%xmm5, %xmm2
	ja	.L109
	movaps	%xmm4, %xmm2
	movl	$1, %ebx
	divss	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
.L35:
	movaps	%xmm1, %xmm0
	movl	%ebx, %edi
	movss	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movss	%xmm2, 16(%rsp)
	call	__scalbnf@PLT
	movss	16(%rsp), %xmm2
	movl	%ebx, %edi
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	call	__scalbnf@PLT
	movaps	48(%rsp), %xmm3
	movaps	%xmm0, %xmm2
	movss	32(%rsp), %xmm4
	movss	12(%rsp), %xmm1
.L34:
	movaps	%xmm1, %xmm5
	movss	.LC4(%rip), %xmm0
	andps	%xmm3, %xmm5
	ucomiss	%xmm5, %xmm0
	jbe	.L36
	movaps	%xmm1, %xmm5
	mulss	%xmm1, %xmm5
.L36:
	movaps	%xmm2, %xmm5
	andps	%xmm3, %xmm5
	ucomiss	%xmm5, %xmm0
	jbe	.L38
	movaps	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
.L38:
	movaps	%xmm4, %xmm7
	andps	%xmm2, %xmm3
	andps	.LC6(%rip), %xmm7
	orps	%xmm7, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L104:
	ucomiss	.LC4(%rip), %xmm1
	jb	.L110
	pxor	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	ja	.L60
.L13:
	andps	.LC6(%rip), %xmm6
	movaps	%xmm6, %xmm3
.L45:
	movaps	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	ucomiss	%xmm1, %xmm1
	jp	.L100
	ucomiss	.LC3(%rip), %xmm1
	ja	.L8
	ucomiss	.LC4(%rip), %xmm1
	jnb	.L51
	ucomiss	12(%rsp), %xmm6
	jp	.L51
	je	.L55
	.p2align 4,,10
	.p2align 3
.L51:
	ucomiss	.LC7(%rip), %xmm1
	jb	.L85
	mulss	.LC8(%rip), %xmm1
	sqrtss	%xmm1, %xmm1
.L20:
	movaps	%xmm6, %xmm4
	andps	%xmm1, %xmm3
	andps	.LC6(%rip), %xmm4
	orps	%xmm4, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L110:
	pxor	%xmm7, %xmm7
	ucomiss	%xmm7, %xmm6
	movss	%xmm7, 12(%rsp)
	jp	.L10
	je	.L111
.L10:
	movss	12(%rsp), %xmm4
	ucomiss	%xmm0, %xmm4
	jbe	.L13
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L87:
	movss	.LC7(%rip), %xmm7
	ucomiss	%xmm5, %xmm7
	jbe	.L90
	ucomiss	%xmm1, %xmm7
	ja	.L112
.L90:
	xorl	%ebx, %ebx
.L29:
	movaps	%xmm4, %xmm1
	movaps	%xmm2, %xmm0
	movaps	%xmm3, 48(%rsp)
	movss	%xmm4, 32(%rsp)
	movss	%xmm2, 16(%rsp)
	call	__ieee754_hypotf@PLT
	movss	16(%rsp), %xmm2
	ucomiss	12(%rsp), %xmm2
	movss	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm3
	jbe	.L113
	addss	%xmm0, %xmm2
	movss	.LC8(%rip), %xmm0
	mulss	%xmm0, %xmm2
	sqrtss	%xmm2, %xmm1
	movaps	%xmm4, %xmm2
	divss	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
.L44:
	testl	%ebx, %ebx
	je	.L34
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L106:
	movl	$-2, %edi
	movss	%xmm6, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	call	__scalbnf@PLT
	movss	%xmm0, 16(%rsp)
	movl	$-2, %edi
	movss	32(%rsp), %xmm6
	movaps	%xmm6, %xmm0
	call	__scalbnf@PLT
	movss	16(%rsp), %xmm2
	movaps	%xmm0, %xmm4
	movaps	48(%rsp), %xmm3
	jmp	.L23
.L113:
	subss	%xmm2, %xmm0
	movaps	%xmm4, %xmm1
	movaps	%xmm0, %xmm2
	movss	.LC8(%rip), %xmm0
	mulss	%xmm0, %xmm2
	sqrtss	%xmm2, %xmm2
	divss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	andps	%xmm3, %xmm1
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm1, %xmm1
	jp	.L100
	ucomiss	.LC3(%rip), %xmm1
	ja	.L8
.L100:
	movss	.LC0(%rip), %xmm1
	movaps	%xmm1, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L108:
	subss	%xmm2, %xmm0
	movss	.LC11(%rip), %xmm1
	ucomiss	%xmm5, %xmm1
	movaps	%xmm0, %xmm2
	movss	.LC8(%rip), %xmm0
	mulss	%xmm0, %xmm2
	sqrtss	%xmm2, %xmm2
	ja	.L114
	movaps	%xmm4, %xmm1
	movl	$1, %ebx
	divss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	andps	%xmm3, %xmm1
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L107:
	movl	$-2, %edi
	movss	%xmm6, 16(%rsp)
	movaps	%xmm3, 48(%rsp)
	call	__scalbnf@PLT
	movss	%xmm0, 32(%rsp)
	movss	16(%rsp), %xmm6
	movaps	48(%rsp), %xmm3
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$24, %edi
	movss	%xmm6, 32(%rsp)
	movl	$-12, %ebx
	movaps	%xmm3, 48(%rsp)
	call	__scalbnf@PLT
	movss	%xmm0, 16(%rsp)
	movl	$24, %edi
	movss	32(%rsp), %xmm6
	movaps	%xmm6, %xmm0
	call	__scalbnf@PLT
	movss	16(%rsp), %xmm2
	movaps	%xmm0, %xmm4
	movaps	48(%rsp), %xmm3
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L85:
	addss	%xmm1, %xmm1
	sqrtss	%xmm1, %xmm1
	mulss	.LC8(%rip), %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L109:
	movaps	%xmm4, %xmm2
	movl	$1, %edi
	movaps	%xmm1, %xmm0
	movss	%xmm4, 16(%rsp)
	divss	%xmm1, %xmm2
	movaps	%xmm3, 32(%rsp)
	movss	%xmm2, 12(%rsp)
	call	__scalbnf@PLT
	movss	12(%rsp), %xmm2
	movaps	%xmm0, %xmm1
	movss	16(%rsp), %xmm4
	movaps	32(%rsp), %xmm3
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L105:
	movss	.LC6(%rip), %xmm2
	movaps	%xmm0, %xmm1
	movaps	%xmm6, %xmm7
	xorps	%xmm2, %xmm1
	andps	%xmm2, %xmm7
	sqrtss	%xmm1, %xmm4
	andps	%xmm4, %xmm3
	pxor	%xmm1, %xmm1
	orps	%xmm7, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L103:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.L63
	movss	.LC0(%rip), %xmm3
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L114:
	movaps	%xmm4, %xmm1
	movl	$1, %edi
	movaps	%xmm2, %xmm0
	movss	%xmm4, 32(%rsp)
	divss	%xmm2, %xmm1
	movaps	%xmm3, 16(%rsp)
	andps	%xmm3, %xmm1
	movss	%xmm1, 12(%rsp)
	call	__scalbnf@PLT
	movss	12(%rsp), %xmm1
	movaps	%xmm0, %xmm2
	movaps	16(%rsp), %xmm3
	movss	32(%rsp), %xmm4
	jmp	.L34
.L63:
	movss	.LC0(%rip), %xmm1
.L12:
	andps	.LC6(%rip), %xmm6
	orps	.LC5(%rip), %xmm6
	movaps	%xmm6, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L111:
	ucomiss	%xmm0, %xmm7
	jbe	.L13
.L60:
	pxor	%xmm1, %xmm1
	jmp	.L12
	.size	__csqrtf, .-__csqrtf
	.weak	csqrtf32
	.set	csqrtf32,__csqrtf
	.weak	csqrtf
	.set	csqrtf,__csqrtf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095039
	.align 4
.LC4:
	.long	8388608
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2139095040
	.long	0
	.long	0
	.long	0
	.align 16
.LC6:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	16777216
	.align 4
.LC8:
	.long	1056964608
	.align 4
.LC9:
	.long	2122317823
	.align 4
.LC10:
	.long	25165824
	.align 4
.LC11:
	.long	1065353216
	.align 4
.LC12:
	.long	2139095040
