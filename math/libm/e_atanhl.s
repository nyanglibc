	.text
	.p2align 4,,15
	.globl	__ieee754_atanhl
	.type	__ieee754_atanhl, @function
__ieee754_atanhl:
	pushq	%rbx
	subq	$16, %rsp
	movq	32(%rsp), %rdx
	movq	40(%rsp), %rbx
	movq	%rdx, %rax
	movl	%ebx, %ecx
	shrq	$32, %rax
	andw	$32767, %cx
	andl	$2147483647, %eax
	orl	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	orl	%edx, %eax
	movswl	%cx, %edx
	shrl	$31, %eax
	addl	%edx, %eax
	cmpl	$16383, %eax
	ja	.L15
	cmpw	$16383, %cx
	je	.L16
	cmpw	$16350, %cx
	jle	.L17
	fldt	32(%rsp)
	cmpw	$16381, %cx
	fstpt	(%rsp)
	movw	%cx, 8(%rsp)
	fldt	(%rsp)
	jg	.L7
	fld	%st(0)
	fadd	%st(1), %st
	fld	%st(1)
	fmul	%st(1), %st
	fld1
	fsubp	%st, %st(3)
	fdivp	%st, %st(2)
	faddp	%st, %st(1)
.L13:
	subq	$16, %rsp
	movswl	%bx, %ebx
	fstpt	(%rsp)
	call	__log1pl@PLT
	cmpl	$32767, %ebx
	fmuls	.LC4(%rip)
	popq	%rax
	popq	%rdx
	jbe	.L1
	addq	$16, %rsp
	fchs
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	fldt	32(%rsp)
	fsub	%st(0), %st
	fdiv	%st(0), %st
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	fldt	.LC1(%rip)
	fldt	32(%rsp)
	fadd	%st, %st(1)
	fstp	%st(1)
	fld	%st(0)
	fabs
	fldt	.LC2(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L11
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	fldt	32(%rsp)
	fdivs	.LC0(%rip)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fld	%st(0)
	fadd	%st(1), %st
	fld1
	fsubp	%st, %st(2)
	fdivp	%st, %st(1)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	fstp	%st(0)
	fldt	32(%rsp)
	jmp	.L1
	.size	__ieee754_atanhl, .-__ieee754_atanhl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	1496818881
	.long	2928804903
	.long	32660
	.long	0
	.align 16
.LC2:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	1056964608
