	.text
#APP
	.symver __ieee754_acoshl,__acoshl_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_acoshl
	.type	__ieee754_acoshl, @function
__ieee754_acoshl:
	subq	$24, %rsp
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	movq	8(%rsp), %rax
	cmpw	$16382, %ax
	jg	.L2
	fld	%st(0)
	fsubp	%st, %st(1)
	fdiv	%st(0), %st
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	cmpw	$16412, %ax
	movswl	%ax, %edx
	jg	.L10
	movq	(%rsp), %rcx
	movq	%rcx, %rax
	shrq	$32, %rax
	addl	$-2147483648, %eax
	orl	%ecx, %eax
	leal	-16383(%rdx), %ecx
	orl	%ecx, %eax
	je	.L7
	cmpl	$16384, %edx
	ja	.L11
	fld1
	fldt	(%rsp)
	fsubp	%st, %st(1)
	fld	%st(0)
	fld	%st(1)
	fadd	%st(2), %st
	fld	%st(2)
	fmulp	%st, %st(3)
	faddp	%st, %st(2)
	fxch	%st(1)
	fsqrt
	faddp	%st, %st(1)
	fstpt	32(%rsp)
	addq	$24, %rsp
	jmp	__log1pl@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$32767, %edx
	jne	.L5
	fldt	(%rsp)
	addq	$24, %rsp
	fadd	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fldz
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	pushq	8(%rsp)
	pushq	8(%rsp)
	call	__ieee754_logl@PLT
	fldln2
	popq	%rax
	faddp	%st, %st(1)
	popq	%rdx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	fldt	(%rsp)
	fld	%st(0)
	fmul	%st(0), %st
	fld1
	fsubr	%st, %st(1)
	fxch	%st(1)
	fsqrt
	fld	%st(2)
	fadd	%st(3), %st
	fxch	%st(1)
	faddp	%st, %st(3)
	fxch	%st(2)
	fdivrp	%st, %st(1)
	fsubrp	%st, %st(1)
	fstpt	32(%rsp)
	addq	$24, %rsp
	jmp	__ieee754_logl@PLT
	.size	__ieee754_acoshl, .-__ieee754_acoshl
