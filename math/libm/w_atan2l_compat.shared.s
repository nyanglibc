	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__atan2l
	.type	__atan2l, @function
__atan2l:
	fldt	8(%rsp)
	movl	$0, %edx
	fldt	24(%rsp)
	fldz
	fucomi	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L19
	fucomip	%st(2), %st
	setnp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	jne	.L17
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L19:
	fstp	%st(0)
.L2:
	fld	%st(0)
	fld	%st(2)
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	fldz
	movl	$0, %edx
	fucomi	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L20
	fucomip	%st(3), %st
	fstp	%st(2)
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L18
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	fstp	%st(0)
	fstp	%st(1)
	fstp	%st(1)
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2
	fstp	%st(0)
	fstp	%st(0)
	movl	$203, %edi
	jmp	__kernel_standard_l@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	fabs
	fldt	.LC1(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	ret
	.size	__atan2l, .-__atan2l
	.weak	atan2f64x
	.set	atan2f64x,__atan2l
	.weak	atan2l
	.set	atan2l,__atan2l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
