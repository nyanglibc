 .section .rodata.cst8,"aM",@progbits,8
 .p2align 3
 .type one,@object
one: .double 1.0
 .size one,.-one;
 .type p2,@object
p2: .byte 0, 0, 0, 0, 0, 0, 0x10, 0x40
 .size p2,.-p2;
 .type p63,@object
p63: .byte 0, 0, 0, 0, 0, 0, 0xe0, 0x43
 .size p63,.-p63;
 .type p64,@object
p64: .byte 0, 0, 0, 0, 0, 0, 0xf0, 0x43
 .size p64,.-p64;
 .type p78,@object
p78: .byte 0, 0, 0, 0, 0, 0, 0xd0, 0x44
 .size p78,.-p78;
 .type pm79,@object
pm79: .byte 0, 0, 0, 0, 0, 0, 0, 0x3b
 .size pm79,.-pm79;
 .section .rodata.cst16,"aM",@progbits,16
 .p2align 3
 .type infinity,@object
inf_zero:
infinity:
 .byte 0, 0, 0, 0, 0, 0, 0xf0, 0x7f
 .size infinity,.-infinity;
 .type zero,@object
zero: .double 0.0
 .size zero,.-zero;
 .type minf_mzero,@object
minf_mzero:
minfinity:
 .byte 0, 0, 0, 0, 0, 0, 0xf0, 0xff
mzero:
 .byte 0, 0, 0, 0, 0, 0, 0, 0x80
 .size minf_mzero,.-minf_mzero;
.section .rodata.cst16,"aM",@progbits,16
.p2align 4
.type ldbl_min,@object
ldbl_min:
.byte 0, 0, 0, 0, 0, 0, 0, 0x80, 0x1, 0
.byte 0, 0, 0, 0, 0, 0
.size ldbl_min, .-ldbl_min
 .text
.globl __ieee754_powl
.type __ieee754_powl,@function
.align 1<<4
__ieee754_powl:
 fldt 24(%rsp)
 fxam
 fnstsw
 movb %ah, %dl
 andb $0x45, %ah
 cmpb $0x40, %ah
 je 11f
 cmpb $0x05, %ah
 je 12f
 cmpb $0x01, %ah
 je 30f
 fldt 8(%rsp)
 fxam
 fnstsw
 movb %ah, %dh
 andb $0x45, %ah
 cmpb $0x40, %ah
 je 20f
 cmpb $0x05, %ah
 je 15f
 cmpb $0x01, %ah
 je 31f
 fxch
 fldl p63(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st
 fstp %st(0)
 jnc 2f
 fld %st
 fistpll -8(%rsp)
 fildll -8(%rsp)
 fucomip %st(1),%st
 je 9f
 fldl pm79(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st
 fstp %st(0)
 jnc 3f
 fstp %st(0)
 fldl pm79(%rip)
 testb $2, %dl
 jnz 3f
 fchs
 jmp 3f
9:
 fldl p2(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st
 fstp %st(0)
 jnc 3f
 mov -8(%rsp),%eax
 mov -4(%rsp),%edx
 orl $0, %edx
 fstp %st(0)
 jns 4f
 fdivrl one(%rip)
 negl %eax
 adcl $0, %edx
 negl %edx
4: fldl one(%rip)
 fxch
 testb $1, %al
 jnz 6f
 fabs
6: shrdl $1, %edx, %eax
 jnc 5f
 fxch
 fabs
 fmul %st(1)
 fxch
5: fld %st
 fabs
 fmulp
 shrl $1, %edx
 movl %eax, %ecx
 orl %edx, %ecx
 jnz 6b
 fstp %st(0)
 fldt ldbl_min(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st(0)
 fstp %st(0)
 jnc 6464f
 fld %st(0)
 fmul %st(0)
 fstp %st(0)
6464:
 ret
30: fldt 8(%rsp)
 fldl one(%rip)
 fucomip %st(1),%st
 je 32f
31:
 faddp
 ret
32: jc 31b
 testb $0x40, 31(%rsp)
 jz 31b
 fstp %st(1)
 ret
 .align 1<<4
2:
 fldl p78(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st
 fstp %st(0)
 jc 3f
 fstp %st(0)
 fldl p78(%rip)
 testb $2, %dl
 jz 3f
 fchs
 .align 1<<4
3:
 subq $40, %rsp
 fstpt 16(%rsp)
 fstpt (%rsp)
 call __powl_helper
 addq $40, %rsp
 ret
 .align 1<<4
11: fstp %st(0)
 fldt 8(%rsp)
 fxam
 fnstsw
 andb $0x45, %ah
 cmpb $0x01, %ah
 je 112f
111: fstp %st(0)
 fldl one(%rip)
 ret
112: testb $0x40, 15(%rsp)
 jnz 111b
 fadd %st(0)
 ret
 .align 1<<4
12: fstp %st(0)
 fldl one(%rip)
 fldt 8(%rsp)
 fabs
 fucompp
 fnstsw
 andb $0x45, %ah
 cmpb $0x45, %ah
 je 13f
 cmpb $0x40, %ah
 je 14f
 shlb $1, %ah
 xorb %ah, %dl
 andl $2, %edx
 lea inf_zero(%rip),%rcx
 fldl (%rcx, %rdx, 4)
 ret
 .align 1<<4
14: fldl one(%rip)
 ret
 .align 1<<4
13: fldt 8(%rsp)
 fadd %st(0)
 ret
 .align 1<<4
15: fstp %st(0)
 testb $2, %dh
 jz 16f
 fldl p64(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st
 fstp %st(0)
 jnc 16f
 fldl p63(%rip)
 fxch
 fprem
 fstp %st(1)
 fld %st
 fistpll -8(%rsp)
 fildll -8(%rsp)
 fucomip %st(1),%st
 ffreep %st
 jne 17f
 mov -8(%rsp), %eax
 mov -4(%rsp), %edx
 andb $1, %al
 jz 18f
 shrl $31, %edx
 lea minf_mzero(%rip),%rcx
 fldl (%rcx, %rdx, 8)
 ret
 .align 1<<4
16: fcompl zero(%rip)
 fnstsw
 shrl $5, %eax
 andl $8, %eax
 lea inf_zero(%rip),%rcx
 fldl (%rcx, %rax, 1)
 ret
 .align 1<<4
17: shll $30, %edx
18: shrl $31, %edx
 lea inf_zero(%rip),%rcx
 fldl (%rcx, %rdx, 8)
 ret
 .align 1<<4
20: fstp %st(0)
 testb $2, %dl
 jz 21f
 testb $2, %dh
 jz 25f
 fldl p64(%rip)
 fld %st(1)
 fabs
 fcomip %st(1), %st
 fstp %st(0)
 jnc 25f
 fldl p63(%rip)
 fxch
 fprem
 fstp %st(1)
 fld %st
 fistpll -8(%rsp)
 fildll -8(%rsp)
 fucomip %st(1),%st
 ffreep %st
 jne 26f
 mov -8(%rsp),%eax
 mov -4(%rsp),%edx
 andb $1, %al
 jz 27f
 fldl one(%rip)
 fdivl zero(%rip)
 fchs
 ret
25: fstp %st(0)
26:
27:
 fldl one(%rip)
 fdivl zero(%rip)
 ret
 .align 1<<4
21: testb $2, %dh
 jz 22f
 fldl p64(%rip)
 fxch
 fcomi %st(1), %st
 fstp %st(1)
 jnc 22f
 fldl p63(%rip)
 fxch
 fprem
 fstp %st(1)
 fld %st
 fistpll -8(%rsp)
 fildll -8(%rsp)
 fucomip %st(1),%st
 ffreep %st
 jne 23f
 mov -8(%rsp),%eax
 mov -4(%rsp),%edx
 andb $1, %al
 jz 24f
 fldl mzero(%rip)
 ret
22: fstp %st(0)
23:
24: fldl zero(%rip)
 ret
.size __ieee754_powl,.-__ieee754_powl

