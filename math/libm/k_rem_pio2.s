	.text
	.p2align 4,,15
	.globl	__kernel_rem_pio2
	.type	__kernel_rem_pio2, @function
__kernel_rem_pio2:
	pushq	%r15
	pushq	%r14
	leaq	init_jk(%rip), %rax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbp
	pushq	%rbx
	leal	-1(%rcx), %r12d
	pxor	%xmm3, %xmm3
	subq	$664, %rsp
	movq	%rsi, 64(%rsp)
	movl	%edx, %esi
	movslq	%r8d, %rdx
	leal	-3(%rsi), %edi
	movl	(%rax,%rdx,4), %ebp
	movl	%edx, 76(%rsp)
	movl	$715827883, %edx
	movl	$0, %r8d
	movl	%edi, %eax
	sarl	$31, %edi
	imull	%edx
	sarl	$2, %edx
	subl	%edi, %edx
	cmovns	%edx, %r8d
	leal	1(%r8), %eax
	leal	0(,%rax,4), %edx
	subl	%edx, %eax
	movl	%ebp, %edx
	leal	(%rsi,%rax,8), %r13d
	movl	%r8d, %eax
	subl	%r12d, %eax
	addl	%r12d, %edx
	js	.L2
	cltq
	movslq	%edx, %rdx
	leaq	1(%rax), %rdi
	movq	%rax, %rsi
	negq	%rsi
	pxor	%xmm3, %xmm3
	addq	%rdi, %rdx
	leaq	176(%rsp,%rsi,8), %rsi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L130:
	addq	$1, %rdi
.L4:
	testl	%eax, %eax
	pxor	%xmm0, %xmm0
	js	.L3
	pxor	%xmm0, %xmm0
	cvtsi2sd	(%r9,%rax,4), %xmm0
.L3:
	cmpq	%rdx, %rdi
	movsd	%xmm0, (%rsi,%rax,8)
	movq	%rdi, %rax
	jne	.L130
.L2:
	testl	%ebp, %ebp
	js	.L5
	movl	%r12d, %eax
	leaq	168(%rsp), %r11
	leaq	496(%rsp), %rdi
	salq	$3, %rax
	leal	0(%rbp,%rcx), %r10d
	leaq	176(%rsp), %rbx
	movl	%r12d, %esi
	subq	%rax, %r11
	movq	%r15, %r14
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%r12d, %r12d
	pxor	%xmm1, %xmm1
	js	.L9
	movslq	%esi, %rcx
	pxor	%xmm1, %xmm1
	salq	$3, %rcx
	movq	%r14, %rdx
	leaq	(%rbx,%rcx), %rax
	addq	%r11, %rcx
	.p2align 4,,10
	.p2align 3
.L7:
	movsd	(%rdx), %xmm0
	subq	$8, %rax
	addq	$8, %rdx
	mulsd	8(%rax), %xmm0
	cmpq	%rcx, %rax
	addsd	%xmm0, %xmm1
	jne	.L7
.L9:
	addl	$1, %esi
	movsd	%xmm1, (%rdi)
	addq	$8, %rdi
	cmpl	%r10d, %esi
	jne	.L6
.L5:
	leal	-2(%rbp), %eax
	leaq	96(%rsp), %rbx
	movslq	%r8d, %r8
	movq	%r15, %rdi
	leaq	(%r9,%r8,4), %r14
	movl	%ebp, 72(%rsp)
	cltq
	movq	%rbx, 8(%rsp)
	leaq	(%rbx,%rax,4), %rax
	movsd	.LC2(%rip), %xmm5
	movsd	.LC3(%rip), %xmm4
	movq	%rax, 56(%rsp)
	movl	%r12d, %eax
	leaq	8(%r15,%rax,8), %rbx
	leal	-1(%rbp), %eax
	movl	%ebp, %r15d
	movq	%rdi, %rbp
	cltq
	movq	%rax, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L10:
	movslq	%r15d, %r8
	testl	%r15d, %r15d
	movsd	496(%rsp,%r8,8), %xmm0
	jle	.L11
	leal	-1(%r15), %edx
	movq	8(%rsp), %rcx
	movslq	%edx, %rsi
	movl	%edx, %edx
	salq	$3, %rsi
	salq	$3, %rdx
	leaq	496(%rsp,%rsi), %rax
	leaq	488(%rsp,%rsi), %rsi
	subq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L12:
	movapd	%xmm0, %xmm1
	subq	$8, %rax
	addq	$4, %rcx
	mulsd	%xmm5, %xmm1
	cvttsd2si	%xmm1, %edx
	pxor	%xmm1, %xmm1
	cvtsi2sd	%edx, %xmm1
	movapd	%xmm1, %xmm2
	addsd	8(%rax), %xmm1
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %edx
	movapd	%xmm1, %xmm0
	movl	%edx, -4(%rcx)
	cmpq	%rsi, %rax
	jne	.L12
.L11:
	movl	%r13d, %edi
	movq	%r8, 40(%rsp)
	movsd	%xmm4, 32(%rsp)
	movsd	%xmm5, 24(%rsp)
	movsd	%xmm3, 16(%rsp)
	call	__scalbn@PLT
	movsd	.LC4(%rip), %xmm1
	movsd	.LC6(%rip), %xmm6
	movq	40(%rsp), %r8
	mulsd	%xmm0, %xmm1
	movsd	.LC5(%rip), %xmm5
	movsd	16(%rsp), %xmm3
	movsd	32(%rsp), %xmm4
	movapd	%xmm1, %xmm2
	movapd	%xmm1, %xmm7
	andpd	%xmm6, %xmm2
	ucomisd	%xmm2, %xmm5
	movsd	24(%rsp), %xmm5
	jbe	.L13
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm2, %xmm2
	movsd	.LC7(%rip), %xmm9
	andnpd	%xmm7, %xmm6
	cvtsi2sdq	%rax, %xmm2
	movapd	%xmm2, %xmm8
	cmpnlesd	%xmm1, %xmm8
	movapd	%xmm8, %xmm1
	andpd	%xmm9, %xmm1
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	orpd	%xmm6, %xmm1
.L13:
	mulsd	.LC8(%rip), %xmm1
	cmpl	$0, %r13d
	subsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvttsd2si	%xmm0, %eax
	cvtsi2sd	%eax, %xmm1
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	jle	.L14
	leal	-1(%r15), %edx
	movl	$24, %ecx
	subl	%r13d, %ecx
	movslq	%edx, %rdx
	movl	96(%rsp,%rdx,4), %r10d
	movl	%r10d, %esi
	sarl	%cl, %esi
	addl	%esi, %eax
	sall	%cl, %esi
	movl	$23, %ecx
	subl	%esi, %r10d
	subl	%r13d, %ecx
	movl	%r10d, 96(%rsp,%rdx,4)
	sarl	%cl, %r10d
.L15:
	testl	%r10d, %r10d
	jle	.L18
	addl	$1, %eax
	testl	%r15d, %r15d
	jle	.L74
.L70:
	movl	96(%rsp), %edx
	testl	%edx, %edx
	jne	.L75
	cmpl	$1, %r15d
	je	.L19
	movq	8(%rsp), %rdi
	movl	$1, %ecx
	addq	$4, %rdi
.L21:
	movl	(%rdi), %edx
	leal	1(%rcx), %esi
	testl	%edx, %edx
	jne	.L20
	movslq	%esi, %rcx
	addq	$4, %rdi
	cmpl	%ecx, %r15d
	jne	.L21
	.p2align 4,,10
	.p2align 3
.L19:
	testl	%r13d, %r13d
	jle	.L24
	cmpl	$1, %r13d
	je	.L25
	cmpl	$2, %r13d
	jne	.L24
	leal	-1(%r15), %ecx
	movslq	%ecx, %rcx
	andl	$4194303, 96(%rsp,%rcx,4)
.L24:
	cmpl	$2, %r10d
	je	.L131
.L18:
	ucomisd	%xmm3, %xmm1
	jp	.L27
	jne	.L27
	movl	72(%rsp), %r9d
	leal	-1(%r15), %edi
	cmpl	%edi, %r9d
	jg	.L29
	movq	8(%rsp), %rsi
	movslq	%edi, %r8
	leaq	0(,%r8,4), %rcx
	leaq	(%rsi,%rcx), %rdx
	leaq	92(%rsp,%rcx), %rsi
	movl	%r9d, %ecx
	notl	%ecx
	addl	%r15d, %ecx
	salq	$2, %rcx
	subq	%rcx, %rsi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L30:
	orl	(%rdx), %ecx
	subq	$4, %rdx
	cmpq	%rsi, %rdx
	jne	.L30
	testl	%ecx, %ecx
	jne	.L31
.L29:
	movq	48(%rsp), %rax
	movl	96(%rsp,%rax,4), %r9d
	testl	%r9d, %r9d
	jne	.L78
	movq	56(%rsp), %rax
	movl	$1, %edi
	.p2align 4,,10
	.p2align 3
.L33:
	subq	$4, %rax
	movl	4(%rax), %r8d
	addl	$1, %edi
	testl	%r8d, %r8d
	je	.L33
.L32:
	leal	1(%r15), %eax
	addl	%r15d, %edi
	cmpl	%edi, %eax
	jg	.L34
	movslq	%eax, %rcx
	notl	%r15d
	addl	%r12d, %eax
	leal	(%r15,%rdi), %r9d
	leal	-1(%rax), %r8d
	leaq	176(%rsp), %r15
	cltq
	leaq	1(%rcx), %rsi
	leaq	496(%rsp), %r10
	subq	%rcx, %rax
	leaq	(%r15,%rax,8), %r11
	addq	%rsi, %r9
	.p2align 4,,10
	.p2align 3
.L37:
	pxor	%xmm0, %xmm0
	testl	%r12d, %r12d
	cvtsi2sd	(%r14,%rcx,4), %xmm0
	movsd	%xmm0, (%r11,%rcx,8)
	js	.L79
	movslq	%r8d, %rax
	pxor	%xmm1, %xmm1
	leaq	(%r15,%rax,8), %rdx
	movq	%rbp, %rax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L132:
	movsd	(%rdx), %xmm0
	subq	$8, %rdx
.L36:
	mulsd	(%rax), %xmm0
	addq	$8, %rax
	cmpq	%rbx, %rax
	addsd	%xmm0, %xmm1
	jne	.L132
.L35:
	addl	$1, %r8d
	cmpq	%r9, %rsi
	movsd	%xmm1, (%r10,%rcx,8)
	movq	%rsi, %rcx
	je	.L34
	addq	$1, %rsi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%edi, %r15d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L79:
	pxor	%xmm1, %xmm1
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$1, %esi
	xorl	%ecx, %ecx
.L20:
	movl	$16777216, %edi
	subl	%edx, %edi
	cmpl	%esi, %r15d
	movl	%edi, 96(%rsp,%rcx,4)
	jle	.L77
	leal	-1(%r15), %ecx
	movq	8(%rsp), %r11
	movslq	%esi, %r9
	movl	96(%rsp,%r9,4), %edi
	subl	%esi, %ecx
	addq	%r9, %rcx
	leaq	(%r11,%r9,4), %rdx
	leaq	4(%r11,%rcx,4), %rsi
	movl	$16777215, %ecx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L133:
	movl	(%rdx), %edi
.L23:
	movl	%ecx, %r11d
	addq	$4, %rdx
	subl	%edi, %r11d
	movl	%r11d, -4(%rdx)
	cmpq	%rsi, %rdx
	jne	.L133
.L77:
	movl	$1, %edx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L14:
	jne	.L16
	leal	-1(%r15), %edx
	movslq	%edx, %rdx
	movl	96(%rsp,%rdx,4), %r10d
	sarl	$23, %r10d
	jmp	.L15
.L16:
	xorl	%r10d, %r10d
	ucomisd	.LC9(%rip), %xmm0
	jb	.L18
	addl	$1, %eax
	testl	%r15d, %r15d
	movl	$2, %r10d
	jg	.L70
	movsd	.LC7(%rip), %xmm7
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm1
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L131:
	movsd	.LC7(%rip), %xmm7
	testl	%edx, %edx
	subsd	%xmm1, %xmm7
	movapd	%xmm7, %xmm1
	je	.L18
	movsd	.LC7(%rip), %xmm0
	movl	%r13d, %edi
	movq	%r8, 88(%rsp)
	movl	%r10d, 84(%rsp)
	movsd	%xmm4, 40(%rsp)
	movl	%eax, 80(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm3, 24(%rsp)
	movsd	%xmm7, 16(%rsp)
	call	__scalbn@PLT
	movsd	16(%rsp), %xmm1
	movq	88(%rsp), %r8
	movl	84(%rsp), %r10d
	subsd	%xmm0, %xmm1
	movl	80(%rsp), %eax
	movsd	40(%rsp), %xmm4
	movsd	32(%rsp), %xmm5
	movsd	24(%rsp), %xmm3
	jmp	.L18
.L78:
	movl	$1, %edi
	jmp	.L32
.L25:
	leal	-1(%r15), %ecx
	movslq	%ecx, %rcx
	andl	$8388607, 96(%rsp,%rcx,4)
	jmp	.L24
.L31:
	movl	96(%rsp,%r8,4), %esi
	subl	$24, %r13d
	movl	%r15d, %edx
	movl	72(%rsp), %ebp
	movl	%edi, %r15d
	testl	%esi, %esi
	jne	.L38
	movq	8(%rsp), %rbx
	subl	$2, %edx
	movslq	%edx, %rdx
	leaq	(%rbx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L39:
	subq	$4, %rdx
	movl	4(%rdx), %ecx
	subl	$1, %r15d
	subl	$24, %r13d
	testl	%ecx, %ecx
	je	.L39
.L38:
	movsd	.LC7(%rip), %xmm0
	movl	%r13d, %edi
	movl	%r10d, 24(%rsp)
	movsd	%xmm3, 32(%rsp)
	movl	%eax, 16(%rsp)
	call	__scalbn@PLT
	testl	%r15d, %r15d
	movl	16(%rsp), %eax
	movl	24(%rsp), %r10d
	movsd	32(%rsp), %xmm3
	js	.L42
	movslq	%r15d, %r9
	movq	8(%rsp), %rdi
	leaq	496(%rsp), %rcx
	leaq	-1(%r9), %rsi
	movsd	.LC2(%rip), %xmm2
	movq	%r9, %rdx
	subq	%r9, %rsi
	.p2align 4,,10
	.p2align 3
.L43:
	pxor	%xmm1, %xmm1
	cvtsi2sd	(%rdi,%rdx,4), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm0
	movsd	%xmm1, (%rcx,%rdx,8)
	subq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L43
	leaq	0(,%r9,8), %rbx
	leaq	1(%r9), %r12
	leaq	336(%rsp), %r11
	movsd	.LC1(%rip), %xmm2
	leaq	PIo2(%rip), %rdi
	addq	%rbx, %rcx
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L44:
	testl	%ebp, %ebp
	movl	%r8d, %esi
	movl	$1, %edx
	movapd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	jns	.L46
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L134:
	cmpl	%esi, %edx
	jg	.L45
	movsd	(%rdi,%rdx,8), %xmm0
	addq	$1, %rdx
.L46:
	mulsd	-8(%rcx,%rdx,8), %xmm0
	cmpl	%edx, %ebp
	addsd	%xmm0, %xmm1
	jge	.L134
.L45:
	movsd	%xmm1, (%r11,%r8,8)
	addq	$1, %r8
	subq	$8, %rcx
	cmpq	%r12, %r8
	jne	.L44
	movl	76(%rsp), %edi
	cmpl	$2, %edi
	jg	.L49
	cmpl	$1, %edi
	jge	.L50
	testl	%edi, %edi
	jne	.L53
	leaq	328(%rsp,%rbx), %rdx
	salq	$3, %r9
	addq	%rbx, %r11
	subq	%r9, %rdx
.L59:
	addsd	(%r11), %xmm3
	subq	$8, %r11
	cmpq	%r11, %rdx
	jne	.L59
.L51:
	testl	%r10d, %r10d
	je	.L60
	xorpd	.LC10(%rip), %xmm3
.L60:
	movq	64(%rsp), %rbx
	movsd	%xmm3, (%rbx)
.L53:
	addq	$664, %rsp
	andl	$7, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L50:
	testl	%r15d, %r15d
	js	.L57
	movslq	%r15d, %rdx
	leaq	328(%rsp), %rcx
	salq	$3, %rdx
	leaq	336(%rsp,%rdx), %rdx
.L61:
	addsd	(%rdx), %xmm3
	subq	$8, %rdx
	cmpq	%rdx, %rcx
	jne	.L61
.L57:
	testl	%r10d, %r10d
	jne	.L135
	movq	64(%rsp), %rdi
	movsd	336(%rsp), %xmm0
	testl	%r15d, %r15d
	subsd	%xmm3, %xmm0
	movsd	%xmm3, (%rdi)
	jle	.L65
.L72:
	leaq	336(%rsp), %r11
	movl	$1, %edx
.L64:
	addsd	(%r11,%rdx,8), %xmm0
	addq	$1, %rdx
	cmpl	%edx, %r15d
	jge	.L64
	testl	%r10d, %r10d
	jne	.L136
.L65:
	movq	64(%rsp), %rdi
	movsd	%xmm0, 8(%rdi)
	jmp	.L53
.L74:
	xorl	%edx, %edx
	jmp	.L19
.L49:
	cmpl	$3, 76(%rsp)
	jne	.L53
	testl	%r15d, %r15d
	jle	.L55
	leal	-1(%r15), %edi
	leaq	336(%rsp), %r11
	leaq	328(%rsp), %rsi
	movslq	%r15d, %r8
	movslq	%edi, %rcx
	movl	%edi, %r9d
	salq	$3, %rcx
	salq	$3, %r9
	leaq	(%r11,%rcx), %rdx
	movsd	336(%rsp,%r8,8), %xmm0
	addq	%rsi, %rcx
	subq	%r9, %rcx
.L66:
	movsd	(%rdx), %xmm1
	subq	$8, %rdx
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm2, 8(%rdx)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rdx)
	cmpq	%rdx, %rcx
	movapd	%xmm2, %xmm0
	jne	.L66
	cmpl	$1, %r15d
	jle	.L55
	movslq	%edi, %rcx
	movsd	336(%rsp,%r8,8), %xmm0
	salq	$3, %rcx
	leal	-2(%r15), %r8d
	leaq	(%rsi,%rcx), %rdi
	leaq	(%r11,%rcx), %rdx
	salq	$3, %r8
	subq	%r8, %rdi
.L67:
	movsd	(%rdx), %xmm1
	subq	$8, %rdx
	movapd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	movsd	%xmm2, 8(%rdx)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rdx)
	cmpq	%rdx, %rdi
	movapd	%xmm2, %xmm0
	jne	.L67
	leaq	8(%rcx), %rdx
	addq	%rdx, %r11
	addq	%rsi, %rdx
	subq	%r8, %rdx
.L68:
	addsd	(%r11), %xmm3
	subq	$8, %r11
	cmpq	%r11, %rdx
	jne	.L68
.L55:
	testl	%r10d, %r10d
	movsd	336(%rsp), %xmm1
	movsd	344(%rsp), %xmm0
	je	.L137
	movq	.LC10(%rip), %xmm2
	movq	64(%rsp), %rbx
	xorpd	%xmm2, %xmm1
	xorpd	%xmm2, %xmm0
	xorpd	%xmm2, %xmm3
	movsd	%xmm1, (%rbx)
	movsd	%xmm0, 8(%rbx)
	movsd	%xmm3, 16(%rbx)
	jmp	.L53
.L27:
	movl	%r13d, %edi
	movapd	%xmm1, %xmm0
	negl	%edi
	movsd	%xmm3, 40(%rsp)
	movl	%r15d, 32(%rsp)
	movl	%eax, 24(%rsp)
	movl	%r10d, 16(%rsp)
	movq	%r8, %r15
	movl	72(%rsp), %ebp
	call	__scalbn@PLT
	movsd	.LC3(%rip), %xmm4
	movl	16(%rsp), %r10d
	movl	24(%rsp), %eax
	movl	32(%rsp), %edx
	ucomisd	%xmm4, %xmm0
	movsd	40(%rsp), %xmm3
	jb	.L121
	movsd	.LC2(%rip), %xmm2
	addl	$24, %r13d
	pxor	%xmm1, %xmm1
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %ecx
	cvtsi2sd	%ecx, %xmm1
	mulsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %esi
	movl	%esi, 96(%rsp,%r15,4)
	leal	1(%rdx), %r15d
	movslq	%r15d, %rdx
	movl	%ecx, 96(%rsp,%rdx,4)
	jmp	.L38
.L121:
	cvttsd2si	%xmm0, %ecx
	movl	%ecx, 96(%rsp,%r15,4)
	movl	%edx, %r15d
	jmp	.L38
.L135:
	movq	.LC10(%rip), %xmm1
	movq	64(%rsp), %rbx
	movapd	%xmm3, %xmm0
	testl	%r15d, %r15d
	xorpd	%xmm1, %xmm0
	movsd	%xmm0, (%rbx)
	movsd	336(%rsp), %xmm0
	subsd	%xmm3, %xmm0
	jg	.L72
.L63:
	xorpd	%xmm1, %xmm0
	jmp	.L65
.L137:
	movq	64(%rsp), %rbx
	movsd	%xmm1, (%rbx)
	movsd	%xmm0, 8(%rbx)
	movsd	%xmm3, 16(%rbx)
	jmp	.L53
.L42:
	movl	76(%rsp), %ebx
	cmpl	$2, %ebx
	jg	.L49
	cmpl	$1, %ebx
	jge	.L50
	testl	%ebx, %ebx
	je	.L51
	jmp	.L53
.L136:
	movq	.LC10(%rip), %xmm1
	jmp	.L63
	.size	__kernel_rem_pio2, .-__kernel_rem_pio2
	.section	.rodata
	.align 32
	.type	PIo2, @object
	.size	PIo2, 64
PIo2:
	.long	1073741824
	.long	1073291771
	.long	0
	.long	1047807021
	.long	2147483648
	.long	1022903960
	.long	1610612736
	.long	997772369
	.long	2147483648
	.long	972036995
	.long	1073741824
	.long	947528992
	.long	2147483648
	.long	920879650
	.long	0
	.long	896135965
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	init_jk, @object
	.size	init_jk, 16
init_jk:
	.long	2
	.long	3
	.long	4
	.long	6
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1073741824
	.long	1073291771
	.align 8
.LC2:
	.long	0
	.long	1047527424
	.align 8
.LC3:
	.long	0
	.long	1097859072
	.align 8
.LC4:
	.long	0
	.long	1069547520
	.align 8
.LC5:
	.long	0
	.long	1127219200
	.section	.rodata.cst16
	.align 16
.LC6:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1072693248
	.align 8
.LC8:
	.long	0
	.long	1075838976
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
