	.text
	.globl	__addtf3
	.p2align 4,,15
	.globl	__roundf128
	.type	__roundf128, @function
__roundf128:
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	(%rsp), %rdi
	movq	%rdx, %rcx
	shrq	$48, %rcx
	andl	$32767, %ecx
	leal	-16383(%rcx), %eax
	cmpl	$47, %eax
	jg	.L2
	testl	%eax, %eax
	js	.L19
	movl	%eax, %ecx
	movabsq	$281474976710655, %rsi
	sarq	%cl, %rsi
	movq	%rdx, %rcx
	andq	%rsi, %rcx
	movdqa	(%rsp), %xmm0
	orq	%rdi, %rcx
	je	.L1
	movl	%eax, %ecx
	movabsq	$140737488355328, %rdi
	notq	%rsi
	sarq	%cl, %rdi
	xorl	%ecx, %ecx
	addq	%rdi, %rdx
	movq	%rcx, (%rsp)
	andq	%rsi, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$111, %eax
	jle	.L6
	cmpl	$16384, %eax
	movdqa	(%rsp), %xmm0
	jne	.L1
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	subl	$16431, %ecx
	movq	$-1, %rsi
	shrq	%cl, %rsi
	movdqa	(%rsp), %xmm0
	testq	%rdi, %rsi
	je	.L1
	movl	$111, %ecx
	notq	%rsi
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rcx
	xorl	%eax, %eax
	addq	%rdi, %rcx
	setc	%al
	andq	%rsi, %rcx
	addq	%rax, %rdx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L19:
	movabsq	$-9223372036854775808, %rcx
	andq	%rcx, %rdx
	xorl	%ecx, %ecx
	cmpl	$-1, %eax
	jne	.L4
	movabsq	$4611404543450677248, %rax
	orq	%rax, %rdx
.L4:
	movq	%rdx, 8(%rsp)
	movq	%rcx, (%rsp)
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	ret
	.size	__roundf128, .-__roundf128
	.weak	roundf128
	.set	roundf128,__roundf128
