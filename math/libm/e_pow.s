	.text
	.p2align 4,,15
	.globl	__pow
	.type	__pow, @function
__pow:
	movq	%xmm0, %r8
	movq	%xmm1, %r9
	movq	%xmm0, %rax
	movq	%xmm1, %rsi
	shrq	$52, %r8
	shrq	$52, %r9
	leal	-1(%r8), %edx
	movl	%r8d, %ecx
	cmpl	$2045, %edx
	ja	.L2
	movl	%r9d, %edx
	xorl	%edi, %edi
	andl	$2047, %edx
	subl	$958, %edx
	cmpl	$127, %edx
	ja	.L2
.L3:
	movabsq	$-4604531861337669632, %rdx
	pxor	%xmm3, %xmm3
	addq	%rax, %rdx
	movabsq	$-4503599627370496, %r8
	movabsq	$-4294967296, %r9
	movq	%rdx, %rcx
	andq	%rdx, %r8
	sarq	$52, %rdx
	shrq	$45, %rcx
	cvtsi2sd	%edx, %xmm3
	andl	$127, %ecx
	subq	%r8, %rax
	leaq	__pow_log_data(%rip), %r8
	movq	%rcx, %rdx
	leaq	2(%rcx), %rcx
	andq	$-134217728, %rsi
	movsd	.LC0(%rip), %xmm4
	salq	$5, %rdx
	salq	$5, %rcx
	movsd	__pow_log_data(%rip), %xmm7
	movsd	8(%r8,%rcx), %xmm2
	movl	$2147483648, %ecx
	addq	%rax, %rcx
	movsd	16+__pow_log_data(%rip), %xmm10
	andq	%r9, %rcx
	movapd	%xmm2, %xmm6
	movq	%rcx, -16(%rsp)
	movsd	48+__pow_log_data(%rip), %xmm13
	movq	-16(%rsp), %xmm0
	movq	%rax, -16(%rsp)
	movsd	-16(%rsp), %xmm5
	addq	%r8, %rdx
	mulsd	%xmm0, %xmm6
	movq	%rsi, -16(%rsp)
	subsd	%xmm0, %xmm5
	movsd	64+__pow_log_data(%rip), %xmm0
	mulsd	%xmm3, %xmm7
	mulsd	8+__pow_log_data(%rip), %xmm3
	subsd	%xmm4, %xmm6
	mulsd	%xmm2, %xmm5
	addsd	88(%rdx), %xmm7
	movapd	%xmm6, %xmm11
	addsd	96(%rdx), %xmm3
	addsd	%xmm5, %xmm11
	movapd	%xmm11, %xmm9
	mulsd	%xmm11, %xmm0
	movapd	%xmm11, %xmm12
	mulsd	%xmm11, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm11, %xmm8
	mulsd	%xmm6, %xmm10
	addsd	56+__pow_log_data(%rip), %xmm0
	addsd	40+__pow_log_data(%rip), %xmm13
	mulsd	%xmm9, %xmm12
	addsd	%xmm7, %xmm8
	mulsd	%xmm10, %xmm6
	addsd	%xmm10, %xmm9
	mulsd	%xmm12, %xmm0
	subsd	%xmm8, %xmm7
	movapd	%xmm8, %xmm2
	mulsd	%xmm9, %xmm5
	addsd	%xmm6, %xmm2
	addsd	%xmm13, %xmm0
	movsd	32+__pow_log_data(%rip), %xmm13
	mulsd	%xmm11, %xmm13
	subsd	%xmm2, %xmm8
	mulsd	%xmm12, %xmm0
	mulsd	%xmm11, %xmm12
	addsd	%xmm7, %xmm11
	addsd	24+__pow_log_data(%rip), %xmm13
	addsd	%xmm8, %xmm6
	addsd	%xmm11, %xmm3
	addsd	%xmm13, %xmm0
	addsd	%xmm5, %xmm3
	movq	-16(%rsp), %xmm5
	mulsd	%xmm12, %xmm0
	addsd	%xmm6, %xmm3
	movapd	%xmm2, %xmm6
	addsd	%xmm0, %xmm3
	addsd	%xmm3, %xmm6
	movq	%xmm6, %rax
	subsd	%xmm6, %xmm2
	andq	$-134217728, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm7
	addsd	%xmm3, %xmm2
	movapd	%xmm7, %xmm0
	subsd	%xmm7, %xmm6
	mulsd	%xmm5, %xmm0
	addsd	%xmm6, %xmm2
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$52, %rdx
	mulsd	%xmm1, %xmm2
	subsd	%xmm5, %xmm1
	andl	$2047, %edx
	leal	-969(%rdx), %eax
	cmpl	$62, %eax
	mulsd	%xmm7, %xmm1
	addsd	%xmm1, %xmm2
	ja	.L51
.L19:
	movsd	__exp_data(%rip), %xmm3
	leaq	__exp_data(%rip), %r8
	movsd	8+__exp_data(%rip), %xmm1
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	%xmm3, %rsi
	subsd	%xmm1, %xmm3
	movsd	16+__exp_data(%rip), %xmm1
	movq	%rsi, %rcx
	leaq	(%rsi,%rdi), %rax
	andl	$127, %ecx
	addq	%rcx, %rcx
	salq	$45, %rax
	mulsd	%xmm3, %xmm1
	addq	120(%r8,%rcx,8), %rax
	testl	%edx, %edx
	mulsd	24+__exp_data(%rip), %xmm3
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm3
	movsd	112(%r8,%rcx,8), %xmm0
	addsd	%xmm3, %xmm2
	movsd	40+__exp_data(%rip), %xmm3
	movapd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	%xmm1, %xmm0
	mulsd	56+__exp_data(%rip), %xmm1
	addsd	32+__exp_data(%rip), %xmm3
	addsd	48+__exp_data(%rip), %xmm1
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	je	.L52
	movq	%rax, -16(%rsp)
	movsd	-16(%rsp), %xmm0
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm4
	addsd	%xmm0, %xmm4
.L1:
	movapd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	(%rsi,%rsi), %rdi
	movabsq	$-9007199254740994, %rdx
	leaq	-1(%rdi), %r10
	cmpq	%rdx, %r10
	ja	.L53
	leaq	(%rax,%rax), %r10
	leaq	-1(%r10), %rdi
	cmpq	%rdx, %rdi
	ja	.L54
	xorl	%edi, %edi
	testq	%rax, %rax
	js	.L55
.L12:
	movl	%r9d, %edx
	andl	$2047, %edx
	leal	-958(%rdx), %r8d
	cmpl	$127, %r8d
	jbe	.L15
	movabsq	$4607182418800017408, %rcx
	cmpq	%rcx, %rax
	je	.L40
	cmpl	$957, %edx
	ja	.L16
	cmpq	%rcx, %rax
	jbe	.L17
	movsd	.LC0(%rip), %xmm4
	addsd	%xmm1, %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L51:
	testl	%eax, %eax
	js	.L56
	cmpl	$1032, %edx
	jbe	.L41
	testq	%rcx, %rcx
	js	.L49
.L21:
	jmp	__math_oflow@PLT
	.p2align 4,,10
	.p2align 3
.L52:
	testl	$2147483648, %esi
	je	.L57
	movabsq	$4602678819172646912, %rdx
	addq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movsd	-16(%rsp), %xmm2
	mulsd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm3
	andpd	.LC6(%rip), %xmm3
	ucomisd	%xmm3, %xmm4
	ja	.L43
	movsd	.LC7(%rip), %xmm1
.L24:
	mulsd	%xmm1, %xmm0
	jmp	__math_check_uflow@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	testl	%ecx, %ecx
	jne	.L3
	mulsd	.LC4(%rip), %xmm0
	movabsq	$9223372036854775807, %rdx
	movq	%xmm0, %rax
	andq	%rdx, %rax
	movabsq	$-234187180623265792, %rdx
	addq	%rdx, %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L55:
	movl	%r9d, %edx
	andl	$2047, %edx
	cmpl	$1022, %edx
	jle	.L13
	cmpl	$1075, %edx
	jg	.L14
	movl	$1075, %ecx
	subl	%edx, %ecx
	movq	$-1, %rdx
	salq	%cl, %rdx
	notq	%rdx
	testq	%rsi, %rdx
	jne	.L13
	movq	%rsi, %rdi
	shrq	%cl, %rdi
	andl	$1, %edi
	sall	$18, %edi
.L14:
	movabsq	$9223372036854775807, %rdx
	movl	%r8d, %ecx
	andq	%rdx, %rax
	andl	$2047, %ecx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%edx, %edx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L43:
	pxor	%xmm5, %xmm5
	movapd	%xmm4, %xmm3
	ucomisd	%xmm0, %xmm5
	jbe	.L26
	movsd	.LC1(%rip), %xmm3
.L26:
	subsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm4
	addsd	%xmm3, %xmm4
	addsd	%xmm2, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	ucomisd	%xmm5, %xmm0
	jp	.L27
	jne	.L27
	movabsq	$-9223372036854775808, %rdx
	andq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm0
.L27:
	movsd	.LC7(%rip), %xmm1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	jmp	.L24
.L59:
	movabsq	$2251799813685248, %rax
	movabsq	$-4503599627370496, %rdx
	xorq	%rax, %rsi
	leaq	(%rsi,%rsi), %rax
	cmpq	%rdx, %rax
	ja	.L8
	.p2align 4,,10
	.p2align 3
.L40:
	movsd	.LC0(%rip), %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	jmp	__math_invalid@PLT
	.p2align 4,,10
	.p2align 3
.L56:
	testl	%edi, %edi
	addsd	%xmm0, %xmm4
	je	.L1
	xorpd	.LC3(%rip), %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movabsq	$-4544132024016830464, %rdx
	addq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm0
	mulsd	.LC5(%rip), %xmm0
	jmp	__math_check_oflow@PLT
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	%rcx, %rax
	setbe	%dl
	cmpl	$2047, %r9d
	setbe	%al
	xorl	%edi, %edi
	cmpb	%al, %dl
	jne	.L21
.L49:
	jmp	__math_uflow@PLT
	.p2align 4,,10
	.p2align 3
.L53:
	testq	%rdi, %rdi
	jne	.L5
	movabsq	$2251799813685248, %rdx
	movsd	.LC0(%rip), %xmm4
	xorq	%rdx, %rax
	movabsq	$-4503599627370496, %rdx
	addq	%rax, %rax
	cmpq	%rdx, %rax
	jbe	.L1
.L8:
	movapd	%xmm0, %xmm4
	addsd	%xmm1, %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L54:
	movapd	%xmm0, %xmm4
	xorl	%edi, %edi
	testq	%rax, %rax
	mulsd	%xmm0, %xmm4
	js	.L58
.L10:
	testq	%r10, %r10
	jne	.L11
	testq	%rsi, %rsi
	jns	.L1
	jmp	__math_divzero@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	.LC0(%rip), %xmm0
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	jmp	.L1
.L5:
	movabsq	$4607182418800017408, %rdx
	cmpq	%rdx, %rax
	je	.L59
	movabsq	$-9007199254740992, %rdx
	addq	%rax, %rax
	cmpq	%rdx, %rdi
	jne	.L8
	cmpq	%rdx, %rax
	ja	.L8
	movabsq	$9214364837600034816, %rdx
	cmpq	%rdx, %rax
	je	.L40
	subq	$1, %rdx
	pxor	%xmm4, %xmm4
	cmpq	%rdx, %rax
	movq	%rsi, %rax
	notq	%rax
	seta	%dl
	shrq	$63, %rax
	cmpb	%al, %dl
	jne	.L1
	movapd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%rsi, %rsi
	jns	.L1
	movsd	.LC0(%rip), %xmm0
	divsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm4
	jmp	.L1
.L58:
	andl	$2047, %r9d
	leal	-1023(%r9), %eax
	cmpl	$52, %eax
	ja	.L10
	movl	$1075, %ecx
	movq	$-1, %rax
	subl	%r9d, %ecx
	salq	%cl, %rax
	notq	%rax
	testq	%rsi, %rax
	jne	.L10
	btq	%rcx, %rsi
	jnc	.L10
	xorpd	.LC3(%rip), %xmm4
	movl	$1, %edi
	jmp	.L10
	.size	__pow, .-__pow
	.weak	powf32x
	.set	powf32x,__pow
	.weak	powf64
	.set	powf64,__pow
	.weak	pow
	.set	pow,__pow
	.globl	__ieee754_pow
	.set	__ieee754_pow,__pow
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1127219200
	.align 8
.LC5:
	.long	0
	.long	2130706432
	.section	.rodata.cst16
	.align 16
.LC6:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1048576
