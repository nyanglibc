	.text
	.p2align 4,,15
	.globl	__roundevenf
	.type	__roundevenf, @function
__roundevenf:
	movd	%xmm0, %esi
	movd	%xmm0, %eax
	andl	$2147483647, %esi
	movl	%esi, %edx
	shrl	$23, %edx
	cmpl	$149, %edx
	jbe	.L2
	cmpl	$255, %edx
	movss	%xmm0, -4(%rsp)
	je	.L16
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$126, %edx
	jbe	.L4
	movl	$1, %esi
	movl	$149, %ecx
	subl	%edx, %ecx
	movl	%esi, %edi
	sall	%cl, %edi
	movl	$150, %ecx
	subl	%edx, %ecx
	leal	-1(%rdi), %edx
	addl	%eax, %edi
	sall	%cl, %esi
	orl	%esi, %edx
	testl	%eax, %edx
	cmovne	%edi, %eax
	negl	%esi
	andl	%esi, %eax
.L6:
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	andl	$-2147483648, %eax
	cmpl	$126, %edx
	jne	.L6
	cmpl	$1056964608, %esi
	movl	%eax, %edx
	seta	%cl
	orl	$1065353216, %edx
	testb	%cl, %cl
	cmovne	%edx, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	addss	%xmm0, %xmm0
	ret
	.size	__roundevenf, .-__roundevenf
	.weak	roundevenf32
	.set	roundevenf32,__roundevenf
	.weak	roundevenf
	.set	roundevenf,__roundevenf
