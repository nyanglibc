	.text
	.p2align 4,,15
	.globl	__roundevenl
	.type	__roundevenl, @function
__roundevenl:
	pushq	%rbx
	movq	24(%rsp), %rdi
	movl	%edi, %edx
	andw	$32767, %dx
	cmpw	$16445, %dx
	jbe	.L2
	cmpw	$32767, %dx
	fldt	16(%rsp)
	je	.L40
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	16(%rsp), %rax
	movl	%edi, %r9d
	movzwl	%dx, %r8d
	movq	%rax, %r11
	movl	%eax, %ebx
	shrq	$32, %r11
	cmpw	$16414, %dx
	movl	%r11d, %r10d
	jbe	.L4
	movl	$1, %esi
	movl	$16445, %ecx
	subl	%r8d, %ecx
	movl	%esi, %edx
	sall	%cl, %edx
	movl	$16446, %ecx
	subl	%r8d, %ecx
	sall	%cl, %esi
	leal	-1(%rdx), %ecx
	orl	%esi, %ecx
	testl	%eax, %ecx
	je	.L5
	addl	%eax, %edx
	movl	%edx, %ebx
	jnc	.L5
	addl	$1, %r10d
	je	.L41
	.p2align 4,,10
	.p2align 3
.L5:
	negl	%esi
	andl	%ebx, %esi
	movl	%esi, %edx
.L8:
	movw	%r9w, -8(%rsp)
	movl	%r10d, -12(%rsp)
	movl	%edx, -16(%rsp)
	fldt	-16(%rsp)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$16414, %r8d
	je	.L42
	cmpl	$16382, %r8d
	jle	.L10
	movl	$1, %edx
	movl	$16413, %ecx
	subl	%r8d, %ecx
	movl	%edx, %esi
	sall	%cl, %esi
	movl	$16414, %ecx
	subl	%r8d, %ecx
	sall	%cl, %edx
	leal	-1(%rsi), %ecx
	orl	%edx, %ecx
	andl	%ecx, %r11d
	orl	%eax, %r11d
	je	.L11
	addl	%esi, %r10d
	jnc	.L11
	leal	1(%rdi), %r9d
	movl	$-2147483648, %r10d
.L11:
	negl	%edx
	andl	%edx, %r10d
	xorl	%edx, %edx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L40:
	fadd	%st(0), %st
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%r11d, %edx
	movl	%eax, %ecx
	andl	$1, %edx
	andl	$2147483647, %ecx
	orl	%ecx, %edx
	je	.L8
	xorl	%edx, %edx
	addl	$-2147483648, %eax
	js	.L8
	addl	$1, %r10d
	jne	.L8
	movl	%r10d, %edx
	leal	1(%rdi), %r9d
	movl	$-2147483648, %r10d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	je	.L43
.L14:
	movl	%edi, %r9d
	xorl	%edx, %edx
	xorl	%r10d, %r10d
	andw	$-32768, %r9w
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L41:
	leal	1(%rdi), %r9d
	movl	$-2147483648, %r10d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L43:
	cmpl	$-2147483648, %r11d
	ja	.L19
	testl	%eax, %eax
	je	.L14
.L19:
	andw	$-32768, %di
	xorl	%edx, %edx
	movl	$-2147483648, %r10d
	orw	$16383, %di
	movl	%edi, %r9d
	jmp	.L8
	.size	__roundevenl, .-__roundevenl
	.weak	roundevenf64x
	.set	roundevenf64x,__roundevenl
	.weak	roundevenl
	.set	roundevenl,__roundevenl
