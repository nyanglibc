	.text
	.p2align 4,,15
	.globl	__GI___issignalingf
	.hidden	__GI___issignalingf
	.type	__GI___issignalingf, @function
__GI___issignalingf:
	movd	%xmm0, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	seta	%al
	movzbl	%al, %eax
	ret
	.size	__GI___issignalingf, .-__GI___issignalingf
	.globl	__issignalingf
	.set	__issignalingf,__GI___issignalingf
