	.text
	.p2align 4,,15
	.globl	__GI___iscanonicall
	.hidden	__GI___iscanonicall
	.type	__GI___iscanonicall, @function
__GI___iscanonicall:
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx
	shrq	$32, %rax
	movl	%eax, %edx
	shrl	$31, %eax
	notl	%edx
	shrl	$31, %edx
	testl	$32767, %ecx
	cmove	%edx, %eax
	ret
	.size	__GI___iscanonicall, .-__GI___iscanonicall
	.globl	__iscanonicall
	.set	__iscanonicall,__GI___iscanonicall
