	.text
	.p2align 4,,15
	.globl	__feholdexcept
	.type	__feholdexcept, @function
__feholdexcept:
#APP
# 28 "../sysdeps/x86_64/fpu/feholdexcpt.c" 1
	fnstenv (%rdi)
	stmxcsr 28(%rdi)
	fnclex
# 0 "" 2
#NO_APP
	movl	28(%rdi), %eax
	andl	$-8128, %eax
	orl	$8064, %eax
	movl	%eax, -4(%rsp)
#APP
# 35 "../sysdeps/x86_64/fpu/feholdexcpt.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	__feholdexcept, .-__feholdexcept
	.weak	feholdexcept
	.set	feholdexcept,__feholdexcept
