	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__remainderf
	.type	__remainderf, @function
__remainderf:
	ucomiss	%xmm0, %xmm0
	jp	.L5
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	ucomiss	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L2
.L5:
	movaps	%xmm0, %xmm2
	andps	.LC1(%rip), %xmm2
	ucomiss	.LC2(%rip), %xmm2
	jbe	.L4
	ucomiss	%xmm1, %xmm1
	jp	.L4
.L2:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	jne	.L10
.L4:
	jmp	__ieee754_remainderf@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$128, %edi
	jmp	__kernel_standard_f@PLT
	.size	__remainderf, .-__remainderf
	.weak	dremf
	.set	dremf,__remainderf
	.weak	remainderf32
	.set	remainderf32,__remainderf
	.weak	remainderf
	.set	remainderf,__remainderf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095039
