	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __matherr,matherr@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.weak	__matherr
	.type	__matherr, @function
__matherr:
	xorl	%eax, %eax
	ret
	.size	__matherr, .-__matherr
