	.text
	.p2align 4,,15
	.globl	__ccosl
	.type	__ccosl, @function
__ccosl:
	fldt	8(%rsp)
	fldt	24(%rsp)
	fchs
	fstpt	8(%rsp)
	fstpt	24(%rsp)
	jmp	__ccoshl@PLT
	.size	__ccosl, .-__ccosl
	.weak	ccosf64x
	.set	ccosf64x,__ccosl
	.weak	ccosl
	.set	ccosl,__ccosl
