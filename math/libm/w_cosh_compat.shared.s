	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cosh
	.type	__cosh, @function
__cosh:
	subq	$24, %rsp
	movsd	%xmm0, 8(%rsp)
	call	__ieee754_cosh@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm3
	movsd	8(%rsp), %xmm2
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andpd	%xmm2, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	movl	$5, %edi
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.size	__cosh, .-__cosh
	.weak	coshf32x
	.set	coshf32x,__cosh
	.weak	coshf64
	.set	coshf64,__cosh
	.weak	cosh
	.set	cosh,__cosh
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
