.globl __ceill
.type __ceill,@function
.align 1<<4
__ceill:
 fldt 8(%rsp)
 fnstenv -28(%rsp)
 movl $0x0800,%edx
 orl -28(%rsp),%edx
 andl $0xfbff,%edx
 movl %edx,-32(%rsp)
 fldcw -32(%rsp)
 frndint
 fnstsw
 andl $0x1, %eax
 orl %eax, -24(%rsp)
 fldenv -28(%rsp)
 ret
.size __ceill,.-__ceill
.weak ceill
ceill = __ceill
.weak ceilf64x
ceilf64x = __ceill
