	.text
	.p2align 4,,15
	.globl	__faddl
	.type	__faddl, @function
__faddl:
	pushq	%rbx
	subq	$80, %rsp
	fldt	112(%rsp)
	fchs
	fldt	96(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	jp	.L17
	jne	.L18
	fldt	112(%rsp)
	movss	.LC1(%rip), %xmm2
	faddp	%st, %st(1)
	fstps	28(%rsp)
	movss	28(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jnb	.L1
.L4:
	ucomiss	%xmm0, %xmm0
	jp	.L16
	fldt	96(%rsp)
	fabs
	fldt	.LC2(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L19
	fldt	112(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	fstp	%st(0)
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
.L17:
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L18:
	fstp	%st(0)
.L2:
#APP
# 44 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstenv 48(%rsp); fnclex
# 0 "" 2
#NO_APP
	movzwl	48(%rsp), %eax
	orw	$3903, %ax
	movw	%ax, 32(%rsp)
#APP
# 88 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 32(%rsp)
# 0 "" 2
#NO_APP
	fldt	96(%rsp)
	fldt	112(%rsp)
	faddp	%st, %st(1)
	fld	%st(0)
	fstpt	(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fstp	%st(0)
#APP
# 163 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstsw %ax
# 0 "" 2
#NO_APP
	movl	%eax, %ebx
#APP
# 130 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldenv 48(%rsp)
# 0 "" 2
#NO_APP
	movl	%ebx, %edi
	shrw	$5, %bx
	andl	$61, %edi
	andl	$1, %ebx
	call	__feraiseexcept@PLT
	movq	(%rsp), %rax
	movss	.LC1(%rip), %xmm2
	orl	%eax, %ebx
	movl	%ebx, 32(%rsp)
	fldt	32(%rsp)
	fstps	28(%rsp)
	movss	28(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jb	.L4
	ucomiss	.LC3(%rip), %xmm0
	jp	.L1
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	fldt	112(%rsp)
	fldt	96(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__faddl, .-__faddl
	.weak	f32addf64x
	.set	f32addf64x,__faddl
	.weak	faddl
	.set	faddl,__faddl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	0
