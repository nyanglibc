.globl __floorl
.type __floorl,@function
.align 1<<4
__floorl:
 fldt 8(%rsp)
 fnstenv -28(%rsp)
 movl $0x400,%edx
 orl -28(%rsp),%edx
 andl $0xf7ff,%edx
 movl %edx,-32(%rsp)
 fldcw -32(%rsp)
 frndint
 fnstsw
 andl $0x1, %eax
 orl %eax, -24(%rsp)
 fldenv -28(%rsp)
 ret
.size __floorl,.-__floorl
.weak floorl
floorl = __floorl
.weak floorf64x
floorf64x = __floorl
