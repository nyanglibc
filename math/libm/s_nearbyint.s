	.text
	.p2align 4,,15
	.globl	__nearbyint
	.type	__nearbyint, @function
__nearbyint:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$52, %rax
	shrq	$63, %rdx
	andl	$2047, %eax
	subl	$1023, %eax
	cmpl	$51, %eax
	jg	.L2
#APP
# 33 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	-44(%rsp), %eax
	movl	%eax, -12(%rsp)
	js	.L7
	andl	$-8128, %eax
	orl	$8064, %eax
	movl	%eax, -44(%rsp)
#APP
# 36 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	leaq	TWO52.6221(%rip), %rax
	movsd	(%rax,%rdx,8), %xmm1
	addsd	%xmm1, %xmm0
	subsd	%xmm1, %xmm0
#APP
# 122 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -12(%rsp)
# 0 "" 2
#NO_APP
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	andl	$-8128, %eax
	orl	$8064, %eax
	movl	%eax, -44(%rsp)
#APP
# 36 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	leaq	TWO52.6221(%rip), %rax
	movapd	%xmm0, %xmm1
	movsd	(%rax,%rdx,8), %xmm2
	addsd	%xmm2, %xmm1
	subsd	%xmm2, %xmm1
#APP
# 122 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr -12(%rsp)
# 0 "" 2
#NO_APP
	movapd	%xmm0, %xmm3
	andpd	.LC0(%rip), %xmm1
	andpd	.LC1(%rip), %xmm3
	orpd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1024, %eax
	jne	.L1
	addsd	%xmm0, %xmm0
	ret
	.size	__nearbyint, .-__nearbyint
	.weak	nearbyintf32x
	.set	nearbyintf32x,__nearbyint
	.weak	nearbyintf64
	.set	nearbyintf64,__nearbyint
	.weak	nearbyint
	.set	nearbyint,__nearbyint
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	TWO52.6221, @object
	.size	TWO52.6221, 16
TWO52.6221:
	.long	0
	.long	1127219200
	.long	0
	.long	-1020264448
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC1:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
