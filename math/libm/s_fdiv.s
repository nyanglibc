	.text
	.p2align 4,,15
	.globl	__fdiv
	.type	__fdiv, @function
__fdiv:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %eax
	movl	%eax, %edx
	andl	$-32704, %edx
	orl	$32640, %edx
	movl	%edx, 28(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movq	%xmm2, %rbp
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %edx
	orl	%eax, %edx
	movl	%edx, 28(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %eax
	notl	%eax
	testl	%edi, %eax
	jne	.L21
.L2:
	shrl	$5, %ebx
	movabsq	$-4294967296, %rax
	andl	$1, %ebx
	pxor	%xmm2, %xmm2
	orl	%ebp, %ebx
	andq	%rax, %rbp
	orq	%rbx, %rbp
	movss	.LC1(%rip), %xmm4
	movq	%rbp, (%rsp)
	cvtsd2ss	(%rsp), %xmm2
	movaps	%xmm2, %xmm3
	andps	.LC0(%rip), %xmm3
	ucomiss	%xmm3, %xmm4
	jnb	.L18
	ucomiss	%xmm2, %xmm2
	jp	.L22
	andpd	.LC2(%rip), %xmm0
	movsd	.LC3(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jb	.L1
.L7:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$40, %rsp
	movaps	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	pxor	%xmm3, %xmm3
	movl	$0, %edx
	ucomiss	%xmm3, %xmm2
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	pxor	%xmm3, %xmm3
	movl	$1, %edx
	ucomisd	%xmm3, %xmm0
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	andpd	.LC2(%rip), %xmm1
	ucomisd	.LC3(%rip), %xmm1
	jbe	.L7
	addq	$40, %rsp
	movaps	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	ucomisd	%xmm1, %xmm0
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__feraiseexcept@PLT
	movsd	8(%rsp), %xmm1
	movsd	(%rsp), %xmm0
	jmp	.L2
	.size	__fdiv, .-__fdiv
	.weak	f32divf32x
	.set	f32divf32x,__fdiv
	.weak	f32divf64
	.set	f32divf64,__fdiv
	.weak	fdiv
	.set	fdiv,__fdiv
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
