	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ilogbl
	.type	__ilogbl, @function
__ilogbl:
	pushq	%rbx
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__ieee754_ilogbl@PLT
	movl	%eax, %ebx
	addl	$2147483647, %eax
	cmpl	$-3, %eax
	popq	%rdx
	popq	%rcx
	ja	.L5
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rax
	movl	$1, %edi
	movl	$33, %fs:(%rax)
	call	__GI___feraiseexcept
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	__ilogbl, .-__ilogbl
	.weak	ilogbf64x
	.set	ilogbf64x,__ilogbl
	.weak	ilogbl
	.set	ilogbl,__ilogbl
