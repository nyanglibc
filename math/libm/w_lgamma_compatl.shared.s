	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __lgammal_compat,lgammal@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__lgammal_compat
	.type	__lgammal_compat, @function
__lgammal_compat:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__ieee754_lgammal_r@PLT
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	popq	%rdx
	popq	%rcx
	movl	(%rax), %eax
	cmpl	$3, %eax
	je	.L2
	movl	28(%rsp), %edx
	movq	__signgam@GOTPCREL(%rip), %rcx
	movl	%edx, (%rcx)
	movq	signgam@GOTPCREL(%rip), %rcx
	movl	%edx, (%rcx)
.L2:
	fldt	.LC0(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L22
	fstp	%st(0)
.L19:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	fldt	48(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L19
	cmpl	$-1, %eax
	je	.L19
	fstp	%st(0)
	fldt	48(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	frndint
	fldcw	14(%rsp)
	fldt	48(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L23
	jne	.L24
	fldz
	xorl	%edi, %edi
	fucomip	%st(1), %st
	fstp	%st(0)
	setnb	%dil
	addl	$214, %edi
.L5:
	pushq	56(%rsp)
	pushq	56(%rsp)
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L19
.L23:
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L24:
	fstp	%st(0)
.L8:
	movl	$214, %edi
	jmp	.L5
	.size	__lgammal_compat, .-__lgammal_compat
	.globl	__gammal
	.set	__gammal,__lgammal_compat
	.weak	gammal
	.set	gammal,__gammal
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
