	.text
	.p2align 4,,15
	.globl	__rint
	.type	__rint, @function
__rint:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$52, %rax
	andl	$2047, %eax
	subl	$1023, %eax
	cmpl	$51, %eax
	jg	.L2
	leaq	TWO52.4462(%rip), %rcx
	shrq	$63, %rdx
	testl	%eax, %eax
	movsd	(%rcx,%rdx,8), %xmm1
	addsd	%xmm1, %xmm0
	subsd	%xmm1, %xmm0
	js	.L6
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1024, %eax
	jne	.L1
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%xmm0, %rsi
	movabsq	$9223372036854775807, %rax
	salq	$63, %rdx
	andq	%rax, %rsi
	movq	%rsi, %rax
	orq	%rdx, %rax
	movq	%rax, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.size	__rint, .-__rint
	.weak	rintf32x
	.set	rintf32x,__rint
	.weak	rintf64
	.set	rintf64,__rint
	.weak	rint
	.set	rint,__rint
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	TWO52.4462, @object
	.size	TWO52.4462, 16
TWO52.4462:
	.long	0
	.long	1127219200
	.long	0
	.long	-1020264448
