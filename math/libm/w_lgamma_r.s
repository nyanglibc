	.text
	.p2align 4,,15
	.globl	__lgamma_r
	.type	__lgamma_r, @function
__lgamma_r:
	subq	$24, %rsp
	movsd	%xmm0, 8(%rsp)
	call	__ieee754_lgamma_r@PLT
	movq	.LC0(%rip), %xmm3
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm2
	movsd	8(%rsp), %xmm1
	andpd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm2
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andpd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__lgamma_r, .-__lgamma_r
	.weak	lgammaf32x_r
	.set	lgammaf32x_r,__lgamma_r
	.weak	lgammaf64_r
	.set	lgammaf64_r,__lgamma_r
	.weak	lgamma_r
	.set	lgamma_r,__lgamma_r
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
