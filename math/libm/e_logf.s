	.text
	.p2align 4,,15
	.globl	__logf
	.type	__logf, @function
__logf:
	movd	%xmm0, %eax
	movd	%xmm0, %ecx
	cmpl	$1065353216, %eax
	je	.L7
	leal	-8388608(%rax), %edx
	cmpl	$2130706431, %edx
	ja	.L11
.L3:
	leal	-1060306944(%rcx), %eax
	pxor	%xmm0, %xmm0
	movsd	272+__logf_data(%rip), %xmm2
	movl	%eax, %esi
	movl	%eax, %edx
	sarl	$23, %eax
	andl	$-8388608, %esi
	shrl	$19, %edx
	subl	%esi, %ecx
	andl	$15, %edx
	movl	%ecx, -12(%rsp)
	leaq	__logf_data(%rip), %rcx
	salq	$4, %rdx
	cvtss2sd	-12(%rsp), %xmm0
	addq	%rcx, %rdx
	mulsd	(%rdx), %xmm0
	movapd	%xmm0, %xmm1
	movsd	264+__logf_data(%rip), %xmm0
	subsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm3
	addsd	280+__logf_data(%rip), %xmm2
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	pxor	%xmm0, %xmm0
	mulsd	%xmm3, %xmm2
	cvtsi2sd	%eax, %xmm0
	mulsd	256+__logf_data(%rip), %xmm0
	addsd	8(%rdx), %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movd	%xmm0, %edx
	addl	%edx, %edx
	je	.L12
	cmpl	$2139095040, %eax
	movss	%xmm0, -12(%rsp)
	je	.L8
	cmpl	$-16777217, %edx
	ja	.L9
	testl	%eax, %eax
	js	.L9
	movss	-12(%rsp), %xmm4
	mulss	.LC1(%rip), %xmm4
	movd	%xmm4, %eax
	leal	-192937984(%rax), %ecx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movss	-12(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %edi
	jmp	__math_divzerof@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	movss	-12(%rsp), %xmm0
	jmp	__math_invalidf@PLT
	.size	__logf, .-__logf
	.weak	logf32
	.set	logf32,__logf
	.weak	logf
	.set	logf,__logf
	.globl	__ieee754_logf
	.set	__ieee754_logf,__logf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1258291200
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	0
	.long	1072693248
