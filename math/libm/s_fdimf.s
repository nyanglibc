	.text
	.p2align 4,,15
	.globl	__fdimf
	.type	__fdimf, @function
__fdimf:
	ucomiss	%xmm0, %xmm1
	movaps	%xmm0, %xmm2
	jnb	.L10
	subss	%xmm1, %xmm0
	movss	.LC1(%rip), %xmm3
	movaps	%xmm0, %xmm4
	andps	%xmm3, %xmm4
	ucomiss	.LC2(%rip), %xmm4
	jbe	.L1
	andps	%xmm3, %xmm2
	ucomiss	.LC2(%rip), %xmm2
	ja	.L1
	andps	%xmm3, %xmm1
	ucomiss	.LC2(%rip), %xmm1
	ja	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pxor	%xmm0, %xmm0
.L1:
	rep ret
	.size	__fdimf, .-__fdimf
	.weak	fdimf32
	.set	fdimf32,__fdimf
	.weak	fdimf
	.set	fdimf,__fdimf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095039
