	.text
	.p2align 4,,15
	.globl	__issignaling
	.type	__issignaling, @function
__issignaling:
	movq	%xmm0, %rax
	movabsq	$2251799813685248, %rdx
	xorq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	movabsq	$9221120237041090560, %rdx
	cmpq	%rdx, %rax
	seta	%al
	movzbl	%al, %eax
	ret
	.size	__issignaling, .-__issignaling
