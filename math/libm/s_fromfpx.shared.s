	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpx
	.type	__fromfpx, @function
__fromfpx:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$64, %esi
	ja	.L36
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$64, %esi
.L10:
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rcx
	andq	%rdx, %rcx
	je	.L37
	movq	%xmm0, %r11
	shrq	$52, %rcx
	leal	-1023(%rcx), %r9d
	shrq	$63, %r11
	leal	-2(%rsi,%r11), %r10d
	cmpl	%r10d, %r9d
	jg	.L57
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %r8
	andq	%rdx, %rax
	orq	%r8, %rax
	cmpl	$51, %r9d
	jle	.L15
	subl	$1075, %ecx
	xorl	%r12d, %r12d
	salq	%cl, %rax
	movq	%rax, %r8
.L16:
	cmpl	$1, %edi
	je	.L42
	jle	.L59
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	cmpl	$3, %edi
	je	.L26
	cmpl	$4, %edi
	jne	.L40
.L26:
	addq	%rax, %r8
.L17:
	testq	%rdx, %rdx
	jns	.L24
.L23:
	cmpl	%r10d, %r9d
	je	.L27
	testb	%bpl, %bpl
	jne	.L28
	testb	%r12b, %r12b
	jne	.L28
.L29:
	movq	%r8, %rax
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$-1, %r9d
	jl	.L38
	movl	$51, %ecx
	movl	$1, %r8d
	movq	%rax, %r13
	subl	%r9d, %ecx
	salq	%cl, %r8
	andq	%r8, %r13
	movq	%r8, %rcx
	setne	%bpl
	subq	$1, %rcx
	movq	%rcx, %rbx
	movl	$52, %ecx
	andq	%rax, %rbx
	setne	%r12b
	subl	%r9d, %ecx
	shrq	%cl, %rax
	cmpl	$1, %edi
	movq	%rax, %r8
	je	.L18
	jle	.L60
	cmpl	$3, %edi
	je	.L21
	cmpl	$4, %edi
	jne	.L17
	testq	%r13, %r13
	je	.L39
	andl	$1, %eax
	movl	$1, %ebp
	orq	%rbx, %rax
	setne	%al
	movzbl	%al, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%edi, %edi
	jne	.L17
.L20:
	testq	%rdx, %rdx
	js	.L23
	movl	%ebp, %eax
	orl	%r12d, %eax
	movzbl	%al, %eax
	addq	%rax, %r8
.L24:
	leal	1(%r10), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%r8, %rax
	sete	%al
.L30:
	testb	%al, %al
	jne	.L57
	testb	%r12b, %r12b
	jne	.L28
	testb	%bpl, %bpl
	je	.L32
.L28:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
.L32:
	testq	%rdx, %rdx
	movq	%r8, %rax
	js	.L29
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$8, %rsp
	movl	%r11d, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%ebp, %ebp
	testl	%edi, %edi
	je	.L20
.L40:
	xorl	%ebp, %ebp
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%ebp, %ebp
.L18:
	testq	%rdx, %rdx
	jns	.L24
	movl	%ebp, %eax
	orl	%r12d, %eax
	movzbl	%al, %eax
	addq	%rax, %r8
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$1, %r12d
	xorl	%r8d, %r8d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1, %eax
	movl	%r9d, %ecx
	salq	%cl, %rax
	cmpq	%r8, %rax
	setne	%al
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	%bpl, %eax
	jmp	.L26
.L39:
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.L26
	.size	__fromfpx, .-__fromfpx
	.weak	fromfpxf32x
	.set	fromfpxf32x,__fromfpx
	.weak	fromfpxf64
	.set	fromfpxf64,__fromfpx
	.weak	fromfpx
	.set	fromfpx,__fromfpx
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
