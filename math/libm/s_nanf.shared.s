	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__nanf
	.type	__nanf, @function
__nanf:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__strtof_nan@PLT
	.size	__nanf, .-__nanf
	.weak	nanf32
	.set	nanf32,__nanf
	.weak	nanf
	.set	nanf,__nanf
