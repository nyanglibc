	.text
#APP
	.symver __ieee754_jnf128,__jnf128_finite@GLIBC_2.26
	.symver __ieee754_ynf128,__ynf128_finite@GLIBC_2.26
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__floatsitf
	.globl	__letf2
	.globl	__subtf3
	.globl	__multf3
	.globl	__divtf3
	.globl	__lttf2
	.globl	__gttf2
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_jnf128
	.type	__ieee754_jnf128, @function
__ieee754_jnf128:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$136, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %rdx
	shrq	$32, %rdx
	movl	%edx, %r12d
	movl	%edx, %ecx
	andl	$2147483647, %r12d
	cmpl	$2147418111, %r12d
	jle	.L2
	movq	(%rsp), %rsi
	movq	%rsi, %rdi
	shrq	$32, %rdi
	orl	%edi, %eax
	orl	%esi, %eax
	movzwl	%dx, %esi
	orl	%esi, %eax
	jne	.L82
.L2:
	testl	%ebx, %ebx
	js	.L83
	je	.L84
.L5:
	cmpl	$1, %ebx
	je	.L85
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 96(%rsp)
# 0 "" 2
#NO_APP
	movl	96(%rsp), %r13d
	shrl	$31, %ecx
	xorl	%r14d, %r14d
	andl	%ebx, %ecx
	movl	%ecx, %ebp
	movl	%r13d, %eax
	andb	$-97, %ah
	cmpl	%eax, %r13d
	movl	%eax, 112(%rsp)
	jne	.L86
.L7:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L51
	cmpl	$2147418111, %r12d
	jg	.L51
	movdqa	(%rsp), %xmm5
	movl	%ebx, %edi
	pand	.LC3(%rip), %xmm5
	movaps	%xmm5, (%rsp)
	call	__floatsitf@PLT
	movaps	%xmm0, 80(%rsp)
	movdqa	(%rsp), %xmm1
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L75
	cmpl	$1093468159, %r12d
	jle	.L14
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	andl	$3, %ebx
	movdqa	(%rsp), %xmm0
	call	__sincosf128@PLT
	cmpl	$2, %ebx
	je	.L16
	cmpl	$3, %ebx
	je	.L17
	cmpl	$1, %ebx
	je	.L18
	movdqa	96(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 16(%rsp)
.L19:
	movdqa	(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
.L20:
	cmpl	$1, %ebp
	jne	.L38
	pxor	.LC1(%rip), %xmm2
.L38:
	testb	%r14b, %r14b
	jne	.L87
.L39:
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	je	.L88
	movdqa	.LC3(%rip), %xmm0
	pand	%xmm2, %xmm0
	movdqa	.LC10(%rip), %xmm1
	movaps	%xmm2, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jns	.L1
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L83:
	movdqa	(%rsp), %xmm6
	negl	%ebx
	leal	-2147483648(%rdx), %ecx
	pxor	.LC1(%rip), %xmm6
	movaps	%xmm6, (%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L82:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$136, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	cmpl	$1, %ebp
	pxor	%xmm2, %xmm2
	je	.L89
.L11:
	testb	%r14b, %r14b
	je	.L1
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 112(%rsp)
# 0 "" 2
#NO_APP
	movl	112(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 112(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 112(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L84:
	movdqa	(%rsp), %xmm0
	call	__ieee754_j0f128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$1069940735, %r12d
	jg	.L22
	cmpl	$399, %ebx
	pxor	%xmm2, %xmm2
	jg	.L20
	movdqa	(%rsp), %xmm0
	movl	$2, %r12d
	movdqa	.LC5(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm7
	movdqa	%xmm0, %xmm2
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm7, (%rsp)
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%r12d, %edi
	addl	$1, %r12d
	movaps	%xmm2, 16(%rsp)
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	cmpl	%ebx, %r12d
	movdqa	%xmm0, %xmm2
	jle	.L23
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L89:
	movdqa	.LC1(%rip), %xmm2
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L88:
	movdqa	%xmm2, %xmm1
	movdqa	.LC10(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__multf3@PLT
	movq	errno@gottpoff(%rip), %rax
	movdqa	%xmm0, %xmm2
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L85:
	movdqa	(%rsp), %xmm0
	call	__ieee754_j1f128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	leal	(%rbx,%rbx), %r15d
	movl	$1, %r12d
	movl	%r15d, %edi
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC6(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	48(%rsp), %xmm2
	jns	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movdqa	%xmm2, %xmm0
	addl	$1, %r12d
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm7
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm7, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	48(%rsp), %xmm2
	js	.L26
.L24:
	leal	(%r12,%rbx), %eax
	cmpl	%ebx, %eax
	leal	(%rax,%rax), %r12d
	jl	.L48
	pxor	%xmm3, %xmm3
	movaps	%xmm3, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%r12d, %edi
	subl	$2, %r12d
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__divtf3@PLT
	cmpl	%r12d, %r15d
	movaps	%xmm0, 16(%rsp)
	jle	.L28
.L27:
	subl	$2, %r15d
	subl	$1, %ebx
	movdqa	64(%rsp), %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	pand	.LC3(%rip), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	movl	%r15d, %edi
	jns	.L76
	call	__floatsitf@PLT
	movdqa	.LC2(%rip), %xmm5
	movdqa	%xmm0, %xmm2
	movdqa	16(%rsp), %xmm4
	movaps	%xmm5, 32(%rsp)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L49:
	movaps	%xmm3, 32(%rsp)
.L31:
	movdqa	%xmm2, %xmm1
	movaps	%xmm2, 80(%rsp)
	movaps	%xmm4, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	48(%rsp), %xmm4
	movdqa	%xmm4, %xmm1
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm2, %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	subl	$1, %ebx
	movdqa	%xmm0, %xmm2
	movdqa	32(%rsp), %xmm4
	movdqa	64(%rsp), %xmm3
	jne	.L49
.L32:
	movdqa	(%rsp), %xmm0
	call	__ieee754_j0f128@PLT
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_j1f128@PLT
	movdqa	.LC3(%rip), %xmm7
	movdqa	64(%rsp), %xmm1
	pand	%xmm0, %xmm7
	pand	.LC3(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	%xmm7, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jg	.L78
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm0
	movl	$1, %r12d
	call	__ieee754_j0f128@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_j1f128@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L45:
	movaps	%xmm0, 16(%rsp)
.L21:
	leal	(%r12,%r12), %edi
	addl	$1, %r12d
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm6
	cmpl	%ebx, %r12d
	movdqa	%xmm0, %xmm2
	movaps	%xmm6, 32(%rsp)
	jl	.L45
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	112(%rsp), %xmm0
	pxor	.LC1(%rip), %xmm0
	movdqa	96(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L19
.L48:
	pxor	%xmm7, %xmm7
	movaps	%xmm7, 16(%rsp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L86:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 112(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r14d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L78:
	movdqa	%xmm2, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L76:
	call	__floatsitf@PLT
	movdqa	16(%rsp), %xmm4
	xorl	%eax, %eax
	movabsq	$4611404543450677248, %rdx
	movq	%rax, 48(%rsp)
	movaps	%xmm0, 80(%rsp)
	movq	%rdx, 56(%rsp)
	movaps	%xmm4, 32(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L90:
	movdqa	48(%rsp), %xmm0
	movdqa	64(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 16(%rsp)
	xorl	%eax, %eax
	movabsq	$4611404543450677248, %rdx
	movq	%rax, 48(%rsp)
	movq	%rdx, 56(%rsp)
.L33:
	subl	$1, %ebx
	je	.L32
.L35:
	movdqa	80(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	.LC6(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC9(%rip), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L90
	movdqa	48(%rsp), %xmm4
	movdqa	64(%rsp), %xmm5
	movaps	%xmm4, 32(%rsp)
	movaps	%xmm5, 48(%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L87:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 112(%rsp)
# 0 "" 2
#NO_APP
	movl	112(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 112(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 112(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L39
.L18:
	movdqa	112(%rsp), %xmm1
	movdqa	96(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L17:
	movdqa	96(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L19
	.size	__ieee754_jnf128, .-__ieee754_jnf128
	.globl	__unordtf2
	.p2align 4,,15
	.globl	__ieee754_ynf128
	.type	__ieee754_ynf128, @function
__ieee754_ynf128:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$96, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %r12
	shrq	$32, %r12
	movl	%r12d, %ebp
	andl	$2147483647, %ebp
	cmpl	$2147418111, %ebp
	jle	.L92
	movq	(%rsp), %rdx
	movq	%rdx, %rcx
	shrq	$32, %rcx
	orl	%ecx, %eax
	orl	%edx, %eax
	movzwl	%r12w, %edx
	orl	%edx, %eax
	jne	.L155
.L92:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L156
.L94:
	testl	%ebx, %ebx
	js	.L157
	movl	$1, %r13d
	je	.L158
.L100:
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 64(%rsp)
# 0 "" 2
#NO_APP
	movl	64(%rsp), %r12d
	movl	%r12d, %r14d
	andl	$-24577, %r14d
	cmpl	%r14d, %r12d
	movl	%r14d, 80(%rsp)
	jne	.L159
	cmpl	$1, %ebx
	je	.L160
	xorl	%r14d, %r14d
	cmpl	$2147418111, %ebp
	jle	.L120
.L154:
	pxor	%xmm2, %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L156:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L149
	testl	%ebx, %ebx
	pxor	%xmm1, %xmm1
	js	.L161
.L98:
	movdqa	.LC11(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L155:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L91:
	addq	$96, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	negl	%ebx
	movl	$1, %r13d
	leal	(%rbx,%rbx), %eax
	andl	$2, %eax
	subl	%eax, %r13d
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L159:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 80(%rsp)
# 0 "" 2
#NO_APP
	cmpl	$1, %ebx
	je	.L162
	cmpl	$2147418111, %ebp
	jg	.L119
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L120:
	cmpl	$1093468159, %ebp
	jle	.L104
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdi
	andl	$3, %ebx
	movdqa	(%rsp), %xmm0
	call	__sincosf128@PLT
	cmpl	$2, %ebx
	je	.L106
	cmpl	$3, %ebx
	je	.L107
	cmpl	$1, %ebx
	je	.L108
	movdqa	80(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
.L109:
	movdqa	(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
.L110:
	movdqa	.LC3(%rip), %xmm4
	pand	%xmm2, %xmm4
	movdqa	.LC12(%rip), %xmm6
	movdqa	%xmm4, %xmm0
	movdqa	%xmm6, %xmm1
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm6, (%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm2
	jne	.L127
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm2
	jle	.L113
.L127:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L113:
	cmpl	$1, %r13d
	je	.L115
	movdqa	.LC1(%rip), %xmm0
	pxor	%xmm2, %xmm0
	pand	.LC3(%rip), %xmm2
	movaps	%xmm2, 16(%rsp)
	movdqa	%xmm0, %xmm2
.L115:
	testb	%r14b, %r14b
	jne	.L163
.L116:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	movaps	%xmm2, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm2
	jne	.L91
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm2
	jle	.L91
	movdqa	%xmm2, %xmm1
	movdqa	(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L149:
	testl	%r12d, %r12d
	jns	.L94
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L158:
	movdqa	(%rsp), %xmm0
	call	__ieee754_y0f128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L104:
	movdqa	(%rsp), %xmm0
	call	__ieee754_y0f128@PLT
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_y1f128@PLT
	movaps	%xmm0, 32(%rsp)
	movq	40(%rsp), %rax
	shrq	$32, %rax
	xorw	%ax, %ax
	cmpl	$-65536, %eax
	je	.L122
	cmpl	$1, %ebx
	jle	.L122
	movl	$1, %ebp
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L164:
	cmpl	%ebx, %ebp
	jge	.L110
	movdqa	16(%rsp), %xmm5
	movaps	%xmm5, 32(%rsp)
.L111:
	leal	(%rbp,%rbp), %edi
	addl	$1, %ebp
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movq	24(%rsp), %rax
	movdqa	%xmm0, %xmm2
	shrq	$32, %rax
	movdqa	32(%rsp), %xmm3
	xorw	%ax, %ax
	cmpl	$-65536, %eax
	movaps	%xmm3, 48(%rsp)
	jne	.L164
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L160:
	movdqa	(%rsp), %xmm0
	call	__ieee754_y1f128@PLT
	movl	%r13d, %edi
	movaps	%xmm0, (%rsp)
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
.L153:
	movdqa	.LC3(%rip), %xmm7
	pand	%xmm2, %xmm7
	movaps	%xmm7, 16(%rsp)
	movdqa	.LC12(%rip), %xmm7
	movaps	%xmm7, (%rsp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L161:
	andl	$1, %ebx
	je	.L98
	movdqa	.LC2(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L107:
	movdqa	80(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L106:
	movdqa	64(%rsp), %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L108:
	movdqa	64(%rsp), %xmm0
	pxor	.LC1(%rip), %xmm0
	movdqa	80(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L163:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 80(%rsp)
# 0 "" 2
#NO_APP
	movl	80(%rsp), %eax
	andl	$24576, %r12d
	andb	$-97, %ah
	orl	%eax, %r12d
	movl	%r12d, 80(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 80(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L119:
	andl	$24576, %r12d
	orl	%r12d, %r14d
	movl	%r14d, 80(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 80(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L122:
	movdqa	32(%rsp), %xmm2
	jmp	.L110
.L162:
	movdqa	(%rsp), %xmm0
	andl	$24576, %r12d
	orl	%r12d, %r14d
	call	__ieee754_y1f128@PLT
	movl	%r13d, %edi
	movaps	%xmm0, (%rsp)
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movl	%r14d, 80(%rsp)
	movdqa	%xmm0, %xmm2
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 80(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L153
	.size	__ieee754_ynf128, .-__ieee754_ynf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	352245758
	.long	3508200361
	.long	1963207094
	.long	1073619165
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC7:
	.long	0
	.long	0
	.long	2019396096
	.long	1077371717
	.align 16
.LC8:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1074553572
	.align 16
.LC9:
	.long	1280106687
	.long	3467686520
	.long	3529067575
	.long	1095443610
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC12:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
