	.text
	.p2align 4,,15
	.globl	__w_scalbln
	.type	__w_scalbln, @function
__w_scalbln:
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm3
	movsd	.LC1(%rip), %xmm2
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	jb	.L2
	pxor	%xmm4, %xmm4
	ucomisd	%xmm4, %xmm0
	jp	.L3
	jne	.L3
.L2:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$40, %rsp
	movsd	%xmm2, 24(%rsp)
	movaps	%xmm1, (%rsp)
	call	__scalbln@PLT
	movapd	(%rsp), %xmm1
	andpd	%xmm0, %xmm1
	movsd	24(%rsp), %xmm2
	ucomisd	%xmm1, %xmm2
	jb	.L6
	pxor	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm0
	jp	.L1
	jne	.L1
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$40, %rsp
	ret
	.size	__w_scalbln, .-__w_scalbln
	.weak	scalblnf32x
	.set	scalblnf32x,__w_scalbln
	.weak	scalblnf64
	.set	scalblnf64,__w_scalbln
	.weak	scalbln
	.set	scalbln,__w_scalbln
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
