	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"powl"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__kernel_standard_l
	.type	__kernel_standard_l, @function
__kernel_standard_l:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$120, %rsp
	leaq	32(%rsp), %rbx
	movq	%rbx, %rdi
	call	__GI_feholdexcept
	fldt	144(%rsp)
	fstpl	24(%rsp)
	movsd	24(%rsp), %xmm0
	fldt	160(%rsp)
	fstpl	24(%rsp)
	movsd	24(%rsp), %xmm1
	movsd	%xmm0, 16(%rsp)
	movq	%rbx, %rdi
	movsd	%xmm1, 8(%rsp)
	call	__GI_fesetenv
	cmpl	$221, %ebp
	movsd	8(%rsp), %xmm1
	movsd	16(%rsp), %xmm0
	je	.L3
	cmpl	$222, %ebp
	je	.L4
	movl	%ebp, %edi
	call	__kernel_standard@PLT
	movsd	%xmm0, 8(%rsp)
	fldl	8(%rsp)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	fldt	144(%rsp)
	fldz
	leaq	.LC0(%rip), %rax
	movsd	%xmm0, 80(%rsp)
	movsd	%xmm1, 88(%rsp)
	movl	$4, 64(%rsp)
	movq	$0x000000000, 96(%rsp)
	movq	%rax, 72(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L16
	fldt	160(%rsp)
	fmuls	.LC1(%rip)
	fld	%st(0)
	frndint
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L27
	je	.L16
.L27:
	movq	.LC8(%rip), %rax
	movq	%rax, 96(%rsp)
.L16:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$2, (%rax)
	jne	.L19
.L21:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L20:
	fldl	96(%rsp)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	fldt	160(%rsp)
	leaq	.LC0(%rip), %rax
	movl	$3, 64(%rsp)
	movq	%rax, 72(%rsp)
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	movsd	%xmm0, 80(%rsp)
	movsd	%xmm1, 88(%rsp)
	fmuls	.LC1(%rip)
	jne	.L5
	fldt	144(%rsp)
	fldz
	movq	.LC2(%rip), %rax
	movq	%rax, 96(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L37
	fld	%st(0)
	frndint
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L23
	je	.L19
.L23:
	movq	.LC4(%rip), %rax
	movq	%rax, 96(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L37:
	fstp	%st(0)
.L19:
	leaq	64(%rsp), %rdi
	call	matherr@PLT
	testl	%eax, %eax
	je	.L21
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L5:
	fldt	144(%rsp)
	fldz
	movq	.LC5(%rip), %rdx
	movq	%rdx, 96(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L38
	fld	%st(0)
	frndint
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L25
	je	.L10
.L25:
	movq	.LC6(%rip), %rcx
	movq	%rcx, 96(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L38:
	fstp	%st(0)
.L10:
	cmpl	$2, %eax
	je	.L21
	jmp	.L19
	.size	__kernel_standard_l, .-__kernel_standard_l
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1056964608
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	3758096384
	.long	1206910975
	.align 8
.LC4:
	.long	3758096384
	.long	-940572673
	.align 8
.LC5:
	.long	0
	.long	2146435072
	.align 8
.LC6:
	.long	0
	.long	-1048576
	.align 8
.LC8:
	.long	0
	.long	-2147483648
