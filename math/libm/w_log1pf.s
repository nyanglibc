	.text
	.p2align 4,,15
	.globl	__w_log1pf
	.type	__w_log1pf, @function
__w_log1pf:
	movss	.LC0(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L7
.L2:
	jmp	__log1pf@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ucomiss	%xmm1, %xmm0
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__w_log1pf, .-__w_log1pf
	.weak	log1pf32
	.set	log1pf32,__w_log1pf
	.weak	log1pf
	.set	log1pf,__w_log1pf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	3212836864
