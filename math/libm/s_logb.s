	.text
	.p2align 4,,15
	.globl	__logb
	.type	__logb, @function
__logb:
	movq	%xmm0, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	je	.L7
	movq	%rax, %rdx
	sarq	$52, %rdx
	cmpq	$2047, %rdx
	je	.L8
	testq	%rdx, %rdx
	je	.L9
.L5:
	pxor	%xmm0, %xmm0
	subq	$1023, %rdx
	cvtsi2sdq	%rdx, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movapd	%xmm0, %xmm1
	movsd	.LC1(%rip), %xmm0
	andpd	.LC0(%rip), %xmm1
	divsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	mulsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	bsrq	%rax, %rax
	xorq	$63, %rax
	subl	$12, %eax
	movslq	%eax, %rdx
	negq	%rdx
	jmp	.L5
	.size	__logb, .-__logb
	.weak	logbf32x
	.set	logbf32x,__logb
	.weak	logbf64
	.set	logbf64,__logb
	.weak	logb
	.set	logb,__logb
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-1074790400
