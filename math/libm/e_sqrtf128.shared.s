	.text
#APP
	.symver __ieee754_sqrtf128,__sqrtf128_finite@GLIBC_2.26
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_sqrtf128
	.type	__ieee754_sqrtf128, @function
__ieee754_sqrtf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$32, %rsp
	movaps	%xmm0, (%rsp)
#APP
# 41 "../sysdeps/x86/fpu/e_sqrtf128.c" 1
	stmxcsr	28(%rsp)
# 0 "" 2
#NO_APP
	movabsq	$281474976710655, %rsi
	movq	8(%rsp), %rcx
	movq	(%rsp), %rax
	movq	%rcx, %rdx
	andq	%rcx, %rsi
	shrq	$63, %rcx
	shrq	$48, %rdx
	movzbl	%cl, %r8d
	movzbl	%cl, %r9d
	andw	$32767, %dx
	je	.L3
	cmpw	$32767, %dx
	je	.L4
	leaq	0(,%rsi,8), %rcx
	movq	%rax, %rsi
	movabsq	$2251799813685248, %rdi
	shrq	$61, %rsi
	salq	$3, %rax
	movzwl	%dx, %edx
	orq	%rdi, %rsi
	xorl	%edi, %edi
	leaq	-16383(%rdx), %rbx
	orq	%rcx, %rsi
	testq	%r9, %r9
	jne	.L68
.L13:
	testb	$1, %bl
	je	.L14
	movq	%rax, %rdx
	addq	%rax, %rax
	shrq	$63, %rdx
	leaq	(%rdx,%rsi,2), %rsi
.L14:
	sarq	%rbx
	movl	$52, %ecx
	movabsq	$2251799813685248, %rdx
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	(%r8,%rdx), %r9
	cmpq	%rsi, %r9
	ja	.L15
	leaq	(%r9,%rdx), %r8
	subq	%r9, %rsi
	addq	%rdx, %r11
.L15:
	movq	%rax, %r9
	shrq	%rdx
	addq	%rax, %rax
	shrq	$63, %r9
	subl	$1, %ecx
	leaq	(%r9,%rsi,2), %rsi
	jne	.L16
	movl	$61, %r10d
	movabsq	$-9223372036854775808, %rdx
	xorl	%r9d, %r9d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L21:
	cmpq	%r8, %rsi
	leaq	(%r9,%rdx), %rcx
	ja	.L17
	jne	.L18
	cmpq	%rax, %rcx
	ja	.L18
.L17:
	movq	%rcx, %r9
	xorl	%r12d, %r12d
	addq	%rdx, %r9
	setc	%r12b
	addq	%rdx, %rbp
#APP
# 43 "../sysdeps/x86/fpu/e_sqrtf128.c" 1
	subq %rcx,%rax
	sbbq %r8,%rsi
# 0 "" 2
#NO_APP
	addq	%r12, %r8
.L18:
	movq	%rax, %rcx
	shrq	%rdx
	addq	%rax, %rax
	shrq	$63, %rcx
	subl	$1, %r10d
	leaq	(%rcx,%rsi,2), %rsi
	jne	.L21
	movq	%rsi, %rdx
	orq	%rax, %rdx
	je	.L22
	cmpq	%rsi, %r8
	jnb	.L69
.L23:
	orq	$4, %rbp
.L24:
	orq	$1, %rbp
.L22:
	testb	$7, %bpl
	je	.L25
	movl	28(%rsp), %eax
	orl	$32, %edi
	andl	$24576, %eax
	je	.L26
	cmpl	$16384, %eax
	je	.L70
.L25:
	btq	$52, %r11
	leaq	16383(%rbx), %rax
	jnc	.L29
	movabsq	$-4503599627370497, %rax
	andq	%rax, %r11
	leaq	16384(%rbx), %rax
.L29:
	movq	%r11, %rdx
	movq	%r11, %rsi
	shrq	$3, %rbp
	salq	$61, %rdx
	salq	$13, %rsi
	andw	$32767, %ax
	orq	%rdx, %rbp
	shrq	$16, %rsi
	xorl	%r8d, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rsi, %rbp
	orq	%rax, %rbp
	je	.L11
	movq	%rsi, %rdi
	movabsq	$140737488355328, %r11
	movq	%rax, %rbp
	shrq	$47, %rdi
	orq	%r11, %rsi
	xorl	$1, %edi
.L12:
	movl	$32767, %eax
.L31:
	movabsq	$281474976710655, %r11
	movq	$0, 8(%rsp)
	movabsq	$-281474976710656, %rdx
	andq	%r11, %rsi
	movq	8(%rsp), %r11
	salq	$48, %rax
	salq	$63, %r8
	movq	%rbp, (%rsp)
	andq	%rdx, %r11
	movabsq	$-9223090561878065153, %rdx
	orq	%r11, %rsi
	andq	%rdx, %rsi
	orq	%rax, %rsi
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rsi
	orq	%r8, %rsi
	testl	%edi, %edi
	movq	%rsi, 8(%rsp)
	movdqa	(%rsp), %xmm0
	je	.L1
	call	__sfp_handle_exceptions@PLT
	movdqa	(%rsp), %xmm0
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r8d, %r8d
	testq	%r9, %r9
	jne	.L71
.L6:
	movq	$0, 8(%rsp)
	movq	8(%rsp), %rax
	movabsq	$-281474976710656, %rcx
	salq	$48, %rdx
	salq	$63, %r8
	movq	$0, (%rsp)
	andq	%rcx, %rax
	movabsq	$-9223090561878065153, %rcx
	andq	%rcx, %rax
	orq	%rax, %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rdx
	orq	%r8, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rsi, %rbx
	orq	%rax, %rbx
	je	.L6
	testq	%rsi, %rsi
	je	.L7
	bsrq	%rsi, %rdx
	xorq	$63, %rdx
	movslq	%edx, %rdx
.L8:
	leaq	-15(%rdx), %rdi
	cmpq	$60, %rdi
	jg	.L9
	leal	3(%rdi), %r8d
	movq	%rax, %rbx
	movl	%r8d, %ecx
	salq	%cl, %rsi
	movl	$61, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rbx
	movl	%r8d, %ecx
	orq	%rbx, %rsi
	salq	%cl, %rax
.L10:
	movq	$-16367, %rbx
	movl	$2, %edi
	subq	%rdx, %rbx
	testq	%r9, %r9
	je	.L13
.L68:
	orl	$1, %edi
	movl	$1, %r8d
	movabsq	$140737488355328, %rsi
	xorl	%ebp, %ebp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %r8d
	movabsq	$140737488355328, %rsi
	movl	$1, %edi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L9:
	leal	-61(%rdi), %ecx
	salq	%cl, %rax
	movq	%rax, %rsi
	xorl	%eax, %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	bsrq	%rax, %rdx
	xorq	$63, %rdx
	movslq	%edx, %rdx
	addq	$64, %rdx
	jmp	.L8
.L69:
	jne	.L24
	cmpq	%rax, %r9
	jb	.L23
	jmp	.L24
.L26:
	movq	%rbp, %rax
	andl	$15, %eax
	cmpq	$4, %rax
	je	.L25
#APP
# 44 "../sysdeps/x86/fpu/e_sqrtf128.c" 1
	addq $4,%rbp
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L25
.L70:
#APP
# 44 "../sysdeps/x86/fpu/e_sqrtf128.c" 1
	addq $8,%rbp
	adcq $0,%r11
# 0 "" 2
#NO_APP
	jmp	.L25
	.size	__ieee754_sqrtf128, .-__ieee754_sqrtf128
