	.text
	.p2align 4,,15
	.globl	__expm1
	.type	__expm1, @function
__expm1:
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$32, %rdx
	movl	%edx, %eax
	andl	$2147483647, %eax
	cmpl	$1078159481, %eax
	jbe	.L2
	cmpl	$1082535489, %eax
	ja	.L36
.L3:
	testl	%edx, %edx
	jns	.L37
	movsd	.LC7(%rip), %xmm1
	addsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm0
	subsd	.LC8(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1071001154, %eax
	ja	.L38
	cmpl	$1016070143, %eax
	jbe	.L39
	movsd	.LC0(%rip), %xmm2
	xorl	%ecx, %ecx
.L13:
	movapd	%xmm0, %xmm8
	movapd	%xmm0, %xmm4
	movsd	.LC13(%rip), %xmm1
	testl	%ecx, %ecx
	mulsd	%xmm2, %xmm8
	movsd	.LC15(%rip), %xmm6
	movsd	.LC8(%rip), %xmm3
	mulsd	%xmm8, %xmm4
	mulsd	%xmm4, %xmm1
	movapd	%xmm4, %xmm7
	mulsd	%xmm4, %xmm6
	mulsd	%xmm4, %xmm7
	addsd	.LC14(%rip), %xmm1
	addsd	%xmm3, %xmm6
	mulsd	%xmm7, %xmm1
	mulsd	%xmm7, %xmm7
	addsd	%xmm6, %xmm1
	movsd	.LC16(%rip), %xmm6
	mulsd	%xmm4, %xmm6
	addsd	.LC17(%rip), %xmm6
	mulsd	%xmm7, %xmm6
	movsd	.LC19(%rip), %xmm7
	addsd	%xmm6, %xmm1
	movsd	.LC18(%rip), %xmm6
	mulsd	%xmm1, %xmm8
	subsd	%xmm8, %xmm6
	subsd	%xmm6, %xmm1
	mulsd	%xmm0, %xmm6
	subsd	%xmm6, %xmm7
	divsd	%xmm7, %xmm1
	mulsd	%xmm4, %xmm1
	je	.L40
	subsd	%xmm5, %xmm1
	cmpl	$-1, %ecx
	mulsd	%xmm0, %xmm1
	subsd	%xmm5, %xmm1
	subsd	%xmm4, %xmm1
	je	.L41
	cmpl	$1, %ecx
	je	.L42
	leal	1(%rcx), %eax
	movl	%ecx, %edx
	sall	$20, %edx
	cmpl	$57, %eax
	ja	.L43
	cmpl	$19, %ecx
	jle	.L44
	movl	$1023, %eax
	subl	%ecx, %eax
	sall	$20, %eax
	salq	$32, %rax
	movq	%rax, -8(%rsp)
	addsd	-8(%rsp), %xmm1
	subsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	movq	%xmm0, %rax
.L34:
	movq	%rax, %rcx
	movl	%eax, %eax
	shrq	$32, %rcx
	addl	%ecx, %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$2146435071, %eax
	jbe	.L4
	movl	%edx, %eax
	andl	$1048575, %eax
	orl	%ecx, %eax
	jne	.L45
	testl	%edx, %edx
	js	.L25
	rep ret
	.p2align 4,,10
	.p2align 3
.L39:
	movapd	%xmm0, %xmm1
	movsd	.LC12(%rip), %xmm2
	andpd	.LC11(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jbe	.L14
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
.L14:
	movsd	.LC6(%rip), %xmm1
	addsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm1
	subsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$1072734897, %eax
	ja	.L9
	testl	%edx, %edx
	movapd	%xmm0, %xmm5
	jns	.L46
	addsd	.LC9(%rip), %xmm5
	movsd	.LC2(%rip), %xmm1
	movsd	.LC0(%rip), %xmm2
	movl	$-1, %ecx
	.p2align 4,,10
	.p2align 3
.L11:
	movapd	%xmm5, %xmm0
	subsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm5
	subsd	%xmm1, %xmm5
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	.LC5(%rip), %xmm0
	jbe	.L3
	movq	errno@gottpoff(%rip), %rax
	movsd	.LC6(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	mulsd	%xmm0, %xmm1
	subsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
	ret
.L46:
	subsd	.LC9(%rip), %xmm5
	movl	$1, %ecx
	movsd	.LC3(%rip), %xmm1
	movsd	.LC0(%rip), %xmm2
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L37:
	movsd	.LC10(%rip), %xmm3
	mulsd	%xmm0, %xmm3
.L33:
	movsd	.LC0(%rip), %xmm2
	movapd	%xmm2, %xmm1
.L12:
	addsd	%xmm3, %xmm1
	movsd	.LC9(%rip), %xmm3
	movapd	%xmm0, %xmm5
	cvttsd2si	%xmm1, %ecx
	pxor	%xmm1, %xmm1
	cvtsi2sd	%ecx, %xmm1
	mulsd	%xmm1, %xmm3
	mulsd	.LC3(%rip), %xmm1
	subsd	%xmm3, %xmm5
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L41:
	subsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movsd	.LC10(%rip), %xmm3
	testl	%edx, %edx
	mulsd	%xmm0, %xmm3
	jns	.L33
	movsd	.LC1(%rip), %xmm1
	movsd	.LC0(%rip), %xmm2
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L42:
	movsd	.LC20(%rip), %xmm4
	ucomisd	%xmm0, %xmm4
	jbe	.L31
	addsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movsd	.LC21(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	subsd	%xmm0, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	movq	%xmm2, %rcx
	movq	%xmm2, %rax
	shrq	$32, %rcx
	movl	%eax, %eax
	addl	%ecx, %edx
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	subsd	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	subsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$2097152, %eax
	subsd	%xmm0, %xmm1
	sarl	%cl, %eax
	movl	%eax, %ecx
	movl	$1072693248, %eax
	subl	%ecx, %eax
	salq	$32, %rax
	movq	%rax, -8(%rsp)
	movsd	-8(%rsp), %xmm2
	subsd	%xmm1, %xmm2
	movq	%xmm2, %rax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L25:
	movsd	.LC4(%rip), %xmm0
	ret
	.size	__expm1, .-__expm1
	.weak	expm1f32x
	.set	expm1f32x,__expm1
	.weak	expm1f64
	.set	expm1f64,__expm1
	.weak	expm1
	.set	expm1,__expm1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1071644672
	.align 8
.LC1:
	.long	0
	.long	-1075838976
	.align 8
.LC2:
	.long	897137782
	.long	-1108723217
	.align 8
.LC3:
	.long	897137782
	.long	1038760431
	.align 8
.LC4:
	.long	0
	.long	-1074790400
	.align 8
.LC5:
	.long	4277811695
	.long	1082535490
	.align 8
.LC6:
	.long	2281731484
	.long	2117592124
	.align 8
.LC7:
	.long	3271095129
	.long	27618847
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	4276092928
	.long	1072049730
	.align 8
.LC10:
	.long	1697350398
	.long	1073157447
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC12:
	.long	0
	.long	1048576
	.align 8
.LC13:
	.long	2661997495
	.long	-1089155559
	.align 8
.LC14:
	.long	436098437
	.long	1062863264
	.align 8
.LC15:
	.long	286331124
	.long	-1079963375
	.align 8
.LC16:
	.long	1846133549
	.long	-1098187337
	.align 8
.LC17:
	.long	2263241273
	.long	1053872074
	.align 8
.LC18:
	.long	0
	.long	1074266112
	.align 8
.LC19:
	.long	0
	.long	1075314688
	.align 8
.LC20:
	.long	0
	.long	-1076887552
	.align 8
.LC21:
	.long	0
	.long	-1073741824
