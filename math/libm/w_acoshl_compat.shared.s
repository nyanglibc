	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__acoshl
	.type	__acoshl, @function
__acoshl:
	subq	$8, %rsp
	fldt	16(%rsp)
	fld1
	fucomip	%st(1), %st
	ja	.L8
.L2:
	fstpt	16(%rsp)
	addq	$8, %rsp
	jmp	__ieee754_acoshl@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$32, %rsp
	movl	$229, %edi
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$40, %rsp
	ret
	.size	__acoshl, .-__acoshl
	.weak	acoshf64x
	.set	acoshf64x,__acoshl
	.weak	acoshl
	.set	acoshl,__acoshl
