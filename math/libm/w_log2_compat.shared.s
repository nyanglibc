	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __log2_compat,log2@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__log2_compat
	.type	__log2_compat, @function
__log2_compat:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L10
.L2:
	jmp	__ieee754_log2@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$24, %rsp
	ucomisd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)
	jp	.L3
	jne	.L3
	movl	$4, %edi
	call	__GI_feraiseexcept
	movsd	8(%rsp), %xmm0
	movl	$48, %edi
	addq	$24, %rsp
	movapd	%xmm0, %xmm1
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movsd	8(%rsp), %xmm0
	movl	$49, %edi
	addq	$24, %rsp
	movapd	%xmm0, %xmm1
	jmp	__kernel_standard@PLT
	.size	__log2_compat, .-__log2_compat
