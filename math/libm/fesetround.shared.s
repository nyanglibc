	.text
	.p2align 4,,15
	.globl	__GI___fesetround
	.hidden	__GI___fesetround
	.type	__GI___fesetround, @function
__GI___fesetround:
	movl	%edi, %eax
	andl	$-3073, %eax
	jne	.L3
#APP
# 32 "../sysdeps/x86_64/fpu/fesetround.c" 1
	fnstcw -6(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-6(%rsp), %edx
	andb	$-13, %dh
	orl	%edi, %edx
	movw	%dx, -6(%rsp)
#APP
# 35 "../sysdeps/x86_64/fpu/fesetround.c" 1
	fldcw -6(%rsp)
# 0 "" 2
# 39 "../sysdeps/x86_64/fpu/fesetround.c" 1
	stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-4(%rsp), %edx
	sall	$3, %edi
	andb	$-97, %dh
	orl	%edx, %edi
	movl	%edi, -4(%rsp)
#APP
# 42 "../sysdeps/x86_64/fpu/fesetround.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %eax
	ret
	.size	__GI___fesetround, .-__GI___fesetround
	.globl	__fesetround
	.set	__fesetround,__GI___fesetround
	.weak	__GI_fesetround
	.hidden	__GI_fesetround
	.set	__GI_fesetround,__fesetround
	.weak	fesetround
	.set	fesetround,__GI_fesetround
