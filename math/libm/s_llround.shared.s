	.text
	.p2align 4,,15
	.globl	__llround
	.type	__llround, @function
__llround:
	movq	%xmm0, %rsi
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$52, %rsi
	sarq	$63, %rax
	andl	$2047, %esi
	orl	$1, %eax
	leal	-1023(%rsi), %ecx
	cmpl	$62, %ecx
	jg	.L3
	testl	%ecx, %ecx
	js	.L11
	movabsq	$4503599627370495, %rdi
	andq	%rdi, %rdx
	addq	$1, %rdi
	orq	%rdi, %rdx
	cmpl	$51, %ecx
	jg	.L12
	movabsq	$2251799813685248, %rsi
	cltq
	shrq	%cl, %rsi
	addq	%rsi, %rdx
	movl	$52, %esi
	subl	%ecx, %esi
	movl	%esi, %ecx
	sarq	%cl, %rdx
	imulq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cvttsd2siq	%xmm0, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movslq	%eax, %rdx
	cmpl	$-1, %ecx
	movl	$0, %eax
	cmove	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leal	-1075(%rsi), %ecx
	cltq
	salq	%cl, %rdx
	imulq	%rdx, %rax
	ret
	.size	__llround, .-__llround
	.globl	__lround
	.set	__lround,__llround
	.weak	lroundf32x
	.set	lroundf32x,__lround
	.weak	lroundf64
	.set	lroundf64,__lround
	.weak	lround
	.set	lround,__lround
	.weak	llroundf32x
	.set	llroundf32x,__llround
	.weak	llroundf64
	.set	llroundf64,__llround
	.weak	llround
	.set	llround,__llround
