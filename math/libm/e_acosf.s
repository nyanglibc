	.text
	.p2align 4,,15
	.globl	__ieee754_acosf
	.type	__ieee754_acosf, @function
__ieee754_acosf:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	cmpl	$1065353216, %eax
	je	.L10
	jg	.L11
	cmpl	$1056964607, %eax
	jg	.L5
	cmpl	$847249408, %eax
	jg	.L6
	movss	.LC4(%rip), %xmm0
	addss	.LC3(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	subss	%xmm0, %xmm0
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%edx, %edx
	pxor	%xmm0, %xmm0
	jle	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%edx, %edx
	js	.L13
	movss	.LC15(%rip), %xmm5
	movaps	%xmm5, %xmm1
	movss	.LC11(%rip), %xmm4
	subss	%xmm0, %xmm1
	movss	.LC5(%rip), %xmm0
	mulss	.LC16(%rip), %xmm1
	mulss	%xmm1, %xmm0
	sqrtss	%xmm1, %xmm3
	movd	%xmm3, %eax
	mulss	%xmm1, %xmm4
	andl	$-4096, %eax
	movl	%eax, -4(%rsp)
	addss	.LC6(%rip), %xmm0
	movd	-4(%rsp), %xmm2
	subss	.LC12(%rip), %xmm4
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	subss	.LC7(%rip), %xmm0
	addss	.LC13(%rip), %xmm4
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	addss	.LC8(%rip), %xmm0
	subss	.LC14(%rip), %xmm4
	mulss	%xmm1, %xmm0
	mulss	%xmm1, %xmm4
	subss	.LC9(%rip), %xmm0
	addss	%xmm5, %xmm4
	mulss	%xmm1, %xmm0
	addss	.LC10(%rip), %xmm0
	mulss	%xmm1, %xmm0
	divss	%xmm4, %xmm0
	movaps	%xmm2, %xmm4
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm3
	subss	%xmm4, %xmm1
	divss	%xmm3, %xmm1
	addss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movss	.LC2(%rip), %xmm0
	addss	.LC1(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movaps	%xmm0, %xmm3
	movss	.LC5(%rip), %xmm1
	mulss	%xmm0, %xmm3
	movss	.LC11(%rip), %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	addss	.LC6(%rip), %xmm1
	subss	.LC12(%rip), %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	subss	.LC7(%rip), %xmm1
	addss	.LC13(%rip), %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	addss	.LC8(%rip), %xmm1
	subss	.LC14(%rip), %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm2
	subss	.LC9(%rip), %xmm1
	addss	.LC15(%rip), %xmm2
	mulss	%xmm3, %xmm1
	addss	.LC10(%rip), %xmm1
	mulss	%xmm3, %xmm1
	divss	%xmm2, %xmm1
	movss	.LC4(%rip), %xmm2
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm2
	subss	%xmm2, %xmm0
	movaps	%xmm0, %xmm1
	movss	.LC3(%rip), %xmm0
	subss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movaps	%xmm0, %xmm1
	movss	.LC15(%rip), %xmm3
	addss	%xmm3, %xmm1
	movss	.LC16(%rip), %xmm4
	movss	.LC5(%rip), %xmm0
	mulss	%xmm1, %xmm4
	movss	.LC11(%rip), %xmm1
	mulss	%xmm4, %xmm0
	sqrtss	%xmm4, %xmm2
	mulss	%xmm4, %xmm1
	addss	.LC6(%rip), %xmm0
	subss	.LC12(%rip), %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm1
	subss	.LC7(%rip), %xmm0
	addss	.LC13(%rip), %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm1
	addss	.LC8(%rip), %xmm0
	subss	.LC14(%rip), %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm1
	subss	.LC9(%rip), %xmm0
	addss	%xmm3, %xmm1
	mulss	%xmm4, %xmm0
	addss	.LC10(%rip), %xmm0
	mulss	%xmm4, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm2, %xmm0
	subss	.LC4(%rip), %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	movss	.LC1(%rip), %xmm0
	subss	%xmm1, %xmm0
	ret
	.size	__ieee754_acosf, .-__ieee754_acosf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1078530010
	.align 4
.LC2:
	.long	874652008
	.align 4
.LC3:
	.long	1070141402
	.align 4
.LC4:
	.long	866263400
	.align 4
.LC5:
	.long	940699400
	.align 4
.LC6:
	.long	978288388
	.align 4
.LC7:
	.long	1025773894
	.align 4
.LC8:
	.long	1045301928
	.align 4
.LC9:
	.long	1051111568
	.align 4
.LC10:
	.long	1042983595
	.align 4
.LC11:
	.long	1033750062
	.align 4
.LC12:
	.long	1060123489
	.align 4
.LC13:
	.long	1073829677
	.align 4
.LC14:
	.long	1075433785
	.align 4
.LC15:
	.long	1065353216
	.align 4
.LC16:
	.long	1056964608
