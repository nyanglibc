	.text
	.p2align 4,,15
	.globl	__ctanhf
	.type	__ctanhf, @function
__ctanhf:
	pushq	%rbx
	subq	$96, %rsp
	movq	%xmm0, 72(%rsp)
	movss	.LC2(%rip), %xmm3
	movss	.LC3(%rip), %xmm1
	movss	72(%rsp), %xmm2
	movaps	%xmm2, %xmm5
	movss	76(%rsp), %xmm0
	andps	%xmm3, %xmm5
	ucomiss	%xmm5, %xmm1
	jb	.L2
	movaps	%xmm0, %xmm6
	andps	%xmm3, %xmm6
	ucomiss	%xmm6, %xmm1
	jb	.L52
	movsd	.LC8(%rip), %xmm1
	movaps	%xmm2, %xmm4
	ucomiss	.LC10(%rip), %xmm6
	mulsd	.LC7(%rip), %xmm1
	mulsd	.LC9(%rip), %xmm1
	cvttsd2si	%xmm1, %ebx
	jbe	.L45
	leaq	92(%rsp), %rsi
	leaq	88(%rsp), %rdi
	movss	%xmm2, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movss	%xmm2, 16(%rsp)
	movss	%xmm5, 12(%rsp)
	call	__sincosf@PLT
	movss	12(%rsp), %xmm5
	movaps	48(%rsp), %xmm3
	movss	16(%rsp), %xmm4
	movss	32(%rsp), %xmm2
.L16:
	pxor	%xmm6, %xmm6
	cvtsi2ss	%ebx, %xmm6
	ucomiss	%xmm6, %xmm5
	ja	.L53
	ucomiss	.LC10(%rip), %xmm5
	ja	.L54
	movss	.LC1(%rip), %xmm2
.L22:
	movss	92(%rsp), %xmm1
	movaps	%xmm1, %xmm7
	movaps	%xmm1, %xmm6
	andps	%xmm3, %xmm7
	mulss	%xmm1, %xmm6
	mulss	.LC12(%rip), %xmm7
	ucomiss	%xmm7, %xmm5
	jbe	.L24
	movaps	%xmm4, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm0, %xmm6
.L24:
	mulss	88(%rsp), %xmm1
	mulss	%xmm4, %xmm2
	divss	%xmm6, %xmm2
	divss	%xmm6, %xmm1
	movaps	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
.L21:
	movaps	%xmm2, %xmm6
	movss	.LC10(%rip), %xmm0
	andps	%xmm3, %xmm6
	ucomiss	%xmm6, %xmm0
	jbe	.L26
	mulss	%xmm2, %xmm2
.L26:
	andps	%xmm1, %xmm3
	ucomiss	%xmm3, %xmm0
	jbe	.L1
	mulss	%xmm1, %xmm1
.L1:
	movss	%xmm4, 64(%rsp)
	movss	%xmm5, 68(%rsp)
	movq	64(%rsp), %xmm0
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	pxor	%xmm0, %xmm0
	addl	%ebx, %ebx
	movaps	%xmm3, 48(%rsp)
	movss	%xmm2, 32(%rsp)
	movss	%xmm6, 16(%rsp)
	cvtsi2ss	%ebx, %xmm0
	movss	%xmm5, 12(%rsp)
	call	__ieee754_expf@PLT
	movss	.LC11(%rip), %xmm1
	mulss	88(%rsp), %xmm1
	movss	16(%rsp), %xmm6
	movss	12(%rsp), %xmm5
	subss	%xmm6, %xmm5
	movss	32(%rsp), %xmm2
	andps	.LC5(%rip), %xmm2
	movaps	48(%rsp), %xmm3
	mulss	92(%rsp), %xmm1
	ucomiss	%xmm6, %xmm5
	orps	.LC4(%rip), %xmm2
	divss	%xmm0, %xmm1
	jbe	.L47
	movaps	%xmm2, %xmm4
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm5
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L54:
	movaps	%xmm2, %xmm0
	movss	%xmm2, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	call	__ieee754_sinhf@PLT
	movss	%xmm0, 12(%rsp)
	movss	16(%rsp), %xmm2
	movaps	%xmm2, %xmm0
	call	__ieee754_coshf@PLT
	movss	12(%rsp), %xmm4
	movaps	%xmm4, %xmm5
	movaps	32(%rsp), %xmm3
	movaps	%xmm0, %xmm2
	andps	%xmm3, %xmm5
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L47:
	addss	%xmm5, %xmm5
	movss	%xmm1, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	movss	%xmm2, 12(%rsp)
	movaps	%xmm5, %xmm0
	call	__ieee754_expf@PLT
	movss	16(%rsp), %xmm1
	divss	%xmm0, %xmm1
	movss	12(%rsp), %xmm2
	movaps	%xmm2, %xmm4
	movaps	32(%rsp), %xmm3
	movaps	%xmm1, %xmm5
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	.LC3(%rip), %xmm5
	jbe	.L5
	andps	%xmm0, %xmm3
	movss	.LC5(%rip), %xmm6
	andps	%xmm6, %xmm2
	ucomiss	%xmm3, %xmm1
	orps	.LC4(%rip), %xmm2
	jb	.L6
	ucomiss	.LC1(%rip), %xmm3
	ja	.L55
.L6:
	movaps	%xmm0, %xmm5
	movaps	%xmm2, %xmm4
	andps	%xmm6, %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	ucomiss	.LC3(%rip), %xmm5
	ja	.L56
.L5:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jp	.L10
	je	.L57
.L10:
	ucomiss	%xmm1, %xmm2
	jp	.L32
	jne	.L32
	movaps	%xmm2, %xmm4
	movss	.LC0(%rip), %xmm5
.L12:
	andps	%xmm3, %xmm0
	ucomiss	.LC3(%rip), %xmm0
	jbe	.L1
	movl	$1, %edi
	movss	%xmm5, 16(%rsp)
	movss	%xmm4, 12(%rsp)
	call	feraiseexcept@PLT
	movss	12(%rsp), %xmm4
	movss	16(%rsp), %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L45:
	movss	%xmm0, 88(%rsp)
	movl	$0x3f800000, 92(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L56:
	movss	.LC5(%rip), %xmm6
	andps	%xmm6, %xmm2
	orps	.LC4(%rip), %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L32:
	movss	.LC0(%rip), %xmm4
	movaps	%xmm4, %xmm5
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	92(%rsp), %rsi
	leaq	88(%rsp), %rdi
	movss	%xmm2, 12(%rsp)
	movaps	%xmm6, 16(%rsp)
	call	__sincosf@PLT
	movss	88(%rsp), %xmm5
	mulss	92(%rsp), %xmm5
	movss	12(%rsp), %xmm2
	movaps	16(%rsp), %xmm6
	movaps	%xmm2, %xmm4
	andps	%xmm6, %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movaps	%xmm2, %xmm4
	movaps	%xmm0, %xmm5
	jmp	.L1
	.size	__ctanhf, .-__ctanhf
	.weak	ctanhf32
	.set	ctanhf32,__ctanhf
	.weak	ctanhf
	.set	ctanhf,__ctanhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.align 4
.LC1:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	1065353216
	.long	0
	.long	0
	.long	0
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1080016896
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	8388608
	.align 4
.LC11:
	.long	1082130432
	.align 4
.LC12:
	.long	872415232
