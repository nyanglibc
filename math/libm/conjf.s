	.text
	.p2align 4,,15
	.globl	__conjf
	.type	__conjf, @function
__conjf:
	movq	%xmm0, -8(%rsp)
	movss	-4(%rsp), %xmm0
	xorps	.LC0(%rip), %xmm0
	movss	-8(%rsp), %xmm1
	movss	%xmm1, -16(%rsp)
	movss	%xmm0, -12(%rsp)
	movq	-16(%rsp), %xmm0
	ret
	.size	__conjf, .-__conjf
	.weak	conjf32
	.set	conjf32,__conjf
	.weak	conjf
	.set	conjf,__conjf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483648
	.long	0
	.long	0
	.long	0
