	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __pow_compat,pow@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pow_compat
	.type	__pow_compat, @function
__pow_compat:
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_pow@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movsd	.LC1(%rip), %xmm3
	movsd	(%rsp), %xmm2
	andpd	%xmm1, %xmm5
	movsd	8(%rsp), %xmm4
	ucomisd	%xmm5, %xmm3
	jb	.L26
	pxor	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm0
	jp	.L1
	je	.L27
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm5
	ucomisd	%xmm5, %xmm3
	jb	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L1
	ucomisd	%xmm0, %xmm0
	jp	.L28
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm2
	jp	.L6
	jne	.L6
	ucomisd	%xmm4, %xmm1
	ja	.L29
.L6:
	movapd	%xmm4, %xmm1
	movapd	%xmm2, %xmm0
	movl	$21, %edi
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L27:
	movapd	%xmm2, %xmm6
	andpd	%xmm1, %xmm6
	ucomisd	%xmm6, %xmm3
	jb	.L1
	ucomisd	%xmm5, %xmm2
	jp	.L16
	je	.L1
.L16:
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movapd	%xmm4, %xmm1
	movapd	%xmm2, %xmm0
	movl	$22, %edi
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
.L29:
	movmskpd	%xmm2, %eax
	testb	$1, %al
	je	.L9
	movmskpd	%xmm0, %eax
	testb	$1, %al
	jne	.L30
.L9:
	movapd	%xmm4, %xmm1
	movapd	%xmm2, %xmm0
	movl	$43, %edi
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
.L30:
	movapd	%xmm4, %xmm1
	movl	$23, %edi
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
.L28:
	movapd	%xmm4, %xmm1
	movl	$24, %edi
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.size	__pow_compat, .-__pow_compat
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
