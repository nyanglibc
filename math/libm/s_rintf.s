	.text
	.p2align 4,,15
	.globl	__rintf
	.type	__rintf, @function
__rintf:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	sarl	$23, %eax
	movzbl	%al, %eax
	subl	$127, %eax
	cmpl	$22, %eax
	jg	.L2
	shrl	$31, %edx
	leaq	TWO23.4462(%rip), %rcx
	testl	%eax, %eax
	movslq	%edx, %rsi
	movss	(%rcx,%rsi,4), %xmm1
	addss	%xmm1, %xmm0
	subss	%xmm1, %xmm0
	js	.L6
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$-128, %eax
	jne	.L1
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movd	%xmm0, %eax
	sall	$31, %edx
	andl	$2147483647, %eax
	orl	%edx, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.size	__rintf, .-__rintf
	.weak	rintf32
	.set	rintf32,__rintf
	.weak	rintf
	.set	rintf,__rintf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	TWO23.4462, @object
	.size	TWO23.4462, 8
TWO23.4462:
	.long	1258291200
	.long	3405774848
