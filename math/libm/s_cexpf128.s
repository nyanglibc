	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__subtf3
	.globl	__lttf2
	.p2align 4,,15
	.globl	__cexpf128
	.type	__cexpf128, @function
__cexpf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$136, %rsp
	movdqa	160(%rsp), %xmm4
	movdqa	.LC4(%rip), %xmm2
	movdqa	176(%rsp), %xmm5
	pand	%xmm4, %xmm2
	movdqa	.LC4(%rip), %xmm3
	pand	%xmm5, %xmm3
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm5, 64(%rsp)
	movaps	%xmm3, (%rsp)
	movaps	%xmm2, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	32(%rsp), %xmm2
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 48(%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L79
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L39
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L39
	movdqa	.LC6(%rip), %xmm6
	movdqa	(%rsp), %xmm0
	movdqa	%xmm6, %xmm1
	movaps	%xmm6, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L13
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L80
	movaps	160(%rsp), %xmm5
	pxor	%xmm0, %xmm0
	movmskps	%xmm5, %eax
	testb	$8, %al
	jne	.L46
	movdqa	.LC1(%rip), %xmm0
.L46:
	movdqa	176(%rsp), %xmm3
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm3, (%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L79:
	movdqa	(%rsp), %xmm0
	movdqa	.LC6(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L8
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L81
.L8:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movdqa	.LC3(%rip), %xmm4
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm4, (%rsp)
.L30:
	movq	%rbx, %rax
	movdqa	16(%rsp), %xmm6
	movdqa	(%rsp), %xmm7
	movaps	%xmm6, (%rbx)
	movaps	%xmm7, 16(%rbx)
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L81:
	movdqa	.LC7(%rip), %xmm1
	movdqa	.LC8(%rip), %xmm0
	call	__multf3@PLT
	call	__fixtfsi@PLT
	movdqa	32(%rsp), %xmm1
	movl	%eax, %ebp
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L63
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	176(%rsp), %xmm0
	call	__sincosf128@PLT
.L19:
	movl	%ebp, %edi
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	160(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L82
.L20:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L64
	movdqa	.LC5(%rip), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
.L25:
	movdqa	16(%rsp), %xmm0
	pand	.LC4(%rip), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L26
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L26:
	movdqa	(%rsp), %xmm0
	pand	.LC4(%rip), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L30
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L64:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	96(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L82:
	movdqa	(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	160(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 96(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L20
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 112(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L13:
	movaps	160(%rsp), %xmm3
	movmskps	%xmm3, %eax
	testb	$8, %al
	jne	.L49
	movdqa	.LC1(%rip), %xmm7
	movaps	%xmm7, 16(%rsp)
.L44:
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L65
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	176(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	96(%rsp), %xmm6
	movaps	%xmm6, 64(%rsp)
	movdqa	112(%rsp), %xmm1
.L31:
	movdqa	16(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm3
	movaps	%xmm0, (%rsp)
	movaps	%xmm3, 16(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L83
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L8
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L8
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L8
	movdqa	.LC3(%rip), %xmm5
	movaps	%xmm5, 16(%rsp)
	movdqa	176(%rsp), %xmm5
	movaps	%xmm5, (%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L63:
	movdqa	176(%rsp), %xmm5
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm5, 96(%rsp)
	movaps	%xmm0, 112(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L80:
	movaps	160(%rsp), %xmm3
	movmskps	%xmm3, %eax
	testb	$8, %al
	je	.L84
	pxor	%xmm5, %xmm5
	movaps	%xmm5, 16(%rsp)
	jmp	.L44
.L65:
	movdqa	.LC2(%rip), %xmm1
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L39:
	movaps	160(%rsp), %xmm7
	movmskps	%xmm7, %eax
	testb	$8, %al
	jne	.L85
	movdqa	176(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	.LC1(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L85:
	pxor	%xmm0, %xmm0
	movdqa	176(%rsp), %xmm1
	call	__copysignf128@PLT
	pxor	%xmm6, %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	jmp	.L30
.L83:
	movdqa	.LC3(%rip), %xmm6
	movaps	%xmm6, 16(%rsp)
	movaps	%xmm6, (%rsp)
	jmp	.L30
.L84:
	movdqa	.LC1(%rip), %xmm3
	movaps	%xmm3, 16(%rsp)
	jmp	.L44
.L49:
	pxor	%xmm3, %xmm3
	movaps	%xmm3, 16(%rsp)
	jmp	.L44
	.size	__cexpf128, .-__cexpf128
	.weak	cexpf128
	.set	cexpf128,__cexpf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1074593784
	.align 16
.LC8:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
