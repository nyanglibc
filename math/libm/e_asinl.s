	.text
	.p2align 4,,15
	.globl	__ieee754_asinl
	.type	__ieee754_asinl, @function
__ieee754_asinl:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rcx
	movq	%rsi, %rdx
	movl	%ecx, %eax
	shrq	$32, %rdx
	andl	$32767, %eax
	movl	%edx, %edi
	sall	$16, %eax
	shrl	$16, %edi
	orl	%edi, %eax
	cmpl	$1073709055, %eax
	jbe	.L2
	cmpl	$1073709056, %eax
	je	.L17
.L3:
	fldt	8(%rsp)
	fsub	%st(0), %st
	fdiv	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1073643519, %eax
	fldt	8(%rsp)
	jbe	.L14
	fabs
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L5:
	fld1
	cmpl	$1073674648, %eax
	fsubp	%st, %st(1)
	fmuls	.LC16(%rip)
	fldt	.LC5(%rip)
	fmul	%st(1), %st
	fldt	.LC6(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC7(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC11(%rip)
	fsubr	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC12(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC13(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC14(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC15(%rip)
	fsubrp	%st, %st(1)
	fld	%st(2)
	fsqrt
	fld	%st(0)
	fstpt	-40(%rsp)
	ja	.L18
	fstp	%st(0)
	movq	-32(%rsp), %rax
	movl	$0, -24(%rsp)
	movw	%ax, -16(%rsp)
	movq	-40(%rsp), %rax
	shrq	$32, %rax
	testw	%cx, %cx
	movl	%eax, -20(%rsp)
	fldt	-24(%rsp)
	fldt	-40(%rsp)
	fld	%st(0)
	fadd	%st(0), %st
	fxch	%st(3)
	fdivrp	%st, %st(4)
	fxch	%st(3)
	fmulp	%st, %st(2)
	fld	%st(0)
	fmul	%st(1), %st
	fsubrp	%st, %st(4)
	fadd	%st, %st(2)
	fxch	%st(3)
	fdivp	%st, %st(2)
	fxch	%st(1)
	fadd	%st(0), %st
	fldt	.LC1(%rip)
	fsubp	%st, %st(1)
	fsubrp	%st, %st(1)
	fxch	%st(1)
	fadd	%st(0), %st
	fldt	.LC18(%rip)
	fsub	%st, %st(1)
	fxch	%st(1)
	fsubrp	%st, %st(2)
	fsubp	%st, %st(1)
	js	.L19
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$1071546367, %eax
	jg	.L6
	fabs
	fldt	.LC2(%rip)
	fucomip	%st(1), %st
	jbe	.L7
	fldt	8(%rsp)
	fmul	%st(0), %st
	fstp	%st(0)
.L7:
	fldt	.LC3(%rip)
	fldt	8(%rsp)
	fadd	%st, %st(1)
	fld1
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jbe	.L20
	fstp	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	addl	$-2147483648, %edx
	orl	%esi, %edx
	jne	.L3
	fldt	.LC0(%rip)
	fldt	8(%rsp)
	fmulp	%st, %st(1)
	fldt	.LC1(%rip)
	fldt	8(%rsp)
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	fdivrp	%st, %st(2)
	testw	%cx, %cx
	fmul	%st, %st(1)
	faddp	%st, %st(1)
	fadd	%st(0), %st
	fldt	.LC17(%rip)
	faddp	%st, %st(1)
	fldt	.LC0(%rip)
	fsubp	%st, %st(1)
	jns	.L1
.L19:
	fchs
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	fld	%st(0)
	fmul	%st(0), %st
	fldt	.LC5(%rip)
	fmul	%st(1), %st
	fldt	.LC6(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC7(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC11(%rip)
	fsubr	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC12(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC13(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC14(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(2)
	fldt	.LC15(%rip)
	fsubrp	%st, %st(2)
	fdivp	%st, %st(1)
	fmul	%st(1), %st
	faddp	%st, %st(1)
	ret
	.size	__ieee754_asinl, .-__ieee754_asinl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	560513589
	.long	3373259426
	.long	16383
	.long	0
	.align 16
.LC1:
	.long	4237266107
	.long	3974526417
	.long	49085
	.long	0
	.align 16
.LC2:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC3:
	.long	1551824589
	.long	3610030761
	.long	32766
	.long	0
	.align 16
.LC5:
	.long	889805265
	.long	4152782857
	.long	16374
	.long	0
	.align 16
.LC6:
	.long	2481372823
	.long	2629074859
	.long	16382
	.long	0
	.align 16
.LC7:
	.long	1745914869
	.long	3183858908
	.long	16385
	.long	0
	.align 16
.LC8:
	.long	2374409172
	.long	2500704123
	.long	16387
	.long	0
	.align 16
.LC9:
	.long	2653240079
	.long	3129233061
	.long	16387
	.long	0
	.align 16
.LC10:
	.long	3333024312
	.long	2707747791
	.long	16386
	.long	0
	.align 16
.LC11:
	.long	2232816169
	.long	4210231785
	.long	16386
	.long	0
	.align 16
.LC12:
	.long	4084184515
	.long	2640832835
	.long	16389
	.long	0
	.align 16
.LC13:
	.long	3188711706
	.long	2865280253
	.long	16390
	.long	0
	.align 16
.LC14:
	.long	324357225
	.long	2803857236
	.long	16390
	.long	0
	.align 16
.LC15:
	.long	2852052695
	.long	4061621687
	.long	16388
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC16:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC17:
	.long	4237266107
	.long	3974526417
	.long	16317
	.long	0
	.align 16
.LC18:
	.long	560513589
	.long	3373259426
	.long	16382
	.long	0
