	.text
	.p2align 4,,15
	.globl	__log1pf
	.type	__log1pf, @function
__log1pf:
	movd	%xmm0, %eax
	cmpl	$1054086102, %eax
	jle	.L2
	cmpl	$2139095039, %eax
	jg	.L34
	cmpl	$1509949439, %eax
	jle	.L12
	movd	%xmm0, %edx
	pxor	%xmm2, %xmm2
	movss	.LC7(%rip), %xmm4
	sarl	$23, %edx
	subl	$127, %edx
.L17:
	andl	$8388607, %eax
	cmpl	$3474678, %eax
	jg	.L18
	movl	%eax, %esi
	orl	$1065353216, %esi
	movl	%esi, -4(%rsp)
	movd	-4(%rsp), %xmm0
.L19:
	subss	%xmm4, %xmm0
	movss	.LC6(%rip), %xmm3
	testl	%eax, %eax
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm3
	jne	.L13
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jp	.L20
	jne	.L20
	testl	%edx, %edx
	pxor	%xmm0, %xmm0
	jne	.L35
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movd	%xmm0, %edx
	andl	$2147483647, %edx
	cmpl	$1065353215, %edx
	jle	.L5
	ucomiss	.LC1(%rip), %xmm0
	jp	.L6
	jne	.L6
	movss	.LC2(%rip), %xmm0
	divss	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$822083583, %edx
	jle	.L36
	addl	$1097468384, %eax
	cmpl	$1097468384, %eax
	jbe	.L12
	movss	.LC6(%rip), %xmm3
	xorl	%edx, %edx
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm3
.L13:
	movss	.LC11(%rip), %xmm1
	movaps	%xmm0, %xmm4
	addss	%xmm0, %xmm1
	testl	%edx, %edx
	divss	%xmm1, %xmm4
	movss	.LC12(%rip), %xmm1
	movaps	%xmm4, %xmm5
	mulss	%xmm4, %xmm5
	mulss	%xmm5, %xmm1
	addss	.LC13(%rip), %xmm1
	mulss	%xmm5, %xmm1
	addss	.LC14(%rip), %xmm1
	mulss	%xmm5, %xmm1
	addss	.LC15(%rip), %xmm1
	mulss	%xmm5, %xmm1
	addss	.LC16(%rip), %xmm1
	mulss	%xmm5, %xmm1
	addss	.LC17(%rip), %xmm1
	mulss	%xmm5, %xmm1
	addss	.LC10(%rip), %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm4, %xmm1
	je	.L37
	pxor	%xmm4, %xmm4
	movss	.LC9(%rip), %xmm5
	cvtsi2ss	%edx, %xmm4
	mulss	%xmm4, %xmm5
	mulss	.LC8(%rip), %xmm4
	addss	%xmm4, %xmm2
	addss	%xmm2, %xmm1
	subss	%xmm1, %xmm3
	subss	%xmm0, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movss	.LC3(%rip), %xmm1
	addss	%xmm0, %xmm1
	cmpl	$612368383, %edx
	movaps	%xmm0, %xmm1
	jg	.L10
	andps	.LC4(%rip), %xmm1
	movss	.LC5(%rip), %xmm2
	ucomiss	%xmm1, %xmm2
	jbe	.L1
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	subss	%xmm0, %xmm0
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	%eax, %edi
	movl	$8388608, %ecx
	addl	$1, %edx
	orl	$1056964608, %edi
	subl	%eax, %ecx
	movl	%edi, -4(%rsp)
	movl	%ecx, %eax
	movd	-4(%rsp), %xmm0
	sarl	$2, %eax
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L12:
	movaps	%xmm0, %xmm3
	movss	.LC7(%rip), %xmm4
	addss	%xmm4, %xmm3
	movd	%xmm3, %edx
	movd	%xmm3, %eax
	sarl	$23, %edx
	subl	$127, %edx
	testl	%edx, %edx
	jle	.L15
	movaps	%xmm3, %xmm6
	movaps	%xmm4, %xmm7
	subss	%xmm0, %xmm6
	subss	%xmm6, %xmm7
	movaps	%xmm7, %xmm0
.L16:
	movaps	%xmm0, %xmm2
	divss	%xmm3, %xmm2
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L20:
	movss	.LC10(%rip), %xmm1
	testl	%edx, %edx
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm4
	mulss	%xmm4, %xmm3
	jne	.L22
.L32:
	subss	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	subss	%xmm1, %xmm3
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L10:
	mulss	%xmm0, %xmm1
	mulss	.LC6(%rip), %xmm1
	subss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	pxor	%xmm1, %xmm1
	movss	.LC9(%rip), %xmm4
	cvtsi2ss	%edx, %xmm1
	mulss	%xmm1, %xmm4
	mulss	.LC8(%rip), %xmm1
	addss	%xmm1, %xmm2
	subss	%xmm2, %xmm3
	subss	%xmm0, %xmm3
	subss	%xmm3, %xmm4
	movaps	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	pxor	%xmm1, %xmm1
	movss	.LC8(%rip), %xmm0
	cvtsi2ss	%edx, %xmm1
	mulss	%xmm1, %xmm0
	mulss	.LC9(%rip), %xmm1
	addss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movaps	%xmm3, %xmm1
	subss	%xmm4, %xmm1
	subss	%xmm1, %xmm0
	jmp	.L16
	.size	__log1pf, .-__log1pf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	3212836864
	.align 4
.LC2:
	.long	3422552064
	.align 4
.LC3:
	.long	1275068416
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	8388608
	.align 4
.LC6:
	.long	1056964608
	.align 4
.LC7:
	.long	1065353216
	.align 4
.LC8:
	.long	924317649
	.align 4
.LC9:
	.long	1060204928
	.align 4
.LC10:
	.long	1059760811
	.align 4
.LC11:
	.long	1073741824
	.align 4
.LC12:
	.long	1041729687
	.align 4
.LC13:
	.long	1042075727
	.align 4
.LC14:
	.long	1044001573
	.align 4
.LC15:
	.long	1046711849
	.align 4
.LC16:
	.long	1049774373
	.align 4
.LC17:
	.long	1053609165
