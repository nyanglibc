	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__kernel_casinh
	.type	__kernel_casinh, @function
__kernel_casinh:
	pushq	%rbx
	movapd	%xmm0, %xmm9
	movapd	%xmm1, %xmm5
	movl	%edi, %ebx
	subq	$144, %rsp
	movapd	%xmm0, %xmm4
	movq	.LC1(%rip), %xmm3
	movsd	.LC2(%rip), %xmm7
	movapd	%xmm1, %xmm2
	andpd	%xmm3, %xmm9
	movapd	%xmm1, %xmm6
	andpd	%xmm3, %xmm5
	ucomisd	%xmm7, %xmm9
	jnb	.L2
	ucomisd	%xmm7, %xmm5
	jb	.L67
.L2:
	testl	%ebx, %ebx
	jne	.L5
	movapd	%xmm9, %xmm0
	movapd	%xmm5, %xmm1
.L6:
	movaps	%xmm3, 32(%rsp)
	movsd	%xmm4, 16(%rsp)
	movsd	%xmm6, (%rsp)
	call	__clog@PLT
	addsd	.LC3(%rip), %xmm0
	movsd	16(%rsp), %xmm4
	movq	.LC4(%rip), %xmm7
	movapd	%xmm4, %xmm2
	movapd	32(%rsp), %xmm3
	andpd	%xmm7, %xmm2
	andpd	%xmm3, %xmm0
	movsd	(%rsp), %xmm6
	orpd	%xmm2, %xmm0
.L7:
	testl	%ebx, %ebx
	je	.L47
	movsd	.LC0(%rip), %xmm6
.L47:
	movapd	%xmm6, %xmm5
	andpd	%xmm3, %xmm1
	addq	$144, %rsp
	andpd	%xmm7, %xmm5
	popq	%rbx
	orpd	%xmm5, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movapd	%xmm2, %xmm0
	movapd	%xmm9, %xmm1
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L67:
	movsd	.LC5(%rip), %xmm7
	movsd	.LC6(%rip), %xmm0
	ucomisd	%xmm7, %xmm9
	jb	.L8
	ucomisd	%xmm5, %xmm0
	ja	.L80
.L8:
	ucomisd	%xmm9, %xmm0
	ja	.L81
.L14:
	movsd	.LC0(%rip), %xmm8
	ucomisd	%xmm8, %xmm5
	jbe	.L68
	movsd	.LC7(%rip), %xmm0
	ucomisd	%xmm5, %xmm0
	jbe	.L20
	ucomisd	%xmm9, %xmm7
	jbe	.L20
	movapd	%xmm5, %xmm0
	movapd	%xmm5, %xmm10
	subsd	%xmm8, %xmm0
	addsd	%xmm8, %xmm10
	mulsd	%xmm0, %xmm10
	movsd	.LC8(%rip), %xmm0
	ucomisd	%xmm9, %xmm0
	jbe	.L69
	sqrtsd	%xmm10, %xmm11
	mulsd	%xmm11, %xmm5
	movsd	%xmm7, (%rsp)
	movaps	%xmm3, 80(%rsp)
	movsd	%xmm4, 64(%rsp)
	movapd	%xmm5, %xmm0
	movsd	%xmm9, 48(%rsp)
	movsd	%xmm6, 32(%rsp)
	addsd	%xmm10, %xmm0
	movsd	%xmm11, 16(%rsp)
	movsd	%xmm8, 112(%rsp)
	movsd	%xmm2, 104(%rsp)
	addsd	%xmm0, %xmm0
	call	__log1p@PLT
	movsd	(%rsp), %xmm7
	testl	%ebx, %ebx
	movsd	16(%rsp), %xmm11
	mulsd	%xmm7, %xmm0
	movapd	80(%rsp), %xmm3
	movsd	32(%rsp), %xmm6
	movsd	48(%rsp), %xmm9
	movsd	%xmm0, (%rsp)
	movsd	64(%rsp), %xmm4
	je	.L25
	movsd	104(%rsp), %xmm2
	movapd	%xmm11, %xmm1
	movq	.LC4(%rip), %xmm7
	movapd	%xmm2, %xmm6
	andpd	%xmm3, %xmm1
	movapd	%xmm9, %xmm0
	movsd	%xmm4, 48(%rsp)
	andpd	%xmm7, %xmm6
	movaps	%xmm7, 32(%rsp)
	orpd	%xmm6, %xmm1
	movaps	%xmm3, 16(%rsp)
	call	__ieee754_atan2@PLT
	movapd	%xmm0, %xmm1
	movapd	16(%rsp), %xmm3
	movsd	(%rsp), %xmm0
	movapd	32(%rsp), %xmm7
	movsd	48(%rsp), %xmm4
	movsd	112(%rsp), %xmm8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L81:
	ucomisd	.LC7(%rip), %xmm5
	jb	.L14
	movsd	.LC0(%rip), %xmm8
	movapd	%xmm5, %xmm10
	movapd	%xmm5, %xmm0
	movsd	%xmm4, 48(%rsp)
	addsd	%xmm8, %xmm10
	movsd	%xmm9, 32(%rsp)
	subsd	%xmm8, %xmm0
	movsd	%xmm6, 16(%rsp)
	movaps	%xmm3, 64(%rsp)
	movsd	%xmm2, 104(%rsp)
	mulsd	%xmm0, %xmm10
	movsd	%xmm8, 80(%rsp)
	sqrtsd	%xmm10, %xmm10
	addsd	%xmm10, %xmm5
	movsd	%xmm10, (%rsp)
	movapd	%xmm5, %xmm0
	call	__ieee754_log@PLT
	testl	%ebx, %ebx
	movsd	(%rsp), %xmm10
	movsd	16(%rsp), %xmm6
	movsd	32(%rsp), %xmm9
	movsd	48(%rsp), %xmm4
	movapd	64(%rsp), %xmm3
	je	.L17
	movapd	%xmm10, %xmm1
	movsd	104(%rsp), %xmm2
	movq	.LC4(%rip), %xmm7
	movapd	%xmm2, %xmm6
	andpd	%xmm3, %xmm1
	movsd	%xmm0, 32(%rsp)
	movapd	%xmm9, %xmm0
	andpd	%xmm7, %xmm6
	movaps	%xmm7, 16(%rsp)
	orpd	%xmm6, %xmm1
	movaps	%xmm3, (%rsp)
	call	__ieee754_atan2@PLT
	movapd	%xmm0, %xmm1
	movsd	32(%rsp), %xmm5
	movapd	(%rsp), %xmm3
	movapd	%xmm5, %xmm0
	movapd	16(%rsp), %xmm7
	movsd	48(%rsp), %xmm4
	movsd	80(%rsp), %xmm8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	ucomisd	%xmm5, %xmm8
	jbe	.L28
	ucomisd	%xmm9, %xmm7
	jbe	.L28
	ucomisd	.LC12(%rip), %xmm5
	jb	.L71
	movapd	%xmm8, %xmm1
	movapd	%xmm5, %xmm0
	subsd	%xmm5, %xmm1
	addsd	%xmm8, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LC8(%rip), %xmm1
	ucomisd	%xmm9, %xmm1
	jbe	.L72
	sqrtsd	%xmm0, %xmm10
	movapd	%xmm9, %xmm0
	movsd	%xmm7, 80(%rsp)
	movaps	%xmm3, 64(%rsp)
	addsd	%xmm9, %xmm0
	movsd	%xmm4, 48(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm6, 16(%rsp)
	divsd	%xmm10, %xmm0
	movsd	%xmm10, (%rsp)
	movsd	%xmm8, 112(%rsp)
	movsd	%xmm2, 104(%rsp)
	call	__log1p@PLT
	movsd	80(%rsp), %xmm7
	testl	%ebx, %ebx
	movsd	16(%rsp), %xmm6
	mulsd	%xmm0, %xmm7
	movapd	64(%rsp), %xmm3
	movsd	48(%rsp), %xmm4
	movsd	(%rsp), %xmm10
	movsd	32(%rsp), %xmm5
	movaps	%xmm3, 48(%rsp)
	movsd	%xmm4, 32(%rsp)
	movsd	%xmm7, 16(%rsp)
	movsd	%xmm6, (%rsp)
	je	.L40
	movsd	104(%rsp), %xmm2
	movapd	%xmm10, %xmm0
	movapd	%xmm2, %xmm1
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm7
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm7, %xmm2
	movsd	32(%rsp), %xmm4
	movq	.LC4(%rip), %xmm7
	movapd	48(%rsp), %xmm3
	movsd	112(%rsp), %xmm8
.L41:
	movapd	%xmm4, %xmm0
	movapd	%xmm7, %xmm5
	movsd	.LC13(%rip), %xmm4
	andpd	%xmm7, %xmm0
	andnpd	%xmm2, %xmm5
	ucomisd	%xmm2, %xmm4
	orpd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
	jbe	.L78
.L48:
	mulsd	%xmm2, %xmm2
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L68:
	jp	.L20
	jne	.L20
	ucomisd	%xmm9, %xmm7
	ja	.L82
.L28:
	movapd	%xmm9, %xmm0
	movapd	%xmm9, %xmm1
	movaps	%xmm3, 80(%rsp)
	addsd	%xmm5, %xmm1
	subsd	%xmm5, %xmm0
	movsd	%xmm2, 64(%rsp)
	movsd	%xmm4, 48(%rsp)
	movsd	%xmm6, 32(%rsp)
	movsd	%xmm9, 16(%rsp)
	mulsd	%xmm1, %xmm0
	movapd	%xmm9, %xmm1
	movsd	%xmm5, (%rsp)
	addsd	%xmm9, %xmm1
	addsd	%xmm8, %xmm0
	mulsd	%xmm5, %xmm1
	call	__csqrt@PLT
	movsd	16(%rsp), %xmm9
	testl	%ebx, %ebx
	movsd	(%rsp), %xmm5
	addsd	%xmm0, %xmm9
	movsd	32(%rsp), %xmm6
	addsd	%xmm5, %xmm1
	movsd	48(%rsp), %xmm4
	movsd	64(%rsp), %xmm2
	movapd	80(%rsp), %xmm3
	jne	.L45
	movapd	%xmm9, %xmm0
	movq	.LC4(%rip), %xmm7
.L46:
	movaps	%xmm7, 48(%rsp)
	movaps	%xmm3, 32(%rsp)
	movsd	%xmm4, 16(%rsp)
	movsd	%xmm6, (%rsp)
	call	__clog@PLT
	movsd	16(%rsp), %xmm4
	movapd	32(%rsp), %xmm3
	movapd	%xmm4, %xmm6
	movapd	48(%rsp), %xmm7
	andpd	%xmm3, %xmm0
	andpd	%xmm7, %xmm6
	orpd	%xmm6, %xmm0
	movsd	(%rsp), %xmm6
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L80:
	movsd	.LC0(%rip), %xmm8
	movsd	%xmm1, 104(%rsp)
	movsd	%xmm1, 16(%rsp)
	movapd	%xmm8, %xmm0
	movapd	%xmm9, %xmm1
	movsd	%xmm4, 48(%rsp)
	movaps	%xmm3, 64(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm9, (%rsp)
	movsd	%xmm8, 80(%rsp)
	call	__ieee754_hypot@PLT
	movsd	(%rsp), %xmm9
	movapd	%xmm0, %xmm10
	movapd	%xmm9, %xmm0
	movsd	%xmm10, (%rsp)
	addsd	%xmm10, %xmm0
	call	__ieee754_log@PLT
	testl	%ebx, %ebx
	movsd	(%rsp), %xmm10
	movsd	16(%rsp), %xmm6
	movsd	32(%rsp), %xmm5
	movsd	48(%rsp), %xmm4
	movapd	64(%rsp), %xmm3
	je	.L11
	movsd	104(%rsp), %xmm2
	movsd	%xmm0, (%rsp)
	movapd	%xmm10, %xmm0
	movapd	%xmm2, %xmm1
	movsd	%xmm4, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	call	__ieee754_atan2@PLT
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm7
	movsd	16(%rsp), %xmm4
	movapd	%xmm7, %xmm0
	movapd	32(%rsp), %xmm3
	movq	.LC4(%rip), %xmm7
	movsd	80(%rsp), %xmm8
.L12:
	movapd	%xmm4, %xmm6
	andpd	%xmm3, %xmm0
	andpd	%xmm7, %xmm6
	orpd	%xmm6, %xmm0
.L78:
	movapd	%xmm8, %xmm6
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L45:
	movq	.LC4(%rip), %xmm7
	movapd	%xmm2, %xmm5
	andpd	%xmm3, %xmm1
	andpd	%xmm7, %xmm5
	orpd	%xmm5, %xmm1
	movapd	%xmm1, %xmm0
	movapd	%xmm9, %xmm1
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L82:
	ucomisd	%xmm9, %xmm0
	jbe	.L70
	movapd	%xmm9, %xmm0
	sqrtsd	%xmm9, %xmm10
	movsd	%xmm7, 80(%rsp)
	movaps	%xmm3, 64(%rsp)
	addsd	%xmm10, %xmm0
	movsd	%xmm8, 48(%rsp)
	movsd	%xmm4, 32(%rsp)
	movsd	%xmm6, 16(%rsp)
	addsd	%xmm0, %xmm0
	movsd	%xmm10, (%rsp)
	movsd	%xmm2, 104(%rsp)
	call	__log1p@PLT
	movsd	80(%rsp), %xmm7
	testl	%ebx, %ebx
	movapd	%xmm0, %xmm5
	movsd	(%rsp), %xmm10
	mulsd	%xmm7, %xmm5
	movapd	64(%rsp), %xmm3
	movsd	16(%rsp), %xmm6
	movsd	32(%rsp), %xmm4
	movsd	48(%rsp), %xmm8
	je	.L32
	movq	.LC4(%rip), %xmm7
	movapd	%xmm10, %xmm0
	movsd	104(%rsp), %xmm2
	movsd	%xmm5, 16(%rsp)
	andpd	%xmm7, %xmm2
	movaps	%xmm7, (%rsp)
	orpd	.LC10(%rip), %xmm2
	movapd	%xmm2, %xmm1
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm5
	movapd	%xmm0, %xmm1
	movapd	(%rsp), %xmm7
	movapd	%xmm5, %xmm0
	movsd	32(%rsp), %xmm4
	movsd	48(%rsp), %xmm8
	movapd	64(%rsp), %xmm3
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm10, %xmm1
	movapd	%xmm5, %xmm0
	movsd	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movsd	%xmm6, (%rsp)
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm7
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm7, %xmm0
	movsd	32(%rsp), %xmm4
	movapd	48(%rsp), %xmm3
.L13:
	movapd	%xmm4, %xmm2
	andpd	%xmm3, %xmm0
	movq	.LC4(%rip), %xmm7
	andpd	%xmm7, %xmm2
	orpd	%xmm2, %xmm0
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L70:
	movapd	%xmm4, %xmm5
	movsd	%xmm8, 104(%rsp)
	movsd	.LC11(%rip), %xmm11
	mulsd	%xmm4, %xmm5
	movapd	%xmm9, %xmm10
	movaps	%xmm3, 112(%rsp)
	movsd	%xmm6, 80(%rsp)
	addsd	%xmm5, %xmm11
	movapd	%xmm5, %xmm0
	movsd	%xmm4, 64(%rsp)
	movsd	%xmm7, 48(%rsp)
	movsd	%xmm9, 32(%rsp)
	sqrtsd	%xmm11, %xmm11
	mulsd	%xmm9, %xmm11
	movsd	%xmm2, 128(%rsp)
	addsd	%xmm11, %xmm0
	subsd	%xmm5, %xmm11
	movapd	%xmm0, %xmm1
	mulsd	%xmm7, %xmm11
	mulsd	%xmm7, %xmm1
	sqrtsd	%xmm11, %xmm11
	movsd	%xmm11, (%rsp)
	sqrtsd	%xmm1, %xmm1
	mulsd	%xmm1, %xmm10
	movsd	%xmm1, 16(%rsp)
	addsd	%xmm11, %xmm10
	addsd	%xmm10, %xmm10
	addsd	%xmm10, %xmm0
	call	__log1p@PLT
	movsd	48(%rsp), %xmm7
	testl	%ebx, %ebx
	movsd	(%rsp), %xmm11
	mulsd	%xmm7, %xmm0
	movapd	112(%rsp), %xmm3
	movsd	16(%rsp), %xmm1
	movsd	32(%rsp), %xmm9
	movsd	64(%rsp), %xmm4
	movsd	80(%rsp), %xmm6
	movsd	104(%rsp), %xmm8
	je	.L33
	addsd	%xmm8, %xmm11
	movsd	%xmm0, 48(%rsp)
	movsd	128(%rsp), %xmm2
	movapd	%xmm9, %xmm0
	movq	.LC4(%rip), %xmm7
	addsd	%xmm1, %xmm0
	movapd	%xmm2, %xmm6
	movapd	%xmm11, %xmm1
	movsd	%xmm8, 32(%rsp)
	andpd	%xmm7, %xmm6
	andpd	%xmm3, %xmm1
	movaps	%xmm7, 16(%rsp)
	orpd	%xmm6, %xmm1
	movaps	%xmm3, (%rsp)
	call	__ieee754_atan2@PLT
	movapd	%xmm0, %xmm1
	movsd	48(%rsp), %xmm10
	movapd	(%rsp), %xmm3
	movapd	%xmm10, %xmm0
	movapd	16(%rsp), %xmm7
	movsd	32(%rsp), %xmm8
	movsd	64(%rsp), %xmm4
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movaps	%xmm3, 48(%rsp)
	movapd	%xmm9, %xmm1
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm10, %xmm0
	movsd	%xmm4, 32(%rsp)
	movsd	%xmm6, (%rsp)
.L77:
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm5
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm5, %xmm0
	movsd	32(%rsp), %xmm4
	movapd	48(%rsp), %xmm3
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L69:
	movapd	%xmm5, %xmm0
	movsd	%xmm6, 104(%rsp)
	movapd	%xmm4, %xmm11
	movsd	%xmm4, 80(%rsp)
	addsd	%xmm5, %xmm0
	movsd	.LC9(%rip), %xmm1
	mulsd	%xmm4, %xmm11
	movapd	%xmm9, %xmm13
	movapd	%xmm9, %xmm12
	movsd	%xmm7, (%rsp)
	mulsd	%xmm5, %xmm13
	movsd	%xmm9, 64(%rsp)
	mulsd	%xmm5, %xmm0
	movsd	%xmm5, 32(%rsp)
	addsd	%xmm11, %xmm1
	movsd	%xmm8, 136(%rsp)
	movaps	%xmm3, 112(%rsp)
	movsd	%xmm2, 128(%rsp)
	addsd	%xmm1, %xmm0
	movapd	%xmm10, %xmm1
	mulsd	%xmm10, %xmm1
	mulsd	%xmm11, %xmm0
	addsd	%xmm0, %xmm1
	sqrtsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm10
	movapd	%xmm0, %xmm1
	movapd	%xmm5, %xmm0
	divsd	%xmm10, %xmm1
	addsd	%xmm11, %xmm10
	addsd	%xmm11, %xmm1
	mulsd	%xmm7, %xmm1
	sqrtsd	%xmm1, %xmm1
	divsd	%xmm1, %xmm13
	movsd	%xmm1, 48(%rsp)
	mulsd	%xmm1, %xmm12
	mulsd	%xmm13, %xmm0
	movsd	%xmm13, 16(%rsp)
	addsd	%xmm0, %xmm12
	addsd	%xmm12, %xmm12
	movapd	%xmm12, %xmm0
	addsd	%xmm10, %xmm0
	call	__log1p@PLT
	movsd	(%rsp), %xmm7
	testl	%ebx, %ebx
	movsd	16(%rsp), %xmm13
	mulsd	%xmm7, %xmm0
	movapd	112(%rsp), %xmm3
	movsd	32(%rsp), %xmm5
	movsd	48(%rsp), %xmm1
	movsd	%xmm0, (%rsp)
	movsd	64(%rsp), %xmm9
	movsd	80(%rsp), %xmm4
	movsd	104(%rsp), %xmm6
	je	.L26
	addsd	%xmm13, %xmm5
	movsd	128(%rsp), %xmm2
	movq	.LC4(%rip), %xmm7
	addsd	%xmm1, %xmm9
	movapd	%xmm2, %xmm6
	movsd	%xmm4, 48(%rsp)
	movaps	%xmm7, 32(%rsp)
	andpd	%xmm7, %xmm6
	andpd	%xmm3, %xmm5
	movapd	%xmm9, %xmm0
	movaps	%xmm3, 16(%rsp)
	orpd	%xmm6, %xmm5
	movapd	%xmm5, %xmm1
	call	__ieee754_atan2@PLT
	movapd	16(%rsp), %xmm3
	movapd	%xmm0, %xmm1
	movapd	32(%rsp), %xmm7
	movsd	(%rsp), %xmm0
	movsd	48(%rsp), %xmm4
	movsd	136(%rsp), %xmm8
	jmp	.L12
.L72:
	movapd	%xmm5, %xmm11
	movsd	%xmm6, 104(%rsp)
	movapd	%xmm4, %xmm13
	movsd	%xmm4, 80(%rsp)
	addsd	%xmm5, %xmm11
	movsd	.LC9(%rip), %xmm1
	mulsd	%xmm4, %xmm13
	movapd	%xmm9, %xmm12
	movapd	%xmm9, %xmm10
	movsd	%xmm7, 64(%rsp)
	mulsd	%xmm5, %xmm12
	movapd	%xmm5, %xmm14
	mulsd	%xmm5, %xmm11
	movsd	%xmm9, 48(%rsp)
	addsd	%xmm13, %xmm1
	movsd	%xmm5, 16(%rsp)
	movaps	%xmm3, 112(%rsp)
	movsd	%xmm8, 136(%rsp)
	addsd	%xmm1, %xmm11
	movapd	%xmm0, %xmm1
	movsd	%xmm2, 128(%rsp)
	mulsd	%xmm0, %xmm1
	mulsd	%xmm13, %xmm11
	addsd	%xmm11, %xmm1
	sqrtsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	movapd	%xmm13, %xmm1
	addsd	%xmm0, %xmm1
	divsd	%xmm0, %xmm11
	mulsd	%xmm7, %xmm1
	movapd	%xmm11, %xmm0
	sqrtsd	%xmm1, %xmm1
	divsd	%xmm1, %xmm12
	movsd	%xmm1, 32(%rsp)
	addsd	%xmm13, %xmm0
	mulsd	%xmm1, %xmm10
	mulsd	%xmm12, %xmm14
	movsd	%xmm12, (%rsp)
	addsd	%xmm14, %xmm10
	addsd	%xmm10, %xmm10
	addsd	%xmm10, %xmm0
	call	__log1p@PLT
	movsd	64(%rsp), %xmm7
	testl	%ebx, %ebx
	movsd	(%rsp), %xmm12
	mulsd	%xmm7, %xmm0
	movapd	112(%rsp), %xmm3
	movsd	16(%rsp), %xmm5
	movsd	32(%rsp), %xmm1
	movsd	48(%rsp), %xmm9
	movsd	80(%rsp), %xmm4
	movsd	104(%rsp), %xmm6
	je	.L43
	movsd	128(%rsp), %xmm2
	addsd	%xmm12, %xmm5
	movq	.LC4(%rip), %xmm7
	movsd	%xmm6, 32(%rsp)
	movapd	%xmm2, %xmm6
	movsd	%xmm0, 48(%rsp)
	movapd	%xmm9, %xmm0
	andpd	%xmm7, %xmm6
	movsd	%xmm4, 64(%rsp)
	andpd	%xmm3, %xmm5
	addsd	%xmm1, %xmm0
	movaps	%xmm7, 16(%rsp)
	orpd	%xmm6, %xmm5
	movaps	%xmm3, (%rsp)
	movapd	%xmm5, %xmm1
	call	__ieee754_atan2@PLT
	movsd	48(%rsp), %xmm10
	movapd	%xmm0, %xmm1
	movapd	(%rsp), %xmm3
	movapd	%xmm10, %xmm2
	movapd	16(%rsp), %xmm7
	movsd	32(%rsp), %xmm6
	movsd	64(%rsp), %xmm4
	movsd	136(%rsp), %xmm8
	jmp	.L41
.L71:
	movapd	%xmm9, %xmm1
	movsd	%xmm7, 80(%rsp)
	movapd	%xmm8, %xmm0
	movsd	%xmm4, 48(%rsp)
	movaps	%xmm3, 64(%rsp)
	movsd	%xmm5, 32(%rsp)
	movsd	%xmm6, 16(%rsp)
	movsd	%xmm2, 112(%rsp)
	movsd	%xmm9, (%rsp)
	movsd	%xmm8, 104(%rsp)
	call	__ieee754_hypot@PLT
	movsd	(%rsp), %xmm9
	movapd	%xmm0, %xmm11
	movapd	%xmm9, %xmm10
	movapd	%xmm9, %xmm0
	movsd	%xmm11, (%rsp)
	addsd	%xmm11, %xmm0
	addsd	%xmm9, %xmm10
	mulsd	%xmm10, %xmm0
	call	__log1p@PLT
	movsd	80(%rsp), %xmm7
	testl	%ebx, %ebx
	movsd	16(%rsp), %xmm6
	mulsd	%xmm0, %xmm7
	movapd	64(%rsp), %xmm3
	movsd	48(%rsp), %xmm4
	movsd	(%rsp), %xmm11
	movsd	32(%rsp), %xmm5
	movaps	%xmm3, 48(%rsp)
	movsd	%xmm4, 32(%rsp)
	movsd	%xmm7, 16(%rsp)
	movsd	%xmm6, (%rsp)
	je	.L44
	movsd	112(%rsp), %xmm2
	movapd	%xmm11, %xmm0
	movapd	%xmm2, %xmm1
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm7
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm7, %xmm2
	movsd	32(%rsp), %xmm4
	movq	.LC4(%rip), %xmm7
	movapd	48(%rsp), %xmm3
	movsd	104(%rsp), %xmm8
	jmp	.L41
.L32:
	movaps	%xmm3, 48(%rsp)
	movapd	%xmm10, %xmm1
	movsd	%xmm4, 32(%rsp)
	movapd	%xmm8, %xmm0
	movsd	%xmm5, 16(%rsp)
	movsd	%xmm6, (%rsp)
	jmp	.L77
.L33:
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm11, %xmm0
	addsd	%xmm9, %xmm1
	movsd	%xmm4, 32(%rsp)
	addsd	%xmm8, %xmm0
	movsd	%xmm6, (%rsp)
	movaps	%xmm3, 48(%rsp)
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm10
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm10, %xmm0
	movsd	32(%rsp), %xmm4
	movapd	48(%rsp), %xmm3
	jmp	.L13
.L25:
	movaps	%xmm3, 48(%rsp)
	movapd	%xmm9, %xmm1
	movsd	%xmm4, 32(%rsp)
	movapd	%xmm11, %xmm0
	movsd	%xmm6, 16(%rsp)
.L76:
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm6
	movapd	%xmm0, %xmm1
	movsd	32(%rsp), %xmm4
	movsd	(%rsp), %xmm0
	movapd	48(%rsp), %xmm3
	jmp	.L13
.L26:
	addsd	%xmm13, %xmm5
	movsd	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	addsd	%xmm9, %xmm1
	movsd	%xmm6, 16(%rsp)
	movapd	%xmm5, %xmm0
	jmp	.L76
.L43:
	addsd	%xmm12, %xmm5
	movsd	%xmm0, 16(%rsp)
	addsd	%xmm9, %xmm1
	movsd	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movapd	%xmm5, %xmm0
	movsd	%xmm6, (%rsp)
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm10
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm10, %xmm2
	movsd	32(%rsp), %xmm4
	movapd	48(%rsp), %xmm3
.L42:
	movapd	%xmm4, %xmm0
	movq	.LC4(%rip), %xmm7
	movsd	.LC13(%rip), %xmm4
	movapd	%xmm7, %xmm5
	andpd	%xmm7, %xmm0
	ucomisd	%xmm2, %xmm4
	andnpd	%xmm2, %xmm5
	orpd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
	jbe	.L47
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L40:
	movapd	%xmm10, %xmm1
.L75:
	movapd	%xmm5, %xmm0
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm7
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm6
	movapd	%xmm7, %xmm2
	movsd	32(%rsp), %xmm4
	movapd	48(%rsp), %xmm3
	jmp	.L42
.L44:
	movapd	%xmm11, %xmm1
	jmp	.L75
	.size	__kernel_casinh, .-__kernel_casinh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC2:
	.long	0
	.long	1127219200
	.align 8
.LC3:
	.long	4277811695
	.long	1072049730
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1071644672
	.align 8
.LC6:
	.long	0
	.long	1015021568
	.align 8
.LC7:
	.long	0
	.long	1073217536
	.align 8
.LC8:
	.long	0
	.long	963641344
	.align 8
.LC9:
	.long	0
	.long	1073741824
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC11:
	.long	0
	.long	1074790400
	.align 8
.LC12:
	.long	0
	.long	1018167296
	.align 8
.LC13:
	.long	0
	.long	1048576
