	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cprojf
	.type	__cprojf, @function
__cprojf:
	movq	%xmm0, -8(%rsp)
	movss	.LC0(%rip), %xmm2
	movss	-8(%rsp), %xmm1
	movaps	%xmm1, %xmm3
	movss	-4(%rsp), %xmm0
	andps	%xmm2, %xmm3
	ucomiss	.LC1(%rip), %xmm3
	ja	.L2
	andps	%xmm0, %xmm2
	ucomiss	.LC1(%rip), %xmm2
	jbe	.L1
.L2:
	andps	.LC2(%rip), %xmm0
	movss	.LC3(%rip), %xmm1
.L1:
	movss	%xmm1, -16(%rsp)
	movss	%xmm0, -12(%rsp)
	movq	-16(%rsp), %xmm0
	ret
	.size	__cprojf, .-__cprojf
	.weak	cprojf32
	.set	cprojf32,__cprojf
	.weak	cprojf
	.set	cprojf,__cprojf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095040
