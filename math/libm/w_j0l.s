	.text
	.p2align 4,,15
	.globl	__j0l
	.type	__j0l, @function
__j0l:
	jmp	__ieee754_j0l@PLT
	.size	__j0l, .-__j0l
	.weak	j0f64x
	.set	j0f64x,__j0l
	.weak	j0l
	.set	j0l,__j0l
	.p2align 4,,15
	.globl	__y0l
	.type	__y0l, @function
__y0l:
	fldt	8(%rsp)
	fldz
	fucomi	%st(1), %st
	jnb	.L11
	fstp	%st(0)
.L4:
	fstpt	8(%rsp)
	jmp	__ieee754_y0l@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y0l, .-__y0l
	.weak	y0f64x
	.set	y0f64x,__y0l
	.weak	y0l
	.set	y0l,__y0l
