	.text
	.globl	__gttf2
	.globl	__eqtf2
	.globl	__addtf3
	.globl	__subtf3
	.globl	__lttf2
	.globl	__fixtfdi
	.p2align 4,,15
	.globl	__llrintf128
	.type	__llrintf128, @function
__llrintf128:
	pushq	%rbx
	subq	$16, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbx
	movq	%rbx, %rax
	shrq	$48, %rax
	andl	$32767, %eax
	subl	$16383, %eax
	cmpl	$62, %eax
	jg	.L2
	movdqa	.LC0(%rip), %xmm1
	shrq	$63, %rbx
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L25
	leaq	two112(%rip), %rdx
	movq	%rbx, %rax
	salq	$4, %rax
	movdqa	(%rsp), %xmm1
	movdqa	(%rdx,%rax), %xmm2
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
.L6:
	movq	8(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rdx, %rcx
	shrq	$48, %rcx
	andl	$32767, %ecx
	movl	%ecx, %esi
	subl	$16383, %esi
	js	.L7
	movabsq	$281474976710655, %rax
	andq	%rdx, %rax
	movabsq	$281474976710656, %rdx
	orq	%rdx, %rax
	cmpl	$48, %esi
	jg	.L8
	movl	$48, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rax
.L7:
	movq	%rax, %rdx
	negq	%rdx
	testq	%rbx, %rbx
	cmovne	%rdx, %rax
.L27:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	.LC1(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L9
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L26
.L9:
	movdqa	(%rsp), %xmm0
	call	__fixtfdi@PLT
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	(%rsp), %xmm0
	call	__nearbyintf128@PLT
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	cmpq	$1, %rax
	sbbl	%edi, %edi
	andl	$31, %edi
	addl	$1, %edi
	call	feraiseexcept@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	subl	$16431, %ecx
	movq	(%rsp), %rdx
	salq	%cl, %rax
	movl	$112, %ecx
	subl	%esi, %ecx
	shrq	%cl, %rdx
	orq	%rdx, %rax
	movq	%rax, %rdx
	negq	%rdx
	testq	%rbx, %rbx
	cmovne	%rdx, %rax
	jmp	.L27
.L26:
	movdqa	(%rsp), %xmm0
	call	__nearbyintf128@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__eqtf2@PLT
	cmpq	$1, %rax
	sbbl	%edi, %edi
	andl	$31, %edi
	addl	$1, %edi
	call	feraiseexcept@PLT
	movabsq	$-9223372036854775808, %rax
	jmp	.L1
	.size	__llrintf128, .-__llrintf128
	.weak	llrintf128
	.set	llrintf128,__llrintf128
	.section	.rodata
	.align 32
	.type	two112, @object
	.size	two112, 32
two112:
	.long	0
	.long	0
	.long	0
	.long	1081016320
	.long	0
	.long	0
	.long	0
	.long	-1066467328
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	4294705152
	.long	4294967295
	.long	1077805055
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-1069678592
	.align 16
.LC2:
	.long	0
	.long	131072
	.long	0
	.long	-1069678592
