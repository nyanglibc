	.text
	.p2align 4,,15
	.globl	fesetexcept
	.type	fesetexcept, @function
fesetexcept:
#APP
# 26 "../sysdeps/x86_64/fpu/fesetexcept.c" 1
	stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	andl	$61, %edi
	orl	%edi, -4(%rsp)
#APP
# 28 "../sysdeps/x86_64/fpu/fesetexcept.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	fesetexcept, .-fesetexcept
