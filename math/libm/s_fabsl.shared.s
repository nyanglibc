.text
.globl __fabsl
.type __fabsl,@function
.align 1<<4
__fabsl:
       fldt 8(%rsp)
       fabs
       ret
.size __fabsl,.-__fabsl
.weak fabsl
fabsl = __fabsl
.weak fabsf64x
fabsf64x = __fabsl
