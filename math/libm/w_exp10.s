	.text
	.p2align 4,,15
	.globl	__exp10
	.type	__exp10, @function
__exp10:
	subq	$24, %rsp
	movsd	%xmm0, 8(%rsp)
	call	__ieee754_exp10@PLT
	movq	.LC0(%rip), %xmm3
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm2
	movsd	8(%rsp), %xmm1
	andpd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm2
	jb	.L2
	ucomisd	.LC2(%rip), %xmm0
	jp	.L1
	je	.L2
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	andpd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	addq	$24, %rsp
	ret
	.size	__exp10, .-__exp10
	.weak	exp10f32x
	.set	exp10f32x,__exp10
	.weak	exp10f64
	.set	exp10f64,__exp10
	.weak	exp10
	.set	exp10,__exp10
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	0
