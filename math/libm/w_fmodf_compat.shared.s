	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fmodf
	.type	__fmodf, @function
__fmodf:
	movaps	%xmm0, %xmm2
	andps	.LC0(%rip), %xmm2
	ucomiss	.LC1(%rip), %xmm2
	seta	%dl
	ja	.L4
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L4
.L2:
	jmp	__ieee754_fmodf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	ucomiss	%xmm1, %xmm0
	jp	.L2
	movl	$127, %edi
	jmp	__kernel_standard_f@PLT
	.size	__fmodf, .-__fmodf
	.weak	fmodf32
	.set	fmodf32,__fmodf
	.weak	fmodf
	.set	fmodf,__fmodf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
