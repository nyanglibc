	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__sqrtf
	.type	__sqrtf, @function
__sqrtf:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.L4
.L2:
	jmp	__ieee754_sqrtf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	movaps	%xmm0, %xmm1
	movl	$126, %edi
	jmp	__kernel_standard_f@PLT
	.size	__sqrtf, .-__sqrtf
	.weak	sqrtf32
	.set	sqrtf32,__sqrtf
	.weak	sqrtf
	.set	sqrtf,__sqrtf
