	.text
	.p2align 4,,15
	.globl	__GI___isnanf
	.hidden	__GI___isnanf
	.type	__GI___isnanf, @function
__GI___isnanf:
	movd	%xmm0, %edx
	movl	$2139095040, %eax
	andl	$2147483647, %edx
	subl	%edx, %eax
	shrl	$31, %eax
	ret
	.size	__GI___isnanf, .-__GI___isnanf
	.globl	__isnanf
	.set	__isnanf,__GI___isnanf
	.weak	isnanf
	.set	isnanf,__isnanf
