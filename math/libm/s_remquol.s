	.text
	.p2align 4,,15
	.globl	__remquol
	.type	__remquol, @function
__remquol:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	104(%rsp), %rbx
	movq	96(%rsp), %rbp
	movl	%ebx, %r13d
	movq	%rbp, %r14
	andw	$32767, %r13w
	shrq	$32, %r14
	movswl	%r13w, %eax
	movl	%eax, %edx
	orl	%r14d, %edx
	orl	%ebp, %edx
	je	.L44
	movq	88(%rsp), %rdx
	movl	%edx, %r12d
	andw	$32767, %r12w
	cmpw	$32767, %r12w
	je	.L4
	cmpw	$32767, %r13w
	movq	%rdi, %r15
	je	.L45
	cmpw	$32763, %r13w
	fldt	80(%rsp)
	jle	.L46
.L6:
	movswl	%r12w, %r12d
	xorl	%edx, %ebx
	subl	%eax, %r12d
	movq	80(%rsp), %rax
	movswl	%bx, %ebx
	shrl	$31, %ebx
	movq	%rax, %rcx
	subl	%ebp, %eax
	shrq	$32, %rcx
	subl	%r14d, %ecx
	orl	%ecx, %r12d
	orl	%eax, %r12d
	je	.L47
	fabs
	cmpw	$32764, %r13w
	fldt	96(%rsp)
	fabs
	jg	.L9
	fld	%st(0)
	xorl	%eax, %eax
	fmuls	.LC2(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	jb	.L50
	fsubp	%st, %st(2)
	fxch	%st(1)
	movl	$4, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L50:
	fstp	%st(2)
	fxch	%st(1)
	jmp	.L23
.L53:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L23:
	fld	%st(1)
	fadd	%st(2), %st
	fxch	%st(1)
	fucomi	%st(1), %st
	jnb	.L48
	fstp	%st(1)
.L13:
	cmpw	$1, %r13w
	jg	.L51
	fld	%st(0)
	fadd	%st(1), %st
	fucomip	%st(2), %st
	ja	.L49
	fstp	%st(1)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L54:
	fstp	%st(1)
	fstp	%st(1)
.L15:
	movl	%eax, %ecx
	fldz
	fxch	%st(1)
	negl	%ecx
	testl	%ebx, %ebx
	cmovne	%ecx, %eax
	fucomi	%st(1), %st
	movl	%eax, (%r15)
	jp	.L52
	fcmove	%st(1), %st
	fstp	%st(1)
	jmp	.L21
.L52:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L21:
	testw	%dx, %dx
	jns	.L1
	fchs
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L45:
	movl	%r14d, %ecx
	andl	$2147483647, %ecx
	orl	%ebp, %ecx
	fldt	80(%rsp)
	je	.L6
	fstp	%st(0)
.L4:
	fldt	80(%rsp)
	fldt	96(%rsp)
	fmulp	%st, %st(1)
	fdiv	%st(0), %st
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	fldt	80(%rsp)
	fldt	96(%rsp)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	fmulp	%st, %st(1)
	fdiv	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$1, %ebx
	fmuls	.LC0(%rip)
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	movl	%eax, (%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	fstp	%st(0)
	fldt	96(%rsp)
	movq	%rdx, 8(%rsp)
	movl	%eax, 4(%rsp)
	subq	$16, %rsp
	fmuls	.LC1(%rip)
	fstpt	(%rsp)
	pushq	104(%rsp)
	pushq	104(%rsp)
	call	__ieee754_fmodl@PLT
	addq	$32, %rsp
	movq	8(%rsp), %rdx
	movl	4(%rsp), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	cmpw	$32765, %r13w
	jle	.L53
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L51:
	fxch	%st(1)
.L12:
	fld	%st(0)
	fmuls	.LC3(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	jbe	.L54
	fsub	%st(1), %st
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L19
	fstp	%st(0)
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L55:
	fstp	%st(1)
.L42:
	addl	$1, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L48:
	fsubp	%st, %st(1)
	addl	$2, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L49:
	fsub	%st(1), %st
	fld	%st(0)
	fadd	%st(1), %st
	fucomip	%st(2), %st
	jb	.L55
	fxch	%st(1)
.L19:
	fsubrp	%st, %st(1)
	addl	$2, %eax
	jmp	.L15
	.size	__remquol, .-__remquol
	.weak	remquof64x
	.set	remquof64x,__remquol
	.weak	remquol
	.set	remquol,__remquol
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	1090519040
	.align 4
.LC2:
	.long	1082130432
	.align 4
.LC3:
	.long	1056964608
