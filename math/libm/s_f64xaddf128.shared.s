	.text
	.globl	__eqtf2
	.globl	__addtf3
	.globl	__trunctfxf2
	.globl	__unordtf2
	.globl	__gttf2
	.p2align 4,,15
	.globl	__f64xaddf128
	.type	__f64xaddf128, @function
__f64xaddf128:
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm4
	subq	$88, %rsp
	movdqa	.LC0(%rip), %xmm3
	pxor	%xmm1, %xmm3
	movaps	%xmm1, 16(%rsp)
	movdqa	%xmm4, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm4, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L16
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	call	__trunctfxf2@PLT
	fld	%st(0)
	fabs
	fldt	.LC1(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L1
.L4:
	fld	%st(0)
	fstpt	32(%rsp)
	fucomip	%st(0), %st
	jp	.L21
	movdqa	(%rsp), %xmm2
	pand	.LC2(%rip), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	32(%rsp)
	jne	.L1
	fstp	%st(0)
	movdqa	(%rsp), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	fldt	32(%rsp)
	jg	.L1
	movdqa	16(%rsp), %xmm2
	fstpt	(%rsp)
	pand	.LC2(%rip), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jne	.L1
	fstp	%st(0)
	movdqa	16(%rsp), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	fldt	(%rsp)
	jg	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebp
	movl	%ebp, %eax
	andl	$-32704, %eax
	orl	$32640, %eax
	movl	%eax, 76(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movdqa	16(%rsp), %xmm0
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %eax
	orl	%ebp, %eax
	movl	%eax, 76(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %ebp
	notl	%ebp
	testl	%edi, %ebp
	jne	.L22
.L6:
	shrl	$5, %ebx
	movq	%xmm0, %rax
	movabsq	$-4294967296, %rdx
	andl	$1, %ebx
	orl	32(%rsp), %ebx
	andq	%rdx, %rax
	orq	%rbx, %rax
	movq	%rax, 32(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__trunctfxf2@PLT
	fld	%st(0)
	fabs
	fldt	.LC1(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L4
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L1
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	fldt	32(%rsp)
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movaps	%xmm0, 48(%rsp)
	call	__GI___feraiseexcept
	movdqa	48(%rsp), %xmm0
	jmp	.L6
	.size	__f64xaddf128, .-__f64xaddf128
	.weak	f64xaddf128
	.set	f64xaddf128,__f64xaddf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
