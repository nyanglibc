.text
.globl __fmaxf
.type __fmaxf,@function
.align 1<<4
__fmaxf:
 ucomiss %xmm0, %xmm1
 jp 1f
 maxss %xmm1, %xmm0
 jmp 2f
1: ucomiss %xmm1, %xmm1
 jp 3f
 movss %xmm0, -4(%rsp)
 testb $0x40, -2(%rsp)
 jz 4f
 movss %xmm1, %xmm0
 ret
3:
 ucomiss %xmm0, %xmm0
 jp 4f
 movss %xmm1, -4(%rsp)
 testb $0x40, -2(%rsp)
 jz 4f
 ret
4:
 addss %xmm1, %xmm0
2: ret
.size __fmaxf,.-__fmaxf
.weak fmaxf
fmaxf = __fmaxf
.weak fmaxf32
fmaxf32 = __fmaxf
