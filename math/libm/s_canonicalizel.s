	.text
	.p2align 4,,15
	.globl	__canonicalizel
	.type	__canonicalizel, @function
__canonicalizel:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	fldt	(%rsi)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__iscanonicall@PLT
	testl	%eax, %eax
	movl	$1, %edx
	popq	%rsi
	popq	%rdi
	je	.L1
	fldt	(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__issignalingl@PLT
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	movl	%eax, %edx
	fldt	(%rsp)
	jne	.L8
	fstpt	(%rbx)
.L1:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	fadd	%st(0), %st
	xorl	%edx, %edx
	movl	%edx, %eax
	fstpt	(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__canonicalizel, .-__canonicalizel
	.weak	canonicalizef64x
	.set	canonicalizef64x,__canonicalizel
	.weak	canonicalizel
	.set	canonicalizel,__canonicalizel
