	.text
	.globl	__addtf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__nearbyintf128
	.type	__nearbyintf128, @function
__nearbyintf128:
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	sarq	$48, %rax
	andl	$32767, %eax
	subq	$16383, %rax
	cmpq	$111, %rax
	jg	.L2
	movq	%rdx, %rbx
	leaq	TWO112.7183(%rip), %rcx
	shrq	$63, %rbx
	movq	%rbx, %rdx
	salq	$4, %rdx
	testq	%rax, %rax
	movdqa	(%rcx,%rdx), %xmm2
	movaps	%xmm2, 16(%rsp)
	js	.L8
	leaq	32(%rsp), %rbx
	movq	%rbx, %rdi
	call	__GI_feholdexcept
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movq	%rbx, %rdi
	movaps	%xmm0, (%rsp)
	call	__GI_fesetenv
	movdqa	(%rsp), %xmm0
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$16384, %rax
	movdqa	(%rsp), %xmm0
	jne	.L1
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	32(%rsp), %rbp
	movq	%rbp, %rdi
	call	__GI_feholdexcept
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movq	%rbp, %rdi
	salq	$63, %rbx
	call	__GI_fesetenv
	movq	8(%rsp), %rax
	movabsq	$9223372036854775807, %rdx
	movdqa	(%rsp), %xmm4
	andq	%rdx, %rax
	movaps	%xmm4, 16(%rsp)
	orq	%rbx, %rax
	movq	%rax, 24(%rsp)
	movdqa	16(%rsp), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__nearbyintf128, .-__nearbyintf128
	.weak	nearbyintf128
	.set	nearbyintf128,__nearbyintf128
	.section	.rodata
	.align 32
	.type	TWO112.7183, @object
	.size	TWO112.7183, 32
TWO112.7183:
	.long	0
	.long	0
	.long	0
	.long	1081016320
	.long	0
	.long	0
	.long	0
	.long	-1066467328
