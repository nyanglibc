	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__lttf2
	.globl	__multf3
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__ieee754_exp10f128
	.type	__ieee754_exp10f128, @function
__ieee754_exp10f128:
	movdqa	%xmm0, %xmm2
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L10
	movdqa	.LC2(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L10
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L18
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L19
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L20
	movdqa	.LC0(%rip), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	.LC4(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	jmp	__ieee754_expf128@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	(%rsp), %xmm0
	movabsq	$-144115188075855872, %rax
	movq	%xmm0, %rdx
	movaps	%xmm0, (%rsp)
	andq	%rax, %rdx
	movq	%rdx, (%rsp)
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC9(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	call	__ieee754_expf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	addq	$40, %rsp
	ret
	.size	__ieee754_exp10f128, .-__ieee754_exp10f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	-1073006880
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1074476112
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1066074112
	.align 16
.LC7:
	.long	2891384230
	.long	1479398573
	.long	465261905
	.long	1073751739
	.align 16
.LC8:
	.long	4275019174
	.long	815959592
	.long	1835186221
	.long	1069903525
	.align 16
.LC9:
	.long	0
	.long	1476395008
	.long	465261905
	.long	1073751739
