	.text
	.globl	__lttf2
	.globl	__addtf3
	.globl	__divtf3
	.globl	__letf2
	.globl	__subtf3
	.globl	__fixtfsi
	.globl	__multf3
	.globl	__floatsitf
	.p2align 4,,15
	.type	gammal_positive, @function
gammal_positive:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$152, %rsp
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L27
	movdqa	.LC5(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L28
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L29
	movdqa	.LC7(%rip), %xmm1
	movq	$0, 128(%rsp)
	movq	$0, 136(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L30
	pxor	%xmm7, %xmm7
	movdqa	.LC1(%rip), %xmm3
	movaps	%xmm3, 80(%rsp)
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm7, (%rsp)
.L10:
	movdqa	16(%rsp), %xmm0
	call	__roundf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	leaq	124(%rsp), %rdi
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__frexpf128@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	64(%rsp), %xmm2
	js	.L12
	movl	124(%rsp), %ebp
.L13:
	movdqa	32(%rsp), %xmm0
	movaps	%xmm2, 64(%rsp)
	call	__fixtfsi@PLT
	imull	%ebp, %eax
	movdqa	64(%rsp), %xmm2
	movl	%eax, (%rbx)
	leaq	176+gamma_coeff(%rip), %rbx
	movdqa	%xmm2, %xmm0
	movdqa	16(%rsp), %xmm1
	leaq	-192(%rbx), %rbp
	call	__ieee754_powf128@PLT
	movl	124(%rsp), %edi
	movaps	%xmm0, 32(%rsp)
	call	__floatsitf@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	call	__ieee754_exp2f128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm5
	pxor	.LC9(%rip), %xmm5
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm5, %xmm0
	call	__ieee754_expf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC10(%rip), %xmm0
	call	__divtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__divtf3@PLT
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm6
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm6, (%rsp)
	movdqa	.LC3(%rip), %xmm0
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	movdqa	(%rbx), %xmm4
	subq	$16, %rbx
	movaps	%xmm4, (%rsp)
.L15:
	movdqa	32(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbx, %rbp
	jne	.L31
	movdqa	16(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	call	__expm1f128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	128(%rsp), %rdi
	movdqa	16(%rsp), %xmm0
	movl	$0, (%rbx)
	call	__ieee754_lgammaf128_r@PLT
	call	__ieee754_expf128@PLT
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	.LC1(%rip), %xmm1
	movl	$0, (%rbx)
	leaq	128(%rsp), %rbx
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movq	%rbx, %rdi
	call	__ieee754_lgammaf128_r@PLT
	call	__ieee754_expf128@PLT
	movdqa	16(%rsp), %xmm1
	call	__divtf3@PLT
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	124(%rsp), %eax
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	leal	-1(%rax), %ebp
	movl	%ebp, 124(%rsp)
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L29:
	movdqa	.LC5(%rip), %xmm1
	movl	$0, (%rbx)
	leaq	128(%rsp), %rbx
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	call	__ceilf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	call	__fixtfsi@PLT
	pxor	%xmm1, %xmm1
	movq	%rbx, %rsi
	movdqa	(%rsp), %xmm0
	movl	%eax, %edi
	call	__gamma_productf128@PLT
	leaq	124(%rsp), %rdi
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_lgammaf128_r@PLT
	call	__ieee754_expf128@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	128(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	16(%rsp), %xmm1
	leaq	128(%rsp), %rbp
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	call	__ceilf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__fixtfsi@PLT
	movdqa	48(%rsp), %xmm3
	movq	%rbp, %rsi
	movl	%eax, %edi
	movdqa	(%rsp), %xmm1
	movdqa	%xmm3, %xmm0
	call	__gamma_productf128@PLT
	movdqa	128(%rsp), %xmm6
	movdqa	64(%rsp), %xmm2
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm2, 16(%rsp)
	jmp	.L10
	.size	gammal_positive, .-gammal_positive
	.globl	__eqtf2
	.globl	__getf2
	.globl	__gttf2
	.globl	__unordtf2
	.globl	__netf2
	.p2align 4,,15
	.globl	__ieee754_gammaf128_r
	.type	__ieee754_gammaf128_r, @function
__ieee754_gammaf128_r:
	pushq	%r12
	pushq	%rbp
	movabsq	$9223372036854775807, %rax
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$64, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbx
	movq	(%rsp), %rdx
	andq	%rbx, %rax
	orq	%rdx, %rax
	je	.L92
	movabsq	$-9223372036854775808, %rax
	movabsq	$9223090561878065151, %rcx
	addq	%rbx, %rax
	cmpq	%rcx, %rax
	jbe	.L93
	movabsq	$-281474976710656, %rax
	cmpq	%rax, %rbx
	jne	.L36
	testq	%rdx, %rdx
	jne	.L36
	movdqa	(%rsp), %xmm1
	movl	$0, (%rdi)
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
.L32:
	addq	$64, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movdqa	%xmm0, %xmm1
	movl	$0, (%rdi)
	movdqa	.LC1(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	addq	$64, %rsp
	popq	%rbx
	movdqa	%xmm2, %xmm0
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movabsq	$9223090561878065152, %rax
	andq	%rax, %rbx
	cmpq	%rax, %rbx
	je	.L94
	movdqa	.LC11(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L95
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 56(%rsp)
# 0 "" 2
#NO_APP
	movl	56(%rsp), %ebx
	xorl	%r12d, %r12d
	movl	%ebx, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebx
	movl	%eax, 60(%rsp)
	jne	.L96
.L41:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L97
	movdqa	.LC13(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L88
	movdqa	(%rsp), %xmm1
	movl	$0, 0(%rbp)
	movdqa	.LC1(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
.L44:
	testb	%r12b, %r12b
	jne	.L98
.L58:
	movdqa	%xmm2, %xmm3
	movdqa	.LC12(%rip), %xmm1
	pand	.LC18(%rip), %xmm3
	movaps	%xmm2, 16(%rsp)
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jne	.L59
	movdqa	32(%rsp), %xmm3
	movdqa	.LC12(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jle	.L59
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	je	.L59
	movdqa	%xmm2, %xmm1
	movdqa	.LC12(%rip), %xmm0
	call	__copysignf128@PLT
	movl	0(%rbp), %edx
	testl	%edx, %edx
	js	.L99
	movdqa	.LC12(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L93:
	movdqa	(%rsp), %xmm0
	call	__rintf128@PLT
	movdqa	(%rsp), %xmm1
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L36
	movdqa	(%rsp), %xmm1
	movl	$0, 0(%rbp)
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L94:
	movdqa	(%rsp), %xmm1
	movl	$0, 0(%rbp)
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L95:
	movdqa	.LC12(%rip), %xmm1
	movl	$0, 0(%rbp)
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	60(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	movl	$0, 0(%rbp)
	call	gammal_positive
	movl	60(%rsp), %edi
	call	__scalbnf128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L59:
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jne	.L32
	movdqa	%xmm2, %xmm1
	movdqa	.LC15(%rip), %xmm0
	call	__copysignf128@PLT
	movl	0(%rbp), %eax
	testl	%eax, %eax
	js	.L100
	movdqa	.LC15(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L88:
	movdqa	(%rsp), %xmm0
	call	__truncf128@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	call	__truncf128@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__eqtf2@PLT
	cmpq	$1, %rax
	movdqa	.LC14(%rip), %xmm1
	sbbl	%eax, %eax
	orl	$1, %eax
	movdqa	(%rsp), %xmm0
	movl	%eax, 0(%rbp)
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L89
	movdqa	.LC15(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L89:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jle	.L51
	movdqa	%xmm2, %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
.L51:
	movdqa	%xmm2, %xmm0
	movdqa	.LC16(%rip), %xmm1
	movaps	%xmm2, 16(%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jg	.L90
	movdqa	%xmm2, %xmm0
	movdqa	.LC17(%rip), %xmm1
	call	__multf3@PLT
	call	__sinf128@PLT
	movdqa	%xmm0, %xmm1
.L55:
	movdqa	(%rsp), %xmm2
	pxor	.LC9(%rip), %xmm2
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 32(%rsp)
	call	__multf3@PLT
	leaq	60(%rsp), %rdi
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	gammal_positive
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC17(%rip), %xmm0
	call	__divtf3@PLT
	movl	60(%rsp), %edi
	negl	%edi
	call	__scalbnf128@PLT
	movdqa	.LC15(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jns	.L44
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L100:
	pxor	.LC9(%rip), %xmm0
	movdqa	.LC15(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm2
	pxor	%xmm0, %xmm2
	jmp	.L32
.L96:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r12d
	jmp	.L41
.L98:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	60(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 60(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L58
.L99:
	pxor	.LC9(%rip), %xmm0
	movdqa	.LC12(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm2
	pxor	%xmm0, %xmm2
	jmp	.L32
.L90:
	movdqa	%xmm2, %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__multf3@PLT
	call	__cosf128@PLT
	movdqa	%xmm0, %xmm1
	jmp	.L55
	.size	__ieee754_gammaf128_r, .-__ieee754_gammaf128_r
	.section	.rodata
	.align 32
	.type	gamma_coeff, @object
	.size	gamma_coeff, 224
gamma_coeff:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073435989
	.long	381774871
	.long	1813430636
	.long	3245086401
	.long	-1074369514
	.long	436314138
	.long	2686058912
	.long	27269633
	.long	1072996378
	.long	327235604
	.long	940802360
	.long	2167935873
	.long	-1074513901
	.long	2154714233
	.long	896592499
	.long	3803287538
	.long	1073002833
	.long	3650698365
	.long	3357502128
	.long	228168647
	.long	-1074399573
	.long	440509466
	.long	2753184164
	.long	1101273665
	.long	1073194010
	.long	2396198748
	.long	2109867594
	.long	1823536441
	.long	-1074142168
	.long	3915612640
	.long	4288776047
	.long	1669455975
	.long	1073508329
	.long	3861223549
	.long	2665208732
	.long	1880195571
	.long	-1073781642
	.long	281588608
	.long	1517614393
	.long	1126354432
	.long	1073917156
	.long	1567867129
	.long	2864086754
	.long	1381813441
	.long	-1073333838
	.long	3894103682
	.long	458129844
	.long	1317123304
	.long	1074401827
	.long	3628689696
	.long	2125725405
	.long	2330051755
	.long	-1072817639
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	3894103682
	.long	458129844
	.long	1317123304
	.long	1074401827
	.align 16
.LC3:
	.long	3628689696
	.long	2125725405
	.long	2330051755
	.long	-1072817639
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1073709056
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073909760
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1073971200
	.align 16
.LC8:
	.long	325511829
	.long	3372790523
	.long	3865572284
	.long	1073637897
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC10:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073844767
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	1074378496
	.align 16
.LC12:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC13:
	.long	0
	.long	0
	.long	0
	.long	-1081278464
	.align 16
.LC14:
	.long	0
	.long	0
	.long	0
	.long	-1073103936
	.align 16
.LC15:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC16:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC17:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073779231
	.align 16
.LC18:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
