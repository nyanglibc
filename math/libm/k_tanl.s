	.text
	.p2align 4,,15
	.globl	__kernel_tanl
	.type	__kernel_tanl, @function
__kernel_tanl:
	fldt	8(%rsp)
	fldt	24(%rsp)
	fld	%st(1)
	fabs
	flds	.LC1(%rip)
	fucomip	%st(1), %st
	jbe	.L2
	fxch	%st(2)
	fnstcw	-10(%rsp)
	movzwl	-10(%rsp), %eax
	orb	$12, %ah
	movw	%ax, -12(%rsp)
	fld	%st(0)
	fldcw	-12(%rsp)
	fistpl	-16(%rsp)
	fldcw	-10(%rsp)
	movl	-16(%rsp), %eax
	testl	%eax, %eax
	jne	.L36
	fstp	%st(1)
	fldz
	movl	$0, %edx
	fucomip	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L4
	cmpl	$-1, %edi
	jne	.L4
	fstp	%st(0)
	fld1
	fdivp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	fxch	%st(2)
.L2:
	flds	.LC5(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L8
	fxch	%st(2)
	fxam
	fnstsw	%ax
	testb	$2, %ah
	movl	$1, %edx
	je	.L10
	fchs
	fxch	%st(1)
	movl	$-1, %edx
	fchs
	fxch	%st(1)
.L10:
	fldt	.LC6(%rip)
	fsubp	%st, %st(1)
	fldt	.LC7(%rip)
	fsubp	%st, %st(2)
	faddp	%st, %st(1)
	fldz
	fxch	%st(1)
	fxch	%st(2)
.L8:
	fldt	.LC8(%rip)
	fld	%st(3)
	fmul	%st(4), %st
	fmul	%st, %st(1)
	fldt	.LC9(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC10(%rip)
	fsubrp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC11(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC12(%rip)
	fsubrp	%st, %st(2)
	fld	%st(4)
	fmul	%st(1), %st
	fldt	.LC13(%rip)
	fsubr	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC14(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC15(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC16(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC17(%rip)
	fsubrp	%st, %st(1)
	fdivrp	%st, %st(3)
	fmul	%st, %st(2)
	fxch	%st(2)
	fadd	%st(4), %st
	fmulp	%st, %st(1)
	faddp	%st, %st(3)
	fldt	.LC18(%rip)
	fmulp	%st, %st(1)
	faddp	%st, %st(2)
	fld	%st(2)
	fadd	%st(2), %st
	flds	.LC5(%rip)
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jnb	.L33
	fstp	%st(1)
	fstp	%st(1)
	cmpl	$1, %edi
	je	.L31
	fld1
	fchs
	fdivp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%edi, -20(%rsp)
	cmpl	$-1, %edx
	fildl	-20(%rsp)
	fld	%st(1)
	fmul	%st(2), %st
	fxch	%st(2)
	fadd	%st(1), %st
	fdivrp	%st, %st(2)
	fxch	%st(2)
	fsubrp	%st, %st(1)
	fsubrp	%st, %st(2)
	fld	%st(1)
	faddp	%st, %st(2)
	fsubp	%st, %st(1)
	je	.L34
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L37:
	fstp	%st(1)
.L31:
	rep ret
	.p2align 4,,10
	.p2align 3
.L34:
	fchs
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1, %edi
	je	.L35
	fstp	%st(1)
	fld1
	fchs
	fdivp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	fldt	.LC3(%rip)
	fucomip	%st(2), %st
	fstp	%st(1)
	fld	%st(0)
	jbe	.L37
	fmul	%st(1), %st
	fstp	%st(0)
	ret
	.size	__kernel_tanl, .-__kernel_tanl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	788529152
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	1059889408
	.section	.rodata.cst16
	.align 16
.LC6:
	.long	560513589
	.long	3373259426
	.long	16382
	.long	0
	.align 16
.LC7:
	.long	4237266107
	.long	3974526417
	.long	49084
	.long	0
	.align 16
.LC8:
	.long	3803029848
	.long	2863255206
	.long	49149
	.long	0
	.align 16
.LC9:
	.long	4145781100
	.long	2960462834
	.long	16390
	.long	0
	.align 16
.LC10:
	.long	3691490987
	.long	3442967154
	.long	16397
	.long	0
	.align 16
.LC11:
	.long	422517903
	.long	2704932782
	.long	16403
	.long	0
	.align 16
.LC12:
	.long	136523268
	.long	2320658831
	.long	16407
	.long	0
	.align 16
.LC13:
	.long	2709086896
	.long	2232683078
	.long	16392
	.long	0
	.align 16
.LC14:
	.long	4080175117
	.long	2631809028
	.long	16399
	.long	0
	.align 16
.LC15:
	.long	1596171917
	.long	4281126576
	.long	16404
	.long	0
	.align 16
.LC16:
	.long	2664647671
	.long	4156397203
	.long	16408
	.long	0
	.align 16
.LC17:
	.long	396426020
	.long	2175617654
	.long	16410
	.long	0
	.align 16
.LC18:
	.long	2863311531
	.long	2863311530
	.long	16381
	.long	0
