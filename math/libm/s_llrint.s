.text
.globl __llrint
.type __llrint,@function
.align 1<<4
__llrint:
 cvtsd2si %xmm0,%rax
 ret
.size __llrint,.-__llrint
.weak llrint
llrint = __llrint
.weak llrintf64
llrintf64 = __llrint
.weak llrintf32x
llrintf32x = __llrint
.globl __lrint
.set __lrint,__llrint
.weak lrint
lrint = __llrint
.weak lrintf64
lrintf64 = __llrint
.weak lrintf32x
lrintf32x = __llrint
