	.text
	.globl	__addtf3
	.p2align 4,,15
	.globl	__floorf128
	.type	__floorf128, @function
__floorf128:
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	(%rsp), %rdi
	movq	%rdx, %rax
	movq	%rdx, %rsi
	sarq	$48, %rax
	andl	$32767, %eax
	subq	$16383, %rax
	cmpq	$47, %rax
	jg	.L2
	testq	%rax, %rax
	js	.L21
	movl	%eax, %ecx
	movabsq	$281474976710655, %rdx
	shrq	%cl, %rdx
	movq	%rsi, %rcx
	andq	%rdx, %rcx
	movdqa	(%rsp), %xmm0
	orq	%rdi, %rcx
	je	.L1
	testq	%rsi, %rsi
	jns	.L6
	movabsq	$281474976710656, %rdi
	movl	%eax, %ecx
	sarq	%cl, %rdi
	addq	%rdi, %rsi
.L6:
	notq	%rdx
	xorl	%eax, %eax
	andq	%rsi, %rdx
	movq	%rax, (%rsp)
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$111, %rax
	jle	.L7
	cmpq	$16384, %rax
	movdqa	(%rsp), %xmm0
	jne	.L1
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	leal	-48(%rax), %ecx
	movq	$-1, %rdx
	movdqa	(%rsp), %xmm0
	shrq	%cl, %rdx
	testq	%rdi, %rdx
	je	.L1
	testq	%rsi, %rsi
	js	.L22
.L8:
	movq	%rdx, %rax
	movq	%rsi, %rdx
	notq	%rax
	andq	%rdi, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L21:
	testq	%rdx, %rdx
	js	.L23
	xorl	%eax, %eax
	xorl	%edx, %edx
.L4:
	movq	%rdx, 8(%rsp)
	movq	%rax, (%rsp)
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movabsq	$9223372036854775807, %rcx
	xorl	%eax, %eax
	andq	%rdx, %rcx
	orq	%rdi, %rcx
	movabsq	$-4611967493404098560, %rcx
	cmovne	%rcx, %rdx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	$48, %rax
	je	.L19
	movl	$112, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	addq	%rax, %rdi
	jnc	.L8
.L19:
	addq	$1, %rsi
	jmp	.L8
	.size	__floorf128, .-__floorf128
	.weak	floorf128
	.set	floorf128,__floorf128
