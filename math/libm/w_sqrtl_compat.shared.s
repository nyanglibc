	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__sqrtl
	.type	__sqrtl, @function
__sqrtl:
	subq	$8, %rsp
	fldt	16(%rsp)
	fldz
	fucomip	%st(1), %st
	ja	.L8
.L2:
	fstpt	16(%rsp)
	addq	$8, %rsp
	jmp	__ieee754_sqrtl@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$32, %rsp
	movl	$226, %edi
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$40, %rsp
	ret
	.size	__sqrtl, .-__sqrtl
	.weak	sqrtf64x
	.set	sqrtf64x,__sqrtl
	.weak	sqrtl
	.set	sqrtl,__sqrtl
