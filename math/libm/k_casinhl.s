	.text
	.p2align 4,,15
	.globl	__kernel_casinhl
	.type	__kernel_casinhl, @function
__kernel_casinhl:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$120, %rsp
	fldt	32(%rbp)
	fldt	16(%rbp)
	fabs
	fxch	%st(1)
	fld	%st(0)
	fstpt	-32(%rbp)
	fld	%st(0)
	fabs
	flds	.LC1(%rip)
	fxch	%st(3)
	fucomi	%st(3), %st
	jnb	.L145
	fxch	%st(1)
	fucomi	%st(3), %st
	fstp	%st(3)
	jb	.L124
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L145:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
.L2:
	testl	%ebx, %ebx
	je	.L140
	fstp	%st(1)
.L6:
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__clogl@PLT
	fldln2
	faddp	%st, %st(1)
.L138:
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fabs
	jne	.L141
.L55:
	addq	$32, %rsp
.L8:
	testl	%ebx, %ebx
	jne	.L131
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L154:
	fstp	%st(0)
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L56:
	fldt	-32(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L93
	fchs
.L93:
	movq	-8(%rbp), %rbx
	leave
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	fchs
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L124:
	flds	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	flds	.LC4(%rip)
	jb	.L9
	fld	%st(0)
	fucomip	%st(4), %st
	jbe	.L9
	fstp	%st(0)
	fxch	%st(1)
	fstpt	-80(%rbp)
	fxch	%st(1)
	subq	$16, %rsp
	movabsq	$-9223372036854775808, %r9
	fstpt	-64(%rbp)
	fld	%st(0)
	fstpt	(%rsp)
	pushq	$16383
	pushq	%r9
	fstpt	-48(%rbp)
	call	__ieee754_hypotl@PLT
	fldt	-48(%rbp)
	popq	%r10
	popq	%r11
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	-48(%rbp)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	testl	%ebx, %ebx
	popq	%rax
	fldt	-48(%rbp)
	popq	%rdx
	fldt	-64(%rbp)
	je	.L12
	fstp	%st(0)
	fldt	-80(%rbp)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
.L13:
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L131
	fchs
	jmp	.L131
.L150:
	fstp	%st(0)
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L131:
	fld1
	fstpt	-32(%rbp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L140:
	fstp	%st(2)
	fxch	%st(1)
	fxch	%st(1)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	fld	%st(0)
	fucomip	%st(2), %st
	ja	.L142
.L15:
	fld1
	fxch	%st(4)
	fucomi	%st(4), %st
	jbe	.L125
	fstp	%st(1)
	flds	.LC5(%rip)
	fucomip	%st(1), %st
	jbe	.L146
	flds	.LC3(%rip)
	fucomi	%st(2), %st
	jbe	.L147
	fld	%st(1)
	fadd	%st(5), %st
	fxch	%st(5)
	fsubr	%st(2), %st
	fmulp	%st, %st(5)
	flds	.LC6(%rip)
	fucomip	%st(3), %st
	jbe	.L126
	fstpt	-80(%rbp)
	fxch	%st(2)
	subq	$16, %rsp
	fstpt	-96(%rbp)
	fstpt	-64(%rbp)
	fld	%st(1)
	fsqrt
	fmul	%st, %st(1)
	fstpt	-48(%rbp)
	faddp	%st, %st(1)
	fadd	%st(0), %st
	fstpt	(%rsp)
	call	__log1pl@PLT
	testl	%ebx, %ebx
	popq	%rcx
	fldt	-80(%rbp)
	popq	%rsi
	fmulp	%st, %st(1)
	fldt	-48(%rbp)
	fldt	-64(%rbp)
	je	.L27
	fldt	-96(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	fabs
	testb	$2, %ah
	jne	.L134
	.p2align 4,,10
	.p2align 3
.L133:
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L142:
	flds	.LC5(%rip)
	fxch	%st(4)
	fucomi	%st(4), %st
	fstp	%st(4)
	jb	.L15
	fstp	%st(0)
	fxch	%st(1)
	fstpt	-80(%rbp)
	subq	$16, %rsp
	fstpt	-64(%rbp)
	fld1
	fld	%st(1)
	fadd	%st(1), %st
	fxch	%st(1)
	fsubr	%st(2), %st
	fmulp	%st, %st(1)
	fsqrt
	fadd	%st, %st(1)
	fstpt	-48(%rbp)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	testl	%ebx, %ebx
	popq	%rdi
	fldt	-48(%rbp)
	popq	%r8
	fldt	-64(%rbp)
	je	.L18
	fldt	-80(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	fabs
	testb	$2, %ah
	je	.L133
.L134:
	fchs
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L146:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L147:
	fstp	%st(0)
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L22
.L151:
	fstp	%st(4)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L22
.L152:
	fstp	%st(4)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	.p2align 4,,10
	.p2align 3
.L22:
	fld1
	fucomi	%st(1), %st
	jbe	.L148
	flds	.LC3(%rip)
	fucomi	%st(3), %st
	jbe	.L149
	flds	.LC10(%rip)
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jb	.L128
	fld	%st(2)
	fadd	%st(2), %st
	fxch	%st(2)
	fsub	%st(3), %st
	fmulp	%st, %st(2)
	flds	.LC6(%rip)
	fucomip	%st(4), %st
	jbe	.L129
	fstpt	-80(%rbp)
	fxch	%st(3)
	subq	$16, %rsp
	fstpt	-96(%rbp)
	fstpt	-64(%rbp)
	fxch	%st(1)
	fsqrt
	fxch	%st(1)
	fadd	%st(0), %st
	fdiv	%st(1), %st
	fxch	%st(1)
	fstpt	-48(%rbp)
.L136:
	fstpt	(%rsp)
	call	__log1pl@PLT
	testl	%ebx, %ebx
	popq	%rax
	fldt	-80(%rbp)
	popq	%rdx
	fmulp	%st, %st(1)
	fldt	-48(%rbp)
	fldt	-64(%rbp)
	je	.L51
	fstp	%st(0)
	fldt	-96(%rbp)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
.L47:
	fld	%st(1)
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fld	%st(2)
	testb	$2, %ah
	fabs
	je	.L58
	fchs
.L58:
	fldt	.LC11(%rip)
	fucomip	%st(4), %st
	fstp	%st(3)
	jbe	.L150
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L125:
	jp	.L151
	jne	.L152
	flds	.LC3(%rip)
	fucomi	%st(3), %st
	ja	.L143
	fstp	%st(0)
	fstp	%st(4)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L148:
	fstp	%st(0)
	fxch	%st(2)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L149:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(2)
.L32:
	fstpt	-80(%rbp)
	subq	$32, %rsp
	fld	%st(0)
	fsub	%st(2), %st
	fld	%st(1)
	fadd	%st(3), %st
	fmulp	%st, %st(1)
	fld1
	faddp	%st, %st(1)
	fld	%st(1)
	fadd	%st(2), %st
	fxch	%st(2)
	fstpt	-64(%rbp)
	fxch	%st(1)
	fmul	%st(2), %st
	fxch	%st(2)
	fstpt	-48(%rbp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__csqrtl@PLT
	addq	$32, %rsp
	testl	%ebx, %ebx
	fldt	-64(%rbp)
	faddp	%st, %st(1)
	fldt	-48(%rbp)
	faddp	%st, %st(2)
	fldt	-80(%rbp)
	je	.L144
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L54
	fchs
.L54:
	fxch	%st(1)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L144:
	fstp	%st(0)
	fxch	%st(1)
.L53:
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__clogl@PLT
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L143:
	fstp	%st(1)
	fxch	%st(1)
	fucomip	%st(2), %st
	jbe	.L127
	fstpt	-80(%rbp)
	fxch	%st(2)
	subq	$16, %rsp
	fstpt	-64(%rbp)
	fstpt	-96(%rbp)
	fld	%st(0)
	fsqrt
	fadd	%st, %st(1)
	fstpt	-48(%rbp)
	fadd	%st(0), %st
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	-80(%rbp)
	testl	%ebx, %ebx
	popq	%r11
	popq	%rax
	fmulp	%st, %st(1)
	fldt	-48(%rbp)
	fldt	-64(%rbp)
	je	.L36
	fstp	%st(0)
	fldt	-96(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fld1
	je	.L37
	fstp	%st(0)
	fld1
	fchs
.L37:
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L12:
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
.L14:
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L56
	fchs
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L127:
	fxch	%st(3)
	fstpt	-112(%rbp)
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	-128(%rbp)
	fldt	16(%rbp)
	fmul	%st(0), %st
	fld	%st(0)
	fadds	.LC9(%rip)
	fsqrt
	fmul	%st(2), %st
	fld	%st(1)
	fadd	%st(1), %st
	fld	%st(0)
	fmul	%st(5), %st
	fsqrt
	fxch	%st(2)
	fsubp	%st, %st(3)
	fxch	%st(2)
	fmul	%st(4), %st
	fxch	%st(4)
	fstpt	-96(%rbp)
	fxch	%st(3)
	fsqrt
	fld	%st(2)
	fstpt	-80(%rbp)
	fxch	%st(2)
	fmul	%st(3), %st
	fxch	%st(3)
	fstpt	-64(%rbp)
	fxch	%st(2)
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	-48(%rbp)
	fadd	%st(0), %st
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	-96(%rbp)
	testl	%ebx, %ebx
	popq	%r9
	popq	%r10
	fmulp	%st, %st(1)
	fldt	-48(%rbp)
	fldt	-64(%rbp)
	fldt	-80(%rbp)
	fldt	-112(%rbp)
	je	.L38
	faddp	%st, %st(3)
	fldt	-128(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(2)
	fabs
	testb	$2, %ah
	je	.L153
	fchs
	fxch	%st(2)
	jmp	.L39
.L153:
	fxch	%st(2)
.L39:
	faddp	%st, %st(1)
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L18:
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L126:
	fxch	%st(3)
	fstpt	-128(%rbp)
	subq	$16, %rsp
	fldt	16(%rbp)
	fmul	%st(0), %st
	fld	%st(1)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fld	%st(1)
	fadds	.LC7(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fld	%st(5)
	fmul	%st(6), %st
	fadd	%st(1), %st
	fsqrt
	faddp	%st, %st(6)
	fdiv	%st(5), %st
	fadd	%st(1), %st
	fmul	%st(4), %st
	fxch	%st(4)
	fstpt	-112(%rbp)
	fxch	%st(3)
	fsqrt
	fld	%st(0)
	fstpt	-48(%rbp)
	fld	%st(2)
	fmul	%st(2), %st
	fdiv	%st(1), %st
	fxch	%st(1)
	fmul	%st(3), %st
	fxch	%st(3)
	fstpt	-96(%rbp)
	fld	%st(0)
	fstpt	-64(%rbp)
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	-80(%rbp)
	faddp	%st, %st(1)
	fadd	%st(0), %st
	fxch	%st(2)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	-112(%rbp)
	testl	%ebx, %ebx
	popq	%rax
	popq	%rdx
	fmulp	%st, %st(1)
	fldt	-80(%rbp)
	fldt	-96(%rbp)
	je	.L29
	fldt	-64(%rbp)
	faddp	%st, %st(2)
	fldt	-128(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L30
	fchs
.L30:
	fldt	-48(%rbp)
	faddp	%st, %st(2)
	jmp	.L133
.L129:
	fxch	%st(4)
	fstpt	-128(%rbp)
	subq	$16, %rsp
	fldt	16(%rbp)
	fmul	%st(0), %st
	fld	%st(2)
	fadd	%st(3), %st
	fmul	%st(3), %st
	fld	%st(1)
	fadds	.LC7(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fld	%st(2)
	fmul	%st(3), %st
	fadd	%st(1), %st
	fsqrt
	faddp	%st, %st(3)
	fld	%st(1)
	fadd	%st(3), %st
	fmul	%st(6), %st
	fxch	%st(6)
	fstpt	-96(%rbp)
	fxch	%st(5)
	fsqrt
	fld	%st(0)
	fstpt	-48(%rbp)
	fld	%st(4)
	fmul	%st(4), %st
	fdiv	%st(1), %st
	fxch	%st(1)
	fmul	%st(5), %st
	fxch	%st(5)
	fstpt	-80(%rbp)
	fld	%st(0)
	fstpt	-112(%rbp)
	fmul	%st(3), %st
	fxch	%st(3)
	fstpt	-64(%rbp)
	fxch	%st(3)
	faddp	%st, %st(2)
	fxch	%st(1)
	fadd	%st(0), %st
	fxch	%st(1)
	fdivrp	%st, %st(3)
	fxch	%st(2)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	testl	%ebx, %ebx
	popq	%rdi
	fldt	-96(%rbp)
	popq	%r8
	fmulp	%st, %st(1)
	fldt	-64(%rbp)
	fldt	-80(%rbp)
	je	.L49
	fldt	-112(%rbp)
	faddp	%st, %st(2)
	fldt	-128(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L50
	fchs
.L50:
	fldt	-48(%rbp)
	faddp	%st, %st(2)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L47
.L128:
	fstpt	-80(%rbp)
	fxch	%st(3)
	subq	$32, %rsp
	fstpt	-96(%rbp)
	fstpt	-64(%rbp)
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	-48(%rbp)
	fstpt	(%rsp)
	call	__ieee754_hypotl@PLT
	fldt	-48(%rbp)
	popq	%rcx
	popq	%rsi
	fld	%st(0)
	fadd	%st(1), %st
	fxch	%st(1)
	fadd	%st(2), %st
	fxch	%st(2)
	fstpt	-48(%rbp)
	fmulp	%st, %st(1)
	jmp	.L136
.L51:
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
.L48:
	fld	%st(1)
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fld	%st(2)
	testb	$2, %ah
	fabs
	je	.L60
	fchs
.L60:
	fldt	.LC11(%rip)
	fucomip	%st(4), %st
	fstp	%st(3)
	jbe	.L154
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L38:
	fxch	%st(2)
	faddp	%st, %st(1)
	fxch	%st(1)
	faddp	%st, %st(2)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L14
.L36:
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L14
.L27:
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L14
.L29:
	fldt	-48(%rbp)
	faddp	%st, %st(1)
	fldt	-64(%rbp)
	faddp	%st, %st(2)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L14
.L49:
	fldt	-48(%rbp)
	faddp	%st, %st(1)
	fldt	-112(%rbp)
	faddp	%st, %st(2)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	jmp	.L48
	.size	__kernel_casinhl, .-__kernel_casinhl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1593835520
	.align 4
.LC3:
	.long	1056964608
	.align 4
.LC4:
	.long	511705088
	.align 4
.LC5:
	.long	1069547520
	.align 4
.LC6:
	.long	8388608
	.align 4
.LC7:
	.long	1073741824
	.align 4
.LC9:
	.long	1082130432
	.align 4
.LC10:
	.long	536870912
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC11:
	.long	0
	.long	2147483648
	.long	1
	.long	0
