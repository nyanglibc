	.text
	.p2align 4,,15
	.globl	__atan2l
	.type	__atan2l, @function
__atan2l:
	fldt	8(%rsp)
	fldt	24(%rsp)
	fld	%st(0)
	fld	%st(2)
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	fldz
	movl	$0, %edx
	fucomi	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L11
	fucomip	%st(3), %st
	fstp	%st(2)
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L10
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	fstp	%st(0)
	fstp	%st(1)
	fstp	%st(1)
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	fabs
	fldt	.LC1(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	ret
	.size	__atan2l, .-__atan2l
	.weak	atan2f64x
	.set	atan2f64x,__atan2l
	.weak	atan2l
	.set	atan2l,__atan2l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
