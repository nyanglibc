	.text
	.p2align 4,,15
	.globl	__cargf128
	.type	__cargf128, @function
__cargf128:
	movdqa	8(%rsp), %xmm1
	movdqa	24(%rsp), %xmm0
	jmp	__atan2f128@PLT
	.size	__cargf128, .-__cargf128
	.weak	cargf128
	.set	cargf128,__cargf128
