	.text
	.globl	__lttf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__kernel_sinf128
	.type	__kernel_sinf128, @function
__kernel_sinf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$64, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbp
	movq	%rbp, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1073491967, %eax
	ja	.L2
	cmpl	$1069940735, %eax
	ja	.L3
	pand	.LC0(%rip), %xmm0
	movdqa	.LC1(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L4
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L4:
	movdqa	(%rsp), %xmm0
	call	__fixtfsi@PLT
	testl	%eax, %eax
	movdqa	(%rsp), %xmm0
	je	.L1
.L3:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
.L1:
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%eax, %edx
	movl	$16382, %ecx
	shrl	$16, %edx
	movdqa	(%rsp), %xmm2
	subl	%edx, %ecx
	movl	$512, %edx
	sall	%cl, %edx
	movdqa	%xmm1, %xmm3
	addl	%edx, %eax
	movl	$-1024, %edx
	sall	%cl, %edx
	pand	.LC0(%rip), %xmm2
	andl	%edx, %eax
	testl	%ecx, %ecx
	je	.L8
	cmpl	$1, %ecx
	je	.L9
	leal	-1073491968(%rax), %ebx
	shrl	$10, %ebx
.L10:
	salq	$32, %rax
	movq	$0, (%rsp)
	testl	%edi, %edi
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
	je	.L11
	testq	%rbp, %rbp
	jns	.L12
	pxor	.LC10(%rip), %xmm3
.L12:
	movdqa	%xmm2, %xmm1
	movaps	%xmm3, (%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm3
	movdqa	%xmm3, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
.L13:
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 48(%rsp)
	call	__multf3@PLT
	leal	2(%rbx), %eax
	leaq	__sincosf128_table(%rip), %r12
	movaps	%xmm0, (%rsp)
	salq	$4, %rax
	movdqa	(%r12,%rax), %xmm4
	movdqa	.LC11(%rip), %xmm1
	movaps	%xmm4, 16(%rsp)
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	leal	3(%rbx), %eax
	salq	$4, %rbx
	salq	$4, %rax
	movdqa	(%r12,%rax), %xmm1
	call	__addtf3@PLT
	movdqa	.LC16(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movdqa	(%r12,%rbx), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	testq	%rbp, %rbp
	jns	.L1
	pxor	.LC10(%rip), %xmm0
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leal	-1073518592(%rax), %ebx
	shrl	$9, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	leal	-1073564672(%rax), %ebx
	shrl	$8, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	%xmm2, %xmm0
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L13
	.size	__kernel_sinf128, .-__kernel_sinf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.long	714772864
	.long	2060366338
	.long	291154936
	.long	1070503184
	.align 16
.LC3:
	.long	101643109
	.long	3351012875
	.long	1029369648
	.long	1071033983
	.align 16
.LC4:
	.long	1100312853
	.long	2152958618
	.long	1631224084
	.long	1071538468
	.align 16
.LC5:
	.long	3270137119
	.long	382197349
	.long	1451185230
	.long	1072016996
	.align 16
.LC6:
	.long	1953806581
	.long	955922514
	.long	978676851
	.long	1072460254
	.align 16
.LC7:
	.long	2053268118
	.long	2686058910
	.long	27269633
	.long	1072865306
	.align 16
.LC8:
	.long	286159680
	.long	286331153
	.long	286331153
	.long	1073221905
	.align 16
.LC9:
	.long	1431655760
	.long	1431655765
	.long	1431655765
	.long	1073501525
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC11:
	.long	2270477792
	.long	3406517392
	.long	4124894775
	.long	-1075238940
	.align 16
.LC12:
	.long	762139716
	.long	48035802
	.long	27269614
	.long	1072668698
	.align 16
.LC13:
	.long	3828089717
	.long	1813423462
	.long	3245086401
	.long	1073048598
	.align 16
.LC14:
	.long	1429819427
	.long	1431655765
	.long	1431655765
	.long	1073370453
	.align 16
.LC15:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC16:
	.long	3881510791
	.long	3698426781
	.long	1323895879
	.long	-1075466652
	.align 16
.LC17:
	.long	2852301633
	.long	208317009
	.long	978676836
	.long	1072460254
	.align 16
.LC18:
	.long	503844232
	.long	2686052114
	.long	27269633
	.long	1072865306
	.align 16
.LC19:
	.long	285088093
	.long	286331153
	.long	286331153
	.long	1073221905
	.align 16
.LC20:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073501525
	.align 16
.LC21:
	.long	0
	.long	0
	.long	0
	.long	1073676288
