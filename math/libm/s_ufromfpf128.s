	.text
	.p2align 4,,15
	.globl	__ufromfpf128
	.type	__ufromfpf128, @function
__ufromfpf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	cmpl	$64, %esi
	movaps	%xmm0, (%rsp)
	ja	.L39
	testl	%esi, %esi
	jne	.L2
.L30:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$64, %esi
.L2:
	movq	8(%rsp), %r9
	movq	(%rsp), %rdx
	movabsq	$9223372036854775807, %rcx
	andq	%r9, %rcx
	movq	%rcx, %rax
	orq	%rdx, %rax
	je	.L29
	shrq	$48, %rcx
	testq	%r9, %r9
	movq	%rcx, %r8
	leal	-16383(%rcx), %r10d
	js	.L5
	leal	-1(%rsi), %r11d
	cmpl	%r11d, %r10d
	jg	.L27
.L7:
	cmpl	$-1, %r10d
	jl	.L8
	movabsq	$281474976710655, %rax
	movl	$112, %ebx
	movabsq	$281474976710656, %rcx
	andq	%r9, %rax
	subl	%r10d, %ebx
	orq	%rcx, %rax
	cmpl	$64, %ebx
	jg	.L9
	movl	$111, %ecx
	movl	$1, %ebp
	subl	%r10d, %ecx
	salq	%cl, %rbp
	testq	%rdx, %rbp
	movq	%rbp, %rcx
	setne	%bpl
	subq	$1, %rcx
	testq	%rdx, %rcx
	leal	-16431(%r8), %ecx
	setne	%r12b
	salq	%cl, %rax
	movl	%ebx, %ecx
	shrq	%cl, %rdx
	orq	%rax, %rdx
	cmpl	$64, %ebx
	cmovne	%rdx, %rax
.L10:
	cmpl	$1, %edi
	je	.L12
	jle	.L84
	cmpl	$3, %edi
	je	.L15
	cmpl	$4, %edi
	jne	.L11
	testb	%bpl, %bpl
	je	.L11
	testb	$1, %al
	jne	.L48
	testb	%r12b, %r12b
	je	.L11
.L48:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%r9, %r9
	jns	.L20
.L17:
	testq	%rax, %rax
	je	.L1
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L29:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1, %edi
	je	.L42
	jle	.L85
	cmpl	$3, %edi
	je	.L34
	cmpl	$4, %edi
	jne	.L40
.L34:
	xorl	%eax, %eax
	testq	%r9, %r9
	js	.L1
	xorl	%eax, %eax
	cmpl	$63, %r11d
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$63, %r10d
	jne	.L1
	testq	%rax, %rax
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1, %edi
	movl	%esi, (%rsp)
	call	feraiseexcept@PLT
	movl	(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	je	.L86
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%r10d, %r10d
	movl	$-1, %r11d
	js	.L7
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$47, %ecx
	movl	$1, %r8d
	subl	%r10d, %ecx
	salq	%cl, %r8
	testq	%r8, %rax
	movq	%r8, %rcx
	setne	%bpl
	subq	$1, %rcx
	andq	%rax, %rcx
	orq	%rdx, %rcx
	movl	$48, %ecx
	setne	%r12b
	subl	%r10d, %ecx
	shrq	%cl, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	%bpl, %ebp
	addq	%rbp, %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L85:
	testl	%edi, %edi
	jne	.L40
	movl	$1, %r12d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
.L14:
	testq	%r9, %r9
	js	.L17
	testb	%bpl, %bpl
	jne	.L46
	testb	%r12b, %r12b
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L20:
	cmpl	$63, %r11d
	je	.L38
	leal	1(%r11), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rax, %rdx
	jne	.L1
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, %r12d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
.L12:
	testq	%r9, %r9
	jns	.L20
	testb	%bpl, %bpl
	jne	.L47
	testb	%r12b, %r12b
	je	.L17
.L47:
	addq	$1, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L84:
	testl	%edi, %edi
	je	.L14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%eax, %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L86:
	movq	$-1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$1, %rax
	jmp	.L20
	.size	__ufromfpf128, .-__ufromfpf128
	.weak	ufromfpf128
	.set	ufromfpf128,__ufromfpf128
