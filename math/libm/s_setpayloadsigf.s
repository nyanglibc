	.text
	.p2align 4,,15
	.globl	__setpayloadsigf
	.type	__setpayloadsigf, @function
__setpayloadsigf:
	movd	%xmm0, %edx
	movd	%xmm0, %eax
	shrl	$23, %edx
	leal	-127(%rdx), %ecx
	cmpl	$21, %ecx
	ja	.L2
	movl	$150, %ecx
	subl	%edx, %ecx
	movl	$-1, %edx
	sall	%cl, %edx
	notl	%edx
	testl	%eax, %edx
	jne	.L2
	testl	%eax, %eax
	movl	.LC0(%rip), %edx
	je	.L5
	andl	$8388607, %eax
	orl	$8388608, %eax
	shrl	%cl, %eax
	orl	$2139095040, %eax
	movl	%eax, %edx
.L5:
	movl	%edx, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$0x00000000, (%rdi)
	movl	$1, %eax
	ret
	.size	__setpayloadsigf, .-__setpayloadsigf
	.weak	setpayloadsigf32
	.set	setpayloadsigf32,__setpayloadsigf
	.weak	setpayloadsigf
	.set	setpayloadsigf,__setpayloadsigf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2139095040
