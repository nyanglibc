	.text
	.globl	__letf2
	.globl	__fixtfdi
	.p2align 4,,15
	.globl	__llroundf128
	.type	__llroundf128, @function
__llroundf128:
	pushq	%rbx
	movabsq	$281474976710656, %rsi
	subq	$16, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rcx
	movq	%rcx, %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	shrq	$48, %rdx
	andl	$32767, %edx
	movl	%eax, %ebx
	movabsq	$281474976710655, %rax
	andq	%rax, %rcx
	subq	$16383, %rdx
	orl	$1, %ebx
	orq	%rcx, %rsi
	cmpq	$47, %rdx
	jg	.L3
	testq	%rdx, %rdx
	js	.L29
	movl	%edx, %ecx
	movabsq	$140737488355328, %rdi
	sarq	%cl, %rdi
	movl	$48, %ecx
	addq	%rdi, %rsi
	subl	%edx, %ecx
	shrq	%cl, %rsi
.L6:
	movslq	%ebx, %rax
	imulq	%rsi, %rax
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpq	$62, %rdx
	movdqa	(%rsp), %xmm2
	jle	.L30
	movdqa	%xmm2, %xmm0
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm2, (%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jle	.L31
	movdqa	%xmm2, %xmm0
	call	__fixtfdi@PLT
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leal	-48(%rdx), %edi
	movabsq	$-9223372036854775808, %r8
	movq	%r8, %rax
	movl	%edi, %ecx
	shrq	%cl, %rax
	movq	%xmm2, %rcx
	addq	%rcx, %rax
	setc	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %rsi
	cmpq	$48, %rdx
	je	.L6
	movl	$112, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rax
	movl	%edi, %ecx
	salq	%cl, %rsi
	orq	%rax, %rsi
	cmpl	$1, %ebx
	jne	.L6
	cmpq	%r8, %rsi
	jne	.L6
	movl	$1, %edi
	movq	%rsi, (%rsp)
	call	feraiseexcept@PLT
	movq	(%rsp), %rsi
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L29:
	movslq	%ebx, %rax
	cmpq	$-1, %rdx
	movl	$0, %esi
	cmovne	%rsi, %rax
	addq	$16, %rsp
	popq	%rbx
	ret
.L31:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movabsq	$-9223372036854775808, %rax
	jmp	.L1
	.size	__llroundf128, .-__llroundf128
	.weak	llroundf128
	.set	llroundf128,__llroundf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	65536
	.long	0
	.long	-1069678592
