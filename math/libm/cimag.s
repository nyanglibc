	.text
	.p2align 4,,15
	.globl	__cimag
	.type	__cimag, @function
__cimag:
	movapd	%xmm1, %xmm0
	ret
	.size	__cimag, .-__cimag
	.weak	cimagf32x
	.set	cimagf32x,__cimag
	.weak	cimagf64
	.set	cimagf64,__cimag
	.weak	cimag
	.set	cimag,__cimag
