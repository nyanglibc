	.text
	.p2align 4,,15
	.globl	__scalbnf
	.type	__scalbnf, @function
__scalbnf:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	sarl	$23, %eax
	andl	$255, %eax
	je	.L14
	cmpl	$255, %eax
	je	.L15
.L4:
	cmpl	$-50000, %edi
	jl	.L12
	cmpl	$50000, %edi
	jg	.L6
	addl	%eax, %edi
	cmpl	$254, %edi
	jg	.L6
	testl	%edi, %edi
	jle	.L8
	andl	$-2139095041, %edx
	sall	$23, %edi
	orl	%edi, %edx
	movl	%edx, -4(%rsp)
.L1:
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	andps	.LC2(%rip), %xmm0
	orps	.LC4(%rip), %xmm0
	mulss	.LC5(%rip), %xmm0
	movss	%xmm0, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$-24, %edi
	jge	.L9
.L12:
	andps	.LC2(%rip), %xmm0
	orps	.LC1(%rip), %xmm0
	mulss	.LC3(%rip), %xmm0
	movss	%xmm0, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	andl	$2147483647, %edx
	movss	%xmm0, -4(%rsp)
	je	.L1
	mulss	.LC0(%rip), %xmm0
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	sarl	$23, %eax
	movzbl	%al, %eax
	subl	$25, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	addss	%xmm0, %xmm0
	movss	%xmm0, -4(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	addl	$25, %edi
	andl	$-2139095041, %edx
	sall	$23, %edi
	orl	%edx, %edi
	movl	%edi, -4(%rsp)
	movss	-4(%rsp), %xmm1
	mulss	.LC6(%rip), %xmm1
	movss	%xmm1, -4(%rsp)
	jmp	.L1
	.size	__scalbnf, .-__scalbnf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1275068416
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	228737632
	.long	0
	.long	0
	.long	0
	.align 16
.LC2:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	228737632
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	1900671690
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	1900671690
	.align 4
.LC6:
	.long	855638016
