	.text
	.p2align 4,,15
	.globl	__totalorderl
	.type	__totalorderl, @function
__totalorderl:
	pushq	%r12
	movl	8(%rdi), %r12d
	movl	$1, %eax
	movl	8(%rsi), %r10d
	movq	(%rdi), %r11
	movq	(%rsi), %r9
	movl	%r12d, %edi
	movl	%r10d, %esi
	sarw	$15, %di
	sarw	$15, %si
	movswl	%di, %edi
	movswl	%si, %esi
	movl	%edi, %ecx
	movl	%esi, %edx
	shrl	$17, %ecx
	shrl	$17, %edx
	xorl	%r12d, %ecx
	xorl	%r10d, %edx
	cmpw	%dx, %cx
	jl	.L1
	movl	$0, %eax
	je	.L8
.L1:
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r11, %rcx
	movq	%r9, %rdx
	movl	$1, %eax
	shrq	$32, %rcx
	shrq	$32, %rdx
	xorl	%edi, %ecx
	xorl	%esi, %edx
	cmpl	%edx, %ecx
	jb	.L1
	xorl	%esi, %r9d
	xorl	%edi, %r11d
	cmpl	%r9d, %r11d
	setbe	%sil
	xorl	%eax, %eax
	cmpl	%edx, %ecx
	sete	%al
	andl	%esi, %eax
	jmp	.L1
	.size	__totalorderl, .-__totalorderl
	.weak	totalorderf64x
	.set	totalorderf64x,__totalorderl
	.weak	totalorderl
	.set	totalorderl,__totalorderl
