	.text
	.globl	__letf2
	.globl	__multf3
	.globl	__subtf3
	.p2align 4,,15
	.type	lg_sinpi, @function
lg_sinpi:
	subq	$24, %rsp
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jle	.L8
	movdqa	%xmm2, %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__multf3@PLT
	addq	$24, %rsp
	jmp	__cosf128@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	addq	$24, %rsp
	jmp	__sinf128@PLT
	.size	lg_sinpi, .-lg_sinpi
	.p2align 4,,15
	.type	lg_cospi, @function
lg_cospi:
	subq	$24, %rsp
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jle	.L15
	movdqa	%xmm2, %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__multf3@PLT
	addq	$24, %rsp
	jmp	__sinf128@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	addq	$24, %rsp
	jmp	__cosf128@PLT
	.size	lg_cospi, .-lg_cospi
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__eqtf2
	.globl	__divtf3
	.globl	__addtf3
	.globl	__gttf2
	.p2align 4,,15
	.globl	__lgamma_negf128
	.type	__lgamma_negf128, @function
__lgamma_negf128:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$632, %rsp
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	call	__floorf128@PLT
	call	__fixtfsi@PLT
	testb	$1, %al
	movl	%eax, %ebx
	je	.L53
	notl	%eax
	movl	%eax, %edi
	shrl	$31, %edi
	addl	%eax, %edi
	sarl	%edi
	call	__floatsitf@PLT
	movaps	%xmm0, 64(%rsp)
.L21:
	subl	$4, %ebx
	movl	%ebx, %eax
	andl	$2, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	xorl	%r14d, %r14d
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 188(%rsp)
# 0 "" 2
#NO_APP
	movl	188(%rsp), %r13d
	orl	$1, %eax
	movl	%eax, 0(%rbp)
	movl	%r13d, %eax
	andb	$-97, %ah
	cmpl	%eax, %r13d
	movl	%eax, 192(%rsp)
	jne	.L54
.L23:
	movslq	%ebx, %rax
	movdqa	(%rsp), %xmm0
	salq	$5, %rax
	movq	%rax, %rdx
	leaq	lgamma_zeros(%rip), %rax
	addq	%rdx, %rax
	movdqa	(%rax), %xmm2
	movdqa	16(%rax), %xmm3
	movdqa	%xmm2, %xmm1
	movaps	%xmm2, 48(%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	cmpl	$1, %ebx
	movaps	%xmm0, 32(%rsp)
	jle	.L55
	movdqa	(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC10(%rip), %xmm4
	pand	%xmm0, %xmm4
	movdqa	48(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	movaps	%xmm4, 80(%rsp)
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm7
	movdqa	.LC10(%rip), %xmm5
	pxor	.LC11(%rip), %xmm7
	movdqa	.LC2(%rip), %xmm1
	pand	%xmm0, %xmm5
	movdqa	80(%rsp), %xmm0
	movaps	%xmm5, 64(%rsp)
	movaps	%xmm7, 112(%rsp)
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L56
	testb	$1, %bl
	movdqa	.LC2(%rip), %xmm1
	jne	.L57
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
.L32:
	movdqa	%xmm1, %xmm0
	movaps	%xmm1, 96(%rsp)
	call	lg_sinpi
	movaps	%xmm0, 64(%rsp)
	movdqa	96(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	lg_cospi
	movaps	%xmm0, 96(%rsp)
	movdqa	80(%rsp), %xmm0
	call	lg_cospi
	movaps	%xmm0, 128(%rsp)
	movdqa	80(%rsp), %xmm0
	call	lg_sinpi
	movdqa	%xmm0, %xmm1
	movdqa	128(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm3
	movaps	%xmm0, 80(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	call	__log1pf128@PLT
	movaps	%xmm0, 144(%rsp)
.L30:
	movdqa	48(%rsp), %xmm1
	movdqa	.LC6(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	.LC6(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	.LC6(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC6(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	cmpl	$19, %ebx
	movaps	%xmm0, (%rsp)
	jle	.L58
	pxor	%xmm3, %xmm3
	movaps	%xmm3, 160(%rsp)
	movaps	%xmm3, 64(%rsp)
.L33:
	xorl	%ebx, %ebx
	leaq	208(%rsp), %r12
	leaq	16+lgamma_coeff(%rip), %r15
	movdqa	.LC12(%rip), %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__divtf3@PLT
	call	__log1pf128@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	call	__log1pf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	.LC6(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	.LC6(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	96(%rsp), %xmm5
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm7
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	leaq	192(%rsp), %rax
	movaps	%xmm0, 192(%rsp)
	movdqa	.LC4(%rip), %xmm7
	movq	%rax, 80(%rsp)
	movaps	%xmm7, 32(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L59:
	movdqa	(%r15,%rbx), %xmm6
	movaps	%xmm6, 32(%rsp)
.L35:
	leaq	(%rbx,%r12), %rbp
	addq	$16, %rbx
	movdqa	(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	cmpq	$416, %rbx
	movaps	%xmm0, 0(%rbp)
	jne	.L59
	movq	80(%rsp), %rbx
	pxor	%xmm0, %xmm0
	leaq	176(%rsp), %rbp
	addq	$416, %rbx
	.p2align 4,,10
	.p2align 3
.L36:
	movdqa	(%rbx), %xmm1
	subq	$16, %rbx
	call	__addtf3@PLT
	cmpq	%rbx, %rbp
	jne	.L36
	movdqa	%xmm0, %xmm1
	movdqa	128(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	144(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%eax, %edi
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L60
	movl	%ebx, %edi
	shrl	$31, %edi
	addl	%ebx, %edi
	sarl	%edi
	negl	%edi
	call	__floatsitf@PLT
	movaps	%xmm0, 64(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L55:
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	call	__floorf128@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__subtf3@PLT
	call	__fixtfsi@PLT
	movslq	%eax, %rbx
	movl	$-33, %edi
	leal	(%rbx,%rbx), %eax
	subl	%eax, %edi
	call	__floatsitf@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	leaq	poly_end(%rip), %rdx
	leaq	poly_deg(%rip), %rax
	leaq	poly_coeff(%rip), %rcx
	movaps	%xmm0, 16(%rsp)
	movq	(%rdx,%rbx,8), %rbp
	movq	(%rax,%rbx,8), %rax
	movq	%rbp, %rdx
	salq	$4, %rdx
	testq	%rax, %rax
	movdqa	(%rcx,%rdx), %xmm0
	je	.L25
	subq	%rax, %rbp
	leaq	-16(%rcx), %rax
	leaq	-16(%rcx,%rdx), %rbx
	salq	$4, %rbp
	addq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L26:
	movdqa	16(%rsp), %xmm1
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbx, %rbp
	jne	.L26
.L25:
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	call	__log1pf128@PLT
.L27:
	testb	%r14b, %r14b
	jne	.L61
.L16:
	addq	$632, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	pxor	%xmm1, %xmm1
	movdqa	.LC6(%rip), %xmm0
	call	__divtf3@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L57:
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$21, %eax
	subl	%ebx, %eax
	movl	%eax, %ebx
	sarl	%ebx
	movl	%ebx, %edi
	call	__floatsitf@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm5
	movl	%ebx, %edi
	movdqa	%xmm0, %xmm2
	movdqa	%xmm5, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__lgamma_productf128@PLT
	call	__log1pf128@PLT
	pxor	.LC11(%rip), %xmm0
	movdqa	80(%rsp), %xmm4
	pxor	%xmm5, %xmm5
	movdqa	96(%rsp), %xmm3
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm3, 80(%rsp)
	movaps	%xmm5, 160(%rsp)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L56:
	movdqa	64(%rsp), %xmm0
	call	lg_sinpi
	movaps	%xmm0, 64(%rsp)
	movdqa	80(%rsp), %xmm0
	call	lg_sinpi
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	call	__ieee754_logf128@PLT
	movaps	%xmm0, 144(%rsp)
	jmp	.L30
.L54:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 192(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r14d
	jmp	.L23
.L61:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 188(%rsp)
# 0 "" 2
#NO_APP
	movl	188(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 188(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 188(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L16
	.size	__lgamma_negf128, .-__lgamma_negf128
	.section	.rodata
	.align 32
	.type	poly_end, @object
	.size	poly_end, 64
poly_end:
	.quad	23
	.quad	48
	.quad	74
	.quad	102
	.quad	131
	.quad	158
	.quad	183
	.quad	207
	.align 32
	.type	poly_deg, @object
	.size	poly_deg, 64
poly_deg:
	.quad	23
	.quad	24
	.quad	25
	.quad	27
	.quad	28
	.quad	26
	.quad	24
	.quad	23
	.align 32
	.type	poly_coeff, @object
	.size	poly_coeff, 3328
poly_coeff:
	.long	192588021
	.long	3949729779
	.long	3318041922
	.long	-1073804431
	.long	2564483596
	.long	707527850
	.long	998293097
	.long	-1073836428
	.long	1113515519
	.long	1363623532
	.long	336089739
	.long	-1074008956
	.long	2568551992
	.long	2470937352
	.long	994369415
	.long	-1073821959
	.long	4197709863
	.long	1459859489
	.long	2277841200
	.long	-1074068531
	.long	1120031211
	.long	1425946858
	.long	1531755539
	.long	-1073818157
	.long	2926314165
	.long	1793473008
	.long	179605705
	.long	1073362467
	.long	4180542396
	.long	1387811008
	.long	2187840815
	.long	-1073815983
	.long	4146810549
	.long	961799705
	.long	2769169461
	.long	1073493856
	.long	2223916935
	.long	2358529119
	.long	1462664232
	.long	-1073812743
	.long	451852802
	.long	1248193698
	.long	4036478393
	.long	1073551043
	.long	458832287
	.long	2916015409
	.long	3409851702
	.long	-1073807711
	.long	340898239
	.long	1289386102
	.long	2877651562
	.long	1073584087
	.long	1126559011
	.long	2654199961
	.long	584756293
	.long	-1073803996
	.long	483134757
	.long	1063396084
	.long	21089920
	.long	1073614912
	.long	749112917
	.long	454686090
	.long	790071871
	.long	-1073799351
	.long	1663505238
	.long	873038529
	.long	2347930387
	.long	1073633642
	.long	2009789592
	.long	2749054235
	.long	1511724379
	.long	-1073793514
	.long	4089645020
	.long	2248526750
	.long	3131764206
	.long	1073653912
	.long	650129214
	.long	3553919459
	.long	772594995
	.long	-1073786382
	.long	15659902
	.long	2304274744
	.long	1184071940
	.long	1073676007
	.long	2085398630
	.long	1806112372
	.long	2079729173
	.long	-1073777871
	.long	2150487639
	.long	1910620612
	.long	1016576444
	.long	1073690524
	.long	536933855
	.long	47481605
	.long	891049209
	.long	-1073764566
	.long	1600256701
	.long	3947894893
	.long	287420332
	.long	-1073814234
	.long	1570025215
	.long	1804398107
	.long	4223707605
	.long	-1073834823
	.long	3926804764
	.long	3875294768
	.long	133127555
	.long	1073532495
	.long	1840958340
	.long	3477796442
	.long	2905956447
	.long	-1073806683
	.long	834671709
	.long	399752242
	.long	1647359849
	.long	1073622483
	.long	513599757
	.long	2702638433
	.long	854839231
	.long	-1073787517
	.long	2753539661
	.long	2983497953
	.long	2360662516
	.long	1073683468
	.long	2477008660
	.long	3439499746
	.long	4121858352
	.long	-1073755738
	.long	2507383058
	.long	782866418
	.long	813684106
	.long	1073731561
	.long	2392147619
	.long	197546320
	.long	1503895243
	.long	-1073723122
	.long	2436320452
	.long	2689596525
	.long	3364477817
	.long	1073771626
	.long	3020683570
	.long	3608515386
	.long	3381334736
	.long	-1073682987
	.long	671147080
	.long	1531320719
	.long	3284235594
	.long	1073815399
	.long	3989100268
	.long	982652313
	.long	1927826053
	.long	-1073648774
	.long	3757769356
	.long	3978534302
	.long	244804051
	.long	1073854246
	.long	2983991861
	.long	993797915
	.long	3666515398
	.long	-1073606221
	.long	2916813092
	.long	1079777311
	.long	322608376
	.long	1073892852
	.long	631887737
	.long	1105207997
	.long	2857697569
	.long	-1073570440
	.long	1966688941
	.long	2958256767
	.long	3163481473
	.long	1073937106
	.long	1280667817
	.long	2662832938
	.long	719556190
	.long	-1073530685
	.long	161064219
	.long	2873700461
	.long	341198543
	.long	1073971253
	.long	2353928950
	.long	875187242
	.long	2914714589
	.long	-1073489616
	.long	1724789394
	.long	518888259
	.long	1993923146
	.long	1074012897
	.long	3590044336
	.long	2089516304
	.long	1977604081
	.long	-1073449972
	.long	2414581816
	.long	3441526072
	.long	1730734205
	.long	1074056194
	.long	3862609034
	.long	37950539
	.long	446741187
	.long	-1073827931
	.long	684741620
	.long	170400191
	.long	2470840339
	.long	-1073820372
	.long	704792840
	.long	2807146635
	.long	2488291232
	.long	1073635758
	.long	2948335837
	.long	399163777
	.long	153257270
	.long	-1073769909
	.long	3552841971
	.long	1451590370
	.long	314363658
	.long	1073730715
	.long	3460073762
	.long	1240668947
	.long	1572783648
	.long	-1073707956
	.long	692150200
	.long	760093242
	.long	2603293767
	.long	1073809438
	.long	1362087799
	.long	1999291172
	.long	2566558700
	.long	-1073639813
	.long	2809810147
	.long	1821605502
	.long	1252871983
	.long	1073880156
	.long	185847632
	.long	3726991009
	.long	1972390565
	.long	-1073569328
	.long	2225613573
	.long	1884699748
	.long	635489894
	.long	1073950277
	.long	173523517
	.long	2988636729
	.long	1356245234
	.long	-1073497885
	.long	1254397462
	.long	1139561473
	.long	1800259784
	.long	1074020398
	.long	2715123872
	.long	3207750539
	.long	3265340685
	.long	-1073425902
	.long	251756129
	.long	2457726956
	.long	3492275767
	.long	1074090707
	.long	2697719423
	.long	3539077173
	.long	1621094308
	.long	-1073353487
	.long	437308878
	.long	2286593244
	.long	4146174253
	.long	1074161270
	.long	3390103309
	.long	1508783989
	.long	1883745359
	.long	-1073281865
	.long	2886230803
	.long	1329674350
	.long	1868469411
	.long	1074232118
	.long	833282017
	.long	776742117
	.long	2579633308
	.long	-1073212469
	.long	2594736427
	.long	1045255693
	.long	971491665
	.long	1074303273
	.long	631520806
	.long	2603411770
	.long	3802735295
	.long	-1073142847
	.long	1699022657
	.long	1057440468
	.long	1197398421
	.long	1074374604
	.long	1972933083
	.long	1120090451
	.long	1391908492
	.long	-1073073128
	.long	1424869780
	.long	2990698566
	.long	3772178209
	.long	1074452931
	.long	333724439
	.long	1303314710
	.long	1745287686
	.long	-1072997618
	.long	1966822651
	.long	2522350431
	.long	1132068850
	.long	-1073844579
	.long	856219213
	.long	1401727764
	.long	3176729655
	.long	-1073796478
	.long	1688196860
	.long	1877763768
	.long	260213654
	.long	1073711106
	.long	3643027950
	.long	1246463822
	.long	3481405275
	.long	-1073702637
	.long	23154201
	.long	4188183684
	.long	546594265
	.long	1073829355
	.long	2484394376
	.long	1740408582
	.long	3684680784
	.long	-1073596331
	.long	3979688161
	.long	2140791200
	.long	854717783
	.long	1073943233
	.long	3142953288
	.long	3728054263
	.long	3798345891
	.long	-1073485164
	.long	3046620728
	.long	2059775228
	.long	1445450433
	.long	1074049882
	.long	4078763809
	.long	782437995
	.long	3517608915
	.long	-1073380547
	.long	4226809279
	.long	3947267122
	.long	1575322650
	.long	1074157600
	.long	4119704502
	.long	1176926253
	.long	4278642929
	.long	-1073270293
	.long	3470711039
	.long	1286013645
	.long	2019586952
	.long	1074270187
	.long	980958083
	.long	2722088360
	.long	2954895423
	.long	-1073159317
	.long	3666885446
	.long	3540761963
	.long	1669555194
	.long	1074376116
	.long	4266220757
	.long	4064045122
	.long	3934620243
	.long	-1073054219
	.long	2662476128
	.long	3875999581
	.long	763834896
	.long	1074484101
	.long	2640400356
	.long	2852359051
	.long	3188125238
	.long	-1072943669
	.long	147909065
	.long	1077673080
	.long	1363851284
	.long	1074596931
	.long	1682623066
	.long	3860504436
	.long	1983677203
	.long	-1072833303
	.long	1050293315
	.long	4181900630
	.long	3561556455
	.long	1074702315
	.long	3890253119
	.long	496214783
	.long	2326533485
	.long	-1072727855
	.long	4063789920
	.long	2873077585
	.long	1314107181
	.long	1074810618
	.long	2364370661
	.long	2183831308
	.long	649995654
	.long	-1072617019
	.long	800264947
	.long	2383381667
	.long	570379909
	.long	1074923446
	.long	1083431021
	.long	1537228931
	.long	2914649209
	.long	-1072507827
	.long	829396092
	.long	3313270693
	.long	3640988903
	.long	1075038245
	.long	1591948326
	.long	2533123341
	.long	3237138506
	.long	-1072391587
	.long	123083096
	.long	2104138209
	.long	2220965725
	.long	-1073944448
	.long	3689697542
	.long	2986893121
	.long	2091907026
	.long	1073728853
	.long	3145012822
	.long	2835797707
	.long	2754308556
	.long	1073791181
	.long	2730801886
	.long	221346059
	.long	1928020215
	.long	1073848641
	.long	959041505
	.long	1911576179
	.long	1556772156
	.long	1073903670
	.long	1307989017
	.long	327222546
	.long	976810312
	.long	1073959077
	.long	2797910333
	.long	2522838889
	.long	6681233
	.long	1074015191
	.long	89206720
	.long	325981848
	.long	1489006104
	.long	1074072261
	.long	412502743
	.long	701884109
	.long	2068378255
	.long	1074125410
	.long	1051370905
	.long	1923284742
	.long	1391241970
	.long	1074177466
	.long	1087195456
	.long	1620483022
	.long	3275951800
	.long	1074231009
	.long	373165923
	.long	305360285
	.long	3752477754
	.long	1074285883
	.long	2527309624
	.long	2746014684
	.long	2343946651
	.long	1074341941
	.long	388223801
	.long	550485599
	.long	815506948
	.long	1074399052
	.long	2949148002
	.long	3085613683
	.long	3156897864
	.long	1074451477
	.long	1950518928
	.long	2069374685
	.long	1509615713
	.long	1074503699
	.long	1701121386
	.long	3106442061
	.long	1368806771
	.long	1074557400
	.long	3982905274
	.long	668511459
	.long	1230970966
	.long	1074612416
	.long	3882031474
	.long	2057102134
	.long	619427402
	.long	1074668601
	.long	2763068092
	.long	484065087
	.long	79468052
	.long	1074725825
	.long	2029077584
	.long	2527103756
	.long	3982823825
	.long	1074777544
	.long	2151263125
	.long	2585532013
	.long	2392282471
	.long	1074829945
	.long	524532901
	.long	2948595748
	.long	2318579652
	.long	1074883805
	.long	2226912436
	.long	1020576478
	.long	375805974
	.long	1074938971
	.long	3616230466
	.long	3288306220
	.long	21252100
	.long	1074995291
	.long	1191920408
	.long	267541474
	.long	216702238
	.long	1075052175
	.long	1506386007
	.long	483227393
	.long	1465866676
	.long	1075102777
	.long	131517007
	.long	327788814
	.long	1479857274
	.long	1075165941
	.long	3976567429
	.long	3049650071
	.long	2309040481
	.long	1075222636
	.long	4215737205
	.long	2639356553
	.long	2494127010
	.long	-1073894028
	.long	3313061289
	.long	3340873427
	.long	58972730
	.long	1073686742
	.long	3651699323
	.long	1856470407
	.long	330148006
	.long	1073732453
	.long	892878647
	.long	1158937512
	.long	2294795930
	.long	1073770763
	.long	9329444
	.long	3379052262
	.long	1152479997
	.long	1073811776
	.long	3397435633
	.long	3762462071
	.long	1683258041
	.long	1073844926
	.long	3864385771
	.long	1340945885
	.long	3004601647
	.long	1073882550
	.long	1131643242
	.long	1052625149
	.long	3745006734
	.long	1073917015
	.long	1833050873
	.long	3592628274
	.long	692994347
	.long	1073952671
	.long	3279696326
	.long	1595836453
	.long	1934219398
	.long	1073988994
	.long	1174451721
	.long	3026601567
	.long	4090736058
	.long	1074022867
	.long	2697186973
	.long	3211631646
	.long	3695269874
	.long	1074061261
	.long	1436142706
	.long	2186744041
	.long	3454051131
	.long	1074093297
	.long	413982863
	.long	3765965064
	.long	2496810655
	.long	1074133905
	.long	1806878124
	.long	1790423783
	.long	2372960145
	.long	1074164003
	.long	148906110
	.long	2602855426
	.long	1568531391
	.long	1074203767
	.long	3154448650
	.long	1330632042
	.long	3829318845
	.long	1074235006
	.long	3326441555
	.long	284475670
	.long	1305687212
	.long	1074273279
	.long	2568082587
	.long	2709045606
	.long	859773924
	.long	1074306326
	.long	992091472
	.long	4259361267
	.long	1077597820
	.long	1074343021
	.long	3269361299
	.long	1130439220
	.long	4109211343
	.long	1074377979
	.long	3177024530
	.long	2879238510
	.long	1151126397
	.long	1074413008
	.long	3266854522
	.long	1325049848
	.long	768221593
	.long	1074449992
	.long	1266549829
	.long	3848775824
	.long	951100127
	.long	1074483125
	.long	4106974926
	.long	1381653388
	.long	1787979471
	.long	1074522101
	.long	3897353844
	.long	1657534244
	.long	776740788
	.long	1074559003
	.long	2493080900
	.long	3604452952
	.long	2292680423
	.long	1074599146
	.long	44830637
	.long	4045115457
	.long	1674176223
	.long	-1073867645
	.long	1830230338
	.long	509556958
	.long	4202091448
	.long	1073650512
	.long	4247754950
	.long	2323376644
	.long	2908501525
	.long	1073681524
	.long	3685307830
	.long	3747960247
	.long	3415262549
	.long	1073706526
	.long	1271607810
	.long	481258571
	.long	3220811305
	.long	1073730169
	.long	4042994766
	.long	365801151
	.long	2150288695
	.long	1073751854
	.long	2346600082
	.long	1134679829
	.long	2731781751
	.long	1073769146
	.long	406327517
	.long	1170530145
	.long	2003948900
	.long	1073791626
	.long	4217889417
	.long	1207938591
	.long	2028845154
	.long	1073812621
	.long	3481922073
	.long	3475414478
	.long	3010216891
	.long	1073829258
	.long	4253205605
	.long	4161735087
	.long	630660539
	.long	1073849266
	.long	2960342839
	.long	250996300
	.long	2740535945
	.long	1073873572
	.long	1592809503
	.long	2601026161
	.long	2866115630
	.long	1073888785
	.long	2558450513
	.long	3510790361
	.long	2108968179
	.long	1073907639
	.long	4094340597
	.long	1303798047
	.long	2141967768
	.long	1073930731
	.long	3132398281
	.long	4224127958
	.long	3436725366
	.long	1073948838
	.long	1101242911
	.long	3372171270
	.long	987931569
	.long	1073966346
	.long	2837263285
	.long	4168798134
	.long	28476983
	.long	1073987927
	.long	2995671727
	.long	2420430859
	.long	4034291619
	.long	1074009213
	.long	2954748392
	.long	1207785064
	.long	696264631
	.long	1074025554
	.long	1920352182
	.long	1758653904
	.long	651104062
	.long	1074045655
	.long	3588135034
	.long	3036224754
	.long	4025167358
	.long	1074069911
	.long	298658020
	.long	2662510693
	.long	1372325924
	.long	1074085108
	.long	952448838
	.long	1452130013
	.long	3259273568
	.long	1074107694
	.long	1622474168
	.long	3987740025
	.long	289712095
	.long	1074132477
	.long	3277822153
	.long	3888683925
	.long	2899298513
	.long	-1073856371
	.long	771484568
	.long	2217324979
	.long	297369613
	.long	1073622551
	.long	873616352
	.long	2818459942
	.long	1921267981
	.long	1073632532
	.long	2439895336
	.long	2605016751
	.long	2725019236
	.long	1073653225
	.long	3230872878
	.long	29107070
	.long	1412723896
	.long	1073658195
	.long	3620138778
	.long	1418444623
	.long	946068842
	.long	1073674721
	.long	1449150091
	.long	2694222112
	.long	866361048
	.long	1073677489
	.long	802797036
	.long	2839870641
	.long	1201696477
	.long	1073685173
	.long	47400115
	.long	2837311638
	.long	4243053446
	.long	1073687639
	.long	803362798
	.long	4165440801
	.long	3834201864
	.long	1073695405
	.long	2357983111
	.long	1258214834
	.long	2340120556
	.long	1073698716
	.long	2536703882
	.long	2924643363
	.long	552074368
	.long	1073706856
	.long	2395107016
	.long	4259629675
	.long	2995548929
	.long	1073711162
	.long	2209482467
	.long	3947022830
	.long	4141985362
	.long	1073719854
	.long	4039450343
	.long	2467377164
	.long	3574532762
	.long	1073725258
	.long	1306397961
	.long	497312641
	.long	115403115
	.long	1073734658
	.long	735706801
	.long	455700892
	.long	3489592226
	.long	1073741259
	.long	2222813791
	.long	3798603384
	.long	1951024284
	.long	1073746673
	.long	1741948940
	.long	235353593
	.long	174676686
	.long	1073750631
	.long	820124433
	.long	951148324
	.long	1079896092
	.long	1073756279
	.long	1312433072
	.long	4188294602
	.long	2993250677
	.long	1073760933
	.long	2190492459
	.long	3773080112
	.long	1889129480
	.long	1073767179
	.long	2121872874
	.long	3498299336
	.long	1325337933
	.long	1073775315
	.long	3073726445
	.long	2205095097
	.long	2506667266
	.long	1073782918
	.align 32
	.type	lgamma_coeff, @object
	.size	lgamma_coeff, 432
lgamma_coeff:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073435989
	.long	381774871
	.long	1813430636
	.long	3245086401
	.long	-1074369514
	.long	436314138
	.long	2686058912
	.long	27269633
	.long	1072996378
	.long	327235604
	.long	940802360
	.long	2167935873
	.long	-1074513901
	.long	2154714233
	.long	896592499
	.long	3803287538
	.long	1073002833
	.long	3650698365
	.long	3357502128
	.long	228168647
	.long	-1074399573
	.long	440509466
	.long	2753184164
	.long	1101273665
	.long	1073194010
	.long	2396198748
	.long	2109867594
	.long	1823536441
	.long	-1074142168
	.long	3915612640
	.long	4288776047
	.long	1669455975
	.long	1073508329
	.long	3861223549
	.long	2665208732
	.long	1880195571
	.long	-1073781642
	.long	281588608
	.long	1517614393
	.long	1126354432
	.long	1073917156
	.long	1567867129
	.long	2864086754
	.long	1381813441
	.long	-1073333838
	.long	3894103682
	.long	458129844
	.long	1317123304
	.long	1074401827
	.long	3628689696
	.long	2125725405
	.long	2330051755
	.long	-1072817639
	.long	2679670996
	.long	2743350236
	.long	144338449
	.long	1074942370
	.long	897026060
	.long	2005404717
	.long	2601790773
	.long	-1072246520
	.long	3861132216
	.long	2039024877
	.long	2700493627
	.long	1075539241
	.long	2595787500
	.long	3982314500
	.long	433710182
	.long	-1071627183
	.long	619094385
	.long	1315575568
	.long	2613695490
	.long	1076183927
	.long	48403949
	.long	3360966763
	.long	3082553256
	.long	-1070962688
	.long	3451246025
	.long	1439139667
	.long	3606639095
	.long	1076870284
	.long	1050652937
	.long	932610209
	.long	3964727013
	.long	-1070256390
	.long	342514334
	.long	2108823254
	.long	1727968737
	.long	1077593538
	.long	1609764252
	.long	539847154
	.long	671815089
	.long	-1069517887
	.long	1452904830
	.long	2408580685
	.long	3723742773
	.long	1078348783
	.long	616093965
	.long	278508745
	.long	30077608
	.long	-1068744225
	.long	3960871360
	.long	3140693792
	.long	4237407279
	.long	1079136727
	.align 32
	.type	lgamma_zeros, @object
	.size	lgamma_zeros, 3072
lgamma_zeros:
	.long	2561641732
	.long	3243636303
	.long	3378515846
	.long	-1073726849
	.long	3982849282
	.long	208225651
	.long	2533319883
	.long	1066326649
	.long	1513230167
	.long	213699282
	.long	279035280
	.long	-1073717324
	.long	1741511177
	.long	742163722
	.long	23659839
	.long	-1081180522
	.long	3446271197
	.long	4158414644
	.long	3687441818
	.long	-1073704352
	.long	1578119352
	.long	956062567
	.long	853535512
	.long	-1081240299
	.long	155750838
	.long	1354263083
	.long	357024510
	.long	-1073677753
	.long	350989035
	.long	1770922757
	.long	1225849401
	.long	-1081178559
	.long	3279446085
	.long	204531650
	.long	3884292440
	.long	-1073675644
	.long	725425392
	.long	306315371
	.long	1076445550
	.long	1066357736
	.long	2417862177
	.long	3443799082
	.long	2007428783
	.long	-1073660043
	.long	32590182
	.long	1870189029
	.long	3615565096
	.long	-1081146921
	.long	3110424147
	.long	2429195029
	.long	2776566637
	.long	-1073659770
	.long	1915958639
	.long	4267132666
	.long	2936287444
	.long	-1081086778
	.long	2335132795
	.long	3589755829
	.long	794369856
	.long	-1073643543
	.long	232352993
	.long	761781207
	.long	4175814330
	.long	-1081083677
	.long	1706318049
	.long	3470270219
	.long	2992150472
	.long	-1073643498
	.long	3749358459
	.long	3474874148
	.long	2891354944
	.long	-1081095290
	.long	3648032757
	.long	4291778714
	.long	3212228718
	.long	-1073627140
	.long	3647422823
	.long	3956231482
	.long	3488990295
	.long	1066393791
	.long	1936613857
	.long	3950842379
	.long	1071570913
	.long	-1073627133
	.long	2464094623
	.long	1678227458
	.long	2654316365
	.long	1066357977
	.long	2907221043
	.long	4042434464
	.long	2549618076
	.long	-1073610753
	.long	4291932485
	.long	3947021050
	.long	2009608928
	.long	1066361553
	.long	2642648465
	.long	2481723751
	.long	872581951
	.long	-1073610752
	.long	1042544836
	.long	3216893819
	.long	1101882259
	.long	1066419404
	.long	3534656075
	.long	33525961
	.long	4198007997
	.long	-1073602561
	.long	1318923010
	.long	485982644
	.long	34662392
	.long	-1081088030
	.long	2145093284
	.long	2967132936
	.long	96958095
	.long	-1073602560
	.long	1438289855
	.long	344697065
	.long	2544934155
	.long	1066404418
	.long	3472138409
	.long	4221176917
	.long	4285271419
	.long	-1073594369
	.long	66408656
	.long	1199672888
	.long	2320000535
	.long	1066191638
	.long	2998676104
	.long	1931802251
	.long	9695863
	.long	-1073594368
	.long	4061605210
	.long	2838002644
	.long	2262536518
	.long	-1081090000
	.long	2589308835
	.long	1043754421
	.long	4294085853
	.long	-1073586177
	.long	1236505452
	.long	3891439243
	.long	2626355397
	.long	-1081171788
	.long	1308611714
	.long	2787881675
	.long	881442
	.long	-1073586176
	.long	2694063215
	.long	3887594947
	.long	3338662451
	.long	1066382357
	.long	2203682728
	.long	1894191036
	.long	4294893842
	.long	-1073577985
	.long	923291975
	.long	3953461272
	.long	333650845
	.long	1066447930
	.long	2178959902
	.long	2397448911
	.long	73453
	.long	-1073577984
	.long	1971931003
	.long	432195784
	.long	117093292
	.long	-1081031735
	.long	4106221803
	.long	3119263730
	.long	4294961645
	.long	-1073569793
	.long	1990766237
	.long	1997966625
	.long	1807194485
	.long	1066161593
	.long	129644320
	.long	1175683277
	.long	5650
	.long	-1073569792
	.long	2398401605
	.long	215362864
	.long	69980334
	.long	-1081019976
	.long	155697466
	.long	1756722115
	.long	4294966892
	.long	-1073561601
	.long	3712279891
	.long	764508531
	.long	3297658870
	.long	-1081141395
	.long	2632702126
	.long	2538245074
	.long	403
	.long	-1073561600
	.long	3971935113
	.long	1470201074
	.long	1042612711
	.long	-1081110667
	.long	165568622
	.long	403445964
	.long	4294967269
	.long	-1073553409
	.long	1852030666
	.long	1036090476
	.long	926252190
	.long	-1081083747
	.long	2048689183
	.long	3891521331
	.long	26
	.long	-1073553408
	.long	4277142546
	.long	487845944
	.long	2355899940
	.long	-1081247109
	.long	3292439133
	.long	1367392652
	.long	4294967294
	.long	-1073545217
	.long	3421238432
	.long	2067118263
	.long	3717444289
	.long	1066312549
	.long	2644591180
	.long	3611270969
	.long	0
	.long	-1073545216
	.long	87114154
	.long	1241529401
	.long	2208435223
	.long	1066250523
	.long	3886628380
	.long	4082539591
	.long	4294967295
	.long	-1073541121
	.long	35634827
	.long	1715137262
	.long	683147818
	.long	-1081009534
	.long	408324231
	.long	212427704
	.long	0
	.long	-1073541120
	.long	3165814765
	.long	1992467826
	.long	2392406789
	.long	-1080970602
	.long	3795063597
	.long	4283165756
	.long	4294967295
	.long	-1073537025
	.long	2755905672
	.long	2207720858
	.long	3837988752
	.long	-1081207210
	.long	499903653
	.long	11801539
	.long	0
	.long	-1073537024
	.long	2417272158
	.long	1752404360
	.long	43656630
	.long	1066459752
	.long	1556045652
	.long	4294346162
	.long	4294967295
	.long	-1073532929
	.long	3823699218
	.long	2113358426
	.long	1287184673
	.long	-1080986698
	.long	2738921644
	.long	621133
	.long	0
	.long	-1073532928
	.long	2944682978
	.long	3813677999
	.long	943210488
	.long	1066531105
	.long	1366292471
	.long	4294936239
	.long	4294967295
	.long	-1073528833
	.long	1842841030
	.long	353635039
	.long	4190647646
	.long	-1080971431
	.long	2928674825
	.long	31056
	.long	0
	.long	-1073528832
	.long	3315799908
	.long	158756892
	.long	3437064880
	.long	1066512304
	.long	474106051
	.long	4294965817
	.long	4294967295
	.long	-1073524737
	.long	1228331036
	.long	556698905
	.long	145590997
	.long	1066496396
	.long	3820861245
	.long	1478
	.long	0
	.long	-1073524736
	.long	2794748330
	.long	2895779607
	.long	3581629640
	.long	-1080987253
	.long	3340388640
	.long	4294967228
	.long	4294967295
	.long	-1073520641
	.long	86571856
	.long	2060606982
	.long	3952527181
	.long	-1081087142
	.long	954578656
	.long	67
	.long	0
	.long	-1073520640
	.long	1032454540
	.long	3949346382
	.long	3959730930
	.long	1066396506
	.long	331971997
	.long	4294967293
	.long	4294967295
	.long	-1073516545
	.long	562292543
	.long	760395096
	.long	646212479
	.long	-1081030682
	.long	3962995299
	.long	2
	.long	0
	.long	-1073516544
	.long	3570277022
	.long	397313460
	.long	646219383
	.long	1066452966
	.long	3771928551
	.long	4294967295
	.long	4294967295
	.long	-1073512449
	.long	438651
	.long	590116613
	.long	3744633555
	.long	1066519509
	.long	523038745
	.long	0
	.long	0
	.long	-1073512448
	.long	271380090
	.long	280549312
	.long	3744633549
	.long	-1080964139
	.long	4274045746
	.long	4294967295
	.long	4294967295
	.long	-1073508353
	.long	1231764784
	.long	3702495797
	.long	731221466
	.long	-1081051952
	.long	20921550
	.long	0
	.long	0
	.long	-1073508352
	.long	2869658320
	.long	3786993369
	.long	731221466
	.long	1066431696
	.long	4294162621
	.long	4294967295
	.long	4294967295
	.long	-1073504257
	.long	2530622923
	.long	3615746418
	.long	3543021679
	.long	-1081357608
	.long	804675
	.long	0
	.long	0
	.long	-1073504256
	.long	534989838
	.long	3619793804
	.long	3543021679
	.long	1066126040
	.long	4294937493
	.long	4294967295
	.long	4294967295
	.long	-1073500161
	.long	3224775171
	.long	3419282669
	.long	1714133994
	.long	-1081030748
	.long	29803
	.long	0
	.long	0
	.long	-1073500160
	.long	904725371
	.long	3419282845
	.long	1714133994
	.long	1066452900
	.long	4294966232
	.long	4294967295
	.long	4294967295
	.long	-1073496065
	.long	1663244298
	.long	1012683203
	.long	889740599
	.long	1066502694
	.long	1064
	.long	0
	.long	0
	.long	-1073496064
	.long	1177456715
	.long	1012683203
	.long	889740599
	.long	-1080980954
	.long	4294967259
	.long	4294967295
	.long	4294967295
	.long	-1073491969
	.long	1283651493
	.long	705591492
	.long	2783263380
	.long	-1081003981
	.long	37
	.long	0
	.long	0
	.long	-1073491968
	.long	1284235070
	.long	705591492
	.long	2783263380
	.long	1066479667
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	-1073487873
	.long	1632391354
	.long	811954026
	.long	673442567
	.long	1066453398
	.long	1
	.long	0
	.long	0
	.long	-1073487872
	.long	1632390045
	.long	811954026
	.long	673442567
	.long	-1081030250
	.long	0
	.long	0
	.long	0
	.long	-1073483776
	.long	1873199046
	.long	3291356900
	.long	779632475
	.long	1066287949
	.long	0
	.long	0
	.long	0
	.long	-1073483776
	.long	1873199041
	.long	3291356900
	.long	779632475
	.long	-1081195699
	.long	0
	.long	0
	.long	0
	.long	-1073479680
	.long	1873199044
	.long	3291356900
	.long	779632475
	.long	1065960269
	.long	0
	.long	0
	.long	0
	.long	-1073479680
	.long	1873199043
	.long	3291356900
	.long	779632475
	.long	-1081523379
	.long	0
	.long	0
	.long	0
	.long	-1073477632
	.long	4289295394
	.long	458457805
	.long	625856725
	.long	1065630081
	.long	0
	.long	0
	.long	0
	.long	-1073477632
	.long	4289295394
	.long	458457805
	.long	625856725
	.long	-1081853567
	.long	0
	.long	0
	.long	0
	.long	-1073475584
	.long	1257887415
	.long	2705295915
	.long	589041623
	.long	1065297680
	.long	0
	.long	0
	.long	0
	.long	-1073475584
	.long	1257887415
	.long	2705295915
	.long	589041623
	.long	-1082185968
	.long	0
	.long	0
	.long	0
	.long	-1073473536
	.long	1518208548
	.long	4068686975
	.long	2624679313
	.long	1064963525
	.long	0
	.long	0
	.long	0
	.long	-1073473536
	.long	1518208548
	.long	4068686975
	.long	2624679313
	.long	-1082520123
	.long	0
	.long	0
	.long	0
	.long	-1073471488
	.long	312944476
	.long	2461035405
	.long	848347850
	.long	1064624024
	.long	0
	.long	0
	.long	0
	.long	-1073471488
	.long	312944476
	.long	2461035405
	.long	848347850
	.long	-1082859624
	.long	0
	.long	0
	.long	0
	.long	-1073469440
	.long	1547536851
	.long	2941024433
	.long	3519630981
	.long	1064279752
	.long	0
	.long	0
	.long	0
	.long	-1073469440
	.long	1547536851
	.long	2941024433
	.long	3519630981
	.long	-1083203896
	.long	0
	.long	0
	.long	0
	.long	-1073467392
	.long	172934376
	.long	2928753975
	.long	3416001594
	.long	1063935306
	.long	0
	.long	0
	.long	0
	.long	-1073467392
	.long	172934376
	.long	2928753975
	.long	3416001594
	.long	-1083548342
	.long	0
	.long	0
	.long	0
	.long	-1073465344
	.long	1793805371
	.long	3724608583
	.long	1150962604
	.long	1063591577
	.long	0
	.long	0
	.long	0
	.long	-1073465344
	.long	1793805371
	.long	3724608583
	.long	1150962604
	.long	-1083892071
	.long	0
	.long	0
	.long	0
	.long	-1073463296
	.long	1152101675
	.long	805412978
	.long	3559527086
	.long	1063242382
	.long	0
	.long	0
	.long	0
	.long	-1073463296
	.long	1152101675
	.long	805412978
	.long	3559527086
	.long	-1084241266
	.long	0
	.long	0
	.long	0
	.long	-1073461248
	.long	3518083805
	.long	209593807
	.long	473550884
	.long	1062888934
	.long	0
	.long	0
	.long	0
	.long	-1073461248
	.long	3518083805
	.long	209593807
	.long	473550884
	.long	-1084594714
	.long	0
	.long	0
	.long	0
	.long	-1073459200
	.long	635222282
	.long	2204913042
	.long	1383411934
	.long	1062539439
	.long	0
	.long	0
	.long	0
	.long	-1073459200
	.long	635222282
	.long	2204913042
	.long	1383411934
	.long	-1084944209
	.long	0
	.long	0
	.long	0
	.long	-1073457152
	.long	1844393760
	.long	584891109
	.long	1160085073
	.long	1062180315
	.long	0
	.long	0
	.long	0
	.long	-1073457152
	.long	1844393760
	.long	584891109
	.long	1160085073
	.long	-1085303333
	.long	0
	.long	0
	.long	0
	.long	-1073455104
	.long	2122280425
	.long	2768084786
	.long	453246662
	.long	1061824299
	.long	0
	.long	0
	.long	0
	.long	-1073455104
	.long	2122280425
	.long	2768084786
	.long	453246662
	.long	-1085659349
	.long	0
	.long	0
	.long	0
	.long	-1073453056
	.long	2445692076
	.long	2027957342
	.long	2267160676
	.long	1061462987
	.long	0
	.long	0
	.long	0
	.long	-1073453056
	.long	2445692076
	.long	2027957342
	.long	2267160676
	.long	-1086020661
	.long	0
	.long	0
	.long	0
	.long	-1073451008
	.long	1327875592
	.long	103588974
	.long	83253585
	.long	1061102600
	.long	0
	.long	0
	.long	0
	.long	-1073451008
	.long	1327875592
	.long	103588974
	.long	83253585
	.long	-1086381048
	.long	0
	.long	0
	.long	0
	.long	-1073448960
	.long	3635816677
	.long	2608378964
	.long	3768657899
	.long	1060736424
	.long	0
	.long	0
	.long	0
	.long	-1073448960
	.long	3635816677
	.long	2608378964
	.long	3768657899
	.long	-1086747224
	.long	0
	.long	0
	.long	0
	.long	-1073446912
	.long	3855533550
	.long	1738919309
	.long	1080782834
	.long	1060374811
	.long	0
	.long	0
	.long	0
	.long	-1073446912
	.long	3855533550
	.long	1738919309
	.long	1080782834
	.long	-1087108837
	.long	0
	.long	0
	.long	0
	.long	-1073444864
	.long	1091441202
	.long	1570022397
	.long	4041206536
	.long	1060002370
	.long	0
	.long	0
	.long	0
	.long	-1073444864
	.long	1091441202
	.long	1570022397
	.long	4041206536
	.long	-1087481278
	.long	0
	.long	0
	.long	0
	.long	-1073442816
	.long	1397044738
	.long	2181427360
	.long	4141952215
	.long	1059633974
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC1:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073779231
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC4:
	.long	381774871
	.long	1813430636
	.long	3245086401
	.long	-1074369514
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-1073741824
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	-1073610752
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073938432
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073414144
	.align 16
.LC10:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC12:
	.long	2889895546
	.long	2503303096
	.long	2830189942
	.long	1073765360
	.align 16
.LC13:
	.long	1326035465
	.long	1902752941
	.long	3318623262
	.long	1066329998
	.align 16
.LC14:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073435989
