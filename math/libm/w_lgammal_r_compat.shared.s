	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__lgammal_r
	.type	__lgammal_r, @function
__lgammal_r:
	subq	$40, %rsp
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__ieee754_lgammal_r@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L15
	fstp	%st(0)
.L12:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	fldt	48(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L12
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L12
	fstp	%st(0)
	fldt	48(%rsp)
	fnstcw	22(%rsp)
	movzwl	22(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 20(%rsp)
	fldcw	20(%rsp)
	frndint
	fldcw	22(%rsp)
	fldt	48(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L16
	jne	.L17
	fldz
	xorl	%edi, %edi
	fucomip	%st(1), %st
	fstp	%st(0)
	setnb	%dil
	addl	$214, %edi
.L4:
	fldt	48(%rsp)
	fstpl	24(%rsp)
	movsd	24(%rsp), %xmm0
	movapd	%xmm0, %xmm1
	call	__kernel_standard@PLT
	movsd	%xmm0, 8(%rsp)
	fldl	8(%rsp)
	jmp	.L12
.L16:
	fstp	%st(0)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	fstp	%st(0)
.L7:
	movl	$214, %edi
	jmp	.L4
	.size	__lgammal_r, .-__lgammal_r
	.weak	lgammaf64x_r
	.set	lgammaf64x_r,__lgammal_r
	.weak	lgammal_r
	.set	lgammal_r,__lgammal_r
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
