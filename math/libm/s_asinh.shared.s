	.text
	.p2align 4,,15
	.globl	__asinh
	.type	__asinh, @function
__asinh:
	movq	%xmm0, %rax
	subq	$40, %rsp
	movapd	%xmm0, %xmm1
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1043333119, %eax
	jle	.L17
	cmpl	$1102053376, %eax
	jg	.L7
	movapd	%xmm0, %xmm5
	movsd	.LC3(%rip), %xmm4
	movq	.LC0(%rip), %xmm6
	cmpl	$1073741824, %eax
	mulsd	%xmm0, %xmm5
	movapd	%xmm0, %xmm7
	andpd	%xmm6, %xmm7
	movapd	%xmm5, %xmm2
	addsd	%xmm4, %xmm2
	sqrtsd	%xmm2, %xmm2
	jle	.L9
	addsd	%xmm7, %xmm2
	movsd	%xmm0, (%rsp)
	addsd	%xmm7, %xmm7
	movaps	%xmm6, 16(%rsp)
	divsd	%xmm2, %xmm4
	movapd	%xmm7, %xmm0
	addsd	%xmm4, %xmm0
	call	__ieee754_log@PLT
	movsd	(%rsp), %xmm1
	movapd	16(%rsp), %xmm6
.L11:
	movapd	%xmm1, %xmm3
	andpd	%xmm6, %xmm0
	andpd	.LC5(%rip), %xmm3
	orpd	%xmm3, %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movapd	%xmm0, %xmm7
	movq	.LC0(%rip), %xmm6
	movsd	.LC1(%rip), %xmm0
	andpd	%xmm6, %xmm7
	ucomisd	%xmm7, %xmm0
	jbe	.L3
	movapd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
.L3:
	movsd	.LC2(%rip), %xmm2
	movsd	.LC3(%rip), %xmm4
	movapd	%xmm1, %xmm0
	addsd	%xmm1, %xmm2
	ucomisd	%xmm4, %xmm2
	ja	.L1
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	sqrtsd	%xmm3, %xmm2
	.p2align 4,,10
	.p2align 3
.L9:
	addsd	%xmm4, %xmm2
	movsd	%xmm1, (%rsp)
	movaps	%xmm6, 16(%rsp)
	divsd	%xmm2, %xmm5
	addsd	%xmm7, %xmm5
	movapd	%xmm5, %xmm0
	call	__log1p@PLT
	movapd	16(%rsp), %xmm6
	movsd	(%rsp), %xmm1
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$2146435071, %eax
	jle	.L10
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	.LC0(%rip), %xmm6
	movsd	%xmm0, 16(%rsp)
	andpd	%xmm6, %xmm0
	movaps	%xmm6, (%rsp)
	call	__ieee754_log@PLT
	movapd	(%rsp), %xmm6
	addsd	.LC4(%rip), %xmm0
	movsd	16(%rsp), %xmm1
	jmp	.L11
	.size	__asinh, .-__asinh
	.weak	asinhf32x
	.set	asinhf32x,__asinh
	.weak	asinhf64
	.set	asinhf64,__asinh
	.weak	asinh
	.set	asinh,__asinh
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1048576
	.align 8
.LC2:
	.long	2281731484
	.long	2117592124
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.align 8
.LC4:
	.long	4277811695
	.long	1072049730
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
