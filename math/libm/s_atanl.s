	.text
	.p2align 4,,15
	.globl	__atanl
	.type	__atanl, @function
__atanl:
	fldt	8(%rsp)
#APP
# 15 "../sysdeps/i386/fpu/s_atanl.c" 1
	fld1
fpatan
# 0 "" 2
#NO_APP
	ret
	.size	__atanl, .-__atanl
	.weak	atanf64x
	.set	atanf64x,__atanl
	.weak	atanl
	.set	atanl,__atanl
