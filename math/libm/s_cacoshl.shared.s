	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cacoshl
	.type	__cacoshl, @function
__cacoshl:
	subq	$24, %rsp
	fldt	32(%rsp)
	fldt	48(%rsp)
	fld	%st(1)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L2
	fldt	.LC6(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L54
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L60
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L55
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L56
	fldz
	fxch	%st(1)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L54:
	fldt	.LC7(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	jnb	.L4
	fldz
	fxch	%st(5)
	fucomi	%st(5), %st
	jp	.L61
	je	.L5
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L32
.L61:
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L32:
	fucomi	%st(0), %st
	jp	.L62
	fldt	.LC6(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L63
	fstp	%st(0)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L65:
	fstp	%st(1)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L66:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
.L30:
	fldt	.LC1(%rip)
	fxch	%st(1)
.L16:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fabs
	je	.L17
	fchs
.L17:
	flds	.LC5(%rip)
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L64
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L65
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L63:
	fxch	%st(1)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L67:
	fstp	%st(2)
	jmp	.L9
.L68:
	fxch	%st(1)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L69:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L9:
	fld	%st(0)
	subq	$32, %rsp
	movl	$1, %edi
	fstpt	32(%rsp)
	fchs
	fxch	%st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_casinhl@PLT
	fldt	32(%rsp)
	addq	$32, %rsp
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L57
	fchs
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	fxch	%st(3)
	fucomi	%st(0), %st
	jp	.L58
	fucomi	%st(2), %st
	fstp	%st(2)
	ja	.L66
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L67
	fxch	%st(1)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L68
	jne	.L69
	fstp	%st(0)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC1(%rip)
	je	.L24
	fstp	%st(0)
	fldt	.LC8(%rip)
.L24:
	addq	$24, %rsp
	fldz
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fstp	%st(1)
	fstp	%st(1)
	fucomi	%st(0), %st
	jp	.L70
	fldt	.LC6(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L48
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
.L51:
	flds	.LC5(%rip)
	flds	.LC4(%rip)
	addq	$24, %rsp
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	fxch	%st(1)
	fchs
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L56:
	fldpi
	fxch	%st(1)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L62:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L64:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L48
.L70:
	fstp	%st(0)
.L48:
	flds	.LC4(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	fldz
	fucomip	%st(2), %st
	fstp	%st(1)
	ja	.L59
	fldt	.LC0(%rip)
	fxch	%st(1)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L59:
	fldpi
	fldt	.LC0(%rip)
	fsubrp	%st, %st(1)
	fxch	%st(1)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L58:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fldt	.LC1(%rip)
	flds	.LC4(%rip)
	jmp	.L1
	.size	__cacoshl, .-__cacoshl
	.weak	cacoshf64x
	.set	cacoshf64x,__cacoshl
	.weak	cacoshl
	.set	cacoshl,__cacoshl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	560513589
	.long	3373259426
	.long	16382
	.long	0
	.align 16
.LC1:
	.long	560513589
	.long	3373259426
	.long	16383
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC4:
	.long	2143289344
	.align 4
.LC5:
	.long	2139095040
	.section	.rodata.cst16
	.align 16
.LC6:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC7:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC8:
	.long	560513589
	.long	3373259426
	.long	49151
	.long	0
