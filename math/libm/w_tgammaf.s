	.text
	.p2align 4,,15
	.globl	__tgammaf
	.type	__tgammaf, @function
__tgammaf:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	movss	%xmm0, 12(%rsp)
	call	__ieee754_gammaf_r@PLT
	movss	.LC0(%rip), %xmm2
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm2, %xmm4
	movss	12(%rsp), %xmm1
	ucomiss	%xmm4, %xmm3
	jb	.L2
	ucomiss	.LC2(%rip), %xmm0
	jp	.L3
	je	.L2
.L3:
	movl	28(%rsp), %eax
	testl	%eax, %eax
	jns	.L1
	xorps	.LC5(%rip), %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movaps	%xmm1, %xmm4
	andps	%xmm2, %xmm4
	ucomiss	%xmm4, %xmm3
	jnb	.L20
	ucomiss	.LC1(%rip), %xmm4
	jbe	.L3
	pxor	%xmm3, %xmm3
	ucomiss	%xmm1, %xmm3
	jbe	.L3
	movaps	%xmm1, %xmm3
	movss	.LC3(%rip), %xmm5
	movaps	%xmm1, %xmm4
	andps	%xmm2, %xmm3
	ucomiss	%xmm3, %xmm5
	jbe	.L15
	cvttss2si	%xmm1, %eax
	pxor	%xmm3, %xmm3
	movss	.LC4(%rip), %xmm5
	andnps	%xmm1, %xmm2
	cvtsi2ss	%eax, %xmm3
	movaps	%xmm3, %xmm4
	cmpnless	%xmm1, %xmm4
	andps	%xmm5, %xmm4
	subss	%xmm4, %xmm3
	movaps	%xmm3, %xmm4
	orps	%xmm2, %xmm4
.L15:
	ucomiss	%xmm4, %xmm1
	jp	.L11
	jne	.L11
.L16:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	pxor	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm1
	jp	.L8
	jne	.L8
.L11:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movaps	%xmm1, %xmm4
	movss	.LC3(%rip), %xmm6
	movaps	%xmm1, %xmm5
	andps	%xmm2, %xmm4
	ucomiss	%xmm4, %xmm6
	jbe	.L10
	cvttss2si	%xmm1, %eax
	pxor	%xmm4, %xmm4
	movss	.LC4(%rip), %xmm6
	andnps	%xmm1, %xmm2
	cvtsi2ss	%eax, %xmm4
	movaps	%xmm4, %xmm5
	cmpnless	%xmm1, %xmm5
	andps	%xmm6, %xmm5
	subss	%xmm5, %xmm4
	movaps	%xmm4, %xmm5
	orps	%xmm2, %xmm5
.L10:
	ucomiss	%xmm1, %xmm5
	jp	.L11
	jne	.L11
	ucomiss	%xmm1, %xmm3
	jbe	.L11
	jmp	.L16
	.size	__tgammaf, .-__tgammaf
	.weak	tgammaf32
	.set	tgammaf32,__tgammaf
	.weak	tgammaf
	.set	tgammaf,__tgammaf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	0
	.align 4
.LC3:
	.long	1258291200
	.align 4
.LC4:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
