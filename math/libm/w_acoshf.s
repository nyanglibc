	.text
	.p2align 4,,15
	.globl	__acoshf
	.type	__acoshf, @function
__acoshf:
	movss	.LC0(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.L4
	jmp	__ieee754_acoshf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_acoshf@PLT
	.size	__acoshf, .-__acoshf
	.weak	acoshf32
	.set	acoshf32,__acoshf
	.weak	acoshf
	.set	acoshf,__acoshf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
