	.text
#APP
	.symver __totalorder0,totalorder@@GLIBC_2.31
	.symver __totalorder1,totalorderf64@@GLIBC_2.31
	.symver __totalorder2,totalorderf32x@@GLIBC_2.31
	.symver __totalorder_compat3,totalorder@GLIBC_2.25
	.symver __totalorder_compat4,totalorderf64@GLIBC_2.27
	.symver __totalorder_compat5,totalorderf32x@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__totalorder
	.type	__totalorder, @function
__totalorder:
	movq	(%rdi), %rax
	movq	(%rsi), %rcx
	cqto
	shrq	%rdx
	xorq	%rax, %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	shrq	%rax
	xorq	%rcx, %rax
	cmpq	%rax, %rdx
	setle	%al
	movzbl	%al, %eax
	ret
	.size	__totalorder, .-__totalorder
	.globl	__totalorder2
	.set	__totalorder2,__totalorder
	.globl	__totalorder1
	.set	__totalorder1,__totalorder
	.globl	__totalorder0
	.set	__totalorder0,__totalorder
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalorder_compat
	.type	__totalorder_compat, @function
__totalorder_compat:
	subq	$24, %rsp
	leaq	8(%rsp), %rdi
	movq	%rsp, %rsi
	movsd	%xmm0, 8(%rsp)
	movsd	%xmm1, (%rsp)
	call	__totalorder@PLT
	addq	$24, %rsp
	ret
	.size	__totalorder_compat, .-__totalorder_compat
	.globl	__totalorder_compat5
	.set	__totalorder_compat5,__totalorder_compat
	.globl	__totalorder_compat4
	.set	__totalorder_compat4,__totalorder_compat
	.globl	__totalorder_compat3
	.set	__totalorder_compat3,__totalorder_compat
