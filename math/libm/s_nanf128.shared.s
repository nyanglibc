	.text
	.p2align 4,,15
	.globl	__nanf128
	.type	__nanf128, @function
__nanf128:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__strtof128_nan@PLT
	.size	__nanf128, .-__nanf128
	.weak	nanf128
	.set	nanf128,__nanf128
