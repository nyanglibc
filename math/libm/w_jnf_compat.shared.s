	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__jnf
	.type	__jnf, @function
__jnf:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L10
.L2:
	jmp	__ieee754_jnf@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	pxor	%xmm2, %xmm2
	movaps	%xmm0, %xmm1
	cvtsi2ss	%edi, %xmm2
	movl	$138, %edi
	movaps	%xmm2, %xmm0
	jmp	__kernel_standard_f@PLT
	.size	__jnf, .-__jnf
	.weak	jnf32
	.set	jnf32,__jnf
	.weak	jnf
	.set	jnf,__jnf
	.p2align 4,,15
	.globl	__ynf
	.type	__ynf, @function
__ynf:
	pxor	%xmm2, %xmm2
	ucomiss	%xmm0, %xmm2
	jnb	.L18
	ucomiss	.LC1(%rip), %xmm0
	ja	.L18
.L32:
	jmp	__ieee754_ynf@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L32
	pushq	%rbx
	movaps	%xmm0, %xmm1
	movl	%edi, %ebx
	subq	$16, %rsp
	ucomiss	%xmm0, %xmm2
	ja	.L35
	ucomiss	%xmm2, %xmm0
	jp	.L16
	je	.L36
.L16:
	cmpl	$2, %eax
	jne	.L37
	addq	$16, %rsp
	popq	%rbx
	jmp	__ieee754_ynf@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$4, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	pxor	%xmm0, %xmm0
	movl	$112, %edi
	movss	12(%rsp), %xmm1
	addq	$16, %rsp
	cvtsi2ss	%ebx, %xmm0
	popq	%rbx
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	pxor	%xmm0, %xmm0
	addq	$16, %rsp
	movl	$139, %edi
	cvtsi2ss	%ebx, %xmm0
	popq	%rbx
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	pxor	%xmm0, %xmm0
	movl	$113, %edi
	movss	12(%rsp), %xmm1
	addq	$16, %rsp
	cvtsi2ss	%ebx, %xmm0
	popq	%rbx
	jmp	__kernel_standard_f@PLT
	.size	__ynf, .-__ynf
	.weak	ynf32
	.set	ynf32,__ynf
	.weak	ynf
	.set	ynf,__ynf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1514737627
