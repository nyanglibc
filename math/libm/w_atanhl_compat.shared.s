	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__atanhl
	.type	__atanhl, @function
__atanhl:
	subq	$8, %rsp
	fldt	16(%rsp)
	fld	%st(0)
	fabs
	fld1
	fxch	%st(1)
	fucomi	%st(1), %st
	jnb	.L10
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L11:
	fstp	%st(0)
	fstp	%st(0)
.L2:
	fstpt	16(%rsp)
	addq	$8, %rsp
	jmp	__ieee754_atanhl@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L11
	xorl	%edi, %edi
	fucomip	%st(1), %st
	fstp	%st(0)
	setbe	%dil
	subq	$32, %rsp
	fld	%st(0)
	fstpt	16(%rsp)
	addl	$230, %edi
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$40, %rsp
	ret
	.size	__atanhl, .-__atanhl
	.weak	atanhf64x
	.set	atanhf64x,__atanhl
	.weak	atanhl
	.set	atanhl,__atanhl
