	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cabsl
	.type	__cabsl, @function
__cabsl:
	jmp	__hypotl@PLT
	.size	__cabsl, .-__cabsl
	.weak	cabsf64x
	.set	cabsf64x,__cabsl
	.weak	cabsl
	.set	cabsl,__cabsl
