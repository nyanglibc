	.text
#APP
	.symver __ieee754_acoshf,__acoshf_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_acoshf
	.type	__ieee754_acoshf, @function
__ieee754_acoshf:
	movd	%xmm0, %eax
	cmpl	$1065353215, %eax
	jle	.L12
	cmpl	$1300234239, %eax
	jle	.L4
	cmpl	$2139095039, %eax
	jle	.L5
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1065353216, %eax
	je	.L7
	cmpl	$1073741824, %eax
	jg	.L13
	subss	.LC2(%rip), %xmm0
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm2
	addss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	addss	%xmm2, %xmm1
	sqrtss	%xmm1, %xmm1
	addss	%xmm1, %xmm0
	jmp	__log1pf@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	subss	%xmm0, %xmm0
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	subq	$8, %rsp
	call	__ieee754_logf@PLT
	addss	.LC1(%rip), %xmm0
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movaps	%xmm0, %xmm1
	movss	.LC2(%rip), %xmm2
	movaps	%xmm0, %xmm3
	mulss	%xmm0, %xmm1
	addss	%xmm0, %xmm3
	subss	%xmm2, %xmm1
	sqrtss	%xmm1, %xmm1
	addss	%xmm1, %xmm0
	divss	%xmm0, %xmm2
	subss	%xmm2, %xmm3
	movaps	%xmm3, %xmm0
	jmp	__ieee754_logf@PLT
	.size	__ieee754_acoshf, .-__ieee754_acoshf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1060205080
	.align 4
.LC2:
	.long	1065353216
