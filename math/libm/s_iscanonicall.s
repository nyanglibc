	.text
	.p2align 4,,15
	.globl	__iscanonicall
	.type	__iscanonicall, @function
__iscanonicall:
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx
	shrq	$32, %rax
	movl	%eax, %edx
	shrl	$31, %eax
	notl	%edx
	shrl	$31, %edx
	testl	$32767, %ecx
	cmove	%edx, %eax
	ret
	.size	__iscanonicall, .-__iscanonicall
