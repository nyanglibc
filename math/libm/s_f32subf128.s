	.text
	.globl	__eqtf2
	.globl	__subtf3
	.globl	__trunctfsf2
	.globl	__unordtf2
	.globl	__gttf2
	.p2align 4,,15
	.globl	__f32subf128
	.type	__f32subf128, @function
__f32subf128:
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L18
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	call	__trunctfsf2@PLT
	movaps	%xmm0, %xmm2
	andps	.LC0(%rip), %xmm0
	movss	.LC1(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L1
	ucomiss	%xmm2, %xmm2
	jp	.L11
.L9:
	movdqa	(%rsp), %xmm3
	pand	.LC2(%rip), %xmm3
	movss	%xmm2, 32(%rsp)
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movss	32(%rsp), %xmm2
	jne	.L1
	movdqa	(%rsp), %xmm3
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movss	32(%rsp), %xmm2
	jg	.L1
	movdqa	16(%rsp), %xmm3
	pand	.LC2(%rip), %xmm3
	movss	%xmm2, (%rsp)
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movss	(%rsp), %xmm2
	jne	.L1
	movdqa	16(%rsp), %xmm3
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movss	(%rsp), %xmm2
	jle	.L21
.L1:
	addq	$88, %rsp
	movaps	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebp
	movl	%ebp, %eax
	andl	$-32704, %eax
	orl	$32640, %eax
	movl	%eax, 76(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %eax
	orl	%ebp, %eax
	movl	%eax, 76(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %ebp
	notl	%ebp
	testl	%edi, %ebp
	jne	.L23
.L6:
	shrl	$5, %ebx
	movq	%xmm0, %rax
	movabsq	$-4294967296, %rdx
	andl	$1, %ebx
	orl	32(%rsp), %ebx
	andq	%rdx, %rax
	orq	%rbx, %rax
	movq	%rax, 32(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__trunctfsf2@PLT
	movaps	%xmm0, %xmm2
	andps	.LC0(%rip), %xmm0
	movss	.LC1(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jb	.L24
	ucomiss	.LC4(%rip), %xmm2
	jp	.L1
	jne	.L1
.L21:
	movq	errno@gottpoff(%rip), %rax
	movaps	%xmm2, %xmm0
	movl	$34, %fs:(%rax)
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	ucomiss	%xmm2, %xmm2
	jnp	.L9
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	movss	%xmm2, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movss	32(%rsp), %xmm2
	jne	.L1
.L11:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movaps	%xmm0, 48(%rsp)
	call	__feraiseexcept@PLT
	movdqa	48(%rsp), %xmm0
	jmp	.L6
	.size	__f32subf128, .-__f32subf128
	.weak	f32subf128
	.set	f32subf128,__f32subf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	0
