	.text
	.globl	__divtf3
	.globl	__multf3
	.globl	__floatditf
	.p2align 4,,15
	.globl	__logbf128
	.type	__logbf128, @function
__logbf128:
	subq	$24, %rsp
	movabsq	$9223372036854775807, %rcx
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	(%rsp), %rdx
	andq	%rcx, %rax
	movq	%rdx, %rsi
	orq	%rax, %rsi
	je	.L10
	movabsq	$9223090561878065151, %rcx
	cmpq	%rcx, %rax
	jg	.L11
	movq	%rax, %rdi
	sarq	$48, %rdi
	testq	%rdi, %rdi
	jne	.L5
	testq	%rax, %rax
	jne	.L6
	bsrq	%rdx, %rdi
	xorq	$63, %rdi
	addl	$64, %edi
.L7:
	subl	$16, %edi
	movslq	%edi, %rdi
	negq	%rdi
.L5:
	subq	$16383, %rdi
	call	__floatditf@PLT
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	.LC0(%rip), %xmm1
	pand	%xmm0, %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__divtf3@PLT
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	bsrq	%rax, %rdi
	xorl	$63, %edi
	jmp	.L7
	.size	__logbf128, .-__logbf128
	.weak	logbf128
	.set	logbf128,__logbf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
