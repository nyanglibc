	.text
	.p2align 4,,15
	.globl	__sincosl
	.type	__sincosl, @function
__sincosl:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
	movq	88(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16381, %ax
	jle	.L2
	cmpw	$16382, %ax
	jne	.L3
	movq	80(%rsp), %rdx
	shrq	$32, %rdx
	cmpl	$-921707870, %edx
	ja	.L3
.L2:
	fldz
	subq	$16, %rsp
	xorl	%edi, %edi
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	pushq	104(%rsp)
	pushq	104(%rsp)
	call	__kernel_sinl@PLT
	fstpt	0(%rbp)
	popq	%rsi
	popq	%rdi
	fldt	16(%rsp)
	fstpt	(%rsp)
	pushq	104(%rsp)
	pushq	104(%rsp)
	call	__kernel_cosl@PLT
	fstpt	(%rbx)
	addq	$32, %rsp
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpw	$32767, %ax
	jne	.L5
	fldt	80(%rsp)
	fld	%st(0)
	fsub	%st(0), %st
	fld	%st(0)
	fstpt	(%rbx)
	fstpt	0(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	16(%rsp), %rdi
	pushq	88(%rsp)
	pushq	88(%rsp)
	call	__ieee754_rem_pio2l@PLT
	andl	$3, %eax
	cmpl	$1, %eax
	popq	%rdx
	popq	%rcx
	je	.L7
	cmpl	$2, %eax
	je	.L8
	testl	%eax, %eax
	je	.L23
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_cosl@PLT
	fchs
	addq	$32, %rsp
	movl	$1, %edi
	fstpt	0(%rbp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_sinl@PLT
	addq	$32, %rsp
	fstpt	(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_cosl@PLT
	fstpt	0(%rbp)
	addq	$32, %rsp
	movl	$1, %edi
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_sinl@PLT
	fchs
	addq	$32, %rsp
	fstpt	(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	pushq	40(%rsp)
	pushq	40(%rsp)
	movl	$1, %edi
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_sinl@PLT
	fstpt	0(%rbp)
	addq	$32, %rsp
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_cosl@PLT
	addq	$32, %rsp
	fstpt	(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	40(%rsp)
	pushq	40(%rsp)
	movl	$1, %edi
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_sinl@PLT
	fchs
	addq	$32, %rsp
	fstpt	0(%rbp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_cosl@PLT
	fchs
	addq	$32, %rsp
	fstpt	(%rbx)
	jmp	.L1
	.size	__sincosl, .-__sincosl
	.weak	sincosf64x
	.set	sincosf64x,__sincosl
	.weak	sincosl
	.set	sincosl,__sincosl
