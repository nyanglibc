	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpf
	.type	__fromfpf, @function
__fromfpf:
	pushq	%r13
	pushq	%r12
	movd	%xmm0, %edx
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$64, %esi
	ja	.L35
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$64, %esi
.L10:
	movl	%edx, %ecx
	andl	$2147483647, %ecx
	je	.L36
	movl	%edx, %r11d
	shrl	$23, %ecx
	shrl	$31, %r11d
	leal	-127(%rcx), %r9d
	leal	-2(%rsi,%r11), %r10d
	cmpl	%r10d, %r9d
	jg	.L32
	movl	%edx, %eax
	andl	$8388607, %eax
	orl	$8388608, %eax
	cmpl	$22, %r9d
	jle	.L15
	subl	$150, %ecx
	xorl	%r13d, %r13d
	salq	%cl, %rax
.L16:
	cmpl	$1, %edi
	je	.L40
	jle	.L61
	cmpl	$3, %edi
	je	.L41
	xorl	%ecx, %ecx
	cmpl	$4, %edi
	je	.L30
	.p2align 4,,10
	.p2align 3
.L17:
	testl	%edx, %edx
	js	.L23
.L26:
	leal	1(%r10), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L9
.L32:
	addq	$8, %rsp
	movl	%r11d, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$-1, %r9d
	jl	.L37
	movl	$22, %ecx
	movl	$1, %r8d
	movl	%eax, %r12d
	subl	%r9d, %ecx
	sall	%cl, %r8d
	andl	%r8d, %r12d
	movl	%r8d, %ecx
	setne	%bpl
	subl	$1, %ecx
	movl	%ecx, %ebx
	movl	$23, %ecx
	andl	%eax, %ebx
	setne	%r13b
	subl	%r9d, %ecx
	shrl	%cl, %eax
	cmpl	$1, %edi
	movl	%eax, %eax
	movq	%rax, %r8
	je	.L18
	jle	.L60
	cmpl	$3, %edi
	je	.L21
	cmpl	$4, %edi
	jne	.L17
	testl	%r12d, %r12d
	je	.L38
	andl	$1, %r8d
	xorl	%ecx, %ecx
	orl	%ebx, %r8d
	setne	%cl
.L30:
	addq	%rcx, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%ebp, %ebp
.L60:
	testl	%edi, %edi
	jne	.L17
	testl	%edx, %edx
	js	.L23
	testb	%bpl, %bpl
	jne	.L43
	testb	%r13b, %r13b
	je	.L26
.L43:
	addq	$1, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%ebp, %ebp
.L18:
	testl	%edx, %edx
	jns	.L26
	testb	%bpl, %bpl
	jne	.L44
	testb	%r13b, %r13b
	je	.L23
.L44:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	%r10d, %r9d
	je	.L62
.L31:
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%ebp, %ebp
.L29:
	addq	%rbp, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, %r13d
	xorl	%eax, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %edx
	movl	%r9d, %ecx
	salq	%cl, %rdx
	cmpq	%rax, %rdx
	jne	.L32
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	%bpl, %ebp
	jmp	.L29
.L38:
	xorl	%ecx, %ecx
	jmp	.L30
	.size	__fromfpf, .-__fromfpf
	.weak	fromfpf32
	.set	fromfpf32,__fromfpf
	.weak	fromfpf
	.set	fromfpf,__fromfpf
