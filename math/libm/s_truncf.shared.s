	.text
	.p2align 4,,15
	.globl	__truncf
	.type	__truncf, @function
__truncf:
	movd	%xmm0, %ecx
	movd	%xmm0, %edx
	sarl	$23, %ecx
	movzbl	%cl, %ecx
	subl	$127, %ecx
	cmpl	$22, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L7
	movl	$8388607, %eax
	sarl	%cl, %eax
	notl	%eax
	andl	%edx, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movss	%xmm0, -4(%rsp)
	addl	$-128, %ecx
	movss	-4(%rsp), %xmm0
	je	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andl	$-2147483648, %edx
	movl	%edx, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.size	__truncf, .-__truncf
	.weak	truncf32
	.set	truncf32,__truncf
	.weak	truncf
	.set	truncf,__truncf
