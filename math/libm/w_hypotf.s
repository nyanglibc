	.text
	.p2align 4,,15
	.globl	__hypotf
	.type	__hypotf, @function
__hypotf:
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_hypotf@PLT
	movss	.LC0(%rip), %xmm4
	movaps	%xmm0, %xmm5
	movss	.LC1(%rip), %xmm1
	andps	%xmm4, %xmm5
	movss	8(%rsp), %xmm2
	movss	12(%rsp), %xmm3
	ucomiss	%xmm5, %xmm1
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andps	%xmm4, %xmm2
	ucomiss	%xmm2, %xmm1
	jb	.L1
	andps	%xmm4, %xmm3
	ucomiss	%xmm3, %xmm1
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__hypotf, .-__hypotf
	.weak	hypotf32
	.set	hypotf32,__hypotf
	.weak	hypotf
	.set	hypotf,__hypotf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
