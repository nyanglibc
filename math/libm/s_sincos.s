	.text
	.p2align 4,,15
	.globl	__sincos
	.type	__sincos, @function
__sincos:
	pushq	%r13
	pushq	%r12
	xorl	%r13d, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$72, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 48(%rsp)
# 0 "" 2
#NO_APP
	movl	48(%rsp), %ebx
	movl	%ebx, %eax
	andb	$-97, %ah
	cmpl	%ebx, %eax
	movl	%eax, 56(%rsp)
	jne	.L66
.L2:
	movq	%xmm0, %rax
	sarq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1073965308, %eax
	jg	.L3
	movq	.LC2(%rip), %xmm5
	movapd	%xmm0, %xmm2
	cmpl	$1044381695, %eax
	andpd	%xmm5, %xmm2
	jg	.L4
	movsd	.LC3(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	ja	.L67
.L5:
	movq	.LC4(%rip), %rax
	movsd	%xmm0, 0(%rbp)
	movq	%rax, (%r12)
.L7:
	testb	%r13b, %r13b
	je	.L1
.L64:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 56(%rsp)
# 0 "" 2
#NO_APP
	movl	56(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 56(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 56(%rsp)
# 0 "" 2
#NO_APP
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$2146435071, %eax
	jg	.L21
	cmpl	$1100554746, %eax
	jg	.L22
	movsd	.LC20(%rip), %xmm3
	movsd	.LC21(%rip), %xmm1
	mulsd	%xmm0, %xmm3
	movsd	.LC24(%rip), %xmm4
	addsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm2
	movq	%xmm3, %rax
	subsd	%xmm1, %xmm2
	movsd	.LC22(%rip), %xmm1
	andl	$3, %eax
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm4
	subsd	%xmm1, %xmm0
	movsd	.LC23(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	.LC25(%rip), %xmm2
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	subsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm6
	subsd	%xmm1, %xmm0
	subsd	%xmm2, %xmm6
	subsd	%xmm4, %xmm0
	subsd	%xmm6, %xmm1
	subsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
.L23:
	movl	%eax, %edx
	andl	$3, %edx
	subl	$1, %edx
	cmpl	$1, %edx
	ja	.L24
	movq	.LC17(%rip), %xmm4
	xorpd	%xmm4, %xmm6
	xorpd	%xmm4, %xmm0
.L24:
	testb	$1, %al
	jne	.L25
	movq	%r12, %rdx
	movq	%rbp, %r12
	movq	%rdx, %rbp
.L25:
	movq	.LC2(%rip), %xmm5
	movapd	%xmm6, %xmm2
	movsd	.LC5(%rip), %xmm3
	leaq	__sincostab(%rip), %rcx
	andpd	%xmm5, %xmm2
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm11
	addsd	%xmm3, %xmm1
	movq	%xmm1, %rdx
	movapd	%xmm1, %xmm4
	movsd	.LC6(%rip), %xmm1
	sall	$2, %edx
	subsd	%xmm3, %xmm4
	movslq	%edx, %rsi
	movsd	(%rcx,%rsi,8), %xmm14
	leal	1(%rdx), %esi
	movslq	%esi, %rsi
	subsd	%xmm4, %xmm11
	movsd	(%rcx,%rsi,8), %xmm10
	leal	2(%rdx), %esi
	addl	$3, %edx
	ucomisd	%xmm2, %xmm1
	movslq	%esi, %rsi
	movslq	%edx, %rdx
	movsd	(%rcx,%rsi,8), %xmm13
	movsd	(%rcx,%rdx,8), %xmm3
	jbe	.L60
	movapd	%xmm6, %xmm1
	movsd	.LC7(%rip), %xmm5
	movsd	.LC16(%rip), %xmm8
	mulsd	%xmm6, %xmm1
	movapd	%xmm0, %xmm2
	movsd	.LC13(%rip), %xmm7
	mulsd	%xmm8, %xmm2
	pxor	%xmm4, %xmm4
	movsd	%xmm7, 16(%rsp)
	mulsd	%xmm1, %xmm5
	movsd	.LC15(%rip), %xmm9
	addsd	.LC8(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	subsd	.LC9(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	addsd	.LC10(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	subsd	.LC11(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	subsd	%xmm2, %xmm5
	movsd	.LC12(%rip), %xmm2
	mulsd	%xmm1, %xmm5
	movsd	.LC14(%rip), %xmm1
	addsd	%xmm0, %xmm5
	addsd	%xmm6, %xmm5
.L28:
	ucomisd	%xmm6, %xmm4
	movsd	%xmm5, (%r12)
	jbe	.L31
	xorpd	.LC17(%rip), %xmm0
.L31:
	addsd	%xmm11, %xmm0
	testb	$2, %al
	movapd	%xmm0, %xmm4
	movapd	%xmm0, %xmm11
	mulsd	%xmm0, %xmm4
	mulsd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm1
	mulsd	%xmm4, %xmm11
	subsd	16(%rsp), %xmm2
	subsd	%xmm9, %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	%xmm11, %xmm2
	addsd	%xmm1, %xmm8
	addsd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm8
	mulsd	%xmm0, %xmm10
	mulsd	%xmm14, %xmm0
	mulsd	%xmm13, %xmm8
	subsd	%xmm10, %xmm3
	subsd	%xmm8, %xmm3
	subsd	%xmm0, %xmm3
	addsd	%xmm3, %xmm13
	je	.L33
	xorpd	.LC17(%rip), %xmm13
.L33:
	movsd	%xmm13, 0(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L21:
	movapd	%xmm0, %xmm1
	andpd	.LC2(%rip), %xmm1
	ucomisd	.LC26(%rip), %xmm1
	ja	.L68
.L34:
	divsd	%xmm0, %xmm0
	testb	%r13b, %r13b
	movsd	%xmm0, (%r12)
	movsd	%xmm0, 0(%rbp)
	jne	.L64
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1072390143, %eax
	jle	.L69
	movsd	.LC18(%rip), %xmm3
	movsd	.LC19(%rip), %xmm1
	subsd	%xmm2, %xmm3
	pxor	%xmm4, %xmm4
	movapd	%xmm3, %xmm10
	addsd	%xmm1, %xmm10
	subsd	%xmm10, %xmm3
	ucomisd	%xmm10, %xmm4
	addsd	%xmm1, %xmm3
	ja	.L70
	movq	.LC17(%rip), %xmm6
	movapd	%xmm3, %xmm7
	movaps	%xmm6, (%rsp)
.L14:
	movapd	%xmm10, %xmm13
	leaq	__sincostab(%rip), %rdx
	andpd	%xmm5, %xmm13
	movsd	.LC5(%rip), %xmm2
	movsd	.LC13(%rip), %xmm14
	movapd	%xmm13, %xmm1
	movapd	%xmm13, %xmm12
	movsd	%xmm14, 16(%rsp)
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm6
	movq	%xmm1, %rax
	movsd	.LC14(%rip), %xmm1
	subsd	%xmm2, %xmm6
	movsd	.LC12(%rip), %xmm2
	sall	$2, %eax
	movslq	%eax, %rcx
	subsd	%xmm6, %xmm12
	addsd	%xmm12, %xmm7
	movapd	%xmm7, %xmm11
	movapd	%xmm7, %xmm6
	mulsd	%xmm7, %xmm11
	movapd	%xmm6, %xmm8
	movapd	%xmm11, %xmm7
	mulsd	%xmm11, %xmm8
	mulsd	%xmm2, %xmm7
	subsd	%xmm14, %xmm7
	mulsd	%xmm8, %xmm7
	movsd	.LC16(%rip), %xmm8
	addsd	%xmm6, %xmm7
	movsd	(%rdx,%rcx,8), %xmm6
	leal	1(%rax), %ecx
	movsd	%xmm6, 24(%rsp)
	movslq	%ecx, %rcx
	movsd	(%rdx,%rcx,8), %xmm6
	leal	2(%rax), %ecx
	addl	$3, %eax
	cltq
	movsd	%xmm6, 32(%rsp)
	mulsd	%xmm7, %xmm6
	movslq	%ecx, %rcx
	movsd	(%rdx,%rax,8), %xmm9
	movsd	(%rdx,%rcx,8), %xmm15
	movsd	%xmm9, 40(%rsp)
	mulsd	24(%rsp), %xmm7
	subsd	%xmm6, %xmm9
	movapd	%xmm9, %xmm14
	movapd	%xmm11, %xmm9
	mulsd	%xmm1, %xmm9
	movapd	%xmm9, %xmm6
	movsd	.LC15(%rip), %xmm9
	subsd	%xmm9, %xmm6
	mulsd	%xmm11, %xmm6
	addsd	%xmm8, %xmm6
	mulsd	%xmm11, %xmm6
	mulsd	%xmm15, %xmm6
	subsd	%xmm6, %xmm14
	subsd	%xmm7, %xmm14
	movapd	%xmm0, %xmm7
	movsd	.LC6(%rip), %xmm0
	andpd	(%rsp), %xmm7
	ucomisd	%xmm13, %xmm0
	addsd	%xmm15, %xmm14
	andpd	%xmm5, %xmm14
	orpd	%xmm7, %xmm14
	movsd	%xmm14, 0(%rbp)
	jbe	.L59
	movapd	%xmm10, %xmm0
	movsd	.LC7(%rip), %xmm5
	mulsd	%xmm3, %xmm8
	mulsd	%xmm10, %xmm0
	mulsd	%xmm0, %xmm5
	addsd	.LC8(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	subsd	.LC9(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	.LC10(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	subsd	.LC11(%rip), %xmm5
	mulsd	%xmm10, %xmm5
	subsd	%xmm8, %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm5, %xmm3
	addsd	%xmm10, %xmm3
.L18:
	movsd	%xmm3, (%r12)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L68:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L69:
	movsd	.LC5(%rip), %xmm3
	leaq	__sincostab(%rip), %rdx
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm10
	addsd	%xmm3, %xmm1
	movq	%xmm1, %rax
	movapd	%xmm1, %xmm7
	movsd	.LC6(%rip), %xmm1
	sall	$2, %eax
	subsd	%xmm3, %xmm7
	movslq	%eax, %rcx
	movsd	(%rdx,%rcx,8), %xmm13
	leal	1(%rax), %ecx
	movslq	%ecx, %rcx
	subsd	%xmm7, %xmm10
	movsd	(%rdx,%rcx,8), %xmm7
	leal	2(%rax), %ecx
	addl	$3, %eax
	ucomisd	%xmm2, %xmm1
	movslq	%ecx, %rcx
	cltq
	movsd	(%rdx,%rcx,8), %xmm12
	movsd	(%rdx,%rax,8), %xmm3
	jbe	.L57
	movapd	%xmm0, %xmm1
	movsd	.LC7(%rip), %xmm5
	pxor	%xmm4, %xmm4
	mulsd	%xmm0, %xmm1
	movsd	.LC13(%rip), %xmm6
	movsd	.LC12(%rip), %xmm2
	movsd	%xmm6, 16(%rsp)
	mulsd	%xmm1, %xmm5
	movsd	.LC15(%rip), %xmm9
	movsd	.LC16(%rip), %xmm8
	addsd	.LC8(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	subsd	.LC9(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	addsd	.LC10(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	subsd	.LC11(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	subsd	%xmm4, %xmm5
	mulsd	%xmm1, %xmm5
	movsd	.LC14(%rip), %xmm1
	addsd	%xmm4, %xmm5
	addsd	%xmm0, %xmm5
.L11:
	ucomisd	%xmm0, %xmm4
	movsd	%xmm5, 0(%rbp)
	jbe	.L13
	movsd	.LC1(%rip), %xmm4
.L13:
	addsd	%xmm10, %xmm4
	movapd	%xmm4, %xmm0
	mulsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm1
	movapd	%xmm2, %xmm10
	movapd	%xmm4, %xmm2
	subsd	16(%rsp), %xmm10
	mulsd	%xmm0, %xmm2
	subsd	%xmm9, %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm10
	addsd	%xmm1, %xmm8
	addsd	%xmm4, %xmm10
	mulsd	%xmm0, %xmm8
	mulsd	%xmm10, %xmm7
	mulsd	%xmm13, %xmm10
	mulsd	%xmm12, %xmm8
	subsd	%xmm7, %xmm3
	subsd	%xmm8, %xmm3
	subsd	%xmm10, %xmm3
	addsd	%xmm3, %xmm12
	movsd	%xmm12, (%r12)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L67:
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdi
	call	__branred@PLT
	movsd	48(%rsp), %xmm6
	movsd	56(%rsp), %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L60:
	pxor	%xmm4, %xmm4
	ucomisd	%xmm6, %xmm4
	jnb	.L71
	movq	.LC17(%rip), %xmm7
	movapd	%xmm0, %xmm15
	movaps	%xmm7, (%rsp)
.L29:
	movapd	%xmm11, %xmm12
	mulsd	%xmm11, %xmm12
	movsd	.LC12(%rip), %xmm2
	movsd	.LC13(%rip), %xmm7
	movsd	.LC16(%rip), %xmm8
	movapd	%xmm12, %xmm1
	movsd	%xmm7, 16(%rsp)
	movapd	%xmm12, %xmm9
	mulsd	%xmm2, %xmm1
	subsd	%xmm7, %xmm1
	movapd	%xmm12, %xmm7
	mulsd	%xmm11, %xmm7
	mulsd	%xmm7, %xmm1
	addsd	%xmm15, %xmm1
	mulsd	%xmm11, %xmm15
	addsd	%xmm11, %xmm1
	movapd	%xmm1, %xmm7
	movsd	%xmm1, 24(%rsp)
	movsd	.LC14(%rip), %xmm1
	mulsd	%xmm3, %xmm7
	mulsd	%xmm1, %xmm9
	addsd	%xmm10, %xmm7
	movsd	%xmm7, 32(%rsp)
	movapd	%xmm9, %xmm7
	movsd	.LC15(%rip), %xmm9
	subsd	%xmm9, %xmm7
	mulsd	%xmm12, %xmm7
	addsd	%xmm8, %xmm7
	mulsd	%xmm12, %xmm7
	movsd	24(%rsp), %xmm12
	mulsd	%xmm13, %xmm12
	addsd	%xmm15, %xmm7
	movsd	32(%rsp), %xmm15
	mulsd	%xmm14, %xmm7
	subsd	%xmm7, %xmm15
	movapd	%xmm15, %xmm7
	movapd	(%rsp), %xmm15
	addsd	%xmm12, %xmm7
	andpd	%xmm6, %xmm15
	addsd	%xmm14, %xmm7
	andpd	%xmm7, %xmm5
	orpd	%xmm15, %xmm5
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L66:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 56(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r13d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L59:
	ucomisd	%xmm10, %xmm4
	jnb	.L72
.L19:
	movapd	%xmm12, %xmm4
	mulsd	%xmm12, %xmm4
	mulsd	%xmm4, %xmm2
	movapd	%xmm4, %xmm0
	mulsd	%xmm4, %xmm1
	mulsd	%xmm12, %xmm0
	subsd	16(%rsp), %xmm2
	subsd	%xmm9, %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm2
	movsd	40(%rsp), %xmm0
	addsd	%xmm1, %xmm8
	addsd	%xmm3, %xmm2
	mulsd	%xmm12, %xmm3
	mulsd	%xmm8, %xmm4
	addsd	%xmm12, %xmm2
	addsd	%xmm4, %xmm3
	movsd	24(%rsp), %xmm4
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm15
	mulsd	%xmm4, %xmm3
	addsd	32(%rsp), %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm3
	addsd	%xmm15, %xmm3
	addsd	%xmm4, %xmm3
	movapd	(%rsp), %xmm4
	andpd	%xmm10, %xmm4
	andpd	%xmm5, %xmm3
	orpd	%xmm4, %xmm3
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L57:
	pxor	%xmm4, %xmm4
	pxor	%xmm14, %xmm14
	ucomisd	%xmm0, %xmm4
	jnb	.L73
.L12:
	movapd	%xmm10, %xmm11
	movsd	.LC12(%rip), %xmm2
	movsd	.LC13(%rip), %xmm6
	mulsd	%xmm10, %xmm11
	movsd	%xmm6, 16(%rsp)
	movsd	.LC15(%rip), %xmm9
	movsd	.LC16(%rip), %xmm8
	movapd	%xmm11, %xmm15
	movapd	%xmm11, %xmm1
	mulsd	%xmm2, %xmm15
	mulsd	%xmm10, %xmm1
	subsd	%xmm6, %xmm15
	movapd	%xmm11, %xmm6
	mulsd	%xmm1, %xmm15
	addsd	%xmm14, %xmm15
	mulsd	%xmm10, %xmm14
	addsd	%xmm10, %xmm15
	movapd	%xmm15, %xmm1
	mulsd	%xmm12, %xmm15
	mulsd	%xmm3, %xmm1
	addsd	%xmm7, %xmm1
	movsd	%xmm1, (%rsp)
	movsd	.LC14(%rip), %xmm1
	mulsd	%xmm1, %xmm6
	subsd	%xmm9, %xmm6
	mulsd	%xmm11, %xmm6
	addsd	%xmm8, %xmm6
	mulsd	%xmm11, %xmm6
	addsd	%xmm14, %xmm6
	movsd	(%rsp), %xmm14
	mulsd	%xmm13, %xmm6
	subsd	%xmm6, %xmm14
	movapd	%xmm0, %xmm6
	andpd	.LC17(%rip), %xmm6
	addsd	%xmm14, %xmm15
	addsd	%xmm13, %xmm15
	andpd	%xmm15, %xmm5
	orpd	%xmm6, %xmm5
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L73:
	movsd	.LC1(%rip), %xmm14
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L72:
	xorpd	(%rsp), %xmm3
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L71:
	movq	.LC17(%rip), %xmm7
	movaps	%xmm7, (%rsp)
	xorpd	%xmm0, %xmm7
	movapd	%xmm7, %xmm15
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L70:
	movq	.LC17(%rip), %xmm6
	movaps	%xmm6, (%rsp)
	xorpd	%xmm3, %xmm6
	movapd	%xmm6, %xmm7
	jmp	.L14
	.size	__sincos, .-__sincos
	.weak	sincosf32x
	.set	sincosf32x,__sincos
	.weak	sincosf64
	.set	sincosf64,__sincos
	.weak	sincos
	.set	sincos,__sincos
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-2147483648
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	1048576
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	0
	.long	1120403456
	.align 8
.LC6:
	.long	2611340116
	.long	1069555908
	.align 8
.LC7:
	.long	3271352153
	.long	-1101341185
	.align 8
.LC8:
	.long	2073722585
	.long	1053236706
	.align 8
.LC9:
	.long	433785016
	.long	1059717536
	.align 8
.LC10:
	.long	286330574
	.long	1065423121
	.align 8
.LC11:
	.long	1431655765
	.long	1069897045
	.align 8
.LC12:
	.long	3895035695
	.long	1065423120
	.align 8
.LC13:
	.long	1431655701
	.long	1069897045
	.align 8
.LC14:
	.long	3990479417
	.long	1062650219
	.align 8
.LC15:
	.long	1431655733
	.long	1067799893
	.align 8
.LC16:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC17:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC18:
	.long	1413754136
	.long	1073291771
	.align 8
.LC19:
	.long	856972295
	.long	1016178214
	.align 8
.LC20:
	.long	1841940611
	.long	1071931184
	.align 8
.LC21:
	.long	0
	.long	1127743488
	.align 8
.LC22:
	.long	1476395008
	.long	1073291771
	.align 8
.LC23:
	.long	1006632960
	.long	-1102193001
	.align 8
.LC24:
	.long	2550136832
	.long	-1131629645
	.align 8
.LC25:
	.long	602091223
	.long	-1160940417
	.align 8
.LC26:
	.long	4294967295
	.long	2146435071
