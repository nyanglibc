	.text
	.globl	__multf3
	.globl	__addtf3
	.globl	__divtf3
	.globl	__letf2
	.p2align 4,,15
	.globl	__ieee754_coshf128
	.type	__ieee754_coshf128, @function
__ieee754_coshf128:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	movdqa	(%rsp), %xmm3
	shrq	$32, %rax
	movl	%edx, %edx
	andl	$2147483647, %eax
	movq	%rax, %rcx
	salq	$32, %rcx
	orq	%rcx, %rdx
	cmpl	$2147418111, %eax
	movaps	%xmm3, 16(%rsp)
	movq	%rdx, 24(%rsp)
	jg	.L14
	cmpl	$1073570531, %eax
	jg	.L4
	cmpl	$1069023231, %eax
	jg	.L15
	movdqa	.LC0(%rip), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1074020351, %eax
	jle	.L16
	cmpl	$1074553571, %eax
	jle	.L17
	movdqa	.LC2(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L18
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movdqa	16(%rsp), %xmm0
	call	__expm1f128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	.LC0(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC0(%rip), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm0
	movdqa	%xmm2, %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	.LC1(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	call	__ieee754_expf128@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	jmp	.L1
	.size	__ieee754_coshf128, .-__ieee754_coshf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC2:
	.long	3058541734
	.long	2178113829
	.long	3145753437
	.long	1074553577
	.align 16
.LC3:
	.long	621136735
	.long	1233211794
	.long	139375215
	.long	2140429604
