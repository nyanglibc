	.text
	.p2align 4,,15
	.globl	__atanhf
	.type	__atanhf, @function
__atanhf:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	jnb	.L7
.L2:
	jmp	__ieee754_atanhf@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__atanhf, .-__atanhf
	.weak	atanhf32
	.set	atanhf32,__atanhf
	.weak	atanhf
	.set	atanhf,__atanhf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
