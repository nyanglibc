	.text
	.p2align 4,,15
	.globl	__ieee754_atanhf
	.type	__ieee754_atanhf, @function
__ieee754_atanhf:
	subq	$40, %rsp
	movaps	%xmm0, %xmm1
	movss	.LC0(%rip), %xmm3
	movss	.LC1(%rip), %xmm7
	movaps	%xmm0, %xmm2
	andps	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm7
	jbe	.L2
	movss	.LC2(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	ja	.L17
	movaps	%xmm1, %xmm5
	movaps	%xmm1, %xmm0
	movss	.LC5(%rip), %xmm4
	addss	%xmm1, %xmm5
	movss	%xmm2, (%rsp)
	subss	%xmm1, %xmm4
	movaps	%xmm3, 16(%rsp)
	mulss	%xmm5, %xmm0
	divss	%xmm4, %xmm0
	addss	%xmm5, %xmm0
	call	__log1pf@PLT
	movss	(%rsp), %xmm2
	mulss	.LC1(%rip), %xmm0
	movaps	16(%rsp), %xmm3
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L2:
	movss	.LC5(%rip), %xmm4
	ucomiss	%xmm1, %xmm4
	movaps	%xmm3, (%rsp)
	jbe	.L8
	movss	%xmm0, 16(%rsp)
	movaps	%xmm1, %xmm0
	subss	%xmm1, %xmm4
	addss	%xmm1, %xmm0
	divss	%xmm4, %xmm0
	call	__log1pf@PLT
	mulss	.LC1(%rip), %xmm0
	movss	16(%rsp), %xmm2
	movaps	(%rsp), %xmm3
.L7:
	movaps	%xmm2, %xmm6
	andps	%xmm3, %xmm0
	andps	.LC7(%rip), %xmm6
	orps	%xmm6, %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movss	.LC3(%rip), %xmm0
	addss	%xmm2, %xmm0
	movss	.LC4(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	movaps	%xmm2, %xmm0
	jbe	.L1
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	ucomiss	%xmm4, %xmm1
	jbe	.L9
	subss	%xmm0, %xmm2
	divss	%xmm2, %xmm2
	movaps	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	divss	.LC6(%rip), %xmm2
	movaps	%xmm2, %xmm0
	jmp	.L1
	.size	__ieee754_atanhf, .-__ieee754_atanhf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1056964608
	.align 4
.LC2:
	.long	830472192
	.align 4
.LC3:
	.long	1900671690
	.align 4
.LC4:
	.long	8388608
	.align 4
.LC5:
	.long	1065353216
	.align 4
.LC6:
	.long	0
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	2147483648
	.long	0
	.long	0
	.long	0
