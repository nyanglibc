	.text
	.p2align 4,,15
	.globl	__GI___finitef128
	.hidden	__GI___finitef128
	.type	__GI___finitef128, @function
__GI___finitef128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rax
	movabsq	$9223090561878065152, %rdx
	andq	%rdx, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__GI___finitef128, .-__GI___finitef128
	.globl	__finitef128
	.set	__finitef128,__GI___finitef128
	.weak	finitef128_do_not_use
	.set	finitef128_do_not_use,__finitef128
