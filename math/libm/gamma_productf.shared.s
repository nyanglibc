	.text
	.p2align 4,,15
	.globl	__gamma_productf
	.type	__gamma_productf, @function
__gamma_productf:
	cvtss2sd	%xmm0, %xmm0
	cmpl	$1, %edi
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm3
	jle	.L2
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	pxor	%xmm2, %xmm2
	cvtsi2sd	%eax, %xmm2
	addl	$1, %eax
	cmpl	%eax, %edi
	addsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm3
	jne	.L3
.L2:
	pxor	%xmm0, %xmm0
	pxor	%xmm1, %xmm1
	pxor	%xmm4, %xmm4
	cvtsd2ss	%xmm3, %xmm0
	cvtss2sd	%xmm0, %xmm1
	subsd	%xmm1, %xmm3
	divsd	%xmm1, %xmm3
	cvtsd2ss	%xmm3, %xmm4
	movss	%xmm4, (%rsi)
	ret
	.size	__gamma_productf, .-__gamma_productf
