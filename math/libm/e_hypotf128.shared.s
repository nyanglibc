	.text
#APP
	.symver __ieee754_hypotf128,__hypotf128_finite@GLIBC_2.26
	.globl	__addtf3
	.globl	__multf3
	.globl	__subtf3
	.globl	__lttf2
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_hypotf128
	.type	__ieee754_hypotf128, @function
__ieee754_hypotf128:
	pushq	%r12
	pushq	%rbp
	movabsq	$9223372036854775807, %rax
	pushq	%rbx
	subq	$96, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbx
	movaps	%xmm1, 16(%rsp)
	andq	%rax, %rbx
	movq	24(%rsp), %rbp
	andq	%rax, %rbp
	cmpq	%rbp, %rbx
	jge	.L2
	movaps	%xmm1, (%rsp)
	movq	%rbx, %rax
	movq	%rbp, %rbx
	movaps	%xmm0, 16(%rsp)
	movq	%rax, %rbp
.L2:
	movq	%rbx, %rdx
	movabsq	$33776997205278720, %rax
	subq	%rbp, %rdx
	movq	%rbx, 8(%rsp)
	movq	%rbp, 24(%rsp)
	cmpq	%rax, %rdx
	movdqa	(%rsp), %xmm4
	movdqa	16(%rsp), %xmm6
	movaps	%xmm4, 48(%rsp)
	movaps	%xmm6, 32(%rsp)
	jg	.L25
	movabsq	$6863204357135925248, %rax
	cmpq	%rax, %rbx
	jg	.L26
	xorl	%r12d, %r12d
.L5:
	movabsq	$2359604729765429247, %rax
	cmpq	%rax, %rbp
	jg	.L8
	movabsq	$281474976710655, %rax
	cmpq	%rax, %rbp
	jg	.L9
	movq	32(%rsp), %rax
	movdqa	48(%rsp), %xmm2
	orq	%rax, %rbp
	je	.L1
	movdqa	.LC2(%rip), %xmm2
	subq	$16382, %r12
	movdqa	%xmm2, %xmm1
	movdqa	32(%rsp), %xmm0
	movaps	%xmm2, (%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm2
	movdqa	48(%rsp), %xmm0
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movq	40(%rsp), %rbp
	movaps	%xmm0, 48(%rsp)
	movq	56(%rsp), %rbx
	cmpq	%rbp, %rbx
	movq	%rbx, %rax
	jge	.L8
	movdqa	32(%rsp), %xmm6
	movq	%rbp, %rbx
	movq	%rax, %rbp
	movaps	%xmm6, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	32(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L27
	movabsq	$281474976710656, %rax
	movq	$0, 16(%rsp)
	movq	%rbp, 24(%rsp)
	addq	%rbx, %rax
	movq	$0, (%rsp)
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	movdqa	.LC0(%rip), %xmm7
	pxor	%xmm1, %xmm7
	movaps	%xmm0, 80(%rsp)
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm2
.L12:
	testq	%r12, %r12
	je	.L1
	xorl	%eax, %eax
	salq	$48, %r12
	movq	%rax, (%rsp)
	movabsq	$4611404543450677248, %rax
	addq	%r12, %rax
	movdqa	%xmm2, %xmm0
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jns	.L1
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	addq	$96, %rsp
	popq	%rbx
	movdqa	%xmm2, %xmm0
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movabsq	$9223090561878065151, %rax
	cmpq	%rax, %rbx
	jle	.L6
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	movdqa	64(%rsp), %xmm2
	jne	.L1
	movdqa	16(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	movdqa	64(%rsp), %xmm2
	jne	.L1
	movabsq	$281474976710655, %rax
	andq	%rax, %rbx
	movq	48(%rsp), %rax
	orq	%rax, %rbx
	jne	.L7
	movdqa	(%rsp), %xmm2
.L7:
	movabsq	$9223090561878065152, %rax
	xorq	%rax, %rbp
	movq	32(%rsp), %rax
	orq	%rax, %rbp
	jne	.L1
	movdqa	32(%rsp), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	%xmm6, %xmm1
	movdqa	%xmm4, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$96, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$0, (%rsp)
	movq	%rbx, 8(%rsp)
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm7
	pxor	.LC0(%rip), %xmm7
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L6:
	movdqa	48(%rsp), %xmm3
	movabsq	$-2702159776422297600, %rax
	addq	%rax, %rbx
	addq	%rax, %rbp
	movl	$9600, %r12d
	movaps	%xmm3, (%rsp)
	movq	%rbx, 8(%rsp)
	movdqa	32(%rsp), %xmm5
	movdqa	(%rsp), %xmm4
	movaps	%xmm5, (%rsp)
	movq	%rbp, 8(%rsp)
	movaps	%xmm4, 48(%rsp)
	movdqa	(%rsp), %xmm6
	movaps	%xmm6, 32(%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movdqa	48(%rsp), %xmm7
	movabsq	$2702159776422297600, %rax
	addq	%rax, %rbx
	addq	%rax, %rbp
	subq	$9600, %r12
	movaps	%xmm7, (%rsp)
	movq	%rbx, 8(%rsp)
	movdqa	32(%rsp), %xmm4
	movdqa	(%rsp), %xmm3
	movaps	%xmm4, (%rsp)
	movq	%rbp, 8(%rsp)
	movaps	%xmm3, 48(%rsp)
	movdqa	(%rsp), %xmm5
	movaps	%xmm5, 32(%rsp)
	jmp	.L8
	.size	__ieee754_hypotf128, .-__ieee754_hypotf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.quad	0
	.quad	9222527611924643840
