	.text
	.p2align 4,,15
	.globl	__ieee754_jn
	.type	__ieee754_jn, @function
__ieee754_jn:
	movq	%xmm0, %rsi
	movd	%xmm0, %eax
	movq	%xmm0, %rdx
	shrq	$32, %rsi
	negl	%eax
	orl	%edx, %eax
	movl	%esi, %ecx
	andl	$2147483647, %ecx
	shrl	$31, %eax
	orl	%ecx, %eax
	cmpl	$2146435072, %eax
	ja	.L78
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$112, %rsp
	testl	%edi, %edi
	js	.L79
	movl	%esi, %ebp
	je	.L80
	cmpl	$1, %ebx
	je	.L81
.L6:
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 96(%rsp)
# 0 "" 2
#NO_APP
	movl	96(%rsp), %r13d
	shrl	$31, %ebp
	xorl	%r14d, %r14d
	movq	.LC4(%rip), %xmm2
	andl	%ebx, %ebp
	movapd	%xmm0, %xmm3
	movl	%r13d, %eax
	andb	$-97, %ah
	andpd	%xmm2, %xmm3
	cmpl	%eax, %r13d
	movl	%eax, 104(%rsp)
	jne	.L82
.L7:
	movl	%edx, %eax
	orl	%ecx, %eax
	je	.L50
	cmpl	$2146435071, %ecx
	jg	.L50
	pxor	%xmm9, %xmm9
	cvtsi2sd	%ebx, %xmm9
	ucomisd	%xmm9, %xmm3
	jb	.L69
	cmpl	$1389363199, %ecx
	jle	.L13
	movapd	%xmm3, %xmm0
	leaq	104(%rsp), %rsi
	leaq	96(%rsp), %rdi
	andl	$3, %ebx
	movsd	%xmm3, (%rsp)
	movaps	%xmm2, 16(%rsp)
	call	__sincos@PLT
	cmpl	$2, %ebx
	movsd	(%rsp), %xmm3
	movapd	16(%rsp), %xmm2
	je	.L15
	cmpl	$3, %ebx
	je	.L16
	cmpl	$1, %ebx
	je	.L17
	movsd	104(%rsp), %xmm0
	addsd	96(%rsp), %xmm0
.L18:
	mulsd	.LC5(%rip), %xmm0
	sqrtsd	%xmm3, %xmm3
	pxor	%xmm8, %xmm8
	divsd	%xmm3, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L80:
	call	__ieee754_j0@PLT
.L1:
	addq	$112, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	negl	%ebx
	xorpd	.LC3(%rip), %xmm0
	cmpl	$1, %ebx
	leal	-2147483648(%rsi), %ebp
	jne	.L6
.L81:
	call	__ieee754_j1@PLT
	addq	$112, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$1041235967, %ecx
	jg	.L21
	cmpl	$33, %ebx
	jg	.L45
	mulsd	.LC6(%rip), %xmm3
	movl	$2, %eax
	movsd	.LC2(%rip), %xmm1
	movapd	%xmm3, %xmm0
	.p2align 4,,10
	.p2align 3
.L22:
	pxor	%xmm4, %xmm4
	mulsd	%xmm3, %xmm0
	cvtsi2sd	%eax, %xmm4
	addl	$1, %eax
	cmpl	%ebx, %eax
	mulsd	%xmm4, %xmm1
	jle	.L22
	divsd	%xmm1, %xmm0
.L76:
	pxor	%xmm8, %xmm8
.L19:
	cmpl	$1, %ebp
	jne	.L37
	xorpd	.LC3(%rip), %xmm0
.L37:
	testb	%r14b, %r14b
	jne	.L83
.L38:
	ucomisd	%xmm8, %xmm0
	jp	.L39
	je	.L84
.L39:
	andpd	%xmm0, %xmm2
	movsd	.LC12(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	jbe	.L1
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	addq	$112, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	pxor	%xmm8, %xmm8
	leal	(%rbx,%rbx), %r12d
	movsd	.LC7(%rip), %xmm6
	movl	$1, %eax
	movsd	.LC2(%rip), %xmm5
	cvtsi2sd	%r12d, %xmm8
	movapd	%xmm6, %xmm0
	movsd	.LC8(%rip), %xmm10
	divsd	%xmm3, %xmm0
	divsd	%xmm3, %xmm8
	movapd	%xmm8, %xmm7
	movapd	%xmm8, %xmm1
	addsd	%xmm0, %xmm7
	mulsd	%xmm7, %xmm1
	subsd	%xmm5, %xmm1
	ucomisd	%xmm1, %xmm10
	jbe	.L23
	.p2align 4,,10
	.p2align 3
.L25:
	addsd	%xmm0, %xmm7
	addl	$1, %eax
	movapd	%xmm7, %xmm4
	mulsd	%xmm1, %xmm4
	subsd	%xmm8, %xmm4
	movapd	%xmm1, %xmm8
	ucomisd	%xmm4, %xmm10
	movapd	%xmm4, %xmm1
	ja	.L25
.L23:
	leal	(%rax,%rbx), %edx
	pxor	%xmm8, %xmm8
	pxor	%xmm1, %xmm1
	cmpl	%ebx, %edx
	leal	(%rdx,%rdx), %eax
	jl	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	pxor	%xmm4, %xmm4
	movapd	%xmm5, %xmm7
	cvtsi2sd	%eax, %xmm4
	subl	$2, %eax
	cmpl	%eax, %r12d
	divsd	%xmm3, %xmm4
	subsd	%xmm1, %xmm4
	divsd	%xmm4, %xmm7
	movapd	%xmm7, %xmm1
	jle	.L27
.L26:
	mulsd	%xmm9, %xmm0
	movsd	%xmm6, 88(%rsp)
	subl	$2, %r12d
	movsd	%xmm5, 64(%rsp)
	subl	$1, %ebx
	movsd	%xmm8, 80(%rsp)
	movsd	%xmm3, 56(%rsp)
	andpd	%xmm2, %xmm0
	movsd	%xmm1, 32(%rsp)
	movsd	%xmm9, 16(%rsp)
	movaps	%xmm2, (%rsp)
	call	__ieee754_log@PLT
	movsd	16(%rsp), %xmm9
	movapd	(%rsp), %xmm2
	mulsd	%xmm0, %xmm9
	movsd	.LC9(%rip), %xmm0
	movsd	32(%rsp), %xmm1
	movsd	56(%rsp), %xmm3
	ucomisd	%xmm9, %xmm0
	pxor	%xmm0, %xmm0
	movsd	80(%rsp), %xmm8
	movsd	64(%rsp), %xmm5
	movsd	88(%rsp), %xmm6
	cvtsi2sd	%r12d, %xmm0
	jbe	.L70
	movapd	%xmm1, %xmm7
	movapd	%xmm5, %xmm9
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L48:
	movapd	%xmm4, %xmm9
.L30:
	movapd	%xmm9, %xmm4
	subl	$1, %ebx
	mulsd	%xmm0, %xmm4
	subsd	%xmm6, %xmm0
	divsd	%xmm3, %xmm4
	subsd	%xmm7, %xmm4
	movapd	%xmm9, %xmm7
	jne	.L48
.L31:
	movapd	%xmm3, %xmm0
	movsd	%xmm8, 80(%rsp)
	movaps	%xmm2, 64(%rsp)
	movsd	%xmm1, 56(%rsp)
	movsd	%xmm9, 32(%rsp)
	movsd	%xmm4, 88(%rsp)
	movsd	%xmm3, 16(%rsp)
	call	__ieee754_j0@PLT
	movsd	16(%rsp), %xmm3
	movsd	%xmm0, (%rsp)
	movapd	%xmm3, %xmm0
	call	__ieee754_j1@PLT
	movsd	(%rsp), %xmm5
	movapd	64(%rsp), %xmm2
	movapd	%xmm0, %xmm3
	movapd	%xmm5, %xmm6
	andpd	%xmm2, %xmm3
	andpd	%xmm2, %xmm6
	movsd	32(%rsp), %xmm9
	movsd	56(%rsp), %xmm1
	ucomisd	%xmm3, %xmm6
	movsd	80(%rsp), %xmm8
	jb	.L72
	mulsd	%xmm5, %xmm1
	movsd	88(%rsp), %xmm4
	movapd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L13:
	movapd	%xmm3, %xmm0
	movsd	%xmm3, (%rsp)
	movaps	%xmm2, 32(%rsp)
	call	__ieee754_j0@PLT
	movsd	%xmm0, 16(%rsp)
	movsd	(%rsp), %xmm3
	movapd	%xmm3, %xmm0
	call	__ieee754_j1@PLT
	movl	$1, %edx
	movapd	%xmm0, %xmm1
	movsd	(%rsp), %xmm3
	movsd	16(%rsp), %xmm4
	movapd	32(%rsp), %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L44:
	movapd	%xmm0, %xmm1
.L20:
	pxor	%xmm0, %xmm0
	leal	(%rdx,%rdx), %eax
	addl	$1, %edx
	cmpl	%ebx, %edx
	cvtsi2sd	%eax, %xmm0
	divsd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm4, %xmm0
	movapd	%xmm1, %xmm4
	jl	.L44
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L82:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 104(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r14d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L50:
	cmpl	$1, %ebp
	pxor	%xmm0, %xmm0
	jne	.L10
	movsd	.LC1(%rip), %xmm0
.L10:
	testb	%r14b, %r14b
	je	.L1
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 104(%rsp)
# 0 "" 2
#NO_APP
	movl	104(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 104(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 104(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L84:
	andpd	.LC3(%rip), %xmm0
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	orpd	.LC11(%rip), %xmm0
	mulsd	.LC12(%rip), %xmm0
	addq	$112, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movapd	%xmm1, %xmm9
	movapd	%xmm5, %xmm4
	movsd	.LC10(%rip), %xmm10
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L85:
	divsd	%xmm7, %xmm4
	divsd	%xmm7, %xmm1
	movapd	%xmm4, %xmm9
	movapd	%xmm5, %xmm4
.L32:
	subl	$1, %ebx
	je	.L31
.L34:
	movapd	%xmm4, %xmm7
	mulsd	%xmm0, %xmm7
	subsd	%xmm6, %xmm0
	divsd	%xmm3, %xmm7
	subsd	%xmm9, %xmm7
	ucomisd	%xmm10, %xmm7
	ja	.L85
	movapd	%xmm4, %xmm9
	movapd	%xmm7, %xmm4
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L45:
	pxor	%xmm8, %xmm8
	pxor	%xmm0, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L72:
	mulsd	%xmm1, %xmm0
	divsd	%xmm9, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	96(%rsp), %xmm0
	subsd	104(%rsp), %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L83:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 104(%rsp)
# 0 "" 2
#NO_APP
	movl	104(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 104(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 104(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L16:
	movsd	104(%rsp), %xmm0
	subsd	96(%rsp), %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L15:
	movsd	104(%rsp), %xmm0
	xorpd	.LC3(%rip), %xmm0
	subsd	96(%rsp), %xmm0
	jmp	.L18
	.size	__ieee754_jn, .-__ieee754_jn
	.p2align 4,,15
	.globl	__ieee754_yn
	.type	__ieee754_yn, @function
__ieee754_yn:
	movq	%xmm0, %rsi
	movd	%xmm0, %eax
	movq	%xmm0, %rdx
	movapd	%xmm0, %xmm1
	shrq	$32, %rsi
	negl	%eax
	orl	%edx, %eax
	movl	%esi, %ecx
	andl	$2147483647, %ecx
	shrl	$31, %eax
	orl	%ecx, %eax
	cmpl	$2146435072, %eax
	ja	.L140
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$40, %rsp
	testl	%edi, %edi
	js	.L141
	movl	$1, %ebp
	je	.L142
.L90:
	movl	%edx, %eax
	orl	%ecx, %eax
	je	.L143
	testl	%esi, %esi
	js	.L144
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 16(%rsp)
# 0 "" 2
#NO_APP
	movl	16(%rsp), %r12d
	movl	%r12d, %r13d
	andl	$-24577, %r13d
	cmpl	%r13d, %r12d
	movl	%r13d, 24(%rsp)
	jne	.L145
	cmpl	$1, %ebx
	je	.L146
	cmpl	$2146435072, %ecx
	je	.L139
	xorl	%r13d, %r13d
.L112:
	cmpl	$1389363199, %ecx
	jg	.L147
	movapd	%xmm1, %xmm0
	movsd	%xmm1, (%rsp)
	call	__ieee754_y0@PLT
	movsd	(%rsp), %xmm1
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm1, %xmm0
	call	__ieee754_y1@PLT
	cmpl	$1, %ebx
	movapd	%xmm0, %xmm2
	jle	.L114
	movq	%xmm0, %rax
	shrq	$32, %rax
	cmpl	$-1048576, %eax
	je	.L114
	movl	$1, %edx
	movsd	(%rsp), %xmm1
	movsd	8(%rsp), %xmm3
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L148:
	movq	%xmm0, %rcx
	shrq	$32, %rcx
	cmpl	$-1048576, %ecx
	je	.L103
	movapd	%xmm0, %xmm2
.L104:
	pxor	%xmm0, %xmm0
	leal	(%rdx,%rdx), %eax
	addl	$1, %edx
	cmpl	%ebx, %edx
	cvtsi2sd	%eax, %xmm0
	divsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm3
	jl	.L148
.L103:
	movq	.LC4(%rip), %xmm1
	movapd	%xmm0, %xmm3
	movsd	.LC13(%rip), %xmm2
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm2
	jnb	.L102
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L102:
	cmpl	$1, %ebp
	je	.L107
	xorpd	.LC3(%rip), %xmm0
.L107:
	testb	%r13b, %r13b
	jne	.L149
.L108:
	andpd	%xmm0, %xmm1
	ucomisd	%xmm2, %xmm1
	jbe	.L86
	andpd	.LC3(%rip), %xmm0
	orpd	.LC14(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	mulsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	call	__ieee754_y0@PLT
.L86:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	negl	%ebx
	movl	$1, %eax
	leal	(%rbx,%rbx), %edi
	andl	$2, %edi
	subl	%edi, %eax
	movl	%eax, %ebp
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L147:
	movapd	%xmm1, %xmm0
	leaq	16(%rsp), %rsi
	leaq	24(%rsp), %rdi
	andl	$3, %ebx
	movsd	%xmm1, (%rsp)
	call	__sincos@PLT
	cmpl	$2, %ebx
	movsd	(%rsp), %xmm1
	je	.L98
	cmpl	$3, %ebx
	movsd	24(%rsp), %xmm0
	je	.L99
	cmpl	$1, %ebx
	jne	.L138
	xorpd	.LC3(%rip), %xmm0
.L138:
	subsd	16(%rsp), %xmm0
.L101:
	mulsd	.LC5(%rip), %xmm0
	sqrtsd	%xmm1, %xmm1
	movsd	.LC13(%rip), %xmm2
	divsd	%xmm1, %xmm0
	movq	.LC4(%rip), %xmm1
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L145:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	cmpl	$1, %ebx
	je	.L150
	cmpl	$2146435072, %ecx
	je	.L111
	movl	$1, %r13d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L140:
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L146:
	movapd	%xmm1, %xmm0
	call	__ieee754_y1@PLT
	pxor	%xmm1, %xmm1
	movsd	.LC13(%rip), %xmm2
	cvtsi2sd	%ebp, %xmm1
	mulsd	%xmm1, %xmm0
	movq	.LC4(%rip), %xmm1
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L143:
	pxor	%xmm0, %xmm0
	movl	%ebp, %eax
	negl	%eax
	cvtsi2sd	%eax, %xmm0
	divsd	.LC0(%rip), %xmm0
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L144:
	pxor	%xmm0, %xmm0
	mulsd	%xmm0, %xmm1
	divsd	%xmm1, %xmm0
	jmp	.L86
.L150:
	movapd	%xmm1, %xmm0
	andl	$24576, %r12d
	orl	%r12d, %r13d
	call	__ieee754_y1@PLT
	pxor	%xmm1, %xmm1
	movl	%r13d, 24(%rsp)
	cvtsi2sd	%ebp, %xmm1
	mulsd	%xmm1, %xmm0
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	movq	.LC4(%rip), %xmm1
	movsd	.LC13(%rip), %xmm2
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L98:
	movsd	16(%rsp), %xmm0
	subsd	24(%rsp), %xmm0
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L99:
	addsd	16(%rsp), %xmm0
	jmp	.L101
.L111:
	andl	$24576, %r12d
	orl	%r12d, %r13d
	movl	%r13d, 24(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 24(%rsp)
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L139:
	pxor	%xmm0, %xmm0
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L149:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	movl	24(%rsp), %eax
	andl	$24576, %r12d
	andb	$-97, %ah
	orl	%eax, %r12d
	movl	%r12d, 24(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L114:
	movapd	%xmm2, %xmm0
	jmp	.L103
	.size	__ieee754_yn, .-__ieee754_yn
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	-2147483648
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	1346542445
	.long	1071779287
	.align 8
.LC6:
	.long	0
	.long	1071644672
	.align 8
.LC7:
	.long	0
	.long	1073741824
	.align 8
.LC8:
	.long	0
	.long	1104006501
	.align 8
.LC9:
	.long	4277811695
	.long	1082535490
	.align 8
.LC10:
	.long	630506365
	.long	1420970413
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	0
	.long	1048576
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC12:
	.long	0
	.long	1048576
	.align 8
.LC13:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC14:
	.long	4294967295
	.long	2146435071
	.long	0
	.long	0
