	.text
	.p2align 4,,15
	.globl	__isnanf
	.type	__isnanf, @function
__isnanf:
	movd	%xmm0, %edx
	movl	$2139095040, %eax
	andl	$2147483647, %edx
	subl	%edx, %eax
	shrl	$31, %eax
	ret
	.size	__isnanf, .-__isnanf
	.weak	isnanf
	.set	isnanf,__isnanf
