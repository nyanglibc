	.text
#APP
	.symver __ieee754_remainder,__remainder_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_remainder
	.type	__ieee754_remainder, @function
__ieee754_remainder:
	movq	%xmm1, %rdx
	movq	%xmm0, %rcx
	pushq	%r13
	movq	%xmm1, %rdi
	pushq	%r12
	pushq	%rbp
	sarq	$32, %rdx
	sarq	$32, %rcx
	pushq	%rbx
	andl	$2147483647, %edx
	movl	%ecx, %eax
	movl	%edi, %esi
	movq	%rdx, %r8
	andl	$2147483647, %eax
	subq	$104, %rsp
	salq	$32, %r8
	movapd	%xmm0, %xmm3
	orq	%r8, %rsi
	cmpl	$2145386495, %eax
	jg	.L2
	cmpl	$2146435071, %edx
	jg	.L2
	cmpl	$55574527, %edx
	jg	.L80
	testl	%edx, %edx
	jne	.L27
	testl	%edi, %edi
	jne	.L27
.L37:
	mulsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	divsd	%xmm3, %xmm0
.L1:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L80:
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 40(%rsp)
# 0 "" 2
#NO_APP
	movl	40(%rsp), %ecx
	movl	%ecx, %edi
	movl	%ecx, 76(%rsp)
	andl	$-24577, %edi
	cmpl	%edi, %ecx
	movl	%edi, 44(%rsp)
	jne	.L81
.L5:
	leal	1048576(%rax), %ecx
	movapd	%xmm3, %xmm0
	cmpl	%ecx, %edx
	jg	.L6
	leal	-22020095(%rax), %ecx
	movq	%rsi, 16(%rsp)
	sarq	$32, %rsi
	movsd	16(%rsp), %xmm4
	movl	%esi, %r10d
	cmpl	%ecx, %edx
	movsd	big(%rip), %xmm6
	jl	.L7
	movapd	%xmm3, %xmm7
	salq	$32, %rsi
	movl	$1, %edx
	movq	%rsi, 16(%rsp)
	divsd	%xmm4, %xmm7
	movq	16(%rsp), %xmm2
	movapd	%xmm2, %xmm1
	movapd	%xmm7, %xmm5
	addsd	%xmm6, %xmm5
	subsd	%xmm6, %xmm5
	movapd	%xmm4, %xmm6
	subsd	%xmm2, %xmm6
	mulsd	%xmm5, %xmm1
	movapd	%xmm6, %xmm2
	subsd	%xmm1, %xmm0
	movapd	%xmm5, %xmm1
	mulsd	%xmm5, %xmm2
	subsd	%xmm7, %xmm1
	subsd	%xmm2, %xmm0
	movsd	.LC1(%rip), %xmm2
	ucomisd	.LC0(%rip), %xmm1
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L8
	ucomisd	.LC1(%rip), %xmm1
	movsd	.LC1(%rip), %xmm2
	setp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	jne	.L78
.L8:
	movapd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm2
	andpd	.LC3(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	jbe	.L6
	ucomisd	%xmm5, %xmm7
	jbe	.L72
.L69:
	subsd	%xmm4, %xmm0
.L6:
#APP
# 122 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	andl	$2146435072, %ecx
	cmpl	$2145386496, %ecx
	je	.L82
	testl	%edx, %edx
	je	.L83
.L30:
	cmpl	$2146435071, %eax
	jg	.L37
	cmpl	$2146435072, %edx
	jg	.L37
	cmpl	$2146435072, %edx
	movapd	%xmm3, %xmm0
	jne	.L1
	movq	%xmm1, %rax
	testl	%eax, %eax
	jne	.L37
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%xmm1, %rdx
	testl	%edx, %edx
	je	.L37
	cmpl	$2146435071, %eax
	jg	.L37
	movapd	%xmm3, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movapd	%xmm1, %xmm2
	movapd	%xmm3, %xmm0
	andpd	.LC3(%rip), %xmm2
	mulsd	t128(%rip), %xmm2
	movapd	%xmm2, %xmm1
	movsd	%xmm2, 16(%rsp)
	call	__ieee754_remainder@PLT
	mulsd	t128(%rip), %xmm0
	movsd	16(%rsp), %xmm2
	movapd	%xmm2, %xmm1
	call	__ieee754_remainder@PLT
	mulsd	tm128(%rip), %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	cmpl	$2146435071, %edx
	jg	.L30
	testl	%edx, %edx
	je	.L84
.L31:
	movapd	%xmm3, %xmm0
	movq	.LC3(%rip), %xmm4
	movapd	%xmm1, %xmm2
	movsd	%xmm3, 24(%rsp)
	mulsd	.LC1(%rip), %xmm0
	andpd	%xmm4, %xmm2
	movaps	%xmm4, (%rsp)
	movapd	%xmm2, %xmm1
	movsd	%xmm2, 16(%rsp)
	call	__ieee754_remainder@PLT
	addsd	%xmm0, %xmm0
	movapd	(%rsp), %xmm4
	movsd	16(%rsp), %xmm2
	movapd	%xmm0, %xmm5
	andpd	%xmm4, %xmm5
	movapd	%xmm5, %xmm1
	subsd	%xmm2, %xmm1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm5, %xmm1
	jnb	.L1
	ucomisd	%xmm5, %xmm2
	jp	.L32
	je	.L85
.L32:
	ucomisd	.LC2(%rip), %xmm0
	jbe	.L70
	subsd	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movsd	.LC4(%rip), %xmm0
	movl	%esi, %r9d
	movl	%esi, %esi
	movapd	%xmm4, %xmm5
	divsd	%xmm4, %xmm0
	movq	%rsi, %rdi
	andl	$2146435072, %r9d
	salq	$32, %rdi
	addl	$20971520, %r9d
	movq	%rdi, 16(%rsp)
	subl	%r9d, %eax
	movsd	16(%rsp), %xmm7
	subsd	%xmm7, %xmm5
	movq	%xmm0, %r12
	movq	%xmm0, %rdx
	movapd	%xmm3, %xmm0
	sarq	$32, %r12
	movq	%xmm5, %rbp
	movq	%xmm5, %rcx
	movl	%r12d, %r11d
	sarq	$32, %rbp
	andl	$-1048576, %eax
	movl	%ebp, %r8d
	jle	.L15
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r11d, %r13d
	movl	%edx, %edx
	movl	%edi, %edi
	subl	%eax, %r13d
	movl	%ecx, %ecx
	salq	$32, %r13
	orq	%r13, %rdx
	leal	(%r10,%rax), %r13d
	addl	%r8d, %eax
	movq	%rdx, 16(%rsp)
	movsd	16(%rsp), %xmm2
	salq	$32, %r13
	orq	%r13, %rdi
	testl	%r8d, %r8d
	mulsd	%xmm0, %xmm2
	movq	%rdi, 16(%rsp)
	cmove	%ebx, %eax
	movsd	16(%rsp), %xmm1
	salq	$32, %rax
	orq	%rax, %rcx
	movq	%rcx, 16(%rsp)
	addsd	%xmm6, %xmm2
	movsd	16(%rsp), %xmm7
	subsd	%xmm6, %xmm2
	mulsd	%xmm2, %xmm1
	mulsd	%xmm7, %xmm2
	subsd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	movq	%xmm0, %rax
	sarq	$32, %rax
	andl	$2146435072, %eax
	subl	%r9d, %eax
	testl	%eax, %eax
	jg	.L17
.L15:
	salq	$32, %r12
	movl	%edx, %edx
	salq	$32, %rsi
	orq	%r12, %rdx
	movl	%edi, %edi
	salq	$32, %rbp
	movq	%rdx, 16(%rsp)
	orq	%rsi, %rdi
	movl	%ecx, %ecx
	movsd	16(%rsp), %xmm2
	movq	%rdi, 16(%rsp)
	movsd	16(%rsp), %xmm5
	orq	%rbp, %rcx
	mulsd	%xmm0, %xmm2
	movq	%rcx, (%rsp)
	movapd	%xmm2, %xmm1
	addsd	%xmm6, %xmm1
	subsd	%xmm6, %xmm1
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	movsd	(%rsp), %xmm5
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm0
	movsd	.LC1(%rip), %xmm5
	mulsd	%xmm4, %xmm5
	movapd	%xmm0, %xmm7
	andpd	.LC3(%rip), %xmm7
	ucomisd	%xmm7, %xmm5
	ja	.L78
	ucomisd	%xmm5, %xmm7
	jbe	.L68
	ucomisd	%xmm2, %xmm1
	jbe	.L69
.L72:
	addsd	%xmm4, %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L78:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L6
	jne	.L6
	ucomisd	%xmm1, %xmm3
	movsd	ZERO(%rip), %xmm0
	ja	.L6
	movsd	nZERO(%rip), %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L81:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L68:
	movapd	%xmm0, %xmm2
	movsd	16(%rsp), %xmm1
	divsd	%xmm4, %xmm2
	addsd	%xmm6, %xmm2
	subsd	%xmm6, %xmm2
	mulsd	%xmm2, %xmm1
	mulsd	(%rsp), %xmm2
	subsd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L85:
	movsd	24(%rsp), %xmm3
	mulsd	.LC2(%rip), %xmm3
	movapd	%xmm3, %xmm0
	jmp	.L1
.L70:
	addsd	%xmm2, %xmm0
	jmp	.L1
.L84:
	movq	%xmm1, %rax
	testl	%eax, %eax
	jne	.L31
	jmp	.L37
	.size	__ieee754_remainder, .-__ieee754_remainder
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	nZERO, @object
	.size	nZERO, 8
nZERO:
	.long	0
	.long	-2147483648
	.align 8
	.type	ZERO, @object
	.size	ZERO, 8
ZERO:
	.zero	8
	.align 8
	.type	tm128, @object
	.size	tm128, 8
tm128:
	.long	0
	.long	938475520
	.align 8
	.type	t128, @object
	.size	t128, 8
t128:
	.long	0
	.long	1206910976
	.align 8
	.type	big, @object
	.size	big, 8
big:
	.long	0
	.long	1127743488
	.align 8
.LC0:
	.long	0
	.long	-1075838976
	.align 8
.LC1:
	.long	0
	.long	1071644672
	.align 8
.LC2:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1072693248
