	.text
	.p2align 4,,15
	.globl	__ceilf
	.type	__ceilf, @function
__ceilf:
	movss	%xmm0, -4(%rsp)
	movl	-4(%rsp), %eax
	movl	%eax, %ecx
	sarl	$23, %ecx
	movzbl	%cl, %ecx
	subl	$127, %ecx
	cmpl	$22, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L14
	movl	$8388607, %edx
	sarl	%cl, %edx
	testl	%edx, %eax
	je	.L5
	testl	%eax, %eax
	jle	.L6
	movl	$8388608, %esi
	sarl	%cl, %esi
	addl	%esi, %eax
.L6:
	notl	%edx
	andl	%eax, %edx
	movl	%edx, -4(%rsp)
.L5:
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$-128, %ecx
	jne	.L5
	movss	-4(%rsp), %xmm2
	addss	%xmm2, %xmm2
	movss	%xmm2, -4(%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%eax, %eax
	js	.L7
	pxor	%xmm1, %xmm1
	movd	%xmm1, %eax
	cmovne	.LC1(%rip), %eax
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movss	.LC2(%rip), %xmm3
	movss	%xmm3, -4(%rsp)
	jmp	.L5
	.size	__ceilf, .-__ceilf
	.weak	ceilf32
	.set	ceilf32,__ceilf
	.weak	ceilf
	.set	ceilf,__ceilf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
	.align 4
.LC2:
	.long	2147483648
