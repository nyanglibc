	.text
	.p2align 4,,15
	.globl	__tgamma
	.type	__tgamma, @function
__tgamma:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	movsd	%xmm0, 8(%rsp)
	call	__ieee754_gamma_r@PLT
	movq	.LC0(%rip), %xmm2
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm3
	movsd	8(%rsp), %xmm1
	andpd	%xmm2, %xmm4
	ucomisd	%xmm4, %xmm3
	jb	.L2
	ucomisd	.LC2(%rip), %xmm0
	jp	.L3
	je	.L2
.L3:
	movl	28(%rsp), %eax
	testl	%eax, %eax
	jns	.L1
	xorpd	.LC5(%rip), %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movapd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm4, %xmm3
	jnb	.L20
	ucomisd	%xmm3, %xmm4
	jbe	.L3
	pxor	%xmm3, %xmm3
	ucomisd	%xmm1, %xmm3
	jbe	.L3
	movapd	%xmm1, %xmm3
	movsd	.LC3(%rip), %xmm5
	movapd	%xmm1, %xmm4
	andpd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm5
	jbe	.L15
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm3, %xmm3
	movsd	.LC4(%rip), %xmm5
	andnpd	%xmm1, %xmm2
	cvtsi2sdq	%rax, %xmm3
	movapd	%xmm3, %xmm4
	cmpnlesd	%xmm1, %xmm4
	andpd	%xmm5, %xmm4
	subsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm4
	orpd	%xmm2, %xmm4
.L15:
	ucomisd	%xmm4, %xmm1
	jp	.L11
	jne	.L11
.L16:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L20:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm1
	jp	.L8
	jne	.L8
.L11:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	movapd	%xmm1, %xmm4
	movsd	.LC3(%rip), %xmm6
	movapd	%xmm1, %xmm5
	andpd	%xmm2, %xmm4
	ucomisd	%xmm4, %xmm6
	jbe	.L10
	cvttsd2siq	%xmm1, %rax
	pxor	%xmm4, %xmm4
	movsd	.LC4(%rip), %xmm6
	andnpd	%xmm1, %xmm2
	cvtsi2sdq	%rax, %xmm4
	movapd	%xmm4, %xmm5
	cmpnlesd	%xmm1, %xmm5
	andpd	%xmm6, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm5
	orpd	%xmm2, %xmm5
.L10:
	ucomisd	%xmm1, %xmm5
	jp	.L11
	jne	.L11
	ucomisd	%xmm1, %xmm3
	jbe	.L11
	jmp	.L16
	.size	__tgamma, .-__tgamma
	.weak	tgammaf32x
	.set	tgammaf32x,__tgamma
	.weak	tgammaf64
	.set	tgammaf64,__tgamma
	.weak	tgamma
	.set	tgamma,__tgamma
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	0
	.align 8
.LC3:
	.long	0
	.long	1127219200
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
