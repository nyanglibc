	.text
	.p2align 4,,15
	.globl	__totalorderf
	.type	__totalorderf, @function
__totalorderf:
	movl	(%rdi), %eax
	movl	(%rsi), %ecx
	cltd
	shrl	%edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	%eax
	xorl	%ecx, %eax
	cmpl	%eax, %edx
	setle	%al
	movzbl	%al, %eax
	ret
	.size	__totalorderf, .-__totalorderf
	.weak	totalorderf32
	.set	totalorderf32,__totalorderf
	.weak	totalorderf
	.set	totalorderf,__totalorderf
