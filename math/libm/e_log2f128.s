	.text
	.globl	__divtf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__lttf2
	.globl	__multf3
	.globl	__floatsitf
	.p2align 4,,15
	.globl	__ieee754_log2f128
	.type	__ieee754_log2f128, @function
__ieee754_log2f128:
	pushq	%r12
	pushq	%rbp
	movabsq	$9223372036854775807, %rax
	pushq	%rbx
	subq	$80, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	(%rsp), %rcx
	andq	%rdx, %rax
	orq	%rcx, %rax
	je	.L25
	testq	%rdx, %rdx
	js	.L26
	movabsq	$9223090561878065151, %rax
	cmpq	%rax, %rdx
	jg	.L27
	movdqa	(%rsp), %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__eqtf2@PLT
	testq	%rax, %rax
	pxor	%xmm0, %xmm0
	je	.L1
	leaq	76(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__frexpf128@PLT
	movl	76(%rsp), %ebx
	movaps	%xmm0, (%rsp)
	leal	2(%rbx), %eax
	movl	%ebx, %r12d
	cmpl	$4, %eax
	ja	.L28
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L22
	movdqa	(%rsp), %xmm0
	subl	$1, %r12d
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
.L13:
	leaq	176+P(%rip), %rbx
	leaq	-192(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	.LC0(%rip), %xmm6
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC1(%rip), %xmm1
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L29:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 16(%rsp)
.L15:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L29
	movdqa	32(%rsp), %xmm0
	leaq	160+Q(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	leaq	-176(%rbx), %rbp
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L17:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L30
	movdqa	48(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
.L10:
	movdqa	%xmm2, %xmm0
	movdqa	.LC22(%rip), %xmm1
	movaps	%xmm2, 32(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC22(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movl	%r12d, %edi
	movaps	%xmm0, (%rsp)
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	.LC4(%rip), %xmm1
	pand	%xmm0, %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__divtf3@PLT
.L1:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movdqa	.LC7(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L21
	movdqa	.LC7(%rip), %xmm1
	subl	$1, %ebx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
.L9:
	movdqa	16(%rsp), %xmm0
	movl	%ebx, %r12d
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	movdqa	(%rsp), %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	movdqa	.LC7(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	jmp	.L9
	.size	__ieee754_log2f128, .-__ieee754_log2f128
	.section	.rodata
	.align 32
	.type	Q, @object
	.size	Q, 192
Q:
	.long	974833098
	.long	3983575615
	.long	1478515891
	.long	1074672606
	.long	1363910953
	.long	372740050
	.long	2168456170
	.long	1074856072
	.long	3928175223
	.long	1093578671
	.long	555124929
	.long	1074953157
	.long	3876003188
	.long	3816404440
	.long	3902617826
	.long	1075005691
	.long	244691732
	.long	101923003
	.long	658470442
	.long	1075016152
	.long	2380495985
	.long	886625134
	.long	1067692091
	.long	1074993848
	.long	3169996515
	.long	3866347283
	.long	1574618040
	.long	1074932588
	.long	1224732860
	.long	694297421
	.long	3061800703
	.long	1074837275
	.long	997651553
	.long	1262749082
	.long	3615157506
	.long	1074705908
	.long	886286085
	.long	4063926484
	.long	870995301
	.long	1074535897
	.long	3346593135
	.long	2667261922
	.long	343375446
	.long	1074317119
	.long	2894954405
	.long	1243681754
	.long	4225387839
	.long	1074037538
	.align 32
	.type	P, @object
	.size	P, 208
P:
	.long	1299777404
	.long	3879778388
	.long	3403010287
	.long	1074567805
	.long	3665220616
	.long	2683394341
	.long	2348546465
	.long	1074737039
	.long	2098957012
	.long	397171827
	.long	2736341389
	.long	1074825596
	.long	1684703774
	.long	3835374584
	.long	783309735
	.long	1074865575
	.long	1907052596
	.long	2722646897
	.long	3130992471
	.long	1074861770
	.long	2166468600
	.long	2515288904
	.long	1783230833
	.long	1074814745
	.long	1153561700
	.long	290522294
	.long	2441227584
	.long	1074735271
	.long	3664047852
	.long	1667887306
	.long	1325748431
	.long	1074613410
	.long	951581226
	.long	1590019597
	.long	2041818642
	.long	1074454046
	.long	3590128992
	.long	4199782165
	.long	2825100328
	.long	1074240371
	.long	1655018215
	.long	1733051370
	.long	1366883990
	.long	1073967969
	.long	445394848
	.long	782022121
	.long	3792914380
	.long	1073610711
	.long	8741116
	.long	2504214818
	.long	2698440066
	.long	1072405764
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	445394848
	.long	782022121
	.long	3792914380
	.long	1073610711
	.align 16
.LC1:
	.long	8741116
	.long	2504214818
	.long	2698440066
	.long	1072405764
	.align 16
.LC2:
	.long	3346593135
	.long	2667261922
	.long	343375446
	.long	1074317119
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC8:
	.long	325511829
	.long	3372790523
	.long	3865572284
	.long	1073637897
	.align 16
.LC9:
	.long	3838760712
	.long	1749523796
	.long	478629722
	.long	-1073822710
	.align 16
.LC10:
	.long	2735601192
	.long	1448826385
	.long	3040057118
	.long	1074086471
	.align 16
.LC11:
	.long	1165886384
	.long	3062951389
	.long	1353261326
	.long	1074395667
	.align 16
.LC12:
	.long	3693756872
	.long	3927060692
	.long	3417556205
	.long	1074610208
	.align 16
.LC13:
	.long	3892757290
	.long	948854074
	.long	2490895907
	.long	1074749100
	.align 16
.LC14:
	.long	2916428899
	.long	628477308
	.long	873499491
	.long	1074125451
	.align 16
.LC15:
	.long	3922856684
	.long	2418196305
	.long	3676626696
	.long	1074459600
	.align 16
.LC16:
	.long	851125885
	.long	3945266192
	.long	3615278364
	.long	1074708762
	.align 16
.LC17:
	.long	2770410685
	.long	2934725590
	.long	4047370030
	.long	1074890438
	.align 16
.LC18:
	.long	3003253112
	.long	4003369444
	.long	1910524869
	.long	1075004755
	.align 16
.LC19:
	.long	844150013
	.long	1745004283
	.long	3051546817
	.long	1074795770
	.align 16
.LC20:
	.long	3413710640
	.long	470022776
	.long	282352930
	.long	1075027832
	.align 16
.LC21:
	.long	2894954405
	.long	1243681754
	.long	4225387839
	.long	1074037538
	.align 16
.LC22:
	.long	4135799018
	.long	2245915711
	.long	3645563071
	.long	1073595729
