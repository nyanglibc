	.text
	.globl	__floatunsitf
	.globl	__divtf3
	.globl	__addtf3
	.globl	__subtf3
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__erfcf128
	.type	__erfcf128, @function
__erfcf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$80, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %rbx
	movdqa	(%rsp), %xmm3
	shrq	$32, %rbx
	movl	%eax, %eax
	movl	%ebx, %edx
	andl	$2147483647, %edx
	movq	%rdx, %rcx
	salq	$32, %rcx
	orq	%rcx, %rax
	cmpl	$2147418111, %edx
	movaps	%xmm3, 16(%rsp)
	movq	%rax, 24(%rsp)
	jg	.L102
	cmpl	$1073545215, %edx
	jg	.L4
	cmpl	$1066205183, %edx
	jg	.L5
	movdqa	(%rsp), %xmm1
	movdqa	.LC48(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	addq	$80, %rsp
	popq	%rbx
	movdqa	%xmm2, %xmm0
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1073692671, %edx
	jg	.L6
	movdqa	16(%rsp), %xmm4
	movdqa	.LC49(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, (%rsp)
	call	__multf3@PLT
	call	__fixtfsi@PLT
	cmpl	$8, %eax
	ja	.L7
	leaq	.L9(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L9:
	.long	.L7-.L9
	.long	.L7-.L9
	.long	.L8-.L9
	.long	.L10-.L9
	.long	.L11-.L9
	.long	.L12-.L9
	.long	.L13-.L9
	.long	.L14-.L9
	.long	.L15-.L9
	.text
	.p2align 4,,10
	.p2align 3
.L102:
	shrl	$31, %ebx
	leal	(%rbx,%rbx), %edi
	call	__floatunsitf@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC48(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$80, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1074113535, %edx
	jg	.L49
	cmpl	$1073881087, %edx
	jle	.L50
	movl	%ebx, %eax
	shrl	$31, %eax
	testb	%al, %al
	jne	.L87
.L50:
	movdqa	(%rsp), %xmm4
	pand	.LC83(%rip), %xmm4
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	movaps	%xmm4, 32(%rsp)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC48(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC49(%rip), %xmm0
	call	__divtf3@PLT
	call	__fixtfsi@PLT
	cmpl	$7, %eax
	ja	.L88
	leaq	.L53(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L53:
	.long	.L88-.L53
	.long	.L89-.L53
	.long	.L90-.L53
	.long	.L91-.L53
	.long	.L92-.L53
	.long	.L93-.L53
	.long	.L94-.L53
	.long	.L95-.L53
	.text
	.p2align 4,,10
	.p2align 3
.L5:
	movdqa	(%rsp), %xmm0
	call	__erff128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC48(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L49:
	testl	%ebx, %ebx
	js	.L87
	movq	errno@gottpoff(%rip), %rax
	movdqa	.LC82(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	movl	$34, %fs:(%rax)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L87:
	movdqa	.LC82(%rip), %xmm1
	movdqa	.LC81(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movdqa	.LC48(%rip), %xmm1
	leaq	112+RNr19(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC18(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC19(%rip), %xmm1
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L103:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 16(%rsp)
.L42:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L103
	movdqa	(%rsp), %xmm0
	leaq	96+RDr19(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC74(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC20(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L104:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 16(%rsp)
.L44:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L104
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC75(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC76(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	.p2align 4,,10
	.p2align 3
.L20:
	testl	%ebx, %ebx
	jns	.L1
	movdqa	%xmm2, %xmm1
	movdqa	.LC81(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	.LC70(%rip), %xmm1
	leaq	112+RNr18(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC15(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC16(%rip), %xmm1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L105:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L38:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L105
	movdqa	(%rsp), %xmm0
	leaq	96+RDr18(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC71(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC17(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L106:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L40:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L106
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC72(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC73(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L13:
	movdqa	.LC66(%rip), %xmm1
	leaq	112+RNr17(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC12(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC13(%rip), %xmm1
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L107:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 16(%rsp)
.L34:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L107
	movdqa	(%rsp), %xmm0
	leaq	96+RDr17(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC67(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC14(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L108:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 16(%rsp)
.L36:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L108
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC68(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC69(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L12:
	movdqa	.LC62(%rip), %xmm1
	leaq	112+RNr16(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC9(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC10(%rip), %xmm1
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L109:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L30:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L109
	movdqa	(%rsp), %xmm0
	leaq	96+RDr16(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC63(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC11(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L110:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L32:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L110
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC64(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC65(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	.LC58(%rip), %xmm1
	leaq	112+RNr15(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC6(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC7(%rip), %xmm1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L111:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L26:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L111
	movdqa	(%rsp), %xmm0
	leaq	96+RDr15(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC59(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L112:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 16(%rsp)
.L28:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L112
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC60(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC61(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	.LC54(%rip), %xmm1
	leaq	112+RNr14(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC3(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
	movdqa	.LC4(%rip), %xmm1
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L113:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 16(%rsp)
.L22:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L113
	movdqa	(%rsp), %xmm0
	leaq	96+RDr14(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC55(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L114:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 16(%rsp)
.L24:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L114
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC56(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC57(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	(%rsp), %xmm0
	leaq	112+RNr13(%rip), %rbp
	movdqa	.LC50(%rip), %xmm1
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC0(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	movdqa	.LC1(%rip), %xmm0
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L115:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L17:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	jne	.L115
	movdqa	(%rsp), %xmm1
	leaq	96+RDr13(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC51(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm4
	movaps	%xmm4, 16(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L116:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L19:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	jne	.L116
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC52(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC53(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L7:
	movdqa	.LC77(%rip), %xmm1
	leaq	112+RNr20(%rip), %rbp
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC21(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC22(%rip), %xmm1
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L117:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L46:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L117
	movdqa	(%rsp), %xmm0
	leaq	96+RDr20(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC78(%rip), %xmm1
	leaq	-112(%rbp), %r12
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC23(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L118:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L48:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L118
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC79(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC80(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L20
.L95:
	movdqa	.LC45(%rip), %xmm5
	leaq	128+RNr8(%rip), %rbp
	movaps	%xmm5, 16(%rsp)
	leaq	-144(%rbp), %r12
	movdqa	.LC46(%rip), %xmm2
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L119:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L59:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm2
	jne	.L119
	movaps	%xmm0, 16(%rsp)
	leaq	112+RDr8(%rip), %rbp
	leaq	-128(%rbp), %r12
	movdqa	.LC91(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC47(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 48(%rsp)
	movdqa	16(%rsp), %xmm2
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L120:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 48(%rsp)
.L84:
	subq	$16, %rbp
	movaps	%xmm2, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm2
	jne	.L120
.L83:
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 64(%rsp)
.L63:
	movabsq	$-144115188075855872, %rdx
	movdqa	32(%rsp), %xmm6
	movq	%xmm6, %rax
	movaps	%xmm6, 16(%rsp)
	andq	%rdx, %rax
	movq	%rax, 16(%rsp)
	movdqa	16(%rsp), %xmm0
	pxor	.LC92(%rip), %xmm0
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC93(%rip), %xmm1
	call	__subtf3@PLT
	call	__ieee754_expf128@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	call	__ieee754_expf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	testl	%ebx, %ebx
	movdqa	32(%rsp), %xmm1
	js	.L85
	call	__divtf3@PLT
	pxor	%xmm1, %xmm1
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
.L94:
	movdqa	.LC42(%rip), %xmm7
	leaq	128+RNr7(%rip), %rbp
	movdqa	.LC43(%rip), %xmm4
	leaq	-144(%rbp), %r12
	movaps	%xmm7, 48(%rsp)
	movaps	%xmm4, 16(%rsp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L121:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 48(%rsp)
.L58:
	subq	$16, %rbp
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movaps	%xmm0, 16(%rsp)
	jne	.L121
	leaq	128+RDr7(%rip), %rbp
	movdqa	.LC90(%rip), %xmm1
	leaq	-144(%rbp), %r12
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC44(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 48(%rsp)
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L122:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 48(%rsp)
.L81:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L122
.L80:
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 64(%rsp)
	jmp	.L63
.L93:
	movdqa	.LC39(%rip), %xmm4
	leaq	128+RNr6(%rip), %rbp
	movdqa	.LC40(%rip), %xmm6
	leaq	-144(%rbp), %r12
	movaps	%xmm4, 48(%rsp)
	movaps	%xmm6, 16(%rsp)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L123:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 48(%rsp)
.L57:
	subq	$16, %rbp
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movaps	%xmm0, 16(%rsp)
	jne	.L123
	leaq	128+RDr6(%rip), %rbp
	movdqa	.LC89(%rip), %xmm1
	leaq	-144(%rbp), %r12
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC41(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 48(%rsp)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L124:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 48(%rsp)
.L78:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L124
	jmp	.L80
.L92:
	movdqa	.LC36(%rip), %xmm5
	leaq	144+RNr5(%rip), %rbp
	movaps	%xmm5, 16(%rsp)
	leaq	-160(%rbp), %r12
	movdqa	.LC37(%rip), %xmm2
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L125:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L56:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm2
	jne	.L125
	movaps	%xmm0, 16(%rsp)
	leaq	128+RDr5(%rip), %rbp
	leaq	-144(%rbp), %r12
	movdqa	.LC88(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC38(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 48(%rsp)
	movdqa	16(%rsp), %xmm2
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L126:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 48(%rsp)
.L75:
	subq	$16, %rbp
	movaps	%xmm2, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm2
	jne	.L126
	jmp	.L83
.L91:
	movdqa	.LC33(%rip), %xmm5
	leaq	144+RNr4(%rip), %rbp
	movdqa	.LC34(%rip), %xmm4
	leaq	-160(%rbp), %r12
	movaps	%xmm5, 48(%rsp)
	movaps	%xmm4, 16(%rsp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L127:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 48(%rsp)
.L55:
	subq	$16, %rbp
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movaps	%xmm0, 16(%rsp)
	jne	.L127
	leaq	144+RDr4(%rip), %rbp
	movdqa	.LC87(%rip), %xmm1
	leaq	-160(%rbp), %r12
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC35(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 48(%rsp)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L128:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 48(%rsp)
.L72:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L128
	jmp	.L80
.L90:
	movdqa	.LC30(%rip), %xmm5
	leaq	160+RNr3(%rip), %rbp
	movaps	%xmm5, 16(%rsp)
	leaq	-176(%rbp), %r12
	movdqa	.LC31(%rip), %xmm2
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L129:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 16(%rsp)
.L54:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm2
	jne	.L129
	movaps	%xmm0, 48(%rsp)
	leaq	144+RDr3(%rip), %rbp
	leaq	-160(%rbp), %r12
	movdqa	.LC86(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC32(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 16(%rsp)
	movdqa	48(%rsp), %xmm2
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L130:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 16(%rsp)
.L69:
	subq	$16, %rbp
	movaps	%xmm2, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm2
	jne	.L130
	jmp	.L83
.L89:
	movdqa	.LC27(%rip), %xmm5
	leaq	160+RNr2(%rip), %rbp
	movaps	%xmm5, 16(%rsp)
	leaq	-176(%rbp), %r12
	movdqa	.LC28(%rip), %xmm2
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L131:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L52:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm2
	jne	.L131
	movaps	%xmm0, 48(%rsp)
	leaq	144+RDr2(%rip), %rbp
	leaq	-160(%rbp), %r12
	movdqa	.LC85(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC29(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 16(%rsp)
	movdqa	48(%rsp), %xmm2
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L132:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 16(%rsp)
.L66:
	subq	$16, %rbp
	movaps	%xmm2, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm2
	jne	.L132
	jmp	.L83
.L88:
	movdqa	.LC24(%rip), %xmm4
	leaq	128+RNr1(%rip), %rbp
	movaps	%xmm4, 16(%rsp)
	leaq	-144(%rbp), %r12
	movdqa	.LC25(%rip), %xmm2
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L133:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 16(%rsp)
.L51:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm2
	jne	.L133
	movaps	%xmm0, 16(%rsp)
	leaq	112+RDr1(%rip), %rbp
	leaq	-128(%rbp), %r12
	movdqa	.LC84(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC26(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 48(%rsp)
	movdqa	16(%rsp), %xmm2
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L134:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 48(%rsp)
.L62:
	subq	$16, %rbp
	movaps	%xmm2, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm2
	jne	.L134
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L85:
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC81(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.size	__erfcf128, .-__erfcf128
	.weak	erfcf128
	.set	erfcf128,__erfcf128
	.globl	__floatsitf
	.globl	__lttf2
	.p2align 4,,15
	.globl	__erff128
	.type	__erff128, @function
__erff128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$64, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rbx
	movdqa	(%rsp), %xmm3
	shrq	$32, %rbx
	movl	%ebx, %eax
	andl	$2147483647, %eax
	cmpl	$2147418111, %eax
	movaps	%xmm3, 16(%rsp)
	jg	.L160
	cmpl	$1073676287, %eax
	jle	.L138
	cmpl	$1073938431, %eax
	jle	.L154
	testl	%ebx, %ebx
	movdqa	.LC48(%rip), %xmm2
	jle	.L154
.L135:
	addq	$64, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%rax, %rcx
	movl	%edx, %edx
	salq	$32, %rcx
	orq	%rcx, %rdx
	cmpl	$1073659903, %eax
	movq	%rdx, 24(%rsp)
	movdqa	16(%rsp), %xmm6
	movaps	%xmm6, 32(%rsp)
	jg	.L140
	cmpl	$1069940735, %eax
	jle	.L141
	movdqa	(%rsp), %xmm1
	leaq	112+TN1(%rip), %rbp
	movdqa	%xmm1, %xmm0
	leaq	-128(%rbp), %r12
	call	__multf3@PLT
	movdqa	.LC95(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	movdqa	.LC96(%rip), %xmm1
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L161:
	movdqa	0(%rbp), %xmm6
	movaps	%xmm6, 16(%rsp)
.L142:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L161
	movdqa	32(%rsp), %xmm0
	leaq	112+TD1(%rip), %rbp
	call	__multf3@PLT
	movdqa	.LC106(%rip), %xmm1
	leaq	-128(%rbp), %r12
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC97(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L162:
	movdqa	0(%rbp), %xmm7
	movaps	%xmm7, 16(%rsp)
.L147:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L162
	movdqa	48(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L160:
	shrl	$31, %ebx
	movl	$1, %edi
	addl	%ebx, %ebx
	subl	%ebx, %edi
	call	__floatsitf@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC48(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	addq	$64, %rsp
	popq	%rbx
	movdqa	%xmm2, %xmm0
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movdqa	16(%rsp), %xmm0
	leaq	112+TN2(%rip), %rbp
	movdqa	.LC48(%rip), %xmm1
	leaq	-128(%rbp), %r12
	call	__subtf3@PLT
	movdqa	.LC98(%rip), %xmm7
	movaps	%xmm7, 32(%rsp)
	movaps	%xmm0, (%rsp)
	movdqa	.LC99(%rip), %xmm7
	movaps	%xmm7, 16(%rsp)
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L163:
	movdqa	0(%rbp), %xmm4
	movaps	%xmm4, 32(%rsp)
.L150:
	subq	$16, %rbp
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movaps	%xmm0, 16(%rsp)
	jne	.L163
	leaq	112+TD2(%rip), %rbp
	movdqa	.LC107(%rip), %xmm1
	leaq	-128(%rbp), %r12
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC100(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 32(%rsp)
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L164:
	movdqa	0(%rbp), %xmm5
	movaps	%xmm5, 32(%rsp)
.L152:
	subq	$16, %rbp
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%r12, %rbp
	movdqa	%xmm0, %xmm1
	jne	.L164
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC108(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L148:
	testl	%ebx, %ebx
	jns	.L135
	pxor	.LC92(%rip), %xmm2
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L154:
	movdqa	(%rsp), %xmm0
	call	__erfcf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC48(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L141:
	cmpl	$524287, %eax
	jg	.L143
	movdqa	.LC101(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC102(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC103(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	pand	.LC83(%rip), %xmm0
	movdqa	.LC104(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jns	.L135
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L143:
	movdqa	.LC105(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L135
	.size	__erff128, .-__erff128
	.weak	erff128
	.set	erff128,__erff128
	.section	.rodata
	.align 32
	.type	RDr1, @object
	.size	RDr1, 144
RDr1:
	.long	340060042
	.long	394326827
	.long	2814774530
	.long	1072505131
	.long	2577614062
	.long	1378026964
	.long	2786405938
	.long	1072911974
	.long	3795809320
	.long	1119368893
	.long	1681870689
	.long	1073234303
	.long	2342411664
	.long	1742588613
	.long	2503950130
	.long	1073493375
	.long	4010278817
	.long	3572644061
	.long	978874159
	.long	1073695245
	.long	3907255378
	.long	2536659252
	.long	4035532758
	.long	1073842546
	.long	2992233540
	.long	4266292544
	.long	1572387473
	.long	1073936299
	.long	1284081952
	.long	2361216278
	.long	2910681733
	.long	1073952994
	.long	2803993979
	.long	3755256967
	.long	2777513288
	.long	1073884959
	.align 32
	.type	RNr1, @object
	.size	RNr1, 160
RNr1:
	.long	268627111
	.long	1041947480
	.long	2835383557
	.long	-1075417821
	.long	1416131844
	.long	87201435
	.long	629375262
	.long	-1074960189
	.long	4284443002
	.long	1394640881
	.long	3360365947
	.long	-1074590243
	.long	2025390796
	.long	1397070830
	.long	2209443161
	.long	-1074293182
	.long	831644469
	.long	539745071
	.long	3914589579
	.long	-1074048400
	.long	3782787376
	.long	1714336921
	.long	886080560
	.long	-1073856609
	.long	3276401547
	.long	3258052028
	.long	1612563047
	.long	-1073721840
	.long	3271880763
	.long	327704521
	.long	4108384073
	.long	-1073650370
	.long	3085734172
	.long	3376702612
	.long	652664368
	.long	-1073657866
	.long	2856477196
	.long	4171140881
	.long	621168806
	.long	-1073788298
	.align 32
	.type	RDr2, @object
	.size	RDr2, 176
RDr2:
	.long	4159612301
	.long	3121067875
	.long	3299660160
	.long	1072457481
	.long	1143964288
	.long	1332196696
	.long	2407580760
	.long	1072875338
	.long	4088871927
	.long	2473109733
	.long	1325144751
	.long	1073208098
	.long	2333505023
	.long	3169346842
	.long	1780083025
	.long	1073482408
	.long	1633585621
	.long	3969753983
	.long	4079923959
	.long	1073701204
	.long	3961473997
	.long	613397419
	.long	2830276601
	.long	1073880486
	.long	2002640002
	.long	1579395899
	.long	3928590900
	.long	1074010543
	.long	596300044
	.long	971363855
	.long	2257683660
	.long	1074088279
	.long	1855211666
	.long	1225086379
	.long	1244596938
	.long	1074113840
	.long	2292544762
	.long	1190726065
	.long	2335949307
	.long	1074075072
	.long	1455096050
	.long	2554530791
	.long	2066603635
	.long	1073945628
	.align 32
	.type	RNr2, @object
	.size	RNr2, 192
RNr2:
	.long	1381103706
	.long	878501875
	.long	2929043303
	.long	-1075460772
	.long	1902170475
	.long	1034613405
	.long	1484909898
	.long	-1074998536
	.long	192540353
	.long	2442269890
	.long	4214437288
	.long	-1074629088
	.long	2457821688
	.long	3200633217
	.long	2032930062
	.long	-1074317025
	.long	3807732477
	.long	3848905594
	.long	2438341769
	.long	-1074056915
	.long	3815826556
	.long	1071379136
	.long	3731873951
	.long	-1073845787
	.long	1955206550
	.long	4091996083
	.long	1992869113
	.long	-1073674422
	.long	2497092904
	.long	2051429023
	.long	1998013409
	.long	-1073558287
	.long	3199055772
	.long	3867652877
	.long	2172959238
	.long	-1073492645
	.long	3227395962
	.long	753911614
	.long	3137338294
	.long	-1073488246
	.long	4173918531
	.long	805857339
	.long	1109432338
	.long	-1073567721
	.long	4212603917
	.long	2844622006
	.long	741559444
	.long	-1073769845
	.align 32
	.type	RDr3, @object
	.size	RDr3, 176
RDr3:
	.long	4019115375
	.long	3177542663
	.long	3305739082
	.long	1072865037
	.long	2240078812
	.long	1518000373
	.long	1311487555
	.long	1073249051
	.long	1754260559
	.long	3774207399
	.long	1152291849
	.long	1073551839
	.long	584592707
	.long	395944207
	.long	941212503
	.long	1073790394
	.long	2184978409
	.long	4170193839
	.long	1958339754
	.long	1073978136
	.long	3218502033
	.long	2637664709
	.long	1587499278
	.long	1074120937
	.long	3717307373
	.long	2286047591
	.long	2155283645
	.long	1074213286
	.long	397895457
	.long	3714120395
	.long	2606768763
	.long	1074257429
	.long	4158719496
	.long	3249053707
	.long	3873272981
	.long	1074240152
	.long	381416649
	.long	369014653
	.long	2130427748
	.long	1074157776
	.long	2751929419
	.long	2619096035
	.long	3732132099
	.long	1073988236
	.align 32
	.type	RNr3, @object
	.size	RNr3, 192
RNr3:
	.long	3227294404
	.long	4008020894
	.long	4260168129
	.long	-1075050997
	.long	2579662268
	.long	2936239049
	.long	3618408668
	.long	-1074610464
	.long	2091616842
	.long	2407658050
	.long	533558577
	.long	-1074261381
	.long	1679223702
	.long	1076883898
	.long	2541538379
	.long	-1073982966
	.long	3754109356
	.long	3863602924
	.long	3277530834
	.long	-1073752357
	.long	4155652732
	.long	3881788983
	.long	2372452173
	.long	-1073576651
	.long	2051648067
	.long	2784969159
	.long	821947601
	.long	-1073446528
	.long	2431946190
	.long	3588835042
	.long	3664968013
	.long	-1073364891
	.long	2052145445
	.long	3934978774
	.long	4119740836
	.long	-1073339759
	.long	3774913917
	.long	3386121101
	.long	2269441651
	.long	-1073382552
	.long	299744570
	.long	1850928023
	.long	2884078513
	.long	-1073505745
	.long	2877106353
	.long	1864440645
	.long	438938254
	.long	-1073755718
	.align 32
	.type	RDr4, @object
	.size	RDr4, 176
RDr4:
	.long	3029240902
	.long	576104494
	.long	3801734174
	.long	-1073917379
	.long	1921326614
	.long	3738550337
	.long	3118650771
	.long	-1073565388
	.long	362742361
	.long	3817974485
	.long	3155390260
	.long	-1073301191
	.long	544259479
	.long	2293228436
	.long	952110567
	.long	-1073098381
	.long	686907376
	.long	1731017264
	.long	1675926221
	.long	-1072949816
	.long	2097864529
	.long	4020705752
	.long	2251701535
	.long	-1072856757
	.long	2850730227
	.long	2977815622
	.long	3625958345
	.long	-1072811164
	.long	2418856487
	.long	1973341208
	.long	2341465180
	.long	-1072820755
	.long	1760465902
	.long	3121813589
	.long	3084624789
	.long	-1072899002
	.long	208359088
	.long	2992115458
	.long	1461992846
	.long	-1073062063
	.long	325952384
	.long	3181587193
	.long	829535598
	.long	-1073345335
	.align 32
	.type	RNr4, @object
	.size	RNr4, 176
RNr4:
	.long	4231989591
	.long	1624204295
	.long	632204319
	.long	1073130266
	.long	2917494539
	.long	2887838549
	.long	3784934894
	.long	1073557983
	.long	1810642815
	.long	2598663156
	.long	2872044093
	.long	1073878947
	.long	3625556072
	.long	1108699232
	.long	1815908829
	.long	1074127586
	.long	2403599543
	.long	3677527413
	.long	3223355172
	.long	1074315744
	.long	3301078098
	.long	2296978327
	.long	3463145484
	.long	1074453184
	.long	557940074
	.long	1792852695
	.long	2703035368
	.long	1074535625
	.long	2104318913
	.long	3566076182
	.long	3742235905
	.long	1074558982
	.long	3961907758
	.long	894968105
	.long	1515808661
	.long	1074526794
	.long	1091455653
	.long	2587852596
	.long	1821053371
	.long	1074407932
	.long	2202865610
	.long	1431220795
	.long	2881691653
	.long	1074178431
	.align 32
	.type	RDr5, @object
	.size	RDr5, 160
RDr5:
	.long	3192525918
	.long	1157304620
	.long	370272351
	.long	1073568229
	.long	4203080646
	.long	3508583482
	.long	1011031249
	.long	1073891079
	.long	870852864
	.long	3510806444
	.long	3431726617
	.long	1074126934
	.long	1031862270
	.long	2688345940
	.long	510071301
	.long	1074291698
	.long	4172939958
	.long	3083606939
	.long	430192454
	.long	1074405829
	.long	100214969
	.long	146239672
	.long	2910512695
	.long	1074465003
	.long	4025419565
	.long	1672533712
	.long	3601401190
	.long	1074465649
	.long	4201256856
	.long	259837794
	.long	3736506547
	.long	1074403880
	.long	1862174987
	.long	4214626901
	.long	371155820
	.long	1074271878
	.long	1606055207
	.long	1513571989
	.long	153391221
	.long	1074043744
	.align 32
	.type	RNr5, @object
	.size	RNr5, 176
RNr5:
	.long	3734722786
	.long	996846798
	.long	238747316
	.long	-1074350908
	.long	1411160088
	.long	2662276564
	.long	3771557307
	.long	-1073933266
	.long	2605866844
	.long	1798762910
	.long	1095138799
	.long	-1073642155
	.long	3605044170
	.long	85100389
	.long	3457764689
	.long	-1073419882
	.long	1522227080
	.long	2326545109
	.long	1302368839
	.long	-1073267289
	.long	1474905191
	.long	2588893003
	.long	1968130242
	.long	-1073168725
	.long	1748455163
	.long	3942753384
	.long	1067485183
	.long	-1073130495
	.long	951881471
	.long	2900907396
	.long	3595355795
	.long	-1073148732
	.long	2791255523
	.long	2486820675
	.long	309137151
	.long	-1073245764
	.long	2946253495
	.long	2854616410
	.long	1144263533
	.long	-1073425253
	.long	2448204785
	.long	1762089962
	.long	2557436825
	.long	-1073739278
	.align 32
	.type	RDr6, @object
	.size	RDr6, 160
RDr6:
	.long	3007433687
	.long	2538432696
	.long	1509118909
	.long	-1073328919
	.long	2369777112
	.long	1490142556
	.long	639435783
	.long	-1073030399
	.long	1522204472
	.long	2035918206
	.long	2421639290
	.long	-1072824316
	.long	1000246819
	.long	3642545634
	.long	4181473404
	.long	-1072689717
	.long	1512299644
	.long	1566252662
	.long	460354199
	.long	-1072616193
	.long	648740635
	.long	2065326266
	.long	3381025822
	.long	-1072600972
	.long	4000496396
	.long	3292620056
	.long	524638490
	.long	-1072643760
	.long	3012650212
	.long	1550447979
	.long	2375156756
	.long	-1072755581
	.long	207865816
	.long	2988500113
	.long	1161976887
	.long	-1072954831
	.long	1777528580
	.long	281185044
	.long	2832206588
	.long	-1073285268
	.align 32
	.type	RNr6, @object
	.size	RNr6, 160
RNr6:
	.long	1184087903
	.long	929315582
	.long	644919941
	.long	1073718367
	.long	317145641
	.long	3072316942
	.long	688782953
	.long	1074127580
	.long	823949060
	.long	3316079680
	.long	1481497963
	.long	1074399464
	.long	948504137
	.long	394485544
	.long	1624437261
	.long	1074587755
	.long	1119619549
	.long	3415343422
	.long	2507363227
	.long	1074706927
	.long	2712526862
	.long	3221341998
	.long	2680498315
	.long	1074764544
	.long	4028475478
	.long	771240031
	.long	4231607337
	.long	1074758825
	.long	3694511857
	.long	1803924509
	.long	3499030851
	.long	1074683627
	.long	2318794642
	.long	408815043
	.long	2545998254
	.long	1074529736
	.long	23021965
	.long	3811171597
	.long	2280834395
	.long	1074250417
	.align 32
	.type	RDr7, @object
	.size	RDr7, 160
RDr7:
	.long	3372336958
	.long	2880070276
	.long	2240177524
	.long	-1073108141
	.long	335196957
	.long	1709626948
	.long	2910912481
	.long	-1072824256
	.long	2697501047
	.long	3918710769
	.long	907960922
	.long	-1072641520
	.long	1789608057
	.long	2065350574
	.long	2046718226
	.long	-1072526628
	.long	652966434
	.long	216612081
	.long	1185011034
	.long	-1072471120
	.long	2870615303
	.long	2105119044
	.long	1681716256
	.long	-1072475198
	.long	4191783342
	.long	2570289079
	.long	2190533712
	.long	-1072542183
	.long	2828023719
	.long	44238119
	.long	2193535699
	.long	-1072678913
	.long	3432714260
	.long	2536195016
	.long	388474595
	.long	-1072901818
	.long	706380816
	.long	3166793199
	.long	564976909
	.long	-1073258707
	.align 32
	.type	RNr7, @object
	.size	RNr7, 160
RNr7:
	.long	500078476
	.long	437719396
	.long	2839149863
	.long	1073941963
	.long	139148218
	.long	3373049919
	.long	1592523736
	.long	1074341518
	.long	1483134338
	.long	1431809870
	.long	100380283
	.long	1074598838
	.long	2942488625
	.long	3847857858
	.long	3574224682
	.long	1074766703
	.long	4099741724
	.long	2549590601
	.long	80445375
	.long	1074868721
	.long	788911926
	.long	381366246
	.long	2677909328
	.long	1074905596
	.long	1932573560
	.long	3317876121
	.long	2342916025
	.long	1074875144
	.long	1824425942
	.long	121300089
	.long	939356835
	.long	1074776732
	.long	1202567024
	.long	2902447826
	.long	2337494919
	.long	1074594656
	.long	1521932787
	.long	3680606074
	.long	183205061
	.long	1074286341
	.align 32
	.type	RDr8, @object
	.size	RDr8, 144
RDr8:
	.long	3344314303
	.long	566475117
	.long	4240791887
	.long	1074068655
	.long	613826783
	.long	187492013
	.long	1068810649
	.long	1074327299
	.long	3826003541
	.long	1501824392
	.long	3695586179
	.long	1074487470
	.long	1402764681
	.long	2182576110
	.long	2705897968
	.long	1074584655
	.long	2952866640
	.long	1763136137
	.long	797919025
	.long	1074612809
	.long	354722013
	.long	1914691734
	.long	365115673
	.long	1074584835
	.long	2250274714
	.long	2858014645
	.long	4014739525
	.long	1074486165
	.long	131732523
	.long	1433465577
	.long	2812285745
	.long	1074320819
	.long	359439791
	.long	1025893849
	.long	4128877303
	.long	1074064322
	.align 32
	.type	RNr8, @object
	.size	RNr8, 160
RNr8:
	.long	3862765733
	.long	1589275242
	.long	27917071
	.long	1074011903
	.long	1417302285
	.long	2845124110
	.long	4281173226
	.long	1074269775
	.long	3393759199
	.long	1745226048
	.long	2754362561
	.long	1074425449
	.long	460973075
	.long	885524864
	.long	287607626
	.long	1074514932
	.long	2874735657
	.long	2262936434
	.long	1094351160
	.long	1074536328
	.long	68062008
	.long	3329586437
	.long	659445374
	.long	1074489179
	.long	956683317
	.long	1932199095
	.long	2469618526
	.long	1074375578
	.long	3981277471
	.long	4184991645
	.long	2593757468
	.long	1074179622
	.long	4168439028
	.long	969362888
	.long	3830019792
	.long	1073878965
	.long	588497985
	.long	3004344865
	.long	8714342
	.long	1073385338
	.align 32
	.type	RDr20, @object
	.size	RDr20, 128
RDr20:
	.long	2786646951
	.long	3743300481
	.long	2354263321
	.long	1074428698
	.long	3215666544
	.long	4086335211
	.long	2156615632
	.long	1074372329
	.long	2786349397
	.long	2530937666
	.long	3814936265
	.long	1074375138
	.long	3217745581
	.long	3717623571
	.long	1549087176
	.long	1074282412
	.long	3419802236
	.long	2438471555
	.long	3280368865
	.long	1074224361
	.long	413230047
	.long	2870250904
	.long	392572744
	.long	1074094578
	.long	4070177078
	.long	4064786526
	.long	3181720989
	.long	1074001193
	.long	1795572320
	.long	3324554926
	.long	1190441894
	.long	1073814407
	.align 32
	.type	RNr20, @object
	.size	RNr20, 144
RNr20:
	.long	1615767265
	.long	126499626
	.long	2785417984
	.long	-1073159518
	.long	779276512
	.long	2361264441
	.long	1932174583
	.long	1074271962
	.long	2006118918
	.long	4240382720
	.long	811862727
	.long	-1073235806
	.long	811435408
	.long	720733020
	.long	1108933584
	.long	1074044987
	.long	1384052243
	.long	1119614803
	.long	2290421832
	.long	-1073531646
	.long	642396794
	.long	710515635
	.long	4268895599
	.long	-1074036849
	.long	1275340480
	.long	3983816419
	.long	3431811803
	.long	-1073699017
	.long	2748438882
	.long	3834721168
	.long	1677600535
	.long	1073644487
	.long	1375531053
	.long	3229016163
	.long	1893590572
	.long	-1074034254
	.align 32
	.type	RDr19, @object
	.size	RDr19, 128
RDr19:
	.long	1341679482
	.long	299299386
	.long	3050867296
	.long	1074419465
	.long	1860859738
	.long	2645894736
	.long	1576294555
	.long	1074347161
	.long	3863548859
	.long	1531673821
	.long	1398145053
	.long	1074359983
	.long	871726460
	.long	3921326870
	.long	4254643772
	.long	1074259678
	.long	3601509025
	.long	1196748266
	.long	2465826322
	.long	1074211678
	.long	3991296297
	.long	1010916877
	.long	1385692965
	.long	1074074306
	.long	2483520086
	.long	1786281980
	.long	2912990722
	.long	1073987645
	.long	62527580
	.long	1649004427
	.long	1232417159
	.long	1073794398
	.align 32
	.type	RNr19, @object
	.size	RNr19, 144
RNr19:
	.long	3229950755
	.long	2569918203
	.long	2490793552
	.long	-1073144629
	.long	2615596236
	.long	2788067735
	.long	3264399824
	.long	1074279102
	.long	3180300812
	.long	3703160687
	.long	305083176
	.long	-1073230108
	.long	2721485166
	.long	1330857716
	.long	2512083747
	.long	1074023745
	.long	3716141543
	.long	1519741534
	.long	2994698519
	.long	-1073517078
	.long	19170757
	.long	2507081579
	.long	3798154250
	.long	1073733349
	.long	3620996061
	.long	997883749
	.long	4039310478
	.long	-1073673381
	.long	278014545
	.long	48225470
	.long	1664481197
	.long	1073643965
	.long	3455414385
	.long	9244791
	.long	2304956969
	.long	-1074044549
	.align 32
	.type	RDr18, @object
	.size	RDr18, 128
RDr18:
	.long	983287897
	.long	4004759685
	.long	2473992105
	.long	1074413488
	.long	3374202419
	.long	3677681766
	.long	4173576508
	.long	1074334602
	.long	4241998114
	.long	562733159
	.long	4043295542
	.long	1074352181
	.long	3129984264
	.long	1909808321
	.long	1282684699
	.long	1074241565
	.long	2746605644
	.long	629744247
	.long	4116540732
	.long	1074205789
	.long	4220414829
	.long	4008407276
	.long	787832533
	.long	1074061274
	.long	1394766051
	.long	610344730
	.long	823094603
	.long	1073981755
	.long	4061473362
	.long	1044950725
	.long	604316314
	.long	1073782854
	.align 32
	.type	RNr18, @object
	.size	RNr18, 144
RNr18:
	.long	840070466
	.long	1582269473
	.long	3751832629
	.long	-1073131646
	.long	2906754391
	.long	1597229124
	.long	1857857396
	.long	1074279004
	.long	3324036698
	.long	1209433339
	.long	1832778383
	.long	-1073232901
	.long	2538759575
	.long	2614288298
	.long	3017504154
	.long	1073907570
	.long	1604492972
	.long	2530739624
	.long	604919714
	.long	-1073498899
	.long	26044799
	.long	1647584227
	.long	1068851722
	.long	1073808100
	.long	3090648346
	.long	3611168824
	.long	2576523092
	.long	-1073661131
	.long	416803164
	.long	2556969603
	.long	2260018110
	.long	1073622992
	.long	1910594759
	.long	175256696
	.long	1169996439
	.long	-1074084024
	.align 32
	.type	RDr17, @object
	.size	RDr17, 128
RDr17:
	.long	2320241942
	.long	2948507488
	.long	2752008630
	.long	1074419598
	.long	818401208
	.long	286493594
	.long	1348668372
	.long	1074331163
	.long	1760505413
	.long	2914800887
	.long	3816936867
	.long	1074354998
	.long	1922621742
	.long	3647649901
	.long	473465519
	.long	1074234966
	.long	3016866574
	.long	4079899173
	.long	4078570008
	.long	1074206324
	.long	317967366
	.long	4179957411
	.long	3440280998
	.long	1074052214
	.long	2867169023
	.long	741191507
	.long	4233508387
	.long	1073981284
	.long	3012420948
	.long	3575915097
	.long	1616127336
	.long	1073774097
	.align 32
	.type	RNr17, @object
	.size	RNr17, 144
RNr17:
	.long	2850698259
	.long	1588189426
	.long	1713649421
	.long	-1073104444
	.long	2204653472
	.long	2371051300
	.long	4110560792
	.long	1074286255
	.long	2645987911
	.long	2048541729
	.long	1462827353
	.long	-1073227105
	.long	2914936680
	.long	3490094794
	.long	2661683020
	.long	-1073517603
	.long	2308861501
	.long	1595941974
	.long	1876087097
	.long	-1073472692
	.long	1723059977
	.long	2056394861
	.long	4145646812
	.long	1073856330
	.long	2935636323
	.long	4180145143
	.long	632909171
	.long	-1073646671
	.long	4083588062
	.long	862471962
	.long	2141001253
	.long	1073574888
	.long	121892186
	.long	693436284
	.long	3437050470
	.long	-1074206747
	.align 32
	.type	RDr16, @object
	.size	RDr16, 128
RDr16:
	.long	999283446
	.long	185248990
	.long	1363397832
	.long	1074430053
	.long	1056065637
	.long	2542291762
	.long	10564151
	.long	1074312326
	.long	474239095
	.long	2315707646
	.long	1113026368
	.long	1074359454
	.long	1809012065
	.long	775439879
	.long	3635267798
	.long	1074217738
	.long	3421525655
	.long	2062407580
	.long	2803503950
	.long	1074206822
	.long	3268585158
	.long	4056905219
	.long	192640877
	.long	1074029882
	.long	2297926053
	.long	3266992374
	.long	621553167
	.long	1073979913
	.long	3132737197
	.long	1271801018
	.long	638158430
	.long	1073753015
	.align 32
	.type	RNr16, @object
	.size	RNr16, 144
RNr16:
	.long	2858183192
	.long	492910799
	.long	1778899956
	.long	-1073076868
	.long	2200554172
	.long	1647669363
	.long	4125130798
	.long	1074303085
	.long	4109999572
	.long	731602577
	.long	1075343489
	.long	-1073215778
	.long	2325466752
	.long	1358787689
	.long	809436957
	.long	-1073453635
	.long	451117080
	.long	2911879028
	.long	2521637785
	.long	-1073445898
	.long	1642419341
	.long	3705764193
	.long	1378179202
	.long	1073900821
	.long	1296249004
	.long	951590342
	.long	1667925557
	.long	-1073631534
	.long	2410344494
	.long	2902352798
	.long	251558549
	.long	1073486015
	.long	807169113
	.long	3369775385
	.long	86138240
	.long	1073302385
	.align 32
	.type	RDr15, @object
	.size	RDr15, 128
RDr15:
	.long	3096000333
	.long	461541801
	.long	448167533
	.long	1074427206
	.long	1011892578
	.long	2185662926
	.long	3278411680
	.long	1074268265
	.long	934239168
	.long	1467249611
	.long	2351050256
	.long	1074353385
	.long	3175941571
	.long	684098411
	.long	1925662656
	.long	1074167895
	.long	2442572076
	.long	2978338212
	.long	2010803470
	.long	1074201309
	.long	1608892765
	.long	709972074
	.long	3090728761
	.long	1073980569
	.long	3627582561
	.long	564044377
	.long	1651457249
	.long	1073973479
	.long	1627075693
	.long	2287355517
	.long	3365407568
	.long	1073701114
	.align 32
	.type	RNr15, @object
	.size	RNr15, 144
RNr15:
	.long	289535974
	.long	1663417864
	.long	3424705379
	.long	-1073068026
	.long	525416116
	.long	2009370550
	.long	42861592
	.long	1074309041
	.long	3121980836
	.long	668147784
	.long	1956230658
	.long	-1073215409
	.long	1036344104
	.long	1857382044
	.long	2575586664
	.long	-1073465431
	.long	1551077240
	.long	3507563141
	.long	2447947424
	.long	-1073418223
	.long	3754266300
	.long	2873637317
	.long	309579610
	.long	1073927727
	.long	4208825755
	.long	3538813687
	.long	3716485335
	.long	-1073627770
	.long	535192662
	.long	3052388126
	.long	3992219974
	.long	1073172794
	.long	3603362715
	.long	2492315104
	.long	1239963847
	.long	1073301013
	.align 32
	.type	RDr14, @object
	.size	RDr14, 128
RDr14:
	.long	4002477837
	.long	692118179
	.long	4286616762
	.long	1074411493
	.long	2878869123
	.long	4055161676
	.long	3143200921
	.long	1074197685
	.long	3066835944
	.long	2408770298
	.long	635239790
	.long	1074340290
	.long	1478492634
	.long	2819826960
	.long	64499212
	.long	1074101184
	.long	39829961
	.long	2053645137
	.long	877863795
	.long	1074186046
	.long	3404857395
	.long	1428456951
	.long	3354104313
	.long	1073919401
	.long	4224021831
	.long	388688333
	.long	1909971857
	.long	1073966161
	.long	2521656344
	.long	3973196549
	.long	2289828433
	.long	1073645456
	.align 32
	.type	RNr14, @object
	.size	RNr14, 144
RNr14:
	.long	1357033755
	.long	2525006112
	.long	1067406496
	.long	-1073073723
	.long	277329303
	.long	1830543558
	.long	182766131
	.long	1074286576
	.long	1505173136
	.long	1169803446
	.long	3251866094
	.long	-1073231319
	.long	772307209
	.long	3488334915
	.long	1262226082
	.long	-1073513151
	.long	3453815058
	.long	4070548511
	.long	4276106299
	.long	-1073406780
	.long	1162072907
	.long	3371210613
	.long	3874959015
	.long	1073914970
	.long	3178062062
	.long	456072134
	.long	708561868
	.long	-1073640430
	.long	411245026
	.long	1661986557
	.long	614544886
	.long	-1074149212
	.long	526809897
	.long	2508576772
	.long	2450847558
	.long	-1074172563
	.align 32
	.type	RDr13, @object
	.size	RDr13, 128
RDr13:
	.long	2692496259
	.long	231397827
	.long	1552552570
	.long	1074402702
	.long	1310955621
	.long	3442244590
	.long	2675118294
	.long	1074166739
	.long	1925418763
	.long	2240509343
	.long	127071922
	.long	1074334074
	.long	3009316993
	.long	2196092923
	.long	693123640
	.long	1074080738
	.long	1875391222
	.long	3723493553
	.long	4127987251
	.long	1074179419
	.long	3444936873
	.long	161623581
	.long	773974848
	.long	1073899123
	.long	2252738804
	.long	2220456610
	.long	740611250
	.long	1073963459
	.long	2266314876
	.long	1467315338
	.long	304374174
	.long	1073630015
	.align 32
	.type	RNr13, @object
	.size	RNr13, 144
RNr13:
	.long	665596174
	.long	508939852
	.long	2693479308
	.long	-1073076682
	.long	3907032953
	.long	2771554796
	.long	2951499188
	.long	1074234141
	.long	619430391
	.long	3987181719
	.long	2110705892
	.long	-1073249073
	.long	3616370188
	.long	700473328
	.long	3178973120
	.long	-1073523508
	.long	1246302161
	.long	2344497090
	.long	3150698685
	.long	-1073396476
	.long	3024339904
	.long	169239349
	.long	2105298105
	.long	1073874137
	.long	364345269
	.long	1901535183
	.long	2592202752
	.long	-1073659358
	.long	2086290685
	.long	268448762
	.long	4021632433
	.long	-1074111365
	.long	2084776121
	.long	1835509902
	.long	26280394
	.long	-1074050120
	.align 32
	.type	TD2, @object
	.size	TD2, 144
TD2:
	.long	266078837
	.long	2486675500
	.long	248408338
	.long	1074597497
	.long	1813872948
	.long	3994406246
	.long	2487304205
	.long	1074559494
	.long	1521205582
	.long	1340101244
	.long	99718143
	.long	1074555596
	.long	3215235485
	.long	4082913624
	.long	3067989774
	.long	1074483492
	.long	12869469
	.long	3883108279
	.long	2829941393
	.long	1074420967
	.long	2233513439
	.long	1696316014
	.long	3776621083
	.long	1074315639
	.long	811739189
	.long	427993635
	.long	3531495707
	.long	1074211174
	.long	1897927612
	.long	3961091383
	.long	4241769877
	.long	1074064346
	.long	3205157491
	.long	728958216
	.long	4110081157
	.long	1073903902
	.align 32
	.type	TN2, @object
	.size	TN2, 144
TN2:
	.long	559576126
	.long	2203598170
	.long	1979963374
	.long	-1073461476
	.long	2339595686
	.long	1630086857
	.long	3190692158
	.long	1074511696
	.long	1481819476
	.long	2788504022
	.long	4214232137
	.long	-1073081871
	.long	2332509333
	.long	2914117387
	.long	2565511827
	.long	1074401413
	.long	1358362473
	.long	3041647630
	.long	2635589808
	.long	1074207963
	.long	3782326852
	.long	1255733858
	.long	2403192122
	.long	1074152978
	.long	2941156198
	.long	216981131
	.long	1714582346
	.long	1073845333
	.long	3370307573
	.long	1976301194
	.long	278142751
	.long	1073973266
	.long	3724067954
	.long	2024352594
	.long	1267042661
	.long	1073612425
	.align 32
	.type	TD1, @object
	.size	TD1, 144
TD1:
	.long	282059975
	.long	4173592157
	.long	1268283036
	.long	-1071310875
	.long	2586009486
	.long	1916018477
	.long	2282588685
	.long	-1071385560
	.long	1673603426
	.long	271001740
	.long	58549099
	.long	-1071538732
	.long	3850380146
	.long	454858369
	.long	4054716715
	.long	-1071734963
	.long	796010417
	.long	2162239801
	.long	445337703
	.long	-1071966933
	.long	3282515045
	.long	3983027660
	.long	3484870077
	.long	-1072236401
	.long	1268562004
	.long	3010348331
	.long	31222417
	.long	-1072549643
	.long	3762865901
	.long	394406292
	.long	4055884505
	.long	-1072908759
	.long	356230184
	.long	1306276942
	.long	1248208925
	.long	-1073355841
	.align 32
	.type	TN1, @object
	.size	TN1, 144
TN1:
	.long	2176826168
	.long	3750548308
	.long	1387090299
	.long	-1071505546
	.long	698934898
	.long	1861385905
	.long	189423225
	.long	1076061413
	.long	2225972816
	.long	2023181388
	.long	3119951013
	.long	1075872787
	.long	2977383696
	.long	3274250810
	.long	2435635792
	.long	1075731569
	.long	3354236556
	.long	4227055104
	.long	1359118769
	.long	1075466372
	.long	2183578764
	.long	1812382487
	.long	2957689338
	.long	1075241004
	.long	247773449
	.long	3692292137
	.long	4013658289
	.long	1074891101
	.long	2244475408
	.long	1346198791
	.long	1169342176
	.long	1074594018
	.long	2958277073
	.long	1685856604
	.long	4236863383
	.long	1074007876
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2086290685
	.long	268448762
	.long	4021632433
	.long	-1074111365
	.align 16
.LC1:
	.long	2084776121
	.long	1835509902
	.long	26280394
	.long	-1074050120
	.align 16
.LC2:
	.long	2252738804
	.long	2220456610
	.long	740611250
	.long	1073963459
	.align 16
.LC3:
	.long	411245026
	.long	1661986557
	.long	614544886
	.long	-1074149212
	.align 16
.LC4:
	.long	526809897
	.long	2508576772
	.long	2450847558
	.long	-1074172563
	.align 16
.LC5:
	.long	4224021831
	.long	388688333
	.long	1909971857
	.long	1073966161
	.align 16
.LC6:
	.long	535192662
	.long	3052388126
	.long	3992219974
	.long	1073172794
	.align 16
.LC7:
	.long	3603362715
	.long	2492315104
	.long	1239963847
	.long	1073301013
	.align 16
.LC8:
	.long	3627582561
	.long	564044377
	.long	1651457249
	.long	1073973479
	.align 16
.LC9:
	.long	2410344494
	.long	2902352798
	.long	251558549
	.long	1073486015
	.align 16
.LC10:
	.long	807169113
	.long	3369775385
	.long	86138240
	.long	1073302385
	.align 16
.LC11:
	.long	2297926053
	.long	3266992374
	.long	621553167
	.long	1073979913
	.align 16
.LC12:
	.long	4083588062
	.long	862471962
	.long	2141001253
	.long	1073574888
	.align 16
.LC13:
	.long	121892186
	.long	693436284
	.long	3437050470
	.long	-1074206747
	.align 16
.LC14:
	.long	2867169023
	.long	741191507
	.long	4233508387
	.long	1073981284
	.align 16
.LC15:
	.long	416803164
	.long	2556969603
	.long	2260018110
	.long	1073622992
	.align 16
.LC16:
	.long	1910594759
	.long	175256696
	.long	1169996439
	.long	-1074084024
	.align 16
.LC17:
	.long	1394766051
	.long	610344730
	.long	823094603
	.long	1073981755
	.align 16
.LC18:
	.long	278014545
	.long	48225470
	.long	1664481197
	.long	1073643965
	.align 16
.LC19:
	.long	3455414385
	.long	9244791
	.long	2304956969
	.long	-1074044549
	.align 16
.LC20:
	.long	2483520086
	.long	1786281980
	.long	2912990722
	.long	1073987645
	.align 16
.LC21:
	.long	2748438882
	.long	3834721168
	.long	1677600535
	.long	1073644487
	.align 16
.LC22:
	.long	1375531053
	.long	3229016163
	.long	1893590572
	.long	-1074034254
	.align 16
.LC23:
	.long	4070177078
	.long	4064786526
	.long	3181720989
	.long	1074001193
	.align 16
.LC24:
	.long	3085734172
	.long	3376702612
	.long	652664368
	.long	-1073657866
	.align 16
.LC25:
	.long	2856477196
	.long	4171140881
	.long	621168806
	.long	-1073788298
	.align 16
.LC26:
	.long	1284081952
	.long	2361216278
	.long	2910681733
	.long	1073952994
	.align 16
.LC27:
	.long	4173918531
	.long	805857339
	.long	1109432338
	.long	-1073567721
	.align 16
.LC28:
	.long	4212603917
	.long	2844622006
	.long	741559444
	.long	-1073769845
	.align 16
.LC29:
	.long	2292544762
	.long	1190726065
	.long	2335949307
	.long	1074075072
	.align 16
.LC30:
	.long	299744570
	.long	1850928023
	.long	2884078513
	.long	-1073505745
	.align 16
.LC31:
	.long	2877106353
	.long	1864440645
	.long	438938254
	.long	-1073755718
	.align 16
.LC32:
	.long	381416649
	.long	369014653
	.long	2130427748
	.long	1074157776
	.align 16
.LC33:
	.long	1091455653
	.long	2587852596
	.long	1821053371
	.long	1074407932
	.align 16
.LC34:
	.long	2202865610
	.long	1431220795
	.long	2881691653
	.long	1074178431
	.align 16
.LC35:
	.long	208359088
	.long	2992115458
	.long	1461992846
	.long	-1073062063
	.align 16
.LC36:
	.long	2946253495
	.long	2854616410
	.long	1144263533
	.long	-1073425253
	.align 16
.LC37:
	.long	2448204785
	.long	1762089962
	.long	2557436825
	.long	-1073739278
	.align 16
.LC38:
	.long	1862174987
	.long	4214626901
	.long	371155820
	.long	1074271878
	.align 16
.LC39:
	.long	2318794642
	.long	408815043
	.long	2545998254
	.long	1074529736
	.align 16
.LC40:
	.long	23021965
	.long	3811171597
	.long	2280834395
	.long	1074250417
	.align 16
.LC41:
	.long	207865816
	.long	2988500113
	.long	1161976887
	.long	-1072954831
	.align 16
.LC42:
	.long	1202567024
	.long	2902447826
	.long	2337494919
	.long	1074594656
	.align 16
.LC43:
	.long	1521932787
	.long	3680606074
	.long	183205061
	.long	1074286341
	.align 16
.LC44:
	.long	3432714260
	.long	2536195016
	.long	388474595
	.long	-1072901818
	.align 16
.LC45:
	.long	4168439028
	.long	969362888
	.long	3830019792
	.long	1073878965
	.align 16
.LC46:
	.long	588497985
	.long	3004344865
	.long	8714342
	.long	1073385338
	.align 16
.LC47:
	.long	131732523
	.long	1433465577
	.long	2812285745
	.long	1074320819
	.align 16
.LC48:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC49:
	.long	0
	.long	0
	.long	0
	.long	1073872896
	.align 16
.LC50:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC51:
	.long	2266314876
	.long	1467315338
	.long	304374174
	.long	1073630015
	.align 16
.LC52:
	.long	3848792402
	.long	3527668866
	.long	1766833021
	.long	1072584942
	.align 16
.LC53:
	.long	0
	.long	0
	.long	0
	.long	1073640068
	.align 16
.LC54:
	.long	0
	.long	0
	.long	0
	.long	1073577984
	.align 16
.LC55:
	.long	2521656344
	.long	3973196549
	.long	2289828433
	.long	1073645456
	.align 16
.LC56:
	.long	2558138570
	.long	3816833954
	.long	1862679084
	.long	1072600740
	.align 16
.LC57:
	.long	0
	.long	0
	.long	0
	.long	1073623318
	.align 16
.LC58:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC59:
	.long	1627075693
	.long	2287355517
	.long	3365407568
	.long	1073701114
	.align 16
.LC60:
	.long	3138049660
	.long	4274113436
	.long	1898110274
	.long	1072564798
	.align 16
.LC61:
	.long	0
	.long	0
	.long	0
	.long	1073605376
	.align 16
.LC62:
	.long	0
	.long	0
	.long	0
	.long	1073627136
	.align 16
.LC63:
	.long	3132737197
	.long	1271801018
	.long	638158430
	.long	1073753015
	.align 16
.LC64:
	.long	3266713238
	.long	2101198717
	.long	3789121481
	.long	1072505957
	.align 16
.LC65:
	.long	0
	.long	0
	.long	0
	.long	1073578444
	.align 16
.LC66:
	.long	0
	.long	0
	.long	0
	.long	1073643520
	.align 16
.LC67:
	.long	3012420948
	.long	3575915097
	.long	1616127336
	.long	1073774097
	.align 16
.LC68:
	.long	3937559099
	.long	4081274126
	.long	790235450
	.long	1072588966
	.align 16
.LC69:
	.long	0
	.long	0
	.long	0
	.long	1073555396
	.align 16
.LC70:
	.long	0
	.long	0
	.long	0
	.long	1073659904
	.align 16
.LC71:
	.long	4061473362
	.long	1044950725
	.long	604316314
	.long	1073782854
	.align 16
.LC72:
	.long	3804534314
	.long	1152863563
	.long	1191421086
	.long	1072608942
	.align 16
.LC73:
	.long	0
	.long	0
	.long	0
	.long	1073527344
	.align 16
.LC74:
	.long	62527580
	.long	1649004427
	.long	1232417159
	.long	1073794398
	.align 16
.LC75:
	.long	4074310746
	.long	4277076548
	.long	4017574802
	.long	1072596363
	.align 16
.LC76:
	.long	0
	.long	0
	.long	0
	.long	1073496608
	.align 16
.LC77:
	.long	0
	.long	0
	.long	0
	.long	1073684480
	.align 16
.LC78:
	.long	1795572320
	.long	3324554926
	.long	1190441894
	.long	1073814407
	.align 16
.LC79:
	.long	3494506502
	.long	1993657251
	.long	1631183275
	.long	1072573821
	.align 16
.LC80:
	.long	0
	.long	0
	.long	0
	.long	1073465632
	.align 16
.LC81:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC82:
	.long	2267459602
	.long	1868678748
	.long	3453581761
	.long	162998
	.align 16
.LC83:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC84:
	.long	2803993979
	.long	3755256967
	.long	2777513288
	.long	1073884959
	.align 16
.LC85:
	.long	1455096050
	.long	2554530791
	.long	2066603635
	.long	1073945628
	.align 16
.LC86:
	.long	2751929419
	.long	2619096035
	.long	3732132099
	.long	1073988236
	.align 16
.LC87:
	.long	325952384
	.long	3181587193
	.long	829535598
	.long	1074138313
	.align 16
.LC88:
	.long	1606055207
	.long	1513571989
	.long	153391221
	.long	1074043744
	.align 16
.LC89:
	.long	1777528580
	.long	281185044
	.long	2832206588
	.long	1074198380
	.align 16
.LC90:
	.long	706380816
	.long	3166793199
	.long	564976909
	.long	1074224941
	.align 16
.LC91:
	.long	359439791
	.long	1025893849
	.long	4128877303
	.long	1074064322
	.align 16
.LC92:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC93:
	.long	0
	.long	0
	.long	0
	.long	1073618944
	.align 16
.LC95:
	.long	2244475408
	.long	1346198791
	.long	1169342176
	.long	1074594018
	.align 16
.LC96:
	.long	2958277073
	.long	1685856604
	.long	4236863383
	.long	1074007876
	.align 16
.LC97:
	.long	3762865901
	.long	394406292
	.long	4055884505
	.long	-1072908759
	.align 16
.LC98:
	.long	3370307573
	.long	1976301194
	.long	278142751
	.long	1073973266
	.align 16
.LC99:
	.long	3724067954
	.long	2024352594
	.long	1267042661
	.long	1073612425
	.align 16
.LC100:
	.long	1897927612
	.long	3961091383
	.long	4241769877
	.long	1074064346
	.align 16
.LC101:
	.long	0
	.long	0
	.long	0
	.long	1073938432
	.align 16
.LC102:
	.long	2817966060
	.long	2295799112
	.long	2820754870
	.long	1073743595
	.align 16
.LC103:
	.long	0
	.long	0
	.long	0
	.long	1073414144
	.align 16
.LC104:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC105:
	.long	2817966060
	.long	2295799112
	.long	2820754870
	.long	1073481451
	.align 16
.LC106:
	.long	356230184
	.long	1306276942
	.long	1248208925
	.long	1074127807
	.align 16
.LC107:
	.long	3205157491
	.long	728958216
	.long	4110081157
	.long	1073903902
	.align 16
.LC108:
	.long	0
	.long	0
	.long	369098752
	.long	1073655980
