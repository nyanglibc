	.text
	.p2align 4,,15
	.globl	__cargl
	.type	__cargl, @function
__cargl:
	fldt	24(%rsp)
	fldt	8(%rsp)
	fstpt	24(%rsp)
	fstpt	8(%rsp)
	jmp	__atan2l@PLT
	.size	__cargl, .-__cargl
	.weak	cargf64x
	.set	cargf64x,__cargl
	.weak	cargl
	.set	cargl,__cargl
