	.text
	.globl	__multf3
	.globl	__trunctfdf2
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__netf2
	.p2align 4,,15
	.globl	__f64mulf128
	.type	__f64mulf128, @function
__f64mulf128:
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebp
	movaps	%xmm0, 16(%rsp)
	movl	%ebp, %eax
	andl	$-32704, %eax
	orl	$32640, %eax
	movaps	%xmm1, 32(%rsp)
	movl	%eax, 76(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %eax
	orl	%ebp, %eax
	movl	%eax, 76(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %ebp
	notl	%ebp
	testl	%edi, %ebp
	jne	.L23
.L2:
	shrl	$5, %ebx
	movq	%xmm0, %rax
	movabsq	$-4294967296, %rdx
	andl	$1, %ebx
	orl	(%rsp), %ebx
	andq	%rdx, %rax
	orq	%rbx, %rax
	movq	%rax, (%rsp)
	movdqa	(%rsp), %xmm0
	call	__trunctfdf2@PLT
	movapd	%xmm0, %xmm2
	andpd	.LC0(%rip), %xmm0
	movsd	.LC1(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L19
	ucomisd	%xmm2, %xmm2
	movsd	%xmm2, (%rsp)
	jp	.L24
	movdqa	16(%rsp), %xmm3
	pand	.LC2(%rip), %xmm3
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	jne	.L1
	movdqa	16(%rsp), %xmm3
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	jg	.L1
	movdqa	32(%rsp), %xmm3
	pand	.LC2(%rip), %xmm3
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	jne	.L1
	movdqa	16(%rsp), %xmm3
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	jg	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$88, %rsp
	movapd	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pxor	%xmm0, %xmm0
	movl	$0, %eax
	pxor	%xmm1, %xmm1
	movsd	%xmm2, (%rsp)
	ucomisd	%xmm0, %xmm2
	movdqa	32(%rsp), %xmm0
	setnp	%bl
	cmovne	%eax, %ebx
	call	__netf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	setne	%al
	testb	%al, %bl
	je	.L1
	pxor	%xmm1, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	je	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm2
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movaps	%xmm0, 48(%rsp)
	call	__GI___feraiseexcept
	movdqa	48(%rsp), %xmm0
	jmp	.L2
	.size	__f64mulf128, .-__f64mulf128
	.weak	f32xmulf128
	.set	f32xmulf128,__f64mulf128
	.weak	f64mulf128
	.set	f64mulf128,__f64mulf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
