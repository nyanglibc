	.text
#APP
	.symver __ieee754_sqrtf,__sqrtf_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_sqrtf
	.type	__ieee754_sqrtf, @function
__ieee754_sqrtf:
	sqrtss	%xmm0, %xmm0
	ret
	.size	__ieee754_sqrtf, .-__ieee754_sqrtf
