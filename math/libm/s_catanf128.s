	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__letf2
	.globl	__divtf3
	.globl	__multf3
	.globl	__lttf2
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__catanf128
	.type	__catanf128, @function
__catanf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$112, %rsp
	movdqa	128(%rsp), %xmm3
	pand	.LC2(%rip), %xmm3
	movdqa	144(%rsp), %xmm5
	movdqa	.LC2(%rip), %xmm4
	pand	%xmm5, %xmm4
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm5, (%rsp)
	movaps	%xmm3, 32(%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L115
	movdqa	.LC3(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L116
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L108
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
.L108:
	pxor	%xmm0, %xmm0
	movdqa	(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC5(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 48(%rsp)
.L6:
	movq	%rbx, %rax
	movdqa	48(%rsp), %xmm6
	movdqa	(%rsp), %xmm7
	movaps	%xmm6, (%rbx)
	movaps	%xmm7, 16(%rbx)
	addq	$112, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	movdqa	.LC4(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L89
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L113
.L90:
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L51
.L52:
	movdqa	.LC6(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L19
	movdqa	.LC6(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L19
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L60
	movdqa	16(%rsp), %xmm5
	movdqa	32(%rsp), %xmm6
	movaps	%xmm5, 48(%rsp)
	movaps	%xmm6, (%rsp)
.L27:
	movdqa	.LC10(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L97
	movdqa	(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	pxor	%xmm1, %xmm1
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L111
	pxor	%xmm5, %xmm5
	movaps	%xmm5, (%rsp)
.L111:
	movdqa	.LC7(%rip), %xmm2
	movdqa	.LC8(%rip), %xmm7
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm7, 80(%rsp)
.L30:
	movdqa	128(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	.LC13(%rip), %xmm2
	jne	.L36
	movdqa	%xmm2, %xmm1
	movdqa	32(%rsp), %xmm0
	movaps	%xmm2, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	js	.L117
.L36:
	movdqa	%xmm2, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L101
	movdqa	128(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
.L39:
	movdqa	.LC7(%rip), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	144(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L102
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__ieee754_logf128@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	128(%rsp), %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L118
	movdqa	.LC7(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L96
	movdqa	128(%rsp), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	128(%rsp), %xmm1
	call	__divtf3@PLT
	movaps	%xmm0, (%rsp)
.L24:
	movdqa	48(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L43
	movdqa	48(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L43:
	movdqa	(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L6
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L89:
	pxor	%xmm1, %xmm1
	movdqa	128(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	16(%rsp), %xmm0
	testq	%rax, %rax
	movdqa	%xmm0, %xmm1
	jne	.L91
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L119
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L51
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L52
	pxor	%xmm1, %xmm1
	movdqa	144(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	128(%rsp), %xmm2
	testq	%rax, %rax
	movaps	%xmm2, 48(%rsp)
	je	.L6
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L118:
	movdqa	144(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L91:
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L90
.L112:
	movdqa	.LC0(%rip), %xmm7
	movaps	%xmm7, (%rsp)
	movaps	%xmm7, 48(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L60:
	movdqa	32(%rsp), %xmm6
	movdqa	16(%rsp), %xmm2
	movaps	%xmm6, 48(%rsp)
	movaps	%xmm2, (%rsp)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L113:
	movdqa	.LC0(%rip), %xmm6
	movaps	%xmm6, (%rsp)
	movaps	%xmm6, 48(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L96:
	movdqa	.LC8(%rip), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	128(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_hypotf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	144(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__divtf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L97:
	movdqa	.LC7(%rip), %xmm7
	movdqa	(%rsp), %xmm0
	movdqa	%xmm7, %xmm1
	movaps	%xmm7, 64(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L98
	movdqa	(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC8(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L115:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L113
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L120
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L121
	movdqa	.LC0(%rip), %xmm5
	movaps	%xmm5, (%rsp)
	movaps	%xmm5, 48(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L101:
	pxor	%xmm2, %xmm2
	movaps	%xmm2, (%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L102:
	movdqa	.LC15(%rip), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__divtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L98:
	movdqa	.LC11(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	movdqa	.LC8(%rip), %xmm5
	testq	%rax, %rax
	movaps	%xmm5, 80(%rsp)
	jns	.L33
	movdqa	%xmm5, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L33
	movdqa	(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm5
	movaps	%xmm0, (%rsp)
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L117:
	movdqa	144(%rsp), %xmm1
	movdqa	80(%rsp), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC14(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L33:
	movdqa	48(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__x2y2m1f128@PLT
	pxor	.LC12(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L119:
	movdqa	.LC0(%rip), %xmm2
	movaps	%xmm2, (%rsp)
	movaps	%xmm2, 48(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L51:
	movdqa	128(%rsp), %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 48(%rsp)
.L54:
	pxor	%xmm0, %xmm0
	movdqa	144(%rsp), %xmm1
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L6
.L120:
	movdqa	.LC0(%rip), %xmm2
	movaps	%xmm2, 48(%rsp)
	jmp	.L54
.L121:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L112
	pxor	%xmm0, %xmm0
	movdqa	(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	.LC0(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 48(%rsp)
	jmp	.L6
	.size	__catanf128, .-__catanf128
	.weak	catanf128
	.set	catanf128,__catanf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC5:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1081278464
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1066270720
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	1073643520
	.align 16
.LC12:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC13:
	.long	0
	.long	0
	.long	0
	.long	1058996224
	.align 16
.LC14:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC15:
	.long	0
	.long	0
	.long	0
	.long	1073807360
