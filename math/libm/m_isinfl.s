	.text
	.p2align 4,,15
	.globl	__isinfl
	.type	__isinfl, @function
__isinfl:
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rdx
	movq	%rdi, %rsi
	movswl	%dx, %edx
	shrq	$32, %rsi
	movl	%edx, %eax
	sarl	$14, %edx
	leal	-2147483648(%rsi), %ecx
	notl	%eax
	andl	$2, %edx
	andl	$32767, %eax
	orl	%edi, %ecx
	orl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	orl	%ecx, %eax
	movl	$1, %ecx
	sarl	$31, %eax
	subl	%edx, %ecx
	notl	%eax
	andl	%ecx, %eax
	ret
	.size	__isinfl, .-__isinfl
	.weak	isinfl
	.set	isinfl,__isinfl
