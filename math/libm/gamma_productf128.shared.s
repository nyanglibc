	.text
	.globl	__divtf3
	.globl	__floatsitf
	.globl	__addtf3
	.globl	__multf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__gamma_productf128
	.type	__gamma_productf128, @function
__gamma_productf128:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbp
	movl	%edi, %ebp
	pushq	%rbx
	subq	$176, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 168(%rsp)
# 0 "" 2
#NO_APP
	movl	168(%rsp), %r12d
	movaps	%xmm0, 128(%rsp)
	movl	%r12d, %eax
	andb	$-97, %ah
	cmpl	%eax, %r12d
	movl	%eax, 172(%rsp)
	movaps	%xmm1, 144(%rsp)
	jne	.L15
	movdqa	128(%rsp), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__divtf3@PLT
	cmpl	$1, %ebp
	movdqa	%xmm0, %xmm2
	movaps	%xmm0, 0(%r13)
	jle	.L8
	xorl	%r14d, %r14d
.L3:
	movl	$1, %ebx
	movdqa	128(%rsp), %xmm4
	movaps	%xmm4, (%rsp)
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ebx, %edi
	addl	$1, %ebx
	movaps	%xmm2, 112(%rsp)
	call	__floatsitf@PLT
	movdqa	128(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 96(%rsp)
	movdqa	48(%rsp), %xmm0
	movdqa	80(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	144(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	112(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm3
	cmpl	%ebx, %ebp
	movdqa	%xmm0, %xmm2
	movaps	%xmm3, (%rsp)
	jne	.L6
	testb	%r14b, %r14b
	movaps	%xmm0, 0(%r13)
	jne	.L4
.L1:
	movdqa	32(%rsp), %xmm0
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	128(%rsp), %xmm5
	movaps	%xmm5, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 172(%rsp)
# 0 "" 2
#NO_APP
	movdqa	%xmm0, %xmm1
	movl	$1, %r14d
	movdqa	144(%rsp), %xmm0
	call	__divtf3@PLT
	cmpl	$1, %ebp
	movdqa	%xmm0, %xmm2
	movaps	%xmm0, 0(%r13)
	jg	.L3
	movdqa	128(%rsp), %xmm6
	movaps	%xmm6, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L4:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 172(%rsp)
# 0 "" 2
#NO_APP
	andl	$24576, %r12d
	movl	172(%rsp), %eax
	andb	$-97, %ah
	orl	%eax, %r12d
	movl	%r12d, 172(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 172(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	__gamma_productf128, .-__gamma_productf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	8388608
	.long	0
	.long	1077411840
