	.text
	.p2align 4,,15
	.globl	__fmaxmagf
	.type	__fmaxmagf, @function
__fmaxmagf:
	movss	.LC0(%rip), %xmm2
	movaps	%xmm0, %xmm3
	andps	%xmm2, %xmm3
	andps	%xmm1, %xmm2
	ucomiss	%xmm2, %xmm3
	ja	.L1
	ucomiss	%xmm3, %xmm2
	ja	.L19
	ucomiss	%xmm2, %xmm3
	jp	.L5
	je	.L21
.L5:
	movd	%xmm0, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	ja	.L8
	movd	%xmm1, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	ja	.L8
	movaps	%xmm1, %xmm2
	cmpordss	%xmm1, %xmm2
	andps	%xmm2, %xmm1
	andnps	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	orps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movaps	%xmm1, %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L21:
	maxss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	addss	%xmm1, %xmm0
	ret
	.size	__fmaxmagf, .-__fmaxmagf
	.weak	fmaxmagf32
	.set	fmaxmagf32,__fmaxmagf
	.weak	fmaxmagf
	.set	fmaxmagf,__fmaxmagf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
