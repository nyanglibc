	.text
	.p2align 4,,15
	.globl	__setpayloadsig
	.type	__setpayloadsig, @function
__setpayloadsig:
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$52, %rdx
	leal	-1023(%rdx), %ecx
	cmpl	$50, %ecx
	ja	.L2
	movl	$1075, %ecx
	subl	%edx, %ecx
	movq	$-1, %rdx
	salq	%cl, %rdx
	notq	%rdx
	testq	%rax, %rdx
	jne	.L2
	testq	%rax, %rax
	movq	.LC0(%rip), %rdx
	je	.L5
	movabsq	$4503599627370495, %rdx
	andq	%rdx, %rax
	addq	$1, %rdx
	orq	%rdx, %rax
	movabsq	$9218868437227405312, %rdx
	shrq	%cl, %rax
	orq	%rax, %rdx
.L5:
	movq	%rdx, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	$0x000000000, (%rdi)
	movl	$1, %eax
	ret
	.size	__setpayloadsig, .-__setpayloadsig
	.weak	setpayloadsigf32x
	.set	setpayloadsigf32x,__setpayloadsig
	.weak	setpayloadsigf64
	.set	setpayloadsigf64,__setpayloadsig
	.weak	setpayloadsig
	.set	setpayloadsig,__setpayloadsig
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146435072
