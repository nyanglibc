	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__acosf
	.type	__acosf, @function
__acosf:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L7
.L2:
	jmp	__ieee754_acosf@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$24, %rsp
	movl	$1, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	movss	12(%rsp), %xmm0
	movl	$101, %edi
	movaps	%xmm0, %xmm1
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	__acosf, .-__acosf
	.weak	acosf32
	.set	acosf32,__acosf
	.weak	acosf
	.set	acosf,__acosf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
