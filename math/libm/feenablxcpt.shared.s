	.text
	.p2align 4,,15
	.globl	feenableexcept
	.type	feenableexcept, @function
feenableexcept:
#APP
# 31 "../sysdeps/x86_64/fpu/feenablxcpt.c" 1
	fstcw -6(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-6(%rsp), %eax
	andl	$61, %edi
	movl	%edi, %edx
	notl	%edx
	andl	%eax, %edx
	movw	%dx, -6(%rsp)
#APP
# 36 "../sysdeps/x86_64/fpu/feenablxcpt.c" 1
	fldcw -6(%rsp)
# 0 "" 2
#NO_APP
	sall	$7, %edi
#APP
# 39 "../sysdeps/x86_64/fpu/feenablxcpt.c" 1
	stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	notl	%edi
	andl	%edi, -4(%rsp)
#APP
# 43 "../sysdeps/x86_64/fpu/feenablxcpt.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	notl	%eax
	andl	$61, %eax
	ret
	.size	feenableexcept, .-feenableexcept
