	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfp
	.type	__fromfp, @function
__fromfp:
	cmpl	$64, %esi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	ja	.L35
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$64, %esi
.L10:
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rcx
	andq	%rdx, %rcx
	je	.L36
	movq	%xmm0, %r10
	shrq	$52, %rcx
	leal	-1023(%rcx), %r8d
	shrq	$63, %r10
	leal	-2(%rsi,%r10), %r9d
	cmpl	%r9d, %r8d
	jg	.L32
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %r11
	andq	%rdx, %rax
	orq	%r11, %rax
	cmpl	$51, %r8d
	jle	.L15
	subl	$1075, %ecx
	xorl	%r12d, %r12d
	salq	%cl, %rax
.L16:
	cmpl	$1, %edi
	je	.L40
	jle	.L61
	cmpl	$3, %edi
	je	.L41
	xorl	%ecx, %ecx
	cmpl	$4, %edi
	je	.L30
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%rdx, %rdx
	js	.L23
.L26:
	leal	1(%r9), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L9
.L32:
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	%r10d, %edi
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
.L9:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$-1, %r8d
	jl	.L37
	movl	$51, %ecx
	movl	$1, %r11d
	movq	%rax, %rbp
	subl	%r8d, %ecx
	salq	%cl, %r11
	andq	%r11, %rbp
	movq	%r11, %rcx
	setne	%bl
	subq	$1, %rcx
	andq	%rax, %rcx
	movq	%rcx, %r11
	movl	$52, %ecx
	setne	%r12b
	subl	%r8d, %ecx
	shrq	%cl, %rax
	cmpl	$1, %edi
	je	.L18
	jle	.L60
	cmpl	$3, %edi
	je	.L21
	cmpl	$4, %edi
	jne	.L17
	testq	%rbp, %rbp
	je	.L38
	movq	%rax, %rcx
	andl	$1, %ecx
	orq	%r11, %rcx
	setne	%cl
	movzbl	%cl, %ecx
.L30:
	addq	%rcx, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	xorl	%ebx, %ebx
.L60:
	testl	%edi, %edi
	jne	.L17
	testq	%rdx, %rdx
	js	.L23
	testb	%bl, %bl
	jne	.L43
	testb	%r12b, %r12b
	je	.L26
.L43:
	addq	$1, %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%ebx, %ebx
.L18:
	testq	%rdx, %rdx
	jns	.L26
	testb	%bl, %bl
	jne	.L44
	testb	%r12b, %r12b
	je	.L23
.L44:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	%r9d, %r8d
	je	.L62
.L31:
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%ebx, %ebx
.L29:
	addq	%rbx, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$1, %r12d
	xorl	%eax, %eax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %edx
	movl	%r8d, %ecx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L32
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	%bl, %ebx
	jmp	.L29
.L38:
	xorl	%ecx, %ecx
	jmp	.L30
	.size	__fromfp, .-__fromfp
	.weak	fromfpf32x
	.set	fromfpf32x,__fromfp
	.weak	fromfpf64
	.set	fromfpf64,__fromfp
	.weak	fromfp
	.set	fromfp,__fromfp
