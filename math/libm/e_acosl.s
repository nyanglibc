	.text
	.p2align 4,,15
	.globl	__ieee754_acosl
	.type	__ieee754_acosl, @function
__ieee754_acosl:
	fldt	8(%rsp)
#APP
# 17 "../sysdeps/i386/fpu/e_acosl.c" 1
	fld	%st
fld1
fsubp
fld1
fadd	%st(2)
fmulp
fsqrt
fabs
fxch	%st(1)
fpatan
# 0 "" 2
#NO_APP
	ret
	.size	__ieee754_acosl, .-__ieee754_acosl
