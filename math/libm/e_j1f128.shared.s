	.text
#APP
	.symver __ieee754_j1f128,__j1f128_finite@GLIBC_2.26
	.symver __ieee754_y1f128,__y1f128_finite@GLIBC_2.26
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__netf2
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__letf2
	.globl	__multf3
	.globl	__lttf2
	.globl	__divtf3
	.globl	__subtf3
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_j1f128
	.type	__ieee754_j1f128, @function
__ieee754_j1f128:
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm4
	subq	$184, %rsp
	pand	.LC52(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movdqa	.LC53(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L107
	movdqa	.LC53(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L127
.L107:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__netf2@PLT
	testq	%rax, %rax
	pxor	%xmm2, %xmm2
	je	.L1
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$184, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	je	.L1
	movdqa	.LC54(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	movaps	%xmm2, 32(%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L129
	movdqa	32(%rsp), %xmm2
	movdqa	.LC55(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	movdqa	.LC52(%rip), %xmm0
	pand	%xmm2, %xmm0
	movdqa	.LC56(%rip), %xmm1
	movaps	%xmm2, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jns	.L8
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
.L8:
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L129:
	movdqa	.LC57(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L130
	movdqa	(%rsp), %xmm7
	leaq	80+J0_2N(%rip), %rbx
	movdqa	%xmm7, %xmm1
	leaq	-96(%rbx), %rbp
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC0(%rip), %xmm3
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm3, 48(%rsp)
	movdqa	.LC1(%rip), %xmm0
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L148:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 48(%rsp)
.L14:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L148
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	leaq	80+J0_2D(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC58(%rip), %xmm1
	leaq	-96(%rbx), %rbp
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm7
	movaps	%xmm7, 48(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L149:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 48(%rsp)
.L16:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L149
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC55(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
.L146:
	pxor	%xmm1, %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jns	.L1
	pxor	.LC59(%rip), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	144(%rsp), %rsi
	leaq	160(%rsp), %rdi
	movdqa	16(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	160(%rsp), %xmm2
	movdqa	.LC59(%rip), %xmm7
	pxor	%xmm2, %xmm7
	movdqa	144(%rsp), %xmm3
	movdqa	%xmm7, %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm2, 48(%rsp)
	movaps	%xmm3, 32(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 96(%rsp)
	movdqa	32(%rsp), %xmm3
	movdqa	48(%rsp), %xmm2
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC60(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L150
.L18:
	movdqa	.LC61(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L151
	movdqa	16(%rsp), %xmm1
	movdqa	.LC63(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC64(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L133
	movdqa	.LC65(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L134
	movdqa	.LC66(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L135
	movdqa	.LC3(%rip), %xmm3
	leaq	128+P16_IN(%rip), %rbx
	movdqa	.LC4(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L152:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 112(%rsp)
.L32:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L152
	leaq	128+P16_ID(%rip), %rbx
	movdqa	.LC67(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 112(%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L153:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
.L34:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L153
	movdqa	80(%rsp), %xmm0
	leaq	144+Q16_IN(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC6(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm7, 80(%rsp)
	movdqa	.LC7(%rip), %xmm2
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L154:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 80(%rsp)
.L36:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L154
	movaps	%xmm0, 80(%rsp)
	leaq	128+Q16_ID(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC68(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 112(%rsp)
	movdqa	80(%rsp), %xmm2
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L155:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L38:
	subq	$16, %rbx
	movaps	%xmm2, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm2
	jne	.L155
.L98:
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 112(%rsp)
.L39:
	movdqa	32(%rsp), %xmm1
	movdqa	128(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC63(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC76(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC62(%rip), %xmm1
.L144:
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__divtf3@PLT
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L133:
	movdqa	.LC76(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L137
	movdqa	.LC77(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L138
	movdqa	.LC27(%rip), %xmm6
	leaq	128+P3r2_4N(%rip), %rbx
	movdqa	.LC28(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm6, 112(%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L156:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
.L69:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L156
	leaq	128+P3r2_4D(%rip), %rbx
	movdqa	.LC78(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC29(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 112(%rsp)
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L157:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L71:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L157
	movdqa	80(%rsp), %xmm0
	leaq	128+Q3r2_4N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC30(%rip), %xmm3
	leaq	-144(%rbx), %rbp
	movdqa	.LC31(%rip), %xmm6
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm6, 80(%rsp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L158:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 112(%rsp)
.L73:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L158
	leaq	128+Q3r2_4D(%rip), %rbx
	movdqa	.LC79(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC32(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 112(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L159:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 112(%rsp)
.L75:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L159
.L81:
	movdqa	80(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 112(%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L151:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC62(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	64(%rsp), %xmm0
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L150:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__cosf128@PLT
	movdqa	144(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	160(%rsp), %xmm0
	call	__multf3@PLT
	pxor	%xmm1, %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L131
	movdqa	96(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 64(%rsp)
	jmp	.L18
.L137:
	movdqa	.LC82(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L139
	movdqa	.LC39(%rip), %xmm3
	leaq	128+P2r3_2r7N(%rip), %rbx
	movaps	%xmm3, 80(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC40(%rip), %xmm2
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L160:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 80(%rsp)
.L86:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L160
	movaps	%xmm0, 112(%rsp)
	leaq	112+P2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC83(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC41(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 80(%rsp)
	movdqa	112(%rsp), %xmm2
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L161:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 80(%rsp)
.L88:
	subq	$16, %rbx
	movaps	%xmm2, 112(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm2
	jne	.L161
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r3_2r7N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC42(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm7, 80(%rsp)
	movdqa	.LC43(%rip), %xmm2
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L162:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 80(%rsp)
.L90:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L162
	movaps	%xmm0, 112(%rsp)
	leaq	112+Q2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC84(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC44(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 80(%rsp)
	movdqa	112(%rsp), %xmm2
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L163:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 80(%rsp)
.L92:
	subq	$16, %rbx
	movaps	%xmm2, 112(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm2
	jne	.L163
	jmp	.L98
.L134:
	movdqa	.LC71(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L136
	movdqa	.LC15(%rip), %xmm3
	leaq	144+P5_8N(%rip), %rbx
	movdqa	.LC16(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L164:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L50:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L164
	leaq	144+P5_8D(%rip), %rbx
	movdqa	.LC72(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC17(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 112(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L165:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
.L52:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L165
	movdqa	80(%rsp), %xmm0
	leaq	144+Q5_8N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC18(%rip), %xmm6
	leaq	-160(%rbx), %rbp
	movdqa	.LC19(%rip), %xmm7
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm6, 112(%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L166:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L54:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L166
	leaq	144+Q5_8D(%rip), %rbx
	movdqa	.LC73(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC20(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 112(%rsp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L167:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
.L56:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L167
	jmp	.L81
.L131:
	movdqa	64(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 96(%rsp)
	jmp	.L18
.L136:
	movdqa	.LC21(%rip), %xmm6
	leaq	144+P4_5N(%rip), %rbx
	movaps	%xmm6, 80(%rsp)
	leaq	-160(%rbx), %rbp
	movdqa	.LC22(%rip), %xmm2
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L168:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 80(%rsp)
.L47:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L168
	movaps	%xmm0, 80(%rsp)
	leaq	128+P4_5D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC74(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC23(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 112(%rsp)
	movdqa	80(%rsp), %xmm2
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L169:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 112(%rsp)
.L59:
	subq	$16, %rbx
	movaps	%xmm2, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm2
	jne	.L169
	movdqa	%xmm2, %xmm0
	leaq	144+Q4_5N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC24(%rip), %xmm5
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm5, 80(%rsp)
	movdqa	.LC25(%rip), %xmm2
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L170:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 80(%rsp)
.L61:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L170
	movaps	%xmm0, 80(%rsp)
	leaq	128+Q4_5D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC75(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC26(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 112(%rsp)
	movdqa	80(%rsp), %xmm2
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L171:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 112(%rsp)
.L63:
	subq	$16, %rbx
	movaps	%xmm2, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm2
	jne	.L171
	jmp	.L98
.L138:
	movdqa	.LC33(%rip), %xmm5
	leaq	128+P2r7_3r2N(%rip), %rbx
	movaps	%xmm5, 80(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC34(%rip), %xmm2
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L172:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 80(%rsp)
.L66:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L172
	movaps	%xmm0, 112(%rsp)
	leaq	112+P2r7_3r2D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC80(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC35(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 80(%rsp)
	movdqa	112(%rsp), %xmm2
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L173:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 80(%rsp)
.L78:
	subq	$16, %rbx
	movaps	%xmm2, 112(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm2
	jne	.L173
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r7_3r2N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC36(%rip), %xmm6
	leaq	-144(%rbx), %rbp
	movdqa	.LC37(%rip), %xmm7
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm6, 112(%rsp)
	movaps	%xmm7, 80(%rsp)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L174:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L80:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L174
	leaq	128+Q2r7_3r2D(%rip), %rbx
	movdqa	.LC81(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC38(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 112(%rsp)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L175:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 112(%rsp)
.L82:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L175
	jmp	.L81
.L139:
	movdqa	.LC45(%rip), %xmm3
	leaq	112+P2_2r3N(%rip), %rbx
	movdqa	.LC46(%rip), %xmm6
	leaq	-128(%rbx), %rbp
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm6, 80(%rsp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L176:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L83:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L176
	leaq	112+P2_2r3D(%rip), %rbx
	movdqa	.LC85(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC47(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 112(%rsp)
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L177:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 112(%rsp)
.L95:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L177
	movdqa	80(%rsp), %xmm0
	leaq	128+Q2_2r3N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC48(%rip), %xmm5
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm5, 80(%rsp)
	movdqa	.LC49(%rip), %xmm2
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L178:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 80(%rsp)
.L97:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L178
	movaps	%xmm0, 112(%rsp)
	leaq	112+Q2_2r3D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC86(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC50(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 80(%rsp)
	movdqa	112(%rsp), %xmm2
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L179:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 80(%rsp)
.L99:
	subq	$16, %rbx
	movaps	%xmm2, 112(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm2
	jne	.L179
	jmp	.L98
.L135:
	movdqa	.LC9(%rip), %xmm7
	leaq	160+P8_16N(%rip), %rbx
	movaps	%xmm7, 80(%rsp)
	leaq	-176(%rbx), %rbp
	movdqa	.LC10(%rip), %xmm2
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L180:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 80(%rsp)
.L29:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L180
	movaps	%xmm0, 80(%rsp)
	leaq	144+P8_16D(%rip), %rbx
	leaq	-160(%rbx), %rbp
	movdqa	.LC69(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC11(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 112(%rsp)
	movdqa	80(%rsp), %xmm2
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L181:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
.L42:
	subq	$16, %rbx
	movaps	%xmm2, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm2
	jne	.L181
	movdqa	%xmm2, %xmm0
	leaq	160+Q8_16N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC12(%rip), %xmm7
	leaq	-176(%rbx), %rbp
	movdqa	.LC13(%rip), %xmm3
	movaps	%xmm0, 128(%rsp)
	movaps	%xmm7, 112(%rsp)
	movaps	%xmm3, 80(%rsp)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L182:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 112(%rsp)
.L44:
	subq	$16, %rbx
	movdqa	80(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 80(%rsp)
	jne	.L182
	movdqa	.LC70(%rip), %xmm1
	leaq	160+Q8_16D(%rip), %rbx
	movdqa	32(%rsp), %xmm0
	leaq	-176(%rbx), %rbp
	call	__addtf3@PLT
	movdqa	.LC14(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 112(%rsp)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L183:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 112(%rsp)
.L46:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L183
	jmp	.L81
	.size	__ieee754_j1f128, .-__ieee754_j1f128
	.p2align 4,,15
	.globl	__ieee754_y1f128
	.type	__ieee754_y1f128, @function
__ieee754_y1f128:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm4
	subq	$168, %rsp
	pand	.LC52(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movdqa	.LC53(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L287
	movdqa	.LC53(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L287
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L320
	movdqa	.LC91(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L321
	movdqa	.LC57(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L309
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 128(%rsp)
# 0 "" 2
#NO_APP
	movl	128(%rsp), %r12d
	xorl	%r13d, %r13d
	movl	%r12d, %eax
	andb	$-97, %ah
	cmpl	%r12d, %eax
	movl	%eax, 144(%rsp)
	jne	.L322
.L196:
	movdqa	(%rsp), %xmm0
	leaq	96+Y0_2N(%rip), %rbx
	movdqa	%xmm0, %xmm1
	leaq	-112(%rbx), %rbp
	call	__multf3@PLT
	movdqa	.LC87(%rip), %xmm5
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm5, 48(%rsp)
	movdqa	.LC88(%rip), %xmm1
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L323:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 48(%rsp)
.L198:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L323
	movdqa	16(%rsp), %xmm0
	leaq	96+Y0_2D(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC93(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC89(%rip), %xmm6
	movaps	%xmm6, 48(%rsp)
	jmp	.L200
	.p2align 4,,10
	.p2align 3
.L324:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 48(%rsp)
.L200:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L324
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC92(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__ieee754_j1f128@PLT
	movdqa	.LC94(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	testb	%r13b, %r13b
	movdqa	%xmm0, %xmm3
	je	.L184
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 144(%rsp)
# 0 "" 2
#NO_APP
	movl	144(%rsp), %eax
	andl	$24576, %r12d
	andb	$-97, %ah
	orl	%eax, %r12d
	movl	%r12d, 144(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 144(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L287:
	movdqa	(%rsp), %xmm5
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC63(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm3
.L184:
	addq	$168, %rsp
	movdqa	%xmm3, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	pxor	%xmm1, %xmm1
	js	.L325
	movdqa	.LC90(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm3
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L321:
	movdqa	(%rsp), %xmm1
	movdqa	.LC92(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	.LC52(%rip), %xmm2
	pand	%xmm0, %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	movdqa	.LC53(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	jne	.L184
	movdqa	16(%rsp), %xmm2
	movdqa	.LC53(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	jle	.L184
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L325:
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm3
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L309:
	leaq	128(%rsp), %rsi
	leaq	144(%rsp), %rdi
	movdqa	16(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	144(%rsp), %xmm2
	movdqa	%xmm2, %xmm5
	movdqa	128(%rsp), %xmm3
	pxor	.LC59(%rip), %xmm5
	movdqa	%xmm3, %xmm1
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm3, (%rsp)
	movdqa	%xmm5, %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm3
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm3, %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC60(%rip), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L326
.L201:
	movdqa	.LC61(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L327
	movdqa	16(%rsp), %xmm1
	movdqa	.LC63(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC64(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L312
	movdqa	.LC65(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L313
	movdqa	.LC66(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L314
	movdqa	.LC3(%rip), %xmm6
	leaq	128+P16_IN(%rip), %rbx
	movdqa	.LC4(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L328:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L214:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L328
	leaq	128+P16_ID(%rip), %rbx
	movdqa	.LC67(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 96(%rsp)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L329:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L216:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L329
	movdqa	64(%rsp), %xmm0
	leaq	144+Q16_IN(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC6(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 64(%rsp)
	movdqa	.LC7(%rip), %xmm2
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L330:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L218:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L330
	movaps	%xmm0, 96(%rsp)
	leaq	128+Q16_ID(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC68(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L331:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L220:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L331
.L280:
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 64(%rsp)
.L221:
	movdqa	(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC76(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC63(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC62(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm3
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L326:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__cosf128@PLT
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	144(%rsp), %xmm0
	call	__multf3@PLT
	pxor	%xmm1, %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L310
	movdqa	48(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 80(%rsp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L327:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC62(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm3
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L312:
	movdqa	.LC76(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L316
	movdqa	.LC77(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L317
	movdqa	.LC27(%rip), %xmm7
	leaq	128+P3r2_4N(%rip), %rbx
	movdqa	.LC28(%rip), %xmm5
	leaq	-144(%rbx), %rbp
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm5, 64(%rsp)
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L332:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L251:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L332
	leaq	128+P3r2_4D(%rip), %rbx
	movdqa	.LC78(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC29(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 96(%rsp)
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L333:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L253:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L333
	movdqa	64(%rsp), %xmm0
	leaq	128+Q3r2_4N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC30(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movdqa	.LC31(%rip), %xmm5
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm5, 64(%rsp)
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L334:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L255:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L334
	leaq	128+Q3r2_4D(%rip), %rbx
	movdqa	.LC79(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC32(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 96(%rsp)
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L335:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L257:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L335
.L263:
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 64(%rsp)
	jmp	.L221
	.p2align 4,,10
	.p2align 3
.L322:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 144(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r13d
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L316:
	movdqa	.LC82(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L318
	movdqa	.LC39(%rip), %xmm6
	leaq	128+P2r3_2r7N(%rip), %rbx
	movaps	%xmm6, 64(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC40(%rip), %xmm2
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L336:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L268:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L336
	movaps	%xmm0, 96(%rsp)
	leaq	112+P2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC83(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC41(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L337:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L270:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L337
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r3_2r7N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC42(%rip), %xmm5
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm5, 64(%rsp)
	movdqa	.LC43(%rip), %xmm2
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L338:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L272:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L338
	movaps	%xmm0, 96(%rsp)
	leaq	112+Q2r3_2r7D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC84(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC44(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L339:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L274:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L339
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L313:
	movdqa	.LC71(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L315
	movdqa	.LC15(%rip), %xmm6
	leaq	144+P5_8N(%rip), %rbx
	movdqa	.LC16(%rip), %xmm7
	leaq	-160(%rbx), %rbp
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L340:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L232:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L340
	leaq	144+P5_8D(%rip), %rbx
	movdqa	.LC72(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC17(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L341:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L234:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L341
	movdqa	64(%rsp), %xmm0
	leaq	144+Q5_8N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC18(%rip), %xmm6
	leaq	-160(%rbx), %rbp
	movdqa	.LC19(%rip), %xmm7
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L342:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L236:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L342
	leaq	144+Q5_8D(%rip), %rbx
	movdqa	.LC73(%rip), %xmm1
	leaq	-160(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC20(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L343:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L238:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L343
	jmp	.L263
.L310:
	movdqa	80(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 48(%rsp)
	jmp	.L201
.L315:
	movdqa	.LC21(%rip), %xmm6
	leaq	144+P4_5N(%rip), %rbx
	movaps	%xmm6, 64(%rsp)
	leaq	-160(%rbx), %rbp
	movdqa	.LC22(%rip), %xmm2
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L344:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L229:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L344
	movaps	%xmm0, 96(%rsp)
	leaq	128+P4_5D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC74(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC23(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L345:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L241:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L345
	movdqa	%xmm2, %xmm0
	leaq	144+Q4_5N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC24(%rip), %xmm5
	leaq	-160(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm5, 64(%rsp)
	movdqa	.LC25(%rip), %xmm2
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L346:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L243:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L346
	movaps	%xmm0, 96(%rsp)
	leaq	128+Q4_5D(%rip), %rbx
	leaq	-144(%rbx), %rbp
	movdqa	.LC75(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC26(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L347:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 64(%rsp)
.L245:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L347
	jmp	.L280
.L317:
	movdqa	.LC33(%rip), %xmm7
	leaq	128+P2r7_3r2N(%rip), %rbx
	movaps	%xmm7, 64(%rsp)
	leaq	-144(%rbx), %rbp
	movdqa	.LC34(%rip), %xmm2
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L348:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L248:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L348
	movaps	%xmm0, 96(%rsp)
	leaq	112+P2r7_3r2D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC80(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC35(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L349:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L260:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L349
	movdqa	%xmm2, %xmm0
	leaq	128+Q2r7_3r2N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC36(%rip), %xmm6
	leaq	-144(%rbx), %rbp
	movdqa	.LC37(%rip), %xmm7
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm7, 64(%rsp)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L350:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L262:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L350
	leaq	128+Q2r7_3r2D(%rip), %rbx
	movdqa	.LC81(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC38(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 96(%rsp)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L351:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 96(%rsp)
.L264:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L351
	jmp	.L263
.L318:
	movdqa	.LC45(%rip), %xmm7
	leaq	112+P2_2r3N(%rip), %rbx
	movdqa	.LC46(%rip), %xmm5
	leaq	-128(%rbx), %rbp
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm5, 64(%rsp)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L352:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L265:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L352
	leaq	112+P2_2r3D(%rip), %rbx
	movdqa	.LC85(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC47(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 96(%rsp)
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L353:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L277:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L353
	movdqa	64(%rsp), %xmm0
	leaq	128+Q2_2r3N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC48(%rip), %xmm7
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 64(%rsp)
	movdqa	.LC49(%rip), %xmm2
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L354:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L279:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L354
	movaps	%xmm0, 96(%rsp)
	leaq	112+Q2_2r3D(%rip), %rbx
	leaq	-128(%rbx), %rbp
	movdqa	.LC86(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC50(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L355:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L281:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L355
	jmp	.L280
.L314:
	movdqa	.LC9(%rip), %xmm7
	leaq	160+P8_16N(%rip), %rbx
	movaps	%xmm7, 64(%rsp)
	leaq	-176(%rbx), %rbp
	movdqa	.LC10(%rip), %xmm2
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L356:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 64(%rsp)
.L211:
	movdqa	%xmm2, %xmm1
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm2
	jne	.L356
	movaps	%xmm0, 96(%rsp)
	leaq	144+P8_16D(%rip), %rbx
	leaq	-160(%rbx), %rbp
	movdqa	.LC69(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC11(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 64(%rsp)
	movdqa	96(%rsp), %xmm2
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L357:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 64(%rsp)
.L224:
	subq	$16, %rbx
	movaps	%xmm2, 96(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm2
	jne	.L357
	movdqa	%xmm2, %xmm0
	leaq	160+Q8_16N(%rip), %rbx
	call	__divtf3@PLT
	movdqa	.LC12(%rip), %xmm7
	leaq	-176(%rbx), %rbp
	movdqa	.LC13(%rip), %xmm6
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm7, 96(%rsp)
	movaps	%xmm6, 64(%rsp)
	jmp	.L226
	.p2align 4,,10
	.p2align 3
.L358:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 96(%rsp)
.L226:
	subq	$16, %rbx
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 64(%rsp)
	jne	.L358
	movdqa	.LC70(%rip), %xmm1
	leaq	160+Q8_16D(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-176(%rbx), %rbp
	call	__addtf3@PLT
	movdqa	.LC14(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 96(%rsp)
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L359:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 96(%rsp)
.L228:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L359
	jmp	.L263
	.size	__ieee754_y1f128, .-__ieee754_y1f128
	.section	.rodata
	.align 32
	.type	Y0_2D, @object
	.size	Y0_2D, 128
Y0_2D:
	.long	3883727390
	.long	2428640721
	.long	1965616201
	.long	1078144263
	.long	3833725501
	.long	2906052806
	.long	339351307
	.long	1077725568
	.long	315890414
	.long	3657264880
	.long	2167128371
	.long	1077239832
	.long	709352944
	.long	2397996578
	.long	723721995
	.long	1076718118
	.long	1686805200
	.long	3110008757
	.long	2143307123
	.long	1076171642
	.long	3337436838
	.long	2312258288
	.long	819052615
	.long	1075593674
	.long	1331022085
	.long	2800896219
	.long	3603804518
	.long	1074994812
	.long	3483272673
	.long	3918963942
	.long	2072018951
	.long	1074358933
	.align 32
	.type	Y0_2N, @object
	.size	Y0_2N, 128
Y0_2N:
	.long	917508635
	.long	4003334946
	.long	2999712131
	.long	-1069492186
	.long	230886870
	.long	1984989958
	.long	4187728738
	.long	1077867804
	.long	3772610167
	.long	809251198
	.long	4005831047
	.long	-1069914576
	.long	460038054
	.long	1421762797
	.long	2843057036
	.long	1077186120
	.long	1640916589
	.long	146029574
	.long	3531754327
	.long	-1070749122
	.long	752586842
	.long	2672536884
	.long	131496330
	.long	1076221790
	.long	2607141716
	.long	855415953
	.long	3882501038
	.long	-1071839881
	.long	314906108
	.long	1763577934
	.long	2794144124
	.long	1074975200
	.align 32
	.type	Q2_2r3D, @object
	.size	Q2_2r3D, 144
Q2_2r3D:
	.long	4199502532
	.long	3982115543
	.long	3273459701
	.long	1072743998
	.long	3515841604
	.long	4041905930
	.long	520566336
	.long	1073156580
	.long	1141140986
	.long	2735250466
	.long	1547555687
	.long	1073467758
	.long	3663999012
	.long	1004467905
	.long	676740918
	.long	1073703986
	.long	3678813180
	.long	3465811817
	.long	1291385841
	.long	1073880832
	.long	1772856558
	.long	3327361972
	.long	2833424179
	.long	1073992275
	.long	3740952893
	.long	706848587
	.long	1198392501
	.long	1074036359
	.long	558762700
	.long	588642690
	.long	1599210801
	.long	1074014082
	.long	592960030
	.long	425767978
	.long	3537269804
	.long	1073901831
	.align 32
	.type	Q2_2r3N, @object
	.size	Q2_2r3N, 160
Q2_2r3N:
	.long	352157537
	.long	2123507860
	.long	1515133840
	.long	-1074957153
	.long	2385866511
	.long	1625246562
	.long	3767910011
	.long	-1074548329
	.long	836554063
	.long	3609871789
	.long	1143080727
	.long	-1074242735
	.long	4009375434
	.long	89276845
	.long	3242732457
	.long	-1074007946
	.long	76187194
	.long	1851377095
	.long	2330320821
	.long	-1073850767
	.long	2728442748
	.long	3774130925
	.long	2470403092
	.long	-1073755352
	.long	1579486407
	.long	237097324
	.long	682605803
	.long	-1073734438
	.long	2839470527
	.long	1747125943
	.long	1212681598
	.long	-1073799273
	.long	2390542353
	.long	2523057807
	.long	3315932222
	.long	-1073981467
	.long	1414819897
	.long	4200512174
	.long	2898105729
	.long	-1074430375
	.align 32
	.type	Q2r3_2r7D, @object
	.size	Q2r3_2r7D, 144
Q2r3_2r7D:
	.long	3771261832
	.long	1148293611
	.long	3014052361
	.long	1072568709
	.long	1962558238
	.long	3586711839
	.long	2892770657
	.long	1072993715
	.long	1604244438
	.long	2277788445
	.long	1852476396
	.long	1073324133
	.long	2183916489
	.long	1511452938
	.long	2174797094
	.long	1073581362
	.long	1393705830
	.long	2136997553
	.long	4238621397
	.long	1073774715
	.long	1701611434
	.long	1830818445
	.long	3746506116
	.long	1073906194
	.long	3943449742
	.long	2055676456
	.long	2749181106
	.long	1073973592
	.long	1723019575
	.long	4279002486
	.long	263557100
	.long	1073969858
	.long	1643617357
	.long	1140618909
	.long	207060704
	.long	1073882984
	.align 32
	.type	Q2r3_2r7N, @object
	.size	Q2r3_2r7N, 160
Q2r3_2r7N:
	.long	3716264833
	.long	327652652
	.long	1331287930
	.long	-1075130913
	.long	987419528
	.long	3610262358
	.long	3739957986
	.long	-1074707704
	.long	2983112558
	.long	694674119
	.long	3396037585
	.long	-1074381924
	.long	1890922958
	.long	3207075287
	.long	810731954
	.long	-1074129201
	.long	3578484296
	.long	2220581942
	.long	3895439445
	.long	-1073946365
	.long	2290853053
	.long	2063591422
	.long	2271246610
	.long	-1073834132
	.long	324376902
	.long	1869896390
	.long	126808121
	.long	-1073790421
	.long	1509318494
	.long	2213541353
	.long	1769997588
	.long	-1073831446
	.long	1801723668
	.long	2556848135
	.long	134115315
	.long	-1073986573
	.long	1063312022
	.long	3673377142
	.long	3120155540
	.long	-1074401381
	.align 32
	.type	Q2r7_3r2D, @object
	.size	Q2r7_3r2D, 160
Q2r7_3r2D:
	.long	1407829434
	.long	3570531370
	.long	4001395388
	.long	1072415530
	.long	303604633
	.long	107462773
	.long	3695403285
	.long	1072870680
	.long	2439142383
	.long	3034199712
	.long	252314396
	.long	1073231233
	.long	29464810
	.long	1031204935
	.long	3912623912
	.long	1073521341
	.long	1241085127
	.long	1474667157
	.long	3015385647
	.long	1073752181
	.long	852416429
	.long	2707978662
	.long	2802787701
	.long	1073923033
	.long	1723925047
	.long	2472947026
	.long	3614505781
	.long	1074032308
	.long	2448740175
	.long	2905942735
	.long	391009244
	.long	1074081275
	.long	1540818302
	.long	2004350207
	.long	4155191494
	.long	1074055118
	.long	2747542035
	.long	1610211576
	.long	1228473556
	.long	1073938493
	.align 32
	.type	Q2r7_3r2N, @object
	.size	Q2r7_3r2N, 160
Q2r7_3r2N:
	.long	1407767722
	.long	766090185
	.long	916987397
	.long	-1075285479
	.long	5406015
	.long	1404077100
	.long	2866883025
	.long	-1074831686
	.long	3332251307
	.long	1048828311
	.long	1715563841
	.long	-1074471116
	.long	500222802
	.long	3844209664
	.long	373370485
	.long	-1074187622
	.long	394582942
	.long	325686179
	.long	428993113
	.long	-1073967324
	.long	4276484073
	.long	1481875272
	.long	3618058040
	.long	-1073804165
	.long	400988554
	.long	1933496135
	.long	2417309834
	.long	-1073714702
	.long	3287146468
	.long	4115010642
	.long	425623181
	.long	-1073692385
	.long	871743697
	.long	3203069404
	.long	1525753026
	.long	-1073763495
	.long	4108865071
	.long	1071653649
	.long	1434799386
	.long	-1073968275
	.align 32
	.type	Q3r2_4D, @object
	.size	Q3r2_4D, 160
Q3r2_4D:
	.long	1183488072
	.long	3677782460
	.long	19223109
	.long	1072154497
	.long	2795049773
	.long	68241616
	.long	2187274100
	.long	1072634199
	.long	1646826560
	.long	2044673700
	.long	223747852
	.long	1073021182
	.long	747177546
	.long	1342078951
	.long	4245268421
	.long	1073333531
	.long	1317046736
	.long	1945424132
	.long	3650561078
	.long	1073587628
	.long	700079616
	.long	1601285897
	.long	1200338304
	.long	1073785201
	.long	3254526616
	.long	2461772358
	.long	2045872127
	.long	1073925046
	.long	821705369
	.long	2527077029
	.long	339901634
	.long	1074002456
	.long	964596936
	.long	2991404285
	.long	2167298302
	.long	1074003893
	.long	225438466
	.long	3579254541
	.long	4068562306
	.long	1073905962
	.align 32
	.type	Q3r2_4N, @object
	.size	Q3r2_4N, 160
Q3r2_4N:
	.long	456427070
	.long	1060160293
	.long	3537258491
	.long	-1075546713
	.long	107941692
	.long	3190762803
	.long	2477493633
	.long	-1075067476
	.long	3178650178
	.long	870705063
	.long	3462268220
	.long	-1074687235
	.long	2047393247
	.long	1939673730
	.long	3730363592
	.long	-1074375674
	.long	232544210
	.long	1169301681
	.long	846361184
	.long	-1074124849
	.long	3318701164
	.long	50665444
	.long	1632929786
	.long	-1073935028
	.long	2580355683
	.long	789638312
	.long	1089049936
	.long	-1073811172
	.long	3071465610
	.long	2717271523
	.long	3052199259
	.long	-1073763614
	.long	501126635
	.long	762707617
	.long	606082627
	.long	-1073800200
	.long	1926635597
	.long	1981298042
	.long	3749472925
	.long	-1073974455
	.align 32
	.type	Q4_5D, @object
	.size	Q4_5D, 160
Q4_5D:
	.long	2984938781
	.long	3748059859
	.long	3162128387
	.long	1071625584
	.long	1858574372
	.long	3521624396
	.long	1715499630
	.long	1072139079
	.long	3832176305
	.long	2606992260
	.long	734415271
	.long	1072565872
	.long	305537088
	.long	2307671879
	.long	17094133
	.long	1072918009
	.long	1786070801
	.long	3459108831
	.long	991107744
	.long	1073220695
	.long	1139307529
	.long	223105152
	.long	1789675731
	.long	1073461075
	.long	298917086
	.long	273745877
	.long	181535296
	.long	1073647329
	.long	4157253788
	.long	1670829748
	.long	3946239258
	.long	1073775117
	.long	212631768
	.long	4069682073
	.long	170690754
	.long	1073835589
	.long	2005002728
	.long	70492843
	.long	2096394205
	.long	1073815724
	.align 32
	.type	Q4_5N, @object
	.size	Q4_5N, 176
Q4_5N:
	.long	2710926930
	.long	3535168302
	.long	2057029279
	.long	-1076074794
	.long	1467488030
	.long	3364078332
	.long	793279690
	.long	-1075560331
	.long	931049939
	.long	2804817295
	.long	4265621261
	.long	-1075138971
	.long	341758261
	.long	3382673320
	.long	2905487716
	.long	-1074783516
	.long	3228305459
	.long	2957218751
	.long	1444504569
	.long	-1074492240
	.long	3898249554
	.long	1415768040
	.long	318864156
	.long	-1074254063
	.long	4169311524
	.long	3205390778
	.long	1743282387
	.long	-1074074053
	.long	174365688
	.long	2543879852
	.long	2859261339
	.long	-1073966382
	.long	3306275223
	.long	179376447
	.long	150849508
	.long	-1073930802
	.long	301906457
	.long	940683751
	.long	1016749094
	.long	-1074003681
	.long	610230655
	.long	1517900145
	.long	3371583605
	.long	-1074324700
	.align 32
	.type	Q5_8D, @object
	.size	Q5_8D, 176
Q5_8D:
	.long	335386281
	.long	824990903
	.long	2180109058
	.long	1071183310
	.long	1137606813
	.long	2978233663
	.long	1022431123
	.long	1071737630
	.long	1676454554
	.long	3149153214
	.long	2210698275
	.long	1072207742
	.long	2680083514
	.long	4262298905
	.long	3730436790
	.long	1072615328
	.long	1936919197
	.long	72725087
	.long	3592670138
	.long	1072965007
	.long	2538853652
	.long	3893845929
	.long	1896786612
	.long	1073261508
	.long	3472830519
	.long	1044173335
	.long	2888425400
	.long	1073505906
	.long	1422356807
	.long	1768341161
	.long	1102283276
	.long	1073696697
	.long	1801994854
	.long	3101736363
	.long	2671429347
	.long	1073826410
	.long	4036076752
	.long	2999044475
	.long	1771021103
	.long	1073885411
	.long	1780818049
	.long	2618325991
	.long	2388994287
	.long	1073850034
	.align 32
	.type	Q5_8N, @object
	.size	Q5_8N, 176
Q5_8N:
	.long	388944206
	.long	2048040824
	.long	1721261760
	.long	-1076520029
	.long	1914062627
	.long	1538028239
	.long	2773705346
	.long	-1075959887
	.long	1533865009
	.long	3852530826
	.long	1522576197
	.long	-1075492840
	.long	2573696054
	.long	1328262715
	.long	3059967399
	.long	-1075089700
	.long	2996374362
	.long	2504276230
	.long	1495578019
	.long	-1074740137
	.long	391318978
	.long	2223342612
	.long	1252623234
	.long	-1074447305
	.long	1475074556
	.long	1912354947
	.long	3131246913
	.long	-1074204523
	.long	1714821243
	.long	1510304121
	.long	447221772
	.long	-1074027828
	.long	2000650724
	.long	2399964768
	.long	2907133794
	.long	-1073915536
	.long	1294074024
	.long	3308859677
	.long	1964293081
	.long	-1073885721
	.long	223934334
	.long	1675074133
	.long	1108080424
	.long	-1073988292
	.align 32
	.type	Q8_16D, @object
	.size	Q8_16D, 192
Q8_16D:
	.long	1839071307
	.long	1395784398
	.long	4219032669
	.long	1070254127
	.long	2282235694
	.long	3665612909
	.long	2289818603
	.long	1070878444
	.long	3514803650
	.long	4097466614
	.long	1494973717
	.long	1071417603
	.long	3685894466
	.long	586283356
	.long	3235945634
	.long	1071899378
	.long	480219599
	.long	2859580577
	.long	2196024706
	.long	1072322123
	.long	705015083
	.long	1845508998
	.long	3631639456
	.long	1072702162
	.long	1953699955
	.long	2078264632
	.long	3750719459
	.long	1073031803
	.long	309269844
	.long	1513398701
	.long	290859564
	.long	1073310837
	.long	1397480172
	.long	211853535
	.long	861480972
	.long	1073544537
	.long	3376940362
	.long	1405981664
	.long	2862667004
	.long	1073708002
	.long	1014106468
	.long	3127253999
	.long	2410513951
	.long	1073810354
	.long	3755885413
	.long	80240280
	.long	3426490220
	.long	1073814235
	.align 32
	.type	Q8_16N, @object
	.size	Q8_16N, 192
Q8_16N:
	.long	3096533355
	.long	2385319269
	.long	1548322612
	.long	-1077447113
	.long	2554413907
	.long	3107096441
	.long	3338560932
	.long	-1076817472
	.long	2846045079
	.long	2225001908
	.long	495331078
	.long	-1076281468
	.long	933130952
	.long	515606588
	.long	2775773060
	.long	-1075804798
	.long	325143245
	.long	3162611163
	.long	2888616968
	.long	-1075375930
	.long	57834509
	.long	1129543273
	.long	2537694728
	.long	-1075001573
	.long	4010448919
	.long	3577777723
	.long	2013890775
	.long	-1074674185
	.long	2704935165
	.long	3381056798
	.long	26006981
	.long	-1074394936
	.long	3136202524
	.long	1567298116
	.long	3905427089
	.long	-1074178319
	.long	1618428799
	.long	2635831363
	.long	45709890
	.long	-1074020517
	.long	1739089136
	.long	2210945664
	.long	1888269005
	.long	-1073944997
	.long	3341159017
	.long	4175921945
	.long	1747226435
	.long	-1073997920
	.align 32
	.type	Q16_ID, @object
	.size	Q16_ID, 160
Q16_ID:
	.long	3298583576
	.long	563015687
	.long	302250279
	.long	1070096479
	.long	1376178121
	.long	3139216479
	.long	3402960813
	.long	1070761721
	.long	2599542349
	.long	1962002488
	.long	1174133605
	.long	1071337699
	.long	2110460727
	.long	391397426
	.long	3294789120
	.long	1071850727
	.long	2175970919
	.long	534776648
	.long	2326086105
	.long	1072306149
	.long	5914700
	.long	2174862497
	.long	930365068
	.long	1072704715
	.long	1346292723
	.long	140958629
	.long	703962642
	.long	1073047097
	.long	3764388039
	.long	2576440204
	.long	2138450653
	.long	1073332681
	.long	2789870555
	.long	4007356359
	.long	1153402051
	.long	1073548824
	.long	3324408434
	.long	527595841
	.long	3005332907
	.long	1073676743
	.align 32
	.type	Q16_IN, @object
	.size	Q16_IN, 176
Q16_IN:
	.long	1598534742
	.long	428294374
	.long	4240917090
	.long	-1077599987
	.long	2240384172
	.long	3600398349
	.long	1235973378
	.long	-1076936715
	.long	3300978550
	.long	1164531848
	.long	328775826
	.long	-1076358485
	.long	208392607
	.long	885118892
	.long	485518219
	.long	-1075848500
	.long	4200330730
	.long	735744317
	.long	1181183678
	.long	-1075396260
	.long	1923449624
	.long	3600811003
	.long	4174173113
	.long	-1074996445
	.long	4056308761
	.long	3313084907
	.long	3084000588
	.long	-1074654480
	.long	1911841130
	.long	3739032362
	.long	2166063847
	.long	-1074378996
	.long	4218769821
	.long	174194856
	.long	1995233420
	.long	-1074176276
	.long	1196226868
	.long	3335234359
	.long	2312859388
	.long	-1074070888
	.long	4025425625
	.long	511745243
	.long	220912580
	.long	-1074185485
	.align 32
	.type	P2_2r3D, @object
	.size	P2_2r3D, 144
P2_2r3D:
	.long	4069744202
	.long	465826858
	.long	1663835123
	.long	1073118221
	.long	4130677210
	.long	97280599
	.long	3220910996
	.long	1073510146
	.long	4048091537
	.long	4082636789
	.long	3016113676
	.long	1073807338
	.long	1051179642
	.long	2069069894
	.long	3309936199
	.long	1074018898
	.long	1807286412
	.long	1076669878
	.long	1612361789
	.long	1074165977
	.long	196741549
	.long	3734212030
	.long	2708791945
	.long	1074249253
	.long	2941827903
	.long	3605056977
	.long	4157968124
	.long	1074263167
	.long	2633894847
	.long	835441905
	.long	3165514341
	.long	1074191684
	.long	266801016
	.long	3152136507
	.long	1225682728
	.long	1074016078
	.align 32
	.type	P2_2r3N, @object
	.size	P2_2r3N, 144
P2_2r3N:
	.long	2101782765
	.long	1289778111
	.long	2192374569
	.long	1072915532
	.long	3316801836
	.long	554786418
	.long	141236866
	.long	1073305785
	.long	2928466134
	.long	161659683
	.long	57628339
	.long	1073596132
	.long	289543060
	.long	3170527696
	.long	1578115692
	.long	1073809519
	.long	3323588291
	.long	3751671330
	.long	3994981649
	.long	1073947439
	.long	3672621886
	.long	2455918842
	.long	3635889058
	.long	1074015882
	.long	2807633051
	.long	1386502926
	.long	3090703336
	.long	1074009284
	.long	3388005343
	.long	995071527
	.long	3611331904
	.long	1073903500
	.long	3041602049
	.long	3219659461
	.long	3353495544
	.long	1073669153
	.align 32
	.type	P2r3_2r7D, @object
	.size	P2r3_2r7D, 144
P2r3_2r7D:
	.long	2100755903
	.long	1306571941
	.long	2212792353
	.long	1072699487
	.long	1038663868
	.long	159443781
	.long	2433667126
	.long	1073114282
	.long	1939782990
	.long	1519645904
	.long	2707546193
	.long	1073434814
	.long	684023967
	.long	887561915
	.long	2810407288
	.long	1073684396
	.long	768641655
	.long	3788182362
	.long	277114064
	.long	1073868644
	.long	930116821
	.long	3028860758
	.long	458673228
	.long	1073985029
	.long	810666717
	.long	2623150479
	.long	2047200144
	.long	1074037497
	.long	2593958104
	.long	4204500273
	.long	3007414425
	.long	1074019145
	.long	781819093
	.long	1939560502
	.long	2905836932
	.long	1073909417
	.align 32
	.type	P2r3_2r7N, @object
	.size	P2r3_2r7N, 160
P2r3_2r7N:
	.long	2825564620
	.long	434299158
	.long	2322954354
	.long	1072498393
	.long	1772572826
	.long	3704492234
	.long	902046335
	.long	1072910546
	.long	2081587990
	.long	2313264620
	.long	58364255
	.long	1073229643
	.long	449417190
	.long	2390573924
	.long	2513761509
	.long	1073475994
	.long	550910103
	.long	3521680855
	.long	844110664
	.long	1073648175
	.long	2660926951
	.long	2099174790
	.long	1129341621
	.long	1073758865
	.long	1633722460
	.long	1307689967
	.long	310677132
	.long	1073801630
	.long	4181033771
	.long	3769605926
	.long	1868319782
	.long	1073755998
	.long	2897796304
	.long	2986967744
	.long	579559029
	.long	1073607552
	.long	3351462352
	.long	419575277
	.long	1674437513
	.long	1073245105
	.align 32
	.type	P2r7_3r2D, @object
	.size	P2r7_3r2D, 144
P2r7_3r2D:
	.long	1527965402
	.long	3498419762
	.long	2338636724
	.long	1072498526
	.long	2039791438
	.long	3935658324
	.long	2780922714
	.long	1072932491
	.long	3022779593
	.long	1129643924
	.long	3294707829
	.long	1073276539
	.long	2908261731
	.long	1377995345
	.long	4030049468
	.long	1073546608
	.long	1171466521
	.long	2990624921
	.long	3669244172
	.long	1073749342
	.long	2163065194
	.long	3685668280
	.long	2892247791
	.long	1073889747
	.long	3321743261
	.long	4134667693
	.long	1313142670
	.long	1073966165
	.long	1918638714
	.long	1114723451
	.long	1765959689
	.long	1073970574
	.long	3236561768
	.long	3432362669
	.long	2181947094
	.long	1073886830
	.align 32
	.type	P2r7_3r2N, @object
	.size	P2r7_3r2N, 160
P2r7_3r2N:
	.long	2528630781
	.long	3456519956
	.long	1157059984
	.long	1072295377
	.long	2781757969
	.long	329707566
	.long	2875124760
	.long	1072727907
	.long	2735316612
	.long	721392008
	.long	3168021457
	.long	1073068426
	.long	834291318
	.long	899664756
	.long	530082856
	.long	1073335079
	.long	1367340383
	.long	3866255648
	.long	1723710724
	.long	1073535880
	.long	1177211961
	.long	875784173
	.long	189496638
	.long	1073671573
	.long	774976820
	.long	2300224782
	.long	2905531856
	.long	1073735992
	.long	2002964328
	.long	3184295329
	.long	3391665795
	.long	1073715662
	.long	1213307415
	.long	3555117741
	.long	2036834322
	.long	1073591518
	.long	440078763
	.long	2976622153
	.long	2333642799
	.long	1073262433
	.align 32
	.type	P3r2_4D, @object
	.size	P3r2_4D, 160
P3r2_4D:
	.long	3016748260
	.long	2282255665
	.long	692720609
	.long	1072331145
	.long	3532643200
	.long	2459507673
	.long	2133065354
	.long	1072802242
	.long	2533126012
	.long	1630216636
	.long	3014359073
	.long	1073176964
	.long	2484010857
	.long	3187446506
	.long	3778582491
	.long	1073486248
	.long	719863274
	.long	2064882185
	.long	1257821558
	.long	1073731092
	.long	1419404615
	.long	2898137371
	.long	3831040899
	.long	1073915033
	.long	2550756125
	.long	3071299958
	.long	603068116
	.long	1074039955
	.long	2390998212
	.long	1478513301
	.long	3938749746
	.long	1074099070
	.long	253522641
	.long	2830799450
	.long	1064900580
	.long	1074082330
	.long	1653964066
	.long	182104687
	.long	3692358499
	.long	1073960838
	.align 32
	.type	P3r2_4N, @object
	.size	P3r2_4N, 160
P3r2_4N:
	.long	1742218795
	.long	3083284853
	.long	2528221083
	.long	1072128496
	.long	7791486
	.long	3050364101
	.long	1361894006
	.long	1072597951
	.long	3907748125
	.long	2841847053
	.long	3012379239
	.long	1072972799
	.long	4059729469
	.long	1273360910
	.long	1212768906
	.long	1073281207
	.long	1693667248
	.long	2348378674
	.long	3081823188
	.long	1073518157
	.long	1665428503
	.long	4127719802
	.long	1278192340
	.long	1073698666
	.long	4007102851
	.long	3196076015
	.long	453173154
	.long	1073817483
	.long	1812125522
	.long	3020097935
	.long	1965174713
	.long	1073863648
	.long	95695608
	.long	419836999
	.long	94562988
	.long	1073818359
	.long	3604434030
	.long	3221970807
	.long	1764328896
	.long	1073640892
	.align 32
	.type	P4_5D, @object
	.size	P4_5D, 160
P4_5D:
	.long	3975451745
	.long	1447112848
	.long	1481425761
	.long	1071755522
	.long	1755976322
	.long	1412863791
	.long	1774976148
	.long	1072261354
	.long	2526006621
	.long	913821166
	.long	2787219226
	.long	1072678341
	.long	3585171775
	.long	1626951393
	.long	3684624870
	.long	1073027255
	.long	193747091
	.long	1818425085
	.long	2860256650
	.long	1073313265
	.long	2421669093
	.long	1801664798
	.long	3574426685
	.long	1073550249
	.long	3307073767
	.long	1877095758
	.long	2679569186
	.long	1073724777
	.long	3007903240
	.long	1183741465
	.long	2923229940
	.long	1073838337
	.long	1524204242
	.long	1349149547
	.long	3810561891
	.long	1073885326
	.long	905943736
	.long	485864309
	.long	2140366357
	.long	1073841776
	.align 32
	.type	P4_5N, @object
	.size	P4_5N, 176
P4_5N:
	.long	2982909657
	.long	896940206
	.long	851960693
	.long	1071551986
	.long	1948769371
	.long	4012474016
	.long	1110112864
	.long	1072058472
	.long	4222229466
	.long	2460652663
	.long	2401905145
	.long	1072472844
	.long	1100895197
	.long	1106400500
	.long	2749846565
	.long	1072824037
	.long	271152416
	.long	343252405
	.long	688036390
	.long	1073106036
	.long	1704382266
	.long	661261054
	.long	1113252858
	.long	1073338458
	.long	976647823
	.long	3783214577
	.long	3965732291
	.long	1073506169
	.long	706547048
	.long	795217550
	.long	3370819019
	.long	1073615723
	.long	716551467
	.long	3760984694
	.long	2982282296
	.long	1073640700
	.long	1639702075
	.long	1577324146
	.long	4211946669
	.long	1073565403
	.long	995315656
	.long	3274874030
	.long	502558402
	.long	1073293221
	.align 32
	.type	P5_8D, @object
	.size	P5_8D, 176
P5_8D:
	.long	3034820447
	.long	2270182891
	.long	60203573
	.long	1071360510
	.long	2919674238
	.long	1264574
	.long	1063515982
	.long	1071914550
	.long	3732279525
	.long	1567709979
	.long	962176130
	.long	1072376045
	.long	3034408204
	.long	3200748183
	.long	100196988
	.long	1072772767
	.long	964244903
	.long	1570836170
	.long	1648614661
	.long	1073112879
	.long	1503260416
	.long	2137749412
	.long	2259062551
	.long	1073404313
	.long	406837567
	.long	3159440111
	.long	3857939609
	.long	1073636018
	.long	1457305714
	.long	2786589821
	.long	1723914899
	.long	1073816513
	.long	1622556965
	.long	3777185830
	.long	2832562252
	.long	1073933063
	.long	3998271623
	.long	3424791321
	.long	2008194518
	.long	1073967674
	.long	1573972445
	.long	236429732
	.long	4120939058
	.long	1073905213
	.align 32
	.type	P5_8N, @object
	.size	P5_8N, 176
P5_8N:
	.long	713834011
	.long	1940123420
	.long	593311752
	.long	1071157086
	.long	1780839646
	.long	426479053
	.long	654505498
	.long	1071713117
	.long	2336131477
	.long	968081561
	.long	329589627
	.long	1072174035
	.long	4161411651
	.long	4219326572
	.long	4159928612
	.long	1072569865
	.long	3005028700
	.long	3024878233
	.long	627196889
	.long	1072907844
	.long	1604422263
	.long	4009579639
	.long	4282083105
	.long	1073194226
	.long	3682737204
	.long	787387080
	.long	2189804899
	.long	1073426519
	.long	2896311269
	.long	753300067
	.long	959837333
	.long	1073600484
	.long	961134101
	.long	2927653898
	.long	1232518237
	.long	1073702297
	.long	1829068188
	.long	1434974267
	.long	2728426510
	.long	1073723375
	.long	1972234322
	.long	1909927757
	.long	32357092
	.long	1073616859
	.align 32
	.type	P8_16D, @object
	.size	P8_16D, 176
P8_16D:
	.long	346430833
	.long	1629268050
	.long	3835220004
	.long	1070493450
	.long	4061484015
	.long	864193640
	.long	2276765523
	.long	1071107821
	.long	3245928302
	.long	2145166828
	.long	1730267053
	.long	1071631409
	.long	2750874379
	.long	3712420010
	.long	3116419735
	.long	1072092975
	.long	2524488118
	.long	2638581431
	.long	3534218175
	.long	1072500903
	.long	4196346134
	.long	1424962625
	.long	1736565718
	.long	1072853173
	.long	222189884
	.long	2105258001
	.long	597782021
	.long	1073161041
	.long	3985334521
	.long	2498570411
	.long	4037271877
	.long	1073415631
	.long	3603521352
	.long	1935075527
	.long	2960160221
	.long	1073608996
	.long	3009455704
	.long	1156009449
	.long	630635065
	.long	1073731282
	.long	2410211387
	.long	1655199528
	.long	347029424
	.long	1073765257
	.align 32
	.type	P8_16N, @object
	.size	P8_16N, 192
P8_16N:
	.long	1075778497
	.long	453403129
	.long	911164194
	.long	1070290970
	.long	2566874549
	.long	854662144
	.long	4212718234
	.long	1070903594
	.long	1135339842
	.long	92998828
	.long	766014174
	.long	1071426908
	.long	1338962940
	.long	2236809317
	.long	1226793168
	.long	1071887786
	.long	1483491138
	.long	2282039784
	.long	1314620947
	.long	1072297735
	.long	2787906917
	.long	2101930483
	.long	3045937468
	.long	1072648297
	.long	734617974
	.long	969962862
	.long	3661309647
	.long	1072956693
	.long	252136643
	.long	1712934089
	.long	2744485485
	.long	1073202998
	.long	4021335858
	.long	3229202698
	.long	424052317
	.long	1073389494
	.long	1659869756
	.long	1616049021
	.long	2281286008
	.long	1073503199
	.long	4196560979
	.long	3545889060
	.long	1335298698
	.long	1073516775
	.long	3256724855
	.long	957996417
	.long	3574496699
	.long	1073343000
	.align 32
	.type	P16_ID, @object
	.size	P16_ID, 160
P16_ID:
	.long	3678949905
	.long	3375564772
	.long	3084368624
	.long	1070545991
	.long	3509922930
	.long	3276962199
	.long	2564786920
	.long	1071194831
	.long	2517904804
	.long	3095258610
	.long	3698000572
	.long	1071748967
	.long	4038153352
	.long	3938471813
	.long	3923012917
	.long	1072240948
	.long	1953083990
	.long	3458835048
	.long	2288403530
	.long	1072668169
	.long	2829644285
	.long	1424117718
	.long	3389521864
	.long	1073039760
	.long	6676631
	.long	3981506373
	.long	3216342709
	.long	1073353593
	.long	2471717186
	.long	814613124
	.long	4045256228
	.long	1073594615
	.long	3537908335
	.long	3309428056
	.long	2748679739
	.long	1073755351
	.long	1021115316
	.long	4110804045
	.long	3716418709
	.long	1073810244
	.align 32
	.type	P16_IN, @object
	.size	P16_IN, 160
P16_IN:
	.long	2375237141
	.long	3164591974
	.long	1012547393
	.long	1070344323
	.long	3514258818
	.long	1101302738
	.long	1760739703
	.long	1070993479
	.long	2326633179
	.long	1484994148
	.long	2458294806
	.long	1071545507
	.long	4130101779
	.long	410049472
	.long	3091906227
	.long	1072039377
	.long	3646352311
	.long	2832500447
	.long	3752212450
	.long	1072463672
	.long	1248762744
	.long	642684834
	.long	776808184
	.long	1072836059
	.long	584435822
	.long	3642384735
	.long	3694844117
	.long	1073147578
	.long	878300334
	.long	414623202
	.long	2865559191
	.long	1073382140
	.long	369292110
	.long	930708983
	.long	3121246214
	.long	1073537838
	.long	441847753
	.long	198987371
	.long	1139069189
	.long	1073559177
	.align 32
	.type	J0_2D, @object
	.size	J0_2D, 112
J0_2D:
	.long	3705512579
	.long	3911083960
	.long	362841722
	.long	1077585493
	.long	4120076261
	.long	710919661
	.long	2282853103
	.long	1077161517
	.long	3139300201
	.long	1752620150
	.long	156122543
	.long	1076670404
	.long	3552891789
	.long	775730943
	.long	1457641599
	.long	1076138962
	.long	804183547
	.long	2519140087
	.long	4121679792
	.long	1075580555
	.long	4289533867
	.long	4237624818
	.long	2673638879
	.long	1074988992
	.long	3741397068
	.long	2912865278
	.long	3781395468
	.long	1074356476
	.align 32
	.type	J0_2N, @object
	.size	J0_2N, 112
J0_2N:
	.long	3705512668
	.long	3911083960
	.long	362841722
	.long	-1070160299
	.long	733224715
	.long	1280179370
	.long	1489846060
	.long	1076993029
	.long	3307527796
	.long	2346821757
	.long	1199902545
	.long	-1070886431
	.long	4054253104
	.long	2063683831
	.long	3940070113
	.long	1076135294
	.long	1932232033
	.long	2802852203
	.long	1431903248
	.long	-1071868026
	.long	628973856
	.long	1177179845
	.long	3550125945
	.long	1075033798
	.long	1459128487
	.long	1001997938
	.long	1639906940
	.long	-1073117725
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	628973856
	.long	1177179845
	.long	3550125945
	.long	1075033798
	.align 16
.LC1:
	.long	1459128487
	.long	1001997938
	.long	1639906940
	.long	-1073117725
	.align 16
.LC2:
	.long	4289533867
	.long	4237624818
	.long	2673638879
	.long	1074988992
	.align 16
.LC3:
	.long	369292110
	.long	930708983
	.long	3121246214
	.long	1073537838
	.align 16
.LC4:
	.long	441847753
	.long	198987371
	.long	1139069189
	.long	1073559177
	.align 16
.LC5:
	.long	3537908335
	.long	3309428056
	.long	2748679739
	.long	1073755351
	.align 16
.LC6:
	.long	1196226868
	.long	3335234359
	.long	2312859388
	.long	-1074070888
	.align 16
.LC7:
	.long	4025425625
	.long	511745243
	.long	220912580
	.long	-1074185485
	.align 16
.LC8:
	.long	2789870555
	.long	4007356359
	.long	1153402051
	.long	1073548824
	.align 16
.LC9:
	.long	4196560979
	.long	3545889060
	.long	1335298698
	.long	1073516775
	.align 16
.LC10:
	.long	3256724855
	.long	957996417
	.long	3574496699
	.long	1073343000
	.align 16
.LC11:
	.long	3009455704
	.long	1156009449
	.long	630635065
	.long	1073731282
	.align 16
.LC12:
	.long	1739089136
	.long	2210945664
	.long	1888269005
	.long	-1073944997
	.align 16
.LC13:
	.long	3341159017
	.long	4175921945
	.long	1747226435
	.long	-1073997920
	.align 16
.LC14:
	.long	1014106468
	.long	3127253999
	.long	2410513951
	.long	1073810354
	.align 16
.LC15:
	.long	1829068188
	.long	1434974267
	.long	2728426510
	.long	1073723375
	.align 16
.LC16:
	.long	1972234322
	.long	1909927757
	.long	32357092
	.long	1073616859
	.align 16
.LC17:
	.long	3998271623
	.long	3424791321
	.long	2008194518
	.long	1073967674
	.align 16
.LC18:
	.long	1294074024
	.long	3308859677
	.long	1964293081
	.long	-1073885721
	.align 16
.LC19:
	.long	223934334
	.long	1675074133
	.long	1108080424
	.long	-1073988292
	.align 16
.LC20:
	.long	4036076752
	.long	2999044475
	.long	1771021103
	.long	1073885411
	.align 16
.LC21:
	.long	1639702075
	.long	1577324146
	.long	4211946669
	.long	1073565403
	.align 16
.LC22:
	.long	995315656
	.long	3274874030
	.long	502558402
	.long	1073293221
	.align 16
.LC23:
	.long	1524204242
	.long	1349149547
	.long	3810561891
	.long	1073885326
	.align 16
.LC24:
	.long	301906457
	.long	940683751
	.long	1016749094
	.long	-1074003681
	.align 16
.LC25:
	.long	610230655
	.long	1517900145
	.long	3371583605
	.long	-1074324700
	.align 16
.LC26:
	.long	212631768
	.long	4069682073
	.long	170690754
	.long	1073835589
	.align 16
.LC27:
	.long	95695608
	.long	419836999
	.long	94562988
	.long	1073818359
	.align 16
.LC28:
	.long	3604434030
	.long	3221970807
	.long	1764328896
	.long	1073640892
	.align 16
.LC29:
	.long	253522641
	.long	2830799450
	.long	1064900580
	.long	1074082330
	.align 16
.LC30:
	.long	501126635
	.long	762707617
	.long	606082627
	.long	-1073800200
	.align 16
.LC31:
	.long	1926635597
	.long	1981298042
	.long	3749472925
	.long	-1073974455
	.align 16
.LC32:
	.long	964596936
	.long	2991404285
	.long	2167298302
	.long	1074003893
	.align 16
.LC33:
	.long	1213307415
	.long	3555117741
	.long	2036834322
	.long	1073591518
	.align 16
.LC34:
	.long	440078763
	.long	2976622153
	.long	2333642799
	.long	1073262433
	.align 16
.LC35:
	.long	1918638714
	.long	1114723451
	.long	1765959689
	.long	1073970574
	.align 16
.LC36:
	.long	871743697
	.long	3203069404
	.long	1525753026
	.long	-1073763495
	.align 16
.LC37:
	.long	4108865071
	.long	1071653649
	.long	1434799386
	.long	-1073968275
	.align 16
.LC38:
	.long	1540818302
	.long	2004350207
	.long	4155191494
	.long	1074055118
	.align 16
.LC39:
	.long	2897796304
	.long	2986967744
	.long	579559029
	.long	1073607552
	.align 16
.LC40:
	.long	3351462352
	.long	419575277
	.long	1674437513
	.long	1073245105
	.align 16
.LC41:
	.long	2593958104
	.long	4204500273
	.long	3007414425
	.long	1074019145
	.align 16
.LC42:
	.long	1801723668
	.long	2556848135
	.long	134115315
	.long	-1073986573
	.align 16
.LC43:
	.long	1063312022
	.long	3673377142
	.long	3120155540
	.long	-1074401381
	.align 16
.LC44:
	.long	1723019575
	.long	4279002486
	.long	263557100
	.long	1073969858
	.align 16
.LC45:
	.long	3388005343
	.long	995071527
	.long	3611331904
	.long	1073903500
	.align 16
.LC46:
	.long	3041602049
	.long	3219659461
	.long	3353495544
	.long	1073669153
	.align 16
.LC47:
	.long	2633894847
	.long	835441905
	.long	3165514341
	.long	1074191684
	.align 16
.LC48:
	.long	2390542353
	.long	2523057807
	.long	3315932222
	.long	-1073981467
	.align 16
.LC49:
	.long	1414819897
	.long	4200512174
	.long	2898105729
	.long	-1074430375
	.align 16
.LC50:
	.long	558762700
	.long	588642690
	.long	1599210801
	.long	1074014082
	.align 16
.LC52:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC53:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC54:
	.long	0
	.long	0
	.long	0
	.long	1069875200
	.align 16
.LC55:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC56:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC57:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC58:
	.long	3741397068
	.long	2912865278
	.long	3781395468
	.long	1074356476
	.align 16
.LC59:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC60:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147352575
	.align 16
.LC61:
	.long	0
	.long	0
	.long	0
	.long	1090453504
	.align 16
.LC62:
	.long	352245758
	.long	3508200361
	.long	1963207094
	.long	1073619165
	.align 16
.LC63:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC64:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC65:
	.long	0
	.long	0
	.long	0
	.long	1073479680
	.align 16
.LC66:
	.long	0
	.long	0
	.long	0
	.long	1073414144
	.align 16
.LC67:
	.long	1021115316
	.long	4110804045
	.long	3716418709
	.long	1073810244
	.align 16
.LC68:
	.long	3324408434
	.long	527595841
	.long	3005332907
	.long	1073676743
	.align 16
.LC69:
	.long	2410211387
	.long	1655199528
	.long	347029424
	.long	1073765257
	.align 16
.LC70:
	.long	3755885413
	.long	80240280
	.long	3426490220
	.long	1073814235
	.align 16
.LC71:
	.long	0
	.long	0
	.long	0
	.long	1073512448
	.align 16
.LC72:
	.long	1573972445
	.long	236429732
	.long	4120939058
	.long	1073905213
	.align 16
.LC73:
	.long	1780818049
	.long	2618325991
	.long	2388994287
	.long	1073850034
	.align 16
.LC74:
	.long	905943736
	.long	485864309
	.long	2140366357
	.long	1073841776
	.align 16
.LC75:
	.long	2005002728
	.long	70492843
	.long	2096394205
	.long	1073815724
	.align 16
.LC76:
	.long	0
	.long	0
	.long	0
	.long	1073577984
	.align 16
.LC77:
	.long	0
	.long	0
	.long	0
	.long	1073561600
	.align 16
.LC78:
	.long	1653964066
	.long	182104687
	.long	3692358499
	.long	1073960838
	.align 16
.LC79:
	.long	225438466
	.long	3579254541
	.long	4068562306
	.long	1073905962
	.align 16
.LC80:
	.long	3236561768
	.long	3432362669
	.long	2181947094
	.long	1073886830
	.align 16
.LC81:
	.long	2747542035
	.long	1610211576
	.long	1228473556
	.long	1073938493
	.align 16
.LC82:
	.long	0
	.long	0
	.long	0
	.long	1073594368
	.align 16
.LC83:
	.long	781819093
	.long	1939560502
	.long	2905836932
	.long	1073909417
	.align 16
.LC84:
	.long	1643617357
	.long	1140618909
	.long	207060704
	.long	1073882984
	.align 16
.LC85:
	.long	266801016
	.long	3152136507
	.long	1225682728
	.long	1074016078
	.align 16
.LC86:
	.long	592960030
	.long	425767978
	.long	3537269804
	.long	1073901831
	.align 16
.LC87:
	.long	2607141716
	.long	855415953
	.long	3882501038
	.long	-1071839881
	.align 16
.LC88:
	.long	314906108
	.long	1763577934
	.long	2794144124
	.long	1074975200
	.align 16
.LC89:
	.long	1331022085
	.long	2800896219
	.long	3603804518
	.long	1074994812
	.align 16
.LC90:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC91:
	.long	0
	.long	0
	.long	0
	.long	1066205184
	.align 16
.LC92:
	.long	2946755178
	.long	710146126
	.long	115121288
	.long	-1073854989
	.align 16
.LC93:
	.long	3483272673
	.long	3918963942
	.long	2072018951
	.long	1074358933
	.align 16
.LC94:
	.long	2946755178
	.long	710146126
	.long	115121288
	.long	1073628659
