	.text
	.p2align 4,,15
	.globl	fedisableexcept
	.type	fedisableexcept, @function
fedisableexcept:
#APP
# 31 "../sysdeps/x86_64/fpu/fedisblxcpt.c" 1
	fstcw -6(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-6(%rsp), %eax
	andl	$61, %edi
	movl	%eax, %edx
	orl	%edi, %edx
	movw	%dx, -6(%rsp)
#APP
# 36 "../sysdeps/x86_64/fpu/fedisblxcpt.c" 1
	fldcw -6(%rsp)
# 0 "" 2
# 39 "../sysdeps/x86_64/fpu/fedisblxcpt.c" 1
	stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	sall	$7, %edi
	orl	%edi, -4(%rsp)
#APP
# 43 "../sysdeps/x86_64/fpu/fedisblxcpt.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	notl	%eax
	andl	$61, %eax
	ret
	.size	fedisableexcept, .-fedisableexcept
