	.text
	.globl	__unordtf2
	.globl	__lttf2
	.globl	__getf2
	.globl	__addtf3
	.p2align 4,,15
	.globl	__fmaxf128
	.type	__fmaxf128, @function
__fmaxf128:
	subq	$40, %rsp
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L11
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L14
.L11:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L12
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L16
.L12:
	movdqa	(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	jne	.L5
	movdqa	16(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	jne	.L5
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L14
.L16:
	movdqa	16(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.size	__fmaxf128, .-__fmaxf128
	.weak	fmaxf128
	.set	fmaxf128,__fmaxf128
