	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ilogb
	.type	__ilogb, @function
__ilogb:
	pushq	%rbx
	call	__ieee754_ilogb@PLT
	movl	%eax, %ebx
	addl	$2147483647, %eax
	cmpl	$-3, %eax
	ja	.L5
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rax
	movl	$1, %edi
	movl	$33, %fs:(%rax)
	call	__GI___feraiseexcept
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	__ilogb, .-__ilogb
	.weak	ilogbf32x
	.set	ilogbf32x,__ilogb
	.weak	ilogbf64
	.set	ilogbf64,__ilogb
	.weak	ilogb
	.set	ilogb,__ilogb
