	.text
#APP
	.symver __feupdateenv,feupdateenv@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___feupdateenv
	.hidden	__GI___feupdateenv
	.type	__GI___feupdateenv, @function
__GI___feupdateenv:
	subq	$24, %rsp
#APP
# 29 "../sysdeps/x86_64/fpu/feupdateenv.c" 1
	fnstsw 10(%rsp)
	stmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	movl	12(%rsp), %eax
	orw	10(%rsp), %ax
	andl	$61, %eax
	movw	%ax, 10(%rsp)
	call	__GI___fesetenv
	movzwl	10(%rsp), %edi
	call	__GI___feraiseexcept
	xorl	%eax, %eax
	addq	$24, %rsp
	ret
	.size	__GI___feupdateenv, .-__GI___feupdateenv
	.globl	__feupdateenv
	.set	__feupdateenv,__GI___feupdateenv
	.globl	__GI_feupdateenv
	.set	__GI_feupdateenv,__feupdateenv
