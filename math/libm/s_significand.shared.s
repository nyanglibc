	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__significand
	.type	__significand, @function
__significand:
	subq	$24, %rsp
	movsd	%xmm0, 8(%rsp)
	call	__ilogb@PLT
	pxor	%xmm1, %xmm1
	negl	%eax
	movsd	8(%rsp), %xmm2
	addq	$24, %rsp
	cvtsi2sd	%eax, %xmm1
	movapd	%xmm2, %xmm0
	jmp	__ieee754_scalb@PLT
	.size	__significand, .-__significand
	.weak	significandf32x
	.set	significandf32x,__significand
	.weak	significandf64
	.set	significandf64,__significand
	.weak	significand
	.set	significand,__significand
