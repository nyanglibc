	.text
#APP
	.symver __ieee754_hypotf,__hypotf_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_hypotf
	.type	__ieee754_hypotf, @function
__ieee754_hypotf:
	movd	%xmm0, %esi
	movd	%xmm1, %edi
	movd	%xmm1, %eax
	movd	%xmm0, %ecx
	andl	$2147483647, %esi
	andl	$2147483647, %edi
	movd	%xmm1, %edx
	cmpl	$2139095040, %esi
	je	.L18
.L2:
	cmpl	$2139095040, %edi
	je	.L19
.L4:
	cmpl	$2139095040, %esi
	jg	.L9
	cmpl	$2139095040, %edi
	jg	.L9
	testl	%esi, %esi
	je	.L7
	testl	%edi, %edi
	je	.L16
	pxor	%xmm1, %xmm1
	movl	%eax, -4(%rsp)
	cvtss2sd	%xmm0, %xmm0
	movss	-4(%rsp), %xmm3
	mulsd	%xmm0, %xmm0
	cvtss2sd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm1
	addsd	%xmm1, %xmm0
	sqrtsd	%xmm0, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movss	.LC0(%rip), %xmm1
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm2
	andps	%xmm1, %xmm0
	andps	%xmm1, %xmm2
	mulss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	$4194304, %ecx
	andl	$2147483647, %ecx
	cmpl	$2143289344, %ecx
	ja	.L4
.L7:
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
.L16:
	andps	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	$4194304, %edx
	andl	$2147483647, %edx
	cmpl	$2143289344, %edx
	ja	.L2
	andps	.LC0(%rip), %xmm0
	ret
	.size	__ieee754_hypotf, .-__ieee754_hypotf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
