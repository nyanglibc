	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__remainder
	.type	__remainder, @function
__remainder:
	ucomisd	%xmm0, %xmm0
	jp	.L5
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	ucomisd	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L2
.L5:
	movapd	%xmm0, %xmm2
	andpd	.LC1(%rip), %xmm2
	ucomisd	.LC2(%rip), %xmm2
	jbe	.L4
	ucomisd	%xmm1, %xmm1
	jp	.L4
.L2:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	jne	.L10
.L4:
	jmp	__ieee754_remainder@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$28, %edi
	jmp	__kernel_standard@PLT
	.size	__remainder, .-__remainder
	.weak	drem
	.set	drem,__remainder
	.weak	remainderf32x
	.set	remainderf32x,__remainder
	.weak	remainderf64
	.set	remainderf64,__remainder
	.weak	remainder
	.set	remainder,__remainder
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
