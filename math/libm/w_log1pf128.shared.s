	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__w_log1pf128
	.type	__w_log1pf128, @function
__w_log1pf128:
	subq	$24, %rsp
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L9
.L2:
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	jmp	__log1pf128@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movq	errno@gottpoff(%rip), %rax
	je	.L10
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__w_log1pf128, .-__w_log1pf128
	.weak	log1pf128
	.set	log1pf128,__w_log1pf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
