	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__atan2f
	.type	__atan2f, @function
__atan2f:
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	ucomiss	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L2
	ucomiss	%xmm2, %xmm0
	setnp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	jne	.L19
.L2:
	subq	$24, %rsp
	movss	%xmm2, 4(%rsp)
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_atan2f@PLT
	movss	4(%rsp), %xmm2
	movl	$0, %edx
	ucomiss	%xmm2, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	movss	8(%rsp), %xmm4
	movl	$1, %edx
	ucomiss	%xmm2, %xmm4
	movss	12(%rsp), %xmm3
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L20
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2
	movl	$103, %edi
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	andps	.LC1(%rip), %xmm3
	movss	.LC2(%rip), %xmm2
	ucomiss	%xmm3, %xmm2
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__atan2f, .-__atan2f
	.weak	atan2f32
	.set	atan2f32,__atan2f
	.weak	atan2f
	.set	atan2f,__atan2f
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095039
