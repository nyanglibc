	.text
	.p2align 4,,15
	.globl	__nextup
	.type	__nextup, @function
__nextup:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	shrq	$32, %rax
	movl	%eax, %ecx
	movl	%eax, %esi
	andl	$2147483647, %ecx
	cmpl	$2146435071, %ecx
	jle	.L2
	leal	-2146435072(%rcx), %edi
	orl	%edx, %edi
	jne	.L17
.L2:
	orl	%edx, %ecx
	movsd	.LC0(%rip), %xmm1
	je	.L1
	testl	%eax, %eax
	js	.L4
	movapd	%xmm0, %xmm1
	andpd	.LC1(%rip), %xmm1
	ucomisd	.LC2(%rip), %xmm1
	ja	.L15
	addl	$1, %eax
	addl	$1, %edx
	cmove	%eax, %esi
.L6:
	movl	%edx, %edx
	salq	$32, %rsi
	orq	%rdx, %rsi
	movq	%rsi, -8(%rsp)
	movq	-8(%rsp), %xmm1
.L1:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movapd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movapd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	subl	$1, %eax
	testl	%edx, %edx
	cmove	%eax, %esi
	subl	$1, %edx
	jmp	.L6
	.size	__nextup, .-__nextup
	.weak	nextupf32x
	.set	nextupf32x,__nextup
	.weak	nextupf64
	.set	nextupf64,__nextup
	.weak	nextup
	.set	nextup,__nextup
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
