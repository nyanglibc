	.text
	.p2align 4,,15
	.globl	__totalordermag
	.type	__totalordermag, @function
__totalordermag:
	movq	(%rdi), %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rdx
	andq	(%rsi), %rax
	cmpq	%rax, %rdx
	setbe	%al
	movzbl	%al, %eax
	ret
	.size	__totalordermag, .-__totalordermag
	.weak	totalordermagf32x
	.set	totalordermagf32x,__totalordermag
	.weak	totalordermagf64
	.set	totalordermagf64,__totalordermag
	.weak	totalordermag
	.set	totalordermag,__totalordermag
