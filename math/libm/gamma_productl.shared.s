	.text
	.p2align 4,,15
	.globl	__gamma_productl
	.type	__gamma_productl, @function
__gamma_productl:
#APP
# 383 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstcw -60(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-60(%rsp), %edx
	movl	%edx, %eax
	movw	%dx, -56(%rsp)
	andb	$-16, %ah
	orb	$3, %ah
	cmpw	%ax, %dx
	movw	%ax, -58(%rsp)
	jne	.L14
	fldt	24(%rsp)
	cmpl	$1, %edi
	fldt	8(%rsp)
	fdivrp	%st, %st(1)
	fld	%st(0)
	fstpt	(%rsi)
	jle	.L8
	xorl	%edx, %edx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %eax
	fldt	8(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	fstp	%st(2)
	fxch	%st(1)
.L6:
	movl	%eax, -76(%rsp)
	addl	$1, %eax
	cmpl	%eax, %edi
	fildl	-76(%rsp)
	fldt	8(%rsp)
	faddp	%st, %st(1)
	fld	%st(0)
	fmul	%st(2), %st
	fldl	.LC0(%rip)
	fld	%st(3)
	fmul	%st(1), %st
	fxch	%st(1)
	fmul	%st(3), %st
	fld	%st(4)
	fsub	%st(2), %st
	faddp	%st, %st(2)
	fld	%st(3)
	fsub	%st(1), %st
	faddp	%st, %st(1)
	fxch	%st(4)
	fsub	%st(1), %st
	fld	%st(3)
	fsub	%st(5), %st
	fld	%st(2)
	fmul	%st(6), %st
	fsub	%st(4), %st
	fxch	%st(3)
	fmul	%st(1), %st
	faddp	%st, %st(3)
	fxch	%st(5)
	fmul	%st(1), %st
	faddp	%st, %st(2)
	fmulp	%st, %st(4)
	faddp	%st, %st(3)
	fdivr	%st, %st(2)
	fldt	24(%rsp)
	fdivp	%st, %st(2)
	fxch	%st(3)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fld	%st(1)
	jne	.L15
	fstp	%st(0)
	testb	%dl, %dl
	fstpt	(%rsi)
	jne	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 391 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw -58(%rsp)
# 0 "" 2
#NO_APP
	fldt	24(%rsp)
	cmpl	$1, %edi
	movl	$1, %edx
	fldt	8(%rsp)
	fdivr	%st, %st(1)
	fxch	%st(1)
	fld	%st(0)
	fstpt	(%rsi)
	jg	.L16
	fstp	%st(0)
.L4:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw -56(%rsp)
# 0 "" 2
#NO_APP
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	fstp	%st(0)
	fldt	8(%rsp)
	ret
	.size	__gamma_productl, .-__gamma_productl
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1048576
	.long	1106247680
