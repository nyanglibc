	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fmaxmagl
	.type	__fmaxmagl, @function
__fmaxmagl:
	subq	$40, %rsp
	fldt	48(%rsp)
	fldt	64(%rsp)
	fld	%st(1)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(1), %st
	ja	.L23
	fxch	%st(1)
	fucomi	%st(1), %st
	ja	.L19
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L5
	je	.L22
.L5:
	fstpt	16(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__GI___issignalingl
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	fldt	(%rsp)
	fldt	16(%rsp)
	jne	.L8
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	32(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__GI___issignalingl
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	fldt	(%rsp)
	fldt	16(%rsp)
	jne	.L24
	fxch	%st(1)
	fucomi	%st(0), %st
	fcmovu	%st(1), %st
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	fxch	%st(1)
	fucomi	%st(1), %st
	fcmovbe	%st(1), %st
	fstp	%st(1)
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	fxch	%st(1)
.L8:
	faddp	%st, %st(1)
	addq	$40, %rsp
	ret
	.size	__fmaxmagl, .-__fmaxmagl
	.weak	fmaxmagf64x
	.set	fmaxmagf64x,__fmaxmagl
	.weak	fmaxmagl
	.set	fmaxmagl,__fmaxmagl
