	.text
	.p2align 4,,15
	.globl	__nextdownf
	.type	__nextdownf, @function
__nextdownf:
	subq	$24, %rsp
	movss	.LC0(%rip), %xmm1
	xorps	%xmm1, %xmm0
	movaps	%xmm1, (%rsp)
	call	__nextupf@PLT
	movaps	(%rsp), %xmm1
	addq	$24, %rsp
	xorps	%xmm1, %xmm0
	ret
	.size	__nextdownf, .-__nextdownf
	.weak	nextdownf32
	.set	nextdownf32,__nextdownf
	.weak	nextdownf
	.set	nextdownf,__nextdownf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483648
	.long	0
	.long	0
	.long	0
