.text
.globl __llrintf
.type __llrintf,@function
.align 1<<4
__llrintf:
 cvtss2si %xmm0,%rax
 ret
.size __llrintf,.-__llrintf
.weak llrintf
llrintf = __llrintf
.weak llrintf32
llrintf32 = __llrintf
.globl __lrintf
.set __lrintf,__llrintf
.weak lrintf
lrintf = __llrintf
.weak lrintf32
lrintf32 = __llrintf
