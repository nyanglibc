	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__acosh
	.type	__acosh, @function
__acosh:
	movsd	.LC0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.L4
.L2:
	jmp	__ieee754_acosh@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	movapd	%xmm0, %xmm1
	movl	$29, %edi
	jmp	__kernel_standard@PLT
	.size	__acosh, .-__acosh
	.weak	acoshf32x
	.set	acoshf32x,__acosh
	.weak	acoshf64
	.set	acoshf64,__acosh
	.weak	acosh
	.set	acosh,__acosh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
