	.text
	.globl	__eqtf2
	.globl	__netf2
	.globl	__unordtf2
	.globl	__gttf2
	.p2align 4,,15
	.globl	__atan2f128
	.type	__atan2f128, @function
__atan2f128:
	subq	$56, %rsp
	movaps	%xmm1, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	call	__ieee754_atan2f128@PLT
	pxor	%xmm1, %xmm1
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	16(%rsp), %xmm2
	pxor	%xmm1, %xmm1
	movdqa	%xmm2, %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	je	.L1
	movdqa	32(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jne	.L1
	movdqa	%xmm2, %xmm0
	movdqa	.LC2(%rip), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L15
.L1:
	movdqa	(%rsp), %xmm0
	addq	$56, %rsp
	ret
.L15:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__atan2f128, .-__atan2f128
	.weak	atan2f128
	.set	atan2f128,__atan2f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
