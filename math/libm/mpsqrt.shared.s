	.text
	.p2align 4,,15
	.globl	__mpsqrt
	.type	__mpsqrt, @function
__mpsqrt:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %ebx
	subq	$1736, %rsp
	movl	(%rdi), %edx
	leaq	48(%rsp), %r14
	movq	%rsi, 8(%rsp)
	leaq	720(%rsp), %r13
	movl	%edx, %eax
	movq	%r14, %rsi
	movq	%r14, 24(%rsp)
	shrl	$31, %eax
	addl	%edx, %eax
	movl	%ebx, %edx
	sarl	%eax
	movl	%eax, %r15d
	movl	%eax, 20(%rsp)
	call	__cpy@PLT
	leaq	40(%rsp), %rsi
	movq	%r14, %rdi
	movl	%ebx, %edx
	leal	(%r15,%r15), %eax
	subl	%eax, 48(%rsp)
	call	__mp_dbl@PLT
	movsd	40(%rsp), %xmm4
	movq	%r13, %rdi
	movsd	.LC1(%rip), %xmm2
	movq	%xmm4, %rdx
	movq	%xmm4, %rax
	movsd	.LC5(%rip), %xmm5
	sarq	$32, %rdx
	movl	%eax, %eax
	movl	%edx, %esi
	movsd	.LC6(%rip), %xmm3
	andl	$2097151, %esi
	orl	$1071644672, %esi
	movapd	%xmm3, %xmm6
	movq	%rsi, %rcx
	movapd	%xmm3, %xmm7
	salq	$32, %rcx
	subl	%esi, %edx
	movl	%ebx, %esi
	orq	%rcx, %rax
	sarl	%edx
	movq	%rax, (%rsp)
	movq	(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	mulsd	%xmm5, %xmm1
	subsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm2
	addsd	.LC2(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	subsd	.LC3(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	.LC4(%rip), %xmm2
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm6
	mulsd	%xmm6, %xmm2
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm7
	movapd	%xmm7, %xmm1
	mulsd	%xmm2, %xmm1
	movq	%xmm1, %rcx
	movq	%xmm1, %rax
	sarq	$32, %rcx
	movl	%eax, %eax
	subl	%edx, %ecx
	movq	%rcx, %rdx
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rax, (%rsp)
	movq	(%rsp), %xmm0
	mulsd	%xmm0, %xmm5
	mulsd	%xmm0, %xmm4
	mulsd	%xmm5, %xmm4
	subsd	%xmm4, %xmm3
	mulsd	%xmm3, %xmm0
	call	__dbl_mp@PLT
	leaq	384(%rsp), %rax
	leaq	mphalf.1990(%rip), %rsi
	movq	%r14, %rdi
	movl	%ebx, %ecx
	movq	%rax, %rdx
	movq	%rax, (%rsp)
	call	__mul@PLT
	leaq	__mpsqrt_mp(%rip), %rax
	movslq	%ebx, %rdx
	movl	(%rax,%rdx,4), %r14d
	testl	%r14d, %r14d
	jle	.L2
	leaq	1056(%rsp), %r12
	leaq	1392(%rsp), %rbp
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__sqr@PLT
	movq	(%rsp), %rsi
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r12, %rdi
	addl	$1, %r15d
	call	__mul@PLT
	leaq	mp3halfs.1991(%rip), %rdi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	__sub@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__mul@PLT
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__cpy@PLT
	cmpl	%r15d, %r14d
	jne	.L3
.L2:
	movl	%ebx, %ecx
	movq	8(%rsp), %rbx
	movq	24(%rsp), %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	call	__mul@PLT
	movl	20(%rsp), %ecx
	addl	%ecx, (%rbx)
	addq	$1736, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__mpsqrt, .-__mpsqrt
	.section	.rodata
	.align 32
	.type	mp3halfs.1991, @object
	.size	mp3halfs.1991, 328
mp3halfs.1991:
	.long	1
	.zero	4
	.quad	1
	.quad	1
	.quad	8388608
	.zero	296
	.align 32
	.type	mphalf.1990, @object
	.size	mphalf.1990, 328
mphalf.1990:
	.long	0
	.zero	4
	.quad	1
	.quad	8388608
	.zero	304
	.hidden	__mpsqrt_mp
	.globl	__mpsqrt_mp
	.align 32
	.type	__mpsqrt_mp, @object
	.size	__mpsqrt_mp, 132
__mpsqrt_mp:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.align 8
.LC1:
	.long	2930198488
	.long	-1077176708
	.align 8
.LC2:
	.long	3929379680
	.long	1071454753
	.align 8
.LC3:
	.long	3167967878
	.long	1071715555
	.align 8
.LC4:
	.long	1221832296
	.long	1072686411
	.align 8
.LC5:
	.long	0
	.long	1071644672
	.align 8
.LC6:
	.long	0
	.long	1073217536
