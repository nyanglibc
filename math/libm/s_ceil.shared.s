	.text
	.p2align 4,,15
	.globl	__ceil
	.type	__ceil, @function
__ceil:
	movq	%xmm0, %rcx
	movq	%xmm0, %rax
	sarq	$52, %rcx
	andl	$2047, %ecx
	subl	$1023, %ecx
	cmpl	$51, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L14
	movabsq	$4503599627370495, %rdx
	sarq	%cl, %rdx
	testq	%rdx, %rax
	je	.L1
	testq	%rax, %rax
	jle	.L6
	movabsq	$4503599627370496, %rsi
	shrq	%cl, %rsi
	addq	%rsi, %rax
.L6:
	notq	%rdx
	andq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1024, %ecx
	je	.L15
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rax, %rax
	js	.L7
	pxor	%xmm0, %xmm0
	movq	%xmm0, %rax
	cmovne	.LC1(%rip), %rax
	movq	%rax, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movsd	.LC2(%rip), %xmm0
	ret
	.size	__ceil, .-__ceil
	.weak	ceilf32x
	.set	ceilf32x,__ceil
	.weak	ceilf64
	.set	ceilf64,__ceil
	.weak	ceil
	.set	ceil,__ceil
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	-2147483648
