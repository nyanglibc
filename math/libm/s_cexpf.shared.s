	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cexpf
	.type	__cexpf, @function
__cexpf:
	pushq	%rbx
	subq	$80, %rsp
	movq	%xmm0, 56(%rsp)
	movss	.LC4(%rip), %xmm4
	movss	56(%rsp), %xmm3
	movaps	%xmm3, %xmm1
	movss	60(%rsp), %xmm0
	movaps	%xmm0, %xmm2
	andps	%xmm4, %xmm1
	andps	%xmm4, %xmm2
	ucomiss	%xmm1, %xmm1
	jp	.L2
	ucomiss	.LC5(%rip), %xmm1
	jbe	.L68
	ucomiss	%xmm2, %xmm2
	jp	.L36
	ucomiss	.LC5(%rip), %xmm2
	ja	.L36
	ucomiss	.LC6(%rip), %xmm2
	movaps	%xmm0, %xmm6
	jnb	.L11
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jp	.L14
	je	.L12
.L14:
	movd	%xmm3, %eax
	testl	%eax, %eax
	jns	.L66
.L47:
	pxor	%xmm3, %xmm3
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L68:
	ucomiss	%xmm2, %xmm2
	movaps	%xmm3, %xmm5
	jp	.L8
	ucomiss	.LC5(%rip), %xmm2
	jbe	.L69
.L8:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movss	.LC3(%rip), %xmm1
	movaps	%xmm1, %xmm0
.L1:
	movss	%xmm1, 48(%rsp)
	movss	%xmm0, 52(%rsp)
	movq	48(%rsp), %xmm0
	addq	$80, %rsp
	popq	%rbx
	ret
.L69:
	movsd	.LC8(%rip), %xmm1
	ucomiss	.LC6(%rip), %xmm2
	mulsd	.LC7(%rip), %xmm1
	cvttsd2si	%xmm1, %ebx
	jbe	.L57
	leaq	76(%rsp), %rsi
	leaq	72(%rsp), %rdi
	movss	%xmm3, 16(%rsp)
	movaps	%xmm4, 32(%rsp)
	movss	%xmm5, (%rsp)
	call	__sincosf@PLT
	movss	(%rsp), %xmm5
	movaps	32(%rsp), %xmm4
	movss	16(%rsp), %xmm3
.L18:
	pxor	%xmm2, %xmm2
	cvtsi2ss	%ebx, %xmm2
	ucomiss	%xmm2, %xmm3
	ja	.L70
.L58:
	movaps	%xmm5, %xmm0
	movaps	%xmm4, (%rsp)
	call	__ieee754_expf@PLT
	movss	76(%rsp), %xmm1
	mulss	%xmm0, %xmm1
	movaps	(%rsp), %xmm4
	mulss	72(%rsp), %xmm0
.L24:
	movaps	%xmm1, %xmm3
	movss	.LC6(%rip), %xmm2
	andps	%xmm4, %xmm3
	ucomiss	%xmm3, %xmm2
	jbe	.L25
	movaps	%xmm1, %xmm3
	mulss	%xmm1, %xmm3
.L25:
	andps	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm2
	jbe	.L1
	movaps	%xmm0, %xmm2
	mulss	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L70:
	movaps	%xmm2, %xmm0
	movss	%xmm3, 16(%rsp)
	movaps	%xmm4, 32(%rsp)
	movss	%xmm2, (%rsp)
	call	__ieee754_expf@PLT
	movss	16(%rsp), %xmm3
	movaps	%xmm3, %xmm5
	movss	(%rsp), %xmm2
	movss	72(%rsp), %xmm1
	subss	%xmm2, %xmm5
	movss	76(%rsp), %xmm3
	mulss	%xmm0, %xmm1
	movaps	%xmm0, %xmm6
	mulss	%xmm0, %xmm3
	movaps	32(%rsp), %xmm4
	ucomiss	%xmm2, %xmm5
	movss	%xmm1, 72(%rsp)
	movss	%xmm3, 76(%rsp)
	jbe	.L58
	subss	%xmm2, %xmm5
	movaps	%xmm1, %xmm0
	mulss	%xmm6, %xmm3
	mulss	%xmm6, %xmm0
	ucomiss	%xmm2, %xmm5
	movaps	%xmm3, %xmm1
	movss	%xmm3, 76(%rsp)
	movss	%xmm0, 72(%rsp)
	jbe	.L58
	movss	.LC5(%rip), %xmm2
	mulss	%xmm2, %xmm1
	mulss	%xmm2, %xmm0
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L11:
	movd	%xmm3, %eax
	testl	%eax, %eax
	js	.L47
.L66:
	movss	.LC1(%rip), %xmm3
.L41:
	ucomiss	.LC6(%rip), %xmm2
	jbe	.L59
	leaq	76(%rsp), %rsi
	leaq	72(%rsp), %rdi
	movss	%xmm3, (%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__sincosf@PLT
	movss	76(%rsp), %xmm1
	movss	72(%rsp), %xmm6
	movss	(%rsp), %xmm3
.L30:
	movss	.LC9(%rip), %xmm0
	movaps	%xmm1, %xmm5
	movaps	%xmm0, %xmm7
	andps	%xmm0, %xmm5
	andnps	%xmm3, %xmm7
	orps	%xmm5, %xmm7
	movaps	%xmm7, %xmm1
	movaps	%xmm6, %xmm7
	andps	%xmm0, %xmm7
	andnps	%xmm3, %xmm0
	orps	%xmm7, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movd	%xmm3, %eax
	testl	%eax, %eax
	js	.L1
	movss	.LC1(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm2, %xmm2
	jp	.L71
	ucomiss	.LC5(%rip), %xmm2
	ja	.L8
	ucomiss	.LC6(%rip), %xmm2
	jnb	.L8
	ucomiss	.LC0(%rip), %xmm0
	jp	.L8
	jne	.L8
	movss	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movss	%xmm0, 72(%rsp)
	movl	$0x3f800000, 76(%rsp)
	jmp	.L18
.L59:
	movss	.LC2(%rip), %xmm1
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	movd	%xmm3, %eax
	testl	%eax, %eax
	js	.L72
	movss	.LC1(%rip), %xmm1
	subss	%xmm0, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L72:
	pxor	%xmm1, %xmm1
	andps	.LC9(%rip), %xmm0
	jmp	.L1
.L71:
	movss	.LC3(%rip), %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1
	.size	__cexpf, .-__cexpf
	.weak	cexpf32
	.set	cexpf32,__cexpf
	.weak	cexpf
	.set	cexpf,__cexpf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	2139095040
	.align 4
.LC2:
	.long	1065353216
	.align 4
.LC3:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	2139095039
	.align 4
.LC6:
	.long	8388608
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1080016896
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	2147483648
	.long	0
	.long	0
	.long	0
