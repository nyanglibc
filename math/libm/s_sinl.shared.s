	.text
	.p2align 4,,15
	.globl	__sinl
	.type	__sinl, @function
__sinl:
	subq	$40, %rsp
	movq	56(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16381, %ax
	jle	.L2
	movq	48(%rsp), %rdx
	shrq	$32, %rdx
	cmpw	$16382, %ax
	jne	.L3
	cmpl	$-921707870, %edx
	ja	.L3
.L2:
	pushq	$0
	pushq	$0
	xorl	%edi, %edi
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__kernel_sinl@PLT
	addq	$32, %rsp
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpw	$32767, %ax
	jne	.L5
	movq	48(%rsp), %rax
	testl	%eax, %eax
	jne	.L6
	cmpl	$-2147483648, %edx
	jne	.L6
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L6:
	fldt	48(%rsp)
	addq	$40, %rsp
	fsub	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rsp, %rdi
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__ieee754_rem_pio2l@PLT
	andl	$3, %eax
	cmpl	$1, %eax
	popq	%rdx
	popq	%rcx
	je	.L8
	cmpl	$2, %eax
	je	.L9
	testl	%eax, %eax
	je	.L27
	pushq	24(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__kernel_cosl@PLT
	addq	$32, %rsp
	fchs
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	pushq	24(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__kernel_cosl@PLT
	addq	$32, %rsp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	pushq	24(%rsp)
	pushq	24(%rsp)
	movl	$1, %edi
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__kernel_sinl@PLT
	addq	$32, %rsp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	pushq	24(%rsp)
	pushq	24(%rsp)
	movl	$1, %edi
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__kernel_sinl@PLT
	addq	$32, %rsp
	fchs
	jmp	.L1
	.size	__sinl, .-__sinl
	.weak	sinf64x
	.set	sinf64x,__sinl
	.weak	sinl
	.set	sinl,__sinl
