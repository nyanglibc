	.text
	.p2align 4,,15
	.globl	__lgammaf
	.type	__lgammaf, @function
__lgammaf:
	leaq	__signgam(%rip), %rdi
	subq	$24, %rsp
	movss	%xmm0, 12(%rsp)
	call	__ieee754_lgammaf_r@PLT
	movss	.LC0(%rip), %xmm3
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm2
	andps	%xmm3, %xmm4
	movss	12(%rsp), %xmm1
	ucomiss	%xmm4, %xmm2
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andps	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm2
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__lgammaf, .-__lgammaf
	.globl	__gammaf
	.set	__gammaf,__lgammaf
	.weak	gammaf
	.set	gammaf,__gammaf
	.weak	lgammaf32
	.set	lgammaf32,__lgammaf
	.weak	lgammaf
	.set	lgammaf,__lgammaf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
