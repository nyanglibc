	.text
	.p2align 4,,15
	.globl	__asinf
	.type	__asinf, @function
__asinf:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L4
	jmp	__ieee754_asinf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_asinf@PLT
	.size	__asinf, .-__asinf
	.weak	asinf32
	.set	asinf32,__asinf
	.weak	asinf
	.set	asinf,__asinf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
