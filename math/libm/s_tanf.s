	.text
	.p2align 4,,15
	.globl	__tanf
	.type	__tanf, @function
__tanf:
	movd	%xmm0, %eax
	movd	%xmm0, %esi
	andl	$2147483647, %eax
	cmpl	$1061752794, %eax
	jle	.L13
	cmpl	$2139095039, %eax
	jle	.L3
	cmpl	$2139095040, %eax
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L4:
	subss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movd	%xmm0, %eax
	pxor	%xmm1, %xmm1
	shrl	$20, %eax
	andl	$2047, %eax
	cvtss2sd	%xmm0, %xmm1
	cmpl	$1070, %eax
	ja	.L6
	movsd	32+__sincosf_table(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %edx
	pxor	%xmm0, %xmm0
	addl	$8388608, %edx
	sarl	$24, %edx
	cvtsi2sd	%edx, %xmm0
	mulsd	40+__sincosf_table(%rip), %xmm0
	subsd	%xmm0, %xmm1
.L7:
	pxor	%xmm0, %xmm0
	addl	%edx, %edx
	movl	$1, %edi
	pxor	%xmm2, %xmm2
	andl	$2, %edx
	subl	%edx, %edi
	cvtsd2ss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm2
	subsd	%xmm2, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	jmp	__kernel_tanf@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	pxor	%xmm1, %xmm1
	movl	$1, %edi
	jmp	__kernel_tanf@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	movd	%xmm0, %edi
	movd	%xmm0, %edx
	leaq	__inv_pio4(%rip), %rax
	movd	%xmm0, %ecx
	pxor	%xmm1, %xmm1
	shrl	$26, %edi
	andl	$8388607, %edx
	andl	$15, %edi
	shrl	$23, %ecx
	orl	$8388608, %edx
	leaq	(%rax,%rdi,4), %r8
	andl	$7, %ecx
	sall	%cl, %edx
	movl	%edx, %ecx
	imull	(%rax,%rdi,4), %edx
	movl	32(%r8), %eax
	imulq	%rcx, %rax
	salq	$32, %rdx
	shrq	$32, %rax
	orq	%rax, %rdx
	movl	16(%r8), %eax
	imulq	%rcx, %rax
	movabsq	$2305843009213693952, %rcx
	addq	%rdx, %rax
	addq	%rax, %rcx
	movq	%rcx, %rdi
	shrq	$62, %rdi
	movq	%rdi, %rdx
	movabsq	$-4611686018427387904, %rdi
	andq	%rdi, %rcx
	subq	%rcx, %rax
	testl	%esi, %esi
	cvtsi2sdq	%rax, %xmm1
	mulsd	.LC1(%rip), %xmm1
	jns	.L7
	xorpd	.LC2(%rip), %xmm1
	jmp	.L7
	.size	__tanf, .-__tanf
	.weak	tanf32
	.set	tanf32,__tanf
	.weak	tanf
	.set	tanf,__tanf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1413754136
	.long	1008280059
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
