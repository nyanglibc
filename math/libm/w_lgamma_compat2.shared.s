	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __lgamma,lgamma@@GLIBC_2.23
#NO_APP
	.p2align 4,,15
	.globl	__lgamma
	.type	__lgamma, @function
__lgamma:
	subq	$24, %rsp
	movq	__signgam@GOTPCREL(%rip), %rdi
	movsd	%xmm0, 8(%rsp)
	call	__ieee754_lgamma_r@PLT
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm3
	movsd	8(%rsp), %xmm2
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	jb	.L15
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movapd	%xmm2, %xmm4
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movapd	%xmm2, %xmm0
	movsd	.LC2(%rip), %xmm4
	movapd	%xmm2, %xmm3
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L4
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm4
	andnpd	%xmm2, %xmm1
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm3
	cmpnlesd	%xmm2, %xmm3
	andpd	%xmm4, %xmm3
	subsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm3
	orpd	%xmm1, %xmm3
.L4:
	ucomisd	%xmm3, %xmm2
	jp	.L8
	jne	.L8
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	ucomisd	%xmm2, %xmm0
	setnb	%dil
	addl	$14, %edi
.L5:
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$14, %edi
	jmp	.L5
	.size	__lgamma, .-__lgamma
	.weak	lgammaf32x
	.set	lgammaf32x,__lgamma
	.weak	lgammaf64
	.set	lgammaf64,__lgamma
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	1127219200
	.align 8
.LC3:
	.long	0
	.long	1072693248
