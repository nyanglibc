	.text
	.globl	__fixtfsi
	.globl	__multf3
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__kernel_cosf128
	.type	__kernel_cosf128, @function
__kernel_cosf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$48, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1073491967, %eax
	ja	.L2
	cmpl	$1069940735, %eax
	ja	.L3
	call	__fixtfsi@PLT
	testl	%eax, %eax
	je	.L10
.L3:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC0(%rip), %xmm1
	call	__addtf3@PLT
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%eax, %edx
	movl	$16382, %ecx
	shrl	$16, %edx
	movaps	(%rsp), %xmm4
	subl	%edx, %ecx
	movl	$512, %edx
	sall	%cl, %edx
	movdqa	%xmm1, %xmm2
	addl	%edx, %eax
	movl	$-1024, %edx
	sall	%cl, %edx
	andl	%edx, %eax
	movmskps	%xmm4, %edx
	andl	$8, %edx
	je	.L5
	movdqa	.LC9(%rip), %xmm0
	movdqa	(%rsp), %xmm5
	pxor	%xmm0, %xmm2
	pxor	%xmm0, %xmm5
	movaps	%xmm5, (%rsp)
.L5:
	testl	%ecx, %ecx
	je	.L7
	cmpl	$1, %ecx
	je	.L8
	leal	-1073491968(%rax), %ebx
	shrl	$10, %ebx
.L9:
	salq	$32, %rax
	movq	$0, 16(%rsp)
	leaq	__sincosf128_table(%rip), %r12
	movq	%rax, 24(%rsp)
	leal	1(%rbx), %ebp
	movdqa	(%rsp), %xmm1
	salq	$4, %rbp
	movdqa	16(%rsp), %xmm0
	addq	%r12, %rbp
	movaps	%xmm2, 32(%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__multf3@PLT
	movl	%ebx, %eax
	addl	$2, %ebx
	salq	$4, %rax
	salq	$4, %rbx
	movdqa	(%r12,%rax), %xmm3
	movdqa	.LC10(%rip), %xmm1
	movaps	%xmm3, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC0(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movdqa	(%r12,%rbx), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC15(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	0(%rbp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leal	-1073518592(%rax), %ebx
	shrl	$9, %ebx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L7:
	leal	-1073564672(%rax), %ebx
	shrl	$8, %ebx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	.LC0(%rip), %xmm0
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__kernel_cosf128, .-__kernel_cosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC1:
	.long	1159938380
	.long	3405235195
	.long	2174208973
	.long	1070771807
	.align 16
.LC2:
	.long	3393754187
	.long	1612633044
	.long	1231626921
	.long	1071289239
	.align 16
.LC3:
	.long	2919187315
	.long	512910039
	.long	2399111197
	.long	1071783661
	.align 16
.LC4:
	.long	541373727
	.long	2860597922
	.long	4218915317
	.long	1072244708
	.align 16
.LC5:
	.long	3874739109
	.long	2686016747
	.long	27269633
	.long	1072668698
	.align 16
.LC6:
	.long	1178467597
	.long	1813430634
	.long	3245086401
	.long	1073048598
	.align 16
.LC7:
	.long	1431400240
	.long	1431655765
	.long	1431655765
	.long	1073370453
	.align 16
.LC8:
	.long	4294967291
	.long	4294967295
	.long	4294967295
	.long	1073610751
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC10:
	.long	3881510791
	.long	3698426781
	.long	1323895879
	.long	-1075466652
	.align 16
.LC11:
	.long	2852301633
	.long	208317009
	.long	978676836
	.long	1072460254
	.align 16
.LC12:
	.long	503844232
	.long	2686052114
	.long	27269633
	.long	1072865306
	.align 16
.LC13:
	.long	285088093
	.long	286331153
	.long	286331153
	.long	1073221905
	.align 16
.LC14:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073501525
	.align 16
.LC15:
	.long	2270477792
	.long	3406517392
	.long	4124894775
	.long	-1075238940
	.align 16
.LC16:
	.long	762139716
	.long	48035802
	.long	27269614
	.long	1072668698
	.align 16
.LC17:
	.long	3828089717
	.long	1813423462
	.long	3245086401
	.long	1073048598
	.align 16
.LC18:
	.long	1429819427
	.long	1431655765
	.long	1431655765
	.long	1073370453
	.align 16
.LC19:
	.long	0
	.long	0
	.long	0
	.long	1073610752
