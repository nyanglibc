	.text
	.p2align 4,,15
	.globl	__cacoshf
	.type	__cacoshf, @function
__cacoshf:
	subq	$72, %rsp
	movq	%xmm0, 56(%rsp)
	movss	.LC7(%rip), %xmm3
	movss	56(%rsp), %xmm4
	movaps	%xmm4, %xmm0
	movss	60(%rsp), %xmm1
	movaps	%xmm1, %xmm2
	andps	%xmm3, %xmm0
	andps	%xmm3, %xmm2
	ucomiss	%xmm0, %xmm0
	jp	.L2
	ucomiss	.LC8(%rip), %xmm0
	jbe	.L43
	ucomiss	%xmm2, %xmm2
	jp	.L40
	ucomiss	.LC8(%rip), %xmm2
	ja	.L44
	movd	%xmm4, %eax
	pxor	%xmm2, %xmm2
	testl	%eax, %eax
	js	.L45
.L15:
	movaps	%xmm1, %xmm6
	andps	%xmm3, %xmm2
	movss	.LC6(%rip), %xmm0
	andps	.LC10(%rip), %xmm6
	orps	%xmm6, %xmm2
	movaps	%xmm2, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L43:
	ucomiss	.LC9(%rip), %xmm0
	jnb	.L4
	pxor	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm4
	jp	.L4
	jne	.L4
	ucomiss	%xmm2, %xmm2
	jp	.L46
	ucomiss	.LC8(%rip), %xmm2
	ja	.L27
	ucomiss	.LC9(%rip), %xmm2
	jb	.L47
.L9:
	movss	.LC10(%rip), %xmm2
	movaps	%xmm1, %xmm0
	movss	%xmm4, 52(%rsp)
	movl	$1, %edi
	xorps	%xmm2, %xmm0
	movss	%xmm1, 28(%rsp)
	movaps	%xmm2, (%rsp)
	movss	%xmm0, 48(%rsp)
	movq	48(%rsp), %xmm0
	call	__kernel_casinhf@PLT
	movss	28(%rsp), %xmm1
	movd	%xmm1, %eax
	movq	%xmm0, 40(%rsp)
	movaps	(%rsp), %xmm2
	testl	%eax, %eax
	movss	40(%rsp), %xmm0
	movss	44(%rsp), %xmm3
	js	.L48
	xorps	%xmm2, %xmm0
	movaps	%xmm3, %xmm1
.L1:
	movss	%xmm0, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movq	32(%rsp), %xmm0
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	ucomiss	%xmm2, %xmm2
	jp	.L37
	ucomiss	.LC8(%rip), %xmm2
	jbe	.L9
.L27:
	movss	.LC1(%rip), %xmm2
.L8:
	movaps	%xmm1, %xmm5
	andps	%xmm3, %xmm2
	movss	.LC6(%rip), %xmm0
	andps	.LC10(%rip), %xmm5
	orps	%xmm5, %xmm2
	movaps	%xmm2, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm2, %xmm2
	jp	.L37
	ucomiss	.LC8(%rip), %xmm2
	jbe	.L37
.L40:
	movss	.LC6(%rip), %xmm0
	movss	.LC5(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L48:
	xorps	%xmm2, %xmm3
	movaps	%xmm3, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L45:
	movss	.LC3(%rip), %xmm2
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L47:
	ucomiss	%xmm0, %xmm1
	jp	.L9
	jne	.L9
	andps	.LC10(%rip), %xmm1
	orps	.LC11(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	movss	.LC5(%rip), %xmm0
	movaps	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L44:
	pxor	%xmm0, %xmm0
	movss	.LC2(%rip), %xmm2
	ucomiss	%xmm4, %xmm0
	ja	.L8
	movss	.LC0(%rip), %xmm2
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L46:
	movss	.LC5(%rip), %xmm0
	movss	.LC1(%rip), %xmm1
	jmp	.L1
	.size	__cacoshf, .-__cacoshf
	.weak	cacoshf32
	.set	cacoshf32,__cacoshf
	.weak	cacoshf
	.set	cacoshf,__cacoshf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1061752795
	.align 4
.LC1:
	.long	1070141403
	.align 4
.LC2:
	.long	1075235812
	.align 4
.LC3:
	.long	1078530011
	.align 4
.LC5:
	.long	2143289344
	.align 4
.LC6:
	.long	2139095040
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC8:
	.long	2139095039
	.align 4
.LC9:
	.long	8388608
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 16
.LC11:
	.long	1070141403
	.long	0
	.long	0
	.long	0
