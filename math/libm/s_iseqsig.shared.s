	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__iseqsig
	.type	__iseqsig, @function
__iseqsig:
	ucomisd	%xmm0, %xmm1
	setnb	%al
	ucomisd	%xmm1, %xmm0
	setnb	%dl
	testb	%al, %al
	je	.L7
	testb	%dl, %dl
	movl	$1, %eax
	je	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	testb	%dl, %dl
	je	.L13
.L5:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	subq	$8, %rsp
	movl	$1, %edi
	call	__GI___feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__iseqsig, .-__iseqsig
