	.text
	.globl	__multf3
	.globl	__divtf3
	.p2align 4,,15
	.globl	__ieee754_fmodf128
	.type	__ieee754_fmodf128, @function
__ieee754_fmodf128:
	pushq	%r12
	pushq	%rbp
	movabsq	$9223372036854775807, %rax
	pushq	%rbx
	subq	$32, %rsp
	movaps	%xmm0, 16(%rsp)
	movq	24(%rsp), %r8
	movaps	%xmm1, (%rsp)
	movq	%r8, %rbp
	andq	%rax, %rbp
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	andq	%rsi, %rax
	movq	%rax, %rcx
	orq	%rdi, %rcx
	je	.L2
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rbp
	jle	.L49
.L2:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
.L1:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rdi, %rdx
	movabsq	$9223090561878065152, %rcx
	negq	%rdx
	orq	%rdi, %rdx
	shrq	$63, %rdx
	orq	%rax, %rdx
	cmpq	%rcx, %rdx
	ja	.L2
	movq	16(%rsp), %r12
	movabsq	$-9223372036854775808, %r10
	movq	%rdi, %rbx
	andq	%r8, %r10
	cmpq	%rax, %rbp
	movq	%r12, %r9
	jg	.L5
	jl	.L37
	cmpq	%rdi, %r12
	jb	.L37
	cmpq	%rdi, %r12
	je	.L30
.L5:
	movabsq	$281474976710655, %rdx
	cmpq	%rdx, %rbp
	jg	.L6
	testq	%rbp, %rbp
	jne	.L7
	testq	%r12, %r12
	movq	%r12, %rcx
	movq	$-16431, %rdx
	jle	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	addq	%rcx, %rcx
	subq	$1, %rdx
	testq	%rcx, %rcx
	jg	.L9
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbp, %rdx
	sarq	$48, %rdx
	subq	$16383, %rdx
.L8:
	movabsq	$281474976710655, %rcx
	cmpq	%rcx, %rax
	jg	.L11
	testq	%rax, %rax
	jne	.L12
	testq	%rdi, %rdi
	movq	%rdi, %rcx
	movq	$-16431, %r11
	jle	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	addq	%rcx, %rcx
	subq	$1, %r11
	testq	%rcx, %rcx
	jg	.L14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rax, %r11
	sarq	$48, %r11
	subq	$16383, %r11
.L13:
	cmpq	$-16382, %rdx
	jl	.L16
	movabsq	$281474976710655, %rcx
	andq	%rcx, %r8
	addq	$1, %rcx
	orq	%rcx, %r8
.L17:
	cmpq	$-16382, %r11
	jl	.L19
	movabsq	$281474976710655, %rax
	andq	%rax, %rsi
	addq	$1, %rax
	orq	%rax, %rsi
.L20:
	subq	%r11, %rdx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rax, %rcx
	orq	%rdi, %rcx
	je	.L30
	movq	%rdi, %rcx
	leaq	(%rdi,%rdi), %r9
	shrq	$63, %rcx
	leaq	(%rcx,%rax,2), %r8
.L25:
	subq	$1, %rdx
.L22:
	movq	%r8, %rax
	movq	%r9, %rdi
	subq	%rsi, %rax
	subq	%rbx, %rdi
	cmpq	%rbx, %r9
	sbbq	$0, %rax
	testq	%rdx, %rdx
	je	.L50
	testq	%rax, %rax
	jns	.L24
	movq	%r9, %rax
	addq	%r9, %r9
	shrq	$63, %rax
	leaq	(%rax,%r8,2), %r8
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L50:
	testq	%rax, %rax
	jns	.L29
	movq	%r9, %rdi
	movq	%r8, %rax
.L29:
	movq	%rax, %rbx
	orq	%rdi, %rbx
	jne	.L51
.L30:
	shrq	$63, %r10
	leaq	Zero(%rip), %rax
	salq	$4, %r10
	movdqa	(%rax,%r10), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movq	$-16382, %r9
	subq	%rdx, %r9
	cmpq	$63, %r9
	jg	.L18
	movl	$64, %ecx
	movq	%r12, %r8
	subl	%r9d, %ecx
	shrq	%cl, %r8
	movl	%r9d, %ecx
	salq	%cl, %rbp
	salq	%cl, %r12
	orq	%rbp, %r8
	movq	%r12, %r9
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	movq	$-16382, %rbx
	subq	%r11, %rbx
	cmpq	$63, %rbx
	jg	.L21
	movl	$64, %ecx
	movq	%rdi, %rsi
	subl	%ebx, %ecx
	shrq	%cl, %rsi
	movl	%ebx, %ecx
	salq	%cl, %rax
	salq	%cl, %rdi
	orq	%rax, %rsi
	movq	%rdi, %rbx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L51:
	movabsq	$281474976710655, %rdx
	cmpq	%rdx, %rax
	movq	%rdx, %rcx
	jg	.L32
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%rdi, %rdx
	subq	$1, %r11
	addq	%rdi, %rdi
	shrq	$63, %rdx
	leaq	(%rdx,%rax,2), %rax
	cmpq	%rcx, %rax
	jle	.L31
.L32:
	cmpq	$-16382, %r11
	jge	.L52
	movq	$-16382, %rdx
	subq	%r11, %rdx
	cmpq	$48, %rdx
	jg	.L34
	movq	%rdi, %rsi
	movl	%edx, %ecx
	movq	%rax, %rdi
	shrq	%cl, %rsi
	movl	$64, %ecx
	subl	%edx, %ecx
	salq	%cl, %rdi
	movl	%edx, %ecx
	sarq	%cl, %rax
	orq	%rsi, %rdi
	orq	%rax, %r10
.L35:
	movq	%r10, 8(%rsp)
	movq	%rdi, (%rsp)
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	leal	-64(%rbx), %ecx
	movq	%rdi, %rsi
	xorl	%ebx, %ebx
	salq	%cl, %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L18:
	leal	-64(%r9), %ecx
	movq	%r12, %r8
	xorl	%r9d, %r9d
	salq	%cl, %r8
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rax, %rcx
	movq	$-16382, %r11
	salq	$15, %rcx
	.p2align 4,,10
	.p2align 3
.L15:
	addq	%rcx, %rcx
	subq	$1, %r11
	testq	%rcx, %rcx
	jg	.L15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rbp, %rcx
	movq	$-16382, %rdx
	salq	$15, %rcx
	.p2align 4,,10
	.p2align 3
.L10:
	addq	%rcx, %rcx
	subq	$1, %rdx
	testq	%rcx, %rcx
	jg	.L10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L37:
	movdqa	16(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	movabsq	$-281474976710656, %rdx
	addq	$16383, %r11
	movq	%rdi, (%rsp)
	addq	%rdx, %rax
	salq	$48, %r11
	orq	%rax, %r10
	orq	%r10, %r11
	movq	%r11, 8(%rsp)
	movdqa	(%rsp), %xmm0
	jmp	.L1
.L34:
	cmpq	$63, %rdx
	jg	.L36
	movl	%edx, %ecx
	shrq	%cl, %rdi
	movl	$64, %ecx
	subl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %rdi
	jmp	.L35
.L36:
	leal	-64(%rdx), %ecx
	sarq	%cl, %rax
	movq	%rax, %rdi
	jmp	.L35
	.size	__ieee754_fmodf128, .-__ieee754_fmodf128
	.section	.rodata
	.align 32
	.type	Zero, @object
	.size	Zero, 32
Zero:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	-2147483648
