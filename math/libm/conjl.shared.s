	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__conjl
	.type	__conjl, @function
__conjl:
	fldt	24(%rsp)
	fchs
	fldt	8(%rsp)
	ret
	.size	__conjl, .-__conjl
	.weak	conjf64x
	.set	conjf64x,__conjl
	.weak	conjl
	.set	conjl,__conjl
