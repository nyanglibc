	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __lgammaf,lgammaf@@GLIBC_2.23
#NO_APP
	.p2align 4,,15
	.globl	__lgammaf
	.type	__lgammaf, @function
__lgammaf:
	subq	$24, %rsp
	movq	__signgam@GOTPCREL(%rip), %rdi
	movss	%xmm0, 12(%rsp)
	call	__ieee754_lgammaf_r@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm4
	movss	12(%rsp), %xmm2
	ucomiss	%xmm4, %xmm3
	jb	.L15
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movaps	%xmm2, %xmm4
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movaps	%xmm2, %xmm0
	movss	.LC2(%rip), %xmm4
	movaps	%xmm2, %xmm3
	andps	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm4
	jbe	.L4
	cvttss2si	%xmm2, %eax
	pxor	%xmm0, %xmm0
	movss	.LC3(%rip), %xmm4
	andnps	%xmm2, %xmm1
	cvtsi2ss	%eax, %xmm0
	movaps	%xmm0, %xmm3
	cmpnless	%xmm2, %xmm3
	andps	%xmm4, %xmm3
	subss	%xmm3, %xmm0
	movaps	%xmm0, %xmm3
	orps	%xmm1, %xmm3
.L4:
	ucomiss	%xmm3, %xmm2
	jp	.L8
	jne	.L8
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	ucomiss	%xmm2, %xmm0
	setnb	%dil
	addl	$114, %edi
.L5:
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$114, %edi
	jmp	.L5
	.size	__lgammaf, .-__lgammaf
	.weak	lgammaf32
	.set	lgammaf32,__lgammaf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	1258291200
	.align 4
.LC3:
	.long	1065353216
