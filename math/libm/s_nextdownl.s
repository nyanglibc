	.text
	.p2align 4,,15
	.globl	__nextdownl
	.type	__nextdownl, @function
__nextdownl:
	subq	$24, %rsp
	fldt	32(%rsp)
	fchs
	fstpt	(%rsp)
	call	__nextupl@PLT
	addq	$24, %rsp
	fchs
	ret
	.size	__nextdownl, .-__nextdownl
	.weak	nextdownf64x
	.set	nextdownf64x,__nextdownl
	.weak	nextdownl
	.set	nextdownl,__nextdownl
