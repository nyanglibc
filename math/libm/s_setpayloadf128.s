	.text
	.p2align 4,,15
	.globl	__setpayloadf128
	.type	__setpayloadf128, @function
__setpayloadf128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rsi
	movq	%rsi, %rdx
	shrq	$48, %rdx
	cmpq	$16493, %rdx
	ja	.L8
	cmpq	$16382, %rdx
	movq	-24(%rsp), %rax
	ja	.L3
	movq	%rsi, %rcx
	orq	%rax, %rcx
	jne	.L8
	movl	$16495, %r8d
	subl	%edx, %r8d
.L13:
	movl	$16431, %ecx
	movq	$-1, %r9
	subl	%edx, %ecx
	salq	%cl, %r9
	notq	%r9
	testq	%rsi, %r9
	setne	%cl
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$16495, %r8d
	subl	%edx, %r8d
	cmpl	$63, %r8d
	jle	.L16
	testq	%rax, %rax
	je	.L13
.L8:
	pxor	%xmm0, %xmm0
	movl	$1, %eax
	movaps	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%r8d, %ecx
	movq	$-1, %r9
	salq	%cl, %r9
	notq	%r9
	testq	%rax, %r9
	setne	%cl
.L7:
	testb	%cl, %cl
	jne	.L8
	testq	%rdx, %rdx
	je	.L14
	movabsq	$281474976710655, %rcx
	andq	%rcx, %rsi
	addq	$1, %rcx
	orq	%rcx, %rsi
	cmpl	$63, %r8d
	jle	.L12
	movl	$16431, %ecx
	movq	%rsi, %rax
	movabsq	$9223231299366420480, %rsi
	subl	%edx, %ecx
	shrq	%cl, %rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	leal	-16431(%rdx), %ecx
	movq	%rsi, %rdx
	salq	%cl, %rdx
	movl	%r8d, %ecx
	shrq	%cl, %rax
	shrq	%cl, %rsi
	orq	%rdx, %rax
.L14:
	movabsq	$9223231299366420480, %rdx
	orq	%rdx, %rsi
.L11:
	movq	%rax, -24(%rsp)
	movq	%rsi, -16(%rsp)
	xorl	%eax, %eax
	movdqa	-24(%rsp), %xmm1
	movaps	%xmm1, (%rdi)
	ret
	.size	__setpayloadf128, .-__setpayloadf128
	.weak	setpayloadf128
	.set	setpayloadf128,__setpayloadf128
