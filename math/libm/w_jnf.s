	.text
	.p2align 4,,15
	.globl	__jnf
	.type	__jnf, @function
__jnf:
	jmp	__ieee754_jnf@PLT
	.size	__jnf, .-__jnf
	.weak	jnf32
	.set	jnf32,__jnf
	.weak	jnf
	.set	jnf,__jnf
	.p2align 4,,15
	.globl	__ynf
	.type	__ynf, @function
__ynf:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L11
.L4:
	jmp	__ieee754_ynf@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	ucomiss	%xmm1, %xmm0
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__ynf, .-__ynf
	.weak	ynf32
	.set	ynf32,__ynf
	.weak	ynf
	.set	ynf,__ynf
