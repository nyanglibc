	.text
#APP
	.symver __ieee754_j1l,__j1l_finite@GLIBC_2.15
	.symver __ieee754_y1l,__y1l_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.type	pone, @function
pone:
	movq	16(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16385, %ax
	jg	.L3
	movq	8(%rsp), %rdx
	cwtl
	sall	$16, %eax
	shrq	$48, %rdx
	orl	%edx, %eax
	cmpl	$1073844595, %eax
	ja	.L4
	cmpl	$1073788634, %eax
	ja	.L5
	fldt	.LC39(%rip)
	fldt	.LC40(%rip)
	fstpt	-24(%rsp)
	fldt	.LC41(%rip)
	fstpt	-40(%rsp)
	fldt	.LC42(%rip)
	fstpt	-56(%rsp)
	fldt	.LC43(%rip)
	fstpt	-72(%rsp)
	fldt	.LC44(%rip)
	fldt	.LC45(%rip)
	fldt	.LC46(%rip)
	fldt	.LC47(%rip)
	fstpt	-88(%rsp)
	fldt	.LC48(%rip)
	fldt	.LC49(%rip)
	fldt	.LC50(%rip)
	fstpt	-104(%rsp)
	fldt	.LC51(%rip)
	fstpt	-120(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	fldt	.LC13(%rip)
	fldt	.LC14(%rip)
	fstpt	-24(%rsp)
	fldt	.LC15(%rip)
	fstpt	-40(%rsp)
	fldt	.LC16(%rip)
	fstpt	-56(%rsp)
	fldt	.LC17(%rip)
	fstpt	-72(%rsp)
	fldt	.LC18(%rip)
	fldt	.LC19(%rip)
	fldt	.LC20(%rip)
	fldt	.LC21(%rip)
	fstpt	-88(%rsp)
	fldt	.LC22(%rip)
	fldt	.LC23(%rip)
	fldt	.LC24(%rip)
	fstpt	-104(%rsp)
	fldt	.LC25(%rip)
	fstpt	-120(%rsp)
.L2:
	fldt	8(%rsp)
	fmul	%st(0), %st
	fld1
	fdiv	%st, %st(1)
	fxch	%st(6)
	fmul	%st(1), %st
	faddp	%st, %st(5)
	fmul	%st, %st(4)
	fxch	%st(4)
	faddp	%st, %st(3)
	fxch	%st(2)
	fmul	%st(3), %st
	fldt	-88(%rsp)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	faddp	%st, %st(1)
	fmul	%st(2), %st
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-120(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fxch	%st(3)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	-24(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-40(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-56(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-72(%rsp)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	-104(%rsp)
	faddp	%st, %st(1)
	fdivrp	%st, %st(2)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	fldt	.LC0(%rip)
	fldt	.LC1(%rip)
	fstpt	-24(%rsp)
	fldt	.LC2(%rip)
	fstpt	-40(%rsp)
	fldt	.LC3(%rip)
	fstpt	-56(%rsp)
	fldt	.LC4(%rip)
	fstpt	-72(%rsp)
	fldt	.LC5(%rip)
	fldt	.LC6(%rip)
	fldt	.LC7(%rip)
	fldt	.LC8(%rip)
	fstpt	-88(%rsp)
	fldt	.LC9(%rip)
	fldt	.LC10(%rip)
	fldt	.LC11(%rip)
	fstpt	-104(%rsp)
	fldt	.LC12(%rip)
	fstpt	-120(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	fldt	.LC26(%rip)
	fldt	.LC27(%rip)
	fstpt	-24(%rsp)
	fldt	.LC28(%rip)
	fstpt	-40(%rsp)
	fldt	.LC29(%rip)
	fstpt	-56(%rsp)
	fldt	.LC30(%rip)
	fstpt	-72(%rsp)
	fldt	.LC31(%rip)
	fldt	.LC32(%rip)
	fldt	.LC33(%rip)
	fldt	.LC34(%rip)
	fstpt	-88(%rsp)
	fldt	.LC35(%rip)
	fldt	.LC36(%rip)
	fldt	.LC37(%rip)
	fstpt	-104(%rsp)
	fldt	.LC38(%rip)
	fstpt	-120(%rsp)
	jmp	.L2
	.size	pone, .-pone
	.p2align 4,,15
	.type	qone, @function
qone:
	subq	$16, %rsp
	movq	32(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16385, %ax
	jg	.L9
	movq	24(%rsp), %rdx
	cwtl
	sall	$16, %eax
	shrq	$48, %rdx
	orl	%edx, %eax
	cmpl	$1073844595, %eax
	ja	.L10
	cmpl	$1073788634, %eax
	ja	.L11
	fldt	.LC96(%rip)
	fldt	.LC97(%rip)
	fstpt	-8(%rsp)
	fldt	.LC98(%rip)
	fstpt	-24(%rsp)
	fldt	.LC99(%rip)
	fstpt	-40(%rsp)
	fldt	.LC100(%rip)
	fstpt	-56(%rsp)
	fldt	.LC101(%rip)
	fstpt	-72(%rsp)
	fldt	.LC102(%rip)
	fldt	.LC103(%rip)
	fldt	.LC104(%rip)
	fstpt	-88(%rsp)
	fldt	.LC105(%rip)
	fstpt	-104(%rsp)
	fldt	.LC106(%rip)
	fldt	.LC107(%rip)
	fldt	.LC108(%rip)
	fstpt	-120(%rsp)
	fldt	.LC109(%rip)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	fldt	.LC68(%rip)
	fldt	.LC69(%rip)
	fstpt	-8(%rsp)
	fldt	.LC70(%rip)
	fstpt	-24(%rsp)
	fldt	.LC71(%rip)
	fstpt	-40(%rsp)
	fldt	.LC72(%rip)
	fstpt	-56(%rsp)
	fldt	.LC73(%rip)
	fstpt	-72(%rsp)
	fldt	.LC74(%rip)
	fldt	.LC75(%rip)
	fldt	.LC76(%rip)
	fstpt	-88(%rsp)
	fldt	.LC77(%rip)
	fstpt	-104(%rsp)
	fldt	.LC78(%rip)
	fldt	.LC79(%rip)
	fldt	.LC80(%rip)
	fstpt	-120(%rsp)
	fldt	.LC81(%rip)
.L8:
	fldt	24(%rsp)
	fmul	%st(0), %st
	fld1
	fdivp	%st, %st(1)
	fmul	%st, %st(5)
	fxch	%st(5)
	faddp	%st, %st(4)
	fxch	%st(3)
	fmul	%st(4), %st
	fldt	-88(%rsp)
	faddp	%st, %st(1)
	fmul	%st(4), %st
	fldt	-104(%rsp)
	faddp	%st, %st(1)
	fmul	%st(4), %st
	faddp	%st, %st(2)
	fxch	%st(1)
	fmul	%st(3), %st
	faddp	%st, %st(1)
	fmul	%st(2), %st
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fxch	%st(2)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	-8(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-24(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-40(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-56(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-72(%rsp)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	-120(%rsp)
	faddp	%st, %st(1)
	fdivrp	%st, %st(1)
	fadds	.LC110(%rip)
	fldt	24(%rsp)
	addq	$16, %rsp
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	fldt	.LC54(%rip)
	fldt	.LC55(%rip)
	fstpt	-8(%rsp)
	fldt	.LC56(%rip)
	fstpt	-24(%rsp)
	fldt	.LC57(%rip)
	fstpt	-40(%rsp)
	fldt	.LC58(%rip)
	fstpt	-56(%rsp)
	fldt	.LC59(%rip)
	fstpt	-72(%rsp)
	fldt	.LC60(%rip)
	fldt	.LC61(%rip)
	fldt	.LC62(%rip)
	fstpt	-88(%rsp)
	fldt	.LC63(%rip)
	fstpt	-104(%rsp)
	fldt	.LC64(%rip)
	fldt	.LC65(%rip)
	fldt	.LC66(%rip)
	fstpt	-120(%rsp)
	fldt	.LC67(%rip)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	fldt	.LC82(%rip)
	fldt	.LC83(%rip)
	fstpt	-8(%rsp)
	fldt	.LC84(%rip)
	fstpt	-24(%rsp)
	fldt	.LC85(%rip)
	fstpt	-40(%rsp)
	fldt	.LC86(%rip)
	fstpt	-56(%rsp)
	fldt	.LC87(%rip)
	fstpt	-72(%rsp)
	fldt	.LC88(%rip)
	fldt	.LC89(%rip)
	fldt	.LC90(%rip)
	fstpt	-88(%rsp)
	fldt	.LC91(%rip)
	fstpt	-104(%rsp)
	fldt	.LC92(%rip)
	fldt	.LC93(%rip)
	fldt	.LC94(%rip)
	fstpt	-120(%rsp)
	fldt	.LC95(%rip)
	jmp	.L8
	.size	qone, .-qone
	.p2align 4,,15
	.globl	__ieee754_j1l
	.type	__ieee754_j1l, @function
__ieee754_j1l:
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	movq	152(%rsp), %rbp
	movl	%ebp, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L41
	movswl	%ax, %ebx
	cmpl	$16383, %ebx
	jg	.L42
	fldt	144(%rsp)
	cmpl	$16349, %ebx
	fmuls	.LC113(%rip)
	jle	.L43
.L23:
	fldt	144(%rsp)
	fld	%st(0)
	fmul	%st(0), %st
	fldt	.LC116(%rip)
	fmul	%st(1), %st
	fldt	.LC117(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC118(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC119(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC120(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fmulp	%st, %st(2)
	fldt	.LC121(%rip)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC122(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC123(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC124(%rip)
	faddp	%st, %st(1)
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
	jmp	.L13
.L45:
	fstp	%st(0)
.L13:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	fldt	144(%rsp)
	leaq	96(%rsp), %rdi
	leaq	80(%rsp), %rsi
	subq	$16, %rsp
	fabs
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__sincosl@PLT
	cmpl	$32766, %ebx
	fldt	112(%rsp)
	fldt	96(%rsp)
	fld	%st(1)
	fsub	%st(1), %st
	fstpt	16(%rsp)
	popq	%rdi
	popq	%r8
	fldt	16(%rsp)
	jne	.L38
	fstp	%st(1)
	fstp	%st(1)
	fsqrt
	jmp	.L17
.L44:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L17:
	fldt	.LC112(%rip)
	fldt	(%rsp)
	fmulp	%st, %st(1)
	fdivp	%st, %st(1)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L38:
	fxch	%st(1)
	fstpt	48(%rsp)
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	48(%rsp)
	fld	%st(0)
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__cosl@PLT
	fldt	112(%rsp)
	fldt	96(%rsp)
	popq	%rcx
	popq	%rsi
	fmulp	%st, %st(1)
	fldz
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	16(%rsp)
	jbe	.L39
	fldt	32(%rsp)
	fchs
	fldt	48(%rsp)
	fsubrp	%st, %st(1)
	fld	%st(0)
	fstpt	64(%rsp)
	fdivrp	%st, %st(2)
	fxch	%st(1)
	fstpt	(%rsp)
.L20:
	cmpl	$16526, %ebx
	fld	%st(0)
	fsqrt
	jg	.L44
	fstpt	48(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	pone
	fstpt	32(%rsp)
	fldt	48(%rsp)
	fstpt	(%rsp)
	call	qone
	popq	%rax
	popq	%rdx
	fldt	(%rsp)
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fldt	64(%rsp)
	fmulp	%st, %st(2)
	fsubp	%st, %st(1)
	fldt	.LC112(%rip)
	fmulp	%st, %st(1)
	fldt	48(%rsp)
	fdivrp	%st, %st(1)
.L22:
	testw	%bp, %bp
	jns	.L13
	addq	$120, %rsp
	fchs
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	fld1
	fldt	144(%rsp)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	fldt	.LC114(%rip)
	fldt	144(%rsp)
	faddp	%st, %st(1)
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L23
	fldt	.LC115(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L25
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L25:
	fldz
	movl	$0, %edx
	fucomi	%st(1), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L45
	fldt	144(%rsp)
	movl	$1, %edx
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L13
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L39:
	fldt	(%rsp)
	fdivrp	%st, %st(2)
	fxch	%st(1)
	fstpt	64(%rsp)
	jmp	.L20
	.size	__ieee754_j1l, .-__ieee754_j1l
	.p2align 4,,15
	.globl	__ieee754_y1l
	.type	__ieee754_y1l, @function
__ieee754_y1l:
	pushq	%rbx
	subq	$96, %rsp
	movq	120(%rsp), %rax
	movq	112(%rsp), %rcx
	movl	%eax, %edx
	movq	%rcx, %rsi
	shrq	$32, %rsi
	andw	$32767, %dx
	testw	%ax, %ax
	js	.L67
	cmpw	$32767, %dx
	je	.L68
	movl	%ecx, %eax
	orl	%esi, %eax
	je	.L69
	movswl	%dx, %ebx
	cmpl	$16383, %ebx
	jg	.L70
	cmpl	$16318, %ebx
	jle	.L71
	fldt	112(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fmul	%st(0), %st
	fldt	.LC127(%rip)
	fmul	%st(1), %st
	fldt	.LC128(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC129(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC130(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC131(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC132(%rip)
	fsubrp	%st, %st(1)
	fldt	.LC133(%rip)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC134(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC135(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC136(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(2)
	fldt	.LC137(%rip)
	faddp	%st, %st(2)
	fdivp	%st, %st(1)
	fld	%st(1)
	fmulp	%st, %st(1)
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__ieee754_j1l@PLT
	fstpt	16(%rsp)
	pushq	136(%rsp)
	pushq	136(%rsp)
	call	__ieee754_logl@PLT
	fldt	32(%rsp)
	fmulp	%st, %st(1)
	fld1
	fldt	144(%rsp)
	fdivrp	%st, %st(1)
	fsubrp	%st, %st(1)
	fldt	.LC138(%rip)
	fmulp	%st, %st(1)
	fldt	48(%rsp)
	addq	$32, %rsp
	faddp	%st, %st(1)
.L46:
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdi
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	__sincosl@PLT
	fldt	80(%rsp)
	cmpl	$32766, %ebx
	fldt	96(%rsp)
	fld	%st(1)
	fchs
	fsub	%st(1), %st
	fstpt	16(%rsp)
	popq	%r9
	popq	%r10
	jne	.L64
	fstp	%st(0)
	fstp	%st(0)
	fldt	112(%rsp)
	fsqrt
.L52:
	fldt	.LC112(%rip)
	fldt	(%rsp)
	fmulp	%st, %st(1)
	fdivp	%st, %st(1)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L64:
	fstpt	32(%rsp)
	subq	$16, %rsp
	fstpt	32(%rsp)
	fldt	128(%rsp)
	fadd	%st(0), %st
	fstpt	(%rsp)
	call	__cosl@PLT
	fldt	80(%rsp)
	fldt	96(%rsp)
	popq	%rdi
	popq	%r8
	fmulp	%st, %st(1)
	fldz
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	16(%rsp)
	fldt	32(%rsp)
	jbe	.L65
	fstp	%st(0)
	fstp	%st(0)
	fldt	(%rsp)
	fdivrp	%st, %st(1)
	fstpt	48(%rsp)
.L55:
	cmpl	$16526, %ebx
	fldt	112(%rsp)
	fsqrt
	jg	.L52
	fstpt	32(%rsp)
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	pone
	popq	%rax
	popq	%rdx
	fstpt	16(%rsp)
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	qone
	popq	%rcx
	popq	%rsi
	fldt	(%rsp)
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fldt	48(%rsp)
	fmulp	%st, %st(2)
	faddp	%st, %st(1)
	fldt	.LC112(%rip)
	fmulp	%st, %st(1)
	fldt	32(%rsp)
	addq	$96, %rsp
	popq	%rbx
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	fldz
	fldt	112(%rsp)
	addq	$96, %rsp
	popq	%rbx
	fmul	%st(1), %st
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	fldt	112(%rsp)
	fld	%st(0)
	fmul	%st(0), %st
	faddp	%st, %st(1)
	fld1
	fdivp	%st, %st(1)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L69:
	fldt	112(%rsp)
	fsubs	.LC125(%rip)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L71:
	fldt	.LC126(%rip)
	fldt	112(%rsp)
	fdivrp	%st, %st(1)
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L46
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L65:
	fsubrp	%st, %st(1)
	fld	%st(0)
	fstpt	48(%rsp)
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
	jmp	.L55
	.size	__ieee754_y1l, .-__ieee754_y1l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1345448154
	.long	2692579870
	.long	16383
	.long	0
	.align 16
.LC1:
	.long	3784923997
	.long	2898369804
	.long	16381
	.long	0
	.align 16
.LC2:
	.long	819944385
	.long	4206977452
	.long	16377
	.long	0
	.align 16
.LC3:
	.long	670983487
	.long	2406919190
	.long	16373
	.long	0
	.align 16
.LC4:
	.long	3305546790
	.long	2190648981
	.long	16367
	.long	0
	.align 16
.LC5:
	.long	1868935804
	.long	3079124469
	.long	16378
	.long	0
	.align 16
.LC6:
	.long	4148417875
	.long	3886469462
	.long	16379
	.long	0
	.align 16
.LC7:
	.long	1010815151
	.long	2458034419
	.long	16378
	.long	0
	.align 16
.LC8:
	.long	3376165208
	.long	3781544667
	.long	16374
	.long	0
	.align 16
.LC9:
	.long	1287100126
	.long	2217854906
	.long	16370
	.long	0
	.align 16
.LC10:
	.long	3764247946
	.long	4084186701
	.long	16363
	.long	0
	.align 16
.LC11:
	.long	4260246654
	.long	2583174070
	.long	16359
	.long	0
	.align 16
.LC12:
	.long	2383367381
	.long	2421725691
	.long	16356
	.long	0
	.align 16
.LC13:
	.long	3106360647
	.long	2442523133
	.long	16384
	.long	0
	.align 16
.LC14:
	.long	4136424094
	.long	2501584343
	.long	16383
	.long	0
	.align 16
.LC15:
	.long	106582729
	.long	3537110375
	.long	16380
	.long	0
	.align 16
.LC16:
	.long	1372602016
	.long	3984983842
	.long	16376
	.long	0
	.align 16
.LC17:
	.long	240660053
	.long	3579220419
	.long	16371
	.long	0
	.align 16
.LC18:
	.long	2446565077
	.long	2211945168
	.long	16378
	.long	0
	.align 16
.LC19:
	.long	3799717150
	.long	3016057512
	.long	16380
	.long	0
	.align 16
.LC20:
	.long	2167257228
	.long	3926540743
	.long	16379
	.long	0
	.align 16
.LC21:
	.long	2754864244
	.long	3063085470
	.long	16377
	.long	0
	.align 16
.LC22:
	.long	34496258
	.long	3612378230
	.long	16373
	.long	0
	.align 16
.LC23:
	.long	865735918
	.long	3318126865
	.long	16368
	.long	0
	.align 16
.LC24:
	.long	1684160740
	.long	4149063668
	.long	16364
	.long	0
	.align 16
.LC25:
	.long	3754363224
	.long	3889747187
	.long	16361
	.long	0
	.align 16
.LC26:
	.long	1680955816
	.long	4284983933
	.long	16384
	.long	0
	.align 16
.LC27:
	.long	3716937400
	.long	3912353371
	.long	16384
	.long	0
	.align 16
.LC28:
	.long	3253385807
	.long	2466153093
	.long	16383
	.long	0
	.align 16
.LC29:
	.long	2933421037
	.long	2455696859
	.long	16380
	.long	0
	.align 16
.LC30:
	.long	3763000951
	.long	3840603910
	.long	16375
	.long	0
	.align 16
.LC31:
	.long	1010054927
	.long	3129673170
	.long	16377
	.long	0
	.align 16
.LC32:
	.long	530811828
	.long	2198356475
	.long	16381
	.long	0
	.align 16
.LC33:
	.long	3387271093
	.long	2772776694
	.long	16381
	.long	0
	.align 16
.LC34:
	.long	3386588926
	.long	4052850224
	.long	16379
	.long	0
	.align 16
.LC35:
	.long	1159838735
	.long	2173800625
	.long	16377
	.long	0
	.align 16
.LC36:
	.long	1411886640
	.long	3532096340
	.long	16372
	.long	0
	.align 16
.LC37:
	.long	3717297950
	.long	3798790056
	.long	16369
	.long	0
	.align 16
.LC38:
	.long	4006363778
	.long	3561365390
	.long	16366
	.long	0
	.align 16
.LC39:
	.long	2921029180
	.long	3655629593
	.long	16385
	.long	0
	.align 16
.LC40:
	.long	2788110909
	.long	2860518813
	.long	16386
	.long	0
	.align 16
.LC41:
	.long	909322868
	.long	3070049526
	.long	16385
	.long	0
	.align 16
.LC42:
	.long	2022610107
	.long	2566705894
	.long	16383
	.long	0
	.align 16
.LC43:
	.long	1781838817
	.long	3304299861
	.long	16379
	.long	0
	.align 16
.LC44:
	.long	1916112695
	.long	2204716904
	.long	16377
	.long	0
	.align 16
.LC45:
	.long	3519440744
	.long	3044424274
	.long	16381
	.long	0
	.align 16
.LC46:
	.long	4292419397
	.long	3570736806
	.long	16382
	.long	0
	.align 16
.LC47:
	.long	1750106309
	.long	2349209875
	.long	16382
	.long	0
	.align 16
.LC48:
	.long	1540980300
	.long	2195629579
	.long	16380
	.long	0
	.align 16
.LC49:
	.long	601580897
	.long	3003256001
	.long	16376
	.long	0
	.align 16
.LC50:
	.long	1771103383
	.long	2623000614
	.long	16374
	.long	0
	.align 16
.LC51:
	.long	4060922459
	.long	2459056214
	.long	16371
	.long	0
	.align 16
.LC54:
	.long	70666451
	.long	2363428687
	.long	16384
	.long	0
	.align 16
.LC55:
	.long	523207047
	.long	3873152525
	.long	16382
	.long	0
	.align 16
.LC56:
	.long	3693980647
	.long	4167288388
	.long	16379
	.long	0
	.align 16
.LC57:
	.long	4164916175
	.long	3638206955
	.long	16375
	.long	0
	.align 16
.LC58:
	.long	3002503946
	.long	2780959628
	.long	16370
	.long	0
	.align 16
.LC59:
	.long	2457659013
	.long	3619066464
	.long	16363
	.long	0
	.align 16
.LC60:
	.long	633032600
	.long	3626492712
	.long	49147
	.long	0
	.align 16
.LC61:
	.long	1409579886
	.long	2355340696
	.long	49147
	.long	0
	.align 16
.LC62:
	.long	3674705644
	.long	2989045770
	.long	49144
	.long	0
	.align 16
.LC63:
	.long	2258912574
	.long	2804910439
	.long	49140
	.long	0
	.align 16
.LC64:
	.long	944771306
	.long	2220027879
	.long	49135
	.long	0
	.align 16
.LC65:
	.long	1587609487
	.long	2941008479
	.long	49128
	.long	0
	.align 16
.LC66:
	.long	349493864
	.long	3199923359
	.long	16355
	.long	0
	.align 16
.LC67:
	.long	2132186913
	.long	2624937130
	.long	49120
	.long	0
	.align 16
.LC68:
	.long	881236189
	.long	2151105201
	.long	16385
	.long	0
	.align 16
.LC69:
	.long	3547095159
	.long	3412060729
	.long	16384
	.long	0
	.align 16
.LC70:
	.long	357099516
	.long	3658684262
	.long	16382
	.long	0
	.align 16
.LC71:
	.long	3204591684
	.long	3229039596
	.long	16379
	.long	0
	.align 16
.LC72:
	.long	3502292934
	.long	2507808499
	.long	16375
	.long	0
	.align 16
.LC73:
	.long	2785139404
	.long	3311135825
	.long	16369
	.long	0
	.align 16
.LC74:
	.long	1404870491
	.long	2291616732
	.long	49148
	.long	0
	.align 16
.LC75:
	.long	3201212155
	.long	3374869628
	.long	49148
	.long	0
	.align 16
.LC76:
	.long	2619417054
	.long	2342884664
	.long	49147
	.long	0
	.align 16
.LC77:
	.long	3160513909
	.long	2347429293
	.long	49144
	.long	0
	.align 16
.LC78:
	.long	1170274208
	.long	3896090316
	.long	49139
	.long	0
	.align 16
.LC79:
	.long	1538301959
	.long	2664968898
	.long	49134
	.long	0
	.align 16
.LC80:
	.long	3646385084
	.long	2951098760
	.long	16362
	.long	0
	.align 16
.LC81:
	.long	1875966451
	.long	2420823200
	.long	49127
	.long	0
	.align 16
.LC82:
	.long	4244449791
	.long	3773626654
	.long	16385
	.long	0
	.align 16
.LC83:
	.long	3001296130
	.long	2691526105
	.long	16386
	.long	0
	.align 16
.LC84:
	.long	160003723
	.long	2605930442
	.long	16385
	.long	0
	.align 16
.LC85:
	.long	2840292587
	.long	4128906612
	.long	16382
	.long	0
	.align 16
.LC86:
	.long	3755764026
	.long	2842764823
	.long	16379
	.long	0
	.align 16
.LC87:
	.long	4249710576
	.long	3269510708
	.long	16374
	.long	0
	.align 16
.LC88:
	.long	2196431731
	.long	2735721082
	.long	49148
	.long	0
	.align 16
.LC89:
	.long	1263791570
	.long	4173155651
	.long	49149
	.long	0
	.align 16
.LC90:
	.long	1883362181
	.long	2882982071
	.long	49149
	.long	0
	.align 16
.LC91:
	.long	2270390302
	.long	2768711486
	.long	49147
	.long	0
	.align 16
.LC92:
	.long	1677817170
	.long	4248797797
	.long	49143
	.long	0
	.align 16
.LC93:
	.long	232185724
	.long	2595855490
	.long	49139
	.long	0
	.align 16
.LC94:
	.long	3041609270
	.long	2483411468
	.long	16368
	.long	0
	.align 16
.LC95:
	.long	1437803055
	.long	4074346121
	.long	49132
	.long	0
	.align 16
.LC96:
	.long	2935453275
	.long	3224989777
	.long	16386
	.long	0
	.align 16
.LC97:
	.long	2903118124
	.long	3974428096
	.long	16387
	.long	0
	.align 16
.LC98:
	.long	529404530
	.long	3313136835
	.long	16387
	.long	0
	.align 16
.LC99:
	.long	3223945719
	.long	2235343032
	.long	16386
	.long	0
	.align 16
.LC100:
	.long	2999367256
	.long	2578078664
	.long	16383
	.long	0
	.align 16
.LC101:
	.long	1580748072
	.long	2429611459
	.long	16379
	.long	0
	.align 16
.LC102:
	.long	116684416
	.long	3138454786
	.long	49148
	.long	0
	.align 16
.LC103:
	.long	147552999
	.long	2336366239
	.long	49151
	.long	0
	.align 16
.LC104:
	.long	311853338
	.long	3056190573
	.long	49151
	.long	0
	.align 16
.LC105:
	.long	2467461283
	.long	2689579054
	.long	49150
	.long	0
	.align 16
.LC106:
	.long	3929733565
	.long	3646439507
	.long	49147
	.long	0
	.align 16
.LC107:
	.long	2661154568
	.long	3781874836
	.long	49143
	.long	0
	.align 16
.LC108:
	.long	3748266032
	.long	2943984018
	.long	16373
	.long	0
	.align 16
.LC109:
	.long	1370156568
	.long	2414970159
	.long	49138
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC110:
	.long	1052770304
	.section	.rodata.cst16
	.align 16
.LC112:
	.long	349923469
	.long	2423175810
	.long	16382
	.long	0
	.section	.rodata.cst4
	.align 4
.LC113:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC114:
	.long	1164966429
	.long	2310419687
	.long	32760
	.long	0
	.align 16
.LC115:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC116:
	.long	3712499864
	.long	2200955925
	.long	49150
	.long	0
	.align 16
.LC117:
	.long	996797214
	.long	3682651247
	.long	16390
	.long	0
	.align 16
.LC118:
	.long	3111363079
	.long	2418070443
	.long	16398
	.long	0
	.align 16
.LC119:
	.long	1915539208
	.long	2750759491
	.long	16404
	.long	0
	.align 16
.LC120:
	.long	4196159621
	.long	3087169955
	.long	16409
	.long	0
	.align 16
.LC121:
	.long	1533064468
	.long	2203016983
	.long	16392
	.long	0
	.align 16
.LC122:
	.long	1335802193
	.long	2284055775
	.long	16400
	.long	0
	.align 16
.LC123:
	.long	200392833
	.long	2730934233
	.long	16407
	.long	0
	.align 16
.LC124:
	.long	4196159636
	.long	3087169955
	.long	16413
	.long	0
	.section	.rodata.cst4
	.align 4
.LC125:
	.long	2139095040
	.section	.rodata.cst16
	.align 16
.LC126:
	.long	1313084714
	.long	2734261102
	.long	49150
	.long	0
	.align 16
.LC127:
	.long	2817409806
	.long	2379853505
	.long	16390
	.long	0
	.align 16
.LC128:
	.long	3990897625
	.long	4022628513
	.long	16398
	.long	0
	.align 16
.LC129:
	.long	2955962905
	.long	2485386251
	.long	16406
	.long	0
	.align 16
.LC130:
	.long	2008710040
	.long	2575321332
	.long	16412
	.long	0
	.align 16
.LC131:
	.long	4096088980
	.long	3865548317
	.long	16416
	.long	0
	.align 16
.LC132:
	.long	1771445032
	.long	3692548241
	.long	16418
	.long	0
	.align 16
.LC133:
	.long	3455328261
	.long	2377533009
	.long	16392
	.long	0
	.align 16
.LC134:
	.long	1750747776
	.long	2886130120
	.long	16400
	.long	0
	.align 16
.LC135:
	.long	2766237073
	.long	2300835898
	.long	16408
	.long	0
	.align 16
.LC136:
	.long	3663567696
	.long	2334773282
	.long	16415
	.long	0
	.align 16
.LC137:
	.long	2962509711
	.long	2354255735
	.long	16421
	.long	0
	.align 16
.LC138:
	.long	1313084714
	.long	2734261102
	.long	16382
	.long	0
