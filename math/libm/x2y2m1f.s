	.text
	.p2align 4,,15
	.globl	__x2y2m1f
	.type	__x2y2m1f, @function
__x2y2m1f:
	pxor	%xmm3, %xmm3
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm3
	movsd	.LC0(%rip), %xmm0
	movapd	%xmm3, %xmm2
	subsd	%xmm0, %xmm2
	addsd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.size	__x2y2m1f, .-__x2y2m1f
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
