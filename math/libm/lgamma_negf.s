	.text
	.p2align 4,,15
	.type	lg_sinpi, @function
lg_sinpi:
	movss	.LC0(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L7
	movss	.LC2(%rip), %xmm1
	subss	%xmm0, %xmm1
	movss	.LC1(%rip), %xmm0
	mulss	%xmm1, %xmm0
	jmp	__cosf@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	mulss	.LC1(%rip), %xmm0
	jmp	__sinf@PLT
	.size	lg_sinpi, .-lg_sinpi
	.p2align 4,,15
	.type	lg_cospi, @function
lg_cospi:
	movss	.LC0(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L13
	movss	.LC2(%rip), %xmm1
	subss	%xmm0, %xmm1
	movss	.LC1(%rip), %xmm0
	mulss	%xmm1, %xmm0
	jmp	__sinf@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	mulss	.LC1(%rip), %xmm0
	jmp	__cosf@PLT
	.size	lg_cospi, .-lg_cospi
	.p2align 4,,15
	.globl	__lgamma_negf
	.type	__lgamma_negf, @function
__lgamma_negf:
	movss	.LC3(%rip), %xmm2
	movss	.LC5(%rip), %xmm7
	mulss	%xmm0, %xmm2
	movss	.LC4(%rip), %xmm8
	movaps	%xmm2, %xmm4
	movaps	%xmm2, %xmm1
	andps	%xmm7, %xmm4
	ucomiss	%xmm4, %xmm8
	jbe	.L15
	cvttss2si	%xmm2, %eax
	pxor	%xmm4, %xmm4
	movss	.LC6(%rip), %xmm3
	movaps	%xmm7, %xmm5
	andnps	%xmm2, %xmm5
	cvtsi2ss	%eax, %xmm4
	movaps	%xmm4, %xmm1
	cmpnless	%xmm2, %xmm1
	andps	%xmm3, %xmm1
	subss	%xmm1, %xmm4
	movaps	%xmm4, %xmm1
	orps	%xmm5, %xmm1
.L15:
	cvttss2si	%xmm1, %eax
	testb	$1, %al
	je	.L49
	movl	%eax, %ecx
	pxor	%xmm1, %xmm1
	notl	%ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	cvtsi2ss	%edx, %xmm1
.L20:
	subl	$4, %eax
	pushq	%rbp
	pushq	%rbx
	movl	%eax, %edx
	andl	$2, %edx
	subq	$72, %rsp
	cmpl	$1, %edx
	sbbl	%edx, %edx
	xorl	%ebp, %ebp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 56(%rsp)
# 0 "" 2
#NO_APP
	movl	56(%rsp), %ebx
	orl	$1, %edx
	movl	%edx, (%rdi)
	movl	%ebx, %edx
	andb	$-97, %dh
	cmpl	%edx, %ebx
	movl	%edx, 60(%rsp)
	jne	.L50
.L22:
	leaq	lgamma_zeros(%rip), %rdx
	movslq	%eax, %rcx
	cmpl	$1, %eax
	movaps	%xmm0, %xmm2
	movss	(%rdx,%rcx,8), %xmm6
	movaps	%xmm0, %xmm3
	subss	%xmm6, %xmm2
	movss	4(%rdx,%rcx,8), %xmm5
	movaps	%xmm2, %xmm4
	subss	%xmm5, %xmm4
	jle	.L51
	movaps	%xmm1, %xmm2
	subss	%xmm6, %xmm1
	subss	%xmm0, %xmm2
	movss	.LC2(%rip), %xmm0
	subss	%xmm5, %xmm1
	andps	%xmm7, %xmm2
	mulss	%xmm2, %xmm0
	andps	%xmm7, %xmm1
	movaps	%xmm4, %xmm7
	xorps	.LC11(%rip), %xmm7
	ucomiss	%xmm1, %xmm0
	movss	%xmm7, 8(%rsp)
	ja	.L52
	testb	$1, %al
	movss	.LC2(%rip), %xmm1
	jne	.L53
	mulss	%xmm4, %xmm1
.L32:
	movaps	%xmm1, %xmm0
	movss	%xmm3, 44(%rsp)
	movss	%xmm4, 40(%rsp)
	movss	%xmm5, 36(%rsp)
	movss	%xmm6, 28(%rsp)
	movss	%xmm2, 16(%rsp)
	movss	%xmm1, 12(%rsp)
	call	lg_sinpi
	movss	12(%rsp), %xmm1
	movss	%xmm0, 24(%rsp)
	movaps	%xmm1, %xmm0
	call	lg_cospi
	movss	16(%rsp), %xmm2
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	movss	%xmm2, 20(%rsp)
	call	lg_cospi
	movss	20(%rsp), %xmm2
	movss	%xmm0, 16(%rsp)
	movaps	%xmm2, %xmm0
	call	lg_sinpi
	movss	16(%rsp), %xmm1
	divss	%xmm0, %xmm1
	movss	12(%rsp), %xmm0
	movss	24(%rsp), %xmm7
	mulss	%xmm1, %xmm0
	subss	%xmm7, %xmm0
	addss	%xmm7, %xmm7
	mulss	%xmm7, %xmm0
	call	__log1pf@PLT
	movss	%xmm0, 32(%rsp)
	movss	44(%rsp), %xmm3
	movss	40(%rsp), %xmm4
	movss	36(%rsp), %xmm5
	movss	28(%rsp), %xmm6
.L30:
	movss	.LC6(%rip), %xmm1
	movaps	%xmm1, %xmm7
	movaps	%xmm1, %xmm0
	movaps	%xmm1, %xmm2
	movss	%xmm1, 28(%rsp)
	subss	%xmm3, %xmm7
	movss	%xmm4, 12(%rsp)
	subss	%xmm6, %xmm2
	subss	%xmm7, %xmm0
	movss	%xmm7, 20(%rsp)
	movss	%xmm2, 24(%rsp)
	subss	%xmm3, %xmm0
	movaps	%xmm1, %xmm3
	subss	%xmm2, %xmm3
	movss	%xmm0, 16(%rsp)
	movaps	%xmm3, %xmm0
	movaps	%xmm2, %xmm3
	subss	%xmm6, %xmm0
	subss	%xmm5, %xmm0
	movss	.LC12(%rip), %xmm5
	subss	%xmm5, %xmm3
	subss	.LC13(%rip), %xmm3
	addss	%xmm3, %xmm0
	divss	%xmm5, %xmm0
	call	__log1pf@PLT
	movaps	%xmm0, %xmm5
	movss	12(%rsp), %xmm4
	movss	20(%rsp), %xmm7
	mulss	%xmm4, %xmm5
	divss	%xmm7, %xmm4
	movaps	%xmm7, %xmm3
	subss	.LC2(%rip), %xmm3
	movss	%xmm5, 12(%rsp)
	addss	16(%rsp), %xmm3
	movss	%xmm3, 16(%rsp)
	movaps	%xmm4, %xmm0
	call	__log1pf@PLT
	movss	28(%rsp), %xmm1
	mulss	16(%rsp), %xmm0
	movaps	%xmm1, %xmm4
	movss	20(%rsp), %xmm7
	movss	24(%rsp), %xmm2
	divss	%xmm7, %xmm1
	addss	12(%rsp), %xmm0
	divss	%xmm2, %xmm4
	mulss	%xmm7, %xmm2
	movss	8(%rsp), %xmm7
	movaps	%xmm4, %xmm9
	divss	%xmm2, %xmm7
	movaps	%xmm1, %xmm2
	mulss	%xmm4, %xmm9
	addss	%xmm1, %xmm4
	mulss	%xmm1, %xmm1
	mulss	%xmm7, %xmm2
	movaps	%xmm7, %xmm3
	movaps	%xmm9, %xmm5
	mulss	.LC16(%rip), %xmm3
	mulss	%xmm2, %xmm4
	movaps	%xmm9, %xmm2
	mulss	%xmm7, %xmm2
	mulss	%xmm4, %xmm1
	addss	%xmm4, %xmm2
	mulss	%xmm2, %xmm5
	mulss	.LC15(%rip), %xmm2
	addss	%xmm5, %xmm1
	mulss	.LC14(%rip), %xmm1
	addss	.LC7(%rip), %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm1, %xmm0
	addss	32(%rsp), %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L49:
	pxor	%xmm1, %xmm1
	cvtsi2ss	%eax, %xmm1
	ucomiss	%xmm1, %xmm2
	jp	.L17
	je	.L54
.L17:
	movl	%eax, %edx
	pxor	%xmm1, %xmm1
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	negl	%edx
	cvtsi2ss	%edx, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L51:
	movss	.LC8(%rip), %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm2, %xmm5
	movaps	%xmm2, %xmm6
	andps	%xmm7, %xmm5
	ucomiss	%xmm5, %xmm8
	ja	.L55
.L24:
	subss	.LC9(%rip), %xmm2
	pxor	%xmm0, %xmm0
	movl	$-33, %edx
	movaps	%xmm3, %xmm6
	cvttss2si	%xmm2, %eax
	leal	(%rax,%rax), %ecx
	cltq
	subl	%ecx, %edx
	cvtsi2ss	%edx, %xmm0
	leaq	poly_deg(%rip), %rdx
	movq	(%rdx,%rax,8), %rcx
	leaq	poly_end(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	leaq	poly_coeff(%rip), %rax
	testq	%rcx, %rcx
	movss	(%rax,%rdx,4), %xmm2
	mulss	.LC10(%rip), %xmm0
	subss	%xmm0, %xmm6
	movaps	%xmm6, %xmm0
	je	.L25
	leaq	-4(%rax,%rdx,4), %rax
	subq	%rcx, %rdx
	leaq	-4+poly_coeff(%rip), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L26:
	mulss	%xmm0, %xmm2
	subq	$4, %rax
	addss	4(%rax), %xmm2
	cmpq	%rax, %rdx
	jne	.L26
.L25:
	mulss	%xmm4, %xmm2
	subss	%xmm1, %xmm3
	divss	%xmm3, %xmm2
	movaps	%xmm2, %xmm0
	call	__log1pf@PLT
.L27:
	testb	%bpl, %bpl
	jne	.L56
.L14:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movss	.LC6(%rip), %xmm0
	divss	.LC7(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	mulss	8(%rsp), %xmm1
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L55:
	cvttss2si	%xmm2, %eax
	pxor	%xmm5, %xmm5
	andnps	%xmm6, %xmm7
	cvtsi2ss	%eax, %xmm5
	movaps	%xmm5, %xmm0
	cmpnless	%xmm2, %xmm0
	movss	.LC6(%rip), %xmm2
	andps	%xmm2, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm5, %xmm2
	orps	%xmm7, %xmm2
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L52:
	movaps	%xmm1, %xmm0
	movss	%xmm3, 36(%rsp)
	movss	%xmm4, 24(%rsp)
	movss	%xmm5, 20(%rsp)
	movss	%xmm6, 16(%rsp)
	movss	%xmm2, 28(%rsp)
	call	lg_sinpi
	movss	28(%rsp), %xmm2
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	call	lg_sinpi
	movss	12(%rsp), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	call	__ieee754_logf@PLT
	movss	16(%rsp), %xmm6
	movss	%xmm0, 32(%rsp)
	movss	20(%rsp), %xmm5
	movss	24(%rsp), %xmm4
	movss	36(%rsp), %xmm3
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L56:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	60(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 60(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L50:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %ebp
	jmp	.L22
	.size	__lgamma_negf, .-__lgamma_negf
	.section	.rodata
	.align 32
	.type	poly_end, @object
	.size	poly_end, 64
poly_end:
	.quad	5
	.quad	11
	.quad	17
	.quad	24
	.quad	31
	.quad	38
	.quad	44
	.quad	50
	.align 32
	.type	poly_deg, @object
	.size	poly_deg, 64
poly_deg:
	.quad	5
	.quad	5
	.quad	5
	.quad	6
	.quad	6
	.quad	6
	.quad	5
	.quad	5
	.align 32
	.type	poly_coeff, @object
	.size	poly_coeff, 204
poly_coeff:
	.long	3213211875
	.long	3209116190
	.long	3187032625
	.long	3210967955
	.long	3179378331
	.long	3211574822
	.long	3211957001
	.long	3209321726
	.long	1046947071
	.long	3212923256
	.long	1058576062
	.long	3215510402
	.long	3210203789
	.long	3211171404
	.long	1060164849
	.long	3217629271
	.long	1072524637
	.long	3225804337
	.long	3208072866
	.long	3214229856
	.long	1069809946
	.long	3226239760
	.long	1084941639
	.long	3240071866
	.long	1099831871
	.long	3195289666
	.long	1072081602
	.long	1080059657
	.long	1087412599
	.long	1094447721
	.long	1101790601
	.long	1109321237
	.long	3201743434
	.long	1066691330
	.long	1072542355
	.long	1077445326
	.long	1082693743
	.long	1087132688
	.long	1092016954
	.long	3205120434
	.long	1062054015
	.long	1066023305
	.long	1069222893
	.long	1072390296
	.long	1075177686
	.long	3206563542
	.long	1058474889
	.long	1059752358
	.long	1062400728
	.long	1063137972
	.long	1065334593
	.align 32
	.type	lgamma_zeros, @object
	.size	lgamma_zeros, 208
lgamma_zeros:
	.long	3223142373
	.long	866119801
	.long	3224361480
	.long	3013721488
	.long	3226021998
	.long	853043604
	.long	3229426571
	.long	867712258
	.long	3229696628
	.long	871681360
	.long	3231693500
	.long	867312288
	.long	3231728467
	.long	872477842
	.long	3233805464
	.long	874963135
	.long	3233811289
	.long	3014733714
	.long	3235905120
	.long	873023377
	.long	3235905952
	.long	855933048
	.long	3238002636
	.long	838683746
	.long	3238002714
	.long	2980237285
	.long	3239051261
	.long	3017937652
	.long	3239051267
	.long	870458817
	.long	3240099840
	.long	882111108
	.long	3240099840
	.long	3029594743
	.long	3241148416
	.long	852963884
	.long	3241148416
	.long	3000447530
	.long	3242196992
	.long	823097032
	.long	3242196992
	.long	2970580679
	.long	3243245568
	.long	791712305
	.long	3243245568
	.long	2939195953
	.long	3244294144
	.long	759810981
	.long	3244294144
	.long	2907294629
	.long	3245342720
	.long	727138207
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1048576000
	.align 4
.LC1:
	.long	1078530011
	.align 4
.LC2:
	.long	1056964608
	.align 4
.LC3:
	.long	3221225472
	.align 4
.LC4:
	.long	1258291200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	1065353216
	.align 4
.LC7:
	.long	0
	.align 4
.LC8:
	.long	3238002688
	.align 4
.LC9:
	.long	1098907648
	.align 4
.LC10:
	.long	1031798784
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC12:
	.long	1076754516
	.align 4
.LC13:
	.long	867255671
	.align 4
.LC14:
	.long	978324737
	.align 4
.LC15:
	.long	3140881249
	.align 4
.LC16:
	.long	1034594987
