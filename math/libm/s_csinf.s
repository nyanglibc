	.text
	.p2align 4,,15
	.globl	__csinf
	.type	__csinf, @function
__csinf:
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movq	%xmm0, 72(%rsp)
	movss	.LC4(%rip), %xmm4
	movss	72(%rsp), %xmm2
	movaps	%xmm2, %xmm0
	movd	%xmm2, %eax
	movss	76(%rsp), %xmm7
	andps	%xmm4, %xmm0
	andl	$-2147483648, %eax
	movss	%xmm7, (%rsp)
	andps	%xmm4, %xmm7
	ucomiss	%xmm0, %xmm0
	movaps	%xmm7, %xmm3
	jp	.L2
	ucomiss	.LC5(%rip), %xmm0
	ja	.L3
	ucomiss	.LC6(%rip), %xmm0
	movl	%eax, %ebp
	jnb	.L7
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm2
	jp	.L7
	jne	.L7
	ucomiss	%xmm7, %xmm7
	jp	.L81
	ucomiss	.LC5(%rip), %xmm3
	jbe	.L36
	testl	%ebp, %ebp
	pxor	%xmm0, %xmm0
	jne	.L82
.L27:
	movaps	%xmm0, %xmm1
	movss	(%rsp), %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	ucomiss	%xmm7, %xmm7
	jp	.L52
	ucomiss	.LC5(%rip), %xmm7
	ja	.L37
	ucomiss	.LC6(%rip), %xmm7
	jnb	.L42
	movss	(%rsp), %xmm6
	ucomiss	.LC1(%rip), %xmm6
	jp	.L42
.L77:
	jne	.L42
	movaps	%xmm0, %xmm1
	movss	(%rsp), %xmm5
	subss	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	ucomiss	%xmm3, %xmm3
	jp	.L52
	ucomiss	.LC5(%rip), %xmm3
	ja	.L35
.L36:
	movsd	.LC8(%rip), %xmm1
	ucomiss	.LC6(%rip), %xmm0
	mulsd	.LC7(%rip), %xmm1
	cvttsd2si	%xmm1, %ebx
	jbe	.L67
	leaq	92(%rsp), %rsi
	leaq	88(%rsp), %rdi
	movss	%xmm3, 28(%rsp)
	movaps	%xmm4, 32(%rsp)
	call	__sincosf@PLT
	movaps	32(%rsp), %xmm4
	movss	28(%rsp), %xmm3
.L11:
	testl	%ebp, %ebp
	je	.L12
	movss	88(%rsp), %xmm0
	xorps	.LC10(%rip), %xmm0
	movss	%xmm0, 88(%rsp)
.L12:
	pxor	%xmm2, %xmm2
	movss	%xmm3, 28(%rsp)
	cvtsi2ss	%ebx, %xmm2
	ucomiss	%xmm2, %xmm3
	jbe	.L68
	movaps	%xmm2, %xmm0
	movss	%xmm2, 32(%rsp)
	movaps	%xmm4, 48(%rsp)
	call	__ieee754_expf@PLT
	movl	(%rsp), %eax
	movss	32(%rsp), %xmm2
	movaps	48(%rsp), %xmm4
	testl	%eax, %eax
	movss	28(%rsp), %xmm3
	js	.L15
	movss	92(%rsp), %xmm6
.L16:
	movss	.LC11(%rip), %xmm1
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm1
	movss	88(%rsp), %xmm5
	ucomiss	%xmm2, %xmm3
	mulss	%xmm1, %xmm5
	mulss	%xmm6, %xmm1
	movss	%xmm5, 88(%rsp)
	movss	%xmm1, 92(%rsp)
	ja	.L83
.L69:
	movaps	%xmm3, %xmm0
	movaps	%xmm4, (%rsp)
	call	__ieee754_expf@PLT
	movss	88(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	movaps	(%rsp), %xmm4
	mulss	92(%rsp), %xmm0
	movaps	%xmm2, %xmm1
	movaps	%xmm0, %xmm5
	.p2align 4,,10
	.p2align 3
.L21:
	movaps	%xmm2, %xmm6
	movss	.LC6(%rip), %xmm3
	andps	%xmm4, %xmm6
	ucomiss	%xmm6, %xmm3
	jbe	.L22
	mulss	%xmm2, %xmm2
.L22:
	andps	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm3
	jbe	.L1
	mulss	%xmm0, %xmm0
.L1:
	movss	%xmm1, 64(%rsp)
	movss	%xmm5, 68(%rsp)
	movq	64(%rsp), %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movss	(%rsp), %xmm0
	movaps	%xmm4, 32(%rsp)
	call	__ieee754_coshf@PLT
	movss	88(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	movss	(%rsp), %xmm0
	movss	%xmm2, 28(%rsp)
	call	__ieee754_sinhf@PLT
	mulss	92(%rsp), %xmm0
	movss	28(%rsp), %xmm2
	movaps	%xmm2, %xmm1
	movaps	32(%rsp), %xmm4
	movaps	%xmm0, %xmm5
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L83:
	subss	%xmm2, %xmm3
	mulss	%xmm0, %xmm5
	mulss	%xmm0, %xmm1
	ucomiss	%xmm2, %xmm3
	movss	%xmm5, 88(%rsp)
	movss	%xmm1, 92(%rsp)
	jbe	.L69
	movss	.LC5(%rip), %xmm0
	mulss	%xmm0, %xmm5
	mulss	%xmm1, %xmm0
	movaps	%xmm5, %xmm2
	movaps	%xmm5, %xmm1
	movaps	%xmm0, %xmm5
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L15:
	movss	92(%rsp), %xmm1
	movaps	%xmm1, %xmm6
	xorps	.LC10(%rip), %xmm6
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm7, %xmm7
	jp	.L52
	ucomiss	.LC5(%rip), %xmm7
	jbe	.L84
.L37:
	movaps	%xmm0, %xmm1
	movss	.LC2(%rip), %xmm5
	subss	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	movss	%xmm0, 88(%rsp)
	movl	$0x3f800000, 92(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L84:
	ucomiss	.LC6(%rip), %xmm7
	jnb	.L42
	movss	(%rsp), %xmm5
	ucomiss	.LC1(%rip), %xmm5
	jnp	.L77
	.p2align 4,,10
	.p2align 3
.L42:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movss	.LC3(%rip), %xmm1
	movaps	%xmm1, %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	ucomiss	.LC6(%rip), %xmm0
	jbe	.L70
	leaq	92(%rsp), %rsi
	leaq	88(%rsp), %rdi
	call	__sincosf@PLT
	movss	.LC10(%rip), %xmm3
	movss	92(%rsp), %xmm5
	movss	.LC12(%rip), %xmm1
	movss	88(%rsp), %xmm0
	andps	%xmm3, %xmm5
	orps	%xmm1, %xmm5
	movaps	%xmm5, %xmm2
.L28:
	andps	%xmm3, %xmm0
	testl	%ebp, %ebp
	orps	%xmm1, %xmm0
	je	.L78
	xorps	%xmm3, %xmm0
.L78:
	movl	(%rsp), %eax
	movaps	%xmm0, %xmm1
	movaps	%xmm2, %xmm5
	testl	%eax, %eax
	jns	.L1
	xorps	%xmm3, %xmm5
	jmp	.L1
.L82:
	movss	.LC0(%rip), %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L81:
	testl	%ebp, %ebp
	movss	.LC3(%rip), %xmm5
	je	.L1
	movss	.LC0(%rip), %xmm1
	movss	.LC3(%rip), %xmm5
	jmp	.L1
.L70:
	movss	.LC2(%rip), %xmm2
	movss	.LC10(%rip), %xmm3
	movss	.LC12(%rip), %xmm1
	jmp	.L28
.L52:
	movss	.LC3(%rip), %xmm1
	movaps	%xmm1, %xmm5
	jmp	.L1
	.size	__csinf, .-__csinf
	.weak	csinf32
	.set	csinf32,__csinf
	.weak	csinf
	.set	csinf,__csinf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2147483648
	.align 4
.LC1:
	.long	0
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	2139095039
	.align 4
.LC6:
	.long	8388608
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1080016896
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC11:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC12:
	.long	2139095040
	.long	0
	.long	0
	.long	0
