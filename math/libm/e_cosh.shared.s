	.text
#APP
	.symver __ieee754_cosh,__cosh_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_cosh
	.type	__ieee754_cosh, @function
__ieee754_cosh:
	movq	%xmm0, %rax
	subq	$24, %rsp
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1077280767, %eax
	jg	.L2
	cmpl	$1071001154, %eax
	jg	.L3
	cmpl	$1015021567, %eax
	movsd	.LC0(%rip), %xmm1
	jg	.L11
	movapd	%xmm1, %xmm0
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1082535489, %eax
	jle	.L12
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rcx
	andq	%rcx, %rdx
	movabsq	$4649460627574225021, %rcx
	cmpq	%rcx, %rdx
	jle	.L13
	cmpl	$2146435071, %eax
	jg	.L14
	movsd	.LC3(%rip), %xmm1
	addq	$24, %rsp
	mulsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	andpd	.LC1(%rip), %xmm0
	call	__ieee754_exp@PLT
	movsd	.LC2(%rip), %xmm1
	addq	$24, %rsp
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	andpd	.LC1(%rip), %xmm0
	call	__ieee754_exp@PLT
	movsd	.LC2(%rip), %xmm2
	addq	$24, %rsp
	movapd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	divsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movapd	%xmm0, %xmm1
	addq	$24, %rsp
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	andpd	.LC1(%rip), %xmm0
	movsd	%xmm1, 8(%rsp)
	call	__expm1@PLT
	movsd	8(%rsp), %xmm1
	addq	$24, %rsp
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm0
	divsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	andpd	.LC1(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	call	__ieee754_exp@PLT
	movsd	.LC2(%rip), %xmm1
	addq	$24, %rsp
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.size	__ieee754_cosh, .-__ieee754_cosh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC2:
	.long	0
	.long	1071644672
	.align 8
.LC3:
	.long	2281731484
	.long	2117592124
