	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__j1
	.type	__j1, @function
__j1:
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	.LC1(%rip), %xmm1
	ja	.L10
.L2:
	jmp	__ieee754_j1@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	movapd	%xmm0, %xmm1
	movl	$36, %edi
	jmp	__kernel_standard@PLT
	.size	__j1, .-__j1
	.weak	j1f32x
	.set	j1f32x,__j1
	.weak	j1f64
	.set	j1f64,__j1
	.weak	j1
	.set	j1,__j1
	.p2align 4,,15
	.globl	__y1
	.type	__y1, @function
__y1:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L18
	ucomisd	.LC1(%rip), %xmm0
	ja	.L18
.L32:
	jmp	__ieee754_y1@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L32
	subq	$24, %rsp
	movapd	%xmm0, %xmm2
	ucomisd	%xmm0, %xmm1
	ja	.L36
	ucomisd	%xmm1, %xmm0
	jp	.L16
	je	.L37
.L16:
	cmpl	$2, %eax
	jne	.L38
	addq	$24, %rsp
	jmp	__ieee754_y1@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$4, %edi
	movsd	%xmm0, 8(%rsp)
	call	__GI___feraiseexcept
	movl	$10, %edi
.L35:
	movsd	8(%rsp), %xmm2
	addq	$24, %rsp
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	movapd	%xmm2, %xmm1
	movl	$37, %edi
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %edi
	movsd	%xmm0, 8(%rsp)
	call	__GI___feraiseexcept
	movl	$11, %edi
	jmp	.L35
	.size	__y1, .-__y1
	.weak	y1f32x
	.set	y1f32x,__y1
	.weak	y1f64
	.set	y1f64,__y1
	.weak	y1
	.set	y1,__y1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1413754136
	.long	1128866299
