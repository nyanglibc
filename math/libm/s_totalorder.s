	.text
	.p2align 4,,15
	.globl	__totalorder
	.type	__totalorder, @function
__totalorder:
	movq	(%rdi), %rax
	movq	(%rsi), %rcx
	cqto
	shrq	%rdx
	xorq	%rax, %rdx
	movq	%rcx, %rax
	sarq	$63, %rax
	shrq	%rax
	xorq	%rcx, %rax
	cmpq	%rax, %rdx
	setle	%al
	movzbl	%al, %eax
	ret
	.size	__totalorder, .-__totalorder
	.weak	totalorderf32x
	.set	totalorderf32x,__totalorder
	.weak	totalorderf64
	.set	totalorderf64,__totalorder
	.weak	totalorder
	.set	totalorder,__totalorder
