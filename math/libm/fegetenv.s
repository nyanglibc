	.text
	.p2align 4,,15
	.globl	__fegetenv
	.type	__fegetenv, @function
__fegetenv:
#APP
# 24 "../sysdeps/x86_64/fpu/fegetenv.c" 1
	fnstenv (%rdi)
fldenv (%rdi)
stmxcsr 28(%rdi)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	__fegetenv, .-__fegetenv
	.weak	fegetenv
	.set	fegetenv,__fegetenv
