	.text
	.p2align 4,,15
	.globl	__tanhf
	.type	__tanhf, @function
__tanhf:
	movd	%xmm0, %eax
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %eax
	cmpl	$2139095039, %eax
	jle	.L2
	movss	.LC0(%rip), %xmm2
	testl	%ebx, %ebx
	movaps	%xmm2, %xmm1
	divss	%xmm0, %xmm1
	js	.L3
	addss	%xmm2, %xmm1
.L1:
	movaps	%xmm1, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1102053375, %eax
	jle	.L16
	movss	.LC0(%rip), %xmm1
	subss	.LC6(%rip), %xmm1
.L10:
	testl	%ebx, %ebx
	jns	.L1
	xorps	.LC5(%rip), %xmm1
	popq	%rbx
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	testl	%eax, %eax
	movaps	%xmm0, %xmm1
	je	.L1
	cmpl	$603979775, %eax
	andps	.LC1(%rip), %xmm1
	jg	.L6
	movss	.LC2(%rip), %xmm2
	ucomiss	%xmm1, %xmm2
	jbe	.L7
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
.L7:
	movss	.LC0(%rip), %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	subss	%xmm2, %xmm1
	popq	%rbx
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1065353215, %eax
	jle	.L9
	addss	%xmm1, %xmm1
	movaps	%xmm1, %xmm0
	call	__expm1f@PLT
	movss	.LC3(%rip), %xmm1
	addss	%xmm1, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movss	.LC0(%rip), %xmm1
	subss	%xmm0, %xmm1
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movss	.LC4(%rip), %xmm0
	mulss	%xmm1, %xmm0
	call	__expm1f@PLT
	movaps	%xmm0, %xmm1
	addss	.LC3(%rip), %xmm0
	xorps	.LC5(%rip), %xmm1
	divss	%xmm0, %xmm1
	jmp	.L10
	.size	__tanhf, .-__tanhf
	.weak	tanhf32
	.set	tanhf32,__tanhf
	.weak	tanhf
	.set	tanhf,__tanhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC2:
	.long	8388608
	.align 4
.LC3:
	.long	1073741824
	.align 4
.LC4:
	.long	3221225472
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	228737632
