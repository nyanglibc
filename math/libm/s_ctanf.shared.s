	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ctanf
	.type	__ctanf, @function
__ctanf:
	pushq	%rbx
	subq	$80, %rsp
	movq	%xmm0, 56(%rsp)
	movss	.LC2(%rip), %xmm3
	movss	.LC3(%rip), %xmm1
	movss	56(%rsp), %xmm0
	movaps	%xmm0, %xmm6
	movss	60(%rsp), %xmm4
	movaps	%xmm4, %xmm5
	andps	%xmm3, %xmm6
	andps	%xmm3, %xmm5
	ucomiss	%xmm6, %xmm1
	jb	.L2
	ucomiss	%xmm5, %xmm1
	jb	.L53
	movsd	.LC8(%rip), %xmm1
	movaps	%xmm4, %xmm2
	ucomiss	.LC10(%rip), %xmm6
	mulsd	.LC7(%rip), %xmm1
	mulsd	.LC9(%rip), %xmm1
	cvttsd2si	%xmm1, %ebx
	jbe	.L47
	leaq	76(%rsp), %rsi
	leaq	72(%rsp), %rdi
	movss	%xmm4, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	movss	%xmm5, 12(%rsp)
	movss	%xmm4, 8(%rsp)
	call	__sincosf@PLT
	movss	8(%rsp), %xmm2
	movaps	32(%rsp), %xmm3
	movss	12(%rsp), %xmm5
	movss	16(%rsp), %xmm4
.L17:
	pxor	%xmm6, %xmm6
	cvtsi2ss	%ebx, %xmm6
	ucomiss	%xmm6, %xmm5
	ja	.L54
	ucomiss	.LC10(%rip), %xmm5
	movss	.LC1(%rip), %xmm0
	ja	.L55
.L23:
	movss	76(%rsp), %xmm1
	movaps	%xmm1, %xmm6
	movaps	%xmm1, %xmm4
	andps	%xmm3, %xmm6
	mulss	%xmm1, %xmm4
	mulss	.LC12(%rip), %xmm6
	ucomiss	%xmm6, %xmm5
	ja	.L56
.L25:
	mulss	72(%rsp), %xmm1
	mulss	%xmm0, %xmm2
	divss	%xmm4, %xmm2
	divss	%xmm4, %xmm1
	movaps	%xmm2, %xmm4
	movaps	%xmm1, %xmm0
.L22:
	movaps	%xmm1, %xmm6
	movss	.LC10(%rip), %xmm5
	andps	%xmm3, %xmm6
	ucomiss	%xmm6, %xmm5
	jbe	.L27
	mulss	%xmm1, %xmm1
.L27:
	andps	%xmm2, %xmm3
	ucomiss	%xmm3, %xmm5
	jbe	.L1
	mulss	%xmm2, %xmm2
.L1:
	movss	%xmm0, 48(%rsp)
	movss	%xmm4, 52(%rsp)
	movq	48(%rsp), %xmm0
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movaps	%xmm2, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm5, %xmm4
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm0, %xmm0
	addl	%ebx, %ebx
	movaps	%xmm3, 32(%rsp)
	movss	%xmm4, 16(%rsp)
	movss	%xmm6, 12(%rsp)
	cvtsi2ss	%ebx, %xmm0
	movss	%xmm5, 8(%rsp)
	call	__ieee754_expf@PLT
	movss	.LC11(%rip), %xmm1
	mulss	72(%rsp), %xmm1
	movss	12(%rsp), %xmm6
	movss	8(%rsp), %xmm5
	movss	16(%rsp), %xmm4
	subss	%xmm6, %xmm5
	movaps	%xmm4, %xmm2
	movaps	32(%rsp), %xmm3
	andps	.LC5(%rip), %xmm2
	ucomiss	%xmm6, %xmm5
	mulss	76(%rsp), %xmm1
	orps	.LC6(%rip), %xmm2
	divss	%xmm0, %xmm1
	jbe	.L49
	movaps	%xmm2, %xmm4
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L55:
	movaps	%xmm4, %xmm0
	movss	%xmm4, 12(%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__ieee754_sinhf@PLT
	movss	%xmm0, 8(%rsp)
	movss	12(%rsp), %xmm4
	movaps	%xmm4, %xmm0
	call	__ieee754_coshf@PLT
	movss	8(%rsp), %xmm2
	movaps	%xmm2, %xmm5
	movaps	16(%rsp), %xmm3
	andps	%xmm3, %xmm5
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L49:
	addss	%xmm5, %xmm5
	movss	%xmm1, 12(%rsp)
	movaps	%xmm3, 16(%rsp)
	movss	%xmm2, 8(%rsp)
	movaps	%xmm5, %xmm0
	call	__ieee754_expf@PLT
	movss	12(%rsp), %xmm1
	divss	%xmm0, %xmm1
	movss	8(%rsp), %xmm2
	movaps	%xmm2, %xmm4
	movaps	16(%rsp), %xmm3
	movaps	%xmm1, %xmm0
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	.LC3(%rip), %xmm5
	ja	.L10
.L31:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jp	.L9
	je	.L1
.L9:
	ucomiss	%xmm1, %xmm4
	movss	.LC0(%rip), %xmm0
	jp	.L33
	jne	.L33
.L13:
	ucomiss	.LC3(%rip), %xmm6
	jbe	.L1
	movl	$1, %edi
	movss	%xmm4, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__GI_feraiseexcept
	movss	8(%rsp), %xmm0
	movss	12(%rsp), %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	ucomiss	%xmm1, %xmm5
	jbe	.L31
	ucomiss	.LC1(%rip), %xmm6
	jbe	.L10
	leaq	76(%rsp), %rsi
	leaq	72(%rsp), %rdi
	movss	%xmm4, 8(%rsp)
	call	__sincosf@PLT
	movss	72(%rsp), %xmm0
	movss	.LC5(%rip), %xmm1
	mulss	76(%rsp), %xmm0
	movss	8(%rsp), %xmm4
	andps	%xmm1, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L47:
	movss	%xmm0, 72(%rsp)
	movl	$0x3f800000, 76(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L10:
	movss	.LC5(%rip), %xmm1
	andps	%xmm1, %xmm0
.L12:
	andps	%xmm1, %xmm4
	orps	.LC6(%rip), %xmm4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movaps	%xmm0, %xmm4
	jmp	.L13
	.size	__ctanf, .-__ctanf
	.weak	ctanf32
	.set	ctanf32,__ctanf
	.weak	ctanf
	.set	ctanf,__ctanf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.align 4
.LC1:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 16
.LC6:
	.long	1065353216
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1080016896
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	8388608
	.align 4
.LC11:
	.long	1082130432
	.align 4
.LC12:
	.long	872415232
