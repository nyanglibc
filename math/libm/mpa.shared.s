	.text
	.p2align 4,,15
	.globl	__acr
	.type	__acr, @function
__acr:
	cmpq	$0, 8(%rdi)
	movq	8(%rsi), %rcx
	jne	.L2
	xorl	%eax, %eax
	testq	%rcx, %rcx
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.L1
	movl	(%rsi), %ecx
	cmpl	%ecx, (%rdi)
	jg	.L1
	movl	$-1, %eax
	jl	.L1
	testl	%edx, %edx
	jle	.L14
	movq	16(%rdi), %rcx
	movq	16(%rsi), %r8
	cmpq	%rcx, %r8
	je	.L16
.L6:
	xorl	%eax, %eax
	cmpq	%rcx, %r8
	setl	%al
	leal	-1(%rax,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L16:
	movslq	%edx, %rax
	movl	$1, %edx
	addq	$1, %rax
.L8:
	addq	$1, %rdx
	cmpq	%rax, %rdx
	je	.L14
	movq	8(%rdi,%rdx,8), %rcx
	movq	8(%rsi,%rdx,8), %r8
	cmpq	%r8, %rcx
	jne	.L6
	jmp	.L8
.L14:
	xorl	%eax, %eax
	ret
	.size	__acr, .-__acr
	.p2align 4,,15
	.globl	__cpy
	.type	__cpy, @function
__cpy:
	movl	(%rdi), %eax
	testl	%edx, %edx
	movl	%eax, (%rsi)
	js	.L17
	movslq	%edx, %rdx
	xorl	%eax, %eax
	leaq	1(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L19:
	movq	8(%rdi,%rax,8), %rdx
	movq	%rdx, 8(%rsi,%rax,8)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L19
.L17:
	rep ret
	.size	__cpy, .-__cpy
	.p2align 4,,15
	.type	add_magnitudes, @function
add_magnitudes:
	movl	(%rdi), %r11d
	movslq	%ecx, %r10
	movq	%rdx, %r8
	movl	%r11d, (%rdx)
	movslq	(%rsi), %r9
	movslq	(%rdi), %rax
	addq	%r10, %r9
	subq	%rax, %r9
	testq	%r9, %r9
	jle	.L41
	pushq	%r12
	leaq	0(,%r10,8), %rax
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %rbx
	leaq	(%rsi,%r9,8), %rbp
	negq	%rbx
	leaq	(%rdi,%rax), %r12
	leaq	(%rdx,%rax), %rsi
	salq	$3, %rbx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L43:
	subq	$16777216, %rax
	movq	%rax, 16(%rsi,%rcx)
	subq	$8, %rcx
	movl	$1, %eax
	cmpq	%rbx, %rcx
	je	.L42
.L25:
	movq	8(%rbp,%rcx), %rdx
	addq	8(%r12,%rcx), %rdx
	addq	%rdx, %rax
	cmpq	$16777215, %rax
	jg	.L43
	movq	%rax, 16(%rsi,%rcx)
	subq	$8, %rcx
	xorl	%eax, %eax
	cmpq	%rbx, %rcx
	jne	.L25
.L42:
	movl	$1, %edx
	subq	%r9, %rdx
	leaq	(%rdx,%r10), %rcx
	leaq	-1(%rdx,%r10), %rdx
	testq	%rdx, %rdx
	jle	.L26
	subq	%rdx, %rcx
	leaq	(%r8,%rcx,8), %rcx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L44:
	subq	$16777216, %rax
	movq	%rax, 8(%rcx,%rdx,8)
	subq	$1, %rdx
	movl	$1, %eax
	je	.L26
.L29:
	addq	8(%rdi,%rdx,8), %rax
	cmpq	$16777215, %rax
	jg	.L44
	movq	%rax, 8(%rcx,%rdx,8)
	xorl	%eax, %eax
	subq	$1, %rdx
	jne	.L29
.L26:
	testq	%rax, %rax
	jne	.L30
	testq	%r10, %r10
	leaq	1(%r10), %rcx
	movl	$1, %eax
	jle	.L21
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$1, %rax
	movq	8(%r8,%rax,8), %rdx
	cmpq	%rcx, %rax
	movq	%rdx, (%r8,%rax,8)
	jne	.L33
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	addl	$1, %r11d
	movq	$1, 16(%r8)
	movl	%r11d, (%r8)
.L21:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movl	%ecx, %edx
	movq	%r8, %rsi
	jmp	__cpy@PLT
	.size	add_magnitudes, .-add_magnitudes
	.p2align 4,,15
	.type	sub_magnitudes, @function
sub_magnitudes:
	pushq	%r13
	pushq	%r12
	movslq	%ecx, %r9
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r8
	movl	(%rdi), %ebp
	movl	%ebp, (%rdx)
	movslq	(%rsi), %r10
	movslq	(%rdi), %rax
	addq	%r9, %r10
	subq	%rax, %r10
	testq	%r10, %r10
	jle	.L70
	cmpq	%r10, %r9
	leaq	1(%r9), %r12
	jg	.L71
.L47:
	movq	$0, 8(%r8,%r12,8)
	xorl	%eax, %eax
.L48:
	leaq	0(,%r9,8), %rdx
	movq	%r10, %r11
	leaq	(%rsi,%r10,8), %rcx
	negq	%r11
	movq	$-1, %r13
	leaq	(%rdi,%rdx), %rbx
	leaq	(%r8,%rdx), %rsi
	salq	$3, %r11
	xorl	%edx, %edx
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rax, 8(%rsi,%rdx)
	subq	$8, %rdx
	xorl	%eax, %eax
	cmpq	%r11, %rdx
	je	.L72
.L51:
	addq	8(%rbx,%rdx), %rax
	subq	8(%rcx,%rdx), %rax
	jns	.L49
	addq	$16777216, %rax
	movq	%rax, 8(%rsi,%rdx)
	subq	$8, %rdx
	movq	%r13, %rax
	cmpq	%r11, %rdx
	jne	.L51
.L72:
	movq	%r9, %rdx
	subq	%r10, %rdx
	testq	%rdx, %rdx
	jle	.L52
	movq	$-1, %rcx
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L53:
	movq	%rax, 16(%r8,%rdx,8)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L52
.L55:
	addq	8(%rdi,%rdx,8), %rax
	subq	$1, %rdx
	testq	%rax, %rax
	jns	.L53
	addq	$16777216, %rax
	testq	%rdx, %rdx
	movq	%rax, 16(%r8,%rdx,8)
	movq	%rcx, %rax
	jne	.L55
.L52:
	cmpq	$0, 16(%r8)
	leal	1(%rbp), %edx
	movl	$1, %eax
	jne	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$1, %rax
	cmpq	$0, 8(%r8,%rax,8)
	je	.L57
	subl	%eax, %edx
	movl	%edx, %ebp
.L56:
	cmpq	%r12, %rax
	movl	%ebp, (%r8)
	jg	.L63
	movq	%rax, %rdx
	leaq	2(%r9), %rsi
	movq	%rax, %rdi
	negq	%rdx
	leaq	(%r8,%rdx,8), %rcx
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$1, %rax
	movq	(%r8,%rax,8), %rdx
	cmpq	%rsi, %rax
	movq	%rdx, 8(%rcx,%rax,8)
	jne	.L59
	leaq	3(%r9), %rax
	subq	%rdi, %rax
.L58:
	cmpq	%rax, %r9
	jl	.L45
	addq	$1, %r9
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$1, %rax
	cmpq	%rax, %r9
	movq	$0, (%r8,%rax,8)
	jne	.L61
.L45:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movq	16(%rsi,%r10,8), %rax
	testq	%rax, %rax
	jle	.L47
	movl	$16777216, %edx
	subq	%rax, %rdx
	movq	$-1, %rax
	movq	%rdx, 8(%r8,%r12,8)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$1, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L70:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	movl	%ecx, %edx
	movq	%r8, %rsi
	jmp	__cpy@PLT
	.size	sub_magnitudes, .-sub_magnitudes
	.p2align 4,,15
	.globl	__mp_dbl
	.type	__mp_dbl, @function
__mp_dbl:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L80
	movl	(%rdi), %r8d
	cmpl	$-41, %r8d
	jl	.L155
	movq	16(%rdi), %rcx
.L78:
	cmpl	$4, %edx
	pushq	%rbx
	jg	.L98
	pxor	%xmm1, %xmm1
	cmpl	$1, %edx
	cvtsi2sdq	%rcx, %xmm1
	je	.L99
	pxor	%xmm0, %xmm0
	cmpl	$2, %edx
	cvtsi2sdq	24(%rdi), %xmm0
	je	.L156
	pxor	%xmm4, %xmm4
	cmpl	$3, %edx
	cvtsi2sdq	32(%rdi), %xmm4
	je	.L157
	pxor	%xmm2, %xmm2
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	40(%rdi), %xmm2
	mulsd	%xmm3, %xmm2
	mulsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm2
	addsd	%xmm1, %xmm0
	mulsd	.LC5(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm1
	.p2align 4,,10
	.p2align 3
.L99:
	pxor	%xmm0, %xmm0
	movslq	%r8d, %rdx
	cmpq	$1, %rdx
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm1, %xmm0
	jle	.L111
	movsd	.LC6(%rip), %xmm1
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L112:
	addq	$1, %rax
	mulsd	%xmm1, %xmm0
	cmpq	%rax, %rdx
	jne	.L112
.L113:
	movsd	%xmm0, (%rsi)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movq	$0x000000000, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	cmpq	$8388607, %rcx
	movq	%rcx, -40(%rsp)
	jg	.L125
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L103:
	addq	%rcx, %rcx
	addq	%r9, %r9
	cmpq	$8388607, %rcx
	jle	.L103
	pxor	%xmm2, %xmm2
	movq	%rcx, -40(%rsp)
	cvtsi2sdq	%r9, %xmm2
.L102:
	leaq	-48(%rsp), %r11
	movl	$2, %ecx
.L104:
	movq	8(%rdi,%rcx,8), %r10
	imulq	%r9, %r10
	movq	%r10, %rbx
	sarq	$24, %r10
	addq	%r10, -8(%r11,%rcx,8)
	andl	$16777215, %ebx
	movq	%rbx, (%r11,%rcx,8)
	addq	$1, %rcx
	cmpq	$5, %rcx
	jne	.L104
	movq	-24(%rsp), %r9
	movq	%r9, %r10
	andl	$524287, %r10d
	cmpq	$262144, %r10
	je	.L158
.L105:
	pxor	%xmm1, %xmm1
	movsd	.LC3(%rip), %xmm3
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r9, %xmm1
	cvtsi2sdq	-32(%rsp), %xmm0
	mulsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-40(%rsp), %xmm1
	mulsd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L158:
	cmpq	$0, -16(%rsp)
	jne	.L146
	movslq	%edx, %rdx
	addq	$1, %rdx
	cmpq	$0, 48(%rdi)
	jne	.L146
.L110:
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	je	.L105
	cmpq	$0, 8(%rdi,%rcx,8)
	je	.L110
	.p2align 4,,10
	.p2align 3
.L146:
	addq	$1, %r9
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$-42, %r8d
	jne	.L77
	movq	16(%rdi), %rcx
	cmpq	$1023, %rcx
	jg	.L78
	movslq	%edx, %rdx
	cmpq	$1, %rdx
	je	.L159
	cmpq	$2, %rdx
	je	.L123
.L150:
	movq	24(%rdi), %r10
	pxor	%xmm4, %xmm4
	pxor	%xmm0, %xmm0
	leaq	1024(%rcx), %r11
	movq	32(%rdi), %rcx
	movsd	.LC1(%rip), %xmm1
	cvtsi2sdq	%r11, %xmm4
	movl	$3, %r9d
	cvtsi2sdq	%r10, %xmm0
.L91:
	movq	%rcx, %r8
	andq	$-32, %r8
	cmpq	%rcx, %r8
	je	.L92
.L121:
	pxor	%xmm2, %xmm2
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	%rcx, %xmm2
	mulsd	%xmm3, %xmm2
.L93:
	addsd	%xmm0, %xmm2
	mulsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	subsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rax, %xmm1
	mulsd	%xmm1, %xmm0
	mulsd	.LC4(%rip), %xmm0
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	testq	%rdx, %rdx
	jg	.L113
	movsd	.LC3(%rip), %xmm3
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L114:
	subq	$1, %rax
	mulsd	%xmm3, %xmm0
	cmpq	%rax, %rdx
	jne	.L114
	movsd	%xmm0, (%rsi)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	movsd	.LC3(%rip), %xmm3
	mulsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm1
	jmp	.L99
.L77:
	cmpl	$-44, %r8d
	jl	.L80
	movslq	%edx, %rdx
	movq	16(%rdi), %rcx
	jne	.L81
	cmpq	$31, %rcx
	jle	.L80
	cmpq	$1, %rdx
	je	.L160
	cmpq	$2, %rdx
	je	.L87
.L124:
	movsd	.LC1(%rip), %xmm1
	xorl	%r10d, %r10d
	movl	$1024, %r11d
	pxor	%xmm0, %xmm0
	movl	$1, %r9d
	movapd	%xmm1, %xmm4
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L125:
	movsd	.LC2(%rip), %xmm2
	movl	$1, %r9d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L157:
	movsd	.LC3(%rip), %xmm3
	mulsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm1
	jmp	.L99
.L160:
	pxor	%xmm0, %xmm0
.L84:
	movq	%rcx, %r8
	movsd	.LC1(%rip), %xmm1
	andq	$-32, %r8
	cmpq	%rcx, %r8
	movapd	%xmm1, %xmm4
	jne	.L121
.L120:
	pxor	%xmm2, %xmm2
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	%r8, %xmm2
	mulsd	%xmm3, %xmm2
	jmp	.L93
.L159:
	leaq	1024(%rcx), %rdx
	xorl	%ecx, %ecx
.L83:
	pxor	%xmm4, %xmm4
	pxor	%xmm0, %xmm0
	pxor	%xmm2, %xmm2
	cvtsi2sdq	%rdx, %xmm4
	cvtsi2sdq	%rcx, %xmm0
	movsd	.LC1(%rip), %xmm1
	movsd	.LC3(%rip), %xmm3
	jmp	.L93
.L123:
	leaq	1024(%rcx), %rdx
	movq	24(%rdi), %rcx
	jmp	.L83
.L87:
	testb	$31, %cl
	je	.L161
	pxor	%xmm2, %xmm2
	movsd	.LC3(%rip), %xmm3
	movsd	.LC1(%rip), %xmm1
	cvtsi2sdq	%rcx, %xmm2
	pxor	%xmm0, %xmm0
	movapd	%xmm1, %xmm4
	mulsd	%xmm3, %xmm2
	jmp	.L93
.L81:
	cmpq	$1, %rdx
	je	.L162
	cmpq	$2, %rdx
	je	.L163
	cmpl	$-42, %r8d
	je	.L150
	cmpl	$-43, %r8d
	jne	.L124
	pxor	%xmm0, %xmm0
	movq	%rcx, %r10
	movl	$1024, %r11d
	movsd	.LC1(%rip), %xmm1
	movl	$2, %r9d
	cvtsi2sdq	%rcx, %xmm0
	movq	24(%rdi), %rcx
	movapd	%xmm1, %xmm4
	jmp	.L91
.L92:
	leaq	1(%r9), %rcx
	cmpq	%rdx, %rcx
	jg	.L120
.L96:
	cmpq	$0, 8(%rdi,%rcx,8)
	je	.L164
.L95:
	pxor	%xmm2, %xmm2
	addq	$1, %r8
	pxor	%xmm4, %xmm4
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r8, %xmm2
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	%r11, %xmm4
	cvtsi2sdq	%r10, %xmm0
	mulsd	%xmm3, %xmm2
	jmp	.L93
.L164:
	addq	$1, %rcx
	cmpq	%rdx, %rcx
	jle	.L96
	movq	%r8, %rcx
.L122:
	pxor	%xmm2, %xmm2
	pxor	%xmm4, %xmm4
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm2
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	%r11, %xmm4
	cvtsi2sdq	%r10, %xmm0
	mulsd	%xmm3, %xmm2
	jmp	.L93
.L161:
	cmpq	$0, 24(%rdi)
	je	.L128
	movq	%rcx, %r8
	movl	$1024, %r11d
	xorl	%r10d, %r10d
	movsd	.LC1(%rip), %xmm1
	jmp	.L95
.L163:
	cmpl	$-42, %r8d
	je	.L123
	cmpl	$-43, %r8d
	jne	.L87
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	movq	24(%rdi), %rcx
	jmp	.L84
.L162:
	movl	$1024, %edx
	jmp	.L83
.L128:
	movl	$1024, %r11d
	xorl	%r10d, %r10d
	movsd	.LC1(%rip), %xmm1
	jmp	.L122
	.size	__mp_dbl, .-__mp_dbl
	.p2align 4,,15
	.globl	__dbl_mp
	.type	__dbl_mp, @function
__dbl_mp:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L166
	je	.L191
.L166:
	ucomisd	%xmm1, %xmm0
	jbe	.L190
	movq	$1, 8(%rdi)
.L171:
	movsd	.LC6(%rip), %xmm2
	movl	$1, (%rdi)
	movl	$2, %eax
	movsd	.LC3(%rip), %xmm1
	ucomisd	%xmm2, %xmm0
	jb	.L172
	.p2align 4,,10
	.p2align 3
.L174:
	mulsd	%xmm1, %xmm0
	movl	%eax, %edx
	addl	$1, %eax
	ucomisd	%xmm2, %xmm0
	jnb	.L174
	movl	%edx, (%rdi)
.L172:
	movsd	.LC2(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.L175
	movl	(%rdi), %eax
	subl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L177:
	mulsd	%xmm2, %xmm0
	movl	%eax, %edx
	subl	$1, %eax
	ucomisd	%xmm0, %xmm1
	ja	.L177
	movl	%edx, (%rdi)
.L175:
	movslq	%esi, %rsi
	movl	$4, %ecx
	cmpq	$4, %rsi
	cmovle	%rsi, %rcx
	testq	%rsi, %rsi
	jle	.L165
	movl	$1, %eax
.L179:
	cvttsd2siq	%xmm0, %rdx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	movq	%rdx, 8(%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rcx
	subsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	jge	.L179
	cmpq	%rax, %rsi
	jl	.L165
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L180:
	movq	$0, 8(%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L180
.L165:
	rep ret
	.p2align 4,,10
	.p2align 3
.L191:
	movq	$0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	movq	$-1, 8(%rdi)
	xorpd	.LC7(%rip), %xmm0
	jmp	.L171
	.size	__dbl_mp, .-__dbl_mp
	.p2align 4,,15
	.globl	__add
	.type	__add, @function
__add:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rsi
	subq	$8, %rsp
	movq	8(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L202
	movq	8(%rbp), %r8
	testq	%r8, %r8
	je	.L203
	cmpq	%r8, %rdx
	movq	%rsi, %r13
	movl	%ecx, %r12d
	movq	%rdi, %rbx
	movl	%ecx, %edx
	movq	%rbp, %rsi
	je	.L204
	call	__acr@PLT
	cmpl	$1, %eax
	je	.L205
	cmpl	$-1, %eax
	je	.L206
	movq	$0, 8(%r13)
.L192:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L203:
	addq	$8, %rsp
	movl	%ecx, %edx
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__cpy@PLT
	.p2align 4,,10
	.p2align 3
.L202:
	addq	$8, %rsp
	movq	%rbp, %rdi
	movl	%ecx, %edx
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__cpy@PLT
	.p2align 4,,10
	.p2align 3
.L204:
	call	__acr@PLT
	testl	%eax, %eax
	movl	%r12d, %ecx
	movq	%r13, %rdx
	jle	.L196
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	add_magnitudes
	movq	8(%rbx), %rax
	movq	%rax, 8(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	add_magnitudes
	movq	8(%rbp), %rax
	movq	%rax, 8(%r13)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L205:
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	sub_magnitudes
	movq	8(%rbx), %rax
	movq	%rax, 8(%r13)
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L206:
	movl	%r12d, %ecx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	sub_magnitudes
	movq	8(%rbp), %rax
	movq	%rax, 8(%r13)
	jmp	.L192
	.size	__add, .-__add
	.p2align 4,,15
	.globl	__sub
	.type	__sub, @function
__sub:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$16, %rsp
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L216
	movq	8(%rsi), %rdx
	movq	%rdi, %rbx
	testq	%rdx, %rdx
	je	.L217
	cmpq	%rdx, %rax
	movl	%ecx, 12(%rsp)
	movl	%ecx, %edx
	je	.L211
	call	__acr@PLT
	testl	%eax, %eax
	movq	%r12, %rdx
	movl	12(%rsp), %ecx
	jle	.L212
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	add_magnitudes
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
.L207:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	addq	$16, %rsp
	movq	%r12, %rsi
	movl	%ecx, %edx
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__cpy@PLT
	.p2align 4,,10
	.p2align 3
.L216:
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movl	%ecx, %edx
	call	__cpy@PLT
	negq	8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	call	__acr@PLT
	cmpl	$1, %eax
	movl	12(%rsp), %ecx
	je	.L218
	cmpl	$-1, %eax
	je	.L219
	movq	$0, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L212:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	add_magnitudes
	movq	8(%rbp), %rax
	negq	%rax
	movq	%rax, 8(%r12)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	sub_magnitudes
	movq	8(%rbx), %rax
	movq	%rax, 8(%r12)
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L219:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	sub_magnitudes
	movq	8(%rbp), %rax
	negq	%rax
	movq	%rax, 8(%r12)
	jmp	.L207
	.size	__sub, .-__sub
	.p2align 4,,15
	.globl	__mul
	.type	__mul, @function
__mul:
	movq	8(%rdi), %rax
	imulq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L221
	pushq	%rbp
	movslq	%ecx, %rcx
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	testq	%rcx, %rcx
	movq	8(%rdi,%rcx,8), %rax
	jle	.L222
	testq	%rax, %rax
	jne	.L257
	cmpq	$0, 8(%rsi,%rcx,8)
	jne	.L258
	movq	%rcx, %rax
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L227:
	testq	%r9, %r9
	jne	.L259
	cmpq	$0, 8(%rsi,%rax,8)
	jne	.L260
.L224:
	subq	$1, %rax
	testq	%rax, %rax
	movq	8(%rdi,%rax,8), %r9
	jne	.L227
	xorl	%r8d, %r8d
	testq	%r9, %r9
	jne	.L231
.L230:
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L231:
	cmpq	$2, %rcx
	leaq	3(%rcx), %r10
	jle	.L229
.L237:
	addq	%r8, %rax
	leaq	1(%rax), %r11
	cmpq	%r11, %r10
	jle	.L236
	leaq	8(%rdx,%r10,8), %r9
	leaq	16(%rdx,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L238:
	movq	$0, (%r9)
	subq	$8, %r9
	cmpq	%r9, %rax
	jne	.L238
	movq	%r11, %r10
.L236:
	leaq	0(,%r10,8), %r12
	leaq	30(%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r11
	andq	$-16, %r11
	testq	%r8, %r8
	jle	.L263
	addq	$1, %r8
	xorl	%r9d, %r9d
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L240:
	movq	8(%rdi,%rax,8), %rbx
	imulq	8(%rsi,%rax,8), %rbx
	addq	%rbx, %r9
	movq	%r9, (%r11,%rax,8)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L240
.L239:
	cmpq	%rax, %r10
	jle	.L241
	leaq	(%r11,%rax,8), %rax
	leaq	(%r11,%r12), %r8
	.p2align 4,,10
	.p2align 3
.L242:
	movq	%r9, (%rax)
	addq	$8, %rax
	cmpq	%rax, %r8
	jne	.L242
.L241:
	cmpq	%r10, %rcx
	jge	.L264
	movq	%r10, %r13
	xorl	%eax, %eax
	subq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L247:
	testb	$1, %r10b
	jne	.L244
	movq	%r10, %r8
	shrq	$63, %r8
	addq	%r10, %r8
	sarq	%r8
	movq	8(%rdi,%r8,8), %rbx
	leaq	(%rbx,%rbx), %r9
	imulq	8(%rsi,%r8,8), %r9
	addq	%r9, %rax
.L244:
	cmpq	%r13, %rcx
	movq	%r13, %r9
	jle	.L245
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L246:
	movq	8(%rdi,%rbx,8), %r8
	movq	8(%rsi,%rbx,8), %r12
	subq	$1, %rbx
	addq	8(%rdi,%r9,8), %r8
	addq	8(%rsi,%r9,8), %r12
	addq	$1, %r9
	imulq	%r12, %r8
	addq	%r8, %rax
	cmpq	%rbx, %r9
	jl	.L246
.L245:
	subq	-8(%r11,%r10,8), %rax
	subq	$1, %r13
	movq	%rax, %r8
	sarq	$24, %rax
	andl	$16777215, %r8d
	movq	%r8, 8(%rdx,%r10,8)
	subq	$1, %r10
	cmpq	%r10, %rcx
	jne	.L247
.L243:
	cmpq	$1, %r10
	jle	.L248
	.p2align 4,,10
	.p2align 3
.L256:
	testb	$1, %r10b
	jne	.L249
	movq	%r10, %r9
	sarq	%r9
	movq	8(%rdi,%r9,8), %rbx
	leaq	(%rbx,%rbx), %r8
	imulq	8(%rsi,%r9,8), %r8
	addq	%r8, %rax
.L249:
	subq	$1, %r10
	cmpq	$1, %r10
	je	.L250
	movq	%r10, %rbx
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L251:
	movq	8(%rdi,%rbx,8), %r8
	movq	8(%rsi,%rbx,8), %r12
	subq	$1, %rbx
	addq	8(%rdi,%r9,8), %r8
	addq	8(%rsi,%r9,8), %r12
	addq	$1, %r9
	imulq	%r12, %r8
	addq	%r8, %rax
	cmpq	%rbx, %r9
	jl	.L251
	subq	(%r11,%r10,8), %rax
	movq	%rax, %r8
	sarq	$24, %rax
	andl	$16777215, %r8d
	movq	%r8, 16(%rdx,%r10,8)
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L250:
	subq	8(%r11), %rax
	movq	%rax, %r8
	sarq	$24, %rax
	andl	$16777215, %r8d
	movq	%r8, 24(%rdx)
.L248:
	movq	%rax, 8(%rdx,%r10,8)
	movl	(%rsi), %eax
	addl	(%rdi), %eax
	cmpq	$0, 16(%rdx)
	je	.L279
.L253:
	movl	%eax, (%rdx)
	movq	8(%rdi), %rax
	imulq	8(%rsi), %rax
	movq	%rax, 8(%rdx)
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
.L259:
	movq	%rsi, %r9
.L223:
	cmpq	$0, 8(%r9,%rax,8)
	movq	%rax, %r8
	je	.L232
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L233:
	cmpq	$0, 8(%r9,%r8,8)
	jne	.L231
.L232:
	subq	$1, %r8
	jne	.L233
	jmp	.L231
.L260:
	movq	%rdi, %r9
	jmp	.L223
.L263:
	xorl	%r9d, %r9d
	movl	$1, %eax
	jmp	.L239
.L264:
	xorl	%eax, %eax
	jmp	.L243
.L221:
	movq	$0, 8(%rdx)
	ret
.L279:
	testq	%rcx, %rcx
	jle	.L254
	leaq	16(%rdx), %r8
	leaq	(%r8,%rcx,8), %r9
	.p2align 4,,10
	.p2align 3
.L255:
	movq	8(%r8), %rcx
	addq	$8, %r8
	movq	%rcx, -8(%r8)
	cmpq	%r8, %r9
	jne	.L255
.L254:
	subl	$1, %eax
	jmp	.L253
.L222:
	testq	%rax, %rax
	je	.L280
	movq	%rcx, %r8
	movq	%rcx, %rax
.L229:
	leaq	(%rcx,%rcx), %r10
	jmp	.L237
.L257:
	movq	%rsi, %r9
	movq	%rcx, %rax
	jmp	.L223
.L258:
	movq	%rdi, %r9
	movq	%rcx, %rax
	jmp	.L223
.L280:
	movq	%rcx, %rax
	jmp	.L230
	.size	__mul, .-__mul
	.p2align 4,,15
	.globl	__sqr
	.type	__sqr, @function
__sqr:
	cmpq	$0, 8(%rdi)
	je	.L318
	movslq	%edx, %r11
	pushq	%rbx
	testq	%r11, %r11
	movq	%r11, %rax
	jle	.L284
	cmpq	$0, 8(%rdi,%r11,8)
	je	.L285
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L286:
	cmpq	$0, 8(%rdi,%rax,8)
	jne	.L284
.L285:
	subq	$1, %rax
	jne	.L286
.L284:
	leal	3(%rdx), %r8d
	leal	(%rdx,%rdx), %ecx
	cmpl	$2, %edx
	movslq	%ecx, %rcx
	movslq	%r8d, %r8
	cmovle	%rcx, %r8
	leaq	1(%rax,%rax), %rcx
	cmpq	%rcx, %r8
	jle	.L289
	salq	$4, %rax
	leaq	8(%rsi,%r8,8), %rdx
	leaq	16(%rsi,%rax), %rax
	.p2align 4,,10
	.p2align 3
.L291:
	movq	$0, (%rdx)
	subq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.L291
	movq	%rcx, %r8
.L289:
	cmpq	%r8, %r11
	jge	.L308
	movq	%r8, %rbx
	xorl	%r10d, %r10d
	subq	%r11, %rbx
	.p2align 4,,10
	.p2align 3
.L296:
	testb	$1, %r8b
	jne	.L293
	movq	%r8, %rax
	shrq	$63, %rax
	addq	%r8, %rax
	sarq	%rax
	movq	8(%rdi,%rax,8), %rax
	imulq	%rax, %rax
	addq	%rax, %r10
.L293:
	cmpq	%rbx, %r11
	movq	%rbx, %rax
	jle	.L294
	movq	%r11, %rdx
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L295:
	movq	8(%rdi,%rax,8), %rcx
	addq	$1, %rax
	imulq	8(%rdi,%rdx,8), %rcx
	subq	$1, %rdx
	addq	%rcx, %r9
	cmpq	%rdx, %rax
	jl	.L295
	leaq	(%r10,%r9,2), %r10
.L294:
	movq	%r10, %rax
	subq	$1, %rbx
	sarq	$24, %r10
	andl	$16777215, %eax
	movq	%rax, 8(%rsi,%r8,8)
	subq	$1, %r8
	cmpq	%r8, %r11
	jne	.L296
	movq	%r11, %r8
.L292:
	cmpq	$1, %r8
	jle	.L297
	.p2align 4,,10
	.p2align 3
.L305:
	testb	$1, %r8b
	jne	.L298
	movq	%r8, %rax
	sarq	%rax
	movq	8(%rdi,%rax,8), %rax
	imulq	%rax, %rax
	addq	%rax, %r10
.L298:
	subq	$1, %r8
	cmpq	$1, %r8
	je	.L299
	movq	%r8, %rdx
	xorl	%r9d, %r9d
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L300:
	movq	8(%rdi,%rax,8), %rcx
	addq	$1, %rax
	imulq	8(%rdi,%rdx,8), %rcx
	subq	$1, %rdx
	addq	%rcx, %r9
	cmpq	%rdx, %rax
	jl	.L300
	leaq	(%r10,%r9,2), %r10
	movq	%r10, %rax
	sarq	$24, %r10
	andl	$16777215, %eax
	movq	%rax, 16(%rsi,%r8,8)
	jmp	.L305
	.p2align 4,,10
	.p2align 3
.L299:
	movq	%r10, %rax
	sarq	$24, %r10
	andl	$16777215, %eax
	movq	%rax, 24(%rsi)
.L297:
	movl	(%rdi), %eax
	movq	%r10, 8(%rsi,%r8,8)
	movq	$1, 8(%rsi)
	addl	%eax, %eax
	cmpq	$0, 16(%rsi)
	je	.L319
.L302:
	movl	%eax, (%rsi)
	popq	%rbx
	ret
.L308:
	xorl	%r10d, %r10d
	jmp	.L292
.L318:
	movq	$0, 8(%rsi)
	ret
.L319:
	testq	%r11, %r11
	jle	.L303
	leaq	16(%rsi), %rdx
	leaq	(%rdx,%r11,8), %rdi
	.p2align 4,,10
	.p2align 3
.L304:
	movq	8(%rdx), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rdx, %rdi
	jne	.L304
.L303:
	subl	$1, %eax
	jmp	.L302
	.size	__sqr, .-__sqr
	.p2align 4,,15
	.globl	__dvd
	.type	__dvd, @function
__dvd:
	cmpq	$0, 8(%rdi)
	jne	.L321
	movq	$0, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %ebx
	subq	$1064, %rsp
	leaq	384(%rsp), %r13
	movq	%rdx, 24(%rsp)
	movq	%rdi, 16(%rsp)
	movl	%ecx, %edx
	movq	%r14, %rdi
	leaq	48(%rsp), %rbp
	movq	%r13, %rsi
	call	__cpy@PLT
	leaq	40(%rsp), %rsi
	movl	%ebx, %edx
	movq	%r13, %rdi
	movl	$0, 384(%rsp)
	call	__mp_dbl@PLT
	movsd	.LC2(%rip), %xmm0
	movl	%ebx, %esi
	movq	%rbp, %rdi
	divsd	40(%rsp), %xmm0
	movsd	%xmm0, 40(%rsp)
	call	__dbl_mp@PLT
	movl	(%r14), %eax
	movslq	%ebx, %rdx
	subl	%eax, 48(%rsp)
	leaq	np1.3278(%rip), %rax
	movslq	(%rax,%rdx,4), %rax
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	jle	.L323
	leaq	720(%rsp), %r12
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L324:
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__cpy@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	addq	$1, %r15
	call	__mul@PLT
	movq	__mptwo@GOTPCREL(%rip), %rdi
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	call	__sub@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__mul@PLT
	cmpq	8(%rsp), %r15
	jne	.L324
.L323:
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%rbp, %rsi
	call	__mul@PLT
	addq	$1064, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__dvd, .-__dvd
	.section	.rodata
	.align 32
	.type	np1.3278, @object
	.size	np1.3278, 132
np1.3278:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.globl	__mptwo
	.align 32
	.type	__mptwo, @object
	.size	__mptwo, 328
__mptwo:
	.long	1
	.zero	4
	.quad	1
	.quad	2
	.zero	304
	.globl	__mpone
	.align 32
	.type	__mpone, @object
	.size	__mpone, 328
__mpone:
	.long	1
	.zero	4
	.quad	1
	.quad	1
	.zero	304
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1083179008
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	1047527424
	.align 8
.LC4:
	.long	0
	.long	1024
	.align 8
.LC5:
	.long	0
	.long	1022361600
	.align 8
.LC6:
	.long	0
	.long	1097859072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
