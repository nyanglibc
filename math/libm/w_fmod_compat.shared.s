	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fmod
	.type	__fmod, @function
__fmod:
	movapd	%xmm0, %xmm2
	andpd	.LC0(%rip), %xmm2
	ucomisd	.LC1(%rip), %xmm2
	seta	%dl
	ja	.L4
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L4
.L2:
	jmp	__ieee754_fmod@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	ucomisd	%xmm1, %xmm0
	jp	.L2
	movl	$27, %edi
	jmp	__kernel_standard@PLT
	.size	__fmod, .-__fmod
	.weak	fmodf32x
	.set	fmodf32x,__fmod
	.weak	fmodf64
	.set	fmodf64,__fmod
	.weak	fmod
	.set	fmod,__fmod
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
