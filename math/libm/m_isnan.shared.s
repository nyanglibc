	.text
	.p2align 4,,15
	.globl	__GI___isnan
	.hidden	__GI___isnan
	.type	__GI___isnan, @function
__GI___isnan:
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rdx
	movabsq	$9218868437227405312, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__GI___isnan, .-__GI___isnan
	.globl	__isnan
	.set	__isnan,__GI___isnan
	.weak	isnan
	.set	isnan,__isnan
