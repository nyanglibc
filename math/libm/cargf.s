	.text
	.p2align 4,,15
	.globl	__cargf
	.type	__cargf, @function
__cargf:
	movq	%xmm0, -16(%rsp)
	movss	-16(%rsp), %xmm1
	movss	-12(%rsp), %xmm0
	jmp	__atan2f@PLT
	.size	__cargf, .-__cargf
	.weak	cargf32
	.set	cargf32,__cargf
	.weak	cargf
	.set	cargf,__cargf
