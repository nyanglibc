	.text
	.p2align 4,,15
	.globl	__getpayload
	.type	__getpayload, @function
__getpayload:
	movq	(%rdi), %rdx
	movabsq	$9218868437227405312, %rax
	movsd	.LC0(%rip), %xmm0
	movq	%rdx, %rcx
	andq	%rax, %rcx
	cmpq	%rax, %rcx
	je	.L7
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movabsq	$4503599627370495, %rax
	testq	%rax, %rdx
	je	.L1
	pxor	%xmm0, %xmm0
	movabsq	$2251799813685247, %rax
	andq	%rax, %rdx
	cvtsi2sdq	%rdx, %xmm0
	ret
	.size	__getpayload, .-__getpayload
	.weak	getpayloadf32x
	.set	getpayloadf32x,__getpayload
	.weak	getpayloadf64
	.set	getpayloadf64,__getpayload
	.weak	getpayload
	.set	getpayload,__getpayload
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
