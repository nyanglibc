	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __lgamma_compat,lgamma@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__lgamma_compat
	.type	__lgamma_compat, @function
__lgamma_compat:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	movsd	%xmm0, 8(%rsp)
	call	__ieee754_lgamma_r@PLT
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movsd	8(%rsp), %xmm2
	movl	(%rax), %eax
	cmpl	$3, %eax
	je	.L2
	movl	28(%rsp), %edx
	movq	__signgam@GOTPCREL(%rip), %rcx
	movl	%edx, (%rcx)
	movq	signgam@GOTPCREL(%rip), %rcx
	movl	%edx, (%rcx)
.L2:
	movq	.LC0(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm3
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	jb	.L22
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movapd	%xmm2, %xmm4
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm3
	jb	.L1
	cmpl	$-1, %eax
	je	.L1
	movapd	%xmm2, %xmm0
	movsd	.LC2(%rip), %xmm4
	movapd	%xmm2, %xmm3
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm4
	jbe	.L5
	cvttsd2siq	%xmm2, %rax
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm4
	andnpd	%xmm2, %xmm1
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm0, %xmm3
	cmpnlesd	%xmm2, %xmm3
	andpd	%xmm4, %xmm3
	subsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm3
	orpd	%xmm1, %xmm3
.L5:
	ucomisd	%xmm3, %xmm2
	jp	.L9
	jne	.L9
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	ucomisd	%xmm2, %xmm0
	setnb	%dil
	addl	$14, %edi
.L6:
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	call	__kernel_standard@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$14, %edi
	jmp	.L6
	.size	__lgamma_compat, .-__lgamma_compat
	.globl	__gamma
	.set	__gamma,__lgamma_compat
	.weak	gamma
	.set	gamma,__gamma
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	1127219200
	.align 8
.LC3:
	.long	0
	.long	1072693248
