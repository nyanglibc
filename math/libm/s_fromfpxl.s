	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpxl
	.type	__fromfpxl, @function
__fromfpxl:
	cmpl	$64, %esi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	32(%rsp), %r9
	movl	40(%rsp), %r10d
	ja	.L36
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$64, %esi
.L10:
	movq	%r9, %rdx
	movl	%r9d, %ebx
	movq	%r9, %rax
	shrq	$32, %rdx
	orl	%edx, %ebx
	je	.L37
	movswl	%r10w, %r9d
	movl	%r10d, %r8d
	shrl	$31, %r9d
	andl	$32767, %r8d
	leal	-2(%rsi,%r9), %r11d
	subl	$16383, %r8d
	cmpl	%r11d, %r8d
	jg	.L59
	salq	$32, %rdx
	movl	%eax, %eax
	orq	%rax, %rdx
	cmpl	$63, %r8d
	je	.L38
	cmpl	$-1, %r8d
	jl	.L39
	movl	$62, %ecx
	movl	$1, %eax
	movq	%rdx, %r12
	subl	%r8d, %ecx
	salq	%cl, %rax
	andq	%rax, %r12
	setne	%bl
	subq	$1, %rax
	andq	%rdx, %rax
	setne	%bpl
	cmpl	$-1, %r8d
	je	.L40
	movl	$63, %ecx
	subl	%r8d, %ecx
	shrq	%cl, %rdx
.L16:
	cmpl	$1, %edi
	je	.L18
	jle	.L61
	cmpl	$3, %edi
	je	.L21
	cmpl	$4, %edi
	jne	.L17
	testq	%r12, %r12
	je	.L41
	movq	%rdx, %rcx
	movl	$1, %ebx
	andl	$1, %ecx
	orq	%rax, %rcx
	setne	%al
	movzbl	%al, %eax
.L26:
	addq	%rax, %rdx
.L17:
	testw	%r10w, %r10w
	jns	.L24
.L23:
	cmpl	%r11d, %r8d
	je	.L27
	testb	%bpl, %bpl
	jne	.L28
	testb	%bl, %bl
	jne	.L28
.L29:
	movq	%rdx, %rax
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
.L9:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%ebp, %ebp
.L15:
	cmpl	$1, %edi
	je	.L44
	jle	.L62
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	cmpl	$3, %edi
	je	.L26
	cmpl	$4, %edi
	je	.L26
.L42:
	xorl	%ebx, %ebx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L61:
	testl	%edi, %edi
	jne	.L17
.L20:
	testw	%r10w, %r10w
	js	.L23
	movl	%ebx, %eax
	orl	%ebp, %eax
	movzbl	%al, %eax
	addq	%rax, %rdx
.L24:
	leal	1(%r11), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%rdx, %rax
	sete	%al
.L30:
	testb	%al, %al
	jne	.L59
	testb	%bpl, %bpl
	jne	.L28
	testb	%bl, %bl
	je	.L32
.L28:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
.L32:
	testw	%r10w, %r10w
	movq	%rdx, %rax
	js	.L29
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	%r9d, %edi
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L62:
	xorl	%ebx, %ebx
	testl	%edi, %edi
	je	.L20
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%ebx, %ebx
.L18:
	testw	%r10w, %r10w
	jns	.L24
	movl	%ebx, %eax
	orl	%ebp, %eax
	movzbl	%al, %eax
	addq	%rax, %rdx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$1, %ebp
	xorl	%edx, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1, %eax
	movl	%r8d, %ecx
	salq	%cl, %rax
	cmpq	%rdx, %rax
	setne	%al
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%edx, %edx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	%bl, %eax
	jmp	.L26
.L41:
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	jmp	.L26
	.size	__fromfpxl, .-__fromfpxl
	.weak	fromfpxf64x
	.set	fromfpxf64x,__fromfpxl
	.weak	fromfpxl
	.set	fromfpxl,__fromfpxl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
