	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __exp10f_compat,exp10f@GLIBC_2.2.5
	.symver __pow10f,pow10f@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__exp10f_compat
	.type	__exp10f_compat, @function
__exp10f_compat:
	subq	$24, %rsp
	movss	%xmm0, 12(%rsp)
	call	__exp10f@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm4
	movss	12(%rsp), %xmm2
	ucomiss	%xmm4, %xmm3
	jb	.L2
	ucomiss	.LC2(%rip), %xmm0
	jp	.L1
	je	.L2
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movd	%xmm2, %eax
	xorl	%edi, %edi
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	testl	%eax, %eax
	sets	%dil
	addq	$24, %rsp
	addl	$146, %edi
	jmp	__kernel_standard_f@PLT
	.size	__exp10f_compat, .-__exp10f_compat
	.globl	__pow10f
	.set	__pow10f,__exp10f_compat
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	0
