	.text
	.p2align 4,,15
	.globl	__setpayloadf
	.type	__setpayloadf, @function
__setpayloadf:
	movd	%xmm0, %edx
	movd	%xmm0, %eax
	shrl	$23, %edx
	cmpl	$148, %edx
	ja	.L2
	cmpl	$126, %edx
	jg	.L8
	testl	%eax, %eax
	je	.L8
.L2:
	movl	$0x00000000, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$150, %ecx
	subl	%edx, %ecx
	movl	$-1, %edx
	sall	%cl, %edx
	notl	%edx
	testl	%eax, %edx
	jne	.L2
	testl	%eax, %eax
	movl	.LC0(%rip), %edx
	je	.L6
	andl	$8388607, %eax
	orl	$8388608, %eax
	shrl	%cl, %eax
	orl	$2143289344, %eax
	movl	%eax, %edx
.L6:
	movl	%edx, (%rdi)
	xorl	%eax, %eax
	ret
	.size	__setpayloadf, .-__setpayloadf
	.weak	setpayloadf32
	.set	setpayloadf32,__setpayloadf
	.weak	setpayloadf
	.set	setpayloadf,__setpayloadf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
