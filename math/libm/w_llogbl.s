	.text
	.p2align 4,,15
	.globl	__llogbl
	.type	__llogbl, @function
__llogbl:
	pushq	%rbx
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__ieee754_ilogbl@PLT
	cmpl	$-2147483648, %eax
	popq	%rdx
	popq	%rcx
	je	.L4
	cmpl	$2147483647, %eax
	movslq	%eax, %rbx
	je	.L7
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movabsq	$-9223372036854775808, %rbx
.L2:
	movq	errno@gottpoff(%rip), %rax
	movl	$1, %edi
	movl	$33, %fs:(%rax)
	call	__feraiseexcept@PLT
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movabsq	$9223372036854775807, %rbx
	jmp	.L2
	.size	__llogbl, .-__llogbl
	.weak	llogbf64x
	.set	llogbf64x,__llogbl
	.weak	llogbl
	.set	llogbl,__llogbl
