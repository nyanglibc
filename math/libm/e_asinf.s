	.text
	.p2align 4,,15
	.globl	__ieee754_asinf
	.type	__ieee754_asinf, @function
__ieee754_asinf:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	cmpl	$1065353216, %eax
	je	.L16
	jg	.L17
	cmpl	$1056964607, %eax
	jg	.L5
	cmpl	$838860799, %eax
	movaps	%xmm0, %xmm2
	jg	.L6
	andps	.LC2(%rip), %xmm2
	movss	.LC3(%rip), %xmm1
	ucomiss	%xmm2, %xmm1
	jbe	.L7
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
.L7:
	movss	.LC4(%rip), %xmm3
	addss	%xmm0, %xmm3
	movss	.LC5(%rip), %xmm1
	ucomiss	%xmm1, %xmm3
	jbe	.L18
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L17:
	subss	%xmm0, %xmm0
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movss	.LC0(%rip), %xmm1
	mulss	%xmm0, %xmm1
	mulss	.LC1(%rip), %xmm0
	addss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	andps	.LC2(%rip), %xmm0
	cmpl	$1064933785, %eax
	movss	.LC5(%rip), %xmm1
	subss	%xmm0, %xmm1
	movss	.LC6(%rip), %xmm0
	mulss	.LC11(%rip), %xmm1
	mulss	%xmm1, %xmm0
	sqrtss	%xmm1, %xmm2
	addss	.LC7(%rip), %xmm0
	mulss	%xmm1, %xmm0
	addss	.LC8(%rip), %xmm0
	mulss	%xmm1, %xmm0
	addss	.LC9(%rip), %xmm0
	mulss	%xmm1, %xmm0
	addss	.LC10(%rip), %xmm0
	mulss	%xmm1, %xmm0
	jle	.L10
	mulss	%xmm2, %xmm0
	movss	.LC12(%rip), %xmm1
	addss	%xmm2, %xmm0
	addss	%xmm0, %xmm0
	addss	%xmm0, %xmm1
	movss	.LC0(%rip), %xmm0
	subss	%xmm1, %xmm0
.L11:
	testl	%edx, %edx
	jg	.L1
	xorps	.LC14(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	subss	%xmm2, %xmm1
	movss	.LC6(%rip), %xmm0
	mulss	.LC11(%rip), %xmm1
	mulss	%xmm1, %xmm0
	sqrtss	%xmm1, %xmm2
	addss	.LC7(%rip), %xmm0
	mulss	%xmm1, %xmm0
	addss	.LC8(%rip), %xmm0
	mulss	%xmm1, %xmm0
	addss	.LC9(%rip), %xmm0
	mulss	%xmm1, %xmm0
	addss	.LC10(%rip), %xmm0
	mulss	%xmm1, %xmm0
.L10:
	movd	%xmm2, %eax
	movaps	%xmm2, %xmm3
	andl	$-4096, %eax
	addss	%xmm2, %xmm3
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm4
	mulss	%xmm0, %xmm3
	movaps	%xmm4, %xmm0
	addss	%xmm4, %xmm2
	mulss	%xmm4, %xmm0
	addss	%xmm4, %xmm4
	subss	%xmm0, %xmm1
	movss	.LC1(%rip), %xmm0
	divss	%xmm2, %xmm1
	addss	%xmm1, %xmm1
	subss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	movss	.LC13(%rip), %xmm0
	movaps	%xmm0, %xmm2
	subss	%xmm4, %xmm2
	movaps	%xmm3, %xmm1
	subss	%xmm2, %xmm1
	subss	%xmm1, %xmm0
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L6:
	mulss	%xmm0, %xmm2
	movss	.LC6(%rip), %xmm1
	mulss	%xmm2, %xmm1
	addss	.LC7(%rip), %xmm1
	mulss	%xmm2, %xmm1
	addss	.LC8(%rip), %xmm1
	mulss	%xmm2, %xmm1
	addss	.LC9(%rip), %xmm1
	mulss	%xmm2, %xmm1
	addss	.LC10(%rip), %xmm1
	mulss	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm1, %xmm0
	ret
	.size	__ieee754_asinf, .-__ieee754_asinf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1070141403
	.align 4
.LC1:
	.long	3007036718
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	8388608
	.align 4
.LC4:
	.long	1900671690
	.align 4
.LC5:
	.long	1065353216
	.align 4
.LC6:
	.long	1026340500
	.align 4
.LC7:
	.long	1019614238
	.align 4
.LC8:
	.long	1027227429
	.align 4
.LC9:
	.long	1033470194
	.align 4
.LC10:
	.long	1042983652
	.align 4
.LC11:
	.long	1056964608
	.align 4
.LC12:
	.long	859553070
	.align 4
.LC13:
	.long	1061752795
	.section	.rodata.cst16
	.align 16
.LC14:
	.long	2147483648
	.long	0
	.long	0
	.long	0
