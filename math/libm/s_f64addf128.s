	.text
	.globl	__eqtf2
	.globl	__addtf3
	.globl	__trunctfdf2
	.globl	__unordtf2
	.globl	__gttf2
	.p2align 4,,15
	.globl	__f64addf128
	.type	__f64addf128, @function
__f64addf128:
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm5
	subq	$88, %rsp
	movdqa	.LC0(%rip), %xmm4
	pxor	%xmm1, %xmm4
	movaps	%xmm1, 16(%rsp)
	movdqa	%xmm5, %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm5, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L16
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	call	__trunctfdf2@PLT
	movapd	%xmm0, %xmm3
	andpd	.LC1(%rip), %xmm0
	movsd	.LC2(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L1
.L4:
	ucomisd	%xmm3, %xmm3
	movsd	%xmm3, 32(%rsp)
	jp	.L21
	movdqa	(%rsp), %xmm2
	pand	.LC3(%rip), %xmm2
	movdqa	.LC4(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movsd	32(%rsp), %xmm3
	jne	.L1
	movdqa	(%rsp), %xmm2
	movdqa	.LC4(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movsd	32(%rsp), %xmm3
	jg	.L1
	movdqa	16(%rsp), %xmm2
	pand	.LC3(%rip), %xmm2
	movsd	%xmm3, (%rsp)
	movdqa	.LC4(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm3
	jne	.L1
	movdqa	16(%rsp), %xmm2
	movdqa	.LC4(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movsd	(%rsp), %xmm3
	jg	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$88, %rsp
	movapd	%xmm3, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebp
	movl	%ebp, %eax
	andl	$-32704, %eax
	orl	$32640, %eax
	movl	%eax, 76(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movdqa	16(%rsp), %xmm0
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %eax
	orl	%ebp, %eax
	movl	%eax, 76(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %ebp
	notl	%ebp
	testl	%edi, %ebp
	jne	.L22
.L6:
	shrl	$5, %ebx
	movq	%xmm0, %rax
	movabsq	$-4294967296, %rdx
	andl	$1, %ebx
	orl	32(%rsp), %ebx
	andq	%rdx, %rax
	orq	%rbx, %rax
	movq	%rax, 32(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__trunctfdf2@PLT
	movapd	%xmm0, %xmm3
	andpd	.LC1(%rip), %xmm0
	movsd	.LC2(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jb	.L4
	ucomisd	.LC5(%rip), %xmm3
	jp	.L1
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	movsd	32(%rsp), %xmm3
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movaps	%xmm0, 48(%rsp)
	call	__feraiseexcept@PLT
	movdqa	48(%rsp), %xmm0
	jmp	.L6
	.size	__f64addf128, .-__f64addf128
	.weak	f32xaddf128
	.set	f32xaddf128,__f64addf128
	.weak	f64addf128
	.set	f64addf128,__f64addf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	0
