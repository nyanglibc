	.text
	.p2align 4,,15
	.globl	__ieee754_log10f
	.type	__ieee754_log10f, @function
__ieee754_log10f:
	movd	%xmm0, %edx
	cmpl	$8388607, %edx
	jg	.L7
	testl	$2147483647, %edx
	je	.L12
	testl	%edx, %edx
	js	.L13
	mulss	.LC2(%rip), %xmm0
	movl	$-25, %eax
	movd	%xmm0, %edx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
.L2:
	cmpl	$2139095039, %edx
	jg	.L14
	movl	%edx, %ecx
	pxor	%xmm2, %xmm2
	sarl	$23, %ecx
	andl	$8388607, %edx
	subq	$24, %rsp
	leal	-127(%rax,%rcx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%ecx, %eax
	cvtsi2ss	%eax, %xmm2
	movl	$127, %eax
	subl	%ecx, %eax
	sall	$23, %eax
	orl	%eax, %edx
	movl	%edx, 8(%rsp)
	movss	8(%rsp), %xmm0
	movss	%xmm2, 12(%rsp)
	call	__ieee754_logf@PLT
	mulss	.LC4(%rip), %xmm0
	movss	12(%rsp), %xmm2
	movss	.LC3(%rip), %xmm1
	mulss	%xmm2, %xmm1
	mulss	.LC5(%rip), %xmm2
	addq	$24, %rsp
	addss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movaps	%xmm0, %xmm1
	movss	.LC1(%rip), %xmm0
	andps	.LC0(%rip), %xmm1
	divss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	subss	%xmm0, %xmm0
	divss	%xmm0, %xmm0
	ret
	.size	__ieee754_log10f, .-__ieee754_log10f
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	3422552064
	.align 4
.LC2:
	.long	1275068416
	.align 4
.LC3:
	.long	894707675
	.align 4
.LC4:
	.long	1054759897
	.align 4
.LC5:
	.long	1050288256
