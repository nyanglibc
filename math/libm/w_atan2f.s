	.text
	.p2align 4,,15
	.globl	__atan2f
	.type	__atan2f, @function
__atan2f:
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_atan2f@PLT
	pxor	%xmm1, %xmm1
	movl	$0, %edx
	ucomiss	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	movss	8(%rsp), %xmm2
	movl	$1, %edx
	ucomiss	%xmm1, %xmm2
	movss	12(%rsp), %xmm3
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L11
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	andps	.LC1(%rip), %xmm3
	movss	.LC2(%rip), %xmm1
	ucomiss	%xmm3, %xmm1
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__atan2f, .-__atan2f
	.weak	atan2f32
	.set	atan2f32,__atan2f
	.weak	atan2f
	.set	atan2f,__atan2f
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095039
