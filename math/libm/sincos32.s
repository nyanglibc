	.text
	.p2align 4,,15
	.globl	__c32
	.type	__c32, @function
__c32:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %ebx
	subq	$2744, %rsp
	movq	%rsi, 32(%rsp)
	leaq	2400(%rsp), %r13
	leaq	48(%rsp), %rsi
	movq	%rdx, 40(%rsp)
	movl	%ecx, %edx
	movq	%rsi, 24(%rsp)
	call	__cpy@PLT
	xorl	%eax, %eax
	subl	$1, 48(%rsp)
	movl	$41, %ecx
	movq	%r13, %rdi
	testl	%ebx, %ebx
	rep stosq
	movl	$1, 2400(%rsp)
	movq	$1, 2408(%rsp)
	jle	.L2
	leal	-1(%rbx), %edx
	leaq	16(%r13), %rax
	leaq	24(%r13,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L3:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L3
.L2:
	leaq	1392(%rsp), %rax
	movq	24(%rsp), %rdi
	movl	%ebx, %edx
	leaq	1728(%rsp), %r12
	leaq	2064(%rsp), %rbp
	leaq	1056(%rsp), %r15
	movq	%rax, %rsi
	movq	%rax, (%rsp)
	movl	$12, %r14d
	call	__sqr@PLT
	leaq	oofac27(%rip), %rdi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	$27, 2416(%rsp)
	call	__mul@PLT
	movl	%ebx, %edx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__cpy@PLT
	movsd	.LC0(%rip), %xmm1
	.p2align 4,,10
	.p2align 3
.L4:
	movapd	%xmm1, %xmm0
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movsd	%xmm1, 8(%rsp)
	subsd	.LC2(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, 2416(%rsp)
	call	__mul@PLT
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	__cpy@PLT
	movq	(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	call	__mul@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sub@PLT
	movsd	8(%rsp), %xmm1
	subl	$1, %r14d
	subsd	.LC3(%rip), %xmm1
	jne	.L4
	movq	(%rsp), %rdi
	leaq	384(%rsp), %r14
	movl	%ebx, %ecx
	movq	%rbp, %rsi
	movq	%r14, %rdx
	call	__mul@PLT
	xorl	%eax, %eax
	movl	$41, %ecx
	movq	%r13, %rdi
	testl	%ebx, %ebx
	rep stosq
	movl	$1, 2400(%rsp)
	movq	$1, 2408(%rsp)
	jle	.L5
	leal	-1(%rbx), %edx
	leaq	16(%r13), %rax
	leaq	24(%r13,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L6:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L6
.L5:
	movq	(%rsp), %rsi
	movq	24(%rsp), %rdi
	movl	%ebx, %edx
	call	__sqr@PLT
	leaq	oofac27(%rip), %rdi
	movl	%ebx, %edx
	movq	%r12, %rsi
	call	__cpy@PLT
	movl	%ebx, %edx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__cpy@PLT
	movsd	.LC1(%rip), %xmm1
	movl	$13, 20(%rsp)
	.p2align 4,,10
	.p2align 3
.L7:
	movapd	%xmm1, %xmm0
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	subsd	.LC2(%rip), %xmm0
	movsd	%xmm1, 8(%rsp)
	mulsd	%xmm1, %xmm0
	cvttsd2siq	%xmm0, %rax
	movq	%rax, 2416(%rsp)
	call	__mul@PLT
	movl	%ebx, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	__cpy@PLT
	movq	(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	call	__mul@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sub@PLT
	subl	$1, 20(%rsp)
	movsd	8(%rsp), %xmm1
	subsd	.LC3(%rip), %xmm1
	jne	.L7
	leaq	720(%rsp), %r15
	movq	24(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%rbp, %rsi
	movq	%r15, %rdx
	call	__mul@PLT
	movl	$24, (%rsp)
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__mul@PLT
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	__sub@PLT
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__add@PLT
	leaq	__mptwo(%rip), %rdi
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r14, %rsi
	call	__sub@PLT
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	__mul@PLT
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r13, %rdi
	call	__add@PLT
	subl	$1, (%rsp)
	jne	.L8
	movq	32(%rsp), %rdx
	leaq	__mpone(%rip), %rdi
	movl	%ebx, %ecx
	movq	%r14, %rsi
	call	__sub@PLT
	movq	40(%rsp), %rsi
	movl	%ebx, %edx
	movq	%r15, %rdi
	call	__cpy@PLT
	addq	$2744, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__c32, .-__c32
	.p2align 4,,15
	.globl	__mpranred
	.type	__mpranred, @function
__mpranred:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movapd	%xmm0, %xmm2
	movq	%rdi, %rbx
	subq	$1024, %rsp
	movsd	%xmm0, (%rsp)
	andpd	.LC4(%rip), %xmm0
	movsd	.LC5(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.L39
	leaq	16(%rsp), %r13
	movsd	(%rsp), %xmm0
	leaq	352(%rsp), %r12
	movq	%r13, %rdi
	call	__dbl_mp@PLT
	movl	16(%rsp), %edx
	movl	$0, %eax
	movq	$1, 24(%rsp)
	movq	$1, 360(%rsp)
	subl	$5, %edx
	cmovs	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	testl	%ebp, %ebp
	movl	%eax, 352(%rsp)
	jle	.L20
	leaq	352(%rsp), %r12
	leal	-1(%rbp), %eax
	movslq	%edx, %rdx
	leaq	toverp(%rip), %rdi
	salq	$3, %rdx
	leaq	16(%r12), %rsi
	leaq	8(,%rax,8), %r8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	(%rdi,%rax), %rcx
	cvttsd2siq	(%rcx,%rdx), %rcx
	movq	%rcx, (%rsi,%rax)
	addq	$8, %rax
	cmpq	%r8, %rax
	jne	.L21
.L20:
	leaq	688(%rsp), %r14
	movq	%r13, %rdi
	movl	%ebp, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdx
	call	__mul@PLT
	movslq	688(%rsp), %rax
	pxor	%xmm0, %xmm0
	movl	%ebp, %edx
	subl	%eax, %edx
	movq	%rax, %rdi
	cvtsi2sdq	696(%rsp,%rax,8), %xmm0
	testl	%edx, %edx
	jle	.L22
	subl	$1, %edx
	leal	1(%rdi), %ecx
	leaq	16(%r14), %rax
	leaq	24(%r14,%rdx,8), %rsi
	movslq	%ecx, %rcx
	.p2align 4,,10
	.p2align 3
.L23:
	movq	-8(%rax,%rcx,8), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	cmpq	%rsi, %rax
	jne	.L23
.L22:
	leal	1(%rbp), %edx
	subl	%edi, %edx
	cmpl	%edx, %ebp
	jl	.L24
	movl	%ebp, %edi
	movslq	%edx, %rcx
	subl	%edx, %edi
	leaq	8(%r14,%rcx,8), %rax
	movq	%rdi, %rdx
	addq	%rcx, %rdx
	leaq	16(%r14,%rdx,8), %rdx
	.p2align 4,,10
	.p2align 3
.L25:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rdx, %rax
	jne	.L25
.L24:
	cmpq	$8388607, 704(%rsp)
	movl	$0, 688(%rsp)
	jg	.L40
	leaq	hp(%rip), %rsi
	movl	%ebp, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)
	call	__mul@PLT
	movsd	8(%rsp), %xmm0
.L27:
	cvttsd2si	%xmm0, %r12d
	pxor	%xmm0, %xmm0
	ucomisd	(%rsp), %xmm0
	jbe	.L28
	negq	8(%rbx)
	negl	%r12d
.L28:
	andl	$3, %r12d
.L16:
	addq	$1024, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	addsd	.LC2(%rip), %xmm0
	leaq	__mpone(%rip), %rsi
	movl	%ebp, %ecx
	movq	%r12, %rdx
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)
	call	__sub@PLT
	leaq	hp(%rip), %rsi
	movl	%ebp, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	__mul@PLT
	movsd	8(%rsp), %xmm0
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L39:
	movsd	hpinv(%rip), %xmm3
	leaq	16(%rsp), %r13
	movsd	toint(%rip), %xmm1
	leaq	352(%rsp), %r14
	mulsd	%xmm2, %xmm3
	movq	%r13, %rdi
	movapd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	movq	%xmm0, %r12
	subsd	%xmm1, %xmm0
	andl	$3, %r12d
	call	__dbl_mp@PLT
	leaq	hp(%rip), %rsi
	movl	%ebp, %ecx
	movq	%r14, %rdx
	movq	%r13, %rdi
	leaq	688(%rsp), %r13
	call	__mul@PLT
	movsd	(%rsp), %xmm0
	movl	%ebp, %esi
	movq	%r13, %rdi
	call	__dbl_mp@PLT
	movl	%ebp, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__sub@PLT
	jmp	.L16
	.size	__mpranred, .-__mpranred
	.p2align 4,,15
	.globl	__mpsin
	.type	__mpsin, @function
__mpsin:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1384, %rsp
	testb	%dil, %dil
	je	.L42
	leaq	32(%rsp), %r12
	leaq	1040(%rsp), %rbx
	leaq	704(%rsp), %r13
	movl	$32, %esi
	movq	%r12, %rdi
	call	__mpranred
	movl	$32, %ecx
	movl	%eax, %ebp
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__c32
	cmpl	$2, %ebp
	je	.L44
	cmpl	$3, %ebp
	je	.L45
	cmpl	$1, %ebp
	jne	.L43
	leaq	24(%rsp), %rsi
	movq	%r13, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	368(%rsp), %r12
	movl	$32, %esi
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	leaq	704(%rsp), %rbp
	leaq	32(%rsp), %r13
	movq	%r12, %rdi
	call	__dbl_mp@PLT
	movsd	8(%rsp), %xmm1
	movl	$32, %esi
	movq	%rbp, %rdi
	movapd	%xmm1, %xmm0
	call	__dbl_mp@PLT
	movl	$32, %ecx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__add@PLT
	movsd	(%rsp), %xmm2
	ucomisd	.LC7(%rip), %xmm2
	ja	.L53
	leaq	1040(%rsp), %rbx
	movl	$32, %ecx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	call	__c32
.L43:
	leaq	24(%rsp), %rsi
	movq	%rbx, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	hp(%rip), %rdi
	leaq	1040(%rsp), %rbx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	$32, %ecx
	call	__sub@PLT
	movl	$32, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__c32
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	24(%rsp), %rsi
	movq	%rbx, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	xorpd	.LC8(%rip), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	leaq	24(%rsp), %rsi
	movq	%r13, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	xorpd	.LC8(%rip), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__mpsin, .-__mpsin
	.p2align 4,,15
	.globl	__mpcos
	.type	__mpcos, @function
__mpcos:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1384, %rsp
	testb	%dil, %dil
	je	.L55
	leaq	32(%rsp), %r12
	leaq	1040(%rsp), %r13
	leaq	704(%rsp), %rbx
	movl	$32, %esi
	movq	%r12, %rdi
	call	__mpranred
	movl	$32, %ecx
	movl	%eax, %ebp
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__c32
	cmpl	$2, %ebp
	je	.L57
	cmpl	$3, %ebp
	je	.L58
	cmpl	$1, %ebp
	jne	.L56
	leaq	24(%rsp), %rsi
	movq	%r13, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	xorpd	.LC8(%rip), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	368(%rsp), %rbp
	movl	$32, %esi
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	leaq	704(%rsp), %rbx
	leaq	32(%rsp), %r12
	movq	%rbp, %rdi
	call	__dbl_mp@PLT
	movsd	8(%rsp), %xmm1
	movl	$32, %esi
	movq	%rbx, %rdi
	movapd	%xmm1, %xmm0
	call	__dbl_mp@PLT
	movl	$32, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__add@PLT
	movsd	(%rsp), %xmm2
	ucomisd	.LC7(%rip), %xmm2
	ja	.L66
	leaq	1040(%rsp), %rdx
	movl	$32, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__c32
.L56:
	leaq	24(%rsp), %rsi
	movq	%rbx, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	hp(%rip), %rdi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movl	$32, %ecx
	call	__sub@PLT
	leaq	1040(%rsp), %rsi
	movl	$32, %ecx
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	__c32
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	24(%rsp), %rsi
	movq	%rbx, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	xorpd	.LC8(%rip), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	24(%rsp), %rsi
	movq	%r13, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	24(%rsp), %xmm0
	addq	$1384, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__mpcos, .-__mpcos
	.section	.rodata
	.align 32
	.type	toverp, @object
	.size	toverp, 600
toverp:
	.long	1610612736
	.long	1097097008
	.long	0
	.long	1096520593
	.long	0
	.long	1094003196
	.long	2147483648
	.long	1094953960
	.long	2684354560
	.long	1097770651
	.long	1073741824
	.long	1097341804
	.long	2147483648
	.long	1096987431
	.long	1073741824
	.long	1095820304
	.long	1610612736
	.long	1097845292
	.long	1610612736
	.long	1097169879
	.long	3758096384
	.long	1097378870
	.long	0
	.long	1094858525
	.long	2147483648
	.long	1095799668
	.long	536870912
	.long	1097597129
	.long	2147483648
	.long	1095202052
	.long	2147483648
	.long	1097478723
	.long	1610612736
	.long	1097843645
	.long	0
	.long	1094496553
	.long	0
	.long	1097131997
	.long	2684354560
	.long	1096828606
	.long	0
	.long	1095196066
	.long	2147483648
	.long	1096850739
	.long	0
	.long	1096550829
	.long	1073741824
	.long	1096277904
	.long	0
	.long	1095551211
	.long	2147483648
	.long	1095549353
	.long	0
	.long	1095563854
	.long	1610612736
	.long	1096846321
	.long	0
	.long	1097318181
	.long	0
	.long	1095602172
	.long	3221225472
	.long	1097007099
	.long	0
	.long	1091985468
	.long	536870912
	.long	1097721314
	.long	1073741824
	.long	1096903489
	.long	1073741824
	.long	1096501211
	.long	2147483648
	.long	1095450471
	.long	2147483648
	.long	1094968708
	.long	3221225472
	.long	1097263592
	.long	0
	.long	1095742287
	.long	1073741824
	.long	1096284811
	.long	2147483648
	.long	1096632814
	.long	2684354560
	.long	1097399676
	.long	2684354560
	.long	1097740135
	.long	0
	.long	1092413404
	.long	1073741824
	.long	1096895058
	.long	1610612736
	.long	1097682303
	.long	3221225472
	.long	1096281159
	.long	0
	.long	1096919969
	.long	0
	.long	1096122572
	.long	3221225472
	.long	1095876382
	.long	0
	.long	1096477436
	.long	0
	.long	1097463684
	.long	3221225472
	.long	1097031302
	.long	0
	.long	1094560227
	.long	3221225472
	.long	1096952875
	.long	0
	.long	1097646945
	.long	1073741824
	.long	1096377953
	.long	0
	.long	1096271144
	.long	1073741824
	.long	1096421411
	.long	0
	.long	1097857808
	.long	3221225472
	.long	1095982281
	.long	0
	.long	1095271171
	.long	0
	.long	1094014666
	.long	1073741824
	.long	1096608306
	.long	3221225472
	.long	1096300702
	.long	1610612736
	.long	1097339277
	.long	1073741824
	.long	1095889158
	.long	2684354560
	.long	1097362681
	.long	536870912
	.long	1097571585
	.long	2147483648
	.long	1095057836
	.long	1610612736
	.long	1097371345
	.long	3221225472
	.long	1096991636
	.long	2147483648
	.long	1097577960
	.long	536870912
	.long	1097476835
	.long	0
	.long	1091893908
	.align 32
	.type	hp, @object
	.size	hp, 328
hp:
	.long	1
	.zero	4
	.quad	1
	.quad	1
	.quad	9576373
	.quad	4473553
	.quad	8677769
	.quad	9225495
	.quad	112697
	.quad	10637828
	.quad	10227988
	.quad	13605096
	.quad	268157
	.quad	5010983
	.quad	3556514
	.quad	9703667
	.quad	1861641
	.quad	12312362
	.quad	3368858
	.quad	7636534
	.quad	6313492
	.quad	14410942
	.quad	2649759
	.quad	12741338
	.quad	14328708
	.quad	9160971
	.quad	7007428
	.quad	12385677
	.quad	15243397
	.quad	13847663
	.quad	14341655
	.quad	16693613
	.quad	15207791
	.quad	14408816
	.quad	14153397
	.quad	1261387
	.quad	6110792
	.quad	2291862
	.quad	4181138
	.quad	5295267
	.zero	16
	.align 32
	.type	oofac27, @object
	.size	oofac27, 328
oofac27:
	.long	-3
	.zero	4
	.quad	1
	.quad	7
	.quad	4631664
	.quad	12006312
	.quad	13118056
	.quad	6538613
	.quad	646354
	.quad	8508025
	.quad	9131256
	.quad	7548776
	.quad	2529842
	.quad	8864927
	.quad	660489
	.quad	15595125
	.quad	12777885
	.quad	11618489
	.quad	13348664
	.quad	5486686
	.quad	514518
	.quad	11275535
	.quad	4727621
	.quad	3575562
	.quad	13579710
	.quad	5829745
	.quad	7531862
	.quad	9507898
	.quad	6915060
	.quad	4079264
	.quad	1907586
	.quad	6078398
	.quad	13789314
	.quad	5504104
	.quad	14136
	.zero	56
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	toint, @object
	.size	toint, 8
toint:
	.long	0
	.long	1127743488
	.align 8
	.type	hpinv, @object
	.size	hpinv, 8
hpinv:
	.long	1841940611
	.long	1071931184
	.align 8
.LC0:
	.long	0
	.long	1077542912
	.align 8
.LC1:
	.long	0
	.long	1077608448
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	1073741824
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	2435842048
	.long	1123013906
	.align 8
.LC7:
	.long	2576980378
	.long	1072273817
	.section	.rodata.cst16
	.align 16
.LC8:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
