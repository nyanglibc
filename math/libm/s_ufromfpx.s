	.text
	.p2align 4,,15
	.globl	__ufromfpx
	.type	__ufromfpx, @function
__ufromfpx:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$64, %esi
	ja	.L30
	testl	%esi, %esi
	jne	.L2
.L19:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$64, %esi
.L2:
	movq	%xmm0, %r8
	movabsq	$9223372036854775807, %rcx
	xorl	%eax, %eax
	andq	%r8, %rcx
	je	.L1
	shrq	$52, %rcx
	testq	%r8, %r8
	leal	-1023(%rcx), %r9d
	js	.L4
	leal	-1(%rsi), %r10d
	cmpl	%r10d, %r9d
	jg	.L22
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdx
	andq	%r8, %rax
	orq	%rdx, %rax
	cmpl	$51, %r9d
	jle	.L7
	subl	$1075, %ecx
	xorl	%ebx, %ebx
	salq	%cl, %rax
.L8:
	cmpl	$1, %edi
	je	.L36
	jle	.L64
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	cmpl	$3, %edi
	je	.L18
	cmpl	$4, %edi
	je	.L18
.L34:
	xorl	%r11d, %r11d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	$63, %r9d
	jne	.L20
	testq	%rax, %rax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %edi
	movl	%esi, 12(%rsp)
	call	feraiseexcept@PLT
	movl	12(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	movq	$-1, %rax
	je	.L1
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r9d, %r9d
	jns	.L19
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdx
	movl	$-1, %r10d
	andq	%r8, %rax
	orq	%rdx, %rax
.L7:
	cmpl	$-1, %r9d
	jl	.L32
	movl	$51, %ecx
	movl	$1, %edx
	subl	%r9d, %ecx
	salq	%cl, %rdx
	movl	$52, %ecx
	movq	%rdx, %rbp
	andq	%rax, %rbp
	setne	%r11b
	subq	$1, %rdx
	andq	%rax, %rdx
	setne	%bl
	subl	%r9d, %ecx
	shrq	%cl, %rax
	cmpl	$1, %edi
	je	.L10
	jle	.L65
	cmpl	$3, %edi
	je	.L13
	cmpl	$4, %edi
	jne	.L9
	testq	%rbp, %rbp
	je	.L33
	movq	%rax, %rcx
	movl	$1, %r11d
	andl	$1, %ecx
	orq	%rdx, %rcx
	setne	%dl
	movzbl	%dl, %edx
.L18:
	addq	%rdx, %rax
.L9:
	testq	%r8, %r8
	jns	.L16
.L15:
	testq	%rax, %rax
	jne	.L19
.L20:
	testb	%r11b, %r11b
	jne	.L39
	testb	%bl, %bl
	je	.L1
.L39:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L65:
	testl	%edi, %edi
	jne	.L9
.L12:
	testq	%r8, %r8
	js	.L15
	movl	%r11d, %edx
	orl	%ebx, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$63, %r10d
	je	.L66
	leal	1(%r10), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L20
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%r11d, %r11d
	testl	%edi, %edi
	je	.L12
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%r11d, %r11d
.L10:
	testq	%r8, %r8
	jns	.L16
	movl	%r11d, %edx
	orl	%ebx, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1, %ebx
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	%r11b, %edx
	jmp	.L18
.L33:
	xorl	%edx, %edx
	xorl	%r11d, %r11d
	jmp	.L18
	.size	__ufromfpx, .-__ufromfpx
	.weak	ufromfpxf32x
	.set	ufromfpxf32x,__ufromfpx
	.weak	ufromfpxf64
	.set	ufromfpxf64,__ufromfpx
	.weak	ufromfpx
	.set	ufromfpx,__ufromfpx
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
