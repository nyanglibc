	.text
	.p2align 4,,15
	.globl	__remainderf
	.type	__remainderf, @function
__remainderf:
	movaps	%xmm0, %xmm2
	andps	.LC0(%rip), %xmm2
	ucomiss	.LC1(%rip), %xmm2
	seta	%dl
	ja	.L4
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L4
.L2:
	jmp	__ieee754_remainderf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ucomiss	%xmm1, %xmm0
	jp	.L2
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_remainderf@PLT
	.size	__remainderf, .-__remainderf
	.weak	dremf
	.set	dremf,__remainderf
	.weak	remainderf32
	.set	remainderf32,__remainderf
	.weak	remainderf
	.set	remainderf,__remainderf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
