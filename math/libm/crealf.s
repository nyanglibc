	.text
	.p2align 4,,15
	.globl	__crealf
	.type	__crealf, @function
__crealf:
	movq	%xmm0, -8(%rsp)
	movss	-8(%rsp), %xmm0
	ret
	.size	__crealf, .-__crealf
	.weak	crealf32
	.set	crealf32,__crealf
	.weak	crealf
	.set	crealf,__crealf
