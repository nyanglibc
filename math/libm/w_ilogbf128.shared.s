	.text
	.p2align 4,,15
	.globl	__ilogbf128
	.type	__ilogbf128, @function
__ilogbf128:
	pushq	%rbx
	call	__ieee754_ilogbf128@PLT
	movl	%eax, %ebx
	addl	$2147483647, %eax
	cmpl	$-3, %eax
	ja	.L5
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rax
	movl	$1, %edi
	movl	$33, %fs:(%rax)
	call	__GI___feraiseexcept
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	__ilogbf128, .-__ilogbf128
	.weak	ilogbf128
	.set	ilogbf128,__ilogbf128
