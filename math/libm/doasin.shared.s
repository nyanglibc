	.text
	.p2align 4,,15
	.globl	__doasin
	.type	__doasin, @function
__doasin:
	movapd	%xmm0, %xmm2
	movapd	%xmm0, %xmm13
	movsd	.LC7(%rip), %xmm7
	addsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm13
	movapd	%xmm0, %xmm6
	mulsd	%xmm7, %xmm3
	movapd	%xmm0, %xmm11
	movsd	cc4.4564(%rip), %xmm8
	mulsd	%xmm1, %xmm2
	subsd	%xmm3, %xmm6
	addsd	%xmm2, %xmm13
	movsd	.LC0(%rip), %xmm2
	addsd	%xmm3, %xmm6
	mulsd	%xmm13, %xmm2
	subsd	%xmm6, %xmm11
	movapd	%xmm6, %xmm4
	movapd	%xmm6, %xmm3
	addsd	.LC1(%rip), %xmm2
	mulsd	%xmm6, %xmm3
	mulsd	%xmm11, %xmm4
	movapd	%xmm11, %xmm10
	mulsd	%xmm11, %xmm10
	movapd	%xmm3, %xmm5
	mulsd	%xmm13, %xmm2
	addsd	%xmm4, %xmm4
	addsd	.LC2(%rip), %xmm2
	addsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm3
	mulsd	%xmm13, %xmm2
	addsd	%xmm4, %xmm3
	movapd	%xmm0, %xmm4
	addsd	.LC3(%rip), %xmm2
	mulsd	%xmm1, %xmm4
	addsd	%xmm10, %xmm3
	movapd	%xmm5, %xmm10
	addsd	%xmm4, %xmm4
	mulsd	%xmm13, %xmm2
	addsd	%xmm4, %xmm3
	movsd	c4.4563(%rip), %xmm4
	addsd	.LC4(%rip), %xmm2
	movapd	%xmm4, %xmm9
	addsd	%xmm3, %xmm10
	mulsd	%xmm13, %xmm2
	subsd	%xmm10, %xmm5
	addsd	.LC5(%rip), %xmm2
	addsd	%xmm3, %xmm5
	movapd	%xmm4, %xmm3
	movsd	%xmm5, -8(%rsp)
	mulsd	%xmm13, %xmm2
	movq	.LC8(%rip), %xmm5
	andpd	%xmm5, %xmm9
	addsd	.LC6(%rip), %xmm2
	mulsd	%xmm13, %xmm2
	movapd	%xmm2, %xmm12
	addsd	%xmm2, %xmm3
	andpd	%xmm5, %xmm12
	ucomisd	%xmm9, %xmm12
	jbe	.L22
	subsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm13
	addsd	%xmm4, %xmm13
	addsd	%xmm8, %xmm13
	addsd	.LC9(%rip), %xmm13
.L4:
	movapd	%xmm13, %xmm4
	movapd	%xmm10, %xmm8
	addsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm2
	movapd	%xmm4, %xmm9
	movapd	%xmm4, %xmm12
	mulsd	%xmm7, %xmm2
	subsd	%xmm4, %xmm3
	mulsd	-8(%rsp), %xmm4
	subsd	%xmm2, %xmm9
	addsd	%xmm2, %xmm9
	movapd	%xmm10, %xmm2
	mulsd	%xmm7, %xmm2
	subsd	%xmm9, %xmm12
	movapd	%xmm9, %xmm14
	subsd	%xmm2, %xmm8
	movapd	%xmm12, %xmm15
	addsd	%xmm8, %xmm2
	movapd	%xmm10, %xmm8
	subsd	%xmm2, %xmm8
	mulsd	%xmm2, %xmm15
	mulsd	%xmm2, %xmm14
	mulsd	%xmm8, %xmm9
	mulsd	%xmm8, %xmm12
	addsd	%xmm15, %xmm9
	movapd	%xmm14, %xmm15
	addsd	%xmm9, %xmm15
	subsd	%xmm15, %xmm14
	addsd	%xmm9, %xmm14
	movsd	c3.4561(%rip), %xmm9
	addsd	%xmm14, %xmm12
	movapd	%xmm3, %xmm14
	movapd	%xmm15, %xmm3
	addsd	%xmm13, %xmm14
	mulsd	%xmm10, %xmm14
	addsd	%xmm14, %xmm4
	movsd	cc3.4562(%rip), %xmm14
	addsd	%xmm12, %xmm4
	movapd	%xmm9, %xmm12
	andpd	%xmm5, %xmm12
	addsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm13
	subsd	%xmm3, %xmm15
	andpd	%xmm5, %xmm13
	ucomisd	%xmm12, %xmm13
	addsd	%xmm4, %xmm15
	movapd	%xmm9, %xmm4
	addsd	%xmm3, %xmm4
	jbe	.L23
	subsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm12
	addsd	%xmm9, %xmm12
	addsd	%xmm14, %xmm12
	addsd	%xmm15, %xmm12
.L7:
	movapd	%xmm12, %xmm3
	movapd	%xmm2, %xmm14
	addsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm13
	movapd	%xmm3, %xmm9
	movapd	%xmm3, %xmm15
	mulsd	%xmm7, %xmm13
	subsd	%xmm3, %xmm4
	mulsd	-8(%rsp), %xmm3
	subsd	%xmm13, %xmm9
	addsd	%xmm13, %xmm9
	movapd	%xmm2, %xmm13
	subsd	%xmm9, %xmm15
	mulsd	%xmm9, %xmm13
	mulsd	%xmm8, %xmm9
	mulsd	%xmm15, %xmm14
	mulsd	%xmm8, %xmm15
	addsd	%xmm14, %xmm9
	movapd	%xmm13, %xmm14
	addsd	%xmm9, %xmm14
	subsd	%xmm14, %xmm13
	addsd	%xmm9, %xmm13
	movapd	%xmm15, %xmm9
	movsd	cc2.4560(%rip), %xmm15
	addsd	%xmm13, %xmm9
	movapd	%xmm4, %xmm13
	movapd	%xmm14, %xmm4
	addsd	%xmm12, %xmm13
	movsd	c2.4559(%rip), %xmm12
	mulsd	%xmm10, %xmm13
	addsd	%xmm13, %xmm3
	addsd	%xmm9, %xmm3
	movapd	%xmm12, %xmm9
	andpd	%xmm5, %xmm9
	addsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm13
	subsd	%xmm4, %xmm14
	andpd	%xmm5, %xmm13
	ucomisd	%xmm9, %xmm13
	addsd	%xmm3, %xmm14
	movapd	%xmm12, %xmm3
	addsd	%xmm4, %xmm3
	jbe	.L24
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm9
	addsd	%xmm12, %xmm9
	addsd	%xmm15, %xmm9
	addsd	%xmm14, %xmm9
.L10:
	movapd	%xmm9, %xmm14
	movapd	%xmm2, %xmm13
	addsd	%xmm3, %xmm14
	movapd	%xmm14, %xmm12
	movapd	%xmm14, %xmm4
	movapd	%xmm14, %xmm15
	mulsd	%xmm7, %xmm12
	subsd	%xmm14, %xmm3
	mulsd	-8(%rsp), %xmm14
	subsd	%xmm12, %xmm4
	addsd	%xmm12, %xmm4
	movapd	%xmm2, %xmm12
	subsd	%xmm4, %xmm15
	mulsd	%xmm4, %xmm12
	mulsd	%xmm8, %xmm4
	mulsd	%xmm15, %xmm13
	mulsd	%xmm8, %xmm15
	addsd	%xmm13, %xmm4
	movapd	%xmm12, %xmm13
	addsd	%xmm4, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm4, %xmm12
	addsd	%xmm12, %xmm15
	movapd	%xmm3, %xmm12
	addsd	%xmm9, %xmm12
	movsd	c1.4557(%rip), %xmm9
	movapd	%xmm9, %xmm4
	movapd	%xmm9, %xmm3
	mulsd	%xmm10, %xmm12
	andpd	%xmm5, %xmm4
	addsd	%xmm12, %xmm14
	addsd	%xmm15, %xmm14
	movapd	%xmm13, %xmm15
	addsd	%xmm14, %xmm15
	movapd	%xmm15, %xmm12
	subsd	%xmm15, %xmm13
	andpd	%xmm5, %xmm12
	addsd	%xmm15, %xmm3
	ucomisd	%xmm4, %xmm12
	addsd	%xmm14, %xmm13
	movsd	cc1.4558(%rip), %xmm14
	jbe	.L25
	subsd	%xmm3, %xmm15
	addsd	%xmm15, %xmm9
	addsd	%xmm9, %xmm14
	addsd	%xmm14, %xmm13
.L13:
	movapd	%xmm13, %xmm12
	addsd	%xmm3, %xmm12
	movapd	%xmm12, %xmm9
	movapd	%xmm12, %xmm4
	movapd	%xmm12, %xmm14
	mulsd	%xmm7, %xmm9
	subsd	%xmm12, %xmm3
	mulsd	-8(%rsp), %xmm12
	subsd	%xmm9, %xmm4
	addsd	%xmm3, %xmm13
	addsd	%xmm9, %xmm4
	movapd	%xmm2, %xmm9
	subsd	%xmm4, %xmm14
	mulsd	%xmm4, %xmm9
	mulsd	%xmm8, %xmm4
	mulsd	%xmm14, %xmm2
	mulsd	%xmm14, %xmm8
	addsd	%xmm4, %xmm2
	movapd	%xmm9, %xmm4
	addsd	%xmm2, %xmm4
	subsd	%xmm4, %xmm9
	movapd	%xmm4, %xmm3
	addsd	%xmm9, %xmm2
	addsd	%xmm8, %xmm2
	movapd	%xmm13, %xmm8
	mulsd	%xmm10, %xmm8
	addsd	%xmm12, %xmm8
	addsd	%xmm2, %xmm8
	addsd	%xmm8, %xmm3
	mulsd	%xmm3, %xmm7
	movapd	%xmm3, %xmm2
	movapd	%xmm3, %xmm10
	subsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm3
	subsd	%xmm7, %xmm2
	addsd	%xmm8, %xmm4
	addsd	%xmm2, %xmm7
	movapd	%xmm6, %xmm2
	subsd	%xmm7, %xmm10
	mulsd	%xmm7, %xmm2
	mulsd	%xmm11, %xmm7
	mulsd	%xmm10, %xmm6
	movapd	%xmm2, %xmm9
	mulsd	%xmm10, %xmm11
	addsd	%xmm6, %xmm7
	addsd	%xmm7, %xmm9
	subsd	%xmm9, %xmm2
	addsd	%xmm2, %xmm7
	movapd	%xmm0, %xmm2
	addsd	%xmm11, %xmm7
	movapd	%xmm4, %xmm11
	mulsd	%xmm0, %xmm11
	addsd	%xmm3, %xmm11
	movapd	%xmm9, %xmm3
	addsd	%xmm7, %xmm11
	addsd	%xmm11, %xmm3
	movapd	%xmm3, %xmm4
	subsd	%xmm3, %xmm9
	andpd	%xmm5, %xmm4
	addsd	%xmm3, %xmm2
	andpd	%xmm0, %xmm5
	addsd	%xmm9, %xmm11
	ucomisd	%xmm5, %xmm4
	ja	.L28
	subsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	addsd	%xmm11, %xmm0
	addsd	%xmm0, %xmm1
.L16:
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm2
	movsd	%xmm0, (%rdi)
	addsd	%xmm2, %xmm1
	movsd	%xmm1, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	subsd	%xmm3, %xmm4
	movapd	%xmm4, %xmm13
	addsd	%xmm2, %xmm13
	addsd	.LC9(%rip), %xmm13
	addsd	%xmm8, %xmm13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L25:
	subsd	%xmm3, %xmm9
	addsd	%xmm9, %xmm15
	addsd	%xmm15, %xmm13
	addsd	%xmm14, %xmm13
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L24:
	subsd	%xmm3, %xmm12
	movapd	%xmm12, %xmm9
	addsd	%xmm4, %xmm9
	addsd	%xmm14, %xmm9
	addsd	%xmm15, %xmm9
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	movapd	%xmm9, %xmm12
	subsd	%xmm4, %xmm12
	addsd	%xmm3, %xmm12
	addsd	%xmm15, %xmm12
	addsd	%xmm14, %xmm12
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L28:
	subsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm1
	addsd	%xmm11, %xmm1
	jmp	.L16
	.size	__doasin, .-__doasin
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	cc1.4558, @object
	.size	cc1.4558, 8
cc1.4558:
	.long	1433883529
	.long	1013273941
	.align 8
	.type	c1.4557, @object
	.size	c1.4557, 8
c1.4557:
	.long	1431655765
	.long	1069897045
	.align 8
	.type	cc2.4560, @object
	.size	cc2.4560, 8
cc2.4560:
	.long	1676779797
	.long	1011456403
	.align 8
	.type	c2.4559, @object
	.size	c2.4559, 8
c2.4559:
	.long	858993459
	.long	1068708659
	.align 8
	.type	cc3.4562, @object
	.size	cc3.4562, 8
cc3.4562:
	.long	1029501125
	.long	-1137569856
	.align 8
	.type	c3.4561, @object
	.size	c3.4561, 8
c3.4561:
	.long	-1227133513
	.long	1067899757
	.align 8
	.type	cc4.4564, @object
	.size	cc4.4564, 8
cc4.4564:
	.long	-14422754
	.long	-1140673984
	.align 8
	.type	c4.4563, @object
	.size	c4.4563, 8
c4.4563:
	.long	-954437179
	.long	1067392113
	.align 8
.LC0:
	.long	2771274701
	.long	1065371271
	.align 8
.LC1:
	.long	549299552
	.long	1065427220
	.align 8
.LC2:
	.long	2116180675
	.long	1065614905
	.align 8
.LC3:
	.long	864161770
	.long	1065855095
	.align 8
.LC4:
	.long	2631044726
	.long	1066178969
	.align 8
.LC5:
	.long	1321349667
	.long	1066517740
	.align 8
.LC6:
	.long	780903923
	.long	1066854586
	.align 8
.LC7:
	.long	33554432
	.long	1101004800
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC8:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC9:
	.long	0
	.long	0
