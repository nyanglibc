.globl __scalbnl
.type __scalbnl,@function
.align 1<<4
__scalbnl:
 movl %edi,-4(%rsp)
 fildl -4(%rsp)
 fldt 8(%rsp)
 fscale
 fstp %st(1)
 ret
.size __scalbnl,.-__scalbnl
