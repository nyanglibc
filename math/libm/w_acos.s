	.text
	.p2align 4,,15
	.globl	__acos
	.type	__acos, @function
__acos:
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	.LC1(%rip), %xmm1
	ja	.L4
	jmp	__ieee754_acos@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_acos@PLT
	.size	__acos, .-__acos
	.weak	acosf32x
	.set	acosf32x,__acos
	.weak	acosf64
	.set	acosf64,__acos
	.weak	acos
	.set	acos,__acos
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
