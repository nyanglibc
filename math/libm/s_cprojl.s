	.text
	.p2align 4,,15
	.globl	__cprojl
	.type	__cprojl, @function
__cprojl:
	fldt	24(%rsp)
	fldt	8(%rsp)
	fxam
	fnstsw	%ax
	fxch	%st(1)
	andb	$69, %ah
	cmpb	$5, %ah
	fxam
	fnstsw	%ax
	je	.L13
	movl	%eax, %edx
	andb	$69, %dh
	cmpb	$5, %dh
	jne	.L5
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L13:
	fstp	%st(0)
	fstp	%st(0)
.L2:
	testb	$2, %ah
	fldz
	jne	.L12
.L4:
	flds	.LC2(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	fldz
	fchs
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	fxch	%st(1)
	ret
	.size	__cprojl, .-__cprojl
	.weak	cprojf64x
	.set	cprojf64x,__cprojl
	.weak	cprojl
	.set	cprojl,__cprojl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095040
