	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__hypotl
	.type	__hypotl, @function
__hypotl:
	subq	$72, %rsp
	fldt	80(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fldt	96(%rsp)
	fld	%st(0)
	fstpt	48(%rsp)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__ieee754_hypotl@PLT
	fld	%st(0)
	addq	$32, %rsp
	fabs
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L7
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	fstp	%st(0)
	fstp	%st(0)
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fldt	(%rsp)
	fld	%st(0)
	fabs
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jb	.L8
	fldt	16(%rsp)
	fld	%st(0)
	fabs
	fxch	%st(3)
	fucomip	%st(3), %st
	fstp	%st(2)
	jb	.L9
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L10
	fstp	%st(2)
	fstpt	64(%rsp)
	movl	$204, %edi
	fstpt	48(%rsp)
	addq	$40, %rsp
	jmp	__kernel_standard_l@PLT
	.size	__hypotl, .-__hypotl
	.weak	hypotf64x
	.set	hypotf64x,__hypotl
	.weak	hypotl
	.set	hypotl,__hypotl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
