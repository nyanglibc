	.text
	.p2align 4,,15
	.globl	__fadd
	.type	__fadd, @function
__fadd:
	pushq	%rbp
	pushq	%rbx
	movapd	%xmm1, %xmm2
	subq	$40, %rsp
	xorpd	.LC0(%rip), %xmm2
	ucomisd	%xmm0, %xmm2
	jp	.L2
	jne	.L2
	movapd	%xmm1, %xmm2
	movss	.LC2(%rip), %xmm4
	addsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movaps	%xmm2, %xmm3
	andps	.LC1(%rip), %xmm3
	ucomiss	%xmm3, %xmm4
	jnb	.L1
.L4:
	ucomiss	%xmm2, %xmm2
	jp	.L20
	movq	.LC3(%rip), %xmm4
	movsd	.LC4(%rip), %xmm3
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	addq	$40, %rsp
	movaps	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %eax
	movl	%eax, %edx
	andl	$-32704, %edx
	orl	$32640, %edx
	movl	%edx, 28(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rbp
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %edx
	orl	%eax, %edx
	movl	%edx, 28(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %eax
	notl	%eax
	testl	%edi, %eax
	jne	.L21
.L6:
	shrl	$5, %ebx
	movabsq	$-4294967296, %rax
	andl	$1, %ebx
	pxor	%xmm2, %xmm2
	orl	%ebp, %ebx
	andq	%rax, %rbp
	orq	%rbx, %rbp
	movss	.LC2(%rip), %xmm4
	movq	%rbp, (%rsp)
	cvtsd2ss	(%rsp), %xmm2
	movaps	%xmm2, %xmm3
	andps	.LC1(%rip), %xmm3
	ucomiss	%xmm3, %xmm4
	jb	.L4
	ucomiss	.LC5(%rip), %xmm2
	jp	.L1
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	ucomisd	%xmm0, %xmm1
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__feraiseexcept@PLT
	movsd	8(%rsp), %xmm1
	movsd	(%rsp), %xmm0
	jmp	.L6
	.size	__fadd, .-__fadd
	.weak	f32addf32x
	.set	f32addf32x,__fadd
	.weak	f32addf64
	.set	f32addf64,__fadd
	.weak	fadd
	.set	fadd,__fadd
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC3:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	0
