	.text
	.p2align 4,,15
	.globl	__ufromfpxf
	.type	__ufromfpxf, @function
__ufromfpxf:
	pushq	%rbp
	pushq	%rbx
	movd	%xmm0, %edx
	subq	$24, %rsp
	cmpl	$64, %esi
	ja	.L30
	testl	%esi, %esi
	jne	.L2
.L19:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$64, %esi
.L2:
	movl	%edx, %ecx
	xorl	%eax, %eax
	andl	$2147483647, %ecx
	je	.L1
	shrl	$23, %ecx
	testl	%edx, %edx
	leal	-127(%rcx), %r9d
	js	.L4
	leal	-1(%rsi), %r10d
	cmpl	%r10d, %r9d
	jg	.L22
	movl	%edx, %eax
	andl	$8388607, %eax
	orl	$8388608, %eax
	cmpl	$22, %r9d
	jle	.L7
	subl	$150, %ecx
	xorl	%ebx, %ebx
	salq	%cl, %rax
.L8:
	cmpl	$1, %edi
	je	.L36
	jle	.L64
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	cmpl	$3, %edi
	je	.L18
	cmpl	$4, %edi
	je	.L18
.L34:
	xorl	%r11d, %r11d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	$63, %r9d
	jne	.L20
	testq	%rax, %rax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %edi
	movl	%esi, 12(%rsp)
	call	__GI_feraiseexcept
	movl	12(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	movq	$-1, %rax
	je	.L1
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r9d, %r9d
	jns	.L19
	movl	%edx, %eax
	movl	$-1, %r10d
	andl	$8388607, %eax
	orl	$8388608, %eax
.L7:
	cmpl	$-1, %r9d
	jl	.L32
	movl	$22, %ecx
	movl	$1, %r8d
	subl	%r9d, %ecx
	sall	%cl, %r8d
	movl	$23, %ecx
	movl	%r8d, %ebp
	andl	%eax, %ebp
	setne	%r11b
	subl	$1, %r8d
	andl	%eax, %r8d
	setne	%bl
	subl	%r9d, %ecx
	shrl	%cl, %eax
	cmpl	$1, %edi
	movl	%eax, %eax
	movq	%rax, %rcx
	je	.L10
	jle	.L65
	cmpl	$3, %edi
	je	.L13
	cmpl	$4, %edi
	jne	.L9
	testl	%ebp, %ebp
	je	.L33
	andl	$1, %ecx
	movl	$1, %r11d
	orl	%r8d, %ecx
	setne	%cl
	movzbl	%cl, %ecx
.L18:
	addq	%rcx, %rax
.L9:
	testl	%edx, %edx
	jns	.L16
.L15:
	testq	%rax, %rax
	jne	.L19
.L20:
	testb	%bl, %bl
	jne	.L39
	testb	%r11b, %r11b
	je	.L1
.L39:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L65:
	testl	%edi, %edi
	jne	.L9
.L12:
	testl	%edx, %edx
	js	.L15
	movl	%r11d, %edx
	orl	%ebx, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$63, %r10d
	je	.L66
	leal	1(%r10), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L20
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%r11d, %r11d
	testl	%edi, %edi
	je	.L12
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%r11d, %r11d
.L10:
	testl	%edx, %edx
	jns	.L16
	movl	%r11d, %edx
	orl	%ebx, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$1, %ebx
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	%r11b, %ecx
	jmp	.L18
.L33:
	xorl	%ecx, %ecx
	xorl	%r11d, %r11d
	jmp	.L18
	.size	__ufromfpxf, .-__ufromfpxf
	.weak	ufromfpxf32
	.set	ufromfpxf32,__ufromfpxf
	.weak	ufromfpxf
	.set	ufromfpxf,__ufromfpxf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
