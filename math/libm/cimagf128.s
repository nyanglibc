	.text
	.p2align 4,,15
	.globl	__cimagf128
	.type	__cimagf128, @function
__cimagf128:
	movdqa	24(%rsp), %xmm0
	ret
	.size	__cimagf128, .-__cimagf128
	.weak	cimagf128
	.set	cimagf128,__cimagf128
