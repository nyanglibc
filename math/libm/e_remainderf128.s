	.text
	.globl	__multf3
	.globl	__divtf3
	.globl	__addtf3
	.globl	__gttf2
	.globl	__subtf3
	.globl	__getf2
	.globl	__lttf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__ieee754_remainderf128
	.type	__ieee754_remainderf128, @function
__ieee754_remainderf128:
	pushq	%r12
	pushq	%rbp
	movabsq	$9223372036854775807, %rax
	pushq	%rbx
	subq	$48, %rsp
	movaps	%xmm1, (%rsp)
	movq	8(%rsp), %rbx
	movq	(%rsp), %rbp
	movaps	%xmm0, 16(%rsp)
	andq	%rax, %rbx
	movq	%rbx, %rcx
	orq	%rbp, %rcx
	je	.L23
	movq	24(%rsp), %r12
	movabsq	$9223090561878065151, %rdx
	andq	%r12, %rax
	cmpq	%rdx, %rax
	jg	.L4
	cmpq	%rdx, %rbx
	jg	.L25
	movabsq	$9222809086901354495, %rdx
	movdqa	16(%rsp), %xmm0
	cmpq	%rdx, %rbx
	jg	.L6
	movdqa	(%rsp), %xmm1
	movq	%rax, 32(%rsp)
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__ieee754_fmodf128@PLT
	movq	32(%rsp), %rax
.L6:
	movq	16(%rsp), %rdx
	subq	%rbx, %rax
	subq	%rbp, %rdx
	orq	%rdx, %rax
	je	.L26
	movdqa	.LC1(%rip), %xmm1
	movabsq	$562949953421311, %rax
	cmpq	%rax, %rbx
	movdqa	(%rsp), %xmm2
	pand	%xmm1, %xmm0
	pand	%xmm1, %xmm2
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm2, (%rsp)
	jg	.L8
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L9
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L22
.L9:
	movabsq	$-9223372036854775808, %rax
	movdqa	16(%rsp), %xmm3
	andq	%rax, %r12
	movq	24(%rsp), %rax
	movaps	%xmm3, (%rsp)
	xorq	%rax, %r12
	movq	%r12, 8(%rsp)
	movdqa	(%rsp), %xmm0
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movabsq	$-9223090561878065152, %rdx
	movdqa	16(%rsp), %xmm0
	addq	%rbx, %rdx
	orq	%rbp, %rdx
	je	.L6
.L4:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
.L23:
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	pxor	%xmm1, %xmm1
	call	__multf3@PLT
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L9
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L9
.L22:
	movdqa	16(%rsp), %xmm0
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L9
	.size	__ieee754_remainderf128, .-__ieee754_remainderf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073610752
