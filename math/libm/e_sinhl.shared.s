	.text
#APP
	.symver __ieee754_sinhl,__sinhl_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_sinhl
	.type	__ieee754_sinhl, @function
__ieee754_sinhl:
	pushq	%rbx
	subq	$16, %rsp
	movq	40(%rsp), %rdx
	movl	%edx, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L40
	testw	%dx, %dx
	movswl	%ax, %ebx
	js	.L19
	flds	.LC0(%rip)
.L4:
	cmpl	$16386, %ebx
	jbe	.L5
	movq	32(%rsp), %rax
	shrq	$32, %rax
	cmpl	$16387, %ebx
	jne	.L8
	cmpl	$-939524096, %eax
	ja	.L8
	fstpt	(%rsp)
	subq	$16, %rsp
	fldt	48(%rsp)
	fabs
	fstpt	(%rsp)
	call	__GI___expm1l
	popq	%rax
	popq	%rdx
	fldt	(%rsp)
.L13:
	fld1
	fadd	%st(2), %st
	fdivr	%st(2), %st
	faddp	%st, %st(2)
	fmulp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L42:
	fstp	%st(1)
	fstp	%st(1)
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	fldt	32(%rsp)
	cmpl	$16350, %ebx
	fabs
	ja	.L9
	fldt	.LC2(%rip)
	fucomip	%st(1), %st
	jbe	.L10
	fldt	32(%rsp)
	fmul	%st(0), %st
	fstp	%st(0)
.L10:
	fldt	.LC3(%rip)
	fldt	32(%rsp)
	fadd	%st, %st(1)
	fld1
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	ja	.L42
	fstp	%st(0)
	fxch	%st(1)
	fstpt	(%rsp)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__GI___expm1l
	popq	%rcx
	popq	%rsi
	fldt	(%rsp)
.L18:
	addq	$16, %rsp
	popq	%rbx
	fld	%st(1)
	fadd	%st(2), %st
	fld	%st(2)
	fmul	%st(3), %st
	fld1
	faddp	%st, %st(4)
	fdivp	%st, %st(3)
	fsubp	%st, %st(2)
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	flds	.LC1(%rip)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$16395, %ebx
	jbe	.L14
	cmpl	$16396, %ebx
	jne	.L15
	cmpl	$-1317922826, %eax
	ja	.L15
.L14:
	fstpt	(%rsp)
	subq	$16, %rsp
	fldt	48(%rsp)
	fabs
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	16(%rsp)
	popq	%r9
	popq	%r10
	addq	$16, %rsp
	popq	%rbx
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	fldt	32(%rsp)
	addq	$16, %rsp
	popq	%rbx
	fadd	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$16396, %ebx
	je	.L41
	fstp	%st(0)
	jmp	.L16
.L43:
	fstp	%st(0)
	jmp	.L16
.L44:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L16:
	fldt	.LC3(%rip)
	fldt	32(%rsp)
	fmulp	%st, %st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$-1317741121, %eax
	jbe	.L17
	cmpl	$-1317741120, %eax
	jne	.L43
	movq	32(%rsp), %rax
	cmpl	$833536234, %eax
	ja	.L44
.L17:
	fstpt	(%rsp)
	subq	$16, %rsp
	fldt	48(%rsp)
	fabs
	fmuls	.LC0(%rip)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	16(%rsp)
	popq	%rdi
	popq	%r8
	fmul	%st(1), %st
	fmulp	%st, %st(1)
	jmp	.L1
.L9:
	fxch	%st(1)
	fstpt	(%rsp)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__GI___expm1l
	popq	%r11
	popq	%rax
	cmpl	$16382, %ebx
	fldt	(%rsp)
	jbe	.L18
	jmp	.L13
	.size	__ieee754_sinhl, .-__ieee754_sinhl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1056964608
	.align 4
.LC1:
	.long	3204448256
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC3:
	.long	382466212
	.long	2888024609
	.long	32763
	.long	0
