	.text
	.p2align 4,,15
	.globl	__fegetround
	.type	__fegetround, @function
__fegetround:
#APP
# 29 "../sysdeps/x86_64/fpu/fegetround.c" 1
	fnstcw -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-4(%rsp), %eax
	andl	$3072, %eax
	ret
	.size	__fegetround, .-__fegetround
	.weak	fegetround
	.set	fegetround,__fegetround
