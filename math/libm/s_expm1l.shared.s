 .section .rodata.cst16,"aM",@progbits,16
 .p2align 4
 .type c0,@object
c0: .byte 0, 0, 0, 0, 0, 0, 0xaa, 0xb8, 0xff, 0x3f
 .byte 0, 0, 0, 0, 0, 0
 .size c0,.-c0;
 .type c1,@object
c1: .byte 0x20, 0xfa, 0xee, 0xc2, 0x5f, 0x70, 0xa5, 0xec, 0xed, 0x3f
 .byte 0, 0, 0, 0, 0, 0
 .size c1,.-c1;
 .text
.globl __expm1l
.type __expm1l,@function
.align 1<<4
__expm1l:
 movzwl 8+8(%rsp), %eax
 xorb $0x80, %ah
 cmpl $0xc006, %eax
 jae __GI___expl
 fldt 8(%rsp)
 fxam
 xorb $0x80, %ah
 cmpl $0xc006, %eax
 fstsw %ax
 movb $0x45, %dh
 jb 4f
 andb %ah, %dh
 cmpb $0x01, %dh
 je 6f
 jmp 1f
4:
 andb %ah, %dh
 cmpb $0x40, %dh
 je 2f
 movzwl 8+8(%rsp), %eax
 andl $0x7fff, %eax
 cmpl $0x3fbf, %eax
 jge 3f
 cmpl $0x0001, %eax
 jge 2f
 fld %st
 fmul %st
 fstp %st
 jmp 2f
3: fldl2e
 fmul %st(1), %st
 fstcw -4(%rsp)
 movl $0xf3ff, %edx
 andl -4(%rsp), %edx
 movl %edx, -8(%rsp)
 fldcw -8(%rsp)
 frndint
 fld %st(1)
 frndint
 fldcw -4(%rsp)
 fld %st(1)
 fldt c0(%rip)
 fld %st(2)
 fmul %st(1), %st
 fsubp %st, %st(2)
 fld %st(4)
 fsub %st(3), %st
 fmulp %st, %st(1)
 faddp %st, %st(1)
 fldt c1(%rip)
 fmul %st(4), %st
 faddp %st, %st(1)
 f2xm1
 fstp %st(1)
 fscale
 fxch
 fld1
 fscale
 fld1
 fsubrp %st, %st(1)
 fstp %st(1)
 faddp %st, %st(1)
 fstp %st(1)
 jmp 2f
1:
 fstp %st
 fld1
 fchs
2: ret
6:
 fadd %st
 ret
.size __expm1l,.-__expm1l
.globl __GI___expm1l
.set __GI___expm1l,__expm1l
.weak expm1l
expm1l = __expm1l
.weak expm1f64x
expm1f64x = __expm1l
