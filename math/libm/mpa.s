	.text
	.p2align 4,,15
	.type	add_magnitudes, @function
add_magnitudes:
	movl	(%rdi), %r11d
	movslq	%ecx, %rcx
	movl	%r11d, (%rdx)
	movslq	(%rsi), %r10
	movslq	(%rdi), %r8
	addq	%rcx, %r10
	subq	%r8, %r10
	testq	%r10, %r10
	jle	.L25
	pushq	%r12
	leaq	0(,%rcx,8), %r9
	pushq	%rbp
	pushq	%rbx
	movq	%r10, %rbx
	leaq	(%rsi,%r10,8), %rbp
	negq	%rbx
	leaq	(%rdi,%r9), %r12
	xorl	%r8d, %r8d
	salq	$3, %rbx
	addq	%rdx, %r9
	xorl	%eax, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L27:
	subq	$16777216, %rax
	movq	%rax, 16(%r9,%r8)
	subq	$8, %r8
	movl	$1, %eax
	cmpq	%rbx, %r8
	je	.L26
.L9:
	movq	8(%rbp,%r8), %rsi
	addq	8(%r12,%r8), %rsi
	addq	%rsi, %rax
	cmpq	$16777215, %rax
	jg	.L27
	movq	%rax, 16(%r9,%r8)
	subq	$8, %r8
	xorl	%eax, %eax
	cmpq	%rbx, %r8
	jne	.L9
.L26:
	movl	$1, %esi
	subq	%r10, %rsi
	leaq	(%rsi,%rcx), %r8
	leaq	-1(%rsi,%rcx), %rsi
	testq	%rsi, %rsi
	jle	.L10
	subq	%rsi, %r8
	leaq	(%rdx,%r8,8), %r8
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	subq	$16777216, %rax
	movq	%rax, 8(%r8,%rsi,8)
	subq	$1, %rsi
	movl	$1, %eax
	je	.L10
.L13:
	addq	8(%rdi,%rsi,8), %rax
	cmpq	$16777215, %rax
	jg	.L28
	movq	%rax, 8(%r8,%rsi,8)
	xorl	%eax, %eax
	subq	$1, %rsi
	jne	.L13
.L10:
	testq	%rax, %rax
	jne	.L14
	testq	%rcx, %rcx
	jle	.L1
	addq	$1, %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$1, %rax
	movq	8(%rdx,%rax,8), %rsi
	cmpq	%rcx, %rax
	movq	%rsi, (%rdx,%rax,8)
	jne	.L15
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	addl	$1, %r11d
	movq	$1, 16(%rdx)
	movl	%r11d, (%rdx)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
	testq	%rcx, %rcx
	movl	%r8d, (%rdx)
	leaq	1(%rcx), %rsi
	js	.L22
	.p2align 4,,10
	.p2align 3
.L5:
	movq	8(%rdi,%rax,8), %rcx
	movq	%rcx, 8(%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	jne	.L5
	rep ret
.L22:
	rep ret
	.size	add_magnitudes, .-add_magnitudes
	.p2align 4,,15
	.type	sub_magnitudes, @function
sub_magnitudes:
	pushq	%r13
	pushq	%r12
	movslq	%ecx, %rcx
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %ebp
	movl	%ebp, (%rdx)
	movslq	(%rsi), %r9
	movslq	(%rdi), %r8
	addq	%rcx, %r9
	subq	%r8, %r9
	testq	%r9, %r9
	jle	.L56
	cmpq	%r9, %rcx
	leaq	1(%rcx), %r12
	jg	.L57
.L33:
	movq	$0, 8(%rdx,%r12,8)
	xorl	%eax, %eax
.L34:
	leaq	0(,%rcx,8), %r10
	movq	%r9, %r11
	leaq	(%rsi,%r9,8), %r8
	negq	%r11
	xorl	%esi, %esi
	movq	$-1, %r13
	leaq	(%rdi,%r10), %rbx
	salq	$3, %r11
	addq	%rdx, %r10
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, 8(%r10,%rsi)
	subq	$8, %rsi
	xorl	%eax, %eax
	cmpq	%r11, %rsi
	je	.L58
.L37:
	addq	8(%rbx,%rsi), %rax
	subq	8(%r8,%rsi), %rax
	jns	.L35
	addq	$16777216, %rax
	movq	%rax, 8(%r10,%rsi)
	subq	$8, %rsi
	movq	%r13, %rax
	cmpq	%r11, %rsi
	jne	.L37
.L58:
	movq	%rcx, %rsi
	subq	%r9, %rsi
	testq	%rsi, %rsi
	jle	.L38
	movq	$-1, %r8
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rax, 16(%rdx,%rsi,8)
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L38
.L41:
	addq	8(%rdi,%rsi,8), %rax
	subq	$1, %rsi
	testq	%rax, %rax
	jns	.L39
	addq	$16777216, %rax
	testq	%rsi, %rsi
	movq	%rax, 16(%rdx,%rsi,8)
	movq	%r8, %rax
	jne	.L41
.L38:
	cmpq	$0, 16(%rdx)
	leal	1(%rbp), %esi
	movl	$1, %eax
	jne	.L42
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$1, %rax
	cmpq	$0, 8(%rdx,%rax,8)
	je	.L43
	subl	%eax, %esi
	movl	%esi, %ebp
.L42:
	cmpq	%r12, %rax
	movl	%ebp, (%rdx)
	jg	.L48
	movq	%rax, %rsi
	leaq	2(%rcx), %r8
	movq	%rax, %r9
	negq	%rsi
	leaq	(%rdx,%rsi,8), %rdi
	.p2align 4,,10
	.p2align 3
.L45:
	addq	$1, %rax
	movq	(%rdx,%rax,8), %rsi
	cmpq	%r8, %rax
	movq	%rsi, 8(%rdi,%rax,8)
	jne	.L45
	leaq	3(%rcx), %rax
	subq	%r9, %rax
.L44:
	cmpq	%rax, %rcx
	jl	.L29
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$1, %rax
	cmpq	%rax, %rcx
	movq	$0, (%rdx,%rax,8)
	jne	.L46
.L29:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movq	16(%rsi,%r9,8), %rax
	testq	%rax, %rax
	jle	.L33
	movl	$16777216, %r8d
	subq	%rax, %r8
	movq	$-1, %rax
	movq	%r8, 8(%rdx,%r12,8)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$1, %eax
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L56:
	testq	%rcx, %rcx
	movl	%r8d, (%rdx)
	js	.L29
	addq	$1, %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L32:
	movq	8(%rdi,%rax,8), %rsi
	movq	%rsi, 8(%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L32
	jmp	.L29
	.size	sub_magnitudes, .-sub_magnitudes
	.p2align 4,,15
	.type	__mp_dbl.part.0, @function
__mp_dbl.part.0:
	movl	(%rdi), %ecx
	cmpl	$-41, %ecx
	jl	.L136
	movq	16(%rdi), %rax
.L62:
	cmpl	$4, %edx
	jg	.L83
	pxor	%xmm1, %xmm1
	cmpl	$1, %edx
	cvtsi2sdq	%rax, %xmm1
	je	.L84
	pxor	%xmm0, %xmm0
	cmpl	$2, %edx
	cvtsi2sdq	24(%rdi), %xmm0
	je	.L137
	pxor	%xmm4, %xmm4
	cmpl	$3, %edx
	cvtsi2sdq	32(%rdi), %xmm4
	je	.L138
	pxor	%xmm3, %xmm3
	movsd	.LC3(%rip), %xmm2
	cvtsi2sdq	40(%rdi), %xmm3
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm0
	addsd	%xmm4, %xmm3
	addsd	%xmm1, %xmm0
	mulsd	.LC5(%rip), %xmm3
	addsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm1
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L83:
	cmpq	$8388607, %rax
	movq	%rax, -48(%rsp)
	jg	.L111
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L88:
	addq	%rax, %rax
	addq	%r8, %r8
	cmpq	$8388607, %rax
	jle	.L88
	pxor	%xmm3, %xmm3
	movq	%rax, -48(%rsp)
	cvtsi2sdq	%r8, %xmm3
.L87:
	leaq	-56(%rsp), %r10
	movl	$2, %eax
.L89:
	movq	8(%rdi,%rax,8), %r9
	imulq	%r8, %r9
	movq	%r9, %r11
	sarq	$24, %r9
	addq	%r9, -8(%r10,%rax,8)
	andl	$16777215, %r11d
	movq	%r11, (%r10,%rax,8)
	addq	$1, %rax
	cmpq	$5, %rax
	jne	.L89
	movq	-32(%rsp), %r8
	movq	%r8, %r9
	andl	$524287, %r9d
	cmpq	$262144, %r9
	je	.L139
.L90:
	pxor	%xmm1, %xmm1
	movsd	.LC3(%rip), %xmm2
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%r8, %xmm1
	cvtsi2sdq	-40(%rsp), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	-48(%rsp), %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	divsd	%xmm3, %xmm1
.L84:
	movslq	%ecx, %rdx
	cmpq	$1, %rdx
	pxor	%xmm0, %xmm0
	cvtsi2sdq	8(%rdi), %xmm0
	mulsd	%xmm1, %xmm0
	jle	.L96
	movsd	.LC6(%rip), %xmm1
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L97:
	addq	$1, %rax
	mulsd	%xmm1, %xmm0
	cmpq	%rax, %rdx
	jne	.L97
.L98:
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	movsd	.LC3(%rip), %xmm2
	mulsd	%xmm2, %xmm4
	addsd	%xmm4, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L139:
	cmpq	$0, -24(%rsp)
	jne	.L130
	movslq	%edx, %rdx
	addq	$1, %rdx
	cmpq	$0, 48(%rdi)
	je	.L95
.L130:
	addq	$1, %r8
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L96:
	testq	%rdx, %rdx
	jg	.L98
	movsd	.LC3(%rip), %xmm2
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L99:
	subq	$1, %rax
	mulsd	%xmm2, %xmm0
	cmpq	%rax, %rdx
	jne	.L99
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	movsd	.LC3(%rip), %xmm2
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L136:
	cmpl	$-42, %ecx
	jne	.L61
	movq	16(%rdi), %rax
	cmpq	$1023, %rax
	jg	.L62
	movslq	%edx, %rdx
	cmpq	$1, %rdx
	je	.L140
	cmpq	$2, %rdx
	je	.L109
.L134:
	movq	24(%rdi), %r8
	pxor	%xmm3, %xmm3
	pxor	%xmm4, %xmm4
	leaq	1024(%rax), %r10
	movq	32(%rdi), %rax
	movsd	.LC1(%rip), %xmm2
	cvtsi2sdq	%r10, %xmm3
	movl	$3, %r9d
	cvtsi2sdq	%r8, %xmm4
.L76:
	movq	%rax, %rcx
	andq	$-32, %rcx
	cmpq	%rax, %rcx
	je	.L77
.L107:
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm1
	cvtsi2sdq	%rax, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
.L78:
	addsd	%xmm3, %xmm0
	pxor	%xmm1, %xmm1
	cvtsi2sdq	8(%rdi), %xmm1
	subsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	mulsd	.LC4(%rip), %xmm0
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	movsd	.LC2(%rip), %xmm3
	movl	$1, %r8d
	jmp	.L87
.L95:
	addq	$1, %rax
	cmpq	%rdx, %rax
	je	.L90
	cmpq	$0, 8(%rdi,%rax,8)
	jne	.L130
	jmp	.L95
.L61:
	cmpl	$-44, %ecx
	jge	.L141
.L64:
	movq	$0x000000000, (%rsi)
	ret
.L140:
	pxor	%xmm0, %xmm0
	addq	$1024, %rax
.L68:
	pxor	%xmm3, %xmm3
	movsd	.LC1(%rip), %xmm2
	cvtsi2sdq	%rax, %xmm3
	jmp	.L78
.L141:
	movslq	%edx, %rdx
	movq	16(%rdi), %rax
	jne	.L65
	cmpq	$31, %rax
	jle	.L64
	cmpq	$1, %rdx
	je	.L142
	cmpq	$2, %rdx
	je	.L72
.L110:
	movsd	.LC1(%rip), %xmm2
	xorl	%r8d, %r8d
	movl	$1024, %r10d
	pxor	%xmm4, %xmm4
	movl	$1, %r9d
	movapd	%xmm2, %xmm3
	jmp	.L76
.L142:
	pxor	%xmm4, %xmm4
.L69:
	movq	%rax, %rcx
	movsd	.LC1(%rip), %xmm2
	andq	$-32, %rcx
	cmpq	%rax, %rcx
	movapd	%xmm2, %xmm3
	jne	.L107
.L106:
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm1
	cvtsi2sdq	%rcx, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	mulsd	%xmm1, %xmm0
	jmp	.L78
.L72:
	testb	$31, %al
	je	.L143
	pxor	%xmm0, %xmm0
	movsd	.LC3(%rip), %xmm1
	movsd	.LC1(%rip), %xmm2
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm0
	addsd	.LC0(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	jmp	.L78
.L109:
	pxor	%xmm0, %xmm0
	addq	$1024, %rax
	cvtsi2sdq	24(%rdi), %xmm0
	addsd	.LC0(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	jmp	.L68
.L65:
	cmpq	$1, %rdx
	je	.L144
	cmpq	$2, %rdx
	je	.L145
	cmpl	$-42, %ecx
	je	.L134
	cmpl	$-43, %ecx
	jne	.L110
	pxor	%xmm4, %xmm4
	movq	%rax, %r8
	movl	$1024, %r10d
	movsd	.LC1(%rip), %xmm2
	movl	$2, %r9d
	cvtsi2sdq	%rax, %xmm4
	movq	24(%rdi), %rax
	movapd	%xmm2, %xmm3
	jmp	.L76
.L144:
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movl	$1024, %eax
	addsd	.LC0(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	jmp	.L68
.L145:
	cmpl	$-42, %ecx
	je	.L109
	cmpl	$-43, %ecx
	jne	.L72
	pxor	%xmm4, %xmm4
	cvtsi2sdq	%rax, %xmm4
	movq	24(%rdi), %rax
	jmp	.L69
.L77:
	leaq	1(%r9), %rax
	cmpq	%rdx, %rax
	jg	.L106
.L81:
	cmpq	$0, 8(%rdi,%rax,8)
	je	.L146
.L80:
	pxor	%xmm0, %xmm0
	addq	$1, %rcx
	pxor	%xmm1, %xmm1
	movsd	.LC3(%rip), %xmm3
	cvtsi2sdq	%rcx, %xmm0
	cvtsi2sdq	%r8, %xmm1
	mulsd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm3, %xmm0
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%r10, %xmm3
	jmp	.L78
.L146:
	addq	$1, %rax
	cmpq	%rdx, %rax
	jle	.L81
	movq	%rcx, %rax
.L108:
	pxor	%xmm0, %xmm0
	pxor	%xmm3, %xmm3
	movsd	.LC3(%rip), %xmm1
	cvtsi2sdq	%rax, %xmm0
	cvtsi2sdq	%r8, %xmm3
	mulsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	pxor	%xmm3, %xmm3
	cvtsi2sdq	%r10, %xmm3
	mulsd	%xmm1, %xmm0
	jmp	.L78
.L143:
	cmpq	$0, 24(%rdi)
	je	.L113
	movq	%rax, %rcx
	movl	$1024, %r10d
	xorl	%r8d, %r8d
	movsd	.LC1(%rip), %xmm2
	jmp	.L80
.L113:
	movl	$1024, %r10d
	xorl	%r8d, %r8d
	movsd	.LC1(%rip), %xmm2
	jmp	.L108
	.size	__mp_dbl.part.0, .-__mp_dbl.part.0
	.p2align 4,,15
	.globl	__acr
	.type	__acr, @function
__acr:
	cmpq	$0, 8(%rdi)
	movq	8(%rsi), %rcx
	jne	.L148
	xorl	%eax, %eax
	testq	%rcx, %rcx
	setne	%al
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.L147
	movl	(%rsi), %ecx
	cmpl	%ecx, (%rdi)
	jg	.L147
	movl	$-1, %eax
	jl	.L147
	testl	%edx, %edx
	jle	.L160
	movq	16(%rdi), %rcx
	movq	16(%rsi), %r8
	cmpq	%rcx, %r8
	je	.L161
.L152:
	xorl	%eax, %eax
	cmpq	%rcx, %r8
	setl	%al
	leal	-1(%rax,%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	rep ret
	.p2align 4,,10
	.p2align 3
.L161:
	movslq	%edx, %rax
	movl	$1, %edx
	addq	$1, %rax
.L154:
	addq	$1, %rdx
	cmpq	%rax, %rdx
	je	.L160
	movq	8(%rdi,%rdx,8), %rcx
	movq	8(%rsi,%rdx,8), %r8
	cmpq	%r8, %rcx
	jne	.L152
	jmp	.L154
.L160:
	xorl	%eax, %eax
	ret
	.size	__acr, .-__acr
	.p2align 4,,15
	.globl	__cpy
	.type	__cpy, @function
__cpy:
	movl	(%rdi), %eax
	testl	%edx, %edx
	movl	%eax, (%rsi)
	js	.L162
	movslq	%edx, %rdx
	xorl	%eax, %eax
	leaq	1(%rdx), %rcx
	.p2align 4,,10
	.p2align 3
.L164:
	movq	8(%rdi,%rax,8), %rdx
	movq	%rdx, 8(%rsi,%rax,8)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L164
.L162:
	rep ret
	.size	__cpy, .-__cpy
	.p2align 4,,15
	.globl	__mp_dbl
	.type	__mp_dbl, @function
__mp_dbl:
	cmpq	$0, 8(%rdi)
	je	.L170
	jmp	__mp_dbl.part.0
	.p2align 4,,10
	.p2align 3
.L170:
	movq	$0x000000000, (%rsi)
	ret
	.size	__mp_dbl, .-__mp_dbl
	.p2align 4,,15
	.globl	__dbl_mp
	.type	__dbl_mp, @function
__dbl_mp:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L172
	je	.L197
.L172:
	ucomisd	%xmm1, %xmm0
	jbe	.L196
	movq	$1, 8(%rdi)
.L177:
	movsd	.LC6(%rip), %xmm2
	movl	$1, (%rdi)
	movl	$2, %eax
	movsd	.LC3(%rip), %xmm1
	ucomisd	%xmm2, %xmm0
	jb	.L178
	.p2align 4,,10
	.p2align 3
.L180:
	mulsd	%xmm1, %xmm0
	movl	%eax, %edx
	addl	$1, %eax
	ucomisd	%xmm2, %xmm0
	jnb	.L180
	movl	%edx, (%rdi)
.L178:
	movsd	.LC2(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.L181
	movl	(%rdi), %eax
	subl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L183:
	mulsd	%xmm2, %xmm0
	movl	%eax, %edx
	subl	$1, %eax
	ucomisd	%xmm0, %xmm1
	ja	.L183
	movl	%edx, (%rdi)
.L181:
	movslq	%esi, %rsi
	movl	$4, %ecx
	cmpq	$4, %rsi
	cmovle	%rsi, %rcx
	testq	%rsi, %rsi
	jle	.L171
	movl	$1, %eax
.L185:
	cvttsd2siq	%xmm0, %rdx
	pxor	%xmm1, %xmm1
	cvtsi2sdq	%rdx, %xmm1
	movq	%rdx, 8(%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rcx
	subsd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	jge	.L185
	cmpq	%rax, %rsi
	jl	.L171
	addq	$1, %rsi
	.p2align 4,,10
	.p2align 3
.L186:
	movq	$0, 8(%rdi,%rax,8)
	addq	$1, %rax
	cmpq	%rsi, %rax
	jne	.L186
.L171:
	rep ret
	.p2align 4,,10
	.p2align 3
.L197:
	movq	$0, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L196:
	movq	$-1, 8(%rdi)
	xorpd	.LC7(%rip), %xmm0
	jmp	.L177
	.size	__dbl_mp, .-__dbl_mp
	.p2align 4,,15
	.globl	__add
	.type	__add, @function
__add:
	movq	8(%rdi), %r10
	testq	%r10, %r10
	jne	.L199
	movl	(%rsi), %eax
	testl	%ecx, %ecx
	movl	%eax, (%rdx)
	js	.L212
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L202:
	movq	8(%rsi,%rax,8), %rdi
	movq	%rdi, 8(%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L202
	rep ret
	.p2align 4,,10
	.p2align 3
.L199:
	movq	8(%rsi), %r11
	testq	%r11, %r11
	jne	.L204
	movl	(%rdi), %eax
	testl	%ecx, %ecx
	movl	%eax, (%rdx)
	js	.L212
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	addq	$1, %rcx
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L214:
	movq	8(%rdi,%rax,8), %r10
.L205:
	movq	%r10, 8(%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L214
.L212:
	rep ret
	.p2align 4,,10
	.p2align 3
.L204:
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r9d
	pushq	%rbx
	movq	%rdx, %rbx
	movl	%ecx, %edx
	movq	%rsi, %rbp
	movq	%rdi, %r12
	call	__acr
	cmpq	%r11, %r10
	je	.L215
	cmpl	$1, %eax
	je	.L216
	cmpl	$-1, %eax
	je	.L217
	movq	$0, 8(%rbx)
.L198:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L215:
	testl	%eax, %eax
	movl	%r9d, %ecx
	movq	%rbx, %rdx
	jle	.L207
	call	add_magnitudes
	movq	8(%rdi), %rax
	movq	%rax, 8(%rbx)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L207:
	movq	%rdi, %rsi
	movq	%rbp, %rdi
	call	add_magnitudes
	movq	8(%rbp), %rax
	movq	%rax, 8(%rbx)
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L216:
	movl	%r9d, %ecx
	movq	%rbx, %rdx
	call	sub_magnitudes
	movq	8(%r12), %rax
	movq	%rax, 8(%rbx)
	jmp	.L198
.L217:
	movq	%rdi, %rsi
	movl	%r9d, %ecx
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	sub_magnitudes
	movq	8(%rbp), %rax
	movq	%rax, 8(%rbx)
	jmp	.L198
	.size	__add, .-__add
	.p2align 4,,15
	.globl	__sub
	.type	__sub, @function
__sub:
	movq	8(%rdi), %r10
	testq	%r10, %r10
	jne	.L219
	movl	(%rsi), %eax
	testl	%ecx, %ecx
	movl	%eax, (%rdx)
	js	.L220
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L221:
	movq	8(%rsi,%rax,8), %rdi
	movq	%rdi, 8(%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L221
.L220:
	negq	8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	movq	8(%rsi), %r11
	testq	%r11, %r11
	jne	.L223
	movl	(%rdi), %eax
	testl	%ecx, %ecx
	movl	%eax, (%rdx)
	js	.L232
	movslq	%ecx, %rcx
	xorl	%eax, %eax
	addq	$1, %rcx
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L234:
	movq	8(%rdi,%rax,8), %r10
.L225:
	movq	%r10, 8(%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L234
.L232:
	rep ret
	.p2align 4,,10
	.p2align 3
.L223:
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r9d
	pushq	%rbx
	movq	%rdx, %rbx
	movl	%ecx, %edx
	movq	%rsi, %rbp
	movq	%rdi, %r12
	call	__acr
	cmpq	%r11, %r10
	je	.L226
	testl	%eax, %eax
	movl	%r9d, %ecx
	movq	%rbx, %rdx
	jle	.L227
	call	add_magnitudes
	movq	8(%rdi), %rax
	movq	%rax, 8(%rbx)
.L218:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L226:
	cmpl	$1, %eax
	je	.L235
	cmpl	$-1, %eax
	je	.L236
	movq	$0, 8(%rbx)
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L227:
	movq	%rdi, %rsi
	movq	%rbp, %rdi
	call	add_magnitudes
	movq	8(%rbp), %rax
	negq	%rax
	movq	%rax, 8(%rbx)
	jmp	.L218
.L235:
	movl	%r9d, %ecx
	movq	%rbx, %rdx
	call	sub_magnitudes
	movq	8(%r12), %rax
	movq	%rax, 8(%rbx)
	jmp	.L218
.L236:
	movq	%rdi, %rsi
	movl	%r9d, %ecx
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	sub_magnitudes
	movq	8(%rbp), %rax
	negq	%rax
	movq	%rax, 8(%rbx)
	jmp	.L218
	.size	__sub, .-__sub
	.p2align 4,,15
	.globl	__mul
	.type	__mul, @function
__mul:
	movq	8(%rdi), %rax
	imulq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L238
	pushq	%rbp
	movslq	%ecx, %rcx
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	testq	%rcx, %rcx
	movq	8(%rdi,%rcx,8), %rax
	jle	.L239
	testq	%rax, %rax
	jne	.L274
	cmpq	$0, 8(%rsi,%rcx,8)
	jne	.L275
	movq	%rcx, %rax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L244:
	testq	%r9, %r9
	jne	.L276
	cmpq	$0, 8(%rsi,%rax,8)
	jne	.L277
.L241:
	subq	$1, %rax
	testq	%rax, %rax
	movq	8(%rdi,%rax,8), %r9
	jne	.L244
	xorl	%r8d, %r8d
	testq	%r9, %r9
	jne	.L248
.L247:
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L248:
	cmpq	$2, %rcx
	leaq	3(%rcx), %r10
	jle	.L246
.L254:
	addq	%r8, %rax
	leaq	1(%rax), %r11
	cmpq	%r11, %r10
	jle	.L253
	leaq	8(%rdx,%r10,8), %r9
	leaq	16(%rdx,%rax,8), %rax
	.p2align 4,,10
	.p2align 3
.L255:
	movq	$0, (%r9)
	subq	$8, %r9
	cmpq	%r9, %rax
	jne	.L255
	movq	%r11, %r10
.L253:
	leaq	0(,%r10,8), %r12
	leaq	30(%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r11
	andq	$-16, %r11
	testq	%r8, %r8
	jle	.L280
	addq	$1, %r8
	xorl	%r9d, %r9d
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L257:
	movq	8(%rdi,%rax,8), %rbx
	imulq	8(%rsi,%rax,8), %rbx
	addq	%rbx, %r9
	movq	%r9, (%r11,%rax,8)
	addq	$1, %rax
	cmpq	%r8, %rax
	jne	.L257
.L256:
	cmpq	%rax, %r10
	jle	.L258
	leaq	(%r11,%rax,8), %rax
	leaq	(%r11,%r12), %r8
	.p2align 4,,10
	.p2align 3
.L259:
	movq	%r9, (%rax)
	addq	$8, %rax
	cmpq	%rax, %r8
	jne	.L259
.L258:
	cmpq	%r10, %rcx
	jge	.L281
	movq	%r10, %r13
	xorl	%eax, %eax
	subq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L264:
	testb	$1, %r10b
	jne	.L261
	movq	%r10, %r8
	shrq	$63, %r8
	addq	%r10, %r8
	sarq	%r8
	movq	8(%rdi,%r8,8), %rbx
	leaq	(%rbx,%rbx), %r9
	imulq	8(%rsi,%r8,8), %r9
	addq	%r9, %rax
.L261:
	cmpq	%r13, %rcx
	movq	%r13, %r9
	jle	.L262
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L263:
	movq	8(%rdi,%rbx,8), %r8
	movq	8(%rsi,%rbx,8), %r12
	subq	$1, %rbx
	addq	8(%rdi,%r9,8), %r8
	addq	8(%rsi,%r9,8), %r12
	addq	$1, %r9
	imulq	%r12, %r8
	addq	%r8, %rax
	cmpq	%rbx, %r9
	jl	.L263
.L262:
	subq	-8(%r11,%r10,8), %rax
	subq	$1, %r13
	movq	%rax, %r8
	sarq	$24, %rax
	andl	$16777215, %r8d
	movq	%r8, 8(%rdx,%r10,8)
	subq	$1, %r10
	cmpq	%r10, %rcx
	jne	.L264
.L260:
	cmpq	$1, %r10
	jle	.L265
	.p2align 4,,10
	.p2align 3
.L273:
	testb	$1, %r10b
	jne	.L266
	movq	%r10, %r9
	sarq	%r9
	movq	8(%rdi,%r9,8), %rbx
	leaq	(%rbx,%rbx), %r8
	imulq	8(%rsi,%r9,8), %r8
	addq	%r8, %rax
.L266:
	subq	$1, %r10
	cmpq	$1, %r10
	je	.L267
	movq	%r10, %rbx
	movl	$1, %r9d
	.p2align 4,,10
	.p2align 3
.L268:
	movq	8(%rdi,%rbx,8), %r8
	movq	8(%rsi,%rbx,8), %r12
	subq	$1, %rbx
	addq	8(%rdi,%r9,8), %r8
	addq	8(%rsi,%r9,8), %r12
	addq	$1, %r9
	imulq	%r12, %r8
	addq	%r8, %rax
	cmpq	%rbx, %r9
	jl	.L268
	subq	(%r11,%r10,8), %rax
	movq	%rax, %r8
	sarq	$24, %rax
	andl	$16777215, %r8d
	movq	%r8, 16(%rdx,%r10,8)
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L267:
	subq	8(%r11), %rax
	movq	%rax, %r8
	sarq	$24, %rax
	andl	$16777215, %r8d
	movq	%r8, 24(%rdx)
.L265:
	movq	%rax, 8(%rdx,%r10,8)
	movl	(%rsi), %eax
	addl	(%rdi), %eax
	cmpq	$0, 16(%rdx)
	je	.L296
.L270:
	movl	%eax, (%rdx)
	movq	8(%rdi), %rax
	imulq	8(%rsi), %rax
	movq	%rax, 8(%rdx)
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
.L276:
	movq	%rsi, %r9
.L240:
	cmpq	$0, 8(%r9,%rax,8)
	movq	%rax, %r8
	je	.L249
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L250:
	cmpq	$0, 8(%r9,%r8,8)
	jne	.L248
.L249:
	subq	$1, %r8
	jne	.L250
	jmp	.L248
.L277:
	movq	%rdi, %r9
	jmp	.L240
.L280:
	xorl	%r9d, %r9d
	movl	$1, %eax
	jmp	.L256
.L281:
	xorl	%eax, %eax
	jmp	.L260
.L238:
	movq	$0, 8(%rdx)
	ret
.L296:
	testq	%rcx, %rcx
	jle	.L271
	leaq	16(%rdx), %r8
	leaq	(%r8,%rcx,8), %r9
	.p2align 4,,10
	.p2align 3
.L272:
	movq	8(%r8), %rcx
	addq	$8, %r8
	movq	%rcx, -8(%r8)
	cmpq	%r8, %r9
	jne	.L272
.L271:
	subl	$1, %eax
	jmp	.L270
.L239:
	testq	%rax, %rax
	je	.L297
	movq	%rcx, %r8
	movq	%rcx, %rax
.L246:
	leaq	(%rcx,%rcx), %r10
	jmp	.L254
.L274:
	movq	%rsi, %r9
	movq	%rcx, %rax
	jmp	.L240
.L275:
	movq	%rdi, %r9
	movq	%rcx, %rax
	jmp	.L240
.L297:
	movq	%rcx, %rax
	jmp	.L247
	.size	__mul, .-__mul
	.p2align 4,,15
	.globl	__sqr
	.type	__sqr, @function
__sqr:
	cmpq	$0, 8(%rdi)
	je	.L335
	movslq	%edx, %r11
	pushq	%rbx
	testq	%r11, %r11
	movq	%r11, %rax
	jle	.L301
	cmpq	$0, 8(%rdi,%r11,8)
	je	.L302
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L303:
	cmpq	$0, 8(%rdi,%rax,8)
	jne	.L301
.L302:
	subq	$1, %rax
	jne	.L303
.L301:
	leal	3(%rdx), %r8d
	leal	(%rdx,%rdx), %ecx
	cmpl	$2, %edx
	movslq	%ecx, %rcx
	movslq	%r8d, %r8
	cmovle	%rcx, %r8
	leaq	1(%rax,%rax), %rcx
	cmpq	%rcx, %r8
	jle	.L306
	salq	$4, %rax
	leaq	8(%rsi,%r8,8), %rdx
	leaq	16(%rsi,%rax), %rax
	.p2align 4,,10
	.p2align 3
.L308:
	movq	$0, (%rdx)
	subq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.L308
	movq	%rcx, %r8
.L306:
	cmpq	%r8, %r11
	jge	.L325
	movq	%r8, %rbx
	xorl	%r10d, %r10d
	subq	%r11, %rbx
	.p2align 4,,10
	.p2align 3
.L313:
	testb	$1, %r8b
	jne	.L310
	movq	%r8, %rax
	shrq	$63, %rax
	addq	%r8, %rax
	sarq	%rax
	movq	8(%rdi,%rax,8), %rax
	imulq	%rax, %rax
	addq	%rax, %r10
.L310:
	cmpq	%rbx, %r11
	movq	%rbx, %rax
	jle	.L311
	movq	%r11, %rdx
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L312:
	movq	8(%rdi,%rax,8), %rcx
	addq	$1, %rax
	imulq	8(%rdi,%rdx,8), %rcx
	subq	$1, %rdx
	addq	%rcx, %r9
	cmpq	%rdx, %rax
	jl	.L312
	leaq	(%r10,%r9,2), %r10
.L311:
	movq	%r10, %rax
	subq	$1, %rbx
	sarq	$24, %r10
	andl	$16777215, %eax
	movq	%rax, 8(%rsi,%r8,8)
	subq	$1, %r8
	cmpq	%r8, %r11
	jne	.L313
	movq	%r11, %r8
.L309:
	cmpq	$1, %r8
	jle	.L314
	.p2align 4,,10
	.p2align 3
.L322:
	testb	$1, %r8b
	jne	.L315
	movq	%r8, %rax
	sarq	%rax
	movq	8(%rdi,%rax,8), %rax
	imulq	%rax, %rax
	addq	%rax, %r10
.L315:
	subq	$1, %r8
	cmpq	$1, %r8
	je	.L316
	movq	%r8, %rdx
	xorl	%r9d, %r9d
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L317:
	movq	8(%rdi,%rax,8), %rcx
	addq	$1, %rax
	imulq	8(%rdi,%rdx,8), %rcx
	subq	$1, %rdx
	addq	%rcx, %r9
	cmpq	%rdx, %rax
	jl	.L317
	leaq	(%r10,%r9,2), %r10
	movq	%r10, %rax
	sarq	$24, %r10
	andl	$16777215, %eax
	movq	%rax, 16(%rsi,%r8,8)
	jmp	.L322
	.p2align 4,,10
	.p2align 3
.L316:
	movq	%r10, %rax
	sarq	$24, %r10
	andl	$16777215, %eax
	movq	%rax, 24(%rsi)
.L314:
	movl	(%rdi), %eax
	movq	%r10, 8(%rsi,%r8,8)
	movq	$1, 8(%rsi)
	addl	%eax, %eax
	cmpq	$0, 16(%rsi)
	je	.L336
.L319:
	movl	%eax, (%rsi)
	popq	%rbx
	ret
.L325:
	xorl	%r10d, %r10d
	jmp	.L309
.L335:
	movq	$0, 8(%rsi)
	ret
.L336:
	testq	%r11, %r11
	jle	.L320
	leaq	16(%rsi), %rdx
	leaq	(%rdx,%r11,8), %rdi
	.p2align 4,,10
	.p2align 3
.L321:
	movq	8(%rdx), %rcx
	addq	$8, %rdx
	movq	%rcx, -8(%rdx)
	cmpq	%rdx, %rdi
	jne	.L321
.L320:
	subl	$1, %eax
	jmp	.L319
	.size	__sqr, .-__sqr
	.p2align 4,,15
	.globl	__dvd
	.type	__dvd, @function
__dvd:
	cmpq	$0, 8(%rdi)
	jne	.L338
	movq	$0, 8(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L338:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1080, %rsp
	movl	(%rsi), %eax
	movl	%eax, 400(%rsp)
	movslq	%ecx, %rax
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	js	.L340
	leaq	400(%rsp), %rbp
	leaq	1(%rax), %r9
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L341:
	movq	8(%rsi,%rax,8), %r8
	movq	%r8, 8(%rbp,%rax,8)
	addq	$1, %rax
	cmpq	%r9, %rax
	jne	.L341
.L340:
	cmpq	$0, 408(%rsp)
	movl	%ecx, %ebx
	movq	%rdx, 40(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 32(%rsp)
	movl	$0, 400(%rsp)
	pxor	%xmm1, %xmm1
	jne	.L353
.L342:
	movsd	.LC2(%rip), %xmm0
	leaq	64(%rsp), %r15
	movl	%ebx, %esi
	divsd	%xmm1, %xmm0
	movq	%r15, %rdi
	movsd	%xmm0, 56(%rsp)
	call	__dbl_mp
	movq	16(%rsp), %rcx
	leaq	np1.3278(%rip), %rdx
	movq	8(%rsp), %rdi
	movl	64(%rsp), %eax
	subl	(%rcx), %eax
	movslq	(%rdx,%rdi,4), %rcx
	movl	%eax, 64(%rsp)
	testq	%rcx, %rcx
	movq	%rcx, 24(%rsp)
	jle	.L343
	leaq	400(%rsp), %rbp
	leaq	736(%rsp), %r14
	leaq	1(%rdi), %r12
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L346:
	cmpq	$0, 8(%rsp)
	movl	%eax, 736(%rsp)
	js	.L344
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L345:
	movq	8(%r15,%rax,8), %rdx
	movq	%rdx, 8(%r14,%rax,8)
	addq	$1, %rax
	cmpq	%r12, %rax
	jne	.L345
.L344:
	movq	16(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	addq	$1, %r13
	call	__mul
	leaq	__mptwo(%rip), %rdi
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	movq	%r15, %rsi
	call	__sub
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	__mul
	cmpq	24(%rsp), %r13
	je	.L343
	movl	64(%rsp), %eax
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L343:
	movq	40(%rsp), %rdx
	movq	32(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r15, %rsi
	call	__mul
	addq	$1080, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L353:
	leaq	400(%rsp), %rbp
	leaq	56(%rsp), %rsi
	movl	%ecx, %edx
	movq	%rbp, %rdi
	call	__mp_dbl.part.0
	movsd	56(%rsp), %xmm1
	jmp	.L342
	.size	__dvd, .-__dvd
	.section	.rodata
	.align 32
	.type	np1.3278, @object
	.size	np1.3278, 132
np1.3278:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.globl	__mptwo
	.align 32
	.type	__mptwo, @object
	.size	__mptwo, 328
__mptwo:
	.long	1
	.zero	4
	.quad	1
	.quad	2
	.zero	304
	.globl	__mpone
	.align 32
	.type	__mpone, @object
	.size	__mpone, 328
__mpone:
	.long	1
	.zero	4
	.quad	1
	.quad	1
	.zero	304
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	1083179008
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	1047527424
	.align 8
.LC4:
	.long	0
	.long	1024
	.align 8
.LC5:
	.long	0
	.long	1022361600
	.align 8
.LC6:
	.long	0
	.long	1097859072
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
