	.text
	.globl	__addtf3
	.globl	__multf3
	.p2align 4,,15
	.globl	__frexpf128
	.type	__frexpf128, @function
__frexpf128:
	pushq	%rbx
	movabsq	$9223372036854775807, %rbx
	movabsq	$9223090561878065151, %rcx
	subq	$32, %rsp
	movl	$0, (%rdi)
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	andq	%rbx, %rax
	cmpq	%rcx, %rax
	ja	.L2
	movq	(%rsp), %rcx
	orq	%rax, %rcx
	je	.L2
	movabsq	$281474976710655, %rsi
	movl	$-16382, %ecx
	cmpq	%rsi, %rax
	ja	.L5
	movdqa	(%rsp), %xmm0
	movq	%rdi, 24(%rsp)
	movdqa	.LC0(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	24(%rsp), %rdi
	movq	%rbx, %rax
	movl	$-16496, %ecx
	andq	%rdx, %rax
.L5:
	shrq	$48, %rax
	addl	%ecx, %eax
	movl	%eax, (%rdi)
	movabsq	$-9223090561878065153, %rax
	andq	%rax, %rdx
	movabsq	$4611123068473966592, %rax
	orq	%rax, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	addq	$32, %rsp
	popq	%rbx
	ret
	.size	__frexpf128, .-__frexpf128
	.weak	frexpf128
	.set	frexpf128,__frexpf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1081147392
