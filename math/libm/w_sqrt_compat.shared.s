	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__sqrt
	.type	__sqrt, @function
__sqrt:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.L4
.L2:
	jmp	__ieee754_sqrt@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	movapd	%xmm0, %xmm1
	movl	$26, %edi
	jmp	__kernel_standard@PLT
	.size	__sqrt, .-__sqrt
	.weak	sqrtf32x
	.set	sqrtf32x,__sqrt
	.weak	sqrtf64
	.set	sqrtf64,__sqrt
	.weak	sqrt
	.set	sqrt,__sqrt
