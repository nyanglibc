	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__hypotf
	.type	__hypotf, @function
__hypotf:
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_hypotf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm5
	movss	.LC1(%rip), %xmm2
	andps	%xmm1, %xmm5
	movss	8(%rsp), %xmm3
	movss	12(%rsp), %xmm4
	ucomiss	%xmm5, %xmm2
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movaps	%xmm3, %xmm5
	andps	%xmm1, %xmm5
	ucomiss	%xmm5, %xmm2
	jb	.L1
	andps	%xmm4, %xmm1
	ucomiss	%xmm1, %xmm2
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movaps	%xmm4, %xmm1
	movaps	%xmm3, %xmm0
	movl	$104, %edi
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	__hypotf, .-__hypotf
	.weak	hypotf32
	.set	hypotf32,__hypotf
	.weak	hypotf
	.set	hypotf,__hypotf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
