	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __logf_compat,logf@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__logf_compat
	.type	__logf_compat, @function
__logf_compat:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L10
.L2:
	jmp	__ieee754_logf@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$24, %rsp
	ucomiss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)
	jp	.L3
	jne	.L3
	movl	$4, %edi
	call	__GI_feraiseexcept
	movss	12(%rsp), %xmm0
	movl	$116, %edi
	movaps	%xmm0, %xmm1
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movss	12(%rsp), %xmm0
	movl	$117, %edi
	movaps	%xmm0, %xmm1
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	__logf_compat, .-__logf_compat
