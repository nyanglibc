	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__acoshf
	.type	__acoshf, @function
__acoshf:
	movss	.LC0(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.L4
.L2:
	jmp	__ieee754_acoshf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	movaps	%xmm0, %xmm1
	movl	$129, %edi
	jmp	__kernel_standard_f@PLT
	.size	__acoshf, .-__acoshf
	.weak	acoshf32
	.set	acoshf32,__acoshf
	.weak	acoshf
	.set	acoshf,__acoshf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
