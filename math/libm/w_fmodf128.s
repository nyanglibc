	.text
	.globl	__unordtf2
	.globl	__letf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__fmodf128
	.type	__fmodf128, @function
__fmodf128:
	pushq	%rbx
	subq	$48, %rsp
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm1, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 32(%rsp)
	movdqa	.LC1(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L10
	movdqa	32(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	setle	%bl
.L4:
	testb	%bl, %bl
	je	.L6
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L2
.L6:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L2:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	addq	$48, %rsp
	popq	%rbx
	jmp	__ieee754_fmodf128@PLT
.L10:
	movl	$1, %ebx
	jmp	.L4
	.size	__fmodf128, .-__fmodf128
	.weak	fmodf128
	.set	fmodf128,__fmodf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
