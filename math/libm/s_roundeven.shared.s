	.text
	.p2align 4,,15
	.globl	__GI___roundeven
	.hidden	__GI___roundeven
	.type	__GI___roundeven, @function
__GI___roundeven:
	movq	%xmm0, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rax, %rdx
	movq	%rdx, %rsi
	shrq	$52, %rsi
	cmpq	$1074, %rsi
	jbe	.L2
	cmpq	$2047, %rsi
	je	.L17
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$1022, %rsi
	jbe	.L4
	movl	$1, %edx
	movl	$1074, %ecx
	subl	%esi, %ecx
	movq	%rdx, %rdi
	salq	%cl, %rdi
	movl	$1075, %ecx
	subl	%esi, %ecx
	salq	%cl, %rdx
	leaq	-1(%rdi), %rcx
	addq	%rax, %rdi
	orq	%rdx, %rcx
	testq	%rax, %rcx
	cmovne	%rdi, %rax
	negq	%rdx
	andq	%rdx, %rax
.L6:
	movq	%rax, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movabsq	$-9223372036854775808, %rcx
	andq	%rcx, %rax
	cmpq	$1022, %rsi
	jne	.L6
	movabsq	$4602678819172646912, %rcx
	cmpq	%rcx, %rdx
	jbe	.L6
	movabsq	$4607182418800017408, %rdx
	orq	%rdx, %rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	addsd	%xmm0, %xmm0
	ret
	.size	__GI___roundeven, .-__GI___roundeven
	.globl	__roundeven
	.set	__roundeven,__GI___roundeven
	.weak	roundevenf32x
	.set	roundevenf32x,__roundeven
	.weak	roundevenf64
	.set	roundevenf64,__roundeven
	.weak	roundeven
	.set	roundeven,__roundeven
