	.text
	.p2align 4,,15
	.globl	__cpowf
	.type	__cpowf, @function
__cpowf:
	subq	$88, %rsp
	movq	%xmm0, 72(%rsp)
	movq	%xmm1, 64(%rsp)
	movss	72(%rsp), %xmm0
	movss	%xmm0, 56(%rsp)
	movss	76(%rsp), %xmm0
	movss	%xmm0, 60(%rsp)
	movss	64(%rsp), %xmm2
	movss	68(%rsp), %xmm3
	movq	56(%rsp), %xmm0
	movss	%xmm2, 12(%rsp)
	movss	%xmm3, 8(%rsp)
	call	__clogf@PLT
	movq	%xmm0, 48(%rsp)
	movss	8(%rsp), %xmm3
	movss	48(%rsp), %xmm0
	movss	12(%rsp), %xmm2
	movss	52(%rsp), %xmm1
	call	__mulsc3@PLT
	movq	%xmm0, 40(%rsp)
	movss	40(%rsp), %xmm0
	movss	%xmm0, 32(%rsp)
	movss	44(%rsp), %xmm0
	movss	%xmm0, 36(%rsp)
	movq	32(%rsp), %xmm0
	addq	$88, %rsp
	jmp	__cexpf@PLT
	.size	__cpowf, .-__cpowf
	.weak	cpowf32
	.set	cpowf32,__cpowf
	.weak	cpowf
	.set	cpowf,__cpowf
