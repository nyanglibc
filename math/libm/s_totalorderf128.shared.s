	.text
#APP
	.symver __totalorderf1280,totalorderf128@@GLIBC_2.31
	.symver __totalorder_compatf1281,totalorderf128@GLIBC_2.26
#NO_APP
	.p2align 4,,15
	.globl	__totalorderf128
	.type	__totalorderf128, @function
__totalorderf128:
	movdqa	(%rdi), %xmm0
	movdqa	(%rsi), %xmm1
	movaps	%xmm0, -40(%rsp)
	movq	-32(%rsp), %rdx
	movaps	%xmm1, -24(%rsp)
	movq	%rdx, %rdi
	sarq	$63, %rdi
	movq	-16(%rsp), %rax
	movq	%rdi, %rcx
	shrq	%rcx
	xorq	%rdx, %rcx
	movq	%rax, %rsi
	sarq	$63, %rsi
	movq	%rsi, %rdx
	shrq	%rdx
	xorq	%rax, %rdx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	jl	.L1
	movq	-40(%rsp), %rax
	xorq	%rax, %rdi
	movq	-24(%rsp), %rax
	xorq	%rax, %rsi
	cmpq	%rsi, %rdi
	setbe	%sil
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	sete	%al
	andl	%esi, %eax
.L1:
	rep ret
	.size	__totalorderf128, .-__totalorderf128
	.globl	__totalorderf1280
	.set	__totalorderf1280,__totalorderf128
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalorder_compatf128
	.type	__totalorder_compatf128, @function
__totalorder_compatf128:
	subq	$40, %rsp
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm1, (%rsp)
	call	__totalorderf128@PLT
	addq	$40, %rsp
	ret
	.size	__totalorder_compatf128, .-__totalorder_compatf128
	.globl	__totalorder_compatf1281
	.set	__totalorder_compatf1281,__totalorder_compatf128
