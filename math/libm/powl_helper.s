	.text
	.p2align 4,,15
	.globl	__powl_helper
	.type	__powl_helper, @function
__powl_helper:
	pushq	%rbx
	subq	$176, %rsp
	fldt	192(%rsp)
	fucomi	%st(0), %st
	jp	.L24
	fldz
	fucomip	%st(1), %st
	jbe	.L30
	fldt	208(%rsp)
	fabs
	flds	.LC1(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L35
	fstp	%st(0)
	xorl	%ebx, %ebx
.L6:
	fabs
.L4:
	leaq	156(%rsp), %rdi
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__frexpl@PLT
	fstpt	16(%rsp)
	popq	%rcx
	popq	%rsi
	fldt	(%rsp)
	fldt	.LC3(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L12
	movl	156(%rsp), %esi
.L13:
	fldt	(%rsp)
	fnstcw	142(%rsp)
	movzwl	142(%rsp), %eax
	leaq	powl_log_table(%rip), %rdi
	orb	$12, %ah
	movw	%ax, 140(%rsp)
	fdivrs	.LC4(%rip)
	fadds	.LC5(%rip)
	fldcw	140(%rsp)
	fistpl	136(%rsp)
	fldcw	142(%rsp)
	movl	136(%rsp), %eax
	leal	-24(%rax,%rax), %edx
	movslq	%edx, %rcx
	addl	$1, %edx
	salq	$4, %rcx
	movslq	%edx, %rdx
	fldt	(%rdi,%rcx)
	salq	$4, %rdx
	cmpl	$16, %eax
	fldt	(%rdi,%rdx)
	je	.L21
	movq	8(%rsp), %rdx
	movw	%dx, 168(%rsp)
	movq	(%rsp), %rdx
	movq	%rdx, %rcx
	andl	$-32, %edx
	shrq	$32, %rcx
	movl	%edx, 160(%rsp)
	movl	%ecx, 164(%rsp)
	fldt	160(%rsp)
	fldt	(%rsp)
	movl	%eax, (%rsp)
	fsub	%st(1), %st
	fildl	(%rsp)
	fmuls	.LC6(%rip)
	fmul	%st(2), %st
	fstpt	(%rsp)
	fdivp	%st, %st(1)
	fldl2e
	fmulp	%st, %st(1)
	fstpt	64(%rsp)
.L14:
	movl	$0, 160(%rsp)
	fld1
	fldt	(%rsp)
	fsubp	%st, %st(1)
	fstpt	(%rsp)
	movq	8(%rsp), %rax
	movw	%ax, 168(%rsp)
	movq	(%rsp), %rax
	shrq	$32, %rax
	xorw	%ax, %ax
	movl	%eax, 164(%rsp)
	movl	$5, %eax
	fldt	160(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fldt	(%rsp)
	fsub	%st(1), %st
	fstpt	48(%rsp)
	fld	%st(0)
	fadd	%st(3), %st
	fsubr	%st, %st(3)
	fxch	%st(3)
	fadd	%st(1), %st
	faddp	%st, %st(2)
	fld	%st(2)
	fadd	%st(2), %st
	fld	%st(1)
	fchs
	fmul	%st, %st(2)
	flds	.LC5(%rip)
	fmul	%st(3), %st
	fld	%st(0)
	fadd	%st(3), %st
	fld	%st(3)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fxch	%st(6)
	fsubp	%st, %st(3)
	fxch	%st(2)
	faddp	%st, %st(4)
	fxch	%st(1)
	faddp	%st, %st(3)
	fld	%st(3)
	fadd	%st(3), %st
	fxch	%st(2)
	fmul	%st(1), %st
	fld	%st(0)
	fmuls	.LC9(%rip)
	fld	%st(0)
	fadd	%st(4), %st
	fld	%st(4)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fxch	%st(6)
	fsubp	%st, %st(4)
	fxch	%st(3)
	faddp	%st, %st(4)
	fxch	%st(2)
	faddp	%st, %st(3)
	fld	%st(3)
	fadd	%st(3), %st
	fldt	.LC10(%rip)
	fmul	%st(3), %st
	fld	%st(0)
	fadd	%st(2), %st
	fld	%st(2)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fxch	%st(6)
	fsubp	%st, %st(2)
	fxch	%st(1)
	faddp	%st, %st(4)
	faddp	%st, %st(3)
	fld	%st(3)
	fadd	%st(3), %st
	fxch	%st(2)
	fmul	%st(1), %st
	fld	%st(0)
	fmuls	.LC11(%rip)
	fld	%st(0)
	fadd	%st(4), %st
	fld	%st(4)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fxch	%st(6)
	fsubp	%st, %st(4)
	fxch	%st(3)
	faddp	%st, %st(4)
	fxch	%st(3)
	faddp	%st, %st(2)
	fld	%st(3)
	fadd	%st(2), %st
	fld	%st(0)
	fstpt	(%rsp)
	fsubrp	%st, %st(4)
	fxch	%st(1)
	faddp	%st, %st(3)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L36:
	fstp	%st(0)
.L15:
	fmul	%st, %st(1)
	movl	%eax, 16(%rsp)
	addl	$1, %eax
	cmpl	$18, %eax
	fildl	16(%rsp)
	fdivr	%st(2), %st
	fldt	(%rsp)
	fadd	%st(1), %st
	fldt	(%rsp)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fxch	%st(4)
	faddp	%st, %st(1)
	fld	%st(3)
	fadd	%st(1), %st
	fld	%st(0)
	fstpt	(%rsp)
	fld	%st(0)
	fsubrp	%st, %st(5)
	fxch	%st(1)
	faddp	%st, %st(4)
	jne	.L36
	fstp	%st(2)
	fstp	%st(0)
	movq	8(%rsp), %rax
	movl	$0, 160(%rsp)
	subq	$16, %rsp
	movw	%ax, 184(%rsp)
	movq	16(%rsp), %rax
	movl	%esi, 16(%rsp)
	shrq	$32, %rax
	movl	%eax, 180(%rsp)
	fldt	176(%rsp)
	fldl	.LC12(%rip)
	fld	%st(1)
	fmul	%st(1), %st
	fxch	%st(2)
	fsubr	%st(3), %st
	faddp	%st, %st(4)
	fmulp	%st, %st(3)
	fldt	.LC13(%rip)
	fmulp	%st, %st(2)
	fxch	%st(2)
	faddp	%st, %st(1)
	fld	%st(1)
	fadd	%st(1), %st
	fld1
	fldt	48(%rsp)
	fadd	%st(1), %st
	fldt	64(%rsp)
	fdivp	%st, %st(1)
#APP
# 186 "../sysdeps/x86/fpu/powl_helper.c" 1
	fyl2xp1
# 0 "" 2
#NO_APP
	fld	%st(1)
	fadd	%st(1), %st
	fxch	%st(4)
	fsub	%st(2), %st
	faddp	%st, %st(3)
	fxch	%st(1)
	fsub	%st(3), %st
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fld	%st(1)
	fadd	%st(1), %st
	fldt	80(%rsp)
	fld	%st(0)
	fadd	%st(2), %st
	fld	%st(2)
	fsub	%st(1), %st
	faddp	%st, %st(2)
	fxch	%st(4)
	fsubp	%st, %st(2)
	fxch	%st(2)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fld	%st(1)
	fstpt	128(%rsp)
	fadd	%st, %st(1)
	fstpt	112(%rsp)
	fildl	16(%rsp)
	fld	%st(0)
	fstpt	96(%rsp)
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	80(%rsp)
	fstpt	16(%rsp)
	movq	24(%rsp), %rax
	movw	%ax, 184(%rsp)
	movq	16(%rsp), %rax
	shrq	$32, %rax
	movl	%eax, 180(%rsp)
	movq	232(%rsp), %rax
	fldt	176(%rsp)
	movw	%ax, 184(%rsp)
	movq	224(%rsp), %rax
	shrq	$32, %rax
	movl	%eax, 180(%rsp)
	fldt	176(%rsp)
	fld	%st(0)
	fstpt	48(%rsp)
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	64(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__roundl@PLT
	fld	%st(0)
	popq	%rax
	popq	%rdx
	fabs
	flds	.LC14(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L22
	fldt	(%rsp)
	fldt	80(%rsp)
	fsubp	%st, %st(1)
	fldt	64(%rsp)
	fadd	%st, %st(1)
	fldt	112(%rsp)
	fsubp	%st, %st(1)
	fldt	96(%rsp)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fldt	(%rsp)
	fldt	48(%rsp)
	fsubr	%st, %st(1)
	fxch	%st(2)
	faddp	%st, %st(1)
	fldt	208(%rsp)
	fmul	%st, %st(1)
	fldt	32(%rsp)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(2)
	faddp	%st, %st(1)
	fldt	16(%rsp)
	fsub	%st(2), %st
	faddp	%st, %st(1)
.L16:
#APP
# 228 "../sysdeps/x86/fpu/powl_helper.c" 1
	f2xm1
# 0 "" 2
#NO_APP
	fld1
	testb	%bl, %bl
	faddp	%st, %st(1)
	je	.L17
	fchs
.L17:
#APP
# 232 "../sysdeps/x86/fpu/powl_helper.c" 1
	fscale
# 0 "" 2
#NO_APP
	fstp	%st(1)
	fld	%st(0)
	fabs
	fldt	.LC15(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L32
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L32:
	addq	$176, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	flds	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jnb	.L7
	fstp	%st(1)
	fnstcw	142(%rsp)
	movzwl	142(%rsp), %eax
	orb	$12, %ah
	movw	%ax, 140(%rsp)
	fld	%st(0)
	fldcw	140(%rsp)
	fistpq	128(%rsp)
	fldcw	142(%rsp)
	movq	128(%rsp), %rbx
.L8:
	movq	%rbx, (%rsp)
	testq	%rbx, %rbx
	fildq	(%rsp)
	jns	.L10
	fadds	.LC1(%rip)
.L10:
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L24
	jne	.L24
	andl	$1, %ebx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L30:
	xorl	%ebx, %ebx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L22:
	fldz
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L12:
	fldt	(%rsp)
	movl	156(%rsp), %eax
	leal	-1(%rax), %esi
	fadd	%st(0), %st
	fstpt	(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	fldz
	fstpt	64(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L7:
	fnstcw	142(%rsp)
	movzwl	142(%rsp), %eax
	fsub	%st, %st(1)
	fxch	%st(1)
	orb	$12, %ah
	movw	%ax, 140(%rsp)
	movabsq	$-9223372036854775808, %rax
	fldcw	140(%rsp)
	fistpq	128(%rsp)
	fldcw	142(%rsp)
	movq	128(%rsp), %rbx
	xorq	%rax, %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L24:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	fldt	224(%rsp)
	subq	$16, %rsp
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	addq	$32, %rsp
	addq	$176, %rsp
	popq	%rbx
	ret
	.size	__powl_helper, .-__powl_helper
	.section	.rodata
	.align 32
	.type	powl_log_table, @object
	.size	powl_log_table, 416
powl_log_table:
	.long	2799473602
	.long	2471170185
	.long	16381
	.long	0
	.long	3937172154
	.long	2190553383
	.long	49083
	.long	0
	.long	1456413561
	.long	3567217124
	.long	16380
	.long	0
	.long	2161954255
	.long	3198646876
	.long	16315
	.long	0
	.long	1059315442
	.long	2294051857
	.long	16380
	.long	0
	.long	3524780484
	.long	3343638584
	.long	49083
	.long	0
	.long	4056953925
	.long	2217526700
	.long	16379
	.long	0
	.long	3452634303
	.long	4284939584
	.long	16312
	.long	0
	.long	0
	.long	0
	.long	32768
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	2333422348
	.long	4166092288
	.long	49146
	.long	0
	.long	860074902
	.long	3450053701
	.long	16311
	.long	0
	.long	1469525839
	.long	4046994289
	.long	49147
	.long	0
	.long	3923858908
	.long	2826960415
	.long	49082
	.long	0
	.long	1017028164
	.long	2952364933
	.long	49148
	.long	0
	.long	3989065753
	.long	3252641677
	.long	16315
	.long	0
	.long	3570470241
	.long	3833577020
	.long	49148
	.long	0
	.long	4147399004
	.long	3137592927
	.long	49082
	.long	0
	.long	1563455517
	.long	2335892829
	.long	49149
	.long	0
	.long	1195891728
	.long	3456803774
	.long	16314
	.long	0
	.long	3807777768
	.long	2735496720
	.long	49149
	.long	0
	.long	3342090590
	.long	3081993011
	.long	49084
	.long	0
	.long	3732944103
	.long	3117334453
	.long	49149
	.long	0
	.long	4225653422
	.long	2467626676
	.long	49082
	.long	0
	.long	4240596886
	.long	3482918757
	.long	49149
	.long	0
	.long	4208846822
	.long	2536020235
	.long	16316
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1602224128
	.align 4
.LC2:
	.long	1593835520
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	2863311530
	.long	2863311530
	.long	16382
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	1098907648
	.align 4
.LC5:
	.long	1056964608
	.align 4
.LC6:
	.long	1031798784
	.align 4
.LC9:
	.long	1051372032
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	2863311530
	.long	2863311530
	.long	16365
	.long	0
	.section	.rodata.cst4
	.align 4
.LC11:
	.long	1048576000
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC12:
	.long	1696595968
	.long	1073157447
	.section	.rodata.cst16
	.align 16
.LC13:
	.long	2098199969
	.long	3090145655
	.long	16350
	.long	0
	.section	.rodata.cst4
	.align 4
.LC14:
	.long	1182853120
	.section	.rodata.cst16
	.align 16
.LC15:
	.long	0
	.long	2147483648
	.long	1
	.long	0
