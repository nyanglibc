	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__catanf
	.type	__catanf, @function
__catanf:
	subq	$88, %rsp
	movq	%xmm0, 72(%rsp)
	movss	.LC2(%rip), %xmm4
	movss	72(%rsp), %xmm2
	movaps	%xmm2, %xmm3
	movss	76(%rsp), %xmm7
	movaps	%xmm7, %xmm6
	andps	%xmm4, %xmm3
	andps	%xmm4, %xmm6
	ucomiss	%xmm3, %xmm3
	jp	.L98
	ucomiss	.LC3(%rip), %xmm3
	jbe	.L99
	ucomiss	%xmm6, %xmm6
	jp	.L53
.L53:
	movss	.LC6(%rip), %xmm1
	andps	%xmm1, %xmm2
	andps	%xmm1, %xmm7
	orps	.LC5(%rip), %xmm2
	movaps	%xmm7, %xmm0
	movaps	%xmm0, %xmm3
.L1:
	movss	%xmm2, 64(%rsp)
	movss	%xmm3, 68(%rsp)
	movq	64(%rsp), %xmm0
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	ucomiss	.LC4(%rip), %xmm3
	jnb	.L9
	pxor	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm2
	jp	.L9
	je	.L100
.L9:
	ucomiss	%xmm6, %xmm6
	jp	.L64
	ucomiss	.LC3(%rip), %xmm6
	ja	.L47
.L48:
	ucomiss	.LC7(%rip), %xmm3
	jnb	.L15
	ucomiss	.LC7(%rip), %xmm6
	jnb	.L15
	ucomiss	%xmm3, %xmm6
	ja	.L57
	movaps	%xmm6, %xmm9
	movaps	%xmm3, %xmm0
.L23:
	movss	.LC11(%rip), %xmm1
	ucomiss	%xmm9, %xmm1
	movss	.LC8(%rip), %xmm8
	jbe	.L81
	movaps	%xmm8, %xmm1
	pxor	%xmm5, %xmm5
	subss	%xmm0, %xmm1
	addss	%xmm8, %xmm0
	mulss	%xmm0, %xmm1
	ucomiss	%xmm5, %xmm1
	jp	.L26
	movd	%xmm1, %eax
	movd	%xmm5, %edx
	cmove	%edx, %eax
	movl	%eax, 52(%rsp)
	movss	52(%rsp), %xmm1
.L26:
	movaps	%xmm2, %xmm0
	movss	%xmm8, 60(%rsp)
	movaps	%xmm4, 32(%rsp)
	addss	%xmm2, %xmm0
	movss	%xmm7, 56(%rsp)
	movss	%xmm3, 16(%rsp)
	movss	%xmm6, (%rsp)
	movss	%xmm2, 52(%rsp)
	call	__ieee754_atan2f@PLT
	movss	(%rsp), %xmm6
	movss	60(%rsp), %xmm8
	movaps	%xmm0, %xmm5
	ucomiss	%xmm8, %xmm6
	movss	.LC9(%rip), %xmm9
	mulss	%xmm9, %xmm5
	movss	52(%rsp), %xmm2
	movss	16(%rsp), %xmm3
	movss	56(%rsp), %xmm7
	movaps	32(%rsp), %xmm4
	jp	.L33
	jne	.L33
	movss	.LC13(%rip), %xmm0
	ucomiss	%xmm3, %xmm0
	ja	.L101
.L33:
	ucomiss	.LC13(%rip), %xmm3
	pxor	%xmm0, %xmm0
	jb	.L36
	mulss	%xmm2, %xmm2
	movaps	%xmm2, %xmm0
.L36:
	movaps	%xmm7, %xmm1
	movaps	%xmm7, %xmm3
	movaps	%xmm4, (%rsp)
	addss	%xmm8, %xmm1
	subss	%xmm8, %xmm3
	movss	%xmm5, 52(%rsp)
	mulss	%xmm1, %xmm1
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	addss	%xmm1, %xmm0
	divss	%xmm3, %xmm0
	ucomiss	%xmm0, %xmm9
	jbe	.L85
	call	__ieee754_logf@PLT
.L91:
	mulss	.LC10(%rip), %xmm0
	movss	52(%rsp), %xmm5
	movaps	%xmm5, %xmm2
	movaps	(%rsp), %xmm4
	movaps	%xmm0, %xmm3
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L15:
	movaps	%xmm2, %xmm5
	movss	.LC8(%rip), %xmm0
	andps	.LC6(%rip), %xmm5
	ucomiss	%xmm3, %xmm0
	orps	.LC5(%rip), %xmm5
	jnb	.L102
	ucomiss	%xmm6, %xmm0
	jb	.L80
	divss	%xmm2, %xmm7
	movaps	%xmm7, %xmm0
	divss	%xmm2, %xmm0
	movaps	%xmm5, %xmm2
	movaps	%xmm0, %xmm3
.L20:
	movaps	%xmm5, %xmm6
	movss	.LC4(%rip), %xmm1
	andps	%xmm4, %xmm6
	ucomiss	%xmm6, %xmm1
	jbe	.L40
	mulss	%xmm5, %xmm5
.L40:
	andps	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.L1
	mulss	%xmm0, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L102:
	divss	%xmm7, %xmm0
	movaps	%xmm5, %xmm2
	movaps	%xmm0, %xmm3
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L100:
	ucomiss	%xmm6, %xmm6
	jp	.L64
	ucomiss	.LC3(%rip), %xmm6
	ja	.L47
	ucomiss	.LC4(%rip), %xmm6
	jnb	.L48
	ucomiss	%xmm0, %xmm7
	jp	.L48
	je	.L93
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L57:
	movaps	%xmm3, %xmm9
	movaps	%xmm6, %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L80:
	movss	.LC9(%rip), %xmm1
	mulss	%xmm1, %xmm2
	movss	%xmm5, (%rsp)
	mulss	%xmm7, %xmm1
	movss	%xmm7, 52(%rsp)
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_hypotf@PLT
	movss	52(%rsp), %xmm7
	divss	%xmm0, %xmm7
	movaps	%xmm0, %xmm1
	movss	(%rsp), %xmm5
	movaps	%xmm5, %xmm2
	movaps	16(%rsp), %xmm4
	movaps	%xmm7, %xmm0
	divss	%xmm1, %xmm0
	mulss	.LC10(%rip), %xmm0
	movaps	%xmm0, %xmm3
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L81:
	ucomiss	%xmm8, %xmm0
	jb	.L82
	movaps	%xmm8, %xmm1
	mulss	%xmm9, %xmm9
	subss	%xmm0, %xmm1
	addss	%xmm8, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	subss	%xmm9, %xmm1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L98:
	ucomiss	%xmm6, %xmm6
	jp	.L64
	ucomiss	.LC3(%rip), %xmm6
	ja	.L103
	ucomiss	.LC4(%rip), %xmm6
	movss	.LC0(%rip), %xmm2
	movaps	%xmm2, %xmm3
	jnb	.L1
	ucomiss	.LC1(%rip), %xmm7
	jp	.L64
	jne	.L64
	andps	.LC6(%rip), %xmm7
.L93:
	movaps	%xmm7, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L64:
	movss	.LC0(%rip), %xmm2
	movaps	%xmm2, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L85:
	mulss	.LC16(%rip), %xmm7
	movaps	%xmm7, %xmm0
	divss	%xmm3, %xmm0
	call	__log1pf@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L82:
	ucomiss	.LC12(%rip), %xmm0
	jnb	.L30
	ucomiss	.LC9(%rip), %xmm9
	jnb	.L30
	movaps	%xmm8, %xmm5
	mulss	%xmm9, %xmm9
	subss	%xmm0, %xmm5
	addss	%xmm8, %xmm0
	mulss	%xmm5, %xmm0
	movaps	%xmm0, %xmm1
	subss	%xmm9, %xmm1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L101:
	andps	.LC6(%rip), %xmm7
	movss	%xmm5, (%rsp)
	movaps	%xmm3, %xmm0
	movaps	%xmm4, 16(%rsp)
	orps	.LC14(%rip), %xmm7
	movss	%xmm7, 52(%rsp)
	call	__ieee754_logf@PLT
	movss	.LC15(%rip), %xmm1
	subss	%xmm0, %xmm1
	movss	52(%rsp), %xmm0
	movss	(%rsp), %xmm5
	movaps	%xmm5, %xmm2
	movaps	16(%rsp), %xmm4
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L30:
	movaps	%xmm9, %xmm1
	movss	%xmm8, 60(%rsp)
	movaps	%xmm4, 32(%rsp)
	movss	%xmm7, 56(%rsp)
	movss	%xmm2, 16(%rsp)
	movss	%xmm3, (%rsp)
	movss	%xmm6, 52(%rsp)
	call	__x2y2m1f@PLT
	movaps	%xmm0, %xmm1
	movss	52(%rsp), %xmm6
	xorps	.LC6(%rip), %xmm1
	movss	(%rsp), %xmm3
	movss	16(%rsp), %xmm2
	movss	56(%rsp), %xmm7
	movaps	32(%rsp), %xmm4
	movss	60(%rsp), %xmm8
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L47:
	movss	.LC6(%rip), %xmm1
	andps	%xmm1, %xmm2
	orps	.LC5(%rip), %xmm2
.L50:
	andps	%xmm1, %xmm7
	movaps	%xmm7, %xmm3
	jmp	.L1
.L103:
	movss	.LC0(%rip), %xmm2
	movss	.LC6(%rip), %xmm1
	jmp	.L50
	.size	__catanf, .-__catanf
	.weak	catanf32
	.set	catanf32,__catanf
	.weak	catanf
	.set	catanf,__catanf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.align 4
.LC1:
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095039
	.align 4
.LC4:
	.long	8388608
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	1070141403
	.long	0
	.long	0
	.long	0
	.align 16
.LC6:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1291845632
	.align 4
.LC8:
	.long	1065353216
	.align 4
.LC9:
	.long	1056964608
	.align 4
.LC10:
	.long	1048576000
	.align 4
.LC11:
	.long	864026624
	.align 4
.LC12:
	.long	1061158912
	.align 4
.LC13:
	.long	679477248
	.section	.rodata.cst16
	.align 16
.LC14:
	.long	1056964608
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC15:
	.long	1060205080
	.align 4
.LC16:
	.long	1082130432
