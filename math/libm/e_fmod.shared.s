	.text
#APP
	.symver __ieee754_fmod,__fmod_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_fmod
	.type	__ieee754_fmod, @function
__ieee754_fmod:
	movq	%xmm0, %rsi
	movq	%xmm1, %rdx
	movabsq	$9223372036854775807, %rcx
	movq	%xmm0, %rax
	movabsq	$-9223372036854775808, %r8
	andq	%rsi, %r8
	andq	%rcx, %rax
	andq	%rcx, %rdx
	movq	%rdx, %r9
	je	.L2
	movabsq	$9218868437227405311, %rdx
	cmpq	%rdx, %rax
	ja	.L2
	addq	$1, %rdx
	cmpq	%rdx, %r9
	jg	.L2
	cmpq	%r9, %rax
	jle	.L33
	movq	%rax, %rdx
	movabsq	$4503599627370495, %rcx
	sarq	$52, %rdx
	subl	$1023, %edx
	cmpq	%rcx, %rax
	jbe	.L34
.L8:
	movq	%r9, %rdi
	movabsq	$4503599627370495, %rcx
	sarq	$52, %rdi
	subl	$1023, %edi
	cmpq	%rcx, %r9
	jle	.L35
.L11:
	cmpl	$-1022, %edx
	jl	.L12
	movabsq	$4503599627370495, %rsi
	andq	%rax, %rsi
	movabsq	$4503599627370496, %rax
	orq	%rax, %rsi
.L13:
	cmpl	$-1022, %edi
	jl	.L14
	movabsq	$4503599627370495, %rcx
	movabsq	$4503599627370496, %rax
	andq	%r9, %rcx
	orq	%rax, %rcx
.L15:
	subl	%edi, %edx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	addq	%rsi, %rsi
	testq	%rax, %rax
	js	.L18
	je	.L22
	leaq	(%rax,%rax), %rsi
.L18:
	subl	$1, %edx
.L16:
	movq	%rsi, %rax
	subq	%rcx, %rax
	testl	%edx, %edx
	jne	.L20
	testq	%rax, %rax
	cmovs	%rsi, %rax
	testq	%rax, %rax
	je	.L22
	movabsq	$4503599627370495, %rcx
	cmpq	%rcx, %rax
	movq	%rcx, %rdx
	ja	.L24
	.p2align 4,,10
	.p2align 3
.L23:
	addq	%rax, %rax
	subl	$1, %edi
	cmpq	%rdx, %rax
	jle	.L23
.L24:
	cmpl	$-1022, %edi
	jl	.L25
	addl	$1023, %edi
	movabsq	$-4503599627370496, %rdx
	salq	$52, %rdi
	addq	%rdx, %rax
	orq	%rdi, %rax
	orq	%r8, %rax
	movq	%rax, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	jl	.L1
.L22:
	leaq	Zero(%rip), %rax
	shrq	$63, %r8
	movsd	(%rax,%r8,8), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	mulsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm0
	ret
.L25:
	movl	$-1022, %ecx
	subl	%edi, %ecx
	sarq	%cl, %rax
	orq	%r8, %rax
	movq	%rax, -8(%rsp)
	movq	-8(%rsp), %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$-1022, %ecx
	subl	%edi, %ecx
	salq	%cl, %r9
	movq	%r9, %rcx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rax, %rcx
	movl	$-1022, %edx
	salq	$11, %rcx
	.p2align 4,,10
	.p2align 3
.L7:
	addq	%rcx, %rcx
	subl	$1, %edx
	testq	%rcx, %rcx
	jg	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r9, %rcx
	movl	$-1022, %edi
	salq	$11, %rcx
	.p2align 4,,10
	.p2align 3
.L10:
	addq	%rcx, %rcx
	subl	$1, %edi
	testq	%rcx, %rcx
	jg	.L10
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$-1022, %ecx
	subl	%edx, %ecx
	salq	%cl, %rax
	movq	%rax, %rsi
	jmp	.L13
	.size	__ieee754_fmod, .-__ieee754_fmod
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	Zero, @object
	.size	Zero, 16
Zero:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
