.globl __ieee754_remainderl
.type __ieee754_remainderl,@function
.align 1<<4
__ieee754_remainderl:
 fldt 24(%rsp)
 fldt 8(%rsp)
1: fprem1
 fstsw %ax
 testl $0x400,%eax
 jnz 1b
 fstp %st(1)
 ret
.size __ieee754_remainderl,.-__ieee754_remainderl
