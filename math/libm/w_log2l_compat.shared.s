	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__log2l
	.type	__log2l, @function
__log2l:
	subq	$24, %rsp
	fldt	32(%rsp)
	fldz
	fucomi	%st(1), %st
	jnb	.L9
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L11:
	fstp	%st(0)
.L2:
	fstpt	32(%rsp)
	addq	$24, %rsp
	jmp	__ieee754_log2l@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L11
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L3
	je	.L10
.L3:
	fstpt	(%rsp)
	movl	$1, %edi
	call	__GI_feraiseexcept
	subq	$32, %rsp
	movl	$249, %edi
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	fstpt	(%rsp)
	movl	$4, %edi
	call	__GI_feraiseexcept
	subq	$32, %rsp
	movl	$248, %edi
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L1
	.size	__log2l, .-__log2l
	.weak	log2f64x
	.set	log2f64x,__log2l
	.weak	log2l
	.set	log2l,__log2l
