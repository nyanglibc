	.text
	.p2align 4,,15
	.globl	__ufromfp
	.type	__ufromfp, @function
__ufromfp:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$64, %esi
	ja	.L35
	testl	%esi, %esi
	jne	.L2
.L31:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$64, %esi
.L2:
	movq	%xmm0, %r8
	movabsq	$9223372036854775807, %rcx
	andq	%r8, %rcx
	je	.L28
	shrq	$52, %rcx
	testq	%r8, %r8
	leal	-1023(%rcx), %r9d
	js	.L5
	leal	-1(%rsi), %r10d
	cmpl	%r10d, %r9d
	jg	.L26
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdx
	andq	%r8, %rax
	orq	%rdx, %rax
	cmpl	$51, %r9d
	jle	.L8
	subl	$1075, %ecx
	xorl	%ebp, %ebp
	salq	%cl, %rax
.L9:
	cmpl	$1, %edi
	je	.L39
	jle	.L75
	cmpl	$3, %edi
	je	.L40
	xorl	%edx, %edx
	cmpl	$4, %edi
	je	.L23
	.p2align 4,,10
	.p2align 3
.L10:
	testq	%r8, %r8
	jns	.L19
.L16:
	testq	%rax, %rax
	je	.L1
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L28:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%r11d, %r11d
.L72:
	testl	%edi, %edi
	jne	.L10
	testq	%r8, %r8
	js	.L16
	testb	%r11b, %r11b
	jne	.L42
	testb	%bpl, %bpl
	je	.L19
.L42:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$63, %r10d
	je	.L76
	leal	1(%r10), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rax, %rdx
	jne	.L1
.L26:
	movl	$1, %edi
	movl	%esi, 12(%rsp)
	call	__GI_feraiseexcept
	movl	12(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	je	.L77
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%r9d, %r9d
	jns	.L31
	movabsq	$4503599627370495, %rax
	movabsq	$4503599627370496, %rdx
	movl	$-1, %r10d
	andq	%r8, %rax
	orq	%rdx, %rax
.L8:
	cmpl	$-1, %r9d
	jl	.L36
	movl	$51, %ecx
	movl	$1, %edx
	subl	%r9d, %ecx
	salq	%cl, %rdx
	movl	$52, %ecx
	movq	%rdx, %rbx
	andq	%rax, %rbx
	setne	%r11b
	subq	$1, %rdx
	andq	%rax, %rdx
	setne	%bpl
	subl	%r9d, %ecx
	shrq	%cl, %rax
	cmpl	$1, %edi
	je	.L11
	jle	.L72
	cmpl	$3, %edi
	je	.L14
	cmpl	$4, %edi
	jne	.L10
	testq	%rbx, %rbx
	je	.L37
	movq	%rax, %rcx
	andl	$1, %ecx
	orq	%rdx, %rcx
	setne	%dl
	movzbl	%dl, %edx
.L23:
	addq	%rdx, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r11d, %r11d
.L22:
	addq	%r11, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%r11d, %r11d
.L11:
	testq	%r8, %r8
	jns	.L19
	testb	%r11b, %r11b
	jne	.L43
	testb	%bpl, %bpl
	je	.L16
.L43:
	addq	$1, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %ebp
	xorl	%eax, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$63, %r9d
	jne	.L1
	testq	%rax, %rax
	jne	.L1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L77:
	movq	$-1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	%r11b, %r11d
	jmp	.L22
.L37:
	xorl	%edx, %edx
	jmp	.L23
	.size	__ufromfp, .-__ufromfp
	.weak	ufromfpf32x
	.set	ufromfpf32x,__ufromfp
	.weak	ufromfpf64
	.set	ufromfpf64,__ufromfp
	.weak	ufromfp
	.set	ufromfp,__ufromfp
