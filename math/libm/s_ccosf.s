	.text
	.p2align 4,,15
	.globl	__ccosf
	.type	__ccosf, @function
__ccosf:
	movq	%xmm0, -16(%rsp)
	movss	-12(%rsp), %xmm0
	xorps	.LC0(%rip), %xmm0
	movss	%xmm0, -24(%rsp)
	movss	-16(%rsp), %xmm0
	movss	%xmm0, -20(%rsp)
	movq	-24(%rsp), %xmm0
	jmp	__ccoshf@PLT
	.size	__ccosf, .-__ccosf
	.weak	ccosf32
	.set	ccosf32,__ccosf
	.weak	ccosf
	.set	ccosf,__ccosf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483648
	.long	0
	.long	0
	.long	0
