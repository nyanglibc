	.text
	.p2align 4,,15
	.globl	__j1f
	.type	__j1f, @function
__j1f:
	jmp	__ieee754_j1f@PLT
	.size	__j1f, .-__j1f
	.weak	j1f32
	.set	j1f32,__j1f
	.weak	j1f
	.set	j1f,__j1f
	.p2align 4,,15
	.globl	__y1f
	.type	__y1f, @function
__y1f:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L11
.L4:
	jmp	__ieee754_y1f@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	ucomiss	%xmm1, %xmm0
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y1f, .-__y1f
	.weak	y1f32
	.set	y1f32,__y1f
	.weak	y1f
	.set	y1f,__y1f
