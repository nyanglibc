	.text
	.p2align 4,,15
	.globl	__tanl
	.type	__tanl, @function
__tanl:
	subq	$40, %rsp
	movq	56(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16382, %ax
	jle	.L14
	cmpw	$32767, %ax
	jne	.L4
	movq	48(%rsp), %rax
	testl	%eax, %eax
	jne	.L5
	shrq	$32, %rax
	cmpl	$-2147483648, %eax
	jne	.L5
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L5:
	fldt	48(%rsp)
	addq	$40, %rsp
	fsub	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rsp, %rdi
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__ieee754_rem_pio2l@PLT
	addl	%eax, %eax
	pushq	40(%rsp)
	movl	$1, %edi
	pushq	40(%rsp)
	andl	$2, %eax
	pushq	40(%rsp)
	pushq	40(%rsp)
	subl	%eax, %edi
	call	__kernel_tanl@PLT
	addq	$48, %rsp
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	pushq	$0
	pushq	$0
	movl	$1, %edi
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__kernel_tanl@PLT
	addq	$32, %rsp
	addq	$40, %rsp
	ret
	.size	__tanl, .-__tanl
	.weak	tanf64x
	.set	tanf64x,__tanl
	.weak	tanl
	.set	tanl,__tanl
