	.text
	.p2align 4,,15
	.globl	__signbitf128
	.type	__signbitf128, @function
__signbitf128:
	movmskps	%xmm0, %eax
	andl	$8, %eax
	ret
	.size	__signbitf128, .-__signbitf128
