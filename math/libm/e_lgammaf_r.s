	.text
	.p2align 4,,15
	.globl	__ieee754_lgammaf_r
	.type	__ieee754_lgammaf_r, @function
__ieee754_lgammaf_r:
	pushq	%rbp
	pushq	%rbx
	movd	%xmm0, %ebx
	movaps	%xmm0, %xmm3
	andl	$2147483647, %ebx
	subq	$72, %rsp
	movl	$1, (%rdi)
	cmpl	$2139095039, %ebx
	jg	.L65
	testl	%ebx, %ebx
	movd	%xmm0, %ebp
	je	.L66
	cmpl	$813694975, %ebx
	jle	.L67
	testl	%ebp, %ebp
	js	.L68
	leal	-1065353216(%rbx), %eax
	pxor	%xmm1, %xmm1
	testl	$-8388609, %eax
	jne	.L47
.L1:
	addq	$72, %rsp
	movaps	%xmm1, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	cmpl	$1258291199, %ebx
	jg	.L69
	leal	-1073741825(%rbx), %eax
	cmpl	$24117246, %eax
	jbe	.L70
	cmpl	$1048575999, %ebx
	jle	.L71
	movss	.LC3(%rip), %xmm1
	movss	.LC2(%rip), %xmm4
	movss	.LC5(%rip), %xmm5
	xorps	%xmm1, %xmm0
	movaps	%xmm4, %xmm7
	movaps	%xmm0, %xmm6
	movaps	%xmm0, %xmm2
	andps	%xmm4, %xmm6
	ucomiss	%xmm6, %xmm5
	ja	.L72
.L13:
	ucomiss	%xmm2, %xmm0
	jp	.L54
	je	.L59
.L54:
	mulss	.LC6(%rip), %xmm0
	movaps	%xmm4, %xmm7
	movaps	%xmm0, %xmm6
	movaps	%xmm0, %xmm2
	andps	%xmm4, %xmm6
	ucomiss	%xmm6, %xmm5
	jbe	.L16
	cvttss2si	%xmm0, %eax
	pxor	%xmm6, %xmm6
	movss	.LC1(%rip), %xmm2
	andnps	%xmm0, %xmm7
	cvtsi2ss	%eax, %xmm6
	movaps	%xmm6, %xmm5
	cmpnless	%xmm0, %xmm5
	andps	%xmm2, %xmm5
	subss	%xmm5, %xmm6
	movaps	%xmm6, %xmm2
	orps	%xmm7, %xmm2
.L16:
	subss	%xmm2, %xmm0
	movss	.LC7(%rip), %xmm2
	addss	%xmm0, %xmm0
	mulss	%xmm0, %xmm2
	cvttss2si	%xmm2, %eax
.L17:
	cmpl	$6, %eax
	ja	.L18
	leaq	.L20(%rip), %rdx
	movl	%eax, %eax
	movq	%rdi, 24(%rsp)
	movaps	%xmm1, 48(%rsp)
	movslq	(%rdx,%rax,4), %rax
	movaps	%xmm4, 32(%rsp)
	movss	%xmm3, 20(%rsp)
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L20:
	.long	.L63-.L20
	.long	.L21-.L20
	.long	.L21-.L20
	.long	.L22-.L20
	.long	.L22-.L20
	.long	.L23-.L20
	.long	.L23-.L20
	.text
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$1073741823, %ebx
	jle	.L73
	cmpl	$1090519039, %ebx
	jg	.L39
	cvttss2si	%xmm3, %eax
	pxor	%xmm0, %xmm0
	movss	.LC58(%rip), %xmm1
	movss	.LC1(%rip), %xmm2
	cvtsi2ss	%eax, %xmm0
	subl	$3, %eax
	cmpl	$4, %eax
	subss	%xmm0, %xmm3
	movss	.LC52(%rip), %xmm0
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC53(%rip), %xmm0
	addss	.LC59(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC54(%rip), %xmm0
	addss	.LC60(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC55(%rip), %xmm0
	addss	.LC61(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC56(%rip), %xmm0
	addss	.LC62(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC57(%rip), %xmm0
	addss	.LC63(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	subss	.LC17(%rip), %xmm0
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm0
	divss	%xmm1, %xmm0
	movss	.LC6(%rip), %xmm1
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	ja	.L38
	leaq	.L41(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L41:
	.long	.L49-.L41
	.long	.L42-.L41
	.long	.L51-.L41
	.long	.L52-.L41
	.long	.L45-.L41
	.text
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$1063675494, %ebx
	jle	.L74
	cmpl	$1071490583, %ebx
	jg	.L75
	cmpl	$1067296287, %ebx
	jle	.L37
	subss	.LC11(%rip), %xmm3
	pxor	%xmm0, %xmm0
.L35:
	movaps	%xmm3, %xmm1
	movss	.LC24(%rip), %xmm2
	mulss	%xmm3, %xmm1
	movaps	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	mulss	%xmm4, %xmm2
	subss	.LC25(%rip), %xmm2
	mulss	%xmm4, %xmm2
	addss	.LC26(%rip), %xmm2
	mulss	%xmm4, %xmm2
	subss	.LC27(%rip), %xmm2
	mulss	%xmm4, %xmm2
	addss	.LC28(%rip), %xmm2
	mulss	%xmm1, %xmm2
	movss	.LC29(%rip), %xmm1
	mulss	%xmm4, %xmm1
	subss	.LC30(%rip), %xmm1
	mulss	%xmm4, %xmm1
	addss	.LC31(%rip), %xmm1
	mulss	%xmm4, %xmm1
	subss	.LC32(%rip), %xmm1
	mulss	%xmm4, %xmm1
	addss	.LC33(%rip), %xmm1
	mulss	%xmm3, %xmm1
	movss	.LC34(%rip), %xmm3
	mulss	%xmm4, %xmm3
	addss	.LC35(%rip), %xmm3
	mulss	%xmm4, %xmm3
	subss	.LC36(%rip), %xmm3
	mulss	%xmm4, %xmm3
	addss	.LC37(%rip), %xmm3
	mulss	%xmm4, %xmm3
	subss	.LC38(%rip), %xmm3
	addss	%xmm3, %xmm1
	movss	.LC39(%rip), %xmm3
	mulss	%xmm4, %xmm1
	subss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	movaps	%xmm2, %xmm1
	subss	.LC40(%rip), %xmm1
	addss	%xmm0, %xmm1
	.p2align 4,,10
	.p2align 3
.L38:
	testl	%ebp, %ebp
	jns	.L1
.L29:
	movss	(%rsp), %xmm6
	addq	$72, %rsp
	subss	%xmm1, %xmm6
	popq	%rbx
	popq	%rbp
	movaps	%xmm6, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	andps	.LC2(%rip), %xmm3
	movaps	%xmm3, %xmm1
	divss	.LC0(%rip), %xmm1
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	mulss	%xmm0, %xmm3
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	movaps	%xmm3, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	testl	%ebp, %ebp
	jns	.L5
	movl	$-1, (%rdi)
.L5:
	andps	.LC2(%rip), %xmm3
	movss	.LC1(%rip), %xmm1
	divss	%xmm3, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	testl	%ebp, %ebp
	js	.L76
	call	__ieee754_logf@PLT
	movaps	%xmm0, %xmm1
	xorps	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L70:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	jmp	__lgamma_negf@PLT
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$1283457023, %ebx
	movaps	%xmm3, %xmm0
	movss	%xmm3, 20(%rsp)
	jg	.L46
	call	__ieee754_logf@PLT
	movss	.LC1(%rip), %xmm5
	movaps	%xmm5, %xmm1
	movss	20(%rsp), %xmm3
	movss	.LC67(%rip), %xmm2
	subss	%xmm5, %xmm0
	divss	%xmm3, %xmm1
	subss	.LC6(%rip), %xmm3
	movaps	%xmm1, %xmm4
	mulss	%xmm1, %xmm4
	mulss	%xmm4, %xmm2
	addss	.LC68(%rip), %xmm2
	mulss	%xmm4, %xmm2
	subss	.LC69(%rip), %xmm2
	mulss	%xmm4, %xmm2
	addss	.LC70(%rip), %xmm2
	mulss	%xmm4, %xmm2
	subss	.LC71(%rip), %xmm2
	mulss	%xmm4, %xmm2
	addss	.LC72(%rip), %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	addss	.LC73(%rip), %xmm2
	addss	%xmm2, %xmm1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L75:
	movss	.LC9(%rip), %xmm0
	subss	%xmm3, %xmm0
	movaps	%xmm0, %xmm3
	pxor	%xmm0, %xmm0
.L33:
	movaps	%xmm3, %xmm4
	movss	.LC12(%rip), %xmm1
	movss	.LC18(%rip), %xmm2
	mulss	%xmm3, %xmm4
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm2
	addss	.LC13(%rip), %xmm1
	addss	.LC19(%rip), %xmm2
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm2
	addss	.LC14(%rip), %xmm1
	addss	.LC20(%rip), %xmm2
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm2
	addss	.LC15(%rip), %xmm1
	addss	.LC21(%rip), %xmm2
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm2
	addss	.LC16(%rip), %xmm1
	addss	.LC22(%rip), %xmm2
	mulss	%xmm4, %xmm1
	mulss	%xmm4, %xmm2
	addss	.LC17(%rip), %xmm1
	addss	.LC23(%rip), %xmm2
	mulss	%xmm3, %xmm1
	mulss	%xmm4, %xmm2
	mulss	.LC6(%rip), %xmm3
	addss	%xmm2, %xmm1
	subss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L46:
	call	__ieee754_logf@PLT
	subss	.LC1(%rip), %xmm0
	movss	20(%rsp), %xmm3
	mulss	%xmm3, %xmm0
	movaps	%xmm0, %xmm1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L72:
	cvttss2si	%xmm0, %eax
	pxor	%xmm6, %xmm6
	movss	.LC1(%rip), %xmm2
	andnps	%xmm0, %xmm7
	cvtsi2ss	%eax, %xmm6
	movaps	%xmm6, %xmm8
	cmpnless	%xmm0, %xmm8
	andps	%xmm2, %xmm8
	subss	%xmm8, %xmm6
	movaps	%xmm6, %xmm2
	orps	%xmm7, %xmm2
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L59:
	subss	%xmm3, %xmm5
	pxor	%xmm0, %xmm0
	movd	%xmm5, %eax
	andl	$1, %eax
	cvtsi2ss	%eax, %xmm0
	sall	$2, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L51:
	movaps	%xmm2, %xmm0
.L43:
	movss	.LC7(%rip), %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
.L42:
	movss	.LC66(%rip), %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
.L40:
	addss	.LC9(%rip), %xmm3
	movss	%xmm1, 20(%rsp)
	mulss	%xmm3, %xmm0
	call	__ieee754_logf@PLT
	movss	20(%rsp), %xmm1
	addss	%xmm0, %xmm1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L49:
	movaps	%xmm2, %xmm0
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L52:
	movaps	%xmm2, %xmm0
.L44:
	movss	.LC65(%rip), %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm2, %xmm0
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L45:
	movss	.LC64(%rip), %xmm0
	addss	%xmm3, %xmm0
	jmp	.L44
.L22:
	movss	.LC1(%rip), %xmm2
	subss	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
.L63:
	movss	.LC4(%rip), %xmm5
	mulss	%xmm5, %xmm0
	movss	%xmm5, (%rsp)
	call	__sinf@PLT
.L62:
	movq	24(%rsp), %rdi
	movss	(%rsp), %xmm5
	movaps	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm1
	movss	20(%rsp), %xmm3
.L24:
	xorps	%xmm0, %xmm1
.L12:
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jp	.L25
	jne	.L25
	andps	%xmm4, %xmm1
	movaps	%xmm1, %xmm0
	movss	.LC1(%rip), %xmm1
	divss	%xmm0, %xmm1
	jmp	.L1
.L21:
	movss	.LC6(%rip), %xmm2
	subss	%xmm0, %xmm2
	movss	.LC4(%rip), %xmm5
	movss	%xmm5, (%rsp)
	movaps	%xmm2, %xmm0
	mulss	%xmm5, %xmm0
	call	__cosf@PLT
	jmp	.L62
.L23:
	subss	.LC8(%rip), %xmm0
	movss	.LC4(%rip), %xmm5
	movss	%xmm5, (%rsp)
	mulss	%xmm5, %xmm0
	call	__cosf@PLT
	movaps	48(%rsp), %xmm1
	movq	24(%rsp), %rdi
	xorps	%xmm1, %xmm0
	movss	(%rsp), %xmm5
	movss	20(%rsp), %xmm3
	movaps	32(%rsp), %xmm4
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L74:
	movaps	%xmm3, %xmm0
	movss	%xmm3, 20(%rsp)
	call	__ieee754_logf@PLT
	cmpl	$1060850207, %ebx
	xorps	.LC3(%rip), %xmm0
	movss	20(%rsp), %xmm3
	jle	.L32
	movss	.LC1(%rip), %xmm1
	subss	%xmm3, %xmm1
	movaps	%xmm1, %xmm3
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L76:
	movss	.LC3(%rip), %xmm1
	movl	$-1, (%rdi)
	xorps	%xmm1, %xmm3
	movaps	%xmm1, (%rsp)
	movaps	%xmm3, %xmm0
	call	__ieee754_logf@PLT
	movaps	(%rsp), %xmm1
	xorps	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movaps	%xmm3, %xmm0
	movss	%xmm2, 48(%rsp)
	movq	%rdi, 32(%rsp)
	movss	%xmm3, 24(%rsp)
	mulss	%xmm1, %xmm0
	movss	%xmm1, 20(%rsp)
	andps	%xmm4, %xmm0
	divss	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	call	__ieee754_logf@PLT
	movss	20(%rsp), %xmm1
	movq	32(%rsp), %rdi
	movss	48(%rsp), %xmm2
	ucomiss	%xmm1, %xmm2
	movss	%xmm0, (%rsp)
	movss	24(%rsp), %xmm3
	jbe	.L27
	movl	$-1, (%rdi)
.L27:
	leal	-1065353216(%rbx), %eax
	xorps	.LC3(%rip), %xmm3
	testl	$-8388609, %eax
	jne	.L47
	pxor	%xmm1, %xmm1
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L71:
	movss	.LC4(%rip), %xmm5
	movq	%rdi, 24(%rsp)
	movss	%xmm0, 20(%rsp)
	mulss	%xmm5, %xmm0
	movss	%xmm5, (%rsp)
	call	__sinf@PLT
	movss	.LC2(%rip), %xmm4
	movaps	%xmm0, %xmm1
	movss	(%rsp), %xmm5
	movq	24(%rsp), %rdi
	movss	20(%rsp), %xmm3
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L37:
	movss	.LC1(%rip), %xmm2
	subss	%xmm2, %xmm3
	pxor	%xmm0, %xmm0
.L34:
	movss	.LC41(%rip), %xmm1
	mulss	%xmm3, %xmm1
	movss	.LC46(%rip), %xmm4
	mulss	%xmm3, %xmm4
	addss	.LC42(%rip), %xmm1
	addss	.LC47(%rip), %xmm4
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm4
	addss	.LC43(%rip), %xmm1
	addss	.LC48(%rip), %xmm4
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm4
	addss	.LC44(%rip), %xmm1
	addss	.LC49(%rip), %xmm4
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm4
	addss	.LC45(%rip), %xmm1
	addss	.LC50(%rip), %xmm4
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm4
	subss	.LC17(%rip), %xmm1
	addss	%xmm4, %xmm2
	mulss	%xmm3, %xmm1
	mulss	.LC51(%rip), %xmm3
	divss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$1047343879, %ebx
	movss	.LC1(%rip), %xmm2
	jle	.L34
	subss	.LC10(%rip), %xmm3
	jmp	.L35
.L18:
	subss	.LC9(%rip), %xmm0
	movss	.LC4(%rip), %xmm5
	movaps	%xmm1, 48(%rsp)
	movq	%rdi, 24(%rsp)
	movaps	%xmm4, 32(%rsp)
	movss	%xmm3, 20(%rsp)
	mulss	%xmm5, %xmm0
	movss	%xmm5, (%rsp)
	call	__sinf@PLT
	movaps	48(%rsp), %xmm1
	movq	24(%rsp), %rdi
	movaps	32(%rsp), %xmm4
	movss	20(%rsp), %xmm3
	movss	(%rsp), %xmm5
	jmp	.L24
	.size	__ieee754_lgammaf_r, .-__ieee754_lgammaf_r
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.align 16
.LC3:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	1078530011
	.align 4
.LC5:
	.long	1258291200
	.align 4
.LC6:
	.long	1056964608
	.align 4
.LC7:
	.long	1082130432
	.align 4
.LC8:
	.long	1069547520
	.align 4
.LC9:
	.long	1073741824
	.align 4
.LC10:
	.long	1055677196
	.align 4
.LC11:
	.long	1069225667
	.align 4
.LC12:
	.long	936608674
	.align 4
.LC13:
	.long	963090279
	.align 4
.LC14:
	.long	983323809
	.align 4
.LC15:
	.long	1005716094
	.align 4
.LC16:
	.long	1032450049
	.align 4
.LC17:
	.long	1033773887
	.align 4
.LC18:
	.long	943467637
	.align 4
.LC19:
	.long	954369093
	.align 4
.LC20:
	.long	973452852
	.align 4
.LC21:
	.long	993881798
	.align 4
.LC22:
	.long	1017682197
	.align 4
.LC23:
	.long	1051007590
	.align 4
.LC24:
	.long	967146347
	.align 4
.LC25:
	.long	985134198
	.align 4
.LC26:
	.long	1002956551
	.align 4
.LC27:
	.long	1023823175
	.align 4
.LC28:
	.long	1056422238
	.align 4
.LC29:
	.long	967830007
	.align 4
.LC30:
	.long	973942917
	.align 4
.LC31:
	.long	991172249
	.align 4
.LC32:
	.long	1009319166
	.align 4
.LC33:
	.long	1032083989
	.align 4
.LC34:
	.long	3114531111
	.align 4
.LC35:
	.long	979826791
	.align 4
.LC36:
	.long	997292030
	.align 4
.LC37:
	.long	1016280893
	.align 4
.LC38:
	.long	1041703228
	.align 4
.LC39:
	.long	837164114
	.align 4
.LC40:
	.long	1039715789
	.align 4
.LC41:
	.long	1012612190
	.align 4
.LC42:
	.long	1047164280
	.align 4
.LC43:
	.long	1064979378
	.align 4
.LC44:
	.long	1069169383
	.align 4
.LC45:
	.long	1059193076
	.align 4
.LC46:
	.long	995284443
	.align 4
.LC47:
	.long	1037398703
	.align 4
.LC48:
	.long	1061482463
	.align 4
.LC49:
	.long	1074280749
	.align 4
.LC50:
	.long	1075654334
	.align 4
.LC51:
	.long	3204448256
	.align 4
.LC52:
	.long	939917159
	.align 4
.LC53:
	.long	988886452
	.align 4
.LC54:
	.long	1020936420
	.align 4
.LC55:
	.long	1041620198
	.align 4
.LC56:
	.long	1051118714
	.align 4
.LC57:
	.long	1046226010
	.align 4
.LC58:
	.long	922081213
	.align 4
.LC59:
	.long	978054870
	.align 4
.LC60:
	.long	1016643412
	.align 4
.LC61:
	.long	1043337070
	.align 4
.LC62:
	.long	1060688069
	.align 4
.LC63:
	.long	1068641595
	.align 4
.LC64:
	.long	1086324736
	.align 4
.LC65:
	.long	1084227584
	.align 4
.LC66:
	.long	1077936128
	.align 4
.LC67:
	.long	3134571752
	.align 4
.LC68:
	.long	979058130
	.align 4
.LC69:
	.long	974915164
	.align 4
.LC70:
	.long	978324733
	.align 4
.LC71:
	.long	993397601
	.align 4
.LC72:
	.long	1034594987
	.align 4
.LC73:
	.long	1054244637
