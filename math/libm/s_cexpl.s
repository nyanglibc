	.text
	.p2align 4,,15
	.globl	__cexpl
	.type	__cexpl, @function
__cexpl:
	subq	$104, %rsp
	fldt	112(%rsp)
	fldt	128(%rsp)
	fld	%st(1)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L2
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L79
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L85
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L86
	fld	%st(1)
	fldt	.LC5(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L11
	fldz
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jp	.L87
	je	.L12
	fxch	%st(3)
	jmp	.L14
.L87:
	fxch	%st(3)
	.p2align 4,,10
	.p2align 3
.L14:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L77
.L50:
	fldz
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L79:
	fld	%st(4)
	fldt	.LC5(%rip)
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jnb	.L88
	fldz
	fxch	%st(5)
	fucomi	%st(5), %st
	fstp	%st(5)
	jp	.L89
	jne	.L90
	fxch	%st(2)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L88:
	fxch	%st(2)
.L4:
	fucomi	%st(0), %st
	jp	.L91
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L42
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L91:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L92:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L93:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
.L97:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L98:
	fstp	%st(0)
	jmp	.L8
.L99:
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L100:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %edi
	call	feraiseexcept@PLT
	flds	.LC3(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L96:
	fxch	%st(1)
.L1:
	addq	$104, %rsp
	fxch	%st(1)
	ret
.L89:
	fstp	%st(1)
	fxch	%st(1)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L90:
	fstp	%st(1)
	fxch	%st(1)
.L51:
	fucomi	%st(0), %st
	jp	.L92
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L93
.L42:
	fldln2
	fnstcw	62(%rsp)
	movzwl	62(%rsp), %eax
	fmuls	.LC7(%rip)
	orb	$12, %ah
	movw	%ax, 60(%rsp)
	fldcw	60(%rsp)
	fistpl	(%rsp)
	fldcw	62(%rsp)
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L69
	fxch	%st(2)
	fstpt	32(%rsp)
	fxch	%st(1)
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdi
	subq	$16, %rsp
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__sincosl@PLT
	popq	%r9
	popq	%r10
	fldt	16(%rsp)
	fldt	32(%rsp)
.L18:
	fildl	(%rsp)
	fxch	%st(1)
	fucomi	%st(1), %st
	ja	.L80
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L94:
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L70
.L95:
	fstp	%st(1)
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L70:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	96(%rsp)
	fmul	%st(1), %st
	fldt	80(%rsp)
	popq	%rcx
	popq	%rsi
	fmulp	%st, %st(2)
	fxch	%st(1)
.L24:
	fldt	.LC5(%rip)
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L25
	fld	%st(1)
	fmul	%st(2), %st
	fstp	%st(0)
.L25:
	fldt	.LC5(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L1
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
	addq	$104, %rsp
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	fstp	%st(2)
	fxch	%st(1)
	fstpt	16(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__ieee754_expl@PLT
	fldt	32(%rsp)
	fldt	16(%rsp)
	fsubr	%st, %st(1)
	fldt	80(%rsp)
	fmul	%st(3), %st
	fld	%st(0)
	fstpt	80(%rsp)
	fldt	96(%rsp)
	fmul	%st(4), %st
	fld	%st(0)
	fstpt	96(%rsp)
	fxch	%st(3)
	popq	%rdi
	popq	%r8
	fucomi	%st(2), %st
	jbe	.L94
	fsub	%st(2), %st
	fxch	%st(1)
	fmul	%st(4), %st
	fld	%st(0)
	fstpt	64(%rsp)
	fxch	%st(3)
	fmulp	%st, %st(4)
	fxch	%st(3)
	fld	%st(0)
	fstpt	80(%rsp)
	fxch	%st(3)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L95
	fstp	%st(0)
	fldt	.LC4(%rip)
	fmul	%st, %st(2)
	fmulp	%st, %st(1)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L11:
	fxch	%st(3)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L50
.L77:
	flds	.LC1(%rip)
.L44:
	fldt	.LC5(%rip)
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jbe	.L71
	fstp	%st(2)
	fxch	%st(1)
	fstpt	(%rsp)
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdi
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__sincosl@PLT
	fldt	96(%rsp)
	fldt	80(%rsp)
	popq	%rax
	popq	%rdx
	fldt	(%rsp)
	fxch	%st(2)
.L30:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	fld	%st(0)
	jne	.L81
	fxch	%st(2)
.L32:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L82
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	jne	.L96
	fstp	%st(0)
	flds	.LC1(%rip)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fstp	%st(2)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L83
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L97
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L98
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L99
	jne	.L100
	flds	.LC3(%rip)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L69:
	fxch	%st(1)
	fstpt	64(%rsp)
	fld1
	fstpt	80(%rsp)
	fxch	%st(1)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L82:
	fchs
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L81:
	fchs
	fxch	%st(2)
	jmp	.L32
.L71:
	fstp	%st(1)
	fld1
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L30
.L85:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L86:
	fstp	%st(0)
	fxch	%st(1)
.L39:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L84
	flds	.LC1(%rip)
	fxch	%st(1)
	fsub	%st(0), %st
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L84:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L34
	fstp	%st(0)
	fldz
	fchs
.L34:
	fldz
	fxch	%st(1)
	jmp	.L1
.L83:
	fstp	%st(0)
	fstp	%st(0)
	flds	.LC3(%rip)
	fld	%st(0)
	jmp	.L1
	.size	__cexpl, .-__cexpl
	.weak	cexpf64x
	.set	cexpf64x,__cexpl
	.weak	cexpl
	.set	cexpl,__cexpl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095040
	.align 4
.LC3:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC5:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1182792704
