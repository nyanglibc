	.text
	.p2align 4,,15
	.type	lg_sinpi, @function
lg_sinpi:
	fldt	8(%rsp)
	flds	.LC0(%rip)
	fucomip	%st(1), %st
	jnb	.L7
	fsubrs	.LC2(%rip)
	fldpi
	fmulp	%st, %st(1)
	fstpt	8(%rsp)
	jmp	__cosl@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	fldpi
	fmulp	%st, %st(1)
	fstpt	8(%rsp)
	jmp	__sinl@PLT
	.size	lg_sinpi, .-lg_sinpi
	.p2align 4,,15
	.type	lg_cospi, @function
lg_cospi:
	fldt	8(%rsp)
	flds	.LC0(%rip)
	fucomip	%st(1), %st
	jnb	.L13
	fsubrs	.LC2(%rip)
	fldpi
	fmulp	%st, %st(1)
	fstpt	8(%rsp)
	jmp	__sinl@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	fldpi
	fmulp	%st, %st(1)
	fstpt	8(%rsp)
	jmp	__cosl@PLT
	.size	lg_cospi, .-lg_cospi
	.p2align 4,,15
	.globl	__lgamma_negl
	.type	__lgamma_negl, @function
__lgamma_negl:
	pushq	%rbx
	subq	$480, %rsp
	fldt	496(%rsp)
	fnstcw	158(%rsp)
	movzwl	158(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 156(%rsp)
	movzwl	158(%rsp), %eax
	orb	$12, %ah
	movw	%ax, 154(%rsp)
	fld	%st(0)
	fmuls	.LC6(%rip)
	fld	%st(0)
	fldcw	156(%rsp)
	frndint
	fldcw	158(%rsp)
	fldcw	154(%rsp)
	fistpl	148(%rsp)
	fldcw	158(%rsp)
	movl	148(%rsp), %eax
	testb	$1, %al
	je	.L48
	fstp	%st(0)
	movl	%eax, %ecx
	notl	%ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	%edx, (%rsp)
	fildl	(%rsp)
.L19:
	leal	-4(%rax), %ebx
	movl	%ebx, %eax
	andl	$2, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, (%rdi)
#APP
# 383 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstcw 174(%rsp)
# 0 "" 2
#NO_APP
	movzwl	174(%rsp), %edx
	movl	%edx, %eax
	movw	%dx, 176(%rsp)
	andb	$-16, %ah
	orb	$3, %ah
	cmpw	%ax, %dx
	movw	%ax, 224(%rsp)
	jne	.L49
	movb	$0, 208(%rsp)
.L22:
	movslq	%ebx, %rax
	salq	$5, %rax
	movq	%rax, %rdx
	leaq	lgamma_zeros(%rip), %rax
	addq	%rdx, %rax
	cmpl	$1, %ebx
	fldt	(%rax)
	fldt	16(%rax)
	fld	%st(3)
	fsub	%st(2), %st
	fsub	%st(1), %st
	jle	.L50
	fld	%st(3)
	fsub	%st(5), %st
	fabs
	fxch	%st(4)
	fsub	%st(3), %st
	fsub	%st(2), %st
	fabs
	fld	%st(1)
	fchs
	fstpt	16(%rsp)
	flds	.LC2(%rip)
	fld	%st(5)
	fmul	%st(1), %st
	fucomip	%st(2), %st
	ja	.L51
	fstp	%st(1)
	testb	$1, %bl
	jne	.L52
	fmul	%st(1), %st
	fxch	%st(5)
.L31:
	fstpt	128(%rsp)
	fxch	%st(3)
	subq	$16, %rsp
	fstpt	48(%rsp)
	fxch	%st(2)
	fstpt	128(%rsp)
	fxch	%st(1)
	fstpt	112(%rsp)
	fstpt	96(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	lg_sinpi
	fstpt	80(%rsp)
	subq	$16, %rsp
	fldt	32(%rsp)
	fstpt	(%rsp)
	call	lg_cospi
	fstpt	32(%rsp)
	popq	%rcx
	popq	%rsi
	fldt	48(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	64(%rsp)
	call	lg_cospi
	fstpt	48(%rsp)
	subq	$16, %rsp
	fldt	80(%rsp)
	fstpt	(%rsp)
	call	lg_sinpi
	popq	%rdi
	popq	%r8
	fldt	48(%rsp)
	fdivp	%st, %st(1)
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fldt	80(%rsp)
	fsubr	%st, %st(1)
	fadd	%st(0), %st
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	fstpt	48(%rsp)
	popq	%r9
	popq	%r10
	fldt	128(%rsp)
	fldt	112(%rsp)
	fldt	96(%rsp)
	fldt	80(%rsp)
.L29:
	cmpl	$7, %ebx
	fld1
	fld	%st(0)
	fsub	%st(2), %st
	fld	%st(1)
	fsub	%st(1), %st
	fsubp	%st, %st(3)
	fxch	%st(3)
	fsubrp	%st, %st(2)
	fld	%st(0)
	fsub	%st(5), %st
	fsubr	%st, %st(1)
	fxch	%st(5)
	fsubrp	%st, %st(1)
	jle	.L53
	fldz
	pxor	%xmm0, %xmm0
	fstpt	48(%rsp)
	fxch	%st(3)
	movss	%xmm0, (%rsp)
.L32:
	fstpt	96(%rsp)
	fxch	%st(2)
	subq	$16, %rsp
	fstpt	96(%rsp)
	fxch	%st(2)
	fstpt	80(%rsp)
	fldt	.LC11(%rip)
	fld	%st(2)
	fstpt	128(%rsp)
	fsubr	%st, %st(2)
	fldt	.LC12(%rip)
	faddp	%st, %st(3)
	fxch	%st(1)
	faddp	%st, %st(2)
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	subq	$16, %rsp
	fldt	128(%rsp)
	fmul	%st, %st(1)
	fxch	%st(1)
	fstpt	128(%rsp)
	flds	.LC2(%rip)
	fldt	96(%rsp)
	fsub	%st, %st(1)
	fldt	112(%rsp)
	faddp	%st, %st(2)
	fxch	%st(1)
	fstpt	112(%rsp)
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	112(%rsp)
	xorl	%eax, %eax
	leaq	16+lgamma_coeff(%rip), %rsi
	fmulp	%st, %st(1)
	fldt	128(%rsp)
	faddp	%st, %st(1)
	fldt	80(%rsp)
	faddp	%st, %st(1)
	fld1
	fld	%st(0)
	fldt	144(%rsp)
	fdivr	%st, %st(1)
	fldt	96(%rsp)
	fdivr	%st, %st(3)
	fld	%st(2)
	fmul	%st(3), %st
	fld	%st(4)
	fmul	%st(5), %st
	fxch	%st(3)
	fmulp	%st, %st(2)
	fldt	48(%rsp)
	fdivp	%st, %st(2)
	fld	%st(1)
	fld	%st(5)
	fmul	%st(3), %st
	fxch	%st(6)
	faddp	%st, %st(5)
	fxch	%st(5)
	fmulp	%st, %st(4)
	fldt	.LC13(%rip)
	fmulp	%st, %st(2)
	fxch	%st(1)
	fstpt	256(%rsp)
	addq	$32, %rsp
	leaq	224(%rsp), %rdx
	leaq	16(%rdx), %rcx
	fldt	.LC5(%rip)
	fxch	%st(4)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L54:
	fldt	(%rsi,%rax)
	fxch	%st(4)
	fxch	%st(3)
.L34:
	fmul	%st(1), %st
	fadd	%st(3), %st
	fxch	%st(3)
	fmul	%st(2), %st
	fxch	%st(4)
	fmul	%st(3), %st
	fstpt	(%rcx,%rax)
	addq	$16, %rax
	cmpq	$240, %rax
	jne	.L54
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	leaq	240(%rdx), %rax
	fldz
	leaq	208(%rsp), %rdx
	.p2align 4,,10
	.p2align 3
.L35:
	fldt	(%rax)
	subq	$16, %rax
	cmpq	%rax, %rdx
	faddp	%st, %st(1)
	jne	.L35
	faddp	%st, %st(1)
	fldt	32(%rsp)
	faddp	%st, %st(1)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L48:
	movl	%eax, (%rsp)
	fildl	(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L16
	je	.L55
.L16:
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	negl	%edx
	movl	%edx, (%rsp)
	fildl	(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L50:
	fstp	%st(2)
	fstp	%st(0)
	fld	%st(2)
	fnstcw	158(%rsp)
	movzwl	158(%rsp), %eax
	leaq	poly_coeff(%rip), %rsi
	fmuls	.LC8(%rip)
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 156(%rsp)
	movzwl	158(%rsp), %eax
	fldcw	156(%rsp)
	frndint
	fldcw	158(%rsp)
	fsubs	.LC9(%rip)
	orb	$12, %ah
	movw	%ax, 154(%rsp)
	movl	$-33, %eax
	fldcw	154(%rsp)
	fistpl	148(%rsp)
	fldcw	158(%rsp)
	movl	148(%rsp), %edx
	leal	(%rdx,%rdx), %ecx
	subl	%ecx, %eax
	movl	%eax, (%rsp)
	movslq	%edx, %rax
	leaq	poly_deg(%rip), %rdx
	fildl	(%rsp)
	movq	(%rdx,%rax,8), %rcx
	leaq	poly_end(%rip), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	%rdx, %rax
	salq	$4, %rax
	testq	%rcx, %rcx
	fmuls	.LC10(%rip)
	fsubr	%st(3), %st
	fldt	(%rsi,%rax)
	je	.L57
	subq	%rcx, %rdx
	leaq	-16(%rsi), %rcx
	leaq	-16(%rsi,%rax), %rax
	salq	$4, %rdx
	addq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L25:
	subq	$16, %rax
	fmul	%st(1), %st
	fldt	16(%rax)
	cmpq	%rax, %rdx
	faddp	%st, %st(1)
	jne	.L25
	fstp	%st(1)
	jmp	.L24
.L57:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L24:
	fmulp	%st, %st(1)
	fxch	%st(2)
	subq	$16, %rsp
	fsubp	%st, %st(1)
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	popq	%rcx
	popq	%rsi
.L26:
	cmpb	$0, 208(%rsp)
	jne	.L56
.L46:
	addq	$480, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	fstp	%st(0)
	fld1
	fdivs	.LC4(%rip)
	addq	$480, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fxch	%st(5)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$9, %edi
	subq	$48, %rsp
	subl	%ebx, %edi
	sarl	%edi
	movl	%edi, 48(%rsp)
	fildl	48(%rsp)
	fld	%st(0)
	fadd	%st(4), %st
	fld	%st(0)
	fstpt	160(%rsp)
	fsub	%st(1), %st
	fsubrp	%st, %st(4)
	fxch	%st(2)
	faddp	%st, %st(3)
	fxch	%st(2)
	fstpt	144(%rsp)
	fld	%st(0)
	fadd	%st(4), %st
	fsub	%st, %st(1)
	fstpt	48(%rsp)
	fsubr	%st, %st(3)
	fxch	%st(1)
	faddp	%st, %st(3)
	fxch	%st(2)
	fld	%st(0)
	fstpt	32(%rsp)
	fstpt	128(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	112(%rsp)
	call	__lgamma_productl@PLT
	addq	$32, %rsp
	fstpt	(%rsp)
	call	__log1pl@PLT
	fchs
	pxor	%xmm1, %xmm1
	fstpt	64(%rsp)
	popq	%rax
	popq	%rdx
	fldt	(%rsp)
	movss	%xmm1, (%rsp)
	fldt	112(%rsp)
	fldt	64(%rsp)
	fldt	96(%rsp)
	fldt	80(%rsp)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L51:
	fstp	%st(0)
	fxch	%st(5)
	fstpt	96(%rsp)
	fxch	%st(3)
	subq	$16, %rsp
	fstpt	48(%rsp)
	fxch	%st(2)
	fstpt	96(%rsp)
	fxch	%st(1)
	fstpt	80(%rsp)
	fstpt	64(%rsp)
	fstpt	(%rsp)
	call	lg_sinpi
	fstpt	16(%rsp)
	subq	$16, %rsp
	fldt	64(%rsp)
	fstpt	(%rsp)
	call	lg_sinpi
	popq	%r11
	popq	%rax
	fldt	16(%rsp)
	fdivp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	fstpt	48(%rsp)
	popq	%rax
	popq	%rdx
	fldt	48(%rsp)
	fldt	64(%rsp)
	fldt	80(%rsp)
	fldt	96(%rsp)
	fxch	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L29
.L49:
#APP
# 391 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 224(%rsp)
# 0 "" 2
#NO_APP
	movb	$1, 208(%rsp)
	jmp	.L22
.L56:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 176(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L46
	.size	__lgamma_negl, .-__lgamma_negl
	.section	.rodata
	.align 32
	.type	poly_end, @object
	.size	poly_end, 64
poly_end:
	.quad	13
	.quad	27
	.quad	42
	.quad	58
	.quad	75
	.quad	91
	.quad	106
	.quad	120
	.align 32
	.type	poly_deg, @object
	.size	poly_deg, 64
poly_deg:
	.quad	13
	.quad	13
	.quad	14
	.quad	15
	.quad	16
	.quad	15
	.quad	14
	.quad	13
	.align 32
	.type	poly_coeff, @object
	.size	poly_coeff, 1936
poly_coeff:
	.long	2795599286
	.long	2243486434
	.long	49151
	.long	0
	.long	1597281559
	.long	3342474688
	.long	49150
	.long	0
	.long	692434391
	.long	4131523076
	.long	49147
	.long	0
	.long	1875096722
	.long	3816594850
	.long	49150
	.long	0
	.long	2344929488
	.long	2179384290
	.long	49147
	.long	0
	.long	1590570809
	.long	3941182886
	.long	49150
	.long	0
	.long	1591394701
	.long	2601616720
	.long	16378
	.long	0
	.long	3041737906
	.long	4012425520
	.long	49150
	.long	0
	.long	1024628972
	.long	2612025755
	.long	16380
	.long	0
	.long	1100200917
	.long	4118590071
	.long	49150
	.long	0
	.long	881025928
	.long	2338113742
	.long	16381
	.long	0
	.long	1540597710
	.long	4282968715
	.long	49150
	.long	0
	.long	1369841131
	.long	3484315409
	.long	16381
	.long	0
	.long	2204013463
	.long	2297459290
	.long	49151
	.long	0
	.long	3621156264
	.long	4069722256
	.long	49150
	.long	0
	.long	1424668109
	.long	3395091936
	.long	49150
	.long	0
	.long	2931924409
	.long	3878126583
	.long	16380
	.long	0
	.long	2955904710
	.long	2169689754
	.long	49151
	.long	0
	.long	1561039597
	.long	2531897624
	.long	16382
	.long	0
	.long	3948006088
	.long	2797705593
	.long	49151
	.long	0
	.long	1751607797
	.long	2382775894
	.long	16383
	.long	0
	.long	1396542228
	.long	3839064777
	.long	49151
	.long	0
	.long	3411355115
	.long	3958678455
	.long	16383
	.long	0
	.long	573492422
	.long	2760325997
	.long	49152
	.long	0
	.long	677706034
	.long	3123511131
	.long	16384
	.long	0
	.long	2560856712
	.long	4074403445
	.long	49152
	.long	0
	.long	1990921328
	.long	2461934990
	.long	16385
	.long	0
	.long	796939628
	.long	3128056343
	.long	49153
	.long	0
	.long	1566671138
	.long	3620900176
	.long	49150
	.long	0
	.long	67732786
	.long	3868608931
	.long	49150
	.long	0
	.long	667964869
	.long	2966899240
	.long	16382
	.long	0
	.long	1117315079
	.long	3374679185
	.long	49151
	.long	0
	.long	1735556164
	.long	3930949982
	.long	16383
	.long	0
	.long	1858631958
	.long	3257282271
	.long	49152
	.long	0
	.long	2918457668
	.long	2215595413
	.long	16385
	.long	0
	.long	1757488987
	.long	3342716001
	.long	49153
	.long	0
	.long	440382924
	.long	2385388833
	.long	16386
	.long	0
	.long	2390343808
	.long	3504889037
	.long	49154
	.long	0
	.long	3770666763
	.long	2535638037
	.long	16387
	.long	0
	.long	3232758154
	.long	3696981024
	.long	49155
	.long	0
	.long	1094615351
	.long	2684235741
	.long	16388
	.long	0
	.long	1087195741
	.long	4033133170
	.long	49156
	.long	0
	.long	3923644403
	.long	2953320171
	.long	16389
	.long	0
	.long	4294527788
	.long	3075383740
	.long	49150
	.long	0
	.long	2249959874
	.long	2504089260
	.long	49151
	.long	0
	.long	1170948890
	.long	3288401857
	.long	16383
	.long	0
	.long	61735913
	.long	3431589825
	.long	49152
	.long	0
	.long	783451697
	.long	2868219978
	.long	16385
	.long	0
	.long	3775861406
	.long	2620059087
	.long	49154
	.long	0
	.long	1870742543
	.long	2304809337
	.long	16387
	.long	0
	.long	95943984
	.long	4115296566
	.long	49155
	.long	0
	.long	3231973452
	.long	3652004398
	.long	16388
	.long	0
	.long	65745430
	.long	3248416444
	.long	49157
	.long	0
	.long	2062127939
	.long	2886772955
	.long	16390
	.long	0
	.long	1502281589
	.long	2566314249
	.long	49159
	.long	0
	.long	2321731500
	.long	2278678884
	.long	16392
	.long	0
	.long	1441479091
	.long	4048664214
	.long	49160
	.long	0
	.long	1257469078
	.long	3787957568
	.long	16393
	.long	0
	.long	3281191672
	.long	3407784289
	.long	49162
	.long	0
	.long	2679029429
	.long	4097851952
	.long	49148
	.long	0
	.long	4226373889
	.long	3869949527
	.long	16383
	.long	0
	.long	3034993677
	.long	3764834837
	.long	16384
	.long	0
	.long	2692458363
	.long	3500194165
	.long	16385
	.long	0
	.long	983611684
	.long	3155897957
	.long	16386
	.long	0
	.long	2016393579
	.long	2823986460
	.long	16387
	.long	0
	.long	4095459021
	.long	2515238962
	.long	16388
	.long	0
	.long	4014665682
	.long	2237836384
	.long	16389
	.long	0
	.long	539432191
	.long	3979427247
	.long	16389
	.long	0
	.long	2068808710
	.long	3537709121
	.long	16390
	.long	0
	.long	2206744605
	.long	3144735631
	.long	16391
	.long	0
	.long	2502623432
	.long	2795405912
	.long	16392
	.long	0
	.long	3309335281
	.long	2484912659
	.long	16393
	.long	0
	.long	2479388594
	.long	2206114170
	.long	16394
	.long	0
	.long	289628459
	.long	3914361444
	.long	16394
	.long	0
	.long	1371761296
	.long	3678434983
	.long	16395
	.long	0
	.long	595078049
	.long	3391653059
	.long	16396
	.long	0
	.long	2916175529
	.long	3602532948
	.long	49149
	.long	0
	.long	3978126224
	.long	2490040769
	.long	16383
	.long	0
	.long	3562223645
	.long	3987900886
	.long	16383
	.long	0
	.long	3880594079
	.long	3095774307
	.long	16384
	.long	0
	.long	3111830700
	.long	2292195928
	.long	16385
	.long	0
	.long	1026449188
	.long	3378459178
	.long	16385
	.long	0
	.long	1528315094
	.long	2463848843
	.long	16386
	.long	0
	.long	2598174570
	.long	3593203612
	.long	16386
	.long	0
	.long	3122580410
	.long	2614072454
	.long	16387
	.long	0
	.long	3239990547
	.long	3804313855
	.long	16387
	.long	0
	.long	511818188
	.long	2766806126
	.long	16388
	.long	0
	.long	1089862438
	.long	4024921496
	.long	16388
	.long	0
	.long	603448110
	.long	2925825922
	.long	16389
	.long	0
	.long	2001009153
	.long	4254167854
	.long	16389
	.long	0
	.long	431201283
	.long	3200933782
	.long	16390
	.long	0
	.long	149144974
	.long	2356769330
	.long	16391
	.long	0
	.long	4084201614
	.long	2319561188
	.long	49150
	.long	0
	.long	1776029492
	.long	3450371387
	.long	16382
	.long	0
	.long	453690729
	.long	2319079086
	.long	16383
	.long	0
	.long	1655348427
	.long	3138348488
	.long	16383
	.long	0
	.long	3608258496
	.long	3913080828
	.long	16383
	.long	0
	.long	1750799621
	.long	2476163093
	.long	16384
	.long	0
	.long	3773041635
	.long	3042791785
	.long	16384
	.long	0
	.long	2565515238
	.long	3779410864
	.long	16384
	.long	0
	.long	76279293
	.long	2319891559
	.long	16385
	.long	0
	.long	1075202777
	.long	2865062878
	.long	16385
	.long	0
	.long	1920445130
	.long	3520671493
	.long	16385
	.long	0
	.long	910701832
	.long	2169220936
	.long	16386
	.long	0
	.long	3339905657
	.long	2667246429
	.long	16386
	.long	0
	.long	577046441
	.long	3359842154
	.long	16386
	.long	0
	.long	2591458305
	.long	4160968744
	.long	16386
	.long	0
	.long	3832083428
	.long	2688996967
	.long	49150
	.long	0
	.long	3221668374
	.long	2534115548
	.long	16382
	.long	0
	.long	478596752
	.long	2861185346
	.long	16382
	.long	0
	.long	1060249225
	.long	3539259702
	.long	16382
	.long	0
	.long	984806456
	.long	3702106650
	.long	16382
	.long	0
	.long	4032627403
	.long	4243627057
	.long	16382
	.long	0
	.long	3400807708
	.long	2186844624
	.long	16383
	.long	0
	.long	3892547602
	.long	2438636492
	.long	16383
	.long	0
	.long	886158708
	.long	2519466332
	.long	16383
	.long	0
	.long	720078895
	.long	2773940504
	.long	16383
	.long	0
	.long	3928894187
	.long	2882143174
	.long	16383
	.long	0
	.long	3502630789
	.long	3148645198
	.long	16383
	.long	0
	.long	3526434714
	.long	3342046839
	.long	16383
	.long	0
	.long	1622713075
	.long	3646323993
	.long	16383
	.long	0
	.align 32
	.type	lgamma_coeff, @object
	.size	lgamma_coeff, 256
lgamma_coeff:
	.long	2863311531
	.long	2863311530
	.long	16379
	.long	0
	.long	190887435
	.long	3054198966
	.long	49142
	.long	0
	.long	218157069
	.long	3490513104
	.long	16372
	.long	0
	.long	163617802
	.long	2617884828
	.long	49140
	.long	0
	.long	3354991288
	.long	3702059352
	.long	16372
	.long	0
	.long	3387155472
	.long	4216686284
	.long	49141
	.long	0
	.long	220254733
	.long	3524075730
	.long	16375
	.long	0
	.long	2057092833
	.long	4061410904
	.long	49145
	.long	0
	.long	4029939665
	.long	3086266816
	.long	16380
	.long	0
	.long	3237597038
	.long	2990225416
	.long	49151
	.long	0
	.long	1728064826
	.long	3597803921
	.long	16386
	.long	0
	.long	1717622107
	.long	2631477550
	.long	49158
	.long	0
	.long	3665038759
	.long	2299635520
	.long	16394
	.long	0
	.long	3797270362
	.long	2366424432
	.long	49166
	.long	0
	.long	923324866
	.long	2832270413
	.long	16402
	.long	0
	.long	379239364
	.long	3900984714
	.long	49174
	.long	0
	.align 32
	.type	lgamma_zeros, @object
	.size	lgamma_zeros, 1984
lgamma_zeros:
	.long	130244779
	.long	2638210224
	.long	49152
	.long	0
	.long	801597926
	.long	2600891655
	.long	16315
	.long	0
	.long	3737650782
	.long	2950301776
	.long	49152
	.long	0
	.long	257384358
	.long	3402783282
	.long	49087
	.long	0
	.long	4073552878
	.long	3375394276
	.long	49152
	.long	0
	.long	3652871493
	.long	3194591955
	.long	49086
	.long	0
	.long	3783207004
	.long	4246964899
	.long	49152
	.long	0
	.long	594330401
	.long	3562410641
	.long	49086
	.long	0
	.long	3433825816
	.long	2168615874
	.long	49153
	.long	0
	.long	1615112966
	.long	3888300920
	.long	49088
	.long	0
	.long	2002249378
	.long	2679815123
	.long	49153
	.long	0
	.long	2558853592
	.long	3500818550
	.long	49086
	.long	0
	.long	2243348581
	.long	2688766655
	.long	49153
	.long	0
	.long	1314093874
	.long	2467674469
	.long	49088
	.long	0
	.long	2409655036
	.long	3220477868
	.long	49153
	.long	0
	.long	3011805639
	.long	2890560720
	.long	16320
	.long	0
	.long	1273259884
	.long	3221969196
	.long	49153
	.long	0
	.long	3339425549
	.long	4166725026
	.long	49085
	.long	0
	.long	1547141096
	.long	3757989819
	.long	49153
	.long	0
	.long	1879799856
	.long	2808424079
	.long	16320
	.long	0
	.long	1878062527
	.long	3758202863
	.long	49153
	.long	0
	.long	2652853558
	.long	3992226961
	.long	16320
	.long	0
	.long	181303417
	.long	4294953980
	.long	49153
	.long	0
	.long	2889000780
	.long	2812325192
	.long	49088
	.long	0
	.long	1168099830
	.long	2147490305
	.long	49154
	.long	0
	.long	3968169575
	.long	3409767452
	.long	49086
	.long	0
	.long	1113489664
	.long	2415918364
	.long	49154
	.long	0
	.long	3681125895
	.long	3731643042
	.long	16320
	.long	0
	.long	3142047853
	.long	2415919843
	.long	49154
	.long	0
	.long	2258877807
	.long	3674767323
	.long	49089
	.long	0
	.long	113114573
	.long	2684354486
	.long	49154
	.long	0
	.long	177179962
	.long	3311202122
	.long	49085
	.long	0
	.long	4181440914
	.long	2684354633
	.long	49154
	.long	0
	.long	713571334
	.long	4136350396
	.long	49089
	.long	0
	.long	1181654811
	.long	2952790009
	.long	49154
	.long	0
	.long	1866871831
	.long	3681236139
	.long	49088
	.long	0
	.long	3113308950
	.long	2952790022
	.long	49154
	.long	0
	.long	1459383557
	.long	2590598144
	.long	16320
	.long	0
	.long	1888041076
	.long	3221225471
	.long	49154
	.long	0
	.long	2019061007
	.long	4030954662
	.long	16321
	.long	0
	.long	2406926195
	.long	3221225472
	.long	49154
	.long	0
	.long	679068617
	.long	2872969089
	.long	49087
	.long	0
	.long	4109819126
	.long	3489660927
	.long	49154
	.long	0
	.long	3819694756
	.long	3217806079
	.long	49087
	.long	0
	.long	185148170
	.long	3489660928
	.long	49154
	.long	0
	.long	2445215524
	.long	4268093579
	.long	16320
	.long	0
	.long	4281742427
	.long	3758096383
	.long	49154
	.long	0
	.long	1053154578
	.long	2352805560
	.long	16321
	.long	0
	.long	13224869
	.long	3758096384
	.long	49154
	.long	0
	.long	3635294703
	.long	2345835755
	.long	49089
	.long	0
	.long	4294085638
	.long	4026531839
	.long	49154
	.long	0
	.long	57800861
	.long	3328200435
	.long	49086
	.long	0
	.long	881658
	.long	4026531840
	.long	49154
	.long	0
	.long	3741819233
	.long	3328454427
	.long	16318
	.long	0
	.long	4294912192
	.long	4294967295
	.long	49154
	.long	0
	.long	2723997130
	.long	3247227966
	.long	49089
	.long	0
	.long	27552
	.long	2147483648
	.long	49155
	.long	0
	.long	2070416777
	.long	3247228093
	.long	16321
	.long	0
	.long	4294965675
	.long	2281701375
	.long	49155
	.long	0
	.long	1042040481
	.long	2621958057
	.long	49090
	.long	0
	.long	1621
	.long	2281701376
	.long	49155
	.long	0
	.long	2004390746
	.long	2621958057
	.long	16322
	.long	0
	.long	4294967206
	.long	2415919103
	.long	49155
	.long	0
	.long	1721219690
	.long	2652434015
	.long	16319
	.long	0
	.long	90
	.long	2415919104
	.long	49155
	.long	0
	.long	1696996760
	.long	2652434015
	.long	49087
	.long	0
	.long	4294967291
	.long	2550136831
	.long	49155
	.long	0
	.long	1599364334
	.long	2243058879
	.long	49090
	.long	0
	.long	5
	.long	2550136832
	.long	49155
	.long	0
	.long	1599372872
	.long	2243058879
	.long	16322
	.long	0
	.long	0
	.long	2684354560
	.long	49155
	.long	0
	.long	269559891
	.long	4070661408
	.long	16321
	.long	0
	.long	0
	.long	2684354560
	.long	49155
	.long	0
	.long	269559848
	.long	4070661408
	.long	49089
	.long	0
	.long	0
	.long	2818572288
	.long	49155
	.long	0
	.long	3886779488
	.long	3101456310
	.long	16317
	.long	0
	.long	0
	.long	2818572288
	.long	49155
	.long	0
	.long	3886779487
	.long	3101456310
	.long	49085
	.long	0
	.long	0
	.long	2952790016
	.long	49155
	.long	0
	.long	3217200291
	.long	2255604589
	.long	16313
	.long	0
	.long	0
	.long	2952790016
	.long	49155
	.long	0
	.long	3217200290
	.long	2255604589
	.long	49081
	.long	0
	.long	0
	.long	3087007744
	.long	49155
	.long	0
	.long	2982203084
	.long	3138232472
	.long	16308
	.long	0
	.long	0
	.long	3087007744
	.long	49155
	.long	0
	.long	2982203084
	.long	3138232472
	.long	49076
	.long	0
	.long	0
	.long	3221225472
	.long	49155
	.long	0
	.long	2544615013
	.long	4184309963
	.long	16303
	.long	0
	.long	0
	.long	3221225472
	.long	49155
	.long	0
	.long	2544615013
	.long	4184309963
	.long	49071
	.long	0
	.long	0
	.long	3355443200
	.long	49155
	.long	0
	.long	3002943143
	.long	2677958376
	.long	16299
	.long	0
	.long	0
	.long	3355443200
	.long	49155
	.long	0
	.long	3002943143
	.long	2677958376
	.long	49067
	.long	0
	.long	0
	.long	3489660928
	.long	49155
	.long	0
	.long	1383255324
	.long	3295948771
	.long	16294
	.long	0
	.long	0
	.long	3489660928
	.long	49155
	.long	0
	.long	1383255324
	.long	3295948771
	.long	49062
	.long	0
	.long	0
	.long	3623878656
	.long	49155
	.long	0
	.long	3866433797
	.long	3906309654
	.long	16289
	.long	0
	.long	0
	.long	3623878656
	.long	49155
	.long	0
	.long	3866433797
	.long	3906309654
	.long	49057
	.long	0
	.long	0
	.long	3758096384
	.long	49155
	.long	0
	.long	2822957498
	.long	2232176945
	.long	16285
	.long	0
	.long	0
	.long	3758096384
	.long	49155
	.long	0
	.long	2822957498
	.long	2232176945
	.long	49053
	.long	0
	.long	0
	.long	3892314112
	.long	49155
	.long	0
	.long	449145814
	.long	2463091802
	.long	16280
	.long	0
	.long	0
	.long	3892314112
	.long	49155
	.long	0
	.long	449145814
	.long	2463091802
	.long	49048
	.long	0
	.long	0
	.long	4026531840
	.long	49155
	.long	0
	.long	1051751174
	.long	2627297922
	.long	16275
	.long	0
	.long	0
	.long	4026531840
	.long	49155
	.long	0
	.long	1051751174
	.long	2627297922
	.long	49043
	.long	0
	.long	0
	.long	4160749568
	.long	49155
	.long	0
	.long	531489303
	.long	2712049468
	.long	16270
	.long	0
	.long	0
	.long	4160749568
	.long	49155
	.long	0
	.long	531489303
	.long	2712049468
	.long	49038
	.long	0
	.long	0
	.long	2147483648
	.long	49156
	.long	0
	.long	531489303
	.long	2712049468
	.long	16265
	.long	0
	.long	0
	.long	2147483648
	.long	49156
	.long	0
	.long	531489303
	.long	2712049468
	.long	49033
	.long	0
	.long	0
	.long	2214592512
	.long	49156
	.long	0
	.long	3899297194
	.long	2629866150
	.long	16260
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1048576000
	.align 4
.LC2:
	.long	1056964608
	.align 4
.LC4:
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	190887435
	.long	3054198966
	.long	49142
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	3221225472
	.align 4
.LC8:
	.long	3238002688
	.align 4
.LC9:
	.long	1098907648
	.align 4
.LC10:
	.long	1031798784
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	2730183323
	.long	2918732888
	.long	16384
	.long	0
	.align 16
.LC12:
	.long	2978317852
	.long	2689029055
	.long	16319
	.long	0
	.align 16
.LC13:
	.long	2863311531
	.long	2863311530
	.long	16379
	.long	0
