.globl __nearbyintl
.type __nearbyintl,@function
.align 1<<4
__nearbyintl:
 fldt 8(%rsp)
 fnstenv -28(%rsp)
 frndint
 fnstsw
 andl $0x1, %eax
 orl %eax, -24(%rsp)
 fldenv -28(%rsp)
 ret
.size __nearbyintl,.-__nearbyintl
.weak nearbyintl
nearbyintl = __nearbyintl
.weak nearbyintf64x
nearbyintf64x = __nearbyintl
