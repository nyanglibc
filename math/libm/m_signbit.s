.globl __signbit
.type __signbit,@function
.align 1<<4
__signbit:
 pmovmskb %xmm0, %eax
 andl $0x80, %eax
 ret
.size __signbit,.-__signbit
