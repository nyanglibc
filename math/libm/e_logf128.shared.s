	.text
#APP
	.symver __ieee754_logf128,__logf128_finite@GLIBC_2.26
	.globl	__divtf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__letf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__floatsitf
	.globl	__multf3
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_logf128
	.type	__ieee754_logf128, @function
__ieee754_logf128:
	pushq	%rbx
	addq	$-128, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	(%rsp), %rdx
	movq	%rax, %rcx
	movq	%rdx, %rdi
	shrq	$32, %rcx
	shrq	$32, %rdi
	orl	%edi, %eax
	movl	%ecx, %esi
	andl	$2147483647, %esi
	orl	%edx, %eax
	orl	%esi, %eax
	je	.L17
	testl	%ecx, %ecx
	js	.L18
	cmpl	$2147418111, %esi
	jle	.L5
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
.L1:
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	pxor	%xmm1, %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__divtf3@PLT
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	pxor	%xmm1, %xmm1
	call	__divtf3@PLT
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	124(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__frexpf128@PLT
	movaps	%xmm0, 16(%rsp)
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rax
	movq	%rdx, %rsi
	movq	%rax, 32(%rsp)
	movq	%rdx, 40(%rsp)
	shrq	$32, %rsi
	movzwl	%si, %eax
	orl	$65536, %eax
	cmpl	$92159, %eax
	ja	.L6
	subl	$65280, %eax
	movq	$0, 24(%rsp)
	movq	24(%rsp), %rdi
	shrl	$9, %eax
	subl	$1, 124(%rsp)
	movl	%edx, %edx
	movl	%eax, %ecx
	leal	64(%rax), %ebx
	movq	$0, 16(%rsp)
	sall	$9, %ecx
	movl	%edi, %edi
	addl	$1073676288, %ecx
	salq	$32, %rcx
	orq	%rcx, %rdi
	movabsq	$-4294967296, %rcx
	andq	%rcx, %rdi
	leal	65536(%rsi), %ecx
	movq	%rdi, 24(%rsp)
	salq	$32, %rcx
	orq	%rcx, %rdx
	movq	%rdx, 40(%rsp)
.L7:
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L8
	movdqa	.LC3(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L8
	movdqa	(%rsp), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__eqtf2@PLT
	testq	%rax, %rax
	pxor	%xmm0, %xmm0
	je	.L1
	movdqa	(%rsp), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__subtf3@PLT
	pxor	%xmm2, %xmm2
	movaps	%xmm0, (%rsp)
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm2, 48(%rsp)
	movaps	%xmm2, 32(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	16(%rsp), %xmm1
	subl	$26, %ebx
	movslq	%ebx, %rbx
	movdqa	32(%rsp), %xmm0
	salq	$4, %rbx
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__divtf3@PLT
	movl	124(%rsp), %edi
	movaps	%xmm0, (%rsp)
	call	__floatsitf@PLT
	movaps	%xmm0, 80(%rsp)
	movdqa	.LC5(%rip), %xmm1
	call	__multf3@PLT
	leaq	logtbl(%rip), %rax
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rax,%rbx), %xmm4
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm4, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm2, %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
.L11:
	movdqa	(%rsp), %xmm3
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm2, 96(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC7(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	96(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	subq	$-128, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leal	-65024(%rax), %ebx
	movq	$0, 24(%rsp)
	movq	24(%rsp), %rdx
	movq	$0, 16(%rsp)
	shrl	$10, %ebx
	movl	%ebx, %eax
	movl	%edx, %edx
	sall	$10, %eax
	addl	$1073610752, %eax
	salq	$32, %rax
	orq	%rax, %rdx
	movabsq	$-4294967296, %rax
	andq	%rax, %rdx
	movq	%rdx, 24(%rsp)
	jmp	.L7
	.size	__ieee754_logf128, .-__ieee754_logf128
	.section	.rodata
	.align 32
	.type	logtbl, @object
	.size	logtbl, 1472
logtbl:
	.long	3120610356
	.long	221410670
	.long	525219082
	.long	-1074084508
	.long	1754184468
	.long	3545750894
	.long	4021071767
	.long	-1074091298
	.long	1972799138
	.long	3876066892
	.long	60576235
	.long	-1074097833
	.long	2561531352
	.long	2199873922
	.long	3778601485
	.long	-1074104122
	.long	3940709288
	.long	3668291637
	.long	999921857
	.long	-1074110167
	.long	880297736
	.long	3629305324
	.long	4041846385
	.long	-1074115976
	.long	996169038
	.long	1008596411
	.long	144338377
	.long	-1074121551
	.long	3681410223
	.long	1859458171
	.long	2980918730
	.long	-1074126900
	.long	4265176897
	.long	4011443616
	.long	1091826237
	.long	-1074132025
	.long	977592820
	.long	1936481205
	.long	1615681634
	.long	-1074138824
	.long	4076946203
	.long	3201081549
	.long	911858108
	.long	-1074148210
	.long	4195885257
	.long	152285216
	.long	2146666505
	.long	-1074157177
	.long	1222853694
	.long	2886488648
	.long	4180848445
	.long	-1074165733
	.long	2839918033
	.long	2272580989
	.long	2619206742
	.long	-1074173885
	.long	3538159334
	.long	3080058171
	.long	2655350001
	.long	-1074181642
	.long	4219092236
	.long	2437072135
	.long	1853587647
	.long	-1074189011
	.long	3895524657
	.long	3719931441
	.long	2997408224
	.long	-1074196000
	.long	2275823266
	.long	2626066679
	.long	2339728059
	.long	-1074204654
	.long	4236379694
	.long	4219638508
	.long	1800682929
	.long	-1074217152
	.long	2652739202
	.long	3730128749
	.long	2201204064
	.long	-1074228931
	.long	1921603921
	.long	799548235
	.long	2954203158
	.long	-1074240004
	.long	3951712976
	.long	2758933115
	.long	709476865
	.long	-1074250383
	.long	4235386498
	.long	3727919937
	.long	2183652475
	.long	-1074260082
	.long	2175906386
	.long	2497195898
	.long	970708325
	.long	-1074272111
	.long	3435160315
	.long	1342695877
	.long	1669613218
	.long	-1074288857
	.long	3333734860
	.long	3757376037
	.long	2376948507
	.long	-1074304312
	.long	3972867337
	.long	2921160355
	.long	1566132616
	.long	-1074318498
	.long	1771142532
	.long	4100803823
	.long	183156960
	.long	-1074331437
	.long	3256245967
	.long	3142558051
	.long	3124821336
	.long	-1074354654
	.long	2195736342
	.long	391209878
	.long	1217277769
	.long	-1074375671
	.long	3926290008
	.long	289930039
	.long	1799123981
	.long	-1074394319
	.long	4169229144
	.long	230172631
	.long	2612749719
	.long	-1074424089
	.long	2190013205
	.long	1781507545
	.long	692020870
	.long	-1074452140
	.long	1590175461
	.long	841549835
	.long	1354207142
	.long	-1074488645
	.long	1398940280
	.long	3227316695
	.long	664604945
	.long	-1074526858
	.long	1616482959
	.long	778907550
	.long	2736990970
	.long	-1074584428
	.long	3705920973
	.long	1334956162
	.long	3298476602
	.long	-1074658638
	.long	4191813877
	.long	2362202754
	.long	1485694738
	.long	-1074790057
	.long	0
	.long	0
	.long	0
	.long	0
	.long	3795680557
	.long	1015640006
	.long	1324975984
	.long	-1074791079
	.long	1680479561
	.long	2198416473
	.long	2015355105
	.long	-1074660678
	.long	978386825
	.long	1533029070
	.long	3769187221
	.long	-1074586733
	.long	3775780357
	.long	3424391088
	.long	3323933694
	.long	-1074530925
	.long	1427876974
	.long	1543148415
	.long	402983553
	.long	-1074493983
	.long	594235303
	.long	1939217084
	.long	328877575
	.long	-1074456754
	.long	1276273755
	.long	2061882992
	.long	499981442
	.long	-1074431419
	.long	573151774
	.long	1023831325
	.long	2145204915
	.long	-1074402402
	.long	2220471351
	.long	792414161
	.long	582552126
	.long	-1074383470
	.long	3747993878
	.long	3670866195
	.long	3553123422
	.long	-1074365360
	.long	731962703
	.long	766567215
	.long	2180239233
	.long	-1074345487
	.long	317082047
	.long	1679517254
	.long	1964676276
	.long	-1074327763
	.long	2584892605
	.long	173231061
	.long	701392095
	.long	-1074316102
	.long	1673878328
	.long	2599693781
	.long	3316984476
	.long	-1074303598
	.long	2124304009
	.long	3928614928
	.long	1889810620
	.long	-1074290261
	.long	503407083
	.long	370505728
	.long	2446658842
	.long	-1074276104
	.long	57071842
	.long	2867386163
	.long	1729235618
	.long	-1074263625
	.long	3208466802
	.long	2573766277
	.long	2200924582
	.long	-1074255743
	.long	442833089
	.long	3606707095
	.long	718313567
	.long	-1074247467
	.long	4262004119
	.long	3019514766
	.long	132123356
	.long	-1074238803
	.long	2714681449
	.long	3291638074
	.long	3756093579
	.long	-1074229757
	.long	125952500
	.long	2553030319
	.long	2469692129
	.long	-1074220332
	.long	473228889
	.long	1562115625
	.long	475941832
	.long	-1074210534
	.long	4235161123
	.long	465964794
	.long	3349952022
	.long	-1074200473
	.long	3651334685
	.long	3109405905
	.long	208869105
	.long	-1074195208
	.long	1258093565
	.long	1913539177
	.long	2130154363
	.long	-1074189765
	.long	1092301891
	.long	3085958015
	.long	3446643575
	.long	-1074184145
	.long	1671673029
	.long	3302975429
	.long	2978337957
	.long	-1074178350
	.long	3266525676
	.long	1935963943
	.long	4027491662
	.long	-1074172383
	.long	1679394074
	.long	2476265459
	.long	1488953350
	.long	-1074166245
	.long	2029038351
	.long	3942848699
	.long	3320396180
	.long	-1074159940
	.long	2256544559
	.long	703402992
	.long	473084655
	.long	-1074153468
	.long	56983496
	.long	1379367245
	.long	1247289911
	.long	-1074146833
	.long	251703541
	.long	185252864
	.long	1223329421
	.long	-1074140036
	.long	4064146299
	.long	1803143274
	.long	2366171570
	.long	-1074134060
	.long	4061842253
	.long	1554689636
	.long	3836244085
	.long	-1074130503
	.long	2768383299
	.long	2458829335
	.long	900044767
	.long	-1074126867
	.long	4089969124
	.long	1346765871
	.long	2395355022
	.long	-1074123155
	.long	1504080260
	.long	1940926378
	.long	53009045
	.long	-1074119366
	.long	1199345296
	.long	4264664085
	.long	2854890744
	.long	-1074115503
	.long	1608747144
	.long	2182920389
	.long	2672505602
	.long	-1074111565
	.long	743530088
	.long	786349409
	.long	35144525
	.long	-1074107553
	.long	2180506447
	.long	1353037437
	.long	4128292014
	.long	-1074103470
	.long	3179016802
	.long	2984876914
	.long	2727309671
	.long	-1074099314
	.long	152696708
	.long	2497530968
	.long	850641237
	.long	-1074095087
	.long	1173978000
	.long	476603673
	.long	3578486027
	.long	-1074090791
	.long	94165711
	.long	561641153
	.long	3166481422
	.long	-1074086425
	.long	643941982
	.long	3246015076
	.long	519164062
	.long	-1074081990
	.long	75578588
	.long	2455352155
	.long	893666223
	.long	-1074077488
	.long	2552923964
	.long	2065391951
	.long	1013515241
	.long	-1074072919
	.long	2090464304
	.long	3940110925
	.long	976136548
	.long	-1074068894
	.long	595599082
	.long	1809620092
	.long	2418670989
	.long	-1074066544
	.long	1451199860
	.long	3022268471
	.long	1129418266
	.long	-1074064161
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-1073872896
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676800
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1073675264
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC5:
	.long	1060072180
	.long	2654681472
	.long	485989052
	.long	1072398205
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073636068
	.align 16
.LC7:
	.long	380973026
	.long	4052534486
	.long	2798455332
	.long	1073418527
	.align 16
.LC8:
	.long	3705936331
	.long	813494183
	.long	3499978229
	.long	1073423520
	.align 16
.LC9:
	.long	1882982962
	.long	2502264554
	.long	2967475679
	.long	1073429267
	.align 16
.LC10:
	.long	2172296746
	.long	716983505
	.long	1426183204
	.long	1073435989
	.align 16
.LC11:
	.long	2337889006
	.long	4238834948
	.long	390451863
	.long	1073443933
	.align 16
.LC12:
	.long	850657503
	.long	2708293088
	.long	2576980617
	.long	1073453465
	.align 16
.LC13:
	.long	3668197376
	.long	3307331514
	.long	1908874353
	.long	1073465116
	.align 16
.LC14:
	.long	3423638963
	.long	4271532811
	.long	4294967295
	.long	1073479679
	.align 16
.LC15:
	.long	1220153407
	.long	613566986
	.long	1227133513
	.long	1073489042
	.align 16
.LC16:
	.long	3370531117
	.long	1431655892
	.long	1431655765
	.long	1073501525
	.align 16
.LC17:
	.long	2570813083
	.long	2576980377
	.long	2576980377
	.long	1073519001
	.align 16
.LC18:
	.long	4292835767
	.long	4294967295
	.long	4294967295
	.long	1073545215
	.align 16
.LC19:
	.long	1431655771
	.long	1431655765
	.long	1431655765
	.long	1073567061
	.align 16
.LC20:
	.long	0
	.long	0
	.long	0
	.long	1073610752
