 .section .rodata.cst8,"aM",@progbits,8
 .p2align 3
 .type one,@object
one: .double 1.0
 .size one,.-one;
 .type limit,@object
limit: .double 0.29
 .size limit,.-limit;
 .text
.globl __ieee754_logl
.type __ieee754_logl,@function
.align 1<<4
__ieee754_logl:
 fldln2
 fldt 8(%rsp)
 fxam
 fnstsw
 fld %st
 testb $1, %ah
 jnz 3f
 movzwl 8+8(%rsp), %eax
 cmpl $0xc000, %eax
 jae 6f
4: fsubl one(%rip)
6: fld %st
 fabs
 fcompl limit(%rip)
 fnstsw
 andb $0x45, %ah
 jz 2f
 fxam
 fnstsw
 andb $0x45, %ah
 cmpb $0x40, %ah
 jne 5f
 fabs
5: fstp %st(1)
 fyl2xp1
 ret
2: fstp %st(0)
 fyl2x
 ret
3: testb $4, %ah
 jnz 4b
 fstp %st(1)
 fstp %st(1)
 fadd %st(0)
 ret
.size __ieee754_logl,.-__ieee754_logl
.globl __logl_finite
.type __logl_finite,@function
.align 1<<4
__logl_finite:
 fldln2
 fldt 8(%rsp)
 fld %st
 fsubl one(%rip)
 fld %st
 fabs
 fcompl limit(%rip)
 fnstsw
 andb $0x45, %ah
 jz 2b
 fxam
 fnstsw
 andb $0x45, %ah
 cmpb $0x40, %ah
 jne 7f
 fabs
7: fstp %st(1)
 fyl2xp1
 ret
.size __logl_finite,.-__logl_finite
.symver __logl_finite, __logl_finite@GLIBC_2.15
