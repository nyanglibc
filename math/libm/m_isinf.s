	.text
	.p2align 4,,15
	.globl	__isinf
	.type	__isinf, @function
__isinf:
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rax
	movabsq	$9218868437227405312, %rcx
	andq	%rdx, %rax
	sarq	$62, %rdx
	xorq	%rax, %rcx
	movq	%rcx, %rax
	negq	%rax
	orq	%rcx, %rax
	sarq	$63, %rax
	notl	%eax
	andl	%edx, %eax
	ret
	.size	__isinf, .-__isinf
	.weak	isinf
	.set	isinf,__isinf
