	.text
	.globl	__eqtf2
	.globl	__divtf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__gttf2
	.globl	__lttf2
	.globl	__multf3
	.p2align 4,,15
	.globl	__ieee754_atanhf128
	.type	__ieee754_atanhf128, @function
__ieee754_atanhf128:
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %rbp
	movdqa	(%rsp), %xmm4
	shrq	$32, %rbp
	movl	%eax, %eax
	movl	%ebp, %ebx
	andl	$2147483647, %ebx
	movq	%rbx, %rdx
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$1073676287, %ebx
	movaps	%xmm4, 16(%rsp)
	movq	%rax, 24(%rsp)
	jbe	.L2
	movdqa	.LC0(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L19
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1069940735, %ebx
	ja	.L6
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	pxor	%xmm1, %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L20
	movdqa	(%rsp), %xmm0
	pand	.LC3(%rip), %xmm0
	movdqa	.LC4(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L16
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movdqa	16(%rsp), %xmm5
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	movaps	%xmm5, 32(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__subtf3@PLT
	cmpl	$1073610751, %ebx
	movdqa	%xmm0, %xmm3
	movdqa	48(%rsp), %xmm2
	jbe	.L8
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__divtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__multf3@PLT
.L11:
	testl	%ebp, %ebp
	jns	.L1
	pxor	.LC6(%rip), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	16(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	movaps	%xmm6, 32(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm3
.L8:
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	movaps	%xmm3, 16(%rsp)
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm3
	movdqa	%xmm3, %xmm1
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	(%rsp), %xmm0
	jmp	.L1
	.size	__ieee754_atanhf128, .-__ieee754_atanhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	621136735
	.long	1233211794
	.long	139375215
	.long	2140429604
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
