.globl __signbitf
.type __signbitf,@function
.align 1<<4
__signbitf:
 pmovmskb %xmm0, %eax
 andl $0x8, %eax
 ret
.size __signbitf,.-__signbitf
