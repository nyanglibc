	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __ieee754_exp,__exp_finite@GLIBC_2.15
	.symver __exp,exp@@GLIBC_2.29
#NO_APP
	.p2align 4,,15
	.globl	__GI___exp
	.hidden	__GI___exp
	.type	__GI___exp, @function
__GI___exp:
	movq	%xmm0, %rdx
	movq	%xmm0, %rcx
	shrq	$52, %rdx
	andl	$2047, %edx
	leal	-969(%rdx), %eax
	cmpl	$62, %eax
	ja	.L21
.L2:
	movsd	__exp_data(%rip), %xmm2
	leaq	__exp_data(%rip), %rdi
	movsd	8+__exp_data(%rip), %xmm1
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movq	%xmm2, %rsi
	subsd	%xmm1, %xmm2
	movsd	16+__exp_data(%rip), %xmm1
	movq	%rsi, %rcx
	movq	%rsi, %rax
	andl	$127, %ecx
	salq	$45, %rax
	addq	%rcx, %rcx
	mulsd	%xmm2, %xmm1
	addq	120(%rdi,%rcx,8), %rax
	testl	%edx, %edx
	mulsd	24+__exp_data(%rip), %xmm2
	movsd	112(%rdi,%rcx,8), %xmm3
	addsd	%xmm1, %xmm0
	movsd	40+__exp_data(%rip), %xmm1
	addsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm1
	addsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm2
	mulsd	56+__exp_data(%rip), %xmm0
	addsd	32+__exp_data(%rip), %xmm1
	addsd	48+__exp_data(%rip), %xmm0
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	je	.L22
	movq	%rax, -16(%rsp)
	movsd	-16(%rsp), %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	testl	%eax, %eax
	js	.L5
	cmpl	$1032, %edx
	jbe	.L13
	movabsq	$-4503599627370496, %rax
	cmpq	%rax, %rcx
	je	.L14
	cmpl	$2047, %edx
	je	.L5
	xorl	%edi, %edi
	testq	%rcx, %rcx
	js	.L23
	jmp	__math_oflow
	.p2align 4,,10
	.p2align 3
.L22:
	testl	$2147483648, %esi
	je	.L24
	movabsq	$4602678819172646912, %rdx
	movsd	.LC1(%rip), %xmm3
	addq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm2
	mulsd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm3
	ja	.L9
	mulsd	.LC3(%rip), %xmm0
.L10:
	jmp	__math_check_uflow
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%edx, %edx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	subsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm4
	addsd	%xmm3, %xmm4
	addsd	%xmm2, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm0
	movsd	.LC3(%rip), %xmm2
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	addsd	%xmm4, %xmm1
	subsd	%xmm3, %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L16
	je	.L11
.L16:
	mulsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
.L11:
	movapd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm1
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L5:
	addsd	.LC1(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movabsq	$-4544132024016830464, %rdx
	addq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm0
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm0
	mulsd	.LC2(%rip), %xmm0
	jmp	__math_check_oflow
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm0, %xmm0
	ret
.L23:
	jmp	__math_uflow
	.size	__GI___exp, .-__GI___exp
	.globl	__exp
	.set	__exp,__GI___exp
	.weak	expf32x
	.set	expf32x,__exp
	.weak	expf64
	.set	expf64,__exp
	.globl	__ieee754_exp
	.set	__ieee754_exp,__exp
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	2130706432
	.align 8
.LC3:
	.long	0
	.long	1048576
	.hidden	__math_uflow
	.hidden	__math_check_oflow
	.hidden	__math_check_uflow
	.hidden	__math_oflow
	.hidden	__exp_data
