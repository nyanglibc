	.text
	.globl	__multf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__scalbnf128
	.type	__scalbnf128, @function
__scalbnf128:
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	sarq	$48, %rax
	andl	$32767, %eax
	jne	.L2
	movabsq	$9223372036854775807, %rax
	movl	%edi, 28(%rsp)
	andq	%rax, %rdx
	movq	(%rsp), %rax
	orq	%rax, %rdx
	je	.L1
	movdqa	.LC0(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movl	28(%rsp), %edi
	movq	%rdx, %rax
	sarq	$48, %rax
	andl	$32767, %eax
	subq	$114, %rax
.L4:
	cmpl	$-50000, %edi
	jl	.L13
	cmpl	$50000, %edi
	jg	.L6
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	$32766, %rax
	jg	.L6
	testq	%rax, %rax
	jle	.L8
	movabsq	$-9223090561878065153, %rcx
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$32767, %rax
	jne	.L4
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movdqa	(%rsp), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__multf3@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movdqa	(%rsp), %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__multf3@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpq	$-113, %rax
	jl	.L13
	addq	$114, %rax
	movabsq	$-9223090561878065153, %rcx
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rdx, %rax
	movdqa	.LC3(%rip), %xmm1
	movq	%rax, 8(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L1
	.size	__scalbnf128, .-__scalbnf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1081147392
	.align 16
.LC1:
	.long	1152451630
	.long	1390727765
	.long	3199038956
	.long	6911849
	.align 16
.LC2:
	.long	621136735
	.long	1233211794
	.long	139375215
	.long	2140429604
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1066205184
