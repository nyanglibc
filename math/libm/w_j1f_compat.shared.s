	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__j1f
	.type	__j1f, @function
__j1f:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	cvtss2sd	%xmm1, %xmm1
	ucomisd	.LC1(%rip), %xmm1
	ja	.L10
.L2:
	jmp	__ieee754_j1f@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	movaps	%xmm0, %xmm1
	movl	$136, %edi
	jmp	__kernel_standard_f@PLT
	.size	__j1f, .-__j1f
	.weak	j1f32
	.set	j1f32,__j1f
	.weak	j1f
	.set	j1f,__j1f
	.p2align 4,,15
	.globl	__y1f
	.type	__y1f, @function
__y1f:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L18
	ucomiss	.LC3(%rip), %xmm0
	ja	.L18
.L32:
	jmp	__ieee754_y1f@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L32
	subq	$24, %rsp
	movaps	%xmm0, %xmm2
	ucomiss	%xmm0, %xmm1
	ja	.L36
	ucomiss	%xmm1, %xmm0
	jp	.L16
	je	.L37
.L16:
	cmpl	$2, %eax
	jne	.L38
	addq	$24, %rsp
	jmp	__ieee754_y1f@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$4, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	movl	$110, %edi
.L35:
	movss	12(%rsp), %xmm2
	addq	$24, %rsp
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	movaps	%xmm2, %xmm1
	movl	$137, %edi
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	movl	$111, %edi
	jmp	.L35
	.size	__y1f, .-__y1f
	.weak	y1f32
	.set	y1f32,__y1f
	.weak	y1f
	.set	y1f,__y1f
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1413754136
	.long	1128866299
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	1514737627
