	.text
	.p2align 4,,15
	.globl	__mpatan2
	.type	__mpatan2, @function
__mpatan2:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movl	%ecx, %ebx
	movq	%rsi, %rdi
	subq	$1016, %rsp
	cmpq	$0, 8(%rsi)
	movq	%rsp, %rbp
	movq	%rbp, %rdx
	jle	.L7
	movq	%r12, %rdi
	call	__dvd@PLT
	movl	%ebx, %edx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__mpatan@PLT
.L1:
	addq	$1016, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	336(%rsp), %r15
	movq	%r12, %rsi
	call	__dvd@PLT
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mul@PLT
	cmpq	$0, 8(%rsp)
	je	.L3
	movq	$1, 8(%rsp)
.L3:
	leaq	672(%rsp), %r14
	leaq	__mpone(%rip), %rsi
	movl	%ebx, %ecx
	movq	%r15, %rdi
	movq	%r14, %rdx
	call	__add@PLT
	movl	%ebx, %edx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	__mpsqrt@PLT
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	__add@PLT
	movq	8(%r12), %rax
	movl	%ebx, %edx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	movq	%rax, 680(%rsp)
	call	__mpatan@PLT
	movl	%ebx, %ecx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__add@PLT
	jmp	.L1
	.size	__mpatan2, .-__mpatan2
