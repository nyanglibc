	.text
	.p2align 4,,15
	.globl	__issignalingf128
	.type	__issignalingf128, @function
__issignalingf128:
	movaps	%xmm0, -24(%rsp)
	movq	-24(%rsp), %rcx
	movq	-16(%rsp), %rdx
	movabsq	$140737488355328, %rax
	xorq	%rax, %rdx
	movq	%rcx, %rax
	negq	%rax
	orq	%rcx, %rax
	shrq	$63, %rax
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	movabsq	$9223231299366420480, %rdx
	cmpq	%rdx, %rax
	seta	%al
	movzbl	%al, %eax
	ret
	.size	__issignalingf128, .-__issignalingf128
