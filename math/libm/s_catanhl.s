	.text
	.p2align 4,,15
	.globl	__catanhl
	.type	__catanhl, @function
__catanhl:
	subq	$88, %rsp
	fldt	96(%rsp)
	fldt	112(%rsp)
	fld	%st(1)
	fabs
	fucomi	%st(0), %st
	jp	.L55
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L117
	movl	$1, %eax
.L3:
	fld	%st(1)
	fabs
	fucomi	%st(0), %st
	jp	.L118
.L53:
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L120
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L6
	fldz
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jp	.L6
	je	.L7
.L6:
	cmpl	$1, %eax
	jle	.L121
.L10:
	flds	.LC7(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	jnb	.L122
	fxch	%st(1)
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L123
	fld1
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	flds	.LC11(%rip)
	jp	.L24
	je	.L67
.L24:
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jb	.L105
	fld	%st(2)
	fmul	%st(3), %st
.L30:
	fld1
	movss	.LC9(%rip), %xmm0
	movss	%xmm0, (%rsp)
	fld	%st(5)
	fadd	%st(1), %st
	fmul	%st(0), %st
	fxch	%st(1)
	fsub	%st(6), %st
	fmul	%st(0), %st
	fadd	%st(2), %st
	fxch	%st(1)
	faddp	%st, %st(2)
	fdivr	%st, %st(1)
	flds	(%rsp)
	fxch	%st(5)
	fstpt	48(%rsp)
	fxch	%st(3)
	fstpt	32(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fxch	%st(2)
	fucomip	%st(2), %st
	jbe	.L106
	fstp	%st(0)
	fstp	%st(1)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	popq	%rcx
	fmuls	.LC10(%rip)
	popq	%rsi
	fldt	16(%rsp)
	fldt	48(%rsp)
	fldt	32(%rsp)
.L34:
	fucomi	%st(2), %st
	jbe	.L35
	fxch	%st(2)
.L35:
	flds	.LC15(%rip)
	fucomip	%st(1), %st
	jbe	.L107
	fstp	%st(0)
	fld1
	fld	%st(0)
	fsub	%st(3), %st
	fxch	%st(1)
	faddp	%st, %st(3)
	fmulp	%st, %st(2)
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	jp	.L124
	fcmove	%st(2), %st
	fstp	%st(2)
	jmp	.L39
.L124:
	fstp	%st(2)
	.p2align 4,,10
	.p2align 3
.L39:
	fld	%st(0)
	faddp	%st, %st(1)
	fxch	%st(1)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	fmuls	(%rsp)
	fld	%st(1)
	fld	%st(1)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L117:
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L57
	fldz
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jp	.L63
	jne	.L63
	movl	$2, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L122:
	fstp	%st(2)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L123:
	fxch	%st(2)
.L15:
	fxam
	fnstsw	%ax
	testb	$2, %ah
	fldt	.LC5(%rip)
	je	.L18
	fstp	%st(0)
	fldt	.LC6(%rip)
.L18:
	fld1
	fucomi	%st(3), %st
	fstp	%st(3)
	jnb	.L119
	fxch	%st(2)
	fucomip	%st(3), %st
	fstp	%st(2)
	jb	.L102
	fxch	%st(2)
	fdiv	%st(1), %st
	fdivp	%st, %st(1)
	fld	%st(0)
	fld	%st(2)
.L21:
	fldt	.LC3(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L125
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L125:
	fstp	%st(2)
	fxch	%st(1)
.L46:
	fldt	.LC3(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L126
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L126:
	fstp	%st(2)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L128:
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L130:
	fstp	%st(5)
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
.L1:
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L135:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L51:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L127
	fstp	%st(0)
	fldz
	fchs
	fxch	%st(1)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L127:
	fxch	%st(1)
.L49:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC5(%rip)
	je	.L128
	fstp	%st(0)
	fldt	.LC6(%rip)
	addq	$88, %rsp
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$4, %eax
.L2:
	fld	%st(1)
	fabs
	fucomi	%st(0), %st
	jnp	.L53
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L119:
	fstp	%st(3)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	fdivrp	%st, %st(1)
	fld	%st(0)
	fld	%st(2)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$1, %eax
	jle	.L129
	fld	%st(3)
	cmpl	$2, %eax
	fld	%st(3)
	je	.L130
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$3, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L67:
	fld	%st(0)
	fucomip	%st(3), %st
	jbe	.L24
	fstp	%st(0)
	fxch	%st(3)
	movss	.LC9(%rip), %xmm1
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	movss	%xmm1, (%rsp)
	flds	(%rsp)
	je	.L131
	fstp	%st(0)
	flds	.LC12(%rip)
	fxch	%st(2)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L131:
	fxch	%st(2)
.L27:
	fstpt	64(%rsp)
	fxch	%st(2)
	subq	$16, %rsp
	fstpt	64(%rsp)
	fstpt	48(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__ieee754_logl@PLT
	fldln2
	fsubp	%st, %st(1)
	fldt	48(%rsp)
	popq	%rdi
	popq	%r8
	fmulp	%st, %st(1)
	fld1
	fldt	16(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	fldt	48(%rsp)
	fldt	64(%rsp)
	jbe	.L104
	fxch	%st(2)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L132:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L28
.L136:
	fxch	%st(2)
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L28:
	fld1
	fld	%st(0)
	fsub	%st(2), %st
	fxch	%st(2)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fxch	%st(1)
	fmul	%st(0), %st
	fsubrp	%st, %st(1)
	fxch	%st(1)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L102:
	fstpt	16(%rsp)
	subq	$32, %rsp
	flds	.LC9(%rip)
	fmul	%st, %st(1)
	fxch	%st(1)
	fstpt	16(%rsp)
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__ieee754_hypotl@PLT
	fldt	32(%rsp)
	fdiv	%st(1), %st
	fdivp	%st, %st(1)
	fmuls	.LC10(%rip)
	fld	%st(0)
	fldt	48(%rsp)
	addq	$32, %rsp
	fld	%st(0)
	fxch	%st(3)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L55:
	xorl	%eax, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L105:
	fldz
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L106:
	fstp	%st(1)
	fxch	%st(1)
	fmuls	.LC14(%rip)
	subq	$16, %rsp
	fdivp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	popq	%rax
	fmuls	.LC10(%rip)
	popq	%rdx
	fldt	48(%rsp)
	fldt	32(%rsp)
	fldt	16(%rsp)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L107:
	fld1
	fxch	%st(3)
	fucomi	%st(3), %st
	jnb	.L132
	flds	.LC16(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L133
	flds	(%rsp)
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L134
	fld	%st(3)
	fsub	%st(1), %st
	fxch	%st(1)
	faddp	%st, %st(4)
	fmulp	%st, %st(3)
	fmul	%st(0), %st
	fsubrp	%st, %st(2)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L121:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L129:
	fstp	%st(0)
	fstp	%st(0)
.L9:
	testl	%eax, %eax
	jne	.L135
	fstp	%st(0)
	fstp	%st(0)
.L114:
	flds	.LC1(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L133:
	fstp	%st(3)
	fxch	%st(1)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L134:
	fstp	%st(3)
	fxch	%st(1)
.L43:
	fstpt	32(%rsp)
	fxch	%st(2)
	subq	$32, %rsp
	fstpt	48(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__x2y2m1l@PLT
	fchs
	addq	$32, %rsp
	fldt	32(%rsp)
	fldt	16(%rsp)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L104:
	flds	.LC15(%rip)
	fucomip	%st(3), %st
	jbe	.L136
	fstp	%st(2)
	fstp	%st(0)
	fldz
	fxch	%st(1)
	jmp	.L39
.L118:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L52
	fstp	%st(0)
	fldz
	fchs
.L52:
	flds	.LC1(%rip)
	fxch	%st(1)
	jmp	.L1
	.size	__catanhl, .-__catanhl
	.weak	catanhf64x
	.set	catanhf64x,__catanhl
	.weak	catanhl
	.set	catanhl,__catanhl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC3:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC5:
	.long	560513589
	.long	3373259426
	.long	16383
	.long	0
	.align 16
.LC6:
	.long	560513589
	.long	3373259426
	.long	49151
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1627389952
	.align 4
.LC9:
	.long	1056964608
	.align 4
.LC10:
	.long	1048576000
	.align 4
.LC11:
	.long	8388608
	.align 4
.LC12:
	.long	3204448256
	.align 4
.LC14:
	.long	1082130432
	.align 4
.LC15:
	.long	528482304
	.align 4
.LC16:
	.long	1061158912
