	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__j0
	.type	__j0, @function
__j0:
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	.LC1(%rip), %xmm1
	ja	.L10
.L2:
	jmp	__ieee754_j0@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	movapd	%xmm0, %xmm1
	movl	$34, %edi
	jmp	__kernel_standard@PLT
	.size	__j0, .-__j0
	.weak	j0f32x
	.set	j0f32x,__j0
	.weak	j0f64
	.set	j0f64,__j0
	.weak	j0
	.set	j0,__j0
	.p2align 4,,15
	.globl	__y0
	.type	__y0, @function
__y0:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L18
	ucomisd	.LC1(%rip), %xmm0
	ja	.L18
.L32:
	jmp	__ieee754_y0@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L32
	subq	$24, %rsp
	movapd	%xmm0, %xmm2
	ucomisd	%xmm0, %xmm1
	ja	.L36
	ucomisd	%xmm1, %xmm0
	jp	.L16
	je	.L37
.L16:
	cmpl	$2, %eax
	jne	.L38
	addq	$24, %rsp
	jmp	__ieee754_y0@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$4, %edi
	movsd	%xmm0, 8(%rsp)
	call	__GI___feraiseexcept
	movl	$8, %edi
.L35:
	movsd	8(%rsp), %xmm2
	addq	$24, %rsp
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	movapd	%xmm2, %xmm1
	movl	$35, %edi
	movapd	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %edi
	movsd	%xmm0, 8(%rsp)
	call	__GI___feraiseexcept
	movl	$9, %edi
	jmp	.L35
	.size	__y0, .-__y0
	.weak	y0f32x
	.set	y0f32x,__y0
	.weak	y0f64
	.set	y0f64,__y0
	.weak	y0
	.set	y0,__y0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1413754136
	.long	1128866299
