.section .rodata.cst16,"aM",@progbits,16
.p2align 4
.type ldbl_min,@object
ldbl_min:
.byte 0, 0, 0, 0, 0, 0, 0, 0x80, 0x1, 0
.byte 0, 0, 0, 0, 0, 0
.size ldbl_min, .-ldbl_min
 .text
.globl __ieee754_exp2l
.type __ieee754_exp2l,@function
.align 1<<4
__ieee754_exp2l:
 fldt 8(%rsp)
 fxam
 fstsw %ax
 movb $0x45, %dh
 andb %ah, %dh
 cmpb $0x05, %dh
 je 1f
 movzwl 8+8(%rsp), %eax
 andl $0x7fff, %eax
 cmpl $0x3fbe, %eax
 jge 3f
 fld1
 faddp
 ret
3: fld %st
 frndint
 fsubr %st,%st(1)
 fxch
 f2xm1
 fld1
 faddp
 fscale
 fstp %st(1)
 fldt ldbl_min(%rip)
 fld %st(1)
 fucomip %st(1), %st(0)
 fstp %st(0)
 jnc 6464f
 fld %st(0)
 fmul %st(0)
 fstp %st(0)
6464:
 ret
1: testl $0x200, %eax
 jz 2f
 fstp %st
 fldz
2: ret
.size __ieee754_exp2l,.-__ieee754_exp2l
.symver __ieee754_exp2l, __exp2l_finite@GLIBC_2.15
