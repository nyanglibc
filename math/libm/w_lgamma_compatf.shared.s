	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __lgammaf_compat,lgammaf@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__lgammaf_compat
	.type	__lgammaf_compat, @function
__lgammaf_compat:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	movss	%xmm0, 12(%rsp)
	call	__ieee754_lgammaf_r@PLT
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movss	12(%rsp), %xmm2
	movl	(%rax), %eax
	cmpl	$3, %eax
	je	.L2
	movl	28(%rsp), %edx
	movq	__signgam@GOTPCREL(%rip), %rcx
	movl	%edx, (%rcx)
	movq	signgam@GOTPCREL(%rip), %rcx
	movl	%edx, (%rcx)
.L2:
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm3
	jb	.L22
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movaps	%xmm2, %xmm4
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm3
	jb	.L1
	cmpl	$-1, %eax
	je	.L1
	movaps	%xmm2, %xmm0
	movss	.LC2(%rip), %xmm4
	movaps	%xmm2, %xmm3
	andps	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm4
	jbe	.L5
	cvttss2si	%xmm2, %eax
	pxor	%xmm0, %xmm0
	movss	.LC3(%rip), %xmm4
	andnps	%xmm2, %xmm1
	cvtsi2ss	%eax, %xmm0
	movaps	%xmm0, %xmm3
	cmpnless	%xmm2, %xmm3
	andps	%xmm4, %xmm3
	subss	%xmm3, %xmm0
	movaps	%xmm0, %xmm3
	orps	%xmm1, %xmm3
.L5:
	ucomiss	%xmm3, %xmm2
	jp	.L9
	jne	.L9
	pxor	%xmm0, %xmm0
	xorl	%edi, %edi
	ucomiss	%xmm2, %xmm0
	setnb	%dil
	addl	$114, %edi
.L6:
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	call	__kernel_standard_f@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$114, %edi
	jmp	.L6
	.size	__lgammaf_compat, .-__lgammaf_compat
	.globl	__gammaf
	.set	__gammaf,__lgammaf_compat
	.weak	gammaf
	.set	gammaf,__gammaf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	1258291200
	.align 4
.LC3:
	.long	1065353216
