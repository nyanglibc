	.text
#APP
	.symver __ieee754_coshl,__coshl_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_coshl
	.type	__ieee754_coshl, @function
__ieee754_coshl:
	subq	$24, %rsp
	movq	32(%rsp), %rax
	shrq	$32, %rax
	movq	%rax, %rdx
	movq	40(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16386, %ax
	movswl	%ax, %ecx
	jle	.L2
	cmpl	$16387, %ecx
	jne	.L5
	cmpl	$-1342177281, %edx
	ja	.L5
.L3:
	cmpl	$16381, %ecx
	jne	.L9
	cmpl	$-1317922826, %edx
	ja	.L9
.L7:
	fldt	32(%rsp)
	subq	$16, %rsp
	fabs
	fstpt	(%rsp)
	call	__GI___expm1l
	fld	%st(0)
	popq	%r9
	fmul	%st(1), %st
	popq	%r10
	fld1
	addq	$24, %rsp
	fadd	%st, %st(2)
	fxch	%st(2)
	fadd	%st(0), %st
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpw	$16380, %ax
	jg	.L3
	cmpw	$16315, %ax
	jg	.L7
	fld1
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpw	$16395, %ax
	jle	.L11
	cmpl	$16396, %ecx
	jne	.L12
	cmpl	$-1318060033, %edx
	ja	.L12
.L11:
	fldt	32(%rsp)
	subq	$16, %rsp
	fabs
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	popq	%rcx
	popq	%rsi
	fmuls	.LC1(%rip)
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	fldt	32(%rsp)
	subq	$16, %rsp
	fabs
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	flds	.LC1(%rip)
	popq	%rdi
	fld	%st(1)
	popq	%r8
	fmul	%st(1), %st
	fxch	%st(2)
	addq	$24, %rsp
	fdivrp	%st, %st(1)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$16396, %ecx
	je	.L37
	cmpl	$32767, %ecx
	je	.L38
.L15:
	fldt	.LC2(%rip)
	fmul	%st(0), %st
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	fldt	32(%rsp)
	fmul	%st(0), %st
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	$-1317741121, %edx
	jbe	.L14
	cmpl	$-1317741120, %edx
	jne	.L15
	movq	32(%rsp), %rax
	cmpl	$833536234, %eax
	ja	.L15
.L14:
	fldt	32(%rsp)
	subq	$16, %rsp
	fabs
	flds	.LC1(%rip)
	fmul	%st, %st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	16(%rsp)
	popq	%rax
	popq	%rdx
	fmul	%st(1), %st
	fmulp	%st, %st(1)
	jmp	.L1
	.size	__ieee754_coshl, .-__ieee754_coshl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1056964608
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	1496818881
	.long	2928804903
	.long	32660
	.long	0
