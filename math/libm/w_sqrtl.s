	.text
	.p2align 4,,15
	.globl	__sqrtl
	.type	__sqrtl, @function
__sqrtl:
	fldt	8(%rsp)
	fldz
	fucomip	%st(1), %st
	ja	.L4
	fstpt	8(%rsp)
	jmp	__ieee754_sqrtl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	fstpt	8(%rsp)
	jmp	__ieee754_sqrtl@PLT
	.size	__sqrtl, .-__sqrtl
	.weak	sqrtf64x
	.set	sqrtf64x,__sqrtl
	.weak	sqrtl
	.set	sqrtl,__sqrtl
