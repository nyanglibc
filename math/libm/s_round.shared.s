	.text
	.p2align 4,,15
	.globl	__round
	.type	__round, @function
__round:
	movq	%xmm0, %rcx
	movq	%xmm0, %rdi
	sarq	$52, %rcx
	andl	$2047, %ecx
	subq	$1023, %rcx
	cmpq	$51, %rcx
	jg	.L2
	testq	%rcx, %rcx
	js	.L10
	movabsq	$4503599627370495, %rsi
	shrq	%cl, %rsi
	testq	%rsi, %rdi
	je	.L1
	movabsq	$2251799813685248, %rdx
	notq	%rsi
	shrq	%cl, %rdx
	leaq	(%rdx,%rdi), %rax
	andq	%rsi, %rax
.L4:
	movq	%rax, -8(%rsp)
	movsd	-8(%rsp), %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	movabsq	$-9223372036854775808, %rax
	andq	%rdi, %rax
	cmpq	$-1, %rcx
	jne	.L4
	movabsq	$4607182418800017408, %rdx
	orq	%rdx, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$1024, %rcx
	jne	.L1
	addsd	%xmm0, %xmm0
	ret
	.size	__round, .-__round
	.weak	roundf32x
	.set	roundf32x,__round
	.weak	roundf64
	.set	roundf64,__round
	.weak	round
	.set	round,__round
