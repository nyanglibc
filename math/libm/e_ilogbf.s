	.text
	.p2align 4,,15
	.globl	__ieee754_ilogbf
	.type	__ieee754_ilogbf, @function
__ieee754_ilogbf:
	movd	%xmm0, %edx
	andl	$2147483647, %edx
	cmpl	$8388607, %edx
	jg	.L2
	testl	%edx, %edx
	je	.L6
	sall	$8, %edx
	movl	$-126, %eax
	.p2align 4,,10
	.p2align 3
.L4:
	addl	%edx, %edx
	subl	$1, %eax
	testl	%edx, %edx
	jg	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$2139095039, %edx
	jle	.L10
	xorl	%eax, %eax
	cmpl	$2139095040, %edx
	setne	%al
	addl	$2147483647, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	sarl	$23, %edx
	leal	-127(%rdx), %eax
	ret
.L6:
	movl	$-2147483648, %eax
	ret
	.size	__ieee754_ilogbf, .-__ieee754_ilogbf
