	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpl
	.type	__fromfpl, @function
__fromfpl:
	cmpl	$64, %esi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	32(%rsp), %r9
	movl	40(%rsp), %r10d
	ja	.L35
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$64, %esi
.L10:
	movq	%r9, %rax
	movl	%r9d, %ebx
	movq	%r9, %rdx
	shrq	$32, %rax
	orl	%eax, %ebx
	je	.L36
	movswl	%r10w, %r9d
	movl	%r10d, %r8d
	shrl	$31, %r9d
	andl	$32767, %r8d
	leal	-2(%rsi,%r9), %r11d
	subl	$16383, %r8d
	cmpl	%r11d, %r8d
	jg	.L32
	salq	$32, %rax
	movl	%edx, %edx
	orq	%rax, %rdx
	cmpl	$63, %r8d
	je	.L37
	cmpl	$-1, %r8d
	jl	.L38
	movl	$62, %ecx
	movl	$1, %eax
	movq	%rdx, %r12
	subl	%r8d, %ecx
	salq	%cl, %rax
	andq	%rax, %r12
	setne	%bl
	subq	$1, %rax
	andq	%rdx, %rax
	setne	%bpl
	cmpl	$-1, %r8d
	je	.L39
	movl	$63, %ecx
	subl	%r8d, %ecx
	shrq	%cl, %rdx
.L16:
	cmpl	$1, %edi
	je	.L18
	jle	.L62
	cmpl	$3, %edi
	je	.L21
	cmpl	$4, %edi
	jne	.L17
	testq	%r12, %r12
	je	.L40
	movq	%rdx, %rcx
	andl	$1, %ecx
	orq	%rax, %rcx
	setne	%al
	movzbl	%al, %eax
.L30:
	addq	%rax, %rdx
.L17:
	testw	%r10w, %r10w
	js	.L23
.L26:
	leal	1(%r11), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rcx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	jne	.L9
.L32:
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	%r9d, %edi
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
.L9:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%ebp, %ebp
.L15:
	cmpl	$1, %edi
	je	.L42
	jle	.L63
	cmpl	$3, %edi
	je	.L43
	xorl	%eax, %eax
	cmpl	$4, %edi
	je	.L30
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%ebx, %ebx
.L62:
	testl	%edi, %edi
	jne	.L17
	testw	%r10w, %r10w
	js	.L23
	testb	%bl, %bl
	jne	.L45
	testb	%bpl, %bpl
	je	.L26
.L45:
	addq	$1, %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%ebx, %ebx
.L18:
	testw	%r10w, %r10w
	jns	.L26
	testb	%bl, %bl
	jne	.L46
	testb	%bpl, %bpl
	je	.L23
.L46:
	addq	$1, %rdx
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	%r11d, %r8d
	je	.L64
.L31:
	movq	%rdx, %rax
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%ebx, %ebx
.L29:
	addq	%rbx, %rdx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$1, %ebp
	xorl	%edx, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$1, %eax
	movl	%r8d, %ecx
	salq	%cl, %rax
	cmpq	%rax, %rdx
	jne	.L32
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%edx, %edx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	%bl, %ebx
	jmp	.L29
.L40:
	xorl	%eax, %eax
	jmp	.L30
	.size	__fromfpl, .-__fromfpl
	.weak	fromfpf64x
	.set	fromfpf64x,__fromfpl
	.weak	fromfpl
	.set	fromfpl,__fromfpl
