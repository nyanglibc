	.text
	.p2align 4,,15
	.globl	__kernel_tanf
	.type	__kernel_tanf, @function
__kernel_tanf:
	movd	%xmm0, %edx
	movaps	%xmm0, %xmm3
	movd	%xmm0, %eax
	andl	$2147483647, %edx
	cmpl	$956301311, %edx
	jg	.L2
	cvttss2si	%xmm0, %ecx
	testl	%ecx, %ecx
	jne	.L3
	leal	1(%rdi), %eax
	orl	%edx, %eax
	je	.L21
	cmpl	$1, %edi
	je	.L22
	movss	.LC4(%rip), %xmm0
	divss	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1059889471, %edx
	jle	.L3
	testl	%eax, %eax
	jns	.L8
	movss	.LC5(%rip), %xmm0
	xorps	%xmm0, %xmm3
	xorps	%xmm0, %xmm1
.L8:
	movss	.LC6(%rip), %xmm0
	subss	%xmm3, %xmm0
	movss	.LC7(%rip), %xmm3
	subss	%xmm1, %xmm3
	movss	.LC8(%rip), %xmm1
	addss	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	andps	.LC1(%rip), %xmm0
	ucomiss	%xmm0, %xmm1
	ja	.L23
	pxor	%xmm1, %xmm1
.L3:
	movaps	%xmm3, %xmm4
	movss	.LC9(%rip), %xmm2
	movss	.LC15(%rip), %xmm5
	cmpl	$1059889471, %edx
	mulss	%xmm3, %xmm4
	movaps	%xmm3, %xmm0
	movaps	%xmm4, %xmm6
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm6
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm5
	addss	.LC10(%rip), %xmm2
	addss	.LC16(%rip), %xmm5
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm5
	addss	.LC11(%rip), %xmm2
	addss	.LC17(%rip), %xmm5
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm5
	addss	.LC12(%rip), %xmm2
	addss	.LC18(%rip), %xmm5
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm5
	addss	.LC13(%rip), %xmm2
	addss	.LC19(%rip), %xmm5
	mulss	%xmm6, %xmm2
	mulss	%xmm6, %xmm5
	addss	.LC14(%rip), %xmm2
	addss	.LC20(%rip), %xmm5
	mulss	%xmm4, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm0, %xmm2
	mulss	.LC21(%rip), %xmm0
	addss	%xmm1, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	jg	.L24
	cmpl	$1, %edi
	je	.L1
	movss	.LC4(%rip), %xmm5
	movd	%xmm0, %eax
	divss	%xmm0, %xmm5
	andl	$-4096, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm4
	movaps	%xmm4, %xmm0
	subss	%xmm3, %xmm0
	subss	%xmm0, %xmm1
	movss	.LC2(%rip), %xmm0
	movd	%xmm5, %eax
	andl	$-4096, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm2
	mulss	%xmm2, %xmm4
	mulss	%xmm2, %xmm1
	addss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm5, %xmm0
	addss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pxor	%xmm2, %xmm2
	sarl	$30, %eax
	movaps	%xmm0, %xmm4
	andl	$2, %eax
	movl	%eax, %edx
	movl	$1, %eax
	cvtsi2ss	%edi, %xmm2
	subl	%edx, %eax
	mulss	%xmm0, %xmm4
	addss	%xmm2, %xmm0
	divss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	subss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	pxor	%xmm0, %xmm0
	cvtsi2ss	%eax, %xmm0
	addss	%xmm3, %xmm3
	subss	%xmm3, %xmm2
	mulss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	andps	.LC1(%rip), %xmm0
	movss	.LC3(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	ja	.L25
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L21:
	andps	.LC1(%rip), %xmm3
	movss	.LC2(%rip), %xmm0
	divss	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	sarl	$30, %eax
	pxor	%xmm1, %xmm1
	andl	$2, %eax
	pxor	%xmm0, %xmm0
	movl	%eax, %edx
	movl	$1, %eax
	subl	%edx, %eax
	imull	%edi, %eax
	addl	%edi, %edi
	cvtsi2ss	%edi, %xmm1
	cvtsi2ss	%eax, %xmm0
	mulss	%xmm1, %xmm3
	movss	.LC2(%rip), %xmm1
	subss	%xmm3, %xmm1
	mulss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	mulss	%xmm3, %xmm0
	movaps	%xmm3, %xmm0
	ret
	.size	__kernel_tanf, .-__kernel_tanf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1065353216
	.align 4
.LC3:
	.long	8388608
	.align 4
.LC4:
	.long	3212836864
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	1061752794
	.align 4
.LC7:
	.long	857874792
	.align 4
.LC8:
	.long	956301312
	.align 4
.LC9:
	.long	936989572
	.align 4
.LC10:
	.long	949338234
	.align 4
.LC11:
	.long	964769721
	.align 4
.LC12:
	.long	985587272
	.align 4
.LC13:
	.long	1007761183
	.align 4
.LC14:
	.long	1029508561
	.align 4
.LC15:
	.long	3080433247
	.align 4
.LC16:
	.long	950268997
	.align 4
.LC17:
	.long	974792392
	.align 4
.LC18:
	.long	996894998
	.align 4
.LC19:
	.long	1018374052
	.align 4
.LC20:
	.long	1040746633
	.align 4
.LC21:
	.long	1051372203
