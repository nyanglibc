	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fminmagf
	.type	__fminmagf, @function
__fminmagf:
	movss	.LC0(%rip), %xmm2
	movaps	%xmm0, %xmm3
	andps	%xmm2, %xmm3
	andps	%xmm1, %xmm2
	ucomiss	%xmm3, %xmm2
	ja	.L1
	ucomiss	%xmm2, %xmm3
	ja	.L19
	jp	.L5
	je	.L21
.L5:
	movd	%xmm0, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	ja	.L8
	movd	%xmm1, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	ja	.L8
	movaps	%xmm1, %xmm2
	cmpordss	%xmm1, %xmm2
	andps	%xmm2, %xmm1
	andnps	%xmm0, %xmm2
	movaps	%xmm2, %xmm0
	orps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movaps	%xmm1, %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L21:
	minss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	addss	%xmm1, %xmm0
	ret
	.size	__fminmagf, .-__fminmagf
	.weak	fminmagf32
	.set	fminmagf32,__fminmagf
	.weak	fminmagf
	.set	fminmagf,__fminmagf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
