	.text
	.p2align 4,,15
	.globl	__lgammal_r
	.type	__lgammal_r, @function
__lgammal_r:
	subq	$8, %rsp
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__ieee754_lgammal_r@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L7
	fstp	%st(0)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fldt	16(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__lgammal_r, .-__lgammal_r
	.weak	lgammaf64x_r
	.set	lgammaf64x_r,__lgammal_r
	.weak	lgammal_r
	.set	lgammal_r,__lgammal_r
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
