	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__csinhl
	.type	__csinhl, @function
__csinhl:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%rbx
	subq	$88, %rsp
	fldt	32(%rbp)
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fld	%st(0)
	movl	%eax, %ebx
	andl	$512, %ebx
	fabs
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L12
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L83
	fstp	%st(0)
	fstp	%st(2)
	fucomi	%st(0), %st
	jp	.L88
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L89
	fld	%st(1)
	fldt	.LC6(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jb	.L84
.L16:
	fldt	.LC6(%rip)
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jbe	.L76
	fstp	%st(0)
	subq	$16, %rsp
	leaq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	fldt	-32(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	flds	.LC2(%rip)
	testb	$2, %ah
	fld	%st(0)
	je	.L38
	fstp	%st(0)
	flds	.LC3(%rip)
.L38:
	fldt	-48(%rbp)
	popq	%rax
	popq	%rdx
.L36:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L39
	fstp	%st(1)
	flds	.LC3(%rip)
	fxch	%st(1)
.L39:
	testl	%ebx, %ebx
	je	.L80
	fchs
.L1:
	movq	-8(%rbp), %rbx
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	fldt	.LC6(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L4
	fldz
	fxch	%st(4)
	fucomip	%st(4), %st
	fstp	%st(3)
	jp	.L90
	jne	.L91
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L92
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L49
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L92:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
.L10:
	testl	%ebx, %ebx
	fldz
	jne	.L85
.L35:
	fld	%st(1)
	fsubp	%st, %st(2)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	fstp	%st(3)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L93
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L49
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L93:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L94:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L95:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
.L8:
	movl	$1, %edi
	call	__GI_feraiseexcept
	flds	.LC4(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L84:
	fldz
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jp	.L16
	jne	.L16
	fstp	%st(0)
	fstp	%st(0)
	testl	%ebx, %ebx
	jne	.L86
	flds	.LC2(%rip)
.L46:
	jmp	.L1
.L90:
	fstp	%st(0)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L91:
	fstp	%st(0)
.L54:
	fucomi	%st(0), %st
	jp	.L94
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L95
.L49:
	fldln2
	fnstcw	-50(%rbp)
	movzwl	-50(%rbp), %eax
	fmuls	.LC8(%rip)
	orb	$12, %ah
	movw	%ax, -52(%rbp)
	fldcw	-52(%rbp)
	fistpl	-80(%rbp)
	fldcw	-50(%rbp)
	fldt	.LC6(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L73
	fstpt	-96(%rbp)
	subq	$16, %rsp
	leaq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	popq	%r9
	popq	%r10
	fldt	-96(%rbp)
.L21:
	testl	%ebx, %ebx
	je	.L22
	fldt	-32(%rbp)
	fchs
	fstpt	-32(%rbp)
.L22:
	fildl	-80(%rbp)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L74
	fstpt	-96(%rbp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	-80(%rbp)
	call	__ieee754_expl@PLT
	fld	%st(0)
	popq	%rdi
	fldt	-80(%rbp)
	popq	%r8
	fldt	-96(%rbp)
	fsub	%st(1), %st
	fxch	%st(3)
	fmuls	.LC10(%rip)
	fldt	-48(%rbp)
	fmul	%st(1), %st
	fld	%st(0)
	fstpt	-48(%rbp)
	fldt	-32(%rbp)
	fmulp	%st, %st(2)
	fxch	%st(1)
	fld	%st(0)
	fstpt	-32(%rbp)
	fxch	%st(4)
	fucomi	%st(2), %st
	ja	.L87
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L96:
	fstp	%st(1)
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L75:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-32(%rbp)
	popq	%rcx
	popq	%rsi
	fmul	%st(1), %st
	fldt	-48(%rbp)
	fmulp	%st, %st(2)
	.p2align 4,,10
	.p2align 3
.L29:
	fldt	.LC6(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L30
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L30:
	fldt	.LC6(%rip)
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L1
	fld	%st(1)
	fmul	%st(2), %st
	fstp	%st(0)
	movq	-8(%rbp), %rbx
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	fstp	%st(1)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	-96(%rbp)
	call	__ieee754_sinhl@PLT
	fldt	-32(%rbp)
	subq	$16, %rsp
	fmulp	%st, %st(1)
	fstpt	-80(%rbp)
	fldt	-96(%rbp)
	fstpt	(%rsp)
	call	__ieee754_coshl@PLT
	fldt	-80(%rbp)
	addq	$32, %rsp
	fldt	-48(%rbp)
	fmulp	%st, %st(2)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L87:
	fsub	%st(2), %st
	fxch	%st(1)
	fmul	%st(3), %st
	fld	%st(0)
	fstpt	-48(%rbp)
	fxch	%st(4)
	fmulp	%st, %st(3)
	fxch	%st(2)
	fld	%st(0)
	fstpt	-32(%rbp)
	fxch	%st(2)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L96
	fstp	%st(0)
	fldt	.LC5(%rip)
	fmul	%st, %st(1)
	fmulp	%st, %st(2)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	flds	.LC4(%rip)
	jp	.L97
	je	.L80
	fstp	%st(1)
	jmp	.L52
.L97:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L52:
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L73:
	fxch	%st(1)
	fstpt	-48(%rbp)
	fld1
	fstpt	-32(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L80:
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L85:
	fchs
	jmp	.L35
.L86:
	flds	.LC3(%rip)
	jmp	.L46
.L88:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L89:
	fstp	%st(0)
.L45:
	flds	.LC2(%rip)
	fld	%st(1)
	fsubp	%st, %st(2)
	jmp	.L1
.L76:
	fstp	%st(1)
	flds	.LC2(%rip)
	fld	%st(0)
	fxch	%st(2)
	jmp	.L36
	.size	__csinhl, .-__csinhl
	.weak	csinhf64x
	.set	csinhf64x,__csinhl
	.weak	csinhl
	.set	csinhl,__csinhl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	4286578688
	.align 4
.LC4:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC6:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC8:
	.long	1182792704
	.align 4
.LC10:
	.long	1056964608
