	.text
	.p2align 4,,15
	.globl	__w_log1pl
	.type	__w_log1pl, @function
__w_log1pl:
	fldt	8(%rsp)
	fld1
	fchs
	fucomi	%st(1), %st
	jnb	.L7
	fstp	%st(0)
.L2:
	fstpt	8(%rsp)
	jmp	__log1pl@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__w_log1pl, .-__w_log1pl
	.weak	log1pf64x
	.set	log1pf64x,__w_log1pl
	.weak	log1pl
	.set	log1pl,__w_log1pl
