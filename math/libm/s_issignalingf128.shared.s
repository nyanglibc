	.text
	.p2align 4,,15
	.globl	__GI___issignalingf128
	.hidden	__GI___issignalingf128
	.type	__GI___issignalingf128, @function
__GI___issignalingf128:
	movaps	%xmm0, -24(%rsp)
	movq	-24(%rsp), %rcx
	movq	-16(%rsp), %rdx
	movabsq	$140737488355328, %rax
	xorq	%rax, %rdx
	movq	%rcx, %rax
	negq	%rax
	orq	%rcx, %rax
	shrq	$63, %rax
	orq	%rdx, %rax
	movabsq	$9223372036854775807, %rdx
	andq	%rdx, %rax
	movabsq	$9223231299366420480, %rdx
	cmpq	%rdx, %rax
	seta	%al
	movzbl	%al, %eax
	ret
	.size	__GI___issignalingf128, .-__GI___issignalingf128
	.globl	__issignalingf128
	.set	__issignalingf128,__GI___issignalingf128
