	.text
	.p2align 4,,15
	.globl	__atan2
	.type	__atan2, @function
__atan2:
	subq	$24, %rsp
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__ieee754_atan2@PLT
	pxor	%xmm1, %xmm1
	movl	$0, %edx
	ucomisd	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	movsd	(%rsp), %xmm2
	movl	$1, %edx
	movsd	8(%rsp), %xmm3
	ucomisd	%xmm1, %xmm2
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L11
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	andpd	.LC1(%rip), %xmm3
	movsd	.LC2(%rip), %xmm1
	ucomisd	%xmm3, %xmm1
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__atan2, .-__atan2
	.weak	atan2f32x
	.set	atan2f32x,__atan2
	.weak	atan2f64
	.set	atan2f64,__atan2
	.weak	atan2
	.set	atan2,__atan2
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
