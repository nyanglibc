	.text
	.p2align 4,,15
	.globl	__ccosf128
	.type	__ccosf128, @function
__ccosf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movdqa	64(%rsp), %xmm0
	movq	%rsp, %rdi
	pushq	56(%rsp)
	pushq	56(%rsp)
	pxor	.LC0(%rip), %xmm0
	subq	$16, %rsp
	movups	%xmm0, (%rsp)
	call	__ccoshf128@PLT
	movq	%rbx, %rax
	movdqa	32(%rsp), %xmm0
	movaps	%xmm0, (%rbx)
	movdqa	48(%rsp), %xmm0
	movaps	%xmm0, 16(%rbx)
	addq	$64, %rsp
	popq	%rbx
	ret
	.size	__ccosf128, .-__ccosf128
	.weak	ccosf128
	.set	ccosf128,__ccosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
