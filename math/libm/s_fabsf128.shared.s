	.text
	.p2align 4,,15
	.globl	__fabsf128
	.type	__fabsf128, @function
__fabsf128:
	movaps	%xmm0, -24(%rsp)
	movq	-24(%rsp), %rax
	movq	-16(%rsp), %rdx
	movq	%rax, -24(%rsp)
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rdx
	movq	%rdx, -16(%rsp)
	movdqa	-24(%rsp), %xmm0
	ret
	.size	__fabsf128, .-__fabsf128
	.weak	fabsf128
	.set	fabsf128,__fabsf128
