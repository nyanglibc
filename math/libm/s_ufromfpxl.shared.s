	.text
	.p2align 4,,15
	.globl	__ufromfpxl
	.type	__ufromfpxl, @function
__ufromfpxl:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$64, %esi
	ja	.L31
	testl	%esi, %esi
	jne	.L2
.L19:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$64, %esi
.L2:
	movq	48(%rsp), %rdx
	xorl	%eax, %eax
	movq	%rdx, %r8
	movl	%edx, %ebx
	shrq	$32, %r8
	orl	%r8d, %ebx
	je	.L1
	movq	56(%rsp), %r10
	movl	%r10d, %r9d
	andl	$32767, %r9d
	subl	$16383, %r9d
	testw	%r10w, %r10w
	js	.L4
	leal	-1(%rsi), %r11d
	cmpl	%r11d, %r9d
	jg	.L22
	salq	$32, %r8
	movl	%edx, %eax
	orq	%r8, %rax
	xorl	%r8d, %r8d
	cmpl	$63, %r9d
	jne	.L30
.L7:
	cmpl	$1, %edi
	je	.L39
	jle	.L68
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	cmpl	$3, %edi
	je	.L18
	cmpl	$4, %edi
	jne	.L37
.L18:
	addq	%rdx, %rax
.L9:
	testw	%r10w, %r10w
	jns	.L16
.L15:
	testq	%rax, %rax
	jne	.L19
.L20:
	testb	%r8b, %r8b
	jne	.L42
	testb	%bl, %bl
	je	.L1
.L42:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	$63, %r9d
	jne	.L20
	testq	%rax, %rax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$1, %edi
	movl	%esi, 12(%rsp)
	call	__GI_feraiseexcept
	movl	12(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	movq	$-1, %rax
	je	.L1
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%r9d, %r9d
	jns	.L19
	salq	$32, %r8
	movl	%edx, %eax
	movl	$-1, %r11d
	orq	%r8, %rax
.L30:
	cmpl	$-1, %r9d
	jl	.L34
	movl	$62, %ecx
	movl	$1, %edx
	subl	%r9d, %ecx
	salq	%cl, %rdx
	movq	%rdx, %rbp
	andq	%rax, %rbp
	setne	%bl
	subq	$1, %rdx
	andq	%rax, %rdx
	setne	%r8b
	cmpl	$-1, %r9d
	je	.L35
	movl	$63, %ecx
	subl	%r9d, %ecx
	shrq	%cl, %rax
.L8:
	cmpl	$1, %edi
	je	.L10
	jle	.L69
	cmpl	$3, %edi
	je	.L13
	cmpl	$4, %edi
	jne	.L9
	testq	%rbp, %rbp
	je	.L36
	movq	%rax, %rcx
	movl	$1, %ebx
	andl	$1, %ecx
	orq	%rdx, %rcx
	setne	%dl
	movzbl	%dl, %edx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L69:
	testl	%edi, %edi
	jne	.L9
.L12:
	testw	%r10w, %r10w
	js	.L15
	movl	%ebx, %edx
	orl	%r8d, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	$63, %r11d
	je	.L70
	leal	1(%r11), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L20
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%ebx, %ebx
	testl	%edi, %edi
	je	.L12
.L37:
	xorl	%ebx, %ebx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%ebx, %ebx
.L10:
	testw	%r10w, %r10w
	jns	.L16
	movl	%ebx, %edx
	orl	%r8d, %edx
	movzbl	%dl, %edx
	addq	%rdx, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$1, %r8d
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	movzbl	%bl, %edx
	jmp	.L18
.L36:
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	jmp	.L18
	.size	__ufromfpxl, .-__ufromfpxl
	.weak	ufromfpxf64x
	.set	ufromfpxf64x,__ufromfpxl
	.weak	ufromfpxl
	.set	ufromfpxl,__ufromfpxl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
