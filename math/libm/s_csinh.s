	.text
	.p2align 4,,15
	.globl	__csinh
	.type	__csinh, @function
__csinh:
	pushq	%rbp
	pushq	%rbx
	movmskpd	%xmm0, %ebp
	movapd	%xmm0, %xmm6
	subq	$88, %rsp
	andl	$1, %ebp
	movq	.LC5(%rip), %xmm3
	movapd	%xmm1, %xmm5
	andpd	%xmm3, %xmm6
	andpd	%xmm3, %xmm5
	ucomisd	%xmm6, %xmm6
	jp	.L2
	ucomisd	.LC6(%rip), %xmm6
	jbe	.L79
	ucomisd	%xmm5, %xmm5
	jp	.L43
	ucomisd	.LC6(%rip), %xmm5
	ja	.L43
	movsd	.LC7(%rip), %xmm4
	movapd	%xmm1, %xmm2
	ucomisd	%xmm4, %xmm5
	jb	.L80
.L16:
	ucomisd	%xmm4, %xmm5
	jbe	.L70
	movapd	%xmm1, %xmm0
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	call	__sincos@PLT
	movq	.LC11(%rip), %xmm3
	movsd	72(%rsp), %xmm0
	movq	.LC13(%rip), %xmm1
	andpd	%xmm3, %xmm0
	movsd	64(%rsp), %xmm2
	orpd	%xmm1, %xmm0
.L36:
	andpd	%xmm3, %xmm2
	testl	%ebp, %ebp
	orpd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	je	.L1
	xorpd	%xmm3, %xmm0
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movsd	.LC7(%rip), %xmm4
	ucomisd	%xmm4, %xmm6
	jnb	.L4
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm0
	jp	.L4
	jne	.L4
	ucomisd	%xmm5, %xmm5
	jp	.L10
	ucomisd	.LC6(%rip), %xmm5
	jbe	.L47
.L10:
	testl	%ebp, %ebp
	pxor	%xmm0, %xmm0
	jne	.L81
.L75:
	subsd	%xmm1, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	%xmm5, %xmm5
	jp	.L8
	ucomisd	.LC6(%rip), %xmm5
	jbe	.L47
.L8:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movsd	.LC4(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L47:
	movsd	.LC9(%rip), %xmm2
	ucomisd	%xmm4, %xmm5
	mulsd	.LC8(%rip), %xmm2
	cvttsd2si	%xmm2, %ebx
	jbe	.L67
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm4, 32(%rsp)
	movapd	%xmm1, %xmm0
	movsd	%xmm6, (%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__sincos@PLT
	movsd	(%rsp), %xmm6
	movapd	16(%rsp), %xmm3
	movsd	32(%rsp), %xmm4
.L21:
	testl	%ebp, %ebp
	je	.L22
	movsd	72(%rsp), %xmm0
	xorpd	.LC11(%rip), %xmm0
	movsd	%xmm0, 72(%rsp)
.L22:
	pxor	%xmm5, %xmm5
	movsd	%xmm4, 56(%rsp)
	movaps	%xmm3, 32(%rsp)
	cvtsi2sd	%ebx, %xmm5
	ucomisd	%xmm5, %xmm6
	jbe	.L68
	movapd	%xmm5, %xmm0
	movsd	%xmm6, 16(%rsp)
	movsd	%xmm5, (%rsp)
	call	__ieee754_exp@PLT
	movsd	.LC12(%rip), %xmm2
	movsd	(%rsp), %xmm5
	mulsd	%xmm0, %xmm2
	movapd	32(%rsp), %xmm3
	movsd	16(%rsp), %xmm6
	movsd	64(%rsp), %xmm1
	subsd	%xmm5, %xmm6
	movsd	56(%rsp), %xmm4
	mulsd	%xmm2, %xmm1
	mulsd	72(%rsp), %xmm2
	ucomisd	%xmm5, %xmm6
	movsd	%xmm1, 64(%rsp)
	movsd	%xmm2, 72(%rsp)
	ja	.L82
.L69:
	movapd	%xmm6, %xmm0
	movsd	%xmm4, 16(%rsp)
	movaps	%xmm3, (%rsp)
	call	__ieee754_exp@PLT
	movapd	%xmm0, %xmm1
	movsd	72(%rsp), %xmm0
	movapd	(%rsp), %xmm3
	mulsd	%xmm1, %xmm0
	mulsd	64(%rsp), %xmm1
	movsd	16(%rsp), %xmm4
	.p2align 4,,10
	.p2align 3
.L29:
	movapd	%xmm0, %xmm2
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L30
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
.L30:
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm4
	jbe	.L1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movapd	%xmm6, %xmm0
	movsd	%xmm6, 16(%rsp)
	call	__ieee754_sinh@PLT
	movsd	72(%rsp), %xmm2
	movsd	16(%rsp), %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm6, %xmm0
	movsd	%xmm2, (%rsp)
	call	__ieee754_cosh@PLT
	movsd	(%rsp), %xmm2
	movapd	%xmm0, %xmm1
	movsd	56(%rsp), %xmm4
	movapd	%xmm2, %xmm0
	mulsd	64(%rsp), %xmm1
	movapd	32(%rsp), %xmm3
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L82:
	subsd	%xmm5, %xmm6
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	ucomisd	%xmm5, %xmm6
	movsd	%xmm1, 64(%rsp)
	movsd	%xmm2, 72(%rsp)
	jbe	.L69
	movsd	.LC6(%rip), %xmm0
	mulsd	.LC6(%rip), %xmm1
	mulsd	%xmm2, %xmm0
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L80:
	ucomisd	.LC1(%rip), %xmm1
	jp	.L16
	jne	.L16
	testl	%ebp, %ebp
	movsd	.LC2(%rip), %xmm0
	je	.L1
	movsd	.LC3(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	.LC1(%rip), %xmm1
	movsd	.LC4(%rip), %xmm0
	jp	.L50
	je	.L1
.L50:
	movapd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	movq	.LC10(%rip), %rax
	movsd	%xmm1, 64(%rsp)
	movq	%rax, 72(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L81:
	movsd	.LC0(%rip), %xmm0
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L43:
	movsd	.LC2(%rip), %xmm0
	jmp	.L75
.L70:
	movsd	.LC2(%rip), %xmm0
	movq	.LC11(%rip), %xmm3
	movq	.LC13(%rip), %xmm1
	jmp	.L36
	.size	__csinh, .-__csinh
	.weak	csinhf32x
	.set	csinhf32x,__csinh
	.weak	csinhf64
	.set	csinhf64,__csinh
	.weak	csinh
	.set	csinh,__csinh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-2147483648
	.align 8
.LC1:
	.long	0
	.long	0
	.align 8
.LC2:
	.long	0
	.long	2146435072
	.align 8
.LC3:
	.long	0
	.long	-1048576
	.align 8
.LC4:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	4294967295
	.long	2146435071
	.align 8
.LC7:
	.long	0
	.long	1048576
	.align 8
.LC8:
	.long	4277811695
	.long	1072049730
	.align 8
.LC9:
	.long	0
	.long	1083176960
	.align 8
.LC10:
	.long	0
	.long	1072693248
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC12:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC13:
	.long	0
	.long	2146435072
	.long	0
	.long	0
