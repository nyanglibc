	.text
	.globl	__multf3
	.globl	__divtf3
	.globl	__letf2
	.globl	__subtf3
	.globl	__addtf3
	.globl	__gttf2
	.globl	__getf2
	.globl	__lttf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__remquof128
	.type	__remquof128, @function
__remquof128:
	pushq	%r14
	pushq	%r13
	movabsq	$9223372036854775807, %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$48, %rsp
	movaps	%xmm1, (%rsp)
	movq	8(%rsp), %rbx
	movq	(%rsp), %r12
	movaps	%xmm0, 16(%rsp)
	movq	%rbx, %rbp
	andq	%rax, %rbp
	movq	%rbp, %rcx
	orq	%r12, %rcx
	je	.L36
	movq	24(%rsp), %r13
	movabsq	$9223090561878065151, %rdx
	andq	%r13, %rax
	cmpq	%rdx, %rax
	jg	.L4
	cmpq	%rdx, %rbp
	movq	%rdi, %r14
	jg	.L38
	movabsq	$9222246136947933183, %rdx
	movdqa	16(%rsp), %xmm0
	cmpq	%rdx, %rbp
	jg	.L6
	movdqa	.LC1(%rip), %xmm1
	movq	%rax, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__ieee754_fmodf128@PLT
	movq	32(%rsp), %rax
.L6:
	movq	16(%rsp), %rdx
	subq	%rbp, %rax
	xorq	%r13, %rbx
	subq	%r12, %rdx
	orq	%rdx, %rax
	jne	.L7
	sarq	$63, %rbx
	pxor	%xmm1, %xmm1
	orl	$1, %ebx
	movl	%ebx, (%r14)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	movabsq	$-9223090561878065152, %rdx
	movdqa	16(%rsp), %xmm0
	addq	%rbp, %rdx
	orq	%r12, %rdx
	je	.L6
.L4:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
.L36:
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$48, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movdqa	.LC2(%rip), %xmm1
	movabsq	$9222527611924643839, %rax
	movdqa	%xmm0, %xmm2
	cmpq	%rax, %rbp
	movdqa	(%rsp), %xmm0
	pand	%xmm1, %xmm2
	pand	%xmm1, %xmm0
	movaps	%xmm0, (%rsp)
	jg	.L9
	movaps	%xmm2, 16(%rsp)
	xorl	%r12d, %r12d
	movdqa	.LC3(%rip), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movaps	%xmm0, 32(%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jg	.L10
	movdqa	%xmm2, %xmm0
	movl	$4, %r12d
	movdqa	32(%rsp), %xmm3
	movdqa	%xmm3, %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
.L10:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 16(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jle	.L39
.L13:
	movabsq	$562949953421311, %rax
	cmpq	%rax, %rbp
	jg	.L12
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jg	.L40
.L15:
	movl	%r12d, %eax
	pxor	%xmm1, %xmm1
	negl	%eax
	testq	%rbx, %rbx
	cmovs	%eax, %r12d
	movdqa	%xmm2, %xmm0
	movl	%r12d, (%r14)
	movaps	%xmm2, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	jne	.L21
	pxor	%xmm2, %xmm2
.L21:
	testq	%r13, %r13
	jns	.L1
	pxor	.LC5(%rip), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movabsq	$9222809086901354495, %rax
	xorl	%r12d, %r12d
	cmpq	%rax, %rbp
	jle	.L10
.L12:
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jns	.L15
	movdqa	%xmm2, %xmm0
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jle	.L19
.L35:
	addl	$1, %r12d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L40:
	movdqa	%xmm2, %xmm0
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__getf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	js	.L35
.L19:
	movdqa	%xmm2, %xmm0
	movdqa	(%rsp), %xmm1
	addl	$2, %r12d
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L39:
	movdqa	32(%rsp), %xmm3
	addl	$2, %r12d
	movdqa	%xmm2, %xmm0
	movdqa	%xmm3, %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L13
	.size	__remquof128, .-__remquof128
	.weak	remquof128
	.set	remquof128,__remquof128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073872896
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1073807360
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
