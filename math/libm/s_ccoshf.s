	.text
	.p2align 4,,15
	.globl	__ccoshf
	.type	__ccoshf, @function
__ccoshf:
	pushq	%rbx
	subq	$96, %rsp
	movq	%xmm0, 72(%rsp)
	movss	.LC3(%rip), %xmm5
	movss	72(%rsp), %xmm6
	movaps	%xmm6, %xmm4
	movss	76(%rsp), %xmm0
	movaps	%xmm0, %xmm2
	andps	%xmm5, %xmm4
	andps	%xmm5, %xmm2
	ucomiss	%xmm4, %xmm4
	jp	.L2
	ucomiss	.LC4(%rip), %xmm4
	jbe	.L72
	ucomiss	%xmm2, %xmm2
	jp	.L41
	ucomiss	.LC4(%rip), %xmm2
	ja	.L41
	ucomiss	.LC5(%rip), %xmm2
	movaps	%xmm0, %xmm1
	jnb	.L16
	ucomiss	.LC0(%rip), %xmm0
	jp	.L16
	je	.L17
.L16:
	ucomiss	.LC5(%rip), %xmm2
	jbe	.L65
	leaq	92(%rsp), %rsi
	leaq	88(%rsp), %rdi
	movss	%xmm6, (%rsp)
	call	__sincosf@PLT
	movss	.LC9(%rip), %xmm3
	movss	92(%rsp), %xmm2
	movss	.LC11(%rip), %xmm0
	movss	88(%rsp), %xmm1
	andps	%xmm3, %xmm2
	movss	(%rsp), %xmm6
	orps	%xmm0, %xmm2
.L36:
	andps	%xmm3, %xmm6
	andps	%xmm3, %xmm1
	orps	.LC12(%rip), %xmm6
	orps	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	mulss	%xmm1, %xmm0
.L1:
	movss	%xmm2, 64(%rsp)
	movss	%xmm0, 68(%rsp)
	movq	64(%rsp), %xmm0
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	ucomiss	.LC5(%rip), %xmm4
	jnb	.L4
	pxor	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm6
	jp	.L51
	jne	.L51
	ucomiss	%xmm2, %xmm2
	jp	.L8
	ucomiss	.LC4(%rip), %xmm2
	ja	.L8
.L45:
	movsd	.LC7(%rip), %xmm1
	ucomiss	.LC5(%rip), %xmm2
	mulsd	.LC6(%rip), %xmm1
	cvttsd2si	%xmm1, %ebx
	jbe	.L62
	leaq	92(%rsp), %rsi
	leaq	88(%rsp), %rdi
	movss	%xmm6, 28(%rsp)
	movaps	%xmm5, 32(%rsp)
	movss	%xmm4, (%rsp)
	call	__sincosf@PLT
	movss	(%rsp), %xmm4
	movaps	32(%rsp), %xmm5
	movss	28(%rsp), %xmm6
.L21:
	pxor	%xmm3, %xmm3
	movss	%xmm4, (%rsp)
	cvtsi2ss	%ebx, %xmm3
	ucomiss	%xmm3, %xmm4
	jbe	.L63
	movaps	%xmm3, %xmm0
	movss	%xmm6, 32(%rsp)
	movaps	%xmm5, 48(%rsp)
	movss	%xmm3, 28(%rsp)
	call	__ieee754_expf@PLT
	movss	32(%rsp), %xmm6
	movd	%xmm6, %eax
	movss	88(%rsp), %xmm1
	movss	28(%rsp), %xmm3
	testl	%eax, %eax
	movaps	48(%rsp), %xmm5
	movss	(%rsp), %xmm4
	jns	.L25
	xorps	.LC9(%rip), %xmm1
.L25:
	movss	.LC10(%rip), %xmm2
	subss	%xmm3, %xmm4
	mulss	%xmm0, %xmm2
	ucomiss	%xmm3, %xmm4
	mulss	%xmm2, %xmm1
	mulss	92(%rsp), %xmm2
	movss	%xmm1, 88(%rsp)
	movss	%xmm2, 92(%rsp)
	ja	.L73
.L64:
	movaps	%xmm4, %xmm0
	movaps	%xmm5, (%rsp)
	call	__ieee754_expf@PLT
	movss	92(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	movaps	(%rsp), %xmm5
	mulss	88(%rsp), %xmm0
	.p2align 4,,10
	.p2align 3
.L30:
	movaps	%xmm2, %xmm3
	movss	.LC5(%rip), %xmm1
	andps	%xmm5, %xmm3
	ucomiss	%xmm3, %xmm1
	jbe	.L31
	movaps	%xmm2, %xmm3
	mulss	%xmm2, %xmm3
.L31:
	andps	%xmm0, %xmm5
	ucomiss	%xmm5, %xmm1
	jbe	.L1
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	ucomiss	%xmm2, %xmm2
	jp	.L11
	ucomiss	.LC4(%rip), %xmm2
	jbe	.L45
.L11:
	pxor	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm6
	jp	.L49
	je	.L8
.L49:
	movss	.LC1(%rip), %xmm3
.L8:
	movaps	%xmm0, %xmm2
	subss	%xmm0, %xmm2
	movaps	%xmm3, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L51:
	ucomiss	%xmm2, %xmm2
	jp	.L49
	ucomiss	.LC4(%rip), %xmm2
	ja	.L49
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L63:
	movaps	%xmm6, %xmm0
	movss	%xmm6, 28(%rsp)
	movaps	%xmm5, 32(%rsp)
	call	__ieee754_coshf@PLT
	movss	92(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	movss	28(%rsp), %xmm6
	movaps	%xmm6, %xmm0
	movss	%xmm2, (%rsp)
	call	__ieee754_sinhf@PLT
	movss	(%rsp), %xmm2
	mulss	88(%rsp), %xmm0
	movaps	32(%rsp), %xmm5
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L73:
	subss	%xmm3, %xmm4
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	ucomiss	%xmm3, %xmm4
	movss	%xmm1, 88(%rsp)
	movss	%xmm2, 92(%rsp)
	jbe	.L64
	movss	.LC4(%rip), %xmm0
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm0
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	.LC0(%rip), %xmm0
	movss	.LC1(%rip), %xmm2
	jp	.L47
	je	.L1
.L47:
	movaps	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	andps	.LC9(%rip), %xmm6
	movss	.LC2(%rip), %xmm2
	orps	.LC12(%rip), %xmm6
	mulss	%xmm6, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L62:
	movss	%xmm0, 88(%rsp)
	movl	$0x3f800000, 92(%rsp)
	jmp	.L21
.L65:
	movss	.LC2(%rip), %xmm2
	movss	.LC9(%rip), %xmm3
	movss	.LC11(%rip), %xmm0
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L41:
	movss	.LC2(%rip), %xmm2
	subss	%xmm0, %xmm0
	jmp	.L1
	.size	__ccoshf, .-__ccoshf
	.weak	ccoshf32
	.set	ccoshf32,__ccoshf
	.weak	ccoshf
	.set	ccoshf,__ccoshf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	2143289344
	.align 4
.LC2:
	.long	2139095040
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	2139095039
	.align 4
.LC5:
	.long	8388608
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC6:
	.long	4277811695
	.long	1072049730
	.align 8
.LC7:
	.long	0
	.long	1080016896
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	2139095040
	.long	0
	.long	0
	.long	0
	.align 16
.LC12:
	.long	1065353216
	.long	0
	.long	0
	.long	0
