	.text
	.p2align 4,,15
	.globl	__ufromfpl
	.type	__ufromfpl, @function
__ufromfpl:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$64, %esi
	ja	.L36
	testl	%esi, %esi
	jne	.L2
.L31:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$64, %esi
.L2:
	movq	48(%rsp), %rdx
	movq	%rdx, %rax
	movl	%edx, %ebx
	shrq	$32, %rax
	orl	%eax, %ebx
	je	.L28
	movq	56(%rsp), %r10
	movl	%r10d, %r9d
	andl	$32767, %r9d
	subl	$16383, %r9d
	testw	%r10w, %r10w
	js	.L5
	leal	-1(%rsi), %r11d
	cmpl	%r11d, %r9d
	jg	.L26
	salq	$32, %rax
	movq	%rax, %r8
	movl	%edx, %eax
	orq	%r8, %rax
	xorl	%r8d, %r8d
	cmpl	$63, %r9d
	jne	.L35
.L8:
	cmpl	$1, %edi
	je	.L42
	jle	.L79
	cmpl	$3, %edi
	je	.L43
	xorl	%edx, %edx
	cmpl	$4, %edi
	je	.L23
	.p2align 4,,10
	.p2align 3
.L10:
	testw	%r10w, %r10w
	jns	.L19
.L16:
	testq	%rax, %rax
	je	.L1
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L28:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	xorl	%ebx, %ebx
.L76:
	testl	%edi, %edi
	jne	.L10
	testw	%r10w, %r10w
	js	.L16
	testb	%bl, %bl
	jne	.L45
	testb	%r8b, %r8b
	je	.L19
.L45:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$63, %r11d
	je	.L80
	leal	1(%r11), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L1
.L26:
	movl	$1, %edi
	movl	%esi, 12(%rsp)
	call	__GI_feraiseexcept
	movl	12(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	je	.L81
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%r9d, %r9d
	jns	.L31
	salq	$32, %rax
	movl	$-1, %r11d
	movq	%rax, %rcx
	movl	%edx, %eax
	orq	%rcx, %rax
.L35:
	cmpl	$-1, %r9d
	jl	.L38
	movl	$62, %ecx
	movl	$1, %edx
	subl	%r9d, %ecx
	salq	%cl, %rdx
	movq	%rdx, %rbp
	andq	%rax, %rbp
	setne	%bl
	subq	$1, %rdx
	andq	%rax, %rdx
	setne	%r8b
	cmpl	$-1, %r9d
	je	.L39
	movl	$63, %ecx
	subl	%r9d, %ecx
	shrq	%cl, %rax
.L9:
	cmpl	$1, %edi
	je	.L11
	jle	.L76
	cmpl	$3, %edi
	je	.L14
	cmpl	$4, %edi
	jne	.L10
	testq	%rbp, %rbp
	je	.L40
	movq	%rax, %rcx
	andl	$1, %ecx
	orq	%rdx, %rcx
	setne	%dl
	movzbl	%dl, %edx
.L23:
	addq	%rdx, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%ebx, %ebx
.L22:
	addq	%rbx, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%ebx, %ebx
.L11:
	testw	%r10w, %r10w
	jns	.L19
	testb	%bl, %bl
	jne	.L46
	testb	%r8b, %r8b
	je	.L16
.L46:
	addq	$1, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$1, %r8d
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$63, %r9d
	jne	.L1
	testq	%rax, %rax
	jne	.L1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L81:
	movq	$-1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	%bl, %ebx
	jmp	.L22
.L40:
	xorl	%edx, %edx
	jmp	.L23
	.size	__ufromfpl, .-__ufromfpl
	.weak	ufromfpf64x
	.set	ufromfpf64x,__ufromfpl
	.weak	ufromfpl
	.set	ufromfpl,__ufromfpl
