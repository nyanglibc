	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__csinhf
	.type	__csinhf, @function
__csinhf:
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movq	%xmm0, 56(%rsp)
	movss	.LC5(%rip), %xmm4
	movss	56(%rsp), %xmm1
	movaps	%xmm1, %xmm5
	movd	%xmm1, %eax
	movss	60(%rsp), %xmm0
	andps	%xmm4, %xmm5
	andl	$-2147483648, %eax
	movaps	%xmm0, %xmm3
	ucomiss	%xmm5, %xmm5
	andps	%xmm4, %xmm3
	jp	.L2
	ucomiss	.LC6(%rip), %xmm5
	movl	%eax, %ebp
	jbe	.L79
	ucomiss	%xmm3, %xmm3
	jp	.L43
	ucomiss	.LC6(%rip), %xmm3
	ja	.L43
	ucomiss	.LC7(%rip), %xmm3
	movaps	%xmm0, %xmm6
	jb	.L80
.L16:
	ucomiss	.LC7(%rip), %xmm3
	jbe	.L70
	leaq	76(%rsp), %rsi
	leaq	72(%rsp), %rdi
	call	__sincosf@PLT
	movss	.LC11(%rip), %xmm1
	movss	76(%rsp), %xmm2
	movss	.LC13(%rip), %xmm0
	movss	72(%rsp), %xmm6
	andps	%xmm1, %xmm2
	orps	%xmm0, %xmm2
.L36:
	andps	%xmm1, %xmm6
	testl	%ebp, %ebp
	orps	%xmm0, %xmm6
	movaps	%xmm6, %xmm0
	je	.L1
	xorps	%xmm1, %xmm2
.L1:
	movss	%xmm2, 48(%rsp)
	movss	%xmm0, 52(%rsp)
	movq	48(%rsp), %xmm0
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	ucomiss	.LC7(%rip), %xmm5
	jnb	.L4
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jp	.L4
	jne	.L4
	ucomiss	%xmm3, %xmm3
	jp	.L10
	ucomiss	.LC6(%rip), %xmm3
	jbe	.L47
.L10:
	testl	%ebp, %ebp
	jne	.L81
.L75:
	subss	%xmm0, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	ucomiss	%xmm3, %xmm3
	jp	.L8
	ucomiss	.LC6(%rip), %xmm3
	jbe	.L47
.L8:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movss	.LC4(%rip), %xmm2
	movaps	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L47:
	movsd	.LC9(%rip), %xmm1
	ucomiss	.LC7(%rip), %xmm3
	mulsd	.LC8(%rip), %xmm1
	cvttsd2si	%xmm1, %ebx
	jbe	.L67
	leaq	76(%rsp), %rsi
	leaq	72(%rsp), %rdi
	movss	%xmm5, (%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__sincosf@PLT
	movaps	16(%rsp), %xmm4
	movss	(%rsp), %xmm5
.L21:
	testl	%ebp, %ebp
	je	.L22
	movss	76(%rsp), %xmm0
	xorps	.LC11(%rip), %xmm0
	movss	%xmm0, 76(%rsp)
.L22:
	pxor	%xmm3, %xmm3
	movaps	%xmm4, 32(%rsp)
	cvtsi2ss	%ebx, %xmm3
	ucomiss	%xmm3, %xmm5
	jbe	.L68
	movaps	%xmm3, %xmm0
	movss	%xmm5, 16(%rsp)
	movss	%xmm3, (%rsp)
	call	__ieee754_expf@PLT
	movss	.LC12(%rip), %xmm2
	mulss	%xmm0, %xmm2
	movss	(%rsp), %xmm3
	movss	16(%rsp), %xmm5
	subss	%xmm3, %xmm5
	movss	72(%rsp), %xmm1
	movaps	32(%rsp), %xmm4
	mulss	%xmm2, %xmm1
	mulss	76(%rsp), %xmm2
	ucomiss	%xmm3, %xmm5
	movss	%xmm1, 72(%rsp)
	movss	%xmm2, 76(%rsp)
	ja	.L82
.L69:
	movaps	%xmm5, %xmm0
	movaps	%xmm4, (%rsp)
	call	__ieee754_expf@PLT
	movss	76(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	movaps	(%rsp), %xmm4
	mulss	72(%rsp), %xmm0
	.p2align 4,,10
	.p2align 3
.L29:
	movaps	%xmm2, %xmm3
	movss	.LC7(%rip), %xmm1
	andps	%xmm4, %xmm3
	ucomiss	%xmm3, %xmm1
	jbe	.L30
	movaps	%xmm2, %xmm3
	mulss	%xmm2, %xmm3
.L30:
	andps	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.L1
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L68:
	movaps	%xmm5, %xmm0
	movss	%xmm5, 16(%rsp)
	call	__ieee754_sinhf@PLT
	movss	76(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	movss	16(%rsp), %xmm5
	movaps	%xmm5, %xmm0
	movss	%xmm2, (%rsp)
	call	__ieee754_coshf@PLT
	movss	(%rsp), %xmm2
	mulss	72(%rsp), %xmm0
	movaps	32(%rsp), %xmm4
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L82:
	subss	%xmm3, %xmm5
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	ucomiss	%xmm3, %xmm5
	movss	%xmm1, 72(%rsp)
	movss	%xmm2, 76(%rsp)
	jbe	.L69
	movss	.LC6(%rip), %xmm0
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm0
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L80:
	ucomiss	.LC1(%rip), %xmm0
	jp	.L16
	jne	.L16
	testl	%ebp, %ebp
	movss	.LC2(%rip), %xmm2
	je	.L1
	movss	.LC3(%rip), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	.LC1(%rip), %xmm0
	movss	.LC4(%rip), %xmm2
	jp	.L50
	je	.L1
.L50:
	movaps	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L67:
	movss	%xmm0, 72(%rsp)
	movl	$0x3f800000, 76(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L81:
	movss	.LC0(%rip), %xmm2
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L43:
	movss	.LC2(%rip), %xmm2
	jmp	.L75
.L70:
	movss	.LC2(%rip), %xmm2
	movss	.LC11(%rip), %xmm1
	movss	.LC13(%rip), %xmm0
	jmp	.L36
	.size	__csinhf, .-__csinhf
	.weak	csinhf32
	.set	csinhf32,__csinhf
	.weak	csinhf
	.set	csinhf,__csinhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2147483648
	.align 4
.LC1:
	.long	0
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	4286578688
	.align 4
.LC4:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC5:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	2139095039
	.align 4
.LC7:
	.long	8388608
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC8:
	.long	4277811695
	.long	1072049730
	.align 8
.LC9:
	.long	0
	.long	1080016896
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC12:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC13:
	.long	2139095040
	.long	0
	.long	0
	.long	0
