	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpxf128
	.type	__fromfpxf128, @function
__fromfpxf128:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$64, %esi
	movaps	%xmm0, (%rsp)
	ja	.L37
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$64, %esi
.L10:
	movq	8(%rsp), %r9
	movq	(%rsp), %rax
	movabsq	$9223372036854775807, %rcx
	andq	%r9, %rcx
	movq	%rcx, %rbx
	orq	%rax, %rbx
	je	.L38
	movq	%rcx, %rdx
	movq	%r9, %rbx
	shrq	$48, %rdx
	shrq	$63, %rbx
	leal	-16383(%rdx), %r10d
	leal	-2(%rsi,%rbx), %r11d
	cmpl	%r11d, %r10d
	jg	.L59
	cmpl	$-1, %r10d
	jl	.L15
	movabsq	$281474976710655, %r8
	movl	$112, %ebp
	movabsq	$281474976710656, %rcx
	andq	%r9, %r8
	subl	%r10d, %ebp
	orq	%rcx, %r8
	cmpl	$64, %ebp
	jg	.L16
	movl	$111, %ecx
	movl	$1, %r12d
	subl	%r10d, %ecx
	salq	%cl, %r12
	testq	%rax, %r12
	movq	%r12, %rcx
	setne	%r12b
	subq	$1, %rcx
	testq	%rax, %rcx
	leal	-16431(%rdx), %ecx
	movq	%r8, %rdx
	setne	%r13b
	salq	%cl, %rdx
	movl	%ebp, %ecx
	shrq	%cl, %rax
	orq	%rdx, %rax
	cmpl	$64, %ebp
	cmovne	%rax, %rdx
.L17:
	cmpl	$1, %edi
	je	.L19
	jle	.L61
	cmpl	$3, %edi
	je	.L22
	cmpl	$4, %edi
	jne	.L18
	testb	%r12b, %r12b
	je	.L18
	movq	%rdx, %rax
	orq	%r13, %rax
	andl	$1, %eax
	addq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%r9, %r9
	jns	.L25
.L24:
	cmpl	%r11d, %r10d
	je	.L28
	testb	%r13b, %r13b
	jne	.L29
	testb	%r12b, %r12b
	jne	.L29
.L30:
	movq	%rdx, %rax
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%eax, %eax
.L9:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$1, %edi
	je	.L41
	jle	.L62
	cmpl	$3, %edi
	je	.L43
	cmpl	$4, %edi
	jne	.L39
.L43:
	xorl	%edx, %edx
	movl	$1, %r13d
	xorl	%r12d, %r12d
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L62:
	testl	%edi, %edi
	jne	.L39
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
.L21:
	testq	%r9, %r9
	js	.L24
	movl	%r12d, %eax
	orl	%r13d, %eax
	movzbl	%al, %eax
	addq	%rax, %rdx
.L25:
	leal	1(%r11), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%rdx, %rax
	sete	%al
.L31:
	testb	%al, %al
	jne	.L59
	testb	%r13b, %r13b
	jne	.L29
	testb	%r12b, %r12b
	je	.L33
.L29:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
.L33:
	testq	%r9, %r9
	movq	%rdx, %rax
	js	.L30
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	addq	$24, %rsp
	movl	%ebx, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$47, %ecx
	movl	$1, %edx
	subl	%r10d, %ecx
	salq	%cl, %rdx
	movl	$48, %ecx
	testq	%rdx, %r8
	setne	%r12b
	subq	$1, %rdx
	andq	%r8, %rdx
	orq	%rax, %rdx
	movq	%r8, %rdx
	setne	%r13b
	subl	%r10d, %ecx
	shrq	%cl, %rdx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
.L19:
	testq	%r9, %r9
	jns	.L25
	movl	%r12d, %eax
	orl	%r13d, %eax
	movzbl	%al, %eax
	addq	%rax, %rdx
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L61:
	testl	%edi, %edi
	je	.L21
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$1, %eax
	movl	%r10d, %ecx
	salq	%cl, %rax
	cmpq	%rdx, %rax
	setne	%al
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L22:
	movzbl	%r12b, %eax
	addq	%rax, %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	jmp	.L18
	.size	__fromfpxf128, .-__fromfpxf128
	.weak	fromfpxf128
	.set	fromfpxf128,__fromfpxf128
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
