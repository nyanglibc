	.text
	.p2align 4,,15
	.globl	__GI___issignalingl
	.hidden	__GI___issignalingl
	.type	__GI___issignalingl, @function
__GI___issignalingl:
	movq	16(%rsp), %rdx
	xorl	%eax, %eax
	andw	$32767, %dx
	je	.L1
	movq	8(%rsp), %rcx
	movl	$1, %eax
	movq	%rcx, %rsi
	shrq	$32, %rsi
	testl	%esi, %esi
	js	.L7
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%ecx, %eax
	xorl	$1073741824, %esi
	negl	%eax
	orl	%eax, %ecx
	shrl	$31, %ecx
	orl	%ecx, %esi
	cmpl	$-1073741824, %esi
	seta	%cl
	xorl	%eax, %eax
	cmpw	$32767, %dx
	sete	%al
	andl	%ecx, %eax
	ret
	.size	__GI___issignalingl, .-__GI___issignalingl
	.globl	__issignalingl
	.set	__issignalingl,__GI___issignalingl
