	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cabs
	.type	__cabs, @function
__cabs:
	jmp	__hypot@PLT
	.size	__cabs, .-__cabs
	.weak	cabsf32x
	.set	cabsf32x,__cabs
	.weak	cabsf64
	.set	cabsf64,__cabs
	.weak	cabs
	.set	cabs,__cabs
