	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __lgammal,lgammal@@GLIBC_2.23
#NO_APP
	.p2align 4,,15
	.globl	__lgammal
	.type	__lgammal, @function
__lgammal:
	subq	$24, %rsp
	movq	__signgam@GOTPCREL(%rip), %rdi
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__ieee754_lgammal_r@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L15
	fstp	%st(0)
.L12:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	fldt	32(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L12
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L12
	fstp	%st(0)
	fldt	32(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	frndint
	fldcw	14(%rsp)
	fldt	32(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L16
	jne	.L17
	fldz
	xorl	%edi, %edi
	fucomip	%st(1), %st
	fstp	%st(0)
	setnb	%dil
	addl	$214, %edi
.L4:
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L12
.L16:
	fstp	%st(0)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L17:
	fstp	%st(0)
.L7:
	movl	$214, %edi
	jmp	.L4
	.size	__lgammal, .-__lgammal
	.weak	lgammaf64x
	.set	lgammaf64x,__lgammal
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
