	.text
	.p2align 4,,15
	.globl	__roundf
	.type	__roundf, @function
__roundf:
	movd	%xmm0, %ecx
	movd	%xmm0, %eax
	sarl	$23, %ecx
	movzbl	%cl, %ecx
	subl	$127, %ecx
	cmpl	$22, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L10
	movl	$8388607, %edi
	movss	%xmm0, -4(%rsp)
	sarl	%cl, %edi
	testl	%edi, %eax
	movss	-4(%rsp), %xmm0
	je	.L1
	movl	$4194304, %edx
	notl	%edi
	sarl	%cl, %edx
	leal	(%rdx,%rax), %eax
	andl	%edi, %eax
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movss	%xmm0, -4(%rsp)
	addl	$-128, %ecx
	movss	-4(%rsp), %xmm0
	jne	.L1
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movd	%xmm0, %eax
	andl	$-2147483648, %eax
	movl	%eax, %edx
	orl	$1065353216, %edx
	cmpl	$-1, %ecx
	cmove	%edx, %eax
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.size	__roundf, .-__roundf
	.weak	roundf32
	.set	roundf32,__roundf
	.weak	roundf
	.set	roundf,__roundf
