	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__creal
	.type	__creal, @function
__creal:
	rep ret
	.size	__creal, .-__creal
	.weak	crealf32x
	.set	crealf32x,__creal
	.weak	crealf64
	.set	crealf64,__creal
	.weak	creal
	.set	creal,__creal
