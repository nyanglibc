	.text
	.p2align 4,,15
	.globl	__llroundl
	.type	__llroundl, @function
__llroundl:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	72(%rsp), %rbx
	movq	64(%rsp), %rsi
	movl	%ebx, %ecx
	movswl	%bx, %ebx
	movq	%rsi, %rdx
	andl	$32767, %ecx
	sarl	$31, %ebx
	shrq	$32, %rdx
	leal	-16383(%rcx), %eax
	orl	$1, %ebx
	cmpl	$30, %eax
	jg	.L3
	testl	%eax, %eax
	js	.L33
	movl	%edx, %ebp
	movl	%eax, %ecx
	movl	$1073741824, %edx
	sarl	%cl, %edx
	addl	%ebp, %edx
	jc	.L34
.L8:
	movl	$31, %ecx
	subl	%eax, %ecx
	shrl	%cl, %edx
.L9:
	movslq	%ebx, %rax
	addq	$40, %rsp
	imulq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$62, %eax
	jg	.L10
	subl	$16414, %ecx
	movl	$-2147483648, %ebp
	xorl	%edi, %edi
	shrl	%cl, %ebp
	movl	%edx, %edx
	addl	%ebp, %esi
	setc	%dil
	cmpl	$1, %edi
	sbbq	$-1, %rdx
	cmpl	$31, %eax
	je	.L9
	salq	%cl, %rdx
	movl	$63, %ecx
	subl	%eax, %ecx
	shrl	%cl, %esi
	movl	%esi, %ebp
	orq	%rbp, %rdx
	cmpl	$1, %ebx
	jne	.L9
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rdx
	jne	.L9
	movl	$1, %edi
	movq	%rdx, 8(%rsp)
	call	feraiseexcept@PLT
	movq	8(%rsp), %rdx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	fldt	64(%rsp)
	fnstcw	30(%rsp)
	movzwl	30(%rsp), %eax
	orb	$12, %ah
	movw	%ax, 28(%rsp)
	fldcw	28(%rsp)
	fistpq	16(%rsp)
	fldcw	30(%rsp)
	movq	16(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	$-1, %eax
	movslq	%ebx, %rbx
	movl	$0, %eax
	cmove	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	shrl	%edx
	addl	$1, %eax
	orl	$-2147483648, %edx
	jmp	.L8
	.size	__llroundl, .-__llroundl
	.weak	llroundf64x
	.set	llroundf64x,__llroundl
	.weak	llroundl
	.set	llroundl,__llroundl
