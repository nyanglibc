	.text
	.p2align 4,,15
	.globl	__log10
	.type	__log10, @function
__log10:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L7
.L2:
	jmp	__ieee754_log10@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ucomisd	%xmm1, %xmm0
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__log10, .-__log10
	.weak	log10f32x
	.set	log10f32x,__log10
	.weak	log10f64
	.set	log10f64,__log10
	.weak	log10
	.set	log10,__log10
