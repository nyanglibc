	.text
	.p2align 4,,15
	.globl	__scalblnf
	.type	__scalblnf, @function
__scalblnf:
	movd	%xmm0, %eax
	movd	%xmm0, %ecx
	sarl	$23, %eax
	andl	$255, %eax
	je	.L14
	cmpl	$255, %eax
	je	.L15
.L4:
	cmpq	$-50000, %rdi
	jl	.L12
	cmpq	$50000, %rdi
	jg	.L6
	movslq	%eax, %rdx
	addq	%rdi, %rdx
	cmpq	$254, %rdx
	jg	.L6
	addl	%edi, %eax
	testl	%eax, %eax
	jle	.L8
	andl	$-2139095041, %ecx
	sall	$23, %eax
	orl	%eax, %ecx
	movl	%ecx, -4(%rsp)
.L1:
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	andps	.LC2(%rip), %xmm0
	orps	.LC4(%rip), %xmm0
	mulss	.LC5(%rip), %xmm0
	movss	%xmm0, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$-24, %eax
	jge	.L9
.L12:
	andps	.LC2(%rip), %xmm0
	orps	.LC1(%rip), %xmm0
	mulss	.LC3(%rip), %xmm0
	movss	%xmm0, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	andl	$2147483647, %ecx
	movss	%xmm0, -4(%rsp)
	je	.L1
	mulss	.LC0(%rip), %xmm0
	movd	%xmm0, %edx
	movd	%xmm0, %ecx
	sarl	$23, %edx
	movzbl	%dl, %edx
	leal	-25(%rdx), %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	addss	%xmm0, %xmm0
	movss	%xmm0, -4(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	addl	$25, %eax
	andl	$-2139095041, %ecx
	sall	$23, %eax
	orl	%ecx, %eax
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm1
	mulss	.LC6(%rip), %xmm1
	movss	%xmm1, -4(%rsp)
	jmp	.L1
	.size	__scalblnf, .-__scalblnf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1275068416
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	228737632
	.long	0
	.long	0
	.long	0
	.align 16
.LC2:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	228737632
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	1900671690
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	1900671690
	.align 4
.LC6:
	.long	855638016
