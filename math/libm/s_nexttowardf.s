	.text
	.p2align 4,,15
	.globl	__nexttowardf
	.type	__nexttowardf, @function
__nexttowardf:
	movss	%xmm0, -16(%rsp)
	movl	-16(%rsp), %eax
	movl	%eax, %edx
	andl	$2147483647, %edx
	cmpl	$2139095040, %edx
	jg	.L2
	movq	16(%rsp), %rcx
	movl	%ecx, %esi
	andw	$32767, %si
	cmpw	$32767, %si
	je	.L23
.L3:
	fldt	8(%rsp)
	flds	-16(%rsp)
	fucomi	%st(1), %st
	jp	.L26
	je	.L24
	fstp	%st(1)
	jmp	.L5
.L26:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%edx, %edx
	je	.L25
	testl	%eax, %eax
	leal	1(%rax), %edx
	js	.L8
	fldt	8(%rsp)
	fxch	%st(1)
	subl	$1, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	cmovbe	%edx, %eax
.L11:
	movl	%eax, %edx
	andl	$2139095040, %edx
	cmpl	$2139095040, %edx
	jne	.L14
	movss	-16(%rsp), %xmm0
	addss	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
.L15:
	movl	%eax, -16(%rsp)
	movss	-16(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	8(%rsp), %rdi
	movq	%rdi, %rsi
	shrq	$32, %rsi
	andl	$2147483647, %esi
	orl	%edi, %esi
	je	.L3
.L2:
	fldt	8(%rsp)
	fadds	-16(%rsp)
	fstps	-12(%rsp)
	movss	-12(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	fstp	%st(0)
	fstps	-12(%rsp)
	movss	-12(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	fstp	%st(0)
	sall	$16, %ecx
	andl	$-2147483648, %ecx
	orl	$1, %ecx
	movl	%ecx, -16(%rsp)
	movss	-16(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$8388607, %edx
	ja	.L15
	movss	-16(%rsp), %xmm0
	mulss	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L8:
	fldt	8(%rsp)
	subl	$1, %eax
	fucomip	%st(1), %st
	fstp	%st(0)
	cmovbe	%edx, %eax
	jmp	.L11
	.size	__nexttowardf, .-__nexttowardf
	.weak	nexttowardf
	.set	nexttowardf,__nexttowardf
