	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__clog10f
	.type	__clog10f, @function
__clog10f:
	pushq	%rbx
	subq	$32, %rsp
	movq	%xmm0, 24(%rsp)
	movss	.LC4(%rip), %xmm3
	movss	24(%rsp), %xmm5
	movaps	%xmm5, %xmm1
	movss	28(%rsp), %xmm4
	andps	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm1
	jp	.L2
	ucomiss	.LC5(%rip), %xmm1
	jbe	.L90
	andps	%xmm4, %xmm3
	ucomiss	%xmm3, %xmm3
	movaps	%xmm3, %xmm2
	jp	.L43
.L8:
	ucomiss	%xmm1, %xmm2
	ja	.L15
	movaps	%xmm2, %xmm0
	movaps	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
.L15:
	ucomiss	.LC9(%rip), %xmm2
	ja	.L91
	movss	.LC6(%rip), %xmm6
	ucomiss	%xmm2, %xmm6
	jbe	.L20
	ucomiss	%xmm1, %xmm6
	ja	.L92
.L20:
	movss	.LC11(%rip), %xmm3
	ucomiss	%xmm3, %xmm2
	movss	%xmm6, (%rsp)
	jp	.L48
	jne	.L48
	mulss	%xmm1, %xmm1
	movss	%xmm4, 8(%rsp)
	movss	%xmm5, 4(%rsp)
	movaps	%xmm1, %xmm0
	call	__log1pf@PLT
	movss	.LC12(%rip), %xmm2
	mulss	%xmm0, %xmm2
	movss	(%rsp), %xmm6
	movss	4(%rsp), %xmm5
	movss	8(%rsp), %xmm4
	ucomiss	%xmm2, %xmm6
	jbe	.L24
	movaps	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	.p2align 4,,10
	.p2align 3
.L24:
	movaps	%xmm5, %xmm1
	movss	%xmm2, (%rsp)
	movaps	%xmm4, %xmm0
	call	__ieee754_atan2f@PLT
	pxor	%xmm1, %xmm1
	movss	(%rsp), %xmm2
	cvtss2sd	%xmm0, %xmm1
	movaps	%xmm2, %xmm0
	mulsd	.LC18(%rip), %xmm1
	cvtsd2ss	%xmm1, %xmm1
.L1:
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movq	16(%rsp), %xmm0
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	ucomiss	.LC6(%rip), %xmm1
	jnb	.L4
	pxor	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm5
	jp	.L4
	jne	.L4
	movaps	%xmm4, %xmm2
	andps	%xmm3, %xmm2
	ucomiss	%xmm2, %xmm2
	jp	.L83
	ucomiss	.LC5(%rip), %xmm2
	ja	.L8
	ucomiss	.LC6(%rip), %xmm2
	jnb	.L8
	ucomiss	%xmm0, %xmm4
	jp	.L8
	jne	.L8
	movd	%xmm5, %eax
	pxor	%xmm2, %xmm2
	testl	%eax, %eax
	jns	.L14
	movss	.LC0(%rip), %xmm2
.L14:
	movaps	%xmm4, %xmm7
	andps	%xmm3, %xmm2
	movss	.LC8(%rip), %xmm0
	andps	.LC7(%rip), %xmm7
	divss	%xmm1, %xmm0
	orps	%xmm7, %xmm2
	movaps	%xmm2, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	andps	%xmm4, %xmm3
	ucomiss	%xmm3, %xmm3
	movaps	%xmm3, %xmm2
	jnp	.L8
.L83:
	movss	.LC3(%rip), %xmm0
	movaps	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L91:
	movaps	%xmm2, %xmm0
	movl	$-1, %edi
	movss	%xmm4, 8(%rsp)
	movss	%xmm5, 4(%rsp)
	movss	%xmm1, (%rsp)
	call	__scalbnf@PLT
	movss	(%rsp), %xmm1
	ucomiss	.LC10(%rip), %xmm1
	movaps	%xmm0, %xmm2
	movss	4(%rsp), %xmm5
	movss	8(%rsp), %xmm4
	jnb	.L93
	pxor	%xmm1, %xmm1
	movl	$-1, %ebx
	movss	.LC11(%rip), %xmm3
.L18:
	ucomiss	%xmm3, %xmm2
	jbe	.L26
	movss	.LC13(%rip), %xmm0
	ucomiss	%xmm2, %xmm0
	jbe	.L26
	ucomiss	%xmm1, %xmm3
	jbe	.L26
	testl	%ebx, %ebx
	je	.L94
	.p2align 4,,10
	.p2align 3
.L26:
	ucomiss	%xmm2, %xmm3
	ja	.L95
.L31:
	movaps	%xmm2, %xmm0
	movss	%xmm4, 4(%rsp)
	movss	%xmm5, (%rsp)
	call	__ieee754_hypotf@PLT
	call	__ieee754_log10f@PLT
	pxor	%xmm1, %xmm1
	movaps	%xmm0, %xmm2
	movss	4(%rsp), %xmm4
	movss	(%rsp), %xmm5
	cvtsi2ss	%ebx, %xmm1
	mulss	.LC17(%rip), %xmm1
	subss	%xmm1, %xmm2
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L92:
	movaps	%xmm2, %xmm0
	movl	$24, %edi
	movss	%xmm4, 12(%rsp)
	movl	$24, %ebx
	movss	%xmm5, 8(%rsp)
	movss	%xmm1, 4(%rsp)
	call	__scalbnf@PLT
	movss	4(%rsp), %xmm1
	movl	$24, %edi
	movss	%xmm0, (%rsp)
	movaps	%xmm1, %xmm0
	call	__scalbnf@PLT
	movss	.LC11(%rip), %xmm3
	movaps	%xmm0, %xmm1
	movss	(%rsp), %xmm2
	movss	8(%rsp), %xmm5
	movss	12(%rsp), %xmm4
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L93:
	movss	%xmm0, (%rsp)
	movaps	%xmm1, %xmm0
	movl	$-1, %edi
	movl	$-1, %ebx
	call	__scalbnf@PLT
	movss	.LC11(%rip), %xmm3
	movaps	%xmm0, %xmm1
	movss	(%rsp), %xmm2
	movss	4(%rsp), %xmm5
	movss	8(%rsp), %xmm4
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%ebx, %ebx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L95:
	testl	%ebx, %ebx
	sete	%al
	ucomiss	.LC15(%rip), %xmm2
	jb	.L33
	movss	.LC16(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.L33
	testb	%al, %al
	jne	.L96
.L33:
	ucomiss	.LC15(%rip), %xmm2
	jb	.L31
	testb	%al, %al
	je	.L31
	movaps	%xmm1, %xmm3
	movaps	%xmm2, %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm0
	addss	%xmm3, %xmm0
	ucomiss	.LC15(%rip), %xmm0
	jb	.L31
	movaps	%xmm2, %xmm0
	movss	%xmm4, 4(%rsp)
	movss	%xmm5, (%rsp)
	call	__x2y2m1f@PLT
.L87:
	call	__log1pf@PLT
	movss	.LC12(%rip), %xmm2
	mulss	%xmm0, %xmm2
	movss	(%rsp), %xmm5
	movss	4(%rsp), %xmm4
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L2:
	andps	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm4
	jp	.L83
	ucomiss	.LC5(%rip), %xmm4
	jbe	.L83
.L43:
	movss	.LC2(%rip), %xmm0
	movss	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L96:
	movaps	%xmm2, %xmm0
	movss	%xmm4, 4(%rsp)
	addss	%xmm3, %xmm2
	movss	%xmm5, (%rsp)
	subss	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L94:
	movaps	%xmm2, %xmm0
	ucomiss	.LC14(%rip), %xmm1
	subss	%xmm3, %xmm0
	addss	%xmm3, %xmm2
	mulss	%xmm2, %xmm0
	jb	.L29
	mulss	%xmm1, %xmm1
	addss	%xmm1, %xmm0
.L29:
	movss	%xmm4, 4(%rsp)
	movss	%xmm5, (%rsp)
	jmp	.L87
	.size	__clog10f, .-__clog10f
	.weak	clog10f32
	.set	clog10f32,__clog10f
	.weak	clog10f
	.set	clog10f,__clog10f
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1068409826
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	2139095039
	.align 4
.LC6:
	.long	8388608
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC8:
	.long	3212836864
	.align 4
.LC9:
	.long	2130706431
	.align 4
.LC10:
	.long	16777216
	.align 4
.LC11:
	.long	1065353216
	.align 4
.LC12:
	.long	1046371289
	.align 4
.LC13:
	.long	1073741824
	.align 4
.LC14:
	.long	872415232
	.align 4
.LC15:
	.long	1056964608
	.align 4
.LC16:
	.long	864026624
	.align 4
.LC17:
	.long	1050288283
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC18:
	.long	354870542
	.long	1071369083
