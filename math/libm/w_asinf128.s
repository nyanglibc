	.text
	.globl	__unordtf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__asinf128
	.type	__asinf128, @function
__asinf128:
	subq	$40, %rsp
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	movdqa	.LC1(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L5
.L2:
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	jmp	__ieee754_asinf128@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rax
	movdqa	(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	addq	$40, %rsp
	jmp	__ieee754_asinf128@PLT
	.size	__asinf128, .-__asinf128
	.weak	asinf128
	.set	asinf128,__asinf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073676288
