	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cacosf
	.type	__cacosf, @function
__cacosf:
	subq	$72, %rsp
	movq	%xmm0, 56(%rsp)
	movss	.LC1(%rip), %xmm0
	movss	56(%rsp), %xmm3
	movaps	%xmm3, %xmm1
	movss	60(%rsp), %xmm2
	andps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jp	.L11
	ucomiss	.LC2(%rip), %xmm1
	movl	$1, %eax
	ja	.L2
	ucomiss	.LC3(%rip), %xmm1
	movl	$4, %eax
	jnb	.L2
	pxor	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm3
	jp	.L14
	movl	$2, %eax
	jne	.L14
	.p2align 4,,10
	.p2align 3
.L2:
	andps	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jp	.L25
	ucomiss	.LC2(%rip), %xmm0
	jbe	.L22
.L25:
	pxor	%xmm4, %xmm4
.L4:
	movss	%xmm3, 48(%rsp)
	movss	%xmm2, 52(%rsp)
	movss	%xmm4, 12(%rsp)
	movq	48(%rsp), %xmm0
	call	__casinf@PLT
	movq	%xmm0, 40(%rsp)
	movss	.LC4(%rip), %xmm0
	subss	40(%rsp), %xmm0
	movss	12(%rsp), %xmm4
	movss	44(%rsp), %xmm1
	ucomiss	%xmm4, %xmm0
	jp	.L8
	movd	%xmm0, %edx
	movd	%xmm4, %eax
	cmovne	%edx, %eax
	movl	%eax, 12(%rsp)
	movss	12(%rsp), %xmm0
.L8:
	xorps	.LC5(%rip), %xmm1
.L1:
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movq	16(%rsp), %xmm0
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	ucomiss	.LC3(%rip), %xmm0
	jnb	.L5
	pxor	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm2
	jp	.L5
	jne	.L5
	cmpl	$1, %eax
	jle	.L4
	cmpl	$2, %eax
	je	.L4
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1, %eax
	jle	.L25
.L7:
	xorps	.LC5(%rip), %xmm2
	movss	%xmm3, 36(%rsp)
	movl	$1, %edi
	movss	%xmm2, 32(%rsp)
	movq	32(%rsp), %xmm0
	call	__kernel_casinhf@PLT
	movq	%xmm0, 24(%rsp)
	movss	28(%rsp), %xmm0
	movss	24(%rsp), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$3, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	jmp	.L2
	.size	__cacosf, .-__cacosf
	.weak	cacosf32
	.set	cacosf32,__cacosf
	.weak	cacosf
	.set	cacosf,__cacosf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095039
	.align 4
.LC3:
	.long	8388608
	.align 4
.LC4:
	.long	1070141403
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
