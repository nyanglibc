	.text
	.p2align 4,,15
	.globl	__sinf
	.type	__sinf, @function
__sinf:
	movd	%xmm0, %eax
	pxor	%xmm1, %xmm1
	movd	%xmm0, %edx
	shrl	$20, %eax
	andl	$2047, %eax
	cvtss2sd	%xmm0, %xmm1
	cmpl	$1011, %eax
	ja	.L2
	movapd	%xmm1, %xmm3
	cmpl	$919, %eax
	mulsd	%xmm1, %xmm3
	jbe	.L15
	movapd	%xmm1, %xmm2
	movsd	96+__sincosf_table(%rip), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm3
	addsd	80+__sincosf_table(%rip), %xmm0
	mulsd	64+__sincosf_table(%rip), %xmm2
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1070, %eax
	ja	.L5
	movsd	32+__sincosf_table(%rip), %xmm0
	leaq	__sincosf_table(%rip), %rcx
	mulsd	%xmm1, %xmm0
	leaq	112(%rcx), %rdx
	cvttsd2si	%xmm0, %eax
	pxor	%xmm0, %xmm0
	addl	$8388608, %eax
	sarl	$24, %eax
	cvtsi2sd	%eax, %xmm0
	testb	$2, %al
	cmove	%rcx, %rdx
	testb	$1, %al
	mulsd	40+__sincosf_table(%rip), %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	je	.L16
	movsd	56(%rdx), %xmm0
	movapd	%xmm2, %xmm3
	movsd	72(%rdx), %xmm1
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm3
	addsd	48(%rdx), %xmm0
	mulsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	movsd	104(%rdx), %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm2
	addsd	88(%rdx), %xmm1
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	andl	$3, %eax
	movapd	%xmm2, %xmm3
	mulsd	(%rcx,%rax,8), %xmm1
	movsd	96(%rdx), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	80(%rdx), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	64(%rdx), %xmm3
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$7, %eax
	jbe	.L17
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$2039, %eax
	ja	.L8
	movd	%xmm0, %ecx
	movd	%xmm0, %esi
	leaq	__inv_pio4(%rip), %r9
	pxor	%xmm1, %xmm1
	shrl	$31, %edx
	andl	$8388607, %ecx
	shrl	$26, %esi
	movl	%ecx, %eax
	movd	%xmm0, %ecx
	andl	$15, %esi
	leaq	(%r9,%rsi,4), %rdi
	orl	$8388608, %eax
	shrl	$23, %ecx
	andl	$7, %ecx
	sall	%cl, %eax
	movl	%eax, %r8d
	movl	32(%rdi), %eax
	movq	%r8, %rcx
	imull	(%r9,%rsi,4), %ecx
	imulq	%r8, %rax
	salq	$32, %rcx
	shrq	$32, %rax
	orq	%rax, %rcx
	movl	16(%rdi), %eax
	movabsq	$-4611686018427387904, %rdi
	imulq	%r8, %rax
	addq	%rcx, %rax
	movabsq	$2305843009213693952, %rcx
	addq	%rax, %rcx
	movq	%rcx, %rsi
	andq	%rdi, %rcx
	subq	%rcx, %rax
	leaq	__sincosf_table(%rip), %rcx
	shrq	$62, %rsi
	cvtsi2sdq	%rax, %xmm1
	addl	%esi, %edx
	leaq	112(%rcx), %rax
	testb	$2, %dl
	cmove	%rcx, %rax
	andl	$1, %esi
	mulsd	.LC0(%rip), %xmm1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	je	.L18
	movsd	56(%rax), %xmm0
	movapd	%xmm2, %xmm3
	movsd	72(%rax), %xmm1
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm3
	addsd	48(%rax), %xmm0
	mulsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	movsd	104(%rax), %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm3, %xmm2
	addsd	88(%rax), %xmm1
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	jmp	__math_invalidf
.L17:
	cvtsd2ss	%xmm3, %xmm3
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	andl	$3, %edx
	movapd	%xmm2, %xmm3
	mulsd	(%rcx,%rdx,8), %xmm1
	movsd	96(%rax), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	80(%rax), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	64(%rax), %xmm3
	mulsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.size	__sinf, .-__sinf
	.weak	sinf32
	.set	sinf32,__sinf
	.weak	sinf
	.set	sinf,__sinf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1413754136
	.long	1008280059
	.hidden	__math_invalidf
	.hidden	__inv_pio4
	.hidden	__sincosf_table
