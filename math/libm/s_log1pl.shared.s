 .section .rodata
 .align 1<<4
limit: .tfloat 0.29
one: .double 1.0
 .text
.globl __log1pl
.type __log1pl,@function
.align 1<<4
__log1pl:
 fldln2
 fldt 8(%rsp)
 fxam
 fnstsw
 fld %st
 testb $1, %ah
 jnz 3f
4:
 fabs
 fldt limit(%rip)
 fcompp
 fnstsw
 andb $1,%ah
 jz 2f
 movzwl 8+8(%rsp), %eax
 xorb $0x80, %ah
 cmpl $0xc040, %eax
 jae 5f
 faddl one(%rip)
5: fyl2x
 ret
2: fyl2xp1
 ret
3: testb $4, %ah
 jnz 4b
 fstp %st(1)
 fstp %st(1)
 fadd %st(0)
 ret
.size __log1pl,.-__log1pl
