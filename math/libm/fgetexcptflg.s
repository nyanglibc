	.text
	.p2align 4,,15
	.globl	fegetexceptflag
	.type	fegetexceptflag, @function
fegetexceptflag:
#APP
# 28 "../sysdeps/x86_64/fpu/fgetexcptflg.c" 1
	fnstsw -6(%rsp)
stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-4(%rsp), %eax
	orw	-6(%rsp), %ax
	andl	$61, %eax
	andl	%eax, %esi
	xorl	%eax, %eax
	movw	%si, (%rdi)
	ret
	.size	fegetexceptflag, .-fegetexceptflag
