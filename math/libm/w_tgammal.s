	.text
	.p2align 4,,15
	.globl	__tgammal
	.type	__tgammal, @function
__tgammal:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__ieee754_gammal_r@PLT
	fld	%st(0)
	popq	%rdx
	fabs
	popq	%rcx
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L3
	je	.L2
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L25:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L3:
	movl	28(%rsp), %eax
	testl	%eax, %eax
	jns	.L22
	fchs
.L22:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fldt	48(%rsp)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L21
	fldt	48(%rsp)
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L25
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L3
	fldt	48(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	frndint
	fldcw	14(%rsp)
	fldt	48(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L10
	jne	.L10
.L14:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	fldz
	fldt	48(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L8
	jne	.L8
.L10:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	fldt	48(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	frndint
	fldcw	14(%rsp)
	fldt	48(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L10
	jne	.L10
	fldt	48(%rsp)
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L10
	jmp	.L14
	.size	__tgammal, .-__tgammal
	.weak	tgammaf64x
	.set	tgammaf64x,__tgammal
	.weak	tgammal
	.set	tgammal,__tgammal
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
