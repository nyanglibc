	.text
	.p2align 4,,15
	.globl	__GI___fegetenv
	.hidden	__GI___fegetenv
	.type	__GI___fegetenv, @function
__GI___fegetenv:
#APP
# 24 "../sysdeps/x86_64/fpu/fegetenv.c" 1
	fnstenv (%rdi)
fldenv (%rdi)
stmxcsr 28(%rdi)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	__GI___fegetenv, .-__GI___fegetenv
	.globl	__fegetenv
	.set	__fegetenv,__GI___fegetenv
	.weak	__GI_fegetenv
	.hidden	__GI_fegetenv
	.set	__GI_fegetenv,__fegetenv
	.weak	fegetenv
	.set	fegetenv,__GI_fegetenv
