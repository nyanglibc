	.text
	.p2align 4,,15
	.globl	__cimagl
	.type	__cimagl, @function
__cimagl:
	fldt	24(%rsp)
	ret
	.size	__cimagl, .-__cimagl
	.weak	cimagf64x
	.set	cimagf64x,__cimagl
	.weak	cimagl
	.set	cimagl,__cimagl
