	.text
	.p2align 4,,15
	.globl	__nextupf
	.type	__nextupf, @function
__nextupf:
	movd	%xmm0, %edx
	movd	%xmm0, %eax
	andl	$2147483647, %edx
	je	.L7
	cmpl	$2139095040, %edx
	jg	.L12
	testl	%eax, %eax
	js	.L4
	movaps	%xmm0, %xmm1
	addl	$1, %eax
	andps	.LC1(%rip), %xmm1
	ucomiss	.LC2(%rip), %xmm1
	ja	.L13
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movss	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	subl	$1, %eax
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	rep ret
	.size	__nextupf, .-__nextupf
	.weak	nextupf32
	.set	nextupf32,__nextupf
	.weak	nextupf
	.set	nextupf,__nextupf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC2:
	.long	2139095039
