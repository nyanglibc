	.text
	.globl	__addtf3
	.globl	__lttf2
	.globl	__multf3
	.globl	__gttf2
	.globl	__divtf3
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__subtf3
	.p2align 4,,15
	.globl	__atanf128
	.type	__atanf128, @function
__atanf128:
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %rbp
	shrq	$32, %rbp
	movl	%ebp, %ebx
	andl	$2147483647, %ebx
	cmpl	$2147418111, %ebx
	jle	.L2
	movq	(%rsp), %rdx
	movzwl	%bx, %ebx
	movq	%rdx, %rcx
	shrq	$32, %rcx
	orl	%ecx, %eax
	orl	%edx, %eax
	orl	%ebx, %eax
	jne	.L21
	testl	%ebp, %ebp
	jns	.L14
.L22:
	movdqa	.LC1(%rip), %xmm0
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1069875200, %ebx
	jg	.L5
	movdqa	(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC3(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L6
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L6:
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	pxor	%xmm1, %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	jg	.L1
.L8:
	testl	%ebp, %ebp
	jns	.L9
	movdqa	(%rsp), %xmm3
	pxor	.LC6(%rip), %xmm3
	movaps	%xmm3, (%rsp)
.L9:
	cmpl	$1073891327, %ebx
	jle	.L10
	movdqa	(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	.LC0(%rip), %xmm4
	movdqa	%xmm0, %xmm2
	movaps	%xmm4, 16(%rsp)
.L11:
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC12(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm2
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	48(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	testl	%ebp, %ebp
	jns	.L1
	pxor	.LC6(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1081212927, %ebx
	jle	.L8
	testl	%ebp, %ebp
	js	.L22
.L14:
	movdqa	.LC0(%rip), %xmm0
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	call	__fixtfsi@PLT
	movslq	%eax, %rbx
	movl	%ebx, %edi
	salq	$4, %rbx
	call	__floatsitf@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	leaq	atantbl(%rip), %rax
	movdqa	%xmm0, %xmm2
	movdqa	(%rax,%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
	jmp	.L11
	.size	__atanf128, .-__atanf128
	.weak	atanf128
	.set	atanf128,__atanf128
	.section	.rodata
	.align 32
	.type	atantbl, @object
	.size	atantbl, 1344
atantbl:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	826178833
	.long	3697643823
	.long	2846540534
	.long	1073479003
	.long	1038872388
	.long	3593132943
	.long	1603454989
	.long	1073542583
	.long	1111865853
	.long	149361990
	.long	2485013999
	.long	1073573729
	.long	2346285173
	.long	4136296392
	.long	1884691380
	.long	1073601222
	.long	3157052296
	.long	1060550918
	.long	3133009899
	.long	1073618432
	.long	4266953467
	.long	306721616
	.long	4197607918
	.long	1073629560
	.long	1252270409
	.long	1021810267
	.long	2086110307
	.long	1073639434
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073648159
	.long	2990666542
	.long	2030427301
	.long	4085664200
	.long	1073655860
	.long	3026361036
	.long	3828791374
	.long	3312994041
	.long	1073662663
	.long	2865559021
	.long	400470404
	.long	3561784669
	.long	1073668685
	.long	1578727316
	.long	2986406159
	.long	3173523305
	.long	1073674032
	.long	3054106035
	.long	1891142308
	.long	1920442397
	.long	1073677542
	.long	956343308
	.long	3909863671
	.long	4073044489
	.long	1073679672
	.long	4120643047
	.long	249483944
	.long	3714027726
	.long	1073681585
	.long	2720048027
	.long	1187434906
	.long	422493156
	.long	1073683310
	.long	899797508
	.long	1696734602
	.long	792702435
	.long	1073684870
	.long	673104168
	.long	1231442836
	.long	4116337570
	.long	1073686286
	.long	766130723
	.long	906677078
	.long	605906652
	.long	1073687578
	.long	250373694
	.long	2240369452
	.long	3616974042
	.long	1073688758
	.long	2582395557
	.long	2466361142
	.long	4222721712
	.long	1073689841
	.long	3942519922
	.long	769290498
	.long	2798523189
	.long	1073690838
	.long	1671240510
	.long	1974308557
	.long	1695234052
	.long	1073691758
	.long	2239880953
	.long	4292312248
	.long	1991747669
	.long	1073692609
	.long	4263580352
	.long	307630518
	.long	68286956
	.long	1073693399
	.long	4007316544
	.long	27150962
	.long	1211577626
	.long	1073694133
	.long	1832282490
	.long	4283744609
	.long	3033513584
	.long	1073694817
	.long	1979886274
	.long	1259597917
	.long	264657050
	.long	1073695457
	.long	3814060476
	.long	3474707500
	.long	2350487968
	.long	1073696055
	.long	2885714808
	.long	1984773756
	.long	3735450209
	.long	1073696616
	.long	3926915536
	.long	2607957773
	.long	1335972939
	.long	1073697144
	.long	3713631184
	.long	3382980122
	.long	3377588687
	.long	1073697640
	.long	2680820038
	.long	1275702228
	.long	3834250358
	.long	1073698108
	.long	2430680138
	.long	1847628066
	.long	4080680554
	.long	1073698550
	.long	537406068
	.long	2870884062
	.long	158654409
	.long	1073698969
	.long	2313036163
	.long	25963788
	.long	86054118
	.long	1073699365
	.long	3993556249
	.long	774130740
	.long	2492776663
	.long	1073699740
	.long	1698125636
	.long	3621826280
	.long	1010937259
	.long	1073700097
	.long	814430410
	.long	2033720641
	.long	1536891468
	.long	1073700436
	.long	2023081674
	.long	1839060427
	.long	827479173
	.long	1073700759
	.long	2105943341
	.long	3415032035
	.long	3741574212
	.long	1073701066
	.long	2250885982
	.long	2372863326
	.long	1818923704
	.long	1073701360
	.long	1360550769
	.long	2463326069
	.long	3391758050
	.long	1073701640
	.long	1759713880
	.long	3102222294
	.long	3561002183
	.long	1073701908
	.long	1173460284
	.long	3026907800
	.long	1412095797
	.long	1073702165
	.long	1288527635
	.long	1582516560
	.long	46610080
	.long	1073702411
	.long	3551227297
	.long	1898539834
	.long	2315153101
	.long	1073702646
	.long	2019350804
	.long	3381792948
	.long	2252065179
	.long	1073702872
	.long	1243956851
	.long	140947372
	.long	2277102278
	.long	1073703089
	.long	3952969523
	.long	689566141
	.long	329897221
	.long	1073703298
	.long	3863456531
	.long	2373524668
	.long	2772085828
	.long	1073703498
	.long	1074937148
	.long	4195063071
	.long	2927823878
	.long	1073703691
	.long	332532165
	.long	295140732
	.long	2572336955
	.long	1073703877
	.long	2285893740
	.long	1447618229
	.long	3354258201
	.long	1073704056
	.long	1313972084
	.long	901052692
	.long	2511664532
	.long	1073704229
	.long	3786786394
	.long	3411598958
	.long	1471895135
	.long	1073704396
	.long	1954260221
	.long	2960068459
	.long	1565478180
	.long	1073704557
	.long	507641146
	.long	3168078621
	.long	4034147838
	.long	1073704712
	.long	1339095278
	.long	4134280251
	.long	1448148286
	.long	1073704863
	.long	2648612744
	.long	1848909430
	.long	3482583833
	.long	1073705008
	.long	1614844576
	.long	812276094
	.long	2563610179
	.long	1073705149
	.long	322272559
	.long	940141082
	.long	3938584714
	.long	1073705285
	.long	1730440933
	.long	666143407
	.long	4206117195
	.long	1073705417
	.long	3523418599
	.long	379793529
	.long	4205418146
	.long	1073705545
	.long	376208587
	.long	136341693
	.long	430415541
	.long	1073705670
	.long	3496831905
	.long	929728301
	.long	2213320191
	.long	1073705790
	.long	2343472528
	.long	2033582680
	.long	1663231399
	.long	1073705907
	.long	4101597259
	.long	991307961
	.long	3733996877
	.long	1073706020
	.long	4228436232
	.long	2498523951
	.long	457238024
	.long	1073706131
	.long	474157346
	.long	2096439213
	.long	1009715287
	.long	1073706238
	.long	565120946
	.long	1243168740
	.long	1650938915
	.long	1073706342
	.long	3446748462
	.long	1417867819
	.long	2905227453
	.long	1073706443
	.long	2038894480
	.long	3024042029
	.long	973787773
	.long	1073706542
	.long	3202941798
	.long	3271194920
	.long	621472879
	.long	1073706638
	.long	3158290792
	.long	244421730
	.long	2293591566
	.long	1073706731
	.long	850530842
	.long	151033580
	.long	2117488480
	.long	1073706822
	.long	1595130630
	.long	3450839533
	.long	493938879
	.long	1073706911
	.long	1577106740
	.long	3202280539
	.long	2098499345
	.long	1073706997
	.long	3589858460
	.long	604962543
	.long	2997856569
	.long	1073707081
	.long	3444271790
	.long	942582768
	.long	3535888389
	.long	1073707163
	.long	3526791714
	.long	378884627
	.long	4039771812
	.long	1073707243
	.long	260369404
	.long	988960570
	.long	526014161
	.long	1073707322
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC1:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	-1073769953
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC4:
	.long	212747462
	.long	4097407749
	.long	1775143647
	.long	2146964332
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073872896
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1073479680
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC12:
	.long	2176379689
	.long	257051585
	.long	3447108485
	.long	-1073825210
	.align 16
.LC13:
	.long	1226340449
	.long	1831966981
	.long	2387645331
	.long	1073919705
	.align 16
.LC14:
	.long	544870077
	.long	3714941262
	.long	2591119617
	.long	1074055445
	.align 16
.LC15:
	.long	33161699
	.long	2346180893
	.long	4281190175
	.long	1074092401
	.align 16
.LC16:
	.long	2703186739
	.long	2733936839
	.long	1490945572
	.long	1074026162
	.align 16
.LC17:
	.long	1313898916
	.long	2398032562
	.long	2699234708
	.long	1073961927
	.align 16
.LC18:
	.long	859915346
	.long	3499625818
	.long	3864669212
	.long	1074136434
	.align 16
.LC19:
	.long	460728862
	.long	3430850131
	.long	2046890882
	.long	1074216459
	.align 16
.LC20:
	.long	3743688556
	.long	2160023093
	.long	3546355385
	.long	1074221104
	.align 16
.LC21:
	.long	3101133049
	.long	2050452629
	.long	3265692827
	.long	1074135301
