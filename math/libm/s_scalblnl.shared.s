	.text
	.p2align 4,,15
	.globl	__scalblnl
	.type	__scalblnl, @function
__scalblnl:
	movq	16(%rsp), %rax
	movswl	%ax, %ecx
	andw	$32767, %ax
	testw	%ax, %ax
	movswl	%ax, %edx
	je	.L26
	cmpl	$32767, %edx
	je	.L27
.L4:
	cmpq	$-50000, %rdi
	jl	.L28
	cmpq	$50000, %rdi
	jg	.L7
	movslq	%edx, %rax
	addq	%rdi, %rax
	cmpq	$32766, %rax
	jg	.L7
	addl	%edx, %edi
	testl	%edi, %edi
	jle	.L10
	fldt	8(%rsp)
	andw	$-32768, %cx
	orl	%ecx, %edi
	fstpt	-24(%rsp)
	movw	%di, -16(%rsp)
	fldt	-24(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fldt	8(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC3(%rip)
	je	.L9
	fstp	%st(0)
	fldt	.LC4(%rip)
.L9:
	fldt	.LC3(%rip)
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	8(%rsp), %rdx
	fldt	8(%rsp)
	movq	%rdx, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	orl	%edx, %eax
	je	.L1
	fmuls	.LC0(%rip)
	fstpt	8(%rsp)
	movq	16(%rsp), %rax
	movswl	%ax, %ecx
	andl	$32767, %eax
	leal	-63(%rax), %edx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$-63, %edi
	fldt	8(%rsp)
	jl	.L29
	andw	$-32768, %cx
	addl	$64, %edi
	orl	%ecx, %edi
	fstpt	-24(%rsp)
	movw	%di, -16(%rsp)
	fldt	-24(%rsp)
	fmuls	.LC5(%rip)
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L27:
	fldt	8(%rsp)
	fadd	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	fldt	8(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC1(%rip)
	jne	.L30
.L12:
	fldt	.LC1(%rip)
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC1(%rip)
	je	.L12
	fstp	%st(0)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	fstp	%st(0)
.L24:
	fldt	.LC2(%rip)
	jmp	.L12
	.size	__scalblnl, .-__scalblnl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1593835520
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	3136694642
	.long	3149193046
	.long	105
	.long	0
	.align 16
.LC2:
	.long	3136694642
	.long	3149193046
	.long	32873
	.long	0
	.align 16
.LC3:
	.long	1496818881
	.long	2928804903
	.long	32660
	.long	0
	.align 16
.LC4:
	.long	1496818881
	.long	2928804903
	.long	65428
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	528482304
