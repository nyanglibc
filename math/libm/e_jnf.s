	.text
	.p2align 4,,15
	.globl	__ieee754_jnf
	.type	__ieee754_jnf, @function
__ieee754_jnf:
	movd	%xmm0, %edx
	andl	$2147483647, %edx
	cmpl	$2139095040, %edx
	jg	.L72
	pushq	%r13
	pushq	%r12
	movd	%xmm0, %eax
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	testl	%edi, %edi
	js	.L73
	jne	.L5
	call	__ieee754_j0f@PLT
.L1:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	xorps	.LC3(%rip), %xmm0
	negl	%edi
	addl	$-2147483648, %eax
.L5:
	cmpl	$1, %edi
	je	.L74
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 88(%rsp)
# 0 "" 2
#NO_APP
	movl	88(%rsp), %r12d
	shrl	$31, %eax
	movss	.LC4(%rip), %xmm4
	andl	%edi, %eax
	movaps	%xmm0, %xmm2
	movl	%eax, %ebp
	xorl	%r13d, %r13d
	movl	%r12d, %eax
	andps	%xmm4, %xmm2
	andb	$-97, %ah
	cmpl	%eax, %r12d
	movl	%eax, 92(%rsp)
	jne	.L75
.L7:
	testl	%edx, %edx
	je	.L44
	cmpl	$2139095039, %edx
	jg	.L44
	pxor	%xmm9, %xmm9
	cvtsi2ss	%edi, %xmm9
	ucomiss	%xmm9, %xmm2
	jnb	.L76
	cmpl	$813694975, %edx
	jg	.L15
	cmpl	$33, %edi
	jg	.L39
	mulss	.LC5(%rip), %xmm2
	movss	.LC2(%rip), %xmm1
	movl	$2, %eax
	movaps	%xmm2, %xmm0
	.p2align 4,,10
	.p2align 3
.L16:
	pxor	%xmm3, %xmm3
	mulss	%xmm2, %xmm0
	cvtsi2ss	%eax, %xmm3
	addl	$1, %eax
	cmpl	%edi, %eax
	mulss	%xmm3, %xmm1
	jle	.L16
	divss	%xmm1, %xmm0
.L70:
	pxor	%xmm8, %xmm8
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	pxor	%xmm8, %xmm8
	leal	(%rdi,%rdi), %ebx
	movss	.LC6(%rip), %xmm6
	movl	$1, %eax
	movaps	%xmm6, %xmm0
	cvtsi2ss	%ebx, %xmm8
	divss	%xmm2, %xmm0
	movss	.LC2(%rip), %xmm5
	movss	.LC7(%rip), %xmm10
	divss	%xmm2, %xmm8
	movaps	%xmm8, %xmm7
	movaps	%xmm8, %xmm1
	addss	%xmm0, %xmm7
	mulss	%xmm7, %xmm1
	subss	%xmm5, %xmm1
	ucomiss	%xmm1, %xmm10
	jbe	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	addss	%xmm0, %xmm7
	movaps	%xmm1, %xmm3
	addl	$1, %eax
	mulss	%xmm7, %xmm3
	subss	%xmm8, %xmm3
	movaps	%xmm1, %xmm8
	ucomiss	%xmm3, %xmm10
	movaps	%xmm3, %xmm1
	ja	.L19
.L17:
	leal	(%rax,%rdi), %edx
	pxor	%xmm8, %xmm8
	pxor	%xmm1, %xmm1
	cmpl	%edi, %edx
	leal	(%rdx,%rdx), %eax
	jl	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	pxor	%xmm3, %xmm3
	movaps	%xmm5, %xmm7
	cvtsi2ss	%eax, %xmm3
	subl	$2, %eax
	cmpl	%eax, %ebx
	divss	%xmm2, %xmm3
	subss	%xmm1, %xmm3
	divss	%xmm3, %xmm7
	movaps	%xmm7, %xmm1
	jle	.L21
.L20:
	mulss	%xmm9, %xmm0
	movss	%xmm6, 76(%rsp)
	movl	%edi, 68(%rsp)
	movss	%xmm5, 72(%rsp)
	subl	$2, %ebx
	movss	%xmm8, 48(%rsp)
	movss	%xmm2, 32(%rsp)
	andps	%xmm4, %xmm0
	movss	%xmm1, 64(%rsp)
	movss	%xmm9, 28(%rsp)
	movaps	%xmm4, (%rsp)
	call	__ieee754_logf@PLT
	movss	28(%rsp), %xmm9
	movl	68(%rsp), %edi
	mulss	%xmm0, %xmm9
	movss	.LC8(%rip), %xmm0
	movaps	(%rsp), %xmm4
	subl	$1, %edi
	movss	64(%rsp), %xmm1
	ucomiss	%xmm9, %xmm0
	pxor	%xmm0, %xmm0
	movss	32(%rsp), %xmm2
	movss	48(%rsp), %xmm8
	movss	72(%rsp), %xmm5
	movss	76(%rsp), %xmm6
	cvtsi2ss	%ebx, %xmm0
	jbe	.L64
	movaps	%xmm1, %xmm7
	movaps	%xmm5, %xmm9
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L42:
	movaps	%xmm3, %xmm9
.L24:
	movaps	%xmm9, %xmm3
	subl	$1, %edi
	mulss	%xmm0, %xmm3
	subss	%xmm6, %xmm0
	divss	%xmm2, %xmm3
	subss	%xmm7, %xmm3
	movaps	%xmm9, %xmm7
	jne	.L42
.L25:
	movaps	%xmm2, %xmm0
	movss	%xmm8, 68(%rsp)
	movaps	%xmm4, 48(%rsp)
	movss	%xmm1, 32(%rsp)
	movss	%xmm9, 64(%rsp)
	movss	%xmm3, 72(%rsp)
	movss	%xmm2, 28(%rsp)
	call	__ieee754_j0f@PLT
	movss	28(%rsp), %xmm2
	movss	%xmm0, (%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_j1f@PLT
	movss	(%rsp), %xmm5
	movaps	48(%rsp), %xmm4
	movaps	%xmm5, %xmm6
	movaps	%xmm0, %xmm2
	andps	%xmm4, %xmm6
	movss	64(%rsp), %xmm9
	andps	%xmm4, %xmm2
	movss	32(%rsp), %xmm1
	movss	68(%rsp), %xmm8
	ucomiss	%xmm2, %xmm6
	jb	.L66
	mulss	%xmm5, %xmm1
	movss	72(%rsp), %xmm3
	movaps	%xmm1, %xmm0
	divss	%xmm3, %xmm0
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$1, %ebp
	jne	.L31
	xorps	.LC3(%rip), %xmm0
.L31:
	testb	%r13b, %r13b
	jne	.L77
.L32:
	ucomiss	%xmm8, %xmm0
	jp	.L33
	je	.L78
.L33:
	andps	%xmm0, %xmm4
	movss	.LC11(%rip), %xmm1
	ucomiss	%xmm4, %xmm1
	jbe	.L1
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	call	__ieee754_j1f@PLT
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	andps	.LC3(%rip), %xmm0
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	orps	.LC10(%rip), %xmm0
	mulss	.LC11(%rip), %xmm0
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movaps	%xmm2, %xmm0
	movl	%edi, 64(%rsp)
	movaps	%xmm4, 32(%rsp)
	movss	%xmm2, (%rsp)
	call	__ieee754_j0f@PLT
	movss	(%rsp), %xmm2
	movss	%xmm0, 28(%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_j1f@PLT
	movss	(%rsp), %xmm2
	movl	$1, %edx
	movaps	%xmm0, %xmm5
	cvtss2sd	%xmm2, %xmm2
	movss	28(%rsp), %xmm3
	movl	64(%rsp), %edi
	movaps	32(%rsp), %xmm4
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L38:
	movaps	%xmm0, %xmm5
.L13:
	pxor	%xmm1, %xmm1
	leal	(%rdx,%rdx), %eax
	cvtss2sd	%xmm3, %xmm3
	pxor	%xmm0, %xmm0
	addl	$1, %edx
	cmpl	%edi, %edx
	cvtsi2sd	%eax, %xmm1
	cvtss2sd	%xmm5, %xmm0
	divsd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	subsd	%xmm3, %xmm1
	movaps	%xmm5, %xmm3
	cvtsd2ss	%xmm1, %xmm0
	jl	.L38
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L72:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	pxor	%xmm8, %xmm8
	pxor	%xmm0, %xmm0
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L66:
	mulss	%xmm1, %xmm0
	divss	%xmm9, %xmm0
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L64:
	movaps	%xmm1, %xmm9
	movaps	%xmm5, %xmm3
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L79:
	divss	%xmm7, %xmm3
	divss	%xmm7, %xmm1
	movaps	%xmm3, %xmm9
	movaps	%xmm5, %xmm3
.L26:
	subl	$1, %edi
	je	.L25
.L28:
	movaps	%xmm3, %xmm7
	mulss	%xmm0, %xmm7
	subss	%xmm6, %xmm0
	divss	%xmm2, %xmm7
	subss	%xmm9, %xmm7
	ucomiss	.LC9(%rip), %xmm7
	ja	.L79
	movaps	%xmm3, %xmm9
	movaps	%xmm7, %xmm3
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L44:
	cmpl	$1, %ebp
	pxor	%xmm0, %xmm0
	jne	.L10
	movss	.LC1(%rip), %xmm0
.L10:
	testb	%r13b, %r13b
	je	.L1
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	92(%rsp), %eax
	andl	$24576, %r12d
	andb	$-97, %ah
	orl	%eax, %r12d
	movl	%r12d, 92(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L75:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r13d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L77:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	92(%rsp), %eax
	andl	$24576, %r12d
	andb	$-97, %ah
	orl	%eax, %r12d
	movl	%r12d, 92(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L32
	.size	__ieee754_jnf, .-__ieee754_jnf
	.p2align 4,,15
	.globl	__ieee754_ynf
	.type	__ieee754_ynf, @function
__ieee754_ynf:
	movd	%xmm0, %eax
	movaps	%xmm0, %xmm4
	andl	$2147483647, %eax
	cmpl	$2139095040, %eax
	jg	.L127
	pushq	%r12
	pushq	%rbp
	movd	%xmm0, %edx
	pushq	%rbx
	subq	$32, %rsp
	testl	%edi, %edi
	js	.L128
	jne	.L101
	call	__ieee754_y0f@PLT
.L80:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movl	$1, %ebx
.L84:
	testl	%eax, %eax
	je	.L129
	testl	%edx, %edx
	js	.L130
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	movl	24(%rsp), %ebp
	movl	%ebp, %r12d
	andl	$-24577, %r12d
	cmpl	%r12d, %ebp
	movl	%r12d, 28(%rsp)
	jne	.L131
	cmpl	$1, %edi
	je	.L132
	cmpl	$2139095040, %eax
	je	.L126
	xorl	%r12d, %r12d
.L100:
	movaps	%xmm4, %xmm0
	movl	%edi, 8(%rsp)
	movss	%xmm4, 4(%rsp)
	call	__ieee754_y0f@PLT
	movss	4(%rsp), %xmm4
	movss	%xmm0, 12(%rsp)
	movaps	%xmm4, %xmm0
	call	__ieee754_y1f@PLT
	movd	%xmm0, %eax
	movaps	%xmm0, %xmm3
	cmpl	$-8388608, %eax
	je	.L102
	movl	8(%rsp), %edi
	cmpl	$1, %edi
	jle	.L102
	movss	4(%rsp), %xmm4
	movl	$1, %edx
	cvtss2sd	%xmm4, %xmm4
	movss	12(%rsp), %xmm2
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L133:
	movd	%xmm0, %eax
	cmpl	$-8388608, %eax
	je	.L90
	movaps	%xmm0, %xmm3
.L91:
	pxor	%xmm1, %xmm1
	leal	(%rdx,%rdx), %eax
	cvtss2sd	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	addl	$1, %edx
	cmpl	%edi, %edx
	cvtsi2sd	%eax, %xmm1
	cvtss2sd	%xmm3, %xmm0
	divsd	%xmm4, %xmm1
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	subsd	%xmm2, %xmm1
	movaps	%xmm3, %xmm2
	cvtsd2ss	%xmm1, %xmm0
	jl	.L133
.L90:
	movss	.LC4(%rip), %xmm2
	movaps	%xmm0, %xmm1
	movss	.LC12(%rip), %xmm3
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.L134
.L93:
	cmpl	$1, %ebx
	je	.L95
	movaps	%xmm0, %xmm3
	movaps	%xmm0, %xmm1
	xorps	.LC3(%rip), %xmm3
	andps	%xmm2, %xmm1
	movaps	%xmm3, %xmm0
.L95:
	testb	%r12b, %r12b
	jne	.L135
.L96:
	ucomiss	.LC12(%rip), %xmm1
	jbe	.L80
	andps	.LC3(%rip), %xmm0
	orps	.LC13(%rip), %xmm0
	mulss	.LC12(%rip), %xmm0
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	negl	%edi
	movl	$1, %ebx
	leal	(%rdi,%rdi), %ecx
	andl	$2, %ecx
	subl	%ecx, %ebx
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L134:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L127:
	addss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	movaps	%xmm4, %xmm0
	call	__ieee754_y1f@PLT
	pxor	%xmm1, %xmm1
	cvtsi2ss	%ebx, %xmm1
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC4(%rip), %xmm1
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L129:
	pxor	%xmm0, %xmm0
	negl	%ebx
	cvtsi2ss	%ebx, %xmm0
	divss	.LC0(%rip), %xmm0
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L130:
	pxor	%xmm2, %xmm2
	mulss	%xmm2, %xmm4
	divss	%xmm4, %xmm2
	movaps	%xmm2, %xmm0
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L102:
	movaps	%xmm3, %xmm0
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L131:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	cmpl	$1, %edi
	je	.L136
	cmpl	$2139095040, %eax
	je	.L99
	movl	$1, %r12d
	jmp	.L100
.L99:
	movl	%ebp, %eax
	andl	$24576, %eax
	orl	%r12d, %eax
	movl	%eax, 28(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L126:
	pxor	%xmm0, %xmm0
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L135:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %eax
	andl	$24576, %ebp
	andb	$-97, %ah
	orl	%eax, %ebp
	movl	%ebp, 28(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L96
.L136:
	movaps	%xmm4, %xmm0
	andl	$24576, %ebp
	orl	%r12d, %ebp
	call	__ieee754_y1f@PLT
	pxor	%xmm1, %xmm1
	movl	%ebp, 28(%rsp)
	cvtsi2ss	%ebx, %xmm1
	mulss	%xmm1, %xmm0
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movaps	%xmm0, %xmm1
	andps	.LC4(%rip), %xmm1
	jmp	.L96
	.size	__ieee754_ynf, .-__ieee754_ynf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	2147483648
	.align 4
.LC2:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 16
.LC4:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	1056964608
	.align 4
.LC6:
	.long	1073741824
	.align 4
.LC7:
	.long	1315859240
	.align 4
.LC8:
	.long	1118925184
	.align 4
.LC9:
	.long	1343554297
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	8388608
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC11:
	.long	8388608
	.align 4
.LC12:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC13:
	.long	2139095039
	.long	0
	.long	0
	.long	0
