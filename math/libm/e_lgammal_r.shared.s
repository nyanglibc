	.text
#APP
	.symver __ieee754_lgammal_r,__lgammal_r_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_lgammal_r
	.type	__ieee754_lgammal_r, @function
__ieee754_lgammal_r:
	flds	.LC77(%rip)
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movl	$1, (%rdi)
	fldt	96(%rsp)
	fstpt	(%rsp)
	movq	(%rsp), %rdx
	movq	8(%rsp), %r12
	movq	%rdx, %rax
	movl	%r12d, %ebx
	movl	%edx, %ebp
	shrq	$32, %rax
	andl	$32767, %ebx
	orl	%eax, %ebp
	movl	%ebx, %ecx
	orl	%ebp, %ecx
	je	.L69
	sall	$16, %ebx
	shrl	$16, %eax
	orl	%eax, %ebx
	cmpl	$2147418111, %ebx
	ja	.L70
	cmpl	$1069580287, %ebx
	movq	%rdi, %r13
	jbe	.L71
	testw	%r12w, %r12w
	js	.L72
	leal	-1073709056(%rbx), %eax
	orl	%ebp, %eax
	je	.L79
	leal	-1073774592(%rbx), %eax
	orl	%ebp, %eax
	je	.L80
.L49:
	cmpl	$1073774591, %ebx
	jg	.L32
	cmpl	$1073669734, %ebx
	jle	.L73
	cmpl	$1073733029, %ebx
	jle	.L38
	fldt	(%rsp)
	fsubs	.LC9(%rip)
	fldz
.L35:
	fldt	.LC12(%rip)
	fmul	%st(2), %st
	fldt	.LC13(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC14(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC15(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC16(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC17(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC18(%rip)
	fadd	%st(3), %st
	fmul	%st(3), %st
	fldt	.LC19(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC20(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC21(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC22(%rip)
	faddp	%st, %st(1)
	fdivrp	%st, %st(1)
	fxch	%st(2)
	fmuls	.LC5(%rip)
	faddp	%st, %st(2)
	faddp	%st, %st(1)
	fxch	%st(1)
	jmp	.L40
.L85:
	fstp	%st(1)
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L40:
	testw	%r12w, %r12w
	jns	.L81
	fsubp	%st, %st(1)
.L77:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	fstp	%st(0)
	fldt	(%rsp)
	flds	.LC2(%rip)
	fucomip	%st(1), %st
	jbe	.L82
	flds	.LC3(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L9
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__lgamma_negl@PLT
	.p2align 4,,10
	.p2align 3
.L79:
	fstp	%st(0)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L80:
	fstp	%st(0)
.L57:
	fldz
	jmp	.L1
.L81:
	fstp	%st(0)
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	fstp	%st(0)
.L9:
	cmpl	$1073577983, %ebx
	jle	.L74
	fldt	(%rsp)
	fnstcw	46(%rsp)
	movzwl	46(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 38(%rsp)
	fchs
	fld	%st(0)
	fldcw	38(%rsp)
	frndint
	fldcw	46(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L61
	je	.L64
.L61:
	fmuls	.LC5(%rip)
	fld	%st(0)
	fldcw	38(%rsp)
	frndint
	fldcw	46(%rsp)
	fsubrp	%st, %st(1)
	fnstcw	46(%rsp)
	movzwl	46(%rsp), %eax
	fadd	%st(0), %st
	orb	$12, %ah
	movw	%ax, 44(%rsp)
	fld	%st(0)
	fmuls	.LC6(%rip)
	fldcw	44(%rsp)
	fistpl	40(%rsp)
	fldcw	46(%rsp)
	movl	40(%rsp), %eax
.L16:
	cmpl	$6, %eax
	ja	.L19
	leaq	.L21(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L21:
	.long	.L20-.L21
	.long	.L22-.L21
	.long	.L22-.L21
	.long	.L23-.L21
	.long	.L23-.L21
	.long	.L24-.L21
	.long	.L24-.L21
	.text
	.p2align 4,,10
	.p2align 3
.L74:
	fldpi
	subq	$16, %rsp
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__sinl@PLT
	popq	%r11
	popq	%rax
	fldz
	fxch	%st(1)
.L13:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L83
	jne	.L84
	fstp	%st(1)
	fabs
	addq	$56, %rsp
	popq	%rbx
	fld1
	popq	%rbp
	fdivp	%st, %st(1)
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	fstp	%st(0)
	testw	%r12w, %r12w
	jns	.L3
	movl	$-1, (%rdi)
.L3:
	fldt	(%rsp)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	fabs
	fld1
	fdivp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	fstp	%st(0)
	fldt	(%rsp)
	fmul	%st(0), %st
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L71:
	fstp	%st(0)
	testw	%r12w, %r12w
	js	.L75
	pushq	8(%rsp)
	pushq	8(%rsp)
	call	__ieee754_logl@PLT
	popq	%rax
	fchs
	popq	%rdx
	jmp	.L1
.L83:
	fxch	%st(1)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L84:
	fxch	%st(1)
.L26:
	fstps	32(%rsp)
	subq	$16, %rsp
	fldt	16(%rsp)
	fmul	%st(1), %st
	fxch	%st(1)
	fstpt	32(%rsp)
	fabs
	fldpi
	fdivp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	popq	%r11
	popq	%rax
	fldt	16(%rsp)
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L76
.L28:
	leal	-1073709056(%rbx), %eax
	orl	%ebp, %eax
	je	.L58
	leal	-1073774592(%rbx), %eax
	orl	%ebp, %eax
	je	.L58
	fldt	(%rsp)
	fchs
	fstpt	(%rsp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L58:
	fldz
	fsubrp	%st, %st(1)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L64:
	fstp	%st(0)
	cmpl	$1077903359, %ebx
	jle	.L78
	fldz
	fldz
.L17:
	fstps	16(%rsp)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__sinl@PLT
	popq	%r9
	popq	%r10
	flds	16(%rsp)
.L25:
	fxch	%st(1)
	fchs
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$1073905663, %ebx
	jg	.L41
	fldt	(%rsp)
	fnstcw	46(%rsp)
	movzwl	46(%rsp), %eax
	orb	$12, %ah
	movw	%ax, 44(%rsp)
	fld	%st(0)
	fldcw	44(%rsp)
	fistpl	16(%rsp)
	fldcw	46(%rsp)
	movl	16(%rsp), %eax
	fildl	16(%rsp)
	subl	$3, %eax
	cmpl	$4, %eax
	fsubrp	%st, %st(1)
	fldt	.LC52(%rip)
	fmul	%st(1), %st
	fldt	.LC53(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC54(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC55(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC56(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC57(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC58(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC59(%rip)
	fsubr	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC60(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC61(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC62(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC63(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC64(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC65(%rip)
	fsubrp	%st, %st(1)
	fdivrp	%st, %st(1)
	fld	%st(1)
	fmuls	.LC5(%rip)
	faddp	%st, %st(1)
	ja	.L85
	leaq	.L43(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L43:
	.long	.L52-.L43
	.long	.L53-.L43
	.long	.L54-.L43
	.long	.L55-.L43
	.long	.L47-.L43
	.text
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$1077837823, %ebx
	jg	.L18
	fldt	(%rsp)
	fsubrs	.LC7(%rip)
	fstpt	16(%rsp)
.L18:
	movq	16(%rsp), %rax
	andl	$1, %eax
	movl	%eax, 16(%rsp)
	sall	$2, %eax
	fildl	16(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L41:
	cmpl	$1078034431, %ebx
	fstpt	16(%rsp)
	jg	.L48
	pushq	8(%rsp)
	pushq	8(%rsp)
	call	__ieee754_logl@PLT
	fld1
	fldt	16(%rsp)
	fdivr	%st(1), %st
	fld	%st(0)
	fmul	%st(1), %st
	fldt	.LC69(%rip)
	fmul	%st(1), %st
	fldt	.LC70(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC71(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC72(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC73(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC74(%rip)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC75(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	.LC76(%rip)
	faddp	%st, %st(1)
	fldt	16(%rsp)
	popq	%rcx
	popq	%rsi
	fsubs	.LC5(%rip)
	fxch	%st(3)
	fsubp	%st, %st(2)
	fxch	%st(1)
	fmulp	%st, %st(2)
	faddp	%st, %st(1)
	fldt	16(%rsp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$-1, 0(%r13)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L73:
	fstpt	16(%rsp)
	pushq	8(%rsp)
	pushq	8(%rsp)
	call	__ieee754_logl@PLT
	popq	%r9
	popq	%r10
	fchs
	cmpl	$1073658697, %ebx
	fldt	16(%rsp)
	jle	.L34
	fld1
	fldt	(%rsp)
	fsubp	%st, %st(1)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L23:
	fld1
	fsubp	%st, %st(1)
.L67:
	fldpi
	subq	$16, %rsp
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__sinl@PLT
	popq	%rax
	fldz
	popq	%rdx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	fsubrs	.LC5(%rip)
	subq	$16, %rsp
	fldpi
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__cosl@PLT
	popq	%rdi
	fldz
	popq	%r8
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L20:
	fldpi
	fmulp	%st, %st(1)
	fldz
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L24:
	fsubs	.LC8(%rip)
	subq	$16, %rsp
	fldpi
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__cosl@PLT
	fchs
	popq	%rcx
	fldz
	popq	%rsi
	jmp	.L25
.L55:
	fld1
.L46:
	fld	%st(2)
	fadds	.LC67(%rip)
	fmulp	%st, %st(1)
.L45:
	fld	%st(2)
	fadds	.LC6(%rip)
	fmulp	%st, %st(1)
.L44:
	fld	%st(2)
	fadds	.LC68(%rip)
	fmulp	%st, %st(1)
	fxch	%st(1)
.L42:
	fstpt	16(%rsp)
	fxch	%st(2)
	subq	$16, %rsp
	fstpt	16(%rsp)
	fadds	.LC9(%rip)
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	fldt	32(%rsp)
	popq	%rdi
	popq	%r8
	faddp	%st, %st(1)
	fldt	(%rsp)
	jmp	.L40
.L54:
	fld1
	jmp	.L45
.L53:
	fld1
	jmp	.L44
.L52:
	fld1
	fxch	%st(1)
	jmp	.L42
.L47:
	fld	%st(1)
	fadds	.LC66(%rip)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L75:
	fldt	(%rsp)
	subq	$16, %rsp
	movl	$-1, (%rdi)
	fchs
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	popq	%rcx
	fchs
	popq	%rsi
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	cmpl	$1073716645, %ebx
	jle	.L39
	fldt	.LC11(%rip)
	fldt	(%rsp)
	fsubp	%st, %st(1)
	fldz
.L37:
	fldt	.LC23(%rip)
	fmul	%st(2), %st
	fldt	.LC24(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC25(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC26(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC27(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC28(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC29(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC30(%rip)
	fadd	%st(3), %st
	fmul	%st(3), %st
	fldt	.LC31(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC32(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC33(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC34(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(3)
	fldt	.LC35(%rip)
	faddp	%st, %st(3)
	fdivp	%st, %st(2)
	fldt	.LC36(%rip)
	faddp	%st, %st(2)
	fxch	%st(1)
	fsubl	.LC37(%rip)
	faddp	%st, %st(1)
	fxch	%st(1)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L19:
	fsubs	.LC9(%rip)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L48:
	pushq	8(%rsp)
	pushq	8(%rsp)
	call	__ieee754_logl@PLT
	fld1
	fsubrp	%st, %st(1)
	fldt	16(%rsp)
	popq	%rax
	popq	%rdx
	fmulp	%st, %st(1)
	fldt	16(%rsp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L39:
	fld1
	fldt	(%rsp)
	fsubp	%st, %st(1)
	fstpt	(%rsp)
	fldz
	jmp	.L36
.L86:
	fxch	%st(1)
.L36:
	fldt	.LC38(%rip)
	fldt	(%rsp)
	fmul	%st, %st(1)
	fldt	.LC39(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC40(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC41(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC42(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC43(%rip)
	faddp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC44(%rip)
	fsubrp	%st, %st(2)
	fmul	%st, %st(1)
	fldt	.LC45(%rip)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	.LC46(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC47(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC48(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC49(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC50(%rip)
	faddp	%st, %st(1)
	fdivrp	%st, %st(2)
	fmuls	.LC51(%rip)
	faddp	%st, %st(1)
	faddp	%st, %st(1)
	fxch	%st(1)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L34:
	cmpl	$1073540402, %ebx
	jle	.L86
	fldt	.LC10(%rip)
	fldt	(%rsp)
	fsubp	%st, %st(1)
	fxch	%st(1)
	fxch	%st(2)
	jmp	.L37
	.size	__ieee754_lgammal_r, .-__ieee754_lgammal_r
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	3221225472
	.align 4
.LC3:
	.long	3255042048
	.align 4
.LC5:
	.long	1056964608
	.align 4
.LC6:
	.long	1082130432
	.align 4
.LC7:
	.long	1593835520
	.align 4
.LC8:
	.long	1069547520
	.align 4
.LC9:
	.long	1073741824
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC10:
	.long	3620203972
	.long	3965389930
	.long	16381
	.long	0
	.align 16
.LC11:
	.long	3052534641
	.long	3138831130
	.long	16383
	.long	0
	.align 16
.LC12:
	.long	988437164
	.long	4044738785
	.long	16384
	.long	0
	.align 16
.LC13:
	.long	640264184
	.long	3809640535
	.long	16389
	.long	0
	.align 16
.LC14:
	.long	1262200120
	.long	3692744428
	.long	16392
	.long	0
	.align 16
.LC15:
	.long	1436462129
	.long	2521545417
	.long	16394
	.long	0
	.align 16
.LC16:
	.long	2802204127
	.long	3893489017
	.long	16393
	.long	0
	.align 16
.LC17:
	.long	575062696
	.long	2660550448
	.long	16392
	.long	0
	.align 16
.LC18:
	.long	1214363083
	.long	4055018143
	.long	16388
	.long	0
	.align 16
.LC19:
	.long	1316542656
	.long	3568084256
	.long	16392
	.long	0
	.align 16
.LC20:
	.long	710255133
	.long	2387260199
	.long	16395
	.long	0
	.align 16
.LC21:
	.long	3509478411
	.long	2690497927
	.long	16396
	.long	0
	.align 16
.LC22:
	.long	1170695206
	.long	2153506069
	.long	16396
	.long	0
	.align 16
.LC23:
	.long	91143063
	.long	2327602913
	.long	16385
	.long	0
	.align 16
.LC24:
	.long	2762413116
	.long	2586602877
	.long	16390
	.long	0
	.align 16
.LC25:
	.long	728430093
	.long	3052252490
	.long	16393
	.long	0
	.align 16
.LC26:
	.long	1732528707
	.long	2864807373
	.long	16395
	.long	0
	.align 16
.LC27:
	.long	3388019525
	.long	2314365455
	.long	16396
	.long	0
	.align 16
.LC28:
	.long	1544689541
	.long	2687843509
	.long	16395
	.long	0
	.align 16
.LC29:
	.long	691275616
	.long	2256473724
	.long	16325
	.long	0
	.align 16
.LC30:
	.long	2584299888
	.long	2161739396
	.long	16389
	.long	0
	.align 16
.LC31:
	.long	3498846122
	.long	4095421092
	.long	16392
	.long	0
	.align 16
.LC32:
	.long	3489930676
	.long	3077057803
	.long	16395
	.long	0
	.align 16
.LC33:
	.long	1686889428
	.long	2153532319
	.long	16397
	.long	0
	.align 16
.LC34:
	.long	2845283441
	.long	2815323869
	.long	16397
	.long	0
	.align 16
.LC35:
	.long	761483951
	.long	2777638319
	.long	16396
	.long	0
	.align 16
.LC36:
	.long	2906000072
	.long	4165657670
	.long	16324
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC37:
	.long	3166931522
	.long	1069488569
	.section	.rodata.cst16
	.align 16
.LC38:
	.long	1885869682
	.long	2407543319
	.long	16385
	.long	0
	.align 16
.LC39:
	.long	3679939566
	.long	3799222353
	.long	16389
	.long	0
	.align 16
.LC40:
	.long	1712203499
	.long	3123525567
	.long	16392
	.long	0
	.align 16
.LC41:
	.long	2373385484
	.long	4009175607
	.long	16393
	.long	0
	.align 16
.LC42:
	.long	1434566442
	.long	4283697420
	.long	16393
	.long	0
	.align 16
.LC43:
	.long	733203441
	.long	2868950064
	.long	16392
	.long	0
	.align 16
.LC44:
	.long	1891935259
	.long	2981719808
	.long	16389
	.long	0
	.align 16
.LC45:
	.long	3534389098
	.long	3025931731
	.long	16388
	.long	0
	.align 16
.LC46:
	.long	2733795235
	.long	3977340261
	.long	16391
	.long	0
	.align 16
.LC47:
	.long	1542270496
	.long	4123944808
	.long	16393
	.long	0
	.align 16
.LC48:
	.long	1622834308
	.long	3970571127
	.long	16394
	.long	0
	.align 16
.LC49:
	.long	479903339
	.long	3564835712
	.long	16394
	.long	0
	.align 16
.LC50:
	.long	1444240185
	.long	2413467374
	.long	16393
	.long	0
	.section	.rodata.cst4
	.align 4
.LC51:
	.long	3204448256
	.section	.rodata.cst16
	.align 16
.LC52:
	.long	1576666607
	.long	3533547023
	.long	49161
	.long	0
	.align 16
.LC53:
	.long	115871971
	.long	4104799287
	.long	16398
	.long	0
	.align 16
.LC54:
	.long	2420028132
	.long	2906067586
	.long	16402
	.long	0
	.align 16
.LC55:
	.long	2751723198
	.long	3398713222
	.long	16404
	.long	0
	.align 16
.LC56:
	.long	2075326266
	.long	3365667173
	.long	16405
	.long	0
	.align 16
.LC57:
	.long	1925952717
	.long	3995062671
	.long	16404
	.long	0
	.align 16
.LC58:
	.long	1997312948
	.long	2979279387
	.long	16403
	.long	0
	.align 16
.LC59:
	.long	4104225037
	.long	3776547674
	.long	16391
	.long	0
	.align 16
.LC60:
	.long	1116499500
	.long	3968512366
	.long	16397
	.long	0
	.align 16
.LC61:
	.long	2814504848
	.long	2278935626
	.long	16402
	.long	0
	.align 16
.LC62:
	.long	2419046535
	.long	2206989466
	.long	16405
	.long	0
	.align 16
.LC63:
	.long	3628838062
	.long	4096628467
	.long	16406
	.long	0
	.align 16
.LC64:
	.long	4292559722
	.long	3603463785
	.long	16407
	.long	0
	.align 16
.LC65:
	.long	3177882823
	.long	2411492045
	.long	16407
	.long	0
	.section	.rodata.cst4
	.align 4
.LC66:
	.long	1086324736
	.align 4
.LC67:
	.long	1084227584
	.align 4
.LC68:
	.long	1077936128
	.section	.rodata.cst16
	.align 16
.LC69:
	.long	3423132036
	.long	2685571522
	.long	16375
	.long	0
	.align 16
.LC70:
	.long	50609553
	.long	4135927201
	.long	16373
	.long	0
	.align 16
.LC71:
	.long	2865839068
	.long	3699954834
	.long	16372
	.long	0
	.align 16
.LC72:
	.long	2681113376
	.long	2617869390
	.long	16372
	.long	0
	.align 16
.LC73:
	.long	820377842
	.long	3490513042
	.long	16372
	.long	0
	.align 16
.LC74:
	.long	61361544
	.long	3054198966
	.long	16374
	.long	0
	.align 16
.LC75:
	.long	2863308748
	.long	2863311530
	.long	16379
	.long	0
	.align 16
.LC76:
	.long	1273711209
	.long	3598654598
	.long	16381
	.long	0
	.section	.rodata.cst4
	.align 4
.LC77:
	.long	2143289344
