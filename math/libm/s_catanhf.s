	.text
	.p2align 4,,15
	.globl	__catanhf
	.type	__catanhf, @function
__catanhf:
	subq	$104, %rsp
	movq	%xmm0, 88(%rsp)
	movss	.LC2(%rip), %xmm7
	movss	88(%rsp), %xmm9
	movaps	%xmm9, %xmm6
	movss	92(%rsp), %xmm8
	andps	%xmm7, %xmm6
	ucomiss	%xmm6, %xmm6
	jp	.L48
	ucomiss	.LC3(%rip), %xmm6
	jbe	.L86
	movl	$1, %eax
.L3:
	movaps	%xmm8, %xmm1
	andps	%xmm7, %xmm1
	ucomiss	%xmm1, %xmm1
	jp	.L87
.L46:
	ucomiss	.LC3(%rip), %xmm1
	ja	.L45
	ucomiss	.LC4(%rip), %xmm1
	jnb	.L6
	ucomiss	.LC0(%rip), %xmm8
	jp	.L6
	je	.L7
.L6:
	cmpl	$1, %eax
	jle	.L9
.L10:
	ucomiss	.LC7(%rip), %xmm6
	jnb	.L13
	ucomiss	.LC7(%rip), %xmm1
	jnb	.L13
	movss	.LC8(%rip), %xmm3
	ucomiss	%xmm3, %xmm6
	jp	.L21
	je	.L88
.L21:
	ucomiss	.LC11(%rip), %xmm1
	pxor	%xmm0, %xmm0
	jb	.L26
	movaps	%xmm8, %xmm0
	mulss	%xmm8, %xmm0
.L26:
	movaps	%xmm9, %xmm2
	movaps	%xmm3, %xmm4
	movss	.LC9(%rip), %xmm5
	addss	%xmm3, %xmm2
	movss	%xmm5, 16(%rsp)
	subss	%xmm9, %xmm4
	movss	%xmm3, 76(%rsp)
	movaps	%xmm7, 48(%rsp)
	mulss	%xmm2, %xmm2
	movss	%xmm8, 32(%rsp)
	mulss	%xmm4, %xmm4
	movss	%xmm1, (%rsp)
	movss	%xmm6, 72(%rsp)
	addss	%xmm0, %xmm4
	addss	%xmm2, %xmm0
	divss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm5
	jbe	.L76
	call	__ieee754_logf@PLT
	movss	.LC10(%rip), %xmm2
	mulss	%xmm0, %xmm2
	movaps	48(%rsp), %xmm7
	movss	72(%rsp), %xmm6
	movss	(%rsp), %xmm1
	movss	32(%rsp), %xmm8
	movss	76(%rsp), %xmm3
.L30:
	ucomiss	%xmm6, %xmm1
	jbe	.L31
	movaps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	movaps	%xmm0, %xmm1
.L31:
	movss	.LC15(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.L77
	movaps	%xmm3, %xmm1
	pxor	%xmm5, %xmm5
	subss	%xmm6, %xmm1
	addss	%xmm3, %xmm6
	mulss	%xmm6, %xmm1
	ucomiss	%xmm5, %xmm1
	jp	.L35
	movd	%xmm1, %eax
	movd	%xmm5, %edx
	cmove	%edx, %eax
	movl	%eax, 72(%rsp)
	movss	72(%rsp), %xmm1
.L35:
	addss	%xmm8, %xmm8
	movss	%xmm2, 72(%rsp)
	movaps	%xmm7, (%rsp)
	movaps	%xmm8, %xmm0
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm3
	mulss	%xmm0, %xmm3
	movss	72(%rsp), %xmm2
	movaps	%xmm2, %xmm4
	movaps	(%rsp), %xmm7
	movaps	%xmm3, %xmm5
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L86:
	ucomiss	.LC4(%rip), %xmm6
	jnb	.L50
	ucomiss	.LC0(%rip), %xmm9
	jp	.L56
	jne	.L56
	movl	$2, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movaps	%xmm8, %xmm3
	movss	.LC8(%rip), %xmm2
	andps	.LC5(%rip), %xmm3
	ucomiss	%xmm1, %xmm2
	orps	.LC6(%rip), %xmm3
	jnb	.L89
	ucomiss	%xmm6, %xmm2
	jb	.L73
	movaps	%xmm9, %xmm2
	movaps	%xmm3, %xmm5
	divss	%xmm8, %xmm2
	divss	%xmm8, %xmm2
	movaps	%xmm2, %xmm4
.L18:
	movaps	%xmm2, %xmm0
	movss	.LC4(%rip), %xmm1
	andps	%xmm7, %xmm0
	ucomiss	%xmm0, %xmm1
	jbe	.L42
	mulss	%xmm2, %xmm2
.L42:
	andps	%xmm3, %xmm7
	ucomiss	%xmm7, %xmm1
	jbe	.L1
	mulss	%xmm3, %xmm3
.L1:
	movss	%xmm4, 80(%rsp)
	movss	%xmm5, 84(%rsp)
	movq	80(%rsp), %xmm0
	addq	$104, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movss	.LC5(%rip), %xmm0
	movaps	%xmm8, %xmm5
	movaps	%xmm9, %xmm4
	andps	%xmm0, %xmm5
	andps	%xmm0, %xmm4
	orps	.LC6(%rip), %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$4, %eax
.L2:
	movaps	%xmm8, %xmm1
	andps	%xmm7, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.L46
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L89:
	divss	%xmm9, %xmm2
	movaps	%xmm3, %xmm5
	movaps	%xmm2, %xmm4
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$1, %eax
	jle	.L9
	cmpl	$2, %eax
	movaps	%xmm9, %xmm4
	movaps	%xmm8, %xmm5
	je	.L1
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$3, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L88:
	movss	.LC11(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.L21
	andps	.LC5(%rip), %xmm9
	movaps	%xmm1, %xmm0
	movss	%xmm3, 76(%rsp)
	movaps	%xmm7, 48(%rsp)
	movss	%xmm8, 32(%rsp)
	movss	%xmm6, 16(%rsp)
	movss	%xmm1, (%rsp)
	orps	.LC12(%rip), %xmm9
	movss	%xmm9, 72(%rsp)
	call	__ieee754_logf@PLT
	movss	.LC13(%rip), %xmm2
	movss	(%rsp), %xmm1
	subss	%xmm0, %xmm2
	movss	76(%rsp), %xmm3
	ucomiss	%xmm3, %xmm1
	movss	16(%rsp), %xmm6
	mulss	72(%rsp), %xmm2
	movss	32(%rsp), %xmm8
	movaps	48(%rsp), %xmm7
	jbe	.L74
	movaps	%xmm6, %xmm0
	movaps	%xmm1, %xmm6
	movaps	%xmm0, %xmm1
.L82:
	movss	.LC9(%rip), %xmm5
	movss	%xmm5, 16(%rsp)
.L79:
	movaps	%xmm3, %xmm0
	mulss	%xmm1, %xmm1
	subss	%xmm6, %xmm0
	addss	%xmm3, %xmm6
	mulss	%xmm0, %xmm6
	subss	%xmm1, %xmm6
	movaps	%xmm6, %xmm1
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L73:
	movaps	%xmm9, %xmm0
	movss	%xmm3, (%rsp)
	movss	.LC9(%rip), %xmm1
	mulss	%xmm1, %xmm0
	movss	%xmm9, 72(%rsp)
	mulss	%xmm8, %xmm1
	movaps	%xmm7, 16(%rsp)
	call	__ieee754_hypotf@PLT
	movss	72(%rsp), %xmm9
	movaps	%xmm9, %xmm2
	movss	(%rsp), %xmm3
	movaps	%xmm3, %xmm5
	divss	%xmm0, %xmm2
	movaps	16(%rsp), %xmm7
	divss	%xmm0, %xmm2
	mulss	.LC10(%rip), %xmm2
	movaps	%xmm2, %xmm4
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%eax, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L76:
	movss	.LC14(%rip), %xmm0
	mulss	%xmm9, %xmm0
	divss	%xmm4, %xmm0
	call	__log1pf@PLT
	movss	.LC10(%rip), %xmm2
	mulss	%xmm0, %xmm2
	movss	76(%rsp), %xmm3
	movaps	48(%rsp), %xmm7
	movss	32(%rsp), %xmm8
	movss	(%rsp), %xmm1
	movss	72(%rsp), %xmm6
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L77:
	ucomiss	%xmm3, %xmm6
	jnb	.L79
	ucomiss	.LC16(%rip), %xmm6
	jnb	.L39
	ucomiss	.LC9(%rip), %xmm1
	jb	.L79
.L39:
	movaps	%xmm6, %xmm0
	movss	%xmm8, (%rsp)
	movaps	%xmm7, 32(%rsp)
	movss	%xmm2, 72(%rsp)
	call	__x2y2m1f@PLT
	movaps	%xmm0, %xmm1
	movss	72(%rsp), %xmm2
	xorps	.LC5(%rip), %xmm1
	movss	(%rsp), %xmm8
	movaps	32(%rsp), %xmm7
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%eax, %eax
	jne	.L45
.L84:
	movss	.LC1(%rip), %xmm4
	movaps	%xmm4, %xmm5
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L74:
	movss	.LC15(%rip), %xmm0
	ucomiss	%xmm1, %xmm0
	jbe	.L82
	movss	.LC9(%rip), %xmm5
	pxor	%xmm1, %xmm1
	movss	%xmm5, 16(%rsp)
	jmp	.L35
.L87:
	movaps	%xmm9, %xmm4
	movss	.LC1(%rip), %xmm5
	andps	.LC5(%rip), %xmm4
	jmp	.L1
	.size	__catanhf, .-__catanhf
	.weak	catanhf32
	.set	catanhf32,__catanhf
	.weak	catanhf
	.set	catanhf,__catanhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095039
	.align 4
.LC4:
	.long	8388608
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 16
.LC6:
	.long	1070141403
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1291845632
	.align 4
.LC8:
	.long	1065353216
	.align 4
.LC9:
	.long	1056964608
	.align 4
.LC10:
	.long	1048576000
	.align 4
.LC11:
	.long	679477248
	.section	.rodata.cst16
	.align 16
.LC12:
	.long	1056964608
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC13:
	.long	1060205080
	.align 4
.LC14:
	.long	1082130432
	.align 4
.LC15:
	.long	864026624
	.align 4
.LC16:
	.long	1061158912
