	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__ctan
	.type	__ctan, @function
__ctan:
	pushq	%rbx
	movapd	%xmm0, %xmm6
	movapd	%xmm1, %xmm5
	subq	$64, %rsp
	movq	.LC2(%rip), %xmm4
	movsd	.LC3(%rip), %xmm2
	andpd	%xmm4, %xmm6
	andpd	%xmm4, %xmm5
	ucomisd	%xmm6, %xmm2
	jb	.L2
	ucomisd	%xmm5, %xmm2
	jb	.L53
	movsd	.LC8(%rip), %xmm2
	movapd	%xmm1, %xmm3
	ucomisd	.LC10(%rip), %xmm6
	mulsd	.LC7(%rip), %xmm2
	mulsd	.LC9(%rip), %xmm2
	cvttsd2si	%xmm2, %ebx
	jbe	.L47
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdi
	movsd	%xmm1, 16(%rsp)
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm5, 8(%rsp)
	movsd	%xmm1, (%rsp)
	call	__sincos@PLT
	movsd	(%rsp), %xmm3
	movsd	8(%rsp), %xmm5
	movsd	16(%rsp), %xmm1
	movapd	32(%rsp), %xmm4
.L17:
	pxor	%xmm6, %xmm6
	cvtsi2sd	%ebx, %xmm6
	ucomisd	%xmm6, %xmm5
	ja	.L54
	ucomisd	.LC10(%rip), %xmm5
	movsd	.LC1(%rip), %xmm0
	ja	.L55
.L23:
	movsd	56(%rsp), %xmm2
	movapd	%xmm2, %xmm7
	movapd	%xmm2, %xmm6
	andpd	%xmm4, %xmm7
	mulsd	%xmm2, %xmm6
	mulsd	.LC12(%rip), %xmm7
	ucomisd	%xmm7, %xmm5
	ja	.L56
.L25:
	mulsd	48(%rsp), %xmm2
	mulsd	%xmm0, %xmm3
	divsd	%xmm6, %xmm3
	divsd	%xmm6, %xmm2
	movapd	%xmm3, %xmm1
	movapd	%xmm2, %xmm0
.L22:
	movapd	%xmm2, %xmm5
	movsd	.LC10(%rip), %xmm7
	andpd	%xmm4, %xmm5
	ucomisd	%xmm5, %xmm7
	jbe	.L27
	mulsd	%xmm2, %xmm2
.L27:
	andpd	%xmm3, %xmm4
	movsd	.LC10(%rip), %xmm7
	ucomisd	%xmm4, %xmm7
	jbe	.L1
	mulsd	%xmm3, %xmm3
.L1:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movapd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm6
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm0, %xmm0
	addl	%ebx, %ebx
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm1, 16(%rsp)
	movsd	%xmm6, 8(%rsp)
	cvtsi2sd	%ebx, %xmm0
	movsd	%xmm5, (%rsp)
	call	__ieee754_exp@PLT
	movsd	.LC11(%rip), %xmm2
	movsd	8(%rsp), %xmm6
	mulsd	48(%rsp), %xmm2
	movsd	(%rsp), %xmm5
	movsd	16(%rsp), %xmm1
	subsd	%xmm6, %xmm5
	movapd	32(%rsp), %xmm4
	movapd	%xmm1, %xmm3
	mulsd	56(%rsp), %xmm2
	andpd	.LC5(%rip), %xmm3
	ucomisd	%xmm6, %xmm5
	orpd	.LC6(%rip), %xmm3
	divsd	%xmm0, %xmm2
	jbe	.L49
	movapd	%xmm3, %xmm1
	divsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L55:
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 8(%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__ieee754_sinh@PLT
	movsd	%xmm0, (%rsp)
	movsd	8(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	call	__ieee754_cosh@PLT
	movsd	(%rsp), %xmm3
	movapd	16(%rsp), %xmm4
	movapd	%xmm3, %xmm5
	andpd	%xmm4, %xmm5
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L49:
	addsd	%xmm5, %xmm5
	movsd	%xmm2, 8(%rsp)
	movaps	%xmm4, 16(%rsp)
	movsd	%xmm3, (%rsp)
	movapd	%xmm5, %xmm0
	call	__ieee754_exp@PLT
	movsd	8(%rsp), %xmm2
	movsd	(%rsp), %xmm3
	divsd	%xmm0, %xmm2
	movapd	16(%rsp), %xmm4
	movapd	%xmm3, %xmm1
	movapd	%xmm2, %xmm0
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm2, %xmm5
	ja	.L10
.L31:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm0
	jp	.L9
	je	.L1
.L9:
	ucomisd	%xmm3, %xmm1
	movsd	.LC0(%rip), %xmm0
	jp	.L33
	jne	.L33
.L13:
	ucomisd	%xmm2, %xmm6
	jbe	.L1
	movl	$1, %edi
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__GI_feraiseexcept
	movsd	(%rsp), %xmm0
	movsd	8(%rsp), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	ucomisd	%xmm2, %xmm5
	jbe	.L31
	ucomisd	.LC1(%rip), %xmm6
	jbe	.L10
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdi
	movsd	%xmm1, (%rsp)
	call	__sincos@PLT
	movsd	48(%rsp), %xmm0
	movq	.LC5(%rip), %xmm2
	mulsd	56(%rsp), %xmm0
	movsd	(%rsp), %xmm1
	andpd	%xmm2, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L47:
	movq	.LC1(%rip), %rax
	movsd	%xmm0, 48(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L10:
	movq	.LC5(%rip), %xmm2
	andpd	%xmm2, %xmm0
.L12:
	andpd	%xmm2, %xmm1
	orpd	.LC6(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movapd	%xmm0, %xmm1
	jmp	.L13
	.size	__ctan, .-__ctan
	.weak	ctanf32x
	.set	ctanf32x,__ctan
	.weak	ctanf64
	.set	ctanf64,__ctan
	.weak	ctan
	.set	ctan,__ctan
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.align 16
.LC6:
	.long	0
	.long	1072693248
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1083176960
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.align 8
.LC10:
	.long	0
	.long	1048576
	.align 8
.LC11:
	.long	0
	.long	1074790400
	.align 8
.LC12:
	.long	0
	.long	1018167296
