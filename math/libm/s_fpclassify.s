	.text
	.p2align 4,,15
	.globl	__fpclassify
	.type	__fpclassify, @function
__fpclassify:
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$32, %rdx
	movl	%edx, %ecx
	andl	$2146435072, %edx
	andl	$1048575, %ecx
	orl	%eax, %ecx
	movl	$2, %eax
	movl	%ecx, %esi
	orl	%edx, %esi
	je	.L1
	testl	%edx, %edx
	movl	$3, %eax
	je	.L1
	cmpl	$2146435072, %edx
	movl	$4, %eax
	jne	.L1
	xorl	%eax, %eax
	testl	%ecx, %ecx
	sete	%al
.L1:
	rep ret
	.size	__fpclassify, .-__fpclassify
