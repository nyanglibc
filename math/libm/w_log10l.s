	.text
	.p2align 4,,15
	.globl	__log10l
	.type	__log10l, @function
__log10l:
	fldt	8(%rsp)
	fldz
	fucomi	%st(1), %st
	jnb	.L7
	fstp	%st(0)
.L2:
	fstpt	8(%rsp)
	jmp	__ieee754_log10l@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__log10l, .-__log10l
	.weak	log10f64x
	.set	log10f64x,__log10l
	.weak	log10l
	.set	log10l,__log10l
