	.text
	.globl	__letf2
	.globl	__getf2
	.p2align 4,,15
	.globl	__iseqsigf128
	.type	__iseqsigf128, @function
__iseqsigf128:
	pushq	%rbx
	subq	$32, %rsp
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__letf2@PLT
	testq	%rax, %rax
	setle	%bl
	movdqa	16(%rsp), %xmm3
	movdqa	(%rsp), %xmm2
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	setns	%dl
	testb	%bl, %bl
	je	.L9
	testb	%dl, %dl
	movl	$1, %eax
	je	.L7
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	testb	%dl, %dl
	je	.L12
.L7:
	addq	$32, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %edi
	call	__GI___feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	addq	$32, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__iseqsigf128, .-__iseqsigf128
