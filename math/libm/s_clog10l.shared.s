	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__clog10l
	.type	__clog10l, @function
__clog10l:
	pushq	%rbx
	subq	$32, %rsp
	fldt	48(%rsp)
	fabs
	fucomi	%st(0), %st
	jp	.L2
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L93
	fstp	%st(1)
	fldt	64(%rsp)
	fabs
	fucomi	%st(0), %st
	jp	.L101
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L106:
	fstp	%st(0)
	fstp	%st(1)
	fxch	%st(1)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L107:
	fstp	%st(2)
	.p2align 4,,10
	.p2align 3
.L8:
	fucomi	%st(1), %st
	ja	.L16
	fxch	%st(1)
.L16:
	fldt	.LC7(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L94
	fldt	.LC5(%rip)
	fucomi	%st(1), %st
	jbe	.L102
	fucomip	%st(2), %st
	ja	.L95
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L102:
	fstp	%st(0)
.L21:
	fld1
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L49
	jne	.L49
	fstp	%st(0)
	fmul	%st(0), %st
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	.LC10(%rip)
	popq	%rax
	popq	%rdx
	fmulp	%st, %st(1)
	fldt	.LC5(%rip)
	fucomip	%st(1), %st
	jbe	.L25
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L25:
	fldt	64(%rsp)
	fldt	48(%rsp)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	fldt	.LC16(%rip)
	fmulp	%st, %st(1)
.L1:
	addq	$32, %rsp
	popq	%rbx
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jnb	.L4
	fldz
	fldt	48(%rsp)
	fldt	64(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(2), %st
	jp	.L103
	jne	.L104
	fucomi	%st(0), %st
	jp	.L105
	fucomi	%st(4), %st
	fstp	%st(4)
	ja	.L106
	fxch	%st(3)
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L107
	fldt	64(%rsp)
	fucomip	%st(3), %st
	fstp	%st(2)
	jp	.L8
	jne	.L8
	fstp	%st(0)
	fldt	48(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	jne	.L96
	fldz
.L14:
	fldt	64(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fabs
	je	.L15
	fchs
.L15:
	fld1
	addq	$32, %rsp
	popq	%rbx
	fchs
	fdivp	%st, %st(2)
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	fstp	%st(1)
	fstp	%st(1)
	fldt	64(%rsp)
	fabs
	jmp	.L91
.L103:
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(2)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L104:
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(2)
.L91:
	fucomi	%st(0), %st
	jnp	.L8
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L105:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L86
.L116:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L86:
	flds	.LC3(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L94:
	fxch	%st(1)
	fstpt	(%rsp)
	subq	$16, %rsp
	movl	$-1, %edi
	fstpt	(%rsp)
	call	__scalbnl@PLT
	popq	%rdi
	fldt	.LC8(%rip)
	popq	%r8
	fldt	(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L97
	fstp	%st(0)
	fldz
	fxch	%st(1)
	movl	$-1, %ebx
.L19:
	fld1
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L108
	flds	.LC11(%rip)
	fucomip	%st(1), %st
	jbe	.L109
	fxch	%st(1)
	fucomi	%st(2), %st
	jbe	.L110
	testl	%ebx, %ebx
	je	.L98
	fstp	%st(0)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L108:
	fstp	%st(1)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L109:
	fstp	%st(1)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L110:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L27:
	fld1
	fucomi	%st(1), %st
	ja	.L99
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L113:
	fstp	%st(1)
	fxch	%st(1)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L114:
	fstp	%st(1)
	fxch	%st(1)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L115:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L32:
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__ieee754_hypotl@PLT
	popq	%rax
	popq	%rdx
	fstpt	(%rsp)
	call	__ieee754_log10l@PLT
	movl	%ebx, 16(%rsp)
	fildl	16(%rsp)
	popq	%rcx
	popq	%rsi
	fldlg2
	fmulp	%st, %st(1)
	fsubrp	%st, %st(1)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L95:
	fxch	%st(1)
	fstpt	16(%rsp)
	subq	$16, %rsp
	movl	$64, %edi
	movl	$64, %ebx
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fstpt	16(%rsp)
	subq	$16, %rsp
	movl	$64, %edi
	fldt	48(%rsp)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	addq	$32, %rsp
	fldt	(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L97:
	fxch	%st(1)
	fstpt	(%rsp)
	subq	$16, %rsp
	movl	$-1, %edi
	movl	$-1, %ebx
	fstpt	(%rsp)
	call	__scalbnl@PLT
	popq	%rcx
	popq	%rsi
	fldt	(%rsp)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%ebx, %ebx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L99:
	flds	.LC13(%rip)
	fxch	%st(2)
	testl	%ebx, %ebx
	sete	%al
	fucomi	%st(2), %st
	fstp	%st(2)
	jb	.L111
	flds	.LC14(%rip)
	fucomip	%st(3), %st
	jbe	.L112
	testb	%al, %al
	jne	.L100
	fstp	%st(0)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L111:
	fstp	%st(0)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L112:
	fstp	%st(0)
.L34:
	flds	.LC13(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jb	.L113
	testb	%al, %al
	je	.L114
	fld	%st(2)
	fmul	%st(3), %st
	fld	%st(1)
	fmul	%st(2), %st
	faddp	%st, %st(1)
	fucomip	%st(2), %st
	fstp	%st(1)
	jb	.L115
	fxch	%st(1)
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__x2y2m1l@PLT
	popq	%rdi
	popq	%r8
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	.LC10(%rip)
	popq	%r9
	popq	%r10
	fmulp	%st, %st(1)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fldt	64(%rsp)
	fabs
	fucomi	%st(0), %st
	jp	.L116
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L86
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L101:
	fstp	%st(0)
	fstp	%st(0)
.L44:
	flds	.LC2(%rip)
	flds	.LC3(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L100:
	fstp	%st(2)
	fxch	%st(1)
	fld	%st(1)
	fsub	%st(1), %st
	fxch	%st(2)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L117:
	fstp	%st(1)
.L90:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__log1pl@PLT
	fldt	.LC10(%rip)
	popq	%r11
	popq	%rbx
	fmulp	%st, %st(1)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L96:
	fldt	.LC0(%rip)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L98:
	fld	%st(1)
	fsub	%st(1), %st
	fxch	%st(2)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	flds	.LC12(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jb	.L117
	fxch	%st(1)
	fmul	%st(0), %st
	faddp	%st, %st(1)
	jmp	.L90
	.size	__clog10l, .-__clog10l
	.weak	clog10f64x
	.set	clog10f64x,__clog10l
	.weak	clog10l
	.set	clog10l,__clog10l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2547844487
	.long	2929975909
	.long	16383
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	2143289344
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC5:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC7:
	.long	4294967295
	.long	4294967295
	.long	32765
	.long	0
	.align 16
.LC8:
	.long	0
	.long	2147483648
	.long	2
	.long	0
	.align 16
.LC10:
	.long	925397397
	.long	3730561193
	.long	16380
	.long	0
	.section	.rodata.cst4
	.align 4
.LC11:
	.long	1073741824
	.align 4
.LC12:
	.long	536870912
	.align 4
.LC13:
	.long	1056964608
	.align 4
.LC14:
	.long	528482304
	.section	.rodata.cst16
	.align 16
.LC16:
	.long	925397397
	.long	3730561193
	.long	16381
	.long	0
