	.text
	.p2align 4,,15
	.globl	__nan
	.type	__nan, @function
__nan:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__strtod_nan@PLT
	.size	__nan, .-__nan
	.weak	nanf32x
	.set	nanf32x,__nan
	.weak	nanf64
	.set	nanf64,__nan
	.weak	nan
	.set	nan,__nan
