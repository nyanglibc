	.text
	.p2align 4,,15
	.globl	fesetmode
	.type	fesetmode, @function
fesetmode:
#APP
# 31 "../sysdeps/x86_64/fpu/fesetmode.c" 1
	stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-4(%rsp), %eax
	andl	$63, %eax
	cmpq	$-1, %rdi
	je	.L5
	movzwl	(%rdi), %edx
	movw	%dx, -6(%rsp)
	movl	4(%rdi), %edx
	andl	$-64, %edx
	orl	%edx, %eax
	movl	%eax, -4(%rsp)
.L3:
#APP
# 47 "../sysdeps/x86_64/fpu/fesetmode.c" 1
	fldcw -6(%rsp)
# 0 "" 2
# 48 "../sysdeps/x86_64/fpu/fesetmode.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$895, %edx
	orl	$8064, %eax
	movw	%dx, -6(%rsp)
	movl	%eax, -4(%rsp)
	jmp	.L3
	.size	fesetmode, .-fesetmode
