	.text
	.globl	__lttf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__kernel_sincosf128
	.type	__kernel_sincosf128, @function
__kernel_sincosf128:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$88, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %r14
	movq	%r14, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1073491967, %eax
	ja	.L2
	cmpl	$1069940735, %eax
	ja	.L3
	pand	.LC0(%rip), %xmm0
	movdqa	.LC1(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L4
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L4:
	movdqa	(%rsp), %xmm0
	call	__fixtfsi@PLT
	testl	%eax, %eax
	je	.L22
.L3:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC3(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, (%r12)
	movdqa	.LC11(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, 0(%rbp)
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%eax, %esi
	movl	$16382, %ecx
	shrl	$16, %esi
	movaps	(%rsp), %xmm6
	subl	%esi, %ecx
	movl	$512, %esi
	sall	%cl, %esi
	movdqa	%xmm1, %xmm2
	addl	%esi, %eax
	movl	$-1024, %esi
	sall	%cl, %esi
	andl	%esi, %eax
	movmskps	%xmm6, %esi
	andl	$8, %esi
	je	.L7
	movdqa	.LC19(%rip), %xmm0
	movdqa	(%rsp), %xmm7
	pxor	%xmm0, %xmm2
	pxor	%xmm0, %xmm7
	movaps	%xmm7, (%rsp)
.L7:
	testl	%ecx, %ecx
	je	.L9
	cmpl	$1, %ecx
	je	.L10
	leal	-1073491968(%rax), %ebx
	shrl	$10, %ebx
.L11:
	salq	$32, %rax
	testl	%edx, %edx
	movq	$0, 16(%rsp)
	movaps	%xmm2, 32(%rsp)
	movq	%rax, 24(%rsp)
	je	.L12
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
.L13:
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	leal	3(%rbx), %r13d
	salq	$4, %r13
	movdqa	.LC20(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC22(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC23(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC24(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movdqa	.LC25(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC26(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC27(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC28(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC29(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movq	__sincosf128_table@GOTPCREL(%rip), %r15
	leal	2(%rbx), %eax
	movdqa	%xmm0, %xmm1
	salq	$4, %rax
	movdqa	(%r15,%rax), %xmm3
	movl	%ebx, %eax
	addq	%r15, %r13
	salq	$4, %rax
	movdqa	(%r15,%rax), %xmm4
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm3, %xmm0
	movaps	%xmm4, (%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__multf3@PLT
	movdqa	0(%r13), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	testq	%r14, %r14
	jns	.L14
	pxor	.LC19(%rip), %xmm0
.L14:
	movaps	%xmm0, (%r12)
	addl	$1, %ebx
	salq	$4, %rbx
	addq	%r15, %rbx
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rbx), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, 0(%rbp)
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leal	-1073518592(%rax), %ebx
	shrl	$9, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	leal	-1073564672(%rax), %ebx
	shrl	$8, %ebx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L22:
	movdqa	(%rsp), %xmm7
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm7, (%r12)
	movaps	%xmm0, 0(%rbp)
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L13
	.size	__kernel_sincosf128, .-__kernel_sincosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC3:
	.long	714772864
	.long	2060366338
	.long	291154936
	.long	1070503184
	.align 16
.LC4:
	.long	101643109
	.long	3351012875
	.long	1029369648
	.long	1071033983
	.align 16
.LC5:
	.long	1100312853
	.long	2152958618
	.long	1631224084
	.long	1071538468
	.align 16
.LC6:
	.long	3270137119
	.long	382197349
	.long	1451185230
	.long	1072016996
	.align 16
.LC7:
	.long	1953806581
	.long	955922514
	.long	978676851
	.long	1072460254
	.align 16
.LC8:
	.long	2053268118
	.long	2686058910
	.long	27269633
	.long	1072865306
	.align 16
.LC9:
	.long	286159680
	.long	286331153
	.long	286331153
	.long	1073221905
	.align 16
.LC10:
	.long	1431655760
	.long	1431655765
	.long	1431655765
	.long	1073501525
	.align 16
.LC11:
	.long	1159938380
	.long	3405235195
	.long	2174208973
	.long	1070771807
	.align 16
.LC12:
	.long	3393754187
	.long	1612633044
	.long	1231626921
	.long	1071289239
	.align 16
.LC13:
	.long	2919187315
	.long	512910039
	.long	2399111197
	.long	1071783661
	.align 16
.LC14:
	.long	541373727
	.long	2860597922
	.long	4218915317
	.long	1072244708
	.align 16
.LC15:
	.long	3874739109
	.long	2686016747
	.long	27269633
	.long	1072668698
	.align 16
.LC16:
	.long	1178467597
	.long	1813430634
	.long	3245086401
	.long	1073048598
	.align 16
.LC17:
	.long	1431400240
	.long	1431655765
	.long	1431655765
	.long	1073370453
	.align 16
.LC18:
	.long	4294967291
	.long	4294967295
	.long	4294967295
	.long	1073610751
	.align 16
.LC19:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC20:
	.long	3881510791
	.long	3698426781
	.long	1323895879
	.long	-1075466652
	.align 16
.LC21:
	.long	2852301633
	.long	208317009
	.long	978676836
	.long	1072460254
	.align 16
.LC22:
	.long	503844232
	.long	2686052114
	.long	27269633
	.long	1072865306
	.align 16
.LC23:
	.long	285088093
	.long	286331153
	.long	286331153
	.long	1073221905
	.align 16
.LC24:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073501525
	.align 16
.LC25:
	.long	2270477792
	.long	3406517392
	.long	4124894775
	.long	-1075238940
	.align 16
.LC26:
	.long	762139716
	.long	48035802
	.long	27269614
	.long	1072668698
	.align 16
.LC27:
	.long	3828089717
	.long	1813423462
	.long	3245086401
	.long	1073048598
	.align 16
.LC28:
	.long	1429819427
	.long	1431655765
	.long	1431655765
	.long	1073370453
	.align 16
.LC29:
	.long	0
	.long	0
	.long	0
	.long	1073610752
