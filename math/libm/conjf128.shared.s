	.text
	.p2align 4,,15
	.globl	__conjf128
	.type	__conjf128, @function
__conjf128:
	movdqa	8(%rsp), %xmm0
	movq	%rdi, %rax
	movaps	%xmm0, (%rdi)
	movdqa	24(%rsp), %xmm0
	pxor	.LC0(%rip), %xmm0
	movaps	%xmm0, 16(%rdi)
	ret
	.size	__conjf128, .-__conjf128
	.weak	conjf128
	.set	conjf128,__conjf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
