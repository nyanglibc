	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cpow
	.type	__cpow, @function
__cpow:
	subq	$24, %rsp
	movsd	%xmm2, 8(%rsp)
	movsd	%xmm3, (%rsp)
	call	__clog@PLT
	movsd	(%rsp), %xmm3
	movsd	8(%rsp), %xmm2
	call	__muldc3@PLT
	addq	$24, %rsp
	jmp	__cexp@PLT
	.size	__cpow, .-__cpow
	.weak	cpowf32x
	.set	cpowf32x,__cpow
	.weak	cpowf64
	.set	cpowf64,__cpow
	.weak	cpow
	.set	cpow,__cpow
