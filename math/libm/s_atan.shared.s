	.text
	.p2align 4,,15
	.type	atanMp.constprop.0, @function
atanMp.constprop.0:
	pushq	%r15
	pushq	%r14
	leaq	u9(%rip), %r15
	pushq	%r13
	pushq	%r12
	xorl	%r13d, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	$6, %ebx
	subq	$2088, %rsp
	leaq	1408(%rsp), %rax
	movsd	%xmm0, 24(%rsp)
	leaq	64(%rsp), %r14
	leaq	400(%rsp), %rbp
	leaq	1072(%rsp), %r12
	movq	%rax, 8(%rsp)
	leaq	1744(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	736(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 40(%rsp)
.L4:
	movsd	24(%rsp), %xmm0
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	__dbl_mp@PLT
	movl	%ebx, %edx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	__mpatan@PLT
	movq	8(%rsp), %rdi
	movsd	(%r15,%r13,2), %xmm0
	movl	%ebx, %esi
	call	__dbl_mp@PLT
	movq	8(%rsp), %rsi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rdi
	call	__mul@PLT
	movq	16(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__add@PLT
	movq	(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__sub@PLT
	movq	32(%rsp), %rsi
	movq	16(%rsp), %rdi
	movl	%ebx, %edx
	call	__mp_dbl@PLT
	movq	40(%rsp), %rsi
	movq	(%rsp), %rdi
	movl	%ebx, %edx
	call	__mp_dbl@PLT
	movsd	48(%rsp), %xmm0
	ucomisd	56(%rsp), %xmm0
	jp	.L5
	je	.L1
.L5:
	addq	$4, %r13
	cmpq	$16, %r13
	je	.L1
	leaq	pr.5962(%rip), %rax
	movl	(%rax,%r13), %ebx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$2088, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	atanMp.constprop.0, .-atanMp.constprop.0
	.p2align 4,,15
	.globl	__atan
	.type	__atan, @function
__atan:
	movq	%xmm0, %rax
	movapd	%xmm0, %xmm2
	sarq	$32, %rax
	movl	%eax, %ecx
	andl	$2146435072, %ecx
	cmpl	$2146435072, %ecx
	je	.L208
.L12:
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$56, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 40(%rsp)
# 0 "" 2
#NO_APP
	movl	40(%rsp), %ebx
	movl	%ebx, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebx
	movl	%eax, 44(%rsp)
	jne	.L209
.L14:
	pxor	%xmm4, %xmm4
	movapd	%xmm2, %xmm6
	ucomisd	%xmm2, %xmm4
	jbe	.L15
	xorpd	.LC1(%rip), %xmm6
.L15:
	movsd	c(%rip), %xmm0
	ucomisd	%xmm6, %xmm0
	jbe	.L170
	movsd	b(%rip), %xmm0
	ucomisd	%xmm6, %xmm0
	jbe	.L171
	movsd	a(%rip), %xmm0
	ucomisd	%xmm6, %xmm0
	ja	.L210
	movapd	%xmm2, %xmm5
	movsd	d13(%rip), %xmm0
	movapd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm5
	mulsd	%xmm5, %xmm0
	mulsd	%xmm5, %xmm1
	addsd	d11(%rip), %xmm0
	movapd	%xmm1, %xmm3
	movsd	u1(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm5, %xmm0
	addsd	d9(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	addsd	d7(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	addsd	d5(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	addsd	d3(%rip), %xmm0
	mulsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm0
	subsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm1
	addsd	%xmm2, %xmm0
	addsd	%xmm2, %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L129
	je	.L23
.L129:
	movsd	f19(%rip), %xmm6
	movapd	%xmm2, %xmm0
	movsd	.LC3(%rip), %xmm3
	mulsd	%xmm5, %xmm6
	movapd	%xmm2, %xmm8
	mulsd	%xmm3, %xmm0
	movapd	%xmm2, %xmm11
	movsd	ff9(%rip), %xmm9
	addsd	f17(%rip), %xmm6
	subsd	%xmm0, %xmm8
	addsd	%xmm0, %xmm8
	mulsd	%xmm5, %xmm6
	movapd	%xmm8, %xmm0
	addsd	f15(%rip), %xmm6
	movapd	%xmm8, %xmm1
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm0
	mulsd	%xmm11, %xmm1
	subsd	%xmm5, %xmm0
	mulsd	%xmm5, %xmm6
	addsd	%xmm1, %xmm0
	addsd	f13(%rip), %xmm6
	addsd	%xmm1, %xmm0
	movapd	%xmm11, %xmm1
	mulsd	%xmm11, %xmm1
	mulsd	%xmm5, %xmm6
	addsd	%xmm1, %xmm0
	movq	.LC4(%rip), %xmm1
	addsd	f11(%rip), %xmm6
	movsd	%xmm0, (%rsp)
	movsd	f9(%rip), %xmm0
	mulsd	%xmm5, %xmm6
	movapd	%xmm0, %xmm12
	movapd	%xmm0, %xmm7
	andpd	%xmm1, %xmm12
	movapd	%xmm6, %xmm10
	addsd	%xmm6, %xmm7
	andpd	%xmm1, %xmm10
	ucomisd	%xmm10, %xmm12
	ja	.L211
	subsd	%xmm7, %xmm6
	addsd	%xmm6, %xmm0
	addsd	%xmm9, %xmm0
	addsd	%xmm4, %xmm0
.L28:
	movapd	%xmm0, %xmm14
	movapd	%xmm5, %xmm6
	movapd	%xmm5, %xmm9
	addsd	%xmm7, %xmm14
	mulsd	%xmm3, %xmm6
	movapd	%xmm14, %xmm10
	movapd	%xmm14, %xmm13
	movapd	%xmm14, %xmm15
	mulsd	%xmm3, %xmm10
	subsd	%xmm6, %xmm9
	subsd	%xmm14, %xmm7
	mulsd	(%rsp), %xmm14
	subsd	%xmm10, %xmm13
	addsd	%xmm9, %xmm6
	movapd	%xmm5, %xmm9
	addsd	%xmm10, %xmm13
	subsd	%xmm6, %xmm9
	movapd	%xmm6, %xmm12
	movapd	%xmm6, %xmm10
	subsd	%xmm13, %xmm15
	mulsd	%xmm13, %xmm12
	mulsd	%xmm9, %xmm13
	mulsd	%xmm15, %xmm10
	mulsd	%xmm9, %xmm15
	addsd	%xmm13, %xmm10
	movapd	%xmm12, %xmm13
	addsd	%xmm10, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm15, %xmm10
	movsd	ff7(%rip), %xmm15
	addsd	%xmm12, %xmm10
	movapd	%xmm7, %xmm12
	movapd	%xmm13, %xmm7
	addsd	%xmm0, %xmm12
	mulsd	%xmm5, %xmm12
	addsd	%xmm12, %xmm14
	movsd	f7(%rip), %xmm12
	movapd	%xmm12, %xmm0
	addsd	%xmm10, %xmm14
	addsd	%xmm14, %xmm7
	subsd	%xmm7, %xmm13
	movapd	%xmm7, %xmm10
	addsd	%xmm7, %xmm0
	andpd	%xmm1, %xmm10
	addsd	%xmm14, %xmm13
	movapd	%xmm12, %xmm14
	andpd	%xmm1, %xmm14
	ucomisd	%xmm10, %xmm14
	jbe	.L175
	subsd	%xmm0, %xmm12
	movapd	%xmm12, %xmm10
	addsd	%xmm7, %xmm10
	addsd	%xmm13, %xmm10
	addsd	%xmm15, %xmm10
.L31:
	movapd	%xmm10, %xmm14
	movapd	%xmm6, %xmm12
	addsd	%xmm0, %xmm14
	movapd	%xmm14, %xmm7
	movapd	%xmm14, %xmm13
	movapd	%xmm14, %xmm15
	mulsd	%xmm3, %xmm7
	subsd	%xmm14, %xmm0
	mulsd	(%rsp), %xmm14
	subsd	%xmm7, %xmm13
	addsd	%xmm7, %xmm13
	movapd	%xmm6, %xmm7
	subsd	%xmm13, %xmm15
	mulsd	%xmm13, %xmm12
	mulsd	%xmm9, %xmm13
	mulsd	%xmm15, %xmm7
	addsd	%xmm13, %xmm7
	movapd	%xmm12, %xmm13
	addsd	%xmm7, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm7, %xmm12
	movapd	%xmm15, %xmm7
	movsd	ff5(%rip), %xmm15
	mulsd	%xmm9, %xmm7
	addsd	%xmm12, %xmm7
	movapd	%xmm0, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm13, %xmm10
	mulsd	%xmm5, %xmm12
	addsd	%xmm12, %xmm14
	movsd	f5(%rip), %xmm12
	movapd	%xmm12, %xmm0
	addsd	%xmm7, %xmm14
	addsd	%xmm14, %xmm10
	subsd	%xmm10, %xmm13
	movapd	%xmm10, %xmm7
	addsd	%xmm10, %xmm0
	andpd	%xmm1, %xmm7
	addsd	%xmm14, %xmm13
	movapd	%xmm12, %xmm14
	andpd	%xmm1, %xmm14
	ucomisd	%xmm7, %xmm14
	jbe	.L176
	movapd	%xmm12, %xmm7
	subsd	%xmm0, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm13, %xmm7
	addsd	%xmm15, %xmm7
.L34:
	movapd	%xmm7, %xmm13
	movapd	%xmm6, %xmm15
	addsd	%xmm0, %xmm13
	movapd	%xmm13, %xmm10
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	mulsd	%xmm3, %xmm10
	subsd	%xmm13, %xmm0
	mulsd	(%rsp), %xmm13
	subsd	%xmm10, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm6, %xmm10
	subsd	%xmm12, %xmm14
	mulsd	%xmm12, %xmm10
	mulsd	%xmm9, %xmm12
	mulsd	%xmm14, %xmm15
	mulsd	%xmm9, %xmm14
	addsd	%xmm12, %xmm15
	movapd	%xmm10, %xmm12
	addsd	%xmm15, %xmm12
	subsd	%xmm12, %xmm10
	addsd	%xmm15, %xmm10
	movsd	ff3(%rip), %xmm15
	addsd	%xmm10, %xmm14
	movapd	%xmm0, %xmm10
	addsd	%xmm7, %xmm10
	movapd	%xmm12, %xmm7
	mulsd	%xmm5, %xmm10
	addsd	%xmm10, %xmm13
	movsd	f3(%rip), %xmm10
	movapd	%xmm10, %xmm0
	addsd	%xmm14, %xmm13
	movapd	%xmm10, %xmm14
	andpd	%xmm1, %xmm14
	addsd	%xmm13, %xmm7
	subsd	%xmm7, %xmm12
	addsd	%xmm7, %xmm0
	addsd	%xmm13, %xmm12
	movapd	%xmm7, %xmm13
	andpd	%xmm1, %xmm13
	ucomisd	%xmm13, %xmm14
	jbe	.L177
	subsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm7
	addsd	%xmm7, %xmm12
	addsd	%xmm15, %xmm12
.L37:
	movapd	%xmm12, %xmm14
	addsd	%xmm0, %xmm14
	movapd	%xmm14, %xmm7
	movapd	%xmm14, %xmm13
	movapd	%xmm14, %xmm10
	mulsd	%xmm3, %xmm7
	subsd	%xmm14, %xmm0
	mulsd	(%rsp), %xmm14
	subsd	%xmm7, %xmm13
	addsd	%xmm12, %xmm0
	addsd	%xmm7, %xmm13
	movapd	%xmm6, %xmm7
	mulsd	%xmm0, %xmm5
	subsd	%xmm13, %xmm10
	mulsd	%xmm13, %xmm7
	mulsd	%xmm9, %xmm13
	addsd	%xmm14, %xmm5
	mulsd	%xmm10, %xmm6
	mulsd	%xmm10, %xmm9
	addsd	%xmm13, %xmm6
	movapd	%xmm7, %xmm13
	addsd	%xmm6, %xmm13
	subsd	%xmm13, %xmm7
	addsd	%xmm6, %xmm7
	addsd	%xmm7, %xmm9
	movapd	%xmm13, %xmm7
	addsd	%xmm9, %xmm5
	addsd	%xmm5, %xmm7
	mulsd	%xmm7, %xmm3
	movapd	%xmm7, %xmm0
	movapd	%xmm7, %xmm9
	subsd	%xmm7, %xmm13
	mulsd	%xmm4, %xmm7
	subsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm3
	movapd	%xmm8, %xmm0
	subsd	%xmm3, %xmm9
	mulsd	%xmm3, %xmm0
	mulsd	%xmm11, %xmm3
	mulsd	%xmm9, %xmm8
	movapd	%xmm0, %xmm6
	mulsd	%xmm9, %xmm11
	addsd	%xmm3, %xmm8
	addsd	%xmm8, %xmm6
	subsd	%xmm6, %xmm0
	movapd	%xmm6, %xmm3
	addsd	%xmm0, %xmm8
	movapd	%xmm2, %xmm0
	andpd	%xmm1, %xmm0
	addsd	%xmm11, %xmm8
	movapd	%xmm13, %xmm11
	addsd	%xmm5, %xmm11
	movapd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm11
	addsd	%xmm7, %xmm11
	addsd	%xmm8, %xmm11
	addsd	%xmm11, %xmm3
	andpd	%xmm3, %xmm1
	subsd	%xmm3, %xmm6
	addsd	%xmm3, %xmm5
	ucomisd	%xmm1, %xmm0
	addsd	%xmm6, %xmm11
	jbe	.L178
	movapd	%xmm2, %xmm0
	subsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	addsd	%xmm3, %xmm11
	addsd	%xmm11, %xmm4
.L40:
	movapd	%xmm4, %xmm1
	addsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm4
	movsd	u5(%rip), %xmm5
	mulsd	%xmm1, %xmm5
	movapd	%xmm4, %xmm0
	addsd	%xmm5, %xmm4
	subsd	%xmm5, %xmm0
	addsd	%xmm1, %xmm4
	addsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm4
	jp	.L63
	je	.L23
	.p2align 4,,10
	.p2align 3
.L63:
	movapd	%xmm2, %xmm0
	call	atanMp.constprop.0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L170:
	movsd	d(%rip), %xmm0
	ucomisd	%xmm6, %xmm0
	jbe	.L184
	movsd	.LC7(%rip), %xmm7
	movapd	%xmm6, %xmm9
	movsd	.LC3(%rip), %xmm3
	leaq	cij(%rip), %rsi
	movapd	%xmm7, %xmm5
	movsd	.LC6(%rip), %xmm12
	movapd	%xmm6, %xmm1
	divsd	%xmm6, %xmm5
	movsd	hpi(%rip), %xmm13
	movsd	u32(%rip), %xmm14
	movapd	%xmm13, %xmm15
	movapd	%xmm5, %xmm0
	movapd	%xmm5, %xmm8
	movapd	%xmm5, %xmm11
	mulsd	%xmm5, %xmm1
	mulsd	%xmm3, %xmm0
	subsd	%xmm0, %xmm8
	addsd	%xmm0, %xmm8
	movapd	%xmm6, %xmm0
	mulsd	%xmm3, %xmm0
	subsd	%xmm8, %xmm11
	movapd	%xmm8, %xmm10
	subsd	%xmm0, %xmm9
	addsd	%xmm0, %xmm9
	movapd	%xmm6, %xmm0
	subsd	%xmm9, %xmm0
	mulsd	%xmm9, %xmm10
	mulsd	%xmm11, %xmm9
	mulsd	%xmm0, %xmm8
	mulsd	%xmm0, %xmm11
	movsd	.LC5(%rip), %xmm0
	mulsd	%xmm5, %xmm0
	addsd	%xmm12, %xmm0
	subsd	%xmm12, %xmm0
	movapd	%xmm10, %xmm12
	subsd	%xmm1, %xmm12
	cvttsd2si	%xmm0, %eax
	movapd	%xmm7, %xmm0
	addsd	%xmm8, %xmm12
	subsd	%xmm1, %xmm0
	movapd	%xmm5, %xmm1
	addsd	%xmm9, %xmm12
	addsd	%xmm11, %xmm12
	subl	$16, %eax
	movslq	%eax, %rcx
	leaq	0(,%rcx,8), %rdx
	subsd	%xmm12, %xmm0
	movsd	hpi1(%rip), %xmm12
	subq	%rcx, %rdx
	cmpl	$111, %eax
	leaq	(%rsi,%rdx,8), %rdx
	subsd	(%rdx), %xmm1
	mulsd	%xmm5, %xmm0
	subsd	8(%rdx), %xmm15
	addsd	%xmm0, %xmm1
	movsd	48(%rdx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	40(%rdx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	32(%rdx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	24(%rdx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	16(%rdx), %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm12, %xmm1
	subsd	%xmm0, %xmm1
	jle	.L212
.L68:
	movapd	%xmm1, %xmm0
	subsd	%xmm14, %xmm0
	addsd	%xmm1, %xmm14
	addsd	%xmm15, %xmm0
	addsd	%xmm15, %xmm14
	ucomisd	%xmm0, %xmm14
	jp	.L69
	je	.L204
.L69:
	addsd	%xmm9, %xmm8
	movapd	%xmm10, %xmm1
	leaq	hij(%rip), %rax
	movq	%rcx, %rdx
	salq	$7, %rdx
	addsd	%xmm8, %xmm1
	subsd	%xmm1, %xmm10
	subsd	%xmm1, %xmm7
	movapd	%xmm5, %xmm1
	mulsd	%xmm4, %xmm1
	addsd	%xmm10, %xmm8
	movapd	%xmm7, %xmm0
	addsd	%xmm8, %xmm11
	subsd	%xmm11, %xmm0
	addsd	%xmm4, %xmm0
	subsd	%xmm1, %xmm0
	movapd	%xmm5, %xmm1
	divsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm5
	subsd	(%rax,%rdx), %xmm1
	addsd	%xmm0, %xmm5
	movapd	%xmm1, %xmm0
	movapd	%xmm5, %xmm6
	movapd	%xmm5, %xmm7
	movapd	%xmm0, %xmm8
	addsd	%xmm1, %xmm6
	movq	.LC4(%rip), %xmm1
	andpd	%xmm1, %xmm8
	andpd	%xmm1, %xmm7
	ucomisd	%xmm7, %xmm8
	jbe	.L185
	subsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm5
.L73:
	movq	%rcx, %rdx
	salq	$7, %rdx
	addq	%rax, %rdx
	movsd	120(%rdx), %xmm0
	movsd	72(%rdx), %xmm7
	mulsd	%xmm6, %xmm0
	movapd	%xmm7, %xmm11
	movapd	%xmm7, %xmm9
	movsd	80(%rdx), %xmm8
	andpd	%xmm1, %xmm11
	addsd	112(%rdx), %xmm0
	mulsd	%xmm6, %xmm0
	addsd	104(%rdx), %xmm0
	mulsd	%xmm6, %xmm0
	addsd	96(%rdx), %xmm0
	mulsd	%xmm6, %xmm0
	addsd	88(%rdx), %xmm0
	mulsd	%xmm6, %xmm0
	movapd	%xmm0, %xmm10
	addsd	%xmm0, %xmm9
	andpd	%xmm1, %xmm10
	ucomisd	%xmm10, %xmm11
	jbe	.L186
	subsd	%xmm9, %xmm7
	addsd	%xmm7, %xmm0
	addsd	%xmm0, %xmm4
	addsd	%xmm8, %xmm4
.L76:
	movapd	%xmm4, %xmm11
	movapd	%xmm6, %xmm0
	movapd	%xmm6, %xmm7
	movq	%rcx, %rdx
	addsd	%xmm9, %xmm11
	mulsd	%xmm3, %xmm0
	salq	$7, %rdx
	movapd	%xmm6, %xmm8
	addq	%rax, %rdx
	subsd	%xmm0, %xmm7
	movapd	%xmm11, %xmm10
	movapd	%xmm11, %xmm15
	mulsd	%xmm3, %xmm10
	subsd	%xmm11, %xmm9
	addsd	%xmm0, %xmm7
	movapd	%xmm11, %xmm0
	mulsd	%xmm5, %xmm11
	subsd	%xmm10, %xmm0
	addsd	%xmm9, %xmm4
	movsd	56(%rdx), %xmm9
	subsd	%xmm7, %xmm8
	movapd	%xmm7, %xmm14
	addsd	%xmm10, %xmm0
	movapd	%xmm7, %xmm10
	mulsd	%xmm6, %xmm4
	subsd	%xmm0, %xmm15
	mulsd	%xmm0, %xmm14
	mulsd	%xmm8, %xmm0
	addsd	%xmm11, %xmm4
	mulsd	%xmm15, %xmm10
	mulsd	%xmm8, %xmm15
	addsd	%xmm0, %xmm10
	movapd	%xmm14, %xmm0
	addsd	%xmm10, %xmm0
	subsd	%xmm0, %xmm14
	addsd	%xmm14, %xmm10
	movapd	%xmm0, %xmm14
	addsd	%xmm15, %xmm10
	movapd	%xmm9, %xmm15
	andpd	%xmm1, %xmm15
	addsd	%xmm10, %xmm4
	movsd	64(%rdx), %xmm10
	addsd	%xmm4, %xmm14
	movapd	%xmm14, %xmm11
	subsd	%xmm14, %xmm0
	andpd	%xmm1, %xmm11
	ucomisd	%xmm11, %xmm15
	addsd	%xmm0, %xmm4
	movapd	%xmm9, %xmm0
	addsd	%xmm14, %xmm0
	jbe	.L187
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm14
	addsd	%xmm4, %xmm14
	addsd	%xmm10, %xmm14
.L79:
	movapd	%xmm14, %xmm9
	movapd	%xmm7, %xmm15
	movapd	%xmm7, %xmm11
	movq	%rcx, %rdx
	addsd	%xmm0, %xmm9
	salq	$7, %rdx
	addq	%rax, %rdx
	movapd	%xmm9, %xmm4
	movapd	%xmm9, %xmm10
	mulsd	%xmm3, %xmm4
	subsd	%xmm9, %xmm0
	subsd	%xmm4, %xmm10
	addsd	%xmm14, %xmm0
	movsd	48(%rdx), %xmm14
	addsd	%xmm4, %xmm10
	movapd	%xmm9, %xmm4
	mulsd	%xmm6, %xmm0
	mulsd	%xmm5, %xmm9
	subsd	%xmm10, %xmm4
	mulsd	%xmm10, %xmm15
	mulsd	%xmm8, %xmm10
	addsd	%xmm9, %xmm0
	mulsd	%xmm4, %xmm11
	mulsd	%xmm8, %xmm4
	addsd	%xmm11, %xmm10
	movapd	%xmm15, %xmm11
	addsd	%xmm10, %xmm11
	subsd	%xmm11, %xmm15
	movapd	%xmm11, %xmm9
	addsd	%xmm15, %xmm10
	addsd	%xmm10, %xmm4
	movsd	40(%rdx), %xmm10
	movapd	%xmm10, %xmm15
	addsd	%xmm4, %xmm0
	andpd	%xmm1, %xmm15
	movapd	%xmm10, %xmm4
	addsd	%xmm0, %xmm9
	subsd	%xmm9, %xmm11
	addsd	%xmm9, %xmm4
	addsd	%xmm11, %xmm0
	movapd	%xmm9, %xmm11
	andpd	%xmm1, %xmm11
	ucomisd	%xmm11, %xmm15
	jbe	.L188
	movapd	%xmm10, %xmm11
	subsd	%xmm4, %xmm11
	addsd	%xmm9, %xmm11
	addsd	%xmm0, %xmm11
	addsd	%xmm14, %xmm11
.L82:
	movapd	%xmm11, %xmm10
	movapd	%xmm7, %xmm14
	movq	%rcx, %rdx
	addsd	%xmm4, %xmm10
	salq	$7, %rdx
	addq	%rax, %rdx
	movapd	%xmm10, %xmm0
	movapd	%xmm10, %xmm9
	movapd	%xmm10, %xmm15
	mulsd	%xmm3, %xmm0
	subsd	%xmm10, %xmm4
	mulsd	%xmm5, %xmm10
	subsd	%xmm0, %xmm9
	addsd	%xmm11, %xmm4
	movsd	24(%rdx), %xmm11
	addsd	%xmm0, %xmm9
	movapd	%xmm7, %xmm0
	mulsd	%xmm6, %xmm4
	subsd	%xmm9, %xmm15
	mulsd	%xmm9, %xmm14
	mulsd	%xmm8, %xmm9
	addsd	%xmm10, %xmm4
	mulsd	%xmm15, %xmm0
	mulsd	%xmm8, %xmm15
	addsd	%xmm9, %xmm0
	movapd	%xmm14, %xmm9
	addsd	%xmm0, %xmm9
	subsd	%xmm9, %xmm14
	movapd	%xmm9, %xmm10
	addsd	%xmm14, %xmm0
	movsd	32(%rdx), %xmm14
	addsd	%xmm15, %xmm0
	movapd	%xmm11, %xmm15
	andpd	%xmm1, %xmm15
	addsd	%xmm0, %xmm4
	movapd	%xmm11, %xmm0
	addsd	%xmm4, %xmm10
	subsd	%xmm10, %xmm9
	addsd	%xmm10, %xmm0
	addsd	%xmm4, %xmm9
	movapd	%xmm10, %xmm4
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm15
	jbe	.L189
	movapd	%xmm11, %xmm4
	subsd	%xmm0, %xmm4
	addsd	%xmm10, %xmm4
	addsd	%xmm9, %xmm4
	addsd	%xmm14, %xmm4
.L85:
	movapd	%xmm4, %xmm9
	salq	$7, %rcx
	addq	%rcx, %rax
	addsd	%xmm0, %xmm9
	mulsd	%xmm9, %xmm3
	movapd	%xmm9, %xmm11
	subsd	%xmm9, %xmm0
	mulsd	%xmm9, %xmm5
	movapd	%xmm3, %xmm10
	movapd	%xmm9, %xmm3
	addsd	%xmm4, %xmm0
	subsd	%xmm10, %xmm3
	mulsd	%xmm0, %xmm6
	addsd	%xmm10, %xmm3
	movapd	%xmm7, %xmm10
	addsd	%xmm6, %xmm5
	subsd	%xmm3, %xmm11
	mulsd	%xmm3, %xmm10
	mulsd	%xmm8, %xmm3
	mulsd	%xmm11, %xmm7
	mulsd	%xmm11, %xmm8
	addsd	%xmm3, %xmm7
	movapd	%xmm10, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm10
	movapd	%xmm3, %xmm0
	addsd	%xmm10, %xmm7
	addsd	%xmm7, %xmm8
	movsd	8(%rax), %xmm7
	movapd	%xmm7, %xmm6
	movapd	%xmm7, %xmm4
	addsd	%xmm8, %xmm5
	andpd	%xmm1, %xmm6
	movsd	16(%rax), %xmm8
	addsd	%xmm5, %xmm0
	subsd	%xmm0, %xmm3
	addsd	%xmm0, %xmm4
	addsd	%xmm5, %xmm3
	movapd	%xmm0, %xmm5
	andpd	%xmm1, %xmm5
	ucomisd	%xmm5, %xmm6
	jbe	.L190
	subsd	%xmm4, %xmm7
	addsd	%xmm0, %xmm7
	addsd	%xmm3, %xmm7
	addsd	%xmm8, %xmm7
.L88:
	movapd	%xmm7, %xmm0
	movapd	%xmm13, %xmm5
	addsd	%xmm4, %xmm0
	andpd	%xmm1, %xmm5
	movapd	%xmm0, %xmm3
	subsd	%xmm0, %xmm4
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm5
	addsd	%xmm4, %xmm7
	movapd	%xmm13, %xmm4
	subsd	%xmm0, %xmm4
	jbe	.L191
	subsd	%xmm4, %xmm13
	subsd	%xmm0, %xmm13
	subsd	%xmm7, %xmm13
	addsd	%xmm13, %xmm12
.L91:
	movapd	%xmm12, %xmm3
	addsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm12
	movsd	u7(%rip), %xmm4
	movapd	%xmm12, %xmm0
	addsd	%xmm4, %xmm12
	subsd	%xmm4, %xmm0
	addsd	%xmm3, %xmm12
	addsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm12
	jp	.L63
.L206:
	jne	.L63
.L203:
	movapd	%xmm2, %xmm5
	andpd	%xmm1, %xmm0
	andpd	.LC1(%rip), %xmm5
	orpd	%xmm5, %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L204:
	movapd	%xmm2, %xmm3
	andpd	.LC4(%rip), %xmm0
	andpd	.LC1(%rip), %xmm3
	orpd	%xmm3, %xmm0
.L23:
	testb	%bpl, %bpl
	jne	.L213
.L11:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%xmm0, %rdx
	andl	$1048575, %eax
	orl	%edx, %eax
	je	.L12
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	movsd	e(%rip), %xmm0
	ucomisd	%xmm6, %xmm0
	jbe	.L192
	movsd	.LC7(%rip), %xmm7
	movapd	%xmm6, %xmm11
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm7, %xmm8
	movapd	%xmm6, %xmm5
	movapd	%xmm7, %xmm15
	divsd	%xmm6, %xmm8
	movapd	%xmm8, %xmm1
	mulsd	%xmm8, %xmm5
	movapd	%xmm8, %xmm9
	mulsd	%xmm3, %xmm1
	movapd	%xmm8, %xmm13
	movapd	%xmm8, %xmm0
	subsd	%xmm5, %xmm15
	mulsd	%xmm8, %xmm0
	subsd	%xmm1, %xmm9
	addsd	%xmm1, %xmm9
	movapd	%xmm6, %xmm1
	mulsd	%xmm3, %xmm1
	subsd	%xmm9, %xmm13
	movapd	%xmm9, %xmm12
	subsd	%xmm1, %xmm11
	addsd	%xmm1, %xmm11
	movapd	%xmm6, %xmm1
	subsd	%xmm11, %xmm1
	mulsd	%xmm11, %xmm12
	mulsd	%xmm13, %xmm11
	mulsd	%xmm1, %xmm9
	mulsd	%xmm1, %xmm13
	movsd	d13(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	d11(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	d9(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	d7(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	d5(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm8, %xmm0
	addsd	d3(%rip), %xmm1
	mulsd	%xmm1, %xmm0
	movapd	%xmm12, %xmm1
	subsd	%xmm5, %xmm1
	movsd	hpi(%rip), %xmm5
	movapd	%xmm5, %xmm10
	movsd	%xmm0, (%rsp)
	movapd	%xmm5, %xmm14
	addsd	%xmm9, %xmm1
	subsd	%xmm8, %xmm14
	addsd	%xmm11, %xmm1
	addsd	%xmm13, %xmm1
	subsd	%xmm1, %xmm15
	movq	.LC4(%rip), %xmm1
	andpd	%xmm1, %xmm10
	mulsd	%xmm8, %xmm15
	movapd	%xmm10, %xmm0
	movsd	%xmm10, 8(%rsp)
	movapd	%xmm8, %xmm10
	andpd	%xmm1, %xmm10
	ucomisd	%xmm10, %xmm0
	jbe	.L193
	movapd	%xmm5, %xmm10
	subsd	%xmm14, %xmm10
	subsd	%xmm8, %xmm10
.L97:
	addsd	hpi1(%rip), %xmm10
	subsd	%xmm15, %xmm10
	movsd	u4(%rip), %xmm15
	subsd	(%rsp), %xmm10
	movapd	%xmm10, %xmm0
	addsd	%xmm15, %xmm10
	subsd	%xmm15, %xmm0
	addsd	%xmm14, %xmm10
	addsd	%xmm14, %xmm0
	ucomisd	%xmm0, %xmm10
	jp	.L98
	je	.L203
.L98:
	addsd	%xmm11, %xmm9
	movapd	%xmm12, %xmm10
	movsd	ff9(%rip), %xmm11
	addsd	%xmm9, %xmm10
	subsd	%xmm10, %xmm12
	subsd	%xmm10, %xmm7
	movsd	f9(%rip), %xmm10
	addsd	%xmm12, %xmm9
	movapd	%xmm7, %xmm0
	movapd	%xmm8, %xmm7
	mulsd	%xmm4, %xmm7
	addsd	%xmm9, %xmm13
	movapd	%xmm8, %xmm9
	subsd	%xmm13, %xmm0
	addsd	%xmm4, %xmm0
	subsd	%xmm7, %xmm0
	divsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm9
	subsd	%xmm9, %xmm8
	movapd	%xmm9, %xmm7
	movapd	%xmm9, %xmm15
	movapd	%xmm8, %xmm13
	addsd	%xmm0, %xmm13
	movapd	%xmm9, %xmm0
	mulsd	%xmm3, %xmm0
	movsd	%xmm13, 16(%rsp)
	mulsd	%xmm9, %xmm13
	subsd	%xmm0, %xmm7
	addsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm15
	movapd	%xmm7, %xmm0
	mulsd	%xmm7, %xmm0
	movapd	%xmm15, %xmm8
	movsd	%xmm15, 24(%rsp)
	movapd	%xmm15, %xmm12
	mulsd	%xmm7, %xmm8
	movapd	%xmm0, %xmm6
	mulsd	%xmm15, %xmm12
	addsd	%xmm8, %xmm8
	addsd	%xmm8, %xmm6
	subsd	%xmm6, %xmm0
	addsd	%xmm8, %xmm0
	movapd	%xmm6, %xmm8
	addsd	%xmm12, %xmm0
	movapd	%xmm13, %xmm12
	addsd	%xmm13, %xmm12
	movapd	%xmm10, %xmm13
	andpd	%xmm1, %xmm13
	addsd	%xmm12, %xmm0
	addsd	%xmm0, %xmm8
	subsd	%xmm8, %xmm6
	addsd	%xmm0, %xmm6
	movsd	f19(%rip), %xmm0
	mulsd	%xmm8, %xmm0
	movsd	%xmm6, (%rsp)
	movapd	%xmm10, %xmm6
	addsd	f17(%rip), %xmm0
	mulsd	%xmm8, %xmm0
	addsd	f15(%rip), %xmm0
	mulsd	%xmm8, %xmm0
	addsd	f13(%rip), %xmm0
	mulsd	%xmm8, %xmm0
	addsd	f11(%rip), %xmm0
	mulsd	%xmm8, %xmm0
	movapd	%xmm0, %xmm12
	addsd	%xmm0, %xmm6
	andpd	%xmm1, %xmm12
	ucomisd	%xmm12, %xmm13
	ja	.L214
	subsd	%xmm6, %xmm0
	addsd	%xmm10, %xmm0
	addsd	%xmm0, %xmm11
	addsd	%xmm11, %xmm4
.L102:
	movapd	%xmm4, %xmm13
	movapd	%xmm8, %xmm0
	movapd	%xmm8, %xmm10
	addsd	%xmm6, %xmm13
	mulsd	%xmm3, %xmm0
	movapd	%xmm13, %xmm11
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	mulsd	%xmm3, %xmm11
	subsd	%xmm0, %xmm10
	subsd	%xmm13, %xmm6
	mulsd	(%rsp), %xmm13
	subsd	%xmm11, %xmm12
	addsd	%xmm10, %xmm0
	movapd	%xmm8, %xmm10
	addsd	%xmm4, %xmm6
	addsd	%xmm12, %xmm11
	subsd	%xmm0, %xmm10
	movapd	%xmm0, %xmm15
	movapd	%xmm0, %xmm12
	mulsd	%xmm8, %xmm6
	subsd	%xmm11, %xmm14
	mulsd	%xmm11, %xmm15
	mulsd	%xmm10, %xmm11
	addsd	%xmm6, %xmm13
	mulsd	%xmm14, %xmm12
	mulsd	%xmm10, %xmm14
	addsd	%xmm12, %xmm11
	movapd	%xmm15, %xmm12
	addsd	%xmm11, %xmm12
	subsd	%xmm12, %xmm15
	movapd	%xmm12, %xmm6
	addsd	%xmm11, %xmm15
	addsd	%xmm14, %xmm15
	addsd	%xmm13, %xmm15
	movsd	f7(%rip), %xmm13
	movapd	%xmm13, %xmm14
	movapd	%xmm13, %xmm4
	addsd	%xmm15, %xmm6
	andpd	%xmm1, %xmm14
	movapd	%xmm6, %xmm11
	subsd	%xmm6, %xmm12
	andpd	%xmm1, %xmm11
	addsd	%xmm6, %xmm4
	ucomisd	%xmm11, %xmm14
	addsd	%xmm15, %xmm12
	movsd	ff7(%rip), %xmm15
	jbe	.L195
	subsd	%xmm4, %xmm13
	addsd	%xmm6, %xmm13
	addsd	%xmm12, %xmm13
	addsd	%xmm15, %xmm13
	movapd	%xmm13, %xmm11
.L105:
	movapd	%xmm11, %xmm12
	movapd	%xmm0, %xmm14
	movapd	%xmm0, %xmm15
	addsd	%xmm4, %xmm12
	movapd	%xmm12, %xmm6
	movapd	%xmm12, %xmm13
	mulsd	%xmm3, %xmm6
	subsd	%xmm12, %xmm4
	subsd	%xmm6, %xmm13
	addsd	%xmm11, %xmm4
	addsd	%xmm13, %xmm6
	movapd	%xmm12, %xmm13
	mulsd	(%rsp), %xmm12
	mulsd	%xmm8, %xmm4
	subsd	%xmm6, %xmm13
	mulsd	%xmm6, %xmm14
	mulsd	%xmm10, %xmm6
	addsd	%xmm4, %xmm12
	mulsd	%xmm13, %xmm15
	mulsd	%xmm10, %xmm13
	addsd	%xmm15, %xmm6
	movapd	%xmm14, %xmm15
	addsd	%xmm6, %xmm15
	subsd	%xmm15, %xmm14
	movapd	%xmm15, %xmm11
	addsd	%xmm6, %xmm14
	addsd	%xmm13, %xmm14
	addsd	%xmm12, %xmm14
	movsd	f5(%rip), %xmm12
	movapd	%xmm12, %xmm13
	movapd	%xmm12, %xmm4
	addsd	%xmm14, %xmm11
	andpd	%xmm1, %xmm13
	movapd	%xmm11, %xmm6
	subsd	%xmm11, %xmm15
	andpd	%xmm1, %xmm6
	addsd	%xmm11, %xmm4
	ucomisd	%xmm6, %xmm13
	addsd	%xmm14, %xmm15
	movsd	ff5(%rip), %xmm14
	jbe	.L196
	subsd	%xmm4, %xmm12
	addsd	%xmm11, %xmm12
	addsd	%xmm15, %xmm12
	movapd	%xmm12, %xmm6
	addsd	%xmm14, %xmm6
.L108:
	movapd	%xmm6, %xmm12
	movapd	%xmm0, %xmm14
	movapd	%xmm0, %xmm15
	addsd	%xmm4, %xmm12
	movapd	%xmm12, %xmm11
	movapd	%xmm12, %xmm13
	mulsd	%xmm3, %xmm11
	subsd	%xmm12, %xmm4
	subsd	%xmm11, %xmm13
	addsd	%xmm6, %xmm4
	addsd	%xmm13, %xmm11
	movapd	%xmm12, %xmm13
	mulsd	(%rsp), %xmm12
	mulsd	%xmm8, %xmm4
	subsd	%xmm11, %xmm13
	mulsd	%xmm11, %xmm14
	mulsd	%xmm10, %xmm11
	addsd	%xmm4, %xmm12
	mulsd	%xmm13, %xmm15
	mulsd	%xmm10, %xmm13
	addsd	%xmm15, %xmm11
	movapd	%xmm14, %xmm15
	addsd	%xmm11, %xmm15
	subsd	%xmm15, %xmm14
	movapd	%xmm15, %xmm4
	addsd	%xmm11, %xmm14
	movsd	f3(%rip), %xmm11
	movapd	%xmm11, %xmm6
	addsd	%xmm13, %xmm14
	movapd	%xmm11, %xmm13
	andpd	%xmm1, %xmm13
	addsd	%xmm12, %xmm14
	addsd	%xmm14, %xmm4
	movapd	%xmm4, %xmm12
	subsd	%xmm4, %xmm15
	andpd	%xmm1, %xmm12
	addsd	%xmm4, %xmm6
	ucomisd	%xmm12, %xmm13
	addsd	%xmm14, %xmm15
	movsd	ff3(%rip), %xmm14
	jbe	.L197
	subsd	%xmm6, %xmm11
	addsd	%xmm4, %xmm11
	addsd	%xmm11, %xmm15
	addsd	%xmm14, %xmm15
.L111:
	movapd	%xmm15, %xmm12
	addsd	%xmm6, %xmm12
	movapd	%xmm12, %xmm4
	movapd	%xmm12, %xmm11
	movapd	%xmm12, %xmm13
	mulsd	%xmm3, %xmm4
	subsd	%xmm12, %xmm6
	mulsd	(%rsp), %xmm12
	subsd	%xmm4, %xmm11
	addsd	%xmm15, %xmm6
	addsd	%xmm11, %xmm4
	movapd	%xmm0, %xmm11
	mulsd	%xmm8, %xmm6
	subsd	%xmm4, %xmm13
	mulsd	%xmm4, %xmm11
	mulsd	%xmm10, %xmm4
	mulsd	%xmm13, %xmm0
	mulsd	%xmm13, %xmm10
	movsd	16(%rsp), %xmm13
	addsd	%xmm4, %xmm0
	movapd	%xmm11, %xmm4
	addsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm11
	movapd	%xmm4, %xmm14
	addsd	%xmm0, %xmm11
	movapd	%xmm6, %xmm0
	movapd	%xmm7, %xmm6
	addsd	%xmm12, %xmm0
	addsd	%xmm10, %xmm11
	addsd	%xmm0, %xmm11
	addsd	%xmm11, %xmm14
	mulsd	%xmm14, %xmm3
	movapd	%xmm14, %xmm0
	movapd	%xmm14, %xmm8
	subsd	%xmm14, %xmm4
	mulsd	%xmm13, %xmm14
	subsd	%xmm3, %xmm0
	addsd	%xmm11, %xmm4
	addsd	%xmm0, %xmm3
	movsd	24(%rsp), %xmm0
	subsd	%xmm3, %xmm8
	mulsd	%xmm3, %xmm6
	mulsd	%xmm0, %xmm3
	mulsd	%xmm8, %xmm7
	movapd	%xmm6, %xmm15
	mulsd	%xmm0, %xmm8
	movapd	%xmm4, %xmm0
	movapd	%xmm9, %xmm4
	mulsd	%xmm9, %xmm0
	addsd	%xmm3, %xmm7
	addsd	%xmm14, %xmm0
	addsd	%xmm7, %xmm15
	subsd	%xmm15, %xmm6
	movapd	%xmm15, %xmm3
	addsd	%xmm6, %xmm7
	addsd	%xmm8, %xmm7
	addsd	%xmm7, %xmm0
	movapd	%xmm9, %xmm7
	andpd	%xmm1, %xmm7
	addsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm6
	subsd	%xmm3, %xmm15
	andpd	%xmm1, %xmm6
	addsd	%xmm3, %xmm4
	ucomisd	%xmm6, %xmm7
	addsd	%xmm15, %xmm0
	jbe	.L198
	movapd	%xmm9, %xmm8
	subsd	%xmm4, %xmm8
	addsd	%xmm3, %xmm8
	addsd	%xmm8, %xmm0
	movapd	%xmm13, %xmm8
	addsd	%xmm0, %xmm8
.L114:
	movapd	%xmm8, %xmm0
	movsd	8(%rsp), %xmm7
	addsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm3
	subsd	%xmm0, %xmm4
	andpd	%xmm1, %xmm3
	ucomisd	%xmm3, %xmm7
	addsd	%xmm4, %xmm8
	movapd	%xmm5, %xmm4
	subsd	%xmm0, %xmm4
	jbe	.L199
	subsd	%xmm4, %xmm5
	subsd	%xmm0, %xmm5
	subsd	%xmm8, %xmm5
	addsd	hpi1(%rip), %xmm5
.L117:
	movapd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm4
	addsd	%xmm4, %xmm5
	movsd	u8(%rip), %xmm4
	movapd	%xmm5, %xmm0
	addsd	%xmm4, %xmm5
	subsd	%xmm4, %xmm0
	addsd	%xmm3, %xmm5
	addsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm5
	jnp	.L206
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L171:
	movsd	.LC5(%rip), %xmm0
	leaq	cij(%rip), %rsi
	movsd	.LC6(%rip), %xmm1
	mulsd	%xmm6, %xmm0
	addsd	%xmm1, %xmm0
	subsd	%xmm1, %xmm0
	movapd	%xmm6, %xmm1
	cvttsd2si	%xmm0, %eax
	subl	$16, %eax
	movslq	%eax, %rdx
	leaq	0(,%rdx,8), %rcx
	subq	%rdx, %rcx
	cmpl	$111, %eax
	leaq	(%rsi,%rcx,8), %rcx
	subsd	(%rcx), %xmm1
	movsd	48(%rcx), %xmm0
	movsd	8(%rcx), %xmm5
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm3
	addsd	40(%rcx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	32(%rcx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	24(%rcx), %xmm0
	mulsd	%xmm1, %xmm0
	addsd	16(%rcx), %xmm0
	mulsd	%xmm0, %xmm3
	jle	.L215
	cmpl	$175, %eax
	movsd	u24(%rip), %xmm1
	jg	.L44
	movsd	u23(%rip), %xmm1
.L44:
	mulsd	%xmm5, %xmm1
	movapd	%xmm3, %xmm0
	subsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm1
	addsd	%xmm5, %xmm0
	addsd	%xmm5, %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L46
	je	.L204
.L46:
	leaq	hij(%rip), %rax
	movq	%rdx, %rcx
	salq	$7, %rcx
	movq	.LC4(%rip), %xmm1
	addq	%rax, %rcx
	subsd	(%rcx), %xmm6
	movsd	120(%rcx), %xmm0
	movsd	72(%rcx), %xmm3
	movsd	80(%rcx), %xmm9
	movapd	%xmm3, %xmm8
	movapd	%xmm3, %xmm7
	andpd	%xmm1, %xmm8
	mulsd	%xmm6, %xmm0
	addsd	112(%rcx), %xmm0
	mulsd	%xmm6, %xmm0
	addsd	104(%rcx), %xmm0
	mulsd	%xmm6, %xmm0
	addsd	96(%rcx), %xmm0
	mulsd	%xmm6, %xmm0
	addsd	88(%rcx), %xmm0
	mulsd	%xmm6, %xmm0
	movapd	%xmm0, %xmm5
	addsd	%xmm0, %xmm7
	andpd	%xmm1, %xmm5
	ucomisd	%xmm5, %xmm8
	jbe	.L179
	subsd	%xmm7, %xmm3
	addsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	addsd	%xmm9, %xmm0
.L50:
	movapd	%xmm0, %xmm11
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm6, %xmm5
	movq	%rdx, %rcx
	addsd	%xmm7, %xmm11
	movapd	%xmm6, %xmm8
	mulsd	%xmm3, %xmm5
	salq	$7, %rcx
	addq	%rax, %rcx
	movapd	%xmm11, %xmm9
	movapd	%xmm11, %xmm10
	movapd	%xmm11, %xmm12
	mulsd	%xmm3, %xmm9
	subsd	%xmm5, %xmm8
	subsd	%xmm11, %xmm7
	mulsd	%xmm4, %xmm11
	subsd	%xmm9, %xmm10
	addsd	%xmm8, %xmm5
	movapd	%xmm6, %xmm8
	addsd	%xmm0, %xmm7
	addsd	%xmm9, %xmm10
	subsd	%xmm5, %xmm8
	movapd	%xmm5, %xmm9
	movapd	%xmm5, %xmm13
	mulsd	%xmm6, %xmm7
	subsd	%xmm10, %xmm12
	mulsd	%xmm10, %xmm9
	mulsd	%xmm8, %xmm10
	addsd	%xmm11, %xmm7
	movsd	56(%rcx), %xmm11
	mulsd	%xmm12, %xmm13
	movapd	%xmm11, %xmm0
	mulsd	%xmm8, %xmm12
	addsd	%xmm10, %xmm13
	movapd	%xmm9, %xmm10
	addsd	%xmm13, %xmm10
	subsd	%xmm10, %xmm9
	addsd	%xmm13, %xmm9
	movapd	%xmm11, %xmm13
	andpd	%xmm1, %xmm13
	addsd	%xmm12, %xmm9
	addsd	%xmm9, %xmm7
	movapd	%xmm10, %xmm9
	addsd	%xmm7, %xmm9
	movapd	%xmm9, %xmm12
	subsd	%xmm9, %xmm10
	andpd	%xmm1, %xmm12
	addsd	%xmm9, %xmm0
	ucomisd	%xmm12, %xmm13
	addsd	%xmm10, %xmm7
	movsd	64(%rcx), %xmm10
	jbe	.L180
	subsd	%xmm0, %xmm11
	addsd	%xmm11, %xmm9
	addsd	%xmm9, %xmm7
	addsd	%xmm10, %xmm7
.L53:
	movapd	%xmm7, %xmm9
	movapd	%xmm5, %xmm13
	movapd	%xmm5, %xmm14
	movq	%rdx, %rcx
	addsd	%xmm0, %xmm9
	salq	$7, %rcx
	addq	%rax, %rcx
	movapd	%xmm9, %xmm11
	movapd	%xmm9, %xmm10
	movapd	%xmm9, %xmm12
	mulsd	%xmm3, %xmm11
	subsd	%xmm9, %xmm0
	mulsd	%xmm4, %xmm9
	subsd	%xmm11, %xmm10
	addsd	%xmm7, %xmm0
	addsd	%xmm11, %xmm10
	mulsd	%xmm6, %xmm0
	subsd	%xmm10, %xmm12
	mulsd	%xmm10, %xmm13
	mulsd	%xmm8, %xmm10
	addsd	%xmm9, %xmm0
	mulsd	%xmm12, %xmm14
	movapd	%xmm13, %xmm11
	mulsd	%xmm8, %xmm12
	addsd	%xmm10, %xmm14
	addsd	%xmm14, %xmm11
	subsd	%xmm11, %xmm13
	movapd	%xmm11, %xmm9
	movapd	%xmm13, %xmm10
	addsd	%xmm14, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm10, %xmm0
	movsd	40(%rcx), %xmm10
	movapd	%xmm10, %xmm13
	movapd	%xmm10, %xmm7
	addsd	%xmm0, %xmm9
	andpd	%xmm1, %xmm13
	movapd	%xmm9, %xmm12
	subsd	%xmm9, %xmm11
	andpd	%xmm1, %xmm12
	addsd	%xmm9, %xmm7
	ucomisd	%xmm12, %xmm13
	addsd	%xmm11, %xmm0
	movsd	48(%rcx), %xmm11
	jbe	.L181
	subsd	%xmm7, %xmm10
	addsd	%xmm10, %xmm9
	addsd	%xmm9, %xmm0
	addsd	%xmm11, %xmm0
.L56:
	movapd	%xmm0, %xmm9
	movapd	%xmm5, %xmm13
	movapd	%xmm5, %xmm14
	movq	%rdx, %rcx
	addsd	%xmm7, %xmm9
	salq	$7, %rcx
	addq	%rax, %rcx
	movapd	%xmm9, %xmm11
	movapd	%xmm9, %xmm10
	movapd	%xmm9, %xmm12
	mulsd	%xmm3, %xmm11
	subsd	%xmm9, %xmm7
	mulsd	%xmm4, %xmm9
	subsd	%xmm11, %xmm10
	addsd	%xmm0, %xmm7
	addsd	%xmm11, %xmm10
	mulsd	%xmm6, %xmm7
	subsd	%xmm10, %xmm12
	mulsd	%xmm10, %xmm13
	mulsd	%xmm8, %xmm10
	addsd	%xmm9, %xmm7
	mulsd	%xmm12, %xmm14
	movapd	%xmm13, %xmm11
	mulsd	%xmm8, %xmm12
	addsd	%xmm10, %xmm14
	addsd	%xmm14, %xmm11
	subsd	%xmm11, %xmm13
	movapd	%xmm11, %xmm9
	movapd	%xmm13, %xmm10
	addsd	%xmm14, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm10, %xmm7
	movsd	24(%rcx), %xmm10
	movapd	%xmm10, %xmm13
	movapd	%xmm10, %xmm0
	addsd	%xmm7, %xmm9
	andpd	%xmm1, %xmm13
	movapd	%xmm9, %xmm12
	subsd	%xmm9, %xmm11
	andpd	%xmm1, %xmm12
	addsd	%xmm9, %xmm0
	ucomisd	%xmm12, %xmm13
	addsd	%xmm11, %xmm7
	movsd	32(%rcx), %xmm11
	jbe	.L182
	subsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm9
	addsd	%xmm9, %xmm7
	addsd	%xmm11, %xmm7
.L59:
	movapd	%xmm7, %xmm9
	salq	$7, %rdx
	addq	%rdx, %rax
	addsd	%xmm0, %xmm9
	mulsd	%xmm9, %xmm3
	movapd	%xmm9, %xmm11
	subsd	%xmm9, %xmm0
	mulsd	%xmm9, %xmm4
	movapd	%xmm3, %xmm10
	movapd	%xmm9, %xmm3
	addsd	%xmm7, %xmm0
	subsd	%xmm10, %xmm3
	mulsd	%xmm0, %xmm6
	addsd	%xmm10, %xmm3
	movapd	%xmm5, %xmm10
	addsd	%xmm4, %xmm6
	subsd	%xmm3, %xmm11
	mulsd	%xmm3, %xmm10
	mulsd	%xmm8, %xmm3
	mulsd	%xmm11, %xmm5
	mulsd	%xmm11, %xmm8
	addsd	%xmm3, %xmm5
	movapd	%xmm10, %xmm3
	addsd	%xmm5, %xmm3
	subsd	%xmm3, %xmm10
	movapd	%xmm3, %xmm0
	addsd	%xmm10, %xmm5
	addsd	%xmm5, %xmm8
	addsd	%xmm8, %xmm6
	movsd	16(%rax), %xmm8
	addsd	%xmm6, %xmm0
	subsd	%xmm0, %xmm3
	movapd	%xmm0, %xmm4
	andpd	%xmm1, %xmm4
	addsd	%xmm6, %xmm3
	movsd	8(%rax), %xmm6
	movapd	%xmm6, %xmm5
	movapd	%xmm6, %xmm7
	andpd	%xmm1, %xmm5
	addsd	%xmm0, %xmm7
	ucomisd	%xmm4, %xmm5
	jbe	.L183
	subsd	%xmm7, %xmm6
	addsd	%xmm0, %xmm6
	addsd	%xmm3, %xmm6
	addsd	%xmm8, %xmm6
.L62:
	movapd	%xmm6, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm7
	addsd	%xmm7, %xmm6
	movsd	u6(%rip), %xmm7
	mulsd	%xmm3, %xmm7
	movapd	%xmm6, %xmm0
	addsd	%xmm7, %xmm6
	subsd	%xmm7, %xmm0
	addsd	%xmm3, %xmm6
	addsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm6
	jnp	.L206
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L210:
	movsd	.LC2(%rip), %xmm0
	ucomisd	%xmm6, %xmm0
	jbe	.L173
	mulsd	%xmm6, %xmm6
	movapd	%xmm2, %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L212:
	movsd	u31(%rip), %xmm14
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L215:
	cmpl	$47, %eax
	movsd	u22(%rip), %xmm1
	jg	.L44
	movsd	u21(%rip), %xmm1
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L192:
	ucomisd	%xmm4, %xmm2
	movsd	hpi(%rip), %xmm0
	ja	.L23
	movsd	mhpi(%rip), %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L213:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	movl	44(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 44(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L209:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %ebp
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L193:
	movapd	%xmm8, %xmm10
	movapd	%xmm5, %xmm0
	addsd	%xmm14, %xmm10
	subsd	%xmm10, %xmm0
	movapd	%xmm0, %xmm10
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L173:
	movapd	%xmm2, %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L183:
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm0
	movapd	%xmm0, %xmm6
	addsd	%xmm8, %xmm6
	addsd	%xmm3, %xmm6
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L182:
	subsd	%xmm0, %xmm9
	addsd	%xmm10, %xmm9
	addsd	%xmm11, %xmm9
	addsd	%xmm9, %xmm7
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L181:
	subsd	%xmm7, %xmm9
	addsd	%xmm10, %xmm9
	addsd	%xmm11, %xmm9
	addsd	%xmm9, %xmm0
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L180:
	subsd	%xmm0, %xmm9
	addsd	%xmm11, %xmm9
	addsd	%xmm10, %xmm9
	addsd	%xmm9, %xmm7
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L179:
	subsd	%xmm7, %xmm0
	addsd	%xmm3, %xmm0
	addsd	%xmm9, %xmm0
	addsd	%xmm4, %xmm0
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L191:
	addsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm13
	addsd	%xmm13, %xmm12
	subsd	%xmm7, %xmm12
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L190:
	subsd	%xmm4, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm0, %xmm7
	addsd	%xmm8, %xmm7
	addsd	%xmm3, %xmm7
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L189:
	subsd	%xmm0, %xmm10
	movapd	%xmm10, %xmm4
	addsd	%xmm11, %xmm4
	addsd	%xmm14, %xmm4
	addsd	%xmm9, %xmm4
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L188:
	subsd	%xmm4, %xmm9
	movapd	%xmm9, %xmm11
	addsd	%xmm10, %xmm11
	addsd	%xmm14, %xmm11
	addsd	%xmm0, %xmm11
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L187:
	subsd	%xmm0, %xmm14
	addsd	%xmm9, %xmm14
	addsd	%xmm10, %xmm14
	addsd	%xmm4, %xmm14
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L186:
	subsd	%xmm9, %xmm0
	addsd	%xmm7, %xmm0
	addsd	%xmm8, %xmm0
	addsd	%xmm0, %xmm4
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L185:
	subsd	%xmm6, %xmm5
	addsd	%xmm0, %xmm5
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L211:
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm0
	addsd	%xmm4, %xmm0
	addsd	%xmm9, %xmm0
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L214:
	subsd	%xmm6, %xmm10
	addsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm4
	addsd	%xmm11, %xmm4
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L199:
	addsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm5
	addsd	hpi1(%rip), %xmm5
	subsd	%xmm8, %xmm5
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L198:
	subsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm8
	addsd	%xmm9, %xmm8
	addsd	16(%rsp), %xmm8
	addsd	%xmm0, %xmm8
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L197:
	subsd	%xmm6, %xmm4
	addsd	%xmm11, %xmm4
	addsd	%xmm14, %xmm4
	addsd	%xmm4, %xmm15
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L196:
	subsd	%xmm4, %xmm11
	addsd	%xmm12, %xmm11
	movapd	%xmm11, %xmm6
	addsd	%xmm14, %xmm6
	addsd	%xmm15, %xmm6
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L195:
	subsd	%xmm4, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm15, %xmm6
	movapd	%xmm6, %xmm11
	addsd	%xmm12, %xmm11
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L178:
	subsd	%xmm5, %xmm3
	addsd	%xmm2, %xmm3
	addsd	%xmm3, %xmm4
	addsd	%xmm11, %xmm4
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L177:
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm10
	addsd	%xmm10, %xmm15
	addsd	%xmm15, %xmm12
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L176:
	movapd	%xmm10, %xmm7
	subsd	%xmm0, %xmm7
	addsd	%xmm12, %xmm7
	addsd	%xmm15, %xmm7
	addsd	%xmm13, %xmm7
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L175:
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm15, %xmm10
	addsd	%xmm13, %xmm10
	jmp	.L31
	.size	__atan, .-__atan
	.weak	atanf32x
	.set	atanf32x,__atan
	.weak	atanf64
	.set	atanf64,__atan
	.weak	atan
	.set	atan,__atan
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	pr.5962, @object
	.size	pr.5962, 16
pr.5962:
	.long	6
	.long	8
	.long	10
	.long	32
	.section	.rodata
	.align 32
	.type	u9, @object
	.size	u9, 32
u9:
	.long	0
	.long	952216155
	.long	0
	.long	901884493
	.long	0
	.long	851552904
	.long	0
	.long	297904726
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	u8, @object
	.size	u8, 8
u8:
	.long	0
	.long	980347702
	.align 8
	.type	u7, @object
	.size	u7, 8
u7:
	.long	0
	.long	982742494
	.align 8
	.type	u6, @object
	.size	u6, 8
u6:
	.long	0
	.long	983090541
	.align 8
	.type	u5, @object
	.size	u5, 8
u5:
	.long	0
	.long	984543953
	.align 8
	.type	u4, @object
	.size	u4, 8
u4:
	.long	0
	.long	1006196196
	.align 8
	.type	u32, @object
	.size	u32, 8
u32:
	.long	0
	.long	1010036961
	.align 8
	.type	u31, @object
	.size	u31, 8
u31:
	.long	0
	.long	1010441951
	.align 8
	.type	u24, @object
	.size	u24, 8
u24:
	.long	0
	.long	1009949919
	.align 8
	.type	u23, @object
	.size	u23, 8
u23:
	.long	0
	.long	1010618455
	.align 8
	.type	u22, @object
	.size	u22, 8
u22:
	.long	0
	.long	1012038608
	.align 8
	.type	u21, @object
	.size	u21, 8
u21:
	.long	0
	.long	1013841856
	.align 8
	.type	u1, @object
	.size	u1, 8
u1:
	.long	0
	.long	1009595266
	.align 8
	.type	hpi1, @object
	.size	hpi1, 8
hpi1:
	.long	856972295
	.long	1016178214
	.align 8
	.type	mhpi, @object
	.size	mhpi, 8
mhpi:
	.long	1413754136
	.long	-1074191877
	.align 8
	.type	hpi, @object
	.size	hpi, 8
hpi:
	.long	1413754136
	.long	1073291771
	.align 8
	.type	e, @object
	.size	e, 8
e:
	.long	0
	.long	1127522290
	.align 8
	.type	d, @object
	.size	d, 8
d:
	.long	0
	.long	1076887552
	.align 8
	.type	c, @object
	.size	c, 8
c:
	.long	0
	.long	1072693248
	.align 8
	.type	b, @object
	.size	b, 8
b:
	.long	0
	.long	1068498944
	.align 8
	.type	a, @object
	.size	a, 8
a:
	.long	0
	.long	1045149306
	.align 8
	.type	f19, @object
	.size	f19, 8
f19:
	.long	-1130254552
	.long	-1079315834
	.align 8
	.type	f17, @object
	.size	f17, 8
f17:
	.long	505290270
	.long	1068375582
	.align 8
	.type	f15, @object
	.size	f15, 8
f15:
	.long	286331153
	.long	-1078914799
	.align 8
	.type	f13, @object
	.size	f13, 8
f13:
	.long	330382100
	.long	1068740923
	.align 8
	.type	f11, @object
	.size	f11, 8
f11:
	.long	1952257862
	.long	-1078508079
	.align 8
	.type	ff9, @object
	.size	ff9, 8
ff9:
	.long	477218588
	.long	1012691399
	.align 8
	.type	f9, @object
	.size	f9, 8
f9:
	.long	477218588
	.long	1069314503
	.align 8
	.type	ff7, @object
	.size	ff7, 8
ff7:
	.long	-1840700270
	.long	-1134409436
	.align 8
	.type	f7, @object
	.size	f7, 8
f7:
	.long	-1840700270
	.long	-1077786332
	.align 8
	.type	ff5, @object
	.size	ff5, 8
ff5:
	.long	-1717986918
	.long	-1133930087
	.align 8
	.type	f5, @object
	.size	f5, 8
f5:
	.long	-1717986918
	.long	1070176665
	.align 8
	.type	ff3, @object
	.size	ff3, 8
ff3:
	.long	1431655765
	.long	-1133161131
	.align 8
	.type	d13, @object
	.size	d13, 8
d13:
	.long	-1959670834
	.long	1068725744
	.align 8
	.type	d11, @object
	.size	d11, 8
d11:
	.long	582040613
	.long	-1078508160
	.align 8
	.type	d9, @object
	.size	d9, 8
d9:
	.long	-451765701
	.long	1069314502
	.align 8
	.type	d7, @object
	.size	d7, 8
d7:
	.long	-1841334781
	.long	-1077786332
	.align 8
	.type	d5, @object
	.size	d5, 8
d5:
	.long	-1717987331
	.long	1070176665
	.align 8
	.type	d3, @object
	.size	d3, 8
d3:
	.long	1431655765
	.long	-1076538027
	.set	f3,d3
	.section	.rodata
	.align 32
	.type	hij, @object
	.size	hij, 30848
hij:
	.long	0
	.long	1068515328
	.long	470182205
	.long	1068513901
	.long	-722996952
	.long	-1136489982
	.long	-383413586
	.long	1072684831
	.long	-1253506718
	.long	1015594944
	.long	-1887551151
	.long	-1078976851
	.long	191156741
	.long	-1135372824
	.long	1128125595
	.long	-1076571489
	.long	1252271667
	.long	-1133137507
	.long	-835348963
	.long	1068489281
	.long	1113421462
	.long	1011771999
	.long	1813755362
	.long	1070077149
	.long	724792223
	.long	-1079030601
	.long	-1033381652
	.long	-1077917518
	.long	-391402554
	.long	1068406345
	.long	-25155950
	.long	1068991123
	.long	0
	.long	1068564480
	.long	-1491250521
	.long	1068562846
	.long	-289185201
	.long	-1138620769
	.long	-1706437975
	.long	1072684040
	.long	1756548645
	.long	1014464759
	.long	-1649983971
	.long	-1078928930
	.long	-1583052865
	.long	1011793734
	.long	-784424696
	.long	-1076574615
	.long	-340503924
	.long	1014191292
	.long	2022929601
	.long	1068540215
	.long	-1007499838
	.long	-1135164893
	.long	2043022554
	.long	1070067943
	.long	-842237055
	.long	-1078964126
	.long	-987338877
	.long	-1077929500
	.long	-229375351
	.long	1068486834
	.long	-153891446
	.long	1068962055
	.long	0
	.long	1068630016
	.long	-1089983143
	.long	1068628077
	.long	1999826817
	.long	-1136618020
	.long	24259457
	.long	1072682931
	.long	-1977537022
	.long	-1134665332
	.long	-296837894
	.long	-1078865211
	.long	699133152
	.long	1010983539
	.long	938180422
	.long	-1076578991
	.long	30981906
	.long	1014215520
	.long	1792764540
	.long	1068601255
	.long	965319085
	.long	-1134821160
	.long	-190328845
	.long	1070055077
	.long	1643661353
	.long	-1078906872
	.long	-1461635679
	.long	-1077956273
	.long	1126494583
	.long	1068545330
	.long	882433437
	.long	1068921674
	.long	0
	.long	1068695552
	.long	831146650
	.long	1068693273
	.long	2042200095
	.long	1006894359
	.long	1222785278
	.long	1072681759
	.long	-658222297
	.long	-1131976070
	.long	-2061461790
	.long	-1078801702
	.long	300930951
	.long	1011222624
	.long	776432185
	.long	-1076583605
	.long	-351146415
	.long	1014125810
	.long	-984948187
	.long	1068661779
	.long	1256615692
	.long	1011953814
	.long	-284337712
	.long	1070041546
	.long	1838852748
	.long	-1078850554
	.long	-2073319150
	.long	-1077991291
	.long	-963121314
	.long	1068596319
	.long	-892133778
	.long	1068879507
	.long	0
	.long	1068761088
	.long	243029405
	.long	1068758431
	.long	677247047
	.long	1012581582
	.long	-1498704919
	.long	1072680525
	.long	413759286
	.long	-1133779861
	.long	1294120501
	.long	-1078738415
	.long	1766877087
	.long	1010959164
	.long	-992526285
	.long	-1076588455
	.long	-672958806
	.long	1012391292
	.long	-1064736132
	.long	1068721762
	.long	-678284593
	.long	-1137258646
	.long	-1589995614
	.long	1070027361
	.long	1608324361
	.long	-1078795215
	.long	1850923826
	.long	-1078027879
	.long	-1331224929
	.long	1068645796
	.long	-474185352
	.long	1068835637
	.long	0
	.long	1068826624
	.long	1735561979
	.long	1068823549
	.long	1650759582
	.long	1010829523
	.long	1403121165
	.long	1072679230
	.long	553462751
	.long	-1134280166
	.long	1253738723
	.long	-1078675361
	.long	1016929929
	.long	1012199812
	.long	632178280
	.long	-1076593537
	.long	-2124009263
	.long	-1132628607
	.long	133714006
	.long	1068781179
	.long	-1861492913
	.long	1012323900
	.long	-1002896740
	.long	1070012533
	.long	-222496240
	.long	-1078740898
	.long	-1761103355
	.long	-1078065989
	.long	-1780227892
	.long	1068693702
	.long	-2137832677
	.long	1068790148
	.long	0
	.long	1068892160
	.long	1339217842
	.long	1068888626
	.long	1140928910
	.long	1012443354
	.long	-1957391336
	.long	1072677873
	.long	-2089844432
	.long	1010096278
	.long	-1663200537
	.long	-1078612551
	.long	-348554045
	.long	1011695969
	.long	-1812401700
	.long	-1076598851
	.long	-3710803
	.long	-1134518917
	.long	-1096371458
	.long	1068840003
	.long	1260713435
	.long	1012838460
	.long	-1914449915
	.long	1069997075
	.long	936565149
	.long	-1078687642
	.long	-161071157
	.long	-1078105571
	.long	182449252
	.long	1068739982
	.long	1096401443
	.long	1068743126
	.long	0
	.long	1068957696
	.long	-588625139
	.long	1068953659
	.long	1362076029
	.long	-1137446898
	.long	-1945656266
	.long	1072676455
	.long	1176833391
	.long	-1132152150
	.long	2117838023
	.long	-1078549995
	.long	1414545293
	.long	-1137376025
	.long	1803735098
	.long	-1076604393
	.long	-1094856229
	.long	1014832909
	.long	-2078452220
	.long	1068898212
	.long	1719780406
	.long	-1136191861
	.long	-1395734834
	.long	1069980999
	.long	-1536075776
	.long	-1078635489
	.long	861543451
	.long	-1078146572
	.long	-1161820641
	.long	1068784582
	.long	725111734
	.long	1068694660
	.long	0
	.long	1069023232
	.long	637726991
	.long	1069018648
	.long	1275870616
	.long	-1134757019
	.long	-1767048500
	.long	1072674976
	.long	407460595
	.long	-1132292562
	.long	1180916873
	.long	-1078487704
	.long	-1497377640
	.long	1012581480
	.long	540522903
	.long	-1076610161
	.long	-487513229
	.long	-1133335213
	.long	2030093550
	.long	1068955781
	.long	183443357
	.long	1012077661
	.long	1141488142
	.long	1069964319
	.long	488074764
	.long	-1078584475
	.long	-1965665966
	.long	-1078188941
	.long	2076329665
	.long	1068827455
	.long	1840450374
	.long	1068644841
	.long	0
	.long	1069088768
	.long	1148779304
	.long	1069083589
	.long	212673740
	.long	1011298952
	.long	-287516969
	.long	1072673436
	.long	602558704
	.long	1016003230
	.long	1793104258
	.long	-1078425689
	.long	1400902929
	.long	1012605557
	.long	1033926012
	.long	-1076616153
	.long	823950024
	.long	-1133405723
	.long	1133015419
	.long	1069012687
	.long	1037070516
	.long	-1134801230
	.long	-423447053
	.long	1069947047
	.long	3224937
	.long	-1078534638
	.long	-1238276583
	.long	-1078232622
	.long	1638882337
	.long	1068868554
	.long	652608137
	.long	1068593763
	.long	0
	.long	1069154304
	.long	1406131441
	.long	1069148481
	.long	493470846
	.long	1012544302
	.long	-624019594
	.long	1072671836
	.long	450435765
	.long	-1131636104
	.long	-2141934219
	.long	-1078363960
	.long	-1898550249
	.long	-1134676552
	.long	1717239505
	.long	-1076622366
	.long	149885179
	.long	1013116655
	.long	331975122
	.long	1069068907
	.long	-1075137606
	.long	1009950481
	.long	-1815545476
	.long	1069929199
	.long	807618689
	.long	-1078486013
	.long	-789163832
	.long	-1078277559
	.long	-835859252
	.long	1068907836
	.long	653986546
	.long	1068541520
	.long	0
	.long	1069219840
	.long	1908875960
	.long	1069213322
	.long	1762703835
	.long	1011614030
	.long	-1555106773
	.long	1072670176
	.long	-346630727
	.long	-1132239485
	.long	998774649
	.long	-1078302527
	.long	329290516
	.long	-1136182771
	.long	1401852828
	.long	-1076628797
	.long	-296491448
	.long	-1133974199
	.long	-1479255693
	.long	1069124418
	.long	1176283076
	.long	-1135462077
	.long	-1326259525
	.long	1069910788
	.long	480060380
	.long	-1078438633
	.long	1044077158
	.long	-1078323694
	.long	2129776825
	.long	1068945263
	.long	1712829205
	.long	1068477473
	.long	0
	.long	1069285376
	.long	-1100019773
	.long	1069278110
	.long	698374535
	.long	1012890511
	.long	-1816424046
	.long	1072668456
	.long	2094120363
	.long	1014507019
	.long	1920309057
	.long	-1078241401
	.long	-1502186937
	.long	1012070719
	.long	-732184943
	.long	-1076635444
	.long	786171746
	.long	1013336121
	.long	1336089894
	.long	1069179200
	.long	-1752623856
	.long	1010828871
	.long	107543910
	.long	1069891830
	.long	-886083953
	.long	-1078392531
	.long	-1919934349
	.long	-1078370970
	.long	-1715127018
	.long	1068980798
	.long	-1891148191
	.long	1068368908
	.long	0
	.long	1069350912
	.long	1547487794
	.long	1069342844
	.long	1113135271
	.long	1010907366
	.long	-101264351
	.long	1072666676
	.long	-1943887303
	.long	1013919881
	.long	491678117
	.long	-1078180591
	.long	-771212056
	.long	1009077263
	.long	-850884232
	.long	-1076642303
	.long	1667427343
	.long	1012273883
	.long	-254195597
	.long	1069233230
	.long	-34088078
	.long	-1139266581
	.long	-1176998908
	.long	1069872338
	.long	-551319545
	.long	-1078347736
	.long	1603534806
	.long	-1078419326
	.long	-1234892020
	.long	1069014409
	.long	-1238993169
	.long	1068258598
	.long	0
	.long	1069416448
	.long	1881061998
	.long	1069407521
	.long	-847839656
	.long	1012485887
	.long	643895994
	.long	1072664838
	.long	1803425969
	.long	1014285058
	.long	1467449708
	.long	-1078120108
	.long	530431453
	.long	1012778980
	.long	933824641
	.long	-1076649371
	.long	-344217536
	.long	-1133134495
	.long	173166923
	.long	1069286490
	.long	-806053223
	.long	-1137325432
	.long	1234103126
	.long	1069852330
	.long	-1601031473
	.long	-1078304277
	.long	1246117927
	.long	-1078468702
	.long	-432816045
	.long	1069046067
	.long	1066533127
	.long	1068146742
	.long	0
	.long	1069481984
	.long	563086395
	.long	1069472140
	.long	1496087315
	.long	-1138514536
	.long	1808747224
	.long	1072662940
	.long	-1859384912
	.long	1012268403
	.long	1618973088
	.long	-1078059961
	.long	1532866594
	.long	1012360823
	.long	553938783
	.long	-1076656646
	.long	1149760696
	.long	1014956483
	.long	-1216402095
	.long	1069338957
	.long	815975678
	.long	1012377709
	.long	-2021477072
	.long	1069831820
	.long	45591853
	.long	-1078262181
	.long	-1121888939
	.long	-1078519037
	.long	-1178484258
	.long	1069075747
	.long	-1916819813
	.long	1068033538
	.long	0
	.long	1069547520
	.long	-1699991698
	.long	1069536698
	.long	-2039084009
	.long	-1135815818
	.long	528611360
	.long	1072660984
	.long	528611360
	.long	-1131445768
	.long	-1660042981
	.long	-1078000160
	.long	495064869
	.long	-1137335911
	.long	-1435049513
	.long	-1076664125
	.long	-946407219
	.long	-1134088884
	.long	-1282293021
	.long	1069390614
	.long	-2138651363
	.long	1012401435
	.long	-1807184762
	.long	1069810825
	.long	-1450966584
	.long	-1078221475
	.long	-451772828
	.long	-1078570268
	.long	1143886766
	.long	1069103427
	.long	-2044170379
	.long	1067919187
	.long	0
	.long	1069580288
	.long	-2078201026
	.long	1069574357
	.long	-2097046531
	.long	-1133911342
	.long	-1726238413
	.long	1072658969
	.long	1717729059
	.long	1014666602
	.long	-1748442825
	.long	-1077940714
	.long	1725687727
	.long	1011717017
	.long	135279162
	.long	-1076671803
	.long	1808188690
	.long	1013586519
	.long	1551214329
	.long	1069441442
	.long	-681799803
	.long	-1136055399
	.long	-637911264
	.long	1069789361
	.long	-295251191
	.long	-1078182181
	.long	-1872827597
	.long	-1078622332
	.long	476429076
	.long	1069129088
	.long	413177988
	.long	1067803889
	.long	0
	.long	1069613056
	.long	-856422270
	.long	1069606573
	.long	465145101
	.long	-1133678173
	.long	848792493
	.long	1072656897
	.long	1502839273
	.long	1015112822
	.long	18579965
	.long	-1077908880
	.long	-1683580982
	.long	-1136513187
	.long	-2144354139
	.long	-1076679679
	.long	144836712
	.long	1014861383
	.long	-1248487426
	.long	1069491422
	.long	1091383787
	.long	-1136449393
	.long	118483748
	.long	1069767446
	.long	-464102536
	.long	-1078144321
	.long	310864070
	.long	-1078675166
	.long	1547870359
	.long	1069152715
	.long	-939950290
	.long	1067687842
	.long	0
	.long	1069645824
	.long	1090914384
	.long	1069638757
	.long	-1964512622
	.long	1013365998
	.long	1212107051
	.long	1072654767
	.long	1474008638
	.long	-1133628800
	.long	-624252860
	.long	-1077879527
	.long	-1384136162
	.long	-1134480441
	.long	1792913064
	.long	-1076687748
	.long	-1995039626
	.long	-1133427651
	.long	1785543890
	.long	1069540538
	.long	1111418490
	.long	1011930296
	.long	154031770
	.long	1069745095
	.long	1527894966
	.long	-1078107915
	.long	674978901
	.long	-1078728707
	.long	-1650824521
	.long	1069174297
	.long	881314857
	.long	1067571247
	.long	0
	.long	1069678592
	.long	-83609817
	.long	1069670906
	.long	349884300
	.long	-1135916967
	.long	950095710
	.long	1072652580
	.long	2056484
	.long	1014599326
	.long	1507064700
	.long	-1077850364
	.long	1288281266
	.long	1012080378
	.long	824286004
	.long	-1076696007
	.long	1845511697
	.long	-1134459241
	.long	1752392466
	.long	1069568146
	.long	-1979259723
	.long	1013426900
	.long	132949484
	.long	1069722326
	.long	-509615332
	.long	-1078072983
	.long	-613707481
	.long	-1078782891
	.long	-180495890
	.long	1069193826
	.long	-1461391904
	.long	1067454299
	.long	0
	.long	1069711360
	.long	387601244
	.long	1069703022
	.long	-1109286128
	.long	1013434071
	.long	1686531362
	.long	1072650336
	.long	-772394388
	.long	1015596799
	.long	331420566
	.long	-1077821397
	.long	-1369585
	.long	-1135496087
	.long	1280776975
	.long	-1076704453
	.long	800108899
	.long	1013862262
	.long	-171830357
	.long	1069591814
	.long	-1410687386
	.long	1013079026
	.long	1603517362
	.long	1069699156
	.long	-938855736
	.long	-1078039540
	.long	1737297182
	.long	-1078837652
	.long	-336023989
	.long	1069211298
	.long	1384059707
	.long	1067224023
	.long	0
	.long	1069744128
	.long	-1292131841
	.long	1069735101
	.long	89527604
	.long	1012462215
	.long	786919060
	.long	1072648036
	.long	-958769051
	.long	1015082121
	.long	-1289553817
	.long	-1077792631
	.long	1607273839
	.long	1011624788
	.long	1166025313
	.long	-1076713082
	.long	1525123978
	.long	-1132879566
	.long	900323700
	.long	1069615027
	.long	-1222253771
	.long	1012855341
	.long	-1680242638
	.long	1069675603
	.long	542616276
	.long	-1078007601
	.long	505136033
	.long	-1078892927
	.long	1973194584
	.long	1069226712
	.long	185971882
	.long	1066989889
	.long	0
	.long	1069776896
	.long	-303326812
	.long	1069767144
	.long	1313211590
	.long	-1133879337
	.long	-52264392
	.long	1072645679
	.long	-653945391
	.long	1015274497
	.long	-135398803
	.long	-1077764069
	.long	1245404314
	.long	-1133833733
	.long	-1265265662
	.long	-1076721891
	.long	802127523
	.long	-1135986942
	.long	-656933236
	.long	1069637775
	.long	-118369252
	.long	1013034080
	.long	1922730442
	.long	1069651685
	.long	-1094234319
	.long	-1077977181
	.long	1265407847
	.long	-1078948652
	.long	-433795563
	.long	1069240069
	.long	-575536628
	.long	1066756209
	.long	0
	.long	1069809664
	.long	-389651860
	.long	1069799150
	.long	216606747
	.long	1013324347
	.long	900755772
	.long	1072643268
	.long	-425811820
	.long	1015140519
	.long	-1211012624
	.long	-1077735715
	.long	-1365664873
	.long	1013658516
	.long	1071000380
	.long	-1076730875
	.long	-1066177031
	.long	1014227769
	.long	-387612957
	.long	1069660053
	.long	172148726
	.long	-1134481188
	.long	-1097615131
	.long	1069627419
	.long	961420279
	.long	-1077948289
	.long	935023381
	.long	-1079024820
	.long	-452860549
	.long	1069251376
	.long	542128018
	.long	1066523361
	.long	0
	.long	1069842432
	.long	-972477794
	.long	1069831118
	.long	-1280989301
	.long	1013053011
	.long	1117367528
	.long	1072640801
	.long	980691760
	.long	-1131986612
	.long	-561306845
	.long	-1077707574
	.long	2068885215
	.long	1013224312
	.long	-1693190960
	.long	-1076740032
	.long	-1210284608
	.long	1014900511
	.long	-922599320
	.long	1069681854
	.long	-1997587033
	.long	1011913247
	.long	2048856688
	.long	1069602824
	.long	-215133130
	.long	-1077928533
	.long	48689308
	.long	-1079137683
	.long	1587337062
	.long	1069260642
	.long	-1416213500
	.long	1066181630
	.long	0
	.long	1069875200
	.long	-1445310697
	.long	1069863047
	.long	-240258728
	.long	1013052195
	.long	-1897188526
	.long	1072638279
	.long	-2019974812
	.long	1015488176
	.long	1849091932
	.long	-1077679649
	.long	1312253551
	.long	1012143445
	.long	-2030007344
	.long	-1076749357
	.long	-1034946721
	.long	-1134518764
	.long	914752772
	.long	1069703172
	.long	515673152
	.long	-1135019830
	.long	-1175131437
	.long	1069577917
	.long	-311370283
	.long	-1077915630
	.long	870421681
	.long	-1079251062
	.long	1948494111
	.long	1069267878
	.long	-577119726
	.long	1065721450
	.long	0
	.long	1069907968
	.long	-1173268726
	.long	1069894936
	.long	-225951629
	.long	1013023362
	.long	-2014622339
	.long	1072635703
	.long	-304475162
	.long	1015314160
	.long	2140765308
	.long	-1077651945
	.long	1680068314
	.long	-1136214181
	.long	-797421881
	.long	-1076758847
	.long	-788092704
	.long	-1133188078
	.long	1231500173
	.long	1069724000
	.long	1630772261
	.long	1012074944
	.long	-1477170846
	.long	1069552717
	.long	1943777004
	.long	-1077903503
	.long	720218532
	.long	-1079364830
	.long	1773228723
	.long	1069273100
	.long	1282943009
	.long	1065176976
	.long	0
	.long	1069940736
	.long	507429482
	.long	1069926785
	.long	738266941
	.long	-1135586133
	.long	-1664210709
	.long	1072633073
	.long	654888302
	.long	-1131709403
	.long	1114082567
	.long	-1077624465
	.long	-1051328489
	.long	1012880826
	.long	1338818899
	.long	-1076768497
	.long	466368029
	.long	1012045192
	.long	1956093836
	.long	1069744333
	.long	197257361
	.long	-1133830747
	.long	-68746744
	.long	1069506964
	.long	1260136047
	.long	-1077892155
	.long	997517400
	.long	-1079478862
	.long	-1830523119
	.long	1069276326
	.long	-1645655313
	.long	1064241943
	.long	0
	.long	1069973504
	.long	-5002636
	.long	1069958591
	.long	1736191156
	.long	-1136700565
	.long	1051348694
	.long	1072630390
	.long	184993633
	.long	1015581778
	.long	-41401207
	.long	-1077597214
	.long	-1101237622
	.long	1013443673
	.long	-401715702
	.long	-1076778305
	.long	-385052514
	.long	-1133432306
	.long	-2042134070
	.long	1069764166
	.long	308361454
	.long	1011023027
	.long	20667727
	.long	1069455501
	.long	304351650
	.long	-1077881587
	.long	2075484865
	.long	-1079593034
	.long	1039720168
	.long	1069277578
	.long	-2012618257
	.long	1061044233
	.long	0
	.long	1070006272
	.long	-1987496999
	.long	1069990356
	.long	1979958311
	.long	-1135438518
	.long	-529728716
	.long	1072627653
	.long	932739010
	.long	-1132085193
	.long	257140012
	.long	-1077570193
	.long	-446285972
	.long	1011697314
	.long	-2041209839
	.long	-1076788265
	.long	1685277823
	.long	1014834464
	.long	-1475420715
	.long	1069783494
	.long	1090252655
	.long	1013017638
	.long	-189435481
	.long	1069403559
	.long	1070868886
	.long	-1077871800
	.long	-1782034152
	.long	-1079707223
	.long	-1652990476
	.long	1069276879
	.long	-1201932519
	.long	-1083760637
	.long	0
	.long	1070039040
	.long	-391654910
	.long	1070022077
	.long	1833592413
	.long	-1135235825
	.long	-154249840
	.long	1072624864
	.long	1432944879
	.long	-1132249610
	.long	-305359736
	.long	-1077543409
	.long	-759569572
	.long	1011906782
	.long	554478390
	.long	-1076798374
	.long	-178367702
	.long	1013447315
	.long	1593678401
	.long	1069802313
	.long	-933807598
	.long	-1133709559
	.long	1821391599
	.long	1069351178
	.long	542577448
	.long	-1077862793
	.long	-1709672995
	.long	-1079821308
	.long	-1331773234
	.long	1069274257
	.long	894663269
	.long	-1082601065
	.long	0
	.long	1070071808
	.long	1271814914
	.long	1070053755
	.long	-1258782262
	.long	1012090800
	.long	-129659390
	.long	1072622023
	.long	2074550241
	.long	-1134468224
	.long	651599595
	.long	-1077516862
	.long	-38408536
	.long	-1135138253
	.long	-1221072653
	.long	-1076808629
	.long	1656414017
	.long	-1133151376
	.long	-1953326325
	.long	1069820618
	.long	2055962001
	.long	1011076533
	.long	-159148004
	.long	1069298392
	.long	-768778548
	.long	-1077854566
	.long	-748909517
	.long	-1079935170
	.long	712192126
	.long	1069269742
	.long	735954996
	.long	-1081938428
	.long	0
	.long	1070104576
	.long	-476517432
	.long	1070085387
	.long	851036429
	.long	-1135236845
	.long	1560076364
	.long	1072619131
	.long	995206959
	.long	1015351234
	.long	1617392011
	.long	-1077490558
	.long	-604224753
	.long	1013776586
	.long	1337946088
	.long	-1076819024
	.long	1400151154
	.long	1009915720
	.long	1771714221
	.long	1069838406
	.long	966054924
	.long	-1134325172
	.long	234583171
	.long	1069245240
	.long	1122916384
	.long	-1077847115
	.long	944870652
	.long	-1080064101
	.long	1473265897
	.long	1069263365
	.long	-1381557088
	.long	-1081518746
	.long	0
	.long	1070137344
	.long	-494429143
	.long	1070116974
	.long	808488015
	.long	-1134487347
	.long	-1631223983
	.long	1072616187
	.long	-1060043078
	.long	-1132200507
	.long	1488194855
	.long	-1077464499
	.long	-66707101
	.long	1013506624
	.long	-122117441
	.long	-1076829557
	.long	1690655545
	.long	-1132753526
	.long	-1879808208
	.long	1069855673
	.long	-1736088614
	.long	-1134890682
	.long	309829901
	.long	1069191756
	.long	733992269
	.long	-1077840439
	.long	1817322304
	.long	-1080290232
	.long	72298347
	.long	1069255162
	.long	-1427323070
	.long	-1081107257
	.long	0
	.long	1070170112
	.long	2097480306
	.long	1070148515
	.long	-788576412
	.long	-1134504994
	.long	957049598
	.long	1072613193
	.long	1312184778
	.long	-1134805737
	.long	-430328866
	.long	-1077438689
	.long	926897485
	.long	1011657489
	.long	-961523036
	.long	-1076840222
	.long	1866167484
	.long	1014865940
	.long	-264036026
	.long	1069872416
	.long	-1476234608
	.long	-1134073427
	.long	1041741168
	.long	1069137977
	.long	227029924
	.long	-1077834534
	.long	1177015104
	.long	-1080515225
	.long	1581445111
	.long	1069245169
	.long	2128854739
	.long	-1080893174
	.long	0
	.long	1070202880
	.long	-378763738
	.long	1070180008
	.long	-407620466
	.long	1011676673
	.long	-1463209257
	.long	1072610148
	.long	2041242370
	.long	1008751501
	.long	-125659650
	.long	-1077413130
	.long	-1826027031
	.long	-1134003583
	.long	-738877317
	.long	-1076851016
	.long	-75666774
	.long	-1134425595
	.long	-696171662
	.long	1069888633
	.long	168584736
	.long	-1135360382
	.long	-1672073865
	.long	1069083939
	.long	752716285
	.long	-1077829396
	.long	-20324478
	.long	-1080738860
	.long	-640421642
	.long	1069233426
	.long	1161088245
	.long	-1080696405
	.long	0
	.long	1070235648
	.long	1611694502
	.long	1070211454
	.long	-1509231756
	.long	-1134502152
	.long	1820043058
	.long	1072607054
	.long	-383778847
	.long	1015675506
	.long	-1761197373
	.long	-1077387825
	.long	1535052496
	.long	-1135136062
	.long	1072023373
	.long	-1076861934
	.long	1250097585
	.long	-1132658605
	.long	-394094787
	.long	1069904321
	.long	-1869549268
	.long	1012521379
	.long	16109792
	.long	1069029679
	.long	-1917503101
	.long	-1077825020
	.long	-1109937026
	.long	-1080960919
	.long	324380931
	.long	1069219976
	.long	1917463257
	.long	-1080504489
	.long	0
	.long	1070268416
	.long	457192057
	.long	1070242851
	.long	-392377723
	.long	1012983995
	.long	68516687
	.long	1072603911
	.long	-41160208
	.long	1015732035
	.long	-494542083
	.long	-1077362778
	.long	-1669116785
	.long	-1133713563
	.long	775486758
	.long	-1076872973
	.long	-283039104
	.long	1013596908
	.long	626877909
	.long	1069919479
	.long	-1102551183
	.long	-1134452806
	.long	-14397931
	.long	1068975230
	.long	-287892631
	.long	-1077821400
	.long	-1393635480
	.long	-1081280528
	.long	-1326574053
	.long	1069204860
	.long	-272042690
	.long	-1080317651
	.long	0
	.long	1070301184
	.long	1464694796
	.long	1070274198
	.long	-65689997
	.long	-1135060296
	.long	-252519846
	.long	1072600718
	.long	1769017101
	.long	-1133510687
	.long	344212661
	.long	-1077337990
	.long	402848401
	.long	-1134139878
	.long	-967797046
	.long	-1076884129
	.long	-1994332348
	.long	1014966830
	.long	-457299827
	.long	1069934103
	.long	-1715563697
	.long	1013447240
	.long	-522925958
	.long	1068920630
	.long	-988308248
	.long	-1077818529
	.long	269597548
	.long	-1081717092
	.long	1135039606
	.long	1069188126
	.long	-2074097575
	.long	-1080136101
	.long	0
	.long	1070333952
	.long	1385324706
	.long	1070305495
	.long	-757013077
	.long	1008827152
	.long	-1244860046
	.long	1072597478
	.long	356246122
	.long	1015153495
	.long	2138685398
	.long	-1077313466
	.long	-170522283
	.long	-1140352530
	.long	1695363828
	.long	-1076903240
	.long	1291957406
	.long	-1133815330
	.long	-702159755
	.long	1069948194
	.long	-499359483
	.long	-1134034625
	.long	-1638599936
	.long	1068865913
	.long	912010117
	.long	-1077816400
	.long	184249239
	.long	-1082168119
	.long	671308818
	.long	1069169820
	.long	-2093922166
	.long	-1079996659
	.long	0
	.long	1070366720
	.long	1299612775
	.long	1070336741
	.long	-178313508
	.long	-1134539557
	.long	-693276469
	.long	1072594190
	.long	-299839107
	.long	1015965537
	.long	-1897661192
	.long	-1077289207
	.long	-311279999
	.long	1013111459
	.long	1056819636
	.long	-1076925991
	.long	-2052980464
	.long	1013742447
	.long	1295969
	.long	1069961751
	.long	-1424783426
	.long	-1134065140
	.long	-702601586
	.long	1068811113
	.long	342712592
	.long	-1077815006
	.long	-1062146806
	.long	-1083022951
	.long	1714014294
	.long	1069149991
	.long	-1041590947
	.long	-1079911462
	.long	0
	.long	1070399488
	.long	-1972097426
	.long	1070367935
	.long	109242918
	.long	-1134994467
	.long	-656275990
	.long	1072590855
	.long	2066321857
	.long	1015112089
	.long	-950949804
	.long	-1077265216
	.long	1325123468
	.long	-1134124291
	.long	1995717546
	.long	-1076948950
	.long	953783985
	.long	1013775416
	.long	-1088775466
	.long	1069974771
	.long	2084267664
	.long	1012067063
	.long	-991184252
	.long	1068756265
	.long	-677746093
	.long	-1077814339
	.long	-49721074
	.long	-1084884389
	.long	-1849209520
	.long	1069128690
	.long	805487499
	.long	-1079829183
	.long	0
	.long	1070432256
	.long	1310544789
	.long	1070399077
	.long	1064430331
	.long	1013218202
	.long	1123012878
	.long	1072587474
	.long	-1608411330
	.long	1015351909
	.long	-966124572
	.long	-1077241495
	.long	928247862
	.long	1009934692
	.long	1800733815
	.long	-1076972108
	.long	-1544593137
	.long	-1134245173
	.long	-991772011
	.long	1069987256
	.long	-1733120897
	.long	1013363240
	.long	1040432000
	.long	1068701403
	.long	-1724830815
	.long	-1077814388
	.long	-2060036753
	.long	1063052720
	.long	-1447679297
	.long	1069105969
	.long	-1284553988
	.long	-1079749901
	.long	0
	.long	1070465024
	.long	-551528650
	.long	1070430165
	.long	355752318
	.long	1013736589
	.long	-1668884538
	.long	1072584046
	.long	885631997
	.long	-1132262363
	.long	1123093641
	.long	-1077218046
	.long	-128488043
	.long	-1136586143
	.long	2065012441
	.long	-1076995457
	.long	2029497880
	.long	1012353962
	.long	387533233
	.long	1069999206
	.long	1392100249
	.long	-1133512597
	.long	-1546194364
	.long	1068646559
	.long	231648447
	.long	-1077815144
	.long	-596553600
	.long	1064551350
	.long	-142178389
	.long	1069081881
	.long	1796362732
	.long	-1079673684
	.long	0
	.long	1070497792
	.long	-2041799266
	.long	1070461200
	.long	1405044609
	.long	-1137344117
	.long	1853191846
	.long	1072580573
	.long	616056213
	.long	1015665442
	.long	214320919
	.long	-1077194872
	.long	1153144739
	.long	-1134411723
	.long	74353211
	.long	-1077018988
	.long	-763831808
	.long	-1134528274
	.long	239913911
	.long	1070010620
	.long	497360753
	.long	-1135032164
	.long	-522780118
	.long	1068591767
	.long	2139838712
	.long	-1077816598
	.long	-1356489448
	.long	1065357013
	.long	-285398585
	.long	1069056481
	.long	-1368743743
	.long	-1079600596
	.long	0
	.long	1070530560
	.long	-1902711731
	.long	1070492180
	.long	539621221
	.long	1013474994
	.long	1117508643
	.long	1072577055
	.long	1217794705
	.long	-1132062902
	.long	216107737
	.long	-1077171975
	.long	-1257328291
	.long	1013113833
	.long	1670543366
	.long	-1077042694
	.long	-2139899185
	.long	-1142064363
	.long	1421398971
	.long	1070021499
	.long	1954978844
	.long	1012935419
	.long	1620655062
	.long	1068537060
	.long	-939089577
	.long	-1077818738
	.long	-1470836104
	.long	1065755235
	.long	228488274
	.long	1069029825
	.long	141834345
	.long	-1079530691
	.long	0
	.long	1070563328
	.long	1159567373
	.long	1070523105
	.long	-1941521775
	.long	-1134975120
	.long	-1545644442
	.long	1072573492
	.long	430516325
	.long	-1131480759
	.long	1162514441
	.long	-1077149357
	.long	-636410934
	.long	1012232895
	.long	-244724414
	.long	-1077066566
	.long	-251318774
	.long	-1133661966
	.long	-452957487
	.long	1070031844
	.long	1049634710
	.long	1013375514
	.long	310455421
	.long	1068465994
	.long	1247120797
	.long	-1077821552
	.long	736623613
	.long	1066146642
	.long	-1168440733
	.long	1069001967
	.long	769520869
	.long	-1079464019
	.long	0
	.long	1070596096
	.long	-114523939
	.long	1070553973
	.long	1022865341
	.long	1013492590
	.long	505290270
	.long	1072569886
	.long	505290270
	.long	1013849630
	.long	-787658362
	.long	-1077127020
	.long	-995719062
	.long	1012859106
	.long	39047806
	.long	-1077090594
	.long	1012911753
	.long	-1134176412
	.long	148409090
	.long	1070041658
	.long	851269604
	.long	1011860950
	.long	1784818500
	.long	1068357106
	.long	-321403680
	.long	-1077825030
	.long	533247305
	.long	1066466366
	.long	628263631
	.long	1068972967
	.long	-289798881
	.long	-1079400621
	.long	0
	.long	1070612480
	.long	-62858268
	.long	1070584785
	.long	-207501846
	.long	1011052870
	.long	1042450456
	.long	1072566236
	.long	-1945882891
	.long	-1134370699
	.long	-466980423
	.long	-1077104965
	.long	-2144994019
	.long	-1133905398
	.long	-454219547
	.long	-1077114773
	.long	1980021082
	.long	-1134965565
	.long	1470312377
	.long	1070050940
	.long	-479704894
	.long	1013397819
	.long	1511750511
	.long	1068248575
	.long	318148820
	.long	-1077829157
	.long	-1519002595
	.long	1066654821
	.long	681641974
	.long	1068942881
	.long	601015135
	.long	-1079340529
	.long	0
	.long	1070628864
	.long	1359373750
	.long	1070605818
	.long	1748171336
	.long	-1133088033
	.long	-1852393610
	.long	1072562543
	.long	-1320223736
	.long	1015142970
	.long	-880512139
	.long	-1077083194
	.long	-62099630
	.long	-1134028302
	.long	-522454875
	.long	-1077139092
	.long	670768151
	.long	-1135227772
	.long	-1258569920
	.long	1070059693
	.long	1102517825
	.long	1013151923
	.long	1141114061
	.long	1068140461
	.long	-1764665688
	.long	-1077833923
	.long	-1628175216
	.long	1066839451
	.long	665411092
	.long	1068911768
	.long	-1747985184
	.long	-1079283773
	.long	0
	.long	1070645248
	.long	-1606589807
	.long	1070621166
	.long	-619363583
	.long	-1136265376
	.long	-1493685663
	.long	1072558808
	.long	1443391151
	.long	-1133711941
	.long	-322387924
	.long	-1077061709
	.long	-2051451876
	.long	1013191954
	.long	899811776
	.long	-1077163543
	.long	1226089937
	.long	1013301208
	.long	1328909063
	.long	1070067920
	.long	1061660108
	.long	-1134502800
	.long	1353646529
	.long	1068032823
	.long	-1025837541
	.long	-1077839313
	.long	1471564272
	.long	1067020131
	.long	-246419672
	.long	1068879686
	.long	-329637182
	.long	-1079230372
	.long	0
	.long	1070661632
	.long	-1747426462
	.long	1070636485
	.long	957134949
	.long	1014378055
	.long	227236582
	.long	1072555032
	.long	603352304
	.long	1014076056
	.long	-967465578
	.long	-1077040511
	.long	1787840523
	.long	1013345549
	.long	427097452
	.long	-1077188119
	.long	-332893633
	.long	-1133977191
	.long	-1647499711
	.long	1070075622
	.long	1169472386
	.long	1010289918
	.long	1689338814
	.long	1067925719
	.long	1265333040
	.long	-1077845313
	.long	2048971267
	.long	1067196743
	.long	-1596541403
	.long	1068846696
	.long	2070839725
	.long	-1079180339
	.long	0
	.long	1070678016
	.long	-452350797
	.long	1070651774
	.long	2077598699
	.long	1014266929
	.long	1431268815
	.long	1072551214
	.long	-573766133
	.long	-1133132154
	.long	-283331208
	.long	-1077019602
	.long	-781746864
	.long	-1136218592
	.long	-1206068428
	.long	-1077212812
	.long	-631125908
	.long	1010232718
	.long	1607291841
	.long	1070082803
	.long	1471502823
	.long	-1136504859
	.long	385956591
	.long	1067819206
	.long	1212760285
	.long	-1077851910
	.long	389532269
	.long	1067369178
	.long	-2146720619
	.long	1068812856
	.long	890404930
	.long	-1079133682
	.long	0
	.long	1070694400
	.long	908341706
	.long	1070667034
	.long	-922256481
	.long	-1133192051
	.long	250736918
	.long	1072547356
	.long	759284070
	.long	-1131420769
	.long	377606857
	.long	-1076998982
	.long	916077592
	.long	1010044852
	.long	836313570
	.long	-1077237612
	.long	1690602916
	.long	1013708190
	.long	-1708634902
	.long	1070089465
	.long	1654455992
	.long	-1135199264
	.long	-1477287291
	.long	1067713338
	.long	774730474
	.long	-1077859089
	.long	1431115037
	.long	1067493850
	.long	-369551537
	.long	1068778225
	.long	2040415868
	.long	-1079090402
	.long	0
	.long	1070710784
	.long	983360839
	.long	1070682263
	.long	2140971816
	.long	-1133013318
	.long	-876434518
	.long	1072543457
	.long	-1013297540
	.long	-1132315782
	.long	70711302
	.long	-1076978654
	.long	-2007497781
	.long	-1134254612
	.long	-1706260125
	.long	-1077262513
	.long	1737733731
	.long	1013390579
	.long	-1777982405
	.long	1070095612
	.long	499992045
	.long	1012912818
	.long	-120822433
	.long	1067608170
	.long	-934933612
	.long	-1077866836
	.long	225359290
	.long	1067575740
	.long	776423874
	.long	1068742864
	.long	-254701055
	.long	-1079050493
	.long	0
	.long	1070727168
	.long	-1559498376
	.long	1070697461
	.long	1955273721
	.long	1013124260
	.long	497516574
	.long	1072539520
	.long	-1776289642
	.long	-1132060680
	.long	-1743467759
	.long	-1076958619
	.long	885719498
	.long	-1134585787
	.long	-143679583
	.long	-1077287506
	.long	-1544656250
	.long	1013484410
	.long	-553056732
	.long	1070101247
	.long	1874515131
	.long	1009844591
	.long	-2080505156
	.long	1067503755
	.long	834242103
	.long	-1077875134
	.long	1529913128
	.long	1067655398
	.long	1969011779
	.long	1068706830
	.long	1575624155
	.long	-1079013942
	.long	0
	.long	1070743552
	.long	556652676
	.long	1070712629
	.long	2026581806
	.long	-1134044756
	.long	-1760533550
	.long	1072535543
	.long	-927783241
	.long	1013424651
	.long	-907013287
	.long	-1076938877
	.long	-794406483
	.long	1011518358
	.long	1082812247
	.long	-1077312582
	.long	-431862010
	.long	1013841913
	.long	1099438765
	.long	1070106375
	.long	570752297
	.long	-1134314759
	.long	-1411981043
	.long	1067349918
	.long	-819025087
	.long	-1077883970
	.long	374993783
	.long	1067732785
	.long	-1521841516
	.long	1068670183
	.long	-1200000041
	.long	-1078982719
	.long	0
	.long	1070759936
	.long	1743027350
	.long	1070727765
	.long	687089934
	.long	-1134460125
	.long	-890541014
	.long	1072531528
	.long	-1602054494
	.long	1015146253
	.long	-1452529774
	.long	-1076919429
	.long	-964148375
	.long	-1133857330
	.long	1563761883
	.long	-1077337735
	.long	1752276353
	.long	1013018624
	.long	-926823929
	.long	1070110998
	.long	414251035
	.long	-1133890759
	.long	212820257
	.long	1067144400
	.long	1213965202
	.long	-1077893326
	.long	-1034894562
	.long	1067807863
	.long	1200665768
	.long	1068632982
	.long	1254240632
	.long	-1078967773
	.long	0
	.long	1070776320
	.long	725094466
	.long	1070742870
	.long	371196040
	.long	-1134800105
	.long	1285092167
	.long	1072527476
	.long	-1703779202
	.long	-1131774548
	.long	1573546146
	.long	-1076900276
	.long	-1883635747
	.long	1013199838
	.long	610288078
	.long	-1077362956
	.long	531861856
	.long	-1134115296
	.long	-1123670731
	.long	1070115122
	.long	-71063444
	.long	-1133632313
	.long	1691357341
	.long	1066940683
	.long	-2120275560
	.long	-1077903188
	.long	-2021326920
	.long	1067880601
	.long	1875205787
	.long	1068595284
	.long	1603734145
	.long	-1078954472
	.long	0
	.long	1070792704
	.long	542633798
	.long	1070757943
	.long	-747766655
	.long	-1133479987
	.long	-1344285254
	.long	1072523386
	.long	-509768387
	.long	-1132974488
	.long	-1831112741
	.long	-1076884486
	.long	-1062251969
	.long	1014518989
	.long	1533283746
	.long	-1077388238
	.long	1584341974
	.long	1012751325
	.long	-1579535662
	.long	1070118751
	.long	696650750
	.long	1009223903
	.long	-293532320
	.long	1066738861
	.long	-1784242710
	.long	-1077913539
	.long	726482412
	.long	1067950969
	.long	-1557590530
	.long	1068557147
	.long	434184839
	.long	-1078942798
	.long	0
	.long	1070809088
	.long	-40149022
	.long	1070772983
	.long	-1044196285
	.long	1009001423
	.long	-1998468446
	.long	1072519260
	.long	19111448
	.long	-1137931534
	.long	-1417082121
	.long	-1076875206
	.long	1919062246
	.long	1014842047
	.long	-1256759840
	.long	-1077413573
	.long	-522337433
	.long	1012751691
	.long	873782908
	.long	1070121890
	.long	-1164787762
	.long	1012588187
	.long	-630160067
	.long	1066539025
	.long	-819746064
	.long	-1077924363
	.long	105144312
	.long	1068018941
	.long	-1053758556
	.long	1068518628
	.long	702427445
	.long	-1078932732
	.long	0
	.long	1070825472
	.long	2055355646
	.long	1070787992
	.long	-1902112054
	.long	1013682469
	.long	1812977538
	.long	1072515098
	.long	-2046855958
	.long	1015209380
	.long	794566168
	.long	-1076866074
	.long	-1899612232
	.long	-1134060302
	.long	-789692869
	.long	-1077438953
	.long	1931495846
	.long	-1135701536
	.long	1744193421
	.long	1070124543
	.long	-184050077
	.long	-1134774695
	.long	-1903233402
	.long	1066280732
	.long	-1567356799
	.long	-1077935643
	.long	-113784146
	.long	1068084494
	.long	-635558529
	.long	1068460623
	.long	-1333968194
	.long	-1078924252
	.long	0
	.long	1070841856
	.long	1337297234
	.long	1070802968
	.long	-1449943249
	.long	-1132816912
	.long	-299966160
	.long	1072510900
	.long	1874534189
	.long	1014265153
	.long	-534586099
	.long	-1076857092
	.long	1429492282
	.long	1013810777
	.long	975183211
	.long	-1077464370
	.long	-913687966
	.long	-1135172271
	.long	1731865016
	.long	1070126716
	.long	-6950938
	.long	-1135300963
	.long	1959684116
	.long	1065889518
	.long	1346087156
	.long	-1077958597
	.long	1622556533
	.long	1068147612
	.long	-260050316
	.long	1068382392
	.long	570490445
	.long	-1078917332
	.long	0
	.long	1070858240
	.long	923265996
	.long	1070817911
	.long	1152807640
	.long	-1133943899
	.long	-1543742694
	.long	1072506668
	.long	657236988
	.long	1015734301
	.long	-1964204658
	.long	-1076848258
	.long	1918459702
	.long	1014279898
	.long	1725284607
	.long	-1077489818
	.long	1017539777
	.long	1010896426
	.long	-1893328971
	.long	1070128414
	.long	1336736615
	.long	-1133701314
	.long	865495171
	.long	1065502779
	.long	721793387
	.long	-1077982883
	.long	122674863
	.long	1068208278
	.long	-1673132599
	.long	1068303729
	.long	671291645
	.long	-1078911947
	.long	0
	.long	1070874624
	.long	-344582788
	.long	1070832820
	.long	1512130323
	.long	-1133316902
	.long	583210533
	.long	1072502402
	.long	-706405567
	.long	1015726353
	.long	132753562
	.long	-1076839573
	.long	2141855194
	.long	1014430547
	.long	-1218508942
	.long	-1077515289
	.long	-294821493
	.long	-1133926531
	.long	1855208809
	.long	1070129643
	.long	-974348610
	.long	-1133935932
	.long	-1674757108
	.long	1064888124
	.long	1884943239
	.long	-1078007982
	.long	379072344
	.long	1068266480
	.long	-1052331001
	.long	1068224740
	.long	-2097925548
	.long	-1078908068
	.long	0
	.long	1070891008
	.long	690426164
	.long	1070847697
	.long	1103926666
	.long	1014052810
	.long	-5230516
	.long	1072498101
	.long	870880997
	.long	-1131427551
	.long	977920596
	.long	-1076831038
	.long	-1839417959
	.long	1013990776
	.long	1970308354
	.long	-1077540775
	.long	-1611743636
	.long	-1134589764
	.long	-1006937060
	.long	1070130408
	.long	-692618747
	.long	1013834004
	.long	-607729577
	.long	1063962291
	.long	-559163501
	.long	-1078033860
	.long	117133991
	.long	1068322210
	.long	736782732
	.long	1068145531
	.long	-348989627
	.long	-1078905664
	.long	0
	.long	1070907392
	.long	-1385398989
	.long	1070862539
	.long	-1374440174
	.long	-1132786549
	.long	-803723739
	.long	1072493768
	.long	-200263784
	.long	1015964424
	.long	270160952
	.long	-1076822652
	.long	2100182580
	.long	1010982861
	.long	-748092170
	.long	-1077566270
	.long	1425177526
	.long	1012741507
	.long	2071244812
	.long	1070130716
	.long	-270178122
	.long	1009973165
	.long	-1439275022
	.long	1061171040
	.long	1285052864
	.long	-1078060482
	.long	-1904753271
	.long	1068375462
	.long	1859486270
	.long	1068066203
	.long	-237775840
	.long	-1078904702
	.long	0
	.long	1070923776
	.long	918728458
	.long	1070877348
	.long	1012249530
	.long	-1133128220
	.long	693897075
	.long	1072489403
	.long	-1853547711
	.long	-1132302379
	.long	-2111724158
	.long	-1076814416
	.long	-1443685103
	.long	1014928980
	.long	-341903775
	.long	-1077591766
	.long	1583366972
	.long	1012350895
	.long	-1396675782
	.long	1070130572
	.long	1692238317
	.long	-1134708070
	.long	-807145442
	.long	-1084071945
	.long	-1649521892
	.long	-1078087816
	.long	-1907474922
	.long	1068426235
	.long	-1027207187
	.long	1067986857
	.long	-516598311
	.long	-1078905148
	.long	0
	.long	1070940160
	.long	-2066693605
	.long	1070892122
	.long	1969473190
	.long	1014096522
	.long	-1596010579
	.long	1072485005
	.long	1231096850
	.long	-1133333816
	.long	-1816425048
	.long	-1076806329
	.long	457758994
	.long	1012789370
	.long	-1078605641
	.long	-1077617256
	.long	966854871
	.long	1012443647
	.long	-1723241020
	.long	1070129983
	.long	433297756
	.long	1010306443
	.long	-1172585774
	.long	-1082901159
	.long	-1216847199
	.long	-1078115827
	.long	-4632292
	.long	1068474529
	.long	-391615315
	.long	1067907591
	.long	-1653235447
	.long	-1078906966
	.long	0
	.long	1070956544
	.long	1483247847
	.long	1070906862
	.long	2082645847
	.long	-1133621817
	.long	-872750169
	.long	1072480576
	.long	-659097159
	.long	-1136589731
	.long	1387482332
	.long	-1076798391
	.long	766140588
	.long	1014143378
	.long	939211247
	.long	-1077642733
	.long	880989041
	.long	1012708274
	.long	-1440161758
	.long	1070128955
	.long	-1768270777
	.long	-1134450517
	.long	-967709516
	.long	-1082187467
	.long	1993106423
	.long	-1078144481
	.long	-270578207
	.long	1068509646
	.long	233029526
	.long	1067828501
	.long	-2065343820
	.long	-1078910119
	.long	0
	.long	1070972928
	.long	1938200953
	.long	1070921567
	.long	-134278904
	.long	-1134994916
	.long	1073198380
	.long	1072476117
	.long	1749771028
	.long	1015472281
	.long	-685951378
	.long	-1076790603
	.long	-1372619919
	.long	1012936492
	.long	583832943
	.long	-1077668191
	.long	-376691519
	.long	1013043870
	.long	1848266655
	.long	1070127495
	.long	1248491648
	.long	-1134502285
	.long	-1868676372
	.long	-1081807534
	.long	-1520278616
	.long	-1078173745
	.long	219518979
	.long	1068531323
	.long	-1294077429
	.long	1067749677
	.long	1351386501
	.long	-1078914568
	.long	0
	.long	1070989312
	.long	-1722692278
	.long	1070936237
	.long	-2138554238
	.long	-1134406044
	.long	-1845523065
	.long	1072471627
	.long	453878640
	.long	-1133042689
	.long	1127176456
	.long	-1076782963
	.long	-126792486
	.long	1014826590
	.long	872669308
	.long	-1077693623
	.long	655883184
	.long	1013667613
	.long	-1747916488
	.long	1070125609
	.long	-1333131739
	.long	1013363240
	.long	-1023501090
	.long	-1081461660
	.long	-273223100
	.long	-1078203585
	.long	971406347
	.long	1068551770
	.long	-1816855693
	.long	1067671211
	.long	-187142280
	.long	-1078920274
	.long	0
	.long	1071005696
	.long	-1910813480
	.long	1070950872
	.long	-418131998
	.long	-1133515679
	.long	1461026761
	.long	1072467108
	.long	1364669626
	.long	1014028820
	.long	-1021751148
	.long	-1076775473
	.long	-1454145979
	.long	1014238414
	.long	74247811
	.long	-1077719022
	.long	-465109744
	.long	-1134224532
	.long	-77698411
	.long	1070123304
	.long	-2109301773
	.long	1013279954
	.long	-1531987173
	.long	-1081121432
	.long	-616908208
	.long	-1078233967
	.long	-1174579476
	.long	1068570994
	.long	-1825971874
	.long	1067593189
	.long	-2105026243
	.long	-1078927194
	.long	0
	.long	1071022080
	.long	392040270
	.long	1070965472
	.long	-1887247273
	.long	1014053754
	.long	604734382
	.long	1072462560
	.long	282302701
	.long	-1131914410
	.long	-1931784784
	.long	-1076768131
	.long	766526591
	.long	1007802719
	.long	289559873
	.long	-1077744382
	.long	1798093475
	.long	-1139683036
	.long	-1931715507
	.long	1070120588
	.long	2034517168
	.long	-1135336175
	.long	-765832171
	.long	-1080934403
	.long	-1127096508
	.long	-1078264858
	.long	649982044
	.long	1068589004
	.long	-1481092122
	.long	1067515695
	.long	132752223
	.long	-1078935287
	.long	0
	.long	1071038464
	.long	-71403891
	.long	1070980035
	.long	460781530
	.long	1013883116
	.long	-1921467954
	.long	1072457983
	.long	1286971587
	.long	-1132221631
	.long	-535011730
	.long	-1076760938
	.long	-76097315
	.long	1014207020
	.long	-1145948553
	.long	-1077769697
	.long	679870962
	.long	1013864746
	.long	1579688963
	.long	1070117467
	.long	1887308282
	.long	-1141932712
	.long	-1235559119
	.long	-1080770080
	.long	-1348128997
	.long	-1078296225
	.long	1668360908
	.long	1068605807
	.long	-1784097875
	.long	1067427253
	.long	1921075024
	.long	-1078944511
	.long	0
	.long	1071054848
	.long	50974690
	.long	1070994564
	.long	359735040
	.long	-1132826025
	.long	665836011
	.long	1072453379
	.long	-1288954510
	.long	1013170859
	.long	100664345
	.long	-1076753892
	.long	-182830552
	.long	-1132765209
	.long	1214960328
	.long	-1077794959
	.long	-129335634
	.long	-1136026955
	.long	-1660782569
	.long	1070113948
	.long	775384780
	.long	-1135346517
	.long	-357840093
	.long	-1080608719
	.long	-1941505074
	.long	-1078328035
	.long	-1858934324
	.long	1068621414
	.long	-728933019
	.long	1067274857
	.long	1898140836
	.long	-1078954821
	.long	0
	.long	1071071232
	.long	-164249146
	.long	1071009055
	.long	-406782530
	.long	-1132485629
	.long	-2034906120
	.long	1072448747
	.long	-1134864851
	.long	-1133887231
	.long	1358635437
	.long	-1076746995
	.long	-1550066662
	.long	-1133182874
	.long	-550578392
	.long	-1077820164
	.long	6779625
	.long	1013614523
	.long	-1859995209
	.long	1070110039
	.long	1169182581
	.long	-1140505266
	.long	1819838705
	.long	-1080450359
	.long	-533907795
	.long	-1078360256
	.long	44668517
	.long	1068635837
	.long	-1220875061
	.long	1067123985
	.long	1453004600
	.long	-1078966173
	.long	0
	.long	1071087616
	.long	-1621121282
	.long	1071023511
	.long	1293605532
	.long	-1136502911
	.long	1043885183
	.long	1072444089
	.long	-1933924929
	.long	1014325299
	.long	480792185
	.long	-1076740245
	.long	-249309836
	.long	1012944064
	.long	-1972284363
	.long	-1077845304
	.long	1162852605
	.long	1012725467
	.long	-1706117836
	.long	1070105747
	.long	1119443376
	.long	-1134130596
	.long	1113066421
	.long	-1080295038
	.long	-444453546
	.long	-1078392855
	.long	797934195
	.long	1068649087
	.long	-1554874554
	.long	1066974780
	.long	-51809680
	.long	-1078978523
	.long	0
	.long	1071104000
	.long	-909387923
	.long	1071037930
	.long	1047390248
	.long	-1134928810
	.long	-511345404
	.long	1072439404
	.long	-171427726
	.long	-1132444769
	.long	-845440912
	.long	-1076733643
	.long	-236162298
	.long	1013624728
	.long	922307842
	.long	-1077870374
	.long	-647133953
	.long	-1133612342
	.long	779567953
	.long	1070101080
	.long	-1199173904
	.long	1013731490
	.long	1948184812
	.long	-1080142790
	.long	2068647618
	.long	-1078425800
	.long	-1474936573
	.long	1068661178
	.long	-1477333451
	.long	1066827379
	.long	1381483486
	.long	-1078998943
	.long	0
	.long	1071120384
	.long	1105518321
	.long	1071052313
	.long	-420026088
	.long	-1132698710
	.long	58835168
	.long	1072434695
	.long	58835168
	.long	1015811591
	.long	-785006220
	.long	-1076727187
	.long	-527098632
	.long	-1137887955
	.long	-1282619185
	.long	-1077895369
	.long	-1368051073
	.long	-1133913215
	.long	-673562308
	.long	1070096044
	.long	1857741500
	.long	1012603529
	.long	-2130917355
	.long	-1080013463
	.long	502474489
	.long	-1078459060
	.long	137091680
	.long	1068672126
	.long	1888605604
	.long	1066681913
	.long	-529341300
	.long	-1079027359
	.long	0
	.long	1071136768
	.long	-717577558
	.long	1071066658
	.long	1316614780
	.long	-1132618605
	.long	916473641
	.long	1072429960
	.long	-1698502111
	.long	-1131993275
	.long	-1653144881
	.long	-1076720877
	.long	1273544694
	.long	1014503865
	.long	-1331474058
	.long	-1077920282
	.long	-936163409
	.long	1012776880
	.long	858410886
	.long	1070090649
	.long	170053933
	.long	1012740174
	.long	-311743184
	.long	-1079940457
	.long	-535656112
	.long	-1078492605
	.long	-357986473
	.long	1068681944
	.long	1001626715
	.long	1066538506
	.long	939838115
	.long	-1079057495
	.long	0
	.long	1071153152
	.long	1384215810
	.long	1071080967
	.long	-1848871424
	.long	-1135750889
	.long	215605758
	.long	1072425201
	.long	-1575088086
	.long	-1132816760
	.long	-1328455191
	.long	-1076714713
	.long	347357504
	.long	-1134517591
	.long	-2143318725
	.long	-1077954087
	.long	710688919
	.long	1012815737
	.long	-306396317
	.long	1070084900
	.long	-1606146236
	.long	1013260220
	.long	-384534589
	.long	-1079869029
	.long	1672915952
	.long	-1078526402
	.long	-596399974
	.long	1068690651
	.long	-1435151720
	.long	1066392759
	.long	595341117
	.long	-1079089261
	.long	0
	.long	1071169536
	.long	-1986979631
	.long	1071095238
	.long	-1594924947
	.long	1009480723
	.long	396676035
	.long	1072420418
	.long	838903112
	.long	-1135219107
	.long	-1845845709
	.long	-1076708694
	.long	-84667064
	.long	-1133784175
	.long	-1440557006
	.long	-1078003554
	.long	-931856967
	.long	1010907963
	.long	-1010946038
	.long	1070078807
	.long	-1072012961
	.long	1013690267
	.long	1250373319
	.long	-1079799190
	.long	-739818547
	.long	-1078560423
	.long	1334940933
	.long	1068698264
	.long	683117320
	.long	1066114875
	.long	1910718109
	.long	-1079122565
	.long	0
	.long	1071185920
	.long	1264825335
	.long	1071109472
	.long	1546331290
	.long	1014406280
	.long	-403932531
	.long	1072415611
	.long	722347815
	.long	-1133571514
	.long	-809641990
	.long	-1076702820
	.long	1577108866
	.long	1014827793
	.long	-2030554981
	.long	-1078052825
	.long	-93563225
	.long	1012753641
	.long	2121132254
	.long	1070072377
	.long	2004281417
	.long	1013503535
	.long	1332576279
	.long	-1079730951
	.long	-560890329
	.long	-1078594637
	.long	-1908056894
	.long	1068704800
	.long	-1174632194
	.long	1065841776
	.long	-961477322
	.long	-1079157315
	.long	0
	.long	1071202304
	.long	1779741137
	.long	1071123668
	.long	-1769670119
	.long	-1132561927
	.long	235512354
	.long	1072410783
	.long	2114223837
	.long	1015379086
	.long	13417416
	.long	-1076697089
	.long	-1769515679
	.long	-1132926198
	.long	-2142319610
	.long	-1078101890
	.long	776163832
	.long	-1138284997
	.long	-228534348
	.long	1070065617
	.long	-1898817309
	.long	-1136458459
	.long	-1769128702
	.long	-1079664320
	.long	-1362394018
	.long	-1078629015
	.long	-1373032555
	.long	1068710279
	.long	1222680269
	.long	1065573661
	.long	1802476234
	.long	-1079193419
	.long	0
	.long	1071218688
	.long	-1193306665
	.long	1071137826
	.long	698040758
	.long	1014855328
	.long	431645910
	.long	1072405932
	.long	-1589714785
	.long	-1135647686
	.long	-1013930551
	.long	-1076691503
	.long	-1434095668
	.long	1012355410
	.long	-1058433199
	.long	-1078150739
	.long	323461975
	.long	1011756356
	.long	-36963008
	.long	1070058536
	.long	-451486968
	.long	1012980786
	.long	428154246
	.long	-1079599303
	.long	-411957123
	.long	-1078663529
	.long	-2091281698
	.long	1068714721
	.long	-449682551
	.long	1065268209
	.long	591754728
	.long	-1079230787
	.long	0
	.long	1071235072
	.long	203341532
	.long	1071151947
	.long	-1560656113
	.long	-1133060870
	.long	-1709526152
	.long	1072401059
	.long	-1214796909
	.long	1016004016
	.long	-1107599782
	.long	-1076686059
	.long	-2043737075
	.long	1012405160
	.long	880341353
	.long	-1078199361
	.long	-639294078
	.long	-1135482864
	.long	-2032735114
	.long	1070051142
	.long	1007923545
	.long	-1134348578
	.long	642482256
	.long	-1079535907
	.long	-1642107921
	.long	-1078698150
	.long	-1817022527
	.long	1068718146
	.long	968637119
	.long	1064752990
	.long	7223474
	.long	-1079269329
	.long	0
	.long	1071251456
	.long	961157925
	.long	1071166029
	.long	-1552642136
	.long	1014721803
	.long	496819352
	.long	1072396166
	.long	878434216
	.long	-1134370260
	.long	-1655195403
	.long	-1076680757
	.long	1966916849
	.long	-1139584189
	.long	-2023640748
	.long	-1078247748
	.long	-1944678435
	.long	1012580076
	.long	2046200916
	.long	1070043442
	.long	-657367334
	.long	-1134196039
	.long	1483921212
	.long	-1079474136
	.long	1450960720
	.long	-1078732851
	.long	170196081
	.long	1068720576
	.long	1185585334
	.long	1064192890
	.long	1001208438
	.long	-1079308956
	.long	0
	.long	1071267840
	.long	385207784
	.long	1071180073
	.long	-517211397
	.long	1014284455
	.long	839011127
	.long	1072391252
	.long	1775016787
	.long	-1132886025
	.long	370805312
	.long	-1076675596
	.long	-2080422181
	.long	1014637345
	.long	647050264
	.long	-1078295889
	.long	279271934
	.long	1010584674
	.long	-934254983
	.long	1070035444
	.long	1674038467
	.long	-1135301907
	.long	-1841829889
	.long	-1079413993
	.long	-35262190
	.long	-1078767606
	.long	-1439943591
	.long	1068722031
	.long	74586375
	.long	1063157965
	.long	230822565
	.long	-1079349579
	.long	0
	.long	1071284224
	.long	2094057058
	.long	1071194078
	.long	-2012918957
	.long	1014040385
	.long	1683256604
	.long	1072386318
	.long	2114923978
	.long	-1139172355
	.long	-474971660
	.long	-1076670577
	.long	1043072277
	.long	1014854153
	.long	1063136559
	.long	-1078343776
	.long	-365178094
	.long	-1138708434
	.long	1713816474
	.long	1070027157
	.long	-1272866166
	.long	1013919954
	.long	-178389339
	.long	-1079355480
	.long	-411960809
	.long	-1078802387
	.long	-1016771916
	.long	1068722535
	.long	-1572948245
	.long	1058140962
	.long	-1964672591
	.long	-1079391112
	.long	0
	.long	1071300608
	.long	1134774535
	.long	1071208045
	.long	937471787
	.long	1013559317
	.long	1088288979
	.long	1072381365
	.long	2033979576
	.long	-1134465210
	.long	-934055401
	.long	-1076665697
	.long	940848660
	.long	1011898168
	.long	-1082789366
	.long	-1078391400
	.long	1803822663
	.long	1011730197
	.long	1229836774
	.long	1070018588
	.long	-190872140
	.long	1012843478
	.long	-606264399
	.long	-1079298597
	.long	-935300586
	.long	-1078837169
	.long	626080249
	.long	1068722111
	.long	1705826010
	.long	-1084519923
	.long	-2142046947
	.long	-1079433468
	.long	0
	.long	1071316992
	.long	1162701592
	.long	1071221973
	.long	-1996682633
	.long	1014621897
	.long	1394862954
	.long	1072376393
	.long	1892456056
	.long	-1135140350
	.long	-1932161852
	.long	-1076660956
	.long	-633165234
	.long	1013545123
	.long	1423308676
	.long	-1078438751
	.long	-1287963379
	.long	-1134650859
	.long	1739197510
	.long	1070009745
	.long	-1532191240
	.long	-1133861073
	.long	-798598586
	.long	-1079243344
	.long	-1272388851
	.long	-1078871927
	.long	323242160
	.long	1068720781
	.long	1734450332
	.long	-1083458623
	.long	1407317587
	.long	-1079476563
	.long	0
	.long	1071333376
	.long	1556447495
	.long	1071235862
	.long	35982587
	.long	1011574422
	.long	635393524
	.long	1072371403
	.long	-192010564
	.long	-1134216516
	.long	7610761
	.long	-1076656353
	.long	-1067584431
	.long	-1133356318
	.long	1842427509
	.long	-1078485822
	.long	-1726413340
	.long	1004655840
	.long	-1246227330
	.long	1070000636
	.long	-1518514486
	.long	1011238459
	.long	-2034115703
	.long	-1079189719
	.long	448319940
	.long	-1078906636
	.long	948441640
	.long	1068718569
	.long	1405010687
	.long	-1082873465
	.long	-441493322
	.long	-1079520315
	.long	0
	.long	1071349760
	.long	1712750594
	.long	1071249712
	.long	1204372378
	.long	-1132690832
	.long	1123476169
	.long	1072366395
	.long	-997784921
	.long	-1133594612
	.long	-123247817
	.long	-1076651889
	.long	1528542090
	.long	-1134335640
	.long	958475012
	.long	-1078532604
	.long	-1171956111
	.long	-1136906720
	.long	622734657
	.long	1069991270
	.long	1193501545
	.long	1008929459
	.long	-737463273
	.long	-1079137720
	.long	-997920052
	.long	-1078941274
	.long	-1665902786
	.long	1068715499
	.long	502230807
	.long	-1082440502
	.long	1943438264
	.long	-1079564641
	.long	0
	.long	1071366144
	.long	1046369035
	.long	1071263523
	.long	-141096853
	.long	1012405461
	.long	863550073
	.long	1072361370
	.long	1148641857
	.long	1014866282
	.long	1358085803
	.long	-1076647560
	.long	-1386597910
	.long	1014726271
	.long	-1509809873
	.long	-1078579090
	.long	-1631351467
	.long	-1137578987
	.long	-1555596182
	.long	1069981653
	.long	-1298571208
	.long	1012342575
	.long	-1496377811
	.long	-1079087342
	.long	-791992747
	.long	-1078975816
	.long	-1740467921
	.long	1068711596
	.long	1226434445
	.long	-1082075255
	.long	-2143821246
	.long	-1079609463
	.long	0
	.long	1071382528
	.long	-1010031619
	.long	1071277294
	.long	458274815
	.long	-1133359636
	.long	2140442718
	.long	1072356328
	.long	2140442718
	.long	1009441768
	.long	-357217543
	.long	-1076643368
	.long	1425869121
	.long	1014348157
	.long	1683652510
	.long	-1078625271
	.long	154273809
	.long	-1134659082
	.long	403257586
	.long	1069971795
	.long	-407967962
	.long	1013073650
	.long	-15818734
	.long	-1079038581
	.long	1724327180
	.long	-1079035775
	.long	-916457836
	.long	1068706884
	.long	634912585
	.long	-1081871376
	.long	-1203183151
	.long	-1079654703
	.long	0
	.long	1071398912
	.long	-711026481
	.long	1071291026
	.long	-9054185
	.long	1011016749
	.long	-1365909847
	.long	1072351270
	.long	1665002583
	.long	-1135390996
	.long	-1392878824
	.long	-1076639310
	.long	1817021735
	.long	1014660620
	.long	-454246154
	.long	-1078671141
	.long	-1060952206
	.long	-1135392368
	.long	1683088247
	.long	1069961702
	.long	714846705
	.long	-1133598700
	.long	-433339982
	.long	-1078991430
	.long	1711158850
	.long	-1079104344
	.long	151083368
	.long	1068701389
	.long	1021934118
	.long	-1081673884
	.long	-555204069
	.long	-1079700285
	.long	0
	.long	1071415296
	.long	1411515787
	.long	1071304719
	.long	949080808
	.long	1015006403
	.long	1188975055
	.long	1072346197
	.long	-607941431
	.long	-1134853302
	.long	-2075510549
	.long	-1076635386
	.long	-1136333080
	.long	1013029093
	.long	1502857950
	.long	-1078716691
	.long	-98002068
	.long	-1138533173
	.long	1625465775
	.long	1069951383
	.long	-813966834
	.long	-1133926816
	.long	862006130
	.long	-1078965293
	.long	-815612711
	.long	-1079172592
	.long	1614543927
	.long	1068695134
	.long	-1447237652
	.long	-1081482822
	.long	-1204803115
	.long	-1079746135
	.long	0
	.long	1071431680
	.long	548311876
	.long	1071318372
	.long	1177957461
	.long	-1132908231
	.long	-840932577
	.long	1072341108
	.long	339732180
	.long	1015329946
	.long	1652232539
	.long	-1076631595
	.long	858360256
	.long	1014473986
	.long	-1249291969
	.long	-1078761916
	.long	274656406
	.long	1010482633
	.long	-585965378
	.long	1069940845
	.long	1567083317
	.long	-1135672703
	.long	1111014019
	.long	-1078943317
	.long	1938330918
	.long	-1079240477
	.long	-35244774
	.long	1068688145
	.long	-1034517697
	.long	-1081298222
	.long	-146917648
	.long	-1079792182
	.long	0
	.long	1071448064
	.long	497441900
	.long	1071331985
	.long	-1312141248
	.long	1014686087
	.long	-937811372
	.long	1072336005
	.long	-1922600309
	.long	-1132215919
	.long	1048713440
	.long	-1076627936
	.long	1388135407
	.long	-1133330638
	.long	-1380584552
	.long	-1078806808
	.long	1802818641
	.long	1010041479
	.long	-1650579972
	.long	1069930097
	.long	1368226417
	.long	1013159529
	.long	432810400
	.long	-1078922134
	.long	-1046046896
	.long	-1079307961
	.long	746745451
	.long	1068680449
	.long	-876498554
	.long	-1081120107
	.long	770218001
	.long	-1079838355
	.long	0
	.long	1071464448
	.long	779321084
	.long	1071345558
	.long	1687690865
	.long	-1134779084
	.long	-1190122020
	.long	1072330888
	.long	1011275733
	.long	-1133014606
	.long	340062133
	.long	-1076624408
	.long	1329406630
	.long	-1134540859
	.long	-1190477364
	.long	-1078851361
	.long	757143000
	.long	1010858161
	.long	1534878114
	.long	1069919146
	.long	735236506
	.long	1010757482
	.long	-1467675110
	.long	-1078901740
	.long	-967310525
	.long	-1079375005
	.long	1215746015
	.long	1068672069
	.long	1445791785
	.long	-1081015173
	.long	-1101649277
	.long	-1079884589
	.long	0
	.long	1071480832
	.long	931538085
	.long	1071359091
	.long	-1267840257
	.long	1014307233
	.long	591976867
	.long	1072325758
	.long	-1856576097
	.long	-1132136766
	.long	-462730095
	.long	-1076621011
	.long	286253239
	.long	-1132936931
	.long	283366770
	.long	-1078895568
	.long	1143455755
	.long	1012118619
	.long	-1027307858
	.long	1069907999
	.long	-1039340149
	.long	-1136018594
	.long	1289613157
	.long	-1078882128
	.long	719633616
	.long	-1079441572
	.long	-1231841607
	.long	1068663031
	.long	-837313333
	.long	-1080932616
	.long	-831612117
	.long	-1079930817
	.long	0
	.long	1071497216
	.long	508723602
	.long	1071372584
	.long	967797606
	.long	-1133203116
	.long	-2008579069
	.long	1072320614
	.long	-1099865585
	.long	-1132083224
	.long	-1271860052
	.long	-1076617742
	.long	-409176683
	.long	-1135223934
	.long	-1317819274
	.long	-1078939425
	.long	-462189275
	.long	1012397120
	.long	1905891179
	.long	1069896665
	.long	713501202
	.long	1012177164
	.long	-784868095
	.long	-1078863294
	.long	889978293
	.long	-1079507628
	.long	-622206593
	.long	1068653361
	.long	-371038796
	.long	-1080853307
	.long	1024893442
	.long	-1079976976
	.long	0
	.long	1071513600
	.long	-917583376
	.long	1071386036
	.long	1888701492
	.long	-1132674610
	.long	1753749656
	.long	1072315458
	.long	-440156777
	.long	1016041599
	.long	-1925707923
	.long	-1076614601
	.long	2115622445
	.long	-1133804716
	.long	1513550269
	.long	-1078982924
	.long	-902198263
	.long	-1136562146
	.long	-148760022
	.long	1069885150
	.long	-2143214466
	.long	-1138681752
	.long	1739596442
	.long	-1078845230
	.long	-951251866
	.long	-1079573139
	.long	267678434
	.long	1068643085
	.long	-1665711594
	.long	-1080777239
	.long	-1725194721
	.long	-1080023007
	.long	0
	.long	1071529984
	.long	535895735
	.long	1071399449
	.long	1129004782
	.long	-1132783649
	.long	1132118174
	.long	1072310290
	.long	-111047647
	.long	-1135344792
	.long	2103157766
	.long	-1076611587
	.long	1577743146
	.long	1014325339
	.long	484491524
	.long	-1079067417
	.long	-722783593
	.long	-1136366334
	.long	-762710517
	.long	1069873463
	.long	-415827317
	.long	1012631735
	.long	-1510502988
	.long	-1078827931
	.long	1638494939
	.long	-1079638071
	.long	-1642876792
	.long	1068632226
	.long	113456723
	.long	-1080704401
	.long	878852017
	.long	-1080104420
	.long	0
	.long	1071546368
	.long	179139065
	.long	1071412821
	.long	-9419804
	.long	-1133032565
	.long	-1753235344
	.long	1072305110
	.long	-649852799
	.long	-1132018607
	.long	-1769804062
	.long	-1076608699
	.long	-1321664076
	.long	1014240623
	.long	727244403
	.long	-1079152955
	.long	-547014264
	.long	1009577254
	.long	1913565746
	.long	1069861611
	.long	-1829505015
	.long	1013006918
	.long	-2127779199
	.long	-1078811389
	.long	584851623
	.long	-1079702394
	.long	-1285692003
	.long	1068620811
	.long	1255967800
	.long	-1080634779
	.long	893668876
	.long	-1080195621
	.long	0
	.long	1071562752
	.long	1928553243
	.long	1071426152
	.long	-2124189880
	.long	1009340519
	.long	-505183781
	.long	1072299919
	.long	7347740
	.long	-1133510682
	.long	-294355796
	.long	-1076605936
	.long	-824966739
	.long	-1132822750
	.long	-124199386
	.long	-1079237748
	.long	-626535633
	.long	1008644497
	.long	840306242
	.long	1069849601
	.long	63963708
	.long	1013227266
	.long	1236946871
	.long	-1078795597
	.long	-905914681
	.long	-1079766079
	.long	1539610063
	.long	1068608865
	.long	1674641257
	.long	-1080568353
	.long	665808912
	.long	-1080286229
	.long	0
	.long	1071579136
	.long	1126962001
	.long	1071439443
	.long	1142485421
	.long	-1132843782
	.long	-1629828404
	.long	1072294718
	.long	-2016265051
	.long	-1131855163
	.long	-1633015715
	.long	-1076603296
	.long	1389331892
	.long	1011869926
	.long	674339571
	.long	-1079321784
	.long	833902256
	.long	-1138721978
	.long	1548675010
	.long	1069837440
	.long	-1270596490
	.long	1011605441
	.long	-1489255617
	.long	-1078780548
	.long	-1197002251
	.long	-1079829096
	.long	2039444609
	.long	1068596412
	.long	-109581878
	.long	-1080505101
	.long	921310584
	.long	-1080376142
	.long	0
	.long	1071595520
	.long	1723331245
	.long	1071452693
	.long	898133040
	.long	1014752888
	.long	1233068013
	.long	1072289507
	.long	-1707752364
	.long	-1131495338
	.long	-1004185208
	.long	-1076600779
	.long	1900298876
	.long	1014881147
	.long	-374060339
	.long	-1079405057
	.long	-1255551131
	.long	1010605398
	.long	651975898
	.long	1069825136
	.long	-35205758
	.long	1013804774
	.long	-1805143142
	.long	-1078766233
	.long	-179527238
	.long	-1079891419
	.long	-1104323283
	.long	1068583477
	.long	899894900
	.long	-1080444995
	.long	-996899703
	.long	-1080465263
	.long	0
	.long	1071611904
	.long	-907246037
	.long	1071465902
	.long	373225773
	.long	1013486625
	.long	1540051655
	.long	1072284286
	.long	-246897840
	.long	-1131860591
	.long	2135146480
	.long	-1076598383
	.long	-1918813145
	.long	-1133278429
	.long	-101605939
	.long	-1079487556
	.long	607372751
	.long	1009690537
	.long	-1282729504
	.long	1069812695
	.long	472524899
	.long	-1134737810
	.long	1526936487
	.long	-1078752644
	.long	775460567
	.long	-1079953021
	.long	-1554287291
	.long	1068570085
	.long	-1727429290
	.long	-1080388008
	.long	1765459358
	.long	-1080553498
	.long	0
	.long	1071628288
	.long	1511139061
	.long	1071479071
	.long	-1514780042
	.long	-1134296706
	.long	1318599401
	.long	1072279056
	.long	1387758551
	.long	-1131592211
	.long	-208369377
	.long	-1076596108
	.long	585139987
	.long	-1132669832
	.long	-1538967651
	.long	-1079569273
	.long	-2095216713
	.long	1008036266
	.long	252754998
	.long	1069800126
	.long	481934310
	.long	1004068651
	.long	-1881286558
	.long	-1078739773
	.long	-1128910119
	.long	-1080013880
	.long	1688366455
	.long	1068556260
	.long	-801101871
	.long	-1080334106
	.long	-141376039
	.long	-1080640761
	.long	0
	.long	1071644672
	.long	90291023
	.long	1071492199
	.long	573531618
	.long	1014639487
	.long	-1717986918
	.long	1072273817
	.long	-1717986918
	.long	-1131832935
	.long	1202590843
	.long	-1076593951
	.long	-343597384
	.long	1012840529
	.long	-1007885659
	.long	-1079650201
	.long	-675741521
	.long	-1137031076
	.long	810889825
	.long	1069787434
	.long	-1807322238
	.long	1013909238
	.long	249039384
	.long	-1078727610
	.long	1364035800
	.long	-1080114661
	.long	-114710477
	.long	1068542025
	.long	-1998604276
	.long	-1080283253
	.long	-2109358327
	.long	-1080726967
	.long	0
	.long	1071652864
	.long	-1157469836
	.long	1071505285
	.long	201986598
	.long	1013201851
	.long	-1285871721
	.long	1072268570
	.long	413728429
	.long	1015663892
	.long	-1526453715
	.long	-1076591913
	.long	-1474147050
	.long	1007533060
	.long	-982852094
	.long	-1079730332
	.long	1205501674
	.long	1011162706
	.long	-138019203
	.long	1069774626
	.long	-768849060
	.long	-1135516226
	.long	-151745443
	.long	-1078716148
	.long	1490075954
	.long	-1080233265
	.long	232775028
	.long	1068527406
	.long	1189499702
	.long	-1080235411
	.long	1680060219
	.long	-1080812039
	.long	0
	.long	1071661056
	.long	1795638448
	.long	1071518331
	.long	1670960299
	.long	-1135431528
	.long	289262682
	.long	1072263316
	.long	1307100533
	.long	-1132189824
	.long	935246053
	.long	-1076589991
	.long	-1647881186
	.long	-1134551598
	.long	-1477269250
	.long	-1079809659
	.long	1949815853
	.long	-1136270316
	.long	783498710
	.long	1069761711
	.long	-475850436
	.long	-1133680919
	.long	-1502203342
	.long	-1078705375
	.long	1914167241
	.long	-1080350250
	.long	-481178
	.long	1068512423
	.long	980325149
	.long	-1080190539
	.long	1251966489
	.long	-1080895903
	.long	0
	.long	1071669248
	.long	107803930
	.long	1071531336
	.long	315529458
	.long	1011065770
	.long	661978707
	.long	1072258054
	.long	998577170
	.long	1015090685
	.long	781262403
	.long	-1076588185
	.long	155611085
	.long	-1132501754
	.long	-18894655
	.long	-1079888176
	.long	2043372457
	.long	-1136695184
	.long	-2034870756
	.long	1069748693
	.long	-1790706534
	.long	1012275586
	.long	-1224039001
	.long	-1078695283
	.long	-173269252
	.long	-1080465578
	.long	-1350794061
	.long	1068495261
	.long	365860536
	.long	-1080148593
	.long	1114791969
	.long	-1080978490
	.long	0
	.long	1071677440
	.long	2132236852
	.long	1071544299
	.long	-1044433867
	.long	1014031677
	.long	1761889188
	.long	1072252785
	.long	-1831470143
	.long	-1132637214
	.long	-1164837495
	.long	-1076586494
	.long	-1010252557
	.long	1014076990
	.long	-214612267
	.long	-1079965876
	.long	-954543480
	.long	1011597974
	.long	-1725815195
	.long	1069735580
	.long	685431652
	.long	1013860880
	.long	-100459343
	.long	-1078685862
	.long	-1140038624
	.long	-1080579212
	.long	-2022895755
	.long	1068463986
	.long	-466486122
	.long	-1080109528
	.long	-968550932
	.long	-1081059736
	.long	0
	.long	1071685632
	.long	-942724924
	.long	1071557221
	.long	-324813051
	.long	1013119788
	.long	1203571370
	.long	1072247510
	.long	1769696828
	.long	1013702636
	.long	252946408
	.long	-1076584915
	.long	541141312
	.long	1013029061
	.long	-1974562081
	.long	-1080052226
	.long	1661576474
	.long	-1136870568
	.long	-428042199
	.long	1069722378
	.long	111461615
	.long	-1134266813
	.long	1956366097
	.long	-1078677101
	.long	666825317
	.long	-1080691119
	.long	674528959
	.long	1068432123
	.long	-524259793
	.long	-1080073294
	.long	-1636065833
	.long	-1081197303
	.long	0
	.long	1071693824
	.long	-734034891
	.long	1071570102
	.long	856196324
	.long	-1134573194
	.long	876360165
	.long	1072242229
	.long	-1682348135
	.long	-1131424657
	.long	1635588513
	.long	-1076583449
	.long	-363285936
	.long	-1132629245
	.long	-1688874666
	.long	-1080204326
	.long	-709571476
	.long	-1139490958
	.long	-704550276
	.long	1069709094
	.long	1130284212
	.long	1011792418
	.long	1548923846
	.long	-1078668991
	.long	803053501
	.long	-1080801269
	.long	1832580278
	.long	1068399715
	.long	1314992475
	.long	-1080039841
	.long	-828149117
	.long	-1081354077
	.long	0
	.long	1071702016
	.long	-1728867376
	.long	1071582942
	.long	-2016733036
	.long	-1133317110
	.long	-1645717880
	.long	1072236942
	.long	-1552343452
	.long	-1132712479
	.long	-383545641
	.long	-1076582094
	.long	679887750
	.long	-1134059077
	.long	1961064
	.long	-1080354760
	.long	-1894777891
	.long	-1137447410
	.long	-1255272443
	.long	1069695734
	.long	924832449
	.long	-1135315221
	.long	323936045
	.long	-1078661521
	.long	1796456140
	.long	-1080909633
	.long	1695516060
	.long	1068366806
	.long	672499444
	.long	-1080021199
	.long	2054374558
	.long	-1081507834
	.long	0
	.long	1071710208
	.long	190059544
	.long	1071595741
	.long	-600298973
	.long	-1133889134
	.long	-219164363
	.long	1072231650
	.long	-735999727
	.long	-1133198400
	.long	-551243055
	.long	-1076580848
	.long	2043718711
	.long	1014053650
	.long	-684302529
	.long	-1080503521
	.long	1649614277
	.long	-1139228869
	.long	-1219070364
	.long	1069682304
	.long	-732948097
	.long	-1135303015
	.long	618139101
	.long	-1078654681
	.long	453318315
	.long	-1081016184
	.long	1245013351
	.long	1068333438
	.long	-515417875
	.long	-1080007176
	.long	1943205563
	.long	-1081658481
	.long	0
	.long	1071718400
	.long	564454549
	.long	1071608498
	.long	259962688
	.long	1012173250
	.long	-1605957583
	.long	1072226354
	.long	-413830963
	.long	-1131670198
	.long	2118216757
	.long	-1076579710
	.long	1617193725
	.long	-1132521417
	.long	2053893159
	.long	-1080650598
	.long	-424182871
	.long	1010312079
	.long	-180188063
	.long	1069668810
	.long	-847740502
	.long	1013618317
	.long	1105202030
	.long	-1078648460
	.long	1881854942
	.long	-1081159942
	.long	2089476316
	.long	1068299652
	.long	-1968943192
	.long	-1079994462
	.long	785789960
	.long	-1081805932
	.long	0
	.long	1071726592
	.long	-754806183
	.long	1071621213
	.long	-601302859
	.long	-1132681885
	.long	296148608
	.long	1072221054
	.long	-1407010568
	.long	1012992998
	.long	45697162
	.long	-1076578679
	.long	-1349397438
	.long	1012834493
	.long	-2111843970
	.long	-1080795985
	.long	240230473
	.long	1010361444
	.long	1825856173
	.long	1069655259
	.long	-199226018
	.long	1008896224
	.long	1033754564
	.long	-1078642847
	.long	-82807721
	.long	-1081365656
	.long	2068245457
	.long	1068265489
	.long	-1827452273
	.long	-1079983030
	.long	-235256439
	.long	-1081950111
	.long	0
	.long	1071734784
	.long	392159107
	.long	1071633888
	.long	516201237
	.long	1013164155
	.long	-1316272311
	.long	1072215749
	.long	-1210741750
	.long	-1134580808
	.long	-1440315124
	.long	-1076577755
	.long	251246216
	.long	1011934101
	.long	-928402240
	.long	-1080939675
	.long	-753331089
	.long	1009268206
	.long	11960765
	.long	1069641656
	.long	663827376
	.long	1008596531
	.long	171655436
	.long	-1078637831
	.long	-337557886
	.long	-1081567614
	.long	-544612218
	.long	1068230988
	.long	240494328
	.long	-1079972850
	.long	145713395
	.long	-1082090946
	.long	0
	.long	1071742976
	.long	1942070284
	.long	1071645596
	.long	1237964179
	.long	-1131728183
	.long	-382599981
	.long	1072210441
	.long	-1905735348
	.long	1013758192
	.long	-1285893416
	.long	-1076576935
	.long	-895088972
	.long	-1134449638
	.long	1841510895
	.long	-1081081661
	.long	-1364284813
	.long	1009541858
	.long	2014506541
	.long	1069628006
	.long	-1802502565
	.long	1013615810
	.long	-1248502628
	.long	-1078633402
	.long	265467139
	.long	-1081765781
	.long	1467867866
	.long	1068196189
	.long	-1555222036
	.long	-1079963894
	.long	-270234928
	.long	-1082326325
	.long	0
	.long	1071751168
	.long	511853101
	.long	1071651892
	.long	-53833490
	.long	1015274308
	.long	546921096
	.long	1072205131
	.long	507700865
	.long	1015031671
	.long	1581024271
	.long	-1076576218
	.long	939541005
	.long	1014856805
	.long	-1275558141
	.long	-1081362021
	.long	1274429058
	.long	1008689900
	.long	2119275502
	.long	1069614316
	.long	-1159405443
	.long	-1138264290
	.long	1712342293
	.long	-1078629547
	.long	102375633
	.long	-1081960128
	.long	-1576848167
	.long	1068161128
	.long	2059046630
	.long	-1079956130
	.long	1769425805
	.long	-1082594273
	.long	0
	.long	1071759360
	.long	153420936
	.long	1071658167
	.long	-1920642032
	.long	1014561203
	.long	-1098950325
	.long	1072199817
	.long	-936974429
	.long	-1133170463
	.long	-341528704
	.long	-1076575604
	.long	-425107542
	.long	1014039721
	.long	-2073183728
	.long	-1081639149
	.long	1253900304
	.long	1009502500
	.long	-1562443873
	.long	1069600591
	.long	-1609318379
	.long	-1134046850
	.long	1467906201
	.long	-1078626256
	.long	-1264672403
	.long	-1082170824
	.long	-1964725978
	.long	1068125843
	.long	779613747
	.long	-1079949528
	.long	-1206089410
	.long	-1082855213
	.long	0
	.long	1071767552
	.long	826491622
	.long	1071664421
	.long	1682098023
	.long	-1132345245
	.long	677424510
	.long	1072194502
	.long	326774159
	.long	-1132340046
	.long	-1656897478
	.long	-1076575090
	.long	427498937
	.long	-1132621137
	.long	1448325543
	.long	-1081912842
	.long	556261031
	.long	-1142724428
	.long	1492815012
	.long	1069586837
	.long	1392781797
	.long	-1136368751
	.long	-669243669
	.long	-1078623518
	.long	1046724873
	.long	-1082544083
	.long	-409976172
	.long	1068090369
	.long	-1187794701
	.long	-1079944058
	.long	-656908063
	.long	-1083109065
	.long	0
	.long	1071775744
	.long	-1797574448
	.long	1071670654
	.long	1600342502
	.long	-1132635319
	.long	-1032321516
	.long	1072189184
	.long	-12744710
	.long	-1132468304
	.long	-1251817947
	.long	-1076574676
	.long	-1483832180
	.long	1014477507
	.long	-1281049417
	.long	-1082235755
	.long	-470634331
	.long	1008003977
	.long	-141816380
	.long	1069573058
	.long	1966726163
	.long	-1133793808
	.long	1168028739
	.long	-1078621320
	.long	327607988
	.long	-1082909565
	.long	-1806698746
	.long	1068054742
	.long	1722630199
	.long	-1079939687
	.long	33321834
	.long	-1083532518
	.long	0
	.long	1071783936
	.long	844012344
	.long	1071676867
	.long	-532393812
	.long	1014722761
	.long	-272798722
	.long	1072183865
	.long	569761318
	.long	-1132779387
	.long	1996450974
	.long	-1076574360
	.long	-558098941
	.long	1012310720
	.long	-640888873
	.long	-1082769364
	.long	1318553281
	.long	-1139066432
	.long	-1191135878
	.long	1069559261
	.long	-360144363
	.long	1011210518
	.long	173583168
	.long	-1078619652
	.long	849574601
	.long	-1083355472
	.long	1912501752
	.long	1068018995
	.long	-998791083
	.long	-1079936385
	.long	721637393
	.long	-1084011506
	.long	0
	.long	1071792128
	.long	140617104
	.long	1071683059
	.long	169191923
	.long	-1133436494
	.long	300269901
	.long	1072178546
	.long	-1395127543
	.long	-1132594692
	.long	628136959
	.long	-1076574141
	.long	395375748
	.long	1014669046
	.long	-1978798284
	.long	-1083413132
	.long	713114327
	.long	-1140515241
	.long	1985877355
	.long	1069543381
	.long	-253584226
	.long	-1136762808
	.long	-1703784920
	.long	-1078618503
	.long	-820780654
	.long	-1084055163
	.long	1681113407
	.long	1067983161
	.long	-1824331427
	.long	-1079934119
	.long	-1885912825
	.long	-1084724428
	.long	0
	.long	1071800320
	.long	372868716
	.long	1071689230
	.long	-220314534
	.long	1014003115
	.long	-1990041763
	.long	1072173225
	.long	265253474
	.long	-1131525142
	.long	73778843
	.long	-1076574018
	.long	-671614820
	.long	1011232500
	.long	1306923733
	.long	-1084677874
	.long	-1168930419
	.long	-1141623207
	.long	-24941463
	.long	1069515741
	.long	670386586
	.long	-1137259690
	.long	1899425242
	.long	-1078617860
	.long	81847
	.long	-1085250740
	.long	1340430274
	.long	1067947272
	.long	-1196539428
	.long	-1079932858
	.long	-479473095
	.long	-1085972530
	.long	0
	.long	1071808512
	.long	1532707802
	.long	1071695380
	.long	330645583
	.long	1012495610
	.long	-1251945636
	.long	1072167904
	.long	-1369831289
	.long	-1132138394
	.long	1472389579
	.long	-1076573990
	.long	-526158004
	.long	1014226446
	.long	1640406844
	.long	1060680513
	.long	2065695601
	.long	1003991778
	.long	-749606675
	.long	1069488094
	.long	1695308444
	.long	1012649528
	.long	242688844
	.long	-1078617712
	.long	-1711958802
	.long	1060112111
	.long	443918028
	.long	1067911359
	.long	832860629
	.long	-1079932569
	.long	1505690913
	.long	1059376254
	.long	0
	.long	1071816704
	.long	-676696195
	.long	1071701509
	.long	-1312478805
	.long	-1132972412
	.long	-204818300
	.long	1072162583
	.long	991929543
	.long	1014897441
	.long	1669532491
	.long	-1076574055
	.long	-1371537665
	.long	-1132761550
	.long	-66663345
	.long	1063421783
	.long	1458577254
	.long	-1145781931
	.long	-2077954088
	.long	1069460449
	.long	1851131114
	.long	1004491049
	.long	-201326748
	.long	-1078618049
	.long	525746569
	.long	1062726660
	.long	-1456574872
	.long	1067875451
	.long	88264959
	.long	-1079933221
	.long	295926750
	.long	1062134343
	.long	0
	.long	1071824896
	.long	-1956126059
	.long	1071707618
	.long	2127139024
	.long	1004736945
	.long	-1589268647
	.long	1072157263
	.long	-1411685829
	.long	-1132214575
	.long	1805376304
	.long	-1076574212
	.long	-742672309
	.long	1014771630
	.long	-953568410
	.long	1064362277
	.long	2048497980
	.long	1007849979
	.long	1724845528
	.long	1069432815
	.long	-849447360
	.long	-1135335241
	.long	-1557709791
	.long	-1078618857
	.long	914954955
	.long	1063628239
	.long	-517881833
	.long	1067839578
	.long	944483685
	.long	-1079934782
	.long	-753007961
	.long	1062983409
	.long	0
	.long	1071833088
	.long	1999665677
	.long	1071713706
	.long	-733185275
	.long	1015454771
	.long	422801721
	.long	1072151944
	.long	15169519
	.long	-1132473784
	.long	-1277074844
	.long	-1076574460
	.long	-730934045
	.long	-1132538080
	.long	1656871671
	.long	1064854437
	.long	818709201
	.long	1008033395
	.long	-1750973990
	.long	1069405201
	.long	-1698410787
	.long	1009230170
	.long	-1704169050
	.long	-1078620126
	.long	-1252831554
	.long	1064249434
	.long	-1486013193
	.long	1067803768
	.long	-1054601802
	.long	-1079937220
	.long	949069580
	.long	1063511457
	.long	0
	.long	1071841280
	.long	-1677995351
	.long	1071719773
	.long	-507449803
	.long	-1135428178
	.long	-1246625887
	.long	1072146625
	.long	1308647075
	.long	1015496266
	.long	2146096568
	.long	-1076574797
	.long	1437319770
	.long	1014470030
	.long	31770992
	.long	1065339697
	.long	-986480678
	.long	-1139091096
	.long	-108794639
	.long	1069377616
	.long	49113360
	.long	-1136769562
	.long	1395607990
	.long	-1078621844
	.long	-353382529
	.long	1064579810
	.long	-505726736
	.long	1067768047
	.long	2086444527
	.long	-1079940502
	.long	2070930896
	.long	1063888706
	.long	0
	.long	1071849472
	.long	-82115218
	.long	1071725819
	.long	1875111224
	.long	1015222542
	.long	-811864468
	.long	1072141308
	.long	-656860308
	.long	-1131558958
	.long	318235935
	.long	-1076575222
	.long	224225389
	.long	1014832656
	.long	-2086852358
	.long	1065585640
	.long	1376982592
	.long	1007895162
	.long	906755797
	.long	1069350070
	.long	-1552983649
	.long	-1134674764
	.long	1065002083
	.long	-1078624000
	.long	-1217451072
	.long	1064874782
	.long	2010999680
	.long	1067732442
	.long	566574249
	.long	-1079944597
	.long	1826201300
	.long	1064251559
	.long	0
	.long	1071857664
	.long	-1774754710
	.long	1071731845
	.long	1418458051
	.long	-1132152196
	.long	-1098332066
	.long	1072135993
	.long	-1687727928
	.long	-1133864897
	.long	-1344880923
	.long	-1076575735
	.long	1584654650
	.long	1014833647
	.long	-61510489
	.long	1065821383
	.long	1122330430
	.long	-1138503950
	.long	-1112607107
	.long	1069322569
	.long	1057291484
	.long	-1138862948
	.long	-941319830
	.long	-1078626583
	.long	-1167266085
	.long	1065161980
	.long	1406983242
	.long	1067696977
	.long	973755297
	.long	-1079949474
	.long	1645225497
	.long	1064452374
	.long	0
	.long	1071865856
	.long	1867593697
	.long	1071737850
	.long	-515785419
	.long	1014587503
	.long	-657617563
	.long	1072130680
	.long	-571345121
	.long	1015368345
	.long	-1731543488
	.long	-1076576333
	.long	-1627810408
	.long	-1134435763
	.long	948856932
	.long	1066053693
	.long	-1019162349
	.long	1008796993
	.long	-940421436
	.long	1069295123
	.long	173649713
	.long	1012139050
	.long	1233337025
	.long	-1078629580
	.long	586599720
	.long	1065397327
	.long	1692057939
	.long	1067661676
	.long	349888306
	.long	-1079955101
	.long	565112359
	.long	1064619552
	.long	0
	.long	1071874048
	.long	-2000782317
	.long	1071743834
	.long	-335494399
	.long	1015833116
	.long	1937590730
	.long	1072125370
	.long	-724876090
	.long	-1131758861
	.long	259245235
	.long	-1076577015
	.long	-1449955829
	.long	1010736831
	.long	-1994373249
	.long	1066282575
	.long	56680457
	.long	-1140291101
	.long	1403711392
	.long	1069267740
	.long	-1593803905
	.long	-1134634202
	.long	334886425
	.long	-1078632981
	.long	-428510178
	.long	1065533204
	.long	-1604448577
	.long	1067626562
	.long	-1080703551
	.long	-1079961448
	.long	-467538351
	.long	1064779695
	.long	0
	.long	1071882240
	.long	-450256998
	.long	1071749797
	.long	744028213
	.long	-1131782036
	.long	-496384589
	.long	1072120062
	.long	1283608732
	.long	1015496447
	.long	1421229433
	.long	-1076577781
	.long	1975316223
	.long	-1135487350
	.long	-1650770256
	.long	1066454915
	.long	1755826735
	.long	-1139772519
	.long	658849077
	.long	1069240427
	.long	12451549
	.long	1009937206
	.long	1736666351
	.long	-1078636775
	.long	1451153128
	.long	1065665253
	.long	78218216
	.long	1067591658
	.long	-58924497
	.long	-1079968483
	.long	805218077
	.long	1064932876
	.long	0
	.long	1071890432
	.long	-2020588923
	.long	1071755740
	.long	-1964011735
	.long	-1134420340
	.long	2015644762
	.long	1072114758
	.long	-429591646
	.long	1015581750
	.long	-1465660881
	.long	-1076578629
	.long	505363993
	.long	-1133017197
	.long	-928786245
	.long	1066565942
	.long	336441116
	.long	1010681048
	.long	-787063053
	.long	1069213191
	.long	1661306732
	.long	-1134575702
	.long	1934333117
	.long	-1078640950
	.long	2084933297
	.long	1065793496
	.long	-1690955945
	.long	1067556983
	.long	980150973
	.long	-1079976175
	.long	-1354572665
	.long	1065079170
	.long	0
	.long	1071898624
	.long	1933702735
	.long	1071761662
	.long	-1654752982
	.long	1014139836
	.long	-2046916046
	.long	1072109457
	.long	-390352688
	.long	1016010173
	.long	1248106430
	.long	-1076579557
	.long	2015979767
	.long	1014198819
	.long	1577857314
	.long	1066675270
	.long	-2042483949
	.long	-1137450390
	.long	-1481221704
	.long	1069186041
	.long	624433542
	.long	-1136861341
	.long	1401809733
	.long	-1078645495
	.long	-1597901787
	.long	1065917960
	.long	2070770406
	.long	1067522559
	.long	-1928255365
	.long	-1079984496
	.long	-1125631276
	.long	1065218663
	.long	0
	.long	1071906816
	.long	-1411449164
	.long	1071767563
	.long	679110287
	.long	-1135007521
	.long	1544257904
	.long	1072104160
	.long	1544257904
	.long	1014432480
	.long	2015451766
	.long	-1076580565
	.long	-1422257192
	.long	-1135076306
	.long	-370890288
	.long	1066782903
	.long	-335994937
	.long	-1137008598
	.long	-899507902
	.long	1069158983
	.long	-1035609213
	.long	-1136224742
	.long	268666292
	.long	-1078650399
	.long	845246168
	.long	1066038674
	.long	-845077958
	.long	1067488404
	.long	-1515863471
	.long	-1079993415
	.long	-192292678
	.long	1065351445
	.long	0
	.long	1071915008
	.long	894899082
	.long	1071773444
	.long	-153625273
	.long	1014416985
	.long	1226871870
	.long	1072098867
	.long	1638114625
	.long	-1134398914
	.long	1861087298
	.long	-1076581651
	.long	-413786412
	.long	1012609914
	.long	1752786947
	.long	1066888849
	.long	1154603923
	.long	-1139900305
	.long	560498213
	.long	1069132025
	.long	-1309932204
	.long	1012138373
	.long	-1706247071
	.long	-1078655652
	.long	-1425284604
	.long	1066155667
	.long	-835098463
	.long	1067454537
	.long	-870108742
	.long	-1080002903
	.long	-501163138
	.long	1065415414
	.long	0
	.long	1071923200
	.long	333980594
	.long	1071779304
	.long	1407956804
	.long	-1131965993
	.long	-1697209347
	.long	1072093578
	.long	-991520670
	.long	-1132429040
	.long	1790224285
	.long	-1076582814
	.long	-1614689237
	.long	1012067621
	.long	1122425896
	.long	1066993113
	.long	-476350713
	.long	-1138586503
	.long	1587005172
	.long	1069105172
	.long	-126381566
	.long	-1135429598
	.long	-864931756
	.long	-1078661242
	.long	1988438033
	.long	1066268973
	.long	-1573786049
	.long	1067391583
	.long	-655143305
	.long	-1080012931
	.long	-2140116428
	.long	1065475242
	.long	0
	.long	1071931392
	.long	1276975402
	.long	1071785143
	.long	-125148988
	.long	1015901501
	.long	-1651817236
	.long	1072088294
	.long	1322274498
	.long	-1135043236
	.long	-1507710456
	.long	-1076584053
	.long	-746160707
	.long	-1134114535
	.long	1270196384
	.long	1067095702
	.long	-1837386307
	.long	1010198926
	.long	-38024074
	.long	1069078431
	.long	1325101473
	.long	1009088701
	.long	1735975779
	.long	-1078667158
	.long	-651156723
	.long	1066378625
	.long	-1973280342
	.long	1067325102
	.long	790116722
	.long	-1080023470
	.long	1045462863
	.long	1065531867
	.long	0
	.long	1071939584
	.long	-489906582
	.long	1071790961
	.long	-1623711154
	.long	1013727772
	.long	-1671297643
	.long	1072083015
	.long	131592386
	.long	1015572447
	.long	1519519879
	.long	-1076585366
	.long	935117875
	.long	-1133099705
	.long	-1149347535
	.long	1067196623
	.long	1303822066
	.long	-1136818443
	.long	1159481949
	.long	1069051810
	.long	363836716
	.long	-1136176830
	.long	302585818
	.long	-1078673390
	.long	1805402029
	.long	1066443226
	.long	1887781861
	.long	1067259295
	.long	1838324686
	.long	-1080035707
	.long	-801760666
	.long	1065585344
	.long	0
	.long	1071947776
	.long	-585636607
	.long	1071796759
	.long	-1944361060
	.long	1015376552
	.long	-515520163
	.long	1072077741
	.long	1443169389
	.long	-1131990441
	.long	-1074009944
	.long	-1076586753
	.long	-944480657
	.long	1010214341
	.long	764761835
	.long	1067295885
	.long	410100125
	.long	1010823400
	.long	1174683458
	.long	1069025313
	.long	-429863007
	.long	1012207340
	.long	1462216907
	.long	-1078679928
	.long	236187580
	.long	1066494454
	.long	-1248769571
	.long	1067194191
	.long	-1325120291
	.long	-1080058667
	.long	65609177
	.long	1065635733
	.long	0
	.long	1071955968
	.long	1080651174
	.long	1071802537
	.long	-1605602356
	.long	1012732744
	.long	-1259719278
	.long	1072072473
	.long	143577732
	.long	-1132540026
	.long	216092797
	.long	-1076588211
	.long	1308567878
	.long	-1134292753
	.long	-1665171785
	.long	1067393494
	.long	-357902366
	.long	1010416674
	.long	-582012816
	.long	1068998946
	.long	486425775
	.long	1011931472
	.long	-1526626081
	.long	-1078686761
	.long	1903518212
	.long	1066543911
	.long	135308090
	.long	1067129819
	.long	-251587821
	.long	-1080082486
	.long	-1881082725
	.long	1065683091
	.long	0
	.long	1071964160
	.long	309580211
	.long	1071808294
	.long	1632755103
	.long	1015299059
	.long	1590505577
	.long	1072067211
	.long	1402938367
	.long	1013399931
	.long	1983647720
	.long	-1076589741
	.long	1613207388
	.long	-1134069222
	.long	804773226
	.long	1067469914
	.long	1231160040
	.long	-1136623474
	.long	-1275314308
	.long	1068972716
	.long	-2035215105
	.long	1011570891
	.long	1271468617
	.long	-1078693878
	.long	-282816264
	.long	1066591618
	.long	1820808576
	.long	1067066203
	.long	-446900842
	.long	-1080107111
	.long	-468952552
	.long	1065727481
	.long	0
	.long	1071972352
	.long	1496354353
	.long	1071814030
	.long	-1329151865
	.long	-1133470093
	.long	624455996
	.long	1072061955
	.long	1119916387
	.long	-1134554222
	.long	795690357
	.long	-1076591340
	.long	1164488511
	.long	-1137351871
	.long	-1819869923
	.long	1067517079
	.long	587875631
	.long	-1135970966
	.long	1069990272
	.long	1068946628
	.long	-616570569
	.long	1012576953
	.long	2091175888
	.long	-1078701270
	.long	-1863639760
	.long	1066637597
	.long	1175859281
	.long	1067003369
	.long	770209023
	.long	-1080132490
	.long	-66315086
	.long	1065768966
	.long	0
	.long	1071980544
	.long	450809971
	.long	1071819746
	.long	28115502
	.long	1015818432
	.long	1296245111
	.long	1072056705
	.long	90055692
	.long	1015280667
	.long	1781333249
	.long	-1076593008
	.long	-246230912
	.long	1013089669
	.long	-68948578
	.long	1067563431
	.long	-228983292
	.long	-1139236731
	.long	-1010564747
	.long	1068920686
	.long	-1325553822
	.long	1010948106
	.long	1219454010
	.long	-1078708926
	.long	95254098
	.long	1066681869
	.long	1599449894
	.long	1066941339
	.long	1273072419
	.long	-1080158574
	.long	854021539
	.long	1065807611
	.long	0
	.long	1071988736
	.long	1577206886
	.long	1071825441
	.long	-633713695
	.long	-1136347120
	.long	450071968
	.long	1072051462
	.long	-1796467536
	.long	-1134038742
	.long	1450862162
	.long	-1076594743
	.long	-1145446520
	.long	1014753052
	.long	355839161
	.long	1067608976
	.long	-16509868
	.long	-1137639572
	.long	1359337722
	.long	1068894897
	.long	1059939474
	.long	1012095625
	.long	-1610075367
	.long	-1078716837
	.long	-1772179289
	.long	1066724455
	.long	-211668507
	.long	1066880134
	.long	-1675234140
	.long	-1080185313
	.long	406733532
	.long	1065843480
	.long	0
	.long	1071996928
	.long	694281356
	.long	1071831116
	.long	171369734
	.long	-1132518078
	.long	-794761772
	.long	1072046225
	.long	-859111282
	.long	1015194237
	.long	579641782
	.long	-1076596544
	.long	847791125
	.long	-1132949861
	.long	-1285167911
	.long	1067653716
	.long	-636511826
	.long	1010724464
	.long	-951989065
	.long	1068869264
	.long	1695088609
	.long	-1137166075
	.long	1358195735
	.long	-1078724992
	.long	456412208
	.long	1066765380
	.long	-1223435382
	.long	1066819775
	.long	1378130218
	.long	-1080212658
	.long	1191162222
	.long	1065876640
	.long	0
	.long	1072005120
	.long	-2079929398
	.long	1071836770
	.long	-1611608179
	.long	1015831902
	.long	-1338700488
	.long	1072040996
	.long	-203730997
	.long	1015152617
	.long	-87809859
	.long	-1076598411
	.long	-2063259488
	.long	1014917524
	.long	-795298578
	.long	1067697658
	.long	-1435867020
	.long	1010121793
	.long	-713270445
	.long	1068843793
	.long	1926317792
	.long	1011929134
	.long	118495200
	.long	-1078733382
	.long	-249886553
	.long	1066804665
	.long	-497304465
	.long	1066760279
	.long	1938069535
	.long	-1080240563
	.long	1034139861
	.long	1065907159
	.long	0
	.long	1072013312
	.long	1966803211
	.long	1071842404
	.long	1641683187
	.long	-1133254894
	.long	-101812298
	.long	1072035774
	.long	-2058381016
	.long	1015630327
	.long	161257718
	.long	-1076600340
	.long	1051715721
	.long	1012769937
	.long	-1951800698
	.long	1067740807
	.long	1400349097
	.long	1010980666
	.long	-88592389
	.long	1068818488
	.long	1076820281
	.long	-1136570811
	.long	1250870392
	.long	-1078741998
	.long	-278021625
	.long	1066842336
	.long	968965230
	.long	1066701664
	.long	-730071589
	.long	-1080268982
	.long	938305845
	.long	1065935105
	.long	0
	.long	1072021504
	.long	76051646
	.long	1071848018
	.long	-2019054556
	.long	1015286955
	.long	-318630522
	.long	1072030560
	.long	-1289046044
	.long	1015456836
	.long	2006943428
	.long	-1076602333
	.long	1803035905
	.long	-1133135396
	.long	649040908
	.long	1067783168
	.long	-103914270
	.long	1011838990
	.long	-2035750078
	.long	1068793354
	.long	909604228
	.long	-1134932487
	.long	2139843906
	.long	-1078750830
	.long	1575938748
	.long	1066878417
	.long	402264115
	.long	1066643944
	.long	349118561
	.long	-1080297869
	.long	171180054
	.long	1065960547
	.long	0
	.long	1072029696
	.long	968329302
	.long	1071853611
	.long	-662984661
	.long	1015405870
	.long	-948089878
	.long	1072025354
	.long	222473062
	.long	-1132034901
	.long	1800879633
	.long	-1076604387
	.long	-1190355929
	.long	-1133116999
	.long	92450474
	.long	1067824746
	.long	-1932950099
	.long	1009259893
	.long	-1704380611
	.long	1068768394
	.long	636129127
	.long	-1137451257
	.long	-446219752
	.long	-1078759869
	.long	-351931460
	.long	1066912931
	.long	2006428649
	.long	1066587133
	.long	-1653489068
	.long	-1080327183
	.long	-30197663
	.long	1065983553
	.long	0
	.long	1072037888
	.long	483276731
	.long	1071859184
	.long	935792898
	.long	1015087633
	.long	-968361327
	.long	1072020156
	.long	-2141787405
	.long	1015296923
	.long	155354832
	.long	-1076606501
	.long	1361779835
	.long	1014358392
	.long	-1404243710
	.long	1067865546
	.long	1642602602
	.long	-1138253340
	.long	691264924
	.long	1068743613
	.long	-1119698938
	.long	-1135512013
	.long	-1774800951
	.long	-1078769105
	.long	-1572860049
	.long	1066945905
	.long	-54182741
	.long	1066531244
	.long	-1631379727
	.long	-1080356880
	.long	-1319019771
	.long	1066004195
	.long	0
	.long	1072046080
	.long	-1240544050
	.long	1071864736
	.long	-1684838215
	.long	1015954337
	.long	623283490
	.long	1072014967
	.long	-1351430426
	.long	1014744942
	.long	1942536988
	.long	-1076608675
	.long	-535931323
	.long	-1136764100
	.long	-1105403466
	.long	1067905575
	.long	-426115924
	.long	-1140545246
	.long	-115369851
	.long	1068719013
	.long	1099442099
	.long	-1136396098
	.long	-2042827831
	.long	-1078778530
	.long	-485111194
	.long	1066977363
	.long	-15635284
	.long	1066476289
	.long	257821122
	.long	-1080386919
	.long	-188164729
	.long	1066022541
	.long	0
	.long	1072054272
	.long	234276100
	.long	1071870269
	.long	-533634963
	.long	-1135282295
	.long	515643471
	.long	1072009786
	.long	-1101441315
	.long	1015193576
	.long	-886137655
	.long	-1076610907
	.long	2104288235
	.long	-1132718023
	.long	-75520392
	.long	1067944838
	.long	-796786082
	.long	1005565246
	.long	-1545774843
	.long	1068694600
	.long	112690818
	.long	1012913511
	.long	-2089515972
	.long	-1078788135
	.long	1475679345
	.long	1067007332
	.long	-1824559931
	.long	1066422278
	.long	-1418307361
	.long	-1080417262
	.long	-1369290060
	.long	1066038662
	.long	0
	.long	1072062464
	.long	759017759
	.long	1071875781
	.long	-873732448
	.long	-1131710239
	.long	-326335997
	.long	1072004613
	.long	1585418972
	.long	1015981445
	.long	764098590
	.long	-1076613195
	.long	1560233246
	.long	-1134317594
	.long	1092671043
	.long	1067983342
	.long	-876293412
	.long	-1136761265
	.long	-1753694245
	.long	1068670376
	.long	2082494373
	.long	1010262237
	.long	891659610
	.long	-1078797911
	.long	-309324344
	.long	1067035836
	.long	708175826
	.long	1066336647
	.long	-188948537
	.long	-1080447869
	.long	-1142206703
	.long	1066052627
	.long	0
	.long	1072070656
	.long	483661594
	.long	1071881273
	.long	836288326
	.long	-1132318653
	.long	-956386281
	.long	1071999450
	.long	103146576
	.long	1015037157
	.long	-1229067904
	.long	-1076615540
	.long	-400194207
	.long	-1133605002
	.long	-2039457975
	.long	1068021091
	.long	-434691455
	.long	1010929172
	.long	387614568
	.long	1068646345
	.long	841340965
	.long	-1135206012
	.long	461964541
	.long	-1078807850
	.long	-898576760
	.long	1067062903
	.long	1654011265
	.long	1066232449
	.long	851661755
	.long	-1080478702
	.long	-684372288
	.long	1066064506
	.long	0
	.long	1072078848
	.long	-438151605
	.long	1071886744
	.long	520573612
	.long	-1133315806
	.long	-446768880
	.long	1071994296
	.long	1701785266
	.long	-1135209028
	.long	-2140902454
	.long	-1076617939
	.long	536039535
	.long	-1133129258
	.long	-598845657
	.long	1068058092
	.long	412518690
	.long	1008811150
	.long	1003265956
	.long	1068622509
	.long	172213649
	.long	1009486236
	.long	-1888008909
	.long	-1078817944
	.long	1188129484
	.long	1067088559
	.long	-566207290
	.long	1066130184
	.long	1943636043
	.long	-1080509727
	.long	2078766122
	.long	1066074369
	.long	0
	.long	1072087040
	.long	-1849193019
	.long	1071892196
	.long	-1795355737
	.long	-1131539376
	.long	2111873641
	.long	1071989152
	.long	372990876
	.long	1015779442
	.long	-1580100356
	.long	-1076620392
	.long	-2044116947
	.long	-1132531311
	.long	1807575531
	.long	1068094352
	.long	-758864219
	.long	1011387116
	.long	-180371738
	.long	1068598871
	.long	17093085
	.long	1012197034
	.long	-1037803802
	.long	-1078828184
	.long	-457630954
	.long	1067112829
	.long	1957794503
	.long	1066029864
	.long	-1906472143
	.long	-1080540908
	.long	-827470206
	.long	1066082284
	.long	0
	.long	1072095232
	.long	706249988
	.long	1071897628
	.long	1895098737
	.long	1015205475
	.long	-979265555
	.long	1071984017
	.long	464173088
	.long	-1132442734
	.long	805603064
	.long	-1076622897
	.long	-1367326327
	.long	-1133691976
	.long	1956305592
	.long	1068129876
	.long	-969814061
	.long	1008775381
	.long	177351764
	.long	1068575436
	.long	2092712983
	.long	1012013574
	.long	-1125732859
	.long	-1078838562
	.long	1214064539
	.long	1067135742
	.long	-642421866
	.long	1065931496
	.long	578306548
	.long	-1080572211
	.long	-2050004064
	.long	1066088321
	.long	0
	.long	1072103424
	.long	-1197566220
	.long	1071903039
	.long	1227244784
	.long	1014943627
	.long	-257200231
	.long	1071978892
	.long	1579154106
	.long	1014085749
	.long	1033918964
	.long	-1076625454
	.long	2012144957
	.long	1012644494
	.long	1281177308
	.long	1068164671
	.long	708416726
	.long	-1135710642
	.long	454527706
	.long	1068552204
	.long	-1276702516
	.long	1011586243
	.long	1627994409
	.long	-1078849070
	.long	818273625
	.long	1067157323
	.long	-1330079251
	.long	1065835088
	.long	-1673508590
	.long	-1080603605
	.long	-752575923
	.long	1066092547
	.long	0
	.long	1072111616
	.long	1196859209
	.long	1071908431
	.long	-115608910
	.long	-1131762296
	.long	838233511
	.long	1071973778
	.long	323712263
	.long	-1131923471
	.long	-622576051
	.long	-1076628062
	.long	-598088830
	.long	-1132516944
	.long	1557731980
	.long	1068198743
	.long	-693432836
	.long	-1138598921
	.long	-1625199238
	.long	1068529178
	.long	-448389058
	.long	1012365336
	.long	1739116337
	.long	-1078859701
	.long	1891032431
	.long	1067177599
	.long	-1611237403
	.long	1065740644
	.long	909964326
	.long	-1080635057
	.long	1332090846
	.long	1066095031
	.long	0
	.long	1072119808
	.long	-529536447
	.long	1071913802
	.long	1395095122
	.long	1013175818
	.long	-1150563284
	.long	1071968673
	.long	2088891736
	.long	1012698473
	.long	363074404
	.long	-1076630718
	.long	1635529066
	.long	1014139548
	.long	588006584
	.long	1068232099
	.long	1356458458
	.long	1010956821
	.long	-389513277
	.long	1068506361
	.long	-1586110668
	.long	1010895248
	.long	1637586492
	.long	-1078870447
	.long	-405126628
	.long	1067196597
	.long	1669039792
	.long	1065648167
	.long	-62689295
	.long	-1080666539
	.long	-422738529
	.long	1066095838
	.long	0
	.long	1072128000
	.long	-1907677482
	.long	1071919154
	.long	275823007
	.long	-1132383911
	.long	-1108858379
	.long	1071963579
	.long	-1227786731
	.long	-1133289486
	.long	-113151718
	.long	-1076633424
	.long	-385715673
	.long	1014838356
	.long	770635202
	.long	1068264745
	.long	195115958
	.long	1011311923
	.long	1231822430
	.long	1068468568
	.long	-508829905
	.long	1011477490
	.long	-1218271188
	.long	-1078881301
	.long	2083387711
	.long	1067214345
	.long	-537431478
	.long	1065557657
	.long	-827839910
	.long	-1080698020
	.long	-956356677
	.long	1066095036
	.long	0
	.long	1072136192
	.long	1534679894
	.long	1071924486
	.long	373258696
	.long	-1132497200
	.long	1765670933
	.long	1071958496
	.long	1627387291
	.long	-1132785717
	.long	-1901780568
	.long	-1076636176
	.long	-1245920254
	.long	1012274547
	.long	491494242
	.long	1068296688
	.long	-221692677
	.long	1011356198
	.long	-1243903929
	.long	1068423782
	.long	976774768
	.long	-1136333671
	.long	-1457169949
	.long	-1078892255
	.long	330473853
	.long	1067230869
	.long	895058611
	.long	1065469115
	.long	1707002166
	.long	-1080729473
	.long	1312071527
	.long	1066092690
	.long	0
	.long	1072144384
	.long	1387977855
	.long	1071929798
	.long	-1903291556
	.long	1015126377
	.long	-331866677
	.long	1071953423
	.long	1193600242
	.long	-1133799308
	.long	-600241824
	.long	-1076638975
	.long	2074161676
	.long	-1133362255
	.long	-1600337294
	.long	1068327934
	.long	-441742076
	.long	1011363980
	.long	-550586798
	.long	1068379426
	.long	1320327891
	.long	-1136031581
	.long	1320536982
	.long	-1078903302
	.long	-1906629033
	.long	1067246195
	.long	-785203596
	.long	1065382536
	.long	1436753566
	.long	-1080760872
	.long	-73397070
	.long	1066088863
	.long	0
	.long	1072152576
	.long	2130593558
	.long	1071935090
	.long	461655241
	.long	-1134322789
	.long	1956391495
	.long	1071948362
	.long	-2099731739
	.long	-1131438843
	.long	-438142461
	.long	-1076641819
	.long	-1290834600
	.long	-1135589414
	.long	1978762732
	.long	1068358491
	.long	601163850
	.long	1011070592
	.long	1200644562
	.long	1068335504
	.long	1613575495
	.long	-1141615012
	.long	-1751228002
	.long	-1078914436
	.long	-1069791456
	.long	1067260351
	.long	28488799
	.long	1065242621
	.long	306557115
	.long	-1080792191
	.long	1627310316
	.long	1066083621
	.long	0
	.long	1072160768
	.long	-346063874
	.long	1071940362
	.long	1719028339
	.long	-1134942634
	.long	791489508
	.long	1071943312
	.long	-1768179141
	.long	1015089806
	.long	-1392709811
	.long	-1076644707
	.long	-1213852344
	.long	-1133883496
	.long	1758974209
	.long	1068388365
	.long	75893793
	.long	-1137681346
	.long	748219036
	.long	1068292018
	.long	-731194208
	.long	1011378475
	.long	1261946533
	.long	-1078925649
	.long	1811621474
	.long	1067273364
	.long	110283893
	.long	1065077293
	.long	-206138606
	.long	-1080823407
	.long	357742830
	.long	1066077025
	.long	0
	.long	1072168960
	.long	-1557750047
	.long	1071945615
	.long	-2014141352
	.long	1014120858
	.long	1202590843
	.long	1071938273
	.long	-343597384
	.long	-1133594543
	.long	810889825
	.long	-1076647638
	.long	-1807322238
	.long	-1132525834
	.long	1364035800
	.long	1068417563
	.long	-195529818
	.long	1010618465
	.long	-1998604276
	.long	1068248971
	.long	1403900427
	.long	-1139475268
	.long	147265861
	.long	-1078936935
	.long	1031401791
	.long	1067285260
	.long	897449063
	.long	1064915859
	.long	984184873
	.long	-1080854494
	.long	-1464267344
	.long	1066069136
	.long	0
	.long	1072177152
	.long	-1312352414
	.long	1071950848
	.long	595769600
	.long	1015750887
	.long	-387689902
	.long	1071933245
	.long	1732611996
	.long	1012388360
	.long	1814356403
	.long	-1076650612
	.long	-1757647595
	.long	-1132973883
	.long	314016071
	.long	1068446092
	.long	-753395636
	.long	-1135631296
	.long	361448679
	.long	1068206367
	.long	1171780083
	.long	1011703711
	.long	1201924282
	.long	-1078948288
	.long	-993943014
	.long	1067296065
	.long	1007492465
	.long	1064758302
	.long	355339260
	.long	-1080885432
	.long	-1722472049
	.long	1066060016
	.long	0
	.long	1072185344
	.long	585012453
	.long	1071956062
	.long	-258003233
	.long	-1131672895
	.long	1016751456
	.long	1071928230
	.long	-2104772094
	.long	1014475103
	.long	1510890268
	.long	-1076653627
	.long	-851478830
	.long	1014846781
	.long	-1696670074
	.long	1068473958
	.long	-690832704
	.long	-1138598316
	.long	1272941015
	.long	1068164207
	.long	723892441
	.long	1010420108
	.long	1466279361
	.long	-1078959701
	.long	1898436169
	.long	1067305807
	.long	318776902
	.long	1064604602
	.long	-1550331482
	.long	-1080916200
	.long	1229342901
	.long	1066049724
	.long	0
	.long	1072193536
	.long	36967656
	.long	1071961256
	.long	497715376
	.long	1013856905
	.long	1805809774
	.long	1071923226
	.long	-474615017
	.long	-1135902189
	.long	-250018368
	.long	-1076656683
	.long	-1561055791
	.long	-1133338135
	.long	-260418961
	.long	1068500056
	.long	-1403004905
	.long	-1139476123
	.long	1724546466
	.long	1068122494
	.long	-1605323725
	.long	1011555866
	.long	1612197455
	.long	-1078971168
	.long	-1934646245
	.long	1067314511
	.long	530903095
	.long	1064454736
	.long	-40834795
	.long	-1080946777
	.long	-174163249
	.long	1066038317
	.long	0
	.long	1072201728
	.long	1538714628
	.long	1071966430
	.long	-1095534228
	.long	1015325501
	.long	-1646723647
	.long	1071918234
	.long	-1350564597
	.long	1011978970
	.long	632158591
	.long	-1076659777
	.long	550319697
	.long	-1133035495
	.long	1917696769
	.long	1068513338
	.long	-342629064
	.long	1012682343
	.long	1684956639
	.long	1068081230
	.long	-1991611529
	.long	1008838603
	.long	1651973241
	.long	-1078982683
	.long	933516612
	.long	1067322204
	.long	1419325586
	.long	1064308679
	.long	928205861
	.long	-1080977143
	.long	-1370393814
	.long	1066025854
	.long	0
	.long	1072209920
	.long	998101374
	.long	1071971585
	.long	1048323253
	.long	1014845759
	.long	-98087183
	.long	1071913254
	.long	1299377331
	.long	1015732272
	.long	-376110710
	.long	-1076662911
	.long	1876298061
	.long	1014922949
	.long	1450948981
	.long	1068526299
	.long	-254331313
	.long	1012011713
	.long	128562724
	.long	1068040417
	.long	-1049899899
	.long	-1135715493
	.long	1885275435
	.long	-1079003776
	.long	1684857134
	.long	1067328911
	.long	-1547290559
	.long	1064028168
	.long	1728331073
	.long	-1081007282
	.long	1326559777
	.long	1066012390
	.long	0
	.long	1072218112
	.long	-1379537535
	.long	1071976720
	.long	1748149822
	.long	1014119885
	.long	-1501147248
	.long	1071908287
	.long	1714214367
	.long	-1133241915
	.long	737277924
	.long	-1076666081
	.long	1699232632
	.long	-1133603619
	.long	602020841
	.long	1068538943
	.long	-892697203
	.long	1008917347
	.long	-644071651
	.long	1068000055
	.long	-603296260
	.long	1006846850
	.long	676298899
	.long	-1079026963
	.long	-745079997
	.long	1067334658
	.long	146366800
	.long	1063751123
	.long	-1439839174
	.long	-1081037176
	.long	2031259367
	.long	1065997979
	.long	0
	.long	1072226304
	.long	-1091442181
	.long	1071981836
	.long	-1558017936
	.long	-1135265353
	.long	-939453883
	.long	1071903332
	.long	1154249227
	.long	-1134149135
	.long	-649955446
	.long	-1076669289
	.long	-1680352298
	.long	1014303322
	.long	1681421214
	.long	1068551273
	.long	1130330509
	.long	1005967667
	.long	724136495
	.long	1067960148
	.long	1530589819
	.long	-1142475010
	.long	93994020
	.long	-1079050212
	.long	270041776
	.long	1067339472
	.long	-1998830999
	.long	1063481518
	.long	726827190
	.long	-1081066808
	.long	-1918520659
	.long	1065982675
	.long	0
	.long	1072234496
	.long	2072577567
	.long	1071986933
	.long	-1411755508
	.long	-1132348556
	.long	-2101903340
	.long	1071898390
	.long	-1539312112
	.long	-1133480770
	.long	-614735132
	.long	-1076672532
	.long	-1978756349
	.long	1014515912
	.long	-1549722652
	.long	1068563293
	.long	165481833
	.long	1010657522
	.long	376930672
	.long	1067920695
	.long	570479060
	.long	-1135695779
	.long	-724714167
	.long	-1079073513
	.long	1808145263
	.long	1067343376
	.long	-799967964
	.long	1063182511
	.long	1328465817
	.long	-1081110472
	.long	-1448434245
	.long	1065966530
	.long	0
	.long	1072242688
	.long	-264885105
	.long	1071992010
	.long	2140095425
	.long	1015587620
	.long	-102694546
	.long	1071893460
	.long	1125869543
	.long	-1131880794
	.long	426359907
	.long	-1076675809
	.long	1597870040
	.long	1013903726
	.long	1883061238
	.long	1068575007
	.long	-1337063822
	.long	-1135186302
	.long	-2140461326
	.long	1067881697
	.long	-1345394880
	.long	1009788953
	.long	383577750
	.long	-1079096853
	.long	-67259553
	.long	1067346396
	.long	1891927312
	.long	1062672656
	.long	-385771320
	.long	-1081168603
	.long	-1580784693
	.long	1065949595
	.long	0
	.long	1072250880
	.long	700910644
	.long	1071997069
	.long	-253853560
	.long	1015649180
	.long	1338976451
	.long	1071888544
	.long	-880803524
	.long	-1136223189
	.long	2011975417
	.long	-1076679121
	.long	-971017407
	.long	1014996230
	.long	1506000095
	.long	1068586418
	.long	897826843
	.long	-1137874642
	.long	438296925
	.long	1067843156
	.long	1570483585
	.long	1011027397
	.long	31016104
	.long	-1079120224
	.long	-1769754044
	.long	1067348558
	.long	1226013167
	.long	1062147028
	.long	192739147
	.long	-1081226124
	.long	-10549723
	.long	1065931919
	.long	0
	.long	1072259072
	.long	892023738
	.long	1072002108
	.long	885554854
	.long	-1131503603
	.long	-1510973880
	.long	1071883640
	.long	14967707
	.long	-1131699732
	.long	-659053505
	.long	-1076682466
	.long	1735271336
	.long	-1133687980
	.long	-249877459
	.long	1068597529
	.long	977008122
	.long	1010074971
	.long	1650226879
	.long	1067805071
	.long	-1092943260
	.long	-1137740406
	.long	-2118778621
	.long	-1079143616
	.long	-829321201
	.long	1067349885
	.long	889051951
	.long	1061184835
	.long	-435716223
	.long	-1081283013
	.long	-29184034
	.long	1065913551
	.long	0
	.long	1072267264
	.long	527642555
	.long	1072007128
	.long	-658134704
	.long	-1133124151
	.long	483560600
	.long	1071878750
	.long	536790746
	.long	1015841859
	.long	452118050
	.long	-1076685842
	.long	-1136690488
	.long	1007292059
	.long	-940145725
	.long	1068608345
	.long	314590464
	.long	1012838167
	.long	-1492938375
	.long	1067767443
	.long	-1529980766
	.long	-1135833740
	.long	957612211
	.long	-1079167018
	.long	-233534922
	.long	1067350402
	.long	-1226873910
	.long	1058089302
	.long	-70191122
	.long	-1081339244
	.long	-1962210802
	.long	1065894538
	.long	0
	.long	1072275456
	.long	-170939490
	.long	1072012128
	.long	1090677701
	.long	-1132351412
	.long	-735719940
	.long	1071873872
	.long	1368059748
	.long	1015436851
	.long	454511067
	.long	-1076689250
	.long	-1592141380
	.long	-1137632224
	.long	1886703331
	.long	1068618869
	.long	1341529644
	.long	-1135541536
	.long	109071855
	.long	1067730273
	.long	-1014605820
	.long	1011778207
	.long	-2112518090
	.long	-1079190422
	.long	119221996
	.long	1067350134
	.long	-243512341
	.long	-1086891131
	.long	799029617
	.long	-1081394795
	.long	1109017403
	.long	1065874925
	.long	0
	.long	1072283648
	.long	-980380393
	.long	1072017110
	.long	1900713014
	.long	1015492911
	.long	-356574731
	.long	1071869008
	.long	1380474058
	.long	1014026716
	.long	-1292819930
	.long	-1076692689
	.long	1046297953
	.long	1012138170
	.long	2093196315
	.long	1068629104
	.long	-2011562149
	.long	1011570919
	.long	1898877545
	.long	1067693559
	.long	2021251243
	.long	-1135721622
	.long	1866039841
	.long	-1079213818
	.long	-915377209
	.long	1067349102
	.long	-319017622
	.long	-1085729570
	.long	-811119659
	.long	-1081449648
	.long	1865985438
	.long	1065854756
	.long	0
	.long	1072291840
	.long	-1675345311
	.long	1072022073
	.long	-370840779
	.long	-1132258348
	.long	2124083932
	.long	1071864158
	.long	-1647767958
	.long	1015148333
	.long	-1180780919
	.long	-1076696157
	.long	1781475073
	.long	-1132784913
	.long	2126801819
	.long	1068639054
	.long	-1925096090
	.long	1012527884
	.long	-1430319834
	.long	1067657302
	.long	1060362218
	.long	-1139661455
	.long	-868374066
	.long	-1079237198
	.long	-1469903699
	.long	1067347332
	.long	-1095077766
	.long	-1085077307
	.long	-1588043823
	.long	-1081503782
	.long	231031638
	.long	1065834075
	.long	0
	.long	1072300032
	.long	-2028561640
	.long	1072027017
	.long	1271261130
	.long	1012925070
	.long	-1394601146
	.long	1071859321
	.long	80846443
	.long	1015966520
	.long	59850146
	.long	-1076699653
	.long	-1709593155
	.long	-1133101368
	.long	129263877
	.long	1068648723
	.long	-688902017
	.long	-1135707160
	.long	1268168563
	.long	1067621502
	.long	-1853696125
	.long	-1138441523
	.long	520560452
	.long	-1079260552
	.long	-994592828
	.long	1067344846
	.long	308229552
	.long	-1084665190
	.long	-315188437
	.long	-1081557181
	.long	-915307059
	.long	1065812922
	.long	0
	.long	1072308224
	.long	-1810873392
	.long	1072031942
	.long	-1102766480
	.long	-1133461643
	.long	-1847452948
	.long	1071854498
	.long	2060437583
	.long	1014685749
	.long	1653430369
	.long	-1076703178
	.long	-463220730
	.long	1013433851
	.long	-1479049294
	.long	1068658113
	.long	-998528927
	.long	-1135332675
	.long	-1037451570
	.long	1067586157
	.long	497610142
	.long	-1135877697
	.long	-1466899096
	.long	-1079283874
	.long	-289066086
	.long	1067341667
	.long	1383377490
	.long	-1084266330
	.long	-1969359528
	.long	-1081609828
	.long	-8348219
	.long	1065791339
	.long	0
	.long	1072316416
	.long	-791294902
	.long	1072036848
	.long	749912
	.long	-1134867203
	.long	1227111128
	.long	1071849689
	.long	1128233957
	.long	-1131616800
	.long	-1515459537
	.long	-1076706730
	.long	71912897
	.long	-1134004133
	.long	-299522020
	.long	1068667229
	.long	1756593803
	.long	1010724933
	.long	1414641618
	.long	1067551268
	.long	2043813333
	.long	-1136972503
	.long	1709166438
	.long	-1079307154
	.long	-1531406919
	.long	1067337818
	.long	343346021
	.long	-1084054060
	.long	-344085745
	.long	-1081661710
	.long	-1065233395
	.long	1065769365
	.long	0
	.long	1072324608
	.long	1262936159
	.long	1072041736
	.long	-1718333037
	.long	-1131562106
	.long	-312745831
	.long	1071844893
	.long	926920261
	.long	1013739128
	.long	-1722074622
	.long	-1076710308
	.long	-405888339
	.long	1014806907
	.long	1744506078
	.long	1068676075
	.long	1226888459
	.long	-1136645849
	.long	547291364
	.long	1067516833
	.long	-324262054
	.long	-1137247401
	.long	284911145
	.long	-1079330385
	.long	285260106
	.long	1067333321
	.long	1482821228
	.long	-1083867600
	.long	671159694
	.long	-1081712812
	.long	-763449593
	.long	1065747037
	.long	0
	.long	1072332800
	.long	291339150
	.long	1072046605
	.long	890169537
	.long	-1134381179
	.long	-1737269176
	.long	1071840112
	.long	1308083450
	.long	1015523153
	.long	123744340
	.long	-1076713911
	.long	-177986436
	.long	1014776299
	.long	-1597323886
	.long	1068684653
	.long	1316322783
	.long	1009987896
	.long	530922725
	.long	1067482851
	.long	672217718
	.long	-1136542452
	.long	569070193
	.long	-1079353560
	.long	148864847
	.long	1067328197
	.long	276517519
	.long	-1083687478
	.long	170063507
	.long	-1081763124
	.long	-1282066758
	.long	1065724392
	.long	0
	.long	1072340992
	.long	825040699
	.long	1072051455
	.long	368227736
	.long	1015665395
	.long	1670159637
	.long	1071835345
	.long	-31833219
	.long	1015225061
	.long	-1227360734
	.long	-1076717540
	.long	-2031485363
	.long	-1135355257
	.long	567551410
	.long	1068692968
	.long	-1373235509
	.long	-1139252094
	.long	1250271460
	.long	1067448274
	.long	-489086885
	.long	-1138146680
	.long	-809968631
	.long	-1079376672
	.long	186621043
	.long	1067322468
	.long	-1120385576
	.long	-1083513596
	.long	422022318
	.long	-1081812635
	.long	-1661528005
	.long	1065701465
	.long	0
	.long	1072349184
	.long	-1193145456
	.long	1072056286
	.long	37376463
	.long	-1134638588
	.long	1728294963
	.long	1071830592
	.long	939760386
	.long	1015483110
	.long	1815719157
	.long	-1076721192
	.long	37267438
	.long	-1133383624
	.long	1910150495
	.long	1068701022
	.long	-736131873
	.long	-1134581246
	.long	-1010393942
	.long	1067382115
	.long	1967144381
	.long	1009982761
	.long	292819535
	.long	-1079399712
	.long	1055765259
	.long	1067316155
	.long	476260197
	.long	-1083345851
	.long	-1527118873
	.long	-1081861336
	.long	-2042193651
	.long	1065678290
	.long	0
	.long	1072357376
	.long	-1228899581
	.long	1072061099
	.long	989013801
	.long	-1134062693
	.long	-1166963224
	.long	1071825853
	.long	-996603349
	.long	-1132276996
	.long	-380082152
	.long	-1076724868
	.long	-2012726838
	.long	1014500702
	.long	350218723
	.long	1068708820
	.long	-1125692242
	.long	1012098153
	.long	-948337736
	.long	1067316855
	.long	-1271966806
	.long	1010250354
	.long	-1623443954
	.long	-1079422676
	.long	1926462244
	.long	1067309279
	.long	48502773
	.long	-1083184143
	.long	-781576839
	.long	-1081909218
	.long	692749284
	.long	1065654900
	.long	0
	.long	1072365568
	.long	958652551
	.long	1072065894
	.long	-337069126
	.long	-1133057425
	.long	1957603462
	.long	1071821129
	.long	-1445539478
	.long	1015451645
	.long	-312125666
	.long	-1076728566
	.long	1198503410
	.long	1013774706
	.long	-1948175457
	.long	1068716364
	.long	1064444438
	.long	1012698127
	.long	806326171
	.long	1067252491
	.long	953272626
	.long	-1139876479
	.long	-213188529
	.long	-1079445556
	.long	466861536
	.long	1067301861
	.long	-1678254869
	.long	-1083103689
	.long	-1589189198
	.long	-1081956274
	.long	99774862
	.long	1065631326
	.long	0
	.long	1072373760
	.long	1316890712
	.long	1072070670
	.long	457085168
	.long	-1135023510
	.long	-1412067028
	.long	1071816419
	.long	-1317263133
	.long	-1134447667
	.long	888283627
	.long	-1076732285
	.long	-97516155
	.long	-1133198205
	.long	1419114211
	.long	1068723659
	.long	823751678
	.long	1012512091
	.long	-1738223303
	.long	1067189018
	.long	-1124015759
	.long	1009319411
	.long	1258448261
	.long	-1079468345
	.long	1420160317
	.long	1067293920
	.long	-1463063044
	.long	-1083028717
	.long	22309289
	.long	-1082002498
	.long	1722092169
	.long	1065607598
	.long	0
	.long	1072381952
	.long	89586538
	.long	1072075428
	.long	1519297228
	.long	1015911609
	.long	1967494659
	.long	1071811724
	.long	692452360
	.long	1015749691
	.long	2045998184
	.long	-1076736026
	.long	247590804
	.long	-1135724719
	.long	-382614213
	.long	1068730707
	.long	383256200
	.long	-1136979827
	.long	1578848437
	.long	1067126434
	.long	1677529191
	.long	1006662848
	.long	-1476626432
	.long	-1079491039
	.long	-586144402
	.long	1067285476
	.long	-916902818
	.long	-1082956608
	.long	-753346809
	.long	-1082047887
	.long	1691490232
	.long	1065583746
	.long	0
	.long	1072390144
	.long	1816855926
	.long	1072080167
	.long	-1673091426
	.long	-1131451547
	.long	-442147351
	.long	1071807043
	.long	-306029239
	.long	1013351534
	.long	1942206497
	.long	-1076739787
	.long	1454069768
	.long	-1132912598
	.long	-1071108988
	.long	1068737513
	.long	64118187
	.long	1010849738
	.long	-1544846003
	.long	1067064734
	.long	-264360015
	.long	-1137216019
	.long	-786624445
	.long	-1079513631
	.long	425617761
	.long	1067276550
	.long	1695744598
	.long	-1082887309
	.long	-142616988
	.long	-1082092435
	.long	-306023742
	.long	1065559797
	.long	0
	.long	1072398336
	.long	-1844757095
	.long	1072084888
	.long	1636353294
	.long	-1131773896
	.long	283476286
	.long	1071802378
	.long	574793404
	.long	-1131457385
	.long	-685376996
	.long	-1076743568
	.long	1551375124
	.long	1014329986
	.long	1274212644
	.long	1068744080
	.long	1779756404
	.long	1009888924
	.long	1407849779
	.long	1067003915
	.long	1761185150
	.long	1009421704
	.long	1403315511
	.long	-1079536115
	.long	301330566
	.long	1067267159
	.long	431661530
	.long	-1082820768
	.long	-1297954178
	.long	-1082141846
	.long	-945807231
	.long	1065535779
	.long	0
	.long	1072406528
	.long	-2057555560
	.long	1072089591
	.long	700262413
	.long	1014761950
	.long	172171197
	.long	1071797727
	.long	-258313123
	.long	1015912550
	.long	1447623455
	.long	-1076747367
	.long	-1260451010
	.long	1010901248
	.long	-86341479
	.long	1068750410
	.long	1704739654
	.long	1010686097
	.long	566415521
	.long	1066943972
	.long	-208174468
	.long	1009305265
	.long	-2075803188
	.long	-1079558487
	.long	1929601272
	.long	1067257322
	.long	-1403924944
	.long	-1082756933
	.long	-285930005
	.long	-1082227565
	.long	-1766778455
	.long	1065511717
	.long	0
	.long	1072414720
	.long	1427461039
	.long	1072094276
	.long	896762510
	.long	-1134823290
	.long	-464882891
	.long	1071793090
	.long	1708261335
	.long	-1134136697
	.long	-1597347629
	.long	-1076751185
	.long	-2004744043
	.long	-1133434516
	.long	918604194
	.long	1068756509
	.long	-2095187266
	.long	1012141046
	.long	-1930895740
	.long	1066884900
	.long	1139497043
	.long	-1139649688
	.long	-2143844104
	.long	-1079580741
	.long	-1941776903
	.long	1067247058
	.long	-89279573
	.long	-1082695750
	.long	-565434150
	.long	-1082311591
	.long	-490766603
	.long	1065487635
	.long	0
	.long	1072422912
	.long	270551791
	.long	1072098943
	.long	-1559862670
	.long	1015452398
	.long	-1327931675
	.long	1071788469
	.long	515179958
	.long	1014673022
	.long	1673149960
	.long	-1076755020
	.long	-1183493763
	.long	1014089425
	.long	1693304617
	.long	1068762378
	.long	1901967369
	.long	1011441901
	.long	-490990533
	.long	1066826695
	.long	-259903165
	.long	-1137918185
	.long	776846407
	.long	-1079602872
	.long	1356896302
	.long	1067236385
	.long	-307608790
	.long	-1082637166
	.long	189288691
	.long	-1082393923
	.long	480915244
	.long	1065463558
	.long	0
	.long	1072431104
	.long	-981973452
	.long	1072103591
	.long	631955190
	.long	-1132936371
	.long	-2128479368
	.long	1071783863
	.long	-2128479368
	.long	1013063607
	.long	1235110025
	.long	-1076758872
	.long	1833828937
	.long	-1134499532
	.long	-438383849
	.long	1068768021
	.long	-1422573082
	.long	1011979508
	.long	1084454069
	.long	1066769353
	.long	1421741584
	.long	-1137058706
	.long	1074609306
	.long	-1079624876
	.long	1467052594
	.long	1067225320
	.long	1811574862
	.long	-1082581128
	.long	1681762013
	.long	-1082474564
	.long	-1541870287
	.long	1065439506
	.long	0
	.long	1072439296
	.long	-2077666920
	.long	1072108222
	.long	863864961
	.long	1015401872
	.long	1705844571
	.long	1071779272
	.long	-676000440
	.long	-1131933654
	.long	-92941580
	.long	-1076762741
	.long	1637229778
	.long	1014118196
	.long	353855038
	.long	1068773443
	.long	-438182415
	.long	-1135937372
	.long	-1776495099
	.long	1066712867
	.long	1554072839
	.long	-1138300818
	.long	851574743
	.long	-1079646748
	.long	-637431347
	.long	1067213880
	.long	1301421355
	.long	-1082527584
	.long	1312150862
	.long	-1082553515
	.long	-850428931
	.long	1065415502
	.long	0
	.long	1072447488
	.long	1531948667
	.long	1072112835
	.long	766724507
	.long	1012484275
	.long	1851581277
	.long	1071774696
	.long	1444816803
	.long	1015087569
	.long	465400052
	.long	-1076766624
	.long	-97336111
	.long	-1132770153
	.long	1223985783
	.long	1068778645
	.long	-1266688119
	.long	-1137913875
	.long	-1497256148
	.long	1066657233
	.long	-1011506789
	.long	-1139495080
	.long	1351907679
	.long	-1079668484
	.long	-1237783614
	.long	1067202083
	.long	1336306958
	.long	-1082476481
	.long	-1215831401
	.long	-1082630781
	.long	-417641238
	.long	1065391566
	.long	0
	.long	1072455680
	.long	1511468543
	.long	1072117430
	.long	111710929
	.long	1014461949
	.long	-1435557001
	.long	1071770135
	.long	-884675577
	.long	-1132227160
	.long	1349708918
	.long	-1076770523
	.long	-162474530
	.long	-1133637197
	.long	-763255765
	.long	1068783631
	.long	-164485429
	.long	-1135232050
	.long	205591124
	.long	1066602446
	.long	-914020570
	.long	1010451474
	.long	-1315015058
	.long	-1079690080
	.long	1838423509
	.long	1067189945
	.long	155265202
	.long	-1082427766
	.long	704937026
	.long	-1082706366
	.long	1087869535
	.long	1065367718
	.long	0
	.long	1072463872
	.long	-1883599345
	.long	1072122007
	.long	374899873
	.long	1011331750
	.long	679476106
	.long	1071765590
	.long	-18711907
	.long	-1134196193
	.long	957960768
	.long	-1076774436
	.long	1428014783
	.long	1012942406
	.long	-45111194
	.long	1068788405
	.long	-1023492541
	.long	1012884135
	.long	944691756
	.long	1066548499
	.long	503160628
	.long	1010539132
	.long	1023357516
	.long	-1079711531
	.long	632344749
	.long	1067177482
	.long	-530329114
	.long	-1082381388
	.long	-592097835
	.long	-1082780279
	.long	1627880325
	.long	1065334734
	.long	0
	.long	1072472064
	.long	193124568
	.long	1072126567
	.long	1282576519
	.long	1015997612
	.long	-158580719
	.long	1071761059
	.long	845618217
	.long	-1131522844
	.long	1941773455
	.long	-1076778363
	.long	1104657792
	.long	1014310317
	.long	256773312
	.long	1068792971
	.long	-665548964
	.long	1009712593
	.long	1988629325
	.long	1066495387
	.long	1118239010
	.long	-1139097866
	.long	-1443993266
	.long	-1079732835
	.long	-1465081576
	.long	1067164709
	.long	-4785633
	.long	-1082337293
	.long	-986826406
	.long	-1082852526
	.long	835798625
	.long	1065287494
	.long	0
	.long	1072480256
	.long	-590952768
	.long	1072131108
	.long	1370343485
	.long	1015125285
	.long	569635468
	.long	1071756545
	.long	1595762614
	.long	-1136299729
	.long	-1678214589
	.long	-1076782303
	.long	-1308156344
	.long	-1134276086
	.long	1218813507
	.long	1068797330
	.long	1107381131
	.long	1012895318
	.long	-296964917
	.long	1066443104
	.long	2070784615
	.long	1009506603
	.long	-2133969783
	.long	-1079753987
	.long	1704304197
	.long	1067151643
	.long	1287808826
	.long	-1082295429
	.long	-1469085042
	.long	-1082923117
	.long	-544120787
	.long	1065240532
	.long	0
	.long	1072488448
	.long	317334215
	.long	1072135633
	.long	-1883173571
	.long	1015493216
	.long	-1216564830
	.long	1071752045
	.long	1851939817
	.long	-1136475635
	.long	1257746748
	.long	-1076786255
	.long	-1140441510
	.long	-1134017429
	.long	-476762949
	.long	1068801486
	.long	1489998115
	.long	1012436576
	.long	1226635844
	.long	1066381499
	.long	239020611
	.long	1008022780
	.long	473660106
	.long	-1079774984
	.long	1894477962
	.long	1067138298
	.long	1588235135
	.long	-1082255746
	.long	727219204
	.long	-1082992062
	.long	-2010143141
	.long	1065193881
	.long	0
	.long	1072496640
	.long	-1117965986
	.long	1072140139
	.long	-1988085952
	.long	1015285408
	.long	-1017895734
	.long	1071747561
	.long	-188840385
	.long	-1131437119
	.long	394107525
	.long	-1076790219
	.long	1788466502
	.long	-1133413503
	.long	340787118
	.long	1068805444
	.long	-590568797
	.long	1010120883
	.long	-81046237
	.long	1066280214
	.long	1612192729
	.long	1007647761
	.long	-1440954815
	.long	-1079795824
	.long	-2060434807
	.long	1067124689
	.long	1970262419
	.long	-1082218192
	.long	-492199714
	.long	-1083059374
	.long	-2005939950
	.long	1065147569
	.long	0
	.long	1072504832
	.long	-342091358
	.long	1072144628
	.long	-847159465
	.long	1013005415
	.long	1360159599
	.long	1071743093
	.long	2050186016
	.long	1015062898
	.long	-1779943675
	.long	-1076794195
	.long	2065815009
	.long	1013098827
	.long	148769404
	.long	1068809205
	.long	91510688
	.long	1010135248
	.long	2104823775
	.long	1066180552
	.long	-28508356
	.long	1009320103
	.long	749687018
	.long	-1079816502
	.long	59280503
	.long	1067110831
	.long	1907112582
	.long	-1082182716
	.long	1654062424
	.long	-1083125063
	.long	-2006116680
	.long	1065101624
	.long	0
	.long	1072513024
	.long	-1389473538
	.long	1072149100
	.long	-217837061
	.long	-1131616243
	.long	1807505274
	.long	1071738640
	.long	-1114980537
	.long	-1132655178
	.long	1479891807
	.long	-1076798181
	.long	-1127808968
	.long	-1134865321
	.long	-386018019
	.long	1068812772
	.long	682398595
	.long	1011231065
	.long	539619651
	.long	1066082499
	.long	725746688
	.long	-1137735100
	.long	2073173065
	.long	-1079837017
	.long	-194451554
	.long	1067096736
	.long	-859059377
	.long	-1082149268
	.long	-1680631201
	.long	-1083199280
	.long	-2011981425
	.long	1065056072
	.long	0
	.long	1072521216
	.long	296093995
	.long	1072153555
	.long	758133384
	.long	1015788491
	.long	499522327
	.long	1071734203
	.long	-1149772714
	.long	-1131477881
	.long	-301502006
	.long	-1076802178
	.long	-600847408
	.long	1014158219
	.long	-704114930
	.long	1068816150
	.long	-1443118397
	.long	-1134774852
	.long	-62726097
	.long	1065986041
	.long	-2055477084
	.long	-1138745620
	.long	1154411443
	.long	-1079857365
	.long	139531833
	.long	1067082421
	.long	-924965922
	.long	-1082124115
	.long	-916172774
	.long	-1083324254
	.long	-342804841
	.long	1065010937
	.long	0
	.long	1072529408
	.long	681549971
	.long	1072157992
	.long	506411689
	.long	1015373954
	.long	1897222587
	.long	1071729781
	.long	1905725228
	.long	-1132410738
	.long	-458600299
	.long	-1076806184
	.long	-1263030766
	.long	1013374257
	.long	-355155439
	.long	1068819341
	.long	-476929250
	.long	1011957680
	.long	-155761290
	.long	1065891167
	.long	-212460873
	.long	1000917828
	.long	232848042
	.long	-1079877544
	.long	-1735167504
	.long	1067067896
	.long	721968538
	.long	-1082109343
	.long	621921621
	.long	-1083446069
	.long	-2017864909
	.long	1064966243
	.long	0
	.long	1072537600
	.long	29431038
	.long	1072162412
	.long	-1955198067
	.long	-1131642184
	.long	1862499396
	.long	1071725375
	.long	-2057123648
	.long	-1134341229
	.long	-954719179
	.long	-1076810199
	.long	219142805
	.long	-1133258112
	.long	1000479371
	.long	1068822349
	.long	-1700523706
	.long	-1139293104
	.long	-1013055674
	.long	1065797863
	.long	-2146719314
	.long	1008590192
	.long	884558678
	.long	-1079897552
	.long	-1473775270
	.long	1067053176
	.long	432712087
	.long	-1082095511
	.long	-121344730
	.long	-1083564759
	.long	-1369843177
	.long	1064922010
	.long	0
	.long	1072545792
	.long	-1397131112
	.long	1072166814
	.long	-2132094431
	.long	1016059338
	.long	543180837
	.long	1071720985
	.long	-1584828168
	.long	-1134770218
	.long	503212964
	.long	-1076814222
	.long	-1654309604
	.long	1013999383
	.long	-704857285
	.long	1068825175
	.long	1599126534
	.long	1008280264
	.long	-382068833
	.long	1065706115
	.long	1588007770
	.long	-1140735252
	.long	-255825073
	.long	-1079917387
	.long	-460702972
	.long	1067038273
	.long	-517469663
	.long	-1082082595
	.long	467912157
	.long	-1083680354
	.long	980601062
	.long	1064878259
	.long	0
	.long	1072553984
	.long	960522571
	.long	1072171200
	.long	1997928101
	.long	-1132458190
	.long	-1921787506
	.long	1071716610
	.long	821617296
	.long	1014092120
	.long	1875052011
	.long	-1076818254
	.long	945341623
	.long	-1132473629
	.long	-1062628779
	.long	1068827824
	.long	338563919
	.long	1012252756
	.long	-1024233009
	.long	1065615910
	.long	-58891739
	.long	-1141078608
	.long	1406504787
	.long	-1079937045
	.long	-1499372931
	.long	1067023200
	.long	-1988789128
	.long	-1082070569
	.long	222510294
	.long	-1083792891
	.long	-1671578209
	.long	1064835007
	.long	0
	.long	1072562176
	.long	-1223325107
	.long	1072175568
	.long	701741335
	.long	-1132726430
	.long	-1107226496
	.long	1071712251
	.long	-1107226496
	.long	-1132394501
	.long	1082740685
	.long	-1076822293
	.long	-1051349094
	.long	1014430649
	.long	-74373641
	.long	1068830298
	.long	791876528
	.long	1010136930
	.long	-2076465830
	.long	1065527234
	.long	1069651895
	.long	1008683412
	.long	1263200373
	.long	-1079956526
	.long	-206458386
	.long	1067007968
	.long	-725481413
	.long	-1082059411
	.long	204843676
	.long	-1083902405
	.long	-439335377
	.long	1064792272
	.long	0
	.long	1072570368
	.long	905969669
	.long	1072179920
	.long	-829272796
	.long	1014257967
	.long	-1186477897
	.long	1071707908
	.long	1218859443
	.long	1011051735
	.long	305614939
	.long	-1076826339
	.long	461814225
	.long	1013523355
	.long	2141998625
	.long	1068832601
	.long	-1077014986
	.long	-1136482100
	.long	996097757
	.long	1065440073
	.long	-2000223527
	.long	-1138368015
	.long	-1595992151
	.long	-1079975828
	.long	2119612593
	.long	1066992590
	.long	1013980720
	.long	-1082049096
	.long	818519623
	.long	-1084008933
	.long	-708838176
	.long	1064750070
	.long	0
	.long	1072578560
	.long	-976359902
	.long	1072184254
	.long	83871427
	.long	1013125233
	.long	-2046356376
	.long	1071703581
	.long	1505626569
	.long	-1133458236
	.long	1685779339
	.long	-1076830392
	.long	1115433307
	.long	1014879356
	.long	1056068215
	.long	1068834735
	.long	-1475336537
	.long	1004471658
	.long	-733765237
	.long	1065354412
	.long	-2122779863
	.long	1006794256
	.long	-72112552
	.long	-1079994949
	.long	-1484415674
	.long	1066977076
	.long	-285304191
	.long	-1082039603
	.long	-2093946176
	.long	-1084112513
	.long	-467549686
	.long	1064708415
	.long	0
	.long	1072586752
	.long	1985214443
	.long	1072188572
	.long	-32883057
	.long	-1137166831
	.long	712995253
	.long	1071699270
	.long	1500006598
	.long	1014329441
	.long	-1261481562
	.long	-1076834451
	.long	1528347532
	.long	1011653035
	.long	608723495
	.long	1068836703
	.long	-1155575978
	.long	1012615769
	.long	908299186
	.long	1065187262
	.long	-890475614
	.long	1008589777
	.long	-515871536
	.long	-1080013887
	.long	2140591818
	.long	1066961438
	.long	-838876288
	.long	-1082030907
	.long	307634059
	.long	-1084213183
	.long	1300342376
	.long	1064667321
	.long	0
	.long	1072594944
	.long	1466745541
	.long	1072192873
	.long	2143860931
	.long	1013364334
	.long	-1401619748
	.long	1071694974
	.long	2008750215
	.long	-1132448264
	.long	2122449577
	.long	-1076838515
	.long	525149015
	.long	1014595656
	.long	326261875
	.long	1068838508
	.long	-172909075
	.long	-1137392721
	.long	2107385617
	.long	1065021859
	.long	452004237
	.long	1005223623
	.long	-1237069537
	.long	-1080032641
	.long	-950618153
	.long	1066945686
	.long	1815913390
	.long	-1082022986
	.long	323326534
	.long	-1084394384
	.long	510712228
	.long	1064626799
	.long	0
	.long	1072603136
	.long	2029550309
	.long	1072197157
	.long	928841268
	.long	1015030449
	.long	288461060
	.long	1071690695
	.long	622142745
	.long	1015726996
	.long	985169034
	.long	-1076842584
	.long	666047364
	.long	1014124326
	.long	-385556360
	.long	1068840152
	.long	-124484829
	.long	-1135231010
	.long	-1480905221
	.long	1064859372
	.long	-1970815408
	.long	-1139183338
	.long	2136970676
	.long	-1080069139
	.long	-249810330
	.long	1066929831
	.long	201532297
	.long	-1082015818
	.long	-1947007583
	.long	-1084584329
	.long	1053240941
	.long	1064586860
	.long	0
	.long	1072611328
	.long	-354657750
	.long	1072201424
	.long	-648547140
	.long	1014017882
	.long	1569128883
	.long	1071686431
	.long	641526071
	.long	-1132029147
	.long	1618312067
	.long	-1076846658
	.long	-1452426769
	.long	1014397695
	.long	2052627195
	.long	1068841640
	.long	138739590
	.long	-1135903090
	.long	-1393449380
	.long	1064699772
	.long	-1322045015
	.long	-1139170382
	.long	1193751531
	.long	-1080105904
	.long	568672727
	.long	1066913884
	.long	-1650635268
	.long	-1082009382
	.long	-741467318
	.long	-1084768699
	.long	2092302407
	.long	1064547514
	.long	0
	.long	1072619520
	.long	-1123929831
	.long	1072205675
	.long	632006240
	.long	-1135399651
	.long	-1781455162
	.long	1071682183
	.long	-322160730
	.long	1015392979
	.long	1688105831
	.long	-1076850736
	.long	-316563161
	.long	1014098284
	.long	-1786853104
	.long	1068842973
	.long	-1349016063
	.long	-1137468381
	.long	1511581090
	.long	1064543030
	.long	1198879198
	.long	-1138789851
	.long	-737918667
	.long	-1080142295
	.long	846753220
	.long	1066897853
	.long	-1108332348
	.long	-1082003655
	.long	-501511221
	.long	-1084947578
	.long	-1750433870
	.long	1064508770
	.long	0
	.long	1072627712
	.long	-11013640
	.long	1072209909
	.long	1578596891
	.long	-1132110950
	.long	-1107820280
	.long	1071677951
	.long	-2146401792
	.long	1013971935
	.long	-1174337240
	.long	-1076854818
	.long	2075001493
	.long	1010357119
	.long	20375199
	.long	1068844155
	.long	232400781
	.long	1011888490
	.long	1427126599
	.long	1064389116
	.long	-2069999521
	.long	1008168598
	.long	-68574482
	.long	-1080178309
	.long	-1333076567
	.long	1066881748
	.long	-1254043386
	.long	-1081998616
	.long	-2053250071
	.long	-1085121051
	.long	1258135329
	.long	1064470636
	.long	0
	.long	1072635904
	.long	-1043382785
	.long	1072214127
	.long	643226477
	.long	-1132397775
	.long	-646853320
	.long	1071673735
	.long	539762860
	.long	1015659172
	.long	-782724181
	.long	-1076858903
	.long	-603146874
	.long	-1132820917
	.long	2095415277
	.long	1068845187
	.long	693062424
	.long	-1136047730
	.long	1114554060
	.long	1064171362
	.long	-226081600
	.long	-1140707576
	.long	1549159236
	.long	-1080213944
	.long	-538281265
	.long	1066865579
	.long	1981657948
	.long	-1081994244
	.long	282464073
	.long	-1085302249
	.long	1483738779
	.long	1064433118
	.long	0
	.long	1072644096
	.long	341636253
	.long	1072218329
	.long	1994146611
	.long	1015802432
	.long	-347794534
	.long	1071669535
	.long	1705696031
	.long	1014909336
	.long	424911638
	.long	-1076862990
	.long	-1581876465
	.long	1014339874
	.long	-1064559258
	.long	1068846073
	.long	-769641716
	.long	1012634451
	.long	1200403972
	.long	1063874670
	.long	-1573923750
	.long	-1141000493
	.long	1544363320
	.long	-1080249200
	.long	-1146379534
	.long	1066849355
	.long	-1670677023
	.long	-1081990519
	.long	-1533541450
	.long	-1085628093
	.long	-1838095202
	.long	1064396222
	.long	0
	.long	1072652288
	.long	116966603
	.long	1072222514
	.long	-1163355509
	.long	-1132789364
	.long	-167071244
	.long	1071665351
	.long	-959490154
	.long	1013184738
	.long	-23491136
	.long	-1076867081
	.long	2110088217
	.long	1014466364
	.long	2093105441
	.long	1068846816
	.long	-1906539323
	.long	1009856791
	.long	1342595841
	.long	1063583457
	.long	24273313
	.long	1006909080
	.long	751154417
	.long	-1080284075
	.long	-145099927
	.long	1066833084
	.long	1821590122
	.long	-1081987420
	.long	324889681
	.long	-1085943647
	.long	-433208945
	.long	1064359953
	.long	0
	.long	1072660480
	.long	-1449344930
	.long	1072226682
	.long	-1425789087
	.long	-1132543614
	.long	-68165631
	.long	1071661183
	.long	-67633087
	.long	1010835454
	.long	-338689527
	.long	-1076871173
	.long	-1191165678
	.long	-1135345860
	.long	1521446289
	.long	1068847418
	.long	-1068405285
	.long	1012179469
	.long	-961789896
	.long	1063297663
	.long	708053266
	.long	1005612014
	.long	-858389718
	.long	-1080318569
	.long	2865524
	.long	1066816776
	.long	-758053237
	.long	-1081984928
	.long	1717724604
	.long	-1086249090
	.long	1701537514
	.long	1064324316
	.long	0
	.long	1072668672
	.long	205844839
	.long	1072230835
	.long	-1187433475
	.long	1013830090
	.long	-21483954
	.long	1071657031
	.long	1383261612
	.long	1013575254
	.long	1235262533
	.long	-1076875266
	.long	-1302173765
	.long	-1133508007
	.long	-66704081
	.long	1068847881
	.long	-1323747104
	.long	1012775496
	.long	-453810754
	.long	1062778395
	.long	-1483025638
	.long	1006155141
	.long	148155671
	.long	-1080352679
	.long	-29910720
	.long	1066800436
	.long	1654728462
	.long	-1081983022
	.long	1060768492
	.long	-1086764461
	.long	-1605645598
	.long	1064273986
	.long	0
	.long	1072676864
	.long	1055846791
	.long	1072234971
	.long	-446677873
	.long	1015449189
	.long	-4227200
	.long	1071652895
	.long	-4227200
	.long	1008738335
	.long	2126413828
	.long	-1076879361
	.long	-812150783
	.long	-1136520193
	.long	-83881253
	.long	1068848209
	.long	-147151457
	.long	-1137093114
	.long	-59412224
	.long	1062228127
	.long	-18524976
	.long	1005307866
	.long	2100569296
	.long	-1080386407
	.long	-710147561
	.long	1066784075
	.long	1447771209
	.long	-1081981684
	.long	-422319738
	.long	-1087335969
	.long	1005541922
	.long	1064205254
	.long	0
	.long	1072685056
	.long	1369015099
	.long	1072239091
	.long	249296162
	.long	1013165706
	.long	-263170
	.long	1071648775
	.long	-263170
	.long	1000345607
	.long	-269749248
	.long	-1076883457
	.long	-394883
	.long	-1136651767
	.long	-363156746
	.long	1068848404
	.long	-1246252480
	.long	-1135436886
	.long	-3691580
	.long	1061169191
	.long	-1074894157
	.long	995861462
	.long	-1747554157
	.long	-1080419751
	.long	671615663
	.long	1066767700
	.long	-1903612702
	.long	-1081980895
	.long	-26172762
	.long	-1088403337
	.long	244178279
	.long	1064137799
	.long	0
	.long	1072691200
	.long	-734428562
	.long	1072242170
	.long	1844743509
	.long	-1132959207
	.long	2147482623
	.long	1071645696
	.long	-4196352
	.long	-1141899266
	.long	-4199427
	.long	-1076886529
	.long	-1610614274
	.long	-1151335296
	.long	1403673259
	.long	1068848465
	.long	-1683313829
	.long	-1135334838
	.long	2147469291
	.long	1059064322
	.long	1198582080
	.long	-1149370370
	.long	-1718101808
	.long	-1080444507
	.long	1353189891
	.long	1066755413
	.long	-2042272621
	.long	-1081980652
	.long	2147382057
	.long	-1090514425
	.long	476315178
	.long	1064088047
	.align 32
	.type	cij, @object
	.size	cij, 13496
cij:
	.long	1709188174
	.long	1068515334
	.long	2069093664
	.long	1068513907
	.long	-815989902
	.long	1072684831
	.long	-836049726
	.long	-1078976845
	.long	-581393395
	.long	-1076571490
	.long	-653811318
	.long	1068489290
	.long	1307990808
	.long	1070077151
	.long	-1179087656
	.long	1068564477
	.long	1667519232
	.long	1068562844
	.long	-1545678065
	.long	1072684040
	.long	1721506541
	.long	-1078928932
	.long	-149713438
	.long	-1076574615
	.long	701531559
	.long	1068540207
	.long	-808153824
	.long	1070067950
	.long	1517540593
	.long	1068630001
	.long	737026112
	.long	1068628063
	.long	1119186225
	.long	1072682931
	.long	-1231164864
	.long	-1078865225
	.long	958590153
	.long	-1076578990
	.long	1378556420
	.long	1068601235
	.long	1449193569
	.long	1070055085
	.long	-1303229790
	.long	1068695549
	.long	-417886048
	.long	1068693270
	.long	1404343972
	.long	1072681759
	.long	1252750196
	.long	-1078801704
	.long	1490890674
	.long	-1076583605
	.long	-791753704
	.long	1068661770
	.long	1562854359
	.long	1070041554
	.long	91485100
	.long	1068761086
	.long	386070176
	.long	1068758429
	.long	-1334727486
	.long	1072680525
	.long	1692087942
	.long	-1078738417
	.long	-348389741
	.long	-1076588455
	.long	-889761811
	.long	1068721753
	.long	469835696
	.long	1070027369
	.long	-1992063786
	.long	1068826607
	.long	216138944
	.long	1068823533
	.long	-1461036543
	.long	1072679230
	.long	2069125065
	.long	-1078675377
	.long	1946284793
	.long	-1076593536
	.long	-1258917367
	.long	1068781156
	.long	1280576375
	.long	1070012541
	.long	-549427928
	.long	1068892152
	.long	1014224928
	.long	1068888619
	.long	-1309274349
	.long	1072677873
	.long	-880826565
	.long	-1078612558
	.long	723432789
	.long	-1076598850
	.long	-1712470941
	.long	1068839989
	.long	596594777
	.long	1069997083
	.long	-1940976298
	.long	1068957711
	.long	1230647200
	.long	1068953675
	.long	873320087
	.long	1072676455
	.long	1301864877
	.long	-1078549980
	.long	335957489
	.long	-1076604394
	.long	732400230
	.long	1068898218
	.long	1352914860
	.long	1069981007
	.long	-1153411121
	.long	1069023247
	.long	-1104358496
	.long	1069018663
	.long	971805254
	.long	1072674976
	.long	836355955
	.long	-1078487689
	.long	-1227063950
	.long	-1076610163
	.long	-694859809
	.long	1068955786
	.long	-164376948
	.long	1069964326
	.long	969878399
	.long	1069088754
	.long	-1617453440
	.long	1069083575
	.long	1129645793
	.long	1072673437
	.long	1773784501
	.long	-1078425702
	.long	-2046906455
	.long	-1076616152
	.long	-1239868914
	.long	1069012666
	.long	-1479832651
	.long	1069947055
	.long	1848575858
	.long	1069154297
	.long	-752184192
	.long	1069148474
	.long	77851712
	.long	1072671837
	.long	1408690839
	.long	-1078363966
	.long	144455414
	.long	-1076622365
	.long	-2007307401
	.long	1069068892
	.long	1674239201
	.long	1069929207
	.long	1662431383
	.long	1069219842
	.long	-836449280
	.long	1069213324
	.long	-1819517850
	.long	1072670176
	.long	1995525707
	.long	-1078302525
	.long	378981756
	.long	-1076628797
	.long	1915366580
	.long	1069124411
	.long	-1872253598
	.long	1069910796
	.long	1615812785
	.long	1069285384
	.long	90507136
	.long	1069278119
	.long	1517954931
	.long	1072668456
	.long	1029357732
	.long	-1078241393
	.long	-144240225
	.long	-1076635445
	.long	-941417592
	.long	1069179197
	.long	-178698907
	.long	1069891837
	.long	1825197754
	.long	1069350914
	.long	-1054242176
	.long	1069342846
	.long	-388797366
	.long	1072666676
	.long	1540015614
	.long	-1078180589
	.long	-1957546255
	.long	-1076642303
	.long	781785456
	.long	1069233223
	.long	-1196575885
	.long	1069872346
	.long	-1296793654
	.long	1069416436
	.long	1241849216
	.long	1069407510
	.long	2027735189
	.long	1072664838
	.long	-258578364
	.long	-1078120119
	.long	1951373919
	.long	-1076649370
	.long	-306711942
	.long	1069286470
	.long	1480564426
	.long	1069852338
	.long	-1449108512
	.long	1069481994
	.long	-1547852480
	.long	1069472150
	.long	462137727
	.long	1072662940
	.long	579315319
	.long	-1078059951
	.long	-306106181
	.long	-1076656648
	.long	-217645232
	.long	1069338955
	.long	-1503156437
	.long	1069831828
	.long	-1165007906
	.long	1069547514
	.long	1778272128
	.long	1069536693
	.long	1214504061
	.long	1072660984
	.long	-783186170
	.long	-1078000165
	.long	1183295132
	.long	-1076664124
	.long	456681690
	.long	1069390600
	.long	-1018843652
	.long	1069810833
	.long	1858907560
	.long	1069580282
	.long	171533312
	.long	1069574352
	.long	-235078549
	.long	1072658969
	.long	-2044858145
	.long	-1077940724
	.long	1515744268
	.long	-1076671802
	.long	412967802
	.long	1069441423
	.long	425163220
	.long	1069789370
	.long	860038931
	.long	1069613059
	.long	-234630752
	.long	1069606576
	.long	-32590749
	.long	1072656896
	.long	-514018918
	.long	-1077908878
	.long	-1193576930
	.long	-1076679680
	.long	-1591779778
	.long	1069491416
	.long	1453229533
	.long	1069767454
	.long	-635680751
	.long	1069645820
	.long	703322624
	.long	1069638754
	.long	2102757175
	.long	1072654767
	.long	187930267
	.long	-1077879529
	.long	866930897
	.long	-1076687747
	.long	-1679540465
	.long	1069540522
	.long	1764047307
	.long	1069745103
	.long	1019888013
	.long	1069678591
	.long	999788192
	.long	1069670906
	.long	1171530153
	.long	1072652580
	.long	-1397978456
	.long	-1077850365
	.long	1659270563
	.long	-1076696007
	.long	799454246
	.long	1069568140
	.long	2013946983
	.long	1069722334
	.long	-641234654
	.long	1069711351
	.long	462546496
	.long	1069703014
	.long	-181528276
	.long	1072650336
	.long	-438426732
	.long	-1077821405
	.long	1810762429
	.long	-1076704451
	.long	1577540634
	.long	1069591803
	.long	-536984262
	.long	1069699164
	.long	-1577313622
	.long	1069744119
	.long	-2094695360
	.long	1069735093
	.long	-954616069
	.long	1072648036
	.long	1632714883
	.long	-1077792638
	.long	2138796757
	.long	-1076713080
	.long	2089317378
	.long	1069615015
	.long	742184657
	.long	1069675612
	.long	-1653022370
	.long	1069776901
	.long	1791594208
	.long	1069767150
	.long	-1806898154
	.long	1072645679
	.long	-665090135
	.long	-1077764064
	.long	776647529
	.long	-1076721892
	.long	-1021680167
	.long	1069637773
	.long	319536720
	.long	1069651694
	.long	-1622992102
	.long	1069809668
	.long	1809208160
	.long	1069799155
	.long	-577128822
	.long	1072643267
	.long	-1277361939
	.long	-1077735711
	.long	-129489308
	.long	-1076730877
	.long	-32283098
	.long	1069660050
	.long	1856795712
	.long	1069627428
	.long	-534932063
	.long	1069842436
	.long	-2031088448
	.long	1069831123
	.long	-476641991
	.long	1072640800
	.long	173177258
	.long	-1077707569
	.long	991239517
	.long	-1076740033
	.long	-483276314
	.long	1069681851
	.long	971239575
	.long	1069602833
	.long	-1433173283
	.long	1069875191
	.long	-1940321664
	.long	1069863039
	.long	887040224
	.long	1072638280
	.long	1531154780
	.long	-1077679656
	.long	-343390814
	.long	-1076749355
	.long	-1402287955
	.long	1069703160
	.long	-1997695286
	.long	1069577926
	.long	891553982
	.long	1069907974
	.long	-1013287968
	.long	1069894942
	.long	162398364
	.long	1072635703
	.long	-1178696368
	.long	-1077651940
	.long	5502254
	.long	-1076758848
	.long	-277333015
	.long	1069723997
	.long	-2045434981
	.long	1069552726
	.long	723074945
	.long	1069940735
	.long	1332994048
	.long	1069926784
	.long	-1374613975
	.long	1072633073
	.long	-1869040686
	.long	-1077624466
	.long	-1895464383
	.long	-1076768497
	.long	-1588615017
	.long	1069744326
	.long	-714733632
	.long	1069506982
	.long	1147201249
	.long	1069973512
	.long	77946400
	.long	1069958600
	.long	-1885114683
	.long	1072630389
	.long	-701519741
	.long	-1077597207
	.long	1772568648
	.long	-1076778307
	.long	377088770
	.long	1069764165
	.long	-138326979
	.long	1069455518
	.long	-1634097082
	.long	1070006267
	.long	1261829440
	.long	1069990352
	.long	1056501653
	.long	1072627654
	.long	1990393443
	.long	-1077570197
	.long	-574124577
	.long	-1076788264
	.long	-1402966682
	.long	1069783485
	.long	118302826
	.long	1069403578
	.long	268227728
	.long	1070039048
	.long	-1252566816
	.long	1070022085
	.long	1165893733
	.long	1072624864
	.long	2104490428
	.long	-1077543402
	.long	-1615998076
	.long	-1076798377
	.long	1944399154
	.long	1069802311
	.long	-1704612277
	.long	1069351196
	.long	344881601
	.long	1070071814
	.long	729779360
	.long	1070053761
	.long	1880524781
	.long	1072622023
	.long	236909421
	.long	-1077516857
	.long	-860045909
	.long	-1076808631
	.long	1550955496
	.long	1069820615
	.long	1048680462
	.long	1069298411
	.long	256408599
	.long	1070104582
	.long	-1139919296
	.long	1070085393
	.long	-757771479
	.long	1072619130
	.long	937837754
	.long	-1077490553
	.long	1616475479
	.long	-1076819026
	.long	333606846
	.long	1069838403
	.long	1873906676
	.long	1069245258
	.long	-749630090
	.long	1070137348
	.long	-2005614336
	.long	1070116979
	.long	785803067
	.long	1072616187
	.long	711899843
	.long	-1077464495
	.long	1763975983
	.long	-1076829558
	.long	1941749823
	.long	1069855669
	.long	-1938665345
	.long	1069191774
	.long	-755888804
	.long	1070170111
	.long	1370446112
	.long	1070148515
	.long	1026704543
	.long	1072613193
	.long	-1022876321
	.long	-1077438689
	.long	-713895281
	.long	-1076840222
	.long	863258878
	.long	1069872410
	.long	-808729056
	.long	1069137995
	.long	-1082068896
	.long	1070202885
	.long	1855883616
	.long	1070180014
	.long	519176423
	.long	1072610148
	.long	1855382977
	.long	-1077413125
	.long	-328542649
	.long	-1076851018
	.long	-180548698
	.long	1069888629
	.long	1144242702
	.long	1069083958
	.long	-554915612
	.long	1070235639
	.long	-1803192096
	.long	1070211446
	.long	848296331
	.long	1072607055
	.long	1477745436
	.long	-1077387831
	.long	-114056789
	.long	-1076861932
	.long	1623480975
	.long	1069904311
	.long	-1101479160
	.long	1069029697
	.long	2112869715
	.long	1070268415
	.long	-1631949952
	.long	1070242850
	.long	279469584
	.long	1072603911
	.long	2141055585
	.long	-1077362778
	.long	1514622607
	.long	-1076872973
	.long	818030034
	.long	1069919472
	.long	-798850769
	.long	1068975249
	.long	1721316005
	.long	1070301182
	.long	-805904416
	.long	1070274196
	.long	421647239
	.long	1072600719
	.long	-529147558
	.long	-1077337992
	.long	1382519748
	.long	-1076884128
	.long	1938041426
	.long	1069934096
	.long	-986505894
	.long	1068920649
	.long	-1270392333
	.long	1070333945
	.long	1349755552
	.long	1070305489
	.long	1448706835
	.long	1072597479
	.long	-809951171
	.long	-1077313471
	.long	-1093067977
	.long	-1076903236
	.long	1833943804
	.long	1069948185
	.long	-1810803842
	.long	1068865932
	.long	1432941548
	.long	1070366727
	.long	1244781536
	.long	1070336748
	.long	418541736
	.long	1072594190
	.long	-182275790
	.long	-1077289202
	.long	561627714
	.long	-1076925996
	.long	912551885
	.long	1069961747
	.long	-597061483
	.long	1068811132
	.long	1612347807
	.long	1070399480
	.long	1239115424
	.long	1070367928
	.long	-1594984434
	.long	1072590856
	.long	977287476
	.long	-1077265221
	.long	-728711630
	.long	-1076948945
	.long	58540343
	.long	1069974762
	.long	-638810790
	.long	1068756284
	.long	2076120215
	.long	1070432249
	.long	503352544
	.long	1070399071
	.long	-263842361
	.long	1072587474
	.long	363519532
	.long	-1077241499
	.long	189052546
	.long	-1076972103
	.long	-1829991081
	.long	1069987247
	.long	1624781230
	.long	1068701422
	.long	1075878688
	.long	1070465032
	.long	-1320835232
	.long	1070430173
	.long	-1105330703
	.long	1072584045
	.long	562474767
	.long	-1077218040
	.long	-1809823587
	.long	-1076995463
	.long	1162865452
	.long	1069999202
	.long	-762029428
	.long	1068646578
	.long	-1062300612
	.long	1070497792
	.long	1017184512
	.long	1070461201
	.long	1508323467
	.long	1072580573
	.long	-1808140641
	.long	-1077194872
	.long	2039342592
	.long	-1077018989
	.long	-1893305463
	.long	1070010613
	.long	445265726
	.long	1068591787
	.long	1834166275
	.long	1070530563
	.long	-884056960
	.long	1070492183
	.long	-472844606
	.long	1072577054
	.long	1848768360
	.long	-1077171973
	.long	-425491741
	.long	-1077042697
	.long	-1284848448
	.long	1070021493
	.long	-1555625206
	.long	1068537079
	.long	1220063171
	.long	1070563322
	.long	-513456800
	.long	1070523099
	.long	1139977015
	.long	1072573493
	.long	1501693910
	.long	-1077149361
	.long	519995546
	.long	-1077066561
	.long	1770673694
	.long	1070031836
	.long	-1479575389
	.long	1068466032
	.long	-348958802
	.long	1070596098
	.long	2007705056
	.long	1070553979
	.long	2024128807
	.long	1072569885
	.long	-984230543
	.long	-1077127016
	.long	-1224290426
	.long	-1077090599
	.long	136574531
	.long	1070041653
	.long	194058472
	.long	1068357145
	.long	-1464891650
	.long	1070612476
	.long	-1254487040
	.long	1070584779
	.long	-36983669
	.long	1072566236
	.long	1815234303
	.long	-1077104969
	.long	-689857837
	.long	-1077114768
	.long	-758870383
	.long	1070050931
	.long	84999600
	.long	1068248614
	.long	-745962313
	.long	1070628860
	.long	1462949024
	.long	1070605815
	.long	1237416627
	.long	1072562544
	.long	-1694609368
	.long	-1077083198
	.long	-1708386253
	.long	-1077139087
	.long	1761877455
	.long	1070059685
	.long	-191379885
	.long	1068140499
	.long	517123543
	.long	1070645247
	.long	-847285216
	.long	1070621165
	.long	-627656880
	.long	1072558808
	.long	-948467682
	.long	-1077061710
	.long	-2037205757
	.long	-1077163542
	.long	1118566887
	.long	1070067913
	.long	78916532
	.long	1068032862
	.long	-1471062822
	.long	1070661635
	.long	41162400
	.long	1070636489
	.long	881432912
	.long	1072555031
	.long	2038689252
	.long	-1077040506
	.long	-1718155594
	.long	-1077188125
	.long	-1326373157
	.long	1070075617
	.long	401513387
	.long	1067925758
	.long	-206594875
	.long	1070678017
	.long	-1226724448
	.long	1070651776
	.long	-532601215
	.long	1072551213
	.long	1751364235
	.long	-1077019599
	.long	-983855782
	.long	-1077212815
	.long	-1502405972
	.long	1070082797
	.long	-952379152
	.long	1067819244
	.long	-1240324855
	.long	1070694403
	.long	-1440847968
	.long	1070667037
	.long	772546354
	.long	1072547355
	.long	-1178965122
	.long	-1076998978
	.long	-1866317028
	.long	-1077237618
	.long	-1949574562
	.long	1070089460
	.long	1357219327
	.long	1067713377
	.long	-1499291309
	.long	1070710783
	.long	-408842816
	.long	1070682262
	.long	-517894337
	.long	1072543457
	.long	-1776167262
	.long	-1076978655
	.long	576879364
	.long	-1077262512
	.long	-7630706
	.long	1070095605
	.long	-1741438762
	.long	1067608209
	.long	758405807
	.long	1070727165
	.long	87816032
	.long	1070697459
	.long	-868562135
	.long	1072539520
	.long	715609774
	.long	-1076958622
	.long	1206753171
	.long	-1077287501
	.long	-2018814934
	.long	1070101240
	.long	361668250
	.long	1067503794
	.long	-246615974
	.long	1070743548
	.long	1297515616
	.long	1070712626
	.long	1442000189
	.long	1072535544
	.long	567564232
	.long	-1076938880
	.long	-261769147
	.long	-1077312578
	.long	-72938280
	.long	1070106367
	.long	-1363649332
	.long	1067349995
	.long	195981257
	.long	1070759934
	.long	-1708670624
	.long	1070727763
	.long	1176049518
	.long	1072531529
	.long	1544210924
	.long	-1076919431
	.long	1583632789
	.long	-1077337732
	.long	-198818539
	.long	1070110991
	.long	-424071579
	.long	1067144476
	.long	-1567119394
	.long	1070776321
	.long	-1397151808
	.long	1070742871
	.long	-459960394
	.long	1072527475
	.long	1129783881
	.long	-1076900274
	.long	-1623891725
	.long	-1077362959
	.long	-545321381
	.long	1070115116
	.long	292502297
	.long	1066940760
	.long	1919377770
	.long	1070792702
	.long	-1292666688
	.long	1070757941
	.long	328230278
	.long	1072523387
	.long	-1344660977
	.long	-1076884487
	.long	-1047024259
	.long	-1077388236
	.long	513405095
	.long	1070118745
	.long	1696973669
	.long	1066738938
	.long	1209350944
	.long	1070809086
	.long	1781521600
	.long	1070772982
	.long	-131597340
	.long	1072519260
	.long	-1269143272
	.long	-1076875207
	.long	1576812402
	.long	-1077413570
	.long	-987693022
	.long	1070121883
	.long	378517038
	.long	1066539102
	.long	-2128455782
	.long	1070825475
	.long	-1351695904
	.long	1070787995
	.long	-2026954541
	.long	1072515097
	.long	525078577
	.long	-1076866072
	.long	1646263673
	.long	-1077438958
	.long	-610427396
	.long	1070124537
	.long	-2134473425
	.long	1066280884
	.long	1056149825
	.long	1070841859
	.long	1181271808
	.long	1070802971
	.long	408489242
	.long	1072510900
	.long	-1544608416
	.long	-1076857090
	.long	808659066
	.long	-1077464375
	.long	-902933416
	.long	1070126710
	.long	-670481231
	.long	1065889669
	.long	1677332017
	.long	1070858241
	.long	2069254144
	.long	1070817912
	.long	1202197466
	.long	1072506668
	.long	1228643720
	.long	-1076848257
	.long	1034550844
	.long	-1077489820
	.long	-1043516623
	.long	1070128408
	.long	-149983737
	.long	1065502929
	.long	1946620050
	.long	1070874627
	.long	252329984
	.long	1070832824
	.long	1000634987
	.long	1072502401
	.long	-663189177
	.long	-1076839572
	.long	1486198438
	.long	-1077515294
	.long	-910955490
	.long	1070129637
	.long	-770724986
	.long	1064888424
	.long	-931073536
	.long	1070891008
	.long	-553667488
	.long	1070847697
	.long	-891529364
	.long	1072498101
	.long	-1579986238
	.long	-1076831038
	.long	1031611764
	.long	-1077540776
	.long	52464576
	.long	1070130403
	.long	1668933426
	.long	1063962889
	.long	1301795531
	.long	1070907392
	.long	-207429088
	.long	1070862539
	.long	-1149315823
	.long	1072493768
	.long	930507337
	.long	-1076822652
	.long	1521205564
	.long	-1077566270
	.long	-938628021
	.long	1070130710
	.long	-1337551422
	.long	1061173418
	.long	-1818799201
	.long	1070923773
	.long	111664832
	.long	1070877346
	.long	-817390114
	.long	1072489403
	.long	1293035908
	.long	-1076814417
	.long	-1325249040
	.long	-1077591762
	.long	455890445
	.long	1070130567
	.long	-1793324915
	.long	-1084072536
	.long	-217812641
	.long	1070940159
	.long	2032089344
	.long	1070892122
	.long	-1537339432
	.long	1072485005
	.long	-1922960863
	.long	-1076806329
	.long	-739656807
	.long	-1077617256
	.long	253236487
	.long	1070129978
	.long	-796143012
	.long	-1082901453
	.long	-393305210
	.long	1070956546
	.long	-176826688
	.long	1070906864
	.long	33773065
	.long	1072480576
	.long	-1207433442
	.long	-1076798390
	.long	-1298898993
	.long	-1077642738
	.long	-66755513
	.long	1070128949
	.long	-1341504051
	.long	-1082187759
	.long	1895093648
	.long	1070972925
	.long	676166368
	.long	1070921565
	.long	-220292938
	.long	1072476117
	.long	-1565249726
	.long	-1076790604
	.long	472406674
	.long	-1077668187
	.long	1360882778
	.long	1070127490
	.long	1996523013
	.long	-1081807679
	.long	-1223856557
	.long	1070989309
	.long	-1909459712
	.long	1070936235
	.long	852640413
	.long	1072471628
	.long	890832513
	.long	-1076782964
	.long	-1082883303
	.long	-1077693620
	.long	-1730959853
	.long	1070125604
	.long	-1440129121
	.long	-1081461804
	.long	1551079906
	.long	1071005692
	.long	1325543040
	.long	1070950869
	.long	1490884623
	.long	1072467109
	.long	493886948
	.long	-1076775474
	.long	-1484756390
	.long	-1077719017
	.long	1455430905
	.long	1070123300
	.long	-1809819167
	.long	-1081121575
	.long	-1709520942
	.long	1071022077
	.long	-184536608
	.long	1070965469
	.long	-822120981
	.long	1072462560
	.long	2089559131
	.long	-1076768132
	.long	-961899713
	.long	-1077744379
	.long	-574238945
	.long	1070120583
	.long	-711706513
	.long	-1080934474
	.long	1870369619
	.long	1071038461
	.long	-1260362048
	.long	1070980033
	.long	1164663066
	.long	1072457984
	.long	-1026109270
	.long	-1076760939
	.long	-1324316937
	.long	-1077769693
	.long	-572379814
	.long	1070117462
	.long	1218282916
	.long	-1080770150
	.long	-265649180
	.long	1071054843
	.long	1780710848
	.long	1070994560
	.long	1288181619
	.long	1072453380
	.long	1267739082
	.long	-1076753894
	.long	-1980368100
	.long	-1077794953
	.long	-1693849327
	.long	1070113944
	.long	319635866
	.long	-1080608788
	.long	311173434
	.long	1071071230
	.long	1112118400
	.long	1071009054
	.long	312216880
	.long	1072448748
	.long	-2089258407
	.long	-1076746996
	.long	-715397892
	.long	-1077820161
	.long	917689859
	.long	1070110035
	.long	776200006
	.long	-1080450428
	.long	-368255878
	.long	1071087611
	.long	95478304
	.long	1071023508
	.long	1752249024
	.long	1072444090
	.long	1920158078
	.long	-1076740247
	.long	-851857353
	.long	-1077845298
	.long	-91026769
	.long	1070105743
	.long	-1537915886
	.long	-1080295107
	.long	-242296136
	.long	1071104003
	.long	1093123808
	.long	1071037934
	.long	-1072298051
	.long	1072439403
	.long	1609445186
	.long	-1076733641
	.long	813130209
	.long	-1077870380
	.long	1576977750
	.long	1070101074
	.long	2035874163
	.long	-1080142858
	.long	1106467916
	.long	1071120381
	.long	-630847584
	.long	1071052310
	.long	-841186249
	.long	1072434695
	.long	-1078666518
	.long	-1076727188
	.long	-522513294
	.long	-1077895365
	.long	714451483
	.long	1070096041
	.long	-663564382
	.long	-1080013497
	.long	650626155
	.long	1071136771
	.long	-1766277632
	.long	1071066661
	.long	1289674552
	.long	1072429959
	.long	-795605773
	.long	-1076720876
	.long	-402955790
	.long	-1077920287
	.long	-1397539863
	.long	1070090643
	.long	-1692539385
	.long	-1079940490
	.long	-896487047
	.long	1071153155
	.long	-1593531872
	.long	1071080970
	.long	-231283978
	.long	1072425199
	.long	430525919
	.long	-1076714711
	.long	139894913
	.long	-1077954098
	.long	871826048
	.long	1070084895
	.long	-267188020
	.long	-1079869062
	.long	-378982443
	.long	1071169539
	.long	-256718080
	.long	1071095241
	.long	-225024875
	.long	1072420416
	.long	-43072790
	.long	-1076708693
	.long	-527524819
	.long	-1078003566
	.long	51934179
	.long	1070078802
	.long	-1408102083
	.long	-1079799223
	.long	901667935
	.long	1071185918
	.long	-1110646848
	.long	1071109470
	.long	1856753527
	.long	1072415612
	.long	762639429
	.long	-1076702820
	.long	-432067365
	.long	-1078052820
	.long	242213550
	.long	1070072374
	.long	241609537
	.long	-1079730983
	.long	-1433832243
	.long	1071202306
	.long	-1196435328
	.long	1071123670
	.long	1147718842
	.long	1072410782
	.long	-326834554
	.long	-1076697089
	.long	-2000266776
	.long	-1078101898
	.long	-1024371616
	.long	1070065612
	.long	-1273965587
	.long	-1079664352
	.long	-527499798
	.long	1071218690
	.long	881158432
	.long	1071137829
	.long	1059652728
	.long	1072405931
	.long	-1149112849
	.long	-1076691502
	.long	837190552
	.long	-1078150747
	.long	-1025789669
	.long	1070058531
	.long	-1740210568
	.long	-1079599335
	.long	1965640555
	.long	1071235074
	.long	698311168
	.long	1071151949
	.long	-560541921
	.long	1072401058
	.long	-1941092910
	.long	-1076686058
	.long	-305246790
	.long	-1078199369
	.long	-1995964727
	.long	1070051137
	.long	121741141
	.long	-1079535938
	.long	-203124500
	.long	1071251458
	.long	-1038465792
	.long	1071166031
	.long	996079581
	.long	1072396165
	.long	-1901170483
	.long	-1076680756
	.long	-727826214
	.long	-1078247757
	.long	1301542890
	.long	1070043437
	.long	-1641979798
	.long	-1079474167
	.long	1166303670
	.long	1071267839
	.long	2002047680
	.long	1071180072
	.long	1779281678
	.long	1072391252
	.long	-601187777
	.long	-1076675597
	.long	1226297347
	.long	-1078295887
	.long	2088068815
	.long	1070035441
	.long	1030753752
	.long	-1079414023
	.long	-2015286610
	.long	1071284226
	.long	-1512058880
	.long	1071194080
	.long	-1596445179
	.long	1072386317
	.long	-1486171374
	.long	-1076670576
	.long	-554945107
	.long	-1078343784
	.long	-1978731458
	.long	1070027152
	.long	141750216
	.long	-1079355509
	.long	1235774596
	.long	1071300609
	.long	1548031712
	.long	1071208046
	.long	-586942794
	.long	1072381364
	.long	689708930
	.long	-1076665696
	.long	66510688
	.long	-1078391403
	.long	621354642
	.long	1070018584
	.long	1467613739
	.long	-1079298626
	.long	793559777
	.long	1071316991
	.long	-1809685184
	.long	1071221972
	.long	-1835589622
	.long	1072376393
	.long	1364458753
	.long	-1076660956
	.long	-1371676034
	.long	-1078438749
	.long	2125722417
	.long	1070009742
	.long	-1230153970
	.long	-1079243373
	.long	-1024233186
	.long	1071333376
	.long	30262464
	.long	1071235863
	.long	-362579941
	.long	1072371402
	.long	912635465
	.long	-1076656353
	.long	1064369094
	.long	-1078485824
	.long	102628398
	.long	1070000633
	.long	-667420221
	.long	-1079189748
	.long	-1485315712
	.long	1071349760
	.long	-210464992
	.long	1071249712
	.long	263198250
	.long	1072366395
	.long	630688939
	.long	-1076651888
	.long	1551260331
	.long	-1078532606
	.long	-1676224904
	.long	1069991266
	.long	-1834914113
	.long	-1079137748
	.long	142480621
	.long	1071366141
	.long	-1089611008
	.long	1071263520
	.long	483290772
	.long	1072361371
	.long	-1955339325
	.long	-1076647561
	.long	166711756
	.long	-1078579081
	.long	1480625466
	.long	1069981652
	.long	-756776941
	.long	-1079087370
	.long	-267884669
	.long	1071382528
	.long	-1924890496
	.long	1071277295
	.long	899222362
	.long	1072356328
	.long	656677766
	.long	-1076643367
	.long	-1039253503
	.long	-1078625274
	.long	-1863923541
	.long	1069971791
	.long	-1704785369
	.long	-1079038608
	.long	-2042734344
	.long	1071398913
	.long	473570208
	.long	1071291028
	.long	904756690
	.long	1072351270
	.long	201808720
	.long	-1076639309
	.long	-1540785860
	.long	-1078671145
	.long	-1785709534
	.long	1069961698
	.long	-252135863
	.long	-1078991457
	.long	1818411967
	.long	1071415292
	.long	1477072960
	.long	1071304716
	.long	1657919496
	.long	1072346198
	.long	-1397029140
	.long	-1076635387
	.long	1109279452
	.long	-1078716681
	.long	-446656771
	.long	1069951382
	.long	-246717286
	.long	-1078965307
	.long	884122121
	.long	1071431682
	.long	-158385344
	.long	1071318373
	.long	507365702
	.long	1072341108
	.long	-488937650
	.long	-1076631595
	.long	-1534902274
	.long	-1078761922
	.long	-990889685
	.long	1069940841
	.long	951124481
	.long	-1078943330
	.long	-1429865991
	.long	1071448060
	.long	1506041024
	.long	1071331982
	.long	-768041948
	.long	1072336006
	.long	-2090683701
	.long	-1076627937
	.long	-960018778
	.long	-1078806799
	.long	1114450246
	.long	1069930097
	.long	1233243765
	.long	-1078922147
	.long	226495588
	.long	1071464452
	.long	-2001771136
	.long	1071345561
	.long	1956171474
	.long	1072330887
	.long	-276204096
	.long	-1076624408
	.long	-1096278485
	.long	-1078851372
	.long	675281486
	.long	1069919141
	.long	292573488
	.long	-1078901752
	.long	-1327086336
	.long	1071480830
	.long	589619520
	.long	1071359090
	.long	-1940203904
	.long	1072325758
	.long	-1606482899
	.long	-1076621011
	.long	-1786759012
	.long	-1078895565
	.long	1238993054
	.long	1069907998
	.long	-275360433
	.long	-1078882141
	.long	771552491
	.long	1071497214
	.long	-1625479264
	.long	1071372582
	.long	448956106
	.long	1072320615
	.long	1493847521
	.long	-1076617742
	.long	-1949175899
	.long	-1078939420
	.long	1881474768
	.long	1069896664
	.long	-1381755482
	.long	-1078863306
	.long	-1976255550
	.long	1071513597
	.long	-990403904
	.long	1071386034
	.long	787942323
	.long	1072315459
	.long	384722044
	.long	-1076614601
	.long	-614305605
	.long	-1078982918
	.long	-2031055386
	.long	1069885150
	.long	2119197179
	.long	-1078845242
	.long	-765192768
	.long	1071529981
	.long	1479027008
	.long	1071399447
	.long	-208544309
	.long	1072310290
	.long	418242314
	.long	-1076611587
	.long	-2002417233
	.long	-1079067406
	.long	1291411811
	.long	1069873463
	.long	-157186943
	.long	-1078827943
	.long	938944114
	.long	1071546367
	.long	1739210176
	.long	1071412820
	.long	-691108173
	.long	1072305110
	.long	1946386065
	.long	-1076608699
	.long	992789573
	.long	-1079152951
	.long	90501377
	.long	1069861610
	.long	206163300
	.long	-1078811400
	.long	-1592016881
	.long	1071562749
	.long	-2048772512
	.long	1071426150
	.long	-1571019882
	.long	1072299920
	.long	-1973128625
	.long	-1076605936
	.long	797384322
	.long	-1079237735
	.long	184286085
	.long	1069849601
	.long	252703742
	.long	-1078795608
	.long	856913261
	.long	1071579135
	.long	-1657746400
	.long	1071439442
	.long	-537310945
	.long	1072294718
	.long	2120923822
	.long	-1076603296
	.long	1049274765
	.long	-1079321780
	.long	634978173
	.long	1069837439
	.long	-1491254131
	.long	-1078780559
	.long	-1857321618
	.long	1071595521
	.long	-1430144224
	.long	1071452694
	.long	-910408939
	.long	1072289506
	.long	5144209
	.long	-1076600778
	.long	-74440823
	.long	-1079405065
	.long	1062985802
	.long	1069825133
	.long	-829767790
	.long	-1078766244
	.long	-854309848
	.long	1071611907
	.long	-650209728
	.long	1071465905
	.long	628088811
	.long	1072284285
	.long	167231813
	.long	-1076598382
	.long	-313388684
	.long	-1079487575
	.long	724006175
	.long	1069812691
	.long	-811219470
	.long	-1078752655
	.long	5528537
	.long	1071628290
	.long	-180954784
	.long	1071479072
	.long	-1427530075
	.long	1072279055
	.long	954033908
	.long	-1076596107
	.long	-1253552268
	.long	-1079569283
	.long	-118504589
	.long	1069800122
	.long	1051113572
	.long	-1078739783
	.long	-630248792
	.long	1071644667
	.long	-1272907104
	.long	1071492195
	.long	-313717589
	.long	1072273818
	.long	-1077125006
	.long	-1076593952
	.long	633305674
	.long	-1079650180
	.long	-174481691
	.long	1069787435
	.long	-135226104
	.long	-1078727621
	.long	-1504634680
	.long	1071652863
	.long	737610752
	.long	1071505285
	.long	-321443099
	.long	1072268570
	.long	-1890085691
	.long	-1076591913
	.long	776602123
	.long	-1079730328
	.long	631330522
	.long	1069774626
	.long	435519588
	.long	-1078716157
	.long	-1676999338
	.long	1071661057
	.long	-97803776
	.long	1071518333
	.long	146897493
	.long	1072263315
	.long	-1787134211
	.long	-1076589991
	.long	642314939
	.long	-1079809674
	.long	1495624113
	.long	1069761707
	.long	58011099
	.long	-1078705384
	.long	-1035664933
	.long	1071669247
	.long	-1533690688
	.long	1071531335
	.long	1327651705
	.long	1072258054
	.long	560199096
	.long	-1076588185
	.long	1266268319
	.long	-1079888173
	.long	-1255098434
	.long	1069748692
	.long	1301484662
	.long	-1078695292
	.long	-2041502212
	.long	1071677438
	.long	710913376
	.long	1071544297
	.long	1544794346
	.long	1072252786
	.long	1865657290
	.long	-1076586494
	.long	-560694165
	.long	-1079965862
	.long	-657072101
	.long	1069735581
	.long	-904410528
	.long	-1078685871
	.long	276737061
	.long	1071685634
	.long	136589696
	.long	1071557225
	.long	-214334901
	.long	1072247508
	.long	1900409973
	.long	-1076584915
	.long	5706788
	.long	-1080052264
	.long	-2049847420
	.long	1069722374
	.long	2109455038
	.long	-1078677110
	.long	1937171924
	.long	1071693823
	.long	-140511904
	.long	1071570101
	.long	-1897810798
	.long	1072242229
	.long	1229554771
	.long	-1076583449
	.long	-1100456849
	.long	-1080204316
	.long	-994971598
	.long	1069709094
	.long	-1636905926
	.long	-1078669000
	.long	387313198
	.long	1071702014
	.long	-1679705216
	.long	1071582939
	.long	-644629152
	.long	1072236943
	.long	-1685788409
	.long	-1076582094
	.long	-528631202
	.long	-1080354726
	.long	-220747772
	.long	1069695736
	.long	-1915051200
	.long	-1078661530
	.long	2134734700
	.long	1071710209
	.long	1629085952
	.long	1071595743
	.long	-79381253
	.long	1072231649
	.long	383993470
	.long	-1076580847
	.long	-818510814
	.long	-1080503548
	.long	1936132270
	.long	1069682301
	.long	-676108915
	.long	-1078654690
	.long	-41125019
	.long	1071718398
	.long	-1882057376
	.long	1071608496
	.long	1198586634
	.long	1072226355
	.long	1544513411
	.long	-1076579710
	.long	2147353444
	.long	-1080650580
	.long	-450248884
	.long	1069668811
	.long	745793008
	.long	-1078648468
	.long	-1348211087
	.long	1071726592
	.long	-483242656
	.long	1071621214
	.long	-1611234081
	.long	1072221053
	.long	397267789
	.long	-1076578679
	.long	1730637424
	.long	-1080795997
	.long	-1577323599
	.long	1069655257
	.long	1606034292
	.long	-1078642855
	.long	1932747143
	.long	1071734783
	.long	1038388480
	.long	1071633887
	.long	213792070
	.long	1072215750
	.long	-1691804725
	.long	-1076577755
	.long	1605833420
	.long	-1080939665
	.long	1440910091
	.long	1069641656
	.long	1665090108
	.long	-1078637839
	.long	1816427394
	.long	1071742975
	.long	34139328
	.long	1071645596
	.long	1223776550
	.long	1072210442
	.long	-1518302525
	.long	-1076576935
	.long	1592749866
	.long	-1081081651
	.long	-337294681
	.long	1069628006
	.long	1161956580
	.long	-1078633409
	.long	-1366086815
	.long	1071751167
	.long	-536273984
	.long	1071651891
	.long	1432768000
	.long	1072205131
	.long	1470061243
	.long	-1076576218
	.long	-2020097238
	.long	-1081362010
	.long	-1781205426
	.long	1069614316
	.long	733753950
	.long	-1078629554
	.long	-1173180848
	.long	1071759359
	.long	-743727072
	.long	1071658166
	.long	-337850620
	.long	1072199817
	.long	-422288447
	.long	-1076575604
	.long	-1285418500
	.long	-1081639140
	.long	-1190456857
	.long	1069600591
	.long	1390198298
	.long	-1078626263
	.long	1262853697
	.long	1071767553
	.long	767580160
	.long	1071664422
	.long	1365400426
	.long	1072194501
	.long	-1342319719
	.long	-1076575090
	.long	1613781450
	.long	-1081912885
	.long	-563752743
	.long	1069586834
	.long	142324583
	.long	-1078623524
	.long	842034995
	.long	1071775744
	.long	-1157927712
	.long	1071670654
	.long	-1578963261
	.long	1072189184
	.long	-1214330682
	.long	-1076574676
	.long	-648156975
	.long	-1082235768
	.long	1713238593
	.long	1069573058
	.long	-1431946323
	.long	-1078621327
	.long	-754300820
	.long	1071783936
	.long	-770289536
	.long	1071676867
	.long	1723065791
	.long	1072183865
	.long	2111852014
	.long	-1076574360
	.long	-2146499281
	.long	-1082769417
	.long	687038173
	.long	1069559260
	.long	-1554919535
	.long	-1078619659
	.long	559809532
	.long	1071792126
	.long	-1623717376
	.long	1071683057
	.long	1220435775
	.long	1072178547
	.long	460930935
	.long	-1076574141
	.long	1650037688
	.long	-1083412893
	.long	-1939977413
	.long	1069543387
	.long	1727962279
	.long	-1078618509
	.long	1748227677
	.long	1071800320
	.long	1687595200
	.long	1071689230
	.long	1169443491
	.long	1072173225
	.long	89866362
	.long	-1076574018
	.long	-1367084892
	.long	-1084677977
	.long	-2046137762
	.long	1069515740
	.long	1893306087
	.long	-1078617866
	.long	-399684272
	.long	1071808512
	.long	157242112
	.long	1071695381
	.long	512964535
	.long	1072167904
	.long	1463578474
	.long	-1076573990
	.long	-988850595
	.long	1060681415
	.long	-909855030
	.long	1069488091
	.long	1171708640
	.long	-1078617718
	.long	540764683
	.long	1071816703
	.long	814032672
	.long	1071701509
	.long	-2061479027
	.long	1072162584
	.long	1720478051
	.long	-1076574055
	.long	-1094022480
	.long	1063421676
	.long	-1687501379
	.long	1069460452
	.long	1465554564
	.long	-1078618054
	.long	-384131693
	.long	1071824897
	.long	-142509536
	.long	1071707619
	.long	1671716247
	.long	1072157262
	.long	1602471021
	.long	-1076574212
	.long	1570537482
	.long	1064362393
	.long	924952800
	.long	1069432809
	.long	930981952
	.long	-1078618862
	.long	-956610232
	.long	1071833087
	.long	1289969728
	.long	1071713706
	.long	1043923231
	.long	1072151944
	.long	-1242898145
	.long	-1076574460
	.long	422339115
	.long	1064854424
	.long	-1238654121
	.long	1069405202
	.long	1594753550
	.long	-1078620131
	.long	969625325
	.long	1071841279
	.long	158379360
	.long	1071719773
	.long	911964827
	.long	1072146626
	.long	-1994001102
	.long	-1076574797
	.long	2019438704
	.long	1065339651
	.long	250475443
	.long	1069377620
	.long	1201415466
	.long	-1078621849
	.long	3027403
	.long	1071849470
	.long	-2114111616
	.long	1071725818
	.long	465475175
	.long	1072141310
	.long	809956551
	.long	-1076575222
	.long	-1889637391
	.long	1065585582
	.long	-2089329512
	.long	1069350077
	.long	1659406251
	.long	-1078624005
	.long	-305676291
	.long	1071857664
	.long	1154503168
	.long	1071731846
	.long	608837029
	.long	1072135993
	.long	-1615371071
	.long	-1076575735
	.long	-2061825051
	.long	1065821410
	.long	1350334176
	.long	1069322567
	.long	432533675
	.long	-1078626587
	.long	-838588368
	.long	1071865857
	.long	-1050318112
	.long	1071737851
	.long	-1388631177
	.long	1072130679
	.long	1957347990
	.long	-1076576333
	.long	95562178
	.long	1066053744
	.long	-2015582901
	.long	1069295118
	.long	-921831581
	.long	-1078629585
	.long	-1984365484
	.long	1071874046
	.long	2010167936
	.long	1071743833
	.long	1712106045
	.long	1072125371
	.long	814463349
	.long	-1076577015
	.long	-32894400
	.long	1066282534
	.long	410586918
	.long	1069267746
	.long	-1063980070
	.long	-1078632986
	.long	-1689409470
	.long	1071882241
	.long	269275456
	.long	1071749799
	.long	-670959479
	.long	1072120061
	.long	741525078
	.long	-1076577781
	.long	-1899346782
	.long	1066454937
	.long	-913951677
	.long	1069240422
	.long	1080455861
	.long	-1078636779
	.long	-831822872
	.long	1071890432
	.long	487262944
	.long	1071755741
	.long	-226070298
	.long	1072114757
	.long	-1841156562
	.long	-1076578629
	.long	-1596776122
	.long	1066565953
	.long	927277857
	.long	1069213190
	.long	2010718038
	.long	-1078640954
	.long	290072071
	.long	1071898623
	.long	-956338400
	.long	1071761661
	.long	543704999
	.long	1072109458
	.long	1721488134
	.long	-1076579557
	.long	83874442
	.long	1066675258
	.long	-402913243
	.long	1069186045
	.long	-2097909318
	.long	-1078645499
	.long	-1042637955
	.long	1071906815
	.long	2133755968
	.long	1071767563
	.long	-2076765901
	.long	1072104160
	.long	-2146239516
	.long	-1076580565
	.long	-1077641397
	.long	1066782900
	.long	-670386491
	.long	1069158985
	.long	1772359821
	.long	-1078650403
	.long	-518153165
	.long	1071915006
	.long	1740893216
	.long	1071773443
	.long	40531616
	.long	1072098868
	.long	-1773057262
	.long	-1076581651
	.long	129640709
	.long	1066888835
	.long	639242785
	.long	1069132030
	.long	492120992
	.long	-1078655655
	.long	-99137930
	.long	1071923199
	.long	263190624
	.long	1071779304
	.long	-1633234590
	.long	1072093578
	.long	1804761915
	.long	-1076582814
	.long	-129311824
	.long	1066993112
	.long	-582464075
	.long	1069105173
	.long	2017390404
	.long	-1078661245
	.long	222658468
	.long	1071931391
	.long	-1620618496
	.long	1071785142
	.long	973681489
	.long	1072088295
	.long	-873283592
	.long	-1076584053
	.long	-2068083690
	.long	1067095690
	.long	-1812219531
	.long	1069078436
	.long	993518843
	.long	-1078667161
	.long	-1886319017
	.long	1071939582
	.long	-577587136
	.long	1071790960
	.long	-1984937127
	.long	1072083016
	.long	-1756690433
	.long	-1076585366
	.long	636617297
	.long	1067196606
	.long	-2098632899
	.long	1069051816
	.long	219346256
	.long	-1078673393
	.long	1931738219
	.long	1071947777
	.long	-481414112
	.long	1071796760
	.long	-227017177
	.long	1072077740
	.long	2139450042
	.long	-1076586753
	.long	-1725426420
	.long	1067295902
	.long	1052142118
	.long	1069025310
	.long	2024474309
	.long	-1078679931
	.long	-1312881408
	.long	1071955968
	.long	-1114952576
	.long	1071802537
	.long	1118551362
	.long	1072072473
	.long	-327883190
	.long	-1076588212
	.long	-792465684
	.long	1067393502
	.long	1529915909
	.long	1068998946
	.long	-330043581
	.long	-1078686764
	.long	487006331
	.long	1071964161
	.long	-630917216
	.long	1071808294
	.long	-1479557454
	.long	1072067210
	.long	1070314055
	.long	-1076589741
	.long	-1489679807
	.long	1067469920
	.long	-269028742
	.long	1068972714
	.long	-1206228046
	.long	-1078693881
	.long	1597434694
	.long	1071972350
	.long	903729536
	.long	1071814029
	.long	813394675
	.long	1072061956
	.long	-2104772890
	.long	-1076591340
	.long	1218002649
	.long	1067517070
	.long	1183677529
	.long	1068946635
	.long	222841225
	.long	-1078701272
	.long	721287001
	.long	1071980545
	.long	-350473600
	.long	1071819746
	.long	-1916413267
	.long	1072056704
	.long	739369911
	.long	-1076593008
	.long	-1998474429
	.long	1067563438
	.long	-12254785
	.long	1068920684
	.long	-53036834
	.long	-1078708929
	.long	-539286493
	.long	1071988737
	.long	-1425800512
	.long	1071825442
	.long	-404334597
	.long	1072051460
	.long	-286965847
	.long	-1076594744
	.long	1768740933
	.long	1067608986
	.long	1782228719
	.long	1068894893
	.long	1996808692
	.long	-1078716839
	.long	1926408015
	.long	1071996926
	.long	381579840
	.long	1071831115
	.long	-833311010
	.long	1072046226
	.long	2071347542
	.long	-1076596544
	.long	1301987047
	.long	1067653708
	.long	-1466325443
	.long	1068869271
	.long	1241153457
	.long	-1078724994
	.long	-294556621
	.long	1072005120
	.long	676282336
	.long	1071836771
	.long	404510496
	.long	1072040996
	.long	-1014779711
	.long	-1076598411
	.long	-1005970854
	.long	1067697663
	.long	192625522
	.long	1068843793
	.long	560989185
	.long	-1078733384
	.long	-1742353110
	.long	1072013311
	.long	770690880
	.long	1071842404
	.long	1007985431
	.long	1072035775
	.long	578440414
	.long	-1076600340
	.long	1839781943
	.long	1067740805
	.long	1633875144
	.long	1068818492
	.long	-2055251594
	.long	-1078742000
	.long	-30784819
	.long	1072021501
	.long	-1525609024
	.long	1071848016
	.long	869208680
	.long	1072030562
	.long	-158630468
	.long	-1076602333
	.long	-566169367
	.long	1067783157
	.long	-570060770
	.long	1068793362
	.long	-631460939
	.long	-1078750832
	.long	1462962073
	.long	1072029696
	.long	1965372544
	.long	1071853611
	.long	-1877105574
	.long	1072025354
	.long	1428640975
	.long	-1076604387
	.long	-1141899077
	.long	1067824747
	.long	-759498830
	.long	1068768395
	.long	1599284250
	.long	-1078759870
	.long	1298159066
	.long	1072037889
	.long	-13706656
	.long	1071859184
	.long	-219522816
	.long	1072020155
	.long	-1308588204
	.long	-1076606502
	.long	418738107
	.long	1067865553
	.long	-1985953358
	.long	1068743611
	.long	781017340
	.long	-1078769106
	.long	741870027
	.long	1072046081
	.long	-2127736768
	.long	1071864737
	.long	1730038249
	.long	1072014966
	.long	588145174
	.long	-1076608675
	.long	1795540983
	.long	1067905581
	.long	-806963413
	.long	1068719012
	.long	1010563643
	.long	-1078778531
	.long	-2018214196
	.long	1072054272
	.long	1769035136
	.long	1071870269
	.long	-923070871
	.long	1072009785
	.long	-1514332306
	.long	-1076610907
	.long	2140869583
	.long	1067944841
	.long	1962245263
	.long	1068694601
	.long	1449962186
	.long	-1078788136
	.long	-1488569985
	.long	1072062464
	.long	-1651078848
	.long	1071875781
	.long	-2096677800
	.long	1072004613
	.long	-29556605
	.long	-1076613196
	.long	1268653803
	.long	1067983345
	.long	424865410
	.long	1068670377
	.long	609773224
	.long	-1078797912
	.long	-454882997
	.long	1072070656
	.long	-1241630464
	.long	1071881273
	.long	920458014
	.long	1071999450
	.long	1953992672
	.long	-1076615540
	.long	-1699729137
	.long	1068021095
	.long	-219926011
	.long	1068646344
	.long	642266483
	.long	-1078807851
	.long	-488409209
	.long	1072078849
	.long	668280352
	.long	1071886746
	.long	-1244109363
	.long	1071994295
	.long	-245363501
	.long	-1076617940
	.long	1266280537
	.long	1068058101
	.long	1184935981
	.long	1068622506
	.long	-1257725093
	.long	-1078817945
	.long	234142715
	.long	1072087038
	.long	1190311648
	.long	1071892195
	.long	-1235701826
	.long	1071989153
	.long	948805943
	.long	-1076620391
	.long	-441204038
	.long	1068094343
	.long	389257209
	.long	1068598880
	.long	31148357
	.long	-1078828184
	.long	-364876566
	.long	1072095231
	.long	464770048
	.long	1071897628
	.long	-750780786
	.long	1071984017
	.long	918355945
	.long	-1076622897
	.long	390333871
	.long	1068129876
	.long	-603711482
	.long	1068575438
	.long	369976663
	.long	-1078838562
	.long	-318641563
	.long	1072103424
	.long	1424299008
	.long	1071903040
	.long	1552650521
	.long	1071978892
	.long	-219525129
	.long	-1076625455
	.long	814514637
	.long	1068164675
	.long	425765623
	.long	1068552204
	.long	-755692251
	.long	-1078849071
	.long	13964779
	.long	1072111618
	.long	-1740917792
	.long	1071908432
	.long	-233350031
	.long	1071973776
	.long	907784540
	.long	-1076628062
	.long	-1689031940
	.long	1068198751
	.long	-1489935795
	.long	1068529175
	.long	-240598956
	.long	-1078859702
	.long	321117805
	.long	1072119809
	.long	-1803294944
	.long	1071913803
	.long	271038977
	.long	1071968673
	.long	-1147695985
	.long	-1076630719
	.long	2003133034
	.long	1068232103
	.long	-1745515382
	.long	1068506361
	.long	50807624
	.long	-1078870447
	.long	634041546
	.long	1072127999
	.long	120256
	.long	1071919154
	.long	1165228908
	.long	1071963580
	.long	1106386218
	.long	-1076633423
	.long	-776101873
	.long	1068264741
	.long	1496706664
	.long	1068468578
	.long	1871578761
	.long	-1078881301
	.long	-1869471882
	.long	1072136190
	.long	1824960256
	.long	1071924485
	.long	1291828654
	.long	1071958497
	.long	186941124
	.long	-1076636175
	.long	-1807792874
	.long	1068296682
	.long	-229995791
	.long	1068423795
	.long	2003479618
	.long	-1078892255
	.long	1202911291
	.long	1072144382
	.long	901881632
	.long	1071929797
	.long	-57728835
	.long	1071953424
	.long	1944005133
	.long	-1076638974
	.long	600168158
	.long	1068327928
	.long	-1672407381
	.long	1068379441
	.long	846080370
	.long	-1078903302
	.long	790526377
	.long	1072152575
	.long	-129108544
	.long	1071935089
	.long	-175722051
	.long	1071948362
	.long	788012635
	.long	-1076641818
	.long	1938331376
	.long	1068358488
	.long	622190200
	.long	1068335514
	.long	-1876492848
	.long	-1078914436
	.long	-1757639419
	.long	1072160769
	.long	-251947648
	.long	1071940363
	.long	879145102
	.long	1071943311
	.long	475242522
	.long	-1076644707
	.long	621821677
	.long	1068388371
	.long	1385231535
	.long	1068292015
	.long	1475280955
	.long	-1078925649
	.long	1777320082
	.long	1072168959
	.long	1125922816
	.long	1071945615
	.long	-1545533878
	.long	1071938273
	.long	1718374596
	.long	-1076647638
	.long	1083745470
	.long	1068417561
	.long	787372000
	.long	1068248980
	.long	688740144
	.long	-1078936935
	.long	-987016654
	.long	1072177153
	.long	-759680192
	.long	1071950849
	.long	-753173259
	.long	1071933244
	.long	-964885008
	.long	-1076650613
	.long	713143765
	.long	1068446098
	.long	-1969090928
	.long	1068206363
	.long	2061299670
	.long	-1078948288
	.long	-1119007169
	.long	1072185344
	.long	-1692550048
	.long	1071956062
	.long	-925488649
	.long	1071928229
	.long	334098247
	.long	-1076653627
	.long	389561707
	.long	1068473961
	.long	663626169
	.long	1068164209
	.long	-1661645552
	.long	-1078959701
	.long	-1937335107
	.long	1072193537
	.long	-48069216
	.long	1071961256
	.long	2042111089
	.long	1071923225
	.long	1547577641
	.long	-1076656683
	.long	2066518002
	.long	1068500059
	.long	970443343
	.long	1068122492
	.long	-1217939500
	.long	-1078971168
	.long	-1243924720
	.long	1072201726
	.long	-1953252192
	.long	1071966429
	.long	1724311231
	.long	1071918235
	.long	-1557208415
	.long	-1076659777
	.long	1636302351
	.long	1068513336
	.long	-1959130317
	.long	1068081242
	.long	-890371159
	.long	-1078982683
	.long	-2042857501
	.long	1072209920
	.long	-1882382720
	.long	1071971585
	.long	-1465382593
	.long	1071913254
	.long	-1242680243
	.long	-1076662911
	.long	675410578
	.long	1068526300
	.long	612652555
	.long	1068040420
	.long	1652109066
	.long	-1079003775
	.long	249135791
	.long	1072218111
	.long	383945408
	.long	1071976720
	.long	949031662
	.long	1071908288
	.long	-1982624313
	.long	-1076666081
	.long	-1269777929
	.long	1068538941
	.long	825035850
	.long	1068000066
	.long	980197394
	.long	-1079026962
	.long	46378670
	.long	1072226304
	.long	-1062532544
	.long	1071981836
	.long	-967470139
	.long	1071903332
	.long	-668210944
	.long	-1076669289
	.long	1750363449
	.long	1068551273
	.long	-612066819
	.long	1067960153
	.long	916661047
	.long	-1079050211
	.long	-322983557
	.long	1072234496
	.long	244141792
	.long	1071986934
	.long	-200174797
	.long	1071898389
	.long	2099333620
	.long	-1076672532
	.long	-91087556
	.long	1068563294
	.long	1768504969
	.long	1067920696
	.long	597751283
	.long	-1079073511
	.long	-766394340
	.long	1072242687
	.long	-739001056
	.long	1071992010
	.long	357883157
	.long	1071893461
	.long	734609989
	.long	-1076675809
	.long	801424374
	.long	1068575007
	.long	461010524
	.long	1067881704
	.long	-2106685706
	.long	-1079096852
	.long	-653593370
	.long	1072250881
	.long	1297008192
	.long	1071997070
	.long	876998514
	.long	1071888543
	.long	-1212426342
	.long	-1076679122
	.long	-469643151
	.long	1068586420
	.long	975531804
	.long	1067843153
	.long	-1995442590
	.long	-1079120223
	.long	83717143
	.long	1072259073
	.long	-714654688
	.long	1072002108
	.long	166446818
	.long	1071883640
	.long	1839530908
	.long	-1076682466
	.long	1315055723
	.long	1068597531
	.long	1908330141
	.long	1067805072
	.long	596710977
	.long	-1079143614
	.long	757156738
	.long	1072267264
	.long	990732576
	.long	1072007128
	.long	32156734
	.long	1071878750
	.long	138564868
	.long	-1076685842
	.long	45987185
	.long	1068608346
	.long	-1631752008
	.long	1067767448
	.long	-192768422
	.long	-1079167017
	.long	-342358447
	.long	1072275453
	.long	-1318344800
	.long	1072012127
	.long	280079226
	.long	1071873874
	.long	-107751818
	.long	-1076689250
	.long	-840096344
	.long	1068618866
	.long	755678618
	.long	1067730288
	.long	1445075617
	.long	-1079190420
	.long	-447725485
	.long	1072283647
	.long	-1252137312
	.long	1072017110
	.long	-91110114
	.long	1071869008
	.long	-1104077226
	.long	-1076692689
	.long	1541648133
	.long	1068629104
	.long	-1395274297
	.long	1067693565
	.long	1524349368
	.long	-1079213816
	.long	1851826740
	.long	1072291841
	.long	2041369728
	.long	1072022074
	.long	-1510296854
	.long	1071864157
	.long	501141226
	.long	-1076696157
	.long	896754968
	.long	1068639056
	.long	586808616
	.long	1067657302
	.long	-829742503
	.long	-1079237196
	.long	-1232767973
	.long	1072300031
	.long	1523844192
	.long	1072027017
	.long	-667764525
	.long	1071859321
	.long	588175937
	.long	-1076699653
	.long	-1304687710
	.long	1068648722
	.long	1280214746
	.long	1067621509
	.long	922757717
	.long	-1079260550
	.long	-1655064253
	.long	1072308225
	.long	-1944551776
	.long	1072031943
	.long	-1629609597
	.long	1071854497
	.long	-1341900935
	.long	-1076703179
	.long	2058948876
	.long	1068658115
	.long	-1784873970
	.long	1067586156
	.long	-715775170
	.long	-1079283872
	.long	-1096936129
	.long	1072316415
	.long	-1446999328
	.long	1072036848
	.long	1870160298
	.long	1071849689
	.long	-1038086376
	.long	-1076706730
	.long	-1501998445
	.long	1068667229
	.long	627740638
	.long	1067551275
	.long	-1501933943
	.long	-1079307152
	.long	2049155534
	.long	1072324606
	.long	1663043072
	.long	1072041735
	.long	-784516770
	.long	1071844894
	.long	1145059634
	.long	-1076710307
	.long	-915935213
	.long	1068676073
	.long	902824632
	.long	1067516845
	.long	1687595249
	.long	-1079330383
	.long	1475498168
	.long	1072332801
	.long	-580627552
	.long	1072046605
	.long	-805319650
	.long	1071840111
	.long	1871544977
	.long	-1076713912
	.long	56970435
	.long	1068684655
	.long	1270278054
	.long	1067482851
	.long	-2020004093
	.long	-1079353558
	.long	-578577079
	.long	1072340992
	.long	-1273837152
	.long	1072051455
	.long	-489316863
	.long	1071835344
	.long	1416089565
	.long	-1076717540
	.long	-14701131
	.long	1068692968
	.long	-1548058564
	.long	1067448278
	.long	1185910199
	.long	-1079376669
	.long	1997402656
	.long	1072349184
	.long	-17367008
	.long	1072056286
	.long	571146111
	.long	1071830592
	.long	922347343
	.long	-1076721192
	.long	-452389878
	.long	1068701022
	.long	1638599551
	.long	1067382123
	.long	-1731315425
	.long	-1079399710
	.long	308002376
	.long	1072357377
	.long	1470254368
	.long	1072061100
	.long	469448025
	.long	1071825853
	.long	1843287517
	.long	-1076724868
	.long	365216368
	.long	1068708821
	.long	-1683206449
	.long	1067316858
	.long	909809451
	.long	-1079422673
	.long	2083657785
	.long	1072365566
	.long	1453032384
	.long	1072065893
	.long	1409022169
	.long	1071821130
	.long	-1661336626
	.long	-1076728565
	.long	749350412
	.long	1068716363
	.long	1167572634
	.long	1067252514
	.long	-1726939102
	.long	-1079445553
	.long	1967679780
	.long	1072373758
	.long	1758395200
	.long	1072070669
	.long	-1905612780
	.long	1071816420
	.long	-391141397
	.long	-1076732285
	.long	-82933048
	.long	1068723657
	.long	-1386387766
	.long	1067189041
	.long	-19194229
	.long	-1079468343
	.long	1605951160
	.long	1072381953
	.long	-784874240
	.long	1072075428
	.long	-1409325426
	.long	1071811723
	.long	-655942134
	.long	-1076736027
	.long	311898490
	.long	1068730709
	.long	495208021
	.long	1067126435
	.long	1762920061
	.long	-1079491036
	.long	946612153
	.long	1072390144
	.long	-1931513920
	.long	1072080167
	.long	-982151752
	.long	1071807043
	.long	1506467040
	.long	-1076739787
	.long	-298537327
	.long	1068737513
	.long	476053253
	.long	1067064744
	.long	-1631016250
	.long	-1079513628
	.long	-772809293
	.long	1072398336
	.long	181188192
	.long	1072084889
	.long	-1719424334
	.long	1071802377
	.long	1980005896
	.long	-1076743568
	.long	-248376421
	.long	1068744080
	.long	1490990049
	.long	1067003920
	.long	756632809
	.long	-1079536112
	.long	1120682055
	.long	1072406526
	.long	-2042339744
	.long	1072089590
	.long	111118111
	.long	1071797728
	.long	625350201
	.long	-1076747366
	.long	-1457077977
	.long	1068750409
	.long	-979230864
	.long	1066943995
	.long	1759644646
	.long	-1079558484
	.long	-1793398099
	.long	1072414721
	.long	1011762592
	.long	1072094277
	.long	-10128464
	.long	1071793089
	.long	-477124633
	.long	-1076751186
	.long	1587622671
	.long	1068756510
	.long	734848735
	.long	1066884900
	.long	1866007237
	.long	-1079580738
	.long	1712782865
	.long	1072422910
	.long	655388640
	.long	1072098942
	.long	-1749710864
	.long	1071788470
	.long	605024050
	.long	-1076755019
	.long	1156284812
	.long	1068762377
	.long	276914425
	.long	1066826718
	.long	656218285
	.long	-1079602869
	.long	1414601842
	.long	1072431103
	.long	1681636160
	.long	1072103591
	.long	-511558517
	.long	1071783863
	.long	-1702539398
	.long	-1076758872
	.long	1911572535
	.long	1068768021
	.long	-1143926916
	.long	1066769368
	.long	1106261777
	.long	-1079624873
	.long	-986228418
	.long	1072439296
	.long	-210934784
	.long	1072108222
	.long	-145444139
	.long	1071779271
	.long	-1658528787
	.long	-1076762741
	.long	-1795914272
	.long	1068773443
	.long	304769156
	.long	1066712873
	.long	1026204905
	.long	-1079646745
	.long	-573352167
	.long	1072447488
	.long	-671469568
	.long	1072112835
	.long	-223825473
	.long	1071774695
	.long	-1302471178
	.long	-1076766625
	.long	-756829681
	.long	1068778645
	.long	2144082736
	.long	1066657238
	.long	1657851919
	.long	-1079668481
	.long	1076065718
	.long	1072455680
	.long	2113877792
	.long	1072117430
	.long	-2033638521
	.long	1071770135
	.long	836631640
	.long	-1076770523
	.long	-122238014
	.long	1068783631
	.long	13539726
	.long	1066602455
	.long	-886464929
	.long	-1079690077
	.long	428888036
	.long	1072463871
	.long	255416416
	.long	1072122007
	.long	-1473922509
	.long	1071765590
	.long	-1487007803
	.long	-1076774436
	.long	2046360957
	.long	1068788405
	.long	-1515648258
	.long	1066548515
	.long	1563383627
	.long	-1079711528
	.long	-102612835
	.long	1072472062
	.long	2045243840
	.long	1072126566
	.long	-2025794839
	.long	1071761060
	.long	-241682132
	.long	-1076778363
	.long	-2138286022
	.long	1068792970
	.long	-2055264986
	.long	1066495404
	.long	-800623242
	.long	-1079732832
	.long	-533139598
	.long	1072480257
	.long	-427732672
	.long	1072131109
	.long	431861240
	.long	1071756544
	.long	-1264293458
	.long	-1076782304
	.long	1110998690
	.long	1068797331
	.long	1929755381
	.long	1066443103
	.long	-1397862889
	.long	-1079753984
	.long	-344849537
	.long	1072488446
	.long	2054834848
	.long	1072135632
	.long	1327458050
	.long	1071752046
	.long	-795267848
	.long	-1076786255
	.long	1520736525
	.long	1068801486
	.long	1668650597
	.long	1066381533
	.long	1294928911
	.long	-1079774981
	.long	-364365331
	.long	1072496639
	.long	-1318025024
	.long	1072140139
	.long	-818802129
	.long	1071747561
	.long	570687660
	.long	-1076790219
	.long	169186881
	.long	1068805444
	.long	-2138485591
	.long	1066280236
	.long	-544619931
	.long	-1079795821
	.long	-331429710
	.long	1072504829
	.long	-926496640
	.long	1072144627
	.long	1923023318
	.long	1071743094
	.long	-1739500224
	.long	-1076794194
	.long	453587967
	.long	1068809204
	.long	-526753587
	.long	1066180597
	.long	1714050820
	.long	-1079816499
	.long	1887759828
	.long	1072513023
	.long	1594028576
	.long	1072149100
	.long	-1181273594
	.long	1071738640
	.long	-1642198444
	.long	-1076798181
	.long	-1406363088
	.long	1068812772
	.long	-271681844
	.long	1066082525
	.long	-1198998473
	.long	-1079837014
	.long	-1921872260
	.long	1072521214
	.long	1217232512
	.long	1072153554
	.long	-433936985
	.long	1071734203
	.long	-1559900494
	.long	-1076802177
	.long	1098641863
	.long	1068816150
	.long	-509615559
	.long	1065986078
	.long	-2065847033
	.long	-1079857362
	.long	1890204419
	.long	1072529407
	.long	-618424960
	.long	1072157991
	.long	-1102059007
	.long	1071729781
	.long	718735018
	.long	-1076806183
	.long	-1264766294
	.long	1068819341
	.long	937372525
	.long	1065891194
	.long	1350358907
	.long	-1079877541
	.long	339016611
	.long	1072537599
	.long	-2100779232
	.long	1072162411
	.long	-308576275
	.long	1071725375
	.long	986306118
	.long	-1076810198
	.long	-407888651
	.long	1068822348
	.long	-765471443
	.long	1065797893
	.long	2038851311
	.long	-1079897549
	.long	-1463230836
	.long	1072545790
	.long	-190798560
	.long	1072166813
	.long	-671342311
	.long	1071720985
	.long	-960620065
	.long	-1076814222
	.long	1666063249
	.long	1068825175
	.long	1207528949
	.long	1065706150
	.long	926610841
	.long	-1079917383
	.long	643251701
	.long	1072553984
	.long	1304213856
	.long	1072171200
	.long	2030298297
	.long	1071716610
	.long	1558179078
	.long	-1076818254
	.long	-861502982
	.long	1068827824
	.long	1866259977
	.long	1065615928
	.long	-1683424662
	.long	-1079937042
	.long	889232230
	.long	1072562174
	.long	-1026862272
	.long	1072175567
	.long	-1312210251
	.long	1071712252
	.long	588086674
	.long	-1076822292
	.long	1975891185
	.long	1068830298
	.long	-473876008
	.long	1065527272
	.long	-1812306901
	.long	-1079956523
	.long	1227705448
	.long	1072570368
	.long	1556842112
	.long	1072179920
	.long	-1836165665
	.long	1071707908
	.long	-301267167
	.long	-1076826340
	.long	-1820591244
	.long	1068832601
	.long	734658011
	.long	1065440089
	.long	-367192028
	.long	-1079975825
	.long	-1810711707
	.long	1072578560
	.long	335551072
	.long	1072184255
	.long	938771930
	.long	1071703581
	.long	455857021
	.long	-1076830392
	.long	1677872404
	.long	1068834735
	.long	-1685809512
	.long	1065354425
	.long	1158292532
	.long	-1079994945
	.long	1197010735
	.long	1072586751
	.long	355582336
	.long	1072188572
	.long	-1954550553
	.long	1071699270
	.long	274464286
	.long	-1076834450
	.long	-104472518
	.long	1068836702
	.long	397021495
	.long	1065187314
	.long	711496161
	.long	-1080013883
	.long	940540319
	.long	1072594944
	.long	1959575648
	.long	1072192873
	.long	-1893882443
	.long	1071694974
	.long	1655533583
	.long	-1076838515
	.long	524270635
	.long	1068838508
	.long	-299586179
	.long	1065021891
	.long	-20044087
	.long	-1080032638
	.long	-1495741573
	.long	1072603136
	.long	-804373920
	.long	1072197157
	.long	-1171174046
	.long	1071690694
	.long	-406114940
	.long	-1076842585
	.long	149489439
	.long	1068840153
	.long	1290134985
	.long	1064859396
	.long	246925781
	.long	-1080069132
	.long	1511242336
	.long	1072611328
	.long	431055840
	.long	1072201425
	.long	784038615
	.long	1071686431
	.long	866385121
	.long	-1076846658
	.long	-1982222499
	.long	1068841640
	.long	-134806517
	.long	1064699801
	.long	-739200005
	.long	-1080105898
	.long	1492704536
	.long	1072619519
	.long	1719782016
	.long	1072205675
	.long	-331128653
	.long	1071682183
	.long	-1211234331
	.long	-1076850736
	.long	2078203199
	.long	1068842973
	.long	1891970662
	.long	1064543078
	.long	1573548821
	.long	-1080142288
	.long	-1107086336
	.long	1072627711
	.long	-582125216
	.long	1072209909
	.long	-536996419
	.long	1071677951
	.long	-622498801
	.long	-1076854818
	.long	-129142947
	.long	1068844154
	.long	2038600734
	.long	1064389156
	.long	-2115598283
	.long	-1080178302
	.long	-424340899
	.long	1072635905
	.long	-1142390432
	.long	1072214128
	.long	-546233630
	.long	1071673734
	.long	-560966875
	.long	-1076858904
	.long	-1243681670
	.long	1068845187
	.long	-1264482603
	.long	1064171362
	.long	-568265579
	.long	-1080213938
	.long	-143597349
	.long	1072644093
	.long	166290304
	.long	1072218328
	.long	-173705559
	.long	1071669536
	.long	489231103
	.long	-1076862989
	.long	-1932460001
	.long	1068846073
	.long	1854987286
	.long	1063874812
	.long	-655665026
	.long	-1080249194
	.long	1310120499
	.long	1072652286
	.long	700239296
	.long	1072222513
	.long	-751065879
	.long	1071665352
	.long	-682851374
	.long	-1076867080
	.long	1496035922
	.long	1068846816
	.long	1905520309
	.long	1063583585
	.long	-1537627002
	.long	-1080284069
	.long	1165426478
	.long	1072660480
	.long	-857455712
	.long	1072226682
	.long	-659981460
	.long	1071661183
	.long	-920961451
	.long	-1076871173
	.long	1597200375
	.long	1068847418
	.long	348728147
	.long	1063297722
	.long	1047584625
	.long	-1080318562
	.long	968270640
	.long	1072668671
	.long	-1477110368
	.long	1072230834
	.long	1661354328
	.long	1071657032
	.long	-1397051992
	.long	-1076875266
	.long	-227260224
	.long	1068847881
	.long	1213202539
	.long	1062778582
	.long	1948459999
	.long	-1080352673
	.long	-1435057991
	.long	1072676865
	.long	366374272
	.long	1072234972
	.long	685356202
	.long	1071652895
	.long	-1450357704
	.long	-1076879362
	.long	144146680
	.long	1068848210
	.long	2134230459
	.long	1062228149
	.long	-510507089
	.long	-1080386401
	.long	967152330
	.long	1072685054
	.long	1837672320
	.long	1072239090
	.long	-468949704
	.long	1071648776
	.long	-753497890
	.long	-1076883456
	.long	-483452906
	.long	1068848404
	.long	1310075090
	.long	1061169685
	.long	-184879712
	.long	-1080419745
	.long	-1902980688
	.long	1072691201
	.long	-1682653440
	.long	1072242171
	.long	-1199258199
	.long	1071645695
	.long	947300692
	.long	-1076886529
	.long	1429855739
	.long	1068848465
	.long	2073421506
	.long	1059063783
	.long	413383256
	.long	-1080444505
	.section	.rodata.cst16
	.align 16
.LC1:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC2:
	.long	0
	.long	1048576
	.align 8
.LC3:
	.long	33554432
	.long	1101004800
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1081081856
	.align 8
.LC6:
	.long	0
	.long	1127219200
	.align 8
.LC7:
	.long	0
	.long	1072693248
