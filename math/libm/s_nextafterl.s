	.text
	.p2align 4,,15
	.globl	__nextafterl
	.type	__nextafterl, @function
__nextafterl:
	pushq	%rbp
	pushq	%rbx
	movq	32(%rsp), %rdx
	movq	24(%rsp), %rax
	movq	48(%rsp), %rcx
	movq	40(%rsp), %r8
	movl	%edx, %esi
	movq	%rax, %rdi
	movl	%edx, %ebx
	movq	%r8, %r11
	andw	$32767, %si
	movl	%ecx, %r9d
	shrq	$32, %rdi
	shrq	$32, %r11
	andw	$32767, %r9w
	cmpw	$32767, %si
	movl	%edi, %r10d
	je	.L63
.L2:
	cmpw	$32767, %r9w
	je	.L64
.L4:
	fldt	40(%rsp)
	fldt	24(%rsp)
	fucomip	%st(1), %st
	jp	.L70
	je	.L1
	fstp	%st(0)
	jmp	.L31
.L70:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%eax, %r9d
	movswl	%si, %esi
	orl	%edi, %r9d
	orl	%esi, %r9d
	je	.L65
	movswl	%dx, %esi
	movswl	%cx, %ecx
	testl	%esi, %esi
	js	.L8
	cmpl	%ecx, %esi
	jg	.L9
	je	.L66
.L10:
	addl	$1, %eax
	jne	.L14
	movl	%edi, %r10d
	addl	$1, %r10d
	je	.L22
	testl	%esi, %esi
	jne	.L14
	cmpl	$-2147483648, %r10d
	jne	.L14
.L22:
	addl	$1, %esi
	orl	$-2147483648, %r10d
	movl	%esi, %ebx
	andl	$32767, %esi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%eax, %eax
	jne	.L18
	cmpl	$-2147483648, %edi
	leal	-1(%rdi), %r10d
	ja	.L18
	testl	%esi, %esi
	je	.L29
	subl	$1, %esi
	testl	%esi, %esi
	movl	%esi, %ebx
	je	.L29
.L61:
	orl	$-2147483648, %r10d
	.p2align 4,,10
	.p2align 3
.L18:
	subl	$1, %eax
.L14:
	cmpl	$32767, %esi
	je	.L67
	testl	%esi, %esi
	jne	.L25
.L13:
	fldt	24(%rsp)
	fmul	%st(0), %st
	fstp	%st(0)
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
.L25:
	movw	%bx, -16(%rsp)
	movl	%r10d, -20(%rsp)
	movl	%eax, -24(%rsp)
	fldt	-24(%rsp)
.L1:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%edi, %ebp
	andl	$2147483647, %ebp
	orl	%eax, %ebp
	je	.L2
	fldt	24(%rsp)
	fldt	40(%rsp)
	faddp	%st, %st(1)
.L68:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%r11d, %r9d
	andl	$2147483647, %r9d
	orl	%r8d, %r9d
	je	.L4
	fldt	24(%rsp)
	fldt	40(%rsp)
	faddp	%st, %st(1)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%ecx, %ecx
	jns	.L16
	cmpl	%ecx, %esi
	jg	.L16
	cmpl	%ecx, %esi
	je	.L69
.L17:
	addl	$1, %eax
	jne	.L32
	movl	%edi, %r10d
	addl	$1, %r10d
	je	.L22
	cmpl	$-32768, %esi
	jne	.L32
	cmpl	$-2147483648, %r10d
	je	.L22
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%edx, %esi
	andl	$32767, %esi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L65:
	andw	$-32768, %cx
	movq	$1, -24(%rsp)
	movw	%cx, -16(%rsp)
	fldt	-24(%rsp)
	fld	%st(0)
	fmul	%st(0), %st
	fstp	%st(0)
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	fldt	24(%rsp)
	fadd	%st(0), %st
	fstp	%st(0)
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	%r11d, %edi
	ja	.L9
	jne	.L10
	cmpl	%r8d, %eax
	ja	.L18
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	testl	%eax, %eax
	jne	.L20
	cmpl	$-2147483648, %edi
	leal	-1(%rdi), %r10d
	setbe	%r8b
	cmpl	$-32768, %esi
	setne	%cl
	testb	%cl, %r8b
	je	.L20
	leal	-1(%rsi), %ebx
	movl	%ebx, %esi
	andl	$32767, %esi
	jne	.L61
	movl	$-32768, %ebx
	movl	$-1, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	%r11d, %edi
	ja	.L16
	jne	.L17
	cmpl	%r8d, %eax
	jbe	.L17
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%edx, %esi
	andl	$32767, %esi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%ebx, %ebx
	movl	$-1, %eax
	jmp	.L13
	.size	__nextafterl, .-__nextafterl
	.weak	nexttowardl
	.set	nexttowardl,__nextafterl
	.globl	__nexttowardl
	.set	__nexttowardl,__nextafterl
	.weak	nextafterf64x
	.set	nextafterf64x,__nextafterl
	.weak	nextafterl
	.set	nextafterl,__nextafterl
