	.text
	.p2align 4,,15
	.globl	__fmodl
	.type	__fmodl, @function
__fmodl:
	fldt	24(%rsp)
	fldt	8(%rsp)
	xorl	%ecx, %ecx
	movl	$0, %edx
	fxam
	fnstsw	%ax
	andb	$69, %ah
	fldz
	cmpb	$5, %ah
	sete	%cl
	fucomip	%st(2), %st
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L4
	testl	%ecx, %ecx
	jne	.L4
	fxch	%st(1)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L9:
	fxch	%st(1)
.L2:
	fstpt	24(%rsp)
	fstpt	8(%rsp)
	jmp	__ieee754_fmodl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	fucomi	%st(1), %st
	jp	.L9
	fxch	%st(1)
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	fstpt	24(%rsp)
	fstpt	8(%rsp)
	jmp	__ieee754_fmodl@PLT
	.size	__fmodl, .-__fmodl
	.weak	fmodf64x
	.set	fmodf64x,__fmodl
	.weak	fmodl
	.set	fmodl,__fmodl
