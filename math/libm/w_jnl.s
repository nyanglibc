	.text
	.p2align 4,,15
	.globl	__jnl
	.type	__jnl, @function
__jnl:
	jmp	__ieee754_jnl@PLT
	.size	__jnl, .-__jnl
	.weak	jnf64x
	.set	jnf64x,__jnl
	.weak	jnl
	.set	jnl,__jnl
	.p2align 4,,15
	.globl	__ynl
	.type	__ynl, @function
__ynl:
	fldt	8(%rsp)
	fldz
	fucomi	%st(1), %st
	jnb	.L11
	fstp	%st(0)
.L4:
	fstpt	8(%rsp)
	jmp	__ieee754_ynl@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__ynl, .-__ynl
	.weak	ynf64x
	.set	ynf64x,__ynl
	.weak	ynl
	.set	ynl,__ynl
