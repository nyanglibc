	.text
	.p2align 4,,15
	.globl	__fsub
	.type	__fsub, @function
__fsub:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	ucomisd	%xmm1, %xmm0
	jp	.L2
	jne	.L2
	movapd	%xmm0, %xmm2
	movss	.LC1(%rip), %xmm4
	subsd	%xmm1, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movaps	%xmm2, %xmm3
	andps	.LC0(%rip), %xmm3
	ucomiss	%xmm3, %xmm4
	jnb	.L1
	ucomiss	%xmm2, %xmm2
	jp	.L11
.L9:
	movq	.LC2(%rip), %xmm4
	movsd	.LC3(%rip), %xmm3
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm3
	jnb	.L20
.L1:
	addq	$40, %rsp
	movaps	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
#APP
# 72 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %eax
	movl	%eax, %edx
	andl	$-32704, %edx
	orl	$32640, %edx
	movl	%edx, 28(%rsp)
#APP
# 75 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	movq	%xmm2, %rbp
#APP
# 140 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	movl	28(%rsp), %ebx
	movl	%ebx, %edi
	andl	$61, %edi
	movl	%edi, %edx
	orl	%eax, %edx
	movl	%edx, 28(%rsp)
#APP
# 146 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 28(%rsp)
# 0 "" 2
#NO_APP
	shrl	$7, %eax
	notl	%eax
	testl	%edi, %eax
	jne	.L22
.L6:
	shrl	$5, %ebx
	movabsq	$-4294967296, %rax
	andl	$1, %ebx
	pxor	%xmm2, %xmm2
	orl	%ebp, %ebx
	andq	%rax, %rbp
	orq	%rbx, %rbp
	movss	.LC1(%rip), %xmm4
	movq	%rbp, (%rsp)
	cvtsd2ss	(%rsp), %xmm2
	movaps	%xmm2, %xmm3
	andps	.LC0(%rip), %xmm3
	ucomiss	%xmm3, %xmm4
	jb	.L23
	ucomiss	.LC4(%rip), %xmm2
	jp	.L1
	jne	.L1
.L20:
	movq	errno@gottpoff(%rip), %rax
	movaps	%xmm2, %xmm0
	movl	$34, %fs:(%rax)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	ucomiss	%xmm2, %xmm2
	jnp	.L9
	ucomisd	%xmm1, %xmm0
	jp	.L1
.L11:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movsd	%xmm1, 8(%rsp)
	movsd	%xmm0, (%rsp)
	call	__feraiseexcept@PLT
	movsd	8(%rsp), %xmm1
	movsd	(%rsp), %xmm0
	jmp	.L6
	.size	__fsub, .-__fsub
	.weak	f32subf32x
	.set	f32subf32x,__fsub
	.weak	f32subf64
	.set	f32subf64,__fsub
	.weak	fsub
	.set	fsub,__fsub
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	0
