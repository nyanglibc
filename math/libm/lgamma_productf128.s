	.text
	.globl	__multf3
	.globl	__floatsitf
	.globl	__addtf3
	.globl	__divtf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__lgamma_productf128
	.type	__lgamma_productf128, @function
__lgamma_productf128:
	testl	%edi, %edi
	jle	.L4
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	xorl	%ebx, %ebx
	subq	$248, %rsp
	movaps	%xmm1, 224(%rsp)
	movdqa	%xmm2, %xmm1
	movaps	%xmm0, 176(%rsp)
	call	__multf3@PLT
	pxor	%xmm3, %xmm3
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm3, 80(%rsp)
	movaps	%xmm3, (%rsp)
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%ebx, %edi
	call	__floatsitf@PLT
	movdqa	224(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	176(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	176(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 144(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	128(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	144(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm4
	movaps	%xmm0, 96(%rsp)
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	208(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 96(%rsp)
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	112(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 144(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, 160(%rsp)
	addl	$1, %ebx
	movdqa	48(%rsp), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 192(%rsp)
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	192(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	128(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm1
	movdqa	128(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	144(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	160(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	144(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	160(%rsp), %xmm7
	cmpl	%ebx, %ebp
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm7, (%rsp)
	jne	.L3
	movdqa	%xmm0, %xmm1
	movdqa	%xmm7, %xmm0
	call	__addtf3@PLT
	addq	$248, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	pxor	%xmm0, %xmm0
	ret
	.size	__lgamma_productf128, .-__lgamma_productf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	8388608
	.long	0
	.long	1077411840
