	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpxf
	.type	__fromfpxf, @function
__fromfpxf:
	pushq	%r13
	pushq	%r12
	movd	%xmm0, %edx
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$64, %esi
	ja	.L36
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$64, %esi
.L10:
	movl	%edx, %ecx
	andl	$2147483647, %ecx
	je	.L37
	movl	%edx, %r11d
	shrl	$23, %ecx
	shrl	$31, %r11d
	leal	-127(%rcx), %r9d
	leal	-2(%rsi,%r11), %r10d
	cmpl	%r10d, %r9d
	jg	.L57
	movl	%edx, %eax
	andl	$8388607, %eax
	orl	$8388608, %eax
	cmpl	$22, %r9d
	jle	.L15
	movl	%eax, %r8d
	subl	$150, %ecx
	xorl	%r13d, %r13d
	salq	%cl, %r8
.L16:
	cmpl	$1, %edi
	je	.L42
	jle	.L59
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	cmpl	$3, %edi
	je	.L26
	cmpl	$4, %edi
	jne	.L40
.L26:
	addq	%rax, %r8
.L17:
	testl	%edx, %edx
	jns	.L24
.L23:
	cmpl	%r10d, %r9d
	je	.L27
	testb	%bpl, %bpl
	jne	.L28
	testb	%r13b, %r13b
	jne	.L28
.L29:
	movq	%r8, %rax
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
.L9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$-1, %r9d
	jl	.L38
	movl	$22, %ecx
	movl	$1, %r8d
	movl	%eax, %r12d
	subl	%r9d, %ecx
	sall	%cl, %r8d
	andl	%r8d, %r12d
	movl	%r8d, %ecx
	setne	%bpl
	subl	$1, %ecx
	movl	%ecx, %ebx
	movl	$23, %ecx
	andl	%eax, %ebx
	setne	%r13b
	subl	%r9d, %ecx
	shrl	%cl, %eax
	cmpl	$1, %edi
	movl	%eax, %r8d
	je	.L18
	jle	.L60
	cmpl	$3, %edi
	je	.L21
	cmpl	$4, %edi
	jne	.L17
	testl	%r12d, %r12d
	je	.L39
	andl	$1, %eax
	movl	$1, %ebp
	orl	%ebx, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%edi, %edi
	jne	.L17
.L20:
	testl	%edx, %edx
	js	.L23
	movl	%ebp, %eax
	orl	%r13d, %eax
	movzbl	%al, %eax
	addq	%rax, %r8
.L24:
	leal	1(%r10), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	cmpq	%r8, %rax
	sete	%al
.L30:
	testb	%al, %al
	jne	.L57
	testb	%bpl, %bpl
	jne	.L28
	testb	%r13b, %r13b
	je	.L32
.L28:
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
.L32:
	testl	%edx, %edx
	movq	%r8, %rax
	js	.L29
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	addq	$8, %rsp
	movl	%r11d, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L59:
	xorl	%ebp, %ebp
	testl	%edi, %edi
	je	.L20
.L40:
	xorl	%ebp, %ebp
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%ebp, %ebp
.L18:
	testl	%edx, %edx
	jns	.L24
	movl	%ebp, %eax
	orl	%r13d, %eax
	movzbl	%al, %eax
	addq	%rax, %r8
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$1, %r13d
	xorl	%r8d, %r8d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1, %eax
	movl	%r9d, %ecx
	salq	%cl, %rax
	cmpq	%r8, %rax
	setne	%al
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	%bpl, %eax
	jmp	.L26
.L39:
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.L26
	.size	__fromfpxf, .-__fromfpxf
	.weak	fromfpxf32
	.set	fromfpxf32,__fromfpxf
	.weak	fromfpxf
	.set	fromfpxf,__fromfpxf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.align 4
.LC1:
	.long	8388608
