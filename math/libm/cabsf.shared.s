	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cabsf
	.type	__cabsf, @function
__cabsf:
	movq	%xmm0, -16(%rsp)
	movss	-12(%rsp), %xmm1
	movss	-16(%rsp), %xmm0
	jmp	__hypotf@PLT
	.size	__cabsf, .-__cabsf
	.weak	cabsf32
	.set	cabsf32,__cabsf
	.weak	cabsf
	.set	cabsf,__cabsf
