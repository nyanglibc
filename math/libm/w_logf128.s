	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__logf128
	.type	__logf128, @function
__logf128:
	pxor	%xmm1, %xmm1
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L9
.L2:
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	jmp	__ieee754_logf128@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movq	errno@gottpoff(%rip), %rax
	je	.L10
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__logf128, .-__logf128
	.weak	logf128
	.set	logf128,__logf128
