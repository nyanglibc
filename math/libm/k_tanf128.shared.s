	.text
	.globl	__fixtfsi
	.globl	__divtf3
	.globl	__lttf2
	.globl	__multf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__floatsitf
	.p2align 4,,15
	.globl	__kernel_tanf128
	.type	__kernel_tanf128, @function
__kernel_tanf128:
	pushq	%r13
	pushq	%r12
	movl	%edi, %r12d
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movaps	%xmm0, 16(%rsp)
	movq	24(%rsp), %rbx
	movaps	%xmm1, 32(%rsp)
	movq	%rbx, %rax
	shrq	$32, %rax
	movl	%eax, %ebp
	andl	$2147483647, %ebp
	cmpl	$1069940735, %ebp
	jg	.L2
	call	__fixtfsi@PLT
	testl	%eax, %eax
	jne	.L3
	movq	16(%rsp), %rax
	movq	%rax, %rdx
	shrq	$32, %rdx
	orl	%edx, %ebx
	orl	%eax, %ebx
	leal	1(%r12), %eax
	orl	%eax, %ebx
	orl	%ebp, %ebx
	je	.L19
	cmpl	$1, %r12d
	je	.L20
	movdqa	16(%rsp), %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1073633601, %ebp
	jle	.L3
	testl	%eax, %eax
	movl	$1, %r13d
	jns	.L8
	movdqa	.LC5(%rip), %xmm0
	movl	$-1, %r13d
	movdqa	16(%rsp), %xmm4
	movdqa	32(%rsp), %xmm5
	pxor	%xmm0, %xmm4
	pxor	%xmm0, %xmm5
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm5, 32(%rsp)
.L8:
	movdqa	16(%rsp), %xmm1
	movdqa	.LC6(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC7(%rip), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	pxor	%xmm3, %xmm3
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
.L3:
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC13(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	.LC18(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	cmpl	$1073633601, %ebp
	movdqa	%xmm0, %xmm2
	jg	.L21
	cmpl	$1, %r12d
	je	.L1
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm0, %xmm1
	movq	$0, 48(%rsp)
	movdqa	.LC4(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movq	$0, 32(%rsp)
	movaps	%xmm0, 64(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$88, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r12d, %edi
	movaps	%xmm0, 48(%rsp)
	call	__floatsitf@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 64(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm2
	movdqa	32(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	cmpl	$-1, %r13d
	movdqa	%xmm0, %xmm2
	jne	.L1
	pxor	.LC5(%rip), %xmm2
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	movdqa	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	16(%rsp), %xmm1
	pand	.LC1(%rip), %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	addq	$88, %rsp
	popq	%rbx
	movdqa	%xmm2, %xmm0
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	16(%rsp), %xmm0
	pand	.LC1(%rip), %xmm0
	movdqa	.LC3(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L16
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	16(%rsp), %xmm2
	jmp	.L1
	.size	__kernel_tanf128, .-__kernel_tanf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC6:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073648159
	.align 16
.LC7:
	.long	549186148
	.long	2793195328
	.long	2418335880
	.long	1066126610
	.align 16
.LC8:
	.long	2600162324
	.long	1118797676
	.long	2639119707
	.long	-1073916589
	.align 16
.LC9:
	.long	1999128885
	.long	852955773
	.long	669380151
	.long	1074159850
	.align 16
.LC10:
	.long	4010994813
	.long	1565905723
	.long	82163727
	.long	1074633327
	.align 16
.LC11:
	.long	1636031401
	.long	958313395
	.long	4284232286
	.long	1075004019
	.align 16
.LC12:
	.long	3057183665
	.long	1543998917
	.long	3810398278
	.long	1075254436
	.align 16
.LC13:
	.long	3352535269
	.long	3311459352
	.long	344802034
	.long	1074268712
	.align 16
.LC14:
	.long	376086368
	.long	270162727
	.long	1879696997
	.long	1074739644
	.align 16
.LC15:
	.long	312434995
	.long	1293606048
	.long	2640363079
	.long	1075117657
	.align 16
.LC16:
	.long	3059611636
	.long	2549000526
	.long	757546406
	.long	1075375995
	.align 16
.LC17:
	.long	1523936538
	.long	4131853545
	.long	2498506561
	.long	1075446618
	.align 16
.LC18:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073567061
