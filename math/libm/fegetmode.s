	.text
	.p2align 4,,15
	.globl	fegetmode
	.type	fegetmode, @function
fegetmode:
#APP
# 25 "../sysdeps/x86_64/fpu/fegetmode.c" 1
	fnstcw (%rdi)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
#APP
# 26 "../sysdeps/x86_64/fpu/fegetmode.c" 1
	stmxcsr 4(%rdi)
# 0 "" 2
#NO_APP
	ret
	.size	fegetmode, .-fegetmode
