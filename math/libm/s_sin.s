	.text
	.p2align 4,,15
	.globl	__sin
	.type	__sin, @function
__sin:
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$40, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 16(%rsp)
# 0 "" 2
#NO_APP
	movl	16(%rsp), %ebx
	movl	%ebx, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebx
	movl	%eax, 24(%rsp)
	jne	.L62
.L2:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1045430271, %eax
	jg	.L3
	movapd	%xmm0, %xmm1
	movsd	.LC5(%rip), %xmm2
	andpd	.LC4(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	ja	.L63
.L4:
	testb	%bpl, %bpl
	jne	.L64
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$1072390143, %eax
	jg	.L6
	movq	.LC4(%rip), %xmm3
	movapd	%xmm0, %xmm4
	movsd	.LC6(%rip), %xmm1
	andpd	%xmm3, %xmm4
	ucomisd	%xmm4, %xmm1
	ja	.L65
	pxor	%xmm2, %xmm2
	pxor	%xmm7, %xmm7
	ucomisd	%xmm0, %xmm2
	jb	.L9
	movsd	.LC1(%rip), %xmm7
.L9:
	movsd	.LC12(%rip), %xmm2
	movapd	%xmm4, %xmm1
	leaq	__sincostab(%rip), %rdx
	addsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm6
	movq	%xmm1, %rax
	movsd	.LC15(%rip), %xmm1
	subsd	%xmm2, %xmm6
	sall	$2, %eax
	movslq	%eax, %rcx
	subsd	%xmm6, %xmm4
	movsd	(%rdx,%rcx,8), %xmm6
	leal	3(%rax), %ecx
	movslq	%ecx, %rcx
	movapd	%xmm4, %xmm8
	movapd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm8
	movapd	%xmm2, %xmm5
	movsd	.LC13(%rip), %xmm4
	mulsd	%xmm8, %xmm1
	mulsd	%xmm8, %xmm4
	mulsd	%xmm8, %xmm5
	subsd	.LC16(%rip), %xmm1
	subsd	.LC14(%rip), %xmm4
	mulsd	%xmm8, %xmm1
	mulsd	%xmm5, %xmm4
	movsd	(%rdx,%rcx,8), %xmm5
	leal	1(%rax), %ecx
	addl	$2, %eax
	cltq
	testb	%bpl, %bpl
	addsd	.LC17(%rip), %xmm1
	movslq	%ecx, %rcx
	addsd	%xmm7, %xmm4
	addsd	%xmm2, %xmm4
	mulsd	%xmm7, %xmm2
	mulsd	%xmm8, %xmm1
	movapd	%xmm0, %xmm7
	andpd	.LC18(%rip), %xmm7
	mulsd	%xmm4, %xmm5
	addsd	%xmm2, %xmm1
	mulsd	(%rdx,%rax,8), %xmm4
	addsd	(%rdx,%rcx,8), %xmm5
	mulsd	%xmm6, %xmm1
	subsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	andpd	%xmm3, %xmm4
	orpd	%xmm7, %xmm4
	movapd	%xmm4, %xmm0
	je	.L1
.L64:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	movl	12(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 12(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1073965308, %eax
	jg	.L10
	movq	.LC4(%rip), %xmm3
	movapd	%xmm0, %xmm2
	movsd	.LC19(%rip), %xmm1
	andpd	%xmm3, %xmm2
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm2
	pxor	%xmm1, %xmm1
	ucomisd	%xmm2, %xmm1
	ja	.L35
	movsd	.LC2(%rip), %xmm5
.L11:
	andpd	%xmm3, %xmm2
	leaq	__sincostab(%rip), %rdx
	movsd	.LC12(%rip), %xmm4
	movapd	%xmm0, %xmm7
	movapd	%xmm2, %xmm1
	addsd	%xmm4, %xmm2
	andpd	.LC18(%rip), %xmm7
	movapd	%xmm2, %xmm6
	movq	%xmm2, %rax
	subsd	%xmm4, %xmm6
	movsd	.LC13(%rip), %xmm4
	sall	$2, %eax
	leal	1(%rax), %esi
	leal	2(%rax), %ecx
	subsd	%xmm6, %xmm1
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	movsd	(%rdx,%rsi,8), %xmm2
	addsd	%xmm5, %xmm1
	movapd	%xmm1, %xmm5
	movapd	%xmm1, %xmm6
	mulsd	%xmm1, %xmm5
	mulsd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm6
	subsd	.LC14(%rip), %xmm4
	mulsd	%xmm6, %xmm4
	movsd	(%rdx,%rcx,8), %xmm6
	leal	3(%rax), %ecx
	cltq
	movslq	%ecx, %rcx
	addsd	%xmm4, %xmm1
	movsd	(%rdx,%rcx,8), %xmm4
	mulsd	%xmm1, %xmm2
	mulsd	(%rdx,%rax,8), %xmm1
	subsd	%xmm2, %xmm4
	movsd	.LC15(%rip), %xmm2
	mulsd	%xmm5, %xmm2
	subsd	.LC16(%rip), %xmm2
	mulsd	%xmm5, %xmm2
	addsd	.LC17(%rip), %xmm2
	mulsd	%xmm5, %xmm2
	mulsd	%xmm6, %xmm2
	subsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm2
	subsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm1
	addsd	%xmm6, %xmm1
	andpd	%xmm3, %xmm1
	orpd	%xmm7, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L63:
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L65:
	movapd	%xmm0, %xmm2
	movsd	.LC7(%rip), %xmm1
	mulsd	%xmm0, %xmm2
	pxor	%xmm3, %xmm3
	mulsd	%xmm2, %xmm1
	addsd	.LC8(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	subsd	.LC9(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	addsd	.LC10(%rip), %xmm1
	mulsd	%xmm2, %xmm1
	subsd	.LC11(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	subsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$1100554746, %eax
	jg	.L12
	movsd	.LC20(%rip), %xmm2
	movsd	.LC21(%rip), %xmm1
	mulsd	%xmm0, %xmm2
	movsd	.LC22(%rip), %xmm3
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm7
	movq	%xmm2, %rax
	subsd	%xmm1, %xmm7
	testb	$1, %al
	mulsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm0
	movsd	.LC23(%rip), %xmm3
	mulsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm0
	movsd	.LC24(%rip), %xmm3
	mulsd	%xmm7, %xmm3
	movapd	%xmm0, %xmm2
	subsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm3, %xmm0
	movsd	.LC25(%rip), %xmm3
	mulsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm1
	subsd	%xmm1, %xmm2
	subsd	%xmm3, %xmm2
	movq	.LC4(%rip), %xmm3
	addsd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	andpd	%xmm3, %xmm2
	jne	.L66
	movsd	.LC6(%rip), %xmm4
	ucomisd	%xmm2, %xmm4
	jbe	.L55
	movapd	%xmm1, %xmm3
	movsd	.LC7(%rip), %xmm2
	movsd	.LC17(%rip), %xmm4
	mulsd	%xmm1, %xmm3
	mulsd	%xmm0, %xmm4
	mulsd	%xmm3, %xmm2
	addsd	.LC8(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	.LC9(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	addsd	.LC10(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	.LC11(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	subsd	%xmm4, %xmm2
	mulsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	.p2align 4,,10
	.p2align 3
.L25:
	testb	$2, %al
	je	.L4
	xorpd	.LC18(%rip), %xmm0
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L62:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$2146435071, %eax
	jle	.L67
	cmpl	$2146435072, %eax
	je	.L68
.L30:
	divsd	%xmm0, %xmm0
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L66:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm1, %xmm3
	jbe	.L14
	xorpd	.LC18(%rip), %xmm0
.L14:
	movapd	%xmm2, %xmm1
	leaq	__sincostab(%rip), %rcx
	movsd	.LC12(%rip), %xmm3
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm7
	movq	%xmm1, %rdx
	subsd	%xmm3, %xmm7
	sall	$2, %edx
	leal	1(%rdx), %edi
	leal	2(%rdx), %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	subsd	%xmm7, %xmm2
	movsd	(%rcx,%rdi,8), %xmm1
	addsd	%xmm2, %xmm0
	movsd	.LC13(%rip), %xmm2
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm3
	mulsd	%xmm3, %xmm2
	mulsd	%xmm3, %xmm4
	subsd	.LC14(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	movsd	(%rcx,%rsi,8), %xmm4
	leal	3(%rdx), %esi
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	addsd	%xmm0, %xmm2
	movsd	(%rcx,%rsi,8), %xmm0
	mulsd	%xmm2, %xmm1
	mulsd	(%rcx,%rdx,8), %xmm2
	subsd	%xmm1, %xmm0
	movsd	.LC15(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	subsd	.LC16(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	addsd	.LC17(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	mulsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm0
	subsd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	24(%rsp), %rsi
	leaq	16(%rsp), %rdi
	call	__branred@PLT
	testb	$1, %al
	movsd	24(%rsp), %xmm4
	movsd	16(%rsp), %xmm1
	je	.L22
	pxor	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	jbe	.L23
	xorpd	.LC18(%rip), %xmm4
.L23:
	movapd	%xmm1, %xmm0
	leaq	__sincostab(%rip), %rcx
	movsd	.LC12(%rip), %xmm1
	andpd	.LC4(%rip), %xmm0
	movsd	.LC13(%rip), %xmm3
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm7
	movq	%xmm2, %rdx
	subsd	%xmm1, %xmm7
	sall	$2, %edx
	leal	1(%rdx), %edi
	leal	2(%rdx), %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	subsd	%xmm7, %xmm0
	movsd	(%rcx,%rsi,8), %xmm2
	leal	3(%rdx), %esi
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	addsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm4
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm4
	mulsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm1
	subsd	.LC14(%rip), %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rcx,%rdi,8), %xmm1
	addsd	%xmm0, %xmm3
	movsd	(%rcx,%rsi,8), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	(%rcx,%rdx,8), %xmm3
	subsd	%xmm1, %xmm0
	movsd	.LC15(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC16(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC17(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movsd	.LC3(%rip), %xmm5
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L55:
	pxor	%xmm4, %xmm4
	movq	.LC18(%rip), %xmm6
	ucomisd	%xmm1, %xmm4
	jb	.L19
	xorpd	%xmm6, %xmm0
.L19:
	movsd	.LC12(%rip), %xmm5
	movapd	%xmm2, %xmm4
	leaq	__sincostab(%rip), %rcx
	addsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm7
	movq	%xmm4, %rdx
	movsd	.LC15(%rip), %xmm4
	subsd	%xmm5, %xmm7
	movsd	.LC13(%rip), %xmm5
	sall	$2, %edx
	movslq	%edx, %rsi
	subsd	%xmm7, %xmm2
	movapd	%xmm2, %xmm9
	movapd	%xmm2, %xmm7
	mulsd	%xmm2, %xmm9
	mulsd	%xmm9, %xmm5
	mulsd	%xmm9, %xmm4
	mulsd	%xmm9, %xmm7
	subsd	.LC14(%rip), %xmm5
	subsd	.LC16(%rip), %xmm4
	mulsd	%xmm7, %xmm5
	mulsd	%xmm9, %xmm4
	movsd	(%rcx,%rsi,8), %xmm7
	leal	3(%rdx), %esi
	addsd	%xmm0, %xmm5
	movslq	%esi, %rsi
	addsd	.LC17(%rip), %xmm4
	movsd	(%rcx,%rsi,8), %xmm8
	leal	1(%rdx), %esi
	addl	$2, %edx
	movslq	%edx, %rdx
	addsd	%xmm2, %xmm5
	mulsd	%xmm0, %xmm2
	movslq	%esi, %rsi
	mulsd	%xmm9, %xmm4
	mulsd	%xmm5, %xmm8
	mulsd	(%rcx,%rdx,8), %xmm5
	addsd	%xmm4, %xmm2
	addsd	(%rcx,%rsi,8), %xmm8
	mulsd	%xmm7, %xmm2
	movapd	%xmm8, %xmm0
	subsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm1, %xmm7
	andpd	%xmm6, %xmm7
	andpd	%xmm3, %xmm0
	orpd	%xmm7, %xmm0
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L68:
	testl	%edx, %edx
	jne	.L30
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L22:
	movq	.LC4(%rip), %xmm3
	movapd	%xmm1, %xmm5
	movsd	.LC6(%rip), %xmm0
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm0
	jbe	.L56
	movapd	%xmm1, %xmm2
	movsd	.LC7(%rip), %xmm0
	movsd	.LC17(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm4, %xmm3
	mulsd	%xmm2, %xmm0
	addsd	.LC8(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	.LC9(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	.LC10(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	.LC11(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L25
.L56:
	pxor	%xmm0, %xmm0
	movq	.LC18(%rip), %xmm6
	ucomisd	%xmm1, %xmm0
	jb	.L28
	xorpd	%xmm6, %xmm4
.L28:
	movsd	.LC12(%rip), %xmm0
	movapd	%xmm5, %xmm2
	leaq	__sincostab(%rip), %rcx
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm7
	movq	%xmm2, %rdx
	subsd	%xmm0, %xmm7
	sall	$2, %edx
	movslq	%edx, %rsi
	movsd	(%rcx,%rsi,8), %xmm2
	leal	3(%rdx), %esi
	subsd	%xmm7, %xmm5
	movsd	.LC13(%rip), %xmm7
	movslq	%esi, %rsi
	movapd	%xmm5, %xmm9
	movapd	%xmm5, %xmm0
	mulsd	%xmm5, %xmm9
	mulsd	%xmm9, %xmm7
	mulsd	%xmm9, %xmm0
	subsd	.LC14(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	movsd	(%rcx,%rsi,8), %xmm0
	leal	1(%rdx), %esi
	addl	$2, %edx
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	addsd	%xmm4, %xmm7
	mulsd	%xmm5, %xmm4
	addsd	%xmm5, %xmm7
	mulsd	%xmm7, %xmm0
	mulsd	(%rcx,%rdx,8), %xmm7
	addsd	(%rcx,%rsi,8), %xmm0
	movapd	%xmm0, %xmm8
	movsd	.LC15(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	subsd	.LC16(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	addsd	.LC17(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	addsd	%xmm4, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm8
	movapd	%xmm8, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm1, %xmm7
	andpd	%xmm6, %xmm7
	addsd	%xmm2, %xmm0
	andpd	%xmm3, %xmm0
	orpd	%xmm7, %xmm0
	jmp	.L25
	.size	__sin, .-__sin
	.weak	sinf32x
	.set	sinf32x,__sin
	.weak	sinf64
	.set	sinf64,__sin
	.weak	sin
	.set	sin,__sin
	.p2align 4,,15
	.globl	__cos
	.type	__cos, @function
__cos:
	pushq	%rbp
	pushq	%rbx
	movq	%xmm0, %rax
	movapd	%xmm0, %xmm1
	subq	$40, %rsp
	sarq	$32, %rax
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 16(%rsp)
# 0 "" 2
#NO_APP
	movl	16(%rsp), %ebx
	andl	$2147483647, %eax
	movl	%ebx, %edx
	andb	$-97, %dh
	cmpl	%edx, %ebx
	movl	%edx, 24(%rsp)
	jne	.L127
	xorl	%ebp, %ebp
	cmpl	$1044381695, %eax
	movsd	.LC26(%rip), %xmm0
	jg	.L100
.L69:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L127:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 24(%rsp)
# 0 "" 2
#NO_APP
	cmpl	$1044381695, %eax
	jle	.L101
	movl	$1, %ebp
.L100:
	cmpl	$1072390143, %eax
	jg	.L72
	pxor	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.L128
.L73:
	andpd	.LC4(%rip), %xmm1
	leaq	__sincostab(%rip), %rdx
	movsd	.LC12(%rip), %xmm3
	movapd	%xmm1, %xmm2
	addsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm6
	movq	%xmm2, %rax
	subsd	%xmm3, %xmm6
	sall	$2, %eax
	leal	1(%rax), %esi
	leal	2(%rax), %ecx
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	subsd	%xmm6, %xmm1
	movsd	(%rdx,%rsi,8), %xmm2
	addsd	%xmm0, %xmm1
	movsd	.LC13(%rip), %xmm0
	movapd	%xmm1, %xmm3
	movapd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm3
	mulsd	%xmm3, %xmm0
	mulsd	%xmm3, %xmm4
	subsd	.LC14(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	movsd	(%rdx,%rcx,8), %xmm4
	leal	3(%rax), %ecx
	cltq
	movslq	%ecx, %rcx
	addsd	%xmm0, %xmm1
	movsd	(%rdx,%rcx,8), %xmm0
	mulsd	%xmm1, %xmm2
	mulsd	(%rdx,%rax,8), %xmm1
	subsd	%xmm2, %xmm0
	movsd	.LC15(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	.LC16(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	addsd	.LC17(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	subsd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
.L74:
	testb	%bpl, %bpl
	je	.L69
.L71:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	movl	12(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 12(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	cmpl	$1073965308, %eax
	jg	.L75
	movq	.LC4(%rip), %xmm4
	movsd	.LC19(%rip), %xmm0
	andpd	%xmm4, %xmm1
	movsd	.LC2(%rip), %xmm2
	movsd	.LC6(%rip), %xmm3
	subsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm3
	jbe	.L121
	movapd	%xmm1, %xmm3
	movsd	.LC7(%rip), %xmm0
	movsd	.LC17(%rip), %xmm4
	mulsd	%xmm1, %xmm3
	mulsd	%xmm2, %xmm4
	mulsd	%xmm3, %xmm0
	addsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	subsd	.LC9(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	addsd	.LC10(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	subsd	.LC11(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm4, %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$1100554746, %eax
	jg	.L80
	movsd	.LC20(%rip), %xmm3
	movsd	.LC21(%rip), %xmm0
	mulsd	%xmm1, %xmm3
	movsd	.LC24(%rip), %xmm4
	addsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm2
	movq	%xmm3, %rax
	subsd	%xmm0, %xmm2
	movsd	.LC22(%rip), %xmm0
	andl	$3, %eax
	addl	$1, %eax
	testb	$1, %al
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm4
	subsd	%xmm0, %xmm1
	movsd	.LC23(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	subsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm1
	subsd	%xmm4, %xmm1
	movsd	.LC25(%rip), %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm0, %xmm2
	subsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	subsd	%xmm4, %xmm0
	movq	.LC4(%rip), %xmm4
	addsd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	andpd	%xmm4, %xmm0
	jne	.L129
	movsd	.LC6(%rip), %xmm3
	ucomisd	%xmm0, %xmm3
	jbe	.L122
	movapd	%xmm2, %xmm3
	movsd	.LC7(%rip), %xmm0
	movsd	.LC17(%rip), %xmm4
	mulsd	%xmm2, %xmm3
	mulsd	%xmm1, %xmm4
	mulsd	%xmm3, %xmm0
	addsd	.LC8(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	subsd	.LC9(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	addsd	.LC10(%rip), %xmm0
	mulsd	%xmm3, %xmm0
	subsd	.LC11(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm4, %xmm0
	mulsd	%xmm3, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	.p2align 4,,10
	.p2align 3
.L93:
	testb	$2, %al
	je	.L74
	xorpd	.LC18(%rip), %xmm0
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L128:
	movsd	.LC1(%rip), %xmm0
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L121:
	pxor	%xmm3, %xmm3
	movq	.LC18(%rip), %xmm6
	ucomisd	%xmm1, %xmm3
	jnb	.L105
.L78:
	movsd	.LC12(%rip), %xmm5
	movapd	%xmm0, %xmm3
	leaq	__sincostab(%rip), %rdx
	addsd	%xmm5, %xmm3
	movapd	%xmm3, %xmm7
	movq	%xmm3, %rax
	movsd	.LC15(%rip), %xmm3
	subsd	%xmm5, %xmm7
	movsd	.LC13(%rip), %xmm5
	sall	$2, %eax
	movslq	%eax, %rcx
	subsd	%xmm7, %xmm0
	movapd	%xmm0, %xmm9
	movapd	%xmm0, %xmm7
	mulsd	%xmm0, %xmm9
	mulsd	%xmm9, %xmm5
	mulsd	%xmm9, %xmm3
	mulsd	%xmm9, %xmm7
	subsd	.LC14(%rip), %xmm5
	subsd	.LC16(%rip), %xmm3
	mulsd	%xmm7, %xmm5
	mulsd	%xmm9, %xmm3
	movsd	(%rdx,%rcx,8), %xmm7
	leal	3(%rax), %ecx
	addsd	%xmm2, %xmm5
	movslq	%ecx, %rcx
	mulsd	%xmm0, %xmm2
	addsd	.LC17(%rip), %xmm3
	movsd	(%rdx,%rcx,8), %xmm8
	leal	1(%rax), %ecx
	addl	$2, %eax
	cltq
	addsd	%xmm0, %xmm5
	movslq	%ecx, %rcx
	mulsd	%xmm9, %xmm3
	mulsd	%xmm5, %xmm8
	mulsd	(%rdx,%rax,8), %xmm5
	addsd	%xmm3, %xmm2
	addsd	(%rdx,%rcx,8), %xmm8
	mulsd	%xmm7, %xmm2
	movapd	%xmm8, %xmm0
	subsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm1, %xmm7
	andpd	%xmm6, %xmm7
	andpd	%xmm4, %xmm0
	orpd	%xmm7, %xmm0
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$2146435071, %eax
	jle	.L130
	cmpl	$2146435072, %eax
	je	.L131
.L98:
	divsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L129:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm2, %xmm3
	jbe	.L82
	xorpd	.LC18(%rip), %xmm1
.L82:
	movapd	%xmm0, %xmm2
	leaq	__sincostab(%rip), %rcx
	movsd	.LC12(%rip), %xmm3
	addsd	%xmm3, %xmm2
	movapd	%xmm2, %xmm6
	movq	%xmm2, %rdx
	subsd	%xmm3, %xmm6
	sall	$2, %edx
	leal	1(%rdx), %edi
	leal	2(%rdx), %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	subsd	%xmm6, %xmm0
	movsd	(%rcx,%rdi,8), %xmm2
	addsd	%xmm0, %xmm1
	movsd	.LC13(%rip), %xmm0
	movapd	%xmm1, %xmm3
	movapd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm3
	mulsd	%xmm3, %xmm0
	mulsd	%xmm3, %xmm4
	subsd	.LC14(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	movsd	(%rcx,%rsi,8), %xmm4
	leal	3(%rdx), %esi
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	addsd	%xmm0, %xmm1
	movsd	(%rcx,%rsi,8), %xmm0
	mulsd	%xmm1, %xmm2
	mulsd	(%rcx,%rdx,8), %xmm1
	subsd	%xmm2, %xmm0
	movsd	.LC15(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	subsd	.LC16(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	addsd	.LC17(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	subsd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L105:
	xorpd	%xmm6, %xmm2
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L130:
	movapd	%xmm1, %xmm0
	leaq	24(%rsp), %rsi
	leaq	16(%rsp), %rdi
	call	__branred@PLT
	addl	$1, %eax
	movsd	24(%rsp), %xmm5
	testb	$1, %al
	movsd	16(%rsp), %xmm1
	je	.L90
	pxor	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	jbe	.L91
	xorpd	.LC18(%rip), %xmm5
.L91:
	leaq	__sincostab(%rip), %rcx
	andpd	.LC4(%rip), %xmm1
	movsd	.LC13(%rip), %xmm3
	movapd	%xmm1, %xmm0
	movsd	.LC12(%rip), %xmm1
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm6
	movq	%xmm2, %rdx
	subsd	%xmm1, %xmm6
	sall	$2, %edx
	leal	1(%rdx), %edi
	leal	2(%rdx), %esi
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	subsd	%xmm6, %xmm0
	movsd	(%rcx,%rsi,8), %xmm2
	leal	3(%rdx), %esi
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	addsd	%xmm5, %xmm0
	movapd	%xmm0, %xmm4
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm4
	mulsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm1
	subsd	.LC14(%rip), %xmm3
	mulsd	%xmm1, %xmm3
	movsd	(%rcx,%rdi,8), %xmm1
	addsd	%xmm0, %xmm3
	movsd	(%rcx,%rsi,8), %xmm0
	mulsd	%xmm3, %xmm1
	mulsd	(%rcx,%rdx,8), %xmm3
	subsd	%xmm1, %xmm0
	movsd	.LC15(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC16(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC17(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	mulsd	%xmm2, %xmm1
	subsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L122:
	pxor	%xmm3, %xmm3
	movq	.LC18(%rip), %xmm6
	ucomisd	%xmm2, %xmm3
	jb	.L87
	xorpd	%xmm6, %xmm1
.L87:
	movsd	.LC12(%rip), %xmm5
	movapd	%xmm0, %xmm3
	leaq	__sincostab(%rip), %rcx
	addsd	%xmm5, %xmm3
	movapd	%xmm3, %xmm7
	movq	%xmm3, %rdx
	movsd	.LC15(%rip), %xmm3
	subsd	%xmm5, %xmm7
	movsd	.LC13(%rip), %xmm5
	sall	$2, %edx
	movslq	%edx, %rsi
	subsd	%xmm7, %xmm0
	movapd	%xmm0, %xmm9
	movapd	%xmm0, %xmm7
	mulsd	%xmm0, %xmm9
	mulsd	%xmm9, %xmm5
	mulsd	%xmm9, %xmm3
	mulsd	%xmm9, %xmm7
	subsd	.LC14(%rip), %xmm5
	subsd	.LC16(%rip), %xmm3
	mulsd	%xmm7, %xmm5
	mulsd	%xmm9, %xmm3
	movsd	(%rcx,%rsi,8), %xmm7
	leal	3(%rdx), %esi
	addsd	%xmm1, %xmm5
	movslq	%esi, %rsi
	addsd	.LC17(%rip), %xmm3
	movsd	(%rcx,%rsi,8), %xmm8
	leal	1(%rdx), %esi
	addl	$2, %edx
	movslq	%edx, %rdx
	addsd	%xmm0, %xmm5
	mulsd	%xmm1, %xmm0
	movslq	%esi, %rsi
	mulsd	%xmm9, %xmm3
	mulsd	%xmm5, %xmm8
	mulsd	(%rcx,%rdx,8), %xmm5
	addsd	%xmm3, %xmm0
	addsd	(%rcx,%rsi,8), %xmm8
	mulsd	%xmm7, %xmm0
	subsd	%xmm0, %xmm8
	movapd	%xmm8, %xmm0
	addsd	%xmm5, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm2, %xmm7
	andpd	%xmm6, %xmm7
	andpd	%xmm4, %xmm0
	orpd	%xmm7, %xmm0
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L131:
	movq	%xmm1, %rax
	testl	%eax, %eax
	jne	.L98
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L90:
	movq	.LC4(%rip), %xmm4
	movapd	%xmm1, %xmm3
	movsd	.LC6(%rip), %xmm0
	andpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm0
	jbe	.L123
	movapd	%xmm1, %xmm2
	movsd	.LC7(%rip), %xmm0
	movsd	.LC17(%rip), %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm5, %xmm3
	mulsd	%xmm2, %xmm0
	addsd	.LC8(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	.LC9(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	.LC10(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	.LC11(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	subsd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm0
	addsd	%xmm1, %xmm0
	jmp	.L93
.L123:
	pxor	%xmm0, %xmm0
	movq	.LC18(%rip), %xmm6
	ucomisd	%xmm1, %xmm0
	jb	.L96
	xorpd	%xmm6, %xmm5
.L96:
	movsd	.LC12(%rip), %xmm0
	movapd	%xmm3, %xmm2
	leaq	__sincostab(%rip), %rcx
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm7
	movq	%xmm2, %rdx
	subsd	%xmm0, %xmm7
	sall	$2, %edx
	movslq	%edx, %rsi
	movsd	(%rcx,%rsi,8), %xmm2
	leal	3(%rdx), %esi
	subsd	%xmm7, %xmm3
	movsd	.LC13(%rip), %xmm7
	movslq	%esi, %rsi
	movapd	%xmm3, %xmm9
	movapd	%xmm3, %xmm0
	mulsd	%xmm3, %xmm9
	mulsd	%xmm9, %xmm7
	mulsd	%xmm9, %xmm0
	subsd	.LC14(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	movsd	(%rcx,%rsi,8), %xmm0
	leal	1(%rdx), %esi
	addl	$2, %edx
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	addsd	%xmm5, %xmm7
	addsd	%xmm3, %xmm7
	mulsd	%xmm5, %xmm3
	mulsd	%xmm7, %xmm0
	mulsd	(%rcx,%rdx,8), %xmm7
	addsd	(%rcx,%rsi,8), %xmm0
	movapd	%xmm0, %xmm8
	movsd	.LC15(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	subsd	.LC16(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	addsd	.LC17(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	addsd	%xmm3, %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm8
	movapd	%xmm8, %xmm0
	addsd	%xmm7, %xmm0
	movapd	%xmm1, %xmm7
	andpd	%xmm6, %xmm7
	addsd	%xmm2, %xmm0
	andpd	%xmm4, %xmm0
	orpd	%xmm7, %xmm0
	jmp	.L93
.L101:
	movsd	.LC26(%rip), %xmm0
	jmp	.L71
	.size	__cos, .-__cos
	.weak	cosf32x
	.set	cosf32x,__cos
	.weak	cosf64
	.set	cosf64,__cos
	.weak	cos
	.set	cos,__cos
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	-2147483648
	.align 8
.LC2:
	.long	856972295
	.long	1016178214
	.align 8
.LC3:
	.long	856972295
	.long	-1131305434
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1048576
	.align 8
.LC6:
	.long	2611340116
	.long	1069555908
	.align 8
.LC7:
	.long	3271352153
	.long	-1101341185
	.align 8
.LC8:
	.long	2073722585
	.long	1053236706
	.align 8
.LC9:
	.long	433785016
	.long	1059717536
	.align 8
.LC10:
	.long	286330574
	.long	1065423121
	.align 8
.LC11:
	.long	1431655765
	.long	1069897045
	.align 8
.LC12:
	.long	0
	.long	1120403456
	.align 8
.LC13:
	.long	3895035695
	.long	1065423120
	.align 8
.LC14:
	.long	1431655701
	.long	1069897045
	.align 8
.LC15:
	.long	3990479417
	.long	1062650219
	.align 8
.LC16:
	.long	1431655733
	.long	1067799893
	.align 8
.LC17:
	.long	0
	.long	1071644672
	.section	.rodata.cst16
	.align 16
.LC18:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC19:
	.long	1413754136
	.long	1073291771
	.align 8
.LC20:
	.long	1841940611
	.long	1071931184
	.align 8
.LC21:
	.long	0
	.long	1127743488
	.align 8
.LC22:
	.long	1476395008
	.long	1073291771
	.align 8
.LC23:
	.long	1006632960
	.long	-1102193001
	.align 8
.LC24:
	.long	2550136832
	.long	-1131629645
	.align 8
.LC25:
	.long	602091223
	.long	-1160940417
	.align 8
.LC26:
	.long	0
	.long	1072693248
