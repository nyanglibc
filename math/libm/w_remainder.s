	.text
	.p2align 4,,15
	.globl	__remainder
	.type	__remainder, @function
__remainder:
	movapd	%xmm0, %xmm2
	andpd	.LC0(%rip), %xmm2
	ucomisd	.LC1(%rip), %xmm2
	seta	%dl
	ja	.L4
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L4
.L2:
	jmp	__ieee754_remainder@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	ucomisd	%xmm1, %xmm0
	jp	.L2
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_remainder@PLT
	.size	__remainder, .-__remainder
	.weak	drem
	.set	drem,__remainder
	.weak	remainderf32x
	.set	remainderf32x,__remainder
	.weak	remainderf64
	.set	remainderf64,__remainder
	.weak	remainder
	.set	remainder,__remainder
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
