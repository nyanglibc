	.text
	.p2align 4,,15
	.globl	__totalordermagf
	.type	__totalordermagf, @function
__totalordermagf:
	movl	(%rdi), %edx
	movl	(%rsi), %eax
	andl	$2147483647, %edx
	andl	$2147483647, %eax
	cmpl	%eax, %edx
	setbe	%al
	movzbl	%al, %eax
	ret
	.size	__totalordermagf, .-__totalordermagf
	.weak	totalordermagf32
	.set	totalordermagf32,__totalordermagf
	.weak	totalordermagf
	.set	totalordermagf,__totalordermagf
