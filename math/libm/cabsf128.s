	.text
	.p2align 4,,15
	.globl	__cabsf128
	.type	__cabsf128, @function
__cabsf128:
	movdqa	24(%rsp), %xmm1
	movdqa	8(%rsp), %xmm0
	jmp	__hypotf128@PLT
	.size	__cabsf128, .-__cabsf128
	.weak	cabsf128
	.set	cabsf128,__cabsf128
