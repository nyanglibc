	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __ieee754_exp2,__exp2_finite@GLIBC_2.15
	.symver __exp2,exp2@@GLIBC_2.29
#NO_APP
	.p2align 4,,15
	.globl	__exp2
	.type	__exp2, @function
__exp2:
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$52, %rdx
	andl	$2047, %edx
	leal	-969(%rdx), %ecx
	cmpl	$62, %ecx
	ja	.L22
.L2:
	movsd	64+__exp_data(%rip), %xmm2
	leaq	__exp_data(%rip), %rdi
	movsd	72+__exp_data(%rip), %xmm3
	movapd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	movq	%xmm1, %rsi
	subsd	%xmm2, %xmm1
	movq	%rsi, %rcx
	movq	%rsi, %rax
	andl	$127, %ecx
	salq	$45, %rax
	addq	%rcx, %rcx
	subsd	%xmm1, %xmm0
	movsd	88+__exp_data(%rip), %xmm1
	addq	120(%rdi,%rcx,8), %rax
	testl	%edx, %edx
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm2
	mulsd	104+__exp_data(%rip), %xmm0
	addsd	80+__exp_data(%rip), %xmm1
	addsd	112(%rdi,%rcx,8), %xmm3
	addsd	96+__exp_data(%rip), %xmm0
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	je	.L23
	movq	%rax, -16(%rsp)
	movsd	-16(%rsp), %xmm1
	mulsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	testl	%ecx, %ecx
	js	.L6
	cmpl	$1032, %edx
	jbe	.L5
	movabsq	$-4503599627370496, %rcx
	cmpq	%rcx, %rax
	je	.L14
	cmpl	$2047, %edx
	je	.L6
	testq	%rax, %rax
	jns	.L24
	movabsq	$-4570929321408987137, %rcx
	cmpq	%rcx, %rax
	jbe	.L5
	xorl	%edi, %edi
	jmp	__math_uflow
	.p2align 4,,10
	.p2align 3
.L23:
	testl	$2147483648, %esi
	je	.L25
	movabsq	$4602678819172646912, %rdx
	movsd	.LC1(%rip), %xmm3
	addq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm2
	mulsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm3
	ja	.L10
	mulsd	.LC2(%rip), %xmm1
	movapd	%xmm1, %xmm0
.L11:
	jmp	__math_check_uflow
	.p2align 4,,10
	.p2align 3
.L5:
	addq	%rax, %rax
	movabsq	$-9143996093422370816, %rcx
	cmpq	%rcx, %rax
	movl	$0, %eax
	cmova	%eax, %edx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	subsd	%xmm1, %xmm2
	movapd	%xmm1, %xmm4
	addsd	%xmm3, %xmm4
	addsd	%xmm2, %xmm0
	movapd	%xmm3, %xmm2
	subsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm1
	movsd	.LC2(%rip), %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	subsd	%xmm3, %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L17
	je	.L12
.L17:
	mulsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
.L12:
	movapd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm1
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L6:
	addsd	.LC1(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movabsq	$-4503599627370496, %rdx
	addq	%rdx, %rax
	movq	%rax, -16(%rsp)
	movq	-16(%rsp), %xmm1
	mulsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm0
	jmp	__math_check_oflow
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm0, %xmm0
	ret
.L24:
	xorl	%edi, %edi
	jmp	__math_oflow
	.size	__exp2, .-__exp2
	.weak	exp2f32x
	.set	exp2f32x,__exp2
	.weak	exp2f64
	.set	exp2f64,__exp2
	.globl	__ieee754_exp2
	.set	__ieee754_exp2,__exp2
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	1048576
	.hidden	__math_oflow
	.hidden	__math_check_oflow
	.hidden	__math_check_uflow
	.hidden	__math_uflow
	.hidden	__exp_data
