	.text
	.globl	__addtf3
	.p2align 4,,15
	.globl	__canonicalizef128
	.type	__canonicalizef128, @function
__canonicalizef128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movdqa	(%rsi), %xmm2
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__issignalingf128@PLT
	testl	%eax, %eax
	movdqa	(%rsp), %xmm2
	jne	.L6
	xorl	%eax, %eax
	movaps	%xmm2, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__canonicalizef128, .-__canonicalizef128
	.weak	canonicalizef128
	.set	canonicalizef128,__canonicalizef128
