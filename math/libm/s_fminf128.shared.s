	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__letf2
	.globl	__addtf3
	.p2align 4,,15
	.globl	__fminf128
	.type	__fminf128, @function
__fminf128:
	subq	$40, %rsp
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L11
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L14
.L11:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L12
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L16
.L12:
	movdqa	(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	jne	.L5
	movdqa	16(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	jne	.L5
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L14
.L16:
	movdqa	16(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	ret
	.size	__fminf128, .-__fminf128
	.weak	fminf128
	.set	fminf128,__fminf128
