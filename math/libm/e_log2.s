	.text
	.p2align 4,,15
	.globl	__log2
	.type	__log2, @function
__log2:
	movq	%xmm0, %rcx
	movq	%xmm0, %rdx
	movabsq	$-4606800540372828160, %rax
	movabsq	$581272283906047, %rsi
	addq	%rcx, %rax
	shrq	$48, %rdx
	cmpq	%rsi, %rax
	jbe	.L11
	leal	-16(%rdx), %eax
	cmpl	$32735, %eax
	ja	.L12
.L4:
	movabsq	$-4604367669032910848, %rax
	movabsq	$-4503599627370496, %rsi
	addq	%rcx, %rax
	movsd	40+__log2_data(%rip), %xmm1
	movq	%rax, %rdx
	andq	%rax, %rsi
	sarq	$52, %rax
	shrq	$46, %rdx
	subq	%rsi, %rcx
	leaq	__log2_data(%rip), %rsi
	andl	$63, %edx
	movq	%rcx, -16(%rsp)
	leaq	73(%rdx), %rcx
	movsd	-16(%rsp), %xmm2
	addq	$9, %rdx
	movsd	24+__log2_data(%rip), %xmm0
	salq	$4, %rcx
	salq	$4, %rdx
	addq	%rsi, %rcx
	addq	%rsi, %rdx
	subsd	(%rcx), %xmm2
	pxor	%xmm3, %xmm3
	movsd	__log2_data(%rip), %xmm6
	cvtsi2sd	%eax, %xmm3
	movapd	%xmm6, %xmm5
	subsd	8(%rcx), %xmm2
	movabsq	$-4294967296, %rcx
	mulsd	(%rdx), %xmm2
	addsd	8(%rdx), %xmm3
	mulsd	%xmm2, %xmm1
	movapd	%xmm2, %xmm8
	mulsd	%xmm2, %xmm0
	movq	%xmm2, %rdi
	mulsd	%xmm2, %xmm8
	andq	%rdi, %rcx
	addsd	32+__log2_data(%rip), %xmm1
	movq	%rcx, -16(%rsp)
	addsd	16+__log2_data(%rip), %xmm0
	movq	-16(%rsp), %xmm7
	movapd	%xmm8, %xmm9
	mulsd	%xmm7, %xmm5
	mulsd	%xmm8, %xmm9
	mulsd	%xmm8, %xmm1
	movapd	%xmm5, %xmm4
	addsd	%xmm3, %xmm4
	addsd	%xmm0, %xmm1
	movsd	56+__log2_data(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	%xmm4, %xmm3
	addsd	48+__log2_data(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	mulsd	8+__log2_data(%rip), %xmm2
	mulsd	%xmm8, %xmm1
	subsd	%xmm7, %xmm0
	mulsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm2
	movapd	%xmm3, %xmm0
	addsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	addsd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movabsq	$4607182418800017408, %rax
	cmpq	%rax, %rcx
	je	.L8
	movapd	%xmm0, %xmm2
	movabsq	$-4294967296, %rax
	movsd	__log2_data(%rip), %xmm7
	subsd	.LC1(%rip), %xmm2
	movsd	120+__log2_data(%rip), %xmm0
	movapd	%xmm7, %xmm1
	movq	%xmm2, %rdi
	movapd	%xmm2, %xmm3
	movapd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm0
	andq	%rdi, %rax
	movq	%rax, -16(%rsp)
	mulsd	%xmm2, %xmm6
	movq	-16(%rsp), %xmm4
	subsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm1
	addsd	112+__log2_data(%rip), %xmm0
	movapd	%xmm6, %xmm5
	mulsd	%xmm7, %xmm3
	movapd	%xmm1, %xmm4
	movsd	8+__log2_data(%rip), %xmm7
	mulsd	%xmm6, %xmm5
	mulsd	%xmm2, %xmm7
	addsd	%xmm3, %xmm7
	movsd	72+__log2_data(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	addsd	64+__log2_data(%rip), %xmm3
	mulsd	%xmm6, %xmm3
	addsd	%xmm3, %xmm4
	subsd	%xmm4, %xmm1
	addsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm3
	movsd	136+__log2_data(%rip), %xmm1
	addsd	%xmm7, %xmm3
	mulsd	%xmm2, %xmm1
	addsd	128+__log2_data(%rip), %xmm1
	mulsd	%xmm6, %xmm1
	addsd	%xmm0, %xmm1
	movsd	104+__log2_data(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	88+__log2_data(%rip), %xmm2
	mulsd	%xmm5, %xmm1
	addsd	96+__log2_data(%rip), %xmm0
	addsd	80+__log2_data(%rip), %xmm2
	mulsd	%xmm6, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	mulsd	%xmm5, %xmm0
	addsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%xmm0, %rax
	addq	%rax, %rax
	je	.L13
	movabsq	$9218868437227405312, %rax
	cmpq	%rax, %rcx
	je	.L1
	testb	$-128, %dh
	jne	.L6
	andl	$32752, %edx
	cmpl	$32752, %edx
	je	.L6
	mulsd	.LC2(%rip), %xmm0
	movabsq	$-234187180623265792, %rax
	movq	%xmm0, %rcx
	addq	%rax, %rcx
	jmp	.L4
.L8:
	pxor	%xmm0, %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %edi
	jmp	__math_divzero@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	jmp	__math_invalid@PLT
	.size	__log2, .-__log2
	.weak	log2f32x
	.set	log2f32x,__log2
	.weak	log2f64
	.set	log2f64,__log2
	.weak	log2
	.set	log2,__log2
	.globl	__ieee754_log2
	.set	__ieee754_log2,__log2
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	1127219200
