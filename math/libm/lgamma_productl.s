	.text
	.p2align 4,,15
	.globl	__lgamma_productl
	.type	__lgamma_productl, @function
__lgamma_productl:
	testl	%edi, %edi
	jle	.L4
	fldt	40(%rsp)
	xorl	%eax, %eax
	fldt	8(%rsp)
	fmulp	%st, %st(1)
	fstpt	-24(%rsp)
	fldz
	fstpt	-88(%rsp)
	fldz
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	fstp	%st(1)
.L3:
	movl	%eax, -104(%rsp)
	addl	$1, %eax
	fildl	-104(%rsp)
	cmpl	%eax, %edi
	fldt	24(%rsp)
	faddp	%st, %st(1)
	fldt	8(%rsp)
	fdiv	%st(1), %st
	fld	%st(1)
	fmul	%st(1), %st
	fldt	.LC2(%rip)
	fmul	%st(2), %st
	fldt	.LC2(%rip)
	fmul	%st(4), %st
	fld	%st(3)
	fsub	%st(2), %st
	faddp	%st, %st(2)
	fld	%st(4)
	fsub	%st(1), %st
	faddp	%st, %st(1)
	fld	%st(3)
	fsub	%st(2), %st
	fstpt	-104(%rsp)
	fld	%st(4)
	fsub	%st(1), %st
	fldt	8(%rsp)
	fsub	%st(4), %st
	fstpt	-72(%rsp)
	fld	%st(2)
	fmul	%st(2), %st
	fsubp	%st, %st(4)
	fld	%st(2)
	fmul	%st(1), %st
	faddp	%st, %st(4)
	fldt	-104(%rsp)
	fmulp	%st, %st(2)
	fxch	%st(3)
	faddp	%st, %st(1)
	fldt	-104(%rsp)
	fmulp	%st, %st(3)
	faddp	%st, %st(2)
	fldt	-72(%rsp)
	fsubp	%st, %st(2)
	fxch	%st(1)
	fdiv	%st(3), %st
	fxch	%st(3)
	fmul	%st(0), %st
	fldt	-24(%rsp)
	fdivp	%st, %st(1)
	fsubrp	%st, %st(3)
	fxch	%st(2)
	fstpt	-72(%rsp)
	fld	%st(0)
	fmul	%st(3), %st
	fldt	.LC2(%rip)
	fmul	%st(4), %st
	fld	%st(4)
	fsub	%st(1), %st
	faddp	%st, %st(1)
	fld	%st(4)
	fsub	%st(1), %st
	fstpt	-40(%rsp)
	fld	%st(2)
	fadd	%st(5), %st
	fld	%st(0)
	fadd	%st(3), %st
	fstpt	-56(%rsp)
	fld	%st(1)
	fmul	%st(5), %st
	fsub	%st(3), %st
	fldt	-104(%rsp)
	fmulp	%st, %st(3)
	faddp	%st, %st(2)
	fldt	-40(%rsp)
	fld	%st(0)
	fxch	%st(6)
	fmulp	%st, %st(1)
	faddp	%st, %st(2)
	fldt	-104(%rsp)
	fmulp	%st, %st(5)
	fxch	%st(1)
	faddp	%st, %st(4)
	fld	%st(4)
	fsub	%st(1), %st
	fadd	%st(3), %st
	fldt	-56(%rsp)
	fsubr	%st, %st(2)
	fxch	%st(2)
	faddp	%st, %st(3)
	faddp	%st, %st(2)
	fxch	%st(3)
	faddp	%st, %st(1)
	fldt	-88(%rsp)
	fmul	%st, %st(2)
	fxch	%st(1)
	faddp	%st, %st(2)
	fldt	-72(%rsp)
	fadd	%st, %st(2)
	fld	%st(1)
	faddp	%st, %st(5)
	fmulp	%st, %st(4)
	fxch	%st(1)
	faddp	%st, %st(3)
	faddp	%st, %st(2)
	fxch	%st(1)
	fstpt	-88(%rsp)
	fld	%st(0)
	jne	.L7
	fstp	%st(0)
	fldt	-88(%rsp)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	fldz
	ret
	.size	__lgamma_productl, .-__lgamma_productl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483648
	.long	2147483648
	.long	16415
	.long	0
