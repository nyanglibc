	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fminmag
	.type	__fminmag, @function
__fminmag:
	movq	.LC0(%rip), %xmm2
	movapd	%xmm0, %xmm3
	andpd	%xmm2, %xmm3
	andpd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	ja	.L21
	ucomisd	%xmm2, %xmm3
	ja	.L19
	jp	.L5
	je	.L24
.L5:
	subq	$24, %rsp
	movsd	%xmm1, (%rsp)
	movsd	%xmm0, 8(%rsp)
	call	__GI___issignaling
	testl	%eax, %eax
	jne	.L8
	movsd	(%rsp), %xmm0
	call	__GI___issignaling
	testl	%eax, %eax
	jne	.L8
	movsd	(%rsp), %xmm4
	movsd	8(%rsp), %xmm6
	movapd	%xmm4, %xmm1
	cmpordsd	%xmm4, %xmm1
	andpd	%xmm1, %xmm4
	andnpd	%xmm6, %xmm1
	movapd	%xmm4, %xmm0
	orpd	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	rep ret
	.p2align 4,,10
	.p2align 3
.L19:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	minsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movsd	8(%rsp), %xmm0
	addsd	(%rsp), %xmm0
.L1:
	addq	$24, %rsp
	ret
	.size	__fminmag, .-__fminmag
	.weak	fminmagf32x
	.set	fminmagf32x,__fminmag
	.weak	fminmagf64
	.set	fminmagf64,__fminmag
	.weak	fminmag
	.set	fminmag,__fminmag
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
