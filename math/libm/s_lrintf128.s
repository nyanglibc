	.text
	.globl	__addtf3
	.globl	__subtf3
	.globl	__gttf2
	.globl	__eqtf2
	.globl	__lttf2
	.globl	__fixtfdi
	.p2align 4,,15
	.globl	__lrintf128
	.type	__lrintf128, @function
__lrintf128:
	pushq	%rbx
	subq	$16, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	shrq	$48, %rax
	andl	$32767, %eax
	subl	$16383, %eax
	cmpl	$62, %eax
	jg	.L2
	movq	%rdx, %rbx
	shrq	$63, %rbx
	cmpl	$47, %eax
	jg	.L3
	leaq	two112(%rip), %rdx
	movq	%rbx, %rax
	salq	$4, %rax
	movdqa	%xmm0, %xmm1
	movdqa	(%rdx,%rax), %xmm2
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andl	$32767, %edx
	subl	$16383, %edx
	js	.L4
	movabsq	$281474976710655, %rax
	andq	%rcx, %rax
	movabsq	$281474976710656, %rcx
	orq	%rcx, %rax
	movl	$48, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rax
.L4:
	movq	%rax, %rdx
	negq	%rdx
	testq	%rbx, %rbx
	cmovne	%rdx, %rax
.L33:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	.LC1(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L10
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L31
.L10:
	movdqa	(%rsp), %xmm0
	call	__fixtfdi@PLT
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L32
	leaq	two112(%rip), %rdx
	movq	%rbx, %rax
	salq	$4, %rax
	movdqa	(%rsp), %xmm1
	movdqa	(%rdx,%rax), %xmm2
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
.L9:
	movq	8(%rsp), %rax
	movabsq	$281474976710655, %rdx
	movq	%rax, %rcx
	andq	%rdx, %rax
	addq	$1, %rdx
	shrq	$48, %rcx
	orq	%rdx, %rax
	movq	(%rsp), %rdx
	andl	$32767, %ecx
	movq	%rax, %rsi
	leal	-16383(%rcx), %edi
	subl	$16431, %ecx
	salq	%cl, %rsi
	movl	$112, %ecx
	subl	%edi, %ecx
	shrq	%cl, %rdx
	orq	%rsi, %rdx
	cmpl	$48, %edi
	cmovne	%rdx, %rax
	movq	%rax, %rdx
	negq	%rdx
	testq	%rbx, %rbx
	cmovne	%rdx, %rax
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L32:
	movdqa	(%rsp), %xmm0
	call	__nearbyintf128@PLT
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__eqtf2@PLT
	cmpq	$1, %rax
	sbbl	%edi, %edi
	andl	$31, %edi
	addl	$1, %edi
	call	feraiseexcept@PLT
	jmp	.L9
.L31:
	movdqa	(%rsp), %xmm0
	call	__nearbyintf128@PLT
	movdqa	.LC1(%rip), %xmm1
	call	__eqtf2@PLT
	cmpq	$1, %rax
	sbbl	%edi, %edi
	andl	$31, %edi
	addl	$1, %edi
	call	feraiseexcept@PLT
	movabsq	$-9223372036854775808, %rax
	jmp	.L1
	.size	__lrintf128, .-__lrintf128
	.weak	lrintf128
	.set	lrintf128,__lrintf128
	.section	.rodata
	.align 32
	.type	two112, @object
	.size	two112, 32
two112:
	.long	0
	.long	0
	.long	0
	.long	1081016320
	.long	0
	.long	0
	.long	0
	.long	-1066467328
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	4294705152
	.long	4294967295
	.long	1077805055
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-1069678592
	.align 16
.LC2:
	.long	0
	.long	131072
	.long	0
	.long	-1069678592
