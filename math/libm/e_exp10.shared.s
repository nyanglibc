	.text
#APP
	.symver __ieee754_exp10,__exp10_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_exp10
	.type	__ieee754_exp10, @function
__ieee754_exp10:
	movapd	%xmm0, %xmm2
	movsd	.LC2(%rip), %xmm1
	andpd	.LC1(%rip), %xmm2
	ucomisd	%xmm2, %xmm1
	jb	.L21
	movsd	.LC3(%rip), %xmm3
	ucomisd	%xmm0, %xmm3
	ja	.L22
	ucomisd	.LC5(%rip), %xmm0
	ja	.L23
	movsd	.LC6(%rip), %xmm3
	movsd	.LC0(%rip), %xmm1
	ucomisd	%xmm2, %xmm3
	ja	.L17
	movq	%xmm0, %rax
	subq	$24, %rsp
	movsd	.LC7(%rip), %xmm1
	andq	$-134217728, %rax
	movq	%rax, (%rsp)
	movq	(%rsp), %xmm2
	subsd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm1
	movsd	.LC8(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	.LC9(%rip), %xmm2
	addsd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	movsd	%xmm1, 8(%rsp)
	call	__ieee754_exp@PLT
	movsd	8(%rsp), %xmm1
	movsd	%xmm0, (%rsp)
	movapd	%xmm1, %xmm0
	call	__ieee754_exp@PLT
	movsd	(%rsp), %xmm1
	addq	$24, %rsp
	mulsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movsd	.LC4(%rip), %xmm1
	mulsd	%xmm1, %xmm1
.L17:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	jmp	__ieee754_exp@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	mulsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.size	__ieee754_exp10, .-__ieee754_exp10
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.align 8
.LC3:
	.long	0
	.long	-1066090496
	.align 8
.LC4:
	.long	0
	.long	1048576
	.align 8
.LC5:
	.long	0
	.long	1081298944
	.align 8
.LC6:
	.long	0
	.long	1013972992
	.align 8
.LC7:
	.long	3149223190
	.long	1073900465
	.align 8
.LC8:
	.long	2887182935
	.long	1046325928
	.align 8
.LC9:
	.long	3087007744
	.long	1073900465
