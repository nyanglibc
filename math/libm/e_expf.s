	.text
	.p2align 4,,15
	.globl	__expf
	.type	__expf, @function
__expf:
	movd	%xmm0, %eax
	pxor	%xmm2, %xmm2
	movd	%xmm0, %edx
	shrl	$20, %eax
	andl	$2047, %eax
	cvtss2sd	%xmm0, %xmm2
	cmpl	$1066, %eax
	ja	.L17
.L2:
	mulsd	296+__exp2f_data(%rip), %xmm2
	leaq	__exp2f_data(%rip), %rdx
	movsd	288+__exp2f_data(%rip), %xmm1
	movsd	.LC4(%rip), %xmm3
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	movq	%xmm0, %rax
	subsd	%xmm1, %xmm0
	movsd	304+__exp2f_data(%rip), %xmm1
	movq	%rax, %rcx
	salq	$47, %rax
	andl	$31, %ecx
	addq	(%rdx,%rcx,8), %rax
	subsd	%xmm0, %xmm2
	movsd	320+__exp2f_data(%rip), %xmm0
	movq	%rax, -16(%rsp)
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm2
	addsd	312+__exp2f_data(%rip), %xmm1
	addsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	mulsd	-16(%rsp), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$-8388608, %edx
	je	.L10
	cmpl	$2039, %eax
	ja	.L18
	ucomiss	.LC1(%rip), %xmm0
	ja	.L19
	movss	.LC2(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.L20
	movss	.LC3(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.L2
	xorl	%edi, %edi
	jmp	__math_may_uflowf@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%edi, %edi
	jmp	__math_oflowf@PLT
.L20:
	xorl	%edi, %edi
	jmp	__math_uflowf@PLT
	.size	__expf, .-__expf
	.weak	expf32
	.set	expf32,__expf
	.weak	expf
	.set	expf,__expf
	.globl	__ieee754_expf
	.set	__ieee754_expf,__expf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1118925335
	.align 4
.LC2:
	.long	3268407732
	.align 4
.LC3:
	.long	3268316879
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1072693248
