	.text
#APP
	.symver __ieee754_powf,__powf_finite@GLIBC_2.15
	.symver __powf,powf@@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__powf
	.type	__powf, @function
__powf:
	movd	%xmm0, %edx
	movd	%xmm1, %esi
	movd	%xmm0, %eax
	leal	-8388608(%rdx), %r8d
	leal	(%rsi,%rsi), %ecx
	cmpl	$2130706431, %r8d
	leal	-1(%rcx), %edi
	ja	.L2
	cmpl	$-16777218, %edi
	ja	.L3
	xorl	%edi, %edi
.L4:
	leal	-1060306944(%rax), %edx
	pxor	%xmm3, %xmm3
	pxor	%xmm2, %xmm2
	leaq	__powf_log2_data(%rip), %rsi
	movl	%edx, %ecx
	andl	$-8388608, %edx
	cvtss2sd	%xmm1, %xmm1
	subl	%edx, %eax
	shrl	$19, %ecx
	sarl	$23, %edx
	andl	$15, %ecx
	movl	%eax, -16(%rsp)
	cvtsi2sd	%edx, %xmm3
	salq	$4, %rcx
	cvtss2sd	-16(%rsp), %xmm2
	addq	%rsi, %rcx
	movsd	.LC4(%rip), %xmm0
	mulsd	(%rcx), %xmm2
	movsd	288+__powf_log2_data(%rip), %xmm4
	subsd	%xmm0, %xmm2
	addsd	8(%rcx), %xmm3
	mulsd	%xmm2, %xmm4
	movapd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm5
	addsd	%xmm4, %xmm3
	movsd	256+__powf_log2_data(%rip), %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm5, %xmm6
	mulsd	272+__powf_log2_data(%rip), %xmm2
	mulsd	%xmm5, %xmm6
	addsd	264+__powf_log2_data(%rip), %xmm4
	addsd	280+__powf_log2_data(%rip), %xmm2
	mulsd	%xmm6, %xmm4
	mulsd	%xmm5, %xmm2
	addsd	%xmm3, %xmm2
	addsd	%xmm4, %xmm2
	mulsd	%xmm2, %xmm1
	movq	%xmm1, %rax
	shrq	$47, %rax
	cmpw	$-32578, %ax
	ja	.L49
.L16:
	movsd	256+__exp2f_data(%rip), %xmm3
	movapd	%xmm1, %xmm2
	leaq	__exp2f_data(%rip), %rdx
	addsd	%xmm3, %xmm2
	movq	%xmm2, %rax
	subsd	%xmm3, %xmm2
	movq	%rax, %rcx
	addq	%rdi, %rax
	andl	$31, %ecx
	salq	$47, %rax
	addq	(%rdx,%rcx,8), %rax
	subsd	%xmm2, %xmm1
	movsd	280+__exp2f_data(%rip), %xmm2
	movq	%rax, -16(%rsp)
	mulsd	%xmm1, %xmm2
	addsd	%xmm2, %xmm0
	movsd	264+__exp2f_data(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm1
	addsd	272+__exp2f_data(%rip), %xmm2
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	mulsd	-16(%rsp), %xmm1
	cvtsd2ss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$-16777218, %edi
	ja	.L3
	leal	(%rdx,%rdx), %r8d
	leal	-1(%r8), %ecx
	cmpl	$-16777218, %ecx
	ja	.L50
	xorl	%edi, %edi
	testl	%edx, %edx
	js	.L51
.L13:
	cmpl	$8388607, %eax
	ja	.L4
	movl	%edx, -16(%rsp)
	movss	-16(%rsp), %xmm7
	mulss	.LC3(%rip), %xmm7
	movd	%xmm7, %edx
	andl	$2147483647, %edx
	leal	-192937984(%rdx), %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L49:
	ucomisd	.LC5(%rip), %xmm1
	ja	.L22
	ucomisd	.LC6(%rip), %xmm1
	jbe	.L19
	movss	.LC7(%rip), %xmm2
	testl	%edi, %edi
	je	.L52
	movss	.LC8(%rip), %xmm3
	movaps	%xmm3, %xmm7
	subss	%xmm2, %xmm7
	ucomiss	%xmm3, %xmm7
	jp	.L22
.L47:
	je	.L19
.L22:
	jmp	__math_oflowf
	.p2align 4,,10
	.p2align 3
.L19:
	movsd	.LC9(%rip), %xmm2
	ucomisd	%xmm1, %xmm2
	jnb	.L53
	movsd	.LC10(%rip), %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.L16
	jmp	__math_may_uflowf
	.p2align 4,,10
	.p2align 3
.L51:
	movd	%xmm1, %r8d
	shrl	$23, %r8d
	movzbl	%r8b, %r8d
	cmpl	$126, %r8d
	jle	.L14
	cmpl	$150, %r8d
	jg	.L15
	movl	$150, %ecx
	movl	$1, %edi
	subl	%r8d, %ecx
	sall	%cl, %edi
	leal	-1(%rdi), %ecx
	testl	%esi, %ecx
	jne	.L14
	andl	%esi, %edi
	movl	$65536, %ecx
	cmovne	%ecx, %edi
.L15:
	andl	$2147483647, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%edx, -16(%rsp)
	movss	-16(%rsp), %xmm0
	jmp	__math_invalidf
	.p2align 4,,10
	.p2align 3
.L53:
	jmp	__math_uflowf
	.p2align 4,,10
	.p2align 3
.L3:
	testl	%ecx, %ecx
	jne	.L6
	xorl	$4194304, %eax
	movss	.LC0(%rip), %xmm0
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	jbe	.L1
.L9:
	movl	%edx, -16(%rsp)
	movss	-16(%rsp), %xmm0
	addss	%xmm1, %xmm0
	ret
.L55:
	xorl	$4194304, %esi
	andl	$2147483647, %esi
	cmpl	$2143289344, %esi
	ja	.L9
.L28:
	movss	.LC0(%rip), %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L50:
	movss	%xmm0, -16(%rsp)
	xorl	%edi, %edi
	testl	%edx, %edx
	movss	-16(%rsp), %xmm0
	mulss	%xmm0, %xmm0
	js	.L54
.L11:
	testl	%r8d, %r8d
	jne	.L12
	testl	%esi, %esi
	jns	.L1
	jmp	__math_divzerof
	.p2align 4,,10
	.p2align 3
.L52:
	movss	.LC0(%rip), %xmm3
	addss	%xmm3, %xmm2
	ucomiss	%xmm3, %xmm2
	jnp	.L47
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1065353216, %eax
	je	.L55
	addl	%eax, %eax
	cmpl	$-16777216, %ecx
	jne	.L9
	cmpl	$-16777216, %eax
	ja	.L9
	cmpl	$2130706432, %eax
	je	.L28
	cmpl	$2130706431, %eax
	movl	%esi, %eax
	notl	%eax
	seta	%dl
	shrl	$31, %eax
	pxor	%xmm0, %xmm0
	cmpb	%al, %dl
	jne	.L1
	movaps	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	testl	%esi, %esi
	jns	.L1
	movss	.LC0(%rip), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
.L54:
	movd	%xmm1, %eax
	shrl	$23, %eax
	movzbl	%al, %eax
	leal	-127(%rax), %edx
	cmpl	$23, %edx
	ja	.L11
	movl	$150, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	leal	-1(%rax), %edx
	testl	%esi, %edx
	jne	.L11
	andl	%esi, %eax
	movl	%eax, %edi
	je	.L11
	xorps	.LC2(%rip), %xmm0
	movl	$1, %edi
	jmp	.L11
	.size	__powf, .-__powf
	.weak	powf32
	.set	powf32,__powf
	.globl	__ieee754_powf
	.set	__ieee754_powf,__powf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	1258291200
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	4291941745
	.long	1080033279
	.align 8
.LC6:
	.long	4288916194
	.long	1080033279
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	855638016
	.align 4
.LC8:
	.long	3212836864
	.section	.rodata.cst8
	.align 8
.LC9:
	.long	0
	.long	-1067270144
	.align 8
.LC10:
	.long	0
	.long	-1067278336
	.hidden	__math_divzerof
	.hidden	__math_uflowf
	.hidden	__math_invalidf
	.hidden	__math_may_uflowf
	.hidden	__math_oflowf
	.hidden	__exp2f_data
	.hidden	__powf_log2_data
