	.text
	.globl	__subtf3
	.globl	__addtf3
	.globl	__extenddftf2
	.p2align 4,,15
	.globl	__ieee754_rem_pio2f128
	.type	__ieee754_rem_pio2f128, @function
__ieee754_rem_pio2f128:
	pushq	%rbp
	pushq	%rbx
	movabsq	$9223372036854775807, %rax
	movabsq	$4611283733356757713, %rdx
	subq	$120, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbp
	andq	%rbp, %rax
	cmpq	%rdx, %rax
	jle	.L12
	movabsq	$4611736148345303580, %rdx
	movq	%rdi, %rbx
	cmpq	%rdx, %rax
	movdqa	(%rsp), %xmm0
	jg	.L4
	testq	%rbp, %rbp
	movdqa	.LC1(%rip), %xmm1
	jle	.L5
	call	__subtf3@PLT
	movdqa	.LC2(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, (%rbx)
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rbx)
	movl	$1, %eax
.L1:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rax
	jg	.L13
	movq	%rax, %rsi
	movq	%xmm0, %rcx
	movq	%rax, %rdx
	sarq	$25, %rsi
	pxor	%xmm0, %xmm0
	andl	$8388607, %esi
	sarq	$48, %rdx
	leaq	48(%rsp), %rdi
	orq	$8388608, %rsi
	subq	$16406, %rdx
	leaq	two_over_pi(%rip), %r9
	cvtsi2sdq	%rsi, %xmm0
	movq	%rax, %rsi
	sarq	%rsi
	salq	$23, %rax
	movl	$3, %r8d
	andl	$16777215, %esi
	movsd	%xmm0, 48(%rsp)
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rsi, %xmm0
	movq	%rcx, %rsi
	shrq	$41, %rsi
	orq	%rsi, %rax
	leaq	40(%rdi), %rsi
	andl	$16777215, %eax
	movsd	%xmm0, 56(%rsp)
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movq	%rcx, %rax
	shrq	$17, %rax
	salq	$7, %rcx
	andl	$16777215, %eax
	andl	$16777215, %ecx
	testq	%rcx, %rcx
	movsd	%xmm0, 64(%rsp)
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	movsd	%xmm0, 72(%rsp)
	pxor	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	setne	%cl
	movzbl	%cl, %ecx
	addl	$4, %ecx
	movsd	%xmm0, 80(%rsp)
	call	__kernel_rem_pio2@PLT
	movsd	96(%rsp), %xmm0
	movl	%eax, 16(%rsp)
	call	__extenddftf2@PLT
	movaps	%xmm0, (%rsp)
	movsd	104(%rsp), %xmm0
	call	__extenddftf2@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movsd	88(%rsp), %xmm0
	call	__extenddftf2@PLT
	movl	16(%rsp), %eax
	testq	%rbp, %rbp
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	movl	%eax, 44(%rsp)
	movdqa	(%rsp), %xmm0
	js	.L8
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm2
	movaps	%xmm0, (%rbx)
	movdqa	%xmm2, %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movl	44(%rsp), %eax
	movaps	%xmm0, 16(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movaps	%xmm0, (%rdi)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movaps	%xmm0, 16(%rdi)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movdqa	%xmm0, %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rbx)
	xorl	%eax, %eax
	movaps	%xmm0, 16(%rbx)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC3(%rip), %xmm0
	movdqa	%xmm1, %xmm3
	movdqa	(%rsp), %xmm5
	pxor	%xmm0, %xmm3
	movdqa	16(%rsp), %xmm2
	pxor	%xmm0, %xmm5
	movaps	%xmm3, (%rbx)
	movdqa	%xmm2, %xmm0
	movaps	%xmm5, (%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movl	44(%rsp), %eax
	movaps	%xmm0, 16(%rbx)
	negl	%eax
	jmp	.L1
.L5:
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, (%rbx)
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC2(%rip), %xmm1
	call	__addtf3@PLT
	movl	$-1, %eax
	movaps	%xmm0, 16(%rbx)
	jmp	.L1
	.size	__ieee754_rem_pio2f128, .-__ieee754_rem_pio2f128
	.section	.rodata
	.align 32
	.type	two_over_pi, @object
	.size	two_over_pi, 3752
two_over_pi:
	.long	10680707
	.long	7228996
	.long	1387004
	.long	2578385
	.long	16069853
	.long	12639074
	.long	9804092
	.long	4427841
	.long	16666979
	.long	11263675
	.long	12935607
	.long	2387514
	.long	4345298
	.long	14681673
	.long	3074569
	.long	13734428
	.long	16653803
	.long	1880361
	.long	10960616
	.long	8533493
	.long	3062596
	.long	8710556
	.long	7349940
	.long	6258241
	.long	3772886
	.long	3769171
	.long	3798172
	.long	8675211
	.long	12450088
	.long	3874808
	.long	9961438
	.long	366607
	.long	15675153
	.long	9132554
	.long	7151469
	.long	3571407
	.long	2607881
	.long	12013382
	.long	4155038
	.long	6285869
	.long	7677882
	.long	13102053
	.long	15825725
	.long	473591
	.long	9065106
	.long	15363067
	.long	6271263
	.long	9264392
	.long	5636912
	.long	4652155
	.long	7056368
	.long	13614112
	.long	10155062
	.long	1944035
	.long	9527646
	.long	15080200
	.long	6658437
	.long	6231200
	.long	6832269
	.long	16767104
	.long	5075751
	.long	3212806
	.long	1398474
	.long	7579849
	.long	6349435
	.long	12618859
	.long	4703257
	.long	12806093
	.long	14477321
	.long	2786137
	.long	12875403
	.long	9837734
	.long	14528324
	.long	13719321
	.long	343717
	.long	16713477
	.long	4161075
	.long	15217346
	.long	14569368
	.long	3308987
	.long	12795174
	.long	15690526
	.long	6224031
	.long	3809077
	.long	13300351
	.long	1935345
	.long	2199676
	.long	8135786
	.long	16412373
	.long	7810352
	.long	4406037
	.long	12981429
	.long	10295747
	.long	12764333
	.long	4279596
	.long	6094860
	.long	4619654
	.long	2978275
	.long	10143387
	.long	25139
	.long	8180404
	.long	9938868
	.long	13980983
	.long	16137943
	.long	1577123
	.long	16545357
	.long	2792804
	.long	11261808
	.long	16284771
	.long	5746810
	.long	15144215
	.long	5654976
	.long	14276155
	.long	3703975
	.long	13312804
	.long	7834326
	.long	2315354
	.long	12132096
	.long	1772273
	.long	14667289
	.long	16724383
	.long	6954598
	.long	6379417
	.long	4717484
	.long	14188414
	.long	12018978
	.long	9037874
	.long	6340582
	.long	13485295
	.long	603756
	.long	13909853
	.long	14147094
	.long	14564184
	.long	9608158
	.long	2630354
	.long	15238696
	.long	5069026
	.long	3328710
	.long	1499912
	.long	13336032
	.long	5292055
	.long	10952179
	.long	6021144
	.long	3412782
	.long	6427267
	.long	84099
	.long	6000373
	.long	8368301
	.long	15919390
	.long	4409928
	.long	13854480
	.long	14212522
	.long	4349870
	.long	13525354
	.long	10758154
	.long	11835859
	.long	15902214
	.long	8353628
	.long	8635043
	.long	8928353
	.long	7893898
	.long	5934255
	.long	12441455
	.long	6530605
	.long	13352948
	.long	15696269
	.long	6799654
	.long	4573781
	.long	3594698
	.long	13805608
	.long	9265602
	.long	7850258
	.long	1320452
	.long	10176018
	.long	12868036
	.long	4507080
	.long	9548365
	.long	15931136
	.long	11355092
	.long	15026473
	.long	1103357
	.long	16563712
	.long	13407262
	.long	15650416
	.long	16072211
	.long	8450540
	.long	12838835
	.long	2685127
	.long	9700755
	.long	4092353
	.long	11733294
	.long	15942923
	.long	10228360
	.long	8069291
	.long	10466606
	.long	12751431
	.long	3093115
	.long	7165196
	.long	9480050
	.long	2090859
	.long	9882417
	.long	4855417
	.long	14842177
	.long	9035764
	.long	9934056
	.long	8709858
	.long	9908633
	.long	7073160
	.long	3563359
	.long	982459
	.long	11835976
	.long	7119975
	.long	4354673
	.long	3300749
	.long	12064159
	.long	648636
	.long	2437517
	.long	3765495
	.long	1836336
	.long	68621
	.long	6817867
	.long	5828140
	.long	9480775
	.long	190324
	.long	2414269
	.long	10911223
	.long	7489646
	.long	15668895
	.long	10917006
	.long	16159156
	.long	5329873
	.long	15862479
	.long	3381280
	.long	8276981
	.long	6841266
	.long	6242013
	.long	220480
	.long	8358277
	.long	2708053
	.long	12608567
	.long	1103981
	.long	3295282
	.long	7687259
	.long	13922638
	.long	7230533
	.long	12650763
	.long	6944042
	.long	13985300
	.long	10290983
	.long	5243997
	.long	14367668
	.long	12941034
	.long	1571207
	.long	8219465
	.long	12199709
	.long	2714006
	.long	11324614
	.long	5510317
	.long	7004816
	.long	9034120
	.long	5272108
	.long	12493828
	.long	9701239
	.long	7352563
	.long	2620416
	.long	11039210
	.long	4833894
	.long	4055140
	.long	8641943
	.long	9912227
	.long	16618563
	.long	9209357
	.long	14565681
	.long	10303890
	.long	9203933
	.long	15185687
	.long	3923720
	.long	2832149
	.long	10518620
	.long	9666650
	.long	9572624
	.long	14215183
	.long	11501676
	.long	4980699
	.long	1019960
	.long	7739481
	.long	1418594
	.long	12307297
	.long	12159431
	.long	12402704
	.long	324306
	.long	2585929
	.long	16168683
	.long	12264155
	.long	11146250
	.long	3090057
	.long	7766884
	.long	3357449
	.long	1741838
	.long	11156049
	.long	12755741
	.long	11464111
	.long	1189468
	.long	5096045
	.long	10254893
	.long	9918144
	.long	8601347
	.long	16183305
	.long	9191467
	.long	10039661
	.long	504889
	.long	1384460
	.long	6013912
	.long	12882677
	.long	4959686
	.long	10865230
	.long	13449127
	.long	3582438
	.long	9736875
	.long	6832861
	.long	14574361
	.long	15699062
	.long	5409640
	.long	3660796
	.long	11248046
	.long	3216863
	.long	10595840
	.long	14351116
	.long	6704484
	.long	11994605
	.long	3171625
	.long	12539479
	.long	3866439
	.long	12188010
	.long	15974005
	.long	14652200
	.long	3178667
	.long	16157798
	.long	1428228
	.long	402170
	.long	1959129
	.long	10793789
	.long	9378647
	.long	642358
	.long	15286862
	.long	10796563
	.long	11871027
	.long	1747696
	.long	11035983
	.long	10863058
	.long	999179
	.long	13465691
	.long	7797027
	.long	297851
	.long	7477129
	.long	5482182
	.long	14839407
	.long	60399
	.long	5786267
	.long	12049092
	.long	12215978
	.long	13619062
	.long	1901265
	.long	3010993
	.long	12687756
	.long	7843267
	.long	14305414
	.long	10509815
	.long	16023750
	.long	3141804
	.long	10153181
	.long	12344383
	.long	7200464
	.long	2082704
	.long	11983658
	.long	3810723
	.long	10137344
	.long	9655213
	.long	284598
	.long	11808041
	.long	8290379
	.long	10946522
	.long	961142
	.long	10574203
	.long	2757142
	.long	2996188
	.long	16639482
	.long	16702345
	.long	16629385
	.long	7108324
	.long	16558342
	.long	7372862
	.long	1404549
	.long	16746493
	.long	474664
	.long	3368801
	.long	8788010
	.long	15383885
	.long	11528115
	.long	7237007
	.long	3762069
	.long	6012721
	.long	4773764
	.long	1498928
	.long	4402631
	.long	3498277
	.long	13529289
	.long	12110640
	.long	16608447
	.long	10616996
	.long	14969861
	.long	10542426
	.long	4681505
	.long	13767266
	.long	8674489
	.long	4809072
	.long	14702187
	.long	86681
	.long	3626320
	.long	12047646
	.long	12906803
	.long	6254099
	.long	14954589
	.long	11087493
	.long	12825117
	.long	3551905
	.long	10794760
	.long	13939178
	.long	2225942
	.long	14969231
	.long	7864103
	.long	8389388
	.long	2965645
	.long	10538319
	.long	10069280
	.long	13869747
	.long	679215
	.long	4389300
	.long	13359633
	.long	13680253
	.long	12704667
	.long	12392363
	.long	8495818
	.long	6056456
	.long	1529134
	.long	5570599
	.long	15733887
	.long	8783841
	.long	6556436
	.long	9257366
	.long	14597767
	.long	2817498
	.long	11937131
	.long	3443067
	.long	16708357
	.long	10403769
	.long	5204584
	.long	11020874
	.long	5948495
	.long	12384301
	.long	9984727
	.long	9816052
	.long	9260301
	.long	10893856
	.long	6248356
	.long	11616020
	.long	9779328
	.long	73932
	.long	8838513
	.long	11984585
	.long	16081087
	.long	1140045
	.long	7014145
	.long	11317388
	.long	13680818
	.long	4740433
	.long	981790
	.long	12808853
	.long	3868323
	.long	3489984
	.long	8117254
	.long	13387232
	.long	16394574
	.long	13159126
	.long	4322280
	.long	14574716
	.long	14181531
	.long	3260121
	.long	12818340
	.long	13916279
	.long	12968809
	.long	1301232
	.long	3947194
	.long	4593734
	.long	6255957
	.long	16104914
	.long	13013614
	.long	6106796
	.long	15549454
	.long	4341276
	.long	8897633
	.long	15334697
	.long	15980263
	.long	13270050
	.long	3510639
	.long	12967944
	.long	9295871
	.long	14838382
	.long	13041072
	.long	12650643
	.long	7626108
	.long	11709803
	.long	10317517
	.long	8090174
	.long	6951366
	.long	11128823
	.long	14644009
	.long	12241333
	.long	5308599
	.long	897762
	.long	2407028
	.long	6323685
	.long	9099380
	.long	2888973
	.long	792705
	.long	9725566
	.long	1452289
	.long	7764639
	.long	12516861
	.long	15680854
	.long	3571417
	.long	1300972
	.long	12171915
	.long	16553924
	.long	2598961
	.long	12807921
	.long	3589524
	.long	5679320
	.long	11905204
	.long	969935
	.long	2984210
	.long	3430255
	.long	9000492
	.long	14929561
	.long	12132566
	.long	11165291
	.long	10234430
	.long	13393681
	.long	4852733
	.long	16512225
	.long	7158670
	.long	2918114
	.long	8705257
	.long	11121916
	.long	13758191
	.long	13186350
	.long	6371631
	.long	4464952
	.long	13162779
	.long	720001
	.long	6966011
	.long	14162991
	.long	8696915
	.long	9214286
	.long	13378132
	.long	14439722
	.long	14075584
	.long	9836811
	.long	12087322
	.long	6591849
	.long	6314534
	.long	15618623
	.long	987519
	.long	1160692
	.long	16108540
	.long	2997300
	.long	15645748
	.long	13393384
	.long	6315741
	.long	10194535
	.long	15676306
	.long	12064713
	.long	10180705
	.long	12343265
	.long	13009745
	.long	1064664
	.long	4747741
	.long	14490669
	.long	10557615
	.long	4598817
	.long	14152537
	.long	9992921
	.long	12604574
	.long	16418383
	.long	16516694
	.long	11434469
	.long	3547785
	.long	2272568
	.long	14455655
	.long	11200597
	.long	3679874
	.long	10217418
	.long	10751313
	.long	11613081
	.long	972713
	.long	4719977
	.long	15774309
	.long	10979455
	.long	9915528
	.long	3592697
	.long	11768353
	.long	4883067
	.long	2215832
	.long	14458688
	.long	5588956
	.long	3831009
	.long	4385639
	.long	14654974
	.long	6280286
	.long	10774395
	.long	8039610
	.long	10679893
	.long	2328619
	.long	5618241
	.long	552537
	.long	8792609
	.long	8603449
	.long	15131529
	.long	13934309
	.long	4258633
	.long	15292159
	.long	13242140
	.long	9066949
	.long	2882196
	.long	12960211
	.long	13616399
	.long	11426523
	.long	8832327
	.long	6439813
	.long	3900961
	.long	9730348
	.long	8872208
	.long	8080426
	.long	1715328
	.long	1228611
	.long	9447048
	.long	8993912
	.long	14992552
	.long	8117221
	.long	12729028
	.long	15397926
	.long	9070583
	.long	12554765
	.long	2859877
	.long	11637565
	.long	752829
	.long	14438820
	.long	6544679
	.long	14541161
	.long	1676442
	.long	9775528
	.long	2674280
	.long	11857161
	.long	2137924
	.long	13277262
	.long	6521456
	.long	2325630
	.long	3324175
	.long	9368999
	.long	15160852
	.long	586017
	.long	2792885
	.long	5078639
	.long	5314981
	.long	11270581
	.long	14081922
	.long	6413718
	.long	144918
	.long	10435268
	.long	10592899
	.long	7204210
	.long	8031545
	.long	11122818
	.long	6042219
	.long	5973830
	.long	15545344
	.long	7798994
	.long	5633276
	.long	5069057
	.long	8417760
	.long	14761865
	.long	11703795
	.long	6596849
	.long	11446091
	.long	3734604
	.long	15381179
	.long	4663051
	.long	11256743
	.long	3455584
	.long	5430580
	.long	16275002
	.long	15433354
	.long	3259190
	.long	5805495
	.long	4716457
	.long	9749418
	.long	13865509
	.long	1998654
	.long	14194510
	.long	12298575
	.long	14200321
	.long	15098292
	.long	3751310
	.long	10822570
	.long	3344014
	.long	1455029
	.long	3912072
	.long	1915549
	.long	4199376
	.long	13376485
	.long	16278247
	.long	3928719
	.long	3782079
	.long	770613
	.long	2258046
	.long	10635200
	.long	13970719
	.long	1682899
	.long	9475295
	.long	9638352
	.long	11807525
	.long	4812210
	.long	14836914
	.long	6222133
	.long	3917476
	.long	1723088
	.long	11192622
	.long	6613126
	.long	5714065
	.long	9972987
	.long	3218202
	.long	553611
	.long	12439265
	.long	6349122
	.long	15426589
	.long	13679523
	.long	15030740
	.long	5864332
	.long	2770051
	.long	15938234
	.long	16271463
	.long	2919707
	.long	3123632
	.long	5305309
	.long	16381299
	.long	14377396
	.long	16655166
	.long	7094694
	.long	15639320
	.long	7821862
	.long	12344924
	.long	13543176
	.long	9755735
	.long	14811542
	.long	15853625
	.long	12470353
	.long	6106415
	.long	5150037
	.long	14249666
	.long	15193941
	.long	6489312
	.long	12594702
	.long	16531616
	.long	12319155
	.long	7415207
	.long	2239995
	.long	16128472
	.long	8621084
	.long	6691302
	.long	11668821
	.long	12276023
	.long	7989257
	.long	11505993
	.long	881630
	.long	724389
	.long	13533016
	.long	11278644
	.long	7489127
	.long	8002067
	.long	10364604
	.long	4543975
	.long	5790901
	.long	7413012
	.long	4745617
	.long	4722016
	.long	5680555
	.long	14036836
	.long	9891340
	.long	2174963
	.long	6122888
	.long	10909316
	.long	9790750
	.long	11247114
	.long	5103358
	.long	5705744
	.long	8612409
	.long	16312881
	.long	10369053
	.long	15386801
	.long	13289835
	.long	3666462
	.long	13960681
	.long	9979715
	.long	10470764
	.long	209335
	.long	3902456
	.long	8840554
	.long	9290563
	.long	6435047
	.long	9686997
	.long	6983469
	.long	7548933
	.long	1765409
	.long	10526667
	.long	7607713
	.long	13986595
	.long	7337765
	.long	3099591
	.long	12101009
	.long	8367193
	.long	6051294
	.long	2174008
	.long	11612822
	.long	6094672
	.long	3564130
	.long	16415510
	.long	16046502
	.long	2805735
	.long	15758339
	.long	13948420
	.long	7330070
	.long	3256767
	.long	13349968
	.long	6019016
	.long	844180
	.long	7067203
	.long	5232924
	.long	14632259
	.long	6239315
	.long	14857642
	.long	13217480
	.long	1250437
	.long	16372415
	.long	12245918
	.long	7796153
	.long	11474383
	.long	13250946
	.long	1357165
	.long	15334989
	.long	5307445
	.long	16101077
	.long	10670273
	.long	13197399
	.long	1650358
	.long	15277458
	.long	512324
	.long	11445190
	.long	3421542
	.long	2545076
	.long	3236322
	.long	3666338
	.long	2137855
	.long	9801251
	.long	4798360
	.long	3536038
	.long	4971522
	.long	12762643
	.long	12484768
	.long	750243
	.long	1137759
	.long	1973201
	.long	898259
	.long	8822405
	.long	9869163
	.long	2803999
	.long	9053890
	.long	3174640
	.long	267282
	.long	16555941
	.long	16198200
	.long	10250360
	.long	14854774
	.long	5296097
	.long	5608052
	.long	9653816
	.long	692983
	.long	5583856
	.long	10894772
	.long	3774833
	.long	14858069
	.long	11111036
	.long	36633
	.long	11293906
	.long	3055796
	.long	16118752
	.long	6342729
	.long	16765545
	.long	11424462
	.long	8019933
	.long	15322630
	.long	16452328
	.long	10816718
	.long	15375678
	.long	3630813
	.long	12072181
	.long	893072
	.long	8679304
	.long	4907989
	.long	629251
	.long	3059245
	.long	16561162
	.long	16489267
	.long	14844381
	.long	1775290
	.long	14205103
	.long	14248855
	.long	11873737
	.long	10233727
	.long	5853653
	.long	2215126
	.long	11946347
	.long	5784930
	.long	11778802
	.long	10593732
	.long	8168143
	.long	11123005
	.long	8092553
	.long	4734264
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC2:
	.long	549186148
	.long	2793195328
	.long	2418335880
	.long	1066192146
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
