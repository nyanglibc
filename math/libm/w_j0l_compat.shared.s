	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__j0l
	.type	__j0l, @function
__j0l:
	subq	$8, %rsp
	fldt	16(%rsp)
	fld	%st(0)
	fabs
	fldl	.LC0(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L14
.L2:
	fstpt	16(%rsp)
	addq	$8, %rsp
	jmp	__ieee754_j0l@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	subq	$32, %rsp
	movl	$234, %edi
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$40, %rsp
	ret
	.size	__j0l, .-__j0l
	.weak	j0f64x
	.set	j0f64x,__j0l
	.weak	j0l
	.set	j0l,__j0l
	.p2align 4,,15
	.globl	__y0l
	.type	__y0l, @function
__y0l:
	subq	$24, %rsp
	fldt	32(%rsp)
	fldz
	fucomip	%st(1), %st
	jnb	.L23
	fldl	.LC0(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L23
.L16:
	fstpt	32(%rsp)
	addq	$24, %rsp
	jmp	__ieee754_y0l@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L16
	fldz
	fucomi	%st(1), %st
	ja	.L37
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L21
	je	.L38
.L21:
	cmpl	$2, %eax
	je	.L16
	subq	$32, %rsp
	movl	$235, %edi
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
.L15:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	fstpt	(%rsp)
	movl	$4, %edi
	call	__GI_feraiseexcept
	subq	$32, %rsp
	movl	$208, %edi
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L37:
	fstp	%st(0)
	fstpt	(%rsp)
	movl	$1, %edi
	call	__GI_feraiseexcept
	subq	$32, %rsp
	movl	$209, %edi
	fldt	32(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L15
	.size	__y0l, .-__y0l
	.weak	y0f64x
	.set	y0f64x,__y0l
	.weak	y0l
	.set	y0l,__y0l
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1413754136
	.long	1128866299
