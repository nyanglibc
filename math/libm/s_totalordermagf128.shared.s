	.text
#APP
	.symver __totalordermagf1280,totalordermagf128@@GLIBC_2.31
	.symver __totalordermag_compatf1281,totalordermagf128@GLIBC_2.26
#NO_APP
	.p2align 4,,15
	.globl	__totalordermagf128
	.type	__totalordermagf128, @function
__totalordermagf128:
	movdqa	(%rdi), %xmm0
	movabsq	$9223372036854775807, %rax
	movdqa	(%rsi), %xmm1
	movaps	%xmm0, -40(%rsp)
	movq	-32(%rsp), %rcx
	movaps	%xmm1, -24(%rsp)
	andq	%rax, %rcx
	movq	-16(%rsp), %rdx
	andq	%rax, %rdx
	movl	$1, %eax
	cmpq	%rdx, %rcx
	jb	.L1
	movq	-40(%rsp), %rcx
	movq	-24(%rsp), %rdx
	sete	%sil
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	setbe	%al
	andl	%esi, %eax
.L1:
	rep ret
	.size	__totalordermagf128, .-__totalordermagf128
	.globl	__totalordermagf1280
	.set	__totalordermagf1280,__totalordermagf128
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalordermag_compatf128
	.type	__totalordermag_compatf128, @function
__totalordermag_compatf128:
	subq	$40, %rsp
	leaq	16(%rsp), %rdi
	movq	%rsp, %rsi
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm1, (%rsp)
	call	__totalordermagf128@PLT
	addq	$40, %rsp
	ret
	.size	__totalordermag_compatf128, .-__totalordermag_compatf128
	.globl	__totalordermag_compatf1281
	.set	__totalordermag_compatf1281,__totalordermag_compatf128
