	.text
	.p2align 4,,15
	.globl	__ieee754_atan2f
	.type	__ieee754_atan2f, @function
__ieee754_atan2f:
	pushq	%rbx
	movd	%xmm0, %edx
	subq	$16, %rsp
	andl	$2147483647, %edx
	movss	%xmm1, 12(%rsp)
	movl	12(%rsp), %eax
	movl	%eax, %esi
	andl	$2147483647, %esi
	cmpl	$2139095040, %esi
	jg	.L29
	cmpl	$2139095040, %edx
	jg	.L29
	cmpl	$1065353216, %eax
	je	.L53
	movl	%eax, %ebx
	movd	%xmm0, %edi
	movd	%xmm0, %ecx
	sarl	$30, %ebx
	andl	$2, %ebx
	shrl	$31, %edi
	orl	%edi, %ebx
	testl	%edx, %edx
	jne	.L6
	cmpl	$2, %ebx
	je	.L7
	cmpl	$3, %ebx
	je	.L8
	movaps	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movss	12(%rsp), %xmm1
	addss	%xmm0, %xmm1
.L1:
	addq	$16, %rsp
	movaps	%xmm1, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%esi, %esi
	je	.L51
	cmpl	$2139095040, %esi
	je	.L54
	cmpl	$2139095040, %edx
	je	.L51
	subl	%esi, %edx
	sarl	$23, %edx
	cmpl	$60, %edx
	jg	.L55
	testl	%eax, %eax
	jns	.L30
	cmpl	$-60, %edx
	pxor	%xmm1, %xmm1
	jge	.L30
.L21:
	cmpl	$1, %ebx
	je	.L24
	cmpl	$2, %ebx
	je	.L25
	testl	%ebx, %ebx
	je	.L1
	addss	.LC13(%rip), %xmm1
	subss	.LC2(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L51:
	testl	%ecx, %ecx
	js	.L56
	movss	.LC3(%rip), %xmm1
	addss	.LC6(%rip), %xmm1
	addq	$16, %rsp
	popq	%rbx
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$16, %rsp
	popq	%rbx
	jmp	__atanf@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movss	.LC3(%rip), %xmm1
	addss	.LC2(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L56:
	movss	.LC5(%rip), %xmm1
	subss	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movss	.LC4(%rip), %xmm1
	subss	.LC3(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	movss	.LC6(%rip), %xmm1
	subss	.LC11(%rip), %xmm1
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L54:
	cmpl	$2139095040, %edx
	je	.L57
	cmpl	$2, %ebx
	je	.L7
	cmpl	$3, %ebx
	je	.L8
	cmpl	$1, %ebx
	movss	.LC1(%rip), %xmm1
	je	.L1
	pxor	%xmm1, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	addss	.LC13(%rip), %xmm1
	movss	.LC2(%rip), %xmm0
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movd	%xmm1, %eax
	addl	$-2147483648, %eax
	movl	%eax, 12(%rsp)
	movd	12(%rsp), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$2, %ebx
	je	.L14
	cmpl	$3, %ebx
	je	.L15
	cmpl	$1, %ebx
	je	.L16
	movss	.LC3(%rip), %xmm1
	addss	.LC7(%rip), %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movss	12(%rsp), %xmm1
	divss	%xmm1, %xmm0
	andps	.LC12(%rip), %xmm0
	call	__atanf@PLT
	movaps	%xmm0, %xmm1
	jmp	.L21
.L16:
	movss	.LC8(%rip), %xmm1
	subss	.LC3(%rip), %xmm1
	jmp	.L1
.L15:
	movss	.LC10(%rip), %xmm1
	mulss	.LC7(%rip), %xmm1
	subss	.LC3(%rip), %xmm1
	jmp	.L1
.L14:
	movss	.LC9(%rip), %xmm1
	mulss	.LC7(%rip), %xmm1
	addss	.LC3(%rip), %xmm1
	jmp	.L1
	.size	__ieee754_atan2f, .-__ieee754_atan2f
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2147483648
	.align 4
.LC2:
	.long	1078530011
	.align 4
.LC3:
	.long	228737632
	.align 4
.LC4:
	.long	3226013659
	.align 4
.LC5:
	.long	3217625051
	.align 4
.LC6:
	.long	1070141403
	.align 4
.LC7:
	.long	1061752795
	.align 4
.LC8:
	.long	3209236443
	.align 4
.LC9:
	.long	1077936128
	.align 4
.LC10:
	.long	3225419776
	.align 4
.LC11:
	.long	859553070
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC12:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC13:
	.long	867941678
