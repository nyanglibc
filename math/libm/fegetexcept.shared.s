	.text
	.p2align 4,,15
	.globl	fegetexcept
	.type	fegetexcept, @function
fegetexcept:
#APP
# 28 "../sysdeps/x86_64/fpu/fegetexcept.c" 1
	fstcw -2(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-2(%rsp), %eax
	notl	%eax
	andl	$61, %eax
	ret
	.size	fegetexcept, .-fegetexcept
