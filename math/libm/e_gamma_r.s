	.text
	.p2align 4,,15
	.type	gamma_positive, @function
gamma_positive:
	pushq	%rbx
	movapd	%xmm0, %xmm2
	movq	%rdi, %rbx
	subq	$64, %rsp
	movsd	.LC2(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	ja	.L28
	movsd	.LC3(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	jnb	.L29
	movsd	.LC4(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	ja	.L30
	movsd	.LC7(%rip), %xmm0
	movq	$0x000000000, 56(%rsp)
	ucomisd	%xmm2, %xmm0
	ja	.L31
	pxor	%xmm5, %xmm5
	movsd	.LC1(%rip), %xmm7
	movsd	%xmm5, 40(%rsp)
	movsd	%xmm7, 24(%rsp)
	movsd	%xmm5, 16(%rsp)
.L11:
	movapd	%xmm2, %xmm0
	movsd	%xmm2, (%rsp)
	call	__round@PLT
	movsd	(%rsp), %xmm2
	leaq	52(%rsp), %rdi
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm2, %xmm7
	subsd	%xmm0, %xmm7
	movapd	%xmm2, %xmm0
	movsd	%xmm7, 32(%rsp)
	call	__frexp@PLT
	movsd	.LC8(%rip), %xmm3
	movsd	(%rsp), %xmm2
	ucomisd	%xmm0, %xmm3
	movsd	8(%rsp), %xmm1
	ja	.L14
	movl	52(%rsp), %edx
.L15:
	cvttsd2si	%xmm1, %eax
	movapd	%xmm2, %xmm1
	movsd	%xmm2, 8(%rsp)
	imull	%edx, %eax
	movl	%eax, (%rbx)
	call	__ieee754_pow@PLT
	movsd	%xmm0, (%rsp)
	pxor	%xmm0, %xmm0
	cvtsi2sd	52(%rsp), %xmm0
	mulsd	32(%rsp), %xmm0
	call	__ieee754_exp2@PLT
	movsd	(%rsp), %xmm6
	movsd	8(%rsp), %xmm2
	mulsd	%xmm0, %xmm6
	movapd	%xmm2, %xmm0
	xorpd	.LC9(%rip), %xmm0
	movsd	%xmm6, (%rsp)
	call	__ieee754_exp@PLT
	movsd	8(%rsp), %xmm2
	movsd	.LC10(%rip), %xmm3
	mulsd	(%rsp), %xmm0
	movsd	%xmm2, (%rsp)
	divsd	%xmm2, %xmm3
	sqrtsd	%xmm3, %xmm3
	mulsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm3
	movapd	%xmm2, %xmm0
	divsd	24(%rsp), %xmm3
	movsd	%xmm3, 8(%rsp)
	call	__ieee754_log@PLT
	movsd	(%rsp), %xmm2
	movsd	.LC11(%rip), %xmm1
	movapd	%xmm2, %xmm4
	mulsd	16(%rsp), %xmm0
	mulsd	%xmm2, %xmm4
	divsd	%xmm4, %xmm1
	subsd	40(%rsp), %xmm0
	addsd	.LC12(%rip), %xmm1
	divsd	%xmm4, %xmm1
	subsd	.LC13(%rip), %xmm1
	divsd	%xmm4, %xmm1
	addsd	.LC14(%rip), %xmm1
	divsd	%xmm4, %xmm1
	subsd	.LC15(%rip), %xmm1
	divsd	%xmm4, %xmm1
	addsd	.LC16(%rip), %xmm1
	divsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	call	__expm1@PLT
	movsd	8(%rsp), %xmm3
	addq	$64, %rsp
	popq	%rbx
	mulsd	%xmm3, %xmm0
	addsd	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$0, (%rdi)
	movapd	%xmm2, %xmm0
	leaq	56(%rsp), %rdi
	call	__ieee754_lgamma_r@PLT
	call	__ieee754_exp@PLT
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movapd	%xmm2, %xmm0
	movl	$0, (%rdi)
	leaq	56(%rsp), %rdi
	movsd	%xmm2, (%rsp)
	addsd	.LC1(%rip), %xmm0
	call	__ieee754_lgamma_r@PLT
	call	__ieee754_exp@PLT
	movsd	(%rsp), %xmm2
	addq	$64, %rsp
	popq	%rbx
	divsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	52(%rsp), %eax
	addsd	%xmm0, %xmm0
	leal	-1(%rax), %edx
	movl	%edx, 52(%rsp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L30:
	movapd	%xmm2, %xmm4
	movl	$0, (%rdi)
	movsd	.LC6(%rip), %xmm3
	subsd	%xmm0, %xmm4
	movsd	.LC5(%rip), %xmm5
	movapd	%xmm4, %xmm1
	movapd	%xmm4, %xmm0
	andpd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm5
	jbe	.L9
	cvttsd2siq	%xmm4, %rax
	pxor	%xmm1, %xmm1
	movsd	.LC1(%rip), %xmm7
	andnpd	%xmm4, %xmm3
	cvtsi2sdq	%rax, %xmm1
	cmpnlesd	%xmm1, %xmm0
	andpd	%xmm7, %xmm0
	addsd	%xmm1, %xmm0
	orpd	%xmm3, %xmm0
.L9:
	cvttsd2si	%xmm0, %edi
	subsd	%xmm0, %xmm2
	leaq	56(%rsp), %rsi
	pxor	%xmm1, %xmm1
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 8(%rsp)
	call	__gamma_product@PLT
	movsd	8(%rsp), %xmm2
	leaq	52(%rsp), %rdi
	movsd	%xmm0, (%rsp)
	movapd	%xmm2, %xmm0
	call	__ieee754_lgamma_r@PLT
	call	__ieee754_exp@PLT
	movsd	.LC1(%rip), %xmm1
	mulsd	(%rsp), %xmm0
	addsd	56(%rsp), %xmm1
	addq	$64, %rsp
	popq	%rbx
	mulsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	subsd	%xmm2, %xmm0
	movsd	.LC6(%rip), %xmm4
	movsd	.LC5(%rip), %xmm5
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm1
	andpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm5
	jbe	.L13
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm3, %xmm3
	movsd	.LC1(%rip), %xmm5
	andnpd	%xmm0, %xmm4
	cvtsi2sdq	%rax, %xmm3
	cmpnlesd	%xmm3, %xmm1
	andpd	%xmm5, %xmm1
	addsd	%xmm3, %xmm1
	orpd	%xmm4, %xmm1
.L13:
	movapd	%xmm2, %xmm3
	cvttsd2si	%xmm1, %edi
	movapd	%xmm2, %xmm6
	leaq	56(%rsp), %rsi
	addsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm0
	movsd	%xmm3, (%rsp)
	subsd	%xmm1, %xmm0
	subsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm1
	movsd	%xmm6, 16(%rsp)
	call	__gamma_product@PLT
	movsd	56(%rsp), %xmm6
	movsd	(%rsp), %xmm3
	movsd	%xmm0, 24(%rsp)
	movsd	%xmm6, 40(%rsp)
	movapd	%xmm3, %xmm2
	jmp	.L11
	.size	gamma_positive, .-gamma_positive
	.p2align 4,,15
	.globl	__ieee754_gamma_r
	.type	__ieee754_gamma_r, @function
__ieee754_gamma_r:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	shrq	$32, %rax
	movl	%eax, %ecx
	andl	$2147483647, %ecx
	orl	%edx, %ecx
	je	.L97
	testl	%eax, %eax
	js	.L98
.L35:
	andl	$2146435072, %eax
	cmpl	$2146435072, %eax
	je	.L99
	ucomisd	.LC17(%rip), %xmm0
	jnb	.L100
	pushq	%r12
	pushq	%rbp
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$80, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 72(%rsp)
# 0 "" 2
#NO_APP
	movl	72(%rsp), %ebp
	movl	%ebp, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebp
	movl	%eax, 76(%rsp)
	jne	.L101
.L42:
	pxor	%xmm3, %xmm3
	movq	%rdi, %rbx
	movapd	%xmm0, %xmm2
	ucomisd	%xmm3, %xmm0
	ja	.L102
	ucomisd	.LC19(%rip), %xmm0
	jnb	.L103
	movq	.LC6(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movsd	.LC5(%rip), %xmm7
	andpd	%xmm1, %xmm5
	movapd	%xmm1, %xmm4
	ucomisd	%xmm5, %xmm7
	jbe	.L48
	cvttsd2siq	%xmm0, %rax
	pxor	%xmm0, %xmm0
	andnpd	%xmm2, %xmm4
	cvtsi2sdq	%rax, %xmm0
	orpd	%xmm4, %xmm0
.L48:
	movsd	.LC2(%rip), %xmm5
	movapd	%xmm0, %xmm4
	movapd	%xmm1, %xmm6
	mulsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm8
	movapd	%xmm4, %xmm9
	andpd	%xmm1, %xmm8
	ucomisd	%xmm8, %xmm7
	jbe	.L49
	cvttsd2siq	%xmm4, %rax
	pxor	%xmm4, %xmm4
	andnpd	%xmm9, %xmm6
	cvtsi2sdq	%rax, %xmm4
	orpd	%xmm6, %xmm4
.L49:
	addsd	%xmm4, %xmm4
	ucomisd	%xmm0, %xmm4
	jp	.L68
	movl	$-1, %eax
	je	.L50
.L68:
	movl	$1, %eax
.L50:
	movsd	.LC20(%rip), %xmm4
	movl	%eax, (%rbx)
	ucomisd	%xmm2, %xmm4
	jb	.L92
	movsd	.LC21(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L45:
	testb	%r12b, %r12b
	jne	.L104
.L61:
	andpd	%xmm0, %xmm1
	movsd	.LC18(%rip), %xmm4
	ucomisd	%xmm4, %xmm1
	jbe	.L62
	ucomisd	%xmm3, %xmm2
	jp	.L78
	je	.L62
.L78:
	movq	.LC9(%rip), %xmm1
	movl	(%rbx), %edx
	andpd	%xmm1, %xmm0
	testl	%edx, %edx
	orpd	.LC24(%rip), %xmm0
	js	.L105
	mulsd	%xmm4, %xmm0
.L32:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movsd	.LC18(%rip), %xmm0
	movl	$0, (%rdi)
	mulsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movsd	.LC1(%rip), %xmm0
	movl	$0, (%rdi)
	movq	.LC6(%rip), %xmm1
	divsd	%xmm2, %xmm0
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L62:
	ucomisd	%xmm3, %xmm0
	jp	.L32
	jne	.L32
	movq	.LC9(%rip), %xmm1
	movl	(%rbx), %eax
	andpd	%xmm1, %xmm0
	testl	%eax, %eax
	orpd	.LC25(%rip), %xmm0
	js	.L106
	mulsd	.LC21(%rip), %xmm0
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movl	$0, (%rdi)
	leaq	76(%rsp), %rdi
	movsd	%xmm3, 16(%rsp)
	movsd	%xmm0, 8(%rsp)
	call	gamma_positive
	movl	76(%rsp), %edi
	call	__scalbn@PLT
	movq	.LC6(%rip), %xmm1
	movsd	8(%rsp), %xmm2
	movsd	16(%rsp), %xmm3
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L97:
	movsd	.LC1(%rip), %xmm1
	movl	$0, (%rdi)
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	cmpl	$-1048577, %eax
	ja	.L36
	movq	.LC6(%rip), %xmm1
	movapd	%xmm0, %xmm4
	movsd	.LC5(%rip), %xmm3
	andpd	%xmm1, %xmm4
	movapd	%xmm0, %xmm2
	ucomisd	%xmm4, %xmm3
	jbe	.L37
	andnpd	%xmm0, %xmm1
	orpd	%xmm1, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm3, %xmm2
	orpd	%xmm1, %xmm2
.L37:
	ucomisd	%xmm0, %xmm2
	jp	.L36
	jne	.L36
	subsd	%xmm0, %xmm0
	movl	$0, (%rdi)
	divsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	cmpl	$-1048576, %eax
	jne	.L35
	testl	%edx, %edx
	jne	.L35
	movl	$0, (%rdi)
	subsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$0, (%rdi)
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	subsd	%xmm2, %xmm0
	ucomisd	%xmm5, %xmm0
	jbe	.L54
	movsd	.LC1(%rip), %xmm4
	subsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
.L54:
	movsd	.LC22(%rip), %xmm4
	movsd	%xmm2, 48(%rsp)
	ucomisd	%xmm0, %xmm4
	movaps	%xmm1, 16(%rsp)
	movsd	%xmm3, 8(%rsp)
	jnb	.L107
	subsd	%xmm0, %xmm5
	movsd	.LC23(%rip), %xmm6
	movsd	%xmm6, 56(%rsp)
	movapd	%xmm5, %xmm0
	mulsd	%xmm6, %xmm0
	call	__cos@PLT
	movsd	48(%rsp), %xmm2
	movapd	16(%rsp), %xmm1
	movsd	8(%rsp), %xmm3
.L58:
	movapd	%xmm2, %xmm4
	leaq	76(%rsp), %rdi
	movsd	%xmm3, 48(%rsp)
	movaps	%xmm1, 32(%rsp)
	movsd	%xmm2, 16(%rsp)
	xorpd	.LC9(%rip), %xmm4
	mulsd	%xmm4, %xmm0
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm4, %xmm0
	call	gamma_positive
	mulsd	8(%rsp), %xmm0
	movl	76(%rsp), %edi
	movsd	56(%rsp), %xmm6
	negl	%edi
	divsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm0
	call	__scalbn@PLT
	movsd	.LC21(%rip), %xmm4
	movsd	16(%rsp), %xmm2
	ucomisd	%xmm0, %xmm4
	movsd	48(%rsp), %xmm3
	movapd	32(%rsp), %xmm1
	jbe	.L45
	movapd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm4
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L106:
	xorpd	%xmm1, %xmm0
	mulsd	.LC21(%rip), %xmm0
	xorpd	%xmm1, %xmm0
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L107:
	movsd	.LC23(%rip), %xmm7
	mulsd	%xmm7, %xmm0
	movsd	%xmm7, 56(%rsp)
	call	__sin@PLT
	movsd	8(%rsp), %xmm3
	movapd	16(%rsp), %xmm1
	movsd	48(%rsp), %xmm2
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L101:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r12d
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L104:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	movl	76(%rsp), %eax
	andl	$24576, %ebp
	andb	$-97, %ah
	orl	%eax, %ebp
	movl	%ebp, 76(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 76(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L105:
	xorpd	%xmm1, %xmm0
	mulsd	%xmm4, %xmm0
	xorpd	%xmm1, %xmm0
	jmp	.L32
	.size	__ieee754_gamma_r, .-__ieee754_gamma_r
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	1071644672
	.align 8
.LC3:
	.long	0
	.long	1073217536
	.align 8
.LC4:
	.long	0
	.long	1075445760
	.align 8
.LC5:
	.long	0
	.long	1127219200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1076363264
	.align 8
.LC8:
	.long	1719614413
	.long	1072079006
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC10:
	.long	1413754136
	.long	1075388923
	.align 8
.LC11:
	.long	3650698365
	.long	-1084265808
	.align 8
.LC12:
	.long	723058467
	.long	1061917982
	.align 8
.LC13:
	.long	327235604
	.long	1061388600
	.align 8
.LC14:
	.long	436314138
	.long	1061814688
	.align 8
.LC15:
	.long	381774871
	.long	1063698796
	.align 8
.LC16:
	.long	1431655765
	.long	1068848469
	.align 8
.LC17:
	.long	0
	.long	1080393728
	.align 8
.LC18:
	.long	4294967295
	.long	2146435071
	.align 8
.LC19:
	.long	0
	.long	-1131413504
	.align 8
.LC20:
	.long	0
	.long	-1066991616
	.align 8
.LC21:
	.long	0
	.long	1048576
	.align 8
.LC22:
	.long	0
	.long	1070596096
	.align 8
.LC23:
	.long	1413754136
	.long	1074340347
	.section	.rodata.cst16
	.align 16
.LC24:
	.long	4294967295
	.long	2146435071
	.long	0
	.long	0
	.align 16
.LC25:
	.long	0
	.long	1048576
	.long	0
	.long	0
