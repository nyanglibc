 .section .rodata.cst16,"aM",@progbits,16
 .align 1<<4
 .type signmask,@object
signmask:
 .byte 0, 0, 0, 0, 0, 0, 0, 0x80
 .byte 0, 0, 0, 0, 0, 0, 0, 0
 .size signmask,.-signmask;
 .type othermask,@object
othermask:
 .byte 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f
 .byte 0, 0, 0, 0, 0, 0, 0, 0
 .size othermask,.-othermask;
 .text
.globl __copysign
.type __copysign,@function
.align 1<<4
__copysign: 
 andpd othermask(%rip),%xmm0
 andpd signmask(%rip),%xmm1
 orpd %xmm1,%xmm0
 ret
.size __copysign,.-__copysign
.weak copysign
copysign = __copysign
.weak copysignf64
copysignf64 = __copysign
.weak copysignf32x
copysignf32x = __copysign
