	.text
#APP
	.symver __ieee754_sqrt,__sqrt_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_sqrt
	.type	__ieee754_sqrt, @function
__ieee754_sqrt:
	sqrtsd	%xmm0, %xmm0
	ret
	.size	__ieee754_sqrt, .-__ieee754_sqrt
