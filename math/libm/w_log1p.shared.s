	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__w_log1p
	.type	__w_log1p, @function
__w_log1p:
	movsd	.LC0(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L7
.L2:
	jmp	__log1p@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	ucomisd	%xmm1, %xmm0
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__w_log1p, .-__w_log1p
	.weak	log1pf32x
	.set	log1pf32x,__w_log1p
	.weak	log1pf64
	.set	log1pf64,__w_log1p
	.weak	log1p
	.set	log1p,__w_log1p
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-1074790400
