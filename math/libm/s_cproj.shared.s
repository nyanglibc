	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__cproj
	.type	__cproj, @function
__cproj:
	movq	.LC0(%rip), %xmm2
	movapd	%xmm0, %xmm4
	movsd	.LC1(%rip), %xmm3
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.L2
	andpd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	jbe	.L1
.L2:
	andpd	.LC2(%rip), %xmm1
	movsd	.LC3(%rip), %xmm0
.L1:
	rep ret
	.size	__cproj, .-__cproj
	.weak	cprojf32x
	.set	cprojf32x,__cproj
	.weak	cprojf64
	.set	cprojf64,__cproj
	.weak	cproj
	.set	cproj,__cproj
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	2146435072
