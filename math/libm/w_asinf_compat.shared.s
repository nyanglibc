	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__asinf
	.type	__asinf, @function
__asinf:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L7
.L2:
	jmp	__ieee754_asinf@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$24, %rsp
	movl	$1, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	movss	12(%rsp), %xmm0
	movl	$102, %edi
	movaps	%xmm0, %xmm1
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	__asinf, .-__asinf
	.weak	asinf32
	.set	asinf32,__asinf
	.weak	asinf
	.set	asinf,__asinf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
