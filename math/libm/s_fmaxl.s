.text
.globl __fmaxl
.type __fmaxl,@function
.align 1<<4
__fmaxl:
 fldt 8(%rsp)
 fldt 24(%rsp)
 fucomi %st(1), %st
 jp 2f
 fcmovb %st(1), %st
 fstp %st(1)
 ret
2:
 fucomi %st(0), %st
 jp 3f
 testb $0x40, 15(%rsp)
 jz 4f
 fstp %st(1)
 ret
3:
 fxch
 fucomi %st(0), %st
 jp 4f
 testb $0x40, 31(%rsp)
 jz 4f
 fstp %st(1)
 ret
4:
 faddp
 ret
.size __fmaxl,.-__fmaxl
.weak fmaxl
fmaxl = __fmaxl
.weak fmaxf64x
fmaxf64x = __fmaxl
