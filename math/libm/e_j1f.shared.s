	.text
#APP
	.symver __ieee754_j1f,__j1f_finite@GLIBC_2.15
	.symver __ieee754_y1f,__y1f_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.type	ponef, @function
ponef:
	movd	%xmm0, %eax
	andl	$2147483647, %eax
	cmpl	$1090519039, %eax
	jg	.L3
	cmpl	$1089936471, %eax
	jg	.L4
	cmpl	$1077336935, %eax
	jg	.L5
	movss	.LC33(%rip), %xmm3
	movss	.LC34(%rip), %xmm2
	movss	.LC35(%rip), %xmm8
	movss	.LC36(%rip), %xmm7
	movss	.LC37(%rip), %xmm1
	movss	.LC38(%rip), %xmm13
	movss	.LC39(%rip), %xmm12
	movss	.LC40(%rip), %xmm11
	movss	.LC41(%rip), %xmm10
	movss	.LC42(%rip), %xmm6
	movss	.LC43(%rip), %xmm9
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movss	.LC11(%rip), %xmm3
	movss	.LC12(%rip), %xmm2
	movss	.LC13(%rip), %xmm8
	movss	.LC14(%rip), %xmm7
	movss	.LC15(%rip), %xmm1
	movss	.LC16(%rip), %xmm13
	movss	.LC17(%rip), %xmm12
	movss	.LC18(%rip), %xmm11
	movss	.LC19(%rip), %xmm10
	movss	.LC20(%rip), %xmm6
	movss	.LC21(%rip), %xmm9
.L2:
	mulss	%xmm0, %xmm0
	movss	.LC44(%rip), %xmm4
	movaps	%xmm4, %xmm5
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm3
	addss	%xmm13, %xmm1
	addss	%xmm3, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm12, %xmm1
	addss	%xmm8, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm11, %xmm1
	addss	%xmm7, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm10, %xmm1
	addss	%xmm6, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm1
	addss	%xmm4, %xmm2
	movaps	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movss	.LC0(%rip), %xmm3
	movss	.LC1(%rip), %xmm2
	pxor	%xmm9, %xmm9
	movss	.LC2(%rip), %xmm8
	movss	.LC3(%rip), %xmm7
	movss	.LC4(%rip), %xmm1
	movss	.LC5(%rip), %xmm13
	movss	.LC6(%rip), %xmm12
	movss	.LC7(%rip), %xmm11
	movss	.LC8(%rip), %xmm10
	movss	.LC9(%rip), %xmm6
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	movss	.LC22(%rip), %xmm3
	movss	.LC23(%rip), %xmm2
	movss	.LC24(%rip), %xmm8
	movss	.LC25(%rip), %xmm7
	movss	.LC26(%rip), %xmm1
	movss	.LC27(%rip), %xmm13
	movss	.LC28(%rip), %xmm12
	movss	.LC29(%rip), %xmm11
	movss	.LC30(%rip), %xmm10
	movss	.LC31(%rip), %xmm6
	movss	.LC32(%rip), %xmm9
	jmp	.L2
	.size	ponef, .-ponef
	.p2align 4,,15
	.type	qonef, @function
qonef:
	movd	%xmm0, %eax
	andl	$2147483647, %eax
	cmpl	$1075838975, %eax
	jg	.L9
	movss	.LC56(%rip), %xmm3
	movss	.LC57(%rip), %xmm9
	movss	.LC58(%rip), %xmm2
	movss	.LC59(%rip), %xmm8
	movss	.LC60(%rip), %xmm7
	movss	.LC61(%rip), %xmm1
	movss	.LC62(%rip), %xmm14
	movss	.LC63(%rip), %xmm13
	movss	.LC64(%rip), %xmm12
	movss	.LC65(%rip), %xmm11
	movss	.LC66(%rip), %xmm6
	movss	.LC67(%rip), %xmm10
.L8:
	movaps	%xmm0, %xmm5
	movss	.LC44(%rip), %xmm4
	movaps	%xmm4, %xmm15
	mulss	%xmm0, %xmm5
	divss	%xmm5, %xmm15
	mulss	%xmm15, %xmm3
	mulss	%xmm15, %xmm1
	addss	%xmm9, %xmm3
	addss	%xmm14, %xmm1
	mulss	%xmm15, %xmm3
	mulss	%xmm15, %xmm1
	addss	%xmm3, %xmm2
	addss	%xmm13, %xmm1
	mulss	%xmm15, %xmm2
	mulss	%xmm15, %xmm1
	addss	%xmm8, %xmm2
	addss	%xmm12, %xmm1
	mulss	%xmm15, %xmm2
	mulss	%xmm15, %xmm1
	addss	%xmm7, %xmm2
	addss	%xmm11, %xmm1
	mulss	%xmm15, %xmm2
	mulss	%xmm15, %xmm1
	addss	%xmm6, %xmm2
	addss	%xmm10, %xmm1
	mulss	%xmm15, %xmm2
	addss	%xmm4, %xmm2
	divss	%xmm2, %xmm1
	addss	.LC68(%rip), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movss	.LC45(%rip), %xmm3
	movss	.LC46(%rip), %xmm9
	pxor	%xmm10, %xmm10
	movss	.LC47(%rip), %xmm2
	movss	.LC48(%rip), %xmm8
	movss	.LC49(%rip), %xmm7
	movss	.LC50(%rip), %xmm1
	movss	.LC51(%rip), %xmm14
	movss	.LC52(%rip), %xmm13
	movss	.LC53(%rip), %xmm12
	movss	.LC54(%rip), %xmm11
	movss	.LC55(%rip), %xmm6
	jmp	.L8
	.size	qonef, .-qonef
	.p2align 4,,15
	.globl	__ieee754_j1f
	.type	__ieee754_j1f, @function
__ieee754_j1f:
	pushq	%rbp
	pushq	%rbx
	movd	%xmm0, %ebx
	movaps	%xmm0, %xmm2
	andl	$2147483647, %ebx
	subq	$72, %rsp
	cmpl	$2139095039, %ebx
	jg	.L35
	cmpl	$1073741823, %ebx
	jg	.L36
	movss	.LC72(%rip), %xmm0
	cmpl	$838860799, %ebx
	mulss	%xmm2, %xmm0
	movss	.LC44(%rip), %xmm4
	jle	.L37
.L19:
	movaps	%xmm2, %xmm5
	movss	.LC79(%rip), %xmm1
	movss	.LC75(%rip), %xmm3
	mulss	%xmm2, %xmm5
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm3
	addss	.LC80(%rip), %xmm1
	subss	.LC76(%rip), %xmm3
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm3
	addss	.LC81(%rip), %xmm1
	addss	.LC77(%rip), %xmm3
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm3
	addss	.LC82(%rip), %xmm1
	subss	.LC78(%rip), %xmm3
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm3
	addss	.LC83(%rip), %xmm1
	mulss	%xmm3, %xmm2
	mulss	%xmm5, %xmm1
	addss	%xmm4, %xmm1
	divss	%xmm1, %xmm2
	addss	%xmm2, %xmm0
.L10:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	andps	.LC69(%rip), %xmm2
	movd	%xmm0, %ebp
	leaq	60(%rsp), %rsi
	leaq	56(%rsp), %rdi
	movaps	%xmm2, %xmm0
	movss	%xmm2, 16(%rsp)
	call	__sincosf@PLT
	movss	56(%rsp), %xmm0
	movss	.LC70(%rip), %xmm5
	movaps	%xmm0, %xmm1
	cmpl	$2130706431, %ebx
	movss	60(%rsp), %xmm4
	xorps	%xmm5, %xmm1
	subss	%xmm4, %xmm0
	movss	16(%rsp), %xmm2
	movaps	%xmm1, %xmm3
	movaps	%xmm0, %xmm1
	subss	%xmm4, %xmm3
	jle	.L38
.L14:
	cmpl	$1543503872, %ebx
	sqrtss	%xmm2, %xmm4
	jg	.L39
	movaps	%xmm2, %xmm0
	movss	%xmm1, 28(%rsp)
	movaps	%xmm5, 32(%rsp)
	movss	%xmm3, 24(%rsp)
	movss	%xmm4, (%rsp)
	movss	%xmm2, 20(%rsp)
	call	ponef
	movss	20(%rsp), %xmm2
	movss	%xmm0, 16(%rsp)
	movaps	%xmm2, %xmm0
	call	qonef
	movaps	%xmm0, %xmm2
	movss	28(%rsp), %xmm1
	movss	16(%rsp), %xmm0
	movss	24(%rsp), %xmm3
	mulss	%xmm1, %xmm0
	mulss	%xmm2, %xmm3
	movss	(%rsp), %xmm4
	movaps	32(%rsp), %xmm5
	subss	%xmm3, %xmm0
	mulss	.LC71(%rip), %xmm0
	divss	%xmm4, %xmm0
.L18:
	testl	%ebp, %ebp
	jns	.L10
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	xorps	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movss	.LC71(%rip), %xmm0
	mulss	%xmm1, %xmm0
	divss	%xmm4, %xmm0
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L38:
	movss	%xmm0, 20(%rsp)
	movaps	%xmm2, %xmm0
	movaps	%xmm5, (%rsp)
	addss	%xmm2, %xmm0
	movss	%xmm3, 24(%rsp)
	call	__cosf@PLT
	movss	56(%rsp), %xmm4
	mulss	60(%rsp), %xmm4
	movss	16(%rsp), %xmm2
	movss	20(%rsp), %xmm1
	movaps	(%rsp), %xmm5
	ucomiss	.LC10(%rip), %xmm4
	jbe	.L33
	movss	24(%rsp), %xmm3
	divss	%xmm3, %xmm0
	movaps	%xmm0, %xmm1
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L35:
	movss	.LC44(%rip), %xmm0
	addq	$72, %rsp
	divss	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movss	.LC73(%rip), %xmm1
	addss	%xmm2, %xmm1
	ucomiss	%xmm4, %xmm1
	jbe	.L19
	movaps	%xmm0, %xmm1
	movss	.LC74(%rip), %xmm3
	andps	.LC69(%rip), %xmm1
	ucomiss	%xmm1, %xmm3
	jbe	.L21
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
.L21:
	pxor	%xmm1, %xmm1
	movl	$0, %edx
	ucomiss	%xmm1, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L10
	ucomiss	%xmm1, %xmm2
	movl	$1, %edx
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L10
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L33:
	movaps	%xmm0, %xmm3
	divss	%xmm1, %xmm3
	jmp	.L14
	.size	__ieee754_j1f, .-__ieee754_j1f
	.p2align 4,,15
	.globl	__ieee754_y1f
	.type	__ieee754_y1f, @function
__ieee754_y1f:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %ebx
	subq	$48, %rsp
	cmpl	$2139095039, %ebx
	jg	.L63
	testl	%ebx, %ebx
	je	.L64
	movd	%xmm0, %eax
	testl	%eax, %eax
	js	.L65
	cmpl	$1073741823, %ebx
	movaps	%xmm0, %xmm2
	jle	.L45
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 40(%rsp)
# 0 "" 2
#NO_APP
	movl	40(%rsp), %ebp
	xorl	%r12d, %r12d
	movl	%ebp, %eax
	andb	$-97, %ah
	cmpl	%ebp, %eax
	movl	%eax, 44(%rsp)
	jne	.L66
.L46:
	movaps	%xmm2, %xmm0
	leaq	40(%rsp), %rsi
	leaq	36(%rsp), %rdi
	movss	%xmm2, 12(%rsp)
	call	__sincosf@PLT
	movss	36(%rsp), %xmm4
	cmpl	$2130706431, %ebx
	movaps	%xmm4, %xmm1
	movss	40(%rsp), %xmm3
	xorps	.LC70(%rip), %xmm1
	movss	12(%rsp), %xmm2
	subss	%xmm3, %xmm1
	jle	.L60
	sqrtss	%xmm2, %xmm3
.L47:
	mulss	.LC71(%rip), %xmm1
	testb	%r12b, %r12b
	movaps	%xmm1, %xmm0
	divss	%xmm3, %xmm0
	je	.L40
.L68:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	movl	44(%rsp), %eax
	andl	$24576, %ebp
	andb	$-97, %ah
	orl	%eax, %ebp
	movl	%ebp, 44(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	$855638016, %ebx
	jle	.L67
	movaps	%xmm0, %xmm3
	movss	.LC92(%rip), %xmm1
	movl	%eax, 20(%rsp)
	mulss	%xmm0, %xmm3
	movss	.LC87(%rip), %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm0
	addss	.LC93(%rip), %xmm1
	addss	.LC88(%rip), %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm0
	addss	.LC94(%rip), %xmm1
	subss	.LC89(%rip), %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm0
	addss	.LC95(%rip), %xmm1
	addss	.LC90(%rip), %xmm0
	mulss	%xmm3, %xmm1
	mulss	%xmm3, %xmm0
	addss	.LC96(%rip), %xmm1
	subss	.LC91(%rip), %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC44(%rip), %xmm1
	divss	%xmm1, %xmm0
	mulss	%xmm2, %xmm0
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_j1f@PLT
	movss	20(%rsp), %xmm2
	movss	%xmm0, 16(%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_logf@PLT
	movss	.LC44(%rip), %xmm1
	mulss	16(%rsp), %xmm0
	movss	20(%rsp), %xmm2
	divss	%xmm2, %xmm1
	subss	%xmm1, %xmm0
	mulss	.LC97(%rip), %xmm0
	addss	12(%rsp), %xmm0
.L40:
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movaps	%xmm2, %xmm0
	movss	%xmm3, 20(%rsp)
	movss	%xmm4, 16(%rsp)
	addss	%xmm2, %xmm0
	movss	%xmm2, 12(%rsp)
	movss	%xmm1, 24(%rsp)
	call	__cosf@PLT
	movss	36(%rsp), %xmm5
	mulss	40(%rsp), %xmm5
	movss	12(%rsp), %xmm2
	movss	16(%rsp), %xmm4
	movss	20(%rsp), %xmm3
	ucomiss	.LC10(%rip), %xmm5
	jbe	.L61
	movss	24(%rsp), %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, 28(%rsp)
.L50:
	cmpl	$1543503872, %ebx
	sqrtss	%xmm2, %xmm3
	jg	.L47
	movaps	%xmm2, %xmm0
	movss	%xmm3, 24(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	%xmm2, 16(%rsp)
	call	ponef
	movss	16(%rsp), %xmm2
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	call	qonef
	movss	20(%rsp), %xmm1
	mulss	28(%rsp), %xmm0
	mulss	12(%rsp), %xmm1
	movss	24(%rsp), %xmm3
	testb	%r12b, %r12b
	addss	%xmm1, %xmm0
	mulss	.LC71(%rip), %xmm0
	divss	%xmm3, %xmm0
	je	.L40
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L63:
	movaps	%xmm0, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm0, %xmm2
	movss	.LC44(%rip), %xmm0
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	divss	%xmm2, %xmm0
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movss	.LC84(%rip), %xmm0
	divss	.LC10(%rip), %xmm0
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L65:
	pxor	%xmm1, %xmm1
	mulss	%xmm1, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L61:
	movaps	%xmm4, %xmm7
	movaps	%xmm0, %xmm1
	subss	%xmm3, %xmm7
	movss	%xmm7, 28(%rsp)
	divss	%xmm7, %xmm1
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L67:
	movss	.LC85(%rip), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	andps	.LC69(%rip), %xmm1
	ucomiss	.LC86(%rip), %xmm1
	jbe	.L40
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L66:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 44(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r12d
	jmp	.L46
	.size	__ieee754_y1f, .-__ieee754_y1f
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1190176907
	.align 4
.LC1:
	.long	1203675494
	.align 4
.LC2:
	.long	1192254517
	.align 4
.LC3:
	.long	1164193509
	.align 4
.LC4:
	.long	1173836758
	.align 4
.LC5:
	.long	1165110253
	.align 4
.LC6:
	.long	1137575587
	.align 4
.LC7:
	.long	1096013034
	.align 4
.LC8:
	.long	1039138816
	.align 4
.LC9:
	.long	1122265644
	.align 4
.LC10:
	.long	0
	.align 4
.LC11:
	.long	1153171840
	.align 4
.LC12:
	.long	1173693830
	.align 4
.LC13:
	.long	1168591395
	.align 4
.LC14:
	.long	1148705201
	.align 4
.LC15:
	.long	1141124550
	.align 4
.LC16:
	.long	1140943031
	.align 4
.LC17:
	.long	1121492426
	.align 4
.LC18:
	.long	1088008227
	.align 4
.LC19:
	.long	1039138815
	.align 4
.LC20:
	.long	1114447701
	.align 4
.LC21:
	.long	761803583
	.align 4
.LC22:
	.long	1120899948
	.align 4
.LC23:
	.long	1147057133
	.align 4
.LC24:
	.long	1149426659
	.align 4
.LC25:
	.long	1135108504
	.align 4
.LC26:
	.long	1111637116
	.align 4
.LC27:
	.long	1119231018
	.align 4
.LC28:
	.long	1108113989
	.align 4
.LC29:
	.long	1081849319
	.align 4
.LC30:
	.long	1039138731
	.align 4
.LC31:
	.long	1108027981
	.align 4
.LC32:
	.long	827318541
	.align 4
.LC33:
	.long	1090901392
	.align 4
.LC34:
	.long	1122720727
	.align 4
.LC35:
	.long	1130907335
	.align 4
.LC36:
	.long	1123718297
	.align 4
.LC37:
	.long	1084381773
	.align 4
.LC38:
	.long	1099795777
	.align 4
.LC39:
	.long	1094967740
	.align 4
.LC40:
	.long	1075287488
	.align 4
.LC41:
	.long	1039137302
	.align 4
.LC42:
	.long	1101757932
	.align 4
.LC43:
	.long	870796968
	.align 4
.LC44:
	.long	1065353216
	.align 4
.LC45:
	.long	3364866888
	.align 4
.LC46:
	.long	1227013780
	.align 4
.LC47:
	.long	1227862684
	.align 4
.LC48:
	.long	1208138966
	.align 4
.LC49:
	.long	1173654295
	.align 4
.LC50:
	.long	3342677635
	.align 4
.LC51:
	.long	3325634362
	.align 4
.LC52:
	.long	3292391043
	.align 4
.LC53:
	.long	3246533773
	.align 4
.LC54:
	.long	3184656384
	.align 4
.LC55:
	.long	1126262071
	.align 4
.LC56:
	.long	3231626295
	.align 4
.LC57:
	.long	1125905138
	.align 4
.LC58:
	.long	1144576298
	.align 4
.LC59:
	.long	1144873006
	.align 4
.LC60:
	.long	1132264263
	.align 4
.LC61:
	.long	3249207730
	.align 4
.LC62:
	.long	3257486623
	.align 4
.LC63:
	.long	3248312086
	.align 4
.LC64:
	.long	3224380451
	.align 4
.LC65:
	.long	3184653429
	.align 4
.LC66:
	.long	1106003028
	.align 4
.LC67:
	.long	3024062770
	.align 4
.LC68:
	.long	1052770304
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC69:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.align 16
.LC70:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC71:
	.long	1058041531
	.align 4
.LC72:
	.long	1056964608
	.align 4
.LC73:
	.long	1900671690
	.align 4
.LC74:
	.long	8388608
	.align 4
.LC75:
	.long	861231058
	.align 4
.LC76:
	.long	931540534
	.align 4
.LC77:
	.long	985165053
	.align 4
.LC78:
	.long	1031798784
	.align 4
.LC79:
	.long	760829566
	.align 4
.LC80:
	.long	833446982
	.align 4
.LC81:
	.long	899547074
	.align 4
.LC82:
	.long	960690870
	.align 4
.LC83:
	.long	1016916057
	.align 4
.LC84:
	.long	3212836864
	.align 4
.LC85:
	.long	3206740355
	.align 4
.LC86:
	.long	2139095039
	.align 4
.LC87:
	.long	3016056835
	.align 4
.LC88:
	.long	935680028
	.align 4
.LC89:
	.long	989507370
	.align 4
.LC90:
	.long	1028562492
	.align 4
.LC91:
	.long	1044955953
	.align 4
.LC92:
	.long	764576207
	.align 4
.LC93:
	.long	836106475
	.align 4
.LC94:
	.long	901120724
	.align 4
.LC95:
	.long	961832011
	.align 4
.LC96:
	.long	1017325674
	.align 4
.LC97:
	.long	1059256707
