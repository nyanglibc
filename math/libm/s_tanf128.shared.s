	.text
	.globl	__subtf3
	.p2align 4,,15
	.globl	__tanf128
	.type	__tanf128, @function
__tanf128:
	subq	$56, %rsp
	movabsq	$9223372036854775807, %rdx
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	andq	%rdx, %rax
	movabsq	$4611283733356757713, %rdx
	cmpq	%rdx, %rax
	jle	.L8
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rax
	jle	.L4
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L9
.L5:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	16(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__ieee754_rem_pio2f128@PLT
	addl	%eax, %eax
	movdqa	32(%rsp), %xmm1
	andl	$2, %eax
	movl	$1, %edi
	movdqa	16(%rsp), %xmm0
	subl	%eax, %edi
	call	__kernel_tanf128@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	pxor	%xmm1, %xmm1
	movl	$1, %edi
	call	__kernel_tanf128@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rsp), %rax
	testq	%rax, %rax
	jne	.L5
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L5
	.size	__tanf128, .-__tanf128
	.weak	tanf128
	.set	tanf128,__tanf128
