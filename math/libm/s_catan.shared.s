	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__catan
	.type	__catan, @function
__catan:
	movq	.LC2(%rip), %xmm3
	movapd	%xmm0, %xmm6
	movapd	%xmm1, %xmm8
	movapd	%xmm1, %xmm7
	andpd	%xmm3, %xmm6
	andpd	%xmm3, %xmm8
	ucomisd	%xmm6, %xmm6
	jp	.L107
	movsd	.LC3(%rip), %xmm1
	ucomisd	%xmm1, %xmm6
	jbe	.L108
	ucomisd	%xmm8, %xmm8
	jp	.L53
.L53:
	movq	.LC6(%rip), %xmm1
	andpd	%xmm1, %xmm0
	andpd	%xmm1, %xmm7
	orpd	.LC5(%rip), %xmm0
	movapd	%xmm7, %xmm2
	movapd	%xmm2, %xmm1
.L96:
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movsd	.LC4(%rip), %xmm4
	ucomisd	%xmm4, %xmm6
	jnb	.L9
	pxor	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm0
	jp	.L9
	je	.L109
.L9:
	ucomisd	%xmm8, %xmm8
	jp	.L64
	ucomisd	%xmm1, %xmm8
	ja	.L47
.L48:
	subq	$104, %rsp
	movapd	%xmm0, %xmm2
	movsd	.LC7(%rip), %xmm0
	ucomisd	%xmm0, %xmm6
	jnb	.L15
	ucomisd	%xmm0, %xmm8
	jnb	.L15
	ucomisd	%xmm6, %xmm8
	ja	.L57
	movapd	%xmm8, %xmm11
	movapd	%xmm6, %xmm0
.L23:
	movsd	.LC11(%rip), %xmm1
	movsd	.LC8(%rip), %xmm10
	ucomisd	%xmm11, %xmm1
	jbe	.L84
	movapd	%xmm10, %xmm1
	subsd	%xmm0, %xmm1
	addsd	%xmm10, %xmm0
	mulsd	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L99
	jne	.L99
	pxor	%xmm1, %xmm1
.L99:
	movsd	.LC9(%rip), %xmm9
.L26:
	movapd	%xmm2, %xmm0
	movsd	%xmm7, 88(%rsp)
	movsd	%xmm9, 80(%rsp)
	addsd	%xmm2, %xmm0
	movsd	%xmm10, 72(%rsp)
	movsd	%xmm4, 64(%rsp)
	movaps	%xmm3, 48(%rsp)
	movsd	%xmm6, 32(%rsp)
	movsd	%xmm8, 16(%rsp)
	movsd	%xmm2, 8(%rsp)
	call	__ieee754_atan2@PLT
	movsd	16(%rsp), %xmm8
	movsd	72(%rsp), %xmm10
	movapd	%xmm0, %xmm5
	movsd	80(%rsp), %xmm9
	ucomisd	%xmm10, %xmm8
	mulsd	%xmm9, %xmm5
	movapd	48(%rsp), %xmm3
	movsd	.LC13(%rip), %xmm0
	movsd	8(%rsp), %xmm2
	movsd	32(%rsp), %xmm6
	movsd	64(%rsp), %xmm4
	movsd	88(%rsp), %xmm7
	jp	.L33
	jne	.L33
	ucomisd	%xmm6, %xmm0
	ja	.L110
.L33:
	ucomisd	%xmm0, %xmm6
	jb	.L90
	mulsd	%xmm2, %xmm2
	movapd	%xmm2, %xmm0
.L36:
	movapd	%xmm7, %xmm2
	movapd	%xmm7, %xmm1
	movsd	%xmm4, 32(%rsp)
	addsd	%xmm10, %xmm2
	movsd	%xmm5, 8(%rsp)
	subsd	%xmm10, %xmm1
	movaps	%xmm3, 16(%rsp)
	mulsd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm9
	jbe	.L91
	call	__ieee754_log@PLT
	movsd	.LC10(%rip), %xmm2
	movsd	8(%rsp), %xmm5
	mulsd	%xmm0, %xmm2
	movapd	16(%rsp), %xmm3
	movapd	%xmm5, %xmm0
	movsd	32(%rsp), %xmm4
	movapd	%xmm2, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L15:
	movapd	%xmm2, %xmm5
	movsd	.LC8(%rip), %xmm0
	andpd	.LC6(%rip), %xmm5
	ucomisd	%xmm6, %xmm0
	orpd	.LC5(%rip), %xmm5
	jnb	.L111
	ucomisd	%xmm8, %xmm0
	jb	.L83
	divsd	%xmm2, %xmm7
	movapd	%xmm7, %xmm0
	divsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	movapd	%xmm5, %xmm0
	movapd	%xmm2, %xmm1
.L20:
	movapd	%xmm5, %xmm6
	andpd	%xmm3, %xmm6
	ucomisd	%xmm6, %xmm4
	jbe	.L40
	mulsd	%xmm5, %xmm5
.L40:
	andpd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm4
	jbe	.L1
	mulsd	%xmm2, %xmm2
.L1:
	addq	$104, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	movapd	%xmm0, %xmm2
	movapd	%xmm5, %xmm0
	divsd	%xmm7, %xmm2
	movapd	%xmm2, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L109:
	ucomisd	%xmm8, %xmm8
	jp	.L64
	ucomisd	%xmm1, %xmm8
	ja	.L47
	ucomisd	%xmm4, %xmm8
	jnb	.L48
	ucomisd	%xmm5, %xmm7
	jp	.L48
	je	.L103
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L57:
	movapd	%xmm6, %xmm11
	movapd	%xmm8, %xmm0
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L83:
	movsd	.LC9(%rip), %xmm1
	movsd	%xmm7, 8(%rsp)
	mulsd	%xmm1, %xmm2
	movsd	%xmm4, 48(%rsp)
	mulsd	%xmm7, %xmm1
	movsd	%xmm5, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	movapd	%xmm2, %xmm0
	call	__ieee754_hypot@PLT
	movsd	8(%rsp), %xmm7
	divsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm2
	divsd	%xmm0, %xmm2
	mulsd	.LC10(%rip), %xmm2
.L100:
	movsd	16(%rsp), %xmm5
	movapd	%xmm2, %xmm1
	movapd	32(%rsp), %xmm3
	movapd	%xmm5, %xmm0
	movsd	48(%rsp), %xmm4
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L84:
	ucomisd	%xmm10, %xmm0
	jb	.L86
	movapd	%xmm10, %xmm1
	mulsd	%xmm11, %xmm11
	movsd	.LC9(%rip), %xmm9
	subsd	%xmm0, %xmm1
	addsd	%xmm10, %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm1
	subsd	%xmm11, %xmm1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L107:
	ucomisd	%xmm8, %xmm8
	jp	.L64
	ucomisd	.LC3(%rip), %xmm8
	ja	.L112
	ucomisd	.LC4(%rip), %xmm8
	movsd	.LC0(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jnb	.L96
	ucomisd	.LC1(%rip), %xmm7
	jp	.L64
	jne	.L64
	andpd	.LC6(%rip), %xmm7
.L103:
	movapd	%xmm7, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movsd	.LC0(%rip), %xmm0
	movapd	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	pxor	%xmm0, %xmm0
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L91:
	mulsd	.LC16(%rip), %xmm7
	movapd	%xmm7, %xmm2
	divsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	call	__log1p@PLT
	movsd	.LC10(%rip), %xmm2
	movsd	8(%rsp), %xmm5
	mulsd	%xmm0, %xmm2
	movapd	16(%rsp), %xmm3
	movapd	%xmm5, %xmm0
	movsd	32(%rsp), %xmm4
	movapd	%xmm2, %xmm1
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L86:
	ucomisd	.LC12(%rip), %xmm0
	movsd	.LC9(%rip), %xmm9
	jnb	.L30
	ucomisd	%xmm9, %xmm11
	jnb	.L30
	movapd	%xmm10, %xmm5
	mulsd	%xmm11, %xmm11
	subsd	%xmm0, %xmm5
	addsd	%xmm10, %xmm0
	mulsd	%xmm5, %xmm0
	movapd	%xmm0, %xmm1
	subsd	%xmm11, %xmm1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L110:
	andpd	.LC6(%rip), %xmm7
	movsd	%xmm4, 48(%rsp)
	movapd	%xmm6, %xmm0
	movsd	%xmm5, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	orpd	.LC14(%rip), %xmm7
	movsd	%xmm7, 8(%rsp)
	call	__ieee754_log@PLT
	movsd	.LC15(%rip), %xmm2
	subsd	%xmm0, %xmm2
	mulsd	8(%rsp), %xmm2
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L30:
	movapd	%xmm11, %xmm1
	movsd	%xmm7, 88(%rsp)
	movsd	%xmm2, 80(%rsp)
	movsd	%xmm9, 72(%rsp)
	movsd	%xmm10, 64(%rsp)
	movsd	%xmm4, 48(%rsp)
	movaps	%xmm3, 32(%rsp)
	movsd	%xmm6, 16(%rsp)
	movsd	%xmm8, 8(%rsp)
	call	__x2y2m1@PLT
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm8
	xorpd	.LC6(%rip), %xmm1
	movapd	32(%rsp), %xmm3
	movsd	16(%rsp), %xmm6
	movsd	48(%rsp), %xmm4
	movsd	64(%rsp), %xmm10
	movsd	72(%rsp), %xmm9
	movsd	80(%rsp), %xmm2
	movsd	88(%rsp), %xmm7
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L47:
	movq	.LC6(%rip), %xmm1
	andpd	%xmm1, %xmm0
	orpd	.LC5(%rip), %xmm0
.L50:
	andpd	%xmm1, %xmm7
	movapd	%xmm7, %xmm1
	jmp	.L96
.L112:
	movsd	.LC0(%rip), %xmm0
	movq	.LC6(%rip), %xmm1
	jmp	.L50
	.size	__catan, .-__catan
	.weak	catanf32x
	.set	catanf32x,__catan
	.weak	catanf64
	.set	catanf64,__catan
	.weak	catan
	.set	catan,__catan
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.align 8
.LC1:
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.align 8
.LC4:
	.long	0
	.long	1048576
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	1413754136
	.long	1073291771
	.long	0
	.long	0
	.align 16
.LC6:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1131413504
	.align 8
.LC8:
	.long	0
	.long	1072693248
	.align 8
.LC9:
	.long	0
	.long	1071644672
	.align 8
.LC10:
	.long	0
	.long	1070596096
	.align 8
.LC11:
	.long	0
	.long	1017118720
	.align 8
.LC12:
	.long	0
	.long	1072168960
	.align 8
.LC13:
	.long	0
	.long	963641344
	.section	.rodata.cst16
	.align 16
.LC14:
	.long	0
	.long	1071644672
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC15:
	.long	4277811695
	.long	1072049730
	.align 8
.LC16:
	.long	0
	.long	1074790400
