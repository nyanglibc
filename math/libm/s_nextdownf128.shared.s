	.text
	.p2align 4,,15
	.globl	__nextdownf128
	.type	__nextdownf128, @function
__nextdownf128:
	subq	$8, %rsp
	pxor	.LC0(%rip), %xmm0
	call	__nextupf128@PLT
	pxor	.LC0(%rip), %xmm0
	addq	$8, %rsp
	ret
	.size	__nextdownf128, .-__nextdownf128
	.weak	nextdownf128
	.set	nextdownf128,__nextdownf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
