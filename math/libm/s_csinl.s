	.text
	.p2align 4,,15
	.globl	__csinl
	.type	__csinl, @function
__csinl:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%rbx
	subq	$88, %rsp
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fld	%st(0)
	movl	%eax, %ebx
	andl	$512, %ebx
	fabs
	fldt	32(%rbp)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L2
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	ja	.L3
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L4
	fldz
	fxch	%st(4)
	fucomip	%st(4), %st
	fstp	%st(3)
	jp	.L90
	jne	.L91
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L87
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L38
	fstp	%st(0)
	fstp	%st(0)
	testl	%ebx, %ebx
	fldz
	jne	.L88
.L27:
	fldt	32(%rbp)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	fstp	%st(3)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L92
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L93
.L80:
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L94
	fldz
	fldt	32(%rbp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L95
	jne	.L96
	fld	%st(0)
	fsubp	%st, %st(1)
	fldt	32(%rbp)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	fstp	%st(3)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L97
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L98
.L38:
	fldln2
	fnstcw	-50(%rbp)
	movzwl	-50(%rbp), %eax
	fmuls	.LC7(%rip)
	orb	$12, %ah
	movw	%ax, -52(%rbp)
	fldcw	-52(%rbp)
	fistpl	-80(%rbp)
	fldcw	-50(%rbp)
	fldt	.LC5(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jbe	.L75
	fstpt	-96(%rbp)
	subq	$16, %rsp
	leaq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	popq	%r9
	popq	%r10
	fldt	-96(%rbp)
.L11:
	testl	%ebx, %ebx
	je	.L12
	fldt	-48(%rbp)
	fchs
	fstpt	-48(%rbp)
.L12:
	fildl	-80(%rbp)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstpt	-80(%rbp)
	jbe	.L76
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	-96(%rbp)
	call	__ieee754_expl@PLT
	fldt	32(%rbp)
	popq	%rdi
	popq	%r8
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fldt	-96(%rbp)
	testb	$2, %ah
	fldt	-80(%rbp)
	jne	.L15
	fldt	-32(%rbp)
	fxch	%st(1)
.L16:
	fsub	%st(2), %st
	fldt	-48(%rbp)
	fld	%st(4)
	fmuls	.LC9(%rip)
	fmul	%st, %st(1)
	fxch	%st(1)
	fld	%st(0)
	fstpt	-48(%rbp)
	fxch	%st(1)
	fmulp	%st, %st(3)
	fxch	%st(2)
	fld	%st(0)
	fstpt	-32(%rbp)
	fxch	%st(1)
	fucomi	%st(3), %st
	ja	.L89
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L101:
	fstp	%st(0)
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L77:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-48(%rbp)
	popq	%rcx
	popq	%rsi
	fmul	%st(1), %st
	fldt	-32(%rbp)
	fmulp	%st, %st(2)
	fld	%st(0)
	fld	%st(2)
	.p2align 4,,10
	.p2align 3
.L21:
	fldt	.LC5(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L99
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L99:
	fstp	%st(2)
	fxch	%st(1)
.L22:
	fldt	.LC5(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L100
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L100:
	fstp	%st(2)
.L1:
	movq	-8(%rbp), %rbx
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	fstp	%st(0)
	pushq	40(%rbp)
	pushq	32(%rbp)
	call	__ieee754_coshl@PLT
	fldt	-48(%rbp)
	pushq	40(%rbp)
	pushq	32(%rbp)
	fmulp	%st, %st(1)
	fstpt	-80(%rbp)
	call	__ieee754_sinhl@PLT
	fldt	-32(%rbp)
	addq	$32, %rsp
	fmulp	%st, %st(1)
	fldt	-80(%rbp)
	fld	%st(0)
	fld	%st(2)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L89:
	fsub	%st(3), %st
	fxch	%st(2)
	fmul	%st(4), %st
	fld	%st(0)
	fstpt	-48(%rbp)
	fxch	%st(1)
	fmulp	%st, %st(4)
	fxch	%st(3)
	fld	%st(0)
	fstpt	-32(%rbp)
	fxch	%st(1)
	fucomi	%st(2), %st
	fstp	%st(2)
	jbe	.L101
	fstp	%st(1)
	fldt	.LC4(%rip)
	fmul	%st, %st(2)
	fmulp	%st, %st(1)
	fld	%st(1)
	fld	%st(1)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L15:
	fldt	-32(%rbp)
	fchs
	fxch	%st(1)
	jmp	.L16
.L90:
	fstp	%st(0)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L91:
	fstp	%st(0)
.L7:
	fucomi	%st(0), %st
	jp	.L102
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L38
	fstp	%st(0)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L98:
	fstp	%st(0)
.L37:
	fldt	.LC5(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L78
	subq	$16, %rsp
	leaq	-32(%rbp), %rsi
	leaq	-48(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	fldt	-48(%rbp)
	fldt	-32(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	flds	.LC2(%rip)
	testb	$2, %ah
	fld	%st(0)
	je	.L30
	fstp	%st(0)
	flds	.LC10(%rip)
.L30:
	fxch	%st(2)
	popq	%rax
	popq	%rdx
.L28:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L31
	fstp	%st(0)
	flds	.LC10(%rip)
.L31:
	testl	%ebx, %ebx
	je	.L85
	fchs
.L85:
	fldt	32(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L1
	fxch	%st(1)
	fchs
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(2)
	fucomi	%st(0), %st
	jp	.L103
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L80
	fstp	%st(0)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L93:
	fstp	%st(0)
.L39:
	fld	%st(0)
	fsubp	%st, %st(1)
	flds	.LC2(%rip)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L75:
	fxch	%st(1)
	fstpt	-48(%rbp)
	fld1
	fstpt	-32(%rbp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L94:
	fstp	%st(0)
	jmp	.L44
.L95:
	fstp	%st(0)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L96:
	fstp	%st(0)
.L44:
	movl	$1, %edi
	call	feraiseexcept@PLT
	flds	.LC3(%rip)
	fld	%st(0)
	jmp	.L1
.L88:
	fstp	%st(0)
	fldz
	fchs
	jmp	.L27
.L78:
	flds	.LC2(%rip)
	fld	%st(0)
	fxch	%st(2)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L87:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	testl	%ebx, %ebx
	je	.L50
	fldz
	fchs
	flds	.LC3(%rip)
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L50:
	flds	.LC3(%rip)
	fldz
	jmp	.L1
.L92:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L54
.L97:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L54
.L102:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L54
.L103:
	fstp	%st(0)
	fstp	%st(0)
.L54:
	flds	.LC3(%rip)
	fld	%st(0)
	jmp	.L1
	.size	__csinl, .-__csinl
	.weak	csinf64x
	.set	csinf64x,__csinl
	.weak	csinl
	.set	csinl,__csinl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC5:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1182792704
	.align 4
.LC9:
	.long	1056964608
	.align 4
.LC10:
	.long	4286578688
