	.text
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__multf3
	.p2align 4,,15
	.globl	__nextafterf128
	.type	__nextafterf128, @function
__nextafterf128:
	pushq	%r14
	pushq	%r13
	movabsq	$9223372036854775807, %rax
	pushq	%r12
	pushq	%rbp
	movabsq	$9223090561878065151, %rdx
	pushq	%rbx
	subq	$32, %rsp
	movaps	%xmm0, 16(%rsp)
	movq	24(%rsp), %rbx
	movq	16(%rsp), %r12
	movaps	%xmm1, (%rsp)
	movq	%rbx, %rbp
	andq	%rax, %rbp
	movq	8(%rsp), %r13
	movq	(%rsp), %r14
	andq	%r13, %rax
	cmpq	%rdx, %rbp
	jle	.L2
	movabsq	$-9223090561878065152, %rdx
	addq	%rbp, %rdx
	orq	%r12, %rdx
	jne	.L3
.L2:
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rax
	jle	.L4
	movabsq	$-9223090561878065152, %rdx
	addq	%rdx, %rax
	orq	%r14, %rax
	jne	.L3
.L4:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm2
	je	.L1
	orq	%r12, %rbp
	je	.L33
	testq	%rbx, %rbx
	js	.L7
	cmpq	%r13, %rbx
	jg	.L13
	jne	.L16
.L31:
	cmpq	%r14, %r12
	jbe	.L16
.L14:
	subq	$1, %r12
.L12:
	movabsq	$9223090561878065152, %rax
	movq	%rbx, %rdx
	andq	%rax, %rdx
	cmpq	%rax, %rdx
	je	.L34
	testq	%rdx, %rdx
	jne	.L18
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L18:
	movq	%rbx, 8(%rsp)
	movq	%r12, (%rsp)
	movdqa	(%rsp), %xmm2
.L1:
	addq	$32, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movabsq	$-9223372036854775808, %rax
	movq	$1, (%rsp)
	andq	%rax, %r13
	movq	%r13, 8(%rsp)
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 16(%rsp)
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%r13, %r13
	js	.L35
	.p2align 4,,10
	.p2align 3
.L13:
	cmpq	$1, %r12
	sbbq	$0, %rbx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L34:
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%r13, %rbx
	jg	.L13
	cmpq	%r13, %rbx
	je	.L31
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$1, %r12
	cmpq	$1, %r12
	adcq	$0, %rbx
	jmp	.L12
	.size	__nextafterf128, .-__nextafterf128
	.weak	nexttowardf128_do_not_use
	.set	nexttowardf128_do_not_use,__nextafterf128
	.globl	__nexttowardf128_do_not_use
	.set	__nexttowardf128_do_not_use,__nextafterf128
	.weak	nextafterf128
	.set	nextafterf128,__nextafterf128
