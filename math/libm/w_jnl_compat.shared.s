	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__jnl
	.type	__jnl, @function
__jnl:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$32, %rsp
	fldt	48(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__ieee754_jnl@PLT
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	popq	%rdx
	popq	%rcx
	movl	(%rax), %eax
	cmpl	$-1, %eax
	sete	%dl
	cmpl	$2, %eax
	sete	%al
	orb	%al, %dl
	jne	.L1
	fldt	(%rsp)
	fucomi	%st(0), %st
	jp	.L8
	fld	%st(0)
	fabs
	fldl	.LC0(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L7
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	fstp	%st(0)
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fstp	%st(1)
	subq	$32, %rsp
	movl	$238, %edi
	fstpt	16(%rsp)
	movl	%ebx, 32(%rsp)
	fildl	32(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__jnl, .-__jnl
	.weak	jnf64x
	.set	jnf64x,__jnl
	.weak	jnl
	.set	jnl,__jnl
	.p2align 4,,15
	.globl	__ynl
	.type	__ynl, @function
__ynl:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$32, %rsp
	fldt	48(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__ieee754_ynl@PLT
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	popq	%rdx
	popq	%rcx
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L9
	fldt	(%rsp)
	fucomi	%st(0), %st
	jp	.L27
	fldz
	fucomi	%st(1), %st
	jnb	.L25
	fstp	%st(0)
	cmpl	$2, %eax
	je	.L28
	fldl	.LC0(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L26
	fstp	%st(0)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L27:
	fstp	%st(0)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L28:
	fstp	%st(0)
.L9:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	fstp	%st(2)
	fxch	%st(1)
	movl	%ebx, (%rsp)
	fildl	(%rsp)
	fxch	%st(2)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L13
	jne	.L13
	subq	$32, %rsp
	movl	$212, %edi
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	fstp	%st(1)
	subq	$32, %rsp
	movl	$239, %edi
	fstpt	16(%rsp)
	movl	%ebx, 32(%rsp)
	fildl	32(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	subq	$32, %rsp
	movl	$213, %edi
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__ynl, .-__ynl
	.weak	ynf64x
	.set	ynf64x,__ynl
	.weak	ynl
	.set	ynl,__ynl
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1413754136
	.long	1128866299
