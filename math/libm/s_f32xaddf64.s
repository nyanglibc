	.text
	.p2align 4,,15
	.globl	__f32xaddf64
	.type	__f32xaddf64, @function
__f32xaddf64:
	movapd	%xmm0, %xmm2
	movq	.LC0(%rip), %xmm4
	addsd	%xmm1, %xmm2
	movsd	.LC1(%rip), %xmm3
	movapd	%xmm2, %xmm5
	andpd	%xmm4, %xmm5
	ucomisd	%xmm5, %xmm3
	jnb	.L10
	ucomisd	%xmm2, %xmm2
	jp	.L13
	andpd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm3
	jb	.L1
.L7:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L1:
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	ucomisd	.LC2(%rip), %xmm2
	jp	.L1
	jne	.L1
	xorpd	.LC3(%rip), %xmm1
	ucomisd	%xmm0, %xmm1
	jp	.L7
	je	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	ucomisd	%xmm1, %xmm0
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__f32xaddf64, .-__f32xaddf64
	.weak	f32xaddf64
	.set	f32xaddf64,__f32xaddf64
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	0
	.section	.rodata.cst16
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
