	.text
	.p2align 4,,15
	.globl	__casinf
	.type	__casinf, @function
__casinf:
	subq	$56, %rsp
	movq	%xmm0, 40(%rsp)
	movss	40(%rsp), %xmm1
	ucomiss	%xmm1, %xmm1
	movss	44(%rsp), %xmm0
	jp	.L2
	ucomiss	%xmm0, %xmm0
	jp	.L2
	movss	.LC5(%rip), %xmm2
	movss	%xmm1, 36(%rsp)
	xorps	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	movss	%xmm0, 32(%rsp)
	movq	32(%rsp), %xmm0
	call	__casinhf@PLT
	movq	%xmm0, 24(%rsp)
	movaps	(%rsp), %xmm2
	movss	24(%rsp), %xmm0
	xorps	%xmm2, %xmm0
	movss	28(%rsp), %xmm1
.L1:
	movss	%xmm1, 16(%rsp)
	movss	%xmm0, 20(%rsp)
	movq	16(%rsp), %xmm0
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	.LC1(%rip), %xmm1
	jp	.L10
	je	.L1
.L10:
	movss	.LC2(%rip), %xmm2
	andps	%xmm2, %xmm1
	ucomiss	.LC3(%rip), %xmm1
	movss	.LC0(%rip), %xmm1
	ja	.L6
	andps	%xmm0, %xmm2
	ucomiss	.LC3(%rip), %xmm2
	jbe	.L9
.L6:
	andps	.LC5(%rip), %xmm0
	orps	.LC4(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movaps	%xmm1, %xmm0
	jmp	.L1
	.size	__casinf, .-__casinf
	.weak	casinf32
	.set	casinf32,__casinf
	.weak	casinf
	.set	casinf,__casinf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.align 4
.LC1:
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	2139095040
	.long	0
	.long	0
	.long	0
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
