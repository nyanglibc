	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__nanl
	.type	__nanl, @function
__nanl:
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__strtold_nan@PLT
	.size	__nanl, .-__nanl
	.weak	nanf64x
	.set	nanf64x,__nanl
	.weak	nanl
	.set	nanl,__nanl
