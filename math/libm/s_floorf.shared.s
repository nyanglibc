	.text
	.p2align 4,,15
	.globl	__floorf
	.type	__floorf, @function
__floorf:
	movd	%xmm0, %ecx
	movd	%xmm0, %eax
	sarl	$23, %ecx
	movzbl	%cl, %ecx
	subl	$127, %ecx
	cmpl	$22, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L14
	movl	$8388607, %edx
	sarl	%cl, %edx
	testl	%edx, %eax
	je	.L5
	testl	%eax, %eax
	jns	.L6
	movl	$8388608, %esi
	sarl	%cl, %esi
	addl	%esi, %eax
.L6:
	notl	%edx
	andl	%eax, %edx
	movl	%edx, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$-128, %ecx
	je	.L15
.L5:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%eax, %eax
	pxor	%xmm0, %xmm0
	jns	.L5
	testl	$2147483647, %eax
	movss	.LC0(%rip), %xmm0
	je	.L5
	movss	.LC1(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	addss	%xmm0, %xmm0
	ret
	.size	__floorf, .-__floorf
	.weak	floorf32
	.set	floorf32,__floorf
	.weak	floorf
	.set	floorf,__floorf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2147483648
	.align 4
.LC1:
	.long	3212836864
