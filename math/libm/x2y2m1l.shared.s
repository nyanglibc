	.text
	.p2align 4,,15
	.type	compare, @function
compare:
	fldt	(%rdi)
	fabs
	fldt	(%rsi)
	fabs
	fucomi	%st(1), %st
	ja	.L3
	xorl	%eax, %eax
	movl	$1, %edx
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%al
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	fstp	%st(0)
	fstp	%st(0)
	movl	$-1, %eax
	ret
	.size	compare, .-compare
	.p2align 4,,15
	.globl	__x2y2m1l
	.type	__x2y2m1l, @function
__x2y2m1l:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$144, %rsp
	fldt	176(%rsp)
	fldt	192(%rsp)
#APP
# 383 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstcw 14(%rsp)
# 0 "" 2
#NO_APP
	movzwl	14(%rsp), %edx
	movl	%edx, %eax
	movw	%dx, 16(%rsp)
	andb	$-16, %ah
	orb	$3, %ah
	cmpw	%ax, %dx
	movw	%ax, 64(%rsp)
	jne	.L12
	movb	$0, 48(%rsp)
.L7:
	fld	%st(1)
	leaq	64(%rsp), %rbx
	leaq	compare(%rip), %rcx
	leaq	compare(%rip), %r12
	movl	$16, %edx
	movl	$5, %esi
	fmul	%st(2), %st
	movq	%rbx, %rdi
	movl	$4, %ebp
	addq	$16, %rbx
	fld	%st(0)
	fstpt	80(%rsp)
	fldl	.LC1(%rip)
	fld	%st(3)
	fmul	%st(1), %st
	fld	%st(4)
	fsub	%st(1), %st
	faddp	%st, %st(1)
	fsubr	%st, %st(4)
	fld	%st(0)
	fmul	%st(5), %st
	fxch	%st(1)
	fmul	%st(0), %st
	fsubp	%st, %st(3)
	fadd	%st, %st(2)
	faddp	%st, %st(2)
	fxch	%st(3)
	fmul	%st(0), %st
	faddp	%st, %st(1)
	fstpt	64(%rsp)
	fld	%st(0)
	fmul	%st(1), %st
	fld	%st(0)
	fstpt	112(%rsp)
	fxch	%st(2)
	fmul	%st(1), %st
	fld	%st(1)
	fsub	%st(1), %st
	faddp	%st, %st(1)
	fsubr	%st, %st(1)
	fld	%st(0)
	fmul	%st(2), %st
	fxch	%st(1)
	fmul	%st(0), %st
	fsubp	%st, %st(3)
	fadd	%st, %st(2)
	faddp	%st, %st(2)
	fmul	%st(0), %st
	faddp	%st, %st(1)
	fstpt	96(%rsp)
	fld1
	fchs
	fstpt	128(%rsp)
	call	qsort@PLT
.L8:
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movl	$16, %edx
	addq	$16, %rbx
	fldt	-32(%rbx)
	fldt	-16(%rbx)
	fld	%st(1)
	fadd	%st(1), %st
	fld	%st(0)
	fstpt	-16(%rbx)
	fsubrp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	-32(%rbx)
	call	qsort@PLT
	subq	$1, %rbp
	jne	.L8
	fldt	128(%rsp)
	cmpb	$0, 48(%rsp)
	fldt	112(%rsp)
	faddp	%st, %st(1)
	fldt	96(%rsp)
	faddp	%st, %st(1)
	fldt	80(%rsp)
	faddp	%st, %st(1)
	fldt	64(%rsp)
	faddp	%st, %st(1)
	jne	.L13
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
#APP
# 391 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 64(%rsp)
# 0 "" 2
#NO_APP
	movb	$1, 48(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 16(%rsp)
# 0 "" 2
#NO_APP
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__x2y2m1l, .-__x2y2m1l
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1048576
	.long	1106247680
