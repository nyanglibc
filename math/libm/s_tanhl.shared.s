	.text
	.p2align 4,,15
	.globl	__tanhl
	.type	__tanhl, @function
__tanhl:
	pushq	%rbx
	movq	24(%rsp), %rbx
	movl	%ebx, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L23
	movq	16(%rsp), %rcx
	movswl	%ax, %esi
	movq	%rcx, %rdx
	shrq	$32, %rdx
	cmpw	$16386, %ax
	jle	.L5
	cmpl	$16387, %esi
	jne	.L8
	cmpl	$-1207959553, %edx
	ja	.L8
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L26:
	fstp	%st(0)
.L6:
	fldt	16(%rsp)
	cmpw	$16327, %ax
	fabs
	jle	.L24
	cmpl	$16382, %esi
	jbe	.L12
	fadd	%st(0), %st
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__GI___expm1l
	flds	.LC3(%rip)
	popq	%rcx
	fadd	%st, %st(1)
	popq	%rsi
	fdivp	%st, %st(1)
	fld1
	fsubp	%st, %st(1)
.L13:
	testw	%bx, %bx
	jns	.L1
	fchs
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	orl	%ecx, %edx
	orl	%esi, %edx
	fldt	16(%rsp)
	jne	.L26
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	fld1
	testw	%bx, %bx
	fldt	16(%rsp)
	fdivr	%st(1), %st
	js	.L25
	faddp	%st, %st(1)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	fldt	.LC1(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L10
	fldt	16(%rsp)
	fmul	%st(0), %st
	fstp	%st(0)
.L10:
	fld1
	fldt	.LC2(%rip)
	faddp	%st, %st(1)
	fldt	16(%rsp)
	popq	%rbx
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	fld1
	fldt	.LC2(%rip)
	fsubrp	%st, %st(1)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L25:
	fsubp	%st, %st(1)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	fmuls	.LC4(%rip)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__GI___expm1l
	fld	%st(0)
	popq	%rax
	fchs
	fxch	%st(1)
	popq	%rdx
	fadds	.LC3(%rip)
	fdivrp	%st, %st(1)
	jmp	.L13
	.size	__tanhl, .-__tanhl
	.weak	tanhf64x
	.set	tanhf64x,__tanhl
	.weak	tanhl
	.set	tanhl,__tanhl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC2:
	.long	3136694642
	.long	3149193046
	.long	105
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC3:
	.long	1073741824
	.align 4
.LC4:
	.long	3221225472
