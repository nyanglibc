	.text
	.p2align 4,,15
	.globl	__feupdateenv
	.type	__feupdateenv, @function
__feupdateenv:
	subq	$24, %rsp
#APP
# 29 "../sysdeps/x86_64/fpu/feupdateenv.c" 1
	fnstsw 10(%rsp)
	stmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	movl	12(%rsp), %eax
	orw	10(%rsp), %ax
	andl	$61, %eax
	movw	%ax, 10(%rsp)
	call	__fesetenv@PLT
	movzwl	10(%rsp), %edi
	call	__feraiseexcept@PLT
	xorl	%eax, %eax
	addq	$24, %rsp
	ret
	.size	__feupdateenv, .-__feupdateenv
	.weak	feupdateenv
	.set	feupdateenv,__feupdateenv
