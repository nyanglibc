	.text
	.p2align 4,,15
	.globl	__atanh
	.type	__atanh, @function
__atanh:
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	.LC1(%rip), %xmm1
	jnb	.L7
.L2:
	jmp	__ieee754_atanh@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__atanh, .-__atanh
	.weak	atanhf32x
	.set	atanhf32x,__atanh
	.weak	atanhf64
	.set	atanhf64,__atanh
	.weak	atanh
	.set	atanh,__atanh
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
