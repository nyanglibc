	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__casinl
	.type	__casinl, @function
__casinl:
	subq	$8, %rsp
	fldt	32(%rsp)
	fldt	16(%rsp)
	fucomi	%st(0), %st
	jp	.L18
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L2
	fchs
	fxch	%st(1)
	subq	$32, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__casinhl@PLT
	addq	$32, %rsp
	fchs
.L1:
	addq	$8, %rsp
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	fxch	%st(1)
.L2:
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L19
	je	.L8
	fxch	%st(1)
	jmp	.L10
.L19:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L10:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andb	$69, %ah
	cmpb	$5, %ah
	fxam
	fnstsw	%ax
	fstp	%st(0)
	je	.L6
	movl	%eax, %edx
	andb	$69, %dh
	cmpb	$5, %dh
	jne	.L9
.L6:
	testb	$2, %ah
	flds	.LC2(%rip)
	je	.L7
	fstp	%st(0)
	flds	.LC3(%rip)
.L7:
	flds	.LC0(%rip)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$8, %rsp
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	flds	.LC0(%rip)
	fld	%st(0)
	jmp	.L1
	.size	__casinl, .-__casinl
	.weak	casinf64x
	.set	casinf64x,__casinl
	.weak	casinl
	.set	casinl,__casinl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.align 4
.LC2:
	.long	2139095040
	.align 4
.LC3:
	.long	4286578688
