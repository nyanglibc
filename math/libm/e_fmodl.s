.globl __ieee754_fmodl
.type __ieee754_fmodl,@function
.align 1<<4
__ieee754_fmodl:
 fldt 24(%rsp)
 fldt 8(%rsp)
1: fprem
 fstsw %ax
 and $04,%ah
 jnz 1b
 fstp %st(1)
 ret
.size __ieee754_fmodl,.-__ieee754_fmodl
