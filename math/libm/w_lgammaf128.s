	.text
	.globl	__unordtf2
	.globl	__gttf2
	.p2align 4,,15
	.globl	__lgammaf128
	.type	__lgammaf128, @function
__lgammaf128:
	leaq	__signgam(%rip), %rdi
	subq	$56, %rsp
	movaps	%xmm0, 32(%rsp)
	call	__ieee754_lgammaf128_r@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L4
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L4
.L1:
	movdqa	(%rsp), %xmm0
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	32(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__lgammaf128, .-__lgammaf128
	.weak	lgammaf128
	.set	lgammaf128,__lgammaf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
