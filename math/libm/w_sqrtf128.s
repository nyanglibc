	.text
	.globl	__unordtf2
	.globl	__getf2
	.p2align 4,,15
	.globl	__sqrtf128
	.type	__sqrtf128, @function
__sqrtf128:
	pxor	%xmm1, %xmm1
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L5
.L2:
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	jmp	__ieee754_sqrtf128@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	movq	errno@gottpoff(%rip), %rax
	movdqa	(%rsp), %xmm0
	movl	$33, %fs:(%rax)
	addq	$24, %rsp
	jmp	__ieee754_sqrtf128@PLT
	.size	__sqrtf128, .-__sqrtf128
	.weak	sqrtf128
	.set	sqrtf128,__sqrtf128
