	.text
	.p2align 4,,15
	.type	with_errnof, @function
with_errnof:
	movq	errno@gottpoff(%rip), %rax
	movl	%edi, %fs:(%rax)
	ret
	.size	with_errnof, .-with_errnof
	.p2align 4,,15
	.type	xflowf, @function
xflowf:
	testl	%edi, %edi
	movaps	%xmm0, %xmm1
	je	.L4
	xorps	.LC0(%rip), %xmm1
.L4:
	movl	$34, %edi
	mulss	%xmm1, %xmm0
	jmp	with_errnof
	.size	xflowf, .-xflowf
	.p2align 4,,15
	.globl	__math_uflowf
	.type	__math_uflowf, @function
__math_uflowf:
	movss	.LC1(%rip), %xmm0
	jmp	xflowf
	.size	__math_uflowf, .-__math_uflowf
	.p2align 4,,15
	.globl	__math_may_uflowf
	.type	__math_may_uflowf, @function
__math_may_uflowf:
	movss	.LC2(%rip), %xmm0
	jmp	xflowf
	.size	__math_may_uflowf, .-__math_may_uflowf
	.p2align 4,,15
	.globl	__math_oflowf
	.type	__math_oflowf, @function
__math_oflowf:
	movss	.LC3(%rip), %xmm0
	jmp	xflowf
	.size	__math_oflowf, .-__math_oflowf
	.p2align 4,,15
	.globl	__math_divzerof
	.type	__math_divzerof, @function
__math_divzerof:
	testl	%edi, %edi
	movss	.LC4(%rip), %xmm0
	jne	.L10
	movss	.LC5(%rip), %xmm0
.L10:
	divss	.LC6(%rip), %xmm0
	movl	$34, %edi
	jmp	with_errnof
	.size	__math_divzerof, .-__math_divzerof
	.p2align 4,,15
	.globl	__math_invalidf
	.type	__math_invalidf, @function
__math_invalidf:
	movaps	%xmm0, %xmm1
	subss	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm1
	divss	%xmm0, %xmm0
	jp	.L12
	movl	$33, %edi
	jmp	with_errnof
	.p2align 4,,10
	.p2align 3
.L12:
	rep ret
	.size	__math_invalidf, .-__math_invalidf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	268435456
	.align 4
.LC2:
	.long	438304768
	.align 4
.LC3:
	.long	1879048192
	.align 4
.LC4:
	.long	3212836864
	.align 4
.LC5:
	.long	1065353216
	.align 4
.LC6:
	.long	0
