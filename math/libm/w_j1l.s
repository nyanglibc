	.text
	.p2align 4,,15
	.globl	__j1l
	.type	__j1l, @function
__j1l:
	jmp	__ieee754_j1l@PLT
	.size	__j1l, .-__j1l
	.weak	j1f64x
	.set	j1f64x,__j1l
	.weak	j1l
	.set	j1l,__j1l
	.p2align 4,,15
	.globl	__y1l
	.type	__y1l, @function
__y1l:
	fldt	8(%rsp)
	fldz
	fucomi	%st(1), %st
	jnb	.L11
	fstp	%st(0)
.L4:
	fstpt	8(%rsp)
	jmp	__ieee754_y1l@PLT
	.p2align 4,,10
	.p2align 3
.L11:
	ja	.L12
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L4
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y1l, .-__y1l
	.weak	y1f64x
	.set	y1f64x,__y1l
	.weak	y1l
	.set	y1l,__y1l
