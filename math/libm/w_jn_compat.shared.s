	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__jn
	.type	__jn, @function
__jn:
	movapd	%xmm0, %xmm1
	andpd	.LC0(%rip), %xmm1
	ucomisd	.LC1(%rip), %xmm1
	ja	.L10
.L2:
	jmp	__ieee754_jn@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	pxor	%xmm2, %xmm2
	movapd	%xmm0, %xmm1
	cvtsi2sd	%edi, %xmm2
	movl	$38, %edi
	movapd	%xmm2, %xmm0
	jmp	__kernel_standard@PLT
	.size	__jn, .-__jn
	.weak	jnf32x
	.set	jnf32x,__jn
	.weak	jnf64
	.set	jnf64,__jn
	.weak	jn
	.set	jn,__jn
	.p2align 4,,15
	.globl	__yn
	.type	__yn, @function
__yn:
	pxor	%xmm2, %xmm2
	ucomisd	%xmm0, %xmm2
	jnb	.L18
	ucomisd	.LC1(%rip), %xmm0
	ja	.L18
.L32:
	jmp	__ieee754_yn@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L32
	pushq	%rbx
	movapd	%xmm0, %xmm1
	movl	%edi, %ebx
	subq	$16, %rsp
	ucomisd	%xmm0, %xmm2
	ja	.L35
	ucomisd	%xmm2, %xmm0
	jp	.L16
	je	.L36
.L16:
	cmpl	$2, %eax
	jne	.L37
	addq	$16, %rsp
	popq	%rbx
	jmp	__ieee754_yn@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$4, %edi
	movsd	%xmm0, 8(%rsp)
	call	__GI___feraiseexcept
	pxor	%xmm0, %xmm0
	movl	$12, %edi
	movsd	8(%rsp), %xmm1
	addq	$16, %rsp
	cvtsi2sd	%ebx, %xmm0
	popq	%rbx
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	pxor	%xmm0, %xmm0
	addq	$16, %rsp
	movl	$39, %edi
	cvtsi2sd	%ebx, %xmm0
	popq	%rbx
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$1, %edi
	movsd	%xmm0, 8(%rsp)
	call	__GI___feraiseexcept
	pxor	%xmm0, %xmm0
	movl	$13, %edi
	movsd	8(%rsp), %xmm1
	addq	$16, %rsp
	cvtsi2sd	%ebx, %xmm0
	popq	%rbx
	jmp	__kernel_standard@PLT
	.size	__yn, .-__yn
	.weak	ynf32x
	.set	ynf32x,__yn
	.weak	ynf64
	.set	ynf64,__yn
	.weak	yn
	.set	yn,__yn
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	1413754136
	.long	1128866299
