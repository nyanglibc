	.text
#APP
	.symver __ieee754_powf128,__powf128_finite@GLIBC_2.26
	.globl	__eqtf2
	.globl	__addtf3
	.globl	__multf3
	.globl	__subtf3
	.globl	__divtf3
	.globl	__gttf2
	.globl	__lttf2
	.globl	__floatsitf
	.globl	__getf2
	.globl	__fixtfsi
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_powf128
	.type	__ieee754_powf128, @function
__ieee754_powf128:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$184, %rsp
	movaps	%xmm1, (%rsp)
	movq	8(%rsp), %rax
	movq	(%rsp), %rbx
	movaps	%xmm0, 16(%rsp)
	movq	%rax, %r12
	movq	%rbx, %r13
	shrq	$32, %r12
	shrq	$32, %r13
	movl	%r12d, %ebp
	orl	%r13d, %eax
	andl	$2147483647, %ebp
	orl	%eax, %ebx
	movl	%ebp, %eax
	orl	%ebx, %eax
	jne	.L2
	call	__GI___issignalingf128
	testl	%eax, %eax
	je	.L111
	movdqa	.LC0(%rip), %xmm6
	xorl	%r15d, %r15d
	movdqa	%xmm6, %xmm1
	movdqa	16(%rsp), %xmm0
	movaps	%xmm6, 48(%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L9
.L54:
	movdqa	(%rsp), %xmm0
	call	__GI___issignalingf128
	testl	%eax, %eax
	je	.L112
	cmpl	$2147418112, %ebp
	sete	%r15b
.L9:
	movq	24(%rsp), %rax
	movq	%rax, %r14
	shrq	$32, %r14
	movl	%r14d, %ecx
	andl	$2147483647, %ecx
	cmpl	$2147418112, %ecx
	ja	.L10
	je	.L113
.L11:
	cmpl	$2147418112, %ebp
	ja	.L10
	testb	%r15b, %r15b
	jne	.L114
	testl	%r14d, %r14d
	js	.L115
	xorl	%edx, %edx
	testl	%ebx, %ebx
	je	.L19
.L18:
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rax
	movdqa	16(%rsp), %xmm2
	pand	.LC10(%rip), %xmm2
	movq	%rsi, %rdi
	shrq	$32, %rdi
	orl	%edi, %eax
	orl	%esi, %eax
	movaps	%xmm2, 32(%rsp)
	jne	.L24
	movl	%r14d, %eax
	andl	$1073741823, %eax
	cmpl	$1073676288, %eax
	je	.L78
	testl	%ecx, %ecx
	jne	.L24
.L78:
	testl	%r12d, %r12d
	jns	.L26
	movdqa	32(%rsp), %xmm1
	movl	%ecx, 16(%rsp)
	movl	%edx, (%rsp)
	movdqa	.LC0(%rip), %xmm0
	call	__divtf3@PLT
	movl	16(%rsp), %ecx
	movl	(%rsp), %edx
	movaps	%xmm0, 32(%rsp)
.L26:
	testl	%r14d, %r14d
	jns	.L1
	subl	$1073676288, %ecx
	orl	%edx, %ecx
	je	.L116
	cmpl	$1, %edx
	jne	.L1
	movdqa	32(%rsp), %xmm3
	pxor	.LC9(%rip), %xmm3
	movaps	%xmm3, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	.LC0(%rip), %xmm5
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm5, %xmm1
	movaps	%xmm5, 48(%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L54
	movdqa	.LC1(%rip), %xmm1
	cmpl	$2147418112, %ebp
	sete	%r14b
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	andb	%r14b, %al
	movl	%eax, %r15d
	je	.L59
	testl	%ebx, %ebx
	jne	.L9
	movdqa	48(%rsp), %xmm3
	movaps	%xmm3, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L111:
	movdqa	.LC0(%rip), %xmm7
	movaps	%xmm7, 32(%rsp)
.L1:
	movdqa	32(%rsp), %xmm0
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%rsp), %rdx
	movq	%rdx, %rsi
	shrq	$32, %rsi
	orl	%esi, %eax
	orl	%edx, %eax
	je	.L11
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L112:
	movdqa	48(%rsp), %xmm4
	movaps	%xmm4, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L115:
	cmpl	$1081081855, %ebp
	movl	$2, %edx
	jbe	.L56
.L15:
	testl	%ebx, %ebx
	jne	.L18
	testb	%r15b, %r15b
	jne	.L55
.L19:
	cmpl	$1073676288, %ebp
	je	.L117
	cmpl	$1073741824, %r12d
	je	.L118
	cmpq	$1073610752, %r12
	jne	.L18
	testl	%r14d, %r14d
	js	.L18
	movdqa	16(%rsp), %xmm0
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__ieee754_sqrtf128@PLT
	.p2align 4,,10
	.p2align 3
.L24:
	shrl	$31, %r14d
	movl	%edx, %eax
	subl	$1, %r14d
	orl	%r14d, %eax
	je	.L119
	subl	$1, %edx
	orl	%r14d, %edx
	je	.L68
	movdqa	48(%rsp), %xmm3
	movaps	%xmm3, 96(%rsp)
.L29:
	cmpl	$1075668299, %ebp
	jbe	.L30
	cmpl	$1081959755, %ebp
	jbe	.L31
	cmpl	$1073676287, %ecx
	ja	.L32
	testl	%r12d, %r12d
	js	.L34
.L33:
	movdqa	.LC12(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L114:
	testl	%ebx, %ebx
	jne	.L10
	testl	%r14d, %r14d
	js	.L120
.L55:
	movq	16(%rsp), %rdx
	movq	24(%rsp), %rax
	movq	%rdx, %rsi
	shrq	$32, %rsi
	orl	%esi, %eax
	orl	%edx, %eax
	leal	-1073676288(%rcx), %edx
	orl	%edx, %eax
	je	.L121
	cmpl	$1073676287, %ecx
	jbe	.L21
	testl	%r12d, %r12d
	js	.L65
	movdqa	(%rsp), %xmm3
	movaps	%xmm3, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L119:
	movdqa	16(%rsp), %xmm1
.L104:
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L120:
	cmpl	$1081081855, %ebp
	ja	.L55
.L56:
	xorl	%edx, %edx
	cmpl	$1073676287, %ebp
	jbe	.L15
	movdqa	(%rsp), %xmm0
	movl	%ecx, 64(%rsp)
	movl	%edx, 32(%rsp)
	call	__floorf128@PLT
	movdqa	(%rsp), %xmm1
	call	__eqtf2@PLT
	testq	%rax, %rax
	movl	32(%rsp), %edx
	movl	64(%rsp), %ecx
	jne	.L15
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	call	__floorf128@PLT
	movdqa	32(%rsp), %xmm1
	call	__eqtf2@PLT
	xorl	%edx, %edx
	testq	%rax, %rax
	movl	64(%rsp), %ecx
	sete	%dl
	addl	$1, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L30:
	pxor	%xmm1, %xmm1
	movl	%ecx, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	movl	16(%rsp), %ecx
	jg	.L38
	pxor	.LC9(%rip), %xmm0
.L38:
	movdqa	.LC2(%rip), %xmm1
	andl	$-134217728, %r13d
	movl	%ecx, 16(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movl	16(%rsp), %ecx
	jns	.L40
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	movl	%ecx, 16(%rsp)
	xorl	%r13d, %r13d
	call	__lttf2@PLT
	testq	%rax, %rax
	movl	16(%rsp), %ecx
	js	.L122
	xorl	%eax, %eax
	movabsq	$4575375746431713280, %rdx
	movq	%rax, (%rsp)
	movq	%rdx, 8(%rsp)
.L40:
	xorl	%eax, %eax
	cmpl	$65535, %ecx
	ja	.L42
	movdqa	32(%rsp), %xmm0
	movdqa	.LC13(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movq	40(%rsp), %rcx
	movl	$-113, %eax
	shrq	$32, %rcx
.L42:
	movl	%ecx, %edx
	movzwl	%cx, %ecx
	shrl	$16, %edx
	leal	-16383(%rax,%rdx), %ebp
	movl	%ecx, %eax
	orl	$1073676288, %eax
	cmpl	$14728, %ecx
	jle	.L72
	cmpl	$47974, %ecx
	jle	.L73
	pxor	%xmm4, %xmm4
	addl	$1, %ebp
	subl	$65536, %eax
	movaps	%xmm4, 64(%rsp)
	movaps	%xmm4, 128(%rsp)
.L43:
	salq	$32, %rax
	movq	40(%rsp), %rdx
	movdqa	32(%rsp), %xmm2
	movl	%edx, %edx
	movdqa	48(%rsp), %xmm1
	orq	%rax, %rdx
	movaps	%xmm2, 16(%rsp)
	movq	%rdx, 24(%rsp)
	movdqa	16(%rsp), %xmm3
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 112(%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 80(%rsp)
	movdqa	48(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	.LC0(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 160(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movq	%xmm0, %rax
	movdqa	32(%rsp), %xmm3
	movabsq	$-576460752303423488, %rdx
	movaps	%xmm0, 16(%rsp)
	andq	%rdx, %rax
	movabsq	$-576460752303423488, %rdx
	movaps	%xmm0, 144(%rsp)
	movq	%rax, 16(%rsp)
	movq	%xmm3, %rax
	movdqa	16(%rsp), %xmm4
	andq	%rdx, %rax
	movaps	%xmm3, 16(%rsp)
	movq	%rax, 16(%rsp)
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 32(%rsp)
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	160(%rsp), %xmm4
	movdqa	%xmm4, %xmm1
	call	__multf3@PLT
	movdqa	144(%rsp), %xmm2
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 112(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC14(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC15(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC16(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC17(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC18(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm3
	movaps	%xmm0, 80(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC19(%rip), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC20(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC21(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC22(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC23(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	112(%rsp), %xmm2
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 144(%rsp)
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm4
	movaps	%xmm0, 80(%rsp)
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm0
	call	__multf3@PLT
	movdqa	.LC24(%rip), %xmm1
	movaps	%xmm0, 112(%rsp)
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	movq	%xmm0, %rax
	movabsq	$-576460752303423488, %rdx
	movaps	%xmm0, 16(%rsp)
	andq	%rdx, %rax
	movq	%rax, 16(%rsp)
	movdqa	16(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC24(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	112(%rsp), %xmm3
	movdqa	%xmm3, %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	144(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movq	%xmm0, %rax
	movabsq	$-576460752303423488, %rdx
	movaps	%xmm0, 16(%rsp)
	andq	%rdx, %rax
	movq	%rax, 16(%rsp)
	movdqa	.LC25(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC26(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC27(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	128(%rsp), %xmm1
	call	__addtf3@PLT
	movl	%ebp, %edi
	movaps	%xmm0, 80(%rsp)
	call	__floatsitf@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	80(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movq	%xmm0, %rax
	movabsq	$-576460752303423488, %rdx
	movaps	%xmm0, 16(%rsp)
	andq	%rdx, %rax
	movq	%rax, 16(%rsp)
	movq	%r13, %rax
	movdqa	16(%rsp), %xmm7
	salq	$32, %rax
	movdqa	(%rsp), %xmm2
	movdqa	%xmm7, %xmm0
	movdqa	32(%rsp), %xmm1
	movaps	%xmm2, 16(%rsp)
	movq	%rax, 16(%rsp)
	movaps	%xmm7, 32(%rsp)
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	cmpl	$1074593791, %eax
	jle	.L44
	movq	(%rsp), %rcx
	subl	$1074593792, %eax
	movq	%rcx, %rsi
	shrq	$32, %rsi
	orl	%esi, %edx
	orl	%ecx, %edx
	orl	%eax, %edx
	jne	.L108
	movdqa	.LC28(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L108
.L46:
	movdqa	.LC8(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	call	__floorf128@PLT
	call	__fixtfsi@PLT
	movl	%eax, %edi
	movl	%eax, %ebp
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movl	%ebp, %ebx
	movdqa	32(%rsp), %xmm0
	sall	$16, %ebx
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
.L51:
	movq	(%rsp), %rax
	movabsq	$-576460752303423488, %rdx
	movdqa	(%rsp), %xmm3
	andq	%rdx, %rax
	movaps	%xmm3, 16(%rsp)
	movq	%rax, 16(%rsp)
	movdqa	.LC29(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC30(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC31(%rip), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm3
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__multf3@PLT
	movdqa	.LC32(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	.LC33(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC34(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC35(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC36(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC37(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC38(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC39(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC40(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movdqa	.LC41(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	%rdx, %rax
	movdqa	(%rsp), %xmm7
	shrq	$32, %rax
	addl	%ebx, %eax
	cmpl	$65535, %eax
	movaps	%xmm7, 16(%rsp)
	jle	.L123
	salq	$32, %rax
	movl	%edx, %edx
	orq	%rax, %rdx
	movq	%rdx, 24(%rsp)
	movdqa	16(%rsp), %xmm2
.L53:
	movdqa	%xmm2, %xmm0
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L68:
	movdqa	.LC1(%rip), %xmm7
	movaps	%xmm7, 96(%rsp)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L117:
	movdqa	16(%rsp), %xmm4
	testl	%r12d, %r12d
	movaps	%xmm4, 32(%rsp)
	jns	.L1
	movdqa	%xmm4, %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L121:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L118:
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L35:
	cmpl	$1073676288, %ecx
	jbe	.L30
	testl	%r12d, %r12d
	jle	.L109
.L108:
	movdqa	.LC11(%rip), %xmm1
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC11(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L44:
	movl	%eax, %ecx
	andl	$2147483647, %ecx
	cmpl	$1074594232, %ecx
	jle	.L48
	movq	(%rsp), %rsi
	addl	$1072889412, %eax
	movq	%rsi, %rcx
	shrq	$32, %rcx
	orl	%ecx, %edx
	orl	%esi, %edx
	orl	%eax, %edx
	jne	.L109
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L46
.L109:
	movdqa	.LC12(%rip), %xmm1
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC12(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L72:
	pxor	%xmm2, %xmm2
	movaps	%xmm2, 64(%rsp)
	movaps	%xmm2, 128(%rsp)
	jmp	.L43
.L31:
	cmpl	$1073676286, %ecx
	ja	.L35
	testl	%r12d, %r12d
	jns	.L109
	jmp	.L108
.L21:
	testl	%r12d, %r12d
	js	.L124
	pxor	%xmm7, %xmm7
	movaps	%xmm7, 32(%rsp)
	jmp	.L1
.L116:
	movdqa	32(%rsp), %xmm1
	jmp	.L104
.L73:
	movdqa	.LC5(%rip), %xmm7
	movdqa	.LC6(%rip), %xmm3
	movaps	%xmm7, 64(%rsp)
	movaps	%xmm3, 128(%rsp)
	movdqa	.LC7(%rip), %xmm7
	movaps	%xmm7, 48(%rsp)
	jmp	.L43
.L48:
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	cmpl	$1073610752, %ecx
	jle	.L51
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L32:
	testl	%r12d, %r12d
	jle	.L33
.L34:
	movdqa	.LC11(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L122:
	movdqa	.LC3(%rip), %xmm3
	movaps	%xmm3, (%rsp)
	jmp	.L40
.L124:
	movdqa	(%rsp), %xmm4
	pxor	.LC9(%rip), %xmm4
	movaps	%xmm4, 32(%rsp)
	jmp	.L1
.L65:
	pxor	%xmm4, %xmm4
	movaps	%xmm4, 32(%rsp)
	jmp	.L1
.L123:
	movl	%ebp, %edi
	call	__scalbnf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	jmp	.L53
.L59:
	movl	%r14d, %r15d
	jmp	.L9
	.size	__ieee754_powf128, .-__ieee754_powf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1065287680
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	-1082195968
	.align 16
.LC5:
	.long	0
	.long	0
	.long	880015277
	.long	1073621888
	.align 16
.LC6:
	.long	1022994665
	.long	2733339721
	.long	46432897
	.long	1070196712
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1073709056
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC10:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC11:
	.long	2548261900
	.long	1587965796
	.long	2577958947
	.long	1726789860
	.align 16
.LC12:
	.long	3423083468
	.long	1948967477
	.long	2655007663
	.long	420555081
	.align 16
.LC13:
	.long	0
	.long	0
	.long	0
	.long	1081081856
	.align 16
.LC14:
	.long	943009924
	.long	3522922774
	.long	528308121
	.long	-1073808321
	.align 16
.LC15:
	.long	2019254231
	.long	483510684
	.long	2686667813
	.long	1073909843
	.align 16
.LC16:
	.long	608650472
	.long	3147967206
	.long	3710298930
	.long	1074033280
	.align 16
.LC17:
	.long	2470719466
	.long	1665951037
	.long	157866600
	.long	1074070667
	.align 16
.LC18:
	.long	1377630418
	.long	3545331261
	.long	2189627533
	.long	1073998967
	.align 16
.LC19:
	.long	4057592134
	.long	2516686072
	.long	3328165482
	.long	1073929764
	.align 16
.LC20:
	.long	3198744227
	.long	1097404991
	.long	1247171709
	.long	1074078065
	.align 16
.LC21:
	.long	3328314824
	.long	3457970361
	.long	67466002
	.long	1074147535
	.align 16
.LC22:
	.long	2810742103
	.long	3066309256
	.long	738611027
	.long	1074143847
	.align 16
.LC23:
	.long	3295684958
	.long	806959069
	.long	2540517494
	.long	1074043491
	.align 16
.LC24:
	.long	0
	.long	0
	.long	0
	.long	1073774592
	.align 16
.LC25:
	.long	0
	.long	3489660928
	.long	2646843455
	.long	1073671280
	.align 16
.LC26:
	.long	1378599673
	.long	3611950101
	.long	2646843455
	.long	1073671280
	.align 16
.LC27:
	.long	3841749140
	.long	3190403228
	.long	89426672
	.long	1070125695
	.align 16
.LC28:
	.long	2761087597
	.long	3831102947
	.long	1985132591
	.long	1070166356
	.align 16
.LC29:
	.long	0
	.long	4026531840
	.long	804234142
	.long	1073636068
	.align 16
.LC30:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC31:
	.long	4081264243
	.long	4076252992
	.long	3820197891
	.long	1070050249
	.align 16
.LC32:
	.long	3704067303
	.long	2629832888
	.long	3264338244
	.long	1073228238
	.align 16
.LC33:
	.long	168955644
	.long	355436134
	.long	2634310475
	.long	1074044948
	.align 16
.LC34:
	.long	2993173127
	.long	3131634941
	.long	110146514
	.long	1074678066
	.align 16
.LC35:
	.long	950098678
	.long	3823507620
	.long	4271474914
	.long	1075191211
	.align 16
.LC36:
	.long	2097954900
	.long	3899501804
	.long	1821577353
	.long	1075569827
	.align 16
.LC37:
	.long	3274450782
	.long	2267390785
	.long	1569081197
	.long	1074385957
	.align 16
.LC38:
	.long	759404159
	.long	742589361
	.long	3098704490
	.long	1074959176
	.align 16
.LC39:
	.long	4128644364
	.long	2279947788
	.long	4146985492
	.long	1075419163
	.align 16
.LC40:
	.long	1573466198
	.long	1850884529
	.long	2439924839
	.long	1075735418
	.align 16
.LC41:
	.long	0
	.long	0
	.long	0
	.long	1073741824
