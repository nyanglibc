	.text
	.p2align 4,,15
	.globl	__powl
	.type	__powl, @function
__powl:
	subq	$8, %rsp
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__ieee754_powl@PLT
	fld	%st(0)
	addq	$32, %rsp
	fabs
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L15
	fldz
	fxch	%st(2)
	fucomi	%st(2), %st
	jp	.L17
	je	.L16
	fstp	%st(1)
	fstp	%st(1)
	jmp	.L1
.L17:
	fstp	%st(1)
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	fstp	%st(1)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	fldt	16(%rsp)
	fabs
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L18
	fldt	32(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	fucomi	%st(0), %st
	jnp	.L5
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	fldt	16(%rsp)
	fabs
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jb	.L19
	fldt	16(%rsp)
	fucomip	%st(2), %st
	fstp	%st(1)
	jp	.L11
	je	.L1
.L11:
	fldt	32(%rsp)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
.L5:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__powl, .-__powl
	.weak	powf64x
	.set	powf64x,__powl
	.weak	powl
	.set	powl,__powl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
