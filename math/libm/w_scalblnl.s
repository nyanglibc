	.text
	.p2align 4,,15
	.globl	__w_scalblnl
	.type	__w_scalblnl, @function
__w_scalblnl:
	subq	$8, %rsp
	fldt	16(%rsp)
	fld	%st(0)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L3
	jne	.L3
.L2:
	fadd	%st(0), %st
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__scalblnl@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L6
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L1
	jne	.L1
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	addq	$8, %rsp
	ret
	.size	__w_scalblnl, .-__w_scalblnl
	.weak	scalblnf64x
	.set	scalblnf64x,__w_scalblnl
	.weak	scalblnl
	.set	scalblnl,__w_scalblnl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
