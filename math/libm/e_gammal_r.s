	.text
	.p2align 4,,15
	.type	gammal_positive, @function
gammal_positive:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$144, %rsp
	fldt	160(%rsp)
	flds	.LC2(%rip)
	fucomip	%st(1), %st
	ja	.L26
	flds	.LC3(%rip)
	fucomi	%st(1), %st
	jnb	.L27
	flds	.LC4(%rip)
	fucomip	%st(2), %st
	ja	.L28
	fstp	%st(0)
	fldz
	fstpt	128(%rsp)
	flds	.LC5(%rip)
	fucomi	%st(1), %st
	ja	.L29
	fstp	%st(0)
	fldz
	fstpt	80(%rsp)
	fldt	80(%rsp)
	fld1
	fstpt	64(%rsp)
	fstpt	48(%rsp)
.L10:
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__roundl@PLT
	popq	%rdi
	popq	%r8
	fldt	(%rsp)
	leaq	124(%rsp), %rdi
	subq	$16, %rsp
	fld	%st(0)
	fsub	%st(2), %st
	fxch	%st(2)
	fstpt	48(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__frexpl@PLT
	fldt	.LC6(%rip)
	popq	%r9
	popq	%r10
	fucomip	%st(1), %st
	fldt	16(%rsp)
	fldt	32(%rsp)
	ja	.L12
	movl	124(%rsp), %ecx
.L13:
	fnstcw	110(%rsp)
	movzwl	110(%rsp), %edx
	orb	$12, %dh
	movw	%dx, 108(%rsp)
	fldcw	108(%rsp)
	fistpl	104(%rsp)
	fldcw	110(%rsp)
	subq	$32, %rsp
	movl	136(%rsp), %eax
	fld	%st(0)
	fstpt	16(%rsp)
	imull	%ecx, %eax
	movl	%eax, (%rbx)
	fstpt	64(%rsp)
	fstpt	(%rsp)
	call	__ieee754_powl@PLT
	fstpt	48(%rsp)
	popq	%rax
	popq	%rdx
	fildl	140(%rsp)
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_exp2l@PLT
	fldt	32(%rsp)
	subq	$16, %rsp
	fmulp	%st, %st(1)
	fstpt	48(%rsp)
	fldt	64(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fchs
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	.LC7(%rip)
	fldt	32(%rsp)
	fdivr	%st, %st(1)
	fxch	%st(1)
	fsqrt
	fldt	48(%rsp)
	fmulp	%st, %st(3)
	fmulp	%st, %st(2)
	fldt	96(%rsp)
	fdivrp	%st, %st(2)
	fxch	%st(1)
	fstpt	48(%rsp)
	popq	%rcx
	popq	%rsi
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	16(%rsp)
	call	__ieee754_logl@PLT
	fldt	16(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fdiv	%st(1), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fdiv	%st(1), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fdiv	%st(1), %st
	fldt	.LC11(%rip)
	faddp	%st, %st(1)
	fdiv	%st(1), %st
	fldt	.LC12(%rip)
	fsubrp	%st, %st(1)
	fdiv	%st(1), %st
	fldt	.LC13(%rip)
	faddp	%st, %st(1)
	fdiv	%st(1), %st
	fldt	.LC14(%rip)
	fsubrp	%st, %st(1)
	fdivp	%st, %st(1)
	fldt	80(%rsp)
	fmulp	%st, %st(3)
	fldt	112(%rsp)
	fsubrp	%st, %st(3)
	fldt	.LC15(%rip)
	faddp	%st, %st(1)
	fdivp	%st, %st(1)
	faddp	%st, %st(1)
	fstpt	(%rsp)
	call	__expm1l@PLT
	fldt	48(%rsp)
	addq	$32, %rsp
	addq	$144, %rsp
	popq	%rbx
	fmul	%st, %st(1)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	fstp	%st(0)
	movl	$0, (%rdi)
	leaq	128(%rsp), %rdi
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_lgammal_r@PLT
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	addq	$32, %rsp
	addq	$144, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	fld1
	movl	$0, (%rdi)
	leaq	128(%rsp), %rdi
	subq	$16, %rsp
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__ieee754_lgammal_r@PLT
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	32(%rsp)
	addq	$32, %rsp
	addq	$144, %rsp
	popq	%rbx
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	fxch	%st(2)
	movl	124(%rsp), %eax
	fadd	%st(0), %st
	fxch	%st(2)
	leal	-1(%rax), %ecx
	movl	%ecx, 124(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$0, (%rdi)
	fnstcw	110(%rsp)
	movzwl	110(%rsp), %eax
	fsubr	%st(1), %st
	leaq	128(%rsp), %rsi
	andb	$-13, %ah
	orb	$8, %ah
	movw	%ax, 102(%rsp)
	movzwl	110(%rsp), %eax
	fldcw	102(%rsp)
	frndint
	fldcw	110(%rsp)
	fsubr	%st, %st(1)
	orb	$12, %ah
	movw	%ax, 108(%rsp)
	fldcw	108(%rsp)
	fistpl	104(%rsp)
	fldcw	110(%rsp)
	movl	104(%rsp), %edi
	pushq	$0
	pushq	$0
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	__gamma_productl@PLT
	fstpt	32(%rsp)
	addq	$32, %rsp
	leaq	124(%rsp), %rdi
	fldt	16(%rsp)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_lgammal_r@PLT
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	160(%rsp)
	fld1
	faddp	%st, %st(1)
	fldt	32(%rsp)
	addq	$32, %rsp
	addq	$144, %rsp
	popq	%rbx
	fmulp	%st, %st(2)
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	fnstcw	110(%rsp)
	movzwl	110(%rsp), %eax
	fsub	%st(1), %st
	leaq	128(%rsp), %rsi
	andb	$-13, %ah
	orb	$8, %ah
	movw	%ax, 102(%rsp)
	movzwl	110(%rsp), %eax
	fldcw	102(%rsp)
	frndint
	fldcw	110(%rsp)
	fld	%st(1)
	fadd	%st(1), %st
	orb	$12, %ah
	movw	%ax, 108(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	fsub	%st(1), %st
	fsubr	%st, %st(2)
	fxch	%st(2)
	fld	%st(0)
	fstpt	48(%rsp)
	fxch	%st(1)
	fldcw	108(%rsp)
	fistpl	104(%rsp)
	fldcw	110(%rsp)
	subq	$32, %rsp
	movl	136(%rsp), %edi
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__gamma_productl@PLT
	fstpt	96(%rsp)
	fldt	160(%rsp)
	fstpt	112(%rsp)
	addq	$32, %rsp
	fldt	(%rsp)
	jmp	.L10
	.size	gammal_positive, .-gammal_positive
	.p2align 4,,15
	.globl	__ieee754_gammal_r
	.type	__ieee754_gammal_r, @function
__ieee754_gammal_r:
	pushq	%rbx
	subq	$96, %rsp
	movq	112(%rsp), %rdx
	movq	120(%rsp), %rax
	movq	%rdx, %rsi
	movl	%eax, %ecx
	shrq	$32, %rsi
	andw	$32767, %cx
	movl	%esi, %r9d
	movswl	%cx, %r8d
	orl	%edx, %r9d
	orl	%r8d, %r9d
	je	.L94
	movswl	%ax, %r10d
	cmpl	$-1, %r10d
	je	.L95
.L33:
	cmpw	$32767, %cx
	je	.L96
	testw	%ax, %ax
	js	.L97
	jmp	.L35
.L107:
	fstp	%st(0)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L108:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L35:
	flds	.LC17(%rip)
	fldt	112(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L98
#APP
# 383 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstcw 42(%rsp)
# 0 "" 2
#NO_APP
	movzwl	42(%rsp), %edx
	movl	%edx, %eax
	movw	%dx, 48(%rsp)
	andb	$-16, %ah
	orb	$3, %ah
	cmpw	%ax, %dx
	movw	%ax, 44(%rsp)
	jne	.L99
	movb	$0, 80(%rsp)
.L40:
	fldz
	movq	%rdi, %rbx
	fldt	112(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L100
	fstp	%st(0)
	flds	.LC19(%rip)
	fldt	112(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L101
	fstp	%st(0)
	fldt	112(%rsp)
	fnstcw	30(%rsp)
	movzwl	30(%rsp), %eax
	orb	$12, %ah
	movw	%ax, 28(%rsp)
	fldcw	28(%rsp)
	frndint
	fldcw	30(%rsp)
	flds	.LC2(%rip)
	fmul	%st(1), %st
	fldcw	28(%rsp)
	frndint
	fldcw	30(%rsp)
	fadd	%st(0), %st
	fucomip	%st(1), %st
	jp	.L64
	movl	$-1, %eax
	je	.L46
.L64:
	movl	$1, %eax
.L46:
	fldt	112(%rsp)
	flds	.LC20(%rip)
	movl	%eax, (%rbx)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L90
	fstp	%st(0)
	fldt	.LC21(%rip)
	fmul	%st(0), %st
	.p2align 4,,10
	.p2align 3
.L43:
	cmpb	$0, 80(%rsp)
	jne	.L102
.L57:
	fxam
	fnstsw	%ax
	fldz
	movl	%eax, %edx
	andb	$69, %dh
	xorl	%esi, %esi
	movl	$1, %ecx
	fldt	112(%rsp)
	cmpb	$5, %dh
	sete	%sil
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%dl
	cmovne	%ecx, %edx
	testb	%dl, %dl
	je	.L58
	testl	%esi, %esi
	jne	.L103
.L58:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L92
	jne	.L92
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC21(%rip)
	je	.L62
	fstp	%st(0)
	fldt	.LC25(%rip)
.L62:
	movl	(%rbx), %eax
	testl	%eax, %eax
	js	.L104
	fldt	.LC21(%rip)
	addq	$96, %rsp
	popq	%rbx
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L98:
	fldt	.LC18(%rip)
	movl	$0, (%rdi)
	fmul	%st(0), %st
.L92:
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	fld1
	movl	$0, (%rdi)
	fdivp	%st, %st(1)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L100:
	movl	$0, (%rdi)
	leaq	44(%rsp), %rdi
	subq	$16, %rsp
	fstpt	(%rsp)
	call	gammal_positive
	subq	$16, %rsp
	fstpt	(%rsp)
	movl	76(%rsp), %edi
	call	__scalbnl@PLT
	addq	$32, %rsp
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L103:
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC18(%rip)
	je	.L59
	fstp	%st(0)
	fldt	.LC24(%rip)
.L59:
	movl	(%rbx), %edx
	testl	%edx, %edx
	js	.L105
	fldt	.LC18(%rip)
	fmulp	%st, %st(1)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L94:
	fld1
	movl	$0, (%rdi)
	fldt	112(%rsp)
	addq	$96, %rsp
	popq	%rbx
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	andl	$2147483647, %esi
	orl	%edx, %esi
	jne	.L33
	fldt	112(%rsp)
	movl	$0, (%rdi)
	fsub	%st(0), %st
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	fldt	112(%rsp)
	movl	$0, (%rdi)
	fadd	%st(0), %st
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L97:
	fldt	112(%rsp)
	frndint
	fldt	112(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	jp	.L107
	jne	.L108
	fld	%st(0)
	movl	$0, (%rdi)
	fsubp	%st, %st(1)
	fdiv	%st(0), %st
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L90:
	fldt	112(%rsp)
	fsubrp	%st, %st(1)
	flds	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L50
	fld1
	fsubp	%st, %st(1)
.L50:
	flds	.LC22(%rip)
	fucomip	%st(1), %st
	jnb	.L106
	fsubrs	.LC2(%rip)
	subq	$16, %rsp
	fldpi
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__cosl@PLT
	popq	%rcx
	popq	%rsi
.L54:
	fldt	112(%rsp)
	leaq	44(%rsp), %rdi
	subq	$16, %rsp
	fchs
	fmul	%st, %st(1)
	fxch	%st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	gammal_positive
	fldt	16(%rsp)
	movl	60(%rsp), %edi
	subq	$16, %rsp
	negl	%edi
	fmulp	%st, %st(1)
	fldpi
	fdivp	%st, %st(1)
	fstpt	(%rsp)
	call	__scalbnl@PLT
	fldt	.LC21(%rip)
	addq	$32, %rsp
	fucomip	%st(1), %st
	jbe	.L43
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L104:
	fchs
	fldt	.LC21(%rip)
	fmulp	%st, %st(1)
	fchs
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L106:
	fldpi
	subq	$16, %rsp
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__sinl@PLT
	popq	%rdi
	popq	%r8
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L99:
#APP
# 391 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 44(%rsp)
# 0 "" 2
#NO_APP
	movb	$1, 80(%rsp)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L102:
#APP
# 439 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 48(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L105:
	fchs
	fldt	.LC18(%rip)
	fmulp	%st, %st(1)
	fchs
	jmp	.L92
	.size	__ieee754_gammal_r, .-__ieee754_gammal_r
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	1056964608
	.align 4
.LC3:
	.long	1069547520
	.align 4
.LC4:
	.long	1089470464
	.align 4
.LC5:
	.long	1095761920
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC6:
	.long	4192101508
	.long	3037000499
	.long	16382
	.long	0
	.align 16
.LC7:
	.long	560513589
	.long	3373259426
	.long	16385
	.long	0
	.align 16
.LC8:
	.long	2057092833
	.long	4061410904
	.long	49145
	.long	0
	.align 16
.LC9:
	.long	220254733
	.long	3524075730
	.long	16375
	.long	0
	.align 16
.LC10:
	.long	3387155472
	.long	4216686284
	.long	16373
	.long	0
	.align 16
.LC11:
	.long	3354991288
	.long	3702059352
	.long	16372
	.long	0
	.align 16
.LC12:
	.long	163617802
	.long	2617884828
	.long	16372
	.long	0
	.align 16
.LC13:
	.long	218157069
	.long	3490513104
	.long	16372
	.long	0
	.align 16
.LC14:
	.long	190887435
	.long	3054198966
	.long	16374
	.long	0
	.align 16
.LC15:
	.long	2863311531
	.long	2863311530
	.long	16379
	.long	0
	.section	.rodata.cst4
	.align 4
.LC17:
	.long	1155235840
	.section	.rodata.cst16
	.align 16
.LC18:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.section	.rodata.cst4
	.align 4
.LC19:
	.long	2667577344
	.align 4
.LC20:
	.long	3302801408
	.section	.rodata.cst16
	.align 16
.LC21:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC22:
	.long	1048576000
	.section	.rodata.cst16
	.align 16
.LC24:
	.long	4294967295
	.long	4294967295
	.long	65534
	.long	0
	.align 16
.LC25:
	.long	0
	.long	2147483648
	.long	32769
	.long	0
