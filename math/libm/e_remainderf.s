	.text
	.p2align 4,,15
	.globl	__ieee754_remainderf
	.type	__ieee754_remainderf, @function
__ieee754_remainderf:
	movd	%xmm1, %eax
	movaps	%xmm1, %xmm2
	andl	$2147483647, %eax
	je	.L24
	pushq	%rbp
	movd	%xmm0, %ebp
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %ebp
	subq	$24, %rsp
	cmpl	$2139095039, %ebp
	jg	.L14
	cmpl	$2139095040, %eax
	jg	.L14
	cmpl	$2130706431, %eax
	jle	.L25
.L6:
	cmpl	%ebp, %eax
	je	.L26
	movss	.LC1(%rip), %xmm1
	cmpl	$16777215, %eax
	andps	%xmm1, %xmm0
	andps	%xmm1, %xmm2
	jg	.L8
	movaps	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	ucomiss	%xmm2, %xmm1
	jbe	.L9
	subss	%xmm2, %xmm0
	movaps	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	ucomiss	%xmm2, %xmm1
	jb	.L9
.L22:
	subss	%xmm2, %xmm0
.L9:
	movd	%xmm0, %eax
	andl	$-2147483648, %ebx
	xorl	%ebx, %eax
	movl	%eax, 8(%rsp)
	movd	8(%rsp), %xmm0
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	mulss	%xmm2, %xmm0
	divss	%xmm0, %xmm0
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	mulss	%xmm1, %xmm0
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movss	.LC2(%rip), %xmm1
	mulss	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.L9
	subss	%xmm2, %xmm0
	ucomiss	%xmm1, %xmm0
	jb	.L9
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L25:
	addss	%xmm1, %xmm1
	movl	%eax, 12(%rsp)
	movss	%xmm2, 8(%rsp)
	call	__ieee754_fmodf@PLT
	movl	12(%rsp), %eax
	movss	8(%rsp), %xmm2
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L26:
	mulss	.LC0(%rip), %xmm0
	jmp	.L1
	.size	__ieee754_remainderf, .-__ieee754_remainderf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC2:
	.long	1056964608
