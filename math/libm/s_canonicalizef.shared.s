	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__canonicalizef
	.type	__canonicalizef, @function
__canonicalizef:
	movss	(%rsi), %xmm0
	movd	%xmm0, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	ja	.L5
	movss	%xmm0, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	addss	%xmm0, %xmm0
	xorl	%eax, %eax
	movss	%xmm0, (%rdi)
	ret
	.size	__canonicalizef, .-__canonicalizef
	.weak	canonicalizef32
	.set	canonicalizef32,__canonicalizef
	.weak	canonicalizef
	.set	canonicalizef,__canonicalizef
