	.text
	.p2align 4,,15
	.globl	__iseqsigl
	.type	__iseqsigl, @function
__iseqsigl:
	subq	$8, %rsp
	fldt	16(%rsp)
	fldt	32(%rsp)
	fucomi	%st(1), %st
	fxch	%st(1)
	setnb	%al
	fucomip	%st(1), %st
	fstp	%st(0)
	setnb	%dl
	testb	%al, %al
	je	.L7
	testb	%dl, %dl
	movl	$1, %eax
	je	.L5
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	testb	%dl, %dl
	je	.L10
.L5:
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$1, %edi
	call	__feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__iseqsigl, .-__iseqsigl
