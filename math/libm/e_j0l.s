	.text
	.p2align 4,,15
	.type	pzero, @function
pzero:
	movq	16(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16385, %ax
	jg	.L3
	movq	8(%rsp), %rdx
	cwtl
	sall	$16, %eax
	shrq	$48, %rdx
	orl	%edx, %eax
	cmpl	$1073844595, %eax
	ja	.L4
	cmpl	$1073788634, %eax
	ja	.L5
	fldt	.LC39(%rip)
	fldt	.LC40(%rip)
	fstpt	-24(%rsp)
	fldt	.LC41(%rip)
	fstpt	-40(%rsp)
	fldt	.LC42(%rip)
	fstpt	-56(%rsp)
	fldt	.LC43(%rip)
	fstpt	-72(%rsp)
	fldt	.LC44(%rip)
	fldt	.LC45(%rip)
	fldt	.LC46(%rip)
	fldt	.LC47(%rip)
	fstpt	-88(%rsp)
	fldt	.LC48(%rip)
	fldt	.LC49(%rip)
	fldt	.LC50(%rip)
	fstpt	-104(%rsp)
	fldt	.LC51(%rip)
	fstpt	-120(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	fldt	.LC13(%rip)
	fldt	.LC14(%rip)
	fstpt	-24(%rsp)
	fldt	.LC15(%rip)
	fstpt	-40(%rsp)
	fldt	.LC16(%rip)
	fstpt	-56(%rsp)
	fldt	.LC17(%rip)
	fstpt	-72(%rsp)
	fldt	.LC18(%rip)
	fldt	.LC19(%rip)
	fldt	.LC20(%rip)
	fldt	.LC21(%rip)
	fstpt	-88(%rsp)
	fldt	.LC22(%rip)
	fldt	.LC23(%rip)
	fldt	.LC24(%rip)
	fstpt	-104(%rsp)
	fldt	.LC25(%rip)
	fstpt	-120(%rsp)
.L2:
	fldt	8(%rsp)
	fmul	%st(0), %st
	fld1
	fdiv	%st, %st(1)
	fxch	%st(6)
	fmul	%st(1), %st
	faddp	%st, %st(5)
	fmul	%st, %st(4)
	fxch	%st(4)
	faddp	%st, %st(3)
	fxch	%st(2)
	fmul	%st(3), %st
	fldt	-88(%rsp)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	faddp	%st, %st(1)
	fmul	%st(2), %st
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-120(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fxch	%st(3)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	-24(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-40(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-56(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-72(%rsp)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	-104(%rsp)
	faddp	%st, %st(1)
	fdivrp	%st, %st(2)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	fldt	.LC0(%rip)
	fldt	.LC1(%rip)
	fstpt	-24(%rsp)
	fldt	.LC2(%rip)
	fstpt	-40(%rsp)
	fldt	.LC3(%rip)
	fstpt	-56(%rsp)
	fldt	.LC4(%rip)
	fstpt	-72(%rsp)
	fldt	.LC5(%rip)
	fldt	.LC6(%rip)
	fldt	.LC7(%rip)
	fldt	.LC8(%rip)
	fstpt	-88(%rsp)
	fldt	.LC9(%rip)
	fldt	.LC10(%rip)
	fldt	.LC11(%rip)
	fstpt	-104(%rsp)
	fldt	.LC12(%rip)
	fstpt	-120(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	fldt	.LC26(%rip)
	fldt	.LC27(%rip)
	fstpt	-24(%rsp)
	fldt	.LC28(%rip)
	fstpt	-40(%rsp)
	fldt	.LC29(%rip)
	fstpt	-56(%rsp)
	fldt	.LC30(%rip)
	fstpt	-72(%rsp)
	fldt	.LC31(%rip)
	fldt	.LC32(%rip)
	fldt	.LC33(%rip)
	fldt	.LC34(%rip)
	fstpt	-88(%rsp)
	fldt	.LC35(%rip)
	fldt	.LC36(%rip)
	fldt	.LC37(%rip)
	fstpt	-104(%rsp)
	fldt	.LC38(%rip)
	fstpt	-120(%rsp)
	jmp	.L2
	.size	pzero, .-pzero
	.p2align 4,,15
	.type	qzero, @function
qzero:
	subq	$16, %rsp
	movq	32(%rsp), %rax
	andw	$32767, %ax
	cmpw	$16385, %ax
	jg	.L9
	movq	24(%rsp), %rdx
	cwtl
	sall	$16, %eax
	shrq	$48, %rdx
	orl	%edx, %eax
	cmpl	$1073844595, %eax
	ja	.L10
	cmpl	$1073788634, %eax
	ja	.L11
	fldt	.LC96(%rip)
	fldt	.LC97(%rip)
	fstpt	-8(%rsp)
	fldt	.LC98(%rip)
	fstpt	-24(%rsp)
	fldt	.LC99(%rip)
	fstpt	-40(%rsp)
	fldt	.LC100(%rip)
	fstpt	-56(%rsp)
	fldt	.LC101(%rip)
	fstpt	-72(%rsp)
	fldt	.LC102(%rip)
	fldt	.LC103(%rip)
	fldt	.LC104(%rip)
	fstpt	-88(%rsp)
	fldt	.LC105(%rip)
	fstpt	-104(%rsp)
	fldt	.LC106(%rip)
	fldt	.LC107(%rip)
	fldt	.LC108(%rip)
	fstpt	-120(%rsp)
	fldt	.LC109(%rip)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	fldt	.LC68(%rip)
	fldt	.LC69(%rip)
	fstpt	-8(%rsp)
	fldt	.LC70(%rip)
	fstpt	-24(%rsp)
	fldt	.LC71(%rip)
	fstpt	-40(%rsp)
	fldt	.LC72(%rip)
	fstpt	-56(%rsp)
	fldt	.LC73(%rip)
	fstpt	-72(%rsp)
	fldt	.LC74(%rip)
	fldt	.LC75(%rip)
	fldt	.LC76(%rip)
	fstpt	-88(%rsp)
	fldt	.LC77(%rip)
	fstpt	-104(%rsp)
	fldt	.LC78(%rip)
	fldt	.LC79(%rip)
	fldt	.LC80(%rip)
	fstpt	-120(%rsp)
	fldt	.LC81(%rip)
.L8:
	fldt	24(%rsp)
	fmul	%st(0), %st
	fld1
	fdivp	%st, %st(1)
	fmul	%st, %st(5)
	fxch	%st(5)
	faddp	%st, %st(4)
	fxch	%st(3)
	fmul	%st(4), %st
	fldt	-88(%rsp)
	faddp	%st, %st(1)
	fmul	%st(4), %st
	fldt	-104(%rsp)
	faddp	%st, %st(1)
	fmul	%st(4), %st
	faddp	%st, %st(2)
	fxch	%st(1)
	fmul	%st(3), %st
	faddp	%st, %st(1)
	fmul	%st(2), %st
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fxch	%st(2)
	fadd	%st(1), %st
	fmul	%st(1), %st
	fldt	-8(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-24(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-40(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-56(%rsp)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	-72(%rsp)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldt	-120(%rsp)
	faddp	%st, %st(1)
	fdivrp	%st, %st(1)
	fsubs	.LC110(%rip)
	fldt	24(%rsp)
	addq	$16, %rsp
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	fldt	.LC54(%rip)
	fldt	.LC55(%rip)
	fstpt	-8(%rsp)
	fldt	.LC56(%rip)
	fstpt	-24(%rsp)
	fldt	.LC57(%rip)
	fstpt	-40(%rsp)
	fldt	.LC58(%rip)
	fstpt	-56(%rsp)
	fldt	.LC59(%rip)
	fstpt	-72(%rsp)
	fldt	.LC60(%rip)
	fldt	.LC61(%rip)
	fldt	.LC62(%rip)
	fstpt	-88(%rsp)
	fldt	.LC63(%rip)
	fstpt	-104(%rsp)
	fldt	.LC64(%rip)
	fldt	.LC65(%rip)
	fldt	.LC66(%rip)
	fstpt	-120(%rsp)
	fldt	.LC67(%rip)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	fldt	.LC82(%rip)
	fldt	.LC83(%rip)
	fstpt	-8(%rsp)
	fldt	.LC84(%rip)
	fstpt	-24(%rsp)
	fldt	.LC85(%rip)
	fstpt	-40(%rsp)
	fldt	.LC86(%rip)
	fstpt	-56(%rsp)
	fldt	.LC87(%rip)
	fstpt	-72(%rsp)
	fldt	.LC88(%rip)
	fldt	.LC89(%rip)
	fldt	.LC90(%rip)
	fstpt	-88(%rsp)
	fldt	.LC91(%rip)
	fstpt	-104(%rsp)
	fldt	.LC92(%rip)
	fldt	.LC93(%rip)
	fldt	.LC94(%rip)
	fstpt	-120(%rsp)
	fldt	.LC95(%rip)
	jmp	.L8
	.size	qzero, .-qzero
	.p2align 4,,15
	.globl	__ieee754_j0l
	.type	__ieee754_j0l, @function
__ieee754_j0l:
	pushq	%rbx
	subq	$112, %rsp
	movq	136(%rsp), %rax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L31
	fldt	128(%rsp)
	movswl	%ax, %ebx
	cmpl	$16383, %ebx
	fabs
	jg	.L32
	cmpl	$16366, %ebx
	jle	.L33
	fldt	128(%rsp)
	cmpl	$16383, %ebx
	fmul	%st(0), %st
	fldt	.LC115(%rip)
	fmul	%st(1), %st
	fldt	.LC116(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC117(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC118(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC119(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC120(%rip)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC121(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC122(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC123(%rip)
	faddp	%st, %st(1)
	fdivrp	%st, %st(1)
	fmul	%st(1), %st
	jne	.L34
	fstp	%st(1)
	fxch	%st(1)
	fmuls	.LC124(%rip)
	fld1
	fld	%st(1)
	fadd	%st(1), %st
	fxch	%st(2)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(1)
	faddp	%st, %st(1)
.L13:
	addq	$112, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	fstp	%st(2)
	fmuls	.LC114(%rip)
	addq	$112, %rsp
	popq	%rbx
	fld1
	fsubp	%st, %st(1)
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	80(%rsp), %rdi
	leaq	96(%rsp), %rsi
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	32(%rsp)
	call	__sincosl@PLT
	cmpl	$32766, %ebx
	fldt	96(%rsp)
	fldt	112(%rsp)
	fld	%st(1)
	fadd	%st(1), %st
	fstpt	16(%rsp)
	popq	%rdi
	popq	%r8
	fldt	16(%rsp)
	jne	.L28
	fstp	%st(1)
	fstp	%st(1)
	fsqrt
	jmp	.L17
.L37:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L17:
	fldt	.LC112(%rip)
	fldt	(%rsp)
	fmulp	%st, %st(1)
	fdivp	%st, %st(1)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L28:
	fxch	%st(1)
	fstpt	48(%rsp)
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	48(%rsp)
	fld	%st(0)
	fadd	%st(1), %st
	fxch	%st(1)
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__cosl@PLT
	fchs
	fldt	96(%rsp)
	fldt	112(%rsp)
	popq	%rcx
	popq	%rsi
	fmulp	%st, %st(1)
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	16(%rsp)
	fldt	32(%rsp)
	fldt	48(%rsp)
	ja	.L35
	fstp	%st(0)
	fstp	%st(0)
	fldt	(%rsp)
	fdivrp	%st, %st(2)
	fxch	%st(1)
	fstpt	64(%rsp)
.L20:
	cmpl	$16526, %ebx
	fld	%st(0)
	fsqrt
	jg	.L37
	fstpt	48(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	48(%rsp)
	call	pzero
	fstpt	32(%rsp)
	fldt	48(%rsp)
	fstpt	(%rsp)
	call	qzero
	popq	%rax
	popq	%rdx
	fldt	(%rsp)
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fldt	64(%rsp)
	fmulp	%st, %st(2)
	fsubp	%st, %st(1)
	fldt	.LC112(%rip)
	fmulp	%st, %st(1)
	fldt	48(%rsp)
	addq	$112, %rsp
	popq	%rbx
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	fldt	128(%rsp)
	addq	$112, %rsp
	popq	%rbx
	fmul	%st(0), %st
	fld1
	fdivp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	fldt	.LC113(%rip)
	fadd	%st(1), %st
	fstp	%st(0)
	cmpl	$16349, %ebx
	jg	.L36
	fstp	%st(0)
	fld1
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L36:
	fld	%st(0)
	fmuls	.LC114(%rip)
	fmulp	%st, %st(1)
	fld1
	fsubp	%st, %st(1)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L35:
	fsubrp	%st, %st(1)
	fld	%st(0)
	fstpt	64(%rsp)
	fdivrp	%st, %st(2)
	fxch	%st(1)
	fstpt	(%rsp)
	jmp	.L20
	.size	__ieee754_j0l, .-__ieee754_j0l
	.p2align 4,,15
	.globl	__ieee754_y0l
	.type	__ieee754_y0l, @function
__ieee754_y0l:
	pushq	%rbx
	subq	$96, %rsp
	movq	120(%rsp), %rax
	movq	112(%rsp), %rcx
	movl	%eax, %edx
	movq	%rcx, %rsi
	shrq	$32, %rsi
	andw	$32767, %dx
	testw	%ax, %ax
	js	.L56
	cmpw	$32767, %dx
	je	.L57
	movl	%ecx, %eax
	orl	%esi, %eax
	je	.L58
	movswl	%dx, %ebx
	cmpl	$16383, %ebx
	jg	.L59
	cmpl	$16350, %ebx
	jle	.L60
	fldt	112(%rsp)
	subq	$16, %rsp
	fld	%st(0)
	fmul	%st(0), %st
	fldt	.LC128(%rip)
	fmul	%st(1), %st
	fldt	.LC129(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC130(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC131(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC132(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC133(%rip)
	fsubrp	%st, %st(1)
	fldt	.LC134(%rip)
	fadd	%st(2), %st
	fmul	%st(2), %st
	fldt	.LC135(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC136(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC137(%rip)
	faddp	%st, %st(1)
	fmulp	%st, %st(2)
	fldt	.LC138(%rip)
	faddp	%st, %st(2)
	fdivp	%st, %st(1)
	fstpt	32(%rsp)
	fstpt	(%rsp)
	call	__ieee754_j0l
	fstpt	16(%rsp)
	pushq	136(%rsp)
	pushq	136(%rsp)
	call	__ieee754_logl@PLT
	fldt	32(%rsp)
	fmulp	%st, %st(1)
	fldt	.LC126(%rip)
	fmulp	%st, %st(1)
	fldt	48(%rsp)
	addq	$32, %rsp
	faddp	%st, %st(1)
.L38:
	addq	$96, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	80(%rsp), %rsi
	leaq	64(%rsp), %rdi
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	__sincosl@PLT
	fldt	80(%rsp)
	cmpl	$32766, %ebx
	fldt	96(%rsp)
	fld	%st(1)
	fsub	%st(1), %st
	fstpt	16(%rsp)
	popq	%r11
	popq	%rax
	jne	.L53
	fstp	%st(0)
	fstp	%st(0)
	fldt	112(%rsp)
	fsqrt
.L44:
	fldt	.LC112(%rip)
	fldt	(%rsp)
	fmulp	%st, %st(1)
	fdivp	%st, %st(1)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L53:
	fstpt	32(%rsp)
	subq	$16, %rsp
	fstpt	32(%rsp)
	fldt	128(%rsp)
	fadd	%st(0), %st
	fstpt	(%rsp)
	call	__cosl@PLT
	fchs
	fldt	80(%rsp)
	fldt	96(%rsp)
	popq	%r9
	popq	%r10
	fmulp	%st, %st(1)
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	fldt	16(%rsp)
	fldt	32(%rsp)
	ja	.L61
	faddp	%st, %st(1)
	fld	%st(0)
	fstpt	48(%rsp)
	fdivrp	%st, %st(1)
	fstpt	(%rsp)
.L47:
	cmpl	$16526, %ebx
	fldt	112(%rsp)
	fsqrt
	jg	.L44
	fstpt	32(%rsp)
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	pzero
	popq	%rcx
	popq	%rsi
	fstpt	16(%rsp)
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	qzero
	popq	%rdi
	popq	%r8
	fldt	(%rsp)
	fldt	16(%rsp)
	fmulp	%st, %st(1)
	fldt	48(%rsp)
	fmulp	%st, %st(2)
	faddp	%st, %st(1)
	fldt	.LC112(%rip)
	fmulp	%st, %st(1)
	fldt	32(%rsp)
	addq	$96, %rsp
	popq	%rbx
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	fldz
	fldt	112(%rsp)
	addq	$96, %rsp
	popq	%rbx
	fmul	%st(1), %st
	fdivrp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	fldt	112(%rsp)
	fld	%st(0)
	fmul	%st(0), %st
	faddp	%st, %st(1)
	fld1
	fdivp	%st, %st(1)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L58:
	fldt	112(%rsp)
	fsubs	.LC125(%rip)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L60:
	pushq	120(%rsp)
	pushq	120(%rsp)
	call	__ieee754_logl@PLT
	fldt	.LC126(%rip)
	popq	%rax
	popq	%rdx
	fmulp	%st, %st(1)
	fldt	.LC127(%rip)
	fsubrp	%st, %st(1)
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L61:
	fstp	%st(0)
	fstp	%st(0)
	fldt	(%rsp)
	fdivrp	%st, %st(1)
	fstpt	48(%rsp)
	jmp	.L47
	.size	__ieee754_y0l, .-__ieee754_y0l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2695552976
	.long	2468528470
	.long	16383
	.long	0
	.align 16
.LC1:
	.long	2009062972
	.long	2539409906
	.long	16381
	.long	0
	.align 16
.LC2:
	.long	1590457637
	.long	3581685762
	.long	16377
	.long	0
	.align 16
.LC3:
	.long	1461686887
	.long	4016590283
	.long	16372
	.long	0
	.align 16
.LC4:
	.long	1098668007
	.long	3600858672
	.long	16366
	.long	0
	.align 16
.LC5:
	.long	2241955399
	.long	2869125108
	.long	49145
	.long	0
	.align 16
.LC6:
	.long	1953006526
	.long	4004406827
	.long	49146
	.long	0
	.align 16
.LC7:
	.long	1911065043
	.long	2517202976
	.long	49145
	.long	0
	.align 16
.LC8:
	.long	3496883307
	.long	3819489613
	.long	49141
	.long	0
	.align 16
.LC9:
	.long	4005359094
	.long	2210024513
	.long	49137
	.long	0
	.align 16
.LC10:
	.long	947951736
	.long	4021553990
	.long	49130
	.long	0
	.align 16
.LC11:
	.long	8410478
	.long	4196018253
	.long	16358
	.long	0
	.align 16
.LC12:
	.long	1346906538
	.long	2360260267
	.long	49123
	.long	0
	.align 16
.LC13:
	.long	2294543209
	.long	2222182587
	.long	16384
	.long	0
	.align 16
.LC14:
	.long	19287567
	.long	2161026751
	.long	16383
	.long	0
	.align 16
.LC15:
	.long	1026548782
	.long	2953583825
	.long	16380
	.long	0
	.align 16
.LC16:
	.long	3861181787
	.long	3247049568
	.long	16376
	.long	0
	.align 16
.LC17:
	.long	2233819729
	.long	2862203325
	.long	16371
	.long	0
	.align 16
.LC18:
	.long	1861065117
	.long	3723646513
	.long	49144
	.long	0
	.align 16
.LC19:
	.long	3577423671
	.long	2969085311
	.long	49147
	.long	0
	.align 16
.LC20:
	.long	4268183480
	.long	3888119027
	.long	49146
	.long	0
	.align 16
.LC21:
	.long	1377704520
	.long	3004979304
	.long	49144
	.long	0
	.align 16
.LC22:
	.long	1844207903
	.long	3499739242
	.long	49140
	.long	0
	.align 16
.LC23:
	.long	2303181134
	.long	3174156551
	.long	49135
	.long	0
	.align 16
.LC24:
	.long	4242664012
	.long	3268575361
	.long	16364
	.long	0
	.align 16
.LC25:
	.long	2114999344
	.long	3677147280
	.long	49128
	.long	0
	.align 16
.LC26:
	.long	3350118586
	.long	3859675017
	.long	16384
	.long	0
	.align 16
.LC27:
	.long	3518223728
	.long	3319699958
	.long	16384
	.long	0
	.align 16
.LC28:
	.long	2908502196
	.long	4019510568
	.long	16382
	.long	0
	.align 16
.LC29:
	.long	1484909370
	.long	3884674889
	.long	16379
	.long	0
	.align 16
.LC30:
	.long	1324476051
	.long	2967591198
	.long	16375
	.long	0
	.align 16
.LC31:
	.long	1395261448
	.long	2355099620
	.long	49144
	.long	0
	.align 16
.LC32:
	.long	2763705886
	.long	4100322310
	.long	49147
	.long	0
	.align 16
.LC33:
	.long	1449035931
	.long	2630737374
	.long	49148
	.long	0
	.align 16
.LC34:
	.long	3568535833
	.long	3828995741
	.long	49146
	.long	0
	.align 16
.LC35:
	.long	1556030458
	.long	4063186589
	.long	49143
	.long	0
	.align 16
.LC36:
	.long	3913278194
	.long	3257787532
	.long	49139
	.long	0
	.align 16
.LC37:
	.long	3891166514
	.long	2880168470
	.long	16369
	.long	0
	.align 16
.LC38:
	.long	2591552814
	.long	3240189149
	.long	49133
	.long	0
	.align 16
.LC39:
	.long	1036566320
	.long	3257282737
	.long	16385
	.long	0
	.align 16
.LC40:
	.long	1065578237
	.long	2380735921
	.long	16386
	.long	0
	.align 16
.LC41:
	.long	1963154603
	.long	2436969554
	.long	16385
	.long	0
	.align 16
.LC42:
	.long	1899332366
	.long	3931544220
	.long	16382
	.long	0
	.align 16
.LC43:
	.long	2849651558
	.long	2459508495
	.long	16379
	.long	0
	.align 16
.LC44:
	.long	3266641771
	.long	2944175061
	.long	49143
	.long	0
	.align 16
.LC45:
	.long	914114068
	.long	2673602224
	.long	49148
	.long	0
	.align 16
.LC46:
	.long	3545020396
	.long	3224123411
	.long	49149
	.long	0
	.align 16
.LC47:
	.long	2299057713
	.long	4248491335
	.long	49148
	.long	0
	.align 16
.LC48:
	.long	3404360279
	.long	3939153775
	.long	49146
	.long	0
	.align 16
.LC49:
	.long	2022679223
	.long	2660046906
	.long	49143
	.long	0
	.align 16
.LC50:
	.long	2973645953
	.long	3814080038
	.long	16373
	.long	0
	.align 16
.LC51:
	.long	3550752295
	.long	4290822966
	.long	49137
	.long	0
	.align 16
.LC54:
	.long	1030886622
	.long	4117298295
	.long	16383
	.long	0
	.align 16
.LC55:
	.long	1681978543
	.long	3171937722
	.long	16382
	.long	0
	.align 16
.LC56:
	.long	670430183
	.long	3290776729
	.long	16379
	.long	0
	.align 16
.LC57:
	.long	3862443752
	.long	2803245818
	.long	16375
	.long	0
	.align 16
.LC58:
	.long	2867454409
	.long	4209238657
	.long	16369
	.long	0
	.align 16
.LC59:
	.long	2509297529
	.long	2701294790
	.long	16363
	.long	0
	.align 16
.LC60:
	.long	966160638
	.long	4214009921
	.long	16378
	.long	0
	.align 16
.LC61:
	.long	2650736698
	.long	2667669490
	.long	16378
	.long	0
	.align 16
.LC62:
	.long	2667233750
	.long	3318534109
	.long	16375
	.long	0
	.align 16
.LC63:
	.long	1067578272
	.long	3063417841
	.long	16371
	.long	0
	.align 16
.LC64:
	.long	3475256934
	.long	2391657843
	.long	16366
	.long	0
	.align 16
.LC65:
	.long	2687691520
	.long	3132050450
	.long	16359
	.long	0
	.align 16
.LC66:
	.long	3758459231
	.long	2362180589
	.long	16355
	.long	0
	.align 16
.LC67:
	.long	3263592355
	.long	2768180378
	.long	16351
	.long	0
	.align 16
.LC68:
	.long	3057604382
	.long	3712180387
	.long	16384
	.long	0
	.align 16
.LC69:
	.long	2897191675
	.long	2746121582
	.long	16384
	.long	0
	.align 16
.LC70:
	.long	1900458522
	.long	2821621525
	.long	16382
	.long	0
	.align 16
.LC71:
	.long	3874101740
	.long	2417626111
	.long	16379
	.long	0
	.align 16
.LC72:
	.long	1206748965
	.long	3673150864
	.long	16374
	.long	0
	.align 16
.LC73:
	.long	4082291246
	.long	2383239230
	.long	16369
	.long	0
	.align 16
.LC74:
	.long	1680610119
	.long	2562875764
	.long	16379
	.long	0
	.align 16
.LC75:
	.long	1492206178
	.long	3681916786
	.long	16379
	.long	0
	.align 16
.LC76:
	.long	3371462270
	.long	2509206796
	.long	16378
	.long	0
	.align 16
.LC77:
	.long	3298018621
	.long	2474159684
	.long	16375
	.long	0
	.align 16
.LC78:
	.long	1954119891
	.long	4047982661
	.long	16370
	.long	0
	.align 16
.LC79:
	.long	1044387761
	.long	2733399395
	.long	16365
	.long	0
	.align 16
.LC80:
	.long	1462096591
	.long	4188956200
	.long	16361
	.long	0
	.align 16
.LC81:
	.long	1413472815
	.long	2454466521
	.long	16358
	.long	0
	.align 16
.LC82:
	.long	3763611966
	.long	3217316263
	.long	16385
	.long	0
	.align 16
.LC83:
	.long	2935123706
	.long	4241371217
	.long	16385
	.long	0
	.align 16
.LC84:
	.long	572590924
	.long	3906473477
	.long	16384
	.long	0
	.align 16
.LC85:
	.long	1998497647
	.long	2986680054
	.long	16382
	.long	0
	.align 16
.LC86:
	.long	1882814228
	.long	4002678071
	.long	16378
	.long	0
	.align 16
.LC87:
	.long	2028811009
	.long	2252547446
	.long	16374
	.long	0
	.align 16
.LC88:
	.long	2332252542
	.long	2941396796
	.long	16379
	.long	0
	.align 16
.LC89:
	.long	1882587370
	.long	2180636787
	.long	16381
	.long	0
	.align 16
.LC90:
	.long	4236565267
	.long	2956816322
	.long	16380
	.long	0
	.align 16
.LC91:
	.long	2178328380
	.long	2794388182
	.long	16378
	.long	0
	.align 16
.LC92:
	.long	4072108056
	.long	4224416128
	.long	16374
	.long	0
	.align 16
.LC93:
	.long	3462877219
	.long	2544274282
	.long	16370
	.long	0
	.align 16
.LC94:
	.long	1815427447
	.long	3361650146
	.long	16367
	.long	0
	.align 16
.LC95:
	.long	2418060855
	.long	3939432793
	.long	16363
	.long	0
	.align 16
.LC96:
	.long	806457184
	.long	2714532256
	.long	16386
	.long	0
	.align 16
.LC97:
	.long	2025377303
	.long	3061926902
	.long	16387
	.long	0
	.align 16
.LC98:
	.long	1846504679
	.long	2409624488
	.long	16387
	.long	0
	.align 16
.LC99:
	.long	2074909693
	.long	3118001801
	.long	16385
	.long	0
	.align 16
.LC100:
	.long	3473013690
	.long	3480763011
	.long	16382
	.long	0
	.align 16
.LC101:
	.long	1524221056
	.long	3194805824
	.long	16378
	.long	0
	.align 16
.LC102:
	.long	1083496575
	.long	3246217423
	.long	16379
	.long	0
	.align 16
.LC103:
	.long	2128838662
	.long	2332971259
	.long	16382
	.long	0
	.align 16
.LC104:
	.long	715111442
	.long	2989531268
	.long	16382
	.long	0
	.align 16
.LC105:
	.long	493580686
	.long	2587894372
	.long	16381
	.long	0
	.align 16
.LC106:
	.long	3626950127
	.long	3455097625
	.long	16378
	.long	0
	.align 16
.LC107:
	.long	3369384220
	.long	3528999288
	.long	16374
	.long	0
	.align 16
.LC108:
	.long	2702693320
	.long	3786973089
	.long	16372
	.long	0
	.align 16
.LC109:
	.long	3054469822
	.long	2218911012
	.long	16369
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC110:
	.long	1040187392
	.section	.rodata.cst16
	.align 16
.LC112:
	.long	349923469
	.long	2423175810
	.long	16382
	.long	0
	.align 16
.LC113:
	.long	1164966429
	.long	2310419687
	.long	32760
	.long	0
	.section	.rodata.cst4
	.align 4
.LC114:
	.long	1048576000
	.section	.rodata.cst16
	.align 16
.LC115:
	.long	2128688044
	.long	4144319464
	.long	16378
	.long	0
	.align 16
.LC116:
	.long	1036925594
	.long	4252072451
	.long	16387
	.long	0
	.align 16
.LC117:
	.long	1525165169
	.long	3676227964
	.long	16395
	.long	0
	.align 16
.LC118:
	.long	1741391787
	.long	2724683324
	.long	16402
	.long	0
	.align 16
.LC119:
	.long	2390577403
	.long	2743793198
	.long	16408
	.long	0
	.align 16
.LC120:
	.long	1513219242
	.long	2616944652
	.long	16392
	.long	0
	.align 16
.LC121:
	.long	2566061102
	.long	3152477633
	.long	16400
	.long	0
	.align 16
.LC122:
	.long	3462956539
	.long	2153171250
	.long	16408
	.long	0
	.align 16
.LC123:
	.long	2390577414
	.long	2743793198
	.long	16414
	.long	0
	.section	.rodata.cst4
	.align 4
.LC124:
	.long	1056964608
	.align 4
.LC125:
	.long	2139095040
	.section	.rodata.cst16
	.align 16
.LC126:
	.long	1313084714
	.long	2734261102
	.long	16382
	.long	0
	.align 16
.LC127:
	.long	1585509444
	.long	2535896270
	.long	16379
	.long	0
	.align 16
.LC128:
	.long	582681986
	.long	4217474829
	.long	16392
	.long	0
	.align 16
.LC129:
	.long	774844704
	.long	2818608404
	.long	16401
	.long	0
	.align 16
.LC130:
	.long	31312559
	.long	2610693843
	.long	16408
	.long	0
	.align 16
.LC131:
	.long	649416246
	.long	3712852142
	.long	16413
	.long	0
	.align 16
.LC132:
	.long	801522177
	.long	3150240762
	.long	16417
	.long	0
	.align 16
.LC133:
	.long	1887483225
	.long	2637280767
	.long	16416
	.long	0
	.align 16
.LC134:
	.long	1358733761
	.long	3937548871
	.long	16391
	.long	0
	.align 16
.LC135:
	.long	3493401596
	.long	4058014994
	.long	16399
	.long	0
	.align 16
.LC136:
	.long	2602684014
	.long	2798179354
	.long	16407
	.long	0
	.align 16
.LC137:
	.long	1396376149
	.long	2492593075
	.long	16414
	.long	0
	.align 16
.LC138:
	.long	3494959912
	.long	2233339505
	.long	16420
	.long	0
