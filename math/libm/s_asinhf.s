	.text
	.p2align 4,,15
	.globl	__asinhf
	.type	__asinhf, @function
__asinhf:
	movd	%xmm0, %eax
	subq	$40, %rsp
	movaps	%xmm0, %xmm1
	andl	$2147483647, %eax
	cmpl	$939524095, %eax
	jle	.L17
	cmpl	$1191182336, %eax
	jg	.L7
	movaps	%xmm0, %xmm5
	movss	.LC3(%rip), %xmm4
	movss	.LC0(%rip), %xmm6
	movaps	%xmm0, %xmm7
	mulss	%xmm0, %xmm5
	cmpl	$1073741824, %eax
	andps	%xmm6, %xmm7
	movaps	%xmm5, %xmm2
	addss	%xmm4, %xmm2
	sqrtss	%xmm2, %xmm2
	jle	.L9
	addss	%xmm7, %xmm2
	movss	%xmm0, (%rsp)
	addss	%xmm7, %xmm7
	movaps	%xmm6, 16(%rsp)
	divss	%xmm2, %xmm4
	movaps	%xmm7, %xmm0
	addss	%xmm4, %xmm0
	call	__ieee754_logf@PLT
	movaps	16(%rsp), %xmm6
	movss	(%rsp), %xmm1
.L11:
	movaps	%xmm1, %xmm3
	andps	%xmm6, %xmm0
	andps	.LC5(%rip), %xmm3
	orps	%xmm3, %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movaps	%xmm0, %xmm7
	movss	.LC0(%rip), %xmm6
	movss	.LC1(%rip), %xmm0
	andps	%xmm6, %xmm7
	ucomiss	%xmm7, %xmm0
	jbe	.L3
	movaps	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
.L3:
	movss	.LC2(%rip), %xmm2
	addss	%xmm1, %xmm2
	movss	.LC3(%rip), %xmm4
	movaps	%xmm1, %xmm0
	ucomiss	%xmm4, %xmm2
	ja	.L1
	movaps	%xmm1, %xmm5
	mulss	%xmm1, %xmm5
	movaps	%xmm5, %xmm3
	addss	%xmm4, %xmm3
	sqrtss	%xmm3, %xmm2
	.p2align 4,,10
	.p2align 3
.L9:
	addss	%xmm4, %xmm2
	movss	%xmm1, (%rsp)
	movaps	%xmm6, 16(%rsp)
	divss	%xmm2, %xmm5
	addss	%xmm7, %xmm5
	movaps	%xmm5, %xmm0
	call	__log1pf@PLT
	movaps	16(%rsp), %xmm6
	movss	(%rsp), %xmm1
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$2139095039, %eax
	jle	.L10
	addss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movss	.LC0(%rip), %xmm6
	movss	%xmm0, 16(%rsp)
	andps	%xmm6, %xmm0
	movaps	%xmm6, (%rsp)
	call	__ieee754_logf@PLT
	movaps	(%rsp), %xmm6
	addss	.LC4(%rip), %xmm0
	movss	16(%rsp), %xmm1
	jmp	.L11
	.size	__asinhf, .-__asinhf
	.weak	asinhf32
	.set	asinhf32,__asinhf
	.weak	asinhf
	.set	asinhf,__asinhf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	8388608
	.align 4
.LC2:
	.long	1900671690
	.align 4
.LC3:
	.long	1065353216
	.align 4
.LC4:
	.long	1060205080
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
