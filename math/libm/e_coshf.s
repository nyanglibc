	.text
	.p2align 4,,15
	.globl	__ieee754_coshf
	.type	__ieee754_coshf, @function
__ieee754_coshf:
	movd	%xmm0, %eax
	subq	$24, %rsp
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	cmpl	$1102053375, %eax
	jg	.L2
	cmpl	$1051816471, %eax
	jg	.L3
	cmpl	$603979775, %eax
	movss	.LC0(%rip), %xmm1
	jg	.L11
	movaps	%xmm1, %xmm0
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1118925183, %eax
	jle	.L12
	cmpl	$1119016188, %eax
	jle	.L13
	cmpl	$2139095039, %eax
	jg	.L14
	movss	.LC3(%rip), %xmm1
	addq	$24, %rsp
	mulss	%xmm1, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	andl	$2147483647, %edx
	movl	%edx, 8(%rsp)
	movss	8(%rsp), %xmm0
	call	__ieee754_expf@PLT
	movss	.LC2(%rip), %xmm1
	addq	$24, %rsp
	mulss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	andl	$2147483647, %edx
	movl	%edx, 8(%rsp)
	movss	8(%rsp), %xmm0
	call	__ieee754_expf@PLT
	movaps	%xmm0, %xmm1
	movss	.LC2(%rip), %xmm2
	addq	$24, %rsp
	mulss	%xmm2, %xmm1
	divss	%xmm0, %xmm2
	addss	%xmm2, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movss	%xmm0, 8(%rsp)
	movss	8(%rsp), %xmm1
	addq	$24, %rsp
	mulss	%xmm1, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	andl	$2147483647, %edx
	movss	%xmm1, 12(%rsp)
	movl	%edx, 8(%rsp)
	movss	8(%rsp), %xmm0
	call	__expm1f@PLT
	movaps	%xmm0, %xmm2
	movss	12(%rsp), %xmm1
	addq	$24, %rsp
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm0
	addss	%xmm0, %xmm0
	divss	%xmm0, %xmm2
	addss	%xmm2, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movss	%xmm0, 8(%rsp)
	movss	.LC2(%rip), %xmm0
	movss	8(%rsp), %xmm3
	andps	.LC1(%rip), %xmm3
	mulss	%xmm3, %xmm0
	movss	%xmm3, 8(%rsp)
	call	__ieee754_expf@PLT
	movss	.LC2(%rip), %xmm1
	addq	$24, %rsp
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.size	__ieee754_coshf, .-__ieee754_coshf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC2:
	.long	1056964608
	.align 4
.LC3:
	.long	1900671690
