	.text
	.p2align 4,,15
	.globl	__nextafterf
	.type	__nextafterf, @function
__nextafterf:
	movd	%xmm1, %esi
	movd	%xmm0, %eax
	movd	%xmm0, %ecx
	movd	%xmm1, %edx
	andl	$2147483647, %esi
	andl	$2147483647, %eax
	cmpl	$2139095040, %esi
	jg	.L15
	cmpl	$2139095040, %eax
	jg	.L15
	ucomiss	%xmm1, %xmm0
	jp	.L16
	je	.L1
.L16:
	testl	%eax, %eax
	je	.L20
	testl	%ecx, %ecx
	js	.L7
	leal	-1(%rcx), %esi
	leal	1(%rcx), %eax
	cmpl	%edx, %ecx
	cmovg	%esi, %eax
.L9:
	movl	%eax, %edx
	andl	$2139095040, %edx
	cmpl	$2139095040, %edx
	jne	.L12
	addss	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
.L13:
	movl	%eax, -4(%rsp)
	movss	-4(%rsp), %xmm1
.L1:
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	addss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	andl	$-2147483648, %edx
	orl	$1, %edx
	movl	%edx, -4(%rsp)
	movss	-4(%rsp), %xmm1
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	cmpl	$8388607, %edx
	jg	.L13
	mulss	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%edx, %edx
	jns	.L17
	cmpl	%edx, %ecx
	leal	1(%rcx), %eax
	setg	%dl
	testb	%dl, %dl
	je	.L9
.L17:
	leal	-1(%rcx), %eax
	jmp	.L9
	.size	__nextafterf, .-__nextafterf
	.weak	nextafterf32
	.set	nextafterf32,__nextafterf
	.weak	nextafterf
	.set	nextafterf,__nextafterf
