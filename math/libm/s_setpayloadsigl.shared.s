	.text
	.p2align 4,,15
	.globl	__setpayloadsigl
	.type	__setpayloadsigl, @function
__setpayloadsigl:
	movq	16(%rsp), %rcx
	leal	-16383(%rcx), %eax
	cmpw	$61, %ax
	ja	.L5
	movq	8(%rsp), %rax
	movzwl	%cx, %edx
	movl	$16446, %esi
	subl	%edx, %esi
	movq	%rax, %r9
	shrq	$32, %r9
	cmpl	$31, %esi
	jg	.L4
	movl	$-1, %r8d
	movl	%esi, %ecx
	sall	%cl, %r8d
	notl	%r8d
	testl	%eax, %r8d
	je	.L12
.L5:
	xorl	%ecx, %ecx
	movq	$0, -24(%rsp)
	movl	$1, %eax
	movw	%cx, -16(%rsp)
	fldt	-24(%rsp)
	fstpt	(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	testl	%eax, %eax
	jne	.L5
	movl	$16414, %ecx
	movl	%r9d, %eax
	subl	%edx, %ecx
	movl	$-1, %edx
	sall	%cl, %edx
	shrl	%cl, %eax
	movl	$-2147483648, %ecx
	notl	%edx
	testl	%r9d, %edx
	jne	.L5
.L8:
	movl	$32767, %edx
	movl	%eax, -24(%rsp)
	movl	%ecx, -20(%rsp)
	movw	%dx, -16(%rsp)
	xorl	%eax, %eax
	fldt	-24(%rsp)
	fstpt	(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leal	-16414(%rdx), %ecx
	movl	%r9d, %edx
	sall	%cl, %edx
	movl	%esi, %ecx
	shrl	%cl, %r9d
	shrl	%cl, %eax
	movl	%r9d, %ecx
	orl	%edx, %eax
	orl	$-2147483648, %ecx
	jmp	.L8
	.size	__setpayloadsigl, .-__setpayloadsigl
	.weak	setpayloadsigf64x
	.set	setpayloadsigf64x,__setpayloadsigl
	.weak	setpayloadsigl
	.set	setpayloadsigl,__setpayloadsigl
