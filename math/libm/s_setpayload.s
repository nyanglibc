	.text
	.p2align 4,,15
	.globl	__setpayload
	.type	__setpayload, @function
__setpayload:
	movq	%xmm0, %rdx
	movq	%xmm0, %rax
	shrq	$52, %rdx
	cmpq	$1073, %rdx
	ja	.L2
	cmpl	$1022, %edx
	jg	.L8
	testq	%rax, %rax
	je	.L8
.L2:
	movq	$0x000000000, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1075, %ecx
	subl	%edx, %ecx
	movq	$-1, %rdx
	salq	%cl, %rdx
	notq	%rdx
	testq	%rax, %rdx
	jne	.L2
	testq	%rax, %rax
	movq	.LC0(%rip), %rdx
	je	.L6
	movabsq	$4503599627370495, %rdx
	andq	%rdx, %rax
	addq	$1, %rdx
	orq	%rdx, %rax
	movabsq	$9221120237041090560, %rdx
	shrq	%cl, %rax
	orq	%rax, %rdx
.L6:
	movq	%rdx, (%rdi)
	xorl	%eax, %eax
	ret
	.size	__setpayload, .-__setpayload
	.weak	setpayloadf32x
	.set	setpayloadf32x,__setpayload
	.weak	setpayloadf64
	.set	setpayloadf64,__setpayload
	.weak	setpayload
	.set	setpayload,__setpayload
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
