	.text
#APP
	.symver __ieee754_atanh,__atanh_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_atanh
	.type	__ieee754_atanh, @function
__ieee754_atanh:
	subq	$40, %rsp
	movapd	%xmm0, %xmm1
	movq	.LC0(%rip), %xmm3
	movsd	.LC1(%rip), %xmm7
	movapd	%xmm0, %xmm2
	andpd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm7
	jbe	.L2
	movsd	.LC2(%rip), %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.L17
	movapd	%xmm1, %xmm5
	movapd	%xmm1, %xmm0
	movsd	.LC5(%rip), %xmm4
	addsd	%xmm1, %xmm5
	movsd	%xmm2, (%rsp)
	subsd	%xmm1, %xmm4
	movaps	%xmm3, 16(%rsp)
	mulsd	%xmm5, %xmm0
	divsd	%xmm4, %xmm0
	addsd	%xmm5, %xmm0
	call	__log1p@PLT
	movsd	(%rsp), %xmm2
	mulsd	.LC1(%rip), %xmm0
	movapd	16(%rsp), %xmm3
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L2:
	movsd	.LC5(%rip), %xmm4
	movaps	%xmm3, (%rsp)
	ucomisd	%xmm1, %xmm4
	jbe	.L8
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm1, %xmm0
	subsd	%xmm1, %xmm4
	addsd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	call	__log1p@PLT
	mulsd	.LC1(%rip), %xmm0
	movsd	16(%rsp), %xmm2
	movapd	(%rsp), %xmm3
.L7:
	movapd	%xmm2, %xmm6
	andpd	%xmm3, %xmm0
	andpd	.LC7(%rip), %xmm6
	orpd	%xmm6, %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	.LC3(%rip), %xmm0
	addsd	%xmm2, %xmm0
	movsd	.LC4(%rip), %xmm0
	ucomisd	%xmm1, %xmm0
	movapd	%xmm2, %xmm0
	jbe	.L1
	mulsd	%xmm2, %xmm0
	movapd	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	ucomisd	%xmm4, %xmm1
	jbe	.L9
	subsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm2
	movapd	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	divsd	.LC6(%rip), %xmm2
	movapd	%xmm2, %xmm0
	jmp	.L1
	.size	__ieee754_atanh, .-__ieee754_atanh
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1071644672
	.align 8
.LC2:
	.long	0
	.long	1043333120
	.align 8
.LC3:
	.long	2281731484
	.long	2117592124
	.align 8
.LC4:
	.long	0
	.long	1048576
	.align 8
.LC5:
	.long	0
	.long	1072693248
	.align 8
.LC6:
	.long	0
	.long	0
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
