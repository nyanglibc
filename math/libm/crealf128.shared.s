	.text
	.p2align 4,,15
	.globl	__crealf128
	.type	__crealf128, @function
__crealf128:
	movdqa	8(%rsp), %xmm0
	ret
	.size	__crealf128, .-__crealf128
	.weak	crealf128
	.set	crealf128,__crealf128
