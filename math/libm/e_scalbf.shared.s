	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __ieee754_scalbf,__scalbf_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.type	invalid_fn, @function
invalid_fn:
	movaps	%xmm1, %xmm5
	movss	.LC0(%rip), %xmm3
	movaps	%xmm1, %xmm2
	andps	%xmm3, %xmm5
	movss	.LC1(%rip), %xmm4
	ucomiss	%xmm5, %xmm4
	jbe	.L2
	andnps	%xmm1, %xmm3
	orps	%xmm3, %xmm4
	addss	%xmm4, %xmm2
	subss	%xmm4, %xmm2
	orps	%xmm3, %xmm2
.L2:
	ucomiss	%xmm1, %xmm2
	jp	.L8
	jne	.L8
	ucomiss	.LC2(%rip), %xmm1
	ja	.L14
	movl	$-65000, %edi
	jmp	__scalbnf@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$65000, %edi
	jmp	__scalbnf@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	subss	%xmm1, %xmm1
	movaps	%xmm1, %xmm0
	divss	%xmm1, %xmm0
	ret
	.size	invalid_fn, .-invalid_fn
	.p2align 4,,15
	.globl	__ieee754_scalbf
	.type	__ieee754_scalbf, @function
__ieee754_scalbf:
	ucomiss	%xmm0, %xmm0
	jp	.L20
	movaps	%xmm1, %xmm2
	movss	.LC3(%rip), %xmm3
	andps	.LC0(%rip), %xmm2
	ucomiss	%xmm2, %xmm3
	jb	.L30
	ucomiss	.LC6(%rip), %xmm2
	jnb	.L22
	cvttss2si	%xmm1, %edi
	pxor	%xmm2, %xmm2
	cvtsi2ss	%edi, %xmm2
	ucomiss	%xmm1, %xmm2
	jp	.L22
	jne	.L22
	jmp	__scalbnf@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	ucomiss	%xmm1, %xmm1
	jp	.L20
	pxor	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm1
	jbe	.L31
.L20:
	mulss	%xmm1, %xmm0
	movaps	%xmm0, %xmm2
.L15:
	movaps	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	jmp	invalid_fn
	.p2align 4,,10
	.p2align 3
.L31:
	ucomiss	%xmm2, %xmm0
	jp	.L27
	movaps	%xmm0, %xmm2
	je	.L15
.L27:
	xorps	.LC5(%rip), %xmm1
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm2
	jmp	.L15
	.size	__ieee754_scalbf, .-__ieee754_scalbf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1258291200
	.align 4
.LC2:
	.long	1199433728
	.align 4
.LC3:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	1325400064
