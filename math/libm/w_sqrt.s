	.text
	.p2align 4,,15
	.globl	__sqrt
	.type	__sqrt, @function
__sqrt:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	ja	.L4
	jmp	__ieee754_sqrt@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_sqrt@PLT
	.size	__sqrt, .-__sqrt
	.weak	sqrtf32x
	.set	sqrtf32x,__sqrt
	.weak	sqrtf64
	.set	sqrtf64,__sqrt
	.weak	sqrt
	.set	sqrt,__sqrt
