	.text
	.p2align 4,,15
	.globl	fesetexceptflag
	.type	fesetexceptflag, @function
fesetexceptflag:
	movl	%esi, %eax
	movzwl	(%rdi), %edx
	andl	$61, %eax
#APP
# 33 "../sysdeps/x86_64/fpu/fsetexcptflg.c" 1
	fnstenv -40(%rsp)
# 0 "" 2
#NO_APP
	notl	%eax
	andw	-36(%rsp), %ax
	andl	%esi, %edx
	andl	$61, %edx
	orl	%edx, %eax
	movw	%ax, -36(%rsp)
#APP
# 41 "../sysdeps/x86_64/fpu/fsetexcptflg.c" 1
	fldenv -40(%rsp)
# 0 "" 2
#NO_APP
	movl	%esi, %eax
	movzwl	(%rdi), %edx
	andl	$61, %eax
#APP
# 44 "../sysdeps/x86_64/fpu/fsetexcptflg.c" 1
	stmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	notl	%eax
	andl	-44(%rsp), %eax
	andl	%edx, %esi
	andl	$61, %esi
	orl	%eax, %esi
	movl	%esi, -44(%rsp)
#APP
# 49 "../sysdeps/x86_64/fpu/fsetexcptflg.c" 1
	ldmxcsr -44(%rsp)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.size	fesetexceptflag, .-fesetexceptflag
