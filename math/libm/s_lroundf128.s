	.text
	.globl	__letf2
	.globl	__fixtfdi
	.p2align 4,,15
	.globl	__lroundf128
	.type	__lroundf128, @function
__lroundf128:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rcx
	movq	%rcx, %rdx
	movq	%rcx, %rax
	shrq	$48, %rdx
	sarq	$63, %rax
	andl	$32767, %edx
	movl	%eax, %ebp
	subq	$16383, %rdx
	orl	$1, %ebp
	cmpq	$62, %rdx
	jg	.L3
	movabsq	$281474976710655, %rbx
	andq	%rbx, %rcx
	addq	$1, %rbx
	orq	%rcx, %rbx
	cmpq	$47, %rdx
	jg	.L4
	testq	%rdx, %rdx
	js	.L29
	movl	%edx, %ecx
	movabsq	$140737488355328, %rax
	sarq	%cl, %rax
	movl	$48, %ecx
	addq	%rax, %rbx
	subl	%edx, %ecx
	shrq	%cl, %rbx
.L7:
	movslq	%ebp, %rax
	addq	$24, %rsp
	imulq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L30
	movdqa	(%rsp), %xmm0
	call	__fixtfdi@PLT
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leal	-48(%rdx), %esi
	movabsq	$-9223372036854775808, %rdi
	movq	%rdi, %rax
	movl	%esi, %ecx
	shrq	%cl, %rax
	movq	(%rsp), %rcx
	addq	%rcx, %rax
	setc	%cl
	movzbl	%cl, %ecx
	addq	%rcx, %rbx
	cmpq	$48, %rdx
	je	.L7
	movl	$112, %ecx
	subl	%edx, %ecx
	shrq	%cl, %rax
	movl	%esi, %ecx
	salq	%cl, %rbx
	orq	%rax, %rbx
	cmpl	$1, %ebp
	jne	.L7
	cmpq	%rdi, %rbx
	jne	.L7
	movl	$1, %edi
	call	feraiseexcept@PLT
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L29:
	movslq	%ebp, %rax
	cmpq	$-1, %rdx
	movl	$0, %ebx
	cmovne	%rbx, %rax
	jmp	.L1
.L30:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movabsq	$-9223372036854775808, %rax
	jmp	.L1
	.size	__lroundf128, .-__lroundf128
	.weak	lroundf128
	.set	lroundf128,__lroundf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	65536
	.long	0
	.long	-1069678592
