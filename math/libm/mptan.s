	.text
	.p2align 4,,15
	.globl	__mptan
	.type	__mptan, @function
__mptan:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebx
	subq	$1016, %rsp
	movq	%rsp, %r13
	leaq	672(%rsp), %rbp
	leaq	336(%rsp), %r12
	movq	%r13, %rdi
	call	__mpranred@PLT
	movl	%ebx, %ecx
	movl	%eax, %r15d
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__c32@PLT
	andl	$1, %r15d
	movl	%ebx, %ecx
	movq	%r14, %rdx
	jne	.L6
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__dvd@PLT
.L1:
	addq	$1016, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__dvd@PLT
	negq	8(%r14)
	jmp	.L1
	.size	__mptan, .-__mptan
