	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__lttf2
	.globl	__subtf3
	.p2align 4,,15
	.globl	__csinf128
	.type	__csinf128, @function
__csinf128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movdqa	160(%rsp), %xmm3
	pand	.LC4(%rip), %xmm3
	movdqa	176(%rsp), %xmm5
	movdqa	.LC4(%rip), %xmm4
	movaps	160(%rsp), %xmm6
	pand	%xmm5, %xmm4
	movdqa	%xmm3, %xmm1
	movmskps	%xmm6, %ebp
	movdqa	%xmm3, %xmm0
	movaps	%xmm5, 16(%rsp)
	andl	$8, %ebp
	movaps	%xmm4, (%rsp)
	movaps	%xmm3, 32(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC5(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L87
	movdqa	(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L50
.L85:
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L88
	movdqa	32(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__subtf3@PLT
	movdqa	.LC3(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
.L25:
	movq	%rbx, %rax
	movdqa	(%rsp), %xmm7
	movdqa	16(%rsp), %xmm5
	movaps	%xmm7, (%rbx)
	movaps	%xmm5, 16(%rbx)
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movdqa	.LC6(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	160(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L89
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L90
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L34
	testl	%ebp, %ebp
	jne	.L91
	movdqa	.LC1(%rip), %xmm1
.L26:
	pxor	%xmm0, %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L88:
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L41
	pxor	%xmm1, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L41
	movdqa	32(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	(%rsp), %xmm4
	movdqa	%xmm4, %xmm1
	movdqa	%xmm4, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L50
.L82:
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L33
.L34:
	movdqa	.LC8(%rip), %xmm1
	movdqa	.LC9(%rip), %xmm0
	call	__multf3@PLT
	call	__fixtfsi@PLT
	movdqa	.LC6(%rip), %xmm1
	movl	%eax, %r12d
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L72
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	32(%rsp), %xmm0
	call	__sincosf128@PLT
.L10:
	testl	%ebp, %ebp
	je	.L11
	movdqa	96(%rsp), %xmm0
	pxor	.LC10(%rip), %xmm0
	movaps	%xmm0, 96(%rsp)
.L11:
	movl	%r12d, %edi
	call	__floatsitf@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L73
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movaps	176(%rsp), %xmm6
	movmskps	%xmm6, %eax
	movaps	%xmm0, 32(%rsp)
	testb	$8, %al
	jne	.L14
	movdqa	112(%rsp), %xmm4
	movaps	%xmm4, 64(%rsp)
.L15:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC11(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 96(%rsp)
	movdqa	48(%rsp), %xmm2
	movdqa	64(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L16
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 112(%rsp)
.L16:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L74
	movdqa	.LC5(%rip), %xmm1
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
.L20:
	movdqa	(%rsp), %xmm0
	pand	.LC4(%rip), %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L21
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L21:
	movdqa	16(%rsp), %xmm0
	pand	.LC4(%rip), %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L25
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L73:
	movdqa	176(%rsp), %xmm0
	call	__ieee754_coshf128@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	176(%rsp), %xmm0
	call	__ieee754_sinhf128@PLT
	movdqa	112(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	112(%rsp), %xmm0
	pxor	.LC10(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L89:
	movdqa	(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L82
.L50:
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L74:
	movdqa	(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	96(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	112(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L85
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L72:
	movdqa	32(%rsp), %xmm5
	movdqa	.LC1(%rip), %xmm0
	movaps	%xmm5, 96(%rsp)
	movaps	%xmm0, 112(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L33:
	movdqa	.LC6(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L75
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	32(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	96(%rsp), %xmm6
	movdqa	112(%rsp), %xmm5
	movaps	%xmm6, 32(%rsp)
	movaps	%xmm5, 16(%rsp)
.L27:
	movdqa	32(%rsp), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC3(%rip), %xmm0
	call	__copysignf128@PLT
	testl	%ebp, %ebp
	je	.L29
	movdqa	(%rsp), %xmm6
	pxor	.LC10(%rip), %xmm6
	movaps	%xmm6, (%rsp)
.L29:
	movaps	%xmm0, 16(%rsp)
	movaps	176(%rsp), %xmm5
	movmskps	%xmm5, %eax
	testb	$8, %al
	je	.L25
	pxor	.LC10(%rip), %xmm0
	movaps	%xmm0, 16(%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movdqa	.LC2(%rip), %xmm4
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm4, (%rsp)
	jmp	.L25
.L91:
	movdqa	.LC0(%rip), %xmm1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L90:
	testl	%ebp, %ebp
	je	.L46
	movdqa	.LC0(%rip), %xmm1
.L31:
	pxor	%xmm0, %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC2(%rip), %xmm0
.L32:
	movaps	%xmm0, 16(%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L46:
	movdqa	.LC1(%rip), %xmm1
	jmp	.L31
.L75:
	movdqa	.LC1(%rip), %xmm5
	movaps	%xmm5, 16(%rsp)
	jmp	.L27
	.size	__csinf128, .-__csinf128
	.weak	csinf128
	.set	csinf128,__csinf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC5:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1074593784
	.align 16
.LC9:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	1073610752
