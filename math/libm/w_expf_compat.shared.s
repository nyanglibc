	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __expf_compat,expf@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__expf_compat
	.type	__expf_compat, @function
__expf_compat:
	subq	$24, %rsp
	movss	%xmm0, 12(%rsp)
	call	__ieee754_expf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm4
	movss	12(%rsp), %xmm2
	ucomiss	%xmm4, %xmm3
	jb	.L2
	ucomiss	.LC2(%rip), %xmm0
	jp	.L1
	je	.L2
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movd	%xmm2, %eax
	xorl	%edi, %edi
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	testl	%eax, %eax
	sets	%dil
	addq	$24, %rsp
	addl	$106, %edi
	jmp	__kernel_standard_f@PLT
	.size	__expf_compat, .-__expf_compat
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	0
