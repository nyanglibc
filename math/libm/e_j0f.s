	.text
	.p2align 4,,15
	.type	pzerof, @function
pzerof:
	movd	%xmm0, %eax
	andl	$2147483647, %eax
	cmpl	$1090519039, %eax
	jg	.L3
	cmpl	$1089936471, %eax
	jg	.L4
	cmpl	$1077336935, %eax
	jg	.L5
	movss	.LC33(%rip), %xmm3
	movss	.LC34(%rip), %xmm2
	movss	.LC35(%rip), %xmm8
	movss	.LC36(%rip), %xmm7
	movss	.LC37(%rip), %xmm1
	movss	.LC38(%rip), %xmm13
	movss	.LC39(%rip), %xmm12
	movss	.LC40(%rip), %xmm11
	movss	.LC41(%rip), %xmm10
	movss	.LC42(%rip), %xmm6
	movss	.LC43(%rip), %xmm9
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movss	.LC11(%rip), %xmm3
	movss	.LC12(%rip), %xmm2
	movss	.LC13(%rip), %xmm8
	movss	.LC14(%rip), %xmm7
	movss	.LC15(%rip), %xmm1
	movss	.LC16(%rip), %xmm13
	movss	.LC17(%rip), %xmm12
	movss	.LC18(%rip), %xmm11
	movss	.LC19(%rip), %xmm10
	movss	.LC20(%rip), %xmm6
	movss	.LC21(%rip), %xmm9
.L2:
	mulss	%xmm0, %xmm0
	movss	.LC44(%rip), %xmm4
	movaps	%xmm4, %xmm5
	divss	%xmm0, %xmm5
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm3
	addss	%xmm13, %xmm1
	addss	%xmm3, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm12, %xmm1
	addss	%xmm8, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm11, %xmm1
	addss	%xmm7, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm10, %xmm1
	addss	%xmm6, %xmm2
	mulss	%xmm5, %xmm1
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm1
	addss	%xmm4, %xmm2
	movaps	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movss	.LC0(%rip), %xmm3
	movss	.LC1(%rip), %xmm2
	pxor	%xmm9, %xmm9
	movss	.LC2(%rip), %xmm8
	movss	.LC3(%rip), %xmm7
	movss	.LC4(%rip), %xmm1
	movss	.LC5(%rip), %xmm13
	movss	.LC6(%rip), %xmm12
	movss	.LC7(%rip), %xmm11
	movss	.LC8(%rip), %xmm10
	movss	.LC9(%rip), %xmm6
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	movss	.LC22(%rip), %xmm3
	movss	.LC23(%rip), %xmm2
	movss	.LC24(%rip), %xmm8
	movss	.LC25(%rip), %xmm7
	movss	.LC26(%rip), %xmm1
	movss	.LC27(%rip), %xmm13
	movss	.LC28(%rip), %xmm12
	movss	.LC29(%rip), %xmm11
	movss	.LC30(%rip), %xmm10
	movss	.LC31(%rip), %xmm6
	movss	.LC32(%rip), %xmm9
	jmp	.L2
	.size	pzerof, .-pzerof
	.p2align 4,,15
	.type	qzerof, @function
qzerof:
	movd	%xmm0, %eax
	andl	$2147483647, %eax
	cmpl	$1090519039, %eax
	jg	.L9
	cmpl	$1089936471, %eax
	jg	.L10
	cmpl	$1077336935, %eax
	jg	.L11
	movss	.LC80(%rip), %xmm3
	movss	.LC81(%rip), %xmm9
	movss	.LC82(%rip), %xmm2
	movss	.LC83(%rip), %xmm8
	movss	.LC84(%rip), %xmm7
	movss	.LC85(%rip), %xmm1
	movss	.LC86(%rip), %xmm14
	movss	.LC87(%rip), %xmm13
	movss	.LC88(%rip), %xmm12
	movss	.LC89(%rip), %xmm11
	movss	.LC90(%rip), %xmm6
	movss	.LC91(%rip), %xmm10
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movss	.LC56(%rip), %xmm3
	movss	.LC57(%rip), %xmm9
	movss	.LC58(%rip), %xmm2
	movss	.LC59(%rip), %xmm8
	movss	.LC60(%rip), %xmm7
	movss	.LC61(%rip), %xmm1
	movss	.LC62(%rip), %xmm14
	movss	.LC63(%rip), %xmm13
	movss	.LC64(%rip), %xmm12
	movss	.LC65(%rip), %xmm11
	movss	.LC66(%rip), %xmm6
	movss	.LC67(%rip), %xmm10
.L8:
	movaps	%xmm0, %xmm5
	movss	.LC44(%rip), %xmm4
	movaps	%xmm4, %xmm15
	mulss	%xmm0, %xmm5
	divss	%xmm5, %xmm15
	mulss	%xmm15, %xmm3
	mulss	%xmm15, %xmm1
	addss	%xmm9, %xmm3
	addss	%xmm14, %xmm1
	mulss	%xmm15, %xmm3
	mulss	%xmm15, %xmm1
	addss	%xmm3, %xmm2
	addss	%xmm13, %xmm1
	mulss	%xmm15, %xmm2
	mulss	%xmm15, %xmm1
	addss	%xmm8, %xmm2
	addss	%xmm12, %xmm1
	mulss	%xmm15, %xmm2
	mulss	%xmm15, %xmm1
	addss	%xmm7, %xmm2
	addss	%xmm11, %xmm1
	mulss	%xmm15, %xmm2
	mulss	%xmm15, %xmm1
	addss	%xmm6, %xmm2
	addss	%xmm10, %xmm1
	mulss	%xmm15, %xmm2
	addss	%xmm4, %xmm2
	divss	%xmm2, %xmm1
	subss	.LC92(%rip), %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movss	.LC45(%rip), %xmm3
	movss	.LC46(%rip), %xmm9
	pxor	%xmm10, %xmm10
	movss	.LC47(%rip), %xmm2
	movss	.LC48(%rip), %xmm8
	movss	.LC49(%rip), %xmm7
	movss	.LC50(%rip), %xmm1
	movss	.LC51(%rip), %xmm14
	movss	.LC52(%rip), %xmm13
	movss	.LC53(%rip), %xmm12
	movss	.LC54(%rip), %xmm11
	movss	.LC55(%rip), %xmm6
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movss	.LC68(%rip), %xmm3
	movss	.LC69(%rip), %xmm9
	movss	.LC70(%rip), %xmm2
	movss	.LC71(%rip), %xmm8
	movss	.LC72(%rip), %xmm7
	movss	.LC73(%rip), %xmm1
	movss	.LC74(%rip), %xmm14
	movss	.LC75(%rip), %xmm13
	movss	.LC76(%rip), %xmm12
	movss	.LC77(%rip), %xmm11
	movss	.LC78(%rip), %xmm6
	movss	.LC79(%rip), %xmm10
	jmp	.L8
	.size	qzerof, .-qzerof
	.p2align 4,,15
	.globl	__ieee754_j0f
	.type	__ieee754_j0f, @function
__ieee754_j0f:
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %ebx
	subq	$48, %rsp
	cmpl	$2139095039, %ebx
	jg	.L27
	movaps	%xmm0, %xmm3
	cmpl	$1073741823, %ebx
	andps	.LC93(%rip), %xmm3
	jg	.L28
	cmpl	$956301311, %ebx
	jg	.L21
	movss	.LC99(%rip), %xmm0
	addss	%xmm3, %xmm0
	cmpl	$838860799, %ebx
	movss	.LC44(%rip), %xmm0
	jle	.L12
	movss	.LC100(%rip), %xmm1
	mulss	%xmm3, %xmm1
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L27:
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	movss	.LC44(%rip), %xmm0
	divss	%xmm1, %xmm0
.L12:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	mulss	%xmm0, %xmm0
	movss	.LC105(%rip), %xmm4
	movss	.LC101(%rip), %xmm2
	cmpl	$1065353215, %ebx
	movss	.LC44(%rip), %xmm5
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm2
	addss	.LC106(%rip), %xmm4
	addss	.LC102(%rip), %xmm2
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm2
	addss	.LC107(%rip), %xmm4
	subss	.LC103(%rip), %xmm2
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm2
	addss	.LC108(%rip), %xmm4
	addss	.LC104(%rip), %xmm2
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm2
	addss	%xmm5, %xmm4
	divss	%xmm4, %xmm2
	jle	.L29
	mulss	.LC109(%rip), %xmm3
	mulss	%xmm2, %xmm0
	movaps	%xmm3, %xmm1
	addss	%xmm5, %xmm1
	subss	%xmm3, %xmm5
	mulss	%xmm5, %xmm1
	addss	%xmm1, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L28:
	movaps	%xmm3, %xmm0
	leaq	44(%rsp), %rsi
	leaq	40(%rsp), %rdi
	movss	%xmm3, 16(%rsp)
	call	__sincosf@PLT
	movss	40(%rsp), %xmm1
	cmpl	$2130706431, %ebx
	movaps	%xmm1, %xmm6
	movss	%xmm1, 24(%rsp)
	movss	44(%rsp), %xmm2
	subss	%xmm2, %xmm6
	movss	16(%rsp), %xmm3
	movss	%xmm2, 20(%rsp)
	movaps	%xmm3, %xmm0
	movss	%xmm6, 12(%rsp)
	jg	.L16
	addss	%xmm3, %xmm0
	call	__cosf@PLT
	movss	40(%rsp), %xmm4
	mulss	44(%rsp), %xmm4
	movss	16(%rsp), %xmm3
	pxor	%xmm5, %xmm5
	xorps	.LC94(%rip), %xmm0
	movss	20(%rsp), %xmm2
	movss	24(%rsp), %xmm1
	ucomiss	%xmm4, %xmm5
	ja	.L30
	addss	%xmm2, %xmm1
	divss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)
.L19:
	cmpl	$1543503872, %ebx
	sqrtss	%xmm3, %xmm2
	jle	.L20
	mulss	.LC98(%rip), %xmm1
	movaps	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	subss	.LC95(%rip), %xmm0
	movss	%xmm3, 20(%rsp)
	call	__sinf@PLT
	movss	20(%rsp), %xmm3
	movss	%xmm0, 16(%rsp)
	movaps	%xmm3, %xmm0
	call	__cosf@PLT
	movss	.LC96(%rip), %xmm1
	mulss	%xmm0, %xmm1
	movss	20(%rsp), %xmm3
	addss	16(%rsp), %xmm1
	divss	.LC97(%rip), %xmm1
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	movaps	%xmm3, %xmm0
	movss	%xmm2, 28(%rsp)
	movss	%xmm1, 24(%rsp)
	movss	%xmm3, 20(%rsp)
	call	pzerof
	movss	20(%rsp), %xmm3
	movss	%xmm0, 16(%rsp)
	movaps	%xmm3, %xmm0
	call	qzerof
	movss	24(%rsp), %xmm1
	mulss	12(%rsp), %xmm0
	mulss	16(%rsp), %xmm1
	movss	28(%rsp), %xmm2
	subss	%xmm0, %xmm1
	movss	.LC98(%rip), %xmm0
	mulss	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L29:
	subss	.LC100(%rip), %xmm2
	mulss	%xmm2, %xmm0
	addss	%xmm5, %xmm0
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L30:
	movaps	%xmm0, %xmm1
	divss	12(%rsp), %xmm1
	jmp	.L19
	.size	__ieee754_j0f, .-__ieee754_j0f
	.p2align 4,,15
	.globl	__ieee754_y0f
	.type	__ieee754_y0f, @function
__ieee754_y0f:
	pushq	%rbx
	movd	%xmm0, %ebx
	andl	$2147483647, %ebx
	subq	$48, %rsp
	cmpl	$2139095039, %ebx
	jg	.L45
	testl	%ebx, %ebx
	je	.L46
	movd	%xmm0, %eax
	testl	%eax, %eax
	js	.L47
	cmpl	$1073741823, %ebx
	movaps	%xmm0, %xmm2
	jg	.L48
	cmpl	$964689920, %ebx
	jle	.L49
	movaps	%xmm0, %xmm3
	movss	.LC119(%rip), %xmm1
	movl	%eax, 20(%rsp)
	mulss	%xmm0, %xmm3
	movss	.LC113(%rip), %xmm0
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC114(%rip), %xmm0
	addss	.LC120(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	subss	.LC115(%rip), %xmm0
	addss	.LC121(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	addss	.LC116(%rip), %xmm0
	addss	.LC122(%rip), %xmm1
	mulss	%xmm3, %xmm0
	mulss	%xmm3, %xmm1
	subss	.LC117(%rip), %xmm0
	addss	.LC44(%rip), %xmm1
	mulss	%xmm3, %xmm0
	addss	.LC118(%rip), %xmm0
	mulss	%xmm3, %xmm0
	subss	.LC112(%rip), %xmm0
	divss	%xmm1, %xmm0
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_j0f
	movss	20(%rsp), %xmm2
	movss	%xmm0, 16(%rsp)
	movaps	%xmm2, %xmm0
	call	__ieee754_logf@PLT
	mulss	16(%rsp), %xmm0
	mulss	.LC111(%rip), %xmm0
	addss	12(%rsp), %xmm0
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L46:
	movss	.LC110(%rip), %xmm0
	divss	.LC10(%rip), %xmm0
.L31:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movaps	%xmm0, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm0, %xmm1
	movss	.LC44(%rip), %xmm0
	addq	$48, %rsp
	popq	%rbx
	divss	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	pxor	%xmm1, %xmm1
	mulss	%xmm1, %xmm0
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	44(%rsp), %rsi
	leaq	40(%rsp), %rdi
	movss	%xmm0, 12(%rsp)
	call	__sincosf@PLT
	movss	40(%rsp), %xmm0
	cmpl	$2130706431, %ebx
	movaps	%xmm0, %xmm1
	movss	44(%rsp), %xmm3
	subss	%xmm3, %xmm1
	movss	12(%rsp), %xmm2
	addss	%xmm0, %xmm3
	jle	.L50
.L37:
	cmpl	$1543503872, %ebx
	sqrtss	%xmm2, %xmm4
	jle	.L40
	mulss	.LC98(%rip), %xmm1
	movaps	%xmm1, %xmm0
	divss	%xmm4, %xmm0
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L49:
	call	__ieee754_logf@PLT
	mulss	.LC111(%rip), %xmm0
	subss	.LC112(%rip), %xmm0
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L40:
	movaps	%xmm2, %xmm0
	movss	%xmm4, 28(%rsp)
	movss	%xmm3, 24(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	%xmm2, 16(%rsp)
	call	pzerof
	movss	16(%rsp), %xmm2
	movss	%xmm0, 12(%rsp)
	movaps	%xmm2, %xmm0
	call	qzerof
	movss	20(%rsp), %xmm1
	mulss	12(%rsp), %xmm1
	movss	24(%rsp), %xmm3
	mulss	%xmm3, %xmm0
	movss	28(%rsp), %xmm4
	addss	%xmm1, %xmm0
	mulss	.LC98(%rip), %xmm0
	divss	%xmm4, %xmm0
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L50:
	movaps	%xmm2, %xmm0
	movss	%xmm3, 20(%rsp)
	movss	%xmm1, 16(%rsp)
	addss	%xmm2, %xmm0
	call	__cosf@PLT
	movss	40(%rsp), %xmm4
	mulss	44(%rsp), %xmm4
	movss	12(%rsp), %xmm2
	pxor	%xmm5, %xmm5
	xorps	.LC94(%rip), %xmm0
	movss	16(%rsp), %xmm1
	movss	20(%rsp), %xmm3
	ucomiss	%xmm4, %xmm5
	ja	.L51
	movaps	%xmm0, %xmm1
	divss	%xmm3, %xmm1
	jmp	.L37
.L51:
	movaps	%xmm0, %xmm3
	divss	%xmm1, %xmm3
	jmp	.L37
	.size	__ieee754_y0f, .-__ieee754_y0f
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1194986426
	.align 4
.LC1:
	.long	1206126716
	.align 4
.LC2:
	.long	1193186779
	.align 4
.LC3:
	.long	1164942315
	.align 4
.LC4:
	.long	3315869786
	.align 4
.LC5:
	.long	3306902390
	.align 4
.LC6:
	.long	3279980564
	.align 4
.LC7:
	.long	3238088326
	.align 4
.LC8:
	.long	3180331008
	.align 4
.LC9:
	.long	1122570648
	.align 4
.LC10:
	.long	0
	.align 4
.LC11:
	.long	1159094510
	.align 4
.LC12:
	.long	1175872968
	.align 4
.LC13:
	.long	1169872836
	.align 4
.LC14:
	.long	1149462547
	.align 4
.LC15:
	.long	3282909049
	.align 4
.LC16:
	.long	3282410907
	.align 4
.LC17:
	.long	3263650171
	.align 4
.LC18:
	.long	3229948808
	.align 4
.LC19:
	.long	3180331007
	.align 4
.LC20:
	.long	1114833928
	.align 4
.LC21:
	.long	2907227530
	.align 4
.LC22:
	.long	1127060664
	.align 4
.LC23:
	.long	1150091238
	.align 4
.LC24:
	.long	1150628723
	.align 4
.LC25:
	.long	1135919562
	.align 4
.LC26:
	.long	3254490469
	.align 4
.LC27:
	.long	3261616402
	.align 4
.LC28:
	.long	3249519186
	.align 4
.LC29:
	.long	3222941077
	.align 4
.LC30:
	.long	3180330936
	.align 4
.LC31:
	.long	1108307092
	.align 4
.LC32:
	.long	2972649499
	.align 4
.LC33:
	.long	1097500058
	.align 4
.LC34:
	.long	1125769242
	.align 4
.LC35:
	.long	1132936242
	.align 4
.LC36:
	.long	1124611312
	.align 4
.LC37:
	.long	3226399757
	.align 4
.LC38:
	.long	3241350966
	.align 4
.LC39:
	.long	3237238687
	.align 4
.LC40:
	.long	3216617932
	.align 4
.LC41:
	.long	3180329746
	.align 4
.LC42:
	.long	1102168877
	.align 4
.LC43:
	.long	3015612599
	.align 4
.LC44:
	.long	1065353216
	.align 4
.LC45:
	.long	3366447977
	.align 4
.LC46:
	.long	1229796185
	.align 4
.LC47:
	.long	1229201108
	.align 4
.LC48:
	.long	1208693395
	.align 4
.LC49:
	.long	1174213314
	.align 4
.LC50:
	.long	1192269472
	.align 4
.LC51:
	.long	1175088330
	.align 4
.LC52:
	.long	1141599001
	.align 4
.LC53:
	.long	1094470291
	.align 4
.LC54:
	.long	1033240576
	.align 4
.LC55:
	.long	1126418090
	.align 4
.LC56:
	.long	3316077246
	.align 4
.LC57:
	.long	1192003777
	.align 4
.LC58:
	.long	1197322013
	.align 4
.LC59:
	.long	1184054932
	.align 4
.LC60:
	.long	1157750023
	.align 4
.LC61:
	.long	1157152587
	.align 4
.LC62:
	.long	1149265869
	.align 4
.LC63:
	.long	1124539536
	.align 4
.LC64:
	.long	1085980038
	.align 4
.LC65:
	.long	1033240575
	.align 4
.LC66:
	.long	1118145952
	.align 4
.LC67:
	.long	765586553
	.align 4
.LC68:
	.long	3272949593
	.align 4
.LC69:
	.long	1159546199
	.align 4
.LC70:
	.long	1170858855
	.align 4
.LC71:
	.long	1164411487
	.align 4
.LC72:
	.long	1144089628
	.align 4
.LC73:
	.long	1126611940
	.align 4
.LC74:
	.long	1126878943
	.align 4
.LC75:
	.long	1110080709
	.align 4
.LC76:
	.long	1079379939
	.align 4
.LC77:
	.long	1033240432
	.align 4
.LC78:
	.long	1111689494
	.align 4
.LC79:
	.long	831940635
	.align 4
.LC80:
	.long	3232363352
	.align 4
.LC81:
	.long	1129622168
	.align 4
.LC82:
	.long	1146928101
	.align 4
.LC83:
	.long	1146303017
	.align 4
.LC84:
	.long	1132899471
	.align 4
.LC85:
	.long	1099040140
	.align 4
.LC86:
	.long	1107121265
	.align 4
.LC87:
	.long	1097330173
	.align 4
.LC88:
	.long	1073726655
	.align 4
.LC89:
	.long	1033238058
	.align 4
.LC90:
	.long	1106439352
	.align 4
.LC91:
	.long	874613211
	.align 4
.LC92:
	.long	1040187392
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC93:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.align 16
.LC94:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC95:
	.long	2137866504
	.align 4
.LC96:
	.long	867144479
	.align 4
.LC97:
	.long	3207922931
	.align 4
.LC98:
	.long	1058041531
	.align 4
.LC99:
	.long	1900671690
	.align 4
.LC100:
	.long	1048576000
	.align 4
.LC101:
	.long	2979966780
	.align 4
.LC102:
	.long	905285256
	.align 4
.LC103:
	.long	960967982
	.align 4
.LC104:
	.long	1015021568
	.align 4
.LC105:
	.long	815810024
	.align 4
.LC106:
	.long	889838246
	.align 4
.LC107:
	.long	955594391
	.align 4
.LC108:
	.long	1015015236
	.align 4
.LC109:
	.long	1056964608
	.align 4
.LC110:
	.long	3212836864
	.align 4
.LC111:
	.long	1059256707
	.align 4
.LC112:
	.long	1033316021
	.align 4
.LC113:
	.long	2922324459
	.align 4
.LC114:
	.long	849871546
	.align 4
.LC115:
	.long	914355516
	.align 4
.LC116:
	.long	968239721
	.align 4
.LC117:
	.long	1013081926
	.align 4
.LC118:
	.long	1043654669
	.align 4
.LC119:
	.long	804421826
	.align 4
.LC120:
	.long	881533292
	.align 4
.LC121:
	.long	949970400
	.align 4
.LC122:
	.long	1011913605
