	.text
	.p2align 4,,15
	.globl	__nexttoward
	.type	__nexttoward, @function
__nexttoward:
	movsd	%xmm0, -32(%rsp)
	movq	-32(%rsp), %rdx
	movq	16(%rsp), %rax
	movq	%rdx, %rcx
	movl	%eax, %r8d
	shrq	$32, %rcx
	andw	$32767, %r8w
	movl	%ecx, %esi
	movl	%ecx, %edi
	andl	$2147483647, %esi
	cmpl	$2146435071, %esi
	jle	.L2
	leal	-2146435072(%rsi), %r9d
	orl	%edx, %r9d
	jne	.L3
.L2:
	cmpw	$32767, %r8w
	je	.L29
.L4:
	fldt	8(%rsp)
	fldl	-32(%rsp)
	fucomi	%st(1), %st
	jp	.L32
	je	.L30
	fstp	%st(1)
	jmp	.L6
.L32:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %esi
	je	.L31
	testl	%ecx, %ecx
	js	.L9
	fldt	8(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L26
.L27:
	subl	$1, %ecx
	testl	%edx, %edx
	cmove	%ecx, %edi
	subl	$1, %edx
.L13:
	movl	%edi, %eax
	andl	$2146435072, %eax
	cmpl	$2146435072, %eax
	jne	.L17
	movsd	-32(%rsp), %xmm0
	addsd	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
.L18:
	movl	%edx, %edx
	salq	$32, %rdi
	orq	%rdx, %rdi
	movq	%rdi, -32(%rsp)
	movsd	-32(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	fstp	%st(0)
	fstpl	-32(%rsp)
	movsd	-32(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	8(%rsp), %r9
	movq	%r9, %r8
	shrq	$32, %r8
	andl	$2147483647, %r8d
	orl	%r9d, %r8d
	je	.L4
.L3:
	fldt	8(%rsp)
	faddl	-32(%rsp)
	fstpl	-32(%rsp)
	movsd	-32(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	fstp	%st(0)
	sall	$16, %eax
	andl	$-2147483648, %eax
	salq	$32, %rax
	orq	$1, %rax
	movq	%rax, -24(%rsp)
	movq	%rax, -32(%rsp)
	movsd	-24(%rsp), %xmm0
	mulsd	%xmm0, %xmm0
	movsd	-32(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$1048575, %eax
	ja	.L18
	movsd	-32(%rsp), %xmm0
	mulsd	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L9:
	fldt	8(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L27
.L26:
	addl	$1, %ecx
	addl	$1, %edx
	cmove	%ecx, %edi
	jmp	.L13
	.size	__nexttoward, .-__nexttoward
	.weak	nexttoward
	.set	nexttoward,__nexttoward
