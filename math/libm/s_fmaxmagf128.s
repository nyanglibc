	.text
	.globl	__unordtf2
	.globl	__getf2
	.globl	__letf2
	.globl	__eqtf2
	.globl	__gttf2
	.globl	__addtf3
	.p2align 4,,15
	.globl	__fmaxmagf128
	.type	__fmaxmagf128, @function
__fmaxmagf128:
	subq	$72, %rsp
	movdqa	%xmm0, %xmm2
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm1, 48(%rsp)
	movdqa	.LC0(%rip), %xmm0
	pand	%xmm0, %xmm2
	pand	%xmm1, %xmm0
	movdqa	%xmm2, %xmm1
	movaps	%xmm2, 16(%rsp)
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L15
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L20
.L15:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L16
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L24
.L16:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L27
	movdqa	32(%rsp), %xmm0
	call	__issignalingf128@PLT
	testl	%eax, %eax
	jne	.L8
	movdqa	48(%rsp), %xmm0
	call	__issignalingf128@PLT
	testl	%eax, %eax
	jne	.L8
	movdqa	48(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L24:
	movdqa	48(%rsp), %xmm0
.L1:
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	32(%rsp), %xmm0
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	32(%rsp), %xmm0
	movdqa	48(%rsp), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	32(%rsp), %xmm0
	jg	.L1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L8:
	movdqa	48(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	addq	$72, %rsp
	ret
	.size	__fmaxmagf128, .-__fmaxmagf128
	.weak	fmaxmagf128
	.set	fmaxmagf128,__fmaxmagf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
