	.text
	.p2align 4,,15
	.globl	__GI___fpclassifyl
	.hidden	__GI___fpclassifyl
	.type	__GI___fpclassifyl, @function
__GI___fpclassifyl:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdx
	movl	$2, %eax
	movq	%rsi, %rcx
	andw	$32767, %dx
	movl	%esi, %r8d
	shrq	$32, %rcx
	movswl	%dx, %edi
	orl	%ecx, %r8d
	orl	%edi, %r8d
	je	.L1
	testl	%edi, %edi
	jne	.L8
	testl	%ecx, %ecx
	movl	$3, %eax
	jns	.L1
.L8:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	js	.L14
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpw	$32767, %dx
	movl	$4, %eax
	jne	.L1
	andl	$2147483647, %ecx
	xorl	%eax, %eax
	orl	%esi, %ecx
	sete	%al
	ret
	.size	__GI___fpclassifyl, .-__GI___fpclassifyl
	.globl	__fpclassifyl
	.set	__fpclassifyl,__GI___fpclassifyl
