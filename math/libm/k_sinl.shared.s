	.text
	.p2align 4,,15
	.globl	__kernel_sinl
	.type	__kernel_sinl, @function
__kernel_sinl:
	fldt	8(%rsp)
	fld	%st(0)
	fabs
	flds	.LC0(%rip)
	fucomi	%st(1), %st
	jbe	.L21
	fstp	%st(0)
	flds	.LC1(%rip)
	fucomip	%st(1), %st
	jbe	.L26
	fldt	.LC2(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L6
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L6:
	fnstcw	-10(%rsp)
	movzwl	-10(%rsp), %eax
	orb	$12, %ah
	movw	%ax, -12(%rsp)
	fld	%st(0)
	fldcw	-12(%rsp)
	fistpl	-16(%rsp)
	fldcw	-10(%rsp)
	movl	-16(%rsp), %eax
	testl	%eax, %eax
	fld	%st(0)
	jne	.L27
	fstp	%st(1)
.L22:
	rep ret
	.p2align 4,,10
	.p2align 3
.L21:
	fld	%st(1)
	fnstcw	-10(%rsp)
	movzwl	-10(%rsp), %eax
	pxor	%xmm0, %xmm0
	fsubs	.LC11(%rip)
	orb	$12, %ah
	movw	%ax, -12(%rsp)
	fmuls	.LC12(%rip)
	fldcw	-12(%rsp)
	fistpl	-16(%rsp)
	fldcw	-10(%rsp)
	movl	-16(%rsp), %eax
	cvtsi2sd	%eax, %xmm0
	sall	$2, %eax
	testl	%edi, %edi
	mulsd	.LC13(%rip), %xmm0
	movsd	%xmm0, -24(%rsp)
	faddl	-24(%rsp)
	jne	.L24
	fsubrp	%st, %st(1)
.L12:
	fld	%st(0)
	leal	2(%rax), %edx
	movq	__sincosl_table@GOTPCREL(%rip), %rcx
	fmul	%st(1), %st
	movslq	%edx, %rdx
	salq	$4, %rdx
	fldt	(%rcx,%rdx)
	leal	3(%rax), %edx
	cltq
	salq	$4, %rax
	movslq	%edx, %rdx
	salq	$4, %rdx
	fldt	.LC15(%rip)
	fmul	%st(2), %st
	fldt	.LC16(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC17(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(2), %st
	fldt	.LC18(%rip)
	faddp	%st, %st(1)
	fmul	%st(2), %st
	fsubs	.LC19(%rip)
	fmul	%st(2), %st
	fmul	%st(1), %st
	fldt	(%rcx,%rdx)
	faddp	%st, %st(1)
	fldt	.LC20(%rip)
	fmul	%st(3), %st
	fldt	.LC21(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fmul	%st(3), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(3)
	fld1
	faddp	%st, %st(3)
	fxch	%st(2)
	fmulp	%st, %st(3)
	fldt	(%rcx,%rax)
	fmulp	%st, %st(3)
	fxch	%st(1)
	faddp	%st, %st(2)
	faddp	%st, %st(1)
	fldz
	fucomip	%st(2), %st
	fstp	%st(1)
	jbe	.L22
	fchs
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	fldz
	fucomip	%st(3), %st
	ja	.L25
.L10:
	fsubp	%st, %st(1)
	fldt	24(%rsp)
	fsubp	%st, %st(1)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L26:
	fstp	%st(0)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	fstp	%st(0)
.L4:
	fld	%st(0)
	fmul	%st(1), %st
	fldt	.LC3(%rip)
	fmul	%st(1), %st
	fldt	.LC4(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC5(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC6(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC7(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC8(%rip)
	fsubrp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC9(%rip)
	faddp	%st, %st(1)
	fmul	%st(1), %st
	fldt	.LC10(%rip)
	fsubrp	%st, %st(1)
	fmulp	%st, %st(1)
	fmul	%st(1), %st
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	fldt	24(%rsp)
	fchs
	fstpt	24(%rsp)
	jmp	.L10
	.size	__kernel_sinl, .-__kernel_sinl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1041760256
	.align 4
.LC1:
	.long	788529152
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC3:
	.long	1442594151
	.long	3397912749
	.long	16334
	.long	0
	.align 16
.LC4:
	.long	2006475742
	.long	3611270829
	.long	16342
	.long	0
	.align 16
.LC5:
	.long	1082802218
	.long	2962370717
	.long	16350
	.long	0
	.align 16
.LC6:
	.long	2854685540
	.long	3610389311
	.long	16357
	.long	0
	.align 16
.LC7:
	.long	3057228925
	.long	3102678314
	.long	16364
	.long	0
	.align 16
.LC8:
	.long	218157069
	.long	3490513104
	.long	16370
	.long	0
	.align 16
.LC9:
	.long	2290649225
	.long	2290649224
	.long	16376
	.long	0
	.align 16
.LC10:
	.long	2863311531
	.long	2863311530
	.long	16380
	.long	0
	.section	.rodata.cst4
	.align 4
.LC11:
	.long	1041498112
	.align 4
.LC12:
	.long	1124073472
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC13:
	.long	0
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC15:
	.long	1931208070
	.long	2482141934
	.long	49129
	.long	0
	.align 16
.LC16:
	.long	217514350
	.long	3490513104
	.long	16367
	.long	0
	.align 16
.LC17:
	.long	190887435
	.long	3054198966
	.long	16373
	.long	0
	.align 16
.LC18:
	.long	2863311531
	.long	2863311530
	.long	16378
	.long	0
	.section	.rodata.cst4
	.align 4
.LC19:
	.long	1056964608
	.section	.rodata.cst16
	.align 16
.LC20:
	.long	2250501689
	.long	3610388340
	.long	49125
	.long	0
	.align 16
.LC21:
	.long	3056731701
	.long	3102678314
	.long	16364
	.long	0
