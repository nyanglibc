	.text
#APP
	.symver __ieee754_lgammaf128_r,__lgammaf128_r_finite@GLIBC_2.26
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__multf3
	.globl	__eqtf2
	.globl	__lttf2
	.globl	__subtf3
	.globl	__divtf3
	.globl	__addtf3
	.globl	__fixtfsi
	.globl	__letf2
	.globl	__floatsitf
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_lgammaf128_r
	.type	__ieee754_lgammaf128_r, @function
__ieee754_lgammaf128_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movdqa	%xmm0, %xmm2
	subq	$88, %rsp
	movl	$1, (%rdi)
	pand	.LC55(%rip), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	.LC56(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L173
	movdqa	16(%rsp), %xmm2
	movdqa	.LC56(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L173
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L5
	movaps	(%rsp), %xmm5
	movmskps	%xmm5, %eax
	testb	$8, %al
	je	.L5
	movl	$-1, (%rbx)
.L5:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L239
	movdqa	.LC64(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L240
	movdqa	.LC144(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L241
	movdqa	.LC145(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L242
	movdqa	(%rsp), %xmm1
	movdqa	.LC146(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__logf128@PLT
	movdqa	.LC61(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	.LC147(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm2
	jg	.L1
	movdqa	(%rsp), %xmm0
	leaq	176+RASY(%rip), %rbx
	movdqa	%xmm0, %xmm1
	leaq	-192(%rbx), %rbp
	movaps	%xmm2, 32(%rsp)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC60(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	.LC52(%rip), %xmm4
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC53(%rip), %xmm0
	movdqa	32(%rsp), %xmm2
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L243:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L169:
	movdqa	%xmm0, %xmm1
	subq	$16, %rbx
	movaps	%xmm2, 32(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	32(%rsp), %xmm2
	jne	.L243
	movdqa	(%rsp), %xmm1
	movaps	%xmm2, 16(%rsp)
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L173:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
.L1:
	addq	$88, %rsp
	movdqa	%xmm2, %xmm0
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	movdqa	.LC57(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L9
	movdqa	.LC58(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L244
.L9:
	movdqa	(%rsp), %xmm3
	pxor	.LC59(%rip), %xmm3
	movdqa	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)
	call	__floorf128@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L245
	movdqa	.LC61(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 48(%rsp)
	call	__floorf128@PLT
	movdqa	48(%rsp), %xmm1
	call	__eqtf2@PLT
	cmpq	$1, %rax
	movdqa	.LC62(%rip), %xmm1
	sbbl	%eax, %eax
	orl	$1, %eax
	movdqa	(%rsp), %xmm0
	movl	%eax, (%rbx)
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L217
	movdqa	16(%rsp), %xmm0
	call	__logf128@PLT
	movdqa	.LC59(%rip), %xmm2
	pxor	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L240:
	movdqa	.LC61(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	call	__floorf128@PLT
	call	__fixtfsi@PLT
	cmpl	$13, %eax
	ja	.L172
	leaq	.L24(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L24:
	.long	.L23-.L24
	.long	.L25-.L24
	.long	.L26-.L24
	.long	.L27-.L24
	.long	.L28-.L24
	.long	.L29-.L24
	.long	.L30-.L24
	.long	.L31-.L24
	.long	.L32-.L24
	.long	.L33-.L24
	.long	.L34-.L24
	.long	.L35-.L24
	.long	.L36-.L24
	.long	.L37-.L24
	.text
	.p2align 4,,10
	.p2align 3
.L241:
	movl	(%rbx), %edi
	call	__floatsitf@PLT
	movdqa	.LC56(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC56(%rip), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L245:
	movdqa	32(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__subtf3@PLT
	movdqa	.LC55(%rip), %xmm1
	pand	%xmm0, %xmm1
	movdqa	.LC60(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	movdqa	.LC136(%rip), %xmm1
	leaq	96+RN12(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-112(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC46(%rip), %xmm3
	movaps	%xmm0, (%rsp)
	movaps	%xmm3, 16(%rsp)
	movdqa	.LC47(%rip), %xmm1
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L246:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L157:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L246
	movdqa	(%rsp), %xmm0
	leaq	80+RD12(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC137(%rip), %xmm1
	leaq	-96(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC48(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 16(%rsp)
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L247:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L159:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L247
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC138(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC139(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	movdqa	.LC132(%rip), %xmm1
	leaq	96+RN11(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-112(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC43(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	movdqa	.LC44(%rip), %xmm1
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L248:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L153:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L248
	movdqa	(%rsp), %xmm0
	leaq	80+RD11(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC133(%rip), %xmm1
	leaq	-96(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC45(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 16(%rsp)
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L249:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L155:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L249
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC134(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC135(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	movdqa	.LC128(%rip), %xmm1
	leaq	96+RN10(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-112(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC40(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC41(%rip), %xmm1
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L250:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L149:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L250
	movdqa	(%rsp), %xmm0
	leaq	96+RD10(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC129(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC42(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L251:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L151:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L251
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC130(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC131(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movdqa	.LC124(%rip), %xmm1
	leaq	96+RN9(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-112(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC37(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC38(%rip), %xmm1
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L252:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L145:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L252
	movdqa	(%rsp), %xmm0
	leaq	96+RD9(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC125(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC39(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 16(%rsp)
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L253:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L147:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L253
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC126(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC127(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L32:
	movdqa	.LC120(%rip), %xmm1
	leaq	112+RN8(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC34(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
	movdqa	.LC35(%rip), %xmm1
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L254:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L141:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L254
	movdqa	(%rsp), %xmm0
	leaq	96+RD8(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC121(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC36(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L255:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L143:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L255
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC122(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC123(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	movdqa	.LC116(%rip), %xmm1
	leaq	112+RN7(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC31(%rip), %xmm3
	movaps	%xmm0, (%rsp)
	movaps	%xmm3, 16(%rsp)
	movdqa	.LC32(%rip), %xmm1
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L256:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L137:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L256
	movdqa	(%rsp), %xmm0
	leaq	96+RD7(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC117(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC33(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 16(%rsp)
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L257:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L139:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L257
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC118(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC119(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	.LC112(%rip), %xmm1
	leaq	112+RN6(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC28(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	movdqa	.LC29(%rip), %xmm1
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L258:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L133:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L258
	movdqa	(%rsp), %xmm0
	leaq	112+RD6(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC113(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC30(%rip), %xmm6
	movdqa	%xmm0, %xmm1
	movaps	%xmm6, 16(%rsp)
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L259:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L135:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L259
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC114(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC115(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movdqa	.LC108(%rip), %xmm1
	leaq	128+RN5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC25(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC26(%rip), %xmm1
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L260:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L129:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L260
	movdqa	(%rsp), %xmm0
	leaq	112+RD5(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC109(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC27(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L261:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L131:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L261
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC110(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC111(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	movdqa	.LC104(%rip), %xmm1
	leaq	128+RN4(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC22(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	movdqa	.LC23(%rip), %xmm1
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L262:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L125:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L262
	movdqa	(%rsp), %xmm0
	leaq	128+RD4(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC105(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC24(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 16(%rsp)
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L263:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 16(%rsp)
.L127:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L263
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC106(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC107(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movdqa	.LC99(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L232
	movdqa	.LC95(%rip), %xmm1
	leaq	112+RN2r5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC17(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
	movdqa	.LC16(%rip), %xmm1
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L264:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L117:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L264
	movdqa	(%rsp), %xmm0
	leaq	112+RD2r5(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC96(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC18(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L265:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L119:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L265
	movdqa	32(%rsp), %xmm0
.L237:
	call	__divtf3@PLT
	movdqa	.LC97(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC98(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	movdqa	.LC90(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L229
	movdqa	.LC89(%rip), %xmm1
	leaq	112+RN1r5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC75(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC6(%rip), %xmm6
	movdqa	.LC7(%rip), %xmm3
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 32(%rsp)
	movaps	%xmm3, 16(%rsp)
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L266:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 32(%rsp)
.L99:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 16(%rsp)
	jne	.L266
	leaq	112+RD1r5(%rip), %rbx
	movdqa	.LC76(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 32(%rsp)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L267:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 32(%rsp)
.L101:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L267
.L100:
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC77(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC78(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	.LC79(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L224
	movdqa	.LC73(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L225
	movdqa	.LC74(%rip), %xmm1
	leaq	112+RN1r5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC75(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC6(%rip), %xmm6
	movdqa	.LC7(%rip), %xmm4
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm6, 48(%rsp)
	movaps	%xmm4, 32(%rsp)
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L268:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 48(%rsp)
.L70:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 32(%rsp)
	jne	.L268
	leaq	112+RD1r5(%rip), %rbx
	movdqa	.LC76(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 48(%rsp)
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L269:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 48(%rsp)
.L72:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L269
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC77(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC78(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
.L73:
	movdqa	(%rsp), %xmm0
	call	__logf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movdqa	.LC65(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L270
	movdqa	.LC66(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L220
	movdqa	.LC0(%rip), %xmm4
	leaq	112+RN1(%rip), %rbx
	movaps	%xmm4, 16(%rsp)
	leaq	-128(%rbx), %rbp
	movdqa	.LC1(%rip), %xmm1
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L271:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 16(%rsp)
.L43:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L271
	movdqa	(%rsp), %xmm0
	leaq	96+RD1(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC67(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L272:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 16(%rsp)
.L45:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L272
.L236:
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
.L46:
	movdqa	(%rsp), %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__logf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L37:
	movdqa	.LC140(%rip), %xmm1
	leaq	96+RN13(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-112(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC49(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
	movdqa	.LC50(%rip), %xmm1
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L273:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L161:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L273
	movdqa	(%rsp), %xmm0
	leaq	80+RD13(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC141(%rip), %xmm1
	leaq	-96(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC51(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L274:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L163:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L274
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC142(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC143(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L242:
	movdqa	(%rsp), %xmm0
	call	__logf128@PLT
	movdqa	.LC60(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L217:
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC61(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	48(%rsp), %xmm2
	jle	.L19
	movdqa	.LC60(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
.L19:
	movdqa	%xmm2, %xmm0
	movdqa	.LC63(%rip), %xmm1
	call	__multf3@PLT
	call	__sinf128@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	leaq	76(%rsp), %rdi
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__ieee754_lgammaf128_r@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC63(%rip), %xmm0
	call	__divtf3@PLT
	call	__logf128@PLT
	movdqa	(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L244:
	movdqa	(%rsp), %xmm0
	movq	%rbx, %rdi
	call	__lgamma_negf128@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
.L224:
	movdqa	.LC60(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	.LC60(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	js	.L275
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L172
	movdqa	.LC86(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L227
	movdqa	.LC60(%rip), %xmm1
	leaq	112+RN1(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC0(%rip), %xmm4
	movaps	%xmm0, (%rsp)
	movaps	%xmm4, 16(%rsp)
	movdqa	.LC1(%rip), %xmm1
	jmp	.L87
.L276:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L87:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L276
	movdqa	(%rsp), %xmm0
	leaq	96+RD1(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC67(%rip), %xmm1
	leaq	-112(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC2(%rip), %xmm5
	movdqa	%xmm0, %xmm1
	movaps	%xmm5, 16(%rsp)
	jmp	.L89
.L277:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L89:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L277
.L88:
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
.L229:
	movdqa	.LC91(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L230
	movdqa	.LC92(%rip), %xmm1
	leaq	112+RN1r75(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC11(%rip), %xmm5
	movaps	%xmm0, (%rsp)
	movaps	%xmm5, 16(%rsp)
	movdqa	.LC9(%rip), %xmm1
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L278:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 16(%rsp)
.L105:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L278
	movdqa	(%rsp), %xmm0
	leaq	112+RD1r75(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC81(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC12(%rip), %xmm7
	movdqa	%xmm0, %xmm1
	movaps	%xmm7, 16(%rsp)
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L279:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L107:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L279
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC82(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC83(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
.L232:
	movdqa	.LC100(%rip), %xmm1
	leaq	128+RN3(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC19(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC20(%rip), %xmm1
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L280:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L121:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L280
	movdqa	(%rsp), %xmm0
	leaq	128+RD3(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC101(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC21(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 16(%rsp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L281:
	movdqa	(%rbx), %xmm4
	movaps	%xmm4, 16(%rsp)
.L123:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L281
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC102(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC103(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
.L220:
	movdqa	.LC68(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L221
	movdqa	.LC69(%rip), %xmm1
	leaq	128+RN1r25(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC3(%rip), %xmm7
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm7, 32(%rsp)
	movdqa	.LC4(%rip), %xmm0
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L282:
	movdqa	(%rbx), %xmm7
	movaps	%xmm7, 32(%rsp)
.L50:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L282
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	leaq	112+RD1r25(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC70(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm7
	movaps	%xmm7, 32(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L283:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 32(%rsp)
.L52:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	jne	.L283
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC71(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC72(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L46
.L270:
	movdqa	(%rsp), %xmm0
	call	__logf128@PLT
	movdqa	.LC59(%rip), %xmm2
	pxor	%xmm0, %xmm2
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L172:
	pxor	%xmm2, %xmm2
	jmp	.L1
.L225:
	movdqa	.LC80(%rip), %xmm1
	leaq	112+RN1r75(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC11(%rip), %xmm7
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm7, 32(%rsp)
	movdqa	.LC9(%rip), %xmm1
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L284:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 32(%rsp)
.L75:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L284
	movdqa	16(%rsp), %xmm0
	leaq	112+RD1r75(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC81(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC12(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 32(%rsp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L285:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 32(%rsp)
.L77:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L285
	movdqa	48(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC82(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC83(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L73
.L230:
	movdqa	.LC93(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L172
	movdqa	.LC94(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L231
	movdqa	.LC93(%rip), %xmm1
	leaq	128+RN2(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC10(%rip), %xmm1
.L110:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L110
	movdqa	(%rsp), %xmm0
	leaq	128+RD2(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC84(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
.L111:
	movdqa	(%rsp), %xmm0
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L111
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
.L275:
	call	__subtf3@PLT
	leaq	112+RNr9(%rip), %rbx
	movdqa	.LC13(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	leaq	-128(%rbx), %rbp
	movaps	%xmm6, 16(%rsp)
	movdqa	.LC14(%rip), %xmm1
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L286:
	movdqa	(%rbx), %xmm5
	movaps	%xmm5, 16(%rsp)
.L81:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L286
	movdqa	(%rsp), %xmm0
	leaq	112+RDr9(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC85(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC15(%rip), %xmm4
	movdqa	%xmm0, %xmm1
	movaps	%xmm4, 16(%rsp)
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L287:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 16(%rsp)
.L83:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L287
	jmp	.L88
.L221:
	movdqa	.LC73(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L222
	movdqa	.LC74(%rip), %xmm1
	leaq	112+RN1r5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC75(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC6(%rip), %xmm3
	movdqa	.LC7(%rip), %xmm6
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm6, 32(%rsp)
	jmp	.L56
.L288:
	movdqa	(%rbx), %xmm6
	movaps	%xmm6, 48(%rsp)
.L56:
	subq	$16, %rbx
	movdqa	32(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 32(%rsp)
	jne	.L288
	leaq	112+RD1r5(%rip), %rbx
	movdqa	.LC76(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm3
	movdqa	%xmm0, %xmm1
	movaps	%xmm3, 48(%rsp)
	jmp	.L58
.L289:
	movdqa	(%rbx), %xmm3
	movaps	%xmm3, 48(%rsp)
.L58:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L289
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC77(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC78(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L46
.L222:
	movdqa	.LC79(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L223
	movdqa	.LC80(%rip), %xmm1
	leaq	112+RN1r75(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC9(%rip), %xmm1
.L61:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L61
	movdqa	16(%rsp), %xmm0
	leaq	112+RD1r75(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC81(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
.L62:
	movdqa	16(%rsp), %xmm0
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L62
	movdqa	32(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC82(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC83(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L46
.L227:
	movdqa	.LC87(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L228
	movdqa	.LC88(%rip), %xmm1
	leaq	128+RN1r25(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC4(%rip), %xmm1
.L92:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L92
	movdqa	(%rsp), %xmm0
	leaq	112+RD1r25(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC70(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
.L93:
	movdqa	(%rsp), %xmm0
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L93
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	.LC71(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	.LC72(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm2
	jmp	.L1
.L231:
	movdqa	.LC95(%rip), %xmm1
	leaq	112+RN2r5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC16(%rip), %xmm1
.L112:
	subq	$16, %rbx
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L112
	movdqa	(%rsp), %xmm0
	leaq	112+RD2r5(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC96(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
.L113:
	movdqa	(%rsp), %xmm0
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L113
	movdqa	16(%rsp), %xmm0
	jmp	.L237
.L228:
	movdqa	.LC89(%rip), %xmm1
	leaq	112+RN1r5(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-128(%rbx), %rbp
	call	__subtf3@PLT
	movdqa	.LC75(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	.LC7(%rip), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
.L94:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movaps	%xmm0, 16(%rsp)
	jne	.L94
	leaq	112+RD1r5(%rip), %rbx
	movdqa	.LC76(%rip), %xmm1
	leaq	-128(%rbx), %rbp
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
.L95:
	movdqa	(%rsp), %xmm0
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L95
	jmp	.L100
.L223:
	movdqa	.LC60(%rip), %xmm1
	leaq	128+RN2(%rip), %rbx
	movdqa	(%rsp), %xmm0
	leaq	-144(%rbx), %rbp
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	.LC10(%rip), %xmm1
.L63:
	subq	$16, %rbx
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L63
	movdqa	16(%rsp), %xmm0
	leaq	128+RD2(%rip), %rbx
	call	__multf3@PLT
	movdqa	.LC84(%rip), %xmm1
	leaq	-144(%rbx), %rbp
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
.L64:
	movdqa	16(%rsp), %xmm0
	subq	$16, %rbx
	call	__multf3@PLT
	movdqa	16(%rbx), %xmm1
	call	__addtf3@PLT
	cmpq	%rbp, %rbx
	movdqa	%xmm0, %xmm1
	jne	.L64
	jmp	.L236
	.size	__ieee754_lgammaf128_r, .-__ieee754_lgammaf128_r
	.section	.rodata
	.align 32
	.type	RDr9, @object
	.size	RDr9, 144
RDr9:
	.long	3608086463
	.long	401617948
	.long	447284504
	.long	-1072531531
	.long	2661975991
	.long	3406912676
	.long	4141331112
	.long	-1072393456
	.long	1295234768
	.long	2577386336
	.long	4221163137
	.long	-1072339592
	.long	2322064982
	.long	2786947033
	.long	34870849
	.long	-1072344531
	.long	109779815
	.long	3208045145
	.long	389371183
	.long	-1072402686
	.long	3537718612
	.long	4079672740
	.long	3128911219
	.long	-1072510833
	.long	2416024623
	.long	2074264622
	.long	4101700438
	.long	-1072680476
	.long	3989407487
	.long	2853912028
	.long	1388002952
	.long	-1072924527
	.long	3912596835
	.long	1180023971
	.long	1412191372
	.long	-1073278859
	.align 32
	.type	RNr9, @object
	.size	RNr9, 144
RNr9:
	.long	1087177754
	.long	2168978984
	.long	2061395512
	.long	1074901434
	.long	3560246755
	.long	3136148366
	.long	265312793
	.long	1075001039
	.long	1773903928
	.long	789264593
	.long	1197534512
	.long	1074977591
	.long	3952569181
	.long	3682480490
	.long	2146887076
	.long	-1072567235
	.long	2599112280
	.long	1251600152
	.long	4041436882
	.long	-1072486632
	.long	3785001412
	.long	3402628008
	.long	1898663894
	.long	-1072534784
	.long	2759343404
	.long	3661292755
	.long	637799073
	.long	-1072655002
	.long	2105296292
	.long	1791999624
	.long	1694069298
	.long	-1072852771
	.long	3146889979
	.long	1622896691
	.long	3035900861
	.long	-1073151431
	.align 32
	.type	RD1, @object
	.size	RD1, 128
RD1:
	.long	1749874529
	.long	2190507463
	.long	4221639448
	.long	1074597467
	.long	1624261046
	.long	734021750
	.long	3475266402
	.long	1074727398
	.long	2089038119
	.long	522341383
	.long	699693765
	.long	1074768335
	.long	1409180771
	.long	664496879
	.long	2334590254
	.long	1074750571
	.long	1091607669
	.long	1879361822
	.long	1858746377
	.long	1074679033
	.long	887533220
	.long	1536441047
	.long	2289756114
	.long	1074550478
	.long	4241393369
	.long	2073561186
	.long	2955981094
	.long	1074357710
	.long	1289152898
	.long	463558985
	.long	1260174628
	.long	1074083165
	.align 32
	.type	RN1, @object
	.size	RN1, 144
RN1:
	.long	2212324901
	.long	3202488393
	.long	2067978470
	.long	-1072941028
	.long	2843349302
	.long	1262173024
	.long	2470365472
	.long	-1072855151
	.long	4027719455
	.long	2107671587
	.long	2830439171
	.long	-1072909987
	.long	3809059856
	.long	1071436807
	.long	2867690600
	.long	1074577875
	.long	2256097477
	.long	380526915
	.long	2069981762
	.long	1074614640
	.long	4244075441
	.long	976635455
	.long	794960013
	.long	1074546515
	.long	1907521288
	.long	208378600
	.long	535744508
	.long	1074403658
	.long	1462101401
	.long	4251443439
	.long	3151578055
	.long	1074169779
	.long	3379928620
	.long	3531565179
	.long	3212023598
	.long	1073815827
	.align 32
	.type	RD1r25, @object
	.size	RD1r25, 144
RD1r25:
	.long	3611660493
	.long	308170619
	.long	2080625336
	.long	1074889923
	.long	106639851
	.long	1588511664
	.long	776606653
	.long	1075010824
	.long	1457892843
	.long	2793077931
	.long	4153562943
	.long	1075054052
	.long	3783773849
	.long	2988703471
	.long	2861886571
	.long	1075030458
	.long	1928006197
	.long	857397054
	.long	2375723028
	.long	1074959890
	.long	688609669
	.long	4106875427
	.long	2778652029
	.long	1074842650
	.long	732155703
	.long	2640826367
	.long	1494616466
	.long	1074670237
	.long	2837009885
	.long	2240965416
	.long	3533071589
	.long	1074434958
	.long	2615273789
	.long	2365840348
	.long	1875423815
	.long	1074121946
	.align 32
	.type	RN1r25, @object
	.size	RN1r25, 160
RN1r25:
	.long	419240128
	.long	81457257
	.long	3748446109
	.long	-1072733773
	.long	3207253473
	.long	1325931567
	.long	3302528770
	.long	-1072737470
	.long	513712952
	.long	4244644619
	.long	2036399474
	.long	1074860347
	.long	2455135104
	.long	3835584788
	.long	44406436
	.long	1074933130
	.long	619792738
	.long	2269717601
	.long	2856109135
	.long	1074919890
	.long	1757152481
	.long	3491829710
	.long	3568310163
	.long	1074837517
	.long	4175907915
	.long	3685079103
	.long	267971595
	.long	1074700045
	.long	2160721768
	.long	196369368
	.long	1163937105
	.long	1074500133
	.long	3368921795
	.long	1009915969
	.long	2615956208
	.long	1074221706
	.long	93209149
	.long	1664214749
	.long	3242524179
	.long	1073822233
	.align 32
	.type	RD1r5, @object
	.size	RD1r5, 144
RD1r5:
	.long	519231968
	.long	641196334
	.long	3320236060
	.long	1075009661
	.long	2471706960
	.long	1236939739
	.long	655468129
	.long	1075120952
	.long	4277415400
	.long	4138011103
	.long	1265380024
	.long	1075141330
	.long	3223347048
	.long	1977853507
	.long	3453631115
	.long	1075111978
	.long	3830198227
	.long	578629318
	.long	910118812
	.long	1075024825
	.long	2102310624
	.long	3104302085
	.long	1541793131
	.long	1074892094
	.long	2112956056
	.long	183514739
	.long	1142063291
	.long	1074708621
	.long	2051983056
	.long	3666650024
	.long	2029955350
	.long	1074464806
	.long	775537868
	.long	3386098703
	.long	1829402702
	.long	1074136933
	.align 32
	.type	RN1r5, @object
	.size	RN1r5, 144
RN1r5:
	.long	4144305329
	.long	2159916779
	.long	3417571711
	.long	1074941274
	.long	4190849508
	.long	3517044018
	.long	2634944254
	.long	1075040849
	.long	1417823200
	.long	3616144777
	.long	1097488426
	.long	1075055581
	.long	1665152732
	.long	1066634473
	.long	2523320369
	.long	1075004779
	.long	3487566799
	.long	2270243818
	.long	104515592
	.long	1074905127
	.long	857519856
	.long	1615290684
	.long	1467215093
	.long	1074748331
	.long	3366991732
	.long	2710294897
	.long	3121180428
	.long	1074535150
	.long	1946306001
	.long	4099211725
	.long	854601278
	.long	1074238806
	.long	1355467089
	.long	148026260
	.long	3039746628
	.long	1073824409
	.align 32
	.type	RD1r75, @object
	.size	RD1r75, 144
RD1r75:
	.long	1872518005
	.long	593675529
	.long	1570662348
	.long	-1072000409
	.long	1118968892
	.long	1232116226
	.long	569296331
	.long	-1071914923
	.long	677779316
	.long	875728963
	.long	4276031973
	.long	-1071916936
	.long	1554791744
	.long	4114424198
	.long	1132968054
	.long	-1071976981
	.long	3126645300
	.long	2228035505
	.long	2991262393
	.long	-1072093458
	.long	14620418
	.long	1309060393
	.long	3792406623
	.long	-1072262700
	.long	2084011205
	.long	3168327250
	.long	134594846
	.long	-1072487095
	.long	3613128419
	.long	365544554
	.long	1409348516
	.long	-1072789697
	.long	3450222377
	.long	1804517144
	.long	2019833687
	.long	-1073204278
	.align 32
	.type	RN1r75, @object
	.size	RN1r75, 144
RN1r75:
	.long	1067416621
	.long	2422728907
	.long	3698866925
	.long	-1072132523
	.long	1353649429
	.long	1828939087
	.long	355986165
	.long	-1072003206
	.long	470692316
	.long	3852269560
	.long	3644428117
	.long	-1071965801
	.long	4019290972
	.long	3093243850
	.long	3950960572
	.long	-1071999309
	.long	3607911675
	.long	3036392929
	.long	699491900
	.long	-1072085424
	.long	2048082621
	.long	4204652772
	.long	218330110
	.long	-1072224241
	.long	3332220057
	.long	4233415572
	.long	288180595
	.long	-1072422284
	.long	3026164841
	.long	1206694823
	.long	188978459
	.long	-1072689581
	.long	836296678
	.long	2838176260
	.long	2212956585
	.long	-1073065650
	.align 32
	.type	RD2, @object
	.size	RD2, 160
RD2:
	.long	840584862
	.long	3697343810
	.long	1601331668
	.long	-1071643139
	.long	3941108776
	.long	1454648920
	.long	2013274638
	.long	-1071567497
	.long	700203269
	.long	1390220642
	.long	1557832011
	.long	-1071569978
	.long	1440710970
	.long	1272250889
	.long	2268207842
	.long	-1071631205
	.long	2367223699
	.long	3645894917
	.long	2887859743
	.long	-1071741980
	.long	3424218837
	.long	927111253
	.long	1619586578
	.long	-1071896835
	.long	2806506725
	.long	4135520846
	.long	3265171271
	.long	-1072101260
	.long	1105939812
	.long	3989613435
	.long	1863498219
	.long	-1072364843
	.long	2347650207
	.long	930270958
	.long	1715877741
	.long	-1072704586
	.long	941791014
	.long	2041524203
	.long	3146896060
	.long	-1073154481
	.align 32
	.type	RN2, @object
	.size	RN2, 160
RN2:
	.long	638591159
	.long	3285731932
	.long	2236840321
	.long	-1071727857
	.long	1799690172
	.long	799467228
	.long	3522635599
	.long	-1071623324
	.long	821189610
	.long	1595849687
	.long	108581200
	.long	-1071601793
	.long	1689154084
	.long	1221482551
	.long	3626321265
	.long	-1071637650
	.long	1973638569
	.long	3837410202
	.long	2128884453
	.long	-1071726915
	.long	1641730696
	.long	3841171676
	.long	1057140973
	.long	-1071863422
	.long	1703625424
	.long	630531741
	.long	1146596984
	.long	-1072045777
	.long	582430974
	.long	972411124
	.long	2232049395
	.long	-1072288081
	.long	82495988
	.long	4294700887
	.long	3892947312
	.long	-1072599399
	.long	2806428948
	.long	2652380014
	.long	778800863
	.long	-1073010004
	.align 32
	.type	RD2r5, @object
	.size	RD2r5, 144
RD2r5:
	.long	1187626274
	.long	1588179390
	.long	4239302262
	.long	-1071674264
	.long	1900689071
	.long	3184858179
	.long	272017206
	.long	-1071626322
	.long	3272401457
	.long	1789510219
	.long	2602061518
	.long	-1071661570
	.long	3184437242
	.long	1801490789
	.long	53222638
	.long	-1071758792
	.long	3398382810
	.long	3684714657
	.long	1133326259
	.long	-1071904359
	.long	547763478
	.long	707121363
	.long	2116629743
	.long	-1072106822
	.long	826425866
	.long	803100170
	.long	965541525
	.long	-1072368453
	.long	155962793
	.long	2272732101
	.long	1175049672
	.long	-1072706912
	.long	3665648173
	.long	3194859307
	.long	1533268643
	.long	-1073155932
	.align 32
	.type	RN2r5, @object
	.size	RN2r5, 144
RN2r5:
	.long	211532280
	.long	4102921444
	.long	128574313
	.long	-1071704387
	.long	3795161073
	.long	3461341667
	.long	2318267553
	.long	-1071638783
	.long	1303010653
	.long	1952686173
	.long	3338720751
	.long	-1071658323
	.long	3862257826
	.long	1527162528
	.long	1835660936
	.long	-1071738608
	.long	1142859846
	.long	4286845962
	.long	935950976
	.long	-1071869204
	.long	3626332156
	.long	959410700
	.long	977671627
	.long	-1072049307
	.long	3106492637
	.long	2489686262
	.long	3655120419
	.long	-1072289530
	.long	1726404340
	.long	3400487056
	.long	3410626244
	.long	-1072600856
	.long	799854866
	.long	2028355813
	.long	1998641922
	.long	-1073010987
	.align 32
	.type	RD3, @object
	.size	RD3, 160
RD3:
	.long	3962364392
	.long	2255649117
	.long	645127339
	.long	-1071258152
	.long	4086115992
	.long	1754407049
	.long	3507223465
	.long	-1071221455
	.long	2394295916
	.long	315635161
	.long	1596323843
	.long	-1071259025
	.long	2008456125
	.long	2181835896
	.long	2602986995
	.long	-1071360784
	.long	2416135617
	.long	3172390815
	.long	3406422415
	.long	-1071505794
	.long	2949551576
	.long	2799908515
	.long	1059675866
	.long	-1071701113
	.long	688851743
	.long	1434685586
	.long	3499560673
	.long	-1071948285
	.long	862593309
	.long	3727913873
	.long	3617679718
	.long	-1072247966
	.long	3709357936
	.long	2979484383
	.long	769113077
	.long	-1072621840
	.long	1168905110
	.long	1159207211
	.long	4246318241
	.long	-1073113720
	.align 32
	.type	RN3, @object
	.size	RN3, 160
RN3:
	.long	297477139
	.long	391808692
	.long	1559371241
	.long	-1071267756
	.long	2764675830
	.long	1806056460
	.long	3845124518
	.long	-1071216552
	.long	2673856561
	.long	1443912843
	.long	3081283710
	.long	-1071242741
	.long	206488463
	.long	3628248733
	.long	3590200685
	.long	-1071327331
	.long	442945365
	.long	1182711450
	.long	1033097947
	.long	-1071461449
	.long	1365997146
	.long	2651663258
	.long	4276700058
	.long	-1071639931
	.long	21337479
	.long	3375182303
	.long	243069093
	.long	-1071871622
	.long	2683024489
	.long	2786925873
	.long	1741132583
	.long	-1072153483
	.long	1236607936
	.long	2680010956
	.long	2077115089
	.long	-1072501356
	.long	65606945
	.long	3742376099
	.long	4276530265
	.long	-1072954262
	.align 32
	.type	RD4, @object
	.size	RD4, 160
RD4:
	.long	3280468266
	.long	571143821
	.long	1116570112
	.long	-1070998602
	.long	2427485180
	.long	2474085838
	.long	2431962367
	.long	-1070984235
	.long	1004364821
	.long	1555463679
	.long	702461652
	.long	-1071051400
	.long	1709971446
	.long	939538590
	.long	2112431107
	.long	-1071177668
	.long	1121194291
	.long	997670957
	.long	1604040075
	.long	-1071354050
	.long	727512512
	.long	498922272
	.long	2451856846
	.long	-1071571986
	.long	1615539299
	.long	3772773578
	.long	1161214154
	.long	-1071840324
	.long	472751001
	.long	1211070832
	.long	2070501488
	.long	-1072167764
	.long	800658263
	.long	2098507139
	.long	3426758673
	.long	-1072568704
	.long	98368049
	.long	511072903
	.long	1064313064
	.long	-1073081840
	.align 32
	.type	RN4, @object
	.size	RN4, 160
RN4:
	.long	3713669496
	.long	3275957019
	.long	1822036913
	.long	-1070978362
	.long	2979083876
	.long	1284078663
	.long	422730439
	.long	-1070957508
	.long	2721100424
	.long	4202672312
	.long	1751717827
	.long	-1071015315
	.long	4107851991
	.long	3097838413
	.long	1656375198
	.long	-1071126236
	.long	3730985628
	.long	2615942033
	.long	2468746705
	.long	-1071292705
	.long	693815106
	.long	1821967168
	.long	3436426437
	.long	-1071499860
	.long	585787343
	.long	2721905315
	.long	1034507781
	.long	-1071756929
	.long	1541016884
	.long	2997226768
	.long	639256854
	.long	-1072067728
	.long	3649043711
	.long	1713102094
	.long	774876618
	.long	-1072443693
	.long	2477511772
	.long	2492536384
	.long	3110060857
	.long	-1072926488
	.align 32
	.type	RD5, @object
	.size	RD5, 144
RD5:
	.long	4163359969
	.long	3409825499
	.long	2654232023
	.long	1076097381
	.long	929490176
	.long	1228630297
	.long	3457539888
	.long	1076082021
	.long	2673430114
	.long	4148675068
	.long	3029473244
	.long	1075986680
	.long	1481673847
	.long	3585384820
	.long	76067148
	.long	1075838327
	.long	2086196040
	.long	1620179114
	.long	1354685513
	.long	1075632568
	.long	2449298653
	.long	3650402362
	.long	1217190886
	.long	1075381554
	.long	1817888346
	.long	1254267362
	.long	3307932352
	.long	1075071337
	.long	514771496
	.long	3738337954
	.long	1395996008
	.long	1074704322
	.long	4034686390
	.long	111014179
	.long	4247876719
	.long	1074257211
	.align 32
	.type	RN5, @object
	.size	RN5, 160
RN5:
	.long	1322513828
	.long	3643137575
	.long	3031892337
	.long	1076131473
	.long	3384688874
	.long	1635066080
	.long	1401217544
	.long	1076126952
	.long	3189610857
	.long	1833781578
	.long	2694468017
	.long	1076043281
	.long	1865801930
	.long	2610502567
	.long	481712990
	.long	1075901653
	.long	3598556651
	.long	50119782
	.long	774754997
	.long	1075708927
	.long	3050828821
	.long	647353000
	.long	2516456327
	.long	1075463493
	.long	3758827631
	.long	2047579060
	.long	507722343
	.long	1075174300
	.long	3608799679
	.long	2510101690
	.long	296573501
	.long	1074820401
	.long	675642491
	.long	490776612
	.long	1819952000
	.long	1074398468
	.long	33728918
	.long	569310943
	.long	4003214401
	.long	1073845539
	.align 32
	.type	RD6, @object
	.size	RD6, 144
RD6:
	.long	3480647406
	.long	1621636958
	.long	3766034445
	.long	-1070912052
	.long	1788465889
	.long	2865111180
	.long	3957832599
	.long	-1070948932
	.long	3760116006
	.long	1298368777
	.long	4039766829
	.long	-1071066350
	.long	1856596908
	.long	3464340849
	.long	3649898546
	.long	-1071245308
	.long	2935802240
	.long	415539481
	.long	1644800952
	.long	-1071480996
	.long	2512712357
	.long	2739025755
	.long	2979530461
	.long	-1071764664
	.long	2035648207
	.long	1162461070
	.long	1139148619
	.long	-1072106292
	.long	2622325881
	.long	3430232136
	.long	3818045919
	.long	-1072529444
	.long	1178413257
	.long	3487189210
	.long	816762539
	.long	-1073061698
	.align 32
	.type	RN6, @object
	.size	RN6, 144
RN6:
	.long	1401417658
	.long	3222640805
	.long	315828891
	.long	-1070857868
	.long	1388394436
	.long	3714813901
	.long	4083908423
	.long	-1070891955
	.long	3182425663
	.long	2323971445
	.long	1320581277
	.long	-1071000867
	.long	1950408532
	.long	49860204
	.long	3950072640
	.long	-1071172646
	.long	577924203
	.long	3463479323
	.long	3811728290
	.long	-1071393170
	.long	4044374772
	.long	3362641842
	.long	2152007444
	.long	-1071668704
	.long	2147814494
	.long	3536864908
	.long	1285363129
	.long	-1071998477
	.long	3986295899
	.long	2802468684
	.long	1420788603
	.long	-1072397665
	.long	4285251288
	.long	4287147732
	.long	3390405454
	.long	-1072894782
	.align 32
	.type	RD7, @object
	.size	RD7, 128
RD7:
	.long	897084477
	.long	2416956795
	.long	2472361376
	.long	1076075204
	.long	2884184812
	.long	2788835952
	.long	3726828561
	.long	1076020846
	.long	2168137959
	.long	3586082465
	.long	3501814451
	.long	1075878183
	.long	3506666846
	.long	1417815562
	.long	2021240417
	.long	1075675422
	.long	2075772900
	.long	2653030136
	.long	1961172030
	.long	1075418312
	.long	1610376039
	.long	3950000198
	.long	935656518
	.long	1075107184
	.long	2243212169
	.long	3741874457
	.long	1317499404
	.long	1074730145
	.long	3409112337
	.long	1866938
	.long	4162077099
	.long	1074269991
	.align 32
	.type	RN7, @object
	.size	RN7, 144
RN7:
	.long	4185625233
	.long	1231298426
	.long	3409332113
	.long	1076134051
	.long	4049709283
	.long	2741438245
	.long	865461754
	.long	1076087056
	.long	2308531563
	.long	1177018659
	.long	1533013909
	.long	1075953270
	.long	1558703921
	.long	109808776
	.long	2742983957
	.long	1075760562
	.long	1325814216
	.long	1785171207
	.long	1700328244
	.long	1075515134
	.long	825253368
	.long	588655013
	.long	3259933674
	.long	1075209981
	.long	1423743707
	.long	1017079035
	.long	3659244316
	.long	1074852768
	.long	1304285006
	.long	942961522
	.long	3468730021
	.long	1074410000
	.long	3908558377
	.long	2799702254
	.long	1003776413
	.long	1073847669
	.align 32
	.type	RD8, @object
	.size	RD8, 128
RD8:
	.long	2462203250
	.long	1316231137
	.long	3251761227
	.long	1076179196
	.long	4091447052
	.long	2677569386
	.long	2732115776
	.long	1076111010
	.long	300210805
	.long	2056533862
	.long	2029726568
	.long	1075957564
	.long	101592124
	.long	3275137036
	.long	1313145213
	.long	1075739812
	.long	3744082617
	.long	14159607
	.long	92834593
	.long	1075469622
	.long	3335663624
	.long	3325265113
	.long	996853536
	.long	1075143026
	.long	1750651343
	.long	1160257232
	.long	569697687
	.long	1074752380
	.long	4137245050
	.long	657833806
	.long	1153435078
	.long	1074280153
	.align 32
	.type	RN8, @object
	.size	RN8, 144
RN8:
	.long	1891085063
	.long	1226574569
	.long	1610815663
	.long	1076245343
	.long	2606227191
	.long	722485045
	.long	4214791556
	.long	1076182333
	.long	329839270
	.long	3602395386
	.long	3242217688
	.long	1076038926
	.long	64422241
	.long	752619051
	.long	3114927724
	.long	1075831545
	.long	2307328414
	.long	1109663516
	.long	1964610555
	.long	1075571678
	.long	1017476203
	.long	1974186184
	.long	350354012
	.long	1075255938
	.long	2092577612
	.long	1030639112
	.long	2330258464
	.long	1074877631
	.long	1846648059
	.long	3519316471
	.long	226321640
	.long	1074424195
	.long	1090972672
	.long	4257722015
	.long	3777569911
	.long	1073849908
	.align 32
	.type	RD9, @object
	.size	RD9, 128
RD9:
	.long	4151219389
	.long	73644689
	.long	1807307145
	.long	-1070903407
	.long	1626295758
	.long	3425651573
	.long	871664296
	.long	-1070986425
	.long	53006893
	.long	1710661870
	.long	2253877233
	.long	-1071164323
	.long	4024705846
	.long	2934750586
	.long	3686253686
	.long	-1071400521
	.long	2812292689
	.long	2216342464
	.long	1093055510
	.long	-1071696913
	.long	326757530
	.long	104260447
	.long	3848984688
	.long	-1072055233
	.long	2143609659
	.long	1850137751
	.long	4278722631
	.long	-1072488593
	.long	1261877921
	.long	2529262188
	.long	2207239783
	.long	-1073039060
	.align 32
	.type	RN9, @object
	.size	RN9, 128
RN9:
	.long	301677892
	.long	162646804
	.long	1518138199
	.long	-1070831830
	.long	4043696077
	.long	664747691
	.long	999354320
	.long	-1070911030
	.long	2084453301
	.long	1966012405
	.long	90042985
	.long	-1071078158
	.long	404855447
	.long	4288345739
	.long	3264794713
	.long	-1071305827
	.long	1259543934
	.long	1193110152
	.long	1752936407
	.long	-1071590544
	.long	2657931404
	.long	1849815148
	.long	981274410
	.long	-1071938442
	.long	78471775
	.long	2667675983
	.long	6469707
	.long	-1072353241
	.long	2049928649
	.long	1190830350
	.long	865677063
	.long	-1072873941
	.align 32
	.type	RD10, @object
	.size	RD10, 128
RD10:
	.long	2539167888
	.long	3459155784
	.long	3301770321
	.long	-1070821282
	.long	3721730937
	.long	4160388642
	.long	652846567
	.long	-1070915914
	.long	1947021067
	.long	3979893978
	.long	4052453252
	.long	-1071101888
	.long	660637637
	.long	1356051608
	.long	3297250251
	.long	-1071350022
	.long	2596062935
	.long	866366448
	.long	1086750278
	.long	-1071652855
	.long	30757905
	.long	401065435
	.long	3874608565
	.long	-1072023733
	.long	2005425617
	.long	3399687011
	.long	2115067551
	.long	-1072469453
	.long	2778357225
	.long	866558376
	.long	3493658784
	.long	-1073023911
	.align 32
	.type	RN10, @object
	.size	RN10, 128
RN10:
	.long	2873879011
	.long	4181852628
	.long	1848781683
	.long	-1070742844
	.long	3142775500
	.long	3089753301
	.long	3174112020
	.long	-1070835750
	.long	1851931600
	.long	1425803264
	.long	2840820165
	.long	-1071011846
	.long	3919498431
	.long	1652768542
	.long	1839690957
	.long	-1071247817
	.long	165438465
	.long	3408007764
	.long	2037638356
	.long	-1071548071
	.long	1142917512
	.long	68574184
	.long	1494117189
	.long	-1071900523
	.long	3642024871
	.long	2891789894
	.long	997540522
	.long	-1072330514
	.long	3620149976
	.long	487658373
	.long	45832721
	.long	-1072861353
	.align 32
	.type	RD11, @object
	.size	RD11, 112
RD11:
	.long	1876309354
	.long	4083414947
	.long	1143971605
	.long	1076069276
	.long	3847326262
	.long	1549732134
	.long	3339073819
	.long	1075959987
	.long	44465053
	.long	1357412151
	.long	1102837822
	.long	1075758547
	.long	790933697
	.long	2577065731
	.long	3880574870
	.long	1075491457
	.long	3580585927
	.long	3752682160
	.long	415985208
	.long	1075163317
	.long	667454538
	.long	3080257929
	.long	2742382103
	.long	1074767652
	.long	3364987661
	.long	3592231997
	.long	2528408977
	.long	1074286904
	.align 32
	.type	RN11, @object
	.size	RN11, 128
RN11:
	.long	2037116117
	.long	1561205024
	.long	713611144
	.long	1076152264
	.long	3383921276
	.long	384035890
	.long	152373101
	.long	1076045917
	.long	2374288297
	.long	2221890097
	.long	566214380
	.long	1075852065
	.long	2854510801
	.long	1231271315
	.long	3593008332
	.long	1075593876
	.long	3119716620
	.long	1436463694
	.long	1877808222
	.long	1075275877
	.long	1035007907
	.long	3713307671
	.long	2537228251
	.long	1074895065
	.long	625025330
	.long	3806418478
	.long	827669597
	.long	1074434128
	.long	276065275
	.long	3805818937
	.long	1453249321
	.long	1073851291
	.align 32
	.type	RD12, @object
	.size	RD12, 112
RD12:
	.long	2216472789
	.long	1114497942
	.long	720081094
	.long	1076127526
	.long	483088007
	.long	3809444069
	.long	1541200101
	.long	1076007188
	.long	1055229138
	.long	2005207801
	.long	1352619674
	.long	1075798170
	.long	2162858324
	.long	2829819176
	.long	2907446464
	.long	1075524896
	.long	1814614711
	.long	1476261895
	.long	2418662640
	.long	1075190241
	.long	893177171
	.long	1020757300
	.long	2962617610
	.long	1074788552
	.long	538160906
	.long	2959111920
	.long	2642699680
	.long	1074294867
	.align 32
	.type	RN12, @object
	.size	RN12, 128
RN12:
	.long	203767144
	.long	667342424
	.long	3456824894
	.long	1076213411
	.long	2044022196
	.long	39340840
	.long	3758423961
	.long	1076102279
	.long	3767074874
	.long	837764957
	.long	656612250
	.long	1075899680
	.long	4270212706
	.long	1356437213
	.long	1428940478
	.long	1075632344
	.long	1472850816
	.long	1754028960
	.long	146050033
	.long	1075307038
	.long	553654054
	.long	1052589827
	.long	2061320935
	.long	1074918127
	.long	2191249614
	.long	3929678202
	.long	3247445539
	.long	1074445665
	.long	1723441698
	.long	3131671018
	.long	1292803901
	.long	1073852737
	.align 32
	.type	RD13, @object
	.size	RD13, 112
RD13:
	.long	1492931948
	.long	3658260610
	.long	2286147311
	.long	1076182211
	.long	1048871108
	.long	3576885226
	.long	91886306
	.long	1076053559
	.long	1726065216
	.long	780679704
	.long	2516545261
	.long	1075841142
	.long	2822272877
	.long	1430929755
	.long	3034090597
	.long	1075555227
	.long	3978205206
	.long	3732707580
	.long	3452359494
	.long	1075210125
	.long	3015004946
	.long	2449724039
	.long	1851934764
	.long	1074800860
	.long	108068208
	.long	1737490853
	.long	108298493
	.long	1074302839
	.align 32
	.type	RN13, @object
	.size	RN13, 128
RN13:
	.long	3938424099
	.long	864543403
	.long	1731245239
	.long	1076269074
	.long	3598993411
	.long	2857701756
	.long	352992559
	.long	1076147542
	.long	3925087854
	.long	156246785
	.long	404730569
	.long	1075936457
	.long	2995324169
	.long	3184210197
	.long	20254415
	.long	1075662805
	.long	95344443
	.long	3192712651
	.long	3342839535
	.long	1075329752
	.long	1270811636
	.long	3965985629
	.long	1014665766
	.long	1074932602
	.long	1844600895
	.long	2657488837
	.long	4027826329
	.long	1074457383
	.long	2828524549
	.long	1054233722
	.long	1001843326
	.long	1073854067
	.align 32
	.type	RASY, @object
	.size	RASY, 208
RASY:
	.long	1431636744
	.long	1431655765
	.long	1431655765
	.long	1073435989
	.long	1027653980
	.long	1813430634
	.long	3245086401
	.long	-1074369514
	.long	2858091496
	.long	2686024107
	.long	27269633
	.long	1072996378
	.long	3936950612
	.long	856832116
	.long	2167935873
	.long	-1074513901
	.long	4076734081
	.long	183134392
	.long	3803287510
	.long	1073002833
	.long	69892328
	.long	205527066
	.long	228155489
	.long	-1074399573
	.long	3772739275
	.long	150700680
	.long	1099181217
	.long	1073194010
	.long	1578598510
	.long	2293232068
	.long	1590686620
	.long	-1074142168
	.long	2970389231
	.long	1707553916
	.long	1079293018
	.long	1073508327
	.long	2900416617
	.long	963505692
	.long	3400004398
	.long	-1073781702
	.long	2500023449
	.long	4067262969
	.long	2477938005
	.long	1073915996
	.long	4081138543
	.long	1965487668
	.long	559287212
	.long	-1073341419
	.long	373761673
	.long	2147301553
	.long	3311571727
	.long	1074344067
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1462101401
	.long	4251443439
	.long	3151578055
	.long	1074169779
	.align 16
.LC1:
	.long	3379928620
	.long	3531565179
	.long	3212023598
	.long	1073815827
	.align 16
.LC2:
	.long	4241393369
	.long	2073561186
	.long	2955981094
	.long	1074357710
	.align 16
.LC3:
	.long	3368921795
	.long	1009915969
	.long	2615956208
	.long	1074221706
	.align 16
.LC4:
	.long	93209149
	.long	1664214749
	.long	3242524179
	.long	1073822233
	.align 16
.LC5:
	.long	2837009885
	.long	2240965416
	.long	3533071589
	.long	1074434958
	.align 16
.LC6:
	.long	1946306001
	.long	4099211725
	.long	854601278
	.long	1074238806
	.align 16
.LC7:
	.long	1355467089
	.long	148026260
	.long	3039746628
	.long	1073824409
	.align 16
.LC8:
	.long	2051983056
	.long	3666650024
	.long	2029955350
	.long	1074464806
	.align 16
.LC9:
	.long	836296678
	.long	2838176260
	.long	2212956585
	.long	-1073065650
	.align 16
.LC10:
	.long	2806428948
	.long	2652380014
	.long	778800863
	.long	-1073010004
	.align 16
.LC11:
	.long	3026164841
	.long	1206694823
	.long	188978459
	.long	-1072689581
	.align 16
.LC12:
	.long	3613128419
	.long	365544554
	.long	1409348516
	.long	-1072789697
	.align 16
.LC13:
	.long	2105296292
	.long	1791999624
	.long	1694069298
	.long	-1072852771
	.align 16
.LC14:
	.long	3146889979
	.long	1622896691
	.long	3035900861
	.long	-1073151431
	.align 16
.LC15:
	.long	3989407487
	.long	2853912028
	.long	1388002952
	.long	-1072924527
	.align 16
.LC16:
	.long	799854866
	.long	2028355813
	.long	1998641922
	.long	-1073010987
	.align 16
.LC17:
	.long	1726404340
	.long	3400487056
	.long	3410626244
	.long	-1072600856
	.align 16
.LC18:
	.long	155962793
	.long	2272732101
	.long	1175049672
	.long	-1072706912
	.align 16
.LC19:
	.long	1236607936
	.long	2680010956
	.long	2077115089
	.long	-1072501356
	.align 16
.LC20:
	.long	65606945
	.long	3742376099
	.long	4276530265
	.long	-1072954262
	.align 16
.LC21:
	.long	3709357936
	.long	2979484383
	.long	769113077
	.long	-1072621840
	.align 16
.LC22:
	.long	3649043711
	.long	1713102094
	.long	774876618
	.long	-1072443693
	.align 16
.LC23:
	.long	2477511772
	.long	2492536384
	.long	3110060857
	.long	-1072926488
	.align 16
.LC24:
	.long	800658263
	.long	2098507139
	.long	3426758673
	.long	-1072568704
	.align 16
.LC25:
	.long	675642491
	.long	490776612
	.long	1819952000
	.long	1074398468
	.align 16
.LC26:
	.long	33728918
	.long	569310943
	.long	4003214401
	.long	1073845539
	.align 16
.LC27:
	.long	514771496
	.long	3738337954
	.long	1395996008
	.long	1074704322
	.align 16
.LC28:
	.long	3986295899
	.long	2802468684
	.long	1420788603
	.long	-1072397665
	.align 16
.LC29:
	.long	4285251288
	.long	4287147732
	.long	3390405454
	.long	-1072894782
	.align 16
.LC30:
	.long	2622325881
	.long	3430232136
	.long	3818045919
	.long	-1072529444
	.align 16
.LC31:
	.long	1304285006
	.long	942961522
	.long	3468730021
	.long	1074410000
	.align 16
.LC32:
	.long	3908558377
	.long	2799702254
	.long	1003776413
	.long	1073847669
	.align 16
.LC33:
	.long	2243212169
	.long	3741874457
	.long	1317499404
	.long	1074730145
	.align 16
.LC34:
	.long	1846648059
	.long	3519316471
	.long	226321640
	.long	1074424195
	.align 16
.LC35:
	.long	1090972672
	.long	4257722015
	.long	3777569911
	.long	1073849908
	.align 16
.LC36:
	.long	1750651343
	.long	1160257232
	.long	569697687
	.long	1074752380
	.align 16
.LC37:
	.long	78471775
	.long	2667675983
	.long	6469707
	.long	-1072353241
	.align 16
.LC38:
	.long	2049928649
	.long	1190830350
	.long	865677063
	.long	-1072873941
	.align 16
.LC39:
	.long	2143609659
	.long	1850137751
	.long	4278722631
	.long	-1072488593
	.align 16
.LC40:
	.long	3642024871
	.long	2891789894
	.long	997540522
	.long	-1072330514
	.align 16
.LC41:
	.long	3620149976
	.long	487658373
	.long	45832721
	.long	-1072861353
	.align 16
.LC42:
	.long	2005425617
	.long	3399687011
	.long	2115067551
	.long	-1072469453
	.align 16
.LC43:
	.long	625025330
	.long	3806418478
	.long	827669597
	.long	1074434128
	.align 16
.LC44:
	.long	276065275
	.long	3805818937
	.long	1453249321
	.long	1073851291
	.align 16
.LC45:
	.long	667454538
	.long	3080257929
	.long	2742382103
	.long	1074767652
	.align 16
.LC46:
	.long	2191249614
	.long	3929678202
	.long	3247445539
	.long	1074445665
	.align 16
.LC47:
	.long	1723441698
	.long	3131671018
	.long	1292803901
	.long	1073852737
	.align 16
.LC48:
	.long	893177171
	.long	1020757300
	.long	2962617610
	.long	1074788552
	.align 16
.LC49:
	.long	1844600895
	.long	2657488837
	.long	4027826329
	.long	1074457383
	.align 16
.LC50:
	.long	2828524549
	.long	1054233722
	.long	1001843326
	.long	1073854067
	.align 16
.LC51:
	.long	3015004946
	.long	2449724039
	.long	1851934764
	.long	1074800860
	.align 16
.LC52:
	.long	4081138543
	.long	1965487668
	.long	559287212
	.long	-1073341419
	.align 16
.LC53:
	.long	373761673
	.long	2147301553
	.long	3311571727
	.long	1074344067
	.align 16
.LC55:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC56:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC57:
	.long	0
	.long	0
	.long	0
	.long	-1073741824
	.align 16
.LC58:
	.long	0
	.long	0
	.long	0
	.long	-1073442816
	.align 16
.LC59:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC60:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC61:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC62:
	.long	0
	.long	0
	.long	0
	.long	-1081671680
	.align 16
.LC63:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073779231
	.align 16
.LC64:
	.long	0
	.long	0
	.long	0
	.long	1073917952
	.align 16
.LC65:
	.long	0
	.long	0
	.long	0
	.long	1065811968
	.align 16
.LC66:
	.long	0
	.long	0
	.long	0
	.long	1073479680
	.align 16
.LC67:
	.long	1289152898
	.long	463558985
	.long	1260174628
	.long	1074083165
	.align 16
.LC68:
	.long	0
	.long	0
	.long	0
	.long	1073577984
	.align 16
.LC69:
	.long	0
	.long	0
	.long	0
	.long	1073545216
	.align 16
.LC70:
	.long	2615273789
	.long	2365840348
	.long	1875423815
	.long	1074121946
	.align 16
.LC71:
	.long	4088692317
	.long	110618385
	.long	3869342999
	.long	1072582744
	.align 16
.LC72:
	.long	0
	.long	0
	.long	0
	.long	1073451664
	.align 16
.LC73:
	.long	0
	.long	0
	.long	0
	.long	1073627136
	.align 16
.LC74:
	.long	0
	.long	0
	.long	0
	.long	1073600692
	.align 16
.LC75:
	.long	345883236
	.long	1402014908
	.long	3620203971
	.long	1072565354
	.align 16
.LC76:
	.long	775537868
	.long	3386098703
	.long	1829402702
	.long	1074136933
	.align 16
.LC77:
	.long	906676314
	.long	1256474285
	.long	4057397496
	.long	1072503052
	.align 16
.LC78:
	.long	0
	.long	0
	.long	0
	.long	1073476000
	.align 16
.LC79:
	.long	0
	.long	0
	.long	0
	.long	1073659904
	.align 16
.LC80:
	.long	0
	.long	0
	.long	0
	.long	1073643520
	.align 16
.LC81:
	.long	3450222377
	.long	1804517144
	.long	2019833687
	.long	1074279370
	.align 16
.LC82:
	.long	60575605
	.long	2072781637
	.long	4048233374
	.long	1072586834
	.align 16
.LC83:
	.long	0
	.long	0
	.long	0
	.long	1073437120
	.align 16
.LC84:
	.long	941791014
	.long	2041524203
	.long	3146896060
	.long	1074329167
	.align 16
.LC85:
	.long	3912596835
	.long	1180023971
	.long	1412191372
	.long	1074204789
	.align 16
.LC86:
	.long	0
	.long	0
	.long	0
	.long	1073684480
	.align 16
.LC87:
	.long	0
	.long	0
	.long	0
	.long	1073700864
	.align 16
.LC88:
	.long	0
	.long	0
	.long	0
	.long	1073692672
	.align 16
.LC89:
	.long	0
	.long	0
	.long	0
	.long	1073706541
	.align 16
.LC90:
	.long	0
	.long	0
	.long	0
	.long	1073717248
	.align 16
.LC91:
	.long	0
	.long	0
	.long	0
	.long	1073733632
	.align 16
.LC92:
	.long	0
	.long	0
	.long	0
	.long	1073725440
	.align 16
.LC93:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC94:
	.long	0
	.long	0
	.long	0
	.long	1073754112
	.align 16
.LC95:
	.long	0
	.long	0
	.long	0
	.long	1073758208
	.align 16
.LC96:
	.long	3665648173
	.long	3194859307
	.long	1533268643
	.long	1074327716
	.align 16
.LC97:
	.long	2129291335
	.long	2151266484
	.long	3544136955
	.long	1072624644
	.align 16
.LC98:
	.long	0
	.long	0
	.long	0
	.long	1073554304
	.align 16
.LC99:
	.long	0
	.long	0
	.long	0
	.long	1073766400
	.align 16
.LC100:
	.long	0
	.long	0
	.long	0
	.long	1073774592
	.align 16
.LC101:
	.long	1168905110
	.long	1159207211
	.long	4246318241
	.long	1074369928
	.align 16
.LC102:
	.long	1060072180
	.long	2654681472
	.long	485989052
	.long	1072398205
	.align 16
.LC103:
	.long	0
	.long	0
	.long	0
	.long	1073636068
	.align 16
.LC104:
	.long	0
	.long	0
	.long	0
	.long	1073807360
	.align 16
.LC105:
	.long	98368049
	.long	511072903
	.long	1064313064
	.long	1074401808
	.align 16
.LC106:
	.long	308078282
	.long	3690627546
	.long	1140868676
	.long	1072594757
	.align 16
.LC107:
	.long	0
	.long	0
	.long	0
	.long	1073728176
	.align 16
.LC108:
	.long	0
	.long	0
	.long	0
	.long	1073823744
	.align 16
.LC109:
	.long	4034686390
	.long	111014179
	.long	4247876719
	.long	1074257211
	.align 16
.LC110:
	.long	573096327
	.long	59330618
	.long	2336107764
	.long	1072619300
	.align 16
.LC111:
	.long	0
	.long	0
	.long	0
	.long	1073780426
	.align 16
.LC112:
	.long	0
	.long	0
	.long	0
	.long	1073840128
	.align 16
.LC113:
	.long	1178413257
	.long	3487189210
	.long	816762539
	.long	1074421950
	.align 16
.LC114:
	.long	229791481
	.long	663332451
	.long	3460932924
	.long	1072357923
	.align 16
.LC115:
	.long	0
	.long	0
	.long	1073741824
	.long	1073820262
	.align 16
.LC116:
	.long	0
	.long	0
	.long	0
	.long	1073856512
	.align 16
.LC117:
	.long	3409112337
	.long	1866938
	.long	4162077099
	.long	1074269991
	.align 16
.LC118:
	.long	1127746618
	.long	2658344000
	.long	2162483352
	.long	1072602471
	.align 16
.LC119:
	.long	0
	.long	0
	.long	1073741824
	.long	1073849618
	.align 16
.LC120:
	.long	0
	.long	0
	.long	0
	.long	1073872896
	.align 16
.LC121:
	.long	4137245050
	.long	657833806
	.long	1153435078
	.long	1074280153
	.align 16
.LC122:
	.long	2406468806
	.long	2342275550
	.long	3425684262
	.long	1072624429
	.align 16
.LC123:
	.long	0
	.long	0
	.long	0
	.long	1073877198
	.align 16
.LC124:
	.long	0
	.long	0
	.long	0
	.long	1073881088
	.align 16
.LC125:
	.long	1261877921
	.long	2529262188
	.long	2207239783
	.long	1074444588
	.align 16
.LC126:
	.long	1313024451
	.long	2380594909
	.long	1847151194
	.long	1072498169
	.align 16
.LC127:
	.long	0
	.long	0
	.long	3758096384
	.long	1073894232
	.align 16
.LC128:
	.long	0
	.long	0
	.long	0
	.long	1073889280
	.align 16
.LC129:
	.long	2778357225
	.long	866558376
	.long	3493658784
	.long	1074459737
	.align 16
.LC130:
	.long	3155134393
	.long	3612914882
	.long	4157557511
	.long	1072570791
	.align 16
.LC131:
	.long	0
	.long	0
	.long	2147483648
	.long	1073912232
	.align 16
.LC132:
	.long	0
	.long	0
	.long	0
	.long	1073897472
	.align 16
.LC133:
	.long	3364987661
	.long	3592231997
	.long	2528408977
	.long	1074286904
	.align 16
.LC134:
	.long	3534215424
	.long	705652254
	.long	1293200319
	.long	1072599189
	.align 16
.LC135:
	.long	0
	.long	0
	.long	1073741824
	.long	1073931095
	.align 16
.LC136:
	.long	0
	.long	0
	.long	0
	.long	1073905664
	.align 16
.LC137:
	.long	538160906
	.long	2959111920
	.long	2642699680
	.long	1074294867
	.align 16
.LC138:
	.long	936314665
	.long	83715363
	.long	1805543097
	.long	1072495060
	.align 16
.LC139:
	.long	0
	.long	0
	.long	1879048192
	.long	1073944585
	.align 16
.LC140:
	.long	0
	.long	0
	.long	0
	.long	1073913856
	.align 16
.LC141:
	.long	108068208
	.long	1737490853
	.long	108298493
	.long	1074302839
	.align 16
.LC142:
	.long	1102360469
	.long	2282361495
	.long	339122773
	.long	1072393552
	.align 16
.LC143:
	.long	0
	.long	0
	.long	2684354560
	.long	1073954763
	.align 16
.LC144:
	.long	1830829558
	.long	3622454446
	.long	2568486907
	.long	2146529706
	.align 16
.LC145:
	.long	0
	.long	0
	.long	0
	.long	1081540608
	.align 16
.LC146:
	.long	537036850
	.long	1248405881
	.long	478563307
	.long	1073665663
	.align 16
.LC147:
	.long	0
	.long	0
	.long	3597986944
	.long	1077591062
