	.text
	.p2align 4,,15
	.globl	__logbl
	.type	__logbl, @function
__logbl:
	fldt	8(%rsp)
#APP
# 14 "../sysdeps/i386/fpu/s_logbl.c" 1
	fxtract
fstp	%st
# 0 "" 2
#NO_APP
	ret
	.size	__logbl, .-__logbl
	.weak	logbf64x
	.set	logbf64x,__logbl
	.weak	logbl
	.set	logbl,__logbl
