	.text
	.globl	__addtf3
	.p2align 4,,15
	.globl	__ceilf128
	.type	__ceilf128, @function
__ceilf128:
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rdx
	movq	(%rsp), %rdi
	movq	%rdx, %rax
	sarq	$48, %rax
	andl	$32767, %eax
	subq	$16383, %rax
	cmpq	$47, %rax
	jg	.L2
	testq	%rax, %rax
	js	.L21
	movl	%eax, %ecx
	movabsq	$281474976710655, %rsi
	shrq	%cl, %rsi
	movq	%rdx, %rcx
	andq	%rsi, %rcx
	movdqa	(%rsp), %xmm0
	orq	%rdi, %rcx
	je	.L1
	testq	%rdx, %rdx
	jle	.L6
	movabsq	$281474976710656, %rdi
	movl	%eax, %ecx
	sarq	%cl, %rdi
	addq	%rdi, %rdx
.L6:
	notq	%rsi
	xorl	%eax, %eax
	andq	%rsi, %rdx
	movq	%rax, (%rsp)
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$111, %rax
	jle	.L7
	cmpq	$16384, %rax
	movdqa	(%rsp), %xmm0
	jne	.L1
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	leal	-48(%rax), %ecx
	movq	$-1, %rsi
	movdqa	(%rsp), %xmm0
	shrq	%cl, %rsi
	testq	%rdi, %rsi
	je	.L1
	testq	%rdx, %rdx
	jle	.L8
	cmpq	$48, %rax
	je	.L19
	movl	$112, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	addq	%rax, %rdi
	jnc	.L8
.L19:
	addq	$1, %rdx
.L8:
	movq	%rsi, %rax
	notq	%rax
	andq	%rdi, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	js	.L10
	orq	%rdi, %rdx
	movabsq	$4611404543450677248, %rdx
	cmove	%rax, %rdx
.L4:
	movq	%rdx, 8(%rsp)
	movq	%rax, (%rsp)
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movabsq	$-9223372036854775808, %rdx
	jmp	.L4
	.size	__ceilf128, .-__ceilf128
	.weak	ceilf128
	.set	ceilf128,__ceilf128
