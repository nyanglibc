	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__casinhf128
	.type	__casinhf128, @function
__casinhf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$64, %rsp
	movdqa	.LC3(%rip), %xmm0
	movdqa	96(%rsp), %xmm3
	movdqa	80(%rsp), %xmm2
	pand	%xmm0, %xmm2
	pand	%xmm3, %xmm0
	movdqa	%xmm2, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	16(%rsp), %xmm2
	movdqa	.LC4(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L49
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L46
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L50
.L24:
	pxor	%xmm0, %xmm0
	movdqa	96(%rsp), %xmm1
	call	__copysignf128@PLT
	movaps	%xmm0, 96(%rsp)
.L8:
	movq	%rbx, %rax
	movdqa	80(%rsp), %xmm4
	movdqa	96(%rsp), %xmm5
	movaps	%xmm4, (%rbx)
	movaps	%xmm5, 16(%rbx)
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movdqa	16(%rsp), %xmm2
	movdqa	.LC5(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	(%rsp), %xmm0
	testq	%rax, %rax
	movdqa	%xmm0, %xmm1
	jne	.L51
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L47
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L20
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L21
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L21
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L52
.L38:
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L20
.L21:
	leaq	32(%rsp), %rdi
	pushq	104(%rsp)
	pushq	104(%rsp)
	pushq	104(%rsp)
	pushq	104(%rsp)
	xorl	%esi, %esi
	call	__kernel_casinhf128@PLT
	movdqa	64(%rsp), %xmm6
	movdqa	80(%rsp), %xmm7
	movaps	%xmm6, 112(%rsp)
	movaps	%xmm7, 128(%rsp)
	addq	$32, %rsp
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L51:
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L38
.L47:
	movdqa	.LC2(%rip), %xmm7
	movaps	%xmm7, 80(%rsp)
	movaps	%xmm7, 96(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L46
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L53
	movdqa	80(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 80(%rsp)
.L46:
	movdqa	.LC2(%rip), %xmm7
	movaps	%xmm7, 96(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	80(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC1(%rip), %xmm0
.L19:
	movdqa	96(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, 96(%rsp)
	movaps	%xmm6, 80(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L50:
	movdqa	80(%rsp), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC0(%rip), %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L52:
	movdqa	.LC2(%rip), %xmm6
	movaps	%xmm6, 80(%rsp)
	movaps	%xmm6, 96(%rsp)
	jmp	.L8
.L53:
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L46
	pxor	%xmm1, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L24
	jmp	.L46
	.size	__casinhf128, .-__casinhf128
	.weak	casinhf128
	.set	casinhf128,__casinhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073648159
	.align 16
.LC1:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	2147418112
