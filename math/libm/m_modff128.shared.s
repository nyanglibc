	.text
	.globl	__subtf3
	.globl	__multf3
	.p2align 4,,15
	.globl	__modff128
	.type	__modff128, @function
__modff128:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$32, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbp
	movq	(%rsp), %r12
	movq	%rbp, %rbx
	sarq	$48, %rbx
	andl	$32767, %ebx
	subq	$16383, %rbx
	cmpq	$47, %rbx
	jg	.L2
	testq	%rbx, %rbx
	js	.L12
	movabsq	$281474976710655, %rax
	movl	%ebx, %ecx
	movq	%rbp, %rdx
	sarq	%cl, %rax
	andq	%rax, %rdx
	orq	%r12, %rdx
	jne	.L5
	movdqa	(%rsp), %xmm2
	movaps	%xmm2, (%rdi)
.L10:
	movabsq	$-9223372036854775808, %rax
	movq	$0, (%rsp)
	andq	%rax, %rbp
	movq	%rbp, 8(%rsp)
	movdqa	(%rsp), %xmm0
.L1:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$111, %rbx
	jle	.L6
	movdqa	.LC0(%rip), %xmm1
	movq	%rdi, 16(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movq	16(%rsp), %rdi
	cmpq	$16384, %rbx
	movaps	%xmm0, (%rdi)
	jne	.L10
	movabsq	$281474976710655, %rax
	andq	%rbp, %rax
	orq	%r12, %rax
	jne	.L1
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	leal	-48(%rbx), %ecx
	movq	$-1, %rax
	shrq	%cl, %rax
	testq	%rax, %r12
	je	.L13
	notq	%rax
	movq	%rbp, 24(%rsp)
	andq	%r12, %rax
	movdqa	(%rsp), %xmm0
	movq	%rax, 16(%rsp)
	movdqa	16(%rsp), %xmm5
	movaps	%xmm5, (%rdi)
	movdqa	%xmm5, %xmm1
	call	__subtf3@PLT
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	notq	%rax
	movq	$0, 16(%rsp)
	andq	%rbp, %rax
	movdqa	(%rsp), %xmm0
	movq	%rax, 24(%rsp)
	movdqa	16(%rsp), %xmm3
	movaps	%xmm3, (%rdi)
	movdqa	%xmm3, %xmm1
	call	__subtf3@PLT
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movabsq	$-9223372036854775808, %rax
	movq	$0, 16(%rsp)
	andq	%rax, %rbp
	movq	%rbp, 24(%rsp)
	movdqa	16(%rsp), %xmm4
	movaps	%xmm4, (%rdi)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movdqa	(%rsp), %xmm6
	movaps	%xmm6, (%rdi)
	jmp	.L10
	.size	__modff128, .-__modff128
	.weak	modff128
	.set	modff128,__modff128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
