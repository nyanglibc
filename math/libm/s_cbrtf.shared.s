	.text
	.p2align 4,,15
	.globl	__cbrtf
	.type	__cbrtf, @function
__cbrtf:
	movaps	%xmm0, %xmm1
	subq	$40, %rsp
	movss	%xmm0, 12(%rsp)
	leaq	28(%rsp), %rdi
	andps	.LC0(%rip), %xmm1
	movaps	%xmm1, %xmm0
	movss	%xmm1, 8(%rsp)
	call	__frexpf@PLT
	movl	28(%rsp), %ecx
	pxor	%xmm5, %xmm5
	movss	8(%rsp), %xmm1
	movss	12(%rsp), %xmm4
	testl	%ecx, %ecx
	jne	.L2
	ucomiss	%xmm1, %xmm1
	jp	.L3
	ucomiss	.LC1(%rip), %xmm1
	ja	.L3
	ucomiss	.LC2(%rip), %xmm1
	pxor	%xmm5, %xmm5
	jnb	.L2
	ucomiss	%xmm5, %xmm4
	jp	.L2
	je	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	cvtss2sd	%xmm0, %xmm0
	pxor	%xmm3, %xmm3
	movsd	.LC4(%rip), %xmm2
	movl	%ecx, %eax
	movsd	.LC5(%rip), %xmm1
	movl	$1431655766, %edi
	mulsd	%xmm0, %xmm2
	imull	%edi
	movl	%ecx, %eax
	sarl	$31, %eax
	subsd	%xmm2, %xmm1
	movl	%edx, %edi
	subl	%eax, %edi
	leal	(%rdi,%rdi,2), %eax
	mulsd	%xmm0, %xmm1
	subl	%eax, %ecx
	leaq	factor(%rip), %rax
	addl	$2, %ecx
	movslq	%ecx, %rcx
	ucomiss	%xmm5, %xmm4
	addsd	.LC6(%rip), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movaps	%xmm1, %xmm2
	mulss	%xmm1, %xmm2
	mulss	%xmm1, %xmm2
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm3
	movapd	%xmm0, %xmm2
	addsd	%xmm0, %xmm2
	addsd	%xmm3, %xmm2
	addsd	%xmm3, %xmm3
	mulsd	%xmm2, %xmm1
	addsd	%xmm0, %xmm3
	pxor	%xmm0, %xmm0
	divsd	%xmm3, %xmm1
	mulsd	(%rax,%rcx,8), %xmm1
	cvtsd2ss	%xmm1, %xmm0
	jbe	.L13
.L6:
	call	__ldexpf@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorps	.LC7(%rip), %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	addss	%xmm4, %xmm4
	addq	$40, %rsp
	movaps	%xmm4, %xmm0
	ret
	.size	__cbrtf, .-__cbrtf
	.weak	cbrtf32
	.set	cbrtf32,__cbrtf
	.weak	cbrtf
	.set	cbrtf,__cbrtf
	.section	.rodata
	.align 32
	.type	factor, @object
	.size	factor, 40
factor:
	.long	4186796682
	.long	1071917218
	.long	2772266556
	.long	1072260606
	.long	0
	.long	1072693248
	.long	4186796683
	.long	1072965794
	.long	2772266557
	.long	1073309182
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	8388608
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC4:
	.long	2428698333
	.long	1070105380
	.align 8
.LC5:
	.long	1227355477
	.long	1072059007
	.align 8
.LC6:
	.long	932107285
	.long	1071613884
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	2147483648
	.long	0
	.long	0
	.long	0
