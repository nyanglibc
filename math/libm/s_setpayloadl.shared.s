	.text
	.p2align 4,,15
	.globl	__setpayloadl
	.type	__setpayloadl, @function
__setpayloadl:
	movq	16(%rsp), %rdx
	cmpw	$16444, %dx
	ja	.L9
	movq	8(%rsp), %rax
	movq	%rax, %rsi
	movl	%eax, %r11d
	shrq	$32, %rsi
	cmpw	$16382, %dx
	ja	.L3
	movl	%esi, %ecx
	orl	%eax, %ecx
	jne	.L9
	testw	%dx, %dx
	jne	.L9
	movl	$16446, %r8d
	xorl	%r10d, %r10d
.L4:
	movl	$16414, %ecx
	movl	$-1, %r9d
	subl	%r10d, %ecx
	sall	%cl, %r9d
	notl	%r9d
	testl	%esi, %r9d
	setne	%cl
.L8:
	testb	%cl, %cl
	jne	.L9
	testw	%dx, %dx
	je	.L15
	cmpl	$31, %r8d
	jle	.L13
	movl	$16414, %ecx
	movl	%esi, %r11d
	movl	$-1073741824, %esi
	subl	%r10d, %ecx
	shrl	%cl, %r11d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L3:
	movzwl	%dx, %r10d
	movl	$16446, %r8d
	subl	%r10d, %r8d
	cmpl	$31, %r8d
	jle	.L17
	testl	%eax, %eax
	je	.L4
.L9:
	xorl	%edx, %edx
	movq	$0, -24(%rsp)
	movl	$1, %eax
	movw	%dx, -16(%rsp)
	fldt	-24(%rsp)
	fstpt	(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r8d, %ecx
	movl	$-1, %r9d
	sall	%cl, %r9d
	notl	%r9d
	testl	%eax, %r9d
	setne	%cl
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L13:
	leal	-16414(%r10), %ecx
	movl	%esi, %r11d
	sall	%cl, %r11d
	movl	%r8d, %ecx
	shrl	%cl, %eax
	shrl	%cl, %esi
	orl	%eax, %r11d
.L15:
	orl	$-1073741824, %esi
.L12:
	movl	$32767, %eax
	movl	%esi, -20(%rsp)
	movl	%r11d, -24(%rsp)
	movw	%ax, -16(%rsp)
	xorl	%eax, %eax
	fldt	-24(%rsp)
	fstpt	(%rdi)
	ret
	.size	__setpayloadl, .-__setpayloadl
	.weak	setpayloadf64x
	.set	setpayloadf64x,__setpayloadl
	.weak	setpayloadl
	.set	setpayloadl,__setpayloadl
