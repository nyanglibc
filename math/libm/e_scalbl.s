 .section .rodata
 .align 1<<4
 .type zero_nan,@object
zero_nan:
 .double 0.0
nan: .byte 0, 0, 0, 0, 0, 0, 0xff, 0x7f
 .byte 0, 0, 0, 0, 0, 0, 0, 0x80
 .byte 0, 0, 0, 0, 0, 0, 0xff, 0x7f
 .size zero_nan,.-zero_nan;
 .text
.globl __ieee754_scalbl
.type __ieee754_scalbl,@function
.align 1<<4
__ieee754_scalbl:
 fldt 24(%rsp)
 fxam
 fnstsw
 fldt 8(%rsp)
 andl $0x4700, %eax
 cmpl $0x0700, %eax
 je 1f
 andl $0x4500, %eax
 cmpl $0x0100, %eax
 je 2f
 fxam
 fnstsw
 andl $0x4500, %eax
 cmpl $0x0100, %eax
 je 2f
 fld %st(1)
 frndint
 fcomip %st(2), %st
 jne 4f
 fscale
 fstp %st(1)
 ret
1: fxam
 fnstsw
 movl 16(%rsp), %edx
 shrl $5, %eax
 fstp %st
 fstp %st
 andl $0x8000, %edx
 andl $0x0228, %eax
 cmpl $0x0028, %eax
 je 4f
 andl $8, %eax
 shrl $11, %edx
 addl %edx, %eax
 lea zero_nan(%rip),%rdx
 fldl (%rdx,%rax,1)
 ret
2: faddp
 ret
4: fstp %st
 fstp %st
 fldz
 fdiv %st
 ret
.size __ieee754_scalbl,.-__ieee754_scalbl
