	.text
#APP
	.symver __ieee754_gammaf_r,__gammaf_r_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.type	gammaf_positive, @function
gammaf_positive:
	pushq	%rbx
	movaps	%xmm0, %xmm2
	movq	%rdi, %rbx
	subq	$48, %rsp
	movss	.LC2(%rip), %xmm0
	ucomiss	%xmm2, %xmm0
	ja	.L25
	movss	.LC3(%rip), %xmm0
	ucomiss	%xmm2, %xmm0
	jnb	.L26
	movss	.LC4(%rip), %xmm0
	ucomiss	%xmm2, %xmm0
	ja	.L27
	movss	.LC5(%rip), %xmm0
	movl	$0x00000000, 40(%rsp)
	ucomiss	%xmm2, %xmm0
	ja	.L28
	pxor	%xmm5, %xmm5
	movss	.LC1(%rip), %xmm7
	movss	%xmm7, 16(%rsp)
	movss	%xmm5, 8(%rsp)
	movss	%xmm5, 12(%rsp)
.L9:
	movaps	%xmm2, %xmm0
	movss	%xmm2, 20(%rsp)
	call	__roundf@PLT
	movss	20(%rsp), %xmm2
	leaq	44(%rsp), %rdi
	movaps	%xmm2, %xmm6
	movss	%xmm0, 28(%rsp)
	movss	%xmm2, 24(%rsp)
	subss	%xmm0, %xmm6
	movaps	%xmm2, %xmm0
	movss	%xmm6, 20(%rsp)
	call	__frexpf@PLT
	movss	.LC8(%rip), %xmm3
	ucomiss	%xmm0, %xmm3
	movss	24(%rsp), %xmm2
	movss	28(%rsp), %xmm1
	ja	.L12
	movl	44(%rsp), %edx
.L13:
	cvttss2si	%xmm1, %eax
	movaps	%xmm2, %xmm1
	movss	%xmm2, 28(%rsp)
	imull	%edx, %eax
	movl	%eax, (%rbx)
	call	__ieee754_powf@PLT
	movss	%xmm0, 24(%rsp)
	pxor	%xmm0, %xmm0
	cvtsi2ss	44(%rsp), %xmm0
	mulss	20(%rsp), %xmm0
	call	__ieee754_exp2f@PLT
	movss	28(%rsp), %xmm2
	movss	24(%rsp), %xmm7
	mulss	%xmm0, %xmm7
	movaps	%xmm2, %xmm0
	movss	%xmm2, 24(%rsp)
	xorps	.LC9(%rip), %xmm0
	movss	%xmm7, 20(%rsp)
	call	__ieee754_expf@PLT
	movss	24(%rsp), %xmm2
	mulss	20(%rsp), %xmm0
	movss	.LC10(%rip), %xmm3
	divss	%xmm2, %xmm3
	sqrtss	%xmm3, %xmm3
	mulss	%xmm3, %xmm0
	movaps	%xmm0, %xmm3
	movaps	%xmm2, %xmm0
	divss	16(%rsp), %xmm3
	movss	%xmm2, 16(%rsp)
	movss	%xmm3, 20(%rsp)
	call	__ieee754_logf@PLT
	movss	16(%rsp), %xmm2
	movaps	%xmm2, %xmm4
	movss	.LC11(%rip), %xmm1
	mulss	12(%rsp), %xmm0
	mulss	%xmm2, %xmm4
	divss	%xmm4, %xmm1
	subss	.LC12(%rip), %xmm1
	divss	%xmm4, %xmm1
	movaps	%xmm0, %xmm4
	subss	8(%rsp), %xmm4
	addss	.LC13(%rip), %xmm1
	movaps	%xmm1, %xmm0
	divss	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	call	__expm1f@PLT
	movss	20(%rsp), %xmm3
	addq	$48, %rsp
	mulss	%xmm3, %xmm0
	popq	%rbx
	addss	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$0, (%rdi)
	movaps	%xmm2, %xmm0
	leaq	44(%rsp), %rdi
	call	__ieee754_lgammaf_r@PLT
	call	__ieee754_expf@PLT
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movaps	%xmm2, %xmm0
	movl	$0, (%rdi)
	leaq	44(%rsp), %rdi
	movss	%xmm2, 8(%rsp)
	addss	.LC1(%rip), %xmm0
	call	__ieee754_lgammaf_r@PLT
	call	__ieee754_expf@PLT
	movss	8(%rsp), %xmm2
	addq	$48, %rsp
	divss	%xmm2, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	44(%rsp), %eax
	addss	%xmm0, %xmm0
	leal	-1(%rax), %edx
	movl	%edx, 44(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	subss	.LC1(%rip), %xmm2
	movl	$0, (%rdi)
	leaq	44(%rsp), %rdi
	movaps	%xmm2, %xmm0
	movss	%xmm2, 8(%rsp)
	call	__ieee754_lgammaf_r@PLT
	call	__ieee754_expf@PLT
	movss	8(%rsp), %xmm2
	addq	$48, %rsp
	mulss	%xmm2, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	subss	%xmm2, %xmm0
	movss	.LC7(%rip), %xmm4
	movss	.LC6(%rip), %xmm5
	movaps	%xmm0, %xmm3
	movaps	%xmm0, %xmm1
	andps	%xmm4, %xmm3
	ucomiss	%xmm3, %xmm5
	jbe	.L11
	cvttss2si	%xmm0, %eax
	pxor	%xmm3, %xmm3
	movss	.LC1(%rip), %xmm5
	andnps	%xmm0, %xmm4
	cvtsi2ss	%eax, %xmm3
	cmpnless	%xmm3, %xmm1
	andps	%xmm5, %xmm1
	addss	%xmm3, %xmm1
	orps	%xmm4, %xmm1
.L11:
	movaps	%xmm2, %xmm3
	cvttss2si	%xmm1, %edi
	movaps	%xmm2, %xmm5
	leaq	40(%rsp), %rsi
	addss	%xmm1, %xmm3
	movaps	%xmm3, %xmm0
	movss	%xmm3, 20(%rsp)
	subss	%xmm1, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm5, %xmm1
	movss	%xmm5, 12(%rsp)
	call	__gamma_productf@PLT
	movss	40(%rsp), %xmm7
	movss	20(%rsp), %xmm3
	movss	%xmm0, 16(%rsp)
	movaps	%xmm3, %xmm2
	movss	%xmm7, 8(%rsp)
	jmp	.L9
	.size	gammaf_positive, .-gammaf_positive
	.p2align 4,,15
	.globl	__ieee754_gammaf_r
	.type	__ieee754_gammaf_r, @function
__ieee754_gammaf_r:
	movd	%xmm0, %eax
	testl	$2147483647, %eax
	je	.L88
	leal	-2147483648(%rax), %edx
	cmpl	$2139095039, %edx
	jbe	.L89
.L32:
	cmpl	$-8388608, %eax
	je	.L90
	andl	$2139095040, %eax
	cmpl	$2139095040, %eax
	je	.L91
	ucomiss	.LC14(%rip), %xmm0
	jnb	.L92
	pushq	%r12
	pushq	%rbp
	xorl	%r12d, %r12d
	pushq	%rbx
	subq	$64, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 56(%rsp)
# 0 "" 2
#NO_APP
	movl	56(%rsp), %ebp
	movl	%ebp, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebp
	movl	%eax, 60(%rsp)
	jne	.L93
.L39:
	pxor	%xmm3, %xmm3
	movq	%rdi, %rbx
	movaps	%xmm0, %xmm2
	ucomiss	%xmm3, %xmm0
	ja	.L94
	ucomiss	.LC16(%rip), %xmm0
	jnb	.L95
	movss	.LC7(%rip), %xmm1
	movaps	%xmm0, %xmm5
	movss	.LC6(%rip), %xmm7
	andps	%xmm1, %xmm5
	movaps	%xmm1, %xmm4
	ucomiss	%xmm5, %xmm7
	jbe	.L45
	cvttss2si	%xmm0, %eax
	pxor	%xmm0, %xmm0
	andnps	%xmm2, %xmm4
	cvtsi2ss	%eax, %xmm0
	orps	%xmm4, %xmm0
.L45:
	movaps	%xmm0, %xmm4
	movss	.LC2(%rip), %xmm6
	movaps	%xmm1, %xmm5
	mulss	%xmm6, %xmm4
	movaps	%xmm4, %xmm8
	movaps	%xmm4, %xmm9
	andps	%xmm1, %xmm8
	ucomiss	%xmm8, %xmm7
	jbe	.L46
	cvttss2si	%xmm4, %eax
	pxor	%xmm4, %xmm4
	andnps	%xmm9, %xmm5
	cvtsi2ss	%eax, %xmm4
	orps	%xmm5, %xmm4
.L46:
	addss	%xmm4, %xmm4
	ucomiss	%xmm0, %xmm4
	jp	.L65
	movl	$-1, %eax
	je	.L47
.L65:
	movl	$1, %eax
.L47:
	movss	.LC17(%rip), %xmm4
	movl	%eax, (%rbx)
	ucomiss	%xmm2, %xmm4
	jb	.L83
	movss	.LC18(%rip), %xmm0
	mulss	%xmm0, %xmm0
	.p2align 4,,10
	.p2align 3
.L42:
	testb	%r12b, %r12b
	jne	.L96
.L58:
	andps	%xmm0, %xmm1
	ucomiss	.LC15(%rip), %xmm1
	jbe	.L59
	ucomiss	%xmm3, %xmm2
	jp	.L75
	je	.L59
.L75:
	movss	.LC9(%rip), %xmm1
	movl	(%rbx), %edx
	andps	%xmm1, %xmm0
	testl	%edx, %edx
	orps	.LC21(%rip), %xmm0
	js	.L97
	mulss	.LC15(%rip), %xmm0
.L29:
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movss	.LC15(%rip), %xmm0
	movl	$0, (%rdi)
	mulss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movss	.LC1(%rip), %xmm0
	movl	$0, (%rdi)
	divss	%xmm2, %xmm0
	movss	.LC7(%rip), %xmm1
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L59:
	ucomiss	%xmm3, %xmm0
	jp	.L29
	jne	.L29
	movss	.LC9(%rip), %xmm1
	movl	(%rbx), %eax
	andps	%xmm1, %xmm0
	testl	%eax, %eax
	orps	.LC22(%rip), %xmm0
	js	.L98
	mulss	.LC18(%rip), %xmm0
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$0, (%rdi)
	leaq	60(%rsp), %rdi
	movss	%xmm3, (%rsp)
	movss	%xmm0, 36(%rsp)
	call	gammaf_positive
	movl	60(%rsp), %edi
	call	__scalbnf@PLT
	movss	.LC7(%rip), %xmm1
	movss	36(%rsp), %xmm2
	movss	(%rsp), %xmm3
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L88:
	movss	.LC1(%rip), %xmm1
	movl	$0, (%rdi)
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$0, (%rdi)
	subss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	movl	$0, (%rdi)
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	subss	%xmm2, %xmm0
	ucomiss	.LC2(%rip), %xmm0
	jbe	.L51
	movss	.LC1(%rip), %xmm4
	subss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
.L51:
	movss	.LC19(%rip), %xmm4
	ucomiss	%xmm0, %xmm4
	movss	%xmm2, 40(%rsp)
	movaps	%xmm1, (%rsp)
	movss	%xmm3, 36(%rsp)
	jnb	.L99
	subss	%xmm0, %xmm6
	movss	.LC20(%rip), %xmm7
	movss	%xmm7, 44(%rsp)
	movaps	%xmm6, %xmm0
	mulss	%xmm7, %xmm0
	call	__cosf@PLT
	movaps	(%rsp), %xmm1
	movss	40(%rsp), %xmm2
	movss	36(%rsp), %xmm3
.L55:
	movaps	%xmm2, %xmm4
	leaq	60(%rsp), %rdi
	movss	%xmm3, 40(%rsp)
	movaps	%xmm1, 16(%rsp)
	movss	%xmm2, (%rsp)
	xorps	.LC9(%rip), %xmm4
	mulss	%xmm4, %xmm0
	movss	%xmm0, 36(%rsp)
	movaps	%xmm4, %xmm0
	call	gammaf_positive
	mulss	36(%rsp), %xmm0
	movss	44(%rsp), %xmm7
	movl	60(%rsp), %edi
	negl	%edi
	divss	%xmm0, %xmm7
	movaps	%xmm7, %xmm0
	call	__scalbnf@PLT
	movss	.LC18(%rip), %xmm4
	ucomiss	%xmm0, %xmm4
	movss	(%rsp), %xmm2
	movss	40(%rsp), %xmm3
	movaps	16(%rsp), %xmm1
	jbe	.L42
	movaps	%xmm0, %xmm4
	mulss	%xmm0, %xmm4
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L98:
	xorps	%xmm1, %xmm0
	mulss	.LC18(%rip), %xmm0
	xorps	%xmm1, %xmm0
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L99:
	movss	.LC20(%rip), %xmm6
	mulss	%xmm6, %xmm0
	movss	%xmm6, 44(%rsp)
	call	__sinf@PLT
	movss	36(%rsp), %xmm3
	movaps	(%rsp), %xmm1
	movss	40(%rsp), %xmm2
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L89:
	movss	.LC7(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC6(%rip), %xmm3
	andps	%xmm1, %xmm4
	movaps	%xmm0, %xmm2
	ucomiss	%xmm4, %xmm3
	jbe	.L33
	andnps	%xmm0, %xmm1
	orps	%xmm1, %xmm3
	addss	%xmm3, %xmm2
	subss	%xmm3, %xmm2
	orps	%xmm1, %xmm2
.L33:
	ucomiss	%xmm0, %xmm2
	jp	.L32
	jne	.L32
	subss	%xmm0, %xmm0
	movl	$0, (%rdi)
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L93:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r12d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L96:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	60(%rsp), %eax
	andl	$24576, %ebp
	andb	$-97, %ah
	orl	%eax, %ebp
	movl	%ebp, 60(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L97:
	xorps	%xmm1, %xmm0
	mulss	.LC15(%rip), %xmm0
	xorps	%xmm1, %xmm0
	jmp	.L29
	.size	__ieee754_gammaf_r, .-__ieee754_gammaf_r
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
	.align 4
.LC2:
	.long	1056964608
	.align 4
.LC3:
	.long	1069547520
	.align 4
.LC4:
	.long	1075838976
	.align 4
.LC5:
	.long	1082130432
	.align 4
.LC6:
	.long	1258291200
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC8:
	.long	1060439283
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC10:
	.long	1086918619
	.align 4
.LC11:
	.long	978324737
	.align 4
.LC12:
	.long	993397601
	.align 4
.LC13:
	.long	1034594987
	.align 4
.LC14:
	.long	1108344832
	.align 4
.LC15:
	.long	2139095039
	.align 4
.LC16:
	.long	3003121664
	.align 4
.LC17:
	.long	3257401344
	.align 4
.LC18:
	.long	8388608
	.align 4
.LC19:
	.long	1048576000
	.align 4
.LC20:
	.long	1078530011
	.section	.rodata.cst16
	.align 16
.LC21:
	.long	2139095039
	.long	0
	.long	0
	.long	0
	.align 16
.LC22:
	.long	8388608
	.long	0
	.long	0
	.long	0
