	.text
	.globl	__addtf3
	.globl	__lttf2
	.globl	__multf3
	.globl	__gttf2
	.globl	__divtf3
	.p2align 4,,15
	.globl	__asinhf128
	.type	__asinhf128, @function
__asinhf128:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %rbp
	movdqa	(%rsp), %xmm2
	shrq	$32, %rbp
	movl	%ebp, %ebx
	andl	$2147483647, %ebx
	cmpl	$2147418112, %ebx
	movaps	%xmm2, 16(%rsp)
	je	.L16
	cmpl	$1070006271, %ebx
	jg	.L4
	movdqa	(%rsp), %xmm0
	pand	.LC0(%rip), %xmm0
	movdqa	.LC1(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L17
.L5:
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	jle	.L18
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbx, %rdx
	movl	%eax, %eax
	salq	$32, %rdx
	orq	%rdx, %rax
	cmpl	$1077215232, %ebx
	movq	%rax, 24(%rsp)
	jg	.L19
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC3(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__addtf3@PLT
	cmpl	$1073741824, %ebx
	jle	.L10
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	call	__ieee754_logf128@PLT
.L9:
	testl	%ebp, %ebp
	jns	.L1
	pxor	.LC5(%rip), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_logf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__addtf3@PLT
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%rsp), %rax
	salq	$32, %rbx
	movdqa	(%rsp), %xmm1
	movl	%eax, %eax
	movdqa	%xmm1, %xmm0
	orq	%rbx, %rax
	movq	%rax, 24(%rsp)
	call	__multf3@PLT
	movdqa	.LC3(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	call	__addtf3@PLT
.L10:
	call	__ieee754_sqrtf128@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	call	__log1pf128@PLT
	jmp	.L9
	.size	__asinhf128, .-__asinhf128
	.weak	asinhf128
	.set	asinhf128,__asinhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.long	621136735
	.long	1233211794
	.long	139375215
	.long	2140429604
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC4:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
