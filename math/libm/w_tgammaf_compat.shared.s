	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__tgammaf
	.type	__tgammaf, @function
__tgammaf:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	movss	%xmm0, 12(%rsp)
	call	__ieee754_gammaf_r@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm4
	movss	12(%rsp), %xmm2
	ucomiss	%xmm4, %xmm3
	jb	.L2
	ucomiss	.LC2(%rip), %xmm0
	jp	.L3
	je	.L2
.L3:
	movl	28(%rsp), %eax
	testl	%eax, %eax
	jns	.L1
	xorps	.LC5(%rip), %xmm0
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movaps	%xmm2, %xmm4
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm3
	jnb	.L24
	ucomiss	.LC1(%rip), %xmm4
	jbe	.L3
	pxor	%xmm3, %xmm3
	ucomiss	%xmm2, %xmm3
	jbe	.L3
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L3
	movaps	%xmm2, %xmm4
	movss	.LC3(%rip), %xmm6
	movaps	%xmm2, %xmm5
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm6
	jbe	.L17
	cvttss2si	%xmm2, %eax
	pxor	%xmm4, %xmm4
	movss	.LC4(%rip), %xmm6
	andnps	%xmm2, %xmm1
	cvtsi2ss	%eax, %xmm4
	movaps	%xmm4, %xmm5
	cmpnless	%xmm2, %xmm5
	andps	%xmm6, %xmm5
	subss	%xmm5, %xmm4
	movaps	%xmm4, %xmm5
	orps	%xmm1, %xmm5
.L17:
	ucomiss	%xmm2, %xmm5
	jp	.L12
	je	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	ucomiss	%xmm3, %xmm0
	jp	.L15
	jne	.L15
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L24:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L3
	pxor	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm2
	jp	.L8
	je	.L27
.L8:
	movaps	%xmm2, %xmm4
	movss	.LC3(%rip), %xmm6
	movaps	%xmm2, %xmm5
	andps	%xmm1, %xmm4
	ucomiss	%xmm4, %xmm6
	jbe	.L11
	cvttss2si	%xmm2, %eax
	pxor	%xmm4, %xmm4
	movss	.LC4(%rip), %xmm6
	andnps	%xmm2, %xmm1
	cvtsi2ss	%eax, %xmm4
	movaps	%xmm4, %xmm5
	cmpnless	%xmm2, %xmm5
	andps	%xmm6, %xmm5
	subss	%xmm5, %xmm4
	movaps	%xmm4, %xmm5
	orps	%xmm1, %xmm5
.L11:
	ucomiss	%xmm2, %xmm5
	jp	.L12
	jne	.L12
	ucomiss	%xmm2, %xmm3
	jbe	.L12
.L18:
	movaps	%xmm2, %xmm1
	movl	$141, %edi
	movaps	%xmm2, %xmm0
	call	__kernel_standard_f@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movaps	%xmm2, %xmm1
	movl	$150, %edi
	movaps	%xmm2, %xmm0
	call	__kernel_standard_f@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movaps	%xmm2, %xmm1
	movl	$140, %edi
	movaps	%xmm2, %xmm0
	call	__kernel_standard_f@PLT
	jmp	.L1
	.size	__tgammaf, .-__tgammaf
	.weak	tgammaf32
	.set	tgammaf32,__tgammaf
	.weak	tgammaf
	.set	tgammaf,__tgammaf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	0
	.align 4
.LC3:
	.long	1258291200
	.align 4
.LC4:
	.long	1065353216
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	2147483648
	.long	0
	.long	0
	.long	0
