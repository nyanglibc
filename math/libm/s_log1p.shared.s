	.text
	.p2align 4,,15
	.globl	__log1p
	.type	__log1p, @function
__log1p:
	movq	%xmm0, %rax
	shrq	$32, %rax
	cmpl	$1071284857, %eax
	movl	%eax, %edx
	jg	.L2
	andl	$2147483647, %edx
	cmpl	$1072693247, %edx
	jg	.L31
	cmpl	$1042284543, %edx
	jle	.L32
	addl	$1076707644, %eax
	cmpl	$1076707644, %eax
	jbe	.L10
	movsd	.LC6(%rip), %xmm6
	xorl	%eax, %eax
	mulsd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm6
.L11:
	movsd	.LC11(%rip), %xmm1
	movapd	%xmm0, %xmm3
	movsd	.LC12(%rip), %xmm4
	testl	%eax, %eax
	addsd	%xmm0, %xmm1
	movsd	.LC14(%rip), %xmm7
	divsd	%xmm1, %xmm3
	movapd	%xmm3, %xmm2
	movapd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm4
	movapd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm7
	mulsd	%xmm2, %xmm3
	addsd	.LC13(%rip), %xmm4
	movapd	%xmm3, %xmm8
	mulsd	%xmm3, %xmm8
	mulsd	%xmm3, %xmm4
	mulsd	%xmm8, %xmm3
	addsd	%xmm7, %xmm4
	movsd	.LC15(%rip), %xmm7
	mulsd	%xmm2, %xmm7
	mulsd	.LC17(%rip), %xmm2
	addsd	.LC16(%rip), %xmm7
	addsd	.LC18(%rip), %xmm2
	mulsd	%xmm8, %xmm7
	mulsd	%xmm3, %xmm2
	addsd	%xmm7, %xmm4
	addsd	%xmm2, %xmm4
	addsd	%xmm6, %xmm4
	mulsd	%xmm1, %xmm4
	je	.L33
	pxor	%xmm1, %xmm1
	movsd	.LC9(%rip), %xmm3
	cvtsi2sd	%eax, %xmm1
	mulsd	%xmm1, %xmm3
	mulsd	.LC8(%rip), %xmm1
	addsd	%xmm5, %xmm1
	addsd	%xmm4, %xmm1
	subsd	%xmm1, %xmm6
	subsd	%xmm0, %xmm6
	subsd	%xmm6, %xmm3
	movapd	%xmm3, %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$2146435071, %eax
	jg	.L34
	cmpl	$1128267775, %eax
	jle	.L10
	pxor	%xmm5, %xmm5
	sarl	$20, %eax
	movapd	%xmm0, %xmm4
	movsd	.LC7(%rip), %xmm7
	subl	$1023, %eax
.L16:
	andl	$1048575, %edx
	cmpl	$434333, %edx
	jg	.L17
	movq	%xmm4, %rcx
	movl	%edx, %esi
	orl	$1072693248, %esi
	salq	$32, %rsi
	movl	%ecx, %ecx
	orq	%rsi, %rcx
	movq	%rcx, -8(%rsp)
.L18:
	movsd	-8(%rsp), %xmm0
	testl	%edx, %edx
	movsd	.LC6(%rip), %xmm6
	subsd	%xmm7, %xmm0
	mulsd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm6
	jne	.L11
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm0
	jp	.L19
	jne	.L19
	testl	%eax, %eax
	pxor	%xmm0, %xmm0
	je	.L1
	pxor	%xmm3, %xmm3
	movsd	.LC8(%rip), %xmm0
	cvtsi2sd	%eax, %xmm3
	mulsd	%xmm3, %xmm0
	mulsd	.LC9(%rip), %xmm3
	addsd	%xmm5, %xmm0
	addsd	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%edx, %esi
	movq	%xmm4, %rcx
	addl	$1, %eax
	orl	$1071644672, %esi
	salq	$32, %rsi
	movl	%ecx, %ecx
	orq	%rsi, %rcx
	movq	%rcx, -8(%rsp)
	movl	$1048576, %ecx
	subl	%edx, %ecx
	movl	%ecx, %edx
	sarl	$2, %edx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L10:
	movsd	.LC7(%rip), %xmm7
	movapd	%xmm0, %xmm4
	addsd	%xmm7, %xmm4
	movq	%xmm4, %rax
	shrq	$32, %rax
	movl	%eax, %edx
	sarl	$20, %eax
	subl	$1023, %eax
	testl	%eax, %eax
	jle	.L14
	movapd	%xmm4, %xmm5
	subsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
	movapd	%xmm7, %xmm5
	subsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
.L15:
	movapd	%xmm0, %xmm5
	divsd	%xmm4, %xmm5
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L19:
	movsd	.LC10(%rip), %xmm1
	testl	%eax, %eax
	mulsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm7
	mulsd	%xmm6, %xmm7
	jne	.L21
	subsd	%xmm7, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	pxor	%xmm2, %xmm2
	movsd	.LC9(%rip), %xmm3
	cvtsi2sd	%eax, %xmm2
	mulsd	%xmm2, %xmm3
	mulsd	.LC8(%rip), %xmm2
	addsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm7
	subsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	ucomisd	.LC1(%rip), %xmm0
	jp	.L4
	jne	.L4
	movsd	.LC2(%rip), %xmm0
	divsd	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	subsd	%xmm4, %xmm6
	subsd	%xmm6, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movapd	%xmm4, %xmm1
	subsd	%xmm7, %xmm1
	subsd	%xmm1, %xmm0
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L4:
	subsd	%xmm0, %xmm0
	divsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movsd	.LC3(%rip), %xmm1
	addsd	%xmm0, %xmm1
	cmpl	$1016070143, %edx
	movapd	%xmm0, %xmm1
	jg	.L8
	andpd	.LC4(%rip), %xmm1
	movsd	.LC5(%rip), %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.L1
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	mulsd	%xmm0, %xmm1
	mulsd	.LC6(%rip), %xmm1
	subsd	%xmm1, %xmm0
	ret
	.size	__log1p, .-__log1p
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	-1074790400
	.align 8
.LC2:
	.long	0
	.long	-1018167296
	.align 8
.LC3:
	.long	0
	.long	1129316352
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1048576
	.align 8
.LC6:
	.long	0
	.long	1071644672
	.align 8
.LC7:
	.long	0
	.long	1072693248
	.align 8
.LC8:
	.long	897137782
	.long	1038760431
	.align 8
.LC9:
	.long	4276092928
	.long	1072049730
	.align 8
.LC10:
	.long	1431655765
	.long	1071994197
	.align 8
.LC11:
	.long	0
	.long	1073741824
	.align 8
.LC12:
	.long	2485293913
	.long	1070745892
	.align 8
.LC13:
	.long	2576873988
	.long	1071225241
	.align 8
.LC14:
	.long	1431655827
	.long	1071994197
	.align 8
.LC15:
	.long	2529887198
	.long	1070024292
	.align 8
.LC16:
	.long	495876271
	.long	1070363077
	.align 8
.LC17:
	.long	3745403460
	.long	1069740306
	.align 8
.LC18:
	.long	3497576095
	.long	1069783561
