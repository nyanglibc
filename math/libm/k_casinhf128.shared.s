	.text
	.globl	__getf2
	.globl	__addtf3
	.globl	__lttf2
	.globl	__subtf3
	.globl	__multf3
	.globl	__gttf2
	.globl	__divtf3
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__kernel_casinhf128
	.type	__kernel_casinhf128, @function
__kernel_casinhf128:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$176, %rsp
	movdqa	224(%rsp), %xmm5
	movdqa	.LC1(%rip), %xmm0
	movdqa	208(%rsp), %xmm4
	pand	%xmm0, %xmm4
	pand	%xmm5, %xmm0
	movdqa	.LC2(%rip), %xmm1
	movaps	%xmm4, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movdqa	%xmm4, %xmm0
	movaps	%xmm5, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L2
	movdqa	.LC2(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L67
.L2:
	testl	%ebp, %ebp
	jne	.L5
	movdqa	16(%rsp), %xmm0
	movdqa	(%rsp), %xmm1
.L6:
	leaq	144(%rsp), %rdi
	subq	$32, %rsp
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	call	__clogf128@PLT
	movdqa	192(%rsp), %xmm2
	movaps	%xmm2, 48(%rsp)
	addq	$32, %rsp
	movdqa	.LC3(%rip), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	208(%rsp), %xmm1
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
.L7:
	testl	%ebp, %ebp
	je	.L47
	movdqa	.LC0(%rip), %xmm3
	movaps	%xmm3, 32(%rsp)
.L47:
	movdqa	%xmm2, %xmm0
	movdqa	32(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	(%rsp), %xmm7
	movq	%rbx, %rax
	movaps	%xmm7, (%rbx)
	movaps	%xmm0, 16(%rbx)
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movdqa	224(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L67:
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L49
	movdqa	.LC5(%rip), %xmm5
	movaps	%xmm5, 64(%rsp)
.L8:
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L14
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L80
.L14:
	movdqa	.LC0(%rip), %xmm3
	movdqa	(%rsp), %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm3, 48(%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L68
	movdqa	.LC6(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L20
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L20
	movdqa	48(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L69
	movdqa	64(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	testl	%ebp, %ebp
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	je	.L25
	movdqa	%xmm2, %xmm0
	movdqa	224(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__ieee754_atan2f128@PLT
	movdqa	(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L49:
	movdqa	.LC5(%rip), %xmm3
	movdqa	(%rsp), %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm3, 64(%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L8
	movdqa	.LC0(%rip), %xmm0
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	call	__ieee754_hypotf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__addtf3@PLT
	call	__ieee754_logf128@PLT
	testl	%ebp, %ebp
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm2
	je	.L11
	movdqa	%xmm2, %xmm0
	movdqa	224(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	64(%rsp), %xmm2
.L12:
	movdqa	%xmm2, %xmm0
	movdqa	208(%rsp), %xmm1
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
.L76:
	movdqa	48(%rsp), %xmm7
	movaps	%xmm7, 32(%rsp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	48(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L28
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L28
	movdqa	.LC10(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	js	.L71
	movdqa	48(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L72
	movdqa	64(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm6
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__divtf3@PLT
.L78:
	call	__log1pf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	testl	%ebp, %ebp
	je	.L44
	movaps	%xmm0, (%rsp)
	movdqa	224(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__ieee754_atan2f128@PLT
	movdqa	(%rsp), %xmm2
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm2, %xmm1
.L41:
	movdqa	%xmm1, %xmm6
	movdqa	208(%rsp), %xmm1
	movdqa	%xmm6, %xmm0
	movaps	%xmm6, 16(%rsp)
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC11(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	64(%rsp), %xmm2
	jns	.L76
.L48:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	movaps	%xmm2, 48(%rsp)
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm2
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L68:
	movdqa	48(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L20
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L81
.L28:
	movdqa	(%rsp), %xmm1
	leaq	144(%rsp), %r12
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC0(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm3
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	subq	$32, %rsp
	movq	%r12, %rdi
	movdqa	80(%rsp), %xmm2
	movups	%xmm0, 16(%rsp)
	movups	%xmm2, (%rsp)
	call	__csqrtf128@PLT
	movdqa	48(%rsp), %xmm1
	movdqa	176(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	192(%rsp), %xmm0
	call	__addtf3@PLT
	addq	$32, %rsp
	testl	%ebp, %ebp
	jne	.L45
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
.L46:
	subq	$32, %rsp
	movq	%r12, %rdi
	movups	%xmm2, (%rsp)
	movups	%xmm1, 16(%rsp)
	call	__clogf128@PLT
	addq	$32, %rsp
	movdqa	208(%rsp), %xmm1
	movdqa	144(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	160(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L45:
	movdqa	224(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm0, %xmm2
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L81:
	movdqa	64(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L70
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	testl	%ebp, %ebp
	movaps	%xmm0, (%rsp)
	je	.L32
	movdqa	224(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	32(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L80:
	movdqa	.LC0(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC0(%rip), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	call	__addtf3@PLT
	call	__ieee754_logf128@PLT
	testl	%ebp, %ebp
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm2
	je	.L25
	movdqa	%xmm2, %xmm0
	movdqa	224(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__ieee754_atan2f128@PLT
	movdqa	.LC0(%rip), %xmm6
	movdqa	(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm6, 48(%rsp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	%xmm2, %xmm0
	movdqa	16(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	(%rsp), %xmm2
.L13:
	movdqa	%xmm2, %xmm0
	movdqa	208(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L11:
	movdqa	%xmm2, %xmm1
	movdqa	(%rsp), %xmm0
	call	__ieee754_atan2f128@PLT
	movdqa	64(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L70:
	movdqa	208(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	call	__addtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, 80(%rsp)
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	96(%rsp), %xmm3
	movdqa	64(%rsp), %xmm1
	movdqa	%xmm3, %xmm0
	call	__subtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	testl	%ebp, %ebp
	movaps	%xmm0, 80(%rsp)
	je	.L33
	movdqa	48(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	224(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L69:
	movdqa	208(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, 80(%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm7
	movaps	%xmm0, 96(%rsp)
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 112(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	128(%rsp), %xmm2
	movaps	%xmm0, 112(%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	112(%rsp), %xmm0
	call	__addtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	testl	%ebp, %ebp
	movaps	%xmm0, 80(%rsp)
	je	.L26
	movdqa	(%rsp), %xmm1
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	224(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L12
.L72:
	movdqa	208(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, 80(%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm7
	movaps	%xmm0, 96(%rsp)
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	64(%rsp), %xmm2
	movaps	%xmm0, 112(%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	call	__ieee754_sqrtf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	128(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 128(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	128(%rsp), %xmm0
	call	__addtf3@PLT
	call	__log1pf128@PLT
	movdqa	.LC4(%rip), %xmm1
	call	__multf3@PLT
	testl	%ebp, %ebp
	je	.L43
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	224(%rsp), %xmm1
	call	__copysignf128@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm2, %xmm1
	jmp	.L41
.L44:
	movaps	%xmm0, 16(%rsp)
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__ieee754_atan2f128@PLT
	movdqa	16(%rsp), %xmm2
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm2, %xmm1
.L42:
	movdqa	%xmm1, %xmm6
	movdqa	208(%rsp), %xmm1
	movdqa	%xmm6, %xmm0
	movaps	%xmm6, 16(%rsp)
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	.LC11(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	48(%rsp), %xmm2
	jns	.L47
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L71:
	movdqa	16(%rsp), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__ieee754_hypotf128@PLT
	movdqa	16(%rsp), %xmm6
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L78
.L32:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_sqrtf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__ieee754_atan2f128@PLT
	movdqa	(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L13
.L33:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L13
.L26:
	movdqa	16(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 16(%rsp)
	jmp	.L13
.L43:
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__ieee754_atan2f128@PLT
	movdqa	48(%rsp), %xmm2
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm2, %xmm1
	jmp	.L42
	.size	__kernel_casinhf128, .-__kernel_casinhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1081016320
	.align 16
.LC3:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1066139648
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1073709056
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1058996224
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073807360
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1066336256
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	65536
