	.text
	.p2align 4,,15
	.globl	__llogbf128
	.type	__llogbf128, @function
__llogbf128:
	pushq	%rbx
	call	__ieee754_ilogbf128@PLT
	cmpl	$-2147483648, %eax
	je	.L4
	cmpl	$2147483647, %eax
	movslq	%eax, %rbx
	je	.L7
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movabsq	$-9223372036854775808, %rbx
.L2:
	movq	errno@gottpoff(%rip), %rax
	movl	$1, %edi
	movl	$33, %fs:(%rax)
	call	__feraiseexcept@PLT
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movabsq	$9223372036854775807, %rbx
	jmp	.L2
	.size	__llogbf128, .-__llogbf128
	.weak	llogbf128
	.set	llogbf128,__llogbf128
