	.text
	.p2align 4,,15
	.type	tanMp, @function
tanMp:
	pushq	%rbx
	movl	$32, %esi
	subq	$352, %rsp
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	call	__mptan@PLT
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	movl	$32, %edx
	call	__mp_dbl@PLT
	movsd	8(%rsp), %xmm0
	addq	$352, %rsp
	popq	%rbx
	ret
	.size	tanMp, .-tanMp
	.p2align 4,,15
	.globl	__tan
	.type	__tan, @function
__tan:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$1096, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 416(%rsp)
# 0 "" 2
#NO_APP
	movl	416(%rsp), %ebx
	movl	%ebx, %eax
	andb	$-97, %ah
	cmpl	%eax, %ebx
	movl	%eax, 752(%rsp)
	jne	.L526
.L5:
	movq	%xmm0, %rax
	sarq	$32, %rax
	movl	%eax, %edx
	andl	$2146435072, %edx
	cmpl	$2146435072, %edx
	je	.L527
	pxor	%xmm1, %xmm1
	movapd	%xmm0, %xmm2
	ucomisd	%xmm0, %xmm1
	ja	.L528
	movsd	g1.6411(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	jb	.L422
.L533:
	movsd	.LC4(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	ja	.L529
.L8:
	testb	%bpl, %bpl
	jne	.L530
.L4:
	addq	$1096, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	movapd	%xmm0, %xmm1
	movsd	g2.6412(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	jnb	.L531
	movsd	g3.6413(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	jb	.L432
	movsd	.LC7(%rip), %xmm0
	leaq	xfg.6467(%rip), %rcx
	pxor	%xmm6, %xmm6
	mulsd	%xmm2, %xmm0
	addsd	mfftnhf.6410(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	movsd	.LC0(%rip), %xmm0
	cltq
	movq	%rax, %rdx
	salq	$5, %rdx
	subsd	(%rcx,%rdx), %xmm2
	ucomisd	%xmm1, %xmm6
	movapd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm5
	ja	.L41
	movsd	.LC1(%rip), %xmm0
.L41:
	movsd	e1.6409(%rip), %xmm6
	movapd	%xmm2, %xmm3
	movq	%rax, %rdx
	mulsd	%xmm5, %xmm6
	salq	$5, %rdx
	mulsd	%xmm5, %xmm3
	addq	%rcx, %rdx
	movsd	8(%rdx), %xmm4
	movsd	16(%rdx), %xmm7
	addsd	e0.6408(%rip), %xmm6
	mulsd	%xmm3, %xmm6
	movapd	%xmm4, %xmm3
	addsd	%xmm7, %xmm3
	addsd	%xmm2, %xmm6
	subsd	%xmm6, %xmm7
	mulsd	%xmm6, %xmm3
	movsd	u3.6420(%rip), %xmm6
	mulsd	%xmm4, %xmm6
	divsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm3, %xmm6
	addsd	%xmm4, %xmm7
	addsd	%xmm4, %xmm6
	ucomisd	%xmm7, %xmm6
	jp	.L43
	je	.L505
.L43:
	pxor	%xmm6, %xmm6
	ucomisd	%xmm3, %xmm6
	movapd	%xmm3, %xmm6
	jbe	.L45
	xorpd	.LC3(%rip), %xmm6
.L45:
	mulsd	ub3.6422(%rip), %xmm6
	movapd	%xmm6, %xmm7
	movsd	ua3.6421(%rip), %xmm6
	mulsd	%xmm4, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm3, %xmm7
	addsd	%xmm6, %xmm3
	subsd	%xmm6, %xmm7
	addsd	%xmm4, %xmm3
	addsd	%xmm4, %xmm7
	ucomisd	%xmm7, %xmm3
	jp	.L47
	je	.L505
.L47:
	salq	$5, %rax
	movapd	%xmm2, %xmm8
	movsd	24(%rcx,%rax), %xmm6
	movsd	a11.6397(%rip), %xmm9
	movapd	%xmm2, %xmm11
	movsd	%xmm6, 16(%rsp)
	movsd	.LC5(%rip), %xmm6
	mulsd	%xmm5, %xmm9
	movapd	%xmm6, %xmm3
	movsd	a5.6391(%rip), %xmm10
	movsd	%xmm6, (%rsp)
	mulsd	%xmm2, %xmm3
	movapd	%xmm10, %xmm7
	addsd	a9.6395(%rip), %xmm9
	movsd	aa5.6392(%rip), %xmm6
	subsd	%xmm3, %xmm8
	mulsd	%xmm5, %xmm9
	addsd	%xmm3, %xmm8
	addsd	a7.6393(%rip), %xmm9
	movapd	%xmm8, %xmm13
	movapd	%xmm8, %xmm3
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm13
	mulsd	%xmm11, %xmm3
	subsd	%xmm5, %xmm13
	mulsd	%xmm5, %xmm9
	addsd	%xmm3, %xmm13
	movapd	%xmm9, %xmm12
	addsd	%xmm9, %xmm7
	addsd	%xmm3, %xmm13
	movapd	%xmm11, %xmm3
	mulsd	%xmm11, %xmm3
	addsd	%xmm3, %xmm13
	movq	.LC6(%rip), %xmm3
	andpd	%xmm3, %xmm12
	movsd	%xmm13, 24(%rsp)
	movapd	%xmm10, %xmm13
	andpd	%xmm3, %xmm13
	ucomisd	%xmm12, %xmm13
	jbe	.L435
	subsd	%xmm7, %xmm10
	pxor	%xmm15, %xmm15
	addsd	%xmm10, %xmm9
	addsd	%xmm15, %xmm9
	addsd	%xmm9, %xmm6
.L51:
	movsd	(%rsp), %xmm15
	movapd	%xmm6, %xmm12
	movapd	%xmm5, %xmm10
	movapd	%xmm15, %xmm9
	movapd	%xmm5, %xmm14
	addsd	%xmm7, %xmm12
	mulsd	%xmm5, %xmm9
	mulsd	%xmm12, %xmm15
	movapd	%xmm12, %xmm13
	subsd	%xmm9, %xmm10
	subsd	%xmm12, %xmm7
	subsd	%xmm15, %xmm13
	addsd	%xmm10, %xmm9
	movapd	%xmm15, %xmm10
	addsd	%xmm6, %xmm7
	addsd	%xmm13, %xmm10
	movapd	%xmm12, %xmm13
	subsd	%xmm9, %xmm14
	movapd	%xmm9, %xmm15
	mulsd	24(%rsp), %xmm12
	mulsd	%xmm5, %xmm7
	subsd	%xmm10, %xmm13
	movsd	%xmm14, 8(%rsp)
	movapd	%xmm9, %xmm14
	mulsd	%xmm10, %xmm14
	mulsd	8(%rsp), %xmm10
	addsd	%xmm7, %xmm12
	mulsd	%xmm13, %xmm15
	mulsd	8(%rsp), %xmm13
	addsd	%xmm10, %xmm15
	movapd	%xmm14, %xmm10
	addsd	%xmm15, %xmm10
	subsd	%xmm10, %xmm14
	movapd	%xmm10, %xmm6
	addsd	%xmm15, %xmm14
	movsd	aa3.6390(%rip), %xmm15
	addsd	%xmm13, %xmm14
	addsd	%xmm12, %xmm14
	movsd	a3.6389(%rip), %xmm12
	movapd	%xmm12, %xmm7
	addsd	%xmm14, %xmm6
	subsd	%xmm6, %xmm10
	movapd	%xmm6, %xmm13
	addsd	%xmm6, %xmm7
	andpd	%xmm3, %xmm13
	addsd	%xmm14, %xmm10
	movapd	%xmm12, %xmm14
	andpd	%xmm3, %xmm14
	ucomisd	%xmm13, %xmm14
	jbe	.L436
	subsd	%xmm7, %xmm12
	addsd	%xmm6, %xmm12
	addsd	%xmm12, %xmm10
	addsd	%xmm15, %xmm10
.L54:
	movapd	%xmm10, %xmm13
	movsd	(%rsp), %xmm15
	addsd	%xmm7, %xmm13
	movapd	%xmm15, %xmm6
	mulsd	%xmm13, %xmm6
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	subsd	%xmm13, %xmm7
	mulsd	24(%rsp), %xmm13
	subsd	%xmm6, %xmm12
	addsd	%xmm10, %xmm7
	addsd	%xmm12, %xmm6
	movapd	%xmm9, %xmm12
	mulsd	%xmm7, %xmm5
	subsd	%xmm6, %xmm14
	mulsd	%xmm6, %xmm12
	mulsd	8(%rsp), %xmm6
	addsd	%xmm13, %xmm5
	mulsd	%xmm14, %xmm9
	mulsd	8(%rsp), %xmm14
	addsd	%xmm6, %xmm9
	movapd	%xmm12, %xmm6
	addsd	%xmm9, %xmm6
	subsd	%xmm6, %xmm12
	movapd	%xmm6, %xmm10
	addsd	%xmm9, %xmm12
	addsd	%xmm14, %xmm12
	addsd	%xmm5, %xmm12
	movapd	%xmm15, %xmm5
	addsd	%xmm12, %xmm10
	mulsd	%xmm10, %xmm5
	movapd	%xmm10, %xmm7
	movapd	%xmm10, %xmm9
	subsd	%xmm10, %xmm6
	subsd	%xmm5, %xmm7
	addsd	%xmm12, %xmm6
	addsd	%xmm5, %xmm7
	movapd	%xmm8, %xmm5
	subsd	%xmm7, %xmm9
	mulsd	%xmm7, %xmm5
	mulsd	%xmm11, %xmm7
	mulsd	%xmm9, %xmm8
	mulsd	%xmm9, %xmm11
	addsd	%xmm7, %xmm8
	movapd	%xmm5, %xmm7
	addsd	%xmm8, %xmm7
	subsd	%xmm7, %xmm5
	addsd	%xmm5, %xmm8
	pxor	%xmm5, %xmm5
	mulsd	%xmm5, %xmm10
	movapd	%xmm7, %xmm5
	addsd	%xmm11, %xmm8
	movapd	%xmm6, %xmm11
	movapd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm11
	addsd	%xmm10, %xmm11
	addsd	%xmm8, %xmm11
	movapd	%xmm2, %xmm8
	andpd	%xmm3, %xmm8
	addsd	%xmm11, %xmm5
	subsd	%xmm5, %xmm7
	addsd	%xmm5, %xmm6
	addsd	%xmm7, %xmm11
	movapd	%xmm5, %xmm7
	andpd	%xmm3, %xmm7
	ucomisd	%xmm7, %xmm8
	jbe	.L437
	subsd	%xmm6, %xmm2
	addsd	%xmm2, %xmm5
	addsd	%xmm5, %xmm11
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm11
.L57:
	movapd	%xmm11, %xmm8
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm8
	andpd	%xmm3, %xmm5
	movapd	%xmm8, %xmm2
	subsd	%xmm8, %xmm6
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm5
	addsd	%xmm6, %xmm11
	movapd	%xmm4, %xmm6
	addsd	%xmm8, %xmm6
	jbe	.L438
	movapd	%xmm4, %xmm2
	subsd	%xmm6, %xmm2
	addsd	%xmm8, %xmm2
	addsd	%xmm11, %xmm2
	addsd	16(%rsp), %xmm2
.L60:
	movapd	%xmm2, %xmm5
	movsd	(%rsp), %xmm15
	movapd	%xmm4, %xmm7
	addsd	%xmm6, %xmm5
	movapd	%xmm8, %xmm10
	movapd	%xmm4, %xmm9
	subsd	%xmm5, %xmm6
	addsd	%xmm2, %xmm6
	movapd	%xmm15, %xmm2
	mulsd	%xmm4, %xmm2
	mulsd	%xmm11, %xmm4
	subsd	%xmm2, %xmm7
	addsd	%xmm7, %xmm2
	movapd	%xmm15, %xmm7
	mulsd	%xmm8, %xmm7
	subsd	%xmm2, %xmm9
	movapd	%xmm2, %xmm12
	subsd	%xmm7, %xmm10
	addsd	%xmm10, %xmm7
	movapd	%xmm8, %xmm10
	mulsd	16(%rsp), %xmm8
	subsd	%xmm7, %xmm10
	mulsd	%xmm7, %xmm12
	mulsd	%xmm9, %xmm7
	addsd	%xmm8, %xmm4
	mulsd	%xmm10, %xmm2
	mulsd	%xmm10, %xmm9
	addsd	%xmm7, %xmm2
	movapd	%xmm12, %xmm7
	addsd	%xmm2, %xmm7
	subsd	%xmm7, %xmm12
	movapd	%xmm7, %xmm8
	addsd	%xmm12, %xmm2
	addsd	%xmm9, %xmm2
	addsd	%xmm2, %xmm4
	movsd	.LC1(%rip), %xmm2
	addsd	%xmm4, %xmm8
	andpd	%xmm8, %xmm3
	subsd	%xmm8, %xmm7
	ucomisd	%xmm3, %xmm2
	addsd	%xmm7, %xmm4
	movapd	%xmm2, %xmm7
	subsd	%xmm8, %xmm7
	ja	.L532
	addsd	%xmm7, %xmm8
	movapd	%xmm2, %xmm3
	pxor	%xmm2, %xmm2
	subsd	%xmm8, %xmm3
	addsd	%xmm2, %xmm3
	subsd	%xmm4, %xmm3
.L63:
	movapd	%xmm3, %xmm8
	movapd	%xmm5, %xmm2
	movsd	(%rsp), %xmm15
	addsd	%xmm7, %xmm8
	movapd	%xmm15, %xmm9
	divsd	%xmm8, %xmm2
	movapd	%xmm8, %xmm12
	movapd	%xmm8, %xmm11
	subsd	%xmm8, %xmm7
	addsd	%xmm7, %xmm3
	mulsd	%xmm8, %xmm15
	mulsd	%xmm2, %xmm9
	movapd	%xmm2, %xmm4
	subsd	%xmm15, %xmm12
	movapd	%xmm2, %xmm10
	mulsd	%xmm2, %xmm3
	subsd	%xmm9, %xmm4
	addsd	%xmm15, %xmm12
	addsd	%xmm9, %xmm4
	subsd	%xmm12, %xmm11
	subsd	%xmm4, %xmm10
	movapd	%xmm4, %xmm9
	mulsd	%xmm11, %xmm4
	mulsd	%xmm12, %xmm9
	mulsd	%xmm10, %xmm12
	mulsd	%xmm11, %xmm10
	addsd	%xmm12, %xmm4
	movapd	%xmm9, %xmm12
	addsd	%xmm4, %xmm12
	subsd	%xmm12, %xmm9
	subsd	%xmm12, %xmm5
	addsd	%xmm9, %xmm4
	addsd	%xmm10, %xmm4
	subsd	%xmm4, %xmm5
	movapd	%xmm2, %xmm4
	addsd	%xmm6, %xmm5
	subsd	%xmm3, %xmm5
	movapd	%xmm5, %xmm3
	divsd	%xmm8, %xmm3
	addsd	%xmm3, %xmm4
	subsd	%xmm4, %xmm2
	addsd	%xmm3, %xmm2
	movsd	u4.6423(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	movapd	%xmm2, %xmm5
	subsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm5
	addsd	%xmm4, %xmm3
	ucomisd	%xmm5, %xmm3
	jp	.L257
	jne	.L257
	mulsd	%xmm5, %xmm0
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L528:
	xorpd	.LC3(%rip), %xmm2
	movsd	g1.6411(%rip), %xmm1
	ucomisd	%xmm2, %xmm1
	jb	.L422
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L527:
	andl	$2147483647, %eax
	cmpl	$2146435072, %eax
	jne	.L7
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L7:
	testb	%bpl, %bpl
	subsd	%xmm0, %xmm0
	je	.L4
.L530:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	movl	60(%rsp), %eax
	andl	$24576, %ebx
	andb	$-97, %ah
	orl	%eax, %ebx
	movl	%ebx, 60(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 60(%rsp)
# 0 "" 2
#NO_APP
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L529:
	mulsd	%xmm2, %xmm2
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L531:
	movapd	%xmm1, %xmm2
	movsd	d11.6388(%rip), %xmm0
	movapd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm3
	addsd	d9.6387(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	d7.6386(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	d5.6385(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	d3.6384(%rip), %xmm0
	mulsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm4
	mulsd	u1.6418(%rip), %xmm3
	movapd	%xmm4, %xmm0
	subsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm3
	addsd	%xmm1, %xmm0
	addsd	%xmm1, %xmm3
	ucomisd	%xmm0, %xmm3
	jp	.L313
	je	.L8
.L313:
	movsd	a27.6407(%rip), %xmm4
	movapd	%xmm1, %xmm8
	movsd	.LC5(%rip), %xmm5
	mulsd	%xmm2, %xmm4
	movapd	%xmm1, %xmm9
	movapd	%xmm5, %xmm0
	movsd	%xmm5, (%rsp)
	movq	.LC6(%rip), %xmm3
	mulsd	%xmm1, %xmm0
	addsd	a25.6406(%rip), %xmm4
	movsd	aa13.6400(%rip), %xmm6
	subsd	%xmm0, %xmm8
	mulsd	%xmm2, %xmm4
	addsd	%xmm0, %xmm8
	addsd	a23.6405(%rip), %xmm4
	movapd	%xmm8, %xmm5
	movapd	%xmm8, %xmm0
	subsd	%xmm8, %xmm9
	mulsd	%xmm8, %xmm5
	mulsd	%xmm2, %xmm4
	mulsd	%xmm9, %xmm0
	subsd	%xmm2, %xmm5
	addsd	a21.6404(%rip), %xmm4
	addsd	%xmm0, %xmm5
	addsd	%xmm0, %xmm5
	movapd	%xmm9, %xmm0
	mulsd	%xmm2, %xmm4
	mulsd	%xmm9, %xmm0
	addsd	a19.6403(%rip), %xmm4
	addsd	%xmm0, %xmm5
	movsd	a13.6399(%rip), %xmm0
	movapd	%xmm0, %xmm11
	movapd	%xmm0, %xmm7
	andpd	%xmm3, %xmm11
	mulsd	%xmm2, %xmm4
	addsd	a17.6402(%rip), %xmm4
	mulsd	%xmm2, %xmm4
	addsd	a15.6401(%rip), %xmm4
	mulsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm10
	addsd	%xmm4, %xmm7
	andpd	%xmm3, %xmm10
	ucomisd	%xmm10, %xmm11
	jbe	.L425
	subsd	%xmm7, %xmm0
	addsd	%xmm4, %xmm0
	pxor	%xmm4, %xmm4
	addsd	%xmm4, %xmm0
	addsd	%xmm6, %xmm0
.L19:
	movapd	%xmm0, %xmm13
	movsd	(%rsp), %xmm15
	movapd	%xmm2, %xmm6
	addsd	%xmm7, %xmm13
	movapd	%xmm15, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm13, %xmm15
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	subsd	%xmm4, %xmm6
	subsd	%xmm13, %xmm7
	mulsd	%xmm5, %xmm13
	subsd	%xmm15, %xmm12
	addsd	%xmm6, %xmm4
	movapd	%xmm2, %xmm6
	addsd	%xmm15, %xmm12
	subsd	%xmm4, %xmm6
	movapd	%xmm4, %xmm11
	movapd	%xmm4, %xmm10
	subsd	%xmm12, %xmm14
	mulsd	%xmm12, %xmm11
	mulsd	%xmm6, %xmm12
	mulsd	%xmm14, %xmm10
	mulsd	%xmm6, %xmm14
	addsd	%xmm12, %xmm10
	movapd	%xmm11, %xmm12
	addsd	%xmm10, %xmm12
	subsd	%xmm12, %xmm11
	addsd	%xmm10, %xmm11
	movapd	%xmm14, %xmm10
	addsd	%xmm11, %xmm10
	movapd	%xmm7, %xmm11
	movapd	%xmm12, %xmm7
	addsd	%xmm0, %xmm11
	mulsd	%xmm2, %xmm11
	addsd	%xmm11, %xmm13
	movsd	a11.6397(%rip), %xmm11
	movapd	%xmm11, %xmm14
	movapd	%xmm11, %xmm0
	addsd	%xmm10, %xmm13
	andpd	%xmm3, %xmm14
	addsd	%xmm13, %xmm7
	movapd	%xmm7, %xmm10
	subsd	%xmm7, %xmm12
	andpd	%xmm3, %xmm10
	addsd	%xmm7, %xmm0
	ucomisd	%xmm10, %xmm14
	addsd	%xmm13, %xmm12
	movsd	aa11.6398(%rip), %xmm13
	jbe	.L426
	subsd	%xmm0, %xmm11
	movapd	%xmm11, %xmm10
	addsd	%xmm7, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm13, %xmm10
.L22:
	movapd	%xmm10, %xmm13
	movsd	(%rsp), %xmm7
	movapd	%xmm4, %xmm11
	addsd	%xmm0, %xmm13
	mulsd	%xmm13, %xmm7
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	subsd	%xmm13, %xmm0
	mulsd	%xmm5, %xmm13
	subsd	%xmm7, %xmm12
	addsd	%xmm7, %xmm12
	movapd	%xmm4, %xmm7
	subsd	%xmm12, %xmm14
	mulsd	%xmm12, %xmm11
	mulsd	%xmm6, %xmm12
	mulsd	%xmm14, %xmm7
	addsd	%xmm12, %xmm7
	movapd	%xmm11, %xmm12
	addsd	%xmm7, %xmm12
	subsd	%xmm12, %xmm11
	movapd	%xmm12, %xmm15
	addsd	%xmm7, %xmm11
	movapd	%xmm14, %xmm7
	mulsd	%xmm6, %xmm7
	addsd	%xmm11, %xmm7
	movapd	%xmm0, %xmm11
	addsd	%xmm10, %xmm11
	movsd	a9.6395(%rip), %xmm10
	movapd	%xmm10, %xmm0
	mulsd	%xmm2, %xmm11
	addsd	%xmm11, %xmm13
	movsd	aa9.6396(%rip), %xmm11
	addsd	%xmm7, %xmm13
	addsd	%xmm13, %xmm15
	subsd	%xmm15, %xmm12
	movapd	%xmm15, %xmm7
	addsd	%xmm15, %xmm0
	andpd	%xmm3, %xmm7
	addsd	%xmm13, %xmm12
	movapd	%xmm10, %xmm13
	andpd	%xmm3, %xmm13
	ucomisd	%xmm7, %xmm13
	jbe	.L427
	movapd	%xmm10, %xmm7
	subsd	%xmm0, %xmm7
	addsd	%xmm15, %xmm7
	addsd	%xmm12, %xmm7
	addsd	%xmm11, %xmm7
.L25:
	movapd	%xmm7, %xmm13
	movsd	(%rsp), %xmm10
	movapd	%xmm4, %xmm11
	addsd	%xmm0, %xmm13
	mulsd	%xmm13, %xmm10
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	subsd	%xmm13, %xmm0
	mulsd	%xmm5, %xmm13
	subsd	%xmm10, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm4, %xmm10
	subsd	%xmm12, %xmm14
	mulsd	%xmm12, %xmm11
	mulsd	%xmm6, %xmm12
	mulsd	%xmm14, %xmm10
	mulsd	%xmm6, %xmm14
	addsd	%xmm12, %xmm10
	movapd	%xmm11, %xmm12
	addsd	%xmm10, %xmm12
	subsd	%xmm12, %xmm11
	movapd	%xmm12, %xmm15
	addsd	%xmm10, %xmm11
	movapd	%xmm14, %xmm10
	addsd	%xmm11, %xmm10
	movapd	%xmm0, %xmm11
	addsd	%xmm7, %xmm11
	movsd	a7.6393(%rip), %xmm7
	movapd	%xmm7, %xmm0
	mulsd	%xmm2, %xmm11
	addsd	%xmm11, %xmm13
	movsd	aa7.6394(%rip), %xmm11
	addsd	%xmm10, %xmm13
	addsd	%xmm13, %xmm15
	subsd	%xmm15, %xmm12
	movapd	%xmm15, %xmm10
	addsd	%xmm15, %xmm0
	andpd	%xmm3, %xmm10
	addsd	%xmm13, %xmm12
	movapd	%xmm7, %xmm13
	andpd	%xmm3, %xmm13
	ucomisd	%xmm10, %xmm13
	jbe	.L428
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm10
	addsd	%xmm15, %xmm10
	addsd	%xmm12, %xmm10
	addsd	%xmm11, %xmm10
.L28:
	movapd	%xmm10, %xmm13
	movsd	(%rsp), %xmm7
	movapd	%xmm4, %xmm11
	addsd	%xmm0, %xmm13
	mulsd	%xmm13, %xmm7
	movapd	%xmm13, %xmm12
	movapd	%xmm13, %xmm14
	subsd	%xmm13, %xmm0
	mulsd	%xmm5, %xmm13
	subsd	%xmm7, %xmm12
	addsd	%xmm7, %xmm12
	movapd	%xmm4, %xmm7
	subsd	%xmm12, %xmm14
	mulsd	%xmm12, %xmm11
	mulsd	%xmm6, %xmm12
	mulsd	%xmm14, %xmm7
	addsd	%xmm12, %xmm7
	movapd	%xmm11, %xmm12
	addsd	%xmm7, %xmm12
	subsd	%xmm12, %xmm11
	movapd	%xmm12, %xmm15
	addsd	%xmm7, %xmm11
	movapd	%xmm14, %xmm7
	mulsd	%xmm6, %xmm7
	addsd	%xmm11, %xmm7
	movapd	%xmm0, %xmm11
	addsd	%xmm10, %xmm11
	movsd	a5.6391(%rip), %xmm10
	movapd	%xmm10, %xmm0
	mulsd	%xmm2, %xmm11
	addsd	%xmm11, %xmm13
	movsd	aa5.6392(%rip), %xmm11
	addsd	%xmm7, %xmm13
	addsd	%xmm13, %xmm15
	subsd	%xmm15, %xmm12
	movapd	%xmm15, %xmm7
	addsd	%xmm15, %xmm0
	andpd	%xmm3, %xmm7
	addsd	%xmm13, %xmm12
	movapd	%xmm10, %xmm13
	andpd	%xmm3, %xmm13
	ucomisd	%xmm7, %xmm13
	jbe	.L429
	movapd	%xmm10, %xmm7
	subsd	%xmm0, %xmm7
	addsd	%xmm15, %xmm7
	addsd	%xmm12, %xmm7
	addsd	%xmm11, %xmm7
.L31:
	movapd	%xmm7, %xmm12
	movsd	(%rsp), %xmm10
	movapd	%xmm4, %xmm14
	addsd	%xmm0, %xmm12
	mulsd	%xmm12, %xmm10
	movapd	%xmm12, %xmm11
	movapd	%xmm12, %xmm13
	subsd	%xmm12, %xmm0
	mulsd	%xmm5, %xmm12
	subsd	%xmm10, %xmm11
	addsd	%xmm10, %xmm11
	movapd	%xmm4, %xmm10
	subsd	%xmm11, %xmm13
	mulsd	%xmm11, %xmm10
	mulsd	%xmm6, %xmm11
	mulsd	%xmm13, %xmm14
	mulsd	%xmm6, %xmm13
	addsd	%xmm11, %xmm14
	movapd	%xmm10, %xmm11
	addsd	%xmm14, %xmm11
	subsd	%xmm11, %xmm10
	addsd	%xmm14, %xmm10
	addsd	%xmm10, %xmm13
	movapd	%xmm0, %xmm10
	addsd	%xmm7, %xmm10
	movapd	%xmm11, %xmm7
	mulsd	%xmm2, %xmm10
	addsd	%xmm10, %xmm12
	movapd	%xmm12, %xmm10
	movsd	aa3.6390(%rip), %xmm12
	addsd	%xmm13, %xmm10
	addsd	%xmm10, %xmm7
	subsd	%xmm7, %xmm11
	movapd	%xmm7, %xmm13
	andpd	%xmm3, %xmm13
	addsd	%xmm10, %xmm11
	movsd	a3.6389(%rip), %xmm10
	movapd	%xmm10, %xmm14
	movapd	%xmm10, %xmm0
	andpd	%xmm3, %xmm14
	addsd	%xmm7, %xmm0
	ucomisd	%xmm13, %xmm14
	jbe	.L430
	subsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm7
	addsd	%xmm7, %xmm11
	addsd	%xmm12, %xmm11
.L34:
	movapd	%xmm11, %xmm14
	movsd	(%rsp), %xmm15
	addsd	%xmm0, %xmm14
	movapd	%xmm15, %xmm7
	mulsd	%xmm14, %xmm7
	movapd	%xmm14, %xmm12
	movapd	%xmm14, %xmm10
	mulsd	%xmm14, %xmm5
	subsd	%xmm14, %xmm0
	subsd	%xmm7, %xmm12
	addsd	%xmm11, %xmm0
	addsd	%xmm7, %xmm12
	movapd	%xmm4, %xmm7
	mulsd	%xmm0, %xmm2
	movapd	%xmm15, %xmm0
	subsd	%xmm12, %xmm10
	mulsd	%xmm12, %xmm7
	mulsd	%xmm6, %xmm12
	addsd	%xmm5, %xmm2
	mulsd	%xmm10, %xmm4
	movapd	%xmm7, %xmm13
	mulsd	%xmm10, %xmm6
	addsd	%xmm12, %xmm4
	addsd	%xmm4, %xmm13
	subsd	%xmm13, %xmm7
	movapd	%xmm13, %xmm5
	addsd	%xmm4, %xmm7
	addsd	%xmm7, %xmm6
	addsd	%xmm6, %xmm2
	addsd	%xmm2, %xmm5
	mulsd	%xmm5, %xmm0
	movapd	%xmm5, %xmm4
	movapd	%xmm5, %xmm6
	subsd	%xmm5, %xmm13
	subsd	%xmm0, %xmm4
	addsd	%xmm0, %xmm4
	movapd	%xmm8, %xmm0
	subsd	%xmm4, %xmm6
	mulsd	%xmm4, %xmm0
	mulsd	%xmm9, %xmm4
	mulsd	%xmm6, %xmm8
	mulsd	%xmm6, %xmm9
	pxor	%xmm6, %xmm6
	addsd	%xmm4, %xmm8
	movapd	%xmm0, %xmm4
	mulsd	%xmm6, %xmm5
	addsd	%xmm8, %xmm4
	subsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm8
	movapd	%xmm4, %xmm0
	addsd	%xmm9, %xmm8
	movapd	%xmm13, %xmm9
	addsd	%xmm2, %xmm9
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm9
	addsd	%xmm5, %xmm9
	addsd	%xmm8, %xmm9
	addsd	%xmm9, %xmm0
	subsd	%xmm0, %xmm4
	addsd	%xmm0, %xmm2
	addsd	%xmm4, %xmm9
	movapd	%xmm1, %xmm4
	andpd	%xmm3, %xmm4
	andpd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm4
	jbe	.L431
	movapd	%xmm1, %xmm4
	pxor	%xmm5, %xmm5
	subsd	%xmm2, %xmm4
	addsd	%xmm4, %xmm0
	addsd	%xmm0, %xmm9
	addsd	%xmm5, %xmm9
.L37:
	movapd	%xmm9, %xmm3
	addsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm2
	addsd	%xmm2, %xmm9
	movsd	u2.6419(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	movapd	%xmm9, %xmm0
	addsd	%xmm2, %xmm9
	subsd	%xmm2, %xmm0
	addsd	%xmm3, %xmm9
	addsd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm9
	jp	.L257
.L507:
	je	.L8
.L257:
	movapd	%xmm1, %xmm0
	call	tanMp
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L505:
	mulsd	%xmm7, %xmm0
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L526:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 752(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %ebp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L432:
	movsd	g4.6414(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	jnb	.L534
	movsd	g5.6415(%rip), %xmm0
	ucomisd	%xmm2, %xmm0
	jb	.L462
	movsd	hpinv.6465(%rip), %xmm5
	movapd	%xmm1, %xmm6
	movsd	toint.6466(%rip), %xmm0
	mulsd	%xmm1, %xmm5
	movsd	mp2.6461(%rip), %xmm3
	movsd	pp3.6463(%rip), %xmm7
	movsd	pp4.6464(%rip), %xmm4
	addsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm2
	subsd	%xmm0, %xmm2
	movsd	mp1.6460(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm7
	mulsd	%xmm2, %xmm4
	subsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm0
	subsd	%xmm3, %xmm0
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm2
	subsd	%xmm7, %xmm3
	movapd	%xmm3, %xmm6
	subsd	%xmm3, %xmm2
	subsd	%xmm4, %xmm6
	subsd	%xmm7, %xmm2
	subsd	%xmm6, %xmm3
	movapd	%xmm6, %xmm7
	subsd	%xmm4, %xmm3
	movapd	%xmm6, %xmm4
	addsd	%xmm3, %xmm2
	movq	.LC6(%rip), %xmm3
	andpd	%xmm3, %xmm7
	movapd	%xmm2, %xmm0
	addsd	%xmm2, %xmm4
	andpd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm7
	jbe	.L463
	subsd	%xmm4, %xmm6
	addsd	%xmm6, %xmm2
.L147:
	pxor	%xmm6, %xmm6
	movsd	%xmm4, 64(%rsp)
	movsd	%xmm2, 72(%rsp)
	ucomisd	%xmm4, %xmm6
	ja	.L535
	movapd	%xmm2, %xmm7
	movapd	%xmm4, %xmm6
	movsd	.LC1(%rip), %xmm0
.L148:
	movsd	gy1.6416(%rip), %xmm8
	ucomisd	%xmm6, %xmm8
	jnb	.L257
	movq	%xmm5, %rax
	movsd	gy2.6417(%rip), %xmm5
	andl	$1, %eax
	ucomisd	%xmm6, %xmm5
	jb	.L466
	movapd	%xmm4, %xmm0
	movsd	d11.6388(%rip), %xmm5
	testl	%eax, %eax
	mulsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm5
	addsd	d9.6387(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	d7.6386(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	d5.6385(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm4, %xmm0
	addsd	d3.6384(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm2, %xmm5
	je	.L154
	movapd	%xmm4, %xmm6
	movapd	%xmm5, %xmm0
	movapd	%xmm4, %xmm7
	andpd	%xmm3, %xmm6
	andpd	%xmm3, %xmm0
	addsd	%xmm5, %xmm7
	movsd	%xmm6, 8(%rsp)
	ucomisd	%xmm0, %xmm6
	jbe	.L467
	movapd	%xmm4, %xmm0
	subsd	%xmm7, %xmm0
	addsd	%xmm0, %xmm5
.L157:
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm7, %xmm9
	movsd	.LC5(%rip), %xmm15
	movapd	%xmm0, %xmm6
	movapd	%xmm7, %xmm11
	movapd	%xmm15, %xmm8
	movsd	%xmm15, (%rsp)
	divsd	%xmm7, %xmm6
	mulsd	%xmm7, %xmm15
	mulsd	%xmm6, %xmm8
	movapd	%xmm6, %xmm12
	subsd	%xmm15, %xmm9
	movapd	%xmm6, %xmm10
	mulsd	%xmm6, %xmm5
	subsd	%xmm8, %xmm12
	addsd	%xmm15, %xmm9
	pxor	%xmm15, %xmm15
	addsd	%xmm8, %xmm12
	subsd	%xmm9, %xmm11
	subsd	%xmm12, %xmm10
	movapd	%xmm12, %xmm8
	mulsd	%xmm11, %xmm12
	mulsd	%xmm9, %xmm8
	mulsd	%xmm10, %xmm9
	mulsd	%xmm11, %xmm10
	addsd	%xmm12, %xmm9
	movapd	%xmm8, %xmm12
	addsd	%xmm9, %xmm12
	subsd	%xmm12, %xmm8
	subsd	%xmm12, %xmm0
	addsd	%xmm9, %xmm8
	addsd	%xmm10, %xmm8
	subsd	%xmm8, %xmm0
	addsd	%xmm15, %xmm0
	subsd	%xmm5, %xmm0
	movapd	%xmm6, %xmm5
	divsd	%xmm7, %xmm0
	movapd	%xmm6, %xmm7
	movsd	u14.6437(%rip), %xmm6
	addsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm5
	mulsd	%xmm7, %xmm6
	addsd	%xmm0, %xmm5
	movapd	%xmm5, %xmm0
	subsd	%xmm6, %xmm0
	addsd	%xmm5, %xmm6
	addsd	%xmm7, %xmm0
	addsd	%xmm7, %xmm6
	ucomisd	%xmm0, %xmm6
	jp	.L158
	je	.L506
.L158:
	movsd	(%rsp), %xmm14
	movapd	%xmm4, %xmm0
	movapd	%xmm4, %xmm7
	mulsd	%xmm4, %xmm14
	subsd	%xmm14, %xmm0
	addsd	%xmm0, %xmm14
	subsd	%xmm14, %xmm7
	movapd	%xmm14, %xmm0
	mulsd	%xmm14, %xmm0
	movapd	%xmm7, %xmm6
	movsd	%xmm7, 16(%rsp)
	movapd	%xmm7, %xmm15
	mulsd	%xmm14, %xmm6
	movapd	%xmm0, %xmm5
	mulsd	%xmm7, %xmm15
	movsd	a13.6399(%rip), %xmm7
	addsd	%xmm6, %xmm6
	movapd	%xmm7, %xmm10
	movapd	%xmm7, %xmm8
	andpd	%xmm3, %xmm10
	addsd	%xmm6, %xmm5
	subsd	%xmm5, %xmm0
	addsd	%xmm6, %xmm0
	movapd	%xmm5, %xmm6
	addsd	%xmm15, %xmm0
	movapd	%xmm2, %xmm15
	mulsd	%xmm4, %xmm15
	addsd	%xmm15, %xmm15
	addsd	%xmm15, %xmm0
	addsd	%xmm0, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm15
	movsd	a27.6407(%rip), %xmm5
	addsd	%xmm0, %xmm15
	movsd	aa13.6400(%rip), %xmm0
	mulsd	%xmm6, %xmm5
	addsd	a25.6406(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	addsd	a23.6405(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	addsd	a21.6404(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	addsd	a19.6403(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	addsd	a17.6402(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	addsd	a15.6401(%rip), %xmm5
	mulsd	%xmm6, %xmm5
	movapd	%xmm5, %xmm9
	addsd	%xmm5, %xmm8
	andpd	%xmm3, %xmm9
	ucomisd	%xmm9, %xmm10
	jbe	.L468
	subsd	%xmm8, %xmm7
	addsd	%xmm7, %xmm5
	pxor	%xmm7, %xmm7
	addsd	%xmm7, %xmm5
	addsd	%xmm5, %xmm0
.L163:
	movapd	%xmm0, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm6, %xmm7
	addsd	%xmm8, %xmm10
	movapd	%xmm9, %xmm5
	mulsd	%xmm6, %xmm5
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm8
	subsd	%xmm5, %xmm7
	subsd	%xmm9, %xmm11
	addsd	%xmm0, %xmm8
	addsd	%xmm7, %xmm5
	movapd	%xmm6, %xmm7
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm6, %xmm8
	subsd	%xmm5, %xmm7
	movapd	%xmm5, %xmm12
	movapd	%xmm5, %xmm13
	mulsd	%xmm15, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm7, %xmm9
	addsd	%xmm8, %xmm10
	movsd	a11.6397(%rip), %xmm8
	mulsd	%xmm11, %xmm13
	mulsd	%xmm7, %xmm11
	movapd	%xmm8, %xmm0
	addsd	%xmm9, %xmm13
	movapd	%xmm12, %xmm9
	addsd	%xmm13, %xmm9
	subsd	%xmm9, %xmm12
	addsd	%xmm13, %xmm12
	movsd	aa11.6398(%rip), %xmm13
	addsd	%xmm11, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm9, %xmm10
	addsd	%xmm12, %xmm10
	subsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	addsd	%xmm10, %xmm0
	andpd	%xmm3, %xmm11
	addsd	%xmm12, %xmm9
	movapd	%xmm8, %xmm12
	andpd	%xmm3, %xmm12
	ucomisd	%xmm11, %xmm12
	jbe	.L469
	subsd	%xmm0, %xmm8
	addsd	%xmm10, %xmm8
	addsd	%xmm9, %xmm8
	addsd	%xmm13, %xmm8
.L166:
	movapd	%xmm8, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm5, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm5, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm8, %xmm0
	movsd	a9.6395(%rip), %xmm8
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm6, %xmm0
	mulsd	%xmm15, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm7, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm8, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm7, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm8, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa9.6396(%rip), %xmm12
	jbe	.L470
	subsd	%xmm0, %xmm8
	addsd	%xmm9, %xmm8
	addsd	%xmm13, %xmm8
	addsd	%xmm12, %xmm8
.L169:
	movapd	%xmm8, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm5, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm5, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm8, %xmm0
	movsd	a7.6393(%rip), %xmm8
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm6, %xmm0
	mulsd	%xmm15, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm7, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm8, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm7, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm8, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa7.6394(%rip), %xmm12
	jbe	.L471
	subsd	%xmm0, %xmm8
	addsd	%xmm9, %xmm8
	addsd	%xmm13, %xmm8
	addsd	%xmm12, %xmm8
.L172:
	movapd	%xmm8, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm5, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm5, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm8, %xmm0
	movsd	a5.6391(%rip), %xmm8
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm6, %xmm0
	mulsd	%xmm15, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm7, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm8, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm7, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm8, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa5.6392(%rip), %xmm12
	jbe	.L472
	subsd	%xmm0, %xmm8
	addsd	%xmm9, %xmm8
	addsd	%xmm13, %xmm8
	addsd	%xmm12, %xmm8
.L175:
	movapd	%xmm8, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm5, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm5, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm8, %xmm0
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm6, %xmm0
	mulsd	%xmm15, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm7, %xmm9
	addsd	%xmm0, %xmm10
	mulsd	%xmm11, %xmm13
	mulsd	%xmm7, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	movapd	%xmm13, %xmm0
	addsd	%xmm9, %xmm12
	movsd	a3.6389(%rip), %xmm9
	movapd	%xmm9, %xmm8
	addsd	%xmm11, %xmm12
	movapd	%xmm9, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm0
	movapd	%xmm0, %xmm10
	subsd	%xmm0, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm0, %xmm8
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa3.6390(%rip), %xmm12
	jbe	.L473
	subsd	%xmm8, %xmm9
	addsd	%xmm0, %xmm9
	addsd	%xmm13, %xmm9
	addsd	%xmm12, %xmm9
.L178:
	movapd	%xmm9, %xmm11
	movsd	(%rsp), %xmm13
	addsd	%xmm8, %xmm11
	movapd	%xmm13, %xmm0
	mulsd	%xmm11, %xmm0
	movapd	%xmm11, %xmm10
	movapd	%xmm11, %xmm12
	subsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm0
	movapd	%xmm5, %xmm10
	subsd	%xmm0, %xmm12
	mulsd	%xmm0, %xmm10
	mulsd	%xmm7, %xmm0
	mulsd	%xmm12, %xmm5
	mulsd	%xmm12, %xmm7
	addsd	%xmm0, %xmm5
	movapd	%xmm10, %xmm0
	addsd	%xmm5, %xmm0
	subsd	%xmm0, %xmm10
	addsd	%xmm5, %xmm10
	addsd	%xmm7, %xmm10
	movapd	%xmm8, %xmm7
	movapd	%xmm13, %xmm8
	subsd	%xmm11, %xmm7
	mulsd	%xmm15, %xmm11
	movsd	16(%rsp), %xmm15
	addsd	%xmm9, %xmm7
	mulsd	%xmm6, %xmm7
	movapd	%xmm0, %xmm6
	addsd	%xmm11, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm7, %xmm6
	mulsd	%xmm6, %xmm8
	movapd	%xmm6, %xmm5
	movapd	%xmm6, %xmm9
	subsd	%xmm6, %xmm0
	mulsd	%xmm2, %xmm6
	subsd	%xmm8, %xmm5
	addsd	%xmm7, %xmm0
	movsd	8(%rsp), %xmm7
	addsd	%xmm8, %xmm5
	movapd	%xmm14, %xmm8
	mulsd	%xmm4, %xmm0
	subsd	%xmm5, %xmm9
	mulsd	%xmm5, %xmm8
	mulsd	%xmm15, %xmm5
	addsd	%xmm6, %xmm0
	mulsd	%xmm9, %xmm14
	mulsd	%xmm15, %xmm9
	addsd	%xmm5, %xmm14
	movapd	%xmm8, %xmm5
	addsd	%xmm14, %xmm5
	subsd	%xmm5, %xmm8
	movapd	%xmm5, %xmm6
	addsd	%xmm8, %xmm14
	addsd	%xmm14, %xmm9
	addsd	%xmm9, %xmm0
	addsd	%xmm0, %xmm6
	andpd	%xmm6, %xmm3
	subsd	%xmm6, %xmm5
	ucomisd	%xmm3, %xmm7
	addsd	%xmm5, %xmm0
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm5
	jbe	.L474
	subsd	%xmm5, %xmm4
	addsd	%xmm4, %xmm6
	addsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm2
.L181:
	movapd	%xmm2, %xmm3
	testl	%eax, %eax
	addsd	%xmm5, %xmm3
	subsd	%xmm3, %xmm5
	addsd	%xmm5, %xmm2
	je	.L182
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm3, %xmm9
	movsd	(%rsp), %xmm7
	movapd	%xmm0, %xmm4
	movapd	%xmm7, %xmm6
	divsd	%xmm3, %xmm4
	mulsd	%xmm4, %xmm6
	movapd	%xmm4, %xmm5
	movapd	%xmm4, %xmm8
	mulsd	%xmm4, %xmm2
	subsd	%xmm6, %xmm5
	addsd	%xmm6, %xmm5
	movapd	%xmm7, %xmm6
	movapd	%xmm3, %xmm7
	mulsd	%xmm3, %xmm6
	subsd	%xmm5, %xmm8
	subsd	%xmm6, %xmm7
	addsd	%xmm6, %xmm7
	movapd	%xmm5, %xmm6
	subsd	%xmm7, %xmm9
	mulsd	%xmm7, %xmm6
	mulsd	%xmm8, %xmm7
	mulsd	%xmm9, %xmm5
	mulsd	%xmm9, %xmm8
	addsd	%xmm7, %xmm5
	movapd	%xmm6, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm6
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm5
	addsd	%xmm8, %xmm5
	subsd	%xmm5, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm0
	movapd	%xmm4, %xmm5
	subsd	%xmm2, %xmm0
	divsd	%xmm3, %xmm0
	movsd	u16.6439(%rip), %xmm3
	addsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm3
	movapd	%xmm4, %xmm2
	addsd	%xmm0, %xmm2
.L515:
	movapd	%xmm2, %xmm0
	addsd	%xmm3, %xmm2
	subsd	%xmm3, %xmm0
	addsd	%xmm5, %xmm2
	addsd	%xmm5, %xmm0
	ucomisd	%xmm0, %xmm2
	jp	.L257
.L513:
	jne	.L257
.L506:
	xorpd	.LC3(%rip), %xmm0
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L534:
	movsd	hpinv.6465(%rip), %xmm2
	movsd	toint.6466(%rip), %xmm0
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm2
	pxor	%xmm7, %xmm7
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm6
	subsd	%xmm0, %xmm6
	movsd	mp1.6460(%rip), %xmm0
	mulsd	%xmm6, %xmm0
	subsd	%xmm0, %xmm5
	movsd	mp2.6461(%rip), %xmm0
	mulsd	%xmm6, %xmm0
	subsd	%xmm0, %xmm5
	movsd	mp3.6462(%rip), %xmm0
	mulsd	%xmm6, %xmm0
	movapd	%xmm5, %xmm4
	movapd	%xmm5, %xmm3
	subsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm3
	movsd	%xmm4, 64(%rsp)
	ucomisd	%xmm4, %xmm7
	subsd	%xmm0, %xmm3
	movsd	%xmm3, 72(%rsp)
	ja	.L536
	movapd	%xmm3, %xmm8
	movapd	%xmm4, %xmm7
	movsd	.LC1(%rip), %xmm0
.L68:
	movsd	gy1.6416(%rip), %xmm9
	ucomisd	%xmm7, %xmm9
	jnb	.L257
	movq	%xmm2, %rax
	movsd	gy2.6417(%rip), %xmm2
	andl	$1, %eax
	ucomisd	%xmm7, %xmm2
	jb	.L443
	movapd	%xmm4, %xmm2
	movsd	d11.6388(%rip), %xmm0
	testl	%eax, %eax
	mulsd	%xmm4, %xmm2
	mulsd	%xmm2, %xmm0
	addsd	d9.6387(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	d7.6386(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	d5.6385(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm2
	addsd	d3.6384(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm3
	movapd	%xmm3, %xmm2
	je	.L74
	movapd	%xmm4, %xmm7
	movapd	%xmm4, %xmm8
	movapd	%xmm2, %xmm0
	addsd	%xmm3, %xmm7
	movq	.LC6(%rip), %xmm3
	andpd	%xmm3, %xmm8
	andpd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm8
	jbe	.L444
	subsd	%xmm7, %xmm4
	movapd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
.L77:
	movsd	.LC1(%rip), %xmm4
	movapd	%xmm7, %xmm9
	movsd	.LC5(%rip), %xmm15
	movapd	%xmm4, %xmm2
	movapd	%xmm7, %xmm12
	movapd	%xmm15, %xmm8
	movsd	%xmm15, (%rsp)
	divsd	%xmm7, %xmm2
	mulsd	%xmm7, %xmm15
	mulsd	%xmm2, %xmm8
	movapd	%xmm2, %xmm10
	subsd	%xmm15, %xmm9
	movapd	%xmm2, %xmm11
	mulsd	%xmm2, %xmm0
	subsd	%xmm8, %xmm10
	addsd	%xmm15, %xmm9
	pxor	%xmm15, %xmm15
	addsd	%xmm8, %xmm10
	subsd	%xmm9, %xmm12
	subsd	%xmm10, %xmm11
	movapd	%xmm10, %xmm8
	mulsd	%xmm12, %xmm10
	mulsd	%xmm9, %xmm8
	mulsd	%xmm11, %xmm9
	mulsd	%xmm12, %xmm11
	addsd	%xmm10, %xmm9
	movapd	%xmm8, %xmm10
	addsd	%xmm9, %xmm10
	subsd	%xmm10, %xmm8
	subsd	%xmm10, %xmm4
	addsd	%xmm9, %xmm8
	addsd	%xmm11, %xmm8
	subsd	%xmm8, %xmm4
	addsd	%xmm15, %xmm4
	subsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm0
	movsd	u6.6425(%rip), %xmm4
	divsd	%xmm7, %xmm0
	movapd	%xmm2, %xmm7
	addsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm2
	mulsd	%xmm7, %xmm4
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	subsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm4
	addsd	%xmm7, %xmm0
	addsd	%xmm7, %xmm4
	ucomisd	%xmm0, %xmm4
	jp	.L78
	je	.L506
.L78:
	movsd	pp3.6463(%rip), %xmm4
	movapd	%xmm5, %xmm2
	mulsd	%xmm6, %xmm4
	mulsd	pp4.6464(%rip), %xmm6
	subsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm8
	subsd	%xmm2, %xmm5
	subsd	%xmm6, %xmm8
	movapd	%xmm5, %xmm0
	subsd	%xmm8, %xmm2
	movapd	%xmm8, %xmm7
	subsd	%xmm4, %xmm0
	movapd	%xmm8, %xmm4
	andpd	%xmm3, %xmm4
	subsd	%xmm6, %xmm2
	addsd	%xmm2, %xmm0
	movapd	%xmm0, %xmm2
	addsd	%xmm0, %xmm7
	andpd	%xmm3, %xmm2
	ucomisd	%xmm2, %xmm4
	jbe	.L446
	subsd	%xmm7, %xmm8
	addsd	%xmm0, %xmm8
	movsd	%xmm8, 8(%rsp)
.L83:
	movsd	(%rsp), %xmm0
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm15
	movsd	%xmm7, 64(%rsp)
	mulsd	%xmm7, %xmm0
	movsd	8(%rsp), %xmm5
	movsd	a13.6399(%rip), %xmm9
	movsd	%xmm5, 72(%rsp)
	mulsd	%xmm7, %xmm5
	subsd	%xmm0, %xmm8
	movapd	%xmm9, %xmm10
	movapd	%xmm9, %xmm6
	andpd	%xmm3, %xmm10
	addsd	%xmm0, %xmm8
	subsd	%xmm8, %xmm15
	movapd	%xmm8, %xmm4
	movapd	%xmm8, %xmm0
	mulsd	%xmm8, %xmm0
	mulsd	%xmm15, %xmm4
	movapd	%xmm15, %xmm14
	mulsd	%xmm15, %xmm14
	movapd	%xmm0, %xmm2
	addsd	%xmm4, %xmm4
	addsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm4
	addsd	%xmm14, %xmm0
	movapd	%xmm5, %xmm14
	addsd	%xmm5, %xmm14
	movsd	aa13.6400(%rip), %xmm5
	addsd	%xmm14, %xmm0
	addsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm14
	movsd	a27.6407(%rip), %xmm2
	addsd	%xmm0, %xmm14
	mulsd	%xmm4, %xmm2
	addsd	a25.6406(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	a23.6405(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	a21.6404(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	a19.6403(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	a17.6402(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	addsd	a15.6401(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm2, %xmm6
	andpd	%xmm3, %xmm0
	ucomisd	%xmm0, %xmm10
	jbe	.L447
	movapd	%xmm9, %xmm0
	subsd	%xmm6, %xmm0
	addsd	%xmm2, %xmm0
	pxor	%xmm2, %xmm2
	addsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm0
.L86:
	movapd	%xmm0, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm10
	movapd	%xmm9, %xmm2
	mulsd	%xmm4, %xmm2
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm6
	subsd	%xmm2, %xmm5
	subsd	%xmm9, %xmm11
	addsd	%xmm0, %xmm6
	addsd	%xmm5, %xmm2
	movapd	%xmm4, %xmm5
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm6
	subsd	%xmm2, %xmm5
	movapd	%xmm2, %xmm12
	movapd	%xmm2, %xmm13
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm6, %xmm10
	movsd	a11.6397(%rip), %xmm6
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	movapd	%xmm6, %xmm0
	addsd	%xmm9, %xmm13
	movapd	%xmm12, %xmm9
	addsd	%xmm13, %xmm9
	subsd	%xmm9, %xmm12
	addsd	%xmm13, %xmm12
	movsd	aa11.6398(%rip), %xmm13
	addsd	%xmm11, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm9, %xmm10
	addsd	%xmm12, %xmm10
	subsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	addsd	%xmm10, %xmm0
	andpd	%xmm3, %xmm11
	addsd	%xmm12, %xmm9
	movapd	%xmm6, %xmm12
	andpd	%xmm3, %xmm12
	ucomisd	%xmm11, %xmm12
	jbe	.L448
	subsd	%xmm0, %xmm6
	addsd	%xmm10, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
.L89:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	movsd	a9.6395(%rip), %xmm6
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm6, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa9.6396(%rip), %xmm12
	jbe	.L449
	subsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm12, %xmm6
.L92:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	movsd	a7.6393(%rip), %xmm6
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm6, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa7.6394(%rip), %xmm12
	jbe	.L450
	subsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm12, %xmm6
.L95:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	movsd	a5.6391(%rip), %xmm6
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm6, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa5.6392(%rip), %xmm12
	jbe	.L451
	subsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm12, %xmm6
.L98:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	movapd	%xmm13, %xmm0
	addsd	%xmm9, %xmm12
	movsd	a3.6389(%rip), %xmm9
	movapd	%xmm9, %xmm6
	addsd	%xmm11, %xmm12
	movapd	%xmm9, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm0
	movapd	%xmm0, %xmm10
	subsd	%xmm0, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm0, %xmm6
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa3.6390(%rip), %xmm12
	jbe	.L452
	subsd	%xmm6, %xmm9
	addsd	%xmm0, %xmm9
	addsd	%xmm13, %xmm9
	addsd	%xmm12, %xmm9
.L101:
	movapd	%xmm9, %xmm11
	movsd	(%rsp), %xmm13
	addsd	%xmm6, %xmm11
	movapd	%xmm13, %xmm0
	mulsd	%xmm11, %xmm0
	movapd	%xmm11, %xmm10
	movapd	%xmm11, %xmm12
	mulsd	%xmm11, %xmm14
	subsd	%xmm11, %xmm6
	subsd	%xmm0, %xmm10
	addsd	%xmm6, %xmm9
	addsd	%xmm10, %xmm0
	movapd	%xmm2, %xmm10
	mulsd	%xmm9, %xmm4
	subsd	%xmm0, %xmm12
	mulsd	%xmm0, %xmm10
	mulsd	%xmm5, %xmm0
	addsd	%xmm4, %xmm14
	movapd	%xmm13, %xmm4
	mulsd	%xmm12, %xmm2
	mulsd	%xmm12, %xmm5
	addsd	%xmm0, %xmm2
	movapd	%xmm10, %xmm0
	addsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm10
	addsd	%xmm2, %xmm10
	movapd	%xmm0, %xmm2
	addsd	%xmm5, %xmm10
	movapd	%xmm8, %xmm5
	addsd	%xmm14, %xmm10
	addsd	%xmm10, %xmm2
	mulsd	%xmm2, %xmm4
	movapd	%xmm2, %xmm6
	subsd	%xmm2, %xmm0
	subsd	%xmm4, %xmm6
	addsd	%xmm10, %xmm0
	addsd	%xmm4, %xmm6
	movapd	%xmm2, %xmm4
	mulsd	%xmm7, %xmm0
	subsd	%xmm6, %xmm4
	mulsd	%xmm6, %xmm5
	mulsd	%xmm15, %xmm6
	mulsd	%xmm4, %xmm8
	mulsd	%xmm4, %xmm15
	movsd	8(%rsp), %xmm4
	addsd	%xmm6, %xmm8
	movapd	%xmm5, %xmm6
	mulsd	%xmm4, %xmm2
	addsd	%xmm8, %xmm6
	addsd	%xmm2, %xmm0
	subsd	%xmm6, %xmm5
	movapd	%xmm6, %xmm2
	addsd	%xmm5, %xmm8
	movapd	%xmm7, %xmm5
	addsd	%xmm8, %xmm15
	addsd	%xmm15, %xmm0
	addsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm6
	addsd	%xmm2, %xmm5
	addsd	%xmm0, %xmm6
	movapd	%xmm7, %xmm0
	andpd	%xmm3, %xmm0
	andpd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm0
	jbe	.L453
	subsd	%xmm5, %xmm7
	movapd	%xmm7, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm6, %xmm0
	addsd	%xmm4, %xmm0
.L104:
	movapd	%xmm0, %xmm4
	testl	%eax, %eax
	addsd	%xmm5, %xmm4
	subsd	%xmm4, %xmm5
	addsd	%xmm0, %xmm5
	je	.L105
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm4, %xmm9
	movsd	(%rsp), %xmm7
	movapd	%xmm0, %xmm2
	movapd	%xmm7, %xmm6
	divsd	%xmm4, %xmm2
	mulsd	%xmm2, %xmm6
	movapd	%xmm2, %xmm3
	movapd	%xmm2, %xmm8
	mulsd	%xmm2, %xmm5
	subsd	%xmm6, %xmm3
	addsd	%xmm6, %xmm3
	movapd	%xmm7, %xmm6
	movapd	%xmm4, %xmm7
	mulsd	%xmm4, %xmm6
	subsd	%xmm3, %xmm8
	subsd	%xmm6, %xmm7
	addsd	%xmm6, %xmm7
	movapd	%xmm3, %xmm6
	subsd	%xmm7, %xmm9
	mulsd	%xmm7, %xmm6
	mulsd	%xmm8, %xmm7
	mulsd	%xmm9, %xmm3
	mulsd	%xmm9, %xmm8
	addsd	%xmm7, %xmm3
	movapd	%xmm6, %xmm7
	addsd	%xmm3, %xmm7
	subsd	%xmm7, %xmm6
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm3
	addsd	%xmm8, %xmm3
	subsd	%xmm3, %xmm0
	pxor	%xmm3, %xmm3
	addsd	%xmm3, %xmm0
	movsd	u8.6427(%rip), %xmm3
	subsd	%xmm5, %xmm0
	divsd	%xmm4, %xmm0
	movapd	%xmm2, %xmm4
	addsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm3
	addsd	%xmm0, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm3, %xmm2
	subsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm2
	addsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm2
	jnp	.L513
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L425:
	subsd	%xmm7, %xmm4
	addsd	%xmm4, %xmm0
	addsd	%xmm6, %xmm0
	pxor	%xmm6, %xmm6
	addsd	%xmm6, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L429:
	movapd	%xmm15, %xmm7
	subsd	%xmm0, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm11, %xmm7
	addsd	%xmm12, %xmm7
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L428:
	subsd	%xmm0, %xmm15
	movapd	%xmm15, %xmm10
	addsd	%xmm7, %xmm10
	addsd	%xmm11, %xmm10
	addsd	%xmm12, %xmm10
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L427:
	movapd	%xmm15, %xmm7
	subsd	%xmm0, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm11, %xmm7
	addsd	%xmm12, %xmm7
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L426:
	subsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm10
	addsd	%xmm11, %xmm10
	addsd	%xmm13, %xmm10
	addsd	%xmm12, %xmm10
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L431:
	subsd	%xmm2, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm9
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L430:
	subsd	%xmm0, %xmm7
	addsd	%xmm7, %xmm10
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm11
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L536:
	movq	.LC3(%rip), %xmm8
	movapd	%xmm4, %xmm7
	movsd	.LC0(%rip), %xmm0
	xorpd	%xmm8, %xmm7
	xorpd	%xmm3, %xmm8
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L443:
	movsd	.LC7(%rip), %xmm2
	leaq	xfg.6467(%rip), %rcx
	movapd	%xmm8, %xmm4
	mulsd	%xmm7, %xmm2
	movsd	e1.6409(%rip), %xmm3
	addsd	mfftnhf.6410(%rip), %xmm2
	cvttsd2si	%xmm2, %edx
	movslq	%edx, %rdx
	movq	%rdx, %rsi
	salq	$5, %rsi
	addq	%rcx, %rsi
	testl	%eax, %eax
	subsd	(%rsi), %xmm7
	movsd	16(%rsi), %xmm6
	addsd	%xmm7, %xmm4
	movapd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm2
	mulsd	%xmm2, %xmm3
	mulsd	%xmm4, %xmm2
	addsd	e0.6408(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	movsd	8(%rsi), %xmm2
	movapd	%xmm2, %xmm5
	addsd	%xmm4, %xmm3
	addsd	%xmm6, %xmm5
	mulsd	%xmm3, %xmm5
	je	.L109
	addsd	%xmm2, %xmm3
	movapd	%xmm6, %xmm15
	divsd	%xmm3, %xmm5
	movsd	u10.6431(%rip), %xmm3
	mulsd	%xmm6, %xmm3
	movapd	%xmm5, %xmm9
	subsd	%xmm3, %xmm9
	addsd	%xmm5, %xmm3
	subsd	%xmm9, %xmm15
	movapd	%xmm15, %xmm9
	movapd	%xmm6, %xmm15
	subsd	%xmm3, %xmm15
	ucomisd	%xmm9, %xmm15
	jp	.L110
	je	.L508
.L110:
	pxor	%xmm3, %xmm3
	movapd	%xmm5, %xmm9
	ucomisd	%xmm5, %xmm3
	jbe	.L112
	xorpd	.LC3(%rip), %xmm9
.L112:
	movapd	%xmm6, %xmm15
	mulsd	ub10.6433(%rip), %xmm9
	movsd	ua10.6432(%rip), %xmm3
	mulsd	%xmm6, %xmm3
	addsd	%xmm9, %xmm3
	movapd	%xmm5, %xmm9
	addsd	%xmm3, %xmm5
	subsd	%xmm3, %xmm9
	movapd	%xmm6, %xmm3
	subsd	%xmm5, %xmm3
	subsd	%xmm9, %xmm15
	ucomisd	%xmm15, %xmm3
	movapd	%xmm15, %xmm9
	jp	.L114
	je	.L508
.L114:
	salq	$5, %rdx
	movq	.LC6(%rip), %xmm3
	movsd	24(%rcx,%rdx), %xmm5
	movapd	%xmm7, %xmm6
	movsd	%xmm5, 32(%rsp)
	movapd	%xmm8, %xmm5
	andpd	%xmm3, %xmm6
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm6
	jbe	.L456
	subsd	%xmm4, %xmm7
	addsd	%xmm8, %xmm7
	movsd	%xmm7, 8(%rsp)
.L123:
	movsd	.LC5(%rip), %xmm5
	movapd	%xmm4, %xmm6
	movapd	%xmm4, %xmm15
	movsd	%xmm5, (%rsp)
	mulsd	%xmm4, %xmm5
	movsd	aa5.6392(%rip), %xmm11
	subsd	%xmm5, %xmm6
	addsd	%xmm6, %xmm5
	subsd	%xmm5, %xmm15
	movapd	%xmm5, %xmm8
	movapd	%xmm5, %xmm6
	mulsd	%xmm5, %xmm6
	mulsd	%xmm15, %xmm8
	movapd	%xmm6, %xmm7
	addsd	%xmm8, %xmm8
	addsd	%xmm8, %xmm7
	subsd	%xmm7, %xmm6
	addsd	%xmm8, %xmm6
	movapd	%xmm15, %xmm8
	mulsd	%xmm15, %xmm8
	addsd	%xmm8, %xmm6
	movsd	8(%rsp), %xmm8
	mulsd	%xmm4, %xmm8
	addsd	%xmm8, %xmm8
	addsd	%xmm8, %xmm6
	movapd	%xmm7, %xmm8
	addsd	%xmm6, %xmm8
	subsd	%xmm8, %xmm7
	addsd	%xmm6, %xmm7
	movsd	a11.6397(%rip), %xmm6
	mulsd	%xmm8, %xmm6
	movsd	%xmm7, 16(%rsp)
	movsd	a5.6391(%rip), %xmm7
	addsd	a9.6395(%rip), %xmm6
	movapd	%xmm7, %xmm10
	movapd	%xmm7, %xmm14
	andpd	%xmm3, %xmm10
	mulsd	%xmm8, %xmm6
	addsd	a7.6393(%rip), %xmm6
	mulsd	%xmm8, %xmm6
	movapd	%xmm6, %xmm9
	addsd	%xmm6, %xmm14
	andpd	%xmm3, %xmm9
	ucomisd	%xmm9, %xmm10
	jbe	.L457
	subsd	%xmm14, %xmm7
	addsd	%xmm7, %xmm6
	pxor	%xmm7, %xmm7
	addsd	%xmm7, %xmm6
	addsd	%xmm11, %xmm6
	movsd	%xmm6, 24(%rsp)
.L126:
	movsd	(%rsp), %xmm13
	movapd	%xmm8, %xmm7
	movsd	24(%rsp), %xmm10
	movapd	%xmm13, %xmm6
	movapd	%xmm8, %xmm9
	addsd	%xmm14, %xmm10
	mulsd	%xmm8, %xmm6
	movapd	%xmm10, %xmm11
	subsd	%xmm6, %xmm7
	subsd	%xmm10, %xmm14
	addsd	%xmm7, %xmm6
	movapd	%xmm13, %xmm7
	addsd	24(%rsp), %xmm14
	mulsd	%xmm10, %xmm7
	subsd	%xmm6, %xmm9
	movapd	%xmm6, %xmm12
	movapd	%xmm6, %xmm13
	subsd	%xmm7, %xmm11
	mulsd	%xmm8, %xmm14
	addsd	%xmm11, %xmm7
	movapd	%xmm10, %xmm11
	mulsd	16(%rsp), %xmm10
	subsd	%xmm7, %xmm11
	mulsd	%xmm7, %xmm12
	mulsd	%xmm9, %xmm7
	addsd	%xmm10, %xmm14
	mulsd	%xmm11, %xmm13
	mulsd	%xmm9, %xmm11
	addsd	%xmm13, %xmm7
	movapd	%xmm12, %xmm13
	addsd	%xmm7, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm7, %xmm12
	movapd	%xmm13, %xmm7
	addsd	%xmm11, %xmm12
	movsd	a3.6389(%rip), %xmm11
	movapd	%xmm11, %xmm10
	addsd	%xmm14, %xmm12
	movapd	%xmm13, %xmm14
	movapd	%xmm11, %xmm13
	andpd	%xmm3, %xmm13
	addsd	%xmm12, %xmm7
	subsd	%xmm7, %xmm14
	addsd	%xmm7, %xmm10
	addsd	%xmm12, %xmm14
	movapd	%xmm7, %xmm12
	andpd	%xmm3, %xmm12
	ucomisd	%xmm12, %xmm13
	jbe	.L458
	subsd	%xmm10, %xmm11
	addsd	%xmm7, %xmm11
	addsd	%xmm11, %xmm14
	addsd	aa3.6390(%rip), %xmm14
.L129:
	movapd	%xmm14, %xmm12
	movsd	(%rsp), %xmm7
	addsd	%xmm10, %xmm12
	mulsd	%xmm12, %xmm7
	movapd	%xmm12, %xmm11
	movapd	%xmm12, %xmm13
	subsd	%xmm12, %xmm10
	mulsd	16(%rsp), %xmm12
	subsd	%xmm7, %xmm11
	addsd	%xmm14, %xmm10
	addsd	%xmm11, %xmm7
	movapd	%xmm6, %xmm11
	mulsd	%xmm8, %xmm10
	subsd	%xmm7, %xmm13
	mulsd	%xmm7, %xmm11
	mulsd	%xmm9, %xmm7
	addsd	%xmm12, %xmm10
	mulsd	%xmm13, %xmm6
	mulsd	%xmm13, %xmm9
	addsd	%xmm7, %xmm6
	movapd	%xmm11, %xmm7
	addsd	%xmm6, %xmm7
	subsd	%xmm7, %xmm11
	movapd	%xmm7, %xmm8
	addsd	%xmm6, %xmm11
	movsd	(%rsp), %xmm6
	addsd	%xmm9, %xmm11
	addsd	%xmm10, %xmm11
	addsd	%xmm11, %xmm8
	mulsd	%xmm8, %xmm6
	movapd	%xmm8, %xmm9
	movapd	%xmm8, %xmm10
	subsd	%xmm6, %xmm9
	addsd	%xmm6, %xmm9
	movapd	%xmm5, %xmm6
	subsd	%xmm9, %xmm10
	mulsd	%xmm9, %xmm6
	mulsd	%xmm15, %xmm9
	mulsd	%xmm10, %xmm5
	addsd	%xmm9, %xmm5
	movapd	%xmm6, %xmm9
	addsd	%xmm5, %xmm9
	subsd	%xmm9, %xmm6
	addsd	%xmm5, %xmm6
	movapd	%xmm15, %xmm5
	movsd	8(%rsp), %xmm15
	mulsd	%xmm10, %xmm5
	addsd	%xmm5, %xmm6
	movapd	%xmm7, %xmm5
	subsd	%xmm8, %xmm5
	mulsd	%xmm15, %xmm8
	addsd	%xmm11, %xmm5
	mulsd	%xmm4, %xmm5
	addsd	%xmm8, %xmm5
	movapd	%xmm4, %xmm8
	andpd	%xmm3, %xmm8
	addsd	%xmm6, %xmm5
	movapd	%xmm9, %xmm6
	addsd	%xmm5, %xmm6
	movapd	%xmm6, %xmm7
	subsd	%xmm6, %xmm9
	andpd	%xmm3, %xmm7
	ucomisd	%xmm7, %xmm8
	addsd	%xmm5, %xmm9
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm5
	jbe	.L459
	subsd	%xmm5, %xmm4
	addsd	%xmm6, %xmm4
	addsd	%xmm9, %xmm4
	addsd	%xmm15, %xmm4
.L132:
	movapd	%xmm4, %xmm9
	movapd	%xmm2, %xmm7
	movapd	%xmm2, %xmm6
	addsd	%xmm5, %xmm9
	andpd	%xmm3, %xmm7
	subsd	%xmm9, %xmm5
	addsd	%xmm9, %xmm6
	addsd	%xmm5, %xmm4
	movapd	%xmm9, %xmm5
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm7
	jbe	.L460
	movapd	%xmm2, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm9, %xmm7
	addsd	%xmm4, %xmm7
	addsd	32(%rsp), %xmm7
.L135:
	movapd	%xmm7, %xmm5
	movsd	(%rsp), %xmm15
	movapd	%xmm2, %xmm13
	addsd	%xmm6, %xmm5
	movapd	%xmm9, %xmm8
	movapd	%xmm2, %xmm10
	movapd	%xmm9, %xmm11
	subsd	%xmm5, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm15, %xmm7
	mulsd	%xmm2, %xmm7
	mulsd	%xmm4, %xmm2
	movsd	.LC1(%rip), %xmm4
	subsd	%xmm7, %xmm13
	addsd	%xmm7, %xmm13
	movapd	%xmm15, %xmm7
	mulsd	%xmm9, %xmm7
	mulsd	32(%rsp), %xmm9
	subsd	%xmm13, %xmm10
	subsd	%xmm7, %xmm8
	addsd	%xmm9, %xmm2
	addsd	%xmm7, %xmm8
	movapd	%xmm13, %xmm7
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm7
	mulsd	%xmm10, %xmm8
	mulsd	%xmm11, %xmm13
	mulsd	%xmm11, %xmm10
	addsd	%xmm8, %xmm13
	movapd	%xmm7, %xmm8
	addsd	%xmm13, %xmm8
	subsd	%xmm8, %xmm7
	movapd	%xmm8, %xmm9
	addsd	%xmm13, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm7, %xmm2
	movapd	%xmm4, %xmm7
	addsd	%xmm2, %xmm9
	andpd	%xmm9, %xmm3
	subsd	%xmm9, %xmm8
	subsd	%xmm9, %xmm7
	ucomisd	%xmm3, %xmm4
	addsd	%xmm2, %xmm8
	ja	.L537
	addsd	%xmm7, %xmm9
	pxor	%xmm3, %xmm3
	subsd	%xmm9, %xmm4
	movapd	%xmm4, %xmm2
	addsd	%xmm3, %xmm2
	subsd	%xmm8, %xmm2
.L138:
	movapd	%xmm2, %xmm3
	testl	%eax, %eax
	addsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm7
	addsd	%xmm2, %xmm7
	je	.L139
	movapd	%xmm3, %xmm2
	movsd	(%rsp), %xmm15
	movapd	%xmm5, %xmm11
	divsd	%xmm5, %xmm2
	movapd	%xmm15, %xmm8
	movapd	%xmm5, %xmm10
	mulsd	%xmm5, %xmm15
	mulsd	%xmm2, %xmm8
	movapd	%xmm2, %xmm4
	subsd	%xmm15, %xmm11
	movapd	%xmm2, %xmm9
	mulsd	%xmm2, %xmm6
	subsd	%xmm8, %xmm4
	addsd	%xmm15, %xmm11
	addsd	%xmm8, %xmm4
	subsd	%xmm11, %xmm10
	subsd	%xmm4, %xmm9
	movapd	%xmm4, %xmm8
	mulsd	%xmm10, %xmm4
	mulsd	%xmm11, %xmm8
	mulsd	%xmm9, %xmm11
	mulsd	%xmm10, %xmm9
	addsd	%xmm11, %xmm4
	movapd	%xmm8, %xmm11
	addsd	%xmm4, %xmm11
	subsd	%xmm11, %xmm8
	subsd	%xmm11, %xmm3
	addsd	%xmm8, %xmm4
	addsd	%xmm9, %xmm4
	subsd	%xmm4, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm6, %xmm3
	divsd	%xmm5, %xmm3
	movapd	%xmm2, %xmm5
	addsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm2
	addsd	%xmm3, %xmm2
	movsd	u12.6435(%rip), %xmm3
	mulsd	%xmm5, %xmm3
.L524:
	movapd	%xmm2, %xmm4
	addsd	%xmm3, %xmm2
	subsd	%xmm3, %xmm4
	addsd	%xmm5, %xmm2
	addsd	%xmm5, %xmm4
	ucomisd	%xmm4, %xmm2
	jp	.L257
	jne	.L257
	xorpd	.LC3(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L463:
	subsd	%xmm4, %xmm2
	addsd	%xmm6, %xmm2
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L462:
	leaq	72(%rsp), %r12
	leaq	64(%rsp), %r13
	movsd	%xmm1, (%rsp)
	movapd	%xmm1, %xmm0
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__branred@PLT
	movsd	64(%rsp), %xmm0
	movsd	72(%rsp), %xmm2
	movq	.LC6(%rip), %xmm3
	movapd	%xmm0, %xmm6
	movapd	%xmm2, %xmm5
	andpd	%xmm3, %xmm6
	movapd	%xmm0, %xmm4
	andpd	%xmm3, %xmm5
	addsd	%xmm2, %xmm4
	movsd	(%rsp), %xmm1
	ucomisd	%xmm5, %xmm6
	jbe	.L481
	subsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm6
	addsd	%xmm2, %xmm6
.L221:
	pxor	%xmm5, %xmm5
	movsd	%xmm4, 64(%rsp)
	movsd	%xmm6, 72(%rsp)
	ucomisd	%xmm4, %xmm5
	ja	.L538
	movapd	%xmm6, %xmm2
	movapd	%xmm4, %xmm5
	movsd	.LC1(%rip), %xmm0
.L222:
	movsd	gy1.6416(%rip), %xmm7
	ucomisd	%xmm5, %xmm7
	jnb	.L257
	movsd	gy2.6417(%rip), %xmm7
	andl	$1, %eax
	ucomisd	%xmm5, %xmm7
	jb	.L484
	movapd	%xmm4, %xmm0
	movsd	d11.6388(%rip), %xmm2
	testl	%eax, %eax
	mulsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm2
	addsd	d9.6387(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	d7.6386(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	d5.6385(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	d3.6384(%rip), %xmm2
	mulsd	%xmm0, %xmm2
	addsd	%xmm6, %xmm2
	je	.L228
	movapd	%xmm4, %xmm5
	movapd	%xmm2, %xmm0
	movapd	%xmm4, %xmm6
	andpd	%xmm3, %xmm5
	andpd	%xmm3, %xmm0
	addsd	%xmm2, %xmm6
	ucomisd	%xmm0, %xmm5
	jbe	.L485
	subsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm2
.L231:
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm6, %xmm8
	movsd	.LC5(%rip), %xmm15
	movapd	%xmm0, %xmm4
	movapd	%xmm6, %xmm10
	movapd	%xmm15, %xmm7
	movsd	%xmm15, (%rsp)
	divsd	%xmm6, %xmm4
	mulsd	%xmm4, %xmm7
	movapd	%xmm4, %xmm5
	movapd	%xmm4, %xmm9
	mulsd	%xmm4, %xmm2
	subsd	%xmm7, %xmm5
	addsd	%xmm7, %xmm5
	movapd	%xmm15, %xmm7
	mulsd	%xmm6, %xmm7
	subsd	%xmm5, %xmm9
	subsd	%xmm7, %xmm8
	addsd	%xmm7, %xmm8
	movapd	%xmm5, %xmm7
	subsd	%xmm8, %xmm10
	mulsd	%xmm8, %xmm7
	mulsd	%xmm9, %xmm8
	mulsd	%xmm10, %xmm5
	mulsd	%xmm10, %xmm9
	addsd	%xmm8, %xmm5
	movapd	%xmm7, %xmm8
	addsd	%xmm5, %xmm8
	subsd	%xmm8, %xmm7
	subsd	%xmm8, %xmm0
	addsd	%xmm7, %xmm5
	addsd	%xmm9, %xmm5
	subsd	%xmm5, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm0
	movapd	%xmm4, %xmm5
	subsd	%xmm2, %xmm0
	divsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm2
	movsd	u22.6449(%rip), %xmm4
	addsd	%xmm0, %xmm2
	mulsd	%xmm5, %xmm4
	movapd	%xmm2, %xmm0
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm0
	addsd	%xmm5, %xmm2
	addsd	%xmm5, %xmm0
	ucomisd	%xmm0, %xmm2
	jp	.L232
	je	.L506
.L232:
	leaq	80(%rsp), %r15
	movapd	%xmm1, %xmm0
	movl	$10, %esi
	movsd	%xmm1, 24(%rsp)
	leaq	416(%rsp), %r14
	movq	%r15, %rdi
	movaps	%xmm3, 32(%rsp)
	call	__mpranred@PLT
	movl	$10, %edx
	andl	$1, %eax
	movq	%r13, %rsi
	movq	%r15, %rdi
	leaq	752(%rsp), %r13
	movl	%eax, 16(%rsp)
	call	__mp_dbl@PLT
	movsd	64(%rsp), %xmm0
	movl	$10, %esi
	movq	%r14, %rdi
	call	__dbl_mp@PLT
	movl	$10, %ecx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__sub@PLT
	movl	$10, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__mp_dbl@PLT
	movsd	64(%rsp), %xmm8
	movsd	(%rsp), %xmm0
	movapd	%xmm8, %xmm7
	movapd	%xmm8, %xmm15
	mulsd	%xmm8, %xmm0
	movapd	32(%rsp), %xmm3
	movsd	72(%rsp), %xmm1
	subsd	%xmm0, %xmm7
	movsd	%xmm1, 8(%rsp)
	addsd	%xmm0, %xmm7
	subsd	%xmm7, %xmm15
	movapd	%xmm7, %xmm2
	movapd	%xmm7, %xmm0
	mulsd	%xmm7, %xmm0
	mulsd	%xmm15, %xmm2
	movapd	%xmm15, %xmm14
	mulsd	%xmm15, %xmm14
	movapd	%xmm0, %xmm5
	addsd	%xmm2, %xmm2
	addsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm0
	movapd	%xmm5, %xmm4
	addsd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	movsd	24(%rsp), %xmm1
	mulsd	%xmm8, %xmm2
	addsd	%xmm14, %xmm0
	addsd	%xmm2, %xmm2
	addsd	%xmm2, %xmm0
	movsd	a13.6399(%rip), %xmm2
	movapd	%xmm2, %xmm10
	movapd	%xmm2, %xmm6
	addsd	%xmm0, %xmm4
	andpd	%xmm3, %xmm10
	subsd	%xmm4, %xmm5
	movapd	%xmm5, %xmm14
	movsd	aa13.6400(%rip), %xmm5
	addsd	%xmm0, %xmm14
	movsd	a27.6407(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	a25.6406(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	a23.6405(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	a21.6404(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	a19.6403(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	a17.6402(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	addsd	a15.6401(%rip), %xmm0
	mulsd	%xmm4, %xmm0
	movapd	%xmm0, %xmm9
	addsd	%xmm0, %xmm6
	andpd	%xmm3, %xmm9
	ucomisd	%xmm9, %xmm10
	jbe	.L487
	subsd	%xmm6, %xmm2
	addsd	%xmm2, %xmm0
	pxor	%xmm2, %xmm2
	addsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm0
.L237:
	movapd	%xmm0, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm10
	movapd	%xmm9, %xmm2
	mulsd	%xmm4, %xmm2
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm6
	subsd	%xmm2, %xmm5
	subsd	%xmm9, %xmm11
	addsd	%xmm0, %xmm6
	addsd	%xmm5, %xmm2
	movapd	%xmm4, %xmm5
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm6
	subsd	%xmm2, %xmm5
	movapd	%xmm2, %xmm12
	movapd	%xmm2, %xmm13
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm6, %xmm10
	movsd	a11.6397(%rip), %xmm6
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	movapd	%xmm6, %xmm0
	addsd	%xmm9, %xmm13
	movapd	%xmm12, %xmm9
	addsd	%xmm13, %xmm9
	subsd	%xmm9, %xmm12
	addsd	%xmm13, %xmm12
	movsd	aa11.6398(%rip), %xmm13
	addsd	%xmm11, %xmm12
	addsd	%xmm10, %xmm12
	movapd	%xmm9, %xmm10
	addsd	%xmm12, %xmm10
	subsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	addsd	%xmm10, %xmm0
	andpd	%xmm3, %xmm11
	addsd	%xmm12, %xmm9
	movapd	%xmm6, %xmm12
	andpd	%xmm3, %xmm12
	ucomisd	%xmm11, %xmm12
	jbe	.L488
	subsd	%xmm0, %xmm6
	addsd	%xmm10, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
.L240:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	movsd	a9.6395(%rip), %xmm6
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm6, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa9.6396(%rip), %xmm12
	jbe	.L489
	subsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm12, %xmm6
.L243:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	movsd	a7.6393(%rip), %xmm6
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm6, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa7.6394(%rip), %xmm12
	jbe	.L490
	subsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm12, %xmm6
.L246:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	movsd	a5.6391(%rip), %xmm6
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	movapd	%xmm6, %xmm0
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm9, %xmm12
	movapd	%xmm13, %xmm9
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm9
	movapd	%xmm9, %xmm10
	subsd	%xmm9, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm9, %xmm0
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa5.6392(%rip), %xmm12
	jbe	.L491
	subsd	%xmm0, %xmm6
	addsd	%xmm9, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm12, %xmm6
.L249:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm9
	movapd	%xmm2, %xmm12
	addsd	%xmm0, %xmm10
	movapd	%xmm2, %xmm13
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm0
	subsd	%xmm9, %xmm11
	addsd	%xmm6, %xmm0
	addsd	%xmm11, %xmm9
	movapd	%xmm10, %xmm11
	mulsd	%xmm4, %xmm0
	mulsd	%xmm14, %xmm10
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm12
	mulsd	%xmm5, %xmm9
	addsd	%xmm0, %xmm10
	mulsd	%xmm11, %xmm13
	mulsd	%xmm5, %xmm11
	addsd	%xmm13, %xmm9
	movapd	%xmm12, %xmm13
	addsd	%xmm9, %xmm13
	subsd	%xmm13, %xmm12
	movapd	%xmm13, %xmm0
	addsd	%xmm9, %xmm12
	movsd	a3.6389(%rip), %xmm9
	movapd	%xmm9, %xmm6
	addsd	%xmm11, %xmm12
	movapd	%xmm9, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm0
	movapd	%xmm0, %xmm10
	subsd	%xmm0, %xmm13
	andpd	%xmm3, %xmm10
	addsd	%xmm0, %xmm6
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm13
	movsd	aa3.6390(%rip), %xmm12
	jbe	.L492
	subsd	%xmm6, %xmm9
	addsd	%xmm0, %xmm9
	addsd	%xmm13, %xmm9
	addsd	%xmm12, %xmm9
.L252:
	movapd	%xmm9, %xmm11
	movsd	(%rsp), %xmm13
	addsd	%xmm6, %xmm11
	movapd	%xmm13, %xmm0
	mulsd	%xmm11, %xmm0
	movapd	%xmm11, %xmm10
	movapd	%xmm11, %xmm12
	mulsd	%xmm11, %xmm14
	subsd	%xmm11, %xmm6
	subsd	%xmm0, %xmm10
	addsd	%xmm6, %xmm9
	movapd	%xmm7, %xmm6
	addsd	%xmm10, %xmm0
	movapd	%xmm2, %xmm10
	mulsd	%xmm9, %xmm4
	subsd	%xmm0, %xmm12
	mulsd	%xmm0, %xmm10
	mulsd	%xmm5, %xmm0
	addsd	%xmm14, %xmm4
	mulsd	%xmm12, %xmm2
	mulsd	%xmm12, %xmm5
	addsd	%xmm0, %xmm2
	movapd	%xmm10, %xmm0
	addsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm10
	addsd	%xmm2, %xmm10
	addsd	%xmm5, %xmm10
	movapd	%xmm0, %xmm5
	addsd	%xmm4, %xmm10
	movapd	%xmm13, %xmm4
	addsd	%xmm10, %xmm5
	mulsd	%xmm5, %xmm4
	movapd	%xmm5, %xmm2
	subsd	%xmm5, %xmm0
	subsd	%xmm4, %xmm2
	addsd	%xmm10, %xmm0
	addsd	%xmm4, %xmm2
	movapd	%xmm5, %xmm4
	mulsd	%xmm8, %xmm0
	subsd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm6
	mulsd	%xmm15, %xmm2
	mulsd	%xmm4, %xmm7
	mulsd	%xmm4, %xmm15
	addsd	%xmm2, %xmm7
	movapd	%xmm6, %xmm2
	addsd	%xmm7, %xmm2
	subsd	%xmm2, %xmm6
	movapd	%xmm2, %xmm4
	addsd	%xmm6, %xmm7
	movsd	8(%rsp), %xmm6
	mulsd	%xmm6, %xmm5
	addsd	%xmm15, %xmm7
	addsd	%xmm5, %xmm0
	movapd	%xmm8, %xmm5
	addsd	%xmm7, %xmm0
	addsd	%xmm0, %xmm4
	subsd	%xmm4, %xmm2
	addsd	%xmm4, %xmm5
	addsd	%xmm0, %xmm2
	movapd	%xmm8, %xmm0
	andpd	%xmm3, %xmm0
	andpd	%xmm4, %xmm3
	ucomisd	%xmm3, %xmm0
	jbe	.L493
	movapd	%xmm8, %xmm0
	subsd	%xmm5, %xmm0
	addsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm6, %xmm0
.L255:
	movapd	%xmm0, %xmm3
	movl	16(%rsp), %eax
	addsd	%xmm5, %xmm3
	testl	%eax, %eax
	subsd	%xmm3, %xmm5
	movapd	%xmm5, %xmm2
	addsd	%xmm0, %xmm2
	je	.L256
	movsd	.LC1(%rip), %xmm0
	movapd	%xmm3, %xmm9
	movsd	(%rsp), %xmm7
	movapd	%xmm0, %xmm4
	movapd	%xmm7, %xmm6
	divsd	%xmm3, %xmm4
	mulsd	%xmm4, %xmm6
	movapd	%xmm4, %xmm5
	movapd	%xmm4, %xmm8
	mulsd	%xmm4, %xmm2
	subsd	%xmm6, %xmm5
	addsd	%xmm6, %xmm5
	movapd	%xmm7, %xmm6
	movapd	%xmm3, %xmm7
	mulsd	%xmm3, %xmm6
	subsd	%xmm5, %xmm8
	subsd	%xmm6, %xmm7
	addsd	%xmm6, %xmm7
	movapd	%xmm5, %xmm6
	subsd	%xmm7, %xmm9
	mulsd	%xmm7, %xmm6
	mulsd	%xmm8, %xmm7
	mulsd	%xmm9, %xmm5
	mulsd	%xmm9, %xmm8
	addsd	%xmm7, %xmm5
	movapd	%xmm6, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm6
	subsd	%xmm7, %xmm0
	addsd	%xmm6, %xmm5
	addsd	%xmm8, %xmm5
	subsd	%xmm5, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm0
	movapd	%xmm4, %xmm5
	subsd	%xmm2, %xmm0
	divsd	%xmm3, %xmm0
	movsd	u24.6451(%rip), %xmm3
	addsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm3
	movapd	%xmm4, %xmm2
	addsd	%xmm0, %xmm2
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L438:
	movapd	%xmm8, %xmm2
	subsd	%xmm6, %xmm2
	addsd	%xmm4, %xmm2
	addsd	16(%rsp), %xmm2
	addsd	%xmm11, %xmm2
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L437:
	subsd	%xmm6, %xmm5
	addsd	%xmm5, %xmm2
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm2
	addsd	%xmm2, %xmm11
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L436:
	subsd	%xmm7, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm15, %xmm6
	addsd	%xmm6, %xmm10
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L435:
	subsd	%xmm7, %xmm9
	pxor	%xmm15, %xmm15
	addsd	%xmm9, %xmm10
	addsd	%xmm10, %xmm6
	addsd	%xmm15, %xmm6
	jmp	.L51
.L535:
	movq	.LC3(%rip), %xmm7
	movapd	%xmm4, %xmm6
	movsd	.LC0(%rip), %xmm0
	xorpd	%xmm7, %xmm6
	xorpd	%xmm2, %xmm7
	jmp	.L148
.L466:
	movsd	.LC7(%rip), %xmm2
	leaq	xfg.6467(%rip), %rcx
	movsd	e1.6409(%rip), %xmm5
	mulsd	%xmm6, %xmm2
	addsd	mfftnhf.6410(%rip), %xmm2
	cvttsd2si	%xmm2, %edx
	movapd	%xmm7, %xmm2
	movslq	%edx, %rdx
	movq	%rdx, %rsi
	salq	$5, %rsi
	addq	%rcx, %rsi
	testl	%eax, %eax
	subsd	(%rsi), %xmm6
	movsd	16(%rsi), %xmm9
	addsd	%xmm6, %xmm2
	movapd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm4, %xmm5
	mulsd	%xmm2, %xmm4
	addsd	e0.6408(%rip), %xmm5
	mulsd	%xmm4, %xmm5
	movsd	8(%rsi), %xmm4
	movapd	%xmm4, %xmm8
	addsd	%xmm2, %xmm5
	addsd	%xmm9, %xmm8
	mulsd	%xmm5, %xmm8
	je	.L186
	addsd	%xmm4, %xmm5
	movapd	%xmm9, %xmm15
	divsd	%xmm5, %xmm8
	movapd	%xmm8, %xmm5
	movsd	u18.6443(%rip), %xmm8
	movapd	%xmm5, %xmm10
	mulsd	%xmm9, %xmm8
	subsd	%xmm8, %xmm10
	addsd	%xmm5, %xmm8
	subsd	%xmm10, %xmm15
	movapd	%xmm15, %xmm10
	movapd	%xmm9, %xmm15
	subsd	%xmm8, %xmm15
	ucomisd	%xmm10, %xmm15
	jp	.L187
	je	.L504
.L187:
	movapd	%xmm5, %xmm8
	pxor	%xmm15, %xmm15
	movapd	%xmm5, %xmm10
	xorpd	.LC3(%rip), %xmm8
	cmpltsd	%xmm15, %xmm10
	movapd	%xmm9, %xmm15
	andpd	%xmm10, %xmm8
	andnpd	%xmm5, %xmm10
	orpd	%xmm10, %xmm8
	movsd	ua18.6444(%rip), %xmm10
	mulsd	ub18.6445(%rip), %xmm8
	mulsd	%xmm9, %xmm10
	addsd	%xmm10, %xmm8
	movapd	%xmm5, %xmm10
	addsd	%xmm8, %xmm5
	subsd	%xmm8, %xmm10
	subsd	%xmm5, %xmm9
	subsd	%xmm10, %xmm15
	ucomisd	%xmm15, %xmm9
	movapd	%xmm15, %xmm10
	jp	.L191
	je	.L504
.L191:
	salq	$5, %rdx
	movapd	%xmm6, %xmm8
	movsd	24(%rcx,%rdx), %xmm5
	andpd	%xmm3, %xmm8
	movsd	%xmm5, 24(%rsp)
	movapd	%xmm7, %xmm5
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm8
	jbe	.L475
	subsd	%xmm2, %xmm6
	addsd	%xmm7, %xmm6
	movsd	%xmm6, 16(%rsp)
.L200:
	movsd	.LC5(%rip), %xmm5
	movapd	%xmm2, %xmm7
	movapd	%xmm2, %xmm15
	movsd	%xmm5, (%rsp)
	mulsd	%xmm2, %xmm5
	movsd	aa5.6392(%rip), %xmm12
	subsd	%xmm5, %xmm7
	addsd	%xmm5, %xmm7
	subsd	%xmm7, %xmm15
	movapd	%xmm7, %xmm8
	movapd	%xmm7, %xmm5
	mulsd	%xmm7, %xmm5
	mulsd	%xmm15, %xmm8
	movapd	%xmm5, %xmm6
	addsd	%xmm8, %xmm8
	addsd	%xmm8, %xmm6
	subsd	%xmm6, %xmm5
	movapd	%xmm6, %xmm9
	addsd	%xmm8, %xmm5
	movapd	%xmm15, %xmm8
	mulsd	%xmm15, %xmm8
	addsd	%xmm8, %xmm5
	movsd	16(%rsp), %xmm8
	mulsd	%xmm2, %xmm8
	addsd	%xmm8, %xmm8
	addsd	%xmm8, %xmm5
	movsd	a5.6391(%rip), %xmm8
	movapd	%xmm8, %xmm11
	addsd	%xmm5, %xmm9
	andpd	%xmm3, %xmm11
	subsd	%xmm9, %xmm6
	addsd	%xmm5, %xmm6
	movsd	a11.6397(%rip), %xmm5
	mulsd	%xmm9, %xmm5
	movsd	%xmm6, 32(%rsp)
	movapd	%xmm8, %xmm6
	addsd	a9.6395(%rip), %xmm5
	mulsd	%xmm9, %xmm5
	addsd	a7.6393(%rip), %xmm5
	mulsd	%xmm9, %xmm5
	movapd	%xmm5, %xmm10
	addsd	%xmm5, %xmm6
	andpd	%xmm3, %xmm10
	ucomisd	%xmm10, %xmm11
	jbe	.L476
	subsd	%xmm6, %xmm8
	addsd	%xmm8, %xmm5
	pxor	%xmm8, %xmm8
	addsd	%xmm8, %xmm5
	addsd	%xmm12, %xmm5
.L203:
	movsd	(%rsp), %xmm13
	movapd	%xmm9, %xmm8
	movapd	%xmm5, %xmm10
	movapd	%xmm13, %xmm14
	addsd	%xmm6, %xmm10
	mulsd	%xmm9, %xmm14
	movapd	%xmm10, %xmm11
	subsd	%xmm14, %xmm8
	subsd	%xmm10, %xmm6
	addsd	%xmm8, %xmm14
	movapd	%xmm9, %xmm8
	addsd	%xmm5, %xmm6
	subsd	%xmm14, %xmm8
	movapd	%xmm14, %xmm12
	mulsd	%xmm9, %xmm6
	movsd	%xmm8, 8(%rsp)
	movapd	%xmm13, %xmm8
	movapd	%xmm14, %xmm13
	mulsd	%xmm10, %xmm8
	subsd	%xmm8, %xmm11
	addsd	%xmm11, %xmm8
	movapd	%xmm10, %xmm11
	mulsd	32(%rsp), %xmm10
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm12
	mulsd	8(%rsp), %xmm8
	addsd	%xmm6, %xmm10
	movsd	a3.6389(%rip), %xmm6
	mulsd	%xmm11, %xmm13
	mulsd	8(%rsp), %xmm11
	addsd	%xmm8, %xmm13
	movapd	%xmm12, %xmm8
	addsd	%xmm13, %xmm8
	subsd	%xmm8, %xmm12
	movapd	%xmm8, %xmm5
	addsd	%xmm13, %xmm12
	movsd	aa3.6390(%rip), %xmm13
	addsd	%xmm11, %xmm12
	movapd	%xmm6, %xmm11
	andpd	%xmm3, %xmm11
	addsd	%xmm10, %xmm12
	addsd	%xmm12, %xmm5
	movapd	%xmm5, %xmm10
	subsd	%xmm5, %xmm8
	andpd	%xmm3, %xmm10
	ucomisd	%xmm10, %xmm11
	addsd	%xmm12, %xmm8
	movapd	%xmm6, %xmm12
	addsd	%xmm5, %xmm12
	jbe	.L477
	subsd	%xmm12, %xmm6
	addsd	%xmm5, %xmm6
	addsd	%xmm8, %xmm6
	addsd	%xmm13, %xmm6
.L206:
	movapd	%xmm6, %xmm10
	movsd	(%rsp), %xmm13
	addsd	%xmm12, %xmm10
	movapd	%xmm13, %xmm5
	mulsd	%xmm10, %xmm5
	movapd	%xmm10, %xmm8
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm12
	mulsd	32(%rsp), %xmm10
	subsd	%xmm5, %xmm8
	addsd	%xmm6, %xmm12
	movapd	%xmm13, %xmm6
	addsd	%xmm8, %xmm5
	movapd	%xmm14, %xmm8
	mulsd	%xmm9, %xmm12
	subsd	%xmm5, %xmm11
	mulsd	%xmm5, %xmm8
	mulsd	8(%rsp), %xmm5
	addsd	%xmm10, %xmm12
	mulsd	%xmm11, %xmm14
	mulsd	8(%rsp), %xmm11
	addsd	%xmm5, %xmm14
	movapd	%xmm8, %xmm5
	addsd	%xmm14, %xmm5
	subsd	%xmm5, %xmm8
	movapd	%xmm5, %xmm10
	addsd	%xmm14, %xmm8
	addsd	%xmm11, %xmm8
	addsd	%xmm12, %xmm8
	addsd	%xmm8, %xmm10
	mulsd	%xmm10, %xmm6
	movapd	%xmm10, %xmm9
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm5
	subsd	%xmm6, %xmm9
	addsd	%xmm8, %xmm5
	addsd	%xmm6, %xmm9
	movapd	%xmm7, %xmm6
	mulsd	%xmm2, %xmm5
	subsd	%xmm9, %xmm11
	mulsd	%xmm9, %xmm6
	mulsd	%xmm15, %xmm9
	mulsd	%xmm11, %xmm7
	mulsd	%xmm11, %xmm15
	addsd	%xmm9, %xmm7
	movapd	%xmm6, %xmm9
	addsd	%xmm7, %xmm9
	subsd	%xmm9, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm2, %xmm7
	addsd	%xmm15, %xmm6
	movsd	16(%rsp), %xmm15
	mulsd	%xmm15, %xmm10
	addsd	%xmm10, %xmm5
	addsd	%xmm6, %xmm5
	movapd	%xmm9, %xmm6
	addsd	%xmm5, %xmm6
	subsd	%xmm6, %xmm9
	movapd	%xmm6, %xmm8
	addsd	%xmm6, %xmm7
	andpd	%xmm3, %xmm8
	addsd	%xmm9, %xmm5
	movapd	%xmm2, %xmm9
	andpd	%xmm3, %xmm9
	ucomisd	%xmm8, %xmm9
	jbe	.L478
	subsd	%xmm7, %xmm2
	addsd	%xmm6, %xmm2
	addsd	%xmm5, %xmm2
	addsd	%xmm15, %xmm2
.L209:
	movapd	%xmm2, %xmm9
	movapd	%xmm4, %xmm6
	addsd	%xmm7, %xmm9
	subsd	%xmm9, %xmm7
	movapd	%xmm9, %xmm5
	addsd	%xmm9, %xmm6
	andpd	%xmm3, %xmm5
	addsd	%xmm7, %xmm2
	movapd	%xmm4, %xmm7
	andpd	%xmm3, %xmm7
	ucomisd	%xmm5, %xmm7
	jbe	.L479
	movapd	%xmm4, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm9, %xmm7
	addsd	%xmm2, %xmm7
	addsd	24(%rsp), %xmm7
.L212:
	movapd	%xmm7, %xmm5
	movsd	(%rsp), %xmm15
	movapd	%xmm4, %xmm12
	mulsd	%xmm4, %xmm2
	addsd	%xmm6, %xmm5
	movapd	%xmm9, %xmm8
	movapd	%xmm4, %xmm10
	movapd	%xmm9, %xmm11
	subsd	%xmm5, %xmm6
	addsd	%xmm7, %xmm6
	movapd	%xmm15, %xmm7
	mulsd	%xmm4, %xmm7
	movsd	.LC1(%rip), %xmm4
	subsd	%xmm7, %xmm12
	addsd	%xmm7, %xmm12
	movapd	%xmm15, %xmm7
	mulsd	%xmm9, %xmm7
	mulsd	24(%rsp), %xmm9
	subsd	%xmm12, %xmm10
	subsd	%xmm7, %xmm8
	addsd	%xmm9, %xmm2
	addsd	%xmm7, %xmm8
	movapd	%xmm12, %xmm7
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm7
	mulsd	%xmm10, %xmm8
	mulsd	%xmm11, %xmm12
	mulsd	%xmm11, %xmm10
	addsd	%xmm8, %xmm12
	movapd	%xmm7, %xmm8
	addsd	%xmm12, %xmm8
	subsd	%xmm8, %xmm7
	movapd	%xmm8, %xmm9
	addsd	%xmm12, %xmm7
	addsd	%xmm10, %xmm7
	addsd	%xmm7, %xmm2
	movapd	%xmm4, %xmm7
	addsd	%xmm2, %xmm9
	andpd	%xmm9, %xmm3
	subsd	%xmm9, %xmm8
	subsd	%xmm9, %xmm7
	ucomisd	%xmm3, %xmm4
	addsd	%xmm2, %xmm8
	ja	.L539
	addsd	%xmm7, %xmm9
	pxor	%xmm3, %xmm3
	subsd	%xmm9, %xmm4
	movapd	%xmm4, %xmm2
	addsd	%xmm3, %xmm2
	subsd	%xmm8, %xmm2
.L215:
	movapd	%xmm2, %xmm3
	testl	%eax, %eax
	addsd	%xmm7, %xmm3
	subsd	%xmm3, %xmm7
	addsd	%xmm2, %xmm7
	je	.L216
	movapd	%xmm3, %xmm2
	movsd	(%rsp), %xmm15
	movapd	%xmm5, %xmm11
	divsd	%xmm5, %xmm2
	movapd	%xmm15, %xmm8
	movapd	%xmm5, %xmm10
	mulsd	%xmm5, %xmm15
	mulsd	%xmm2, %xmm8
	movapd	%xmm2, %xmm4
	subsd	%xmm15, %xmm11
	movapd	%xmm2, %xmm9
	mulsd	%xmm2, %xmm6
	subsd	%xmm8, %xmm4
	addsd	%xmm15, %xmm11
	addsd	%xmm8, %xmm4
	subsd	%xmm11, %xmm10
	subsd	%xmm4, %xmm9
	movapd	%xmm4, %xmm8
	mulsd	%xmm10, %xmm4
	mulsd	%xmm11, %xmm8
	mulsd	%xmm9, %xmm11
	mulsd	%xmm10, %xmm9
	addsd	%xmm11, %xmm4
	movapd	%xmm8, %xmm11
	addsd	%xmm4, %xmm11
	subsd	%xmm11, %xmm8
	subsd	%xmm11, %xmm3
	addsd	%xmm8, %xmm4
	addsd	%xmm9, %xmm4
	subsd	%xmm4, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm6, %xmm3
	divsd	%xmm5, %xmm3
	movapd	%xmm2, %xmm5
	addsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm2
	addsd	%xmm3, %xmm2
	movsd	u20.6447(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	jmp	.L524
.L532:
	movapd	%xmm2, %xmm3
	subsd	%xmm7, %xmm3
	subsd	%xmm8, %xmm3
	subsd	%xmm4, %xmm3
	pxor	%xmm4, %xmm4
	addsd	%xmm4, %xmm3
	jmp	.L63
.L109:
	subsd	%xmm3, %xmm6
	divsd	%xmm6, %xmm5
	movsd	u9.6428(%rip), %xmm6
	mulsd	%xmm2, %xmm6
	movapd	%xmm5, %xmm3
	subsd	%xmm6, %xmm3
	addsd	%xmm5, %xmm6
	addsd	%xmm2, %xmm3
	addsd	%xmm2, %xmm6
	ucomisd	%xmm3, %xmm6
	jp	.L116
	jne	.L116
	mulsd	%xmm3, %xmm0
	jmp	.L8
.L74:
	movsd	u5.6424(%rip), %xmm3
	movapd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm3
	addsd	%xmm4, %xmm0
	addsd	%xmm3, %xmm4
	ucomisd	%xmm0, %xmm4
	jp	.L502
	je	.L8
.L502:
	movsd	.LC5(%rip), %xmm3
	movsd	%xmm3, (%rsp)
	movq	.LC6(%rip), %xmm3
	jmp	.L78
.L481:
	subsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm6
	addsd	%xmm0, %xmm6
	jmp	.L221
.L504:
	xorpd	.LC3(%rip), %xmm0
	mulsd	%xmm10, %xmm0
	jmp	.L8
.L508:
	xorpd	.LC3(%rip), %xmm0
	mulsd	%xmm9, %xmm0
	jmp	.L8
.L538:
	movq	.LC3(%rip), %xmm2
	movapd	%xmm4, %xmm5
	movsd	.LC0(%rip), %xmm0
	xorpd	%xmm2, %xmm5
	xorpd	%xmm6, %xmm2
	jmp	.L222
.L484:
	movsd	.LC7(%rip), %xmm4
	leaq	xfg.6467(%rip), %rcx
	movsd	e1.6409(%rip), %xmm6
	mulsd	%xmm5, %xmm4
	addsd	mfftnhf.6410(%rip), %xmm4
	cvttsd2si	%xmm4, %edx
	movapd	%xmm2, %xmm4
	movslq	%edx, %rdx
	movq	%rdx, %rsi
	salq	$5, %rsi
	addq	%rcx, %rsi
	testl	%eax, %eax
	subsd	(%rsi), %xmm5
	movsd	16(%rsi), %xmm9
	addsd	%xmm5, %xmm4
	movapd	%xmm5, %xmm7
	movapd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm5, %xmm6
	mulsd	%xmm4, %xmm5
	addsd	e0.6408(%rip), %xmm6
	mulsd	%xmm5, %xmm6
	movsd	8(%rsi), %xmm5
	movapd	%xmm5, %xmm8
	addsd	%xmm4, %xmm6
	addsd	%xmm9, %xmm8
	mulsd	%xmm6, %xmm8
	je	.L260
	addsd	%xmm5, %xmm6
	movapd	%xmm9, %xmm15
	divsd	%xmm6, %xmm8
	movsd	u26.6455(%rip), %xmm6
	mulsd	%xmm9, %xmm6
	movapd	%xmm8, %xmm10
	subsd	%xmm6, %xmm10
	addsd	%xmm8, %xmm6
	subsd	%xmm10, %xmm15
	movapd	%xmm15, %xmm10
	movapd	%xmm9, %xmm15
	subsd	%xmm6, %xmm15
	ucomisd	%xmm10, %xmm15
	jp	.L261
	je	.L504
.L261:
	movapd	%xmm8, %xmm6
	pxor	%xmm15, %xmm15
	movapd	%xmm8, %xmm10
	xorpd	.LC3(%rip), %xmm6
	cmpltsd	%xmm15, %xmm10
	movapd	%xmm9, %xmm15
	andpd	%xmm10, %xmm6
	andnpd	%xmm8, %xmm10
	orpd	%xmm10, %xmm6
	movsd	ua26.6456(%rip), %xmm10
	mulsd	ub26.6457(%rip), %xmm6
	mulsd	%xmm9, %xmm10
	addsd	%xmm10, %xmm6
	movapd	%xmm8, %xmm10
	subsd	%xmm6, %xmm10
	addsd	%xmm8, %xmm6
	subsd	%xmm10, %xmm15
	subsd	%xmm6, %xmm9
	movapd	%xmm15, %xmm10
	ucomisd	%xmm15, %xmm9
	jp	.L265
	je	.L504
.L265:
	salq	$5, %rdx
	movapd	%xmm7, %xmm8
	movsd	24(%rcx,%rdx), %xmm6
	andpd	%xmm3, %xmm8
	movsd	%xmm6, 24(%rsp)
	movapd	%xmm2, %xmm6
	andpd	%xmm3, %xmm6
	ucomisd	%xmm6, %xmm8
	jbe	.L494
	subsd	%xmm4, %xmm7
	addsd	%xmm7, %xmm2
.L274:
	movsd	.LC5(%rip), %xmm6
	movapd	%xmm4, %xmm7
	movapd	%xmm4, %xmm14
	movsd	%xmm6, (%rsp)
	mulsd	%xmm4, %xmm6
	movsd	aa5.6392(%rip), %xmm12
	subsd	%xmm6, %xmm7
	addsd	%xmm7, %xmm6
	subsd	%xmm6, %xmm14
	movapd	%xmm6, %xmm9
	movapd	%xmm6, %xmm7
	mulsd	%xmm6, %xmm7
	mulsd	%xmm14, %xmm9
	movapd	%xmm7, %xmm8
	addsd	%xmm9, %xmm9
	addsd	%xmm9, %xmm8
	subsd	%xmm8, %xmm7
	addsd	%xmm9, %xmm7
	movapd	%xmm14, %xmm9
	mulsd	%xmm14, %xmm9
	addsd	%xmm9, %xmm7
	movapd	%xmm2, %xmm9
	mulsd	%xmm4, %xmm9
	addsd	%xmm9, %xmm9
	addsd	%xmm9, %xmm7
	movapd	%xmm8, %xmm9
	addsd	%xmm7, %xmm9
	subsd	%xmm9, %xmm8
	addsd	%xmm7, %xmm8
	movsd	a11.6397(%rip), %xmm7
	mulsd	%xmm9, %xmm7
	movsd	%xmm8, 32(%rsp)
	movsd	a5.6391(%rip), %xmm8
	addsd	a9.6395(%rip), %xmm7
	movapd	%xmm8, %xmm11
	movapd	%xmm8, %xmm15
	andpd	%xmm3, %xmm11
	mulsd	%xmm9, %xmm7
	addsd	a7.6393(%rip), %xmm7
	mulsd	%xmm9, %xmm7
	movapd	%xmm7, %xmm10
	addsd	%xmm7, %xmm15
	andpd	%xmm3, %xmm10
	ucomisd	%xmm10, %xmm11
	jbe	.L495
	subsd	%xmm15, %xmm8
	addsd	%xmm7, %xmm8
	pxor	%xmm7, %xmm7
	addsd	%xmm7, %xmm8
	addsd	%xmm12, %xmm8
	movsd	%xmm8, 16(%rsp)
.L277:
	movsd	(%rsp), %xmm13
	movapd	%xmm9, %xmm8
	movsd	16(%rsp), %xmm10
	movapd	%xmm13, %xmm7
	addsd	%xmm15, %xmm10
	mulsd	%xmm9, %xmm7
	movapd	%xmm10, %xmm11
	subsd	%xmm7, %xmm8
	addsd	%xmm8, %xmm7
	movapd	%xmm9, %xmm8
	subsd	%xmm7, %xmm8
	movapd	%xmm7, %xmm12
	movsd	%xmm8, 8(%rsp)
	movapd	%xmm13, %xmm8
	movapd	%xmm7, %xmm13
	mulsd	%xmm10, %xmm8
	subsd	%xmm8, %xmm11
	addsd	%xmm11, %xmm8
	movapd	%xmm10, %xmm11
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm12
	mulsd	8(%rsp), %xmm8
	mulsd	%xmm11, %xmm13
	mulsd	8(%rsp), %xmm11
	addsd	%xmm13, %xmm8
	movapd	%xmm12, %xmm13
	addsd	%xmm8, %xmm13
	subsd	%xmm13, %xmm12
	addsd	%xmm8, %xmm12
	movapd	%xmm15, %xmm8
	subsd	%xmm10, %xmm8
	mulsd	32(%rsp), %xmm10
	addsd	%xmm12, %xmm11
	addsd	16(%rsp), %xmm8
	mulsd	%xmm9, %xmm8
	addsd	%xmm8, %xmm10
	movapd	%xmm13, %xmm8
	addsd	%xmm10, %xmm11
	movsd	a3.6389(%rip), %xmm10
	movapd	%xmm10, %xmm12
	addsd	%xmm11, %xmm8
	andpd	%xmm3, %xmm12
	movapd	%xmm8, %xmm15
	subsd	%xmm8, %xmm13
	andpd	%xmm3, %xmm15
	ucomisd	%xmm15, %xmm12
	addsd	%xmm13, %xmm11
	movapd	%xmm10, %xmm13
	addsd	%xmm8, %xmm13
	jbe	.L496
	subsd	%xmm13, %xmm10
	movsd	aa3.6390(%rip), %xmm15
	addsd	%xmm8, %xmm10
	addsd	%xmm11, %xmm10
	addsd	%xmm10, %xmm15
.L280:
	movapd	%xmm15, %xmm10
	movsd	(%rsp), %xmm8
	addsd	%xmm13, %xmm10
	mulsd	%xmm10, %xmm8
	movapd	%xmm10, %xmm12
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm13
	mulsd	32(%rsp), %xmm10
	subsd	%xmm8, %xmm12
	addsd	%xmm15, %xmm13
	addsd	%xmm8, %xmm12
	movapd	%xmm7, %xmm8
	mulsd	%xmm9, %xmm13
	movsd	(%rsp), %xmm9
	subsd	%xmm12, %xmm11
	mulsd	%xmm12, %xmm8
	mulsd	8(%rsp), %xmm12
	addsd	%xmm13, %xmm10
	mulsd	%xmm11, %xmm7
	mulsd	8(%rsp), %xmm11
	addsd	%xmm7, %xmm12
	movapd	%xmm8, %xmm7
	addsd	%xmm12, %xmm7
	subsd	%xmm7, %xmm8
	addsd	%xmm12, %xmm8
	addsd	%xmm11, %xmm8
	addsd	%xmm10, %xmm8
	movapd	%xmm7, %xmm10
	addsd	%xmm8, %xmm10
	mulsd	%xmm10, %xmm9
	movapd	%xmm10, %xmm13
	movapd	%xmm10, %xmm11
	subsd	%xmm10, %xmm7
	mulsd	%xmm2, %xmm10
	subsd	%xmm9, %xmm13
	addsd	%xmm7, %xmm8
	addsd	%xmm9, %xmm13
	movapd	%xmm6, %xmm9
	mulsd	%xmm4, %xmm8
	subsd	%xmm13, %xmm11
	mulsd	%xmm13, %xmm9
	mulsd	%xmm14, %xmm13
	addsd	%xmm10, %xmm8
	movapd	%xmm4, %xmm10
	mulsd	%xmm11, %xmm6
	movapd	%xmm13, %xmm12
	mulsd	%xmm14, %xmm11
	andpd	%xmm3, %xmm10
	addsd	%xmm6, %xmm12
	movapd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	subsd	%xmm6, %xmm9
	movapd	%xmm6, %xmm7
	addsd	%xmm12, %xmm9
	addsd	%xmm11, %xmm9
	addsd	%xmm9, %xmm8
	addsd	%xmm8, %xmm7
	movapd	%xmm7, %xmm9
	subsd	%xmm7, %xmm6
	andpd	%xmm3, %xmm9
	ucomisd	%xmm9, %xmm10
	addsd	%xmm6, %xmm8
	movapd	%xmm4, %xmm6
	addsd	%xmm7, %xmm6
	jbe	.L497
	subsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm7
	addsd	%xmm7, %xmm8
	addsd	%xmm8, %xmm2
.L283:
	movapd	%xmm2, %xmm7
	movapd	%xmm5, %xmm4
	addsd	%xmm6, %xmm7
	andpd	%xmm3, %xmm4
	subsd	%xmm7, %xmm6
	addsd	%xmm2, %xmm6
	movapd	%xmm7, %xmm2
	andpd	%xmm3, %xmm2
	movapd	%xmm6, %xmm8
	ucomisd	%xmm2, %xmm4
	movapd	%xmm5, %xmm6
	addsd	%xmm7, %xmm6
	jbe	.L498
	movapd	%xmm5, %xmm2
	subsd	%xmm6, %xmm2
	addsd	%xmm7, %xmm2
	addsd	%xmm8, %xmm2
	addsd	24(%rsp), %xmm2
.L286:
	movapd	%xmm2, %xmm4
	movsd	(%rsp), %xmm15
	movapd	%xmm5, %xmm9
	addsd	%xmm6, %xmm4
	movapd	%xmm7, %xmm10
	movapd	%xmm5, %xmm11
	movapd	%xmm7, %xmm12
	subsd	%xmm4, %xmm6
	addsd	%xmm2, %xmm6
	movapd	%xmm15, %xmm2
	mulsd	%xmm7, %xmm15
	mulsd	%xmm5, %xmm2
	mulsd	24(%rsp), %xmm7
	mulsd	%xmm8, %xmm5
	subsd	%xmm15, %xmm10
	subsd	%xmm2, %xmm9
	addsd	%xmm9, %xmm2
	movapd	%xmm15, %xmm9
	addsd	%xmm10, %xmm9
	subsd	%xmm2, %xmm11
	movapd	%xmm2, %xmm10
	subsd	%xmm9, %xmm12
	mulsd	%xmm9, %xmm10
	mulsd	%xmm11, %xmm9
	mulsd	%xmm12, %xmm2
	mulsd	%xmm12, %xmm11
	addsd	%xmm9, %xmm2
	movapd	%xmm10, %xmm9
	addsd	%xmm2, %xmm9
	subsd	%xmm9, %xmm10
	movapd	%xmm9, %xmm8
	addsd	%xmm10, %xmm2
	movapd	%xmm11, %xmm10
	addsd	%xmm2, %xmm10
	movapd	%xmm5, %xmm2
	addsd	%xmm7, %xmm2
	movapd	%xmm9, %xmm7
	addsd	%xmm10, %xmm2
	addsd	%xmm2, %xmm8
	subsd	%xmm8, %xmm7
	andpd	%xmm8, %xmm3
	addsd	%xmm2, %xmm7
	movsd	.LC1(%rip), %xmm2
	ucomisd	%xmm3, %xmm2
	movapd	%xmm2, %xmm5
	subsd	%xmm8, %xmm5
	ja	.L540
	addsd	%xmm5, %xmm8
	pxor	%xmm3, %xmm3
	subsd	%xmm8, %xmm2
	addsd	%xmm3, %xmm2
	subsd	%xmm7, %xmm2
.L289:
	movapd	%xmm2, %xmm3
	testl	%eax, %eax
	addsd	%xmm5, %xmm3
	subsd	%xmm3, %xmm5
	addsd	%xmm2, %xmm5
	movapd	%xmm5, %xmm7
	je	.L290
	movapd	%xmm3, %xmm2
	movsd	(%rsp), %xmm15
	movapd	%xmm4, %xmm11
	divsd	%xmm4, %xmm2
	movapd	%xmm4, %xmm10
	movapd	%xmm15, %xmm8
	mulsd	%xmm4, %xmm15
	mulsd	%xmm2, %xmm8
	movapd	%xmm2, %xmm5
	subsd	%xmm15, %xmm11
	movapd	%xmm2, %xmm9
	mulsd	%xmm2, %xmm6
	subsd	%xmm8, %xmm5
	addsd	%xmm15, %xmm11
	addsd	%xmm8, %xmm5
	subsd	%xmm11, %xmm10
	subsd	%xmm5, %xmm9
	movapd	%xmm5, %xmm8
	mulsd	%xmm10, %xmm5
	mulsd	%xmm11, %xmm8
	mulsd	%xmm9, %xmm11
	mulsd	%xmm10, %xmm9
	addsd	%xmm11, %xmm5
	movapd	%xmm8, %xmm11
	addsd	%xmm5, %xmm11
	subsd	%xmm11, %xmm8
	subsd	%xmm11, %xmm3
	addsd	%xmm8, %xmm5
	addsd	%xmm9, %xmm5
	subsd	%xmm5, %xmm3
	movapd	%xmm2, %xmm5
	addsd	%xmm7, %xmm3
	subsd	%xmm6, %xmm3
	divsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm5
	subsd	%xmm5, %xmm2
	addsd	%xmm3, %xmm2
	movsd	u28.6459(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	jmp	.L524
.L154:
	movsd	u13.6436(%rip), %xmm6
	movapd	%xmm5, %xmm0
	mulsd	%xmm4, %xmm6
	subsd	%xmm6, %xmm0
	addsd	%xmm5, %xmm6
	addsd	%xmm4, %xmm0
	addsd	%xmm4, %xmm6
	ucomisd	%xmm0, %xmm6
	jp	.L366
	je	.L8
.L366:
	movapd	%xmm4, %xmm5
	andpd	%xmm3, %xmm5
	movsd	%xmm5, 8(%rsp)
	movsd	.LC5(%rip), %xmm5
	movsd	%xmm5, (%rsp)
	jmp	.L158
.L186:
	subsd	%xmm5, %xmm9
	movsd	u17.6440(%rip), %xmm5
	mulsd	%xmm4, %xmm5
	divsd	%xmm9, %xmm8
	movapd	%xmm8, %xmm9
	subsd	%xmm5, %xmm9
	addsd	%xmm8, %xmm5
	addsd	%xmm4, %xmm9
	addsd	%xmm4, %xmm5
	ucomisd	%xmm9, %xmm5
	jp	.L193
	je	.L511
.L193:
	movapd	%xmm8, %xmm5
	pxor	%xmm15, %xmm15
	movapd	%xmm8, %xmm9
	xorpd	.LC3(%rip), %xmm5
	cmpltsd	%xmm15, %xmm9
	andpd	%xmm9, %xmm5
	andnpd	%xmm8, %xmm9
	orpd	%xmm9, %xmm5
	movsd	ua17.6441(%rip), %xmm9
	mulsd	ub17.6442(%rip), %xmm5
	mulsd	%xmm4, %xmm9
	addsd	%xmm9, %xmm5
	movapd	%xmm8, %xmm9
	addsd	%xmm5, %xmm8
	subsd	%xmm5, %xmm9
	addsd	%xmm4, %xmm8
	addsd	%xmm4, %xmm9
	ucomisd	%xmm9, %xmm8
	jp	.L191
	jne	.L191
.L511:
	mulsd	%xmm9, %xmm0
	jmp	.L8
.L105:
	movsd	u7.6426(%rip), %xmm2
	movapd	%xmm5, %xmm0
	mulsd	%xmm4, %xmm2
	subsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm2
	addsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm4
	ucomisd	%xmm0, %xmm4
	jnp	.L507
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L444:
	subsd	%xmm7, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm4, %xmm0
	jmp	.L77
.L116:
	pxor	%xmm3, %xmm3
	ucomisd	%xmm5, %xmm3
	movapd	%xmm5, %xmm3
	jbe	.L118
	xorpd	.LC3(%rip), %xmm3
.L118:
	mulsd	ub9.6430(%rip), %xmm3
	movapd	%xmm3, %xmm6
	movsd	ua9.6429(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	addsd	%xmm6, %xmm3
	movapd	%xmm5, %xmm6
	subsd	%xmm3, %xmm6
	addsd	%xmm5, %xmm3
	addsd	%xmm2, %xmm6
	addsd	%xmm2, %xmm3
	ucomisd	%xmm6, %xmm3
	jp	.L114
	jne	.L114
	mulsd	%xmm6, %xmm0
	jmp	.L8
.L453:
	subsd	%xmm5, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm7, %xmm0
	addsd	8(%rsp), %xmm0
	addsd	%xmm6, %xmm0
	jmp	.L104
.L452:
	subsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm9
	addsd	%xmm12, %xmm9
	addsd	%xmm13, %xmm9
	jmp	.L101
.L451:
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm13, %xmm6
	jmp	.L98
.L450:
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm13, %xmm6
	jmp	.L95
.L449:
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm13, %xmm6
	jmp	.L92
.L448:
	subsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm9, %xmm6
	jmp	.L89
.L447:
	subsd	%xmm6, %xmm2
	movapd	%xmm2, %xmm0
	addsd	%xmm9, %xmm0
	addsd	%xmm5, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm0
	jmp	.L86
.L446:
	subsd	%xmm7, %xmm0
	addsd	%xmm8, %xmm0
	movsd	%xmm0, 8(%rsp)
	jmp	.L83
.L260:
	subsd	%xmm6, %xmm9
	divsd	%xmm9, %xmm8
	movsd	u25.6452(%rip), %xmm9
	mulsd	%xmm5, %xmm9
	movapd	%xmm8, %xmm6
	subsd	%xmm9, %xmm8
	addsd	%xmm6, %xmm9
	addsd	%xmm5, %xmm8
	addsd	%xmm5, %xmm9
	ucomisd	%xmm8, %xmm9
	jp	.L267
	jne	.L267
	mulsd	%xmm8, %xmm0
	jmp	.L8
.L228:
	movsd	u21.6448(%rip), %xmm5
	movapd	%xmm2, %xmm0
	mulsd	%xmm4, %xmm5
	subsd	%xmm5, %xmm0
	addsd	%xmm5, %xmm2
	addsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm4
	ucomisd	%xmm0, %xmm4
	jp	.L503
	je	.L8
.L503:
	movsd	.LC5(%rip), %xmm5
	movsd	%xmm5, (%rsp)
	jmp	.L232
.L467:
	subsd	%xmm7, %xmm5
	addsd	%xmm4, %xmm5
	jmp	.L157
.L182:
	movsd	u15.6438(%rip), %xmm4
	mulsd	%xmm3, %xmm4
.L517:
	movapd	%xmm2, %xmm0
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm0
	addsd	%xmm3, %xmm0
	addsd	%xmm2, %xmm3
	ucomisd	%xmm0, %xmm3
	jnp	.L507
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L474:
	subsd	%xmm5, %xmm6
	addsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm2
	addsd	%xmm0, %xmm2
	jmp	.L181
.L473:
	subsd	%xmm8, %xmm0
	addsd	%xmm9, %xmm0
	addsd	%xmm12, %xmm0
	movapd	%xmm0, %xmm9
	addsd	%xmm13, %xmm9
	jmp	.L178
.L472:
	subsd	%xmm0, %xmm9
	addsd	%xmm8, %xmm9
	movapd	%xmm9, %xmm8
	addsd	%xmm12, %xmm8
	addsd	%xmm13, %xmm8
	jmp	.L175
.L471:
	subsd	%xmm0, %xmm9
	addsd	%xmm8, %xmm9
	movapd	%xmm9, %xmm8
	addsd	%xmm12, %xmm8
	addsd	%xmm13, %xmm8
	jmp	.L172
.L470:
	subsd	%xmm0, %xmm9
	addsd	%xmm8, %xmm9
	movapd	%xmm9, %xmm8
	addsd	%xmm12, %xmm8
	addsd	%xmm13, %xmm8
	jmp	.L169
.L469:
	subsd	%xmm0, %xmm10
	addsd	%xmm8, %xmm10
	movapd	%xmm10, %xmm8
	addsd	%xmm13, %xmm8
	addsd	%xmm9, %xmm8
	jmp	.L166
.L468:
	subsd	%xmm8, %xmm5
	addsd	%xmm5, %xmm7
	pxor	%xmm5, %xmm5
	addsd	%xmm7, %xmm0
	addsd	%xmm5, %xmm0
	jmp	.L163
.L139:
	movapd	%xmm5, %xmm4
	movsd	(%rsp), %xmm15
	movapd	%xmm3, %xmm11
	divsd	%xmm3, %xmm4
	movapd	%xmm15, %xmm8
	mulsd	%xmm3, %xmm15
	mulsd	%xmm4, %xmm8
	movapd	%xmm4, %xmm2
	movapd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm7
	subsd	%xmm8, %xmm2
	addsd	%xmm8, %xmm2
	movapd	%xmm3, %xmm8
	subsd	%xmm15, %xmm8
	subsd	%xmm2, %xmm10
	movapd	%xmm2, %xmm9
	addsd	%xmm15, %xmm8
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm9
	mulsd	%xmm10, %xmm8
	mulsd	%xmm11, %xmm2
	movapd	%xmm9, %xmm12
	mulsd	%xmm11, %xmm10
	addsd	%xmm2, %xmm8
	addsd	%xmm8, %xmm12
	subsd	%xmm12, %xmm5
	movapd	%xmm5, %xmm2
	movapd	%xmm9, %xmm5
	subsd	%xmm12, %xmm5
	addsd	%xmm8, %xmm5
	addsd	%xmm10, %xmm5
	subsd	%xmm5, %xmm2
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm2
	subsd	%xmm7, %xmm2
	divsd	%xmm3, %xmm2
	movsd	u11.6434(%rip), %xmm3
	addsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm3
	addsd	%xmm4, %xmm2
.L525:
	movapd	%xmm2, %xmm4
	addsd	%xmm3, %xmm2
	subsd	%xmm3, %xmm4
	addsd	%xmm5, %xmm2
	addsd	%xmm5, %xmm4
	ucomisd	%xmm4, %xmm2
	jp	.L257
.L522:
	jne	.L257
	mulsd	%xmm4, %xmm0
	jmp	.L8
.L459:
	subsd	%xmm5, %xmm6
	addsd	%xmm6, %xmm4
	addsd	8(%rsp), %xmm4
	addsd	%xmm9, %xmm4
	jmp	.L132
.L458:
	subsd	%xmm10, %xmm7
	addsd	%xmm11, %xmm7
	addsd	aa3.6390(%rip), %xmm7
	addsd	%xmm7, %xmm14
	jmp	.L129
.L457:
	subsd	%xmm14, %xmm6
	addsd	%xmm7, %xmm6
	pxor	%xmm7, %xmm7
	addsd	%xmm11, %xmm6
	addsd	%xmm7, %xmm6
	movsd	%xmm6, 24(%rsp)
	jmp	.L126
.L456:
	subsd	%xmm4, %xmm8
	addsd	%xmm7, %xmm8
	movsd	%xmm8, 8(%rsp)
	jmp	.L123
.L460:
	movapd	%xmm9, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm2, %xmm7
	addsd	32(%rsp), %xmm7
	addsd	%xmm4, %xmm7
	jmp	.L135
.L537:
	subsd	%xmm7, %xmm4
	pxor	%xmm3, %xmm3
	movapd	%xmm4, %xmm2
	subsd	%xmm9, %xmm2
	subsd	%xmm8, %xmm2
	addsd	%xmm3, %xmm2
	jmp	.L138
.L216:
	movapd	%xmm5, %xmm4
	movsd	(%rsp), %xmm15
	movapd	%xmm3, %xmm11
	divsd	%xmm3, %xmm4
	movapd	%xmm15, %xmm8
	mulsd	%xmm3, %xmm15
	mulsd	%xmm4, %xmm8
	movapd	%xmm4, %xmm2
	movapd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm7
	subsd	%xmm8, %xmm2
	addsd	%xmm8, %xmm2
	movapd	%xmm3, %xmm8
	subsd	%xmm15, %xmm8
	subsd	%xmm2, %xmm10
	movapd	%xmm2, %xmm9
	addsd	%xmm15, %xmm8
	subsd	%xmm8, %xmm11
	mulsd	%xmm8, %xmm9
	mulsd	%xmm10, %xmm8
	mulsd	%xmm11, %xmm2
	movapd	%xmm9, %xmm12
	mulsd	%xmm11, %xmm10
	addsd	%xmm2, %xmm8
	addsd	%xmm8, %xmm12
	subsd	%xmm12, %xmm5
	movapd	%xmm5, %xmm2
	movapd	%xmm9, %xmm5
	subsd	%xmm12, %xmm5
	addsd	%xmm8, %xmm5
	addsd	%xmm10, %xmm5
	subsd	%xmm5, %xmm2
	movapd	%xmm4, %xmm5
	addsd	%xmm6, %xmm2
	subsd	%xmm7, %xmm2
	divsd	%xmm3, %xmm2
	movapd	%xmm4, %xmm3
	addsd	%xmm2, %xmm5
	subsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm2
	movsd	u19.6446(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	jmp	.L525
.L479:
	movapd	%xmm9, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm4, %xmm7
	addsd	24(%rsp), %xmm7
	addsd	%xmm2, %xmm7
	jmp	.L212
.L478:
	subsd	%xmm7, %xmm6
	addsd	%xmm6, %xmm2
	addsd	16(%rsp), %xmm2
	addsd	%xmm5, %xmm2
	jmp	.L209
.L477:
	subsd	%xmm12, %xmm5
	addsd	%xmm5, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm8, %xmm6
	jmp	.L206
.L476:
	subsd	%xmm6, %xmm5
	addsd	%xmm8, %xmm5
	pxor	%xmm8, %xmm8
	addsd	%xmm12, %xmm5
	addsd	%xmm8, %xmm5
	jmp	.L203
.L475:
	subsd	%xmm2, %xmm7
	addsd	%xmm6, %xmm7
	movsd	%xmm7, 16(%rsp)
	jmp	.L200
.L485:
	subsd	%xmm6, %xmm2
	addsd	%xmm4, %xmm2
	jmp	.L231
.L267:
	movapd	%xmm6, %xmm8
	pxor	%xmm15, %xmm15
	movapd	%xmm6, %xmm9
	xorpd	.LC3(%rip), %xmm8
	cmpltsd	%xmm15, %xmm9
	andpd	%xmm9, %xmm8
	andnpd	%xmm6, %xmm9
	orpd	%xmm9, %xmm8
	movsd	ua25.6453(%rip), %xmm9
	mulsd	ub25.6454(%rip), %xmm8
	mulsd	%xmm5, %xmm9
	addsd	%xmm9, %xmm8
	movapd	%xmm6, %xmm9
	addsd	%xmm8, %xmm6
	subsd	%xmm8, %xmm9
	addsd	%xmm5, %xmm6
	addsd	%xmm5, %xmm9
	ucomisd	%xmm9, %xmm6
	jp	.L265
	jne	.L265
	jmp	.L511
.L256:
	movsd	u23.6450(%rip), %xmm4
	mulsd	%xmm3, %xmm4
	jmp	.L517
.L493:
	subsd	%xmm5, %xmm4
	movapd	%xmm4, %xmm0
	addsd	%xmm8, %xmm0
	addsd	8(%rsp), %xmm0
	addsd	%xmm2, %xmm0
	jmp	.L255
.L492:
	subsd	%xmm6, %xmm0
	addsd	%xmm0, %xmm9
	addsd	%xmm12, %xmm9
	addsd	%xmm13, %xmm9
	jmp	.L252
.L491:
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm13, %xmm6
	jmp	.L249
.L490:
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm13, %xmm6
	jmp	.L246
.L489:
	subsd	%xmm0, %xmm9
	addsd	%xmm9, %xmm6
	addsd	%xmm12, %xmm6
	addsd	%xmm13, %xmm6
	jmp	.L243
.L488:
	subsd	%xmm0, %xmm10
	addsd	%xmm10, %xmm6
	addsd	%xmm13, %xmm6
	addsd	%xmm9, %xmm6
	jmp	.L240
.L487:
	subsd	%xmm6, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm5, %xmm0
	pxor	%xmm5, %xmm5
	addsd	%xmm5, %xmm0
	jmp	.L237
.L539:
	subsd	%xmm7, %xmm4
	pxor	%xmm3, %xmm3
	movapd	%xmm4, %xmm2
	subsd	%xmm9, %xmm2
	subsd	%xmm8, %xmm2
	addsd	%xmm3, %xmm2
	jmp	.L215
.L290:
	movapd	%xmm4, %xmm5
	movsd	(%rsp), %xmm15
	movapd	%xmm3, %xmm9
	divsd	%xmm3, %xmm5
	movapd	%xmm3, %xmm11
	movapd	%xmm15, %xmm2
	mulsd	%xmm5, %xmm2
	movapd	%xmm5, %xmm8
	movapd	%xmm5, %xmm10
	mulsd	%xmm5, %xmm7
	subsd	%xmm2, %xmm8
	addsd	%xmm2, %xmm8
	movapd	%xmm15, %xmm2
	mulsd	%xmm3, %xmm2
	subsd	%xmm8, %xmm10
	subsd	%xmm2, %xmm9
	addsd	%xmm9, %xmm2
	movapd	%xmm8, %xmm9
	subsd	%xmm2, %xmm11
	mulsd	%xmm2, %xmm9
	mulsd	%xmm10, %xmm2
	mulsd	%xmm11, %xmm8
	movapd	%xmm9, %xmm12
	mulsd	%xmm11, %xmm10
	addsd	%xmm2, %xmm8
	addsd	%xmm8, %xmm12
	subsd	%xmm12, %xmm4
	movapd	%xmm4, %xmm2
	movapd	%xmm9, %xmm4
	subsd	%xmm12, %xmm4
	addsd	%xmm8, %xmm4
	addsd	%xmm10, %xmm4
	subsd	%xmm4, %xmm2
	addsd	%xmm6, %xmm2
	movapd	%xmm5, %xmm6
	subsd	%xmm7, %xmm2
	divsd	%xmm3, %xmm2
	movsd	u27.6458(%rip), %xmm3
	addsd	%xmm2, %xmm6
	subsd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm3
	addsd	%xmm5, %xmm2
	movapd	%xmm2, %xmm4
	addsd	%xmm3, %xmm2
	subsd	%xmm3, %xmm4
	addsd	%xmm6, %xmm2
	addsd	%xmm6, %xmm4
	ucomisd	%xmm4, %xmm2
	jnp	.L522
	jmp	.L257
.L498:
	movapd	%xmm7, %xmm2
	subsd	%xmm6, %xmm2
	addsd	%xmm5, %xmm2
	addsd	24(%rsp), %xmm2
	addsd	%xmm8, %xmm2
	jmp	.L286
.L497:
	subsd	%xmm6, %xmm7
	addsd	%xmm7, %xmm4
	addsd	%xmm4, %xmm2
	addsd	%xmm8, %xmm2
	jmp	.L283
.L496:
	subsd	%xmm13, %xmm8
	movsd	aa3.6390(%rip), %xmm15
	addsd	%xmm10, %xmm8
	addsd	%xmm8, %xmm15
	addsd	%xmm11, %xmm15
	jmp	.L280
.L495:
	subsd	%xmm15, %xmm7
	addsd	%xmm8, %xmm7
	addsd	%xmm12, %xmm7
	movapd	%xmm7, %xmm8
	pxor	%xmm7, %xmm7
	addsd	%xmm7, %xmm8
	movsd	%xmm8, 16(%rsp)
	jmp	.L277
.L494:
	subsd	%xmm4, %xmm2
	addsd	%xmm7, %xmm2
	jmp	.L274
.L540:
	subsd	%xmm5, %xmm2
	pxor	%xmm3, %xmm3
	subsd	%xmm8, %xmm2
	subsd	%xmm7, %xmm2
	addsd	%xmm3, %xmm2
	jmp	.L289
	.size	__tan, .-__tan
	.weak	tanf32x
	.set	tanf32x,__tan
	.weak	tanf64
	.set	tanf64,__tan
	.weak	tan
	.set	tan,__tan
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	u27.6458, @object
	.size	u27.6458, 8
u27.6458:
	.long	0
	.long	986980811
	.set	u28.6459,u27.6458
	.align 8
	.type	u23.6450, @object
	.size	u23.6450, 8
u23.6450:
	.long	0
	.long	973924102
	.align 8
	.type	u24.6451, @object
	.size	u24.6451, 8
u24.6451:
	.long	0
	.long	973930333
	.align 8
	.type	u21.6448, @object
	.size	u21.6448, 8
u21.6448:
	.long	0
	.long	1010547128
	.align 8
	.type	u22.6449, @object
	.size	u22.6449, 8
u22.6449:
	.long	0
	.long	1010380296
	.align 8
	.type	u19.6446, @object
	.size	u19.6446, 8
u19.6446:
	.long	0
	.long	966867809
	.set	u20.6447,u19.6446
	.align 8
	.type	ua17.6441, @object
	.size	ua17.6441, 8
ua17.6441:
	.long	0
	.long	1006481695
	.set	ua18.6444,ua17.6441
	.align 8
	.type	u15.6438, @object
	.size	u15.6438, 8
u15.6438:
	.long	0
	.long	984970874
	.align 8
	.type	u16.6439, @object
	.size	u16.6439, 8
u16.6439:
	.long	0
	.long	984971076
	.align 8
	.type	g5.6415, @object
	.size	g5.6415, 8
g5.6415:
	.long	0
	.long	1100470148
	.align 8
	.type	u11.6434, @object
	.size	u11.6434, 8
u11.6434:
	.long	0
	.long	971311542
	.set	u12.6435,u11.6434
	.align 8
	.type	ub9.6430, @object
	.size	ub9.6430, 8
ub9.6430:
	.long	0
	.long	1020011381
	.set	ub25.6454,ub9.6430
	.set	ub17.6442,ub9.6430
	.align 8
	.type	u9.6428, @object
	.size	u9.6428, 8
u9.6428:
	.long	0
	.long	1014827719
	.set	u25.6452,u9.6428
	.set	u17.6440,u9.6428
	.align 8
	.type	ub10.6433, @object
	.size	ub10.6433, 8
ub10.6433:
	.long	0
	.long	1020027909
	.set	ub26.6457,ub10.6433
	.set	ub18.6445,ub10.6433
	.align 8
	.type	u10.6431, @object
	.size	u10.6431, 8
u10.6431:
	.long	0
	.long	1014907055
	.set	u26.6455,u10.6431
	.set	u18.6443,u10.6431
	.align 8
	.type	u7.6426, @object
	.size	u7.6426, 8
u7.6426:
	.long	0
	.long	965753835
	.align 8
	.type	u8.6427, @object
	.size	u8.6427, 8
u8.6427:
	.long	0
	.long	966547118
	.align 8
	.type	pp4.6464, @object
	.size	pp4.6464, 8
pp4.6464:
	.long	602091223
	.long	-1160940417
	.align 8
	.type	pp3.6463, @object
	.size	pp3.6463, 8
pp3.6463:
	.long	-1744830464
	.long	-1131629645
	.align 8
	.type	u5.6424, @object
	.size	u5.6424, 8
u5.6424:
	.long	0
	.long	1010423818
	.set	u13.6436,u5.6424
	.align 8
	.type	u6.6425, @object
	.size	u6.6425, 8
u6.6425:
	.long	0
	.long	1010256986
	.set	u14.6437,u6.6425
	.align 8
	.type	gy1.6416, @object
	.size	gy1.6416, 8
gy1.6416:
	.long	-1698910392
	.long	1048238066
	.align 8
	.type	mp3.6462, @object
	.size	mp3.6462, 8
mp3.6462:
	.long	-1713944590
	.long	-1131629645
	.align 8
	.type	mp2.6461, @object
	.size	mp2.6461, 8
mp2.6461:
	.long	1006632960
	.long	-1102193001
	.align 8
	.type	mp1.6460, @object
	.size	mp1.6460, 8
mp1.6460:
	.long	1476395008
	.long	1073291771
	.align 8
	.type	toint.6466, @object
	.size	toint.6466, 8
toint.6466:
	.long	0
	.long	1127743488
	.align 8
	.type	hpinv.6465, @object
	.size	hpinv.6465, 8
hpinv.6465:
	.long	1841940611
	.long	1071931184
	.align 8
	.type	g4.6414, @object
	.size	g4.6414, 8
g4.6414:
	.long	0
	.long	1077477376
	.align 8
	.type	u4.6423, @object
	.size	u4.6423, 8
u4.6423:
	.long	0
	.long	966284994
	.align 8
	.type	ub3.6422, @object
	.size	ub3.6422, 8
ub3.6422:
	.long	0
	.long	1019746456
	.align 8
	.type	ua3.6421, @object
	.size	ua3.6421, 8
ua3.6421:
	.long	0
	.long	1006472024
	.set	ua26.6456,ua3.6421
	.set	ua25.6453,ua3.6421
	.set	ua10.6432,ua3.6421
	.set	ua9.6429,ua3.6421
	.align 8
	.type	u3.6420, @object
	.size	u3.6420, 8
u3.6420:
	.long	0
	.long	1014554955
	.align 8
	.type	e1.6409, @object
	.size	e1.6409, 8
e1.6409:
	.long	-525945761
	.long	1069617426
	.align 8
	.type	e0.6408, @object
	.size	e0.6408, 8
e0.6408:
	.long	1431653821
	.long	1070945621
	.section	.rodata
	.align 32
	.type	xfg.6467, @object
	.size	xfg.6467, 5952
xfg.6467:
	.long	508665184
	.long	1068498944
	.long	-1765481920
	.long	1068500311
	.long	1652631479
	.long	1076884820
	.long	-1640670458
	.long	-1147527698
	.long	454717456
	.long	1068564480
	.long	-1430744393
	.long	1068566120
	.long	-1103110284
	.long	1076761287
	.long	58566465
	.long	1000903220
	.long	621142864
	.long	1068630016
	.long	-11241436
	.long	1068631963
	.long	-561695181
	.long	1076651461
	.long	-379502000
	.long	1001867374
	.long	-52673966
	.long	1068695551
	.long	1588685928
	.long	1068697843
	.long	-445913265
	.long	1076553178
	.long	-1851641804
	.long	-1149057242
	.long	-469793633
	.long	1068761087
	.long	357145036
	.long	1068763761
	.long	74150738
	.long	1076464707
	.long	943944591
	.long	1000001199
	.long	-305571569
	.long	1068826623
	.long	1117511040
	.long	1068829719
	.long	-1850354060
	.long	1076384644
	.long	-1270858462
	.long	-1144926163
	.long	15631351
	.long	1068892160
	.long	-629093343
	.long	1068895719
	.long	-283008345
	.long	1076311844
	.long	1917992957
	.long	-1147804356
	.long	-351933887
	.long	1068957695
	.long	-1195041814
	.long	1068961764
	.long	-85630209
	.long	1076245360
	.long	-1879177690
	.long	-1147556594
	.long	-603646473
	.long	1069023231
	.long	498418840
	.long	1069027856
	.long	243182428
	.long	1076184403
	.long	-701573498
	.long	-1148105593
	.long	-477833301
	.long	1069088767
	.long	713897767
	.long	1069093996
	.long	-44567555
	.long	1076128307
	.long	1401020525
	.long	-1145316723
	.long	-461167801
	.long	1069154303
	.long	-720498608
	.long	1069160186
	.long	-984384874
	.long	1076076514
	.long	-1055790405
	.long	1002960533
	.long	658850019
	.long	1069219840
	.long	2061927703
	.long	1069226430
	.long	909633862
	.long	1076028545
	.long	-1916953197
	.long	1002637304
	.long	-21025125
	.long	1069285375
	.long	-2090218215
	.long	1069292728
	.long	397341219
	.long	1075983990
	.long	1112186099
	.long	1002766392
	.long	592613445
	.long	1069350912
	.long	-1055031561
	.long	1069359083
	.long	-1193001650
	.long	1075942495
	.long	1386672676
	.long	998318087
	.long	-663574487
	.long	1069416447
	.long	-1897605449
	.long	1069425497
	.long	1981373791
	.long	1075903756
	.long	-2046965115
	.long	-1145775701
	.long	-13485180
	.long	1069481983
	.long	-326077467
	.long	1069491972
	.long	807411727
	.long	1075867505
	.long	310602130
	.long	1001880226
	.long	216502677
	.long	1069547520
	.long	-1137652639
	.long	1069553015
	.long	-698987896
	.long	1075828041
	.long	-1772352816
	.long	-1144483217
	.long	-297735912
	.long	1069580287
	.long	-2105955327
	.long	1069586317
	.long	-1835746301
	.long	1075764149
	.long	1252935126
	.long	-1149197005
	.long	213946877
	.long	1069613056
	.long	1155511031
	.long	1069619653
	.long	108891672
	.long	1075703995
	.long	1341538980
	.long	-1144247272
	.long	228144801
	.long	1069645824
	.long	-1190634933
	.long	1069653023
	.long	1909339689
	.long	1075647258
	.long	107558235
	.long	-1144822445
	.long	-312528504
	.long	1069678591
	.long	-294338771
	.long	1069686429
	.long	-15935915
	.long	1075593654
	.long	1200541637
	.long	998795253
	.long	99641937
	.long	1069711360
	.long	1426796863
	.long	1069719873
	.long	386066130
	.long	1075542930
	.long	939602430
	.long	-1146246319
	.long	-29543522
	.long	1069744127
	.long	-1486915873
	.long	1069753354
	.long	-55164771
	.long	1075494856
	.long	220798339
	.long	1002834895
	.long	-309818001
	.long	1069776895
	.long	332160439
	.long	1069786875
	.long	-1898630643
	.long	1075449231
	.long	1324286891
	.long	1001956494
	.long	46793997
	.long	1069809664
	.long	-497584453
	.long	1069820435
	.long	-91962692
	.long	1075405869
	.long	-866758289
	.long	-1143947939
	.long	-229433508
	.long	1069842431
	.long	-558418940
	.long	1069854037
	.long	-126971851
	.long	1075364606
	.long	1520601032
	.long	1000144871
	.long	326584722
	.long	1069875200
	.long	2098127623
	.long	1069887682
	.long	923578165
	.long	1075325292
	.long	-115514062
	.long	-1144031791
	.long	-304231635
	.long	1069907967
	.long	1572091005
	.long	1069921370
	.long	1377373198
	.long	1075287790
	.long	399888502
	.long	998810207
	.long	-199669344
	.long	1069940735
	.long	331323498
	.long	1069955103
	.long	757224957
	.long	1075251977
	.long	-1313024537
	.long	-1147405533
	.long	-130755679
	.long	1069973503
	.long	-1899472558
	.long	1069988881
	.long	1657741928
	.long	1075217740
	.long	-2099012092
	.long	1000269115
	.long	-40827158
	.long	1070006271
	.long	-224856329
	.long	1070022706
	.long	525305001
	.long	1075184977
	.long	287716536
	.long	-1145725878
	.long	258251343
	.long	1070039040
	.long	1822767804
	.long	1070056580
	.long	1208602997
	.long	1075153593
	.long	1943260462
	.long	1003035765
	.long	-284771368
	.long	1070071807
	.long	-545425747
	.long	1070090502
	.long	-584372058
	.long	1075123502
	.long	139214575
	.long	-1145068838
	.long	35414836
	.long	1070104576
	.long	-647628410
	.long	1070124475
	.long	1370964630
	.long	1075094626
	.long	1960640011
	.long	-1147224018
	.long	-208861354
	.long	1070137343
	.long	685863879
	.long	1070158500
	.long	397778621
	.long	1075066891
	.long	-1217033744
	.long	-1144189156
	.long	273611937
	.long	1070170112
	.long	1178915388
	.long	1070192577
	.long	-887182379
	.long	1075040229
	.long	-367489634
	.long	-1143613705
	.long	155286320
	.long	1070202880
	.long	158168746
	.long	1070226708
	.long	-1284358736
	.long	1075014580
	.long	-67922020
	.long	1002323469
	.long	-350477332
	.long	1070235647
	.long	-1419761523
	.long	1070260893
	.long	2016440791
	.long	1074989886
	.long	1028883647
	.long	-1144529534
	.long	-82371565
	.long	1070268415
	.long	-1574852245
	.long	1070295135
	.long	-658475907
	.long	1074966093
	.long	-715830926
	.long	1003775051
	.long	179979454
	.long	1070301184
	.long	-320920545
	.long	1070329434
	.long	-956639685
	.long	1074943153
	.long	-795690918
	.long	1000610070
	.long	289221182
	.long	1070333952
	.long	-1276566205
	.long	1070363792
	.long	-1704804299
	.long	1074921020
	.long	1101018937
	.long	-1146893255
	.long	-190971698
	.long	1070366719
	.long	257551024
	.long	1070398210
	.long	-380197327
	.long	1074899651
	.long	-1079456933
	.long	-1145592957
	.long	100012459
	.long	1070399488
	.long	-1981381554
	.long	1070432688
	.long	-208591219
	.long	1074879007
	.long	-1294167565
	.long	1003723612
	.long	343297259
	.long	1070432256
	.long	668909325
	.long	1070467229
	.long	-70760640
	.long	1074859051
	.long	-2072959728
	.long	1002523981
	.long	347925110
	.long	1070465024
	.long	383502368
	.long	1070501833
	.long	-1609784178
	.long	1074839749
	.long	1678711710
	.long	-1143785592
	.long	-42363459
	.long	1070497791
	.long	-2001402844
	.long	1070536501
	.long	-1107159398
	.long	1074821068
	.long	572514167
	.long	-1145692178
	.long	214127670
	.long	1070530560
	.long	-48126965
	.long	1070571235
	.long	518520808
	.long	1074802979
	.long	-945818175
	.long	1003759587
	.long	30530099
	.long	1070563328
	.long	-1210326692
	.long	1070601066
	.long	2010470885
	.long	1074780505
	.long	-696694569
	.long	1001440929
	.long	101454770
	.long	1070596096
	.long	-1431482600
	.long	1070618501
	.long	645255554
	.long	1074746526
	.long	418438416
	.long	-1144958456
	.long	59266194
	.long	1070612480
	.long	1711620823
	.long	1070635971
	.long	-549494804
	.long	1074713570
	.long	-1199329694
	.long	-1143384489
	.long	86989331
	.long	1070628864
	.long	-1709617083
	.long	1070653476
	.long	-93785400
	.long	1074681592
	.long	1515470478
	.long	1004459962
	.long	45751029
	.long	1070645248
	.long	-488115637
	.long	1070671017
	.long	-1073847489
	.long	1074650548
	.long	207628175
	.long	-1143162234
	.long	-144293183
	.long	1070661631
	.long	-513605658
	.long	1070688595
	.long	51500501
	.long	1074620397
	.long	-468951138
	.long	997924682
	.long	53871344
	.long	1070678016
	.long	1598640270
	.long	1070706211
	.long	-1200586138
	.long	1074591098
	.long	-2044660991
	.long	-1143560510
	.long	-16967149
	.long	1070694399
	.long	-617964639
	.long	1070723864
	.long	-1928265570
	.long	1074562617
	.long	1773280571
	.long	1003727302
	.long	-149408163
	.long	1070710783
	.long	205644716
	.long	1070741557
	.long	-533994372
	.long	1074534918
	.long	312380625
	.long	1004246554
	.long	-16769536
	.long	1070727167
	.long	-1292486306
	.long	1070759288
	.long	-218195323
	.long	1074507969
	.long	-426780712
	.long	1004565673
	.long	-29205910
	.long	1070743551
	.long	1636805310
	.long	1070777060
	.long	420157663
	.long	1074481740
	.long	-166352175
	.long	-1143010017
	.long	94290395
	.long	1070759936
	.long	-665307004
	.long	1070794872
	.long	215493570
	.long	1074456200
	.long	337387405
	.long	-1143643197
	.long	905345
	.long	1070776320
	.long	-1342327080
	.long	1070812726
	.long	1383136277
	.long	1074431322
	.long	-1624450307
	.long	-1144596339
	.long	-65155797
	.long	1070792703
	.long	-1453820484
	.long	1070830622
	.long	-1433075018
	.long	1074407080
	.long	-2088128164
	.long	-1142866771
	.long	-155134816
	.long	1070809087
	.long	1939359221
	.long	1070848561
	.long	1294300064
	.long	1074383450
	.long	-1397824647
	.long	-1144580554
	.long	112289683
	.long	1070825472
	.long	-608139024
	.long	1070866543
	.long	-1824201926
	.long	1074360407
	.long	494555009
	.long	-1144864686
	.long	120950691
	.long	1070841856
	.long	1864577309
	.long	1070884570
	.long	1520833258
	.long	1074337930
	.long	1751027420
	.long	998658981
	.long	-63235889
	.long	1070858239
	.long	-383242645
	.long	1070902641
	.long	1679713341
	.long	1074315997
	.long	1062632548
	.long	999682287
	.long	905896
	.long	1070874624
	.long	530838225
	.long	1070920759
	.long	1618147361
	.long	1074294588
	.long	-1778423627
	.long	1001819898
	.long	-72075501
	.long	1070891007
	.long	-1280560324
	.long	1070938922
	.long	1034249874
	.long	1074273684
	.long	696646975
	.long	-1145090689
	.long	-82756620
	.long	1070907391
	.long	1854855618
	.long	1070957133
	.long	-1465004017
	.long	1074253266
	.long	479934039
	.long	1003440010
	.long	-135231543
	.long	1070923775
	.long	121922710
	.long	1070975392
	.long	1145407723
	.long	1074233318
	.long	-1528120564
	.long	-1144948772
	.long	143341588
	.long	1070940160
	.long	1447110137
	.long	1070993699
	.long	1652664165
	.long	1074213822
	.long	-177188412
	.long	-1144863444
	.long	-137012890
	.long	1070956543
	.long	-510351915
	.long	1071012055
	.long	1941208351
	.long	1074194763
	.long	-107385402
	.long	1004154831
	.long	146744464
	.long	1070972928
	.long	-1217023347
	.long	1071030462
	.long	364309544
	.long	1074176126
	.long	-1437432392
	.long	-1142895984
	.long	167413055
	.long	1070989312
	.long	1705405693
	.long	1071048920
	.long	477106793
	.long	1074157896
	.long	-2054037190
	.long	1004137109
	.long	123297738
	.long	1071005696
	.long	-1068144004
	.long	1071067429
	.long	-1162961662
	.long	1074140059
	.long	1075129219
	.long	1004047103
	.long	51808218
	.long	1071022080
	.long	-1830629424
	.long	1071085991
	.long	-891254741
	.long	1074122603
	.long	-1622961071
	.long	-1143429982
	.long	-171388958
	.long	1071038463
	.long	-1615572243
	.long	1071104606
	.long	-983436851
	.long	1074105515
	.long	-157057676
	.long	-1142722138
	.long	-132255183
	.long	1071054847
	.long	-810231115
	.long	1071123275
	.long	-1925405229
	.long	1074088783
	.long	1738394015
	.long	1003177606
	.long	-93243298
	.long	1071071231
	.long	-536368714
	.long	1071141999
	.long	-1280171658
	.long	1074072395
	.long	-1513671561
	.long	-1144187584
	.long	38444075
	.long	1071087616
	.long	-1476594225
	.long	1071160779
	.long	827968481
	.long	1074056341
	.long	-712348976
	.long	-1143200278
	.long	48980664
	.long	1071104000
	.long	-334780977
	.long	1071179615
	.long	-2026770710
	.long	1074040609
	.long	-870655237
	.long	-1144927373
	.long	88756945
	.long	1071120384
	.long	-1950866711
	.long	1071198509
	.long	-1854888151
	.long	1074025190
	.long	-356515090
	.long	-1143424233
	.long	-113639261
	.long	1071136767
	.long	1270459690
	.long	1071217461
	.long	-1366143361
	.long	1074010074
	.long	-1707635513
	.long	1003796480
	.long	-69430847
	.long	1071153151
	.long	661763585
	.long	1071236472
	.long	2022004633
	.long	1073995252
	.long	-1610332711
	.long	1004078973
	.long	-154003994
	.long	1071169535
	.long	-521516533
	.long	1071255542
	.long	412748169
	.long	1073980715
	.long	-1983880899
	.long	-1143314383
	.long	49755375
	.long	1071185920
	.long	1936455033
	.long	1071274674
	.long	-383497050
	.long	1073966453
	.long	1265771383
	.long	1004775128
	.long	82770100
	.long	1071202304
	.long	-1615404080
	.long	1071293867
	.long	-922182473
	.long	1073952460
	.long	583794875
	.long	1000302947
	.long	69125994
	.long	1071218688
	.long	1366738916
	.long	1071313123
	.long	-888433402
	.long	1073938727
	.long	1401268297
	.long	1001521115
	.long	134499911
	.long	1071235072
	.long	1996952100
	.long	1071332442
	.long	1356291783
	.long	1073925247
	.long	2028993674
	.long	-1146029346
	.long	33039195
	.long	1071251456
	.long	-416974802
	.long	1071351825
	.long	419167031
	.long	1073912012
	.long	995157787
	.long	-1142595885
	.long	-74682616
	.long	1071267839
	.long	-1750038797
	.long	1071371274
	.long	357425227
	.long	1073899015
	.long	1163897571
	.long	-1143028130
	.long	-79862815
	.long	1071284223
	.long	2107259786
	.long	1071390789
	.long	2120269755
	.long	1073886249
	.long	357481779
	.long	1004732444
	.long	-168534968
	.long	1071300607
	.long	2072856038
	.long	1071410371
	.long	-619000215
	.long	1073873708
	.long	1054029334
	.long	-1142559752
	.long	84456268
	.long	1071316992
	.long	-1568764617
	.long	1071430021
	.long	-733636053
	.long	1073861386
	.long	-1369316655
	.long	-1143991246
	.long	176451852
	.long	1071333376
	.long	-1008402091
	.long	1071449740
	.long	2001745117
	.long	1073849277
	.long	1491132968
	.long	-1142420706
	.long	-158144040
	.long	1071349759
	.long	-996317657
	.long	1071469529
	.long	-31979926
	.long	1073837374
	.long	-144649410
	.long	-1146092344
	.long	42004813
	.long	1071366144
	.long	-454553581
	.long	1071489389
	.long	-1550933934
	.long	1073825673
	.long	667534570
	.long	1005078716
	.long	1122769
	.long	1071382528
	.long	-356509456
	.long	1071509321
	.long	548978188
	.long	1073814168
	.long	-791281394
	.long	-1142861186
	.long	-17407318
	.long	1071398911
	.long	-368283536
	.long	1071529326
	.long	854879437
	.long	1073802853
	.long	412745741
	.long	1005113921
	.long	159668248
	.long	1071415296
	.long	-211121381
	.long	1071549405
	.long	-892914570
	.long	1073791723
	.long	314495620
	.long	1003626999
	.long	-83586113
	.long	1071431679
	.long	-523891440
	.long	1071569559
	.long	622622774
	.long	1073780775
	.long	1860977079
	.long	-1142899372
	.long	-44919265
	.long	1071448063
	.long	-273423285
	.long	1071589789
	.long	1961122293
	.long	1073770002
	.long	-378943441
	.long	-1143390345
	.long	-31254108
	.long	1071464447
	.long	387159184
	.long	1071610097
	.long	1047756436
	.long	1073759401
	.long	1230588794
	.long	-1148065844
	.long	-95558250
	.long	1071480831
	.long	1670334952
	.long	1071630482
	.long	588273573
	.long	1073748967
	.long	-173039777
	.long	1003782832
	.long	-26043799
	.long	1071497215
	.long	2087366681
	.long	1071647809
	.long	-1084152224
	.long	1073735567
	.long	14313502
	.long	1005214938
	.long	165046274
	.long	1071513600
	.long	-155679793
	.long	1071658081
	.long	-1140479876
	.long	1073715342
	.long	-231266711
	.long	-1142920141
	.long	-81717673
	.long	1071529983
	.long	421378280
	.long	1071668395
	.long	2077222590
	.long	1073695427
	.long	1591268187
	.long	-1143614177
	.long	171902602
	.long	1071546368
	.long	-1778690832
	.long	1071678749
	.long	340567048
	.long	1073675814
	.long	-1564382779
	.long	-1144534892
	.long	28160096
	.long	1071562752
	.long	-595897095
	.long	1071689145
	.long	1589531303
	.long	1073656495
	.long	824991741
	.long	1003808576
	.long	-10744575
	.long	1071579135
	.long	-1839920499
	.long	1071699584
	.long	225574384
	.long	1073637464
	.long	-1766632295
	.long	1004394662
	.long	-113418292
	.long	1071595519
	.long	1170948980
	.long	1071710066
	.long	1020153920
	.long	1073618713
	.long	2082405044
	.long	-1144393492
	.long	-99757681
	.long	1071611903
	.long	-1806838520
	.long	1071720591
	.long	697009997
	.long	1073600236
	.long	-1774018394
	.long	1005219517
	.long	-46489830
	.long	1071628287
	.long	326438718
	.long	1071731161
	.long	1476829822
	.long	1073582026
	.long	-60445574
	.long	1004153347
	.long	-62385152
	.long	1071644671
	.long	1502168266
	.long	1071741775
	.long	-2108481168
	.long	1073564077
	.long	1025732035
	.long	1002717486
	.long	-11306900
	.long	1071652863
	.long	131611975
	.long	1071752435
	.long	-2061762581
	.long	1073546383
	.long	704125716
	.long	1003575453
	.long	-18677321
	.long	1071661055
	.long	-1197038718
	.long	1071763140
	.long	2085723574
	.long	1073528938
	.long	1529140914
	.long	1003712742
	.long	7222975
	.long	1071669248
	.long	290371774
	.long	1071773893
	.long	-1555208314
	.long	1073511736
	.long	1268302292
	.long	-1142254882
	.long	-43996605
	.long	1071677439
	.long	-1410543925
	.long	1071784692
	.long	1999907524
	.long	1073494772
	.long	61194946
	.long	-1143126026
	.long	58155752
	.long	1071685632
	.long	1067053032
	.long	1071795540
	.long	1979010987
	.long	1073478040
	.long	360127046
	.long	1005451252
	.long	-14915295
	.long	1071693823
	.long	1502492112
	.long	1071806436
	.long	-1965174394
	.long	1073461535
	.long	1577998963
	.long	1004597612
	.long	-16313013
	.long	1071702015
	.long	-1229178490
	.long	1071817381
	.long	-2097060755
	.long	1073445252
	.long	-1146465520
	.long	-1141887536
	.long	50126244
	.long	1071710208
	.long	44408606
	.long	1071828377
	.long	1884428735
	.long	1073429186
	.long	288335382
	.long	-1142582441
	.long	30282048
	.long	1071718400
	.long	-552966206
	.long	1071839422
	.long	-1699172822
	.long	1073413332
	.long	-1828457658
	.long	1004717378
	.long	80802645
	.long	1071726592
	.long	150783935
	.long	1071850520
	.long	1232837174
	.long	1073397686
	.long	-1247440332
	.long	1005305924
	.long	16452746
	.long	1071734784
	.long	611410315
	.long	1071861669
	.long	220647703
	.long	1073382243
	.long	1065072585
	.long	-1143211086
	.long	40833203
	.long	1071742976
	.long	-145256362
	.long	1071872870
	.long	1800205489
	.long	1073366998
	.long	-1358209559
	.long	1005647540
	.long	-49710856
	.long	1071751167
	.long	688703372
	.long	1071884126
	.long	918688533
	.long	1073351948
	.long	-1567182238
	.long	1005265193
	.long	-40598517
	.long	1071759359
	.long	-2051742556
	.long	1071895435
	.long	854781167
	.long	1073337088
	.long	-1178894988
	.long	-1143231718
	.long	-69591391
	.long	1071767551
	.long	-1086525905
	.long	1071906799
	.long	1735458520
	.long	1073322414
	.long	-1263736412
	.long	1004145628
	.long	-57109197
	.long	1071775743
	.long	-1675128820
	.long	1071918219
	.long	-522587732
	.long	1073307922
	.long	799201306
	.long	-1142023701
	.long	19631398
	.long	1071783936
	.long	-518425142
	.long	1071929695
	.long	-844476800
	.long	1073293609
	.long	-1396167349
	.long	1005728642
	.long	83154607
	.long	1071792128
	.long	1294090545
	.long	1071941229
	.long	-2124663576
	.long	1073279471
	.long	997460633
	.long	-1143945473
	.long	-54494246
	.long	1071800319
	.long	-1732784112
	.long	1071952820
	.long	1944584097
	.long	1073265504
	.long	450341314
	.long	1005306651
	.long	70096441
	.long	1071808512
	.long	-1236975407
	.long	1071964470
	.long	-197703368
	.long	1073251704
	.long	461760769
	.long	1004760553
	.long	62794144
	.long	1071816704
	.long	1388314006
	.long	1071976180
	.long	-1073468410
	.long	1073238069
	.long	1995829181
	.long	1004625153
	.long	-89579758
	.long	1071824895
	.long	1043287565
	.long	1071987950
	.long	-2059215097
	.long	1073224595
	.long	392571435
	.long	1004885453
	.long	-24622959
	.long	1071833087
	.long	1813652592
	.long	1071999781
	.long	-467473808
	.long	1073211278
	.long	-1123873775
	.long	1005597956
	.long	-24701653
	.long	1071841279
	.long	-1679119711
	.long	1072011674
	.long	-779097677
	.long	1073198116
	.long	2000555620
	.long	-1151034785
	.long	-4085983
	.long	1071849471
	.long	-1344215708
	.long	1072023630
	.long	864925908
	.long	1073185106
	.long	1571350837
	.long	1005253672
	.long	-9395342
	.long	1071857663
	.long	-2112283292
	.long	1072035650
	.long	287489198
	.long	1073172244
	.long	-555434065
	.long	1004215371
	.long	70707254
	.long	1071865856
	.long	-35205889
	.long	1072047734
	.long	1984177452
	.long	1073159527
	.long	-527680773
	.long	-1142145776
	.long	-54618559
	.long	1071874047
	.long	-291504721
	.long	1072059884
	.long	-1493433067
	.long	1073146953
	.long	785552743
	.long	1004381267
	.long	-4794678
	.long	1071882239
	.long	1583849953
	.long	1072072101
	.long	-1138038423
	.long	1073134519
	.long	787757186
	.long	1005371259
	.long	76763116
	.long	1071890432
	.long	750812533
	.long	1072084385
	.long	279981816
	.long	1073122223
	.long	-21509443
	.long	-1142437637
	.long	66441087
	.long	1071898624
	.long	1046077174
	.long	1072096737
	.long	241258511
	.long	1073110061
	.long	56588357
	.long	-1142748252
	.long	86510297
	.long	1071906816
	.long	-1851044802
	.long	1072109158
	.long	435166352
	.long	1073098031
	.long	-1482902
	.long	-1141773835
	.long	77527909
	.long	1071915008
	.long	414137245
	.long	1072121650
	.long	-1201379868
	.long	1073086130
	.long	671963422
	.long	1002752727
	.long	3525814
	.long	1071923200
	.long	-883953179
	.long	1072134212
	.long	2084918312
	.long	1073074357
	.long	2074435964
	.long	1005423640
	.long	53765911
	.long	1071931392
	.long	-1171945608
	.long	1072146847
	.long	-197582252
	.long	1073062708
	.long	-58415760
	.long	-1141911222
	.long	50341237
	.long	1071939584
	.long	-667519345
	.long	1072159555
	.long	-587629044
	.long	1073051182
	.long	1599992990
	.long	-1143112006
	.long	89912193
	.long	1071947776
	.long	904785658
	.long	1072172338
	.long	-364453089
	.long	1073039776
	.long	1988247212
	.long	-1143233246
	.long	80613603
	.long	1071955968
	.long	-696401496
	.long	1072185195
	.long	-310199100
	.long	1073028488
	.long	125643242
	.long	-1142232787
	.long	23879905
	.long	1071964160
	.long	-905269455
	.long	1072198129
	.long	-1112752362
	.long	1073017316
	.long	1190019678
	.long	1005070680
	.long	-6918390
	.long	1071972351
	.long	738143662
	.long	1072211141
	.long	957834622
	.long	1073006258
	.long	1552336645
	.long	-1141823550
	.long	7792363
	.long	1071980544
	.long	391533032
	.long	1072224231
	.long	1329961733
	.long	1072995311
	.long	1099021308
	.long	-1145968175
	.long	64040251
	.long	1071988736
	.long	-1451823786
	.long	1072237400
	.long	-34452748
	.long	1072984473
	.long	-1022497617
	.long	1004768279
	.long	-51905907
	.long	1071996927
	.long	-264619511
	.long	1072250650
	.long	1595089739
	.long	1072973744
	.long	1893695008
	.long	1005697202
	.long	53589098
	.long	1072005120
	.long	954741044
	.long	1072263983
	.long	1759032347
	.long	1072963120
	.long	-1190411637
	.long	-1142020904
	.long	-65764210
	.long	1072013311
	.long	-2072922999
	.long	1072277398
	.long	1575201395
	.long	1072952600
	.long	-529132234
	.long	-1142117159
	.long	-75705451
	.long	1072021503
	.long	615037269
	.long	1072290898
	.long	1334837013
	.long	1072942182
	.long	-2113645039
	.long	-1141474153
	.long	904256
	.long	1072029696
	.long	1307949374
	.long	1072304483
	.long	1971663400
	.long	1072931864
	.long	-729681072
	.long	-1141537318
	.long	49271548
	.long	1072037888
	.long	819028771
	.long	1072318155
	.long	410608302
	.long	1072921645
	.long	-1510507600
	.long	1005585603
	.long	-21493622
	.long	1072046079
	.long	87630016
	.long	1072331915
	.long	-1994954327
	.long	1072911522
	.long	1072574505
	.long	1002203952
	.long	15976966
	.long	1072054272
	.long	681677082
	.long	1072345764
	.long	175049290
	.long	1072901495
	.long	-87750630
	.long	1005758200
	.long	-23149836
	.long	1072062463
	.long	-724667847
	.long	1072359703
	.long	122792264
	.long	1072891561
	.long	1370201533
	.long	-1142337355
	.long	64239974
	.long	1072070656
	.long	1888197002
	.long	1072373735
	.long	-677293113
	.long	1072881718
	.long	-1578852018
	.long	1004353250
	.long	-88705149
	.long	1072078847
	.long	777475172
	.long	1072387860
	.long	83913910
	.long	1072871967
	.long	1195126321
	.long	1005230666
	.long	-55156855
	.long	1072087039
	.long	-1742799569
	.long	1072402079
	.long	-381101577
	.long	1072862303
	.long	-402464910
	.long	-1144368492
	.long	-61470295
	.long	1072095231
	.long	-85102607
	.long	1072416394
	.long	355680542
	.long	1072852728
	.long	-182729370
	.long	-1141337773
	.long	-61935738
	.long	1072103423
	.long	-970848770
	.long	1072430807
	.long	242259852
	.long	1072843238
	.long	-1564868778
	.long	1002672161
	.long	5085363
	.long	1072111616
	.long	1902733485
	.long	1072445319
	.long	1639872337
	.long	1072833832
	.long	1961763345
	.long	1005570501
	.long	-65712011
	.long	1072119807
	.long	1590099278
	.long	1072459931
	.long	-1244318959
	.long	1072824509
	.long	-1570132381
	.long	1004702285
	.long	77120003
	.long	1072128000
	.long	842860737
	.long	1072474645
	.long	-1824309403
	.long	1072815268
	.long	346197943
	.long	1006160612
	.long	-14789066
	.long	1072136191
	.long	1095221548
	.long	1072489462
	.long	-1075669848
	.long	1072806107
	.long	1263970116
	.long	-1142431332
	.long	36099162
	.long	1072144384
	.long	1092676479
	.long	1072504384
	.long	-782269832
	.long	1072797025
	.long	53239116
	.long	1004948264
	.long	38839722
	.long	1072152576
	.long	-1324178975
	.long	1072519412
	.long	-1973824155
	.long	1072788021
	.long	-1193510809
	.long	-1142483234
	.long	75692078
	.long	1072160768
	.long	896974320
	.long	1072534549
	.long	-1574298533
	.long	1072779093
	.long	-516925948
	.long	1005717473
	.long	33882174
	.long	1072168960
	.long	1691787255
	.long	1072549795
	.long	-478236049
	.long	1072770240
	.long	-484207112
	.long	-1143656762
	.long	-67529711
	.long	1072177151
	.long	-335966383
	.long	1072565152
	.long	383636950
	.long	1072761462
	.long	1501978693
	.long	-1142103850
	.long	38331299
	.long	1072185344
	.long	-1682442907
	.long	1072580623
	.long	-75177892
	.long	1072752755
	.long	362143634
	.long	1005905384
	.long	21348695
	.long	1072193536
	.long	168866731
	.long	1072596209
	.long	2092792606
	.long	1072744121
	.long	-705617456
	.long	-1142841882
	.long	36057529
	.long	1072201728
	.long	199356487
	.long	1072611911
	.long	1829453542
	.long	1072735557
	.long	-1548080017
	.long	-1144884559
	.long	73365544
	.long	1072209920
	.long	1816508956
	.long	1072627731
	.long	-1356198135
	.long	1072727062
	.long	1533265817
	.long	1005071190
	.long	-3766577
	.long	1072218111
	.long	-264469941
	.long	1072643671
	.long	860889632
	.long	1072718636
	.long	767486757
	.long	-1143981651
	.long	-41532218
	.long	1072226303
	.long	-2022431203
	.long	1072659734
	.long	-581226471
	.long	1072710276
	.long	-504649421
	.long	1003986739
	.long	-28411593
	.long	1072234495
	.long	446277185
	.long	1072675921
	.long	-1620474869
	.long	1072701983
	.long	2079252504
	.long	-1142251900
	.long	70591775
	.long	1072242688
	.long	-1632492656
	.long	1072692233
	.long	1870418408
	.long	1072693755
	.long	822291978
	.long	-1151724825
	.section	.rodata.cst8
	.align 8
	.type	mfftnhf.6410, @object
	.size	mfftnhf.6410, 8
mfftnhf.6410:
	.long	0
	.long	-1070661632
	.align 8
	.type	g3.6413, @object
	.size	g3.6413, 8
g3.6413:
	.long	0
	.long	1072246554
	.align 8
	.type	u2.6419, @object
	.size	u2.6419, 8
u2.6419:
	.long	0
	.long	964942925
	.align 8
	.type	aa3.6390, @object
	.size	aa3.6390, 8
aa3.6390:
	.long	1431655765
	.long	1014322517
	.align 8
	.type	aa5.6392, @object
	.size	aa5.6392, 8
aa5.6392:
	.long	286331153
	.long	1010897169
	.align 8
	.type	a5.6391, @object
	.size	a5.6391, 8
a5.6391:
	.long	286331153
	.long	1069617425
	.align 8
	.type	aa7.6394, @object
	.size	aa7.6394, 8
aa7.6394:
	.long	395409688
	.long	-1136160391
	.align 8
	.type	a7.6393, @object
	.size	a7.6393, 8
a7.6393:
	.long	463583772
	.long	1068212666
	.align 8
	.type	aa9.6396, @object
	.size	aa9.6396, 8
aa9.6396:
	.long	-1955838723
	.long	-1140218584
	.align 8
	.type	a9.6395, @object
	.size	a9.6395, 8
a9.6395:
	.long	-2010377990
	.long	1066820852
	.align 8
	.type	aa11.6398, @object
	.size	aa11.6398, 8
aa11.6398:
	.long	-1894110189
	.long	-1137956565
	.align 8
	.type	a11.6397, @object
	.size	a11.6397, 8
a11.6397:
	.long	1441186365
	.long	1065494243
	.align 8
	.type	aa13.6400, @object
	.size	aa13.6400, 8
aa13.6400:
	.long	-915875471
	.long	-1140686641
	.align 8
	.type	a13.6399, @object
	.size	a13.6399, 8
a13.6399:
	.long	236289504
	.long	1064135997
	.align 8
	.type	a15.6401, @object
	.size	a15.6401, 8
a15.6401:
	.long	1160476131
	.long	1062722102
	.align 8
	.type	a17.6402, @object
	.size	a17.6402, 8
a17.6402:
	.long	1208182596
	.long	1061377410
	.align 8
	.type	a19.6403, @object
	.size	a19.6403, 8
a19.6403:
	.long	1934431844
	.long	1060067287
	.align 8
	.type	a21.6404, @object
	.size	a21.6404, 8
a21.6404:
	.long	-1963151443
	.long	1058629601
	.align 8
	.type	a23.6405, @object
	.size	a23.6405, 8
a23.6405:
	.long	-291351975
	.long	1057265624
	.align 8
	.type	a27.6407, @object
	.size	a27.6407, 8
a27.6407:
	.long	-739367703
	.long	1054543730
	.align 8
	.type	a25.6406, @object
	.size	a25.6406, 8
a25.6406:
	.long	-744857520
	.long	1055961394
	.align 8
	.type	u1.6418, @object
	.size	u1.6418, 8
u1.6418:
	.long	0
	.long	1019790138
	.align 8
	.type	d3.6384, @object
	.size	d3.6384, 8
d3.6384:
	.long	1431655765
	.long	1070945621
	.set	a3.6389,d3.6384
	.align 8
	.type	d5.6385, @object
	.size	d5.6385, 8
d5.6385:
	.long	286328774
	.long	1069617425
	.align 8
	.type	d7.6386, @object
	.size	d7.6386, 8
d7.6386:
	.long	484149061
	.long	1068212666
	.align 8
	.type	d11.6388, @object
	.size	d11.6388, 8
d11.6388:
	.long	1022551274
	.long	1065498714
	.align 8
	.type	d9.6387, @object
	.size	d9.6387, 8
d9.6387:
	.long	1238353510
	.long	1066820845
	.align 8
	.type	g2.6412, @object
	.size	g2.6412, 8
g2.6412:
	.long	0
	.long	1068441901
	.set	gy2.6417,g2.6412
	.align 8
	.type	g1.6411, @object
	.size	g1.6411, 8
g1.6411:
	.long	0
	.long	1045105004
	.align 8
.LC0:
	.long	0
	.long	-1074790400
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC4:
	.long	0
	.long	1048576
	.align 8
.LC5:
	.long	33554432
	.long	1101004800
	.section	.rodata.cst16
	.align 16
.LC6:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC7:
	.long	0
	.long	1081081856
