	.text
	.p2align 4,,15
	.globl	__GI___fpclassifyf128
	.hidden	__GI___fpclassifyf128
	.type	__GI___fpclassifyf128, @function
__GI___fpclassifyf128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rcx
	movq	-24(%rsp), %rax
	movabsq	$281474976710655, %rdx
	movabsq	$9223090561878065152, %rsi
	andq	%rcx, %rdx
	andq	%rsi, %rcx
	orq	%rax, %rdx
	movl	$2, %eax
	movq	%rdx, %rdi
	orq	%rcx, %rdi
	je	.L1
	testq	%rcx, %rcx
	movl	$3, %eax
	je	.L1
	cmpq	%rsi, %rcx
	movl	$4, %eax
	jne	.L1
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%al
.L1:
	rep ret
	.size	__GI___fpclassifyf128, .-__GI___fpclassifyf128
	.globl	__fpclassifyf128
	.set	__fpclassifyf128,__GI___fpclassifyf128
