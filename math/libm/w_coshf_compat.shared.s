	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__coshf
	.type	__coshf, @function
__coshf:
	subq	$24, %rsp
	movss	%xmm0, 12(%rsp)
	call	__ieee754_coshf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm3
	andps	%xmm1, %xmm4
	movss	12(%rsp), %xmm2
	ucomiss	%xmm4, %xmm3
	jb	.L7
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	andps	%xmm2, %xmm1
	ucomiss	%xmm1, %xmm3
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	movl	$105, %edi
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.size	__coshf, .-__coshf
	.weak	coshf32
	.set	coshf32,__coshf
	.weak	coshf
	.set	coshf,__coshf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
