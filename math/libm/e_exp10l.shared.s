 .section .rodata.cst16,"aM",@progbits,16
 .p2align 4
 .type c0,@object
c0: .byte 0, 0, 0, 0, 0, 0, 0x9a, 0xd4, 0x00, 0x40
 .byte 0, 0, 0, 0, 0, 0
 .size c0,.-c0;
 .type c1,@object
c1: .byte 0x58, 0x92, 0xfc, 0x15, 0x37, 0x9a, 0x97, 0xf0, 0xef, 0x3f
 .byte 0, 0, 0, 0, 0, 0
 .size c1,.-c1;
 .type csat,@object
csat: .byte 0, 0, 0, 0, 0, 0, 0, 0x80, 0x0e, 0x40
 .byte 0, 0, 0, 0, 0, 0
 .size csat,.-csat;
.section .rodata.cst16,"aM",@progbits,16
.p2align 4
.type ldbl_min,@object
ldbl_min:
.byte 0, 0, 0, 0, 0, 0, 0, 0x80, 0x1, 0
.byte 0, 0, 0, 0, 0, 0
.size ldbl_min, .-ldbl_min
 .text
.globl __ieee754_exp10l
.type __ieee754_exp10l,@function
.align 1<<4
__ieee754_exp10l:
 fldt 8(%rsp)
 fxam
 movzwl 8+8(%rsp), %eax
 andl $0x7fff, %eax
 cmpl $0x400d, %eax
 jg 5f
 cmpl $0x3fbc, %eax
 jge 3f
 fld1
 faddp
 jmp 2f
5:
 fstsw %ax
 movb $0x45, %dh
 andb %ah, %dh
 cmpb $0x05, %dh
 je 1f
 cmpb $0x01, %dh
 je 6f
 fstp %st
 fldt csat(%rip)
 andb $2, %ah
 jz 3f
 fchs
3: fldl2t
 fmul %st(1), %st
 fstcw -4(%rsp)
 movl $0xf3ff, %edx
 andl -4(%rsp), %edx
 movl %edx, -8(%rsp)
 fldcw -8(%rsp)
 frndint
 fld %st(1)
 frndint
 fldcw -4(%rsp)
 fld %st(1)
 fldt c0(%rip)
 fld %st(2)
 fmul %st(1), %st
 fsubp %st, %st(2)
 fld %st(4)
 fsub %st(3), %st
 fmulp %st, %st(1)
 faddp %st, %st(1)
 fldt c1(%rip)
 fmul %st(4), %st
 faddp %st, %st(1)
 f2xm1
 fld1
 faddp
 fstp %st(1)
 fscale
 fstp %st(1)
 fldt ldbl_min(%rip)
 fld %st(1)
 fcomip %st(1), %st(0)
 fstp %st(0)
 jnc 6464f
 fld %st(0)
 fmul %st(0)
 fstp %st(0)
6464:
 fstp %st(1)
 jmp 2f
1:
 testl $0x200, %eax
 jz 2f
 fstp %st
 fldz
2: ret
6:
 fadd %st
 ret
.size __ieee754_exp10l,.-__ieee754_exp10l
.symver __ieee754_exp10l, __exp10l_finite@GLIBC_2.15
