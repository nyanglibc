	.text
	.p2align 4,,15
	.globl	__trunc
	.type	__trunc, @function
__trunc:
	movq	%xmm0, %rcx
	movq	%xmm0, %rdx
	sarq	$52, %rcx
	andl	$2047, %ecx
	subq	$1023, %rcx
	cmpq	$51, %rcx
	jg	.L2
	movabsq	$-9223372036854775808, %rsi
	andq	%rdx, %rsi
	testq	%rcx, %rcx
	js	.L7
	movabsq	$4503599627370495, %rax
	shrq	%cl, %rax
	notq	%rax
	andq	%rdx, %rax
	orq	%rsi, %rax
	movq	%rax, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$1024, %rcx
	je	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	addsd	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rsi, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.size	__trunc, .-__trunc
	.weak	truncf32x
	.set	truncf32x,__trunc
	.weak	truncf64
	.set	truncf64,__trunc
	.weak	trunc
	.set	trunc,__trunc
