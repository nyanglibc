	.text
	.globl	__unordtf2
	.globl	__lttf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__atanhf128
	.type	__atanhf128, @function
__atanhf128:
	subq	$40, %rsp
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movaps	%xmm0, 16(%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	movdqa	.LC1(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC1(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L9
.L2:
	movdqa	16(%rsp), %xmm0
	addq	$40, %rsp
	jmp	__ieee754_atanhf128@PLT
	.p2align 4,,10
	.p2align 3
.L9:
	movdqa	.LC1(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movq	errno@gottpoff(%rip), %rax
	je	.L10
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__atanhf128, .-__atanhf128
	.weak	atanhf128
	.set	atanhf128,__atanhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073676288
