	.text
	.p2align 4,,15
	.globl	__fsubl
	.type	__fsubl, @function
__fsubl:
	pushq	%rbx
	subq	$80, %rsp
	fldt	112(%rsp)
	fldt	96(%rsp)
	fucomi	%st(1), %st
	jp	.L19
	jne	.L20
	movss	.LC1(%rip), %xmm2
	fsubp	%st, %st(1)
	fstps	28(%rsp)
	movss	28(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jnb	.L1
	ucomiss	%xmm0, %xmm0
	jp	.L10
.L8:
	fldt	96(%rsp)
	fabs
	fldt	.LC2(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L21
	fldt	112(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	fstp	%st(0)
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
.L19:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L20:
	fstp	%st(0)
	fstp	%st(0)
.L2:
#APP
# 44 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstenv 48(%rsp); fnclex
# 0 "" 2
#NO_APP
	movzwl	48(%rsp), %eax
	orw	$3903, %ax
	movw	%ax, 32(%rsp)
#APP
# 88 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 32(%rsp)
# 0 "" 2
#NO_APP
	fldt	96(%rsp)
	fldt	112(%rsp)
	fsubrp	%st, %st(1)
	fld	%st(0)
	fstpt	(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fstp	%st(0)
#APP
# 163 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstsw %ax
# 0 "" 2
#NO_APP
	movl	%eax, %ebx
#APP
# 130 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldenv 48(%rsp)
# 0 "" 2
#NO_APP
	movl	%ebx, %edi
	shrw	$5, %bx
	andl	$61, %edi
	andl	$1, %ebx
	call	__feraiseexcept@PLT
	movq	(%rsp), %rax
	movss	.LC1(%rip), %xmm2
	orl	%eax, %ebx
	movl	%ebx, 32(%rsp)
	fldt	32(%rsp)
	fstps	28(%rsp)
	movss	28(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jb	.L18
	ucomiss	.LC3(%rip), %xmm0
	jp	.L1
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	ucomiss	%xmm0, %xmm0
	jnp	.L8
	fldt	96(%rsp)
	fldt	112(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__fsubl, .-__fsubl
	.weak	f32subf64x
	.set	f32subf64x,__fsubl
	.weak	fsubl
	.set	fsubl,__fsubl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	0
