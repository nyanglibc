	.text
	.p2align 4,,15
	.globl	__ieee754_fmodf
	.type	__ieee754_fmodf, @function
__ieee754_fmodf:
	movd	%xmm0, %edx
	movd	%xmm1, %edi
	movd	%xmm0, %ecx
	movd	%xmm1, %r8d
	andl	$2147483647, %edx
	andl	$2147483647, %edi
	sete	%sil
	cmpl	$2139095040, %edi
	setg	%al
	orb	%al, %sil
	jne	.L31
	cmpl	$2139095039, %edx
	jle	.L2
.L31:
	mulss	%xmm1, %xmm0
	divss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	%edi, %edx
	jl	.L1
	movd	%xmm0, %r9d
	andl	$-2147483648, %r9d
	cmpl	%edi, %edx
	je	.L24
	cmpl	$8388607, %edx
	jg	.L6
	movl	%edx, %eax
	movl	$-126, %esi
	sall	$8, %eax
	testl	%eax, %eax
	je	.L40
	.p2align 4,,10
	.p2align 3
.L7:
	addl	%eax, %eax
	subl	$1, %esi
	testl	%eax, %eax
	jg	.L7
.L10:
	cmpl	$8388607, %edi
	jg	.L11
.L8:
	movl	%edi, %eax
	movl	$-126, %r10d
	sall	$8, %eax
	.p2align 4,,10
	.p2align 3
.L12:
	subl	$1, %r10d
	addl	%eax, %eax
	jns	.L12
.L13:
	cmpl	$-126, %esi
	jl	.L14
.L9:
	andl	$8388607, %ecx
	movl	%ecx, %edx
	orl	$8388608, %edx
.L15:
	cmpl	$-126, %r10d
	jl	.L16
	movl	%r8d, %ecx
	andl	$8388607, %ecx
	orl	$8388608, %ecx
.L17:
	subl	%r10d, %esi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	addl	%edx, %edx
	testl	%eax, %eax
	js	.L20
	je	.L24
	leal	(%rax,%rax), %edx
.L20:
	subl	$1, %esi
.L18:
	movl	%edx, %eax
	subl	%ecx, %eax
	testl	%esi, %esi
	jne	.L22
	testl	%eax, %eax
	cmovs	%edx, %eax
	testl	%eax, %eax
	jne	.L37
.L24:
	movl	%r9d, %eax
	leaq	Zero(%rip), %rdx
	shrl	$31, %eax
	movss	(%rdx,%rax,4), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	addl	%eax, %eax
	subl	$1, %r10d
.L37:
	cmpl	$8388607, %eax
	jle	.L25
	cmpl	$-126, %r10d
	jge	.L41
	movl	$-126, %ecx
	subl	%r10d, %ecx
	sarl	%cl, %eax
	orl	%r9d, %eax
	movl	%eax, -4(%rsp)
	movd	-4(%rsp), %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$-126, %ecx
	subl	%r10d, %ecx
	sall	%cl, %edi
	movl	%edi, %ecx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, %esi
	sarl	$23, %esi
	subl	$127, %esi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$-126, %ecx
	subl	%esi, %ecx
	sall	%cl, %edx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%edi, %r10d
	sarl	$23, %r10d
	subl	$127, %r10d
	jmp	.L13
.L40:
	cmpl	$8388607, %edi
	jle	.L8
	movl	%edi, %r10d
	sarl	$23, %r10d
	subl	$127, %r10d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L41:
	addl	$127, %r10d
	subl	$8388608, %eax
	sall	$23, %r10d
	orl	%r9d, %eax
	orl	%eax, %r10d
	movl	%r10d, -4(%rsp)
	movd	-4(%rsp), %xmm0
	ret
	.size	__ieee754_fmodf, .-__ieee754_fmodf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	Zero, @object
	.size	Zero, 8
Zero:
	.long	0
	.long	2147483648
