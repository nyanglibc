	.text
	.globl	__eqtf2
	.globl	__addtf3
	.globl	__netf2
	.globl	__multf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__fmaf128
	.type	__fmaf128, @function
__fmaf128:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$184, %rsp
	movaps	%xmm1, (%rsp)
	movq	8(%rsp), %r15
	movaps	%xmm0, 128(%rsp)
	shrq	$48, %r15
	andw	$32767, %r15w
	movzwl	142(%rsp), %r12d
	movzwl	%r15w, %eax
	movdqa	(%rsp), %xmm5
	movaps	%xmm2, 16(%rsp)
	andw	$32767, %r12w
	movq	16(%rsp), %r13
	movq	24(%rsp), %r14
	movzwl	%r12w, %esi
	addl	%eax, %esi
	cmpw	$32653, %r12w
	seta	%cl
	cmpl	$49036, %esi
	setg	%al
	orl	%eax, %ecx
	cmpw	$32653, %r15w
	seta	%bl
	orb	%cl, %bl
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm5, 48(%rsp)
	jne	.L2
	movq	24(%rsp), %rax
	shrq	$48, %rax
	andw	$32767, %ax
	cmpw	$32653, %ax
	ja	.L2
	cmpl	$16496, %esi
	jle	.L2
	pxor	%xmm1, %xmm1
.L169:
	call	__eqtf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	sete	%bl
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	xorl	%ebp, %ebp
.L5:
	orb	%al, %bl
	je	.L58
	pxor	%xmm1, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L58
	movdqa	(%rsp), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L58:
	leaq	144(%rsp), %rbx
	movq	%rbx, %rdi
	call	__GI_feholdexcept
	xorl	%edi, %edi
	call	__GI_fesetround
	movdqa	32(%rsp), %xmm0
	movdqa	.LC7(%rip), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	96(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	80(%rsp), %xmm2
	movaps	%xmm0, 64(%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	48(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 80(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	80(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm4
	movaps	%xmm0, 64(%rsp)
	movl	$32, %edi
	call	__GI_feclearexcept
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L174
.L60:
	movl	$3072, %edi
	call	__GI_fesetround
	movdqa	64(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movq	%xmm0, %rax
	movaps	%xmm0, 128(%rsp)
	andl	$1, %eax
	testl	%ebp, %ebp
	jne	.L63
	testl	%eax, %eax
	jne	.L64
	pextrw	$7, %xmm0, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L64
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, 128(%rsp)
.L64:
	movq	%rbx, %rdi
	call	__GI_feupdateenv
	movdqa	128(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L174:
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L60
	movq	%rbx, %rdi
	call	__GI_feupdateenv
	movdqa	48(%rsp), %xmm0
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
.L1:
	addq	$184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	16(%rsp), %xmm7
	pextrw	$7, %xmm7, %ebp
	andw	$32767, %bp
	cmpw	$32767, %bp
	je	.L175
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	movb	%cl, 96(%rsp)
	movl	%esi, 80(%rsp)
	call	__netf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm0
	setne	64(%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	movl	80(%rsp), %esi
	movzbl	96(%rsp), %ecx
	sete	%al
	testb	%al, 64(%rsp)
	je	.L10
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	movb	%cl, 80(%rsp)
	movl	%esi, 64(%rsp)
	call	__netf2@PLT
	testq	%rax, %rax
	movl	64(%rsp), %esi
	movzbl	80(%rsp), %ecx
	je	.L10
.L18:
	movdqa	(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	32(%rsp), %xmm0
	movb	%cl, 96(%rsp)
	movl	%esi, 80(%rsp)
	pextrw	$7, %xmm0, %edi
	andw	$32767, %di
	cmpw	$32767, %di
	movl	%edi, 64(%rsp)
	je	.L14
	movdqa	(%rsp), %xmm4
	pextrw	$7, %xmm4, %r8d
	andw	$32767, %r8w
	cmpw	$32767, %r8w
	movl	%r8d, 120(%rsp)
	je	.L14
	pxor	%xmm1, %xmm1
	call	__eqtf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	sete	127(%rsp)
	call	__eqtf2@PLT
	movzbl	127(%rsp), %edx
	testq	%rax, %rax
	movl	120(%rsp), %r8d
	sete	%al
	movl	64(%rsp), %edi
	movl	80(%rsp), %esi
	movzbl	96(%rsp), %ecx
	orb	%al, %dl
	jne	.L14
	cmpl	$49150, %esi
	jg	.L18
	movq	%r14, %rax
	shrq	$48, %rax
	movl	%eax, %r9d
	andw	$32767, %r9w
	cmpl	$16267, %esi
	jg	.L19
	movzbl	143(%rsp), %eax
	movq	56(%rsp), %rdx
	shrq	$63, %rdx
	shrb	$7, %al
	cmpb	%dl, %al
	je	.L80
	movdqa	.LC0(%rip), %xmm2
.L20:
	cmpw	$2, %r9w
	ja	.L176
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	movaps	%xmm2, 48(%rsp)
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movq	24(%rsp), %rax
	shrq	$48, %rax
	andw	$32767, %ax
	cmpw	$114, %ax
	ja	.L22
	movdqa	(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
.L22:
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	pxor	%xmm1, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__eqtf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	movdqa	(%rsp), %xmm0
	sete	%bl
	call	__netf2@PLT
	testq	%rax, %rax
	setne	%al
	testb	%al, %bl
	je	.L14
.L167:
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	jne	.L18
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L175:
	movdqa	32(%rsp), %xmm0
	pextrw	$7, %xmm0, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L7
	movdqa	(%rsp), %xmm7
	pextrw	$7, %xmm7, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L8
	movdqa	16(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__netf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	movdqa	16(%rsp), %xmm0
	setne	%bl
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	testb	%al, %bl
	jne	.L167
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L63:
	cmpl	$1, %ebp
	jne	.L65
	testl	%eax, %eax
	jne	.L66
	pextrw	$7, %xmm0, %eax
	andw	$32767, %ax
	cmpw	$32767, %ax
	je	.L66
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, 128(%rsp)
.L66:
	movq	%rbx, %rdi
	call	__GI_feupdateenv
	movdqa	128(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$49036, %esi
	jle	.L23
	cmpw	%r15w, %r12w
	jbe	.L24
	movzwl	142(%rsp), %eax
	leal	-113(%r12), %edx
	movl	%r9d, 48(%rsp)
	pxor	%xmm1, %xmm1
	andw	$32767, %dx
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 142(%rsp)
	movdqa	128(%rsp), %xmm4
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 32(%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%bl
	xorl	%eax, %eax
	movl	48(%rsp), %r9d
.L26:
	cmpw	$113, %r9w
	movl	$1, %ebp
	jbe	.L5
	leal	-113(%r9), %edx
	movq	%r14, %rcx
	movabsq	$-9223090561878065153, %rsi
	andq	%rsi, %rcx
	movq	%r13, 16(%rsp)
	andl	$32767, %edx
	salq	$48, %rdx
	orq	%rdx, %rcx
	movq	%rcx, 24(%rsp)
	jmp	.L5
.L23:
	cmpw	$32653, %r9w
	jbe	.L29
	cmpl	$16609, %esi
	jg	.L30
	cmpw	%r15w, %r12w
	leal	228(%r12), %edx
	ja	.L168
	movq	56(%rsp), %rdx
	leal	228(%r15), %eax
	movabsq	$-9223090561878065153, %rcx
	andl	$32767, %eax
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 56(%rsp)
.L32:
	leal	-113(%r9), %eax
	movabsq	$-9223090561878065153, %rcx
	movq	%r14, %rdx
	pxor	%xmm1, %xmm1
	andq	%rcx, %rdx
	andl	$32767, %eax
	movdqa	128(%rsp), %xmm0
	salq	$48, %rax
	movq	%r13, 16(%rsp)
	movl	$1, %ebp
	movdqa	48(%rsp), %xmm4
	orq	%rax, %rdx
	movq	%rdx, 24(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm4, (%rsp)
	call	__eqtf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	sete	%bl
	movdqa	48(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	jmp	.L5
.L176:
	movdqa	16(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	jmp	.L1
.L80:
	movdqa	.LC1(%rip), %xmm2
	jmp	.L20
.L24:
	movq	56(%rsp), %rdx
	leal	-113(%r15), %eax
	movabsq	$-9223090561878065153, %rcx
	pxor	%xmm1, %xmm1
	movl	%r9d, 64(%rsp)
	andl	$32767, %eax
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 56(%rsp)
	movdqa	48(%rsp), %xmm4
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, (%rsp)
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	xorl	%ebx, %ebx
	movl	64(%rsp), %r9d
	jmp	.L26
.L29:
	testb	%cl, %cl
	je	.L36
	movzwl	142(%rsp), %eax
	leal	-113(%r12), %edx
	andw	$32767, %dx
	andw	$-32768, %ax
	orl	%edx, %eax
	testw	%r8w, %r8w
	movw	%ax, 142(%rsp)
	je	.L37
	movq	56(%rsp), %rdx
	leal	113(%r15), %eax
	movabsq	$-9223090561878065153, %rcx
	andl	$32767, %eax
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 56(%rsp)
.L171:
	movdqa	128(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
.L172:
	pxor	%xmm1, %xmm1
	movdqa	48(%rsp), %xmm4
	movaps	%xmm4, (%rsp)
	call	__eqtf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	sete	%bl
	movdqa	48(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	xorl	%ebp, %ebp
	jmp	.L5
.L65:
	testl	%eax, %eax
	jne	.L67
	movl	$32, %edi
	call	__GI_fetestexcept
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, 128(%rsp)
	movdqa	128(%rsp), %xmm0
.L67:
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	16(%rsp), %xmm4
	movaps	%xmm4, 48(%rsp)
	movl	$32, %edi
	call	__GI_fetestexcept
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__GI_feupdateenv
	testl	%ebp, %ebp
	je	.L177
	movq	24(%rsp), %rax
	shrq	$48, %rax
	andw	$32767, %ax
	cmpw	$228, %ax
	ja	.L178
	movdqa	16(%rsp), %xmm4
	pextrw	$7, %xmm4, %eax
	andw	$32767, %ax
	cmpw	$228, %ax
	je	.L179
	movl	48(%rsp), %edx
	movq	16(%rsp), %rax
	movabsq	$-4294967296, %rcx
	movdqa	.LC8(%rip), %xmm1
	orl	$1, %edx
	andq	%rcx, %rax
	orq	%rdx, %rax
	movq	%rax, 48(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L1
.L30:
	cmpw	%r15w, %r12w
	jbe	.L33
	cmpw	$113, %r12w
	jbe	.L32
	leal	-113(%r12), %edx
.L168:
	movzwl	142(%rsp), %eax
	andw	$32767, %dx
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 142(%rsp)
	jmp	.L32
.L36:
	testb	%bl, %bl
	je	.L42
	movq	56(%rsp), %rcx
	leal	-113(%r15), %eax
	movabsq	$-9223090561878065153, %rsi
	andl	$32767, %eax
	salq	$48, %rax
	andq	%rsi, %rcx
	orq	%rax, %rcx
	testw	%di, %di
	movq	%rcx, 56(%rsp)
	je	.L43
	movzwl	142(%rsp), %eax
	leal	113(%r12), %edx
	andw	$32767, %dx
	andw	$-32768, %ax
	orl	%edx, %eax
	movw	%ax, 142(%rsp)
	jmp	.L171
.L177:
	movdqa	.LC8(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	jmp	.L1
.L37:
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	128(%rsp), %xmm4
	pxor	%xmm1, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 32(%rsp)
	jmp	.L169
.L178:
	movdqa	128(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L1
.L33:
	leal	-113(%r15), %eax
	movq	56(%rsp), %rcx
	andl	$32767, %eax
	salq	$48, %rax
	movq	%rax, %rdx
	movabsq	$-9223090561878065153, %rax
	andq	%rcx, %rax
	orq	%rdx, %rax
	cmpw	$113, %r15w
	cmova	%rax, %rcx
	movq	%rcx, 56(%rsp)
	jmp	.L32
.L42:
	cmpw	%r15w, %r12w
	jbe	.L48
	movzwl	142(%rsp), %edx
	leal	228(%r12), %eax
	andw	$32767, %ax
	andw	$-32768, %dx
	orl	%edx, %eax
	movw	%ax, 142(%rsp)
.L49:
	cmpw	$458, %r9w
	ja	.L171
	testw	%bp, %bp
	je	.L53
	leal	228(%r9), %eax
	movq	%r14, %rdx
	movabsq	$-9223090561878065153, %rcx
	andq	%rcx, %rdx
	movq	%r13, 16(%rsp)
	andl	$32767, %eax
	salq	$48, %rax
	orq	%rax, %rdx
	movq	%rdx, 24(%rsp)
.L170:
	pxor	%xmm1, %xmm1
	movdqa	128(%rsp), %xmm0
	movdqa	48(%rsp), %xmm4
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm4, (%rsp)
	call	__eqtf2@PLT
	pxor	%xmm1, %xmm1
	testq	%rax, %rax
	sete	%bl
	movdqa	48(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	sete	%al
	orl	$-1, %ebp
	jmp	.L5
.L43:
	movdqa	32(%rsp), %xmm0
	movdqa	.LC5(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 128(%rsp)
	jmp	.L172
.L48:
	movq	56(%rsp), %rdx
	leal	228(%r15), %eax
	movabsq	$-9223090561878065153, %rcx
	andl	$32767, %eax
	salq	$48, %rax
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 56(%rsp)
	jmp	.L49
.L179:
	movdqa	128(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	pextrw	$7, %xmm0, %eax
	andw	$32767, %ax
	cmpw	$229, %ax
	je	.L180
	movl	48(%rsp), %eax
	xorl	%r14d, %r14d
	movabsq	$9223372036854775807, %rsi
	movq	%r14, %rcx
	movdqa	.LC8(%rip), %xmm1
	andq	%rsi, %rcx
	leal	(%rax,%rax), %edx
	andl	$-4, %eax
	andl	$6, %edx
	movl	%edx, %edi
	movq	24(%rsp), %rdx
	orl	$1, %edi
	movq	%rdi, %r13
	shrq	$63, %rdx
	salq	$63, %rdx
	orq	%rdx, %rcx
	movq	16(%rsp), %rdx
	movq	%rcx, %r14
	movabsq	$-4294967296, %rcx
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, 48(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movq	%r13, 16(%rsp)
	movq	%r14, 24(%rsp)
	movaps	%xmm0, (%rsp)
	movdqa	.LC9(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	jmp	.L1
.L53:
	movdqa	16(%rsp), %xmm0
	movdqa	.LC6(%rip), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L170
.L180:
	movdqa	.LC8(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L1
	.size	__fmaf128, .-__fmaf128
	.weak	fmaf128
	.set	fmaf128,__fmaf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC1:
	.long	1
	.long	0
	.long	0
	.long	0
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	1081147392
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1066205184
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1081081856
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1088618496
	.align 16
.LC7:
	.long	0
	.long	8388608
	.long	0
	.long	1077411840
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	1058734080
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073545216
