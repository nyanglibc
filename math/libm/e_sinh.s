	.text
	.p2align 4,,15
	.globl	__ieee754_sinh
	.type	__ieee754_sinh, @function
__ieee754_sinh:
	movq	%xmm0, %rax
	pushq	%rbx
	shrq	$32, %rax
	subq	$16, %rsp
	movl	%eax, %ebx
	andl	$2147483647, %ebx
	cmpl	$2146435071, %ebx
	jg	.L27
	testl	%eax, %eax
	js	.L15
	movsd	.LC0(%rip), %xmm2
.L4:
	cmpl	$1077280767, %ebx
	jg	.L5
	movapd	%xmm0, %xmm1
	cmpl	$1043333119, %ebx
	andpd	.LC2(%rip), %xmm1
	jle	.L28
	movapd	%xmm1, %xmm0
	movsd	%xmm2, (%rsp)
	call	__expm1@PLT
	cmpl	$1072693247, %ebx
	movsd	(%rsp), %xmm2
	jle	.L29
	movsd	.LC5(%rip), %xmm1
	movapd	%xmm0, %xmm5
	addsd	%xmm0, %xmm1
	divsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm0
	mulsd	%xmm2, %xmm0
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1082535489, %ebx
	jle	.L30
	cmpl	$1082536909, %ebx
	jle	.L12
	cmpl	$1082536910, %ebx
	jne	.L13
	movq	%xmm0, %rax
	cmpl	$-1883637635, %eax
	ja	.L13
.L12:
	andpd	.LC2(%rip), %xmm0
	movsd	%xmm2, (%rsp)
	mulsd	.LC0(%rip), %xmm0
	call	__ieee754_exp@PLT
	movsd	(%rsp), %xmm2
	addq	$16, %rsp
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	popq	%rbx
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movsd	.LC1(%rip), %xmm2
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L13:
	mulsd	.LC4(%rip), %xmm0
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movsd	.LC5(%rip), %xmm4
.L14:
	movapd	%xmm0, %xmm1
	movapd	%xmm0, %xmm3
	addq	$16, %rsp
	addsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm3
	addsd	%xmm4, %xmm0
	popq	%rbx
	divsd	%xmm0, %xmm3
	subsd	%xmm3, %xmm1
	movapd	%xmm1, %xmm0
	mulsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	andpd	.LC2(%rip), %xmm0
	movsd	%xmm2, (%rsp)
	call	__ieee754_exp@PLT
	movsd	(%rsp), %xmm2
	addq	$16, %rsp
	popq	%rbx
	mulsd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$16, %rsp
	addsd	%xmm0, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movsd	.LC3(%rip), %xmm3
	ucomisd	%xmm1, %xmm3
	jbe	.L7
	movapd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm3
.L7:
	movsd	.LC4(%rip), %xmm3
	movsd	.LC5(%rip), %xmm4
	addsd	%xmm0, %xmm3
	ucomisd	%xmm4, %xmm3
	ja	.L1
	movapd	%xmm1, %xmm0
	movsd	%xmm4, 8(%rsp)
	movsd	%xmm2, (%rsp)
	call	__expm1@PLT
	movsd	(%rsp), %xmm2
	movsd	8(%rsp), %xmm4
	jmp	.L14
	.size	__ieee754_sinh, .-__ieee754_sinh
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1071644672
	.align 8
.LC1:
	.long	0
	.long	-1075838976
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	1048576
	.align 8
.LC4:
	.long	1017934899
	.long	2142010143
	.align 8
.LC5:
	.long	0
	.long	1072693248
