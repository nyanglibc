.globl __truncl
.type __truncl,@function
.align 1<<4
__truncl:
 fldt 8(%rsp)
 fnstenv -28(%rsp)
 movl $0xc00, %edx
 orl -28(%rsp), %edx
 movl %edx, -32(%rsp)
 fldcw -32(%rsp)
 frndint
 fnstsw
 andl $0x1, %eax
 orl %eax, -24(%rsp)
 fldenv -28(%rsp)
 ret
.size __truncl,.-__truncl
.weak truncl
truncl = __truncl
.weak truncf64x
truncf64x = __truncl
