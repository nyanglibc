	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__atanhf
	.type	__atanhf, @function
__atanhf:
	movaps	%xmm0, %xmm2
	movss	.LC1(%rip), %xmm1
	andps	.LC0(%rip), %xmm2
	ucomiss	%xmm1, %xmm2
	jnb	.L6
.L2:
	jmp	__ieee754_atanhf@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	xorl	%edi, %edi
	ucomiss	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
	setbe	%dil
	addl	$130, %edi
	jmp	__kernel_standard_f@PLT
	.size	__atanhf, .-__atanhf
	.weak	atanhf32
	.set	atanhf32,__atanhf
	.weak	atanhf
	.set	atanhf,__atanhf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
