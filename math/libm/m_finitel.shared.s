.globl __finitel
.type __finitel,@function
.align 1<<4
__finitel:
 movl 16(%rsp),%eax
 orl $0xffff8000, %eax
 incl %eax
 shrl $31, %eax
 ret
.size __finitel,.-__finitel
.weak finitel
finitel = __finitel
.globl __GI___finitel
.set __GI___finitel,__finitel
