	.text
#APP
	.symver __ieee754_sinhf,__sinhf_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_sinhf
	.type	__ieee754_sinhf, @function
__ieee754_sinhf:
	pushq	%rbx
	subq	$16, %rsp
	movss	%xmm0, 8(%rsp)
	movl	8(%rsp), %eax
	movl	%eax, %ebx
	andl	$2147483647, %ebx
	cmpl	$2139095039, %ebx
	jg	.L20
	testl	%eax, %eax
	js	.L14
	movss	.LC0(%rip), %xmm2
.L4:
	cmpl	$1102053375, %ebx
	movss	8(%rsp), %xmm1
	jle	.L21
	cmpl	$1118925183, %ebx
	jle	.L22
	cmpl	$1119016188, %ebx
	jle	.L23
	mulss	.LC4(%rip), %xmm1
	movaps	%xmm1, %xmm0
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movaps	%xmm1, %xmm3
	cmpl	$830472191, %ebx
	andps	.LC2(%rip), %xmm3
	jle	.L24
	movaps	%xmm3, %xmm0
	movss	%xmm2, 8(%rsp)
	call	__expm1f@PLT
	cmpl	$1065353215, %ebx
	movss	8(%rsp), %xmm2
	jle	.L25
	movss	.LC5(%rip), %xmm1
	movaps	%xmm0, %xmm6
	addss	%xmm0, %xmm1
	addq	$16, %rsp
	popq	%rbx
	divss	%xmm1, %xmm6
	addss	%xmm6, %xmm0
	mulss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movss	.LC1(%rip), %xmm2
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L25:
	movss	.LC5(%rip), %xmm4
.L13:
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm3
	addq	$16, %rsp
	addss	%xmm0, %xmm1
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm0
	popq	%rbx
	divss	%xmm0, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm1, %xmm0
	mulss	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	andps	.LC2(%rip), %xmm1
	movss	%xmm2, 8(%rsp)
	movaps	%xmm1, %xmm0
	call	__ieee754_expf@PLT
	movss	8(%rsp), %xmm2
	addq	$16, %rsp
	mulss	%xmm2, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$16, %rsp
	addss	%xmm0, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	andps	.LC2(%rip), %xmm1
	movss	%xmm2, 8(%rsp)
	movss	.LC0(%rip), %xmm0
	mulss	%xmm1, %xmm0
	call	__ieee754_expf@PLT
	movss	8(%rsp), %xmm2
	mulss	%xmm0, %xmm2
	mulss	%xmm2, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	movss	.LC3(%rip), %xmm0
	ucomiss	%xmm3, %xmm0
	jbe	.L7
	movaps	%xmm1, %xmm0
	mulss	%xmm1, %xmm0
.L7:
	movss	.LC4(%rip), %xmm5
	addss	%xmm1, %xmm5
	movss	.LC5(%rip), %xmm4
	movaps	%xmm1, %xmm0
	ucomiss	%xmm4, %xmm5
	ja	.L1
	movaps	%xmm3, %xmm0
	movss	%xmm4, 12(%rsp)
	movss	%xmm2, 8(%rsp)
	call	__expm1f@PLT
	movss	8(%rsp), %xmm2
	movss	12(%rsp), %xmm4
	jmp	.L13
	.size	__ieee754_sinhf, .-__ieee754_sinhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1056964608
	.align 4
.LC1:
	.long	3204448256
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC3:
	.long	8388608
	.align 4
.LC4:
	.long	2096152002
	.align 4
.LC5:
	.long	1065353216
