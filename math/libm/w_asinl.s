	.text
	.p2align 4,,15
	.globl	__asinl
	.type	__asinl, @function
__asinl:
	fldt	8(%rsp)
	fld	%st(0)
	fabs
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L4
	fstpt	8(%rsp)
	jmp	__ieee754_asinl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	fstpt	8(%rsp)
	jmp	__ieee754_asinl@PLT
	.size	__asinl, .-__asinl
	.weak	asinf64x
	.set	asinf64x,__asinl
	.weak	asinl
	.set	asinl,__asinl
