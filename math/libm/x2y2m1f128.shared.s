	.text
	.globl	__gttf2
	.globl	__netf2
	.p2align 4,,15
	.type	compare, @function
compare:
	subq	$40, %rsp
	movdqa	.LC0(%rip), %xmm0
	movdqa	(%rdi), %xmm3
	movdqa	(%rsi), %xmm2
	pand	%xmm0, %xmm3
	pand	%xmm0, %xmm2
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm3, 16(%rsp)
	movaps	%xmm2, (%rsp)
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L4
	movdqa	16(%rsp), %xmm3
	movdqa	(%rsp), %xmm2
	movdqa	%xmm3, %xmm1
	movdqa	%xmm2, %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$-1, %eax
	jmp	.L1
	.size	compare, .-compare
	.globl	__multf3
	.globl	__subtf3
	.globl	__addtf3
	.p2align 4,,15
	.globl	__x2y2m1f128
	.type	__x2y2m1f128, @function
__x2y2m1f128:
	pushq	%r14
	pushq	%r13
	xorl	%r14d, %r14d
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$176, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	92(%rsp), %r13d
	movaps	%xmm0, (%rsp)
	movl	%r13d, %eax
	andb	$-97, %ah
	cmpl	%eax, %r13d
	movl	%eax, 96(%rsp)
	movaps	%xmm1, 16(%rsp)
	jne	.L18
.L8:
	leaq	96(%rsp), %rbx
	leaq	compare(%rip), %r12
	movl	$4, %ebp
	movdqa	(%rsp), %xmm6
	movdqa	%xmm6, %xmm1
	movdqa	%xmm6, %xmm0
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 64(%rsp)
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	32(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm4
	movdqa	%xmm4, %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	48(%rsp), %xmm3
	movaps	%xmm0, (%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm5
	movaps	%xmm0, 96(%rsp)
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	call	__multf3@PLT
	movdqa	.LC1(%rip), %xmm1
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 48(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm4
	movdqa	%xmm4, %xmm1
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	32(%rsp), %xmm3
	movaps	%xmm0, (%rsp)
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	leaq	compare(%rip), %rcx
	movq	%rbx, %rdi
	movl	$16, %edx
	movaps	%xmm0, 128(%rsp)
	movl	$5, %esi
	addq	$16, %rbx
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm0, 160(%rsp)
	call	qsort@PLT
.L9:
	movdqa	-16(%rbx), %xmm2
	movdqa	(%rbx), %xmm3
	movdqa	%xmm2, %xmm0
	movdqa	%xmm3, %xmm1
	movaps	%xmm2, (%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__addtf3@PLT
	movaps	%xmm0, (%rbx)
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm3
	movdqa	%xmm3, %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__addtf3@PLT
	movaps	%xmm0, -16(%rbx)
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movl	$16, %edx
	addq	$16, %rbx
	call	qsort@PLT
	subq	$1, %rbp
	jne	.L9
	movdqa	144(%rsp), %xmm1
	movdqa	160(%rsp), %xmm0
	call	__addtf3@PLT
	movdqa	128(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	112(%rsp), %xmm1
	call	__addtf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__addtf3@PLT
	testb	%r14b, %r14b
	jne	.L19
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 96(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r14d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	movl	92(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 92(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 92(%rsp)
# 0 "" 2
#NO_APP
	addq	$176, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__x2y2m1f128, .-__x2y2m1f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	8388608
	.long	0
	.long	1077411840
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	-1073807360
