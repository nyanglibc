	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__significandf
	.type	__significandf, @function
__significandf:
	subq	$24, %rsp
	movss	%xmm0, 12(%rsp)
	call	__ilogbf@PLT
	pxor	%xmm1, %xmm1
	negl	%eax
	movss	12(%rsp), %xmm2
	addq	$24, %rsp
	movaps	%xmm2, %xmm0
	cvtsi2ss	%eax, %xmm1
	jmp	__ieee754_scalbf@PLT
	.size	__significandf, .-__significandf
	.weak	significandf32
	.set	significandf32,__significandf
	.weak	significandf
	.set	significandf,__significandf
