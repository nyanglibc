	.text
	.p2align 4,,15
	.globl	__copysignf128
	.type	__copysignf128, @function
__copysignf128:
	pand	.LC1(%rip), %xmm1
	pand	.LC0(%rip), %xmm0
	por	%xmm1, %xmm0
	ret
	.size	__copysignf128, .-__copysignf128
	.weak	copysignf128
	.set	copysignf128,__copysignf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
