.text
.globl __fmin
.type __fmin,@function
.align 1<<4
__fmin:
 ucomisd %xmm0, %xmm1
 jp 1f
 minsd %xmm1, %xmm0
 jmp 2f
1: ucomisd %xmm1, %xmm1
 jp 3f
 movsd %xmm0, -8(%rsp)
 testb $0x8, -2(%rsp)
 jz 4f
 movsd %xmm1, %xmm0
 ret
3:
 ucomisd %xmm0, %xmm0
 jp 4f
 movsd %xmm1, -8(%rsp)
 testb $0x8, -2(%rsp)
 jz 4f
 ret
4:
 addsd %xmm1, %xmm0
2: ret
.size __fmin,.-__fmin
.weak fmin
fmin = __fmin
.weak fminf64
fminf64 = __fmin
.weak fminf32x
fminf32x = __fmin
