	.text
	.p2align 4,,15
	.globl	__fpclassifyf
	.type	__fpclassifyf, @function
__fpclassifyf:
	movd	%xmm0, %edx
	movl	$2, %eax
	andl	$2147483647, %edx
	je	.L1
	cmpl	$8388607, %edx
	movl	$3, %eax
	jbe	.L1
	cmpl	$2139095039, %edx
	movl	$4, %eax
	jbe	.L1
	xorl	%eax, %eax
	cmpl	$2139095040, %edx
	setbe	%al
.L1:
	rep ret
	.size	__fpclassifyf, .-__fpclassifyf
