	.text
	.p2align 4,,15
	.type	compare, @function
compare:
	movq	.LC0(%rip), %xmm2
	movsd	(%rdi), %xmm1
	movsd	(%rsi), %xmm0
	andpd	%xmm2, %xmm1
	andpd	%xmm2, %xmm0
	ucomisd	%xmm1, %xmm0
	ja	.L3
	xorl	%eax, %eax
	movl	$1, %edx
	ucomisd	%xmm0, %xmm1
	setp	%al
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	ret
	.size	compare, .-compare
	.p2align 4,,15
	.globl	__x2y2m1
	.type	__x2y2m1, @function
__x2y2m1:
	pushq	%r14
	pushq	%r13
	xorl	%r14d, %r14d
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$64, %rsp
#APP
# 415 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	movl	12(%rsp), %r13d
	movl	%r13d, %eax
	andb	$-97, %ah
	cmpl	%eax, %r13d
	movl	%eax, 16(%rsp)
	jne	.L16
.L6:
	movsd	.LC1(%rip), %xmm2
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm4
	movq	.LC2(%rip), %rax
	mulsd	%xmm2, %xmm3
	movapd	%xmm0, %xmm5
	mulsd	%xmm1, %xmm2
	leaq	16(%rsp), %rbx
	leaq	compare(%rip), %rcx
	mulsd	%xmm0, %xmm5
	leaq	compare(%rip), %r12
	movl	$8, %edx
	movq	%rbx, %rdi
	movl	$5, %esi
	addq	$8, %rbx
	subsd	%xmm3, %xmm4
	movl	$4, %ebp
	movq	%rax, 48(%rsp)
	movsd	%xmm5, 24(%rsp)
	addsd	%xmm4, %xmm3
	subsd	%xmm3, %xmm0
	movapd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm3
	mulsd	%xmm0, %xmm4
	subsd	%xmm5, %xmm3
	mulsd	%xmm0, %xmm0
	addsd	%xmm4, %xmm3
	addsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm0
	movapd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm3
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm2, %xmm0
	movapd	%xmm1, %xmm2
	movsd	%xmm3, 40(%rsp)
	subsd	%xmm0, %xmm2
	addsd	%xmm2, %xmm0
	subsd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm0
	mulsd	%xmm1, %xmm2
	subsd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm1
	addsd	%xmm2, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rsp)
	call	qsort@PLT
.L7:
	movsd	-8(%rbx), %xmm1
	movq	%rbp, %rsi
	movsd	(%rbx), %xmm0
	movq	%rbx, %rdi
	movapd	%xmm1, %xmm2
	movq	%r12, %rcx
	movl	$8, %edx
	addq	$8, %rbx
	addsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm0
	movsd	%xmm2, -8(%rbx)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -16(%rbx)
	call	qsort@PLT
	subq	$1, %rbp
	jne	.L7
	movsd	48(%rsp), %xmm0
	testb	%r14b, %r14b
	addsd	40(%rsp), %xmm0
	addsd	32(%rsp), %xmm0
	addsd	24(%rsp), %xmm0
	addsd	16(%rsp), %xmm0
	jne	.L17
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L16:
#APP
# 421 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 16(%rsp)
# 0 "" 2
#NO_APP
	movl	$1, %r14d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 226 "../sysdeps/x86/fpu/fenv_private.h" 1
	stmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	movl	12(%rsp), %eax
	andl	$24576, %r13d
	andb	$-97, %ah
	orl	%eax, %r13d
	movl	%r13d, 12(%rsp)
#APP
# 228 "../sysdeps/x86/fpu/fenv_private.h" 1
	ldmxcsr 12(%rsp)
# 0 "" 2
#NO_APP
	addq	$64, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__x2y2m1, .-__x2y2m1
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	33554432
	.long	1101004800
	.align 8
.LC2:
	.long	0
	.long	-1074790400
