	.text
	.p2align 4,,15
	.globl	__fmull
	.type	__fmull, @function
__fmull:
	pushq	%rbx
	subq	$80, %rsp
#APP
# 44 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstenv 48(%rsp); fnclex
# 0 "" 2
#NO_APP
	movzwl	48(%rsp), %eax
	orw	$3903, %ax
	movw	%ax, 32(%rsp)
#APP
# 88 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldcw 32(%rsp)
# 0 "" 2
#NO_APP
	fldt	96(%rsp)
	fldt	112(%rsp)
	fmulp	%st, %st(1)
	fld	%st(0)
	fstpt	(%rsp)
	fld	%st(0)
	fstpt	32(%rsp)
	fstp	%st(0)
#APP
# 163 "../sysdeps/x86/fpu/fenv_private.h" 1
	fnstsw %ax
# 0 "" 2
#NO_APP
	movl	%eax, %ebx
#APP
# 130 "../sysdeps/x86/fpu/fenv_private.h" 1
	fldenv 48(%rsp)
# 0 "" 2
#NO_APP
	movl	%ebx, %edi
	shrw	$5, %bx
	andl	$61, %edi
	andl	$1, %ebx
	call	__GI___feraiseexcept
	movq	(%rsp), %rax
	movss	.LC1(%rip), %xmm2
	orl	%eax, %ebx
	movl	%ebx, 32(%rsp)
	fldt	32(%rsp)
	fstps	28(%rsp)
	movss	28(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	%xmm1, %xmm2
	jnb	.L12
	ucomiss	%xmm0, %xmm0
	jp	.L15
	fldt	96(%rsp)
	fabs
	fldt	.LC2(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L16
	fldt	112(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	fstp	%st(0)
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	fldz
	pxor	%xmm1, %xmm1
	movl	$0, %eax
	movl	$1, %ecx
	fldt	112(%rsp)
	ucomiss	%xmm1, %xmm0
	movss	%xmm1, (%rsp)
	setnp	%dl
	cmovne	%eax, %edx
	fucomip	%st(1), %st
	setp	%al
	cmovne	%ecx, %eax
	testb	%al, %dl
	je	.L17
	fldt	96(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%al
	cmove	%eax, %ecx
	testb	%cl, %cl
	jne	.L6
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	fldt	96(%rsp)
	fldt	112(%rsp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__fmull, .-__fmull
	.weak	f32mulf64x
	.set	f32mulf64x,__fmull
	.weak	fmull
	.set	fmull,__fmull
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
