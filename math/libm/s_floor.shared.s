	.text
	.p2align 4,,15
	.globl	__floor
	.type	__floor, @function
__floor:
	movq	%xmm0, %rcx
	movq	%xmm0, %rax
	sarq	$52, %rcx
	andl	$2047, %ecx
	subl	$1023, %ecx
	cmpl	$51, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L14
	movabsq	$4503599627370495, %rdx
	sarq	%cl, %rdx
	testq	%rdx, %rax
	je	.L1
	testq	%rax, %rax
	js	.L15
.L6:
	notq	%rdx
	andq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1024, %ecx
	jne	.L1
	addsd	%xmm0, %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rax, %rax
	pxor	%xmm0, %xmm0
	jns	.L1
	movabsq	$9223372036854775807, %rdx
	movsd	.LC0(%rip), %xmm0
	testq	%rdx, %rax
	je	.L1
	movsd	.LC2(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movabsq	$4503599627370496, %rsi
	sarq	%cl, %rsi
	addq	%rsi, %rax
	jmp	.L6
	.size	__floor, .-__floor
	.weak	floorf32x
	.set	floorf32x,__floor
	.weak	floorf64
	.set	floorf64,__floor
	.weak	floor
	.set	floor,__floor
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	-2147483648
	.align 8
.LC2:
	.long	0
	.long	-1074790400
