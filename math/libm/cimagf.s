	.text
	.p2align 4,,15
	.globl	__cimagf
	.type	__cimagf, @function
__cimagf:
	movq	%xmm0, -8(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.size	__cimagf, .-__cimagf
	.weak	cimagf32
	.set	cimagf32,__cimagf
	.weak	cimagf
	.set	cimagf,__cimagf
