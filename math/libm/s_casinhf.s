	.text
	.p2align 4,,15
	.globl	__casinhf
	.type	__casinhf, @function
__casinhf:
	movq	%xmm0, -16(%rsp)
	movss	.LC3(%rip), %xmm4
	movss	-16(%rsp), %xmm1
	movaps	%xmm1, %xmm0
	movss	-12(%rsp), %xmm3
	movaps	%xmm3, %xmm2
	andps	%xmm4, %xmm0
	andps	%xmm4, %xmm2
	ucomiss	%xmm0, %xmm0
	jp	.L2
	ucomiss	.LC4(%rip), %xmm0
	jbe	.L37
	ucomiss	%xmm2, %xmm2
	jp	.L32
	ucomiss	.LC4(%rip), %xmm2
	ja	.L38
.L21:
	andps	.LC7(%rip), %xmm3
.L1:
	movss	%xmm1, -40(%rsp)
	movss	%xmm3, -36(%rsp)
	movq	-40(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	ucomiss	.LC5(%rip), %xmm0
	jnb	.L4
	pxor	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jp	.L4
	jne	.L4
	ucomiss	%xmm2, %xmm2
	jp	.L34
	ucomiss	.LC4(%rip), %xmm2
	ja	.L17
	ucomiss	.LC5(%rip), %xmm2
	jnb	.L18
	ucomiss	%xmm0, %xmm3
	jp	.L18
	jne	.L18
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	ucomiss	%xmm2, %xmm2
	jp	.L34
	ucomiss	.LC4(%rip), %xmm2
	ja	.L17
.L18:
	movss	%xmm1, -24(%rsp)
	xorl	%edi, %edi
	movss	%xmm3, -20(%rsp)
	movq	-24(%rsp), %xmm0
	jmp	__kernel_casinhf@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm2, %xmm2
	jp	.L32
	ucomiss	.LC4(%rip), %xmm2
	jbe	.L39
	andps	.LC7(%rip), %xmm1
	orps	.LC8(%rip), %xmm1
.L32:
	movss	.LC2(%rip), %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movss	.LC7(%rip), %xmm0
	movss	.LC1(%rip), %xmm2
	andps	%xmm0, %xmm1
	orps	.LC8(%rip), %xmm1
.L16:
	movaps	%xmm3, %xmm5
	andps	%xmm0, %xmm5
	andnps	%xmm2, %xmm0
	orps	%xmm5, %xmm0
	movaps	%xmm0, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	movss	.LC2(%rip), %xmm1
	movaps	%xmm1, %xmm3
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	movss	.LC7(%rip), %xmm0
	movss	.LC0(%rip), %xmm2
	andps	%xmm0, %xmm1
	orps	.LC8(%rip), %xmm1
	jmp	.L16
.L39:
	ucomiss	.LC5(%rip), %xmm2
	jnb	.L32
	ucomiss	.LC6(%rip), %xmm3
	jp	.L32
	je	.L21
	jmp	.L32
	.size	__casinhf, .-__casinhf
	.weak	casinhf32
	.set	casinhf32,__casinhf
	.weak	casinhf
	.set	casinhf,__casinhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1061752795
	.align 4
.LC1:
	.long	1070141403
	.align 4
.LC2:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	2139095039
	.align 4
.LC5:
	.long	8388608
	.align 4
.LC6:
	.long	0
	.section	.rodata.cst16
	.align 16
.LC7:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.align 16
.LC8:
	.long	2139095040
	.long	0
	.long	0
	.long	0
