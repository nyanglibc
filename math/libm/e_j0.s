	.text
	.p2align 4,,15
	.type	pzero, @function
pzero:
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1102053375, %eax
	jg	.L4
	cmpl	$1075838975, %eax
	jle	.L9
	pxor	%xmm14, %xmm14
	movsd	.LC0(%rip), %xmm2
	movsd	.LC1(%rip), %xmm8
	movsd	.LC2(%rip), %xmm3
	movsd	.LC3(%rip), %xmm11
	movsd	.LC4(%rip), %xmm6
	movsd	.LC5(%rip), %xmm13
	movsd	.LC6(%rip), %xmm1
	movsd	.LC7(%rip), %xmm15
	movsd	.LC8(%rip), %xmm12
	movsd	.LC9(%rip), %xmm9
.L3:
	movsd	.LC44(%rip), %xmm5
	mulsd	%xmm0, %xmm0
	movapd	%xmm5, %xmm4
	divsd	%xmm0, %xmm4
	movapd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm1
	mulsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm10
	mulsd	%xmm4, %xmm12
	addsd	%xmm15, %xmm1
	mulsd	%xmm4, %xmm6
	addsd	%xmm11, %xmm3
	mulsd	%xmm4, %xmm9
	mulsd	%xmm4, %xmm2
	movapd	%xmm10, %xmm7
	addsd	%xmm12, %xmm14
	mulsd	%xmm10, %xmm7
	addsd	%xmm6, %xmm13
	mulsd	%xmm10, %xmm1
	addsd	%xmm5, %xmm9
	mulsd	%xmm10, %xmm3
	addsd	%xmm8, %xmm2
	mulsd	%xmm7, %xmm13
	addsd	%xmm14, %xmm1
	addsd	%xmm9, %xmm3
	mulsd	%xmm7, %xmm2
	addsd	%xmm13, %xmm1
	addsd	%xmm3, %xmm2
	divsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$1074933386, %eax
	jg	.L6
	cmpl	$1074191212, %eax
	jg	.L7
	movsd	.LC33(%rip), %xmm2
	movsd	.LC34(%rip), %xmm8
	movsd	.LC35(%rip), %xmm3
	movsd	.LC36(%rip), %xmm11
	movsd	.LC37(%rip), %xmm6
	movsd	.LC38(%rip), %xmm13
	movsd	.LC39(%rip), %xmm1
	movsd	.LC40(%rip), %xmm15
	movsd	.LC41(%rip), %xmm12
	movsd	.LC42(%rip), %xmm9
	movsd	.LC43(%rip), %xmm14
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movsd	.LC44(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movsd	.LC11(%rip), %xmm2
	movsd	.LC12(%rip), %xmm8
	movsd	.LC13(%rip), %xmm3
	movsd	.LC14(%rip), %xmm11
	movsd	.LC15(%rip), %xmm6
	movsd	.LC16(%rip), %xmm13
	movsd	.LC17(%rip), %xmm1
	movsd	.LC18(%rip), %xmm15
	movsd	.LC19(%rip), %xmm12
	movsd	.LC20(%rip), %xmm9
	movsd	.LC21(%rip), %xmm14
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movsd	.LC22(%rip), %xmm2
	movsd	.LC23(%rip), %xmm8
	movsd	.LC24(%rip), %xmm3
	movsd	.LC25(%rip), %xmm11
	movsd	.LC26(%rip), %xmm6
	movsd	.LC27(%rip), %xmm13
	movsd	.LC28(%rip), %xmm1
	movsd	.LC29(%rip), %xmm15
	movsd	.LC30(%rip), %xmm12
	movsd	.LC31(%rip), %xmm9
	movsd	.LC32(%rip), %xmm14
	jmp	.L3
	.size	pzero, .-pzero
	.p2align 4,,15
	.type	qzero, @function
qzero:
	movq	%xmm0, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	cmpl	$1102053375, %eax
	jg	.L17
	cmpl	$1075838975, %eax
	jle	.L18
	pxor	%xmm14, %xmm14
	movsd	.LC45(%rip), %xmm9
	movsd	.LC46(%rip), %xmm11
	movsd	.LC47(%rip), %xmm10
	movsd	.LC48(%rip), %xmm3
	movsd	.LC49(%rip), %xmm12
	movsd	.LC50(%rip), %xmm5
	movsd	.LC51(%rip), %xmm13
	movsd	.LC52(%rip), %xmm1
	movsd	.LC53(%rip), %xmm15
	movsd	.LC54(%rip), %xmm7
	movsd	.LC55(%rip), %xmm6
.L13:
	movapd	%xmm0, %xmm2
	movsd	.LC44(%rip), %xmm4
	mulsd	%xmm0, %xmm2
	divsd	%xmm2, %xmm4
	movapd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm4
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm6
	mulsd	%xmm2, %xmm7
	movapd	%xmm4, %xmm8
	mulsd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm12
	mulsd	%xmm11, %xmm2
	addsd	.LC44(%rip), %xmm6
	mulsd	%xmm4, %xmm8
	addsd	%xmm7, %xmm14
	addsd	%xmm5, %xmm13
	mulsd	%xmm4, %xmm12
	addsd	%xmm15, %xmm1
	addsd	%xmm2, %xmm10
	mulsd	%xmm8, %xmm13
	mulsd	%xmm4, %xmm1
	addsd	%xmm12, %xmm6
	mulsd	%xmm8, %xmm10
	mulsd	%xmm8, %xmm4
	addsd	%xmm1, %xmm14
	addsd	%xmm6, %xmm10
	mulsd	%xmm4, %xmm9
	addsd	%xmm14, %xmm13
	addsd	%xmm9, %xmm10
	divsd	%xmm10, %xmm13
	subsd	.LC93(%rip), %xmm13
	divsd	%xmm0, %xmm13
	movapd	%xmm13, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$1074933386, %eax
	jg	.L15
	cmpl	$1074191212, %eax
	jg	.L16
	movsd	.LC80(%rip), %xmm9
	movsd	.LC81(%rip), %xmm11
	movsd	.LC82(%rip), %xmm10
	movsd	.LC83(%rip), %xmm3
	movsd	.LC84(%rip), %xmm12
	movsd	.LC85(%rip), %xmm5
	movsd	.LC86(%rip), %xmm13
	movsd	.LC87(%rip), %xmm1
	movsd	.LC88(%rip), %xmm15
	movsd	.LC89(%rip), %xmm7
	movsd	.LC90(%rip), %xmm6
	movsd	.LC91(%rip), %xmm14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L17:
	movsd	.LC92(%rip), %xmm13
	divsd	%xmm0, %xmm13
	movapd	%xmm13, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movsd	.LC56(%rip), %xmm9
	movsd	.LC57(%rip), %xmm11
	movsd	.LC58(%rip), %xmm10
	movsd	.LC59(%rip), %xmm3
	movsd	.LC60(%rip), %xmm12
	movsd	.LC61(%rip), %xmm5
	movsd	.LC62(%rip), %xmm13
	movsd	.LC63(%rip), %xmm1
	movsd	.LC64(%rip), %xmm15
	movsd	.LC65(%rip), %xmm7
	movsd	.LC66(%rip), %xmm6
	movsd	.LC67(%rip), %xmm14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L16:
	movsd	.LC68(%rip), %xmm9
	movsd	.LC69(%rip), %xmm11
	movsd	.LC70(%rip), %xmm10
	movsd	.LC71(%rip), %xmm3
	movsd	.LC72(%rip), %xmm12
	movsd	.LC73(%rip), %xmm5
	movsd	.LC74(%rip), %xmm13
	movsd	.LC75(%rip), %xmm1
	movsd	.LC76(%rip), %xmm15
	movsd	.LC77(%rip), %xmm7
	movsd	.LC78(%rip), %xmm6
	movsd	.LC79(%rip), %xmm14
	jmp	.L13
	.size	qzero, .-qzero
	.p2align 4,,15
	.globl	__ieee754_j0
	.type	__ieee754_j0, @function
__ieee754_j0:
	pushq	%rbx
	movq	%xmm0, %rbx
	shrq	$32, %rbx
	subq	$64, %rsp
	andl	$2147483647, %ebx
	cmpl	$2146435071, %ebx
	jg	.L33
	movapd	%xmm0, %xmm3
	cmpl	$1073741823, %ebx
	andpd	.LC94(%rip), %xmm3
	jg	.L34
	cmpl	$1059061759, %ebx
	jg	.L27
	movsd	.LC97(%rip), %xmm0
	addsd	%xmm3, %xmm0
	cmpl	$1044381695, %ebx
	movsd	.LC44(%rip), %xmm0
	jle	.L19
	movsd	.LC98(%rip), %xmm1
	mulsd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L33:
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	.LC44(%rip), %xmm0
	divsd	%xmm1, %xmm0
.L19:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	mulsd	%xmm0, %xmm0
	cmpl	$1072693247, %ebx
	movsd	.LC99(%rip), %xmm2
	movsd	.LC101(%rip), %xmm4
	movsd	.LC44(%rip), %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm1
	subsd	.LC100(%rip), %xmm2
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm2
	addsd	%xmm4, %xmm2
	movsd	.LC102(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	mulsd	.LC106(%rip), %xmm5
	addsd	%xmm4, %xmm2
	movsd	.LC103(%rip), %xmm4
	mulsd	%xmm0, %xmm4
	addsd	.LC104(%rip), %xmm4
	mulsd	%xmm1, %xmm4
	movsd	.LC105(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm6, %xmm1
	addsd	%xmm1, %xmm4
	addsd	%xmm5, %xmm4
	divsd	%xmm4, %xmm2
	jle	.L35
	mulsd	.LC107(%rip), %xmm3
	mulsd	%xmm2, %xmm0
	movapd	%xmm3, %xmm1
	addsd	%xmm6, %xmm1
	subsd	%xmm3, %xmm6
	mulsd	%xmm6, %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L34:
	movapd	%xmm3, %xmm0
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdi
	movsd	%xmm3, 8(%rsp)
	call	__sincos@PLT
	movsd	48(%rsp), %xmm0
	cmpl	$2145386495, %ebx
	movsd	56(%rsp), %xmm1
	movapd	%xmm0, %xmm2
	movsd	8(%rsp), %xmm3
	subsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm1
	jle	.L36
.L23:
	cmpl	$1207959552, %ebx
	sqrtsd	%xmm3, %xmm4
	jle	.L26
	mulsd	.LC96(%rip), %xmm1
	movapd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L26:
	movapd	%xmm3, %xmm0
	movsd	%xmm4, 40(%rsp)
	movsd	%xmm1, 32(%rsp)
	movsd	%xmm2, 24(%rsp)
	movsd	%xmm3, 16(%rsp)
	call	pzero
	movsd	16(%rsp), %xmm3
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm3, %xmm0
	call	qzero
	movsd	32(%rsp), %xmm1
	movapd	%xmm0, %xmm3
	movsd	24(%rsp), %xmm2
	movsd	8(%rsp), %xmm0
	mulsd	%xmm3, %xmm2
	mulsd	%xmm1, %xmm0
	movsd	40(%rsp), %xmm4
	subsd	%xmm2, %xmm0
	mulsd	.LC96(%rip), %xmm0
	divsd	%xmm4, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L35:
	subsd	.LC98(%rip), %xmm2
	mulsd	%xmm2, %xmm0
	addsd	%xmm6, %xmm0
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L36:
	movapd	%xmm3, %xmm0
	movsd	%xmm1, 24(%rsp)
	movsd	%xmm2, 16(%rsp)
	addsd	%xmm3, %xmm0
	call	__cos@PLT
	movsd	48(%rsp), %xmm4
	pxor	%xmm5, %xmm5
	mulsd	56(%rsp), %xmm4
	xorpd	.LC95(%rip), %xmm0
	movsd	8(%rsp), %xmm3
	movsd	16(%rsp), %xmm2
	movsd	24(%rsp), %xmm1
	ucomisd	%xmm4, %xmm5
	ja	.L37
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L37:
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	jmp	.L23
	.size	__ieee754_j0, .-__ieee754_j0
	.p2align 4,,15
	.globl	__ieee754_y0
	.type	__ieee754_y0, @function
__ieee754_y0:
	movq	%xmm0, %rdx
	pushq	%rbx
	shrq	$32, %rdx
	subq	$64, %rsp
	movl	%edx, %ebx
	andl	$2147483647, %ebx
	cmpl	$2146435071, %ebx
	jg	.L52
	movq	%xmm0, %rax
	orl	%ebx, %eax
	je	.L53
	testl	%edx, %edx
	js	.L54
	cmpl	$1073741823, %ebx
	movapd	%xmm0, %xmm2
	jg	.L55
	cmpl	$1044381696, %ebx
	jle	.L56
	movapd	%xmm0, %xmm1
	movsd	.LC113(%rip), %xmm3
	movsd	%xmm2, 24(%rsp)
	mulsd	%xmm0, %xmm1
	movsd	.LC111(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm5
	subsd	.LC112(%rip), %xmm0
	subsd	.LC110(%rip), %xmm3
	movapd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm0
	addsd	%xmm3, %xmm0
	movsd	.LC114(%rip), %xmm3
	mulsd	%xmm1, %xmm3
	subsd	.LC115(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm0
	movapd	%xmm5, %xmm3
	mulsd	%xmm4, %xmm3
	mulsd	.LC120(%rip), %xmm4
	mulsd	.LC116(%rip), %xmm3
	addsd	%xmm3, %xmm0
	movsd	.LC117(%rip), %xmm3
	mulsd	%xmm1, %xmm3
	mulsd	.LC119(%rip), %xmm1
	addsd	.LC118(%rip), %xmm3
	addsd	.LC44(%rip), %xmm1
	mulsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm1
	addsd	%xmm4, %xmm1
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm2, %xmm0
	call	__ieee754_j0
	movsd	24(%rsp), %xmm2
	movsd	%xmm0, 16(%rsp)
	movapd	%xmm2, %xmm0
	call	__ieee754_log@PLT
	mulsd	16(%rsp), %xmm0
	mulsd	.LC109(%rip), %xmm0
	addsd	8(%rsp), %xmm0
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L53:
	movsd	.LC108(%rip), %xmm0
	divsd	.LC10(%rip), %xmm0
.L38:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm0, %xmm1
	movsd	.LC44(%rip), %xmm0
	addq	$64, %rsp
	popq	%rbx
	divsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	pxor	%xmm1, %xmm1
	mulsd	%xmm1, %xmm0
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	56(%rsp), %rsi
	leaq	48(%rsp), %rdi
	movsd	%xmm0, 8(%rsp)
	call	__sincos@PLT
	movsd	48(%rsp), %xmm0
	cmpl	$2145386495, %ebx
	movsd	56(%rsp), %xmm3
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm2
	subsd	%xmm3, %xmm1
	addsd	%xmm0, %xmm3
	jle	.L57
.L44:
	cmpl	$1207959552, %ebx
	sqrtsd	%xmm2, %xmm4
	jle	.L47
	mulsd	.LC96(%rip), %xmm1
	movapd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L56:
	call	__ieee754_log@PLT
	mulsd	.LC109(%rip), %xmm0
	subsd	.LC110(%rip), %xmm0
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L47:
	movapd	%xmm2, %xmm0
	movsd	%xmm4, 40(%rsp)
	movsd	%xmm3, 32(%rsp)
	movsd	%xmm1, 24(%rsp)
	movsd	%xmm2, 16(%rsp)
	call	pzero
	movsd	16(%rsp), %xmm2
	movsd	%xmm0, 8(%rsp)
	movapd	%xmm2, %xmm0
	call	qzero
	movsd	24(%rsp), %xmm1
	movsd	32(%rsp), %xmm3
	mulsd	8(%rsp), %xmm1
	mulsd	%xmm3, %xmm0
	movsd	40(%rsp), %xmm4
	addsd	%xmm1, %xmm0
	mulsd	.LC96(%rip), %xmm0
	divsd	%xmm4, %xmm0
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L57:
	movapd	%xmm2, %xmm0
	movsd	%xmm3, 24(%rsp)
	movsd	%xmm1, 16(%rsp)
	addsd	%xmm2, %xmm0
	call	__cos@PLT
	movsd	48(%rsp), %xmm4
	pxor	%xmm5, %xmm5
	mulsd	56(%rsp), %xmm4
	xorpd	.LC95(%rip), %xmm0
	movsd	8(%rsp), %xmm2
	movsd	16(%rsp), %xmm1
	movsd	24(%rsp), %xmm3
	ucomisd	%xmm4, %xmm5
	ja	.L58
	movapd	%xmm0, %xmm1
	divsd	%xmm3, %xmm1
	jmp	.L44
.L58:
	movapd	%xmm0, %xmm3
	divsd	%xmm1, %xmm3
	jmp	.L44
	.size	__ieee754_y0, .-__ieee754_y0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	1328302556
	.long	1088897399
	.align 8
.LC1:
	.long	2409605565
	.long	1090289935
	.align 8
.LC2:
	.long	1857466463
	.long	1088672443
	.align 8
.LC3:
	.long	1348036920
	.long	1085141885
	.align 8
.LC4:
	.long	919364669
	.long	-1061911285
	.align 8
.LC5:
	.long	3444427516
	.long	-1063032210
	.align 8
.LC6:
	.long	2065295459
	.long	-1066397438
	.align 8
.LC7:
	.long	3025119097
	.long	-1071633968
	.align 8
.LC8:
	.long	4294966578
	.long	-1078853633
	.align 8
.LC9:
	.long	128542545
	.long	1079845427
	.align 8
.LC10:
	.long	0
	.long	0
	.align 8
.LC11:
	.long	3339446372
	.long	1084410909
	.align 8
.LC12:
	.long	4202101304
	.long	1086508216
	.align 8
.LC13:
	.long	2411601248
	.long	1085758200
	.align 8
.LC14:
	.long	1551771748
	.long	1083206914
	.align 8
.LC15:
	.long	681806807
	.long	-1066031377
	.align 8
.LC16:
	.long	1732430947
	.long	-1066093645
	.align 8
.LC17:
	.long	1518147459
	.long	-1068438737
	.align 8
.LC18:
	.long	4178340799
	.long	-1072651408
	.align 8
.LC19:
	.long	3868916678
	.long	-1078853633
	.align 8
.LC20:
	.long	211338718
	.long	1078878337
	.align 8
.LC21:
	.long	1206162892
	.long	-1112991567
	.align 8
.LC22:
	.long	4231499905
	.long	1080406678
	.align 8
.LC23:
	.long	3099827070
	.long	1083285500
	.align 8
.LC24:
	.long	1829790166
	.long	1083352686
	.align 8
.LC25:
	.long	1179286542
	.long	1081514041
	.align 8
.LC26:
	.long	2828195855
	.long	-1069583700
	.align 8
.LC27:
	.long	1107958341
	.long	-1068692958
	.align 8
.LC28:
	.long	1287212564
	.long	-1070205110
	.align 8
.LC29:
	.long	2930016372
	.long	-1073527374
	.align 8
.LC30:
	.long	4156613195
	.long	-1078853642
	.align 8
.LC31:
	.long	2215083475
	.long	1078062482
	.align 8
.LC32:
	.long	1877060230
	.long	-1104813821
	.align 8
.LC33:
	.long	1144592393
	.long	1076711603
	.align 8
.LC34:
	.long	985070335
	.long	1080245251
	.align 8
.LC35:
	.long	1122633627
	.long	1081141126
	.align 8
.LC36:
	.long	250120079
	.long	1080100510
	.align 8
.LC37:
	.long	2945443599
	.long	-1073095039
	.align 8
.LC38:
	.long	3307496195
	.long	-1071226138
	.align 8
.LC39:
	.long	3987711987
	.long	-1071740173
	.align 8
.LC40:
	.long	2317658179
	.long	-1074317767
	.align 8
.LC41:
	.long	1230904898
	.long	-1078853790
	.align 8
.LC42:
	.long	2425051481
	.long	1077295205
	.align 8
.LC43:
	.long	3911647853
	.long	-1099443434
	.align 8
.LC44:
	.long	0
	.long	1072693248
	.align 8
.LC45:
	.long	747974853
	.long	-1055589011
	.align 8
.LC46:
	.long	685640509
	.long	1093248619
	.align 8
.LC47:
	.long	2208639811
	.long	1093174234
	.align 8
.LC48:
	.long	1423150143
	.long	1090610770
	.align 8
.LC49:
	.long	1315636579
	.long	1086300760
	.align 8
.LC50:
	.long	244323686
	.long	1088557780
	.align 8
.LC51:
	.long	1041822829
	.long	1086410137
	.align 8
.LC52:
	.long	355473445
	.long	1082223971
	.align 8
.LC53:
	.long	1538471126
	.long	1076332882
	.align 8
.LC54:
	.long	4294966828
	.long	1068679167
	.align 8
.LC55:
	.long	911948220
	.long	1080326357
	.align 8
.LC56:
	.long	3202074121
	.long	-1061885353
	.align 8
.LC57:
	.long	528110752
	.long	1088524568
	.align 8
.LC58:
	.long	2548048754
	.long	1089189347
	.align 8
.LC59:
	.long	2069438061
	.long	1087530962
	.align 8
.LC60:
	.long	3659645134
	.long	1084242848
	.align 8
.LC61:
	.long	1405543334
	.long	1084168169
	.align 8
.LC62:
	.long	2647180417
	.long	1083182329
	.align 8
.LC63:
	.long	176654569
	.long	1080091538
	.align 8
.LC64:
	.long	3113565651
	.long	1075271600
	.align 8
.LC65:
	.long	3513954380
	.long	1068679167
	.align 8
.LC66:
	.long	4217247043
	.long	1079292339
	.align 8
.LC67:
	.long	701271257
	.long	1035222415
	.align 8
.LC68:
	.long	538768399
	.long	-1067276309
	.align 8
.LC69:
	.long	3645878720
	.long	1084467370
	.align 8
.LC70:
	.long	3620170280
	.long	1085881452
	.align 8
.LC71:
	.long	3830316131
	.long	1085075531
	.align 8
.LC72:
	.long	2253672115
	.long	1082535299
	.align 8
.LC73:
	.long	2180653536
	.long	1080350588
	.align 8
.LC74:
	.long	3797875999
	.long	1080383963
	.align 8
.LC75:
	.long	2519576285
	.long	1078284184
	.align 8
.LC76:
	.long	1628740853
	.long	1074446588
	.align 8
.LC77:
	.long	244123714
	.long	1068679150
	.align 8
.LC78:
	.long	3219342246
	.long	1078485282
	.align 8
.LC79:
	.long	1792985986
	.long	1043516675
	.align 8
.LC80:
	.long	4172491057
	.long	-1072349590
	.align 8
.LC81:
	.long	234885989
	.long	1080726867
	.align 8
.LC82:
	.long	2623324692
	.long	1082890108
	.align 8
.LC83:
	.long	582205218
	.long	1082811973
	.align 8
.LC84:
	.long	3838921536
	.long	1081136529
	.align 8
.LC85:
	.long	1904298932
	.long	1076904113
	.align 8
.LC86:
	.long	704371786
	.long	1077914254
	.align 8
.LC87:
	.long	2868473829
	.long	1076690367
	.align 8
.LC88:
	.long	3878123420
	.long	1073739927
	.align 8
.LC89:
	.long	1049116212
	.long	1068678853
	.align 8
.LC90:
	.long	4156586733
	.long	1077829014
	.align 8
.LC91:
	.long	1425501147
	.long	1048850747
	.align 8
.LC92:
	.long	0
	.long	-1077936128
	.align 8
.LC93:
	.long	0
	.long	1069547520
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC94:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.align 16
.LC95:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC96:
	.long	1346542445
	.long	1071779287
	.align 8
.LC97:
	.long	2281731484
	.long	2117592124
	.align 8
.LC98:
	.long	0
	.long	1070596096
	.align 8
.LC99:
	.long	206584089
	.long	1052684753
	.align 8
.LC100:
	.long	3055208169
	.long	1059645093
	.align 8
.LC101:
	.long	4294967293
	.long	1066401791
	.align 8
.LC102:
	.long	1943420878
	.long	-1103899161
	.align 8
.LC103:
	.long	3464811945
	.long	1050753876
	.align 8
.LC104:
	.long	3713522676
	.long	1058973394
	.align 8
.LC105:
	.long	2194195108
	.long	1066401000
	.align 8
.LC106:
	.long	4101266831
	.long	1041500348
	.align 8
.LC107:
	.long	0
	.long	1071644672
	.align 8
.LC108:
	.long	0
	.long	-1074790400
	.align 8
.LC109:
	.long	1841940611
	.long	1071931184
	.align 8
.LC110:
	.long	2580271135
	.long	1068688598
	.align 8
.LC111:
	.long	548576107
	.long	1060554061
	.align 8
.LC112:
	.long	2976709271
	.long	1066159336
	.align 8
.LC113:
	.long	2649351164
	.long	1069980929
	.align 8
.LC114:
	.long	995011540
	.long	1045758039
	.align 8
.LC115:
	.long	1943166125
	.long	1053818535
	.align 8
.LC116:
	.long	1765782472
	.long	-1111104451
	.align 8
.LC117:
	.long	2146566909
	.long	1049715757
	.align 8
.LC118:
	.long	4118333121
	.long	1058270395
	.align 8
.LC119:
	.long	2445920026
	.long	1066013296
	.align 8
.LC120:
	.long	1003936239
	.long	1040076824
