	.text
	.p2align 4,,15
	.globl	__branred
	.type	__branred, @function
__branred:
	mulsd	tm600(%rip), %xmm0
	movl	$715827883, %edx
	pushq	%rbp
	movsd	.LC1(%rip), %xmm4
	pushq	%rbx
	movsd	t576(%rip), %xmm11
	movsd	tm24(%rip), %xmm7
	movq	%xmm11, %rbx
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm6
	mulsd	%xmm4, %xmm3
	movapd	%xmm3, %xmm0
	subsd	%xmm6, %xmm0
	subsd	%xmm0, %xmm3
	movq	%xmm3, %rcx
	subsd	%xmm3, %xmm6
	sarq	$52, %rcx
	andl	$2047, %ecx
	subl	$450, %ecx
	movl	%ecx, %eax
	sarl	$31, %ecx
	imull	%edx
	movl	$0, %eax
	sarl	$2, %edx
	subl	%ecx, %edx
	cmovs	%eax, %edx
	sarq	$32, %rbx
	movq	%xmm11, %rax
	leal	(%rdx,%rdx,2), %ecx
	movl	%ebx, %r11d
	movslq	%edx, %rdx
	movl	%eax, %eax
	salq	$3, %rdx
	sall	$23, %ecx
	subl	%ecx, %r11d
	movq	%r11, %rcx
	leaq	toverp(%rip), %r11
	salq	$32, %rcx
	orq	%rcx, %rax
	leaq	-56(%rsp), %rcx
	movq	%rax, -64(%rsp)
	xorl	%eax, %eax
	movq	-64(%rsp), %xmm2
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	(%r11,%rax), %r8
	movsd	(%r8,%rdx), %xmm1
	mulsd	%xmm3, %xmm1
	mulsd	%xmm2, %xmm1
	mulsd	%xmm7, %xmm2
	movsd	%xmm1, (%rcx,%rax)
	addq	$8, %rax
	cmpq	$48, %rax
	jne	.L2
	pxor	%xmm0, %xmm0
	leaq	24(%rcx), %rax
	movq	%rcx, %r9
	pxor	%xmm9, %xmm9
	movq	%rcx, %r10
	movsd	big(%rip), %xmm5
.L3:
	movsd	(%r10), %xmm1
	addq	$8, %r10
	movapd	%xmm1, %xmm2
	addsd	%xmm5, %xmm2
	subsd	%xmm5, %xmm2
	subsd	%xmm2, %xmm1
	addsd	%xmm2, %xmm9
	movsd	%xmm1, -8(%r10)
	cmpq	%r10, %rax
	jne	.L3
	pxor	%xmm1, %xmm1
	leaq	-48(%rcx), %rax
	movq	%rcx, %r8
	.p2align 4,,10
	.p2align 3
.L4:
	addsd	40(%r8), %xmm1
	subq	$8, %r8
	cmpq	%r8, %rax
	jne	.L4
	movsd	-56(%rsp), %xmm3
	movapd	%xmm5, %xmm2
	movq	%xmm6, %rbp
	movl	$715827883, %edx
	subsd	%xmm1, %xmm3
	movsd	big1(%rip), %xmm10
	addsd	%xmm1, %xmm2
	sarq	$52, %rbp
	andl	$2047, %ebp
	subl	$450, %ebp
	addsd	-48(%rsp), %xmm3
	movl	%ebp, %eax
	sarl	$31, %ebp
	subsd	%xmm5, %xmm2
	imull	%edx
	movl	$0, %eax
	subsd	%xmm2, %xmm1
	sarl	$2, %edx
	addsd	-40(%rsp), %xmm3
	subl	%ebp, %edx
	addsd	%xmm2, %xmm9
	cmovs	%eax, %edx
	movq	%xmm11, %rax
	leal	(%rdx,%rdx,2), %ebp
	movslq	%edx, %rdx
	movl	%eax, %eax
	salq	$3, %rdx
	sall	$23, %ebp
	subl	%ebp, %ebx
	addsd	-32(%rsp), %xmm3
	salq	$32, %rbx
	orq	%rbx, %rax
	movq	%rax, -64(%rsp)
	xorl	%eax, %eax
	addsd	-24(%rsp), %xmm3
	addsd	-16(%rsp), %xmm3
	movapd	%xmm3, %xmm8
	addsd	%xmm1, %xmm8
	subsd	%xmm8, %xmm1
	addsd	%xmm1, %xmm3
	movapd	%xmm10, %xmm1
	addsd	%xmm9, %xmm1
	subsd	%xmm10, %xmm1
	subsd	%xmm1, %xmm9
	movq	-64(%rsp), %xmm1
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	(%r11,%rax), %rbx
	movsd	(%rbx,%rdx), %xmm2
	mulsd	%xmm6, %xmm2
	mulsd	%xmm1, %xmm2
	mulsd	%xmm7, %xmm1
	movsd	%xmm2, (%rcx,%rax)
	addq	$8, %rax
	cmpq	$48, %rax
	jne	.L5
	pxor	%xmm1, %xmm1
	movq	%rcx, %rax
.L6:
	movsd	(%rax), %xmm2
	movapd	%xmm5, %xmm6
	addq	$8, %rax
	addsd	%xmm2, %xmm6
	subsd	%xmm5, %xmm6
	subsd	%xmm6, %xmm2
	addsd	%xmm6, %xmm1
	movsd	%xmm2, -8(%rax)
	cmpq	%rax, %r10
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	addsd	40(%r9), %xmm0
	subq	$8, %r9
	cmpq	%r9, %r8
	jne	.L7
	movsd	-56(%rsp), %xmm2
	movapd	%xmm5, %xmm6
	subsd	%xmm0, %xmm2
	addsd	%xmm0, %xmm6
	addsd	-48(%rsp), %xmm2
	subsd	%xmm5, %xmm6
	subsd	%xmm6, %xmm0
	addsd	-40(%rsp), %xmm2
	addsd	%xmm6, %xmm1
	movapd	%xmm8, %xmm6
	addsd	-32(%rsp), %xmm2
	addsd	-24(%rsp), %xmm2
	addsd	-16(%rsp), %xmm2
	movapd	%xmm2, %xmm5
	addsd	%xmm0, %xmm5
	subsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	movapd	%xmm10, %xmm2
	addsd	%xmm1, %xmm2
	subsd	%xmm10, %xmm2
	subsd	%xmm2, %xmm1
	movq	.LC2(%rip), %xmm2
	andpd	%xmm2, %xmm6
	andpd	%xmm5, %xmm2
	addsd	%xmm1, %xmm9
	movapd	%xmm8, %xmm1
	ucomisd	%xmm2, %xmm6
	addsd	%xmm5, %xmm1
	jbe	.L24
	ucomisd	.LC3(%rip), %xmm1
	subsd	%xmm1, %xmm8
	addsd	%xmm5, %xmm8
	ja	.L28
.L25:
	movsd	.LC5(%rip), %xmm2
	ucomisd	%xmm1, %xmm2
	jbe	.L13
	movsd	.LC4(%rip), %xmm2
	addsd	%xmm2, %xmm1
	subsd	%xmm2, %xmm9
.L13:
	movapd	%xmm8, %xmm2
	movsd	hp0(%rip), %xmm11
	movsd	mp2(%rip), %xmm10
	addsd	%xmm3, %xmm2
	movsd	mp1(%rip), %xmm6
	cvttsd2si	%xmm9, %eax
	popq	%rbx
	popq	%rbp
	addsd	%xmm0, %xmm2
	addsd	%xmm3, %xmm0
	movapd	%xmm10, %xmm3
	addsd	%xmm1, %xmm2
	mulsd	%xmm2, %xmm4
	movapd	%xmm2, %xmm7
	subsd	%xmm2, %xmm1
	andl	$3, %eax
	movapd	%xmm4, %xmm5
	addsd	%xmm8, %xmm1
	subsd	%xmm2, %xmm5
	addsd	%xmm1, %xmm0
	subsd	%xmm5, %xmm4
	movapd	%xmm11, %xmm5
	mulsd	%xmm2, %xmm5
	mulsd	hp1(%rip), %xmm2
	subsd	%xmm4, %xmm7
	mulsd	%xmm11, %xmm0
	movapd	%xmm5, %xmm1
	mulsd	%xmm7, %xmm3
	addsd	%xmm3, %xmm2
	movapd	%xmm6, %xmm3
	mulsd	%xmm7, %xmm6
	mulsd	%xmm4, %xmm3
	mulsd	%xmm10, %xmm4
	addsd	%xmm2, %xmm0
	subsd	%xmm5, %xmm3
	addsd	%xmm4, %xmm3
	addsd	%xmm6, %xmm3
	addsd	%xmm3, %xmm0
	addsd	%xmm0, %xmm1
	subsd	%xmm1, %xmm5
	movsd	%xmm1, (%rdi)
	addsd	%xmm5, %xmm0
	movsd	%xmm0, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	ucomisd	.LC3(%rip), %xmm1
	subsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm8
	jbe	.L25
.L28:
	movsd	.LC4(%rip), %xmm2
	subsd	%xmm2, %xmm1
	addsd	%xmm2, %xmm9
	jmp	.L13
	.size	__branred, .-__branred
	.section	.rodata
	.align 32
	.type	toverp, @object
	.size	toverp, 600
toverp:
	.long	1610612736
	.long	1097097008
	.long	0
	.long	1096520593
	.long	0
	.long	1094003196
	.long	2147483648
	.long	1094953960
	.long	2684354560
	.long	1097770651
	.long	1073741824
	.long	1097341804
	.long	2147483648
	.long	1096987431
	.long	1073741824
	.long	1095820304
	.long	1610612736
	.long	1097845292
	.long	1610612736
	.long	1097169879
	.long	3758096384
	.long	1097378870
	.long	0
	.long	1094858525
	.long	2147483648
	.long	1095799668
	.long	536870912
	.long	1097597129
	.long	2147483648
	.long	1095202052
	.long	2147483648
	.long	1097478723
	.long	1610612736
	.long	1097843645
	.long	0
	.long	1094496553
	.long	0
	.long	1097131997
	.long	2684354560
	.long	1096828606
	.long	0
	.long	1095196066
	.long	2147483648
	.long	1096850739
	.long	0
	.long	1096550829
	.long	1073741824
	.long	1096277904
	.long	0
	.long	1095551211
	.long	2147483648
	.long	1095549353
	.long	0
	.long	1095563854
	.long	1610612736
	.long	1096846321
	.long	0
	.long	1097318181
	.long	0
	.long	1095602172
	.long	3221225472
	.long	1097007099
	.long	0
	.long	1091985468
	.long	536870912
	.long	1097721314
	.long	1073741824
	.long	1096903489
	.long	1073741824
	.long	1096501211
	.long	2147483648
	.long	1095450471
	.long	2147483648
	.long	1094968708
	.long	3221225472
	.long	1097263592
	.long	0
	.long	1095742287
	.long	1073741824
	.long	1096284811
	.long	2147483648
	.long	1096632814
	.long	2684354560
	.long	1097399676
	.long	2684354560
	.long	1097740135
	.long	0
	.long	1092413404
	.long	1073741824
	.long	1096895058
	.long	1610612736
	.long	1097682303
	.long	3221225472
	.long	1096281159
	.long	0
	.long	1096919969
	.long	0
	.long	1096122572
	.long	3221225472
	.long	1095876382
	.long	0
	.long	1096477436
	.long	0
	.long	1097463684
	.long	3221225472
	.long	1097031302
	.long	0
	.long	1094560227
	.long	3221225472
	.long	1096952875
	.long	0
	.long	1097646945
	.long	1073741824
	.long	1096377953
	.long	0
	.long	1096271144
	.long	1073741824
	.long	1096421411
	.long	0
	.long	1097857808
	.long	3221225472
	.long	1095982281
	.long	0
	.long	1095271171
	.long	0
	.long	1094014666
	.long	1073741824
	.long	1096608306
	.long	3221225472
	.long	1096300702
	.long	1610612736
	.long	1097339277
	.long	1073741824
	.long	1095889158
	.long	2684354560
	.long	1097362681
	.long	536870912
	.long	1097571585
	.long	2147483648
	.long	1095057836
	.long	1610612736
	.long	1097371345
	.long	3221225472
	.long	1096991636
	.long	2147483648
	.long	1097577960
	.long	536870912
	.long	1097476835
	.long	0
	.long	1091893908
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	mp2, @object
	.size	mp2, 8
mp2:
	.long	1073741824
	.long	-1102193001
	.align 8
	.type	mp1, @object
	.size	mp1, 8
mp1:
	.long	1476395008
	.long	1073291771
	.align 8
	.type	hp1, @object
	.size	hp1, 8
hp1:
	.long	856972295
	.long	1016178214
	.align 8
	.type	hp0, @object
	.size	hp0, 8
hp0:
	.long	1413754136
	.long	1073291771
	.align 8
	.type	big1, @object
	.size	big1, 8
big1:
	.long	0
	.long	1129840640
	.align 8
	.type	big, @object
	.size	big, 8
big:
	.long	0
	.long	1127743488
	.align 8
	.type	tm24, @object
	.size	tm24, 8
tm24:
	.long	0
	.long	1047527424
	.align 8
	.type	tm600, @object
	.size	tm600, 8
tm600:
	.long	0
	.long	443547648
	.align 8
	.type	t576, @object
	.size	t576, 8
t576:
	.long	0
	.long	1676673024
	.align 8
.LC1:
	.long	33554432
	.long	1101004800
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	0
	.long	1071644672
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC5:
	.long	0
	.long	-1075838976
