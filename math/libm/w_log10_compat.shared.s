	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__log10
	.type	__log10, @function
__log10:
	pxor	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jnb	.L10
.L2:
	jmp	__ieee754_log10@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L2
	subq	$24, %rsp
	ucomisd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)
	jp	.L3
	jne	.L3
	movl	$4, %edi
	call	__GI___feraiseexcept
	movsd	8(%rsp), %xmm0
	movl	$18, %edi
	addq	$24, %rsp
	movapd	%xmm0, %xmm1
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %edi
	call	__GI___feraiseexcept
	movsd	8(%rsp), %xmm0
	movl	$19, %edi
	addq	$24, %rsp
	movapd	%xmm0, %xmm1
	jmp	__kernel_standard@PLT
	.size	__log10, .-__log10
	.weak	log10f32x
	.set	log10f32x,__log10
	.weak	log10f64
	.set	log10f64,__log10
	.weak	log10
	.set	log10,__log10
