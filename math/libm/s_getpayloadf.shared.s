	.text
	.p2align 4,,15
	.globl	__getpayloadf
	.type	__getpayloadf, @function
__getpayloadf:
	movl	(%rdi), %eax
	movss	.LC0(%rip), %xmm0
	movl	%eax, %edx
	andl	$2139095040, %edx
	cmpl	$2139095040, %edx
	je	.L7
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	testl	$8388607, %eax
	je	.L1
	pxor	%xmm0, %xmm0
	andl	$4194303, %eax
	cvtsi2ss	%eax, %xmm0
	ret
	.size	__getpayloadf, .-__getpayloadf
	.weak	getpayloadf32
	.set	getpayloadf32,__getpayloadf
	.weak	getpayloadf
	.set	getpayloadf,__getpayloadf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	3212836864
