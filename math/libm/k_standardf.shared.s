	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__kernel_standard_f
	.type	__kernel_standard_f, @function
__kernel_standard_f:
	cvtss2sd	%xmm0, %xmm0
	subq	$8, %rsp
	cvtss2sd	%xmm1, %xmm1
	call	__kernel_standard@PLT
	cvtsd2ss	%xmm0, %xmm0
	addq	$8, %rsp
	ret
	.size	__kernel_standard_f, .-__kernel_standard_f
