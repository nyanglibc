	.text
	.p2align 4,,15
	.type	invalid_fn, @function
invalid_fn:
	movsd	.LC0(%rip), %xmm3
	movapd	%xmm1, %xmm5
	movsd	.LC1(%rip), %xmm4
	movapd	%xmm1, %xmm2
	andpd	%xmm3, %xmm5
	ucomisd	%xmm5, %xmm4
	jbe	.L2
	andnpd	%xmm1, %xmm3
	orpd	%xmm3, %xmm4
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm2
	orpd	%xmm3, %xmm2
.L2:
	ucomisd	%xmm1, %xmm2
	jp	.L8
	jne	.L8
	ucomisd	.LC2(%rip), %xmm1
	ja	.L14
	movl	$-65000, %edi
	jmp	__scalbn@PLT
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$65000, %edi
	jmp	__scalbn@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	subsd	%xmm1, %xmm1
	movapd	%xmm1, %xmm0
	divsd	%xmm1, %xmm0
	ret
	.size	invalid_fn, .-invalid_fn
	.p2align 4,,15
	.globl	__ieee754_scalb
	.type	__ieee754_scalb, @function
__ieee754_scalb:
	ucomisd	%xmm0, %xmm0
	jp	.L20
	movapd	%xmm1, %xmm2
	movsd	.LC3(%rip), %xmm3
	andpd	.LC0(%rip), %xmm2
	ucomisd	%xmm2, %xmm3
	jb	.L30
	ucomisd	.LC6(%rip), %xmm2
	jnb	.L22
	cvttsd2si	%xmm1, %edi
	pxor	%xmm2, %xmm2
	cvtsi2sd	%edi, %xmm2
	ucomisd	%xmm1, %xmm2
	jp	.L22
	jne	.L22
	jmp	__scalbn@PLT
	.p2align 4,,10
	.p2align 3
.L30:
	ucomisd	%xmm1, %xmm1
	jp	.L20
	pxor	%xmm2, %xmm2
	ucomisd	%xmm2, %xmm1
	jbe	.L31
.L20:
	mulsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm2
.L15:
	movapd	%xmm2, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	jmp	invalid_fn
	.p2align 4,,10
	.p2align 3
.L31:
	ucomisd	%xmm2, %xmm0
	jp	.L27
	movapd	%xmm0, %xmm2
	je	.L15
.L27:
	xorpd	.LC5(%rip), %xmm1
	divsd	%xmm1, %xmm0
	movapd	%xmm0, %xmm2
	jmp	.L15
	.size	__ieee754_scalb, .-__ieee754_scalb
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1127219200
	.align 8
.LC2:
	.long	0
	.long	1089453312
	.align 8
.LC3:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC6:
	.long	0
	.long	1105199104
