	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__j0f
	.type	__j0f, @function
__j0f:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L10
.L2:
	jmp	__ieee754_j0f@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$2, %eax
	je	.L2
	cmpl	$-1, %eax
	je	.L2
	movaps	%xmm0, %xmm1
	movl	$134, %edi
	jmp	__kernel_standard_f@PLT
	.size	__j0f, .-__j0f
	.weak	j0f32
	.set	j0f32,__j0f
	.weak	j0f
	.set	j0f,__j0f
	.p2align 4,,15
	.globl	__y0f
	.type	__y0f, @function
__y0f:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L18
	ucomiss	.LC1(%rip), %xmm0
	ja	.L18
.L32:
	jmp	__ieee754_y0f@PLT
	.p2align 4,,10
	.p2align 3
.L18:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L32
	subq	$24, %rsp
	movaps	%xmm0, %xmm2
	ucomiss	%xmm0, %xmm1
	ja	.L36
	ucomiss	%xmm1, %xmm0
	jp	.L16
	je	.L37
.L16:
	cmpl	$2, %eax
	jne	.L38
	addq	$24, %rsp
	jmp	__ieee754_y0f@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$4, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	movl	$108, %edi
.L35:
	movss	12(%rsp), %xmm2
	addq	$24, %rsp
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm0
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L38:
	movaps	%xmm2, %xmm1
	movl	$135, %edi
	movaps	%xmm2, %xmm0
	addq	$24, %rsp
	jmp	__kernel_standard_f@PLT
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %edi
	movss	%xmm0, 12(%rsp)
	call	__GI_feraiseexcept
	movl	$109, %edi
	jmp	.L35
	.size	__y0f, .-__y0f
	.weak	y0f32
	.set	y0f32,__y0f
	.weak	y0f
	.set	y0f,__y0f
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1514737627
