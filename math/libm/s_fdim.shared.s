	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__fdim
	.type	__fdim, @function
__fdim:
	ucomisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm2
	jnb	.L10
	subsd	%xmm1, %xmm0
	movq	.LC1(%rip), %xmm4
	movsd	.LC2(%rip), %xmm3
	movapd	%xmm0, %xmm5
	andpd	%xmm4, %xmm5
	ucomisd	%xmm3, %xmm5
	jbe	.L1
	andpd	%xmm4, %xmm2
	ucomisd	%xmm3, %xmm2
	ja	.L1
	andpd	%xmm4, %xmm1
	ucomisd	%xmm3, %xmm1
	ja	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pxor	%xmm0, %xmm0
.L1:
	rep ret
	.size	__fdim, .-__fdim
	.weak	fdimf32x
	.set	fdimf32x,__fdim
	.weak	fdimf64
	.set	fdimf64,__fdim
	.weak	fdim
	.set	fdim,__fdim
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
