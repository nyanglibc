	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__atan2
	.type	__atan2, @function
__atan2:
	pxor	%xmm2, %xmm2
	movl	$0, %edx
	ucomisd	%xmm2, %xmm1
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L2
	ucomisd	%xmm2, %xmm0
	setnp	%al
	cmove	%eax, %edx
	testb	%dl, %dl
	jne	.L19
.L2:
	subq	$40, %rsp
	movsd	%xmm2, 8(%rsp)
	movsd	%xmm1, 24(%rsp)
	movsd	%xmm0, 16(%rsp)
	call	__ieee754_atan2@PLT
	movsd	8(%rsp), %xmm2
	movl	$0, %edx
	ucomisd	%xmm2, %xmm0
	setnp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	je	.L1
	movsd	16(%rsp), %xmm4
	movl	$1, %edx
	movsd	24(%rsp), %xmm3
	ucomisd	%xmm2, %xmm4
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L20
.L1:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2
	movl	$3, %edi
	jmp	__kernel_standard@PLT
	.p2align 4,,10
	.p2align 3
.L20:
	andpd	.LC1(%rip), %xmm3
	movsd	.LC2(%rip), %xmm2
	ucomisd	%xmm3, %xmm2
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__atan2, .-__atan2
	.weak	atan2f32x
	.set	atan2f32x,__atan2
	.weak	atan2f64
	.set	atan2f64,__atan2
	.weak	atan2
	.set	atan2,__atan2
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
