	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__letf2
	.globl	__eqtf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__subtf3
	.globl	__divtf3
	.globl	__lttf2
	.globl	__addtf3
	.p2align 4,,15
	.globl	__ctanhf128
	.type	__ctanhf128, @function
__ctanhf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$120, %rsp
	movdqa	144(%rsp), %xmm6
	movdqa	.LC2(%rip), %xmm3
	pand	%xmm6, %xmm3
	movdqa	160(%rsp), %xmm7
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm6, (%rsp)
	movaps	%xmm7, 32(%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L2
	movdqa	160(%rsp), %xmm4
	pand	.LC2(%rip), %xmm4
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm4, %xmm0
	movaps	%xmm4, 48(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L3
	movdqa	.LC3(%rip), %xmm1
	movdqa	48(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L3
	movdqa	.LC5(%rip), %xmm1
	movdqa	.LC6(%rip), %xmm0
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__multf3@PLT
	call	__fixtfsi@PLT
	movdqa	.LC8(%rip), %xmm1
	movl	%eax, %ebp
	movdqa	48(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L50
	leaq	96(%rsp), %rsi
	leaq	80(%rsp), %rdi
	movdqa	160(%rsp), %xmm0
	call	__sincosf128@PLT
.L15:
	movl	%ebp, %edi
	call	__floatsitf@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 32(%rsp)
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L59
	movdqa	.LC8(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L60
	movdqa	.LC1(%rip), %xmm2
	movaps	%xmm2, 64(%rsp)
.L21:
	movdqa	96(%rsp), %xmm5
	movdqa	%xmm5, %xmm1
	movdqa	%xmm5, %xmm0
	movaps	%xmm5, 48(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 32(%rsp)
	movdqa	48(%rsp), %xmm5
	pand	.LC2(%rip), %xmm5
	movdqa	.LC10(%rip), %xmm1
	movdqa	%xmm5, %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L23
	movdqa	(%rsp), %xmm7
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
.L23:
	movdqa	64(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	80(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	32(%rsp), %xmm1
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
.L20:
	movdqa	(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC8(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L25
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L25:
	movdqa	32(%rsp), %xmm0
	pand	.LC2(%rip), %xmm0
	movdqa	.LC8(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L8
	movdqa	32(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L8:
	movdqa	(%rsp), %xmm2
	movq	%rbx, %rax
	movdqa	32(%rsp), %xmm6
	movaps	%xmm2, (%rbx)
	movaps	%xmm6, 16(%rbx)
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	leal	(%rbp,%rbp), %edi
	call	__floatsitf@PLT
	call	__ieee754_expf128@PLT
	movdqa	144(%rsp), %xmm1
	movaps	%xmm0, 48(%rsp)
	movdqa	.LC1(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	32(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC9(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L52
	movdqa	48(%rsp), %xmm1
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L60:
	movdqa	144(%rsp), %xmm0
	call	__ieee754_sinhf128@PLT
	movaps	%xmm0, (%rsp)
	movdqa	144(%rsp), %xmm0
	call	__ieee754_coshf128@PLT
	movdqa	.LC2(%rip), %xmm7
	movdqa	(%rsp), %xmm1
	pand	%xmm1, %xmm7
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm7, 16(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L52:
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	call	__ieee754_expf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	64(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L5
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L5
	movdqa	144(%rsp), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	160(%rsp), %xmm2
	pand	.LC2(%rip), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L6
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L6
	movdqa	.LC1(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L61
.L6:
	pxor	%xmm0, %xmm0
	movdqa	160(%rsp), %xmm1
	call	__copysignf128@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L3:
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L5
	movdqa	.LC3(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jg	.L62
.L5:
	pxor	%xmm1, %xmm1
	movdqa	160(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L8
	pxor	%xmm1, %xmm1
	movdqa	144(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	.LC0(%rip), %xmm7
	jne	.L49
.L57:
	movdqa	160(%rsp), %xmm2
	pand	.LC2(%rip), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movaps	%xmm7, 32(%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L8
	movdqa	16(%rsp), %xmm2
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L8
	movl	$1, %edi
	call	__GI_feraiseexcept
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L50:
	movdqa	160(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm0
	movaps	%xmm2, 80(%rsp)
	movaps	%xmm0, 96(%rsp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L62:
	movdqa	144(%rsp), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, (%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L49:
	movaps	%xmm7, (%rsp)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	96(%rsp), %rsi
	leaq	80(%rsp), %rdi
	movdqa	160(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	96(%rsp), %xmm1
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	pxor	%xmm0, %xmm0
	call	__copysignf128@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L8
	.size	__ctanhf128, .-__ctanhf128
	.weak	ctanhf128
	.set	ctanhf128,__ctanhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1074593784
	.align 16
.LC6:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073807360
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	1066336256
