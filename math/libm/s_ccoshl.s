	.text
	.p2align 4,,15
	.globl	__ccoshl
	.type	__ccoshl, @function
__ccoshl:
	pushq	%rbp
	movq	%rsp, %rbp
	subq	$80, %rsp
	fldt	32(%rbp)
	fldt	16(%rbp)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L12
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L88
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L92
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L93
	fld	%st(1)
	fldt	.LC4(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L16
	fldz
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jp	.L16
	je	.L17
.L16:
	fldt	.LC4(%rip)
	fxch	%st(2)
	fucomip	%st(2), %st
	fstp	%st(1)
	jbe	.L81
	fstp	%st(0)
	subq	$16, %rsp
	leaq	-16(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	fldt	-16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	flds	.LC2(%rip)
	testb	$2, %ah
	fld	%st(0)
	je	.L38
	fstp	%st(0)
	flds	.LC9(%rip)
.L38:
	fldt	-32(%rbp)
	popq	%rax
	popq	%rdx
.L36:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L39
	fstp	%st(1)
	flds	.LC9(%rip)
	fxch	%st(1)
.L39:
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fld1
	je	.L40
	fstp	%st(0)
	fld1
	fchs
.L40:
	fmulp	%st, %st(2)
.L1:
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L4
	fldz
	fldt	16(%rbp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L94
	jne	.L95
	fxch	%st(2)
	fucomi	%st(0), %st
	jp	.L96
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L48
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L96:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
.L54:
	fldz
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	fxch	%st(2)
	fucomi	%st(0), %st
	jp	.L97
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L48
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L97:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L11:
	fldz
	fldt	16(%rbp)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L53
	fldz
	je	.L8
	fstp	%st(0)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L98:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L99:
	fstp	%st(0)
	fstp	%st(0)
.L53:
	flds	.LC1(%rip)
.L8:
	fld	%st(1)
	fsubp	%st, %st(2)
	fxch	%st(1)
	jmp	.L1
.L94:
	fstp	%st(1)
	fxch	%st(1)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L95:
	fstp	%st(1)
	fxch	%st(1)
.L55:
	fucomi	%st(0), %st
	jp	.L98
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L99
.L48:
	fldln2
	fnstcw	-34(%rbp)
	movzwl	-34(%rbp), %eax
	fmuls	.LC6(%rip)
	orb	$12, %ah
	movw	%ax, -36(%rbp)
	fldcw	-36(%rbp)
	fistpl	-64(%rbp)
	fldcw	-34(%rbp)
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L78
	fstpt	-80(%rbp)
	subq	$16, %rsp
	leaq	-16(%rbp), %rsi
	leaq	-32(%rbp), %rdi
	fstpt	(%rsp)
	call	__sincosl@PLT
	popq	%r9
	popq	%r10
	fldt	-80(%rbp)
.L21:
	fildl	-64(%rbp)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstpt	-64(%rbp)
	jbe	.L79
	subq	$16, %rsp
	fld	%st(0)
	fstpt	(%rsp)
	fstpt	-80(%rbp)
	call	__ieee754_expl@PLT
	fldt	16(%rbp)
	popq	%rdi
	popq	%r8
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fldt	-32(%rbp)
	testb	$2, %ah
	fldt	-80(%rbp)
	fldt	-64(%rbp)
	je	.L25
	fxch	%st(2)
	fchs
	fxch	%st(2)
.L25:
	fsub	%st(1), %st
	fld	%st(3)
	fmuls	.LC8(%rip)
	fmul	%st, %st(3)
	fxch	%st(3)
	fld	%st(0)
	fstpt	-32(%rbp)
	fldt	-16(%rbp)
	fmulp	%st, %st(4)
	fxch	%st(3)
	fld	%st(0)
	fstpt	-16(%rbp)
	fxch	%st(1)
	fucomi	%st(2), %st
	ja	.L89
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L100:
	fstp	%st(1)
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L80:
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_expl@PLT
	fldt	-16(%rbp)
	popq	%rcx
	popq	%rsi
	fmul	%st(1), %st
	fldt	-32(%rbp)
	fmulp	%st, %st(2)
	.p2align 4,,10
	.p2align 3
.L30:
	fldt	.LC4(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L31
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L31:
	fldt	.LC4(%rip)
	fld	%st(2)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L1
	fld	%st(1)
	fmul	%st(2), %st
	fstp	%st(0)
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	fstp	%st(0)
	pushq	24(%rbp)
	pushq	16(%rbp)
	call	__ieee754_coshl@PLT
	fldt	-16(%rbp)
	pushq	24(%rbp)
	pushq	16(%rbp)
	fmulp	%st, %st(1)
	fstpt	-64(%rbp)
	call	__ieee754_sinhl@PLT
	fldt	-64(%rbp)
	addq	$32, %rsp
	fldt	-32(%rbp)
	fmulp	%st, %st(2)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L89:
	fsub	%st(2), %st
	fxch	%st(3)
	fmul	%st(4), %st
	fld	%st(0)
	fstpt	-32(%rbp)
	fxch	%st(1)
	fmulp	%st, %st(4)
	fxch	%st(3)
	fld	%st(0)
	fstpt	-16(%rbp)
	fxch	%st(2)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L100
	fstp	%st(0)
	fldt	.LC3(%rip)
	fmul	%st, %st(1)
	fmulp	%st, %st(2)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L17:
	fstp	%st(0)
	fstp	%st(0)
	fldt	16(%rbp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fld1
	jne	.L90
.L49:
	flds	.LC2(%rip)
	fxch	%st(1)
	fmulp	%st, %st(2)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	fstp	%st(0)
	fstp	%st(0)
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	flds	.LC1(%rip)
	jp	.L101
	je	.L91
	fstp	%st(1)
	jmp	.L51
.L101:
	fstp	%st(1)
	.p2align 4,,10
	.p2align 3
.L51:
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L78:
	fxch	%st(1)
	fstpt	-32(%rbp)
	fld1
	fstpt	-16(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L90:
	fstp	%st(0)
	fld1
	fchs
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L91:
	jmp	.L1
.L81:
	fstp	%st(1)
	flds	.LC2(%rip)
	fld	%st(0)
	fxch	%st(2)
	jmp	.L36
.L92:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L93:
	fstp	%st(0)
.L44:
	flds	.LC2(%rip)
	fld	%st(1)
	fsubp	%st, %st(2)
	jmp	.L1
	.size	__ccoshl, .-__ccoshl
	.weak	ccoshf64x
	.set	ccoshf64x,__ccoshl
	.weak	ccoshl
	.set	ccoshl,__ccoshl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2143289344
	.align 4
.LC2:
	.long	2139095040
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC4:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC6:
	.long	1182792704
	.align 4
.LC8:
	.long	1056964608
	.align 4
.LC9:
	.long	4286578688
