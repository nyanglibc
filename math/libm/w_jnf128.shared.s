	.text
	.p2align 4,,15
	.globl	__jnf128
	.type	__jnf128, @function
__jnf128:
	jmp	__ieee754_jnf128@PLT
	.size	__jnf128, .-__jnf128
	.weak	jnf128
	.set	jnf128,__jnf128
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__lttf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__ynf128
	.type	__ynf128, @function
__ynf128:
	pushq	%rbx
	pxor	%xmm1, %xmm1
	movl	%edi, %ebx
	subq	$16, %rsp
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L4
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L12
.L4:
	movdqa	(%rsp), %xmm0
	addq	$16, %rsp
	movl	%ebx, %edi
	popq	%rbx
	jmp	__ieee754_ynf128@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L13
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L13:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__ynf128, .-__ynf128
	.weak	ynf128
	.set	ynf128,__ynf128
