	.text
	.p2align 4,,15
	.globl	__cbrt
	.type	__cbrt, @function
__cbrt:
	movapd	%xmm0, %xmm1
	subq	$40, %rsp
	movsd	%xmm0, 8(%rsp)
	leaq	28(%rsp), %rdi
	andpd	.LC0(%rip), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, (%rsp)
	call	__frexp@PLT
	movl	28(%rsp), %ecx
	movapd	%xmm0, %xmm4
	pxor	%xmm3, %xmm3
	movsd	(%rsp), %xmm1
	testl	%ecx, %ecx
	movsd	8(%rsp), %xmm2
	jne	.L2
	ucomisd	%xmm1, %xmm1
	jp	.L3
	ucomisd	.LC1(%rip), %xmm1
	ja	.L3
	ucomisd	.LC2(%rip), %xmm1
	pxor	%xmm3, %xmm3
	jnb	.L2
	ucomisd	%xmm3, %xmm2
	jp	.L2
	je	.L3
	.p2align 4,,10
	.p2align 3
.L2:
	movsd	.LC4(%rip), %xmm0
	movl	%ecx, %eax
	movl	$1431655766, %edi
	movsd	.LC5(%rip), %xmm1
	mulsd	%xmm4, %xmm0
	imull	%edi
	movl	%ecx, %eax
	sarl	$31, %eax
	subsd	%xmm0, %xmm1
	movapd	%xmm4, %xmm0
	movl	%edx, %edi
	addsd	%xmm4, %xmm0
	subl	%eax, %edi
	leal	(%rdi,%rdi,2), %eax
	mulsd	%xmm4, %xmm1
	subl	%eax, %ecx
	leaq	factor(%rip), %rax
	addl	$2, %ecx
	ucomisd	%xmm3, %xmm2
	subsd	.LC6(%rip), %xmm1
	movslq	%ecx, %rcx
	mulsd	%xmm4, %xmm1
	addsd	.LC7(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	subsd	.LC8(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC9(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	.LC10(%rip), %xmm1
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm0
	addsd	%xmm5, %xmm5
	mulsd	%xmm0, %xmm1
	addsd	%xmm5, %xmm4
	movapd	%xmm1, %xmm0
	divsd	%xmm4, %xmm0
	mulsd	(%rax,%rcx,8), %xmm0
	jbe	.L13
.L6:
	call	__ldexp@PLT
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorpd	.LC11(%rip), %xmm0
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	addsd	%xmm2, %xmm2
	addq	$40, %rsp
	movapd	%xmm2, %xmm0
	ret
	.size	__cbrt, .-__cbrt
	.weak	cbrtf32x
	.set	cbrtf32x,__cbrt
	.weak	cbrtf64
	.set	cbrtf64,__cbrt
	.weak	cbrt
	.set	cbrt,__cbrt
	.section	.rodata
	.align 32
	.type	factor, @object
	.size	factor, 40
factor:
	.long	4186796682
	.long	1071917218
	.long	2772266556
	.long	1072260606
	.long	0
	.long	1072693248
	.long	4186796683
	.long	1072965794
	.long	2772266557
	.long	1073309182
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.align 8
.LC2:
	.long	0
	.long	1048576
	.align 8
.LC4:
	.long	3901961837
	.long	1069717505
	.align 8
.LC5:
	.long	1877469572
	.long	1072242218
	.align 8
.LC6:
	.long	3483504396
	.long	1073568486
	.align 8
.LC7:
	.long	2913817068
	.long	1073976144
	.align 8
.LC8:
	.long	2009959773
	.long	1073802114
	.align 8
.LC9:
	.long	3733989556
	.long	1073226125
	.align 8
.LC10:
	.long	3122040818
	.long	1071036060
	.section	.rodata.cst16
	.align 16
.LC11:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
