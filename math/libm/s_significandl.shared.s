	.text
	.p2align 4,,15
	.globl	__significandl
	.type	__significandl, @function
__significandl:
	fldt	8(%rsp)
#APP
# 13 "../sysdeps/i386/fpu/s_significandl.c" 1
	fxtract
fstp	%st(1)
# 0 "" 2
#NO_APP
	ret
	.size	__significandl, .-__significandl
	.weak	significandl
	.set	significandl,__significandl
