	.text
	.p2align 4,,15
	.globl	__scalbln
	.type	__scalbln, @function
__scalbln:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$52, %rax
	andl	$2047, %eax
	je	.L14
	cmpq	$2047, %rax
	je	.L15
.L4:
	cmpq	$-50000, %rdi
	jl	.L12
	cmpq	$50000, %rdi
	jg	.L6
	addq	%rax, %rdi
	cmpq	$2046, %rdi
	jg	.L6
	testq	%rdi, %rdi
	jle	.L8
	movabsq	$-9218868437227405313, %rax
	salq	$52, %rdi
	andq	%rax, %rdx
	orq	%rdi, %rdx
	movq	%rdx, -8(%rsp)
.L1:
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	andpd	.LC2(%rip), %xmm0
	orpd	.LC4(%rip), %xmm0
	mulsd	.LC5(%rip), %xmm0
	movsd	%xmm0, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpq	$-53, %rdi
	jge	.L9
.L12:
	andpd	.LC2(%rip), %xmm0
	orpd	.LC1(%rip), %xmm0
	mulsd	.LC3(%rip), %xmm0
	movsd	%xmm0, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movabsq	$4503599627370495, %rax
	movsd	%xmm0, -8(%rsp)
	testq	%rax, %rdx
	je	.L1
	mulsd	.LC0(%rip), %xmm0
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$52, %rax
	andl	$2047, %eax
	subq	$54, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L15:
	addsd	%xmm0, %xmm0
	movsd	%xmm0, -8(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$54, %rdi
	movabsq	$-9218868437227405313, %rax
	salq	$52, %rdi
	andq	%rax, %rdx
	orq	%rdx, %rdi
	movq	%rdi, -8(%rsp)
	movsd	-8(%rsp), %xmm1
	mulsd	.LC6(%rip), %xmm1
	movsd	%xmm1, -8(%rsp)
	jmp	.L1
	.size	__scalbln, .-__scalbln
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1129316352
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	3271095129
	.long	27618847
	.long	0
	.long	0
	.align 16
.LC2:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	3271095129
	.long	27618847
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	2281731484
	.long	2117592124
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	2281731484
	.long	2117592124
	.align 8
.LC6:
	.long	0
	.long	1016070144
