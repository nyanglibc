	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__nextdown
	.type	__nextdown, @function
__nextdown:
	subq	$24, %rsp
	movq	.LC0(%rip), %xmm1
	xorpd	%xmm1, %xmm0
	movaps	%xmm1, (%rsp)
	call	__nextup@PLT
	movapd	(%rsp), %xmm1
	addq	$24, %rsp
	xorpd	%xmm1, %xmm0
	ret
	.size	__nextdown, .-__nextdown
	.weak	nextdownf32x
	.set	nextdownf32x,__nextdown
	.weak	nextdownf64
	.set	nextdownf64,__nextdown
	.weak	nextdown
	.set	nextdown,__nextdown
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
