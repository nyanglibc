	.text
#APP
	.symver __totalordermagl0,totalordermagl@@GLIBC_2.31
	.symver __totalordermagl1,totalordermagf64x@@GLIBC_2.31
	.symver __totalordermag_compatl2,totalordermagl@GLIBC_2.25
	.symver __totalordermag_compatl3,totalordermagf64x@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__totalordermagl
	.type	__totalordermagl, @function
__totalordermagl:
	movl	8(%rdi), %r10d
	movq	(%rdi), %r9
	movl	$1, %eax
	movl	8(%rsi), %edi
	movq	(%rsi), %rsi
	movq	%r10, %rcx
	movq	%rdi, %rdx
	andw	$32767, %cx
	andw	$32767, %dx
	cmpw	%dx, %cx
	jb	.L1
	movl	$0, %eax
	je	.L7
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r9, %rcx
	movq	%rsi, %rdx
	movl	$1, %eax
	shrq	$32, %rcx
	shrq	$32, %rdx
	cmpl	%edx, %ecx
	jb	.L1
	sete	%dl
	xorl	%eax, %eax
	cmpl	%esi, %r9d
	setbe	%al
	andl	%edx, %eax
	ret
	.size	__totalordermagl, .-__totalordermagl
	.globl	__totalordermagl1
	.set	__totalordermagl1,__totalordermagl
	.globl	__totalordermagl0
	.set	__totalordermagl0,__totalordermagl
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalordermag_compatl
	.type	__totalordermag_compatl, @function
__totalordermag_compatl:
	subq	$8, %rsp
	leaq	32(%rsp), %rsi
	leaq	16(%rsp), %rdi
	call	__totalordermagl@PLT
	addq	$8, %rsp
	ret
	.size	__totalordermag_compatl, .-__totalordermag_compatl
	.globl	__totalordermag_compatl3
	.set	__totalordermag_compatl3,__totalordermag_compatl
	.globl	__totalordermag_compatl2
	.set	__totalordermag_compatl2,__totalordermag_compatl
