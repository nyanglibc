	.text
#APP
	.symver __totalorderf0,totalorderf@@GLIBC_2.31
	.symver __totalorderf1,totalorderf32@@GLIBC_2.31
	.symver __totalorder_compatf2,totalorderf@GLIBC_2.25
	.symver __totalorder_compatf3,totalorderf32@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__totalorderf
	.type	__totalorderf, @function
__totalorderf:
	movl	(%rdi), %eax
	movl	(%rsi), %ecx
	cltd
	shrl	%edx
	xorl	%eax, %edx
	movl	%ecx, %eax
	sarl	$31, %eax
	shrl	%eax
	xorl	%ecx, %eax
	cmpl	%eax, %edx
	setle	%al
	movzbl	%al, %eax
	ret
	.size	__totalorderf, .-__totalorderf
	.globl	__totalorderf1
	.set	__totalorderf1,__totalorderf
	.globl	__totalorderf0
	.set	__totalorderf0,__totalorderf
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__totalorder_compatf
	.type	__totalorder_compatf, @function
__totalorder_compatf:
	subq	$24, %rsp
	leaq	8(%rsp), %rsi
	leaq	12(%rsp), %rdi
	movss	%xmm0, 12(%rsp)
	movss	%xmm1, 8(%rsp)
	call	__totalorderf@PLT
	addq	$24, %rsp
	ret
	.size	__totalorder_compatf, .-__totalorder_compatf
	.globl	__totalorder_compatf3
	.set	__totalorder_compatf3,__totalorder_compatf
	.globl	__totalorder_compatf2
	.set	__totalorder_compatf2,__totalorder_compatf
