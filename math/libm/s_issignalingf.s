	.text
	.p2align 4,,15
	.globl	__issignalingf
	.type	__issignalingf, @function
__issignalingf:
	movd	%xmm0, %eax
	xorl	$4194304, %eax
	andl	$2147483647, %eax
	cmpl	$2143289344, %eax
	seta	%al
	movzbl	%al, %eax
	ret
	.size	__issignalingf, .-__issignalingf
