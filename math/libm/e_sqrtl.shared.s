	.text
#APP
	.symver __ieee754_sqrtl,__sqrtl_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_sqrtl
	.type	__ieee754_sqrtl, @function
__ieee754_sqrtl:
	fldt	8(%rsp)
	fsqrt
	ret
	.size	__ieee754_sqrtl, .-__ieee754_sqrtl
