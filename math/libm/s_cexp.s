	.text
	.p2align 4,,15
	.globl	__cexp
	.type	__cexp, @function
__cexp:
	pushq	%rbx
	movapd	%xmm0, %xmm3
	movapd	%xmm0, %xmm6
	subq	$80, %rsp
	movapd	%xmm1, %xmm2
	movq	.LC4(%rip), %xmm4
	andpd	%xmm4, %xmm0
	andpd	%xmm4, %xmm2
	ucomisd	%xmm0, %xmm0
	jp	.L2
	ucomisd	.LC5(%rip), %xmm0
	jbe	.L68
	ucomisd	%xmm2, %xmm2
	jp	.L36
	ucomisd	.LC5(%rip), %xmm2
	ja	.L36
	movsd	.LC6(%rip), %xmm5
	movapd	%xmm1, %xmm7
	ucomisd	%xmm5, %xmm2
	jnb	.L11
	pxor	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jp	.L14
	je	.L12
.L14:
	movmskpd	%xmm3, %eax
	testb	$1, %al
	je	.L66
.L47:
	pxor	%xmm3, %xmm3
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L68:
	ucomisd	%xmm2, %xmm2
	movsd	.LC6(%rip), %xmm5
	jp	.L8
	ucomisd	.LC5(%rip), %xmm2
	jbe	.L69
.L8:
	movl	$1, %edi
	call	feraiseexcept@PLT
	movsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm1
.L1:
	addq	$80, %rsp
	popq	%rbx
	ret
.L69:
	movsd	.LC8(%rip), %xmm0
	ucomisd	%xmm5, %xmm2
	mulsd	.LC7(%rip), %xmm0
	cvttsd2si	%xmm0, %ebx
	jbe	.L57
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm5, 56(%rsp)
	movapd	%xmm1, %xmm0
	movsd	%xmm3, 16(%rsp)
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm6, (%rsp)
	call	__sincos@PLT
	movsd	(%rsp), %xmm6
	movsd	16(%rsp), %xmm3
	movapd	32(%rsp), %xmm4
	movsd	56(%rsp), %xmm5
.L18:
	pxor	%xmm2, %xmm2
	cvtsi2sd	%ebx, %xmm2
	ucomisd	%xmm2, %xmm3
	ja	.L70
.L58:
	movapd	%xmm6, %xmm0
	movsd	%xmm5, 16(%rsp)
	movaps	%xmm4, (%rsp)
	call	__ieee754_exp@PLT
	movapd	%xmm0, %xmm1
	movsd	72(%rsp), %xmm0
	movsd	16(%rsp), %xmm5
	mulsd	%xmm1, %xmm0
	movapd	(%rsp), %xmm4
	mulsd	64(%rsp), %xmm1
.L24:
	movapd	%xmm0, %xmm2
	andpd	%xmm4, %xmm2
	ucomisd	%xmm2, %xmm5
	jbe	.L25
	movapd	%xmm0, %xmm2
	mulsd	%xmm0, %xmm2
.L25:
	andpd	%xmm1, %xmm4
	ucomisd	%xmm4, %xmm5
	jbe	.L1
	movapd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm2
	addq	$80, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movapd	%xmm2, %xmm0
	movsd	%xmm5, 56(%rsp)
	movaps	%xmm4, 32(%rsp)
	movsd	%xmm3, 16(%rsp)
	movsd	%xmm2, (%rsp)
	call	__ieee754_exp@PLT
	movsd	16(%rsp), %xmm3
	movsd	(%rsp), %xmm2
	movapd	%xmm3, %xmm6
	movsd	64(%rsp), %xmm1
	movsd	72(%rsp), %xmm3
	subsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm3
	movapd	32(%rsp), %xmm4
	movsd	56(%rsp), %xmm5
	ucomisd	%xmm2, %xmm6
	movsd	%xmm1, 64(%rsp)
	movsd	%xmm3, 72(%rsp)
	jbe	.L58
	subsd	%xmm2, %xmm6
	mulsd	%xmm0, %xmm1
	mulsd	%xmm3, %xmm0
	ucomisd	%xmm2, %xmm6
	movsd	%xmm1, 64(%rsp)
	movsd	%xmm0, 72(%rsp)
	jbe	.L58
	mulsd	.LC5(%rip), %xmm0
	mulsd	.LC5(%rip), %xmm1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L11:
	movmskpd	%xmm3, %eax
	testb	$1, %al
	jne	.L47
.L66:
	movsd	.LC1(%rip), %xmm3
.L41:
	ucomisd	%xmm5, %xmm2
	jbe	.L59
	movapd	%xmm1, %xmm0
	leaq	72(%rsp), %rsi
	leaq	64(%rsp), %rdi
	movsd	%xmm3, (%rsp)
	movaps	%xmm4, 16(%rsp)
	call	__sincos@PLT
	movsd	72(%rsp), %xmm0
	movsd	64(%rsp), %xmm7
	movsd	(%rsp), %xmm3
.L30:
	movq	.LC9(%rip), %xmm1
	movapd	%xmm0, %xmm5
	movapd	%xmm1, %xmm6
	andpd	%xmm1, %xmm5
	andnpd	%xmm3, %xmm6
	orpd	%xmm5, %xmm6
	movapd	%xmm6, %xmm0
	movapd	%xmm7, %xmm6
	andpd	%xmm1, %xmm6
	andnpd	%xmm3, %xmm1
	orpd	%xmm6, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movmskpd	%xmm3, %eax
	testb	$1, %al
	jne	.L1
	movsd	.LC1(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	ucomisd	%xmm2, %xmm2
	jp	.L71
	ucomisd	.LC5(%rip), %xmm2
	ja	.L8
	ucomisd	.LC6(%rip), %xmm2
	jnb	.L8
	ucomisd	.LC0(%rip), %xmm1
	jp	.L8
	jne	.L8
	movsd	.LC3(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movq	.LC2(%rip), %rax
	movsd	%xmm1, 64(%rsp)
	movq	%rax, 72(%rsp)
	jmp	.L18
.L59:
	movsd	.LC2(%rip), %xmm0
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L36:
	movmskpd	%xmm3, %eax
	testb	$1, %al
	jne	.L72
	movsd	.LC1(%rip), %xmm0
	subsd	%xmm1, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L72:
	pxor	%xmm0, %xmm0
	andpd	.LC9(%rip), %xmm1
	jmp	.L1
.L71:
	movsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm1
	jmp	.L1
	.size	__cexp, .-__cexp
	.weak	cexpf32x
	.set	cexpf32x,__cexp
	.weak	cexpf64
	.set	cexpf64,__cexp
	.weak	cexp
	.set	cexp,__cexp
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	0
	.align 8
.LC1:
	.long	0
	.long	2146435072
	.align 8
.LC2:
	.long	0
	.long	1072693248
	.align 8
.LC3:
	.long	0
	.long	2146959360
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	4294967295
	.long	2146435071
	.align 8
.LC6:
	.long	0
	.long	1048576
	.align 8
.LC7:
	.long	4277811695
	.long	1072049730
	.align 8
.LC8:
	.long	0
	.long	1083176960
	.section	.rodata.cst16
	.align 16
.LC9:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
