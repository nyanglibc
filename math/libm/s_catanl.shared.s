	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__catanl
	.type	__catanl, @function
__catanl:
	subq	$88, %rsp
	fldt	96(%rsp)
	fldt	112(%rsp)
	fld	%st(1)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L149
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L150
	fstp	%st(0)
	fstp	%st(0)
	fucomip	%st(0), %st
	jp	.L60
.L60:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L157
	fstp	%st(0)
	fldz
	fchs
	fxch	%st(1)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L157:
	fxch	%st(1)
.L50:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC4(%rip)
	jne	.L151
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L175:
	fxch	%st(1)
.L146:
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L150:
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jb	.L125
	fstp	%st(1)
	fxch	%st(2)
	fucomi	%st(0), %st
	jp	.L158
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L159
	jmp	.L55
.L169:
	fstp	%st(4)
	fxch	%st(1)
	fxch	%st(3)
	jmp	.L55
.L170:
	fxch	%st(1)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L55
.L171:
	fxch	%st(1)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L173:
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(2)
.L55:
	flds	.LC7(%rip)
	fxch	%st(2)
	fucomi	%st(2), %st
	jnb	.L160
	fxch	%st(1)
	fucomi	%st(2), %st
	fstp	%st(2)
	jnb	.L161
	fxch	%st(1)
	fucomi	%st(1), %st
	ja	.L68
	fld	%st(0)
	fld	%st(2)
	fld1
	flds	.LC11(%rip)
	fucomip	%st(3), %st
	jbe	.L162
	fstp	%st(2)
	fxch	%st(1)
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L177:
	fstp	%st(2)
	fxch	%st(1)
.L156:
	fld	%st(0)
	fsub	%st(2), %st
	fxch	%st(2)
	faddp	%st, %st(1)
	fmulp	%st, %st(1)
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L143
	jne	.L143
	fstp	%st(0)
	fldz
.L143:
	flds	.LC9(%rip)
.L30:
	fld	%st(5)
	fadd	%st(6), %st
	fxch	%st(1)
	fxch	%st(2)
#APP
# 28 "../sysdeps/x86/fpu/math_private.h" 1
	fpatan
# 0 "" 2
#NO_APP
	fmul	%st(1), %st
	fld1
	fxch	%st(3)
	fucomip	%st(3), %st
	fstp	%st(2)
	flds	.LC13(%rip)
	jp	.L37
	jne	.L37
	fld	%st(0)
	fucomip	%st(4), %st
	ja	.L152
.L37:
	fxch	%st(3)
	fucomip	%st(3), %st
	fstp	%st(2)
	jb	.L135
	fxch	%st(3)
	fmul	%st(0), %st
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(1)
.L41:
	fld1
	fld	%st(4)
	fadd	%st(1), %st
	fmul	%st(0), %st
	fxch	%st(1)
	fsubr	%st(5), %st
	fmul	%st(0), %st
	fadd	%st(2), %st
	fxch	%st(2)
	faddp	%st, %st(1)
	fdiv	%st(1), %st
	fxch	%st(2)
	fstpt	(%rsp)
	fxch	%st(2)
	fucomip	%st(1), %st
	jbe	.L136
	fstp	%st(1)
	fstp	%st(1)
	subq	$16, %rsp
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
.L144:
	fmuls	.LC10(%rip)
	fldt	16(%rsp)
	popq	%rax
	popq	%rdx
	fld	%st(0)
	fld	%st(2)
.L24:
	fldt	.LC3(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L163
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L163:
	fstp	%st(2)
	fxch	%st(1)
.L45:
	fldt	.LC3(%rip)
	fld	%st(3)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L164
	fxch	%st(2)
	fmul	%st(0), %st
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L164:
	fstp	%st(2)
.L1:
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	fldz
	fxch	%st(6)
	fucomi	%st(6), %st
	jp	.L165
	jne	.L166
	fxch	%st(4)
	fucomi	%st(0), %st
	jp	.L167
	fucomi	%st(3), %st
	fstp	%st(3)
	ja	.L168
	fxch	%st(2)
	fucomi	%st(1), %st
	fstp	%st(1)
	jnb	.L169
	fxch	%st(3)
	fucomi	%st(4), %st
	fstp	%st(4)
	jp	.L170
	jne	.L171
	fstp	%st(0)
	fstp	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L165:
	fstp	%st(3)
	fstp	%st(5)
	fstp	%st(0)
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(1)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L166:
	fstp	%st(3)
	fstp	%st(5)
	fstp	%st(0)
	fxch	%st(2)
	fxch	%st(3)
	fxch	%st(1)
.L9:
	fucomi	%st(0), %st
	jp	.L172
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L173
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L54
.L159:
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	jmp	.L54
.L168:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(2)
	fxch	%st(1)
.L54:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldt	.LC4(%rip)
	je	.L174
	fstp	%st(0)
	fldt	.LC5(%rip)
	fxch	%st(1)
	jmp	.L57
.L174:
	fxch	%st(1)
.L57:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L175
	fstp	%st(0)
	fldz
	fchs
	fxch	%st(1)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L149:
	fstp	%st(0)
	fstp	%st(2)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L176
	fldt	.LC2(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	ja	.L153
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L154
	fstp	%st(0)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L158:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L167:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L172:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L176:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L67
.L178:
	fstp	%st(0)
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L179:
	fstp	%st(0)
.L67:
	flds	.LC0(%rip)
	addq	$88, %rsp
	fld	%st(0)
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	fstp	%st(2)
	fxch	%st(1)
	fxch	%st(3)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L161:
	fxch	%st(3)
.L18:
	fxam
	fnstsw	%ax
	testb	$2, %ah
	fldt	.LC4(%rip)
	je	.L21
	fstp	%st(0)
	fldt	.LC5(%rip)
.L21:
	fld1
	fucomi	%st(5), %st
	fstp	%st(5)
	jnb	.L155
	fxch	%st(4)
	fucomip	%st(2), %st
	fstp	%st(1)
	jb	.L128
	fdivr	%st, %st(1)
	fdivrp	%st, %st(1)
	fld	%st(1)
	fld	%st(1)
	fxch	%st(3)
	fxch	%st(2)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L155:
	fstp	%st(2)
	fstp	%st(0)
	fxch	%st(1)
	fdivrp	%st, %st(2)
	fld	%st(0)
	fld	%st(2)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L128:
	fxch	%st(2)
	fstpt	16(%rsp)
	subq	$32, %rsp
	flds	.LC9(%rip)
	fld	%st(1)
	fstpt	32(%rsp)
	fmul	%st, %st(1)
	fxch	%st(1)
	fstpt	16(%rsp)
	fmulp	%st, %st(1)
	fstpt	(%rsp)
	call	__ieee754_hypotl@PLT
	fldt	32(%rsp)
	fdiv	%st(1), %st
	fdivp	%st, %st(1)
	fmuls	.LC10(%rip)
	fldt	48(%rsp)
	addq	$32, %rsp
	fld	%st(0)
	fld	%st(2)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L68:
	fld	%st(1)
	fld	%st(1)
	fld1
	flds	.LC11(%rip)
	fucomip	%st(3), %st
	ja	.L177
	fxch	%st(1)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L162:
	fxch	%st(1)
.L129:
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L131
	fld1
	fsub	%st(1), %st
	fld1
	faddp	%st, %st(2)
	fmulp	%st, %st(1)
	fxch	%st(1)
	fmul	%st(0), %st
	fsubrp	%st, %st(1)
	flds	.LC9(%rip)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L135:
	fstp	%st(3)
	fxch	%st(1)
	fxch	%st(2)
	fldz
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L136:
	fstp	%st(0)
	fxch	%st(1)
	fmuls	.LC16(%rip)
	subq	$16, %rsp
	fdivp	%st, %st(1)
	fstpt	(%rsp)
	call	__log1pl@PLT
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L151:
	fstp	%st(0)
	fldt	.LC5(%rip)
	jmp	.L1
.L154:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L178
	jne	.L179
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	fldz
	je	.L66
	fstp	%st(0)
	fldz
	fchs
.L66:
	flds	.LC0(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L131:
	flds	.LC12(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	flds	.LC9(%rip)
	jnb	.L34
	fldt	.LC17(%rip)
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jnb	.L34
	fld1
	fsub	%st(2), %st
	fstpt	(%rsp)
	fld1
	faddp	%st, %st(2)
	fldt	(%rsp)
	fmulp	%st, %st(2)
	fxch	%st(2)
	fmul	%st(0), %st
	fsubrp	%st, %st(1)
	fxch	%st(1)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L152:
	fstp	%st(0)
	fstp	%st(4)
	fxch	%st(1)
	fxch	%st(2)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	je	.L180
	fstp	%st(2)
	fxch	%st(1)
	flds	.LC14(%rip)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L180:
	fxch	%st(2)
.L40:
	fstpt	16(%rsp)
	fxch	%st(1)
	subq	$16, %rsp
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__ieee754_logl@PLT
	fldln2
	fsubp	%st, %st(1)
	fldt	32(%rsp)
	fmulp	%st, %st(1)
	fldt	16(%rsp)
	popq	%rcx
	popq	%rsi
	fld	%st(0)
	fld	%st(2)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	fstps	76(%rsp)
	fxch	%st(4)
	subq	$32, %rsp
	fstpt	80(%rsp)
	fxch	%st(4)
	fstpt	64(%rsp)
	fxch	%st(1)
	fstpt	48(%rsp)
	fstpt	32(%rsp)
	fxch	%st(1)
	fstpt	16(%rsp)
	fstpt	(%rsp)
	call	__x2y2m1l@PLT
	fchs
	addq	$32, %rsp
	fldt	(%rsp)
	fldt	16(%rsp)
	fldt	32(%rsp)
	flds	76(%rsp)
	fldt	48(%rsp)
	fxch	%st(4)
	fxch	%st(2)
	fxch	%st(5)
	fxch	%st(1)
	jmp	.L30
.L153:
	fstp	%st(0)
	flds	.LC0(%rip)
	fxch	%st(1)
	jmp	.L57
	.size	__catanl, .-__catanl
	.weak	catanf64x
	.set	catanf64x,__catanl
	.weak	catanl
	.set	catanl,__catanl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC3:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC4:
	.long	560513589
	.long	3373259426
	.long	16383
	.long	0
	.align 16
.LC5:
	.long	560513589
	.long	3373259426
	.long	49151
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	1627389952
	.align 4
.LC9:
	.long	1056964608
	.align 4
.LC10:
	.long	1048576000
	.align 4
.LC11:
	.long	528482304
	.align 4
.LC12:
	.long	1061158912
	.align 4
.LC13:
	.long	8388608
	.align 4
.LC14:
	.long	3204448256
	.align 4
.LC16:
	.long	1082130432
	.section	.rodata.cst16
	.align 16
.LC17:
	.long	0
	.long	2147483648
	.long	16382
	.long	0
