	.text
	.p2align 4,,15
	.globl	__lroundf
	.type	__lroundf, @function
__lroundf:
	movd	%xmm0, %esi
	movd	%xmm0, %edi
	movd	%xmm0, %edx
	shrl	$23, %esi
	sarl	$31, %edi
	movzbl	%sil, %esi
	orl	$1, %edi
	leal	-127(%rsi), %ecx
	cmpl	$62, %ecx
	jg	.L3
	testl	%ecx, %ecx
	js	.L11
	andl	$8388607, %edx
	orl	$8388608, %edx
	cmpl	$22, %ecx
	jg	.L12
	movl	$4194304, %eax
	sarl	%cl, %eax
	addl	%eax, %edx
	movl	$23, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	movslq	%edi, %rax
	shrl	%cl, %edx
	imulq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movss	%xmm0, -4(%rsp)
	cvttss2siq	-4(%rsp), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movslq	%edi, %rax
	cmpl	$-1, %ecx
	movl	$0, %edx
	cmovne	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leal	-150(%rsi), %ecx
	movl	%edx, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	movslq	%edi, %rax
	imulq	%rdx, %rax
	ret
	.size	__lroundf, .-__lroundf
	.weak	lroundf32
	.set	lroundf32,__lroundf
	.weak	lroundf
	.set	lroundf,__lroundf
