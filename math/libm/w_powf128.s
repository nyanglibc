	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__eqtf2
	.globl	__netf2
	.p2align 4,,15
	.globl	__powf128
	.type	__powf128, @function
__powf128:
	subq	$72, %rsp
	movaps	%xmm1, 48(%rsp)
	movaps	%xmm0, 32(%rsp)
	call	__ieee754_powf128@PLT
	movdqa	%xmm0, %xmm2
	movaps	%xmm0, (%rsp)
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L10
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L10
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L17
.L1:
	movdqa	(%rsp), %xmm0
	addq	$72, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movdqa	32(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L1
	movdqa	48(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L1
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	je	.L5
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movdqa	32(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L1
	pxor	%xmm1, %xmm1
	movdqa	32(%rsp), %xmm0
	call	__netf2@PLT
	testq	%rax, %rax
	je	.L1
	movdqa	48(%rsp), %xmm2
	pand	.LC0(%rip), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L1
.L5:
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__powf128, .-__powf128
	.weak	powf128
	.set	powf128,__powf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
