	.text
	.p2align 4,,15
	.globl	__nextupl
	.type	__nextupl, @function
__nextupl:
	movq	16(%rsp), %rcx
	movq	8(%rsp), %rdx
	movl	%ecx, %eax
	movq	%rdx, %rsi
	movl	%ecx, %r9d
	andw	$32767, %ax
	shrq	$32, %rsi
	movswl	%cx, %edi
	cmpw	$32767, %ax
	movl	%esi, %r8d
	je	.L30
	movl	%edx, %r10d
	cwtl
	orl	%esi, %r10d
	orl	%eax, %r10d
	jne	.L31
	fldt	.LC0(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	testl	%edi, %edi
	js	.L5
	fldt	8(%rsp)
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L1
	fstp	%st(0)
	addl	$1, %edx
	jne	.L7
	addl	$1, %r8d
	je	.L8
	testl	%edi, %edi
	jne	.L7
	cmpl	$-2147483648, %r8d
	jne	.L7
.L8:
	orl	$-2147483648, %r8d
	leal	1(%rcx), %r9d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%esi, %eax
	andl	$2147483647, %eax
	orl	%edx, %eax
	jne	.L32
	testl	%edi, %edi
	js	.L10
	fldt	8(%rsp)
	movl	$1, %edx
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	je	.L1
	fstp	%st(0)
.L7:
	movw	%r9w, -16(%rsp)
	movl	%r8d, -20(%rsp)
	movl	%edx, -24(%rsp)
	fldt	-24(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	fldt	8(%rsp)
	fadd	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%edx, %edx
	je	.L10
.L9:
	subl	$1, %edx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$-2147483648, %esi
	leal	-1(%rsi), %r8d
	setbe	%cl
	cmpl	$-32768, %edi
	setne	%al
	testb	%al, %cl
	je	.L9
	subl	$1, %edi
	movl	$-32768, %r9d
	testl	$32767, %edi
	je	.L9
	orl	$-2147483648, %r8d
	movl	%edi, %r9d
	jmp	.L9
	.size	__nextupl, .-__nextupl
	.weak	nextupf64x
	.set	nextupf64x,__nextupl
	.weak	nextupl
	.set	nextupl,__nextupl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	1
	.long	0
	.long	0
	.long	0
