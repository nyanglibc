	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__eqtf2
	.globl	__addtf3
	.p2align 4,,15
	.globl	__w_scalblnf128
	.type	__w_scalblnf128, @function
__w_scalblnf128:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	movdqa	.LC1(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L2
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L14
.L2:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm3
.L1:
	addq	$32, %rsp
	movdqa	%xmm3, %xmm0
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm0
	movq	%rbx, %rdi
	call	__scalblnf128@PLT
	movdqa	.LC0(%rip), %xmm2
	pand	%xmm0, %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	movdqa	.LC1(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	jne	.L6
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	jg	.L6
	pxor	%xmm1, %xmm1
	movdqa	%xmm3, %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	jne	.L1
.L6:
	movq	errno@gottpoff(%rip), %rax
	movdqa	%xmm3, %xmm0
	movl	$34, %fs:(%rax)
	addq	$32, %rsp
	popq	%rbx
	ret
	.size	__w_scalblnf128, .-__w_scalblnf128
	.weak	scalblnf128
	.set	scalblnf128,__w_scalblnf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
