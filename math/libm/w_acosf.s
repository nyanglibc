	.text
	.p2align 4,,15
	.globl	__acosf
	.type	__acosf, @function
__acosf:
	movaps	%xmm0, %xmm1
	andps	.LC0(%rip), %xmm1
	ucomiss	.LC1(%rip), %xmm1
	ja	.L4
	jmp	__ieee754_acosf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_acosf@PLT
	.size	__acosf, .-__acosf
	.weak	acosf32
	.set	acosf32,__acosf
	.weak	acosf
	.set	acosf,__acosf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
