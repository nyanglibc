	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__getf2
	.globl	__eqtf2
	.globl	__multf3
	.globl	__fixtfsi
	.globl	__floatsitf
	.globl	__lttf2
	.globl	__subtf3
	.p2align 4,,15
	.globl	__ccoshf128
	.type	__ccoshf128, @function
__ccoshf128:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$136, %rsp
	movdqa	160(%rsp), %xmm3
	pand	.LC3(%rip), %xmm3
	movdqa	176(%rsp), %xmm5
	movdqa	.LC3(%rip), %xmm4
	pand	%xmm5, %xmm4
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	movaps	%xmm5, 48(%rsp)
	movaps	%xmm4, (%rsp)
	movaps	%xmm3, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L2
	movdqa	.LC4(%rip), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L80
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L43
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L43
	movdqa	.LC5(%rip), %xmm6
	movdqa	(%rsp), %xmm0
	movdqa	%xmm6, %xmm1
	movaps	%xmm6, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L17
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L18
.L17:
	movdqa	32(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L70
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	176(%rsp), %xmm0
	call	__sincosf128@PLT
	movdqa	96(%rsp), %xmm4
	movdqa	.LC2(%rip), %xmm7
	movaps	%xmm4, 48(%rsp)
	movaps	%xmm7, 32(%rsp)
	movdqa	112(%rsp), %xmm1
.L36:
	movdqa	.LC10(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	.LC10(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	160(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	32(%rsp), %xmm0
	call	__copysignf128@PLT
	movdqa	%xmm0, %xmm1
	movdqa	16(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
.L35:
	movq	%rbx, %rax
	movdqa	16(%rsp), %xmm6
	movdqa	(%rsp), %xmm7
	movaps	%xmm6, (%rbx)
	movaps	%xmm7, 16(%rbx)
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movdqa	.LC5(%rip), %xmm5
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm5, %xmm1
	movaps	%xmm5, 32(%rsp)
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L4
	pxor	%xmm1, %xmm1
	movdqa	160(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	(%rsp), %xmm0
	testq	%rax, %rax
	movdqa	%xmm0, %xmm1
	jne	.L81
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L52
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L47
.L52:
	pxor	%xmm2, %xmm2
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L13
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L47
.L13:
	pxor	%xmm1, %xmm1
	movdqa	160(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L52
.L51:
	movdqa	.LC1(%rip), %xmm2
.L8:
	movdqa	176(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	movaps	%xmm2, (%rsp)
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L81:
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L51
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L51
.L47:
	movdqa	.LC6(%rip), %xmm1
	movdqa	.LC7(%rip), %xmm0
	call	__multf3@PLT
	call	__fixtfsi@PLT
	movdqa	32(%rsp), %xmm1
	movl	%eax, %ebp
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L67
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdi
	movdqa	176(%rsp), %xmm0
	call	__sincosf128@PLT
.L21:
	movl	%ebp, %edi
	call	__floatsitf@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L68
	movdqa	(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movaps	160(%rsp), %xmm2
	movmskps	%xmm2, %eax
	movaps	%xmm0, 48(%rsp)
	testb	$8, %al
	jne	.L24
	movdqa	96(%rsp), %xmm5
	movaps	%xmm5, 64(%rsp)
.L25:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC9(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	48(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	64(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm2
	movdqa	112(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm1
	movaps	%xmm0, 64(%rsp)
	movaps	%xmm0, 112(%rsp)
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L26
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	movdqa	80(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	48(%rsp), %xmm1
	movaps	%xmm0, 96(%rsp)
	movdqa	64(%rsp), %xmm0
	call	__multf3@PLT
	movaps	%xmm0, 112(%rsp)
.L26:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L69
	movdqa	.LC4(%rip), %xmm1
	movdqa	112(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, (%rsp)
	movdqa	96(%rsp), %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm6
	movaps	%xmm0, (%rsp)
	movaps	%xmm6, 16(%rsp)
.L30:
	movdqa	16(%rsp), %xmm0
	pand	.LC3(%rip), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L31
	movdqa	16(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
.L31:
	movdqa	(%rsp), %xmm0
	pand	.LC3(%rip), %xmm0
	movdqa	32(%rsp), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L35
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__multf3@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L68:
	movdqa	160(%rsp), %xmm0
	call	__ieee754_coshf128@PLT
	movdqa	112(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	160(%rsp), %xmm0
	call	__ieee754_sinhf128@PLT
	movdqa	96(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L24:
	movdqa	96(%rsp), %xmm0
	pxor	.LC8(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L69:
	movdqa	16(%rsp), %xmm0
	call	__ieee754_expf128@PLT
	movdqa	112(%rsp), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	16(%rsp), %xmm2
	movdqa	96(%rsp), %xmm1
	movdqa	%xmm2, %xmm0
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	movaps	%xmm0, (%rsp)
	movaps	%xmm2, 16(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L14
	movdqa	.LC4(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L14
	movdqa	.LC5(%rip), %xmm1
	movdqa	(%rsp), %xmm0
	call	__getf2@PLT
	testq	%rax, %rax
	jns	.L14
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L15
	movdqa	.LC1(%rip), %xmm5
	movaps	%xmm5, 16(%rsp)
	movaps	%xmm5, (%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L14:
	pxor	%xmm1, %xmm1
	movdqa	176(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	je	.L82
	movdqa	.LC1(%rip), %xmm2
	movaps	%xmm2, 16(%rsp)
	movaps	%xmm2, (%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L18:
	movdqa	160(%rsp), %xmm1
	movdqa	.LC2(%rip), %xmm0
	call	__copysignf128@PLT
	movdqa	176(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC10(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L67:
	movdqa	176(%rsp), %xmm6
	movdqa	.LC2(%rip), %xmm0
	movaps	%xmm6, 96(%rsp)
	movaps	%xmm0, 112(%rsp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L82:
	movdqa	.LC1(%rip), %xmm6
	movdqa	176(%rsp), %xmm4
	movaps	%xmm6, 16(%rsp)
	movaps	%xmm4, (%rsp)
	jmp	.L35
.L70:
	movdqa	.LC2(%rip), %xmm6
	movaps	%xmm6, 32(%rsp)
	movdqa	%xmm6, %xmm1
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L43:
	movdqa	176(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	.LC10(%rip), %xmm7
	movaps	%xmm0, (%rsp)
	movaps	%xmm7, 16(%rsp)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L15:
	movdqa	.LC1(%rip), %xmm2
	movdqa	176(%rsp), %xmm7
	movaps	%xmm2, 16(%rsp)
	movaps	%xmm7, (%rsp)
	jmp	.L35
	.size	__ccoshf128, .-__ccoshf128
	.weak	ccoshf128
	.set	ccoshf128,__ccoshf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC4:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	1074593784
	.align 16
.LC7:
	.long	1731200998
	.long	4082602951
	.long	804234142
	.long	1073636068
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC9:
	.long	0
	.long	0
	.long	0
	.long	1073610752
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	2147418112
