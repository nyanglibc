	.text
	.p2align 4,,15
	.globl	__nextafter
	.type	__nextafter, @function
__nextafter:
	movq	%xmm0, %rdx
	movq	%xmm1, %rcx
	movq	%xmm0, %rax
	movq	%xmm1, %r8
	shrq	$32, %rdx
	shrq	$32, %rcx
	movl	%edx, %edi
	movl	%ecx, %esi
	movl	%edx, %r9d
	andl	$2147483647, %edi
	andl	$2147483647, %esi
	cmpl	$2146435071, %edi
	jle	.L2
	leal	-2146435072(%rdi), %r10d
	orl	%eax, %r10d
	jne	.L3
.L2:
	cmpl	$2146435071, %esi
	jg	.L33
.L4:
	ucomisd	%xmm1, %xmm0
	jp	.L21
	je	.L1
.L21:
	orl	%eax, %edi
	je	.L34
	testl	%edx, %edx
	js	.L8
	cmpl	%ecx, %edx
	jg	.L14
	jne	.L17
.L31:
	cmpl	%r8d, %eax
	jbe	.L17
.L15:
	subl	$1, %eax
.L13:
	movl	%r9d, %edx
	andl	$2146435072, %edx
	cmpl	$2146435072, %edx
	jne	.L18
	addsd	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
.L19:
	movl	%eax, %eax
	salq	$32, %r9
	orq	%rax, %r9
	movq	%r9, -8(%rsp)
	movq	-8(%rsp), %xmm1
.L1:
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	subl	$2146435072, %esi
	orl	%r8d, %esi
	je	.L4
.L3:
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	andl	$-2147483648, %ecx
	salq	$32, %rcx
	orq	$1, %rcx
	movq	%rcx, -8(%rsp)
	movsd	-8(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	mulsd	%xmm0, %xmm0
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$1048575, %edx
	jg	.L19
	mulsd	%xmm0, %xmm0
	movq	errno@gottpoff(%rip), %rdx
	movl	$34, %fs:(%rdx)
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%ecx, %ecx
	js	.L35
	.p2align 4,,10
	.p2align 3
.L14:
	subl	$1, %edx
	testl	%eax, %eax
	cmove	%edx, %r9d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	%ecx, %edx
	jg	.L14
	cmpq	%rcx, %rdx
	je	.L31
	.p2align 4,,10
	.p2align 3
.L17:
	addl	$1, %edx
	addl	$1, %eax
	cmove	%edx, %r9d
	jmp	.L13
	.size	__nextafter, .-__nextafter
	.weak	nextafterf32x
	.set	nextafterf32x,__nextafter
	.weak	nextafterf64
	.set	nextafterf64,__nextafter
	.weak	nextafter
	.set	nextafter,__nextafter
