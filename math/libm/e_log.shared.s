	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
	.symver __ieee754_log,__log_finite@GLIBC_2.15
	.symver __log,log@@GLIBC_2.29
#NO_APP
	.p2align 4,,15
	.globl	__log
	.type	__log, @function
__log:
	movq	%xmm0, %rcx
	movq	%xmm0, %rdx
	movabsq	$-4606619468846596096, %rax
	movabsq	$854320534781951, %rsi
	addq	%rcx, %rax
	shrq	$48, %rdx
	cmpq	%rsi, %rax
	jbe	.L11
	leal	-16(%rdx), %eax
	cmpl	$32735, %eax
	ja	.L12
.L4:
	movabsq	$-4604367669032910848, %rax
	movabsq	$-4503599627370496, %rsi
	addq	%rcx, %rax
	pxor	%xmm3, %xmm3
	movq	%rax, %rdx
	andq	%rax, %rsi
	sarq	$52, %rax
	shrq	$45, %rdx
	subq	%rsi, %rcx
	leaq	__log_data(%rip), %rsi
	andl	$127, %edx
	movq	%rcx, -16(%rsp)
	leaq	137(%rdx), %rcx
	movsd	-16(%rsp), %xmm1
	cvtsi2sd	%eax, %xmm3
	addq	$9, %rdx
	salq	$4, %rcx
	salq	$4, %rdx
	addq	%rsi, %rcx
	addq	%rsi, %rdx
	subsd	(%rcx), %xmm1
	movsd	__log_data(%rip), %xmm2
	movsd	48+__log_data(%rip), %xmm0
	movsd	32+__log_data(%rip), %xmm6
	subsd	8(%rcx), %xmm1
	mulsd	%xmm3, %xmm2
	mulsd	8+__log_data(%rip), %xmm3
	mulsd	(%rdx), %xmm1
	addsd	8(%rdx), %xmm2
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm5
	movapd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm6
	addsd	%xmm2, %xmm5
	mulsd	%xmm1, %xmm4
	addsd	40+__log_data(%rip), %xmm0
	addsd	24+__log_data(%rip), %xmm6
	subsd	%xmm5, %xmm2
	mulsd	%xmm4, %xmm0
	addsd	%xmm6, %xmm0
	movapd	%xmm1, %xmm6
	addsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm6
	mulsd	16+__log_data(%rip), %xmm4
	addsd	%xmm3, %xmm1
	mulsd	%xmm6, %xmm0
	addsd	%xmm4, %xmm1
	addsd	%xmm1, %xmm0
	addsd	%xmm5, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movabsq	$4607182418800017408, %rax
	cmpq	%rax, %rcx
	je	.L8
	subsd	.LC1(%rip), %xmm0
	movsd	120+__log_data(%rip), %xmm2
	movsd	128+__log_data(%rip), %xmm3
	movsd	104+__log_data(%rip), %xmm5
	movsd	56+__log_data(%rip), %xmm7
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm4
	mulsd	%xmm0, %xmm1
	movapd	%xmm0, %xmm6
	addsd	112+__log_data(%rip), %xmm2
	mulsd	%xmm1, %xmm3
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm5
	mulsd	80+__log_data(%rip), %xmm1
	addsd	%xmm3, %xmm2
	movsd	136+__log_data(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm2
	movsd	96+__log_data(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm4, %xmm2
	addsd	88+__log_data(%rip), %xmm3
	addsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm2
	movsd	72+__log_data(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm4, %xmm2
	addsd	64+__log_data(%rip), %xmm3
	addsd	%xmm3, %xmm1
	movapd	%xmm0, %xmm3
	addsd	%xmm1, %xmm2
	movsd	.LC2(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	mulsd	%xmm4, %xmm2
	movapd	%xmm0, %xmm4
	addsd	%xmm1, %xmm4
	subsd	%xmm1, %xmm4
	movapd	%xmm0, %xmm1
	movapd	%xmm4, %xmm5
	subsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm5
	addsd	%xmm4, %xmm0
	mulsd	%xmm7, %xmm3
	mulsd	%xmm7, %xmm5
	mulsd	%xmm3, %xmm0
	addsd	%xmm5, %xmm1
	subsd	%xmm1, %xmm6
	addsd	%xmm6, %xmm5
	addsd	%xmm5, %xmm0
	addsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%xmm0, %rax
	addq	%rax, %rax
	je	.L13
	movabsq	$9218868437227405312, %rax
	cmpq	%rax, %rcx
	je	.L1
	testb	$-128, %dh
	jne	.L6
	andl	$32752, %edx
	cmpl	$32752, %edx
	je	.L6
	mulsd	.LC3(%rip), %xmm0
	movabsq	$-234187180623265792, %rax
	movq	%xmm0, %rcx
	addq	%rax, %rcx
	jmp	.L4
.L8:
	pxor	%xmm0, %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$1, %edi
	jmp	__math_divzero
	.p2align 4,,10
	.p2align 3
.L6:
	jmp	__math_invalid
	.size	__log, .-__log
	.weak	logf32x
	.set	logf32x,__log
	.weak	logf64
	.set	logf64,__log
	.globl	__ieee754_log
	.set	__ieee754_log,__log
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1072693248
	.align 8
.LC2:
	.long	0
	.long	1101004800
	.align 8
.LC3:
	.long	0
	.long	1127219200
	.hidden	__math_invalid
	.hidden	__math_divzero
	.hidden	__log_data
