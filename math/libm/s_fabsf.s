	.text
	.p2align 4,,15
	.globl	__fabsf
	.type	__fabsf, @function
__fabsf:
	andps	.LC0(%rip), %xmm0
	ret
	.size	__fabsf, .-__fabsf
	.weak	fabsf32
	.set	fabsf32,__fabsf
	.weak	fabsf
	.set	fabsf,__fabsf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
