	.text
	.p2align 4,,15
	.globl	__fesetround
	.type	__fesetround, @function
__fesetround:
	movl	%edi, %eax
	andl	$-3073, %eax
	jne	.L3
#APP
# 32 "../sysdeps/x86_64/fpu/fesetround.c" 1
	fnstcw -6(%rsp)
# 0 "" 2
#NO_APP
	movzwl	-6(%rsp), %edx
	andb	$-13, %dh
	orl	%edi, %edx
	movw	%dx, -6(%rsp)
#APP
# 35 "../sysdeps/x86_64/fpu/fesetround.c" 1
	fldcw -6(%rsp)
# 0 "" 2
# 39 "../sysdeps/x86_64/fpu/fesetround.c" 1
	stmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	movl	-4(%rsp), %edx
	sall	$3, %edi
	andb	$-97, %dh
	orl	%edx, %edi
	movl	%edi, -4(%rsp)
#APP
# 42 "../sysdeps/x86_64/fpu/fesetround.c" 1
	ldmxcsr -4(%rsp)
# 0 "" 2
#NO_APP
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %eax
	ret
	.size	__fesetround, .-__fesetround
	.weak	fesetround
	.set	fesetround,__fesetround
