	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__tgammal
	.type	__tgammal, @function
__tgammal:
	subq	$40, %rsp
	leaq	28(%rsp), %rdi
	pushq	56(%rsp)
	pushq	56(%rsp)
	call	__ieee754_gammal_r@PLT
	fld	%st(0)
	popq	%rdx
	fabs
	popq	%rcx
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L3
	je	.L2
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L30:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L3:
	movl	28(%rsp), %eax
	testl	%eax, %eax
	jns	.L26
	fchs
.L26:
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fldt	48(%rsp)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L25
	fldt	48(%rsp)
	fxam
	fnstsw	%ax
	andb	$69, %ah
	cmpb	$5, %ah
	jne	.L30
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L3
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L3
	fldt	48(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	frndint
	fldcw	14(%rsp)
	fldt	48(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jp	.L11
	je	.L31
	jmp	.L11
.L35:
	fstp	%st(0)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L36:
	fstp	%st(0)
.L11:
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L32
	jne	.L33
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L25:
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L3
	fldz
	fldt	48(%rsp)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L34
	je	.L29
	fstp	%st(0)
	jmp	.L8
.L34:
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L8:
	fldt	48(%rsp)
	fnstcw	14(%rsp)
	movzwl	14(%rsp), %eax
	andb	$-13, %ah
	orb	$4, %ah
	movw	%ax, 12(%rsp)
	fldcw	12(%rsp)
	frndint
	fldcw	14(%rsp)
	fldt	48(%rsp)
	fxch	%st(1)
	fucomip	%st(1), %st
	jp	.L35
	jne	.L36
	fldz
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L11
	fstp	%st(0)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L31:
	fstp	%st(0)
.L16:
	pushq	56(%rsp)
	pushq	56(%rsp)
	movl	$241, %edi
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L29:
	fstp	%st(1)
	subq	$16, %rsp
	movl	$250, %edi
	fstpt	(%rsp)
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L26
.L32:
	fstp	%st(0)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L33:
	fstp	%st(0)
.L14:
	pushq	56(%rsp)
	pushq	56(%rsp)
	movl	$240, %edi
	pushq	72(%rsp)
	pushq	72(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L26
	.size	__tgammal, .-__tgammal
	.weak	tgammaf64x
	.set	tgammaf64x,__tgammal
	.weak	tgammal
	.set	tgammal,__tgammal
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
