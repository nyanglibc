	.text
	.globl	__subtf3
	.globl	__unordtf2
	.globl	__letf2
	.p2align 4,,15
	.globl	__sincosf128
	.type	__sincosf128, @function
__sincosf128:
	pushq	%rbp
	pushq	%rbx
	movabsq	$9223372036854775807, %rdx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	andq	%rdx, %rax
	movabsq	$4611283733356757713, %rdx
	cmpq	%rdx, %rax
	jle	.L13
	movabsq	$9223090561878065151, %rdx
	cmpq	%rdx, %rax
	jle	.L4
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__subtf3@PLT
	movdqa	(%rsp), %xmm2
	pand	.LC1(%rip), %xmm2
	movaps	%xmm0, 0(%rbp)
	movaps	%xmm0, (%rbx)
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	movdqa	.LC2(%rip), %xmm1
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L1
	movdqa	(%rsp), %xmm2
	movdqa	.LC2(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__letf2@PLT
	testq	%rax, %rax
	jle	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	16(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__ieee754_rem_pio2f128@PLT
	andl	$3, %eax
	movl	$1, %edx
	cmpl	$1, %eax
	je	.L6
	cmpl	$2, %eax
	je	.L7
	testl	%eax, %eax
	je	.L14
	movdqa	16(%rsp), %xmm0
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movdqa	32(%rsp), %xmm1
	call	__kernel_sincosf128@PLT
	movdqa	(%rbx), %xmm0
	pxor	.LC3(%rip), %xmm0
	movaps	%xmm0, (%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	pxor	%xmm1, %xmm1
	xorl	%edx, %edx
	call	__kernel_sincosf128@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movdqa	16(%rsp), %xmm0
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movdqa	32(%rsp), %xmm1
	call	__kernel_sincosf128@PLT
	movdqa	0(%rbp), %xmm0
	pxor	.LC3(%rip), %xmm0
	movaps	%xmm0, 0(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	32(%rsp), %xmm1
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movdqa	16(%rsp), %xmm0
	call	__kernel_sincosf128@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movdqa	32(%rsp), %xmm1
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movdqa	16(%rsp), %xmm0
	call	__kernel_sincosf128@PLT
	movdqa	(%rbx), %xmm1
	movdqa	.LC3(%rip), %xmm2
	pxor	%xmm2, %xmm1
	movaps	%xmm1, (%rbx)
	movdqa	0(%rbp), %xmm0
	pxor	%xmm2, %xmm0
	movaps	%xmm0, 0(%rbp)
	jmp	.L1
	.size	__sincosf128, .-__sincosf128
	.weak	sincosf128
	.set	sincosf128,__sincosf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
