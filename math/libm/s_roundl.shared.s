	.text
	.p2align 4,,15
	.globl	__roundl
	.type	__roundl, @function
__roundl:
	movl	16(%rsp), %edx
	movq	8(%rsp), %rax
	movq	%rdx, %rcx
	movq	%rax, %r8
	movswl	%dx, %r9d
	andl	$32767, %ecx
	shrq	$32, %r8
	movq	%rax, %rdi
	leal	-16383(%rcx), %esi
	movl	%r8d, %r10d
	cmpl	$30, %esi
	jg	.L2
	testl	%esi, %esi
	js	.L27
	movl	$2147483647, %r11d
	movl	%esi, %ecx
	movq	%rax, -40(%rsp)
	sarl	%cl, %r11d
	movl	%edx, -32(%rsp)
	andl	%r11d, %r8d
	orl	%eax, %r8d
	fldt	-40(%rsp)
	je	.L1
	fstp	%st(0)
	movl	$1073741824, %eax
	notl	%r11d
	xorl	%edx, %edx
	sarl	%cl, %eax
	addl	%r10d, %eax
	movl	%r11d, %r10d
	setc	%dl
	andl	%eax, %r10d
	xorl	%eax, %eax
	addl	%edx, %r9d
	orl	$-2147483648, %r10d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$62, %esi
	jle	.L9
	movq	%rax, -40(%rsp)
	movl	%edx, -32(%rsp)
	cmpl	$16384, %esi
	fldt	-40(%rsp)
	je	.L28
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	subl	$16414, %ecx
	movl	$-1, %r8d
	movq	%rax, -40(%rsp)
	shrl	%cl, %r8d
	movl	%edx, -32(%rsp)
	testl	%eax, %r8d
	fldt	-40(%rsp)
	je	.L1
	fstp	%st(0)
	movl	$62, %ecx
	movl	$1, %eax
	subl	%esi, %ecx
	sall	%cl, %eax
	addl	%edi, %eax
	jc	.L29
.L12:
	notl	%r8d
	andl	%r8d, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	andl	$32768, %r9d
	xorl	%r10d, %r10d
	xorl	%eax, %eax
	cmpl	$-1, %esi
	jne	.L4
	orl	$16383, %r9d
	movl	$-2147483648, %r10d
.L4:
	movw	%r9w, -16(%rsp)
	movl	%r10d, -20(%rsp)
	movl	%eax, -24(%rsp)
	fldt	-24(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	fadd	%st(0), %st
	ret
.L29:
	addl	$1, %r10d
	jnc	.L12
	addl	$1, %r9d
	movl	$-2147483648, %r10d
	jmp	.L12
	.size	__roundl, .-__roundl
	.weak	roundf64x
	.set	roundf64x,__roundl
	.weak	roundl
	.set	roundl,__roundl
