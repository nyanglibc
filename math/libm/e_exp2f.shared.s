	.text
#APP
	.symver __ieee754_exp2f,__exp2f_finite@GLIBC_2.15
	.symver __exp2f,exp2f@@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.globl	__exp2f
	.type	__exp2f, @function
__exp2f:
	movd	%xmm0, %eax
	pxor	%xmm2, %xmm2
	movd	%xmm0, %edx
	shrl	$20, %eax
	andl	$2047, %eax
	cvtss2sd	%xmm0, %xmm2
	cmpl	$1071, %eax
	ja	.L17
.L2:
	movsd	256+__exp2f_data(%rip), %xmm1
	leaq	__exp2f_data(%rip), %rdx
	movsd	.LC3(%rip), %xmm3
	movapd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	movq	%xmm0, %rax
	subsd	%xmm1, %xmm0
	movsd	264+__exp2f_data(%rip), %xmm1
	movq	%rax, %rcx
	salq	$47, %rax
	andl	$31, %ecx
	addq	(%rdx,%rcx,8), %rax
	subsd	%xmm0, %xmm2
	movsd	280+__exp2f_data(%rip), %xmm0
	movq	%rax, -16(%rsp)
	mulsd	%xmm2, %xmm1
	mulsd	%xmm2, %xmm0
	mulsd	%xmm2, %xmm2
	addsd	272+__exp2f_data(%rip), %xmm1
	addsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	addsd	%xmm3, %xmm0
	mulsd	-16(%rsp), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$-8388608, %edx
	je	.L10
	cmpl	$2039, %eax
	ja	.L18
	ucomiss	.LC0(%rip), %xmm0
	ja	.L19
	movss	.LC1(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jnb	.L20
	movss	.LC2(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.L2
	xorl	%edi, %edi
	jmp	__math_may_uflowf
	.p2align 4,,10
	.p2align 3
.L18:
	addss	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	pxor	%xmm0, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%edi, %edi
	jmp	__math_oflowf
.L20:
	xorl	%edi, %edi
	jmp	__math_uflowf
	.size	__exp2f, .-__exp2f
	.weak	exp2f32
	.set	exp2f32,__exp2f
	.globl	__ieee754_exp2f
	.set	__ieee754_exp2f,__exp2f
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	0
	.align 4
.LC1:
	.long	3272998912
	.align 4
.LC2:
	.long	3272933376
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC3:
	.long	0
	.long	1072693248
	.hidden	__math_uflowf
	.hidden	__math_oflowf
	.hidden	__math_may_uflowf
	.hidden	__exp2f_data
