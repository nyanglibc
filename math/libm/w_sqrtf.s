	.text
	.p2align 4,,15
	.globl	__sqrtf
	.type	__sqrtf, @function
__sqrtf:
	pxor	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	ja	.L4
	jmp	__ieee754_sqrtf@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	__ieee754_sqrtf@PLT
	.size	__sqrtf, .-__sqrtf
	.weak	sqrtf32
	.set	sqrtf32,__sqrtf
	.weak	sqrtf
	.set	sqrtf,__sqrtf
