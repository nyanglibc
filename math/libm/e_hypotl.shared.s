	.text
#APP
	.symver __ieee754_hypotl,__hypotl_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_hypotl
	.type	__ieee754_hypotl, @function
__ieee754_hypotl:
	pushq	%rbx
	subq	$64, %rsp
	movl	88(%rsp), %r10d
	movl	104(%rsp), %edi
	movq	80(%rsp), %r9
	movq	96(%rsp), %rsi
	movq	%r10, %rax
	movq	%rdi, %rbx
	andl	$32767, %eax
	andl	$32767, %ebx
	cmpl	%ebx, %eax
	jnb	.L2
	movq	%r9, (%rsp)
	movl	%r10d, 8(%rsp)
	movq	%rsi, %r9
	movl	%edi, %r10d
	movq	(%rsp), %rsi
	movl	8(%rsp), %edi
	movl	%eax, %edx
	movl	%ebx, %eax
	movl	%edx, %ebx
.L2:
	movl	%r10d, 56(%rsp)
	movq	%r9, 48(%rsp)
	movl	%eax, %edx
	movw	%ax, 56(%rsp)
	subl	%ebx, %edx
	fldt	48(%rsp)
	movl	%edi, 56(%rsp)
	movq	%rsi, 48(%rsp)
	cmpl	$70, %edx
	movw	%bx, 56(%rsp)
	fld	%st(0)
	fstpt	16(%rsp)
	fldt	48(%rsp)
	fld	%st(0)
	fstpt	(%rsp)
	ja	.L25
	fstp	%st(0)
	fstp	%st(0)
	xorl	%edx, %edx
	cmpl	$24383, %eax
	ja	.L26
.L5:
	cmpl	$8382, %ebx
	jbe	.L27
	jmp	.L8
.L30:
	fstp	%st(0)
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L8:
	fldt	16(%rsp)
	fldt	(%rsp)
	fsubr	%st, %st(1)
	fxch	%st(1)
	fucomi	%st(1), %st
	ja	.L28
	fstp	%st(1)
	fldt	16(%rsp)
	movq	(%rsp), %rcx
	movw	%bx, 56(%rsp)
	addl	$1, %eax
	movl	$0, 48(%rsp)
	shrq	$32, %rcx
	movl	%ecx, 52(%rsp)
	fadd	%st(0), %st
	fld	%st(0)
	fstpt	16(%rsp)
	fldt	48(%rsp)
	movw	%ax, 56(%rsp)
	movq	16(%rsp), %rax
	shrq	$32, %rax
	movl	%eax, 52(%rsp)
	fldt	48(%rsp)
	fld	%st(1)
	fmul	%st(1), %st
	fld	%st(4)
	fchs
	fmulp	%st, %st(5)
	fldt	(%rsp)
	fsub	%st, %st(3)
	fxch	%st(3)
	fmul	%st(2), %st
	fxch	%st(4)
	fsubp	%st, %st(2)
	fxch	%st(1)
	fmulp	%st, %st(2)
	fxch	%st(2)
	faddp	%st, %st(1)
	fsubrp	%st, %st(2)
	fsubp	%st, %st(1)
	fsqrt
.L12:
	testl	%edx, %edx
	je	.L1
	fld1
	addw	$16383, %dx
	fstpt	48(%rsp)
	movw	%dx, 56(%rsp)
	fldt	48(%rsp)
	fmulp	%st, %st(1)
	fldt	.LC1(%rip)
	fucomip	%st(1), %st
	jbe	.L1
	fld	%st(0)
	fmul	%st(1), %st
	fstp	%st(0)
.L1:
	addq	$64, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	fstp	%st(0)
	movw	%ax, 56(%rsp)
	movq	16(%rsp), %rax
	movl	$0, 48(%rsp)
	shrq	$32, %rax
	movl	%eax, 52(%rsp)
	fldt	48(%rsp)
	fld	%st(0)
	fmul	%st(1), %st
	fld	%st(2)
	fchs
	fmulp	%st, %st(3)
	fldt	16(%rsp)
	fadd	%st(2), %st
	fldt	16(%rsp)
	fsubp	%st, %st(3)
	fmulp	%st, %st(2)
	fxch	%st(2)
	fsubp	%st, %st(1)
	fsubrp	%st, %st(1)
	fsqrt
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$64, %rsp
	popq	%rbx
	faddp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$32767, %eax
	je	.L29
	fldt	16(%rsp)
	subl	$9600, %eax
	subl	$9600, %ebx
	movl	$9600, %edx
	fstpt	48(%rsp)
	movw	%ax, 56(%rsp)
	fldt	48(%rsp)
	fstpt	16(%rsp)
	fldt	(%rsp)
	fstpt	48(%rsp)
	movw	%bx, 56(%rsp)
	fldt	48(%rsp)
	fstpt	(%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L27:
	testl	%ebx, %ebx
	jne	.L9
	movq	(%rsp), %rax
	fldt	16(%rsp)
	movq	%rax, %rcx
	shrq	$32, %rcx
	movl	%ecx, %edi
	orl	%eax, %edi
	je	.L1
	fstp	%st(0)
	movabsq	$-9223372036854775808, %rax
	subl	$16382, %edx
	movq	%rax, 48(%rsp)
	movl	$32765, %eax
	movw	%ax, 56(%rsp)
	fldt	48(%rsp)
	fldt	(%rsp)
	fmul	%st(1), %st
	fld	%st(0)
	fstpt	(%rsp)
	movq	8(%rsp), %rbx
	fldt	16(%rsp)
	movswl	%bx, %ebx
	fmulp	%st, %st(2)
	fxch	%st(1)
	fld	%st(0)
	fstpt	16(%rsp)
	movq	24(%rsp), %rax
	cwtl
	cmpl	%ebx, %eax
	jnb	.L30
	fxch	%st(1)
	fstpt	16(%rsp)
	movl	%eax, %ecx
	movl	%ebx, %eax
	movl	%ecx, %ebx
	fstpt	(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	fldt	16(%rsp)
	addl	$9600, %eax
	addl	$9600, %ebx
	subl	$9600, %edx
	fstpt	48(%rsp)
	movw	%ax, 56(%rsp)
	fldt	48(%rsp)
	fstpt	16(%rsp)
	fldt	(%rsp)
	fstpt	48(%rsp)
	movw	%bx, 56(%rsp)
	fldt	48(%rsp)
	fstpt	(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L29:
	fldt	16(%rsp)
	fldt	(%rsp)
	faddp	%st, %st(1)
	fstpt	32(%rsp)
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__GI___issignalingl
	popq	%rsi
	popq	%rdi
	testl	%eax, %eax
	fldt	32(%rsp)
	jne	.L1
	fstp	%st(0)
	pushq	8(%rsp)
	pushq	8(%rsp)
	call	__GI___issignalingl
	popq	%rdx
	popq	%rcx
	testl	%eax, %eax
	fldt	32(%rsp)
	jne	.L1
	movq	16(%rsp), %rdx
	movq	%rdx, %rax
	shrq	$32, %rax
	andl	$2147483647, %eax
	orl	%edx, %eax
	movq	(%rsp), %rdx
	fldt	16(%rsp)
	movq	%rdx, %rax
	fcmovne	%st(1), %st
	fstp	%st(1)
	shrq	$32, %rax
	xorl	$32767, %ebx
	andl	$2147483647, %eax
	orl	%edx, %eax
	orl	%ebx, %eax
	fldt	(%rsp)
	fcmovne	%st(1), %st
	fstp	%st(1)
	jmp	.L1
	.size	__ieee754_hypotl, .-__ieee754_hypotl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	2147483648
	.long	1
	.long	0
