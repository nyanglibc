	.text
	.p2align 4,,15
	.type	fromfp_domain_error, @function
fromfp_domain_error:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	$1, %edi
	movl	%esi, %ebx
	subq	$8, %rsp
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.L1
	leal	-1(%rbx), %ecx
	movl	$1, %eax
	salq	%cl, %rax
	movq	%rax, %rdx
	subq	$1, %rax
	negq	%rdx
	testb	%bpl, %bpl
	cmovne	%rdx, %rax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	fromfp_domain_error, .-fromfp_domain_error
	.p2align 4,,15
	.globl	__fromfpf128
	.type	__fromfpf128, @function
__fromfpf128:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$64, %esi
	movaps	%xmm0, (%rsp)
	ja	.L37
	testl	%esi, %esi
	jne	.L10
	movl	$1, %edi
	call	feraiseexcept@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$64, %esi
.L10:
	movq	8(%rsp), %r9
	movq	(%rsp), %rdx
	movabsq	$9223372036854775807, %rcx
	andq	%r9, %rcx
	movq	%rcx, %rax
	orq	%rdx, %rax
	je	.L38
	movq	%r9, %rbx
	shrq	$48, %rcx
	shrq	$63, %rbx
	leal	-16383(%rcx), %r10d
	movq	%rcx, %r8
	leal	-2(%rsi,%rbx), %r11d
	cmpl	%r11d, %r10d
	jg	.L34
	cmpl	$-1, %r10d
	jl	.L15
	movabsq	$281474976710655, %rax
	movl	$112, %ebp
	movabsq	$281474976710656, %rcx
	andq	%r9, %rax
	subl	%r10d, %ebp
	orq	%rcx, %rax
	cmpl	$64, %ebp
	jg	.L16
	movl	$111, %ecx
	movl	$1, %r12d
	subl	%r10d, %ecx
	salq	%cl, %r12
	testq	%rdx, %r12
	movq	%r12, %rcx
	setne	%r12b
	subq	$1, %rcx
	testq	%rdx, %rcx
	leal	-16431(%r8), %ecx
	setne	%r13b
	salq	%cl, %rax
	movl	%ebp, %ecx
	shrq	%cl, %rdx
	orq	%rax, %rdx
	cmpl	$64, %ebp
	cmovne	%rdx, %rax
.L17:
	cmpl	$1, %edi
	je	.L19
	jle	.L70
	cmpl	$3, %edi
	je	.L22
	cmpl	$4, %edi
	jne	.L18
	testb	%r12b, %r12b
	je	.L18
	testb	$1, %al
	jne	.L46
	testb	%r13b, %r13b
	je	.L18
.L46:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%r9, %r9
	jns	.L27
.L24:
	cmpl	%r11d, %r10d
	je	.L71
.L33:
	negq	%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%eax, %eax
.L9:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$1, %edi
	je	.L41
	jle	.L72
	xorl	%eax, %eax
	cmpl	$3, %edi
	je	.L18
	cmpl	$4, %edi
	je	.L18
.L39:
	xorl	%eax, %eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$1, %edx
	movl	%r10d, %ecx
	salq	%cl, %rdx
	cmpq	%rax, %rdx
	je	.L33
.L34:
	addq	$24, %rsp
	movl	%ebx, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	fromfp_domain_error
	.p2align 4,,10
	.p2align 3
.L72:
	testl	%edi, %edi
	jne	.L39
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%eax, %eax
.L21:
	testq	%r9, %r9
	js	.L24
	testb	%r12b, %r12b
	jne	.L44
	testb	%r13b, %r13b
	je	.L27
.L44:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L27:
	leal	1(%r11), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rdx, %rax
	jne	.L9
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$47, %ecx
	movl	$1, %r8d
	subl	%r10d, %ecx
	salq	%cl, %r8
	testq	%r8, %rax
	movq	%r8, %rcx
	setne	%r12b
	subq	$1, %rcx
	andq	%rax, %rcx
	orq	%rdx, %rcx
	movl	$48, %ecx
	setne	%r13b
	subl	%r10d, %ecx
	shrq	%cl, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$1, %r13d
	xorl	%r12d, %r12d
	xorl	%eax, %eax
.L19:
	testq	%r9, %r9
	jns	.L27
	testb	%r12b, %r12b
	jne	.L45
	testb	%r13b, %r13b
	je	.L24
.L45:
	addq	$1, %rax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L70:
	testl	%edi, %edi
	je	.L21
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L22:
	movzbl	%r12b, %r12d
	addq	%r12, %rax
	jmp	.L18
	.size	__fromfpf128, .-__fromfpf128
	.weak	fromfpf128
	.set	fromfpf128,__fromfpf128
