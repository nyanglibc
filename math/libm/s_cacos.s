	.text
	.p2align 4,,15
	.globl	__cacos
	.type	__cacos, @function
__cacos:
	subq	$24, %rsp
	movapd	%xmm0, %xmm3
	movq	.LC1(%rip), %xmm2
	movapd	%xmm0, %xmm4
	andpd	%xmm2, %xmm3
	ucomisd	%xmm3, %xmm3
	jp	.L11
	ucomisd	.LC2(%rip), %xmm3
	movl	$1, %eax
	ja	.L2
	ucomisd	.LC3(%rip), %xmm3
	movl	$4, %eax
	jnb	.L2
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm0
	jp	.L14
	movl	$2, %eax
	jne	.L14
	.p2align 4,,10
	.p2align 3
.L2:
	andpd	%xmm1, %xmm2
	ucomisd	%xmm2, %xmm2
	jp	.L25
	ucomisd	.LC2(%rip), %xmm2
	jbe	.L22
.L25:
	pxor	%xmm3, %xmm3
.L4:
	movapd	%xmm4, %xmm0
	movsd	%xmm3, 8(%rsp)
	call	__casin@PLT
	movsd	.LC4(%rip), %xmm2
	movsd	8(%rsp), %xmm3
	subsd	%xmm0, %xmm2
	ucomisd	%xmm3, %xmm2
	movapd	%xmm2, %xmm0
	jp	.L8
	movq	%xmm3, %rax
	movq	%xmm2, %rdx
	cmovne	%rdx, %rax
	movq	%rax, 8(%rsp)
	movsd	8(%rsp), %xmm0
.L8:
	xorpd	.LC5(%rip), %xmm1
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	ucomisd	.LC3(%rip), %xmm2
	jnb	.L5
	pxor	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm1
	jp	.L5
	jne	.L5
	cmpl	$1, %eax
	jle	.L4
	cmpl	$2, %eax
	je	.L4
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$1, %eax
	jle	.L25
.L7:
	movapd	%xmm1, %xmm0
	movapd	%xmm4, %xmm1
	movl	$1, %edi
	xorpd	.LC5(%rip), %xmm0
	call	__kernel_casinh@PLT
	movapd	%xmm0, %xmm2
	addq	$24, %rsp
	movapd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$3, %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	jmp	.L2
	.size	__cacos, .-__cacos
	.weak	cacosf32x
	.set	cacosf32x,__cacos
	.weak	cacosf64
	.set	cacosf64,__cacos
	.weak	cacos
	.set	cacos,__cacos
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.align 8
.LC3:
	.long	0
	.long	1048576
	.align 8
.LC4:
	.long	1413754136
	.long	1073291771
	.section	.rodata.cst16
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
