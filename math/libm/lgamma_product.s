	.text
	.p2align 4,,15
	.globl	__lgamma_product
	.type	__lgamma_product, @function
__lgamma_product:
	movsd	%xmm1, -24(%rsp)
	testl	%edi, %edi
	fldl	-24(%rsp)
	movsd	%xmm2, -24(%rsp)
	fldl	-24(%rsp)
	faddp	%st, %st(1)
	jle	.L4
	movsd	%xmm0, -24(%rsp)
	xorl	%eax, %eax
	fldl	-24(%rsp)
	fldz
	fld1
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%eax, -24(%rsp)
	addl	$1, %eax
	fildl	-24(%rsp)
	cmpl	%eax, %edi
	fadd	%st(4), %st
	fdivr	%st(3), %st
	fld	%st(2)
	fadd	%st(2), %st
	fmulp	%st, %st(1)
	faddp	%st, %st(2)
	jne	.L3
	fstp	%st(0)
	fstp	%st(1)
	fstp	%st(1)
	fstpl	-16(%rsp)
	movsd	-16(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	fstp	%st(0)
	pxor	%xmm0, %xmm0
	ret
	.size	__lgamma_product, .-__lgamma_product
