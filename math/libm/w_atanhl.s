	.text
	.p2align 4,,15
	.globl	__atanhl
	.type	__atanhl, @function
__atanhl:
	fldt	8(%rsp)
	fld	%st(0)
	fabs
	fld1
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L7
.L2:
	fstpt	8(%rsp)
	jmp	__ieee754_atanhl@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rax
	jp	.L3
	je	.L8
.L3:
	movl	$33, %fs:(%rax)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:(%rax)
	jmp	.L2
	.size	__atanhl, .-__atanhl
	.weak	atanhf64x
	.set	atanhf64x,__atanhl
	.weak	atanhl
	.set	atanhl,__atanhl
