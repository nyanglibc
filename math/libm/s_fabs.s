	.text
	.p2align 4,,15
	.globl	__fabs
	.type	__fabs, @function
__fabs:
	andpd	.LC0(%rip), %xmm0
	ret
	.size	__fabs, .-__fabs
	.weak	fabsf32x
	.set	fabsf32x,__fabs
	.weak	fabsf64
	.set	fabsf64,__fabs
	.weak	fabs
	.set	fabs,__fabs
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
