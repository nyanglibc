	.text
	.p2align 4,,15
	.globl	__hypotl
	.type	__hypotl, @function
__hypotl:
	subq	$8, %rsp
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__ieee754_hypotl@PLT
	fld	%st(0)
	addq	$32, %rsp
	fabs
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L7
	fstp	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	fstp	%st(0)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fldt	16(%rsp)
	fabs
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L8
	fldt	32(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	__hypotl, .-__hypotl
	.weak	hypotf64x
	.set	hypotf64x,__hypotl
	.weak	hypotl
	.set	hypotl,__hypotl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
