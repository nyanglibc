	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__carg
	.type	__carg, @function
__carg:
	movapd	%xmm0, %xmm2
	movapd	%xmm1, %xmm0
	movapd	%xmm2, %xmm1
	jmp	__atan2@PLT
	.size	__carg, .-__carg
	.weak	cargf32x
	.set	cargf32x,__carg
	.weak	cargf64
	.set	cargf64,__carg
	.weak	carg
	.set	carg,__carg
