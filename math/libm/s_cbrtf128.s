	.text
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__multf3
	.globl	__subtf3
	.globl	__divtf3
	.p2align 4,,15
	.globl	__cbrtf128
	.type	__cbrtf128, @function
__cbrtf128:
	pushq	%rbp
	pushq	%rbx
	movdqa	%xmm0, %xmm2
	subq	$56, %rsp
	pand	.LC0(%rip), %xmm2
	movaps	%xmm0, (%rsp)
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, 16(%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L14
	movdqa	16(%rsp), %xmm2
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm2, %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jg	.L14
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	(%rsp), %xmm3
	testq	%rax, %rax
	movdqa	%xmm3, %xmm0
	je	.L1
	pxor	%xmm1, %xmm1
	movl	$1, %ebp
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L19
.L5:
	leaq	44(%rsp), %rdi
	movdqa	(%rsp), %xmm0
	call	__frexpf128@PLT
	movdqa	.LC4(%rip), %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC6(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC7(%rip), %xmm1
	call	__subtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC8(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm1
	call	__multf3@PLT
	movdqa	.LC9(%rip), %xmm1
	call	__addtf3@PLT
	movl	44(%rsp), %ecx
	testl	%ecx, %ecx
	js	.L6
	movl	%ecx, %eax
	movl	$1431655766, %edi
	imull	%edi
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	leal	0(,%rdx,4), %eax
	movl	%edx, %esi
	movl	%edx, %ebx
	movl	%edx, 44(%rsp)
	subl	%eax, %esi
	addl	%esi, %ecx
	cmpl	$1, %ecx
	je	.L20
	cmpl	$2, %ecx
	jne	.L8
	movdqa	.LC11(%rip), %xmm1
	call	__multf3@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%ebx, %edi
	call	__ldexpf128@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__multf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movaps	%xmm0, 16(%rsp)
	call	__multf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	movaps	%xmm2, (%rsp)
	call	__subtf3@PLT
	movdqa	.LC14(%rip), %xmm1
	call	__multf3@PLT
	movdqa	(%rsp), %xmm2
	movdqa	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	call	__subtf3@PLT
	cmpl	$-1, %ebp
	jne	.L1
	pxor	.LC3(%rip), %xmm0
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movdqa	(%rsp), %xmm0
	movdqa	%xmm0, %xmm1
	call	__addtf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movdqa	(%rsp), %xmm4
	movl	$-1, %ebp
	pxor	.LC3(%rip), %xmm4
	movaps	%xmm4, (%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ecx, %edi
	movl	$-1431655765, %ebx
	negl	%edi
	movl	%edi, %eax
	mull	%ebx
	movl	%edx, %ebx
	shrl	%ebx
	leal	0(,%rbx,4), %eax
	movl	%ebx, %esi
	subl	%eax, %esi
	movl	%esi, %eax
	subl	%ecx, %eax
	cmpl	$1, %eax
	je	.L21
	cmpl	$2, %eax
	je	.L22
.L10:
	negl	%ebx
	movl	%ebx, 44(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	movdqa	.LC10(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	movdqa	.LC13(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L21:
	movdqa	.LC12(%rip), %xmm1
	call	__multf3@PLT
	jmp	.L10
	.size	__cbrtf128, .-__cbrtf128
	.weak	cbrtf128
	.set	cbrtf128,__cbrtf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC1:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC4:
	.long	4289636977
	.long	3396517431
	.long	3076930579
	.long	1073485365
	.align 16
.LC5:
	.long	655738926
	.long	770949465
	.long	4003507371
	.long	1073629084
	.align 16
.LC6:
	.long	297611164
	.long	3671916163
	.long	925250205
	.long	1073695133
	.align 16
.LC7:
	.long	3110095466
	.long	895722272
	.long	2257922715
	.long	1073708381
	.align 16
.LC8:
	.long	4282385557
	.long	937668812
	.long	1692029592
	.long	1073697947
	.align 16
.LC9:
	.long	4220155393
	.long	3302136450
	.long	4269577174
	.long	1073578162
	.align 16
.LC10:
	.long	3071655461
	.long	2921479642
	.long	798545704
	.long	1073693322
	.align 16
.LC11:
	.long	2595503557
	.long	3358262681
	.long	3931363043
	.long	1073714783
	.align 16
.LC12:
	.long	2595503557
	.long	3358262681
	.long	3931363043
	.long	1073649247
	.align 16
.LC13:
	.long	3071655461
	.long	2921479642
	.long	798545704
	.long	1073627786
	.align 16
.LC14:
	.long	1431655765
	.long	1431655765
	.long	1431655765
	.long	1073567061
