	.text
	.p2align 4,,15
	.globl	__casinhl
	.type	__casinhl, @function
__casinhl:
	fldt	8(%rsp)
	fldt	24(%rsp)
	fld	%st(1)
	fabs
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L2
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	jbe	.L55
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L59
	fucomip	%st(1), %st
	fstp	%st(0)
	ja	.L24
	pxor	%xmm0, %xmm0
.L25:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	movss	%xmm0, -12(%rsp)
	fldz
	jne	.L56
	jmp	.L32
.L65:
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L32:
.L1:
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	jnb	.L4
	fldz
	fxch	%st(5)
	fucomi	%st(5), %st
	jp	.L60
	jne	.L61
	fxch	%st(3)
	fucomi	%st(0), %st
	jp	.L62
	fucomi	%st(2), %st
	fstp	%st(2)
	ja	.L63
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L64
	fxch	%st(1)
	fucomi	%st(2), %st
	fstp	%st(2)
	jp	.L21
	je	.L65
	jmp	.L21
.L64:
	fstp	%st(2)
	fxch	%st(1)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L67:
	fxch	%st(1)
.L21:
	fstpt	8(%rsp)
	xorl	%edi, %edi
	fstpt	24(%rsp)
	jmp	__kernel_casinhl@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	fstp	%st(0)
	fxch	%st(1)
	fucomi	%st(0), %st
	jp	.L66
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L67
	fxch	%st(1)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L63:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(2)
	fxch	%st(1)
.L20:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	flds	.LC7(%rip)
	jne	.L57
.L18:
	fldt	.LC1(%rip)
	fxch	%st(2)
.L19:
	fxam
	fnstsw	%ax
	fstp	%st(0)
	fxch	%st(1)
	testb	$2, %ah
	fabs
	je	.L32
	fchs
	jmp	.L32
.L60:
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L61:
	fstp	%st(5)
	fstp	%st(0)
	fstp	%st(0)
	fxch	%st(1)
	fxch	%st(2)
	fxch	%st(1)
.L33:
	fucomi	%st(0), %st
	jp	.L68
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jbe	.L21
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L2:
	fstp	%st(0)
	fucomi	%st(0), %st
	jp	.L69
	fldt	.LC3(%rip)
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jbe	.L58
	fstp	%st(0)
	fstp	%st(0)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	flds	.LC7(%rip)
	je	.L26
	fstp	%st(0)
	flds	.LC8(%rip)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L59:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L26
.L69:
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L26
.L70:
	fstp	%st(0)
	jmp	.L26
.L71:
	fstp	%st(0)
.L26:
	flds	.LC2(%rip)
	fxch	%st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	fstp	%st(0)
	fldz
	fchs
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L57:
	fstp	%st(0)
	flds	.LC8(%rip)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L62:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L66:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L68:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
.L52:
	flds	.LC2(%rip)
	fld	%st(0)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L24:
	fxch	%st(1)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	flds	.LC7(%rip)
	je	.L23
	fstp	%st(0)
	flds	.LC8(%rip)
.L23:
	fldt	.LC0(%rip)
	fxch	%st(2)
	jmp	.L19
.L58:
	fldt	.LC4(%rip)
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L70
	fldz
	fxch	%st(1)
	pxor	%xmm0, %xmm0
	fucomi	%st(1), %st
	fstp	%st(1)
	movss	%xmm0, -12(%rsp)
	jp	.L71
	je	.L25
	fstp	%st(0)
	jmp	.L26
	.size	__casinhl, .-__casinhl
	.weak	casinhf64x
	.set	casinhf64x,__casinhl
	.weak	casinhl
	.set	casinhl,__casinhl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	560513589
	.long	3373259426
	.long	16382
	.long	0
	.align 16
.LC1:
	.long	560513589
	.long	3373259426
	.long	16383
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC2:
	.long	2143289344
	.section	.rodata.cst16
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC4:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.section	.rodata.cst4
	.align 4
.LC7:
	.long	2139095040
	.align 4
.LC8:
	.long	4286578688
