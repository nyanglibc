	.text
	.p2align 4,,15
	.globl	__ufromfpf
	.type	__ufromfpf, @function
__ufromfpf:
	pushq	%rbp
	pushq	%rbx
	movd	%xmm0, %edx
	subq	$24, %rsp
	cmpl	$64, %esi
	ja	.L35
	testl	%esi, %esi
	jne	.L2
.L31:
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$64, %esi
.L2:
	movl	%edx, %ecx
	andl	$2147483647, %ecx
	je	.L28
	shrl	$23, %ecx
	testl	%edx, %edx
	leal	-127(%rcx), %r9d
	js	.L5
	leal	-1(%rsi), %r10d
	cmpl	%r10d, %r9d
	jg	.L26
	movl	%edx, %eax
	andl	$8388607, %eax
	orl	$8388608, %eax
	cmpl	$22, %r9d
	jle	.L8
	subl	$150, %ecx
	xorl	%r11d, %r11d
	salq	%cl, %rax
.L9:
	cmpl	$1, %edi
	je	.L39
	jle	.L75
	cmpl	$3, %edi
	je	.L40
	xorl	%ecx, %ecx
	cmpl	$4, %edi
	je	.L23
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%edx, %edx
	jns	.L19
.L16:
	testq	%rax, %rax
	je	.L1
	movl	$1, %edi
	call	__GI_feraiseexcept
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
.L28:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	xorl	%ebx, %ebx
.L72:
	testl	%edi, %edi
	jne	.L10
	testl	%edx, %edx
	js	.L16
	testb	%bl, %bl
	jne	.L42
	testb	%r11b, %r11b
	je	.L19
.L42:
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	$63, %r10d
	je	.L76
	leal	1(%r10), %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	cmpq	%rax, %rdx
	jne	.L1
.L26:
	movl	$1, %edi
	movl	%esi, 12(%rsp)
	call	__GI_feraiseexcept
	movl	12(%rsp), %esi
	movq	errno@gottpoff(%rip), %rax
	cmpl	$64, %esi
	movl	$33, %fs:(%rax)
	je	.L77
	movl	$1, %eax
	movl	%esi, %ecx
	salq	%cl, %rax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%r9d, %r9d
	jns	.L31
	movl	%edx, %eax
	movl	$-1, %r10d
	andl	$8388607, %eax
	orl	$8388608, %eax
.L8:
	cmpl	$-1, %r9d
	jl	.L36
	movl	$22, %ecx
	movl	$1, %r8d
	subl	%r9d, %ecx
	sall	%cl, %r8d
	movl	$23, %ecx
	movl	%r8d, %ebp
	andl	%eax, %ebp
	setne	%bl
	subl	$1, %r8d
	andl	%eax, %r8d
	setne	%r11b
	subl	%r9d, %ecx
	shrl	%cl, %eax
	cmpl	$1, %edi
	movl	%eax, %eax
	movq	%rax, %rcx
	je	.L11
	jle	.L72
	cmpl	$3, %edi
	je	.L14
	cmpl	$4, %edi
	jne	.L10
	testl	%ebp, %ebp
	je	.L37
	andl	$1, %ecx
	orl	%r8d, %ecx
	setne	%cl
	movzbl	%cl, %ecx
.L23:
	addq	%rcx, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%ebx, %ebx
.L22:
	addq	%rbx, %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%ebx, %ebx
.L11:
	testl	%edx, %edx
	jns	.L19
	testb	%bl, %bl
	jne	.L43
	testb	%r11b, %r11b
	je	.L16
.L43:
	addq	$1, %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$1, %r11d
	xorl	%eax, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$63, %r9d
	jne	.L1
	testq	%rax, %rax
	jne	.L1
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L77:
	movq	$-1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	%bl, %ebx
	jmp	.L22
.L37:
	xorl	%ecx, %ecx
	jmp	.L23
	.size	__ufromfpf, .-__ufromfpf
	.weak	ufromfpf32
	.set	ufromfpf32,__ufromfpf
	.weak	ufromfpf
	.set	ufromfpf,__ufromfpf
