	.text
	.globl	__divtf3
	.globl	__subtf3
	.globl	__addtf3
	.globl	__eqtf2
	.globl	__lttf2
	.globl	__multf3
	.p2align 4,,15
	.globl	__tanhf128
	.type	__tanhf128, @function
__tanhf128:
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rbx
	shrq	$32, %rbx
	movdqa	(%rsp), %xmm2
	movl	%ebx, %ebp
	andl	$2147483647, %ebp
	cmpl	$2147418111, %ebp
	movaps	%xmm2, 32(%rsp)
	jbe	.L2
	movdqa	%xmm0, %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__divtf3@PLT
	testl	%ebx, %ebx
	movdqa	.LC0(%rip), %xmm1
	js	.L16
	call	__addtf3@PLT
	movaps	%xmm0, 16(%rsp)
.L1:
	movdqa	16(%rsp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1074020351, %ebp
	jbe	.L17
	movdqa	.LC4(%rip), %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
.L10:
	testl	%ebx, %ebx
	jns	.L1
	movdqa	16(%rsp), %xmm4
	pxor	.LC7(%rip), %xmm4
	movaps	%xmm4, 16(%rsp)
	movdqa	16(%rsp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	movdqa	(%rsp), %xmm3
	testq	%rax, %rax
	movaps	%xmm3, 16(%rsp)
	je	.L1
	cmpl	$1069940735, %ebp
	ja	.L6
	movdqa	.LC2(%rip), %xmm0
	pand	%xmm3, %xmm0
	movdqa	.LC3(%rip), %xmm1
	call	__lttf2@PLT
	testq	%rax, %rax
	jns	.L7
	movdqa	(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
.L7:
	movdqa	.LC4(%rip), %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__addtf3@PLT
	movdqa	(%rsp), %xmm1
	call	__multf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	movdqa	16(%rsp), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	24(%rsp), %rax
	movq	%rbp, %rdx
	salq	$32, %rdx
	movl	%eax, %eax
	orq	%rdx, %rax
	cmpl	$1073676287, %ebp
	movq	%rax, 40(%rsp)
	jbe	.L9
	movdqa	32(%rsp), %xmm1
	movdqa	%xmm1, %xmm0
	call	__addtf3@PLT
	call	__expm1f128@PLT
	movdqa	.LC5(%rip), %xmm1
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC5(%rip), %xmm0
	call	__divtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC0(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movdqa	.LC6(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__multf3@PLT
	call	__expm1f128@PLT
	movdqa	.LC7(%rip), %xmm5
	pxor	%xmm0, %xmm5
	movdqa	.LC5(%rip), %xmm1
	movaps	%xmm5, (%rsp)
	call	__addtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	(%rsp), %xmm0
	call	__divtf3@PLT
	movaps	%xmm0, 16(%rsp)
	jmp	.L10
	.size	__tanhf128, .-__tanhf128
	.weak	tanhf128
	.set	tanhf128,__tanhf128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC3:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC4:
	.long	1152451630
	.long	1390727765
	.long	3199038956
	.long	6911849
	.align 16
.LC5:
	.long	0
	.long	0
	.long	0
	.long	1073741824
	.align 16
.LC6:
	.long	0
	.long	0
	.long	0
	.long	-1073741824
	.align 16
.LC7:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
