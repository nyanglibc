	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__sinhl
	.type	__sinhl, @function
__sinhl:
	subq	$8, %rsp
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__ieee754_sinhl@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomi	%st(1), %st
	fstp	%st(1)
	jb	.L7
	fstp	%st(0)
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	fldt	16(%rsp)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	_LIB_VERSION@GOTPCREL(%rip), %rax
	cmpl	$-1, (%rax)
	je	.L1
	fstp	%st(0)
	pushq	24(%rsp)
	pushq	24(%rsp)
	movl	$225, %edi
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__kernel_standard_l@PLT
	addq	$32, %rsp
	jmp	.L1
	.size	__sinhl, .-__sinhl
	.weak	sinhf64x
	.set	sinhf64x,__sinhl
	.weak	sinhl
	.set	sinhl,__sinhl
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
