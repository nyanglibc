	.text
#APP
	.symver __ieee754_atan2f128,__atan2f128_finite@GLIBC_2.26
	.globl	__addtf3
	.globl	__subtf3
	.globl	__divtf3
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_atan2f128
	.type	__ieee754_atan2f128, @function
__ieee754_atan2f128:
	pushq	%rbx
	movabsq	$9223372036854775807, %rdx
	movabsq	$9223090561878065152, %rsi
	subq	$48, %rsp
	movaps	%xmm1, (%rsp)
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdi
	movaps	%xmm0, 16(%rsp)
	movq	%rcx, %rax
	movq	%rdi, %r8
	negq	%rax
	andq	%rdx, %r8
	orq	%rcx, %rax
	shrq	$63, %rax
	orq	%r8, %rax
	cmpq	%rsi, %rax
	ja	.L2
	movq	16(%rsp), %r9
	movq	24(%rsp), %r10
	movq	%r9, %rax
	andq	%r10, %rdx
	negq	%rax
	orq	%r9, %rax
	shrq	$63, %rax
	orq	%rdx, %rax
	cmpq	%rsi, %rax
	ja	.L2
	movabsq	$-4611404543450677248, %rax
	addq	%rdi, %rax
	orq	%rcx, %rax
	je	.L52
	movq	%rdi, %rbx
	movq	%r10, %rax
	sarq	$62, %rbx
	shrq	$63, %rax
	andl	$2, %ebx
	orq	%rax, %rbx
	orq	%rdx, %r9
	jne	.L6
	cmpq	$2, %rbx
	je	.L7
	cmpq	$3, %rbx
	jne	.L53
.L8:
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movdqa	16(%rsp), %xmm1
	movdqa	(%rsp), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
.L1:
	movdqa	32(%rsp), %xmm0
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orq	%r8, %rcx
	je	.L50
	cmpq	%rsi, %r8
	je	.L54
	cmpq	%rsi, %rdx
	je	.L50
	subq	%r8, %rdx
	sarq	$48, %rdx
	cmpq	$120, %rdx
	jle	.L20
	movdqa	.LC11(%rip), %xmm1
	movdqa	.LC6(%rip), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
.L21:
	cmpq	$1, %rbx
	je	.L24
	cmpq	$2, %rbx
	je	.L25
	testq	%rbx, %rbx
	je	.L1
	movdqa	.LC13(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	.LC3(%rip), %xmm1
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	movdqa	16(%rsp), %xmm0
	addq	$48, %rsp
	popq	%rbx
	jmp	__atanf128@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	testq	%r10, %r10
	movdqa	.LC2(%rip), %xmm1
	js	.L55
	movdqa	.LC6(%rip), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L53:
	movdqa	16(%rsp), %xmm2
	movaps	%xmm2, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L54:
	cmpq	%r8, %rdx
	je	.L56
	cmpq	$2, %rbx
	je	.L7
	cmpq	$3, %rbx
	je	.L8
	cmpq	$1, %rbx
	je	.L57
	pxor	%xmm7, %xmm7
	movaps	%xmm7, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	movdqa	.LC5(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%rdi, %rdi
	jns	.L29
	pxor	%xmm3, %xmm3
	cmpq	$-120, %rdx
	movaps	%xmm3, 32(%rsp)
	jl	.L21
.L29:
	movdqa	(%rsp), %xmm1
	movdqa	16(%rsp), %xmm0
	call	__divtf3@PLT
	pand	.LC12(%rip), %xmm0
	call	__atanf128@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L21
.L25:
	movdqa	.LC13(%rip), %xmm1
	movdqa	32(%rsp), %xmm0
	call	__subtf3@PLT
	movdqa	%xmm0, %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L24:
	movdqa	32(%rsp), %xmm4
	movabsq	$-9223372036854775808, %rax
	movq	40(%rsp), %rdx
	movaps	%xmm4, (%rsp)
	xorq	%rax, %rdx
	movq	%rdx, 8(%rsp)
	movdqa	(%rsp), %xmm5
	movaps	%xmm5, 32(%rsp)
	jmp	.L1
.L56:
	cmpq	$2, %rbx
	movdqa	.LC2(%rip), %xmm1
	je	.L14
	cmpq	$3, %rbx
	je	.L15
	cmpq	$1, %rbx
	je	.L16
	movdqa	.LC7(%rip), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L57:
	movdqa	.LC1(%rip), %xmm6
	movaps	%xmm6, 32(%rsp)
	jmp	.L1
.L16:
	movdqa	.LC8(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L15:
	movdqa	.LC10(%rip), %xmm0
	call	__subtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
.L14:
	movdqa	.LC9(%rip), %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, 32(%rsp)
	jmp	.L1
	.size	__ieee754_atan2f128, .-__ieee754_atan2f128
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC2:
	.long	1152451630
	.long	1390727765
	.long	3199038956
	.long	6911849
	.align 16
.LC3:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073779231
	.align 16
.LC4:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	-1073704417
	.align 16
.LC5:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	-1073769953
	.align 16
.LC6:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073713695
	.align 16
.LC7:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	1073648159
	.align 16
.LC8:
	.long	3306619320
	.long	2221509004
	.long	3041149649
	.long	-1073835489
	.align 16
.LC9:
	.long	2479964490
	.long	592389929
	.long	3354604061
	.long	1073753495
	.align 16
.LC10:
	.long	2479964490
	.long	592389929
	.long	3354604061
	.long	-1073730153
	.align 16
.LC11:
	.long	549186148
	.long	2793195328
	.long	2418335880
	.long	1066192146
	.align 16
.LC12:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147483647
	.align 16
.LC13:
	.long	549186148
	.long	2793195328
	.long	2418335880
	.long	1066257682
