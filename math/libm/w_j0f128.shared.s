	.text
	.p2align 4,,15
	.globl	__j0f128
	.type	__j0f128, @function
__j0f128:
	jmp	__ieee754_j0f128@PLT
	.size	__j0f128, .-__j0f128
	.weak	j0f128
	.set	j0f128,__j0f128
	.globl	__unordtf2
	.globl	__gttf2
	.globl	__lttf2
	.globl	__eqtf2
	.p2align 4,,15
	.globl	__y0f128
	.type	__y0f128, @function
__y0f128:
	pxor	%xmm1, %xmm1
	subq	$24, %rsp
	movaps	%xmm0, (%rsp)
	call	__unordtf2@PLT
	testq	%rax, %rax
	jne	.L4
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__gttf2@PLT
	testq	%rax, %rax
	jle	.L12
.L4:
	movdqa	(%rsp), %xmm0
	addq	$24, %rsp
	jmp	__ieee754_y0f128@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__lttf2@PLT
	testq	%rax, %rax
	js	.L13
	pxor	%xmm1, %xmm1
	movdqa	(%rsp), %xmm0
	call	__eqtf2@PLT
	testq	%rax, %rax
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L13:
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L4
	.size	__y0f128, .-__y0f128
	.weak	y0f128
	.set	y0f128,__y0f128
