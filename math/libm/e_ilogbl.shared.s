.globl __ieee754_ilogbl
.type __ieee754_ilogbl,@function
.align 1<<4
__ieee754_ilogbl:
 fldt 8(%rsp)
 fxam
 fstsw %ax
 movb $0x45, %dh
 andb %ah, %dh
 cmpb $0x05, %dh
 je 1f
 cmpb $0x40, %dh
 je 2f
 fxtract
 fstp %st
 fistpl -4(%rsp)
 fwait
 movl -4(%rsp),%eax
 ret
1: fstp %st
 movl $0x7fffffff, %eax
 ret
2: fstp %st
 movl $0x80000000, %eax
 ret
.size __ieee754_ilogbl,.-__ieee754_ilogbl
