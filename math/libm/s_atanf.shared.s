	.text
	.p2align 4,,15
	.globl	__atanf
	.type	__atanf, @function
__atanf:
	movd	%xmm0, %eax
	movaps	%xmm0, %xmm3
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	cmpl	$1275068415, %eax
	jle	.L2
	cmpl	$2139095040, %eax
	jg	.L18
	testl	%edx, %edx
	jle	.L5
	movss	.LC1(%rip), %xmm0
	addss	.LC0(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1054867455, %eax
	jg	.L6
	cmpl	$822083583, %eax
	jg	.L14
	andps	.LC3(%rip), %xmm0
	movss	.LC4(%rip), %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.L8
	movaps	%xmm3, %xmm0
	mulss	%xmm3, %xmm0
.L8:
	movss	.LC5(%rip), %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	ucomiss	.LC6(%rip), %xmm1
	jbe	.L14
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L6:
	andps	.LC3(%rip), %xmm3
	cmpl	$1066926079, %eax
	movaps	%xmm3, %xmm1
	jg	.L10
	cmpl	$1060110335, %eax
	jg	.L11
	addss	%xmm3, %xmm3
	xorl	%eax, %eax
	addss	.LC7(%rip), %xmm1
	subss	.LC6(%rip), %xmm3
	divss	%xmm1, %xmm3
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	addss	%xmm0, %xmm3
	movaps	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	$-1, %rax
.L7:
	movaps	%xmm3, %xmm2
	movss	.LC10(%rip), %xmm1
	cmpl	$-1, %eax
	mulss	%xmm3, %xmm2
	movaps	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm0, %xmm1
	addss	.LC11(%rip), %xmm1
	mulss	%xmm0, %xmm1
	addss	.LC12(%rip), %xmm1
	mulss	%xmm0, %xmm1
	addss	.LC13(%rip), %xmm1
	mulss	%xmm0, %xmm1
	addss	.LC14(%rip), %xmm1
	mulss	%xmm0, %xmm1
	addss	.LC15(%rip), %xmm1
	mulss	%xmm2, %xmm1
	movss	.LC16(%rip), %xmm2
	mulss	%xmm0, %xmm2
	subss	.LC17(%rip), %xmm2
	mulss	%xmm0, %xmm2
	subss	.LC18(%rip), %xmm2
	mulss	%xmm0, %xmm2
	subss	.LC19(%rip), %xmm2
	mulss	%xmm0, %xmm2
	subss	.LC20(%rip), %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm1
	je	.L19
	leaq	atanlo(%rip), %rcx
	testl	%edx, %edx
	subss	(%rcx,%rax,4), %xmm1
	leaq	atanhi(%rip), %rcx
	movss	(%rcx,%rax,4), %xmm0
	subss	%xmm3, %xmm1
	subss	%xmm1, %xmm0
	jns	.L1
	xorps	.LC21(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$1075576831, %eax
	jg	.L12
	movss	.LC8(%rip), %xmm0
	movl	$2, %eax
	mulss	%xmm0, %xmm1
	subss	%xmm0, %xmm3
	addss	.LC6(%rip), %xmm1
	divss	%xmm1, %xmm3
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	movss	.LC2(%rip), %xmm0
	subss	.LC1(%rip), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	subss	%xmm1, %xmm3
	movaps	%xmm3, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movss	.LC9(%rip), %xmm3
	movl	$3, %eax
	divss	%xmm1, %xmm3
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	movss	.LC6(%rip), %xmm0
	movl	$1, %eax
	subss	%xmm0, %xmm3
	addss	%xmm0, %xmm1
	divss	%xmm1, %xmm3
	jmp	.L7
	.size	__atanf, .-__atanf
	.weak	atanf32
	.set	atanf32,__atanf
	.weak	atanf
	.set	atanf,__atanf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	atanlo, @object
	.size	atanlo, 16
atanlo:
	.long	833369961
	.long	857874792
	.long	856952756
	.long	866263400
	.align 16
	.type	atanhi, @object
	.size	atanhi, 16
atanhi:
	.long	1055744824
	.long	1061752794
	.long	1065064542
	.long	1070141402
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1070141402
	.align 4
.LC1:
	.long	866263400
	.align 4
.LC2:
	.long	3217625050
	.section	.rodata.cst16
	.align 16
.LC3:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC4:
	.long	8388608
	.align 4
.LC5:
	.long	1900671690
	.align 4
.LC6:
	.long	1065353216
	.align 4
.LC7:
	.long	1073741824
	.align 4
.LC8:
	.long	1069547520
	.align 4
.LC9:
	.long	3212836864
	.align 4
.LC10:
	.long	1015376343
	.align 4
.LC11:
	.long	1028381273
	.align 4
.LC12:
	.long	1032350517
	.align 4
.LC13:
	.long	1035611758
	.align 4
.LC14:
	.long	1041385765
	.align 4
.LC15:
	.long	1051372203
	.align 4
.LC16:
	.long	3172311585
	.align 4
.LC17:
	.long	1030680939
	.align 4
.LC18:
	.long	1033734037
	.align 4
.LC19:
	.long	1038323256
	.align 4
.LC20:
	.long	1045220557
	.section	.rodata.cst16
	.align 16
.LC21:
	.long	2147483648
	.long	0
	.long	0
	.long	0
