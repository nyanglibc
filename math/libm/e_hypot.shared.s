	.text
#APP
	.symver __ieee754_hypot,__hypot_finite@GLIBC_2.15
#NO_APP
	.p2align 4,,15
	.globl	__ieee754_hypot
	.type	__ieee754_hypot, @function
__ieee754_hypot:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%xmm1, %rbp
	pushq	%rbx
	movq	%xmm0, %rbx
	shrq	$32, %rbp
	subq	$40, %rsp
	shrq	$32, %rbx
	andl	$2147483647, %ebp
	andl	$2147483647, %ebx
	cmpl	%ebp, %ebx
	jge	.L2
	movapd	%xmm0, %xmm2
	movl	%ebx, %eax
	movl	%ebp, %ebx
	movapd	%xmm1, %xmm0
	movl	%eax, %ebp
	movapd	%xmm2, %xmm1
.L2:
	movq	%xmm0, %rdx
	movq	%rbx, %r12
	movq	%rbp, %r13
	salq	$32, %r12
	salq	$32, %r13
	movl	%edx, %eax
	movq	%xmm1, %rdx
	orq	%rax, %r12
	movl	%edx, %eax
	movq	%r12, 8(%rsp)
	orq	%rax, %r13
	movl	%ebx, %eax
	subl	%ebp, %eax
	movsd	8(%rsp), %xmm2
	cmpl	$62914560, %eax
	movq	%r13, 8(%rsp)
	movsd	8(%rsp), %xmm3
	jg	.L25
	xorl	%eax, %eax
	cmpl	$1596981248, %ebx
	jg	.L26
.L5:
	cmpl	$600834047, %ebp
	jle	.L27
.L8:
	movapd	%xmm2, %xmm4
	subsd	%xmm3, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.L28
	movq	%rbp, %rdi
	leal	1048576(%rbx), %edx
	addsd	%xmm2, %xmm2
	salq	$32, %rdi
	movapd	%xmm4, %xmm5
	movq	%rdi, 8(%rsp)
	salq	$32, %rdx
	movq	8(%rsp), %xmm1
	movq	%rdx, 8(%rsp)
	movq	8(%rsp), %xmm6
	movapd	%xmm1, %xmm0
	subsd	%xmm6, %xmm2
	xorpd	.LC0(%rip), %xmm5
	mulsd	%xmm6, %xmm0
	mulsd	%xmm3, %xmm2
	subsd	%xmm1, %xmm3
	mulsd	%xmm5, %xmm4
	movapd	%xmm0, %xmm1
	mulsd	%xmm6, %xmm3
	addsd	%xmm3, %xmm2
	subsd	%xmm2, %xmm4
	subsd	%xmm4, %xmm1
	sqrtsd	%xmm1, %xmm1
.L12:
	testl	%eax, %eax
	je	.L1
	sall	$20, %eax
	movsd	.LC1(%rip), %xmm0
	addl	$1072693248, %eax
	salq	$32, %rax
	movq	%rax, 8(%rsp)
	mulsd	8(%rsp), %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.L1
	movapd	%xmm1, %xmm0
	mulsd	%xmm1, %xmm0
.L1:
	addq	$40, %rsp
	movapd	%xmm1, %xmm0
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movapd	%xmm3, %xmm0
	movq	%rbx, %rsi
	salq	$32, %rsi
	xorpd	.LC0(%rip), %xmm0
	movq	%rsi, 8(%rsp)
	movq	8(%rsp), %xmm4
	movapd	%xmm4, %xmm1
	mulsd	%xmm0, %xmm3
	movapd	%xmm2, %xmm0
	subsd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm1
	addsd	%xmm4, %xmm0
	mulsd	%xmm0, %xmm2
	subsd	%xmm2, %xmm3
	subsd	%xmm3, %xmm1
	sqrtsd	%xmm1, %xmm1
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L25:
	addsd	%xmm3, %xmm2
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	movapd	%xmm2, %xmm1
	popq	%r13
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$2146435071, %ebx
	jle	.L6
	movapd	%xmm2, %xmm1
	movapd	%xmm2, %xmm0
	movsd	%xmm3, 16(%rsp)
	addsd	%xmm3, %xmm1
	movsd	%xmm2, 24(%rsp)
	movsd	%xmm1, 8(%rsp)
	call	__GI___issignaling
	testl	%eax, %eax
	movsd	8(%rsp), %xmm1
	jne	.L1
	movsd	16(%rsp), %xmm3
	movapd	%xmm3, %xmm0
	call	__GI___issignaling
	testl	%eax, %eax
	movsd	8(%rsp), %xmm1
	jne	.L1
	andl	$1048575, %ebx
	movsd	16(%rsp), %xmm3
	orl	%r12d, %ebx
	jne	.L7
	movsd	24(%rsp), %xmm2
	movapd	%xmm2, %xmm1
.L7:
	xorl	$2146435072, %ebp
	orl	%r13d, %ebp
	jne	.L1
	movapd	%xmm3, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	$1048575, %ebp
	jg	.L9
	movq	%xmm3, %rdx
	movapd	%xmm2, %xmm1
	orl	%edx, %ebp
	je	.L1
	movabsq	$9209861237972664320, %rdx
	subl	$1022, %eax
	movq	%rdx, 8(%rsp)
	movsd	8(%rsp), %xmm0
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm2
	movq	%xmm3, %rcx
	movq	%xmm2, %rdx
	shrq	$32, %rcx
	shrq	$32, %rdx
	movl	%ecx, %ebp
	cmpl	%ecx, %edx
	movl	%edx, %ebx
	jge	.L8
	movapd	%xmm2, %xmm0
	movl	%ecx, %ebx
	movl	%edx, %ebp
	movapd	%xmm3, %xmm2
	movapd	%xmm0, %xmm3
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	subl	$629145600, %ebx
	subl	$629145600, %ebp
	movl	%r12d, %eax
	movq	%rbx, %rdx
	salq	$32, %rdx
	orq	%rdx, %rax
	movq	%rbp, %rdx
	movq	%rax, 8(%rsp)
	salq	$32, %rdx
	movl	%r13d, %eax
	orq	%rdx, %rax
	movq	8(%rsp), %xmm2
	movq	%rax, 8(%rsp)
	movl	$600, %eax
	movq	8(%rsp), %xmm3
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%xmm2, %rdx
	addl	$629145600, %ebx
	addl	$629145600, %ebp
	movq	%rbx, %rcx
	subl	$600, %eax
	salq	$32, %rcx
	movl	%edx, %edx
	orq	%rcx, %rdx
	movq	%rbp, %rcx
	movq	%rdx, 8(%rsp)
	movq	%xmm3, %rdx
	salq	$32, %rcx
	movq	8(%rsp), %xmm2
	movl	%edx, %edx
	orq	%rcx, %rdx
	movq	%rdx, 8(%rsp)
	movq	8(%rsp), %xmm3
	jmp	.L8
	.size	__ieee754_hypot, .-__ieee754_hypot
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1048576
