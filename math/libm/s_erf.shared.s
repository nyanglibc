	.text
	.p2align 4,,15
	.globl	__erf
	.type	__erf, @function
__erf:
	pushq	%rbx
	movq	%xmm0, %rbx
	shrq	$32, %rbx
	subq	$48, %rsp
	movl	%ebx, %eax
	andl	$2147483647, %eax
	cmpl	$2146435071, %eax
	jg	.L18
	cmpl	$1072365567, %eax
	jg	.L4
	cmpl	$1043333119, %eax
	jg	.L5
	cmpl	$8388607, %eax
	movapd	%xmm0, %xmm1
	jg	.L6
	mulsd	.LC1(%rip), %xmm1
	mulsd	.LC2(%rip), %xmm0
	movsd	.LC5(%rip), %xmm2
	addsd	%xmm1, %xmm0
	mulsd	.LC3(%rip), %xmm0
	movapd	%xmm0, %xmm1
	andpd	.LC4(%rip), %xmm1
	ucomisd	%xmm1, %xmm2
	jbe	.L1
	movapd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm1
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$1072955391, %eax
	jg	.L8
	movapd	%xmm0, %xmm1
	movsd	.LC0(%rip), %xmm7
	movsd	.LC17(%rip), %xmm0
	testl	%ebx, %ebx
	andpd	.LC4(%rip), %xmm1
	movsd	.LC19(%rip), %xmm2
	movsd	.LC21(%rip), %xmm6
	subsd	%xmm7, %xmm1
	mulsd	%xmm1, %xmm0
	movapd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm2
	mulsd	%xmm1, %xmm4
	mulsd	%xmm1, %xmm6
	subsd	.LC18(%rip), %xmm0
	subsd	.LC20(%rip), %xmm2
	movapd	%xmm4, %xmm5
	movapd	%xmm4, %xmm3
	subsd	.LC22(%rip), %xmm6
	mulsd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm0
	mulsd	%xmm5, %xmm3
	mulsd	%xmm5, %xmm6
	addsd	%xmm2, %xmm0
	movsd	.LC23(%rip), %xmm2
	mulsd	%xmm3, %xmm2
	mulsd	.LC29(%rip), %xmm3
	addsd	%xmm6, %xmm0
	addsd	%xmm2, %xmm0
	movsd	.LC24(%rip), %xmm2
	mulsd	%xmm1, %xmm2
	addsd	.LC25(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	movsd	.LC26(%rip), %xmm4
	mulsd	%xmm1, %xmm4
	mulsd	.LC27(%rip), %xmm1
	addsd	%xmm4, %xmm7
	addsd	.LC28(%rip), %xmm1
	addsd	%xmm7, %xmm2
	mulsd	%xmm5, %xmm1
	addsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm1
	divsd	%xmm1, %xmm0
	js	.L9
	addsd	.LC30(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movsd	.LC0(%rip), %xmm2
	shrl	$31, %ebx
	pxor	%xmm1, %xmm1
	addl	%ebx, %ebx
	divsd	%xmm0, %xmm2
	movl	$1, %eax
	subl	%ebx, %eax
	cvtsi2sd	%eax, %xmm1
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
.L1:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1075314687, %eax
	jle	.L10
	testl	%ebx, %ebx
	js	.L11
	movsd	.LC0(%rip), %xmm0
	subsd	.LC32(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movapd	%xmm0, %xmm2
	movsd	.LC7(%rip), %xmm1
	mulsd	%xmm0, %xmm2
	movsd	.LC9(%rip), %xmm3
	mulsd	%xmm2, %xmm1
	movapd	%xmm2, %xmm4
	mulsd	%xmm2, %xmm3
	mulsd	%xmm2, %xmm4
	subsd	.LC8(%rip), %xmm1
	addsd	.LC10(%rip), %xmm3
	movapd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm5
	mulsd	%xmm4, %xmm1
	addsd	%xmm3, %xmm1
	movsd	.LC11(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm1
	movsd	.LC12(%rip), %xmm3
	mulsd	%xmm2, %xmm3
	addsd	.LC13(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	movsd	.LC14(%rip), %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	.LC15(%rip), %xmm2
	addsd	.LC0(%rip), %xmm4
	addsd	.LC16(%rip), %xmm2
	addq	$48, %rsp
	popq	%rbx
	addsd	%xmm4, %xmm3
	mulsd	%xmm5, %xmm2
	addsd	%xmm3, %xmm2
	divsd	%xmm2, %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movapd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm0
	movsd	.LC0(%rip), %xmm1
	cmpl	$1074191213, %eax
	andpd	.LC4(%rip), %xmm3
	movapd	%xmm1, %xmm6
	divsd	%xmm0, %xmm6
	movapd	%xmm6, %xmm4
	movapd	%xmm6, %xmm0
	mulsd	%xmm6, %xmm4
	movapd	%xmm0, %xmm5
	movapd	%xmm4, %xmm2
	mulsd	%xmm4, %xmm2
	movapd	%xmm2, %xmm6
	mulsd	%xmm4, %xmm6
	jle	.L19
	mulsd	.LC49(%rip), %xmm5
	movsd	.LC51(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	subsd	.LC50(%rip), %xmm5
	subsd	.LC52(%rip), %xmm7
	mulsd	%xmm4, %xmm5
	addsd	%xmm7, %xmm5
	movsd	.LC53(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	subsd	.LC54(%rip), %xmm7
	mulsd	%xmm2, %xmm7
	addsd	%xmm7, %xmm5
	movsd	.LC55(%rip), %xmm7
	mulsd	%xmm6, %xmm7
	addsd	%xmm7, %xmm5
	movsd	%xmm5, 32(%rsp)
	movsd	.LC56(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	.LC57(%rip), %xmm5
	mulsd	%xmm5, %xmm4
	movsd	.LC58(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm4
	movsd	.LC59(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	.LC61(%rip), %xmm0
	addsd	.LC60(%rip), %xmm5
	addsd	.LC62(%rip), %xmm0
	mulsd	%xmm5, %xmm2
	mulsd	%xmm6, %xmm0
	addsd	%xmm4, %xmm2
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 40(%rsp)
.L13:
	movq	%xmm3, %rax
	movabsq	$-4294967296, %rdx
	movsd	%xmm1, 24(%rsp)
	movsd	%xmm3, 16(%rsp)
	andq	%rdx, %rax
	movq	%rax, (%rsp)
	movq	(%rsp), %xmm2
	movapd	%xmm2, %xmm0
	movsd	%xmm2, 8(%rsp)
	xorpd	.LC63(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	subsd	.LC64(%rip), %xmm0
	call	__ieee754_exp@PLT
	movsd	8(%rsp), %xmm2
	movsd	16(%rsp), %xmm3
	movapd	%xmm2, %xmm7
	movsd	%xmm0, (%rsp)
	addsd	%xmm3, %xmm2
	movsd	%xmm3, 8(%rsp)
	subsd	%xmm3, %xmm7
	mulsd	%xmm7, %xmm2
	movsd	32(%rsp), %xmm7
	divsd	40(%rsp), %xmm7
	movapd	%xmm7, %xmm0
	addsd	%xmm2, %xmm0
	call	__ieee754_exp@PLT
	mulsd	(%rsp), %xmm0
	testl	%ebx, %ebx
	movsd	8(%rsp), %xmm3
	movsd	24(%rsp), %xmm1
	divsd	%xmm3, %xmm0
	js	.L14
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	mulsd	.LC6(%rip), %xmm1
	addsd	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	mulsd	.LC33(%rip), %xmm5
	movsd	.LC35(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	subsd	.LC34(%rip), %xmm5
	subsd	.LC36(%rip), %xmm7
	mulsd	%xmm4, %xmm5
	addsd	%xmm7, %xmm5
	movsd	.LC37(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	subsd	.LC38(%rip), %xmm7
	mulsd	%xmm2, %xmm7
	addsd	%xmm7, %xmm5
	movsd	.LC39(%rip), %xmm7
	mulsd	%xmm0, %xmm7
	subsd	.LC40(%rip), %xmm7
	mulsd	%xmm6, %xmm7
	addsd	%xmm7, %xmm5
	movsd	%xmm5, 32(%rsp)
	movsd	.LC41(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	.LC42(%rip), %xmm5
	mulsd	%xmm5, %xmm4
	movsd	.LC43(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	addsd	%xmm1, %xmm5
	addsd	%xmm5, %xmm4
	movsd	.LC44(%rip), %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	.LC46(%rip), %xmm0
	addsd	.LC45(%rip), %xmm5
	addsd	.LC47(%rip), %xmm0
	mulsd	%xmm2, %xmm5
	mulsd	%xmm2, %xmm2
	mulsd	%xmm6, %xmm0
	addsd	%xmm5, %xmm4
	mulsd	.LC48(%rip), %xmm2
	addsd	%xmm4, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, 40(%rsp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movsd	.LC32(%rip), %xmm0
	subsd	.LC0(%rip), %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	movsd	.LC31(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	subsd	%xmm1, %xmm0
	jmp	.L1
	.size	__erf, .-__erf
	.weak	erff32x
	.set	erff32x,__erf
	.weak	erff64
	.set	erff64,__erf
	.weak	erf
	.set	erf,__erf
	.p2align 4,,15
	.globl	__erfc
	.type	__erfc, @function
__erfc:
	pushq	%rbx
	movq	%xmm0, %rbx
	shrq	$32, %rbx
	subq	$48, %rsp
	movl	%ebx, %eax
	andl	$2147483647, %eax
	cmpl	$2146435071, %eax
	jg	.L41
	cmpl	$1072365567, %eax
	jg	.L23
	cmpl	$1013972991, %eax
	jle	.L42
	movapd	%xmm0, %xmm3
	movsd	.LC7(%rip), %xmm2
	movsd	.LC9(%rip), %xmm4
	cmpl	$1070596095, %ebx
	mulsd	%xmm0, %xmm3
	movsd	.LC14(%rip), %xmm6
	mulsd	%xmm3, %xmm2
	movapd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm4
	mulsd	%xmm3, %xmm1
	mulsd	%xmm3, %xmm6
	subsd	.LC8(%rip), %xmm2
	addsd	.LC10(%rip), %xmm4
	movapd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm5
	mulsd	%xmm1, %xmm2
	addsd	%xmm4, %xmm2
	movsd	.LC11(%rip), %xmm4
	mulsd	%xmm5, %xmm4
	addsd	%xmm4, %xmm2
	movsd	.LC12(%rip), %xmm4
	mulsd	%xmm3, %xmm4
	mulsd	.LC15(%rip), %xmm3
	addsd	.LC13(%rip), %xmm4
	addsd	.LC16(%rip), %xmm3
	mulsd	%xmm1, %xmm4
	movsd	.LC0(%rip), %xmm1
	addsd	%xmm1, %xmm6
	mulsd	%xmm5, %xmm3
	addsd	%xmm6, %xmm4
	addsd	%xmm4, %xmm3
	divsd	%xmm3, %xmm2
	mulsd	%xmm0, %xmm2
	jle	.L40
	movsd	.LC65(%rip), %xmm1
	subsd	%xmm1, %xmm0
.L40:
	addsd	%xmm0, %xmm2
	addq	$48, %rsp
	popq	%rbx
	subsd	%xmm2, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	cmpl	$1072955391, %eax
	jg	.L26
	andpd	.LC4(%rip), %xmm0
	testl	%ebx, %ebx
	movsd	.LC0(%rip), %xmm1
	movsd	.LC17(%rip), %xmm2
	movsd	.LC19(%rip), %xmm3
	subsd	%xmm1, %xmm0
	movsd	.LC21(%rip), %xmm6
	mulsd	%xmm0, %xmm2
	movapd	%xmm0, %xmm7
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm7
	mulsd	%xmm0, %xmm6
	subsd	.LC18(%rip), %xmm2
	subsd	.LC20(%rip), %xmm3
	movapd	%xmm7, %xmm5
	movapd	%xmm7, %xmm4
	subsd	.LC22(%rip), %xmm6
	mulsd	%xmm7, %xmm5
	mulsd	%xmm7, %xmm2
	mulsd	%xmm5, %xmm4
	mulsd	%xmm5, %xmm6
	addsd	%xmm3, %xmm2
	movsd	.LC23(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	mulsd	.LC29(%rip), %xmm4
	addsd	%xmm6, %xmm2
	movsd	.LC26(%rip), %xmm6
	mulsd	%xmm0, %xmm6
	addsd	%xmm3, %xmm2
	movsd	.LC24(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm6
	mulsd	.LC27(%rip), %xmm0
	addsd	.LC25(%rip), %xmm3
	addsd	.LC28(%rip), %xmm0
	mulsd	%xmm7, %xmm3
	mulsd	%xmm5, %xmm0
	addsd	%xmm6, %xmm3
	addsd	%xmm3, %xmm0
	addsd	%xmm4, %xmm0
	divsd	%xmm0, %xmm2
	js	.L27
	movsd	.LC66(%rip), %xmm0
	subsd	%xmm2, %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L41:
	movsd	.LC0(%rip), %xmm2
	shrl	$31, %ebx
	pxor	%xmm1, %xmm1
	addl	%ebx, %ebx
	divsd	%xmm0, %xmm2
	cvtsi2sd	%ebx, %xmm1
	movapd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
.L20:
	addq	$48, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$1077673983, %eax
	jg	.L28
	movapd	%xmm0, %xmm2
	movsd	.LC0(%rip), %xmm8
	mulsd	%xmm0, %xmm0
	cmpl	$1074191212, %eax
	movapd	%xmm8, %xmm7
	andpd	.LC4(%rip), %xmm2
	divsd	%xmm0, %xmm7
	movapd	%xmm7, %xmm0
	jg	.L29
	movsd	.LC33(%rip), %xmm3
	movsd	.LC35(%rip), %xmm1
	movapd	%xmm7, %xmm6
	mulsd	%xmm7, %xmm3
	mulsd	%xmm7, %xmm1
	mulsd	%xmm7, %xmm6
	subsd	.LC34(%rip), %xmm3
	subsd	.LC36(%rip), %xmm1
	movapd	%xmm6, %xmm4
	movapd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm4
	mulsd	%xmm6, %xmm3
	mulsd	%xmm4, %xmm5
	addsd	%xmm1, %xmm3
	movsd	.LC37(%rip), %xmm1
	mulsd	%xmm7, %xmm1
	mulsd	.LC39(%rip), %xmm7
	subsd	.LC38(%rip), %xmm1
	subsd	.LC40(%rip), %xmm7
	mulsd	%xmm4, %xmm1
	mulsd	%xmm5, %xmm7
	addsd	%xmm1, %xmm3
	movsd	.LC41(%rip), %xmm1
	mulsd	%xmm0, %xmm1
	addsd	%xmm7, %xmm3
	addsd	.LC42(%rip), %xmm1
	movsd	%xmm3, 32(%rsp)
	movsd	.LC43(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	%xmm6, %xmm1
	addsd	%xmm3, %xmm8
	movsd	.LC44(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	mulsd	.LC46(%rip), %xmm0
	addsd	%xmm8, %xmm1
	addsd	.LC45(%rip), %xmm3
	addsd	.LC47(%rip), %xmm0
	mulsd	%xmm4, %xmm3
	mulsd	%xmm4, %xmm4
	mulsd	%xmm5, %xmm0
	addsd	%xmm3, %xmm1
	addsd	%xmm1, %xmm0
	movsd	.LC48(%rip), %xmm1
	mulsd	%xmm4, %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 40(%rsp)
.L30:
	movq	%xmm2, %rax
	movabsq	$-4294967296, %rdx
	movsd	%xmm2, 24(%rsp)
	andq	%rdx, %rax
	movq	%rax, 8(%rsp)
	movq	8(%rsp), %xmm1
	movapd	%xmm1, %xmm0
	movsd	%xmm1, 16(%rsp)
	xorpd	.LC63(%rip), %xmm0
	mulsd	%xmm1, %xmm0
	subsd	.LC64(%rip), %xmm0
	call	__ieee754_exp@PLT
	movsd	16(%rsp), %xmm1
	movsd	24(%rsp), %xmm2
	movapd	%xmm1, %xmm7
	movsd	%xmm0, 8(%rsp)
	addsd	%xmm2, %xmm1
	movsd	%xmm2, 16(%rsp)
	subsd	%xmm2, %xmm7
	mulsd	%xmm7, %xmm1
	movsd	32(%rsp), %xmm7
	divsd	40(%rsp), %xmm7
	movapd	%xmm7, %xmm0
	addsd	%xmm1, %xmm0
	call	__ieee754_exp@PLT
	mulsd	8(%rsp), %xmm0
	testl	%ebx, %ebx
	movsd	16(%rsp), %xmm2
	divsd	%xmm2, %xmm0
	jle	.L32
	ucomisd	.LC68(%rip), %xmm0
	jp	.L20
	jne	.L20
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L42:
	movsd	.LC0(%rip), %xmm1
	addq	$48, %rsp
	popq	%rbx
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	testl	%ebx, %ebx
	jle	.L34
	movq	errno@gottpoff(%rip), %rax
	movsd	.LC32(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%ebx, %edx
	shrl	$31, %edx
	testb	%dl, %dl
	je	.L31
	cmpl	$1075314687, %eax
	jle	.L31
.L34:
	movsd	.LC67(%rip), %xmm0
	subsd	.LC32(%rip), %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L27:
	addsd	.LC30(%rip), %xmm2
	addsd	%xmm1, %xmm2
	movapd	%xmm2, %xmm0
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L31:
	movsd	.LC49(%rip), %xmm1
	movsd	.LC51(%rip), %xmm3
	movapd	%xmm0, %xmm6
	mulsd	%xmm0, %xmm1
	mulsd	%xmm0, %xmm3
	mulsd	%xmm0, %xmm6
	subsd	.LC50(%rip), %xmm1
	subsd	.LC52(%rip), %xmm3
	movapd	%xmm6, %xmm5
	movapd	%xmm6, %xmm4
	mulsd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm1
	mulsd	%xmm5, %xmm4
	addsd	%xmm3, %xmm1
	movsd	.LC53(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	subsd	.LC54(%rip), %xmm3
	mulsd	%xmm5, %xmm3
	addsd	%xmm3, %xmm1
	movsd	.LC55(%rip), %xmm3
	mulsd	%xmm4, %xmm3
	addsd	%xmm3, %xmm1
	movsd	.LC56(%rip), %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm1, 32(%rsp)
	movsd	.LC58(%rip), %xmm1
	addsd	.LC57(%rip), %xmm3
	mulsd	%xmm0, %xmm1
	addsd	%xmm1, %xmm8
	movsd	.LC59(%rip), %xmm1
	mulsd	%xmm6, %xmm3
	mulsd	%xmm0, %xmm1
	mulsd	.LC61(%rip), %xmm0
	addsd	%xmm3, %xmm8
	addsd	.LC60(%rip), %xmm1
	addsd	.LC62(%rip), %xmm0
	mulsd	%xmm5, %xmm1
	mulsd	%xmm4, %xmm0
	addsd	%xmm1, %xmm8
	addsd	%xmm0, %xmm8
	movsd	%xmm8, 40(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L32:
	movsd	.LC67(%rip), %xmm1
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	jmp	.L20
	.size	__erfc, .-__erfc
	.weak	erfcf32x
	.set	erfcf32x,__erfc
	.weak	erfcf64
	.set	erfcf64,__erfc
	.weak	erfc
	.set	erfc,__erfc
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.align 8
.LC1:
	.long	0
	.long	1076887552
	.align 8
.LC2:
	.long	2182404969
	.long	1073770170
	.align 8
.LC3:
	.long	0
	.long	1068498944
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC4:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC5:
	.long	0
	.long	1048576
	.align 8
.LC6:
	.long	2182404969
	.long	1069575866
	.align 8
.LC7:
	.long	593914084
	.long	-1082678639
	.align 8
.LC8:
	.long	3688307023
	.long	1067264593
	.align 8
.LC9:
	.long	1763490067
	.long	-1076572803
	.align 8
.LC10:
	.long	2182404968
	.long	1069575866
	.align 8
.LC11:
	.long	301995692
	.long	-1090983210
	.align 8
.LC12:
	.long	3302189839
	.long	1064620066
	.align 8
.LC13:
	.long	1429655226
	.long	1068541260
	.align 8
.LC14:
	.long	3453672457
	.long	1071216505
	.align 8
.LC15:
	.long	1117937952
	.long	-1093624765
	.align 8
.LC16:
	.long	572267024
	.long	1059151305
	.align 8
.LC17:
	.long	2152800484
	.long	1070882762
	.align 8
.LC18:
	.long	4223190001
	.long	1071108672
	.align 8
.LC19:
	.long	2912072525
	.long	1071287552
	.align 8
.LC20:
	.long	3203888440
	.long	1063475640
	.align 8
.LC21:
	.long	1503106539
	.long	1067592246
	.align 8
.LC22:
	.long	1027483884
	.long	1069310872
	.align 8
.LC23:
	.long	177604415
	.long	-1084113096
	.align 8
.LC24:
	.long	3651135911
	.long	1068655452
	.align 8
.LC25:
	.long	2464902963
	.long	1071729392
	.align 8
.LC26:
	.long	418308899
	.long	1069235814
	.align 8
.LC27:
	.long	1800527132
	.long	1066134978
	.align 8
.LC28:
	.long	3882038559
	.long	1069557344
	.align 8
.LC29:
	.long	1463096605
	.long	1065913172
	.align 8
.LC30:
	.long	1610612736
	.long	1072368321
	.align 8
.LC31:
	.long	1610612736
	.long	-1075115327
	.align 8
.LC32:
	.long	3271095129
	.long	27618847
	.align 8
.LC33:
	.long	3838550925
	.long	-1068552182
	.align 8
.LC34:
	.long	1102112550
	.long	1076174340
	.align 8
.LC35:
	.long	3837424480
	.long	-1075432426
	.align 8
.LC36:
	.long	1611490357
	.long	1065628690
	.align 8
.LC37:
	.long	3956059058
	.long	-1066986660
	.align 8
.LC38:
	.long	2217222758
	.long	1080315057
	.align 8
.LC39:
	.long	3332031068
	.long	-1071406865
	.align 8
.LC40:
	.long	1474614002
	.long	1079267941
	.align 8
.LC41:
	.long	3582597745
	.long	1081813261
	.align 8
.LC42:
	.long	1382737697
	.long	1080112396
	.align 8
.LC43:
	.long	3178264199
	.long	1077126841
	.align 8
.LC44:
	.long	1466958612
	.long	1081790497
	.align 8
.LC45:
	.long	569124968
	.long	1082403609
	.align 8
.LC46:
	.long	2387102355
	.long	1075464175
	.align 8
.LC47:
	.long	3997740588
	.long	1079716003
	.align 8
.LC48:
	.long	4000619106
	.long	-1079054350
	.align 8
.LC49:
	.long	1137044888
	.long	-1067183011
	.align 8
.LC50:
	.long	1432328538
	.long	1077002761
	.align 8
.LC51:
	.long	1891796446
	.long	-1075211334
	.align 8
.LC52:
	.long	971534154
	.long	1065628690
	.align 8
.LC53:
	.long	1781422482
	.long	-1064303519
	.align 8
.LC54:
	.long	326496808
	.long	1082387592
	.align 8
.LC55:
	.long	2614900799
	.long	-1065469874
	.align 8
.LC56:
	.long	412963096
	.long	1083704043
	.align 8
.LC57:
	.long	572235530
	.long	1081367726
	.align 8
.LC58:
	.long	639455632
	.long	1077827211
	.align 8
.LC59:
	.long	3470736358
	.long	1084486169
	.align 8
.LC60:
	.long	1754014826
	.long	1084817335
	.align 8
.LC61:
	.long	1114713442
	.long	-1070173982
	.align 8
.LC62:
	.long	3886016355
	.long	1081976948
	.section	.rodata.cst16
	.align 16
.LC63:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC64:
	.long	0
	.long	1071775744
	.align 8
.LC65:
	.long	0
	.long	1071644672
	.align 8
.LC66:
	.long	2147483648
	.long	1069798650
	.align 8
.LC67:
	.long	0
	.long	1073741824
	.align 8
.LC68:
	.long	0
	.long	0
