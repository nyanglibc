	.text
	.p2align 4,,15
	.globl	__feraiseexcept
	.type	__feraiseexcept, @function
__feraiseexcept:
	testb	$1, %dil
	je	.L2
	pxor	%xmm0, %xmm0
#APP
# 36 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	divss %xmm0, %xmm0 
# 0 "" 2
#NO_APP
.L2:
	testb	$4, %dil
	je	.L3
	movss	.LC1(%rip), %xmm0
	pxor	%xmm1, %xmm1
#APP
# 46 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	divss %xmm1, %xmm0
# 0 "" 2
#NO_APP
.L3:
	testb	$8, %dil
	je	.L4
#APP
# 60 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fnstenv -40(%rsp)
# 0 "" 2
#NO_APP
	orw	$8, -36(%rsp)
#APP
# 66 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fldenv -40(%rsp)
# 0 "" 2
# 69 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fwait
# 0 "" 2
#NO_APP
.L4:
	testb	$16, %dil
	je	.L5
#APP
# 82 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fnstenv -40(%rsp)
# 0 "" 2
#NO_APP
	orw	$16, -36(%rsp)
#APP
# 88 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fldenv -40(%rsp)
# 0 "" 2
# 91 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fwait
# 0 "" 2
#NO_APP
.L5:
	andl	$32, %edi
	je	.L6
#APP
# 104 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fnstenv -40(%rsp)
# 0 "" 2
#NO_APP
	orw	$32, -36(%rsp)
#APP
# 110 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fldenv -40(%rsp)
# 0 "" 2
# 113 "../sysdeps/x86_64/fpu/fraiseexcpt.c" 1
	fwait
# 0 "" 2
#NO_APP
.L6:
	xorl	%eax, %eax
	ret
	.size	__feraiseexcept, .-__feraiseexcept
	.weak	feraiseexcept
	.set	feraiseexcept,__feraiseexcept
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	1065353216
