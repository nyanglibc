	.text
#APP
	.symver matherr,matherr@GLIBC_2.2.5
	.symver _LIB_VERSION,_LIB_VERSION@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__kernel_casinhf
	.type	__kernel_casinhf, @function
__kernel_casinhf:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$192, %rsp
	movq	%xmm0, 184(%rsp)
	movss	.LC1(%rip), %xmm3
	movss	184(%rsp), %xmm4
	movaps	%xmm4, %xmm9
	movss	188(%rsp), %xmm2
	movaps	%xmm2, %xmm5
	andps	%xmm3, %xmm9
	movaps	%xmm2, %xmm6
	andps	%xmm3, %xmm5
	ucomiss	.LC2(%rip), %xmm9
	jnb	.L2
	ucomiss	.LC2(%rip), %xmm5
	jb	.L67
.L2:
	testl	%ebx, %ebx
	jne	.L5
	movaps	%xmm9, %xmm2
.L6:
	movss	%xmm2, 176(%rsp)
	movss	%xmm5, 180(%rsp)
	movaps	%xmm3, 32(%rsp)
	movss	%xmm4, 16(%rsp)
	movss	%xmm6, (%rsp)
	movq	176(%rsp), %xmm0
	call	__clogf@PLT
	movq	%xmm0, 168(%rsp)
	movss	.LC4(%rip), %xmm7
	movss	.LC3(%rip), %xmm1
	addss	168(%rsp), %xmm1
	movss	16(%rsp), %xmm4
	movaps	%xmm4, %xmm6
	movaps	32(%rsp), %xmm3
	andps	%xmm7, %xmm6
	movss	172(%rsp), %xmm0
	andps	%xmm3, %xmm1
	orps	%xmm6, %xmm1
	movss	(%rsp), %xmm6
	movaps	%xmm1, %xmm4
.L7:
	testl	%ebx, %ebx
	je	.L47
	movss	.LC0(%rip), %xmm6
.L47:
	movaps	%xmm6, %xmm1
	andps	%xmm0, %xmm3
	movss	%xmm4, 128(%rsp)
	andps	%xmm7, %xmm1
	orps	%xmm1, %xmm3
	movss	%xmm3, 132(%rsp)
	movq	128(%rsp), %xmm0
	addq	$192, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movaps	%xmm9, %xmm5
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L67:
	ucomiss	.LC5(%rip), %xmm9
	movss	.LC6(%rip), %xmm1
	jb	.L8
	ucomiss	%xmm5, %xmm1
	ja	.L80
.L8:
	ucomiss	%xmm9, %xmm1
	ja	.L81
.L14:
	movss	.LC0(%rip), %xmm8
	ucomiss	%xmm8, %xmm5
	jbe	.L68
	movss	.LC7(%rip), %xmm0
	ucomiss	%xmm5, %xmm0
	jbe	.L20
	movss	.LC5(%rip), %xmm7
	ucomiss	%xmm9, %xmm7
	jbe	.L20
	movaps	%xmm5, %xmm0
	movaps	%xmm5, %xmm10
	subss	%xmm8, %xmm0
	addss	%xmm8, %xmm10
	mulss	%xmm0, %xmm10
	movss	.LC8(%rip), %xmm0
	ucomiss	%xmm9, %xmm0
	jbe	.L69
	sqrtss	%xmm10, %xmm11
	mulss	%xmm11, %xmm5
	movss	%xmm7, (%rsp)
	movaps	%xmm3, 80(%rsp)
	movss	%xmm4, 64(%rsp)
	movaps	%xmm5, %xmm0
	movss	%xmm9, 48(%rsp)
	movss	%xmm6, 32(%rsp)
	addss	%xmm10, %xmm0
	movss	%xmm11, 16(%rsp)
	movss	%xmm8, 96(%rsp)
	movss	%xmm2, 116(%rsp)
	addss	%xmm0, %xmm0
	call	__log1pf@PLT
	movss	(%rsp), %xmm7
	testl	%ebx, %ebx
	mulss	%xmm7, %xmm0
	movss	16(%rsp), %xmm11
	movss	32(%rsp), %xmm6
	movss	48(%rsp), %xmm9
	movss	64(%rsp), %xmm4
	movss	%xmm0, (%rsp)
	movaps	80(%rsp), %xmm3
	je	.L25
	movss	.LC4(%rip), %xmm7
	andps	%xmm3, %xmm11
	movss	116(%rsp), %xmm2
	movss	%xmm4, 48(%rsp)
	movaps	%xmm2, %xmm4
	movaps	%xmm9, %xmm0
	andps	%xmm7, %xmm4
	movaps	%xmm7, 32(%rsp)
	orps	%xmm4, %xmm11
	movaps	%xmm3, 16(%rsp)
	movaps	%xmm11, %xmm1
	call	__ieee754_atan2f@PLT
	movss	(%rsp), %xmm1
	movaps	16(%rsp), %xmm3
	movaps	32(%rsp), %xmm7
	movss	48(%rsp), %xmm4
	movss	96(%rsp), %xmm8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L81:
	ucomiss	.LC7(%rip), %xmm5
	jb	.L14
	movaps	%xmm5, %xmm10
	movaps	%xmm5, %xmm0
	movss	.LC0(%rip), %xmm8
	subss	%xmm8, %xmm0
	movss	%xmm4, 48(%rsp)
	addss	%xmm8, %xmm10
	movss	%xmm9, 32(%rsp)
	movaps	%xmm3, 64(%rsp)
	movss	%xmm6, 16(%rsp)
	mulss	%xmm0, %xmm10
	movss	%xmm2, 116(%rsp)
	movss	%xmm8, 80(%rsp)
	sqrtss	%xmm10, %xmm10
	addss	%xmm10, %xmm5
	movss	%xmm10, (%rsp)
	movaps	%xmm5, %xmm0
	call	__ieee754_logf@PLT
	testl	%ebx, %ebx
	movss	(%rsp), %xmm10
	movss	16(%rsp), %xmm6
	movss	32(%rsp), %xmm9
	movss	48(%rsp), %xmm4
	movaps	64(%rsp), %xmm3
	je	.L17
	movss	.LC4(%rip), %xmm7
	movaps	%xmm10, %xmm1
	movss	116(%rsp), %xmm2
	movaps	%xmm2, %xmm4
	andps	%xmm3, %xmm1
	movss	%xmm0, 32(%rsp)
	movaps	%xmm9, %xmm0
	andps	%xmm7, %xmm4
	movaps	%xmm7, 16(%rsp)
	orps	%xmm4, %xmm1
	movaps	%xmm3, (%rsp)
	call	__ieee754_atan2f@PLT
	movss	32(%rsp), %xmm5
	movaps	%xmm5, %xmm1
	movaps	(%rsp), %xmm3
	movaps	16(%rsp), %xmm7
	movss	48(%rsp), %xmm4
	movss	80(%rsp), %xmm8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	ucomiss	%xmm5, %xmm8
	jbe	.L28
	movss	.LC5(%rip), %xmm7
	ucomiss	%xmm9, %xmm7
	jbe	.L28
	ucomiss	.LC12(%rip), %xmm5
	jb	.L71
	movaps	%xmm8, %xmm1
	movaps	%xmm5, %xmm0
	subss	%xmm5, %xmm1
	addss	%xmm8, %xmm0
	mulss	%xmm1, %xmm0
	movss	.LC8(%rip), %xmm1
	ucomiss	%xmm9, %xmm1
	jbe	.L72
	sqrtss	%xmm0, %xmm10
	movaps	%xmm9, %xmm0
	movss	%xmm7, 64(%rsp)
	movaps	%xmm3, 80(%rsp)
	addss	%xmm9, %xmm0
	movss	%xmm4, 48(%rsp)
	movss	%xmm5, 32(%rsp)
	movss	%xmm6, 16(%rsp)
	divss	%xmm10, %xmm0
	movss	%xmm10, (%rsp)
	movss	%xmm8, 96(%rsp)
	movss	%xmm2, 116(%rsp)
	call	__log1pf@PLT
	movss	64(%rsp), %xmm7
	testl	%ebx, %ebx
	mulss	%xmm0, %xmm7
	movaps	80(%rsp), %xmm3
	movss	16(%rsp), %xmm6
	movss	48(%rsp), %xmm4
	movss	(%rsp), %xmm10
	movss	32(%rsp), %xmm5
	movaps	%xmm3, 48(%rsp)
	movss	%xmm4, 32(%rsp)
	movss	%xmm7, 16(%rsp)
	movss	%xmm6, (%rsp)
	je	.L40
	movss	116(%rsp), %xmm2
	movaps	%xmm10, %xmm0
	movaps	%xmm2, %xmm1
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm7
	movaps	%xmm7, %xmm1
	movaps	48(%rsp), %xmm3
	movss	.LC4(%rip), %xmm7
	movss	(%rsp), %xmm6
	movss	32(%rsp), %xmm4
	movss	96(%rsp), %xmm8
.L41:
	movaps	%xmm4, %xmm5
	movaps	%xmm7, %xmm2
	andps	%xmm7, %xmm5
	andnps	%xmm1, %xmm2
	orps	%xmm5, %xmm2
	movaps	%xmm2, %xmm4
	movss	.LC13(%rip), %xmm2
	ucomiss	%xmm1, %xmm2
	jbe	.L78
.L48:
	mulss	%xmm1, %xmm1
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L68:
	jp	.L20
	jne	.L20
	movss	.LC5(%rip), %xmm10
	ucomiss	%xmm9, %xmm10
	ja	.L82
.L28:
	movaps	%xmm9, %xmm0
	movaps	%xmm9, %xmm1
	movaps	%xmm3, 80(%rsp)
	subss	%xmm5, %xmm0
	addss	%xmm5, %xmm1
	movss	%xmm2, 64(%rsp)
	movss	%xmm4, 48(%rsp)
	movss	%xmm6, 32(%rsp)
	movss	%xmm9, 16(%rsp)
	mulss	%xmm1, %xmm0
	movss	%xmm5, (%rsp)
	addss	%xmm0, %xmm8
	movaps	%xmm9, %xmm0
	addss	%xmm9, %xmm0
	movss	%xmm8, 160(%rsp)
	mulss	%xmm5, %xmm0
	movss	%xmm0, 164(%rsp)
	movq	160(%rsp), %xmm0
	call	__csqrtf@PLT
	movq	%xmm0, 152(%rsp)
	testl	%ebx, %ebx
	movss	16(%rsp), %xmm9
	movss	152(%rsp), %xmm8
	movss	(%rsp), %xmm5
	addss	%xmm9, %xmm8
	addss	156(%rsp), %xmm5
	movss	32(%rsp), %xmm6
	movss	48(%rsp), %xmm4
	movss	64(%rsp), %xmm2
	movaps	80(%rsp), %xmm3
	jne	.L45
	movaps	%xmm8, %xmm2
	movss	.LC4(%rip), %xmm7
.L46:
	movss	%xmm2, 144(%rsp)
	movss	%xmm5, 148(%rsp)
	movaps	%xmm7, 48(%rsp)
	movaps	%xmm3, 32(%rsp)
	movss	%xmm4, 16(%rsp)
	movss	%xmm6, (%rsp)
	movq	144(%rsp), %xmm0
	call	__clogf@PLT
	movss	16(%rsp), %xmm4
	movq	%xmm0, 136(%rsp)
	movaps	%xmm4, %xmm2
	movaps	32(%rsp), %xmm3
	movaps	48(%rsp), %xmm7
	movss	136(%rsp), %xmm0
	andps	%xmm7, %xmm2
	movss	(%rsp), %xmm6
	andps	%xmm3, %xmm0
	orps	%xmm2, %xmm0
	movaps	%xmm0, %xmm4
	movss	140(%rsp), %xmm0
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L80:
	movaps	%xmm9, %xmm1
	movss	%xmm4, 48(%rsp)
	movss	.LC0(%rip), %xmm0
	movaps	%xmm3, 64(%rsp)
	movss	%xmm5, 32(%rsp)
	movss	%xmm2, 80(%rsp)
	movss	%xmm2, 16(%rsp)
	movss	%xmm9, (%rsp)
	call	__ieee754_hypotf@PLT
	movss	(%rsp), %xmm9
	movaps	%xmm0, %xmm10
	movaps	%xmm9, %xmm0
	addss	%xmm10, %xmm0
	movss	%xmm10, (%rsp)
	call	__ieee754_logf@PLT
	testl	%ebx, %ebx
	movss	(%rsp), %xmm10
	movss	16(%rsp), %xmm6
	movss	32(%rsp), %xmm5
	movss	48(%rsp), %xmm4
	movaps	64(%rsp), %xmm3
	je	.L11
	movss	80(%rsp), %xmm2
	movss	%xmm0, (%rsp)
	movaps	%xmm2, %xmm1
	movaps	%xmm10, %xmm0
	movss	%xmm4, 16(%rsp)
	movaps	%xmm3, 32(%rsp)
	call	__ieee754_atan2f@PLT
	movss	(%rsp), %xmm7
	movaps	%xmm7, %xmm1
	movaps	32(%rsp), %xmm3
	movss	.LC4(%rip), %xmm7
	movss	.LC0(%rip), %xmm8
	movss	16(%rsp), %xmm4
.L12:
	movaps	%xmm4, %xmm2
	andps	%xmm3, %xmm1
	andps	%xmm7, %xmm2
	orps	%xmm2, %xmm1
	movaps	%xmm1, %xmm4
.L78:
	movaps	%xmm8, %xmm6
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L45:
	movaps	%xmm2, %xmm1
	movss	.LC4(%rip), %xmm7
	andps	%xmm3, %xmm5
	andps	%xmm7, %xmm1
	orps	%xmm1, %xmm5
	movaps	%xmm5, %xmm2
	movaps	%xmm8, %xmm5
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L82:
	ucomiss	%xmm9, %xmm1
	jbe	.L70
	movaps	%xmm9, %xmm0
	sqrtss	%xmm9, %xmm5
	movss	%xmm8, 64(%rsp)
	movaps	%xmm3, 80(%rsp)
	addss	%xmm5, %xmm0
	movss	%xmm10, 48(%rsp)
	movss	%xmm4, 32(%rsp)
	movss	%xmm6, 16(%rsp)
	addss	%xmm0, %xmm0
	movss	%xmm5, (%rsp)
	movss	%xmm2, 116(%rsp)
	call	__log1pf@PLT
	movss	48(%rsp), %xmm10
	testl	%ebx, %ebx
	mulss	%xmm0, %xmm10
	movss	(%rsp), %xmm5
	movss	16(%rsp), %xmm6
	movss	32(%rsp), %xmm4
	movss	64(%rsp), %xmm8
	movaps	80(%rsp), %xmm3
	je	.L32
	movss	.LC4(%rip), %xmm7
	movaps	%xmm5, %xmm0
	movss	116(%rsp), %xmm2
	andps	%xmm7, %xmm2
	movss	%xmm8, 48(%rsp)
	movaps	%xmm3, 64(%rsp)
	movss	%xmm10, 16(%rsp)
	movaps	%xmm7, (%rsp)
	orps	.LC10(%rip), %xmm2
	movaps	%xmm2, %xmm1
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm10
	movaps	%xmm10, %xmm1
	movaps	(%rsp), %xmm7
	movss	32(%rsp), %xmm4
	movss	48(%rsp), %xmm8
	movaps	64(%rsp), %xmm3
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
	movss	%xmm0, 16(%rsp)
	movaps	%xmm10, %xmm1
	movaps	%xmm5, %xmm0
	movss	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movss	%xmm6, (%rsp)
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm7
	movaps	%xmm7, %xmm1
	movaps	48(%rsp), %xmm3
	movss	(%rsp), %xmm6
	movss	32(%rsp), %xmm4
.L13:
	movaps	%xmm4, %xmm2
	movss	.LC4(%rip), %xmm7
	andps	%xmm3, %xmm1
	andps	%xmm7, %xmm2
	orps	%xmm2, %xmm1
	movaps	%xmm1, %xmm4
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L70:
	movaps	%xmm4, %xmm1
	movss	%xmm8, 116(%rsp)
	movss	.LC11(%rip), %xmm7
	mulss	%xmm4, %xmm1
	movaps	%xmm9, %xmm11
	movaps	%xmm3, 96(%rsp)
	movss	%xmm6, 80(%rsp)
	addss	%xmm1, %xmm7
	movaps	%xmm1, %xmm0
	movss	%xmm4, 64(%rsp)
	movss	%xmm10, 48(%rsp)
	movss	%xmm9, 32(%rsp)
	sqrtss	%xmm7, %xmm7
	mulss	%xmm9, %xmm7
	movss	%xmm2, 120(%rsp)
	addss	%xmm7, %xmm0
	subss	%xmm1, %xmm7
	movaps	%xmm0, %xmm5
	mulss	%xmm10, %xmm7
	mulss	%xmm10, %xmm5
	sqrtss	%xmm7, %xmm7
	movss	%xmm7, (%rsp)
	sqrtss	%xmm5, %xmm5
	mulss	%xmm5, %xmm11
	movss	%xmm5, 16(%rsp)
	addss	%xmm7, %xmm11
	addss	%xmm11, %xmm11
	addss	%xmm11, %xmm0
	call	__log1pf@PLT
	movss	48(%rsp), %xmm10
	testl	%ebx, %ebx
	mulss	%xmm0, %xmm10
	movss	(%rsp), %xmm7
	movss	16(%rsp), %xmm5
	movss	32(%rsp), %xmm9
	movss	64(%rsp), %xmm4
	movss	80(%rsp), %xmm6
	movss	116(%rsp), %xmm8
	movaps	96(%rsp), %xmm3
	je	.L33
	addss	%xmm8, %xmm7
	movaps	%xmm9, %xmm0
	movss	120(%rsp), %xmm2
	movaps	%xmm2, %xmm4
	movss	%xmm10, 48(%rsp)
	addss	%xmm5, %xmm0
	movss	%xmm8, 32(%rsp)
	movaps	%xmm7, %xmm1
	movss	.LC4(%rip), %xmm7
	movaps	%xmm3, (%rsp)
	andps	%xmm7, %xmm4
	andps	%xmm3, %xmm1
	movaps	%xmm7, 16(%rsp)
	orps	%xmm4, %xmm1
	call	__ieee754_atan2f@PLT
	movss	48(%rsp), %xmm10
	movaps	%xmm10, %xmm1
	movaps	(%rsp), %xmm3
	movaps	16(%rsp), %xmm7
	movss	32(%rsp), %xmm8
	movss	64(%rsp), %xmm4
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movss	%xmm0, 16(%rsp)
	movaps	%xmm9, %xmm1
	movaps	%xmm10, %xmm0
	movss	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movss	%xmm6, (%rsp)
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm5
	movaps	%xmm5, %xmm1
	movss	(%rsp), %xmm6
	movss	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm3
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L69:
	movaps	%xmm5, %xmm0
	movss	%xmm6, 116(%rsp)
	movaps	%xmm4, %xmm11
	movss	%xmm4, 80(%rsp)
	addss	%xmm5, %xmm0
	movss	.LC9(%rip), %xmm1
	mulss	%xmm4, %xmm11
	movaps	%xmm9, %xmm13
	movaps	%xmm9, %xmm12
	movss	%xmm7, (%rsp)
	mulss	%xmm5, %xmm13
	movss	%xmm9, 64(%rsp)
	mulss	%xmm5, %xmm0
	movss	%xmm5, 32(%rsp)
	addss	%xmm11, %xmm1
	movss	%xmm8, 124(%rsp)
	movaps	%xmm3, 96(%rsp)
	movss	%xmm2, 120(%rsp)
	addss	%xmm1, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm10, %xmm1
	mulss	%xmm11, %xmm0
	addss	%xmm0, %xmm1
	sqrtss	%xmm1, %xmm1
	addss	%xmm1, %xmm10
	movaps	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	divss	%xmm10, %xmm1
	addss	%xmm11, %xmm10
	addss	%xmm11, %xmm1
	mulss	%xmm7, %xmm1
	sqrtss	%xmm1, %xmm1
	divss	%xmm1, %xmm13
	movss	%xmm1, 48(%rsp)
	mulss	%xmm1, %xmm12
	mulss	%xmm13, %xmm0
	movss	%xmm13, 16(%rsp)
	addss	%xmm0, %xmm12
	addss	%xmm12, %xmm12
	movaps	%xmm12, %xmm0
	addss	%xmm10, %xmm0
	call	__log1pf@PLT
	movss	(%rsp), %xmm7
	testl	%ebx, %ebx
	mulss	%xmm7, %xmm0
	movss	16(%rsp), %xmm13
	movss	32(%rsp), %xmm5
	movss	48(%rsp), %xmm1
	movss	64(%rsp), %xmm9
	movss	%xmm0, (%rsp)
	movss	80(%rsp), %xmm4
	movss	116(%rsp), %xmm6
	movaps	96(%rsp), %xmm3
	je	.L26
	addss	%xmm13, %xmm5
	movss	.LC4(%rip), %xmm7
	movss	120(%rsp), %xmm2
	addss	%xmm1, %xmm9
	movss	%xmm4, 48(%rsp)
	movaps	%xmm2, %xmm4
	movaps	%xmm7, 32(%rsp)
	andps	%xmm7, %xmm4
	andps	%xmm3, %xmm5
	movaps	%xmm9, %xmm0
	movaps	%xmm3, 16(%rsp)
	orps	%xmm4, %xmm5
	movaps	%xmm5, %xmm1
	call	__ieee754_atan2f@PLT
	movss	(%rsp), %xmm1
	movaps	16(%rsp), %xmm3
	movaps	32(%rsp), %xmm7
	movss	48(%rsp), %xmm4
	movss	124(%rsp), %xmm8
	jmp	.L12
.L72:
	movaps	%xmm5, %xmm11
	movss	%xmm6, 116(%rsp)
	movaps	%xmm4, %xmm14
	movss	%xmm4, 80(%rsp)
	addss	%xmm5, %xmm11
	movss	.LC9(%rip), %xmm1
	mulss	%xmm4, %xmm14
	movaps	%xmm9, %xmm13
	movaps	%xmm9, %xmm10
	movss	%xmm7, 64(%rsp)
	mulss	%xmm5, %xmm13
	movss	%xmm9, 48(%rsp)
	mulss	%xmm5, %xmm11
	movss	%xmm5, 16(%rsp)
	addss	%xmm14, %xmm1
	movaps	%xmm14, %xmm12
	movaps	%xmm3, 96(%rsp)
	movss	%xmm8, 124(%rsp)
	addss	%xmm1, %xmm11
	movaps	%xmm0, %xmm1
	movss	%xmm2, 120(%rsp)
	mulss	%xmm0, %xmm1
	mulss	%xmm14, %xmm11
	addss	%xmm11, %xmm1
	sqrtss	%xmm1, %xmm1
	addss	%xmm1, %xmm0
	movaps	%xmm5, %xmm1
	addss	%xmm0, %xmm12
	divss	%xmm0, %xmm11
	mulss	%xmm7, %xmm12
	movaps	%xmm11, %xmm0
	sqrtss	%xmm12, %xmm12
	divss	%xmm12, %xmm13
	movss	%xmm12, 32(%rsp)
	addss	%xmm14, %xmm0
	mulss	%xmm12, %xmm10
	mulss	%xmm13, %xmm1
	movss	%xmm13, (%rsp)
	addss	%xmm1, %xmm10
	addss	%xmm10, %xmm10
	addss	%xmm10, %xmm0
	call	__log1pf@PLT
	movss	64(%rsp), %xmm7
	testl	%ebx, %ebx
	mulss	%xmm7, %xmm0
	movss	(%rsp), %xmm13
	movss	16(%rsp), %xmm5
	movss	32(%rsp), %xmm12
	movss	48(%rsp), %xmm9
	movss	80(%rsp), %xmm4
	movss	116(%rsp), %xmm6
	movaps	96(%rsp), %xmm3
	je	.L43
	addss	%xmm13, %xmm5
	movss	.LC4(%rip), %xmm7
	movss	120(%rsp), %xmm2
	movss	%xmm4, 64(%rsp)
	movaps	%xmm2, %xmm4
	movss	%xmm0, 48(%rsp)
	movaps	%xmm9, %xmm0
	andps	%xmm7, %xmm4
	movss	%xmm6, 32(%rsp)
	andps	%xmm3, %xmm5
	addss	%xmm12, %xmm0
	movaps	%xmm7, 16(%rsp)
	orps	%xmm4, %xmm5
	movaps	%xmm3, (%rsp)
	movaps	%xmm5, %xmm1
	call	__ieee754_atan2f@PLT
	movss	48(%rsp), %xmm10
	movaps	%xmm10, %xmm1
	movaps	(%rsp), %xmm3
	movaps	16(%rsp), %xmm7
	movss	32(%rsp), %xmm6
	movss	64(%rsp), %xmm4
	movss	124(%rsp), %xmm8
	jmp	.L41
.L71:
	movaps	%xmm9, %xmm1
	movss	%xmm7, 64(%rsp)
	movaps	%xmm8, %xmm0
	movss	%xmm4, 48(%rsp)
	movaps	%xmm3, 80(%rsp)
	movss	%xmm5, 32(%rsp)
	movss	%xmm6, 16(%rsp)
	movss	%xmm2, 96(%rsp)
	movss	%xmm9, (%rsp)
	movss	%xmm8, 116(%rsp)
	call	__ieee754_hypotf@PLT
	movss	(%rsp), %xmm9
	movaps	%xmm0, %xmm11
	movaps	%xmm9, %xmm10
	movaps	%xmm9, %xmm0
	addss	%xmm9, %xmm10
	movss	%xmm11, (%rsp)
	addss	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	call	__log1pf@PLT
	movss	64(%rsp), %xmm7
	testl	%ebx, %ebx
	mulss	%xmm0, %xmm7
	movaps	80(%rsp), %xmm3
	movss	16(%rsp), %xmm6
	movss	48(%rsp), %xmm4
	movss	(%rsp), %xmm11
	movss	32(%rsp), %xmm5
	movaps	%xmm3, 48(%rsp)
	movss	%xmm4, 32(%rsp)
	movss	%xmm7, 16(%rsp)
	movss	%xmm6, (%rsp)
	je	.L44
	movss	96(%rsp), %xmm2
	movaps	%xmm11, %xmm0
	movaps	%xmm2, %xmm1
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm7
	movaps	%xmm7, %xmm1
	movss	(%rsp), %xmm6
	movss	.LC4(%rip), %xmm7
	movss	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm3
	movss	116(%rsp), %xmm8
	jmp	.L41
.L32:
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm5, %xmm1
	movss	%xmm4, 32(%rsp)
	movaps	%xmm8, %xmm0
	movss	%xmm10, 16(%rsp)
	movss	%xmm6, (%rsp)
.L76:
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm10
	movaps	%xmm10, %xmm1
	movss	(%rsp), %xmm6
	movss	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm3
	jmp	.L13
.L33:
	addss	%xmm8, %xmm7
	movaps	%xmm9, %xmm1
	movaps	%xmm3, 48(%rsp)
	addss	%xmm5, %xmm1
	movss	%xmm4, 32(%rsp)
	movss	%xmm10, 16(%rsp)
	movaps	%xmm7, %xmm0
	movss	%xmm6, (%rsp)
	jmp	.L76
.L25:
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm9, %xmm1
	movss	%xmm4, 32(%rsp)
	movaps	%xmm11, %xmm0
	movss	%xmm6, 16(%rsp)
.L77:
	call	__ieee754_atan2f@PLT
	movss	(%rsp), %xmm1
	movss	16(%rsp), %xmm6
	movss	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm3
	jmp	.L13
.L26:
	addss	%xmm13, %xmm5
	movss	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	addss	%xmm9, %xmm1
	movss	%xmm6, 16(%rsp)
	movaps	%xmm5, %xmm0
	jmp	.L77
.L43:
	addss	%xmm13, %xmm5
	movaps	%xmm9, %xmm1
	movss	%xmm0, 16(%rsp)
	addss	%xmm12, %xmm1
	movss	%xmm4, 32(%rsp)
	movaps	%xmm3, 48(%rsp)
	movaps	%xmm5, %xmm0
	movss	%xmm6, (%rsp)
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm10
	movaps	%xmm10, %xmm1
	movaps	48(%rsp), %xmm3
	movss	(%rsp), %xmm6
	movss	32(%rsp), %xmm4
.L42:
	movss	.LC4(%rip), %xmm7
	movaps	%xmm4, %xmm5
	movaps	%xmm7, %xmm2
	andps	%xmm7, %xmm5
	andnps	%xmm1, %xmm2
	orps	%xmm5, %xmm2
	movaps	%xmm2, %xmm4
	movss	.LC13(%rip), %xmm2
	ucomiss	%xmm1, %xmm2
	jbe	.L47
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L40:
	movaps	%xmm10, %xmm1
.L75:
	movaps	%xmm5, %xmm0
	call	__ieee754_atan2f@PLT
	movss	16(%rsp), %xmm7
	movaps	%xmm7, %xmm1
	movss	(%rsp), %xmm6
	movss	32(%rsp), %xmm4
	movaps	48(%rsp), %xmm3
	jmp	.L42
.L44:
	movaps	%xmm11, %xmm1
	jmp	.L75
	.size	__kernel_casinhf, .-__kernel_casinhf
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1065353216
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC1:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC2:
	.long	1258291200
	.align 4
.LC3:
	.long	1060205080
	.section	.rodata.cst16
	.align 16
.LC4:
	.long	2147483648
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC5:
	.long	1056964608
	.align 4
.LC6:
	.long	847249408
	.align 4
.LC7:
	.long	1069547520
	.align 4
.LC8:
	.long	679477248
	.align 4
.LC9:
	.long	1073741824
	.section	.rodata.cst16
	.align 16
.LC10:
	.long	1065353216
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4
	.align 4
.LC11:
	.long	1082130432
	.align 4
.LC12:
	.long	872415232
	.align 4
.LC13:
	.long	8388608
