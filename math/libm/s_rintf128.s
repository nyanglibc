	.text
	.globl	__addtf3
	.globl	__subtf3
	.p2align 4,,15
	.globl	__rintf128
	.type	__rintf128, @function
__rintf128:
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movaps	%xmm0, (%rsp)
	movq	8(%rsp), %rax
	movq	%rax, %rbx
	sarq	$48, %rbx
	andl	$32767, %ebx
	subq	$16383, %rbx
	cmpq	$111, %rbx
	jg	.L2
	shrq	$63, %rax
	leaq	TWO112.7079(%rip), %rdx
	movq	%rax, %rbp
	salq	$4, %rax
	movdqa	(%rdx,%rax), %xmm2
	movdqa	%xmm2, %xmm1
	movaps	%xmm2, 16(%rsp)
	call	__addtf3@PLT
	movdqa	16(%rsp), %xmm2
	movdqa	%xmm2, %xmm1
	call	__subtf3@PLT
	testq	%rbx, %rbx
	movaps	%xmm0, (%rsp)
	js	.L7
.L1:
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpq	$16384, %rbx
	movdqa	(%rsp), %xmm3
	jne	.L1
	movdqa	%xmm3, %xmm1
	movdqa	%xmm3, %xmm0
	call	__addtf3@PLT
	movaps	%xmm0, (%rsp)
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	8(%rsp), %rdx
	movabsq	$9223372036854775807, %rax
	movdqa	(%rsp), %xmm4
	andq	%rax, %rdx
	movq	%rbp, %rax
	movaps	%xmm4, 16(%rsp)
	salq	$63, %rax
	orq	%rax, %rdx
	movq	%rdx, 24(%rsp)
	movdqa	16(%rsp), %xmm5
	movaps	%xmm5, (%rsp)
	movdqa	(%rsp), %xmm0
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__rintf128, .-__rintf128
	.weak	rintf128
	.set	rintf128,__rintf128
	.section	.rodata
	.align 32
	.type	TWO112.7079, @object
	.size	TWO112.7079, 32
TWO112.7079:
	.long	0
	.long	0
	.long	0
	.long	1081016320
	.long	0
	.long	0
	.long	0
	.long	-1066467328
