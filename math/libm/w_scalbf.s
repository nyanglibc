	.text
	.p2align 4,,15
	.globl	__scalbf
	.type	__scalbf, @function
__scalbf:
	subq	$24, %rsp
	movss	%xmm1, 12(%rsp)
	movss	%xmm0, 8(%rsp)
	call	__ieee754_scalbf@PLT
	movss	.LC0(%rip), %xmm1
	movaps	%xmm0, %xmm4
	movss	.LC1(%rip), %xmm5
	andps	%xmm1, %xmm4
	movss	8(%rsp), %xmm2
	movss	12(%rsp), %xmm3
	ucomiss	%xmm4, %xmm5
	jb	.L2
	ucomiss	.LC2(%rip), %xmm0
	jp	.L1
	je	.L2
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	ucomiss	%xmm0, %xmm0
	jp	.L13
	ucomiss	.LC1(%rip), %xmm4
	jbe	.L6
	andps	%xmm1, %xmm2
	ucomiss	.LC1(%rip), %xmm2
	ja	.L1
.L9:
	andps	%xmm1, %xmm3
	ucomiss	.LC1(%rip), %xmm3
	ja	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	ucomiss	.LC2(%rip), %xmm2
	jp	.L9
	jne	.L9
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	ucomiss	%xmm3, %xmm2
	jp	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$33, %fs:(%rax)
	jmp	.L1
	.size	__scalbf, .-__scalbf
	.weak	scalbf32
	.set	scalbf32,__scalbf
	.weak	scalbf
	.set	scalbf,__scalbf
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	2147483647
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC1:
	.long	2139095039
	.align 4
.LC2:
	.long	0
