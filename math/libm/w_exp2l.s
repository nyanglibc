	.text
	.p2align 4,,15
	.globl	__exp2l
	.type	__exp2l, @function
__exp2l:
	subq	$8, %rsp
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__ieee754_exp2l@PLT
	fld	%st(0)
	popq	%rax
	fabs
	popq	%rdx
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L2
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L1
	je	.L2
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fldt	16(%rsp)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L1
	movq	errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	addq	$8, %rsp
	ret
	.size	__exp2l, .-__exp2l
	.weak	exp2f64x
	.set	exp2f64x,__exp2l
	.weak	exp2l
	.set	exp2l,__exp2l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
