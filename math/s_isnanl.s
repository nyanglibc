	.text
	.p2align 4,,15
	.globl	__isnanl
	.hidden	__isnanl
	.type	__isnanl, @function
__isnanl:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdx
	movq	%rsi, %rcx
	leal	(%rdx,%rdx), %eax
	shrq	$32, %rcx
	movl	%ecx, %edx
	andl	$65534, %eax
	notl	%ecx
	andl	$2147483647, %edx
	orl	%edx, %esi
	movl	%esi, %edx
	negl	%edx
	orl	%esi, %edx
	shrl	$31, %edx
	orl	%eax, %edx
	negl	%eax
	andl	%ecx, %eax
	movl	$65534, %ecx
	subl	%edx, %ecx
	shrl	$31, %eax
	movl	%ecx, %edx
	shrl	$16, %edx
	orl	%edx, %eax
	ret
	.size	__isnanl, .-__isnanl
	.weak	isnanl
	.set	isnanl,__isnanl
