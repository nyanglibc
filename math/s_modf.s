	.text
	.p2align 4,,15
	.globl	__modf
	.type	__modf, @function
__modf:
	movq	%xmm0, %rcx
	movq	%xmm0, %rax
	sarq	$52, %rcx
	andl	$2047, %ecx
	subl	$1023, %ecx
	cmpl	$51, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L9
	movabsq	$4503599627370495, %rdx
	shrq	%cl, %rdx
	testq	%rdx, %rax
	jne	.L5
	movabsq	$-9223372036854775808, %rdx
	movsd	%xmm0, (%rdi)
	andq	%rdx, %rax
	movq	%rax, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1024, %ecx
	movsd	%xmm0, (%rdi)
	je	.L10
.L6:
	movabsq	$-9223372036854775808, %rdx
	andq	%rdx, %rax
	movq	%rax, -8(%rsp)
.L1:
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movabsq	$4503599627370495, %rdx
	movsd	%xmm0, -8(%rsp)
	testq	%rdx, %rax
	je	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	notq	%rdx
	andq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movq	-8(%rsp), %xmm1
	subsd	%xmm1, %xmm0
	movsd	%xmm1, (%rdi)
	movsd	%xmm0, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movabsq	$-9223372036854775808, %rdx
	movsd	%xmm0, -8(%rsp)
	andq	%rdx, %rax
	movq	%rax, (%rdi)
	movsd	-8(%rsp), %xmm0
	ret
	.size	__modf, .-__modf
	.weak	modff32x
	.set	modff32x,__modf
	.weak	modff64
	.set	modff64,__modf
	.weak	modf
	.set	modf,__modf
