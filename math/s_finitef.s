	.text
	.p2align 4,,15
	.globl	__finitef
	.hidden	__finitef
	.type	__finitef, @function
__finitef:
	movd	%xmm0, %eax
	andl	$2139095040, %eax
	subl	$2139095040, %eax
	shrl	$31, %eax
	ret
	.size	__finitef, .-__finitef
	.weak	finitef
	.set	finitef,__finitef
