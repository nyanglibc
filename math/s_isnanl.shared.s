	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isnanl
	.hidden	__GI___isnanl
	.type	__GI___isnanl, @function
__GI___isnanl:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdx
	movq	%rsi, %rcx
	leal	(%rdx,%rdx), %eax
	shrq	$32, %rcx
	movl	%ecx, %edx
	andl	$65534, %eax
	notl	%ecx
	andl	$2147483647, %edx
	orl	%edx, %esi
	movl	%esi, %edx
	negl	%edx
	orl	%esi, %edx
	shrl	$31, %edx
	orl	%eax, %edx
	negl	%eax
	andl	%ecx, %eax
	movl	$65534, %ecx
	subl	%edx, %ecx
	shrl	$31, %eax
	movl	%ecx, %edx
	shrl	$16, %edx
	orl	%edx, %eax
	ret
	.size	__GI___isnanl, .-__GI___isnanl
	.globl	__isnanl
	.set	__isnanl,__GI___isnanl
	.weak	isnanl
	.set	isnanl,__isnanl
