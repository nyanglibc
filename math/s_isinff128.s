	.text
	.p2align 4,,15
	.globl	__isinff128
	.hidden	__isinff128
	.type	__isinff128, @function
__isinff128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %rcx
	movq	-24(%rsp), %rsi
	movabsq	$9223372036854775807, %rdx
	movabsq	$9223090561878065152, %rax
	andq	%rcx, %rdx
	sarq	$62, %rcx
	xorq	%rdx, %rax
	orq	%rsi, %rax
	movq	%rax, %rdx
	negq	%rax
	orq	%rdx, %rax
	sarq	$63, %rax
	notl	%eax
	andl	%ecx, %eax
	ret
	.size	__isinff128, .-__isinff128
	.weak	isinff128_do_not_use
	.set	isinff128_do_not_use,__isinff128
