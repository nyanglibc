	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__frexpl
	.type	__frexpl, @function
__frexpl:
	movq	16(%rsp), %rax
	movl	$0, (%rdi)
	movl	%eax, %ecx
	andw	$32767, %cx
	cmpw	$32767, %cx
	je	.L2
	movq	8(%rsp), %r8
	movswl	%cx, %edx
	movq	%r8, %rsi
	shrq	$32, %rsi
	orl	%r8d, %esi
	orl	%edx, %esi
	je	.L2
	testw	%cx, %cx
	je	.L5
	cwtl
	movl	$-16382, %ecx
.L6:
	fldt	8(%rsp)
	andl	$32768, %eax
	addl	%ecx, %edx
	orl	$16382, %eax
	movl	%edx, (%rdi)
	fstpt	-24(%rsp)
	movw	%ax, -16(%rsp)
	fldt	-24(%rsp)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	fldt	8(%rsp)
	fadd	%st(0), %st
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	fldt	8(%rsp)
	movl	$-16447, %ecx
	fmuls	.LC0(%rip)
	fstpt	8(%rsp)
	movq	16(%rsp), %rdx
	movswl	%dx, %eax
	andl	$32767, %edx
	jmp	.L6
	.size	__frexpl, .-__frexpl
	.weak	frexpf64x
	.set	frexpf64x,__frexpl
	.weak	frexpl
	.set	frexpl,__frexpl
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	1610612736
