 .section .rodata
 .align 1<<4
 .type mask,@object
mask:
 .byte 0xff, 0xff, 0xff, 0x7f
 .size mask,.-mask;
 .text
.globl __copysignf
.type __copysignf,@function
.align 1<<4
__copysignf:
 movss mask(%rip),%xmm3
 andps %xmm3,%xmm0
 andnps %xmm1,%xmm3
 orps %xmm3,%xmm0
 retq
.size __copysignf,.-__copysignf
.weak copysignf
copysignf = __copysignf
.weak copysignf32
copysignf32 = __copysignf
