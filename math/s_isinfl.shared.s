	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isinfl
	.hidden	__GI___isinfl
	.type	__GI___isinfl, @function
__GI___isinfl:
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rdx
	movq	%rdi, %rsi
	movswl	%dx, %edx
	shrq	$32, %rsi
	movl	%edx, %eax
	sarl	$14, %edx
	leal	-2147483648(%rsi), %ecx
	notl	%eax
	andl	$2, %edx
	andl	$32767, %eax
	orl	%edi, %ecx
	orl	%eax, %ecx
	movl	%ecx, %eax
	negl	%eax
	orl	%ecx, %eax
	movl	$1, %ecx
	sarl	$31, %eax
	subl	%edx, %ecx
	notl	%eax
	andl	%ecx, %eax
	ret
	.size	__GI___isinfl, .-__GI___isinfl
	.globl	__isinfl
	.set	__isinfl,__GI___isinfl
	.weak	isinfl
	.set	isinfl,__isinfl
