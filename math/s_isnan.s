	.text
	.p2align 4,,15
	.globl	__isnan
	.hidden	__isnan
	.type	__isnan, @function
__isnan:
	movq	%xmm0, %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rax, %rdx
	movabsq	$9218868437227405312, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__isnan, .-__isnan
	.weak	isnan
	.set	isnan,__isnan
