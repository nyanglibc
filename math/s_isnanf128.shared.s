	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___isnanf128
	.hidden	__GI___isnanf128
	.type	__GI___isnanf128, @function
__GI___isnanf128:
	movaps	%xmm0, -24(%rsp)
	movq	-24(%rsp), %rax
	movq	-16(%rsp), %rcx
	movq	%rax, %rdx
	negq	%rdx
	orq	%rax, %rdx
	movabsq	$9223372036854775807, %rax
	andq	%rcx, %rax
	shrq	$63, %rdx
	orq	%rax, %rdx
	movabsq	$9223090561878065152, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__GI___isnanf128, .-__GI___isnanf128
	.globl	__isnanf128
	.set	__isnanf128,__GI___isnanf128
	.weak	isnanf128_do_not_use
	.set	isnanf128_do_not_use,__isnanf128
