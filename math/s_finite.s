	.text
	.p2align 4,,15
	.globl	__finite
	.hidden	__finite
	.type	__finite, @function
__finite:
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rdx
	andq	%rdx, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__finite, .-__finite
	.weak	finite
	.set	finite,__finite
