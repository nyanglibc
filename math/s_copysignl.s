.globl __copysignl
.type __copysignl,@function
.align 1<<4
__copysignl:
 movl 32(%rsp),%edx
 movl 16(%rsp),%eax
 andl $0x8000,%edx
 andl $0x7fff,%eax
 orl %edx,%eax
 movl %eax,16(%rsp)
 fldt 8(%rsp)
 ret
.size __copysignl,.-__copysignl
.weak copysignl
copysignl = __copysignl
.weak copysignf64x
copysignf64x = __copysignl
