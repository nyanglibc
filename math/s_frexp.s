	.text
	.p2align 4,,15
	.globl	__frexp
	.type	__frexp, @function
__frexp:
	movq	%xmm0, %rax
	movq	%xmm0, %rdx
	sarq	$52, %rax
	andl	$2047, %eax
	cmpl	$2047, %eax
	je	.L2
	pxor	%xmm1, %xmm1
	movl	$1, %esi
	ucomisd	%xmm1, %xmm0
	setp	%cl
	cmovne	%esi, %ecx
	testb	%cl, %cl
	je	.L2
	testl	%eax, %eax
	leal	-1022(%rax), %ecx
	je	.L12
.L3:
	movabsq	$-9218868437227405313, %rax
	movl	%ecx, (%rdi)
	andq	%rax, %rdx
	movabsq	$4602678819172646912, %rax
	orq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addsd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	movl	%ecx, (%rdi)
	movsd	%xmm0, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	mulsd	.LC1(%rip), %xmm0
	movq	%xmm0, %rcx
	movq	%xmm0, %rdx
	sarq	$52, %rcx
	andl	$2047, %ecx
	subl	$1076, %ecx
	jmp	.L3
	.size	__frexp, .-__frexp
	.weak	frexpf32x
	.set	frexpf32x,__frexp
	.weak	frexpf64
	.set	frexpf64,__frexp
	.weak	frexp
	.set	frexp,__frexp
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	0
	.long	1129316352
