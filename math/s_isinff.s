	.text
	.p2align 4,,15
	.globl	__isinff
	.hidden	__isinff
	.type	__isinff, @function
__isinff:
	movd	%xmm0, %eax
	movd	%xmm0, %edx
	andl	$2147483647, %eax
	sarl	$30, %edx
	xorl	$2139095040, %eax
	movl	%eax, %ecx
	negl	%eax
	orl	%ecx, %eax
	sarl	$31, %eax
	notl	%eax
	andl	%edx, %eax
	ret
	.size	__isinff, .-__isinff
	.weak	isinff
	.set	isinff,__isinff
