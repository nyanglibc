	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___finite
	.hidden	__GI___finite
	.type	__GI___finite, @function
__GI___finite:
	movq	%xmm0, %rax
	movabsq	$9218868437227405312, %rdx
	andq	%rdx, %rax
	subq	%rdx, %rax
	shrq	$63, %rax
	ret
	.size	__GI___finite, .-__GI___finite
	.globl	__finite
	.set	__finite,__GI___finite
	.weak	finite
	.set	finite,__finite
