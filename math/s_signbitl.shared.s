	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__signbitl
	.type	__signbitl, @function
__signbitl:
	fldt	8(%rsp)
	fxam
	fnstsw	%ax
	fstp	%st(0)
	andl	$512, %eax
	ret
	.size	__signbitl, .-__signbitl
