	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__modf
	.type	__modf, @function
__modf:
	movq	%xmm0, %rcx
	movq	%xmm0, %rdx
	sarq	$52, %rcx
	andl	$2047, %ecx
	subl	$1023, %ecx
	cmpl	$51, %ecx
	jg	.L2
	testl	%ecx, %ecx
	js	.L8
	movabsq	$4503599627370495, %rax
	shrq	%cl, %rax
	testq	%rax, %rdx
	jne	.L5
	movabsq	$-9223372036854775808, %rax
	movsd	%xmm0, (%rdi)
	andq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movq	-8(%rsp), %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	mulsd	.LC0(%rip), %xmm0
	cmpl	$1024, %ecx
	movsd	%xmm0, (%rdi)
	je	.L9
.L6:
	movabsq	$-9223372036854775808, %rax
	andq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movq	-8(%rsp), %xmm0
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	movabsq	$4503599627370495, %rax
	testq	%rax, %rdx
	je	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	notq	%rax
	andq	%rdx, %rax
	movq	%rax, -8(%rsp)
	movq	-8(%rsp), %xmm1
	movsd	%xmm1, (%rdi)
	subsd	%xmm1, %xmm0
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movabsq	$-9223372036854775808, %rax
	andq	%rax, %rdx
	movq	%rdx, (%rdi)
	ret
	.size	__modf, .-__modf
	.weak	modff32x
	.set	modff32x,__modf
	.weak	modff64
	.set	modff64,__modf
	.weak	modf
	.set	modf,__modf
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
