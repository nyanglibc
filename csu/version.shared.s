	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_print_version
	.hidden	__libc_print_version
	.type	__libc_print_version, @function
__libc_print_version:
	leaq	banner(%rip), %rsi
	movl	$413, %edx
	movl	$1, %edi
	jmp	__GI___write
	.size	__libc_print_version, .-__libc_print_version
	.p2align 4,,15
	.globl	__gnu_get_libc_release
	.type	__gnu_get_libc_release, @function
__gnu_get_libc_release:
	leaq	__libc_release(%rip), %rax
	ret
	.size	__gnu_get_libc_release, .-__gnu_get_libc_release
	.weak	gnu_get_libc_release
	.set	gnu_get_libc_release,__gnu_get_libc_release
	.p2align 4,,15
	.globl	__gnu_get_libc_version
	.type	__gnu_get_libc_version, @function
__gnu_get_libc_version:
	leaq	__libc_version(%rip), %rax
	ret
	.size	__gnu_get_libc_version, .-__gnu_get_libc_version
	.weak	gnu_get_libc_version
	.set	gnu_get_libc_version,__gnu_get_libc_version
	.p2align 4,,15
	.globl	__libc_main
	.type	__libc_main, @function
__libc_main:
	leaq	banner(%rip), %rsi
	movl	$1, %edi
	subq	$8, %rsp
	movl	$413, %edx
	call	__GI___write
	xorl	%edi, %edi
	call	__GI__exit
	.size	__libc_main, .-__libc_main
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	banner, @object
	.size	banner, 414
banner:
	.ascii	"GNU C Library (GNU libc) release release version 2.33.\nCopy"
	.ascii	"right (C) 2021 Free Software Foundation, Inc.\nThis is free "
	.ascii	"software; see the source for copying co"
	.string	"nditions.\nThere is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\nPARTICULAR PURPOSE.\nCompiled by GNU CC version 7.3.0.\nlibc ABIs: UNIQUE IFUNC ABSOLUTE\nFor bug reporting instructions, please see:\n<https://www.gnu.org/software/libc/bugs.html>.\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	__libc_version, @object
	.size	__libc_version, 5
__libc_version:
	.string	"2.33"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__libc_release, @object
	.size	__libc_release, 8
__libc_release:
	.string	"release"
