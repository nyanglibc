	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/full"
.LC1:
	.string	"/dev/null"
	.text
	.p2align 4,,15
	.type	check_one_fd, @function
check_one_fd:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movl	%esi, %ebp
	movl	$1, %esi
	movl	%edi, %ebx
	subq	$144, %rsp
	call	__GI___fcntl64_nocancel
	cmpl	$-1, %eax
	je	.L11
.L1:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L11:
	cmpl	$9, rtld_errno(%rip)
	jne	.L1
	leaq	.LC1(%rip), %rax
	leaq	.LC0(%rip), %rdi
	xorl	%r12d, %r12d
	cmpl	$131072, %ebp
	movl	%ebp, %esi
	setne	%r12b
	cmove	%rax, %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	leaq	259(,%r12,4), %r12
	call	__GI___open_nocancel
	cmpl	%eax, %ebx
	jne	.L5
	movq	%rsp, %rsi
	movl	%ebx, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	jne	.L5
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$8192, %eax
	jne	.L5
	cmpq	%r12, 40(%rsp)
	je	.L1
	.p2align 4,,10
	.p2align 3
.L5:
#APP
# 81 "check_fds.c" 1
	hlt
# 0 "" 2
#NO_APP
	jmp	.L5
	.size	check_one_fd, .-check_one_fd
	.p2align 4,,15
	.globl	__libc_check_standard_fds
	.hidden	__libc_check_standard_fds
	.type	__libc_check_standard_fds, @function
__libc_check_standard_fds:
	subq	$8, %rsp
	movl	$131073, %esi
	xorl	%edi, %edi
	call	check_one_fd
	movl	$131072, %esi
	movl	$1, %edi
	call	check_one_fd
	movl	$131072, %esi
	movl	$2, %edi
	addq	$8, %rsp
	jmp	check_one_fd
	.size	__libc_check_standard_fds, .-__libc_check_standard_fds
	.hidden	rtld_errno
