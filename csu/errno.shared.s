	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.globl	errno
	.section	.tbss,"awT",@nobits
	.align 4
	.type	errno, @object
	.size	errno, 4
errno:
	.zero	4
	.globl	__libc_errno
	.hidden	__libc_errno
	.set	__libc_errno,errno
