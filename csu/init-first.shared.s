	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_init_first
	.type	__libc_init_first, @function
__libc_init_first:
	rep ret
	.size	__libc_init_first, .-__libc_init_first
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	_init_first, @function
_init_first:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	cmpb	$0, __libc_initial(%rip)
	je	.L4
	movq	__fpu_control@GOTPCREL(%rip), %rax
	movzwl	(%rax), %edi
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpw	%di, 80(%rax)
	je	.L4
	movq	%rdx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	__setfpucw
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
.L4:
	movq	__environ@GOTPCREL(%rip), %rax
	movl	%ebx, __libc_argc(%rip)
	movl	%ebx, %edi
	movq	%rsi, __libc_argv(%rip)
	movq	%rdx, (%rax)
	addq	$16, %rsp
	popq	%rbx
	jmp	__init_misc
	.size	_init_first, .-_init_first
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	_init_first
	.text
	.p2align 4,,15
	.globl	_dl_start
	.type	_dl_start, @function
_dl_start:
	subq	$8, %rsp
	call	__GI_abort
	.size	_dl_start, .-_dl_start
	.hidden	__libc_argv
	.comm	__libc_argv,8,8
	.hidden	__libc_argc
	.comm	__libc_argc,4,4
	.hidden	__init_misc
	.hidden	__setfpucw
	.hidden	__libc_initial
