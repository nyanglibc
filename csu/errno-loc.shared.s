	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___errno_location
	.hidden	__GI___errno_location
	.type	__GI___errno_location, @function
__GI___errno_location:
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__GI___errno_location, .-__GI___errno_location
	.globl	__errno_location
	.set	__errno_location,__GI___errno_location
