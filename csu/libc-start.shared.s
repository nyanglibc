	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\ninitialize program: %s\n\n"
.LC1:
	.string	"\ntransferring control: %s\n\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__libc_start_main
	.type	__libc_start_main, @function
__libc_start_main:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$152, %rsp
	testq	%r9, %r9
	movq	%rdi, 24(%rsp)
	movl	%esi, 20(%rsp)
	movq	%rdx, 8(%rsp)
	je	.L5
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	__GI___cxa_atexit
.L5:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	movl	(%rdx), %ebx
	andl	$2, %ebx
	jne	.L26
.L4:
	testq	%rbp, %rbp
	je	.L7
	movq	__environ@GOTPCREL(%rip), %rax
	movq	8(%rsp), %rsi
	movl	20(%rsp), %edi
	movq	(%rax), %rdx
	call	*%rbp
.L7:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movl	800(%rax), %r12d
	testl	%r12d, %r12d
	jne	.L27
.L9:
	testl	%ebx, %ebx
	jne	.L28
.L18:
	leaq	32(%rsp), %rdi
	call	__GI__setjmp
	testl	%eax, %eax
	jne	.L20
#APP
# 325 "../csu/libc-start.c" 1
	movq %fs:768,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 104(%rsp)
#APP
# 326 "../csu/libc-start.c" 1
	movq %fs:760,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 112(%rsp)
	leaq	32(%rsp), %rax
#APP
# 329 "../csu/libc-start.c" 1
	movq %rax,%fs:768
# 0 "" 2
#NO_APP
	movq	__environ@GOTPCREL(%rip), %rax
	movq	8(%rsp), %rsi
	movl	20(%rsp), %edi
	movq	(%rax), %rdx
	movq	24(%rsp), %rax
	call	*%rax
.L21:
	movl	%eax, %edi
	call	__GI_exit
.L20:
	movq	216+__libc_pthread_functions(%rip), %rax
#APP
# 338 "../csu/libc-start.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	movq	200+__libc_pthread_functions(%rip), %rax
#APP
# 350 "../csu/libc-start.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
# 357 "../csu/libc-start.c" 1
	lock;decl (%rax); sete %dl
# 0 "" 2
#NO_APP
	testb	%dl, %dl
	jne	.L23
	movl	$60, %edx
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%edi, %edi
	movl	%edx, %eax
#APP
# 35 "../sysdeps/unix/sysv/linux/exit-thread.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L22
.L26:
	movq	8(%rsp), %rax
	leaq	.LC0(%rip), %rdi
	movq	(%rax), %rsi
	xorl	%eax, %eax
	call	*736(%rdx)
	jmp	.L4
.L23:
	xorl	%eax, %eax
	jmp	.L21
.L27:
	movq	792(%rax), %r14
	movq	_rtld_global@GOTPCREL(%rip), %rax
	xorl	%ebp, %ebp
	movq	(%rax), %r15
	leaq	1160(%r15), %r13
.L12:
	cmpl	%ebp, %r12d
	jbe	.L9
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.L16
	movq	_rtld_global@GOTPCREL(%rip), %rdx
	leaq	2568(%rdx), %rcx
	cmpq	%rcx, %r15
	je	.L29
	movq	%rbp, %rdi
	salq	$4, %rdi
	addq	%r13, %rdi
.L15:
	call	*%rax
.L16:
	movq	64(%r14), %r14
	addq	$1, %rbp
	jmp	.L12
.L29:
	movq	%rbp, %rcx
	salq	$4, %rcx
	leaq	3728(%rdx,%rcx), %rdi
	jmp	.L15
.L28:
	movq	8(%rsp), %rax
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	leaq	.LC1(%rip), %rdi
	movq	(%rax), %rsi
	xorl	%eax, %eax
	call	*736(%rdx)
	jmp	.L18
	.size	__libc_start_main, .-__libc_start_main
	.hidden	__libc_pthread_functions
