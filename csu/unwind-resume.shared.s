	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libgcc_s.so.1"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"libgcc_s.so.1 must be installed for unwinding to work\n"
	.section	.rodata.str1.1
.LC2:
	.string	"_Unwind_Resume"
.LC3:
	.string	"__gcc_personality_v0"
#NO_APP
	.section	.text.unlikely,"ax",@progbits
	.globl	__libgcc_s_init
	.hidden	__libgcc_s_init
	.type	__libgcc_s_init, @function
__libgcc_s_init:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	movl	$-2147483646, %esi
	subq	$8, %rsp
	call	__GI___libc_dlopen_mode
	testq	%rax, %rax
	jne	.L2
.L3:
	leaq	.LC1(%rip), %rdi
	call	__GI___libc_fatal
.L2:
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	__GI___libc_dlsym
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L3
	leaq	.LC3(%rip), %rsi
	movq	%rbp, %rdi
	call	__GI___libc_dlsym
	testq	%rax, %rax
	je	.L3
	movq	%rbx, %rdx
#APP
# 52 "../sysdeps/gnu/unwind-resume.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
# 48 "../sysdeps/gnu/unwind-resume.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
#NO_APP
	movq	%rax, libgcc_s_personality(%rip)
	movq	%rdx, __libgcc_s_resume(%rip)
	popq	%rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__libgcc_s_init, .-__libgcc_s_init
	.text
	.p2align 4,,15
	.globl	_Unwind_Resume
	.type	_Unwind_Resume, @function
_Unwind_Resume:
	subq	$24, %rsp
	movq	__libgcc_s_resume(%rip), %rax
	testq	%rax, %rax
	je	.L15
.L13:
#APP
# 66 "../sysdeps/gnu/unwind-resume.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rdi, 8(%rsp)
	call	__libgcc_s_init
	movq	__libgcc_s_resume(%rip), %rax
	movq	8(%rsp), %rdi
	jmp	.L13
	.size	_Unwind_Resume, .-_Unwind_Resume
	.p2align 4,,15
	.globl	__gcc_personality_v0
	.type	__gcc_personality_v0, @function
__gcc_personality_v0:
	subq	$40, %rsp
	movq	libgcc_s_personality(%rip), %rax
	testq	%rax, %rax
	je	.L19
.L17:
#APP
# 80 "../sysdeps/gnu/unwind-resume.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	addq	$40, %rsp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r8, 24(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movl	%esi, 4(%rsp)
	movl	%edi, (%rsp)
	call	__libgcc_s_init
	movq	libgcc_s_personality(%rip), %rax
	movq	24(%rsp), %r8
	movq	16(%rsp), %rcx
	movq	8(%rsp), %rdx
	movl	4(%rsp), %esi
	movl	(%rsp), %edi
	jmp	.L17
	.size	__gcc_personality_v0, .-__gcc_personality_v0
	.local	libgcc_s_personality
	.comm	libgcc_s_personality,8,8
	.hidden	__libgcc_s_resume
	.comm	__libgcc_s_resume,8,8
