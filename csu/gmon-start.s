	.text
	.p2align 4,,15
	.globl	__gmon_start__
	.type	__gmon_start__, @function
__gmon_start__:
	movl	called.4615(%rip), %eax
	testl	%eax, %eax
	je	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	_start(%rip), %rdi
	leaq	etext(%rip), %rsi
	subq	$8, %rsp
	movl	$1, called.4615(%rip)
	call	__monstartup@PLT
	movq	_mcleanup@GOTPCREL(%rip), %rdi
	addq	$8, %rsp
	jmp	atexit@PLT
	.size	__gmon_start__, .-__gmon_start__
	.local	called.4615
	.comm	called.4615,4,4
