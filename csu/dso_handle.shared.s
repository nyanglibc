	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	__dso_handle
	.globl	__dso_handle
	.section	.data.rel.ro.local,"aw",@progbits
	.align 8
	.type	__dso_handle, @object
	.size	__dso_handle, 8
__dso_handle:
	.quad	__dso_handle
