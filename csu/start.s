.globl _start
.type _start,@function
.align 1<<4
_start: 
 xorl %ebp, %ebp
 mov %rdx, %r9
 popq %rsi
 mov %rsp, %rdx
 and $~15, %rsp
 pushq %rax
 pushq %rsp
 mov __libc_csu_fini@GOTPCREL(%rip), %r8
 mov __libc_csu_init@GOTPCREL(%rip), %rcx
 mov main@GOTPCREL(%rip), %rdi
 call *__libc_start_main@GOTPCREL(%rip)
 hlt
.size _start,.-_start;
 .data
 .globl __data_start
__data_start:
 .long 0
 .weak data_start
 data_start = __data_start
