	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"cannot set %fs base address for thread-local storage"
	.text
	.p2align 4,,15
	.globl	__libc_setup_tls
	.type	__libc_setup_tls, @function
__libc_setup_tls:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	_dl_phdr(%rip), %rax
	movq	_dl_ns(%rip), %r13
	testq	%rax, %rax
	je	.L11
	movq	_dl_phnum(%rip), %rcx
	leaq	0(,%rcx,8), %rdx
	subq	%rcx, %rdx
	leaq	(%rax,%rdx,8), %rdx
	cmpq	%rdx, %rax
	jb	.L6
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$56, %rax
	cmpq	%rax, %rdx
	jbe	.L11
.L6:
	cmpl	$7, (%rax)
	jne	.L4
	movq	16(%rax), %r12
	addq	0(%r13), %r12
	movq	32(%rax), %rbx
	movq	40(%rax), %rbp
	movq	%r12, 16(%rsp)
	movq	48(%rax), %r12
	movq	%rbx, 8(%rsp)
	cmpq	$64, %r12
	jbe	.L2
	xorl	%edi, %edi
	movq	%r12, %r15
	movq	%r12, %r14
	call	_dl_tls_static_surplus_init
	movq	_dl_tls_static_surplus(%rip), %rax
	xorl	%edx, %edx
	negq	%r15
	leaq	-1(%r12), %rsi
	leaq	-1(%rbp,%rax), %rax
	addq	%r12, %rax
	divq	%r12
	imulq	%r12, %rax
	movq	%rax, %rbx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%r12d, %r12d
	movq	$0, 16(%rsp)
	movq	$0, 8(%rsp)
	xorl	%ebp, %ebp
.L2:
	xorl	%edi, %edi
	movq	$-64, %r15
	movl	$64, %r14d
	call	_dl_tls_static_surplus_init
	movq	_dl_tls_static_surplus(%rip), %rax
	leaq	63(%rbp,%rax), %rsi
	movq	%rsi, %rbx
	movl	$63, %esi
	andq	$-64, %rbx
.L9:
	leaq	2496(%rbx,%r14), %rdi
	movq	%rsi, 24(%rsp)
	call	__sbrk
	movq	24(%rsp), %rsi
	movq	$62, _dl_static_dtv(%rip)
	addq	%rax, %rsi
	movq	%rsi, %rcx
	andq	%r15, %rcx
	testq	%r12, %r12
	je	.L12
	leaq	-1(%rbp,%r12), %rax
	movq	%r12, %r15
.L7:
	xorl	%edx, %edx
	movq	16(%rsp), %rsi
	movq	%rbx, %rdi
	divq	%r15
	movq	8(%rsp), %rdx
	movq	%rcx, 24(%rsp)
	movq	$0, 40+_dl_static_dtv(%rip)
	imulq	%rax, %r15
	subq	%r15, %rdi
	movq	%r15, 1112(%r13)
	addq	%rcx, %rdi
	movq	%rdi, 32+_dl_static_dtv(%rip)
	call	memcpy@PLT
	movq	24(%rsp), %rcx
	leaq	16+_dl_static_dtv(%rip), %rax
	movl	$4098, %edi
	leaq	(%rcx,%rbx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rsi, (%rsi)
	movl	$158, %eax
	movq	%rsi, 16(%rsi)
#APP
# 187 "libc-tls.c" 1
	syscall
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L8
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	_dl_stack_user(%rip), %rdx
	leaq	_dl_stack_used(%rip), %rax
	movq	%rdx, _dl_stack_user(%rip)
	movq	%rax, 8+_dl_stack_used(%rip)
	movq	%rax, _dl_stack_used(%rip)
	movq	%fs:16, %rax
	leaq	704(%rax), %rcx
	movq	%rdx, 704(%rax)
	movq	%rdx, 712(%rax)
	movq	%rcx, 8+_dl_stack_user(%rip)
	movq	16(%rsp), %rax
	movq	%r12, 1096(%r13)
	movq	%rbp, 1088(%r13)
	movq	$1, 1120(%r13)
	movq	%r13, 40+static_slotinfo(%rip)
	movq	%r15, _dl_tls_static_used(%rip)
	movq	%rax, 1072(%r13)
	movq	8(%rsp), %rax
	movq	%r14, _dl_tls_static_align(%rip)
	movq	%rcx, _dl_stack_user(%rip)
	movq	$64, static_slotinfo(%rip)
	movq	$1, _dl_tls_max_dtv_idx(%rip)
	movq	%rax, 1080(%r13)
	leaq	static_slotinfo(%rip), %rax
	movq	$1, _dl_tls_static_nelem(%rip)
	movq	%rax, _dl_tls_dtv_slotinfo_list(%rip)
	movq	_dl_tls_static_surplus(%rip), %rax
	leaq	63(%r15,%rax), %rax
	andq	$-64, %rax
	addq	$2496, %rax
	movq	%rax, _dl_tls_static_size(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbp, %rax
	movl	$1, %r15d
	jmp	.L7
	.size	__libc_setup_tls, .-__libc_setup_tls
	.comm	_dl_tls_generation,8,8
	.comm	_dl_tls_static_optional,8,8
	.comm	_dl_tls_static_surplus,8,8
	.comm	_dl_tls_static_align,8,8
	.comm	_dl_tls_static_used,8,8
	.comm	_dl_tls_static_size,8,8
	.comm	_dl_tls_static_nelem,8,8
	.comm	_dl_tls_dtv_slotinfo_list,8,8
	.comm	_dl_tls_dtv_gaps,1,1
	.comm	_dl_tls_max_dtv_idx,8,8
	.local	static_slotinfo
	.comm	static_slotinfo,1040,16
	.comm	_dl_static_dtv,1024,32
	.hidden	__libc_fatal
	.hidden	__sbrk
	.hidden	_dl_tls_static_surplus_init
