	.text
	.p2align 4,,15
	.globl	__libc_init_first
	.type	__libc_init_first, @function
__libc_init_first:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	movl	%edi, __libc_argc(%rip)
	movq	%rsi, __libc_argv(%rip)
	movq	%rdx, __environ(%rip)
	call	_dl_non_dynamic_init
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__init_misc
	.size	__libc_init_first, .-__libc_init_first
	.p2align 4,,15
	.globl	_dl_start
	.type	_dl_start, @function
_dl_start:
	subq	$8, %rsp
	call	abort
	.size	_dl_start, .-_dl_start
	.hidden	__libc_argv
	.comm	__libc_argv,8,8
	.hidden	__libc_argc
	.comm	__libc_argc,4,4
	.hidden	abort
	.hidden	__init_misc
	.hidden	_dl_non_dynamic_init
