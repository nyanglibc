 .weak __gmon_start__
 .section .init,"ax",@progbits
 .p2align 2
 .globl _init
 .hidden _init
 .type _init, @function
_init:

 subq $8, %rsp
 movq __gmon_start__@GOTPCREL(%rip), %rax
 testq %rax, %rax
 je .Lno_weak_fn
 call *%rax
.Lno_weak_fn:
 .section .fini,"ax",@progbits
 .p2align 2
 .globl _fini
 .hidden _fini
 .type _fini, @function
_fini:

 subq $8, %rsp
