	.text
	.p2align 4,,15
	.globl	__libc_csu_init
	.type	__libc_csu_init, @function
__libc_csu_init:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	leaq	__init_array_start(%rip), %r12
	pushq	%rbp
	leaq	__init_array_end(%rip), %rbp
	pushq	%rbx
	movl	%edi, %r13d
	movq	%rsi, %r14
	subq	%r12, %rbp
	subq	$8, %rsp
	sarq	$3, %rbp
	call	_init@PLT
	testq	%rbp, %rbp
	je	.L1
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movl	%r13d, %edi
	call	*(%r12,%rbx,8)
	addq	$1, %rbx
	cmpq	%rbx, %rbp
	jne	.L3
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__libc_csu_init, .-__libc_csu_init
	.p2align 4,,15
	.globl	__libc_csu_fini
	.type	__libc_csu_fini, @function
__libc_csu_fini:
	rep ret
	.size	__libc_csu_fini, .-__libc_csu_fini
	.hidden	__init_array_end
	.hidden	__init_array_start
