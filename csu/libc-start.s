	.text
	.p2align 4,,15
	.type	get_common_cache_info, @function
get_common_cache_info:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	(%rdi), %r9
	movl	(%rsi), %r11d
	movl	$3, -20(%rsp)
	testq	%r9, %r9
	jg	.L2
	movq	%rdx, %r9
	movl	$2, -20(%rsp)
	orl	$-1, %r14d
.L2:
	testb	$16, 35+_dl_x86_cpu_features(%rip)
	je	.L3
	movl	8+_dl_x86_cpu_features(%rip), %ebx
	movl	4+_dl_x86_cpu_features(%rip), %eax
	movl	_dl_x86_cpu_features(%rip), %r13d
	movl	%ebx, -12(%rsp)
	movl	12+_dl_x86_cpu_features(%rip), %ebx
	cmpl	$3, %eax
	movl	%eax, -8(%rsp)
	movl	%ebx, -4(%rsp)
	jg	.L94
	movb	$1, -13(%rsp)
	xorl	%r15d, %r15d
.L4:
	movzbl	26+_dl_x86_cpu_features(%rip), %r11d
.L25:
	testq	%r9, %r9
	jle	.L26
	testl	%r11d, %r11d
	je	.L26
	movq	%r9, %rax
	movl	%r11d, %ecx
	cqto
	idivq	%rcx
	movq	%rax, %r9
.L26:
	cmpb	$0, -13(%rsp)
	jne	.L3
	testl	%r15d, %r15d
	je	.L27
	movq	%rbp, %rax
	movslq	%r15d, %r15
	cqto
	idivq	%r15
	movq	%rax, %rbp
.L27:
	addq	%rbp, %r9
.L3:
	movq	%r9, (%rdi)
	movl	%r11d, (%rsi)
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L94:
	testl	%r14d, %r14d
	movl	$3, %r8d
	jne	.L95
.L5:
	movq	%r9, %r10
	xorl	%ecx, %ecx
	movb	$1, -13(%rsp)
	xorl	%r15d, %r15d
	movl	$4, %r12d
	movq	%rdi, %r9
	.p2align 4,,10
	.p2align 3
.L6:
	leal	1(%rcx), %edi
	movl	%r12d, %eax
#APP
# 538 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$1, %r13d
	je	.L96
.L8:
	movl	%eax, %ecx
	shrl	$5, %ecx
	andl	$7, %ecx
	cmpl	$2, %ecx
	je	.L9
	cmpl	$3, %ecx
	jne	.L7
	testb	$2, %r8b
	je	.L7
	shrl	$14, %eax
	shrl	%edx
	andl	$-3, %r8d
	andl	$1023, %eax
	movl	%eax, %r14d
	movl	%edx, %eax
	andl	$1, %eax
	movb	%al, -13(%rsp)
.L10:
	testl	%r8d, %r8d
	je	.L97
.L7:
	movl	%edi, %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L96:
	testb	$31, %al
	jne	.L8
	movq	%r9, %rdi
	movq	%r10, %r9
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	testb	$1, %r8b
	je	.L7
	shrl	$14, %eax
	andl	$-2, %r8d
	andl	$1023, %eax
	movl	%eax, %r15d
	jmp	.L10
.L97:
	cmpl	$6, -12(%rsp)
	movq	%r9, %rdi
	movq	%r10, %r9
	sete	-12(%rsp)
	movzbl	-12(%rsp), %edx
	cmpl	$3, %r13d
	sete	%al
	testb	%al, %dl
	jne	.L12
	cmpl	$10, -8(%rsp)
	jle	.L12
	testl	%r15d, %r15d
	movl	$1, %eax
	setg	%cl
	xorl	%r12d, %r12d
	cmpl	$3, -20(%rsp)
	sete	%r12b
	movl	%r12d, %edx
	andl	%ecx, %edx
	testl	%r14d, %r14d
	jg	.L13
	xorl	%eax, %eax
	cmpl	$2, -20(%rsp)
	sete	%al
	andl	%ecx, %eax
.L13:
	leal	(%rax,%rax), %r12d
	movq	%r9, %r10
	movl	%r8d, %ecx
	orl	%edx, %r12d
	jmp	.L14
.L15:
	cmpl	$512, %ecx
	je	.L98
.L16:
	movl	%r9d, %ecx
.L14:
	testl	%r12d, %r12d
	je	.L90
	leal	1(%rcx), %r9d
	movl	$11, %eax
#APP
# 595 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	andl	$65280, %ecx
	andl	$255, %ebx
	je	.L90
	testl	%ecx, %ecx
	je	.L90
	cmpl	$256, %ecx
	jne	.L15
	testb	$1, %r12b
	je	.L16
#APP
# 609 "../sysdeps/x86/dl-cacheinfo.h" 1
	bsr %r15d, %ecx
# 0 "" 2
#NO_APP
	orl	$-1, %r15d
	addl	$1, %ecx
	subl	$1, %ebx
	sall	%cl, %r15d
	andl	$-2, %r12d
	notl	%r15d
	andl	%ebx, %r15d
	jmp	.L16
.L95:
	movl	$1, %r8d
	orl	$-1, %r14d
	jmp	.L5
.L90:
	movq	%r10, %r9
.L12:
	testl	%r15d, %r15d
	je	.L20
	addl	$1, %r15d
.L20:
	testl	%r14d, %r14d
	jle	.L21
	cmpl	$2, -20(%rsp)
	je	.L22
	leal	1(%r14), %r11d
	jmp	.L25
.L98:
	testb	$2, %r12b
	je	.L16
	subl	$1, %ebx
	cmpl	$2, -20(%rsp)
	je	.L99
#APP
# 626 "../sysdeps/x86/dl-cacheinfo.h" 1
	bsr %r14d, %ecx
# 0 "" 2
#NO_APP
	orl	$-1, %r14d
	addl	$1, %ecx
	sall	%cl, %r14d
	notl	%r14d
	andl	%ebx, %r14d
.L28:
	andl	$-3, %r12d
	jmp	.L16
.L21:
	cmpl	$2, -20(%rsp)
	je	.L22
	testl	%r14d, %r14d
	je	.L25
	orl	$-1, %r11d
	jmp	.L25
.L22:
	testl	%r15d, %r15d
	je	.L25
	subl	$1, %r13d
	movl	%r15d, %r11d
	jne	.L25
	cmpl	$2, %r15d
	jbe	.L25
	cmpb	$0, -12(%rsp)
	je	.L25
	movl	-4(%rsp), %ecx
	subl	$55, %ecx
	cmpl	$38, %ecx
	ja	.L25
	movl	$1, %eax
	movabsq	$309242363905, %rdx
	movl	$2, %r11d
	salq	%cl, %rax
	testq	%rdx, %rax
	cmove	%r15d, %r11d
	jmp	.L25
.L99:
#APP
# 626 "../sysdeps/x86/dl-cacheinfo.h" 1
	bsr %r15d, %ecx
# 0 "" 2
#NO_APP
	orl	$-1, %r15d
	addl	$1, %ecx
	sall	%cl, %r15d
	notl	%r15d
	andl	%ebx, %r15d
	jmp	.L28
	.size	get_common_cache_info, .-get_common_cache_info
	.p2align 4,,15
	.type	handle_zhaoxin, @function
handle_zhaoxin:
	subl	$185, %edi
	movl	$-1431655765, %edx
	xorl	%ecx, %ecx
	movl	%edi, %eax
	pushq	%rbx
	mull	%edx
	movl	$4, %eax
	shrl	%edx
	leal	(%rdx,%rdx,2), %r8d
#APP
# 443 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L111
	xorl	%esi, %esi
	movl	$4, %r9d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L141:
	testb	%r10b, %r10b
	je	.L102
	cmpl	$3, %r8d
	je	.L103
.L104:
	cmpl	$3, %eax
	jne	.L106
	cmpl	$9, %r8d
	je	.L103
.L106:
	addl	$1, %esi
	movl	%r9d, %eax
	movl	%esi, %ecx
#APP
# 443 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L111
.L110:
	shrl	$5, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sete	%r10b
	cmpl	$1, %edx
	je	.L141
.L102:
	cmpl	$2, %edx
	jne	.L105
	testb	%r10b, %r10b
	je	.L105
	testl	%r8d, %r8d
	jne	.L106
.L103:
	movl	%edi, %eax
	movl	$-1431655765, %edx
	mull	%edx
	shrl	%edx
	leal	(%rdx,%rdx,2), %eax
	subl	%eax, %edi
	movl	%edi, %edx
	je	.L142
	subl	$1, %edx
	je	.L143
	andl	$4095, %ebx
	leaq	1(%rbx), %rax
	popq	%rbx
	ret
.L142:
	movl	%ebx, %eax
	movl	%ebx, %edx
	addl	$1, %ecx
	shrl	$22, %eax
	andl	$4095, %edx
	shrl	$12, %ebx
	addl	$1, %eax
	addl	$1, %edx
	andl	$1023, %ebx
	imull	%edx, %eax
	imull	%eax, %ecx
	leal	1(%rbx), %eax
	popq	%rbx
	imull	%ecx, %eax
	ret
.L111:
	xorl	%eax, %eax
	popq	%rbx
	ret
.L143:
	shrl	$22, %ebx
	leal	1(%rbx), %eax
	popq	%rbx
	ret
.L105:
	cmpl	$2, %eax
	jne	.L104
	cmpl	$6, %r8d
	je	.L103
	jmp	.L104
	.size	handle_zhaoxin, .-handle_zhaoxin
	.p2align 4,,15
	.type	handle_amd, @function
handle_amd:
	movl	$-2147483648, %esi
	pushq	%rbx
	movl	%esi, %eax
#APP
# 320 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	xorl	%edx, %edx
	cmpl	$190, %edi
	movl	%eax, %esi
	setg	%dl
	xorl	%eax, %eax
	subl	$2147483643, %edx
	cmpl	%esi, %edx
	ja	.L144
	movl	%edx, %eax
#APP
# 330 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$187, %edi
	jle	.L193
.L147:
	subl	$189, %edi
	cmpl	$7, %edi
	ja	.L148
	leaq	.L150(%rip), %rsi
	movslq	(%rsi,%rdi,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L150:
	.long	.L149-.L150
	.long	.L192-.L150
	.long	.L152-.L150
	.long	.L153-.L150
	.long	.L154-.L150
	.long	.L155-.L150
	.long	.L156-.L150
	.long	.L157-.L150
	.text
.L149:
	shrl	$16, %ecx
	movzbl	%cl, %eax
	cmpl	$255, %eax
	jne	.L144
	leal	0(,%rcx,4), %eax
	andl	$261120, %eax
.L144:
	popq	%rbx
	ret
.L193:
	addl	$3, %edi
	movl	%edx, %ecx
	jmp	.L147
.L154:
	xorl	%eax, %eax
	testb	$-16, %ch
	je	.L144
.L192:
	movzbl	%cl, %eax
	popq	%rbx
	ret
.L156:
	movl	%edx, %eax
	leaq	.L171(%rip), %rsi
	shrl	$12, %eax
	andl	$15, %eax
	movslq	(%rsi,%rax,4), %rcx
	addq	%rsi, %rcx
	jmp	*%rcx
	.section	.rodata
	.align 4
	.align 4
.L171:
	.long	.L144-.L171
	.long	.L144-.L171
	.long	.L144-.L171
	.long	.L169-.L171
	.long	.L144-.L171
	.long	.L169-.L171
	.long	.L182-.L171
	.long	.L169-.L171
	.long	.L173-.L171
	.long	.L169-.L171
	.long	.L174-.L171
	.long	.L175-.L171
	.long	.L176-.L171
	.long	.L177-.L171
	.long	.L185-.L171
	.long	.L178-.L171
	.text
.L153:
	movl	%ecx, %eax
	leaq	.L161(%rip), %rsi
	shrl	$12, %eax
	movl	%eax, %edx
	andl	$15, %edx
	movslq	(%rsi,%rdx,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L161:
	.long	.L160-.L161
	.long	.L160-.L161
	.long	.L160-.L161
	.long	.L169-.L161
	.long	.L160-.L161
	.long	.L169-.L161
	.long	.L182-.L161
	.long	.L169-.L161
	.long	.L173-.L161
	.long	.L169-.L161
	.long	.L174-.L161
	.long	.L175-.L161
	.long	.L176-.L161
	.long	.L177-.L161
	.long	.L185-.L161
	.long	.L168-.L161
	.text
.L152:
	xorl	%eax, %eax
	testb	$-16, %ch
	je	.L144
	movl	%ecx, %eax
	shrl	$6, %eax
	andl	$67107840, %eax
	popq	%rbx
	ret
.L155:
	xorl	%eax, %eax
	testb	$-16, %dh
	je	.L144
	leal	(%rdx,%rdx), %eax
	popq	%rbx
	andl	$2146959360, %eax
	ret
.L185:
	movl	$128, %eax
	popq	%rbx
	ret
.L182:
	movl	$8, %eax
	popq	%rbx
	ret
.L176:
	movl	$64, %eax
	popq	%rbx
	ret
.L175:
	movl	$48, %eax
	popq	%rbx
	ret
.L174:
	movl	$32, %eax
	popq	%rbx
	ret
.L173:
	movl	$16, %eax
	popq	%rbx
	ret
.L177:
	movl	$96, %eax
	popq	%rbx
	ret
.L178:
	leal	(%rdx,%rdx), %eax
	movzbl	%dl, %ecx
	xorl	%edx, %edx
	popq	%rbx
	andl	$2146959360, %eax
	divl	%ecx
	movl	%eax, %eax
	ret
.L168:
	movl	%ecx, %edx
	movzbl	%cl, %ecx
	shrl	$6, %edx
	movl	%edx, %eax
	xorl	%edx, %edx
	andl	$67107840, %eax
	divl	%ecx
	popq	%rbx
	movl	%eax, %eax
	ret
.L160:
	movq	%rdx, %rax
	popq	%rbx
	ret
.L169:
	xorl	%eax, %eax
	popq	%rbx
	ret
.L148:
	movl	%ecx, %eax
	shrl	$14, %eax
	andl	$261120, %eax
	popq	%rbx
	ret
.L157:
	xorl	%eax, %eax
	testb	$-16, %dh
	je	.L144
	movzbl	%dl, %eax
	popq	%rbx
	ret
	.size	handle_amd, .-handle_amd
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../sysdeps/x86/dl-cacheinfo.h"
.LC1:
	.string	"offset == 2"
	.text
	.p2align 4,,15
	.type	intel_check_word.isra.0, @function
intel_check_word.isra.0:
	testl	%esi, %esi
	js	.L279
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	leal	-185(%rdi), %edx
	movl	$-1431655765, %r10d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %eax
	mull	%r10d
	subq	$8, %rsp
	shrl	%edx
	testl	%esi, %esi
	leal	(%rdx,%rdx,2), %ebp
	je	.L195
	leaq	intel_02_known(%rip), %r13
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L283:
	cmpl	$9, %ebp
	movb	$1, (%rcx)
	je	.L195
.L197:
	shrl	$8, %esi
	testl	%esi, %esi
	je	.L195
.L223:
	movzbl	%sil, %eax
	cmpl	$64, %eax
	je	.L283
	cmpl	$255, %eax
	je	.L284
	cmpl	$73, %eax
	jne	.L214
	cmpl	$9, %ebp
	jne	.L214
	cmpl	$15, (%r8)
	jne	.L224
	cmpl	$6, (%r9)
	jne	.L224
	subl	$3, %edi
	movl	$6, %ebp
.L214:
	movl	%esi, %ebx
	movl	$68, %r10d
	xorl	%edx, %edx
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L225:
	movq	%rax, %r10
.L218:
	cmpq	%rdx, %r10
	jbe	.L197
.L219:
	leaq	(%rdx,%r10), %rax
	shrq	%rax
	leaq	0(%r13,%rax,8), %r11
	cmpb	(%r11), %bl
	je	.L285
	jb	.L225
	leaq	1(%rax), %rdx
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L285:
	movzbl	3(%r11), %edx
	cmpl	%ebp, %edx
	je	.L286
	cmpb	$6, %dl
	jne	.L197
	shrl	$8, %esi
	movb	$1, (%r12)
	testl	%esi, %esi
	jne	.L223
	.p2align 4,,10
	.p2align 3
.L195:
	xorl	%eax, %eax
.L194:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L224:
	movl	$9, %ebp
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L279:
	xorl	%eax, %eax
	ret
.L284:
	xorl	%ecx, %ecx
	movl	$4, %eax
#APP
# 155 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L195
	xorl	%esi, %esi
	movl	$4, %r8d
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L287:
	testb	%r9b, %r9b
	je	.L199
	cmpl	$3, %ebp
	je	.L204
.L200:
	cmpl	$3, %eax
	jne	.L205
	cmpl	$9, %ebp
	je	.L204
.L205:
	cmpl	$4, %eax
	jne	.L209
	cmpl	$12, %ebp
	je	.L204
.L209:
	addl	$1, %esi
	movl	%r8d, %eax
	movl	%esi, %ecx
#APP
# 155 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$31, %edx
	je	.L195
.L213:
	shrl	$5, %eax
	andl	$7, %eax
	cmpl	$1, %eax
	sete	%r9b
	cmpl	$1, %edx
	je	.L287
.L199:
	cmpl	$2, %edx
	jne	.L203
	testb	%r9b, %r9b
	je	.L203
	testl	%ebp, %ebp
	jne	.L205
.L204:
	subl	$185, %edi
	subl	%ebp, %edi
	je	.L288
	cmpl	$1, %edi
	je	.L289
	cmpl	$2, %edi
	jne	.L290
	andl	$4095, %ebx
	leaq	1(%rbx), %rax
	jmp	.L194
.L286:
	subl	$185, %edi
	subl	%ebp, %edi
	jne	.L220
	movl	4(%r11), %eax
	jmp	.L194
.L288:
	movl	%ebx, %eax
	movl	%ebx, %edx
	addl	$1, %ecx
	shrl	$22, %eax
	andl	$4095, %edx
	shrl	$12, %ebx
	addl	$1, %edx
	addl	$1, %eax
	imull	%edx, %eax
	movl	%ebx, %edx
	andl	$1023, %edx
	addl	$1, %edx
	imull	%ecx, %eax
	imull	%edx, %eax
	jmp	.L194
.L220:
	cmpl	$1, %edi
	je	.L291
	cmpl	$2, %edi
	jne	.L292
	movzbl	2(%r11), %eax
	jmp	.L194
.L289:
	movl	%ebx, %eax
	shrl	$22, %eax
	addl	$1, %eax
	jmp	.L194
.L291:
	movzbl	1(%r11), %eax
	jmp	.L194
.L203:
	cmpl	$2, %eax
	jne	.L200
	cmpl	$6, %ebp
	je	.L204
	jmp	.L200
.L290:
	leaq	__PRETTY_FUNCTION__.10214(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$183, %edx
	call	__assert_fail
.L292:
	leaq	__PRETTY_FUNCTION__.10214(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$231, %edx
	call	__assert_fail
	.size	intel_check_word.isra.0, .-intel_check_word.isra.0
	.p2align 4,,15
	.type	get_common_indices.constprop.2, @function
get_common_indices.constprop.2:
	pushq	%rbx
	movq	%rdx, %r8
	movq	%rcx, %r9
	movl	$1, %eax
#APP
# 324 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%edx, 32+_dl_x86_cpu_features(%rip)
	movl	%eax, %edx
	movl	%ebx, 24+_dl_x86_cpu_features(%rip)
	shrl	$8, %edx
	movl	%ecx, 28+_dl_x86_cpu_features(%rip)
	movl	%eax, 20+_dl_x86_cpu_features(%rip)
	andl	$15, %edx
	movl	%edx, (%rdi)
	movl	%eax, %edx
	shrl	$4, %edx
	andl	$15, %edx
	movl	%edx, (%rsi)
	movl	%eax, %edx
	shrl	$12, %edx
	andl	$240, %edx
	movl	%edx, (%r8)
	movl	%eax, %edx
	andl	$15, %edx
	movl	%edx, (%r9)
	cmpl	$15, (%rdi)
	je	.L299
.L294:
	cmpl	$6, 4+_dl_x86_cpu_features(%rip)
	jle	.L295
	movl	$7, %esi
	xorl	%ecx, %ecx
	movl	%esi, %eax
#APP
# 342 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 52+_dl_x86_cpu_features(%rip)
	movl	%ebx, 56+_dl_x86_cpu_features(%rip)
	movl	%esi, %eax
	movl	%ecx, 60+_dl_x86_cpu_features(%rip)
	movl	%edx, 64+_dl_x86_cpu_features(%rip)
	movl	$1, %ecx
#APP
# 347 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 212+_dl_x86_cpu_features(%rip)
	movl	%ebx, 216+_dl_x86_cpu_features(%rip)
	movl	%ecx, 220+_dl_x86_cpu_features(%rip)
	movl	%edx, 224+_dl_x86_cpu_features(%rip)
.L295:
	cmpl	$12, 4+_dl_x86_cpu_features(%rip)
	jle	.L296
	movl	$13, %eax
	movl	$1, %ecx
#APP
# 355 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 116+_dl_x86_cpu_features(%rip)
	movl	%ebx, 120+_dl_x86_cpu_features(%rip)
	movl	%ecx, 124+_dl_x86_cpu_features(%rip)
	movl	%edx, 128+_dl_x86_cpu_features(%rip)
.L296:
	cmpl	$24, 4+_dl_x86_cpu_features(%rip)
	jle	.L293
	movl	$25, %eax
	xorl	%ecx, %ecx
#APP
# 362 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 244+_dl_x86_cpu_features(%rip)
	movl	%ebx, 248+_dl_x86_cpu_features(%rip)
	movl	%ecx, 252+_dl_x86_cpu_features(%rip)
	movl	%edx, 256+_dl_x86_cpu_features(%rip)
.L293:
	popq	%rbx
	ret
.L299:
	shrl	$20, %eax
	movzbl	%al, %eax
	addl	$15, %eax
	movl	%eax, (%rdi)
	movl	(%r8), %eax
	addl	%eax, (%rsi)
	jmp	.L294
	.size	get_common_indices.constprop.2, .-get_common_indices.constprop.2
	.p2align 4,,15
	.type	handle_intel.constprop.5, @function
handle_intel.constprop.5:
	cmpl	$1, 4+_dl_x86_cpu_features(%rip)
	jbe	.L310
	pushq	%r15
	pushq	%r14
	movl	$1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$40, %rsp
	movb	$0, 30(%rsp)
	movb	$0, 31(%rsp)
	leaq	30(%rsp), %r13
	movl	$1, 12(%rsp)
	leaq	31(%rsp), %r12
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L306:
	movl	%edx, %r14d
.L304:
	movl	$2, %eax
#APP
# 272 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$1, %r14d
	movl	%edx, 8(%rsp)
	movl	%ecx, %r15d
	movl	%eax, %esi
	jne	.L303
	movzbl	%al, %eax
	xorb	%sil, %sil
	movl	%eax, 12(%rsp)
.L303:
	leaq	12+_dl_x86_cpu_features(%rip), %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L300
	leaq	12+_dl_x86_cpu_features(%rip), %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebx, %esi
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L300
	leaq	12+_dl_x86_cpu_features(%rip), %r9
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%r15d, %esi
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L300
	leaq	12+_dl_x86_cpu_features(%rip), %r9
	movl	8(%rsp), %esi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebp, %edi
	leaq	-4(%r9), %r8
	call	intel_check_word.isra.0
	testq	%rax, %rax
	jne	.L300
	cmpl	%r14d, 12(%rsp)
	leal	1(%r14), %edx
	ja	.L306
	subl	$191, %ebp
	cmpl	$5, %ebp
	ja	.L300
	cmpb	$0, 30(%rsp)
	jne	.L305
.L300:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L305:
	orq	$-1, %rax
	jmp	.L300
.L310:
	orq	$-1, %rax
	ret
	.size	handle_intel.constprop.5, .-handle_intel.constprop.5
	.p2align 4,,15
	.type	update_usable.constprop.3, @function
update_usable.constprop.3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$160, %rsp
	movl	28+_dl_x86_cpu_features(%rip), %r8d
	movl	60+_dl_x86_cpu_features(%rip), %esi
	movl	32+_dl_x86_cpu_features(%rip), %r12d
	movl	56+_dl_x86_cpu_features(%rip), %r14d
	movl	64+_dl_x86_cpu_features(%rip), %r13d
	movl	%r8d, %r9d
	movl	%r8d, %eax
	movl	%esi, %edx
	andl	$47718915, %r9d
	orl	44+_dl_x86_cpu_features(%rip), %r9d
	andl	$134217728, %eax
	andl	$4194737, %edx
	movl	%esi, %ebx
	orl	76+_dl_x86_cpu_features(%rip), %edx
	movl	%r8d, %ecx
	andl	$16, %ebx
	movl	%r12d, %edi
	andl	$1073741824, %ecx
	movl	%ebx, -116(%rsp)
	movl	92+_dl_x86_cpu_features(%rip), %ebx
	orl	%eax, %r9d
	movl	%r14d, %r10d
	movl	%r13d, %ebp
	orl	%ecx, %r9d
	movl	%esi, %ecx
	andl	$394821904, %edi
	andl	$436207616, %ecx
	movl	%ebx, %r11d
	orl	48+_dl_x86_cpu_features(%rip), %edi
	orl	%edx, %ecx
	movl	96+_dl_x86_cpu_features(%rip), %edx
	andl	$562826008, %r10d
	andl	$1130512, %ebp
	orl	72+_dl_x86_cpu_features(%rip), %r10d
	orl	80+_dl_x86_cpu_features(%rip), %ebp
	andl	$2097505, %r11d
	orl	108+_dl_x86_cpu_features(%rip), %r11d
	movl	%ebx, -108(%rsp)
	andl	$134217728, %edx
	orl	%edx, 112+_dl_x86_cpu_features(%rip)
	movl	184+_dl_x86_cpu_features(%rip), %edx
	movl	%edi, 48+_dl_x86_cpu_features(%rip)
	movl	%r9d, 44+_dl_x86_cpu_features(%rip)
	movl	%r10d, 72+_dl_x86_cpu_features(%rip)
	movl	%ecx, -112(%rsp)
	andl	$512, %edx
	movl	%ecx, 76+_dl_x86_cpu_features(%rip)
	movl	%ebp, 80+_dl_x86_cpu_features(%rip)
	movl	%r11d, 108+_dl_x86_cpu_features(%rip)
	orl	%edx, 200+_dl_x86_cpu_features(%rip)
	movl	212+_dl_x86_cpu_features(%rip), %r15d
	movl	%r15d, %ebx
	andl	$7168, %ebx
	orl	228+_dl_x86_cpu_features(%rip), %ebx
	testl	%eax, %eax
	movl	%ebx, 228+_dl_x86_cpu_features(%rip)
	je	.L313
	xorl	%ecx, %ecx
#APP
# 105 "../sysdeps/x86/cpu-features.c" 1
	xgetbv
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	andl	$6, %edx
	cmpl	$6, %edx
	je	.L379
.L315:
	andl	$393216, %eax
	cmpl	$393216, %eax
	je	.L380
.L319:
	movl	116+_dl_x86_cpu_features(%rip), %r8d
	orl	$67108864, 44+_dl_x86_cpu_features(%rip)
	movl	%r8d, %ebp
	andl	$23, %ebp
	orl	132+_dl_x86_cpu_features(%rip), %ebp
	cmpl	$12, 4+_dl_x86_cpu_features(%rip)
	movl	%ebp, 132+_dl_x86_cpu_features(%rip)
	jg	.L381
.L313:
	movl	-116(%rsp), %eax
	testl	%eax, %eax
	je	.L330
	orl	$8, 76+_dl_x86_cpu_features(%rip)
.L330:
	movl	248+_dl_x86_cpu_features(%rip), %eax
	testb	$1, %al
	je	.L331
	movl	264+_dl_x86_cpu_features(%rip), %edx
	andl	$8388608, %esi
	orl	%esi, 76+_dl_x86_cpu_features(%rip)
	andl	$4, %eax
	orl	$1, %edx
	orl	%edx, %eax
	movl	%eax, 264+_dl_x86_cpu_features(%rip)
.L331:
	movl	%edi, %edx
	xorl	%eax, %eax
	andl	$33024, %edx
	cmpl	$33024, %edx
	je	.L382
.L332:
	movl	%eax, 280+_dl_x86_cpu_features(%rip)
	addq	$160, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L381:
	xorl	%ecx, %ecx
	movl	$13, %eax
#APP
# 214 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	testl	%ebx, %ebx
	je	.L313
	addl	$127, %ebx
	andl	$-64, %ebx
	andb	$2, %r8b
	movl	%ebx, %eax
	movl	%ebx, 296+_dl_x86_cpu_features(%rip)
	movq	%rax, 288+_dl_x86_cpu_features(%rip)
	je	.L313
	movabsq	$687194767360, %rax
	movl	$576, -96(%rsp)
	leaq	-96(%rsp), %r10
	movq	%rax, -104(%rsp)
	movabsq	$1099511627936, %rax
	leaq	32(%rsp), %r9
	movq	%rax, 24(%rsp)
	movl	$3, %r11d
	movl	$2, %r8d
	movl	$238, %r13d
	movl	$13, %r14d
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L384:
	movl	%r14d, %eax
	movl	%r8d, %ecx
#APP
# 242 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$2, %r8d
	movl	%eax, (%r9)
	je	.L328
	movl	-4(%r9), %eax
	addl	-4(%r10), %eax
	andl	$2, %ecx
	je	.L378
	addl	$63, %eax
	andl	$-64, %eax
.L378:
	movl	%eax, (%r10)
.L327:
	cmpl	$32, %r11d
	je	.L383
.L328:
	addl	$1, %r8d
	addl	$1, %r11d
	addq	$4, %r10
	addq	$4, %r9
.L321:
	btl	%r8d, %r13d
	jc	.L384
	cmpl	$2, %r8d
	movl	$0, (%r9)
	je	.L328
	movl	-4(%r9), %eax
	addl	-4(%r10), %eax
	movl	%eax, (%r10)
	jmp	.L327
.L382:
	movl	%r12d, %eax
	andl	$1, %eax
	je	.L332
	andl	$125829120, %edi
	cmpl	$125829120, %edi
	je	.L385
	xorl	%eax, %eax
	jmp	.L332
.L380:
	andl	$54525952, %r13d
	orl	%r13d, 80+_dl_x86_cpu_features(%rip)
	jmp	.L319
.L379:
	testl	$268435456, %r8d
	je	.L316
	orl	$268435456, %r9d
	testb	$32, %r14b
	jne	.L386
.L317:
	movl	%r15d, %edx
	andl	$536875008, %r8d
	andl	$16, %edx
	orl	%r9d, %r8d
	orl	%edx, %ebx
	movl	%esi, %edx
	movl	%r8d, 44+_dl_x86_cpu_features(%rip)
	andl	$1536, %edx
	orl	-112(%rsp), %edx
	movl	%ebx, 228+_dl_x86_cpu_features(%rip)
	movl	%edx, 76+_dl_x86_cpu_features(%rip)
	movl	-108(%rsp), %edx
	andl	$2048, %edx
	orl	%edx, %r11d
	movl	%r11d, 108+_dl_x86_cpu_features(%rip)
.L316:
	movl	%eax, %edx
	andl	$224, %edx
	cmpl	$224, %edx
	jne	.L315
	testl	$65536, %r14d
	je	.L315
	movl	72+_dl_x86_cpu_features(%rip), %edx
	movl	%r14d, %ecx
	andl	$32, %r15d
	andl	$268435456, %ecx
	orl	%r15d, 228+_dl_x86_cpu_features(%rip)
	orl	$65536, %edx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$134217728, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$67108864, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$-2147483648, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$131072, %ecx
	orl	%ecx, %edx
	movl	%r14d, %ecx
	andl	$2097152, %r14d
	andl	$1073741824, %ecx
	orl	%ecx, %edx
	orl	%edx, %r14d
	movl	%esi, %edx
	andl	$22594, %edx
	orl	76+_dl_x86_cpu_features(%rip), %edx
	movl	%r14d, 72+_dl_x86_cpu_features(%rip)
	movl	%edx, 76+_dl_x86_cpu_features(%rip)
	movl	%r13d, %edx
	andl	$8388876, %edx
	orl	%edx, %ebp
	movl	%ebp, 80+_dl_x86_cpu_features(%rip)
	jmp	.L315
.L385:
	movl	44+_dl_x86_cpu_features(%rip), %edx
	testb	$32, %dh
	je	.L332
	movl	108+_dl_x86_cpu_features(%rip), %ecx
	movl	%ecx, %esi
	andl	$1, %esi
	je	.L332
	movl	%edx, %edi
	movl	%esi, %eax
	andl	$9961985, %edi
	cmpl	$9961985, %edi
	jne	.L332
	testl	$268435456, %edx
	je	.L340
	movl	72+_dl_x86_cpu_features(%rip), %esi
	testb	$32, %sil
	je	.L340
	movl	%edx, %edi
	movl	$3, %eax
	andl	$536875008, %edi
	cmpl	$536875008, %edi
	jne	.L332
	andb	$32, %cl
	je	.L340
	andl	$4194304, %edx
	je	.L340
	movl	%esi, %edx
	movl	$7, %eax
	andl	$1342373888, %edx
	cmpl	$1342373888, %edx
	jne	.L332
	movl	%esi, %eax
	sarl	$31, %eax
	andl	$8, %eax
	addl	$7, %eax
	jmp	.L332
.L383:
	movl	148(%rsp), %eax
	addl	20(%rsp), %eax
	je	.L313
	addl	$127, %eax
	orl	$2, %ebp
	andl	$-64, %eax
	movl	%ebp, 132+_dl_x86_cpu_features(%rip)
	movq	%rax, 288+_dl_x86_cpu_features(%rip)
	jmp	.L313
.L386:
	orl	$32, %r10d
	orl	$256, 276+_dl_x86_cpu_features(%rip)
	movl	%r10d, 72+_dl_x86_cpu_features(%rip)
	jmp	.L317
.L340:
	movl	$3, %eax
	jmp	.L332
	.size	update_usable.constprop.3, .-update_usable.constprop.3
	.section	.rodata.str1.1
.LC2:
	.string	"haswell"
.LC3:
	.string	"xeon_phi"
.LC4:
	.string	"../csu/libc-start.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"__ehdr_start.e_phentsize == sizeof *GL(dl_phdr)"
	.align 8
.LC6:
	.string	"Unexpected reloc type in static binary.\n"
	.section	.rodata.str1.1
.LC7:
	.string	"FATAL: kernel too old\n"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"FATAL: cannot determine kernel version\n"
	.text
	.p2align 4,,15
	.globl	__libc_start_main
	.hidden	__libc_start_main
	.type	__libc_start_main, @function
__libc_start_main:
	pushq	%r15
	pushq	%r14
	movslq	%esi, %rax
	pushq	%r13
	pushq	%r12
	movq	%r9, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$472, %rsp
	movq	%rdi, 24(%rsp)
	movl	%eax, 12(%rsp)
	leaq	8(%rdx,%rax,8), %rdi
	movq	528(%rsp), %rax
	movq	%rdx, 16(%rsp)
	movq	%r8, 32(%rsp)
	movq	%rdi, __environ(%rip)
	movq	%rax, __libc_stack_end(%rip)
	.p2align 4,,10
	.p2align 3
.L388:
	addq	$8, %rdi
	cmpq	$0, -8(%rdi)
	jne	.L388
	call	_dl_aux_init
	cmpq	$0, _dl_phdr(%rip)
	je	.L497
.L389:
	call	__libc_init_secure
	movq	__environ(%rip), %rdi
	call	__tunables_init
	xorl	%eax, %eax
	movl	$0, 144(%rsp)
	movl	$0, 148(%rsp)
#APP
# 398 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$1970169159, %ebx
	movl	$0, 152(%rsp)
	movl	%eax, 4+_dl_x86_cpu_features(%rip)
	jne	.L391
	cmpl	$1818588270, %ecx
	jne	.L391
	cmpl	$1231384169, %edx
	je	.L498
.L392:
	cmpl	$1953391939, %ebx
	jne	.L420
	cmpl	$1936487777, %ecx
	jne	.L420
	cmpl	$1215460705, %edx
	je	.L427
.L421:
	cmpl	$6, 4+_dl_x86_cpu_features(%rip)
	jle	.L435
	movl	$7, %esi
	xorl	%ecx, %ecx
	movl	%esi, %eax
#APP
# 342 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 52+_dl_x86_cpu_features(%rip)
	movl	%ebx, 56+_dl_x86_cpu_features(%rip)
	movl	%esi, %eax
	movl	%ecx, 60+_dl_x86_cpu_features(%rip)
	movl	%edx, 64+_dl_x86_cpu_features(%rip)
	movl	$1, %ecx
#APP
# 347 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 212+_dl_x86_cpu_features(%rip)
	movl	%ebx, 216+_dl_x86_cpu_features(%rip)
	movl	%ecx, 220+_dl_x86_cpu_features(%rip)
	movl	%edx, 224+_dl_x86_cpu_features(%rip)
.L435:
	cmpl	$12, 4+_dl_x86_cpu_features(%rip)
	jle	.L436
	movl	$13, %eax
	movl	$1, %ecx
#APP
# 355 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 116+_dl_x86_cpu_features(%rip)
	movl	%ebx, 120+_dl_x86_cpu_features(%rip)
	movl	%ecx, 124+_dl_x86_cpu_features(%rip)
	movl	%edx, 128+_dl_x86_cpu_features(%rip)
.L436:
	cmpl	$24, 4+_dl_x86_cpu_features(%rip)
	jle	.L437
	movl	$25, %eax
	xorl	%ecx, %ecx
#APP
# 362 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 244+_dl_x86_cpu_features(%rip)
	movl	%ebx, 248+_dl_x86_cpu_features(%rip)
	movl	%ecx, 252+_dl_x86_cpu_features(%rip)
	movl	%edx, 256+_dl_x86_cpu_features(%rip)
.L437:
	call	update_usable.constprop.3
	movl	$4, %eax
.L417:
	movl	32+_dl_x86_cpu_features(%rip), %edx
	testb	$1, %dh
	je	.L438
	orl	$1, 276+_dl_x86_cpu_features(%rip)
.L438:
	andb	$-128, %dh
	je	.L439
	orl	$2, 276+_dl_x86_cpu_features(%rip)
.L439:
	movl	148(%rsp), %edx
	cmpl	$1, %eax
	movl	144(%rsp), %r11d
	movl	%eax, _dl_x86_cpu_features(%rip)
	movq	$-1, 184(%rsp)
	movl	%edx, 12+_dl_x86_cpu_features(%rip)
	movl	152(%rsp), %edx
	movl	%r11d, 8+_dl_x86_cpu_features(%rip)
	movl	$0, 172(%rsp)
	movl	%edx, 16+_dl_x86_cpu_features(%rip)
	je	.L499
	cmpl	$3, %eax
	je	.L500
	cmpl	$2, %eax
	je	.L501
	orq	$-1, %rbx
	movq	%rbx, %r10
	movq	%rbx, 88(%rsp)
	movq	%rbx, %r14
	movq	%rbx, 80(%rsp)
	movq	%rbx, 72(%rsp)
	movq	%rbx, %r13
	movq	%rbx, 64(%rsp)
	movq	%rbx, 56(%rsp)
	movq	%rbx, %r15
	movq	%rbx, 48(%rsp)
	movq	%rbx, 40(%rsp)
.L441:
	movq	40(%rsp), %rax
	testb	$1, 74+_dl_x86_cpu_features(%rip)
	movq	%r13, 376+_dl_x86_cpu_features(%rip)
	movq	%r14, 400+_dl_x86_cpu_features(%rip)
	movq	%r10, 416+_dl_x86_cpu_features(%rip)
	movq	%rbx, 424+_dl_x86_cpu_features(%rip)
	movq	%rax, 344+_dl_x86_cpu_features(%rip)
	movq	48(%rsp), %rax
	movq	%rax, 352+_dl_x86_cpu_features(%rip)
	movq	56(%rsp), %rax
	movq	%rax, 360+_dl_x86_cpu_features(%rip)
	movq	64(%rsp), %rax
	movq	%rax, 368+_dl_x86_cpu_features(%rip)
	movq	72(%rsp), %rax
	movq	%rax, 384+_dl_x86_cpu_features(%rip)
	movq	80(%rsp), %rax
	movq	%rax, 392+_dl_x86_cpu_features(%rip)
	movq	88(%rsp), %rax
	movq	%rax, 408+_dl_x86_cpu_features(%rip)
	movq	184(%rsp), %rax
	movq	%rax, 96(%rsp)
	movl	276+_dl_x86_cpu_features(%rip), %eax
	je	.L449
	testb	$64, %ah
	jne	.L449
	movl	$8192, %ebx
	movl	$512, %r13d
.L450:
	leaq	192(%rsp), %rsi
	xorl	%edx, %edx
	movl	$24, %edi
	call	__tunable_get_val
	movq	192(%rsp), %r14
	leaq	200(%rsp), %rsi
	movl	$4, %edi
	testq	%r14, %r14
	cmove	%r15, %r14
	xorl	%edx, %edx
	call	__tunable_get_val
	movq	200(%rsp), %rax
	testq	%rax, %rax
	je	.L452
	movq	%rax, 184(%rsp)
.L452:
	leaq	208(%rsp), %rsi
	xorl	%edx, %edx
	movl	$13, %edi
	call	__tunable_get_val
	movq	208(%rsp), %rax
	testq	%rax, %rax
	jne	.L453
	imulq	$3, 96(%rsp), %rax
	movl	$4, %ecx
	cqto
	idivq	%rcx
.L453:
	leaq	216(%rsp), %rsi
	xorl	%edx, %edx
	movl	$8, %edi
	movq	%rax, 136(%rsp)
	call	__tunable_get_val
	movq	216(%rsp), %rdx
	movl	%r13d, %r8d
	leaq	224(%rsp), %rsi
	movl	$12, %edi
	movq	%r8, 128(%rsp)
	cmpq	%r8, %rdx
	cmovg	%edx, %ebx
	xorl	%edx, %edx
	orq	$-1, %r13
	call	__tunable_get_val
	leaq	232(%rsp), %rcx
	leaq	240(%rsp), %rdx
	leaq	248(%rsp), %rsi
	movl	$24, %edi
	movq	224(%rsp), %r15
	movq	%r13, 232(%rsp)
	movq	$0, 240(%rsp)
	movq	%r14, 248(%rsp)
	call	__tunable_set_val
	movq	184(%rsp), %rdx
	leaq	256(%rsp), %rcx
	leaq	272(%rsp), %rsi
	movl	$4, %edi
	movq	%r13, 256(%rsp)
	movq	$0, 264(%rsp)
	movq	%rdx, 272(%rsp)
	leaq	264(%rsp), %rdx
	call	__tunable_set_val
	movq	136(%rsp), %rax
	leaq	280(%rsp), %rcx
	leaq	288(%rsp), %rdx
	leaq	296(%rsp), %rsi
	movl	$13, %edi
	movq	%r13, 280(%rsp)
	movq	$0, 288(%rsp)
	movq	%rax, 296(%rsp)
	movq	%rax, 120(%rsp)
	call	__tunable_set_val
	movq	128(%rsp), %r8
	leaq	304(%rsp), %rcx
	leaq	312(%rsp), %rdx
	leaq	320(%rsp), %rsi
	movl	%ebx, %eax
	movl	$8, %edi
	movq	%rax, 320(%rsp)
	movq	%r13, 304(%rsp)
	movq	%r8, 312(%rsp)
	call	__tunable_set_val
	leaq	328(%rsp), %rcx
	leaq	336(%rsp), %rdx
	leaq	344(%rsp), %rsi
	movl	$12, %edi
	movq	%r13, 328(%rsp)
	movq	$1, 336(%rsp)
	movq	%r15, 344(%rsp)
	call	__tunable_set_val
	movq	184(%rsp), %rdx
	movq	120(%rsp), %rax
	leaq	176(%rsp), %rsi
	movl	$20, %edi
	movq	%r14, 304+_dl_x86_cpu_features(%rip)
	movq	%r15, 336+_dl_x86_cpu_features(%rip)
	movq	%rdx, 312+_dl_x86_cpu_features(%rip)
	leaq	_dl_tunable_set_hwcaps(%rip), %rdx
	movq	%rax, 320+_dl_x86_cpu_features(%rip)
	movl	%ebx, %eax
	movq	%rax, 328+_dl_x86_cpu_features(%rip)
	call	__tunable_get_val
	cmpl	$1, _dl_x86_cpu_features(%rip)
	movq	$2, _dl_hwcap(%rip)
	je	.L502
.L456:
	movq	304+_dl_x86_cpu_features(%rip), %rcx
	movl	$2, %esi
	movq	%rcx, %rax
	movq	%rcx, __x86_raw_data_cache_size(%rip)
	xorb	%cl, %cl
	cqto
	movq	%rcx, __x86_data_cache_size(%rip)
	idivq	%rsi
	movq	%rax, __x86_raw_data_cache_size_half(%rip)
	movq	%rcx, %rax
	movq	312+_dl_x86_cpu_features(%rip), %rcx
	cqto
	idivq	%rsi
	movq	%rcx, __x86_raw_shared_cache_size(%rip)
	movq	%rax, __x86_data_cache_size_half(%rip)
	movq	%rcx, %rax
	xorb	%cl, %cl
	cqto
	movq	%rcx, __x86_shared_cache_size(%rip)
	idivq	%rsi
	movq	%rax, __x86_raw_shared_cache_size_half(%rip)
	movq	%rcx, %rax
	cqto
	idivq	%rsi
	movq	%rax, __x86_shared_cache_size_half(%rip)
	movq	320+_dl_x86_cpu_features(%rip), %rax
	movq	%rax, __x86_shared_non_temporal_threshold(%rip)
	movq	328+_dl_x86_cpu_features(%rip), %rax
	movq	%rax, __x86_rep_movsb_threshold(%rip)
	movq	336+_dl_x86_cpu_features(%rip), %rax
	movq	%rax, __x86_rep_stosb_threshold(%rip)
	call	_dl_relocate_static_pie
	movq	__rela_iplt_start@GOTPCREL(%rip), %rbx
	jmp	.L461
.L463:
	cmpl	$37, 8(%rbx)
	movq	(%rbx), %r13
	jne	.L462
	call	*16(%rbx)
	addq	$24, %rbx
	movq	%rax, 0(%r13)
.L461:
	cmpq	__rela_iplt_end@GOTPCREL(%rip), %rbx
	jb	.L463
	call	__libc_setup_tls
	movq	_dl_random(%rip), %rax
	movq	(%rax), %rax
	xorb	%al, %al
#APP
# 219 "../csu/libc-start.c" 1
	movq %rax,%fs:40
# 0 "" 2
#NO_APP
	call	_dl_discover_osversion
	testl	%eax, %eax
	js	.L464
	movl	_dl_osversion(%rip), %edx
	testl	%edx, %edx
	jne	.L503
.L465:
	movl	%eax, _dl_osversion(%rip)
.L466:
	cmpl	$197119, %eax
	jle	.L504
	cmpq	$0, __pthread_initialize_minimal@GOTPCREL(%rip)
	je	.L471
	call	__pthread_initialize_minimal@PLT
.L471:
	movq	_dl_random(%rip), %rax
	movq	8(%rax), %rax
#APP
# 240 "../csu/libc-start.c" 1
	movq %rax,%fs:48
# 0 "" 2
#NO_APP
	testq	%r12, %r12
	je	.L470
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	__cxa_atexit
.L470:
	movl	$1, %edi
	call	__libc_early_init
	movq	__environ(%rip), %rdx
	movq	16(%rsp), %rsi
	movl	12(%rsp), %edi
	call	__libc_init_first
	cmpq	$0, 32(%rsp)
	je	.L476
	movq	32(%rsp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	__cxa_atexit
.L476:
	cmpl	$0, __libc_enable_secure(%rip)
	je	.L475
	call	__libc_check_standard_fds
.L475:
	testq	%rbp, %rbp
	je	.L478
	movq	__environ(%rip), %rdx
	movq	16(%rsp), %rsi
	movl	12(%rsp), %edi
	call	*%rbp
.L478:
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	_dl_debug_initialize
	leaq	352(%rsp), %rdi
	call	_setjmp
	testl	%eax, %eax
	jne	.L505
#APP
# 325 "../csu/libc-start.c" 1
	movq %fs:768,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 424(%rsp)
#APP
# 326 "../csu/libc-start.c" 1
	movq %fs:760,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 432(%rsp)
	leaq	352(%rsp), %rax
#APP
# 329 "../csu/libc-start.c" 1
	movq %rax,%fs:768
# 0 "" 2
#NO_APP
	movq	__environ(%rip), %rdx
	movq	16(%rsp), %rsi
	movl	12(%rsp), %edi
	movq	24(%rsp), %rax
	call	*%rax
.L482:
	movl	%eax, %edi
	call	exit
.L497:
	cmpw	$56, 54+__ehdr_start(%rip)
	jne	.L506
	leaq	__ehdr_start(%rip), %rax
	addq	32+__ehdr_start(%rip), %rax
	movq	%rax, _dl_phdr(%rip)
	movzwl	56+__ehdr_start(%rip), %eax
	movq	%rax, _dl_phnum(%rip)
	jmp	.L389
.L506:
	leaq	__PRETTY_FUNCTION__.11789(%rip), %rcx
	leaq	.LC4(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$187, %edx
	call	__assert_fail
.L391:
	cmpl	$1752462657, %ebx
	jne	.L418
	cmpl	$1145913699, %ecx
	jne	.L418
	cmpl	$1769238117, %edx
	je	.L419
.L420:
	cmpl	$1750278176, %ebx
	sete	%sil
	cmpl	$538995041, %ecx
	sete	%al
	testb	%al, %sil
	je	.L421
	cmpl	$1751608929, %edx
	jne	.L421
.L427:
	leaq	168(%rsp), %rcx
	leaq	164(%rsp), %rdx
	leaq	148(%rsp), %rsi
	leaq	144(%rsp), %rdi
	call	get_common_indices.constprop.2
	movl	$-2147483648, %esi
	movl	%esi, %eax
#APP
# 295 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483648, %eax
	movl	%eax, %esi
	jbe	.L429
	movl	$-2147483647, %eax
#APP
# 297 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483642, %esi
	movl	%eax, 84+_dl_x86_cpu_features(%rip)
	movl	%ebx, 88+_dl_x86_cpu_features(%rip)
	movl	%ecx, 92+_dl_x86_cpu_features(%rip)
	movl	%edx, 96+_dl_x86_cpu_features(%rip)
	jbe	.L429
	movl	$-2147483641, %eax
#APP
# 303 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483641, %esi
	movl	%eax, 148+_dl_x86_cpu_features(%rip)
	movl	%ebx, 152+_dl_x86_cpu_features(%rip)
	movl	%ecx, 156+_dl_x86_cpu_features(%rip)
	movl	%edx, 160+_dl_x86_cpu_features(%rip)
	je	.L429
	movl	$-2147483640, %eax
#APP
# 309 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 180+_dl_x86_cpu_features(%rip)
	movl	%ebx, 184+_dl_x86_cpu_features(%rip)
	movl	%ecx, 188+_dl_x86_cpu_features(%rip)
	movl	%edx, 192+_dl_x86_cpu_features(%rip)
.L429:
	call	update_usable.constprop.3
	movl	164(%rsp), %eax
	movl	144(%rsp), %edx
	addl	148(%rsp), %eax
	cmpl	$6, %edx
	movl	%eax, 148(%rsp)
	je	.L507
	cmpl	$7, %edx
	je	.L508
.L432:
	movl	$3, %eax
	jmp	.L417
.L505:
	call	__nptl_deallocate_tsd@PLT
	movq	__nptl_nthreads@GOTPCREL(%rip), %rax
#APP
# 357 "../csu/libc-start.c" 1
	lock;decl (%rax); sete %dl
# 0 "" 2
#NO_APP
	testb	%dl, %dl
	je	.L509
	xorl	%eax, %eax
	jmp	.L482
.L449:
	andl	$256, %eax
	cmpl	$1, %eax
	sbbl	%ebx, %ebx
	andl	$-2048, %ebx
	addl	$4096, %ebx
	cmpl	$1, %eax
	sbbl	%r13d, %r13d
	andl	$-128, %r13d
	addl	$256, %r13d
	jmp	.L450
.L509:
	movl	$60, %edx
	.p2align 4,,10
	.p2align 3
.L483:
	xorl	%edi, %edi
	movl	%edx, %eax
#APP
# 35 "../sysdeps/unix/sysv/linux/exit-thread.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L483
.L502:
	movl	72+_dl_x86_cpu_features(%rip), %eax
	testl	$268435456, %eax
	je	.L457
	testl	$134217728, %eax
	je	.L458
	testl	$67108864, %eax
	je	.L457
	leaq	.LC3(%rip), %rax
.L459:
	movq	%rax, _dl_platform(%rip)
	jmp	.L456
.L418:
	cmpl	$1869052232, %ebx
	jne	.L392
	cmpl	$1701734773, %ecx
	jne	.L392
	cmpl	$1852131182, %edx
	jne	.L421
.L419:
	leaq	152(%rsp), %rcx
	leaq	160(%rsp), %rdx
	leaq	148(%rsp), %rsi
	leaq	144(%rsp), %rdi
	call	get_common_indices.constprop.2
	movl	$-2147483648, %esi
	movl	%esi, %eax
#APP
# 295 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483648, %eax
	movl	%eax, %esi
	jbe	.L423
	movl	$-2147483647, %eax
#APP
# 297 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483642, %esi
	movl	%eax, 84+_dl_x86_cpu_features(%rip)
	movl	%ebx, 88+_dl_x86_cpu_features(%rip)
	movl	%ecx, 92+_dl_x86_cpu_features(%rip)
	movl	%edx, 96+_dl_x86_cpu_features(%rip)
	jbe	.L423
	movl	$-2147483641, %eax
#APP
# 303 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483641, %esi
	movl	%eax, 148+_dl_x86_cpu_features(%rip)
	movl	%ebx, 152+_dl_x86_cpu_features(%rip)
	movl	%ecx, 156+_dl_x86_cpu_features(%rip)
	movl	%edx, 160+_dl_x86_cpu_features(%rip)
	je	.L423
	movl	$-2147483640, %eax
#APP
# 309 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 180+_dl_x86_cpu_features(%rip)
	movl	%ebx, 184+_dl_x86_cpu_features(%rip)
	movl	%ecx, 188+_dl_x86_cpu_features(%rip)
	movl	%edx, 192+_dl_x86_cpu_features(%rip)
.L423:
	call	update_usable.constprop.3
	testb	$16, 47+_dl_x86_cpu_features(%rip)
	je	.L425
	movl	92+_dl_x86_cpu_features(%rip), %eax
	andl	$65536, %eax
	orl	%eax, 108+_dl_x86_cpu_features(%rip)
.L425:
	cmpl	$21, 144(%rsp)
	je	.L510
.L426:
	movl	$2, %eax
	jmp	.L417
.L458:
	movl	%eax, %edx
	andl	$1073872896, %edx
	cmpl	$1073872896, %edx
	jne	.L457
	testl	%eax, %eax
	jns	.L457
	movq	$6, _dl_hwcap(%rip)
.L457:
	testb	$32, %al
	je	.L456
	movl	44+_dl_x86_cpu_features(%rip), %edx
	testb	$16, %dh
	je	.L456
	andl	$264, %eax
	cmpl	$264, %eax
	jne	.L456
	testb	$32, 108+_dl_x86_cpu_features(%rip)
	je	.L456
	andl	$12582912, %edx
	cmpl	$12582912, %edx
	jne	.L456
	leaq	.LC2(%rip), %rax
	jmp	.L459
.L500:
	movl	$188, %edi
	orq	$-1, %rbx
	call	handle_zhaoxin
	movl	$191, %edi
	movq	%rax, %r15
	call	handle_zhaoxin
	movl	$194, %edi
	movq	%rax, %r13
	call	handle_zhaoxin
	movl	$185, %edi
	movq	%rax, %r14
	movq	%rax, 184(%rsp)
	call	handle_zhaoxin
	movl	$189, %edi
	movq	%rax, 40(%rsp)
	movq	%r15, 48(%rsp)
	call	handle_zhaoxin
	movl	$190, %edi
	movq	%rax, 56(%rsp)
	call	handle_zhaoxin
	movl	$192, %edi
	movq	%rax, 64(%rsp)
	call	handle_zhaoxin
	movl	$193, %edi
	movq	%rax, 72(%rsp)
	call	handle_zhaoxin
	movl	$195, %edi
	movq	%rax, 80(%rsp)
	call	handle_zhaoxin
	movl	$196, %edi
	movq	%rax, 88(%rsp)
	call	handle_zhaoxin
	leaq	172(%rsp), %rsi
	leaq	184(%rsp), %rdi
	movq	%r13, %rdx
	movq	%rax, 112(%rsp)
	call	get_common_cache_info
	movq	112(%rsp), %r10
	jmp	.L441
.L499:
	movl	$188, %edi
	call	handle_intel.constprop.5
	movl	$191, %edi
	movq	%rax, %r15
	call	handle_intel.constprop.5
	movl	$194, %edi
	movq	%rax, %r13
	call	handle_intel.constprop.5
	movl	$185, %edi
	movq	%rax, 184(%rsp)
	call	handle_intel.constprop.5
	movl	$189, %edi
	movq	%rax, 40(%rsp)
	movq	%r15, 48(%rsp)
	call	handle_intel.constprop.5
	movl	$190, %edi
	movq	%rax, 56(%rsp)
	call	handle_intel.constprop.5
	movl	$192, %edi
	movq	%rax, 64(%rsp)
	call	handle_intel.constprop.5
	movl	$193, %edi
	movq	%rax, 72(%rsp)
	call	handle_intel.constprop.5
	movl	$195, %edi
	movq	%rax, 80(%rsp)
	movq	184(%rsp), %r14
	call	handle_intel.constprop.5
	movl	$196, %edi
	movq	%rax, 88(%rsp)
	call	handle_intel.constprop.5
	movl	$197, %edi
	movq	%rax, 104(%rsp)
	call	handle_intel.constprop.5
	leaq	172(%rsp), %rsi
	leaq	184(%rsp), %rdi
	movq	%r13, %rdx
	movq	%rax, %rbx
	call	get_common_cache_info
	movq	104(%rsp), %r10
	jmp	.L441
.L462:
	leaq	.LC6(%rip), %rdi
	call	__libc_fatal
.L504:
	leaq	.LC7(%rip), %rdi
	call	__libc_fatal
.L503:
	cmpl	%eax, %edx
	jbe	.L466
	jmp	.L465
.L498:
	leaq	152(%rsp), %rcx
	leaq	156(%rsp), %rdx
	leaq	148(%rsp), %rsi
	leaq	144(%rsp), %rdi
	call	get_common_indices.constprop.2
	movl	$-2147483648, %esi
	movl	%esi, %eax
#APP
# 295 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483648, %eax
	movl	%eax, %esi
	jbe	.L394
	movl	$-2147483647, %eax
#APP
# 297 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483642, %esi
	movl	%eax, 84+_dl_x86_cpu_features(%rip)
	movl	%ebx, 88+_dl_x86_cpu_features(%rip)
	movl	%ecx, 92+_dl_x86_cpu_features(%rip)
	movl	%edx, 96+_dl_x86_cpu_features(%rip)
	jbe	.L394
	movl	$-2147483641, %eax
#APP
# 303 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	cmpl	$-2147483641, %esi
	movl	%eax, 148+_dl_x86_cpu_features(%rip)
	movl	%ebx, 152+_dl_x86_cpu_features(%rip)
	movl	%ecx, 156+_dl_x86_cpu_features(%rip)
	movl	%edx, 160+_dl_x86_cpu_features(%rip)
	je	.L394
	movl	$-2147483640, %eax
#APP
# 309 "../sysdeps/x86/cpu-features.c" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	%eax, 180+_dl_x86_cpu_features(%rip)
	movl	%ebx, 184+_dl_x86_cpu_features(%rip)
	movl	%ecx, 188+_dl_x86_cpu_features(%rip)
	movl	%edx, 192+_dl_x86_cpu_features(%rip)
.L394:
	call	update_usable.constprop.3
	cmpl	$6, 144(%rsp)
	je	.L511
.L397:
	movl	276+_dl_x86_cpu_features(%rip), %eax
	movl	%eax, %edx
	orb	$8, %ah
	orb	$64, %dh
	testb	$8, 59+_dl_x86_cpu_features(%rip)
	cmove	%edx, %eax
	movl	%eax, 276+_dl_x86_cpu_features(%rip)
	movl	$1, %eax
	jmp	.L417
.L508:
	cmpl	$27, %eax
	je	.L434
	cmpl	$59, %eax
	jne	.L432
	andl	$-268435457, 44+_dl_x86_cpu_features(%rip)
	andl	$-33, 72+_dl_x86_cpu_features(%rip)
	andl	$-257, 276+_dl_x86_cpu_features(%rip)
	jmp	.L432
.L507:
	cmpl	$15, %eax
	je	.L434
	cmpl	$25, %eax
	jne	.L432
.L434:
	movl	276+_dl_x86_cpu_features(%rip), %eax
	andl	$-268435457, 44+_dl_x86_cpu_features(%rip)
	andl	$-33, 72+_dl_x86_cpu_features(%rip)
	andb	$-2, %ah
	orb	$-128, %al
	movl	%eax, 276+_dl_x86_cpu_features(%rip)
	jmp	.L432
.L511:
	movl	156(%rsp), %eax
	addl	148(%rsp), %eax
	cmpl	$77, %eax
	movl	%eax, 148(%rsp)
	ja	.L399
	cmpl	$76, %eax
	jnb	.L400
	cmpl	$38, %eax
	je	.L401
	ja	.L402
	cmpl	$31, %eax
	ja	.L403
	cmpl	$30, %eax
	jnb	.L404
	cmpl	$26, %eax
	je	.L404
	cmpl	$28, %eax
	jne	.L398
.L401:
	orl	$64, 276+_dl_x86_cpu_features(%rip)
	jmp	.L397
.L405:
	cmpl	$55, %eax
	je	.L400
	cmpl	$74, %eax
	jne	.L398
.L400:
	orl	$1200, 276+_dl_x86_cpu_features(%rip)
.L410:
	cmpl	$63, %eax
	movl	152(%rsp), %edx
	je	.L411
	ja	.L412
	cmpl	$60, %eax
	jne	.L397
.L413:
	andl	$-2049, 72+_dl_x86_cpu_features(%rip)
	jmp	.L397
.L399:
	cmpl	$95, %eax
	je	.L400
	jbe	.L512
	cmpl	$134, %eax
	je	.L408
	jbe	.L513
	cmpl	$150, %eax
	je	.L408
	cmpl	$156, %eax
	jne	.L398
.L408:
	orl	$1204, 276+_dl_x86_cpu_features(%rip)
	jmp	.L410
.L512:
	cmpl	$90, %eax
	je	.L400
	ja	.L407
	cmpl	$87, %eax
	je	.L400
.L398:
	testb	$16, 31+_dl_x86_cpu_features(%rip)
	je	.L410
.L404:
	orl	$1076, 276+_dl_x86_cpu_features(%rip)
	jmp	.L410
.L407:
	leal	-92(%rax), %edx
	cmpl	$1, %edx
	ja	.L398
	jmp	.L400
.L412:
	subl	$69, %eax
	cmpl	$1, %eax
	ja	.L397
	jmp	.L413
.L411:
	cmpl	$3, %edx
	ja	.L397
	jmp	.L413
.L501:
	movl	$188, %edi
	call	handle_amd
	movl	$191, %edi
	movq	%rax, %r15
	call	handle_amd
	movl	$194, %edi
	movq	%rax, %r9
	call	handle_amd
	movl	$185, %edi
	movq	%rax, %r8
	movq	%rax, 184(%rsp)
	call	handle_amd
	movl	$189, %edi
	movq	%rax, 40(%rsp)
	movq	%r15, 48(%rsp)
	call	handle_amd
	movl	$190, %edi
	movq	%rax, 56(%rsp)
	call	handle_amd
	movl	$192, %edi
	movq	%r9, %r13
	movq	%rax, 64(%rsp)
	call	handle_amd
	movl	$193, %edi
	movq	%rax, 72(%rsp)
	call	handle_amd
	movl	$195, %edi
	movq	%r8, %r14
	movq	%rax, 80(%rsp)
	call	handle_amd
	movl	$196, %edi
	movq	%rax, 88(%rsp)
	call	handle_amd
	movq	%rax, %r10
	movl	$-2147483648, %eax
#APP
# 786 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	testq	%r8, %r8
	jle	.L514
	cmpl	$-2147483641, %eax
	jbe	.L444
	movl	$-2147483640, %eax
#APP
# 797 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	movl	$1, %eax
	shrl	$12, %ecx
	andl	$15, %ecx
	movl	%eax, %edx
	sall	%cl, %edx
	cmpl	$22, %r11d
	movl	%edx, 172(%rsp)
	jbe	.L446
#APP
# 805 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	andl	$268435456, %edx
	je	.L485
.L484:
	shrl	$16, %ebx
	movzbl	%bl, %eax
	movl	%eax, 172(%rsp)
.L485:
	movl	172(%rsp), %edx
	testl	%edx, %edx
	je	.L447
.L446:
	movq	%r8, %rax
	movl	%edx, %ecx
	cqto
	idivq	%rcx
	movq	%rax, 184(%rsp)
.L447:
	cmpl	$22, %r11d
	movq	184(%rsp), %rsi
	jbe	.L448
	movl	$-2147483619, %eax
	movl	$3, %ecx
#APP
# 822 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	shrl	$14, %eax
	orq	$-1, %rbx
	andl	$4095, %eax
	addq	$1, %rax
	imulq	%rax, %rsi
	movq	%rsi, 184(%rsp)
	jmp	.L441
.L513:
	cmpl	$117, %eax
	je	.L400
	cmpl	$122, %eax
	jne	.L398
	jmp	.L400
.L448:
	addq	%r9, 184(%rsp)
	orq	$-1, %rbx
	jmp	.L441
.L514:
	movq	%r9, 184(%rsp)
	orq	$-1, %rbx
	jmp	.L441
.L444:
	movl	$1, %eax
#APP
# 805 "../sysdeps/x86/dl-cacheinfo.h" 1
	cpuid
	
# 0 "" 2
#NO_APP
	andl	$268435456, %edx
	jne	.L484
	jmp	.L447
.L464:
	leaq	.LC8(%rip), %rdi
	call	__libc_fatal
.L510:
	movl	148(%rsp), %eax
	subl	$96, %eax
	cmpl	$31, %eax
	ja	.L426
	movl	276+_dl_x86_cpu_features(%rip), %eax
	andb	$-2, %ah
	orl	$24, %eax
	movl	%eax, 276+_dl_x86_cpu_features(%rip)
	jmp	.L426
.L403:
	cmpl	$37, %eax
	je	.L404
	jmp	.L398
.L402:
	cmpl	$47, %eax
	ja	.L405
	cmpl	$46, %eax
	jnb	.L404
	cmpl	$44, %eax
	je	.L404
	jmp	.L398
	.size	__libc_start_main, .-__libc_start_main
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10214, @object
	.size	__PRETTY_FUNCTION__.10214, 17
__PRETTY_FUNCTION__.10214:
	.string	"intel_check_word"
	.align 16
	.type	__PRETTY_FUNCTION__.11789, @object
	.size	__PRETTY_FUNCTION__.11789, 18
__PRETTY_FUNCTION__.11789:
	.string	"__libc_start_main"
	.section	.rodata
	.align 32
	.type	intel_02_known, @object
	.size	intel_02_known, 544
intel_02_known:
	.byte	6
	.byte	4
	.byte	32
	.byte	0
	.long	8192
	.byte	8
	.byte	4
	.byte	32
	.byte	0
	.long	16384
	.byte	9
	.byte	4
	.byte	32
	.byte	0
	.long	32768
	.byte	10
	.byte	2
	.byte	32
	.byte	3
	.long	8192
	.byte	12
	.byte	4
	.byte	32
	.byte	3
	.long	16384
	.byte	13
	.byte	4
	.byte	64
	.byte	3
	.long	16384
	.byte	14
	.byte	6
	.byte	64
	.byte	3
	.long	24576
	.byte	33
	.byte	8
	.byte	64
	.byte	6
	.long	262144
	.byte	34
	.byte	4
	.byte	64
	.byte	9
	.long	524288
	.byte	35
	.byte	8
	.byte	64
	.byte	9
	.long	1048576
	.byte	37
	.byte	8
	.byte	64
	.byte	9
	.long	2097152
	.byte	41
	.byte	8
	.byte	64
	.byte	9
	.long	4194304
	.byte	44
	.byte	8
	.byte	64
	.byte	3
	.long	32768
	.byte	48
	.byte	8
	.byte	64
	.byte	0
	.long	32768
	.byte	57
	.byte	4
	.byte	64
	.byte	6
	.long	131072
	.byte	58
	.byte	6
	.byte	64
	.byte	6
	.long	196608
	.byte	59
	.byte	2
	.byte	64
	.byte	6
	.long	131072
	.byte	60
	.byte	4
	.byte	64
	.byte	6
	.long	262144
	.byte	61
	.byte	6
	.byte	64
	.byte	6
	.long	393216
	.byte	62
	.byte	4
	.byte	64
	.byte	6
	.long	524288
	.byte	63
	.byte	2
	.byte	64
	.byte	6
	.long	262144
	.byte	65
	.byte	4
	.byte	32
	.byte	6
	.long	131072
	.byte	66
	.byte	4
	.byte	32
	.byte	6
	.long	262144
	.byte	67
	.byte	4
	.byte	32
	.byte	6
	.long	524288
	.byte	68
	.byte	4
	.byte	32
	.byte	6
	.long	1048576
	.byte	69
	.byte	4
	.byte	32
	.byte	6
	.long	2097152
	.byte	70
	.byte	4
	.byte	64
	.byte	9
	.long	4194304
	.byte	71
	.byte	8
	.byte	64
	.byte	9
	.long	8388608
	.byte	72
	.byte	12
	.byte	64
	.byte	6
	.long	3145728
	.byte	73
	.byte	16
	.byte	64
	.byte	6
	.long	4194304
	.byte	74
	.byte	12
	.byte	64
	.byte	9
	.long	6291456
	.byte	75
	.byte	16
	.byte	64
	.byte	9
	.long	8388608
	.byte	76
	.byte	12
	.byte	64
	.byte	9
	.long	12582912
	.byte	77
	.byte	16
	.byte	64
	.byte	9
	.long	16777216
	.byte	78
	.byte	24
	.byte	64
	.byte	6
	.long	6291456
	.byte	96
	.byte	8
	.byte	64
	.byte	3
	.long	16384
	.byte	102
	.byte	4
	.byte	64
	.byte	3
	.long	8192
	.byte	103
	.byte	4
	.byte	64
	.byte	3
	.long	16384
	.byte	104
	.byte	4
	.byte	64
	.byte	3
	.long	32768
	.byte	120
	.byte	8
	.byte	64
	.byte	6
	.long	1048576
	.byte	121
	.byte	8
	.byte	64
	.byte	6
	.long	131072
	.byte	122
	.byte	8
	.byte	64
	.byte	6
	.long	262144
	.byte	123
	.byte	8
	.byte	64
	.byte	6
	.long	524288
	.byte	124
	.byte	8
	.byte	64
	.byte	6
	.long	1048576
	.byte	125
	.byte	8
	.byte	64
	.byte	6
	.long	2097152
	.byte	127
	.byte	2
	.byte	64
	.byte	6
	.long	524288
	.byte	-128
	.byte	8
	.byte	64
	.byte	6
	.long	524288
	.byte	-126
	.byte	8
	.byte	32
	.byte	6
	.long	262144
	.byte	-125
	.byte	8
	.byte	32
	.byte	6
	.long	524288
	.byte	-124
	.byte	8
	.byte	32
	.byte	6
	.long	1048576
	.byte	-123
	.byte	8
	.byte	32
	.byte	6
	.long	2097152
	.byte	-122
	.byte	4
	.byte	64
	.byte	6
	.long	524288
	.byte	-121
	.byte	8
	.byte	64
	.byte	6
	.long	1048576
	.byte	-48
	.byte	4
	.byte	64
	.byte	9
	.long	524288
	.byte	-47
	.byte	4
	.byte	64
	.byte	9
	.long	1048576
	.byte	-46
	.byte	4
	.byte	64
	.byte	9
	.long	2097152
	.byte	-42
	.byte	8
	.byte	64
	.byte	9
	.long	1048576
	.byte	-41
	.byte	8
	.byte	64
	.byte	9
	.long	2097152
	.byte	-40
	.byte	8
	.byte	64
	.byte	9
	.long	4194304
	.byte	-36
	.byte	12
	.byte	64
	.byte	9
	.long	2097152
	.byte	-35
	.byte	12
	.byte	64
	.byte	9
	.long	4194304
	.byte	-34
	.byte	12
	.byte	64
	.byte	9
	.long	8388608
	.byte	-30
	.byte	16
	.byte	64
	.byte	9
	.long	2097152
	.byte	-29
	.byte	16
	.byte	64
	.byte	9
	.long	4194304
	.byte	-28
	.byte	16
	.byte	64
	.byte	9
	.long	8388608
	.byte	-22
	.byte	24
	.byte	64
	.byte	9
	.long	12582912
	.byte	-21
	.byte	24
	.byte	64
	.byte	9
	.long	18874368
	.byte	-20
	.byte	24
	.byte	64
	.byte	9
	.long	25165824
	.hidden	__x86_rep_stosb_threshold
	.globl	__x86_rep_stosb_threshold
	.data
	.align 8
	.type	__x86_rep_stosb_threshold, @object
	.size	__x86_rep_stosb_threshold, 8
__x86_rep_stosb_threshold:
	.quad	2048
	.hidden	__x86_rep_movsb_threshold
	.globl	__x86_rep_movsb_threshold
	.align 8
	.type	__x86_rep_movsb_threshold, @object
	.size	__x86_rep_movsb_threshold, 8
__x86_rep_movsb_threshold:
	.quad	2048
	.hidden	__x86_shared_non_temporal_threshold
	.comm	__x86_shared_non_temporal_threshold,8,8
	.hidden	__x86_raw_shared_cache_size
	.globl	__x86_raw_shared_cache_size
	.align 8
	.type	__x86_raw_shared_cache_size, @object
	.size	__x86_raw_shared_cache_size, 8
__x86_raw_shared_cache_size:
	.quad	1048576
	.hidden	__x86_raw_shared_cache_size_half
	.globl	__x86_raw_shared_cache_size_half
	.align 8
	.type	__x86_raw_shared_cache_size_half, @object
	.size	__x86_raw_shared_cache_size_half, 8
__x86_raw_shared_cache_size_half:
	.quad	524288
	.hidden	__x86_shared_cache_size
	.globl	__x86_shared_cache_size
	.align 8
	.type	__x86_shared_cache_size, @object
	.size	__x86_shared_cache_size, 8
__x86_shared_cache_size:
	.quad	1048576
	.hidden	__x86_shared_cache_size_half
	.globl	__x86_shared_cache_size_half
	.align 8
	.type	__x86_shared_cache_size_half, @object
	.size	__x86_shared_cache_size_half, 8
__x86_shared_cache_size_half:
	.quad	524288
	.hidden	__x86_raw_data_cache_size
	.globl	__x86_raw_data_cache_size
	.align 8
	.type	__x86_raw_data_cache_size, @object
	.size	__x86_raw_data_cache_size, 8
__x86_raw_data_cache_size:
	.quad	32768
	.hidden	__x86_raw_data_cache_size_half
	.globl	__x86_raw_data_cache_size_half
	.align 8
	.type	__x86_raw_data_cache_size_half, @object
	.size	__x86_raw_data_cache_size_half, 8
__x86_raw_data_cache_size_half:
	.quad	16384
	.hidden	__x86_data_cache_size
	.globl	__x86_data_cache_size
	.align 8
	.type	__x86_data_cache_size, @object
	.size	__x86_data_cache_size, 8
__x86_data_cache_size:
	.quad	32768
	.hidden	__x86_data_cache_size_half
	.globl	__x86_data_cache_size_half
	.align 8
	.type	__x86_data_cache_size_half, @object
	.size	__x86_data_cache_size_half, 8
__x86_data_cache_size_half:
	.quad	16384
	.weak	__nptl_nthreads
	.weak	__nptl_deallocate_tsd
	.weak	__pthread_initialize_minimal
	.weak	__rela_iplt_end
	.weak	__rela_iplt_start
	.hidden	__libc_fatal
	.hidden	_dl_platform
	.hidden	__nptl_nthreads
	.hidden	__nptl_deallocate_tsd
	.hidden	_dl_phnum
	.hidden	__ehdr_start
	.hidden	exit
	.hidden	_setjmp
	.hidden	_dl_debug_initialize
	.hidden	__libc_check_standard_fds
	.hidden	__libc_enable_secure
	.hidden	__libc_init_first
	.hidden	__libc_early_init
	.hidden	__cxa_atexit
	.hidden	__pthread_initialize_minimal
	.hidden	_dl_osversion
	.hidden	_dl_discover_osversion
	.hidden	_dl_random
	.hidden	__libc_setup_tls
	.hidden	__rela_iplt_end
	.hidden	__rela_iplt_start
	.hidden	_dl_relocate_static_pie
	.hidden	_dl_hwcap
	.hidden	_dl_tunable_set_hwcaps
	.hidden	__tunable_set_val
	.hidden	__tunable_get_val
	.hidden	__tunables_init
	.hidden	__libc_init_secure
	.hidden	_dl_phdr
	.hidden	_dl_aux_init
	.hidden	__libc_stack_end
	.hidden	__environ
	.hidden	__assert_fail
	.hidden	_dl_x86_cpu_features
