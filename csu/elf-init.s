	.text
	.p2align 4,,15
	.globl	__libc_csu_init
	.type	__libc_csu_init, @function
__libc_csu_init:
	pushq	%r15
	leaq	__preinit_array_start(%rip), %r15
	pushq	%r14
	leaq	__preinit_array_end(%rip), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	%r15, %r14
	movq	%rsi, %r12
	movq	%rdx, %r13
	sarq	$3, %r14
	subq	$8, %rsp
	testq	%r14, %r14
	je	.L2
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	*(%r15,%rbx,8)
	addq	$1, %rbx
	cmpq	%rbx, %r14
	jne	.L3
.L2:
	leaq	__init_array_start(%rip), %r15
	leaq	__init_array_end(%rip), %r14
	call	_init@PLT
	subq	%r15, %r14
	sarq	$3, %r14
	testq	%r14, %r14
	je	.L1
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	*(%r15,%rbx,8)
	addq	$1, %rbx
	cmpq	%rbx, %r14
	jne	.L5
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__libc_csu_init, .-__libc_csu_init
	.p2align 4,,15
	.globl	__libc_csu_fini
	.type	__libc_csu_fini, @function
__libc_csu_fini:
	pushq	%rbp
	leaq	__fini_array_end(%rip), %rax
	leaq	__fini_array_start(%rip), %rbp
	pushq	%rbx
	subq	%rbp, %rax
	sarq	$3, %rax
	subq	$8, %rsp
	testq	%rax, %rax
	je	.L17
	leaq	-1(%rax), %rbx
	.p2align 4,,10
	.p2align 3
.L18:
	call	*0(%rbp,%rbx,8)
	subq	$1, %rbx
	cmpq	$-1, %rbx
	jne	.L18
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_fini@PLT
	.size	__libc_csu_fini, .-__libc_csu_fini
	.hidden	__fini_array_start
	.hidden	__fini_array_end
	.hidden	__init_array_end
	.hidden	__init_array_start
	.hidden	__preinit_array_end
	.hidden	__preinit_array_start
