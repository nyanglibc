	.text
	.p2align 4,,15
	.globl	__errno_location
	.hidden	__errno_location
	.type	__errno_location, @function
__errno_location:
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__errno_location, .-__errno_location
