#!/bin/sh
set -e
################################################################################
#### CONFIGURATION
destdir=
prefix=/nyan/glibc/nyan/1
user_id=0
group_id=0
################################################################################
build_dir=$(readlink -f .)
printf "BUILD_DIR=$build_dir\n"

mkdir -p $destdir$prefix

#ELF loader, isolate it or linking conflict may happen.
mkdir -p $destdir$prefix/lib64
cp -f $build_dir/elf/ld-linux-x86-64.so.2 $destdir$prefix/lib64
chmod 755 $destdir$prefix/lib64/ld-linux-x86-64.so.2

#libc
mkdir -p $destdir$prefix/lib
cp -f $build_dir/libc.so $destdir$prefix/lib/libc-2.33.so
chmod 755 $destdir$prefix/lib/libc-2.33.so
ln -sTf libc-2.33.so $destdir$prefix/lib/libc.so.6

#libdl
mkdir -p $destdir$prefix/lib
cp -f $build_dir/dlfcn/libdl/libdl.so $destdir$prefix/lib/libdl-2.33.so
chmod 755 $destdir$prefix/lib/libdl-2.33.so
ln -sTf libdl-2.33.so $destdir$prefix/lib/libdl.so.2

#libpthread
mkdir -p $destdir$prefix/lib
cp -f $build_dir/nptl/libpthread.so $destdir$prefix/lib/libpthread-2.33.so
chmod 755 $destdir$prefix/lib/libpthread-2.33.so
ln -sTf libpthread-2.33.so $destdir$prefix/lib/libpthread.so.0

#librt
mkdir -p $destdir$prefix/lib
cp -f $build_dir/rt/librt.so $destdir$prefix/lib/librt-2.33.so
chmod 755 $destdir$prefix/lib/librt-2.33.so
ln -sTf librt-2.33.so $destdir$prefix/lib/librt.so.1

#libcrypt
mkdir -p $destdir$prefix/lib
cp -f $build_dir/crypt/libcrypt.so $destdir$prefix/lib/libcrypt-2.33.so
chmod 755 $destdir$prefix/lib/libcrypt-2.33.so
ln -sTf libcrypt-2.33.so $destdir$prefix/lib/libcrypt.so.1

#libm
mkdir -p $destdir$prefix/lib
cp -f $build_dir/math/libm/libm.so $destdir$prefix/lib/libm-2.33.so
chmod 755 $destdir$prefix/lib/libm-2.33.so
ln -sTf libm-2.33.so $destdir$prefix/lib/libm.so.6

#libmvec
mkdir -p $destdir$prefix/lib
cp -f $build_dir/mathvec/libmvec.so $destdir$prefix/lib/libmvec-2.33.so
chmod 755 $destdir$prefix/lib/libmvec-2.33.so
ln -sTf libmvec-2.33.so $destdir$prefix/lib/libmvec.so.1

#libresolv
mkdir -p $destdir$prefix/lib
cp -f $build_dir/resolv/libresolv/libresolv.so $destdir$prefix/lib/libresolv-2.33.so
chmod 755 $destdir$prefix/lib/libresolv-2.33.so
ln -sTf libresolv-2.33.so $destdir$prefix/lib/libresolv.so.2

#libanl
mkdir -p $destdir$prefix/lib
cp -f $build_dir/resolv/libanl/libanl.so $destdir$prefix/lib/libanl-2.33.so
chmod 755 $destdir$prefix/lib/libanl-2.33.so
ln -sTf libanl-2.33.so $destdir$prefix/lib/libanl.so.1
#===============================================================================
# nss
mkdir -p $destdir$prefix/lib
cp -f $build_dir/resolv/libnss_dns/libnss_dns.so $destdir$prefix/lib/libnss_dns-2.33.so
chmod 755 $destdir$prefix/lib/libnss_dns-2.33.so
ln -sTf libnss_dns-2.33.so $destdir$prefix/lib/libnss_dns.so.2

cp -f $build_dir/nss/libnss_compat/libnss_compat.so $destdir$prefix/lib/libnss_compat-2.33.so
chmod 755 $destdir$prefix/lib/libnss_compat-2.33.so
ln -sTf libnss_compat-2.33.so $destdir$prefix/lib/libnss_compat.so.2

cp -f $build_dir/nss/libnss_db/libnss_db.so $destdir$prefix/lib/libnss_db-2.33.so
chmod 755 $destdir$prefix/lib/libnss_db-2.33.so
ln -sTf libnss_db-2.33.so $destdir$prefix/lib/libnss_db.so.2

cp -f $build_dir/nss/libnss_files/libnss_files.so $destdir$prefix/lib/libnss_files-2.33.so
chmod 755 $destdir$prefix/lib/libnss_files-2.33.so
ln -sTf libnss_files-2.33.so $destdir$prefix/lib/libnss_files.so.2

cp -f $build_dir/hesiod/libnss_hesiod.so $destdir$prefix/lib/libnss_hesiod-2.33.so
chmod 755 $destdir$prefix/lib/libnss_hesiod-2.33.so
ln -sTf libnss_hesiod-2.33.so $destdir$prefix/lib/libnss_hesiod.so.2
#===============================================================================
#libutil
mkdir -p $destdir$prefix/lib
cp -f $build_dir/login/libutil/libutil.so $destdir$prefix/lib/libutil-2.33.so
chmod 755 $destdir$prefix/lib/libutil-2.33.so
ln -sTf libutil-2.33.so $destdir$prefix/lib/libutil.so.1

#gconv modules: for the web and email until utf-8 is really there
mkdir -p $destdir$prefix/lib/gconv
cp -f $build_dir/gconv-modules $destdir$prefix/lib/gconv/gconv-modules
chmod 644 $destdir$prefix/lib/gconv/gconv-modules
gconv_modules="\
ANSI_X3.110.so \
ARMSCII-8.so \
ASMO_449.so \
BIG5.so \
BIG5HKSCS.so \
BRF.so \
CP10007.so \
CP1125.so \
CP1250.so \
CP1251.so \
CP1252.so \
CP1253.so \
CP1254.so \
CP1255.so \
CP1256.so \
CP1257.so \
CP1258.so \
CP737.so \
CP770.so \
CP771.so \
CP772.so \
CP773.so \
CP774.so \
CP775.so \
CP932.so \
CSN_369103.so \
CWI.so \
DEC-MCS.so \
EBCDIC-AT-DE-A.so \
EBCDIC-AT-DE.so \
EBCDIC-CA-FR.so \
EBCDIC-DK-NO-A.so \
EBCDIC-DK-NO.so \
EBCDIC-ES-A.so \
EBCDIC-ES-S.so \
EBCDIC-ES.so \
EBCDIC-FI-SE-A.so \
EBCDIC-FI-SE.so \
EBCDIC-FR.so \
EBCDIC-IS-FRISS.so \
EBCDIC-IT.so \
EBCDIC-PT.so \
EBCDIC-UK.so \
EBCDIC-US.so \
ECMA-CYRILLIC.so \
EUC-CN.so \
EUC-JISX0213.so \
EUC-JP-MS.so \
EUC-JP.so \
EUC-KR.so \
EUC-TW.so \
GB18030.so \
GBBIG5.so \
GBGBK.so \
GBK.so \
GEORGIAN-ACADEMY.so \
GEORGIAN-PS.so \
GOST_19768-74.so \
GREEK-CCITT.so \
GREEK7-OLD.so \
GREEK7.so \
HP-GREEK8.so \
HP-ROMAN8.so \
HP-ROMAN9.so \
HP-THAI8.so \
HP-TURKISH8.so \
IBM037.so \
IBM038.so \
IBM1004.so \
IBM1008.so \
IBM1008_420.so \
IBM1025.so \
IBM1026.so \
IBM1046.so \
IBM1047.so \
IBM1097.so \
IBM1112.so \
IBM1122.so \
IBM1123.so \
IBM1124.so \
IBM1129.so \
IBM1130.so \
IBM1132.so \
IBM1133.so \
IBM1137.so \
IBM1140.so \
IBM1141.so \
IBM1142.so \
IBM1143.so \
IBM1144.so \
IBM1145.so \
IBM1146.so \
IBM1147.so \
IBM1148.so \
IBM1149.so \
IBM1153.so \
IBM1154.so \
IBM1155.so \
IBM1156.so \
IBM1157.so \
IBM1158.so \
IBM1160.so \
IBM1161.so \
IBM1162.so \
IBM1163.so \
IBM1164.so \
IBM1166.so \
IBM1167.so \
IBM12712.so \
IBM1364.so \
IBM1371.so \
IBM1388.so \
IBM1390.so \
IBM1399.so \
IBM16804.so \
IBM256.so \
IBM273.so \
IBM274.so \
IBM275.so \
IBM277.so \
IBM278.so \
IBM280.so \
IBM281.so \
IBM284.so \
IBM285.so \
IBM290.so \
IBM297.so \
IBM420.so \
IBM423.so \
IBM424.so \
IBM437.so \
IBM4517.so \
IBM4899.so \
IBM4909.so \
IBM4971.so \
IBM500.so \
IBM5347.so \
IBM803.so \
IBM850.so \
IBM851.so \
IBM852.so \
IBM855.so \
IBM856.so \
IBM857.so \
IBM858.so \
IBM860.so \
IBM861.so \
IBM862.so \
IBM863.so \
IBM864.so \
IBM865.so \
IBM866.so \
IBM866NAV.so \
IBM868.so \
IBM869.so \
IBM870.so \
IBM871.so \
IBM874.so \
IBM875.so \
IBM880.so \
IBM891.so \
IBM901.so \
IBM902.so \
IBM903.so \
IBM9030.so \
IBM904.so \
IBM905.so \
IBM9066.so \
IBM918.so \
IBM921.so \
IBM922.so \
IBM930.so \
IBM932.so \
IBM933.so \
IBM935.so \
IBM937.so \
IBM939.so \
IBM943.so \
IBM9448.so \
IEC_P27-1.so \
INIS-8.so \
INIS-CYRILLIC.so \
INIS.so \
ISIRI-3342.so \
ISO-2022-CN-EXT.so \
ISO-2022-CN.so \
ISO-2022-JP-3.so \
ISO-2022-JP.so \
ISO-2022-KR.so \
ISO-IR-197.so \
ISO-IR-209.so \
ISO646.so \
ISO8859-1.so \
ISO8859-10.so \
ISO8859-11.so \
ISO8859-13.so \
ISO8859-14.so \
ISO8859-15.so \
ISO8859-16.so \
ISO8859-2.so \
ISO8859-3.so \
ISO8859-4.so \
ISO8859-5.so \
ISO8859-6.so \
ISO8859-7.so \
ISO8859-8.so \
ISO8859-9.so \
ISO8859-9E.so \
ISO_10367-BOX.so \
ISO_11548-1.so \
ISO_2033.so \
ISO_5427-EXT.so \
ISO_5427.so \
ISO_5428.so \
ISO_6937-2.so \
ISO_6937.so \
JOHAB.so \
KOI-8.so \
KOI8-R.so \
KOI8-RU.so \
KOI8-T.so \
KOI8-U.so \
LATIN-GREEK-1.so \
LATIN-GREEK.so \
MAC-CENTRALEUROPE.so \
MAC-IS.so \
MAC-SAMI.so \
MAC-UK.so \
MACINTOSH.so \
MIK.so \
NATS-DANO.so \
NATS-SEFI.so \
PT154.so \
RK1048.so \
SAMI-WS2.so \
SHIFT_JISX0213.so \
SJIS.so \
T.61.so \
TCVN5712-1.so \
TIS-620.so \
TSCII.so \
UHC.so \
UNICODE.so \
UTF-16.so \
UTF-32.so \
UTF-7.so \
VISCII.so \
libCNS.so \
libGB.so \
libISOIR165.so \
libJIS.so \
libJISX0213.so \
libKSC.so \
"
for m in $gconv_modules
do
	cp -f $build_dir/iconvdata/$m $destdir$prefix/lib/gconv/$m
	chmod 755 $destdir$prefix/lib/gconv/$m
done

# TODO: move this in install_troubleshooting
# troubleshooting (not strictly runtime)
mkdir -p $destdir$prefix/lib
cp -f $build_dir/debug/libSegFault/libSegFault.so $destdir$prefix/lib/libSegFault.so
chmod 755 $destdir$prefix/lib/libSegFault.so
cp -f $build_dir/debug/libpcprofile/libpcprofile.so $destdir$prefix/lib/libpcprofile.so
chmod 755 $destdir$prefix/lib/libpcprofile.so
cp -f $build_dir/malloc/libmemusage/libmemusage.so $destdir$prefix/lib/libmemusage.so
chmod 755 $destdir$prefix/lib/libmemusage.so
cp -f $build_dir/nptl_db/libthread_db.so $destdir$prefix/lib/libthread_db-1.0.so
chmod 755 $destdir$prefix/lib/libthread_db-1.0.so
ln -sTf libthread_db-1.0.so $destdir$prefix/lib/libthread_db.so.1
mkdir -p $destdir$prefix/lib/audit
cp -f $build_dir/elf/sotruss-lib.so $destdir$prefix/lib/audit/sotruss-lib.so
chmod 755 $destdir$prefix/lib/audit/sotruss-lib.so

#C.utf8 locale
mkdir -p $destdir$prefix/lib/locale
cp -f $build_dir/locale/locale-archive $destdir$prefix/lib/locale/locale-archive
chmod 644 $destdir$prefix/lib/locale/locale-archive
mkdir -p $destdir$prefix/share/locale
printf "en_US.UTF-8	 C.UTF-8\n" >$destdir$prefix/share/locale/locale.alias
chmod 644 $destdir$prefix/share/locale/locale.alias

chown $user_id:$group_id -R $destdir$prefix || true
