	.text
	.p2align 4,,15
	.globl	atof
	.type	atof, @function
atof:
	xorl	%esi, %esi
	jmp	strtod
	.size	atof, .-atof
	.hidden	strtod
