	.text
	.p2align 4,,15
	.type	round_away, @function
round_away:
	cmpl	$1024, %r8d
	je	.L3
	jle	.L18
	cmpl	$2048, %r8d
	je	.L6
	cmpl	$3072, %r8d
	jne	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r8d, %r8d
	jne	.L2
	orl	%esi, %ecx
	movl	$0, %eax
	testb	%dl, %dl
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmovne	%ecx, %eax
	ret
.L2:
	subq	$8, %rsp
	call	abort
	.size	round_away, .-round_away
	.globl	__multf3
	.globl	__addtf3
	.p2align 4,,15
	.type	round_and_return, @function
round_and_return:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 46(%rsp)
# 0 "" 2
#NO_APP
	movzwl	46(%rsp), %eax
	andw	$3072, %ax
	cmpw	$1024, %ax
	je	.L21
	jbe	.L70
	cmpw	$2048, %ax
	je	.L24
	cmpw	$3072, %ax
	jne	.L20
	movl	$3072, %r11d
.L23:
	cmpq	$-16382, %rbx
	jge	.L26
.L71:
	cmpq	$-16495, %rbx
	jge	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	je	.L28
	movdqa	.LC0(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$-16382, %rbx
	movl	$2048, %r11d
	jl	.L71
.L26:
	cmpq	$16383, %rbx
	jle	.L72
.L43:
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	je	.L52
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	testw	%ax, %ax
	jne	.L20
	xorl	%r11d, %r11d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$-1, %rdx
	movl	%r8d, %ecx
	movq	$-16382, %rax
	salq	%cl, %rdx
	subq	%rbx, %rax
	notq	%rdx
	testq	%r15, %rdx
	setne	%dl
	movzbl	%dl, %edx
	orl	%edx, %r9d
	cmpq	$113, %rax
	je	.L73
	movq	$-16383, %r14
	movq	%r14, %r13
	subq	%rbx, %r13
	cmpq	$63, %rax
	jle	.L32
	movq	%r13, %rdx
	movq	%r13, %rcx
	sarq	$6, %rdx
	andl	$63, %ecx
	cmpq	$1, %rdx
	movq	0(%rbp,%rdx,8), %r15
	jne	.L33
	xorl	%edx, %edx
	cmpq	$0, 0(%rbp)
	setne	%dl
	orl	%edx, %r9d
.L33:
	movq	$-1, %rdx
	movl	%ecx, (%rsp)
	salq	%cl, %rdx
	notq	%rdx
	testq	%r15, %rdx
	setne	%dl
	movzbl	%dl, %edx
	orl	%edx, %r9d
	andl	$63, %eax
	jne	.L34
	movq	8(%rbp), %r10
	movq	%r10, 0(%rbp)
.L35:
	movl	%r9d, %ebx
	movl	%r10d, %esi
	movq	$0, 8(%rbp)
	andl	$1, %ebx
	andl	$1, %esi
.L31:
	movl	(%rsp), %edi
	movq	%r15, %r13
	movl	%edi, %ecx
	shrq	%cl, %r13
	andl	$1, %r13d
	jne	.L41
	testb	%bl, %bl
	je	.L74
.L41:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r9d, 28(%rsp)
	movdqa	.LC1(%rip), %xmm1
	movq	%r10, 16(%rsp)
	movl	%r11d, 24(%rsp)
	movdqa	%xmm1, %xmm0
	movl	$34, %fs:(%rax)
	movl	%esi, 8(%rsp)
	call	__multf3@PLT
	movl	8(%rsp), %esi
	movl	24(%rsp), %r11d
	movq	$-16383, %rbx
	movq	16(%rsp), %r10
	movl	28(%rsp), %r9d
.L40:
	testl	%r9d, %r9d
	movl	$1, %r14d
	jne	.L44
	movzbl	(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r15, %rax
.L42:
	testq	%rax, %rax
	setne	%r14b
	movzbl	%r14b, %r9d
.L44:
	movzbl	%r13b, %edx
	movl	%r11d, %r8d
	movl	%r9d, %ecx
	movl	%r12d, %edi
	movq	%r10, (%rsp)
	call	round_away
	testb	%al, %al
	je	.L50
	movq	(%rsp), %r10
	movq	8(%rbp), %rax
	addq	$1, %r10
	movq	%r10, 0(%rbp)
	jnc	.L48
	addq	$1, %rax
	movq	%rax, 8(%rbp)
.L48:
	btq	$49, %rax
	jc	.L75
	cmpq	$-16383, %rbx
	jne	.L50
	movabsq	$281474976710656, %rdx
	xorl	%ebx, %ebx
	testq	%rdx, %rax
	setne	%bl
	subl	$16383, %ebx
	.p2align 4,,10
	.p2align 3
.L50:
	testb	%r14b, %r14b
	jne	.L59
	testb	%r13b, %r13b
	jne	.L59
.L53:
	movl	%r12d, %edx
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	__mpn_construct_float128
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %r11d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	0(%rbp), %rdi
	movl	%r9d, %ebx
	movl	%r13d, (%rsp)
	andl	$1, %ebx
	cmpq	$1, %rax
	movq	%rdi, 8(%rsp)
	je	.L36
	movq	%rbp, %rsi
	movl	%eax, %ecx
	movl	$2, %edx
	movq	%rbp, %rdi
	movl	%r9d, 16(%rsp)
	movl	%r11d, 24(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r10
	movq	8(%rsp), %r15
	movl	24(%rsp), %r11d
	movl	16(%rsp), %r9d
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L59:
	movdqa	.LC1(%rip), %xmm1
	movdqa	.LC4(%rip), %xmm0
	call	__addtf3@PLT
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L72:
	movq	0(%rbp), %r10
	movq	%r15, %r13
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movl	%r8d, (%rsp)
	andl	$1, %r13d
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L75:
	addq	$1, %rbx
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mpn_rshift
	movabsq	$281474976710656, %rax
	orq	%rax, 8(%rbp)
	cmpq	$16384, %rbx
	je	.L43
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L74:
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r15, %rax
	jne	.L41
	movq	$-16383, %rbx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	cmpq	$0, 0(%rbp)
	movq	8(%rbp), %r15
	movq	$0, 0(%rbp)
	movq	$0, 8(%rbp)
	movl	$48, (%rsp)
	setne	%al
	xorl	%esi, %esi
	xorl	%r10d, %r10d
	orl	%eax, %r9d
	movl	%r9d, %ebx
	andl	$1, %ebx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r15, %rdx
	movq	8(%rsp), %r15
	movl	%r11d, %r8d
	shrq	%cl, %rdx
	movl	%r12d, %edi
	movl	%r9d, %ecx
	andl	$1, %edx
	movl	%r11d, 16(%rsp)
	movl	%r9d, 24(%rsp)
	movq	%r15, %rsi
	andl	$1, %esi
	call	round_away
	testb	%al, %al
	movl	24(%rsp), %r9d
	movl	16(%rsp), %r11d
	jne	.L37
	movq	%rbp, %rsi
	movl	$1, %ecx
	movl	$2, %edx
	movq	%rbp, %rdi
	movl	%r11d, 8(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r10
	movl	8(%rsp), %r11d
	movl	24(%rsp), %r9d
	movl	%r10d, %esi
	andl	$1, %esi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	8(%rbp), %rsi
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%rbp, %rdi
	movl	%r9d, 24(%rsp)
	movl	%r11d, 8(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r10
	movl	8(%rsp), %r11d
	movl	24(%rsp), %r9d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%r15d, %r15d
	cmpq	$-1, 8(%rsp)
	movl	$1, %ecx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	movl	$2, %edx
	movl	%r9d, 16(%rsp)
	movl	%r11d, 24(%rsp)
	sete	%r15b
	addq	8(%rbp), %r15
	call	__mpn_rshift
	movq	8(%rsp), %rax
	movq	0(%rbp), %r10
	movl	%r13d, %ecx
	movl	24(%rsp), %r11d
	movl	16(%rsp), %r9d
	movq	%rax, %rdi
	movl	%r10d, %esi
	shrq	%cl, %rdi
	andl	$1, %esi
	movq	%rdi, %r13
	andl	$1, %r13d
	btq	$49, %r15
	jnc	.L39
	movq	%rax, %r15
	movq	%r14, %rbx
	jmp	.L40
.L20:
	call	abort
.L39:
	movq	8(%rsp), %r15
	jmp	.L31
	.size	round_and_return, .-round_and_return
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"../stdlib/strtod_l.c"
.LC6:
	.string	"digcnt > 0"
.LC7:
	.string	"*nsize < MPNSIZE"
	.text
	.p2align 4,,15
	.type	str_to_mpn.isra.0, @function
str_to_mpn.isra.0:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testl	%esi, %esi
	movq	$0, (%rcx)
	movq	80(%rsp), %r14
	jle	.L120
	movq	%rdi, %rbx
	movl	%esi, %r15d
	movq	%rdx, %r12
	movq	%rcx, %r13
.L119:
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L77:
	movsbq	(%rbx), %rax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L86
	testq	%r14, %r14
	je	.L87
	cmpb	(%r14), %al
	je	.L121
.L87:
	addq	%r9, %rbx
	movsbq	(%rbx), %rax
.L86:
	leaq	0(%rbp,%rbp,4), %rcx
	addq	$1, %rbx
	addl	$1, %edx
	subl	$1, %r15d
	leaq	-48(%rax,%rcx,2), %rbp
	je	.L122
	cmpl	$19, %edx
	jne	.L77
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L79
	movq	%rbp, (%r12)
	movq	$1, 0(%r13)
	xorl	%ebp, %ebp
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L79:
	movabsq	$-8446744073709551616, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r9, 8(%rsp)
	movq	%r8, (%rsp)
	call	__mpn_mul_1
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	addq	(%r12), %rdx
	movq	0(%r13), %rsi
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	setc	%cl
	movq	%rdx, (%r12)
	testq	%rcx, %rcx
	jne	.L123
.L82:
	testq	%rax, %rax
	je	.L119
	movq	0(%r13), %rdx
	cmpq	$861, %rdx
	jg	.L124
	movq	%rax, (%r12,%rdx,8)
	xorl	%ebp, %ebp
	addq	$1, 0(%r13)
	xorl	%edx, %edx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L122:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jle	.L93
	movl	$19, %eax
	subl	%edx, %eax
	cltq
	cmpq	%rax, %rcx
	jle	.L125
.L93:
	movslq	%edx, %rax
	leaq	_tens_in_limb(%rip), %rdx
	movq	(%rdx,%rax,8), %rcx
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L95
.L126:
	movq	%rbp, (%r12)
	movq	$1, 0(%r13)
.L109:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movzbl	1(%r14), %eax
	testb	%al, %al
	je	.L88
	cmpb	1(%rbx), %al
	jne	.L87
	leaq	2(%rbx), %rcx
	leaq	2(%r14), %rsi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1, %rcx
	addq	$1, %rsi
	cmpb	%al, %dil
	jne	.L87
.L89:
	movzbl	(%rsi), %edi
	movq	%rcx, %r10
	movsbq	(%rcx), %rax
	testb	%dil, %dil
	jne	.L90
.L91:
	movq	%r10, %rbx
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L123:
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	movq	8(%r12,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L82
.L83:
	cmpq	%rdx, %rsi
	jne	.L84
	addq	$1, %rax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L125:
	movslq	%edx, %rax
	movq	$0, (%r8)
	movq	0(%r13), %rdx
	leaq	_tens_in_limb(%rip), %rsi
	imulq	(%rsi,%rcx,8), %rbp
	addq	%rax, %rcx
	testq	%rdx, %rdx
	movq	(%rsi,%rcx,8), %rcx
	je	.L126
.L95:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	addq	(%r12), %rdx
	movq	0(%r13), %rsi
	setc	%cl
	movq	%rdx, (%r12)
	testq	%rcx, %rcx
	je	.L99
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L101:
	movq	8(%r12,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L99
.L100:
	cmpq	%rsi, %rdx
	jne	.L101
	addq	$1, %rax
.L99:
	testq	%rax, %rax
	je	.L109
	movq	0(%r13), %rdx
	cmpq	$861, %rdx
	jg	.L127
	leaq	1(%rdx), %rcx
	movq	%rcx, 0(%r13)
	movq	%rax, (%r12,%rdx,8)
	jmp	.L109
.L88:
	leaq	1(%rbx), %r10
	movsbq	1(%rbx), %rax
	jmp	.L91
.L127:
	leaq	__PRETTY_FUNCTION__.12597(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$453, %edx
	call	__assert_fail
.L124:
	leaq	__PRETTY_FUNCTION__.12597(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$397, %edx
	call	__assert_fail
.L120:
	leaq	__PRETTY_FUNCTION__.12597(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$380, %edx
	call	__assert_fail
	.size	str_to_mpn.isra.0, .-str_to_mpn.isra.0
	.section	.rodata.str1.1
.LC13:
	.string	"decimal_len > 0"
.LC14:
	.string	"inf"
.LC15:
	.string	"inity"
.LC16:
	.string	"nan"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"dig_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC18:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_EXP - MANT_DIG) / 4"
	.align 8
.LC19:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX / 4"
	.align 8
.LC20:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_EXP - 3) / 4"
	.align 8
.LC21:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_10_EXP - MANT_DIG)"
	.align 8
.LC22:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC23:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_10_EXP - 1)"
	.section	.rodata.str1.1
.LC24:
	.string	"dig_no >= int_no"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"lead_zero <= (base == 16 ? (uintmax_t) INTMAX_MAX / 4 : (uintmax_t) INTMAX_MAX)"
	.align 8
.LC26:
	.string	"lead_zero <= (base == 16 ? ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN) / 4 : ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN))"
	.section	.rodata.str1.1
.LC27:
	.string	"bits != 0"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"int_no <= (uintmax_t) (exponent < 0 ? (INTMAX_MAX - bits + 1) / 4 : (INTMAX_MAX - exponent - bits + 1) / 4)"
	.align 8
.LC29:
	.string	"dig_no > int_no && exponent <= 0 && exponent >= MIN_10_EXP - (DIG + 2)"
	.section	.rodata.str1.1
.LC30:
	.string	"numsize < RETURN_LIMB_SIZE"
.LC31:
	.string	"int_no > 0 && exponent == 0"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"int_no == 0 && *startp != L_('0')"
	.section	.rodata.str1.1
.LC33:
	.string	"need_frac_digits > 0"
.LC34:
	.string	"numsize == 1 && n < d"
.LC35:
	.string	"empty == 1"
.LC36:
	.string	"numsize == densize"
.LC37:
	.string	"cy != 0"
	.text
	.p2align 4,,15
	.globl	____strtof128_l_internal
	.hidden	____strtof128_l_internal
	.type	____strtof128_l_internal, @function
____strtof128_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r10d, %r10d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r8
	subq	$13928, %rsp
	testl	%edx, %edx
	movq	8(%rcx), %rax
	movq	%rsi, (%rsp)
	movq	$0, 8(%rsp)
	jne	.L652
.L129:
	movq	64(%rax), %r13
	movq	%r8, 24(%rsp)
	movq	%r10, 16(%rsp)
	movq	%r13, %rdi
	call	strlen
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	movq	16(%rsp), %r10
	movq	24(%rsp), %r8
	je	.L653
	movq	$0, 104(%rsp)
	leaq	-1(%r14), %rax
	movq	104(%r8), %rdi
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L409:
	movq	%r11, %rax
.L131:
	movsbq	1(%rax), %rcx
	leaq	1(%rax), %r11
	testb	$32, 1(%rdi,%rcx,2)
	movq	%rcx, %rdx
	jne	.L409
	cmpb	$45, %cl
	je	.L654
	cmpb	$43, %cl
	movl	$0, 16(%rsp)
	je	.L655
.L133:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L411
	cmpb	%al, %dl
	jne	.L135
	movl	$1, %esi
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L137:
	addq	$1, %rsi
	cmpb	%r9b, %cl
	jne	.L135
.L136:
	movzbl	0(%r13,%rsi), %ecx
	movzbl	(%r11,%rsi), %r9d
	testb	%cl, %cl
	jne	.L137
.L134:
	subl	$48, %r9d
	cmpb	$9, %r9b
	jbe	.L138
.L135:
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	jbe	.L138
	movq	112+_nl_C_locobj(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	cmpb	$105, %al
	je	.L656
	cmpb	$110, %al
	je	.L657
.L178:
	cmpq	$0, (%rsp)
	jne	.L658
.L147:
	pxor	%xmm0, %xmm0
.L128:
	addq	$13928, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L138:
	cmpb	$48, %dl
	movq	112(%r8), %rsi
	je	.L659
	testq	%r10, %r10
	movl	$10, %r9d
	je	.L660
.L152:
	movsbq	%dl, %r12
	movzbl	(%r10), %ebp
	movq	%r11, %rbx
	cmpb	$48, %r12b
	movq	$-1, %r15
	je	.L417
	.p2align 4,,10
	.p2align 3
.L661:
	testb	%bpl, %bpl
	je	.L418
	cmpb	%bpl, (%rbx)
	jne	.L153
	movl	$1, %edx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L158:
	addq	$1, %rdx
	cmpb	%cl, -1(%rbx,%rdx)
	jne	.L153
.L157:
	movzbl	(%r10,%rdx), %ecx
	testb	%cl, %cl
	jne	.L158
	subq	$1, %rdx
.L156:
	addq	%rbx, %rdx
.L155:
	movsbq	1(%rdx), %r12
	leaq	1(%rdx), %rbx
	cmpb	$48, %r12b
	jne	.L661
.L417:
	movq	%rbx, %rdx
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L655:
	movsbq	1(%r11), %rdx
	leaq	2(%rax), %r11
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L660:
	movsbq	%dl, %r12
	movq	%r11, %rbx
	movl	$10, %r9d
	.p2align 4,,10
	.p2align 3
.L153:
	leal	-48(%r12), %edx
	cmpb	$9, %dl
	jbe	.L421
	movsbq	%r12b, %rdx
	movl	(%rsi,%rdx,4), %edi
	leal	-97(%rdi), %edx
	cmpb	$5, %dl
	ja	.L456
	cmpl	$16, %r9d
	je	.L421
.L456:
	testb	%al, %al
	je	.L161
	cmpb	%al, (%rbx)
	jne	.L162
	movl	$1, %edx
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L164:
	addq	$1, %rdx
	cmpb	%cl, -1(%rbx,%rdx)
	jne	.L162
.L163:
	movzbl	0(%r13,%rdx), %ecx
	testb	%cl, %cl
	jne	.L164
.L161:
	cmpl	$16, %r9d
	jne	.L421
	cmpq	%r11, %rbx
	jne	.L421
	movq	32(%rsp), %rdi
	movsbq	(%rbx,%rdi), %rdx
	leal	-48(%rdx), %edi
	cmpb	$9, %dil
	ja	.L662
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%rbx, %rbp
	xorl	%r15d, %r15d
.L159:
	movq	$-1, %rcx
.L175:
	leal	-48(%r12), %edx
	cmpb	$9, %dl
	jbe	.L168
	cmpl	$16, %r9d
	je	.L663
.L169:
	testq	%r10, %r10
	jne	.L664
.L171:
	cmpq	$0, 8(%rsp)
	je	.L176
	cmpq	%r11, %rbp
	ja	.L665
.L176:
	xorl	%esi, %esi
	testq	%r15, %r15
	sete	%sil
	negq	%rsi
	testb	%al, %al
	je	.L186
	cmpb	%al, 0(%rbp)
	jne	.L427
	movl	$1, %eax
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L189:
	addq	$1, %rax
	cmpb	%dl, -1(%rbp,%rax)
	jne	.L427
.L188:
	movzbl	0(%r13,%rax), %edx
	testb	%dl, %dl
	jne	.L189
.L186:
	movq	32(%rsp), %rax
	movq	%r15, %rdx
	leaq	0(%rbp,%rax), %rcx
	movsbq	(%rcx), %r12
	movq	%rcx, %rbp
	subq	%rcx, %rdx
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L666:
	movq	112(%r8), %rdi
	movsbq	%r12b, %rax
	movl	(%rdi,%rax,4), %eax
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L187
.L191:
	cmpb	$48, %r12b
	je	.L190
	cmpq	$-1, %rsi
	movq	%rbp, %rax
	sete	%dil
	subq	%rcx, %rax
	testb	%dil, %dil
	cmovne	%rax, %rsi
.L190:
	addq	$1, %rbp
	movsbq	0(%rbp), %r12
.L394:
	leal	-48(%r12), %eax
	leaq	(%rdx,%rbp), %r14
	cmpb	$9, %al
	jbe	.L191
	cmpl	$16, %r9d
	je	.L666
.L187:
	testq	%r14, %r14
	js	.L667
	movq	112(%r8), %rax
	cmpl	$16, %r9d
	movl	(%rax,%r12,4), %eax
	jne	.L457
	cmpb	$112, %al
	jne	.L457
.L193:
	movsbl	1(%rbp), %ecx
	cmpb	$45, %cl
	je	.L668
	cmpb	$43, %cl
	je	.L669
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L431
	cmpl	$16, %r9d
	leaq	1(%rbp), %r12
	je	.L200
.L201:
	testq	%r15, %r15
	je	.L209
	testq	%rsi, %rsi
	jne	.L459
	testq	%r15, %r15
	js	.L459
	movl	$4933, %edi
	subq	%r15, %rdi
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L663:
	movsbq	%r12b, %rdx
	movl	(%rsi,%rdx,4), %edx
	subl	$97, %edx
	cmpb	$5, %dl
	ja	.L169
	.p2align 4,,10
	.p2align 3
.L168:
	addq	$1, %r15
	movq	%rbp, %rdx
.L170:
	leaq	1(%rdx), %rbp
	movsbq	1(%rdx), %r12
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L654:
	movsbq	1(%r11), %rdx
	movl	$1, 16(%rsp)
	leaq	2(%rax), %r11
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L457:
	cmpl	$16, %r9d
	je	.L431
	cmpb	$101, %al
	je	.L193
.L431:
	movq	%rbp, %r12
.L195:
	cmpq	%r15, %r14
	jbe	.L226
	cmpb	$48, -1(%rbp)
	jne	.L185
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L227:
	subq	$1, %rax
	leaq	(%rax,%r14), %rdx
	subq	%rbp, %rdx
	cmpb	$48, -1(%rax)
	je	.L227
	cmpq	%rdx, %r15
	ja	.L670
	movq	%rdx, %r14
	movq	%rax, %rbp
.L226:
	cmpq	%r15, %r14
	jne	.L185
	testq	%r14, %r14
	je	.L185
	movq	104(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L671
	cmpq	$0, (%rsp)
	je	.L236
.L397:
	movq	(%rsp), %rax
	movq	%r12, (%rax)
.L235:
	testq	%r14, %r14
	jne	.L236
.L166:
	movl	16(%rsp), %r15d
	testl	%r15d, %r15d
	je	.L147
	movdqa	.LC10(%rip), %xmm0
	jmp	.L128
.L690:
	testb	%dl, %dl
	movq	8(%rsp), %rsi
	je	.L185
.L642:
	movq	%rcx, 104(%rsp)
	.p2align 4,,10
	.p2align 3
.L185:
	cmpq	$0, (%rsp)
	jne	.L397
	jmp	.L235
.L658:
	movq	(%rsp), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, (%rax)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L427:
	movq	%r15, %r14
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L659:
	movsbq	1(%r11), %rdi
	cmpl	$120, (%rsi,%rdi,4)
	movq	%rdi, %r12
	je	.L149
	testq	%r10, %r10
	movl	$10, %r9d
	jne	.L152
.L150:
	movq	%r11, %rbx
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L672:
	movsbq	1(%rbx), %r12
.L154:
	addq	$1, %rbx
	cmpb	$48, %r12b
	je	.L672
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L236:
	testq	%rsi, %rsi
	je	.L237
	movzbl	0(%r13), %ecx
	movzbl	(%rbx), %eax
	movzbl	1(%r13), %edi
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L238:
	movzbl	1(%rbx), %eax
.L240:
	addq	$1, %rbx
.L243:
	cmpb	%al, %cl
	jne	.L238
	testb	%dil, %dil
	je	.L239
	movzbl	1(%rbx), %eax
	cmpb	%dil, %al
	jne	.L240
	movl	$2, %eax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$1, %rax
	cmpb	%dl, -1(%rbx,%rax)
	jne	.L436
.L241:
	movzbl	0(%r13,%rax), %edx
	testb	%dl, %dl
	jne	.L242
.L239:
	cmpl	$16, %r9d
	je	.L673
	testq	%rsi, %rsi
	movq	%rsi, %rcx
	js	.L400
	movq	104(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L245
.L246:
	movq	32(%rsp), %rdi
	subq	%rcx, %rax
	subq	%rsi, %r14
	movq	%rax, 104(%rsp)
	leaq	(%rdi,%rsi), %rdx
	addq	%rdx, %rbx
.L237:
	cmpl	$16, %r9d
	je	.L674
	movq	104(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L675
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
.L271:
	addq	%rax, %r15
	subq	%rax, %rcx
	movl	$4933, %eax
	subq	%r15, %rax
	movq	%rcx, 104(%rsp)
	cmpq	%rax, %rcx
	jg	.L645
	cmpq	$-4966, %rcx
	jl	.L676
	testq	%r15, %r15
	jne	.L276
	testq	%r14, %r14
	je	.L279
	leaq	4966(%rcx), %rax
	cmpq	$4966, %rax
	ja	.L279
	cmpb	$48, (%rbx)
	je	.L306
	movl	$1, %eax
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movl	$16496, %esi
	shrq	%rdx
	leal	114(%rdx), %eax
	movl	%ecx, %edx
	cmpl	$16496, %eax
	cmovg	%esi, %eax
	addl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L677
	leaq	104(%rsp), %rax
	movq	$0, 56(%rsp)
	xorl	%ebp, %ebp
	movq	%rax, 88(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 72(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, (%rsp)
.L308:
	movslq	%ecx, %rax
	movq	%r14, %rsi
	movl	%r14d, %ecx
	subl	%r15d, %ecx
	subq	%r15, %rsi
	movl	$0, 84(%rsp)
	cmpq	%rsi, %rax
	movslq	%ecx, %rcx
	cmovg	%rcx, %rax
	addq	%r15, %rax
	cmpq	%rax, %r14
	jle	.L310
	movq	%rax, %r14
	movl	$1, 84(%rsp)
.L310:
	movl	%r14d, %eax
	xorl	%r12d, %r12d
	movq	%rbx, 64(%rsp)
	subl	%r15d, %eax
	movl	%ebp, 80(%rsp)
	leaq	_fpioconst_pow10(%rip), %r14
	movl	%eax, %r15d
	movl	%eax, 40(%rsp)
	movq	(%rsp), %rax
	subl	%edx, %r15d
	movl	$1, %r13d
	movq	%r10, 48(%rsp)
	movq	%r12, %rbx
	movq	%rax, 24(%rsp)
	leaq	7024(%rsp), %rax
	movq	%rax, 8(%rsp)
	movq	%rax, %rbp
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L679:
	leaq	0(,%r12,8), %rdx
	movq	%rbp, %rdi
	movq	%r12, %rbx
	call	memcpy@PLT
.L311:
	addl	%r13d, %r13d
	addq	$24, %r14
	testl	%r15d, %r15d
	je	.L678
.L313:
	testl	%r15d, %r13d
	je	.L311
	movq	8(%r14), %rax
	leaq	__tens(%rip), %rdi
	xorl	%r13d, %r15d
	testq	%rbx, %rbx
	leaq	-1(%rax), %r12
	movq	(%r14), %rax
	leaq	8(%rdi,%rax,8), %rsi
	je	.L679
	movq	%r12, %rdx
	movq	24(%rsp), %r12
	movq	%rbx, %r8
	movq	%rbp, %rcx
	movq	%r12, %rdi
	call	__mpn_mul
	movq	8(%r14), %rdx
	testq	%rax, %rax
	leaq	-1(%rbx,%rdx), %rbx
	jne	.L444
	movq	%rbp, %rax
	subq	$1, %rbx
	movq	%r12, %rbp
	movq	%rax, 24(%rsp)
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L669:
	movsbl	2(%rbp), %ecx
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L431
	cmpl	$16, %r9d
	leaq	2(%rbp), %r12
	jne	.L201
.L200:
	testq	%r15, %r15
	je	.L680
	testq	%rsi, %rsi
	jne	.L458
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L458
	movl	$4096, %eax
	subq	%r15, %rax
	leaq	3(,%rax,4), %rdi
.L206:
	testq	%rdi, %rdi
	movl	$0, %eax
	cmovs	%rax, %rdi
.L639:
	movq	%rdi, %rax
	movabsq	$-3689348814741910323, %rdx
	movl	$0, 8(%rsp)
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	movq	%rdi, 24(%rsp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L436:
	movl	%edi, %eax
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L668:
	movsbl	2(%rbp), %ecx
	leaq	2(%rbp), %r12
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L431
	cmpl	$16, %r9d
	je	.L681
	movabsq	$9223372036854770763, %rax
	cmpq	%rax, %r15
	ja	.L682
	leaq	5044(%r15), %rdi
.L640:
	movq	%rdi, %rax
	movabsq	$-3689348814741910323, %rdx
	movl	$1, 8(%rsp)
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %rdi
	movq	%rdi, 24(%rsp)
.L203:
	movq	104(%rsp), %rax
	xorl	%r11d, %r11d
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L213:
	subl	$48, %ecx
	cmpq	%rdx, %rax
	movslq	%ecx, %rcx
	je	.L683
.L215:
	leaq	(%rax,%rax,4), %rax
	addq	$1, %r12
	movl	$1, %r11d
	leaq	(%rcx,%rax,2), %rax
	movsbl	(%r12), %ecx
	leal	-48(%rcx), %edi
	cmpb	$9, %dil
	ja	.L684
.L217:
	cmpq	%rdx, %rax
	jle	.L213
.L216:
	testb	%r11b, %r11b
	jne	.L685
.L214:
	cmpq	$-1, %rsi
	je	.L686
	movl	8(%rsp), %r13d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r13d, %r13d
	movl	$34, %fs:(%rax)
	jne	.L687
	movl	16(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L223
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__multf3@PLT
	.p2align 4,,10
	.p2align 3
.L224:
	addq	$1, %r12
	movzbl	(%r12), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L224
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.L128
	movq	%r12, (%rax)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L683:
	cmpq	%rcx, 24(%rsp)
	jge	.L215
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	$16, %r9d
	je	.L688
	xorl	%r15d, %r15d
	cmpb	$101, %dil
	movq	%rbx, %rbp
	je	.L159
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L652:
	movq	80(%rax), %rdi
	movq	%rdi, 8(%rsp)
	movzbl	(%rdi), %edi
	leal	-1(%rdi), %edx
	movb	%dil, 16(%rsp)
	cmpb	$125, %dl
	ja	.L407
	movq	72(%rax), %r10
	cmpb	$0, (%r10)
	jne	.L129
	movq	$0, 8(%rsp)
	xorl	%r10d, %r10d
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$3, %edx
	movq	%r11, %rdi
	movq	%r11, 8(%rsp)
	call	__strncasecmp_l
	testl	%eax, %eax
	jne	.L178
	movq	(%rsp), %r15
	testq	%r15, %r15
	je	.L141
	movq	8(%rsp), %r11
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	leaq	3(%r11), %rbx
	movq	%r11, (%rsp)
	movq	%rbx, %rdi
	call	__strncasecmp_l
	movq	(%rsp), %r11
	addq	$8, %r11
	testl	%eax, %eax
	cmove	%r11, %rbx
	movq	%rbx, (%r15)
.L141:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	je	.L412
	movdqa	.LC12(%rip), %xmm0
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L657:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	$3, %edx
	movq	%r11, %rdi
	movq	%r11, 8(%rsp)
	call	__strncasecmp_l
	testl	%eax, %eax
	jne	.L178
	movq	8(%rsp), %r11
	movdqa	.LC8(%rip), %xmm0
	cmpb	$40, 3(%r11)
	leaq	3(%r11), %rbx
	je	.L689
.L144:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.L146
	movq	%rbx, (%rax)
.L146:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	je	.L128
	pxor	.LC10(%rip), %xmm0
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L411:
	movl	%edx, %r9d
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L671:
	xorl	%eax, %eax
	cmpl	$16, %r9d
	movq	%rsi, 8(%rsp)
	sete	%al
	subq	$1, %rbp
	xorl	%edx, %edx
	leaq	1(%rax,%rax,2), %r11
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L230:
	movsbl	%al, %edi
	subl	$48, %edi
	cmpl	$9, %edi
	seta	%dil
.L231:
	testb	%dil, %dil
	jne	.L232
	cmpb	$48, %al
	jne	.L690
	addq	%r11, %rcx
	subq	$1, %r15
	subq	$1, %r14
	movq	%rcx, %rax
	setne	%dl
	shrq	$63, %rax
	andb	%al, %dl
	je	.L691
.L232:
	subq	$1, %rbp
.L229:
	cmpl	$16, %r9d
	movzbl	0(%rbp), %eax
	jne	.L230
	movq	104(%r8), %rsi
	movsbq	%al, %rdi
	movzwl	(%rsi,%rdi,2), %edi
	shrw	$12, %di
	xorl	$1, %edi
	andl	$1, %edi
	jmp	.L231
.L674:
	movsbq	(%rbx), %rdx
	movq	104(%r8), %rbp
	testb	$16, 1(%rbp,%rdx,2)
	movq	%rdx, %rax
	jne	.L643
	.p2align 4,,10
	.p2align 3
.L249:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
	testb	$16, 1(%rbp,%rdx,2)
	movq	%rdx, %rax
	je	.L249
	jmp	.L643
	.p2align 4,,10
	.p2align 3
.L251:
	movsbq	(%rbx), %rax
.L643:
	addq	$1, %rbx
	cmpb	$48, %al
	je	.L251
	movsbl	%al, %edx
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L692
	movq	112(%r8), %rdx
	movl	(%rdx,%rax,4), %eax
	subl	$87, %eax
	cltq
.L253:
	leaq	nbits.12735(%rip), %rdx
	movl	(%rdx,%rax,4), %edx
	testl	%edx, %edx
	je	.L693
	movl	$49, %ecx
	movl	$48, %edi
	movslq	%edx, %rsi
	subl	%edx, %ecx
	subl	%edx, %edi
	salq	%cl, %rax
	movq	104(%rsp), %rcx
	movq	%rax, 120(%rsp)
	testq	%rcx, %rcx
	js	.L694
	movabsq	$9223372036854775807, %rax
	subq	%rcx, %rax
	subq	%rsi, %rax
	leaq	4(%rax), %rsi
	addq	$1, %rax
	cmovs	%rsi, %rax
	sarq	$2, %rax
.L256:
	cmpq	%r15, %rax
	jb	.L695
	leal	-1(%rdx), %eax
	movl	$1, %r9d
	movl	$3, %r13d
	movq	32(%rsp), %r12
	cltq
	leaq	-4(%rax,%r15,4), %rsi
	addq	%rcx, %rsi
	movq	%rsi, 104(%rsp)
	movq	%rsi, (%rsp)
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L697:
	leal	-3(%rdi), %ecx
	subl	$4, %edi
	salq	%cl, %rax
	movq	%rax, %rcx
	orq	%r15, %rcx
	movq	%rcx, 112(%rsp,%rdx,8)
.L263:
	movq	%r11, %rbx
	movq	%r10, %r14
.L258:
	movq	%r14, %r10
	subq	$1, %r10
	je	.L696
	movsbq	(%rbx), %rax
	testb	$16, 1(%rbp,%rax,2)
	movq	%rax, %rdx
	jne	.L259
	addq	%r12, %rbx
	movsbq	(%rbx), %rdx
.L259:
	movsbl	%dl, %eax
	leaq	1(%rbx), %r11
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L644
	movq	112(%r8), %rax
	movl	(%rax,%rdx,4), %eax
	subl	$87, %eax
.L644:
	movslq	%r9d, %rdx
	cmpl	$2, %edi
	cltq
	movq	112(%rsp,%rdx,8), %r15
	jg	.L697
	movl	%r13d, %ecx
	movq	%rax, %rsi
	subl	%edi, %ecx
	shrq	%cl, %rsi
	movq	%rsi, %rcx
	orq	%r15, %rcx
	movq	%rcx, 112(%rsp,%rdx,8)
	leal	61(%rdi), %ecx
	salq	%cl, %rax
	testl	%r9d, %r9d
	je	.L698
	movq	%rax, 112(%rsp)
	addl	$60, %edi
	xorl	%r9d, %r9d
	jmp	.L263
.L149:
	testq	%r10, %r10
	leaq	2(%r11), %rbx
	movzbl	2(%r11), %edx
	jne	.L415
	cmpb	$48, %dl
	jne	.L416
	movsbq	3(%r11), %r12
	movq	$0, 8(%rsp)
	movq	%rbx, %r11
	movl	$16, %r9d
	jmp	.L150
.L664:
	movzbl	(%r10), %edx
	testb	%dl, %dl
	je	.L424
	cmpb	%dl, 0(%rbp)
	jne	.L171
	movl	$1, %edx
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	addq	$1, %rdx
	cmpb	%dil, -1(%rbp,%rdx)
	jne	.L171
.L173:
	movzbl	(%r10,%rdx), %edi
	testb	%dil, %dil
	jne	.L174
	subq	$1, %rdx
.L172:
	addq	%rbp, %rdx
	jmp	.L170
.L684:
	movl	8(%rsp), %r11d
	movq	%rax, %rdx
	negq	%rdx
	testl	%r11d, %r11d
	cmovne	%rdx, %rax
	movq	%rax, 104(%rsp)
	jmp	.L195
.L407:
	movq	$0, 8(%rsp)
	jmp	.L129
.L687:
	movl	16(%rsp), %ebp
	testl	%ebp, %ebp
	je	.L222
	movdqa	.LC0(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__multf3@PLT
	jmp	.L224
.L692:
	movslq	%edx, %rax
	jmp	.L253
.L209:
	cmpq	$-1, %rsi
	je	.L433
	movabsq	$9223372036854770874, %rax
	cmpq	%rax, %rsi
	ja	.L699
	leaq	4933(%rsi), %rdi
	jmp	.L639
.L662:
	movl	(%rsi,%rdx,4), %edx
	subl	$97, %edx
	cmpb	$5, %dl
	jbe	.L421
.L165:
	movq	8(%rsp), %rcx
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	movl	%r9d, 24(%rsp)
	movq	%r11, 8(%rsp)
	call	__correctly_grouped_prefixmb
	cmpq	$0, (%rsp)
	je	.L166
	movq	8(%rsp), %r11
	movl	24(%rsp), %r9d
	cmpq	%rax, %r11
	je	.L700
.L167:
	movq	(%rsp), %rdi
.L648:
	movq	%rax, (%rdi)
	jmp	.L166
.L681:
	movabsq	$2305843009213689828, %rax
	cmpq	%rax, %r15
	ja	.L701
	leaq	16494(,%r15,4), %rdi
	jmp	.L640
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%rbp, %r9
	cmpq	(%rsp), %r9
	movq	48(%rsp), %r10
	movq	%rbx, %r12
	movl	%r15d, 48(%rsp)
	movq	64(%rsp), %rbx
	movl	80(%rsp), %ebp
	je	.L702
.L314:
	subq	$8, %rsp
	movq	%rbx, %rdi
	pushq	%r10
	movq	48(%rsp), %r9
	movq	104(%rsp), %r8
	movq	88(%rsp), %rcx
	movq	16(%rsp), %rdx
	movl	56(%rsp), %esi
	call	str_to_mpn.isra.0
	leaq	-1(%r12), %rax
	bsrq	7040(%rsp,%rax,8), %rbx
	movq	%rax, 56(%rsp)
	popq	%rcx
	popq	%rsi
	xorq	$63, %rbx
	testl	%ebx, %ebx
	jne	.L315
.L647:
	movq	96(%rsp), %rdx
.L316:
	movq	56(%rsp), %rax
	cmpq	$1, %r12
	movq	%rax, 104(%rsp)
	je	.L319
	cmpq	$2, %r12
	jne	.L703
	cmpq	$1, %rdx
	movq	7024(%rsp), %r15
	movq	7032(%rsp), %r13
	movq	128(%rsp), %r8
	jg	.L329
	cmpq	%r8, %r13
	jbe	.L445
	testl	%ebp, %ebp
	je	.L704
	cmpl	$49, %ebp
	jle	.L705
	movl	$113, %eax
	subl	%ebp, %eax
	movl	%eax, %ecx
	movl	%eax, 48(%rsp)
	jne	.L706
	leaq	112(%rsp), %r12
	movq	%r8, %rbx
.L334:
	addl	$64, %ebp
	cmpl	$113, %ebp
	jle	.L447
	xorl	%r14d, %r14d
	xorl	%r8d, %r8d
.L335:
	orq	%rbx, %r8
	movq	104(%rsp), %rax
	movl	$63, %r8d
	setne	%r9b
	subl	48(%rsp), %r8d
	orb	84(%rsp), %r9b
	movl	16(%rsp), %edx
	movq	%r14, %rcx
	movq	%r12, %rdi
	leaq	-1(%rax), %rsi
	andl	$1, %r9d
	movslq	%r8d, %r8
	call	round_and_return
	jmp	.L128
.L444:
	movq	%rbp, %rax
	movq	24(%rsp), %rbp
	movq	%rax, 24(%rsp)
	jmp	.L311
.L696:
	cmpl	$1, %r9d
	movq	(%rsp), %rsi
	jne	.L269
	movq	$0, 112(%rsp)
.L269:
	movl	16(%rsp), %edx
	leaq	112(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L128
.L686:
	movl	16(%rsp), %r14d
	pxor	%xmm0, %xmm0
	testl	%r14d, %r14d
	je	.L224
	movdqa	.LC10(%rip), %xmm0
	jmp	.L224
.L675:
	movq	%r15, %rax
	negq	%rax
	cmpq	%rcx, %rax
	cmovl	%rcx, %rax
	jmp	.L271
.L315:
	movq	8(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	(%rsp), %rdi
	movq	96(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L647
	movq	96(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, 128(%rsp,%rcx,8)
	movq	%rdx, 96(%rsp)
	jmp	.L316
.L276:
	leaq	104(%rsp), %r8
	leaq	128(%rsp), %rdx
	leaq	96(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r8, 96(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%rdx, 8(%rsp)
	pushq	%r10
	movq	48(%rsp), %r9
	movq	%r10, 24(%rsp)
	call	str_to_mpn.isra.0
	movq	120(%rsp), %rdx
	movq	%rax, %rbx
	popq	%rdi
	popq	%r8
	testq	%rdx, %rdx
	movq	96(%rsp), %r13
	movq	8(%rsp), %r10
	jle	.L281
	leaq	7024(%rsp), %rax
	movl	$1, %r9d
	leaq	_fpioconst_pow10(%rip), %rbp
	movq	%r13, %rdi
	movl	%r9d, %r12d
	movq	%rbx, %r13
	movq	%rax, 8(%rsp)
	movq	%rax, %r11
	movq	(%rsp), %rax
	movq	%rax, 24(%rsp)
.L282:
	movslq	%r12d, %rax
	testq	%rdx, %rax
	je	.L283
.L708:
	movq	8(%rbp), %rsi
	xorq	%rdx, %rax
	movq	%r10, 48(%rsp)
	movq	%rax, 104(%rsp)
	movq	0(%rbp), %rax
	leaq	-1(%rsi), %rbx
	leaq	__tens(%rip), %rsi
	cmpq	%rdi, %rbx
	leaq	8(%rsi,%rax,8), %rsi
	jg	.L284
	movq	%rsi, %rcx
	movq	24(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%rbx, %r8
	movq	%r11, %rdi
	movq	%r11, 40(%rsp)
	call	__mpn_mul
	movq	40(%rsp), %r11
	movq	48(%rsp), %r10
.L285:
	movq	96(%rsp), %rdi
	movq	104(%rsp), %rdx
	addq	%rbx, %rdi
	testq	%rax, %rax
	movq	%rdi, 96(%rsp)
	jne	.L286
	subq	$1, %rdi
	movq	%rdi, 96(%rsp)
.L286:
	addl	%r12d, %r12d
	addq	$24, %rbp
	testq	%rdx, %rdx
	je	.L707
	movq	24(%rsp), %rax
	movq	%r11, 24(%rsp)
	movq	%rax, %r11
	movslq	%r12d, %rax
	testq	%rdx, %rax
	jne	.L708
.L283:
	addl	%r12d, %r12d
	addq	$24, %rbp
	jmp	.L282
.L284:
	movq	24(%rsp), %rcx
	movq	%rdi, %r8
	movq	%rbx, %rdx
	movq	%r11, %rdi
	movq	%r11, 40(%rsp)
	call	__mpn_mul
	movq	48(%rsp), %r10
	movq	40(%rsp), %r11
	jmp	.L285
.L707:
	cmpq	8(%rsp), %r11
	movq	%r13, %rbx
	movq	%rdi, %r13
	jne	.L281
	leaq	0(,%rdi,8), %rdx
	movq	(%rsp), %rdi
	movq	%r11, %rsi
	movq	%r10, 8(%rsp)
	call	memcpy@PLT
	movq	8(%rsp), %r10
.L281:
	leaq	-1(%r13), %rdx
	movl	%r13d, %r11d
	sall	$6, %r11d
	bsrq	128(%rsp,%rdx,8), %rax
	xorq	$63, %rax
	subl	%eax, %r11d
	cmpl	$16384, %r11d
	movl	%r11d, %ebp
	jg	.L645
	cmpl	$113, %r11d
	jle	.L290
	leal	-113(%r11), %eax
	movl	%eax, %esi
	sarl	$6, %esi
	andl	$63, %eax
	movslq	%esi, %r10
	movq	%r10, %r9
	jne	.L291
	movdqu	128(%rsp,%r9,8), %xmm0
	subq	$1, %r10
	movl	$63, %r8d
	movq	128(%rsp,%r10,8), %rbx
	movaps	%xmm0, 112(%rsp)
.L292:
	cmpq	$0, 128(%rsp)
	jne	.L440
	movq	(%rsp), %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L295:
	movslq	%eax, %rdx
	addq	$1, %rax
	cmpq	$0, -8(%rcx,%rax,8)
	je	.L295
.L294:
	cmpq	%r15, %r14
	movl	$1, %r9d
	ja	.L296
	xorl	%r9d, %r9d
	cmpq	%r10, %rdx
	setl	%r9b
.L296:
	leal	-1(%r11), %esi
	movl	16(%rsp), %edx
	leaq	112(%rsp), %rdi
	movq	%rbx, %rcx
	movslq	%esi, %rsi
	call	round_and_return
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L673:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rsi
	ja	.L400
	movq	104(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	cmpq	%rsi, %rdx
	jb	.L245
	leaq	0(,%rsi,4), %rcx
	jmp	.L246
.L691:
	movq	8(%rsp), %rsi
	jmp	.L642
.L418:
	movq	%r15, %rdx
	jmp	.L156
.L688:
	cmpb	$112, %dil
	jne	.L165
	cmpq	%r11, %rbx
	jne	.L421
	jmp	.L165
.L703:
	movq	40(%rsp), %rax
	movq	8(%rsp), %rdi
	movq	7024(%rsp,%rax,8), %r14
	leaq	-2(%r12), %rax
	movq	%rax, 72(%rsp)
	movq	7024(%rsp,%rax,8), %r15
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rsi
	movq	(%rsp), %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	js	.L709
	movq	96(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	$0, 128(%rsp,%rdx,8)
	movq	%rax, 96(%rsp)
.L354:
	cmpq	%rax, %r12
	jle	.L355
	movq	%r12, %rbx
	subq	%rax, %rbx
	movq	%rbx, %r11
	salq	$6, %r11
	testl	%ebp, %ebp
	je	.L710
	addq	56(%rsp), %r11
	cmpq	$113, %r11
	jg	.L358
	cmpq	$1, %rbx
	jne	.L359
	movq	112(%rsp), %rdx
	movq	$0, 112(%rsp)
	movq	%rdx, 120(%rsp)
.L360:
	movl	%ebx, %edx
	sall	$6, %edx
	addl	%edx, %ebp
.L357:
	testl	%eax, %eax
	movslq	%eax, %rcx
	jle	.L365
	leal	-1(%rax), %edi
	movq	(%rsp), %rax
	addq	%rbx, %rcx
	movslq	%edi, %rdx
	movl	%edi, %edi
	leaq	0(,%rdx,8), %rsi
	salq	$3, %rdi
	subq	%rdx, %rcx
	addq	%rsi, %rax
	leaq	120(%rsp,%rsi), %rsi
	subq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L364
.L365:
	movq	(%rsp), %rax
	movq	%rax, %rdx
	addq	$8, %rax
	leaq	(%rax,%rbx,8), %rcx
	jmp	.L363
.L711:
	addq	$8, %rax
.L363:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L711
	cmpl	$113, %ebp
	movq	$0, 7024(%rsp,%r12,8)
	movq	128(%rsp,%r12,8), %rbx
	jg	.L451
	leaq	120(%rsp), %rsi
.L369:
	leaq	1(%r12), %rax
	movq	(%rsp), %rdi
	movq	%rax, 64(%rsp)
	leal	-2(%r12), %eax
	movslq	%eax, %rdx
	movl	%eax, %eax
	leaq	0(,%rdx,8), %r8
	salq	$3, %rax
	addq	%r8, %rdi
	addq	%rsi, %r8
	subq	%rax, %r8
	leal	-1(%r12), %eax
	movq	%rdi, 56(%rsp)
	movslq	%eax, %r9
	movl	%eax, 80(%rsp)
	leaq	112(%rsp), %rax
	subq	%rdx, %r9
	movq	%rax, 88(%rsp)
	movq	%r8, %rax
	movl	%ebp, %r8d
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L373:
	cmpq	%rbx, %r14
	movq	$-1, %r13
	je	.L374
	movq	40(%rsp), %rax
	movq	%rbx, %rdx
	movq	72(%rsp), %rsi
	movq	128(%rsp,%rax,8), %rax
	movq	%rax, 24(%rsp)
#APP
# 1727 "../stdlib/strtod_l.c" 1
	divq %r14
# 0 "" 2
#NO_APP
	movq	%rax, %r13
	movq	%rdx, %rbx
	movq	%r15, %rax
#APP
# 1728 "../stdlib/strtod_l.c" 1
	mulq %r13
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L712:
	xorl	%edx, %edx
	cmpq	%r15, %rax
	setb	%dl
	subq	%r15, %rax
	subq	%rdx, %rcx
.L375:
	cmpq	%rbx, %rcx
	ja	.L378
	jne	.L374
	cmpq	%rax, 128(%rsp,%rsi,8)
	jnb	.L374
.L378:
	subq	$1, %r13
	addq	%r14, %rbx
	jnc	.L712
.L374:
	movq	(%rsp), %rbx
	movq	64(%rsp), %rdx
	movq	%r13, %rcx
	movq	8(%rsp), %rsi
	movq	%r9, 32(%rsp)
	movl	%r8d, 24(%rsp)
	movq	%rbx, %rdi
	call	__mpn_submul_1
	cmpq	%rax, 128(%rsp,%r12,8)
	movl	24(%rsp), %r8d
	movq	32(%rsp), %r9
	je	.L379
	movq	8(%rsp), %rdx
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	__mpn_add_n
	testq	%rax, %rax
	movl	24(%rsp), %r8d
	movq	32(%rsp), %r9
	je	.L713
	subq	$1, %r13
.L379:
	movq	40(%rsp), %rax
	movl	80(%rsp), %edx
	movq	128(%rsp,%rax,8), %rbx
	testl	%edx, %edx
	movq	56(%rsp), %rax
	movq	%rbx, 128(%rsp,%r12,8)
	jle	.L384
	.p2align 4,,10
	.p2align 3
.L381:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%r9,8)
	subq	$8, %rax
	cmpq	%rax, %rbp
	jne	.L381
.L384:
	testl	%r8d, %r8d
	movq	$0, 128(%rsp)
	jne	.L714
	testq	%r13, %r13
	jne	.L385
	subq	$64, 104(%rsp)
	movq	$0, 120(%rsp)
	movq	$0, 112(%rsp)
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L714:
	cmpl	$49, %r8d
	leal	64(%r8), %r11d
	jle	.L715
	movl	$113, %eax
	subl	%r8d, %eax
	movl	%eax, 48(%rsp)
	je	.L388
	movq	88(%rsp), %rdi
	movl	%eax, %ecx
	movl	$2, %edx
	movq	%r9, 32(%rsp)
	movl	%r11d, 24(%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	$64, %ecx
	subl	48(%rsp), %ecx
	movq	%r13, %rax
	movq	32(%rsp), %r9
	movl	24(%rsp), %r11d
	shrq	%cl, %rax
	orq	%rax, 112(%rsp)
.L388:
	cmpl	$113, %r11d
	jg	.L372
	movl	%r11d, %r8d
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L385:
	bsrq	%r13, %rax
	movl	$64, %r8d
	movq	$0, 120(%rsp)
	xorq	$63, %rax
	movq	%r13, 112(%rsp)
	subl	%eax, %r8d
	cltq
	subq	%rax, 104(%rsp)
	jmp	.L373
.L715:
	movq	112(%rsp), %rax
	movq	%r13, 112(%rsp)
	movq	%rax, 120(%rsp)
	jmp	.L388
.L319:
	movq	128(%rsp), %r14
	movq	7024(%rsp), %r8
	cmpq	%r8, %r14
	jnb	.L460
	cmpq	$1, %rdx
	jne	.L460
	leaq	112(%rsp), %r12
	movl	48(%rsp), %r15d
	xorl	%r9d, %r9d
	movq	%r12, %rax
	movq	%r8, %r12
	movq	%rax, %r8
.L321:
	movq	%r14, %rdx
	movq	%r9, %rax
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %r12
# 0 "" 2
#NO_APP
	testl	%ebp, %ebp
	movq	%rdx, %r13
	movq	%rax, %rbx
	movq	%rdx, %r14
	je	.L716
	cmpl	$49, %ebp
	leal	64(%rbp), %r10d
	jle	.L717
	movl	$113, %r15d
	subl	%ebp, %r15d
	je	.L328
	movl	%r15d, %ecx
	movq	%r8, %rsi
	movq	%r8, %rdi
	movl	$2, %edx
	movq	%r9, 24(%rsp)
	movl	%r10d, 8(%rsp)
	movq	%r8, (%rsp)
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%rbx, %rax
	movq	24(%rsp), %r9
	subl	%r15d, %ecx
	movl	8(%rsp), %r10d
	movq	(%rsp), %r8
	shrq	%cl, %rax
	orq	%rax, 112(%rsp)
.L328:
	cmpl	$113, %r10d
	jg	.L718
.L326:
	movl	%r10d, %ebp
	jmp	.L321
.L716:
	testq	%rax, %rax
	jne	.L324
.L719:
	movq	%r14, %rdx
	subq	$64, 104(%rsp)
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %r12
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	movq	%rdx, %r14
	je	.L719
.L324:
	bsrq	%rax, %rdx
	movl	$64, %r10d
	movq	%r9, 120(%rsp)
	xorq	$63, %rdx
	movq	%rax, 112(%rsp)
	subl	%edx, %r10d
	movslq	%edx, %rdx
	subq	%rdx, 104(%rsp)
	jmp	.L326
.L718:
	testq	%r13, %r13
	movq	104(%rsp), %rax
	movq	%r8, %r12
	setne	%r9b
	orb	84(%rsp), %r9b
	movl	$63, %r8d
	movl	16(%rsp), %edx
	subl	%r15d, %r8d
	movq	%rbx, %rcx
	leaq	-1(%rax), %rsi
	movslq	%r8d, %r8
	movq	%r12, %rdi
	andl	$1, %r9d
	call	round_and_return
	jmp	.L128
.L451:
	xorl	%r13d, %r13d
.L372:
	testl	%r12d, %r12d
	movl	%r12d, %edx
	js	.L389
	movslq	%r12d, %rax
	cmpq	$0, 128(%rsp,%rax,8)
	jne	.L389
	leal	-1(%r12), %eax
	movl	%r12d, %r12d
	movq	(%rsp), %rsi
	cltq
	movq	%rax, %rcx
	subq	%r12, %rcx
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L720:
	subq	$1, %rax
	cmpq	$0, 8(%rsi,%rax,8)
	jne	.L389
.L390:
	cmpq	%rax, %rcx
	movl	%eax, %edx
	jne	.L720
.L389:
	notl	%edx
	movq	104(%rsp), %rax
	movl	$63, %r8d
	shrl	$31, %edx
	subl	48(%rsp), %r8d
	leaq	112(%rsp), %rdi
	movl	%edx, %r9d
	orl	84(%rsp), %r9d
	movl	16(%rsp), %edx
	leaq	-1(%rax), %rsi
	movq	%r13, %rcx
	movslq	%r8d, %r8
	call	round_and_return
	jmp	.L128
.L694:
	movabsq	$-9223372036854775808, %rax
	subq	%rsi, %rax
	sarq	$2, %rax
	jmp	.L256
.L222:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L224
.L223:
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L224
.L412:
	movdqa	.LC11(%rip), %xmm0
	jmp	.L128
.L665:
	movq	8(%rsp), %rcx
	movq	%r10, %rdx
	movq	%r11, %rdi
	movq	%rbp, %rsi
	movq	%r8, 48(%rsp)
	movl	%r9d, 40(%rsp)
	movq	%r10, 24(%rsp)
	movq	%r11, 8(%rsp)
	call	__correctly_grouped_prefixmb
	cmpq	%rax, %rbp
	movq	8(%rsp), %r11
	movq	24(%rsp), %r10
	movl	40(%rsp), %r9d
	movq	48(%rsp), %r8
	je	.L721
	cmpq	%rax, %r11
	je	.L178
	cmpq	%rax, %rbx
	ja	.L616
	movq	%rbx, %r12
	movl	$0, %r14d
	jnb	.L616
.L180:
	movzbl	(%r12), %edi
	leal	-48(%rdi), %edx
	cmpb	$10, %dl
	adcq	$0, %r14
	addq	$1, %r12
	cmpq	%r12, %rax
	jne	.L180
	movq	%r14, %r15
	xorl	%esi, %esi
	jmp	.L185
.L680:
	cmpq	$-1, %rsi
	je	.L432
	movabsq	$2305843009213689855, %rax
	cmpq	%rax, %rsi
	ja	.L722
	leaq	16387(,%rsi,4), %rdi
	jmp	.L639
.L645:
	movl	16(%rsp), %r10d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r10d, %r10d
	movl	$34, %fs:(%rax)
	je	.L273
	movdqa	.LC2(%rip), %xmm1
	movdqa	.LC3(%rip), %xmm0
	call	__multf3@PLT
	jmp	.L128
.L290:
	cmpq	%r15, %r14
	jne	.L297
	leal	-1(%r11), %ebx
	movl	%ebx, %edx
	sarl	$31, %edx
	shrl	$26, %edx
	leal	(%rbx,%rdx), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$48, %eax
	je	.L723
	cmpl	$47, %eax
	jle	.L724
	cmpq	$1, %r13
	jg	.L725
	leal	-48(%rax), %ecx
	leaq	112(%rsp), %r12
	movl	$2, %eax
	subq	%r13, %rax
	movq	(%rsp), %rsi
	movq	%r13, %rdx
	leaq	(%r12,%rax,8), %rdi
	call	__mpn_rshift
	movl	$1, %edx
	subq	96(%rsp), %rdx
	testq	%rdx, %rdx
	movq	%rax, 112(%rsp,%rdx,8)
	jle	.L300
	cmpq	$1, %rdx
	movq	$0, 112(%rsp)
	je	.L300
.L646:
	movq	$0, 120(%rsp)
.L300:
	movl	16(%rsp), %edx
	movslq	%ebx, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	call	round_and_return
	jmp	.L128
.L717:
	movq	112(%rsp), %rax
	movq	%rbx, 112(%rsp)
	movq	%rax, 120(%rsp)
	jmp	.L328
.L702:
	movq	(%rsp), %rsi
	movq	8(%rsp), %rdi
	leaq	0(,%r12,8), %rdx
	movq	%r10, 24(%rsp)
	call	memcpy@PLT
	movq	24(%rsp), %r10
	jmp	.L314
.L329:
	movq	136(%rsp), %rbx
	leaq	112(%rsp), %r12
.L332:
	movl	$113, %r10d
.L336:
	cmpq	%rbx, %r13
	jne	.L337
.L726:
	addq	%r13, %r8
	movq	%r8, %rbx
	jnc	.L340
	subq	%r15, %rbx
	xorl	%r8d, %r8d
	movq	$-1, %r14
#APP
# 1607 "../stdlib/strtod_l.c" 1
	addq %r15,%r8
	adcq $0,%rbx
# 0 "" 2
#NO_APP
	testl	%ebp, %ebp
	jne	.L342
	movl	$64, %ebp
	xorl	%eax, %eax
	movq	$-1, %r14
.L349:
	subq	%rax, 104(%rsp)
	cmpq	%rbx, %r13
	movq	$0, 120(%rsp)
	movq	%r14, 112(%rsp)
	je	.L726
.L337:
	movq	%rbx, %rdx
	movq	%r8, %rax
#APP
# 1615 "../stdlib/strtod_l.c" 1
	divq %r13
# 0 "" 2
#NO_APP
	movq	%rax, %r14
	movq	%rdx, %rbx
	movq	%r15, %rax
#APP
# 1616 "../stdlib/strtod_l.c" 1
	mulq %r14
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L348:
	cmpq	%rbx, %rdx
	ja	.L344
	jne	.L345
	testq	%rax, %rax
	je	.L345
.L344:
	subq	$1, %r14
#APP
# 1625 "../stdlib/strtod_l.c" 1
	subq %r15,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%r13, %rbx
	jnc	.L348
.L345:
	xorl	%r8d, %r8d
#APP
# 1630 "../stdlib/strtod_l.c" 1
	subq %rax,%r8
	sbbq %rdx,%rbx
# 0 "" 2
#NO_APP
	testl	%ebp, %ebp
	jne	.L342
	testq	%r14, %r14
	movl	$64, %eax
	je	.L349
	bsrq	%r14, %rdx
	movl	$64, %ebp
	xorq	$63, %rdx
	movslq	%edx, %rax
	subl	%edx, %ebp
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L342:
	cmpl	$49, %ebp
	leal	64(%rbp), %r9d
	jle	.L727
	movl	%r10d, %eax
	subl	%ebp, %eax
	movl	%eax, %ebp
	movl	%eax, 48(%rsp)
	je	.L352
	movl	%eax, %ecx
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r8, 24(%rsp)
	movl	%r10d, 8(%rsp)
	movl	%r9d, (%rsp)
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%r14, %rax
	movq	24(%rsp), %r8
	subl	%ebp, %ecx
	movl	8(%rsp), %r10d
	movl	(%rsp), %r9d
	shrq	%cl, %rax
	orq	%rax, 112(%rsp)
.L352:
	cmpl	$113, %r9d
	jg	.L335
	movl	%r9d, %ebp
	jmp	.L336
.L340:
	xorl	%eax, %eax
	testq	%r15, %r15
	movq	%r15, %rdx
	setne	%al
	movq	$-1, %r14
	subq	%rax, %rdx
	movq	%r15, %rax
	negq	%rax
	jmp	.L348
.L727:
	movq	112(%rsp), %rax
	movq	%r14, 112(%rsp)
	movq	%rax, 120(%rsp)
	jmp	.L352
.L355:
	jne	.L728
	testl	%r12d, %r12d
	jle	.L729
	leal	-1(%r12), %ecx
	movq	(%rsp), %rax
	leaq	120(%rsp), %rsi
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	salq	$3, %rdx
	salq	$3, %rcx
	addq	%rdx, %rax
	addq	%rsi, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L370:
	movq	(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rax, %rcx
	jne	.L370
.L371:
	movq	$0, 128(%rsp)
	movq	$0, 7024(%rsp,%r12,8)
	movq	128(%rsp,%r12,8), %rbx
	jmp	.L369
.L698:
	subq	$2, %r14
	movq	(%rsp), %rsi
	je	.L265
	cmpb	$48, 1(%rbx)
	jne	.L438
	addq	$2, %rbx
	addq	%r14, %r11
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L267:
	addq	$1, %rbx
	cmpb	$48, -1(%rbx)
	jne	.L438
.L266:
	cmpq	%rbx, %r11
	jne	.L267
.L265:
	movl	16(%rsp), %edx
	leaq	112(%rsp), %rdi
	movl	$63, %r8d
	movq	%rax, %rcx
	call	round_and_return
	jmp	.L128
.L433:
	movq	$3, 24(%rsp)
	movl	$493, %edx
	movl	$0, 8(%rsp)
	jmp	.L203
.L676:
	movl	16(%rsp), %r9d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r9d, %r9d
	movl	$34, %fs:(%rax)
	je	.L275
	movdqa	.LC0(%rip), %xmm1
	movdqa	.LC1(%rip), %xmm0
	call	__multf3@PLT
	jmp	.L128
.L709:
	movq	96(%rsp), %rax
	jmp	.L354
.L689:
	leaq	7024(%rsp), %rsi
	leaq	4(%r11), %rdi
	movl	$41, %edx
	call	__strtof128_nan
	movq	7024(%rsp), %rax
	cmpb	$41, (%rax)
	leaq	1(%rax), %rdx
	cmove	%rdx, %rbx
	jmp	.L144
.L297:
	leaq	112(%rsp), %r12
	movq	(%rsp), %rsi
	leaq	0(,%r13,8), %rdx
	movl	%r11d, 24(%rsp)
	movq	%r10, 8(%rsp)
	movq	%r12, %rdi
	call	memcpy@PLT
	cmpq	$1, %r13
	movq	8(%rsp), %r10
	movl	24(%rsp), %r11d
	jg	.L305
	movq	$0, 112(%rsp,%r13,8)
.L305:
	cmpq	%r15, %r14
	jbe	.L279
	movq	104(%rsp), %rax
	leaq	4966(%rax), %rdx
	cmpq	$4966, %rdx
	ja	.L279
	testl	%r11d, %r11d
	jle	.L306
	testq	%rax, %rax
	jne	.L730
	movl	$114, %ecx
	movslq	%r11d, %rax
	xorl	%edx, %edx
	subl	%r11d, %ecx
	movq	%rax, 56(%rsp)
	jmp	.L308
.L291:
	movq	128(%rsp,%r10,8), %rbx
	movslq	%eax, %r8
	subq	$1, %r8
	cmpq	%r10, %rdx
	movq	%rbx, %rdi
	jle	.L404
	leal	1(%rsi), %ebp
	movl	%eax, %ecx
	movl	$64, %r12d
	movq	%rbx, %r13
	subl	%eax, %r12d
	movslq	%ebp, %rbp
	shrq	%cl, %r13
	movl	%r12d, %ecx
	movq	128(%rsp,%rbp,8), %rdi
	movq	%r13, 8(%rsp)
	movq	%rdi, %r13
	salq	%cl, %r13
	movq	8(%rsp), %rcx
	orq	%r13, %rcx
	cmpq	%rbp, %rdx
	movq	%rcx, 112(%rsp)
	jle	.L293
	movl	%eax, %ecx
	shrq	%cl, %rdi
	leal	2(%rsi), %ecx
	movq	%rdi, %rbp
	movslq	%ecx, %rcx
	movq	128(%rsp,%rcx,8), %rdi
	movl	%r12d, %ecx
	movq	%rdi, %rsi
	salq	%cl, %rsi
	orq	%rsi, %rbp
	movq	%rbp, 120(%rsp)
.L293:
	subq	%r9, %rdx
	cmpq	$1, %rdx
	jg	.L292
.L404:
	movl	%eax, %ecx
	shrq	%cl, %rdi
	movq	%rdi, 120(%rsp)
	jmp	.L292
.L273:
	movdqa	.LC3(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L128
.L700:
	leaq	-1(%rbx), %rax
	cmpl	$16, %r9d
	cmovne	%r14, %rax
	jmp	.L167
.L445:
	xorl	%ebx, %ebx
	leaq	112(%rsp), %r12
	jmp	.L332
.L275:
	movdqa	.LC1(%rip), %xmm1
	movdqa	%xmm1, %xmm0
	call	__multf3@PLT
	jmp	.L128
.L721:
	movzbl	0(%r13), %eax
	jmp	.L176
.L432:
	movq	$7, 24(%rsp)
	movl	$1638, %edx
	movl	$0, 8(%rsp)
	jmp	.L203
.L438:
	movl	$1, %r9d
	jmp	.L265
.L616:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.L648
	jmp	.L166
.L710:
	subq	%r11, 104(%rsp)
	movl	$0, 48(%rsp)
	jmp	.L357
.L358:
	movl	$113, %edx
	subl	%ebp, %edx
	cmpl	$63, %edx
	movl	%edx, 48(%rsp)
	jg	.L731
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	je	.L360
	leaq	112(%rsp), %rdi
	movl	$2, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	96(%rsp), %rax
	jmp	.L360
.L424:
	movq	%rcx, %rdx
	jmp	.L172
.L704:
	movq	56(%rsp), %r11
	movq	%r8, %rbx
	movl	$0, 48(%rsp)
	xorl	%r8d, %r8d
	leaq	112(%rsp), %r12
	subq	$64, %r11
	movq	%r11, 104(%rsp)
	jmp	.L332
.L724:
	movl	$2, %ebp
	movl	$48, %ecx
	leaq	112(%rsp), %r12
	subl	%eax, %ecx
	movq	%rbp, %rax
	movq	(%rsp), %rsi
	subq	%r13, %rax
	movq	%r13, %rdx
	leaq	(%r12,%rax,8), %rdi
	call	__mpn_lshift
	subq	96(%rsp), %rbp
	testq	%rbp, %rbp
	jle	.L300
.L651:
	cmpq	$1, %rbp
	movq	$0, 112(%rsp)
	jne	.L646
	jmp	.L300
.L440:
	xorl	%edx, %edx
	jmp	.L294
.L729:
	leaq	120(%rsp), %rsi
	jmp	.L371
.L723:
	movl	$2, %ebp
	leaq	112(%rsp), %r12
	movq	(%rsp), %rsi
	subq	%r13, %rbp
	leaq	0(,%r13,8), %rdx
	leaq	(%r12,%rbp,8), %rdi
	call	memcpy@PLT
	testq	%rbp, %rbp
	jg	.L651
	jmp	.L300
.L706:
	leaq	112(%rsp), %r12
	movl	$2, %edx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_lshift
	movq	128(%rsp), %rbx
	jmp	.L334
.L653:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$598, %edx
	call	__assert_fail
.L731:
	leaq	112(%rsp), %rsi
	movl	%edx, %ecx
	movl	$1, %edx
	andl	$63, %ecx
	leaq	8(%rsi), %rdi
	call	__mpn_lshift
	movq	$0, 112(%rsp)
	movq	96(%rsp), %rax
	jmp	.L360
.L447:
	xorl	%r8d, %r8d
	jmp	.L332
.L705:
	movq	112(%rsp), %rax
	movq	%r8, %rbx
	movq	$0, 112(%rsp)
	leaq	112(%rsp), %r12
	movq	%rax, 120(%rsp)
	jmp	.L334
.L306:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$1376, %edx
	call	__assert_fail
.L279:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$1360, %edx
	call	__assert_fail
.L677:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	movl	$1397, %edx
	call	__assert_fail
.L459:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$946, %edx
	call	__assert_fail
.L725:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$1316, %edx
	call	__assert_fail
.L460:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$1497, %edx
	call	__assert_fail
.L693:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$1100, %edx
	call	__assert_fail
.L682:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$938, %edx
	call	__assert_fail
.L667:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$875, %edx
	call	__assert_fail
.L416:
	movq	%rbx, %r11
	movq	$0, 8(%rsp)
	movsbq	%dl, %r12
	movl	$16, %r9d
	jmp	.L153
.L713:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC37(%rip), %rdi
	movl	$1750, %edx
	call	__assert_fail
.L415:
	movq	%rbx, %r11
	movl	$16, %r9d
	movq	$0, 8(%rsp)
	jmp	.L152
.L245:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$1076, %edx
	call	__assert_fail
.L400:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$1072, %edx
	call	__assert_fail
.L670:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	movl	$1021, %edx
	call	__assert_fail
.L701:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$906, %edx
	call	__assert_fail
.L730:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC31(%rip), %rdi
	movl	$1370, %edx
	call	__assert_fail
.L359:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$1671, %edx
	call	__assert_fail
.L695:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$1121, %edx
	call	__assert_fail
.L685:
	movq	%rax, 104(%rsp)
	jmp	.L214
.L722:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$926, %edx
	call	__assert_fail
.L699:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$958, %edx
	call	__assert_fail
.L458:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$914, %edx
	call	__assert_fail
.L728:
	leaq	__PRETTY_FUNCTION__.12647(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$1708, %edx
	call	__assert_fail
	.size	____strtof128_l_internal, .-____strtof128_l_internal
	.p2align 4,,15
	.weak	__strtof128_l
	.hidden	__strtof128_l
	.type	__strtof128_l, @function
__strtof128_l:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	jmp	____strtof128_l_internal
	.size	__strtof128_l, .-__strtof128_l
	.weak	strtof128_l
	.set	strtof128_l,__strtof128_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12597, @object
	.size	__PRETTY_FUNCTION__.12597, 11
__PRETTY_FUNCTION__.12597:
	.string	"str_to_mpn"
	.section	.rodata
	.align 32
	.type	nbits.12735, @object
	.size	nbits.12735, 64
nbits.12735:
	.long	0
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12647, @object
	.size	__PRETTY_FUNCTION__.12647, 25
__PRETTY_FUNCTION__.12647:
	.string	"____strtof128_l_internal"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	0
	.long	0
	.long	-2147418112
	.align 16
.LC1:
	.long	0
	.long	0
	.long	0
	.long	65536
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	-65537
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	4294967295
	.long	2147418111
	.align 16
.LC4:
	.long	0
	.long	0
	.long	0
	.long	1073676288
	.align 16
.LC8:
	.long	0
	.long	0
	.long	0
	.long	2147450880
	.align 16
.LC10:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC11:
	.long	0
	.long	0
	.long	0
	.long	2147418112
	.align 16
.LC12:
	.long	0
	.long	0
	.long	0
	.long	-65536
	.hidden	__strtof128_nan
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
	.hidden	__mpn_cmp
	.hidden	__mpn_lshift
	.hidden	__correctly_grouped_prefixmb
	.hidden	__strncasecmp_l
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	_nl_C_locobj
	.hidden	strlen
	.hidden	__assert_fail
	.hidden	__mpn_mul_1
	.hidden	__mpn_rshift
	.hidden	__mpn_construct_float128
	.hidden	abort
