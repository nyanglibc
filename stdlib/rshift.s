 .text
.globl __mpn_rshift
.type __mpn_rshift,@function
.align 1<<4
__mpn_rshift:
 mov %edx, %eax
 and $3, %eax
 jne .Lnb00
.Lb00:
 mov (%rsi), %r10
 mov 8(%rsi), %r11
 xor %eax, %eax
 shrd %cl, %r10, %rax
 mov 16(%rsi), %r8
 lea 8(%rsi), %rsi
 lea -24(%rdi), %rdi
 sub $4, %rdx
 jmp .L00
.Lnb00:
 cmp $2, %eax
 jae .Lnb01
.Lb01: mov (%rsi), %r9
 xor %eax, %eax
 shrd %cl, %r9, %rax
 sub $2, %rdx
 jb .Lle1
 mov 8(%rsi), %r10
 mov 16(%rsi), %r11
 lea 16(%rsi), %rsi
 lea -16(%rdi), %rdi
 jmp .L01
.Lle1: shr %cl, %r9
 mov %r9, (%rdi)
 ret
.Lnb01:
 jne .Lb11
.Lb10: mov (%rsi), %r8
 mov 8(%rsi), %r9
 xor %eax, %eax
 shrd %cl, %r8, %rax
 sub $3, %rdx
 jb .Lle2
 mov 16(%rsi), %r10
 lea 24(%rsi), %rsi
 lea -8(%rdi), %rdi
 jmp .L10
.Lle2: shrd %cl, %r9, %r8
 mov %r8, (%rdi)
 shr %cl, %r9
 mov %r9, 8(%rdi)
 ret
 .p2align 4
.Lb11:
 mov (%rsi), %r11
 mov 8(%rsi), %r8
 xor %eax, %eax
 shrd %cl, %r11, %rax
 mov 16(%rsi), %r9
 lea 32(%rsi), %rsi
 sub $4, %rdx
 jb .Lend
 .p2align 4
.Ltop: shrd %cl, %r8, %r11
 mov -8(%rsi), %r10
 mov %r11, (%rdi)
.L10: shrd %cl, %r9, %r8
 mov (%rsi), %r11
 mov %r8, 8(%rdi)
.L01: shrd %cl, %r10, %r9
 mov 8(%rsi), %r8
 mov %r9, 16(%rdi)
.L00: shrd %cl, %r11, %r10
 mov 16(%rsi), %r9
 mov %r10, 24(%rdi)
 add $32, %rsi
 lea 32(%rdi), %rdi
 sub $4, %rdx
 jnc .Ltop
.Lend: shrd %cl, %r8, %r11
 mov %r11, (%rdi)
 shrd %cl, %r9, %r8
 mov %r8, 8(%rdi)
 shr %cl, %r9
 mov %r9, 16(%rdi)
 ret
.size __mpn_rshift,.-__mpn_rshift
