	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	try, @function
try:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	%esi, %edi
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %r12
	movl	%edx, %ebx
	movl	%ecx, %r14d
	movq	%r9, %r13
	subq	$8, %rsp
	call	__GI_nl_langinfo
	movq	%rax, %rbp
	movq	(%r12), %rax
	cmpq	%rbp, %rax
	je	.L2
	testq	%rax, %rax
	je	.L3
	movq	%r13, %rdi
	call	__GI___regfree
	movq	$0, (%r12)
.L3:
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	__GI___regcomp
	testl	%eax, %eax
	jne	.L5
	movq	%rbp, (%r12)
.L2:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__GI___regexec
	testl	%eax, %eax
	movl	%r14d, %eax
	cmove	%ebx, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	jmp	.L1
	.size	try, .-try
	.p2align 4,,15
	.globl	rpmatch
	.type	rpmatch, @function
rpmatch:
	pushq	%rbx
	leaq	yesre.4174(%rip), %r9
	leaq	yesexpr.4172(%rip), %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$327680, %esi
	movq	%rdi, %rbx
	call	try
	testl	%eax, %eax
	je	.L15
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbx, %rdi
	leaq	nore.4175(%rip), %r9
	leaq	noexpr.4173(%rip), %r8
	popq	%rbx
	movl	$-1, %ecx
	xorl	%edx, %edx
	movl	$327681, %esi
	jmp	try
	.size	rpmatch, .-rpmatch
	.local	nore.4175
	.comm	nore.4175,64,32
	.local	noexpr.4173
	.comm	noexpr.4173,8,8
	.local	yesre.4174
	.comm	yesre.4174,64,32
	.local	yesexpr.4172
	.comm	yesexpr.4172,8,8
