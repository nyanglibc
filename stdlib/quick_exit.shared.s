	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_quick_exit,quick_exit@@GLIBC_2.24
	.symver __old_quick_exit,quick_exit@GLIBC_2.10
#NO_APP
	.p2align 4,,15
	.globl	__new_quick_exit
	.type	__new_quick_exit, @function
__new_quick_exit:
	leaq	__quick_exit_funcs(%rip), %rsi
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__run_exit_handlers
	.size	__new_quick_exit, .-__new_quick_exit
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__old_quick_exit
	.type	__old_quick_exit, @function
__old_quick_exit:
	leaq	__quick_exit_funcs(%rip), %rsi
	subq	$8, %rsp
	movl	$1, %ecx
	xorl	%edx, %edx
	call	__run_exit_handlers
	.size	__old_quick_exit, .-__old_quick_exit
	.hidden	__run_exit_handlers
	.hidden	__quick_exit_funcs
