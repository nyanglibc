	.text
	.p2align 4,,15
	.globl	atoi
	.hidden	atoi
	.type	atoi, @function
atoi:
	subq	$8, %rsp
	movl	$10, %edx
	xorl	%esi, %esi
	call	strtol
	addq	$8, %rsp
	ret
	.size	atoi, .-atoi
	.hidden	strtol
