	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"sh"
.LC1:
	.string	"-c"
.LC2:
	.string	"/bin/sh"
#NO_APP
	.text
	.p2align 4,,15
	.type	do_system, @function
do_system:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$872, %rsp
	movl	$-1, 8(%rsp)
	movq	$1, 368(%rsp)
	movl	$0, 504(%rsp)
	movq	$0, 376(%rsp)
#APP
# 118 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movl	sa_refcntr(%rip), %eax
	leaq	368(%rsp), %rbx
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, sa_refcntr(%rip)
	je	.L34
.L4:
#APP
# 125 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	subl	$1, lock(%rip)
.L6:
	leaq	112(%rsp), %r12
	leaq	8(%rbx), %rsi
	xorl	%edi, %edi
	orq	$65536, 376(%rsp)
	movq	%r12, %rdx
	call	__GI___sigprocmask
	cmpq	$1, intr(%rip)
	movq	$0, 240(%rsp)
	je	.L7
	movq	$2, 240(%rsp)
.L7:
	cmpq	$1, quit(%rip)
	je	.L8
	orq	$4, 240(%rsp)
.L8:
	leaq	528(%rsp), %rbx
	movq	%rbx, %rdi
	call	__posix_spawnattr_init
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__posix_spawnattr_setsigmask
	leaq	240(%rsp), %rsi
	movq	%rbx, %rdi
	call	__posix_spawnattr_setsigdefault
	movl	$12, %esi
	movq	%rbx, %rdi
	call	__posix_spawnattr_setflags
	movq	__environ@GOTPCREL(%rip), %rax
	leaq	12(%rsp), %rdi
	leaq	48(%rsp), %r8
	leaq	.LC2(%rip), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rcx
	movq	%rbp, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	(%rax), %r9
	leaq	.LC0(%rip), %rax
	movq	%rax, 48(%rsp)
	leaq	.LC1(%rip), %rax
	movq	%rax, 56(%rsp)
	call	__GI___posix_spawn
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__posix_spawnattr_destroy
	testl	%ebp, %ebp
	jne	.L9
	leaq	quit(%rip), %rax
	movl	__libc_pthread_functions_init(%rip), %r13d
	movl	12(%rsp), %edi
	movq	%rax, 16(%rsp)
	leaq	intr(%rip), %rax
	testl	%r13d, %r13d
	movl	%edi, 32(%rsp)
	movq	%rax, 24(%rsp)
	je	.L10
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	80(%rsp), %rdi
	leaq	16(%rsp), %rdx
#APP
# 167 "../sysdeps/posix/system.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	cancel_handler(%rip), %rsi
	call	*%rax
	movl	12(%rsp), %edi
.L11:
	leaq	8(%rsp), %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L35:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L14
.L13:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	__GI___waitpid
	cmpl	$-1, %eax
	movl	12(%rsp), %edi
	je	.L35
.L14:
	cmpl	%edi, %eax
	je	.L15
	movl	$-1, 8(%rsp)
.L15:
	testl	%r13d, %r13d
	je	.L17
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	80(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 175 "../sysdeps/posix/system.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$32512, 8(%rsp)
.L17:
#APP
# 183 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L18
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L19:
	subl	$1, sa_refcntr(%rip)
	je	.L36
.L20:
#APP
# 192 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L21
	subl	$1, lock(%rip)
.L22:
	testl	%ebp, %ebp
	je	.L23
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%ebp, %fs:(%rax)
.L23:
	movl	8(%rsp), %eax
	addq	$872, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	intr(%rip), %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	__GI___sigaction
	leaq	quit(%rip), %rdx
	movq	%rbx, %rsi
	movl	$3, %edi
	call	__GI___sigaction
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	intr(%rip), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigaction
	leaq	quit(%rip), %rsi
	xorl	%edx, %edx
	movl	$3, %edi
	call	__GI___sigaction
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	call	__GI___sigprocmask
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	cancel_handler(%rip), %rax
	movq	%rax, 80(%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 88(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L19
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
#APP
# 125 "../sysdeps/posix/system.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L6
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 125 "../sysdeps/posix/system.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
#APP
# 192 "../sysdeps/posix/system.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L22
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 192 "../sysdeps/posix/system.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.size	do_system, .-do_system
	.p2align 4,,15
	.type	cancel_handler, @function
cancel_handler:
	pushq	%rbx
	movl	$9, %esi
	movq	%rdi, %rbx
	movl	$62, %eax
	subq	$16, %rsp
	movl	16(%rdi), %edi
#APP
# 43 "../sysdeps/unix/sysv/linux/not-errno.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L40
	movq	104+__libc_pthread_functions(%rip), %rax
	leaq	12(%rsp), %rsi
	movl	$1, %edi
#APP
# 85 "../sysdeps/posix/system.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L49:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L39
.L40:
	movl	16(%rbx), %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	__GI___waitpid
	cmpl	$-1, %eax
	je	.L49
.L39:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L41
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	12(%rsp), %edi
#APP
# 88 "../sysdeps/posix/system.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L41:
#APP
# 90 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L42
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L43:
	subl	$1, sa_refcntr(%rip)
	je	.L50
.L44:
#APP
# 96 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L45
	subl	$1, lock(%rip)
.L37:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	movl	$3, %edi
	call	__GI___sigaction
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigaction
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L43
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
#APP
# 96 "../sysdeps/posix/system.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L37
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 96 "../sysdeps/posix/system.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L37
	.size	cancel_handler, .-cancel_handler
	.section	.rodata.str1.1
.LC3:
	.string	"exit 0"
	.text
	.p2align 4,,15
	.globl	__libc_system
	.type	__libc_system, @function
__libc_system:
	testq	%rdi, %rdi
	je	.L55
	jmp	do_system
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC3(%rip), %rdi
	subq	$8, %rsp
	call	do_system
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	__libc_system, .-__libc_system
	.weak	system
	.set	system,__libc_system
	.local	lock
	.comm	lock,4,4
	.local	sa_refcntr
	.comm	sa_refcntr,4,4
	.local	quit
	.comm	quit,152,32
	.local	intr
	.comm	intr,152,32
	.hidden	__lll_lock_wait_private
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__posix_spawnattr_destroy
	.hidden	__posix_spawnattr_setflags
	.hidden	__posix_spawnattr_setsigdefault
	.hidden	__posix_spawnattr_setsigmask
	.hidden	__posix_spawnattr_init
