	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtof_internal
	.hidden	__GI___strtof_internal
	.type	__GI___strtof_internal, @function
__GI___strtof_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtof_l_internal
	.size	__GI___strtof_internal, .-__GI___strtof_internal
	.globl	__strtof_internal
	.set	__strtof_internal,__GI___strtof_internal
	.p2align 4,,15
	.weak	__GI_strtof
	.hidden	__GI_strtof
	.type	__GI_strtof, @function
__GI_strtof:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtof_l_internal
	.size	__GI_strtof, .-__GI_strtof
	.globl	strtof
	.set	strtof,__GI_strtof
	.weak	strtof32
	.set	strtof32,strtof
