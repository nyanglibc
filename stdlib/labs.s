	.text
	.p2align 4,,15
	.globl	labs
	.type	labs, @function
labs:
	movq	%rdi, %rdx
	movq	%rdi, %rax
	sarq	$63, %rdx
	xorq	%rdx, %rax
	subq	%rdx, %rax
	ret
	.size	labs, .-labs
	.weak	imaxabs
	.set	imaxabs,labs
