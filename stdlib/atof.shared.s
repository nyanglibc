	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	atof
	.type	atof, @function
atof:
	xorl	%esi, %esi
	jmp	__GI_strtod
	.size	atof, .-atof
