	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___getrandom
	.hidden	__GI___getrandom
	.type	__GI___getrandom, @function
__GI___getrandom:
#APP
# 29 "../sysdeps/unix/sysv/linux/getrandom.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$318, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/getrandom.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$318, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/getrandom.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__GI___getrandom, .-__GI___getrandom
	.globl	__getrandom
	.set	__getrandom,__GI___getrandom
	.weak	getrandom
	.set	getrandom,__getrandom
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
