	.text
	.p2align 4,,15
	.globl	__strtoul_internal
	.hidden	__strtoul_internal
	.type	__strtoul_internal, @function
__strtoul_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	____strtoul_l_internal
	.size	__strtoul_internal, .-__strtoul_internal
	.globl	__strtoull_internal
	.set	__strtoull_internal,__strtoul_internal
	.p2align 4,,15
	.globl	__strtoul
	.type	__strtoul, @function
__strtoul:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	____strtoul_l_internal
	.size	__strtoul, .-__strtoul
	.weak	strtoul
	.hidden	strtoul
	.set	strtoul,__strtoul
	.weak	strtoumax
	.set	strtoumax,strtoul
	.weak	strtouq
	.set	strtouq,strtoul
	.weak	strtoull
	.set	strtoull,strtoul
	.hidden	____strtoul_l_internal
