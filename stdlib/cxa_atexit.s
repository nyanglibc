	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"cxa_atexit.c"
.LC1:
	.string	"l != NULL"
	.text
	.p2align 4,,15
	.globl	__new_exitfn
	.hidden	__new_exitfn
	.type	__new_exitfn, @function
__new_exitfn:
	cmpb	$0, __exit_funcs_done(%rip)
	jne	.L26
	pushq	%rbp
	pushq	%rbx
	xorl	%r9d, %r9d
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	movq	%rbp, %r8
	je	.L30
	.p2align 4,,10
	.p2align 3
.L3:
	movq	8(%r8), %rax
	testq	%rax, %rax
	je	.L4
	leaq	-1(%rax), %rdx
	movq	%rdx, %rcx
	salq	$5, %rcx
	cmpq	$0, 16(%r8,%rcx)
	jne	.L14
	salq	$5, %rax
	leaq	-48(%r8,%rax), %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	subq	$32, %rcx
	leaq	-1(%rdx), %rsi
	cmpq	$0, 32(%rcx)
	jne	.L5
	movq	%rsi, %rdx
.L6:
	testq	%rdx, %rdx
	jne	.L7
.L4:
	movq	(%r8), %rax
	movq	$0, 8(%r8)
	movq	%r8, %r9
	testq	%rax, %rax
	je	.L8
	movq	%rax, %r8
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	$32, %rdx
	je	.L31
	movq	%rdx, %rax
	addq	$1, %rdx
	salq	$5, %rax
	movq	%rdx, 8(%r8)
	leaq	16(%r8,%rax), %rax
.L9:
	movq	$1, (%rax)
	addq	$1, __new_exitfn_called(%rip)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L31:
	testq	%r9, %r9
	movq	%r9, %r8
	jne	.L8
	movl	$1040, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	%rax, %r8
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L1
	movq	%rbp, (%r8)
	movq	%r8, (%rbx)
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	16(%r8), %rax
	movq	$1, 8(%r8)
	jmp	.L9
.L30:
	leaq	__PRETTY_FUNCTION__.7768(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$112, %edx
	call	__assert_fail
.L26:
	xorl	%eax, %eax
	ret
	.size	__new_exitfn, .-__new_exitfn
	.section	.rodata.str1.1
.LC2:
	.string	"func != NULL"
	.text
	.p2align 4,,15
	.globl	__internal_atexit
	.hidden	__internal_atexit
	.type	__internal_atexit, @function
__internal_atexit:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L44
	movq	%rdi, %rbx
	movq	%rsi, %r12
	movq	%rdx, %rbp
#APP
# 43 "cxa_atexit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L34
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
.L35:
	movq	%rcx, %rdi
	call	__new_exitfn
	testq	%rax, %rax
	je	.L45
	movq	%rbx, %rdi
	movq	%r12, 16(%rax)
	movq	%rbp, 24(%rax)
#APP
# 53 "cxa_atexit.c" 1
	xor %fs:48, %rdi
rol $2*8+1, %rdi
# 0 "" 2
#NO_APP
	movq	$4, (%rax)
	movq	%rdi, 8(%rax)
#APP
# 59 "cxa_atexit.c" 1
	movl %fs:24,%r8d
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	jne	.L39
	subl	$1, __exit_funcs_lock(%rip)
.L32:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L45:
#APP
# 48 "cxa_atexit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L37
	subl	$1, __exit_funcs_lock(%rip)
	movl	$-1, %r8d
	addq	$16, %rsp
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __exit_funcs_lock(%rip)
	je	.L35
	leaq	__exit_funcs_lock(%rip), %rdi
	movq	%rcx, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rcx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
#APP
# 59 "cxa_atexit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jle	.L32
.L43:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__exit_funcs_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 59 "cxa_atexit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
#APP
# 48 "cxa_atexit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	$-1, %r8d
	jle	.L32
	jmp	.L43
.L44:
	leaq	__PRETTY_FUNCTION__.7665(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$41, %edx
	call	__assert_fail
	.size	__internal_atexit, .-__internal_atexit
	.p2align 4,,15
	.globl	__cxa_atexit
	.hidden	__cxa_atexit
	.type	__cxa_atexit, @function
__cxa_atexit:
	leaq	__exit_funcs(%rip), %rcx
	jmp	__internal_atexit
	.size	__cxa_atexit, .-__cxa_atexit
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.7768, @object
	.size	__PRETTY_FUNCTION__.7768, 13
__PRETTY_FUNCTION__.7768:
	.string	"__new_exitfn"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.7665, @object
	.size	__PRETTY_FUNCTION__.7665, 18
__PRETTY_FUNCTION__.7665:
	.string	"__internal_atexit"
	.hidden	__new_exitfn_called
	.comm	__new_exitfn_called,8,8
	.hidden	__exit_funcs
	.globl	__exit_funcs
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__exit_funcs, @object
	.size	__exit_funcs, 8
__exit_funcs:
	.quad	initial
	.local	initial
	.comm	initial,1040,32
	.comm	__exit_funcs_lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__assert_fail
	.hidden	__exit_funcs_done
