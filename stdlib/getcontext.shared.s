.globl __getcontext
.type __getcontext,@function
.align 1<<4
__getcontext:
	movq %rbx, 128(%rdi)
	movq %rbp, 120(%rdi)
	movq %r12, 72(%rdi)
	movq %r13, 80(%rdi)
	movq %r14, 88(%rdi)
	movq %r15, 96(%rdi)
	movq %rdi, 104(%rdi)
	movq %rsi, 112(%rdi)
	movq %rdx, 136(%rdi)
	movq %rcx, 152(%rdi)
	movq %r8, 40(%rdi)
	movq %r9, 48(%rdi)
	movq (%rsp), %rcx
	movq %rcx, 168(%rdi)
	leaq 8(%rsp), %rcx
	movq %rcx, 160(%rdi)
	leaq 424(%rdi), %rcx
	movq %rcx, 224(%rdi)
	fnstenv (%rcx)
	fldenv (%rcx)
	stmxcsr 448(%rdi)
	leaq 296(%rdi), %rdx
	xorl %esi,%esi
	xorl %edi, %edi
	movl $8,%r10d
	movl $14, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	xorl %eax, %eax
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __getcontext,.-__getcontext
.weak getcontext
getcontext = __getcontext
