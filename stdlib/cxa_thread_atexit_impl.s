	.text
	.p2align 4,,15
	.globl	__cxa_thread_atexit_impl
	.type	__cxa_thread_atexit_impl, @function
__cxa_thread_atexit_impl:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$32, %esi
	movl	$1, %edi
	movq	%rdx, %r12
	subq	$8, %rsp
	call	calloc@PLT
#APP
# 102 "cxa_thread_atexit_impl.c" 1
	xor %fs:48, %rbp
rol $2*8+1, %rbp
# 0 "" 2
#NO_APP
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rax, %rbx
	movq	%rbp, (%rax)
	movq	%r13, 8(%rax)
	movq	%fs:tls_dtor_list@tpoff, %rax
	movq	%rbx, %fs:tls_dtor_list@tpoff
	movq	%rax, 24(%rbx)
	je	.L2
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	testq	%r12, %r12
	jne	.L3
	movq	%fs:lm_cache@tpoff, %rax
.L4:
	lock addq	$1, 1128(%rax)
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L6
	leaq	_dl_load_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L6:
	movq	%fs:lm_cache@tpoff, %rax
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r12, %rdi
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	cmove	_dl_ns(%rip), %rax
	movq	%rax, %fs:lm_cache@tpoff
	jmp	.L4
	.size	__cxa_thread_atexit_impl, .-__cxa_thread_atexit_impl
	.p2align 4,,15
	.weak	__call_tls_dtors
	.hidden	__call_tls_dtors
	.type	__call_tls_dtors, @function
__call_tls_dtors:
	pushq	%rbx
	movq	%fs:tls_dtor_list@tpoff, %rbx
	testq	%rbx, %rbx
	je	.L15
	.p2align 4,,10
	.p2align 3
.L17:
	movq	24(%rbx), %rdx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
#APP
# 151 "cxa_thread_atexit_impl.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rdx, %fs:tls_dtor_list@tpoff
	call	*%rax
	movq	16(%rbx), %rax
	lock subq	$1, 1128(%rax)
	movq	%rbx, %rdi
	call	free@PLT
	movq	%fs:tls_dtor_list@tpoff, %rbx
	testq	%rbx, %rbx
	jne	.L17
.L15:
	popq	%rbx
	ret
	.size	__call_tls_dtors, .-__call_tls_dtors
	.section	.tbss,"awT",@nobits
	.align 8
	.type	lm_cache, @object
	.size	lm_cache, 8
lm_cache:
	.zero	8
	.align 8
	.type	tls_dtor_list, @object
	.size	tls_dtor_list, 8
tls_dtor_list:
	.zero	8
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
