	.text
	.p2align 4,,15
	.globl	__strtod_nan
	.hidden	__strtod_nan
	.type	__strtod_nan, @function
__strtod_nan:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rbx
.L2:
	movzbl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L3
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	jbe	.L3
	cmpb	$95, %cl
	je	.L3
	cmpb	%dl, %cl
	je	.L4
.L6:
	movsd	.LC0(%rip), %xmm0
.L5:
	testq	%rbp, %rbp
	je	.L1
	movq	%rbx, 0(%rbp)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L4:
	leaq	24(%rsp), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	____strtoull_l_internal
	cmpq	%rbx, 24(%rsp)
	jne	.L6
	movabsq	$2251795518717952, %rdx
	movl	%eax, %esi
	andq	%rax, %rdx
	movsd	.LC0(%rip), %xmm0
	movq	%rdx, %rcx
	movabsq	$9221120237041090560, %rdx
	orq	%rcx, %rdx
	orq	%rsi, %rdx
	movq	%rdx, %rcx
	shrq	$32, %rcx
	andl	$1048575, %ecx
	orl	%eax, %ecx
	je	.L5
	movq	%rdx, 8(%rsp)
	movsd	8(%rsp), %xmm0
	jmp	.L5
	.size	__strtod_nan, .-__strtod_nan
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	2146959360
	.hidden	____strtoull_l_internal
	.hidden	_nl_C_locobj
