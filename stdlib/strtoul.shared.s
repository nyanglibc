	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtoul_internal
	.hidden	__GI___strtoul_internal
	.type	__GI___strtoul_internal, @function
__GI___strtoul_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__GI_____strtoul_l_internal
	.size	__GI___strtoul_internal, .-__GI___strtoul_internal
	.globl	__strtoul_internal
	.set	__strtoul_internal,__GI___strtoul_internal
	.globl	__GI___strtoull_internal
	.set	__GI___strtoull_internal,__strtoul_internal
	.globl	__strtoull_internal
	.set	__strtoull_internal,__strtoul_internal
	.p2align 4,,15
	.globl	__strtoul
	.type	__strtoul, @function
__strtoul:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	__GI_____strtoul_l_internal
	.size	__strtoul, .-__strtoul
	.weak	__GI_strtoul
	.hidden	__GI_strtoul
	.set	__GI_strtoul,__strtoul
	.weak	strtoul
	.set	strtoul,__GI_strtoul
	.weak	strtoumax
	.set	strtoumax,strtoul
	.weak	strtouq
	.set	strtouq,strtoul
	.weak	strtoull
	.set	strtoull,strtoul
