	.text
	.p2align 4,,15
	.globl	__mpn_divrem
	.hidden	__mpn_divrem
	.type	__mpn_divrem, @function
__mpn_divrem:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r10
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	cmpq	$1, %r9
	je	.L3
	cmpq	$2, %r9
	je	.L4
	testq	%r9, %r9
	je	.L68
	movq	%rcx, %rax
	movq	%rdi, 48(%rsp)
	movq	%rcx, %rbx
	subq	%r9, %rax
	movq	%rsi, 40(%rsp)
	movq	%r9, 8(%rsp)
	leaq	(%rdx,%rax,8), %r14
	leaq	0(,%r9,8), %rax
	movq	%r8, 24(%rsp)
	movq	$0, 32(%rsp)
	leaq	-8(%rax), %rdi
	movq	-8(%r8,%rax), %r15
	leaq	-16(%rax), %rsi
	movq	%rax, 56(%rsp)
	movq	-16(%r8,%rax), %r12
	leaq	(%r14,%rdi), %r13
	movq	%rdi, 64(%rsp)
	movq	%rsi, 72(%rsp)
	movq	0(%r13), %rcx
	cmpq	%rcx, %r15
	jbe	.L69
.L29:
	addq	40(%rsp), %rbx
	subq	8(%rsp), %rbx
	subq	$1, %rbx
	js	.L1
	movq	%r13, %rax
	movq	%r12, %r13
	movq	%rbx, %r12
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L42:
	cmpq	%r12, 40(%rsp)
	jg	.L32
	movq	56(%rsp), %rax
	subq	$8, %r14
	movq	(%r14,%rax), %rax
	movq	%rax, 16(%rsp)
	movq	64(%rsp), %rax
	leaq	(%r14,%rax), %rbp
.L33:
	cmpq	%rcx, %r15
	movq	$-1, %rbx
	je	.L36
	movq	%rcx, %rdx
	movq	0(%rbp), %rax
	movq	72(%rsp), %rdi
#APP
# 201 "divrem.c" 1
	divq %r15
# 0 "" 2
#NO_APP
	movq	%rax, %rbx
	movq	%rdx, %rcx
	movq	%r13, %rax
#APP
# 202 "divrem.c" 1
	mulq %rbx
# 0 "" 2
#NO_APP
	movq	%rdx, %rsi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%edx, %edx
	cmpq	%r13, %rax
	setb	%dl
	subq	%r13, %rax
	subq	%rdx, %rsi
.L37:
	cmpq	%rcx, %rsi
	ja	.L40
	jne	.L36
	cmpq	%rax, (%r14,%rdi)
	jnb	.L36
.L40:
	subq	$1, %rbx
	addq	%r15, %rcx
	jnc	.L70
.L36:
	movq	8(%rsp), %rdx
	movq	24(%rsp), %rsi
	movq	%rbx, %rcx
	movq	%r14, %rdi
	call	__mpn_submul_1
	cmpq	%rax, 16(%rsp)
	je	.L41
	movq	8(%rsp), %rcx
	movq	24(%rsp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	subq	$1, %rbx
	call	__mpn_add_n
.L41:
	movq	48(%rsp), %rax
	movq	%rbx, (%rax,%r12,8)
	subq	$1, %r12
	movq	0(%rbp), %rcx
	cmpq	$-1, %r12
	jne	.L42
.L1:
	movq	32(%rsp), %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	ud2
	.p2align 4,,10
	.p2align 3
.L32:
	cmpq	$0, 8(%rsp)
	movq	0(%rbp), %rax
	movq	%rax, 16(%rsp)
	jle	.L34
	movq	56(%rsp), %rax
	addq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-8(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 8(%rax)
	cmpq	%rax, %r14
	jne	.L35
.L34:
	movq	$0, (%r14)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	-16(%rdx,%rcx,8), %r12
	movq	8(%r8), %r11
	movq	(%r8), %rbp
	movq	$0, 32(%rsp)
	movq	8(%r12), %r8
	movq	(%r12), %r9
	cmpq	%r8, %r11
	jbe	.L71
.L12:
	leaq	-3(%rsi,%rcx), %rbx
	testq	%rbx, %rbx
	js	.L14
	movq	%rbp, %r13
	movq	$-1, %r14
	negq	%r13
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L72:
	movq	-8(%r12), %rcx
	subq	$8, %r12
	cmpq	%r8, %r11
	jne	.L17
.L73:
	addq	%r11, %r9
	movq	%r9, %r8
	jnc	.L20
	movq	%r14, (%rdi,%rbx,8)
	subq	$1, %rbx
	subq	%rbp, %r8
	movq	%rcx, %r9
#APP
# 121 "divrem.c" 1
	addq %rbp,%r9
	adcq $0,%r8
# 0 "" 2
#NO_APP
	cmpq	$-1, %rbx
	je	.L14
.L28:
	cmpq	%rbx, %rsi
	jle	.L72
	xorl	%ecx, %ecx
	cmpq	%r8, %r11
	movq	$0, (%r12)
	je	.L73
.L17:
	movq	%r9, %rax
	movq	%r8, %rdx
#APP
# 130 "divrem.c" 1
	divq %r11
# 0 "" 2
#NO_APP
	movq	%rax, %r9
	movq	%rdx, %r8
	movq	%rbp, %rax
#APP
# 131 "divrem.c" 1
	mulq %r9
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L23:
	cmpq	%r8, %rdx
	ja	.L24
	jne	.L25
	cmpq	%rcx, %rax
	jbe	.L25
.L24:
	subq	$1, %r9
#APP
# 141 "divrem.c" 1
	subq %rbp,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%r11, %r8
	jnc	.L23
.L25:
	movq	%r9, (%rdi,%rbx,8)
	subq	$1, %rbx
	movq	%rcx, %r9
#APP
# 148 "divrem.c" 1
	subq %rax,%r9
	sbbq %rdx,%r8
# 0 "" 2
#NO_APP
	cmpq	$-1, %rbx
	jne	.L28
.L14:
	movq	%r8, 8(%r12)
	movq	%r9, (%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	testq	%rbp, %rbp
	movq	%rbp, %rdx
	setne	%al
	movq	$-1, %r9
	subq	%rax, %rdx
	movq	%r13, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%r8), %r8
	movq	-8(%rdx,%rcx,8), %rdx
	movq	$0, 32(%rsp)
	cmpq	%rdx, %r8
	jbe	.L74
.L6:
	subq	$2, %rcx
	js	.L7
	leaq	(%rcx,%rsi), %rax
	leaq	(%rdi,%rax,8), %r9
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%r10,%rcx,8), %rax
	subq	$1, %rcx
	subq	$8, %r9
#APP
# 73 "divrem.c" 1
	divq %r8
# 0 "" 2
#NO_APP
	movq	%rax, 8(%r9)
	cmpq	$-1, %rcx
	jne	.L8
.L7:
	subq	$1, %rsi
	js	.L9
	leaq	(%rdi,%rsi,8), %rcx
	subq	$8, %rdi
	xorl	%esi, %esi
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, %rax
	subq	$8, %rcx
#APP
# 77 "divrem.c" 1
	divq %r8
# 0 "" 2
#NO_APP
	movq	%rax, 8(%rcx)
	cmpq	%rcx, %rdi
	jne	.L10
.L9:
	movq	%rdx, (%r10)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L74:
	subq	%r8, %rdx
	movq	$1, 32(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L71:
	jb	.L48
	cmpq	%r9, %rbp
	ja	.L12
.L48:
#APP
# 97 "divrem.c" 1
	subq %rbp,%r9
	sbbq %r11,%r8
# 0 "" 2
#NO_APP
	movq	$1, 32(%rsp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L69:
	jb	.L31
	movq	8(%rsp), %rax
	movq	24(%rsp), %rsi
	movq	%r14, %rdi
	movq	%rcx, 16(%rsp)
	leaq	-1(%rax), %rdx
	call	__mpn_cmp
	testl	%eax, %eax
	movq	16(%rsp), %rcx
	js	.L29
.L31:
	movq	8(%rsp), %rcx
	movq	24(%rsp), %rdx
	movq	%r14, %rsi
	movq	%r14, %rdi
	call	__mpn_sub_n
	movq	0(%r13), %rcx
	movq	$1, 32(%rsp)
	jmp	.L29
	.size	__mpn_divrem, .-__mpn_divrem
	.hidden	__mpn_sub_n
	.hidden	__mpn_cmp
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
