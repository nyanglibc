	.text
	.p2align 4,,15
	.globl	__mpn_construct_float
	.hidden	__mpn_construct_float
	.type	__mpn_construct_float, @function
__mpn_construct_float:
	addl	$127, %esi
	movq	(%rdi), %rax
	sall	$31, %edx
	movzbl	%sil, %esi
	sall	$23, %esi
	andl	$8388607, %eax
	orl	%esi, %edx
	orl	%eax, %edx
	movl	%edx, -4(%rsp)
	movss	-4(%rsp), %xmm0
	ret
	.size	__mpn_construct_float, .-__mpn_construct_float
