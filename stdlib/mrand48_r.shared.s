	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	mrand48_r
	.type	mrand48_r, @function
mrand48_r:
	testq	%rdi, %rdi
	je	.L2
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	jmp	__jrand48_r
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$-1, %eax
	ret
	.size	mrand48_r, .-mrand48_r
	.hidden	__jrand48_r
