	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__cxa_thread_atexit_impl
	.type	__cxa_thread_atexit_impl, @function
__cxa_thread_atexit_impl:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$32, %esi
	movl	$1, %edi
	movq	%rdx, %r12
	subq	$8, %rsp
	call	calloc@PLT
#APP
# 102 "cxa_thread_atexit_impl.c" 1
	xor %fs:48, %rbp
rol $2*8+1, %rbp
# 0 "" 2
#NO_APP
	movq	%rax, %rbx
	movq	%rbp, (%rax)
	movq	%r13, 8(%rax)
	movq	tls_dtor_list@gottpoff(%rip), %rax
	movq	_rtld_global@GOTPCREL(%rip), %rbp
	movq	%fs:(%rax), %rdx
	leaq	2440(%rbp), %rdi
	movq	%rbx, %fs:(%rax)
	movq	%rdx, 24(%rbx)
	call	*3984(%rbp)
	testq	%r12, %r12
	jne	.L2
	movq	lm_cache@gottpoff(%rip), %r12
	movq	%fs:(%r12), %rax
.L3:
	lock addq	$1, 1128(%rax)
	leaq	2440(%rbp), %rdi
	call	*3992(%rbp)
	movq	%fs:(%r12), %rax
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r12, %rdi
	call	_dl_find_dso_for_object@PLT
	testq	%rax, %rax
	je	.L7
.L4:
	movq	lm_cache@gottpoff(%rip), %r12
	movq	%rax, %fs:(%r12)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L7:
	movq	0(%rbp), %rax
	jmp	.L4
	.size	__cxa_thread_atexit_impl, .-__cxa_thread_atexit_impl
	.p2align 4,,15
	.globl	__GI___call_tls_dtors
	.hidden	__GI___call_tls_dtors
	.type	__GI___call_tls_dtors, @function
__GI___call_tls_dtors:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	tls_dtor_list@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movq	24(%rbx), %rdx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
#APP
# 151 "cxa_thread_atexit_impl.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	movq	%rdx, %fs:0(%rbp)
	call	*%rax
	movq	16(%rbx), %rax
	lock subq	$1, 1128(%rax)
	movq	%rbx, %rdi
	call	free@PLT
	movq	%fs:0(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L10
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI___call_tls_dtors, .-__GI___call_tls_dtors
	.globl	__call_tls_dtors
	.set	__call_tls_dtors,__GI___call_tls_dtors
	.section	.tbss,"awT",@nobits
	.align 8
	.type	lm_cache, @object
	.size	lm_cache, 8
lm_cache:
	.zero	8
	.align 8
	.type	tls_dtor_list, @object
	.size	tls_dtor_list, 8
tls_dtor_list:
	.zero	8
