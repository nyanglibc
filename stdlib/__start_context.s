.globl __start_context
.type __start_context,@function
.align 1<<4
__start_context:
 movq %rbx, %rsp
 movq (%rsp), %rdi
 testq %rdi, %rdi
 je 2f
 call __setcontext
 movq %rax,%rdi
2:
 call exit
.Lhlt:
 hlt
.size __start_context,.-__start_context
