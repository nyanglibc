	.text
	.p2align 4,,15
	.globl	nrand48
	.type	nrand48, @function
nrand48:
	subq	$24, %rsp
	leaq	__libc_drand48_data(%rip), %rsi
	leaq	8(%rsp), %rdx
	call	__nrand48_r
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	nrand48, .-nrand48
	.hidden	__nrand48_r
	.hidden	__libc_drand48_data
