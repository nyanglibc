	.text
	.p2align 4,,15
	.globl	at_quick_exit
	.hidden	at_quick_exit
	.type	at_quick_exit, @function
at_quick_exit:
	movq	__dso_handle(%rip), %rsi
	jmp	__cxa_at_quick_exit@PLT
	.size	at_quick_exit, .-at_quick_exit
	.hidden	__dso_handle
