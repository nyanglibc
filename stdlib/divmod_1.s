	.text
	.p2align 4,,15
	.globl	__mpn_divmod_1
	.type	__mpn_divmod_1, @function
__mpn_divmod_1:
	movq	%rdx, %rax
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L1
	leaq	-1(%rax), %r8
	movq	(%rsi,%r8,8), %rdx
	cmpq	%rcx, %rdx
	jnb	.L6
	movq	$0, (%rdi,%r8,8)
	leaq	-2(%rax), %r8
.L3:
	testq	%r8, %r8
	js	.L1
	leaq	(%rdi,%r8,8), %rdi
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rsi,%r8,8), %rax
	subq	$1, %r8
	subq	$8, %rdi
#APP
# 195 "divmod_1.c" 1
	divq %rcx
# 0 "" 2
#NO_APP
	movq	%rax, 8(%rdi)
	cmpq	$-1, %r8
	jne	.L4
.L1:
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%edx, %edx
	jmp	.L3
	.size	__mpn_divmod_1, .-__mpn_divmod_1
