	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	mrand48
	.type	mrand48, @function
mrand48:
	subq	$24, %rsp
	leaq	__libc_drand48_data(%rip), %rsi
	leaq	8(%rsp), %rdx
	movq	%rsi, %rdi
	call	__jrand48_r
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	mrand48, .-mrand48
	.hidden	__jrand48_r
	.hidden	__libc_drand48_data
