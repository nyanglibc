	.text
	.p2align 4,,15
	.type	round_away, @function
round_away:
	cmpl	$1024, %r8d
	je	.L3
	jle	.L18
	cmpl	$2048, %r8d
	je	.L6
	cmpl	$3072, %r8d
	jne	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r8d, %r8d
	jne	.L2
	orl	%esi, %ecx
	movl	$0, %eax
	testb	%dl, %dl
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmovne	%ecx, %eax
	ret
.L2:
	subq	$8, %rsp
	call	abort
	.size	round_away, .-round_away
	.p2align 4,,15
	.type	round_and_return, @function
round_and_return:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %rdx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 46(%rsp)
# 0 "" 2
#NO_APP
	movzwl	46(%rsp), %eax
	andw	$3072, %ax
	cmpw	$1024, %ax
	je	.L21
	jbe	.L60
	cmpw	$2048, %ax
	je	.L24
	cmpw	$3072, %ax
	jne	.L20
	movl	$3072, %r10d
.L23:
	cmpq	$-16382, %rbx
	jge	.L26
.L62:
	cmpq	$-16446, %rbx
	jge	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	fldt	.LC0(%rip)
	movl	$34, %fs:(%rax)
	jne	.L61
.L45:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	fmul	%st(0), %st
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$-16382, %rbx
	movl	$2048, %r10d
	jl	.L62
.L26:
	cmpq	$16383, %rbx
	jle	.L63
.L37:
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	fldt	.LC2(%rip)
	movl	$34, %fs:(%rax)
	je	.L45
	fldt	.LC3(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	testw	%ax, %ax
	jne	.L20
	xorl	%r10d, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$-1, %r14
	movl	%r8d, %ecx
	movq	$-16382, %rax
	movq	%r14, %rdi
	subq	%rbx, %rax
	movq	0(%rbp), %r11
	salq	%cl, %rdi
	movq	%rdi, %rcx
	notq	%rcx
	testq	%rdx, %rcx
	setne	%cl
	movzbl	%cl, %ecx
	orl	%ecx, %r9d
	movl	%r9d, %edi
	andl	$1, %edi
	cmpq	$64, %rax
	movb	%dil, 27(%rsp)
	je	.L64
	movq	$-16383, %rcx
	subq	%rbx, %rcx
	movq	%r11, %rbx
	shrq	%cl, %rbx
	movl	%ecx, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	cmpq	$1, %rax
	je	.L32
	movq	%rbp, %rsi
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%rbp, %rdi
	movl	%r9d, 28(%rsp)
	movq	%r11, 16(%rsp)
	movl	%r10d, 8(%rsp)
	call	__mpn_rshift
	movq	0(%rbp), %r12
	movl	8(%rsp), %r10d
	movq	16(%rsp), %r11
	movl	28(%rsp), %r9d
	movl	%r12d, %esi
	andl	$1, %esi
.L31:
	andl	$1, %ebx
	jne	.L35
	cmpb	$0, 27(%rsp)
	je	.L65
.L35:
	fldt	.LC0(%rip)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	fmul	%st(0), %st
	fstp	%st(0)
	movq	$-16383, %rbx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L61:
	fldt	.LC1(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	fmulp	%st, %st(1)
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%r8d, %ecx
	movq	%r11, %rsi
	movl	%r10d, %r8d
	shrq	%cl, %rdx
	andl	$1, %esi
	movl	%r9d, %ecx
	andl	$1, %edx
	movl	%r15d, %edi
	movl	%r10d, 28(%rsp)
	movl	%r9d, 16(%rsp)
	movq	%r11, 8(%rsp)
	call	round_away
	movq	8(%rsp), %r11
	movq	%rbp, %rsi
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rdi
	cmpq	$-1, %r11
	sete	%r12b
	andl	%eax, %r12d
	call	__mpn_rshift
	testb	%r12b, %r12b
	movq	0(%rbp), %r12
	movq	8(%rsp), %r11
	movl	16(%rsp), %r9d
	movl	28(%rsp), %r10d
	movl	%r12d, %esi
	je	.L33
	andl	$1, %esi
	movq	%r14, %r11
	movq	$-16383, %rbx
.L34:
	testl	%r9d, %r9d
	movl	$1, %r14d
	jne	.L38
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
.L36:
	testq	%rax, %rax
	setne	%r14b
	movzbl	%r14b, %r9d
.L38:
	movzbl	%r13b, %edx
	movl	%r10d, %r8d
	movl	%r9d, %ecx
	movl	%r15d, %edi
	call	round_away
	testb	%al, %al
	je	.L39
	addq	$1, %r12
	movq	%r12, 0(%rbp)
	jc	.L66
.L42:
	cmpq	$-16383, %rbx
	je	.L44
.L39:
	movl	%ebx, %esi
.L43:
	testb	%r14b, %r14b
	jne	.L52
	testb	%r13b, %r13b
	jne	.L52
.L46:
	movl	%r15d, %edx
	movq	%rbp, %rdi
	call	__mpn_construct_long_double
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	fld1
	fldt	.LC0(%rip)
	faddp	%st, %st(1)
	fstp	%st(0)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L63:
	movq	0(%rbp), %r12
	movq	%rdx, %r13
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movl	%r8d, 4(%rsp)
	movq	%rdx, %r11
	andl	$1, %r13d
	movl	%r12d, %esi
	andl	$1, %esi
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$1, %rbx
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mpn_rshift
	movabsq	$-9223372036854775808, %rax
	orq	%rax, 0(%rbp)
	cmpq	$16384, %rbx
	je	.L37
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L44:
	shrq	$63, %r12
	leal	-16383(%r12), %esi
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
	jne	.L35
	movq	$-16383, %rbx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r11, %rbx
	movq	$0, 0(%rbp)
	xorl	%esi, %esi
	shrq	$63, %rbx
	xorl	%r12d, %r12d
	movl	$63, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	jmp	.L31
.L33:
	andl	$1, %esi
	jmp	.L31
.L20:
	call	abort
	.size	round_and_return, .-round_and_return
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC6:
	.string	"../stdlib/strtod_l.c"
.LC7:
	.string	"digcnt > 0"
.LC8:
	.string	"*nsize < MPNSIZE"
	.text
	.p2align 4,,15
	.type	str_to_mpn.isra.0, @function
str_to_mpn.isra.0:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testl	%esi, %esi
	movq	$0, (%rcx)
	movq	80(%rsp), %r14
	jle	.L111
	movq	%rdi, %rbx
	movl	%esi, %r15d
	movq	%rdx, %r12
	movq	%rcx, %r13
.L110:
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L68:
	movsbq	(%rbx), %rax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L77
	testq	%r14, %r14
	je	.L78
	cmpb	(%r14), %al
	je	.L112
.L78:
	addq	%r9, %rbx
	movsbq	(%rbx), %rax
.L77:
	leaq	0(%rbp,%rbp,4), %rcx
	addq	$1, %rbx
	addl	$1, %edx
	subl	$1, %r15d
	leaq	-48(%rax,%rcx,2), %rbp
	je	.L113
	cmpl	$19, %edx
	jne	.L68
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L70
	movq	%rbp, (%r12)
	movq	$1, 0(%r13)
	xorl	%ebp, %ebp
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L70:
	movabsq	$-8446744073709551616, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r9, 8(%rsp)
	movq	%r8, (%rsp)
	call	__mpn_mul_1
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	addq	(%r12), %rdx
	movq	0(%r13), %rsi
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	setc	%cl
	movq	%rdx, (%r12)
	testq	%rcx, %rcx
	jne	.L114
.L73:
	testq	%rax, %rax
	je	.L110
	movq	0(%r13), %rdx
	cmpq	$858, %rdx
	jg	.L115
	movq	%rax, (%r12,%rdx,8)
	xorl	%ebp, %ebp
	addq	$1, 0(%r13)
	xorl	%edx, %edx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jle	.L84
	movl	$19, %eax
	subl	%edx, %eax
	cltq
	cmpq	%rax, %rcx
	jle	.L116
.L84:
	movslq	%edx, %rax
	leaq	_tens_in_limb(%rip), %rdx
	movq	(%rdx,%rax,8), %rcx
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L86
.L117:
	movq	%rbp, (%r12)
	movq	$1, 0(%r13)
.L100:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movzbl	1(%r14), %eax
	testb	%al, %al
	je	.L79
	cmpb	1(%rbx), %al
	jne	.L78
	leaq	2(%rbx), %rcx
	leaq	2(%r14), %rsi
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$1, %rcx
	addq	$1, %rsi
	cmpb	%al, %dil
	jne	.L78
.L80:
	movzbl	(%rsi), %edi
	movq	%rcx, %r10
	movsbq	(%rcx), %rax
	testb	%dil, %dil
	jne	.L81
.L82:
	movq	%r10, %rbx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L114:
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%r12,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L73
.L74:
	cmpq	%rdx, %rsi
	jne	.L75
	addq	$1, %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L116:
	movslq	%edx, %rax
	movq	$0, (%r8)
	movq	0(%r13), %rdx
	leaq	_tens_in_limb(%rip), %rsi
	imulq	(%rsi,%rcx,8), %rbp
	addq	%rax, %rcx
	testq	%rdx, %rdx
	movq	(%rsi,%rcx,8), %rcx
	je	.L117
.L86:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	addq	(%r12), %rdx
	movq	0(%r13), %rsi
	setc	%cl
	movq	%rdx, (%r12)
	testq	%rcx, %rcx
	je	.L90
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movq	8(%r12,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L90
.L91:
	cmpq	%rsi, %rdx
	jne	.L92
	addq	$1, %rax
.L90:
	testq	%rax, %rax
	je	.L100
	movq	0(%r13), %rdx
	cmpq	$858, %rdx
	jg	.L118
	leaq	1(%rdx), %rcx
	movq	%rcx, 0(%r13)
	movq	%rax, (%r12,%rdx,8)
	jmp	.L100
.L79:
	leaq	1(%rbx), %r10
	movsbq	1(%rbx), %rax
	jmp	.L82
.L118:
	leaq	__PRETTY_FUNCTION__.11822(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$453, %edx
	call	__assert_fail
.L115:
	leaq	__PRETTY_FUNCTION__.11822(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$397, %edx
	call	__assert_fail
.L111:
	leaq	__PRETTY_FUNCTION__.11822(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$380, %edx
	call	__assert_fail
	.size	str_to_mpn.isra.0, .-str_to_mpn.isra.0
	.section	.rodata.str1.1
.LC13:
	.string	"decimal_len > 0"
.LC14:
	.string	"inf"
.LC15:
	.string	"inity"
.LC16:
	.string	"nan"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"dig_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC18:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_EXP - MANT_DIG) / 4"
	.align 8
.LC19:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX / 4"
	.align 8
.LC20:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_EXP - 3) / 4"
	.align 8
.LC21:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_10_EXP - MANT_DIG)"
	.align 8
.LC22:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC23:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_10_EXP - 1)"
	.section	.rodata.str1.1
.LC24:
	.string	"dig_no >= int_no"
	.section	.rodata.str1.8
	.align 8
.LC25:
	.string	"lead_zero <= (base == 16 ? (uintmax_t) INTMAX_MAX / 4 : (uintmax_t) INTMAX_MAX)"
	.align 8
.LC26:
	.string	"lead_zero <= (base == 16 ? ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN) / 4 : ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN))"
	.section	.rodata.str1.1
.LC27:
	.string	"bits != 0"
	.section	.rodata.str1.8
	.align 8
.LC28:
	.string	"int_no <= (uintmax_t) (exponent < 0 ? (INTMAX_MAX - bits + 1) / 4 : (INTMAX_MAX - exponent - bits + 1) / 4)"
	.align 8
.LC29:
	.string	"dig_no > int_no && exponent <= 0 && exponent >= MIN_10_EXP - (DIG + 2)"
	.section	.rodata.str1.1
.LC30:
	.string	"int_no > 0 && exponent == 0"
	.section	.rodata.str1.8
	.align 8
.LC31:
	.string	"int_no == 0 && *startp != L_('0')"
	.section	.rodata.str1.1
.LC32:
	.string	"need_frac_digits > 0"
.LC33:
	.string	"numsize == 1 && n < d"
.LC34:
	.string	"empty == 1"
.LC35:
	.string	"numsize == densize"
.LC36:
	.string	"cy != 0"
	.text
	.p2align 4,,15
	.globl	____strtold_l_internal
	.hidden	____strtold_l_internal
	.type	____strtold_l_internal, @function
____strtold_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r10d, %r10d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r8
	subq	$13912, %rsp
	testl	%edx, %edx
	movq	8(%rcx), %rax
	movq	%rsi, 8(%rsp)
	movq	$0, 16(%rsp)
	jne	.L629
.L120:
	movq	64(%rax), %r13
	movq	%r8, 32(%rsp)
	movq	%r10, 24(%rsp)
	movq	%r13, %rdi
	call	strlen
	testq	%rax, %rax
	movq	%rax, 40(%rsp)
	movq	24(%rsp), %r10
	movq	32(%rsp), %r8
	je	.L630
	movq	$0, 128(%rsp)
	leaq	-1(%r14), %rax
	movq	104(%r8), %rdi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L389:
	movq	%r11, %rax
.L122:
	movsbq	1(%rax), %rcx
	leaq	1(%rax), %r11
	testb	$32, 1(%rdi,%rcx,2)
	movq	%rcx, %rdx
	jne	.L389
	cmpb	$45, %cl
	je	.L631
	cmpb	$43, %cl
	movl	$0, 24(%rsp)
	je	.L632
.L124:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L391
	cmpb	%al, %dl
	jne	.L126
	movl	$1, %esi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$1, %rsi
	cmpb	%r9b, %cl
	jne	.L126
.L127:
	movzbl	0(%r13,%rsi), %ecx
	movzbl	(%r11,%rsi), %r9d
	testb	%cl, %cl
	jne	.L128
.L125:
	subl	$48, %r9d
	cmpb	$9, %r9b
	jbe	.L129
.L126:
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	jbe	.L129
	movq	112+_nl_C_locobj(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	cmpb	$105, %al
	je	.L633
	cmpb	$110, %al
	je	.L634
.L169:
	cmpq	$0, 8(%rsp)
	jne	.L635
.L138:
	fldz
.L119:
	addq	$13912, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	$48, %dl
	movq	112(%r8), %rsi
	je	.L636
	testq	%r10, %r10
	movl	$10, %r9d
	je	.L637
.L143:
	movsbq	%dl, %r12
	movzbl	(%r10), %ebp
	movq	%r11, %rbx
	cmpb	$48, %r12b
	movq	$-1, %r15
	je	.L397
	.p2align 4,,10
	.p2align 3
.L638:
	testb	%bpl, %bpl
	je	.L398
	cmpb	%bpl, (%rbx)
	jne	.L144
	movl	$1, %edx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$1, %rdx
	cmpb	%cl, -1(%rbx,%rdx)
	jne	.L144
.L148:
	movzbl	(%r10,%rdx), %ecx
	testb	%cl, %cl
	jne	.L149
	subq	$1, %rdx
.L147:
	addq	%rbx, %rdx
.L146:
	movsbq	1(%rdx), %r12
	leaq	1(%rdx), %rbx
	cmpb	$48, %r12b
	jne	.L638
.L397:
	movq	%rbx, %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L632:
	movsbq	1(%r11), %rdx
	leaq	2(%rax), %r11
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L637:
	movsbq	%dl, %r12
	movq	%r11, %rbx
	movl	$10, %r9d
	.p2align 4,,10
	.p2align 3
.L144:
	leal	-48(%r12), %edx
	cmpb	$9, %dl
	jbe	.L401
	movsbq	%r12b, %rdx
	movl	(%rsi,%rdx,4), %edi
	leal	-97(%rdi), %edx
	cmpb	$5, %dl
	ja	.L435
	cmpl	$16, %r9d
	je	.L401
.L435:
	testb	%al, %al
	je	.L152
	cmpb	(%rbx), %al
	jne	.L153
	movl	$1, %edx
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$1, %rdx
	cmpb	%cl, -1(%rbx,%rdx)
	jne	.L153
.L154:
	movzbl	0(%r13,%rdx), %ecx
	testb	%cl, %cl
	jne	.L155
.L152:
	cmpl	$16, %r9d
	jne	.L401
	cmpq	%r11, %rbx
	jne	.L401
	movq	40(%rsp), %rdi
	movsbq	(%rbx,%rdi), %rdx
	leal	-48(%rdx), %edi
	cmpb	$9, %dil
	ja	.L639
	.p2align 4,,10
	.p2align 3
.L401:
	movq	%rbx, %rbp
	xorl	%r15d, %r15d
.L150:
	movq	$-1, %rcx
.L166:
	leal	-48(%r12), %edx
	cmpb	$9, %dl
	jbe	.L159
	cmpl	$16, %r9d
	je	.L640
.L160:
	testq	%r10, %r10
	jne	.L641
.L162:
	cmpq	$0, 16(%rsp)
	je	.L167
	cmpq	%r11, %rbp
	ja	.L642
.L167:
	xorl	%esi, %esi
	testq	%r15, %r15
	sete	%sil
	negq	%rsi
	testb	%al, %al
	je	.L177
	cmpb	%al, 0(%rbp)
	jne	.L407
	movl	$1, %eax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	addq	$1, %rax
	cmpb	%dl, -1(%rbp,%rax)
	jne	.L407
.L179:
	movzbl	0(%r13,%rax), %edx
	testb	%dl, %dl
	jne	.L180
.L177:
	movq	40(%rsp), %rax
	movq	%r15, %rdx
	leaq	0(%rbp,%rax), %rcx
	movsbq	(%rcx), %r12
	movq	%rcx, %rbp
	subq	%rcx, %rdx
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L643:
	movq	112(%r8), %rdi
	movsbq	%r12b, %rax
	movl	(%rdi,%rax,4), %eax
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L178
.L182:
	cmpb	$48, %r12b
	je	.L181
	cmpq	$-1, %rsi
	movq	%rbp, %rax
	sete	%dil
	subq	%rcx, %rax
	testb	%dil, %dil
	cmovne	%rax, %rsi
.L181:
	addq	$1, %rbp
	movsbq	0(%rbp), %r12
.L373:
	leal	-48(%r12), %eax
	leaq	(%rdx,%rbp), %r14
	cmpb	$9, %al
	jbe	.L182
	cmpl	$16, %r9d
	je	.L643
.L178:
	testq	%r14, %r14
	js	.L644
	movq	112(%r8), %rax
	cmpl	$16, %r9d
	movl	(%rax,%r12,4), %eax
	jne	.L436
	cmpb	$112, %al
	jne	.L436
.L184:
	movsbl	1(%rbp), %ecx
	cmpb	$45, %cl
	je	.L645
	cmpb	$43, %cl
	je	.L646
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L411
	cmpl	$16, %r9d
	leaq	1(%rbp), %rdi
	je	.L191
.L192:
	testq	%r15, %r15
	je	.L200
	testq	%rsi, %rsi
	jne	.L438
	testq	%r15, %r15
	js	.L438
	movl	$4933, %r11d
	subq	%r15, %r11
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L640:
	movsbq	%r12b, %rdx
	movl	(%rsi,%rdx,4), %edx
	subl	$97, %edx
	cmpb	$5, %dl
	ja	.L160
	.p2align 4,,10
	.p2align 3
.L159:
	addq	$1, %r15
	movq	%rbp, %rdx
.L161:
	leaq	1(%rdx), %rbp
	movsbq	1(%rdx), %r12
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L631:
	movsbq	1(%r11), %rdx
	movl	$1, 24(%rsp)
	leaq	2(%rax), %r11
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L436:
	cmpl	$16, %r9d
	je	.L411
	cmpb	$101, %al
	je	.L184
.L411:
	movq	%rbp, %rdi
.L186:
	cmpq	%r15, %r14
	jbe	.L217
	cmpb	$48, -1(%rbp)
	jne	.L176
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L218:
	subq	$1, %rax
	leaq	(%rax,%r14), %rdx
	subq	%rbp, %rdx
	cmpb	$48, -1(%rax)
	je	.L218
	cmpq	%rdx, %r15
	ja	.L647
	movq	%rdx, %r14
	movq	%rax, %rbp
.L217:
	cmpq	%r15, %r14
	jne	.L176
	testq	%r14, %r14
	je	.L176
	movq	128(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L648
	cmpq	$0, 8(%rsp)
	je	.L227
.L376:
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
.L226:
	testq	%r14, %r14
	jne	.L227
.L157:
	movl	24(%rsp), %r14d
	testl	%r14d, %r14d
	je	.L138
	fldz
	fchs
	jmp	.L119
.L667:
	testb	%dl, %dl
	movq	16(%rsp), %rsi
	je	.L176
.L619:
	movq	%rcx, 128(%rsp)
	.p2align 4,,10
	.p2align 3
.L176:
	cmpq	$0, 8(%rsp)
	jne	.L376
	jmp	.L226
.L635:
	movq	8(%rsp), %rax
	fldz
	movq	%r14, (%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L407:
	movq	%r15, %r14
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L636:
	movsbq	1(%r11), %rdi
	cmpl	$120, (%rsi,%rdi,4)
	movq	%rdi, %r12
	je	.L140
	testq	%r10, %r10
	movl	$10, %r9d
	jne	.L143
.L141:
	movq	%r11, %rbx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L649:
	movsbq	1(%rbx), %r12
.L145:
	addq	$1, %rbx
	cmpb	$48, %r12b
	je	.L649
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L227:
	testq	%rsi, %rsi
	je	.L228
	movzbl	0(%r13), %ecx
	movzbl	(%rbx), %eax
	movzbl	1(%r13), %edi
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L229:
	movzbl	1(%rbx), %eax
.L231:
	addq	$1, %rbx
.L234:
	cmpb	%al, %cl
	jne	.L229
	testb	%dil, %dil
	je	.L230
	movzbl	1(%rbx), %eax
	cmpb	%dil, %al
	jne	.L231
	movl	$2, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	addq	$1, %rax
	cmpb	%dl, -1(%rbx,%rax)
	jne	.L416
.L232:
	movzbl	0(%r13,%rax), %edx
	testb	%dl, %dl
	jne	.L233
.L230:
	cmpl	$16, %r9d
	je	.L650
	testq	%rsi, %rsi
	movq	%rsi, %rcx
	js	.L379
	movq	128(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L236
.L237:
	movq	40(%rsp), %rdi
	subq	%rcx, %rax
	subq	%rsi, %r14
	movq	%rax, 128(%rsp)
	leaq	(%rdi,%rsi), %rdx
	addq	%rdx, %rbx
.L228:
	cmpl	$16, %r9d
	je	.L651
	movq	128(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L652
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
.L261:
	addq	%rax, %r15
	subq	%rax, %rcx
	movl	$4933, %eax
	subq	%r15, %rax
	movq	%rcx, 128(%rsp)
	cmpq	%rax, %rcx
	jg	.L623
	cmpq	$-4951, %rcx
	jl	.L653
	testq	%r15, %r15
	jne	.L266
	testq	%r14, %r14
	je	.L269
	leaq	4951(%rcx), %rax
	cmpq	$4951, %rax
	ja	.L269
	cmpb	$48, (%rbx)
	je	.L291
	movl	$1, %eax
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movl	$16447, %esi
	shrq	%rdx
	leal	65(%rdx), %eax
	movl	%ecx, %edx
	cmpl	$16447, %eax
	cmovg	%esi, %eax
	addl	%eax, %ecx
	testl	%ecx, %ecx
	jle	.L654
	leaq	128(%rsp), %rax
	movq	$0, 56(%rsp)
	xorl	%ebp, %ebp
	movq	%rax, 104(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 96(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 8(%rsp)
.L293:
	movslq	%ecx, %rax
	movq	%r14, %rsi
	movl	%r14d, %ecx
	subl	%r15d, %ecx
	subq	%r15, %rsi
	movl	$0, 92(%rsp)
	cmpq	%rsi, %rax
	movslq	%ecx, %rcx
	cmovg	%rcx, %rax
	addq	%r15, %rax
	cmpq	%rax, %r14
	jle	.L295
	movq	%rax, %r14
	movl	$1, 92(%rsp)
.L295:
	movl	%r14d, %eax
	xorl	%r14d, %r14d
	movl	%ebp, 72(%rsp)
	subl	%r15d, %eax
	movq	%rbx, 80(%rsp)
	leaq	_fpioconst_pow10(%rip), %r13
	movl	%eax, %r15d
	movl	%eax, 48(%rsp)
	movq	8(%rsp), %rax
	subl	%edx, %r15d
	movl	$1, %r12d
	movq	%r10, 64(%rsp)
	movq	%r14, %rbx
	movq	%rax, 32(%rsp)
	leaq	7024(%rsp), %rax
	movq	%rax, 16(%rsp)
	movq	%rax, %rbp
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L656:
	leaq	0(,%r14,8), %rdx
	movq	%rbp, %rdi
	movq	%r14, %rbx
	call	memcpy@PLT
.L296:
	addl	%r12d, %r12d
	addq	$24, %r13
	testl	%r15d, %r15d
	je	.L655
.L298:
	testl	%r15d, %r12d
	je	.L296
	movq	8(%r13), %rax
	leaq	__tens(%rip), %rdi
	xorl	%r12d, %r15d
	testq	%rbx, %rbx
	leaq	-1(%rax), %r14
	movq	0(%r13), %rax
	leaq	8(%rdi,%rax,8), %rsi
	je	.L656
	movq	%r14, %rdx
	movq	32(%rsp), %r14
	movq	%rbx, %r8
	movq	%rbp, %rcx
	movq	%r14, %rdi
	call	__mpn_mul
	movq	8(%r13), %rdx
	testq	%rax, %rax
	leaq	-1(%rbx,%rdx), %rbx
	jne	.L426
	movq	%rbp, %rax
	subq	$1, %rbx
	movq	%r14, %rbp
	movq	%rax, 32(%rsp)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L646:
	movsbl	2(%rbp), %ecx
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L411
	cmpl	$16, %r9d
	leaq	2(%rbp), %rdi
	jne	.L192
.L191:
	testq	%r15, %r15
	je	.L657
	testq	%rsi, %rsi
	jne	.L437
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L437
	movl	$4096, %eax
	subq	%r15, %rax
	leaq	3(,%rax,4), %r11
.L197:
	testq	%r11, %r11
	movl	$0, %eax
	cmovs	%rax, %r11
.L616:
	movq	%r11, %rax
	movabsq	$-3689348814741910323, %rdx
	movl	$0, 16(%rsp)
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r11
	movq	%r11, 32(%rsp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L416:
	movl	%edi, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L645:
	movsbl	2(%rbp), %ecx
	leaq	2(%rbp), %rdi
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L411
	cmpl	$16, %r9d
	je	.L658
	movabsq	$9223372036854770812, %rax
	cmpq	%rax, %r15
	ja	.L659
	leaq	4995(%r15), %r11
.L617:
	movq	%r11, %rax
	movabsq	$-3689348814741910323, %rdx
	movl	$1, 16(%rsp)
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r11
	movq	%r11, 32(%rsp)
.L194:
	movq	128(%rsp), %rax
	xorl	%r12d, %r12d
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L204:
	subl	$48, %ecx
	cmpq	%rdx, %rax
	movslq	%ecx, %rcx
	je	.L660
.L206:
	leaq	(%rax,%rax,4), %rax
	addq	$1, %rdi
	movl	$1, %r12d
	leaq	(%rcx,%rax,2), %rax
	movsbl	(%rdi), %ecx
	leal	-48(%rcx), %r11d
	cmpb	$9, %r11b
	ja	.L661
.L208:
	cmpq	%rdx, %rax
	jle	.L204
.L207:
	testb	%r12b, %r12b
	jne	.L662
.L205:
	cmpq	$-1, %rsi
	je	.L663
	movl	16(%rsp), %r12d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	jne	.L664
	movl	24(%rsp), %ebx
	fldt	.LC2(%rip)
	testl	%ebx, %ebx
	je	.L214
	fldt	.LC3(%rip)
	fmulp	%st, %st(1)
	.p2align 4,,10
	.p2align 3
.L215:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L215
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L119
	movq	%rdi, (%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L660:
	cmpq	%rcx, 32(%rsp)
	jge	.L206
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L153:
	cmpl	$16, %r9d
	je	.L665
	xorl	%r15d, %r15d
	cmpb	$101, %dil
	movq	%rbx, %rbp
	je	.L150
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L629:
	movq	80(%rax), %rdi
	movq	%rdi, 16(%rsp)
	movzbl	(%rdi), %edi
	leal	-1(%rdi), %edx
	movb	%dil, 24(%rsp)
	cmpb	$125, %dl
	ja	.L387
	movq	72(%rax), %r10
	cmpb	$0, (%r10)
	jne	.L120
	movq	$0, 16(%rsp)
	xorl	%r10d, %r10d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L633:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$3, %edx
	movq	%r11, %rdi
	movq	%r11, 16(%rsp)
	call	__strncasecmp_l
	testl	%eax, %eax
	jne	.L169
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.L132
	movq	16(%rsp), %r11
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	leaq	3(%r11), %rbx
	movq	%r11, 8(%rsp)
	movq	%rbx, %rdi
	call	__strncasecmp_l
	movq	8(%rsp), %r11
	addq	$8, %r11
	testl	%eax, %eax
	cmove	%r11, %rbx
	movq	%rbx, (%r15)
.L132:
	movl	24(%rsp), %eax
	testl	%eax, %eax
	je	.L392
	flds	.LC12(%rip)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L634:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	$3, %edx
	movq	%r11, %rdi
	movq	%r11, 16(%rsp)
	call	__strncasecmp_l
	testl	%eax, %eax
	jne	.L169
	movq	16(%rsp), %r11
	cmpb	$40, 3(%r11)
	leaq	3(%r11), %rbx
	je	.L666
	flds	.LC5(%rip)
.L135:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L137
	movq	%rbx, (%rax)
.L137:
	movl	24(%rsp), %r15d
	testl	%r15d, %r15d
	je	.L119
	fchs
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L391:
	movl	%edx, %r9d
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L648:
	xorl	%eax, %eax
	cmpl	$16, %r9d
	movq	%rsi, 16(%rsp)
	sete	%al
	subq	$1, %rbp
	xorl	%edx, %edx
	leaq	1(%rax,%rax,2), %r12
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movsbl	%al, %r11d
	subl	$48, %r11d
	cmpl	$9, %r11d
	seta	%r11b
.L222:
	testb	%r11b, %r11b
	jne	.L223
	cmpb	$48, %al
	jne	.L667
	addq	%r12, %rcx
	subq	$1, %r15
	subq	$1, %r14
	movq	%rcx, %rax
	setne	%dl
	shrq	$63, %rax
	andb	%al, %dl
	je	.L668
.L223:
	subq	$1, %rbp
.L220:
	cmpl	$16, %r9d
	movzbl	0(%rbp), %eax
	jne	.L221
	movq	104(%r8), %rsi
	movsbq	%al, %r11
	movzwl	(%rsi,%r11,2), %r11d
	shrw	$12, %r11w
	xorl	$1, %r11d
	andl	$1, %r11d
	jmp	.L222
.L651:
	movsbq	(%rbx), %rax
	movq	104(%r8), %r9
	testb	$16, 1(%r9,%rax,2)
	movq	%rax, %rdx
	jne	.L620
	.p2align 4,,10
	.p2align 3
.L240:
	addq	$1, %rbx
	movsbq	(%rbx), %rax
	testb	$16, 1(%r9,%rax,2)
	movq	%rax, %rdx
	je	.L240
	jmp	.L620
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
.L620:
	cmpb	$48, %dl
	je	.L242
	movsbl	%dl, %r10d
	leaq	1(%rbx), %rax
	subl	$48, %r10d
	cmpl	$9, %r10d
	ja	.L243
.L621:
	leaq	nbits.11960(%rip), %rdx
	movslq	%r10d, %r10
	movl	(%rdx,%r10,4), %edx
	testl	%edx, %edx
	je	.L669
	movq	128(%rsp), %rsi
	movl	$64, %ecx
	movl	$63, %edi
	subl	%edx, %ecx
	subl	%edx, %edi
	movslq	%edx, %r11
	salq	%cl, %r10
	testq	%rsi, %rsi
	movq	%r10, 136(%rsp)
	js	.L670
	movabsq	$9223372036854775807, %rcx
	subq	%rsi, %rcx
	subq	%r11, %rcx
	leaq	4(%rcx), %r11
	addq	$1, %rcx
	cmovs	%r11, %rcx
	sarq	$2, %rcx
.L247:
	cmpq	%r15, %rcx
	jb	.L671
	subl	$1, %edx
	movslq	%edx, %rdx
	leaq	-4(%rdx,%r15,4), %rdx
	addq	%rdx, %rsi
	subq	$1, %r14
	movq	%rsi, 128(%rsp)
	je	.L249
	movq	%r10, %rbx
	xorl	%ebp, %ebp
	movq	40(%rsp), %r12
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L673:
	leal	-3(%rdi), %ecx
	subl	$4, %edi
	movq	%r11, %rax
	movl	$1, %ebp
	salq	%cl, %rdx
	orq	%rdx, %rbx
	testq	%r14, %r14
	je	.L672
.L254:
	movsbq	(%rax), %rdx
	testb	$16, 1(%r9,%rdx,2)
	movq	%rdx, %rcx
	jne	.L250
	addq	%r12, %rax
	movsbq	(%rax), %rcx
.L250:
	movsbl	%cl, %edx
	leaq	1(%rax), %r11
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L622
	movq	112(%r8), %rdx
	movl	(%rdx,%rcx,4), %edx
	subl	$87, %edx
.L622:
	subq	$1, %r14
	cmpl	$2, %edi
	movslq	%edx, %rdx
	jg	.L673
	testb	%bpl, %bpl
	movl	$3, %ecx
	cmovne	%rbx, %r10
	subl	%edi, %ecx
	movq	%rdx, %rbx
	shrq	%cl, %rbx
	leal	61(%rdi), %ecx
	orq	%rbx, %r10
	salq	%cl, %rdx
	testq	%r14, %r14
	movq	%r10, 136(%rsp)
	movq	%rdx, %rcx
	je	.L418
	cmpb	$48, 1(%rax)
	jne	.L420
	addq	$2, %rax
	addq	%r11, %r14
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	addq	$1, %rax
	cmpb	$48, -1(%rax)
	jne	.L420
.L258:
	cmpq	%r14, %rax
	jne	.L259
.L418:
	xorl	%r9d, %r9d
.L257:
	movl	24(%rsp), %edx
	leaq	136(%rsp), %rdi
	movl	$63, %r8d
	call	round_and_return
	jmp	.L119
.L140:
	testq	%r10, %r10
	leaq	2(%r11), %rbx
	movzbl	2(%r11), %edx
	jne	.L395
	cmpb	$48, %dl
	jne	.L396
	movsbq	3(%r11), %r12
	movq	$0, 16(%rsp)
	movq	%rbx, %r11
	movl	$16, %r9d
	jmp	.L141
.L641:
	movzbl	(%r10), %edx
	testb	%dl, %dl
	je	.L404
	cmpb	%dl, 0(%rbp)
	jne	.L162
	movl	$1, %edx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	addq	$1, %rdx
	cmpb	%dil, -1(%rbp,%rdx)
	jne	.L162
.L164:
	movzbl	(%r10,%rdx), %edi
	testb	%dil, %dil
	jne	.L165
	subq	$1, %rdx
.L163:
	addq	%rbp, %rdx
	jmp	.L161
.L661:
	movl	16(%rsp), %r11d
	movq	%rax, %rdx
	negq	%rdx
	testl	%r11d, %r11d
	cmovne	%rdx, %rax
	movq	%rax, 128(%rsp)
	jmp	.L186
.L387:
	movq	$0, 16(%rsp)
	jmp	.L120
.L664:
	movl	24(%rsp), %ebp
	fldt	.LC0(%rip)
	testl	%ebp, %ebp
	je	.L214
	fldt	.LC1(%rip)
	fmulp	%st, %st(1)
	jmp	.L215
.L243:
	movq	112(%r8), %rcx
	movl	(%rcx,%rdx,4), %r10d
	subl	$87, %r10d
	jmp	.L621
.L200:
	cmpq	$-1, %rsi
	je	.L413
	movabsq	$9223372036854770874, %rax
	cmpq	%rax, %rsi
	ja	.L674
	leaq	4933(%rsi), %r11
	jmp	.L616
.L639:
	movl	(%rsi,%rdx,4), %edx
	subl	$97, %edx
	cmpb	$5, %dl
	jbe	.L401
.L156:
	movq	16(%rsp), %rcx
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	movl	%r9d, 32(%rsp)
	movq	%r11, 16(%rsp)
	call	__correctly_grouped_prefixmb
	cmpq	$0, 8(%rsp)
	je	.L157
	movq	16(%rsp), %r11
	movl	32(%rsp), %r9d
	cmpq	%rax, %r11
	je	.L675
.L158:
	movq	8(%rsp), %rdi
.L627:
	movq	%rax, (%rdi)
	jmp	.L157
.L670:
	movabsq	$-9223372036854775808, %rcx
	subq	%r11, %rcx
	sarq	$2, %rcx
	jmp	.L247
.L658:
	movabsq	$2305843009213689840, %rax
	cmpq	%rax, %r15
	ja	.L676
	leaq	16445(,%r15,4), %r11
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L655:
	movq	%rbp, %r9
	cmpq	8(%rsp), %r9
	movq	%rbx, %r14
	movq	64(%rsp), %r10
	movl	72(%rsp), %ebp
	movq	80(%rsp), %rbx
	movl	%r15d, 88(%rsp)
	je	.L677
.L299:
	subq	$8, %rsp
	movq	%rbx, %rdi
	pushq	%r10
	movq	56(%rsp), %r9
	movq	120(%rsp), %r8
	movq	112(%rsp), %rcx
	movq	24(%rsp), %rdx
	movl	64(%rsp), %esi
	call	str_to_mpn.isra.0
	leaq	-1(%r14), %rax
	bsrq	7040(%rsp,%rax,8), %rbx
	movq	%rax, 64(%rsp)
	popq	%rcx
	popq	%rsi
	xorq	$63, %rbx
	testl	%ebx, %ebx
	jne	.L300
.L624:
	movq	120(%rsp), %rdx
.L301:
	movq	56(%rsp), %rax
	cmpq	$1, %r14
	movq	%rax, 128(%rsp)
	je	.L304
	cmpq	$2, %r14
	jne	.L678
	cmpq	$1, %rdx
	movq	7024(%rsp), %rdi
	movq	7032(%rsp), %rsi
	movq	144(%rsp), %r12
	jg	.L312
	cmpq	%r12, %rsi
	jbe	.L427
	testl	%ebp, %ebp
	je	.L679
	movl	$64, %eax
	leaq	136(%rsp), %rdi
	subl	%ebp, %eax
	movq	%rdi, 56(%rsp)
	movl	%eax, %ebp
	je	.L316
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	144(%rsp), %r12
.L316:
	movq	%r12, %rbx
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
.L317:
	orq	%r13, %rbx
	movq	128(%rsp), %rax
	movl	$63, %r8d
	setne	%r9b
	orb	92(%rsp), %r9b
	subl	%ebp, %r8d
	leaq	-1(%rax), %rsi
	andl	$1, %r9d
.L626:
	movl	24(%rsp), %edx
	movq	56(%rsp), %rdi
	movslq	%r8d, %r8
	movq	%r12, %rcx
	call	round_and_return
	jmp	.L119
.L426:
	movq	%rbp, %rax
	movq	32(%rsp), %rbp
	movq	%rax, 32(%rsp)
	jmp	.L296
.L663:
	movl	24(%rsp), %r13d
	fldz
	testl	%r13d, %r13d
	je	.L215
	fchs
	jmp	.L215
.L214:
	fmul	%st(0), %st
	jmp	.L215
.L652:
	movq	%r15, %rax
	negq	%rax
	cmpq	%rcx, %rax
	cmovl	%rcx, %rax
	jmp	.L261
.L300:
	movq	16(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r14, %rdx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	8(%rsp), %rdi
	movq	120(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L624
	movq	120(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, 144(%rsp,%rcx,8)
	movq	%rdx, 120(%rsp)
	jmp	.L301
.L672:
	movq	%rbx, 136(%rsp)
.L249:
	movl	24(%rsp), %edx
	leaq	136(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L119
.L266:
	leaq	128(%rsp), %r8
	leaq	144(%rsp), %rdx
	leaq	120(%rsp), %rcx
	subq	$8, %rsp
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r8, 112(%rsp)
	movq	%rcx, 104(%rsp)
	movq	%rdx, 16(%rsp)
	pushq	%r10
	movq	56(%rsp), %r9
	movq	%r10, 32(%rsp)
	call	str_to_mpn.isra.0
	movq	144(%rsp), %rdx
	movq	%rax, %rbx
	popq	%rdi
	popq	%r8
	testq	%rdx, %rdx
	movq	120(%rsp), %r12
	movq	16(%rsp), %r10
	jle	.L271
	leaq	7024(%rsp), %rax
	movl	$1, %r9d
	leaq	_fpioconst_pow10(%rip), %rbp
	movq	%r12, %rdi
	movl	%r9d, %r13d
	movq	%rbx, %r12
	movq	%rax, 16(%rsp)
	movq	%rax, %r11
	movq	8(%rsp), %rax
	movq	%rax, 32(%rsp)
.L272:
	movslq	%r13d, %rax
	testq	%rdx, %rax
	je	.L273
.L681:
	movq	8(%rbp), %rsi
	xorq	%rdx, %rax
	movq	%r10, 56(%rsp)
	movq	%rax, 128(%rsp)
	movq	0(%rbp), %rax
	leaq	-1(%rsi), %rbx
	leaq	__tens(%rip), %rsi
	cmpq	%rdi, %rbx
	leaq	8(%rsi,%rax,8), %rsi
	jg	.L274
	movq	%rsi, %rcx
	movq	32(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%rbx, %r8
	movq	%r11, %rdi
	movq	%r11, 48(%rsp)
	call	__mpn_mul
	movq	48(%rsp), %r11
	movq	56(%rsp), %r10
.L275:
	movq	120(%rsp), %rdi
	movq	128(%rsp), %rdx
	addq	%rbx, %rdi
	testq	%rax, %rax
	movq	%rdi, 120(%rsp)
	jne	.L276
	subq	$1, %rdi
	movq	%rdi, 120(%rsp)
.L276:
	addl	%r13d, %r13d
	addq	$24, %rbp
	testq	%rdx, %rdx
	je	.L680
	movq	32(%rsp), %rax
	movq	%r11, 32(%rsp)
	movq	%rax, %r11
	movslq	%r13d, %rax
	testq	%rdx, %rax
	jne	.L681
.L273:
	addl	%r13d, %r13d
	addq	$24, %rbp
	jmp	.L272
.L274:
	movq	32(%rsp), %rcx
	movq	%rdi, %r8
	movq	%rbx, %rdx
	movq	%r11, %rdi
	movq	%r11, 48(%rsp)
	call	__mpn_mul
	movq	56(%rsp), %r10
	movq	48(%rsp), %r11
	jmp	.L275
.L680:
	cmpq	16(%rsp), %r11
	movq	%r12, %rbx
	movq	%rdi, %r12
	jne	.L271
	leaq	0(,%rdi,8), %rdx
	movq	8(%rsp), %rdi
	movq	%r11, %rsi
	movq	%r10, 16(%rsp)
	call	memcpy@PLT
	movq	16(%rsp), %r10
.L271:
	leaq	-1(%r12), %rdx
	movl	%r12d, %r13d
	sall	$6, %r13d
	bsrq	144(%rsp,%rdx,8), %rax
	xorq	$63, %rax
	subl	%eax, %r13d
	cmpl	$16384, %r13d
	movl	%r13d, %ebp
	jg	.L623
	cmpl	$64, %r13d
	jle	.L280
	leal	-64(%r13), %ecx
	movl	%ecx, %eax
	sarl	$6, %eax
	andl	$63, %ecx
	movslq	%eax, %rsi
	movq	144(%rsp,%rsi,8), %r10
	jne	.L281
	subq	$1, %rsi
	movq	%r10, 136(%rsp)
	movl	$63, %r8d
	movq	144(%rsp,%rsi,8), %r10
.L282:
	cmpq	$0, 144(%rsp)
	jne	.L422
	movq	8(%rsp), %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L284:
	movslq	%eax, %rdx
	addq	$1, %rax
	cmpq	$0, -8(%rcx,%rax,8)
	je	.L284
.L283:
	cmpq	%r15, %r14
	movl	$1, %r9d
	ja	.L285
	xorl	%r9d, %r9d
	cmpq	%rsi, %rdx
	setl	%r9b
.L285:
	leal	-1(%r13), %esi
	movl	24(%rsp), %edx
	leaq	136(%rsp), %rdi
	movq	%r10, %rcx
	movslq	%esi, %rsi
	call	round_and_return
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L650:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rsi
	ja	.L379
	movq	128(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	cmpq	%rsi, %rdx
	jb	.L236
	leaq	0(,%rsi,4), %rcx
	jmp	.L237
.L668:
	movq	16(%rsp), %rsi
	jmp	.L619
.L398:
	movq	%r15, %rdx
	jmp	.L147
.L665:
	cmpb	$112, %dil
	jne	.L156
	cmpq	%r11, %rbx
	jne	.L401
	jmp	.L156
.L678:
	movq	48(%rsp), %rax
	movq	16(%rsp), %rdi
	movq	7024(%rsp,%rax,8), %r13
	leaq	-2(%r14), %rax
	movq	%rax, 96(%rsp)
	movq	7024(%rsp,%rax,8), %r15
	movq	%r14, %rax
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rsi
	movq	8(%rsp), %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	movq	120(%rsp), %rdx
	js	.L334
	movq	120(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	$0, 144(%rsp,%rax,8)
	movq	%rdx, 120(%rsp)
.L334:
	cmpq	%rdx, %r14
	jle	.L335
	movq	%r14, %rbx
	subq	%rdx, %rbx
	movq	%rbx, %rax
	salq	$6, %rax
	testl	%ebp, %ebp
	je	.L682
	addq	56(%rsp), %rax
	cmpq	$64, %rax
	jne	.L338
	cmpq	$1, %rbx
	jne	.L683
	leaq	136(%rsp), %rax
	movq	$0, 136(%rsp)
	movq	%rax, 56(%rsp)
.L340:
	movl	%ebx, %eax
	sall	$6, %eax
	addl	%eax, %ebp
.L337:
	testl	%edx, %edx
	movslq	%edx, %rcx
	jle	.L344
	subl	$1, %edx
	movq	8(%rsp), %rax
	addq	%rbx, %rcx
	movslq	%edx, %rdi
	movl	%edx, %edx
	leaq	0(,%rdi,8), %rsi
	salq	$3, %rdx
	subq	%rdi, %rcx
	addq	%rsi, %rax
	addq	56(%rsp), %rsi
	subq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L343:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L343
.L344:
	movq	8(%rsp), %rax
	movq	%rax, %rdx
	addq	$8, %rax
	leaq	(%rax,%rbx,8), %rcx
	jmp	.L342
.L684:
	addq	$8, %rax
.L342:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L684
	cmpl	$64, %ebp
	movq	$0, 7024(%rsp,%r14,8)
	movq	144(%rsp,%r14,8), %rbx
	jg	.L430
.L348:
	leaq	1(%r14), %rax
	movq	8(%rsp), %rdi
	movq	%rax, 72(%rsp)
	leal	-2(%r14), %eax
	movslq	%eax, %rcx
	movl	%eax, %eax
	leaq	0(,%rcx,8), %rdx
	salq	$3, %rax
	addq	%rdx, %rdi
	movq	%rdi, 64(%rsp)
	movq	56(%rsp), %rdi
	leaq	(%rdi,%rdx), %r8
	subq	%rax, %r8
	leal	-1(%r14), %eax
	movq	%r8, %rdi
	movl	%ebp, %r8d
	movslq	%eax, %r9
	movl	%eax, 80(%rsp)
	movq	%rdi, %rbp
	subq	%rcx, %r9
	movq	%r9, %rax
	movq	%r14, %r9
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L352:
	cmpq	%rbx, %r13
	movq	$-1, %r12
	je	.L353
	movq	48(%rsp), %rax
	movq	%rbx, %rdx
	movq	96(%rsp), %rsi
	movq	144(%rsp,%rax,8), %rax
	movq	%rax, 32(%rsp)
#APP
# 1727 "../stdlib/strtod_l.c" 1
	divq %r13
# 0 "" 2
#NO_APP
	movq	%rax, %r12
	movq	%rdx, %rbx
	movq	%r15, %rax
#APP
# 1728 "../stdlib/strtod_l.c" 1
	mulq %r12
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L685:
	xorl	%edx, %edx
	cmpq	%r15, %rax
	setb	%dl
	subq	%r15, %rax
	subq	%rdx, %rcx
.L354:
	cmpq	%rbx, %rcx
	ja	.L357
	jne	.L353
	cmpq	%rax, 144(%rsp,%rsi,8)
	jnb	.L353
.L357:
	subq	$1, %r12
	addq	%r13, %rbx
	jnc	.L685
.L353:
	movq	8(%rsp), %rbx
	movq	72(%rsp), %rdx
	movq	%r12, %rcx
	movq	16(%rsp), %rsi
	movq	%r9, 40(%rsp)
	movl	%r8d, 32(%rsp)
	movq	%rbx, %rdi
	call	__mpn_submul_1
	movq	40(%rsp), %r9
	movl	32(%rsp), %r8d
	cmpq	%rax, 144(%rsp,%r9,8)
	je	.L358
	movq	16(%rsp), %rdx
	movq	%r9, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	movl	%r8d, 40(%rsp)
	movq	%r9, 32(%rsp)
	call	__mpn_add_n
	testq	%rax, %rax
	movq	32(%rsp), %r9
	movl	40(%rsp), %r8d
	je	.L686
	subq	$1, %r12
.L358:
	movq	48(%rsp), %rax
	movl	80(%rsp), %edx
	movq	144(%rsp,%rax,8), %rbx
	testl	%edx, %edx
	movq	64(%rsp), %rax
	movq	%rbx, 144(%rsp,%r9,8)
	jle	.L363
	.p2align 4,,10
	.p2align 3
.L360:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%r14,8)
	subq	$8, %rax
	cmpq	%rax, %rbp
	jne	.L360
.L363:
	testl	%r8d, %r8d
	movq	$0, 144(%rsp)
	jne	.L687
	testq	%r12, %r12
	jne	.L364
	subq	$64, 128(%rsp)
	movq	$0, 136(%rsp)
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L687:
	testl	%r8d, %r8d
	leal	64(%r8), %r10d
	jle	.L688
	movl	$64, %r11d
	movl	%r11d, %eax
	movl	%r11d, 32(%rsp)
	subl	%r8d, %eax
	movl	%eax, 88(%rsp)
	je	.L367
	movq	56(%rsp), %rdi
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%r9, 104(%rsp)
	movl	%r10d, 40(%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	32(%rsp), %r11d
	movq	%r12, %rax
	movq	104(%rsp), %r9
	movl	40(%rsp), %r10d
	movl	%r11d, %ecx
	subl	88(%rsp), %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
.L367:
	cmpl	$64, %r10d
	jg	.L689
	movl	%r10d, %r8d
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L364:
	bsrq	%r12, %rax
	movl	$64, %r8d
	movq	%r12, 136(%rsp)
	xorq	$63, %rax
	subl	%eax, %r8d
	cltq
	subq	%rax, 128(%rsp)
	jmp	.L352
.L688:
	movq	%r12, 136(%rsp)
	jmp	.L367
.L689:
	movq	%r9, %r14
.L351:
	testl	%r14d, %r14d
	movl	%r14d, %edx
	js	.L368
	movslq	%r14d, %rax
	cmpq	$0, 144(%rsp,%rax,8)
	jne	.L368
	leal	-1(%r14), %eax
	movl	%r14d, %r14d
	movq	8(%rsp), %rsi
	cltq
	movq	%rax, %rcx
	subq	%r14, %rcx
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L690:
	subq	$1, %rax
	cmpq	$0, 8(%rsi,%rax,8)
	jne	.L368
.L369:
	cmpq	%rax, %rcx
	movl	%eax, %edx
	jne	.L690
.L368:
	movq	128(%rsp), %rax
	notl	%edx
	movl	$63, %r8d
	shrl	$31, %edx
	subl	88(%rsp), %r8d
	movl	%edx, %r9d
	orl	92(%rsp), %r9d
	leaq	-1(%rax), %rsi
	jmp	.L626
.L304:
	movq	144(%rsp), %rcx
	movq	7024(%rsp), %rsi
	cmpq	%rsi, %rcx
	jnb	.L439
	cmpq	$1, %rdx
	jne	.L439
	xorl	%edi, %edi
	movl	$64, %r8d
.L306:
	movq	%rcx, %rdx
	movq	%rdi, %rax
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	testl	%ebp, %ebp
	movq	%rax, %r9
	movq	%rdx, %rcx
	je	.L691
	movl	$64, %r13d
	movq	%rax, %rbx
	leaq	136(%rsp), %rdi
	movl	%r13d, %eax
	movq	%rdx, %r12
	subl	%ebp, %eax
	movq	%rdi, 56(%rsp)
	movl	%eax, %ebp
	je	.L311
	movl	%eax, %ecx
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	%r13d, %ecx
	movq	%rbx, %rax
	subl	%ebp, %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
.L311:
	testq	%r12, %r12
	movq	128(%rsp), %rax
	movl	$63, %r8d
	setne	%r9b
	orb	92(%rsp), %r9b
	movl	24(%rsp), %edx
	movq	56(%rsp), %rdi
	subl	%ebp, %r8d
	movq	%rbx, %rcx
	leaq	-1(%rax), %rsi
	movslq	%r8d, %r8
	andl	$1, %r9d
	call	round_and_return
	jmp	.L119
.L691:
	testq	%r9, %r9
	jne	.L309
.L692:
	movq	%r9, %rax
	movq	%rcx, %rdx
	subq	$64, 128(%rsp)
#APP
# 1501 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	movq	%rax, %r9
	movq	%rdx, %rcx
	testq	%r9, %r9
	je	.L692
.L309:
	bsrq	%r9, %rax
	movl	%r8d, %ebp
	movq	%r9, 136(%rsp)
	xorq	$63, %rax
	movslq	%eax, %rdx
	subl	%eax, %ebp
	subq	%rdx, 128(%rsp)
	jmp	.L306
.L392:
	flds	.LC11(%rip)
	jmp	.L119
.L642:
	movq	16(%rsp), %rcx
	movq	%r10, %rdx
	movq	%r11, %rdi
	movq	%rbp, %rsi
	movq	%r8, 56(%rsp)
	movl	%r9d, 48(%rsp)
	movq	%r10, 32(%rsp)
	movq	%r11, 16(%rsp)
	call	__correctly_grouped_prefixmb
	cmpq	%rax, %rbp
	movq	16(%rsp), %r11
	movq	32(%rsp), %r10
	movl	48(%rsp), %r9d
	movq	56(%rsp), %r8
	je	.L693
	cmpq	%rax, %r11
	je	.L169
	cmpq	%rax, %rbx
	ja	.L590
	movq	%rbx, %rdi
	movl	$0, %r14d
	jnb	.L590
.L171:
	movzbl	(%rdi), %esi
	leal	-48(%rsi), %edx
	cmpb	$10, %dl
	adcq	$0, %r14
	addq	$1, %rdi
	cmpq	%rdi, %rax
	jne	.L171
	movq	%r14, %r15
	xorl	%esi, %esi
	jmp	.L176
.L657:
	cmpq	$-1, %rsi
	je	.L412
	movabsq	$2305843009213689855, %rax
	cmpq	%rax, %rsi
	ja	.L694
	leaq	16387(,%rsi,4), %r11
	jmp	.L616
.L623:
	movl	24(%rsp), %r10d
	movq	__libc_errno@gottpoff(%rip), %rax
	fldt	.LC2(%rip)
	testl	%r10d, %r10d
	movl	$34, %fs:(%rax)
	je	.L265
	fldt	.LC3(%rip)
	fmulp	%st, %st(1)
	jmp	.L119
.L280:
	cmpq	%r15, %r14
	jne	.L286
	subl	$1, %r13d
	movl	$1, %ebx
	leaq	136(%rsp), %rdi
	movl	%r13d, %edx
	movq	%rbx, %rbp
	sarl	$31, %edx
	subq	%r12, %rbp
	movq	%rdi, 56(%rsp)
	shrl	$26, %edx
	leaq	(%rdi,%rbp,8), %rdi
	leal	0(%r13,%rdx), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$63, %eax
	je	.L695
	movq	8(%rsp), %rsi
	movl	$63, %ecx
	movq	%r12, %rdx
	subl	%eax, %ecx
	call	__mpn_lshift
	subq	120(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.L289
.L288:
	movq	$0, 136(%rsp)
.L289:
	movl	24(%rsp), %edx
	movq	56(%rsp), %rdi
	movslq	%r13d, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L119
.L420:
	movl	$1, %r9d
	jmp	.L257
.L677:
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	leaq	0(,%r14,8), %rdx
	movq	%r10, 32(%rsp)
	call	memcpy@PLT
	movq	32(%rsp), %r10
	jmp	.L299
.L265:
	fmul	%st(0), %st
	jmp	.L119
.L335:
	jne	.L696
	testl	%r14d, %r14d
	jle	.L697
	leal	-1(%r14), %ecx
	movq	8(%rsp), %rax
	leaq	136(%rsp), %rdi
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	movq	%rdi, 56(%rsp)
	salq	$3, %rdx
	salq	$3, %rcx
	addq	%rdx, %rax
	addq	%rdi, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L349:
	movq	(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rax, %rcx
	jne	.L349
.L350:
	movq	$0, 144(%rsp)
	movq	$0, 7024(%rsp,%r14,8)
	movq	144(%rsp,%r14,8), %rbx
	jmp	.L348
.L312:
	movq	152(%rsp), %rbx
.L315:
	movq	%rdi, %r9
	xorl	%r8d, %r8d
	movl	$64, %r10d
	negq	%r9
.L331:
	cmpq	%rbx, %rsi
	jne	.L318
	addq	%r12, %rbx
	jnc	.L321
	subq	%rdi, %rbx
	movq	%r8, %r13
#APP
# 1607 "../stdlib/strtod_l.c" 1
	addq %rdi,%r13
	adcq $0,%rbx
# 0 "" 2
#NO_APP
	testl	%ebp, %ebp
	je	.L428
	movq	$-1, %r12
.L323:
	movl	$64, %r14d
	movl	%r14d, %eax
	subl	%ebp, %eax
	movl	%eax, %ebp
	leaq	136(%rsp), %rax
	jne	.L385
	movq	%rax, 56(%rsp)
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L318:
	movq	%r12, %rax
	movq	%rbx, %rdx
#APP
# 1615 "../stdlib/strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	movq	%rax, %r12
	movq	%rdx, %rbx
	movq	%rdi, %rax
#APP
# 1616 "../stdlib/strtod_l.c" 1
	mulq %r12
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L329:
	cmpq	%rbx, %rdx
	ja	.L325
	jne	.L326
	testq	%rax, %rax
	je	.L326
.L325:
	subq	$1, %r12
#APP
# 1625 "../stdlib/strtod_l.c" 1
	subq %rdi,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%rsi, %rbx
	jnc	.L329
.L326:
	movq	%r8, %r13
#APP
# 1630 "../stdlib/strtod_l.c" 1
	subq %rax,%r13
	sbbq %rdx,%rbx
# 0 "" 2
#NO_APP
	testl	%ebp, %ebp
	jne	.L323
	testq	%r12, %r12
	movl	$64, %eax
	jne	.L698
.L330:
	movq	%r12, 136(%rsp)
	subq	%rax, 128(%rsp)
	movq	%r13, %r12
	jmp	.L331
.L321:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rdi, %rdx
	setne	%al
	movq	$-1, %r12
	subq	%rax, %rdx
	movq	%r9, %rax
	jmp	.L329
.L428:
	movl	$64, %ebp
	xorl	%eax, %eax
	movq	$-1, %r12
	jmp	.L330
.L385:
	movl	%ebp, %ecx
	movq	%rax, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, 56(%rsp)
	call	__mpn_lshift
	movl	%r14d, %ecx
	movq	%r12, %rax
	subl	%ebp, %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
	jmp	.L317
.L413:
	movq	$3, 32(%rsp)
	movl	$493, %edx
	movl	$0, 16(%rsp)
	jmp	.L194
.L653:
	movl	24(%rsp), %r9d
	movq	__libc_errno@gottpoff(%rip), %rax
	fldt	.LC0(%rip)
	testl	%r9d, %r9d
	movl	$34, %fs:(%rax)
	je	.L265
	fldt	.LC1(%rip)
	fmulp	%st, %st(1)
	jmp	.L119
.L666:
	leaq	7024(%rsp), %rsi
	leaq	4(%r11), %rdi
	movl	$41, %edx
	call	__strtold_nan
	movq	7024(%rsp), %rax
	cmpb	$41, (%rax)
	leaq	1(%rax), %rdx
	cmove	%rdx, %rbx
	jmp	.L135
.L286:
	movq	8(%rsp), %rsi
	leaq	136(%rsp), %rdi
	leaq	0(,%r12,8), %rdx
	movq	%r10, 16(%rsp)
	call	memcpy@PLT
	cmpq	%r15, %r14
	jbe	.L269
	movq	128(%rsp), %rax
	leaq	4951(%rax), %rdx
	cmpq	$4951, %rdx
	ja	.L269
	testl	%r13d, %r13d
	jle	.L291
	testq	%rax, %rax
	movq	16(%rsp), %r10
	jne	.L699
	movl	$65, %ecx
	movslq	%r13d, %rax
	xorl	%edx, %edx
	subl	%r13d, %ecx
	movq	%rax, 56(%rsp)
	jmp	.L293
.L281:
	movslq	%ecx, %r8
	movq	%r10, %rdi
	subq	$1, %r8
	shrq	%cl, %rdi
	cmpq	%rsi, %rdx
	jg	.L382
	movq	%rdi, 136(%rsp)
	jmp	.L282
.L675:
	leaq	-1(%rbx), %rax
	cmpl	$16, %r9d
	cmovne	%r14, %rax
	jmp	.L158
.L427:
	xorl	%ebx, %ebx
	jmp	.L315
.L693:
	movzbl	0(%r13), %eax
	jmp	.L167
.L412:
	movq	$7, 32(%rsp)
	movl	$1638, %edx
	movl	$0, 16(%rsp)
	jmp	.L194
.L590:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.L627
	jmp	.L157
.L682:
	subq	%rax, 128(%rsp)
	leaq	136(%rsp), %rax
	movl	$0, 88(%rsp)
	movq	%rax, 56(%rsp)
	jmp	.L337
.L338:
	movl	$64, %eax
	leaq	136(%rsp), %rdi
	subl	%ebp, %eax
	movq	%rdi, 56(%rsp)
	movl	%eax, %ecx
	movl	%eax, 88(%rsp)
	je	.L340
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	120(%rsp), %rdx
	jmp	.L340
.L404:
	movq	%rcx, %rdx
	jmp	.L163
.L382:
	addl	$1, %eax
	movl	$64, %edx
	cltq
	subl	%ecx, %edx
	movq	144(%rsp,%rax,8), %rax
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %rdi
	movq	%rdi, 136(%rsp)
	jmp	.L282
.L695:
	movq	8(%rsp), %rsi
	leaq	0(,%r12,8), %rdx
	call	memcpy@PLT
	testq	%rbp, %rbp
	jle	.L289
	jmp	.L288
.L679:
	movq	56(%rsp), %rax
	movq	%r12, %rbx
	xorl	%r12d, %r12d
	subq	$64, %rax
	movq	%rax, 128(%rsp)
	jmp	.L315
.L697:
	leaq	136(%rsp), %rax
	movq	%rax, 56(%rsp)
	jmp	.L350
.L422:
	xorl	%edx, %edx
	jmp	.L283
.L430:
	xorl	%r12d, %r12d
	jmp	.L351
.L698:
	bsrq	%r12, %rdx
	movl	%r10d, %ebp
	xorq	$63, %rdx
	movslq	%edx, %rax
	subl	%edx, %ebp
	jmp	.L330
.L630:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$598, %edx
	call	__assert_fail
.L291:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC31(%rip), %rdi
	movl	$1376, %edx
	call	__assert_fail
.L269:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$1360, %edx
	call	__assert_fail
.L437:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$914, %edx
	call	__assert_fail
.L696:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$1708, %edx
	call	__assert_fail
.L676:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$906, %edx
	call	__assert_fail
.L686:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$1750, %edx
	call	__assert_fail
.L662:
	movq	%rax, 128(%rsp)
	jmp	.L205
.L669:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$1100, %edx
	call	__assert_fail
.L694:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$926, %edx
	call	__assert_fail
.L699:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$1370, %edx
	call	__assert_fail
.L438:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$946, %edx
	call	__assert_fail
.L683:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$1671, %edx
	call	__assert_fail
.L396:
	movq	%rbx, %r11
	movq	$0, 16(%rsp)
	movsbq	%dl, %r12
	movl	$16, %r9d
	jmp	.L144
.L439:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	movl	$1497, %edx
	call	__assert_fail
.L659:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$938, %edx
	call	__assert_fail
.L674:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$958, %edx
	call	__assert_fail
.L644:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC17(%rip), %rdi
	movl	$875, %edx
	call	__assert_fail
.L647:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	movl	$1021, %edx
	call	__assert_fail
.L654:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$1397, %edx
	call	__assert_fail
.L236:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$1076, %edx
	call	__assert_fail
.L379:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$1072, %edx
	call	__assert_fail
.L671:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$1121, %edx
	call	__assert_fail
.L395:
	movq	%rbx, %r11
	movl	$16, %r9d
	movq	$0, 16(%rsp)
	jmp	.L143
	.size	____strtold_l_internal, .-____strtold_l_internal
	.p2align 4,,15
	.weak	__strtold_l
	.hidden	__strtold_l
	.type	__strtold_l, @function
__strtold_l:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	jmp	____strtold_l_internal
	.size	__strtold_l, .-__strtold_l
	.weak	strtold_l
	.set	strtold_l,__strtold_l
	.weak	strtof64x_l
	.set	strtof64x_l,strtold_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11822, @object
	.size	__PRETTY_FUNCTION__.11822, 11
__PRETTY_FUNCTION__.11822:
	.string	"str_to_mpn"
	.section	.rodata
	.align 32
	.type	nbits.11960, @object
	.size	nbits.11960, 64
nbits.11960:
	.long	0
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11872, @object
	.size	__PRETTY_FUNCTION__.11872, 23
__PRETTY_FUNCTION__.11872:
	.string	"____strtold_l_internal"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	0
	.long	2147483648
	.long	1
	.long	0
	.align 16
.LC1:
	.long	0
	.long	2147483648
	.long	32769
	.long	0
	.align 16
.LC2:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.align 16
.LC3:
	.long	4294967295
	.long	4294967295
	.long	65534
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC5:
	.long	2143289344
	.align 4
.LC11:
	.long	2139095040
	.align 4
.LC12:
	.long	4286578688
	.hidden	__strtold_nan
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
	.hidden	__mpn_cmp
	.hidden	__mpn_lshift
	.hidden	__correctly_grouped_prefixmb
	.hidden	__strncasecmp_l
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	_nl_C_locobj
	.hidden	strlen
	.hidden	__assert_fail
	.hidden	__mpn_mul_1
	.hidden	__mpn_construct_long_double
	.hidden	__mpn_rshift
	.hidden	abort
