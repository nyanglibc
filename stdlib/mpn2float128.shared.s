	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mpn_construct_float128
	.hidden	__mpn_construct_float128
	.type	__mpn_construct_float128, @function
__mpn_construct_float128:
	movq	$0, -16(%rsp)
	movq	%rdx, %rax
	movq	-16(%rsp), %rdx
	movabsq	$9223372036854775807, %rcx
	salq	$63, %rax
	addw	$16383, %si
	andl	$32767, %esi
	movq	$0, -24(%rsp)
	andq	%rcx, %rdx
	salq	$48, %rsi
	movabsq	$-4294967296, %rcx
	orq	%rax, %rdx
	movabsq	$-9223090561878065153, %rax
	andq	%rax, %rdx
	movq	-24(%rsp), %rax
	orq	%rsi, %rdx
	movq	%rdx, -16(%rsp)
	movq	(%rdi), %rdx
	andq	%rcx, %rax
	movl	%edx, %esi
	shrq	$32, %rdx
	orq	%rsi, %rax
	salq	$32, %rdx
	movl	%eax, %eax
	orq	%rdx, %rax
	movq	-16(%rsp), %rdx
	movq	%rax, -24(%rsp)
	movq	8(%rdi), %rax
	andq	%rcx, %rdx
	movl	%eax, %esi
	shrq	$32, %rax
	orq	%rsi, %rdx
	movq	%rdx, -16(%rsp)
	movdqa	-24(%rsp), %xmm0
	pinsrw	$6, %eax, %xmm0
	ret
	.size	__mpn_construct_float128, .-__mpn_construct_float128
