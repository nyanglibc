	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_lrand48_r
	.hidden	__GI_lrand48_r
	.type	__GI_lrand48_r, @function
__GI_lrand48_r:
	testq	%rdi, %rdi
	je	.L2
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	jmp	__nrand48_r
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$-1, %eax
	ret
	.size	__GI_lrand48_r, .-__GI_lrand48_r
	.globl	lrand48_r
	.set	lrand48_r,__GI_lrand48_r
	.hidden	__nrand48_r
