	.text
	.p2align 4,,15
	.type	round_away, @function
round_away:
	cmpl	$1024, %r8d
	je	.L3
	jle	.L18
	cmpl	$2048, %r8d
	je	.L6
	cmpl	$3072, %r8d
	jne	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmove	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testl	%r8d, %r8d
	jne	.L2
	orl	%esi, %ecx
	movl	$0, %eax
	testb	%dl, %dl
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	orl	%edx, %ecx
	movl	$0, %eax
	testb	%dil, %dil
	cmovne	%ecx, %eax
	ret
.L2:
	subq	$8, %rsp
	call	abort
	.size	round_away, .-round_away
	.p2align 4,,15
	.type	round_and_return, @function
round_and_return:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %rdx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$56, %rsp
#APP
# 94 "../sysdeps/generic/get-rounding-mode.h" 1
	fnstcw 46(%rsp)
# 0 "" 2
#NO_APP
	movzwl	46(%rsp), %eax
	andw	$3072, %ax
	cmpw	$1024, %ax
	je	.L21
	jbe	.L61
	cmpw	$2048, %ax
	je	.L24
	cmpw	$3072, %ax
	jne	.L20
	movl	$3072, %r10d
.L23:
	cmpq	$-1022, %rbx
	jge	.L26
.L62:
	cmpq	$-1075, %rbx
	jge	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	movl	$34, %fs:(%rax)
	je	.L28
	movsd	.LC1(%rip), %xmm0
	mulsd	.LC0(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	cmpq	$-1022, %rbx
	movl	$2048, %r10d
	jl	.L62
.L26:
	cmpq	$1023, %rbx
	jle	.L63
.L38:
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r15d, %r15d
	movl	$34, %fs:(%rax)
	je	.L44
	movsd	.LC3(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movsd	.LC2(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	mulsd	%xmm0, %xmm0
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	testw	%ax, %ax
	jne	.L20
	xorl	%r10d, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L27:
	movq	$-1, %rax
	movl	%r8d, %ecx
	movq	$-1022, %rsi
	salq	%cl, %rax
	subq	%rbx, %rsi
	movq	0(%rbp), %r11
	notq	%rax
	testq	%rdx, %rax
	setne	%al
	movzbl	%al, %eax
	orl	%eax, %r9d
	movl	%r9d, %eax
	andl	$1, %eax
	cmpq	$53, %rsi
	movb	%al, 11(%rsp)
	je	.L64
	movq	$-1023, %r14
	movq	%r14, %rcx
	subq	%rbx, %rcx
	movq	%r11, %rbx
	shrq	%cl, %rbx
	movl	%ecx, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	cmpq	$1, %rsi
	je	.L32
	movl	%esi, %ecx
	movl	%r9d, 24(%rsp)
	movq	%r11, 16(%rsp)
	movl	%r10d, 12(%rsp)
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
.L59:
	call	__mpn_rshift
	movq	0(%rbp), %r12
	movl	12(%rsp), %r10d
	movq	16(%rsp), %r11
	movl	24(%rsp), %r9d
	movl	%r12d, %esi
	andl	$1, %esi
.L31:
	andl	$1, %ebx
	jne	.L36
	cmpb	$0, 11(%rsp)
	je	.L65
.L36:
	movq	__libc_errno@gottpoff(%rip), %rax
	movsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	movl	$34, %fs:(%rax)
.L60:
	movq	$-1023, %rbx
.L35:
	testl	%r9d, %r9d
	movl	$1, %r14d
	jne	.L39
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
.L37:
	testq	%rax, %rax
	setne	%r14b
	movzbl	%r14b, %r9d
.L39:
	movzbl	%r13b, %edx
	movl	%r10d, %r8d
	movl	%r9d, %ecx
	movl	%r15d, %edi
	call	round_away
	testb	%al, %al
	je	.L40
	addq	$1, %r12
	btq	$53, %r12
	movq	%r12, 0(%rbp)
	jc	.L66
	cmpq	$-1023, %rbx
	je	.L43
.L40:
	movl	%ebx, %esi
.L42:
	testb	%r14b, %r14b
	jne	.L51
	testb	%r13b, %r13b
	jne	.L51
.L45:
	movl	%r15d, %edx
	movq	%rbp, %rdi
	call	__mpn_construct_double
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$1024, %r10d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	movsd	.LC0(%rip), %xmm0
	addq	$56, %rsp
	popq	%rbx
	mulsd	%xmm0, %xmm0
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	%r8d, %ecx
	movq	%r11, %rsi
	movl	%r10d, %r8d
	shrq	%cl, %rdx
	andl	$1, %esi
	movl	%r9d, %ecx
	andl	$1, %edx
	movl	%r15d, %edi
	movq	%r11, 24(%rsp)
	movl	%r10d, 16(%rsp)
	movl	%r9d, 12(%rsp)
	call	round_away
	movl	12(%rsp), %r9d
	movl	16(%rsp), %r10d
	testb	%al, %al
	movq	24(%rsp), %r11
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	movl	%r9d, 24(%rsp)
	movl	%r10d, 12(%rsp)
	movq	%r11, 16(%rsp)
	je	.L59
	call	__mpn_rshift
	movq	16(%rsp), %r11
	movq	0(%rbp), %r12
	movl	12(%rsp), %r10d
	movl	24(%rsp), %r9d
	leaq	1(%r11), %rax
	movl	%r12d, %esi
	andl	$1, %esi
	btq	$53, %rax
	jc	.L60
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L51:
	movsd	.LC0(%rip), %xmm0
	addsd	.LC4(%rip), %xmm0
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L63:
	movq	0(%rbp), %r12
	movq	%rdx, %r13
	movl	%r8d, %ecx
	shrq	%cl, %r13
	movl	%r8d, 4(%rsp)
	movq	%rdx, %r11
	andl	$1, %r13d
	movl	%r12d, %esi
	andl	$1, %esi
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$1, %rbx
	movl	$1, %ecx
	movl	$1, %edx
	movq	%rbp, %rsi
	movq	%rbp, %rdi
	call	__mpn_rshift
	movabsq	$4503599627370496, %rax
	orq	%rax, 0(%rbp)
	cmpq	$1024, %rbx
	je	.L38
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movabsq	$4503599627370496, %rax
	xorl	%esi, %esi
	testq	%rax, %r12
	setne	%sil
	subl	$1023, %esi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L65:
	movzbl	4(%rsp), %ecx
	movq	$-1, %rax
	salq	%cl, %rax
	notq	%rax
	andq	%r11, %rax
	jne	.L36
	movq	$-1023, %rbx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%r11, %rbx
	movq	$0, 0(%rbp)
	xorl	%esi, %esi
	shrq	$52, %rbx
	xorl	%r12d, %r12d
	movl	$52, 4(%rsp)
	movl	%ebx, %r13d
	andl	$1, %r13d
	jmp	.L31
.L20:
	call	abort
	.size	round_and_return, .-round_and_return
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"strtod_l.c"
.LC6:
	.string	"digcnt > 0"
.LC7:
	.string	"*nsize < MPNSIZE"
	.text
	.p2align 4,,15
	.type	str_to_mpn.isra.0, @function
str_to_mpn.isra.0:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testl	%esi, %esi
	movq	$0, (%rcx)
	movq	80(%rsp), %r14
	jle	.L111
	movq	%rdi, %rbx
	movl	%esi, %r15d
	movq	%rdx, %r12
	movq	%rcx, %r13
.L110:
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L68:
	movsbq	(%rbx), %rax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L77
	testq	%r14, %r14
	je	.L78
	cmpb	(%r14), %al
	je	.L112
.L78:
	addq	%r9, %rbx
	movsbq	(%rbx), %rax
.L77:
	leaq	0(%rbp,%rbp,4), %rcx
	addq	$1, %rbx
	addl	$1, %edx
	subl	$1, %r15d
	leaq	-48(%rax,%rcx,2), %rbp
	je	.L113
	cmpl	$19, %edx
	jne	.L68
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L70
	movq	%rbp, (%r12)
	movq	$1, 0(%r13)
	xorl	%ebp, %ebp
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L70:
	movabsq	$-8446744073709551616, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r9, 8(%rsp)
	movq	%r8, (%rsp)
	call	__mpn_mul_1
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	addq	(%r12), %rdx
	movq	0(%r13), %rsi
	movq	(%rsp), %r8
	movq	8(%rsp), %r9
	setc	%cl
	movq	%rdx, (%r12)
	testq	%rcx, %rcx
	jne	.L114
.L73:
	testq	%rax, %rax
	je	.L110
	movq	0(%r13), %rdx
	cmpq	$58, %rdx
	jg	.L115
	movq	%rax, (%r12,%rdx,8)
	xorl	%ebp, %ebp
	addq	$1, 0(%r13)
	xorl	%edx, %edx
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L113:
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	jle	.L84
	movl	$19, %eax
	subl	%edx, %eax
	cltq
	cmpq	%rax, %rcx
	jle	.L116
.L84:
	movslq	%edx, %rax
	leaq	_tens_in_limb(%rip), %rdx
	movq	(%rdx,%rax,8), %rcx
	movq	0(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L86
.L117:
	movq	%rbp, (%r12)
	movq	$1, 0(%r13)
.L100:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movzbl	1(%r14), %eax
	testb	%al, %al
	je	.L79
	cmpb	1(%rbx), %al
	jne	.L78
	leaq	2(%rbx), %rcx
	leaq	2(%r14), %rsi
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	addq	$1, %rcx
	addq	$1, %rsi
	cmpb	%al, %dil
	jne	.L78
.L80:
	movzbl	(%rsi), %edi
	movq	%rcx, %r10
	movsbq	(%rcx), %rax
	testb	%dil, %dil
	jne	.L81
.L82:
	movq	%r10, %rbx
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L114:
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L75:
	movq	8(%r12,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L73
.L74:
	cmpq	%rdx, %rsi
	jne	.L75
	addq	$1, %rax
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L116:
	movslq	%edx, %rax
	movq	$0, (%r8)
	movq	0(%r13), %rdx
	leaq	_tens_in_limb(%rip), %rsi
	imulq	(%rsi,%rcx,8), %rbp
	addq	%rax, %rcx
	testq	%rdx, %rdx
	movq	(%rsi,%rcx,8), %rcx
	je	.L117
.L86:
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_mul_1
	xorl	%ecx, %ecx
	movq	%rbp, %rdx
	addq	(%r12), %rdx
	movq	0(%r13), %rsi
	setc	%cl
	movq	%rdx, (%r12)
	testq	%rcx, %rcx
	je	.L90
	subq	$1, %rsi
	xorl	%edx, %edx
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L92:
	movq	8(%r12,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r12,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L90
.L91:
	cmpq	%rsi, %rdx
	jne	.L92
	addq	$1, %rax
.L90:
	testq	%rax, %rax
	je	.L100
	movq	0(%r13), %rdx
	cmpq	$58, %rdx
	jg	.L118
	leaq	1(%rdx), %rcx
	movq	%rcx, 0(%r13)
	movq	%rax, (%r12,%rdx,8)
	jmp	.L100
.L79:
	leaq	1(%rbx), %r10
	movsbq	1(%rbx), %rax
	jmp	.L82
.L118:
	leaq	__PRETTY_FUNCTION__.11822(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$453, %edx
	call	__assert_fail
.L115:
	leaq	__PRETTY_FUNCTION__.11822(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$397, %edx
	call	__assert_fail
.L111:
	leaq	__PRETTY_FUNCTION__.11822(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$380, %edx
	call	__assert_fail
	.size	str_to_mpn.isra.0, .-str_to_mpn.isra.0
	.section	.rodata.str1.1
.LC13:
	.string	"decimal_len > 0"
.LC14:
	.string	"inf"
.LC15:
	.string	"inity"
.LC16:
	.string	"nan"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC18:
	.string	"dig_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC19:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_EXP - MANT_DIG) / 4"
	.align 8
.LC20:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX / 4"
	.align 8
.LC21:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_EXP - 3) / 4"
	.align 8
.LC22:
	.string	"int_no <= (uintmax_t) (INTMAX_MAX + MIN_10_EXP - MANT_DIG)"
	.align 8
.LC23:
	.string	"lead_zero == 0 && int_no <= (uintmax_t) INTMAX_MAX"
	.align 8
.LC24:
	.string	"lead_zero <= (uintmax_t) (INTMAX_MAX - MAX_10_EXP - 1)"
	.section	.rodata.str1.1
.LC25:
	.string	"dig_no >= int_no"
	.section	.rodata.str1.8
	.align 8
.LC26:
	.string	"lead_zero <= (base == 16 ? (uintmax_t) INTMAX_MAX / 4 : (uintmax_t) INTMAX_MAX)"
	.align 8
.LC27:
	.string	"lead_zero <= (base == 16 ? ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN) / 4 : ((uintmax_t) exponent - (uintmax_t) INTMAX_MIN))"
	.section	.rodata.str1.1
.LC28:
	.string	"bits != 0"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"int_no <= (uintmax_t) (exponent < 0 ? (INTMAX_MAX - bits + 1) / 4 : (INTMAX_MAX - exponent - bits + 1) / 4)"
	.align 8
.LC30:
	.string	"dig_no > int_no && exponent <= 0 && exponent >= MIN_10_EXP - (DIG + 2)"
	.section	.rodata.str1.1
.LC31:
	.string	"int_no > 0 && exponent == 0"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"int_no == 0 && *startp != L_('0')"
	.section	.rodata.str1.1
.LC33:
	.string	"need_frac_digits > 0"
.LC34:
	.string	"numsize == 1 && n < d"
.LC35:
	.string	"numsize == densize"
.LC36:
	.string	"cy != 0"
	.text
	.p2align 4,,15
	.globl	____strtod_l_internal
	.hidden	____strtod_l_internal
	.type	____strtod_l_internal, @function
____strtod_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r10d, %r10d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r8
	subq	$1112, %rsp
	testl	%edx, %edx
	movq	8(%rcx), %rax
	movq	%rsi, 8(%rsp)
	movq	$0, 16(%rsp)
	jne	.L631
.L120:
	movq	64(%rax), %r13
	movq	%r8, 32(%rsp)
	movq	%r10, 24(%rsp)
	movq	%r13, %rdi
	call	strlen
	testq	%rax, %rax
	movq	%rax, 40(%rsp)
	movq	24(%rsp), %r10
	movq	32(%rsp), %r8
	je	.L632
	movq	$0, 128(%rsp)
	leaq	-1(%r14), %rax
	movq	104(%r8), %rdi
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L396:
	movq	%r11, %rax
.L122:
	movsbq	1(%rax), %rcx
	leaq	1(%rax), %r11
	testb	$32, 1(%rdi,%rcx,2)
	movq	%rcx, %rdx
	jne	.L396
	cmpb	$45, %cl
	je	.L633
	cmpb	$43, %cl
	movl	$0, 24(%rsp)
	je	.L634
.L124:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L398
	cmpb	%dl, %al
	jne	.L126
	movl	$1, %esi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	addq	$1, %rsi
	cmpb	%r9b, %cl
	jne	.L126
.L127:
	movzbl	0(%r13,%rsi), %ecx
	movzbl	(%r11,%rsi), %r9d
	testb	%cl, %cl
	jne	.L128
.L125:
	subl	$48, %r9d
	cmpb	$9, %r9b
	jbe	.L129
.L126:
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	jbe	.L129
	movq	112+_nl_C_locobj(%rip), %rax
	movl	(%rax,%rdx,4), %eax
	cmpb	$105, %al
	je	.L635
	cmpb	$110, %al
	je	.L636
.L169:
	cmpq	$0, 8(%rsp)
	jne	.L637
.L138:
	pxor	%xmm0, %xmm0
.L119:
	addq	$1112, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	$48, %dl
	movq	112(%r8), %rsi
	je	.L638
	testq	%r10, %r10
	movl	$10, %r9d
	je	.L639
.L143:
	movsbq	%dl, %r12
	movzbl	(%r10), %ebp
	movq	%r11, %rbx
	cmpb	$48, %r12b
	movq	$-1, %r15
	je	.L404
	.p2align 4,,10
	.p2align 3
.L640:
	testb	%bpl, %bpl
	je	.L405
	cmpb	%bpl, (%rbx)
	jne	.L144
	movl	$1, %edx
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L149:
	addq	$1, %rdx
	cmpb	%cl, -1(%rbx,%rdx)
	jne	.L144
.L148:
	movzbl	(%r10,%rdx), %ecx
	testb	%cl, %cl
	jne	.L149
	subq	$1, %rdx
.L147:
	addq	%rbx, %rdx
.L146:
	movsbq	1(%rdx), %r12
	leaq	1(%rdx), %rbx
	cmpb	$48, %r12b
	jne	.L640
.L404:
	movq	%rbx, %rdx
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L634:
	movsbq	1(%r11), %rdx
	leaq	2(%rax), %r11
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L639:
	movsbq	%dl, %r12
	movq	%r11, %rbx
	movl	$10, %r9d
	.p2align 4,,10
	.p2align 3
.L144:
	leal	-48(%r12), %edx
	cmpb	$9, %dl
	jbe	.L408
	movsbq	%r12b, %rdx
	movl	(%rsi,%rdx,4), %edi
	leal	-97(%rdi), %edx
	cmpb	$5, %dl
	ja	.L439
	cmpl	$16, %r9d
	je	.L408
.L439:
	testb	%al, %al
	je	.L152
	cmpb	(%rbx), %al
	jne	.L153
	movl	$1, %edx
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$1, %rdx
	cmpb	%cl, -1(%rbx,%rdx)
	jne	.L153
.L154:
	movzbl	0(%r13,%rdx), %ecx
	testb	%cl, %cl
	jne	.L155
.L152:
	cmpl	$16, %r9d
	jne	.L408
	cmpq	%r11, %rbx
	jne	.L408
	movq	40(%rsp), %rdi
	movsbq	(%rbx,%rdi), %rdx
	leal	-48(%rdx), %edi
	cmpb	$9, %dil
	ja	.L641
	.p2align 4,,10
	.p2align 3
.L408:
	movq	%rbx, %rbp
	xorl	%r15d, %r15d
.L150:
	movq	$-1, %rcx
.L166:
	leal	-48(%r12), %edx
	cmpb	$9, %dl
	jbe	.L159
	cmpl	$16, %r9d
	je	.L642
.L160:
	testq	%r10, %r10
	jne	.L643
.L162:
	cmpq	$0, 16(%rsp)
	je	.L167
	cmpq	%r11, %rbp
	ja	.L644
.L167:
	xorl	%esi, %esi
	testq	%r15, %r15
	sete	%sil
	negq	%rsi
	testb	%al, %al
	je	.L177
	cmpb	%al, 0(%rbp)
	jne	.L414
	movl	$1, %eax
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L180:
	addq	$1, %rax
	cmpb	%dl, -1(%rbp,%rax)
	jne	.L414
.L179:
	movzbl	0(%r13,%rax), %edx
	testb	%dl, %dl
	jne	.L180
.L177:
	movq	40(%rsp), %rax
	movq	%r15, %rdx
	leaq	0(%rbp,%rax), %rcx
	movsbq	(%rcx), %r12
	movq	%rcx, %rbp
	subq	%rcx, %rdx
	jmp	.L378
	.p2align 4,,10
	.p2align 3
.L645:
	movq	112(%r8), %rdi
	movsbq	%r12b, %rax
	movl	(%rdi,%rax,4), %eax
	subl	$97, %eax
	cmpb	$5, %al
	ja	.L178
.L182:
	cmpb	$48, %r12b
	je	.L181
	cmpq	$-1, %rsi
	movq	%rbp, %rax
	sete	%dil
	subq	%rcx, %rax
	testb	%dil, %dil
	cmovne	%rax, %rsi
.L181:
	addq	$1, %rbp
	movsbq	0(%rbp), %r12
.L378:
	leal	-48(%r12), %eax
	leaq	(%rdx,%rbp), %r14
	cmpb	$9, %al
	jbe	.L182
	cmpl	$16, %r9d
	je	.L645
.L178:
	testq	%r14, %r14
	js	.L646
	movq	112(%r8), %rax
	cmpl	$16, %r9d
	movl	(%rax,%r12,4), %eax
	jne	.L440
	cmpb	$112, %al
	jne	.L440
.L184:
	movsbl	1(%rbp), %ecx
	cmpb	$45, %cl
	je	.L647
	cmpb	$43, %cl
	je	.L648
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L418
	cmpl	$16, %r9d
	leaq	1(%rbp), %rdi
	je	.L191
.L192:
	testq	%r15, %r15
	je	.L200
	testq	%rsi, %rsi
	jne	.L442
	testq	%r15, %r15
	js	.L442
	movl	$309, %r11d
	subq	%r15, %r11
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L642:
	movsbq	%r12b, %rdx
	movl	(%rsi,%rdx,4), %edx
	subl	$97, %edx
	cmpb	$5, %dl
	ja	.L160
	.p2align 4,,10
	.p2align 3
.L159:
	addq	$1, %r15
	movq	%rbp, %rdx
.L161:
	leaq	1(%rdx), %rbp
	movsbq	1(%rdx), %r12
	jmp	.L166
	.p2align 4,,10
	.p2align 3
.L633:
	movsbq	1(%r11), %rdx
	movl	$1, 24(%rsp)
	leaq	2(%rax), %r11
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L440:
	cmpl	$16, %r9d
	je	.L418
	cmpb	$101, %al
	je	.L184
.L418:
	movq	%rbp, %rdi
.L186:
	cmpq	%r15, %r14
	jbe	.L217
	cmpb	$48, -1(%rbp)
	jne	.L176
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L218:
	subq	$1, %rax
	leaq	(%rax,%r14), %rdx
	subq	%rbp, %rdx
	cmpb	$48, -1(%rax)
	je	.L218
	cmpq	%rdx, %r15
	ja	.L649
	movq	%rdx, %r14
	movq	%rax, %rbp
.L217:
	cmpq	%r15, %r14
	jne	.L176
	testq	%r14, %r14
	je	.L176
	movq	128(%rsp), %rcx
	testq	%rcx, %rcx
	js	.L650
	cmpq	$0, 8(%rsp)
	je	.L227
.L381:
	movq	8(%rsp), %rax
	movq	%rdi, (%rax)
.L226:
	testq	%r14, %r14
	jne	.L227
.L157:
	movl	24(%rsp), %r14d
	movsd	.LC10(%rip), %xmm0
	testl	%r14d, %r14d
	jne	.L119
	jmp	.L138
.L669:
	testb	%dl, %dl
	movq	16(%rsp), %rsi
	je	.L176
.L623:
	movq	%rcx, 128(%rsp)
	.p2align 4,,10
	.p2align 3
.L176:
	cmpq	$0, 8(%rsp)
	jne	.L381
	jmp	.L226
.L637:
	movq	8(%rsp), %rax
	pxor	%xmm0, %xmm0
	movq	%r14, (%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r15, %r14
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L638:
	movsbq	1(%r11), %rdi
	cmpl	$120, (%rsi,%rdi,4)
	movq	%rdi, %r12
	je	.L140
	testq	%r10, %r10
	movl	$10, %r9d
	jne	.L143
.L141:
	movq	%r11, %rbx
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L651:
	movsbq	1(%rbx), %r12
.L145:
	addq	$1, %rbx
	cmpb	$48, %r12b
	je	.L651
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L227:
	testq	%rsi, %rsi
	je	.L228
	movzbl	0(%r13), %ecx
	movzbl	1(%r13), %edi
	movzbl	(%rbx), %eax
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L229:
	movzbl	1(%rbx), %eax
.L231:
	addq	$1, %rbx
.L234:
	cmpb	%al, %cl
	jne	.L229
	testb	%dil, %dil
	je	.L230
	movzbl	1(%rbx), %eax
	cmpb	%dil, %al
	jne	.L231
	movl	$2, %eax
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	addq	$1, %rax
	cmpb	%dl, -1(%rbx,%rax)
	jne	.L423
.L232:
	movzbl	0(%r13,%rax), %edx
	testb	%dl, %dl
	jne	.L233
.L230:
	cmpl	$16, %r9d
	je	.L652
	testq	%rsi, %rsi
	movq	%rsi, %rcx
	js	.L384
	movq	128(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	cmpq	%rsi, %rdx
	jb	.L236
.L237:
	movq	40(%rsp), %rdi
	subq	%rcx, %rax
	subq	%rsi, %r14
	movq	%rax, 128(%rsp)
	leaq	(%rdi,%rsi), %rdx
	addq	%rdx, %rbx
.L228:
	cmpl	$16, %r9d
	je	.L653
	movq	128(%rsp), %rax
	testq	%rax, %rax
	js	.L654
	movq	%r14, %rdx
	subq	%r15, %rdx
	cmpq	%rax, %rdx
	cmovg	%rax, %rdx
.L261:
	subq	%rdx, %rax
	addq	%rdx, %r15
	movq	%rax, %rcx
	movq	%rax, 128(%rsp)
	movl	$309, %eax
	subq	%r15, %rax
	cmpq	%rax, %rcx
	jg	.L627
	cmpq	$-324, %rcx
	jl	.L655
	testq	%r15, %r15
	jne	.L266
	testq	%r14, %r14
	je	.L269
	leaq	324(%rcx), %rax
	cmpq	$324, %rax
	ja	.L269
	cmpb	$48, (%rbx)
	je	.L291
	movl	$1, %eax
	movabsq	$-6148914691236517205, %rsi
	subq	%rcx, %rax
	leaq	(%rax,%rax,4), %rdx
	addq	%rdx, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	movl	$1076, %esi
	shrq	%rdx
	leal	54(%rdx), %eax
	movl	%ecx, %edx
	cmpl	$1076, %eax
	cmovg	%esi, %eax
	addl	%ecx, %eax
	testl	%eax, %eax
	jle	.L656
	leaq	128(%rsp), %rdi
	movq	$0, 56(%rsp)
	movl	$0, 8(%rsp)
	movq	%rdi, 104(%rsp)
	leaq	120(%rsp), %rdi
	movq	%rdi, 88(%rsp)
	leaq	144(%rsp), %rdi
	movq	%rdi, 16(%rsp)
.L293:
	movslq	%eax, %rcx
	movq	%r14, %rsi
	movl	%r14d, %eax
	subl	%r15d, %eax
	subq	%r15, %rsi
	movl	$0, 100(%rsp)
	cmpq	%rsi, %rcx
	cltq
	cmovg	%rax, %rcx
	addq	%r15, %rcx
	cmpq	%rcx, %r14
	jle	.L295
	movq	%rcx, %r14
	movl	$1, 100(%rsp)
.L295:
	movl	%r14d, %eax
	xorl	%r12d, %r12d
	movq	%rbx, 80(%rsp)
	subl	%r15d, %eax
	movq	%r12, %rbx
	leaq	_fpioconst_pow10(%rip), %r14
	movl	%eax, %ebp
	movl	%eax, 64(%rsp)
	movq	16(%rsp), %rax
	subl	%edx, %ebp
	movl	$1, %r13d
	movq	%r10, 72(%rsp)
	movq	%rax, 48(%rsp)
	leaq	624(%rsp), %rax
	movq	%rax, 32(%rsp)
	movq	%rax, %r12
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L658:
	leaq	0(,%r15,8), %rdx
	movq	%r12, %rdi
	movq	%r15, %rbx
	call	memcpy@PLT
.L296:
	addl	%r13d, %r13d
	addq	$24, %r14
	testl	%ebp, %ebp
	je	.L657
.L298:
	testl	%ebp, %r13d
	je	.L296
	movq	8(%r14), %rax
	leaq	__tens(%rip), %rdi
	xorl	%r13d, %ebp
	testq	%rbx, %rbx
	leaq	-1(%rax), %r15
	movq	(%r14), %rax
	leaq	8(%rdi,%rax,8), %rsi
	je	.L658
	movq	%r15, %rdx
	movq	48(%rsp), %r15
	movq	%rbx, %r8
	movq	%r12, %rcx
	movq	%r15, %rdi
	call	__mpn_mul
	movq	8(%r14), %rdx
	testq	%rax, %rax
	leaq	-1(%rbx,%rdx), %rbx
	jne	.L433
	movq	%r12, %rax
	subq	$1, %rbx
	movq	%r15, %r12
	movq	%rax, 48(%rsp)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L648:
	movsbl	2(%rbp), %ecx
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L418
	cmpl	$16, %r9d
	leaq	2(%rbp), %rdi
	jne	.L192
.L191:
	testq	%r15, %r15
	je	.L659
	testq	%rsi, %rsi
	jne	.L441
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %r15
	ja	.L441
	movl	$256, %eax
	subq	%r15, %rax
	leaq	3(,%rax,4), %r11
.L197:
	testq	%r11, %r11
	movl	$0, %eax
	cmovs	%rax, %r11
.L620:
	movq	%r11, %rax
	movabsq	$-3689348814741910323, %rdx
	movl	$0, 16(%rsp)
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r11
	movq	%r11, 32(%rsp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L423:
	movl	%edi, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L647:
	movsbl	2(%rbp), %ecx
	leaq	2(%rbp), %rdi
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	ja	.L418
	cmpl	$16, %r9d
	je	.L660
	movabsq	$9223372036854775447, %rax
	cmpq	%rax, %r15
	ja	.L661
	leaq	360(%r15), %r11
.L621:
	movq	%r11, %rax
	movabsq	$-3689348814741910323, %rdx
	movl	$1, 16(%rsp)
	mulq	%rdx
	shrq	$3, %rdx
	leaq	(%rdx,%rdx,4), %rax
	addq	%rax, %rax
	subq	%rax, %r11
	movq	%r11, 32(%rsp)
.L194:
	movq	128(%rsp), %rax
	xorl	%r12d, %r12d
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L204:
	subl	$48, %ecx
	cmpq	%rdx, %rax
	movslq	%ecx, %rcx
	je	.L662
.L206:
	leaq	(%rax,%rax,4), %rax
	addq	$1, %rdi
	movl	$1, %r12d
	leaq	(%rcx,%rax,2), %rax
	movsbl	(%rdi), %ecx
	leal	-48(%rcx), %r11d
	cmpb	$9, %r11b
	ja	.L663
.L208:
	cmpq	%rdx, %rax
	jle	.L204
.L207:
	testb	%r12b, %r12b
	jne	.L664
.L205:
	cmpq	$-1, %rsi
	je	.L665
	movl	16(%rsp), %r12d
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	jne	.L666
	movl	24(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L214
	movsd	.LC3(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	.p2align 4,,10
	.p2align 3
.L215:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	subl	$48, %eax
	cmpb	$9, %al
	jbe	.L215
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L119
	movq	%rdi, (%rax)
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L662:
	cmpq	%rcx, 32(%rsp)
	jge	.L206
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L153:
	cmpl	$16, %r9d
	je	.L667
	xorl	%r15d, %r15d
	cmpb	$101, %dil
	movq	%rbx, %rbp
	je	.L150
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L631:
	movq	80(%rax), %rdi
	movq	%rdi, 16(%rsp)
	movzbl	(%rdi), %edi
	leal	-1(%rdi), %edx
	movb	%dil, 24(%rsp)
	cmpb	$125, %dl
	ja	.L394
	movq	72(%rax), %r10
	cmpb	$0, (%r10)
	jne	.L120
	movq	$0, 16(%rsp)
	xorl	%r10d, %r10d
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L635:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC14(%rip), %rsi
	movl	$3, %edx
	movq	%r11, %rdi
	movq	%r11, 16(%rsp)
	call	__strncasecmp_l
	testl	%eax, %eax
	jne	.L169
	movq	8(%rsp), %r15
	testq	%r15, %r15
	je	.L132
	movq	16(%rsp), %r11
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	leaq	3(%r11), %rbx
	movq	%r11, 8(%rsp)
	movq	%rbx, %rdi
	call	__strncasecmp_l
	movq	8(%rsp), %r11
	addq	$8, %r11
	testl	%eax, %eax
	cmove	%r11, %rbx
	movq	%rbx, (%r15)
.L132:
	movl	24(%rsp), %eax
	movsd	.LC11(%rip), %xmm0
	testl	%eax, %eax
	je	.L119
	movsd	.LC12(%rip), %xmm0
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L636:
	leaq	_nl_C_locobj(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movl	$3, %edx
	movq	%r11, %rdi
	movq	%r11, 16(%rsp)
	call	__strncasecmp_l
	testl	%eax, %eax
	jne	.L169
	movq	16(%rsp), %r11
	movsd	.LC8(%rip), %xmm0
	cmpb	$40, 3(%r11)
	leaq	3(%r11), %rbx
	je	.L668
.L135:
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L137
	movq	%rbx, (%rax)
.L137:
	movl	24(%rsp), %r15d
	testl	%r15d, %r15d
	je	.L119
	xorpd	.LC17(%rip), %xmm0
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L398:
	movl	%edx, %r9d
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L650:
	xorl	%eax, %eax
	cmpl	$16, %r9d
	movq	%rsi, 16(%rsp)
	sete	%al
	subq	$1, %rbp
	xorl	%edx, %edx
	leaq	1(%rax,%rax,2), %r12
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L221:
	movsbl	%al, %r11d
	subl	$48, %r11d
	cmpl	$9, %r11d
	seta	%r11b
.L222:
	testb	%r11b, %r11b
	jne	.L223
	cmpb	$48, %al
	jne	.L669
	addq	%r12, %rcx
	subq	$1, %r15
	subq	$1, %r14
	movq	%rcx, %rax
	setne	%dl
	shrq	$63, %rax
	andb	%al, %dl
	je	.L670
.L223:
	subq	$1, %rbp
.L220:
	cmpl	$16, %r9d
	movzbl	0(%rbp), %eax
	jne	.L221
	movq	104(%r8), %rsi
	movsbq	%al, %r11
	movzwl	(%rsi,%r11,2), %r11d
	shrw	$12, %r11w
	xorl	$1, %r11d
	andl	$1, %r11d
	jmp	.L222
.L140:
	testq	%r10, %r10
	leaq	2(%r11), %rbx
	movzbl	2(%r11), %edx
	jne	.L402
	cmpb	$48, %dl
	jne	.L403
	movsbq	3(%r11), %r12
	movq	$0, 16(%rsp)
	movq	%rbx, %r11
	movl	$16, %r9d
	jmp	.L141
.L653:
	movsbq	(%rbx), %rax
	movq	104(%r8), %r9
	testb	$16, 1(%r9,%rax,2)
	movq	%rax, %rdx
	jne	.L624
	.p2align 4,,10
	.p2align 3
.L240:
	addq	$1, %rbx
	movsbq	(%rbx), %rax
	testb	$16, 1(%r9,%rax,2)
	movq	%rax, %rdx
	je	.L240
	jmp	.L624
	.p2align 4,,10
	.p2align 3
.L242:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
.L624:
	cmpb	$48, %dl
	je	.L242
	movsbl	%dl, %r10d
	leaq	1(%rbx), %rax
	subl	$48, %r10d
	cmpl	$9, %r10d
	ja	.L243
.L625:
	leaq	nbits.11960(%rip), %rdx
	movslq	%r10d, %r10
	movl	(%rdx,%r10,4), %edx
	testl	%edx, %edx
	je	.L671
	movq	128(%rsp), %rsi
	movl	$53, %ecx
	movl	$52, %edi
	subl	%edx, %ecx
	subl	%edx, %edi
	movslq	%edx, %r11
	salq	%cl, %r10
	testq	%rsi, %rsi
	movq	%r10, 136(%rsp)
	js	.L672
	movabsq	$9223372036854775807, %rcx
	subq	%rsi, %rcx
	subq	%r11, %rcx
	leaq	4(%rcx), %r11
	addq	$1, %rcx
	cmovs	%r11, %rcx
	sarq	$2, %rcx
.L247:
	cmpq	%r15, %rcx
	jb	.L673
	subl	$1, %edx
	movslq	%edx, %rdx
	leaq	-4(%rdx,%r15,4), %rdx
	addq	%rdx, %rsi
	subq	$1, %r14
	movq	%rsi, 128(%rsp)
	je	.L249
	movq	%r10, %rbx
	xorl	%ebp, %ebp
	movq	40(%rsp), %r12
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L675:
	leal	-3(%rdi), %ecx
	subl	$4, %edi
	movq	%r11, %rax
	movl	$1, %ebp
	salq	%cl, %rdx
	orq	%rdx, %rbx
	testq	%r14, %r14
	je	.L674
.L254:
	movsbq	(%rax), %rdx
	testb	$16, 1(%r9,%rdx,2)
	movq	%rdx, %rcx
	jne	.L250
	addq	%r12, %rax
	movsbq	(%rax), %rcx
.L250:
	movsbl	%cl, %edx
	leaq	1(%rax), %r11
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L626
	movq	112(%r8), %rdx
	movl	(%rdx,%rcx,4), %edx
	subl	$87, %edx
.L626:
	subq	$1, %r14
	cmpl	$2, %edi
	movslq	%edx, %rdx
	jg	.L675
	testb	%bpl, %bpl
	movl	$3, %ecx
	cmovne	%rbx, %r10
	subl	%edi, %ecx
	movq	%rdx, %rbx
	shrq	%cl, %rbx
	leal	61(%rdi), %ecx
	orq	%rbx, %r10
	salq	%cl, %rdx
	testq	%r14, %r14
	movq	%r10, 136(%rsp)
	movq	%rdx, %rcx
	je	.L425
	cmpb	$48, 1(%rax)
	jne	.L427
	addq	$2, %rax
	addq	%r11, %r14
	jmp	.L258
	.p2align 4,,10
	.p2align 3
.L259:
	addq	$1, %rax
	cmpb	$48, -1(%rax)
	jne	.L427
.L258:
	cmpq	%r14, %rax
	jne	.L259
.L425:
	xorl	%r9d, %r9d
.L257:
	movl	24(%rsp), %edx
	leaq	136(%rsp), %rdi
	movl	$63, %r8d
	call	round_and_return
	jmp	.L119
.L643:
	movzbl	(%r10), %edx
	testb	%dl, %dl
	je	.L411
	cmpb	%dl, 0(%rbp)
	jne	.L162
	movl	$1, %edx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	addq	$1, %rdx
	cmpb	%dil, -1(%rbp,%rdx)
	jne	.L162
.L164:
	movzbl	(%r10,%rdx), %edi
	testb	%dil, %dil
	jne	.L165
	subq	$1, %rdx
.L163:
	addq	%rbp, %rdx
	jmp	.L161
.L663:
	movl	16(%rsp), %r11d
	movq	%rax, %rdx
	negq	%rdx
	testl	%r11d, %r11d
	cmovne	%rdx, %rax
	movq	%rax, 128(%rsp)
	jmp	.L186
.L394:
	movq	$0, 16(%rsp)
	jmp	.L120
.L666:
	movl	24(%rsp), %ebp
	testl	%ebp, %ebp
	je	.L213
	movsd	.LC1(%rip), %xmm0
	mulsd	.LC0(%rip), %xmm0
	jmp	.L215
.L243:
	movq	112(%r8), %rcx
	movl	(%rcx,%rdx,4), %r10d
	subl	$87, %r10d
	jmp	.L625
.L200:
	cmpq	$-1, %rsi
	je	.L420
	movabsq	$9223372036854775498, %rax
	cmpq	%rax, %rsi
	ja	.L676
	leaq	309(%rsi), %r11
	jmp	.L620
.L641:
	movl	(%rsi,%rdx,4), %edx
	subl	$97, %edx
	cmpb	$5, %dl
	jbe	.L408
.L156:
	movq	16(%rsp), %rcx
	movq	%r10, %rdx
	movq	%rbx, %rsi
	movq	%r11, %rdi
	movl	%r9d, 32(%rsp)
	movq	%r11, 16(%rsp)
	call	__correctly_grouped_prefixmb
	cmpq	$0, 8(%rsp)
	je	.L157
	movq	16(%rsp), %r11
	movl	32(%rsp), %r9d
	cmpq	%r11, %rax
	je	.L677
.L158:
	movq	8(%rsp), %rdi
.L629:
	movq	%rax, (%rdi)
	jmp	.L157
.L672:
	movabsq	$-9223372036854775808, %rcx
	subq	%r11, %rcx
	sarq	$2, %rcx
	jmp	.L247
.L660:
	movabsq	$2305843009213693683, %rax
	cmpq	%rax, %r15
	ja	.L678
	leaq	1074(,%r15,4), %r11
	jmp	.L621
	.p2align 4,,10
	.p2align 3
.L657:
	movq	%r12, %r9
	cmpq	16(%rsp), %r9
	movq	%rbx, %r12
	movq	72(%rsp), %r10
	movq	80(%rsp), %rbx
	je	.L679
.L299:
	subq	$8, %rsp
	movq	%rbx, %rdi
	pushq	%r10
	movq	56(%rsp), %r9
	movq	120(%rsp), %r8
	movq	104(%rsp), %rcx
	movq	32(%rsp), %rdx
	movl	80(%rsp), %esi
	call	str_to_mpn.isra.0
	leaq	-1(%r12), %rax
	bsrq	640(%rsp,%rax,8), %rbx
	movq	%rax, 64(%rsp)
	popq	%r13
	popq	%r14
	xorq	$63, %rbx
	testl	%ebx, %ebx
	jne	.L300
.L628:
	movq	120(%rsp), %rdx
.L301:
	movq	56(%rsp), %rax
	cmpq	$1, %r12
	movq	%rax, 128(%rsp)
	je	.L304
	cmpq	$2, %r12
	jne	.L680
	cmpq	$1, %rdx
	movq	624(%rsp), %rdi
	movq	632(%rsp), %rsi
	movq	144(%rsp), %r13
	jg	.L315
	cmpq	%r13, %rsi
	jbe	.L434
	movl	8(%rsp), %r12d
	testl	%r12d, %r12d
	je	.L681
	movl	$53, %eax
	subl	8(%rsp), %eax
	leaq	136(%rsp), %rdi
	movq	%rdi, 56(%rsp)
	movl	%eax, %ecx
	je	.L319
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	144(%rsp), %r13
.L319:
	movl	8(%rsp), %r8d
	movq	128(%rsp), %rax
	movq	%r13, %rbx
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	addl	$10, %r8d
	movslq	%r8d, %r8
.L320:
	orq	%r13, %rbx
	movl	24(%rsp), %edx
	movq	56(%rsp), %rdi
	setne	%r9b
	orb	100(%rsp), %r9b
	leaq	-1(%rax), %rsi
	movq	%r12, %rcx
	andl	$1, %r9d
	call	round_and_return
	jmp	.L119
.L433:
	movq	%r12, %rax
	movq	48(%rsp), %r12
	movq	%rax, 48(%rsp)
	jmp	.L296
.L665:
	movl	24(%rsp), %r13d
	pxor	%xmm0, %xmm0
	testl	%r13d, %r13d
	je	.L215
	movsd	.LC10(%rip), %xmm0
	jmp	.L215
.L654:
	movq	%r15, %rdx
	negq	%rdx
	cmpq	%rax, %rdx
	cmovl	%rax, %rdx
	jmp	.L261
.L300:
	movq	32(%rsp), %rdi
	movl	%ebx, %ecx
	movq	%r12, %rdx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	16(%rsp), %rdi
	movq	120(%rsp), %rdx
	movl	%ebx, %ecx
	movq	%rdi, %rsi
	call	__mpn_lshift
	testq	%rax, %rax
	je	.L628
	movq	120(%rsp), %rcx
	leaq	1(%rcx), %rdx
	movq	%rax, 144(%rsp,%rcx,8)
	movq	%rdx, 120(%rsp)
	jmp	.L301
.L674:
	movq	%rbx, 136(%rsp)
.L249:
	movl	24(%rsp), %edx
	leaq	136(%rsp), %rdi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L119
.L266:
	leaq	120(%rsp), %rcx
	leaq	144(%rsp), %rdx
	leaq	128(%rsp), %r8
	subq	$8, %rsp
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movq	%r8, 112(%rsp)
	movq	%rcx, 96(%rsp)
	movq	%rdx, 24(%rsp)
	pushq	%r10
	movq	56(%rsp), %r9
	movq	%r10, 24(%rsp)
	call	str_to_mpn.isra.0
	movq	144(%rsp), %rdx
	movq	%rax, %rbx
	popq	%rax
	popq	%rcx
	testq	%rdx, %rdx
	movq	120(%rsp), %rbp
	movq	8(%rsp), %r10
	jle	.L271
	leaq	624(%rsp), %rax
	movl	$1, %r9d
	leaq	_fpioconst_pow10(%rip), %r12
	movq	%rbp, %rdi
	movl	%r9d, %r13d
	movq	%rbx, %rbp
	movq	%rax, 32(%rsp)
	movq	%rax, %r11
	movq	16(%rsp), %rax
	movq	%rax, 8(%rsp)
.L272:
	movslq	%r13d, %rax
	testq	%rdx, %rax
	je	.L273
.L683:
	movq	8(%r12), %rsi
	xorq	%rdx, %rax
	movq	%r10, 56(%rsp)
	movq	%rax, 128(%rsp)
	movq	(%r12), %rax
	leaq	-1(%rsi), %rbx
	leaq	__tens(%rip), %rsi
	cmpq	%rdi, %rbx
	leaq	8(%rsi,%rax,8), %rsi
	jg	.L274
	movq	%rsi, %rcx
	movq	8(%rsp), %rsi
	movq	%rdi, %rdx
	movq	%rbx, %r8
	movq	%r11, %rdi
	movq	%r11, 48(%rsp)
	call	__mpn_mul
	movq	48(%rsp), %r11
	movq	56(%rsp), %r10
.L275:
	movq	120(%rsp), %rdi
	movq	128(%rsp), %rdx
	addq	%rbx, %rdi
	testq	%rax, %rax
	movq	%rdi, 120(%rsp)
	jne	.L276
	subq	$1, %rdi
	movq	%rdi, 120(%rsp)
.L276:
	addl	%r13d, %r13d
	addq	$24, %r12
	testq	%rdx, %rdx
	je	.L682
	movq	8(%rsp), %rax
	movq	%r11, 8(%rsp)
	movq	%rax, %r11
	movslq	%r13d, %rax
	testq	%rdx, %rax
	jne	.L683
.L273:
	addl	%r13d, %r13d
	addq	$24, %r12
	jmp	.L272
.L274:
	movq	8(%rsp), %rcx
	movq	%rdi, %r8
	movq	%rbx, %rdx
	movq	%r11, %rdi
	movq	%r11, 48(%rsp)
	call	__mpn_mul
	movq	56(%rsp), %r10
	movq	48(%rsp), %r11
	jmp	.L275
.L682:
	cmpq	32(%rsp), %r11
	movq	%rbp, %rbx
	movq	%rdi, %rbp
	jne	.L271
	leaq	0(,%rdi,8), %rdx
	movq	16(%rsp), %rdi
	movq	%r11, %rsi
	movq	%r10, 8(%rsp)
	call	memcpy@PLT
	movq	8(%rsp), %r10
.L271:
	leaq	-1(%rbp), %rdx
	movl	%ebp, %r12d
	sall	$6, %r12d
	bsrq	144(%rsp,%rdx,8), %rax
	xorq	$63, %rax
	subl	%eax, %r12d
	cmpl	$1024, %r12d
	movl	%r12d, 8(%rsp)
	jg	.L627
	cmpl	$53, %r12d
	jle	.L280
	leal	-53(%r12), %ecx
	movl	%ecx, %eax
	sarl	$6, %eax
	andl	$63, %ecx
	movslq	%eax, %rsi
	movq	144(%rsp,%rsi,8), %r10
	jne	.L281
	subq	$1, %rsi
	movq	%r10, 136(%rsp)
	movl	$63, %r8d
	movq	144(%rsp,%rsi,8), %r10
.L282:
	cmpq	$0, 144(%rsp)
	jne	.L429
	movq	16(%rsp), %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L284:
	movslq	%eax, %rdx
	addq	$1, %rax
	cmpq	$0, -8(%rcx,%rax,8)
	je	.L284
.L283:
	cmpq	%r15, %r14
	movl	$1, %r9d
	ja	.L285
	xorl	%r9d, %r9d
	cmpq	%rsi, %rdx
	setl	%r9b
.L285:
	leal	-1(%r12), %esi
	movl	24(%rsp), %edx
	leaq	136(%rsp), %rdi
	movq	%r10, %rcx
	movslq	%esi, %rsi
	call	round_and_return
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L652:
	movabsq	$2305843009213693951, %rax
	cmpq	%rax, %rsi
	ja	.L384
	movq	128(%rsp), %rax
	movabsq	$-9223372036854775808, %rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	cmpq	%rsi, %rdx
	jb	.L236
	leaq	0(,%rsi,4), %rcx
	jmp	.L237
.L670:
	movq	16(%rsp), %rsi
	jmp	.L623
.L405:
	movq	%r15, %rdx
	jmp	.L147
.L667:
	cmpb	$112, %dil
	jne	.L156
	cmpq	%r11, %rbx
	jne	.L408
	jmp	.L156
.L680:
	movq	48(%rsp), %rax
	movq	32(%rsp), %rdi
	movq	624(%rsp,%rax,8), %r14
	leaq	-2(%r12), %rax
	movq	%rax, 88(%rsp)
	movq	624(%rsp,%rax,8), %r15
	movq	%r12, %rax
	subq	%rdx, %rax
	leaq	(%rdi,%rax,8), %rsi
	movq	16(%rsp), %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	js	.L684
	movq	120(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	$0, 144(%rsp,%rdx,8)
	movq	%rax, 120(%rsp)
.L340:
	cmpq	%rax, %r12
	jle	.L341
	movq	%r12, %rbx
	movl	8(%rsp), %ecx
	subq	%rax, %rbx
	movq	%rbx, %rdx
	salq	$6, %rdx
	testl	%ecx, %ecx
	je	.L685
	movl	$53, %ebp
	subl	8(%rsp), %ebp
	leaq	136(%rsp), %rdi
	movq	%rdi, 56(%rsp)
	je	.L344
	movl	%ebp, %ecx
	movl	$1, %edx
	movq	%rdi, %rsi
	call	__mpn_lshift
	movq	120(%rsp), %rax
.L344:
	movl	%ebx, %edx
	sall	$6, %edx
	addl	%edx, 8(%rsp)
.L343:
	testl	%eax, %eax
	movslq	%eax, %rcx
	jle	.L348
	leal	-1(%rax), %edi
	movq	16(%rsp), %rax
	addq	%rbx, %rcx
	movslq	%edi, %rdx
	movl	%edi, %edi
	leaq	0(,%rdx,8), %rsi
	salq	$3, %rdi
	subq	%rdx, %rcx
	addq	%rsi, %rax
	addq	56(%rsp), %rsi
	subq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L347:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	subq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L347
.L348:
	movq	16(%rsp), %rax
	movq	%rax, %rdx
	addq	$8, %rax
	leaq	(%rax,%rbx,8), %rcx
	jmp	.L346
.L686:
	addq	$8, %rax
.L346:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L686
	cmpl	$53, 8(%rsp)
	movq	$0, 624(%rsp,%r12,8)
	movq	144(%rsp,%r12,8), %rbx
	jg	.L687
.L352:
	leaq	1(%r12), %rax
	movq	16(%rsp), %rdi
	movl	%ebp, 104(%rsp)
	movq	%r12, %rbp
	movq	%rax, 72(%rsp)
	leal	-2(%r12), %eax
	movslq	%eax, %rcx
	movl	%eax, %eax
	leaq	0(,%rcx,8), %rdx
	salq	$3, %rax
	addq	%rdx, %rdi
	movq	%rdi, 64(%rsp)
	movq	56(%rsp), %rdi
	leaq	(%rdi,%rdx), %r8
	subq	%rax, %r8
	leal	-1(%r12), %eax
	movq	%r15, %r12
	movq	%r8, %r15
	movslq	%eax, %r9
	movl	%eax, 80(%rsp)
	subq	%rcx, %r9
	.p2align 4,,10
	.p2align 3
.L356:
	cmpq	%r14, %rbx
	movq	$-1, %r13
	je	.L357
	movq	48(%rsp), %rax
	movq	%rbx, %rdx
	movq	88(%rsp), %rsi
	movq	144(%rsp,%rax,8), %rax
	movq	%rax, 40(%rsp)
#APP
# 1727 "strtod_l.c" 1
	divq %r14
# 0 "" 2
#NO_APP
	movq	%rax, %r13
	movq	%rdx, %rbx
	movq	%r12, %rax
#APP
# 1728 "strtod_l.c" 1
	mulq %r13
# 0 "" 2
#NO_APP
	movq	%rdx, %rcx
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L688:
	xorl	%edx, %edx
	cmpq	%r12, %rax
	setb	%dl
	subq	%r12, %rax
	subq	%rdx, %rcx
.L358:
	cmpq	%rbx, %rcx
	ja	.L361
	jne	.L357
	cmpq	%rax, 144(%rsp,%rsi,8)
	jnb	.L357
.L361:
	subq	$1, %r13
	addq	%r14, %rbx
	jnc	.L688
.L357:
	movq	16(%rsp), %rbx
	movq	72(%rsp), %rdx
	movq	%r13, %rcx
	movq	32(%rsp), %rsi
	movq	%r9, 40(%rsp)
	movq	%rbx, %rdi
	call	__mpn_submul_1
	cmpq	%rax, 144(%rsp,%rbp,8)
	movq	40(%rsp), %r9
	je	.L362
	movq	32(%rsp), %rdx
	movq	%rbp, %rcx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	__mpn_add_n
	testq	%rax, %rax
	movq	40(%rsp), %r9
	je	.L689
	subq	$1, %r13
.L362:
	movq	48(%rsp), %rax
	movl	80(%rsp), %edx
	movq	144(%rsp,%rax,8), %rbx
	testl	%edx, %edx
	movq	64(%rsp), %rax
	movq	%rbx, 144(%rsp,%rbp,8)
	jle	.L367
	.p2align 4,,10
	.p2align 3
.L364:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%r9,8)
	subq	$8, %rax
	cmpq	%rax, %r15
	jne	.L364
.L367:
	movl	8(%rsp), %eax
	movq	$0, 144(%rsp)
	testl	%eax, %eax
	jne	.L690
	testq	%r13, %r13
	movq	128(%rsp), %rsi
	je	.L368
	bsrq	%r13, %rax
	xorq	$63, %rax
	movslq	%eax, %rcx
	movl	%eax, %edx
	subq	%rcx, %rsi
	movl	$64, %ecx
	movl	%ecx, %edi
	movq	%rsi, 128(%rsp)
	subl	%eax, %edi
	cmpl	$53, %edi
	jle	.L369
	movq	%rbp, %r12
	leal	53(%rdx), %ebp
	movq	%r13, %rax
	subl	%ebp, %ecx
	shrq	%cl, %rax
	movq	%rax, 136(%rsp)
.L355:
	testl	%r12d, %r12d
	movl	%r12d, %edx
	js	.L373
	movslq	%r12d, %rax
	cmpq	$0, 144(%rsp,%rax,8)
	jne	.L373
	leal	-1(%r12), %eax
	movl	%r12d, %r12d
	movq	16(%rsp), %rdi
	cltq
	movq	%rax, %rcx
	subq	%r12, %rcx
	jmp	.L374
	.p2align 4,,10
	.p2align 3
.L691:
	subq	$1, %rax
	cmpq	$0, 8(%rdi,%rax,8)
	jne	.L373
.L374:
	cmpq	%rax, %rcx
	movl	%eax, %edx
	jne	.L691
.L373:
	notl	%edx
	movl	$63, %r8d
	movq	56(%rsp), %rdi
	shrl	$31, %edx
	subl	%ebp, %r8d
	subq	$1, %rsi
	movl	%edx, %r9d
	orl	100(%rsp), %r9d
	movl	24(%rsp), %edx
	movslq	%r8d, %r8
	movq	%r13, %rcx
	call	round_and_return
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L690:
	movl	8(%rsp), %edi
	cmpl	$-10, %edi
	leal	64(%rdi), %eax
	jge	.L371
	movq	%r13, 136(%rsp)
	movl	%eax, 8(%rsp)
.L370:
	cmpl	$53, 8(%rsp)
	jle	.L356
	movq	%rbp, %r12
	movq	128(%rsp), %rsi
	movl	104(%rsp), %ebp
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L368:
	subq	$64, %rsi
	movq	$0, 136(%rsp)
	movq	%rsi, 128(%rsp)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L371:
	movl	$53, %edx
	subl	8(%rsp), %edx
	movl	%eax, 8(%rsp)
	testl	%edx, %edx
	movl	%edx, 104(%rsp)
	je	.L370
	movq	56(%rsp), %rdi
	movl	%edx, %ecx
	movl	$1, %edx
	movq	%r9, 40(%rsp)
	movq	%rdi, %rsi
	call	__mpn_lshift
	movl	$64, %ecx
	subl	104(%rsp), %ecx
	movq	%r13, %rax
	movq	40(%rsp), %r9
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
	jmp	.L370
.L304:
	movq	144(%rsp), %rbx
	movq	624(%rsp), %rsi
	cmpq	%rsi, %rbx
	jnb	.L443
	cmpq	$1, %rdx
	jne	.L443
	movl	8(%rsp), %r8d
	xorl	%edi, %edi
	movl	$64, %ecx
	movq	%rbx, %rdx
.L306:
	movq	%rdi, %rax
#APP
# 1501 "strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	je	.L386
	movq	%rax, %r12
	movl	$53, %ebp
	leaq	136(%rsp), %rax
	subl	%r8d, %ebp
	movq	%rdx, %rbx
	movq	%rax, 56(%rsp)
	jne	.L692
.L313:
	movq	128(%rsp), %rdx
.L311:
	testq	%rbx, %rbx
	leaq	-1(%rdx), %rsi
	movl	$63, %r8d
	setne	%r9b
	orb	100(%rsp), %r9b
	movl	24(%rsp), %edx
	movq	56(%rsp), %rdi
	subl	%ebp, %r8d
	movq	%r12, %rcx
	movslq	%r8d, %r8
	andl	$1, %r9d
	call	round_and_return
	jmp	.L119
.L309:
	subq	$64, %r8
	movq	%r8, 128(%rsp)
#APP
# 1501 "strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
.L386:
	testq	%rax, %rax
	movq	128(%rsp), %r8
	je	.L309
	bsrq	%rax, %r9
	movl	%ecx, %ebx
	xorq	$63, %r9
	movslq	%r9d, %r10
	subl	%r9d, %ebx
	movl	%r9d, %ebp
	subq	%r10, %r8
	cmpl	$53, %ebx
	movq	%r8, 128(%rsp)
	jle	.L310
	addl	$53, %ebp
	movq	%rax, %r12
	movq	%rdx, %rbx
	subl	%ebp, %ecx
	movq	%r8, %rdx
	shrq	%cl, %rax
	movq	%rax, 136(%rsp)
	leaq	136(%rsp), %rax
	movq	%rax, 56(%rsp)
	jmp	.L311
.L214:
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L215
.L213:
	movsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L215
.L692:
	movl	%ebp, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, %rdi
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%r12, %rax
	subl	%ebp, %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
	jmp	.L313
.L644:
	movq	16(%rsp), %rcx
	movq	%r10, %rdx
	movq	%r11, %rdi
	movq	%rbp, %rsi
	movq	%r8, 56(%rsp)
	movl	%r9d, 48(%rsp)
	movq	%r10, 32(%rsp)
	movq	%r11, 16(%rsp)
	call	__correctly_grouped_prefixmb
	cmpq	%rax, %rbp
	movq	16(%rsp), %r11
	movq	32(%rsp), %r10
	movl	48(%rsp), %r9d
	movq	56(%rsp), %r8
	je	.L693
	cmpq	%r11, %rax
	je	.L169
	cmpq	%rax, %rbx
	ja	.L593
	movq	%rbx, %rdi
	movl	$0, %r14d
	jnb	.L593
.L171:
	movzbl	(%rdi), %esi
	leal	-48(%rsi), %edx
	cmpb	$10, %dl
	adcq	$0, %r14
	addq	$1, %rdi
	cmpq	%rdi, %rax
	jne	.L171
	movq	%r14, %r15
	xorl	%esi, %esi
	jmp	.L176
.L659:
	cmpq	$-1, %rsi
	je	.L419
	movabsq	$2305843009213693695, %rax
	cmpq	%rax, %rsi
	ja	.L694
	leaq	1027(,%rsi,4), %r11
	jmp	.L620
.L627:
	movl	24(%rsp), %edi
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%edi, %edi
	movl	$34, %fs:(%rax)
	je	.L263
	movsd	.LC3(%rip), %xmm0
	mulsd	.LC2(%rip), %xmm0
	jmp	.L119
.L280:
	cmpq	%r15, %r14
	jne	.L286
	subl	$1, %r12d
	movl	$1, %ebx
	leaq	136(%rsp), %rdi
	movl	%r12d, %edx
	movq	%rbx, %r13
	sarl	$31, %edx
	subq	%rbp, %r13
	movq	%rdi, 56(%rsp)
	shrl	$26, %edx
	leaq	(%rdi,%r13,8), %rdi
	leal	(%r12,%rdx), %eax
	andl	$63, %eax
	subl	%edx, %eax
	cmpl	$52, %eax
	je	.L695
	movq	16(%rsp), %rsi
	movl	$52, %ecx
	movq	%rbp, %rdx
	subl	%eax, %ecx
	call	__mpn_lshift
	subq	120(%rsp), %rbx
	testq	%rbx, %rbx
	jle	.L289
.L288:
	movq	$0, 136(%rsp)
.L289:
	movl	24(%rsp), %edx
	movq	56(%rsp), %rdi
	movslq	%r12d, %rsi
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	call	round_and_return
	jmp	.L119
.L427:
	movl	$1, %r9d
	jmp	.L257
.L679:
	movq	16(%rsp), %rsi
	movq	32(%rsp), %rdi
	leaq	0(,%r12,8), %rdx
	movq	%r10, 48(%rsp)
	call	memcpy@PLT
	movq	48(%rsp), %r10
	jmp	.L299
.L315:
	movq	152(%rsp), %rbx
.L318:
	movq	%rdi, %r10
	xorl	%r8d, %r8d
	movl	$64, %r9d
	negq	%r10
.L321:
	cmpq	%rsi, %rbx
	jne	.L322
	addq	%r13, %rbx
	jnc	.L325
	movl	8(%rsp), %ebp
	subq	%rdi, %rbx
	xorl	%r13d, %r13d
#APP
# 1607 "strtod_l.c" 1
	addq %rdi,%r13
	adcq $0,%rbx
# 0 "" 2
#NO_APP
	movq	$-1, %r12
	testl	%ebp, %ebp
	jne	.L327
	movq	128(%rsp), %rax
	movl	$10, %r8d
	movabsq	$9007199254740991, %rdx
	movq	$-1, %r12
.L391:
	leaq	136(%rsp), %rdi
	movq	%rdx, 136(%rsp)
	movq	%rdi, 56(%rsp)
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L322:
	movq	%rbx, %rdx
	movq	%r13, %rax
#APP
# 1615 "strtod_l.c" 1
	divq %rsi
# 0 "" 2
#NO_APP
	movq	%rax, %r12
	movq	%rdx, %rbx
	movq	%rdi, %rax
#APP
# 1616 "strtod_l.c" 1
	mulq %r12
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L333:
	cmpq	%rbx, %rdx
	ja	.L329
	jne	.L330
	testq	%rax, %rax
	je	.L330
.L329:
	subq	$1, %r12
#APP
# 1625 "strtod_l.c" 1
	subq %rdi,%rax
	sbbq $0,%rdx
# 0 "" 2
#NO_APP
	addq	%rsi, %rbx
	jnc	.L333
.L330:
	movl	8(%rsp), %r11d
	movq	%r8, %r13
#APP
# 1630 "strtod_l.c" 1
	subq %rax,%r13
	sbbq %rdx,%rbx
# 0 "" 2
#NO_APP
	testl	%r11d, %r11d
	jne	.L327
	testq	%r12, %r12
	movq	128(%rsp), %rax
	jne	.L696
	subq	$64, %rax
	movq	%r8, 136(%rsp)
	movq	%rax, 128(%rsp)
	jmp	.L321
	.p2align 4,,10
	.p2align 3
.L327:
	movl	$53, %ebp
	subl	8(%rsp), %ebp
	leaq	136(%rsp), %rax
	jne	.L392
	movq	%rax, 56(%rsp)
.L337:
	movl	$63, %r8d
	movq	128(%rsp), %rax
	subl	%ebp, %r8d
	movslq	%r8d, %r8
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L325:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rdi, %rdx
	setne	%al
	movq	$-1, %r12
	subq	%rax, %rdx
	movq	%r10, %rax
	jmp	.L333
.L392:
	movl	%ebp, %ecx
	movq	%rax, %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%rax, 56(%rsp)
	call	__mpn_lshift
	movl	$64, %ecx
	movq	%r12, %rax
	subl	%ebp, %ecx
	shrq	%cl, %rax
	orq	%rax, 136(%rsp)
	jmp	.L337
.L696:
	bsrq	%r12, %rdx
	movl	%r9d, %r15d
	xorq	$63, %rdx
	movslq	%edx, %r11
	subl	%edx, %r15d
	movl	%edx, %ecx
	subq	%r11, %rax
	cmpl	$53, %r15d
	movq	%rax, 128(%rsp)
	jle	.L335
	leal	53(%rcx), %esi
	movl	$63, %r8d
	movl	%r9d, %ecx
	movq	%r12, %rdx
	subl	%esi, %ecx
	subl	%esi, %r8d
	shrq	%cl, %rdx
	movslq	%r8d, %r8
	jmp	.L391
.L341:
	jne	.L697
	testl	%r12d, %r12d
	jle	.L698
	leal	-1(%r12), %ecx
	movq	16(%rsp), %rax
	leaq	136(%rsp), %rdi
	movslq	%ecx, %rdx
	movl	%ecx, %ecx
	movq	%rdi, 56(%rsp)
	salq	$3, %rdx
	salq	$3, %rcx
	addq	%rdx, %rax
	addq	%rdi, %rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align 4,,10
	.p2align 3
.L353:
	movq	(%rax), %rdx
	subq	$8, %rax
	movq	%rdx, 16(%rax)
	cmpq	%rax, %rcx
	jne	.L353
.L354:
	movq	$0, 144(%rsp)
	movq	$0, 624(%rsp,%r12,8)
	movq	144(%rsp,%r12,8), %rbx
	jmp	.L352
.L655:
	movl	24(%rsp), %esi
	movq	__libc_errno@gottpoff(%rip), %rax
	testl	%esi, %esi
	movl	$34, %fs:(%rax)
	je	.L265
	movsd	.LC1(%rip), %xmm0
	mulsd	.LC0(%rip), %xmm0
	jmp	.L119
.L420:
	movq	$9, 32(%rsp)
	movl	$30, %edx
	movl	$0, 16(%rsp)
	jmp	.L194
.L684:
	movq	120(%rsp), %rax
	jmp	.L340
.L668:
	leaq	624(%rsp), %rsi
	leaq	4(%r11), %rdi
	movl	$41, %edx
	call	__strtod_nan
	movq	624(%rsp), %rax
	cmpb	$41, (%rax)
	leaq	1(%rax), %rdx
	cmove	%rdx, %rbx
	jmp	.L135
.L286:
	movq	16(%rsp), %rsi
	leaq	136(%rsp), %rdi
	leaq	0(,%rbp,8), %rdx
	movq	%r10, 32(%rsp)
	call	memcpy@PLT
	cmpq	%r15, %r14
	jbe	.L269
	movq	128(%rsp), %rax
	leaq	324(%rax), %rdx
	cmpq	$324, %rdx
	ja	.L269
	testl	%r12d, %r12d
	jle	.L291
	testq	%rax, %rax
	movq	32(%rsp), %r10
	jne	.L699
	movslq	8(%rsp), %rdi
	movl	$54, %eax
	xorl	%edx, %edx
	subl	%edi, %eax
	movq	%rdi, 56(%rsp)
	jmp	.L293
.L281:
	movslq	%ecx, %r8
	movq	%r10, %rdi
	subq	$1, %r8
	shrq	%cl, %rdi
	cmpq	%rsi, %rdx
	jg	.L388
	movq	%rdi, 136(%rsp)
	jmp	.L282
.L263:
	movsd	.LC2(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L119
.L677:
	leaq	-1(%rbx), %rax
	cmpl	$16, %r9d
	cmovne	%r14, %rax
	jmp	.L158
.L434:
	xorl	%ebx, %ebx
	jmp	.L318
.L693:
	movzbl	0(%r13), %eax
	jmp	.L167
.L265:
	movsd	.LC0(%rip), %xmm0
	mulsd	%xmm0, %xmm0
	jmp	.L119
.L419:
	movq	$7, 32(%rsp)
	movl	$102, %edx
	movl	$0, 16(%rsp)
	jmp	.L194
.L593:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.L629
	jmp	.L157
.L685:
	leaq	136(%rsp), %rdi
	subq	%rdx, 128(%rsp)
	xorl	%ebp, %ebp
	movq	%rdi, 56(%rsp)
	jmp	.L343
.L388:
	addl	$1, %eax
	movl	$64, %edx
	cltq
	subl	%ecx, %edx
	movq	144(%rsp,%rax,8), %rax
	movl	%edx, %ecx
	salq	%cl, %rax
	orq	%rax, %rdi
	movq	%rdi, 136(%rsp)
	jmp	.L282
.L411:
	movq	%rcx, %rdx
	jmp	.L163
.L695:
	movq	16(%rsp), %rsi
	leaq	0(,%rbp,8), %rdx
	call	memcpy@PLT
	testq	%r13, %r13
	jle	.L289
	jmp	.L288
.L429:
	xorl	%edx, %edx
	jmp	.L283
.L687:
	movq	128(%rsp), %rsi
	xorl	%r13d, %r13d
	jmp	.L355
.L681:
	movq	56(%rsp), %rax
	movq	%r13, %rbx
	xorl	%r13d, %r13d
	subq	$64, %rax
	movq	%rax, 128(%rsp)
	jmp	.L318
.L698:
	leaq	136(%rsp), %rax
	movq	%rax, 56(%rsp)
	jmp	.L354
.L291:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC32(%rip), %rdi
	movl	$1376, %edx
	call	__assert_fail
.L632:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$598, %edx
	call	__assert_fail
.L646:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$875, %edx
	call	__assert_fail
.L442:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC23(%rip), %rdi
	movl	$946, %edx
	call	__assert_fail
.L697:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC35(%rip), %rdi
	movl	$1708, %edx
	call	__assert_fail
.L335:
	movl	%r15d, 8(%rsp)
	movq	%r12, 136(%rsp)
	jmp	.L321
.L402:
	movq	%rbx, %r11
	movl	$16, %r9d
	movq	$0, 16(%rsp)
	jmp	.L143
.L699:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC31(%rip), %rdi
	movl	$1370, %edx
	call	__assert_fail
.L369:
	movl	%edi, 8(%rsp)
	movq	%r13, 136(%rsp)
	jmp	.L370
.L403:
	movq	%rbx, %r11
	movq	$0, 16(%rsp)
	movsbq	%dl, %r12
	movl	$16, %r9d
	jmp	.L144
.L678:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$906, %edx
	call	__assert_fail
.L269:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC30(%rip), %rdi
	movl	$1360, %edx
	call	__assert_fail
.L441:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$914, %edx
	call	__assert_fail
.L664:
	movq	%rax, 128(%rsp)
	jmp	.L205
.L689:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC36(%rip), %rdi
	movl	$1750, %edx
	call	__assert_fail
.L310:
	movl	%ebx, %r8d
	movq	%rax, 136(%rsp)
	jmp	.L306
.L671:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC28(%rip), %rdi
	movl	$1100, %edx
	call	__assert_fail
.L673:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC29(%rip), %rdi
	movl	$1121, %edx
	call	__assert_fail
.L443:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC34(%rip), %rdi
	movl	$1497, %edx
	call	__assert_fail
.L661:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC22(%rip), %rdi
	movl	$938, %edx
	call	__assert_fail
.L649:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC25(%rip), %rdi
	movl	$1021, %edx
	call	__assert_fail
.L656:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC33(%rip), %rdi
	movl	$1397, %edx
	call	__assert_fail
.L676:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC24(%rip), %rdi
	movl	$958, %edx
	call	__assert_fail
.L694:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$926, %edx
	call	__assert_fail
.L236:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC27(%rip), %rdi
	movl	$1076, %edx
	call	__assert_fail
.L384:
	leaq	__PRETTY_FUNCTION__.11872(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC26(%rip), %rdi
	movl	$1072, %edx
	call	__assert_fail
	.size	____strtod_l_internal, .-____strtod_l_internal
	.p2align 4,,15
	.weak	__strtod_l
	.hidden	__strtod_l
	.type	__strtod_l, @function
__strtod_l:
	movq	%rdx, %rcx
	xorl	%edx, %edx
	jmp	____strtod_l_internal
	.size	__strtod_l, .-__strtod_l
	.weak	strtod_l
	.set	strtod_l,__strtod_l
	.weak	strtof32x_l
	.set	strtof32x_l,strtod_l
	.weak	strtof64_l
	.set	strtof64_l,strtod_l
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11822, @object
	.size	__PRETTY_FUNCTION__.11822, 11
__PRETTY_FUNCTION__.11822:
	.string	"str_to_mpn"
	.section	.rodata
	.align 32
	.type	nbits.11960, @object
	.size	nbits.11960, 64
nbits.11960:
	.long	0
	.long	1
	.long	2
	.long	2
	.long	3
	.long	3
	.long	3
	.long	3
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.long	4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11872, @object
	.size	__PRETTY_FUNCTION__.11872, 22
__PRETTY_FUNCTION__.11872:
	.string	"____strtod_l_internal"
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1048576
	.align 8
.LC1:
	.long	0
	.long	-2146435072
	.align 8
.LC2:
	.long	4294967295
	.long	2146435071
	.align 8
.LC3:
	.long	4294967295
	.long	-1048577
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC8:
	.long	0
	.long	2146959360
	.align 8
.LC10:
	.long	0
	.long	-2147483648
	.align 8
.LC11:
	.long	0
	.long	2146435072
	.align 8
.LC12:
	.long	0
	.long	-1048576
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC17:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.hidden	__strtod_nan
	.hidden	__mpn_add_n
	.hidden	__mpn_submul_1
	.hidden	__mpn_cmp
	.hidden	__mpn_lshift
	.hidden	__correctly_grouped_prefixmb
	.hidden	__strncasecmp_l
	.hidden	__mpn_mul
	.hidden	__tens
	.hidden	_fpioconst_pow10
	.hidden	_nl_C_locobj
	.hidden	strlen
	.hidden	__assert_fail
	.hidden	__mpn_mul_1
	.hidden	__mpn_construct_double
	.hidden	__mpn_rshift
	.hidden	abort
