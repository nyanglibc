	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__cxa_at_quick_exit
	.type	__cxa_at_quick_exit, @function
__cxa_at_quick_exit:
	leaq	__quick_exit_funcs(%rip), %rcx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	__internal_atexit
	.size	__cxa_at_quick_exit, .-__cxa_at_quick_exit
	.hidden	__quick_exit_funcs
	.globl	__quick_exit_funcs
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__quick_exit_funcs, @object
	.size	__quick_exit_funcs, 8
__quick_exit_funcs:
	.quad	initial_quick
	.local	initial_quick
	.comm	initial_quick,1040,32
	.hidden	__internal_atexit
