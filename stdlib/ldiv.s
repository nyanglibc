	.text
	.p2align 4,,15
	.globl	ldiv
	.type	ldiv, @function
ldiv:
	movq	%rdi, %rax
	cqto
	idivq	%rsi
	ret
	.size	ldiv, .-ldiv
	.weak	imaxdiv
	.set	imaxdiv,ldiv
