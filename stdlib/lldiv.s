	.text
	.p2align 4,,15
	.globl	lldiv
	.type	lldiv, @function
lldiv:
	movq	%rdi, %rax
	cqto
	idivq	%rsi
	ret
	.size	lldiv, .-lldiv
