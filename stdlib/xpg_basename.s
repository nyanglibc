	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"."
	.text
	.p2align 4,,15
	.globl	__xpg_basename
	.type	__xpg_basename, @function
__xpg_basename:
	testq	%rdi, %rdi
	leaq	.LC0(%rip), %rax
	je	.L20
	cmpb	$0, (%rdi)
	je	.L20
	pushq	%rbx
	movl	$47, %esi
	movq	%rdi, %rbx
	call	strrchr
	testq	%rax, %rax
	je	.L12
	cmpb	$0, 1(%rax)
	jne	.L3
	cmpq	%rax, %rbx
	jb	.L24
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	subq	$1, %rax
	cmpq	%rax, %rbx
	je	.L23
.L24:
	cmpb	$47, -1(%rax)
	je	.L26
	movq	%rax, %rdx
	subq	$1, %rax
	cmpq	%rax, %rbx
	movb	$0, (%rdx)
	jnb	.L1
	cmpb	$47, -2(%rdx)
	jne	.L8
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	cmpb	$47, -1(%rax)
	je	.L1
.L8:
	subq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L27
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	rep ret
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$1, %rax
.L23:
	cmpb	$0, 1(%rax)
	jne	.L7
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	__xpg_basename, .-__xpg_basename
	.hidden	strrchr
