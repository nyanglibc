	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtod_internal
	.hidden	__GI___strtod_internal
	.type	__GI___strtod_internal, @function
__GI___strtod_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtod_l_internal
	.size	__GI___strtod_internal, .-__GI___strtod_internal
	.globl	__strtod_internal
	.set	__strtod_internal,__GI___strtod_internal
	.p2align 4,,15
	.weak	__GI_strtod
	.hidden	__GI_strtod
	.type	__GI_strtod, @function
__GI_strtod:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtod_l_internal
	.size	__GI_strtod, .-__GI_strtod
	.globl	strtod
	.set	strtod,__GI_strtod
	.weak	strtof32x
	.set	strtof32x,strtod
	.weak	strtof64
	.set	strtof64,strtod
