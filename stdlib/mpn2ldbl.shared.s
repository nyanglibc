	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mpn_construct_long_double
	.hidden	__mpn_construct_long_double
	.type	__mpn_construct_long_double, @function
__mpn_construct_long_double:
	movl	%edx, %eax
	movzbl	-15(%rsp), %edx
	addw	$16383, %si
	sall	$7, %eax
	andl	$127, %edx
	orl	%eax, %edx
	movl	%esi, %eax
	movb	%dl, -15(%rsp)
	movzwl	-16(%rsp), %esi
	andw	$32767, %ax
	andw	$-32768, %si
	orl	%eax, %esi
	movq	(%rdi), %rax
	movw	%si, -16(%rsp)
	movl	%eax, -24(%rsp)
	shrq	$32, %rax
	movl	%eax, -20(%rsp)
	fldt	-24(%rsp)
	ret
	.size	__mpn_construct_long_double, .-__mpn_construct_long_double
