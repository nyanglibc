	.text
	.p2align 4,,15
	.globl	__makecontext
	.type	__makecontext, @function
__makecontext:
	pushq	%rbx
	movq	$-8, %rax
	movq	%rcx, -24(%rsp)
	movq	32(%rdi), %rcx
	addq	16(%rdi), %rcx
	cmpl	$6, %edx
	movq	%r8, -16(%rsp)
	movq	%r9, -8(%rsp)
	jle	.L2
	leal	-6(%rdx), %r8d
	movslq	%r8d, %r8
	salq	$3, %r8
	subq	%r8, %rax
.L2:
	addq	%rax, %rcx
	movl	$6, %eax
	movq	%rsi, 168(%rdi)
	andq	$-16, %rcx
	cmpl	$6, %edx
	movq	8(%rdi), %rsi
	cmovge	%edx, %eax
	leaq	-8(%rcx), %r8
	leaq	__start_context(%rip), %rbx
	subl	$5, %eax
	testl	%edx, %edx
	movl	$24, -72(%rsp)
	cltq
	movq	%r8, 160(%rdi)
	leaq	(%r8,%rax,8), %rax
	movq	%rax, 128(%rdi)
	movq	%rbx, -8(%rcx)
	movq	%rsi, (%rax)
	leaq	16(%rsp), %rax
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movq	%rax, -56(%rsp)
	jle	.L1
	leal	-1(%rdx), %r8d
	leaq	16(%rsp), %rbx
	leaq	.L6(%rip), %rsi
	movq	%rax, %r11
	movl	$24, %r9d
	xorl	%eax, %eax
	addq	$1, %r8
	subq	$48, %rcx
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	$5, %eax
	ja	.L4
	movl	%eax, %edx
	movslq	(%rsi,%rdx,4), %rdx
	addq	%rsi, %rdx
	cmpl	$47, %r9d
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L5-.L6
	.long	.L7-.L6
	.long	.L8-.L6
	.long	.L9-.L6
	.long	.L10-.L6
	.long	.L11-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	$47, %r9d
	leaq	(%rcx,%rax,8), %rdx
	ja	.L25
	movl	%r9d, %r10d
	addl	$8, %r9d
	addq	%r11, %r10
.L26:
	movq	(%r10), %r10
	movq	%r10, (%rdx)
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$1, %rax
	cmpq	%rax, %r8
	jne	.L27
.L1:
	popq	%rbx
	ret
.L10:
	ja	.L21
	movl	%r9d, %edx
	addl	$8, %r9d
	addq	%r11, %rdx
.L22:
	movq	(%rdx), %rdx
	movq	%rdx, 40(%rdi)
	jmp	.L14
.L9:
	ja	.L19
	movl	%r9d, %edx
	addl	$8, %r9d
	addq	%r11, %rdx
.L20:
	movq	(%rdx), %rdx
	movq	%rdx, 152(%rdi)
	jmp	.L14
.L8:
	ja	.L17
	movl	%r9d, %edx
	addl	$8, %r9d
	addq	%r11, %rdx
.L18:
	movq	(%rdx), %rdx
	movq	%rdx, 136(%rdi)
	jmp	.L14
.L5:
	ja	.L12
	movl	%r9d, %edx
	addl	$8, %r9d
	addq	%r11, %rdx
.L13:
	movq	(%rdx), %rdx
	movq	%rdx, 104(%rdi)
	jmp	.L14
.L7:
	ja	.L15
	movl	%r9d, %edx
	addl	$8, %r9d
	addq	%r11, %rdx
.L16:
	movq	(%rdx), %rdx
	movq	%rdx, 112(%rdi)
	jmp	.L14
.L11:
	ja	.L23
	movl	%r9d, %edx
	addl	$8, %r9d
	addq	%r11, %rdx
.L24:
	movq	(%rdx), %rdx
	movq	%rdx, 48(%rdi)
	jmp	.L14
.L25:
	movq	%rbx, %r10
	addq	$8, %rbx
	jmp	.L26
.L21:
	movq	%rbx, %rdx
	addq	$8, %rbx
	jmp	.L22
.L23:
	movq	%rbx, %rdx
	addq	$8, %rbx
	jmp	.L24
.L19:
	movq	%rbx, %rdx
	addq	$8, %rbx
	jmp	.L20
.L12:
	movq	%rbx, %rdx
	addq	$8, %rbx
	jmp	.L13
.L17:
	movq	%rbx, %rdx
	addq	$8, %rbx
	jmp	.L18
.L15:
	movq	%rbx, %rdx
	addq	$8, %rbx
	jmp	.L16
	.size	__makecontext, .-__makecontext
	.weak	makecontext
	.set	makecontext,__makecontext
	.hidden	__start_context
