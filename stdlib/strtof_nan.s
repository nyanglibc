	.text
	.p2align 4,,15
	.globl	__strtof_nan
	.hidden	__strtof_nan
	.type	__strtof_nan, @function
__strtof_nan:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rbx
.L2:
	movzbl	(%rbx), %ecx
	movl	%ecx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L3
	leal	-48(%rcx), %eax
	cmpb	$9, %al
	jbe	.L3
	cmpb	$95, %cl
	je	.L3
	cmpb	%dl, %cl
	je	.L4
.L6:
	movss	.LC0(%rip), %xmm0
.L5:
	testq	%rbp, %rbp
	je	.L1
	movq	%rbx, 0(%rbp)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L4:
	leaq	24(%rsp), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	____strtoull_l_internal
	cmpq	%rbx, 24(%rsp)
	jne	.L6
	andl	$4194303, %eax
	orl	$2143289344, %eax
	movl	%eax, 12(%rsp)
	movss	12(%rsp), %xmm0
	jmp	.L5
	.size	__strtof_nan, .-__strtof_nan
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC0:
	.long	2143289344
	.hidden	____strtoull_l_internal
	.hidden	_nl_C_locobj
