.globl __setcontext
.type __setcontext,@function
.align 1<<4
__setcontext:
	pushq %rdi
	leaq 296(%rdi), %rsi
	xorl %edx, %edx
	movl $2, %edi
	movl $8,%r10d
	movl $14, %eax
	syscall
	popq %rdx
	cmpq $-4095, %rax
	jae 0f
	movq 224(%rdx), %rcx
	fldenv (%rcx)
	ldmxcsr 448(%rdx)
	movq 160(%rdx), %rsp
	movq 128(%rdx), %rbx
	movq 120(%rdx), %rbp
	movq 72(%rdx), %r12
	movq 80(%rdx), %r13
	movq 88(%rdx), %r14
	movq 96(%rdx), %r15
	movq 168(%rdx), %rcx
	pushq %rcx
	movq 112(%rdx), %rsi
	movq 104(%rdx), %rdi
	movq 152(%rdx), %rcx
	movq 40(%rdx), %r8
	movq 48(%rdx), %r9
	movq 136(%rdx), %rdx
	xorl %eax, %eax
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __setcontext,.-__setcontext
.weak setcontext
setcontext = __setcontext
