	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_wctomb
	.hidden	__GI_wctomb
	.type	__GI_wctomb, @function
__GI_wctomb:
	testq	%rdi, %rdi
	pushq	%rbx
	je	.L9
	leaq	__wctomb_state(%rip), %rdx
	call	__wcrtomb
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L10
.L4:
	movq	16(%rax), %rax
	movq	$0, __wctomb_state(%rip)
	popq	%rbx
	movl	88(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L6
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L4
.L6:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L4
	.size	__GI_wctomb, .-__GI_wctomb
	.globl	wctomb
	.set	wctomb,__GI_wctomb
	.hidden	__wctomb_state
	.comm	__wctomb_state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
	.hidden	__wcrtomb
