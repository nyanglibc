 .text
.globl __mpn_submul_1
.type __mpn_submul_1,@function
.align 1<<4
__mpn_submul_1:
 push %rbx
 push %rbp
 lea (%rdx), %rbx
 neg %rbx
 mov (%rsi), %rax
 mov (%rdi), %r10
 lea -16(%rdi,%rdx,8), %rdi
 lea (%rsi,%rdx,8), %rsi
 mul %rcx
 bt $0, %ebx
 jc .Lodd
 lea (%rax), %r11
 mov 8(%rsi,%rbx,8), %rax
 lea (%rdx), %rbp
 mul %rcx
 add $2, %rbx
 jns .Ln2
 lea (%rax), %r8
 mov (%rsi,%rbx,8), %rax
 lea (%rdx), %r9
 jmp .Lmid
.Lodd: add $1, %rbx
 jns .Ln1
 lea (%rax), %r8
 mov (%rsi,%rbx,8), %rax
 lea (%rdx), %r9
 mul %rcx
 lea (%rax), %r11
 mov 8(%rsi,%rbx,8), %rax
 lea (%rdx), %rbp
 jmp .Le
 .p2align 4
.Ltop: mul %rcx
 sub %r8, %r10
 lea (%rax), %r8
 mov (%rsi,%rbx,8), %rax
 adc %r9, %r11
 mov %r10, -8(%rdi,%rbx,8)
 mov (%rdi,%rbx,8), %r10
 lea (%rdx), %r9
 adc $0, %rbp
.Lmid: mul %rcx
 sub %r11, %r10
 lea (%rax), %r11
 mov 8(%rsi,%rbx,8), %rax
 adc %rbp, %r8
 mov %r10, (%rdi,%rbx,8)
 mov 8(%rdi,%rbx,8), %r10
 lea (%rdx), %rbp
 adc $0, %r9
.Le: add $2, %rbx
 js .Ltop
 mul %rcx
 sub %r8, %r10
 adc %r9, %r11
 mov %r10, -8(%rdi)
 adc $0, %rbp
.Ln2: mov (%rdi), %r10
 sub %r11, %r10
 adc %rbp, %rax
 mov %r10, (%rdi)
 adc $0, %rdx
.Ln1: mov 8(%rdi), %r10
 sub %rax, %r10
 mov %r10, 8(%rdi)
 mov %ebx, %eax
 adc %rdx, %rax
 pop %rbp
 pop %rbx
 ret
.size __mpn_submul_1,.-__mpn_submul_1
