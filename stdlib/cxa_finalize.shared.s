	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__cxa_finalize
	.type	__cxa_finalize, @function
__cxa_finalize:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
#APP
# 33 "cxa_finalize.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	__exit_funcs_lock@GOTPCREL(%rip), %r13
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%r13)
# 0 "" 2
#NO_APP
.L12:
	movq	__exit_funcs(%rip), %r15
	testq	%r15, %r15
	je	.L4
.L14:
	movq	8(%r15), %rax
	leaq	16(%r15), %r12
	salq	$5, %rax
	leaq	-16(%r15,%rax), %rbx
	cmpq	%rbx, %r12
	jbe	.L13
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L48:
	cmpq	%rbp, 24(%rbx)
	je	.L6
.L7:
	subq	$32, %rbx
	cmpq	%rbx, %r12
	ja	.L5
.L13:
	testq	%rbp, %rbp
	jne	.L48
.L6:
	cmpq	$4, (%rbx)
	jne	.L7
	movq	__new_exitfn_called(%rip), %r14
	movq	8(%rbx), %r8
	movq	16(%rbx), %r9
	movq	$0, (%rbx)
#APP
# 79 "cxa_finalize.c" 1
	ror $2*8+1, %r8
xor %fs:48, %r8
# 0 "" 2
# 82 "cxa_finalize.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L8
	subl	$1, 0(%r13)
.L9:
	xorl	%esi, %esi
	movq	%r9, %rdi
	call	*%r8
#APP
# 84 "cxa_finalize.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	movl	$1, %edi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edi, 0(%r13)
# 0 "" 2
#NO_APP
.L11:
	cmpq	%r14, __new_exitfn_called(%rip)
	jne	.L12
	subq	$32, %rbx
	cmpq	%rbx, %r12
	jbe	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L14
.L4:
	movq	__quick_exit_funcs(%rip), %rsi
	testq	%rsi, %rsi
	je	.L15
	movl	$31, %edi
	.p2align 4,,10
	.p2align 3
.L20:
	movq	8(%rsi), %rax
	salq	$5, %rax
	leaq	-16(%rsi,%rax), %rdx
	leaq	16(%rsi), %rax
	cmpq	%rax, %rdx
	jb	.L16
	leaq	15(%rsi), %rax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	leaq	-32(%rdx), %rax
	addq	%rax, %rcx
	notq	%rcx
	andq	$-32, %rcx
	addq	%rdx, %rcx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L49:
	cmpq	%rbp, 24(%rdx)
	je	.L17
	cmpq	%rax, %rcx
	movq	%rax, %rdx
	je	.L16
.L50:
	subq	$32, %rax
.L19:
	testq	%rbp, %rbp
	jne	.L49
.L17:
	cmpq	%rax, %rcx
	movq	$0, (%rdx)
	movq	%rax, %rdx
	jne	.L50
.L16:
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.L20
.L15:
	testq	%rbp, %rbp
	je	.L21
	movq	%rbp, %rdi
	call	__unregister_atfork
.L21:
#APP
# 109 "cxa_finalize.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L22
	subl	$1, 0(%r13)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, 0(%r13)
	je	.L11
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
#APP
# 82 "cxa_finalize.c" 1
	xchgl %eax, 0(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L9
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 82 "cxa_finalize.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L9
.L22:
	xorl	%eax, %eax
#APP
# 109 "cxa_finalize.c" 1
	xchgl %eax, 0(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 109 "cxa_finalize.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%r13)
	je	.L12
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L12
	.size	__cxa_finalize, .-__cxa_finalize
	.hidden	__lll_lock_wait_private
	.hidden	__unregister_atfork
	.hidden	__quick_exit_funcs
	.hidden	__new_exitfn_called
	.hidden	__exit_funcs
