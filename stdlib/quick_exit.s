	.text
	.p2align 4,,15
	.globl	__new_quick_exit
	.type	__new_quick_exit, @function
__new_quick_exit:
	leaq	__quick_exit_funcs(%rip), %rsi
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__run_exit_handlers
	.size	__new_quick_exit, .-__new_quick_exit
	.weak	quick_exit
	.set	quick_exit,__new_quick_exit
	.hidden	__run_exit_handlers
	.hidden	__quick_exit_funcs
