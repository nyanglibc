	.text
	.p2align 4,,15
	.globl	atoll
	.type	atoll, @function
atoll:
	movl	$10, %edx
	xorl	%esi, %esi
	jmp	strtoll
	.size	atoll, .-atoll
	.hidden	strtoll
