	.text
	.p2align 4,,15
	.globl	srand48
	.type	srand48, @function
srand48:
	leaq	__libc_drand48_data(%rip), %rsi
	jmp	__srand48_r
	.size	srand48, .-srand48
	.hidden	__srand48_r
	.hidden	__libc_drand48_data
