	.text
	.p2align 4,,15
	.globl	rand
	.type	rand, @function
rand:
	subq	$8, %rsp
	call	__random
	addq	$8, %rsp
	ret
	.size	rand, .-rand
	.hidden	__random
