	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"sh"
.LC1:
	.string	"-c"
.LC2:
	.string	"/bin/sh"
	.text
	.p2align 4,,15
	.type	do_system, @function
do_system:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$872, %rsp
	movl	$-1, 8(%rsp)
	movq	$1, 368(%rsp)
	movl	$0, 504(%rsp)
	movq	$0, 376(%rsp)
#APP
# 118 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movl	sa_refcntr(%rip), %eax
	leaq	368(%rsp), %rbx
	leal	1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, sa_refcntr(%rip)
	je	.L34
.L4:
#APP
# 125 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	subl	$1, lock(%rip)
.L6:
	leaq	112(%rsp), %r12
	leaq	8(%rbx), %rsi
	xorl	%edi, %edi
	orq	$65536, 376(%rsp)
	movq	%r12, %rdx
	call	__sigprocmask
	cmpq	$1, intr(%rip)
	movq	$0, 240(%rsp)
	je	.L7
	movq	$2, 240(%rsp)
.L7:
	cmpq	$1, quit(%rip)
	je	.L8
	orq	$4, 240(%rsp)
.L8:
	leaq	528(%rsp), %rbx
	movq	%rbx, %rdi
	call	__posix_spawnattr_init
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__posix_spawnattr_setsigmask
	leaq	240(%rsp), %rsi
	movq	%rbx, %rdi
	call	__posix_spawnattr_setsigdefault
	movl	$12, %esi
	movq	%rbx, %rdi
	call	__posix_spawnattr_setflags
	leaq	.LC0(%rip), %rax
	movq	__environ(%rip), %r9
	leaq	12(%rsp), %rdi
	leaq	48(%rsp), %r8
	leaq	.LC2(%rip), %rsi
	xorl	%edx, %edx
	movq	%rax, 48(%rsp)
	leaq	.LC1(%rip), %rax
	movq	%rbx, %rcx
	movq	%rbp, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	%rax, 56(%rsp)
	call	__posix_spawn
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__posix_spawnattr_destroy
	testl	%ebp, %ebp
	jne	.L9
	leaq	quit(%rip), %rax
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %r13
	movl	12(%rsp), %edi
	movq	%rax, 16(%rsp)
	leaq	intr(%rip), %rax
	testq	%r13, %r13
	movl	%edi, 32(%rsp)
	movq	%rax, 24(%rsp)
	je	.L10
	leaq	80(%rsp), %rdi
	leaq	16(%rsp), %rdx
	leaq	cancel_handler(%rip), %rsi
	call	_pthread_cleanup_push_defer@PLT
	movl	12(%rsp), %edi
.L11:
	leaq	8(%rsp), %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L35:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L14
.L13:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	__waitpid
	cmpl	$-1, %eax
	movl	12(%rsp), %edi
	je	.L35
.L14:
	cmpl	%edi, %eax
	je	.L15
	movl	$-1, 8(%rsp)
.L15:
	testq	%r13, %r13
	je	.L17
	leaq	80(%rsp), %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$32512, 8(%rsp)
.L17:
#APP
# 183 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L18
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L19:
	subl	$1, sa_refcntr(%rip)
	je	.L36
.L20:
#APP
# 192 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L21
	subl	$1, lock(%rip)
.L22:
	testl	%ebp, %ebp
	je	.L23
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%ebp, %fs:(%rax)
.L23:
	movl	8(%rsp), %eax
	addq	$872, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	cancel_handler(%rip), %rax
	movq	%rax, 80(%rsp)
	leaq	16(%rsp), %rax
	movq	%rax, 88(%rsp)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	intr(%rip), %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	__sigaction
	leaq	quit(%rip), %rdx
	movq	%rbx, %rsi
	movl	$3, %edi
	call	__sigaction
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	intr(%rip), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__sigaction
	leaq	quit(%rip), %rsi
	xorl	%edx, %edx
	movl	$3, %edi
	call	__sigaction
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	call	__sigprocmask
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L19
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
#APP
# 125 "../sysdeps/posix/system.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L6
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 125 "../sysdeps/posix/system.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
#APP
# 192 "../sysdeps/posix/system.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L22
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 192 "../sysdeps/posix/system.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L22
	.size	do_system, .-do_system
	.p2align 4,,15
	.type	cancel_handler, @function
cancel_handler:
	pushq	%rbp
	pushq	%rbx
	movl	$9, %esi
	movq	%rdi, %rbx
	movl	$62, %eax
	subq	$24, %rsp
	movl	16(%rdi), %edi
#APP
# 43 "../sysdeps/unix/sysv/linux/not-errno.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	__pthread_setcancelstate@GOTPCREL(%rip), %rbp
	testq	%rbp, %rbp
	je	.L40
	leaq	12(%rsp), %rsi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L55:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L39
.L40:
	movl	16(%rbx), %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	__waitpid
	cmpl	$-1, %eax
	je	.L55
.L39:
	testq	%rbp, %rbp
	je	.L41
	movl	12(%rsp), %edi
	xorl	%esi, %esi
	call	__pthread_setcancelstate@PLT
.L41:
#APP
# 90 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L42
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L43:
	subl	$1, sa_refcntr(%rip)
	je	.L56
.L44:
#APP
# 96 "../sysdeps/posix/system.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L45
	subl	$1, lock(%rip)
.L37:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rbx), %rsi
	xorl	%edx, %edx
	movl	$3, %edi
	call	__sigaction
	movq	8(%rbx), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__sigaction
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L43
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
#APP
# 96 "../sysdeps/posix/system.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L37
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 96 "../sysdeps/posix/system.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L37
	.size	cancel_handler, .-cancel_handler
	.section	.rodata.str1.1
.LC3:
	.string	"exit 0"
	.text
	.p2align 4,,15
	.globl	__libc_system
	.type	__libc_system, @function
__libc_system:
	testq	%rdi, %rdi
	je	.L61
	jmp	do_system
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC3(%rip), %rdi
	subq	$8, %rsp
	call	do_system
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	__libc_system, .-__libc_system
	.weak	system
	.set	system,__libc_system
	.local	lock
	.comm	lock,4,4
	.local	sa_refcntr
	.comm	sa_refcntr,4,4
	.local	quit
	.comm	quit,152,32
	.local	intr
	.comm	intr,152,32
	.weak	__pthread_setcancelstate
	.weak	_pthread_cleanup_pop_restore
	.weak	_pthread_cleanup_push_defer
	.hidden	__lll_lock_wait_private
	.hidden	__sigaction
	.hidden	__waitpid
	.hidden	__posix_spawnattr_destroy
	.hidden	__posix_spawn
	.hidden	__posix_spawnattr_setflags
	.hidden	__posix_spawnattr_setsigdefault
	.hidden	__posix_spawnattr_setsigmask
	.hidden	__posix_spawnattr_init
	.hidden	__sigprocmask
