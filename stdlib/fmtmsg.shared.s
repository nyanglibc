	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
.LFB71:
	movq	severity_list(%rip), %rdi
	testq	%rdi, %rdi
	je	.L10
	pushq	%rbx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	je	.L13
.L4:
	cmpl	$4, (%rdi)
	movq	16(%rdi), %rbx
	jle	.L3
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L4
.L13:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	rep ret
.LFE71:
	.size	free_mem, .-free_mem
	.text
	.p2align 4,,15
	.type	internal_addseverity, @function
internal_addseverity:
.LFB69:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$24, %rsp
	movq	severity_list(%rip), %rbp
	testq	%rbp, %rbp
	je	.L15
	cmpl	0(%rbp), %edi
	je	.L16
	movq	%rbp, %rax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	cmpl	%ebx, (%rdi)
	je	.L18
	movq	%rdi, %rax
.L17:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L19
.L15:
	testq	%rsi, %rsi
	je	.L26
	movl	$24, %edi
	movq	%rsi, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	8(%rsp), %rsi
	je	.L26
	movl	%ebx, (%rax)
	movq	%rbp, 16(%rax)
	movq	%rsi, 8(%rax)
	movq	%rax, severity_list(%rip)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%rsi, %rsi
	je	.L33
.L22:
	movq	%rsi, 8(%rdi)
	xorl	%eax, %eax
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movq	16(%rdi), %rdx
	movq	%rdx, 16(%rax)
.L23:
	call	free@PLT
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	testq	%rsi, %rsi
	movq	%rbp, %rdi
	jne	.L22
	movq	16(%rbp), %rax
	movq	%rax, severity_list(%rip)
	jmp	.L23
.L26:
	movl	$-1, %eax
	jmp	.L14
.LFE69:
	.size	internal_addseverity, .-internal_addseverity
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"MSGVERB"
.LC1:
	.string	"SEV_LEVEL"
	.text
	.p2align 4,,15
	.type	init, @function
init:
.LFB68:
	pushq	%r15
	pushq	%r14
	leaq	.LC0(%rip), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	call	__GI_getenv
	leaq	.LC1(%rip), %rdi
	movq	%rax, %rbp
	call	__GI_getenv
	testq	%rbp, %rbp
	movq	%rax, %r12
	je	.L35
	cmpb	$0, 0(%rbp)
	jne	.L86
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$31, print(%rip)
.L40:
	testq	%r12, %r12
	je	.L34
#APP
# 254 "fmtmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L43
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L44:
	cmpb	$0, (%r12)
	leaq	8(%rsp), %r13
	je	.L56
	.p2align 4,,10
	.p2align 3
.L53:
	movl	$58, %esi
	movq	%r12, %rdi
	call	__strchrnul@PLT
	cmpq	%rax, %r12
	movq	%rax, %rbp
	jnb	.L45
	cmpb	$44, (%r12)
	leaq	1(%r12), %rbx
	jne	.L47
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L48:
	addq	$1, %rbx
	cmpb	$44, -1(%rbx)
	je	.L46
.L47:
	cmpq	%rbx, %rbp
	jne	.L48
.L45:
	xorl	%r12d, %r12d
	cmpb	$58, 0(%rbp)
	sete	%r12b
	addq	%rbp, %r12
	cmpb	$0, (%r12)
	jne	.L53
.L56:
#APP
# 290 "fmtmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L87
	subl	$1, lock(%rip)
.L34:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	cmpq	%rbx, %rbp
	jbe	.L45
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__GI_strtol
	movq	%rax, %r12
	movq	8(%rsp), %rax
	cmpq	%rbx, %rax
	je	.L45
	cmpq	%rbp, %rax
	jnb	.L45
	leaq	1(%rax), %rdi
	movq	%rdi, 8(%rsp)
	cmpb	$44, (%rax)
	jne	.L45
	cmpl	$4, %r12d
	jle	.L45
	movq	%rbp, %rsi
	subq	%rdi, %rsi
	call	__GI___strndup
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L45
	movq	%rax, %rsi
	movl	%r12d, %edi
	call	internal_addseverity
	testl	%eax, %eax
	je	.L45
	movq	%rbx, %rdi
	call	free@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	4+keywords(%rip), %r14
	movl	$5, %ebx
	xorl	%r15d, %r15d
.L38:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	jne	.L36
	addq	%rbp, %rbx
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	je	.L37
	testb	%al, %al
	je	.L37
.L36:
	addq	$1, %r15
	addq	$16, %r14
	cmpq	$5, %r15
	je	.L35
	movl	-4(%r14), %ebx
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%r13d, %edx
	movl	%r15d, %ecx
	movq	%rbx, %rbp
	sall	%cl, %edx
	orl	%edx, print(%rip)
	cmpb	$58, %al
	jne	.L59
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %rbp
.L59:
	testb	%al, %al
	jne	.L39
	jmp	.L40
.L43:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L44
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L44
.L87:
	xorl	%eax, %eax
#APP
# 290 "fmtmsg.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L34
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 290 "fmtmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L34
.LFE68:
	.size	init, .-init
	.section	.rodata.str1.1
.LC2:
	.string	"TO FIX: "
.LC3:
	.string	""
.LC4:
	.string	": "
.LC5:
	.string	"\n"
.LC6:
	.string	"  "
.LC7:
	.string	"%s%s%s%s%s%s%s%s%s%s\n"
	.text
	.p2align 4,,15
	.globl	fmtmsg
	.type	fmtmsg, @function
fmtmsg:
.LFB67:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rcx, %r14
	movq	%r8, %r12
	subq	$56, %rsp
	movl	__libc_pthread_functions_init(%rip), %esi
	movq	%r9, (%rsp)
	testl	%esi, %esi
	je	.L89
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	init(%rip), %rsi
	leaq	once.9254(%rip), %rdi
#APP
# 108 "fmtmsg.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L90:
	testq	%rbx, %rbx
	je	.L91
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L130
	movq	%rax, %rdx
	subq	%rbx, %rdx
	cmpq	$10, %rdx
	jg	.L130
	leaq	1(%rax), %rdi
	call	__GI_strlen
	cmpq	$14, %rax
	ja	.L130
.L91:
	movl	__libc_pthread_functions_init(%rip), %edx
	movl	$0, 44(%rsp)
	testl	%edx, %edx
	je	.L93
	movq	104+__libc_pthread_functions(%rip), %rax
	leaq	44(%rsp), %rsi
	movl	$1, %edi
#APP
# 129 "fmtmsg.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L93:
#APP
# 133 "fmtmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L94
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L95:
	movq	severity_list(%rip), %r10
	movl	$-1, %ebp
	testq	%r10, %r10
	jne	.L99
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L176:
	movq	16(%r10), %r10
	testq	%r10, %r10
	je	.L175
.L99:
	cmpl	%r13d, (%r10)
	jne	.L176
	xorl	%ebp, %ebp
	testl	$256, %r15d
	je	.L121
	movl	print(%rip), %ecx
	testq	%rbx, %rbx
	setne	%al
	movl	%ecx, %edx
	andl	%ecx, %eax
	shrl	%edx
	testl	%r13d, %r13d
	setne	%r9b
	andl	%edx, %r9d
	movl	%ecx, %edx
	shrl	$2, %edx
	testq	%r14, %r14
	setne	%sil
	andl	%edx, %esi
	movl	%ecx, %edx
	shrl	$3, %edx
	testq	%r12, %r12
	setne	%dil
	andl	%edi, %edx
	andl	$16, %ecx
	je	.L153
	cmpq	$0, (%rsp)
	je	.L153
	testb	%dl, %dl
	movzbl	%al, %ecx
	je	.L177
	testb	%sil, %sil
	je	.L178
	leaq	.LC2(%rip), %rdx
	movq	(%rsp), %rbp
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdi
	movq	%rdx, 8(%rsp)
.L104:
	testb	%r9b, %r9b
	movq	%r14, 24(%rsp)
	leaq	.LC5(%rip), %r11
	je	.L179
.L173:
	leaq	.LC4(%rip), %r9
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L153:
	xorl	%ecx, %ecx
	testb	%al, %al
	je	.L102
	movl	%esi, %ecx
	orl	%edx, %ecx
	orl	%r9d, %ecx
	movzbl	%cl, %ecx
.L102:
	testb	%dl, %dl
	jne	.L180
	leaq	.LC3(%rip), %rdi
	movq	%rdi, 8(%rsp)
.L124:
	testb	%sil, %sil
	jne	.L181
	leaq	.LC3(%rip), %rbp
	xorl	%r8d, %r8d
	movq	%rbp, 24(%rsp)
.L103:
	testb	%r9b, %r9b
	je	.L135
	orl	%esi, %edx
	leaq	.LC3(%rip), %rsi
	movzbl	%dl, %edx
	orl	%r8d, %edx
	movq	%rsi, %r11
	movq	%rsi, %r9
	jne	.L173
.L106:
	movq	8(%r10), %r8
.L105:
	testl	%ecx, %ecx
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rcx
	movq	%r10, 16(%rsp)
	pushq	%rbp
	cmove	%rdx, %rcx
	testb	%al, %al
	movq	stderr@GOTPCREL(%rip), %rax
	pushq	%rsi
	pushq	%rdi
	leaq	.LC7(%rip), %rsi
	pushq	32(%rsp)
	pushq	%r11
	cmovne	%rbx, %rdx
	movq	(%rax), %rdi
	pushq	64(%rsp)
	xorl	%eax, %eax
	call	__fxprintf
	addq	$48, %rsp
	movq	16(%rsp), %r10
	shrl	$31, %eax
	movl	%eax, %ebp
.L121:
	testl	$512, %r15d
	jne	.L182
.L96:
#APP
# 198 "fmtmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L118
	subl	$1, lock(%rip)
.L119:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L88
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	44(%rsp), %edi
#APP
# 201 "fmtmsg.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L88:
	addq	$56, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	testq	%r14, %r14
	setne	%sil
	testq	%r12, %r12
	setne	%dl
	cmpq	$0, (%rsp)
	setne	%cl
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L109
	movl	%edx, %eax
	orl	%ecx, %eax
	orl	%esi, %eax
	testl	%r13d, %r13d
	setne	%sil
	orl	%esi, %eax
	movzbl	%al, %eax
.L109:
	cmpq	$0, (%rsp)
	je	.L110
	testq	%r12, %r12
	leaq	.LC6(%rip), %r15
	leaq	.LC2(%rip), %rdi
	je	.L183
.L111:
	testq	%r14, %r14
	jne	.L184
.L127:
	leaq	.LC3(%rip), %rsi
	testl	%r13d, %r13d
	movq	%rsi, %r14
	je	.L142
	testb	%dl, %dl
	jne	.L174
	testb	%cl, %cl
	jne	.L174
	movq	%rsi, %r9
.L114:
	movq	8(%r10), %r8
.L113:
	leaq	.LC3(%rip), %rdx
	leaq	.LC4(%rip), %rcx
	testl	%eax, %eax
	pushq	(%rsp)
	pushq	%r15
	cmove	%rdx, %rcx
	testq	%rbx, %rbx
	pushq	%r12
	cmove	%rdx, %rbx
	pushq	%rdi
	pushq	%rsi
	leaq	.LC7(%rip), %rsi
	pushq	%r14
	movq	%rbx, %rdx
	movl	$3, %edi
	xorl	%eax, %eax
	call	__GI_syslog
	addq	$48, %rsp
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L175:
	movl	$-1, %ebp
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	.LC3(%rip), %r9
	testl	%r13d, %r13d
	leaq	.LC5(%rip), %rsi
	movq	%r9, %r8
	je	.L113
.L174:
	leaq	.LC4(%rip), %r9
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L181:
	testb	%dl, %dl
	jne	.L134
	movq	%r14, 24(%rsp)
	leaq	.LC3(%rip), %rbp
	xorl	%r8d, %r8d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L183:
	leaq	.LC3(%rip), %r12
	movq	%r12, %r15
	movq	%r12, %rdi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L89:
	movl	once.9254(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L90
	call	init
	orl	$2, once.9254(%rip)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L110:
	testq	%r12, %r12
	je	.L185
	leaq	.LC3(%rip), %rdi
	movq	%rdi, (%rsp)
	movq	%rdi, %r15
	leaq	.LC2(%rip), %rdi
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	.LC3(%rip), %rsi
	movq	%rsi, %r11
	movq	%rsi, %r9
	movq	%rsi, %r8
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L95
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L118:
	xorl	%eax, %eax
#APP
# 198 "fmtmsg.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L119
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 198 "fmtmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L119
.L178:
	leaq	.LC2(%rip), %rdx
	leaq	.LC3(%rip), %r11
	testb	%r9b, %r9b
	movq	%r12, %rdi
	movq	(%rsp), %rbp
	leaq	.LC6(%rip), %rsi
	movq	%rdx, 8(%rsp)
	movq	%r11, 24(%rsp)
	jne	.L173
	movq	%r11, %r9
	movq	%r11, %r8
	jmp	.L105
.L179:
	leaq	.LC3(%rip), %r9
	movq	%r9, %r8
	jmp	.L105
.L185:
	leaq	.LC3(%rip), %r12
	testq	%r14, %r14
	movq	%r12, %r15
	movq	%r12, (%rsp)
	movq	%r12, %rdi
	je	.L127
	testl	%r13d, %r13d
	movq	%r12, %rsi
	jne	.L174
	movq	%r12, %r9
	movq	%r12, %r8
	jmp	.L113
.L130:
	movl	$-1, %ebp
	jmp	.L88
.L134:
	leaq	.LC3(%rip), %rsi
	movq	%rsi, %rbp
	jmp	.L104
.L142:
	movq	%rsi, %r9
	movq	%rsi, %r8
	jmp	.L113
.L180:
	leaq	.LC2(%rip), %r11
	movq	%r12, %rdi
	movq	%r11, 8(%rsp)
	jmp	.L124
.L177:
	testb	%sil, %sil
	movq	(%rsp), %rbp
	leaq	.LC3(%rip), %rdi
	jne	.L151
	movl	$1, %r8d
	movq	%rdi, 8(%rsp)
	movq	%rdi, 24(%rsp)
	jmp	.L103
.L151:
	movq	%rdi, %rsi
	movq	%rdi, 8(%rsp)
	jmp	.L104
.LFE67:
	.size	fmtmsg, .-fmtmsg
	.p2align 4,,15
	.globl	__addseverity
	.type	__addseverity, @function
__addseverity:
.LFB70:
	cmpl	$4, %edi
	jle	.L191
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
#APP
# 357 "fmtmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L188
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L189:
	movl	%ebx, %edi
	call	internal_addseverity
	movl	%eax, %r8d
#APP
# 363 "fmtmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L190
	subl	$1, lock(%rip)
.L186:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L189
	leaq	lock(%rip), %rdi
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rsi
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%eax, %eax
#APP
# 363 "fmtmsg.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L186
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 363 "fmtmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L191:
	movl	$-1, %r8d
	movl	%r8d, %eax
	ret
.LFE70:
	.size	__addseverity, .-__addseverity
	.weak	addseverity
	.set	addseverity,__addseverity
	.local	once.9254
	.comm	once.9254,4,4
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	print
	.comm	print,4,4
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	severity_list, @object
	.size	severity_list, 8
severity_list:
	.quad	infosev
	.section	.rodata.str1.1
.LC8:
	.string	"INFO"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 16
	.type	infosev, @object
	.size	infosev, 24
infosev:
	.long	4
	.zero	4
	.quad	.LC8
	.quad	warningsev
	.section	.rodata.str1.1
.LC9:
	.string	"WARNING"
	.section	.data.rel.ro.local
	.align 16
	.type	warningsev, @object
	.size	warningsev, 24
warningsev:
	.long	3
	.zero	4
	.quad	.LC9
	.quad	errorsev
	.section	.rodata.str1.1
.LC10:
	.string	"ERROR"
	.section	.data.rel.ro.local
	.align 16
	.type	errorsev, @object
	.size	errorsev, 24
errorsev:
	.long	2
	.zero	4
	.quad	.LC10
	.quad	haltsev
	.section	.rodata.str1.1
.LC11:
	.string	"HALT"
	.section	.data.rel.ro.local
	.align 16
	.type	haltsev, @object
	.size	haltsev, 24
haltsev:
	.long	1
	.zero	4
	.quad	.LC11
	.quad	nosev
	.align 16
	.type	nosev, @object
	.size	nosev, 24
nosev:
	.long	0
	.zero	4
	.quad	.LC3
	.quad	0
	.section	.rodata
	.align 32
	.type	keywords, @object
	.size	keywords, 80
keywords:
	.long	5
	.string	"label"
	.zero	6
	.long	8
	.string	"severity"
	.zero	3
	.long	4
	.string	"text"
	.zero	7
	.long	6
	.string	"action"
	.zero	5
	.long	3
	.string	"tag"
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__fxprintf
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__lll_lock_wait_private
