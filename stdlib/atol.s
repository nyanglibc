	.text
	.p2align 4,,15
	.globl	atol
	.type	atol, @function
atol:
	movl	$10, %edx
	xorl	%esi, %esi
	jmp	strtol
	.size	atol, .-atol
	.hidden	strtol
