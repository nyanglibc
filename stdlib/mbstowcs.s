	.text
	.p2align 4,,15
	.globl	mbstowcs
	.type	mbstowcs, @function
mbstowcs:
	subq	$40, %rsp
	movq	%rsi, 8(%rsp)
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %rsi
	movq	$0, 24(%rsp)
	call	__mbsrtowcs
	addq	$40, %rsp
	ret
	.size	mbstowcs, .-mbstowcs
	.hidden	__mbsrtowcs
