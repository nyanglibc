	.text
	.p2align 4,,15
	.globl	__mpn_add_1
	.hidden	__mpn_add_1
	.type	__mpn_add_1, @function
__mpn_add_1:
	xorl	%eax, %eax
	addq	(%rsi), %rcx
	leaq	8(%rsi), %r8
	leaq	8(%rdi), %rsi
	setc	%al
	movq	%rcx, (%rdi)
	testq	%rax, %rax
	jne	.L4
.L5:
	cmpq	%r8, %rsi
	je	.L9
	cmpq	$1, %rdx
	jle	.L9
	subq	$1, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%r8,%rax,8), %rcx
	movq	%rcx, (%rsi,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L10
.L9:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$8, %r8
	movq	-8(%r8), %rax
	addq	$8, %rsi
	addq	$1, %rax
	testq	%rax, %rax
	movq	%rax, -8(%rsi)
	jne	.L5
.L4:
	subq	$1, %rdx
	jne	.L6
	movl	$1, %eax
	ret
	.size	__mpn_add_1, .-__mpn_add_1
	.p2align 4,,15
	.globl	__mpn_add
	.type	__mpn_add, @function
__mpn_add:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rdx, %rbx
	subq	$16, %rsp
	testq	%r8, %r8
	jne	.L21
.L15:
	cmpq	%rbx, %r8
	je	.L14
	leaq	0(,%r8,8), %rdi
	addq	$16, %rsp
	subq	%r8, %rbx
	movq	%rbx, %rdx
	movq	%rax, %rcx
	leaq	(%r12,%rdi), %rsi
	addq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__mpn_add_1
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r8, 8(%rsp)
	call	__mpn_add_n
	movq	8(%rsp), %r8
	jmp	.L15
	.size	__mpn_add, .-__mpn_add
	.p2align 4,,15
	.globl	__mpn_sub_1
	.hidden	__mpn_sub_1
	.type	__mpn_sub_1, @function
__mpn_sub_1:
	movq	(%rsi), %rax
	leaq	8(%rsi), %r8
	movq	%rax, %rsi
	xorl	%eax, %eax
	subq	%rcx, %rsi
	leaq	8(%rdi), %rcx
	setb	%al
	movq	%rsi, (%rdi)
	testq	%rax, %rax
	jne	.L25
.L26:
	cmpq	%r8, %rcx
	je	.L30
	cmpq	$1, %rdx
	jle	.L30
	subq	$1, %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%r8,%rax,8), %rsi
	movq	%rsi, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L31
.L30:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$8, %r8
	movq	-8(%r8), %rax
	addq	$8, %rcx
	leaq	-1(%rax), %rsi
	testq	%rax, %rax
	movq	%rsi, -8(%rcx)
	jne	.L26
.L25:
	subq	$1, %rdx
	jne	.L27
	movl	$1, %eax
	ret
	.size	__mpn_sub_1, .-__mpn_sub_1
	.p2align 4,,15
	.globl	__mpn_sub
	.type	__mpn_sub, @function
__mpn_sub:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rdx, %rbx
	subq	$16, %rsp
	testq	%r8, %r8
	jne	.L41
.L35:
	cmpq	%rbx, %r8
	je	.L34
	leaq	0(,%r8,8), %rdi
	addq	$16, %rsp
	subq	%r8, %rbx
	movq	%rbx, %rdx
	movq	%rax, %rcx
	leaq	(%r12,%rdi), %rsi
	addq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__mpn_sub_1
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rcx, %rdx
	movq	%r8, %rcx
	movq	%r8, 8(%rsp)
	call	__mpn_sub_n
	movq	8(%rsp), %r8
	jmp	.L35
	.size	__mpn_sub, .-__mpn_sub
	.hidden	__mpn_sub_n
	.hidden	__mpn_add_n
