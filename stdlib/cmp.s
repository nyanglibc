	.text
	.p2align 4,,15
	.globl	__mpn_cmp
	.hidden	__mpn_cmp
	.type	__mpn_cmp, @function
__mpn_cmp:
	subq	$1, %rdx
	jns	.L5
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	subq	$1, %rdx
	cmpq	$-1, %rdx
	je	.L6
.L5:
	movq	(%rdi,%rdx,8), %rax
	movq	(%rsi,%rdx,8), %rcx
	cmpq	%rcx, %rax
	je	.L9
.L3:
	cmpq	%rax, %rcx
	sbbl	%eax, %eax
	andl	$2, %eax
	subl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.size	__mpn_cmp, .-__mpn_cmp
