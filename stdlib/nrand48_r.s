	.text
	.p2align 4,,15
	.globl	__nrand48_r
	.hidden	__nrand48_r
	.type	__nrand48_r, @function
__nrand48_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %rbx
	subq	$8, %rsp
	call	__drand48_iterate
	testl	%eax, %eax
	js	.L3
	movzwl	2(%rbp), %ecx
	movzwl	4(%rbp), %eax
	shrw	%cx
	sall	$15, %eax
	movzwl	%cx, %ecx
	orl	%ecx, %eax
	cltq
	movq	%rax, (%rbx)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__nrand48_r, .-__nrand48_r
	.weak	nrand48_r
	.set	nrand48_r,__nrand48_r
	.hidden	__drand48_iterate
