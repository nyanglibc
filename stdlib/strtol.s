	.text
	.p2align 4,,15
	.globl	__strtol_internal
	.hidden	__strtol_internal
	.type	__strtol_internal, @function
__strtol_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	____strtol_l_internal
	.size	__strtol_internal, .-__strtol_internal
	.globl	__strtoll_internal
	.set	__strtoll_internal,__strtol_internal
	.p2align 4,,15
	.globl	__strtol
	.type	__strtol, @function
__strtol:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	____strtol_l_internal
	.size	__strtol, .-__strtol
	.weak	strtol
	.hidden	strtol
	.set	strtol,__strtol
	.weak	strtoimax
	.set	strtoimax,strtol
	.weak	strtoq
	.set	strtoq,strtol
	.weak	strtoll
	.set	strtoll,strtol
	.hidden	____strtol_l_internal
