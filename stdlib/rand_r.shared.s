	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	rand_r
	.type	rand_r, @function
rand_r:
	imull	$1103515245, (%rdi), %eax
	addl	$12345, %eax
	imull	$1103515245, %eax, %edx
	shrl	$6, %eax
	andl	$2096128, %eax
	addl	$12345, %edx
	movl	%edx, %ecx
	imull	$1103515245, %edx, %edx
	shrl	$16, %ecx
	andl	$1023, %ecx
	orl	%ecx, %eax
	addl	$12345, %edx
	sall	$10, %eax
	movl	%edx, %ecx
	movl	%edx, (%rdi)
	shrl	$16, %ecx
	andl	$1023, %ecx
	xorl	%ecx, %eax
	ret
	.size	rand_r, .-rand_r
