	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	a64l
	.type	a64l, @function
a64l:
	leaq	a64l_table(%rip), %rsi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.L3:
	movsbl	(%rdi), %edx
	subl	$46, %edx
	cmpl	$76, %edx
	ja	.L2
	movsbl	(%rsi,%rdx), %edx
	cmpl	$64, %edx
	je	.L2
	sall	%cl, %edx
	addl	$6, %ecx
	addq	$1, %rdi
	orq	%rdx, %rax
	cmpl	$36, %ecx
	jne	.L3
.L2:
	rep ret
	.size	a64l, .-a64l
	.section	.rodata
	.align 32
	.type	a64l_table, @object
	.size	a64l_table, 77
a64l_table:
	.byte	0
	.byte	1
	.byte	2
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	10
	.byte	11
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	25
	.byte	26
	.byte	27
	.byte	28
	.byte	29
	.byte	30
	.byte	31
	.byte	32
	.byte	33
	.byte	34
	.byte	35
	.byte	36
	.byte	37
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	64
	.byte	38
	.byte	39
	.byte	40
	.byte	41
	.byte	42
	.byte	43
	.byte	44
	.byte	45
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	58
	.byte	59
	.byte	60
	.byte	61
	.byte	62
	.byte	63
