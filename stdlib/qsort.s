	.text
	.p2align 4,,15
	.globl	_quicksort
	.type	_quicksort, @function
_quicksort:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1096, %rsp
	testq	%rsi, %rsi
	movq	%rdi, 48(%rsp)
	je	.L1
	leaq	0(,%rdx,4), %rax
	movq	%rdx, %r14
	movq	%rcx, %r12
	movq	%r8, %r13
	movq	%rax, 40(%rsp)
	leaq	-1(%rsi), %rax
	imulq	%rdx, %rax
	addq	%rdi, %rax
	cmpq	$4, %rsi
	movq	%rax, 24(%rsp)
	jbe	.L3
	movq	%rax, 8(%rsp)
	leaq	64(%rsp), %rax
	movq	%rdx, %r15
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	negq	%r15
	movq	%rax, 56(%rsp)
	leaq	80(%rsp), %rax
	movq	%rdi, 16(%rsp)
	movq	%rdx, (%rsp)
	movq	%rax, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L22:
	movq	16(%rsp), %rdi
	movq	8(%rsp), %rax
	xorl	%edx, %edx
	movq	(%rsp), %rsi
	subq	%rdi, %rax
	divq	%rsi
	movq	%r13, %rdx
	shrq	%rax
	imulq	%rsi, %rax
	movq	%rdi, %rsi
	leaq	(%rdi,%rax), %rbp
	movq	%rbp, %rdi
	call	*%r12
	testl	%eax, %eax
	js	.L58
.L4:
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	8(%rsp), %rdi
	call	*%r12
	testl	%eax, %eax
	js	.L59
.L6:
	movq	8(%rsp), %rax
	movq	16(%rsp), %r14
	addq	(%rsp), %r14
	leaq	(%rax,%r15), %rbx
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	*%r12
	testl	%eax, %eax
	jns	.L11
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L12:
	addq	%r15, %rbx
.L11:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	*%r12
	testl	%eax, %eax
	js	.L12
	cmpq	%rbx, %r14
	jb	.L61
	je	.L62
.L16:
	cmpq	%rbx, %r14
	jbe	.L9
.L17:
	movq	%rbx, %rdx
	subq	16(%rsp), %rdx
	movq	8(%rsp), %rax
	movq	40(%rsp), %rdi
	subq	%r14, %rax
	cmpq	%rdi, %rdx
	ja	.L19
	cmpq	%rax, %rdi
	movq	%r14, 16(%rsp)
	jb	.L20
	movq	32(%rsp), %rax
	movq	-16(%rax), %rdi
	subq	$16, %rax
	movq	%rdi, 16(%rsp)
	movq	8(%rax), %rdi
	movq	%rax, 32(%rsp)
	movq	%rdi, 8(%rsp)
.L20:
	movq	56(%rsp), %rdi
	cmpq	%rdi, 32(%rsp)
	ja	.L22
	movq	(%rsp), %r14
.L3:
	movq	48(%rsp), %rax
	movq	40(%rsp), %rbx
	movq	24(%rsp), %rsi
	addq	%rax, %rbx
	leaq	(%rax,%r14), %rbp
	cmpq	%rbx, %rsi
	movq	%rbx, %rdi
	cmovb	%rsi, %rdi
	movq	%rbp, %r15
	cmpq	%rbp, %rdi
	jb	.L26
	movq	%rbp, (%rsp)
	movq	%rdi, %rbx
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rbp, %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	call	*%r12
	testl	%eax, %eax
	cmovs	%r15, %rbp
	addq	%r14, %r15
	cmpq	%r15, %rbx
	jnb	.L23
	movq	48(%rsp), %rax
	movq	%rbp, %rbx
	movq	(%rsp), %rbp
	cmpq	%rax, %rbx
	je	.L26
	leaq	(%rbx,%r14), %rsi
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	(%rbx), %edx
	movzbl	(%rax), %ecx
	addq	$1, %rbx
	addq	$1, %rax
	cmpq	%rsi, %rbx
	movb	%cl, -1(%rbx)
	movb	%dl, -1(%rax)
	jne	.L27
.L26:
	movq	%r14, %r15
	leaq	-1(%r14), %rax
	leaq	0(%rbp,%r15), %rbx
	cmpq	24(%rsp), %rbx
	movq	%rax, (%rsp)
	ja	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r15, %r14
	negq	%r14
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	addq	%r14, %rbp
.L28:
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*%r12
	testl	%eax, %eax
	js	.L29
	leaq	0(%rbp,%r15), %rsi
	cmpq	%rsi, %rbx
	je	.L30
	movq	(%rsp), %rdi
	addq	%rbx, %rdi
	jc	.L30
	leaq	-1(%rbx), %r9
	movq	%r9, %r11
	.p2align 4,,10
	.p2align 3
.L33:
	cmpq	%rsi, %r9
	movzbl	(%rdi), %r10d
	movq	%r9, %rax
	jb	.L43
	movq	%rdi, %rdx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rcx, %rax
.L32:
	movzbl	(%rax), %ecx
	movb	%cl, (%rdx)
	leaq	(%rax,%r14), %rcx
	subq	%r15, %rdx
	cmpq	%rcx, %rsi
	jbe	.L44
	subq	$1, %rdi
	subq	$1, %r9
	movb	%r10b, (%rax)
	cmpq	%r11, %rdi
	jne	.L33
.L30:
	movq	%rbx, %rbp
	leaq	0(%rbp,%r15), %rbx
	cmpq	24(%rsp), %rbx
	jbe	.L34
.L1:
	addq	$1096, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rdi, %rax
	subq	$1, %rdi
	subq	$1, %r9
	cmpq	%r11, %rdi
	movb	%r10b, (%rax)
	jne	.L33
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L60:
	addq	(%rsp), %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L61:
	movq	(%rsp), %rax
	movq	%rbx, %rdx
	leaq	(%r14,%rax), %rdi
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	(%rax), %ecx
	movzbl	(%rdx), %esi
	addq	$1, %rax
	addq	$1, %rdx
	cmpq	%rdi, %rax
	movb	%sil, -1(%rax)
	movb	%cl, -1(%rdx)
	jne	.L14
	cmpq	%rbp, %r14
	je	.L36
	cmpq	%rbp, %rbx
	cmove	%r14, %rbp
.L15:
	addq	%r15, %rbx
	movq	%rdi, %r14
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rbx, %rbp
	jmp	.L15
.L19:
	cmpq	%rax, 40(%rsp)
	jnb	.L39
	movq	32(%rsp), %rdi
	cmpq	%rax, %rdx
	leaq	16(%rdi), %rcx
	jle	.L21
	movq	%rdi, %rax
	movq	16(%rsp), %rdi
	movq	%rcx, 32(%rsp)
	movq	%rbx, 8(%rax)
	movq	%r14, 16(%rsp)
	movq	%rdi, (%rax)
	jmp	.L20
.L62:
	addq	(%rsp), %r14
	addq	%r15, %rbx
	jmp	.L17
.L39:
	movq	%rbx, 8(%rsp)
	jmp	.L20
.L59:
	movq	(%rsp), %rax
	movq	8(%rsp), %rdx
	leaq	0(%rbp,%rax), %rbx
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L7:
	movzbl	(%rax), %ecx
	movzbl	(%rdx), %esi
	addq	$1, %rax
	addq	$1, %rdx
	cmpq	%rbx, %rax
	movb	%sil, -1(%rax)
	movb	%cl, -1(%rdx)
	jne	.L7
	movq	%r13, %rdx
	movq	16(%rsp), %rsi
	movq	%rbp, %rdi
	call	*%r12
	testl	%eax, %eax
	jns	.L6
	movq	16(%rsp), %rdx
	movq	%rbp, %rax
.L8:
	movzbl	(%rax), %ecx
	movzbl	(%rdx), %esi
	addq	$1, %rax
	addq	$1, %rdx
	cmpq	%rbx, %rax
	movb	%sil, -1(%rax)
	movb	%cl, -1(%rdx)
	jne	.L8
	jmp	.L6
.L58:
	movq	(%rsp), %rax
	movq	16(%rsp), %rdx
	leaq	0(%rbp,%rax), %rdi
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	(%rax), %ecx
	movzbl	(%rdx), %esi
	addq	$1, %rax
	addq	$1, %rdx
	cmpq	%rdi, %rax
	movb	%sil, -1(%rax)
	movb	%cl, -1(%rdx)
	jne	.L5
	jmp	.L4
.L21:
	movq	32(%rsp), %rax
	movq	8(%rsp), %rdi
	movq	%rcx, 32(%rsp)
	movq	%rbx, 8(%rsp)
	movq	%r14, (%rax)
	movq	%rdi, 8(%rax)
	jmp	.L20
	.size	_quicksort, .-_quicksort
