	.text
	.p2align 4,,15
	.globl	__run_exit_handlers
	.hidden	__run_exit_handlers
	.type	__run_exit_handlers, @function
__run_exit_handlers:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$8, %rsp
	cmpq	$0, __call_tls_dtors@GOTPCREL(%rip)
	je	.L2
	testb	%cl, %cl
	jne	.L36
.L2:
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L4:
#APP
# 56 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L5
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
.L18:
	movq	0(%rbp), %r14
	testq	%r14, %r14
	jne	.L7
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L39:
	cmpq	$4, %rdx
	je	.L14
	cmpq	$2, %rdx
	jne	.L12
	movq	24(%rax), %rdx
	movq	32(%rax), %rsi
	movl	%ebx, %edi
#APP
# 89 "exit.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	call	*%rdx
.L12:
#APP
# 112 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L16
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
.L17:
	cmpq	%r15, __new_exitfn_called(%rip)
	jne	.L18
.L7:
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.L38
	leaq	-1(%rax), %r8
	movq	__new_exitfn_called(%rip), %r15
	movq	%r8, 8(%r14)
#APP
# 76 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	subl	$1, __exit_funcs_lock(%rip)
.L11:
	movq	%r8, %rax
	salq	$5, %rax
	addq	%r14, %rax
	movq	16(%rax), %rdx
	cmpq	$3, %rdx
	jne	.L39
	movq	24(%rax), %rax
#APP
# 96 "exit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	movq	24(%rax), %rdx
	movq	$0, 16(%rax)
	movl	%ebx, %esi
#APP
# 106 "exit.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	movq	32(%rax), %rdi
	call	*%rdx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%r12d, %eax
	lock cmpxchgl	%edx, __exit_funcs_lock(%rip)
	je	.L17
	leaq	__exit_funcs_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%r12d, %eax
#APP
# 76 "exit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L11
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__exit_funcs_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 76 "exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L38:
	movq	(%r14), %rax
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L20
	movq	%r14, %rdi
	call	free@PLT
.L20:
#APP
# 126 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L21
	subl	$1, __exit_funcs_lock(%rip)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L37:
	movb	$1, __exit_funcs_done(%rip)
#APP
# 66 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L8
	subl	$1, __exit_funcs_lock(%rip)
.L9:
	testb	%r13b, %r13b
	je	.L23
	movq	__start___libc_atexit@GOTPCREL(%rip), %rbp
	movq	__stop___libc_atexit@GOTPCREL(%rip), %rax
	cmpq	%rax, %rbp
	jnb	.L23
	movq	%rbp, %rdx
	notq	%rdx
	addq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbp,%rax,8), %r12
	.p2align 4,,10
	.p2align 3
.L24:
	call	*0(%rbp)
	addq	$8, %rbp
	cmpq	%rbp, %r12
	jne	.L24
.L23:
	movl	%ebx, %edi
	call	_exit
.L5:
	movl	%r12d, %eax
	lock cmpxchgl	%edx, __exit_funcs_lock(%rip)
	je	.L18
	leaq	__exit_funcs_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L18
.L21:
	movl	%r12d, %eax
#APP
# 126 "exit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L4
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__exit_funcs_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 126 "exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L4
.L36:
	call	__call_tls_dtors@PLT
	jmp	.L2
.L8:
	xorl	%eax, %eax
#APP
# 66 "exit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L9
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__exit_funcs_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 66 "exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L9
	.size	__run_exit_handlers, .-__run_exit_handlers
	.p2align 4,,15
	.globl	exit
	.hidden	exit
	.type	exit, @function
exit:
	leaq	__exit_funcs(%rip), %rsi
	subq	$8, %rsp
	movl	$1, %ecx
	movl	$1, %edx
	call	__run_exit_handlers
	.size	exit, .-exit
	.hidden	__exit_funcs_done
	.globl	__exit_funcs_done
	.bss
	.type	__exit_funcs_done, @object
	.size	__exit_funcs_done, 1
__exit_funcs_done:
	.zero	1
	.weak	__stop___libc_atexit
	.weak	__start___libc_atexit
	.weak	__call_tls_dtors
	.hidden	__exit_funcs
	.hidden	_exit
	.hidden	__lll_lock_wait_private
	.hidden	__new_exitfn_called
	.hidden	__call_tls_dtors
