	.text
	.p2align 4,,15
	.globl	seed48
	.type	seed48, @function
seed48:
	leaq	__libc_drand48_data(%rip), %rsi
	subq	$8, %rsp
	call	__seed48_r
	leaq	6+__libc_drand48_data(%rip), %rax
	addq	$8, %rsp
	ret
	.size	seed48, .-seed48
	.hidden	__seed48_r
	.hidden	__libc_drand48_data
