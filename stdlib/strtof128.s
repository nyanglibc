	.text
	.p2align 4,,15
	.globl	__strtof128_internal
	.hidden	__strtof128_internal
	.type	__strtof128_internal, @function
__strtof128_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____strtof128_l_internal
	.size	__strtof128_internal, .-__strtof128_internal
	.p2align 4,,15
	.weak	strtof128
	.hidden	strtof128
	.type	strtof128, @function
strtof128:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____strtof128_l_internal
	.size	strtof128, .-strtof128
	.hidden	____strtof128_l_internal
