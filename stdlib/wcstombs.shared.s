	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	wcstombs
	.type	wcstombs, @function
wcstombs:
	subq	$40, %rsp
	movq	%rsi, 8(%rsp)
	leaq	24(%rsp), %rcx
	leaq	8(%rsp), %rsi
	movq	$0, 24(%rsp)
	call	__wcsrtombs
	addq	$40, %rsp
	ret
	.size	wcstombs, .-wcstombs
	.hidden	__wcsrtombs
