	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	llabs
	.type	llabs, @function
llabs:
	movq	%rdi, %rdx
	movq	%rdi, %rax
	sarq	$63, %rdx
	xorq	%rdx, %rax
	subq	%rdx, %rax
	ret
	.size	llabs, .-llabs
