	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"-"
	.globl	__lttf2
#NO_APP
	.text
	.p2align 4,,15
	.globl	__vstrfmon_l_internal
	.hidden	__vstrfmon_l_internal
	.type	__vstrfmon_l_internal, @function
__vstrfmon_l_internal:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r11
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r14
	pxor	%xmm2, %xmm2
	subq	$552, %rsp
	movq	32(%rdx), %r10
	movq	%r8, 72(%rsp)
	movq	%rdx, 64(%rsp)
	movq	%rdi, %r8
	movl	%r9d, 96(%rsp)
.L2:
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L359
.L145:
	cmpb	$37, %al
	je	.L3
	leaq	-1(%r11,%r15), %rdx
	cmpq	%rdx, %r8
	jnb	.L21
	addq	$1, %r14
	movb	%al, (%r8)
	addq	$1, %r8
	movzbl	(%r14), %eax
	testb	%al, %al
	jne	.L145
.L359:
	movq	%r8, %rax
	movb	$0, (%r8)
	subq	%r11, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movzbl	1(%r14), %eax
	cmpb	$37, %al
	jne	.L171
	leaq	-1(%r11,%r15), %rax
	cmpq	%rax, %r8
	jnb	.L21
	movb	$37, (%r8)
	addq	$2, %r14
	addq	$1, %r8
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$-2, %ebp
	cmpb	$43, %al
	movl	$0, 28(%rsp)
	movl	%ebp, %r13d
	movb	$32, 149(%rsp)
	movl	$1, (%rsp)
	movl	$1, 24(%rsp)
	leaq	1(%r14), %rdx
	je	.L12
	.p2align 4,,10
	.p2align 3
.L367:
	jle	.L360
	cmpb	$61, %al
	je	.L15
	cmpb	$94, %al
	je	.L9
	cmpb	$45, %al
	je	.L361
.L11:
	movsbl	%al, %ecx
	movq	$-1, %rbx
	subl	$48, %ecx
	cmpl	$9, %ecx
	jbe	.L362
	cmpb	$35, %al
	movl	$-1, %r9d
	je	.L363
.L24:
	cmpb	$46, %al
	movl	$-1, %r12d
	je	.L364
.L26:
	cmpb	$76, %al
	leaq	1(%rdx), %r14
	movl	$0, 100(%rsp)
	movl	$0, 144(%rsp)
	je	.L365
	cmpb	$105, %al
	je	.L30
.L369:
	cmpb	$110, %al
	jne	.L330
	movq	72(%r10), %rax
	movq	%r11, 104(%rsp)
	movq	%r8, 80(%rsp)
	movl	%r9d, 40(%rsp)
	movaps	%xmm2, 48(%rsp)
	movq	%rax, %rdi
	movq	%r10, 32(%rsp)
	movq	%rax, 136(%rsp)
	call	__GI_strlen
	cmpl	$-2, %r13d
	movq	%rax, 120(%rsp)
	movq	32(%rsp), %r10
	movl	40(%rsp), %r9d
	movq	80(%rsp), %r8
	movdqa	48(%rsp), %xmm2
	movq	104(%rsp), %r11
	jne	.L357
	movq	168(%r10), %rax
	movsbl	(%rax), %r13d
.L357:
	cmpl	$-2, %ebp
	jne	.L352
	movq	176(%r10), %rax
	movsbl	(%rax), %ebp
.L352:
	xorl	%edx, %edx
	cmpl	$-1, %r12d
	movb	$32, 151(%rsp)
	je	.L218
.L36:
	movzbl	(%rsp), %eax
	andl	$1, %eax
	cmpl	$-1, %r9d
	movb	%al, 150(%rsp)
	je	.L37
	testb	%al, %al
	je	.L37
	movq	96(%r10), %rsi
	movl	%r9d, %edi
	movq	%r11, 104(%rsp)
	movq	%r8, 80(%rsp)
	movl	%edx, 40(%rsp)
	movaps	%xmm2, 48(%rsp)
	movq	%r10, 32(%rsp)
	movl	%r9d, (%rsp)
	call	__guess_grouping
	movl	(%rsp), %r9d
	movq	104(%rsp), %r11
	movq	80(%rsp), %r8
	movl	40(%rsp), %edx
	movq	32(%rsp), %r10
	movdqa	48(%rsp), %xmm2
	addl	%eax, %r9d
.L37:
	cmpl	$1, 144(%rsp)
	movq	72(%rsp), %rsi
	je	.L366
	movl	4(%rsi), %ecx
	cmpl	$175, %ecx
	ja	.L47
	movl	%ecx, %eax
	addl	$16, %ecx
	addq	16(%rsi), %rax
	movl	%ecx, 4(%rsi)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L360:
	cmpb	$33, %al
	je	.L172
	cmpb	$40, %al
	jne	.L11
	cmpl	$-2, %ebp
	jne	.L330
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L10:
	movzbl	1(%rdx), %eax
	movq	%rdx, %r14
	leaq	1(%r14), %rdx
	cmpb	$43, %al
	jne	.L367
.L12:
	cmpl	$-2, %ebp
	jne	.L330
	movq	168(%r10), %rax
	movsbl	(%rax), %r13d
	movq	176(%r10), %rax
	movsbl	(%rax), %ebp
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movzbl	2(%r14), %eax
	leaq	2(%r14), %rdx
	testb	%al, %al
	movb	%al, 149(%rsp)
	jne	.L10
	.p2align 4,,10
	.p2align 3
.L330:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$0, 24(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$1, 28(%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$0, (%rsp)
	jmp	.L10
.L381:
	movq	136(%rsp), %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L104
	movq	40(%rsp), %rsi
	cmpq	%rsi, %r13
	jnb	.L21
	movq	%rsi, %rcx
	subq	%r13, %rcx
	addq	%rax, %rcx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	cmpq	%rcx, %rax
	je	.L21
.L105:
	addq	$1, %rax
	addq	$1, %r13
	movb	%dl, -1(%r13)
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L106
.L104:
	cmpl	$4, %ebp
	jne	.L107
	cmpl	$2, (%rsp)
	jne	.L108
	cmpq	40(%rsp), %r13
	jnb	.L21
	movzbl	151(%rsp), %eax
	addq	$1, %r13
	movb	%al, -1(%r13)
.L108:
	movq	112(%rsp), %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L110
	movq	40(%rsp), %rsi
	cmpq	%rsi, %r13
	jnb	.L21
	movq	%rsi, %rcx
	subq	%r13, %rcx
	addq	%rax, %rcx
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L109:
	cmpq	%rcx, %rax
	je	.L21
.L111:
	addq	$1, %rax
	addq	$1, %r13
	movb	%dl, -1(%r13)
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L109
.L110:
	cmpl	$1, (%rsp)
	jne	.L112
	cmpq	40(%rsp), %r13
	jnb	.L21
	movb	$32, 0(%r13)
	addq	$1, %r13
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	304(%rsp), %rax
	xorl	%esi, %esi
	movq	%r10, 208(%rsp)
	movq	%r8, 176(%rsp)
	movl	%r9d, 128(%rsp)
	movq	%rax, %rdi
	movq	%rax, 80(%rsp)
	movq	%r11, 152(%rsp)
	movaps	%xmm2, 160(%rsp)
	movq	$0, 440(%rsp)
	call	_IO_init_internal
	leaq	_IO_str_jumps(%rip), %rsi
	movq	%r13, %rcx
	movq	152(%rsp), %r11
	movq	80(%rsp), %rdi
	movq	%rsi, 520(%rsp)
	movq	%r13, %rsi
	leaq	(%r11,%r15), %rdx
	subq	%r13, %rdx
	call	_IO_str_init_static_internal@PLT
	movl	128(%rsp), %r9d
	movzbl	150(%rsp), %ecx
	testl	%r12d, %r12d
	movq	40(%rsp), %rsi
	movl	$0, 284(%rsp)
	leaq	272(%rsp), %rdx
	movq	80(%rsp), %rdi
	movl	%r12d, 272(%rsp)
	leal	1(%r9,%r12), %eax
	movl	$102, 280(%rsp)
	movb	$0, (%rsi)
	movq	64(%rsp), %rsi
	cmovne	%eax, %r9d
	sall	$7, %ecx
	orb	144(%rsp), %cl
	movzbl	100(%rsp), %eax
	movl	%r9d, 276(%rsp)
	movb	%cl, 284(%rsp)
	movsbl	149(%rsp), %ecx
	sall	$4, %eax
	orl	$1, %eax
	movb	%al, 285(%rsp)
	leaq	256(%rsp), %rax
	movl	%ecx, 288(%rsp)
	leaq	248(%rsp), %rcx
	movq	%rax, 248(%rsp)
	call	__GI___printf_fp_l
	testl	%eax, %eax
	js	.L5
	movq	40(%rsp), %rsi
	cmpb	$0, (%rsi)
	jne	.L21
	cltq
	movq	152(%rsp), %r11
	movq	176(%rsp), %r8
	leaq	0(%r13,%rax), %rdi
	movl	32(%rsp), %r13d
	movq	208(%rsp), %r10
	movdqa	160(%rsp), %xmm2
	testl	%r13d, %r13d
	jne	.L116
	cmpl	$3, %ebp
	jne	.L117
	cmpl	$1, (%rsp)
	je	.L368
.L118:
	movq	112(%rsp), %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L120
	movq	40(%rsp), %rsi
	cmpq	%rsi, %rdi
	jnb	.L21
	movq	%rsi, %rcx
	subq	%rdi, %rcx
	addq	%rax, %rcx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L119:
	cmpq	%rcx, %rax
	je	.L21
.L121:
	addq	$1, %rax
	addq	$1, %rdi
	movb	%dl, -1(%rdi)
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L119
.L120:
	movl	24(%rsp), %r12d
	testl	%r12d, %r12d
	je	.L122
	cmpl	$2, (%rsp)
	jne	.L159
.L123:
	cmpq	40(%rsp), %rdi
	jnb	.L21
	movzbl	151(%rsp), %eax
	leaq	1(%rdi), %rcx
	movb	%al, (%rdi)
.L124:
	movq	120(%rsp), %rax
	testl	%eax, %eax
	jle	.L206
	movq	40(%rsp), %rsi
	cmpq	%rsi, %rcx
	jnb	.L21
	movq	136(%rsp), %rdi
	subl	$1, %eax
	leaq	1(%rdi,%rax), %rdx
	movq	%rsi, %rax
	movq	%rdi, %rsi
	subq	%rcx, %rax
	addq	%rdi, %rax
	movq	%rcx, %rdi
	.p2align 4,,10
	.p2align 3
.L126:
	movsb
	cmpq	%rdx, %rsi
	je	.L125
	cmpq	%rax, %rsi
	jne	.L126
	.p2align 4,,10
	.p2align 3
.L21:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %fs:(%rax)
.L5:
	movq	$-1, %rax
.L1:
	addq	$552, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L362:
	movsbl	2(%r14), %eax
	movslq	%ecx, %rbx
	leaq	2(%r14), %rdx
	movabsq	$922337203685477580, %rcx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L23
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L22:
	cmpq	%rcx, %rbx
	jg	.L21
.L23:
	leaq	(%rbx,%rbx,4), %rsi
	cltq
	addq	$1, %rdx
	leaq	(%rax,%rsi,2), %rbx
	movsbl	(%rdx), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L22
.L20:
	movq	%r8, %rax
	movq	%r15, %rsi
	subq	%r11, %rax
	subq	%rax, %rsi
	cmpq	%rbx, %rsi
	jbe	.L21
	movzbl	(%rdx), %eax
	movl	$-1, %r9d
	cmpb	$35, %al
	jne	.L24
	jmp	.L363
	.p2align 4,,10
	.p2align 3
.L364:
	movsbl	1(%rdx), %r12d
	subl	$48, %r12d
	cmpl	$9, %r12d
	ja	.L330
	leaq	2(%rdx), %rcx
	movsbl	2(%rdx), %edx
	movl	%edx, %eax
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L177
	.p2align 4,,10
	.p2align 3
.L27:
	leal	(%r12,%r12,4), %eax
	addq	$1, %rcx
	leal	(%rdx,%rax,2), %r12d
	movsbl	(%rcx), %edx
	movl	%edx, %eax
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L27
.L177:
	movq	%rcx, %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L365:
	movl	96(%rsp), %edi
	leaq	2(%rdx), %r14
	movl	%edi, %eax
	notl	%eax
	andl	$1, %eax
	andl	$2, %edi
	movl	$0, %edi
	movl	%eax, %esi
	movl	%eax, 144(%rsp)
	movzbl	1(%rdx), %eax
	cmovne	%esi, %edi
	movl	%edi, 100(%rsp)
	cmpb	$105, %al
	jne	.L369
.L30:
	movq	64(%r10), %rax
	leaq	244(%rsp), %rcx
	movl	$3, %edx
	movq	%r11, 112(%rsp)
	movq	%r8, 104(%rsp)
	movq	%rcx, %rdi
	movl	%r9d, 48(%rsp)
	movq	%r10, 40(%rsp)
	movq	%rax, %rsi
	movq	%rax, 32(%rsp)
	movaps	%xmm2, 80(%rsp)
	call	__GI_strncpy
	movq	%rax, %rcx
	cmpl	$-2, %r13d
	movq	32(%rsp), %rax
	movb	$0, 247(%rsp)
	movq	40(%rsp), %r10
	movl	48(%rsp), %r9d
	movdqa	80(%rsp), %xmm2
	movzbl	3(%rax), %eax
	movq	104(%rsp), %r8
	movq	112(%rsp), %r11
	movb	%al, 151(%rsp)
	jne	.L355
	movq	224(%r10), %rax
	movsbl	(%rax), %r13d
.L355:
	cmpl	$-2, %ebp
	jne	.L349
	movq	232(%r10), %rax
	movsbl	(%rax), %ebp
.L349:
	cmpl	$-1, %r12d
	movq	$3, 120(%rsp)
	movq	%rcx, 136(%rsp)
	movl	$1, %edx
	jne	.L36
	movl	$7, %eax
.L147:
	movq	64(%r10,%rax,8), %rax
	movsbl	(%rax), %r12d
	movl	$2, %eax
	cmpl	$-1, %r12d
	cmove	%eax, %r12d
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L363:
	movsbl	1(%rdx), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L330
	leaq	2(%rdx), %rcx
	movsbl	2(%rdx), %edx
	movl	%eax, %r9d
	movl	%edx, %eax
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L175
	.p2align 4,,10
	.p2align 3
.L25:
	leal	(%r9,%r9,4), %eax
	addq	$1, %rcx
	leal	(%rdx,%rax,2), %r9d
	movsbl	(%rcx), %edx
	movl	%edx, %eax
	subl	$48, %edx
	cmpl	$9, %edx
	jbe	.L25
.L175:
	cmpb	$46, %al
	movq	%rcx, %rdx
	movl	$-1, %r12d
	jne	.L26
	jmp	.L364
	.p2align 4,,10
	.p2align 3
.L366:
	cmpl	$1, 100(%rsp)
	je	.L370
	movq	8(%rsi), %rax
	movq	%rax, (%rsp)
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, 8(%rsi)
	fldt	(%rax)
	fldz
	fucomip	%st(1), %st
	ja	.L45
	fstpt	256(%rsp)
	seta	%sil
	movzbl	%sil, %esi
.L46:
	movq	104(%r10), %rax
	testl	%esi, %esi
	movq	%rax, 112(%rsp)
	movq	112(%r10), %rax
	movzbl	(%rax), %ecx
	jne	.L371
	testl	%edx, %edx
	je	.L372
	movq	192(%r10), %rdx
	testb	%cl, %cl
	movsbl	(%rdx), %edi
	movq	200(%r10), %rdx
	movl	%edi, 32(%rsp)
	movsbl	(%rdx), %edi
	movq	208(%r10), %rdx
	movsbl	(%rdx), %ecx
	leaq	.LC0(%rip), %rdx
	movl	%edi, (%rsp)
	cmovne	%rax, %rdx
	movl	$19, %eax
	movq	%rdx, 128(%rsp)
.L154:
	movq	64(%r10,%rax,8), %rax
	movl	%ebp, %edx
	movl	%r13d, %ebp
	movl	%edx, %r13d
	movsbl	(%rax), %eax
.L55:
	movl	32(%rsp), %edx
	testl	%edx, %edx
	movl	$0, %edx
	setne	152(%rsp)
	testl	%ecx, %ecx
	movl	%edx, %edi
	setne	80(%rsp)
	cmpl	$-1, (%rsp)
	cmovne	(%rsp), %edi
	cmpl	$-1, %eax
	movl	%edi, (%rsp)
	leaq	-1(%r11,%r15), %rdi
	movq	%rdi, 40(%rsp)
	je	.L373
	movl	%esi, %edi
	andl	$1, %edi
	cmpl	$-1, %ebp
	movb	%dil, 48(%rsp)
	je	.L374
	cmpl	$-1, %r13d
	je	.L375
.L67:
	cmpl	$2, (%rsp)
	je	.L77
	testl	%ebp, %ebp
	sete	104(%rsp)
	movzbl	104(%rsp), %edi
	andb	%dil, 48(%rsp)
.L66:
	cmpl	$2, %eax
	je	.L376
.L80:
	cmpl	$-1, %r9d
	je	.L196
.L163:
	movl	32(%rsp), %edi
	xorl	%edx, %edx
	testl	%edi, %edi
	je	.L76
	cmpl	$1, (%rsp)
	movl	120(%rsp), %edx
	sbbl	$-1, %edx
.L76:
	testl	%ecx, %ecx
	movl	$0, 80(%rsp)
	je	.L85
	movl	120(%rsp), %edi
	testl	%eax, %eax
	movl	%edi, 80(%rsp)
	je	.L85
.L165:
	addl	$1, 80(%rsp)
.L85:
	cmpb	$0, 48(%rsp)
	je	.L86
.L166:
	addl	$1, %edx
.L87:
	cmpl	$1, %r13d
	je	.L377
	testl	%ecx, %ecx
	jne	.L378
.L93:
	movl	80(%rsp), %eax
	cmpl	%edx, %eax
	jle	.L199
	movl	%eax, %r13d
.L347:
	subl	%edx, %r13d
	cmpq	40(%rsp), %r8
	jnb	.L21
	leal	-1(%r13), %eax
	movq	40(%rsp), %rcx
	leaq	1(%r8,%rax), %rdx
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$1, %rax
	movb	$32, -1(%rax)
	cmpq	%rdx, %rax
	je	.L75
	cmpq	%rcx, %rax
	jne	.L96
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L373:
	cmpl	$-1, %ebp
	je	.L379
	movl	%esi, %eax
	andl	$1, %eax
	movb	%al, 48(%rsp)
	xorl	%eax, %eax
	cmpl	$-1, %r13d
	jne	.L67
	cmpl	$2, (%rsp)
	je	.L380
	testl	%ebp, %ebp
	movl	$1, %r13d
	sete	104(%rsp)
	movzbl	104(%rsp), %edi
	andb	%dil, 48(%rsp)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%r8, %rdx
	xorl	%r9d, %r9d
.L75:
	cmpb	$0, 48(%rsp)
	je	.L200
	cmpq	40(%rsp), %rdx
	jnb	.L21
	movl	32(%rsp), %eax
	leaq	1(%rdx), %r13
	movb	$40, (%rdx)
	testl	%eax, %eax
	je	.L99
.L98:
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jne	.L381
	cmpl	$4, %ebp
	jne	.L112
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	$1, %ebp
	je	.L382
.L88:
	movl	32(%rsp), %eax
	testl	%eax, %eax
	je	.L89
	leal	-3(%rbp), %eax
	cmpl	$1, %eax
	ja	.L89
	movq	112(%rsp), %rdi
	movq	%r11, 232(%rsp)
	movq	%r10, 224(%rsp)
	movq	%r8, 216(%rsp)
	movaps	%xmm2, 192(%rsp)
	movl	%r9d, 208(%rsp)
	movl	%edx, 176(%rsp)
	movl	%ecx, 160(%rsp)
	movl	%esi, 152(%rsp)
	call	__GI_strlen
	movl	176(%rsp), %edx
	movq	232(%rsp), %r11
	movq	224(%rsp), %r10
	movq	216(%rsp), %r8
	movl	208(%rsp), %r9d
	movl	160(%rsp), %ecx
	movl	152(%rsp), %esi
	addl	%eax, %edx
	movdqa	192(%rsp), %xmm2
	.p2align 4,,10
	.p2align 3
.L89:
	orl	%r13d, %esi
	jne	.L87
	movl	80(%rsp), %r13d
	addl	$1, %r13d
	cmpl	%r13d, %edx
	jl	.L347
	movq	%r8, %r13
.L91:
	movl	32(%rsp), %eax
	testl	%eax, %eax
	jne	.L383
	movb	$0, 48(%rsp)
.L99:
	leal	-2(%rbp), %eax
	cmpl	$3, %eax
	jbe	.L112
	testl	%ebp, %ebp
	je	.L112
.L169:
	movq	112(%rsp), %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L112
	movq	40(%rsp), %rsi
	cmpq	%r13, %rsi
	jbe	.L21
	movq	%rsi, %rcx
	subq	%r13, %rcx
	addq	%rax, %rcx
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	%rcx, %rax
	je	.L21
.L113:
	addq	$1, %rax
	addq	$1, %r13
	movb	%dl, -1(%r13)
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L114
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$2, %ebp
	je	.L384
.L122:
	cmpb	$0, 48(%rsp)
	je	.L207
	cmpq	40(%rsp), %rdi
	jnb	.L21
	leaq	1(%rdi), %rdx
	movb	$41, (%rdi)
.L135:
	movq	%rdx, %rax
	subq	%r8, %rax
	cmpq	%rbx, %rax
	jge	.L136
	movl	28(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L137
	cmpq	40(%rsp), %rdx
	jnb	.L21
	leaq	1(%rdx), %rax
	movb	$32, (%rdx)
	movq	40(%rsp), %rcx
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%rdx, %rbx
	jg	.L138
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L339:
	addq	$1, %rax
	movb	$32, -1(%rax)
	movq	%rax, %rdx
	subq	%r8, %rdx
	cmpq	%rbx, %rdx
	jge	.L208
.L138:
	cmpq	%rcx, %rax
	jne	.L339
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L372:
	movq	136(%r10), %rdx
	testb	%cl, %cl
	movsbl	(%rdx), %edi
	movq	144(%r10), %rdx
	movl	%edi, 32(%rsp)
	movsbl	(%rdx), %edi
	movq	152(%r10), %rdx
	movsbl	(%rdx), %ecx
	leaq	.LC0(%rip), %rdx
	movl	%edi, (%rsp)
	cmovne	%rax, %rdx
	movl	$12, %eax
	movq	%rdx, 128(%rsp)
	jmp	.L154
	.p2align 4,,10
	.p2align 3
.L376:
	xorl	%eax, %eax
	testl	%r13d, %r13d
	je	.L80
	cmpl	$1, %r13d
	sete	%dl
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L374:
	cmpl	$-1, %r13d
	je	.L65
	cmpl	$2, (%rsp)
	je	.L62
.L346:
	movb	$0, 48(%rsp)
	movb	$0, 104(%rsp)
	movl	$1, %ebp
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L218:
	movl	$8, %eax
	jmp	.L147
	.p2align 4,,10
	.p2align 3
.L199:
	movq	%r8, %rdx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L77:
	testl	%ebp, %ebp
	movl	$0, (%rsp)
	movb	$1, 104(%rsp)
	je	.L66
	cmpl	$1, %ebp
	movb	$0, 48(%rsp)
	movb	$0, 104(%rsp)
	sete	%dl
.L73:
	movl	32(%rsp), %edi
	testl	%edi, %edi
	jne	.L222
	testb	%dl, %dl
	movl	$0, (%rsp)
	jne	.L66
.L222:
	cmpl	$2, %ebp
	jne	.L223
	cmpb	$0, 152(%rsp)
	je	.L223
	movl	$0, (%rsp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L375:
	cmpl	$2, (%rsp)
	je	.L385
	testl	%ebp, %ebp
	sete	104(%rsp)
	movzbl	104(%rsp), %edi
	andb	%dil, 48(%rsp)
.L161:
	cmpl	$2, %eax
	movl	$1, %edx
	movl	$1, %r13d
	jne	.L80
.L155:
	testl	%ecx, %ecx
	jne	.L224
	testb	%dl, %dl
	je	.L224
	movl	$1, %r13d
	xorl	%eax, %eax
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L371:
	movq	112(%rsp), %rsi
	movq	%rsi, 128(%rsp)
.L160:
	testb	%cl, %cl
	leaq	.LC0(%rip), %rcx
	cmovne	%rax, %rcx
	testl	%edx, %edx
	movq	%rcx, 112(%rsp)
	je	.L386
	movq	208(%r10), %rax
	movsbl	(%rax), %eax
	movl	%eax, 32(%rsp)
	movq	216(%r10), %rax
	movsbl	(%rax), %eax
	movl	%eax, (%rsp)
	movq	192(%r10), %rax
	movsbl	(%rax), %ecx
	movl	$17, %eax
.L150:
	movq	64(%r10,%rax,8), %rax
	movl	$1, %esi
	movsbl	(%rax), %eax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L386:
	movq	152(%r10), %rax
	movsbl	(%rax), %eax
	movl	%eax, 32(%rsp)
	movq	160(%r10), %rax
	movsbl	(%rax), %eax
	movl	%eax, (%rsp)
	movq	136(%r10), %rax
	movsbl	(%rax), %ecx
	movl	$10, %eax
	jmp	.L150
.L117:
	movl	24(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L159
.L125:
	cmpl	$4, %ebp
	jne	.L116
	cmpl	$2, (%rsp)
	je	.L387
.L128:
	movq	112(%rsp), %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L207
	movq	40(%rsp), %rsi
	cmpq	%rsi, %rdi
	jnb	.L21
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	addq	%rcx, %rdx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L129:
	cmpq	%rdx, %rcx
	je	.L21
.L131:
	addq	$1, %rcx
	addq	$1, %rdi
	movb	%al, -1(%rdi)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L129
.L207:
	movq	%rdi, %rdx
	jmp	.L135
.L383:
	testl	$-3, %ebp
	je	.L388
.L170:
	leal	-4(%rbp), %eax
	movb	$0, 48(%rsp)
	cmpl	$1, %eax
	jbe	.L98
.L168:
	movq	112(%rsp), %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L100
	movq	40(%rsp), %rsi
	cmpq	%r13, %rsi
	jbe	.L21
	movq	%rsi, %rcx
	subq	%r13, %rcx
	addq	%rax, %rcx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L102:
	cmpq	%rcx, %rax
	je	.L21
.L101:
	addq	$1, %rax
	addq	$1, %r13
	movb	%dl, -1(%r13)
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L102
.L100:
	cmpl	$2, (%rsp)
	movb	$0, 48(%rsp)
	jne	.L98
	cmpq	40(%rsp), %r13
	jnb	.L21
	movb	$32, 0(%r13)
	addq	$1, %r13
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L378:
	leal	-3(%r13), %eax
	cmpl	$1, %eax
	ja	.L93
	movq	128(%rsp), %rdi
	movq	%r11, 216(%rsp)
	movq	%r10, 192(%rsp)
	movq	%r8, 208(%rsp)
	movaps	%xmm2, 176(%rsp)
	movl	%r9d, 160(%rsp)
	movl	%edx, 152(%rsp)
	call	__GI_strlen
	movq	216(%rsp), %r11
	addl	%eax, 80(%rsp)
	movq	192(%rsp), %r10
	movq	208(%rsp), %r8
	movdqa	176(%rsp), %xmm2
	movl	160(%rsp), %r9d
	movl	152(%rsp), %edx
	jmp	.L93
.L215:
	xorl	%eax, %eax
.L62:
	cmpl	$1, 32(%rsp)
	sbbl	%edi, %edi
	notl	%edi
	andl	$2, %edi
	movl	%edi, (%rsp)
	jmp	.L346
.L385:
	testl	%ebp, %ebp
	jne	.L70
	cmpl	$2, %eax
	jne	.L69
	testl	%ecx, %ecx
	je	.L156
	cmpl	$-1, %r9d
	je	.L211
	movl	32(%rsp), %edi
	movq	120(%rsp), %rax
	movl	$0, %edx
	movl	$0, (%rsp)
	movb	$1, 104(%rsp)
	movl	$1, %r13d
	testl	%edi, %edi
	movl	%eax, 80(%rsp)
	cmovne	%rax, %rdx
	jmp	.L165
.L208:
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%rdx, %r8
	jmp	.L2
.L379:
	cmpl	$-1, %r13d
	je	.L389
	cmpl	$2, (%rsp)
	je	.L215
	cmpl	$-1, %r9d
	je	.L162
	movb	$0, 48(%rsp)
	movb	$0, 104(%rsp)
	movl	$1, %ebp
	xorl	%eax, %eax
	jmp	.L163
.L377:
	movq	128(%rsp), %rdi
	movq	%r11, 216(%rsp)
	movq	%r10, 192(%rsp)
	movq	%r8, 208(%rsp)
	movaps	%xmm2, 176(%rsp)
	movl	%r9d, 160(%rsp)
	movl	%edx, 152(%rsp)
	call	__GI_strlen
	movl	152(%rsp), %edx
	addl	%eax, 80(%rsp)
	movl	160(%rsp), %r9d
	movq	208(%rsp), %r8
	movdqa	176(%rsp), %xmm2
	movq	192(%rsp), %r10
	movq	216(%rsp), %r11
	jmp	.L93
.L382:
	movq	112(%rsp), %rdi
	movq	%r11, 232(%rsp)
	movq	%r10, 224(%rsp)
	movq	%r8, 216(%rsp)
	movaps	%xmm2, 192(%rsp)
	movl	%r9d, 208(%rsp)
	movl	%edx, 176(%rsp)
	movl	%ecx, 160(%rsp)
	movl	%esi, 152(%rsp)
	call	__GI_strlen
	movl	176(%rsp), %edx
	movl	152(%rsp), %esi
	movl	160(%rsp), %ecx
	movl	208(%rsp), %r9d
	movdqa	192(%rsp), %xmm2
	addl	%eax, %edx
	movq	216(%rsp), %r8
	movq	224(%rsp), %r10
	movq	232(%rsp), %r11
	jmp	.L89
.L384:
	cmpl	$2, (%rsp)
	je	.L390
.L132:
	movq	112(%rsp), %rcx
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L207
	movq	40(%rsp), %rsi
	cmpq	%rsi, %rdi
	jnb	.L21
	movq	%rsi, %rdx
	subq	%rdi, %rdx
	addq	%rcx, %rdx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L133:
	cmpq	%rdx, %rcx
	je	.L21
.L134:
	addq	$1, %rcx
	addq	$1, %rdi
	movb	%al, -1(%rdi)
	movzbl	(%rcx), %eax
	testb	%al, %al
	jne	.L133
	jmp	.L207
.L45:
	fchs
	fstpt	256(%rsp)
.L44:
	movq	104(%r10), %rax
	movq	%rax, 128(%rsp)
	movq	112(%r10), %rax
	movzbl	(%rax), %ecx
	jmp	.L160
.L65:
	cmpl	$2, (%rsp)
	je	.L214
	movb	$0, 48(%rsp)
	movb	$0, 104(%rsp)
	movl	$1, %ebp
	jmp	.L161
.L224:
	cmpl	$2, %r13d
	jne	.L225
	xorl	%eax, %eax
	cmpb	$0, 80(%rsp)
	jne	.L80
.L225:
	movl	$2, %eax
	jmp	.L80
.L200:
	movq	%rdx, %r13
	jmp	.L91
.L137:
	subq	%rax, %rbx
	leaq	-1(%rdx), %rax
	cmpq	%rax, %r8
	ja	.L140
	leaq	-1(%r8), %rsi
	.p2align 4,,10
	.p2align 3
.L141:
	movzbl	(%rax), %ecx
	subq	$1, %rax
	movb	%cl, 1(%rax,%rbx)
	cmpq	%rax, %rsi
	jne	.L141
.L140:
	addq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L142:
	subq	$1, %rbx
	testq	%rbx, %rbx
	movb	$32, (%r8,%rbx)
	jne	.L142
	jmp	.L136
.L159:
	leal	-2(%rbp), %eax
	andl	$-3, %eax
	sete	%al
	cmpl	$1, %ebp
	sete	%dl
	orb	104(%rsp), %dl
	orl	%edx, %eax
	cmpl	$1, (%rsp)
	sete	%dl
	testb	%dl, %al
	jne	.L123
	movq	%rdi, %rcx
	jmp	.L124
.L389:
	cmpl	$2, (%rsp)
	je	.L391
	cmpl	$-1, %r9d
	je	.L164
	movb	$0, 48(%rsp)
	movb	$0, 104(%rsp)
	movl	$1, %r13d
	movl	$1, %ebp
	xorl	%eax, %eax
	jmp	.L163
.L69:
	cmpl	$-1, %r9d
	je	.L186
	movl	32(%rsp), %ebp
	movl	$0, %edx
	movb	$1, 104(%rsp)
	movl	$0, (%rsp)
	movl	$1, %r13d
	testl	%ebp, %ebp
	cmovne	120(%rsp), %edx
	xorl	%ebp, %ebp
	jmp	.L76
.L390:
	cmpq	40(%rsp), %rdi
	jnb	.L21
	movb	$32, (%rdi)
	addq	$1, %rdi
	jmp	.L132
.L380:
	testl	%ebp, %ebp
	je	.L69
.L70:
	cmpl	$1, %ebp
	movl	$1, %r13d
	sete	%dl
	testl	%ebp, %ebp
	sete	104(%rsp)
	movzbl	104(%rsp), %edi
	andb	%dil, 48(%rsp)
	jmp	.L73
.L214:
	movl	$1, %r13d
	jmp	.L62
.L368:
	cmpq	%rsi, %rdi
	jnb	.L21
	movb	$32, (%rdi)
	addq	$1, %rdi
	jmp	.L118
.L162:
	movl	32(%rsp), %eax
	testl	%eax, %eax
	je	.L392
	movq	%r8, %r13
	movb	$0, 104(%rsp)
	movl	$1, %ebp
	xorl	%r9d, %r9d
	jmp	.L170
.L387:
	cmpq	40(%rsp), %rdi
	jnb	.L21
	movb	$32, (%rdi)
	addq	$1, %rdi
	jmp	.L128
.L186:
	movq	%r8, %rdx
	movb	$1, 104(%rsp)
	movl	$0, (%rsp)
	xorl	%ebp, %ebp
	xorl	%r9d, %r9d
	jmp	.L75
.L391:
	xorl	%eax, %eax
	movl	$1, %r13d
	jmp	.L62
.L156:
	cmpl	$-1, %r9d
	je	.L211
	movl	32(%rsp), %ebp
	movl	$0, %edx
	testl	%ebp, %ebp
	cmovne	120(%rsp), %rdx
	cmpb	$0, 48(%rsp)
	je	.L337
	movzbl	48(%rsp), %eax
	movl	$0, (%rsp)
	movl	%ecx, %ebp
	movl	$0, 80(%rsp)
	movl	$1, %r13d
	movb	%al, 104(%rsp)
	jmp	.L166
.L164:
	movl	32(%rsp), %edx
	xorl	%r9d, %r9d
	movq	%r8, %r13
	movl	$1, %ebp
	movb	$0, 104(%rsp)
	testl	%edx, %edx
	jne	.L168
	movb	$0, 48(%rsp)
	jmp	.L169
.L392:
	xorl	%r9d, %r9d
	movq	%r8, %r13
	movl	$1, %ebp
	movb	$0, 48(%rsp)
	movb	$0, 104(%rsp)
	jmp	.L99
.L211:
	movl	$0, (%rsp)
	xorl	%r9d, %r9d
	movq	%r8, %rdx
	movb	$1, 104(%rsp)
	jmp	.L75
.L107:
	cmpl	$1, (%rsp)
	jne	.L112
	cmpq	40(%rsp), %r13
	jnb	.L21
	movzbl	151(%rsp), %eax
	addq	$1, %r13
	movb	%al, -1(%r13)
	jmp	.L112
.L206:
	movq	%rcx, %rdi
	jmp	.L125
.L337:
	movl	$0, (%rsp)
	movl	%ecx, %ebp
	movl	$0, 80(%rsp)
	movl	$1, %r13d
	movb	$1, 104(%rsp)
	jmp	.L88
.L388:
	movb	$0, 48(%rsp)
	jmp	.L98
.L223:
	movl	$2, (%rsp)
	jmp	.L66
.L47:
	movq	72(%rsp), %rsi
	movq	8(%rsi), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, 8(%rsi)
.L48:
	pxor	%xmm1, %xmm1
	movsd	(%rax), %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.L49
	seta	%sil
	movsd	%xmm0, 256(%rsp)
	movzbl	%sil, %esi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L49:
	xorpd	.LC5(%rip), %xmm0
	movsd	%xmm0, 256(%rsp)
	jmp	.L44
.L370:
	movl	4(%rsi), %ecx
	cmpl	$175, %ecx
	ja	.L40
	movl	%ecx, %eax
	addq	16(%rsi), %rax
	addl	$16, %ecx
	movl	%ecx, 4(%rsi)
.L41:
	movdqa	(%rax), %xmm3
	pxor	%xmm1, %xmm1
	movq	%r11, 112(%rsp)
	movq	%r10, 104(%rsp)
	movdqa	%xmm3, %xmm0
	movq	%r8, 80(%rsp)
	movl	%r9d, 40(%rsp)
	movaps	%xmm2, 48(%rsp)
	movl	%edx, 32(%rsp)
	movaps	%xmm3, (%rsp)
	call	__lttf2@PLT
	testq	%rax, %rax
	movdqa	(%rsp), %xmm3
	movl	32(%rsp), %edx
	movl	40(%rsp), %r9d
	movdqa	48(%rsp), %xmm2
	movq	80(%rsp), %r8
	movq	104(%rsp), %r10
	movq	112(%rsp), %r11
	js	.L393
	movdqa	%xmm2, %xmm1
	movdqa	%xmm3, %xmm0
	movq	%r11, 104(%rsp)
	movq	%r10, 80(%rsp)
	movq	%r8, 48(%rsp)
	movl	%r9d, 40(%rsp)
	movl	%edx, 32(%rsp)
	movaps	%xmm2, (%rsp)
	movaps	%xmm3, 256(%rsp)
	call	__lttf2@PLT
	xorl	%esi, %esi
	testq	%rax, %rax
	sets	%sil
	movq	104(%rsp), %r11
	movq	80(%rsp), %r10
	movq	48(%rsp), %r8
	movl	40(%rsp), %r9d
	movl	32(%rsp), %edx
	movdqa	(%rsp), %xmm2
	jmp	.L46
.L393:
	pxor	.LC2(%rip), %xmm3
	movaps	%xmm3, 256(%rsp)
	jmp	.L44
.L40:
	movq	72(%rsp), %rsi
	movq	8(%rsi), %rax
	movq	%rax, (%rsp)
	addq	$15, %rax
	andq	$-16, %rax
	leaq	16(%rax), %rcx
	movq	%rcx, 8(%rsi)
	jmp	.L41
	.size	__vstrfmon_l_internal, .-__vstrfmon_l_internal
	.p2align 4,,15
	.globl	___strfmon_l
	.type	___strfmon_l, @function
___strfmon_l:
	subq	$216, %rsp
	testb	%al, %al
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L396
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L396:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %r8
	xorl	%r9d, %r9d
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$32, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vstrfmon_l_internal
	addq	$216, %rsp
	ret
	.size	___strfmon_l, .-___strfmon_l
	.weak	strfmon_l
	.set	strfmon_l,___strfmon_l
	.globl	__strfmon_l
	.set	__strfmon_l,___strfmon_l
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC2:
	.long	0
	.long	0
	.long	0
	.long	-2147483648
	.align 16
.LC5:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.hidden	_IO_str_jumps
	.hidden	_IO_init_internal
	.hidden	__guess_grouping
