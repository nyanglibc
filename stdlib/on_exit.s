	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"on_exit.c"
.LC1:
	.string	"func != NULL"
	.text
	.p2align 4,,15
	.globl	__on_exit
	.type	__on_exit, @function
__on_exit:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L14
	movq	%rdi, %rbx
	movq	%rsi, %rbp
#APP
# 33 "on_exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
.L4:
	leaq	__exit_funcs(%rip), %rdi
	call	__new_exitfn
	testq	%rax, %rax
	je	.L15
	movq	%rbx, %rdi
	movq	%rbp, 16(%rax)
	movq	$2, (%rax)
#APP
# 43 "on_exit.c" 1
	xor %fs:48, %rdi
rol $2*8+1, %rdi
# 0 "" 2
#NO_APP
	movq	%rdi, 8(%rax)
#APP
# 48 "on_exit.c" 1
	movl %fs:24,%r8d
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	jne	.L8
	subl	$1, __exit_funcs_lock(%rip)
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
#APP
# 38 "on_exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	subl	$1, __exit_funcs_lock(%rip)
	movl	$-1, %r8d
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __exit_funcs_lock(%rip)
	je	.L4
	leaq	__exit_funcs_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
#APP
# 48 "on_exit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jle	.L1
.L12:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__exit_funcs_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 48 "on_exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
#APP
# 38 "on_exit.c" 1
	xchgl %eax, __exit_funcs_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	$-1, %r8d
	jle	.L1
	jmp	.L12
.L14:
	leaq	__PRETTY_FUNCTION__.7663(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$31, %edx
	call	__assert_fail
	.size	__on_exit, .-__on_exit
	.weak	on_exit
	.set	on_exit,__on_exit
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.7663, @object
	.size	__PRETTY_FUNCTION__.7663, 10
__PRETTY_FUNCTION__.7663:
	.string	"__on_exit"
	.hidden	__assert_fail
	.hidden	__lll_lock_wait_private
	.hidden	__new_exitfn
	.hidden	__exit_funcs
