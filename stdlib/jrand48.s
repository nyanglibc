	.text
	.p2align 4,,15
	.globl	jrand48
	.type	jrand48, @function
jrand48:
	subq	$24, %rsp
	leaq	__libc_drand48_data(%rip), %rsi
	leaq	8(%rsp), %rdx
	call	__jrand48_r
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	jrand48, .-jrand48
	.hidden	__jrand48_r
	.hidden	__libc_drand48_data
