	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	div
	.type	div, @function
div:
	movl	%edi, %eax
	cltd
	idivl	%esi
	salq	$32, %rdx
	movl	%eax, %eax
	orq	%rdx, %rax
	ret
	.size	div, .-div
