	.text
	.p2align 4,,15
	.globl	__mpn_extract_long_double
	.hidden	__mpn_extract_long_double
	.type	__mpn_extract_long_double, @function
__mpn_extract_long_double:
	movq	%rdi, %r8
	movl	16(%rsp), %edi
	movq	8(%rsp), %rsi
	movl	%edi, -16(%rsp)
	movzbl	-15(%rsp), %eax
	movq	%rsi, -24(%rsp)
	shrb	$7, %al
	movzbl	%al, %eax
	movl	%eax, (%rcx)
	movzwl	-16(%rsp), %eax
	movq	%rdi, %rcx
	andl	$32767, %eax
	subl	$16383, %eax
	andw	$32767, %cx
	movl	%eax, (%rdx)
	movq	%rsi, %rax
	movq	%rsi, (%r8)
	jne	.L2
	testq	%rsi, %rsi
	je	.L6
	movabsq	$9223372036854775807, %rcx
	andq	%rcx, %rax
	testq	%rax, %rax
	movq	%rax, (%r8)
	je	.L5
	bsrq	%rax, %rcx
	xorq	$63, %rcx
	salq	%cl, %rax
	movq	%rax, (%r8)
	movl	$-16382, %eax
	subl	%ecx, %eax
	movl	%eax, (%rdx)
.L4:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rsi, %rsi
	jne	.L4
	cmpw	$32767, %cx
	je	.L4
.L6:
	movl	$0, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movabsq	$-9223372036854775808, %rax
	movq	%rax, (%r8)
	movl	$-16382, (%rdx)
	movl	$1, %eax
	ret
	.size	__mpn_extract_long_double, .-__mpn_extract_long_double
