	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	atoll
	.type	atoll, @function
atoll:
	movl	$10, %edx
	xorl	%esi, %esi
	jmp	__GI_strtoll
	.size	atoll, .-atoll
