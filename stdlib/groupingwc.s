	.text
	.p2align 4,,15
	.globl	__correctly_grouped_prefixwc
	.hidden	__correctly_grouped_prefixwc
	.type	__correctly_grouped_prefixwc, @function
__correctly_grouped_prefixwc:
	testq	%rcx, %rcx
	movq	%rsi, %rax
	je	.L34
	cmpq	%rdi, %rsi
	jbe	.L35
	leaq	-4(%rax), %r8
	movl	$4, %esi
	subq	%rdi, %rsi
	cmpq	%r8, %rdi
	ja	.L34
	pushq	%rbp
	pushq	%rbx
	.p2align 4,,10
	.p2align 3
.L37:
	cmpl	-4(%rax), %edx
	je	.L4
	leaq	-8(%rax,%rsi), %r9
	andq	$-4, %r9
	negq	%r9
	leaq	-8(%rax,%r9), %r9
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	%edx, (%r8)
	je	.L4
.L5:
	subq	$4, %r8
	cmpq	%r9, %r8
	jne	.L6
.L28:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movsbq	(%rcx), %r11
	movq	%rax, %r10
	subq	%r8, %r10
	sarq	$2, %r10
	movsbl	%r11b, %r9d
	addl	$1, %r9d
	movslq	%r9d, %r9
	cmpq	%r9, %r10
	je	.L42
	movq	%r8, %rax
	jle	.L18
	leaq	4(%r8,%r11,4), %rax
.L18:
	cmpq	%rdi, %rax
	jbe	.L43
.L20:
	leaq	-4(%rax), %r8
	cmpq	%r8, %rdi
	jbe	.L37
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	-4(%r8), %r10
	movq	%rcx, %rbp
	movq	%r10, %rbx
	.p2align 4,,10
	.p2align 3
.L7:
	movsbq	1(%rbp), %r11
	testb	%r11b, %r11b
	je	.L8
	addq	$1, %rbp
	cmpb	$127, %r11b
	movq	%r10, %r9
	je	.L10
.L44:
	testb	%r11b, %r11b
	js	.L10
	cmpq	%r10, %rdi
	ja	.L28
	cmpl	%edx, -4(%r8)
	je	.L23
	leaq	-8(%r8,%rsi), %r9
	andq	$-4, %r9
	negq	%r9
	leaq	-8(%r8,%r9), %r9
	movq	%r10, %r8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	%edx, (%r8)
	je	.L16
.L12:
	subq	$4, %r8
	cmpq	%r9, %r8
	jne	.L17
	subq	%r8, %r10
	sarq	$2, %r10
	cmpq	%r10, %r11
	jge	.L28
.L26:
	movq	%rbx, %rax
	cmpq	%rdi, %rax
	ja	.L20
.L43:
	cmpq	%rdi, %rax
	cmovb	%rdi, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movsbq	0(%rbp), %r11
	movq	%r10, %r9
	cmpb	$127, %r11b
	jne	.L44
.L10:
	cmpq	%r10, %rdi
	ja	.L28
	cmpl	%edx, -4(%r8)
	je	.L13
	leaq	-8(%r8,%rsi), %r10
	andq	$-4, %r10
	negq	%r10
	leaq	-8(%r8,%r10), %r8
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	%edx, (%r9)
	je	.L13
.L14:
	subq	$4, %r9
	cmpq	%r8, %r9
	jne	.L15
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r9, %r8
.L19:
	leaq	-4(%r8), %r10
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	%r8, %rdi
	ja	.L26
.L11:
	subq	%r8, %r10
	sarq	$2, %r10
	cmpq	%r11, %r10
	je	.L19
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r10, %r8
	jmp	.L11
.L35:
	cmpq	%rdi, %rsi
	cmovb	%rdi, %rax
.L34:
	rep ret
	.size	__correctly_grouped_prefixwc, .-__correctly_grouped_prefixwc
