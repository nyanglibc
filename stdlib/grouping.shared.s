	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__correctly_grouped_prefixmb
	.hidden	__correctly_grouped_prefixmb
	.type	__correctly_grouped_prefixmb, @function
__correctly_grouped_prefixmb:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$40, %rsp
	testq	%rcx, %rcx
	movq	%rcx, 16(%rsp)
	je	.L36
	movq	%rdi, %r15
	movq	%rdx, %rdi
	movq	%rdx, 24(%rsp)
	call	__GI_strlen
	cmpq	%r15, %rbp
	jbe	.L3
	leaq	-3(%rax), %rbx
	movq	24(%rsp), %rdx
	leaq	-1(%r15), %rsi
	leaq	-2(%rax), %r12
	movq	%rbx, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	-1(%rbp), %r9
	cmpq	%r9, %r15
	ja	.L36
	movzbl	(%rdx), %ebx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	subq	$1, %r9
	cmpq	%rsi, %r9
	je	.L36
.L8:
	movzbl	-1(%r9,%rax), %r11d
	cmpb	%bl, %r11b
	jne	.L4
	movzbl	1(%rdx), %ecx
	testb	%cl, %cl
	je	.L5
	cmpb	%cl, -2(%r9,%rax)
	jne	.L4
	movq	8(%rsp), %rdi
	leaq	2(%rdx), %rcx
	leaq	(%r9,%rdi), %r8
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movzbl	(%r8), %r10d
	addq	$1, %rcx
	subq	$1, %r8
	cmpb	%dil, %r10b
	jne	.L4
.L6:
	movzbl	(%rcx), %edi
	testb	%dil, %dil
	jne	.L7
.L5:
	cmpq	%r9, %r15
	ja	.L36
	movq	16(%rsp), %rbx
	movq	%rbp, %r8
	subq	%r9, %r8
	movsbq	(%rbx), %rcx
	movq	%rcx, %rdi
	addl	$1, %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %r8
	je	.L63
	leaq	1(%r9,%rdi), %rbp
	cmovle	%r9, %rbp
.L25:
	cmpq	%r15, %rbp
	ja	.L26
.L3:
	cmpq	%r15, %rbp
	cmovb	%r15, %rbp
.L36:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L63:
	movq	%rbx, %r14
	leaq	-1(%r9), %r13
	movsbq	1(%r14), %rbx
	movq	%r13, 24(%rsp)
	testb	%bl, %bl
	je	.L11
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$1, %r14
	cmpb	$127, %bl
	movq	%r13, %r10
	je	.L13
.L65:
	testb	%bl, %bl
	js	.L13
	cmpq	%r13, %r15
	ja	.L36
	movq	%r13, %r9
	.p2align 4,,10
	.p2align 3
.L28:
	testb	%r11b, %r11b
	je	.L22
	cmpb	-1(%r9,%rax), %r11b
	jne	.L20
	leaq	1(%rdx), %rcx
	leaq	(%r9,%r12), %r8
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L21:
	movzbl	(%r8), %r10d
	addq	$1, %rcx
	subq	$1, %r8
	cmpb	%dil, %r10b
	jne	.L20
.L23:
	movzbl	(%rcx), %edi
	testb	%dil, %dil
	jne	.L21
.L22:
	subq	%r9, %r13
	cmpq	%r9, %r15
	ja	.L29
	cmpq	%rbx, %r13
	jne	.L30
.L19:
	movsbq	1(%r14), %rbx
	leaq	-1(%r9), %r13
	testb	%bl, %bl
	jne	.L64
.L11:
	movsbq	(%r14), %rbx
	movq	%r13, %r10
	cmpb	$127, %bl
	jne	.L65
.L13:
	cmpq	%r13, %r15
	ja	.L36
	.p2align 4,,10
	.p2align 3
.L27:
	testb	%r11b, %r11b
	je	.L17
	cmpb	-1(%r10,%rax), %r11b
	jne	.L15
	leaq	1(%rdx), %rcx
	leaq	(%r10,%r12), %r8
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	(%r8), %r9d
	addq	$1, %rcx
	subq	$1, %r8
	cmpb	%dil, %r9b
	jne	.L15
.L18:
	movzbl	(%rcx), %edi
	testb	%dil, %dil
	jne	.L16
.L17:
	cmpq	%r10, %r15
	ja	.L36
	movq	%r10, %r9
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	subq	$1, %r9
	cmpq	%rsi, %r9
	jne	.L28
	subq	%rsi, %r13
.L29:
	cmpq	%r13, %rbx
	jge	.L36
.L30:
	movq	24(%rsp), %rbp
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L15:
	subq	$1, %r10
	cmpq	%rsi, %r10
	jne	.L27
	jmp	.L36
	.size	__correctly_grouped_prefixmb, .-__correctly_grouped_prefixmb
