	.text
	.p2align 4,,15
	.globl	__seed48_r
	.hidden	__seed48_r
	.type	__seed48_r, @function
__seed48_r:
	movl	(%rsi), %eax
	movl	%eax, 6(%rsi)
	movzwl	4(%rsi), %eax
	movw	%ax, 10(%rsi)
	movzwl	4(%rdi), %eax
	movw	%ax, 4(%rsi)
	movzwl	2(%rdi), %eax
	movw	%ax, 2(%rsi)
	movzwl	(%rdi), %eax
	movl	$5, 20(%rsi)
	movw	%ax, (%rsi)
	movabsq	$-2383276746959945717, %rax
	movq	%rax, 12(%rsi)
	xorl	%eax, %eax
	ret
	.size	__seed48_r, .-__seed48_r
	.weak	seed48_r
	.set	seed48_r,__seed48_r
