	.text
	.p2align 4,,15
	.globl	__mpn_extract_double
	.hidden	__mpn_extract_double
	.type	__mpn_extract_double, @function
__mpn_extract_double:
	movq	%xmm0, %rsi
	movq	%xmm0, %rax
	shrq	$63, %rsi
	movl	%esi, (%rcx)
	movq	%xmm0, %rcx
	movabsq	$4503599627370495, %rsi
	andq	%rax, %rsi
	shrq	$48, %rax
	shrq	$52, %rcx
	andl	$2047, %ecx
	subl	$1023, %ecx
	testl	$32752, %eax
	movl	%ecx, (%rdx)
	movq	%rsi, (%rdi)
	jne	.L2
	testq	%rsi, %rsi
	jne	.L3
	movl	$0, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movabsq	$4503599627370496, %rax
	orq	%rax, %rsi
	movl	$1, %eax
	movq	%rsi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	bsrq	%rsi, %rcx
	movl	$-1022, %eax
	xorq	$63, %rcx
	subl	$11, %ecx
	subl	%ecx, %eax
	salq	%cl, %rsi
	movq	%rsi, (%rdi)
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.size	__mpn_extract_double, .-__mpn_extract_double
