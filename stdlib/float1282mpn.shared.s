	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mpn_extract_float128
	.hidden	__mpn_extract_float128
	.type	__mpn_extract_float128, @function
__mpn_extract_float128:
	movaps	%xmm0, -24(%rsp)
	movq	-16(%rsp), %r8
	movq	-24(%rsp), %rsi
	movq	%r8, %rax
	movdqa	-24(%rsp), %xmm1
	shrq	$63, %rax
	movl	%eax, (%rcx)
	movq	%r8, %rax
	movl	%r8d, %r8d
	shrq	$48, %rax
	pextrw	$7, %xmm1, %ecx
	andl	$32767, %eax
	subl	$16383, %eax
	movl	%eax, (%rdx)
	pextrw	$6, %xmm1, %eax
	movq	%rsi, (%rdi)
	salq	$32, %rax
	orq	%r8, %rax
	testw	$32767, %cx
	movq	%rax, 8(%rdi)
	jne	.L2
	testq	%rsi, %rsi
	je	.L10
	testq	%rax, %rax
	jne	.L4
	bsrq	%rsi, %rax
	xorq	$63, %rax
	cmpl	$14, %eax
	movl	%eax, %r8d
	jg	.L11
	movl	$15, %ecx
	movq	%rsi, %r10
	subl	%eax, %ecx
	shrq	%cl, %r10
	leal	49(%rax), %ecx
	movq	%r10, 8(%rdi)
	salq	%cl, %rsi
	movq	%rsi, (%rdi)
.L8:
	movl	$-16431, %eax
	subl	%r8d, %eax
	movl	%eax, (%rdx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movabsq	$281474976710656, %rdx
	orq	%rdx, %rax
	movq	%rax, 8(%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	testq	%rax, %rax
	je	.L12
.L4:
	bsrq	%rax, %r8
	movl	$64, %ecx
	movq	%rsi, %r9
	xorq	$63, %r8
	subl	$15, %r8d
	subl	%r8d, %ecx
	shrq	%cl, %r9
	movl	%r8d, %ecx
	salq	%cl, %rax
	salq	%cl, %rsi
	orq	%r9, %rax
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	movl	$-16382, %eax
	subl	%r8d, %eax
	movl	%eax, (%rdx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$0, (%rdx)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leal	-15(%rax), %ecx
	movq	$0, (%rdi)
	salq	%cl, %rsi
	movq	%rsi, 8(%rdi)
	jmp	.L8
	.size	__mpn_extract_float128, .-__mpn_extract_float128
