	.text
	.p2align 4,,15
	.globl	__strtof_internal
	.hidden	__strtof_internal
	.type	__strtof_internal, @function
__strtof_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____strtof_l_internal
	.size	__strtof_internal, .-__strtof_internal
	.p2align 4,,15
	.weak	strtof
	.hidden	strtof
	.type	strtof, @function
strtof:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____strtof_l_internal
	.size	strtof, .-strtof
	.weak	strtof32
	.set	strtof32,strtof
	.hidden	____strtof_l_internal
