	.text
	.p2align 4,,15
	.globl	__mpn_mod_1
	.type	__mpn_mod_1, @function
__mpn_mod_1:
	movq	%rdx, %r8
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L1
	leaq	-1(%rsi), %rcx
	movq	(%rdi,%rcx,8), %rdx
	cmpq	%r8, %rdx
	jnb	.L6
	leaq	-2(%rsi), %rcx
.L3:
	testq	%rcx, %rcx
	js	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rdi,%rcx,8), %rax
	subq	$1, %rcx
#APP
# 185 "mod_1.c" 1
	divq %r8
# 0 "" 2
#NO_APP
	cmpq	$-1, %rcx
	jne	.L4
.L1:
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%edx, %edx
	jmp	.L3
	.size	__mpn_mod_1, .-__mpn_mod_1
