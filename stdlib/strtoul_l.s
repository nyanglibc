	.text
	.p2align 4,,15
	.globl	____strtoul_l_internal
	.hidden	____strtoul_l_internal
	.type	____strtoul_l_internal, @function
____strtoul_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	xorl	%r14d, %r14d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	subq	$24, %rsp
	testl	%ecx, %ecx
	jne	.L92
.L2:
	cmpl	$1, %edx
	je	.L3
	cmpl	$36, %edx
	jbe	.L93
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movsbq	(%r12), %rcx
	movq	104(%r8), %rsi
	movq	%r12, %rbx
	testb	$32, 1(%rsi,%rcx,2)
	movq	%rcx, %rax
	je	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rbx
	movsbq	(%rbx), %rcx
	testb	$32, 1(%rsi,%rcx,2)
	movq	%rcx, %rax
	jne	.L4
.L5:
	testb	%al, %al
	je	.L44
	cmpb	$45, %al
	je	.L94
	cmpb	$43, %al
	movl	$0, 4(%rsp)
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %rbx
.L9:
	movzbl	(%rbx), %esi
	cmpb	$48, %sil
	je	.L95
	testl	%edx, %edx
	je	.L15
.L11:
	cmpl	$10, %edx
	je	.L15
	leal	-2(%rdx), %eax
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
.L13:
	leaq	__strtol_ul_rem_tab(%rip), %rdi
	leaq	__strtol_ul_max_tab(%rip), %rcx
	cltq
	cmpq	%rbx, %r15
	movq	(%rcx,%rax,8), %rcx
	movzbl	(%rdi,%rax), %eax
	movzbl	%sil, %edi
	movb	%al, 2(%rsp)
	je	.L49
	testb	%dil, %dil
	je	.L49
	movq	120+_nl_C_locobj(%rip), %r10
.L39:
	leaq	-1(%rbp), %rsi
	movq	%rbx, %r8
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	movslq	%edx, %r11
	movq	%rsi, 8(%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L97:
	jne	.L33
	cmpb	2(%rsp), %sil
	ja	.L51
.L33:
	imulq	%r11, %rax
	movzbl	%sil, %esi
	addq	%rsi, %rax
	movq	%r8, %rsi
.L30:
	movzbl	1(%rsi), %edi
	leaq	1(%rsi), %r8
	testb	%dil, %dil
	je	.L31
	cmpq	%r15, %r8
	je	.L31
.L34:
	leal	-48(%rdi), %esi
	cmpb	$9, %sil
	jbe	.L24
	testq	%rbp, %rbp
	jne	.L96
.L25:
	movq	104+_nl_C_locobj(%rip), %rsi
	testb	$4, 1(%rsi,%rdi,2)
	je	.L31
	movl	(%r10,%rdi,4), %esi
	subl	$55, %esi
.L24:
	movzbl	%sil, %edi
	cmpl	%edx, %edi
	jge	.L31
	cmpq	%rcx, %rax
	jbe	.L97
.L51:
	movq	%r8, %rsi
	movl	$1, %r9d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$1, %rbx
	movl	$1, 4(%rsp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%esi, %esi
	movb	%dil, 3(%rsp)
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	(%r8,%rsi), %edi
	cmpb	%dil, (%r14,%rsi)
	jne	.L26
	addq	$1, %rsi
	cmpq	%rbp, %rsi
	jne	.L27
.L28:
	movq	8(%rsp), %rdi
	leaq	(%r8,%rdi), %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%rbx, %r8
	je	.L7
	testq	%r13, %r13
	je	.L36
	movq	%r8, 0(%r13)
.L36:
	testl	%r9d, %r9d
	jne	.L98
	movl	4(%rsp), %ecx
	movq	%rax, %rdx
	negq	%rdx
	testl	%ecx, %ecx
	cmovne	%rdx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	%rbp, %rsi
	movzbl	3(%rsp), %edi
	jne	.L25
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L15:
	testq	%r15, %r15
	jne	.L99
	movl	$8, %eax
	movl	$10, %edx
	xorl	%ebp, %ebp
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%r12, %r8
.L7:
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1
	movq	%r8, %rdx
	subq	%r12, %rdx
	cmpq	$1, %rdx
	jle	.L38
	movsbq	-1(%r8), %rcx
	movq	120+_nl_C_locobj(%rip), %rdx
	cmpl	$88, (%rdx,%rcx,4)
	je	.L100
.L38:
	movq	%r12, 0(%r13)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L92:
	movq	8(%r8), %rcx
	movq	80(%rcx), %r15
	movzbl	(%r15), %eax
	subl	$1, %eax
	cmpb	$125, %al
	ja	.L41
	movq	72(%rcx), %r14
	cmpb	$0, (%r14)
	jne	.L2
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L95:
	testl	$-17, %edx
	jne	.L11
	movsbq	1(%rbx), %rax
	movq	120+_nl_C_locobj(%rip), %r10
	cmpl	$88, (%r10,%rax,4)
	je	.L101
	testl	%edx, %edx
	jne	.L11
	movzbl	6+__strtol_ul_rem_tab(%rip), %eax
	movq	48+__strtol_ul_max_tab(%rip), %rcx
	movl	$8, %edx
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	movl	$48, %edi
	movb	%al, 2(%rsp)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%r15d, %r15d
	jmp	.L2
.L100:
	cmpb	$48, -2(%r8)
	jne	.L38
	subq	$1, %r8
	movq	%r8, 0(%r13)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%r14, %rdi
	movb	%sil, 2(%rsp)
	call	strlen
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L49
	movzbl	(%r14), %r8d
	movzbl	2(%rsp), %esi
	cmpb	%sil, %r8b
	jne	.L16
	xorl	%edx, %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movzbl	(%rbx,%rdx), %ecx
	cmpb	%cl, (%r14,%rdx)
	jne	.L16
.L17:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L18
.L49:
	movq	%rbx, %r8
	jmp	.L7
.L98:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	movq	$-1, %rax
	jmp	.L1
.L101:
	movzbl	2(%rbx), %esi
	movl	$14, %eax
	addq	$2, %rbx
	movl	$16, %edx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	jmp	.L13
.L16:
	movzbl	%sil, %edi
	testb	%dil, %dil
	je	.L48
	movq	104+_nl_C_locobj(%rip), %r9
	movq	120+_nl_C_locobj(%rip), %r10
	movq	%rbx, %rsi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$1, %rsi
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	je	.L19
.L23:
	leal	-48(%rdi), %edx
	cmpb	$9, %dl
	jbe	.L20
	movl	%r8d, %ecx
	xorl	%edx, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L102:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	je	.L20
	movzbl	(%r14,%rdx), %ecx
.L22:
	cmpb	%cl, (%rsi,%rdx)
	je	.L102
	cmpq	%rdx, %rax
	jbe	.L20
	testb	$4, 1(%r9,%rdi,2)
	je	.L19
	cmpl	$64, (%r10,%rdi,4)
	jle	.L20
.L19:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	__correctly_grouped_prefixmb
	movzbl	(%rbx), %esi
	movq	%rax, %r15
	movl	$10, %edx
	movl	$8, %eax
	jmp	.L13
.L48:
	movq	%rbx, %rsi
	jmp	.L19
	.size	____strtoul_l_internal, .-____strtoul_l_internal
	.globl	____strtoull_l_internal
	.set	____strtoull_l_internal,____strtoul_l_internal
	.p2align 4,,15
	.weak	__strtoul_l
	.hidden	__strtoul_l
	.type	__strtoul_l, @function
__strtoul_l:
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	____strtoul_l_internal
	.size	__strtoul_l, .-__strtoul_l
	.weak	strtoull_l
	.set	strtoull_l,__strtoul_l
	.weak	__strtoull_l
	.set	__strtoull_l,__strtoul_l
	.weak	strtoul_l
	.set	strtoul_l,__strtoul_l
	.hidden	__correctly_grouped_prefixmb
	.hidden	strlen
	.hidden	_nl_C_locobj
	.hidden	__strtol_ul_max_tab
	.hidden	__strtol_ul_rem_tab
