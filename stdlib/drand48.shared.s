	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	drand48
	.type	drand48, @function
drand48:
	subq	$24, %rsp
	leaq	__libc_drand48_data(%rip), %rsi
	leaq	8(%rsp), %rdx
	movq	%rsi, %rdi
	call	__erand48_r
	movsd	8(%rsp), %xmm0
	addq	$24, %rsp
	ret
	.size	drand48, .-drand48
	.hidden	__erand48_r
	.hidden	__libc_drand48_data
