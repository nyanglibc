	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getsubopt
	.type	getsubopt, @function
getsubopt:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	(%rdi), %rbx
	cmpb	$0, (%rbx)
	je	.L10
	movq	%rsi, %r12
	movq	%rdi, 8(%rsp)
	movl	$44, %esi
	movq	%rbx, %rdi
	movq	%rdx, 16(%rsp)
	call	__strchrnul@PLT
	movq	%rax, %rdx
	movl	$61, %esi
	movq	%rbx, %rdi
	subq	%rbx, %rdx
	movq	%rax, %r15
	movq	%rax, (%rsp)
	call	__GI_memchr
	movq	(%r12), %r14
	testq	%rax, %rax
	cmove	%r15, %rax
	movq	%rax, 24(%rsp)
	testq	%r14, %r14
	je	.L4
	movq	%rax, %rbp
	xorl	%r15d, %r15d
	subq	%rbx, %rbp
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__GI_strncmp
	testl	%eax, %eax
	movl	%r15d, %r13d
	jne	.L5
	cmpb	$0, (%r14,%rbp)
	je	.L19
.L5:
	addq	$1, %r15
	movq	(%r12,%r15,8), %r14
	testq	%r14, %r14
	jne	.L8
.L4:
	movq	16(%rsp), %rax
	movq	%rbx, (%rax)
	movq	(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L20
.L9:
	movq	8(%rsp), %rax
	movq	(%rsp), %rcx
	movl	$-1, %r13d
	movq	%rcx, (%rax)
.L1:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, (%rsp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L19:
	movq	24(%rsp), %rcx
	movl	$0, %edx
	movq	%rcx, %rsi
	leaq	1(%rcx), %rax
	movq	(%rsp), %rcx
	cmpq	%rcx, %rsi
	movq	16(%rsp), %rsi
	cmove	%rdx, %rax
	movq	%rax, (%rsi)
	cmpb	$0, (%rcx)
	je	.L7
	leaq	1(%rcx), %rax
	movb	$0, (%rcx)
	movq	%rax, (%rsp)
.L7:
	movq	8(%rsp), %rax
	movq	(%rsp), %rcx
	movq	%rcx, (%rax)
	jmp	.L1
.L10:
	movl	$-1, %r13d
	jmp	.L1
	.size	getsubopt, .-getsubopt
