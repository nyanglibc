	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__srand48_r
	.hidden	__srand48_r
	.type	__srand48_r, @function
__srand48_r:
	movl	%edi, %edi
	movl	$5, 20(%rsi)
	movq	%rdi, %rax
	movw	%di, 2(%rsi)
	sarq	$16, %rax
	movw	%ax, 4(%rsi)
	movl	$13070, %eax
	movw	%ax, (%rsi)
	movabsq	$-2383276746959945717, %rax
	movq	%rax, 12(%rsi)
	xorl	%eax, %eax
	ret
	.size	__srand48_r, .-__srand48_r
	.weak	srand48_r
	.set	srand48_r,__srand48_r
