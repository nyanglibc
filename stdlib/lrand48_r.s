	.text
	.p2align 4,,15
	.globl	lrand48_r
	.hidden	lrand48_r
	.type	lrand48_r, @function
lrand48_r:
	testq	%rdi, %rdi
	je	.L2
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	jmp	__nrand48_r
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$-1, %eax
	ret
	.size	lrand48_r, .-lrand48_r
	.hidden	__nrand48_r
