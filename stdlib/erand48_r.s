	.text
	.p2align 4,,15
	.globl	__erand48_r
	.hidden	__erand48_r
	.type	__erand48_r, @function
__erand48_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %rbx
	subq	$24, %rsp
	call	__drand48_iterate
	testl	%eax, %eax
	js	.L3
	movzwl	2(%rbp), %edx
	movzwl	4(%rbp), %ecx
	movl	%edx, %eax
	sall	$4, %ecx
	sall	$20, %edx
	shrw	$12, %ax
	movzwl	%ax, %eax
	orl	%eax, %ecx
	movzwl	0(%rbp), %eax
	salq	$32, %rcx
	sall	$4, %eax
	orl	%edx, %eax
	movabsq	$4607182418800017408, %rdx
	orq	%rdx, %rcx
	orq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, 8(%rsp)
	movsd	8(%rsp), %xmm0
	subsd	.LC0(%rip), %xmm0
	movsd	%xmm0, (%rbx)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__erand48_r, .-__erand48_r
	.weak	erand48_r
	.set	erand48_r,__erand48_r
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC0:
	.long	0
	.long	1072693248
	.hidden	__drand48_iterate
