	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	strfromf128
	.type	strfromf128, @function
strfromf128:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$376, %rsp
	cmpb	$37, (%rdx)
	leaq	16(%rsp), %rax
	movq	$0, 200(%rsp)
	movaps	%xmm0, 16(%rsp)
	movq	%rax, 8(%rsp)
	jne	.L14
	movq	%rdi, %r15
	movzbl	1(%rdx), %ebx
	cmpb	$46, %bl
	je	.L3
	movsbl	%bl, %r14d
	movl	$-1, %r13d
.L4:
	leal	-65(%rbx), %eax
	cmpb	$38, %al
	ja	.L14
	movabsq	$485331304561, %rdx
	btq	%rax, %rdx
	jnc	.L14
	testq	%rsi, %rsi
	je	.L24
	leaq	-1(%rsi), %r12
	leaq	64(%rsp), %rbp
.L15:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$-1, %edx
	movl	$32768, %esi
	movq	%rbp, %rdi
	andl	$-33, %ebx
	call	_IO_no_init@PLT
	leaq	_IO_strn_jumps(%rip), %rax
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	movq	%rax, 280(%rsp)
	call	_IO_str_init_static_internal@PLT
	pxor	%xmm0, %xmm0
	leaq	32(%rsp), %rax
	movl	%r13d, 32(%rsp)
	movups	%xmm0, 36(%rsp)
	orb	$16, 45(%rsp)
	cmpb	$65, %bl
	movl	%r14d, 40(%rsp)
	je	.L16
	leaq	8(%rsp), %rcx
	movq	%rbp, %rdi
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rsi
	movq	%rax, %rdx
	call	__GI___printf_fp_l
.L17:
	addq	$240, %rbp
	cmpq	%rbp, 120(%rsp)
	je	.L1
	movq	104(%rsp), %rdx
	movb	$0, (%rdx)
.L1:
	addq	$376, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	64(%rsp), %rbp
	movl	$63, %r12d
	leaq	240(%rbp), %r15
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	8(%rsp), %rdx
	movq	%rax, %rsi
	movq	%rbp, %rdi
	call	__printf_fphex
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L3:
	movsbl	2(%rdx), %r14d
	leaq	2(%rdx), %rax
	xorl	%r13d, %r13d
	leal	-48(%r14), %edx
	movl	%r14d, %ebx
	cmpl	$9, %edx
	ja	.L4
	movl	%edx, %r13d
	movl	$2147483647, %edi
	movl	$-1, %r8d
.L5:
	movsbl	1(%rax), %r14d
	leaq	1(%rax), %r9
	leal	-48(%r14), %edx
	movl	%r14d, %ebx
	cmpl	$9, %edx
	ja	.L4
	testl	%r13d, %r13d
	js	.L6
	cmpl	$214748364, %r13d
	jg	.L33
	leal	0(%r13,%r13,4), %r13d
	movl	%edi, %ecx
	subl	%edx, %ecx
	addl	%r13d, %r13d
	cmpl	%ecx, %r13d
	jg	.L33
	addl	%edx, %r13d
.L6:
	movq	%r9, %rax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L33:
	movsbl	2(%rax), %r14d
	leal	-48(%r14), %edx
	movl	%r14d, %ebx
	cmpl	$9, %edx
	ja	.L22
	movsbl	3(%rax), %r14d
	leal	-48(%r14), %edx
	movl	%r14d, %ebx
	cmpl	$9, %edx
	ja	.L22
	movsbl	4(%rax), %r14d
	leal	-48(%r14), %edx
	movl	%r14d, %ebx
	cmpl	$9, %edx
	ja	.L22
	movsbl	5(%rax), %r14d
	leaq	5(%rax), %r9
	leal	-48(%r14), %eax
	movl	%r14d, %ebx
	cmpl	$9, %eax
	ja	.L22
	movl	%r8d, %r13d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-1, %r13d
	jmp	.L4
.L14:
	call	__GI_abort
	.size	strfromf128, .-strfromf128
	.hidden	__printf_fphex
	.hidden	_IO_strn_jumps
