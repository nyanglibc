	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__drand48_iterate
	.hidden	__drand48_iterate
	.type	__drand48_iterate, @function
__drand48_iterate:
	cmpw	$0, 14(%rsi)
	je	.L2
	movzwl	12(%rsi), %ecx
	movq	16(%rsi), %r8
.L3:
	movzwl	4(%rdi), %eax
	movzwl	(%rdi), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movzwl	2(%rdi), %edx
	sall	$16, %edx
	orq	%rdx, %rax
	imulq	%r8, %rax
	addq	%rcx, %rax
	movq	%rax, %rdx
	movw	%ax, (%rdi)
	shrq	$32, %rax
	shrq	$16, %rdx
	movw	%ax, 4(%rdi)
	xorl	%eax, %eax
	movw	%dx, 2(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movabsq	$-2383276746959945717, %rax
	movl	$5, 20(%rsi)
	movl	$11, %ecx
	movq	%rax, 12(%rsi)
	movabsq	$25214903917, %r8
	jmp	.L3
	.size	__drand48_iterate, .-__drand48_iterate
	.hidden	__libc_drand48_data
	.comm	__libc_drand48_data,24,16
