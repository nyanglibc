	.text
	.p2align 4,,15
	.globl	abs
	.type	abs, @function
abs:
	movl	%edi, %edx
	movl	%edi, %eax
	sarl	$31, %edx
	xorl	%edx, %eax
	subl	%edx, %eax
	ret
	.size	abs, .-abs
