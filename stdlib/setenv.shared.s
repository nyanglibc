	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__add_to_environ
	.hidden	__add_to_environ
	.type	__add_to_environ, @function
__add_to_environ:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r15
	pushq	%rbx
	subq	$56, %rsp
	movq	%rdi, -56(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -64(%rbp)
	movl	%ecx, -68(%rbp)
	call	__GI_strlen
	testq	%r15, %r15
	movq	%rax, %rbx
	je	.L57
.L2:
#APP
# 133 "setenv.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, envlock(%rip)
# 0 "" 2
#NO_APP
.L4:
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L32
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.L32
	xorl	%r13d, %r13d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%r15, %r13
.L9:
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	__GI_strncmp
	testl	%eax, %eax
	jne	.L7
	cmpb	$61, (%r12,%rbx)
	je	.L8
.L7:
	addq	$8, %r14
	movq	(%r14), %r12
	leaq	1(%r13), %r15
	testq	%r12, %r12
	jne	.L34
	leaq	24(,%r13,8), %rsi
.L5:
	movq	last_environ(%rip), %r13
	movq	%r13, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L54
	movq	__environ@GOTPCREL(%rip), %rax
	salq	$3, %r15
	movq	(%rax), %rsi
	cmpq	%rsi, %r13
	je	.L14
	movq	%r8, %rdi
	movq	%r15, %rdx
	call	__GI_memcpy@PLT
	movq	%rax, %r8
.L14:
	movq	__environ@GOTPCREL(%rip), %rax
	leaq	(%r8,%r15), %r14
	movq	%r8, last_environ(%rip)
	movq	$0, (%r14)
	movq	$0, 8(%r8,%r15)
	movq	%r8, (%rax)
	movq	(%r14), %rax
.L30:
	testq	%rax, %rax
	je	.L36
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L15
.L36:
	cmpq	$0, -64(%rbp)
	je	.L58
.L17:
	movq	-64(%rbp), %rax
	movq	%rax, (%r14)
.L15:
#APP
# 245 "setenv.c" 1
	movl %fs:24,%r8d
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	jne	.L29
	subl	$1, envlock(%rip)
.L1:
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$16, %esi
	xorl	%r15d, %r15d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L30
	leaq	16(,%r13,8), %rsi
	movq	%r13, %r15
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-80(%rbp), %rdi
	call	__GI_strlen
	addq	$1, %rax
	movq	%rax, -88(%rbp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L54:
#APP
# 218 "setenv.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L28
	subl	$1, envlock(%rip)
	movl	$-1, %r8d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L58:
	movq	-88(%rbp), %rax
	leaq	1(%rax,%rbx), %r12
	movq	%r12, %rdi
	call	__GI___libc_alloca_cutoff
	cmpq	$4096, %r12
	jbe	.L18
	testl	%eax, %eax
	je	.L59
.L18:
	leaq	30(%r12), %rax
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdx
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %rdi
	movq	%rax, %r13
	call	__GI_mempcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	leaq	1(%rax), %rdi
	movb	$61, (%rax)
	call	__GI_memcpy@PLT
	leaq	__GI_strcmp(%rip), %rdx
	leaq	known_values(%rip), %rsi
	movq	%r13, %rdi
	call	__GI___tfind
	testq	%rax, %rax
	je	.L22
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	jne	.L17
.L22:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	je	.L54
	movq	-64(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	__GI_memcpy@PLT
.L25:
	movq	-64(%rbp), %rdi
	leaq	__GI_strcmp(%rip), %rdx
	leaq	known_values(%rip), %rsi
	call	__GI___tsearch
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, envlock(%rip)
	je	.L4
	leaq	envlock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%eax, %eax
#APP
# 245 "setenv.c" 1
	xchgl %eax, envlock(%rip)
# 0 "" 2
#NO_APP
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	envlock(%rip), %rdi
	movl	$202, %eax
#APP
# 245 "setenv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L28:
	xorl	%eax, %eax
#APP
# 218 "setenv.c" 1
	xchgl %eax, envlock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L60
.L50:
	movl	$-1, %r8d
	jmp	.L1
.L59:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	je	.L54
	movq	%rbx, %rdx
	movq	-64(%rbp), %rbx
	movq	-56(%rbp), %rsi
	movq	%rbx, %rdi
	call	__GI_mempcpy@PLT
	movq	-88(%rbp), %rdx
	movq	-80(%rbp), %rsi
	leaq	1(%rax), %rdi
	movb	$61, (%rax)
	call	__GI_memcpy@PLT
	leaq	__GI_strcmp(%rip), %rdx
	leaq	known_values(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI___tfind
	testq	%rax, %rax
	je	.L25
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.L25
	movq	-64(%rbp), %rdi
	call	free@PLT
	movq	%rbx, -64(%rbp)
	jmp	.L17
.L60:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	envlock(%rip), %rdi
	movl	$202, %eax
#APP
# 158 "setenv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L50
	.size	__add_to_environ, .-__add_to_environ
	.p2align 4,,15
	.globl	__setenv
	.hidden	__setenv
	.type	__setenv, @function
__setenv:
	testq	%rdi, %rdi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L62
	cmpb	$0, (%rdi)
	je	.L62
	movq	%rsi, %rbp
	movl	$61, %esi
	movl	%edx, %r12d
	movq	%rdi, %rbx
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L62
	movl	%r12d, %ecx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	xorl	%edx, %edx
	jmp	__add_to_environ
	.p2align 4,,10
	.p2align 3
.L62:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__setenv, .-__setenv
	.weak	setenv
	.set	setenv,__setenv
	.p2align 4,,15
	.globl	__unsetenv
	.hidden	__unsetenv
	.type	__unsetenv, @function
__unsetenv:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L71
	cmpb	$0, (%rdi)
	je	.L71
	movl	$61, %esi
	movq	%rdi, %r12
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L71
	movq	%r12, %rdi
	call	__GI_strlen
	movq	%rax, %r13
#APP
# 276 "setenv.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L74
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, envlock(%rip)
# 0 "" 2
#NO_APP
.L75:
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.L83
.L76:
	movq	0(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L83
.L82:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI_strncmp
	testl	%eax, %eax
	jne	.L79
	cmpb	$61, (%rbx,%r13)
	je	.L92
.L79:
	addq	$8, %rbp
	movq	0(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L82
.L83:
#APP
# 296 "setenv.c" 1
	movl %fs:24,%r8d
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	jne	.L93
	subl	$1, envlock(%rip)
.L70:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L80:
	movq	8(%rax), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	testq	%rdx, %rdx
	jne	.L80
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, envlock(%rip)
	je	.L75
	leaq	envlock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%eax, %eax
#APP
# 296 "setenv.c" 1
	xchgl %eax, envlock(%rip)
# 0 "" 2
#NO_APP
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jle	.L70
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	envlock(%rip), %rdi
	movl	$202, %eax
#APP
# 296 "setenv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$22, %fs:(%rax)
	jmp	.L70
	.size	__unsetenv, .-__unsetenv
	.weak	unsetenv
	.set	unsetenv,__unsetenv
	.p2align 4,,15
	.globl	__clearenv
	.hidden	__clearenv
	.type	__clearenv, @function
__clearenv:
	pushq	%rbx
#APP
# 307 "setenv.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L95
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, envlock(%rip)
# 0 "" 2
#NO_APP
.L96:
	movq	__environ@GOTPCREL(%rip), %rbx
	movq	(%rbx), %rdi
	cmpq	%rdi, last_environ(%rip)
	jne	.L97
	testq	%rdi, %rdi
	jne	.L107
.L97:
	movq	$0, (%rbx)
#APP
# 319 "setenv.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L98
	subl	$1, envlock(%rip)
.L99:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	call	free@PLT
	movq	$0, last_environ(%rip)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
#APP
# 319 "setenv.c" 1
	xchgl %eax, envlock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L99
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	envlock(%rip), %rdi
	movl	$202, %eax
#APP
# 319 "setenv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, envlock(%rip)
	je	.L96
	leaq	envlock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L96
	.size	__clearenv, .-__clearenv
	.weak	clearenv
	.set	clearenv,__clearenv
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	subq	$8, %rsp
	call	__clearenv
	movq	free@GOTPCREL(%rip), %rsi
	movq	known_values(%rip), %rdi
	call	__GI___tdestroy
	movq	$0, known_values(%rip)
	addq	$8, %rsp
	ret
	.size	free_mem, .-free_mem
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	last_environ
	.comm	last_environ,8,8
	.local	known_values
	.comm	known_values,8,8
	.local	envlock
	.comm	envlock,4,4
	.hidden	__lll_lock_wait_private
