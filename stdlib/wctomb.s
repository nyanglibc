	.text
	.p2align 4,,15
	.globl	wctomb
	.hidden	wctomb
	.type	wctomb, @function
wctomb:
	testq	%rdi, %rdi
	pushq	%rbx
	je	.L9
	leaq	__wctomb_state(%rip), %rdx
	call	__wcrtomb
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L10
.L4:
	movq	16(%rax), %rax
	movq	$0, __wctomb_state(%rip)
	popq	%rbx
	movl	88(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L6
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L4
.L6:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L4
	.size	wctomb, .-wctomb
	.hidden	__wctomb_state
	.comm	__wctomb_state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
	.hidden	_nl_current_LC_CTYPE
	.hidden	__wcrtomb
