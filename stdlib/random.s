	.text
	.p2align 4,,15
	.type	__srandom.part.0, @function
__srandom.part.0:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 212 "random.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	ret
	.size	__srandom.part.0, .-__srandom.part.0
	.set	__setstate.part.1,__srandom.part.0
	.p2align 4,,15
	.globl	__srandom
	.type	__srandom, @function
__srandom:
	pushq	%rbx
	movl	%edi, %ebx
#APP
# 210 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L5:
	leaq	unsafe_state(%rip), %rsi
	movl	%ebx, %edi
	call	__srandom_r
#APP
# 212 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	subl	$1, lock(%rip)
.L3:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L5
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
#APP
# 212 "random.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L3
	popq	%rbx
	jmp	__srandom.part.0
	.size	__srandom, .-__srandom
	.weak	srand
	.set	srand,__srandom
	.weak	srandom
	.set	srandom,__srandom
	.p2align 4,,15
	.globl	__initstate
	.type	__initstate, @function
__initstate:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$24, %rsp
#APP
# 235 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, lock(%rip)
# 0 "" 2
#NO_APP
.L11:
	movq	16+unsafe_state(%rip), %rax
	leaq	unsafe_state(%rip), %rcx
	movl	%ebp, %edi
	leaq	-4(%rax), %rbx
	call	__initstate_r
	movl	%eax, %r8d
#APP
# 241 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, lock(%rip)
.L13:
	cmpl	$-1, %r8d
	movl	$0, %eax
	cmove	%rax, %rbx
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, lock(%rip)
	je	.L11
	leaq	lock(%rip), %rdi
	movq	%rdx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 241 "random.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 241 "random.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
	.size	__initstate, .-__initstate
	.weak	initstate
	.set	initstate,__initstate
	.p2align 4,,15
	.globl	__setstate
	.type	__setstate, @function
__setstate:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
#APP
# 261 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L17
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L18:
	movq	16+unsafe_state(%rip), %rax
	leaq	unsafe_state(%rip), %rsi
	movq	%rbp, %rdi
	leaq	-4(%rax), %rbx
	call	__setstate_r
	testl	%eax, %eax
	movl	$0, %eax
	cmovs	%rax, %rbx
#APP
# 268 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L20
	subl	$1, lock(%rip)
.L16:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L18
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
#APP
# 268 "random.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L16
	call	__setstate.part.1
	jmp	.L16
	.size	__setstate, .-__setstate
	.weak	setstate
	.set	setstate,__setstate
	.p2align 4,,15
	.globl	__random
	.hidden	__random
	.type	__random, @function
__random:
	subq	$24, %rsp
#APP
# 291 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L24
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L25:
	leaq	12(%rsp), %rsi
	leaq	unsafe_state(%rip), %rdi
	call	__random_r
#APP
# 295 "random.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L26
	subl	$1, lock(%rip)
.L27:
	movslq	12(%rsp), %rax
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L25
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%eax, %eax
#APP
# 295 "random.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L27
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 295 "random.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L27
	.size	__random, .-__random
	.weak	random
	.set	random,__random
	.local	lock
	.comm	lock,4,4
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	unsafe_state, @object
	.size	unsafe_state, 48
unsafe_state:
	.quad	randtbl+16
	.quad	randtbl+4
	.quad	randtbl+4
	.long	3
	.long	31
	.long	3
	.zero	4
	.quad	randtbl+128
	.data
	.align 32
	.type	randtbl, @object
	.size	randtbl, 128
randtbl:
	.long	3
	.long	-1726662223
	.long	379960547
	.long	1735697613
	.long	1040273694
	.long	1313901226
	.long	1627687941
	.long	-179304937
	.long	-2073333483
	.long	1780058412
	.long	-1989503057
	.long	-615974602
	.long	344556628
	.long	939512070
	.long	-1249116260
	.long	1507946756
	.long	-812545463
	.long	154635395
	.long	1388815473
	.long	-1926676823
	.long	525320961
	.long	-1009028674
	.long	968117788
	.long	-123449607
	.long	1284210865
	.long	435012392
	.long	-2017506339
	.long	-911064859
	.long	-370259173
	.long	1132637927
	.long	1398500161
	.long	-205601318
	.hidden	__random_r
	.hidden	__setstate_r
	.hidden	__initstate_r
	.hidden	__lll_lock_wait_private
	.hidden	__srandom_r
