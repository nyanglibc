	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getentropy
	.type	getentropy, @function
getentropy:
	cmpq	$256, %rsi
	movq	%rdi, %r8
	ja	.L9
	leaq	(%rdi,%rsi), %r9
	cmpq	%r9, %rdi
	jnb	.L4
	movl	$318, %r10d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L7:
	je	.L9
	addq	%rax, %r8
.L8:
	cmpq	%r8, %r9
	jbe	.L4
.L10:
	movq	%r9, %rsi
	xorl	%edx, %edx
	movq	%r8, %rdi
	subq	%r8, %rsi
	movl	%r10d, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/getentropy.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L19
	testq	%rax, %rax
	jns	.L7
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L6:
	cmpl	$4, %eax
	je	.L8
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$5, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	getentropy, .-getentropy
