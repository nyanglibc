	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtof128_internal
	.hidden	__GI___strtof128_internal
	.type	__GI___strtof128_internal, @function
__GI___strtof128_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtof128_l_internal
	.size	__GI___strtof128_internal, .-__GI___strtof128_internal
	.globl	__strtof128_internal
	.set	__strtof128_internal,__GI___strtof128_internal
	.p2align 4,,15
	.weak	__GI_strtof128
	.hidden	__GI_strtof128
	.type	__GI_strtof128, @function
__GI_strtof128:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtof128_l_internal
	.size	__GI_strtof128, .-__GI_strtof128
	.globl	strtof128
	.set	strtof128,__GI_strtof128
