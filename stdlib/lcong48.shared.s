	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	lcong48
	.type	lcong48, @function
lcong48:
	leaq	__libc_drand48_data(%rip), %rsi
	jmp	__lcong48_r
	.size	lcong48, .-lcong48
	.hidden	__lcong48_r
	.hidden	__libc_drand48_data
