	.text
	.p2align 4,,15
	.globl	__libc_secure_getenv
	.hidden	__libc_secure_getenv
	.type	__libc_secure_getenv, @function
__libc_secure_getenv:
	movl	__libc_enable_secure(%rip), %eax
	testl	%eax, %eax
	jne	.L2
	jmp	getenv
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.size	__libc_secure_getenv, .-__libc_secure_getenv
	.weak	secure_getenv
	.set	secure_getenv,__libc_secure_getenv
	.hidden	getenv
