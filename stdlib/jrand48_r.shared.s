	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__jrand48_r
	.hidden	__jrand48_r
	.type	__jrand48_r, @function
__jrand48_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__drand48_iterate
	testl	%eax, %eax
	js	.L3
	movslq	2(%rbx), %rax
	movq	%rax, 0(%rbp)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__jrand48_r, .-__jrand48_r
	.weak	jrand48_r
	.set	jrand48_r,__jrand48_r
	.hidden	__drand48_iterate
