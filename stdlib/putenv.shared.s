	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	putenv
	.type	putenv, @function
putenv:
	pushq	%rbp
	movl	$61, %esi
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	__GI_strchr
	testq	%rax, %rax
	je	.L2
	subq	%rbx, %rax
	leaq	1(%rax), %r13
	movq	%rax, %r12
	movq	%r13, %rdi
	call	__GI___libc_alloca_cutoff
	cmpq	$4096, %r13
	movq	%r12, %rsi
	movq	%rbx, %rdi
	jbe	.L3
	testl	%eax, %eax
	je	.L10
.L3:
	call	__GI___strnlen
	leaq	31(%rax), %rdx
	movq	%rbx, %rsi
	andq	$-16, %rdx
	subq	%rdx, %rsp
	movq	%rax, %rdx
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movb	$0, (%rdi,%rax)
	call	__GI_memcpy@PLT
	movl	$1, %ecx
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__add_to_environ
.L1:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
.L10:
	call	__GI___strndup
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L11
	movl	$1, %ecx
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__add_to_environ
	movq	%r12, %rdi
	movl	%eax, -36(%rbp)
	call	free@PLT
	movl	-36(%rbp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbx, %rdi
	call	__unsetenv
	leaq	-24(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
.L11:
	orl	$-1, %eax
	jmp	.L1
	.size	putenv, .-putenv
	.hidden	__unsetenv
	.hidden	__add_to_environ
