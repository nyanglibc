	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_bsearch
	.hidden	__GI_bsearch
	.type	__GI_bsearch, @function
__GI_bsearch:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	je	.L2
	movq	%rdx, %r12
	movq	%rcx, %r13
	movq	%r8, %r14
	xorl	%ebp, %ebp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	je	.L1
	leaq	1(%rbx), %rbp
	cmpq	%rbp, %r12
	jbe	.L2
.L5:
	leaq	0(%rbp,%r12), %rbx
	movq	(%rsp), %rdi
	shrq	%rbx
	movq	%rbx, %r15
	imulq	%r13, %r15
	addq	8(%rsp), %r15
	movq	%r15, %rsi
	call	*%r14
	testl	%eax, %eax
	jns	.L16
	movq	%rbx, %r12
	cmpq	%rbp, %r12
	ja	.L5
.L2:
	xorl	%r15d, %r15d
.L1:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI_bsearch, .-__GI_bsearch
	.globl	bsearch
	.set	bsearch,__GI_bsearch
