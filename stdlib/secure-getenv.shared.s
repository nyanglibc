	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __libc_secure_getenv,__secure_getenv@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_secure_getenv
	.hidden	__GI___libc_secure_getenv
	.type	__GI___libc_secure_getenv, @function
__GI___libc_secure_getenv:
	movq	__libc_enable_secure@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2
	jmp	__GI_getenv
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	ret
	.size	__GI___libc_secure_getenv, .-__GI___libc_secure_getenv
	.weak	secure_getenv
	.set	secure_getenv,__GI___libc_secure_getenv
	.weak	__libc_secure_getenv
	.set	__libc_secure_getenv,__GI___libc_secure_getenv
