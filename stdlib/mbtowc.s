	.text
	.p2align 4,,15
	.globl	mbtowc
	.type	mbtowc, @function
mbtowc:
	testq	%rsi, %rsi
	pushq	%rbx
	je	.L12
	cmpb	$0, (%rsi)
	jne	.L6
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L1
	movl	$0, (%rdi)
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	state.9344(%rip), %rcx
	call	__mbrtowc
	movl	$-1, %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L13
.L4:
	movq	$0, state.9344(%rip)
	movq	(%rax), %rax
	popq	%rbx
	movl	88(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L7
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L4
.L7:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L4
	.size	mbtowc, .-mbtowc
	.local	state.9344
	.comm	state.9344,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
	.hidden	_nl_current_LC_CTYPE
	.hidden	__mbrtowc
