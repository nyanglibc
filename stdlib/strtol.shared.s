	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtol_internal
	.hidden	__GI___strtol_internal
	.type	__GI___strtol_internal, @function
__GI___strtol_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__GI_____strtol_l_internal
	.size	__GI___strtol_internal, .-__GI___strtol_internal
	.globl	__strtol_internal
	.set	__strtol_internal,__GI___strtol_internal
	.globl	__GI___strtoll_internal
	.set	__GI___strtoll_internal,__strtol_internal
	.globl	__strtoll_internal
	.set	__strtoll_internal,__strtol_internal
	.p2align 4,,15
	.globl	__strtol
	.type	__strtol, @function
__strtol:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	__GI_____strtol_l_internal
	.size	__strtol, .-__strtol
	.weak	__GI_strtol
	.hidden	__GI_strtol
	.set	__GI_strtol,__strtol
	.weak	strtol
	.set	strtol,__GI_strtol
	.weak	strtoimax
	.set	strtoimax,strtol
	.globl	__GI_strtoq
	.set	__GI_strtoq,strtol
	.weak	strtoq
	.set	strtoq,strtol
	.globl	__GI_strtoll
	.set	__GI_strtoll,strtol
	.weak	strtoll
	.set	strtoll,strtol
