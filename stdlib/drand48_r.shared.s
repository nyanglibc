	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	drand48_r
	.type	drand48_r, @function
drand48_r:
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	jmp	__erand48_r
	.size	drand48_r, .-drand48_r
	.hidden	__erand48_r
