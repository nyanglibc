	.text
	.globl	_tens_in_limb
	.section	.rodata
	.align 32
	.type	_tens_in_limb, @object
	.size	_tens_in_limb, 160
_tens_in_limb:
	.quad	0
	.quad	10
	.quad	100
	.quad	1000
	.quad	10000
	.quad	100000
	.quad	1000000
	.quad	10000000
	.quad	100000000
	.quad	1000000000
	.quad	10000000000
	.quad	100000000000
	.quad	1000000000000
	.quad	10000000000000
	.quad	100000000000000
	.quad	1000000000000000
	.quad	10000000000000000
	.quad	100000000000000000
	.quad	1000000000000000000
	.quad	-8446744073709551616
