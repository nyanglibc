	.text
	.p2align 4,,15
	.globl	____strtol_l_internal
	.hidden	____strtol_l_internal
	.type	____strtol_l_internal, @function
____strtol_l_internal:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	xorl	%r14d, %r14d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %r13
	subq	$24, %rsp
	testl	%ecx, %ecx
	jne	.L99
.L2:
	cmpl	$1, %edx
	je	.L3
	cmpl	$36, %edx
	jbe	.L100
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movsbq	(%r12), %rcx
	movq	104(%r8), %rsi
	movq	%r12, %rbx
	testb	$32, 1(%rsi,%rcx,2)
	movq	%rcx, %rax
	je	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rbx
	movsbq	(%rbx), %rcx
	testb	$32, 1(%rsi,%rcx,2)
	movq	%rcx, %rax
	jne	.L4
.L5:
	testb	%al, %al
	je	.L48
	cmpb	$45, %al
	je	.L101
	cmpb	$43, %al
	movl	$0, 4(%rsp)
	sete	%al
	movzbl	%al, %eax
	addq	%rax, %rbx
.L9:
	movzbl	(%rbx), %esi
	cmpb	$48, %sil
	je	.L102
	testl	%edx, %edx
	je	.L15
.L11:
	cmpl	$10, %edx
	je	.L15
	leal	-2(%rdx), %eax
	leaq	__strtol_ul_rem_tab(%rip), %rcx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	cltq
	movzbl	(%rcx,%rax), %ecx
	movb	%cl, 2(%rsp)
	leaq	__strtol_ul_max_tab(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
.L13:
	cmpq	%rbx, %r15
	movzbl	%sil, %edi
	je	.L7
	testb	%dil, %dil
	je	.L7
	movq	120+_nl_C_locobj(%rip), %r10
.L14:
	leaq	-1(%rbp), %rsi
	movq	%rbx, %r8
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	movslq	%edx, %r11
	movq	%rsi, 8(%rsp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L104:
	jne	.L33
	cmpb	2(%rsp), %sil
	ja	.L54
.L33:
	imulq	%r11, %rax
	movzbl	%sil, %esi
	addq	%rsi, %rax
	movq	%r8, %rsi
.L30:
	leaq	1(%rsi), %r8
	movzbl	1(%rsi), %edi
	cmpq	%r8, %r15
	je	.L31
	testb	%dil, %dil
	je	.L31
.L34:
	leal	-48(%rdi), %esi
	cmpb	$9, %sil
	jbe	.L24
	testq	%rbp, %rbp
	jne	.L103
.L25:
	movq	104+_nl_C_locobj(%rip), %rsi
	testb	$4, 1(%rsi,%rdi,2)
	je	.L31
	movl	(%r10,%rdi,4), %esi
	subl	$55, %esi
.L24:
	movzbl	%sil, %edi
	cmpl	%edx, %edi
	jge	.L31
	cmpq	%rcx, %rax
	jbe	.L104
.L54:
	movq	%r8, %rsi
	movl	$1, %r9d
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L101:
	addq	$1, %rbx
	movl	$1, 4(%rsp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L103:
	xorl	%esi, %esi
	movb	%dil, 3(%rsp)
	.p2align 4,,10
	.p2align 3
.L27:
	movzbl	(%r8,%rsi), %edi
	cmpb	%dil, (%r14,%rsi)
	jne	.L26
	addq	$1, %rsi
	cmpq	%rbp, %rsi
	jne	.L27
.L28:
	movq	8(%rsp), %rdi
	leaq	(%r8,%rdi), %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%rbx, %r8
	je	.L7
	testq	%r13, %r13
	je	.L36
	movq	%r8, 0(%r13)
.L36:
	testl	%r9d, %r9d
	je	.L105
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$0, 4(%rsp)
	movabsq	$-9223372036854775808, %rdx
	movl	$34, %fs:(%rax)
	movabsq	$9223372036854775807, %rax
	cmovne	%rdx, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	%rsi, %rbp
	movzbl	3(%rsp), %edi
	jne	.L25
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L15:
	testq	%r15, %r15
	jne	.L106
	movabsq	$1844674407370955161, %rcx
	movb	$5, 2(%rsp)
	movl	$10, %edx
	xorl	%ebp, %ebp
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r12, %rbx
.L7:
	xorl	%eax, %eax
	testq	%r13, %r13
	je	.L1
	movq	%rbx, %rdx
	subq	%r12, %rdx
	cmpq	$1, %rdx
	jle	.L40
	movsbq	-1(%rbx), %rcx
	movq	120+_nl_C_locobj(%rip), %rdx
	cmpl	$88, (%rdx,%rcx,4)
	je	.L107
.L40:
	movq	%r12, 0(%r13)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L99:
	movq	8(%r8), %rcx
	movq	80(%rcx), %r15
	movzbl	(%r15), %eax
	subl	$1, %eax
	cmpb	$125, %al
	ja	.L45
	movq	72(%rcx), %r14
	cmpb	$0, (%r14)
	jne	.L2
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L105:
	movl	4(%rsp), %edx
	testl	%edx, %edx
	je	.L108
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rax
	ja	.L109
	negq	%rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L102:
	testl	$-17, %edx
	jne	.L11
	movsbq	1(%rbx), %rax
	movq	120+_nl_C_locobj(%rip), %r10
	cmpl	$88, (%r10,%rax,4)
	je	.L110
	testl	%edx, %edx
	jne	.L11
	movb	$7, 2(%rsp)
	movabsq	$2305843009213693951, %rcx
	movl	$8, %edx
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	movl	$48, %edi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%r15d, %r15d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L108:
	testq	%rax, %rax
	jns	.L1
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	movabsq	$9223372036854775807, %rax
	jmp	.L1
.L107:
	cmpb	$48, -2(%rbx)
	jne	.L40
	subq	$1, %rbx
	movq	%rbx, 0(%r13)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r14, %rdi
	movb	%sil, 2(%rsp)
	call	strlen
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L7
	movzbl	(%r14), %r8d
	movzbl	2(%rsp), %esi
	cmpb	%sil, %r8b
	jne	.L16
	xorl	%edx, %edx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	movzbl	(%rbx,%rdx), %ecx
	cmpb	%cl, (%r14,%rdx)
	jne	.L16
.L17:
	addq	$1, %rdx
	cmpq	%rax, %rdx
	jne	.L18
	jmp	.L7
.L110:
	movzbl	2(%rbx), %esi
	movabsq	$1152921504606846975, %rcx
	addq	$2, %rbx
	movb	$15, 2(%rsp)
	movl	$16, %edx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	jmp	.L13
.L16:
	movzbl	%sil, %edi
	testb	%dil, %dil
	je	.L52
	movq	104+_nl_C_locobj(%rip), %r9
	movq	120+_nl_C_locobj(%rip), %r10
	movq	%rbx, %rsi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$1, %rsi
	movzbl	(%rsi), %edi
	testb	%dil, %dil
	je	.L19
.L23:
	leal	-48(%rdi), %edx
	cmpb	$9, %dl
	jbe	.L20
	movl	%r8d, %ecx
	xorl	%edx, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$1, %rdx
	cmpq	%rax, %rdx
	je	.L20
	movzbl	(%r14,%rdx), %ecx
.L22:
	cmpb	%cl, (%rsi,%rdx)
	je	.L111
	cmpq	%rdx, %rax
	jbe	.L20
	testb	$4, 1(%r9,%rdi,2)
	je	.L19
	cmpl	$64, (%r10,%rdi,4)
	jle	.L20
.L19:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	__correctly_grouped_prefixmb
	movzbl	(%rbx), %esi
	movq	%rax, %r15
	movabsq	$1844674407370955161, %rcx
	movb	$5, 2(%rsp)
	movl	$10, %edx
	jmp	.L13
.L109:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	movq	%rdx, %rax
	jmp	.L1
.L52:
	movq	%rbx, %rsi
	jmp	.L19
	.size	____strtol_l_internal, .-____strtol_l_internal
	.globl	____strtoll_l_internal
	.set	____strtoll_l_internal,____strtol_l_internal
	.p2align 4,,15
	.weak	__strtol_l
	.hidden	__strtol_l
	.type	__strtol_l, @function
__strtol_l:
	movq	%rcx, %r8
	xorl	%ecx, %ecx
	jmp	____strtol_l_internal
	.size	__strtol_l, .-__strtol_l
	.weak	strtoll_l
	.set	strtoll_l,__strtol_l
	.weak	__strtoll_l
	.set	__strtoll_l,__strtol_l
	.weak	strtol_l
	.set	strtol_l,__strtol_l
	.hidden	__strtol_ul_rem_tab
	.globl	__strtol_ul_rem_tab
	.section	.rodata
	.align 32
	.type	__strtol_ul_rem_tab, @object
	.size	__strtol_ul_rem_tab, 35
__strtol_ul_rem_tab:
	.byte	1
	.byte	0
	.byte	3
	.byte	0
	.byte	3
	.byte	1
	.byte	7
	.byte	6
	.byte	5
	.byte	4
	.byte	3
	.byte	2
	.byte	1
	.byte	0
	.byte	15
	.byte	0
	.byte	15
	.byte	16
	.byte	15
	.byte	15
	.byte	15
	.byte	5
	.byte	15
	.byte	15
	.byte	15
	.byte	24
	.byte	15
	.byte	23
	.byte	15
	.byte	15
	.byte	31
	.byte	15
	.byte	17
	.byte	15
	.byte	15
	.hidden	__strtol_ul_max_tab
	.globl	__strtol_ul_max_tab
	.align 32
	.type	__strtol_ul_max_tab, @object
	.size	__strtol_ul_max_tab, 280
__strtol_ul_max_tab:
	.quad	9223372036854775807
	.quad	6148914691236517205
	.quad	4611686018427387903
	.quad	3689348814741910323
	.quad	3074457345618258602
	.quad	2635249153387078802
	.quad	2305843009213693951
	.quad	2049638230412172401
	.quad	1844674407370955161
	.quad	1676976733973595601
	.quad	1537228672809129301
	.quad	1418980313362273201
	.quad	1317624576693539401
	.quad	1229782938247303441
	.quad	1152921504606846975
	.quad	1085102592571150095
	.quad	1024819115206086200
	.quad	970881267037344821
	.quad	922337203685477580
	.quad	878416384462359600
	.quad	838488366986797800
	.quad	802032351030850070
	.quad	768614336404564650
	.quad	737869762948382064
	.quad	709490156681136600
	.quad	683212743470724133
	.quad	658812288346769700
	.quad	636094623231363848
	.quad	614891469123651720
	.quad	595056260442243600
	.quad	576460752303423487
	.quad	558992244657865200
	.quad	542551296285575047
	.quad	527049830677415760
	.quad	512409557603043100
	.hidden	__correctly_grouped_prefixmb
	.hidden	strlen
	.hidden	_nl_C_locobj
