	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_getenv
	.hidden	__GI_getenv
	.type	__GI_getenv, @function
__GI_getenv:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.L10
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L10
	cmpb	$0, 1(%rdi)
	movq	0(%rbp), %rbx
	jne	.L3
	orb	$61, %ah
	testq	%rbx, %rbx
	jne	.L6
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rbp
	movq	0(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1
.L6:
	cmpw	(%rbx), %ax
	jne	.L5
	addq	$2, %rbx
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rdi, %r13
	addq	$2, %r13
	call	__GI_strlen
	movzwl	-2(%r13), %r12d
	testq	%rbx, %rbx
	movq	%rax, %r15
	leaq	-2(%rax), %r14
	jne	.L8
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rbp
	movq	0(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L1
.L8:
	cmpw	(%rbx), %r12w
	jne	.L7
	leaq	2(%rbx), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	__GI_strncmp
	testl	%eax, %eax
	jne	.L7
	cmpb	$61, (%rbx,%r15)
	jne	.L7
	leaq	1(%rbx,%r15), %rbx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%ebx, %ebx
	jmp	.L1
	.size	__GI_getenv, .-__GI_getenv
	.globl	getenv
	.set	getenv,__GI_getenv
