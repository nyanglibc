	.text
	.p2align 4,,15
	.globl	__setstate_r
	.hidden	__setstate_r
	.type	__setstate_r, @function
__setstate_r:
	testq	%rdi, %rdi
	leaq	4(%rdi), %r10
	je	.L2
	testq	%rsi, %rsi
	je	.L2
	movl	24(%rsi), %ecx
	movq	16(%rsi), %rdx
	testl	%ecx, %ecx
	je	.L11
	movq	8(%rsi), %rax
	subq	%rdx, %rax
	sarq	$2, %rax
	leal	(%rax,%rax,4), %eax
	addl	%eax, %ecx
	movl	%ecx, -4(%rdx)
.L4:
	movl	(%rdi), %r8d
	movl	$1717986919, %ecx
	movl	%r8d, %eax
	imull	%ecx
	movl	%r8d, %eax
	sarl	$31, %eax
	sarl	%edx
	subl	%eax, %edx
	leal	(%rdx,%rdx,4), %eax
	movl	%r8d, %edx
	subl	%eax, %edx
	cmpl	$4, %edx
	ja	.L2
	leaq	random_poly_info(%rip), %rax
	movslq	%edx, %r9
	testl	%edx, %edx
	movl	%edx, 24(%rsi)
	movslq	20(%rax,%r9,4), %r8
	movl	(%rax,%r9,4), %r9d
	movl	%r9d, 32(%rsi)
	movl	%r8d, 28(%rsi)
	je	.L5
	movl	(%rdi), %edi
	movl	%edi, %eax
	sarl	$31, %edi
	imull	%ecx
	sarl	%edx
	subl	%edi, %edx
	movslq	%edx, %rax
	leaq	(%r10,%rax,4), %rax
	movq	%rax, 8(%rsi)
	leal	(%r9,%rdx), %eax
	cltd
	idivl	%r8d
	movslq	%edx, %rax
	leaq	(%r10,%rax,4), %rax
	movq	%rax, (%rsi)
.L5:
	leaq	(%r10,%r8,4), %rax
	movq	%r10, 16(%rsi)
	movq	%rax, 40(%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$0, -4(%rdx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__setstate_r, .-__setstate_r
	.weak	setstate_r
	.set	setstate_r,__setstate_r
	.p2align 4,,15
	.globl	__random_r
	.hidden	__random_r
	.type	__random_r, @function
__random_r:
	testq	%rdi, %rdi
	je	.L13
	testq	%rsi, %rsi
	je	.L13
	movl	24(%rdi), %eax
	movq	16(%rdi), %r8
	testl	%eax, %eax
	je	.L19
	movq	(%rdi), %rdx
	movq	8(%rdi), %rcx
	movq	40(%rdi), %r9
	movl	(%rcx), %eax
	addl	(%rdx), %eax
	addq	$4, %rdx
	addq	$4, %rcx
	movl	%eax, -4(%rdx)
	shrl	%eax
	cmpq	%rdx, %r9
	movl	%eax, (%rsi)
	ja	.L20
	movq	%r8, (%rdi)
	movq	%rcx, 8(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	%rcx, %r9
	ja	.L18
	movq	%r8, %rcx
.L18:
	movq	%rdx, %r8
	movq	%rcx, 8(%rdi)
	xorl	%eax, %eax
	movq	%r8, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	imull	$1103515245, (%r8), %edx
	addl	$12345, %edx
	andl	$2147483647, %edx
	movl	%edx, (%r8)
	movl	%edx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__random_r, .-__random_r
	.weak	random_r
	.set	random_r,__random_r
	.p2align 4,,15
	.globl	__srandom_r
	.hidden	__srandom_r
	.type	__srandom_r, @function
__srandom_r:
	testq	%rsi, %rsi
	je	.L31
	movl	24(%rsi), %eax
	cmpl	$4, %eax
	ja	.L31
	movq	16(%rsi), %r9
	testl	%edi, %edi
	movl	$1, %ecx
	cmovne	%edi, %ecx
	testl	%eax, %eax
	movl	%ecx, (%r9)
	je	.L38
	pushq	%rbx
	subq	$16, %rsp
	movslq	28(%rsi), %r10
	cmpq	$1, %r10
	movq	%r10, %rbx
	jle	.L25
	movl	$1, %r8d
	movl	$-2092037281, %r11d
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%ecx, %eax
	imull	%r11d
	movl	%ecx, %eax
	sarl	$31, %eax
	addl	%ecx, %edx
	sarl	$16, %edx
	subl	%eax, %edx
	imull	$127773, %edx, %eax
	imull	$-2836, %edx, %edx
	subl	%eax, %ecx
	imull	$16807, %ecx, %ecx
	addl	%ecx, %edx
	leal	2147483647(%rdx), %ecx
	testl	%edx, %edx
	cmovns	%edx, %ecx
	movl	%ecx, (%r9,%r8,4)
	addq	$1, %r8
	cmpq	%r10, %r8
	jne	.L27
.L25:
	movslq	32(%rsi), %rax
	movq	%r9, 8(%rsi)
	leaq	(%r9,%rax,4), %rax
	movq	%rax, (%rsi)
	leal	(%rbx,%rbx,4), %eax
	leal	-1(%rax,%rax), %r10d
	testl	%r10d, %r10d
	js	.L28
	leaq	12(%rsp), %rbx
	movq	%rsi, %r11
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rbx, %rsi
	movq	%r11, %rdi
	call	__random_r
	subl	$1, %r10d
	cmpl	$-1, %r10d
	jne	.L29
.L28:
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%eax, %eax
	ret
.L31:
.L22:
	movl	$-1, %eax
	ret
	.size	__srandom_r, .-__srandom_r
	.weak	srandom_r
	.set	srandom_r,__srandom_r
	.p2align 4,,15
	.globl	__initstate_r
	.hidden	__initstate_r
	.type	__initstate_r, @function
__initstate_r:
	testq	%rcx, %rcx
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L40
	movq	16(%rcx), %rax
	testq	%rax, %rax
	je	.L41
	movl	24(%rcx), %r9d
	testl	%r9d, %r9d
	jne	.L42
	movl	$0, -4(%rax)
.L41:
	cmpq	$127, %rdx
	movq	%rcx, %rbx
	movq	%rsi, %rbp
	ja	.L58
.L43:
	cmpq	$31, %rdx
	ja	.L46
	cmpq	$7, %rdx
	jbe	.L40
	leaq	4(%rsi), %rax
	movq	$0, 24(%rcx)
	movl	$0, 32(%rcx)
	movq	%rcx, %rsi
	movq	%rax, 40(%rcx)
	movq	%rax, 16(%rcx)
	call	__srandom_r
	movl	$0, 0(%rbp)
	xorl	%eax, %eax
.L39:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	cmpq	$256, %rdx
	sbbl	%ecx, %ecx
	andl	$-32, %ecx
	addl	$63, %ecx
	cmpq	$256, %rdx
	sbbl	%esi, %esi
	andl	$2, %esi
	addl	$1, %esi
	cmpq	$256, %rdx
	sbbq	%rax, %rax
	andq	$-128, %rax
	addq	$252, %rax
	cmpq	$256, %rdx
	sbbl	%r12d, %r12d
	addl	$4, %r12d
	movl	%r12d, %edx
.L45:
	leaq	4(%rbp), %r13
	movl	%esi, 32(%rbx)
	movl	%edx, 24(%rbx)
	movl	%ecx, 28(%rbx)
	movq	%rbx, %rsi
	addq	%r13, %rax
	movq	%r13, 16(%rbx)
	movq	%rax, 40(%rbx)
	call	__srandom_r
	movq	8(%rbx), %rax
	subq	%r13, %rax
	sarq	$2, %rax
	leal	(%rax,%rax,4), %eax
	addl	%r12d, %eax
	movl	%eax, 0(%rbp)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movq	8(%rcx), %r8
	movq	%rcx, %rbx
	movq	%rsi, %rbp
	subq	%rax, %r8
	sarq	$2, %r8
	leal	(%r8,%r8,4), %r8d
	addl	%r8d, %r9d
	cmpq	$127, %rdx
	movl	%r9d, -4(%rax)
	jbe	.L43
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L46:
	cmpq	$64, %rdx
	sbbl	%ecx, %ecx
	andl	$-8, %ecx
	addl	$15, %ecx
	cmpq	$64, %rdx
	sbbl	%esi, %esi
	andl	$2, %esi
	addl	$1, %esi
	cmpq	$64, %rdx
	sbbq	%rax, %rax
	andq	$-32, %rax
	addq	$60, %rax
	cmpq	$64, %rdx
	sbbl	%r12d, %r12d
	addl	$2, %r12d
	movl	%r12d, %edx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L39
	.size	__initstate_r, .-__initstate_r
	.weak	initstate_r
	.set	initstate_r,__initstate_r
	.section	.rodata
	.align 32
	.type	random_poly_info, @object
	.size	random_poly_info, 40
random_poly_info:
	.long	0
	.long	3
	.long	1
	.long	3
	.long	1
	.long	0
	.long	7
	.long	15
	.long	31
	.long	63
