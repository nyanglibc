	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__run_exit_handlers
	.hidden	__run_exit_handlers
	.type	__run_exit_handlers, @function
__run_exit_handlers:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
	testb	%cl, %cl
	movl	%edx, 12(%rsp)
	jne	.L32
.L2:
	movq	__exit_funcs_lock@GOTPCREL(%rip), %r14
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L3:
#APP
# 56 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L4
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r14)
# 0 "" 2
#NO_APP
.L17:
	movq	0(%rbp), %r15
	testq	%r15, %r15
	jne	.L6
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	$4, %rdx
	je	.L13
	cmpq	$2, %rdx
	jne	.L11
	movq	24(%rax), %rdx
	movq	32(%rax), %rsi
	movl	%ebx, %edi
#APP
# 89 "exit.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	call	*%rdx
.L11:
#APP
# 112 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L15
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r14)
# 0 "" 2
#NO_APP
.L16:
	cmpq	%r13, __new_exitfn_called(%rip)
	jne	.L17
.L6:
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.L34
	leaq	-1(%rax), %r9
	movq	__new_exitfn_called(%rip), %r13
	movq	%r9, 8(%r15)
#APP
# 76 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
	subl	$1, (%r14)
.L10:
	movq	%r9, %rax
	salq	$5, %rax
	addq	%r15, %rax
	movq	16(%rax), %rdx
	cmpq	$3, %rdx
	jne	.L35
	movq	24(%rax), %rax
#APP
# 96 "exit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movq	24(%rax), %rdx
	movq	$0, 16(%rax)
	movl	%ebx, %esi
#APP
# 106 "exit.c" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	movq	32(%rax), %rdi
	call	*%rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L15:
	movl	%r12d, %eax
	lock cmpxchgl	%edx, (%r14)
	je	.L16
	movq	%r14, %rdi
	call	__lll_lock_wait_private
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%r12d, %eax
#APP
# 76 "exit.c" 1
	xchgl %eax, (%r14)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L10
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r14, %rdi
	movl	$202, %eax
#APP
# 76 "exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%r15), %rax
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L19
	movq	%r15, %rdi
	call	free@PLT
.L19:
#APP
# 126 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L20
	subl	$1, (%r14)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L33:
	movb	$1, __exit_funcs_done(%rip)
#APP
# 66 "exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	subl	$1, (%r14)
.L8:
	cmpb	$0, 12(%rsp)
	je	.L22
	leaq	__start___libc_atexit(%rip), %rbp
	leaq	__stop___libc_atexit(%rip), %rax
	cmpq	%rax, %rbp
	jnb	.L22
	leaq	8(%rbp), %rdx
	addq	$7, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbp,%rax,8), %r12
	.p2align 4,,10
	.p2align 3
.L23:
	call	*0(%rbp)
	addq	$8, %rbp
	cmpq	%rbp, %r12
	jne	.L23
.L22:
	movl	%ebx, %edi
	call	__GI__exit
.L4:
	movl	%r12d, %eax
	lock cmpxchgl	%edx, (%r14)
	je	.L17
	movq	%r14, %rdi
	call	__lll_lock_wait_private
	jmp	.L17
.L20:
	movl	%r12d, %eax
#APP
# 126 "exit.c" 1
	xchgl %eax, (%r14)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L3
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r14, %rdi
	movl	$202, %eax
#APP
# 126 "exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L3
.L32:
	call	__GI___call_tls_dtors
	jmp	.L2
.L7:
	xorl	%eax, %eax
#APP
# 66 "exit.c" 1
	xchgl %eax, (%r14)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L8
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r14, %rdi
	movl	$202, %eax
#APP
# 66 "exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L8
	.size	__run_exit_handlers, .-__run_exit_handlers
	.p2align 4,,15
	.globl	__GI_exit
	.hidden	__GI_exit
	.type	__GI_exit, @function
__GI_exit:
	leaq	__exit_funcs(%rip), %rsi
	subq	$8, %rsp
	movl	$1, %ecx
	movl	$1, %edx
	call	__run_exit_handlers
	.size	__GI_exit, .-__GI_exit
	.globl	exit
	.set	exit,__GI_exit
	.hidden	__exit_funcs_done
	.globl	__exit_funcs_done
	.bss
	.type	__exit_funcs_done, @object
	.size	__exit_funcs_done, 1
__exit_funcs_done:
	.zero	1
	.hidden	__exit_funcs
	.hidden	__stop___libc_atexit
	.hidden	__start___libc_atexit
	.hidden	__lll_lock_wait_private
	.hidden	__new_exitfn_called
