 .text
.globl __mpn_sub_n
.type __mpn_sub_n,@function
.align 1<<4
__mpn_sub_n:
 xor %r8, %r8
 mov (%rsi), %r10
 mov (%rdx), %r11
 lea -8(%rsi,%rcx,8), %rsi
 lea -8(%rdx,%rcx,8), %rdx
 lea -16(%rdi,%rcx,8), %rdi
 mov %ecx, %eax
 neg %rcx
 and $3, %eax
 je .Lb00
 add %rax, %rcx
 cmp $2, %eax
 jl .Lb01
 je .Lb10
.Lb11: shr %r8
 jmp .Le11
.Lb00: shr %r8
 mov %r10, %r8
 mov %r11, %r9
 lea 4(%rcx), %rcx
 jmp .Le00
.Lb01: shr %r8
 jmp .Le01
.Lb10: shr %r8
 mov %r10, %r8
 mov %r11, %r9
 jmp .Le10
.Lend: sbb %r11, %r10
 mov %r10, 8(%rdi)
 mov %ecx, %eax
 adc %eax, %eax
 ret
 .p2align 4
.Ltop:
 mov -24(%rsi,%rcx,8), %r8
 mov -24(%rdx,%rcx,8), %r9
 sbb %r11, %r10
 mov %r10, -24(%rdi,%rcx,8)
.Le00:
 mov -16(%rsi,%rcx,8), %r10
 mov -16(%rdx,%rcx,8), %r11
 sbb %r9, %r8
 mov %r8, -16(%rdi,%rcx,8)
.Le11:
 mov -8(%rsi,%rcx,8), %r8
 mov -8(%rdx,%rcx,8), %r9
 sbb %r11, %r10
 mov %r10, -8(%rdi,%rcx,8)
.Le10:
 mov (%rsi,%rcx,8), %r10
 mov (%rdx,%rcx,8), %r11
 sbb %r9, %r8
 mov %r8, (%rdi,%rcx,8)
.Le01:
 jrcxz .Lend
 lea 4(%rcx), %rcx
 jmp .Ltop
.size __mpn_sub_n,.-__mpn_sub_n
