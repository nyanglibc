	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_abort
	.hidden	__GI_abort
	.type	__GI_abort, @function
__GI_abort:
	pushq	%rbx
	subq	$288, %rsp
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+lock(%rip)
	je	.L2
#APP
# 54 "abort.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L4:
	movq	%rbx, 8+lock(%rip)
.L2:
	movl	stage(%rip), %eax
	addl	$1, 4+lock(%rip)
	testl	%eax, %eax
	je	.L22
.L5:
	cmpl	$1, %eax
	je	.L23
	cmpl	$2, %eax
	je	.L14
.L15:
	cmpl	$3, %eax
	je	.L24
.L16:
	cmpl	$4, %eax
	je	.L25
.L17:
	cmpl	$5, %eax
	je	.L26
	.p2align 4,,10
	.p2align 3
.L18:
#APP
# 121 "abort.c" 1
	hlt
# 0 "" 2
#NO_APP
	jmp	.L18
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L4
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
.L26:
	movl	$127, %edi
	movl	$6, stage(%rip)
	call	__GI__exit
.L25:
	movl	$5, stage(%rip)
#APP
# 107 "abort.c" 1
	hlt
# 0 "" 2
#NO_APP
	movl	stage(%rip), %eax
	jmp	.L17
.L24:
	movl	$6, %edi
	movl	$4, stage(%rip)
	call	__GI_raise
	movl	stage(%rip), %eax
	jmp	.L16
.L23:
	movl	4+lock(%rip), %eax
	movl	$0, stage(%rip)
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+lock(%rip)
	jne	.L8
	movq	$0, 8+lock(%rip)
#APP
# 77 "abort.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L9
	subl	$1, lock(%rip)
.L8:
	movl	$6, %edi
	call	__GI_raise
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+lock(%rip)
	je	.L11
#APP
# 81 "abort.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L13:
	movq	%rbx, 8+lock(%rip)
.L11:
	addl	$1, 4+lock(%rip)
.L14:
	leaq	128(%rsp), %rsi
	xorl	%eax, %eax
	movl	$38, %ecx
	xorl	%edx, %edx
	movl	$3, stage(%rip)
	movq	%rsi, %rdi
	rep stosl
	movl	$6, %edi
	movq	$-1, 136(%rsp)
	call	__GI___sigaction
	movl	stage(%rip), %eax
	jmp	.L15
.L12:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L13
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L13
.L22:
	movq	%rsp, %rsi
	xorl	%edx, %edx
	movl	$1, %edi
	movl	$1, stage(%rip)
	movq	$32, (%rsp)
	call	__GI___sigprocmask
	movl	stage(%rip), %eax
	jmp	.L5
.L9:
#APP
# 77 "abort.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L8
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 77 "abort.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L8
	.size	__GI_abort, .-__GI_abort
	.globl	abort
	.set	abort,__GI_abort
	.local	lock
	.comm	lock,16,16
	.local	stage
	.comm	stage,4,4
	.hidden	__GI___abort_msg
	.globl	__GI___abort_msg
	.bss
	.align 8
	.type	__GI___abort_msg, @object
	.size	__GI___abort_msg, 8
__GI___abort_msg:
	.zero	8
	.globl	__abort_msg
	.set	__abort_msg,__GI___abort_msg
	.hidden	__lll_lock_wait_private
