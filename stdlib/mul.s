	.text
	.p2align 4,,15
	.globl	__mpn_mul
	.hidden	__mpn_mul
	.type	__mpn_mul, @function
__mpn_mul:
	pushq	%rbp
	movq	%rdi, %r9
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rdx, %r14
	movq	%r8, %rbx
	subq	$72, %rsp
	cmpq	$31, %r8
	movq	%rcx, -56(%rbp)
	jg	.L2
	xorl	%eax, %eax
	testq	%r8, %r8
	je	.L1
	movq	(%rcx), %rcx
	cmpq	$1, %rcx
	ja	.L4
	je	.L5
	testq	%rdx, %rdx
	jle	.L8
	leaq	(%rdi,%rdx,8), %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L10:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L10
.L8:
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r8, %r15
	movq	-56(%rbp), %rdx
	movq	%rbx, %rcx
	salq	$4, %r15
	movq	%rdi, -64(%rbp)
	addq	$16, %r15
	subq	%r15, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %r8
	movq	%rax, -72(%rbp)
	call	__mpn_impn_mul_n
	leaq	0(,%rbx,8), %r10
	movq	-64(%rbp), %r9
	movq	%r14, %r11
	subq	%rbx, %r11
	addq	%r10, %r13
	cmpq	%r11, %rbx
	leaq	(%r9,%r10), %r12
	jg	.L14
	subq	%r15, %rsp
	movq	%r14, -104(%rbp)
	movq	%r9, -96(%rbp)
	leaq	15(%rsp), %r15
	movq	%r13, %r14
	movq	%rbx, %r13
	movq	%r11, %rbx
	andq	$-16, %r15
	leaq	(%r15,%r10), %rax
	movq	%r15, -64(%rbp)
	movq	%r10, %r15
	movq	%rax, -88(%rbp)
	addq	$8, %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L22:
	movq	-72(%rbp), %r8
	movq	-56(%rbp), %rdx
	movq	%r13, %rcx
	movq	-64(%rbp), %rdi
	movq	%r14, %rsi
	call	__mpn_impn_mul_n
	movq	-64(%rbp), %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	movq	%r13, %rcx
	addq	%r15, %r12
	call	__mpn_add_n
	movq	-88(%rbp), %rdi
	xorl	%edx, %edx
	leaq	8(%r12), %rcx
	movq	-80(%rbp), %rsi
	addq	(%rdi), %rax
	setc	%dl
	movq	%rax, (%r12)
	movq	%r13, %rax
	testq	%rdx, %rdx
	jne	.L17
.L18:
	cmpq	%rsi, %rcx
	je	.L20
	cmpq	$1, %rax
	jle	.L20
	leaq	-1(%rax), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rsi,%rax,8), %rdi
	movq	%rdi, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jne	.L21
.L20:
	subq	%r13, %rbx
	addq	%r15, %r14
	cmpq	%rbx, %r13
	jle	.L22
	movq	%rbx, %r11
	movq	-96(%rbp), %r9
	movq	%r13, %rbx
	movq	%r14, %r13
	movq	-104(%rbp), %r14
	movq	%r15, %r10
.L14:
	testq	%r11, %r11
	jne	.L48
.L23:
	addq	%r14, %rbx
	movq	-8(%r9,%rbx,8), %rax
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rdi, -64(%rbp)
	call	__mpn_mul_1
	movq	-64(%rbp), %r9
.L7:
	cmpq	$1, %rbx
	leaq	0(,%r14,8), %r15
	movq	%rax, (%r9,%r14,8)
	leaq	8(%r9), %r12
	jle	.L1
	movq	-56(%rbp), %r8
	leaq	(%r9,%rbx,8), %rbx
	movq	%r14, %rax
	movq	%r13, -56(%rbp)
	movq	%r15, %r14
	movq	%rax, %r15
	movq	%rbx, %r13
	addq	$8, %r8
	movq	%r8, %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$0, %eax
	je	.L49
.L12:
	movq	%rax, (%r12,%r14)
	addq	$8, %r12
	addq	$8, %rbx
	cmpq	%r12, %r13
	je	.L1
.L13:
	movq	(%rbx), %rcx
	cmpq	$1, %rcx
	jbe	.L50
	movq	-56(%rbp), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	call	__mpn_addmul_1
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L19:
	addq	$8, %rsi
	movq	-8(%rsi), %rdi
	addq	$8, %rcx
	leaq	1(%rdi), %rdx
	testq	%rdx, %rdx
	movq	%rdx, -8(%rcx)
	jne	.L18
.L17:
	subq	$1, %rax
	jne	.L19
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-56(%rbp), %rdx
	movq	%r15, %rcx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_add_n
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L5:
	testq	%rdx, %rdx
	jle	.L8
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L9:
	movq	0(%r13,%rax,8), %rdx
	movq	%rdx, (%r9,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r14
	jne	.L9
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-72(%rbp), %r15
	movq	-56(%rbp), %rsi
	movq	%r11, %r8
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r9, -88(%rbp)
	movq	%r10, -80(%rbp)
	movq	%r11, -64(%rbp)
	movq	%r15, %rdi
	call	__mpn_mul
	movq	%rbx, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r12, %rdi
	call	__mpn_add_n
	movq	-80(%rbp), %r10
	movq	%r15, %rdx
	xorl	%esi, %esi
	movq	-64(%rbp), %r11
	movq	-88(%rbp), %r9
	addq	%r10, %rdx
	addq	%r12, %r10
	addq	(%rdx), %rax
	leaq	8(%rdx), %rcx
	leaq	8(%r10), %rdx
	setc	%sil
	movq	%rax, (%r10)
	testq	%rsi, %rsi
	jne	.L26
.L27:
	cmpq	%rcx, %rdx
	je	.L23
	cmpq	$1, %r11
	jle	.L23
	subq	$1, %r11
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L29:
	movq	(%rcx,%rax,8), %rsi
	movq	%rsi, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	%r11, %rax
	jne	.L29
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$8, %rcx
	movq	-8(%rcx), %rax
	addq	$8, %rdx
	addq	$1, %rax
	testq	%rax, %rax
	movq	%rax, -8(%rdx)
	jne	.L27
.L26:
	subq	$1, %r11
	jne	.L28
	jmp	.L23
	.size	__mpn_mul, .-__mpn_mul
	.hidden	__mpn_addmul_1
	.hidden	__mpn_mul_1
	.hidden	__mpn_add_n
	.hidden	__mpn_impn_mul_n
