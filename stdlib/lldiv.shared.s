	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	lldiv
	.type	lldiv, @function
lldiv:
	movq	%rdi, %rax
	cqto
	idivq	%rsi
	ret
	.size	lldiv, .-lldiv
