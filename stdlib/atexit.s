	.text
	.p2align 4,,15
	.globl	atexit
	.hidden	atexit
	.type	atexit, @function
atexit:
	movq	__dso_handle(%rip), %rdx
	xorl	%esi, %esi
	jmp	__cxa_atexit
	.size	atexit, .-atexit
	.hidden	__cxa_atexit
	.hidden	__dso_handle
