	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4,,15
	.globl	l64a
	.type	l64a, @function
l64a:
	andl	$4294967295, %edi
	leaq	.LC0(%rip), %rax
	je	.L1
	leaq	-1+result.3575(%rip), %rsi
	leaq	conv_table(%rip), %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rdi, %rdx
	shrq	$6, %rdi
	andl	$63, %edx
	movzbl	(%rcx,%rdx), %edx
	movb	%dl, (%rsi,%rax)
	movslq	%eax, %rdx
	addq	$1, %rax
	testq	%rdi, %rdi
	jne	.L3
	leaq	result.3575(%rip), %rax
	movb	$0, (%rax,%rdx)
.L1:
	rep ret
	.size	l64a, .-l64a
	.local	result.3575
	.comm	result.3575,7,1
	.section	.rodata
	.align 32
	.type	conv_table, @object
	.size	conv_table, 64
conv_table:
	.byte	46
	.byte	47
	.byte	48
	.byte	49
	.byte	50
	.byte	51
	.byte	52
	.byte	53
	.byte	54
	.byte	55
	.byte	56
	.byte	57
	.byte	65
	.byte	66
	.byte	67
	.byte	68
	.byte	69
	.byte	70
	.byte	71
	.byte	72
	.byte	73
	.byte	74
	.byte	75
	.byte	76
	.byte	77
	.byte	78
	.byte	79
	.byte	80
	.byte	81
	.byte	82
	.byte	83
	.byte	84
	.byte	85
	.byte	86
	.byte	87
	.byte	88
	.byte	89
	.byte	90
	.byte	97
	.byte	98
	.byte	99
	.byte	100
	.byte	101
	.byte	102
	.byte	103
	.byte	104
	.byte	105
	.byte	106
	.byte	107
	.byte	108
	.byte	109
	.byte	110
	.byte	111
	.byte	112
	.byte	113
	.byte	114
	.byte	115
	.byte	116
	.byte	117
	.byte	118
	.byte	119
	.byte	120
	.byte	121
	.byte	122
