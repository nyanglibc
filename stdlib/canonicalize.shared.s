	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __realpath,realpath@@GLIBC_2.3
	.symver __old_realpath,realpath@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___realpath
	.hidden	__GI___realpath
	.type	__GI___realpath, @function
__GI___realpath:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$3192, %rsp
	testq	%rdi, %rdi
	movq	%rsi, (%rsp)
	je	.L127
	movzbl	(%rdi), %eax
	movq	%rdi, %rbp
	testb	%al, %al
	je	.L128
	leaq	1104(%rsp), %rcx
	leaq	1120(%rsp), %rdx
	leaq	64(%rsp), %r15
	cmpb	$47, %al
	movq	$1024, 1112(%rsp)
	movq	$1024, 2152(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rdx, 1104(%rsp)
	leaq	2144(%rsp), %rcx
	leaq	2160(%rsp), %rdx
	leaq	16(%r15), %rbx
	movq	$1024, 72(%rsp)
	movq	%rcx, 8(%rsp)
	movl	$1024, %esi
	movq	%rdx, 2144(%rsp)
	movq	%rbx, 64(%rsp)
	jne	.L5
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movq	64(%rsp), %rbx
	movq	72(%rsp), %rsi
.L5:
	movq	%rbx, %rdi
	call	__GI___getcwd
	testq	%rax, %rax
	jne	.L130
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L50
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L131
.L57:
	movl	$1, %r12d
	movl	$1, %ebp
.L8:
	movq	16(%rsp), %rax
	movq	1104(%rsp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L44
	call	free@PLT
.L44:
	movq	8(%rsp), %rax
	movq	2144(%rsp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L45
	call	free@PLT
.L45:
	testb	%r12b, %r12b
	je	.L46
	movq	64(%rsp), %rdi
	leaq	16(%r15), %rax
	cmpq	%rax, %rdi
	je	.L47
	call	free@PLT
.L47:
	testb	%bpl, %bpl
	movl	$0, %eax
	cmove	(%rsp), %rax
	movq	%rax, (%rsp)
.L82:
	movq	(%rsp), %rax
	addq	$3192, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	17(%r15), %r13
	movb	$47, 80(%rsp)
.L10:
	movb	$0, 43(%rsp)
	movl	$0, 44(%rsp)
.L40:
	testb	%al, %al
	je	.L12
	cmpb	$47, %al
	jne	.L41
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rbp
	movzbl	0(%rbp), %eax
	cmpb	$47, %al
	je	.L11
	testb	%al, %al
	je	.L12
.L41:
	movq	%rbp, %r12
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L132:
	cmpb	$47, %dl
	je	.L67
.L13:
	addq	$1, %r12
	movzbl	(%r12), %edx
	testb	%dl, %dl
	jne	.L132
.L67:
	movq	%r12, %rcx
	subq	%rbp, %rcx
	movq	%rcx, 24(%rsp)
	je	.L12
	cmpq	$1, %rcx
	je	.L133
	cmpb	$46, %al
	jne	.L17
	cmpq	$2, 24(%rsp)
	jne	.L17
	cmpb	$46, 1(%rbp)
	jne	.L17
	leaq	1(%rbx), %rax
	cmpq	%rax, %r13
	ja	.L134
.L16:
	movzbl	(%r12), %eax
	movq	%r12, %rbp
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$46, %al
	je	.L16
.L17:
	cmpb	$47, -1(%r13)
	je	.L19
	movb	$47, 0(%r13)
	addq	$1, %r13
.L19:
	movq	24(%rsp), %rax
	leaq	2(%rax), %rsi
	movq	72(%rsp), %rax
	movq	%rsi, %rcx
	addq	%rbx, %rax
	subq	%r13, %rax
	cmpq	%rax, %rsi
	jbe	.L21
	movq	%rcx, %r14
	movq	%rbp, 32(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L136:
	movq	72(%rsp), %rax
	movq	64(%rsp), %rbx
	subq	%rbp, %rax
	leaq	(%rbx,%rbp), %r13
	cmpq	%r14, %rax
	jnb	.L135
.L20:
	movq	%r13, %rbp
	movq	%r15, %rdi
	subq	%rbx, %rbp
	call	__GI___libc_scratch_buffer_grow_preserve
	testb	%al, %al
	jne	.L136
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r13, %rsi
	movq	%r15, %rdi
	subq	%rbx, %rsi
	call	__GI___libc_scratch_buffer_dupfree
	movq	%rax, (%rsp)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L135:
	movq	32(%rsp), %rbp
.L21:
	movq	24(%rsp), %rdx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	call	__GI_mempcpy@PLT
	movq	8(%rsp), %r14
	movq	%rax, %r13
	movb	$0, (%rax)
	movq	%r12, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L23:
	movq	2152(%rsp), %rax
	movq	2144(%rsp), %rbp
	movq	%rbx, %rdi
	leaq	-1(%rax), %r12
	movq	%rbp, %rsi
	movq	%r12, %rdx
	call	__readlink
	cmpq	%rax, %r12
	jg	.L22
	movq	%r14, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L23
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L130:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__GI___rawmemchr
	movq	%rax, %r13
	movzbl	0(%rbp), %eax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rbx, %rsi
	movl	$1, %ebp
.L7:
	cmpq	$0, (%rsp)
	leaq	1(%rbx), %r13
	movb	$0, (%rbx)
	je	.L137
	movq	%r13, %rax
	subq	%rsi, %rax
	cmpq	$4096, %rax
	jle	.L43
	cmpq	%rsi, (%rsp)
	movq	%rsi, %rbx
	sete	%r12b
	orl	%ebp, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L22:
	testq	%rax, %rax
	movq	24(%rsp), %r12
	movq	%rax, %rcx
	jns	.L24
	movzbl	(%r12), %edx
	movq	%r12, %rax
.L25:
	cmpb	$47, %dl
	je	.L35
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rcx, %rax
.L35:
	movzbl	1(%rax), %edx
	leaq	1(%rax), %rcx
	cmpb	$47, %dl
	je	.L62
	addq	$2, %rax
	testb	%dl, %dl
	je	.L37
	cmpb	$46, %dl
	jne	.L36
	movzbl	1(%rcx), %edx
	testb	%dl, %dl
	je	.L37
	cmpb	$46, %dl
	jne	.L25
	movzbl	2(%rcx), %eax
	testb	%al, %al
	je	.L37
	cmpb	$47, %al
	je	.L37
.L36:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$22, %fs:(%rax)
	setne	%bpl
.L39:
	testb	%bpl, %bpl
	je	.L16
	movq	%rbx, %rsi
	movq	%r13, %rbx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L128:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, (%rsp)
	movl	$2, %fs:(%rax)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L137:
	testq	%rsi, %rsi
	movq	%rsi, %rbx
	sete	%r12b
	orl	%ebp, %r12d
	jmp	.L8
.L37:
	movl	$47, %eax
	xorl	%edx, %edx
	movl	$512, %ecx
	movw	%ax, 0(%r13)
	movq	%rbx, %rsi
	movl	$-100, %edi
	call	__faccessat@PLT
	testl	%eax, %eax
	setne	%bpl
	jmp	.L39
.L134:
	leaq	-1(%r13), %rax
	cmpq	%rax, %rbx
	jnb	.L60
	cmpb	$47, -2(%r13)
	je	.L60
.L18:
	subq	$1, %rax
	cmpq	%rax, %rbx
	je	.L54
	cmpb	$47, -1(%rax)
	jne	.L18
.L60:
	movq	%rax, %r13
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L24:
	movq	sysconf_symloop_max.5738(%rip), %rax
	addl	$1, 44(%rsp)
	testq	%rax, %rax
	je	.L138
.L26:
	testq	%rax, %rax
	movl	$40, %edx
	jle	.L27
	cmpl	$40, %eax
	cmovnb	%eax, %edx
.L27:
	cmpl	%edx, 44(%rsp)
	ja	.L139
	cmpb	$0, 43(%rsp)
	movb	$0, 0(%rbp,%rcx)
	movq	1104(%rsp), %rax
	movq	%rcx, 48(%rsp)
	movq	%rax, 24(%rsp)
	je	.L29
	movq	%r12, %rcx
	movq	%r12, %rdi
	subq	%rax, %rcx
	movq	%rcx, 56(%rsp)
	call	__GI_strlen
	movq	48(%rsp), %rcx
	movq	%rax, 32(%rsp)
	leaq	(%rcx,%rax), %r14
	cmpq	1112(%rsp), %r14
	jb	.L30
	movq	%rbp, 24(%rsp)
	movq	16(%rsp), %rbp
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L141:
	cmpq	1112(%rsp), %r14
	jb	.L140
.L31:
	movq	%rbp, %rdi
	call	__GI___libc_scratch_buffer_grow_preserve
	testb	%al, %al
	jne	.L141
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rsp), %r14
	movq	%r14, %rdi
	movq	%r14, %rbx
	call	__GI_strcpy
	cmpq	%r14, %r14
	sete	%r12b
	orl	%ebp, %r12d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L127:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, (%rsp)
	movl	$22, %fs:(%rax)
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	1(%rbx), %rax
	movq	%rbx, %rsi
	cmpq	%rax, %r13
	jbe	.L65
	cmpb	$47, -1(%r13)
	je	.L142
.L65:
	movq	%r13, %rbx
	xorl	%ebp, %ebp
	jmp	.L7
.L29:
	movq	%r12, %rdi
	call	__GI_strlen
	movq	48(%rsp), %rcx
	movq	%rax, 32(%rsp)
	movq	$0, 56(%rsp)
	leaq	(%rax,%rcx), %r14
	cmpq	%r14, 1112(%rsp)
	ja	.L32
	movq	%rbp, 24(%rsp)
	movq	%rcx, 48(%rsp)
	movq	16(%rsp), %rbp
	jmp	.L31
.L140:
	cmpb	$0, 43(%rsp)
	movq	1104(%rsp), %rax
	movq	24(%rsp), %rbp
	movq	48(%rsp), %rcx
	movq	%rax, 24(%rsp)
	je	.L32
.L30:
	movq	56(%rsp), %r12
	addq	24(%rsp), %r12
.L32:
	movq	24(%rsp), %r14
	movq	32(%rsp), %rdx
	movq	%r12, %rsi
	movq	%rcx, 24(%rsp)
	leaq	(%r14,%rcx), %rdi
	addq	$1, %rdx
	movq	%r14, %r12
	call	__GI_memmove
	movq	24(%rsp), %rcx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	movq	%rcx, %rdx
	call	__GI_memcpy@PLT
	cmpb	$47, 0(%rbp)
	leaq	1(%rbx), %rax
	je	.L143
	cmpq	%rax, %r13
	movb	$1, 43(%rsp)
	jbe	.L16
	leaq	-1(%r13), %rax
	cmpq	%rbx, %rax
	jbe	.L60
	cmpb	$47, -2(%r13)
	je	.L60
.L34:
	subq	$1, %rax
	cmpq	%rbx, %rax
	je	.L61
	cmpb	$47, -1(%rax)
	jne	.L34
	movq	%rax, %r13
	movb	$1, 43(%rsp)
	jmp	.L16
.L138:
	movl	$173, %edi
	movq	%rcx, 24(%rsp)
	call	__GI___sysconf
	movq	24(%rsp), %rcx
	movq	%rax, sysconf_symloop_max.5738(%rip)
	jmp	.L26
.L143:
	movb	$47, (%rbx)
	movq	%rax, %r13
	movb	$1, 43(%rsp)
	jmp	.L16
.L142:
	leaq	-1(%r13), %rbx
	xorl	%ebp, %ebp
	jmp	.L7
.L54:
	movq	%rbx, %r13
	jmp	.L16
.L139:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rbx, %rsi
	movl	$1, %ebp
	movq	%r13, %rbx
	movl	$40, %fs:(%rax)
	jmp	.L7
.L61:
	movq	%rbx, %r13
	movb	$1, 43(%rsp)
	jmp	.L16
	.size	__GI___realpath, .-__GI___realpath
	.globl	__realpath
	.set	__realpath,__GI___realpath
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__old_realpath
	.type	__old_realpath, @function
__old_realpath:
	testq	%rsi, %rsi
	je	.L148
	jmp	__GI___realpath
	.p2align 4,,10
	.p2align 3
.L148:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__old_realpath, .-__old_realpath
	.text
	.p2align 4,,15
	.globl	__canonicalize_file_name
	.type	__canonicalize_file_name, @function
__canonicalize_file_name:
	xorl	%esi, %esi
	jmp	__GI___realpath
	.size	__canonicalize_file_name, .-__canonicalize_file_name
	.weak	canonicalize_file_name
	.set	canonicalize_file_name,__canonicalize_file_name
	.local	sysconf_symloop_max.5738
	.comm	sysconf_symloop_max.5738,8,8
	.hidden	__readlink
