	.text
	.p2align 4,,15
	.globl	__lcong48_r
	.hidden	__lcong48_r
	.type	__lcong48_r, @function
__lcong48_r:
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
	movzwl	4(%rdi), %eax
	movw	%ax, 4(%rsi)
	movzwl	10(%rdi), %eax
	movzwl	6(%rdi), %edx
	salq	$32, %rax
	orq	%rdx, %rax
	movzwl	8(%rdi), %edx
	sall	$16, %edx
	orq	%rdx, %rax
	movq	%rax, 16(%rsi)
	movzwl	12(%rdi), %eax
	movw	%ax, 12(%rsi)
	movl	$1, %eax
	movw	%ax, 14(%rsi)
	xorl	%eax, %eax
	ret
	.size	__lcong48_r, .-__lcong48_r
	.weak	lcong48_r
	.set	lcong48_r,__lcong48_r
