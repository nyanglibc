	.text
	.p2align 4,,15
	.globl	__run_exit_handlers
	.hidden	__run_exit_handlers
	.type	__run_exit_handlers, @function
__run_exit_handlers:
	pushq	%r15
	pushq	%r14
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testb	%cl, %cl
	movq	%rsi, (%rsp)
	movl	%edx, 12(%rsp)
	jne	.L42
.L2:
	movq	__pthread_mutex_lock@GOTPCREL(%rip), %r13
	movq	__pthread_mutex_unlock@GOTPCREL(%rip), %r12
	.p2align 4,,10
	.p2align 3
.L17:
	testq	%r13, %r13
	je	.L14
	movq	__exit_funcs_lock@GOTPCREL(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L14:
	movq	(%rsp), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	jne	.L4
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	$4, %rdx
	je	.L11
	cmpq	$2, %rdx
	jne	.L9
	movq	24(%rax), %rdx
	movq	32(%rax), %rsi
	movl	%r14d, %edi
#APP
# 89 "exit.c" 1
	ror $2*8+1, %rdx
xor __pointer_chk_guard_local(%rip), %rdx
# 0 "" 2
#NO_APP
	call	*%rdx
.L9:
	testq	%r13, %r13
	je	.L13
	movq	__exit_funcs_lock@GOTPCREL(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L13:
	cmpq	%rbp, __new_exitfn_called(%rip)
	jne	.L14
.L4:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L44
	leaq	-1(%rax), %r15
	testq	%r12, %r12
	movq	__new_exitfn_called(%rip), %rbp
	movq	%r15, 8(%rbx)
	je	.L8
	movq	__exit_funcs_lock@GOTPCREL(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L8:
	movq	%r15, %rax
	salq	$5, %rax
	addq	%rbx, %rax
	movq	16(%rax), %rdx
	cmpq	$3, %rdx
	jne	.L45
	movq	24(%rax), %rax
#APP
# 96 "exit.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L11:
	movq	24(%rax), %rdx
	movq	$0, 16(%rax)
	movl	%r14d, %esi
#APP
# 106 "exit.c" 1
	ror $2*8+1, %rdx
xor __pointer_chk_guard_local(%rip), %rdx
# 0 "" 2
#NO_APP
	movq	32(%rax), %rdi
	call	*%rdx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%rbx), %rax
	movq	(%rsp), %rcx
	testq	%rax, %rax
	movq	%rax, (%rcx)
	je	.L16
	movq	%rbx, %rdi
	call	*__rtld_free(%rip)
.L16:
	testq	%r12, %r12
	je	.L17
	movq	__exit_funcs_lock@GOTPCREL(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L43:
	testq	%r12, %r12
	movb	$1, __exit_funcs_done(%rip)
	je	.L5
	movq	__exit_funcs_lock@GOTPCREL(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L5:
	cmpb	$0, 12(%rsp)
	je	.L6
	leaq	__start___libc_atexit(%rip), %rbx
	leaq	__stop___libc_atexit(%rip), %rax
	cmpq	%rax, %rbx
	jnb	.L6
	leaq	7+__stop___libc_atexit(%rip), %rax
	leaq	8+__start___libc_atexit(%rip), %rdx
	subq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L19:
	call	*(%rbx)
	addq	$8, %rbx
	cmpq	%rbp, %rbx
	jne	.L19
.L6:
	movl	%r14d, %edi
	call	__GI__exit
.L42:
	call	__call_tls_dtors@PLT
	jmp	.L2
	.size	__run_exit_handlers, .-__run_exit_handlers
	.p2align 4,,15
	.globl	exit
	.type	exit, @function
exit:
	leaq	__exit_funcs(%rip), %rsi
	subq	$8, %rsp
	movl	$1, %ecx
	movl	$1, %edx
	call	__run_exit_handlers
	.size	exit, .-exit
	.hidden	__exit_funcs_done
	.globl	__exit_funcs_done
	.bss
	.type	__exit_funcs_done, @object
	.size	__exit_funcs_done, 1
__exit_funcs_done:
	.zero	1
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__exit_funcs
	.hidden	__stop___libc_atexit
	.hidden	__start___libc_atexit
	.hidden	__rtld_free
	.hidden	__new_exitfn_called
