	.text
	.p2align 4,,15
	.globl	__cxa_thread_atexit_impl
	.type	__cxa_thread_atexit_impl, @function
__cxa_thread_atexit_impl:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rdi, %rbp
	movl	$32, %esi
	movl	$1, %edi
	subq	$8, %rsp
#APP
# 102 "cxa_thread_atexit_impl.c" 1
	xor __pointer_chk_guard_local(%rip), %rbp
rol $2*8+1, %rbp
# 0 "" 2
#NO_APP
	call	*__rtld_calloc(%rip)
	movq	%rax, %rbx
	movq	%rbp, (%rax)
	movq	%r13, 8(%rax)
	movq	tls_dtor_list@gottpoff(%rip), %rax
	leaq	2440+_rtld_local(%rip), %rdi
	movq	%fs:(%rax), %rdx
	movq	%rbx, %fs:(%rax)
	movq	%rdx, 24(%rbx)
	call	*3984+_rtld_local(%rip)
	testq	%r12, %r12
	jne	.L2
	movq	lm_cache@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %rax
.L3:
	lock addq	$1, 1128(%rax)
	leaq	2440+_rtld_local(%rip), %rdi
	call	*3992+_rtld_local(%rip)
	movq	%fs:0(%rbp), %rax
	movq	%rax, 16(%rbx)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r12, %rdi
	call	__GI__dl_find_dso_for_object
	testq	%rax, %rax
	cmove	_rtld_local(%rip), %rax
	movq	lm_cache@gottpoff(%rip), %rbp
	movq	%rax, %fs:0(%rbp)
	jmp	.L3
	.size	__cxa_thread_atexit_impl, .-__cxa_thread_atexit_impl
	.p2align 4,,15
	.globl	__call_tls_dtors
	.type	__call_tls_dtors, @function
__call_tls_dtors:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	tls_dtor_list@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movq	24(%rbx), %rdx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdi
#APP
# 151 "cxa_thread_atexit_impl.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	movq	%rdx, %fs:0(%rbp)
	call	*%rax
	movq	16(%rbx), %rax
	lock subq	$1, 1128(%rax)
	movq	%rbx, %rdi
	call	*__rtld_free(%rip)
	movq	%fs:0(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L9
.L7:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__call_tls_dtors, .-__call_tls_dtors
	.section	.tbss,"awT",@nobits
	.align 8
	.type	lm_cache, @object
	.size	lm_cache, 8
lm_cache:
	.zero	8
	.align 8
	.type	tls_dtor_list, @object
	.size	tls_dtor_list, 8
tls_dtor_list:
	.zero	8
	.hidden	__rtld_free
	.hidden	_rtld_local
	.hidden	__rtld_calloc
