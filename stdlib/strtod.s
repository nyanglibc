	.text
	.p2align 4,,15
	.globl	__strtod_internal
	.hidden	__strtod_internal
	.type	__strtod_internal, @function
__strtod_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____strtod_l_internal
	.size	__strtod_internal, .-__strtod_internal
	.p2align 4,,15
	.weak	strtod
	.hidden	strtod
	.type	strtod, @function
strtod:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____strtod_l_internal
	.size	strtod, .-strtod
	.weak	strtof32x
	.set	strtof32x,strtod
	.weak	strtof64
	.set	strtof64,strtod
	.hidden	____strtod_l_internal
