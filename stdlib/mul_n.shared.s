	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__mpn_impn_mul_n_basecase
	.hidden	__mpn_impn_mul_n_basecase
	.type	__mpn_impn_mul_n_basecase, @function
__mpn_impn_mul_n_basecase:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdx), %rcx
	cmpq	$1, %rcx
	ja	.L2
	je	.L3
	testq	%r12, %r12
	jle	.L6
	leaq	(%rdi,%r12,8), %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L8:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L8
.L6:
	xorl	%eax, %eax
.L5:
	leaq	0(,%r12,8), %r15
	leaq	(%rbx,%r15), %r13
	addq	$8, %rbx
	cmpq	$1, %r12
	movq	%rax, 0(%r13)
	jle	.L1
	addq	$8, %rbp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$0, %eax
	je	.L19
.L11:
	movq	%rax, (%rbx,%r15)
	addq	$8, %rbx
	addq	$8, %rbp
	cmpq	%rbx, %r13
	je	.L1
.L12:
	movq	0(%rbp), %rcx
	cmpq	$1, %rcx
	jbe	.L20
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__mpn_addmul_1
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r12, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%rbx, %rdi
	call	__mpn_add_n
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%r12, %rdx
	call	__mpn_mul_1
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%r12, %r12
	jle	.L6
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%r14,%rax,8), %rdx
	movq	%rdx, (%rbx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L7
	jmp	.L6
	.size	__mpn_impn_mul_n_basecase, .-__mpn_impn_mul_n_basecase
	.p2align 4,,15
	.globl	__mpn_impn_mul_n
	.hidden	__mpn_impn_mul_n
	.type	__mpn_impn_mul_n, @function
__mpn_impn_mul_n:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$72, %rsp
	andl	$1, %ecx
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	je	.L22
	leaq	-1(%r14), %rbx
	cmpq	$31, %rbx
	jg	.L23
	movq	%rbx, %rcx
	call	__mpn_impn_mul_n_basecase
.L24:
	movq	16(%rsp), %r15
	movq	8(%rsp), %r13
	leaq	0(%rbp,%rbx,8), %r12
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	(%r15,%rbx,8), %rcx
	movq	%r13, %rsi
	call	__mpn_addmul_1
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	salq	$4, %rdx
	movq	%rax, 0(%rbp,%rdx)
	movq	0(%r13,%rbx,8), %rcx
	movq	%r14, %rdx
	addq	%r14, %rbx
	call	__mpn_addmul_1
	movq	%rax, 0(%rbp,%rbx,8)
.L21:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%r14, %rbx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %r15
	sarq	%rbx
	leaq	0(,%rbx,8), %rax
	addq	%rax, %rdx
	addq	%rax, %r15
	movq	%rax, 32(%rsp)
	leaq	0(,%r14,8), %rax
	cmpq	$31, %rbx
	movq	%rdx, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	(%rdi,%rax), %r13
	jg	.L26
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__mpn_impn_mul_n_basecase
.L27:
	movq	8(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	movq	%rbx, %rcx
	js	.L28
	movq	8(%rsp), %rdx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	__mpn_sub_n
	movl	$0, 40(%rsp)
.L29:
	movq	48(%rsp), %r15
	movq	16(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rdi
	call	__mpn_cmp
	movq	32(%rsp), %rsi
	testl	%eax, %eax
	movq	%rbx, %rcx
	leaq	0(%rbp,%rsi), %rdi
	movq	%rdi, 24(%rsp)
	js	.L30
	movq	16(%rsp), %rdx
	movq	%r15, %rsi
	call	__mpn_sub_n
	xorl	$1, 40(%rsp)
	cmpq	$31, %rbx
	jg	.L32
.L69:
	movq	24(%rsp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	call	__mpn_impn_mul_n_basecase
	testq	%rbx, %rbx
	leaq	(%rbx,%r14), %rdi
	jle	.L34
.L33:
	leaq	(%rbx,%r14), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rax
	subq	%r14, %rcx
	leaq	0(%rbp,%rdi,8), %rsi
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L35
.L34:
	leaq	0(%rbp,%rdi,8), %r15
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	movq	%r15, %rdx
	call	__mpn_add_n
	movq	%rax, 48(%rsp)
	movl	40(%rsp), %eax
	movq	%r14, %rcx
	movq	24(%rsp), %rdi
	movq	%r12, %rdx
	testl	%eax, %eax
	movq	%rdi, %rsi
	je	.L36
	call	__mpn_sub_n
	movq	48(%rsp), %rsi
	subq	%rax, %rsi
	cmpq	$31, %rbx
	movq	%rsi, 40(%rsp)
	jg	.L38
.L68:
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	movq	%rbx, %rcx
	movq	%r12, %rdi
	call	__mpn_impn_mul_n_basecase
.L39:
	movq	24(%rsp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	%rdi, %rsi
	call	__mpn_add_n
	movq	40(%rsp), %rdx
	addq	%rax, %rdx
	je	.L45
	xorl	%eax, %eax
	addq	(%r15), %rdx
	setc	%al
	movq	%rdx, (%r15)
	testq	%rax, %rax
	je	.L45
	leaq	-1(%rbx), %rcx
	xorl	%eax, %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L47:
	movq	8(%r15,%rax,8), %rdi
	leaq	1(%rdi), %rdx
	movq	%rdx, 8(%r15,%rax,8)
	addq	$1, %rax
	testq	%rdx, %rdx
	jne	.L45
.L46:
	cmpq	%rax, %rcx
	jne	.L47
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	jle	.L42
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%r12,%rax,8), %rdx
	movq	%rdx, 0(%rbp,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L41
.L42:
	movq	32(%rsp), %rdx
	movq	24(%rsp), %rdi
	movq	%rbx, %rcx
	addq	%r12, %rdx
	movq	%rdi, %rsi
	call	__mpn_add_n
	testq	%rax, %rax
	je	.L21
	movq	0(%r13), %rax
	xorl	%edx, %edx
	addq	$1, %rax
	jc	.L67
.L49:
	testq	%rdx, %rdx
	movq	%rax, 0(%r13)
	je	.L21
	leaq	-1(%r14), %rax
	xorl	%edx, %edx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	movq	8(%r13,%rdx,8), %rdi
	leaq	1(%rdi), %rcx
	movq	%rcx, 8(%r13,%rdx,8)
	addq	$1, %rdx
	testq	%rcx, %rcx
	jne	.L21
.L52:
	cmpq	%rax, %rdx
	jne	.L53
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L36:
	call	__mpn_add_n
	addq	48(%rsp), %rax
	cmpq	$31, %rbx
	movq	%rax, 40(%rsp)
	jle	.L68
.L38:
	movq	56(%rsp), %r8
	movq	16(%rsp), %rdx
	movq	%rbx, %rcx
	movq	8(%rsp), %rsi
	movq	%r12, %rdi
	addq	%r12, %r8
	call	__mpn_impn_mul_n
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L26:
	movq	48(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__mpn_impn_mul_n
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L23:
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	movq	%rbx, %rcx
	call	__mpn_impn_mul_n
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	movq	48(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdi
	call	__mpn_sub_n
	cmpq	$31, %rbx
	jle	.L69
.L32:
	movq	56(%rsp), %rax
	movq	24(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	leaq	(%r12,%rax), %r8
	call	__mpn_impn_mul_n
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L28:
	movq	8(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rbp, %rdi
	call	__mpn_sub_n
	movl	$1, 40(%rsp)
	jmp	.L29
.L67:
	movl	$1, %edx
	jmp	.L49
	.size	__mpn_impn_mul_n, .-__mpn_impn_mul_n
	.p2align 4,,15
	.globl	__mpn_impn_sqr_n_basecase
	.hidden	__mpn_impn_sqr_n_basecase
	.type	__mpn_impn_sqr_n_basecase, @function
__mpn_impn_sqr_n_basecase:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	(%rsi), %rcx
	cmpq	$1, %rcx
	ja	.L71
	je	.L72
	testq	%rdx, %rdx
	jle	.L75
	leaq	(%rdi,%rdx,8), %rdx
	movq	%rdi, %rax
	.p2align 4,,10
	.p2align 3
.L77:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L77
.L75:
	xorl	%eax, %eax
.L74:
	leaq	0(,%r12,8), %r13
	cmpq	$1, %r12
	leaq	(%r14,%r13), %r15
	movq	%rax, (%r15)
	jle	.L70
	movl	$8, %ebx
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$0, %eax
	je	.L86
.L80:
	movq	%rax, (%r15,%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r13
	je	.L70
.L81:
	movq	0(%rbp,%rbx), %rcx
	leaq	(%r14,%rbx), %rdi
	cmpq	$1, %rcx
	jbe	.L87
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	__mpn_addmul_1
	movq	%rax, (%r15,%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r13
	jne	.L81
.L70:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%rdi, %rsi
	call	__mpn_add_n
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L71:
	call	__mpn_mul_1
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L72:
	testq	%rdx, %rdx
	jle	.L75
	xorl	%eax, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L88:
	movq	0(%rbp,%rax,8), %rcx
.L76:
	movq	%rcx, (%r14,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L88
	jmp	.L75
	.size	__mpn_impn_sqr_n_basecase, .-__mpn_impn_sqr_n_basecase
	.p2align 4,,15
	.globl	__mpn_impn_sqr_n
	.hidden	__mpn_impn_sqr_n
	.type	__mpn_impn_sqr_n, @function
__mpn_impn_sqr_n:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$56, %rsp
	andl	$1, %edx
	movq	%rsi, 8(%rsp)
	je	.L90
	leaq	-1(%rbp), %r13
	cmpq	$31, %r13
	jg	.L91
	movq	%r13, %rdx
	call	__mpn_impn_sqr_n_basecase
.L92:
	movq	8(%rsp), %r12
	leaq	0(,%r13,8), %r14
	movq	%r13, %rdx
	leaq	(%r12,%r14), %r15
	addq	%rbx, %r14
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	(%r15), %rcx
	call	__mpn_addmul_1
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	salq	$4, %rdx
	movq	%rax, (%rbx,%rdx)
	movq	(%r15), %rcx
	movq	%rbp, %rdx
	addq	%r13, %rbp
	call	__mpn_addmul_1
	movq	%rax, (%rbx,%rbp,8)
.L89:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rbp, %r12
	movq	8(%rsp), %r14
	sarq	%r12
	leaq	0(,%r12,8), %rax
	addq	%rax, %r14
	movq	%rax, 24(%rsp)
	leaq	0(,%rbp,8), %rax
	cmpq	$31, %r12
	movq	%rax, 40(%rsp)
	leaq	(%rdi,%rax), %r13
	jg	.L94
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__mpn_impn_sqr_n_basecase
.L95:
	movq	8(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	__mpn_cmp
	testl	%eax, %eax
	movq	%r12, %rcx
	js	.L96
	movq	8(%rsp), %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__mpn_sub_n
	cmpq	$31, %r12
	jg	.L98
.L133:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	__mpn_impn_sqr_n_basecase
	testq	%r12, %r12
	jle	.L131
.L99:
	leaq	(%r12,%rbp), %rdi
	movq	%r12, %rcx
	movq	%r13, %rax
	subq	%rbp, %rcx
	leaq	(%rbx,%rdi,8), %rsi
	.p2align 4,,10
	.p2align 3
.L101:
	movq	(%rax), %rdx
	movq	%rdx, (%rax,%rcx,8)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.L101
	leaq	(%rbx,%rdi,8), %r14
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	movq	%r14, %rdx
	call	__mpn_add_n
	movq	24(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rax, 32(%rsp)
	leaq	(%rbx,%rsi), %rcx
	movq	%rcx, %rdi
	movq	%rcx, 16(%rsp)
	movq	%rbp, %rcx
	movq	%rdi, %rsi
	call	__mpn_sub_n
	movq	32(%rsp), %rcx
	subq	%rax, %rcx
	cmpq	$31, %r12
	movq	%rcx, 32(%rsp)
	jg	.L102
.L118:
	movq	8(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	__mpn_impn_sqr_n_basecase
.L103:
	movq	16(%rsp), %rdi
	movq	%rbp, %rcx
	movq	%r15, %rdx
	movq	%rdi, %rsi
	call	__mpn_add_n
	addq	32(%rsp), %rax
	je	.L109
	xorl	%edx, %edx
	addq	(%r14), %rax
	setc	%dl
	movq	%rax, (%r14)
	testq	%rdx, %rdx
	je	.L109
	leaq	-1(%r12), %rcx
	xorl	%eax, %eax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	movq	8(%r14,%rax,8), %rsi
	leaq	1(%rsi), %rdx
	movq	%rdx, 8(%r14,%rax,8)
	addq	$1, %rax
	testq	%rdx, %rdx
	jne	.L109
.L110:
	cmpq	%rax, %rcx
	jne	.L111
	.p2align 4,,10
	.p2align 3
.L109:
	xorl	%eax, %eax
	testq	%r12, %r12
	jle	.L106
	.p2align 4,,10
	.p2align 3
.L105:
	movq	(%r15,%rax,8), %rdx
	movq	%rdx, (%rbx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L105
.L106:
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rdi
	movq	%r12, %rcx
	addq	%r15, %rdx
	movq	%rdi, %rsi
	call	__mpn_add_n
	testq	%rax, %rax
	je	.L89
	movq	0(%r13), %rax
	xorl	%edx, %edx
	addq	$1, %rax
	jc	.L132
.L113:
	testq	%rdx, %rdx
	movq	%rax, 0(%r13)
	je	.L89
	subq	$1, %rbp
	xorl	%eax, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L117:
	movq	8(%r13,%rax,8), %rdi
	leaq	1(%rdi), %rdx
	movq	%rdx, 8(%r13,%rax,8)
	addq	$1, %rax
	testq	%rdx, %rdx
	jne	.L89
.L116:
	cmpq	%rbp, %rax
	jne	.L117
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L94:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__mpn_impn_sqr_n
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L91:
	movq	8(%rsp), %rsi
	movq	%r13, %rdx
	call	__mpn_impn_sqr_n
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L96:
	movq	8(%rsp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	__mpn_sub_n
	cmpq	$31, %r12
	jle	.L133
.L98:
	movq	40(%rsp), %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	leaq	(%r15,%rax), %rcx
	call	__mpn_impn_sqr_n
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L102:
	movq	40(%rsp), %rcx
	movq	8(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	addq	%r15, %rcx
	call	__mpn_impn_sqr_n
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	(%r12,%rbp), %rax
	movq	%r12, %rcx
	movq	%r13, %rsi
	movq	%r13, %rdi
	leaq	(%rbx,%rax,8), %r14
	movq	%r14, %rdx
	call	__mpn_add_n
	movq	24(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rax, 32(%rsp)
	leaq	(%rbx,%rsi), %rcx
	movq	%rcx, %rdi
	movq	%rcx, 16(%rsp)
	movq	%rbp, %rcx
	movq	%rdi, %rsi
	call	__mpn_sub_n
	movq	32(%rsp), %rcx
	subq	%rax, %rcx
	movq	%rcx, 32(%rsp)
	jmp	.L118
.L132:
	movl	$1, %edx
	jmp	.L113
	.size	__mpn_impn_sqr_n, .-__mpn_impn_sqr_n
	.p2align 4,,15
	.globl	__mpn_mul_n
	.type	__mpn_mul_n, @function
__mpn_mul_n:
	pushq	%rbp
	cmpq	%rdx, %rsi
	movq	%rcx, %rax
	movq	%rsp, %rbp
	je	.L140
	cmpq	$31, %rcx
	jle	.L141
	salq	$4, %rcx
	addq	$16, %rcx
	subq	%rcx, %rsp
	movq	%rax, %rcx
	leaq	15(%rsp), %r8
	andq	$-16, %r8
	call	__mpn_impn_mul_n
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	call	__mpn_impn_mul_n_basecase
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	cmpq	$31, %rcx
	movq	%rcx, %rdx
	jg	.L136
	call	__mpn_impn_sqr_n_basecase
	leave
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	salq	$4, %rdx
	addq	$16, %rdx
	subq	%rdx, %rsp
	movq	%rax, %rdx
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	call	__mpn_impn_sqr_n
	leave
	ret
	.size	__mpn_mul_n, .-__mpn_mul_n
	.hidden	__mpn_sub_n
	.hidden	__mpn_cmp
	.hidden	__mpn_mul_1
	.hidden	__mpn_add_n
	.hidden	__mpn_addmul_1
