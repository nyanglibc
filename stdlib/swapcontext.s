.globl __swapcontext
.type __swapcontext,@function
.align 1<<4
__swapcontext:
	movq %rbx, 128(%rdi)
	movq %rbp, 120(%rdi)
	movq %r12, 72(%rdi)
	movq %r13, 80(%rdi)
	movq %r14, 88(%rdi)
	movq %r15, 96(%rdi)
	movq %rdi, 104(%rdi)
	movq %rsi, 112(%rdi)
	movq %rdx, 136(%rdi)
	movq %rcx, 152(%rdi)
	movq %r8, 40(%rdi)
	movq %r9, 48(%rdi)
	movq (%rsp), %rcx
	movq %rcx, 168(%rdi)
	leaq 8(%rsp), %rcx
	movq %rcx, 160(%rdi)
	leaq 424(%rdi), %rcx
	movq %rcx, 224(%rdi)
	fnstenv (%rcx)
	stmxcsr 448(%rdi)
	movq %rsi, %r12
	movq %rdi, %r9
	leaq 296(%rdi), %rdx
	leaq 296(%rsi), %rsi
	movl $2, %edi
	movl $8,%r10d
	movl $14, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	movq %r12, %rdx
	movq 224(%rdx), %rcx
	fldenv (%rcx)
	ldmxcsr 448(%rdx)
	movq 160(%rdx), %rsp
	movq 128(%rdx), %rbx
	movq 120(%rdx), %rbp
	movq 72(%rdx), %r12
	movq 80(%rdx), %r13
	movq 88(%rdx), %r14
	movq 96(%rdx), %r15
	movq 168(%rdx), %rcx
	pushq %rcx
	movq 104(%rdx), %rdi
	movq 112(%rdx), %rsi
	movq 152(%rdx), %rcx
	movq 40(%rdx), %r8
	movq 48(%rdx), %r9
	movq 136(%rdx), %rdx
	xorl %eax, %eax
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __swapcontext,.-__swapcontext
.weak swapcontext
swapcontext = __swapcontext
