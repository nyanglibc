	.text
	.p2align 4,,15
	.globl	__strtold_internal
	.hidden	__strtold_internal
	.type	__strtold_internal, @function
__strtold_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	____strtold_l_internal
	.size	__strtold_internal, .-__strtold_internal
	.p2align 4,,15
	.weak	strtold
	.hidden	strtold
	.type	strtold, @function
strtold:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	____strtold_l_internal
	.size	strtold, .-strtold
	.weak	strtof64x
	.set	strtof64x,strtold
	.hidden	____strtold_l_internal
