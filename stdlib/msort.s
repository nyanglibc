	.text
	.p2align 4,,15
	.type	msort_with_tmp.part.0, @function
msort_with_tmp.part.0:
.LFB49:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %rax
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	shrq	%r13
	subq	%r13, %rax
	movq	%rdi, %r12
	subq	$56, %rsp
	movq	(%rdi), %r8
	movq	%rax, %rbx
	movq	%rsi, 24(%rsp)
	movq	%rdx, 40(%rsp)
	imulq	%r13, %r8
	cmpq	$1, %r13
	leaq	(%rsi,%r8), %r14
	ja	.L66
.L2:
	cmpq	$1, %rbx
	ja	.L67
.L3:
	movq	(%r12), %rax
	testq	%rbx, %rbx
	movq	32(%r12), %r15
	movq	24(%rsp), %rbp
	movq	%rax, (%rsp)
	movq	16(%r12), %rax
	movq	%rax, 8(%rsp)
	movq	24(%r12), %rax
	movq	%rax, 16(%rsp)
	setne	%al
	testq	%r13, %r13
	setne	%dl
	andl	%edx, %eax
	movq	8(%r12), %rdx
	cmpq	$1, %rdx
	je	.L5
	jb	.L6
	cmpq	$2, %rdx
	je	.L7
	cmpq	$3, %rdx
	jne	.L4
	testb	%al, %al
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	testq	%r13, %r13
	jne	.L68
.L32:
	movq	40(%rsp), %rdx
	movq	32(%r12), %rsi
	movq	24(%rsp), %rdi
	subq	%rbx, %rdx
	imulq	(%rsp), %rdx
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L70:
	movq	%r10, %r15
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%r13, %r13
	je	.L10
	testq	%rbx, %rbx
	je	.L10
	movq	(%rsp), %rax
	movq	16(%rsp), %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	leaq	(%r15,%rax), %r10
	movq	8(%rsp), %rax
	movq	%r10, 32(%rsp)
	call	*%rax
	testl	%eax, %eax
	movq	32(%rsp), %r10
	jle	.L69
	movq	(%rsp), %rax
	movq	%r14, %rsi
	subq	$1, %rbx
	addq	%rax, %r14
.L20:
	cmpq	%r10, %r15
	jnb	.L70
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%r15, %rdi
	movsq
	cmpq	%rdi, %r10
	movq	%rdi, %r15
	ja	.L21
	movq	%r10, %r15
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%r14), %rax
	subq	$1, %rbx
	addq	$8, %r14
	movq	%rax, (%r15)
.L26:
	addq	$8, %r15
	testq	%r13, %r13
	je	.L10
	testq	%rbx, %rbx
	je	.L10
.L9:
	movq	16(%rsp), %rdx
	movq	(%r14), %rsi
	movq	0(%rbp), %rdi
	movq	8(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	jg	.L25
	movq	0(%rbp), %rax
	subq	$1, %r13
	addq	$8, %rbp
	movq	%rax, (%r15)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%rbp, %rsi
	movq	%r15, %rdi
	subq	$1, %r13
	call	__mempcpy@PLT
	addq	(%rsp), %rbp
	movq	%rax, %r15
.L4:
	testq	%r13, %r13
	je	.L10
.L72:
	testq	%rbx, %rbx
	je	.L10
	movq	16(%rsp), %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	movq	8(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	movq	(%rsp), %rdx
	jle	.L71
	movq	%r14, %rsi
	movq	%r15, %rdi
	subq	$1, %rbx
	call	__mempcpy@PLT
	addq	(%rsp), %r14
	testq	%r13, %r13
	movq	%rax, %r15
	jne	.L72
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L69:
	movq	(%rsp), %rax
	movq	%rbp, %rsi
	subq	$1, %r13
	addq	%rax, %rbp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%rsp), %rdx
	movq	%rbp, %rsi
	movq	%r15, %rdi
	imulq	%r13, %rdx
	call	memcpy@PLT
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L67:
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	msort_with_tmp.part.0
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r13, %rdx
	call	msort_with_tmp.part.0
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	testb	%al, %al
	jne	.L14
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L12:
	movl	(%r14), %eax
	subq	$1, %rbx
	addq	$4, %r14
	movl	%eax, (%r15)
.L13:
	addq	$4, %r15
	testq	%r13, %r13
	je	.L10
	testq	%rbx, %rbx
	je	.L10
.L14:
	movq	16(%rsp), %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	movq	8(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	jg	.L12
	movl	0(%rbp), %eax
	subq	$1, %r13
	addq	$4, %rbp
	movl	%eax, (%r15)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L5:
	testb	%al, %al
	jne	.L11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L16:
	movq	(%r14), %rax
	subq	$1, %rbx
	addq	$8, %r14
	movq	%rax, (%r15)
.L17:
	addq	$8, %r15
	testq	%r13, %r13
	je	.L10
	testq	%rbx, %rbx
	je	.L10
.L11:
	movq	16(%rsp), %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	movq	8(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	jg	.L16
	movq	0(%rbp), %rax
	subq	$1, %r13
	addq	$8, %rbp
	movq	%rax, (%r15)
	jmp	.L17
.LFE49:
	.size	msort_with_tmp.part.0, .-msort_with_tmp.part.0
	.p2align 4,,15
	.globl	__qsort_r
	.hidden	__qsort_r
	.type	__qsort_r, @function
__qsort_r:
.LFB47:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r15
	pushq	%rbx
	movq	%rcx, %r13
	subq	$104, %rsp
	cmpq	$32, %rdx
	movq	%rdi, -112(%rbp)
	movq	%rsi, -120(%rbp)
	ja	.L74
	imulq	%rdx, %rsi
	movq	%rsi, %r12
	cmpq	$1023, %r12
	ja	.L76
.L107:
	addq	$30, %r12
	movq	$0, -136(%rbp)
	andq	$-16, %r12
	subq	%r12, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -64(%rbp)
.L77:
	cmpq	$32, %r15
	movq	%r15, -96(%rbp)
	movq	$4, -88(%rbp)
	movq	%r13, -80(%rbp)
	movq	%r8, -72(%rbp)
	jbe	.L83
	movq	-120(%rbp), %rax
	movq	-64(%rbp), %rbx
	leaq	0(,%rax,8), %rsi
	addq	%rsi, %rbx
	leaq	(%rbx,%rsi), %rax
	cmpq	%rax, %rbx
	movq	%rax, -128(%rbp)
	jnb	.L97
	movq	-112(%rbp), %rdx
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	addq	%r15, %rdx
	cmpq	%rax, -128(%rbp)
	ja	.L85
	addq	-64(%rbp), %rsi
.L84:
	movq	-120(%rbp), %rdx
	movq	$8, -96(%rbp)
	movq	$3, -88(%rbp)
	cmpq	$1, %rdx
	ja	.L106
	cmpq	$0, -120(%rbp)
	je	.L88
.L87:
	movq	-112(%rbp), %r12
	movq	$0, -104(%rbp)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$1, -104(%rbp)
	addq	%r15, %r12
	movq	-104(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	je	.L88
.L91:
	movq	-104(%rbp), %rax
	movq	(%rbx,%rax,8), %r13
	cmpq	%r12, %r13
	je	.L89
	movq	-128(%rbp), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	%r12, %rdi
	movq	-104(%rbp), %rsi
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L98:
	movq	%rax, %r13
.L90:
	movq	%r13, %rax
	subq	-112(%rbp), %rax
	xorl	%edx, %edx
	movq	%rdi, (%rbx,%rsi,8)
	movq	%r13, %rsi
	divq	%r15
	movq	%r15, %rdx
	movq	%rax, %r14
	call	memcpy@PLT
	leaq	(%rbx,%r14,8), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	(%rdx), %rax
	cmpq	%r12, %rax
	jne	.L98
	movq	-128(%rbp), %rsi
	movq	%r13, (%rdx)
	movq	%r15, %rdx
	call	memcpy@PLT
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L74:
	movq	-120(%rbp), %r12
	salq	$4, %r12
	addq	%rdx, %r12
	cmpq	$1023, %r12
	jbe	.L107
.L76:
	movl	pagesize.8385(%rip), %edx
	testl	%edx, %edx
	je	.L108
.L78:
	movslq	%edx, %rsi
	movq	%r12, %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	phys_pages.8384(%rip), %rax
	jbe	.L80
.L82:
	movq	-120(%rbp), %rsi
	movq	-112(%rbp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	call	_quicksort@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movq	-112(%rbp), %rax
	orq	%r15, %rax
	testb	$3, %al
	je	.L109
.L93:
	movq	-120(%rbp), %rdx
	cmpq	$1, %rdx
	jbe	.L88
	movq	-112(%rbp), %rsi
	leaq	-96(%rbp), %rdi
	call	msort_with_tmp.part.0
.L88:
	movq	-136(%rbp), %rdi
	call	free@PLT
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movq	__libc_errno@gottpoff(%rip), %r14
	movq	%r12, %rdi
	movq	%r8, -104(%rbp)
	movl	%fs:(%r14), %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -136(%rbp)
	movq	-104(%rbp), %r8
	movl	%ebx, %fs:(%r14)
	je	.L82
	movq	%rax, -64(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$85, %edi
	movq	%r8, -104(%rbp)
	call	__sysconf
	cmpq	$-1, %rax
	movabsq	$2305843009213693951, %rdx
	movq	-104(%rbp), %r8
	je	.L79
	leaq	3(%rax), %rdx
	testq	%rax, %rax
	cmovns	%rax, %rdx
	sarq	$2, %rdx
.L79:
	movq	%r8, -104(%rbp)
	movq	%rdx, phys_pages.8384(%rip)
	movl	$30, %edi
	call	__sysconf
	movq	-104(%rbp), %r8
	movl	%eax, %edx
	movl	%eax, pagesize.8385(%rip)
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	-96(%rbp), %rdi
	call	msort_with_tmp.part.0
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L109:
	cmpq	$4, %r15
	je	.L110
	cmpq	$8, %r15
	je	.L111
.L95:
	testb	$7, %al
	jne	.L93
	movq	$2, -88(%rbp)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%rbx, %rsi
	jmp	.L84
.L111:
	testb	$7, -112(%rbp)
	jne	.L95
	movq	$1, -88(%rbp)
	jmp	.L93
.L110:
	movq	$0, -88(%rbp)
	jmp	.L93
.LFE47:
	.size	__qsort_r, .-__qsort_r
	.weak	qsort_r
	.set	qsort_r,__qsort_r
	.p2align 4,,15
	.globl	qsort
	.hidden	qsort
	.type	qsort, @function
qsort:
.LFB48:
	xorl	%r8d, %r8d
	jmp	__qsort_r
.LFE48:
	.size	qsort, .-qsort
	.local	phys_pages.8384
	.comm	phys_pages.8384,8,8
	.local	pagesize.8385
	.comm	pagesize.8385,4,4
	.hidden	__sysconf
