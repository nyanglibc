	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"on_exit.c"
.LC1:
	.string	"func != NULL"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__on_exit
	.type	__on_exit, @function
__on_exit:
	testq	%rdi, %rdi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L14
	movq	%rdi, %rbp
	movq	%rsi, %r12
#APP
# 33 "on_exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	__exit_funcs_lock@GOTPCREL(%rip), %rbx
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L4:
	leaq	__exit_funcs(%rip), %rdi
	call	__new_exitfn
	testq	%rax, %rax
	je	.L15
	movq	%rbp, %rdi
	movq	%r12, 16(%rax)
	movq	$2, (%rax)
#APP
# 43 "on_exit.c" 1
	xor %fs:48, %rdi
rol $2*8+1, %rdi
# 0 "" 2
#NO_APP
	movq	%rdi, 8(%rax)
#APP
# 48 "on_exit.c" 1
	movl %fs:24,%r8d
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	jne	.L8
	subl	$1, (%rbx)
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
#APP
# 38 "on_exit.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	subl	$1, (%rbx)
	movl	$-1, %r8d
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L4
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
#APP
# 48 "on_exit.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	xorl	%r8d, %r8d
	cmpl	$1, %eax
	jle	.L1
.L12:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 48 "on_exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
#APP
# 38 "on_exit.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	$-1, %r8d
	jle	.L1
	jmp	.L12
.L14:
	leaq	__PRETTY_FUNCTION__.7661(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$31, %edx
	call	__GI___assert_fail
	.size	__on_exit, .-__on_exit
	.weak	on_exit
	.set	on_exit,__on_exit
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.7661, @object
	.size	__PRETTY_FUNCTION__.7661, 10
__PRETTY_FUNCTION__.7661:
	.string	"__on_exit"
	.hidden	__lll_lock_wait_private
	.hidden	__new_exitfn
	.hidden	__exit_funcs
