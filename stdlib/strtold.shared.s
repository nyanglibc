	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___strtold_internal
	.hidden	__GI___strtold_internal
	.type	__GI___strtold_internal, @function
__GI___strtold_internal:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtold_l_internal
	.size	__GI___strtold_internal, .-__GI___strtold_internal
	.globl	__strtold_internal
	.set	__strtold_internal,__GI___strtold_internal
	.p2align 4,,15
	.weak	__GI_strtold
	.hidden	__GI_strtold
	.type	__GI_strtold, @function
__GI_strtold:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movq	%fs:(%rax), %rcx
	jmp	__GI_____strtold_l_internal
	.size	__GI_strtold, .-__GI_strtold
	.globl	strtold
	.set	strtold,__GI_strtold
	.weak	strtof64x
	.set	strtof64x,strtold
