	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	mblen
	.type	mblen, @function
mblen:
	testq	%rdi, %rdi
	pushq	%rbx
	je	.L10
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	jne	.L11
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	state(%rip), %rcx
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	movq	$0, state(%rip)
	call	__GI___mbrtowc
	movl	$-1, %edx
	testl	%eax, %eax
	cmovs	%edx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rbx
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L12
.L4:
	movq	$0, state(%rip)
	movq	(%rax), %rax
	popq	%rbx
	movl	88(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	_nl_C_LC_CTYPE(%rip), %rax
	cmpq	%rax, %rbx
	je	.L6
	movq	%rbx, %rdi
	call	__wcsmbs_load_conv
	movq	40(%rbx), %rax
	jmp	.L4
.L6:
	leaq	__wcsmbs_gconv_fcts_c(%rip), %rax
	jmp	.L4
	.size	mblen, .-mblen
	.local	state
	.comm	state,8,8
	.hidden	__wcsmbs_gconv_fcts_c
	.hidden	__wcsmbs_load_conv
	.hidden	_nl_C_LC_CTYPE
