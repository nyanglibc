	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_atoi
	.hidden	__GI_atoi
	.type	__GI_atoi, @function
__GI_atoi:
	subq	$8, %rsp
	movl	$10, %edx
	xorl	%esi, %esi
	call	__GI_strtol
	addq	$8, %rsp
	ret
	.size	__GI_atoi, .-__GI_atoi
	.globl	atoi
	.set	atoi,__GI_atoi
