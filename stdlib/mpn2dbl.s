	.text
	.p2align 4,,15
	.globl	__mpn_construct_double
	.hidden	__mpn_construct_double
	.type	__mpn_construct_double, @function
__mpn_construct_double:
	movq	(%rdi), %rax
	addw	$1023, %si
	salq	$63, %rdx
	andl	$2047, %esi
	salq	$52, %rsi
	movl	%eax, %ecx
	orq	%rsi, %rdx
	movabsq	$4503595332403200, %rsi
	andq	%rsi, %rax
	orq	%rcx, %rdx
	orq	%rax, %rdx
	movq	%rdx, -8(%rsp)
	movsd	-8(%rsp), %xmm0
	ret
	.size	__mpn_construct_double, .-__mpn_construct_double
