	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	eventfd_read
	.type	eventfd_read, @function
eventfd_read:
	subq	$8, %rsp
	movl	$8, %edx
	call	__GI___read
	cmpq	$8, %rax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	eventfd_read, .-eventfd_read
