.text
 .globl epoll_ctl
 .type epoll_ctl,@function
 .align 1<<4
 epoll_ctl: 
 
 movq %rcx, %r10
 movl $233, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size epoll_ctl,.-epoll_ctl
.globl __GI_epoll_ctl 
 .set __GI_epoll_ctl,epoll_ctl
