	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ftruncate64
	.hidden	__ftruncate64
	.type	__ftruncate64, @function
__ftruncate64:
	movl	$77, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/ftruncate64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__ftruncate64, .-__ftruncate64
	.weak	ftruncate
	.set	ftruncate,__ftruncate64
	.weak	__ftruncate
	.hidden	__ftruncate
	.set	__ftruncate,__ftruncate64
	.weak	ftruncate64
	.set	ftruncate64,__ftruncate64
