	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	eventfd_write
	.type	eventfd_write, @function
eventfd_write:
	subq	$24, %rsp
	movl	$8, %edx
	movq	%rsi, 8(%rsp)
	leaq	8(%rsp), %rsi
	call	__GI___write
	cmpq	$8, %rax
	setne	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	eventfd_write, .-eventfd_write
