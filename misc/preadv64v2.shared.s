	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	preadv64v2
	.type	preadv64v2, @function
preadv64v2:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %r12d
	movq	%rcx, %rbx
	movl	%r8d, %ebp
	subq	$24, %rsp
#APP
# 26 "../sysdeps/unix/sysv/linux/preadv64v2.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	%r8d, %r9d
	movq	%rcx, %r10
	xorl	%r8d, %r8d
	movl	$327, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/preadv64v2.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
.L3:
	testq	%rax, %rax
	js	.L12
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rcx
	movl	%fs:(%rcx), %edx
.L4:
	cmpl	$38, %edx
	jne	.L1
	testl	%ebp, %ebp
	jne	.L13
	cmpq	$-1, %rbx
	je	.L14
	addq	$24, %rsp
	movq	%rbx, %rcx
	movl	%r14d, %edx
	popq	%rbx
	movq	%r13, %rsi
	movl	%r12d, %edi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__GI_preadv64
	.p2align 4,,10
	.p2align 3
.L2:
	call	__libc_enable_asynccancel
	movl	%ebp, %r9d
	movl	%eax, %r15d
	xorl	%r8d, %r8d
	movq	%rbx, %r10
	movl	%r14d, %edx
	movq	%r13, %rsi
	movl	%r12d, %edi
	movl	$327, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/preadv64v2.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L15
	movl	%r15d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rcx
	movl	%eax, %edx
	movq	$-1, %rax
	negl	%edx
	movl	%edx, %fs:(%rcx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$24, %rsp
	movl	%r14d, %edx
	movq	%r13, %rsi
	popq	%rbx
	movl	%r12d, %edi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__GI___readv
.L15:
	movq	__libc_errno@gottpoff(%rip), %rcx
	negl	%eax
	movl	%r15d, %edi
	movl	%eax, %fs:(%rcx)
	movq	%rcx, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rcx
	movq	$-1, %rax
	movl	%fs:(%rcx), %edx
	jmp	.L4
.L13:
	movl	$95, %fs:(%rcx)
	movq	$-1, %rax
	jmp	.L1
	.size	preadv64v2, .-preadv64v2
	.globl	preadv2
	.set	preadv2,preadv64v2
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
