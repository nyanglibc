	.text
	.p2align 4,,15
	.globl	__prctl
	.hidden	__prctl
	.type	__prctl, @function
__prctl:
	leaq	8(%rsp), %rax
	movq	%rsi, -40(%rsp)
	movq	%rdx, -32(%rsp)
	movq	%rcx, -24(%rsp)
	movq	%r8, -16(%rsp)
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movl	$32, -72(%rsp)
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	movq	%rax, -56(%rsp)
	movq	32(%rax), %r8
	movq	24(%rax), %r10
	movl	$157, %eax
#APP
# 38 "../sysdeps/unix/sysv/linux/prctl.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L13
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__prctl, .-__prctl
	.weak	prctl
	.set	prctl,__prctl
