	.text
	.p2align 4,,15
	.globl	open_by_handle_at
	.type	open_by_handle_at, @function
open_by_handle_at:
#APP
# 27 "../sysdeps/unix/sysv/linux/open_by_handle_at.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$304, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/open_by_handle_at.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$304, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/open_by_handle_at.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	open_by_handle_at, .-open_by_handle_at
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
