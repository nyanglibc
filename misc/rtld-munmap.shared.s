.text
.globl __munmap
.type __munmap,@function
.align 1<<4
__munmap:
	movl $11, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	lea rtld_errno(%rip), %rcx
	neg %eax
	movl %eax, (%rcx)
	or $-1, %rax
	ret
.size __munmap,.-__munmap
.globl __GI___munmap
.set __GI___munmap,__munmap
.weak munmap
munmap = __munmap
.globl __GI_munmap
.set __GI_munmap,munmap
