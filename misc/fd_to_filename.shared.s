	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"fd_to_filename.c"
.LC1:
	.string	"descriptor >= 0"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__fd_to_filename
	.hidden	__fd_to_filename
	.type	__fd_to_filename, @function
__fd_to_filename:
	testl	%edi, %edi
	js	.L11
	movabsq	$7310238724270485551, %rax
	leaq	14(%rsi), %rcx
	movl	$1714382444, 8(%rsi)
	movq	%rax, (%rsi)
	movl	$12132, %eax
	movl	%edi, %edx
	movw	%ax, 12(%rsi)
	movl	$-858993459, %r8d
.L3:
	movl	%edx, %eax
	addq	$1, %rcx
	mull	%r8d
	shrl	$3, %edx
	testl	%edx, %edx
	jne	.L3
	movb	$0, (%rcx)
	movl	$-858993459, %r8d
.L4:
	movl	%edi, %eax
	subq	$1, %rcx
	mull	%r8d
	shrl	$3, %edx
	leal	(%rdx,%rdx,4), %eax
	addl	%eax, %eax
	subl	%eax, %edi
	addl	$48, %edi
	testl	%edx, %edx
	movb	%dil, (%rcx)
	movl	%edx, %edi
	jne	.L4
	movq	%rsi, %rax
	ret
.L11:
	leaq	__PRETTY_FUNCTION__.2539(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$27, %edx
	call	__GI___assert_fail
	.size	__fd_to_filename, .-__fd_to_filename
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.2539, @object
	.size	__PRETTY_FUNCTION__.2539, 17
__PRETTY_FUNCTION__.2539:
	.string	"__fd_to_filename"
