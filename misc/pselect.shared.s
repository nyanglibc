	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pselect
	.type	__pselect, @function
__pselect:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r10
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebx, %ebx
	subq	$56, %rsp
	testq	%r8, %r8
	je	.L2
	movdqu	(%r8), %xmm0
	leaq	16(%rsp), %rbx
	movaps	%xmm0, 16(%rsp)
.L2:
	movq	%r9, 32(%rsp)
	movq	$8, 40(%rsp)
#APP
# 52 "../sysdeps/unix/sysv/linux/pselect.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	32(%rsp), %r9
	jne	.L3
	movq	%rbx, %r8
	movl	$270, %eax
#APP
# 52 "../sysdeps/unix/sysv/linux/pselect.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L13
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r10, %r15
	movq	%rdx, %r14
	movq	%rsi, %r13
	movl	%edi, %r12d
	movq	%r9, 8(%rsp)
	call	__libc_enable_asynccancel
	movq	8(%rsp), %r9
	movl	%eax, %ebp
	movq	%rbx, %r8
	movq	%r15, %r10
	movq	%r14, %rdx
	movq	%r13, %rsi
	movl	%r12d, %edi
	movl	$270, %eax
#APP
# 52 "../sysdeps/unix/sysv/linux/pselect.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
.L7:
	movl	%ebp, %edi
	movl	%eax, 8(%rsp)
	call	__libc_disable_asynccancel
	movl	8(%rsp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L7
	.size	__pselect, .-__pselect
	.weak	pselect
	.set	pselect,__pselect
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
