	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC5:
	.string	"%.*f"
	.text
	.p2align 4,,15
	.globl	__fcvt_r
	.hidden	__fcvt_r
	.type	__fcvt_r, @function
__fcvt_r:
	testq	%rcx, %rcx
	je	.L52
	pushq	%r15
	pushq	%r14
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movapd	%xmm0, %xmm1
	subq	$24, %rsp
	andpd	.LC0(%rip), %xmm0
	movsd	.LC1(%rip), %xmm2
	ucomisd	%xmm0, %xmm2
	jnb	.L53
	movl	$0, (%rdx)
.L48:
	cmpl	$17, %edi
	movl	$17, %ecx
	cmovle	%edi, %ecx
	xorl	%r12d, %r12d
.L7:
	movapd	%xmm1, %xmm0
	leaq	.LC5(%rip), %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	movl	$1, %eax
	movsd	%xmm1, 8(%rsp)
	call	__snprintf
	movslq	%eax, %rbx
	cmpq	%rbx, %r14
	jle	.L26
	testq	%rbx, %rbx
	movsd	8(%rsp), %xmm1
	jle	.L10
	movsbl	0(%rbp), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L10
	xorl	%esi, %esi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L55:
	movsbl	1(%rbp,%rsi), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L54
	movq	%r9, %rsi
.L12:
	leaq	1(%rsi), %r9
	cmpq	%r9, %rbx
	jne	.L55
	movl	%eax, 0(%r13)
.L14:
	testl	%r12d, %r12d
	je	.L50
	subq	$1, %r14
	addl	%r12d, 0(%r13)
	cmpq	%rbx, %r14
	jle	.L50
	subl	$1, %r12d
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$1, %rbx
	cmpq	%rbx, %r14
	movb	$48, -1(%rbp,%rbx)
	setg	%dl
	testl	%r12d, %r12d
	setg	%al
	subl	$1, %r12d
	testb	%al, %dl
	jne	.L24
	movb	$0, 0(%rbp,%rbx)
.L50:
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movmskpd	%xmm1, %eax
	xorl	%ecx, %ecx
	andl	$1, %eax
	setne	%cl
	movl	%ecx, (%rdx)
	je	.L5
	xorpd	.LC2(%rip), %xmm1
.L5:
	testl	%edi, %edi
	jns	.L48
	negl	%edi
	movapd	%xmm1, %xmm0
	movsd	.LC3(%rip), %xmm3
	movl	%edi, %r12d
	movsd	.LC4(%rip), %xmm2
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L8:
	mulsd	%xmm3, %xmm1
	ucomisd	%xmm1, %xmm2
	ja	.L25
	addl	$1, %eax
	movapd	%xmm1, %xmm0
	cmpl	%eax, %r12d
	jne	.L8
	xorl	%ecx, %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L54:
	movslq	%r9d, %rcx
	movl	%r9d, 0(%r13)
	leaq	2(%rbp,%rsi), %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L56:
	movsbl	(%rsi), %eax
	addq	$1, %rdx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L15
	movq	%r15, %r9
.L16:
	leaq	1(%r9), %r15
	movq	%rdx, %rsi
	cmpq	%r15, %rbx
	jg	.L56
.L15:
	cmpl	$1, %ecx
	je	.L57
	testl	%ecx, %ecx
	movl	$0, %edi
	cmovns	%rcx, %rdi
	addq	%rbp, %rdi
.L20:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	call	memmove
	movl	0(%r13), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	0(%r13), %eax
	cltq
	subq	%rax, %r15
	movq	%rbx, %rax
	subq	%r15, %rax
	movb	$0, 0(%rbp,%rax)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L57:
	cmpb	$48, 0(%rbp)
	jne	.L31
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	ucomisd	%xmm0, %xmm1
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L18
.L31:
	leaq	1(%rbp), %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L10:
	movl	$0, 0(%r13)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%eax, %r12d
	movapd	%xmm0, %xmm1
	xorl	%ecx, %ecx
	jmp	.L7
.L18:
	cmpq	%r15, %rbx
	movl	$0, 0(%r13)
	jle	.L30
	cmpb	$48, (%rsi)
	jne	.L30
	leaq	2(%rbp,%r9), %rcx
	movl	$-1, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	subl	$1, %edx
	cmpb	$48, (%rsi)
	leaq	1(%rcx), %rcx
	jne	.L58
.L22:
	addq	$1, %r15
	movslq	%edx, %rax
	movl	%edx, 0(%r13)
	cmpq	%r15, %rbx
	movq	%rcx, %rsi
	jne	.L21
	testl	%edx, %edx
	movl	$0, %edi
	cmovns	%edx, %edi
	movslq	%edi, %rdi
	addq	%rbp, %rdi
	jmp	.L20
.L26:
	movl	$-1, %eax
	jmp	.L1
.L52:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
.L58:
	testl	%eax, %eax
	movl	$0, %edi
	cmovns	%rax, %rdi
	addq	%rbp, %rdi
	jmp	.L20
.L30:
	movq	%rbp, %rdi
	jmp	.L20
	.size	__fcvt_r, .-__fcvt_r
	.weak	fcvt_r
	.set	fcvt_r,__fcvt_r
	.p2align 4,,15
	.globl	__ecvt_r
	.hidden	__ecvt_r
	.type	__ecvt_r, @function
__ecvt_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movapd	%xmm0, %xmm1
	subq	$8, %rsp
	testl	%edi, %edi
	movq	.LC0(%rip), %xmm2
	setle	%al
	testq	%r8, %r8
	movsd	.LC1(%rip), %xmm3
	setne	%sil
	andpd	%xmm2, %xmm1
	andl	%esi, %eax
	ucomisd	%xmm1, %xmm3
	jb	.L60
	pxor	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jp	.L84
	je	.L95
.L84:
	ucomisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm4
	ja	.L101
.L63:
	movsd	.LC7(%rip), %xmm5
	xorl	%ebx, %ebx
	ucomisd	%xmm4, %xmm5
	jbe	.L65
	divsd	%xmm5, %xmm0
	movl	$-307, %ebx
	ucomisd	%xmm0, %xmm1
	movapd	%xmm0, %xmm4
	ja	.L102
.L65:
	movsd	.LC4(%rip), %xmm1
	ucomisd	%xmm4, %xmm1
	ja	.L103
	movsd	.LC8(%rip), %xmm5
	ucomisd	%xmm5, %xmm4
	jb	.L61
	.p2align 4,,10
	.p2align 3
.L72:
	mulsd	%xmm5, %xmm1
	addl	$1, %ebx
	movapd	%xmm1, %xmm6
	mulsd	%xmm5, %xmm6
	ucomisd	%xmm6, %xmm4
	jnb	.L72
	divsd	%xmm1, %xmm0
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%ebx, %ebx
	testb	%al, %al
	je	.L73
	movb	$0, (%rcx)
	xorl	%ebx, %ebx
	movl	$1, 0(%rbp)
	xorl	%eax, %eax
.L74:
	movl	%eax, (%rdx)
.L75:
	addl	%ebx, 0(%rbp)
	xorl	%eax, %eax
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$17, %edi
	movl	$17, %eax
	movq	%rbp, %rsi
	cmovg	%eax, %edi
	subl	$1, %edi
	call	__fcvt_r
	testl	%eax, %eax
	je	.L75
	movl	$-1, %eax
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%ebx, %ebx
.L61:
	testb	%al, %al
	je	.L73
	andpd	%xmm0, %xmm2
	movb	$0, (%rcx)
	movl	$1, 0(%rbp)
	ucomisd	%xmm2, %xmm3
	jb	.L82
	movmskpd	%xmm0, %eax
	andl	$1, %eax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L103:
	movapd	%xmm1, %xmm6
	movsd	.LC8(%rip), %xmm5
	.p2align 4,,10
	.p2align 3
.L70:
	mulsd	%xmm5, %xmm6
	movapd	%xmm4, %xmm7
	subl	$1, %ebx
	mulsd	%xmm6, %xmm7
	ucomisd	%xmm7, %xmm1
	ja	.L70
	mulsd	%xmm6, %xmm0
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L101:
	xorpd	.LC2(%rip), %xmm4
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L102:
	xorpd	.LC2(%rip), %xmm4
	movl	$-307, %ebx
	jmp	.L65
.L82:
	xorl	%eax, %eax
	jmp	.L74
	.size	__ecvt_r, .-__ecvt_r
	.weak	ecvt_r
	.set	ecvt_r,__ecvt_r
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	2147483647
	.long	0
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	4294967295
	.long	2146435071
	.section	.rodata.cst16
	.align 16
.LC2:
	.long	0
	.long	-2147483648
	.long	0
	.long	0
	.section	.rodata.cst8
	.align 8
.LC3:
	.long	2576980378
	.long	1069128089
	.align 8
.LC4:
	.long	0
	.long	1072693248
	.align 8
.LC7:
	.long	742442509
	.long	3275288
	.align 8
.LC8:
	.long	0
	.long	1076101120
	.hidden	memmove
	.hidden	__snprintf
