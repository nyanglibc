	.text
#APP
	.section .gnu.glibc-stub.revoke
	.previous
	.section .gnu.warning.revoke
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__revoke
	.type	__revoke, @function
__revoke:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__revoke, .-__revoke
	.weak	revoke
	.set	revoke,__revoke
	.section	.gnu.warning.revoke
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_revoke, @object
	.size	__evoke_link_warning_revoke, 47
__evoke_link_warning_revoke:
	.string	"revoke is not implemented and will always fail"
