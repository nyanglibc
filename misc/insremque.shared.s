	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	insque
	.type	insque, @function
insque:
	testq	%rsi, %rsi
	je	.L9
	movq	(%rsi), %rax
	movq	%rdi, (%rsi)
	testq	%rax, %rax
	je	.L4
	movq	%rdi, 8(%rax)
.L4:
	movq	%rax, (%rdi)
	movq	%rsi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	ret
	.size	insque, .-insque
	.p2align 4,,15
	.globl	remque
	.type	remque, @function
remque:
	movq	(%rdi), %rdx
	movq	8(%rdi), %rax
	testq	%rdx, %rdx
	je	.L11
	movq	%rax, 8(%rdx)
.L11:
	testq	%rax, %rax
	je	.L10
	movq	%rdx, (%rax)
.L10:
	rep ret
	.size	remque, .-remque
