.text
 .globl unshare
 .type unshare,@function
 .align 1<<4
 unshare: 
 
 movl $272, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size unshare,.-unshare
.globl __GI_unshare 
 .set __GI_unshare,unshare
