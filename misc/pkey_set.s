	.text
	.p2align 4,,15
	.globl	pkey_set
	.type	pkey_set, @function
pkey_set:
	cmpl	$15, %edi
	ja	.L5
	cmpl	$3, %esi
	ja	.L5
	xorl	%r8d, %r8d
	addl	%edi, %edi
	movl	%r8d, %ecx
#APP
# 27 "../sysdeps/unix/sysv/linux/x86/arch-pkey.h" 1
	.byte 0x0f, 0x01, 0xee
# 0 "" 2
#NO_APP
	movl	$3, %edx
	movl	%edi, %ecx
	sall	%cl, %edx
	sall	%cl, %esi
	movl	%r8d, %ecx
	notl	%edx
	andl	%edx, %eax
	movl	%r8d, %edx
	orl	%esi, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/x86/arch-pkey.h" 1
	.byte 0x0f, 0x01, 0xef
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	pkey_set, .-pkey_set
