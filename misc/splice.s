	.text
	.p2align 4,,15
	.globl	splice
	.type	splice, @function
splice:
	movq	%rcx, %r10
#APP
# 26 "../sysdeps/unix/sysv/linux/splice.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$275, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/splice.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r14
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r13d
	movq	%rsi, %r12
	movl	%edi, %ebx
	subq	$24, %rsp
	movl	%r9d, 8(%rsp)
	call	__libc_enable_asynccancel
	movl	8(%rsp), %r9d
	movl	%eax, %ebp
	movq	%r15, %r8
	movq	%r14, %r10
	movl	%r13d, %edx
	movq	%r12, %rsi
	movl	%ebx, %edi
	movl	$275, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/splice.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%ebp, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	splice, .-splice
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
