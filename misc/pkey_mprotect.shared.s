	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	pkey_mprotect
	.type	pkey_mprotect, @function
pkey_mprotect:
	cmpl	$-1, %ecx
	movl	%ecx, %r10d
	je	.L6
	movl	$329, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/pkey_mprotect.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	jmp	__GI___mprotect
	.size	pkey_mprotect, .-pkey_mprotect
