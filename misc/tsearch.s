	.text
	.p2align 4,,15
	.type	trecurse, @function
trecurse:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	testq	$-2, 8(%rdi)
	je	.L14
.L2:
	movq	%rdi, %rbx
	xorl	%esi, %esi
	movl	%edx, %r12d
	call	*%rbp
	movq	8(%rbx), %rdi
	andq	$-2, %rdi
	jne	.L15
.L3:
	movq	%rbx, %rdi
	movl	%r12d, %edx
	movl	$1, %esi
	call	*%rbp
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L16
.L4:
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	$2, %esi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L14:
	cmpq	$0, 16(%rdi)
	jne	.L2
	popq	%rbx
	movq	%rbp, %rax
	movl	$3, %esi
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L15:
	leal	1(%r12), %edx
	movq	%rbp, %rsi
	call	trecurse
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	leal	1(%r12), %edx
	movq	%rbp, %rsi
	call	trecurse
	jmp	.L4
	.size	trecurse, .-trecurse
	.p2align 4,,15
	.type	trecurse_r, @function
trecurse_r:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	testq	$-2, 8(%rdi)
	je	.L29
.L18:
	movq	%rdi, %rbx
	xorl	%esi, %esi
	movq	%rdx, %r12
	call	*%rbp
	movq	8(%rbx), %rdi
	andq	$-2, %rdi
	jne	.L30
.L19:
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movl	$1, %esi
	call	*%rbp
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L31
.L20:
	movq	%r12, %rdx
	movq	%rbx, %rdi
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	$2, %esi
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L29:
	cmpq	$0, 16(%rdi)
	jne	.L18
	popq	%rbx
	movq	%rbp, %rax
	movl	$3, %esi
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L30:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	trecurse_r
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	trecurse_r
	jmp	.L20
	.size	trecurse_r, .-trecurse_r
	.p2align 4,,15
	.type	tdestroy_recurse, @function
tdestroy_recurse:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	andq	$-2, %rdi
	jne	.L42
.L33:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L43
.L34:
	movq	(%rbx), %rdi
	call	*%rbp
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rbp, %rsi
	call	tdestroy_recurse
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L42:
	call	tdestroy_recurse
	jmp	.L33
	.size	tdestroy_recurse, .-tdestroy_recurse
	.p2align 4,,15
	.type	maybe_split_for_insert.isra.0, @function
maybe_split_for_insert.isra.0:
	andq	$-2, %rdi
	movq	8(%rdi), %rax
	movq	16(%rdi), %r10
	movq	%rax, %r11
	andq	$-2, %r11
	cmpl	$1, %r9d
	je	.L45
	testq	%r10, %r10
	je	.L77
	testq	%r11, %r11
	je	.L77
	testb	$1, 8(%r10)
	je	.L77
	testb	$1, 8(%r11)
	je	.L80
	orq	$1, %rax
	movq	%rax, 8(%rdi)
.L57:
	andq	$-2, 8(%r10)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L45:
	orq	$1, %rax
	testq	%r10, %r10
	movq	%rax, 8(%rdi)
	jne	.L57
.L51:
	testq	%r11, %r11
	je	.L52
	andq	$-2, 8(%r11)
.L52:
	testq	%rsi, %rsi
	je	.L77
	movq	(%rsi), %rax
	andq	$-2, %rax
	movq	8(%rax), %rsi
	testb	$1, %sil
	jne	.L81
.L77:
	rep ret
	.p2align 4,,10
	.p2align 3
.L81:
	pushq	%rbp
	pushq	%rbx
	movq	(%rdx), %rbx
	movq	%rbx, %r9
	andq	$-2, %r9
	testl	%ecx, %ecx
	setg	%bpl
	testl	%r8d, %r8d
	setg	%r8b
	cmpb	%r8b, %bpl
	je	.L53
	orq	$1, %rsi
	movq	%rsi, 8(%rax)
	movq	%r10, %rsi
	orq	$1, 8(%r9)
	andq	$-2, 8(%rdi)
	andl	$1, %esi
	testl	%ecx, %ecx
	js	.L82
	movq	%r11, 16(%rax)
	movq	%rax, 8(%rdi)
	movq	8(%r9), %rax
	andl	$1, %eax
	orq	%rax, %r10
	movq	%r10, 8(%r9)
	orq	%rsi, %r9
	movq	%r9, 16(%rdi)
.L55:
	movq	(%rdx), %rax
	andl	$1, %eax
	orq	%rax, %rdi
	movq	%rdi, (%rdx)
.L44:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	rep ret
	.p2align 4,,10
	.p2align 3
.L53:
	andl	$1, %ebx
	andq	$-2, %rsi
	orq	%rax, %rbx
	movq	%rbx, (%rdx)
	movq	%rsi, 8(%rax)
	orq	$1, 8(%r9)
	testl	%ecx, %ecx
	js	.L83
	movq	8(%rax), %rdx
	movq	%rdx, %rcx
	andl	$1, %edx
	andq	$-2, %rcx
	movq	%rcx, 16(%r9)
	orq	%rdx, %r9
	movq	%r9, 8(%rax)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L83:
	movq	16(%rax), %rdx
	orq	$1, %rdx
	movq	%rdx, 8(%r9)
	movq	%r9, 16(%rax)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L82:
	movq	8(%rax), %rcx
	andl	$1, %ecx
	orq	%rcx, %r10
	movq	%r10, 8(%rax)
	orq	%rsi, %rax
	movq	%rax, 16(%rdi)
	movq	%r11, 16(%r9)
	movq	%r9, 8(%rdi)
	jmp	.L55
	.size	maybe_split_for_insert.isra.0, .-maybe_split_for_insert.isra.0
	.p2align 4,,15
	.globl	__tsearch
	.hidden	__tsearch
	.type	__tsearch, @function
__tsearch:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	testq	%rsi, %rsi
	movq	%rdi, 16(%rsp)
	movq	%rdx, 24(%rsp)
	je	.L93
	movq	(%rsi), %rbx
	movq	%rsi, %r12
	movq	%rbx, %rax
	andq	$-2, %rax
	je	.L94
	andq	$-2, 8(%rax)
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	movq	16(%rbx), %rax
	leaq	16(%rbx), %r10
.L90:
	testq	$-2, %rax
	je	.L91
	movq	(%r10), %rbx
	movq	%r13, %r15
	movl	%r14d, %r8d
	movq	%r12, %r13
	movl	%ebp, %r14d
	movq	%r10, %r12
.L88:
	andq	$-2, %rbx
	je	.L105
	movl	%r8d, (%rsp)
	movq	%rbx, 8(%rsp)
	movq	(%rbx), %rsi
	movq	16(%rsp), %rdi
	movq	24(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L84
	movl	(%rsp), %r8d
	movq	(%r12), %rdi
	xorl	%r9d, %r9d
	movl	%r14d, %ecx
	movq	%r15, %rdx
	movq	%r13, %rsi
	call	maybe_split_for_insert.isra.0
	testl	%ebp, %ebp
	jns	.L89
	leaq	8(%rbx), %r10
	movq	8(%rbx), %rax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L94:
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
.L87:
	movl	%ebp, %r14d
	movq	%r12, %r10
.L91:
	movl	$24, %edi
	movq	%r10, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, 8(%rsp)
	je	.L93
	movq	(%rsp), %r10
	movq	(%r10), %rax
	andl	$1, %eax
	orq	%rcx, %rax
	cmpq	%r12, %r10
	movq	%rax, (%r10)
	movq	16(%rsp), %rax
	movq	$1, 8(%rcx)
	movq	$0, 16(%rcx)
	movq	%rax, (%rcx)
	je	.L84
	movq	(%r10), %rdi
	movl	$1, %r9d
	movl	%r14d, %r8d
	movl	%ebp, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	maybe_split_for_insert.isra.0
.L84:
	movq	8(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movq	$0, 8(%rsp)
	jmp	.L84
.L105:
	movl	%r14d, %ebp
	jmp	.L87
	.size	__tsearch, .-__tsearch
	.weak	tsearch
	.set	tsearch,__tsearch
	.p2align 4,,15
	.globl	__tfind
	.hidden	__tfind
	.type	__tfind, @function
__tfind:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$8, %rsp
	testq	%rsi, %rsi
	jne	.L107
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L111:
	movq	(%rbx), %rsi
	movq	%r12, %rdi
	movq	%rbx, %r13
	call	*%rbp
	testl	%eax, %eax
	je	.L106
	leaq	8(%rbx), %rsi
	addq	$16, %rbx
	testl	%eax, %eax
	cmovns	%rbx, %rsi
.L107:
	movq	(%rsi), %rbx
	andq	$-2, %rbx
	jne	.L111
.L112:
	xorl	%r13d, %r13d
.L106:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__tfind, .-__tfind
	.weak	tfind
	.set	tfind,__tfind
	.p2align 4,,15
	.globl	__tdelete
	.hidden	__tdelete
	.type	__tdelete, @function
__tdelete:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$376, %rsp
	leaq	15(%rsp), %r8
	andq	$-16, %r8
	testq	%rsi, %rsi
	je	.L122
	movq	(%rsi), %rbx
	movq	%rsi, %r12
	andq	$-2, %rbx
	je	.L122
	movq	%rdi, %r13
	movq	%rdx, %r14
	movq	%rbx, %rax
	xorl	%r15d, %r15d
	movl	$40, -52(%rbp)
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L125:
	movq	16(%rbx), %rax
	addq	$1, %r15
	leaq	16(%rbx), %r12
	testq	%rax, %rax
	je	.L122
.L123:
	movq	%r8, -72(%rbp)
	movl	%r15d, -64(%rbp)
	movq	%r13, %rdi
	movq	(%rax), %rsi
	call	*%r14
	testl	%eax, %eax
	movl	%eax, %r9d
	movl	-64(%rbp), %r10d
	movq	-72(%rbp), %r8
	je	.L215
	cmpl	%r15d, -52(%rbp)
	leaq	0(,%r15,8), %rbx
	je	.L216
.L124:
	movq	%r12, (%r8,%rbx)
	movq	(%r12), %rbx
	andq	$-2, %rbx
	testl	%r9d, %r9d
	jns	.L125
	movq	8(%rbx), %rax
	addq	$1, %r15
	leaq	8(%rbx), %r12
	andq	$-2, %rax
	testq	%rax, %rax
	jne	.L123
.L122:
	xorl	%ebx, %ebx
.L119:
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L216:
	addl	$20, -52(%rbp)
	movq	%r8, %rsi
	movq	%rbx, %rdx
	movslq	-52(%rbp), %rax
	movl	%r9d, -64(%rbp)
	leaq	30(,%rax,8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	call	memcpy@PLT
	movl	-64(%rbp), %r9d
	movq	%rax, %r8
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L215:
	movq	(%r12), %r11
	movq	%r11, %r9
	andq	$-2, %r9
	movq	8(%r9), %r13
	andq	$-2, %r13
	cmpq	$0, 16(%r9)
	je	.L128
	testq	%r13, %r13
	je	.L128
	movslq	%r10d, %r13
	leaq	16(%r9), %r14
	movq	%r12, %r15
	salq	$3, %r13
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L129:
	movq	(%r14), %rdi
	addl	$1, %r10d
	movq	%r15, (%r8,%r13)
	addq	$8, %r13
	andq	$-2, %rdi
	testq	$-2, 8(%rdi)
	je	.L130
	movq	%r14, %r15
	leaq	8(%rdi), %r14
.L131:
	cmpl	%r10d, -52(%rbp)
	jne	.L129
	addl	$20, -52(%rbp)
	movq	%r8, %rsi
	movq	%r13, %rdx
	movslq	-52(%rbp), %rax
	movl	%r10d, -56(%rbp)
	movq	%r9, -72(%rbp)
	movq	%r11, -64(%rbp)
	leaq	30(,%rax,8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	call	memcpy@PLT
	movl	-56(%rbp), %r10d
	movq	%rax, %r8
	movq	-72(%rbp), %r9
	movq	-64(%rbp), %r11
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L128:
	testq	%r13, %r13
	movq	%r9, %rdi
	je	.L130
.L132:
	testl	%r10d, %r10d
	jne	.L133
	andl	$1, %r11d
	orq	%r13, %r11
	movq	%r11, (%r12)
.L134:
	cmpq	%rdi, %r9
	je	.L136
	movq	(%rdi), %rax
	movq	%rax, (%r9)
.L136:
	testb	$1, 8(%rdi)
	jne	.L137
	testl	%r10d, %r10d
	movq	%r13, %rcx
	je	.L217
.L138:
	testq	%rcx, %rcx
	je	.L164
	movq	8(%rcx), %rax
	testb	$1, %al
	jne	.L165
.L164:
	movslq	%r10d, %r14
	movq	-8(%r8,%r14,8), %r12
	movq	(%r12), %r11
	movq	%r11, %rax
	andq	$-2, %rax
	movq	8(%rax), %rdx
	movq	%rax, %r13
	andq	$-2, %rdx
	cmpq	%rcx, %rdx
	movq	%rdx, %rsi
	je	.L218
	movq	8(%rdx), %rcx
	movq	16(%rdx), %r9
	movq	%rcx, %r15
	andq	$-2, %r15
	testb	$1, %cl
	je	.L152
	movq	%r9, %rcx
	andl	$1, %r11d
	movq	%r15, 8(%rdx)
	orq	$1, %rcx
	orq	%rsi, %r11
	addl	$1, %r10d
	movq	%rcx, 8(%rax)
	movq	%rax, 16(%rdx)
	movq	%r11, (%r12)
	leaq	16(%rsi), %r12
	movq	%r9, %rsi
	andq	$-2, %rsi
	movq	8(%rsi), %rcx
	movq	16(%rsi), %r9
	movq	%r12, (%r8,%r14,8)
	movq	%rcx, %r15
	andq	$-2, %r15
.L152:
	testq	%r9, %r9
	je	.L153
	movq	8(%r9), %rdx
	testb	$1, %dl
	je	.L153
	movq	8(%rax), %r8
	andl	$1, %r8d
	testq	%r15, %r15
	jne	.L219
.L157:
	movq	%rdx, %rcx
	orq	$1, %rdx
	andq	$-2, %rcx
	testq	%r8, %r8
	cmove	%rcx, %rdx
	movq	%rdx, 8(%r9)
	movq	8(%rax), %rdx
	andl	$1, %edx
	orq	16(%r9), %rdx
	movq	%rdx, 8(%rax)
	movq	8(%r9), %rdx
	movq	%rdx, %rcx
	andl	$1, %edx
	andq	$-2, %rcx
	movq	%rcx, 16(%rsi)
	orq	%rdx, %rsi
	movq	(%r12), %rdx
	movq	%rsi, 8(%r9)
	movq	%rax, 16(%r9)
	andl	$1, %edx
	orq	%rdx, %r9
	movq	%r9, (%r12)
	andq	$-2, 8(%rax)
.L137:
	call	free@PLT
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L153:
	testq	%r15, %r15
	je	.L155
	testb	$1, 8(%r15)
	je	.L155
	movq	8(%rax), %r8
	andl	$1, %r8d
.L158:
	orq	$1, %rcx
	testq	%r8, %r8
	cmovne	%rcx, %r15
	movq	%r15, 8(%rsi)
	andq	$-2, 8(%rax)
	movq	8(%rsi), %rdx
	andq	$-2, %rdx
	andq	$-2, 8(%rdx)
	movq	8(%rax), %rdx
	andl	$1, %edx
	orq	%rdx, %r9
	movq	%r9, 8(%rax)
	movq	%rax, 16(%rsi)
	movq	(%r12), %rax
	andl	$1, %eax
	orq	%rax, %rsi
	movq	%rsi, (%r12)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L155:
	orq	$1, %rcx
	movq	%rcx, 8(%rsi)
.L145:
	subl	$1, %r10d
	movq	%rax, %rcx
	jne	.L138
.L163:
	movq	8(%r13), %rax
	movq	%r13, %rcx
.L165:
	andq	$-2, %rax
	movq	%rax, 8(%rcx)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L218:
	movq	16(%rax), %rdx
	movq	8(%rdx), %rcx
	movq	%rcx, %rsi
	andq	$-2, %rsi
	testb	$1, %cl
	je	.L140
	movq	%rsi, 8(%rdx)
	orq	$1, 8(%rax)
	andl	$1, %r11d
	movq	8(%rdx), %rcx
	orq	%rdx, %r11
	addl	$1, %r10d
	movq	%rcx, %rsi
	andl	$1, %ecx
	andq	$-2, %rsi
	orq	%rax, %rcx
	movq	%rsi, 16(%rax)
	movq	%rcx, 8(%rdx)
	movq	8(%rsi), %rcx
	movq	%r11, (%r12)
	leaq	8(%rdx), %r12
	movq	%rsi, %rdx
	movq	%r12, (%r8,%r14,8)
	movq	%rcx, %rsi
	andq	$-2, %rsi
.L140:
	testq	%rsi, %rsi
	je	.L141
	movq	8(%rsi), %r9
	testb	$1, %r9b
	je	.L141
	movq	16(%rdx), %r11
	movq	8(%rax), %r8
	andl	$1, %r8d
	testq	%r11, %r11
	jne	.L220
.L146:
	testq	%r8, %r8
	je	.L148
	orq	$1, %r9
	movq	%r9, 8(%rsi)
.L149:
	movq	8(%rdx), %rcx
	andq	$-2, %r9
	movq	%r9, 16(%rax)
	andl	$1, %ecx
	orq	16(%rsi), %rcx
	movq	%rcx, 8(%rdx)
	movq	%rdx, 16(%rsi)
	movq	8(%rsi), %rdx
	andl	$1, %edx
	orq	%rax, %rdx
	movq	%rdx, 8(%rsi)
	movq	(%r12), %rdx
	andl	$1, %edx
	orq	%rdx, %rsi
	movq	%rsi, (%r12)
	andq	$-2, 8(%rax)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L141:
	movq	16(%rdx), %r9
	testq	%r9, %r9
	je	.L143
	testb	$1, 8(%r9)
	movq	%r9, %r11
	je	.L143
	movq	8(%rax), %r8
	andl	$1, %r8d
.L147:
	orq	$1, %rcx
	testq	%r8, %r8
	cmovne	%rcx, %rsi
	movq	%rsi, 8(%rdx)
	andq	$-2, 8(%rax)
	andq	$-2, 8(%r11)
	movq	8(%rdx), %rcx
	movq	%rcx, %rsi
	andl	$1, %ecx
	andq	$-2, %rsi
	movq	%rsi, 16(%rax)
	orq	%rcx, %rax
	movq	%rax, 8(%rdx)
	movq	(%r12), %rax
	andl	$1, %eax
	orq	%rax, %rdx
	movq	%rdx, (%r12)
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	orq	$1, %rcx
	movq	%rcx, 8(%rdx)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L133:
	movslq	%r10d, %rax
	movq	-8(%r8,%rax,8), %rax
	movq	(%rax), %rax
	andq	$-2, %rax
	cmpq	16(%rax), %rdi
	je	.L221
	movq	8(%rax), %rdx
	andl	$1, %edx
	orq	%r13, %rdx
	movq	%rdx, 8(%rax)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L130:
	movq	16(%rdi), %r13
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L221:
	movq	%r13, 16(%rax)
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L148:
	andq	$-2, %r9
	movq	%r9, 8(%rsi)
	jmp	.L149
.L219:
	testb	$1, 8(%r15)
	je	.L157
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L217:
	testq	%r13, %r13
	je	.L137
	jmp	.L163
.L220:
	testb	$1, 8(%r11)
	je	.L146
	jmp	.L147
	.size	__tdelete, .-__tdelete
	.weak	tdelete
	.set	tdelete,__tdelete
	.p2align 4,,15
	.globl	__twalk
	.hidden	__twalk
	.type	__twalk, @function
__twalk:
	testq	%rdi, %rdi
	je	.L222
	testq	%rsi, %rsi
	je	.L222
	xorl	%edx, %edx
	jmp	trecurse
	.p2align 4,,10
	.p2align 3
.L222:
	rep ret
	.size	__twalk, .-__twalk
	.weak	twalk
	.set	twalk,__twalk
	.p2align 4,,15
	.globl	__twalk_r
	.hidden	__twalk_r
	.type	__twalk_r, @function
__twalk_r:
	testq	%rdi, %rdi
	je	.L230
	testq	%rsi, %rsi
	je	.L230
	jmp	trecurse_r
	.p2align 4,,10
	.p2align 3
.L230:
	rep ret
	.size	__twalk_r, .-__twalk_r
	.weak	twalk_r
	.set	twalk_r,__twalk_r
	.p2align 4,,15
	.globl	__tdestroy
	.hidden	__tdestroy
	.type	__tdestroy, @function
__tdestroy:
	testq	%rdi, %rdi
	je	.L238
	jmp	tdestroy_recurse
	.p2align 4,,10
	.p2align 3
.L238:
	rep ret
	.size	__tdestroy, .-__tdestroy
	.weak	tdestroy
	.set	tdestroy,__tdestroy
