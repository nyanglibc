	.text
	.p2align 4,,15
	.globl	__gethostname
	.hidden	__gethostname
	.type	__gethostname, @function
__gethostname:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	subq	$400, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__uname
	testl	%eax, %eax
	jne	.L3
	addq	$65, %rbx
	movl	%eax, %r14d
	movq	%rbx, %rdi
	call	strlen
	leaq	1(%rax), %rbp
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	cmpq	%r12, %rbp
	cmovbe	%rbp, %rdx
	call	memcpy@PLT
	cmpq	%r12, %rbp
	ja	.L6
.L1:
	addq	$400, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %r14d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r14d
	movl	$36, %fs:(%rax)
	jmp	.L1
	.size	__gethostname, .-__gethostname
	.weak	gethostname
	.set	gethostname,__gethostname
	.hidden	strlen
	.hidden	__uname
