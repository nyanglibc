	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___lxstat64
	.type	___lxstat64, @function
___lxstat64:
	cmpl	$1, %edi
	movq	%rsi, %rax
	ja	.L2
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movl	$6, %eax
#APP
# 50 "../sysdeps/unix/sysv/linux/lxstat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	___lxstat64, .-___lxstat64
	.globl	__lxstat
	.set	__lxstat,___lxstat64
	.globl	__lxstat64
	.set	__lxstat64,___lxstat64
