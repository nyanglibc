	.text
	.p2align 4,,15
	.globl	ptrace
	.type	ptrace, @function
ptrace:
	leaq	8(%rsp), %rax
	leal	-1(%rdi), %r8d
	movq	%rsi, -40(%rsp)
	movq	%rdx, -32(%rsp)
	movq	%rcx, -24(%rsp)
	leaq	-80(%rsp), %r10
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	cmpl	$3, %r8d
	movl	8(%rax), %esi
	movq	16(%rax), %rdx
	movq	%rax, -56(%rsp)
	cmovnb	24(%rax), %r10
	movl	$101, %eax
	movl	$24, -72(%rsp)
#APP
# 45 "../sysdeps/unix/sysv/linux/ptrace.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L16
	testq	%rax, %rax
	js	.L1
	cmpl	$2, %r8d
	jbe	.L17
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	-80(%rsp), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	ptrace, .-ptrace
