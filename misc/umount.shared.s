	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__umount
	.type	__umount, @function
__umount:
	xorl	%esi, %esi
	jmp	__GI___umount2
	.size	__umount, .-__umount
	.weak	umount
	.set	umount,__umount
