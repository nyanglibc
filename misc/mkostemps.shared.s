	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	mkostemps
	.type	mkostemps, @function
mkostemps:
	testl	%esi, %esi
	js	.L5
	xorl	%ecx, %ecx
	jmp	__gen_tempname
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	mkostemps, .-mkostemps
	.weak	mkostemps64
	.set	mkostemps64,mkostemps
	.hidden	__gen_tempname
