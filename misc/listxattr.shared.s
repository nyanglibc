.text
 .globl listxattr
 .type listxattr,@function
 .align 1<<4
 listxattr: 
 
 movl $194, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size listxattr,.-listxattr
.globl __GI_listxattr 
 .set __GI_listxattr,listxattr
