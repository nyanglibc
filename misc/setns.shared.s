.text
 .globl setns
 .type setns,@function
 .align 1<<4
 setns: 
 
 movl $308, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size setns,.-setns
.globl __GI_setns 
 .set __GI_setns,setns
