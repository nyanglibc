.text
 .globl fgetxattr
 .type fgetxattr,@function
 .align 1<<4
 fgetxattr: 
 
 movq %rcx, %r10
 movl $193, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size fgetxattr,.-fgetxattr
