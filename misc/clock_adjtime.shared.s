	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___clock_adjtime
	.hidden	__GI___clock_adjtime
	.type	__GI___clock_adjtime, @function
__GI___clock_adjtime:
	movl	$305, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/clock_adjtime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___clock_adjtime, .-__GI___clock_adjtime
	.globl	__clock_adjtime
	.set	__clock_adjtime,__GI___clock_adjtime
	.globl	clock_adjtime
	.set	clock_adjtime,__clock_adjtime
