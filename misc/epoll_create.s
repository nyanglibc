.text
 .globl epoll_create
 .type epoll_create,@function
 .align 1<<4
 epoll_create: 
 
 movl $213, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size epoll_create,.-epoll_create
