	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_epoll_pwait
	.hidden	__GI_epoll_pwait
	.type	__GI_epoll_pwait, @function
__GI_epoll_pwait:
	movl	%ecx, %r10d
#APP
# 40 "../sysdeps/unix/sysv/linux/epoll_pwait.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$8, %r9d
	movl	$281, %eax
#APP
# 40 "../sysdeps/unix/sysv/linux/epoll_pwait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r14d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r13d
	movq	%rsi, %r12
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	movl	$8, %r9d
	movl	%eax, %ebp
	movq	%r15, %r8
	movl	%r14d, %r10d
	movl	%r13d, %edx
	movq	%r12, %rsi
	movl	%ebx, %edi
	movl	$281, %eax
#APP
# 40 "../sysdeps/unix/sysv/linux/epoll_pwait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%ebp, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__GI_epoll_pwait, .-__GI_epoll_pwait
	.globl	epoll_pwait
	.set	epoll_pwait,__GI_epoll_pwait
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
