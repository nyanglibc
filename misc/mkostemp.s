	.text
	.p2align 4,,15
	.globl	mkostemp
	.type	mkostemp, @function
mkostemp:
	movl	%esi, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	__gen_tempname
	.size	mkostemp, .-mkostemp
	.weak	mkostemp64
	.set	mkostemp64,mkostemp
	.hidden	__gen_tempname
