.text
 .globl __compat_get_kernel_syms
 .type __compat_get_kernel_syms,@function
 .align 1<<4
 __compat_get_kernel_syms: 
 
 movl $177, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __compat_get_kernel_syms,.-__compat_get_kernel_syms
.globl __GI___compat_get_kernel_syms 
 .set __GI___compat_get_kernel_syms,__compat_get_kernel_syms
.symver __compat_get_kernel_syms, get_kernel_syms@GLIBC_2.2.5
