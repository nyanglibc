	.text
	.p2align 4,,15
	.globl	mkstemps
	.type	mkstemps, @function
mkstemps:
	testl	%esi, %esi
	js	.L5
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	__gen_tempname
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	mkstemps, .-mkstemps
	.weak	mkstemps64
	.set	mkstemps64,mkstemps
	.hidden	__gen_tempname
