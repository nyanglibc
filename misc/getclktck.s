	.text
	.p2align 4,,15
	.globl	__getclktck
	.hidden	__getclktck
	.type	__getclktck, @function
__getclktck:
	movl	_dl_clktck(%rip), %eax
	movl	$100, %edx
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.size	__getclktck, .-__getclktck
