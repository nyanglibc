	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	": %s"
	.text
	.p2align 4,,15
	.type	print_errno_message, @function
print_errno_message:
.LFB74:
	
	subq	$1032, %rsp
	movl	$1024, %edx
	movq	%rsp, %rsi
	call	__strerror_r
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	addq	$1032, %rsp
	ret
.LFE74:
	.size	print_errno_message, .-print_errno_message
	.section	.rodata.str4.8,"aMS",@progbits,4
	.align 8
.LC1:
	.string	"o"
	.string	""
	.string	""
	.string	"u"
	.string	""
	.string	""
	.string	"t"
	.string	""
	.string	""
	.string	" "
	.string	""
	.string	""
	.string	"o"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	" "
	.string	""
	.string	""
	.string	"m"
	.string	""
	.string	""
	.string	"e"
	.string	""
	.string	""
	.string	"m"
	.string	""
	.string	""
	.string	"o"
	.string	""
	.string	""
	.string	"r"
	.string	""
	.string	""
	.string	"y"
	.string	""
	.string	""
	.string	"\n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC2:
	.string	"\n"
	.text
	.p2align 4,,15
	.type	error_tail, @function
error_tail:
.LFB75:
	
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	%esi, %ebx
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	subq	$8, %rsp
	movq	stderr(%rip), %rdi
	movl	%r8d, %ecx
	call	__vfxprintf
	testl	%eax, %eax
	js	.L12
.L5:
	addl	$1, error_message_count(%rip)
	testl	%ebx, %ebx
	jne	.L13
.L6:
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	stderr(%rip), %rdi
	call	_IO_fflush
	testl	%ebp, %ebp
	jne	.L14
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$12, %fs:(%rax)
	jne	.L5
	movq	stderr(%rip), %rsi
	movl	192(%rsi), %eax
	testl	%eax, %eax
	jle	.L5
	leaq	.LC1(%rip), %rdi
	call	fputws_unlocked
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%ebx, %edi
	call	print_errno_message
	jmp	.L6
.L14:
	movl	%ebp, %edi
	call	exit
.LFE75:
	.size	error_tail, .-error_tail
	.section	.rodata.str1.1
.LC3:
	.string	"%s: "
	.text
	.p2align 4,,15
	.globl	__error_internal
	.type	__error_internal, @function
__error_internal:
.LFB76:
	
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	%esi, %r12d
	movq	%rcx, %r14
	subq	$24, %rsp
	movq	__pthread_setcancelstate@GOTPCREL(%rip), %rbx
	movl	$0, 12(%rsp)
	testq	%rbx, %rbx
	je	.L16
	leaq	12(%rsp), %rsi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
.L16:
	movq	stdout(%rip), %rdi
	call	_IO_fflush
	movq	error_print_progname(%rip), %rax
	testq	%rax, %rax
	je	.L17
	call	*%rax
.L18:
	movl	%r15d, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movl	%r12d, %esi
	movl	%ebp, %edi
	call	error_tail
	testq	%rbx, %rbx
	je	.L15
	movl	12(%rsp), %edi
	xorl	%esi, %esi
	call	__pthread_setcancelstate@PLT
.L15:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	program_invocation_name(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L18
.LFE76:
	.size	__error_internal, .-__error_internal
	.p2align 4,,15
	.globl	__error
	.type	__error, @function
__error:
.LFB77:
	
	subq	$216, %rsp
	testb	%al, %al
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L29
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L29:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$24, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__error_internal
	addq	$216, %rsp
	ret
.LFE77:
	.size	__error, .-__error
	.weak	error
	.set	error,__error
	.section	.rodata.str1.1
.LC4:
	.string	"%s:%d: "
.LC5:
	.string	" "
.LC6:
	.string	"%s:"
	.text
	.p2align 4,,15
	.globl	__error_at_line_internal
	.type	__error_at_line_internal, @function
__error_at_line_internal:
.LFB78:
	
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %r12d
	movq	%rdx, %rbx
	movl	%ecx, %ebp
	movq	%r8, %r14
	subq	$24, %rsp
	movl	error_one_per_line(%rip), %eax
	testl	%eax, %eax
	je	.L32
	cmpl	%ecx, old_line_number.11560(%rip)
	je	.L57
.L33:
	movq	%rbx, old_file_name.11559(%rip)
	movl	%ebp, old_line_number.11560(%rip)
.L32:
	cmpq	$0, __pthread_setcancelstate@GOTPCREL(%rip)
	movl	$0, 12(%rsp)
	je	.L35
	leaq	12(%rsp), %rsi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
.L35:
	movq	stdout(%rip), %rdi
	call	_IO_fflush
	movq	error_print_progname(%rip), %rax
	testq	%rax, %rax
	je	.L36
	call	*%rax
.L37:
	leaq	.LC5(%rip), %rax
	leaq	.LC4(%rip), %rsi
	testq	%rbx, %rbx
	movl	%ebp, %ecx
	movq	%rbx, %rdx
	cmove	%rax, %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movl	80(%rsp), %r8d
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movl	%r12d, %edi
	call	error_tail
	cmpq	$0, __pthread_setcancelstate@GOTPCREL(%rip)
	je	.L31
	movl	12(%rsp), %edi
	xorl	%esi, %esi
	call	__pthread_setcancelstate@PLT
.L31:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movq	old_file_name.11559(%rip), %rdi
	cmpq	%rdx, %rdi
	je	.L31
	testq	%rdx, %rdx
	je	.L33
	testq	%rdi, %rdi
	je	.L33
	movq	%rdx, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L31
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L36:
	movq	program_invocation_name(%rip), %rdx
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L37
.LFE78:
	.size	__error_at_line_internal, .-__error_at_line_internal
	.p2align 4,,15
	.globl	__error_at_line
	.type	__error_at_line, @function
__error_at_line:
.LFB79:
	
	subq	$216, %rsp
	testb	%al, %al
	movq	%r9, 72(%rsp)
	je	.L60
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L60:
	leaq	224(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$40, 8(%rsp)
	movl	$48, 12(%rsp)
	subq	$8, %rsp
	movq	%rax, 32(%rsp)
	pushq	$0
	leaq	24(%rsp), %r9
	call	__error_at_line_internal
	addq	$232, %rsp
	ret
.LFE79:
	.size	__error_at_line, .-__error_at_line
	.weak	error_at_line
	.set	error_at_line,__error_at_line
	.local	old_file_name.11559
	.comm	old_file_name.11559,8,8
	.local	old_line_number.11560
	.comm	old_line_number.11560,4,4
	.comm	error_one_per_line,4,4
	.comm	error_message_count,4,4
	.comm	error_print_progname,8,8
	.weak	__pthread_setcancelstate
	.hidden	strcmp
	.hidden	exit
	.hidden	fputws_unlocked
	.hidden	_IO_fflush
	.hidden	__vfxprintf
	.hidden	__fxprintf
	.hidden	__strerror_r
