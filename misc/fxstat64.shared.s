	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___fxstat64
	.type	___fxstat64, @function
___fxstat64:
	cmpl	$1, %edi
	movl	%esi, %eax
	ja	.L2
	movl	%eax, %edi
	movq	%rdx, %rsi
	movl	$5, %eax
#APP
# 51 "../sysdeps/unix/sysv/linux/fxstat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	___fxstat64, .-___fxstat64
	.globl	__fxstat
	.set	__fxstat,___fxstat64
	.globl	__fxstat64
	.set	__fxstat64,___fxstat64
