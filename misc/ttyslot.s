	.text
	.p2align 4,,15
	.globl	ttyslot
	.type	ttyslot, @function
ttyslot:
	pushq	%rbp
	movl	$72, %edi
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$8, %rsp
	call	__sysconf
	addq	$1, %rax
	movq	%rax, %r12
	movl	$32, %eax
	cmove	%rax, %r12
	xorl	%ebx, %ebx
	leaq	30(%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r13
	call	__setttyent
	andq	$-16, %r13
.L10:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movl	%ebx, %edi
	call	__ttyname_r
	testl	%eax, %eax
	je	.L16
	addl	$1, %ebx
	cmpl	$3, %ebx
	jne	.L10
.L9:
	xorl	%ebx, %ebx
	call	__endttyent
.L1:
	leaq	-24(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r13, %rdi
	movl	$47, %esi
	movl	$1, %ebx
	call	strrchr
	leaq	1(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %r13
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rax), %rdi
	movq	%r13, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L17
	addl	$1, %ebx
.L5:
	call	__getttyent
	testq	%rax, %rax
	jne	.L8
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L17:
	call	__endttyent
	jmp	.L1
	.size	ttyslot, .-ttyslot
	.hidden	__getttyent
	.hidden	strcmp
	.hidden	strrchr
	.hidden	__endttyent
	.hidden	__ttyname_r
	.hidden	__setttyent
	.hidden	__sysconf
