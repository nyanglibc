	.text
	.p2align 4,,15
	.globl	__timerfd_gettime
	.type	__timerfd_gettime, @function
__timerfd_gettime:
	movl	$287, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/timerfd_gettime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__timerfd_gettime, .-__timerfd_gettime
	.globl	timerfd_gettime
	.set	timerfd_gettime,__timerfd_gettime
