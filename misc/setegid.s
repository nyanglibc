	.text
	.p2align 4,,15
	.globl	setegid
	.hidden	setegid
	.type	setegid, @function
setegid:
	cmpl	$-1, %edi
	je	.L12
	cmpq	$0, __nptl_setxid@GOTPCREL(%rip)
	jne	.L13
	movl	$-1, %edx
	movl	%edi, %esi
	movl	$119, %eax
	movl	%edx, %edi
#APP
# 34 "../sysdeps/unix/sysv/linux/setegid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	subq	$56, %rsp
	movq	$-1, %rax
	movl	%edi, %ecx
	movq	%rsp, %rdi
	movl	$119, (%rsp)
	movq	%rax, 8(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rax, 24(%rsp)
	call	__nptl_setxid@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	%edi, %eax
	ret
	.size	setegid, .-setegid
	.weak	__nptl_setxid
	.hidden	__nptl_setxid
