.text
 .globl eventfd
 .type eventfd,@function
 .align 1<<4
 eventfd: 
 
 movl $290, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size eventfd,.-eventfd
