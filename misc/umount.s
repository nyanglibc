	.text
	.p2align 4,,15
	.globl	__umount
	.type	__umount, @function
__umount:
	xorl	%esi, %esi
	jmp	__umount2
	.size	__umount, .-__umount
	.weak	umount
	.set	umount,__umount
	.hidden	__umount2
