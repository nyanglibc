.text
 .globl __personality
 .type __personality,@function
 .align 1<<4
 __personality: 
 
 movl $135, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __personality,.-__personality
.weak personality 
 personality = __personality
