	.text
	.p2align 4,,15
	.globl	mlock2
	.type	mlock2, @function
mlock2:
	testl	%edx, %edx
	jne	.L2
	movl	$149, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/mlock2.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$325, %ecx
	movl	%ecx, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/mlock2.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L10
	testl	%eax, %eax
	je	.L1
	movq	__libc_errno@gottpoff(%rip), %rcx
	movl	%fs:(%rcx), %edx
.L7:
	cmpl	$38, %edx
	jne	.L1
	movl	$22, %fs:(%rcx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rcx
	negl	%edx
	movl	$-1, %eax
	movl	%edx, %fs:(%rcx)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	mlock2, .-mlock2
