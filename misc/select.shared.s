	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___select
	.hidden	__GI___select
	.type	__GI___select, @function
__GI___select:
	pushq	%r14
	pushq	%r13
	movq	%r8, %rax
	pushq	%r12
	pushq	%rbp
	xorl	%r8d, %r8d
	pushq	%rbx
	movq	%rcx, %r10
	subq	$32, %rsp
	testq	%rax, %rax
	je	.L2
	imulq	$1000, 8(%rax), %rcx
	movq	(%rax), %rax
	leaq	16(%rsp), %r8
	movq	%rax, 16(%rsp)
	movq	%rcx, 24(%rsp)
.L2:
#APP
# 49 "../sysdeps/unix/sysv/linux/select.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	xorl	%r9d, %r9d
	movl	$270, %eax
#APP
# 49 "../sysdeps/unix/sysv/linux/select.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L13
.L1:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r10, %r13
	movq	%rdx, %r12
	movq	%rsi, %r14
	movl	%edi, %ebp
	movq	%r8, 8(%rsp)
	call	__libc_enable_asynccancel
	xorl	%r9d, %r9d
	movl	%eax, %ebx
	movq	8(%rsp), %r8
	movq	%r13, %r10
	movq	%r12, %rdx
	movq	%r14, %rsi
	movl	%ebp, %edi
	movl	$270, %eax
#APP
# 49 "../sysdeps/unix/sysv/linux/select.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
.L7:
	movl	%ebx, %edi
	movl	%eax, 8(%rsp)
	call	__libc_disable_asynccancel
	movl	8(%rsp), %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L7
	.size	__GI___select, .-__GI___select
	.globl	__select
	.set	__select,__GI___select
	.weak	__libc_select
	.set	__libc_select,__select
	.weak	select
	.set	select,__select
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
