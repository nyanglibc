	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__fcvt
	.type	__fcvt, @function
__fcvt:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movapd	%xmm0, %xmm1
	movl	%edi, %ebx
	movq	%rsi, %rbp
	subq	$16, %rsp
	movq	fcvt_bufptr(%rip), %rcx
	testq	%rcx, %rcx
	je	.L11
.L2:
	movapd	%xmm1, %xmm0
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$328, %r8d
	call	__fcvt_r
	movq	fcvt_bufptr(%rip), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	fcvt_buffer(%rip), %rcx
	movl	$20, %r8d
	movsd	%xmm0, 8(%rsp)
	call	__fcvt_r
	cmpl	$-1, %eax
	movsd	8(%rsp), %xmm1
	je	.L3
.L5:
	addq	$16, %rsp
	leaq	fcvt_buffer(%rip), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$328, %edi
	movsd	%xmm1, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, fcvt_bufptr(%rip)
	movsd	8(%rsp), %xmm1
	jne	.L2
	jmp	.L5
	.size	__fcvt, .-__fcvt
	.globl	fcvt
	.set	fcvt,__fcvt
	.p2align 4,,15
	.globl	__ecvt
	.type	__ecvt, @function
__ecvt:
	leaq	ecvt_buffer(%rip), %rcx
	subq	$8, %rsp
	movl	$20, %r8d
	call	__ecvt_r
	leaq	ecvt_buffer(%rip), %rax
	addq	$8, %rsp
	ret
	.size	__ecvt, .-__ecvt
	.globl	ecvt
	.set	ecvt,__ecvt
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%.*g"
	.text
	.p2align 4,,15
	.globl	__gcvt
	.type	__gcvt, @function
__gcvt:
	pushq	%rbx
	cmpl	$17, %edi
	movq	%rsi, %rbx
	movl	$17, %edx
	leaq	.LC0(%rip), %rsi
	movl	$1, %eax
	cmovle	%edi, %edx
	movq	%rbx, %rdi
	call	sprintf
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	__gcvt, .-__gcvt
	.globl	gcvt
	.set	gcvt,__gcvt
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	fcvt_bufptr, @object
	.size	fcvt_bufptr, 8
fcvt_bufptr:
	.zero	8
	.local	ecvt_buffer
	.comm	ecvt_buffer,20,16
	.local	fcvt_buffer
	.comm	fcvt_buffer,20,16
	.hidden	sprintf
	.hidden	__ecvt_r
	.hidden	__fcvt_r
