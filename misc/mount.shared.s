.text
 .globl __mount
 .type __mount,@function
 .align 1<<4
 __mount: 
 
 movq %rcx, %r10
 movl $165, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __mount,.-__mount
.globl __GI___mount 
 .set __GI___mount,__mount
.weak mount 
 mount = __mount
.globl __GI_mount 
 .set __GI_mount,mount
