	.text
	.p2align 4,,15
	.globl	__libc_allocate_once_slow
	.hidden	__libc_allocate_once_slow
	.type	__libc_allocate_once_slow, @function
__libc_allocate_once_slow:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rdi, %rbx
	movq	%rcx, %rdi
	subq	$8, %rsp
	call	*%rsi
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L1
	xorl	%edx, %edx
.L3:
	movq	%rdx, %rax
	lock cmpxchgq	%rsi, (%rbx)
	jne	.L12
.L1:
	addq	$8, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L12:
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.L3
	testq	%r12, %r12
	je	.L13
	movq	%rbp, %rdi
	call	*%r12
	movq	%r13, %rsi
	jmp	.L1
.L13:
	movq	%rsi, %rdi
	call	free@PLT
	movq	%r13, %rsi
	jmp	.L1
	.size	__libc_allocate_once_slow, .-__libc_allocate_once_slow
