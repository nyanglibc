	.text
	.p2align 4,,15
	.globl	process_vm_readv
	.type	process_vm_readv, @function
process_vm_readv:
	movq	%rcx, %r10
	movl	$310, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/process_vm_readv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	process_vm_readv, .-process_vm_readv
