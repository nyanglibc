	.text
	.p2align 4,,15
	.globl	___adjtimex
	.type	___adjtimex, @function
___adjtimex:
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	__clock_adjtime
	.size	___adjtimex, .-___adjtimex
	.weak	ntp_adjtime
	.set	ntp_adjtime,___adjtimex
	.weak	adjtimex
	.set	adjtimex,___adjtimex
	.globl	__adjtimex
	.hidden	__adjtimex
	.set	__adjtimex,___adjtimex
	.hidden	__clock_adjtime
