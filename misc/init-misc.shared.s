	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__init_misc
	.hidden	__init_misc
	.type	__init_misc, @function
__init_misc:
	testq	%rsi, %rsi
	je	.L12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rsi), %rbp
	testq	%rbp, %rbp
	je	.L1
	movq	%rsi, %rbx
	movq	%rbp, %rdi
	movl	$47, %esi
	call	__GI_strrchr
	testq	%rax, %rax
	je	.L16
	movq	__progname@GOTPCREL(%rip), %rdx
	addq	$1, %rax
	movq	%rax, (%rdx)
.L4:
	movq	(%rbx), %rdx
	movq	__progname_full@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	rep ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	__progname@GOTPCREL(%rip), %rax
	movq	%rbp, (%rax)
	jmp	.L4
	.size	__init_misc, .-__init_misc
	.globl	__progname
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__progname, @object
	.size	__progname, 8
__progname:
	.quad	.LC0
	.weak	program_invocation_short_name
	.set	program_invocation_short_name,__progname
	.globl	__progname_full
	.align 8
	.type	__progname_full, @object
	.size	__progname_full, 8
__progname_full:
	.quad	.LC0
	.weak	program_invocation_name
	.set	program_invocation_name,__progname_full
