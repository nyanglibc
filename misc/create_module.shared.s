.text
 .globl __compat_create_module
 .type __compat_create_module,@function
 .align 1<<4
 __compat_create_module: 
 
 movl $174, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __compat_create_module,.-__compat_create_module
.globl __GI___compat_create_module 
 .set __GI___compat_create_module,__compat_create_module
.symver __compat_create_module, create_module@GLIBC_2.2.5
