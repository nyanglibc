	.text
	.p2align 4,,15
	.globl	__umount2
	.hidden	__umount2
	.type	__umount2, @function
__umount2:
	movl	$166, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/umount2.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__umount2, .-__umount2
	.weak	umount2
	.set	umount2,__umount2
