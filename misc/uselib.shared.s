.text
 .globl __compat_uselib
 .type __compat_uselib,@function
 .align 1<<4
 __compat_uselib: 
 
 movl $134, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __compat_uselib,.-__compat_uselib
.globl __GI___compat_uselib 
 .set __GI___compat_uselib,__compat_uselib
.symver __compat_uselib, uselib@GLIBC_2.2.5
