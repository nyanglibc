.text
 .globl __mprotect
 .type __mprotect,@function
 .align 1<<4
 __mprotect: 
 
 movl $10, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __mprotect,.-__mprotect
.weak mprotect 
 mprotect = __mprotect
