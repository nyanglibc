	.text
	.p2align 4,,15
	.globl	pwritev64
	.hidden	pwritev64
	.type	pwritev64, @function
pwritev64:
	movq	%rcx, %r10
#APP
# 26 "../sysdeps/unix/sysv/linux/pwritev64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	xorl	%r8d, %r8d
	movl	$296, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/pwritev64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	xorl	%r8d, %r8d
	movl	%eax, %r9d
	movq	%r12, %r10
	movl	%r13d, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$296, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/pwritev64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r9d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	pwritev64, .-pwritev64
	.globl	pwritev
	.hidden	pwritev
	.set	pwritev,pwritev64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
