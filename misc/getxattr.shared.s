.text
 .globl getxattr
 .type getxattr,@function
 .align 1<<4
 getxattr: 
 
 movq %rcx, %r10
 movl $191, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size getxattr,.-getxattr
.globl __GI_getxattr 
 .set __GI_getxattr,getxattr
