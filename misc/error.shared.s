	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	": %s"
#NO_APP
	.text
	.p2align 4,,15
	.type	print_errno_message, @function
print_errno_message:
.LFB74:
	
	subq	$1032, %rsp
	movl	$1024, %edx
	movq	%rsp, %rsi
	call	__GI___strerror_r
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	addq	$1032, %rsp
	ret
.LFE74:
	.size	print_errno_message, .-print_errno_message
	.section	.rodata.str4.8,"aMS",@progbits,4
	.align 8
.LC1:
	.string	"o"
	.string	""
	.string	""
	.string	"u"
	.string	""
	.string	""
	.string	"t"
	.string	""
	.string	""
	.string	" "
	.string	""
	.string	""
	.string	"o"
	.string	""
	.string	""
	.string	"f"
	.string	""
	.string	""
	.string	" "
	.string	""
	.string	""
	.string	"m"
	.string	""
	.string	""
	.string	"e"
	.string	""
	.string	""
	.string	"m"
	.string	""
	.string	""
	.string	"o"
	.string	""
	.string	""
	.string	"r"
	.string	""
	.string	""
	.string	"y"
	.string	""
	.string	""
	.string	"\n"
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.string	""
	.section	.rodata.str1.1
.LC2:
	.string	"\n"
	.text
	.p2align 4,,15
	.type	error_tail, @function
error_tail:
.LFB75:
	
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	movq	stderr@GOTPCREL(%rip), %rbx
	movl	%esi, %ebp
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	movl	%r8d, %ecx
	movq	(%rbx), %rdi
	call	__vfxprintf
	testl	%eax, %eax
	js	.L12
.L5:
	movq	error_message_count@GOTPCREL(%rip), %rax
	addl	$1, (%rax)
	testl	%ebp, %ebp
	jne	.L13
.L6:
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	(%rbx), %rdi
	call	__GI__IO_fflush
	testl	%r12d, %r12d
	jne	.L14
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$12, %fs:(%rax)
	jne	.L5
	movq	(%rbx), %rsi
	movl	192(%rsi), %eax
	testl	%eax, %eax
	jle	.L5
	leaq	.LC1(%rip), %rdi
	call	__GI_fputws_unlocked
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%ebp, %edi
	call	print_errno_message
	jmp	.L6
.L14:
	movl	%r12d, %edi
	call	__GI_exit
.LFE75:
	.size	error_tail, .-error_tail
	.section	.rodata.str1.1
.LC3:
	.string	"%s: "
	.text
	.p2align 4,,15
	.globl	__error_internal
	.type	__error_internal, @function
__error_internal:
.LFB76:
	
	pushq	%r14
	pushq	%r13
	movl	%r8d, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movl	%esi, %ebp
	movl	%edi, %ebx
	movq	%rcx, %r13
	subq	$16, %rsp
	movl	__libc_pthread_functions_init(%rip), %edx
	movl	$0, 12(%rsp)
	testl	%edx, %edx
	je	.L16
	movq	104+__libc_pthread_functions(%rip), %rax
	leaq	12(%rsp), %rsi
	movl	$1, %edi
#APP
# 243 "error.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L16:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	call	__GI__IO_fflush
	movq	error_print_progname@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L17
	call	*%rax
.L18:
	movl	%r14d, %r8d
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	%ebp, %esi
	movl	%ebx, %edi
	call	error_tail
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L15
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	12(%rsp), %edi
#APP
# 267 "error.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L15:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	program_invocation_name@GOTPCREL(%rip), %rax
	leaq	.LC3(%rip), %rsi
	xorl	%edi, %edi
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L18
.LFE76:
	.size	__error_internal, .-__error_internal
	.p2align 4,,15
	.globl	__error
	.type	__error, @function
__error:
.LFB77:
	
	subq	$216, %rsp
	testb	%al, %al
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L23
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L23:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rcx
	xorl	%r8d, %r8d
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$24, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__error_internal@PLT
	addq	$216, %rsp
	ret
.LFE77:
	.size	__error, .-__error
	.weak	error
	.set	error,__error
	.section	.rodata.str1.1
.LC4:
	.string	"%s:%d: "
.LC5:
	.string	" "
.LC6:
	.string	"%s:"
	.text
	.p2align 4,,15
	.globl	__error_at_line_internal
	.type	__error_at_line_internal, @function
__error_at_line_internal:
.LFB78:
	
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %ebp
	movl	%edi, %r12d
	movq	%rdx, %rbx
	movq	%r8, %r14
	subq	$24, %rsp
	movq	error_one_per_line@GOTPCREL(%rip), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L26
	cmpl	%ebp, old_line_number.11560(%rip)
	je	.L45
.L27:
	movq	%rbx, old_file_name.11559(%rip)
	movl	%ebp, old_line_number.11560(%rip)
.L26:
	movl	__libc_pthread_functions_init(%rip), %edx
	movl	$0, 12(%rsp)
	testl	%edx, %edx
	je	.L29
	movq	104+__libc_pthread_functions(%rip), %rax
	leaq	12(%rsp), %rsi
	movl	$1, %edi
#APP
# 312 "error.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L29:
	movq	stdout@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	call	__GI__IO_fflush
	movq	error_print_progname@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L30
	call	*%rax
.L31:
	leaq	.LC5(%rip), %rax
	leaq	.LC4(%rip), %rsi
	testq	%rbx, %rbx
	movl	%ebp, %ecx
	movq	%rbx, %rdx
	cmove	%rax, %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movl	80(%rsp), %r8d
	movq	%r15, %rcx
	movq	%r14, %rdx
	movl	%r13d, %esi
	movl	%r12d, %edi
	call	error_tail
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L25
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	12(%rsp), %edi
#APP
# 345 "error.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L25:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	old_file_name.11559(%rip), %rdi
	cmpq	%rdx, %rdi
	je	.L25
	testq	%rdx, %rdx
	je	.L27
	testq	%rdi, %rdi
	je	.L27
	movq	%rdx, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L25
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	movq	program_invocation_name@GOTPCREL(%rip), %rax
	leaq	.LC6(%rip), %rsi
	xorl	%edi, %edi
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L31
.LFE78:
	.size	__error_at_line_internal, .-__error_at_line_internal
	.p2align 4,,15
	.globl	__error_at_line
	.type	__error_at_line, @function
__error_at_line:
.LFB79:
	
	subq	$216, %rsp
	testb	%al, %al
	movq	%r9, 72(%rsp)
	je	.L48
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L48:
	leaq	224(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$40, 8(%rsp)
	movl	$48, 12(%rsp)
	subq	$8, %rsp
	movq	%rax, 32(%rsp)
	pushq	$0
	leaq	24(%rsp), %r9
	call	__error_at_line_internal@PLT
	addq	$232, %rsp
	ret
.LFE79:
	.size	__error_at_line, .-__error_at_line
	.weak	error_at_line
	.set	error_at_line,__error_at_line
	.local	old_file_name.11559
	.comm	old_file_name.11559,8,8
	.local	old_line_number.11560
	.comm	old_line_number.11560,4,4
	.comm	error_one_per_line,4,4
	.comm	error_message_count,4,4
	.comm	error_print_progname,8,8
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__vfxprintf
	.hidden	__fxprintf
