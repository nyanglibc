	.text
	.p2align 4,,15
	.globl	getdomainname
	.hidden	getdomainname
	.type	getdomainname, @function
getdomainname:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$400, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	uname
	testl	%eax, %eax
	js	.L3
	addq	$325, %rbx
	movq	%rbx, %rdi
	call	strlen
	addq	$1, %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	cmpq	%r12, %rax
	movq	%rbp, %rdi
	cmovbe	%rax, %rdx
	call	memcpy@PLT
	xorl	%eax, %eax
.L1:
	addq	$400, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	getdomainname, .-getdomainname
	.hidden	strlen
	.hidden	uname
