.text
 .globl delete_module
 .type delete_module,@function
 .align 1<<4
 delete_module: 
 
 movl $176, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size delete_module,.-delete_module
.globl __GI_delete_module 
 .set __GI_delete_module,delete_module
