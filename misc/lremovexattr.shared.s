.text
 .globl lremovexattr
 .type lremovexattr,@function
 .align 1<<4
 lremovexattr: 
 
 movl $198, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size lremovexattr,.-lremovexattr
.globl __GI_lremovexattr 
 .set __GI_lremovexattr,lremovexattr
