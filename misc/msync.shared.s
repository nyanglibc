	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	msync
	.type	msync, @function
msync:
#APP
# 25 "../sysdeps/unix/sysv/linux/msync.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$26, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/msync.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$26, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/msync.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	msync, .-msync
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
