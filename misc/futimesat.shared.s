	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__futimesat
	.type	__futimesat, @function
__futimesat:
	movq	%rdx, %rax
	subq	$40, %rsp
	xorl	%edx, %edx
	testq	%rax, %rax
	je	.L2
	imulq	$1000, 8(%rax), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	%rdx, 8(%rsp)
	imulq	$1000, 24(%rax), %rdx
	movq	16(%rax), %rax
	movq	%rax, 16(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%rsp, %rdx
.L2:
	xorl	%ecx, %ecx
	call	__GI___utimensat64_helper
	addq	$40, %rsp
	ret
	.size	__futimesat, .-__futimesat
	.weak	futimesat
	.set	futimesat,__futimesat
