	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/getpagesize.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"GLRO(dl_pagesize) != 0"
	.text
	.p2align 4,,15
	.globl	__getpagesize
	.hidden	__getpagesize
	.type	__getpagesize, @function
__getpagesize:
	movq	_dl_pagesize(%rip), %rax
	testq	%rax, %rax
	je	.L7
	rep ret
.L7:
	leaq	__PRETTY_FUNCTION__.10040(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$28, %edx
	call	__assert_fail
	.size	__getpagesize, .-__getpagesize
	.weak	getpagesize
	.set	getpagesize,__getpagesize
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10040, @object
	.size	__PRETTY_FUNCTION__.10040, 14
__PRETTY_FUNCTION__.10040:
	.string	"__getpagesize"
	.hidden	__assert_fail
