.text
 .globl timerfd_create
 .type timerfd_create,@function
 .align 1<<4
 timerfd_create: 
 
 movl $283, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size timerfd_create,.-timerfd_create
.globl __GI_timerfd_create 
 .set __GI_timerfd_create,timerfd_create
