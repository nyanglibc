.text
 .globl acct
 .type acct,@function
 .align 1<<4
 acct:

 
 movl $163, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size acct,.-acct
.globl __GI_acct 
 .set __GI_acct,acct
