.text
 .globl __madvise
 .type __madvise,@function
 .align 1<<4
 __madvise: 
 
 movl $28, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __madvise,.-__madvise
.globl __GI___madvise 
 .set __GI___madvise,__madvise
.weak madvise 
 madvise = __madvise
.globl __GI_madvise 
 .set __GI_madvise,madvise
