	.text
	.p2align 4,,15
	.globl	__clock_adjtime
	.hidden	__clock_adjtime
	.type	__clock_adjtime, @function
__clock_adjtime:
	movl	$305, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/clock_adjtime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__clock_adjtime, .-__clock_adjtime
	.globl	clock_adjtime
	.set	clock_adjtime,__clock_adjtime
