	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	pkey_get
	.type	pkey_get, @function
pkey_get:
	cmpl	$15, %edi
	ja	.L5
	xorl	%ecx, %ecx
#APP
# 27 "../sysdeps/unix/sysv/linux/x86/arch-pkey.h" 1
	.byte 0x0f, 0x01, 0xee
# 0 "" 2
#NO_APP
	leal	(%rdi,%rdi), %ecx
	shrl	%cl, %eax
	andl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	pkey_get, .-pkey_get
