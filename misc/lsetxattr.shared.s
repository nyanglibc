.text
 .globl lsetxattr
 .type lsetxattr,@function
 .align 1<<4
 lsetxattr: 
 
 movq %rcx, %r10
 movl $189, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size lsetxattr,.-lsetxattr
.globl __GI_lsetxattr 
 .set __GI_lsetxattr,lsetxattr
