.text
 .globl __swapon
 .type __swapon,@function
 .align 1<<4
 __swapon: 
 
 movl $167, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __swapon,.-__swapon
.globl __GI___swapon 
 .set __GI___swapon,__swapon
.weak swapon 
 swapon = __swapon
.globl __GI_swapon 
 .set __GI_swapon,swapon
