	.text
	.p2align 4,,15
	.globl	epoll_wait
	.type	epoll_wait, @function
epoll_wait:
	movl	%ecx, %r10d
#APP
# 30 "../sysdeps/unix/sysv/linux/epoll_wait.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$232, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/epoll_wait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	movl	%r13d, %r10d
	movl	%eax, %r8d
	movl	%r12d, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$232, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/epoll_wait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	epoll_wait, .-epoll_wait
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
