	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__xmknodat
	.type	__xmknodat, @function
__xmknodat:
	testl	%edi, %edi
	movl	%esi, %eax
	movq	%rdx, %rsi
	movl	%ecx, %edx
	jne	.L5
	movq	(%r8), %rcx
	movl	%eax, %edi
	jmp	__GI___mknodat
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__xmknodat, .-__xmknodat
