.text
.globl __mprotect
.type __mprotect,@function
.align 1<<4
__mprotect:
	movl $10, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	lea rtld_errno(%rip), %rcx
	neg %eax
	movl %eax, (%rcx)
	or $-1, %rax
	ret
.size __mprotect,.-__mprotect
.globl __GI___mprotect
.set __GI___mprotect,__mprotect
.weak mprotect
mprotect = __mprotect
.globl __GI_mprotect
.set __GI_mprotect,mprotect
