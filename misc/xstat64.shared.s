	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___xstat64
	.type	___xstat64, @function
___xstat64:
	cmpl	$1, %edi
	movq	%rsi, %rax
	ja	.L2
	movq	%rax, %rdi
	movq	%rdx, %rsi
	movl	$4, %eax
#APP
# 50 "../sysdeps/unix/sysv/linux/xstat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	___xstat64, .-___xstat64
	.globl	__xstat64
	.set	__xstat64,___xstat64
	.globl	__xstat
	.set	__xstat,___xstat64
