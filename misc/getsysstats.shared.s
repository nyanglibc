	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/getsysstats.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"*cp <= *re"
#NO_APP
	.text
	.p2align 4,,15
	.type	next_line, @function
next_line:
.LFB75:
	
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movl	$10, %esi
	movq	%rcx, %rbx
	subq	$24, %rsp
	movq	(%rcx), %r13
	movq	(%rdx), %r12
	movl	%edi, 12(%rsp)
	movq	%r8, (%rsp)
	movq	%r13, %rbp
	movq	%r12, %rdi
	subq	%r12, %rbp
	movq	%rbp, %rdx
	call	__GI_memchr
	testq	%rax, %rax
	movq	(%rsp), %r8
	je	.L18
.L2:
	addq	$1, %rax
	movq	%rax, (%r15)
	movq	(%rbx), %rdx
	cmpq	%rdx, %rax
	ja	.L19
	cmpq	%r12, %rdx
	je	.L7
.L1:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpq	%r14, %r12
	je	.L3
	cmpq	%r8, %r13
	je	.L20
.L3:
	leaq	-1(%r13), %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	__GI_memmove
	movq	(%rbx), %rax
	movl	12(%rsp), %edi
	movq	%r13, %rdx
	addq	%r14, %rax
	subq	(%r15), %rax
	movq	%rax, (%rbx)
	movq	%r14, (%r15)
	movq	(%rbx), %rsi
	subq	%rsi, %rdx
	call	__GI___read_nocancel
	testq	%rax, %rax
	js	.L7
	addq	(%rbx), %rax
	movl	$10, %esi
	movq	%rax, (%rbx)
	movq	(%r15), %r12
	movq	%rax, %rdx
	movq	%rax, %rbp
	movq	%rax, (%rsp)
	subq	%r12, %rdx
	movq	%r12, %rdi
	call	__GI_memchr
	testq	%rax, %rax
	jne	.L2
	cmpq	%rbp, %r13
	movq	%rbp, %rcx
	jne	.L13
	movq	%rbp, %rax
	subq	%r14, %rax
	leaq	(%rax,%rax,2), %rax
	leaq	3(%rax), %rbp
	testq	%rax, %rax
	cmovns	%rax, %rbp
	sarq	$2, %rbp
	addq	%r14, %rbp
	subq	%rbp, %rcx
	movq	%rcx, %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L23:
	movq	(%rbx), %r12
	movq	%rax, %rdx
	movl	$10, %esi
	movq	%r12, %rdi
	call	__GI_memchr
	movb	$10, (%r12)
	addq	(%rbx), %r13
	testq	%rax, %rax
	movq	%r13, (%rbx)
	jne	.L21
	cmpq	%r13, (%rsp)
	jne	.L22
.L9:
	movl	12(%rsp), %edi
	movq	%rbp, (%rbx)
	movq	%r14, %rdx
	movq	%rbp, %rsi
	call	__GI___read_nocancel
	testq	%rax, %rax
	movq	%rax, %r13
	jns	.L23
.L7:
	xorl	%r12d, %r12d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%r15), %r12
	jmp	.L2
.L22:
	movq	(%r15), %r12
	jmp	.L3
.L13:
	movq	(%rsp), %r13
	jmp	.L3
.L19:
	leaq	__PRETTY_FUNCTION__.10848(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$119, %edx
	call	__GI___assert_fail
.LFE75:
	.size	next_line, .-next_line
	.p2align 4,,15
	.type	sysinfo_mempages, @function
sysinfo_mempages:
.LFB78:
	
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %ebx
	subq	$8, %rsp
	call	__GI___getpagesize
	movslq	%eax, %rdx
	cmpq	$1, %rdx
	jbe	.L25
	cmpl	$1, %ebx
	ja	.L26
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L42:
	cmpq	$1, %rdx
	jbe	.L25
.L26:
	shrl	%ebx
	shrq	%rdx
	cmpl	$1, %ebx
	ja	.L42
.L25:
	movl	%ebx, %eax
	imulq	%rbp, %rax
	cmpq	$1, %rdx
	jbe	.L28
	.p2align 4,,10
	.p2align 3
.L29:
	shrq	%rdx
	shrq	%rax
	cmpq	$1, %rdx
	jne	.L29
.L28:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE78:
	.size	sysinfo_mempages, .-sysinfo_mempages
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"/sys/devices/system/cpu/online"
	.section	.rodata.str1.1
.LC3:
	.string	"/proc/stat"
.LC4:
	.string	"cpu"
.LC5:
	.string	"/proc/cpuinfo"
.LC6:
	.string	"processor"
	.text
	.p2align 4,,15
	.globl	__GI___get_nprocs
	.hidden	__GI___get_nprocs
	.type	__GI___get_nprocs, @function
__GI___get_nprocs:
.LFB76:
	
	pushq	%rbp
	movl	$5, %edi
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%r14, %rsi
	subq	$72, %rsp
	call	__GI___clock_gettime
	movq	-64(%rbp), %rax
	movq	%rax, %rcx
	movq	%rax, -96(%rbp)
	movq	timestamp.10853(%rip), %rax
	cmpq	%rcx, %rax
	je	.L76
.L44:
	movl	$8192, %edi
	call	__GI___libc_alloca_cutoff
	cmpl	$1, %eax
	leaq	.LC2(%rip), %rdi
	movl	$524288, %esi
	sbbq	%r12, %r12
	andq	$-7680, %r12
	leaq	8208(%r12), %rax
	addq	$8192, %r12
	subq	%rax, %rsp
	xorl	%eax, %eax
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	addq	%r13, %r12
	movq	%r13, -88(%rbp)
	movq	%r12, -80(%rbp)
	movq	%r12, -72(%rbp)
	call	__GI___open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %edi
	movl	%eax, -100(%rbp)
	je	.L48
	leaq	-80(%rbp), %rax
	leaq	-72(%rbp), %r15
	movq	%r13, %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%rax, %rdx
	call	next_line
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L49
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$10, %edx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__GI_strtoul
	movq	-64(%rbp), %rdx
	movq	%rax, %r15
	cmpq	%r13, %rdx
	je	.L49
	cmpb	$45, (%rdx)
	je	.L77
.L51:
	addl	$1, %ebx
	subl	%r15d, %ebx
	leal	(%rbx,%rax), %r15d
	movq	-72(%rbp), %rax
	movl	%r15d, %ebx
	cmpq	%rax, %rdx
	jb	.L78
.L67:
	movl	-100(%rbp), %edi
	call	__GI___close_nocancel
	testl	%r15d, %r15d
	jle	.L48
.L57:
	movl	%ebx, cached_result.10852(%rip)
	movq	-96(%rbp), %rax
	movq	%rax, timestamp.10853(%rip)
.L43:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	1(%rdx), %r13
	movq	%r14, %rsi
	movl	$10, %edx
	movq	%r13, %rdi
	call	__GI_strtoul
	movq	-64(%rbp), %rdx
	cmpq	%r13, %rdx
	jne	.L51
	.p2align 4,,10
	.p2align 3
.L49:
	movl	-100(%rbp), %edi
	call	__GI___close_nocancel
.L48:
	leaq	.LC3(%rip), %rdi
	xorl	%eax, %eax
	movl	$524288, %esi
	movq	%r12, -80(%rbp)
	movq	%r12, -72(%rbp)
	call	__GI___open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %r14d
	je	.L79
	leaq	-80(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-72(%rbp), %r15
	movq	%rax, %r13
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	.LC4(%rip), %rdi
	movl	$3, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L75
	movsbl	3(%rax), %eax
	subl	$48, %eax
	cmpl	$10, %eax
	adcl	$0, %ebx
.L59:
	movq	-88(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movl	%r14d, %edi
	call	next_line
	testq	%rax, %rax
	jne	.L63
.L75:
	movl	%r14d, %edi
	call	__GI___close_nocancel
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L78:
	cmpb	$44, (%rdx)
	movq	%rdx, %r13
	je	.L80
.L54:
	cmpb	$10, 0(%r13)
	jne	.L56
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$1, %r13
	cmpq	%r13, %rax
	ja	.L54
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L76:
	movl	cached_result.10852(%rip), %ebx
	testl	%ebx, %ebx
	jns	.L43
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	.LC5(%rip), %rdi
	xorl	%eax, %eax
	movl	$524288, %esi
	movl	$2, %ebx
	call	__GI___open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %r14d
	je	.L57
	leaq	-80(%rbp), %rax
	xorl	%ebx, %ebx
	leaq	-72(%rbp), %r15
	movq	%rax, %r13
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	.LC6(%rip), %rdi
	movl	$9, %ecx
	repz cmpsb
	seta	%al
	setb	%dl
	subl	%edx, %eax
	movsbl	%al, %eax
	cmpl	$1, %eax
	adcl	$0, %ebx
.L64:
	movq	-88(%rbp), %rsi
	movq	%r12, %r8
	movq	%r15, %rcx
	movq	%r13, %rdx
	movl	%r14d, %edi
	call	next_line
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L66
	jmp	.L75
.LFE76:
	.size	__GI___get_nprocs, .-__GI___get_nprocs
	.globl	__get_nprocs
	.set	__get_nprocs,__GI___get_nprocs
	.weak	get_nprocs
	.set	get_nprocs,__get_nprocs
	.section	.rodata.str1.1
.LC7:
	.string	"/sys/devices/system/cpu"
	.text
	.p2align 4,,15
	.globl	__GI___get_nprocs_conf
	.hidden	__GI___get_nprocs_conf
	.type	__GI___get_nprocs_conf, @function
__GI___get_nprocs_conf:
.LFB77:
	
	pushq	%r14
	pushq	%r13
	leaq	.LC7(%rip), %rdi
	pushq	%r12
	pushq	%rbp
	xorl	%r12d, %r12d
	pushq	%rbx
	leaq	.LC4(%rip), %r13
	subq	$16, %rsp
	call	__opendir
	testq	%rax, %rax
	movq	%rax, %rbp
	leaq	8(%rsp), %r14
	je	.L95
	.p2align 4,,10
	.p2align 3
.L82:
	movq	%rbp, %rdi
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L96
.L86:
	cmpb	$4, 18(%rax)
	jne	.L82
	leaq	19(%rax), %rsi
	movl	$3, %ecx
	movq	%r13, %rdi
	repz cmpsb
	jne	.L82
	leaq	22(%rax), %rbx
	movl	$10, %edx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__GI_strtoul
	cmpq	$-1, %rax
	je	.L82
	movq	8(%rsp), %rax
	cmpq	%rax, %rbx
	je	.L82
	cmpb	$1, (%rax)
	movq	%rbp, %rdi
	adcl	$0, %r12d
	call	__GI___readdir64
	testq	%rax, %rax
	jne	.L86
.L96:
	movq	%rbp, %rdi
	call	__closedir
	addq	$16, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	call	__GI___get_nprocs
	addq	$16, %rsp
	movl	%eax, %r12d
	popq	%rbx
	movl	%r12d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.LFE77:
	.size	__GI___get_nprocs_conf, .-__GI___get_nprocs_conf
	.globl	__get_nprocs_conf
	.set	__get_nprocs_conf,__GI___get_nprocs_conf
	.weak	get_nprocs_conf
	.set	get_nprocs_conf,__get_nprocs_conf
	.p2align 4,,15
	.globl	__GI___get_phys_pages
	.hidden	__GI___get_phys_pages
	.type	__GI___get_phys_pages, @function
__GI___get_phys_pages:
.LFB79:
	
	subq	$120, %rsp
	movq	%rsp, %rdi
	call	__sysinfo
	movl	104(%rsp), %esi
	movq	32(%rsp), %rdi
	addq	$120, %rsp
	jmp	sysinfo_mempages
.LFE79:
	.size	__GI___get_phys_pages, .-__GI___get_phys_pages
	.globl	__get_phys_pages
	.set	__get_phys_pages,__GI___get_phys_pages
	.weak	get_phys_pages
	.set	get_phys_pages,__get_phys_pages
	.p2align 4,,15
	.globl	__GI___get_avphys_pages
	.hidden	__GI___get_avphys_pages
	.type	__GI___get_avphys_pages, @function
__GI___get_avphys_pages:
.LFB80:
	
	subq	$120, %rsp
	movq	%rsp, %rdi
	call	__sysinfo
	movl	104(%rsp), %esi
	movq	40(%rsp), %rdi
	addq	$120, %rsp
	jmp	sysinfo_mempages
.LFE80:
	.size	__GI___get_avphys_pages, .-__GI___get_avphys_pages
	.globl	__get_avphys_pages
	.set	__get_avphys_pages,__GI___get_avphys_pages
	.weak	get_avphys_pages
	.set	get_avphys_pages,__get_avphys_pages
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10848, @object
	.size	__PRETTY_FUNCTION__.10848, 10
__PRETTY_FUNCTION__.10848:
	.string	"next_line"
	.data
	.align 4
	.type	cached_result.10852, @object
	.size	cached_result.10852, 4
cached_result.10852:
	.long	-1
	.local	timestamp.10853
	.comm	timestamp.10853,8,8
	.hidden	__sysinfo
	.hidden	__closedir
	.hidden	__opendir
