.text
 .globl munlockall
 .type munlockall,@function
 .align 1<<4
 munlockall: 
 
 movl $152, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size munlockall,.-munlockall
.globl __GI_munlockall 
 .set __GI_munlockall,munlockall
