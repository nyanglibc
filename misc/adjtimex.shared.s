	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	___adjtimex
	.type	___adjtimex, @function
___adjtimex:
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	__GI___clock_adjtime
	.size	___adjtimex, .-___adjtimex
	.weak	ntp_adjtime
	.set	ntp_adjtime,___adjtimex
	.weak	adjtimex
	.set	adjtimex,___adjtimex
	.globl	__GI___adjtimex
	.hidden	__GI___adjtimex
	.set	__GI___adjtimex,___adjtimex
	.globl	__adjtimex
	.set	__adjtimex,__GI___adjtimex
