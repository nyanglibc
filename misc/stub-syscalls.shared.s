	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.glibc-stub.__compat_bdflush
	.previous
	.section .gnu.warning.__compat_bdflush
	.previous
	.symver __bdflush_GLIBC_2_0,bdflush@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_no_syscall
	.type	_no_syscall, @function
_no_syscall:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movq	$-1, %rax
	ret
	.size	_no_syscall, .-_no_syscall
	.globl	__bdflush_GLIBC_2_0
	.set	__bdflush_GLIBC_2_0,_no_syscall
	.weak	__GI___compat_bdflush
	.set	__GI___compat_bdflush,_no_syscall
	.weak	__compat_bdflush
	.set	__compat_bdflush,_no_syscall
	.section	.gnu.warning.__compat_bdflush
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning___compat_bdflush, @object
	.size	__evoke_link_warning___compat_bdflush, 57
__evoke_link_warning___compat_bdflush:
	.string	"__compat_bdflush is not implemented and will always fail"
