.text
 .globl epoll_create1
 .type epoll_create1,@function
 .align 1<<4
 epoll_create1: 
 
 movl $291, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size epoll_create1,.-epoll_create1
.globl __GI_epoll_create1 
 .set __GI_epoll_create1,epoll_create1
