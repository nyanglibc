.text
 .globl fremovexattr
 .type fremovexattr,@function
 .align 1<<4
 fremovexattr: 
 
 movl $199, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size fremovexattr,.-fremovexattr
.globl __GI_fremovexattr 
 .set __GI_fremovexattr,fremovexattr
