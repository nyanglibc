	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__qfcvt
	.type	__qfcvt, @function
__qfcvt:
	movq	qfcvt_bufptr(%rip), %rcx
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movl	%edi, %ebx
	testq	%rcx, %rcx
	je	.L11
.L2:
	pushq	40(%rsp)
	pushq	40(%rsp)
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$4965, %r8d
	call	__GI___qfcvt_r
	popq	%rdx
	movq	qfcvt_bufptr(%rip), %rax
	popq	%rcx
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	pushq	40(%rsp)
	leaq	qfcvt_buffer(%rip), %rcx
	pushq	40(%rsp)
	movl	$33, %r8d
	call	__GI___qfcvt_r
	cmpl	$-1, %eax
	popq	%rsi
	popq	%rdi
	je	.L3
.L5:
	popq	%rbx
	leaq	qfcvt_buffer(%rip), %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$4965, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, qfcvt_bufptr(%rip)
	jne	.L2
	jmp	.L5
	.size	__qfcvt, .-__qfcvt
	.globl	qfcvt
	.set	qfcvt,__qfcvt
	.p2align 4,,15
	.globl	__qecvt
	.type	__qecvt, @function
__qecvt:
	subq	$8, %rsp
	leaq	qecvt_buffer(%rip), %rcx
	movl	$33, %r8d
	pushq	24(%rsp)
	pushq	24(%rsp)
	call	__GI___qecvt_r
	leaq	qecvt_buffer(%rip), %rax
	addq	$24, %rsp
	ret
	.size	__qecvt, .-__qecvt
	.globl	qecvt
	.set	qecvt,__qecvt
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%.*Lg"
	.text
	.p2align 4,,15
	.globl	__qgcvt
	.type	__qgcvt, @function
__qgcvt:
	pushq	%rbx
	cmpl	$21, %edi
	pushq	24(%rsp)
	movq	%rsi, %rbx
	pushq	24(%rsp)
	movl	$21, %edx
	leaq	.LC0(%rip), %rsi
	cmovle	%edi, %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	__GI_sprintf
	popq	%rax
	movq	%rbx, %rax
	popq	%rdx
	popq	%rbx
	ret
	.size	__qgcvt, .-__qgcvt
	.globl	qgcvt
	.set	qgcvt,__qgcvt
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	qfcvt_bufptr, @object
	.size	qfcvt_bufptr, 8
qfcvt_bufptr:
	.zero	8
	.local	qecvt_buffer
	.comm	qecvt_buffer,33,32
	.local	qfcvt_buffer
	.comm	qfcvt_buffer,33,32
