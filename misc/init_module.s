.text
 .globl init_module
 .type init_module,@function
 .align 1<<4
 init_module: 
 
 movq %rcx, %r10
 movl $175, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size init_module,.-init_module
