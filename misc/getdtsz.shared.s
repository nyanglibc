	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getdtablesize
	.hidden	__getdtablesize
	.type	__getdtablesize, @function
__getdtablesize:
	subq	$24, %rsp
	movl	$7, %edi
	movq	%rsp, %rsi
	call	__GI___getrlimit
	testl	%eax, %eax
	js	.L3
	movl	(%rsp), %eax
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$256, %eax
	addq	$24, %rsp
	ret
	.size	__getdtablesize, .-__getdtablesize
	.weak	getdtablesize
	.set	getdtablesize,__getdtablesize
