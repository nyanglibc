.text
 .globl inotify_add_watch
 .type inotify_add_watch,@function
 .align 1<<4
 inotify_add_watch: 
 
 movl $254, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size inotify_add_watch,.-inotify_add_watch
.globl __GI_inotify_add_watch 
 .set __GI_inotify_add_watch,inotify_add_watch
