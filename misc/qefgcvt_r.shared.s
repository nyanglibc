	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"%.*Lf"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___qfcvt_r
	.hidden	__GI___qfcvt_r
	.type	__GI___qfcvt_r, @function
__GI___qfcvt_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testq	%rcx, %rcx
	fldt	80(%rsp)
	je	.L50
	fldt	.LC0(%rip)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jnb	.L51
	movl	$0, (%rdx)
.L46:
	cmpl	$21, %edi
	movl	$21, %r13d
	cmovg	%r13d, %edi
	xorl	%r13d, %r13d
.L7:
	subq	$16, %rsp
	movq	%rcx, %rbp
	leaq	.LC3(%rip), %rdx
	fld	%st(0)
	fstpt	(%rsp)
	movl	%edi, %ecx
	xorl	%eax, %eax
	movq	%rsi, %r14
	movq	%rbp, %rdi
	movq	%r8, %rsi
	movq	%r8, %r12
	fstpt	16(%rsp)
	call	__GI___snprintf
	movslq	%eax, %rbx
	cmpq	%rbx, %r12
	popq	%rdx
	popq	%rcx
	jle	.L26
	testq	%rbx, %rbx
	fldt	(%rsp)
	jle	.L57
	movsbl	0(%rbp), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L58
	xorl	%r10d, %r10d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L53:
	movsbl	1(%rbp,%r10), %edx
	subl	$48, %edx
	cmpl	$9, %edx
	ja	.L52
	movq	%r9, %r10
.L12:
	leaq	1(%r10), %r9
	cmpq	%r9, %rbx
	jne	.L53
	fstp	%st(0)
	movl	%eax, (%r14)
.L14:
	testl	%r13d, %r13d
	je	.L48
	subq	$1, %r12
	addl	%r13d, (%r14)
	cmpq	%rbx, %r12
	jle	.L48
	subl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	movb	$48, -1(%rbp,%rbx)
	setg	%dl
	testl	%r13d, %r13d
	setg	%al
	subl	$1, %r13d
	testb	%al, %dl
	jne	.L24
	movb	$0, 0(%rbp,%rbx)
.L48:
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%r9d, %r9d
	fxam
	fnstsw	%ax
	andl	$512, %eax
	setne	%r9b
	movl	%r9d, (%rdx)
	je	.L5
	fchs
.L5:
	testl	%edi, %edi
	jns	.L46
	fld	%st(0)
	negl	%edi
	xorl	%eax, %eax
	movl	%edi, %r13d
	fldl	.LC1(%rip)
	fld1
	fxch	%st(3)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L59:
	fxch	%st(2)
	fxch	%st(1)
	fxch	%st(3)
.L8:
	fmul	%st(1), %st
	fxch	%st(3)
	fucomi	%st(3), %st
	ja	.L25
	fstp	%st(2)
	fxch	%st(1)
	addl	$1, %eax
	fld	%st(2)
	cmpl	%eax, %r13d
	jne	.L59
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	xorl	%edi, %edi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L52:
	movslq	%r9d, %rcx
	movl	%r9d, (%r14)
	leaq	2(%rbp,%r10), %rdx
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L54:
	movsbl	(%rsi), %eax
	addq	$1, %rdx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L15
	movq	%r15, %r9
.L16:
	leaq	1(%r9), %r15
	movq	%rdx, %rsi
	cmpq	%r15, %rbx
	jg	.L54
.L15:
	cmpl	$1, %ecx
	je	.L55
	fstp	%st(0)
	testl	%ecx, %ecx
	movl	$0, %edi
	cmovns	%rcx, %rdi
	addq	%rbp, %rdi
.L20:
	movq	%rbx, %rdx
	subq	%r15, %rdx
	call	__GI_memmove
	movl	(%r14), %eax
	testl	%eax, %eax
	movl	$0, %eax
	cmovns	(%r14), %eax
	cltq
	subq	%rax, %r15
	movq	%rbx, %rax
	subq	%r15, %rax
	movb	$0, 0(%rbp,%rax)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L55:
	cmpb	$48, 0(%rbp)
	jne	.L60
	fldz
	movl	$1, %edx
	fucomip	%st(1), %st
	fstp	%st(0)
	setp	%al
	cmovne	%edx, %eax
	testb	%al, %al
	jne	.L18
	jmp	.L31
.L60:
	fstp	%st(0)
.L31:
	leaq	1(%rbp), %rdi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	fstp	%st(0)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L58:
	fstp	%st(0)
.L10:
	movl	$0, (%r14)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(1)
	movl	%eax, %r13d
	xorl	%edi, %edi
	jmp	.L7
.L18:
	cmpq	%r15, %rbx
	movl	$0, (%r14)
	jle	.L30
	cmpb	$48, (%rsi)
	jne	.L30
	leaq	2(%rbp,%r9), %rcx
	movl	$-1, %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	subl	$1, %edx
	cmpb	$48, (%rsi)
	leaq	1(%rcx), %rcx
	jne	.L56
.L22:
	addq	$1, %r15
	movslq	%edx, %rax
	movl	%edx, (%r14)
	cmpq	%r15, %rbx
	movq	%rcx, %rsi
	jne	.L21
	testl	%edx, %edx
	movl	$0, %edi
	cmovns	%edx, %edi
	movslq	%edi, %rdi
	addq	%rbp, %rdi
	jmp	.L20
.L26:
	movl	$-1, %eax
	jmp	.L1
.L50:
	fstp	%st(0)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
.L56:
	testl	%eax, %eax
	movl	$0, %edi
	cmovns	%rax, %rdi
	addq	%rbp, %rdi
	jmp	.L20
.L30:
	movq	%rbp, %rdi
	jmp	.L20
	.size	__GI___qfcvt_r, .-__GI___qfcvt_r
	.globl	__qfcvt_r
	.set	__qfcvt_r,__GI___qfcvt_r
	.weak	qfcvt_r
	.set	qfcvt_r,__qfcvt_r
	.p2align 4,,15
	.globl	__GI___qecvt_r
	.hidden	__GI___qecvt_r
	.type	__GI___qecvt_r, @function
__GI___qecvt_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	testl	%edi, %edi
	fldt	32(%rsp)
	setle	%al
	testq	%r8, %r8
	setne	%sil
	andl	%esi, %eax
	fld	%st(0)
	fabs
	fldt	.LC0(%rip)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L62
	fldz
	fxch	%st(1)
	fucomi	%st(1), %st
	fstp	%st(1)
	jp	.L86
	je	.L97
.L86:
	fldz
	fld	%st(1)
	fxch	%st(1)
	fucomip	%st(2), %st
	ja	.L103
.L65:
	fldt	.LC6(%rip)
	xorl	%ebx, %ebx
	fucomi	%st(1), %st
	jbe	.L106
	fstp	%st(1)
	fdivrp	%st, %st(1)
	movl	$-4931, %ebx
	fldz
	fld	%st(1)
	fxch	%st(1)
	fucomip	%st(2), %st
	ja	.L104
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L106:
	fstp	%st(0)
.L67:
	flds	.LC7(%rip)
	fld1
	fucomi	%st(2), %st
	ja	.L105
	fld	%st(1)
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jb	.L107
	.p2align 4,,10
	.p2align 3
.L74:
	fld	%st(1)
	addl	$1, %ebx
	fmul	%st, %st(1)
	fmul	%st(1), %st
	fxch	%st(3)
	fucomi	%st(3), %st
	fstp	%st(3)
	jnb	.L74
	fstp	%st(2)
	fstp	%st(0)
	fdivrp	%st, %st(1)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L62:
	xorl	%ebx, %ebx
	testb	%al, %al
	je	.L75
	fstp	%st(0)
	movb	$0, (%rcx)
	xorl	%ebx, %ebx
	movl	$1, 0(%rbp)
	xorl	%eax, %eax
.L76:
	movl	%eax, (%rdx)
.L77:
	addl	%ebx, 0(%rbp)
	xorl	%eax, %eax
.L61:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$21, %edi
	movl	$21, %eax
	movq	%rbp, %rsi
	cmovg	%eax, %edi
	subq	$16, %rsp
	subl	$1, %edi
	fstpt	(%rsp)
	call	__GI___qfcvt_r
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L77
	movl	$-1, %eax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%ebx, %ebx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L107:
	fstp	%st(0)
	fstp	%st(0)
	fstp	%st(0)
	.p2align 4,,10
	.p2align 3
.L63:
	testb	%al, %al
	je	.L75
	fldt	.LC0(%rip)
	movb	$0, (%rcx)
	movl	$1, 0(%rbp)
	fld	%st(1)
	fabs
	fxch	%st(1)
	fucomip	%st(1), %st
	fstp	%st(0)
	jb	.L84
	fxam
	fnstsw	%ax
	fstp	%st(0)
	testb	$2, %ah
	setne	%al
	movzbl	%al, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L105:
	fld1
	fxch	%st(1)
	.p2align 4,,10
	.p2align 3
.L72:
	fmul	%st(2), %st
	subl	$1, %ebx
	fld	%st(3)
	fmul	%st(1), %st
	fxch	%st(2)
	fucomi	%st(2), %st
	fstp	%st(2)
	ja	.L72
	fstp	%st(3)
	fstp	%st(0)
	fstp	%st(0)
	fmulp	%st, %st(1)
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L103:
	fchs
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L104:
	fchs
	movl	$-4931, %ebx
	jmp	.L67
.L84:
	fstp	%st(0)
	xorl	%eax, %eax
	jmp	.L76
	.size	__GI___qecvt_r, .-__GI___qecvt_r
	.globl	__qecvt_r
	.set	__qecvt_r,__GI___qecvt_r
	.weak	qecvt_r
	.set	qecvt_r,__qecvt_r
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.long	4294967295
	.long	4294967295
	.long	32766
	.long	0
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
.LC1:
	.long	2576980378
	.long	1069128089
	.section	.rodata.cst16
	.align 16
.LC6:
	.long	3168843697
	.long	3193661164
	.long	2
	.long	0
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
.LC7:
	.long	1092616192
