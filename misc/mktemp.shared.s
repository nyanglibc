	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.mktemp
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__mktemp
	.type	__mktemp, @function
__mktemp:
	pushq	%rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$2, %ecx
	movq	%rdi, %rbx
	call	__gen_tempname
	testl	%eax, %eax
	jns	.L2
	movb	$0, (%rbx)
.L2:
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	__mktemp, .-__mktemp
	.weak	mktemp
	.set	mktemp,__mktemp
	.section	.gnu.warning.mktemp
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_mktemp, @object
	.size	__evoke_link_warning_mktemp, 68
__evoke_link_warning_mktemp:
	.string	"the use of `mktemp' is dangerous, better use `mkstemp' or `mkdtemp'"
	.hidden	__gen_tempname
