	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getclktck
	.hidden	__getclktck
	.type	__getclktck, @function
__getclktck:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movl	$100, %edx
	movl	56(%rax), %eax
	testl	%eax, %eax
	cmove	%edx, %eax
	ret
	.size	__getclktck, .-__getclktck
