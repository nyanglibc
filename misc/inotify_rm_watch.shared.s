.text
 .globl inotify_rm_watch
 .type inotify_rm_watch,@function
 .align 1<<4
 inotify_rm_watch: 
 
 movl $255, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size inotify_rm_watch,.-inotify_rm_watch
.globl __GI_inotify_rm_watch 
 .set __GI_inotify_rm_watch,inotify_rm_watch
