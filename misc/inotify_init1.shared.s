.text
 .globl inotify_init1
 .type inotify_init1,@function
 .align 1<<4
 inotify_init1: 
 
 movl $294, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size inotify_init1,.-inotify_init1
.globl __GI_inotify_init1 
 .set __GI_inotify_init1,inotify_init1
