	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_getdomainname
	.hidden	__GI_getdomainname
	.type	__GI_getdomainname, @function
__GI_getdomainname:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$400, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__GI_uname
	testl	%eax, %eax
	js	.L3
	addq	$325, %rbx
	movq	%rbx, %rdi
	call	__GI_strlen
	addq	$1, %rax
	movq	%r12, %rdx
	movq	%rbx, %rsi
	cmpq	%r12, %rax
	movq	%rbp, %rdi
	cmovbe	%rax, %rdx
	call	__GI_memcpy@PLT
	xorl	%eax, %eax
.L1:
	addq	$400, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__GI_getdomainname, .-__GI_getdomainname
	.globl	getdomainname
	.set	getdomainname,__GI_getdomainname
