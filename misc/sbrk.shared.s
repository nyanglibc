	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sbrk
	.hidden	__GI___sbrk
	.type	__GI___sbrk, @function
__GI___sbrk:
	cmpb	$0, __libc_initial(%rip)
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	jne	.L2
	testq	%rdi, %rdi
	jne	.L10
	xorl	%edi, %edi
	call	__brk
	testl	%eax, %eax
	js	.L11
	movq	__curbrk@GOTPCREL(%rip), %rax
	movq	(%rax), %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__curbrk@GOTPCREL(%rip), %r12
	movq	(%r12), %rbx
	testq	%rbx, %rbx
	je	.L15
.L5:
	cmpq	$0, %rbp
	je	.L1
	jle	.L8
	movq	%rbx, %rax
	addq	%rbp, %rax
	setc	%al
.L9:
	testb	%al, %al
	je	.L16
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rbx
	movl	$12, %fs:(%rax)
.L1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%edi, %edi
	call	__brk
	testl	%eax, %eax
	js	.L11
	movq	(%r12), %rbx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	(%rbx,%rbp), %rdi
	call	__brk
	testl	%eax, %eax
	jns	.L1
.L11:
	movq	$-1, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbp, %rax
	negq	%rax
	cmpq	%rax, %rbx
	setb	%al
	jmp	.L9
	.size	__GI___sbrk, .-__GI___sbrk
	.globl	__sbrk
	.set	__sbrk,__GI___sbrk
	.weak	sbrk
	.set	sbrk,__sbrk
	.hidden	__brk
	.hidden	__libc_initial
