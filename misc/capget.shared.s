.text
 .globl capget
 .type capget,@function
 .align 1<<4
 capget: 
 
 movl $125, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size capget,.-capget
.globl __GI_capget 
 .set __GI_capget,capget
