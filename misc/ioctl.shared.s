.text
 .globl __ioctl
 .type __ioctl,@function
 .align 1<<4
 __ioctl: 
 
 movl $16, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __ioctl,.-__ioctl
.globl __GI___ioctl 
 .set __GI___ioctl,__ioctl
.weak ioctl 
 ioctl = __ioctl
.globl __GI_ioctl 
 .set __GI_ioctl,ioctl
