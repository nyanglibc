	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: "
.LC1:
	.string	"\n"
	.text
	.p2align 4,,15
	.globl	__vwarnx_internal
	.type	__vwarnx_internal, @function
__vwarnx_internal:
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	movq	__progname(%rip), %rdx
	pushq	%rbx
	movq	%rdi, %rbx
	movq	stderr(%rip), %rdi
	movq	%rsi, %rbp
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	call	__fxprintf
	testq	%rbx, %rbx
	je	.L2
	movq	stderr(%rip), %rdi
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	call	__vfxprintf
.L2:
	popq	%rbx
	popq	%rbp
	popq	%r12
	movq	stderr(%rip), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	jmp	__fxprintf
	.size	__vwarnx_internal, .-__vwarnx_internal
	.section	.rodata.str1.1
.LC2:
	.string	": %m\n"
.LC3:
	.string	"%s: %m\n"
	.text
	.p2align 4,,15
	.globl	__vwarn_internal
	.type	__vwarn_internal, @function
__vwarn_internal:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	__progname(%rip), %r8
	testq	%rbx, %rbx
	movq	stderr(%rip), %rdi
	je	.L9
	movq	__libc_errno@gottpoff(%rip), %r13
	movq	%rsi, %rbp
	leaq	.LC0(%rip), %rsi
	movl	%edx, %r12d
	xorl	%eax, %eax
	movq	%r8, %rdx
	movl	%fs:0(%r13), %r14d
	call	__fxprintf
	movq	stderr(%rip), %rdi
	movq	%rbx, %rsi
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	call	__vfxprintf
	movl	%r14d, %fs:0(%r13)
	movq	stderr(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	xorl	%eax, %eax
	jmp	__fxprintf
	.p2align 4,,10
	.p2align 3
.L9:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	leaq	.LC3(%rip), %rsi
	movq	%r8, %rdx
	xorl	%eax, %eax
	jmp	__fxprintf
	.size	__vwarn_internal, .-__vwarn_internal
	.p2align 4,,15
	.globl	vwarn
	.hidden	vwarn
	.type	vwarn, @function
vwarn:
	xorl	%edx, %edx
	jmp	__vwarn_internal
	.size	vwarn, .-vwarn
	.p2align 4,,15
	.globl	vwarnx
	.hidden	vwarnx
	.type	vwarnx, @function
vwarnx:
	xorl	%edx, %edx
	jmp	__vwarnx_internal
	.size	vwarnx, .-vwarnx
	.p2align 4,,15
	.globl	warn
	.hidden	warn
	.type	warn, @function
warn:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L15
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L15:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vwarn_internal
	addq	$216, %rsp
	ret
	.size	warn, .-warn
	.p2align 4,,15
	.globl	warnx
	.hidden	warnx
	.type	warnx, @function
warnx:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L19
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L19:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vwarnx_internal
	addq	$216, %rsp
	ret
	.size	warnx, .-warnx
	.p2align 4,,15
	.globl	verr
	.hidden	verr
	.type	verr, @function
verr:
	pushq	%rbx
	movl	%edi, %ebx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	xorl	%edx, %edx
	call	__vwarn_internal
	movl	%ebx, %edi
	call	exit
	.size	verr, .-verr
	.p2align 4,,15
	.globl	verrx
	.hidden	verrx
	.type	verrx, @function
verrx:
	pushq	%rbx
	movl	%edi, %ebx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	xorl	%edx, %edx
	call	__vwarnx_internal
	movl	%ebx, %edi
	call	exit
	.size	verrx, .-verrx
	.p2align 4,,15
	.globl	err
	.type	err, @function
err:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L27
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L27:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	verr
	.size	err, .-err
	.p2align 4,,15
	.globl	errx
	.type	errx, @function
errx:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L31
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L31:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	verrx
	.size	errx, .-errx
	.hidden	exit
	.hidden	__vfxprintf
	.hidden	__fxprintf
