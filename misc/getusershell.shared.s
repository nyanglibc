	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"/etc/shells"
.LC2:
	.string	"/bin/sh"
.LC3:
	.string	"/bin/csh"
#NO_APP
	.text
	.p2align 4,,15
	.type	initshells, @function
initshells:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$152, %rsp
	movq	shells(%rip), %rdi
	call	free@PLT
	movq	strings(%rip), %rdi
	movq	$0, shells(%rip)
	call	free@PLT
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movq	$0, strings(%rip)
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	je	.L2
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	__GI_fileno
	movq	%rsp, %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	cmpl	$-1, %eax
	je	.L4
	movq	48(%rsp), %r13
	movabsq	$6917529027641081853, %rax
	cmpq	%rax, %r13
	jbe	.L55
.L4:
	movq	%rbp, %rdi
	call	_IO_new_fclose@PLT
.L2:
	leaq	.LC2(%rip), %rax
	movq	%rax, okshells(%rip)
	leaq	.LC3(%rip), %rax
	movq	%rax, 8+okshells(%rip)
	leaq	okshells(%rip), %rax
.L1:
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	3(%r13), %r12
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, strings(%rip)
	je	.L4
	movq	%r13, %rax
	movabsq	$6148914691236517206, %rdx
	sarq	$63, %r13
	imulq	%rdx
	subq	%r13, %rdx
	leaq	0(,%rdx,8), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, shells(%rip)
	je	.L7
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, %rcx
	movl	%r12d, %esi
	movq	%rbp, %rdx
	subq	%rax, %rcx
	movq	%rbx, %rdi
	subl	%ecx, %esi
	call	__GI_fgets_unlocked
	testq	%rax, %rax
	je	.L14
	movzbl	(%rbx), %eax
	cmpb	$35, %al
	jne	.L53
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	cmpb	$35, %al
	je	.L9
.L53:
	cmpb	$47, %al
	je	.L9
	testb	%al, %al
	jne	.L10
.L11:
	movq	strings(%rip), %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	cmpb	$35, %al
	je	.L11
	testb	%al, %al
	je	.L11
	movzbl	1(%rbx), %eax
	testb	%al, %al
	je	.L11
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movsbq	(%rbx), %rdi
	leaq	8(%r13), %rsi
	movq	%rbx, 0(%r13)
	movq	%fs:(%rdx), %rcx
	movq	%rdi, %rdx
	testb	$32, 1(%rcx,%rdi,2)
	jne	.L12
	testb	%dil, %dil
	setne	%dil
	cmpb	$35, %dl
	setne	%dl
	testb	%dl, %dil
	jne	.L13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L56:
	cmpb	$35, %al
	setne	%dl
	testb	%al, %al
	setne	%al
	testb	%al, %dl
	je	.L12
	movzbl	1(%rbx), %eax
.L13:
	movsbq	%al, %rdx
	addq	$1, %rbx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L56
.L12:
	movb	$0, (%rbx)
	movq	%rsi, %r13
	addq	$1, %rbx
	movq	strings(%rip), %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L14:
	movq	$0, 0(%r13)
	movq	%rbp, %rdi
	call	_IO_new_fclose@PLT
	movq	shells(%rip), %rax
	jmp	.L1
.L7:
	movq	%rbx, %rdi
	call	free@PLT
	movq	$0, strings(%rip)
	jmp	.L4
	.size	initshells, .-initshells
	.p2align 4,,15
	.globl	getusershell
	.type	getusershell, @function
getusershell:
	movq	curshell(%rip), %rdx
	testq	%rdx, %rdx
	je	.L69
	movq	(%rdx), %rax
	testq	%rax, %rax
	je	.L66
	addq	$8, %rdx
	movq	%rdx, curshell(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	rep ret
	.p2align 4,,10
	.p2align 3
.L69:
	subq	$8, %rsp
	call	initshells
	movq	%rax, %rdx
	movq	%rax, curshell(%rip)
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L57
	addq	$8, %rdx
	movq	%rdx, curshell(%rip)
.L57:
	addq	$8, %rsp
	ret
	.size	getusershell, .-getusershell
	.p2align 4,,15
	.globl	endusershell
	.type	endusershell, @function
endusershell:
	subq	$8, %rsp
	movq	shells(%rip), %rdi
	call	free@PLT
	movq	strings(%rip), %rdi
	movq	$0, shells(%rip)
	call	free@PLT
	movq	$0, strings(%rip)
	movq	$0, curshell(%rip)
	addq	$8, %rsp
	ret
	.size	endusershell, .-endusershell
	.p2align 4,,15
	.globl	setusershell
	.type	setusershell, @function
setusershell:
	subq	$8, %rsp
	call	initshells
	movq	%rax, curshell(%rip)
	addq	$8, %rsp
	ret
	.size	setusershell, .-setusershell
	.local	strings
	.comm	strings,8,8
	.local	shells
	.comm	shells,8,8
	.local	curshell
	.comm	curshell,8,8
	.local	okshells
	.comm	okshells,24,16
