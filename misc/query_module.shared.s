.text
 .globl __compat_query_module
 .type __compat_query_module,@function
 .align 1<<4
 __compat_query_module: 
 
 movq %rcx, %r10
 movl $178, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __compat_query_module,.-__compat_query_module
.globl __GI___compat_query_module 
 .set __GI___compat_query_module,__compat_query_module
.symver __compat_query_module, query_module@GLIBC_2.2.5
