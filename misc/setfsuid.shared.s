.text
 .globl setfsuid
 .type setfsuid,@function
 .align 1<<4
 setfsuid: 
 
 movl $122, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size setfsuid,.-setfsuid
.globl __GI_setfsuid 
 .set __GI_setfsuid,setfsuid
