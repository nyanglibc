.text
 .globl __sysinfo
 .type __sysinfo,@function
 .align 1<<4
 __sysinfo: 
 
 movl $99, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __sysinfo,.-__sysinfo
.globl __GI___sysinfo 
 .set __GI___sysinfo,__sysinfo
.weak sysinfo 
 sysinfo = __sysinfo
.globl __GI_sysinfo 
 .set __GI_sysinfo,sysinfo
