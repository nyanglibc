.text
 .globl fanotify_mark
 .type fanotify_mark,@function
 .align 1<<4
 fanotify_mark: 
 
 movq %rcx, %r10
 movl $301, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size fanotify_mark,.-fanotify_mark
.globl __GI_fanotify_mark 
 .set __GI_fanotify_mark,fanotify_mark
