	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver loc1,loc1@GLIBC_2.2.5
	.symver loc2,loc2@GLIBC_2.2.5
	.symver locs,locs@GLIBC_2.2.5
	.symver step,step@GLIBC_2.2.5
	.symver advance,advance@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.weak	step
	.type	step, @function
step:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	8(%rsi), %rdi
	movl	$1, %edx
	movl	$2, %r8d
	movq	%rbx, %rsi
	subq	$16, %rsp
	andq	$-8, %rdi
	leaq	8(%rsp), %rcx
	call	__GI___regexec
	xorl	%edx, %edx
	cmpl	$1, %eax
	je	.L1
	movslq	8(%rsp), %rax
	movq	loc1@GOTPCREL(%rip), %rdx
	addq	%rbx, %rax
	movq	%rax, (%rdx)
	movslq	12(%rsp), %rax
	movl	$1, %edx
	addq	%rax, %rbx
	movq	loc2@GOTPCREL(%rip), %rax
	movq	%rbx, (%rax)
.L1:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	ret
	.size	step, .-step
	.p2align 4,,15
	.weak	advance
	.type	advance, @function
advance:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	8(%rsi), %rdi
	movl	$1, %edx
	movl	$2, %r8d
	movq	%rbx, %rsi
	subq	$16, %rsp
	andq	$-8, %rdi
	leaq	8(%rsp), %rcx
	call	__GI___regexec
	xorl	%edx, %edx
	cmpl	$1, %eax
	je	.L8
	movl	8(%rsp), %eax
	testl	%eax, %eax
	jne	.L8
	movslq	12(%rsp), %rax
	movl	$1, %edx
	addq	%rax, %rbx
	movq	loc2@GOTPCREL(%rip), %rax
	movq	%rbx, (%rax)
.L8:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	ret
	.size	advance, .-advance
	.globl	locs
	.bss
	.align 8
	.type	locs, @object
	.size	locs, 8
locs:
	.zero	8
	.globl	loc2
	.align 8
	.type	loc2, @object
	.size	loc2, 8
loc2:
	.zero	8
	.globl	loc1
	.align 8
	.type	loc1, @object
	.size	loc1, 8
loc1:
	.zero	8
