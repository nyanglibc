.text
 .globl sethostname
 .type sethostname,@function
 .align 1<<4
 sethostname: 
 
 movl $170, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size sethostname,.-sethostname
.globl __GI_sethostname 
 .set __GI_sethostname,sethostname
