	.text
#APP
	.section .gnu.glibc-stub.stty
	.previous
	.section .gnu.warning.stty
	.previous
#NO_APP
	.p2align 4,,15
	.globl	stty
	.type	stty, @function
stty:
	testq	%rsi, %rsi
	movq	__libc_errno@gottpoff(%rip), %rax
	je	.L5
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	stty, .-stty
	.section	.gnu.warning.stty
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_stty, @object
	.size	__evoke_link_warning_stty, 45
__evoke_link_warning_stty:
	.string	"stty is not implemented and will always fail"
