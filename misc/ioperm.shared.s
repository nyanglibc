.text
 .globl ioperm
 .type ioperm,@function
 .align 1<<4
 ioperm: 
 
 movl $173, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size ioperm,.-ioperm
.globl __GI_ioperm 
 .set __GI_ioperm,ioperm
