	.text
	.p2align 4,,15
	.globl	__mmap64
	.type	__mmap64, @function
__mmap64:
	testl	$4095, %r9d
	jne	.L18
	testq	%rdi, %rdi
	pushq	%r15
	movl	%r8d, %r15d
	pushq	%r14
	movl	%ecx, %r14d
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%r9, %rbx
	je	.L19
.L4:
	movq	%rbx, %r9
	movl	%r15d, %r8d
	movl	%r14d, %r10d
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	movl	$9, %eax
#APP
# 59 "../sysdeps/unix/sysv/linux/mmap64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L20
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$22, rtld_errno(%rip)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	testb	$4, %dl
	je	.L4
	testb	$2, 381+_rtld_local_ro(%rip)
	je	.L4
	movl	%ecx, %r10d
	movl	$9, %ecx
	xorl	%edi, %edi
	orl	$64, %r10d
	movl	%ecx, %eax
#APP
# 54 "../sysdeps/unix/sysv/linux/mmap64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	jbe	.L1
	negl	%edx
	movl	%edx, rtld_errno(%rip)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L20:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movq	$-1, %rax
	jmp	.L1
	.size	__mmap64, .-__mmap64
	.weak	__mmap
	.hidden	__mmap
	.set	__mmap,__mmap64
	.weak	mmap
	.set	mmap,__mmap64
	.weak	mmap64
	.set	mmap64,__mmap64
	.hidden	_rtld_local_ro
	.hidden	rtld_errno
