	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_seteuid
	.hidden	__GI_seteuid
	.type	__GI_seteuid, @function
__GI_seteuid:
	cmpl	$-1, %edi
	je	.L12
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	jne	.L13
	movl	$-1, %edx
	movl	%edi, %esi
	movl	$117, %eax
	movl	%edx, %edi
#APP
# 34 "../sysdeps/unix/sysv/linux/seteuid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	subq	$56, %rsp
	movq	$-1, %rax
	movl	%edi, %ecx
	movq	%rax, 8(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rsp, %rdi
	movl	$117, (%rsp)
	movq	%rcx, 16(%rsp)
	movq	224+__libc_pthread_functions(%rip), %rax
#APP
# 34 "../sysdeps/unix/sysv/linux/seteuid.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	%edi, %eax
	ret
	.size	__GI_seteuid, .-__GI_seteuid
	.globl	seteuid
	.set	seteuid,__GI_seteuid
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
