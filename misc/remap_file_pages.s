.text
 .globl __remap_file_pages
 .type __remap_file_pages,@function
 .align 1<<4
 __remap_file_pages: 
 
 movq %rcx, %r10
 movl $216, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __remap_file_pages,.-__remap_file_pages
.weak remap_file_pages 
 remap_file_pages = __remap_file_pages
