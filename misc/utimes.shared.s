	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__utimes
	.hidden	__utimes
	.type	__utimes, @function
__utimes:
	subq	$40, %rsp
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L2
	imulq	$1000, 8(%rsi), %rax
	movq	(%rsi), %rdx
	movq	%rdx, (%rsp)
	movq	16(%rsi), %rdx
	movq	%rax, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsp, %rdx
	imulq	$1000, 24(%rsi), %rax
	movq	%rax, 24(%rsp)
.L2:
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movl	$-100, %edi
	call	__GI___utimensat64_helper
	addq	$40, %rsp
	ret
	.size	__utimes, .-__utimes
	.weak	utimes
	.set	utimes,__utimes
