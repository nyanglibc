.text
 .globl setdomainname
 .type setdomainname,@function
 .align 1<<4
 setdomainname: 
 
 movl $171, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size setdomainname,.-setdomainname
.globl __GI_setdomainname 
 .set __GI_setdomainname,setdomainname
