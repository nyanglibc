	.text
	.p2align 4,,15
	.type	openlog_internal, @function
openlog_internal:
.LFB80:
	
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L2
	movq	%rdi, LogTag(%rip)
.L2:
	testl	%edx, %edx
	movl	%esi, LogStat(%rip)
	je	.L3
	testl	$-1017, %edx
	je	.L25
.L3:
	movl	$2, %r12d
	movabsq	$7453294859427079215, %r13
	xorl	%r14d, %r14d
.L10:
	movl	LogFile(%rip), %edi
	cmpl	$-1, %edi
	je	.L26
.L4:
	movl	connected(%rip), %eax
	testl	%eax, %eax
	jne	.L1
	movq	__libc_errno@gottpoff(%rip), %rbx
	leaq	SyslogAddr(%rip), %rsi
	movl	$110, %edx
	movl	%fs:(%rbx), %ebp
	call	__connect
	cmpl	$-1, %eax
	jne	.L8
	movl	%fs:(%rbx), %r15d
	movl	LogFile(%rip), %edi
	movl	%eax, LogFile(%rip)
	call	__close
	cmpl	$91, %r15d
	movl	%ebp, %fs:(%rbx)
	je	.L27
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%edx, LogFacility(%rip)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	%eax, %eax
	cmpl	$2, LogType(%rip)
	setne	%al
	addl	$1, %eax
	cmpl	$1, %r12d
	movl	%eax, LogType(%rip)
	je	.L1
	movl	$1, %r12d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L26:
	testb	$8, LogStat(%rip)
	pxor	%xmm0, %xmm0
	movl	$1, %edx
	movq	%r13, 2+SyslogAddr(%rip)
	movq	%r14, 10+SyslogAddr(%rip)
	movw	%dx, SyslogAddr(%rip)
	movq	$0, 98+SyslogAddr(%rip)
	movups	%xmm0, 18+SyslogAddr(%rip)
	movl	$0, 106+SyslogAddr(%rip)
	movups	%xmm0, 34+SyslogAddr(%rip)
	movups	%xmm0, 50+SyslogAddr(%rip)
	movups	%xmm0, 66+SyslogAddr(%rip)
	movups	%xmm0, 82+SyslogAddr(%rip)
	je	.L1
	xorl	%edx, %edx
	movl	$1, %edi
	movl	LogType(%rip), %esi
	orl	$524288, %esi
	call	__socket
	cmpl	$-1, %eax
	movl	%eax, %edi
	movl	%eax, LogFile(%rip)
	jne	.L4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, connected(%rip)
	jmp	.L1
.LFE80:
	.size	openlog_internal, .-openlog_internal
	.p2align 4,,15
	.type	closelog_internal.part.0, @function
closelog_internal.part.0:
.LFB85:
	
	subq	$8, %rsp
	movl	LogFile(%rip), %edi
	call	__close
	movl	$-1, LogFile(%rip)
	movl	$0, connected(%rip)
	addq	$8, %rsp
	ret
.LFE85:
	.size	closelog_internal.part.0, .-closelog_internal.part.0
	.p2align 4,,15
	.type	cancel_handler, @function
cancel_handler:
.LFB74:
	
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L31
	movq	(%rdi), %rdi
	call	free@PLT
.L31:
#APP
# 109 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L32
	subl	$1, syslog_lock(%rip)
.L30:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%eax, %eax
#APP
# 109 "../misc/syslog.c" 1
	xchgl %eax, syslog_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L30
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	syslog_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 109 "../misc/syslog.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L30
.LFE74:
	.size	cancel_handler, .-cancel_handler
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"syslog: unknown facility/priority: %x"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"<%d>"
.LC2:
	.string	"%h %e %T "
.LC3:
	.string	"[%d]"
.LC4:
	.string	"\n"
.LC5:
	.string	"/dev/console"
.LC6:
	.string	"%s\r\n"
	.text
	.p2align 4,,15
	.globl	__vsyslog_internal
	.hidden	__vsyslog_internal
	.type	__vsyslog_internal, @function
__vsyslog_internal:
.LFB79:
	
	pushq	%r15
	pushq	%r14
	movl	%ecx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movq	%rdx, %r14
	subq	$200, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	andl	$-1024, %edi
	movq	$0, 32(%rsp)
	movq	$0, 40(%rsp)
	movl	%fs:(%rax), %r12d
	jne	.L91
.L39:
	movq	$0, 48(%rsp)
	movq	$0, 56(%rsp)
#APP
# 184 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L40
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, syslog_lock(%rip)
# 0 "" 2
#NO_APP
.L41:
	movl	LogMask(%rip), %eax
	movl	%ebp, %ecx
	andl	$7, %ecx
	sarl	%cl, %eax
	andl	$1, %eax
	movl	%eax, 12(%rsp)
	je	.L66
	testl	$1016, %ebp
	jne	.L47
	orl	LogFacility(%rip), %ebp
.L47:
	leaq	40(%rsp), %rsi
	leaq	32(%rsp), %rdi
	call	__open_memstream
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L92
	orl	$32768, (%rax)
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdi
	movl	%ebp, %edx
	xorl	%eax, %eax
.LEHB0:
	call	fprintf
	leaq	96(%rsp), %rsi
	movl	$5, %edi
	call	__clock_gettime
	movq	96(%rsp), %rax
	leaq	128(%rsp), %rsi
	leaq	24(%rsp), %rdi
	movq	%rax, 24(%rsp)
	call	__localtime_r
	movq	40(%rbx), %rdi
	movq	48(%rbx), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	leaq	.LC2(%rip), %rdx
	movq	%rax, %rcx
	subq	%rdi, %rsi
	call	__strftime_l
	addq	%rax, 40(%rbx)
	movq	%rbx, %rdi
	call	_IO_ftell
	movq	LogTag(%rip), %rdi
	movq	%rax, %rbp
	testq	%rdi, %rdi
	je	.L93
.L51:
	movq	%rbx, %rsi
	call	__fputs_unlocked
	testb	$1, LogStat(%rip)
	jne	.L53
.L56:
	cmpq	$0, LogTag(%rip)
	je	.L57
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L94
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$58, (%rax)
.L59:
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	jnb	.L95
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	$32, (%rax)
.L57:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	movl	%r12d, %fs:(%rax)
	call	__vfprintf_internal
	movq	%rbx, %rdi
	call	_IO_new_fclose@PLT
	movq	32(%rsp), %rax
	movq	%rax, 48(%rsp)
.L50:
	testb	$32, LogStat(%rip)
	je	.L61
	movq	32(%rsp), %rdx
	leaq	(%rdx,%rbp), %rax
	movq	%rax, 96(%rsp)
	movq	40(%rsp), %rax
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movq	%rcx, 104(%rsp)
	cmpb	$10, -1(%rdx,%rax)
	je	.L62
	leaq	.LC4(%rip), %rax
	movq	$1, 120(%rsp)
	movl	$2, 12(%rsp)
	movq	%rax, 112(%rsp)
.L62:
	movl	12(%rsp), %edx
	leaq	96(%rsp), %rsi
	movl	$2, %edi
	call	__writev
.L61:
	movl	connected(%rip), %eax
	testl	%eax, %eax
	je	.L96
	cmpl	$1, LogType(%rip)
	jne	.L76
.L75:
	addq	$1, 40(%rsp)
.L64:
	testl	%eax, %eax
	jne	.L76
.L65:
	testb	$2, LogStat(%rip)
	je	.L66
	leaq	.LC5(%rip), %rdi
	xorl	%edx, %edx
	movl	$257, %esi
	xorl	%eax, %eax
	call	__open
.LEHE0:
	testl	%eax, %eax
	movl	%eax, %ebx
	jns	.L97
	.p2align 4,,10
	.p2align 3
.L66:
#APP
# 331 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L98
	subl	$1, syslog_lock(%rip)
.L72:
	movq	32(%rsp), %rdi
	leaq	64(%rsp), %rax
	cmpq	%rax, %rdi
	je	.L38
	call	free@PLT
.L38:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	.LC0(%rip), %rsi
	movl	%ebp, %edx
	movl	$35, %edi
	xorl	%eax, %eax
	andl	$1023, %ebp
.LEHB1:
	call	syslog
.LEHE1:
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L76:
	movq	40(%rsp), %rdx
	movq	32(%rsp), %rsi
	movl	$16384, %ecx
	movl	LogFile(%rip), %edi
.LEHB2:
	call	__send
	testq	%rax, %rax
	jns	.L66
	movl	connected(%rip), %ecx
	testl	%ecx, %ecx
	je	.L65
	call	closelog_internal.part.0
	movl	LogStat(%rip), %esi
	movq	LogTag(%rip), %rdi
	xorl	%edx, %edx
	orl	$8, %esi
	call	openlog_internal
	movl	connected(%rip), %eax
	testl	%eax, %eax
	je	.L65
	movq	40(%rsp), %rdx
	movq	32(%rsp), %rsi
	movl	$16384, %ecx
	movl	LogFile(%rip), %edi
	call	__send
	testq	%rax, %rax
	jns	.L66
	movl	connected(%rip), %edx
	testl	%edx, %edx
	je	.L65
	call	closelog_internal.part.0
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L92:
	movdqa	.LC7(%rip), %xmm0
	movaps	%xmm0, 64(%rsp)
	call	__getpid
	leaq	128(%rsp), %r8
	movl	%eax, %ecx
	movl	$1717986919, %edi
	leaq	12(%r8), %rsi
	.p2align 4,,10
	.p2align 3
.L49:
	movl	%ecx, %eax
	subq	$1, %rsi
	imull	%edi
	movl	%ecx, %eax
	sarl	$31, %eax
	sarl	$2, %edx
	subl	%eax, %edx
	leal	(%rdx,%rdx,4), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
	addl	$48, %ecx
	testl	%edx, %edx
	movb	%cl, (%rsi)
	movl	%edx, %ecx
	jne	.L49
	leaq	12(%r8), %rdx
	leaq	79(%rsp), %rdi
	leaq	64(%rsp), %rbx
	xorl	%ebp, %ebp
	subq	%rsi, %rdx
	call	__mempcpy@PLT
	movl	$93, %esi
	addq	$1, %rax
	movq	%rbx, 32(%rsp)
	movw	%si, -1(%rax)
	subq	%rbx, %rax
	movq	%rax, 40(%rsp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L93:
	movq	__progname(%rip), %rdi
	testq	%rdi, %rdi
	movq	%rdi, LogTag(%rip)
	jne	.L51
	testb	$1, LogStat(%rip)
	je	.L57
.L53:
	call	__getpid
	leaq	.LC3(%rip), %rsi
	movl	%eax, %edx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L96:
	movl	LogStat(%rip), %esi
	movq	LogTag(%rip), %rdi
	xorl	%edx, %edx
	orl	$8, %esi
	call	openlog_internal
	cmpl	$1, LogType(%rip)
	movl	connected(%rip), %eax
	jne	.L64
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, syslog_lock(%rip)
	je	.L41
	leaq	syslog_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
#APP
# 331 "../misc/syslog.c" 1
	xchgl %eax, syslog_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L72
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	syslog_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 331 "../misc/syslog.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L97:
	movq	32(%rsp), %rdx
	leaq	.LC6(%rip), %rsi
	movl	%eax, %edi
	xorl	%eax, %eax
	addq	%rbp, %rdx
	call	__dprintf
	movl	%ebx, %edi
	call	__close
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$32, %esi
	movq	%rbx, %rdi
	call	__overflow
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	__overflow
.LEHE2:
	jmp	.L59
.L77:
	leaq	48(%rsp), %rdi
	movq	%rax, %rbx
	call	cancel_handler
	movq	%rbx, %rdi
.LEHB3:
	call	_Unwind_Resume@PLT
.LEHE3:
.LFE79:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA79:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE79-.LLSDACSB79
.LLSDACSB79:
	.uleb128 .LEHB0-.LFB79
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L77-.LFB79
	.uleb128 0
	.uleb128 .LEHB1-.LFB79
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB2-.LFB79
	.uleb128 .LEHE2-.LEHB2
	.uleb128 .L77-.LFB79
	.uleb128 0
	.uleb128 .LEHB3-.LFB79
	.uleb128 .LEHE3-.LEHB3
	.uleb128 0
	.uleb128 0
.LLSDACSE79:
	.text
	.size	__vsyslog_internal, .-__vsyslog_internal
	.p2align 4,,15
	.globl	__syslog
	.type	__syslog, @function
__syslog:
.LFB75:
	
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L101
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L101:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vsyslog_internal
	addq	$216, %rsp
	ret
.LFE75:
	.size	__syslog, .-__syslog
	.globl	syslog
	.hidden	syslog
	.set	syslog,__syslog
	.p2align 4,,15
	.globl	__vsyslog
	.type	__vsyslog, @function
__vsyslog:
.LFB76:
	
	xorl	%ecx, %ecx
	jmp	__vsyslog_internal
.LFE76:
	.size	__vsyslog, .-__vsyslog
	.weak	vsyslog
	.set	vsyslog,__vsyslog
	.p2align 4,,15
	.globl	__syslog_chk
	.type	__syslog_chk, @function
__syslog_chk:
.LFB77:
	
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, %r10
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L108
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L108:
	leaq	224(%rsp), %rax
	xorl	%ecx, %ecx
	testl	%esi, %esi
	setg	%cl
	leaq	8(%rsp), %rdx
	movq	%r10, %rsi
	addl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$24, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vsyslog_internal
	addq	$216, %rsp
	ret
.LFE77:
	.size	__syslog_chk, .-__syslog_chk
	.p2align 4,,15
	.globl	__vsyslog_chk
	.type	__vsyslog_chk, @function
__vsyslog_chk:
.LFB78:
	
	movq	%rdx, %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	testl	%esi, %esi
	movq	%rax, %rsi
	setg	%cl
	addl	%ecx, %ecx
	jmp	__vsyslog_internal
.LFE78:
	.size	__vsyslog_chk, .-__vsyslog_chk
	.p2align 4,,15
	.globl	openlog
	.type	openlog, @function
openlog:
.LFB81:
	
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 392 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L114
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, syslog_lock(%rip)
# 0 "" 2
#NO_APP
.L115:
	movl	%r12d, %edx
	movl	%ebp, %esi
	movq	%rbx, %rdi
.LEHB4:
	call	openlog_internal
	popq	%rbx
	popq	%rbp
	popq	%r12
	xorl	%edi, %edi
	jmp	cancel_handler
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, syslog_lock(%rip)
	je	.L115
	leaq	syslog_lock(%rip), %rdi
	call	__lll_lock_wait_private
.LEHE4:
	jmp	.L115
.L119:
	movq	%rax, %rbx
	xorl	%edi, %edi
	call	cancel_handler
	movq	%rbx, %rdi
.LEHB5:
	call	_Unwind_Resume@PLT
.LEHE5:
.LFE81:
	.section	.gcc_except_table
.LLSDA81:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE81-.LLSDACSB81
.LLSDACSB81:
	.uleb128 .LEHB4-.LFB81
	.uleb128 .LEHE4-.LEHB4
	.uleb128 .L119-.LFB81
	.uleb128 0
	.uleb128 .LEHB5-.LFB81
	.uleb128 .LEHE5-.LEHB5
	.uleb128 0
	.uleb128 0
.LLSDACSE81:
	.text
	.size	openlog, .-openlog
	.p2align 4,,15
	.globl	closelog
	.type	closelog, @function
closelog:
.LFB83:
	
	pushq	%rbx
#APP
# 423 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L122
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, syslog_lock(%rip)
# 0 "" 2
#NO_APP
.L123:
	movl	connected(%rip), %eax
	testl	%eax, %eax
	je	.L127
.LEHB6:
	call	closelog_internal.part.0
.L127:
	popq	%rbx
	xorl	%edi, %edi
	movq	$0, LogTag(%rip)
	movl	$2, LogType(%rip)
	jmp	cancel_handler
	.p2align 4,,10
	.p2align 3
.L122:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, syslog_lock(%rip)
	je	.L123
	leaq	syslog_lock(%rip), %rdi
	call	__lll_lock_wait_private
.LEHE6:
	jmp	.L123
.L129:
	movq	%rax, %rbx
	xorl	%edi, %edi
	call	cancel_handler
	movq	%rbx, %rdi
.LEHB7:
	call	_Unwind_Resume@PLT
.LEHE7:
.LFE83:
	.section	.gcc_except_table
.LLSDA83:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE83-.LLSDACSB83
.LLSDACSB83:
	.uleb128 .LEHB6-.LFB83
	.uleb128 .LEHE6-.LEHB6
	.uleb128 .L129-.LFB83
	.uleb128 0
	.uleb128 .LEHB7-.LFB83
	.uleb128 .LEHE7-.LEHB7
	.uleb128 0
	.uleb128 0
.LLSDACSE83:
	.text
	.size	closelog, .-closelog
	.p2align 4,,15
	.globl	setlogmask
	.type	setlogmask, @function
setlogmask:
.LFB84:
	
	pushq	%rbx
	movl	%edi, %ebx
#APP
# 440 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L132
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, syslog_lock(%rip)
# 0 "" 2
#NO_APP
.L133:
	testl	%ebx, %ebx
	movl	LogMask(%rip), %r8d
	je	.L134
	movl	%ebx, LogMask(%rip)
.L134:
#APP
# 446 "../misc/syslog.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L135
	subl	$1, syslog_lock(%rip)
.L131:
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, syslog_lock(%rip)
	je	.L133
	leaq	syslog_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L135:
	xorl	%eax, %eax
#APP
# 446 "../misc/syslog.c" 1
	xchgl %eax, syslog_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L131
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	syslog_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 446 "../misc/syslog.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L131
.LFE84:
	.size	setlogmask, .-setlogmask
	.local	SyslogAddr
	.comm	SyslogAddr,110,32
	.local	syslog_lock
	.comm	syslog_lock,4,4
	.data
	.align 4
	.type	LogMask, @object
	.size	LogMask, 4
LogMask:
	.long	255
	.align 4
	.type	LogFacility, @object
	.size	LogFacility, 4
LogFacility:
	.long	8
	.local	LogTag
	.comm	LogTag,8,8
	.local	LogStat
	.comm	LogStat,4,4
	.local	connected
	.comm	connected,4,4
	.align 4
	.type	LogFile, @object
	.size	LogFile, 4
LogFile:
	.long	-1
	.align 4
	.type	LogType, @object
	.size	LogType, 4
LogType:
	.long	2
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC7:
	.quad	7863397576860792175
	.quad	25649928863706469
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__overflow
	.hidden	__dprintf
	.hidden	__lll_lock_wait_private
	.hidden	__getpid
	.hidden	__send
	.hidden	__open
	.hidden	__writev
	.hidden	__vfprintf_internal
	.hidden	__fputs_unlocked
	.hidden	_IO_ftell
	.hidden	__strftime_l
	.hidden	_nl_C_locobj
	.hidden	__localtime_r
	.hidden	__clock_gettime
	.hidden	fprintf
	.hidden	__open_memstream
	.hidden	__socket
	.hidden	__close
	.hidden	__connect
