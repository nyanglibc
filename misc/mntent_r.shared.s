	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	decode_name, @function
decode_name:
	movq	%rdi, %rax
	movq	%rdi, %rcx
	movq	%rdi, %rdx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L2:
	movb	%sil, -1(%rcx)
	movq	%rdx, %rsi
.L5:
	cmpb	$0, (%rsi)
	leaq	1(%rsi), %rdx
	je	.L13
.L8:
	movzbl	(%rdx), %esi
	addq	$1, %rcx
	cmpb	$92, %sil
	jne	.L2
	movzbl	1(%rdx), %r8d
	cmpb	$48, %r8b
	je	.L14
	cmpb	$92, %r8b
	jne	.L7
	leaq	1(%rdx), %rsi
	movb	$92, -1(%rcx)
	cmpb	$0, (%rsi)
	leaq	1(%rsi), %rdx
	jne	.L8
.L13:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	$49, %r8b
	jne	.L2
	cmpb	$51, 2(%rdx)
	jne	.L2
	cmpb	$52, 3(%rdx)
	jne	.L2
	movb	$92, -1(%rcx)
	leaq	3(%rdx), %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
	movzbl	2(%rdx), %edi
	cmpb	$52, %dil
	je	.L15
	cmpb	$49, %dil
	jne	.L2
	cmpb	$49, 3(%rdx)
	je	.L16
	cmpb	$50, 3(%rdx)
	jne	.L2
	movb	$10, -1(%rcx)
	leaq	3(%rdx), %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L15:
	cmpb	$48, 3(%rdx)
	jne	.L2
	movb	$32, -1(%rcx)
	leaq	3(%rdx), %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	movb	$9, -1(%rcx)
	leaq	3(%rdx), %rsi
	jmp	.L5
	.size	decode_name, .-decode_name
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	" \t"
.LC2:
	.string	" %d %d "
	.text
	.p2align 4,,15
	.type	get_mnt_entry, @function
get_mnt_entry:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	%ecx, %r13d
	movq	%rdx, %rbx
	subq	$1040, %rsp
	leaq	16(%rsp), %rbp
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%r12, %rdx
	movl	%r13d, %esi
	movq	%rbx, %rdi
	call	__GI___fgets_unlocked
	testq	%rax, %rax
	je	.L36
	movl	$10, %esi
	movq	%rbx, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L19
	cmpq	%rax, %rbx
	je	.L20
	movzbl	-1(%rax), %edx
	cmpb	$9, %dl
	je	.L58
	cmpb	$32, %dl
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L58:
	subq	$1, %rax
	cmpq	%rax, %rbx
	je	.L37
	movzbl	-1(%rax), %edx
	cmpb	$32, %dl
	je	.L58
	cmpb	$9, %dl
	je	.L58
.L20:
	movb	$0, (%rax)
.L23:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI_strspn
	addq	%rbx, %rax
	movq	%rax, 8(%rsp)
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L59
	cmpb	$35, %al
	je	.L59
	leaq	8(%rsp), %rbx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI___strsep
	testq	%rax, %rax
	leaq	.LC0(%rip), %rdx
	je	.L27
	movq	%rax, %rdi
	call	decode_name
	movq	%rax, %rdx
.L27:
	movq	8(%rsp), %rbp
	movq	%rdx, (%r14)
	testq	%rbp, %rbp
	je	.L28
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__GI_strspn
	addq	%rax, %rbp
	movq	%rbp, 8(%rsp)
.L28:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI___strsep
	testq	%rax, %rax
	leaq	.LC0(%rip), %rdx
	je	.L29
	movq	%rax, %rdi
	call	decode_name
	movq	%rax, %rdx
.L29:
	movq	8(%rsp), %rbp
	movq	%rdx, 8(%r14)
	testq	%rbp, %rbp
	je	.L30
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__GI_strspn
	addq	%rax, %rbp
	movq	%rbp, 8(%rsp)
.L30:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI___strsep
	testq	%rax, %rax
	leaq	.LC0(%rip), %rdx
	je	.L31
	movq	%rax, %rdi
	call	decode_name
	movq	%rax, %rdx
.L31:
	movq	8(%rsp), %rbp
	movq	%rdx, 16(%r14)
	testq	%rbp, %rbp
	je	.L32
	leaq	.LC1(%rip), %rsi
	movq	%rbp, %rdi
	call	__GI_strspn
	addq	%rax, %rbp
	movq	%rbp, 8(%rsp)
.L32:
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI___strsep
	testq	%rax, %rax
	leaq	.LC0(%rip), %rdx
	je	.L33
	movq	%rax, %rdi
	call	decode_name
	movq	%rax, %rdx
.L33:
	movq	8(%rsp), %rdi
	movq	%rdx, 24(%r14)
	testq	%rdi, %rdi
	je	.L34
	leaq	36(%r14), %rcx
	leaq	32(%r14), %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	call	__GI___isoc99_sscanf
	testl	%eax, %eax
	je	.L34
	cmpl	$1, %eax
	je	.L35
	movl	$1, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$10, %esi
	movq	%rbp, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L23
.L19:
	movq	%r12, %rdx
	movl	$1024, %esi
	movq	%rbp, %rdi
	call	__GI___fgets_unlocked
	testq	%rax, %rax
	jne	.L25
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbx, %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
.L17:
	addq	$1040, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$0, 32(%r14)
.L35:
	movl	$0, 36(%r14)
	addq	$1040, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	get_mnt_entry, .-get_mnt_entry
	.section	.rodata.str1.1
.LC3:
	.string	" \t\n\\"
	.text
	.p2align 4,,15
	.type	write_string, @function
write_string:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L74:
	addq	$1, %r15
	movzbl	-1(%r15), %r13d
	movq	40(%rbx), %r14
	movq	48(%rbx), %r12
	testb	%r13b, %r13b
	je	.L88
	movsbl	%r13b, %ebp
	leaq	.LC3(%rip), %rdi
	movl	%ebp, %esi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L89
	cmpq	%r14, %r12
	jbe	.L90
	leaq	1(%r14), %rax
	movq	%rax, 40(%rbx)
	movb	$92, (%r14)
.L79:
	movl	%ebp, %esi
	movq	40(%rbx), %rax
	sarl	$6, %esi
	andl	$3, %esi
	addl	$48, %esi
	cmpq	48(%rbx), %rax
	jnb	.L91
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%sil, (%rax)
.L81:
	sarl	$3, %ebp
	movq	40(%rbx), %rax
	andl	$7, %ebp
	cmpq	48(%rbx), %rax
	leal	48(%rbp), %esi
	jnb	.L92
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%sil, (%rax)
.L83:
	andl	$7, %r13d
	movq	40(%rbx), %rax
	cmpq	48(%rbx), %rax
	leal	48(%r13), %esi
	jnb	.L93
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rbx)
	movb	%sil, (%rax)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L89:
	cmpq	%r14, %r12
	jbe	.L94
	leaq	1(%r14), %rax
	movq	%rax, 40(%rbx)
	movb	%r13b, (%r14)
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L88:
	cmpq	%r14, %r12
	jbe	.L95
	leaq	1(%r14), %rax
	movq	%rax, 40(%rbx)
	movb	$32, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movl	$92, %esi
	movq	%rbx, %rdi
	call	__GI___overflow
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rbx, %rdi
	call	__GI___overflow
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rbx, %rdi
	call	__GI___overflow
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%rbx, %rdi
	call	__GI___overflow
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L94:
	movzbl	%r13b, %esi
	movq	%rbx, %rdi
	call	__GI___overflow
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$8, %rsp
	movq	%rbx, %rdi
	movl	$32, %esi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__GI___overflow
	.size	write_string, .-write_string
	.p2align 4,,15
	.globl	__GI___setmntent
	.hidden	__GI___setmntent
	.type	__GI___setmntent, @function
__GI___setmntent:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	call	__GI_strlen
	leaq	18(%rax), %rcx
	movq	%rax, %rdx
	movq	%rbx, %rsi
	andq	$-16, %rcx
	subq	%rcx, %rsp
	movq	%rsp, %rdi
	call	__GI_mempcpy
	movl	$25955, %edx
	movb	$0, 2(%rax)
	movq	%rsp, %rsi
	movw	%dx, (%rax)
	movq	%r12, %rdi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	je	.L96
	orl	$32768, (%rax)
.L96:
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	ret
	.size	__GI___setmntent, .-__GI___setmntent
	.globl	__setmntent
	.set	__setmntent,__GI___setmntent
	.weak	setmntent
	.set	setmntent,__setmntent
	.p2align 4,,15
	.globl	__GI___endmntent
	.hidden	__GI___endmntent
	.type	__GI___endmntent, @function
__GI___endmntent:
	testq	%rdi, %rdi
	je	.L108
	subq	$8, %rsp
	call	_IO_new_fclose@PLT
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movl	$1, %eax
	ret
	.size	__GI___endmntent, .-__GI___endmntent
	.globl	__endmntent
	.set	__endmntent,__GI___endmntent
	.weak	endmntent
	.set	endmntent,__endmntent
	.section	.rodata.str1.1
.LC4:
	.string	"%d %d\n"
	.text
	.p2align 4,,15
	.globl	__addmntent
	.type	__addmntent, @function
__addmntent:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$2, %edx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__GI_fseek
	testl	%eax, %eax
	je	.L112
.L114:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movq	0(%rbp), %rsi
	movq	%rbx, %rdi
	call	write_string
	movq	8(%rbp), %rsi
	movq	%rbx, %rdi
	call	write_string
	movq	16(%rbp), %rsi
	movq	%rbx, %rdi
	call	write_string
	movq	24(%rbp), %rsi
	movq	%rbx, %rdi
	call	write_string
	movl	36(%rbp), %ecx
	movl	32(%rbp), %edx
	leaq	.LC4(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	__GI_fprintf
	testb	$32, (%rbx)
	jne	.L114
	movq	%rbx, %rdi
	call	__GI_fflush
	testl	%eax, %eax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__addmntent, .-__addmntent
	.weak	addmntent
	.set	addmntent,__addmntent
	.p2align 4,,15
	.globl	__GI___hasmntopt
	.hidden	__GI___hasmntopt
	.type	__GI___hasmntopt, @function
__GI___hasmntopt:
	pushq	%r13
	pushq	%r12
	movabsq	$2305860601399738369, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	__GI_strlen
	movq	24(%rbx), %rbx
	movq	%rax, %r13
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	$44, -1(%rax)
	je	.L118
.L119:
	movl	$44, %esi
	movq	%rax, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L116
	leaq	1(%rax), %rbx
.L117:
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_strstr
	testq	%rax, %rax
	je	.L116
	cmpq	%rax, %rbx
	jne	.L129
.L118:
	movzbl	(%rax,%r13), %edx
	cmpb	$61, %dl
	ja	.L119
	btq	%rdx, %r12
	jnc	.L119
.L116:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___hasmntopt, .-__GI___hasmntopt
	.globl	__hasmntopt
	.set	__hasmntopt,__GI___hasmntopt
	.weak	hasmntopt
	.set	hasmntopt,__hasmntopt
	.section	.rodata.str1.1
.LC5:
	.string	"autofs"
.LC6:
	.string	"ignore"
	.text
	.p2align 4,,15
	.globl	__GI___getmntent_r
	.hidden	__GI___getmntent_r
	.type	__GI___getmntent_r, @function
__GI___getmntent_r:
	pushq	%r14
	leaq	.LC5(%rip), %r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movl	%ecx, %ebp
	pushq	%rbx
	movq	%rsi, %rbx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L138:
	movq	16(%rbx), %rsi
	movl	$7, %ecx
	movq	%r14, %rdi
	repz cmpsb
	jne	.L131
	leaq	.LC6(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI___hasmntopt
	testq	%rax, %rax
	je	.L131
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rbx)
	movups	%xmm0, (%rbx)
	movups	%xmm0, 16(%rbx)
.L132:
	movl	%ebp, %ecx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	get_mnt_entry
	testb	%al, %al
	jne	.L138
	xorl	%ebx, %ebx
.L131:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI___getmntent_r, .-__GI___getmntent_r
	.globl	__getmntent_r
	.set	__getmntent_r,__GI___getmntent_r
	.weak	getmntent_r
	.set	getmntent_r,__getmntent_r
