	.text
	.p2align 4,,15
	.globl	ualarm
	.type	ualarm, @function
ualarm:
	subq	$72, %rsp
	movl	%edi, %eax
	xorl	%edi, %edi
	leaq	32(%rsp), %rdx
	movq	%rax, 24(%rsp)
	movl	%esi, %eax
	movq	%rsp, %rsi
	movq	$0, 16(%rsp)
	movq	$0, (%rsp)
	movq	%rax, 8(%rsp)
	call	__setitimer
	testl	%eax, %eax
	movl	$-1, %edx
	js	.L1
	imull	$1000000, 48(%rsp), %edx
	addl	56(%rsp), %edx
.L1:
	movl	%edx, %eax
	addq	$72, %rsp
	ret
	.size	ualarm, .-ualarm
	.hidden	__setitimer
