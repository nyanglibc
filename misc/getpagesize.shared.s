	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/getpagesize.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"GLRO(dl_pagesize) != 0"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___getpagesize
	.hidden	__GI___getpagesize
	.type	__GI___getpagesize, @function
__GI___getpagesize:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L7
	rep ret
.L7:
	leaq	__PRETTY_FUNCTION__.10071(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$28, %edx
	call	__GI___assert_fail
	.size	__GI___getpagesize, .-__GI___getpagesize
	.globl	__getpagesize
	.set	__getpagesize,__GI___getpagesize
	.weak	getpagesize
	.set	getpagesize,__getpagesize
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10071, @object
	.size	__PRETTY_FUNCTION__.10071, 14
__PRETTY_FUNCTION__.10071:
	.string	"__getpagesize"
