	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/hostid"
#NO_APP
	.text
	.p2align 4,,15
	.globl	sethostid
	.type	sethostid, @function
sethostid:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	__libc_enable_secure@GOTPCREL(%rip), %rax
	movl	%edi, 12(%rsp)
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L8
	movslq	%edi, %rax
	cmpq	%rdi, %rax
	jne	.L9
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movl	$420, %edx
	movl	$577, %esi
	call	__GI___open_nocancel
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L5
	leaq	12(%rsp), %rsi
	movl	$4, %edx
	movl	%eax, %edi
	call	__GI___write_nocancel
	movl	%ebx, %edi
	movq	%rax, %rbp
	call	__GI___close_nocancel
	xorl	%eax, %eax
	cmpq	$4, %rbp
	setne	%al
	negl	%eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.size	sethostid, .-sethostid
