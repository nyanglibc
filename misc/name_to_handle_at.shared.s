.text
 .globl name_to_handle_at
 .type name_to_handle_at,@function
 .align 1<<4
 name_to_handle_at: 
 
 movq %rcx, %r10
 movl $303, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size name_to_handle_at,.-name_to_handle_at
.globl __GI_name_to_handle_at 
 .set __GI_name_to_handle_at,name_to_handle_at
