.text
 .globl pkey_alloc
 .type pkey_alloc,@function
 .align 1<<4
 pkey_alloc: 
 
 movl $330, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size pkey_alloc,.-pkey_alloc
.globl __GI_pkey_alloc 
 .set __GI_pkey_alloc,pkey_alloc
