.text
 .globl inotify_init
 .type inotify_init,@function
 .align 1<<4
 inotify_init: 
 
 movl $253, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size inotify_init,.-inotify_init
