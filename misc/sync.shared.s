.text
 .globl sync
 .type sync,@function
 .align 1<<4
 sync: 
 
 movl $162, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size sync,.-sync
.globl __GI_sync 
 .set __GI_sync,sync
