	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/"
.LC1:
	.string	"/dev/null"
	.text
	.p2align 4,,15
	.globl	daemon
	.type	daemon, @function
daemon:
	pushq	%r12
	pushq	%rbp
	movl	%edi, %r12d
	pushq	%rbx
	movl	%esi, %ebp
	subq	$144, %rsp
	call	__fork
	cmpl	$-1, %eax
	movl	%eax, %ebx
	je	.L1
	testl	%eax, %eax
	jne	.L19
	call	__setsid
	cmpl	$-1, %eax
	je	.L10
	testl	%r12d, %r12d
	je	.L20
.L5:
	testl	%ebp, %ebp
	je	.L21
.L1:
	addq	$144, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%edi, %edi
	call	_exit
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC1(%rip), %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$2, %esi
	call	__open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %ebp
	je	.L6
	movq	%rsp, %rsi
	movl	%eax, %edi
	call	__fstat64
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L6
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$8192, %eax
	jne	.L7
	cmpq	$259, 40(%rsp)
	jne	.L7
	xorl	%esi, %esi
	movl	%ebp, %edi
	call	__dup2
	movl	$1, %esi
	movl	%ebp, %edi
	call	__dup2
	movl	$2, %esi
	movl	%ebp, %edi
	call	__dup2
	cmpl	$2, %ebp
	jle	.L1
	movl	%ebp, %edi
	call	__close
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC0(%rip), %rdi
	call	__chdir
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%ebp, %edi
	call	__close_nocancel
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$19, %fs:(%rax)
.L9:
	movl	$-1, %ebx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ebp, %edi
	call	__close_nocancel
	jmp	.L9
.L10:
	movl	%eax, %ebx
	jmp	.L1
	.size	daemon, .-daemon
	.hidden	__close_nocancel
	.hidden	__chdir
	.hidden	__close
	.hidden	__dup2
	.hidden	__fstat64
	.hidden	__open_nocancel
	.hidden	_exit
	.hidden	__setsid
	.hidden	__fork
