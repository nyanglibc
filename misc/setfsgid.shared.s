.text
 .globl setfsgid
 .type setfsgid,@function
 .align 1<<4
 setfsgid: 
 
 movl $123, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size setfsgid,.-setfsgid
.globl __GI_setfsgid 
 .set __GI_setfsgid,setfsgid
