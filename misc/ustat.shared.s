	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __old_ustat,ustat@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__old_ustat
	.type	__old_ustat, @function
__old_ustat:
	movl	%edi, %eax
	cmpq	%rax, %rdi
	jne	.L7
	movl	$136, %eax
#APP
# 51 "../sysdeps/unix/sysv/linux/ustat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__old_ustat, .-__old_ustat
