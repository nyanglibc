	.text
	.p2align 4,,15
	.globl	__fxstatat64
	.type	__fxstatat64, @function
__fxstatat64:
	cmpl	$1, %edi
	movl	%esi, %eax
	movq	%rdx, %rsi
	ja	.L2
	movl	%eax, %edi
	movl	%r8d, %r10d
	movq	%rcx, %rdx
	movl	$262, %eax
#APP
# 41 "../sysdeps/unix/sysv/linux/fxstatat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__fxstatat64, .-__fxstatat64
	.globl	__fxstatat
	.set	__fxstatat,__fxstatat64
