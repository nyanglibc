	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"w+ce"
.LC1:
	.string	"/dev/tty"
.LC2:
	.string	"%s"
.LC3:
	.string	"\n"
	.text
	.p2align 4,,15
	.globl	getpass
	.type	getpass, @function
getpass:
.LFB68:
	
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rsi
	pushq	%rbp
	pushq	%rbx
	leaq	.LC1(%rip), %rdi
	subq	$136, %rsp
.LEHB0:
	call	_IO_new_fopen@PLT
.LEHE0:
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L27
	orl	$32768, (%rax)
	movq	%rax, %r12
	movq	%rax, %rbx
.L3:
	leaq	64(%rsp), %r14
	movq	%rbx, %rdi
	call	fileno
	movq	%r14, %rsi
	movl	%eax, %edi
.LEHB1:
	call	__tcgetattr
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L28
.L4:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__fxprintf
	movq	%r12, %rdi
	call	__fflush_unlocked
	leaq	bufsize.10259(%rip), %rsi
	leaq	buf.10258(%rip), %rdi
	movq	%rbx, %rdx
	call	__getline
.LEHE1:
	movq	buf.10258(%rip), %rdx
	testq	%rdx, %rdx
	je	.L6
	testq	%rax, %rax
	js	.L29
	leaq	-1(%rdx,%rax), %rax
	cmpb	$10, (%rax)
	je	.L30
.L6:
	testl	%r13d, %r13d
	je	.L10
	movq	%rbx, %rdi
	call	fileno
	movq	%rsp, %rdx
	movl	$2, %esi
	movl	%eax, %edi
	call	tcsetattr
.L10:
	cmpq	%rbx, stdin(%rip)
	je	.L12
	movq	%rbx, %rdi
.LEHB2:
	call	_IO_new_fclose@PLT
.LEHE2:
.L12:
	movq	buf.10258(%rip), %rax
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movb	$0, (%rdx)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L28:
	movdqa	64(%rsp), %xmm0
	movq	%rbx, %rdi
	movq	112(%rsp), %rax
	andl	$-10, 76(%rsp)
	xorl	%r13d, %r13d
	movaps	%xmm0, (%rsp)
	movq	%rax, 48(%rsp)
	movdqa	80(%rsp), %xmm0
	movl	120(%rsp), %eax
	movaps	%xmm0, 16(%rsp)
	movdqa	96(%rsp), %xmm0
	movl	%eax, 56(%rsp)
	movaps	%xmm0, 32(%rsp)
	call	fileno
	movq	%r14, %rdx
	movl	$2, %esi
	movl	%eax, %edi
	call	tcsetattr
	testl	%eax, %eax
	sete	%r13b
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L27:
	movq	stdin(%rip), %rbx
	movq	stderr(%rip), %r12
	cmpq	%r12, %rbx
	cmove	%rbx, %rbp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L30:
	testl	%r13d, %r13d
	movb	$0, (%rax)
	je	.L10
	leaq	.LC3(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
.LEHB3:
	call	__fxprintf
.LEHE3:
	jmp	.L6
.L17:
	testq	%rbp, %rbp
	movq	%rax, %rbx
	je	.L14
	movq	%rbp, %rdi
.LEHB4:
	call	_IO_new_fclose@PLT
.L14:
	movq	%rbx, %rdi
	call	_Unwind_Resume@PLT
.LEHE4:
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L17-.LFB68
	.uleb128 0
	.uleb128 .LEHB2-.LFB68
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB3-.LFB68
	.uleb128 .LEHE3-.LEHB3
	.uleb128 .L17-.LFB68
	.uleb128 0
	.uleb128 .LEHB4-.LFB68
	.uleb128 .LEHE4-.LEHB4
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	getpass, .-getpass
	.local	bufsize.10259
	.comm	bufsize.10259,8,8
	.local	buf.10258
	.comm	buf.10258,8,8
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	tcsetattr
	.hidden	__getline
	.hidden	__fflush_unlocked
	.hidden	__fxprintf
	.hidden	__tcgetattr
	.hidden	fileno
