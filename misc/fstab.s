	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rq"
.LC1:
	.string	"rw"
.LC2:
	.string	"ro"
.LC3:
	.string	"sw"
.LC4:
	.string	"??"
.LC5:
	.string	"xx"
	.text
	.p2align 4,,15
	.type	fstab_convert, @function
fstab_convert:
	pushq	%r12
	pushq	%rbp
	leaq	16(%rdi), %rbp
	pushq	%rbx
	movq	16(%rdi), %rax
	leaq	.LC1(%rip), %rsi
	movq	%rdi, %rbx
	leaq	56(%rdi), %r12
	movq	%rax, 56(%rdi)
	movq	24(%rdi), %rax
	movq	%rax, 64(%rdi)
	movq	32(%rdi), %rax
	movq	%rax, 72(%rdi)
	movq	40(%rdi), %rax
	movq	%rax, 80(%rdi)
	movq	%rbp, %rdi
	call	__hasmntopt
	testq	%rax, %rax
	leaq	.LC1(%rip), %rdx
	je	.L10
.L2:
	movl	48(%rbx), %eax
	movq	%rdx, 88(%rbx)
	movl	%eax, 96(%rbx)
	movl	52(%rbx), %eax
	movl	%eax, 100(%rbx)
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC0(%rip), %rsi
	movq	%rbp, %rdi
	call	__hasmntopt
	testq	%rax, %rax
	leaq	.LC0(%rip), %rdx
	jne	.L2
	leaq	.LC2(%rip), %rsi
	movq	%rbp, %rdi
	call	__hasmntopt
	testq	%rax, %rax
	leaq	.LC2(%rip), %rdx
	jne	.L2
	leaq	.LC3(%rip), %rsi
	movq	%rbp, %rdi
	call	__hasmntopt
	testq	%rax, %rax
	leaq	.LC3(%rip), %rdx
	jne	.L2
	leaq	.LC5(%rip), %rsi
	movq	%rbp, %rdi
	call	__hasmntopt
	leaq	.LC5(%rip), %rdx
	testq	%rax, %rax
	leaq	.LC4(%rip), %rax
	cmove	%rax, %rdx
	jmp	.L2
	.size	fstab_convert, .-fstab_convert
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	fstab_free, @function
fstab_free:
	movq	8+fstab_state(%rip), %rdi
	jmp	free@PLT
	.size	fstab_free, .-fstab_free
	.section	.rodata.str1.1
.LC6:
	.string	"r"
.LC7:
	.string	"/etc/fstab"
	.text
	.p2align 4,,15
	.type	fstab_init, @function
fstab_init:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	cmpq	$0, 8+fstab_state(%rip)
	je	.L24
	movq	fstab_state(%rip), %rdi
	testq	%rdi, %rdi
	je	.L16
.L26:
	testl	%ebx, %ebx
	leaq	fstab_state(%rip), %rax
	jne	.L25
.L12:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rax, 8(%rsp)
	call	rewind
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$8128, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L17
	movq	fstab_state(%rip), %rdi
	movq	%rax, 8+fstab_state(%rip)
	testq	%rdi, %rdi
	jne	.L26
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	call	__setmntent
	testq	%rax, %rax
	je	.L17
	movq	%rax, fstab_state(%rip)
	addq	$16, %rsp
	leaq	fstab_state(%rip), %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
	jmp	.L12
	.size	fstab_init, .-fstab_init
	.p2align 4,,15
	.globl	setfsent
	.type	setfsent, @function
setfsent:
	subq	$8, %rsp
	movl	$1, %edi
	call	fstab_init
	testq	%rax, %rax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	setfsent, .-setfsent
	.p2align 4,,15
	.globl	getfsent
	.type	getfsent, @function
getfsent:
	pushq	%rbx
	xorl	%edi, %edi
	call	fstab_init
	testq	%rax, %rax
	je	.L31
	movq	8(%rax), %rdx
	movq	(%rax), %rdi
	leaq	16(%rax), %rsi
	movl	$8128, %ecx
	movq	%rax, %rbx
	call	__getmntent_r
	testq	%rax, %rax
	je	.L31
	movq	%rbx, %rdi
	popq	%rbx
	jmp	fstab_convert
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	getfsent, .-getfsent
	.p2align 4,,15
	.globl	getfsspec
	.type	getfsspec, @function
getfsspec:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	$1, %edi
	call	fstab_init
	testq	%rax, %rax
	movq	%rax, %rbx
	leaq	16(%rax), %rbp
	jne	.L40
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rax), %rdi
	movq	%r12, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L47
.L40:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	movl	$8128, %ecx
	movq	%rbp, %rsi
	call	__getmntent_r
	testq	%rax, %rax
	jne	.L41
.L39:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	fstab_convert
	.size	getfsspec, .-getfsspec
	.p2align 4,,15
	.globl	getfsfile
	.type	getfsfile, @function
getfsfile:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	$1, %edi
	call	fstab_init
	testq	%rax, %rax
	movq	%rax, %rbx
	leaq	16(%rax), %rbp
	jne	.L51
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%rax), %rdi
	movq	%r12, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L58
.L51:
	movq	8(%rbx), %rdx
	movq	(%rbx), %rdi
	movl	$8128, %ecx
	movq	%rbp, %rsi
	call	__getmntent_r
	testq	%rax, %rax
	jne	.L52
.L50:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	fstab_convert
	.size	getfsfile, .-getfsfile
	.p2align 4,,15
	.globl	endfsent
	.type	endfsent, @function
endfsent:
	movq	fstab_state(%rip), %rdi
	testq	%rdi, %rdi
	je	.L65
	subq	$8, %rsp
	call	__endmntent
	movq	$0, fstab_state(%rip)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	rep ret
	.size	endfsent, .-endfsent
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_fstab_free__, @object
	.size	__elf_set___libc_subfreeres_element_fstab_free__, 8
__elf_set___libc_subfreeres_element_fstab_free__:
	.quad	fstab_free
	.local	fstab_state
	.comm	fstab_state,104,32
	.hidden	__endmntent
	.hidden	strcmp
	.hidden	__getmntent_r
	.hidden	__setmntent
	.hidden	rewind
	.hidden	__hasmntopt
