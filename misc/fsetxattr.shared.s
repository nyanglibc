.text
 .globl fsetxattr
 .type fsetxattr,@function
 .align 1<<4
 fsetxattr: 
 
 movq %rcx, %r10
 movl $190, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size fsetxattr,.-fsetxattr
.globl __GI_fsetxattr 
 .set __GI_fsetxattr,fsetxattr
