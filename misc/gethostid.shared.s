	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/hostid"
#NO_APP
	.text
	.p2align 4,,15
	.globl	gethostid
	.type	gethostid, @function
gethostid:
	pushq	%r14
	pushq	%r13
	leaq	.LC0(%rip), %rdi
	pushq	%r12
	pushq	%rbp
	xorl	%edx, %edx
	pushq	%rbx
	xorl	%esi, %esi
	xorl	%eax, %eax
	subq	$1184, %rsp
	call	__GI___open_nocancel
	testl	%eax, %eax
	js	.L2
	leaq	12(%rsp), %rsi
	movl	%eax, %ebx
	movl	$4, %edx
	movl	%eax, %edi
	call	__GI___read_nocancel
	movl	%ebx, %edi
	movq	%rax, %rbp
	call	__GI___close_nocancel
	cmpq	$4, %rbp
	je	.L25
.L2:
	leaq	64(%rsp), %rbx
	movl	$64, %esi
	movq	%rbx, %rdi
	call	__gethostname
	testl	%eax, %eax
	js	.L23
	cmpb	$0, 64(%rsp)
	je	.L23
	leaq	144(%rsp), %rbp
	leaq	20(%rsp), %r14
	leaq	24(%rsp), %r13
	leaq	32(%rsp), %r12
	movq	$1024, 152(%rsp)
	movl	$1024, %ecx
	leaq	16(%rbp), %rdx
	movq	%rdx, 144(%rsp)
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__gethostbyname_r
	testl	%eax, %eax
	jne	.L6
	movq	24(%rsp), %rax
	movq	144(%rsp), %rdi
	testq	%rax, %rax
	jne	.L26
.L8:
	addq	$16, %rbp
	cmpq	%rbp, %rdi
	je	.L23
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%eax, %eax
.L1:
	addq	$1184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$-1, 20(%rsp)
	jne	.L10
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L10
	movq	%rbp, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	je	.L23
	movq	152(%rsp), %rcx
	movq	144(%rsp), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L25:
	movslq	12(%rsp), %rax
	addq	$1184, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	144(%rsp), %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$4, 20(%rax)
	movl	$4, %edx
	movl	$0, 16(%rsp)
	cmovle	20(%rax), %edx
	movq	24(%rax), %rax
	leaq	16(%rsp), %rcx
	movq	(%rax), %rsi
	movslq	%edx, %rdx
	testq	%rdx, %rdx
	je	.L13
	xorl	%eax, %eax
.L12:
	movzbl	(%rsi,%rax), %r8d
	movb	%r8b, (%rcx,%rax)
	addq	$1, %rax
	cmpq	%rdx, %rax
	jb	.L12
.L13:
	addq	$16, %rbp
	cmpq	%rbp, %rdi
	je	.L14
	call	free@PLT
.L14:
	movl	16(%rsp), %eax
	roll	$16, %eax
	cltq
	jmp	.L1
	.size	gethostid, .-gethostid
	.hidden	__gethostbyname_r
	.hidden	__gethostname
