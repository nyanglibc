.text
 .globl fanotify_init
 .type fanotify_init,@function
 .align 1<<4
 fanotify_init: 
 
 movl $300, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size fanotify_init,.-fanotify_init
