.text
 .globl __munmap
 .type __munmap,@function
 .align 1<<4
 __munmap: 
 
 movl $11, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __munmap,.-__munmap
.weak munmap 
 munmap = __munmap
