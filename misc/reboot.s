	.text
	.p2align 4,,15
	.globl	reboot
	.type	reboot, @function
reboot:
	movl	%edi, %edx
	movl	$672274793, %esi
	movl	$-18751827, %edi
	movl	$169, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/reboot.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	reboot, .-reboot
