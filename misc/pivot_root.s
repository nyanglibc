.text
 .globl pivot_root
 .type pivot_root,@function
 .align 1<<4
 pivot_root: 
 
 movl $155, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size pivot_root,.-pivot_root
