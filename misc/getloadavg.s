	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/loadavg"
	.text
	.p2align 4,,15
	.globl	getloadavg
	.type	getloadavg, @function
getloadavg:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	movl	%esi, %r14d
	xorl	%esi, %esi
	subq	$104, %rsp
	call	__open_nocancel
	testl	%eax, %eax
	js	.L6
	leaq	16(%rsp), %rbx
	movl	%eax, %ebp
	movl	$64, %edx
	movl	%eax, %edi
	movq	%rbx, %rsi
	call	__read_nocancel
	movl	%ebp, %edi
	movq	%rax, %r13
	movl	$-1, %ebp
	call	__close_nocancel
	testq	%r13, %r13
	jle	.L1
	cmpl	$3, %r14d
	movb	$0, 15(%rsp,%r13)
	movl	$3, %r13d
	cmovle	%r14d, %r13d
	xorl	%ebp, %ebp
	testl	%r14d, %r14d
	jle	.L1
	leaq	8(%rsp), %r15
	leaq	_nl_C_locobj(%rip), %r14
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	addl	$1, %ebp
	addq	$8, %r12
	movq	%rax, %rbx
	cmpl	%r13d, %ebp
	jge	.L1
.L5:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	__strtod_l
	movq	8(%rsp), %rax
	movsd	%xmm0, (%r12)
	cmpq	%rbx, %rax
	jne	.L4
.L6:
	movl	$-1, %ebp
.L1:
	addq	$104, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	getloadavg, .-getloadavg
	.hidden	__strtod_l
	.hidden	_nl_C_locobj
	.hidden	__close_nocancel
	.hidden	__read_nocancel
	.hidden	__open_nocancel
