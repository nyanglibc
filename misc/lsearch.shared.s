	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	lsearch
	.type	lsearch, @function
lsearch:
	pushq	%r15
	pushq	%r14
	xorl	%r15d, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rcx, %r12
	movq	%r8, %r14
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	(%rdx), %rdi
	movq	%rsi, 8(%rsp)
	testq	%rdi, %rdi
	jne	.L2
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	movq	0(%rbp), %rdi
	addq	$1, %r15
	addq	%r12, %rbx
	cmpq	%rdi, %r15
	jnb	.L7
.L2:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	*%r14
	testl	%eax, %eax
	jne	.L4
	movq	0(%rbp), %rdi
	cmpq	%r15, %rdi
	jbe	.L7
	testq	%rbx, %rbx
	jne	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	imulq	%r12, %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	addq	8(%rsp), %rdi
	call	__GI_memcpy@PLT
	addq	$1, 0(%rbp)
	movq	%rax, %rbx
.L1:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	lsearch, .-lsearch
	.p2align 4,,15
	.globl	__GI_lfind
	.hidden	__GI_lfind
	.type	__GI_lfind, @function
__GI_lfind:
	cmpq	$0, (%rdx)
	je	.L29
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%r8, %r13
	xorl	%ebp, %ebp
	subq	$8, %rsp
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	addq	%r15, %rbx
	addq	$1, %rbp
	cmpq	%rbp, (%r12)
	jbe	.L20
.L17:
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*%r13
	testl	%eax, %eax
	jne	.L19
	cmpq	%rbp, (%r12)
	jbe	.L20
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%eax, %eax
	ret
	.size	__GI_lfind, .-__GI_lfind
	.globl	lfind
	.set	lfind,__GI_lfind
