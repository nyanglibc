	.text
#APP
	.section .gnu.glibc-stub.__compat_bdflush
	.previous
	.section .gnu.warning.__compat_bdflush
	.previous
#NO_APP
	.p2align 4,,15
	.globl	_no_syscall
	.type	_no_syscall, @function
_no_syscall:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movq	$-1, %rax
	ret
	.size	_no_syscall, .-_no_syscall
	.weak	__GI___compat_bdflush
	.set	__GI___compat_bdflush,_no_syscall
	.weak	__compat_bdflush
	.set	__compat_bdflush,_no_syscall
	.section	.gnu.warning.__compat_bdflush
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning___compat_bdflush, @object
	.size	__evoke_link_warning___compat_bdflush, 57
__evoke_link_warning___compat_bdflush:
	.string	"__compat_bdflush is not implemented and will always fail"
