.text
 .globl vhangup
 .type vhangup,@function
 .align 1<<4
 vhangup: 
 
 movl $153, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size vhangup,.-vhangup
