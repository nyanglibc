.text
 .globl __arch_prctl
 .type __arch_prctl,@function
 .align 1<<4
 __arch_prctl: 
 
 movl $158, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __arch_prctl,.-__arch_prctl
.weak arch_prctl 
 arch_prctl = __arch_prctl
