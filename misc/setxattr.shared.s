.text
 .globl setxattr
 .type setxattr,@function
 .align 1<<4
 setxattr: 
 
 movq %rcx, %r10
 movl $188, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size setxattr,.-setxattr
.globl __GI_setxattr 
 .set __GI_setxattr,setxattr
