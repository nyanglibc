.text
 .globl llistxattr
 .type llistxattr,@function
 .align 1<<4
 llistxattr: 
 
 movl $195, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size llistxattr,.-llistxattr
.globl __GI_llistxattr 
 .set __GI_llistxattr,llistxattr
