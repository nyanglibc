	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__brk
	.hidden	__brk
	.type	__brk, @function
__brk:
	movl	$12, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/brk.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	__curbrk@GOTPCREL(%rip), %rcx
	cmpq	%rdi, %rax
	movq	%rax, (%rcx)
	jb	.L5
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__brk, .-__brk
	.weak	brk
	.set	brk,__brk
	.globl	__curbrk
	.bss
	.align 8
	.type	__curbrk, @object
	.size	__curbrk, 8
__curbrk:
	.zero	8
