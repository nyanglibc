	.text
#APP
	.section .gnu.glibc-stub.chflags
	.previous
	.section .gnu.warning.chflags
	.previous
#NO_APP
	.p2align 4,,15
	.globl	chflags
	.type	chflags, @function
chflags:
	testq	%rdi, %rdi
	movq	__libc_errno@gottpoff(%rip), %rax
	je	.L5
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	chflags, .-chflags
	.section	.gnu.warning.chflags
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_chflags, @object
	.size	__evoke_link_warning_chflags, 48
__evoke_link_warning_chflags:
	.string	"chflags is not implemented and will always fail"
