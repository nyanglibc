.text
 .globl __mremap
 .type __mremap,@function
 .align 1<<4
 __mremap: 
 
 movq %rcx, %r10
 movl $25, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __mremap,.-__mremap
.globl __GI___mremap 
 .set __GI___mremap,__mremap
.weak mremap 
 mremap = __mremap
.globl __GI_mremap 
 .set __GI_mremap,mremap
