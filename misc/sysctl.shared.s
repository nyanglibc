	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver ___sysctl,sysctl@GLIBC_2.2.5
	.symver ___sysctl2,__sysctl@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	___sysctl
	.type	___sysctl, @function
___sysctl:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	___sysctl, .-___sysctl
	.globl	___sysctl2
	.set	___sysctl2,___sysctl
