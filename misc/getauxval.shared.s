	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___getauxval2
	.hidden	__GI___getauxval2
	.type	__GI___getauxval2, @function
__GI___getauxval2:
	cmpq	$16, %rdi
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	je	.L12
	cmpq	$26, %rdi
	je	.L13
	movq	96(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$16, %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L8
.L7:
	cmpq	%rdx, %rdi
	jne	.L6
	movq	8(%rax), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	728(%rax), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	88(%rax), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.size	__GI___getauxval2, .-__GI___getauxval2
	.globl	__getauxval2
	.set	__getauxval2,__GI___getauxval2
	.p2align 4,,15
	.globl	__GI___getauxval
	.hidden	__GI___getauxval
	.type	__GI___getauxval, @function
__GI___getauxval:
	cmpq	$16, %rdi
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	je	.L27
	cmpq	$26, %rdi
	jne	.L17
	movq	728(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	96(%rax), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L21
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$16, %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L18
.L21:
	cmpq	%rdx, %rdi
	jne	.L20
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	88(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__GI___getauxval, .-__GI___getauxval
	.weak	getauxval
	.set	getauxval,__GI___getauxval
	.globl	__getauxval
	.set	__getauxval,__GI___getauxval
