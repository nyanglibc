	.text
#APP
	.section .gnu.glibc-stub.fchflags
	.previous
	.section .gnu.warning.fchflags
	.previous
#NO_APP
	.p2align 4,,15
	.globl	fchflags
	.type	fchflags, @function
fchflags:
	testl	%edi, %edi
	movq	__libc_errno@gottpoff(%rip), %rax
	js	.L5
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	fchflags, .-fchflags
	.section	.gnu.warning.fchflags
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_fchflags, @object
	.size	__evoke_link_warning_fchflags, 49
__evoke_link_warning_fchflags:
	.string	"fchflags is not implemented and will always fail"
