        .text
.globl __clone
 .type __clone,@function
 .align 1<<4
 __clone: 
 
 movq $-22,%rax
 testq %rdi,%rdi
 jz 0f
 testq %rsi,%rsi
 jz 0f
 subq $16,%rsi
 movq %rcx,8(%rsi)
 movq %rdi,0(%rsi)
 movq %rdx, %rdi
 movq %r8, %rdx
 movq %r9, %r8
 mov 8(%rsp), %r10
 movl $56,%eax
 syscall
 testq %rax,%rax
 jl 0f
 jz .Lthread_start
 ret
.Lthread_start:
 
 xorl %ebp, %ebp
 popq %rax
 popq %rdi
 call *%rax
 movq %rax, %rdi
 movl $60, %eax
 syscall
 
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __clone,.-__clone
.weak clone 
 clone = __clone
