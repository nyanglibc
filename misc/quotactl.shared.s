.text
 .globl quotactl
 .type quotactl,@function
 .align 1<<4
 quotactl: 
 
 movq %rcx, %r10
 movl $179, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size quotactl,.-quotactl
.globl __GI_quotactl 
 .set __GI_quotactl,quotactl
