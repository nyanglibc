	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__xmknod
	.type	__xmknod, @function
__xmknod:
	testl	%edi, %edi
	jne	.L5
	movq	(%rcx), %rcx
	movl	$-100, %edi
	jmp	__GI___mknodat
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__xmknod, .-__xmknod
