	.text
	.p2align 4,,15
	.globl	__getauxval2
	.hidden	__getauxval2
	.type	__getauxval2, @function
__getauxval2:
	cmpq	$16, %rdi
	je	.L12
	cmpq	$26, %rdi
	je	.L13
	movq	_dl_auxv(%rip), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$16, %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L8
.L7:
	cmpq	%rdx, %rdi
	jne	.L6
	movq	8(%rax), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	_dl_hwcap2(%rip), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	_dl_hwcap(%rip), %rax
	movq	%rax, (%rsi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.size	__getauxval2, .-__getauxval2
	.p2align 4,,15
	.globl	__getauxval
	.hidden	__getauxval
	.type	__getauxval, @function
__getauxval:
	cmpq	$16, %rdi
	je	.L27
	cmpq	$26, %rdi
	jne	.L17
	movq	_dl_hwcap2(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	_dl_auxv(%rip), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L21
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$16, %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L18
.L21:
	cmpq	%rdx, %rdi
	jne	.L20
	movq	8(%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	_dl_hwcap(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__getauxval, .-__getauxval
	.weak	getauxval
	.set	getauxval,__getauxval
