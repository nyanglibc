	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s: "
.LC1:
	.string	"\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__vwarnx_internal
	.type	__vwarnx_internal, @function
__vwarnx_internal:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	movq	__progname@GOTPCREL(%rip), %rax
	movq	stderr@GOTPCREL(%rip), %rbx
	movq	(%rax), %rdx
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	call	__fxprintf
	testq	%rbp, %rbp
	je	.L2
	movq	(%rbx), %rdi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	__vfxprintf
.L2:
	movq	(%rbx), %rdi
	addq	$8, %rsp
	leaq	.LC1(%rip), %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	xorl	%eax, %eax
	jmp	__fxprintf
	.size	__vwarnx_internal, .-__vwarnx_internal
	.section	.rodata.str1.1
.LC2:
	.string	": %m\n"
.LC3:
	.string	"%s: %m\n"
	.text
	.p2align 4,,15
	.globl	__vwarn_internal
	.type	__vwarn_internal, @function
__vwarn_internal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	__progname@GOTPCREL(%rip), %rdi
	movq	stderr@GOTPCREL(%rip), %rbp
	testq	%rbx, %rbx
	movq	(%rdi), %r8
	movq	0(%rbp), %rdi
	je	.L9
	movq	__libc_errno@gottpoff(%rip), %r14
	movq	%rsi, %r12
	leaq	.LC0(%rip), %rsi
	movl	%edx, %r13d
	xorl	%eax, %eax
	movq	%r8, %rdx
	movl	%fs:(%r14), %r15d
	call	__fxprintf
	movq	0(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%r13d, %ecx
	movq	%r12, %rdx
	call	__vfxprintf
	movl	%r15d, %fs:(%r14)
	movq	0(%rbp), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	jmp	__fxprintf
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$8, %rsp
	leaq	.LC3(%rip), %rsi
	movq	%r8, %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	xorl	%eax, %eax
	jmp	__fxprintf
	.size	__vwarn_internal, .-__vwarn_internal
	.p2align 4,,15
	.globl	__GI_vwarn
	.hidden	__GI_vwarn
	.type	__GI_vwarn, @function
__GI_vwarn:
	xorl	%edx, %edx
	jmp	__vwarn_internal@PLT
	.size	__GI_vwarn, .-__GI_vwarn
	.globl	vwarn
	.set	vwarn,__GI_vwarn
	.p2align 4,,15
	.globl	__GI_vwarnx
	.hidden	__GI_vwarnx
	.type	__GI_vwarnx, @function
__GI_vwarnx:
	xorl	%edx, %edx
	jmp	__vwarnx_internal@PLT
	.size	__GI_vwarnx, .-__GI_vwarnx
	.globl	vwarnx
	.set	vwarnx,__GI_vwarnx
	.p2align 4,,15
	.globl	__GI_warn
	.hidden	__GI_warn
	.type	__GI_warn, @function
__GI_warn:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L15
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L15:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vwarn_internal@PLT
	addq	$216, %rsp
	ret
	.size	__GI_warn, .-__GI_warn
	.globl	warn
	.set	warn,__GI_warn
	.p2align 4,,15
	.globl	__GI_warnx
	.hidden	__GI_warnx
	.type	__GI_warnx, @function
__GI_warnx:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rsi, 40(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L19
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L19:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$8, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__vwarnx_internal@PLT
	addq	$216, %rsp
	ret
	.size	__GI_warnx, .-__GI_warnx
	.globl	warnx
	.set	warnx,__GI_warnx
	.p2align 4,,15
	.globl	__GI_verr
	.hidden	__GI_verr
	.type	__GI_verr, @function
__GI_verr:
	pushq	%rbx
	movl	%edi, %ebx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	xorl	%edx, %edx
	call	__vwarn_internal@PLT
	movl	%ebx, %edi
	call	__GI_exit
	.size	__GI_verr, .-__GI_verr
	.globl	verr
	.set	verr,__GI_verr
	.p2align 4,,15
	.globl	__GI_verrx
	.hidden	__GI_verrx
	.type	__GI_verrx, @function
__GI_verrx:
	pushq	%rbx
	movl	%edi, %ebx
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	xorl	%edx, %edx
	call	__vwarnx_internal@PLT
	movl	%ebx, %edi
	call	__GI_exit
	.size	__GI_verrx, .-__GI_verrx
	.globl	verrx
	.set	verrx,__GI_verrx
	.p2align 4,,15
	.globl	err
	.type	err, @function
err:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L27
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L27:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__GI_verr
	.size	err, .-err
	.p2align 4,,15
	.globl	errx
	.type	errx, @function
errx:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L31
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L31:
	leaq	224(%rsp), %rax
	leaq	8(%rsp), %rdx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	call	__GI_verrx
	.size	errx, .-errx
	.hidden	__vfxprintf
	.hidden	__fxprintf
