	.text
	.p2align 4,,15
	.globl	__truncate64
	.type	__truncate64, @function
__truncate64:
	movl	$76, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/truncate64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__truncate64, .-__truncate64
	.weak	truncate
	.set	truncate,__truncate64
	.weak	truncate64
	.set	truncate64,__truncate64
