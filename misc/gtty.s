	.text
#APP
	.section .gnu.glibc-stub.gtty
	.previous
	.section .gnu.warning.gtty
	.previous
#NO_APP
	.p2align 4,,15
	.globl	gtty
	.type	gtty, @function
gtty:
	testq	%rsi, %rsi
	movq	__libc_errno@gottpoff(%rip), %rax
	je	.L5
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	gtty, .-gtty
	.section	.gnu.warning.gtty
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_gtty, @object
	.size	__evoke_link_warning_gtty, 45
__evoke_link_warning_gtty:
	.string	"gtty is not implemented and will always fail"
