.text
 .globl __compat_nfsservctl
 .type __compat_nfsservctl,@function
 .align 1<<4
 __compat_nfsservctl: 
 
 movl $180, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __compat_nfsservctl,.-__compat_nfsservctl
.globl __GI___compat_nfsservctl 
 .set __GI___compat_nfsservctl,__compat_nfsservctl
.symver __compat_nfsservctl, nfsservctl@GLIBC_2.2.5
