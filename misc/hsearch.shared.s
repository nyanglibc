	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__hdestroy
	.type	__hdestroy, @function
__hdestroy:
	leaq	htab(%rip), %rdi
	jmp	__GI___hdestroy_r
	.size	__hdestroy, .-__hdestroy
	.weak	hdestroy
	.set	hdestroy,__hdestroy
	.p2align 4,,15
	.globl	hsearch
	.type	hsearch, @function
hsearch:
	subq	$24, %rsp
	leaq	htab(%rip), %r8
	leaq	8(%rsp), %rcx
	call	__GI___hsearch_r
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	hsearch, .-hsearch
	.p2align 4,,15
	.globl	hcreate
	.type	hcreate, @function
hcreate:
	leaq	htab(%rip), %rsi
	jmp	__GI___hcreate_r
	.size	hcreate, .-hcreate
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element___hdestroy__, @object
	.size	__elf_set___libc_subfreeres_element___hdestroy__, 8
__elf_set___libc_subfreeres_element___hdestroy__:
	.quad	__hdestroy
	.local	htab
	.comm	htab,16,16
