	.text
	.p2align 4,,15
	.globl	tee
	.type	tee, @function
tee:
	movl	%ecx, %r10d
#APP
# 25 "../sysdeps/unix/sysv/linux/tee.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$276, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/tee.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %r12d
	movl	%esi, %ebp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	movl	%r12d, %r10d
	movl	%eax, %r8d
	movq	%r13, %rdx
	movl	%ebp, %esi
	movl	%ebx, %edi
	movl	$276, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/tee.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	tee, .-tee
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
