.text
 .globl prlimit
 .type prlimit,@function
 .align 1<<4
 prlimit: 
 
 movq %rcx, %r10
 movl $302, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size prlimit,.-prlimit
.globl __GI_prlimit 
 .set __GI_prlimit,prlimit
.weak prlimit64 
 prlimit64 = prlimit
.globl __GI_prlimit64 
 .set __GI_prlimit64,prlimit64
