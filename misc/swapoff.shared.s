.text
 .globl __swapoff
 .type __swapoff,@function
 .align 1<<4
 __swapoff: 
 
 movl $168, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size __swapoff,.-__swapoff
.globl __GI___swapoff 
 .set __GI___swapoff,__swapoff
.weak swapoff 
 swapoff = __swapoff
.globl __GI_swapoff 
 .set __GI_swapoff,swapoff
