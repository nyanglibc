	.text
	.p2align 4,,15
	.globl	mkdtemp
	.type	mkdtemp, @function
mkdtemp:
	pushq	%rbx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$1, %ecx
	movq	%rdi, %rbx
	call	__gen_tempname
	testl	%eax, %eax
	movl	$0, %eax
	cmove	%rbx, %rax
	popq	%rbx
	ret
	.size	mkdtemp, .-mkdtemp
	.hidden	__gen_tempname
