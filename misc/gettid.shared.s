.text
 .globl __gettid
 .type __gettid,@function
 .align 1<<4
 __gettid: 
 
 movl $186, %eax
 syscall
 ret
 .size __gettid,.-__gettid
.globl __GI___gettid 
 .set __GI___gettid,__gettid
.weak gettid 
 gettid = __gettid
.globl __GI_gettid 
 .set __GI_gettid,gettid
