	.text
	.p2align 4,,15
	.globl	usleep
	.type	usleep, @function
usleep:
	movl	%edi, %eax
	movl	$1125899907, %edx
	subq	$24, %rsp
	mull	%edx
	xorl	%esi, %esi
	shrl	$18, %edx
	movl	%edx, %eax
	imull	$1000000, %edx, %edx
	movq	%rax, (%rsp)
	subl	%edx, %edi
	imulq	$1000, %rdi, %rdi
	movq	%rdi, 8(%rsp)
	movq	%rsp, %rdi
	call	__nanosleep
	addq	$24, %rsp
	ret
	.size	usleep, .-usleep
	.hidden	__nanosleep
