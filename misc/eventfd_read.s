	.text
	.p2align 4,,15
	.globl	eventfd_read
	.type	eventfd_read, @function
eventfd_read:
	subq	$8, %rsp
	movl	$8, %edx
	call	__read
	cmpq	$8, %rax
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	negl	%eax
	ret
	.size	eventfd_read, .-eventfd_read
	.hidden	__read
