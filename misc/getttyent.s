	.text
	.p2align 4,,15
	.type	skip, @function
skip:
	movsbl	(%rdi), %edx
	testl	%edx, %edx
	movl	%edx, %esi
	je	.L15
	movq	%rdi, %rcx
	xorl	%r9d, %r9d
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L5:
	movzbl	(%rdi), %eax
	cmpl	$35, %edx
	movb	%al, (%rcx)
	je	.L25
	leal	-9(%rdx), %ecx
	leaq	1(%rdi), %rax
	cmpl	$1, %ecx
	jbe	.L17
	cmpl	$32, %edx
	je	.L17
	movq	%r8, %rcx
.L4:
	movsbl	1(%rdi), %edx
	movq	%rax, %rdi
	testl	%edx, %edx
	movl	%edx, %esi
	je	.L26
.L12:
	cmpl	$34, %edx
	je	.L27
	cmpl	$1, %r9d
	leaq	1(%rcx), %r8
	jne	.L5
	cmpb	$92, %sil
	leaq	1(%rdi), %rax
	je	.L28
.L6:
	movzbl	(%rdi), %edx
.L8:
	movb	%dl, (%rcx)
	movsbl	1(%rdi), %edx
	movq	%r8, %rcx
	movq	%rax, %rdi
	testl	%edx, %edx
	movl	%edx, %esi
	jne	.L12
.L26:
	movq	%rcx, %r8
.L2:
	movb	$0, -1(%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	xorl	$1, %r9d
	leaq	1(%rdi), %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L28:
	movzbl	1(%rdi), %edx
	cmpb	$34, %dl
	jne	.L6
	leaq	2(%rdi), %rsi
	movq	%rax, %rdi
	movq	%rsi, %rax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movb	%sil, zapchar(%rip)
	movb	$0, (%rdi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rax
.L10:
	movzbl	(%rax), %edx
	leal	-9(%rdx), %ecx
	cmpb	$1, %cl
	jbe	.L11
	cmpb	$32, %dl
	je	.L11
	movb	$0, -1(%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movb	$0, (%rdi)
	movb	%sil, zapchar(%rip)
	movq	%rdi, %rax
	movb	$0, -1(%r8)
	ret
.L15:
	movq	%rdi, %r8
	movq	%rdi, %rax
	jmp	.L2
	.size	skip, .-skip
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"/etc/ttys"
	.text
	.p2align 4,,15
	.globl	__setttyent
	.hidden	__setttyent
	.type	__setttyent, @function
__setttyent:
	subq	$8, %rsp
	movq	tf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L30
	call	rewind
	movl	$1, %eax
.L29:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_IO_new_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, tf(%rip)
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L29
	orl	$32768, (%rdx)
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	__setttyent, .-__setttyent
	.weak	setttyent
	.set	setttyent,__setttyent
	.section	.rodata.str1.1
.LC2:
	.string	"off"
.LC3:
	.string	"on"
.LC4:
	.string	"secure"
.LC5:
	.string	"window"
	.text
	.p2align 4,,15
	.globl	__getttyent
	.hidden	__getttyent
	.type	__getttyent, @function
__getttyent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	tf(%rip), %rdx
	testq	%rdx, %rdx
	je	.L36
.L68:
	leaq	line.9018(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$100, %esi
	movq	%rbx, %rdi
	call	__fgets_unlocked
	testq	%rax, %rax
	je	.L67
	movl	$10, %esi
	movq	%rbx, %rdi
	call	strchr
	testq	%rax, %rax
	je	.L105
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rbp
	movsbq	(%rbx), %rdx
	leaq	line.9018(%rip), %rdi
	movq	%fs:0(%rbp), %rcx
	movq	%rdx, %rax
	testb	$32, 1(%rcx,%rdx,2)
	je	.L43
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$1, %rdi
	movsbq	(%rdi), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	movq	%rdx, %rax
	jne	.L42
.L43:
	testb	%al, %al
	je	.L47
	cmpb	$35, %al
	jne	.L50
.L47:
	movq	tf(%rip), %rdx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L105:
	movq	tf(%rip), %rdx
	movq	8(%rdx), %rax
	movq	16(%rdx), %rsi
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	1(%rax), %rcx
	movq	%rcx, 8(%rdx)
	cmpb	$10, (%rax)
	je	.L37
.L48:
	movq	%rcx, %rax
.L41:
	cmpq	%rsi, %rax
	jb	.L44
	movq	%rdx, %rdi
	call	__uflow
	cmpl	$-1, %eax
	je	.L47
	cmpl	$10, %eax
	je	.L47
	movq	tf(%rip), %rdx
	movq	8(%rdx), %rcx
	movq	16(%rdx), %rsi
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
.L35:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movb	$0, zapchar(%rip)
	movq	%rdi, tty.9015(%rip)
	call	skip
	movq	%rax, 8+tty.9015(%rip)
	cmpb	$0, (%rax)
	movq	%rax, %rbx
	jne	.L52
	movq	$0, 16+tty.9015(%rip)
	movq	$0, 8+tty.9015(%rip)
.L53:
	movl	$0, 24+tty.9015(%rip)
	movq	$0, 32+tty.9015(%rip)
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L55
	leaq	.LC2(%rip), %r12
	leaq	.LC3(%rip), %r13
	leaq	.LC4(%rip), %r14
	leaq	.LC5(%rip), %r15
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L106:
	movsbq	3(%rbx), %rcx
	movq	%fs:0(%rbp), %rdx
	testb	$32, 1(%rdx,%rcx,2)
	je	.L56
	andl	$-2, 24+tty.9015(%rip)
.L57:
	movq	%rbx, %rdi
	call	skip
	movq	%rax, %rbx
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L55
.L62:
	movl	$3, %ecx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	repz cmpsb
	je	.L106
.L56:
	movl	$2, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	repz cmpsb
	jne	.L58
	movsbq	2(%rbx), %rcx
	movq	%fs:0(%rbp), %rdx
	testb	$32, 1(%rdx,%rcx,2)
	je	.L58
	orl	$1, 24+tty.9015(%rip)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$6, %ecx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	repz cmpsb
	jne	.L59
	movsbq	6(%rbx), %rcx
	movq	%fs:0(%rbp), %rdx
	testb	$32, 1(%rdx,%rcx,2)
	je	.L59
	orl	$2, 24+tty.9015(%rip)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$6, %ecx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	repz cmpsb
	jne	.L60
	cmpb	$61, 6(%rbx)
	jne	.L60
	movl	$61, %esi
	movq	%rbx, %rdi
	call	strchr
	cmpq	$1, %rax
	sbbq	$-1, %rax
	movq	%rax, 32+tty.9015(%rip)
	jmp	.L57
.L55:
	cmpb	$35, zapchar(%rip)
	je	.L94
.L63:
	movq	%rbx, 40+tty.9015(%rip)
	cmpb	$0, (%rbx)
	jne	.L66
	movq	$0, 40+tty.9015(%rip)
.L66:
	movl	$10, %esi
	movq	%rbx, %rdi
	call	strchr
	movq	%rax, %rdx
	leaq	tty.9015(%rip), %rax
	testq	%rdx, %rdx
	je	.L35
	movb	$0, (%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L60:
	cmpb	$35, zapchar(%rip)
	je	.L94
	cmpb	$35, %al
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$1, %rbx
	movsbl	(%rbx), %eax
	cmpl	$32, %eax
	je	.L94
	cmpl	$9, %eax
	je	.L94
	jmp	.L63
.L36:
	call	__setttyent
	testl	%eax, %eax
	je	.L67
	movq	tf(%rip), %rdx
	jmp	.L68
.L52:
	movq	%rax, %rdi
	call	skip
	movq	%rax, 16+tty.9015(%rip)
	cmpb	$0, (%rax)
	movq	%rax, %rbx
	jne	.L54
	movq	$0, 16+tty.9015(%rip)
	jmp	.L53
.L54:
	movq	%rax, %rdi
	call	skip
	movq	%rax, %rbx
	jmp	.L53
	.size	__getttyent, .-__getttyent
	.weak	getttyent
	.set	getttyent,__getttyent
	.p2align 4,,15
	.globl	__getttynam
	.type	__getttynam, @function
__getttynam:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__setttyent
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L110:
	movq	(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L109
.L108:
	call	__getttyent
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L110
.L109:
	movq	tf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L107
	call	_IO_new_fclose@PLT
	movq	$0, tf(%rip)
.L107:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	__getttynam, .-__getttynam
	.weak	getttynam
	.set	getttynam,__getttynam
	.p2align 4,,15
	.globl	__endttyent
	.hidden	__endttyent
	.type	__endttyent, @function
__endttyent:
	movq	tf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L123
	subq	$8, %rsp
	call	_IO_new_fclose@PLT
	cmpl	$-1, %eax
	movq	$0, tf(%rip)
	setne	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$1, %eax
	ret
	.size	__endttyent, .-__endttyent
	.weak	endttyent
	.set	endttyent,__endttyent
	.local	tty.9015
	.comm	tty.9015,48,32
	.local	line.9018
	.comm	line.9018,100,32
	.local	tf
	.comm	tf,8,8
	.local	zapchar
	.comm	zapchar,1,1
	.hidden	strcmp
	.hidden	__uflow
	.hidden	strchr
	.hidden	__fgets_unlocked
	.hidden	rewind
