	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.type	deallocate, @function
deallocate:
	movq	%rsi, %rdi
	jmp	free@PLT
	.size	deallocate, .-deallocate
	.p2align 4,,15
	.type	allocate, @function
allocate:
	movl	$4136, %edi
	jmp	malloc@PLT
	.size	allocate, .-allocate
	.p2align 4,,15
	.globl	getmntent
	.type	getmntent, @function
getmntent:
	pushq	%rbx
	movq	mntent_buffer(%rip), %rsi
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	je	.L9
.L6:
	movq	%rbx, %rdi
	leaq	40(%rsi), %rdx
	movl	$4096, %ecx
	popq	%rbx
	jmp	__GI___getmntent_r
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	allocate(%rip), %rsi
	leaq	deallocate(%rip), %rdx
	leaq	mntent_buffer(%rip), %rdi
	xorl	%ecx, %ecx
	call	__GI___libc_allocate_once_slow
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L6
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	getmntent, .-getmntent
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	mntent_buffer, @object
	.size	mntent_buffer, 8
mntent_buffer:
	.zero	8
