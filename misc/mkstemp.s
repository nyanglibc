	.text
	.p2align 4,,15
	.globl	mkstemp
	.type	mkstemp, @function
mkstemp:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__gen_tempname
	.size	mkstemp, .-mkstemp
	.weak	mkstemp64
	.set	mkstemp64,mkstemp
	.hidden	__gen_tempname
