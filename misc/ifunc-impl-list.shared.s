	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_ifunc_impl_list
	.type	__libc_ifunc_impl_list, @function
__libc_ifunc_impl_list:
	xorl	%eax, %eax
	ret
	.size	__libc_ifunc_impl_list, .-__libc_ifunc_impl_list
