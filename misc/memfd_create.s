.text
 .globl memfd_create
 .type memfd_create,@function
 .align 1<<4
 memfd_create: 
 
 movl $319, %eax
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size memfd_create,.-memfd_create
