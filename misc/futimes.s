	.text
	.p2align 4,,15
	.globl	__futimes
	.hidden	__futimes
	.type	__futimes, @function
__futimes:
	subq	$40, %rsp
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L2
	imulq	$1000, 8(%rsi), %rax
	movq	(%rsi), %rdx
	movq	%rdx, (%rsp)
	movq	16(%rsi), %rdx
	movq	%rax, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsp, %rdx
	imulq	$1000, 24(%rsi), %rax
	movq	%rax, 24(%rsp)
.L2:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	__utimensat64_helper
	addq	$40, %rsp
	ret
	.size	__futimes, .-__futimes
	.weak	futimes
	.set	futimes,__futimes
	.hidden	__utimensat64_helper
