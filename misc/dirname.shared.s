	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	dirname
	.type	dirname, @function
dirname:
	testq	%rdi, %rdi
	leaq	dot.2503(%rip), %rax
	je	.L26
	pushq	%rbx
	movl	$47, %esi
	movq	%rdi, %rbx
	call	__GI_strrchr
	testq	%rax, %rax
	je	.L3
	cmpq	%rax, %rbx
	je	.L3
	cmpb	$0, 1(%rax)
	je	.L31
.L4:
	cmpb	$47, -1(%rax)
	jne	.L10
.L29:
	movq	%rax, %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	cmpb	$47, -1(%rax)
	jne	.L10
.L11:
	subq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L12
.L13:
	leaq	1(%rbx), %rax
	leaq	2(%rbx), %rcx
	cmpq	%rdx, %rax
	cmove	%rcx, %rax
.L10:
	movb	$0, (%rax)
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpb	$47, -1(%rax)
	movq	%rax, %rdx
	jne	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	subq	$1, %rdx
	cmpq	%rdx, %rbx
	je	.L29
	cmpb	$47, -1(%rdx)
	je	.L6
.L5:
	subq	%rbx, %rdx
	movl	$47, %esi
	movq	%rbx, %rdi
	call	__memrchr@PLT
.L3:
	testq	%rax, %rax
	je	.L32
	cmpq	%rbx, %rax
	jne	.L4
	movq	%rbx, %rdx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L26:
	rep ret
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	dot.2503(%rip), %rax
	popq	%rbx
	ret
	.size	dirname, .-dirname
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	dot.2503, @object
	.size	dot.2503, 2
dot.2503:
	.string	"."
