	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__timerfd_settime
	.type	__timerfd_settime, @function
__timerfd_settime:
	movq	%rcx, %r10
	movl	$286, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/timerfd_settime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__timerfd_settime, .-__timerfd_settime
	.globl	timerfd_settime
	.set	timerfd_settime,__timerfd_settime
