	.text
	.p2align 4,,15
	.globl	__hcreate_r
	.hidden	__hcreate_r
	.type	__hcreate_r, @function
__hcreate_r:
	testq	%rsi, %rsi
	je	.L22
	xorl	%eax, %eax
	cmpq	$0, (%rsi)
	je	.L23
	rep ret
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$3, %rdi
	movl	$3, %r8d
	movl	$4294967293, %eax
	cmovnb	%rdi, %r8
	orq	$1, %r8
	cmpq	%rax, %r8
	ja	.L9
	subq	%r8, %rax
	movl	$-1431655765, %r9d
	andq	$-2, %rax
	leaq	2(%r8,%rax), %r10
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$8, %r8d
	movl	%r8d, %edi
	jbe	.L5
	movl	%r8d, %eax
	mull	%r9d
	shrl	%edx
	leal	(%rdx,%rdx,2), %eax
	cmpl	%r8d, %eax
	je	.L6
	movl	$3, %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%edx, %edx
	je	.L6
.L7:
	addl	$2, %ecx
	xorl	%edx, %edx
	movl	%edi, %eax
	divl	%ecx
	cmpl	%eax, %ecx
	jbe	.L8
.L5:
	pushq	%rbx
	movq	%rsi, %rbx
	movl	%edi, 8(%rsi)
	movl	$0, 12(%rsi)
	addl	$1, %edi
	movl	$24, %esi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, (%rbx)
	setne	%al
	movzbl	%al, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$2, %r8
	cmpq	%r10, %r8
	jne	.L10
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__hcreate_r, .-__hcreate_r
	.weak	hcreate_r
	.set	hcreate_r,__hcreate_r
	.p2align 4,,15
	.globl	__hdestroy_r
	.hidden	__hdestroy_r
	.type	__hdestroy_r, @function
__hdestroy_r:
	testq	%rdi, %rdi
	je	.L30
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	free@PLT
	movq	$0, (%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	ret
	.size	__hdestroy_r, .-__hdestroy_r
	.weak	hdestroy_r
	.set	hdestroy_r,__hdestroy_r
	.p2align 4,,15
	.globl	__hsearch_r
	.hidden	__hsearch_r
	.type	__hsearch_r, @function
__hsearch_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$56, %rsp
	movq	%rsi, 40(%rsp)
	movl	%edx, 36(%rsp)
	movq	%rcx, 24(%rsp)
	call	strlen
	testl	%eax, %eax
	je	.L44
	leal	-1(%rax), %edx
	movl	%eax, %ebx
	leaq	-1(%rbp), %rcx
	movl	%edx, %eax
	addq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L33:
	movsbl	(%rax), %edx
	sall	$4, %ebx
	subq	$1, %rax
	addl	%edx, %ebx
	cmpq	%rax, %rcx
	jne	.L33
	testl	%ebx, %ebx
	movl	$1, %eax
	cmove	%eax, %ebx
.L32:
	movl	8(%r13), %r12d
	xorl	%edx, %edx
	movl	%ebx, %eax
	movq	0(%r13), %r15
	divl	%r12d
	leal	1(%rdx), %eax
	movq	%rax, %r14
	leaq	(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %rdx
	movl	(%rdx), %eax
	testl	%eax, %eax
	je	.L34
	cmpl	%ebx, %eax
	je	.L51
.L35:
	leal	-2(%r12), %ecx
	movl	%ebx, %eax
	xorl	%edx, %edx
	movl	%r12d, %r11d
	divl	%ecx
	movl	%r14d, %ecx
	leal	1(%rdx), %r8d
	subl	%r8d, %r11d
	.p2align 4,,10
	.p2align 3
.L37:
	leal	(%rcx,%r11), %eax
	movl	%ecx, %edx
	subl	%r8d, %edx
	cmpl	%r8d, %ecx
	cmova	%edx, %eax
	cmpl	%r14d, %eax
	movl	%eax, %ecx
	je	.L34
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %rdx
	movl	(%rdx), %eax
	cmpl	%ebx, %eax
	je	.L52
	testl	%eax, %eax
	jne	.L37
	movl	%ecx, %r14d
.L34:
	cmpl	$1, 36(%rsp)
	je	.L53
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	movq	24(%rsp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
.L31:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%rdx), %rsi
	movq	%rbp, %rdi
	movl	%r11d, 32(%rsp)
	movl	%r8d, 20(%rsp)
	movl	%ecx, 16(%rsp)
	movq	%rdx, 8(%rsp)
	call	strcmp
	testl	%eax, %eax
	movq	8(%rsp), %rdx
	movl	16(%rsp), %ecx
	movl	20(%rsp), %r8d
	movl	32(%rsp), %r11d
	jne	.L37
	movq	24(%rsp), %rax
	addq	$8, %rdx
	movq	%rdx, (%rax)
	movl	$1, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L53:
	movl	12(%r13), %edx
	cmpl	%edx, %r12d
	je	.L54
	movl	%r14d, %r9d
	movq	40(%rsp), %rdi
	addl	$1, %edx
	leaq	(%r9,%r9,2), %rax
	leaq	(%r15,%rax,8), %rax
	movq	%rdi, 16(%rax)
	movq	24(%rsp), %rdi
	addq	$8, %rax
	movl	%ebx, -8(%rax)
	movq	%rbp, (%rax)
	movl	%edx, 12(%r13)
	movq	%rax, (%rdi)
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L51:
	movq	8(%rdx), %rsi
	movq	%rbp, %rdi
	movq	%rdx, 8(%rsp)
	call	strcmp
	testl	%eax, %eax
	jne	.L35
	movq	8(%rsp), %rdx
	movq	24(%rsp), %rax
	addq	$8, %rdx
	movq	%rdx, (%rax)
	movl	$1, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L54:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	movq	24(%rsp), %rax
	movq	$0, (%rax)
	xorl	%eax, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$1, %ebx
	jmp	.L32
	.size	__hsearch_r, .-__hsearch_r
	.weak	hsearch_r
	.set	hsearch_r,__hsearch_r
	.hidden	strcmp
	.hidden	strlen
