 .text
.globl syscall
 .type syscall,@function
 .align 1<<4
 syscall: 
 
 movq %rdi, %rax
 movq %rsi, %rdi
 movq %rdx, %rsi
 movq %rcx, %rdx
 movq %r8, %r10
 movq %r9, %r8
 movq 8(%rsp),%r9
 syscall
 cmpq $-4095, %rax
 jae 0f
 ret
0: movq __libc_errno@GOTTPOFF(%rip), %rcx
 neg %eax
 movl %eax, %fs:(%rcx)
 or $-1, %rax
 ret
 .size syscall,.-syscall
