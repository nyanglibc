	.text
	.p2align 4,,15
	.globl	__gnu_dev_major
	.hidden	__gnu_dev_major
	.type	__gnu_dev_major, @function
__gnu_dev_major:
	movq	%rdi, %rax
	shrq	$8, %rax
	movl	%eax, %edx
	movq	%rdi, %rax
	shrq	$32, %rax
	andl	$4095, %edx
	andl	$-4096, %eax
	orl	%edx, %eax
	ret
	.size	__gnu_dev_major, .-__gnu_dev_major
	.weak	gnu_dev_major
	.hidden	gnu_dev_major
	.set	gnu_dev_major,__gnu_dev_major
	.p2align 4,,15
	.globl	__gnu_dev_minor
	.hidden	__gnu_dev_minor
	.type	__gnu_dev_minor, @function
__gnu_dev_minor:
	movq	%rdi, %rax
	movzbl	%dil, %edx
	shrq	$12, %rax
	xorb	%al, %al
	orl	%edx, %eax
	ret
	.size	__gnu_dev_minor, .-__gnu_dev_minor
	.weak	gnu_dev_minor
	.hidden	gnu_dev_minor
	.set	gnu_dev_minor,__gnu_dev_minor
	.p2align 4,,15
	.globl	__gnu_dev_makedev
	.hidden	__gnu_dev_makedev
	.type	__gnu_dev_makedev, @function
__gnu_dev_makedev:
	movq	%rsi, %rax
	movabsq	$17592184995840, %rdx
	movl	%edi, %edi
	salq	$12, %rax
	movzbl	%sil, %esi
	andq	%rdx, %rax
	orq	%rsi, %rax
	movq	%rdi, %rsi
	salq	$8, %rsi
	andl	$1048320, %esi
	orq	%rax, %rsi
	movq	%rdi, %rax
	movabsq	$-17592186044416, %rdi
	salq	$32, %rax
	andq	%rdi, %rax
	orq	%rsi, %rax
	ret
	.size	__gnu_dev_makedev, .-__gnu_dev_makedev
	.weak	gnu_dev_makedev
	.hidden	gnu_dev_makedev
	.set	gnu_dev_makedev,__gnu_dev_makedev
