	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___inet6_scopeid_pton
	.hidden	__GI___inet6_scopeid_pton
	.type	__GI___inet6_scopeid_pton, @function
__GI___inet6_scopeid_pton:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
	andl	$49407, %eax
	cmpl	$33022, %eax
	je	.L2
	cmpb	$-1, (%rdi)
	je	.L22
.L3:
	movsbl	(%rbx), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L5
	leaq	8(%rsp), %rsi
	leaq	_nl_C_locobj(%rip), %r8
	xorl	%ecx, %ecx
	movl	$10, %edx
	movq	%rbx, %rdi
	call	__GI_____strtoull_l_internal
	movq	8(%rsp), %rdx
	cmpb	$0, (%rdx)
	jne	.L5
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	ja	.L5
.L20:
	movl	%eax, 0(%rbp)
	xorl	%eax, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movzbl	1(%rdi), %eax
	andl	$15, %eax
	subl	$1, %eax
	cmpb	$1, %al
	ja	.L3
.L2:
	movq	%rbx, %rdi
	call	__GI___if_nametoindex
	testl	%eax, %eax
	je	.L3
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.size	__GI___inet6_scopeid_pton, .-__GI___inet6_scopeid_pton
	.globl	__inet6_scopeid_pton
	.set	__inet6_scopeid_pton,__GI___inet6_scopeid_pton
	.hidden	_nl_C_locobj
