	.text
#APP
	.section .gnu.warning.setnetent
	.previous
	.section .gnu.warning.endnetent
	.previous
	.section .gnu.warning.getnetent_r
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"setnetent"
#NO_APP
	.text
	.p2align 4,,15
	.globl	setnetent
	.type	setnetent, @function
setnetent:
.LFB70:
	subq	$24, %rsp
	movl	%edi, %r9d
#APP
# 124 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	leaq	stayopen_tmp(%rip), %rax
	pushq	$1
	leaq	last_nip(%rip), %r8
	leaq	startp(%rip), %rcx
	leaq	nip(%rip), %rdx
	leaq	__nss_networks_lookup2(%rip), %rsi
	pushq	%rax
	leaq	.LC0(%rip), %rdi
	call	__nss_setent
	movq	__libc_errno@gottpoff(%rip), %r8
	movl	%fs:(%r8), %r9d
#APP
# 129 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L4
	subl	$1, lock(%rip)
.L5:
	movl	%r9d, %fs:(%r8)
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	movl	%edi, 12(%rsp)
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	movl	12(%rsp), %r9d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
#APP
# 129 "../nss/getXXent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L5
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 129 "../nss/getXXent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L5
.LFE70:
	.size	setnetent, .-setnetent
	.section	.rodata.str1.1
.LC1:
	.string	"endnetent"
	.text
	.p2align 4,,15
	.globl	endnetent
	.type	endnetent, @function
endnetent:
.LFB71:
	cmpq	$0, startp(%rip)
	je	.L15
	subq	$8, %rsp
#APP
# 142 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L11:
	leaq	last_nip(%rip), %r8
	leaq	startp(%rip), %rcx
	leaq	nip(%rip), %rdx
	leaq	__nss_networks_lookup2(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$1, %r9d
	call	__nss_endent
	movq	__libc_errno@gottpoff(%rip), %r8
	movl	%fs:(%r8), %r9d
#APP
# 146 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, lock(%rip)
.L13:
	movl	%r9d, %fs:(%r8)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	rep ret
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 146 "../nss/getXXent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 146 "../nss/getXXent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L11
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L11
.LFE71:
	.size	endnetent, .-endnetent
	.section	.rodata.str1.1
.LC2:
	.string	"getnetent_r"
	.text
	.p2align 4,,15
	.globl	__getnetent_r
	.hidden	__getnetent_r
	.type	__getnetent_r, @function
__getnetent_r:
.LFB72:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
#APP
# 159 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L19
	movl	$1, %edi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edi, lock(%rip)
# 0 "" 2
#NO_APP
.L20:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	subq	$8, %rsp
	leaq	last_nip(%rip), %r9
	leaq	startp(%rip), %r8
	leaq	.LC2(%rip), %rdi
	pushq	%rax
	leaq	stayopen_tmp(%rip), %rax
	pushq	%rcx
	leaq	nip(%rip), %rcx
	pushq	%rdx
	leaq	__nss_networks_lookup2(%rip), %rdx
	pushq	%rsi
	leaq	.LC0(%rip), %rsi
	pushq	%rbx
	pushq	$1
	pushq	%rax
	call	__nss_getent_r
	movq	__libc_errno@gottpoff(%rip), %r8
	movl	%eax, %r9d
	movl	%fs:(%r8), %ebx
#APP
# 165 "../nss/getXXent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	addq	$64, %rsp
	testl	%eax, %eax
	jne	.L21
	subl	$1, lock(%rip)
.L22:
	movl	%ebx, %fs:(%r8)
	addq	$32, %rsp
	movl	%r9d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%eax, %eax
	movl	$1, %edi
	lock cmpxchgl	%edi, lock(%rip)
	je	.L20
	leaq	lock(%rip), %rdi
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
#APP
# 165 "../nss/getXXent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L22
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 165 "../nss/getXXent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L22
.LFE72:
	.size	__getnetent_r, .-__getnetent_r
	.globl	__new_getnetent_r
	.set	__new_getnetent_r,__getnetent_r
	.weak	getnetent_r
	.set	getnetent_r,__new_getnetent_r
	.section	.gnu.warning.getnetent_r
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getnetent_r, @object
	.size	__evoke_link_warning_getnetent_r, 135
__evoke_link_warning_getnetent_r:
	.string	"Using 'getnetent_r' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	.gnu.warning.endnetent
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_endnetent, @object
	.size	__evoke_link_warning_endnetent, 133
__evoke_link_warning_endnetent:
	.string	"Using 'endnetent' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	.gnu.warning.setnetent
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_setnetent, @object
	.size	__evoke_link_warning_setnetent, 133
__evoke_link_warning_setnetent:
	.string	"Using 'setnetent' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.local	lock
	.comm	lock,4,4
	.local	stayopen_tmp
	.comm	stayopen_tmp,4,4
	.local	startp
	.comm	startp,8,8
	.local	last_nip
	.comm	last_nip,8,8
	.local	nip
	.comm	nip,8,8
	.hidden	__nss_getent_r
	.hidden	__nss_endent
	.hidden	__lll_lock_wait_private
	.hidden	__nss_setent
	.hidden	__nss_networks_lookup2
