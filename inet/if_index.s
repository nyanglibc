	.text
	.p2align 4,,15
	.globl	__if_nametoindex
	.hidden	__if_nametoindex
	.type	__if_nametoindex, @function
__if_nametoindex:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$48, %rsp
	call	strlen
	cmpq	$15, %rax
	jbe	.L2
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$19, %fs:(%rax)
	addq	$48, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rsp, %rbp
	movq	%rbx, %rsi
	movl	$16, %edx
	movq	%rbp, %rdi
	call	strncpy
	call	__opensock
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L9
	movl	%eax, %edi
	movq	%rbp, %rdx
	xorl	%eax, %eax
	movl	$35123, %esi
	call	__ioctl
	testl	%eax, %eax
	js	.L11
	movl	%ebx, %edi
	call	__close_nocancel
	movl	16(%rsp), %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%ebx, %edi
	movl	%fs:0(%rbp), %r12d
	call	__close_nocancel
	cmpl	$22, %r12d
	jne	.L9
	movl	$38, %fs:0(%rbp)
.L9:
	addq	$48, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__if_nametoindex, .-__if_nametoindex
	.weak	if_nametoindex
	.hidden	if_nametoindex
	.set	if_nametoindex,__if_nametoindex
	.p2align 4,,15
	.globl	__if_freenameindex
	.hidden	__if_freenameindex
	.type	__if_freenameindex, @function
__if_freenameindex:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$16, %rbx
	call	free@PLT
.L13:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L14
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L14
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.size	__if_freenameindex, .-__if_freenameindex
	.weak	if_freenameindex
	.hidden	if_freenameindex
	.set	if_freenameindex,__if_freenameindex
	.p2align 4,,15
	.globl	__if_nameindex
	.type	__if_nameindex, @function
__if_nameindex:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	leaq	16(%rsp), %rbx
	movq	$0, 16(%rsp)
	movl	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 40(%rsp)
	movq	%rbx, %rdi
	call	__netlink_open
	testl	%eax, %eax
	js	.L38
	movl	$18, %esi
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	__netlink_request
	testl	%eax, %eax
	js	.L18
	movq	32(%rsp), %r15
	testq	%r15, %r15
	je	.L19
	movl	20(%rsp), %r10d
	movq	%r15, %r8
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L23:
	movq	8(%r8), %rdx
	leal	1(%r9), %esi
	movl	%esi, %edi
	testq	%rdx, %rdx
	je	.L20
	movq	16(%r8), %rcx
	cmpq	$15, %rcx
	jbe	.L20
	movl	(%rdx), %eax
	cmpl	$15, %eax
	jbe	.L20
	movl	%eax, %r11d
	cmpq	%r11, %rcx
	jnb	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L65:
	movl	(%rdx), %eax
	cmpl	$15, %eax
	jbe	.L20
	movl	%eax, %esi
	cmpq	%rsi, %rcx
	jb	.L20
	leal	1(%r9), %esi
.L22:
	cmpl	%r10d, 12(%rdx)
	movl	%esi, %edi
	jne	.L21
	movl	24(%r8), %esi
	cmpl	%esi, 8(%rdx)
	jne	.L21
	movzwl	4(%rdx), %esi
	cmpw	$3, %si
	je	.L20
	cmpw	$16, %si
	jne	.L21
	leal	2(%r9), %esi
	movl	%edi, %r9d
	movl	%esi, %edi
	.p2align 4,,10
	.p2align 3
.L21:
	addl	$3, %eax
	andl	$-4, %eax
	subq	%rax, %rcx
	addq	%rax, %rdx
	cmpq	$15, %rcx
	ja	.L65
.L20:
	movq	(%r8), %r8
	testq	%r8, %r8
	jne	.L23
	salq	$4, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L25
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L35:
	movq	8(%r15), %rbp
	movq	%rcx, %r13
	salq	$4, %r13
	addq	%r14, %r13
	testq	%rbp, %rbp
	je	.L26
	movq	16(%r15), %r12
	cmpq	$15, %r12
	jbe	.L26
	movl	0(%rbp), %eax
	cmpl	$15, %eax
	jbe	.L26
	cmpq	%rax, %r12
	jnb	.L34
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movl	0(%rbp), %eax
	addl	$3, %eax
	andl	$-4, %eax
	subq	%rax, %r12
	addq	%rax, %rbp
	cmpq	$15, %r12
	jbe	.L26
	movl	0(%rbp), %eax
	cmpl	$15, %eax
	jbe	.L26
	cmpq	%r12, %rax
	ja	.L26
.L34:
	movq	%rcx, %r13
	movl	20(%rsp), %edi
	salq	$4, %r13
	addq	%r14, %r13
	cmpl	%edi, 12(%rbp)
	jne	.L27
	movl	24(%r15), %edi
	cmpl	%edi, 8(%rbp)
	jne	.L27
	movzwl	4(%rbp), %esi
	cmpw	$3, %si
	je	.L26
	cmpw	$16, %si
	jne	.L27
	movl	20(%rbp), %ecx
	subq	$32, %rax
	leaq	32(%rbp), %rsi
	cmpq	$3, %rax
	movl	%ecx, 0(%r13)
	jbe	.L29
	movzwl	32(%rbp), %ecx
	cmpw	$3, %cx
	jbe	.L29
	movzwl	%cx, %r8d
	cmpq	%r8, %rax
	jb	.L29
	cmpw	$3, 34(%rbp)
	jne	.L31
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L68:
	movzwl	(%rsi), %ecx
	cmpw	$3, %cx
	jbe	.L29
	movzwl	%cx, %r8d
	cmpq	%rax, %r8
	ja	.L29
	cmpw	$3, 2(%rsi)
	je	.L67
.L31:
	addl	$3, %ecx
	andl	$131068, %ecx
	subq	%rcx, %rax
	addq	%rcx, %rsi
	cmpq	$3, %rax
	ja	.L68
.L29:
	leal	1(%rdx), %ecx
	movq	%rcx, %r13
	movq	%rcx, %rdx
	salq	$4, %r13
	addq	%r14, %r13
	jmp	.L27
.L19:
	movl	$16, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, %r13
	jne	.L37
.L25:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$105, %fs:(%rax)
.L18:
	movq	%rbx, %rdi
	call	__netlink_free_handle
	movq	%rbx, %rdi
	call	__netlink_close
.L16:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.L35
.L37:
	movl	$0, 0(%r13)
	movq	$0, 8(%r13)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L67:
	leaq	4(%rsi), %rdi
	leaq	-4(%r8), %rsi
.L32:
	movl	%edx, 12(%rsp)
	call	__strndup
	testq	%rax, %rax
	movq	%rax, 8(%r13)
	movl	12(%rsp), %edx
	jne	.L29
	movl	$0, 0(%r13)
	movq	%r14, %rdi
	call	__if_freenameindex
	jmp	.L25
.L38:
	xorl	%r14d, %r14d
	jmp	.L16
.L66:
	leaq	36(%rbp), %rdi
	leaq	-4(%r8), %rsi
	jmp	.L32
	.size	__if_nameindex, .-__if_nameindex
	.weak	if_nameindex
	.hidden	if_nameindex
	.set	if_nameindex,__if_nameindex
	.p2align 4,,15
	.globl	__if_indextoname
	.type	__if_indextoname, @function
__if_indextoname:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$56, %rsp
	call	__opensock
	testl	%eax, %eax
	js	.L75
	movl	%ebp, 16(%rsp)
	movq	%rsp, %rbp
	movl	%eax, %edi
	movl	%eax, %ebx
	movq	%rbp, %rdx
	movl	$35088, %esi
	xorl	%eax, %eax
	call	__ioctl
	movl	%ebx, %edi
	movl	%eax, %r12d
	call	__close_nocancel
	testl	%r12d, %r12d
	jns	.L72
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$19, %fs:(%rax)
	je	.L76
.L75:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%rbp, %rsi
	movq	%r13, %rdi
	movl	$16, %edx
	call	strncpy
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$6, %fs:(%rax)
	jmp	.L75
	.size	__if_indextoname, .-__if_indextoname
	.weak	if_indextoname
	.hidden	if_indextoname
	.set	if_indextoname,__if_indextoname
	.hidden	__strndup
	.hidden	__netlink_close
	.hidden	__netlink_free_handle
	.hidden	__netlink_request
	.hidden	__netlink_open
	.hidden	__close_nocancel
	.hidden	__ioctl
	.hidden	__opensock
	.hidden	strncpy
	.hidden	strlen
