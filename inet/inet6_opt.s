	.text
	.p2align 4,,15
	.globl	inet6_opt_init
	.type	inet6_opt_init, @function
inet6_opt_init:
	testq	%rdi, %rdi
	je	.L3
	leal	-1(%rsi), %eax
	cmpl	$2047, %eax
	movl	$-1, %eax
	ja	.L1
	testb	$7, %sil
	jne	.L1
	shrl	$3, %esi
	movl	$2, %eax
	subl	$1, %esi
	movb	%sil, 1(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$2, %eax
	ret
	.size	inet6_opt_init, .-inet6_opt_init
	.p2align 4,,15
	.globl	inet6_opt_append
	.type	inet6_opt_append, @function
inet6_opt_append:
	movl	%edx, %r11d
	cmpb	$1, %cl
	setbe	%dl
	cmpl	$1, %r11d
	setbe	%al
	orb	%al, %dl
	jne	.L20
	cmpl	$255, %r8d
	ja	.L20
	leal	-1(%r9), %eax
	cmpb	$7, %al
	ja	.L20
	movzbl	%r9b, %r9d
	movq	%rdi, %r10
	leal	-1(%r9), %edi
	testl	%edi, %r9d
	jne	.L20
	cmpl	%r8d, %r9d
	ja	.L20
	pushq	%rbx
	movl	%ecx, %ebx
	leal	2(%r11), %ecx
	movl	%ecx, %eax
	cltd
	idivl	%r9d
	subl	%edx, %r9d
	andl	%edi, %r9d
	testq	%r10, %r10
	je	.L9
	addl	%r9d, %ecx
	addl	%r8d, %ecx
	cmpl	%esi, %ecx
	ja	.L21
	cmpl	$1, %r9d
	je	.L32
	testl	%r9d, %r9d
	jne	.L33
.L11:
	addl	%r11d, %r9d
	movslq	%r9d, %rax
	addq	%rax, %r10
	movq	16(%rsp), %rax
	movb	%bl, (%r10)
	movb	%r8b, 1(%r10)
	addq	$2, %r10
	movq	%r10, (%rax)
.L16:
	leal	2(%r9,%r8), %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movslq	%r11d, %rax
	movb	$0, (%r10,%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	addl	%r11d, %r9d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L33:
	leal	-2(%r9), %eax
	movslq	%r11d, %rdx
	addq	%r10, %rdx
	movb	%al, 1(%rdx)
	movzbl	%al, %eax
	movb	$1, (%rdx)
	cmpl	$8, %eax
	leaq	2(%rdx), %rcx
	jnb	.L12
	testb	$4, %al
	jne	.L34
	testl	%eax, %eax
	je	.L11
	testb	$2, %al
	movb	$0, 2(%rdx)
	je	.L11
	xorl	%edx, %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	8(%rcx), %rdi
	movq	$0, 2(%rdx)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	rep stosq
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$-1, %eax
	popq	%rbx
	ret
.L34:
	movl	$0, 2(%rdx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L11
	.size	inet6_opt_append, .-inet6_opt_append
	.p2align 4,,15
	.globl	inet6_opt_finish
	.type	inet6_opt_finish, @function
inet6_opt_finish:
	cmpl	$1, %edx
	jbe	.L44
	movl	%edx, %eax
	negl	%eax
	andl	$7, %eax
	testq	%rdi, %rdi
	leal	(%rdx,%rax), %r8d
	je	.L35
	cmpl	%esi, %r8d
	ja	.L44
	cmpl	$1, %eax
	je	.L54
	testl	%eax, %eax
	je	.L35
	movslq	%edx, %rdx
	subl	$2, %eax
	addq	%rdi, %rdx
	movb	%al, 1(%rdx)
	movzbl	%al, %eax
	movb	$1, (%rdx)
	cmpl	$8, %eax
	leaq	2(%rdx), %rcx
	jnb	.L39
	testb	$4, %al
	jne	.L55
	testl	%eax, %eax
	je	.L35
	testb	$2, %al
	movb	$0, 2(%rdx)
	je	.L35
	xorl	%edx, %edx
	movw	%dx, -2(%rcx,%rax)
.L35:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movslq	%edx, %rdx
	movl	%r8d, %eax
	movb	$0, (%rdi,%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	10(%rdx), %rdi
	movq	$0, 2(%rdx)
	movq	$0, -8(%rcx,%rax)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	xorl	%eax, %eax
	rep stosq
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$-1, %r8d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$0, 2(%rdx)
	movl	$0, -4(%rcx,%rax)
	jmp	.L35
	.size	inet6_opt_finish, .-inet6_opt_finish
	.p2align 4,,15
	.globl	inet6_opt_set_val
	.type	inet6_opt_set_val, @function
inet6_opt_set_val:
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rax
	addq	%rax, %rdi
	movq	%rdx, %rsi
	movl	%ecx, %edx
	subq	$8, %rsp
	movq	%rax, %rbx
	movq	%rdx, %rbp
	call	memcpy@PLT
	addq	$8, %rsp
	leal	(%rbx,%rbp), %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	inet6_opt_set_val, .-inet6_opt_set_val
	.p2align 4,,15
	.globl	inet6_opt_next
	.type	inet6_opt_next, @function
inet6_opt_next:
	testl	%edx, %edx
	je	.L65
	cmpl	$1, %edx
	je	.L67
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	%esi, %edx
	jnb	.L67
.L64:
	movslq	%edx, %rax
	addq	%rdi, %rax
	movzbl	(%rax), %r10d
	testb	%r10b, %r10b
	jne	.L62
	addl	$1, %edx
	cmpl	%esi, %edx
	jb	.L64
.L67:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movzbl	1(%rax), %r11d
	cmpb	$1, %r10b
	leal	2(%rdx,%r11), %edx
	je	.L61
	cmpl	%edx, %esi
	jb	.L67
	movb	%r10b, (%rcx)
	movzbl	1(%rax), %ecx
	addq	$2, %rax
	movl	%ecx, (%r8)
	movq	%rax, (%r9)
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$2, %edx
	jmp	.L61
	.size	inet6_opt_next, .-inet6_opt_next
	.p2align 4,,15
	.globl	inet6_opt_find
	.type	inet6_opt_find, @function
inet6_opt_find:
	testl	%edx, %edx
	je	.L78
	cmpl	$1, %edx
	je	.L80
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	%esi, %edx
	jnb	.L80
	movslq	%edx, %rax
	addq	%rdi, %rax
	movzbl	(%rax), %r10d
	testb	%r10b, %r10b
	jne	.L75
	addl	$1, %edx
	testb	%cl, %cl
	jne	.L74
	movslq	%edx, %rax
	movl	$0, (%r8)
	addq	%rax, %rdi
	movl	%edx, %eax
	movq	%rdi, (%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movzbl	1(%rax), %r11d
	cmpb	%cl, %r10b
	leal	2(%r11,%rdx), %edx
	jne	.L74
	cmpl	%edx, %esi
	jb	.L80
	addq	$2, %rax
	movl	%r11d, (%r8)
	movq	%rax, (%r9)
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$2, %edx
	jmp	.L74
	.size	inet6_opt_find, .-inet6_opt_find
	.p2align 4,,15
	.globl	inet6_opt_get_val
	.type	inet6_opt_get_val, @function
inet6_opt_get_val:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rax
	movslq	%esi, %rsi
	movl	%ecx, %edx
	subq	$8, %rsp
	movq	%rsi, %rbx
	addq	%rdi, %rsi
	movq	%rax, %rdi
	movq	%rdx, %rbp
	call	memcpy@PLT
	addq	$8, %rsp
	leal	(%rbx,%rbp), %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	inet6_opt_get_val, .-inet6_opt_get_val
