	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%x:%x:%x:%x:%x:%x"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_ether_ntoa_r
	.hidden	__GI_ether_ntoa_r
	.type	__GI_ether_ntoa_r, @function
__GI_ether_ntoa_r:
	pushq	%rbx
	movzbl	5(%rdi), %eax
	movq	%rsi, %rbx
	movzbl	(%rdi), %edx
	movzbl	1(%rdi), %ecx
	leaq	.LC0(%rip), %rsi
	pushq	%rax
	movzbl	4(%rdi), %eax
	pushq	%rax
	movzbl	3(%rdi), %r9d
	xorl	%eax, %eax
	movzbl	2(%rdi), %r8d
	movq	%rbx, %rdi
	call	__GI_sprintf
	popq	%rax
	movq	%rbx, %rax
	popq	%rdx
	popq	%rbx
	ret
	.size	__GI_ether_ntoa_r, .-__GI_ether_ntoa_r
	.globl	ether_ntoa_r
	.set	ether_ntoa_r,__GI_ether_ntoa_r
