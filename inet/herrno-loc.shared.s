	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.weak	__GI___h_errno_location
	.hidden	__GI___h_errno_location
	.type	__GI___h_errno_location, @function
__GI___h_errno_location:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__GI___h_errno_location, .-__GI___h_errno_location
	.globl	__h_errno_location
	.set	__h_errno_location,__GI___h_errno_location
