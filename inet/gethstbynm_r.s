	.text
#APP
	.section .gnu.warning.gethostbyname_r
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gethostbyname_r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__gethostbyname_r
	.hidden	__gethostbyname_r
	.type	__gethostbyname_r, @function
__gethostbyname_r:
.LFB86:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rcx, %r13
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	%rdx, 56(%rsp)
	movq	%r8, 32(%rsp)
	movl	$-1, 76(%rsp)
	call	__resolv_context_get
	testq	%rax, %rax
	movq	%rax, 40(%rsp)
	je	.L43
	leaq	56(%rsp), %rdx
	subq	$8, %rsp
	xorl	%ecx, %ecx
	pushq	%rbx
	pushq	$2
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%r14, %rdi
	leaq	100(%rsp), %rax
	pushq	%rax
	movq	64(%rsp), %r9
	call	__nss_hostname_digits_dots
	addq	$32, %rsp
	cmpl	$-1, %eax
	je	.L5
	cmpl	$1, %eax
	je	.L44
	leaq	80(%rsp), %rax
	leaq	88(%rsp), %rcx
	leaq	.LC0(%rip), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rcx, 24(%rsp)
	movq	%rax, 16(%rsp)
	call	__nss_hosts_lookup2
	testl	%eax, %eax
	jne	.L9
	movq	__libc_errno@gottpoff(%rip), %r15
	xorl	%ebp, %ebp
	movq	$0, 48(%rsp)
	movq	%r15, %rax
	addq	%fs:0, %rax
	movq	%rax, 8(%rsp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L10:
	testl	%ebp, %ebp
	jne	.L13
	cmpl	$1, %eax
	leal	4(%rax,%rax), %ecx
	sete	%dl
.L14:
	movq	80(%rsp), %rax
	movl	8(%rax), %eax
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L16
	testb	%dl, %dl
	jne	.L45
.L16:
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	call	__nss_next2
	testl	%eax, %eax
	jne	.L46
.L18:
	movq	88(%rsp), %rdi
	call	_dl_mcount_wrapper_check
	movq	8(%rsp), %r8
	movq	%rbx, %r9
	movq	%r13, %rcx
	movq	56(%rsp), %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	*88(%rsp)
	cmpl	$-2, %eax
	movl	%eax, %r8d
	movl	%eax, 76(%rsp)
	jne	.L10
	cmpl	$-1, (%rbx)
	je	.L47
.L11:
	testl	%ebp, %ebp
	je	.L16
.L15:
	movl	$22, %fs:(%r15)
	movl	$1, 76(%rsp)
	movl	$1, %edx
	movl	$6, %ecx
	movl	$1, %r8d
	movl	$1, %ebp
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L43:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, (%rbx)
	movq	$0, (%r15)
	movl	%fs:(%rax), %eax
.L1:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	cmpl	$1, 76(%rsp)
	je	.L48
	movq	32(%rsp), %rax
	movq	__libc_errno@gottpoff(%rip), %r15
	movq	$0, (%rax)
.L8:
	movq	40(%rsp), %rdi
	call	__resolv_context_put
	movl	76(%rsp), %edx
	cmpl	$1, %edx
	jbe	.L25
	movl	%fs:(%r15), %eax
	cmpl	$34, %eax
	je	.L49
	cmpl	$-2, %edx
	jne	.L1
.L24:
	cmpl	$-1, (%rbx)
	je	.L1
	movl	$11, %eax
.L22:
	movl	%eax, %fs:(%r15)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	40(%rsp), %rdi
	call	__resolv_context_put
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	cmpl	$1, %eax
	jne	.L15
	movl	$22, %fs:(%r15)
	movl	$-1, 76(%rsp)
	xorl	%ebp, %ebp
	movl	$-1, %r8d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	$0, 48(%rsp)
	je	.L50
.L17:
	movl	$22, %fs:(%r15)
	movl	$-1, 76(%rsp)
	movl	$-1, %r8d
	movl	$1, %ebp
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L47:
	cmpl	$34, %fs:(%r15)
	jne	.L11
	movq	48(%rsp), %rdi
	call	free@PLT
	movq	32(%rsp), %rax
	movq	$0, (%rax)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L46:
	movl	76(%rsp), %r13d
	movl	$1, %ebp
.L19:
	movq	48(%rsp), %rdi
	call	free@PLT
	cmpl	$1, %r13d
	je	.L7
	movq	32(%rsp), %rax
	cmpl	$-1, %r13d
	movq	$0, (%rax)
	jne	.L20
	testb	%bpl, %bpl
	jne	.L8
	cmpl	$2, %fs:(%r15)
	je	.L21
	movl	%r13d, (%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	testb	%bpl, %bpl
	jne	.L8
.L21:
	movl	$3, (%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L48:
	movq	__libc_errno@gottpoff(%rip), %r15
.L7:
	movq	32(%rsp), %rax
	movq	%r12, %rdi
	movq	%r12, (%rax)
	call	_res_hconf_reorder_addrs@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L17
	movq	32(%rsp), %rax
	movl	$12, %fs:(%r15)
	movl	$-1, 76(%rsp)
	movq	$0, (%rax)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L49:
	cmpl	$-2, %edx
	je	.L24
	movl	$22, %eax
	jmp	.L22
.L9:
	movl	76(%rsp), %r13d
	xorl	%ebp, %ebp
	movq	$0, 48(%rsp)
	movq	__libc_errno@gottpoff(%rip), %r15
	jmp	.L19
.LFE86:
	.size	__gethostbyname_r, .-__gethostbyname_r
	.globl	__new_gethostbyname_r
	.set	__new_gethostbyname_r,__gethostbyname_r
	.weak	gethostbyname_r
	.set	gethostbyname_r,__new_gethostbyname_r
	.section	.gnu.warning.gethostbyname_r
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_gethostbyname_r, @object
	.size	__evoke_link_warning_gethostbyname_r, 139
__evoke_link_warning_gethostbyname_r:
	.string	"Using 'gethostbyname_r' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.hidden	__resolv_context_put
	.hidden	_dl_mcount_wrapper_check
	.hidden	__nss_next2
	.hidden	__nss_hosts_lookup2
	.hidden	__nss_hostname_digits_dots
	.hidden	__resolv_context_get
