	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%x:%x:%x:%x:%x:%x"
	.text
	.p2align 4,,15
	.globl	ether_ntoa_r
	.hidden	ether_ntoa_r
	.type	ether_ntoa_r, @function
ether_ntoa_r:
	pushq	%rbx
	movzbl	5(%rdi), %eax
	movq	%rsi, %rbx
	movzbl	(%rdi), %edx
	movzbl	1(%rdi), %ecx
	leaq	.LC0(%rip), %rsi
	pushq	%rax
	movzbl	4(%rdi), %eax
	pushq	%rax
	movzbl	3(%rdi), %r9d
	xorl	%eax, %eax
	movzbl	2(%rdi), %r8d
	movq	%rbx, %rdi
	call	sprintf
	popq	%rax
	movq	%rbx, %rax
	popq	%rdx
	popq	%rbx
	ret
	.size	ether_ntoa_r, .-ether_ntoa_r
	.hidden	sprintf
