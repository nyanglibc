	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	htons
	.type	htons, @function
htons:
	movl	%edi, %eax
	rolw	$8, %ax
	ret
	.size	htons, .-htons
	.weak	ntohs
	.set	ntohs,htons
