	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	token, @function
token:
	movq	cfile(%rip), %rdi
	testb	$48, (%rdi)
	je	.L33
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movabsq	$17596481013248, %rbx
.L7:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L34
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L4:
	cmpl	$44, %eax
	jbe	.L35
.L5:
	leaq	1+tokval(%rip), %rbx
	movb	%al, tokval(%rip)
	movabsq	$17596481013248, %rbp
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L36
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L18:
	cmpl	$44, %eax
	jbe	.L37
	cmpl	$92, %eax
	movq	cfile(%rip), %rdi
	jne	.L16
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L38
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L16:
	addq	$1, %rbx
	movb	%al, -1(%rbx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L37:
	btq	%rax, %rbp
	jc	.L12
	movq	cfile(%rip), %rdi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L35:
	btq	%rax, %rbx
	jc	.L7
	cmpl	$34, %eax
	jne	.L5
	leaq	tokval(%rip), %rbx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %rbx
	movb	%al, -1(%rbx)
.L25:
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L39
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
.L11:
	cmpl	$34, %eax
	je	.L12
	cmpl	$92, %eax
	movq	cfile(%rip), %rdi
	jne	.L8
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L40
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L39:
	call	__GI___uflow
	cmpl	$-1, %eax
	jne	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movb	$0, (%rbx)
	cmpb	$0, tokval(%rip)
	je	.L2
	leaq	tokstr(%rip), %r12
	leaq	tokval(%rip), %rbp
	leaq	toktab(%rip), %r13
	xorl	%r14d, %r14d
	xorl	%edi, %edi
.L24:
	addq	%r12, %rdi
	movq	%rbp, %rsi
	movslq	%r14d, %rbx
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L41
	addq	$1, %r14
	cmpq	$7, %r14
	je	.L26
	movslq	0(%r13,%r14,8), %rdi
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L34:
	call	__GI___uflow
	cmpl	$-1, %eax
	jne	.L42
.L2:
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	call	__GI___uflow
	cmpl	$-1, %eax
	jne	.L18
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$10, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L42:
	movq	cfile(%rip), %rdi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	toktab(%rip), %rax
	movl	4(%rax,%rbx,8), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L38:
	call	__GI___uflow
	movq	cfile(%rip), %rdi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L40:
	call	__GI___uflow
	movq	cfile(%rip), %rdi
	jmp	.L8
	.size	token, .-token
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"HOME"
.LC1:
	.string	"rce"
.LC2:
	.string	"%s"
.LC3:
	.string	"out of memory"
.LC4:
	.string	"anonymous"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC5:
	.string	"Error: .netrc file is readable by others."
	.align 8
.LC6:
	.string	"Remove 'password' line or make file unreadable by others."
	.section	.rodata.str1.1
.LC7:
	.string	"Unknown .netrc keyword %s"
	.text
	.p2align 4,,15
	.globl	__GI_ruserpass
	.hidden	__GI_ruserpass
	.type	__GI_ruserpass, @function
__GI_ruserpass:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r15
	pushq	%rbx
	movq	%rsi, %r12
	subq	$1208, %rsp
	movq	%rdi, -1232(%rbp)
	leaq	.LC0(%rip), %rdi
	call	__GI___libc_secure_getenv
	testq	%rax, %rax
	je	.L73
	movq	%rax, %rdi
	movq	%rax, %r13
	call	__GI_strlen
	addq	$38, %rax
	movq	%r13, %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdi
	call	__GI_stpcpy@PLT
	movabsq	$27991866937847343, %rcx
	leaq	.LC1(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rcx, (%rax)
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, cfile(%rip)
	je	.L106
	orl	$32768, (%rax)
	leaq	-1072(%rbp), %rbx
	movl	$1024, %esi
	movq	%rbx, %rdi
	call	__gethostname
	testl	%eax, %eax
	js	.L107
.L46:
	movl	$46, %esi
	movq	%rbx, %rdi
	leaq	tokval(%rip), %r13
	leaq	.LC7(%rip), %r14
	call	__strchrnul@PLT
	movl	$0, -1224(%rbp)
	movq	%rax, -1240(%rbp)
.L48:
	call	token
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L71
	cmpl	$1, %ebx
	je	.L49
	cmpl	$11, %ebx
	jne	.L48
	movl	-1224(%rbp), %ebx
	testl	%ebx, %ebx
	je	.L108
.L49:
	leaq	-1216(%rbp), %rax
	movq	%rax, -1224(%rbp)
	.p2align 4,,10
	.p2align 3
.L56:
	call	token
	cmpl	$1, %eax
	jbe	.L71
	cmpl	$11, %eax
	je	.L71
	cmpl	$3, %eax
	je	.L53
	jg	.L54
	cmpl	$2, %eax
	jne	.L52
	call	token
	testl	%eax, %eax
	je	.L56
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L109
	movq	%r13, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L56
	movl	%ebx, -1224(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L71:
	movq	cfile(%rip), %rdi
	xorl	%r12d, %r12d
	call	_IO_new_fclose@PLT
.L43:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	cmpl	$5, %eax
	jle	.L56
.L52:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movq	%r14, %rsi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%r13, %rsi
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	__GI_warnx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L53:
	movq	(%r12), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$10, %ecx
	repz cmpsb
	je	.L64
	movq	cfile(%rip), %rdi
	call	__GI_fileno
	movq	-1224(%rbp), %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L64
	testb	$63, -1192(%rbp)
	jne	.L110
.L64:
	call	token
	testl	%eax, %eax
	je	.L56
	cmpq	$0, (%r15)
	jne	.L56
	leaq	tokval(%rip), %rdx
.L65:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L65
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%r13, %rdx
	leal	1(%rdx), %edi
	movq	%rdx, -1248(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	-1248(%rbp), %rdx
	je	.L105
	addq	$1, %rdx
	movq	%r13, %rsi
	call	__GI_memcpy@PLT
	movq	%rax, (%r15)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L106:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r12d, %r12d
	cmpl	$2, %fs:(%rax)
	je	.L43
	leaq	.LC2(%rip), %rdi
	movq	%rbx, %rsi
	xorl	%eax, %eax
	call	__GI_warn
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L107:
	movb	$0, -1072(%rbp)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L108:
	call	token
	cmpl	$10, %eax
	jne	.L48
	movq	-1232(%rbp), %rdi
	movq	%r13, %rsi
	call	__GI___strcasecmp
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L49
	movq	-1232(%rbp), %rdi
	movl	$46, %esi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L48
	movq	-1240(%rbp), %rsi
	movq	%rax, %rdi
	call	__GI___strcasecmp
	testl	%eax, %eax
	jne	.L48
	movq	-1232(%rbp), %rdi
	movq	%rbx, %rcx
	movq	%r13, %rsi
	subq	%rdi, %rcx
	movq	%rcx, %rdx
	movq	%rcx, -1248(%rbp)
	call	__strncasecmp@PLT
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L48
	movq	-1248(%rbp), %rcx
	cmpb	$0, 0(%r13,%rcx)
	je	.L49
	movl	$0, -1224(%rbp)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L109:
	leaq	tokval(%rip), %rdx
.L59:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L59
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%r13, %rdx
	leal	1(%rdx), %edi
	movq	%rdx, -1248(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	-1248(%rbp), %rdx
	je	.L105
	addq	$1, %rdx
	movq	%r13, %rsi
	call	__GI_memcpy@PLT
	movq	%rax, (%r12)
	jmp	.L56
.L73:
	movl	$-1, %r12d
	jmp	.L43
.L110:
	leaq	.LC5(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	__GI_warnx
	leaq	.LC6(%rip), %rsi
	movl	$5, %edx
.L104:
.L62:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$-1, %r12d
	call	__GI___dcgettext
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	__GI_warnx
	movq	cfile(%rip), %rdi
	call	_IO_new_fclose@PLT
	jmp	.L43
.L105:
	movl	$5, %edx
	leaq	.LC3(%rip), %rsi
	jmp	.L104
	.size	__GI_ruserpass, .-__GI_ruserpass
	.globl	ruserpass
	.set	ruserpass,__GI_ruserpass
	.section	.rodata
	.align 32
	.type	toktab, @object
	.size	toktab, 56
toktab:
	.long	0
	.long	1
	.long	8
	.long	2
	.long	14
	.long	3
	.long	23
	.long	3
	.long	30
	.long	4
	.long	38
	.long	11
	.long	46
	.long	5
	.align 32
	.type	tokstr, @object
	.size	tokstr, 53
tokstr:
	.string	"default"
	.string	"login"
	.string	"password"
	.string	"passwd"
	.string	"account"
	.string	"machine"
	.string	"macdef"
	.local	tokval
	.comm	tokval,100,32
	.local	cfile
	.comm	cfile,8,8
	.hidden	__gethostname
