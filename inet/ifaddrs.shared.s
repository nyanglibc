	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__netlink_free_handle
	.hidden	__netlink_free_handle
	.type	__netlink_free_handle, @function
__netlink_free_handle:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	16(%rdi), %rdi
	movq	__libc_errno@gottpoff(%rip), %rbp
	testq	%rdi, %rdi
	movl	%fs:0(%rbp), %r12d
	je	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L3
.L2:
	movl	%r12d, %fs:0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__netlink_free_handle, .-__netlink_free_handle
	.p2align 4,,15
	.globl	__netlink_request
	.hidden	__netlink_request
	.type	__netlink_request, @function
__netlink_request:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbx
	leaq	-112(%rbp), %r12
	movq	%rdi, %rbx
	subq	$4248, %rsp
	movl	8(%rdi), %eax
	movq	$4096, -120(%rbp)
	leaq	15(%rsp), %r14
	andq	$-16, %r14
	testl	%eax, %eax
	movq	%r14, -128(%rbp)
	je	.L64
.L11:
	movw	%r13w, -108(%rbp)
	leaq	-140(%rbp), %r13
	movl	$769, %edx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	$16, %edi
	movl	$20, -112(%rbp)
	movw	%dx, -106(%rbp)
	movl	$0, -100(%rbp)
	movl	%eax, -104(%rbp)
	movb	$0, -96(%rbp)
	movw	%cx, -95(%rbp)
	movq	$0, -138(%rbp)
	movb	$0, 19(%r12)
	movw	%si, 10(%r13)
	movw	%di, -140(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L65:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L39
.L13:
	movl	(%rbx), %edi
	xorl	%ecx, %ecx
	movl	$12, %r9d
	movq	%r13, %r8
	movl	$20, %edx
	movq	%r12, %rsi
	call	__sendto
	cmpq	$-1, %rax
	je	.L65
	testl	%eax, %eax
	js	.L39
	leaq	-128(%rbp), %rax
	movq	%rax, -152(%rbp)
	.p2align 4,,10
	.p2align 3
.L16:
	movq	-152(%rbp), %rax
	movq	%r13, -112(%rbp)
	movl	$12, -104(%rbp)
	movq	$1, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movq	%rax, -96(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L67:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L66
.L18:
	movl	(%rbx), %edi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	__recvmsg
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L67
	movl	(%rbx), %edi
	movq	%rax, %rsi
	call	__GI___netlink_assert_response
	testq	%r15, %r15
	js	.L39
	movl	-136(%rbp), %eax
	testl	%eax, %eax
	jne	.L16
	movl	-64(%rbp), %r8d
	andl	$32, %r8d
	jne	.L39
	cmpq	$15, %r15
	jle	.L16
	movl	(%r14), %edx
	cmpl	$15, %edx
	jbe	.L16
	movl	%edx, %eax
	cmpq	%rax, %r15
	jb	.L16
	movl	4(%rbx), %edi
	movq	%r15, %rsi
	movq	%r14, %rax
	xorl	%r9d, %r9d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L29:
	movl	(%rax), %edx
	cmpl	$15, %edx
	jbe	.L62
	movl	%edx, %r10d
	cmpq	%rsi, %r10
	ja	.L62
.L34:
	cmpl	%edi, 12(%rax)
	jne	.L25
	movl	8(%rbx), %ecx
	cmpl	%ecx, 8(%rax)
	jne	.L25
	movzwl	4(%rax), %r10d
	addq	$1, %r9
	cmpw	$3, %r10w
	je	.L26
	cmpw	$2, %r10w
	je	.L68
.L25:
	addl	$3, %edx
	andl	$-4, %edx
	subq	%rdx, %rsi
	addq	%rdx, %rax
	cmpq	$15, %rsi
	ja	.L29
.L62:
	testq	%r9, %r9
	je	.L16
	movb	$0, -157(%rbp)
.L31:
	leaq	32(%r15), %rdi
	movl	%r8d, -156(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	je	.L39
	leaq	32(%rax), %rdi
	movq	$0, (%rax)
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rax, -168(%rbp)
	call	__GI_memcpy@PLT
	movq	-168(%rbp), %r9
	cmpq	$0, 16(%rbx)
	movl	-156(%rbp), %r8d
	movq	%rax, 8(%r9)
	movl	8(%rbx), %eax
	movq	%r15, 16(%r9)
	movl	%eax, 24(%r9)
	je	.L69
	movq	24(%rbx), %rax
	movq	%r9, (%rax)
.L38:
	cmpb	$0, -157(%rbp)
	movq	%r9, 24(%rbx)
	je	.L16
.L10:
	leaq	-40(%rbp), %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movl	(%rbx), %edi
	movq	%r15, %rsi
	call	__GI___netlink_assert_response
.L39:
	movl	$-1, %r8d
	jmp	.L10
.L69:
	movq	%r9, 16(%rbx)
	jmp	.L38
.L64:
	movq	%r12, %rsi
	movl	$5, %edi
	call	__GI___clock_gettime
	movq	-112(%rbp), %rdx
	movl	%edx, %eax
	movl	%edx, 8(%rbx)
	jmp	.L11
.L26:
	testq	%r9, %r9
	je	.L10
	movb	$1, -157(%rbp)
	jmp	.L31
.L68:
	cmpl	$35, %edx
	ja	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$5, %fs:(%rax)
	jmp	.L39
.L27:
	movl	16(%rax), %eax
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	jmp	.L39
	.size	__netlink_request, .-__netlink_request
	.p2align 4,,15
	.globl	__netlink_close
	.hidden	__netlink_close
	.type	__netlink_close, @function
__netlink_close:
	movl	(%rdi), %edi
	movl	$3, %eax
#APP
# 247 "../sysdeps/unix/sysv/linux/ifaddrs.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	ret
	.size	__netlink_close, .-__netlink_close
	.p2align 4,,15
	.globl	__netlink_open
	.hidden	__netlink_open
	.type	__netlink_open, @function
__netlink_open:
	pushq	%rbp
	pushq	%rbx
	xorl	%edx, %edx
	movq	%rdi, %rbx
	movl	$524291, %esi
	movl	$16, %edi
	subq	$24, %rsp
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, (%rbx)
	js	.L75
	leaq	4(%rsp), %rbp
	xorl	%edx, %edx
	movq	$0, 6(%rsp)
	movl	$16, %ecx
	movl	%eax, %edi
	movw	%dx, 10(%rbp)
	movq	%rbp, %rsi
	movl	$12, %edx
	movw	%cx, 4(%rsp)
	call	__bind
	testl	%eax, %eax
	js	.L74
	movl	(%rbx), %edi
	movq	%rsp, %rdx
	movq	%rbp, %rsi
	movl	$12, (%rsp)
	call	__getsockname
	testl	%eax, %eax
	js	.L74
	movl	8(%rsp), %eax
	movl	%eax, 4(%rbx)
	xorl	%eax, %eax
.L71:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movl	(%rbx), %edi
	movl	$3, %eax
#APP
# 247 "../sysdeps/unix/sysv/linux/ifaddrs.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	$-1, %eax
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$-1, %eax
	jmp	.L71
	.size	__netlink_open, .-__netlink_open
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/ifaddrs.c"
	.align 8
.LC1:
	.string	"ifa_data_ptr <= (char *) &ifas[newlink + newaddr] + ifa_data_size"
	.text
	.p2align 4,,15
	.type	getifaddrs_internal, @function
getifaddrs_internal:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	leaq	-80(%rbp), %rbx
	subq	$152, %rsp
	movq	%rdi, -88(%rbp)
	movq	$0, (%rdi)
	movq	%rbx, %rdi
	movq	$0, -80(%rbp)
	movl	$0, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	call	__netlink_open
	testl	%eax, %eax
	js	.L175
	movl	$18, %esi
	movq	%rbx, %rdi
	call	__netlink_request
	testl	%eax, %eax
	jns	.L79
	movq	-64(%rbp), %rbx
.L80:
	movq	__libc_errno@gottpoff(%rip), %r14
	testq	%rbx, %rbx
	movl	$-1, %r13d
	movl	%fs:(%r14), %r15d
	je	.L166
	movq	%rbx, %rdi
	.p2align 4,,10
	.p2align 3
.L167:
	movq	(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L167
.L166:
	movl	%r15d, %fs:(%r14)
	movl	-80(%rbp), %edi
	movl	$3, %eax
#APP
# 247 "../sysdeps/unix/sysv/linux/ifaddrs.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L77:
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	movq	%rbx, %rdi
	movl	$22, %esi
	addl	$1, -72(%rbp)
	call	__netlink_request
	testl	%eax, %eax
	movq	-64(%rbp), %rbx
	js	.L80
	testq	%rbx, %rbx
	je	.L82
	movl	-76(%rbp), %r13d
	movq	%rbx, %rdi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L93:
	movq	8(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L83
	movq	16(%rdi), %rcx
	cmpq	$15, %rcx
	jbe	.L83
	movl	(%rdx), %eax
	cmpl	$15, %eax
	jbe	.L83
	movl	%eax, %r11d
	cmpq	%r11, %rcx
	jnb	.L92
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L86:
	cmpw	$20, %si
	sete	%sil
	movzbl	%sil, %esi
	addl	%esi, %r9d
	.p2align 4,,10
	.p2align 3
.L84:
	addl	$3, %eax
	andl	$-4, %eax
	subq	%rax, %rcx
	addq	%rax, %rdx
	cmpq	$15, %rcx
	jbe	.L83
	movl	(%rdx), %eax
	cmpl	$15, %eax
	jbe	.L83
	movl	%eax, %r11d
	cmpq	%rcx, %r11
	ja	.L83
.L92:
	cmpl	%r13d, 12(%rdx)
	jne	.L84
	movl	24(%rdi), %esi
	cmpl	%esi, 8(%rdx)
	jne	.L84
	testb	$16, 6(%rdx)
	jne	.L85
	movzwl	4(%rdx), %esi
	cmpw	$3, %si
	je	.L83
	cmpw	$16, %si
	jne	.L86
	subq	$32, %r11
	leaq	32(%rdx), %r12
	cmpq	$3, %r11
	jbe	.L87
	movzwl	32(%rdx), %esi
	cmpw	$3, %si
	jbe	.L87
	movzwl	%si, %r14d
	cmpq	%r14, %r11
	jb	.L87
	cmpw	$7, 34(%rdx)
	jne	.L89
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L252:
	movzwl	(%r12), %esi
	cmpw	$3, %si
	jbe	.L87
	movzwl	%si, %r14d
	cmpq	%r11, %r14
	ja	.L87
	cmpw	$7, 2(%r12)
	je	.L88
.L89:
	addl	$3, %esi
	andl	$131068, %esi
	subq	%rsi, %r11
	addq	%rsi, %r12
	cmpq	$3, %r11
	ja	.L252
.L87:
	addl	$1, %r10d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L83:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L93
	addl	%r10d, %r9d
	movl	%r10d, -92(%rbp)
	jne	.L253
	movq	__libc_errno@gottpoff(%rip), %r14
	xorl	%r13d, %r13d
	movq	%rbx, %rdi
	movl	%fs:(%r14), %r15d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L88:
	subq	$4, %r14
	addl	$1, %r10d
	addq	%r14, %r8
	jmp	.L84
.L258:
	movl	-92(%rbp), %eax
	movl	-96(%rbp), %r8d
	subq	$24, %rbx
	movl	20(%r12), %ecx
	leaq	24(%r12), %r13
	addl	%eax, %r8d
	testl	%eax, %eax
	leal	-1(%rcx), %edi
	jle	.L244
	movq	-112(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L254
	cmpl	%eax, %edi
	je	.L255
	movl	-124(%rbp), %r11d
	movq	-112(%rbp), %rax
	leaq	4(%rax), %rdx
	movl	$1, %eax
	addq	$1, %r11
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L119:
	movl	(%rdx), %ecx
	cmpl	$-1, %ecx
	je	.L256
	addq	$4, %rdx
	cmpl	%ecx, %edi
	leaq	1(%rax), %rsi
	je	.L257
	movq	%rsi, %rax
.L118:
	cmpq	%rax, %r11
	jne	.L119
.L244:
	movq	-104(%rbp), %rdi
	movq	-152(%rbp), %rbx
	call	free@PLT
.L85:
	movq	__libc_errno@gottpoff(%rip), %r14
	movl	$-11, %r13d
	movq	%rbx, %rdi
	movl	%fs:(%r14), %r15d
	jmp	.L167
.L253:
	movl	%r9d, %r9d
	movl	$1, %edi
	imulq	$184, %r9, %r12
	leaq	(%r12,%r8), %rax
	movq	%rax, %rsi
	movq	%rax, -168(%rbp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	%rax, -104(%rbp)
	je	.L176
	movl	-92(%rbp), %eax
	movl	$-1, %esi
	addq	%r15, %r12
	leaq	0(,%rax,4), %rdx
	movq	%rax, %r14
	movq	%rax, -184(%rbp)
	leaq	30(%rdx), %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %rdi
	movq	%rax, -112(%rbp)
	call	__GI_memset
	leal	-1(%r14), %eax
	movq	%r12, -120(%rbp)
	movq	%rbx, %r10
	movl	$0, -96(%rbp)
	movl	%r13d, %r9d
	movq	%rbx, -152(%rbp)
	movl	%eax, -124(%rbp)
	.p2align 4,,10
	.p2align 3
.L160:
	movq	8(%r10), %r12
	testq	%r12, %r12
	je	.L96
	movq	16(%r10), %r14
	cmpq	$15, %r14
	jbe	.L96
	movl	(%r12), %ebx
	cmpl	$15, %ebx
	jbe	.L96
	cmpq	%rbx, %r14
	jnb	.L159
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L98:
	cmpw	$20, %ax
	je	.L258
	.p2align 4,,10
	.p2align 3
.L97:
	movl	(%r12), %eax
	addl	$3, %eax
	andl	$-4, %eax
	subq	%rax, %r14
	addq	%rax, %r12
	cmpq	$15, %r14
	jbe	.L96
	movl	(%r12), %ebx
	cmpl	$15, %ebx
	jbe	.L96
	cmpq	%r14, %rbx
	ja	.L96
.L159:
	cmpl	12(%r12), %r9d
	jne	.L97
	movl	24(%r10), %eax
	cmpl	%eax, 8(%r12)
	jne	.L97
	movzwl	4(%r12), %eax
	cmpw	$3, %ax
	je	.L96
	cmpw	$16, %ax
	jne	.L98
	movl	-92(%rbp), %esi
	movl	20(%r12), %eax
	subq	$32, %rbx
	leaq	32(%r12), %r13
	testl	%esi, %esi
	leal	-1(%rax), %r8d
	jle	.L244
	movq	-112(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L259
	cmpl	%eax, %r8d
	je	.L260
	movl	-124(%rbp), %r11d
	movq	-112(%rbp), %rax
	leaq	4(%rax), %rdx
	movl	$1, %eax
	addq	$1, %r11
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L105:
	movl	(%rdx), %ecx
	cmpl	$-1, %ecx
	je	.L261
	addq	$4, %rdx
	cmpl	%ecx, %r8d
	leaq	1(%rax), %rsi
	je	.L262
	movq	%rsi, %rax
.L104:
	cmpq	%rax, %r11
	jne	.L105
	jmp	.L244
.L176:
	movl	$-1, %r13d
.L95:
	movq	__libc_errno@gottpoff(%rip), %r14
	movq	%rbx, %rdi
	movl	%fs:(%r14), %r15d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L96:
	movq	(%r10), %r10
	testq	%r10, %r10
	jne	.L160
	movq	-168(%rbp), %rax
	addq	-104(%rbp), %rax
	cmpq	%rax, -120(%rbp)
	movq	-152(%rbp), %rbx
	ja	.L263
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	je	.L163
	movl	-92(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L163
	imulq	$184, -184(%rbp), %rax
	subl	$1, %ecx
	xorl	%edx, %edx
	addq	$1, %rcx
	addq	-104(%rbp), %rax
.L165:
	movq	-112(%rbp), %rdi
	cmpl	$-1, (%rdi,%rdx,4)
	jne	.L164
	leal	-1(%rdx), %esi
	movq	-104(%rbp), %rdi
	imulq	$184, %rsi, %rsi
	movq	%rax, (%rdi,%rsi)
.L164:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L165
.L163:
	movq	-88(%rbp), %rax
	movq	-104(%rbp), %rcx
	xorl	%r13d, %r13d
	movq	%rcx, (%rax)
	jmp	.L95
.L261:
	imulq	$184, %rax, %rax
	movq	-104(%rbp), %rcx
	movl	%r8d, (%rdx)
	leaq	(%rcx,%rax), %r15
	movq	%r15, -184(%rcx,%rax)
.L101:
	movl	24(%r12), %eax
	cmpq	$3, %rbx
	movl	%eax, 16(%r15)
	jbe	.L97
	movzwl	32(%r12), %eax
	cmpw	$3, %ax
	jbe	.L97
	leaq	140(%r15), %rcx
	cmpq	%rax, %rbx
	movq	%rcx, -160(%rbp)
	jb	.L97
	movl	%r9d, -128(%rbp)
	movq	%r14, -136(%rbp)
	movq	%r12, %r14
	movq	-120(%rbp), %r9
	movq	%r10, -144(%rbp)
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L109:
	cmpw	$3, %dx
	je	.L111
	cmpw	$7, %dx
	jne	.L107
	leaq	(%r9,%r11), %r12
	movq	%r9, 48(%r15)
	movq	%r9, %rdi
	movq	%r11, %rdx
	call	__GI_memcpy@PLT
	movq	%r12, %r9
	.p2align 4,,10
	.p2align 3
.L107:
	movzwl	0(%r13), %eax
	addl	$3, %eax
	andl	$131068, %eax
	subq	%rax, %rbx
	addq	%rax, %r13
	cmpq	$3, %rbx
	jbe	.L243
	movzwl	0(%r13), %eax
	cmpw	$3, %ax
	jbe	.L243
	cmpq	%rbx, %rax
	ja	.L243
.L113:
	movzwl	2(%r13), %edx
	leaq	4(%r13), %rsi
	leaq	-4(%rax), %r11
	cmpw	$2, %dx
	je	.L108
	ja	.L109
	cmpw	$1, %dx
	jne	.L107
	cmpq	$36, %r11
	ja	.L107
	movl	$17, %ecx
	leaq	68(%r15), %rdi
	movq	%r11, %rdx
	movw	%cx, 56(%r15)
	movq	%r9, -176(%rbp)
	movq	%r11, -120(%rbp)
	call	__GI_memcpy@PLT
	movl	20(%r14), %eax
	movq	-120(%rbp), %r11
	movq	-176(%rbp), %r9
	movl	%eax, 60(%r15)
	movzwl	18(%r14), %eax
	movb	%r11b, 67(%r15)
	movw	%ax, 64(%r15)
	leaq	56(%r15), %rax
	movq	%rax, 24(%r15)
	jmp	.L107
.L111:
	subq	$3, %rax
	cmpq	$17, %rax
	ja	.L107
	leaq	164(%r15), %rdi
	movq	%r11, %rdx
	movq	%r9, -120(%rbp)
	movq	%rdi, 8(%r15)
	call	__GI_mempcpy@PLT
	movq	-120(%rbp), %r9
	movb	$0, (%rax)
	jmp	.L107
.L108:
	cmpq	$36, %r11
	ja	.L107
	movq	-160(%rbp), %rdi
	movl	$17, %edx
	movq	%r9, -176(%rbp)
	movw	%dx, 128(%r15)
	movq	%r11, %rdx
	movq	%r11, -120(%rbp)
	call	__GI_memcpy@PLT
	movl	20(%r14), %eax
	movq	-120(%rbp), %r11
	movq	-176(%rbp), %r9
	movl	%eax, 132(%r15)
	movzwl	18(%r14), %eax
	movb	%r11b, 139(%r15)
	movw	%ax, 136(%r15)
	leaq	128(%r15), %rax
	movq	%rax, 40(%r15)
	jmp	.L107
.L243:
	movq	%r9, -120(%rbp)
	movq	%r14, %r12
	movl	-128(%rbp), %r9d
	movq	-136(%rbp), %r14
	movq	-144(%rbp), %r10
	jmp	.L97
.L262:
	imulq	$184, %rax, %rax
	movq	-104(%rbp), %rcx
	leaq	(%rcx,%rax), %r15
	jmp	.L101
.L256:
	imulq	$184, %rax, %rax
	movq	-104(%rbp), %rcx
	movl	%edi, (%rdx)
	leaq	(%rcx,%rax), %rdx
	movq	%rdx, -184(%rcx,%rax)
.L115:
	movslq	%r8d, %rax
	movq	-104(%rbp), %rcx
	movl	16(%rdx), %edx
	imulq	$184, %rax, %rax
	testl	%r8d, %r8d
	leaq	(%rcx,%rax), %r15
	movl	%edx, 16(%r15)
	jle	.L169
	movq	-104(%rbp), %rcx
	movq	%r15, -184(%rcx,%rax)
.L169:
	addl	$1, -96(%rbp)
	cmpq	$3, %rbx
	jbe	.L120
	movzwl	24(%r12), %eax
	cmpw	$3, %ax
	jbe	.L120
	cmpq	%rbx, %rax
	ja	.L120
	leaq	56(%r15), %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%r15, %rbx
	movq	%r14, -136(%rbp)
	movq	%r13, %r12
	movl	%r9d, -128(%rbp)
	movq	%r10, -144(%rbp)
	movq	%rcx, %r14
	movq	%rdi, %r13
	movq	%rdx, %r15
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L123:
	cmpw	$3, %di
	je	.L125
	cmpw	$4, %di
	jne	.L121
	cmpq	$0, 40(%rbx)
	leaq	128(%rbx), %rax
	je	.L140
	pxor	%xmm0, %xmm0
	movups	%xmm0, 128(%rbx)
	movl	$0, 32(%rax)
	movups	%xmm0, 16(%rax)
.L140:
	movq	%rax, 40(%rbx)
	movzbl	16(%r13), %edi
	cmpb	$2, %dil
	movw	%di, 128(%rbx)
	je	.L142
	cmpb	$10, %dil
	jne	.L264
	cmpq	$16, %rdx
	je	.L265
	.p2align 4,,10
	.p2align 3
.L121:
	movzwl	(%r12), %eax
	addl	$3, %eax
	andl	$131068, %eax
	subq	%rax, %r15
	addq	%rax, %r12
	cmpq	$3, %r15
	jbe	.L246
	movzwl	(%r12), %eax
	cmpw	$3, %ax
	jbe	.L246
	cmpq	%r15, %rax
	ja	.L246
.L146:
	movzwl	2(%r12), %edi
	leaq	4(%r12), %rsi
	leaq	-4(%rax), %rdx
	cmpw	$2, %di
	je	.L122
	ja	.L123
	cmpw	$1, %di
	jne	.L121
	cmpq	$0, 24(%rbx)
	je	.L127
	leaq	128(%rbx), %rdi
	movq	%rdi, 40(%rbx)
.L128:
	movzbl	16(%r13), %r10d
	cmpb	$2, %r10b
	movw	%r10w, (%rdi)
	je	.L130
	cmpb	$10, %r10b
	jne	.L266
	cmpq	$16, %rdx
	jne	.L121
	movdqu	4(%r12), %xmm0
	movups	%xmm0, 8(%rdi)
	movl	4(%r12), %eax
	andl	$49407, %eax
	cmpl	$33022, %eax
	je	.L134
	cmpb	$-1, 4(%r12)
	jne	.L121
	movzbl	5(%r12), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L121
.L134:
	movl	20(%r13), %eax
	movl	%eax, 24(%rdi)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L122:
	cmpq	$0, 24(%rbx)
	je	.L135
	movdqu	56(%rbx), %xmm0
	movl	88(%rbx), %eax
	movups	%xmm0, 128(%rbx)
	movdqu	72(%rbx), %xmm0
	movl	%eax, 160(%rbx)
	leaq	128(%rbx), %rax
	movups	%xmm0, 144(%rbx)
	pxor	%xmm0, %xmm0
	movq	%rax, 40(%rbx)
	movups	%xmm0, (%r14)
	movl	$0, 32(%r14)
	movups	%xmm0, 16(%r14)
.L135:
	movq	%r14, 24(%rbx)
	movzbl	16(%r13), %edi
	cmpb	$2, %dil
	movw	%di, 56(%rbx)
	je	.L137
	cmpb	$10, %dil
	jne	.L267
	cmpq	$16, %rdx
	jne	.L121
	movdqu	4(%r12), %xmm0
	movups	%xmm0, 64(%rbx)
	movl	4(%r12), %eax
	andl	$49407, %eax
	cmpl	$33022, %eax
	je	.L139
	cmpb	$-1, 4(%r12)
	jne	.L121
	movzbl	5(%r12), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L121
.L139:
	movl	20(%r13), %eax
	movl	%eax, 80(%rbx)
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L125:
	subq	$3, %rax
	cmpq	$17, %rax
	ja	.L145
	leaq	164(%rbx), %rdi
	movq	%rdi, 8(%rbx)
	call	__GI_mempcpy@PLT
	movb	$0, (%rax)
	jmp	.L121
.L246:
	movl	-128(%rbp), %r9d
	movq	-136(%rbp), %r14
	movq	%rbx, %r15
	movq	-144(%rbp), %r10
	movq	%r13, %r12
.L120:
	cmpq	$0, 8(%r15)
	je	.L268
.L147:
	movq	24(%r15), %rax
	testq	%rax, %rax
	je	.L97
	movzwl	(%rax), %edx
	cmpw	$17, %dx
	je	.L97
	testw	%dx, %dx
	je	.L97
	leaq	92(%r15), %rax
	cmpw	$2, %dx
	movq	%rax, 32(%r15)
	je	.L155
	cmpw	$10, %dx
	jne	.L269
	leaq	100(%r15), %rax
	movl	$128, %ecx
.L157:
	movw	%dx, 92(%r15)
	movzbl	17(%r12), %edx
	cmpl	%ecx, %edx
	cmova	%ecx, %edx
	movl	%edx, %ecx
	shrl	$3, %ecx
	testl	%ecx, %ecx
	je	.L171
	subl	$1, %ecx
	leaq	1(%rax,%rcx), %rcx
.L158:
	addq	$1, %rax
	movb	$-1, -1(%rax)
	cmpq	%rcx, %rax
	jne	.L158
.L171:
	andl	$7, %edx
	je	.L97
	movl	$8, %ecx
	subl	%edx, %ecx
	movl	$255, %edx
	sall	%cl, %edx
	movb	%dl, (%rax)
	jmp	.L97
.L137:
	cmpq	$4, %rdx
	jne	.L121
	movl	4(%r12), %eax
	movl	%eax, 60(%rbx)
	jmp	.L121
.L267:
	cmpq	$36, %rdx
	ja	.L121
	leaq	58(%rbx), %rdi
	call	__GI_memcpy@PLT
	jmp	.L121
.L266:
	cmpq	$36, %rdx
	ja	.L121
	addq	$2, %rdi
	call	__GI_memcpy@PLT
	jmp	.L121
.L130:
	cmpq	$4, %rdx
	jne	.L121
	movl	4(%r12), %eax
	movl	%eax, 4(%rdi)
	jmp	.L121
.L264:
	cmpq	$36, %rdx
	ja	.L121
	leaq	130(%rbx), %rdi
	call	__GI_memcpy@PLT
	jmp	.L121
.L142:
	cmpq	$4, %rdx
	jne	.L121
	movl	4(%r12), %eax
	movl	%eax, 132(%rbx)
	jmp	.L121
.L127:
	movq	%r14, %rdi
	movq	%r14, 24(%rbx)
	jmp	.L128
.L257:
	imulq	$184, %rax, %rax
	movq	-104(%rbp), %rcx
	leaq	(%rcx,%rax), %rdx
	jmp	.L115
.L175:
	movl	$-1, %r13d
	jmp	.L77
.L82:
	movq	__libc_errno@gottpoff(%rip), %r14
	xorl	%r13d, %r13d
	movl	%fs:(%r14), %r15d
	jmp	.L166
.L265:
	movdqu	4(%r12), %xmm0
	movups	%xmm0, 136(%rbx)
	movl	4(%r12), %eax
	andl	$49407, %eax
	cmpl	$33022, %eax
	je	.L144
	cmpb	$-1, 4(%r12)
	jne	.L121
	movzbl	5(%r12), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L121
.L144:
	movl	20(%r13), %eax
	movl	%eax, 152(%rbx)
	jmp	.L121
.L268:
	movl	20(%r12), %eax
	leal	-1(%rax), %esi
	movq	-112(%rbp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L270
	cmpl	%eax, %esi
	je	.L271
	movl	-124(%rbp), %r8d
	movq	-112(%rbp), %rax
	leaq	4(%rax), %rdx
	movl	$1, %eax
	addq	$1, %r8
	jmp	.L152
.L153:
	movl	(%rdx), %ecx
	cmpl	$-1, %ecx
	je	.L272
	addq	$4, %rdx
	cmpl	%ecx, %esi
	leaq	1(%rax), %rdi
	je	.L273
	movq	%rdi, %rax
.L152:
	cmpq	%rax, %r8
	jne	.L153
	jmp	.L244
.L269:
	movw	%dx, 92(%r15)
	jmp	.L97
.L155:
	leaq	96(%r15), %rax
	movl	$32, %ecx
	jmp	.L157
.L260:
	movq	-104(%rbp), %r15
	jmp	.L101
.L259:
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %r15
	movl	%r8d, (%rax)
	jmp	.L101
.L255:
	movq	-104(%rbp), %rdx
	jmp	.L115
.L254:
	movq	-112(%rbp), %rax
	movq	-104(%rbp), %rdx
	movl	%edi, (%rax)
	jmp	.L115
.L273:
	imulq	$184, %rax, %rax
	addq	-104(%rbp), %rax
.L149:
	movq	8(%rax), %rax
	movq	%rax, 8(%r15)
	jmp	.L147
.L272:
	movl	%esi, (%rdx)
	movq	-104(%rbp), %rbx
	imulq	$184, %rax, %rdx
	leaq	(%rbx,%rdx), %rax
	movq	%rax, -184(%rbx,%rdx)
	jmp	.L149
.L145:
	call	__GI_abort
.L263:
	leaq	__PRETTY_FUNCTION__.9838(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$800, %edx
	call	__GI___assert_fail
.L271:
	movq	-104(%rbp), %rax
	jmp	.L149
.L270:
	movq	-112(%rbp), %rax
	movl	%esi, (%rax)
	movq	-104(%rbp), %rax
	jmp	.L149
	.size	getifaddrs_internal, .-getifaddrs_internal
	.p2align 4,,15
	.globl	__GI___getifaddrs
	.hidden	__GI___getifaddrs
	.type	__GI___getifaddrs, @function
__GI___getifaddrs:
	pushq	%rbx
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L275:
	movq	%rbx, %rdi
	call	getifaddrs_internal
	cmpl	$-11, %eax
	je	.L275
	popq	%rbx
	ret
	.size	__GI___getifaddrs, .-__GI___getifaddrs
	.weak	__GI_getifaddrs
	.hidden	__GI_getifaddrs
	.set	__GI_getifaddrs,__GI___getifaddrs
	.weak	getifaddrs
	.set	getifaddrs,__GI_getifaddrs
	.globl	__getifaddrs
	.set	__getifaddrs,__GI___getifaddrs
	.p2align 4,,15
	.globl	__GI___freeifaddrs
	.hidden	__GI___freeifaddrs
	.type	__GI___freeifaddrs, @function
__GI___freeifaddrs:
	jmp	free@PLT
	.size	__GI___freeifaddrs, .-__GI___freeifaddrs
	.weak	__GI_freeifaddrs
	.hidden	__GI_freeifaddrs
	.set	__GI_freeifaddrs,__GI___freeifaddrs
	.weak	freeifaddrs
	.set	freeifaddrs,__GI_freeifaddrs
	.globl	__freeifaddrs
	.set	__freeifaddrs,__GI___freeifaddrs
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9838, @object
	.size	__PRETTY_FUNCTION__.9838, 20
__PRETTY_FUNCTION__.9838:
	.string	"getifaddrs_internal"
	.hidden	__getsockname
	.hidden	__bind
	.hidden	__recvmsg
	.hidden	__sendto
