	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__check_pf
	.hidden	__check_pf
	.type	__check_pf, @function
__check_pf:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$280, %rsp
	movq	$0, (%rdx)
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%rdx, -216(%rbp)
	movq	%rcx, -224(%rbp)
	movq	$0, (%rcx)
#APP
# 307 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	xorl	%edx, %edx
	movl	$524291, %esi
	movl	$16, %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %r14d
	js	.L42
	leaq	-184(%rbp), %rbx
	xorl	%r11d, %r11d
	movq	$0, -182(%rbp)
	movl	$16, %r12d
	movl	$12, %edx
	movl	%eax, %edi
	movw	%r11w, 10(%rbx)
	movq	%rbx, %rsi
	movw	%r12w, -184(%rbp)
	movl	$12, -188(%rbp)
	call	__bind
	testl	%eax, %eax
	je	.L5
.L82:
	movl	%r14d, %edi
	xorl	%r15d, %r15d
	call	__GI___close_nocancel
	xorl	%r8d, %r8d
.L4:
#APP
# 341 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L36
	subl	$1, lock(%rip)
.L37:
	testq	%r15, %r15
	je	.L38
	movzbl	8(%r15), %eax
	addq	$24, %r15
	movq	-200(%rbp), %rcx
	movb	%al, (%rcx)
	movzbl	-15(%r15), %eax
	movq	-208(%rbp), %rcx
	movb	%al, (%rcx)
	movq	-8(%r15), %rax
	testq	%r8, %r8
	movq	-224(%rbp), %rcx
	movq	%rax, (%rcx)
	movq	-216(%rbp), %rax
	movq	%r15, (%rax)
	je	.L1
	movl	4(%r8), %eax
	testl	%eax, %eax
	je	.L1
#APP
# 352 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	lock;addl $-1, 4(%r8); setz %al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L84
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-200(%rbp), %rax
	movb	$1, (%rax)
	movq	-208(%rbp), %rax
	movb	$1, (%rax)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	-188(%rbp), %rdx
	movq	%rbx, %rsi
	movl	%r14d, %edi
	call	__getsockname
	testl	%eax, %eax
	jne	.L82
	movl	-180(%rbp), %eax
	movl	$5, %edi
	movq	%rsp, -264(%rbp)
	movl	$0, -132(%rbp)
	leaq	-172(%rbp), %r12
	leaq	-144(%rbp), %rbx
	movl	%eax, -268(%rbp)
	movabsq	$216454351579774996, %rax
	movq	%rax, -144(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, %rsi
	movq	%rax, %r13
	call	__GI___clock_gettime
	movq	-112(%rbp), %rax
	subq	$4096, %rsp
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	movl	$16, %r10d
	movw	%r8w, -127(%rbp)
	movq	$0, -170(%rbp)
	movb	$0, -128(%rbp)
	movl	%eax, -136(%rbp)
	movb	$0, 19(%rbx)
	movq	%r12, -232(%rbp)
	movw	%r9w, 10(%r12)
	movw	%r10w, -172(%rbp)
	movq	%rsp, -256(%rbp)
	movq	%rsp, -160(%rbp)
	movq	$4096, -152(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L85:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L43
.L8:
	xorl	%ecx, %ecx
	movl	$12, %r9d
	movq	%r12, %r8
	movl	$20, %edx
	movq	%rbx, %rsi
	movl	%r14d, %edi
	call	__sendto
	cmpq	$-1, %rax
	je	.L85
	testq	%rax, %rax
	js	.L43
	leaq	-160(%rbp), %rax
	xorl	%r15d, %r15d
	movb	$0, -289(%rbp)
	movq	%r15, -240(%rbp)
	movb	$0, -290(%rbp)
	movq	%r13, %r15
	movq	$32, -288(%rbp)
	movq	$0, -280(%rbp)
	movq	%rax, -248(%rbp)
	.p2align 4,,10
	.p2align 3
.L10:
	movq	-232(%rbp), %rax
	movl	$12, -104(%rbp)
	movq	$1, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	movq	%rax, -112(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -96(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L87:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L86
.L12:
	xorl	%edx, %edx
	movq	%r15, %rsi
	movl	%r14d, %edi
	call	__recvmsg
	cmpq	$-1, %rax
	je	.L87
	movq	%rax, %r12
	movq	%rax, %rsi
	movl	%r14d, %edi
	call	__GI___netlink_assert_response
	testq	%r12, %r12
	js	.L80
	testb	$32, -64(%rbp)
	jne	.L80
	cmpq	$15, %r12
	jle	.L10
	movq	-256(%rbp), %r13
	movl	0(%r13), %ebx
	cmpl	$15, %ebx
	jbe	.L10
	movl	%ebx, %eax
	cmpq	%r12, %rax
	ja	.L10
	movl	-168(%rbp), %edx
	xorl	%r8d, %r8d
	movl	-268(%rbp), %ecx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L19:
	cmpw	$3, %si
	movl	$1, %eax
	cmove	%eax, %r8d
	.p2align 4,,10
	.p2align 3
.L18:
	addl	$3, %ebx
	andl	$-4, %ebx
	subq	%rbx, %r12
	addq	%rbx, %r13
	cmpq	$15, %r12
	jbe	.L31
	movl	0(%r13), %ebx
	cmpl	$15, %ebx
	jbe	.L31
	movl	%ebx, %eax
	cmpq	%r12, %rax
	ja	.L31
.L32:
	testl	%edx, %edx
	jne	.L18
	cmpl	12(%r13), %ecx
	jne	.L18
	movl	-136(%rbp), %edi
	cmpl	%edi, 8(%r13)
	jne	.L18
	movzwl	4(%r13), %esi
	cmpw	$20, %si
	jne	.L19
	movzbl	16(%r13), %r11d
	movl	%r11d, %esi
	andl	$-9, %esi
	cmpb	$2, %sil
	jne	.L18
	subq	$24, %rax
	cmpq	$3, %rax
	jbe	.L46
	movzwl	24(%r13), %esi
	cmpw	$3, %si
	jbe	.L46
	movzwl	%si, %edi
	cmpq	%rdi, %rax
	jb	.L46
	leaq	24(%r13), %rdi
	xorl	%r9d, %r9d
	movl	%edx, -272(%rbp)
.L26:
	movzwl	2(%rdi), %r10d
	cmpw	$1, %r10w
	je	.L22
	leaq	4(%rdi), %rdx
	cmpw	$2, %r10w
	cmove	%rdx, %r9
	addl	$3, %esi
	andl	$131068, %esi
	subq	%rsi, %rax
	addq	%rsi, %rdi
	cmpq	$3, %rax
	jbe	.L79
	movzwl	(%rdi), %esi
	cmpw	$3, %si
	jbe	.L79
	movzwl	%si, %r10d
	cmpq	%r10, %rax
	jnb	.L26
.L79:
	testq	%r9, %r9
	movl	-272(%rbp), %edx
	jne	.L24
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L84:
	movq	%r8, %rdi
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	testb	%r8b, %r8b
	je	.L10
	movq	-240(%rbp), %r15
	testq	%r15, %r15
	je	.L34
	cmpb	$0, -289(%rbp)
	je	.L34
	movabsq	$8589934592, %rax
	movb	$1, 9(%r15)
	movq	%rax, (%r15)
	movzbl	-290(%rbp), %eax
	movb	%al, 8(%r15)
	movq	-280(%rbp), %rax
	movq	%rax, 16(%r15)
.L35:
	movq	-264(%rbp), %rsp
	movl	%r14d, %edi
	call	__GI___close_nocancel
	movq	cache(%rip), %r8
	movq	%r15, cache(%rip)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L86:
	movq	%rax, %rsi
	movl	%r14d, %edi
	movq	-240(%rbp), %r15
	call	__GI___netlink_assert_response
.L9:
	movq	%r15, %rdi
	call	free@PLT
	movq	-264(%rbp), %rsp
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L22:
	movl	-272(%rbp), %edx
	leaq	4(%rdi), %r9
.L24:
	cmpb	$2, %r11b
	movl	(%r9), %eax
	je	.L88
	testl	%eax, %eax
	jne	.L50
	movl	4(%r9), %edi
	testl	%edi, %edi
	je	.L89
.L50:
	movb	$1, -289(%rbp)
.L20:
	movq	-280(%rbp), %rax
	testq	%rax, %rax
	je	.L52
	movq	-288(%rbp), %rdi
	cmpq	%rdi, %rax
	je	.L52
.L28:
	movq	-240(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L80
	movq	-280(%rbp), %rsi
	movzbl	18(%r13), %eax
	leaq	1(%rsi), %r10
	leaq	(%rsi,%rsi,2), %rsi
	testb	$36, %al
	leaq	(%rdi,%rsi,8), %rsi
	setne	%dil
	shrb	$3, %al
	andl	$2, %eax
	orl	%edi, %eax
	cmpb	$2, %r11b
	movb	%al, 24(%rsi)
	movzbl	17(%r13), %eax
	movb	%al, 25(%rsi)
	movl	20(%r13), %eax
	movl	%eax, 28(%rsi)
	je	.L90
	movdqu	(%r9), %xmm0
	movq	%r10, -280(%rbp)
	movups	%xmm0, 32(%rsi)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L36:
	xorl	%eax, %eax
#APP
# 341 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L37
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 341 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L52:
	salq	-288(%rbp)
	movq	-240(%rbp), %rdi
	movq	-288(%rbp), %rax
	movl	%edx, -308(%rbp)
	movb	%r8b, -291(%rbp)
	movl	%ecx, -296(%rbp)
	movq	%r9, -304(%rbp)
	leaq	3(%rax,%rax,2), %rsi
	movb	%r11b, -272(%rbp)
	salq	$3, %rsi
	call	realloc@PLT
	movl	-308(%rbp), %edx
	movq	%rax, -240(%rbp)
	movzbl	-291(%rbp), %r8d
	movl	-296(%rbp), %ecx
	movq	-304(%rbp), %r9
	movzbl	-272(%rbp), %r11d
	jmp	.L28
.L90:
	movq	-240(%rbp), %rdi
	leaq	(%r10,%r10,2), %rax
	movl	$0, 32(%rsi)
	movl	$0, 36(%rsi)
	movq	%r10, -280(%rbp)
	movl	$-65536, 16(%rdi,%rax,8)
	movl	(%r9), %eax
	movl	%eax, 44(%rsi)
	jmp	.L18
.L88:
	cmpl	$16777343, %eax
	movzbl	-290(%rbp), %eax
	movl	$1, %edi
	cmovne	%edi, %eax
	movb	%al, -290(%rbp)
	jmp	.L20
.L89:
	movl	8(%r9), %esi
	testl	%esi, %esi
	jne	.L50
	movzbl	-289(%rbp), %eax
	cmpl	$16777216, 12(%r9)
	movl	$1, %edi
	cmovne	%edi, %eax
	movb	%al, -289(%rbp)
	jmp	.L20
.L43:
	xorl	%r15d, %r15d
	jmp	.L9
.L80:
	movq	-240(%rbp), %r15
	jmp	.L9
.L46:
	xorl	%r9d, %r9d
	jmp	.L20
.L34:
	movq	%r15, %rdi
	call	free@PLT
#APP
# 281 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	lock;addl $2, 4+noai6ai_cached(%rip)
# 0 "" 2
#NO_APP
	movzbl	-290(%rbp), %eax
	leaq	noai6ai_cached(%rip), %r15
	movb	%al, 8+noai6ai_cached(%rip)
	movzbl	-289(%rbp), %eax
	movb	%al, 9+noai6ai_cached(%rip)
	jmp	.L35
	.size	__check_pf, .-__check_pf
	.p2align 4,,15
	.globl	__free_in6ai
	.hidden	__free_in6ai
	.type	__free_in6ai, @function
__free_in6ai:
	testq	%rdi, %rdi
	je	.L103
	pushq	%rbp
	pushq	%rbx
	leaq	-24(%rdi), %rbp
	subq	$8, %rsp
#APP
# 380 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	lock;addl $-1, -20(%rdi); setz %al
# 0 "" 2
#NO_APP
	testb	%al, %al
	je	.L91
	movq	%rdi, %rbx
#APP
# 382 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L95
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L96:
	movl	-20(%rbx), %eax
	testl	%eax, %eax
	je	.L106
.L97:
#APP
# 388 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L98
	subl	$1, lock(%rip)
.L91:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	rep ret
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L96
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%eax, %eax
#APP
# 388 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L91
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 388 "../sysdeps/unix/sysv/linux/check_pf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L91
	.size	__free_in6ai, .-__free_in6ai
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	freecache, @function
freecache:
	movq	cache(%rip), %rdi
	testq	%rdi, %rdi
	je	.L107
	addq	$24, %rdi
	jmp	__free_in6ai
	.p2align 4,,10
	.p2align 3
.L107:
	rep ret
	.size	freecache, .-freecache
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_freecache__, @object
	.size	__elf_set___libc_subfreeres_element_freecache__, 8
__elf_set___libc_subfreeres_element_freecache__:
	.quad	freecache
	.local	lock
	.comm	lock,4,4
	.local	cache
	.comm	cache,8,8
	.data
	.align 16
	.type	noai6ai_cached, @object
	.size	noai6ai_cached, 24
noai6ai_cached:
	.zero	4
	.long	1
	.zero	8
	.quad	0
	.hidden	__lll_lock_wait_private
	.hidden	__recvmsg
	.hidden	__sendto
	.hidden	__getsockname
	.hidden	__bind
