	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	inet_lnaof
	.type	inet_lnaof, @function
inet_lnaof:
	bswap	%edi
	testl	%edi, %edi
	js	.L2
	movl	%edi, %eax
	andl	$16777215, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edi, %edx
	movzwl	%di, %eax
	movzbl	%dil, %edi
	andl	$-1073741824, %edx
	cmpl	$-2147483648, %edx
	cmovne	%edi, %eax
	ret
	.size	inet_lnaof, .-inet_lnaof
