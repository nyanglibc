	.text
	.p2align 4,,15
	.globl	setipv4sourcefilter
	.type	setipv4sourcefilter, @function
setipv4sourcefilter:
	pushq	%rbp
	movl	%r8d, %eax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	16(,%rax,4), %r12
	pushq	%rbx
	movl	%edi, %r13d
	movl	%esi, %r15d
	movq	%r12, %rdi
	movq	%r9, %r14
	subq	$24, %rsp
	movl	%edx, -60(%rbp)
	movl	%ecx, -56(%rbp)
	movl	%r8d, -52(%rbp)
	call	__libc_alloca_cutoff
	cmpq	$4096, %r12
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %ecx
	movl	-60(%rbp), %edx
	jbe	.L2
	testl	%eax, %eax
	je	.L9
.L2:
	leaq	30(%r12), %rax
	movq	%r14, %rsi
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movl	%edx, (%rbx)
	leaq	16(%rbx), %rdi
	leaq	-16(%r12), %rdx
	movl	%ecx, 8(%rbx)
	movl	%r8d, 12(%rbx)
	movl	%r15d, 4(%rbx)
	call	memcpy@PLT
	movl	%r12d, %r8d
	movq	%rbx, %rcx
	movl	$41, %edx
	xorl	%esi, %esi
	movl	%r13d, %edi
	call	__setsockopt
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L9:
	movq	%r12, %rdi
	movl	%r8d, -60(%rbp)
	movl	%edx, -52(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movl	-52(%rbp), %edx
	movl	-56(%rbp), %ecx
	movl	-60(%rbp), %r8d
	je	.L10
	movl	%edx, (%rax)
	leaq	16(%rax), %rdi
	leaq	-16(%r12), %rdx
	movq	%r14, %rsi
	movl	%r15d, 4(%rax)
	movl	%ecx, 8(%rax)
	movl	%r8d, 12(%rax)
	call	memcpy@PLT
	movl	%r12d, %r8d
	movq	%rbx, %rcx
	movl	%r13d, %edi
	movl	$41, %edx
	xorl	%esi, %esi
	call	__setsockopt
	movq	%rbx, %rdi
	movl	%eax, -52(%rbp)
	call	free@PLT
	movl	-52(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L10:
	orl	$-1, %eax
	jmp	.L1
	.size	setipv4sourcefilter, .-setipv4sourcefilter
	.hidden	__setsockopt
	.hidden	__libc_alloca_cutoff
