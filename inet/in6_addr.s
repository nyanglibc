	.text
	.hidden	__in6addr_loopback
	.globl	__in6addr_loopback
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	__in6addr_loopback, @object
	.size	__in6addr_loopback, 16
__in6addr_loopback:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.weak	in6addr_loopback
	.hidden	in6addr_loopback
	.set	in6addr_loopback,__in6addr_loopback
	.hidden	__in6addr_any
	.globl	__in6addr_any
	.align 16
	.type	__in6addr_any, @object
	.size	__in6addr_any, 16
__in6addr_any:
	.zero	16
	.weak	in6addr_any
	.hidden	in6addr_any
	.set	in6addr_any,__in6addr_any
