	.text
	.p2align 4,,15
	.globl	ether_line
	.type	ether_line, @function
ether_line:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %r10
	xorl	%r8d, %r8d
	movq	%fs:(%rax), %r9
	movsbq	(%rdi), %rax
	movq	%fs:(%r10), %r11
	movl	(%r9,%rax,4), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L70
	leal	-97(%rax), %ecx
	cmpb	$5, %cl
	ja	.L72
.L70:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
.L2:
	movsbl	%al, %eax
	leal	-48(%rax), %ecx
	subl	$87, %eax
	cmpl	$9, %ecx
	cmovbe	%ecx, %eax
	movsbq	1(%rdi), %rcx
	cmpq	$4, %r8
	setbe	%bpl
	movl	(%r9,%rcx,4), %r12d
	cmpb	$58, %r12b
	movsbq	%r12b, %rcx
	je	.L23
	testb	%bpl, %bpl
	je	.L23
.L6:
	leal	-48(%r12), %ecx
	leaq	2(%rdi), %rbx
	cmpb	$9, %cl
	jbe	.L24
	leal	-97(%r12), %ecx
	cmpb	$5, %cl
	ja	.L10
.L24:
	sall	$4, %eax
	movl	%eax, %r13d
	movsbl	%r12b, %eax
	leal	-48(%rax), %r12d
	leal	-87(%rax), %ecx
	cmpl	$9, %r12d
	movl	%r12d, %eax
	cmova	%ecx, %eax
	movzbl	2(%rdi), %ecx
	addl	%r13d, %eax
	cmpb	$58, %cl
	je	.L8
	testb	%bpl, %bpl
	jne	.L10
.L8:
	testb	%cl, %cl
	movb	%al, (%rsi,%r8)
	movq	%rbx, %rdi
	jne	.L21
	addq	$1, %r8
	cmpq	$6, %r8
	je	.L73
.L16:
	movsbq	(%rdi), %rax
	movl	(%r9,%rax,4), %eax
	leal	-48(%rax), %ecx
	cmpb	$9, %cl
	jbe	.L2
	leal	-97(%rax), %ecx
	cmpb	$5, %cl
	jbe	.L2
.L10:
	popq	%rbx
	movl	$-1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$5, %r8
	leaq	1(%rdi), %rbx
	jne	.L8
	testb	%r12b, %r12b
	je	.L8
	testb	$32, 1(%r11,%rcx,2)
	je	.L6
	movb	%al, 5(%rsi)
.L21:
	addq	$1, %r8
	leaq	1(%rbx), %rdi
	cmpq	$6, %r8
	jne	.L16
.L73:
	movsbq	(%rdi), %rcx
	movq	%fs:(%r10), %rsi
	testb	$32, 1(%rsi,%rcx,2)
	movq	%rcx, %rax
	je	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %rdi
	movsbq	(%rdi), %rcx
	testb	$32, 1(%rsi,%rcx,2)
	movq	%rcx, %rax
	jne	.L18
.L17:
	cmpb	$35, %al
	je	.L10
	testb	%al, %al
	je	.L10
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L19
	cmpb	$35, %al
	je	.L19
	movsbq	%al, %rcx
	testb	$32, 1(%rsi,%rcx,2)
	jne	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$1, %rdi
	addq	$1, %rdx
	movb	%al, -1(%rdx)
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L19
	cmpb	$35, %al
	je	.L19
	movq	%fs:(%r10), %rsi
	movsbq	%al, %rcx
	testb	$32, 1(%rsi,%rcx,2)
	je	.L20
.L19:
	popq	%rbx
	xorl	%eax, %eax
	movb	$0, (%rdx)
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$-1, %eax
	ret
	.size	ether_line, .-ether_line
