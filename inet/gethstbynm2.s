	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section .gnu.warning.gethostbyname2
	.previous
#NO_APP
	.p2align 4,,15
	.globl	gethostbyname2
	.type	gethostbyname2, @function
gethostbyname2:
.LFB84:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	$0, 4(%rsp)
	call	__resolv_context_get
	testq	%rax, %rax
	je	.L31
	movq	%rax, %r15
#APP
# 116 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L5:
	cmpq	$0, buffer(%rip)
	je	.L6
.L9:
	leaq	4(%rsp), %r12
	leaq	buffer_size.12408(%rip), %r8
	leaq	buffer(%rip), %rcx
	leaq	resbuf.12409(%rip), %rdx
	xorl	%r9d, %r9d
	movq	%rbx, %rsi
	pushq	%r12
	pushq	%rbp
	movq	%r15, %rdi
	pushq	$0
	leaq	32(%rsp), %r13
	pushq	%r13
	call	__nss_hostname_digits_dots_context
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L11
	movq	buffer(%rip), %rcx
	testq	%rcx, %rcx
	je	.L10
	leaq	resbuf.12409(%rip), %r14
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rax, buffer(%rip)
.L12:
	subq	$8, %rsp
	movq	buffer_size.12408(%rip), %r8
	movq	%r14, %rdx
	pushq	%r12
	movq	%rbx, %rdi
	movq	%r13, %r9
	movl	%ebp, %esi
	call	__gethostbyname2_r
	cmpl	$34, %eax
	movq	buffer(%rip), %rdi
	popq	%rdx
	popq	%rcx
	jne	.L16
	cmpl	$-1, 4(%rsp)
	jne	.L16
	movq	buffer_size.12408(%rip), %rax
	leaq	(%rax,%rax), %rsi
	movq	%rsi, buffer_size.12408(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L14
	movq	buffer(%rip), %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, buffer(%rip)
	movl	$12, %fs:(%rax)
.L10:
	movq	$0, 8(%rsp)
.L11:
#APP
# 163 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L18
	subl	$1, lock(%rip)
.L19:
	movq	%r15, %rdi
	call	__resolv_context_put
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L32
.L20:
	movq	8(%rsp), %rax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	__libc_h_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L16:
	testq	%rdi, %rdi
	jne	.L11
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1024, %edi
	movq	$1024, buffer_size.12408(%rip)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, buffer(%rip)
	je	.L10
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L19
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L5
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L5
.L31:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1
.LFE84:
	.size	gethostbyname2, .-gethostbyname2
	.local	resbuf.12409
	.comm	resbuf.12409,32,32
	.local	buffer_size.12408
	.comm	buffer_size.12408,8,8
	.section	.gnu.warning.gethostbyname2
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_gethostbyname2, @object
	.size	__evoke_link_warning_gethostbyname2, 138
__evoke_link_warning_gethostbyname2:
	.string	"Using 'gethostbyname2' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__resolv_context_put
	.hidden	__gethostbyname2_r
	.hidden	__nss_hostname_digits_dots_context
	.hidden	__resolv_context_get
