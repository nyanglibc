	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___inet_makeaddr
	.hidden	__GI___inet_makeaddr
	.type	__GI___inet_makeaddr, @function
__GI___inet_makeaddr:
	cmpl	$127, %edi
	jbe	.L7
	cmpl	$65535, %edi
	jbe	.L8
	movl	%edi, %eax
	orl	%esi, %eax
	cmpl	$16777215, %edi
	ja	.L3
	sall	$8, %edi
	movzbl	%sil, %eax
	orl	%edi, %eax
	bswap	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	sall	$16, %edi
	movzwl	%si, %eax
	orl	%edi, %eax
.L3:
	bswap	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%esi, %eax
	sall	$24, %edi
	andl	$16777215, %eax
	orl	%edi, %eax
	bswap	%eax
	ret
	.size	__GI___inet_makeaddr, .-__GI___inet_makeaddr
	.globl	__inet_makeaddr
	.set	__inet_makeaddr,__GI___inet_makeaddr
	.weak	inet_makeaddr
	.set	inet_makeaddr,__inet_makeaddr
