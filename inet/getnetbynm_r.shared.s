	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_getnetbyname_r,getnetbyname_r@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getnetbyname_r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__getnetbyname_r
	.hidden	__getnetbyname_r
	.type	__getnetbyname_r, @function
__getnetbyname_r:
.LFB77:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %r12
	movq	%r8, %rbx
	subq	$104, %rsp
	movq	%rdi, 16(%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%r8, 72(%rsp)
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	%rax, 64(%rsp)
	je	.L46
	leaq	88(%rsp), %rax
	leaq	80(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%rax, 32(%rsp)
	movq	%rdi, 40(%rsp)
	call	__GI___nss_networks_lookup2
	testl	%eax, %eax
	movl	%eax, 60(%rsp)
	jne	.L4
	xorl	%ebx, %ebx
	movq	$0, 48(%rsp)
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%fs:0, %r15
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L5:
	testl	%ebx, %ebx
	je	.L8
	cmpl	$1, %eax
	jne	.L9
	movl	$22, %fs:0(%rbp)
	xorl	%ebx, %ebx
	movl	$-1, %r14d
	.p2align 4,,10
	.p2align 3
.L10:
	movq	32(%rsp), %rcx
	movq	40(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%r14d, %r8d
	call	__GI___nss_next2
	testl	%eax, %eax
	jne	.L47
.L15:
	movq	88(%rsp), %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	%r12, %r9
	leaq	(%r15,%rbp), %r8
	movq	%r13, %rcx
	movq	24(%rsp), %rdx
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	call	*88(%rsp)
	cmpl	$-2, %eax
	movl	%eax, %r14d
	jne	.L5
	cmpl	$-1, (%r12)
	je	.L48
.L6:
	testl	%ebx, %ebx
	je	.L8
.L9:
	movq	80(%rsp), %rax
	movl	$22, %fs:0(%rbp)
	movl	8(%rax), %eax
	shrl	$6, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L24
.L13:
	cmpq	$0, 48(%rsp)
	je	.L49
.L12:
	movl	$22, %fs:0(%rbp)
	movl	$-1, %r14d
	movl	$1, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	movq	80(%rsp), %rax
	leal	4(%r14,%r14), %ecx
	movl	8(%rax), %eax
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L25
	cmpl	$1, %r14d
	je	.L13
.L25:
	xorl	%ebx, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$1, %r14d
	movl	$1, %ebx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L48:
	cmpl	$34, %fs:0(%rbp)
	jne	.L6
	movq	48(%rsp), %rdi
	call	free@PLT
	movq	72(%rsp), %rax
	movq	64(%rsp), %rdi
	movq	$0, (%rax)
	call	__GI___resolv_context_put
	movl	%fs:0(%rbp), %eax
	movl	%eax, 60(%rsp)
.L22:
	cmpl	$-1, (%r12)
	je	.L1
	movl	$11, 60(%rsp)
	.p2align 4,,10
	.p2align 3
.L14:
	movl	60(%rsp), %eax
	movl	%eax, %fs:0(%rbp)
.L1:
	movl	60(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movq	48(%rsp), %rdi
	call	free@PLT
	cmpl	$1, %r14d
	je	.L50
	movq	72(%rsp), %rax
	movq	64(%rsp), %rdi
	movq	$0, (%rax)
	call	__GI___resolv_context_put
	cmpl	$1, %r14d
	jbe	.L14
	movl	%fs:0(%rbp), %eax
	cmpl	$34, %eax
	movl	%eax, 60(%rsp)
	jne	.L21
	cmpl	$-2, %r14d
	je	.L22
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L46:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, (%r12)
	movq	$0, (%rbx)
	movl	%fs:(%rax), %eax
	movl	%eax, 60(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L12
	movq	72(%rsp), %rax
	movq	64(%rsp), %rdi
	movl	$12, %fs:0(%rbp)
	movq	$0, (%rax)
	call	__GI___resolv_context_put
	movl	%fs:0(%rbp), %eax
	cmpl	$34, %eax
	movl	%eax, 60(%rsp)
	je	.L29
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	72(%rsp), %rax
	cmpl	$2, %fs:0(%rbp)
	movq	$0, (%rax)
	je	.L51
	movl	$-1, (%r12)
.L44:
	movq	64(%rsp), %rdi
	call	__GI___resolv_context_put
	movl	%fs:0(%rbp), %eax
	cmpl	$34, %eax
	movl	%eax, 60(%rsp)
	jne	.L1
.L29:
	movl	$22, 60(%rsp)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$3, (%r12)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L50:
	movq	72(%rsp), %rax
	movq	8(%rsp), %rdx
	movq	64(%rsp), %rdi
	movq	%rdx, (%rax)
	call	__GI___resolv_context_put
	jmp	.L14
.L21:
	cmpl	$-2, %r14d
	je	.L22
	jmp	.L1
.LFE77:
	.size	__getnetbyname_r, .-__getnetbyname_r
	.globl	__new_getnetbyname_r
	.set	__new_getnetbyname_r,__getnetbyname_r
