	.text
	.p2align 4,,15
	.globl	inet_netof
	.hidden	inet_netof
	.type	inet_netof, @function
inet_netof:
	bswap	%edi
	testl	%edi, %edi
	js	.L2
	movl	%edi, %eax
	shrl	$24, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edi, %edx
	movl	%edi, %eax
	shrl	$8, %edi
	andl	$-1073741824, %edx
	shrl	$16, %eax
	cmpl	$-2147483648, %edx
	cmovne	%edi, %eax
	ret
	.size	inet_netof, .-inet_netof
