	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"deadline.c"
.LC1:
	.string	"result.current.tv_sec >= 0"
	.text
	.p2align 4,,15
	.globl	__deadline_current_time
	.hidden	__deadline_current_time
	.type	__deadline_current_time, @function
__deadline_current_time:
	pushq	%rbx
	movl	$1, %edi
	subq	$16, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__clock_gettime
	testl	%eax, %eax
	jne	.L9
.L2:
	cmpq	$0, (%rsp)
	js	.L10
	movq	(%rsp), %rax
	movq	8(%rsp), %rdx
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	__clock_gettime
	jmp	.L2
.L10:
	leaq	__PRETTY_FUNCTION__.4190(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$33, %edx
	call	__assert_fail
	.size	__deadline_current_time, .-__deadline_current_time
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"__is_timeval_valid_timeout (tv)"
	.text
	.p2align 4,,15
	.globl	__deadline_from_timeval
	.hidden	__deadline_from_timeval
	.type	__deadline_from_timeval, @function
__deadline_from_timeval:
	cmpq	$999999, %rcx
	ja	.L14
	testq	%rdx, %rdx
	jns	.L31
.L14:
	leaq	__PRETTY_FUNCTION__.4199(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	movl	$49, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L31:
	addq	%rdi, %rdx
	jc	.L17
	imull	$1000, %ecx, %ecx
	addl	%ecx, %esi
	cmpl	$999999999, %esi
	jg	.L32
.L16:
	testq	%rdx, %rdx
	js	.L17
	movq	%rdx, %rax
	movslq	%esi, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	cmpq	$-1, %rdx
	jne	.L33
.L17:
	movq	$-1, %rax
	movq	%rax, %rdx
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	subl	$1000000000, %esi
	addq	$1, %rdx
	jmp	.L16
	.size	__deadline_from_timeval, .-__deadline_from_timeval
	.section	.rodata.str1.1
.LC3:
	.string	"sec > 0"
	.text
	.p2align 4,,15
	.globl	__deadline_to_ms
	.hidden	__deadline_to_ms
	.type	__deadline_to_ms, @function
__deadline_to_ms:
	testq	%rcx, %rcx
	js	.L43
	cmpq	%rdx, %rdi
	jg	.L42
	cmpq	%rsi, %rcx
	jg	.L45
	cmpq	%rdx, %rdi
	je	.L42
.L45:
	subq	%rdi, %rdx
	cmpq	$2147483646, %rdx
	movq	%rdx, %rdi
	jg	.L43
	subl	%esi, %ecx
	js	.L52
.L37:
	leal	999999(%rcx), %esi
	cmpl	$1000000000, %esi
	jle	.L39
	leal	-999000001(%rcx), %esi
	addq	$1, %rdi
.L39:
	cmpq	$2147483, %rdi
	movl	$2147483647, %eax
	jg	.L34
	movl	%esi, %eax
	movl	$1125899907, %edx
	sarl	$31, %esi
	imull	%edx
	imull	$1000, %edi, %edi
	movl	%edx, %eax
	movl	$2147483647, %edx
	sarl	$18, %eax
	subl	%esi, %eax
	addl	%edi, %eax
	cmovs	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$2147483647, %eax
.L34:
	rep ret
	.p2align 4,,10
	.p2align 3
.L52:
	testq	%rdx, %rdx
	je	.L53
	subq	$1, %rdi
	addl	$1000000000, %ecx
	jmp	.L37
.L53:
	leaq	__PRETTY_FUNCTION__.4209(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	subq	$8, %rsp
	movl	$95, %edx
	call	__assert_fail
	.size	__deadline_to_ms, .-__deadline_to_ms
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.4209, @object
	.size	__PRETTY_FUNCTION__.4209, 17
__PRETTY_FUNCTION__.4209:
	.string	"__deadline_to_ms"
	.align 16
	.type	__PRETTY_FUNCTION__.4199, @object
	.size	__PRETTY_FUNCTION__.4199, 24
__PRETTY_FUNCTION__.4199:
	.string	"__deadline_from_timeval"
	.align 16
	.type	__PRETTY_FUNCTION__.4190, @object
	.size	__PRETTY_FUNCTION__.4190, 24
__PRETTY_FUNCTION__.4190:
	.string	"__deadline_current_time"
	.hidden	__assert_fail
	.hidden	__clock_gettime
