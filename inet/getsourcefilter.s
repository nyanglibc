	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/getsourcefilter.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"sol_map[cnt].sol != -1"
	.text
	.p2align 4,,15
	.globl	__get_sol
	.hidden	__get_sol
	.type	__get_sol, @function
__get_sol:
	leaq	sol_map(%rip), %rdx
	movl	$-1, %eax
	xorl	%ecx, %ecx
	leaq	84(%rdx), %r8
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$12, %rdx
	cmpq	%r8, %rdx
	je	.L1
.L15:
	movl	(%rdx), %ecx
	cmpl	$-1, %ecx
	je	.L14
.L2:
	cmpl	%esi, 8(%rdx)
	jne	.L3
	cmpl	%edi, 4(%rdx)
	je	.L6
	cmpl	$-1, %eax
	cmove	%ecx, %eax
	addq	$12, %rdx
	cmpq	%r8, %rdx
	jne	.L15
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ecx, %eax
	ret
.L14:
	leaq	__PRETTY_FUNCTION__.5556(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$70, %edx
	call	__assert_fail
	.size	__get_sol, .-__get_sol
	.p2align 4,,15
	.globl	getsourcefilter
	.type	getsourcefilter, @function
getsourcefilter:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbx
	movl	%ecx, %r13d
	movq	%r9, %r15
	subq	$56, %rsp
	movl	(%r9), %r12d
	movl	%edi, -76(%rbp)
	movl	%esi, -72(%rbp)
	movq	%r8, -88(%rbp)
	movl	%r12d, %edx
	sall	$7, %edx
	leal	144(%rdx), %ebx
	movq	%rbx, %rdi
	movl	%ebx, -52(%rbp)
	movl	%ebx, -68(%rbp)
	call	__libc_alloca_cutoff
	movl	-68(%rbp), %edx
	movl	-72(%rbp), %esi
	cmpl	$4096, %edx
	jbe	.L17
	testl	%eax, %eax
	je	.L26
.L17:
	addq	$30, %rbx
	movl	$1, -68(%rbp)
	shrq	$4, %rbx
	salq	$4, %rbx
	subq	%rbx, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
.L19:
	leaq	8(%rbx), %rdi
	movl	%esi, (%rbx)
	movl	%r13d, %edx
	movq	%r14, %rsi
	call	memcpy@PLT
	movzwl	(%r14), %edi
	movl	%r12d, 140(%rbx)
	movl	%r13d, %esi
	call	__get_sol
	cmpl	$-1, %eax
	movl	%eax, %r12d
	jne	.L20
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L21:
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	jne	.L16
	movq	%rbx, %rdi
	call	free@PLT
.L16:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	-76(%rbp), %edi
	leaq	-52(%rbp), %r8
	movq	%rbx, %rcx
	movl	$48, %edx
	movl	%eax, %esi
	call	__getsockopt
	testl	%eax, %eax
	movl	%eax, %r12d
	jne	.L21
	movl	136(%rbx), %eax
	movq	-88(%rbp), %rcx
	leaq	144(%rbx), %rsi
	movl	140(%rbx), %r13d
	movq	16(%rbp), %rdi
	movl	%eax, (%rcx)
	movl	(%r15), %edx
	movq	%rdx, %rcx
	movq	%rdx, %rax
	movl	%r13d, %edx
	salq	$7, %rax
	salq	$7, %rdx
	cmpl	%r13d, %ecx
	cmovb	%rax, %rdx
	call	memcpy@PLT
	movl	%r13d, (%r15)
	jmp	.L21
.L26:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L24
	movl	$0, -68(%rbp)
	movl	-72(%rbp), %esi
	jmp	.L19
.L24:
	orl	$-1, %r12d
	jmp	.L16
	.size	getsourcefilter, .-getsourcefilter
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.5556, @object
	.size	__PRETTY_FUNCTION__.5556, 10
__PRETTY_FUNCTION__.5556:
	.string	"__get_sol"
	.section	.rodata
	.align 32
	.type	sol_map, @object
	.size	sol_map, 84
sol_map:
	.long	0
	.long	2
	.long	16
	.long	41
	.long	10
	.long	28
	.long	257
	.long	3
	.long	16
	.long	256
	.long	4
	.long	16
	.long	258
	.long	5
	.long	16
	.long	260
	.long	11
	.long	28
	.long	263
	.long	17
	.long	20
	.hidden	__getsockopt
	.hidden	__libc_alloca_cutoff
	.hidden	__assert_fail
