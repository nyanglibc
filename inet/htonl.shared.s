 .text
.globl htonl
.type htonl,@function
.align 1<<4
htonl:
 movl %edi, %eax
 bswap %eax
 ret
.size htonl,.-htonl
.weak ntohl
ntohl = htonl
