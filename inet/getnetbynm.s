	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section .gnu.warning.getnetbyname
	.previous
#NO_APP
	.p2align 4,,15
	.globl	getnetbyname
	.type	getnetbyname, @function
getnetbyname:
.LFB70:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$24, %rsp
	movl	$0, 4(%rsp)
#APP
# 116 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	buffer(%rip), %rdx
	movq	buffer_size.11543(%rip), %rbx
	testq	%rdx, %rdx
	je	.L27
.L5:
	leaq	4(%rsp), %r14
	leaq	8(%rsp), %r13
	leaq	resbuf.11544(%rip), %r12
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, buffer(%rip)
.L15:
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%rbx, %rcx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__getnetbyname_r
	cmpl	$34, %eax
	movq	buffer(%rip), %r15
	jne	.L9
	cmpl	$-1, 4(%rsp)
	jne	.L9
	movq	buffer_size.11543(%rip), %rax
	movq	%r15, %rdi
	leaq	(%rax,%rax), %rbx
	movq	%rbx, %rsi
	movq	%rbx, buffer_size.11543(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L7
	movq	%r15, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, buffer(%rip)
	movl	$12, %fs:(%rax)
.L11:
	movq	$0, 8(%rsp)
.L16:
#APP
# 163 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, lock(%rip)
.L13:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	movq	__libc_h_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
.L14:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	testq	%r15, %r15
	jne	.L16
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1024, %edi
	movq	$1024, buffer_size.11543(%rip)
	movl	$1024, %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, buffer(%rip)
	jne	.L5
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
.LFE70:
	.size	getnetbyname, .-getnetbyname
	.local	resbuf.11544
	.comm	resbuf.11544,24,16
	.local	buffer_size.11543
	.comm	buffer_size.11543,8,8
	.section	.gnu.warning.getnetbyname
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getnetbyname, @object
	.size	__evoke_link_warning_getnetbyname, 136
__evoke_link_warning_getnetbyname:
	.string	"Using 'getnetbyname' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__getnetbyname_r
