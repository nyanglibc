	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d.%d.%d.%d"
#NO_APP
	.text
	.p2align 4,,15
	.globl	inet_ntoa
	.type	inet_ntoa, @function
inet_ntoa:
	pushq	%rbx
	movq	buffer@gottpoff(%rip), %rbx
	movl	%edi, %r9d
	addq	%fs:0, %rbx
	movl	%edi, %eax
	movzbl	%dil, %ecx
	subq	$8, %rsp
	shrl	$24, %edi
	movzbl	%ah, %eax
	pushq	%rdi
	leaq	.LC0(%rip), %rdx
	shrl	$16, %r9d
	movl	%eax, %r8d
	movzbl	%r9b, %r9d
	movl	$18, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	__GI___snprintf
	popq	%rax
	movq	%rbx, %rax
	popq	%rdx
	popq	%rbx
	ret
	.size	inet_ntoa, .-inet_ntoa
	.section	.tbss,"awT",@nobits
	.type	buffer, @object
	.size	buffer, 18
buffer:
	.zero	18
