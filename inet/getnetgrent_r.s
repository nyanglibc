	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"endnetgrent"
	.text
	.p2align 4,,15
	.type	endnetgrent_hook, @function
endnetgrent_hook:
.LFB72:
	movq	80(%rdi), %rax
	leaq	-1(%rax), %rdx
	cmpq	$-3, %rdx
	jbe	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L12:
	pushq	%rbx
	leaq	.LC0(%rip), %rsi
	movq	%rdi, %rbx
	movq	%rax, %rdi
	call	__nss_lookup_function
	testq	%rax, %rax
	je	.L3
	movq	%rbx, %rdi
	call	*%rax
.L3:
	movq	$0, 80(%rbx)
	popq	%rbx
	ret
.LFE72:
	.size	endnetgrent_hook, .-endnetgrent_hook
	.section	.rodata.str1.1
.LC1:
	.string	"setnetgrent"
.LC2:
	.string	"getnetgrent_r.c"
.LC3:
	.string	"datap->data == NULL"
	.text
	.p2align 4,,15
	.type	__internal_setnetgrent_reuse, @function
__internal_setnetgrent_reuse:
.LFB73:
	pushq	%r15
	pushq	%r14
	movl	$-1, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rsi, %rdi
	leaq	80(%rbx), %r13
	subq	$40, %rsp
	leaq	24(%rsp), %r14
	movq	%rdx, 8(%rsp)
	call	endnetgrent_hook
	leaq	.LC1(%rip), %rsi
	xorl	%edx, %edx
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	__nss_netgroup_lookup2
	testl	%eax, %eax
	jne	.L15
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	$0, 32(%rbx)
	jne	.L37
.L16:
	movq	24(%rsp), %rdi
	call	_dl_mcount_wrapper_check
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	*24(%rsp)
	leaq	.LC1(%rip), %rsi
	movl	%eax, %r15d
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%r14, %rcx
	movq	%r13, %rdi
	movq	80(%rbx), %rbp
	call	__nss_next2
	cmpl	$1, %r15d
	jne	.L17
	testl	%eax, %eax
	jne	.L17
	leaq	.LC0(%rip), %rsi
	movq	%rbp, %rdi
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L20
	movq	%rax, %rdi
	call	_dl_mcount_wrapper_check
	movq	%rbx, %rdi
	call	*%rbp
	cmpq	$0, 32(%rbx)
	je	.L16
.L37:
	leaq	__PRETTY_FUNCTION__.11986(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$103, %edx
	call	__assert_fail
	.p2align 4,,10
	.p2align 3
.L17:
	testl	%eax, %eax
	je	.L20
.L15:
	movq	%r12, %rdi
	call	strlen
	leaq	9(%rax), %rdi
	movq	%rax, %r13
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L38
	movq	64(%rbx), %rax
	leaq	8(%rbp), %rdi
	leaq	1(%r13), %rdx
	movq	%r12, %rsi
	movq	%rax, 0(%rbp)
	call	memcpy@PLT
	xorl	%eax, %eax
	cmpl	$1, %r15d
	movq	%rbp, 64(%rbx)
	sete	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	8(%rsp), %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, (%rcx)
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.LFE73:
	.size	__internal_setnetgrent_reuse, .-__internal_setnetgrent_reuse
	.p2align 4,,15
	.type	free_memory.isra.1, @function
free_memory.isra.1:
.LFB83:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L53
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rdi), %rax
	movq	%rax, 0(%rbp)
	call	free@PLT
	movq	0(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L41
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L54
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rdi), %rax
	movq	%rax, (%rbx)
	call	free@PLT
.L53:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L43
.L54:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE83:
	.size	free_memory.isra.1, .-free_memory.isra.1
	.p2align 4,,15
	.globl	__internal_setnetgrent
	.hidden	__internal_setnetgrent
	.type	__internal_setnetgrent, @function
__internal_setnetgrent:
.LFB74:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	leaq	72(%rsi), %rsi
	leaq	64(%rbx), %rdi
	subq	$8, %rsp
	call	free_memory.isra.1
	movq	__libc_errno@gottpoff(%rip), %rdx
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	addq	%fs:0, %rdx
	popq	%rbx
	popq	%rbp
	jmp	__internal_setnetgrent_reuse
.LFE74:
	.size	__internal_setnetgrent, .-__internal_setnetgrent
	.p2align 4,,15
	.globl	setnetgrent
	.type	setnetgrent, @function
setnetgrent:
.LFB76:
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 171 "getnetgrent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L58
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L59:
	leaq	72+dataset(%rip), %rsi
	leaq	-8(%rsi), %rdi
	call	free_memory.isra.1
	movq	__libc_errno@gottpoff(%rip), %rdx
	addq	%fs:0, %rdx
	leaq	dataset(%rip), %rsi
	movq	%rbx, %rdi
	call	__internal_setnetgrent_reuse
	movl	%eax, %r8d
#APP
# 177 "getnetgrent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L60
	subl	$1, lock(%rip)
.L57:
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L59
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%eax, %eax
#APP
# 177 "getnetgrent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L57
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 177 "getnetgrent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L57
.LFE76:
	.size	setnetgrent, .-setnetgrent
	.p2align 4,,15
	.globl	__internal_endnetgrent
	.hidden	__internal_endnetgrent
	.type	__internal_endnetgrent, @function
__internal_endnetgrent:
.LFB77:
	pushq	%rbx
	movq	%rdi, %rbx
	call	endnetgrent_hook
	leaq	72(%rbx), %rsi
	leaq	64(%rbx), %rdi
	popq	%rbx
	jmp	free_memory.isra.1
.LFE77:
	.size	__internal_endnetgrent, .-__internal_endnetgrent
	.p2align 4,,15
	.globl	endnetgrent
	.type	endnetgrent, @function
endnetgrent:
.LFB78:
	subq	$8, %rsp
#APP
# 195 "getnetgrent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L66
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L67:
	leaq	dataset(%rip), %rdi
	call	endnetgrent_hook
	leaq	72+dataset(%rip), %rsi
	leaq	-8(%rsi), %rdi
	call	free_memory.isra.1
#APP
# 199 "getnetgrent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L68
	subl	$1, lock(%rip)
.L65:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L67
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%eax, %eax
#APP
# 199 "getnetgrent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L65
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 199 "getnetgrent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L65
.LFE78:
	.size	endnetgrent, .-endnetgrent
	.section	.rodata.str1.1
.LC4:
	.string	"getnetgrent_r"
	.text
	.p2align 4,,15
	.globl	__internal_getnetgrent_r
	.hidden	__internal_getnetgrent_r
	.type	__internal_getnetgrent_r, @function
__internal_getnetgrent_r:
.LFB79:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	80(%rcx), %rax
	movq	112(%rsp), %rbx
	testq	%rax, %rax
	je	.L94
	movq	%rsi, 32(%rsp)
	leaq	.LC4(%rip), %rsi
	movq	%rdi, 24(%rsp)
	movq	%rax, %rdi
	movq	%r9, 8(%rsp)
	movq	%r8, (%rsp)
	movq	%rcx, %r13
	movq	%rdx, 40(%rsp)
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L94
	movq	__libc_errno@gottpoff(%rip), %r15
	addq	%fs:0, %r15
.L89:
	movq	%r12, %rdi
	call	_dl_mcount_wrapper_check
	movq	%r15, %rcx
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%r13, %rdi
	call	*%r12
	cmpl	$2, %eax
	movl	%eax, %ebp
	je	.L123
	testl	%eax, %eax
	jne	.L78
.L123:
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L80
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L124:
	testl	%eax, %eax
	jne	.L81
.L80:
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, 72(%r13)
	movq	64(%r13), %rax
	movq	%rax, (%rdi)
	movq	%rdi, 64(%r13)
	addq	$8, %rdi
	call	__internal_setnetgrent_reuse
	movq	72(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L124
	testl	%eax, %eax
	je	.L82
.L81:
	movq	80(%r13), %rdi
	testq	%rdi, %rdi
	je	.L82
	leaq	.LC4(%rip), %rsi
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L89
.L82:
	cmpl	$1, %ebp
	jne	.L94
	movq	8(%r13), %rbp
.L84:
	movq	24(%rsp), %rax
	movq	32(%rsp), %rcx
	movq	40(%rsp), %rbx
	movq	%rbp, (%rax)
	movq	16(%r13), %rax
	movq	%rax, (%rcx)
	movq	24(%r13), %rax
	movq	%rax, (%rbx)
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	cmpl	$1, %eax
	jne	.L94
	cmpl	$1, 0(%r13)
	movq	8(%r13), %rbp
	jne	.L84
	movq	64(%r13), %r14
	testq	%r14, %r14
	je	.L85
	.p2align 4,,10
	.p2align 3
.L86:
	leaq	8(%r14), %rsi
	movq	%rbp, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L89
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.L86
.L85:
	movq	72(%r13), %rax
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, %rdx
	je	.L88
.L87:
	leaq	8(%rdx), %rsi
	movq	%rbp, %rdi
	movq	%rdx, 16(%rsp)
	call	strcmp
	testl	%eax, %eax
	je	.L89
	movq	16(%rsp), %rdx
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L87
.L88:
	movq	%rbp, %rdi
	call	strlen
	leaq	9(%rax), %rdi
	movq	%rax, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	16(%rsp), %rdx
	je	.L94
	leaq	8(%rcx), %rdi
	movq	%r14, (%rcx)
	addq	$1, %rdx
	movq	%rbp, %rsi
	movq	%rcx, 16(%rsp)
	call	memcpy@PLT
	movq	16(%rsp), %rcx
	movq	%rcx, 72(%r13)
	jmp	.L89
.LFE79:
	.size	__internal_getnetgrent_r, .-__internal_getnetgrent_r
	.p2align 4,,15
	.globl	__getnetgrent_r
	.hidden	__getnetgrent_r
	.type	__getnetgrent_r, @function
__getnetgrent_r:
.LFB80:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
#APP
# 345 "getnetgrent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L126
	movl	$1, %edi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edi, lock(%rip)
# 0 "" 2
#NO_APP
.L127:
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	subq	$8, %rsp
	movq	%r8, %r9
	movq	%rcx, %r8
	leaq	dataset(%rip), %rcx
	movq	%rbx, %rdi
	pushq	%rax
	call	__internal_getnetgrent_r
	movl	%eax, %r8d
#APP
# 350 "getnetgrent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L128
	subl	$1, lock(%rip)
.L125:
	addq	$32, %rsp
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	xorl	%eax, %eax
	movl	$1, %edi
	lock cmpxchgl	%edi, lock(%rip)
	je	.L127
	leaq	lock(%rip), %rdi
	movq	%r8, 24(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	__lll_lock_wait_private
	movq	24(%rsp), %r8
	movq	16(%rsp), %rcx
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L128:
	xorl	%eax, %eax
#APP
# 350 "getnetgrent_r.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L125
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 350 "getnetgrent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L125
.LFE80:
	.size	__getnetgrent_r, .-__getnetgrent_r
	.weak	getnetgrent_r
	.set	getnetgrent_r,__getnetgrent_r
	.section	.rodata.str1.1
.LC5:
	.string	"entry.data == NULL"
	.text
	.p2align 4,,15
	.globl	innetgr
	.hidden	innetgr
	.type	innetgr, @function
innetgr:
.LFB81:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rdx, %r15
	subq	$1208, %rsp
	leaq	80(%rsp), %rbp
	movq	%rdi, 48(%rsp)
	movq	%rcx, 32(%rsp)
	movl	$11, %ecx
	movq	%rbx, 16(%rsp)
	movq	%rbp, %rdi
	rep stosq
	leaq	72(%rsp), %rax
	movq	%rax, 24(%rsp)
	leaq	176(%rsp), %rax
	movq	%rax, 8(%rsp)
.L149:
	movq	24(%rsp), %rcx
	leaq	80(%rbp), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%edx, %edx
	call	__nss_netgroup_lookup2
	testl	%eax, %eax
	je	.L148
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L135:
	movq	160(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L151
	movq	%rax, %rdi
	call	_dl_mcount_wrapper_check
	movq	%rbp, %rdi
	call	*%r12
.L151:
	movq	24(%rsp), %rcx
	leaq	80(%rbp), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%ebx, %r8d
	call	__nss_next2
	testl	%eax, %eax
	jne	.L132
.L148:
	cmpq	$0, 112(%rsp)
	jne	.L187
	movq	72(%rsp), %rdi
	call	_dl_mcount_wrapper_check
	movq	%rbp, %rsi
	movq	16(%rsp), %rdi
	call	*72(%rsp)
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L135
	movq	160(%rsp), %rdi
	leaq	.LC4(%rip), %rsi
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L135
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	movq	%rax, (%rsp)
	.p2align 4,,10
	.p2align 3
.L136:
	movq	%r12, %rdi
	call	_dl_mcount_wrapper_check
	movq	(%rsp), %rcx
	movl	$1024, %edx
	movq	8(%rsp), %rsi
	movq	%rbp, %rdi
	call	*%r12
	cmpl	$1, %eax
	movl	%eax, %r13d
	jne	.L188
	cmpl	$1, 80(%rsp)
	movq	88(%rsp), %rbx
	je	.L189
	testq	%r14, %r14
	je	.L144
	testq	%rbx, %rbx
	je	.L144
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__strcasecmp
	testl	%eax, %eax
	jne	.L136
.L144:
	testq	%r15, %r15
	movq	96(%rsp), %rdi
	je	.L145
	testq	%rdi, %rdi
	je	.L145
	movq	%r15, %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L136
.L145:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L143
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L143
	movq	%rax, %rsi
	call	__strcasecmp
	testl	%eax, %eax
	jne	.L136
.L143:
	movq	160(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	call	__nss_lookup_function
	testq	%rax, %rax
	jne	.L190
.L186:
	xorl	%ebx, %ebx
	cmpl	$1, %r13d
	sete	%bl
.L153:
	leaq	72(%rbp), %rsi
	leaq	64(%rbp), %rdi
	call	free_memory.isra.1
	addq	$1208, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	movq	144(%rsp), %r13
	testq	%r13, %r13
	je	.L138
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	8(%r13), %rsi
	movq	%rbx, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L136
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L140
.L138:
	movq	152(%rsp), %rax
	testq	%rax, %rax
	movq	%rax, 40(%rsp)
	movq	%rax, %r13
	je	.L142
.L141:
	leaq	8(%r13), %rsi
	movq	%rbx, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L136
	movq	0(%r13), %r13
	testq	%r13, %r13
	jne	.L141
.L142:
	movq	48(%rsp), %rdi
	movq	%rbx, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L136
	movq	%rbx, %rdi
	call	strlen
	leaq	1(%rax), %rdx
	leaq	9(%rax), %rdi
	movq	%rdx, 56(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L158
	movq	40(%rsp), %rax
	movq	56(%rsp), %rdx
	leaq	8(%r13), %rdi
	movq	%rbx, %rsi
	movq	%rax, 0(%r13)
	call	memcpy@PLT
	movq	%r13, 152(%rsp)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L132:
	movq	152(%rsp), %rax
	testq	%rax, %rax
	je	.L191
	movq	(%rax), %rdx
	movq	%rdx, 152(%rsp)
	movq	144(%rsp), %rdx
	movq	%rdx, (%rax)
	movq	%rax, 144(%rsp)
	addq	$8, %rax
	movq	%rax, 16(%rsp)
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L188:
	movq	160(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	call	__nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L155
	movq	%rbx, %rdi
	call	_dl_mcount_wrapper_check
	movq	%rbp, %rdi
	call	*%rbx
.L155:
	movl	$2, %ebx
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L191:
	xorl	%ebx, %ebx
	jmp	.L153
.L158:
	movl	$-1, %r13d
	jmp	.L143
.L187:
	leaq	__PRETTY_FUNCTION__.12198(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$397, %edx
	call	__assert_fail
.L190:
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	_dl_mcount_wrapper_check
	movq	%rbp, %rdi
	movq	(%rsp), %rax
	call	*%rax
	jmp	.L186
.LFE81:
	.size	innetgr, .-innetgr
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.12198, @object
	.size	__PRETTY_FUNCTION__.12198, 8
__PRETTY_FUNCTION__.12198:
	.string	"innetgr"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11986, @object
	.size	__PRETTY_FUNCTION__.11986, 29
__PRETTY_FUNCTION__.11986:
	.string	"__internal_setnetgrent_reuse"
	.local	dataset
	.comm	dataset,88,32
	.local	lock
	.comm	lock,4,4
	.hidden	__strcasecmp
	.hidden	strcmp
	.hidden	__lll_lock_wait_private
	.hidden	strlen
	.hidden	__assert_fail
	.hidden	__nss_next2
	.hidden	_dl_mcount_wrapper_check
	.hidden	__nss_netgroup_lookup2
	.hidden	__nss_lookup_function
