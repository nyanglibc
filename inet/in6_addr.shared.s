	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	__GI___in6addr_loopback
	.globl	__GI___in6addr_loopback
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	__GI___in6addr_loopback, @object
	.size	__GI___in6addr_loopback, 16
__GI___in6addr_loopback:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.globl	__in6addr_loopback
	.set	__in6addr_loopback,__GI___in6addr_loopback
	.weak	__GI_in6addr_loopback
	.hidden	__GI_in6addr_loopback
	.set	__GI_in6addr_loopback,__in6addr_loopback
	.weak	in6addr_loopback
	.set	in6addr_loopback,__GI_in6addr_loopback
	.hidden	__GI___in6addr_any
	.globl	__GI___in6addr_any
	.align 16
	.type	__GI___in6addr_any, @object
	.size	__GI___in6addr_any, 16
__GI___in6addr_any:
	.zero	16
	.globl	__in6addr_any
	.set	__in6addr_any,__GI___in6addr_any
	.weak	__GI_in6addr_any
	.hidden	__GI_in6addr_any
	.set	__GI_in6addr_any,__in6addr_any
	.weak	in6addr_any
	.set	in6addr_any,__GI_in6addr_any
