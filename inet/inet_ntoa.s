	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d.%d.%d.%d"
	.text
	.p2align 4,,15
	.globl	inet_ntoa
	.type	inet_ntoa, @function
inet_ntoa:
	pushq	%rbx
	movq	%fs:0, %rbx
	movl	%edi, %r9d
	movl	%edi, %eax
	movzbl	%dil, %ecx
	shrl	$24, %edi
	subq	$8, %rsp
	movzbl	%ah, %eax
	leaq	.LC0(%rip), %rdx
	pushq	%rdi
	addq	$buffer@tpoff, %rbx
	shrl	$16, %r9d
	movl	%eax, %r8d
	movq	%rbx, %rdi
	movzbl	%r9b, %r9d
	movl	$18, %esi
	xorl	%eax, %eax
	call	__snprintf
	popq	%rax
	movq	%rbx, %rax
	popq	%rdx
	popq	%rbx
	ret
	.size	inet_ntoa, .-inet_ntoa
	.section	.tbss,"awT",@nobits
	.type	buffer, @object
	.size	buffer, 18
buffer:
	.zero	18
	.hidden	__snprintf
