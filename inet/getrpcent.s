	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section .gnu.warning.getrpcent
	.previous
#NO_APP
	.p2align 4,,15
	.globl	getrpcent
	.type	getrpcent, @function
getrpcent:
.LFB70:
	pushq	%rbx
#APP
# 81 "../nss/getXXent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	leaq	buffer_size.10506(%rip), %r8
	leaq	buffer(%rip), %rdx
	leaq	resbuf.10510(%rip), %rsi
	leaq	__getrpcent_r(%rip), %rdi
	xorl	%r9d, %r9d
	movl	$1024, %ecx
	call	__nss_getent
	movq	__libc_errno@gottpoff(%rip), %r8
	movq	%rax, %r9
	movl	%fs:(%r8), %ebx
#APP
# 89 "../nss/getXXent.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	subl	$1, lock(%rip)
.L5:
	movl	%ebx, %fs:(%r8)
	movq	%r9, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
#APP
# 89 "../nss/getXXent.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L5
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 89 "../nss/getXXent.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L5
.LFE70:
	.size	getrpcent, .-getrpcent
	.local	buffer_size.10506
	.comm	buffer_size.10506,8,8
	.local	resbuf.10510
	.comm	resbuf.10510,24,16
	.section	.gnu.warning.getrpcent
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getrpcent, @object
	.size	__evoke_link_warning_getrpcent, 133
__evoke_link_warning_getrpcent:
	.string	"Using 'getrpcent' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__nss_getent
	.hidden	__getrpcent_r
