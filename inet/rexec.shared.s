	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d"
.LC1:
	.string	"rexec: strdup"
.LC2:
	.string	"rexec: socket"
.LC3:
	.string	""
.LC4:
	.string	"getsockname"
.LC5:
	.string	"%u"
.LC6:
	.string	"accept"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_rexec_af
	.hidden	__GI_rexec_af
	.type	__GI_rexec_af, @function
__GI_rexec_af:
	pushq	%r15
	pushq	%r14
	rolw	$8, %si
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%r9, %r15
	subq	$472, %rsp
	leaq	96(%rsp), %rbp
	movq	%rdx, 72(%rsp)
	movq	%rdx, 8(%rsp)
	leaq	.LC0(%rip), %rdx
	movq	%rcx, 64(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rbp, %rdi
	movzwl	%si, %ecx
	movl	$32, %esi
	movq	%r8, 24(%rsp)
	movl	528(%rsp), %ebx
	call	__GI___snprintf
	pxor	%xmm0, %xmm0
	leaq	160(%rsp), %rdx
	movq	(%r12), %rdi
	leaq	88(%rsp), %rcx
	movzwl	%bx, %ebx
	movq	%rbp, %rsi
	movb	$0, 127(%rsp)
	movl	%ebx, 164(%rsp)
	movups	%xmm0, 172(%rsp)
	movl	$0, 44(%rdx)
	movl	$1, 168(%rsp)
	movups	%xmm0, 28(%rdx)
	movl	$2, 160(%rsp)
	call	__GI_getaddrinfo
	testl	%eax, %eax
	jne	.L34
	movl	%eax, %r13d
	movq	88(%rsp), %rax
	cmpq	$0, 32(%rax)
	je	.L3
	movq	ahostbuf(%rip), %rdi
	call	free@PLT
	movq	88(%rsp), %rax
	movq	32(%rax), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, ahostbuf(%rip)
	je	.L55
	movq	%rax, (%r12)
	movq	88(%rsp), %rax
	leaq	64(%rsp), %rdx
	leaq	72(%rsp), %rsi
	movl	$1, %ebx
	movq	32(%rax), %rdi
	call	__GI_ruserpass
.L5:
	movq	88(%rsp), %rax
	xorl	%edx, %edx
	movl	8(%rax), %esi
	movl	4(%rax), %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %r14d
	js	.L56
	movq	88(%rsp), %rax
	movl	%r14d, %edi
	movl	16(%rax), %edx
	movq	24(%rax), %rsi
	call	__GI___connect
	testl	%eax, %eax
	jns	.L7
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$111, %fs:(%rax)
	jne	.L8
	cmpl	$16, %ebx
	jg	.L8
	movl	%r14d, %edi
	call	__GI___close
	movl	%ebx, %edi
	addl	%ebx, %ebx
	call	__sleep
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	movq	88(%rsp), %rax
	movl	$-1, %r14d
	movq	32(%rax), %rdi
	call	__GI_perror
.L1:
	addq	$472, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	.LC2(%rip), %rdi
	movl	$-1, %r14d
	call	__GI_perror
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%r15, %r15
	je	.L57
	movq	88(%rsp), %rax
	xorl	%edx, %edx
	movl	8(%rax), %esi
	movl	4(%rax), %edi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, 36(%rsp)
	js	.L58
	movl	36(%rsp), %edi
	movl	$1, %esi
	leaq	336(%rsp), %rbx
	call	__listen
	movl	36(%rsp), %edi
	leaq	80(%rsp), %rdx
	movq	%rbx, %rsi
	movl	$128, 80(%rsp)
	call	__getsockname
	testl	%eax, %eax
	js	.L59
	movzwl	336(%rsp), %edi
	call	__GI___libc_sa_len
	cmpl	80(%rsp), %eax
	jne	.L60
	subq	$8, %rsp
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	pushq	$2
	movl	%eax, %esi
	movl	$32, %r9d
	movq	%rbp, %r8
	movq	%rbx, %rdi
	call	__GI_getnameinfo
	popq	%rdx
	popq	%rcx
	xorl	%esi, %esi
	testl	%eax, %eax
	movw	%si, 50(%rsp)
	je	.L61
.L15:
	leaq	128(%rsp), %rax
	leaq	.LC5(%rip), %rsi
	movl	%r13d, %edx
	movq	%rax, %rbp
	movq	%rax, 40(%rsp)
	movq	%rax, %rdi
	xorl	%eax, %eax
	call	__GI_sprintf
	movq	%rbp, %rdx
.L16:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L16
	movl	%eax, %ecx
	movq	40(%rsp), %rsi
	movl	%r14d, %edi
	shrl	$16, %ecx
	testl	$32896, %eax
	leaq	208(%rsp), %r13
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	leaq	84(%rsp), %rbp
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%rsi, %rdx
	addq	$1, %rdx
	call	__GI___write
	movq	%r12, 56(%rsp)
	movl	$128, 84(%rsp)
	movl	36(%rsp), %r12d
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L63:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L62
.L19:
	movq	%rbp, %rdx
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	__GI_accept
	cmpl	$-1, %eax
	je	.L63
	movl	36(%rsp), %edi
	movl	%eax, 52(%rsp)
	movq	56(%rsp), %r12
	call	__GI___close
	movl	52(%rsp), %edx
	testl	%edx, %edx
	js	.L33
	movl	%edx, (%r15)
.L10:
	movq	72(%rsp), %rdi
	movq	%rdi, 336(%rsp)
	call	__GI_strlen
	movq	64(%rsp), %rdi
	addq	$1, %rax
	movq	%rax, 344(%rsp)
	movq	%rdi, 352(%rsp)
	call	__GI_strlen
	addq	$1, %rax
	movq	%rax, 360(%rsp)
	movq	24(%rsp), %rax
	movq	%rax, %rdi
	movq	%rax, 368(%rsp)
	call	__GI_strlen
	addq	$1, %rax
	movq	%rax, 376(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L64:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L23
.L24:
	movl	$3, %edx
	movq	%rbx, %rsi
	movl	%r14d, %edi
	call	__GI___writev
	cmpq	$-1, %rax
	je	.L64
.L23:
	movq	72(%rsp), %rdi
	cmpq	8(%rsp), %rdi
	je	.L25
	call	free@PLT
.L25:
	movq	64(%rsp), %rdi
	cmpq	16(%rsp), %rdi
	je	.L26
	call	free@PLT
.L26:
	movq	40(%rsp), %rsi
	movl	$1, %edx
	movl	%r14d, %edi
	call	__GI___read
	cmpq	$1, %rax
	jne	.L65
	cmpb	$0, 128(%rsp)
	movq	40(%rsp), %rbx
	jne	.L29
	movq	88(%rsp), %rdi
	call	__GI_freeaddrinfo
	jmp	.L1
.L32:
	movl	$1, %edx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	__GI___write
	cmpb	$10, 128(%rsp)
	je	.L28
.L29:
	movl	$1, %edx
	movq	%rbx, %rsi
	movl	%r14d, %edi
	call	__GI___read
	cmpq	$1, %rax
	je	.L32
.L28:
	cmpw	$0, 50(%rsp)
	je	.L22
	movl	(%r15), %edi
	call	__GI___close
.L22:
	movl	%r14d, %edi
	movl	$-1, %r14d
	call	__GI___close
	movq	88(%rsp), %rdi
	call	__GI_freeaddrinfo
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L62:
	movl	36(%rsp), %edi
	call	__GI___close
.L33:
	leaq	.LC6(%rip), %rdi
	call	__GI_perror
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC3(%rip), %rsi
	movl	%r14d, %edi
	movl	$1, %edx
	leaq	336(%rsp), %rbx
	call	__GI___write
	leaq	128(%rsp), %rax
	xorl	%edi, %edi
	movw	%di, 50(%rsp)
	movq	%rax, 40(%rsp)
	jmp	.L10
.L61:
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	__GI_strtol
	movw	%ax, 50(%rsp)
	movzwl	%ax, %r13d
	jmp	.L15
.L34:
	movl	$-1, %r14d
	jmp	.L1
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, (%r12)
	movl	$-1, %r14d
	movl	$2, %fs:(%rax)
	jmp	.L1
.L55:
	leaq	.LC1(%rip), %rdi
	movl	$-1, %r14d
	call	__GI_perror
	jmp	.L1
.L60:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	36(%rsp), %edi
	movl	$22, %fs:(%rax)
	call	__GI___close
	jmp	.L22
.L65:
	movq	(%r12), %rdi
	call	__GI_perror
	jmp	.L28
.L59:
	leaq	.LC4(%rip), %rdi
	call	__GI_perror
	movl	36(%rsp), %edi
	call	__GI___close
	jmp	.L22
.L58:
	movl	%r14d, %edi
	movl	$-1, %r14d
	call	__GI___close
	jmp	.L1
	.size	__GI_rexec_af, .-__GI_rexec_af
	.globl	rexec_af
	.set	rexec_af,__GI_rexec_af
	.p2align 4,,15
	.globl	rexec
	.type	rexec, @function
rexec:
	subq	$16, %rsp
	pushq	$2
	call	__GI_rexec_af
	addq	$24, %rsp
	ret
	.size	rexec, .-rexec
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	ahostbuf, @object
	.size	ahostbuf, 8
ahostbuf:
	.zero	8
	.comm	rexecoptions,4,4
	.hidden	__getsockname
	.hidden	__listen
	.hidden	__sleep
