	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	gethostbyaddr
	.type	gethostbyaddr, @function
gethostbyaddr:
.LFB70:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	$0, 20(%rsp)
#APP
# 116 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	buffer(%rip), %r8
	movq	buffer_size.11534(%rip), %r9
	testq	%r8, %r8
	je	.L27
.L5:
	leaq	20(%rsp), %r15
	leaq	24(%rsp), %r14
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, buffer(%rip)
.L15:
	pushq	%r15
	leaq	resbuf.11535(%rip), %rcx
	pushq	%r14
	movl	%r12d, %edx
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__gethostbyaddr_r
	cmpl	$34, %eax
	movq	buffer(%rip), %r13
	popq	%rdx
	popq	%rcx
	jne	.L9
	cmpl	$-1, 20(%rsp)
	jne	.L9
	movq	buffer_size.11534(%rip), %rax
	movq	%r13, %rdi
	leaq	(%rax,%rax), %r9
	movq	%r9, %rsi
	movq	%r9, buffer_size.11534(%rip)
	movq	%r9, 8(%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	movq	8(%rsp), %r9
	jne	.L7
	movq	%r13, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, buffer(%rip)
	movl	$12, %fs:(%rax)
.L11:
	movq	$0, 24(%rsp)
.L16:
#APP
# 163 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, lock(%rip)
.L13:
	movl	20(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	movq	__libc_h_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
.L14:
	movq	24(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	testq	%r13, %r13
	jne	.L16
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1024, %edi
	movq	$1024, buffer_size.11534(%rip)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	movq	%rax, buffer(%rip)
	movl	$1024, %r9d
	jne	.L5
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
.LFE70:
	.size	gethostbyaddr, .-gethostbyaddr
	.local	resbuf.11535
	.comm	resbuf.11535,32,32
	.local	buffer_size.11534
	.comm	buffer_size.11534,8,8
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__gethostbyaddr_r
