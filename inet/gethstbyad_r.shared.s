	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_gethostbyaddr_r,gethostbyaddr_r@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gethostbyaddr_r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__gethostbyaddr_r
	.hidden	__gethostbyaddr_r
	.type	__gethostbyaddr_r, @function
__gethostbyaddr_r:
.LFB77:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movl	%esi, 12(%rsp)
	movl	%edx, 64(%rsp)
	movq	%rcx, 24(%rsp)
	movq	%r8, 32(%rsp)
	movq	%r9, 16(%rsp)
	movq	168(%rsp), %r12
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	%rax, 72(%rsp)
	je	.L50
	cmpl	$16, 12(%rsp)
	jne	.L4
	movq	8+__GI___in6addr_any(%rip), %rdx
	movq	__GI___in6addr_any(%rip), %rax
	xorq	8(%r13), %rdx
	xorq	0(%r13), %rax
	orq	%rax, %rdx
	je	.L51
.L4:
	leaq	88(%rsp), %rax
	leaq	80(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%edx, %edx
	movq	%rax, %rcx
	movq	%rax, 48(%rsp)
	movq	%rdi, 40(%rsp)
	call	__GI___nss_hosts_lookup2
	testl	%eax, %eax
	movl	%eax, 68(%rsp)
	jne	.L7
	xorl	%ebx, %ebx
	movq	$0, 56(%rsp)
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%fs:0, %r15
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%ebx, %ebx
	je	.L11
	cmpl	$1, %r14d
	jne	.L12
	movl	$22, %fs:0(%rbp)
	xorl	%ebx, %ebx
	movl	$-1, %r14d
	.p2align 4,,10
	.p2align 3
.L13:
	movq	48(%rsp), %rcx
	movq	40(%rsp), %rdi
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%r14d, %r8d
	call	__GI___nss_next2
	testl	%eax, %eax
	jne	.L52
.L18:
	movq	88(%rsp), %rdi
	call	__GI__dl_mcount_wrapper_check
	leaq	(%r15,%rbp), %rax
	pushq	%r12
	movq	%r13, %rdi
	pushq	%rax
	movq	32(%rsp), %r9
	movq	48(%rsp), %r8
	movq	40(%rsp), %rcx
	movl	80(%rsp), %edx
	movl	28(%rsp), %esi
	call	*104(%rsp)
	movl	%eax, %r14d
	cmpl	$-2, %r14d
	popq	%rax
	popq	%rdx
	jne	.L8
	cmpl	$-1, (%r12)
	je	.L53
.L9:
	testl	%ebx, %ebx
	je	.L11
.L12:
	movq	80(%rsp), %rax
	movl	$22, %fs:0(%rbp)
	movl	8(%rax), %eax
	shrl	$6, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L27
.L16:
	cmpq	$0, 56(%rsp)
	je	.L54
.L15:
	movl	$22, %fs:0(%rbp)
	movl	$-1, %r14d
	movl	$1, %ebx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movq	80(%rsp), %rax
	leal	4(%r14,%r14), %ecx
	movl	8(%rax), %eax
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L28
	cmpl	$1, %r14d
	je	.L16
.L28:
	xorl	%ebx, %ebx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1, %r14d
	movl	$1, %ebx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L53:
	cmpl	$34, %fs:0(%rbp)
	jne	.L9
	movq	56(%rsp), %rdi
	call	free@PLT
	movq	160(%rsp), %rax
	movq	72(%rsp), %rdi
	movq	$0, (%rax)
	call	__GI___resolv_context_put
	movl	%fs:0(%rbp), %eax
	movl	%eax, 68(%rsp)
.L25:
	cmpl	$-1, (%r12)
	je	.L1
	movl	$11, 68(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L52:
	movq	56(%rsp), %rdi
	call	free@PLT
	cmpl	$1, %r14d
	je	.L55
	movq	160(%rsp), %rax
	movq	72(%rsp), %rdi
	movq	$0, (%rax)
	call	__GI___resolv_context_put
	cmpl	$1, %r14d
	ja	.L56
.L17:
	movl	68(%rsp), %eax
	movl	%eax, %fs:0(%rbp)
.L1:
	movl	68(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	24(%rsp), %rbx
	movq	160(%rsp), %rsi
	movq	%rbx, %rdi
	movq	%rbx, (%rsi)
	call	_res_hconf_reorder_addrs@PLT
	movq	%rbx, %rdi
	call	_res_hconf_trim_domains@PLT
	movq	72(%rsp), %rdi
	call	__GI___resolv_context_put
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L54:
	movq	16(%rsp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 56(%rsp)
	jne	.L15
	movq	160(%rsp), %rax
	movl	$12, %fs:0(%rbp)
	movq	$0, (%rax)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	160(%rsp), %rax
	cmpl	$2, %fs:0(%rbp)
	movq	$0, (%rax)
	je	.L57
	movl	$-1, (%r12)
.L47:
	movq	72(%rsp), %rdi
	call	__GI___resolv_context_put
	movl	%fs:0(%rbp), %eax
	cmpl	$34, %eax
	movl	%eax, 68(%rsp)
	jne	.L1
.L32:
	movl	$22, 68(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L50:
	movq	160(%rsp), %rax
	movl	$-1, (%r12)
	movq	$0, (%rax)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 68(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$3, (%r12)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L51:
	movq	160(%rsp), %rax
	movl	$1, (%r12)
	movl	$2, 68(%rsp)
	movq	$0, (%rax)
	jmp	.L1
.L56:
	movl	%fs:0(%rbp), %eax
	cmpl	$34, %eax
	movl	%eax, 68(%rsp)
	je	.L58
	cmpl	$-2, %r14d
	je	.L25
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	$-2, %r14d
	je	.L25
	jmp	.L32
.LFE77:
	.size	__gethostbyaddr_r, .-__gethostbyaddr_r
	.globl	__new_gethostbyaddr_r
	.set	__new_gethostbyaddr_r,__gethostbyaddr_r
