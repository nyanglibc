	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__check_native
	.hidden	__check_native
	.type	__check_native, @function
__check_native:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	-172(%rbp), %r12
	pushq	%rbx
	subq	$184, %rsp
	movl	%edi, -184(%rbp)
	movq	%rsi, -192(%rbp)
	movl	%edx, -180(%rbp)
	movl	$524291, %esi
	xorl	%edx, %edx
	movl	$16, %edi
	movq	%rcx, -200(%rbp)
	call	__GI___socket
	xorl	%esi, %esi
	movl	$16, %edi
	testl	%eax, %eax
	movq	$0, -170(%rbp)
	movw	%di, -172(%rbp)
	movw	%si, 10(%r12)
	movl	$12, -176(%rbp)
	jns	.L34
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$12, %edx
	movq	%r12, %rsi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	__bind
	testl	%eax, %eax
	jne	.L1
	leaq	-176(%rbp), %rdx
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	__getsockname
	testl	%eax, %eax
	jne	.L1
	leaq	-112(%rbp), %r13
	movabsq	$216454334399905812, %rax
	movl	$5, %edi
	movl	-168(%rbp), %r14d
	movq	%rax, -144(%rbp)
	movq	%r13, %rsi
	movl	$0, -132(%rbp)
	call	__GI___clock_gettime
	movq	-112(%rbp), %rax
	subq	$4112, %rsp
	leaq	-144(%rbp), %rdi
	xorl	%edx, %edx
	movl	$16, %ecx
	movb	$0, -128(%rbp)
	movb	$0, -125(%rbp)
	movq	$0, 2(%r12)
	movq	%rdi, %r15
	movl	%eax, -136(%rbp)
	xorl	%eax, %eax
	movw	%dx, 10(%r12)
	movw	%ax, -127(%rbp)
	leaq	15(%rsp), %rax
	movw	%cx, -172(%rbp)
	movq	$4096, -152(%rbp)
	andq	$-16, %rax
	movq	%rax, -208(%rbp)
	movq	%rax, -160(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L35:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L1
.L4:
	xorl	%ecx, %ecx
	movl	$12, %r9d
	movq	%r12, %r8
	movl	$20, %edx
	movq	%r15, %rsi
	movl	%ebx, %edi
	call	__sendto
	cmpq	$-1, %rax
	je	.L35
	testq	%rax, %rax
	js	.L1
	leaq	-160(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r12, -112(%rbp)
	movl	$12, -104(%rbp)
	movq	%r15, -96(%rbp)
	movq	$1, -88(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -72(%rbp)
	movl	$0, -64(%rbp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L37:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L36
.L8:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%ebx, %edi
	call	__recvmsg
	cmpq	$-1, %rax
	je	.L37
	movq	%rax, %rsi
	movl	%ebx, %edi
	movq	%rax, -216(%rbp)
	call	__GI___netlink_assert_response
	movq	-216(%rbp), %rax
	testq	%rax, %rax
	js	.L1
	testb	$32, -64(%rbp)
	jne	.L1
	cmpq	$15, %rax
	jle	.L6
	movq	-208(%rbp), %rsi
	movl	(%rsi), %edx
	cmpl	$15, %edx
	jbe	.L6
	movl	%edx, %ecx
	cmpq	%rax, %rcx
	ja	.L6
	movl	-168(%rbp), %edi
	movl	-136(%rbp), %r8d
	movq	%rsi, %rcx
	xorl	%r9d, %r9d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L15:
	cmpw	$3, %si
	movl	$1, %esi
	cmove	%esi, %r9d
	.p2align 4,,10
	.p2align 3
.L14:
	addl	$3, %edx
	andl	$-4, %edx
	subq	%rdx, %rax
	addq	%rdx, %rcx
	cmpq	$15, %rax
	jbe	.L21
	movl	(%rcx), %edx
	cmpl	$15, %edx
	jbe	.L21
	movl	%edx, %esi
	cmpq	%rsi, %rax
	jb	.L21
.L22:
	testl	%edi, %edi
	jne	.L14
	cmpl	12(%rcx), %r14d
	jne	.L14
	cmpl	%r8d, 8(%rcx)
	jne	.L14
	movzwl	4(%rcx), %esi
	cmpw	$16, %si
	jne	.L15
	movzwl	18(%rcx), %r10d
	leal	-768(%r10), %esi
	cmpw	$1, %si
	seta	%r11b
	xorl	%esi, %esi
	cmpw	$776, %r10w
	setne	%sil
	movl	-184(%rbp), %r10d
	andl	%r11d, %esi
	movl	20(%rcx), %r11d
	cmpl	%r10d, %r11d
	je	.L16
	cmpl	$-1, %r10d
	sete	%r10b
	cmpl	-180(%rbp), %r11d
	je	.L38
.L18:
	cmpl	$-1, -180(%rbp)
	sete	%sil
	andl	%esi, %r10d
.L20:
	testb	%r10b, %r10b
	je	.L14
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%ebx, %edi
	call	__GI___close_nocancel
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	testb	%r9b, %r9b
	je	.L6
	jmp	.L23
.L5:
	.p2align 4,,10
	.p2align 3
.L36:
	movl	%ebx, %edi
	movq	%rax, %rsi
	call	__GI___netlink_assert_response
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	-180(%rbp), %r10d
	cmpl	%r10d, -184(%rbp)
	movq	-192(%rbp), %r11
	movl	%esi, (%r11)
	je	.L39
	movl	$1, %r10d
	movl	$-1, -184(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-200(%rbp), %r11
	movl	$-1, -180(%rbp)
	movl	%esi, (%r11)
	jmp	.L20
.L39:
	movq	-200(%rbp), %rax
	movl	%esi, (%rax)
	jmp	.L23
	.size	__check_native, .-__check_native
	.hidden	__recvmsg
	.hidden	__sendto
	.hidden	__getsockname
	.hidden	__bind
