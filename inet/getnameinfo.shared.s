	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.type	checked_copy, @function
checked_copy:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rdx, %rdi
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	call	__GI_strlen
	leaq	1(%rax), %rdx
	cmpq	%rbp, %rdx
	ja	.L3
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__GI_memcpy@PLT
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-12, %eax
	jmp	.L1
	.size	checked_copy, .-checked_copy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"localhost"
	.text
	.p2align 4,,15
	.type	nrl_domainname, @function
nrl_domainname:
	movl	not_first.12745(%rip), %edi
	testl	%edi, %edi
	jne	.L59
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1112, %rsp
#APP
# 94 "getnameinfo.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock.12746(%rip)
# 0 "" 2
#NO_APP
.L10:
	movl	not_first.12745(%rip), %esi
	testl	%esi, %esi
	jne	.L11
	leaq	-1088(%rbp), %rbx
	movq	$1024, -1080(%rbp)
	movl	$1, not_first.12745(%rip)
	movl	$1024, %ecx
	leaq	-1136(%rbp), %r12
	leaq	-1128(%rbp), %r13
	leaq	16(%rbx), %rdx
	leaq	-1120(%rbp), %r14
	leaq	.LC0(%rip), %r15
	movq	%rdx, -1088(%rbp)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-1080(%rbp), %rcx
	movq	-1088(%rbp), %rdx
.L12:
	movq	%r12, %r9
	movq	%r13, %r8
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__gethostbyname_r
	testl	%eax, %eax
	je	.L13
	cmpl	$-1, -1136(%rbp)
	jne	.L13
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L13
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L63
.L15:
	movq	-1088(%rbp), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L11
	call	free@PLT
.L11:
#APP
# 181 "getnameinfo.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L31
	subl	$1, lock.12746(%rip)
.L8:
	movq	domain(%rip), %rax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movq	domain(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock.12746(%rip)
	je	.L10
	leaq	lock.12746(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L13:
	movq	-1128(%rbp), %rax
	testq	%rax, %rax
	je	.L18
	movq	(%rax), %rdi
	movl	$46, %esi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L18
.L62:
	leaq	1(%rax), %rdi
	call	__GI___strdup
	movq	%rax, domain(%rip)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	movq	-1080(%rbp), %rsi
	movq	-1088(%rbp), %rdi
	call	__gethostname
	testl	%eax, %eax
	je	.L64
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L18
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	xorl	%eax, %eax
#APP
# 181 "getnameinfo.c" 1
	xchgl %eax, lock.12746(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L8
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock.12746(%rip), %rdi
	movl	$202, %eax
#APP
# 181 "getnameinfo.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L64:
	movq	-1088(%rbp), %r15
	movl	$46, %esi
	movq	%r15, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L62
	movq	%r15, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rdx
	addq	$31, %rax
	movq	%r15, %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	call	__GI_memcpy@PLT
	movq	%rax, -1144(%rbp)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L65:
	movq	-1088(%rbp), %r15
.L22:
	movq	-1080(%rbp), %rcx
	movq	-1144(%rbp), %rdi
	movq	%r12, %r9
	movq	%r13, %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	__gethostbyname_r
	testl	%eax, %eax
	je	.L23
	cmpl	$-1, -1136(%rbp)
	jne	.L23
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L23
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L65
	jmp	.L15
.L23:
	movq	-1128(%rbp), %rax
	testq	%rax, %rax
	je	.L25
	movq	(%rax), %rdi
	movl	$46, %esi
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L62
.L25:
	leaq	-1132(%rbp), %r15
	movl	$16777343, -1132(%rbp)
	.p2align 4,,10
	.p2align 3
.L26:
	movq	-1080(%rbp), %r9
	movq	-1088(%rbp), %r8
	movq	%r14, %rcx
	pushq	%r12
	pushq	%r13
	movl	$2, %edx
	movl	$4, %esi
	movq	%r15, %rdi
	call	__gethostbyaddr_r
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L27
	cmpl	$-1, -1136(%rbp)
	jne	.L27
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L27
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L26
	jmp	.L15
.L27:
	movq	-1128(%rbp), %rax
	testq	%rax, %rax
	je	.L15
	movq	(%rax), %rdi
	movl	$46, %esi
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L62
	jmp	.L15
	.size	nrl_domainname, .-nrl_domainname
	.section	.rodata.str1.1
.LC1:
	.string	"%c%s"
.LC2:
	.string	"%c%u"
.LC3:
	.string	"udp"
.LC4:
	.string	"tcp"
.LC5:
	.string	"%d"
	.text
	.p2align 4,,15
	.globl	__GI_getnameinfo
	.hidden	__GI_getnameinfo
	.type	__GI_getnameinfo, @function
__GI_getnameinfo:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1528, %rsp
	movl	1584(%rsp), %r14d
	andl	$-256, %r14d
	jne	.L139
	testq	%rdi, %rdi
	je	.L218
	cmpl	$1, %esi
	jbe	.L218
	movl	1584(%rsp), %r10d
	andl	$8, %r10d
	je	.L68
	testq	%rdx, %rdx
	jne	.L68
	testq	%r8, %r8
	je	.L135
.L68:
	movzwl	(%rdi), %eax
	movl	%r9d, %r13d
	movq	%r8, %r12
	movl	%ecx, %r11d
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	cmpw	$2, %ax
	je	.L69
	cmpw	$10, %ax
	je	.L71
	cmpw	$1, %ax
	je	.L239
.L218:
	movl	$-6, %r14d
.L66:
	addq	$1528, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	cmpl	$27, %esi
	jbe	.L218
.L70:
	leaq	480(%rsp), %r15
	testq	%rbp, %rbp
	movq	$1024, 488(%rsp)
	leaq	16(%r15), %r8
	movq	%r8, 480(%rsp)
	je	.L111
	testl	%r11d, %r11d
	jne	.L240
.L111:
	testq	%r12, %r12
	je	.L117
	testl	%r13d, %r13d
	jne	.L241
.L117:
	movq	480(%rsp), %rdi
.L238:
	leaq	16(%r15), %r10
	cmpq	%r10, %rdi
	je	.L66
	call	free@PLT
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L69:
	cmpl	$15, %esi
	ja	.L70
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L239:
	leaq	480(%rsp), %r15
	testq	%rbp, %rbp
	movq	$1024, 488(%rsp)
	leaq	16(%r15), %rax
	movq	%rax, 480(%rsp)
	je	.L75
	testl	%r11d, %r11d
	je	.L75
	testb	$1, 1584(%rsp)
	je	.L242
	testl	%r10d, %r10d
	jne	.L135
.L116:
	leaq	.LC0(%rip), %rdx
	movl	%r11d, %esi
	movq	%rbp, %rdi
	call	checked_copy
.L115:
	testl	%eax, %eax
	je	.L111
.L229:
	movq	480(%rsp), %rdi
	movl	%eax, %r14d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L241:
	movzwl	(%rbx), %eax
	cmpw	$2, %ax
	je	.L119
	cmpw	$10, %ax
	je	.L119
	cmpw	$1, %ax
	je	.L120
	movl	$-6, %r14d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L240:
	testb	$1, 1584(%rsp)
	je	.L243
	testl	%r10d, %r10d
	jne	.L135
.L136:
	cmpw	$10, %ax
	je	.L244
	leaq	4(%rbx), %rsi
	movl	%r11d, %ecx
	movq	%rbp, %rdx
	movl	$2, %edi
	call	__GI_inet_ntop
	testq	%rax, %rax
	jne	.L111
.L230:
	movq	480(%rsp), %rdi
	movl	$-12, %r14d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L75:
	testq	%r12, %r12
	je	.L66
	testl	%r13d, %r13d
	je	.L66
.L120:
	leaq	2(%rbx), %rdx
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	checked_copy
.L127:
	testl	%eax, %eax
	je	.L117
.L217:
	movl	%eax, %r14d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L119:
	testb	$2, 1584(%rsp)
	jne	.L126
	testb	$16, 1584(%rsp)
	leaq	.LC4(%rip), %rax
	leaq	.LC3(%rip), %rbp
	leaq	72(%rsp), %rcx
	movq	%r12, (%rsp)
	movl	%r13d, 8(%rsp)
	movq	%rcx, %r12
	cmove	%rax, %rbp
	leaq	80(%rsp), %rax
	movq	%rax, %r13
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	je	.L144
.L122:
	movzwl	2(%rbx), %edi
	movq	488(%rsp), %r8
	movq	%r12, %r9
	movq	480(%rsp), %rcx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	call	__getservbyport_r
	cmpl	$34, %eax
	je	.L124
	movq	72(%rsp), %rax
	movq	(%rsp), %r12
	movl	8(%rsp), %r13d
	testq	%rax, %rax
	je	.L126
	movq	(%rax), %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	checked_copy
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L126:
	movzwl	2(%rbx), %ecx
	leaq	.LC5(%rip), %rdx
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%r12, %rdi
	rolw	$8, %cx
	movzwl	%cx, %ecx
	call	__GI___snprintf
	testl	%eax, %eax
	js	.L145
	cltq
	cmpq	%r13, %rax
	jb	.L117
	movl	$-12, %r14d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L244:
	leaq	8(%rbx), %rsi
	movl	%r11d, %ecx
	movq	%rbp, %rdx
	movl	$10, %edi
	movl	%r11d, (%rsp)
	call	__GI_inet_ntop
	testq	%rax, %rax
	movl	(%rsp), %r11d
	je	.L230
	movl	24(%rbx), %r8d
	testl	%r8d, %r8d
	movl	%r8d, (%rsp)
	je	.L111
	movl	%r11d, %edx
	movq	%rbp, %rdi
	movq	%rdx, %rsi
	movq	%rdx, 8(%rsp)
	call	__GI___strnlen
	movq	8(%rsp), %rdx
	leaq	0(%rbp,%rax), %r9
	movl	(%rsp), %r8d
	subq	%rax, %rdx
	movl	8(%rbx), %eax
	movq	%rdx, %rbp
	andl	$49407, %eax
	cmpl	$33022, %eax
	je	.L106
	cmpb	$-1, 8(%rbx)
	je	.L245
.L107:
	leaq	.LC2(%rip), %rdx
	xorl	%eax, %eax
	movl	$37, %ecx
	movq	%rbp, %rsi
	movq	%r9, %rdi
	call	__GI___snprintf
	testl	%eax, %eax
	js	.L231
.L112:
	cltq
	cmpq	%rax, %rbp
	ja	.L111
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L135:
	movl	$-2, %r14d
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L243:
	cmpw	$10, %ax
	movq	$0, 64(%rsp)
	je	.L246
	leaq	4(%rbx), %rax
	leaq	80(%rsp), %rcx
	leaq	60(%rsp), %rdx
	leaq	64(%rsp), %rsi
	movl	%r14d, 8(%rsp)
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r12, 40(%rsp)
	movl	$1024, %r9d
	movl	%r10d, (%rsp)
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movl	%r11d, 36(%rsp)
	movq	%rcx, %r12
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L247:
	movq	488(%rsp), %r9
	movq	480(%rsp), %r8
.L88:
	pushq	%rbx
	pushq	%r14
	movq	%r12, %rcx
	movl	$2, %edx
	movl	$4, %esi
	movq	%rbp, %rdi
	call	__gethostbyaddr_r
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L225
	cmpl	$-1, 60(%rsp)
	jne	.L222
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L223
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L247
.L87:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movl	60(%rsp), %edx
	movl	$-10, %r14d
	movq	480(%rsp), %rdi
	movl	%edx, %fs:(%rax)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	80(%rsp), %rax
	movl	%r11d, 8(%rsp)
	movl	%r10d, (%rsp)
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)
	call	__GI_uname
	testl	%eax, %eax
	movl	(%rsp), %r10d
	movl	8(%rsp), %r11d
	je	.L248
	testl	%r10d, %r10d
	je	.L116
.L232:
	movq	480(%rsp), %rdi
	movl	$-2, %r14d
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$-10, %eax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L139:
	movl	$-1, %r14d
	jmp	.L66
.L246:
	leaq	8(%rbx), %rax
	leaq	80(%rsp), %rcx
	leaq	60(%rsp), %rdx
	leaq	64(%rsp), %rsi
	movl	%r14d, 8(%rsp)
	movq	%rbx, 16(%rsp)
	movq	%rbp, 24(%rsp)
	movq	%r12, 40(%rsp)
	movl	$1024, %r9d
	movl	%r10d, (%rsp)
	movq	%rsi, %r14
	movq	%rdx, %rbx
	movq	%rax, %rbp
	movl	%r11d, 36(%rsp)
	movq	%rcx, %r12
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L249:
	movq	488(%rsp), %r9
	movq	480(%rsp), %r8
.L82:
	pushq	%rbx
	pushq	%r14
	movl	$16, %esi
	movq	%rbp, %rdi
	movq	%r12, %rcx
	movl	$10, %edx
	call	__gethostbyaddr_r
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	je	.L225
	cmpl	$-1, 60(%rsp)
	jne	.L222
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L223
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L249
	jmp	.L87
.L248:
	movq	16(%rsp), %rdx
	movl	%r11d, %esi
	movq	%rbp, %rdi
	addq	$65, %rdx
	call	checked_copy
	jmp	.L115
.L225:
	cmpq	$0, 64(%rsp)
	movl	(%rsp), %r10d
	movl	8(%rsp), %r14d
	movq	16(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	36(%rsp), %r11d
	movq	40(%rsp), %r12
	jne	.L86
	movl	60(%rsp), %eax
	cmpl	$-1, %eax
	je	.L131
.L91:
	cmpl	$2, %eax
	je	.L250
.L93:
	testl	%r10d, %r10d
	jne	.L232
	movzwl	(%rbx), %eax
	jmp	.L136
.L86:
	testb	$4, 1584(%rsp)
	jne	.L94
.L226:
	movq	64(%rsp), %rax
	movq	(%rax), %rcx
.L95:
	testb	$32, 1584(%rsp)
	jne	.L251
.L97:
	movq	%rcx, %rdi
	movl	%r11d, 8(%rsp)
	movq	%rcx, 72(%rsp)
	movq	%rcx, (%rsp)
	call	__GI_strlen
	movl	8(%rsp), %r11d
	leaq	1(%rax), %rdx
	movq	(%rsp), %rcx
	cmpq	%rdx, %r11
	jb	.L230
	movq	%rcx, %rsi
	movq	%rbp, %rdi
	call	__GI_memcpy@PLT
	jmp	.L111
.L222:
	cmpq	$0, 64(%rsp)
	movl	(%rsp), %r10d
	movl	8(%rsp), %r14d
	movq	16(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	36(%rsp), %r11d
	movq	40(%rsp), %r12
	jne	.L86
	movl	60(%rsp), %eax
	jmp	.L91
.L223:
	cmpq	$0, 64(%rsp)
	movl	(%rsp), %r10d
	movl	8(%rsp), %r14d
	movq	16(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	36(%rsp), %r11d
	movq	40(%rsp), %r12
	jne	.L86
.L131:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movq	480(%rsp), %rdi
	movl	$-11, %r14d
	movl	$-1, %fs:(%rax)
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L251:
	leaq	72(%rsp), %rax
	movq	%rcx, %rdi
	movl	%r11d, 8(%rsp)
	movl	%r10d, (%rsp)
	movq	%rax, %rsi
	call	__GI___idna_from_dns_encoding
	cmpl	$-105, %eax
	movl	(%rsp), %r10d
	movl	8(%rsp), %r11d
	je	.L252
	testl	%eax, %eax
	je	.L253
	cmpl	$-2, %eax
	jne	.L229
	jmp	.L93
.L94:
	movl	%r11d, 8(%rsp)
	movl	%r10d, (%rsp)
	call	nrl_domainname
	testq	%rax, %rax
	movl	(%rsp), %r10d
	movl	8(%rsp), %r11d
	je	.L226
	movq	64(%rsp), %rdx
	movq	%rax, %rsi
	movl	%r11d, 16(%rsp)
	movl	%r10d, 8(%rsp)
	movq	(%rdx), %rcx
	movq	%rcx, %rdi
	movq	%rcx, (%rsp)
	call	__GI_strstr
	movq	(%rsp), %rcx
	movl	8(%rsp), %r10d
	movl	16(%rsp), %r11d
	cmpq	%rax, %rcx
	je	.L95
	testq	%rax, %rax
	je	.L95
	cmpb	$46, -1(%rax)
	jne	.L95
	movb	$0, -1(%rax)
	jmp	.L226
.L245:
	movzbl	9(%rbx), %eax
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L107
.L106:
	leaq	80(%rsp), %rax
	movl	%r8d, %edi
	movq	%r9, 8(%rsp)
	movl	%r8d, (%rsp)
	movq	%rax, %rsi
	movq	%rax, 16(%rsp)
	call	__GI_if_indextoname
	testq	%rax, %rax
	movl	(%rsp), %r8d
	movq	8(%rsp), %r9
	je	.L107
	movq	16(%rsp), %r8
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movl	$37, %ecx
	movq	%rbp, %rsi
	movq	%r9, %rdi
	call	__GI___snprintf
	testl	%eax, %eax
	jns	.L112
.L231:
	movq	480(%rsp), %rdi
	movl	$-11, %r14d
	jmp	.L238
.L253:
	movq	72(%rsp), %rax
	movl	%r11d, 8(%rsp)
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	__GI_strlen
	movl	8(%rsp), %r11d
	leaq	1(%rax), %rdx
	cmpq	%rdx, %r11
	jb	.L230
	movq	(%rsp), %rsi
	movq	%rbp, %rdi
	call	__GI_memcpy@PLT
	movq	(%rsp), %rdi
	call	free@PLT
	jmp	.L111
.L252:
	movq	64(%rsp), %rax
	movq	(%rax), %rcx
	jmp	.L97
.L145:
	movl	$-11, %r14d
	jmp	.L117
.L250:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movq	480(%rsp), %rdi
	movl	$-3, %r14d
	movl	$2, %fs:(%rax)
	jmp	.L238
	.size	__GI_getnameinfo, .-__GI_getnameinfo
	.globl	getnameinfo
	.set	getnameinfo,__GI_getnameinfo
	.local	lock.12746
	.comm	lock.12746,4,4
	.local	not_first.12745
	.comm	not_first.12745,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	domain, @object
	.size	domain, 8
domain:
	.zero	8
	.hidden	__getservbyport_r
	.hidden	__gethostbyaddr_r
	.hidden	__gethostname
	.hidden	__lll_lock_wait_private
	.hidden	__gethostbyname_r
