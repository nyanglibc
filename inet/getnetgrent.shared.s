	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.type	allocate, @function
allocate:
.LFB70:
	subq	$8, %rsp
	movl	$1024, %edi
	call	malloc@PLT
	movq	%rax, buffer(%rip)
	addq	$8, %rsp
	ret
.LFE70:
	.size	allocate, .-allocate
	.p2align 4,,15
	.globl	getnetgrent
	.type	getnetgrent, @function
getnetgrent:
.LFB71:
	pushq	%r12
	movq	%rdx, %r12
	movl	__libc_pthread_functions_init(%rip), %edx
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L5
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	allocate(%rip), %rsi
	leaq	once.11495(%rip), %rdi
#APP
# 40 "getnetgrent.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	movq	buffer(%rip), %rcx
.L6:
	testq	%rcx, %rcx
	je	.L11
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	$1024, %r8d
	jmp	__getnetgrent_r
	.p2align 4,,10
	.p2align 3
.L5:
	movl	once.11495(%rip), %eax
	testl	%eax, %eax
	je	.L7
	movq	buffer(%rip), %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1024, %edi
	call	malloc@PLT
	movl	$2, once.11495(%rip)
	movq	%rax, %rcx
	movq	%rax, buffer(%rip)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.LFE71:
	.size	getnetgrent, .-getnetgrent
	.local	once.11495
	.comm	once.11495,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.hidden	__getnetgrent_r
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
