	.text
	.p2align 4,,15
	.globl	setsourcefilter
	.type	setsourcefilter, @function
setsourcefilter:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbx
	movl	%r9d, %ebx
	movq	%rbx, %r12
	salq	$7, %rbx
	leaq	144(%rbx), %r13
	subq	$40, %rsp
	movl	%edi, -64(%rbp)
	movl	%esi, -56(%rbp)
	movl	%ecx, -52(%rbp)
	movq	%r13, %rdi
	movl	%r8d, -60(%rbp)
	call	__libc_alloca_cutoff
	cmpq	$4096, %r13
	movl	-56(%rbp), %esi
	jbe	.L2
	testl	%eax, %eax
	je	.L10
.L2:
	leaq	30(%r13), %rax
	movl	$1, -56(%rbp)
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
	movq	%r9, %r15
.L4:
	movl	-52(%rbp), %edx
	leaq	8(%r15), %rdi
	movl	%esi, (%r15)
	movq	%r14, %rsi
	call	memcpy@PLT
	movl	-60(%rbp), %ecx
	movq	16(%rbp), %rsi
	leaq	144(%r15), %rdi
	movq	%rbx, %rdx
	movl	%r12d, 140(%r15)
	movl	%ecx, 136(%r15)
	call	memcpy@PLT
	movzwl	(%r14), %edi
	movl	-52(%rbp), %esi
	call	__get_sol
	cmpl	$-1, %eax
	jne	.L5
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	$22, %fs:(%rdx)
.L6:
	movl	-56(%rbp), %edx
	testl	%edx, %edx
	jne	.L1
	movq	%r15, %rdi
	movl	%eax, -52(%rbp)
	call	free@PLT
	movl	-52(%rbp), %eax
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	-64(%rbp), %edi
	movl	%r13d, %r8d
	movq	%r15, %rcx
	movl	$48, %edx
	movl	%eax, %esi
	call	__setsockopt
	jmp	.L6
.L10:
	movq	%r13, %rdi
	movl	%esi, -68(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L7
	movl	$0, -56(%rbp)
	movl	-68(%rbp), %esi
	jmp	.L4
.L7:
	orl	$-1, %eax
	jmp	.L1
	.size	setsourcefilter, .-setsourcefilter
	.hidden	__setsockopt
	.hidden	__get_sol
	.hidden	__libc_alloca_cutoff
