	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Unexpected error %d on netlink descriptor %d.\n"
	.align 8
.LC1:
	.string	"Unexpected error %d on netlink descriptor %d (address family %d).\n"
	.align 8
.LC2:
	.string	"Unexpected netlink response of size %zd on descriptor %d\n"
	.align 8
.LC3:
	.string	"Unexpected netlink response of size %zd on descriptor %d (address family %d)\n"
	.text
	.p2align 4,,15
	.globl	__netlink_assert_response
	.hidden	__netlink_assert_response
	.type	__netlink_assert_response, @function
__netlink_assert_response:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movl	%edi, %ebp
	pushq	%rbx
	subq	$224, %rsp
	testq	%rsi, %rsi
	js	.L18
	cmpq	$15, %rsi
	movq	%rsi, %rbx
	jle	.L19
	addq	$224, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	__libc_errno@gottpoff(%rip), %r12
	leaq	16(%rsp), %r13
	leaq	12(%rsp), %rdx
	movl	$128, 12(%rsp)
	movq	%r13, %rsi
	movl	%fs:(%r12), %ebx
	call	__getsockname
	testl	%eax, %eax
	js	.L20
	movzwl	16(%rsp), %r14d
	cmpl	$16, %r14d
	jne	.L5
	movl	%ebx, %eax
	andl	$-5, %eax
	cmpl	$107, %eax
	je	.L5
	cmpl	$9, %ebx
	je	.L5
	cmpl	$88, %ebx
	je	.L5
	cmpl	$11, %ebx
	je	.L21
.L9:
	movl	%ebx, %fs:(%r12)
	addq	$224, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$3, %esi
	movl	%ebp, %edi
	call	__fcntl
	testl	%eax, %eax
	js	.L5
	testb	$8, %ah
	je	.L9
.L5:
	leaq	.LC1(%rip), %rdx
	movl	%r14d, %r9d
	movl	%ebp, %r8d
	movl	%ebx, %ecx
	movl	$200, %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf
.L12:
	movq	%r13, %rdi
	call	__libc_fatal
.L19:
	leaq	16(%rsp), %r13
	leaq	12(%rsp), %rdx
	movl	$128, 12(%rsp)
	movq	%r13, %rsi
	call	__getsockname
	testl	%eax, %eax
	js	.L22
	movzwl	16(%rsp), %r9d
	leaq	.LC3(%rip), %rdx
	movl	%ebp, %r8d
	movq	%rbx, %rcx
	movl	$200, %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf
	jmp	.L12
.L20:
	leaq	.LC0(%rip), %rdx
	movl	%ebp, %r8d
	movl	%ebx, %ecx
	movl	$200, %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf
	jmp	.L12
.L22:
	leaq	.LC2(%rip), %rdx
	movl	%ebp, %r8d
	movq	%rbx, %rcx
	movl	$200, %esi
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__snprintf
	jmp	.L12
	.size	__netlink_assert_response, .-__netlink_assert_response
	.hidden	__libc_fatal
	.hidden	__snprintf
	.hidden	__fcntl
	.hidden	__getsockname
