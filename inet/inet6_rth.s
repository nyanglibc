	.text
	.p2align 4,,15
	.globl	inet6_rth_space
	.type	inet6_rth_space, @function
inet6_rth_space:
	testl	%edi, %edi
	jne	.L6
	movl	%esi, %eax
	sall	$4, %eax
	addl	$8, %eax
	cmpl	$128, %esi
	cmovnb	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.size	inet6_rth_space, .-inet6_rth_space
	.p2align 4,,15
	.globl	inet6_rth_init
	.type	inet6_rth_init, @function
inet6_rth_init:
	testl	%edx, %edx
	jne	.L12
	cmpl	$127, %ecx
	ja	.L12
	pushq	%rbx
	movslq	%ecx, %rbx
	sall	$4, %ecx
	leal	8(%rcx), %edx
	cmpl	%esi, %edx
	ja	.L13
	salq	$4, %rbx
	xorl	%esi, %esi
	shrq	$3, %rbx
	call	memset@PLT
	movb	%bl, 1(%rax)
	movb	$0, 2(%rax)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	inet6_rth_init, .-inet6_rth_init
	.p2align 4,,15
	.globl	inet6_rth_add
	.type	inet6_rth_add, @function
inet6_rth_add:
	cmpb	$0, 2(%rdi)
	jne	.L23
	movzbl	1(%rdi), %eax
	movzbl	3(%rdi), %edx
	sall	$3, %eax
	movq	%rdx, %rcx
	cltq
	shrq	$4, %rax
	cmpq	%rdx, %rax
	je	.L23
	addl	$1, %ecx
	salq	$4, %rdx
	xorl	%eax, %eax
	movb	%cl, 3(%rdi)
	movdqu	(%rsi), %xmm0
	movups	%xmm0, 8(%rdi,%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$-1, %eax
	ret
	.size	inet6_rth_add, .-inet6_rth_add
	.p2align 4,,15
	.globl	inet6_rth_reverse
	.type	inet6_rth_reverse, @function
inet6_rth_reverse:
	cmpb	$0, 2(%rdi)
	jne	.L42
	movq	(%rdi), %rax
	movq	%rax, (%rsi)
	movzbl	1(%rdi), %ecx
	sall	$3, %ecx
	movslq	%ecx, %rcx
	shrq	$4, %rcx
	movl	%ecx, %r9d
	sarl	%r9d
	je	.L28
	leal	-1(%rcx), %edx
	leal	-1(%r9), %r8d
	xorl	%eax, %eax
	movslq	%edx, %rdx
	addq	$1, %r8
	salq	$4, %rdx
	salq	$4, %r8
	.p2align 4,,10
	.p2align 3
.L29:
	movdqu	8(%rdi,%rdx), %xmm1
	movdqu	8(%rdi,%rax), %xmm0
	movups	%xmm1, 8(%rsi,%rax)
	addq	$16, %rax
	movups	%xmm0, 8(%rsi,%rdx)
	subq	$16, %rdx
	cmpq	%r8, %rax
	jne	.L29
.L28:
	cmpq	%rsi, %rdi
	je	.L30
	testb	$1, %cl
	je	.L30
	movslq	%r9d, %r9
	salq	$4, %r9
	movdqu	8(%rdi,%r9), %xmm0
	movups	%xmm0, 8(%rsi,%r9)
.L30:
	movb	%cl, 3(%rsi)
	xorl	%eax, %eax
	ret
.L42:
	movl	$-1, %eax
	ret
	.size	inet6_rth_reverse, .-inet6_rth_reverse
	.p2align 4,,15
	.globl	inet6_rth_segments
	.type	inet6_rth_segments, @function
inet6_rth_segments:
	cmpb	$0, 2(%rdi)
	jne	.L47
	movzbl	1(%rdi), %eax
	sall	$3, %eax
	shrq	$4, %rax
	andl	$127, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$-1, %eax
	ret
	.size	inet6_rth_segments, .-inet6_rth_segments
	.p2align 4,,15
	.globl	inet6_rth_getaddr
	.type	inet6_rth_getaddr, @function
inet6_rth_getaddr:
	cmpb	$0, 2(%rdi)
	jne	.L52
	movzbl	1(%rdi), %eax
	movslq	%esi, %rsi
	sall	$3, %eax
	cltq
	shrq	$4, %rax
	cmpq	%rax, %rsi
	jnb	.L52
	salq	$4, %rsi
	leaq	8(%rdi,%rsi), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%eax, %eax
	ret
	.size	inet6_rth_getaddr, .-inet6_rth_getaddr
