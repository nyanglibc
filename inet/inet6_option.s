	.text
#APP
	.section .gnu.warning.inet6_option_space
	.previous
	.section .gnu.warning.inet6_option_init
	.previous
	.section .gnu.warning.inet6_option_append
	.previous
	.section .gnu.warning.inet6_option_alloc
	.previous
	.section .gnu.warning.inet6_option_next
	.previous
	.section .gnu.warning.inet6_option_find
	.previous
#NO_APP
	.p2align 4,,15
	.type	add_pad, @function
add_pad:
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	(%rdi), %rax
	addq	%rax, %rdi
	cmpl	$1, %ebx
	je	.L9
	testl	%ebx, %ebx
	jne	.L10
	addq	%rax, %rbx
	movq	%rbx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leal	-2(%rbx), %eax
	leal	-2(%rbx), %edx
	movb	$1, (%rdi)
	xorl	%esi, %esi
	addq	$2, %rdi
	movb	%al, -1(%rdi)
	movslq	%edx, %rdx
	call	memset@PLT
	movq	0(%rbp), %rax
	addq	%rax, %rbx
	movq	%rbx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movb	$0, (%rdi)
	movq	0(%rbp), %rax
	addq	%rax, %rbx
	movq	%rbx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	add_pad, .-add_pad
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"inet6_option.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"((cmsg->cmsg_len - CMSG_LEN (0)) % 8) == 0"
	.text
	.p2align 4,,15
	.type	option_alloc, @function
option_alloc:
	leal	-4(%rdx), %eax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	andl	$-5, %eax
	je	.L12
	leal	-1(%rdx), %eax
	cmpl	$1, %eax
	ja	.L14
.L12:
	cmpl	$7, %ecx
	ja	.L14
	movq	(%rdi), %r8
	movl	%r8d, %eax
	subl	$16, %eax
	je	.L24
.L15:
	movl	%esi, %ebx
	leal	-1(%rdx), %esi
	movq	%rdi, %rbp
	andl	%esi, %eax
	subl	%eax, %edx
	andl	%esi, %edx
	leal	(%rdx,%rcx), %esi
	call	add_pad
	movq	0(%rbp), %rdx
	movslq	%ebx, %rsi
	movq	%rbp, %rdi
	addq	%rdx, %rsi
	leaq	0(%rbp,%rdx), %r12
	movq	%rsi, 0(%rbp)
	negl	%esi
	andl	$7, %esi
	call	add_pad
	movq	0(%rbp), %rdx
	testb	$7, %dl
	jne	.L25
	subq	$16, %rdx
	shrq	$3, %rdx
	subl	$1, %edx
	cmpl	$255, %edx
	jg	.L14
	movb	%dl, 17(%rbp)
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r12d, %r12d
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$2, %r8
	movl	$2, %eax
	movq	%r8, (%rdi)
	jmp	.L15
.L25:
	leaq	__PRETTY_FUNCTION__.3903(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$204, %edx
	call	__assert_fail
	.size	option_alloc, .-option_alloc
	.p2align 4,,15
	.type	get_opt_end, @function
get_opt_end:
	cmpq	%rdx, %rsi
	jnb	.L31
	cmpb	$0, (%rsi)
	je	.L32
	leaq	2(%rsi), %rax
	cmpq	%rax, %rdx
	jb	.L31
	movzbl	1(%rsi), %eax
	leaq	2(%rsi,%rax), %rax
	cmpq	%rax, %rdx
	jb	.L31
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$1, %rsi
	xorl	%eax, %eax
	movq	%rsi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$-1, %eax
	ret
	.size	get_opt_end, .-get_opt_end
	.p2align 4,,15
	.globl	inet6_option_space
	.type	inet6_option_space, @function
inet6_option_space:
	leal	9(%rdi), %eax
	andl	$-8, %eax
	addl	$16, %eax
	ret
	.size	inet6_option_space, .-inet6_option_space
	.p2align 4,,15
	.globl	inet6_option_init
	.type	inet6_option_init, @function
inet6_option_init:
	cmpl	$54, %edx
	je	.L38
	cmpl	$59, %edx
	jne	.L37
.L38:
	movq	$16, (%rdi)
	movl	$41, 8(%rdi)
	xorl	%eax, %eax
	movl	%edx, 12(%rdi)
	movq	%rdi, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$-1, %eax
	ret
	.size	inet6_option_init, .-inet6_option_init
	.p2align 4,,15
	.globl	inet6_option_append
	.type	inet6_option_append, @function
inet6_option_append:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$1, %ebx
	subq	$8, %rsp
	cmpb	$0, (%rsi)
	je	.L43
	movzbl	1(%rsi), %ebx
	addl	$2, %ebx
.L43:
	movl	%ebx, %esi
	call	option_alloc
	testq	%rax, %rax
	je	.L50
	cmpl	$8, %ebx
	jnb	.L45
	testb	$4, %bl
	jne	.L59
	testl	%ebx, %ebx
	je	.L46
	movzbl	0(%rbp), %edx
	testb	$2, %bl
	movb	%dl, (%rax)
	jne	.L60
.L46:
	xorl	%eax, %eax
.L42:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	0(%rbp), %rdx
	leaq	8(%rax), %rdi
	movq	%rbp, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movl	%ebx, %edx
	movq	-8(%rbp,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	leal	(%rbx,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	movl	%ecx, %ecx
	rep movsq
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L60:
	movzwl	-2(%rbp,%rbx), %edx
	movw	%dx, -2(%rax,%rbx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L59:
	movl	0(%rbp), %edx
	movl	%edx, (%rax)
	movl	-4(%rbp,%rbx), %edx
	movl	%edx, -4(%rax,%rbx)
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$-1, %eax
	jmp	.L42
	.size	inet6_option_append, .-inet6_option_append
	.p2align 4,,15
	.globl	inet6_option_alloc
	.type	inet6_option_alloc, @function
inet6_option_alloc:
	jmp	option_alloc
	.size	inet6_option_alloc, .-inet6_option_alloc
	.p2align 4,,15
	.globl	inet6_option_next
	.type	inet6_option_next, @function
inet6_option_next:
	cmpl	$41, 8(%rdi)
	jne	.L74
	movl	12(%rdi), %eax
	cmpl	$54, %eax
	je	.L69
	cmpl	$59, %eax
	jne	.L74
.L69:
	movq	(%rdi), %rax
	cmpq	$17, %rax
	jbe	.L74
	movzbl	17(%rdi), %edx
	leaq	8(,%rdx,8), %rdx
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rax
	jb	.L74
	subq	$16, %rsp
	movq	%rsi, %rcx
	movq	(%rsi), %rsi
	leaq	16(%rdi,%rdx), %r8
	addq	$18, %rdi
	testq	%rsi, %rsi
	je	.L77
	cmpq	%rdi, %rsi
	jb	.L65
	leaq	8(%rsp), %r9
	movq	%r8, %rdx
	movq	%r9, %rdi
	call	get_opt_end
	testl	%eax, %eax
	jne	.L65
	movq	8(%rsp), %rdi
.L68:
	movq	%rdi, (%rcx)
	movq	%rdi, %rsi
	movq	%r8, %rdx
	movq	%r9, %rdi
	call	get_opt_end
	addq	$16, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %r9
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L65:
	movl	$-1, %eax
	addq	$16, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$-1, %eax
	ret
	.size	inet6_option_next, .-inet6_option_next
	.p2align 4,,15
	.globl	inet6_option_find
	.type	inet6_option_find, @function
inet6_option_find:
	cmpl	$41, 8(%rdi)
	jne	.L92
	movl	12(%rdi), %eax
	cmpl	$54, %eax
	je	.L87
	cmpl	$59, %eax
	jne	.L92
.L87:
	movq	(%rdi), %rax
	cmpq	$17, %rax
	jbe	.L92
	movzbl	17(%rdi), %ecx
	leaq	8(,%rcx,8), %rcx
	leaq	16(%rcx), %r8
	cmpq	%r8, %rax
	jb	.L92
	subq	$16, %rsp
	movq	%rsi, %r11
	movq	(%rsi), %rsi
	leaq	16(%rdi,%rcx), %r9
	movl	%edx, %r8d
	leaq	18(%rdi), %rcx
	testq	%rsi, %rsi
	je	.L96
	cmpq	%rsi, %rcx
	ja	.L81
	leaq	8(%rsp), %r10
	movq	%r9, %rdx
	movq	%r10, %rdi
	call	get_opt_end
	testl	%eax, %eax
	je	.L95
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L97:
	movzbl	(%rcx), %edi
	cmpl	%r8d, %edi
	je	.L85
.L95:
	movq	8(%rsp), %rcx
.L86:
	movq	%r9, %rdx
	movq	%rcx, %rsi
	movq	%r10, %rdi
	call	get_opt_end
	testl	%eax, %eax
	je	.L97
.L81:
	movl	$-1, %eax
	addq	$16, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rcx, (%r11)
	addq	$16, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	movq	%rcx, 8(%rsp)
	leaq	8(%rsp), %r10
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$-1, %eax
	ret
	.size	inet6_option_find, .-inet6_option_find
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.3903, @object
	.size	__PRETTY_FUNCTION__.3903, 13
__PRETTY_FUNCTION__.3903:
	.string	"option_alloc"
	.section	.gnu.warning.inet6_option_find
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_inet6_option_find, @object
	.size	__evoke_link_warning_inet6_option_find, 59
__evoke_link_warning_inet6_option_find:
	.string	"inet6_option_find is obsolete, use the RFC 3542 interfaces"
	.section	.gnu.warning.inet6_option_next
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_inet6_option_next, @object
	.size	__evoke_link_warning_inet6_option_next, 59
__evoke_link_warning_inet6_option_next:
	.string	"inet6_option_next is obsolete, use the RFC 3542 interfaces"
	.section	.gnu.warning.inet6_option_alloc
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_inet6_option_alloc, @object
	.size	__evoke_link_warning_inet6_option_alloc, 60
__evoke_link_warning_inet6_option_alloc:
	.string	"inet6_option_alloc is obsolete, use the RFC 3542 interfaces"
	.section	.gnu.warning.inet6_option_append
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_inet6_option_append, @object
	.size	__evoke_link_warning_inet6_option_append, 61
__evoke_link_warning_inet6_option_append:
	.string	"inet6_option_append is obsolete, use the RFC 3542 interfaces"
	.section	.gnu.warning.inet6_option_init
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_inet6_option_init, @object
	.size	__evoke_link_warning_inet6_option_init, 59
__evoke_link_warning_inet6_option_init:
	.string	"inet6_option_init is obsolete, use the RFC 3542 interfaces"
	.section	.gnu.warning.inet6_option_space
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_inet6_option_space, @object
	.size	__evoke_link_warning_inet6_option_space, 60
__evoke_link_warning_inet6_option_space:
	.string	"inet6_option_space is obsolete, use the RFC 3542 interfaces"
	.hidden	__assert_fail
