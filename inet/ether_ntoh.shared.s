	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getntohost_r"
#NO_APP
	.text
	.p2align 4,,15
	.globl	ether_ntohost
	.type	ether_ntohost, @function
ether_ntohost:
	pushq	%r15
	pushq	%r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rsi
	subq	$1096, %rsp
	leaq	40(%rsp), %r12
	leaq	32(%rsp), %rbp
	movq	%rdi, 24(%rsp)
	movq	%r12, %rcx
	movq	%rbp, %rdi
	call	__GI___nss_ethers_lookup2
	testl	%eax, %eax
	movl	%eax, 20(%rsp)
	jne	.L4
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	leaq	64(%rsp), %r15
	leaq	48(%rsp), %r14
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	8(%rsp), %r8
	movl	$1024, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*40(%rsp)
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%r12, %rcx
	movq	%rbp, %rdi
	movl	%eax, %ebx
	call	__GI___nss_next2
	testl	%eax, %eax
	je	.L2
	cmpl	$1, %ebx
	je	.L10
.L4:
	movl	$-1, 20(%rsp)
.L1:
	movl	20(%rsp), %eax
	addq	$1096, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	48(%rsp), %rsi
	movq	24(%rsp), %rdi
	call	__GI_strcpy
	jmp	.L1
	.size	ether_ntohost, .-ether_ntohost
