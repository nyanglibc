	.text
	.p2align 4,,15
	.globl	__idna_name_classify
	.hidden	__idna_name_classify
	.type	__idna_name_classify, @function
__idna_name_classify:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movl	$1, %r15d
	pushq	%rbp
	pushq	%rbx
	xorl	%ebx, %ebx
	subq	$40, %rsp
	movq	$0, 24(%rsp)
	leaq	24(%rsp), %r12
	leaq	20(%rsp), %rbp
	call	strlen
	leaq	1(%r14,%rax), %r13
	movb	$0, 12(%rsp)
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r14, %rsi
	subq	%r14, %rdx
	movq	%rbp, %rdi
	call	mbrtowc
	testq	%rax, %rax
	je	.L2
	cmpq	$-2, %rax
	je	.L9
	cmpq	$-1, %rax
	je	.L18
	addq	%rax, %r14
	movl	20(%rsp), %eax
	cmpl	$92, %eax
	je	.L12
	addl	$-128, %eax
	cmovge	%r15d, %ebx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	movb	$1, 12(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	testb	%bl, %bl
	je	.L1
	movl	12(%rsp), %eax
	andl	$1, %eax
	addl	$1, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$40, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edx
	movl	$3, %eax
	cmpl	$84, %edx
	je	.L1
	xorl	%eax, %eax
	cmpl	$12, %edx
	setne	%al
	addq	$40, %rsp
	popq	%rbx
	addl	$4, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__idna_name_classify, .-__idna_name_classify
	.hidden	mbrtowc
	.hidden	strlen
