	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_ether_aton_r
	.hidden	__GI_ether_aton_r
	.type	__GI_ether_aton_r, @function
__GI_ether_aton_r:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	pushq	%rbx
	movq	%rsi, %rax
	xorl	%r8d, %r8d
	movq	%fs:(%rdx), %r10
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rsi
.L13:
	movsbq	(%rdi), %rdx
	leaq	1(%rdi), %r9
	movl	(%r10,%rdx,4), %edx
	leal	-48(%rdx), %ecx
	cmpb	$9, %cl
	jbe	.L17
	leal	-97(%rdx), %ecx
	cmpb	$5, %cl
	ja	.L16
.L17:
	movsbl	%dl, %edx
	leal	-48(%rdx), %ecx
	subl	$87, %edx
	cmpl	$9, %ecx
	cmovbe	%ecx, %edx
	movsbq	1(%rdi), %rcx
	cmpq	$4, %r8
	setbe	%r11b
	movl	(%r10,%rcx,4), %ecx
	cmpb	$58, %cl
	movsbq	%cl, %rbx
	je	.L18
	testb	%r11b, %r11b
	je	.L18
.L6:
	leal	-48(%rcx), %ebx
	leaq	2(%rdi), %r9
	cmpb	$9, %bl
	jbe	.L19
	leal	-97(%rcx), %ebx
	cmpb	$5, %bl
	ja	.L16
.L19:
	movsbl	%cl, %ecx
	sall	$4, %edx
	leal	-48(%rcx), %ebx
	subl	$87, %ecx
	cmpl	$9, %ebx
	cmovbe	%ebx, %ecx
	addl	%ecx, %edx
	cmpb	$58, 2(%rdi)
	je	.L8
	testb	%r11b, %r11b
	jne	.L16
.L8:
	movb	%dl, (%rax,%r8)
	addq	$1, %r8
	leaq	1(%r9), %rdi
	cmpq	$6, %r8
	jne	.L13
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpq	$5, %r8
	jne	.L8
	testb	%cl, %cl
	je	.L8
	testb	$32, 1(%rsi,%rbx,2)
	jne	.L8
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__GI_ether_aton_r, .-__GI_ether_aton_r
	.globl	ether_aton_r
	.set	ether_aton_r,__GI_ether_aton_r
