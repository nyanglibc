	.text
	.p2align 4,,15
	.globl	inet_network
	.type	inet_network, @function
inet_network:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%fs:(%rax), %r11
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	leaq	-16(%rsp), %rbx
	movq	%rbx, %r10
	movq	%fs:(%rax), %rbp
.L2:
	movzbl	(%rdi), %edx
	xorl	%eax, %eax
	movl	$10, %r9d
	cmpb	$48, %dl
	jne	.L3
	movzbl	1(%rdi), %edx
	movl	$1, %eax
	addq	$1, %rdi
	movl	$8, %r9d
.L3:
	movl	%edx, %ecx
	andl	$-33, %ecx
	cmpb	$88, %cl
	jne	.L4
	movzbl	1(%rdi), %edx
	xorl	%eax, %eax
	addq	$1, %rdi
	movl	$16, %r9d
.L4:
	xorl	%ecx, %ecx
	testb	%dl, %dl
	movq	%rdi, %r12
	je	.L32
.L11:
	movsbl	%dl, %esi
	leal	-48(%rsi), %r8d
	cmpl	$9, %r8d
	ja	.L5
	cmpl	$8, %r9d
	jne	.L6
	subl	$56, %edx
	cmpb	$1, %dl
	jbe	.L22
.L6:
	imull	%r9d, %ecx
	leal	-48(%rsi,%rcx), %ecx
.L8:
	movzbl	1(%rdi), %edx
	addq	$1, %rdi
	movl	$1, %eax
	movq	%rdi, %r12
	testb	%dl, %dl
	jne	.L11
.L32:
	cmpq	%rsp, %r10
	setnb	%sil
	xorl	$1, %eax
	orb	%al, %sil
	jne	.L22
	cmpl	$255, %ecx
	jbe	.L17
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$-1, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$16, %r9d
	jne	.L9
	movsbq	%dl, %rsi
	testb	$16, 1(%r11,%rsi,2)
	jne	.L33
.L9:
	xorl	$1, %eax
	cmpl	$255, %ecx
	seta	%sil
	orb	%sil, %al
	jne	.L22
	cmpq	%rsp, %r10
	jnb	.L22
	cmpb	$46, %dl
	jne	.L17
	addq	$4, %r10
	movl	%ecx, -4(%r10)
	addq	$1, %rdi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L33:
	movl	0(%rbp,%rsi,4), %eax
	sall	$4, %ecx
	leal	-87(%rcx,%rax), %ecx
	jmp	.L8
.L17:
	movsbq	%dl, %rax
	testb	$32, 1(%r11,%rax,2)
	je	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$1, %r12
	movsbq	(%r12), %rax
	testb	$32, 1(%r11,%rax,2)
	movq	%rax, %rdx
	jne	.L14
.L15:
	testb	%dl, %dl
	movl	$-1, %eax
	jne	.L1
	leaq	4(%r10), %rdx
	movl	%ecx, (%r10)
	subq	%rbx, %rdx
	sarq	$2, %rdx
	testl	%edx, %edx
	movl	%edx, %eax
	je	.L1
	leal	-1(%rdx), %eax
	leaq	4(%rbx,%rax,4), %rcx
	xorl	%eax, %eax
.L16:
	movzbl	(%rbx), %edx
	sall	$8, %eax
	addq	$4, %rbx
	orl	%edx, %eax
	cmpq	%rcx, %rbx
	jne	.L16
	jmp	.L1
	.size	inet_network, .-inet_network
