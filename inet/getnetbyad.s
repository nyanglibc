	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section .gnu.warning.getnetbyaddr
	.previous
#NO_APP
	.p2align 4,,15
	.globl	getnetbyaddr
	.type	getnetbyaddr, @function
getnetbyaddr:
.LFB70:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$24, %rsp
	movl	$0, 4(%rsp)
#APP
# 116 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	buffer(%rip), %rcx
	movq	buffer_size.11545(%rip), %rbx
	testq	%rcx, %rcx
	je	.L27
.L5:
	leaq	8(%rsp), %r15
	leaq	4(%rsp), %r14
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rax, buffer(%rip)
.L15:
	subq	$8, %rsp
	leaq	resbuf.11546(%rip), %rdx
	movq	%r15, %r9
	pushq	%r14
	movq	%rbx, %r8
	movl	%r12d, %esi
	movl	%ebp, %edi
	call	__getnetbyaddr_r
	cmpl	$34, %eax
	movq	buffer(%rip), %r13
	popq	%rdx
	popq	%rcx
	jne	.L9
	cmpl	$-1, 4(%rsp)
	jne	.L9
	movq	buffer_size.11545(%rip), %rax
	movq	%r13, %rdi
	leaq	(%rax,%rax), %rbx
	movq	%rbx, %rsi
	movq	%rbx, buffer_size.11545(%rip)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L7
	movq	%r13, %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, buffer(%rip)
	movl	$12, %fs:(%rax)
.L11:
	movq	$0, 8(%rsp)
.L16:
#APP
# 163 "../nss/getXXbyYY.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L12
	subl	$1, lock(%rip)
.L13:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	je	.L14
	movq	__libc_h_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
.L14:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	testq	%r13, %r13
	jne	.L16
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$1024, %edi
	movq	$1024, buffer_size.11545(%rip)
	movl	$1024, %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, buffer(%rip)
	jne	.L5
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 163 "../nss/getXXbyYY.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L3
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
.LFE70:
	.size	getnetbyaddr, .-getnetbyaddr
	.local	resbuf.11546
	.comm	resbuf.11546,24,16
	.local	buffer_size.11545
	.comm	buffer_size.11545,8,8
	.section	.gnu.warning.getnetbyaddr
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getnetbyaddr, @object
	.size	__evoke_link_warning_getnetbyaddr, 136
__evoke_link_warning_getnetbyaddr:
	.string	"Using 'getnetbyaddr' in statically linked applications requires at runtime the shared libraries from the glibc version used for linking"
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__getnetbyaddr_r
