	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"lstat failed"
.LC1:
	.string	"not regular file"
.LC2:
	.string	"rce"
.LC3:
	.string	"cannot open"
.LC4:
	.string	"fstat failed"
.LC5:
	.string	"bad owner"
.LC6:
	.string	"writeable by other than owner"
.LC7:
	.string	"hard linked somewhere"
#NO_APP
	.text
	.p2align 4,,15
	.type	iruserfopen, @function
iruserfopen:
.LFB88:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$144, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__GI___lstat64
	testl	%eax, %eax
	jne	.L31
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	je	.L5
	leaq	.LC1(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	testq	%rax, %rax
	je	.L3
.L29:
	movq	__rcmd_errstr@GOTPCREL(%rip), %rdx
	xorl	%ebx, %ebx
	movq	%rax, (%rdx)
.L1:
	addq	$144, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L32
	movq	%rax, %rdi
	call	__GI_fileno
	movq	%rbp, %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L33
	movl	28(%rsp), %eax
	testl	%eax, %eax
	je	.L9
	cmpl	%r12d, %eax
	jne	.L34
.L9:
	testb	$18, 24(%rsp)
	jne	.L35
	cmpq	$1, 16(%rsp)
	ja	.L36
.L11:
	orl	$32768, (%rbx)
	addq	$144, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	testq	%rax, %rax
	jne	.L29
.L3:
	movl	0, %eax
	ud2
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	testq	%rax, %rax
	je	.L3
	movq	__rcmd_errstr@GOTPCREL(%rip), %rdx
	movq	%rax, (%rdx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	.LC5(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
.L8:
	testq	%rax, %rax
	je	.L11
	movq	__rcmd_errstr@GOTPCREL(%rip), %rdx
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	movq	%rax, (%rdx)
	call	_IO_new_fclose@PLT
	addq	$144, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	.LC7(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC6(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
.LFE88:
	.size	iruserfopen, .-iruserfopen
	.section	.rodata.str1.1
.LC8:
	.string	"+@"
.LC9:
	.string	"-@"
.LC10:
	.string	"+"
	.text
	.p2align 4,,15
	.type	__validuser2_sa, @function
__validuser2_sa:
.LFB97:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	leaq	96(%rsp), %rax
	leaq	80(%rsp), %rbp
	leaq	72(%rsp), %rbx
	movq	%rsi, 16(%rsp)
	movq	%rdx, 40(%rsp)
	movq	%rcx, (%rsp)
	movq	%r8, 8(%rsp)
	movq	%r9, 32(%rsp)
	movq	$0, 72(%rsp)
	movq	$0, 80(%rsp)
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__getline
	testq	%rax, %rax
	jle	.L126
	movq	80(%rsp), %rax
	movq	72(%rsp), %rdx
	movb	$0, -1(%rdx,%rax)
	movq	72(%rsp), %rax
	movzbl	(%rax), %esi
	testb	%sil, %sil
	je	.L105
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %r10
	movsbq	%sil, %r8
	movq	%fs:(%r10), %r9
	movzwl	(%r9,%r8,2), %r11d
	andw	$8192, %r11w
	je	.L73
	movq	%rax, %rdx
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$1, %rdx
	movzbl	(%rdx), %ecx
	testb	%cl, %cl
	je	.L105
	movsbq	%cl, %rdi
	testb	$32, 1(%r9,%rdi,2)
	jne	.L41
.L40:
	cmpb	$35, %cl
	je	.L105
	testw	%r11w, %r11w
	jne	.L42
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rcx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L129:
	movq	%fs:(%r10), %rdx
	movsbq	%sil, %r8
	testb	$32, 1(%rdx,%r8,2)
	jne	.L42
.L44:
	movq	%fs:(%rcx), %rdx
	addq	$1, %rax
	movl	(%rdx,%r8,4), %edx
	movb	%dl, -1(%rax)
	movzbl	(%rax), %esi
	testb	%sil, %sil
	jne	.L129
	movq	%rax, %rdx
.L43:
	movb	$0, (%rdx)
	movq	72(%rsp), %r14
	cmpb	$0, (%r14)
	je	.L77
.L140:
	cmpb	$0, (%rax)
	leaq	.LC8(%rip), %rsi
	movl	$2, %ecx
	cmove	(%rsp), %rax
	movq	%rax, %rdi
	repz cmpsb
	je	.L130
	leaq	.LC9(%rip), %rsi
	movl	$2, %ecx
	movq	%rax, %rdi
	repz cmpsb
	je	.L131
	cmpb	$45, (%rax)
	je	.L132
	leaq	.LC10(%rip), %rsi
	movl	$2, %ecx
	movq	%rax, %rdi
	repz cmpsb
	je	.L79
	movq	8(%rsp), %rdi
	movq	%rax, %rsi
	xorl	%r13d, %r13d
	call	__GI_strcmp
	testl	%eax, %eax
	sete	%r13b
.L53:
	testl	%r13d, %r13d
	jne	.L56
	cmpb	$45, (%r14)
	jne	.L105
.L56:
	leaq	.LC8(%rip), %rsi
	movl	$2, %ecx
	movq	%r14, %rdi
	repz cmpsb
	je	.L133
	leaq	.LC9(%rip), %rsi
	movl	$2, %ecx
	movq	%r14, %rdi
	repz cmpsb
	je	.L134
	cmpb	$45, (%r14)
	je	.L135
	leaq	.LC10(%rip), %rsi
	movl	$2, %ecx
	movq	%r14, %rdi
	movl	$1, %r15d
	repz cmpsb
	seta	%dl
	setb	%al
	cmpb	%al, %dl
	je	.L136
.L61:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	movq	40(%rsp), %rdx
	movl	$46, %ecx
	movl	56(%rsp), %esi
	movq	32(%rsp), %rdi
	call	__GI_getnameinfo
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L63
	movq	24(%rsp), %rdi
	movq	%r14, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L58
.L63:
	movq	16(%rsp), %rax
	pxor	%xmm0, %xmm0
	leaq	144(%rsp), %rdx
	leaq	88(%rsp), %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movzwl	(%rax), %eax
	movaps	%xmm0, 144(%rsp)
	movaps	%xmm0, 160(%rsp)
	movaps	%xmm0, 176(%rsp)
	movl	%eax, 148(%rsp)
	call	__GI_getaddrinfo
	testl	%eax, %eax
	movl	%eax, 56(%rsp)
	je	.L64
.L125:
	xorl	%r15d, %r15d
	.p2align 4,,10
	.p2align 3
.L65:
	testl	%r15d, %r15d
	setg	%al
.L62:
	testl	%r13d, %r13d
	jle	.L70
	testb	%al, %al
	jne	.L137
.L70:
	testl	%r13d, %r13d
	jns	.L105
	testb	%al, %al
	je	.L105
.L126:
	movq	72(%rsp), %r14
	movl	$-1, %ebx
.L50:
	movq	%r14, %rdi
	call	free@PLT
	addq	$200, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	movq	32(%rsp), %rsi
	leaq	2(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__GI_innetgr
	movl	%eax, %r15d
.L58:
	testl	%r15d, %r15d
	jns	.L65
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L42:
	cmpb	$32, %sil
	je	.L82
	cmpb	$9, %sil
	movq	%rax, %rdx
	jne	.L43
.L82:
	leaq	1(%rax), %rdx
	movb	$0, (%rax)
	movsbq	1(%rax), %rax
	testb	%al, %al
	je	.L46
	movq	%fs:(%r10), %rsi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$1, %rdx
	movsbq	(%rdx), %rax
	testb	%al, %al
	je	.L46
.L127:
	testb	$32, 1(%rsi,%rax,2)
	jne	.L138
	movq	%rdx, %rax
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L139:
	testb	$32, 1(%rsi,%rcx,2)
	jne	.L43
.L49:
	addq	$1, %rdx
	movsbq	(%rdx), %rcx
	testb	%cl, %cl
	jne	.L139
	movb	$0, (%rdx)
	movq	72(%rsp), %r14
	cmpb	$0, (%r14)
	jne	.L140
.L77:
	movl	$-1, %ebx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L130:
	movq	8(%rsp), %rdx
	leaq	2(%rax), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	__GI_innetgr
	movq	72(%rsp), %r14
	movl	%eax, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L131:
	movq	8(%rsp), %rdx
	leaq	2(%rax), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	__GI_innetgr
	negl	%eax
	movq	72(%rsp), %r14
	movl	%eax, %r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rdx, %rax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L79:
	movl	$1, %r13d
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L132:
	movq	8(%rsp), %rsi
	leaq	1(%rax), %rdi
	xorl	%r13d, %r13d
	call	__GI_strcmp
	testl	%eax, %eax
	sete	%r13b
	negl	%r13d
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L73:
	movl	%esi, %ecx
	jmp	.L40
.L134:
	movq	32(%rsp), %rsi
	leaq	2(%r14), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__GI_innetgr
	negl	%eax
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jns	.L65
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L135:
	addq	$1, %r14
	movl	$-1, %r15d
	jmp	.L61
.L136:
	movl	$1, %eax
	jmp	.L62
.L64:
	movq	88(%rsp), %r14
	testq	%r14, %r14
	movq	%r14, 48(%rsp)
	je	.L66
	movq	16(%rsp), %rax
	movl	%r15d, 60(%rsp)
	movq	%r14, %r15
	movzwl	(%rax), %r8d
	movl	%r8d, %r14d
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L67:
	movq	40(%r15), %r15
	testq	%r15, %r15
	je	.L68
.L69:
	cmpl	%r14d, 4(%r15)
	jne	.L67
	movl	16(%r15), %edx
	movq	24(%r15), %rdi
	movq	16(%rsp), %rsi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	jne	.L67
	movl	60(%rsp), %r15d
	movl	%r15d, 56(%rsp)
.L68:
	movq	48(%rsp), %rdi
	call	__GI_freeaddrinfo
	movl	56(%rsp), %r15d
	testl	%r15d, %r15d
	jns	.L65
	jmp	.L126
.L137:
	movq	72(%rsp), %r14
	xorl	%ebx, %ebx
	jmp	.L50
.L66:
	xorl	%edi, %edi
	call	__GI_freeaddrinfo
	jmp	.L125
.LFE97:
	.size	__validuser2_sa, .-__validuser2_sa
	.section	.rodata.str1.1
.LC11:
	.string	"/etc/hosts.equiv"
	.text
	.p2align 4,,15
	.type	ruserok2_sa, @function
ruserok2_sa:
.LFB89:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rcx, %r14
	movq	%r8, %r12
	movq	%r9, %r15
	movl	$-1, %ebx
	subq	$104, %rsp
	testl	%edx, %edx
	movq	%rdi, -120(%rbp)
	je	.L159
.L142:
	movl	$70, %edi
	call	__GI___sysconf
	leaq	30(%rax), %rdx
	leaq	-96(%rbp), %rsi
	leaq	-104(%rbp), %r8
	movq	%rax, %rcx
	movq	%r12, %rdi
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	call	__getpwnam_r
	testl	%eax, %eax
	je	.L160
.L149:
	movl	$-1, %ebx
.L141:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L149
	movq	32(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -128(%rbp)
	call	__GI_strlen
	leaq	39(%rax), %rcx
	movq	-128(%rbp), %rsi
	movq	%rax, %rdx
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	movq	%rcx, %rdi
	movq	%rcx, -136(%rbp)
	call	__GI_mempcpy@PLT
	movabsq	$8319401333991026223, %rcx
	movb	$0, 8(%rax)
	movq	%rcx, (%rax)
	call	__geteuid
	movl	%eax, -128(%rbp)
	movq	-104(%rbp), %rax
	movl	16(%rax), %edi
	call	__GI_seteuid
	movq	-104(%rbp), %rax
	movq	-136(%rbp), %rcx
	movl	16(%rax), %esi
	movq	%rcx, %rdi
	call	iruserfopen
	testq	%rax, %rax
	je	.L146
	movq	-120(%rbp), %rsi
	movq	%rax, %rdi
	movq	%r15, %r9
	movq	%r14, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rax, -120(%rbp)
	call	__validuser2_sa
	movq	-120(%rbp), %r10
	movl	%eax, %ebx
	movq	%r10, %rdi
	call	_IO_new_fclose@PLT
.L146:
	movl	-128(%rbp), %edi
	call	__GI_seteuid
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L159:
	leaq	.LC11(%rip), %rdi
	xorl	%esi, %esi
	call	iruserfopen
	testq	%rax, %rax
	je	.L143
	movq	-120(%rbp), %rsi
	movq	%r15, %r9
	movq	%r14, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%rax, -128(%rbp)
	call	__validuser2_sa
	movq	-128(%rbp), %r10
	movl	%eax, %ebx
	movq	%r10, %rdi
	call	_IO_new_fclose@PLT
	testl	%ebx, %ebx
	je	.L141
.L143:
	movq	__check_rhosts_file@GOTPCREL(%rip), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L142
	jmp	.L149
.LFE89:
	.size	ruserok2_sa, .-ruserok2_sa
	.p2align 4,,15
	.globl	__GI_rresvport_af
	.hidden	__GI_rresvport_af
	.type	__GI_rresvport_af, @function
__GI_rresvport_af:
.LFB84:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	cmpw	$2, %si
	je	.L163
	cmpw	$10, %si
	jne	.L177
	movq	%rsp, %rbp
	movl	$28, %r12d
	leaq	2(%rbp), %r14
.L164:
	xorl	%edx, %edx
	movl	%esi, %r13d
	movq	%rdi, %r15
	movzwl	%si, %edi
	movl	$1, %esi
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L174
	movw	%r13w, (%rsp)
	movl	(%r15), %r13d
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	cmpl	$511, %r13d
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rbp)
	movw	%ax, 26(%rbp)
	jg	.L166
	movl	$2, %eax
	movl	$512, %r13d
	movl	$512, (%r15)
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L169:
	movl	(%r15), %edx
	movl	$1023, %ecx
	leal	-1(%rdx), %eax
	cmpl	$512, %edx
	cmove	%ecx, %eax
	cmpl	%r13d, %eax
	movl	%eax, (%r15)
	je	.L171
	rolw	$8, %ax
.L172:
	movw	%ax, (%r14)
	movl	%r12d, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	call	__bind
	testl	%eax, %eax
	jns	.L161
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$98, %fs:(%rax)
	je	.L169
	movl	%ebx, %edi
	movl	$-1, %ebx
	call	__GI___close
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L177:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$97, %fs:(%rax)
.L161:
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%rsp, %rbp
	movl	$16, %r12d
	leaq	2(%rbp), %r14
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L166:
	cmpl	$1023, %r13d
	jle	.L178
	movl	$1023, (%r15)
	movl	$-253, %eax
	movl	$1023, %r13d
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L171:
	movl	%ebx, %edi
	movl	$-1, %ebx
	call	__GI___close
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$11, %fs:(%rax)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L178:
	movl	%r13d, %eax
	rolw	$8, %ax
	jmp	.L172
.L174:
	movl	$-1, %ebx
	jmp	.L161
.LFE84:
	.size	__GI_rresvport_af, .-__GI_rresvport_af
	.globl	rresvport_af
	.set	rresvport_af,__GI_rresvport_af
	.section	.rodata.str1.1
.LC12:
	.string	"%d"
.LC13:
	.string	"%s: Unknown host\n"
.LC14:
	.string	"rcmd: getaddrinfo: %s\n"
.LC15:
	.string	"rcmd: Cannot allocate memory\n"
.LC16:
	.string	"%s"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.string	"rcmd: socket: All ports in use\n"
	.section	.rodata.str1.1
.LC18:
	.string	"rcmd: socket: %m\n"
.LC19:
	.string	"connect to address %s: "
.LC20:
	.string	"Trying %s...\n"
.LC21:
	.string	"%s: %s\n"
.LC22:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"rcmd: write (setting up stderr): %m\n"
	.align 8
.LC24:
	.string	"poll: protocol failure in circuit setup\n"
	.align 8
.LC25:
	.string	"rcmd: poll (setting up stderr): %m\n"
	.section	.rodata.str1.1
.LC26:
	.string	"rcmd: accept: %m\n"
	.section	.rodata.str1.8
	.align 8
.LC27:
	.string	"socket: protocol failure in circuit setup\n"
	.section	.rodata.str1.1
.LC28:
	.string	"rcmd: %s: short read"
.LC29:
	.string	"rcmd: %s: %m\n"
	.text
	.p2align 4,,15
	.globl	__GI_rcmd_af
	.hidden	__GI_rcmd_af
	.type	__GI_rcmd_af, @function
__GI_rcmd_af:
.LFB82:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$744, %rsp
	movl	800(%rsp), %ebp
	testw	$-3, %bp
	je	.L180
	cmpw	$10, %bp
	jne	.L283
.L180:
	movl	%esi, %ebx
	movq	%r9, 40(%rsp)
	movq	%r8, 64(%rsp)
	rolw	$8, %bx
	movq	%rdi, %r14
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rdi, 32(%rsp)
	movzwl	%bp, %ebp
	call	__GI___getpid
	pxor	%xmm0, %xmm0
	movzwl	%bx, %ecx
	leaq	104(%rsp), %rbx
	leaq	.LC12(%rip), %rdx
	movl	$8, %esi
	movl	%eax, %r15d
	xorl	%eax, %eax
	leaq	176(%rsp), %r12
	movq	%rbx, %rdi
	movl	$0, 220(%rsp)
	movl	$2, 176(%rsp)
	movups	%xmm0, 188(%rsp)
	movl	%ebp, 180(%rsp)
	movl	$1, 184(%rsp)
	movups	%xmm0, 204(%rsp)
	call	__GI___snprintf
	leaq	88(%rsp), %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	(%r14), %rdi
	call	__GI_getaddrinfo
	testl	%eax, %eax
	jne	.L284
	movq	88(%rsp), %rbx
	movl	$1, %r9d
	movl	$1, %r10d
	movw	%r9w, 116(%rsp)
	movw	%r10w, 124(%rsp)
	cmpq	$0, 32(%rbx)
	je	.L184
	movq	ahostbuf(%rip), %rdi
	call	free@PLT
	movq	88(%rsp), %rax
	movq	32(%rax), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, ahostbuf(%rip)
	je	.L285
	movq	32(%rsp), %rcx
	movq	88(%rsp), %rbx
	movq	%rax, (%rcx)
.L186:
	leaq	400(%rsp), %rax
	leaq	272(%rsp), %rsi
	xorl	%edi, %edi
	movq	$4194304, 272(%rsp)
	xorl	%r12d, %r12d
	movl	$1, %r13d
	movq	%rax, %rdx
	movq	%rax, 72(%rsp)
	call	__GI___sigprocmask
	leaq	80(%rsp), %rax
	movl	$1023, 80(%rsp)
	movq	%rax, 8(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	528(%rsp), %rax
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L201:
	movzwl	4(%rbx), %esi
	movq	8(%rsp), %rdi
	call	__GI_rresvport_af
	testl	%eax, %eax
	movl	%eax, %ebp
	js	.L286
	movl	%eax, %edi
	movl	%r15d, %edx
	movl	$8, %esi
	xorl	%eax, %eax
	call	__GI___fcntl
	movl	16(%rbx), %edx
	movq	24(%rbx), %rsi
	movl	%ebp, %edi
	call	__GI___connect
	testl	%eax, %eax
	jns	.L191
	movl	%ebp, %edi
	call	__GI___close
	movq	__libc_errno@gottpoff(%rip), %r14
	movl	%fs:(%r14), %ebp
	cmpl	$98, %ebp
	je	.L287
	cmpl	$111, %ebp
	movl	$1, %eax
	cmove	%eax, %r12d
	cmpq	$0, 40(%rbx)
	jne	.L288
	cmpl	$16, %r13d
	jg	.L198
	andl	$1, %r12d
	je	.L198
	movl	%r13d, %edi
	xorl	%r12d, %r12d
	addl	%r13d, %r13d
	call	__sleep
	movq	88(%rsp), %rbx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L284:
	cmpl	$-2, %eax
	jne	.L183
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.L183
	leaq	.LC13(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	movl	$-1, %ebp
	call	__fxprintf
.L179:
	addq	$744, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	movq	$0, 528(%rsp)
	subq	$8, %rsp
	movl	16(%rbx), %esi
	movq	24(%rbx), %rdi
	pushq	$1
	xorl	%r8d, %r8d
	movq	32(%rsp), %rdx
	xorl	%r9d, %r9d
	movl	$46, %ecx
	call	__GI_getnameinfo
	leaq	.LC19(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	32(%rsp), %rdx
	movq	40(%rsp), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	popq	%rdi
	popq	%r8
	js	.L196
	movq	528(%rsp), %rdx
	leaq	.LC16(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	528(%rsp), %rdi
	call	free@PLT
.L196:
	xorl	%edi, %edi
	movl	%ebp, %fs:(%r14)
	call	__GI_perror
	movq	40(%rbx), %rbx
	subq	$8, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$46, %ecx
	movl	16(%rbx), %esi
	movq	24(%rbx), %rdi
	pushq	$1
	movq	32(%rsp), %r14
	movq	%r14, %rdx
	call	__GI_getnameinfo
	leaq	.LC20(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	40(%rsp), %rdi
	movq	%rax, %rsi
	movq	%r14, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	popq	%rcx
	popq	%rsi
	js	.L201
	movq	528(%rsp), %rdx
	leaq	.LC16(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	528(%rsp), %rdi
	call	free@PLT
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L287:
	subl	$1, 80(%rsp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L183:
	movl	%eax, %edi
	movl	$-1, %ebp
	call	__GI_gai_strerror
	leaq	.LC14(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L184:
	movq	32(%rsp), %rax
	movq	$0, (%rax)
	jmp	.L186
	.p2align 4,,10
	.p2align 3
.L286:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$11, %fs:(%rax)
	je	.L289
	leaq	.LC18(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
.L282:
	movq	72(%rsp), %rsi
	movl	$2, %edi
	xorl	%edx, %edx
	call	__GI___sigprocmask
	movq	88(%rsp), %rdi
	call	__GI_freeaddrinfo
.L281:
	movl	$-1, %ebp
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L191:
	subl	$1, 80(%rsp)
	cmpq	$0, 40(%rsp)
	je	.L290
	movzwl	4(%rbx), %esi
	movq	8(%rsp), %rdi
	call	__GI_rresvport_af
	movl	%eax, %r13d
	movl	16(%rbx), %eax
	testl	%r13d, %r13d
	movl	%eax, 84(%rsp)
	js	.L229
	movl	$1, %esi
	movl	%r13d, %edi
	leaq	224(%rsp), %r12
	call	__listen
	movl	80(%rsp), %ecx
	leaq	.LC12(%rip), %rdx
	movl	$8, %esi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__GI___snprintf
	movq	%r12, %rdx
.L204:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L204
	movl	%eax, %ecx
	movq	%r12, %rsi
	movl	%ebp, %edi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%r12, %rdx
	addq	$1, %rdx
	call	__GI___write
	movq	%r12, %rcx
.L206:
	movl	(%rcx), %esi
	addq	$4, %rcx
	leal	-16843009(%rsi), %edx
	notl	%esi
	andl	%esi, %edx
	andl	$-2139062144, %edx
	je	.L206
	movl	%edx, %esi
	shrl	$16, %esi
	testl	$32896, %edx
	cmove	%esi, %edx
	leaq	2(%rcx), %rsi
	movl	%edx, %ebx
	cmove	%rsi, %rcx
	addb	%dl, %bl
	sbbq	$3, %rcx
	subq	%r12, %rcx
	addq	$1, %rcx
	cmpq	%rax, %rcx
	jne	.L291
	movq	__libc_errno@gottpoff(%rip), %r14
	leaq	112(%rsp), %rdi
	movl	$-1, %edx
	movl	$2, %esi
	movl	%ebp, 112(%rsp)
	movl	%r13d, 120(%rsp)
	movl	$0, %fs:(%r14)
	call	__GI___poll
	testl	%eax, %eax
	jle	.L210
	testb	$1, 126(%rsp)
	je	.L210
	leaq	528(%rsp), %r15
	leaq	84(%rsp), %rbx
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L293:
	cmpl	$4, %fs:(%r14)
	jne	.L292
.L211:
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movl	%r13d, %edi
	call	__GI_accept
	cmpl	$-1, %eax
	je	.L293
	movl	%eax, %r15d
	movzwl	528(%rsp), %eax
	cmpw	$2, %ax
	je	.L222
	xorl	%r14d, %r14d
	cmpw	$10, %ax
	je	.L222
.L220:
	movl	%r13d, %edi
	call	__GI___close
	testl	%r15d, %r15d
	js	.L224
	movq	40(%rsp), %rax
	movl	%r15d, (%rax)
	leal	-512(%r14), %eax
	cmpw	$511, %ax
	jbe	.L202
	leaq	.LC27(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	$0, 96(%rsp)
	call	__GI___dcgettext
	leaq	96(%rsp), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L228
.L237:
	movq	96(%rsp), %rdx
	leaq	.LC16(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	96(%rsp), %rdi
	call	free@PLT
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L289:
	leaq	.LC17(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L290:
	leaq	.LC22(%rip), %rsi
	movl	$1, %edx
	movl	%ebp, %edi
	leaq	224(%rsp), %r12
	leaq	84(%rsp), %rbx
	call	__GI___write
	movl	$0, 80(%rsp)
.L202:
	movq	48(%rsp), %rax
	movq	%rax, %rdi
	movq	%rax, 224(%rsp)
	call	__GI_strlen
	addq	$1, %rax
	movq	%rax, 232(%rsp)
	movq	56(%rsp), %rax
	movq	%rax, %rdi
	movq	%rax, 240(%rsp)
	call	__GI_strlen
	addq	$1, %rax
	movq	%rax, 248(%rsp)
	movq	64(%rsp), %rax
	movq	%rax, %rdi
	movq	%rax, 256(%rsp)
	call	__GI_strlen
	addq	$1, %rax
	movq	%rax, 264(%rsp)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L294:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L233
.L231:
	movl	$3, %edx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	__GI___writev
	cmpq	$-1, %rax
	je	.L294
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L296:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L295
.L233:
	movl	$1, %edx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	__GI___read
	cmpq	$-1, %rax
	je	.L296
	cmpq	$1, %rax
	jne	.L297
	cmpb	$0, 84(%rsp)
	jne	.L240
	movq	72(%rsp), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigprocmask
	movq	88(%rsp), %rdi
	call	__GI_freeaddrinfo
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L240:
	movl	$1, %edx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	__GI___read
	cmpq	$1, %rax
	jne	.L228
	movl	$1, %edx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	__GI___write
	cmpb	$10, 84(%rsp)
	jne	.L240
.L228:
	movl	80(%rsp), %eax
	testl	%eax, %eax
	jne	.L298
.L229:
	movl	%ebp, %edi
	call	__GI___close
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L295:
	movq	32(%rsp), %rax
	movq	$0, 96(%rsp)
	movq	(%rax), %rbx
.L236:
	leaq	96(%rsp), %rdi
	leaq	.LC29(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L237
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L198:
	movq	88(%rsp), %rdi
	call	__GI_freeaddrinfo
	movl	%fs:(%r14), %edi
	leaq	528(%rsp), %rsi
	movl	$200, %edx
	call	__GI___strerror_r
	movq	%rax, %rcx
	movq	32(%rsp), %rax
	leaq	.LC21(%rip), %rsi
	xorl	%edi, %edi
	movq	(%rax), %rdx
	xorl	%eax, %eax
	call	__fxprintf
	movq	72(%rsp), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigprocmask
	jmp	.L281
.L283:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$97, %fs:(%rax)
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L292:
	movl	%r13d, %edi
	call	__GI___close
.L224:
	leaq	.LC26(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movl	$0, 80(%rsp)
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L222:
	movzwl	530(%rsp), %eax
	movl	%eax, %r14d
	rolw	$8, %r14w
	jmp	.L220
.L291:
	leaq	.LC23(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	$0, 528(%rsp)
	call	__GI___dcgettext
	leaq	528(%rsp), %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L213
.L216:
	movl	%r13d, %edi
	call	__GI___close
	jmp	.L229
.L298:
	movq	40(%rsp), %rax
	movl	(%rax), %edi
	call	__GI___close
	jmp	.L229
.L210:
	movl	%fs:(%r14), %edx
	movq	$0, 528(%rsp)
	testl	%edx, %edx
	jne	.L212
	leaq	528(%rsp), %rax
	movq	%rax, %rbx
.L217:
	leaq	.LC24(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%rbx, %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L216
.L213:
	movq	528(%rsp), %rdx
	leaq	.LC16(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	528(%rsp), %rdi
	call	free@PLT
	jmp	.L216
.L285:
	leaq	.LC15(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movl	$-1, %ebp
	call	__GI___dcgettext
	leaq	.LC16(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L179
.L212:
	leaq	.LC25(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	528(%rsp), %rcx
	movq	%rax, %rsi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rcx, %rbx
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L213
	cmpl	$0, %fs:(%r14)
	jne	.L216
	jmp	.L217
.L297:
	movq	32(%rsp), %rcx
	testq	%rax, %rax
	movq	$0, 96(%rsp)
	movq	(%rcx), %rbx
	jne	.L236
	leaq	.LC28(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	96(%rsp), %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L237
	jmp	.L228
.LFE82:
	.size	__GI_rcmd_af, .-__GI_rcmd_af
	.globl	rcmd_af
	.set	rcmd_af,__GI_rcmd_af
	.p2align 4,,15
	.globl	rcmd
	.type	rcmd, @function
rcmd:
.LFB83:
	subq	$16, %rsp
	movzwl	%si, %esi
	pushq	$2
	call	__GI_rcmd_af
	addq	$24, %rsp
	ret
.LFE83:
	.size	rcmd, .-rcmd
	.p2align 4,,15
	.globl	rresvport
	.type	rresvport, @function
rresvport:
.LFB85:
	movl	$2, %esi
	jmp	__GI_rresvport_af
.LFE85:
	.size	rresvport, .-rresvport
	.p2align 4,,15
	.globl	__GI_ruserok_af
	.hidden	__GI_ruserok_af
	.type	__GI_ruserok_af, @function
__GI_ruserok_af:
.LFB86:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r13
	pushq	%rbx
	pxor	%xmm0, %xmm0
	movzwl	%r8w, %r8d
	movl	%esi, %r12d
	xorl	%esi, %esi
	subq	$80, %rsp
	movq	%rdi, %rbp
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movaps	%xmm0, 32(%rsp)
	movl	%r8d, 36(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 64(%rsp)
	call	__GI_getaddrinfo
	testl	%eax, %eax
	jne	.L307
	movq	24(%rsp), %rbx
	movl	$-1, %eax
	testq	%rbx, %rbx
	jne	.L306
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L305:
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L312
.L306:
	movl	16(%rbx), %esi
	movq	24(%rbx), %rdi
	movq	%rbp, %r9
	movq	%r14, %r8
	movq	%r13, %rcx
	movl	%r12d, %edx
	call	ruserok2_sa
	testl	%eax, %eax
	jne	.L305
	movq	24(%rsp), %rbx
.L304:
	movq	%rbx, %rdi
	movl	%eax, 12(%rsp)
	call	__GI_freeaddrinfo
	movl	12(%rsp), %eax
.L302:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L312:
	movq	24(%rsp), %rbx
	movl	$-1, %eax
	jmp	.L304
.L307:
	movl	$-1, %eax
	jmp	.L302
.LFE86:
	.size	__GI_ruserok_af, .-__GI_ruserok_af
	.globl	ruserok_af
	.set	ruserok_af,__GI_ruserok_af
	.p2align 4,,15
	.globl	ruserok
	.type	ruserok, @function
ruserok:
.LFB87:
	movl	$2, %r8d
	jmp	__GI_ruserok_af
.LFE87:
	.size	ruserok, .-ruserok
	.section	.rodata.str1.1
.LC30:
	.string	"-"
	.text
	.p2align 4,,15
	.globl	__GI_iruserok_af
	.hidden	__GI_iruserok_af
	.type	__GI_iruserok_af, @function
__GI_iruserok_af:
.LFB91:
	pxor	%xmm0, %xmm0
	subq	$40, %rsp
	cmpw	$2, %r8w
	movl	%esi, %eax
	movq	$0, 16(%rsp)
	movl	$0, 24(%rsp)
	movaps	%xmm0, (%rsp)
	je	.L316
	cmpw	$10, %r8w
	jne	.L322
	movdqu	(%rdi), %xmm0
	movl	$10, %esi
	movw	%si, (%rsp)
	movl	$28, %esi
	movups	%xmm0, 8(%rsp)
.L318:
	leaq	.LC30(%rip), %r9
	movq	%rcx, %r8
	movq	%rsp, %rdi
	movq	%rdx, %rcx
	movl	%eax, %edx
	call	ruserok2_sa
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	xorl	%eax, %eax
	addq	$40, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	movl	(%rdi), %esi
	movl	$2, %r8d
	movw	%r8w, (%rsp)
	movl	%esi, 4(%rsp)
	movl	$16, %esi
	jmp	.L318
.LFE91:
	.size	__GI_iruserok_af, .-__GI_iruserok_af
	.globl	iruserok_af
	.set	iruserok_af,__GI_iruserok_af
	.p2align 4,,15
	.globl	iruserok
	.type	iruserok, @function
iruserok:
.LFB92:
	subq	$24, %rsp
	movl	$2, %r8d
	movl	%edi, 12(%rsp)
	leaq	12(%rsp), %rdi
	call	__GI_iruserok_af
	addq	$24, %rsp
	ret
.LFE92:
	.size	iruserok, .-iruserok
	.p2align 4,,15
	.globl	__ivaliduser
	.type	__ivaliduser, @function
__ivaliduser:
.LFB93:
	subq	$24, %rsp
	movl	$2, %r9d
	movq	%rcx, %r8
	movq	%rsp, %rax
	movw	%r9w, (%rsp)
	xorl	%ecx, %ecx
	leaq	.LC30(%rip), %r9
	movq	$0, 2(%rsp)
	movl	%esi, 4(%rsp)
	movw	%cx, 14(%rax)
	movl	$0, 10(%rax)
	movq	%rdx, %rcx
	movq	%rax, %rsi
	movl	$16, %edx
	call	__validuser2_sa
	addq	$24, %rsp
	ret
.LFE93:
	.size	__ivaliduser, .-__ivaliduser
	.comm	__rcmd_errstr,8,8
	.globl	__check_rhosts_file
	.data
	.align 4
	.type	__check_rhosts_file, @object
	.size	__check_rhosts_file, 4
__check_rhosts_file:
	.long	1
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	ahostbuf, @object
	.size	ahostbuf, 8
ahostbuf:
	.zero	8
	.hidden	__listen
	.hidden	__fxprintf
	.hidden	__sleep
	.hidden	__bind
	.hidden	__geteuid
	.hidden	__getpwnam_r
	.hidden	__getline
