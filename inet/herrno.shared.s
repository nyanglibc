	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.globl	__h_errno
	.section	.tbss,"awT",@nobits
	.align 4
	.type	__h_errno, @object
	.size	__h_errno, 4
__h_errno:
	.zero	4
	.globl	__libc_h_errno
	.hidden	__libc_h_errno
	.set	__libc_h_errno,__h_errno
