	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_bindresvport
	.hidden	__GI_bindresvport
	.type	__GI_bindresvport, @function
__GI_bindresvport:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movl	%edi, %ebp
	pushq	%rbx
	subq	$16, %rsp
	testq	%rsi, %rsi
	je	.L17
	cmpw	$2, (%rsi)
	movq	%rsi, %rbx
	jne	.L18
.L3:
	cmpw	$0, port.8570(%rip)
	je	.L19
.L5:
	movswl	startport.8573(%rip), %eax
	movl	$1024, %r12d
	subl	%eax, %r12d
#APP
# 84 "bindresvport.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L7:
	testl	%r12d, %r12d
	jle	.L14
	movzwl	port.8570(%rip), %eax
	movl	$1023, %r13d
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%r14d, %r14d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L20:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$98, %fs:(%rax)
	jne	.L8
	addl	$1, %r14d
	cmpl	%r14d, %r12d
	jle	.L8
	movzwl	port.8570(%rip), %eax
.L10:
	leal	1(%rax), %edx
	rolw	$8, %ax
	movw	%ax, 2(%rbx)
	movw	%dx, port.8570(%rip)
	movswl	%dx, %edx
	cmpl	%r13d, %edx
	jle	.L9
	movzwl	startport.8573(%rip), %eax
	movw	%ax, port.8570(%rip)
.L9:
	movl	$16, %edx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	__bind
	testl	%eax, %eax
	movl	%eax, %r8d
	js	.L20
.L8:
	cmpl	%r12d, %r14d
	jne	.L11
	cmpw	$512, startport.8573(%rip)
	je	.L11
	movswl	port.8570(%rip), %edx
	movl	$512, %eax
	movl	$88, %r12d
	movw	%ax, startport.8573(%rip)
	movl	$599, %r13d
	movl	%edx, %eax
	imull	$2979, %edx, %edx
	movl	%eax, %ecx
	sarw	$15, %cx
	sarl	$18, %edx
	subl	%ecx, %edx
	leal	(%rdx,%rdx,4), %ecx
	leal	(%rdx,%rcx,2), %edx
	sall	$3, %edx
	subl	%edx, %eax
	addw	$512, %ax
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L11:
#APP
# 106 "bindresvport.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L13
	subl	$1, lock(%rip)
.L1:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L19:
	call	__GI___getpid
	movl	$1296593901, %edx
	movl	%eax, %ecx
	imull	%edx
	movl	%ecx, %eax
	sarl	$31, %eax
	sarl	$7, %edx
	subl	%eax, %edx
	imull	$424, %edx, %eax
	subl	%eax, %ecx
	leal	600(%rcx), %eax
	movw	%ax, port.8570(%rip)
	jmp	.L5
.L17:
	movq	%rsp, %rbx
	xorl	%edx, %edx
	movl	$2, %ecx
	movq	$0, 2(%rsp)
	movw	%cx, (%rsp)
	movl	$0, 10(%rbx)
	movw	%dx, 14(%rbx)
	jmp	.L3
.L14:
	xorl	%r14d, %r14d
	movl	$-1, %r8d
	jmp	.L8
.L6:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L7
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
#APP
# 106 "bindresvport.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 106 "bindresvport.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L18:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$97, %fs:(%rax)
	jmp	.L1
	.size	__GI_bindresvport, .-__GI_bindresvport
	.globl	bindresvport
	.set	bindresvport,__GI_bindresvport
	.data
	.align 2
	.type	startport.8573, @object
	.size	startport.8573, 2
startport.8573:
	.value	600
	.local	port.8570
	.comm	port.8570,2,2
	.local	lock
	.comm	lock,4,4
	.hidden	__lll_lock_wait_private
	.hidden	__bind
