	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_inet_netof
	.hidden	__GI_inet_netof
	.type	__GI_inet_netof, @function
__GI_inet_netof:
	bswap	%edi
	testl	%edi, %edi
	js	.L2
	movl	%edi, %eax
	shrl	$24, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edi, %edx
	movl	%edi, %eax
	shrl	$8, %edi
	andl	$-1073741824, %edx
	shrl	$16, %eax
	cmpl	$-2147483648, %edx
	cmovne	%edi, %eax
	ret
	.size	__GI_inet_netof, .-__GI_inet_netof
	.globl	inet_netof
	.set	inet_netof,__GI_inet_netof
