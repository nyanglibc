	.text
	.p2align 4,,15
	.globl	__ifreq
	.hidden	__ifreq
	.type	__ifreq, @function
__ifreq:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r13
	pushq	%rbx
	movl	%edx, %ebp
	movl	%edx, %ebx
	subq	$16, %rsp
	testl	%edx, %edx
	js	.L18
.L2:
	movq	%rsp, %r12
	xorl	%eax, %eax
	movl	$35090, %esi
	movq	%r12, %rdx
	movl	%ebp, %edi
	movq	$0, 8(%rsp)
	movl	$0, (%rsp)
	call	__ioctl
	testl	%eax, %eax
	js	.L13
	movl	(%rsp), %eax
	testl	%eax, %eax
	movslq	%eax, %rsi
	jne	.L4
.L13:
	movl	$160, %esi
	movl	$160, %eax
.L4:
	movq	8(%rsp), %rdi
	movl	%eax, (%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	je	.L8
	movq	%rax, 8(%rsp)
	movq	%r12, %rdx
	xorl	%eax, %eax
	movl	$35090, %esi
	movl	%ebp, %edi
	call	__ioctl
	testl	%eax, %eax
	js	.L8
	movslq	(%rsp), %rdx
	movabsq	$-3689348814741910323, %r12
	movq	%rdx, %rax
	mulq	%r12
	shrq	$5, %rdx
	cmpl	%ebp, %ebx
	movq	%rdx, %r12
	je	.L10
	movl	%ebp, %edi
	call	__close
.L10:
	movl	%r12d, (%r14)
	movslq	%r12d, %r12
	movq	8(%rsp), %rdi
	leaq	(%r12,%r12,4), %rsi
	salq	$3, %rsi
	call	realloc@PLT
	movq	%rax, 0(%r13)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	8(%rsp), %rdi
	call	free@PLT
	cmpl	%ebp, %ebx
	jne	.L19
	movl	$0, (%r14)
	movq	$0, 0(%r13)
.L20:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	call	__opensock
	testl	%eax, %eax
	movl	%eax, %ebp
	jns	.L2
	movl	$0, (%r14)
	movq	$0, 0(%r13)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%ebp, %edi
	call	__close
	movl	$0, (%r14)
	movq	$0, 0(%r13)
	jmp	.L20
	.size	__ifreq, .-__ifreq
	.hidden	__opensock
	.hidden	__close
	.hidden	__ioctl
