	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gethostton_r"
	.text
	.p2align 4,,15
	.globl	ether_hostton
	.type	ether_hostton, @function
ether_hostton:
	pushq	%r15
	pushq	%r14
	xorl	%edx, %edx
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$1096, %rsp
	leaq	40(%rsp), %r12
	leaq	32(%rsp), %rbp
	movq	%rsi, 24(%rsp)
	leaq	.LC0(%rip), %rsi
	movq	%r12, %rcx
	movq	%rbp, %rdi
	call	__nss_ethers_lookup2
	testl	%eax, %eax
	movl	%eax, 20(%rsp)
	jne	.L4
	movq	__libc_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	leaq	64(%rsp), %r15
	leaq	48(%rsp), %r14
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	8(%rsp), %r8
	movl	$1024, %ecx
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	*40(%rsp)
	leaq	.LC0(%rip), %rsi
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%r12, %rcx
	movq	%rbp, %rdi
	movl	%eax, %ebx
	call	__nss_next2
	testl	%eax, %eax
	je	.L2
	cmpl	$1, %ebx
	je	.L10
.L4:
	movl	$-1, 20(%rsp)
.L1:
	movl	20(%rsp), %eax
	addq	$1096, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	56(%rsp), %eax
	movq	24(%rsp), %rbx
	movl	%eax, (%rbx)
	movzwl	60(%rsp), %eax
	movw	%ax, 4(%rbx)
	jmp	.L1
	.size	ether_hostton, .-ether_hostton
	.hidden	__nss_next2
	.hidden	__nss_ethers_lookup2
