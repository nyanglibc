	.text
	.p2align 4,,15
	.type	functions_deallocate, @function
functions_deallocate:
	movq	(%rsi), %rdi
	pushq	%rbx
	movq	%rsi, %rbx
	call	__libc_dlclose
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.size	functions_deallocate, .-functions_deallocate
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libidn2.so.0"
.LC1:
	.string	"IDN2_0.0.0"
.LC2:
	.string	"idn2_lookup_ul"
.LC3:
	.string	"idn2_to_unicode_lzlz"
	.text
	.p2align 4,,15
	.type	functions_allocate, @function
functions_allocate:
	pushq	%r12
	pushq	%rbp
	movl	$24, %edi
	pushq	%rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L4
	leaq	.LC0(%rip), %rdi
	movl	$-2147483646, %esi
	call	__libc_dlopen_mode
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L13
	leaq	.LC1(%rip), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	__libc_dlvsym
	leaq	.LC1(%rip), %rdx
	leaq	.LC3(%rip), %rsi
	movq	%rax, %rbx
	movq	%r12, %rdi
	call	__libc_dlvsym
	testq	%rbx, %rbx
	je	.L10
	testq	%rax, %rax
	je	.L10
	movq	%rbx, %rdx
	movq	%r12, 0(%rbp)
#APP
# 76 "idna.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
# 77 "idna.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rdx, 8(%rbp)
	movq	%rax, 16(%rbp)
.L4:
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r12, %rdi
	call	__libc_dlclose
.L13:
	movq	%rbp, %rdi
	xorl	%ebp, %ebp
	call	free@PLT
	jmp	.L4
	.size	functions_allocate, .-functions_allocate
	.p2align 4,,15
	.globl	__idna_to_dns_encoding
	.hidden	__idna_to_dns_encoding
	.type	__idna_to_dns_encoding, @function
__idna_to_dns_encoding:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	__idna_name_classify
	cmpl	$5, %eax
	ja	.L15
	leaq	.L17(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L17:
	.long	.L16-.L17
	.long	.L15-.L17
	.long	.L27-.L17
	.long	.L27-.L17
	.long	.L26-.L17
	.long	.L20-.L17
	.text
	.p2align 4,,10
	.p2align 3
.L15:
	movq	functions.11661(%rip), %rax
	testq	%rax, %rax
	jne	.L23
	leaq	functions_deallocate(%rip), %rdx
	leaq	functions_allocate(%rip), %rsi
	leaq	functions.11661(%rip), %rdi
	xorl	%ecx, %ecx
	call	__libc_allocate_once_slow
	testq	%rax, %rax
	je	.L27
.L23:
	movq	8(%rax), %rax
	xorl	%edx, %edx
	movq	$0, 8(%rsp)
#APP
# 141 "idna.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	8(%rsp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L35
	cmpl	$-100, %eax
	je	.L26
.L27:
	movl	$-105, %eax
.L14:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rbx, %rdi
	call	__strdup
	testq	%rax, %rax
	jne	.L21
.L26:
	addq	$24, %rsp
	movl	$-10, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$24, %rsp
	movl	$-11, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rax, 0(%rbp)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movq	8(%rsp), %rdx
	movq	%rdx, 0(%rbp)
	jmp	.L14
	.size	__idna_to_dns_encoding, .-__idna_to_dns_encoding
	.p2align 4,,15
	.globl	__idna_from_dns_encoding
	.hidden	__idna_from_dns_encoding
	.type	__idna_from_dns_encoding, @function
__idna_from_dns_encoding:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	functions.11661(%rip), %rax
	testq	%rax, %rax
	je	.L47
.L38:
	movq	16(%rax), %rax
	xorl	%edx, %edx
	movq	$0, 8(%rsp)
#APP
# 168 "idna.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	8(%rsp), %rsi
	movq	%rbp, %rdi
	call	*%rax
	testl	%eax, %eax
	je	.L48
	cmpl	$-100, %eax
	je	.L42
	movl	$-105, %eax
.L36:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	8(%rsp), %rdx
	movq	%rdx, (%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	functions_deallocate(%rip), %rdx
	leaq	functions_allocate(%rip), %rsi
	leaq	functions.11661(%rip), %rdi
	xorl	%ecx, %ecx
	call	__libc_allocate_once_slow
	testq	%rax, %rax
	jne	.L38
	movq	%rbp, %rdi
	call	__strdup
	testq	%rax, %rax
	je	.L42
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L42:
	addq	$24, %rsp
	movl	$-10, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__idna_from_dns_encoding, .-__idna_from_dns_encoding
	.local	functions.11661
	.comm	functions.11661,8,8
	.hidden	__strdup
	.hidden	__libc_allocate_once_slow
	.hidden	__idna_name_classify
	.hidden	__libc_dlvsym
	.hidden	__libc_dlopen_mode
	.hidden	__libc_dlclose
