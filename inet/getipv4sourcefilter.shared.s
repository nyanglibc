	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getipv4sourcefilter
	.type	getipv4sourcefilter, @function
getipv4sourcefilter:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbx
	movq	%r8, %rbx
	subq	$56, %rsp
	movl	(%r8), %r14d
	movl	%edi, -72(%rbp)
	movl	%edx, -68(%rbp)
	movq	%rcx, -80(%rbp)
	movq	%r9, -88(%rbp)
	leal	16(,%r14,4), %r15d
	movq	%r15, %rdi
	movl	%r15d, -52(%rbp)
	call	__GI___libc_alloca_cutoff
	cmpl	$4096, %r15d
	movl	-68(%rbp), %edx
	jbe	.L2
	testl	%eax, %eax
	je	.L17
.L2:
	leaq	30(%r15), %rcx
	movl	-72(%rbp), %edi
	leaq	-52(%rbp), %r8
	xorl	%esi, %esi
	shrq	$4, %rcx
	salq	$4, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
	movl	%edx, (%r9)
	movl	%r13d, 4(%r9)
	movq	%r9, %rcx
	movl	%r14d, 12(%r9)
	movl	$41, %edx
	movq	%r9, %r15
	call	__getsockopt
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L18
.L1:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$1, %r12d
.L7:
	movl	8(%r15), %eax
	movq	-80(%rbp), %rsi
	movl	12(%r15), %r13d
	movq	-88(%rbp), %rdi
	movl	%eax, (%rsi)
	movl	(%rbx), %edx
	leaq	16(%r15), %rsi
	movq	%rdx, %rcx
	leaq	0(,%rdx,4), %rax
	movl	%r13d, %edx
	salq	$2, %rdx
	cmpl	%r13d, %ecx
	cmovb	%rax, %rdx
	call	__GI_memcpy@PLT
	testl	%r12d, %r12d
	movl	%r13d, (%rbx)
	jne	.L10
.L8:
	movq	%r15, %rdi
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%r12d, %r12d
	jmp	.L1
.L17:
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movl	-68(%rbp), %edx
	je	.L19
	movl	-72(%rbp), %edi
	leaq	-52(%rbp), %r8
	movl	%edx, (%rax)
	xorl	%esi, %esi
	movl	%r13d, 4(%rax)
	movl	%r14d, 12(%rax)
	movq	%rax, %rcx
	movl	$41, %edx
	call	__getsockopt
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L7
	jmp	.L8
.L19:
	orl	$-1, %r12d
	jmp	.L1
	.size	getipv4sourcefilter, .-getipv4sourcefilter
	.hidden	__getsockopt
