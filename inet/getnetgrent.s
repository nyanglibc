	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.type	allocate, @function
allocate:
.LFB70:
	subq	$8, %rsp
	movl	$1024, %edi
	call	malloc@PLT
	movq	%rax, buffer(%rip)
	addq	$8, %rsp
	ret
.LFE70:
	.size	allocate, .-allocate
	.p2align 4,,15
	.globl	getnetgrent
	.type	getnetgrent, @function
getnetgrent:
.LFB71:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	cmpq	$0, __pthread_once@GOTPCREL(%rip)
	je	.L5
	leaq	allocate(%rip), %rsi
	leaq	once.11508(%rip), %rdi
	movq	%rdx, 8(%rsp)
	call	__pthread_once@PLT
	movq	buffer(%rip), %rcx
	movq	8(%rsp), %rdx
.L6:
	testq	%rcx, %rcx
	je	.L11
	addq	$24, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	movl	$1024, %r8d
	jmp	__getnetgrent_r
	.p2align 4,,10
	.p2align 3
.L5:
	movl	once.11508(%rip), %eax
	testl	%eax, %eax
	je	.L7
	movq	buffer(%rip), %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$1024, %edi
	movq	%rdx, 8(%rsp)
	call	malloc@PLT
	movl	$2, once.11508(%rip)
	movq	%rax, %rcx
	movq	%rax, buffer(%rip)
	movq	8(%rsp), %rdx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
.LFE71:
	.size	getnetgrent, .-getnetgrent
	.local	once.11508
	.comm	once.11508,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.weak	__pthread_once
	.hidden	__getnetgrent_r
