	.text
	.p2align 4,,15
	.globl	__assert
	.type	__assert, @function
__assert:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	call	__assert_fail
	.size	__assert, .-__assert
	.hidden	__assert_fail
