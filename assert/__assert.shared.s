	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__assert
	.type	__assert, @function
__assert:
	subq	$8, %rsp
	xorl	%ecx, %ecx
	call	__GI___assert_fail
	.size	__assert, .-__assert
