	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%s%s%s:%u: %s%sUnexpected error: %s.\n%n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___assert_perror_fail
	.hidden	__GI___assert_perror_fail
	.type	__GI___assert_perror_fail, @function
__GI___assert_perror_fail:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$1024, %edx
	movq	%rcx, %r13
	subq	$1032, %rsp
	movq	%rsp, %rsi
	call	__GI___strerror_r
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movq	%rax, %rbx
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__assert_fail_base
	.size	__GI___assert_perror_fail, .-__GI___assert_perror_fail
	.globl	__assert_perror_fail
	.set	__assert_perror_fail,__GI___assert_perror_fail
	.hidden	__assert_fail_base
