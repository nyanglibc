	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	": "
.LC1:
	.string	""
.LC2:
	.string	"%s"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__assert_fail_base
	.hidden	__assert_fail_base
	.type	__assert_fail_base, @function
__assert_fail_base:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %r12
	movq	%r8, %rbx
	subq	$16, %rsp
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L2
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	$1, %edi
#APP
# 53 "assert.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L2:
	testq	%rbx, %rbx
	leaq	.LC0(%rip), %r8
	je	.L17
.L3:
	movq	__progname@GOTPCREL(%rip), %rax
	leaq	4(%rsp), %rdx
	leaq	.LC1(%rip), %rcx
	leaq	8(%rsp), %rdi
	movl	%r14d, %r9d
	movq	(%rax), %rsi
	leaq	.LC0(%rip), %rax
	cmpb	$0, (%rsi)
	pushq	%rdx
	movq	%rsi, %rdx
	pushq	%r13
	pushq	%r8
	movq	%rbp, %rsi
	pushq	%rbx
	movq	%r12, %r8
	cmovne	%rax, %rcx
	xorl	%eax, %eax
	call	__GI___asprintf
	addq	$32, %rsp
	testl	%eax, %eax
	js	.L5
	movq	8(%rsp), %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	stderr@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	call	__GI__IO_fflush
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movl	4(%rsp), %esi
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	movq	24(%rax), %rax
	addl	%eax, %esi
	negl	%eax
	andl	%eax, %esi
	movl	%esi, 4(%rsp)
	movslq	%esi, %rsi
	call	__GI___mmap
	cmpq	$-1, %rax
	movq	%rax, %rbx
	je	.L7
	movl	4(%rsp), %eax
	movq	8(%rsp), %rsi
	leaq	4(%rbx), %rdi
	movl	%eax, (%rbx)
	call	__GI_strcpy
#APP
# 77 "assert.c" 1
	xchgq %rbx, __GI___abort_msg(%rip)
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	je	.L7
	movl	(%rbx), %esi
	movq	%rbx, %rdi
	call	__GI___munmap
.L7:
	movq	8(%rsp), %rdi
	call	free@PLT
.L9:
	call	__GI_abort
.L5:
	leaq	errstr.11898(%rip), %rsi
	movl	$18, %edx
	movl	$2, %edi
	call	__GI___libc_write
	jmp	.L9
.L17:
	leaq	.LC1(%rip), %rbx
	movq	%rbx, %r8
	jmp	.L3
	.size	__assert_fail_base, .-__assert_fail_base
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s%s%s:%u: %s%sAssertion `%s' failed.\n%n"
	.text
	.p2align 4,,15
	.globl	__GI___assert_fail
	.hidden	__GI___assert_fail
	.type	__GI___assert_fail, @function
__GI___assert_fail:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	%edx, %r12d
	subq	$8, %rsp
	movl	$5, %edx
	call	__GI___dcgettext
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__assert_fail_base
	.size	__GI___assert_fail, .-__GI___assert_fail
	.globl	__assert_fail
	.set	__assert_fail,__GI___assert_fail
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	errstr.11898, @object
	.size	errstr.11898, 19
errstr.11898:
	.string	"Unexpected error.\n"
	.hidden	__fxprintf
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
