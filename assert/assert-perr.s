	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%s%s%s:%u: %s%sUnexpected error: %s.\n%n"
	.text
	.p2align 4,,15
	.globl	__assert_perror_fail
	.hidden	__assert_perror_fail
	.type	__assert_perror_fail, @function
__assert_perror_fail:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$1024, %edx
	movq	%rcx, %r13
	subq	$1032, %rsp
	movq	%rsp, %rsi
	call	__strerror_r
	leaq	.LC0(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	%rax, %rbx
	movl	$5, %edx
	call	__dcgettext
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__assert_fail_base
	.size	__assert_perror_fail, .-__assert_perror_fail
	.hidden	__assert_fail_base
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__strerror_r
