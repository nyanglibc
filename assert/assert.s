	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	": "
.LC1:
	.string	""
.LC2:
	.string	"%s"
	.text
	.p2align 4,,15
	.globl	__assert_fail_base
	.hidden	__assert_fail_base
	.type	__assert_fail_base, @function
__assert_fail_base:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r14d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %r12
	movq	%r8, %rbx
	subq	$16, %rsp
	cmpq	$0, __pthread_setcancelstate@GOTPCREL(%rip)
	je	.L2
	xorl	%esi, %esi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
.L2:
	testq	%rbx, %rbx
	leaq	.LC0(%rip), %r8
	je	.L20
.L3:
	movq	__progname(%rip), %rdx
	leaq	.LC0(%rip), %rax
	leaq	.LC1(%rip), %rcx
	leaq	8(%rsp), %rdi
	movl	%r14d, %r9d
	movq	%rbp, %rsi
	cmpb	$0, (%rdx)
	cmovne	%rax, %rcx
	leaq	4(%rsp), %rax
	pushq	%rax
	pushq	%r13
	xorl	%eax, %eax
	pushq	%r8
	pushq	%rbx
	movq	%r12, %r8
	call	__asprintf
	addq	$32, %rsp
	testl	%eax, %eax
	js	.L5
	movq	8(%rsp), %rdx
	leaq	.LC2(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	stderr(%rip), %rdi
	call	_IO_fflush
	movq	_dl_pagesize(%rip), %rax
	movl	4(%rsp), %esi
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	addl	%eax, %esi
	negl	%eax
	andl	%eax, %esi
	movl	%esi, 4(%rsp)
	movslq	%esi, %rsi
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %rbx
	je	.L7
	movl	4(%rsp), %eax
	movq	8(%rsp), %rsi
	leaq	4(%rbx), %rdi
	movl	%eax, (%rbx)
	call	strcpy
#APP
# 77 "assert.c" 1
	xchgq %rbx, __abort_msg(%rip)
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	je	.L7
	movl	(%rbx), %esi
	movq	%rbx, %rdi
	call	__munmap
.L7:
	movq	8(%rsp), %rdi
	call	free@PLT
.L9:
	call	abort
.L5:
	leaq	errstr.11867(%rip), %rsi
	movl	$18, %edx
	movl	$2, %edi
	call	__libc_write
	jmp	.L9
.L20:
	leaq	.LC1(%rip), %rbx
	movq	%rbx, %r8
	jmp	.L3
	.size	__assert_fail_base, .-__assert_fail_base
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"%s%s%s:%u: %s%sAssertion `%s' failed.\n%n"
	.text
	.p2align 4,,15
	.globl	__assert_fail
	.hidden	__assert_fail
	.type	__assert_fail, @function
__assert_fail:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	.LC3(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	%edx, %r12d
	subq	$8, %rsp
	movl	$5, %edx
	call	__dcgettext
	movq	%r13, %r8
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__assert_fail_base
	.size	__assert_fail, .-__assert_fail
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	errstr.11867, @object
	.size	errstr.11867, 19
errstr.11867:
	.string	"Unexpected error.\n"
	.weak	__pthread_setcancelstate
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__libc_write
	.hidden	abort
	.hidden	__munmap
	.hidden	__abort_msg
	.hidden	strcpy
	.hidden	__mmap
	.hidden	_IO_fflush
	.hidden	__fxprintf
	.hidden	__asprintf
