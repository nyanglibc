#!/bin/sh
set -e
################################################################################
#### CONFIGURATION
destdir=
prefix=/nyan/glibc/nyan/1
user_id=0
group_id=0
################################################################################
build_dir=$(readlink -f .)
printf "BUILD_DIR=$build_dir\n"

mkdir -p $destdir$prefix/include
cp -rf $build_dir/include/* $destdir$prefix/include

mkdir -p $destdir$prefix/lib
cp -f $build_dir/libc.so.ld $destdir$prefix/lib/libc.so
chmod 644 $destdir$prefix/lib/libc.so
cp -f $build_dir/libc.a $destdir$prefix/lib/libc.a
chmod 644 $destdir$prefix/lib/libc.a
cp -f $build_dir/libc_nonshared.a $destdir$prefix/lib/libc_nonshared.a
chmod 644 $destdir$prefix/lib/libc_nonshared.a

# those will be used by the C compiler "driver" to generate working binaries
# with the glibc runtime (or the failure of the "standard" and gnu to split the
# C lib and runtime from the C compiler properly).
mkdir -p $destdir$prefix/lib
cp -f $build_dir/csu/Mcrt1.o $destdir$prefix/lib/Mcrt1.o
chmod 644 $destdir$prefix/lib/Mcrt1.o
cp -f $build_dir/csu/Scrt1.o $destdir$prefix/lib/Scrt1.o
chmod 644 $destdir$prefix/lib/Scrt1.o
cp -f $build_dir/csu/crt1.o $destdir$prefix/lib/crt1.o
chmod 644 $destdir$prefix/lib/crt1.o
cp -f $build_dir/csu/crti.o $destdir$prefix/lib/crti.o
chmod 644 $destdir$prefix/lib/crti.o
cp -f $build_dir/csu/crtn.o $destdir$prefix/lib/crtn.o
chmod 644 $destdir$prefix/lib/crtn.o
cp -f $build_dir/csu/gcrt1.o $destdir$prefix/lib/gcrt1.o
chmod 644 $destdir$prefix/lib/gcrt1.o
cp -f $build_dir/csu/grcrt1.o $destdir$prefix/lib/grcrt1.o
chmod 644 $destdir$prefix/lib/grcrt1.o
cp -f $build_dir/csu/rcrt1.o $destdir$prefix/lib/rcrt1.o
chmod 644 $destdir$prefix/lib/rcrt1.o

cp -f $build_dir/resolv/libanl/libanl.a $destdir$prefix/lib/libanl.a
chmod 644 $destdir$prefix/lib/libanl.a
ln -sTf libanl.so.1 $destdir$prefix/lib/libanl.so

cp -f $build_dir/crypt/libcrypt.a $destdir$prefix/lib/libcrypt.a
chmod 644 $destdir$prefix/lib/libcrypt.a
ln -sTf libcrypt.so.1 $destdir$prefix/lib/libcrypt.so

cp -f $build_dir/dlfcn/libdl/libdl.a $destdir$prefix/lib/libdl.a
chmod 644 $destdir$prefix/lib/libdl.a
ln -sTf libdl.so.2 $destdir$prefix/lib/libdl.so

cp -f $build_dir/mathvec/libmvec.a $destdir$prefix/lib/libmvec.a
chmod 644 $destdir$prefix/lib/libmvec.a
ln -sTf libmvec.so.1 $destdir$prefix/lib/libmvec.so

mkdir -p $destdir$prefix/lib
cp -f $build_dir/libm.so.ld $destdir$prefix/lib/libm.so
chmod 644 $destdir$prefix/lib/libm.so
cp -f $build_dir/math/libm/libm.a $destdir$prefix/lib/libm.a
chmod 644 $destdir$prefix/lib/libm.a

# really used?
ln -sTf libnss_compat.so.2 $destdir$prefix/lib/libnss_compat.so
ln -sTf libnss_db.so.2 $destdir$prefix/lib/libnss_db.so
ln -sTf libnss_dns.so.2 $destdir$prefix/lib/libnss_dns.so
ln -sTf libnss_files.so.2 $destdir$prefix/lib/libnss_files.so
ln -sTf libnss_hesiod.so.2 $destdir$prefix/lib/libnss_hesiod.so

cp -f $build_dir/nptl/libpthread.a $destdir$prefix/lib/libpthread.a
chmod 644 $destdir$prefix/lib/libpthread.a
ln -sTf libpthread.so.0 $destdir$prefix/lib/libpthread.so

cp -f $build_dir/resolv/libresolv/libresolv.a $destdir$prefix/lib/libresolv.a
chmod 644 $destdir$prefix/lib/libresolv.a
ln -sTf libresolv.so.2 $destdir$prefix/lib/libresolv.so

cp -f $build_dir/rt/librt.a $destdir$prefix/lib/librt.a
chmod 644 $destdir$prefix/lib/librt.a
ln -sTf librt.so.1 $destdir$prefix/lib/librt.so

ln -sTf libthread_db.so.1 $destdir$prefix/lib/libthread_db.so

cp -f $build_dir/login/libutil/libutil.a $destdir$prefix/lib/libutil.a
chmod 644 $destdir$prefix/lib/libutil.a
ln -sTf libutil.so.1 $destdir$prefix/lib/libutil.so

chown $user_id:$group_id -R $destdir$prefix || true
