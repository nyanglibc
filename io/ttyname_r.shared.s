	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"stdin"
.LC1:
	.string	"stdout"
.LC2:
	.string	"stderr"
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.type	getttyname_r, @function
getttyname_r:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$200, %rsp
	movq	%rsi, 16(%rsp)
	movl	%ecx, 44(%rsp)
	call	__GI_strlen
	movq	%r14, %rdi
	movq	%rax, 24(%rsp)
	call	__opendir
	testq	%rax, %rax
	je	.L19
	movq	%rax, %rbx
	leaq	48(%rsp), %rax
	leaq	.LC0(%rip), %r13
	leaq	.LC1(%rip), %r15
	movq	%rax, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbx, %rdi
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L22
	movq	8(%rbp), %rcx
	cmpq	%rcx, (%rax)
	je	.L4
	movl	(%r12), %edx
	testl	%edx, %edx
	je	.L2
.L4:
	leaq	19(%rax), %r8
	movl	$6, %ecx
	movq	%r13, %rdi
	movq	%r8, %rsi
	repz cmpsb
	je	.L2
	movl	$7, %ecx
	movq	%r8, %rsi
	movq	%r15, %rdi
	repz cmpsb
	je	.L2
	leaq	.LC2(%rip), %rdi
	movl	$7, %ecx
	movq	%r8, %rsi
	repz cmpsb
	je	.L2
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	__GI_strlen
	leaq	1(%rax), %rdx
	cmpq	16(%rsp), %rdx
	movq	8(%rsp), %r8
	ja	.L23
	movq	24(%rsp), %rax
	movq	%r8, %rsi
	leaq	(%r14,%rax), %rdi
	call	__GI___stpncpy
	movq	32(%rsp), %rsi
	movb	$0, (%rax)
	movq	%r14, %rdi
	call	__GI___stat64
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L2
	movq	8(%rbp), %rax
	cmpq	%rax, 56(%rsp)
	jne	.L2
	movq	0(%rbp), %rax
	cmpq	%rax, 48(%rsp)
	jne	.L2
	movl	72(%rsp), %eax
	andl	$61440, %eax
	cmpl	$8192, %eax
	jne	.L2
	movq	40(%rbp), %rax
	cmpq	%rax, 88(%rsp)
	jne	.L2
	movq	%rbx, %rdi
	movl	%edx, 8(%rsp)
	call	__closedir
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	44(%rsp), %ebx
	movl	8(%rsp), %edx
	movl	%ebx, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rbx, %rdi
	call	__closedir
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	44(%rsp), %ebx
	movl	$25, %edx
	movl	%ebx, %fs:(%rax)
.L1:
	addq	$200, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, (%r12)
	movl	%fs:(%rax), %edx
	jmp	.L1
.L23:
	movl	$-1, (%r12)
	movq	%rbx, %rdi
	call	__closedir
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %edx
	movl	$34, %fs:(%rax)
	jmp	.L1
	.size	getttyname_r, .-getttyname_r
	.section	.rodata.str1.1
.LC3:
	.string	"(unreachable)"
	.text
	.p2align 4,,15
	.globl	__ttyname_r
	.hidden	__ttyname_r
	.type	__ttyname_r, @function
__ttyname_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$424, %rsp
	testq	%rsi, %rsi
	movl	$0, 28(%rsp)
	je	.L59
	cmpq	$9, %rdx
	jbe	.L60
	movq	__libc_errno@gottpoff(%rip), %r13
	movq	%rsi, %rbx
	leaq	64(%rsp), %rsi
	movq	%rdx, %r12
	movl	%edi, %ebp
	movl	%fs:0(%r13), %r15d
	call	__GI___tcgetattr
	testl	%eax, %eax
	js	.L58
	leaq	128(%rsp), %r14
	movl	%ebp, %edi
	movq	%r14, %rsi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L58
	leaq	32(%rsp), %r8
	movl	$12132, %edx
	leaq	46(%rsp), %rsi
	xorl	%ecx, %ecx
	movabsq	$7310238724270485551, %rax
	movw	%dx, 44(%rsp)
	movslq	%ebp, %rdi
	movl	$10, %edx
	movq	%r8, 8(%rsp)
	movq	%rax, 32(%rsp)
	movl	$1714382444, 40(%rsp)
	movb	$0, 46(%rsp)
	call	_fitoa_word
	movq	8(%rsp), %r8
	leaq	-1(%r12), %rdx
	movb	$0, (%rax)
	movq	%rbx, %rsi
	movq	%r8, %rdi
	call	__readlink
	cmpq	$-1, %rax
	je	.L61
	cmpq	$13, %rax
	movq	%rax, %rbp
	jbe	.L35
	movabsq	$7521962890978293032, %rax
	cmpq	%rax, (%rbx)
	je	.L62
.L35:
	movb	$0, (%rbx,%rbp)
	cmpb	$47, (%rbx)
	leaq	272(%rsp), %rdx
	je	.L36
.L37:
	movl	$1, %ebp
.L32:
	movabsq	$8319397760812278831, %rax
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	movl	$47, %eax
	movw	%ax, 8(%rbx)
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L38
	movl	296(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L63
.L38:
	cmpl	$-1, 28(%rsp)
	movl	%r15d, %fs:0(%r13)
	je	.L51
	leaq	28(%rsp), %r9
.L43:
	subq	$5, %r12
	movq	%r9, %r8
	movb	$0, 5(%rbx)
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r9, 8(%rsp)
	call	getttyname_r
	cltq
	movq	8(%rsp), %r9
	testq	%rax, %rax
	jne	.L64
.L40:
	xorl	%eax, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L60:
	movq	__libc_errno@gottpoff(%rip), %r13
.L31:
	movl	$34, %fs:0(%r13)
	movl	$34, %eax
.L24:
	addq	$424, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movl	%fs:0(%r13), %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L59:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$22, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L51:
	movl	$1, %edx
	movl	$2, %eax
.L41:
	testl	%ebp, %ebp
	je	.L24
	testb	%dl, %dl
	je	.L24
	movq	168(%rsp), %rdx
	movq	%rdx, %rcx
	shrq	$32, %rdx
	shrq	$8, %rcx
	andl	$-4096, %edx
	andl	$4095, %ecx
	orl	%ecx, %edx
	subl	$136, %edx
	cmpl	$7, %edx
	ja	.L24
	movl	$19, %fs:0(%r13)
	movl	$19, %eax
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	$36, %fs:0(%r13)
	je	.L31
	xorl	%ebp, %ebp
	leaq	272(%rsp), %rdx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L64:
	cmpl	$-1, 28(%rsp)
	je	.L57
	movb	$0, 5(%rbx)
	movq	%r9, %r8
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	$1, 28(%rsp)
	call	getttyname_r
	cltq
.L57:
	testq	%rax, %rax
	setne	%dl
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	movq	%rdx, 8(%rsp)
	call	__GI___stat64
	testl	%eax, %eax
	movq	8(%rsp), %rdx
	jne	.L37
	movq	280(%rsp), %rcx
	cmpq	%rcx, 136(%rsp)
	jne	.L37
	movq	128(%rsp), %rcx
	cmpq	%rcx, 272(%rsp)
	jne	.L37
	movl	296(%rsp), %ecx
	andl	$61440, %ecx
	cmpl	$8192, %ecx
	jne	.L37
	movq	168(%rsp), %rcx
	cmpq	%rcx, 312(%rsp)
	jne	.L37
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L62:
	cmpl	$1701601889, 8(%rbx)
	jne	.L35
	cmpb	$41, 12(%rbx)
	jne	.L35
	subq	$13, %rbp
	leaq	13(%rbx), %rsi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	call	__GI_memmove
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	28(%rsp), %r9
	leaq	-9(%r12), %rsi
	movl	%r15d, %ecx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movq	%r9, %r8
	movq	%r9, 8(%rsp)
	call	getttyname_r
	cltq
	movq	8(%rsp), %r9
	testq	%rax, %rax
	je	.L40
	cmpl	$-1, 28(%rsp)
	je	.L57
	jmp	.L43
	.size	__ttyname_r, .-__ttyname_r
	.weak	ttyname_r
	.set	ttyname_r,__ttyname_r
	.hidden	__readlink
	.hidden	_fitoa_word
	.hidden	__closedir
	.hidden	__opendir
