	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	mkfifoat
	.type	mkfifoat, @function
mkfifoat:
	orb	$16, %dh
	xorl	%ecx, %ecx
	jmp	__GI___mknodat
	.size	mkfifoat, .-mkfifoat
