	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	lchmod
	.type	lchmod, @function
lchmod:
	movl	%esi, %edx
	movl	$256, %ecx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	__GI_fchmodat
	.size	lchmod, .-lchmod
