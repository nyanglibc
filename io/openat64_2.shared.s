	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"invalid openat64 call: O_CREAT or O_TMPFILE without mode"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__openat64_2
	.type	__openat64_2, @function
__openat64_2:
	testb	$64, %dl
	jne	.L2
	movl	%edx, %eax
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
	xorl	%eax, %eax
	jmp	__GI___openat64
.L2:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__GI___fortify_fail
	.size	__openat64_2, .-__openat64_2
