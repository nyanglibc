	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	mkfifo
	.type	mkfifo, @function
mkfifo:
	orl	$4096, %esi
	xorl	%edx, %edx
	jmp	__GI___mknod
	.size	mkfifo, .-mkfifo
