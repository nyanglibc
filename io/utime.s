	.text
	.p2align 4,,15
	.globl	__utime
	.type	__utime, @function
__utime:
	subq	$40, %rsp
	xorl	%edx, %edx
	testq	%rsi, %rsi
	je	.L2
	movq	(%rsi), %rax
	movq	$0, 8(%rsp)
	movq	%rsp, %rdx
	movq	$0, 24(%rsp)
	movq	%rax, (%rsp)
	movq	8(%rsi), %rax
	movq	%rax, 16(%rsp)
.L2:
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movl	$-100, %edi
	call	__utimensat64_helper
	addq	$40, %rsp
	ret
	.size	__utime, .-__utime
	.globl	utime
	.hidden	utime
	.set	utime,__utime
	.hidden	__utimensat64_helper
