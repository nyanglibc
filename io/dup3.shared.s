.text
.globl __dup3
.type __dup3,@function
.align 1<<4
__dup3:
	movl $292, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __dup3,.-__dup3
.globl __GI___dup3
.set __GI___dup3,__dup3
.weak dup3
dup3 = __dup3
.globl __GI_dup3
.set __GI_dup3,dup3
