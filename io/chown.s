.text
.globl __libc_chown
.type __libc_chown,@function
.align 1<<4
__libc_chown:
	movl $92, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __libc_chown,.-__libc_chown
.weak __chown
__chown = __libc_chown
.weak chown
chown = __libc_chown

