	.text
	.p2align 4,,15
	.globl	__GI___close_nocancel
	.hidden	__GI___close_nocancel
	.type	__GI___close_nocancel, @function
__GI___close_nocancel:
	movl	$3, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/close_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.size	__GI___close_nocancel, .-__GI___close_nocancel
	.hidden	rtld_errno
