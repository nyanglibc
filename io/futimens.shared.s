	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__futimens
	.type	__futimens, @function
__futimens:
	testl	%edi, %edi
	js	.L5
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	__GI___utimensat64_helper
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__futimens, .-__futimens
	.weak	futimens
	.set	futimens,__futimens
