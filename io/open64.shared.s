	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_open64
	.type	__libc_open64, @function
__libc_open64:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %r10d
	subq	$104, %rsp
	andl	$64, %r10d
	movq	%rdx, 64(%rsp)
	jne	.L2
	movl	%esi, %eax
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
.L3:
#APP
# 48 "../sysdeps/unix/sysv/linux/open64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	movl	%esi, %edx
	movl	$257, %eax
	movq	%rdi, %rsi
	movl	$-100, %edi
#APP
# 48 "../sysdeps/unix/sysv/linux/open64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L13
.L1:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	128(%rsp), %rax
	movl	$16, 24(%rsp)
	movl	64(%rsp), %r10d
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movl	%r10d, 12(%rsp)
	call	__libc_enable_asynccancel
	movl	12(%rsp), %r10d
	movl	%eax, %r8d
	movl	%ebx, %edx
	movq	%rbp, %rsi
	movl	$-100, %edi
	movl	$257, %eax
#APP
# 48 "../sysdeps/unix/sysv/linux/open64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
.L10:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L10
	.size	__libc_open64, .-__libc_open64
	.weak	open
	.set	open,__libc_open64
	.globl	__GI___open
	.hidden	__GI___open
	.set	__GI___open,__libc_open64
	.weak	__open
	.set	__open,__GI___open
	.globl	__GI___libc_open
	.hidden	__GI___libc_open
	.set	__GI___libc_open,__libc_open64
	.weak	open64
	.set	open64,__libc_open64
	.globl	__GI___open64
	.hidden	__GI___open64
	.set	__GI___open64,__libc_open64
	.weak	__open64
	.set	__open64,__GI___open64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
