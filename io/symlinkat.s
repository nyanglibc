.text
.globl symlinkat
.type symlinkat,@function
.align 1<<4
symlinkat:
	movl $266, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size symlinkat,.-symlinkat
