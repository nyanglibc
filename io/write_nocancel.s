	.text
	.p2align 4,,15
	.globl	__write_nocancel
	.hidden	__write_nocancel
	.type	__write_nocancel, @function
__write_nocancel:
	movl	$1, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/write_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	__write_nocancel, .-__write_nocancel
