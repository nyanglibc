.text
.globl __statfs
.type __statfs,@function
.align 1<<4
__statfs:
	movl $137, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __statfs,.-__statfs
.weak statfs
statfs = __statfs
.weak statfs64
statfs64 = __statfs
