	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_fcntl64
	.hidden	__GI___libc_fcntl64
	.type	__GI___libc_fcntl64, @function
__GI___libc_fcntl64:
	subq	$104, %rsp
	cmpl	$7, %esi
	leaq	112(%rsp), %rax
	movq	%rdx, 64(%rsp)
	movl	$16, 24(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	je	.L12
	cmpl	$38, %esi
	jne	.L4
.L12:
#APP
# 49 "../sysdeps/unix/sysv/linux/fcntl64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	movl	$72, %eax
#APP
# 49 "../sysdeps/unix/sysv/linux/fcntl64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L15
.L1:
	addq	$104, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	call	__fcntl64_nocancel_adjusted
	addq	$104, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%esi, 12(%rsp)
	movl	%edi, 8(%rsp)
	movq	%rdx, (%rsp)
	call	__libc_enable_asynccancel
	movq	(%rsp), %rdx
	movl	%eax, %r8d
	movl	12(%rsp), %esi
	movl	8(%rsp), %edi
	movl	$72, %eax
#APP
# 49 "../sysdeps/unix/sysv/linux/fcntl64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L16
.L10:
	movl	%r8d, %edi
	movl	%eax, (%rsp)
	call	__libc_disable_asynccancel
	movl	(%rsp), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
.L16:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L10
	.size	__GI___libc_fcntl64, .-__GI___libc_fcntl64
	.globl	__libc_fcntl64
	.set	__libc_fcntl64,__GI___libc_fcntl64
	.weak	fcntl
	.set	fcntl,__libc_fcntl64
	.weak	__GI___fcntl
	.set	__GI___fcntl,__libc_fcntl64
	.weak	__fcntl
	.set	__fcntl,__libc_fcntl64
	.weak	__GI___libc_fcntl
	.hidden	__GI___libc_fcntl
	.set	__GI___libc_fcntl,__libc_fcntl64
	.weak	fcntl64
	.set	fcntl64,__libc_fcntl64
	.weak	__GI___fcntl64
	.hidden	__GI___fcntl64
	.set	__GI___fcntl64,__libc_fcntl64
	.weak	__fcntl64
	.set	__fcntl64,__GI___fcntl64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
	.hidden	__fcntl64_nocancel_adjusted
