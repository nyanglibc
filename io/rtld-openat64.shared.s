	.text
	.p2align 4,,15
	.globl	__libc_openat64
	.type	__libc_openat64, @function
__libc_openat64:
	testb	$64, %dl
	movq	%rcx, -24(%rsp)
	jne	.L2
	movl	%edx, %eax
	xorl	%r10d, %r10d
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
.L3:
	movl	$257, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/openat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	8(%rsp), %rax
	movl	$24, -72(%rsp)
	movl	-24(%rsp), %r10d
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movq	%rax, -56(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.size	__libc_openat64, .-__libc_openat64
	.weak	openat
	.set	openat,__libc_openat64
	.globl	__openat
	.set	__openat,__libc_openat64
	.weak	openat64
	.set	openat64,__libc_openat64
	.globl	__openat64
	.set	__openat64,__libc_openat64
	.hidden	rtld_errno
