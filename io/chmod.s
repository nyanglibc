.text
.globl __chmod
.type __chmod,@function
.align 1<<4
__chmod:
	movl $90, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __chmod,.-__chmod
.weak chmod
chmod = __chmod
