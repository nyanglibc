	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___read_nocancel
	.hidden	__GI___read_nocancel
	.type	__GI___read_nocancel, @function
__GI___read_nocancel:
	xorl	%eax, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/read_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	__GI___read_nocancel, .-__GI___read_nocancel
	.globl	__read_nocancel
	.set	__read_nocancel,__GI___read_nocancel
