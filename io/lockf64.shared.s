	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__lockf64
	.type	__lockf64, @function
__lockf64:
	pushq	%rbx
	pxor	%xmm0, %xmm0
	subq	$32, %rsp
	cmpl	$1, %esi
	movups	%xmm0, 4(%rsp)
	movq	$0, 20(%rsp)
	movl	$0, 28(%rsp)
	movl	$65537, (%rsp)
	movq	%rdx, 16(%rsp)
	je	.L3
	jle	.L14
	cmpl	$2, %esi
	je	.L6
	cmpl	$3, %esi
	jne	.L2
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$5, %esi
	movw	%dx, (%rsp)
	movq	%rsp, %rdx
	call	__GI___fcntl
	testl	%eax, %eax
	js	.L11
	cmpw	$2, (%rsp)
	je	.L10
	movl	24(%rsp), %ebx
	call	__GI___getpid
	cmpl	%eax, %ebx
	jne	.L15
.L10:
	addq	$32, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rsp, %rdx
	movl	$6, %esi
	xorl	%eax, %eax
	call	__GI___fcntl64
.L1:
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%esi, %esi
	jne	.L2
	movl	$2, %eax
	movq	%rsp, %rdx
	movl	$6, %esi
	movw	%ax, (%rsp)
	xorl	%eax, %eax
	call	__GI___fcntl64
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$32, %rsp
	movl	$-1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rsp, %rdx
	movl	$7, %esi
	xorl	%eax, %eax
	call	__GI___fcntl64
	addq	$32, %rsp
	popq	%rbx
	ret
.L15:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$13, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
.L11:
	movl	$-1, %eax
	jmp	.L1
	.size	__lockf64, .-__lockf64
	.weak	lockf64
	.set	lockf64,__lockf64
	.weak	lockf
	.set	lockf,lockf64
