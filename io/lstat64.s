	.text
	.p2align 4,,15
	.globl	__lstat64
	.hidden	__lstat64
	.type	__lstat64, @function
__lstat64:
	movq	%rsi, %rdx
	movl	$256, %ecx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	__fstatat64
	.size	__lstat64, .-__lstat64
	.weak	lstat
	.set	lstat,__lstat64
	.globl	__lstat
	.set	__lstat,__lstat64
	.weak	lstat64
	.set	lstat64,__lstat64
	.hidden	__fstatat64
