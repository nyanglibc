	.text
	.p2align 4,,15
	.globl	__libc_write
	.hidden	__libc_write
	.type	__libc_write, @function
__libc_write:
#APP
# 26 "../sysdeps/unix/sysv/linux/write.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/write.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movq	%r12, %rdx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$1, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/write.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_write, .-__libc_write
	.weak	write
	.hidden	write
	.set	write,__libc_write
	.weak	__write
	.hidden	__write
	.set	__write,__libc_write
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
