	.text
	.p2align 4,,15
	.globl	__internal_statvfs
	.hidden	__internal_statvfs
	.type	__internal_statvfs, @function
__internal_statvfs:
	movq	72(%rdx), %rcx
	movq	8(%rdx), %rax
	pxor	%xmm0, %xmm0
	testq	%rcx, %rcx
	movq	%rax, (%rsi)
	cmovne	%rcx, %rax
	movq	56(%rdx), %rcx
	movq	%rax, 8(%rsi)
	movq	16(%rdx), %rax
	movq	%rax, 16(%rsi)
	movq	24(%rdx), %rax
	movq	%rax, 24(%rsi)
	movq	32(%rdx), %rax
	movq	%rax, 32(%rsi)
	movq	40(%rdx), %rax
	movq	%rax, 40(%rsi)
	movq	48(%rdx), %rax
	movq	%rcx, 64(%rsi)
	movq	64(%rdx), %rcx
	movups	%xmm0, 88(%rsi)
	movq	%rax, 48(%rsi)
	movq	$0, 104(%rsi)
	movq	%rax, 56(%rsi)
	movq	80(%rdx), %rax
	movq	%rcx, 80(%rsi)
	xorq	$32, %rax
	movq	%rax, 72(%rsi)
	ret
	.size	__internal_statvfs, .-__internal_statvfs
