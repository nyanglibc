.text
.globl __symlink
.type __symlink,@function
.align 1<<4
__symlink:
	movl $88, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __symlink,.-__symlink
.globl __GI___symlink
.set __GI___symlink,__symlink
.weak symlink
symlink = __symlink
.globl __GI_symlink
.set __GI_symlink,symlink
