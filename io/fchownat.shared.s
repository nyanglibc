.text
.globl fchownat
.type fchownat,@function
.align 1<<4
fchownat:
	movq %rcx, %r10
	movl $260, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size fchownat,.-fchownat
.globl __GI_fchownat
.set __GI_fchownat,fchownat
