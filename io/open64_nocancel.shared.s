	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___open64_nocancel
	.hidden	__GI___open64_nocancel
	.type	__GI___open64_nocancel, @function
__GI___open64_nocancel:
	movl	%esi, %r10d
	movq	%rdx, -32(%rsp)
	andl	$64, %r10d
	jne	.L2
	movl	%esi, %eax
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
.L3:
	movl	%esi, %edx
	movl	$257, %eax
	movq	%rdi, %rsi
	movl	$-100, %edi
#APP
# 45 "../sysdeps/unix/sysv/linux/open64_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	8(%rsp), %rax
	movl	$16, -72(%rsp)
	movl	-32(%rsp), %r10d
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movq	%rax, -56(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___open64_nocancel, .-__GI___open64_nocancel
	.globl	__open64_nocancel
	.set	__open64_nocancel,__GI___open64_nocancel
	.globl	__GI___open_nocancel
	.hidden	__GI___open_nocancel
	.set	__GI___open_nocancel,__open64_nocancel
	.globl	__open_nocancel
	.set	__open_nocancel,__GI___open_nocancel
