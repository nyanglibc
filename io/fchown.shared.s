.text
.globl __fchown
.type __fchown,@function
.align 1<<4
__fchown:
	movl $93, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __fchown,.-__fchown
.globl __GI___fchown
.set __GI___fchown,__fchown
.weak fchown
fchown = __fchown
.globl __GI_fchown
.set __GI_fchown,fchown
