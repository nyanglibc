	.text
	.p2align 4,,15
	.globl	__mknodat
	.hidden	__mknodat
	.type	__mknodat, @function
__mknodat:
	movl	%ecx, %eax
	cmpq	%rax, %rcx
	jne	.L7
	movl	%ecx, %r10d
	movl	$259, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/mknodat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__mknodat, .-__mknodat
	.weak	mknodat
	.set	mknodat,__mknodat
