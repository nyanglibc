	.text
	.p2align 4,,15
	.globl	__GI___pread64_nocancel
	.hidden	__GI___pread64_nocancel
	.type	__GI___pread64_nocancel, @function
__GI___pread64_nocancel:
	movq	%rcx, %r10
	movl	$17, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/pread64_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movq	$-1, %rax
	ret
	.size	__GI___pread64_nocancel, .-__GI___pread64_nocancel
	.globl	__pread64_nocancel
	.set	__pread64_nocancel,__GI___pread64_nocancel
	.hidden	rtld_errno
