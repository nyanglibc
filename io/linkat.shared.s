.text
.globl linkat
.type linkat,@function
.align 1<<4
linkat:
	movq %rcx, %r10
	movl $265, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size linkat,.-linkat
.globl __GI_linkat
.set __GI_linkat,linkat
