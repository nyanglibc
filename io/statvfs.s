	.text
	.p2align 4,,15
	.globl	__statvfs
	.type	__statvfs, @function
__statvfs:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__statfs
	testl	%eax, %eax
	js	.L3
	movl	$-1, %ecx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__internal_statvfs
	xorl	%eax, %eax
.L1:
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__statvfs, .-__statvfs
	.weak	statvfs64
	.set	statvfs64,__statvfs
	.globl	__statvfs64
	.set	__statvfs64,__statvfs
	.weak	statvfs
	.hidden	statvfs
	.set	statvfs,__statvfs
	.hidden	__internal_statvfs
	.hidden	__statfs
