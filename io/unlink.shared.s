.text
.globl __unlink
.type __unlink,@function
.align 1<<4
__unlink:
	movl $87, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __unlink,.-__unlink
.globl __GI___unlink
.set __GI___unlink,__unlink
.weak unlink
unlink = __unlink
.globl __GI_unlink
.set __GI_unlink,unlink
