	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4,,15
	.type	internal_fallocate, @function
internal_fallocate:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$296, %rsp
	testq	%rsi, %rsi
	js	.L15
	testq	%rdx, %rdx
	movq	%rdx, %rbx
	js	.L15
	movq	%rsi, %rax
	movq	%rsi, %r15
	movl	$27, %ebp
	addq	%rdx, %rax
	js	.L1
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$3, %esi
	movl	%edi, %r13d
	call	__fcntl
	testl	%eax, %eax
	js	.L4
	testb	$4, %ah
	jne	.L4
	leaq	144(%rsp), %rsi
	movl	%r13d, %edi
	call	__fstat64
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L4
	movl	168(%rsp), %eax
	andl	$61440, %eax
	cmpl	$4096, %eax
	je	.L17
	cmpl	$32768, %eax
	jne	.L18
	testq	%rbx, %rbx
	jne	.L5
	cmpq	%r15, 192(%rsp)
	jge	.L1
	movq	%r15, %rsi
	movl	%r13d, %edi
	call	__ftruncate
	testl	%eax, %eax
	je	.L1
.L32:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebp
.L1:
	addq	$296, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$9, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$22, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	16(%rsp), %rsi
	movl	%r13d, %edi
	movq	%rsi, 8(%rsp)
	call	__fstatfs64
	testl	%eax, %eax
	jne	.L32
	movq	24(%rsp), %rax
	movl	$512, %r14d
	testq	%rax, %rax
	je	.L9
	movl	%eax, %r14d
	cmpq	$4096, %rax
	movl	$4096, %eax
	cmovge	%rax, %r14
.L9:
	leaq	-1(%rbx), %rax
	leaq	.LC0(%rip), %r12
	cqto
	idivq	%r14
	addq	%rdx, %r15
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r15, %rcx
	movl	$1, %edx
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	__pwrite
	cmpq	$1, %rax
	jne	.L32
.L13:
	addq	%r14, %r15
	testq	%rbx, %rbx
	jle	.L1
.L14:
	subq	%r14, %rbx
	cmpq	%r15, 192(%rsp)
	jle	.L10
	movq	8(%rsp), %rsi
	movq	%r15, %rcx
	movl	$1, %edx
	movl	%r13d, %edi
	call	__pread
	testq	%rax, %rax
	js	.L32
	cmpq	$1, %rax
	jne	.L10
	cmpb	$0, 16(%rsp)
	je	.L10
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$19, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$29, %ebp
	jmp	.L1
	.size	internal_fallocate, .-internal_fallocate
	.p2align 4,,15
	.globl	posix_fallocate
	.type	posix_fallocate, @function
posix_fallocate:
	pushq	%rbx
	movq	%rsi, %r9
	movq	%rdx, %rbx
	movq	%rdx, %r10
	movl	$285, %eax
	movq	%rsi, %rdx
	xorl	%esi, %esi
#APP
# 29 "../sysdeps/unix/sysv/linux/posix_fallocate.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L37
	cmpl	$-95, %eax
	je	.L36
	negl	%eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rbx, %rdx
	movq	%r9, %rsi
	popq	%rbx
	jmp	internal_fallocate
	.size	posix_fallocate, .-posix_fallocate
	.hidden	__pread
	.hidden	__pwrite
	.hidden	__fstatfs64
	.hidden	__ftruncate
	.hidden	__fstat64
	.hidden	__fcntl
