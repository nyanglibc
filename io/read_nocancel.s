	.text
	.p2align 4,,15
	.globl	__read_nocancel
	.hidden	__read_nocancel
	.type	__read_nocancel, @function
__read_nocancel:
	xorl	%eax, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/read_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	__read_nocancel, .-__read_nocancel
