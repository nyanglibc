	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"invalid open64 call: O_CREAT or O_TMPFILE without mode"
	.text
	.p2align 4,,15
	.globl	__open64_2
	.type	__open64_2, @function
__open64_2:
	testb	$64, %sil
	jne	.L2
	movl	%esi, %eax
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
	xorl	%eax, %eax
	jmp	__open64
.L2:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__fortify_fail
	.size	__open64_2, .-__open64_2
	.hidden	__fortify_fail
	.hidden	__open64
