	.text
	.p2align 4,,15
	.globl	__fcntl64_nocancel_adjusted
	.hidden	__fcntl64_nocancel_adjusted
	.type	__fcntl64_nocancel_adjusted, @function
__fcntl64_nocancel_adjusted:
	cmpl	$9, %esi
	je	.L10
	movl	$72, %eax
#APP
# 63 "../sysdeps/unix/sysv/linux/fcntl_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	-8(%rsp), %rdx
	movl	$16, %esi
	movl	$72, %eax
#APP
# 55 "../sysdeps/unix/sysv/linux/fcntl_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L11
.L8:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$2, -8(%rsp)
	movl	-4(%rsp), %eax
	jne	.L1
	negl	%eax
	ret
	.size	__fcntl64_nocancel_adjusted, .-__fcntl64_nocancel_adjusted
	.p2align 4,,15
	.globl	__GI___fcntl64_nocancel
	.hidden	__GI___fcntl64_nocancel
	.type	__GI___fcntl64_nocancel, @function
__GI___fcntl64_nocancel:
	subq	$80, %rsp
	leaq	88(%rsp), %rax
	movq	%rdx, 48(%rsp)
	movl	$16, 8(%rsp)
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	call	__fcntl64_nocancel_adjusted
	addq	$80, %rsp
	ret
	.size	__GI___fcntl64_nocancel, .-__GI___fcntl64_nocancel
	.globl	__fcntl64_nocancel
	.set	__fcntl64_nocancel,__GI___fcntl64_nocancel
	.hidden	rtld_errno
