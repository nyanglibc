	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___access
	.hidden	__GI___access
	.type	__GI___access, @function
__GI___access:
	movl	$21, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/access.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___access, .-__GI___access
	.globl	__access
	.set	__access,__GI___access
	.weak	access
	.set	access,__access
