	.text
	.p2align 4,,15
	.globl	__faccessat
	.type	__faccessat, @function
__faccessat:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r14d
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r10d
	pushq	%rbx
	movl	$439, %ecx
	movl	%edi, %ebx
	movq	%rsi, %rbp
	movl	%edx, %r13d
	movl	%ecx, %eax
	subq	$144, %rsp
#APP
# 29 "../sysdeps/unix/sysv/linux/faccessat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L33
	testl	%eax, %eax
	jne	.L4
.L11:
	xorl	%eax, %eax
.L1:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %r12
	movl	%fs:(%r12), %edx
.L3:
	cmpl	$38, %edx
	jne	.L1
	testl	$-769, %r14d
	jne	.L34
	testl	%r14d, %r14d
	je	.L7
	testl	$-513, %r14d
	jne	.L8
	movl	__libc_enable_secure(%rip), %eax
	testl	%eax, %eax
	je	.L7
.L8:
	movl	%r14d, %ecx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	andl	$256, %ecx
	movq	%rsp, %rdx
	call	__fstatat64
	testl	%eax, %eax
	jne	.L21
	movl	%r13d, %ebx
	andl	$7, %ebx
	je	.L11
	andl	$512, %r14d
	je	.L12
	call	__geteuid
.L13:
	testl	%eax, %eax
	jne	.L14
	andl	$1, %r13d
	je	.L11
	testb	$73, 24(%rsp)
	jne	.L11
.L14:
	cmpl	%eax, 28(%rsp)
	je	.L35
	testl	%r14d, %r14d
	movl	32(%rsp), %ebp
	je	.L17
	call	__getegid
.L18:
	cmpl	%eax, %ebp
	je	.L20
	movl	32(%rsp), %edi
	call	__group_member
	testl	%eax, %eax
	jne	.L20
	movl	24(%rsp), %eax
	andl	%ebx, %eax
	.p2align 4,,10
	.p2align 3
.L16:
	cmpl	%ebx, %eax
	je	.L11
	movl	$13, %fs:(%r12)
	orl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%r13d, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$269, %eax
#APP
# 40 "../sysdeps/unix/sysv/linux/faccessat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L1
	negl	%eax
	movl	%eax, %fs:(%r12)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	movq	__libc_errno@gottpoff(%rip), %r12
	negl	%edx
	movl	$-1, %eax
	movl	%edx, %fs:(%r12)
	jmp	.L3
.L34:
	movl	$22, %fs:(%r12)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	call	__getuid
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L20:
	leal	0(,%rbx,8), %eax
	andl	24(%rsp), %eax
	shrl	$3, %eax
	jmp	.L16
.L35:
	movl	%ebx, %eax
	sall	$6, %eax
	andl	24(%rsp), %eax
	shrl	$6, %eax
	jmp	.L16
.L17:
	call	__getgid
	jmp	.L18
.L21:
	movl	$-1, %eax
	jmp	.L1
	.size	__faccessat, .-__faccessat
	.weak	faccessat
	.set	faccessat,__faccessat
	.hidden	__getgid
	.hidden	__getuid
	.hidden	__group_member
	.hidden	__getegid
	.hidden	__geteuid
	.hidden	__fstatat64
