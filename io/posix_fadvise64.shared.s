	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___posix_fadvise64_l64
	.hidden	__GI___posix_fadvise64_l64
	.type	__GI___posix_fadvise64_l64, @function
__GI___posix_fadvise64_l64:
	movl	%ecx, %r10d
	movl	$221, %eax
#APP
# 48 "../sysdeps/unix/sysv/linux/posix_fadvise64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	negl	%edx
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmova	%edx, %eax
	ret
	.size	__GI___posix_fadvise64_l64, .-__GI___posix_fadvise64_l64
	.weak	posix_fadvise64
	.set	posix_fadvise64,__GI___posix_fadvise64_l64
	.globl	posix_fadvise
	.set	posix_fadvise,__GI___posix_fadvise64_l64
	.globl	__posix_fadvise64_l64
	.set	__posix_fadvise64_l64,__GI___posix_fadvise64_l64
