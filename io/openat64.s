	.text
	.p2align 4,,15
	.globl	__libc_openat64
	.type	__libc_openat64, @function
__libc_openat64:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$96, %rsp
	testb	$64, %dl
	movq	%rcx, 72(%rsp)
	jne	.L2
	movl	%edx, %eax
	xorl	%r10d, %r10d
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
.L3:
#APP
# 45 "../sysdeps/unix/sysv/linux/openat64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	movl	$257, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/openat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
.L1:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	128(%rsp), %rax
	movl	$24, 24(%rsp)
	movl	72(%rsp), %r10d
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, %ebx
	movq	%rsi, %r12
	movl	%edi, %ebp
	movl	%r10d, 12(%rsp)
	call	__libc_enable_asynccancel
	movl	12(%rsp), %r10d
	movl	%eax, %r8d
	movl	%ebx, %edx
	movq	%r12, %rsi
	movl	%ebp, %edi
	movl	$257, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/openat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L15
.L10:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
.L15:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L10
	.size	__libc_openat64, .-__libc_openat64
	.weak	openat
	.set	openat,__libc_openat64
	.globl	__openat
	.hidden	__openat
	.set	__openat,__libc_openat64
	.weak	openat64
	.set	openat64,__libc_openat64
	.globl	__openat64
	.hidden	__openat64
	.set	__openat64,__libc_openat64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
