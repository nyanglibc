.text
.globl __fstatfs
.type __fstatfs,@function
.align 1<<4
__fstatfs:
	movl $138, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __fstatfs,.-__fstatfs
.globl __GI___fstatfs
.set __GI___fstatfs,__fstatfs
.weak fstatfs
fstatfs = __fstatfs
.globl __GI_fstatfs
.set __GI_fstatfs,fstatfs
.weak fstatfs64
fstatfs64 = __fstatfs
.globl __GI_fstatfs64
.set __GI_fstatfs64,fstatfs64
.weak __fstatfs64
__fstatfs64 = __fstatfs
.globl __GI___fstatfs64
.set __GI___fstatfs64,__fstatfs64
