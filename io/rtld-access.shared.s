	.text
	.p2align 4,,15
	.globl	__access
	.hidden	__access
	.type	__access, @function
__access:
	movl	$21, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/access.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.size	__access, .-__access
	.weak	access
	.set	access,__access
	.hidden	rtld_errno
