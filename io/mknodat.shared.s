	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___mknodat
	.hidden	__GI___mknodat
	.type	__GI___mknodat, @function
__GI___mknodat:
	movl	%ecx, %eax
	cmpq	%rax, %rcx
	jne	.L7
	movl	%ecx, %r10d
	movl	$259, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/mknodat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI___mknodat, .-__GI___mknodat
	.globl	__mknodat
	.set	__mknodat,__GI___mknodat
	.weak	mknodat
	.set	mknodat,__mknodat
