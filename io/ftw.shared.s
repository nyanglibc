	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_nftw,nftw@@GLIBC_2.3.3
	.symver __old_nftw,nftw@GLIBC_2.2.5
	.symver __new_nftw64,nftw64@@GLIBC_2.3.3
	.symver __old_nftw64,nftw64@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	object_compare, @function
object_compare:
.LFB49:
	movq	8(%rsi), %rdx
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	setb	%dl
	seta	%al
	movzbl	%dl, %edx
	subl	%edx, %eax
	jne	.L1
	movq	(%rsi), %rdx
	movq	(%rdi), %rcx
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	setb	%dl
	seta	%al
	movzbl	%dl, %edx
	subl	%edx, %eax
.L1:
	rep ret
.LFE49:
	.size	object_compare, .-object_compare
	.p2align 4,,15
	.type	add_object.isra.1, @function
add_object.isra.1:
.LFB60:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$16, %edi
	movq	%rdx, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	je	.L6
	movq	(%r12), %rdx
	leaq	80(%rbx), %rsi
	movq	%rax, %rdi
	movq	%rdx, (%rax)
	movq	0(%rbp), %rdx
	movq	%rdx, 8(%rax)
	leaq	object_compare(%rip), %rdx
	call	__GI___tsearch
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	negl	%eax
.L4:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-1, %eax
	jmp	.L4
.LFE60:
	.size	add_object.isra.1, .-add_object.isra.1
	.p2align 4,,15
	.type	process_entry.isra.3, @function
process_entry.isra.3:
.LFB62:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rdx, %rbp
	subq	$168, %rsp
	cmpb	$46, (%rdx)
	je	.L53
.L9:
	movslq	40(%rbx), %rax
	movq	24(%rbx), %rdi
	leaq	2(%rax,%r13), %rsi
	cmpq	32(%rbx), %rsi
	jbe	.L13
	addq	%rsi, %rsi
	movq	%rsi, 32(%rbx)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L20
	movq	%rax, 24(%rbx)
	movslq	40(%rbx), %rax
.L13:
	addq	%rax, %rdi
	movq	%r13, %rdx
	movq	%rbp, %rsi
	call	__GI_mempcpy@PLT
	movb	$0, (%rax)
	movl	8(%r12), %edi
	cmpl	$-1, %edi
	jne	.L54
	movl	48(%rbx), %eax
	testb	$4, %al
	jne	.L17
	movq	24(%rbx), %rbp
.L17:
	leaq	16(%rsp), %r13
	testb	$1, %al
	movq	%rbp, %rdi
	movq	%r13, %rsi
	je	.L18
	call	__lstat@PLT
.L16:
	testl	%eax, %eax
	js	.L55
.L19:
	movl	40(%rsp), %eax
	movl	48(%rbx), %ecx
	andl	$61440, %eax
	movl	%ecx, %edx
	andl	$2, %edx
	cmpl	$16384, %eax
	je	.L26
	cmpl	$40960, %eax
	sete	%al
	movzbl	%al, %eax
	sall	$2, %eax
.L27:
	testl	%edx, %edx
	jne	.L34
.L52:
	cltq
	salq	$2, %rax
.L29:
	movq	56(%rbx), %rdx
	leaq	40(%rbx), %rcx
	movq	24(%rbx), %rdi
	movq	%r13, %rsi
	movl	(%rdx,%rax), %edx
	call	*64(%rbx)
.L31:
	testb	$16, 48(%rbx)
	je	.L8
	cmpl	$2, %eax
	jne	.L8
.L12:
	xorl	%eax, %eax
.L8:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movl	48(%rbx), %ecx
	leaq	16(%rsp), %r13
	movq	%rbp, %rsi
	movq	%r13, %rdx
	sall	$8, %ecx
	andl	$256, %ecx
	call	__fstatat@PLT
	testl	%eax, %eax
	jns	.L19
.L55:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$13, %eax
	je	.L37
	cmpl	$2, %eax
	je	.L37
.L20:
	addq	$168, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	movzbl	1(%rdx), %eax
	testb	%al, %al
	je	.L12
	cmpb	$46, %al
	jne	.L9
	cmpb	$0, 2(%rdx)
	jne	.L9
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L18:
	call	__stat@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L37:
	testb	$1, 48(%rbx)
	jne	.L22
	movl	8(%r12), %edi
	cmpl	$-1, %edi
	je	.L23
	movl	$256, %ecx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	call	__fstatat@PLT
.L24:
	testl	%eax, %eax
	jne	.L22
	movl	40(%rsp), %eax
	andl	$61440, %eax
	cmpl	$40960, %eax
	jne	.L22
	movl	48(%rbx), %edx
	movl	$6, %eax
	andl	$2, %edx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L26:
	testl	%edx, %edx
	je	.L28
	movq	16(%rsp), %rax
	cmpq	%rax, 72(%rbx)
	jne	.L12
.L28:
	andl	$1, %ecx
	je	.L30
.L32:
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	ftw_dir
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L22:
	movl	$12, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L34:
	movq	72(%rbx), %rcx
	cmpq	%rcx, 16(%rsp)
	jne	.L12
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	__lstat@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	movq	16(%rsp), %rax
	leaq	80(%rbx), %rsi
	leaq	object_compare(%rip), %rdx
	movq	%rsp, %rdi
	movq	%rax, (%rsp)
	movq	24(%rsp), %rax
	movq	%rax, 8(%rsp)
	call	__GI___tfind
	testq	%rax, %rax
	jne	.L12
	leaq	8(%r13), %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	add_object.isra.1
	testl	%eax, %eax
	je	.L32
	jmp	.L31
.LFE62:
	.size	process_entry.isra.3, .-process_entry.isra.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"."
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"../sysdeps/wordsize-64/../../io/ftw.c"
	.section	.rodata.str1.1
.LC2:
	.string	"startp != data->dirbuf"
.LC3:
	.string	"dir.content == NULL"
.LC4:
	.string	"/"
.LC5:
	.string	".."
	.text
	.p2align 4,,15
	.type	ftw_dir, @function
ftw_dir:
.LFB54:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movl	40(%rdi), %ecx
	testq	%rdx, %rdx
	movq	%rsi, 16(%rsp)
	movq	%rdx, 32(%rsp)
	movl	%ecx, 44(%rsp)
	je	.L57
	movq	%rdx, %rax
	movq	8(%rdi), %rdx
	addq	$8, %rax
	movq	%rax, 24(%rsp)
	movq	(%rdi), %rax
	movq	(%rax,%rdx,8), %rbx
	testq	%rbx, %rbx
	je	.L58
.L105:
	movl	$1024, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L62
	movq	(%rbx), %rax
	xorl	%ebp, %ebp
	movl	$1024, %r14d
	movq	%rax, (%rsp)
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	19(%rax), %rbx
	movq	%rbx, %rdi
	call	__GI_strlen
	leaq	(%rax,%rbp), %r13
	movq	%rax, %rdx
	leaq	2(%r13), %rax
	cmpq	%r14, %rax
	jb	.L61
	leaq	(%rdx,%rdx), %rax
	movl	$1024, %ecx
	movq	%r15, %rdi
	movq	%rdx, 8(%rsp)
	cmpq	$1024, %rax
	cmovb	%rcx, %rax
	addq	%rax, %r14
	movq	%r14, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	8(%rsp), %rdx
	je	.L165
	movq	%rax, %r15
.L61:
	leaq	(%r15,%rbp), %rdi
	movq	%rbx, %rsi
	leaq	1(%r13), %rbp
	call	__GI_mempcpy@PLT
	movb	$0, (%rax)
.L60:
	movq	(%rsp), %rdi
	call	__GI___readdir64
	testq	%rax, %rax
	jne	.L63
	movq	8(%r12), %rdx
	movq	(%r12), %rax
	leaq	1(%rbp), %rsi
	movb	$0, (%r15,%rbp)
	movq	%r15, %rdi
	leaq	(%rax,%rdx,8), %rbx
	movq	(%rbx), %r13
	call	realloc@PLT
	movq	%rax, 16(%r13)
	movq	(%rbx), %rax
	cmpq	$0, 16(%rax)
	je	.L165
	movq	(%rsp), %rdi
	call	__closedir
	movq	8(%r12), %rdx
	movq	(%r12), %rax
	cmpq	$0, 24(%rsp)
	movq	24(%r12), %rdi
	leaq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdx
	movq	$0, (%rdx)
	movl	$-1, 8(%rdx)
	movq	$0, (%rax)
	jne	.L107
	.p2align 4,,10
	.p2align 3
.L65:
	testb	$4, 48(%r12)
	je	.L68
	movslq	40(%r12), %rax
	addq	%rax, %rdi
	leaq	.LC0(%rip), %rax
	cmpb	$0, (%rdi)
	cmove	%rax, %rdi
.L68:
	call	__opendir
	movq	%rax, 48(%rsp)
.L67:
	testq	%rax, %rax
	je	.L62
.L66:
	movq	%rax, %rdi
	leaq	48(%rsp), %rbx
	call	__dirfd@PLT
	movq	(%r12), %rdx
	movl	%eax, 56(%rsp)
	movq	8(%r12), %rax
	movq	$0, 64(%rsp)
	movq	%rbx, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	16(%r12), %rax
	movq	%rax, 8(%r12)
	jne	.L69
	movq	$0, 8(%r12)
.L69:
	movl	48(%r12), %eax
	testb	$8, %al
	jne	.L72
	leaq	40(%r12), %rcx
	movq	24(%r12), %rdi
	movl	$1, %edx
	movq	16(%rsp), %rsi
	call	*64(%r12)
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L73
	movl	48(%r12), %eax
.L72:
	testb	$4, %al
	jne	.L75
.L78:
	addl	$1, 44(%r12)
	movq	24(%r12), %rbp
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	__GI___rawmemchr
	cmpq	%rax, %rbp
	je	.L166
	cmpb	$47, -1(%rax)
	je	.L79
	movb	$47, (%rax)
	movq	24(%r12), %rbp
	addq	$1, %rax
.L79:
	subq	%rbp, %rax
	movl	%eax, 40(%r12)
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L168:
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L167
	leaq	19(%rax), %rbp
	movq	%rbp, %rdi
	call	__GI_strlen
	movq	%rbp, %rdx
	movq	%rax, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	process_entry.isra.3
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L81
.L80:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.L168
	movq	64(%rsp), %rax
	movq	%rax, %r13
.L103:
	cmpb	$0, (%rax)
	jne	.L91
.L88:
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L165:
	movq	%r15, %rdi
	call	free@PLT
.L62:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$13, %fs:(%rax)
	jne	.L70
	leaq	40(%r12), %rcx
	movq	24(%r12), %rdi
	movl	$2, %edx
	movq	16(%rsp), %rsi
	call	*64(%r12)
	movl	%eax, %ebp
.L56:
	addq	$88, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	24(%rdi), %rdi
.L107:
	movq	24(%rsp), %rax
	movl	(%rax), %eax
	cmpl	$-1, %eax
	je	.L65
	movslq	40(%r12), %rsi
	movl	$67584, %edx
	addq	%rdi, %rsi
	movl	%eax, %edi
	xorl	%eax, %eax
	call	__GI___openat64_nocancel
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movq	$0, 48(%rsp)
	je	.L62
	movl	%eax, %edi
	call	__fdopendir
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L66
	movl	%ebx, %edi
	call	__GI___close_nocancel
	movq	48(%rsp), %rax
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L57:
	movq	8(%rdi), %rdx
	movq	(%rdi), %rax
	movq	$0, 24(%rsp)
	movq	(%rax,%rdx,8), %rbx
	testq	%rbx, %rbx
	jne	.L105
	movq	24(%r12), %rdi
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L75:
	movq	48(%rsp), %rdi
	call	__dirfd@PLT
	movl	%eax, %edi
	call	__fchdir
	testl	%eax, %eax
	jns	.L78
	movl	$-1, %ebp
	.p2align 4,,10
	.p2align 3
.L73:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	48(%rsp), %rdi
	movl	%fs:(%rbx), %r13d
	call	__closedir
	movq	8(%r12), %rdx
	movl	%r13d, %fs:(%rbx)
	leaq	-1(%rdx), %rax
	testq	%rdx, %rdx
	movq	%rax, 8(%r12)
	jne	.L74
	movq	16(%r12), %rax
	subq	$1, %rax
	movq	%rax, 8(%r12)
.L74:
	movq	(%r12), %rdx
	movq	$0, (%rdx,%rax,8)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L89:
	cmpb	$0, 0(%r13)
	je	.L169
.L91:
	movq	%r13, %rdi
	call	__GI_strlen@PLT
	movq	%r13, %rdx
	movq	%rax, %rcx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%rax, %r14
	call	process_entry.isra.3
	testl	%eax, %eax
	movl	%eax, %ebp
	leaq	1(%r13,%r14), %r13
	je	.L89
	cmpl	$3, %eax
	movq	64(%rsp), %rax
	sete	%r13b
.L90:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%rax, %rdi
	movl	%fs:(%rbx), %r14d
	call	free@PLT
	movl	%r14d, %fs:(%rbx)
	jmp	.L87
	.p2align 4,,10
	.p2align 3
.L167:
	movq	48(%rsp), %rdi
	movq	64(%rsp), %rax
	testq	%rdi, %rdi
	movq	%rax, %r13
	je	.L103
	xorl	%ebp, %ebp
.L104:
	movq	__libc_errno@gottpoff(%rip), %rbx
	testq	%rax, %rax
	movl	%fs:(%rbx), %r13d
	jne	.L170
	call	__closedir
	movq	8(%r12), %rdx
	movl	$-1, 56(%rsp)
	movl	%r13d, %fs:(%rbx)
	leaq	-1(%rdx), %rax
	testq	%rdx, %rdx
	movq	%rax, 8(%r12)
	jne	.L86
	movq	16(%r12), %rax
	subq	$1, %rax
	movq	%rax, 8(%r12)
.L86:
	movq	(%r12), %rdx
	cmpl	$3, %ebp
	sete	%r13b
	movq	$0, (%rdx,%rax,8)
.L87:
	movslq	40(%r12), %rax
	testb	$16, 48(%r12)
	movq	24(%r12), %rdx
	leaq	-1(%rdx,%rax), %rax
	je	.L113
	testb	%r13b, %r13b
	jne	.L92
.L113:
	movb	$0, (%rax)
	movl	44(%rsp), %eax
	subl	$1, 44(%r12)
	testl	%ebp, %ebp
	movl	%eax, 40(%r12)
	je	.L102
.L94:
	cmpq	$0, 32(%rsp)
	je	.L56
	movl	48(%r12), %eax
	testb	$4, %al
	je	.L56
	testl	%ebp, %ebp
	je	.L97
	testb	$16, %al
	je	.L56
	leal	1(%rbp), %eax
	andl	$-3, %eax
	je	.L56
.L96:
	movq	32(%rsp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	__dirfd@PLT
	movl	%eax, %edi
	call	__fchdir
	testl	%eax, %eax
	je	.L56
.L101:
	cmpl	$1, 40(%r12)
	je	.L171
	leaq	.LC5(%rip), %rdi
	call	__chdir
	testl	%eax, %eax
	jns	.L56
.L70:
	movl	$-1, %ebp
	jmp	.L56
.L92:
	movb	$0, (%rax)
	movl	44(%rsp), %eax
	subl	$1, 44(%r12)
	movl	%eax, 40(%r12)
.L102:
	movl	48(%r12), %eax
	movl	%eax, %ebp
	andl	$8, %ebp
	je	.L172
	leaq	40(%r12), %rcx
	movq	24(%r12), %rdi
	movl	$5, %edx
	movq	16(%rsp), %rsi
	call	*64(%r12)
	movl	%eax, %ebp
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L81:
	movq	48(%rsp), %rdi
	movq	64(%rsp), %rax
	testq	%rdi, %rdi
	jne	.L104
	cmpl	$3, %ebp
	sete	%r13b
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L172:
	cmpq	$0, 32(%rsp)
	je	.L56
	andl	$4, %eax
	movl	%eax, %ebp
	je	.L56
.L97:
	xorl	%ebp, %ebp
	jmp	.L96
.L169:
	movq	64(%rsp), %rax
	jmp	.L88
.L171:
	leaq	.LC4(%rip), %rdi
	call	__chdir
	testl	%eax, %eax
	jns	.L56
	jmp	.L70
.L166:
	leaq	__PRETTY_FUNCTION__.9320(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$534, %edx
	call	__GI___assert_fail
.L170:
	leaq	__PRETTY_FUNCTION__.9320(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$556, %edx
	call	__GI___assert_fail
.LFE54:
	.size	ftw_dir, .-ftw_dir
	.p2align 4,,15
	.type	ftw_startup, @function
ftw_startup:
.LFB55:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$264, %rsp
	cmpb	$0, (%rdi)
	je	.L234
	testl	%ecx, %ecx
	movl	$1, %r13d
	movl	%r8d, %ebx
	cmovg	%ecx, %r13d
	movq	%rdx, %r12
	movl	%esi, %ebp
	movslq	%r13d, %r13
	movq	%rdi, %r14
	movq	$0, 24(%rsp)
	movq	%r13, 32(%rsp)
	call	__GI_strlen
	addq	%rax, %rax
	movl	$4096, %edx
	cmpq	$4096, %rax
	cmovb	%rdx, %rax
	salq	$3, %r13
	leaq	0(%r13,%rax), %rdi
	movq	%rax, 48(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L206
	movq	%r13, %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__GI_memset@PLT
	addq	%rax, %r13
	movq	%r14, %rsi
	movq	%r13, %rdi
	movq	%r13, 40(%rsp)
	call	__GI_stpcpy@PLT
	leaq	1(%r13), %rcx
	movq	%rax, %r9
	cmpq	%rcx, %rax
	jbe	.L208
	cmpb	$47, -1(%rax)
	je	.L177
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L236:
	cmpb	$47, -1(%r9)
	jne	.L235
.L177:
	subq	$1, %r9
	cmpq	%rcx, %r9
	jne	.L236
.L176:
	cmpq	%rcx, %r13
	movb	$0, (%rcx)
	movl	$0, 60(%rsp)
	jb	.L233
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L237:
	subq	$1, %rcx
	cmpq	%rcx, %r13
	je	.L209
.L233:
	cmpb	$47, -1(%rcx)
	jne	.L237
.L178:
	leaq	ftw_arr(%rip), %rax
	leaq	nftw_arr(%rip), %rdx
	subq	%r13, %rcx
	testl	%ebp, %ebp
	movl	%ecx, 56(%rsp)
	movl	%ebx, 64(%rsp)
	cmovne	%rdx, %rax
	testb	$4, %bl
	movq	%r12, 80(%rsp)
	movq	%rax, 72(%rsp)
	movq	$0, 96(%rsp)
	jne	.L238
	movq	__libc_errno@gottpoff(%rip), %rbp
	xorl	%r14d, %r14d
	movl	$-1, %r15d
.L191:
	leaq	112(%rsp), %rax
	testb	$1, %bl
	movq	%r13, %rdi
	movq	%rax, 8(%rsp)
	movq	%rax, %rsi
	je	.L192
	call	__lstat@PLT
	testl	%eax, %eax
	js	.L193
	movl	136(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L239
.L204:
	cmpl	$40960, %eax
	movq	72(%rsp), %rdx
	leaq	56(%rsp), %rcx
	sete	%al
	movq	8(%rsp), %rsi
	movq	40(%rsp), %rdi
	movzbl	%al, %eax
	salq	$4, %rax
	movl	(%rdx,%rax), %edx
	call	*80(%rsp)
	movl	%eax, %r13d
.L199:
	andl	$16, %ebx
	movl	%fs:0(%rbp), %r12d
	je	.L190
	leal	-2(%r13), %eax
	cmpl	$1, %eax
	movl	$0, %eax
	cmovbe	%eax, %r13d
	cmpl	$-1, %r15d
	jne	.L240
	.p2align 4,,10
	.p2align 3
.L201:
	testq	%r14, %r14
	je	.L183
	movq	%r14, %rdi
	call	__chdir
	movq	%r14, %rdi
	call	free@PLT
	movl	%r12d, %fs:0(%rbp)
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L192:
	call	__stat@PLT
	testl	%eax, %eax
	jns	.L195
	movl	%fs:0(%rbp), %r12d
	cmpl	$2, %r12d
	jne	.L196
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	call	__lstat@PLT
	testl	%eax, %eax
	jne	.L193
	movl	136(%rsp), %eax
	andl	$61440, %eax
	cmpl	$40960, %eax
	jne	.L193
	movq	72(%rsp), %rax
	leaq	56(%rsp), %rcx
	movq	8(%rsp), %rsi
	movq	40(%rsp), %rdi
	movl	24(%rax), %edx
	call	*80(%rsp)
	movl	%eax, %r13d
	jmp	.L199
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%r13, %rcx
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L235:
	movq	%r9, %rcx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L238:
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	movl	$65536, %esi
	call	__GI___open
	cmpl	$-1, %eax
	movl	%eax, %r15d
	je	.L241
	movq	32(%rsp), %rax
	cmpq	$1, %rax
	jbe	.L213
	movq	__libc_errno@gottpoff(%rip), %rbp
	subq	$1, %rax
	xorl	%r14d, %r14d
	movq	%rax, 32(%rsp)
.L184:
	movslq	56(%rsp), %rax
	testl	%eax, %eax
	jle	.L189
	cmpl	$1, %eax
	je	.L242
	movq	40(%rsp), %rdx
	leaq	-1(%rdx,%rax), %rax
	movzbl	(%rax), %r12d
	movb	$0, (%rax)
	movq	40(%rsp), %rdi
	call	__chdir
	movl	%eax, %r13d
	movslq	56(%rsp), %rax
	movq	40(%rsp), %rdx
	movb	%r12b, -1(%rdx,%rax)
.L188:
	testl	%r13d, %r13d
	je	.L189
	movl	%fs:0(%rbp), %r12d
.L190:
	cmpl	$-1, %r15d
	je	.L201
.L240:
	movl	%r15d, %edi
	call	__fchdir
	movl	%r15d, %edi
	call	__GI___close_nocancel
	movl	%r12d, %fs:0(%rbp)
.L183:
	movq	free@GOTPCREL(%rip), %rsi
	movq	96(%rsp), %rdi
	call	__GI___tdestroy
	movq	16(%rsp), %rdi
	call	free@PLT
	movl	%r12d, %fs:0(%rbp)
.L173:
	addq	$264, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L193:
	movl	%fs:0(%rbp), %r12d
.L196:
	movl	$-1, %r13d
	jmp	.L190
	.p2align 4,,10
	.p2align 3
.L195:
	movl	136(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L204
	movq	8(%rsp), %rsi
	movq	112(%rsp), %rax
	leaq	16(%rsp), %r12
	movq	%r12, %rdi
	leaq	8(%rsi), %rdx
	movq	%rax, 88(%rsp)
	call	add_object.isra.1
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L199
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L213:
	xorl	%r14d, %r14d
	movq	__libc_errno@gottpoff(%rip), %rbp
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L208:
	movq	%rax, %rcx
	jmp	.L176
	.p2align 4,,10
	.p2align 3
.L241:
	movq	__libc_errno@gottpoff(%rip), %rbp
	xorl	%r13d, %r13d
	movl	%fs:0(%rbp), %r12d
	cmpl	$13, %r12d
	jne	.L183
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__GI___getcwd
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L184
	movl	%fs:0(%rbp), %r12d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L189:
	testb	$4, 64(%rsp)
	movq	40(%rsp), %r13
	je	.L191
	movslq	56(%rsp), %rax
	addq	%rax, %r13
	leaq	.LC0(%rip), %rax
	cmpb	$0, 0(%r13)
	cmove	%rax, %r13
	jmp	.L191
.L239:
	movq	112(%rsp), %rax
	leaq	16(%rsp), %r12
	movq	%rax, 88(%rsp)
.L202:
	movq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r12, %rdi
	call	ftw_dir
	movl	%eax, %r13d
	jmp	.L199
.L242:
	leaq	.LC4(%rip), %rdi
	call	__chdir
	movl	%eax, %r13d
	jmp	.L188
.L234:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r13d
	movl	$2, %fs:(%rax)
	jmp	.L173
.L206:
	movl	$-1, %r13d
	jmp	.L173
.LFE55:
	.size	ftw_startup, .-ftw_startup
	.p2align 4,,15
	.globl	ftw
	.type	ftw, @function
ftw:
.LFB56:
	movl	%edx, %ecx
	xorl	%r8d, %r8d
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	ftw_startup
.LFE56:
	.size	ftw, .-ftw
	.weak	ftw64
	.set	ftw64,ftw
	.p2align 4,,15
	.globl	__new_nftw
	.type	__new_nftw, @function
__new_nftw:
.LFB57:
	testl	$-32, %ecx
	movl	%ecx, %r8d
	jne	.L248
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movl	$1, %esi
	jmp	ftw_startup
	.p2align 4,,10
	.p2align 3
.L248:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
.LFE57:
	.size	__new_nftw, .-__new_nftw
	.globl	__new_nftw64
	.set	__new_nftw64,__new_nftw
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__old_nftw
	.type	__old_nftw, @function
__old_nftw:
.LFB58:
	andl	$15, %ecx
	movl	%ecx, %r8d
	movl	%edx, %ecx
	movq	%rsi, %rdx
	movl	$1, %esi
	jmp	ftw_startup
.LFE58:
	.size	__old_nftw, .-__old_nftw
	.globl	__old_nftw64
	.set	__old_nftw64,__old_nftw
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9320, @object
	.size	__PRETTY_FUNCTION__.9320, 8
__PRETTY_FUNCTION__.9320:
	.string	"ftw_dir"
	.section	.rodata
	.align 16
	.type	ftw_arr, @object
	.size	ftw_arr, 28
ftw_arr:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	0
	.long	1
	.long	3
	.align 16
	.type	nftw_arr, @object
	.size	nftw_arr, 28
nftw_arr:
	.long	0
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	6
	.hidden	__chdir
	.hidden	__fchdir
	.hidden	__fdopendir
	.hidden	__opendir
	.hidden	__closedir
