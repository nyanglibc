	.text
	.p2align 4,,15
	.globl	__isatty
	.hidden	__isatty
	.type	__isatty, @function
__isatty:
	subq	$72, %rsp
	movq	%rsp, %rsi
	call	__tcgetattr
	testl	%eax, %eax
	sete	%al
	addq	$72, %rsp
	movzbl	%al, %eax
	ret
	.size	__isatty, .-__isatty
	.weak	isatty
	.set	isatty,__isatty
	.hidden	__tcgetattr
