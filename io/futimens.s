	.text
	.p2align 4,,15
	.globl	__futimens
	.type	__futimens, @function
__futimens:
	testl	%edi, %edi
	js	.L5
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	__utimensat64_helper
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__futimens, .-__futimens
	.weak	futimens
	.set	futimens,__futimens
	.hidden	__utimensat64_helper
