.text
.globl __dup
.type __dup,@function
.align 1<<4
__dup:
	movl $32, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __dup,.-__dup
.weak dup
dup = __dup
