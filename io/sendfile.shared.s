.text
.globl sendfile
.type sendfile,@function
.align 1<<4
sendfile:
	movq %rcx, %r10
	movl $40, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size sendfile,.-sendfile
.globl __GI_sendfile
.set __GI_sendfile,sendfile
.weak sendfile64
sendfile64 = sendfile
.globl __GI_sendfile64
.set __GI_sendfile64,sendfile64
