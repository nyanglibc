.text
.globl __dup2
.type __dup2,@function
.align 1<<4
__dup2:
	movl $33, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __dup2,.-__dup2
.globl __GI___dup2
.set __GI___dup2,__dup2
.weak dup2
dup2 = __dup2
.globl __GI_dup2
.set __GI_dup2,dup2
