	.text
	.p2align 4,,15
	.type	fts_sort, @function
fts_sort:
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	cmpl	%edx, 48(%r12)
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	16(%rdi), %rdi
	jl	.L17
.L2:
	testq	%rbx, %rbx
	je	.L5
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$8, %rdx
	movq	%rbx, -8(%rdx)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L6
.L5:
	movq	56(%r12), %rcx
	movslq	%ebp, %rsi
	movl	$8, %edx
	call	qsort
	movq	16(%r12), %r8
	cmpl	$1, %ebp
	movq	(%r8), %rax
	je	.L10
	leal	-2(%rbp), %edx
	leaq	8(%r8), %rcx
	movq	%rax, %rdi
	movq	%rdx, %rbp
	leaq	(%rcx,%rdx,8), %rdx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rcx), %rdi
	addq	$8, %rcx
.L9:
	movq	(%rcx), %rsi
	cmpq	%rdx, %rcx
	movq	%rsi, 16(%rdi)
	jne	.L8
	movq	8(%r8,%rbp,8), %rdx
.L7:
	movq	$0, 16(%rdx)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leal	40(%rdx), %eax
	movl	%eax, 48(%r12)
	cltq
	leaq	0(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L18
	movq	%rax, 16(%r12)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rax, %rdx
	jmp	.L7
.L18:
	movq	16(%r12), %rdi
	call	free@PLT
	movq	$0, 16(%r12)
	movl	$0, 48(%r12)
	movq	%rbx, %rax
	jmp	.L1
	.size	fts_sort, .-fts_sort
	.p2align 4,,15
	.type	fts_palloc.isra.2, @function
fts_palloc.isra.2:
	pushq	%rbx
	movl	(%rsi), %eax
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	leal	256(%rax,%rdx), %eax
	cmpl	$65534, %eax
	movl	%eax, (%rsi)
	ja	.L24
	movslq	%eax, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L25
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$0, (%rbx)
	movl	$36, %fs:(%rax)
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rbx), %rdi
	call	free@PLT
	movq	$0, (%rbx)
	movl	$1, %eax
	popq	%rbx
	ret
	.size	fts_palloc.isra.2, .-fts_palloc.isra.2
	.p2align 4,,15
	.type	fts_stat.isra.4, @function
fts_stat.isra.4:
	pushq	%r13
	pushq	%r12
	movl	%edi, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$152, %rsp
	testb	$8, %al
	movq	%rsp, %rbx
	jne	.L27
	movq	104(%rsi), %rbx
.L27:
	andl	$2, %eax
	movq	40(%rbp), %rdi
	movq	%rbx, %rsi
	orl	%edx, %eax
	je	.L28
	call	__stat@PLT
	testl	%eax, %eax
	jne	.L52
.L29:
	movl	24(%rbx), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L53
	cmpl	$40960, %eax
	je	.L41
	cmpl	$32768, %eax
	sete	%al
	movzbl	%al, %eax
	leal	3(%rax,%rax,4), %eax
.L26:
	addq	$152, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	call	__lstat@PLT
	testl	%eax, %eax
	je	.L29
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 56(%rbp)
.L32:
	leaq	8(%rbx), %rdi
	movq	$0, (%rbx)
	movq	$0, 136(%rbx)
	xorl	%eax, %eax
	andq	$-8, %rdi
	subq	%rdi, %rbx
	leal	144(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	addq	$152, %rsp
	movl	$10, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	cmpb	$46, 112(%rbp)
	movq	(%rbx), %rcx
	movq	8(%rbx), %rdx
	movq	16(%rbx), %rax
	movq	%rcx, 80(%rbp)
	movq	%rdx, 72(%rbp)
	movq	%rax, 88(%rbp)
	je	.L54
.L34:
	movq	8(%rbp), %rax
	cmpw	$0, 96(%rax)
	jns	.L36
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L35:
	movq	8(%rax), %rax
	cmpw	$0, 96(%rax)
	js	.L40
.L36:
	cmpq	72(%rax), %rdx
	jne	.L35
	cmpq	80(%rax), %rcx
	jne	.L35
	movq	%rax, 0(%rbp)
	movl	$2, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L52:
	movq	__libc_errno@gottpoff(%rip), %r12
	movq	40(%rbp), %rdi
	movq	%rbx, %rsi
	movl	%fs:(%r12), %r13d
	call	__lstat@PLT
	testl	%eax, %eax
	jne	.L30
	movl	$0, %fs:(%r12)
	movl	$13, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L54:
	cmpb	$0, 113(%rbp)
	movl	$5, %eax
	je	.L26
	movl	112(%rbp), %esi
	andl	$16776960, %esi
	cmpl	$11776, %esi
	jne	.L34
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$152, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movl	$12, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L30:
	movl	%r13d, 56(%rbp)
	jmp	.L32
	.size	fts_stat.isra.4, .-fts_stat.isra.4
	.p2align 4,,15
	.type	fts_alloc.isra.5, @function
fts_alloc.isra.5:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rcx, %rbp
	subq	$8, %rsp
	andl	$8, %esi
	jne	.L56
	leaq	279(%rcx), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L60
	leaq	112(%rax), %rcx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	leaq	17(%rax,%rbp), %rax
	movb	$0, 112(%rbx,%rbp)
	andq	$-16, %rax
	movq	%rax, 104(%rbx)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L56:
	leaq	120(%rcx), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L60
	leaq	112(%rbx), %rdi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movb	$0, 112(%rbx,%rbp)
.L59:
	movw	%bp, 66(%rbx)
	movq	%r13, 48(%rbx)
	movl	$0, 56(%rbx)
	movl	$196608, 100(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 32(%rbx)
.L55:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%ebx, %ebx
	jmp	.L55
	.size	fts_alloc.isra.5, .-fts_alloc.isra.5
	.p2align 4,,15
	.type	fts_safe_changedir.isra.7.part.8, @function
fts_safe_changedir.isra.7.part.8:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edx, %ebx
	subq	$144, %rsp
	testl	%edx, %edx
	js	.L82
	movq	%rsp, %rsi
	movl	%edx, %edi
	movl	%ebx, %r14d
	call	__fstat64
	testl	%eax, %eax
	je	.L77
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	$-1, %r12d
	movl	%fs:0(%rbp), %r13d
.L74:
	movl	%r13d, %fs:0(%rbp)
.L67:
	addq	$144, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	call	__open
	testl	%eax, %eax
	movl	%eax, %r14d
	js	.L83
	movq	%rsp, %rsi
	movl	%eax, %edi
	call	__fstat64
	testl	%eax, %eax
	jne	.L84
.L77:
	movq	(%rsp), %rax
	cmpq	%rax, 0(%rbp)
	je	.L85
.L71:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	$2, %r13d
	movl	$-1, %r12d
	movl	$2, %fs:0(%rbp)
.L73:
	testl	%ebx, %ebx
	jns	.L74
.L75:
	movl	%r14d, %edi
	call	__close
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	movq	8(%rsp), %rax
	cmpq	%rax, (%r12)
	jne	.L71
	movl	%r14d, %edi
	call	__fchdir
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%eax, %r12d
	movl	%fs:0(%rbp), %r13d
	jmp	.L73
.L83:
	movl	$-1, %r12d
	jmp	.L67
.L84:
	movq	__libc_errno@gottpoff(%rip), %rbp
	orl	$-1, %r12d
	movl	%fs:0(%rbp), %r13d
	jmp	.L75
	.size	fts_safe_changedir.isra.7.part.8, .-fts_safe_changedir.isra.7.part.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	".."
	.text
	.p2align 4,,15
	.type	fts_build, @function
fts_build:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	(%rdi), %rax
	movl	%esi, 68(%rsp)
	movq	40(%rax), %rdi
	movq	%rax, 48(%rsp)
	call	__opendir
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L231
	movl	68(%rsp), %esi
	cmpl	$2, %esi
	je	.L147
	movl	64(%rbx), %eax
	cmpl	$3, %esi
	sete	%bpl
	movl	%eax, %edx
	andl	$24, %edx
	cmpl	$24, %edx
	je	.L232
	movl	$0, 64(%rsp)
	movl	$-1, 24(%rsp)
	movl	$1, %r12d
.L91:
	movl	64(%rbx), %eax
	andl	$4, %eax
	movl	%eax, 76(%rsp)
	je	.L233
.L151:
	movl	$1, 76(%rsp)
.L90:
	movq	48(%rsp), %rcx
	movzwl	64(%rcx), %edx
	movq	48(%rcx), %rcx
	cmpb	$47, -1(%rcx,%rdx)
	jne	.L95
	subl	$1, %edx
.L95:
	testb	$4, 64(%rbx)
	movq	$0, 56(%rsp)
	je	.L96
	movslq	%edx, %rax
	addq	32(%rbx), %rax
	leaq	1(%rax), %rcx
	movb	$47, (%rax)
	movq	%rcx, 56(%rsp)
.L96:
	movl	44(%rbx), %r15d
	leal	1(%rdx), %eax
	movl	%eax, 28(%rsp)
	subl	%eax, %r15d
	movq	48(%rsp), %rax
	cmpq	$0, 16(%rsp)
	movslq	%r15d, %r15
	movzwl	96(%rax), %eax
	movw	%ax, 46(%rsp)
	je	.L234
	movslq	28(%rsp), %rax
	movl	$0, 72(%rsp)
	xorl	%r13d, %r13d
	movq	$0, 8(%rsp)
	movl	$0, 40(%rsp)
	movq	%rax, 32(%rsp)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L240:
	movl	64(%rsp), %eax
	testl	%eax, %eax
	je	.L111
	testb	$-5, 18(%r12)
	jne	.L110
.L111:
	movl	64(%rbx), %edi
	leaq	112(%r14), %rsi
	testb	$4, %dil
	jne	.L235
	movq	%rsi, 40(%r14)
.L116:
	xorl	%edx, %edx
	movq	%r14, %rsi
	call	fts_stat.isra.4
	movl	24(%rsp), %ebp
	movw	%ax, 98(%r14)
	testl	%ebp, %ebp
	jle	.L114
	leal	-1(%rax), %edx
	cmpw	$1, %dx
	jbe	.L160
	cmpw	$5, %ax
	je	.L160
.L114:
	testq	%r13, %r13
	movq	$0, 16(%r14)
	je	.L155
.L241:
	movq	8(%rsp), %rax
	movq	%r14, 16(%rax)
.L118:
	addl	$1, 40(%rsp)
	movq	%r14, 8(%rsp)
.L97:
	movq	16(%rsp), %rdi
	call	__readdir
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L236
	movl	64(%rbx), %r14d
	testb	$32, %r14b
	jne	.L99
	cmpb	$46, 19(%r12)
	je	.L237
.L99:
	leaq	19(%r12), %rbp
	movq	%rbp, %rdi
	call	strlen
	movq	32(%rbx), %rdi
	movl	%r14d, %esi
	movq	%rax, %rcx
	movq	%rbp, %rdx
	call	fts_alloc.isra.5
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L101
	movq	%rbp, %rdi
	call	strlen
	cmpq	%r15, %rax
	movq	%rax, %rdx
	jnb	.L238
	addq	32(%rsp), %rdx
	cmpq	$65534, %rdx
	ja	.L239
.L107:
	movzwl	46(%rsp), %eax
	movq	%rbp, %rdi
	addl	$1, %eax
	movw	%ax, 96(%r14)
	movq	(%rbx), %rax
	movq	%rax, 8(%r14)
	call	strlen
	addw	28(%rsp), %ax
	movw	%ax, 64(%r14)
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jne	.L240
.L110:
	testb	$4, 64(%rbx)
	leaq	112(%r14), %rax
	je	.L113
	movq	48(%r14), %rax
.L113:
	movl	$11, %r12d
	testq	%r13, %r13
	movq	%rax, 40(%r14)
	movw	%r12w, 98(%r14)
	movq	$0, 16(%r14)
	jne	.L241
.L155:
	movq	%r14, %r13
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L147:
	movl	$0, 64(%rsp)
	movl	$0, 24(%rsp)
	movl	$0, 76(%rsp)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L238:
	movq	32(%rsp), %rcx
	leaq	44(%rbx), %rsi
	leaq	32(%rbx), %rdi
	movq	32(%rbx), %r15
	leaq	1(%rax,%rcx), %rdx
	call	fts_palloc.isra.2
	testl	%eax, %eax
	jne	.L101
	movq	32(%rbx), %rax
	cmpq	%r15, %rax
	je	.L106
	testb	$4, 64(%rbx)
	movl	$1, 72(%rsp)
	je	.L106
	addq	32(%rsp), %rax
	movq	%rax, 56(%rsp)
.L106:
	movq	%rbp, %rdi
	movl	44(%rbx), %r15d
	subl	28(%rsp), %r15d
	call	strlen
	movq	%rax, %rdx
	addq	32(%rsp), %rdx
	movslq	%r15d, %r15
	cmpq	$65534, %rdx
	jbe	.L107
.L239:
	movq	%r14, %rdi
	call	free@PLT
	testq	%r13, %r13
	je	.L108
	movq	%r13, %rdi
	.p2align 4,,10
	.p2align 3
.L109:
	movq	16(%rdi), %rbp
	call	free@PLT
	testq	%rbp, %rbp
	movq	%rbp, %rdi
	jne	.L109
.L108:
	movq	16(%rsp), %rdi
	xorl	%r13d, %r13d
	call	__closedir
	movq	48(%rsp), %rax
	movl	$7, %edx
	movw	%dx, 98(%rax)
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$512, 64(%rbx)
	movl	$36, %fs:(%rax)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L237:
	cmpb	$0, 20(%r12)
	je	.L97
	cmpw	$46, 20(%r12)
	jne	.L99
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L235:
	movq	48(%r14), %rax
	movq	56(%rsp), %rdi
	movq	%rax, 40(%r14)
	movzwl	66(%r14), %eax
	leaq	1(%rax), %rdx
	call	memmove
	movl	64(%rbx), %edi
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L160:
	subl	$1, 24(%rsp)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L236:
	movq	16(%rsp), %rdi
	call	__closedir
	movl	72(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L242
.L141:
	testb	$4, 64(%rbx)
	je	.L128
	movl	28(%rsp), %eax
	cmpl	%eax, 44(%rbx)
	je	.L161
	movl	40(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L129
.L161:
	subq	$1, 56(%rsp)
.L129:
	movq	56(%rsp), %rax
	movb	$0, (%rax)
.L128:
	movl	76(%rsp), %r10d
	testl	%r10d, %r10d
	je	.L131
	cmpl	$1, 68(%rsp)
	je	.L143
	movl	40(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L143
.L132:
	cmpq	$0, 56(%rbx)
	je	.L86
	movl	40(%rsp), %eax
	cmpl	$1, %eax
	je	.L86
	addq	$88, %rsp
	movq	%r13, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	movl	%eax, %edx
	jmp	fts_sort
	.p2align 4,,10
	.p2align 3
.L131:
	movl	40(%rsp), %edi
	testl	%edi, %edi
	jne	.L132
.L145:
	cmpl	$3, 68(%rsp)
	je	.L146
.L138:
	testq	%r13, %r13
	movq	%r13, %rdi
	je	.L230
	.p2align 4,,10
	.p2align 3
.L139:
	movq	16(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L139
.L230:
	xorl	%r13d, %r13d
.L86:
	addq	$88, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	cmpl	$3, 68(%rsp)
	jne	.L230
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	48(%rsp), %rsi
	movl	$4, %edi
	movl	%fs:(%rax), %eax
	movw	%di, 98(%rsi)
	movl	%eax, 56(%rsi)
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L101:
	movq	__libc_errno@gottpoff(%rip), %r12
	movq	%r14, %rdi
	movl	%fs:(%r12), %r15d
	call	free@PLT
	testq	%r13, %r13
	je	.L104
	movq	%r13, %rdi
	.p2align 4,,10
	.p2align 3
.L105:
	movq	16(%rdi), %rbp
	call	free@PLT
	testq	%rbp, %rbp
	movq	%rbp, %rdi
	jne	.L105
.L104:
	movq	16(%rsp), %rdi
	xorl	%r13d, %r13d
	call	__closedir
	movq	48(%rsp), %rax
	movl	$7, %ecx
	movl	%r15d, %fs:(%r12)
	movw	%cx, 98(%rax)
	orl	$512, 64(%rbx)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L232:
	movq	48(%rsp), %rsi
	testb	$32, %al
	movl	$1, 64(%rsp)
	movq	88(%rsi), %rdx
	leal	-2(%rdx), %ecx
	cmovne	%edx, %ecx
	testl	%ecx, %ecx
	movl	%ecx, 24(%rsp)
	setne	%r12b
	jne	.L91
	testb	%bpl, %bpl
	jne	.L91
	movl	$0, 24(%rsp)
	movl	$0, 76(%rsp)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L242:
	movq	8(%rbx), %rax
	movq	32(%rbx), %rcx
	testq	%rax, %rax
	je	.L121
	.p2align 4,,10
	.p2align 3
.L123:
	movq	40(%rax), %rdx
	leaq	112(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L122
	subq	48(%rax), %rdx
	addq	%rcx, %rdx
	movq	%rdx, 40(%rax)
.L122:
	movq	%rcx, 48(%rax)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L123
.L121:
	movq	%r13, %rax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L126:
	movq	%rdx, %rax
.L124:
	cmpw	$0, 96(%rax)
	js	.L141
	movq	40(%rax), %rdx
	leaq	112(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L125
	subq	48(%rax), %rdx
	addq	%rcx, %rdx
	movq	%rdx, 40(%rax)
.L125:
	movq	16(%rax), %rdx
	movq	%rcx, 48(%rax)
	testq	%rdx, %rdx
	jne	.L126
	movq	8(%rax), %rdx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L233:
	movq	16(%rsp), %rax
	movq	48(%rsp), %r15
	xorl	%ecx, %ecx
	movl	(%rax), %edx
	leaq	80(%r15), %rsi
	leaq	72(%r15), %rdi
	call	fts_safe_changedir.isra.7.part.8
	testl	%eax, %eax
	je	.L151
	testb	%bpl, %bpl
	je	.L94
	testb	%r12b, %r12b
	je	.L94
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 56(%r15)
.L94:
	movq	48(%rsp), %rax
	movq	16(%rsp), %rdi
	orw	$1, 100(%rax)
	call	__closedir
	movq	$0, 16(%rsp)
	jmp	.L90
.L234:
	movl	64(%rbx), %eax
	andl	$4, %eax
	movl	%eax, 40(%rsp)
	je	.L142
	movl	76(%rsp), %edx
	movq	56(%rsp), %rax
	testl	%edx, %edx
	movb	$0, -1(%rax)
	je	.L243
	xorl	%r13d, %r13d
	movl	$0, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L143:
	movl	64(%rbx), %eax
	movq	48(%rsp), %rcx
	andl	$4, %eax
	cmpw	$0, 96(%rcx)
	je	.L244
	testl	%eax, %eax
	jne	.L131
	movq	48(%rsp), %rax
	leaq	.LC0(%rip), %rcx
	movl	$-1, %edx
	movq	8(%rax), %rax
	leaq	80(%rax), %rsi
	leaq	72(%rax), %rdi
	call	fts_safe_changedir.isra.7.part.8
	testl	%eax, %eax
	setne	%al
.L135:
	testb	%al, %al
	je	.L131
	movq	48(%rsp), %rax
	movl	$7, %r8d
	movw	%r8w, 98(%rax)
	orl	$512, 64(%rbx)
	testq	%r13, %r13
	je	.L230
	movq	%r13, %rdi
	.p2align 4,,10
	.p2align 3
.L137:
	movq	16(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L137
	jmp	.L230
.L243:
	cmpl	$3, 68(%rsp)
	jne	.L230
	xorl	%r13d, %r13d
.L146:
	movq	48(%rsp), %rax
	movl	$6, %esi
	movw	%si, 98(%rax)
	jmp	.L138
.L244:
	testl	%eax, %eax
	jne	.L131
	movl	40(%rbx), %edi
	call	__fchdir
	testl	%eax, %eax
	setne	%al
	jmp	.L135
.L142:
	movl	76(%rsp), %eax
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L145
	jmp	.L143
	.size	fts_build, .-fts_build
	.section	.rodata.str1.1
.LC1:
	.string	""
.LC2:
	.string	"."
	.text
	.p2align 4,,15
	.globl	fts_open
	.type	fts_open, @function
fts_open:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	testl	$-256, %esi
	jne	.L300
	movq	%rdi, %r12
	movl	%esi, %ebp
	movl	$72, %edi
	movl	$1, %esi
	movq	%rdx, %r14
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L245
	movq	(%r12), %rdi
	movq	%r14, 56(%rax)
	movl	%ebp, %eax
	orl	$4, %eax
	testb	$2, %bpl
	cmovne	%eax, %ebp
	testq	%rdi, %rdi
	movl	%ebp, 64(%rbx)
	je	.L267
	movq	%r12, %r13
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L251:
	call	strlen
	cmpq	%rax, %rbp
	cmovb	%rax, %rbp
	addq	$8, %r13
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L251
	leaq	1(%rbp), %rdx
	movl	$4096, %eax
	cmpq	$4096, %rdx
	cmovb	%rax, %rdx
.L250:
	leaq	44(%rbx), %rsi
	leaq	32(%rbx), %rdi
	call	fts_palloc.isra.2
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L252
	cmpq	$0, (%r12)
	je	.L268
	movl	64(%rbx), %esi
	movq	32(%rbx), %rdi
	leaq	.LC1(%rip), %rdx
	xorl	%ecx, %ecx
	call	fts_alloc.isra.5
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L254
	movq	(%r12), %rdx
	movl	$-1, %esi
	movw	%si, 96(%rax)
	testq	%rdx, %rdx
	je	.L253
	xorl	%ebp, %ebp
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L303:
	movq	%rbp, 16(%r15)
	movq	%r15, %rbp
.L260:
	addq	$8, %r12
	movq	(%r12), %rdx
	addl	$1, %r13d
	testq	%rdx, %rdx
	je	.L301
.L261:
	movq	%rdx, %rdi
	movq	%rdx, 8(%rsp)
	call	strlen
	testq	%rax, %rax
	movq	8(%rsp), %rdx
	je	.L302
	movl	64(%rbx), %esi
	movq	32(%rbx), %rdi
	movq	%rax, %rcx
	call	fts_alloc.isra.5
	xorl	%ecx, %ecx
	movq	%rax, %r15
	movl	64(%rbx), %edi
	movw	%cx, 96(%rax)
	movq	16(%rsp), %rax
	movq	%r15, %rsi
	movl	%edi, %edx
	movq	%rax, 8(%r15)
	leaq	112(%r15), %rax
	andl	$1, %edx
	movq	%rax, 40(%r15)
	call	fts_stat.isra.4
	movl	$1, %ecx
	cmpw	$5, %ax
	cmove	%ecx, %eax
	testq	%r14, %r14
	movw	%ax, 98(%r15)
	jne	.L303
	testq	%rbp, %rbp
	movq	$0, 16(%r15)
	je	.L269
	movq	24(%rsp), %rax
	movq	%r15, 24(%rsp)
	movq	%r15, 16(%rax)
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L268:
	movq	$0, 16(%rsp)
.L253:
	movl	64(%rbx), %esi
	movq	32(%rbx), %rdi
	leaq	.LC1(%rip), %rdx
	xorl	%ecx, %ecx
	call	fts_alloc.isra.5
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L265
	xorl	%ebp, %ebp
.L266:
	testb	$4, 64(%rbx)
	movl	$9, %edx
	movq	%rbp, 16(%rax)
	movw	%dx, 98(%rax)
	jne	.L245
	leaq	.LC2(%rip), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__open
	testl	%eax, %eax
	movl	%eax, 40(%rbx)
	jns	.L245
	orl	$4, 64(%rbx)
.L245:
	addq	$40, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L302:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L263:
	movq	16(%rbp), %r12
	movq	%rbp, %rdi
	call	free@PLT
	movq	%r12, %rbp
.L299:
	testq	%rbp, %rbp
	jne	.L263
.L265:
	movq	16(%rsp), %rdi
	call	free@PLT
.L254:
	movq	32(%rbx), %rdi
	call	free@PLT
.L252:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L269:
	movq	%r15, 24(%rsp)
	movq	%r15, %rbp
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L301:
	testq	%r14, %r14
	je	.L262
	cmpl	$1, %r13d
	jle	.L262
	movq	%rbp, %rsi
	movl	%r13d, %edx
	movq	%rbx, %rdi
	call	fts_sort
	movl	64(%rbx), %esi
	movq	32(%rbx), %rdi
	leaq	.LC1(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rax, %rbp
	call	fts_alloc.isra.5
	testq	%rax, %rax
	movq	%rax, (%rbx)
	jne	.L266
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L300:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$22, %fs:(%rax)
	jmp	.L245
	.p2align 4,,10
	.p2align 3
.L267:
	movl	$4096, %edx
	jmp	.L250
	.p2align 4,,10
	.p2align 3
.L262:
	movl	64(%rbx), %esi
	movq	32(%rbx), %rdi
	leaq	.LC1(%rip), %rdx
	xorl	%ecx, %ecx
	call	fts_alloc.isra.5
	testq	%rax, %rax
	movq	%rax, (%rbx)
	jne	.L266
	jmp	.L263
	.size	fts_open, .-fts_open
	.weak	fts64_open
	.set	fts64_open,fts_open
	.p2align 4,,15
	.globl	fts_close
	.type	fts_close, @function
fts_close:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L305
	cmpw	$0, 96(%rdi)
	jns	.L308
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L307:
	call	free@PLT
	cmpw	$0, 96(%rbx)
	movq	%rbx, %rdi
	js	.L306
.L308:
	movq	16(%rdi), %rbx
	testq	%rbx, %rbx
	jne	.L307
	movq	8(%rdi), %rbx
	call	free@PLT
	cmpw	$0, 96(%rbx)
	movq	%rbx, %rdi
	jns	.L308
.L306:
	movq	%rbx, %rdi
	call	free@PLT
.L305:
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L309
	.p2align 4,,10
	.p2align 3
.L310:
	movq	16(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L310
.L309:
	movq	16(%rbp), %rdi
	call	free@PLT
	movq	32(%rbp), %rdi
	call	free@PLT
	testb	$4, 64(%rbp)
	jne	.L313
	movl	40(%rbp), %edi
	call	__fchdir
	testl	%eax, %eax
	je	.L328
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	40(%rbp), %edi
	movl	%fs:(%rbx), %r12d
	call	__close
	testl	%r12d, %r12d
	jne	.L329
.L313:
	movq	%rbp, %rdi
	call	free@PLT
	xorl	%eax, %eax
.L304:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L328:
	movl	40(%rbp), %edi
	call	__close
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L315:
	movq	%rdi, %rbx
	movq	%rbx, %rdi
	call	free@PLT
	jmp	.L305
.L329:
	movq	%rbp, %rdi
	call	free@PLT
	movl	%r12d, %fs:(%rbx)
	movl	$-1, %eax
	jmp	.L304
	.size	fts_close, .-fts_close
	.weak	fts64_close
	.set	fts64_close,fts_close
	.p2align 4,,15
	.globl	fts_read
	.type	fts_read, @function
fts_read:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	(%rdi), %rbp
	testq	%rbp, %rbp
	je	.L412
	movl	64(%rdi), %ecx
	testb	$2, %ch
	jne	.L412
	movzwl	102(%rbp), %eax
	movl	$3, %r9d
	movq	%rdi, %r12
	movw	%r9w, 102(%rbp)
	cmpl	$1, %eax
	je	.L414
	cmpl	$2, %eax
	movzwl	98(%rbp), %edx
	je	.L415
	cmpw	$1, %dx
	jne	.L356
	jmp	.L416
	.p2align 4,,10
	.p2align 3
.L419:
	movq	%rbx, (%r12)
	movq	%rbp, %rdi
	call	free@PLT
	cmpw	$0, 96(%rbx)
	je	.L417
	movzwl	102(%rbx), %eax
	movq	%rbx, %rbp
	cmpw	$4, %ax
	jne	.L418
.L338:
.L356:
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L419
	movq	8(%rbp), %rbx
	movq	%rbp, %rdi
	movq	%rbx, (%r12)
	call	free@PLT
	cmpw	$-1, 96(%rbx)
	je	.L420
	movzwl	64(%rbx), %eax
	movq	32(%r12), %rdx
	movb	$0, (%rdx,%rax)
	cmpw	$0, 96(%rbx)
	jne	.L361
	testb	$4, 64(%r12)
	je	.L421
.L366:
	movl	56(%rbx), %edx
	xorl	%eax, %eax
	movq	%rbx, %rbp
	testl	%edx, %edx
	setne	%al
	addl	$6, %eax
	movw	%ax, 98(%rbx)
.L330:
	popq	%rbx
	movq	%rbp, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L415:
	leal	-12(%rdx), %eax
	cmpw	$1, %ax
	jbe	.L422
	cmpw	$1, %dx
	jne	.L356
.L368:
	testb	$64, %cl
	je	.L340
	movq	24(%r12), %rax
	cmpq	%rax, 80(%rbp)
	je	.L340
.L339:
	testb	$2, 100(%rbp)
	jne	.L423
.L341:
	movq	8(%r12), %rdi
	testq	%rdi, %rdi
	je	.L342
	.p2align 4,,10
	.p2align 3
.L343:
	movq	16(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L343
	movq	$0, 8(%r12)
.L342:
	movl	$6, %edi
	movq	%rbp, %rax
	movw	%di, 98(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L422:
	movl	$1, %edx
	movq	%rbp, %rsi
	movl	%ecx, %edi
	call	fts_stat.isra.4
	cmpw	$1, %ax
	movw	%ax, 98(%rbp)
	jne	.L330
	testb	$4, 64(%r12)
	jne	.L330
	leaq	.LC2(%rip), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__open
	testl	%eax, %eax
	movl	%eax, 60(%rbp)
	js	.L424
	orw	$2, 100(%rbp)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L340:
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.L344
	testb	$1, %ch
	jne	.L425
	andl	$4, %ecx
	je	.L426
.L348:
	movq	$0, 8(%r12)
	movq	%rbx, (%r12)
.L350:
	movq	8(%rbx), %rcx
	movq	32(%r12), %rdi
	movzwl	64(%rcx), %edx
	movq	48(%rcx), %rcx
	cmpb	$47, -1(%rcx,%rdx)
	movq	%rdx, %rax
	jne	.L359
	subl	$1, %eax
	movslq	%eax, %rdx
.L359:
	addq	%rdx, %rdi
	leaq	112(%rbx), %rsi
	movq	%rbx, %rbp
	movb	$47, (%rdi)
	movzwl	66(%rbx), %eax
	addq	$1, %rdi
	leaq	1(%rax), %rdx
	call	memmove
	popq	%rbx
	movq	%rbp, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	cmpl	$4, %eax
	jne	.L368
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%rbp, %rsi
	xorl	%edx, %edx
	movl	%ecx, %edi
	call	fts_stat.isra.4
	movw	%ax, 98(%rbp)
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L417:
	testb	$4, 64(%r12)
	jne	.L353
	movl	40(%r12), %edi
	call	__fchdir
	testl	%eax, %eax
	jne	.L413
.L353:
	movzwl	66(%rbx), %eax
	leaq	112(%rbx), %r13
	movq	32(%r12), %rdi
	movq	%r13, %rsi
	leaq	1(%rax), %rdx
	movw	%ax, 64(%rbx)
	call	memmove
	movl	$47, %esi
	movq	%r13, %rdi
	call	strrchr
	testq	%rax, %rax
	je	.L354
	cmpq	%rax, %r13
	je	.L427
.L355:
	leaq	1(%rax), %rbp
	movq	%rbp, %rdi
	call	strlen
	leal	1(%rax), %edx
	movq	%rax, %r14
	movq	%rbp, %rsi
	movq	%r13, %rdi
	movslq	%edx, %rdx
	call	memmove
	movw	%r14w, 66(%rbx)
.L354:
	movq	32(%r12), %rax
	movq	%rbx, %rbp
	movq	%rax, 48(%rbx)
	movq	%rax, 40(%rbx)
	movq	80(%rbx), %rax
	movq	%rax, 24(%r12)
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L418:
	cmpw	$2, %ax
	jne	.L350
	movl	64(%r12), %edi
	movl	$1, %edx
	movq	%rbx, %rsi
	call	fts_stat.isra.4
	cmpw	$1, %ax
	movw	%ax, 98(%rbx)
	je	.L428
.L357:
	movl	$3, %ecx
	movw	%cx, 102(%rbx)
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L361:
	movzwl	100(%rbx), %eax
	testb	$2, %al
	jne	.L429
	testb	$1, %al
	jne	.L366
	testb	$4, 64(%r12)
	jne	.L366
	movq	8(%rbx), %rax
	leaq	.LC0(%rip), %rcx
	movl	$-1, %edx
	leaq	80(%rax), %rsi
	leaq	72(%rax), %rdi
	call	fts_safe_changedir.isra.7.part.8
	testl	%eax, %eax
	je	.L366
.L413:
	orl	$512, 64(%r12)
.L412:
	xorl	%ebp, %ebp
	popq	%rbx
	movq	%rbp, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L425:
	andb	$-2, %ch
	movl	%ecx, 64(%r12)
	.p2align 4,,10
	.p2align 3
.L346:
	movq	16(%rbx), %r13
	movq	%rbx, %rdi
	call	free@PLT
	testq	%r13, %r13
	movq	%r13, %rbx
	jne	.L346
	movq	$0, 8(%r12)
.L344:
	movl	$3, %esi
	movq	%r12, %rdi
	call	fts_build
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, 8(%r12)
	jne	.L348
	testb	$2, 65(%r12)
	je	.L330
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L429:
	testb	$4, 64(%r12)
	movl	60(%rbx), %edi
	jne	.L364
	call	__fchdir
	testl	%eax, %eax
	jne	.L365
	movl	60(%rbx), %edi
.L364:
	call	__close
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L427:
	cmpb	$0, 113(%rbx)
	jne	.L355
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L423:
	movl	60(%rbp), %edi
	call	__close
	jmp	.L341
.L426:
	movq	40(%rbp), %rcx
	leaq	80(%rbp), %rsi
	leaq	72(%rbp), %rdi
	movl	$-1, %edx
	call	fts_safe_changedir.isra.7.part.8
	testl	%eax, %eax
	je	.L347
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	8(%r12), %rbx
	orw	$1, 100(%rbp)
	movl	%fs:(%rax), %eax
	testq	%rbx, %rbx
	movl	%eax, 56(%rbp)
	je	.L348
	movq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L349:
	movq	8(%rax), %rdx
	movq	40(%rdx), %rdx
	movq	%rdx, 40(%rax)
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L349
	jmp	.L348
.L421:
	movl	40(%r12), %edi
	call	__fchdir
	testl	%eax, %eax
	je	.L366
	jmp	.L413
.L347:
	movq	8(%r12), %rbx
	jmp	.L348
.L428:
	testb	$4, 64(%r12)
	jne	.L357
	leaq	.LC2(%rip), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__open
	testl	%eax, %eax
	movl	%eax, 60(%rbx)
	js	.L430
	orw	$2, 100(%rbx)
	jmp	.L357
.L420:
	movq	%rbx, %rdi
	xorl	%ebp, %ebp
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	movq	$0, (%r12)
	jmp	.L330
.L424:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %r8d
	movw	%r8w, 98(%rbp)
	movl	%fs:(%rax), %eax
	movl	%eax, 56(%rbp)
	jmp	.L330
.L430:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %esi
	movw	%si, 98(%rbx)
	movl	%fs:(%rax), %eax
	movl	%eax, 56(%rbx)
	jmp	.L357
.L365:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	60(%rbx), %edi
	movl	%fs:0(%rbp), %r13d
	call	__close
	orl	$512, 64(%r12)
	movl	%r13d, %fs:0(%rbp)
	xorl	%ebp, %ebp
	jmp	.L330
	.size	fts_read, .-fts_read
	.weak	fts64_read
	.set	fts64_read,fts_read
	.p2align 4,,15
	.globl	fts_set
	.type	fts_set, @function
fts_set:
	cmpl	$4, %edx
	ja	.L434
	movw	%dx, 102(%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L434:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$1, %eax
	ret
	.size	fts_set, .-fts_set
	.weak	fts64_set
	.set	fts64_set,fts_set
	.p2align 4,,15
	.globl	fts_children
	.type	fts_children, @function
fts_children:
	testl	$-257, %esi
	jne	.L454
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testb	$2, 65(%rdi)
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	(%rdi), %r12
	movl	$0, %fs:(%rax)
	jne	.L440
	movzwl	98(%r12), %eax
	cmpw	$9, %ax
	je	.L455
	cmpw	$1, %ax
	jne	.L440
	movq	%rdi, %rbp
	movq	8(%rdi), %rdi
	movl	%esi, %r13d
	testq	%rdi, %rdi
	je	.L441
	.p2align 4,,10
	.p2align 3
.L442:
	movq	16(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L442
.L441:
	cmpl	$256, %r13d
	movl	$1, %ebx
	jne	.L443
	orl	$256, 64(%rbp)
	movl	$2, %ebx
.L443:
	cmpw	$0, 96(%r12)
	jne	.L444
	movq	40(%r12), %rax
	cmpb	$47, (%rax)
	je	.L444
	testb	$4, 64(%rbp)
	je	.L445
.L444:
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	fts_build
	movq	%rax, 8(%rbp)
.L435:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L455:
	movq	16(%r12), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L440:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	leaq	.LC2(%rip), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__open
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L440
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	fts_build
	movl	%r12d, %edi
	movq	%rax, 8(%rbp)
	call	__fchdir
	testl	%eax, %eax
	jne	.L440
	movl	%r12d, %edi
	call	__close
	movq	8(%rbp), %rax
	jmp	.L435
	.p2align 4,,10
	.p2align 3
.L454:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	fts_children, .-fts_children
	.weak	fts64_children
	.set	fts64_children,fts_children
	.hidden	strrchr
	.hidden	memmove
	.hidden	__closedir
	.hidden	strlen
	.hidden	__readdir
	.hidden	__opendir
	.hidden	__fchdir
	.hidden	__close
	.hidden	__open
	.hidden	__fstat64
	.hidden	qsort
