	.text
	.p2align 4,,15
	.globl	__creat64
	.type	__creat64, @function
__creat64:
#APP
# 28 "../sysdeps/unix/sysv/linux/creat64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$85, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/creat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	movl	%ebp, %esi
	movl	%eax, %edx
	movq	%rbx, %rdi
	movl	$85, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/creat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%edx, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rcx
	negl	%eax
	movl	%eax, %fs:(%rcx)
	movl	$-1, %eax
	jmp	.L6
	.size	__creat64, .-__creat64
	.weak	creat
	.set	creat,__creat64
	.globl	__creat
	.set	__creat,__creat64
	.weak	creat64
	.set	creat64,__creat64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
