.text
.globl __chdir
.type __chdir,@function
.align 1<<4
__chdir:
	movl $80, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __chdir,.-__chdir
.globl __GI___chdir
.set __GI___chdir,__chdir
.weak chdir
chdir = __chdir
.globl __GI_chdir
.set __GI_chdir,chdir
