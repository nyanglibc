	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	statx_generic.isra.0, @function
statx_generic.isra.0:
	testl	$-6401, %edx
	jne	.L8
	pushq	%rbx
	movq	%rcx, %rbx
	movl	%edx, %ecx
	subq	$400, %rsp
	movq	%rsp, %rdx
	call	__GI___fstatat64
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1
	leaq	152(%rsp), %rdi
	xorl	%eax, %eax
	movl	$31, %ecx
	movl	$2047, 144(%rsp)
	rep stosq
	movq	56(%rsp), %rax
	movl	%eax, 148(%rsp)
	movq	16(%rsp), %rax
	movl	%eax, 160(%rsp)
	movl	28(%rsp), %eax
	movl	%eax, 164(%rsp)
	movl	32(%rsp), %eax
	movl	%eax, 168(%rsp)
	movl	24(%rsp), %eax
	movw	%ax, 172(%rsp)
	movq	8(%rsp), %rax
	movq	%rax, 176(%rsp)
	movq	48(%rsp), %rax
	movq	%rax, 184(%rsp)
	movq	64(%rsp), %rax
	movq	%rax, 192(%rsp)
	movq	72(%rsp), %rax
	movq	%rax, 208(%rsp)
	movq	80(%rsp), %rax
	movl	%eax, 216(%rsp)
	movq	104(%rsp), %rax
	movq	%rax, 240(%rsp)
	movq	112(%rsp), %rax
	movl	%eax, 248(%rsp)
	movq	88(%rsp), %rax
	movq	%rax, 256(%rsp)
	movq	96(%rsp), %rax
	movl	%eax, 264(%rsp)
	movq	40(%rsp), %rax
	movq	%rax, %rcx
	shrq	$8, %rcx
	movl	%ecx, %esi
	movq	%rax, %rcx
	shrq	$32, %rcx
	andl	$4095, %esi
	andl	$-4096, %ecx
	orl	%esi, %ecx
	movl	%ecx, 272(%rsp)
	movzbl	%al, %ecx
	shrq	$12, %rax
	xorb	%al, %al
	orl	%ecx, %eax
	movl	%eax, 276(%rsp)
	movq	(%rsp), %rax
	movdqa	144(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	movq	%rax, %rcx
	shrq	$8, %rcx
	movl	%ecx, %esi
	movq	%rax, %rcx
	movdqa	160(%rsp), %xmm0
	shrq	$32, %rcx
	andl	$4095, %esi
	andl	$-4096, %ecx
	movups	%xmm0, 16(%rbx)
	orl	%esi, %ecx
	movdqa	176(%rsp), %xmm0
	movl	%ecx, 280(%rsp)
	movzbl	%al, %ecx
	shrq	$12, %rax
	movups	%xmm0, 32(%rbx)
	xorb	%al, %al
	orl	%ecx, %eax
	movdqa	192(%rsp), %xmm0
	movl	%eax, 284(%rsp)
	movups	%xmm0, 48(%rbx)
	movdqa	208(%rsp), %xmm0
	movups	%xmm0, 64(%rbx)
	movdqa	224(%rsp), %xmm0
	movups	%xmm0, 80(%rbx)
	movdqa	240(%rsp), %xmm0
	movups	%xmm0, 96(%rbx)
	movdqa	256(%rsp), %xmm0
	movups	%xmm0, 112(%rbx)
	movdqa	272(%rsp), %xmm0
	movups	%xmm0, 128(%rbx)
	movdqa	288(%rsp), %xmm0
	movups	%xmm0, 144(%rbx)
	movdqa	304(%rsp), %xmm0
	movups	%xmm0, 160(%rbx)
	movdqa	320(%rsp), %xmm0
	movups	%xmm0, 176(%rbx)
	movdqa	336(%rsp), %xmm0
	movups	%xmm0, 192(%rbx)
	movdqa	352(%rsp), %xmm0
	movups	%xmm0, 208(%rbx)
	movdqa	368(%rsp), %xmm0
	movups	%xmm0, 224(%rbx)
	movdqa	384(%rsp), %xmm0
	movups	%xmm0, 240(%rbx)
.L1:
	addq	$400, %rsp
	movl	%edx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %edx
	movl	$22, %fs:(%rax)
	movl	%edx, %eax
	ret
	.size	statx_generic.isra.0, .-statx_generic.isra.0
	.p2align 4,,15
	.globl	statx
	.type	statx, @function
statx:
	movl	%ecx, %r10d
	movl	$332, %ecx
	pushq	%r12
	movl	%edi, %r9d
	pushq	%rbp
	movq	%r8, %r12
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movl	%ecx, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/statx.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L14
	testl	%eax, %eax
	jne	.L15
.L9:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%fs:(%rdx), %edx
.L11:
	cmpl	$38, %edx
	jne	.L9
	movq	%r12, %rcx
	movl	%ebp, %edx
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	movl	%r9d, %edi
	jmp	statx_generic.isra.0
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L11
	.size	statx, .-statx
