	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fstatat64
	.hidden	__GI___fstatat64
	.type	__GI___fstatat64, @function
__GI___fstatat64:
	movl	%ecx, %r10d
	movl	$262, %eax
#APP
# 57 "../sysdeps/unix/sysv/linux/fstatat64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L5
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___fstatat64, .-__GI___fstatat64
	.globl	__fstatat64
	.set	__fstatat64,__GI___fstatat64
	.globl	__GI___fstatat
	.set	__GI___fstatat,__fstatat64
	.weak	fstatat
	.set	fstatat,__fstatat64
	.globl	__fstatat
	.set	__fstatat,__fstatat64
	.weak	fstatat64
	.set	fstatat64,__fstatat64
