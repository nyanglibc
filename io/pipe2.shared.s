.text
.globl __pipe2
.type __pipe2,@function
.align 1<<4
__pipe2:
	movl $293, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __pipe2,.-__pipe2
.globl __GI___pipe2
.set __GI___pipe2,__pipe2
.weak pipe2
pipe2 = __pipe2
.globl __GI_pipe2
.set __GI_pipe2,pipe2
