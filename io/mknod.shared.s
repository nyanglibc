	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___mknod
	.hidden	__GI___mknod
	.type	__GI___mknod, @function
__GI___mknod:
	movq	%rdx, %rcx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	__GI___mknodat
	.size	__GI___mknod, .-__GI___mknod
	.globl	__mknod
	.set	__mknod,__GI___mknod
	.weak	mknod
	.set	mknod,__mknod
