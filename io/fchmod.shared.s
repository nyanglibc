.text
.globl __fchmod
.type __fchmod,@function
.align 1<<4
__fchmod:
	movl $91, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __fchmod,.-__fchmod
.globl __GI___fchmod
.set __GI___fchmod,__fchmod
.weak fchmod
fchmod = __fchmod
.globl __GI_fchmod
.set __GI_fchmod,fchmod
