.text
.globl __umask
.type __umask,@function
.align 1<<4
__umask:
	movl $95, %eax
	syscall;
	ret
.size __umask,.-__umask
.weak umask
umask = __umask

