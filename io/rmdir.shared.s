.text
.globl __rmdir
.type __rmdir,@function
.align 1<<4
__rmdir:
	movl $84, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __rmdir,.-__rmdir
.globl __GI___rmdir
.set __GI___rmdir,__rmdir
.weak rmdir
rmdir = __rmdir
.globl __GI_rmdir
.set __GI_rmdir,rmdir
