	.text
	.p2align 4,,15
	.globl	mkfifoat
	.type	mkfifoat, @function
mkfifoat:
	orb	$16, %dh
	xorl	%ecx, %ecx
	jmp	__mknodat
	.size	mkfifoat, .-mkfifoat
	.hidden	__mknodat
