	.text
	.p2align 4,,15
	.globl	__utimensat64_helper
	.hidden	__utimensat64_helper
	.type	__utimensat64_helper, @function
__utimensat64_helper:
	movl	%ecx, %r10d
	movl	$280, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/utimensat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__utimensat64_helper, .-__utimensat64_helper
	.p2align 4,,15
	.globl	__utimensat
	.type	__utimensat, @function
__utimensat:
	testq	%rsi, %rsi
	movl	%ecx, %r10d
	je	.L11
	movl	$280, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/utimensat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L11:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__utimensat, .-__utimensat
	.weak	utimensat
	.set	utimensat,__utimensat
