	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4,,15
	.globl	__fstat64
	.hidden	__fstat64
	.type	__fstat64, @function
__fstat64:
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	movl	$4096, %ecx
	jmp	__fstatat64
	.size	__fstat64, .-__fstat64
	.weak	fstat
	.set	fstat,__fstat64
	.globl	__fstat
	.set	__fstat,__fstat64
	.weak	fstat64
	.set	fstat64,__fstat64
	.hidden	__fstatat64
