	.text
	.p2align 4,,15
	.globl	__openat64_nocancel
	.hidden	__openat64_nocancel
	.type	__openat64_nocancel, @function
__openat64_nocancel:
	testb	$64, %dl
	movq	%rcx, -24(%rsp)
	jne	.L2
	movl	%edx, %eax
	xorl	%r10d, %r10d
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
.L3:
	movl	$257, %eax
#APP
# 43 "../sysdeps/unix/sysv/linux/openat64_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	8(%rsp), %rax
	movl	$24, -72(%rsp)
	movl	-24(%rsp), %r10d
	movq	%rax, -64(%rsp)
	leaq	-48(%rsp), %rax
	movq	%rax, -56(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__openat64_nocancel, .-__openat64_nocancel
	.globl	__openat_nocancel
	.hidden	__openat_nocancel
	.set	__openat_nocancel,__openat64_nocancel
