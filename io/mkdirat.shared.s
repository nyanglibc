.text
.globl mkdirat
.type mkdirat,@function
.align 1<<4
mkdirat:
	movl $258, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size mkdirat,.-mkdirat
.globl __GI_mkdirat
.set __GI_mkdirat,mkdirat
