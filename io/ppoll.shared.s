	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ppoll
	.type	__ppoll, @function
__ppoll:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r10
	pushq	%rbp
	pushq	%rbx
	xorl	%ebx, %ebx
	subq	$40, %rsp
	testq	%rdx, %rdx
	je	.L2
	movdqu	(%rdx), %xmm0
	leaq	16(%rsp), %rbx
	movaps	%xmm0, 16(%rsp)
.L2:
#APP
# 48 "../sysdeps/unix/sysv/linux/ppoll.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$8, %r8d
	movq	%rbx, %rdx
	movl	$271, %eax
#APP
# 48 "../sysdeps/unix/sysv/linux/ppoll.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L13
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%r10, %r13
	movq	%rsi, %r12
	movq	%rdi, %rbp
	call	__libc_enable_asynccancel
	movl	$8, %r8d
	movl	%eax, %r9d
	movq	%r13, %r10
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movl	$271, %eax
#APP
# 48 "../sysdeps/unix/sysv/linux/ppoll.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L14
.L7:
	movl	%r9d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
.L14:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L7
	.size	__ppoll, .-__ppoll
	.globl	__GI_ppoll
	.hidden	__GI_ppoll
	.set	__GI_ppoll,__ppoll
	.globl	ppoll
	.set	ppoll,__GI_ppoll
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
