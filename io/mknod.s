	.text
	.p2align 4,,15
	.globl	__mknod
	.hidden	__mknod
	.type	__mknod, @function
__mknod:
	movq	%rdx, %rcx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	__mknodat
	.size	__mknod, .-__mknod
	.weak	mknod
	.set	mknod,__mknod
	.hidden	__mknodat
