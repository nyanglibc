.text
.globl readlinkat
.type readlinkat,@function
.align 1<<4
readlinkat:
	movq %rcx, %r10
	movl $267, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size readlinkat,.-readlinkat

