	.text
#APP
	.section .gnu.warning.getwd
	.previous
#NO_APP
	.p2align 4,,15
	.globl	getwd
	.type	getwd, @function
getwd:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$4096, %rsp
	testq	%rdi, %rdi
	je	.L7
	movq	%rsp, %r12
	movq	%rdi, %rbx
	movl	$4096, %esi
	movq	%r12, %rdi
	call	__getcwd
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L8
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rbx, %rbp
	call	strcpy
.L1:
	addq	$4096, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$1024, %edx
	movq	%rbx, %rsi
	movl	%fs:(%rax), %edi
	call	__strerror_r
	jmp	.L1
	.size	getwd, .-getwd
	.section	.gnu.warning.getwd
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_getwd, @object
	.size	__evoke_link_warning_getwd, 58
__evoke_link_warning_getwd:
	.string	"the `getwd' function is dangerous and should not be used."
	.hidden	__strerror_r
	.hidden	strcpy
	.hidden	__getcwd
