	.text
	.p2align 4,,15
	.globl	__poll
	.hidden	__poll
	.type	__poll, @function
__poll:
#APP
# 29 "../sysdeps/unix/sysv/linux/poll.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$7, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/poll.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$7, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/poll.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__poll, .-__poll
	.globl	__libc_poll
	.set	__libc_poll,__poll
	.weak	poll
	.set	poll,__poll
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
