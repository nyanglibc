	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___fstat64
	.hidden	__GI___fstat64
	.type	__GI___fstat64, @function
__GI___fstat64:
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	movl	$4096, %ecx
	jmp	__GI___fstatat64
	.size	__GI___fstat64, .-__GI___fstat64
	.globl	__fstat64
	.set	__fstat64,__GI___fstat64
	.weak	fstat
	.set	fstat,__fstat64
	.globl	__fstat
	.set	__fstat,__fstat64
	.weak	fstat64
	.set	fstat64,__fstat64
