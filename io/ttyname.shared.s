	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"stdin"
.LC1:
	.string	"stdout"
.LC2:
	.string	"stderr"
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.type	getttyname, @function
getttyname:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$200, %rsp
	movq	%rdi, 16(%rsp)
	movl	%edx, 36(%rsp)
	call	__GI_strlen
	movq	%rbx, %rdi
	movq	%rax, 24(%rsp)
	leaq	1(%rax), %r15
	call	__opendir
	testq	%rax, %rax
	je	.L23
	cmpq	%r15, namelen.8896(%rip)
	movq	%rax, %rbx
	ja	.L24
.L4:
	leaq	48(%rsp), %rax
	leaq	.LC0(%rip), %r12
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L5:
	movq	%rbx, %rdi
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L25
	movq	8(%rbp), %rdx
	cmpq	%rdx, (%rax)
	je	.L6
	movl	0(%r13), %edx
	testl	%edx, %edx
	je	.L5
.L6:
	leaq	19(%rax), %r14
	movl	$6, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	repz cmpsb
	je	.L5
	leaq	.LC1(%rip), %rdi
	movl	$7, %ecx
	movq	%r14, %rsi
	repz cmpsb
	je	.L5
	leaq	.LC2(%rip), %rdi
	movl	$7, %ecx
	movq	%r14, %rsi
	repz cmpsb
	je	.L5
	movzwl	16(%rax), %ecx
	movq	getttyname_name(%rip), %r8
	addq	%rax, %rcx
	subq	%r14, %rcx
	leaq	(%r15,%rcx), %rax
	cmpq	namelen.8896(%rip), %rax
	movq	%rax, (%rsp)
	jbe	.L8
	movq	%r8, %rdi
	movq	%rcx, 40(%rsp)
	call	free@PLT
	movq	(%rsp), %rax
	leaq	(%rax,%rax), %rdi
	movq	%rdi, namelen.8896(%rip)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, getttyname_name(%rip)
	movq	40(%rsp), %rcx
	je	.L26
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	%rax, %rdi
	movq	%rcx, 40(%rsp)
	movq	%rax, (%rsp)
	call	__GI_mempcpy@PLT
	movq	40(%rsp), %rcx
	movq	(%rsp), %r8
	movb	$47, (%rax)
.L8:
	leaq	(%r8,%r15), %rdi
	movq	%rcx, %rdx
	movq	%r14, %rsi
	movq	%r8, (%rsp)
	call	__GI_memcpy@PLT
	movq	(%rsp), %r8
	movq	8(%rsp), %rsi
	movq	%r8, %rdi
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L5
	movq	8(%rbp), %rax
	cmpq	%rax, 56(%rsp)
	jne	.L5
	movq	0(%rbp), %rax
	cmpq	%rax, 48(%rsp)
	jne	.L5
	movl	72(%rsp), %eax
	andl	$61440, %eax
	cmpl	$8192, %eax
	jne	.L5
	movq	40(%rbp), %rax
	cmpq	%rax, 88(%rsp)
	jne	.L5
	movq	%rbx, %rdi
	call	__closedir
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	36(%rsp), %ebx
	movq	getttyname_name(%rip), %r14
	movl	%ebx, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	__closedir
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	36(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
.L1:
	addq	$200, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	getttyname_name(%rip), %rdi
	call	__GI_mempcpy@PLT
	movb	$47, (%rax)
	jmp	.L4
.L23:
	movl	$-1, 0(%r13)
	xorl	%r14d, %r14d
	jmp	.L1
.L26:
	movl	$-1, 0(%r13)
	movq	%rbx, %rdi
	movq	%rax, %r14
	call	__closedir
	jmp	.L1
	.size	getttyname, .-getttyname
	.section	.rodata.str1.1
.LC3:
	.string	"(unreachable)"
.LC4:
	.string	"/dev/pts"
.LC5:
	.string	"/dev"
	.text
	.p2align 4,,15
	.globl	ttyname
	.type	ttyname, @function
ttyname:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$400, %rsp
	movq	__libc_errno@gottpoff(%rip), %rbp
	leaq	48(%rsp), %rsi
	movl	$0, 12(%rsp)
	movl	%fs:0(%rbp), %r14d
	call	__GI___tcgetattr
	testl	%eax, %eax
	js	.L56
	leaq	112(%rsp), %r13
	movl	%ebx, %edi
	movq	%r13, %rsi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L56
	leaq	16(%rsp), %r12
	movabsq	$7310238724270485551, %rax
	xorl	%ecx, %ecx
	movq	%rax, 16(%rsp)
	movl	$10, %edx
	movl	$12132, %eax
	leaq	14(%r12), %rsi
	movslq	%ebx, %rdi
	movl	$1714382444, 24(%rsp)
	movw	%ax, 28(%rsp)
	movb	$0, 30(%rsp)
	call	_fitoa_word
	movq	buflen.8909(%rip), %rdx
	movb	$0, (%rax)
	testq	%rdx, %rdx
	je	.L57
	movq	ttyname_buf(%rip), %rax
.L32:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	__readlink
	cmpq	$-1, %rax
	je	.L48
	cmpq	buflen.8909(%rip), %rax
	movq	%rax, %rbx
	jnb	.L56
	cmpq	$13, %rax
	movq	ttyname_buf(%rip), %rcx
	jbe	.L34
	movabsq	$7521962890978293032, %rax
	cmpq	%rax, (%rcx)
	je	.L58
.L34:
	movb	$0, (%rcx,%rbx)
	cmpb	$47, (%rcx)
	leaq	256(%rsp), %rbx
	je	.L37
.L38:
	movl	$1, %r12d
.L33:
	leaq	.LC4(%rip), %rdi
	movq	%rbx, %rsi
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L40
	movl	280(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L59
.L40:
	cmpl	$-1, 12(%rsp)
	movl	%r14d, %fs:0(%rbp)
	je	.L45
	leaq	12(%rsp), %rbx
.L44:
	leaq	.LC5(%rip), %rdi
	movq	%rbx, %rcx
	movl	%r14d, %edx
	movq	%r13, %rsi
	call	getttyname
	testq	%rax, %rax
	jne	.L27
	cmpl	$-1, 12(%rsp)
	je	.L45
	leaq	.LC5(%rip), %rdi
	movl	%r14d, %edx
	movq	%rbx, %rcx
	movq	%r13, %rsi
	movl	$1, 12(%rsp)
	call	getttyname
	testq	%rax, %rax
	sete	%dl
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	12(%rsp), %rbx
	leaq	.LC4(%rip), %rdi
	movl	%r14d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rcx
	call	getttyname
	testq	%rax, %rax
	jne	.L27
	cmpl	$-1, 12(%rsp)
	jne	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$1, %edx
	xorl	%eax, %eax
.L46:
	testl	%r12d, %r12d
	je	.L27
	testb	%dl, %dl
	je	.L27
	movq	152(%rsp), %rax
	movq	%rax, %rdx
	shrq	$32, %rax
	shrq	$8, %rdx
	andl	$-4096, %eax
	andl	$4095, %edx
	orl	%edx, %eax
	subl	$136, %eax
	cmpl	$7, %eax
	ja	.L56
	movl	$19, %fs:0(%rbp)
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%eax, %eax
.L27:
	addq	$400, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$4096, %edi
	movq	$4095, buflen.8909(%rip)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, ttyname_buf(%rip)
	movl	$4095, %edx
	jne	.L32
	movq	$0, buflen.8909(%rip)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbx, %rsi
	movq	%rcx, %rdi
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L38
	movq	264(%rsp), %rax
	cmpq	%rax, 120(%rsp)
	jne	.L38
	movq	112(%rsp), %rax
	cmpq	%rax, 256(%rsp)
	jne	.L38
	movl	280(%rsp), %eax
	andl	$61440, %eax
	cmpl	$8192, %eax
	jne	.L38
	movq	152(%rsp), %rax
	cmpq	%rax, 296(%rsp)
	jne	.L38
	movq	ttyname_buf(%rip), %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L58:
	cmpl	$1701601889, 8(%rcx)
	jne	.L34
	cmpb	$41, 12(%rcx)
	jne	.L34
	leaq	13(%rcx), %rsi
	subq	$13, %rbx
	movq	%rcx, %rdi
	movq	%rbx, %rdx
	call	__GI_memmove
	movq	%rax, %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%r12d, %r12d
	leaq	256(%rsp), %rbx
	jmp	.L33
	.size	ttyname, .-ttyname
	.local	namelen.8896
	.comm	namelen.8896,8,8
	.local	buflen.8909
	.comm	buflen.8909,8,8
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	ttyname_buf, @object
	.size	ttyname_buf, 8
ttyname_buf:
	.zero	8
	.align 8
	.type	getttyname_name, @object
	.size	getttyname_name, 8
getttyname_name:
	.zero	8
	.hidden	__readlink
	.hidden	_fitoa_word
	.hidden	__closedir
	.hidden	__opendir
