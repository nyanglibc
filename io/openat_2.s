	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"invalid openat call: O_CREAT or O_TMPFILE without mode"
	.text
	.p2align 4,,15
	.globl	__openat_2
	.type	__openat_2, @function
__openat_2:
	testb	$64, %dl
	jne	.L2
	movl	%edx, %eax
	andl	$4259840, %eax
	cmpl	$4259840, %eax
	je	.L2
	xorl	%eax, %eax
	jmp	__openat
.L2:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__fortify_fail
	.size	__openat_2, .-__openat_2
	.hidden	__fortify_fail
	.hidden	__openat
