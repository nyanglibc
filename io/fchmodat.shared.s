	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"/proc/self/fd/%d"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_fchmodat
	.hidden	__GI_fchmodat
	.type	__GI_fchmodat, @function
__GI_fchmodat:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$176, %rsp
	testl	%ecx, %ecx
	jne	.L2
	movl	$268, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/fchmodat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L3
	addq	$176, %rsp
	movl	%eax, %ebx
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$256, %ecx
	jne	.L18
	xorl	%eax, %eax
	movl	%edx, %ebp
	movl	$2752512, %edx
	call	__GI___openat_nocancel
	testl	%eax, %eax
	movl	%eax, %ebx
	jns	.L19
.L1:
	addq	$176, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	32(%rsp), %rdx
	leaq	.LC0(%rip), %rsi
	movl	$4096, %ecx
	movl	%eax, %edi
	call	__GI___fstatat64
	testl	%eax, %eax
	jne	.L16
	movl	56(%rsp), %eax
	andl	$61440, %eax
	cmpl	$40960, %eax
	je	.L20
	movq	%rsp, %r12
	leaq	.LC1(%rip), %rdx
	xorl	%eax, %eax
	movl	%ebx, %ecx
	movl	$32, %esi
	movq	%r12, %rdi
	call	__GI___snprintf
	testl	%eax, %eax
	js	.L16
	movl	%ebp, %esi
	movq	%r12, %rdi
	call	__GI___chmod
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L11
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$2, %fs:(%rax)
	jne	.L11
	movl	$95, %fs:(%rax)
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%ebx, %edi
	movl	%ebp, %ebx
	call	__GI___close_nocancel
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%ebx, %edi
	movl	$-1, %ebx
	call	__GI___close_nocancel
	addq	$176, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	$-1, %ebx
	movl	%eax, %fs:(%rdx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$22, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	movl	%ebx, %edi
	movl	$-1, %ebx
	call	__GI___close_nocancel
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$95, %fs:(%rax)
	jmp	.L1
	.size	__GI_fchmodat, .-__GI_fchmodat
	.globl	fchmodat
	.set	fchmodat,__GI_fchmodat
