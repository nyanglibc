	.text
	.p2align 4,,15
	.globl	mkfifo
	.type	mkfifo, @function
mkfifo:
	orl	$4096, %esi
	xorl	%edx, %edx
	jmp	__mknod
	.size	mkfifo, .-mkfifo
	.hidden	__mknod
