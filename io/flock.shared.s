.text
.globl __flock
.type __flock,@function
.align 1<<4
__flock:
	movl $73, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __flock,.-__flock
.globl __GI___flock
.set __GI___flock,__flock
.weak flock
flock = __flock
.globl __GI_flock
.set __GI_flock,flock
