.text
.globl __pipe
.type __pipe,@function
.align 1<<4
__pipe:
	movl $22, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __pipe,.-__pipe
.globl __GI___pipe
.set __GI___pipe,__pipe
.weak pipe
pipe = __pipe
.globl __GI_pipe
.set __GI_pipe,pipe
