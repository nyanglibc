	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"."
.LC1:
	.string	"/"
.LC2:
	.string	".."
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"../sysdeps/unix/sysv/linux/getcwd.c"
	.align 8
.LC4:
	.string	"errno != ERANGE || buf != NULL || size != 0"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___getcwd
	.hidden	__GI___getcwd
	.type	__GI___getcwd, @function
__GI___getcwd:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$264, %rsp
	testq	%rsi, %rsi
	movq	%rsi, 24(%rsp)
	jne	.L2
	testq	%rdi, %rdi
	jne	.L100
	call	__GI___getpagesize
	movl	$4096, %esi
	cmpl	$4096, %eax
	cmovge	%eax, %esi
	movslq	%esi, %rsi
.L5:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	8(%rsp), %rsi
	jne	.L6
	.p2align 4,,10
	.p2align 3
.L101:
	xorl	%r15d, %r15d
.L1:
	addq	$264, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rdi, %rdi
	movq	24(%rsp), %rsi
	je	.L5
	movq	%rdi, %r15
.L6:
	movq	%r15, %rdi
	movl	$79, %eax
#APP
# 81 "../sysdeps/unix/sysv/linux/getcwd.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L103
	cmpl	$0, %eax
	jle	.L9
	testq	%rbx, %rbx
	sete	70(%rsp)
	cmpq	$0, 24(%rsp)
	movzbl	70(%rsp), %ecx
	sete	%dl
	andl	%ecx, %edx
	cmpb	$47, (%r15)
	je	.L104
	testb	%dl, %dl
	jne	.L105
.L15:
	cmpq	$0, 24(%rsp)
	jne	.L54
.L100:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r15d, %r15d
	movl	$22, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L105:
	movq	%r15, %rdi
	call	free@PLT
	movl	$4096, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	$0, 40(%rsp)
	je	.L16
	movq	$4096, 48(%rsp)
.L17:
	movq	48(%rsp), %rax
	leaq	112(%rsp), %rsi
	leaq	.LC0(%rip), %rdi
	movq	%rsi, 8(%rsp)
	leaq	(%r15,%rax), %rbx
	leaq	-1(%rbx), %rax
	movb	$0, -1(%rbx)
	movq	%rax, 72(%rsp)
	call	__GI___lstat64
	testl	%eax, %eax
	js	.L99
	movq	112(%rsp), %rax
	movq	8(%rsp), %rsi
	leaq	.LC1(%rip), %rdi
	movq	%rax, 16(%rsp)
	movq	120(%rsp), %rax
	movq	%rax, %r14
	call	__GI___lstat64
	testl	%eax, %eax
	js	.L99
	movq	112(%rsp), %rax
	cmpq	%rax, 16(%rsp)
	movq	%r14, %rcx
	movq	120(%rsp), %rdi
	movq	%rax, 80(%rsp)
	setne	%dl
	cmpq	%rdi, %r14
	movq	%rdi, 88(%rsp)
	setne	%al
	orb	%al, %dl
	movb	%dl, 71(%rsp)
	je	.L21
	movq	__libc_errno@gottpoff(%rip), %r12
	movq	%r15, 56(%rsp)
	xorl	%ebp, %ebp
	movl	$-100, %r14d
	movq	%rcx, %r15
.L82:
	leaq	.LC2(%rip), %rsi
	xorl	%edx, %edx
	movl	%r14d, %edi
	xorl	%eax, %eax
	call	__GI___openat64
	testl	%eax, %eax
	movl	%eax, %r14d
	js	.L22
	movq	8(%rsp), %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	jne	.L23
	testq	%rbp, %rbp
	je	.L27
	movq	%rbp, %rdi
	call	__closedir
	testl	%eax, %eax
	jne	.L95
.L27:
	movq	112(%rsp), %rax
	movl	%r14d, %edi
	movq	%rax, 32(%rsp)
	movq	120(%rsp), %rax
	movq	%rax, 96(%rsp)
	call	__fdopendir
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L95
	movzbl	71(%rsp), %r13d
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rbp, %rdi
	movl	$0, %fs:(%r12)
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L106
	cmpb	$46, 19(%rax)
	je	.L51
.L32:
	testb	%r13b, %r13b
	je	.L34
	cmpq	%r15, (%rax)
	movq	16(%rsp), %rsi
	sete	%dl
	cmpq	%rsi, 32(%rsp)
	setne	%cl
	orb	%cl, %dl
	je	.L26
	movl	%edx, %r13d
.L34:
	leaq	19(%rax), %rbx
	movq	8(%rsp), %rdx
	movl	$256, %ecx
	movl	%r14d, %edi
	movq	%rbx, %rsi
	call	__GI___fstatat64
	testl	%eax, %eax
	jne	.L26
	movl	136(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	jne	.L26
	movq	16(%rsp), %rax
	cmpq	%rax, 112(%rsp)
	jne	.L26
	cmpq	%r15, 120(%rsp)
	jne	.L26
	movq	72(%rsp), %r13
	subq	56(%rsp), %r13
	movq	%rbx, %rdi
	call	__GI_strlen
	movq	%rax, %rcx
	cmpq	%rax, %r13
	ja	.L35
	cmpq	$0, 24(%rsp)
	jne	.L107
	movq	48(%rsp), %rax
	movq	%rcx, 104(%rsp)
	cmpq	%rax, %rcx
	movq	%rax, %r15
	cmovnb	%rcx, %r15
	addq	%rax, %r15
	jc	.L39
	movq	56(%rsp), %rdi
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L39
	movq	48(%rsp), %rax
	movq	%r13, %rdi
	leaq	(%r8,%r13), %rsi
	movq	%r8, 16(%rsp)
	subq	%rax, %rdi
	subq	%r13, %rax
	addq	%r15, %rdi
	movq	%rax, %rdx
	addq	%r8, %rdi
	call	__GI_memcpy@PLT
	movq	16(%rsp), %r8
	movq	104(%rsp), %rcx
	movq	%rax, 72(%rsp)
	movq	%r15, 48(%rsp)
	movq	%r8, 56(%rsp)
.L35:
	movq	72(%rsp), %r8
	movq	%rbx, %rsi
	movq	%rcx, %rdx
	subq	%rcx, %r8
	movq	%r8, %rdi
	call	__GI_memcpy@PLT
	movq	%rax, %r8
	leaq	-1(%rax), %rax
	movq	96(%rsp), %rbx
	movb	$47, -1(%r8)
	movq	%rax, 72(%rsp)
	movq	32(%rsp), %rax
	cmpq	%rax, 80(%rsp)
	movq	%rbx, %r15
	movq	%rax, 16(%rsp)
	jne	.L82
	cmpq	%rbx, 88(%rsp)
	jne	.L82
	movq	%rbp, %rdi
	movq	56(%rsp), %r15
	call	__closedir
	testl	%eax, %eax
	jne	.L41
	movq	48(%rsp), %rax
	leaq	(%r15,%rax), %rbx
.L21:
	movq	48(%rsp), %rax
	leaq	-1(%r15,%rax), %rax
	cmpq	72(%rsp), %rax
	je	.L108
.L42:
	movq	72(%rsp), %rsi
	movq	%r15, %rdi
	subq	%rsi, %rbx
	movq	%rbx, %rdx
	call	__GI_memmove
	cmpq	$0, 24(%rsp)
	jne	.L43
	cmpq	48(%rsp), %rbx
	jnb	.L1
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	realloc@PLT
	movq	%rax, 40(%rsp)
.L43:
	movq	40(%rsp), %rax
	testq	%rax, %rax
	cmovne	%rax, %r15
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L106:
	movl	%fs:(%r12), %ebx
	testl	%ebx, %ebx
	jne	.L97
	testb	%r13b, %r13b
	je	.L97
	movq	%rbp, %rdi
	call	__GI___rewinddir
	movq	%rbp, %rdi
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L109
	xorl	%r13d, %r13d
	cmpb	$46, 19(%rax)
	jne	.L34
.L51:
	cmpb	$0, 20(%rax)
	je	.L26
	cmpw	$46, 20(%rax)
	jne	.L32
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L97:
	movq	56(%rsp), %r15
.L29:
	testl	%ebx, %ebx
	jne	.L31
	movl	$2, %fs:(%r12)
	movl	$2, %ebx
.L31:
	movq	%rbp, %rdi
	call	__closedir
.L19:
	cmpq	$0, 40(%rsp)
	je	.L110
.L46:
	movl	%ebx, %fs:(%r12)
.L16:
	cmpq	$0, 24(%rsp)
	je	.L101
	cmpb	$0, 70(%rsp)
	je	.L101
	movq	40(%rsp), %rdi
	call	free@PLT
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L9:
	je	.L13
	movq	__libc_errno@gottpoff(%rip), %r12
	movl	%fs:(%r12), %eax
.L8:
	cmpl	$36, %eax
	je	.L13
	cmpl	$34, %eax
	je	.L111
.L48:
	testq	%rbx, %rbx
	jne	.L101
	movq	%r15, %rdi
	xorl	%r15d, %r15d
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L104:
	testb	%dl, %dl
	jne	.L112
.L11:
	testq	%rbx, %rbx
	cmovne	%rbx, %r15
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L54:
	movq	24(%rsp), %rax
	movq	%r15, 40(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%rbx, %rbx
	sete	70(%rsp)
	cmpq	$0, 24(%rsp)
	movzbl	70(%rsp), %eax
	sete	%dl
	andl	%eax, %edx
	testb	%dl, %dl
	je	.L15
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L103:
	movq	__libc_errno@gottpoff(%rip), %r12
	negl	%eax
	movl	%eax, %fs:(%r12)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%r15, %rdi
	call	free@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L99:
	movq	__libc_errno@gottpoff(%rip), %r12
	movl	%fs:(%r12), %ebx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L112:
	movslq	%eax, %rsi
	call	realloc@PLT
	movq	%rax, %rbx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L111:
	testq	%rbx, %rbx
	jne	.L48
	cmpq	$0, 24(%rsp)
	jne	.L48
	leaq	__PRETTY_FUNCTION__.9117(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$124, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L109:
	movq	56(%rsp), %r15
	movl	%fs:(%r12), %ebx
	jmp	.L29
.L95:
	movq	56(%rsp), %r15
	movl	%fs:(%r12), %ebx
.L45:
	movl	%r14d, %edi
	call	__GI___close_nocancel
	jmp	.L19
.L22:
	testq	%rbp, %rbp
	movq	56(%rsp), %r15
	movl	%fs:(%r12), %ebx
	je	.L19
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L23:
	testq	%rbp, %rbp
	movq	56(%rsp), %r15
	movl	%fs:(%r12), %ebx
	je	.L45
	movq	%rbp, %rdi
	call	__closedir
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L108:
	subq	$1, 72(%rsp)
	movb	$47, -1(%rax)
	jmp	.L42
.L41:
	movl	%fs:(%r12), %ebx
	jmp	.L19
.L39:
	movq	56(%rsp), %r15
	movl	$12, %fs:(%r12)
	movl	$12, %ebx
	jmp	.L31
.L107:
	movq	56(%rsp), %r15
	movl	$34, %fs:(%r12)
	movl	$34, %ebx
	jmp	.L31
	.size	__GI___getcwd, .-__GI___getcwd
	.globl	__getcwd
	.set	__getcwd,__GI___getcwd
	.weak	getcwd
	.set	getcwd,__getcwd
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9117, @object
	.size	__PRETTY_FUNCTION__.9117, 9
__PRETTY_FUNCTION__.9117:
	.string	"__getcwd"
	.hidden	__fdopendir
	.hidden	__closedir
