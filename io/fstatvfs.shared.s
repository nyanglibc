	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fstatvfs
	.type	__fstatvfs, @function
__fstatvfs:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movl	%edi, %ebp
	addq	$-128, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__fstatfs
	testl	%eax, %eax
	js	.L3
	movl	%ebp, %ecx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	xorl	%edi, %edi
	call	__internal_statvfs
	xorl	%eax, %eax
.L1:
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	__fstatvfs, .-__fstatvfs
	.weak	fstatvfs64
	.set	fstatvfs64,__fstatvfs
	.globl	__fstatvfs64
	.set	__fstatvfs64,__fstatvfs
	.weak	__GI_fstatvfs
	.hidden	__GI_fstatvfs
	.set	__GI_fstatvfs,__fstatvfs
	.weak	fstatvfs
	.set	fstatvfs,__GI_fstatvfs
	.hidden	__internal_statvfs
	.hidden	__fstatfs
