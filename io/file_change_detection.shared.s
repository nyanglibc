	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___file_is_unchanged
	.hidden	__GI___file_is_unchanged
	.type	__GI___file_is_unchanged, @function
__GI___file_is_unchanged:
	movq	(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	js	.L1
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	js	.L1
	testq	%rdx, %rdx
	jne	.L3
	testq	%rcx, %rcx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpq	%rcx, %rdx
	jne	.L1
	movq	8(%rsi), %rcx
	cmpq	%rcx, 8(%rdi)
	jne	.L1
	movq	16(%rsi), %rcx
	cmpq	%rcx, 16(%rdi)
	jne	.L1
	movq	24(%rsi), %rcx
	cmpq	%rcx, 24(%rdi)
	jne	.L1
	movq	32(%rsi), %rcx
	cmpq	%rcx, 32(%rdi)
	jne	.L1
	movq	40(%rsi), %rax
	cmpq	%rax, 40(%rdi)
	sete	%al
.L1:
	rep ret
	.size	__GI___file_is_unchanged, .-__GI___file_is_unchanged
	.globl	__file_is_unchanged
	.set	__file_is_unchanged,__GI___file_is_unchanged
	.p2align 4,,15
	.globl	__GI___file_change_detection_for_stat
	.hidden	__GI___file_change_detection_for_stat
	.type	__GI___file_change_detection_for_stat, @function
__GI___file_change_detection_for_stat:
	movl	24(%rsi), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L18
	cmpl	$32768, %eax
	je	.L17
	movq	$-1, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	48(%rsi), %rax
	movdqu	88(%rsi), %xmm0
	movq	%rax, (%rdi)
	movq	8(%rsi), %rax
	movups	%xmm0, 16(%rdi)
	movdqu	104(%rsi), %xmm0
	movq	%rax, 8(%rdi)
	movups	%xmm0, 32(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	$0, (%rdi)
	ret
	.size	__GI___file_change_detection_for_stat, .-__GI___file_change_detection_for_stat
	.globl	__file_change_detection_for_stat
	.set	__file_change_detection_for_stat,__GI___file_change_detection_for_stat
	.p2align 4,,15
	.globl	__GI___file_change_detection_for_path
	.hidden	__GI___file_change_detection_for_path
	.type	__GI___file_change_detection_for_path, @function
__GI___file_change_detection_for_path:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$144, %rsp
	movq	%rsp, %rsi
	call	__GI___stat64
	testl	%eax, %eax
	je	.L20
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edx
	xorl	%eax, %eax
	cmpl	$40, %edx
	ja	.L19
	movabsq	$1099514781702, %rcx
	btq	%rdx, %rcx
	jc	.L28
.L19:
	addq	$144, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L28
	cmpl	$32768, %eax
	je	.L23
	movq	$-1, (%rbx)
	addq	$144, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	$0, (%rbx)
	addq	$144, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	48(%rsp), %rax
	movdqu	88(%rsp), %xmm0
	movq	%rax, (%rbx)
	movq	8(%rsp), %rax
	movups	%xmm0, 16(%rbx)
	movdqu	104(%rsp), %xmm0
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	movups	%xmm0, 32(%rbx)
	jmp	.L19
	.size	__GI___file_change_detection_for_path, .-__GI___file_change_detection_for_path
	.globl	__file_change_detection_for_path
	.set	__file_change_detection_for_path,__GI___file_change_detection_for_path
	.p2align 4,,15
	.globl	__GI___file_change_detection_for_fp
	.hidden	__GI___file_change_detection_for_fp
	.type	__GI___file_change_detection_for_fp, @function
__GI___file_change_detection_for_fp:
	testq	%rsi, %rsi
	je	.L39
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$144, %rsp
	call	__GI___fileno
	movq	%rsp, %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L40
.L29:
	addq	$144, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L41
	cmpl	$32768, %eax
	je	.L34
	movq	$-1, (%rbx)
	addq	$144, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	$0, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movq	$0, (%rbx)
	movl	$1, %eax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L34:
	movq	48(%rsp), %rax
	movdqu	88(%rsp), %xmm0
	movq	%rax, (%rbx)
	movq	8(%rsp), %rax
	movups	%xmm0, 16(%rbx)
	movdqu	104(%rsp), %xmm0
	movq	%rax, 8(%rbx)
	movl	$1, %eax
	movups	%xmm0, 32(%rbx)
	jmp	.L29
	.size	__GI___file_change_detection_for_fp, .-__GI___file_change_detection_for_fp
	.globl	__file_change_detection_for_fp
	.set	__file_change_detection_for_fp,__GI___file_change_detection_for_fp
