	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	fallocate64
	.type	fallocate64, @function
fallocate64:
	movq	%rcx, %r10
#APP
# 27 "../sysdeps/unix/sysv/linux/fallocate64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$285, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/fallocate64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movl	%esi, %ebp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	movq	%r13, %r10
	movl	%eax, %r8d
	movq	%r12, %rdx
	movl	%ebp, %esi
	movl	%ebx, %edi
	movl	$285, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/fallocate64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	fallocate64, .-fallocate64
	.weak	fallocate
	.set	fallocate,fallocate64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
