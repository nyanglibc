	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__euidaccess
	.type	__euidaccess, @function
__euidaccess:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebx
	subq	$152, %rsp
	movq	%rsp, %rsi
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L11
	movl	%ebx, %r13d
	movl	%eax, %ebp
	andl	$7, %r13d
	jne	.L17
.L1:
	addq	$152, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	call	__geteuid
	movl	%eax, %r14d
	call	__getegid
	movl	%eax, %r15d
	call	__getuid
	cmpl	%r14d, %eax
	je	.L18
.L4:
	testl	%r14d, %r14d
	jne	.L5
	andl	$1, %ebx
	je	.L1
	testb	$73, 24(%rsp)
	jne	.L1
.L5:
	cmpl	%r14d, 28(%rsp)
	je	.L19
	movl	32(%rsp), %edi
	cmpl	%r15d, %edi
	je	.L10
	call	__group_member
	testl	%eax, %eax
	je	.L20
.L10:
	leal	0(,%r13,8), %eax
	andl	24(%rsp), %eax
	shrl	$3, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%r13d, %eax
	sall	$6, %eax
	andl	24(%rsp), %eax
	shrl	$6, %eax
.L8:
	cmpl	%r13d, %eax
	je	.L1
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$13, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	call	__getgid
	cmpl	%r15d, %eax
	jne	.L4
	movl	%r13d, %esi
	movq	%r12, %rdi
	call	__GI___access
	movl	%eax, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	movl	24(%rsp), %eax
	andl	%r13d, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$-1, %ebp
	jmp	.L1
	.size	__euidaccess, .-__euidaccess
	.weak	eaccess
	.set	eaccess,__euidaccess
	.weak	euidaccess
	.set	euidaccess,__euidaccess
	.hidden	__getgid
	.hidden	__group_member
	.hidden	__getuid
	.hidden	__getegid
	.hidden	__geteuid
