	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___lstat64
	.hidden	__GI___lstat64
	.type	__GI___lstat64, @function
__GI___lstat64:
	movq	%rsi, %rdx
	movl	$256, %ecx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	__GI___fstatat64
	.size	__GI___lstat64, .-__GI___lstat64
	.globl	__lstat64
	.set	__lstat64,__GI___lstat64
	.weak	lstat
	.set	lstat,__lstat64
	.globl	__lstat
	.set	__lstat,__lstat64
	.weak	lstat64
	.set	lstat64,__lstat64
