	.text
	.p2align 4,,15
	.globl	lchmod
	.type	lchmod, @function
lchmod:
	movl	%esi, %edx
	movl	$256, %ecx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	fchmodat
	.size	lchmod, .-lchmod
	.hidden	fchmodat
