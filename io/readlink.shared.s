.text
.globl __readlink
.type __readlink,@function
.align 1<<4
__readlink:
	movl $89, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __readlink,.-__readlink
.globl __GI___readlink
.set __GI___readlink,__readlink
.weak readlink
readlink = __readlink
.globl __GI_readlink
.set __GI_readlink,readlink
