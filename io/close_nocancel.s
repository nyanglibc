	.text
	.p2align 4,,15
	.globl	__close_nocancel
	.hidden	__close_nocancel
	.type	__close_nocancel, @function
__close_nocancel:
	movl	$3, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/close_nocancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__close_nocancel, .-__close_nocancel
