.text
.globl __fchdir
.type __fchdir,@function
.align 1<<4
__fchdir:
	movl $81, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __fchdir,.-__fchdir
.globl __GI___fchdir
.set __GI___fchdir,__fchdir
.weak fchdir
fchdir = __fchdir
.globl __GI_fchdir
.set __GI_fchdir,fchdir
