.text
.globl __lchown
.type __lchown,@function
.align 1<<4
__lchown:
	movl $94, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __lchown,.-__lchown
.weak lchown
lchown = __lchown
