	.text
	.p2align 4,,15
	.globl	__stat64
	.hidden	__stat64
	.type	__stat64, @function
__stat64:
	movq	%rsi, %rdx
	xorl	%ecx, %ecx
	movq	%rdi, %rsi
	movl	$-100, %edi
	jmp	__fstatat64
	.size	__stat64, .-__stat64
	.weak	stat
	.set	stat,__stat64
	.globl	__stat
	.set	__stat,__stat64
	.weak	stat64
	.set	stat64,__stat64
	.hidden	__fstatat64
