	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"PWD"
.LC1:
	.string	"."
#NO_APP
	.text
	.p2align 4,,15
	.globl	get_current_dir_name
	.type	get_current_dir_name, @function
get_current_dir_name:
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	subq	$288, %rsp
	call	__GI_getenv
	testq	%rax, %rax
	je	.L2
	leaq	.LC1(%rip), %rdi
	movq	%rsp, %rsi
	movq	%rax, %rbx
	call	__GI___stat64
	testl	%eax, %eax
	je	.L9
.L2:
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__GI___getcwd
.L1:
	addq	$288, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	144(%rsp), %rsi
	movq	%rbx, %rdi
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L2
	movq	(%rsp), %rax
	cmpq	%rax, 144(%rsp)
	jne	.L2
	movq	8(%rsp), %rax
	cmpq	%rax, 152(%rsp)
	jne	.L2
	movq	%rbx, %rdi
	call	__GI___strdup
	jmp	.L1
	.size	get_current_dir_name, .-get_current_dir_name
