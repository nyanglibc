	.text
	.p2align 4,,15
	.type	calc_sizes, @function
calc_sizes:
.LFB81:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	32(%rdi), %rbp
	testq	%rax, %rax
	je	.L18
	addq	$1, 16(%rsi)
	xorl	%edx, %edx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L5:
	addl	$1, %edx
.L6:
	addq	$48, %rax
	movl	-40(%rax), %edi
	testl	%edi, %edi
	jne	.L5
	cmpq	$0, -48(%rax)
	jne	.L5
	cmpq	$0, -16(%rax)
	jne	.L5
	movl	-8(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L5
	leal	(%rdx,%rdx,2), %eax
	movslq	%edx, %rdx
	addq	%rdx, 8(%rsi)
	cltq
	addq	%rax, (%rsi)
.L4:
	testq	%rbp, %rbp
	je	.L1
	movq	0(%rbp), %rdi
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.L1
.L8:
	addq	$32, %rbp
	movq	%rbx, %rsi
	call	calc_sizes
	movq	0(%rbp), %rdi
	addq	$1, 24(%rbx)
	testq	%rdi, %rdi
	jne	.L8
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L18:
	cmpq	$0, 8(%rdi)
	je	.L4
	addq	$1, 16(%rsi)
	jmp	.L4
.LFE81:
	.size	calc_sizes, .-calc_sizes
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"3600"
	.text
	.p2align 4,,15
	.type	argp_default_parser, @function
argp_default_parser:
.LFB75:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$8, %rsp
	cmpl	$-3, %edi
	je	.L21
	movq	%rsi, %rbp
	jle	.L39
	cmpl	$-2, %edi
	je	.L24
	cmpl	$63, %edi
	jne	.L32
	movq	80(%rdx), %rsi
	movq	%rbx, %rdi
	movl	$634, %edx
	call	__argp_state_help
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rsi, program_invocation_name(%rip)
	movq	%rbp, %rdi
	movl	$47, %esi
	call	strrchr
	testq	%rax, %rax
	movq	%rax, 64(%rbx)
	je	.L26
	addq	$1, %rax
	movq	%rax, 64(%rbx)
.L27:
	movq	%rax, program_invocation_short_name(%rip)
	movl	28(%rbx), %eax
	andl	$3, %eax
	cmpl	$1, %eax
	jne	.L37
	movq	16(%rbx), %rax
	movq	%rbp, (%rax)
.L37:
	xorl	%eax, %eax
.L41:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	cmpl	$-4, %edi
	jne	.L32
	leaq	.LC0(%rip), %rax
	testq	%rsi, %rsi
	movl	$10, %edx
	cmove	%rax, %rbp
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	strtol
	movl	%eax, _argp_hang(%rip)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L40:
	movl	$1, %edi
	call	__sleep
.L36:
	movl	_argp_hang(%rip), %eax
	leal	-1(%rax), %edx
	testl	%eax, %eax
	movl	%edx, _argp_hang(%rip)
	jg	.L40
	xorl	%eax, %eax
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$8, %rsp
	movl	$7, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	80(%rdx), %rsi
	movq	%rbx, %rdi
	movl	$513, %edx
	call	__argp_state_help
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rbp, 64(%rbx)
	movq	%rbp, %rax
	jmp	.L27
.LFE75:
	.size	argp_default_parser, .-argp_default_parser
	.p2align 4,,15
	.type	convert_options, @function
convert_options:
.LFB79:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	32(%rdi), %rax
	movq	%rdi, 40(%rsp)
	movq	%rsi, 48(%rsp)
	movl	%edx, 60(%rsp)
	movq	%rcx, 24(%rsp)
	testq	%r12, %r12
	movq	%rax, 32(%rsp)
	movq	%r12, %r15
	je	.L98
	.p2align 4,,10
	.p2align 3
.L43:
	movl	8(%r15), %edx
	testl	%edx, %edx
	jne	.L59
	cmpq	$0, (%r15)
	je	.L99
.L59:
	movl	24(%r15), %eax
	testb	$4, %al
	je	.L68
	movl	24(%r12), %ebx
.L60:
	andl	$8, %ebx
	jne	.L47
	testb	$8, %al
	jne	.L48
	leal	-1(%rdx), %eax
	cmpl	$254, %eax
	ja	.L48
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rsi
	movslq	%edx, %rax
	movq	%fs:(%rsi), %rsi
	testb	$64, 1(%rsi,%rax,2)
	je	.L48
	movq	8(%r14), %rax
	leaq	1(%rax), %rsi
	movq	%rsi, 8(%r14)
	movb	%dl, (%rax)
	cmpq	$0, 16(%r12)
	je	.L50
	movq	8(%r14), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movb	$58, (%rax)
	testb	$1, 24(%r12)
	je	.L50
	movq	8(%r14), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%r14)
	movb	$58, (%rax)
.L50:
	movq	8(%r14), %rax
	movb	$0, (%rax)
	.p2align 4,,10
	.p2align 3
.L48:
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	je	.L47
	movq	(%r14), %rax
	movq	16(%rax), %r13
	movq	%rax, 16(%rsp)
	movq	0(%r13), %rdi
	movq	%r13, 8(%rsp)
	testq	%rdi, %rdi
	je	.L53
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rbp, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L100
	addq	$32, %r13
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	jne	.L55
.L53:
	cmpq	$0, 16(%r12)
	movq	16(%r14), %rdx
	movq	%rbp, (%rdx)
	je	.L56
	xorl	%ebx, %ebx
	testb	$1, 24(%r12)
	setne	%bl
	addl	$1, %ebx
.L56:
	movl	%ebx, 8(%rdx)
	movl	8(%r15), %eax
	movq	$0, 16(%rdx)
	testl	%eax, %eax
	je	.L57
	andl	$16777215, %eax
	movl	%eax, %esi
.L58:
	movq	16(%rsp), %rcx
	movq	24(%rsp), %rax
	subq	80(%rcx), %rax
	movabsq	$-8198552921648689607, %rcx
	sarq	$3, %rax
	imulq	%rcx, %rax
	addl	$1, %eax
	sall	$24, %eax
	addl	%esi, %eax
	movl	%eax, 24(%rdx)
	leaq	32(%rdx), %rax
	movq	%rax, 16(%r14)
	movq	$0, 32(%rdx)
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$48, %r15
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L68:
	movl	%eax, %ebx
	movq	%r15, %r12
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$0, 32(%r15)
	jne	.L59
	movl	40(%r15), %eax
	testl	%eax, %eax
	jne	.L59
	movq	40(%rsp), %rax
	movq	8(%rax), %rax
.L44:
	movq	24(%rsp), %rcx
	movq	32(%rsp), %rsi
	movq	%rax, (%rcx)
	movq	40(%rsp), %rax
	testq	%rsi, %rsi
	movq	%rax, 8(%rcx)
	movq	8(%r14), %rax
	movl	$0, 24(%rcx)
	movq	$0, 48(%rcx)
	movq	$0, 64(%rcx)
	movq	$0, 56(%rcx)
	movq	%rax, 16(%rcx)
	movq	48(%rsp), %rax
	movq	%rax, 32(%rcx)
	movl	60(%rsp), %eax
	movl	%eax, 40(%rcx)
	je	.L61
	cmpq	$0, (%rsi)
	je	.L69
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L63:
	leal	1(%rax), %edx
	movq	%rdx, %rcx
	movq	%rdx, %rax
	salq	$5, %rcx
	cmpq	$0, (%rsi,%rcx)
	jne	.L63
	salq	$3, %rdx
.L62:
	movq	24(%r14), %rax
	movq	24(%rsp), %rcx
	addq	%rax, %rdx
	movq	%rax, 56(%rcx)
	movq	%rdx, 24(%r14)
.L61:
	movq	24(%rsp), %rax
	addq	$72, %rax
.L45:
	movq	32(%rsp), %rbx
	testq	%rbx, %rbx
	je	.L42
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L42
	movq	24(%rsp), %r12
	xorl	%edx, %edx
.L65:
	addq	$32, %rbx
	movq	%r14, %r8
	movq	%rax, %rcx
	movq	%r12, %rsi
	leal	1(%rdx), %ebp
	call	convert_options
	movq	(%rbx), %rdi
	movl	%ebp, %edx
	testq	%rdi, %rdi
	jne	.L65
.L42:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r13, %rdx
	subq	8(%rsp), %rdx
	sarq	$5, %rdx
	testl	%edx, %edx
	jns	.L47
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L57:
	movl	8(%r12), %esi
	andl	$16777215, %esi
	jmp	.L58
.L98:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L44
	movq	%rcx, %rax
	movq	$0, 24(%rsp)
	jmp	.L45
.L69:
	xorl	%edx, %edx
	jmp	.L62
.LFE79:
	.size	convert_options, .-convert_options
	.section	.rodata.str1.1
.LC1:
	.string	"%s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(PROGRAM ERROR) No version known!?"
	.text
	.p2align 4,,15
	.type	argp_version_parser, @function
argp_version_parser:
.LFB76:
	cmpl	$86, %edi
	jne	.L109
	movq	argp_program_version_hook(%rip), %rax
	pushq	%rbx
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L104
	movq	80(%rdx), %rdi
	movq	%rdx, %rsi
	call	*%rax
.L105:
	xorl	%eax, %eax
	testb	$32, 28(%rbx)
	je	.L114
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L114:
	xorl	%edi, %edi
	call	exit
	.p2align 4,,10
	.p2align 3
.L109:
	movl	$7, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	movq	argp_program_version(%rip), %rdx
	testq	%rdx, %rdx
	je	.L106
	movq	80(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	fprintf
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L106:
	movq	(%rbx), %rax
	leaq	.LC2(%rip), %rsi
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	%rbx, %rdi
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__argp_error
	jmp	.L105
.LFE76:
	.size	argp_version_parser, .-argp_version_parser
	.section	.rodata.str1.1
.LC3:
	.string	"???"
.LC4:
	.string	"--"
.LC5:
	.string	"-%c: %s"
.LC6:
	.string	"--%s: %s"
.LC7:
	.string	"%s: Too many arguments\n"
	.text
	.p2align 4,,15
	.globl	__argp_parse
	.type	__argp_parse, @function
__argp_parse:
.LFB87:
	pushq	%rbp
	movl	%ecx, %eax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r12d
	pushq	%rbx
	movq	%rdi, %r15
	movq	%rdx, %r13
	subq	$344, %rsp
	andl	$4, %eax
	movq	%r8, -376(%rbp)
	sete	%r8b
	andl	$16, %ecx
	movl	%esi, -356(%rbp)
	movq	%r9, -368(%rbp)
	movl	%eax, -360(%rbp)
	movzbl	%r8b, %r8d
	je	.L319
	testq	%rdi, %rdi
	movq	%r8, -336(%rbp)
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -312(%rbp)
	jne	.L120
	movl	$104, %r14d
	xorl	%edx, %edx
	movl	$9, %ebx
	movl	$1, %esi
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L319:
	subq	$144, %rsp
	pxor	%xmm0, %xmm0
	leaq	15(%rsp), %rsi
	subq	$80, %rsp
	xorl	%eax, %eax
	leaq	15(%rsp), %rdx
	movl	$16, %ecx
	andq	$-16, %rsi
	andq	$-16, %rdx
	movq	%rsi, %rdi
	testq	%r15, %r15
	movaps	%xmm0, 32(%rdx)
	rep stosq
	movaps	%xmm0, (%rdx)
	movq	%rsi, %rax
	movaps	%xmm0, 16(%rdx)
	movq	$0, 48(%rdx)
	movq	%rsi, 32(%rdx)
	je	.L117
	addq	$32, %rax
	movq	%r15, (%rsi)
.L117:
	cmpq	$0, argp_program_version(%rip)
	leaq	argp_default_argp(%rip), %rcx
	movq	%rcx, (%rax)
	je	.L320
.L118:
	leaq	argp_version_argp(%rip), %rdi
	leaq	64(%rax), %rcx
	movq	%rdi, 32(%rax)
.L119:
	movq	$0, (%rcx)
	movq	%r8, -336(%rbp)
	movq	%rdx, %r15
	movq	$0, -328(%rbp)
	movq	$0, -320(%rbp)
	movq	$0, -312(%rbp)
.L120:
	leaq	-336(%rbp), %rsi
	movq	%r15, %rdi
	call	calc_sizes
	movq	-320(%rbp), %rax
	movq	-312(%rbp), %rdx
	movq	-336(%rbp), %r8
	leaq	1(%rax), %rsi
	movq	-328(%rbp), %rax
	leaq	(%rsi,%rsi,8), %rbx
	leaq	4(%rbx,%rax,4), %rcx
	addq	%rdx, %rcx
	leaq	0(,%rcx,8), %r14
.L121:
	leaq	1(%r8,%r14), %rdi
	movq	%rsi, -352(%rbp)
	movq	%rdx, -344(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	movq	-344(%rbp), %rdx
	movq	-352(%rbp), %rsi
	je	.L321
	leaq	(%rsi,%rsi,8), %rcx
	leaq	(%rbx,%rdx), %r9
	addq	%rax, %r14
	movq	%rax, -192(%rbp)
	salq	$3, %rdx
	xorl	%esi, %esi
	leaq	(%rax,%rcx,8), %r8
	leaq	(%rax,%r9,8), %r9
	movabsq	$4294967297, %rax
	movq	%r14, -264(%rbp)
	movq	%rax, -248(%rbp)
	movq	%r8, %rdi
	movq	%r8, -176(%rbp)
	movq	%r9, -256(%rbp)
	movq	%r9, -352(%rbp)
	movl	$0, -240(%rbp)
	movq	$0, -232(%rbp)
	movl	$0, -224(%rbp)
	movq	$0, -216(%rbp)
	movq	$0, -208(%rbp)
	movl	$0, -200(%rbp)
	call	memset@PLT
	movq	-352(%rbp), %r9
	movq	%rax, %r8
	leaq	-272(%rbp), %rax
	testb	$8, %r12b
	movq	%r14, -296(%rbp)
	movq	%r8, -280(%rbp)
	movq	%rax, -344(%rbp)
	movq	%rax, -304(%rbp)
	movq	%r9, -288(%rbp)
	je	.L124
	leaq	1(%r14), %rax
	movb	$45, (%r14)
	movq	%rax, -296(%rbp)
.L125:
	movb	$0, (%rax)
	movq	-288(%rbp), %rax
	testq	%r15, %r15
	movq	$0, (%rax)
	movq	%r15, -272(%rbp)
	je	.L126
	movq	-192(%rbp), %rcx
	leaq	-304(%rbp), %r8
	movq	%r15, %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	convert_options
	movq	-272(%rbp), %r15
	movq	-192(%rbp), %r14
	movq	%rax, -184(%rbp)
.L127:
	movq	-344(%rbp), %rcx
	pxor	%xmm0, %xmm0
	movq	stderr(%rip), %rdx
	movl	-356(%rbp), %edi
	cmpq	%r14, %rax
	movq	%r15, -160(%rbp)
	movl	$1, -168(%rbp)
	movups	%xmm0, -148(%rbp)
	movq	$0, 172(%rcx)
	movl	$0, 180(%rcx)
	movups	%xmm0, 140(%rcx)
	movups	%xmm0, 156(%rcx)
	movq	%rdx, -88(%rbp)
	movq	stdout(%rip), %rdx
	movl	%edi, -152(%rbp)
	movq	%r13, -144(%rbp)
	movl	%r12d, -132(%rbp)
	movq	%rcx, -72(%rbp)
	movq	%rdx, -80(%rbp)
	ja	.L322
	testb	$2, %r12b
	movq	%r13, %rax
	je	.L138
.L334:
	andl	$1, %r12d
	movl	$0, -244(%rbp)
	jne	.L323
.L139:
	cmpq	%rax, %r13
	je	.L324
.L140:
	movq	program_invocation_short_name(%rip), %rax
	movq	%rax, -96(%rbp)
.L180:
	movslq	-136(%rbp), %rdx
	movl	-152(%rbp), %r10d
	movl	-168(%rbp), %ecx
.L143:
	movl	-124(%rbp), %eax
	testl	%eax, %eax
	je	.L144
.L328:
	cmpl	%edx, %eax
	jle	.L145
	movl	$0, -124(%rbp)
.L144:
	testl	%ecx, %ecx
	jne	.L325
.L145:
	cmpl	%r10d, %edx
	jge	.L151
	testb	$4, -132(%rbp)
	jne	.L152
	movq	-144(%rbp), %rax
	leal	1(%rdx), %r14d
	movq	(%rax,%rdx,8), %r13
	movq	%r13, -232(%rbp)
.L219:
	movq	-192(%rbp), %rbx
	cmpq	-184(%rbp), %rbx
	leal	-1(%r14), %eax
	movl	%eax, -352(%rbp)
	movl	%eax, -136(%rbp)
	jnb	.L154
	movq	-344(%rbp), %rcx
	leaq	112(%rcx), %r12
.L158:
	movq	(%rbx), %r8
	leal	1(%rax), %edx
	movl	%edx, -136(%rbp)
	testq	%r8, %r8
	je	.L155
	movq	64(%rbx), %rax
	xorl	%edi, %edi
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rax, -104(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -120(%rbp)
	movq	56(%rbx), %rax
	movq	%rax, -112(%rbp)
	movl	24(%rbx), %eax
	movl	%eax, -128(%rbp)
	call	*%r8
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	cmpl	$7, %r15d
	movq	%rax, 64(%rbx)
	je	.L326
	testl	%r15d, %r15d
	leaq	72(%rbx), %r14
	jne	.L181
	movslq	-136(%rbp), %rdx
	movl	-152(%rbp), %r10d
.L161:
	movl	-352(%rbp), %ecx
	cmpl	%edx, %ecx
	jl	.L327
	movl	-124(%rbp), %eax
	movl	$1, -168(%rbp)
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.L328
	testl	%ecx, %ecx
	je	.L145
	.p2align 4,,10
	.p2align 3
.L325:
	testb	$64, -132(%rbp)
	movl	%edx, -248(%rbp)
	movl	$-1, -240(%rbp)
	movq	-256(%rbp), %rcx
	movq	-264(%rbp), %rdx
	movq	-144(%rbp), %rsi
	je	.L329
	movq	-344(%rbp), %rax
	xorl	%r8d, %r8d
	movl	%r10d, %edi
	leaq	24(%rax), %r9
	call	_getopt_long_only_r@PLT
	movl	%eax, %r12d
.L146:
	movslq	-248(%rbp), %rdx
	cmpl	$-1, %r12d
	movl	%edx, -136(%rbp)
	je	.L330
	cmpl	$63, %r12d
	je	.L331
	cmpl	$1, %r12d
	jne	.L218
	movq	-232(%rbp), %r13
	movl	%edx, %r14d
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L124:
	movl	-360(%rbp), %edx
	movq	%r14, %rax
	testl	%edx, %edx
	je	.L125
	addq	$1, %rax
	movb	$43, (%r14)
	movq	%rax, -296(%rbp)
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L326:
	movq	(%rbx), %r8
	movl	-136(%rbp), %eax
	leaq	72(%rbx), %r14
	subl	$1, %eax
	testq	%r8, %r8
	movl	%eax, -136(%rbp)
	je	.L332
	movq	64(%rbx), %rax
	xorl	%esi, %esi
	movq	%r12, %rdx
	movl	$16777222, %edi
	movq	%rax, -104(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, -120(%rbp)
	movq	56(%rbx), %rax
	movq	%rax, -112(%rbp)
	movl	24(%rbx), %eax
	movl	%eax, -128(%rbp)
	call	*%r8
	cmpq	%r14, -184(%rbp)
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, 64(%rbx)
	jbe	.L160
	cmpl	$7, %r15d
	jne	.L160
	movl	-136(%rbp), %eax
	movq	%r14, %rbx
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L322:
	movq	-368(%rbp), %rax
	leaq	112(%rcx), %r12
	movq	%rax, 48(%r14)
	.p2align 4,,10
	.p2align 3
.L129:
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.L130
	movl	40(%r14), %edx
	movq	56(%rax), %rax
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 48(%r14)
.L130:
	movq	(%r14), %rax
	addq	$72, %r14
	testq	%rax, %rax
	je	.L333
.L131:
	movq	-8(%r14), %rdx
	xorl	%esi, %esi
	movl	$16777219, %edi
	movq	%rdx, -104(%rbp)
	movq	-24(%r14), %rdx
	movq	%rdx, -120(%rbp)
	movq	-16(%r14), %rdx
	movq	%rdx, -112(%rbp)
	movl	-48(%r14), %edx
	movl	%edx, -128(%rbp)
	movq	%r12, %rdx
	call	*%rax
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, -8(%r14)
	cmpq	%r14, -184(%rbp)
	jbe	.L134
	testl	%r15d, %r15d
	je	.L129
	cmpl	$7, %r15d
	je	.L129
.L115:
	leaq	-40(%rbp), %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	movq	-64(%r14), %rax
	movq	32(%rax), %rax
	testq	%rax, %rax
	je	.L132
	cmpq	$0, (%rax)
	je	.L132
	movq	-16(%r14), %rax
	movq	-24(%r14), %rdx
	movq	%rdx, (%rax)
	movq	-72(%r14), %rax
	testq	%rax, %rax
	jne	.L131
	.p2align 4,,10
	.p2align 3
.L132:
	cmpq	%r14, -184(%rbp)
	ja	.L129
.L313:
	movl	-132(%rbp), %r12d
	movq	-144(%rbp), %rax
.L336:
	testb	$2, %r12b
	jne	.L334
.L138:
	cmpq	%rax, %r13
	movl	$1, -244(%rbp)
	jne	.L140
.L324:
	movq	0(%r13), %r12
	testq	%r12, %r12
	je	.L140
	movq	%r12, %rdi
	movl	$47, %esi
	call	strrchr
	leaq	1(%rax), %rdx
	testq	%rax, %rax
	cmovne	%rdx, %r12
	movq	%r12, -96(%rbp)
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%eax, -136(%rbp)
	addq	$72, %rbx
.L226:
	cmpq	-184(%rbp), %rbx
	jb	.L158
.L154:
	movl	-136(%rbp), %edx
	movl	-152(%rbp), %r10d
.L151:
	cmpl	%r10d, %edx
	jne	.L152
	movq	-192(%rbp), %r14
	movq	-184(%rbp), %r12
	xorl	%r15d, %r15d
	movq	-344(%rbp), %rax
	cmpq	%r12, %r14
	leaq	112(%rax), %r13
	jnb	.L335
	.p2align 4,,10
	.p2align 3
.L182:
	movl	24(%r14), %eax
	addq	$72, %r14
	testl	%eax, %eax
	jne	.L184
	movq	-72(%r14), %rax
	testq	%rax, %rax
	je	.L185
	movq	-8(%r14), %rdx
	xorl	%esi, %esi
	movl	$16777218, %edi
	movq	%rdx, -104(%rbp)
	movq	-24(%r14), %rdx
	movq	%rdx, -120(%rbp)
	movq	-16(%r14), %rdx
	movl	$0, -128(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%r13, %rdx
	call	*%rax
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, -8(%r14)
	movq	-184(%rbp), %r12
.L184:
	cmpq	%r14, %r12
	jbe	.L186
	testl	%r15d, %r15d
	je	.L182
	cmpl	$7, %r15d
	je	.L182
.L196:
	movq	-376(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L199
	movl	-136(%rbp), %eax
	movl	%eax, (%rcx)
.L199:
	testl	%r15d, %r15d
	je	.L198
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L329:
	movq	-344(%rbp), %rax
	xorl	%r8d, %r8d
	movl	%r10d, %edi
	leaq	24(%rax), %r9
	call	_getopt_long_r@PLT
	movl	%eax, %r12d
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-192(%rbp), %r14
	movq	%r14, -184(%rbp)
	movq	%r14, %rax
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L327:
	movl	%edx, %eax
	subl	%ecx, %eax
	movl	-168(%rbp), %ecx
	addl	%eax, -48(%r14)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L321:
	movl	$12, %r15d
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L134:
	cmpl	$7, %r15d
	je	.L313
	testl	%r15d, %r15d
	jne	.L115
	movl	-132(%rbp), %r12d
	movq	-144(%rbp), %rax
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L152:
	movq	-376(%rbp), %rax
	testq	%rax, %rax
	je	.L200
	movq	-184(%rbp), %r12
	movl	%edx, (%rax)
.L198:
	subq	$72, %r12
	cmpq	-192(%rbp), %r12
	movq	-344(%rbp), %rax
	leaq	112(%rax), %r13
	jb	.L213
	.p2align 4,,10
	.p2align 3
.L206:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L208
	movq	64(%r12), %rdx
	xorl	%esi, %esi
	movl	$16777220, %edi
	subq	$72, %r12
	movq	%rdx, -104(%rbp)
	movq	120(%r12), %rdx
	movq	%rdx, -120(%rbp)
	movq	128(%r12), %rdx
	movq	%rdx, -112(%rbp)
	movl	96(%r12), %edx
	movl	%edx, -128(%rbp)
	movq	%r13, %rdx
	call	*%rax
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, 136(%r12)
	movq	-192(%rbp), %rax
	cmpq	%rax, %r12
	jb	.L209
	testl	%r15d, %r15d
	je	.L206
	cmpl	$7, %r15d
	je	.L206
	movq	-184(%rbp), %rcx
	leaq	-72(%rcx), %r12
	cmpq	%rax, %r12
	jb	.L217
	.p2align 4,,10
	.p2align 3
.L211:
	movq	-344(%rbp), %rbx
	addq	$112, %rbx
	.p2align 4,,10
	.p2align 3
.L216:
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.L215
	movq	64(%r12), %rdx
	xorl	%esi, %esi
	movl	$16777223, %edi
	movq	%rdx, -104(%rbp)
	movq	48(%r12), %rdx
	movq	%rdx, -120(%rbp)
	movq	56(%r12), %rdx
	movq	%rdx, -112(%rbp)
	movl	24(%r12), %edx
	movl	%edx, -128(%rbp)
	movq	%rbx, %rdx
	call	*%rax
	movq	-104(%rbp), %rax
	movq	%rax, 64(%r12)
.L215:
	subq	$72, %r12
	cmpq	-192(%rbp), %r12
	jnb	.L216
.L214:
	cmpl	$7, %r15d
	movl	$22, %eax
	cmove	%eax, %r15d
.L217:
	movq	-64(%rbp), %rdi
	call	free@PLT
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L330:
	cmpl	$1, %edx
	movl	$0, -168(%rbp)
	movl	-152(%rbp), %r10d
	jle	.L145
	movq	-144(%rbp), %rcx
	movslq	%edx, %rax
	leaq	.LC4(%rip), %rdi
	movq	-8(%rcx,%rax,8), %rsi
	movl	$3, %ecx
	repz cmpsb
	jne	.L145
	movl	%edx, -124(%rbp)
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L323:
	subq	$8, %rax
	addl	$1, -152(%rbp)
	movq	%rax, -144(%rbp)
	jmp	.L139
.L200:
	testb	$2, -132(%rbp)
	movq	-88(%rbp), %rsi
	je	.L337
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-344(%rbp), %rax
	movl	$260, %edx
	movl	$7, %r15d
	leaq	112(%rax), %rdi
	call	__argp_state_help
.L316:
	movq	-184(%rbp), %r12
.L201:
	movq	-192(%rbp), %r13
	cmpq	%r12, %r13
	jnb	.L202
	movq	-344(%rbp), %rax
	leaq	112(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L204:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L203
	movq	64(%r13), %rdx
	xorl	%esi, %esi
	movl	$16777221, %edi
	movq	%rdx, -104(%rbp)
	movq	48(%r13), %rdx
	movq	%rdx, -120(%rbp)
	movq	56(%r13), %rdx
	movq	%rdx, -112(%rbp)
	movl	24(%r13), %edx
	movl	%edx, -128(%rbp)
	movq	%r14, %rdx
	call	*%rax
	movq	-104(%rbp), %rax
	movq	%rax, 64(%r13)
	movq	-184(%rbp), %r12
.L203:
	addq	$72, %r13
	cmpq	%r12, %r13
	jb	.L204
	movq	-192(%rbp), %r13
	subq	$72, %r12
.L205:
	cmpq	%r12, %r13
	jbe	.L211
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L331:
	cmpl	$-1, -240(%rbp)
	jne	.L149
.L150:
	movq	-264(%rbp), %rdi
	movl	%r12d, %esi
	call	strchr
	testq	%rax, %rax
	je	.L168
	movq	-192(%rbp), %r13
	movq	-184(%rbp), %rdx
	cmpq	%rdx, %r13
	jb	.L172
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$72, %r13
	cmpq	%rdx, %r13
	jnb	.L168
.L172:
	cmpq	16(%r13), %rax
	jnb	.L170
	movq	0(%r13), %rax
	movq	-232(%rbp), %rsi
	testq	%rax, %rax
	je	.L168
	movq	64(%r13), %rdx
	movq	-344(%rbp), %rcx
	movl	%r12d, %edi
	xorl	%r14d, %r14d
	movq	%rdx, -104(%rbp)
	movq	48(%r13), %rdx
	movq	%rdx, -120(%rbp)
	movq	56(%r13), %rdx
	movq	%rdx, -112(%rbp)
	movl	24(%r13), %edx
	movl	%edx, -128(%rbp)
	leaq	112(%rcx), %rdx
	call	*%rax
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, 64(%r13)
	jmp	.L171
.L339:
	testl	%r14d, %r14d
	je	.L168
.L173:
	movq	-256(%rbp), %r13
	cmpl	24(%r13), %r12d
	jne	.L315
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L338:
	addq	$32, %r13
	cmpl	24(%r13), %r12d
	je	.L175
.L315:
	cmpq	$0, 0(%r13)
	jne	.L338
.L175:
	movq	-272(%rbp), %rax
	leaq	bad_key_err.10388(%rip), %rsi
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	0(%r13), %rdx
	leaq	.LC3(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	testq	%rdx, %rdx
	cmove	%rcx, %rdx
	movq	-344(%rbp), %rcx
	addl	$1, %r12d
	andl	$-3, %r12d
	leaq	112(%rcx), %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__argp_error
.L167:
	testl	%r12d, %r12d
	je	.L154
.L149:
	movq	-88(%rbp), %rsi
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L218:
	movl	%r12d, %r14d
	sarl	$24, %r14d
	testl	%r14d, %r14d
	je	.L150
	movslq	%r14d, %rax
	movl	%r12d, %edi
	movq	-232(%rbp), %rsi
	leaq	(%rax,%rax,8), %rdx
	movq	-192(%rbp), %rax
	sall	$8, %edi
	sarl	$8, %edi
	leaq	-72(%rax,%rdx,8), %r13
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L173
	movq	64(%r13), %rdx
	movq	-344(%rbp), %rcx
	movq	%rdx, -104(%rbp)
	movq	48(%r13), %rdx
	movq	%rdx, -120(%rbp)
	movq	56(%r13), %rdx
	movq	%rdx, -112(%rbp)
	movl	24(%r13), %edx
	movl	%edx, -128(%rbp)
	leaq	112(%rcx), %rdx
	call	*%rax
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, 64(%r13)
.L171:
	cmpl	$7, %r15d
	je	.L339
	testl	%r15d, %r15d
	je	.L180
.L181:
	cmpl	$7, %r15d
	jne	.L316
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L208:
	movq	-192(%rbp), %rax
	subq	$72, %r12
	cmpq	%rax, %r12
	jnb	.L206
	movl	$7, %r15d
	.p2align 4,,10
	.p2align 3
.L209:
	movq	-184(%rbp), %rcx
	cmpl	$7, %r15d
	leaq	-72(%rcx), %r12
	jne	.L212
	xorl	%r15d, %r15d
	cmpq	%r12, %rax
	jbe	.L211
.L213:
	xorl	%r15d, %r15d
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L320:
	cmpq	$0, argp_program_version_hook(%rip)
	leaq	32(%rax), %rcx
	jne	.L118
	jmp	.L119
.L168:
	movq	-272(%rbp), %rax
	leaq	bad_key_err.10388(%rip), %rsi
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	-344(%rbp), %rcx
	leaq	.LC5(%rip), %rsi
	movl	%r12d, %edx
	addl	$1, %r12d
	andl	$-3, %r12d
	leaq	112(%rcx), %rdi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__argp_error
	jmp	.L167
.L337:
	testq	%rsi, %rsi
	je	.L179
	movq	-272(%rbp), %rax
	leaq	.LC7(%rip), %rsi
	movq	-96(%rbp), %r12
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	-88(%rbp), %rdi
	movq	%rax, %rsi
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	fprintf
	movq	-88(%rbp), %rsi
	jmp	.L179
.L186:
	leaq	-72(%r12), %r13
	cmpq	-192(%rbp), %r13
	jb	.L191
	cmpl	$7, %r15d
	je	.L189
	testl	%r15d, %r15d
	jne	.L196
.L189:
	movq	-344(%rbp), %rax
	leaq	112(%rax), %r12
	.p2align 4,,10
	.p2align 3
.L193:
	movq	0(%r13), %rax
	testq	%rax, %rax
	je	.L194
	movq	64(%r13), %rdx
	xorl	%esi, %esi
	movl	$16777217, %edi
	subq	$72, %r13
	movq	%rdx, -104(%rbp)
	movq	120(%r13), %rdx
	movq	%rdx, -120(%rbp)
	movq	128(%r13), %rdx
	movq	%rdx, -112(%rbp)
	movl	96(%r13), %edx
	movl	%edx, -128(%rbp)
	movq	%r12, %rdx
	call	*%rax
	movl	%eax, %r15d
	movq	-104(%rbp), %rax
	movq	%rax, 136(%r13)
	cmpq	-192(%rbp), %r13
	jb	.L340
	testl	%r15d, %r15d
	je	.L193
	cmpl	$7, %r15d
	je	.L193
	movq	-184(%rbp), %r12
	jmp	.L196
.L340:
	movq	-184(%rbp), %r12
.L191:
	cmpl	$7, %r15d
	jne	.L196
.L227:
	movq	-376(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L198
.L317:
	movl	-136(%rbp), %eax
	movl	%eax, (%rcx)
	jmp	.L198
.L194:
	subq	$72, %r13
	cmpq	-192(%rbp), %r13
	jnb	.L193
	movq	-184(%rbp), %r12
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L202:
	subq	$72, %r12
	jmp	.L205
.L212:
	cmpq	%r12, %rax
	jbe	.L211
	jmp	.L217
.L185:
	cmpq	%r14, %r12
	jbe	.L341
	movl	$7, %r15d
	jmp	.L182
.L335:
	leaq	-72(%r12), %r13
	cmpq	%r13, %r14
	jbe	.L189
	movq	-376(%rbp), %rcx
	testq	%rcx, %rcx
	jne	.L317
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L341:
	leaq	-72(%r12), %r13
	cmpq	-192(%rbp), %r13
	jnb	.L189
	jmp	.L227
.L332:
	movq	%r14, %rbx
	jmp	.L226
.L160:
	testl	%r15d, %r15d
	jne	.L162
	movslq	-152(%rbp), %rdx
	movl	%edx, -136(%rbp)
	movl	%edx, %r10d
	jmp	.L161
.L162:
	cmpl	$7, %r15d
	je	.L154
	jmp	.L316
.LFE87:
	.size	__argp_parse, .-__argp_parse
	.weak	argp_parse
	.set	argp_parse,__argp_parse
	.p2align 4,,15
	.globl	__argp_input
	.hidden	__argp_input
	.type	__argp_input, @function
__argp_input:
.LFB88:
	testq	%rsi, %rsi
	je	.L348
	movq	88(%rsi), %rdx
	movq	80(%rdx), %rax
	movq	88(%rdx), %rdx
	cmpq	%rdx, %rax
	jnb	.L348
	cmpq	8(%rax), %rdi
	jne	.L345
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L346:
	cmpq	%rdi, 8(%rax)
	je	.L344
.L345:
	addq	$72, %rax
	cmpq	%rdx, %rax
	jb	.L346
.L348:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	movq	48(%rax), %rax
	ret
.LFE88:
	.size	__argp_input, .-__argp_input
	.weak	_argp_input
	.set	_argp_input,__argp_input
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	bad_key_err.10388, @object
	.size	bad_key_err.10388, 53
bad_key_err.10388:
	.string	"(PROGRAM ERROR) Option should have been recognized!?"
	.section	.rodata.str1.1
.LC8:
	.string	"libc"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	argp_version_argp, @object
	.size	argp_version_argp, 56
argp_version_argp:
	.quad	argp_version_options
	.quad	argp_version_parser
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC8
	.section	.rodata.str1.1
.LC9:
	.string	"version"
.LC10:
	.string	"Print program version"
	.section	.data.rel.ro.local
	.align 32
	.type	argp_version_options, @object
	.size	argp_version_options, 96
argp_version_options:
	.quad	.LC9
	.long	86
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.quad	.LC10
	.long	-1
	.zero	4
	.quad	0
	.long	0
	.zero	36
	.align 32
	.type	argp_default_argp, @object
	.size	argp_default_argp, 56
argp_default_argp:
	.quad	argp_default_options
	.quad	argp_default_parser
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	.LC8
	.section	.rodata.str1.1
.LC11:
	.string	"help"
.LC12:
	.string	"Give this help list"
.LC13:
	.string	"usage"
.LC14:
	.string	"Give a short usage message"
.LC15:
	.string	"program-name"
.LC16:
	.string	"NAME"
.LC17:
	.string	"Set the program name"
.LC18:
	.string	"HANG"
.LC19:
	.string	"SECS"
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"Hang for SECS seconds (default 3600)"
	.section	.data.rel.ro.local
	.align 32
	.type	argp_default_options, @object
	.size	argp_default_options, 240
argp_default_options:
	.quad	.LC11
	.long	63
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.quad	.LC12
	.long	-1
	.zero	4
	.quad	.LC13
	.long	-3
	.zero	4
	.quad	0
	.long	0
	.zero	4
	.quad	.LC14
	.zero	8
	.quad	.LC15
	.long	-2
	.zero	4
	.quad	.LC16
	.long	2
	.zero	4
	.quad	.LC17
	.zero	8
	.quad	.LC18
	.long	-4
	.zero	4
	.quad	.LC19
	.long	3
	.zero	4
	.quad	.LC20
	.zero	8
	.quad	0
	.long	0
	.zero	36
	.local	_argp_hang
	.comm	_argp_hang,4,4
	.hidden	strchr
	.hidden	__argp_error
	.hidden	__dcgettext
	.hidden	fprintf
	.hidden	exit
	.hidden	strcmp
	.hidden	__sleep
	.hidden	strtol
	.hidden	strrchr
	.hidden	__argp_state_help
