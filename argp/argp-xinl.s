	.text
	.p2align 4,,15
	.globl	__argp_usage
	.type	__argp_usage, @function
__argp_usage:
	movq	stderr(%rip), %rsi
	movl	$262, %edx
	jmp	__argp_state_help
	.size	__argp_usage, .-__argp_usage
	.weak	argp_usage
	.set	argp_usage,__argp_usage
	.p2align 4,,15
	.globl	__option_is_short
	.type	__option_is_short, @function
__option_is_short:
	movl	24(%rdi), %eax
	andl	$8, %eax
	jne	.L5
	movslq	8(%rdi), %rdx
	leal	-1(%rdx), %ecx
	cmpl	$254, %ecx
	ja	.L3
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzwl	(%rax,%rdx,2), %eax
	shrw	$14, %ax
	andl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
.L3:
	rep ret
	.size	__option_is_short, .-__option_is_short
	.weak	_option_is_short
	.set	_option_is_short,__option_is_short
	.p2align 4,,15
	.globl	__option_is_end
	.type	__option_is_end, @function
__option_is_end:
	movl	8(%rdi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L6
	cmpq	$0, (%rdi)
	je	.L11
.L6:
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	$0, 32(%rdi)
	jne	.L6
	movl	40(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	ret
	.size	__option_is_end, .-__option_is_end
	.weak	_option_is_end
	.set	_option_is_end,__option_is_end
	.hidden	__argp_state_help
