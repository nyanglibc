	.text
	.p2align 4,,15
	.globl	__argp_fmtstream_write
	.hidden	__argp_fmtstream_write
	.type	__argp_fmtstream_write, @function
__argp_fmtstream_write:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	movq	56(%rdi), %rdi
	movq	%rdx, %rbp
	leaq	(%rdi,%rdx), %rax
	cmpq	%rax, 64(%rbx)
	jnb	.L2
	movq	%rdx, %rsi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L1
	movq	56(%rbx), %rdi
.L2:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	addq	%rbp, 56(%rbx)
	movq	%rbp, %rax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__argp_fmtstream_write, .-__argp_fmtstream_write
	.p2align 4,,15
	.globl	__argp_fmtstream_puts
	.type	__argp_fmtstream_puts, @function
__argp_fmtstream_puts:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	call	strlen
	movq	%rax, %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L8
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__argp_fmtstream_write
	cmpq	%rax, %rbx
	setne	%al
	movzbl	%al, %eax
	negl	%eax
.L8:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__argp_fmtstream_puts, .-__argp_fmtstream_puts
	.p2align 4,,15
	.globl	__argp_fmtstream_putc
	.type	__argp_fmtstream_putc, @function
__argp_fmtstream_putc:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %ebx
	subq	$8, %rsp
	movq	56(%rdi), %rax
	cmpq	64(%rdi), %rax
	jnb	.L19
.L15:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbp)
	movb	%bl, (%rax)
	movsbl	%bl, %eax
.L14:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %esi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L17
	movq	56(%rbp), %rax
	jmp	.L15
.L17:
	movl	$-1, %eax
	jmp	.L14
	.size	__argp_fmtstream_putc, .-__argp_fmtstream_putc
	.p2align 4,,15
	.globl	__argp_fmtstream_set_lmargin
	.type	__argp_fmtstream_set_lmargin, @function
__argp_fmtstream_set_lmargin:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	56(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	32(%rdi), %rax
	jbe	.L21
	movq	%rsi, 8(%rsp)
	call	__argp_fmtstream_update
	movq	8(%rsp), %rsi
.L21:
	movq	8(%rbx), %rax
	movq	%rsi, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__argp_fmtstream_set_lmargin, .-__argp_fmtstream_set_lmargin
	.p2align 4,,15
	.globl	__argp_fmtstream_set_rmargin
	.type	__argp_fmtstream_set_rmargin, @function
__argp_fmtstream_set_rmargin:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	56(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	32(%rdi), %rax
	jbe	.L24
	movq	%rsi, 8(%rsp)
	call	__argp_fmtstream_update
	movq	8(%rsp), %rsi
.L24:
	movq	16(%rbx), %rax
	movq	%rsi, 16(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__argp_fmtstream_set_rmargin, .-__argp_fmtstream_set_rmargin
	.p2align 4,,15
	.globl	__argp_fmtstream_set_wmargin
	.type	__argp_fmtstream_set_wmargin, @function
__argp_fmtstream_set_wmargin:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	56(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	32(%rdi), %rax
	jbe	.L27
	movq	%rsi, 8(%rsp)
	call	__argp_fmtstream_update
	movq	8(%rsp), %rsi
.L27:
	movq	24(%rbx), %rax
	movq	%rsi, 24(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__argp_fmtstream_set_wmargin, .-__argp_fmtstream_set_wmargin
	.p2align 4,,15
	.globl	__argp_fmtstream_point
	.type	__argp_fmtstream_point, @function
__argp_fmtstream_point:
	movq	56(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	32(%rdi), %rax
	pushq	%rbx
	movq	%rdi, %rbx
	jbe	.L30
	call	__argp_fmtstream_update
.L30:
	cmpq	$0, 40(%rbx)
	movl	$0, %eax
	cmovns	40(%rbx), %rax
	popq	%rbx
	ret
	.size	__argp_fmtstream_point, .-__argp_fmtstream_point
	.hidden	__argp_fmtstream_update
	.hidden	strlen
	.hidden	__argp_fmtstream_ensure
