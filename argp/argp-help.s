	.text
	.p2align 4,,15
	.type	hol_entry_short_iterate, @function
hol_entry_short_iterate:
.LFB95:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	8(%rdi), %ebp
	movq	(%rdi), %r15
	movq	16(%rdi), %r9
	testl	%ebp, %ebp
	je	.L8
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%r15, %rbx
	.p2align 4,,10
	.p2align 3
.L6:
	movl	24(%rbx), %edi
	movl	%edi, %eax
	andl	$8, %eax
	jne	.L9
	movl	8(%rbx), %edx
	movl	$1, %r8d
	leal	-1(%rdx), %ecx
	cmpl	$254, %ecx
	ja	.L3
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rsi
	movslq	%edx, %rcx
	movq	%fs:(%rsi), %rsi
	testb	$64, 1(%rsi,%rcx,2)
	je	.L3
	movsbl	(%r9), %ecx
	cmpl	%edx, %ecx
	je	.L22
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$48, %rbx
	subl	$1, %ebp
	je	.L1
	testb	%r8b, %r8b
	jne	.L6
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %r8d
	xorl	%eax, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	testb	$4, %dil
	cmove	%rbx, %r15
	andl	$2, %edi
	jne	.L5
	movq	%r9, 8(%rsp)
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%r12
	movq	8(%rsp), %r9
	testl	%eax, %eax
	sete	%r8b
.L5:
	addq	$1, %r9
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.LFE95:
	.size	hol_entry_short_iterate, .-hol_entry_short_iterate
	.p2align 4,,15
	.type	canon_doc_option, @function
canon_doc_option:
.LFB106:
	movq	(%rdi), %rdx
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	(%rdx), %rsi
	movq	%fs:(%rax), %r8
	testb	$32, 1(%r8,%rsi,2)
	movq	%rsi, %rcx
	je	.L24
	leaq	1(%rdx), %rax
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rax, (%rdi)
	movsbq	(%rax), %rsi
	movq	%rax, %rdx
	addq	$1, %rax
	testb	$32, 1(%r8,%rsi,2)
	movq	%rsi, %rcx
	jne	.L25
.L24:
	xorl	%eax, %eax
	cmpb	$45, %cl
	setne	%al
	testb	%cl, %cl
	je	.L23
	testb	$8, (%r8,%rsi,2)
	jne	.L23
	addq	$1, %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L40:
	addq	$1, %rdx
	testb	$8, (%r8,%rcx,2)
	jne	.L23
.L27:
	movq	%rdx, (%rdi)
	movsbq	(%rdx), %rcx
	testb	%cl, %cl
	jne	.L40
.L23:
	rep ret
.LFE106:
	.size	canon_doc_option, .-canon_doc_option
	.p2align 4,,15
	.type	hol_free, @function
hol_free:
.LFB94:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	24(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L42
	.p2align 4,,10
	.p2align 3
.L43:
	movq	40(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L43
.L42:
	movl	8(%rbp), %eax
	testl	%eax, %eax
	je	.L44
	movq	0(%rbp), %rdi
	call	free@PLT
	movq	16(%rbp), %rdi
	call	free@PLT
.L44:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
.LFE94:
	.size	hol_free, .-hol_free
	.p2align 4,,15
	.type	indent_to, @function
indent_to:
.LFB111:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	56(%rdi), %rax
	subq	48(%rdi), %rax
	cmpq	32(%rdi), %rax
	ja	.L60
.L51:
	cmpq	$0, 40(%rbx)
	movl	$0, %eax
	cmovns	40(%rbx), %rax
	subl	%eax, %ebp
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movb	$32, (%rax)
.L54:
	subl	$1, %ebp
.L52:
	testl	%ebp, %ebp
	jle	.L61
	movq	56(%rbx), %rax
	cmpq	64(%rbx), %rax
	jb	.L53
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L54
	movq	56(%rbx), %rax
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L61:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	call	__argp_fmtstream_update
	jmp	.L51
.LFE111:
	.size	indent_to, .-indent_to
	.p2align 4,,15
	.type	argp_doc, @function
argp_doc:
.LFB126:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	$5, %edx
	movq	%r9, %rbx
	subq	$56, %rsp
	movq	%rsi, 8(%rsp)
	movq	24(%rdi), %rsi
	movq	48(%rdi), %rdi
	movl	%ecx, 4(%rsp)
	movl	%r8d, 40(%rsp)
	call	__dcgettext
	testq	%rax, %rax
	movq	%rax, %r13
	movq	32(%r12), %rbp
	je	.L166
	movl	$11, %esi
	movq	%rax, %rdi
	call	strchr
	testl	%r14d, %r14d
	movq	40(%r12), %rcx
	jne	.L167
	testq	%rax, %rax
	je	.L72
	subq	%r13, %rax
	setne	24(%rsp)
	testq	%rcx, %rcx
	movq	%rax, 32(%rsp)
	je	.L109
	testq	%rax, %rax
	je	.L110
	movq	%r13, %rdi
	movq	%rax, %rsi
	call	__strndup
	testq	%rax, %rax
	movq	%rax, %r13
	movb	$1, 24(%rsp)
	setne	46(%rsp)
.L74:
	movq	8(%rsp), %rsi
	movq	%r12, %rdi
	call	__argp_input
	movq	%rax, 16(%rsp)
	movq	40(%r12), %rax
	movl	$33554433, %edi
.L75:
	movq	16(%rsp), %rdx
	movq	%r13, %rsi
	xorl	%r15d, %r15d
	call	*%rax
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L76
.L73:
	movl	4(%rsp), %ecx
	cmpq	%r8, %r13
	movq	56(%rbx), %r15
	sete	%dl
	andb	24(%rsp), %dl
	testl	%ecx, %ecx
	je	.L77
.L108:
	cmpq	%r15, 64(%rbx)
	jbe	.L168
.L78:
	leaq	1(%r15), %rax
	movq	%rax, 56(%rbx)
	movb	$10, (%r15)
	movq	56(%rbx), %r15
.L77:
	testb	%dl, %dl
	je	.L80
	movq	32(%rsp), %rsi
	leaq	(%r15,%rsi), %rax
	cmpq	%rax, 64(%rbx)
	jb	.L169
.L81:
	movq	32(%rsp), %rdx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r8, 24(%rsp)
	call	memcpy@PLT
	movq	32(%rsp), %r15
	addq	56(%rbx), %r15
	movq	24(%rsp), %r8
	movq	%r15, 56(%rbx)
.L83:
	movq	%r15, %rcx
	subq	48(%rbx), %rcx
	cmpq	32(%rbx), %rcx
	ja	.L170
.L86:
	cmpq	$0, 40(%rbx)
	movl	$0, %eax
	cmovns	40(%rbx), %rax
	cmpq	%rax, 8(%rbx)
	jnb	.L88
	movq	56(%rbx), %rax
	cmpq	64(%rbx), %rax
	jnb	.L171
.L89:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movb	$10, (%rax)
.L88:
	cmpq	%r13, %r8
	movl	$1, %r15d
	je	.L76
	movq	%r8, %rdi
	call	free@PLT
.L76:
	cmpb	$0, 46(%rsp)
	je	.L91
	cmpq	$0, 40(%r12)
	je	.L65
	movq	%r13, %rdi
	call	free@PLT
.L91:
	testl	%r14d, %r14d
	je	.L65
	movq	40(%r12), %rcx
	testq	%rcx, %rcx
	je	.L65
	xorl	%esi, %esi
	movq	16(%rsp), %rdx
	movl	$33554436, %edi
	call	*%rcx
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L65
	orl	4(%rsp), %r15d
	je	.L93
	movq	56(%rbx), %rax
	cmpq	64(%rbx), %rax
	jnb	.L172
.L94:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movb	$10, (%rax)
.L93:
	movq	%r12, %rdi
	call	strlen
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L97
	movq	56(%rbx), %rdi
	leaq	(%rdi,%rax), %rax
	cmpq	%rax, 64(%rbx)
	jnb	.L98
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	jne	.L173
	.p2align 4,,10
	.p2align 3
.L97:
	movq	%r12, %rdi
	call	free@PLT
	movq	56(%rbx), %rax
	subq	48(%rbx), %rax
	cmpq	32(%rbx), %rax
	ja	.L174
.L100:
	cmpq	$0, 40(%rbx)
	movl	$0, %eax
	cmovns	40(%rbx), %rax
	cmpq	%rax, 8(%rbx)
	jb	.L101
.L165:
	movl	$1, %r15d
.L65:
	testq	%rbp, %rbp
	je	.L62
	movq	0(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L62
	movl	40(%rsp), %edx
	movl	%r15d, %eax
	xorl	$1, %eax
	xorl	$1, %edx
	movl	%edx, %r12d
	orl	%edx, %eax
	andl	$1, %r12d
	testb	$1, %al
	je	.L114
.L145:
	movl	4(%rsp), %eax
	movl	40(%rsp), %r8d
	addq	$32, %rbp
	movq	8(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %r9
	movl	%r14d, %edx
	orl	%r15d, %eax
	setne	%cl
	call	argp_doc
	movq	0(%rbp), %rdi
	orl	%eax, %r15d
	testq	%rdi, %rdi
	je	.L62
	testl	%r15d, %r15d
	je	.L145
	testb	%r12b, %r12b
	jne	.L145
.L62:
	addq	$56, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L167:
	testq	%rax, %rax
	je	.L175
	testq	%rcx, %rcx
	leaq	1(%rax), %r13
	je	.L176
	movq	8(%rsp), %rsi
	movq	%r12, %rdi
	call	__argp_input
	movb	$0, 46(%rsp)
	movq	%rax, 16(%rsp)
	movl	$33554434, %edi
	movq	40(%r12), %rax
	movb	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L176:
	testq	%r13, %r13
	je	.L164
.L106:
	movl	4(%rsp), %eax
	movq	56(%rbx), %r15
	movq	%r13, %r8
	movb	$0, 46(%rsp)
	testl	%eax, %eax
	jne	.L116
	movq	$0, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L80:
	movq	%r8, %rdi
	movq	%r8, 24(%rsp)
	call	strlen
	testq	%rax, %rax
	movq	%rax, %r9
	movq	24(%rsp), %r8
	je	.L83
	leaq	(%r15,%rax), %rax
	cmpq	%rax, 64(%rbx)
	jnb	.L84
	movq	%r9, %rsi
	movq	%rbx, %rdi
	movq	%r8, 32(%rsp)
	movq	%r9, 24(%rsp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movq	56(%rbx), %r15
	movq	24(%rsp), %r9
	movq	32(%rsp), %r8
	je	.L83
.L84:
	movq	%r9, %rdx
	movq	%r8, %rsi
	movq	%r15, %rdi
	movq	%r9, 32(%rsp)
	movq	%r8, 24(%rsp)
	call	memcpy@PLT
	movq	32(%rsp), %r9
	addq	56(%rbx), %r9
	movq	24(%rsp), %r8
	movq	%r9, %r15
	movq	%r9, 56(%rbx)
	movq	%r15, %rcx
	subq	48(%rbx), %rcx
	cmpq	32(%rbx), %rcx
	jbe	.L86
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%rbx, %rdi
	movq	%r8, 24(%rsp)
	call	__argp_fmtstream_update
	movq	24(%rsp), %r8
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L166:
	cmpq	$0, 40(%r12)
	je	.L164
	movq	8(%rsp), %rsi
	movq	%r12, %rdi
	call	__argp_input
	cmpl	$1, %r14d
	movq	%rax, 16(%rsp)
	movb	$0, 46(%rsp)
	sbbl	%edi, %edi
	movq	40(%r12), %rax
	movb	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	addl	$33554434, %edi
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L169:
	movq	%rbx, %rdi
	movq	%r8, 24(%rsp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movq	56(%rbx), %r15
	movq	24(%rsp), %r8
	jne	.L81
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L101:
	movq	56(%rbx), %rax
	cmpq	64(%rbx), %rax
	jnb	.L177
.L102:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movb	$10, (%rax)
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L164:
	xorl	%r15d, %r15d
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L173:
	movq	56(%rbx), %rdi
.L98:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	addq	%r13, 56(%rbx)
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L171:
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%r8, 24(%rsp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movq	24(%rsp), %r8
	je	.L88
	movq	56(%rbx), %rax
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L168:
	movl	$1, %esi
	movq	%rbx, %rdi
	movb	%dl, 47(%rsp)
	movq	%r8, 24(%rsp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movq	56(%rbx), %r15
	movq	24(%rsp), %r8
	movzbl	47(%rsp), %edx
	jne	.L78
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L174:
	movq	%rbx, %rdi
	call	__argp_fmtstream_update
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L109:
	movzbl	24(%rsp), %eax
	movq	%r13, %r8
	movq	$0, 16(%rsp)
	movb	%al, 46(%rsp)
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L175:
	testq	%rcx, %rcx
	je	.L164
	movq	8(%rsp), %rsi
	movq	%r12, %rdi
	xorl	%r13d, %r13d
	call	__argp_input
	movb	$0, 46(%rsp)
	movq	%rax, 16(%rsp)
	movl	$33554434, %edi
	movq	40(%r12), %rax
	movb	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L114:
	movl	$1, %r15d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L172:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L93
	movq	56(%rbx), %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L177:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L165
	movq	56(%rbx), %rax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L116:
	xorl	%edx, %edx
	movq	$0, 32(%rsp)
	movq	$0, 16(%rsp)
	jmp	.L108
.L72:
	testq	%rcx, %rcx
	je	.L106
	movq	8(%rsp), %rsi
	movq	%r12, %rdi
	call	__argp_input
	movb	$0, 46(%rsp)
	movq	%rax, 16(%rsp)
	movl	$33554433, %edi
	movq	40(%r12), %rax
	movb	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	jmp	.L75
.L110:
	movb	$0, 46(%rsp)
	movb	$0, 24(%rsp)
	jmp	.L74
.LFE126:
	.size	argp_doc, .-argp_doc
	.p2align 4,,15
	.type	until_short, @function
until_short:
.LFB97:
	movl	24(%rdi), %eax
	andl	$8, %eax
	jne	.L180
	movl	8(%rdi), %edx
	leal	-1(%rdx), %ecx
	cmpl	$254, %ecx
	ja	.L178
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rcx
	movslq	%edx, %rsi
	movq	%fs:(%rcx), %rcx
	testb	$64, 1(%rcx,%rsi,2)
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	xorl	%eax, %eax
.L178:
	rep ret
.LFE97:
	.size	until_short, .-until_short
	.p2align 4,,15
	.type	space, @function
space:
.LFB112:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	56(%rdi), %rdx
	movq	%rdx, %rax
	subq	48(%rdi), %rax
	movq	%rdx, %rcx
	cmpq	32(%rdi), %rax
	ja	.L197
.L184:
	cmpq	$0, 40(%rbx)
	movl	$0, %eax
	movq	64(%rbx), %rdi
	cmovns	40(%rbx), %rax
	addq	%rax, %rsi
	cmpq	16(%rbx), %rsi
	jb	.L185
	cmpq	%rdi, %rdx
	jnb	.L186
.L188:
	leaq	1(%rdx), %rax
	movq	%rax, 56(%rbx)
	movb	$10, (%rdx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	cmpq	%rdi, %rdx
	jnb	.L198
.L189:
	leaq	1(%rcx), %rax
	movq	%rax, 56(%rbx)
	movb	$32, (%rcx)
.L183:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	movq	%rsi, 8(%rsp)
	call	__argp_fmtstream_update
	movq	56(%rbx), %rdx
	movq	8(%rsp), %rsi
	movq	%rdx, %rcx
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L186:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L183
	movq	56(%rbx), %rdx
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L198:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L183
	movq	56(%rbx), %rcx
	jmp	.L189
.LFE112:
	.size	space, .-space
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	" [-%c[%s]]"
.LC1:
	.string	"[-%c %s]"
	.text
	.p2align 4,,15
	.type	usage_argful_short_opt, @function
usage_argful_short_opt:
.LFB120:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L209
.L200:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	24(%rdi), %ebx
	orl	24(%rsi), %ebx
	testb	$16, %bl
	je	.L210
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L210:
	movq	%rcx, %r12
	movq	%rdx, %rcx
	movq	%rdi, %rbp
	movl	$5, %edx
	movq	%rax, %rsi
	movq	%rcx, %rdi
	call	__dcgettext
	andl	$1, %ebx
	movq	%rax, %r13
	jne	.L211
	movq	%rax, %rdi
	call	strlen
	leaq	6(%rax), %rsi
	movq	%r12, %rdi
	call	space
	movl	8(%rbp), %edx
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	jne	.L200
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L211:
	movl	8(%rbp), %edx
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rcx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.LFE120:
	.size	usage_argful_short_opt, .-usage_argful_short_opt
	.p2align 4,,15
	.type	add_argless_short_opt, @function
add_argless_short_opt:
.LFB119:
	cmpq	$0, 16(%rdi)
	je	.L214
.L213:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	cmpq	$0, 16(%rsi)
	jne	.L213
	movl	24(%rdi), %eax
	orl	24(%rsi), %eax
	testb	$16, %al
	jne	.L213
	movq	(%rcx), %rax
	movl	8(%rdi), %edx
	leaq	1(%rax), %rsi
	movq	%rsi, (%rcx)
	movb	%dl, (%rax)
	jmp	.L213
.LFE119:
	.size	add_argless_short_opt, .-add_argless_short_opt
	.section	.rodata.str1.1
.LC2:
	.string	" [-%s]"
.LC3:
	.string	" [--%s[=%s]]"
.LC4:
	.string	" [--%s=%s]"
.LC5:
	.string	" [--%s]"
	.text
	.p2align 4,,15
	.type	hol_usage, @function
hol_usage:
.LFB122:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$56, %rsp
	movl	8(%rdi), %r13d
	testl	%r13d, %r13d
	je	.L215
	movq	%rdi, %r12
	movq	16(%rdi), %rdi
	movq	%rsi, %rbx
	leaq	-56(%rbp), %r15
	call	strlen
	addq	$31, %rax
	movq	(%r12), %r14
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -72(%rbp)
	movq	%rax, -56(%rbp)
	leal	-1(%r13), %eax
	leaq	3(%rax,%rax,2), %r13
	salq	$4, %r13
	addq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L217:
	movq	40(%r14), %rax
	leaq	add_argless_short_opt(%rip), %rsi
	movq	%r14, %rdi
	movq	%r15, %rcx
	addq	$48, %r14
	movq	48(%rax), %rdx
	call	hol_entry_short_iterate
	cmpq	%r13, %r14
	jne	.L217
	movq	-56(%rbp), %rax
	cmpq	-72(%rbp), %rax
	ja	.L254
	movl	8(%r12), %eax
	movq	(%r12), %r13
	testl	%eax, %eax
	je	.L215
.L256:
	subl	$1, %eax
	leaq	usage_argful_short_opt(%rip), %r15
	leaq	3(%rax,%rax,2), %r14
	salq	$4, %r14
	addq	%r13, %r14
	.p2align 4,,10
	.p2align 3
.L220:
	movq	40(%r13), %rax
	movq	%r13, %rdi
	movq	%rbx, %rcx
	movq	%r15, %rsi
	addq	$48, %r13
	movq	48(%rax), %rdx
	call	hol_entry_short_iterate
	cmpq	%r14, %r13
	jne	.L220
	movl	8(%r12), %eax
	movq	(%r12), %r15
	testl	%eax, %eax
	je	.L215
	subl	$1, %eax
	leaq	3(%rax,%rax,2), %r13
	salq	$4, %r13
	leaq	(%r15,%r13), %rax
	movq	%rax, -80(%rbp)
	.p2align 4,,10
	.p2align 3
.L228:
	movq	40(%r15), %rax
	movq	(%r15), %r13
	movq	48(%rax), %rax
	movq	%rax, -72(%rbp)
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.L221
	subl	$1, %eax
	movq	%r13, %r12
	leaq	3(%rax,%rax,2), %r10
	salq	$4, %r10
	leaq	0(%r13,%r10), %r14
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L222:
	addq	$48, %r12
	cmpq	%r14, %r12
	je	.L221
.L227:
	movq	(%r12), %rdx
	testq	%rdx, %rdx
	je	.L222
	movl	24(%r12), %eax
	testb	$4, %al
	cmove	%r12, %r13
	testb	$2, %al
	jne	.L222
	orl	24(%r13), %eax
	movq	16(%r12), %rsi
	movl	%eax, %r11d
	andl	$16, %eax
	testq	%rsi, %rsi
	je	.L255
	testl	%eax, %eax
	jne	.L222
.L229:
	movq	-72(%rbp), %rdi
	movl	$5, %edx
	movl	%r11d, -84(%rbp)
	call	__dcgettext
	movl	-84(%rbp), %r11d
	movq	%rax, %rcx
	movq	(%r12), %rdx
	andl	$1, %r11d
	je	.L226
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	addq	$48, %r12
	call	__argp_fmtstream_printf
	cmpq	%r14, %r12
	jne	.L227
	.p2align 4,,10
	.p2align 3
.L221:
	addq	$48, %r15
	cmpq	-80(%rbp), %r15
	jne	.L228
.L215:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	testl	%eax, %eax
	jne	.L222
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L229
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L226:
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L254:
	leaq	1(%rax), %rdx
	leaq	.LC2(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rdx, -56(%rbp)
	movq	-72(%rbp), %rdx
	movb	$0, (%rax)
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
	movl	8(%r12), %eax
	movq	(%r12), %r13
	testl	%eax, %eax
	jne	.L256
	jmp	.L215
.LFE122:
	.size	hol_usage, .-hol_usage
	.p2align 4,,15
	.type	arg.isra.6, @function
arg.isra.6:
.LFB140:
	testq	%rdi, %rdi
	je	.L257
	movq	%rdi, %rax
	movq	%r8, %rdi
	movq	%rsi, %r8
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r12
	pushq	%rbx
	testb	$1, (%r8)
	movq	%rdx, %rbp
	movq	%r9, %rbx
	movq	%rax, %rsi
	movl	$5, %edx
	jne	.L264
	call	__dcgettext
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	xorl	%eax, %eax
	jmp	__argp_fmtstream_printf
	.p2align 4,,10
	.p2align 3
.L264:
	call	__dcgettext
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%rax, %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	xorl	%eax, %eax
	jmp	__argp_fmtstream_printf
	.p2align 4,,10
	.p2align 3
.L257:
	rep ret
.LFE140:
	.size	arg.isra.6, .-arg.isra.6
	.section	.rodata.str1.1
.LC6:
	.string	"argp-help.c"
.LC7:
	.string	"hol"
.LC8:
	.string	"! oalias (opts)"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC9:
	.string	"hol->entries && hol->short_options"
	.section	.rodata.str1.1
.LC10:
	.string	"entries && short_options"
	.text
	.p2align 4,,15
	.type	argp_hol, @function
argp_hol:
.LFB123:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movq	32(%rdi), %rax
	movq	(%rdi), %rbx
	movq	%rdi, 8(%rsp)
	movl	$32, %edi
	movq	%rsi, 16(%rsp)
	movq	%rax, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	8(%rsp), %r9
	movq	16(%rsp), %r10
	je	.L377
	testq	%rbx, %rbx
	movq	%rax, %r15
	movl	$0, 8(%rax)
	movq	$0, 24(%rax)
	je	.L267
	movl	24(%rbx), %r13d
	andl	$4, %r13d
	jne	.L268
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%esi, %esi
	xorl	%r14d, %r14d
	movq	%fs:(%rax), %r12
	movq	%rbx, %rax
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L271:
	movl	24(%rax), %ecx
	testb	$4, %cl
	jne	.L274
	addl	$1, %esi
	movl	%esi, 8(%r15)
.L274:
	andl	$8, %ecx
	jne	.L270
	leal	-1(%rdx), %ecx
	cmpl	$254, %ecx
	ja	.L270
	movzwl	(%r12,%rdx,2), %edx
	andw	$16384, %dx
	cmpw	$1, %dx
	sbbl	$-1, %r14d
.L270:
	addq	$48, %rax
.L269:
	movslq	8(%rax), %rdx
	testl	%edx, %edx
	jne	.L271
	cmpq	$0, (%rax)
	jne	.L271
	cmpq	$0, 32(%rax)
	jne	.L271
	movl	40(%rax), %edi
	testl	%edi, %edi
	jne	.L271
	leaq	(%rsi,%rsi,2), %rdi
	movq	%r9, 16(%rsp)
	movq	%r10, 8(%rsp)
	salq	$4, %rdi
	call	malloc@PLT
	leal	1(%r14), %edi
	movq	%rax, %rbp
	movq	%rax, (%r15)
	call	malloc@PLT
	testq	%rbp, %rbp
	movq	%rax, 16(%r15)
	je	.L275
	testq	%rax, %rax
	je	.L275
	movl	8(%rbx), %esi
	movq	%rax, %rdi
	movq	16(%rsp), %r9
	movq	8(%rsp), %r10
	movl	40(%rbx), %edx
	testl	%esi, %esi
	jne	.L285
	.p2align 4,,10
	.p2align 3
.L380:
	cmpq	$0, (%rbx)
	je	.L378
	testl	%edx, %edx
	movq	%rbx, 0(%rbp)
	movl	$0, 8(%rbp)
	movq	%rdi, 16(%rbp)
	jne	.L318
	cmpq	$0, (%rbx)
	jne	.L277
.L311:
	addl	$1, %r13d
	.p2align 4,,10
	.p2align 3
.L277:
	movl	%r13d, 24(%rbp)
	movq	%r10, 32(%rbp)
	movl	$1, %edx
	movq	%r9, 40(%rbp)
	movl	24(%rbx), %ecx
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L282:
	movl	24(%rbx), %ecx
	addl	$1, %edx
	testb	$4, %cl
	je	.L283
.L284:
	andl	$8, %ecx
	movl	%edx, 8(%rbp)
	jne	.L278
	movl	8(%rbx), %esi
	leal	-1(%rsi), %ecx
	cmpl	$254, %ecx
	ja	.L278
	movslq	%esi, %rcx
	testb	$64, 1(%r12,%rcx,2)
	jne	.L379
	.p2align 4,,10
	.p2align 3
.L278:
	addq	$48, %rbx
	movl	8(%rbx), %esi
	testl	%esi, %esi
	jne	.L282
	cmpq	$0, (%rbx)
	jne	.L282
	cmpq	$0, 32(%rbx)
	jne	.L282
	movl	40(%rbx), %ecx
	testl	%ecx, %ecx
	jne	.L282
	.p2align 4,,10
	.p2align 3
.L283:
	addq	$48, %rbp
	testl	%esi, %esi
	movl	40(%rbx), %edx
	je	.L380
.L285:
	testl	%edx, %edx
	movq	%rbx, 0(%rbp)
	movl	$0, 8(%rbp)
	movq	%rdi, 16(%rbp)
	cmovne	%edx, %r13d
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L379:
	cmpq	%rdi, %rax
	movl	%esi, %r8d
	jnb	.L279
	cmpb	(%rax), %sil
	je	.L278
	movq	%rax, %rcx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L281:
	cmpb	(%rcx), %r8b
	je	.L278
.L280:
	addq	$1, %rcx
	cmpq	%rdi, %rcx
	jne	.L281
.L279:
	movb	%sil, (%rdi)
	addq	$1, %rdi
	jmp	.L278
	.p2align 4,,10
	.p2align 3
.L378:
	cmpq	$0, 32(%rbx)
	je	.L381
	testl	%edx, %edx
	movq	%rbx, 0(%rbp)
	movl	$0, 8(%rbp)
	movq	%rdi, 16(%rbp)
	je	.L311
.L318:
	movl	%edx, %r13d
	jmp	.L277
.L381:
	testl	%edx, %edx
	je	.L382
	movq	%rbx, 0(%rbp)
	movl	$0, 8(%rbp)
	movl	%edx, %r13d
	movq	%rdi, 16(%rbp)
	jmp	.L277
.L382:
	movb	$0, (%rdi)
.L267:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.L265
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.L265
	leaq	24(%r15), %rax
	movq	%r15, %r14
	movq	%r9, 32(%rsp)
	movq	%r10, %r13
	movq	%rax, 56(%rsp)
.L310:
	movq	(%rsp), %rax
	movl	24(%rax), %r15d
	movq	16(%rax), %r12
	testl	%r15d, %r15d
	jne	.L292
	testq	%r12, %r12
	movq	%r13, %rax
	je	.L293
.L292:
	movq	32(%rsp), %rax
	movl	$48, %edi
	movq	32(%rax), %rbx
	call	malloc@PLT
	testq	%rax, %rax
	je	.L293
	movq	(%rsp), %rdx
	movq	32(%rsp), %rdi
	movl	%r15d, 12(%rax)
	movq	%r12, (%rax)
	movq	%r13, 16(%rax)
	subq	%rbx, %rdx
	movq	%rdi, 24(%rax)
	sarq	$5, %rdx
	movl	%edx, 8(%rax)
	xorl	%edx, %edx
	testq	%r13, %r13
	je	.L294
	movl	32(%r13), %edi
	leal	1(%rdi), %edx
.L294:
	movl	%edx, 32(%rax)
	movq	24(%r14), %rdx
	movq	%rax, 24(%r14)
	movq	%rdx, 40(%rax)
.L293:
	movq	%rax, %rsi
	movq	%rbp, %rdi
	call	argp_hol
	movq	24(%r14), %rdx
	movq	%rax, %rbx
	testq	%rdx, %rdx
	jne	.L296
	jmp	.L383
	.p2align 4,,10
	.p2align 3
.L317:
	movq	%rax, %rdx
.L296:
	movq	40(%rdx), %rax
	testq	%rax, %rax
	jne	.L317
	addq	$40, %rdx
.L295:
	movl	8(%rbx), %ebp
	movq	24(%rbx), %rax
	testl	%ebp, %ebp
	movq	%rax, (%rdx)
	movq	$0, 24(%rbx)
	je	.L297
	movl	8(%r14), %r15d
	movq	16(%rbx), %rdi
	testl	%r15d, %r15d
	movq	%rdi, 16(%rsp)
	jne	.L298
	movq	(%rbx), %rax
	movl	%ebp, 8(%r14)
	movq	%rdi, 16(%r14)
	movq	%rax, (%r14)
	movl	$0, 8(%rbx)
.L297:
	movq	%rbx, %rdi
	call	hol_free
	addq	$32, (%rsp)
	movq	(%rsp), %rax
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	jne	.L310
	movq	%r14, %r15
.L265:
	addq	$88, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L298:
	leal	0(%rbp,%r15), %eax
	leaq	(%rax,%rax,2), %rdi
	movl	%eax, 44(%rsp)
	salq	$4, %rdi
	call	malloc@PLT
	movq	16(%r14), %rdi
	movq	%rax, 8(%rsp)
	movq	%rdi, 24(%rsp)
	call	strlen
	movq	16(%rsp), %rdi
	movl	%eax, %r12d
	call	strlen
	leaq	1(%r12,%rax), %rdi
	call	malloc@PLT
	cmpq	$0, 8(%rsp)
	je	.L319
	testq	%rax, %rax
	je	.L319
	movq	%rax, 72(%rsp)
	movq	(%rbx), %rax
	movq	(%r14), %rcx
	movq	8(%rsp), %rdi
	movq	%rax, 48(%rsp)
	movl	%r15d, %eax
	leaq	(%rax,%rax,2), %rdx
	movq	%rcx, %rsi
	movq	%rcx, 64(%rsp)
	salq	$4, %rdx
	call	__mempcpy@PLT
	movl	%ebp, %edx
	movq	48(%rsp), %rsi
	movq	%rax, %rdi
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	call	memcpy@PLT
	movq	72(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%r12, %rdx
	movq	%r9, %rdi
	call	memcpy@PLT
	movq	%rax, %rsi
	movq	%rax, %r9
	leal	-1(%r15), %eax
	movq	8(%rsp), %rdx
	subq	24(%rsp), %rsi
	leaq	3(%rax,%rax,2), %rax
	movq	64(%rsp), %rcx
	salq	$4, %rax
	addq	%rdx, %rax
.L301:
	addq	%rsi, 16(%rdx)
	addq	$48, %rdx
	cmpq	%rax, %rdx
	jne	.L301
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	leaq	(%r9,%r12), %r8
	movq	%r8, %r11
	movq	%fs:(%rax), %r12
	leal	-1(%rbp), %eax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	leaq	(%rdx,%rax), %r10
.L309:
	movl	8(%rdx), %esi
	movq	%r11, 16(%rdx)
	movq	(%rdx), %rax
	testl	%esi, %esi
	je	.L302
	subl	$1, %esi
	leaq	3(%rsi,%rsi,2), %rsi
	salq	$4, %rsi
	addq	%rax, %rsi
	.p2align 4,,10
	.p2align 3
.L308:
	testb	$8, 24(%rax)
	jne	.L303
	movl	8(%rax), %edi
	leal	-1(%rdi), %ebp
	cmpl	$254, %ebp
	ja	.L303
	movq	16(%rsp), %r15
	movzbl	(%r15), %ebp
	movslq	%edi, %r15
	testb	$64, 1(%r12,%r15,2)
	je	.L303
	movsbl	%bpl, %r15d
	cmpl	%edi, %r15d
	je	.L384
	.p2align 4,,10
	.p2align 3
.L303:
	addq	$48, %rax
	cmpq	%rax, %rsi
	jne	.L308
.L302:
	addq	$48, %rdx
	cmpq	%rdx, %r10
	jne	.L309
	movq	%rcx, %rdi
	movb	$0, (%r11)
	movq	%r9, 16(%rsp)
	call	free@PLT
	movq	16(%r14), %rdi
	call	free@PLT
	movq	8(%rsp), %rax
	movq	16(%rsp), %r9
	movq	%rax, (%r14)
	movl	44(%rsp), %eax
	movq	%r9, 16(%r14)
	movl	%eax, 8(%r14)
	jmp	.L297
.L384:
	cmpq	%r8, %r9
	jnb	.L304
	cmpb	%bpl, (%r9)
	je	.L305
	movq	%r9, %rdi
	jmp	.L306
.L307:
	cmpb	(%rdi), %bpl
	je	.L305
.L306:
	addq	$1, %rdi
	cmpq	%rdi, %r8
	jne	.L307
.L304:
	movb	%bpl, (%r11)
	addq	$1, %r11
.L305:
	addq	$1, 16(%rsp)
	jmp	.L303
.L383:
	movq	56(%rsp), %rdx
	jmp	.L295
.L319:
	leaq	__PRETTY_FUNCTION__.12768(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$856, %edx
	call	__assert_fail
.L377:
	leaq	__PRETTY_FUNCTION__.12590(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$436, %edx
	call	__assert_fail
.L268:
	leaq	__PRETTY_FUNCTION__.12590(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$446, %edx
	call	__assert_fail
.L275:
	leaq	__PRETTY_FUNCTION__.12590(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$460, %edx
	call	__assert_fail
.LFE123:
	.size	argp_hol, .-argp_hol
	.p2align 4,,15
	.type	hol_find_entry.isra.8, @function
hol_find_entry.isra.8:
.LFB142:
	testl	%esi, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L391
	leal	-1(%rsi), %eax
	movq	%rdx, %r13
	movq	%rdi, %r12
	leaq	3(%rax,%rax,2), %r14
	salq	$4, %r14
	addq	%rdi, %r14
.L390:
	movl	8(%r12), %edx
	movq	(%r12), %rbx
	testl	%edx, %edx
	leal	-1(%rdx), %eax
	je	.L387
	leaq	3(%rax,%rax,2), %rbp
	salq	$4, %rbp
	addq	%rbx, %rbp
	.p2align 4,,10
	.p2align 3
.L389:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L388
	testb	$2, 24(%rbx)
	jne	.L388
	movq	%r13, %rsi
	call	strcmp
	testl	%eax, %eax
	je	.L385
.L388:
	addq	$48, %rbx
	cmpq	%rbp, %rbx
	jne	.L389
.L387:
	addq	$48, %r12
	cmpq	%r12, %r14
	jne	.L390
.L391:
	xorl	%r12d, %r12d
.L385:
	popq	%rbx
	movq	%r12, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.LFE142:
	.size	hol_find_entry.isra.8, .-hol_find_entry.isra.8
	.p2align 4,,15
	.type	argp_args_levels.isra.10, @function
argp_args_levels.isra.10:
.LFB144:
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L405
	movl	$10, %esi
	xorl	%ebp, %ebp
	call	strchr
	testq	%rax, %rax
	setne	%bpl
.L405:
	testq	%rbx, %rbx
	je	.L404
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L404
.L407:
	movq	32(%rax), %rsi
	movq	16(%rax), %rdi
	addq	$32, %rbx
	call	argp_args_levels.isra.10
	addq	%rax, %rbp
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L407
.L404:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
.LFE144:
	.size	argp_args_levels.isra.10, .-argp_args_levels.isra.10
	.p2align 4,,15
	.type	filter_doc.part.11, @function
filter_doc.part.11:
.LFB145:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdx, %rbx
	movq	%rcx, %rsi
	movq	%rdx, %rdi
	call	__argp_input
	movq	40(%rbx), %rcx
	movq	%r12, %rsi
	movl	%ebp, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	movq	%rax, %rdx
	jmp	*%rcx
.LFE145:
	.size	filter_doc.part.11, .-filter_doc.part.11
	.p2align 4,,15
	.type	argp_args_usage, @function
argp_args_usage:
.LFB125:
	pushq	%r15
	pushq	%r14
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%r8, %r12
	subq	$56, %rsp
	movq	(%rdx), %rax
	movq	32(%rdi), %rbx
	movq	%rsi, 16(%rsp)
	movq	16(%rdi), %rsi
	movq	48(%rdi), %rdi
	movq	%rdx, 8(%rsp)
	movl	$5, %edx
	movq	%rax, 32(%rsp)
	call	__dcgettext
	cmpq	$0, 40(%rbp)
	movq	%rax, 40(%rsp)
	movq	%rax, %r15
	je	.L423
	movq	%r13, %rcx
	movq	%rbp, %rdx
	movl	$33554438, %esi
	movq	%rax, %rdi
	call	filter_doc.part.11
	movq	%rax, %r15
.L423:
	testq	%r15, %r15
	je	.L424
	movl	$10, %esi
	movq	%r15, %rdi
	call	__strchrnul@PLT
	cmpb	$0, (%rax)
	movq	%rax, %rbp
	je	.L441
	movq	32(%rsp), %rax
	movsbl	(%rax), %eax
	testl	%eax, %eax
	movl	%eax, 24(%rsp)
	jle	.L442
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L427:
	leaq	1(%rbp), %r13
	movl	$10, %esi
	movl	%edx, 4(%rsp)
	movq	%r13, %rdi
	call	__strchrnul@PLT
	movl	4(%rsp), %edx
	movq	%rax, %rbp
	addl	$1, %edx
	cmpl	24(%rsp), %edx
	jne	.L427
.L426:
	movq	8(%rsp), %rax
	movb	$1, 4(%rsp)
	addq	$1, (%rax)
	jmp	.L425
	.p2align 4,,10
	.p2align 3
.L441:
	movq	%r15, %r13
	movb	$0, 4(%rsp)
.L425:
	leaq	1(%rbp), %rsi
	movq	%r12, %rdi
	subq	%r13, %rsi
	call	space
	movq	56(%r12), %rdi
	movq	%rbp, %rcx
	subq	%r13, %rcx
	leaq	(%rdi,%rcx), %rax
	cmpq	%rax, 64(%r12)
	jnb	.L428
	movq	%rcx, %rsi
	movq	%r12, %rdi
	movq	%rcx, 24(%rsp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	jne	.L463
.L429:
	cmpq	%r15, 40(%rsp)
	je	.L430
	movq	%r15, %rdi
	call	free@PLT
.L430:
	testq	%rbx, %rbx
	je	.L431
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L431
	.p2align 4,,10
	.p2align 3
.L432:
	movq	8(%rsp), %rdx
	movq	16(%rsp), %rsi
	addq	$32, %rbx
	movl	%r14d, %ecx
	movq	%r12, %r8
	xorl	%r14d, %r14d
	call	argp_args_usage
	movq	(%rbx), %rdi
	testl	%eax, %eax
	sete	%r14b
	testq	%rdi, %rdi
	jne	.L432
.L431:
	testl	%r14d, %r14d
	je	.L462
	cmpb	$0, 4(%rsp)
	je	.L462
	cmpb	$0, 0(%rbp)
	movq	32(%rsp), %rax
	movzbl	(%rax), %edx
	jne	.L464
	xorl	%eax, %eax
	testb	%dl, %dl
	jle	.L422
	movq	32(%rsp), %rbx
	movb	$0, (%rbx)
.L422:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L463:
	movq	56(%r12), %rdi
	movq	24(%rsp), %rcx
.L428:
	movq	%rcx, %rdx
	movq	%r13, %rsi
	movq	%rcx, 24(%rsp)
	call	memcpy@PLT
	movq	24(%rsp), %rcx
	addq	%rcx, 56(%r12)
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L424:
	testq	%rbx, %rbx
	je	.L462
	movq	(%rbx), %rdi
	xorl	%ebp, %ebp
	movb	$0, 4(%rsp)
	testq	%rdi, %rdi
	jne	.L432
.L462:
	xorl	%eax, %eax
	testl	%r14d, %r14d
	sete	%al
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L464:
	addl	$1, %edx
	movb	%dl, (%rax)
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	movq	%r15, %r13
	jmp	.L426
.LFE125:
	.size	argp_args_usage, .-argp_args_usage
	.p2align 4,,15
	.type	print_header, @function
print_header:
.LFB115:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rsi
	movl	$5, %edx
	subq	$24, %rsp
	movq	48(%rbp), %rdi
	call	__dcgettext
	cmpq	$0, 40(%rbp)
	movq	%rax, %r13
	movq	%rax, %rbx
	je	.L466
	movq	32(%r12), %rcx
	movq	%rbp, %rdx
	movl	$33554435, %esi
	movq	%rax, %rdi
	call	filter_doc.part.11
	movq	%rax, %rbx
.L466:
	testq	%rbx, %rbx
	je	.L467
	cmpb	$0, (%rbx)
	movq	16(%r12), %rax
	jne	.L495
.L468:
	movl	$1, 8(%rax)
.L467:
	cmpq	%rbx, %r13
	je	.L465
	addq	$24, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L495:
	cmpq	$0, (%rax)
	movq	8(%r12), %rbp
	je	.L469
	movq	56(%rbp), %rax
	cmpq	64(%rbp), %rax
	jnb	.L496
.L470:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbp)
	movb	$10, (%rax)
.L492:
	movq	8(%r12), %rbp
.L469:
	movl	24+uparams(%rip), %esi
	movq	%rbp, %rdi
	call	indent_to
	movq	8(%r12), %rbp
	movslq	24+uparams(%rip), %rax
	movq	56(%rbp), %r14
	movq	%r14, %rdx
	subq	48(%rbp), %rdx
	cmpq	32(%rbp), %rdx
	ja	.L497
	movq	%rax, 8(%rbp)
	movq	%rbp, %r15
.L473:
	movq	%rax, 24(%r15)
	movq	%rbx, %rdi
	call	strlen
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L498
	movq	%r14, %rax
	subq	48(%rbp), %rax
	movq	%rbp, %r15
	cmpq	32(%rbp), %rax
	ja	.L499
.L477:
	cmpq	%r14, 64(%r15)
	movq	$0, 8(%rbp)
	jbe	.L500
.L478:
	leaq	1(%r14), %rax
	movq	%rax, 56(%r15)
	movb	$10, (%r14)
.L494:
	movq	16(%r12), %rax
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L465:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L498:
	leaq	(%r14,%rax), %rax
	cmpq	%rax, 64(%rbp)
	jnb	.L475
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L493
	movq	56(%rbp), %r14
.L475:
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	addq	%r15, 56(%rbp)
.L493:
	movq	8(%r12), %rbp
	movq	56(%rbp), %r14
	movq	%rbp, %r15
	movq	%r14, %rax
	subq	48(%rbp), %rax
	cmpq	32(%rbp), %rax
	jbe	.L477
.L499:
	movq	%rbp, %rdi
	call	__argp_fmtstream_update
	movq	8(%r12), %r15
	movq	56(%r15), %r14
	cmpq	%r14, 64(%r15)
	movq	$0, 8(%rbp)
	ja	.L478
.L500:
	movl	$1, %esi
	movq	%r15, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L494
	movq	56(%r15), %r14
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L496:
	movl	$1, %esi
	movq	%rbp, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L492
	movq	56(%rbp), %rax
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L497:
	movq	%rbp, %rdi
	movq	%rax, 8(%rsp)
	call	__argp_fmtstream_update
	movq	8(%r12), %r15
	movl	24+uparams(%rip), %eax
	movq	56(%r15), %r14
	movq	32(%r15), %rcx
	movl	%eax, 4(%rsp)
	movq	8(%rsp), %rax
	movq	%r14, %rsi
	subq	48(%r15), %rsi
	movq	%rax, 8(%rbp)
	cmpq	%rcx, %rsi
	jbe	.L482
	movq	%r15, %rdi
	call	__argp_fmtstream_update
	movq	8(%r12), %rbp
	movslq	4(%rsp), %rax
	movq	56(%rbp), %r14
	jmp	.L473
.L482:
	movq	%r15, %rbp
	movslq	4(%rsp), %rax
	jmp	.L473
.LFE115:
	.size	print_header, .-print_header
	.p2align 4,,15
	.type	comma, @function
comma:
.LFB116:
	pushq	%r14
	pushq	%r13
	movl	%edi, %r13d
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	8(%rsi), %rbx
	movl	24(%rsi), %esi
	testl	%esi, %esi
	je	.L502
	movq	16(%rbp), %rax
	movq	0(%rbp), %rdx
	movl	8(%rax), %ecx
	movq	(%rax), %r14
	movq	32(%rdx), %r12
	testl	%ecx, %ecx
	je	.L503
	testq	%r14, %r14
	je	.L503
	movl	24(%r14), %eax
	cmpl	%eax, 24(%rdx)
	je	.L503
	movq	56(%rbx), %rax
	cmpq	64(%rbx), %rax
	jnb	.L541
.L504:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%rbx)
	movb	$10, (%rax)
.L539:
	movq	8(%rbp), %rbx
.L503:
	testq	%r12, %r12
	je	.L506
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.L506
	cmpb	$0, (%rdi)
	je	.L506
	testq	%r14, %r14
	je	.L507
	movq	32(%r14), %rax
	cmpq	%r12, %rax
	je	.L506
	testq	%rax, %rax
	jne	.L508
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L542:
	cmpq	%rax, %r12
	je	.L515
.L508:
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.L542
.L515:
	cmpq	%rax, %r12
	je	.L506
.L507:
	movq	24(%r12), %rsi
	movq	%rbp, %rdx
	movq	24(%rbx), %r14
	call	print_header
	movq	8(%rbp), %r12
	movq	56(%r12), %rax
	subq	48(%r12), %rax
	movq	%r12, %rbx
	cmpq	32(%r12), %rax
	ja	.L543
.L510:
	movslq	%r14d, %r14
	movq	%r14, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L506:
	movl	$0, 24(%rbp)
.L511:
	movl	%r13d, %esi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	indent_to
	.p2align 4,,10
	.p2align 3
.L502:
	movq	56(%rbx), %rax
	leaq	2(%rax), %rdx
	cmpq	%rdx, 64(%rbx)
	jb	.L544
.L512:
	movl	$8236, %edx
	movw	%dx, (%rax)
	addq	$2, 56(%rbx)
.L540:
	movq	8(%rbp), %rbx
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L544:
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L540
	movq	56(%rbx), %rax
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L541:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L539
	movq	56(%rbx), %rax
	jmp	.L504
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r12, %rdi
	call	__argp_fmtstream_update
	movq	8(%rbp), %rbx
	jmp	.L510
.LFE116:
	.size	comma, .-comma
	.section	.rodata.str1.1
.LC11:
	.string	"%s"
.LC12:
	.string	": %s"
	.text
	.p2align 4,,15
	.type	__argp_failure_internal.part.13, @function
__argp_failure_internal.part.13:
.LFB147:
	pushq	%r15
	pushq	%r14
	movl	%r9d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r12d
	movq	%rdi, %rbx
	movq	%rcx, %rbp
	subq	$232, %rsp
	testq	%rdi, %rdi
	movq	%r8, 8(%rsp)
	je	.L546
	movq	72(%rdi), %r14
	testq	%r14, %r14
	je	.L545
	movq	%r14, %rdi
	call	__flockfile
	movq	64(%rbx), %rdx
.L558:
	leaq	.LC11(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	__fxprintf
	testq	%rbp, %rbp
	je	.L550
	movq	8(%rsp), %rdx
	leaq	16(%rsp), %rdi
	movl	%r15d, %ecx
	movq	%rbp, %rsi
	call	__vasprintf_internal
	testl	%eax, %eax
	js	.L551
	movq	16(%rsp), %rdx
.L552:
	leaq	.LC12(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	__fxprintf
	movq	16(%rsp), %rdi
	call	free@PLT
.L550:
	testl	%r12d, %r12d
	jne	.L572
.L553:
	movl	192(%r14), %eax
	testl	%eax, %eax
	jg	.L573
	movq	40(%r14), %rax
	cmpq	48(%r14), %rax
	jnb	.L574
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%r14)
	movb	$10, (%rax)
.L555:
	movq	%r14, %rdi
	call	__funlockfile
	testl	%r13d, %r13d
	je	.L545
	testq	%rbx, %rbx
	je	.L557
	testb	$32, 28(%rbx)
	je	.L557
.L545:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	movq	stderr(%rip), %r14
	testq	%r14, %r14
	je	.L545
	movq	%r14, %rdi
	call	__flockfile
	movq	program_invocation_short_name(%rip), %rdx
	jmp	.L558
	.p2align 4,,10
	.p2align 3
.L573:
	movq	%r14, %rsi
	movl	$10, %edi
	call	putwc_unlocked
	jmp	.L555
	.p2align 4,,10
	.p2align 3
.L551:
	movq	$0, 16(%rsp)
	xorl	%edx, %edx
	jmp	.L552
	.p2align 4,,10
	.p2align 3
.L572:
	leaq	16(%rsp), %rsi
	movl	%r12d, %edi
	movl	$200, %edx
	call	__strerror_r
	leaq	.LC12(%rip), %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L553
.L557:
	movl	%r13d, %edi
	call	exit
	.p2align 4,,10
	.p2align 3
.L574:
	movl	$10, %esi
	movq	%r14, %rdi
	call	__overflow
	jmp	.L555
.LFE147:
	.size	__argp_failure_internal.part.13, .-__argp_failure_internal.part.13
	.p2align 4,,15
	.type	hol_entry_qcmp, @function
hol_entry_qcmp:
.LFB108:
	movq	32(%rdi), %rdx
	movq	32(%rsi), %rcx
	movl	24(%rdi), %r9d
	movl	24(%rsi), %r8d
	cmpq	%rcx, %rdx
	je	.L576
	testq	%rdx, %rdx
	je	.L578
	testq	%rcx, %rcx
	je	.L581
	movl	32(%rdx), %eax
	movl	32(%rcx), %esi
	cmpl	%eax, %esi
	jge	.L583
	.p2align 4,,10
	.p2align 3
.L582:
	movq	16(%rdx), %rdx
	movl	32(%rdx), %eax
	cmpl	%esi, %eax
	jg	.L582
.L583:
	cmpl	%esi, %eax
	jge	.L587
	.p2align 4,,10
	.p2align 3
.L586:
	movq	16(%rcx), %rcx
	cmpl	%eax, 32(%rcx)
	jg	.L586
	movq	16(%rdx), %rsi
	movq	16(%rcx), %rax
	cmpq	%rax, %rsi
	je	.L659
	.p2align 4,,10
	.p2align 3
.L620:
	movq	%rax, %rcx
	movq	%rsi, %rdx
.L587:
	movq	16(%rdx), %rsi
	movq	16(%rcx), %rax
	cmpq	%rax, %rsi
	jne	.L620
.L659:
	movl	12(%rcx), %eax
	movl	12(%rdx), %esi
	cmpl	%esi, %eax
	je	.L660
	movl	%eax, %edx
	movl	%esi, %edi
	movl	%esi, %ecx
	notl	%edx
	shrl	$31, %edi
	subl	%eax, %ecx
	shrl	$31, %edx
	subl	%esi, %eax
	cmpb	%dl, %dil
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L616:
	movq	%rax, %rcx
.L578:
	movq	16(%rcx), %rax
	testq	%rax, %rax
	jne	.L616
	movl	12(%rcx), %eax
	cmpl	%eax, %r9d
	je	.L617
	movl	%eax, %edx
	movl	%r9d, %esi
	movl	%r9d, %ecx
	notl	%edx
	shrl	$31, %esi
	subl	%eax, %ecx
	shrl	$31, %edx
	subl	%r9d, %eax
	cmpb	%dl, %sil
	cmovne	%ecx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L618:
	movq	%rcx, %rdx
.L581:
	movq	16(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.L618
	movl	12(%rdx), %edx
	movl	$1, %eax
	cmpl	%edx, %r8d
	je	.L655
	movl	%r8d, %ecx
	movl	%edx, %edi
	movl	%edx, %esi
	notl	%ecx
	shrl	$31, %edi
	movl	%r8d, %eax
	shrl	$31, %ecx
	subl	%r8d, %esi
	subl	%edx, %eax
	cmpb	%cl, %dil
	cmovne	%esi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L655:
	rep ret
	.p2align 4,,10
	.p2align 3
.L576:
	cmpl	%r8d, %r9d
	je	.L661
	movl	%r8d, %ecx
	movl	%r9d, %esi
	movl	%r9d, %edx
	notl	%ecx
	shrl	$31, %esi
	movl	%r8d, %eax
	shrl	$31, %ecx
	subl	%r8d, %edx
	subl	%r9d, %eax
	cmpb	%cl, %sil
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	movl	8(%rcx), %eax
	subl	8(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L661:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	leaq	until_short(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	40(%rdi), %rax
	movq	48(%rax), %rdx
	call	hol_entry_short_iterate
	movl	%eax, %r12d
	movq	40(%rbp), %rax
	leaq	until_short(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movq	48(%rax), %rdx
	call	hol_entry_short_iterate
	movq	(%rbx), %rdx
	movl	%eax, %r11d
	movq	0(%rbp), %rax
	movl	8(%rbx), %ecx
	movl	24(%rdx), %edi
	movl	24(%rax), %r10d
	andl	$8, %edi
	andl	$8, %r10d
	testl	%ecx, %ecx
	je	.L652
	subl	$1, %ecx
	leaq	3(%rcx,%rcx,2), %rsi
	salq	$4, %rsi
	addq	%rdx, %rsi
	.p2align 4,,10
	.p2align 3
.L595:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L592
	testb	$2, 24(%rdx)
	je	.L662
.L592:
	addq	$48, %rdx
	cmpq	%rsi, %rdx
	jne	.L595
.L652:
	movl	8(%rbp), %edx
	xorl	%esi, %esi
	movq	$0, (%rsp)
	testl	%edx, %edx
	je	.L663
.L593:
	subl	$1, %edx
	leaq	3(%rdx,%rdx,2), %rcx
	salq	$4, %rcx
	addq	%rax, %rcx
	.p2align 4,,10
	.p2align 3
.L600:
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	je	.L598
	testb	$2, 24(%rax)
	je	.L599
.L598:
	addq	$48, %rax
	cmpq	%rcx, %rax
	jne	.L600
	xorl	%edx, %edx
.L599:
	testb	%sil, %sil
	movq	%rdx, 8(%rsp)
	jne	.L601
	testl	%r10d, %r10d
	je	.L597
	testq	%rdx, %rdx
	je	.L597
	xorl	%r9d, %r9d
.L615:
	leaq	8(%rsp), %rdi
	call	canon_doc_option
	xorl	%r10d, %r10d
	testl	%eax, %eax
	setne	%r10b
.L603:
	cmpl	%r9d, %r10d
	je	.L597
	movl	%r9d, %eax
	subl	%r10d, %eax
.L575:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L662:
	movl	8(%rbp), %edx
	testl	%edi, %edi
	movq	%rcx, (%rsp)
	setne	%sil
	testl	%edx, %edx
	jne	.L593
	testl	%edi, %edi
	movq	$0, 8(%rsp)
	je	.L597
.L601:
	movq	%rsp, %rdi
	call	canon_doc_option
	xorl	%r9d, %r9d
	testl	%eax, %eax
	setne	%r9b
	testl	%r10d, %r10d
	je	.L603
	cmpq	$0, 8(%rsp)
	jne	.L615
	xorl	%r10d, %r10d
	jmp	.L603
.L663:
	movq	$0, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L597:
	movl	%r12d, %eax
	movsbl	%r12b, %ecx
	movl	%r11d, %r8d
	orb	%r11b, %al
	jne	.L605
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L623
	movq	8(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L607
	call	__strcasecmp
	jmp	.L575
.L605:
	testb	%r12b, %r12b
	je	.L608
	movsbq	%cl, %rax
	salq	$2, %rax
.L609:
	testb	%r11b, %r11b
	je	.L606
	movsbq	%r8b, %rdx
	salq	$2, %rdx
.L610:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rsi
	movq	%fs:(%rsi), %rsi
	movl	(%rsi,%rax), %eax
	subl	(%rsi,%rdx), %eax
	jne	.L575
	movsbl	%r8b, %eax
	subl	%ecx, %eax
	jmp	.L575
.L623:
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.L606:
	movq	8(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L625
	movsbq	(%rdx), %rdx
	movq	%rdx, %r8
	salq	$2, %rdx
	jmp	.L610
.L607:
	movsbq	(%rdi), %rax
	movq	%rax, %rcx
	salq	$2, %rax
	jmp	.L609
.L608:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.L607
	xorl	%eax, %eax
	jmp	.L609
	.p2align 4,,10
	.p2align 3
.L625:
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	jmp	.L610
.L617:
	movl	$-1, %eax
	ret
.LFE108:
	.size	hol_entry_qcmp, .-hol_entry_qcmp
	.p2align 4,,15
	.globl	__argp_failure_internal
	.type	__argp_failure_internal, @function
__argp_failure_internal:
.LFB132:
	testq	%rdi, %rdi
	je	.L665
	testb	$2, 28(%rdi)
	jne	.L664
.L665:
	jmp	__argp_failure_internal.part.13
	.p2align 4,,10
	.p2align 3
.L664:
	rep ret
.LFE132:
	.size	__argp_failure_internal, .-__argp_failure_internal
	.p2align 4,,15
	.globl	__argp_failure
	.hidden	__argp_failure
	.type	__argp_failure, @function
__argp_failure:
.LFB133:
	subq	$216, %rsp
	testb	%al, %al
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L674
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L674:
	leaq	224(%rsp), %rax
	testq	%rdi, %rdi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$32, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	je	.L672
	testb	$2, 28(%rdi)
	jne	.L670
.L672:
	leaq	8(%rsp), %r8
	xorl	%r9d, %r9d
	call	__argp_failure_internal.part.13
.L670:
	addq	$216, %rsp
	ret
.LFE133:
	.size	__argp_failure, .-__argp_failure
	.weak	argp_failure
	.set	argp_failure,__argp_failure
	.section	.rodata.str1.1
.LC13:
	.string	"ARGP_HELP_FMT"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"%.*s: ARGP_HELP_FMT parameter requires a value"
	.align 8
.LC15:
	.string	"%.*s: Unknown ARGP_HELP_FMT parameter"
	.section	.rodata.str1.1
.LC16:
	.string	"Garbage in ARGP_HELP_FMT: %s"
.LC17:
	.string	"help"
.LC18:
	.string	"version"
.LC19:
	.string	"Usage:"
.LC20:
	.string	"%s %s"
.LC21:
	.string	"  or: "
.LC22:
	.string	" [OPTION...]"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"Try `%s --help' or `%s --usage' for more information.\n"
	.section	.rodata.str1.1
.LC24:
	.string	"[%s]"
.LC25:
	.string	" %s"
.LC26:
	.string	"--%s"
.LC27:
	.string	"[=%s]"
.LC28:
	.string	"=%s"
	.section	.rodata.str1.8
	.align 8
.LC29:
	.string	"Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options."
	.section	.rodata.str1.1
.LC30:
	.string	"Report bugs to %s.\n"
	.text
	.p2align 4,,15
	.type	_help, @function
_help:
.LFB127:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$184, %rsp
	testq	%rdx, %rdx
	movq	%rdi, -144(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdx, -160(%rbp)
	movl	%ecx, -132(%rbp)
	movq	%r8, -152(%rbp)
	je	.L679
	movq	%rdx, %rdi
	call	__flockfile
	leaq	.LC13(%rip), %rdi
	call	getenv
	testq	%rax, %rax
	je	.L708
	movzbl	(%rax), %esi
	leaq	144+uparam_names(%rip), %rbx
	movq	%rax, %r15
.L682:
	testb	%sil, %sil
	je	.L708
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %r12
	movsbq	%sil, %rax
	movq	%fs:(%r12), %rcx
	movzwl	(%rcx,%rax,2), %edx
	testb	$32, %dh
	je	.L1005
	.p2align 4,,10
	.p2align 3
.L683:
	addq	$1, %r15
	movsbq	(%r15), %rax
	movzwl	(%rcx,%rax,2), %edx
	movq	%rax, %rsi
	testb	$32, %dh
	jne	.L683
	testb	$4, %dh
	jne	.L821
	testb	%al, %al
	movq	%r15, %r13
	jne	.L822
.L708:
	movslq	32+uparams(%rip), %rdx
	movq	-160(%rbp), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	__argp_make_fmtstream
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1006
	testb	$11, -132(%rbp)
	movq	$0, -128(%rbp)
	jne	.L1007
.L711:
	xorl	%ebx, %ebx
	testb	$3, -132(%rbp)
	jne	.L1008
.L714:
	testb	$16, -132(%rbp)
	jne	.L1009
.L727:
	movl	-132(%rbp), %eax
	movl	%eax, %r12d
	andl	$8, %r12d
	testb	$4, %al
	jne	.L1010
	testl	%r12d, %r12d
	je	.L729
	movq	-128(%rbp), %rax
	movl	8(%rax), %eax
	testl	%eax, %eax
	je	.L729
	testl	%ebx, %ebx
	jne	.L732
	movq	-128(%rbp), %rbx
	movq	(%rbx), %r15
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
.L736:
	subl	$1, %eax
	leaq	3(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	%r15, %rax
	movq	%rax, -184(%rbp)
	leaq	-112(%rbp), %rax
	movq	%rax, -192(%rbp)
	leaq	-96(%rbp), %rax
	movq	%rax, -176(%rbp)
	.p2align 4,,10
	.p2align 3
.L799:
	movq	56(%r14), %rcx
	subq	48(%r14), %rcx
	movq	32(%r14), %r8
	movq	(%r15), %r12
	movq	16(%r15), %r11
	cmpq	%r8, %rcx
	ja	.L1011
.L738:
	movq	8(%r14), %rax
	movl	24(%r12), %edx
	xorl	%r10d, %r10d
	movq	$0, 8(%r14)
	movq	%rax, -152(%rbp)
	movq	24(%r14), %rax
	movl	%edx, %edi
	andl	$8, %edi
	movq	%r15, -96(%rbp)
	movq	%r14, -88(%rbp)
	movl	$1, -72(%rbp)
	movq	%rax, -168(%rbp)
	movq	-192(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-120(%rbp), %rax
	movq	%rax, -64(%rbp)
	jne	.L739
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.L739
	subl	$1, %eax
	leaq	3(%rax,%rax,2), %rsi
	movq	%r12, %rax
	salq	$4, %rsi
	addq	%r12, %rsi
	.p2align 4,,10
	.p2align 3
.L741:
	cmpq	$0, (%rax)
	je	.L740
	testb	$2, 24(%rax)
	je	.L839
.L740:
	addq	$48, %rax
	cmpq	%rsi, %rax
	jne	.L741
	xorl	%r10d, %r10d
.L739:
	cmpq	%r8, %rcx
	movslq	8+uparams(%rip), %rbx
	ja	.L1012
.L742:
	movl	8(%r15), %eax
	movq	%rbx, 24(%r14)
	testl	%eax, %eax
	je	.L743
	subl	$1, %eax
	movq	%r12, %rbx
	leaq	3(%rax,%rax,2), %r13
	salq	$4, %r13
	addq	%r12, %r13
	jmp	.L756
	.p2align 4,,10
	.p2align 3
.L755:
	movl	24(%rbx), %edx
.L756:
	testb	$8, %dl
	jne	.L744
	movl	8(%rbx), %eax
	leal	-1(%rax), %ecx
	cmpl	$254, %ecx
	ja	.L744
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rsi
	movslq	%eax, %rcx
	movq	%fs:(%rsi), %rsi
	testb	$64, 1(%rsi,%rcx,2)
	je	.L744
	movsbl	(%r11), %ecx
	cmpl	%ecx, %eax
	je	.L1013
	.p2align 4,,10
	.p2align 3
.L744:
	addq	$48, %rbx
	cmpq	%r13, %rbx
	jne	.L755
	movl	24(%r12), %edi
	andl	$8, %edi
.L743:
	movq	56(%r14), %rax
	subq	48(%r14), %rax
	testl	%edi, %edi
	movq	32(%r14), %rdx
	je	.L757
	cmpq	%rdx, %rax
	movslq	16+uparams(%rip), %rbx
	ja	.L1014
.L758:
	movl	8(%r15), %eax
	movq	%rbx, 24(%r14)
	testl	%eax, %eax
	je	.L759
	subl	$1, %eax
	movq	%r12, -200(%rbp)
	leaq	3(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%r12, %rbx
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L761:
	addq	$48, %r12
	cmpq	%rbx, %r12
	je	.L1015
.L765:
	cmpq	$0, (%r12)
	je	.L761
	testb	$2, 24(%r12)
	jne	.L761
	movq	-176(%rbp), %rsi
	movl	16+uparams(%rip), %edi
	call	comma
	movq	-120(%rbp), %rax
	xorl	%edi, %edi
	movq	(%r12), %rsi
	testq	%rax, %rax
	je	.L763
	movq	(%rax), %rax
	movq	48(%rax), %rdi
.L763:
	movl	$5, %edx
	call	__dcgettext
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L761
	movq	56(%r14), %rdi
	leaq	(%rdi,%rax), %rax
	cmpq	%rax, 64(%r14)
	jnb	.L764
	movq	%r8, %rsi
	movq	%r14, %rdi
	movq	%r8, -208(%rbp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L761
	movq	56(%r14), %rdi
	movq	-208(%rbp), %r8
.L764:
	movq	%r8, %rdx
	movq	%r13, %rsi
	addq	$48, %r12
	movq	%r8, -208(%rbp)
	call	memcpy@PLT
	movq	-208(%rbp), %r8
	addq	%r8, 56(%r14)
	cmpq	%rbx, %r12
	jne	.L765
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	-200(%rbp), %r12
.L759:
	movq	56(%r14), %rax
	subq	48(%r14), %rax
	cmpq	32(%r14), %rax
	ja	.L1016
.L770:
	movl	-72(%rbp), %esi
	movq	$0, 8(%r14)
	testl	%esi, %esi
	je	.L771
	testb	$8, 24(%r12)
	jne	.L772
	movslq	8(%r12), %rax
	leal	-1(%rax), %edx
	cmpl	$254, %edx
	ja	.L772
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	testb	$64, 1(%rdx,%rax,2)
	je	.L772
	.p2align 4,,10
	.p2align 3
.L773:
	movq	56(%r14), %rax
	subq	48(%r14), %rax
	cmpq	32(%r14), %rax
	movslq	-152(%rbp), %rbx
	ja	.L1017
	movq	%rbx, 8(%r14)
.L798:
	movslq	-168(%rbp), %rax
	addq	$48, %r15
	cmpq	-184(%rbp), %r15
	movq	%rax, 24(%r14)
	jne	.L799
	movl	-100(%rbp), %edx
	testl	%edx, %edx
	je	.L737
	movl	4+uparams(%rip), %eax
	testl	%eax, %eax
	je	.L737
	movq	-120(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L801
	movq	(%rbx), %rax
	leaq	.LC29(%rip), %rsi
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	(%rbx), %rdx
	movq	%rbx, %rcx
	movq	%rax, %r12
	movq	%rax, %rbx
	testq	%rdx, %rdx
	je	.L802
	cmpq	$0, 40(%rdx)
	je	.L802
	movl	$33554437, %esi
	movq	%rax, %rdi
	call	filter_doc.part.11
	movq	%rax, %rbx
.L802:
	testq	%rbx, %rbx
	je	.L737
	cmpb	$0, (%rbx)
	je	.L804
	movq	56(%r14), %rax
	cmpq	64(%r14), %rax
	jnb	.L1018
.L805:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%r14)
	movb	$10, (%rax)
.L806:
	movq	%rbx, %rdi
	call	strlen
	movq	%rax, %r13
	movq	56(%r14), %rdi
	movq	64(%r14), %rax
	testq	%r13, %r13
	je	.L807
	leaq	(%rdi,%r13), %rdx
	cmpq	%rdx, %rax
	jnb	.L808
	movq	%r14, %rdi
	movq	%r13, %rsi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movq	56(%r14), %rdi
	je	.L1003
.L808:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	memcpy@PLT
	movq	56(%r14), %rdi
	addq	%r13, %rdi
	movq	%rdi, 56(%r14)
.L1003:
	movq	64(%r14), %rax
.L807:
	cmpq	%rax, %rdi
	jnb	.L1019
.L810:
	leaq	1(%rdi), %rax
	movq	%rax, 56(%r14)
	movb	$10, (%rdi)
.L804:
	cmpq	%r12, %rbx
	je	.L737
	movq	%rbx, %rdi
	call	free@PLT
.L737:
	movl	$1, %ebx
.L729:
	testb	$32, -132(%rbp)
	jne	.L1020
.L812:
	testb	$64, -132(%rbp)
	je	.L813
	movq	argp_program_bug_address(%rip), %r12
	testq	%r12, %r12
	je	.L813
	testl	%ebx, %ebx
	je	.L814
	movq	56(%r14), %rax
	cmpq	64(%r14), %rax
	jnb	.L1021
.L815:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%r14)
	movb	$10, (%rax)
.L1004:
	movq	argp_program_bug_address(%rip), %r12
.L814:
	movq	-144(%rbp), %rax
	leaq	.LC30(%rip), %rsi
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	%r12, %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
.L813:
	movq	-160(%rbp), %rdi
	call	__funlockfile
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L817
	movq	%rax, %rdi
	call	hol_free
.L817:
	movq	%r14, %rdi
	call	__argp_fmtstream_free
.L679:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1005:
	testb	$4, %dh
	je	.L1022
.L821:
	cmpb	$45, %sil
	sete	%dil
	cmpb	$95, %sil
	sete	%al
	orb	%al, %dil
	jne	.L849
	testb	$8, %dl
	je	.L823
.L849:
	movq	%r15, %r13
	.p2align 4,,10
	.p2align 3
.L941:
	addq	$1, %r13
	movsbq	0(%r13), %rdx
	movq	%rdx, %rax
	movzwl	(%rcx,%rdx,2), %edx
	cmpb	$45, %al
	sete	%r8b
	cmpb	$95, %al
	sete	%dil
	orb	%dil, %r8b
	jne	.L941
	testb	$8, %dl
	jne	.L941
	movq	%r13, %rdi
	subq	%r15, %rdi
	movq	%rdi, -128(%rbp)
.L685:
	andb	$32, %dh
	je	.L688
	.p2align 4,,10
	.p2align 3
.L689:
	addq	$1, %r13
	movsbq	0(%r13), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	movq	%rdx, %rax
	jne	.L689
.L688:
	testb	%al, %al
	je	.L690
	cmpb	$44, %al
	je	.L690
	cmpb	$61, %al
	je	.L1023
	movsbl	0(%r13), %eax
.L692:
	subl	$48, %eax
	movl	$0, -168(%rbp)
	movl	$0, -176(%rbp)
	cmpl	$9, %eax
	jbe	.L1024
.L694:
	leaq	uparam_names(%rip), %r14
	movq	%r14, %r12
	jmp	.L704
	.p2align 4,,10
	.p2align 3
.L700:
	addq	$16, %r12
	cmpq	%rbx, %r12
	je	.L1025
.L704:
	movq	%r12, %r14
.L698:
	movl	(%r14), %edx
	addq	$4, %r14
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L698
	movl	%eax, %edx
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%r14), %rdx
	movl	%eax, %edi
	cmove	%rdx, %r14
	addb	%al, %dil
	sbbq	$3, %r14
	subq	%r12, %r14
	cmpq	-128(%rbp), %r14
	jne	.L700
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	strncmp
	testl	%eax, %eax
	jne	.L700
	movl	-168(%rbp), %r11d
	movq	%r12, %rax
	movq	%r14, %r12
	movq	%rax, %r14
	testl	%r11d, %r11d
	je	.L701
	cmpb	$0, 14(%rax)
	jne	.L701
	movq	-120(%rbp), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L702
	movq	(%rax), %rax
	movq	48(%rax), %rdi
.L702:
	leaq	.LC14(%rip), %rsi
	movl	$5, %edx
	call	__dcgettext
	movq	%r15, %r9
	movl	%r12d, %r8d
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1025:
	xorl	%edi, %edi
	cmpq	$0, -120(%rbp)
	je	.L819
	movq	-120(%rbp), %rax
	movq	(%rax), %rax
	movq	48(%rax), %rdi
.L819:
	leaq	.LC15(%rip), %rsi
	movl	$5, %edx
	call	__dcgettext
	movl	-128(%rbp), %r8d
	movq	%r15, %r9
.L1000:
	movq	-120(%rbp), %rdi
	movq	%rax, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__argp_failure
.L703:
	movzbl	0(%r13), %esi
	movq	%r13, %r15
	cmpb	$44, %sil
	jne	.L682
	addq	$1, %r15
	movzbl	1(%r13), %esi
	jmp	.L682
	.p2align 4,,10
	.p2align 3
.L690:
	cmpb	$110, %sil
	movl	$1, -168(%rbp)
	movl	$1, -176(%rbp)
	jne	.L694
	cmpb	$111, 1(%r15)
	jne	.L694
	cmpb	$45, 2(%r15)
	jne	.L694
	addq	$3, %r15
	subq	$3, -128(%rbp)
	movl	$0, -176(%rbp)
	jmp	.L694
.L1022:
	movq	%r15, %r13
.L822:
	movq	-120(%rbp), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L707
	movq	(%rax), %rax
	movq	48(%rax), %rdi
.L707:
	leaq	.LC16(%rip), %rsi
	movl	$5, %edx
	call	__dcgettext
	movq	-120(%rbp), %rdi
	movq	%rax, %rcx
	movq	%r13, %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__argp_failure
	jmp	.L708
.L701:
	movzbl	15(%r14), %eax
	movl	-176(%rbp), %esi
	leaq	uparams(%rip), %rdi
	movl	%esi, (%rdi,%rax)
	jmp	.L703
.L1006:
	movq	-160(%rbp), %rdi
	call	__funlockfile
	jmp	.L679
	.p2align 4,,10
	.p2align 3
.L772:
	cmpq	$0, (%r12)
	jne	.L773
	movq	40(%r15), %rsi
	movq	32(%r12), %rdi
	movq	-176(%rbp), %rdx
	call	print_header
	.p2align 4,,10
	.p2align 3
.L774:
	movq	%r15, -112(%rbp)
	jmp	.L773
	.p2align 4,,10
	.p2align 3
.L771:
	movq	32(%r12), %rsi
	testq	%rsi, %rsi
	je	.L1026
	movq	-120(%rbp), %rax
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L778
	movq	(%rax), %rax
	movq	48(%rax), %rdi
.L778:
	movl	$5, %edx
	call	__dcgettext
	movq	40(%r15), %rdx
	movq	%rax, %r13
	movq	%rax, %rbx
	testq	%rdx, %rdx
	je	.L779
	cmpq	$0, 40(%rdx)
	je	.L779
	movl	8(%r12), %esi
.L820:
	movq	-120(%rbp), %rcx
	movq	%rbx, %rdi
	call	filter_doc.part.11
	movq	%rax, %r13
.L779:
	testq	%r13, %r13
	je	.L777
	cmpb	$0, 0(%r13)
	je	.L782
	movq	56(%r14), %rax
	subq	48(%r14), %rax
	cmpq	32(%r14), %rax
	ja	.L1027
	cmpq	$0, 40(%r14)
	movslq	20+uparams(%rip), %rcx
	movl	$0, %eax
	cmovns	40(%r14), %rax
	movq	%rcx, %rsi
.L784:
	movq	%rcx, 8(%r14)
	movslq	%esi, %rdx
.L785:
	movq	%rdx, 24(%r14)
	leal	3(%rsi), %edx
	cmpl	%eax, %edx
	jnb	.L786
	movq	56(%r14), %rax
	cmpq	64(%r14), %rax
	jnb	.L1028
.L787:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%r14)
	movb	$10, (%rax)
.L789:
	movq	%r13, %rdi
	call	strlen
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L782
	movq	56(%r14), %rdi
	leaq	(%rdi,%rax), %rax
	cmpq	%rax, 64(%r14)
	jnb	.L794
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L782
	movq	56(%r14), %rdi
.L794:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	addq	%r12, 56(%r14)
	.p2align 4,,10
	.p2align 3
.L782:
	cmpq	%r13, %rbx
	je	.L777
	movq	%r13, %rdi
	call	free@PLT
.L777:
	movq	56(%r14), %rax
	movq	%rax, %rdx
	subq	48(%r14), %rdx
	cmpq	32(%r14), %rdx
	ja	.L1029
.L795:
	cmpq	%rax, 64(%r14)
	movq	$0, 8(%r14)
	jbe	.L1030
.L796:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%r14)
	movb	$10, (%rax)
	jmp	.L774
	.p2align 4,,10
	.p2align 3
.L757:
	cmpq	%rdx, %rax
	movslq	12+uparams(%rip), %rbx
	ja	.L1031
.L766:
	movl	8(%r15), %eax
	movq	%rbx, 24(%r14)
	testl	%eax, %eax
	je	.L759
	subl	$1, %eax
	movq	%r12, %r13
	leaq	3(%rax,%rax,2), %rbx
	salq	$4, %rbx
	addq	%r12, %rbx
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L767:
	addq	$48, %r13
	cmpq	%rbx, %r13
	je	.L759
.L769:
	cmpq	$0, 0(%r13)
	je	.L767
	testb	$2, 24(%r13)
	jne	.L767
	movq	-176(%rbp), %rsi
	movl	12+uparams(%rip), %edi
	call	comma
	movq	0(%r13), %rdx
	leaq	.LC26(%rip), %rsi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	__argp_fmtstream_printf
	movq	-120(%rbp), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L768
	movq	(%rax), %rax
	movq	48(%rax), %r8
.L768:
	movq	16(%r12), %rdi
	leaq	24(%r12), %rsi
	leaq	.LC27(%rip), %rcx
	leaq	.LC28(%rip), %rdx
	movq	%r14, %r9
	call	arg.isra.6
	jmp	.L767
	.p2align 4,,10
	.p2align 3
.L1017:
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	movq	56(%r14), %rax
	subq	48(%r14), %rax
	cmpq	32(%r14), %rax
	movq	%rbx, 8(%r14)
	jbe	.L798
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	jmp	.L798
	.p2align 4,,10
	.p2align 3
.L1016:
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	jmp	.L770
	.p2align 4,,10
	.p2align 3
.L1011:
	movq	%r14, %rdi
	movq	%r11, -152(%rbp)
	call	__argp_fmtstream_update
	movq	56(%r14), %rcx
	movq	32(%r14), %r8
	subq	48(%r14), %rcx
	movq	-152(%rbp), %r11
	jmp	.L738
	.p2align 4,,10
	.p2align 3
.L1012:
	movq	%r14, %rdi
	movq	%r11, -208(%rbp)
	movl	%r10d, -200(%rbp)
	call	__argp_fmtstream_update
	movl	24(%r12), %edx
	movq	-208(%rbp), %r11
	movl	-200(%rbp), %r10d
	movl	%edx, %edi
	andl	$8, %edi
	jmp	.L742
.L1013:
	andl	$2, %edx
	je	.L1032
.L746:
	addq	$1, %r11
	jmp	.L744
.L1007:
	movq	-144(%rbp), %rdi
	xorl	%esi, %esi
	call	argp_hol
	movq	(%rax), %rbx
	movl	8(%rax), %r12d
	leaq	.LC17(%rip), %rdx
	movq	%rax, %r15
	movq	%rax, -128(%rbp)
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	hol_find_entry.isra.8
	testq	%rax, %rax
	je	.L712
	movl	$-1, 24(%rax)
	movl	8(%r15), %r12d
.L712:
	leaq	.LC18(%rip), %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	hol_find_entry.isra.8
	testq	%rax, %rax
	je	.L713
	movl	$-1, 24(%rax)
	movq	-128(%rbp), %rax
	movl	8(%rax), %r12d
.L713:
	testl	%r12d, %r12d
	je	.L711
	leaq	hol_entry_qcmp(%rip), %rcx
	movl	%r12d, %esi
	movl	$48, %edx
	movq	%rbx, %rdi
	call	qsort
	jmp	.L711
.L1010:
	movq	-144(%rbp), %rax
	leaq	.LC23(%rip), %rsi
	movl	$5, %edx
	movl	$1, %ebx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	-152(%rbp), %rdx
	movq	%rax, %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	%rdx, %rcx
	call	__argp_fmtstream_printf
	testl	%r12d, %r12d
	je	.L729
	movq	-128(%rbp), %rax
	movl	8(%rax), %r9d
	testl	%r9d, %r9d
	je	.L729
.L732:
	movq	56(%r14), %rax
	cmpq	64(%r14), %rax
	jnb	.L1033
.L730:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%r14)
	movb	$10, (%rax)
.L1002:
	movq	-128(%rbp), %rax
	movq	-128(%rbp), %rbx
	movl	8(%rax), %eax
	movq	(%rbx), %r15
	movq	$0, -112(%rbp)
	movq	$0, -104(%rbp)
	testl	%eax, %eax
	jne	.L736
	jmp	.L737
.L1020:
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movl	%ebx, %ecx
	movq	%r14, %r9
	xorl	%r8d, %r8d
	movl	$1, %edx
	call	argp_doc
	orl	%eax, %ebx
	jmp	.L812
.L1009:
	movq	-120(%rbp), %rsi
	movq	-144(%rbp), %rdi
	movq	%r14, %r9
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	argp_doc
	orl	%eax, %ebx
	jmp	.L727
.L1008:
	movq	-144(%rbp), %rax
	movl	$1, %r15d
	movq	32(%rax), %rsi
	movq	16(%rax), %rdi
	call	argp_args_levels.isra.10
	leaq	30(%rax), %rcx
	movq	%rax, %rdx
	xorl	%esi, %esi
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	movq	%r13, %rdi
	movq	%r13, -168(%rbp)
	call	memset@PLT
	leaq	-96(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	%r14, %rax
	movl	%r15d, %r14d
	movq	%rax, %r15
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L715:
	movq	-168(%rbp), %rax
	movq	24(%r15), %r12
	testl	%r14d, %r14d
	movq	%rbx, 24(%r15)
	movl	$5, %edx
	leaq	.LC19(%rip), %rsi
	movq	%rax, -96(%rbp)
	movq	-144(%rbp), %rax
	movq	48(%rax), %rdi
	jne	.L1001
	leaq	.LC21(%rip), %rsi
.L1001:
	call	__dcgettext
	movq	-152(%rbp), %rcx
	leaq	.LC20(%rip), %rsi
	movq	%rax, %rdx
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	__argp_fmtstream_printf
	movq	56(%r15), %rax
	subq	48(%r15), %rax
	cmpq	32(%r15), %rax
	movslq	28+uparams(%rip), %r14
	ja	.L1034
.L718:
	testb	$2, -132(%rbp)
	movq	8(%r15), %rbx
	movq	%r14, 8(%r15)
	je	.L719
	movq	-128(%rbp), %rax
	movl	8(%rax), %r10d
	testl	%r10d, %r10d
	jne	.L1035
.L720:
	movq	-176(%rbp), %rdx
	movq	-120(%rbp), %rsi
	movl	$1, %ecx
	movq	-144(%rbp), %rdi
	movq	%r15, %r8
	call	argp_args_usage
	movl	%eax, %r13d
	movq	56(%r15), %rax
	movq	%rax, %rcx
	subq	48(%r15), %rcx
	cmpq	32(%r15), %rcx
	ja	.L1036
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	movq	%r12, 24(%r15)
.L723:
	cmpq	%rax, 64(%r15)
	movq	%rbx, 8(%r15)
	jbe	.L1037
.L724:
	leaq	1(%rax), %rcx
	movq	%rcx, 56(%r15)
	movb	$10, (%rax)
.L725:
	xorl	%r14d, %r14d
	testl	%r13d, %r13d
	je	.L1038
.L726:
	movq	56(%r15), %rax
	subq	48(%r15), %rax
	cmpq	32(%r15), %rax
	movslq	28+uparams(%rip), %rbx
	jbe	.L715
	movq	%r15, %rdi
	call	__argp_fmtstream_update
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L1037:
	movl	$1, %esi
	movq	%r15, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L725
	movq	56(%r15), %rax
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L1036:
	movq	%r15, %rdi
	movslq	%r12d, %r12
	movslq	%ebx, %rbx
	call	__argp_fmtstream_update
	movq	56(%r15), %rax
	movq	%r12, 24(%r15)
	movq	%rax, %rcx
	subq	48(%r15), %rcx
	cmpq	32(%r15), %rcx
	jbe	.L723
	movq	%r15, %rdi
	call	__argp_fmtstream_update
	movq	56(%r15), %rax
	jmp	.L723
	.p2align 4,,10
	.p2align 3
.L719:
	movq	-128(%rbp), %rdi
	movq	%r15, %rsi
	call	hol_usage
	orl	$2, -132(%rbp)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L1034:
	movq	%r15, %rdi
	call	__argp_fmtstream_update
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L1035:
	movq	-144(%rbp), %rax
	leaq	.LC22(%rip), %rsi
	movl	$5, %edx
	movq	48(%rax), %rdi
	call	__dcgettext
	movq	%rax, %rdi
	movq	%rax, %r14
	call	strlen
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L720
	movq	56(%r15), %rdi
	leaq	(%rdi,%rax), %rax
	cmpq	%rax, 64(%r15)
	jnb	.L721
	movq	%r13, %rsi
	movq	%r15, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L720
	movq	56(%r15), %rdi
.L721:
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	memcpy@PLT
	addq	%r13, 56(%r15)
	jmp	.L720
.L1038:
	movq	%r15, %r14
	movl	$1, %ebx
	jmp	.L714
.L839:
	movl	$1, %r10d
	jmp	.L739
.L1023:
	movsbq	1(%r13), %rsi
	leaq	1(%r13), %rdx
	movq	%rdx, %r13
	testb	$32, 1(%rcx,%rsi,2)
	movq	%rsi, %rax
	je	.L692
	.p2align 4,,10
	.p2align 3
.L693:
	addq	$1, %r13
	movsbq	0(%r13), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	movq	%rdx, %rax
	jne	.L693
	jmp	.L692
.L1014:
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	jmp	.L758
.L1031:
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	jmp	.L766
.L1030:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L774
	movq	56(%r14), %rax
	jmp	.L796
.L1029:
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	movq	56(%r14), %rax
	jmp	.L795
.L1024:
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%r13, %rdi
	call	strtol
	movl	%eax, -176(%rbp)
	jmp	.L999
	.p2align 4,,10
	.p2align 3
.L1039:
	addq	$1, %r13
.L999:
	movsbq	0(%r13), %rax
	movq	%rax, %rdx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L1039
	movq	%fs:(%r12), %rcx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L829
	.p2align 4,,10
	.p2align 3
.L697:
	addq	$1, %r13
	movsbq	0(%r13), %rax
	testb	$32, 1(%rcx,%rax,2)
	jne	.L697
.L829:
	movl	$0, -168(%rbp)
	jmp	.L694
.L1026:
	movq	40(%r15), %rdx
	testq	%rdx, %rdx
	je	.L777
	cmpq	$0, 40(%rdx)
	je	.L777
	movl	8(%r12), %esi
	xorl	%ebx, %ebx
	jmp	.L820
.L786:
	cmpl	%eax, %esi
	ja	.L790
	movq	56(%r14), %rax
	leaq	3(%rax), %rdx
	cmpq	%rdx, 64(%r14)
	jnb	.L791
	movl	$3, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L789
	movq	56(%r14), %rax
.L791:
	movl	$8224, %ecx
	movb	$32, 2(%rax)
	movw	%cx, (%rax)
	addq	$3, 56(%r14)
	jmp	.L789
.L1032:
	movq	-176(%rbp), %rsi
	movl	8+uparams(%rip), %edi
	movq	%r11, -208(%rbp)
	movl	%r10d, -200(%rbp)
	call	comma
	movq	56(%r14), %rax
	cmpq	64(%r14), %rax
	movl	-200(%rbp), %r10d
	movq	-208(%rbp), %r11
	jnb	.L1040
.L747:
	leaq	1(%rax), %rdx
	movq	%rdx, 56(%r14)
	movb	$45, (%rax)
.L748:
	movq	56(%r14), %rax
	cmpq	64(%r14), %rax
	movzbl	(%r11), %edx
	jnb	.L1041
.L749:
	leaq	1(%rax), %rcx
	movq	%rcx, 56(%r14)
	movb	%dl, (%rax)
.L750:
	testl	%r10d, %r10d
	movq	16(%r12), %rdi
	je	.L751
	movl	uparams(%rip), %r8d
	testl	%r8d, %r8d
	jne	.L751
	testq	%rdi, %rdi
	je	.L746
	movl	$1, -100(%rbp)
	jmp	.L746
.L823:
	movl	%esi, %eax
	movq	%r15, %r13
	movq	$0, -128(%rbp)
	jmp	.L685
.L1033:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L1002
	movq	56(%r14), %rax
	jmp	.L730
.L790:
	movq	%r14, %rdi
	call	indent_to
	jmp	.L789
.L1028:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L789
	movq	56(%r14), %rax
	jmp	.L787
.L1040:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movl	-200(%rbp), %r10d
	movq	-208(%rbp), %r11
	je	.L748
	movq	56(%r14), %rax
	jmp	.L747
.L751:
	movq	-120(%rbp), %rax
	xorl	%r8d, %r8d
	testq	%rax, %rax
	je	.L753
	movq	(%rax), %rax
	movq	48(%rax), %r8
.L753:
	leaq	24(%r12), %rsi
	leaq	.LC24(%rip), %rcx
	leaq	.LC25(%rip), %rdx
	movq	%r14, %r9
	movq	%r11, -208(%rbp)
	movl	%r10d, -200(%rbp)
	call	arg.isra.6
	movl	-200(%rbp), %r10d
	movq	-208(%rbp), %r11
	jmp	.L746
.L1027:
	movq	%r14, %rdi
	call	__argp_fmtstream_update
	cmpq	$0, 40(%r14)
	movl	$0, %eax
	movq	56(%r14), %rdx
	cmovns	40(%r14), %rax
	subq	48(%r14), %rdx
	movslq	20+uparams(%rip), %rcx
	cmpq	32(%r14), %rdx
	movq	%rcx, %rsi
	jbe	.L784
	movq	%r14, %rdi
	movl	%eax, -208(%rbp)
	movq	%rcx, -200(%rbp)
	call	__argp_fmtstream_update
	movq	56(%r14), %rsi
	subq	48(%r14), %rsi
	cmpq	32(%r14), %rsi
	movq	-200(%rbp), %rcx
	movslq	20+uparams(%rip), %rdx
	movl	-208(%rbp), %eax
	movq	%rcx, 8(%r14)
	jbe	.L846
	movq	%r14, %rdi
	movl	%edx, -200(%rbp)
	call	__argp_fmtstream_update
	movl	20+uparams(%rip), %esi
	movslq	-200(%rbp), %rdx
	movl	-208(%rbp), %eax
	jmp	.L785
.L1021:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L1004
	movq	56(%r14), %rax
	jmp	.L815
.L1041:
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%r11, -216(%rbp)
	movl	%r10d, -208(%rbp)
	movb	%dl, -200(%rbp)
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	movl	-208(%rbp), %r10d
	movq	-216(%rbp), %r11
	je	.L750
	movq	56(%r14), %rax
	movzbl	-200(%rbp), %edx
	jmp	.L749
.L801:
	leaq	.LC29(%rip), %rsi
	movl	$5, %edx
	xorl	%edi, %edi
	call	__dcgettext
	movq	%rax, %rbx
	movq	%rax, %r12
	jmp	.L802
.L1019:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L804
	movq	56(%r14), %rdi
	jmp	.L810
.L1018:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__argp_fmtstream_ensure
	testl	%eax, %eax
	je	.L806
	movq	56(%r14), %rax
	jmp	.L805
.L846:
	movl	%edx, %esi
	jmp	.L785
.LFE127:
	.size	_help, .-_help
	.p2align 4,,15
	.globl	__argp_help
	.type	__argp_help, @function
__argp_help:
.LFB128:
	movq	%rcx, %r8
	movl	%edx, %ecx
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	_help
.LFE128:
	.size	__argp_help, .-__argp_help
	.weak	argp_help
	.set	argp_help,__argp_help
	.p2align 4,,15
	.globl	__argp_state_help
	.hidden	__argp_state_help
	.type	__argp_state_help, @function
__argp_state_help:
.LFB129:
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %ebp
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L1044
	movl	28(%rdi), %eax
	movq	%rdi, %rbx
	testb	$2, %al
	jne	.L1043
	testq	%rsi, %rsi
	je	.L1043
	orb	$-128, %dl
	testb	$64, %al
	movq	64(%rdi), %r8
	cmovne	%edx, %ebp
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	(%rdi), %rdi
	movl	%ebp, %ecx
	call	_help
	testb	$32, 28(%rbx)
	jne	.L1043
.L1049:
	testl	$256, %ebp
	jne	.L1065
	andl	$512, %ebp
	jne	.L1066
.L1043:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1044:
	testq	%rsi, %rsi
	je	.L1043
	movq	program_invocation_short_name(%rip), %r8
	movl	%edx, %ecx
	xorl	%edi, %edi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	_help
	jmp	.L1049
	.p2align 4,,10
	.p2align 3
.L1066:
	xorl	%edi, %edi
	call	exit
.L1065:
	movl	argp_err_exit_status(%rip), %edi
	call	exit
.LFE129:
	.size	__argp_state_help, .-__argp_state_help
	.weak	argp_state_help
	.set	argp_state_help,__argp_state_help
	.section	.rodata.str1.1
.LC31:
	.string	"%s: %s\n"
	.text
	.p2align 4,,15
	.type	__argp_error_internal.part.15, @function
__argp_error_internal.part.15:
.LFB149:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	testq	%rdi, %rdi
	je	.L1068
	movq	72(%rdi), %rbp
.L1069:
	testq	%rbp, %rbp
	je	.L1067
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	movl	%ecx, %r13d
	movq	%rdx, %r14
	movq	%rsi, %r12
	call	__flockfile
	leaq	8(%rsp), %rdi
	movl	%r13d, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	__vasprintf_internal
	testl	%eax, %eax
	js	.L1071
	testq	%rbx, %rbx
	movq	8(%rsp), %rcx
	je	.L1073
.L1079:
	movq	64(%rbx), %rdx
.L1074:
	leaq	.LC31(%rip), %rsi
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	__fxprintf
	movq	8(%rsp), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	movl	$260, %edx
	movq	%rbp, %rsi
	call	__argp_state_help
	movq	%rbp, %rdi
	call	__funlockfile
.L1067:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1071:
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	movq	$0, 8(%rsp)
	jne	.L1079
.L1073:
	movq	program_invocation_short_name(%rip), %rdx
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1068:
	movq	stderr(%rip), %rbp
	jmp	.L1069
.LFE149:
	.size	__argp_error_internal.part.15, .-__argp_error_internal.part.15
	.p2align 4,,15
	.globl	__argp_error_internal
	.type	__argp_error_internal, @function
__argp_error_internal:
.LFB130:
	testq	%rdi, %rdi
	je	.L1081
	testb	$2, 28(%rdi)
	jne	.L1080
.L1081:
	jmp	__argp_error_internal.part.15
	.p2align 4,,10
	.p2align 3
.L1080:
	rep ret
.LFE130:
	.size	__argp_error_internal, .-__argp_error_internal
	.p2align 4,,15
	.globl	__argp_error
	.hidden	__argp_error
	.type	__argp_error, @function
__argp_error:
.LFB131:
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L1090
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L1090:
	leaq	224(%rsp), %rax
	testq	%rdi, %rdi
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	movl	$48, 12(%rsp)
	movq	%rax, 24(%rsp)
	je	.L1088
	testb	$2, 28(%rdi)
	jne	.L1086
.L1088:
	leaq	8(%rsp), %rdx
	xorl	%ecx, %ecx
	call	__argp_error_internal.part.15
.L1086:
	addq	$216, %rsp
	ret
.LFE131:
	.size	__argp_error, .-__argp_error
	.weak	argp_error
	.set	argp_error,__argp_error
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12768, @object
	.size	__PRETTY_FUNCTION__.12768, 11
__PRETTY_FUNCTION__.12768:
	.string	"hol_append"
	.align 8
	.type	__PRETTY_FUNCTION__.12590, @object
	.size	__PRETTY_FUNCTION__.12590, 9
__PRETTY_FUNCTION__.12590:
	.string	"make_hol"
	.section	.rodata
	.align 32
	.type	uparam_names, @object
	.size	uparam_names, 144
uparam_names:
	.string	"dup-args"
	.zero	5
	.byte	1
	.byte	0
	.string	"dup-args-note"
	.byte	1
	.byte	4
	.string	"short-opt-col"
	.byte	0
	.byte	8
	.string	"long-opt-col"
	.zero	1
	.byte	0
	.byte	12
	.string	"doc-opt-col"
	.zero	2
	.byte	0
	.byte	16
	.string	"opt-doc-col"
	.zero	2
	.byte	0
	.byte	20
	.string	"header-col"
	.zero	3
	.byte	0
	.byte	24
	.string	"usage-indent"
	.zero	1
	.byte	0
	.byte	28
	.string	"rmargin"
	.zero	6
	.byte	0
	.byte	32
	.data
	.align 32
	.type	uparams, @object
	.size	uparams, 36
uparams:
	.long	0
	.long	1
	.long	2
	.long	6
	.long	2
	.long	29
	.long	1
	.long	12
	.long	79
	.hidden	strtol
	.hidden	qsort
	.hidden	strncmp
	.hidden	__argp_fmtstream_free
	.hidden	__argp_make_fmtstream
	.hidden	getenv
	.hidden	__strcasecmp
	.hidden	__overflow
	.hidden	exit
	.hidden	__strerror_r
	.hidden	putwc_unlocked
	.hidden	__funlockfile
	.hidden	__vasprintf_internal
	.hidden	__fxprintf
	.hidden	__flockfile
	.hidden	strcmp
	.hidden	__assert_fail
	.hidden	__argp_fmtstream_printf
	.hidden	strlen
	.hidden	__argp_input
	.hidden	__strndup
	.hidden	strchr
	.hidden	__dcgettext
	.hidden	__argp_fmtstream_update
	.hidden	__argp_fmtstream_ensure
