	.text
	.p2align 4,,15
	.globl	__argp_make_fmtstream
	.hidden	__argp_make_fmtstream
	.type	__argp_make_fmtstream, @function
__argp_make_fmtstream:
.LFB87:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r14
	pushq	%r12
	pushq	%rbp
	movl	$72, %edi
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdx, %r12
	movq	%rcx, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1
	movq	%r14, (%rax)
	movq	%r13, 8(%rax)
	movl	$200, %edi
	movq	%r12, 16(%rax)
	movq	%rbp, 24(%rax)
	movq	$0, 40(%rax)
	movq	$0, 32(%rax)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rbx)
	je	.L9
	movq	%rax, 56(%rbx)
	addq	$200, %rax
	movq	%rax, 64(%rbx)
.L1:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L1
.LFE87:
	.size	__argp_make_fmtstream, .-__argp_make_fmtstream
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%.*s\n"
	.text
	.p2align 4,,15
	.globl	__argp_fmtstream_update
	.hidden	__argp_fmtstream_update
	.type	__argp_fmtstream_update, @function
__argp_fmtstream_update:
.LFB89:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	32(%rdi), %rbp
	addq	48(%rdi), %rbp
	movq	56(%rdi), %r14
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	%r14, %rbp
	jnb	.L22
.L55:
	movq	40(%r15), %rbx
	movq	%rbp, %r13
	testq	%rbx, %rbx
	jne	.L12
	movq	8(%r15), %r12
	testq	%r12, %r12
	je	.L13
	leaq	(%r14,%r12), %rax
	cmpq	%rax, 64(%r15)
	jbe	.L14
	leaq	0(%rbp,%r12), %rbx
	movq	%r14, %rdx
	movq	%rbp, %rsi
	subq	%rbp, %rdx
	movq	%rbx, %rdi
	movq	%rbx, %r13
	call	memmove
	addq	%r12, 56(%r15)
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movl	$32, %esi
	movq	%rbx, %rbp
	call	memset
.L15:
	movq	56(%r15), %r14
	movq	%r12, %rbx
	movq	%r12, 40(%r15)
.L12:
	movq	%r14, %r12
	movl	$10, %esi
	movq	%rbp, %rdi
	subq	%r13, %r12
	movq	%r12, %rdx
	call	memchr
	testq	%rbx, %rbx
	movq	%rax, %rcx
	jns	.L19
	movq	$0, 40(%r15)
	xorl	%ebx, %ebx
.L19:
	testq	%rcx, %rcx
	movq	16(%r15), %r10
	je	.L79
	movq	%rcx, %rax
	subq	%r13, %rax
	addq	%rbx, %rax
	cmpq	%r10, %rax
	jl	.L80
	movq	24(%r15), %rdi
	leaq	-1(%r10), %r9
	movq	%r10, 8(%rsp)
	testq	%rdi, %rdi
	jns	.L25
	cmpq	%rcx, %r14
	jbe	.L26
	movq	%r9, %rdi
	movq	%r14, %rdx
	movq	%rcx, %rsi
	subq	%rbx, %rdi
	subq	%rcx, %rdx
	movq	%r9, 24(%rsp)
	addq	%rbp, %rdi
	movq	%rcx, 16(%rsp)
	call	memmove
	movq	24(%rsp), %r9
	movq	16(%rsp), %rcx
	subq	40(%r15), %r9
	addq	56(%r15), %rcx
	movq	8(%rsp), %r10
	movq	$0, 40(%r15)
	addq	%rbp, %r9
	movq	%rcx, %r14
	addq	%r10, %rbp
	subq	%r9, %r14
	cmpq	%r14, %rbp
	movq	%r14, 56(%r15)
	jb	.L55
.L22:
	subq	48(%r15), %r14
	movq	%r14, 32(%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	40(%rdi), %rax
	cmpq	48(%rdi), %rax
	jnb	.L81
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movb	$32, (%rax)
.L17:
	addq	$1, %rbx
	cmpq	%rbx, %r12
	je	.L15
.L14:
	movq	(%r15), %rdi
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jle	.L16
	movq	%rdi, %rsi
	movl	$32, %edi
	call	putwc_unlocked
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L79:
	leaq	(%rbx,%r12), %rax
	cmpq	%rax, %r10
	ja	.L82
	movq	24(%r15), %rdi
	leaq	-1(%r10), %r9
	testq	%rdi, %rdi
	js	.L56
	movq	%r14, %rcx
.L25:
	movq	%rbp, %rax
	subq	%rbx, %rax
	leaq	1(%rax,%r9), %rsi
	cmpq	%rbp, %rsi
	jb	.L59
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	(%rsi), %rdx
	movq	%rsi, %rbx
	movq	%fs:(%rax), %rax
	testb	$1, (%rax,%rdx,2)
	je	.L29
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L83:
	movsbq	(%rbx), %rdx
	testb	$1, (%rax,%rdx,2)
	jne	.L28
.L29:
	subq	$1, %rbx
	cmpq	%rbp, %rbx
	jnb	.L83
.L27:
	leaq	1(%rbx), %r9
	cmpq	%rbp, %r9
	movq	%r9, %rbx
	jbe	.L84
.L32:
	leaq	1(%rbp,%r12), %rax
	cmpq	%rax, %r9
	je	.L85
	leaq	1(%rbx), %rdx
	movq	%r9, %rax
	subq	%rdx, %rax
	cmpq	%rdi, %rax
	jge	.L40
.L38:
	cmpq	%r14, %r9
	jnb	.L76
	movq	64(%r15), %rax
	addq	$1, %rdi
	subq	%r14, %rax
	cmpq	%rdi, %rax
	jle	.L42
	subq	%r9, %r14
	addq	%rbx, %rdi
	movq	%r9, %rsi
	movq	%r14, %rdx
	call	memmove
	movq	24(%r15), %rax
	movb	$10, (%rbx)
	movq	24(%r15), %rdx
	leaq	1(%rbx,%rax), %r9
	leaq	(%r9,%r14), %r12
	movq	%r9, %r14
	subq	%rbp, %r14
	subq	%r13, %r12
	cmpq	%rax, %rdx
	movq	%rbp, %r13
	leaq	1(%rbx), %rbp
	jg	.L86
	.p2align 4,,10
	.p2align 3
.L44:
	testq	%rdx, %rdx
	jle	.L47
	movq	%rbp, %rax
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %rax
	movb	$32, -1(%rax)
	movq	%rax, %rdx
	subq	%rbp, %rdx
	cmpq	%rdx, 24(%r15)
	jg	.L49
	movq	%rax, %rbp
.L47:
	cmpq	%r9, %rbp
	jb	.L87
.L53:
	movq	24(%r15), %rax
	subq	%r14, %r12
	movq	$-1, %rsi
	leaq	0(%rbp,%r12), %r14
	testq	%rax, %rax
	movq	%r14, 56(%r15)
	cmove	%rsi, %rax
	movq	%rax, 40(%r15)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r14, %r12
	movl	$10, %esi
	movq	%rbp, %rdi
	subq	%rbp, %r12
	movq	%r12, %rdx
	call	memchr
	movq	%rax, %rcx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L85:
	movq	64(%r15), %rax
	subq	%rbx, %rax
	cmpq	%rdi, %rax
	jle	.L38
.L76:
	leaq	1(%rbx), %rdx
	movq	%r9, %rax
	subq	%rdx, %rax
.L40:
	movq	%r9, %r14
	movq	%rbp, %r13
	movb	$10, (%rbx)
	subq	%rbp, %r14
	movq	%rdx, %rbp
.L43:
	movq	24(%r15), %rdx
	cmpq	%rax, %rdx
	jle	.L44
.L86:
	leaq	1(%r13,%r12), %rax
	cmpq	%rax, %r9
	je	.L45
.L48:
	testq	%rdx, %rdx
	movl	$1, %ebx
	jg	.L46
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L50:
	movq	40(%rdi), %rax
	cmpq	48(%rdi), %rax
	jnb	.L88
	leaq	1(%rax), %rdx
	movq	%rdx, 40(%rdi)
	movb	$32, (%rax)
.L51:
	movq	%rbx, %rax
	addq	$1, %rbx
	cmpq	24(%r15), %rax
	jge	.L47
.L46:
	movq	(%r15), %rdi
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L50
	movq	%rdi, %rsi
	movl	$32, %edi
	movq	%r9, 8(%rsp)
	call	putwc_unlocked
	movq	8(%rsp), %r9
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L80:
	movq	$0, 40(%r15)
	leaq	1(%rcx), %rbp
	movq	56(%r15), %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	1(%rbx), %r9
	cmpq	%rbp, %r9
	jbe	.L34
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L90:
	movsbq	(%rbx), %rdx
	movq	%rbx, %rsi
	testb	$1, (%rax,%rdx,2)
	jne	.L89
.L34:
	leaq	1(%rsi), %rbx
	cmpq	%rcx, %rbx
	jb	.L90
	jne	.L62
	movq	$0, 40(%r15)
	leaq	2(%rsi), %rbp
	movq	56(%r15), %r14
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L91:
	movsbq	-1(%rbx), %rcx
	testb	$1, (%rax,%rcx,2)
	je	.L32
	movq	%rdx, %rbx
.L31:
	leaq	-1(%rbx), %rdx
	cmpq	%rbp, %rdx
	jnb	.L91
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L87:
	leaq	0(%r13,%r12), %rdx
	movq	%r9, %rsi
	movq	%rbp, %rdi
	subq	%r9, %rdx
	call	memmove
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L45:
	movq	64(%r15), %rax
	subq	%r9, %rax
	cmpq	%rax, %rdx
	jg	.L48
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rbx, %r9
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$1, %r9
	movsbq	(%r9), %rdx
	testb	$1, (%rax,%rdx,2)
	jne	.L36
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L88:
	movl	$32, %esi
	movq	%r9, 8(%rsp)
	call	__overflow
	movq	8(%rsp), %r9
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L42:
	movq	48(%r15), %rcx
	movq	(%r15), %rdi
	movq	%rbx, %rdx
	leaq	.LC0(%rip), %rsi
	xorl	%eax, %eax
	movq	%r9, 8(%rsp)
	subq	%rcx, %rdx
	call	__fxprintf
	movq	8(%rsp), %r9
	movq	48(%r15), %rbp
	movq	%r9, %rax
	subq	%rbp, %r13
	subq	%rbp, %rax
	addq	%r13, %r12
	movq	%rbp, %r13
	movq	%rax, %r14
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rbx, %r9
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$32, %esi
	call	__overflow
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L84:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rsi, %rbx
	jmp	.L27
.L26:
	leaq	(%r12,%rbx), %rax
.L56:
	subq	%rax, %r9
	movq	%rax, 40(%r15)
	addq	%r9, %r14
	movq	%r14, 56(%r15)
	jmp	.L22
.L82:
	movq	%rax, 40(%r15)
	movq	56(%r15), %r14
	jmp	.L22
.LFE89:
	.size	__argp_fmtstream_update, .-__argp_fmtstream_update
	.section	.rodata.str1.1
.LC1:
	.string	"%.*s"
	.text
	.p2align 4,,15
	.globl	__argp_fmtstream_free
	.hidden	__argp_fmtstream_free
	.type	__argp_fmtstream_free, @function
__argp_fmtstream_free:
.LFB88:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__argp_fmtstream_update
	movq	56(%rbx), %rdx
	movq	48(%rbx), %rdi
	cmpq	%rdi, %rdx
	jbe	.L93
	subq	%rdi, %rdx
	movq	%rdi, %rcx
	movq	(%rbx), %rdi
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	call	__fxprintf
	movq	48(%rbx), %rdi
.L93:
	call	free@PLT
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
.LFE88:
	.size	__argp_fmtstream_free, .-__argp_fmtstream_free
	.p2align 4,,15
	.type	__argp_fmtstream_ensure.part.0, @function
__argp_fmtstream_ensure.part.0:
.LFB92:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$1, %r12d
	call	__argp_fmtstream_update
	movq	48(%rbx), %rcx
	movq	56(%rbx), %rdx
	leaq	.LC1(%rip), %rsi
	movq	(%rbx), %rdi
	xorl	%eax, %eax
	subq	%rcx, %rdx
	call	__fxprintf
	movq	48(%rbx), %rdi
	movq	64(%rbx), %rsi
	movq	$0, 32(%rbx)
	subq	%rdi, %rsi
	movq	%rdi, 56(%rbx)
	cmpq	%rbp, %rsi
	jnb	.L95
	addq	%rsi, %rbp
	jc	.L100
	movq	%rbp, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L100
	leaq	(%rax,%rbp), %rsi
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movq	%rsi, 64(%rbx)
.L95:
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r12d, %r12d
	movl	$12, %fs:(%rax)
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.LFE92:
	.size	__argp_fmtstream_ensure.part.0, .-__argp_fmtstream_ensure.part.0
	.p2align 4,,15
	.globl	__argp_fmtstream_ensure
	.hidden	__argp_fmtstream_ensure
	.type	__argp_fmtstream_ensure, @function
__argp_fmtstream_ensure:
.LFB90:
	movq	64(%rdi), %rax
	subq	56(%rdi), %rax
	cmpq	%rsi, %rax
	jb	.L109
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	jmp	__argp_fmtstream_ensure.part.0
.LFE90:
	.size	__argp_fmtstream_ensure, .-__argp_fmtstream_ensure
	.p2align 4,,15
	.globl	__argp_fmtstream_printf
	.hidden	__argp_fmtstream_printf
	.type	__argp_fmtstream_printf, @function
__argp_fmtstream_printf:
.LFB91:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$216, %rsp
	testb	%al, %al
	movq	%rdx, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	je	.L116
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.L116:
	movl	$150, %esi
	leaq	8(%rsp), %rbp
	movq	64(%r13), %rdx
	movq	%rdx, %rax
	subq	56(%r13), %rax
	cmpq	%rax, %rsi
	jbe	.L112
	movq	%r13, %rdi
	call	__argp_fmtstream_ensure.part.0
	testl	%eax, %eax
	je	.L118
.L120:
	movq	64(%r13), %rdx
.L112:
	movq	56(%r13), %rdi
	leaq	256(%rsp), %rax
	movq	%rdx, %rbx
	xorl	%r8d, %r8d
	movq	%r12, %rdx
	movq	%rbp, %rcx
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movl	$16, 8(%rsp)
	subq	%rdi, %rbx
	movl	$48, 12(%rsp)
	movq	%rbx, %rsi
	movq	%rax, 24(%rsp)
	call	__vsnprintf_internal
	movslq	%eax, %rdx
	cmpq	%rbx, %rdx
	jb	.L114
	movq	64(%r13), %rdx
	addl	$1, %eax
	movslq	%eax, %rsi
	movq	%rdx, %rax
	subq	56(%r13), %rax
	cmpq	%rax, %rsi
	jbe	.L112
	movq	%r13, %rdi
	call	__argp_fmtstream_ensure.part.0
	testl	%eax, %eax
	jne	.L120
.L118:
	movq	$-1, %rdx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L114:
	addq	%rdx, 56(%r13)
.L110:
	addq	$216, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.LFE91:
	.size	__argp_fmtstream_printf, .-__argp_fmtstream_printf
	.hidden	__vsnprintf_internal
	.hidden	__fxprintf
	.hidden	__overflow
	.hidden	putwc_unlocked
	.hidden	memchr
	.hidden	memset
	.hidden	memmove
