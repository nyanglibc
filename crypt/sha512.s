	.text
	.p2align 4,,15
	.globl	__sha512_init_ctx
	.type	__sha512_init_ctx, @function
__sha512_init_ctx:
	movabsq	$7640891576956012808, %rax
	movq	$0, 72(%rdi)
	movq	$0, 64(%rdi)
	movq	%rax, (%rdi)
	movabsq	$-4942790177534073029, %rax
	movq	$0, 80(%rdi)
	movq	%rax, 8(%rdi)
	movabsq	$4354685564936845355, %rax
	movq	%rax, 16(%rdi)
	movabsq	$-6534734903238641935, %rax
	movq	%rax, 24(%rdi)
	movabsq	$5840696475078001361, %rax
	movq	%rax, 32(%rdi)
	movabsq	$-7276294671716946913, %rax
	movq	%rax, 40(%rdi)
	movabsq	$2270897969802886507, %rax
	movq	%rax, 48(%rdi)
	movabsq	$6620516959819538809, %rax
	movq	%rax, 56(%rdi)
	ret
	.size	__sha512_init_ctx, .-__sha512_init_ctx
	.p2align 4,,15
	.globl	__sha512_process_block
	.type	__sha512_process_block, @function
__sha512_process_block:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$624, %rsp
	movq	(%rdx), %rax
	movq	%rdi, -56(%rsp)
	movq	%rsi, %rdi
	movq	%rdx, -32(%rsp)
	shrq	$3, %rdi
	movq	%rax, -96(%rsp)
	movq	8(%rdx), %rax
	movq	%rdi, -48(%rsp)
	movq	%rax, -88(%rsp)
	movq	16(%rdx), %rax
	movq	%rax, -80(%rsp)
	movq	24(%rdx), %rax
	movq	%rax, -104(%rsp)
	movq	32(%rdx), %rax
	movq	%rax, -72(%rsp)
	movq	40(%rdx), %rax
	movq	%rax, -112(%rsp)
	movq	48(%rdx), %rax
	movq	%rax, -64(%rsp)
	movq	56(%rdx), %rax
	xorl	%edx, %edx
	addq	%rsi, 64(%rbx)
	adcq	%rdx, 72(%rbx)
	testq	%rdi, %rdi
	movq	%rax, -120(%rsp)
	je	.L4
	leaq	-24(%rsp), %r14
	leaq	K(%rip), %r15
	leaq	512(%r14), %rax
	movq	%rax, -40(%rsp)
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-56(%rsp), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rcx,%rax), %rdx
	bswap	%rdx
	movq	%rdx, (%r14,%rax)
	addq	$8, %rax
	cmpq	$128, %rax
	jne	.L5
	subq	$-128, -56(%rsp)
	movq	-40(%rsp), %rdi
	movq	%r14, %rsi
	.p2align 4,,10
	.p2align 3
.L6:
	movq	112(%rsi), %rax
	movq	8(%rsi), %rcx
	addq	$8, %rsi
	movq	%rax, %rdx
	movq	%rax, %r8
	shrq	$6, %rax
	rolq	$3, %r8
	rorq	$19, %rdx
	xorq	%r8, %rdx
	movq	%rcx, %r8
	xorq	%rax, %rdx
	movq	-8(%rsi), %rax
	addq	64(%rsi), %rax
	rorq	$8, %r8
	addq	%rax, %rdx
	movq	%rcx, %rax
	shrq	$7, %rcx
	rorq	%rax
	xorq	%r8, %rax
	xorq	%rcx, %rax
	addq	%rdx, %rax
	movq	%rax, 120(%rsi)
	cmpq	%rsi, %rdi
	jne	.L6
	movq	-120(%rsp), %rax
	movq	-64(%rsp), %rbx
	xorl	%r9d, %r9d
	movq	-112(%rsp), %rbp
	movq	-72(%rsp), %rdi
	movabsq	$4794697086780616226, %r12
	movq	-104(%rsp), %r13
	movq	-80(%rsp), %r10
	movq	-88(%rsp), %r11
	movq	-96(%rsp), %r8
	movq	%rax, %rsi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%r15,%r9), %r12
	movq	%r10, %r13
	movq	%rbx, %rsi
	movq	%r11, %r10
	movq	%rbp, %rbx
	movq	%r8, %r11
	movq	%rdi, %rbp
	movq	%rax, %r8
	movq	%rcx, %rdi
.L8:
	movq	%rdi, %rax
	movq	%rdi, %rdx
	movq	%rdi, %rcx
	rorq	$18, %rdx
	rorq	$14, %rax
	andq	%rbp, %rcx
	xorq	%rdx, %rax
	movq	%rdi, %rdx
	rolq	$23, %rdx
	xorq	%rax, %rdx
	movq	%rdi, %rax
	notq	%rax
	andq	%rbx, %rax
	xorq	%rcx, %rax
	movq	%r8, %rcx
	addq	%rdx, %rax
	movq	%r8, %rdx
	addq	(%r14,%r9), %rax
	rolq	$30, %rcx
	rorq	$28, %rdx
	addq	$8, %r9
	xorq	%rcx, %rdx
	movq	%r8, %rcx
	rolq	$25, %rcx
	xorq	%rdx, %rcx
	movq	%r11, %rdx
	addq	%rsi, %rax
	xorq	%r10, %rdx
	movq	%r11, %rsi
	addq	%r12, %rax
	andq	%r8, %rdx
	andq	%r10, %rsi
	xorq	%rsi, %rdx
	addq	%rcx, %rdx
	leaq	(%rax,%r13), %rcx
	addq	%rdx, %rax
	cmpq	$640, %r9
	jne	.L17
	addq	%rax, -96(%rsp)
	addq	%r8, -88(%rsp)
	addq	%r11, -80(%rsp)
	addq	%r10, -104(%rsp)
	addq	%rcx, -72(%rsp)
	addq	%rdi, -112(%rsp)
	addq	%rbp, -64(%rsp)
	addq	%rbx, -120(%rsp)
	subq	$16, -48(%rsp)
	jne	.L9
.L4:
	movq	-32(%rsp), %rax
	movq	-96(%rsp), %rbx
	movq	%rbx, (%rax)
	movq	-88(%rsp), %rbx
	movq	%rbx, 8(%rax)
	movq	-80(%rsp), %rbx
	movq	%rbx, 16(%rax)
	movq	-104(%rsp), %rbx
	movq	%rbx, 24(%rax)
	movq	-72(%rsp), %rbx
	movq	%rbx, 32(%rax)
	movq	-112(%rsp), %rbx
	movq	%rbx, 40(%rax)
	movq	-64(%rsp), %rbx
	movq	%rbx, 48(%rax)
	movq	-120(%rsp), %rbx
	movq	%rbx, 56(%rax)
	addq	$624, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__sha512_process_block, .-__sha512_process_block
	.p2align 4,,15
	.globl	__sha512_finish_ctx
	.type	__sha512_finish_ctx, @function
__sha512_finish_ctx:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	80(%rdi), %rax
	movq	%rdi, %rbx
	xorl	%edi, %edi
	addq	%rax, 64(%rbx)
	adcq	%rdi, 72(%rbx)
	cmpq	$111, %rax
	jbe	.L19
	movl	$240, %edx
	movl	$256, %r12d
	movl	$30, %r13d
	subq	%rax, %rdx
	movl	$31, %r14d
.L20:
	leaq	88(%rbx,%rax), %rdi
	leaq	fillbuf(%rip), %rsi
	call	memcpy@PLT
	movq	64(%rbx), %rdx
	leaq	88(%rbx), %rdi
	movq	%r12, %rsi
	leaq	0(,%rdx,8), %rax
	shrq	$61, %rdx
	bswap	%rax
	movq	%rax, 88(%rbx,%r14,8)
	movq	72(%rbx), %rax
	salq	$3, %rax
	orq	%rdx, %rax
	movq	%rbx, %rdx
	bswap	%rax
	movq	%rax, 88(%rbx,%r13,8)
	call	__sha512_process_block
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movq	(%rbx,%rax), %rdx
	bswap	%rdx
	movq	%rdx, 0(%rbp,%rax)
	addq	$8, %rax
	cmpq	$64, %rax
	jne	.L21
	popq	%rbx
	movq	%rbp, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$112, %edx
	movl	$128, %r12d
	movl	$14, %r13d
	subq	%rax, %rdx
	movl	$15, %r14d
	jmp	.L20
	.size	__sha512_finish_ctx, .-__sha512_finish_ctx
	.p2align 4,,15
	.globl	__sha512_process_bytes
	.type	__sha512_process_bytes, @function
__sha512_process_bytes:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	80(%rdx), %r14
	testq	%r14, %r14
	jne	.L54
.L25:
	cmpq	$127, %rbx
	ja	.L55
.L31:
	testq	%rbx, %rbx
	jne	.L56
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	80(%rbp), %r8
	cmpl	$8, %ebx
	leaq	88(%rbp,%r8), %rdx
	jnb	.L33
	testb	$4, %bl
	jne	.L57
	testl	%ebx, %ebx
	je	.L34
	movzbl	(%r12), %eax
	testb	$2, %bl
	movb	%al, (%rdx)
	jne	.L58
.L34:
	addq	%r8, %rbx
	cmpq	$127, %rbx
	ja	.L59
.L37:
	movq	%rbx, 80(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%rbx, %r13
	movq	%r12, %rdi
	movq	%rbp, %rdx
	andq	$-128, %r13
	andl	$127, %ebx
	movq	%r13, %rsi
	addq	%r13, %r12
	call	__sha512_process_block
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$256, %r13d
	leaq	88(%rdx,%r14), %rdi
	subq	%r14, %r13
	cmpq	%rsi, %r13
	cmova	%rsi, %r13
	movq	%r12, %rsi
	movq	%r13, %rdx
	call	memcpy@PLT
	movq	80(%rbp), %rsi
	addq	%r13, %rsi
	cmpq	$128, %rsi
	movq	%rsi, 80(%rbp)
	ja	.L60
.L26:
	addq	%r13, %r12
	subq	%r13, %rbx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%r12), %rax
	leaq	8(%rdx), %rdi
	movq	%r12, %rsi
	andq	$-8, %rdi
	movq	%rax, (%rdx)
	movl	%ebx, %eax
	movq	-8(%r12,%rax), %rcx
	movq	%rcx, -8(%rdx,%rax)
	subq	%rdi, %rdx
	leal	(%rbx,%rdx), %ecx
	addq	%r8, %rbx
	subq	%rdx, %rsi
	shrl	$3, %ecx
	cmpq	$127, %rbx
	rep movsq
	jbe	.L37
.L59:
	leaq	88(%rbp), %r12
	movq	%rbp, %rdx
	movl	$128, %esi
	addq	$-128, %rbx
	movq	%r12, %rdi
	call	__sha512_process_block
	leaq	216(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	88(%rbp), %r15
	andq	$-128, %rsi
	movq	%rbp, %rdx
	addq	%r13, %r14
	movq	%r15, %rdi
	andq	$-128, %r14
	call	__sha512_process_block
	movq	80(%rbp), %rax
	leaq	88(%rbp,%r14), %rsi
	andl	$127, %eax
	cmpl	$8, %eax
	movq	%rax, 80(%rbp)
	jnb	.L27
	testb	$4, %al
	jne	.L61
	testl	%eax, %eax
	je	.L26
	movzbl	(%rsi), %edx
	testb	$2, %al
	movb	%dl, 88(%rbp)
	je	.L26
	movl	%eax, %eax
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%r15,%rax)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rsi), %rdx
	leaq	96(%rbp), %rdi
	andq	$-8, %rdi
	movq	%rdx, 88(%rbp)
	movl	%eax, %edx
	movq	-8(%rsi,%rdx), %rcx
	movq	%rcx, -8(%r15,%rdx)
	subq	%rdi, %r15
	leal	(%rax,%r15), %ecx
	subq	%r15, %rsi
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L26
.L61:
	movl	(%rsi), %edx
	movl	%eax, %eax
	movl	%edx, 88(%rbp)
	movl	-4(%rsi,%rax), %edx
	movl	%edx, -4(%r15,%rax)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L57:
	movl	(%r12), %eax
	movl	%eax, (%rdx)
	movl	%ebx, %eax
	movl	-4(%r12,%rax), %ecx
	movl	%ecx, -4(%rdx,%rax)
	jmp	.L34
.L58:
	movl	%ebx, %eax
	movzwl	-2(%r12,%rax), %ecx
	movw	%cx, -2(%rdx,%rax)
	jmp	.L34
	.size	__sha512_process_bytes, .-__sha512_process_bytes
	.section	.rodata
	.align 32
	.type	K, @object
	.size	K, 640
K:
	.quad	4794697086780616226
	.quad	8158064640168781261
	.quad	-5349999486874862801
	.quad	-1606136188198331460
	.quad	4131703408338449720
	.quad	6480981068601479193
	.quad	-7908458776815382629
	.quad	-6116909921290321640
	.quad	-2880145864133508542
	.quad	1334009975649890238
	.quad	2608012711638119052
	.quad	6128411473006802146
	.quad	8268148722764581231
	.quad	-9160688886553864527
	.quad	-7215885187991268811
	.quad	-4495734319001033068
	.quad	-1973867731355612462
	.quad	-1171420211273849373
	.quad	1135362057144423861
	.quad	2597628984639134821
	.quad	3308224258029322869
	.quad	5365058923640841347
	.quad	6679025012923562964
	.quad	8573033837759648693
	.quad	-7476448914759557205
	.quad	-6327057829258317296
	.quad	-5763719355590565569
	.quad	-4658551843659510044
	.quad	-4116276920077217854
	.quad	-3051310485924567259
	.quad	489312712824947311
	.quad	1452737877330783856
	.quad	2861767655752347644
	.quad	3322285676063803686
	.quad	5560940570517711597
	.quad	5996557281743188959
	.quad	7280758554555802590
	.quad	8532644243296465576
	.quad	-9096487096722542874
	.quad	-7894198246740708037
	.quad	-6719396339535248540
	.quad	-6333637450476146687
	.quad	-4446306890439682159
	.quad	-4076793802049405392
	.quad	-3345356375505022440
	.quad	-2983346525034927856
	.quad	-860691631967231958
	.quad	1182934255886127544
	.quad	1847814050463011016
	.quad	2177327727835720531
	.quad	2830643537854262169
	.quad	3796741975233480872
	.quad	4115178125766777443
	.quad	5681478168544905931
	.quad	6601373596472566643
	.quad	7507060721942968483
	.quad	8399075790359081724
	.quad	8693463985226723168
	.quad	-8878714635349349518
	.quad	-8302665154208450068
	.quad	-8016688836872298968
	.quad	-6606660893046293015
	.quad	-4685533653050689259
	.quad	-4147400797238176981
	.quad	-3880063495543823972
	.quad	-3348786107499101689
	.quad	-1523767162380948706
	.quad	-757361751448694408
	.quad	500013540394364858
	.quad	748580250866718886
	.quad	1242879168328830382
	.quad	1977374033974150939
	.quad	2944078676154940804
	.quad	3659926193048069267
	.quad	4368137639120453308
	.quad	4836135668995329356
	.quad	5532061633213252278
	.quad	6448918945643986474
	.quad	6902733635092675308
	.quad	7801388544844847127
	.align 32
	.type	fillbuf, @object
	.size	fillbuf, 128
fillbuf:
	.byte	-128
	.byte	0
	.zero	126
