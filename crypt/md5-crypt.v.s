	.file	"md5-crypt.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/md5-crypt.v.d -MF /run/asm/crypt/md5-crypt.o.dt -MP
# -MT /run/asm/crypt/.o -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC
# -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h md5-crypt.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/crypt/md5-crypt.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fpie -ftls-model=initial-exec
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fpic -fpie
# -fplt -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"$"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__md5_crypt_r
	.type	__md5_crypt_r, @function
__md5_crypt_r:
.LFB41:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%r15	#
	pushq	%r14	#
	pushq	%r13	#
	pushq	%r12	#
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r15	# key, key
	pushq	%rbx	#
# md5-crypt.c:107:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0)
	leaq	md5_salt_prefix(%rip), %rdi	#,
	.cfi_offset 3, -56
# md5-crypt.c:93: {
	movq	%rsi, %rbx	# salt, salt
	subq	$456, %rsp	#,
# md5-crypt.c:93: {
	movq	%rdx, -440(%rbp)	# buffer, %sfp
# md5-crypt.c:107:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0)
	movl	$3, %edx	#,
# md5-crypt.c:93: {
	movl	%ecx, -404(%rbp)	# buflen, buflen
# md5-crypt.c:107:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0)
	call	strncmp@PLT	#
# md5-crypt.c:109:     salt += sizeof (md5_salt_prefix) - 1;
	leaq	3(%rbx), %rdx	#, tmp384
	testl	%eax, %eax	# _1
# md5-crypt.c:111:   salt_len = MIN (strcspn (salt, "$"), 8);
	leaq	.LC0(%rip), %rsi	#,
# md5-crypt.c:109:     salt += sizeof (md5_salt_prefix) - 1;
	cmovne	%rbx, %rdx	# tmp384,, salt, tmp384
	movq	%rdx, %rdi	# tmp384, salt
	movq	%rdx, -432(%rbp)	# salt, %sfp
# md5-crypt.c:111:   salt_len = MIN (strcspn (salt, "$"), 8);
	call	strcspn@PLT	#
	cmpq	$8, %rax	#, _2
	movq	%rax, %rcx	#, _2
	movl	$8, %eax	#, tmp386
	cmovb	%rcx, %rax	# _2,, tmp386
# md5-crypt.c:112:   key_len = strlen (key);
	movq	%r15, %rdi	# key,
# md5-crypt.c:111:   salt_len = MIN (strcspn (salt, "$"), 8);
	movq	%rax, -424(%rbp)	# tmp386, %sfp
# md5-crypt.c:112:   key_len = strlen (key);
	call	strlen@PLT	#
# md5-crypt.c:114:   if ((key - (char *) 0) % __alignof__ (md5_uint32) != 0)
	testb	$3, %r15b	#, key
# md5-crypt.c:112:   key_len = strlen (key);
	movq	%rax, -416(%rbp)	# tmp184, %sfp
# md5-crypt.c:114:   if ((key - (char *) 0) % __alignof__ (md5_uint32) != 0)
	je	.L34	#,
# md5-crypt.c:118:       if (__libc_use_alloca (alloca_used + key_len + __alignof__ (md5_uint32)))
	leaq	4(%rax), %rbx	#, _5
# ../sysdeps/pthread/allocalim.h:27:   return (__glibc_likely (__libc_alloca_cutoff (size))
	movq	%rbx, %rdi	# _5,
	call	__libc_alloca_cutoff@PLT	#
# ../sysdeps/pthread/allocalim.h:29:           || __glibc_likely (size <= PTHREAD_STACK_MIN / 4)
	testl	%eax, %eax	# _174
	jne	.L5	#,
	cmpq	$4096, %rbx	#, _5
	ja	.L68	#,
.L5:
# md5-crypt.c:119: 	tmp = (char *) alloca (key_len + __alignof__ (md5_uint32));
	addq	$30, %rbx	#, tmp194
# md5-crypt.c:102:   char *free_key = NULL;
	movq	$0, -456(%rbp)	#, %sfp
# md5-crypt.c:119: 	tmp = (char *) alloca (key_len + __alignof__ (md5_uint32));
	andq	$-16, %rbx	#, tmp198
	subq	%rbx, %rsp	# tmp198,
	leaq	15(%rsp), %rbx	#, tmp200
	andq	$-16, %rbx	#, <retval>
.L7:
# md5-crypt.c:127:       key = copied_key =
	movq	-416(%rbp), %rdx	# %sfp,
# md5-crypt.c:129: 		- (tmp - (char *) 0) % __alignof__ (md5_uint32),
	leaq	4(%rbx), %rdi	#, tmp203
# md5-crypt.c:127:       key = copied_key =
	movq	%r15, %rsi	# key,
	call	memcpy@PLT	#
	movq	%rax, %r15	#, key
	movq	%rax, -464(%rbp)	# key, %sfp
.L4:
# md5-crypt.c:134:   if ((salt - (char *) 0) % __alignof__ (md5_uint32) != 0)
	testb	$3, -432(%rbp)	#, %sfp
# md5-crypt.c:101:   char *copied_salt = NULL;
	movq	$0, -448(%rbp)	#, %sfp
# md5-crypt.c:134:   if ((salt - (char *) 0) % __alignof__ (md5_uint32) != 0)
	jne	.L69	#,
.L8:
# md5-crypt.c:160:   md5_init_ctx (&ctx, nss_ctx);
	leaq	-368(%rbp), %r12	#, tmp382
# md5-crypt.c:178:   md5_init_ctx (&alt_ctx, nss_alt_ctx);
	leaq	-208(%rbp), %rbx	#, tmp383
# md5-crypt.c:160:   md5_init_ctx (&ctx, nss_ctx);
	movq	%r12, %rdi	# tmp382,
	call	__md5_init_ctx@PLT	#
# md5-crypt.c:163:   md5_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	-416(%rbp), %r14	# %sfp, tmp184
	movq	%r12, %rdx	# tmp382,
	movq	%r15, %rdi	# key,
	movq	%r14, %rsi	# tmp184,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:167:   md5_process_bytes (md5_salt_prefix, sizeof (md5_salt_prefix) - 1,
	leaq	md5_salt_prefix(%rip), %rdi	#,
	movq	%r12, %rdx	# tmp382,
	movl	$3, %esi	#,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:173:   md5_process_bytes (salt, salt_len, &ctx, nss_ctx);
	movq	-432(%rbp), %r13	# %sfp, salt
	movq	-424(%rbp), %rsi	# %sfp,
	movq	%r12, %rdx	# tmp382,
	movq	%r13, %rdi	# salt,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:178:   md5_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	%rbx, %rdi	# tmp383,
	movq	%rbx, -472(%rbp)	# tmp383, %sfp
	call	__md5_init_ctx@PLT	#
# md5-crypt.c:181:   md5_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%rbx, %rdx	# tmp383,
	movq	%r14, %rsi	# tmp184,
	movq	%r15, %rdi	# key,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:184:   md5_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	movq	-424(%rbp), %rsi	# %sfp,
	movq	%r13, %rdi	# salt,
	movq	%rbx, %rdx	# tmp383,
# md5-crypt.c:191:   md5_finish_ctx (&alt_ctx, nss_alt_ctx, alt_result);
	leaq	-384(%rbp), %r13	#, tmp381
# md5-crypt.c:184:   md5_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:187:   md5_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%rbx, %rdx	# tmp383,
	movq	%r14, %rsi	# tmp184,
	movq	%r15, %rdi	# key,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:191:   md5_finish_ctx (&alt_ctx, nss_alt_ctx, alt_result);
	movq	%r13, %rsi	# tmp381,
	movq	%rbx, %rdi	# tmp383,
	call	__md5_finish_ctx@PLT	#
# md5-crypt.c:194:   for (cnt = key_len; cnt > 16; cnt -= 16)
	cmpq	$16, %r14	#, tmp184
	jbe	.L15	#,
	leaq	-17(%r14), %rax	#, _194
	leaq	-16(%r14), %rcx	#, _192
	movq	%rax, -488(%rbp)	# _194, %sfp
	andq	$-16, %rax	#, tmp268
	movq	%rcx, -480(%rbp)	# _192, %sfp
	subq	%rax, %rcx	# tmp268, _175
	movq	%rcx, %rbx	# _175, _175
	.p2align 4,,10
	.p2align 3
.L16:
# md5-crypt.c:195:     md5_process_bytes (alt_result, 16, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp382,
	movl	$16, %esi	#,
	movq	%r13, %rdi	# tmp381,
# md5-crypt.c:194:   for (cnt = key_len; cnt > 16; cnt -= 16)
	subq	$16, %r14	#, cnt
# md5-crypt.c:195:     md5_process_bytes (alt_result, 16, &ctx, nss_ctx);
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:194:   for (cnt = key_len; cnt > 16; cnt -= 16)
	cmpq	%rbx, %r14	# _175, cnt
	jne	.L16	#,
# md5-crypt.c:196:   md5_process_bytes (alt_result, cnt, &ctx, nss_ctx);
	movq	-488(%rbp), %r14	# %sfp, _194
	movq	-480(%rbp), %rsi	# %sfp, _192
	movq	%r12, %rdx	# tmp382,
	movq	%r13, %rdi	# tmp381,
	andq	$-16, %r14	#, _194
	subq	%r14, %rsi	# tmp274, _192
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:199:   *alt_result = '\0';
	movb	$0, -384(%rbp)	#, alt_result
.L17:
# md5-crypt.c:194:   for (cnt = key_len; cnt > 16; cnt -= 16)
	movq	-416(%rbp), %rbx	# %sfp, cnt
	.p2align 4,,10
	.p2align 3
.L20:
# md5-crypt.c:206:     md5_process_bytes ((cnt & 1) != 0
	testb	$1, %bl	#, cnt
	movq	%r15, %rdi	# key, iftmp.7_88
	movq	%r12, %rdx	# tmp382,
	cmovne	%r13, %rdi	# tmp381,, iftmp.7_88
	movl	$1, %esi	#,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:205:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	shrq	%rbx	# cnt
	jne	.L20	#,
.L18:
# md5-crypt.c:211:   md5_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	%r13, %rsi	# tmp381,
	movq	%r12, %rdi	# tmp382,
# md5-crypt.c:216:   for (cnt = 0; cnt < 1000; ++cnt)
	xorl	%ebx, %ebx	# cnt
# md5-crypt.c:211:   md5_finish_ctx (&ctx, nss_ctx, alt_result);
	call	__md5_finish_ctx@PLT	#
	jmp	.L27	#
	.p2align 4,,10
	.p2align 3
.L73:
# md5-crypt.c:223: 	md5_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	-416(%rbp), %rsi	# %sfp,
	movq	%r15, %rdi	# key,
	call	__md5_process_bytes@PLT	#
.L22:
# md5-crypt.c:228:       if (cnt % 3 != 0)
	movabsq	$-6148914691236517205, %rax	#, tmp406
	mulq	%rbx	# cnt
	shrq	%rdx	# tmp288
	leaq	(%rdx,%rdx,2), %rax	#, tmp293
	cmpq	%rax, %rbx	# tmp293, cnt
	jne	.L70	#,
.L23:
# md5-crypt.c:232:       if (cnt % 7 != 0)
	movabsq	$5270498306774157605, %rax	#, tmp407
	imulq	%rbx	# cnt
	movq	%rbx, %rax	# cnt, tmp318
	sarq	$63, %rax	#, tmp318
	sarq	%rdx	# tmp317
	subq	%rax, %rdx	# tmp318, tmp314
	leaq	0(,%rdx,8), %rax	#, tmp320
	subq	%rdx, %rax	# tmp314, tmp321
	cmpq	%rax, %rbx	# tmp321, cnt
	jne	.L71	#,
.L24:
# md5-crypt.c:236:       if ((cnt & 1) != 0)
	testq	%r14, %r14	# _12
# md5-crypt.c:237: 	md5_process_bytes (alt_result, 16, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp382,
# md5-crypt.c:236:       if ((cnt & 1) != 0)
	je	.L25	#,
# md5-crypt.c:237: 	md5_process_bytes (alt_result, 16, &ctx, nss_ctx);
	movl	$16, %esi	#,
	movq	%r13, %rdi	# tmp381,
	call	__md5_process_bytes@PLT	#
.L26:
# md5-crypt.c:242:       md5_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	%r13, %rsi	# tmp381,
	movq	%r12, %rdi	# tmp382,
# md5-crypt.c:216:   for (cnt = 0; cnt < 1000; ++cnt)
	addq	$1, %rbx	#, cnt
# md5-crypt.c:242:       md5_finish_ctx (&ctx, nss_ctx, alt_result);
	call	__md5_finish_ctx@PLT	#
# md5-crypt.c:216:   for (cnt = 0; cnt < 1000; ++cnt)
	cmpq	$1000, %rbx	#, cnt
	je	.L72	#,
.L27:
# md5-crypt.c:219:       md5_init_ctx (&ctx, nss_ctx);
	movq	%r12, %rdi	# tmp382,
# md5-crypt.c:222:       if ((cnt & 1) != 0)
	movq	%rbx, %r14	# cnt, _12
# md5-crypt.c:219:       md5_init_ctx (&ctx, nss_ctx);
	call	__md5_init_ctx@PLT	#
# md5-crypt.c:222:       if ((cnt & 1) != 0)
	andl	$1, %r14d	#, _12
# md5-crypt.c:223: 	md5_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp382,
# md5-crypt.c:222:       if ((cnt & 1) != 0)
	jne	.L73	#,
# md5-crypt.c:225: 	md5_process_bytes (alt_result, 16, &ctx, nss_ctx);
	movl	$16, %esi	#,
	movq	%r13, %rdi	# tmp381,
	call	__md5_process_bytes@PLT	#
	jmp	.L22	#
	.p2align 4,,10
	.p2align 3
.L25:
# md5-crypt.c:239: 	md5_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	-416(%rbp), %rsi	# %sfp,
	movq	%r15, %rdi	# key,
	call	__md5_process_bytes@PLT	#
	jmp	.L26	#
	.p2align 4,,10
	.p2align 3
.L71:
# md5-crypt.c:233: 	md5_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	-416(%rbp), %rsi	# %sfp,
	movq	%r12, %rdx	# tmp382,
	movq	%r15, %rdi	# key,
	call	__md5_process_bytes@PLT	#
	jmp	.L24	#
	.p2align 4,,10
	.p2align 3
.L70:
# md5-crypt.c:229: 	md5_process_bytes (salt, salt_len, &ctx, nss_ctx);
	movq	-424(%rbp), %rsi	# %sfp,
	movq	-432(%rbp), %rdi	# %sfp,
	movq	%r12, %rdx	# tmp382,
	call	__md5_process_bytes@PLT	#
	jmp	.L23	#
	.p2align 4,,10
	.p2align 3
.L72:
# md5-crypt.c:252:   cp = __stpncpy (buffer, md5_salt_prefix, MAX (0, buflen));
	movl	-404(%rbp), %edx	# buflen,
	xorl	%ebx, %ebx	# tmp330
	movq	-440(%rbp), %rdi	# %sfp,
	leaq	md5_salt_prefix(%rip), %rsi	#,
	testl	%edx, %edx	#
	movl	%ebx, %edx	# tmp330, tmp329
	cmovns	-404(%rbp), %edx	# buflen,, tmp329
	movslq	%edx, %rdx	# tmp329, tmp331
	call	__stpncpy@PLT	#
# md5-crypt.c:253:   buflen -= sizeof (md5_salt_prefix) - 1;
	movl	-404(%rbp), %ecx	# buflen, tmp444
# md5-crypt.c:255:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	movq	-424(%rbp), %r15	# %sfp, _2
	movq	%rax, %rdi	# _18,
	movq	-432(%rbp), %rsi	# %sfp,
# md5-crypt.c:252:   cp = __stpncpy (buffer, md5_salt_prefix, MAX (0, buflen));
	movq	%rax, -392(%rbp)	# _18, cp
# md5-crypt.c:253:   buflen -= sizeof (md5_salt_prefix) - 1;
	leal	-3(%rcx), %edx	#, _22
# md5-crypt.c:255:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	testl	%edx, %edx	# _22
# md5-crypt.c:253:   buflen -= sizeof (md5_salt_prefix) - 1;
	movl	%edx, -404(%rbp)	# _22, buflen
# md5-crypt.c:255:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	cmovs	%ebx, %edx	# _22,, tmp330, tmp334
	movslq	%edx, %rdx	# tmp334, tmp336
	cmpq	%r15, %rdx	# _2, tmp336
	cmova	%r15, %rdx	# tmp336,, _2, tmp333
	call	__stpncpy@PLT	#
# md5-crypt.c:256:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	movl	-404(%rbp), %ecx	# buflen, buflen.13_27
# md5-crypt.c:255:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	movq	%rax, -392(%rbp)	# _26, cp
# md5-crypt.c:256:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	testl	%ecx, %ecx	# buflen.13_27
	cmovns	%ecx, %ebx	# buflen.13_27,, tmp330
	movslq	%ebx, %rdx	# tmp330,
	cmpq	%r15, %rdx	# _2, tmp340
	cmova	%r15, %rdx	# tmp340,, _2, tmp337
	subl	%edx, %ecx	# tmp337, _34
# md5-crypt.c:258:   if (buflen > 0)
	testl	%ecx, %ecx	# _34
# md5-crypt.c:256:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	movl	%ecx, -404(%rbp)	# _34, buflen
# md5-crypt.c:258:   if (buflen > 0)
	jle	.L28	#,
# md5-crypt.c:260:       *cp++ = '$';
	leaq	1(%rax), %rdx	#, tmp341
	movq	%rdx, -392(%rbp)	# tmp341, cp
	movb	$36, (%rax)	#, *_26
# md5-crypt.c:261:       --buflen;
	subl	$1, -404(%rbp)	#, buflen
.L28:
# md5-crypt.c:264:   __b64_from_24bit (&cp, &buflen,
	movzbl	-378(%rbp), %ecx	# alt_result, alt_result
	movzbl	-384(%rbp), %edx	# alt_result, alt_result
	leaq	-404(%rbp), %rbx	#, tmp345
	movzbl	-372(%rbp), %r8d	# alt_result,
	leaq	-392(%rbp), %r14	#, tmp346
	movl	$4, %r9d	#,
	movq	%rbx, %rsi	# tmp345,
	movq	%r14, %rdi	# tmp346,
	call	__b64_from_24bit@PLT	#
# md5-crypt.c:266:   __b64_from_24bit (&cp, &buflen,
	movzbl	-377(%rbp), %ecx	# alt_result, alt_result
	movzbl	-383(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-371(%rbp), %r8d	# alt_result,
	movq	%rbx, %rsi	# tmp345,
	movq	%r14, %rdi	# tmp346,
	call	__b64_from_24bit@PLT	#
# md5-crypt.c:268:   __b64_from_24bit (&cp, &buflen,
	movzbl	-376(%rbp), %ecx	# alt_result, alt_result
	movzbl	-382(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-370(%rbp), %r8d	# alt_result,
	movq	%rbx, %rsi	# tmp345,
	movq	%r14, %rdi	# tmp346,
	call	__b64_from_24bit@PLT	#
# md5-crypt.c:270:   __b64_from_24bit (&cp, &buflen,
	movzbl	-375(%rbp), %ecx	# alt_result, alt_result
	movzbl	-381(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-369(%rbp), %r8d	# alt_result,
	movq	%rbx, %rsi	# tmp345,
	movq	%r14, %rdi	# tmp346,
	call	__b64_from_24bit@PLT	#
# md5-crypt.c:272:   __b64_from_24bit (&cp, &buflen,
	movzbl	-374(%rbp), %ecx	# alt_result, alt_result
	movzbl	-380(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-379(%rbp), %r8d	# alt_result,
	movq	%rbx, %rsi	# tmp345,
	movq	%r14, %rdi	# tmp346,
	call	__b64_from_24bit@PLT	#
# md5-crypt.c:274:   __b64_from_24bit (&cp, &buflen,
	movzbl	-373(%rbp), %r8d	# alt_result,
	xorl	%ecx, %ecx	#
	xorl	%edx, %edx	#
	movl	$2, %r9d	#,
	movq	%rbx, %rsi	# tmp345,
	movq	%r14, %rdi	# tmp346,
	call	__b64_from_24bit@PLT	#
# md5-crypt.c:276:   if (buflen <= 0)
	movl	-404(%rbp), %eax	# buflen,
	testl	%eax, %eax	#
	jle	.L74	#,
# md5-crypt.c:282:     *cp = '\0';		/* Terminate the string.  */
	movq	-392(%rbp), %rax	# cp, cp.21_71
	movq	-440(%rbp), %rbx	# %sfp, <retval>
	movb	$0, (%rax)	#, *cp.21_71
.L30:
# md5-crypt.c:289:   __md5_init_ctx (&ctx);
	movq	%r12, %rdi	# tmp382,
	call	__md5_init_ctx@PLT	#
# md5-crypt.c:290:   __md5_finish_ctx (&ctx, alt_result);
	movq	%r13, %rsi	# tmp381,
	movq	%r12, %rdi	# tmp382,
	call	__md5_finish_ctx@PLT	#
# md5-crypt.c:291:   explicit_bzero (&ctx, sizeof (ctx));
	movl	$156, %edx	#,
	movl	$156, %esi	#,
	movq	%r12, %rdi	# tmp382,
	call	__explicit_bzero_chk@PLT	#
# md5-crypt.c:292:   explicit_bzero (&alt_ctx, sizeof (alt_ctx));
	movq	-472(%rbp), %rdi	# %sfp,
	movl	$156, %edx	#,
	movl	$156, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# md5-crypt.c:294:   if (copied_key != NULL)
	movq	-464(%rbp), %rax	# %sfp, copied_key
	testq	%rax, %rax	# copied_key
	je	.L31	#,
# md5-crypt.c:295:     explicit_bzero (copied_key, key_len);
	movq	-416(%rbp), %rsi	# %sfp,
	movq	$-1, %rdx	#,
	movq	%rax, %rdi	# copied_key,
	call	__explicit_bzero_chk@PLT	#
.L31:
# md5-crypt.c:296:   if (copied_salt != NULL)
	movq	-448(%rbp), %rax	# %sfp, copied_salt
	testq	%rax, %rax	# copied_salt
	je	.L32	#,
# md5-crypt.c:297:     explicit_bzero (copied_salt, salt_len);
	movq	-424(%rbp), %rsi	# %sfp,
	movq	$-1, %rdx	#,
	movq	%rax, %rdi	# copied_salt,
	call	__explicit_bzero_chk@PLT	#
.L32:
# md5-crypt.c:299:   free (free_key);
	movq	-456(%rbp), %rdi	# %sfp,
	call	free@PLT	#
.L1:
# md5-crypt.c:301: }
	leaq	-40(%rbp), %rsp	#,
	movq	%rbx, %rax	# <retval>,
	popq	%rbx	#
	popq	%r12	#
	popq	%r13	#
	popq	%r14	#
	popq	%r15	#
	popq	%rbp	#
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
.L34:
	.cfi_restore_state
# md5-crypt.c:102:   char *free_key = NULL;
	movq	$0, -456(%rbp)	#, %sfp
# md5-crypt.c:100:   char *copied_key = NULL;
	movq	$0, -464(%rbp)	#, %sfp
	jmp	.L4	#
.L74:
# md5-crypt.c:278:       __set_errno (ERANGE);
	movq	errno@gottpoff(%rip), %rax	#, tmp370
# md5-crypt.c:279:       buffer = NULL;
	xorl	%ebx, %ebx	# <retval>
# md5-crypt.c:278:       __set_errno (ERANGE);
	movl	$34, %fs:(%rax)	#, errno
	jmp	.L30	#
.L69:
# md5-crypt.c:136:       char *tmp = (char *) alloca (salt_len + __alignof__ (md5_uint32));
	movq	-424(%rbp), %rsi	# %sfp, _2
	leaq	34(%rsi), %rax	#, tmp213
	andq	$-16, %rax	#, tmp217
	subq	%rax, %rsp	# tmp217,
# md5-crypt.c:137:       salt = copied_salt =
	movl	%esi, %eax	# _2,
# md5-crypt.c:136:       char *tmp = (char *) alloca (salt_len + __alignof__ (md5_uint32));
	leaq	15(%rsp), %rdx	#, tmp219
	andq	$-16, %rdx	#, tmp221
# md5-crypt.c:137:       salt = copied_salt =
	cmpl	$8, %esi	#, _2
# md5-crypt.c:139: 		- (tmp - (char *) 0) % __alignof__ (md5_uint32),
	leaq	4(%rdx), %rcx	#, tmp222
# md5-crypt.c:137:       salt = copied_salt =
	jnb	.L9	#,
	andl	$4, %esi	#, _2
	jne	.L75	#,
	testl	%eax, %eax	# _2
	je	.L10	#,
	movq	-432(%rbp), %rsi	# %sfp, salt
	testb	$2, %al	#, _2
	movzbl	(%rsi), %esi	#* salt, tmp235
	movb	%sil, 4(%rdx)	# tmp235,
	jne	.L76	#,
.L10:
	movq	%rcx, -432(%rbp)	# salt, %sfp
	movq	%rcx, -448(%rbp)	# salt, %sfp
	jmp	.L8	#
.L9:
	movq	-432(%rbp), %rdi	# %sfp, salt
	addq	$8, %rdx	#, tmp252
	movq	(%rdi), %rax	#* salt, tmp244
	movq	%rax, -4(%rdx)	# tmp244,
	movq	-424(%rbp), %rbx	# %sfp, _2
	movl	%ebx, %eax	# _2, _2
	movq	-8(%rdi,%rax), %rsi	#, tmp251
	movq	%rsi, -8(%rcx,%rax)	# tmp251,
	movq	%rcx, %rax	# tmp222, tmp224
	subq	%rdx, %rax	# tmp252, tmp224
	subq	%rax, %rdi	# tmp224, salt
	addl	%ebx, %eax	# _2, _2
	andl	$-8, %eax	#, _2
	movq	%rdi, %r8	# salt, salt
	cmpl	$8, %eax	#, _2
	jb	.L10	#,
	andl	$-8, %eax	#, tmp254
	xorl	%esi, %esi	# tmp253
.L13:
	movl	%esi, %edi	# tmp253, tmp255
	addl	$8, %esi	#, tmp253
	movq	(%r8,%rdi), %r9	#, tmp256
	cmpl	%eax, %esi	# tmp254, tmp253
	movq	%r9, (%rdx,%rdi)	# tmp256,
	jb	.L13	#,
	jmp	.L10	#
.L15:
# md5-crypt.c:196:   md5_process_bytes (alt_result, cnt, &ctx, nss_ctx);
	movq	-416(%rbp), %rbx	# %sfp, tmp184
	movq	%r12, %rdx	# tmp382,
	movq	%r13, %rdi	# tmp381,
	movq	%rbx, %rsi	# tmp184,
	call	__md5_process_bytes@PLT	#
# md5-crypt.c:205:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	testq	%rbx, %rbx	# tmp184
# md5-crypt.c:199:   *alt_result = '\0';
	movb	$0, -384(%rbp)	#, alt_result
# md5-crypt.c:205:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	je	.L18	#,
	jmp	.L17	#
	.p2align 4,,10
	.p2align 3
.L75:
# md5-crypt.c:137:       salt = copied_salt =
	movq	-432(%rbp), %rdi	# %sfp, salt
	movl	(%rdi), %esi	#* salt, tmp227
	movl	%esi, 4(%rdx)	# tmp227,
	movl	-4(%rdi,%rax), %edx	#, tmp234
	movl	%edx, -4(%rcx,%rax)	# tmp234,
	jmp	.L10	#
.L68:
# md5-crypt.c:122: 	  free_key = tmp = (char *) malloc (key_len + __alignof__ (md5_uint32));
	movq	%rbx, %rdi	# _5,
	call	malloc@PLT	#
# md5-crypt.c:123: 	  if (tmp == NULL)
	testq	%rax, %rax	# <retval>
# md5-crypt.c:122: 	  free_key = tmp = (char *) malloc (key_len + __alignof__ (md5_uint32));
	movq	%rax, %rbx	#, <retval>
# md5-crypt.c:123: 	  if (tmp == NULL)
	je	.L1	#,
	movq	%rax, -456(%rbp)	# <retval>, %sfp
	jmp	.L7	#
.L76:
# md5-crypt.c:137:       salt = copied_salt =
	movq	-432(%rbp), %rsi	# %sfp, salt
	movzwl	-2(%rsi,%rax), %edx	#, tmp243
	movw	%dx, -2(%rcx,%rax)	# tmp243,
	jmp	.L10	#
	.cfi_endproc
.LFE41:
	.size	__md5_crypt_r, .-__md5_crypt_r
	.p2align 4,,15
	.globl	__md5_crypt
	.type	__md5_crypt, @function
__md5_crypt:
.LFB42:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %r12	# key, key
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
# md5-crypt.c:315:   int needed = 3 + strlen (salt) + 1 + 26 + 1;
	movq	%rsi, %rdi	# salt,
# md5-crypt.c:310: {
	movq	%rsi, %rbp	# salt, salt
# md5-crypt.c:315:   int needed = 3 + strlen (salt) + 1 + 26 + 1;
	call	strlen@PLT	#
# md5-crypt.c:317:   if (buflen < needed)
	movl	buflen.5407(%rip), %ecx	# buflen, buflen.23_4
# md5-crypt.c:315:   int needed = 3 + strlen (salt) + 1 + 26 + 1;
	leal	31(%rax), %ebx	#, needed
	movq	buffer(%rip), %rdx	# buffer, <retval>
# md5-crypt.c:317:   if (buflen < needed)
	cmpl	%ebx, %ecx	# needed, buflen.23_4
	jge	.L78	#,
# md5-crypt.c:319:       char *new_buffer = (char *) realloc (buffer, needed);
	movq	%rdx, %rdi	# <retval>,
	movslq	%ebx, %rsi	# needed, needed
	call	realloc@PLT	#
# md5-crypt.c:320:       if (new_buffer == NULL)
	testq	%rax, %rax	# <retval>
# md5-crypt.c:319:       char *new_buffer = (char *) realloc (buffer, needed);
	movq	%rax, %rdx	#, <retval>
# md5-crypt.c:320:       if (new_buffer == NULL)
	je	.L77	#,
# md5-crypt.c:323:       buffer = new_buffer;
	movq	%rax, buffer(%rip)	# <retval>, buffer
# md5-crypt.c:324:       buflen = needed;
	movl	%ebx, buflen.5407(%rip)	# needed, buflen
	movl	%ebx, %ecx	# needed, buflen.23_4
.L78:
# md5-crypt.c:328: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# md5-crypt.c:327:   return __md5_crypt_r (key, salt, buffer, buflen);
	movq	%rbp, %rsi	# salt,
	movq	%r12, %rdi	# key,
# md5-crypt.c:328: }
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
# md5-crypt.c:327:   return __md5_crypt_r (key, salt, buffer, buflen);
	jmp	__md5_crypt_r	#
	.p2align 4,,10
	.p2align 3
.L77:
	.cfi_restore_state
# md5-crypt.c:328: }
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax	#
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE42:
	.size	__md5_crypt, .-__md5_crypt
	.local	buflen.5407
	.comm	buflen.5407,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.section	.rodata.str1.1
	.type	md5_salt_prefix, @object
	.size	md5_salt_prefix, 4
md5_salt_prefix:
	.string	"$1$"
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
