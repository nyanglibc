	.file	"crypt_util.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/crypt_util.shared.v.d
# -MF /run/asm/crypt/crypt_util.os.dt -MP -MT /run/asm/crypt/.os
# -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC -D SHARED
# -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h crypt_util.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/crypt/crypt_util.shared.v.s -O2
# -Wall -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
#APP
	.symver encrypt_r,encrypt_r@GLIBC_2.2.5
	.symver encrypt,encrypt@GLIBC_2.2.5
	.symver setkey_r,setkey_r@GLIBC_2.2.5
	.symver setkey,setkey@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__init_des_r
	.type	__init_des_r, @function
__init_des_r:
.LFB47:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
# crypt_util.c:346:   sb[0] = (long64*)__data->sb0; sb[1] = (long64*)__data->sb1;
	leaq	32896(%rdi), %rax	#, tmp400
# crypt_util.c:331: {
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
# crypt_util.c:346:   sb[0] = (long64*)__data->sb0; sb[1] = (long64*)__data->sb1;
	leaq	128(%rdi), %rbp	#, _1
# crypt_util.c:331: {
	subq	$344, %rsp	#,
	.cfi_def_cfa_offset 400
# crypt_util.c:346:   sb[0] = (long64*)__data->sb0; sb[1] = (long64*)__data->sb1;
	movq	%rax, 56(%rsp)	# tmp400, sb
# crypt_util.c:347:   sb[2] = (long64*)__data->sb2; sb[3] = (long64*)__data->sb3;
	leaq	65664(%rdi), %rax	#, tmp401
# crypt_util.c:331: {
	movq	%rdi, 40(%rsp)	# __data, %sfp
# crypt_util.c:346:   sb[0] = (long64*)__data->sb0; sb[1] = (long64*)__data->sb1;
	movq	%rbp, 48(%rsp)	# _1, sb
# crypt_util.c:347:   sb[2] = (long64*)__data->sb2; sb[3] = (long64*)__data->sb3;
	movq	%rax, 64(%rsp)	# tmp401, sb
	leaq	98432(%rdi), %rax	#, tmp402
	movq	%rax, 72(%rsp)	# tmp402, sb
# crypt_util.c:350:   if(small_tables_initialized == 0) {
	movl	small_tables_initialized.7490(%rip), %eax	# small_tables_initialized, small_tables_initialized.0_5
	testl	%eax, %eax	# small_tables_initialized.0_5
	jne	.L2	#,
# crypt_util.c:352:     __libc_lock_lock (_ufc_tables_lock);
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)	#,
	je	.L3	#,
# crypt_util.c:352:     __libc_lock_lock (_ufc_tables_lock);
	leaq	_ufc_tables_lock(%rip), %rdi	#,
	call	__pthread_mutex_lock@PLT	#
.L3:
# crypt_util.c:353:     if(small_tables_initialized)
	movl	small_tables_initialized.7490(%rip), %eax	# small_tables_initialized, small_tables_initialized.2_6
	leaq	eperm32tab(%rip), %rbx	#, tmp698
	testl	%eax, %eax	# small_tables_initialized.2_6
	je	.L55	#,
.L4:
# crypt_util.c:460:     __libc_lock_unlock(_ufc_tables_lock);
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)	#,
	je	.L20	#,
# crypt_util.c:460:     __libc_lock_unlock(_ufc_tables_lock);
	leaq	_ufc_tables_lock(%rip), %rdi	#,
	call	__pthread_mutex_unlock@PLT	#
.L20:
# crypt_util.c:481:     _ufc_clearmem(__data->sb0,
	movl	$131072, %edx	#,
	xorl	%esi, %esi	#
	movq	%rbp, %rdi	# _1,
	call	memset@PLT	#
	leaq	sbox(%rip), %r12	#, tmp694
	leaq	48(%rsp), %rax	#, tmp835
	movl	$24, %r13d	#, ivtmp.87
	movl	$0, 28(%rsp)	#, %sfp
	movl	$15, 24(%rsp)	#, %sfp
	movl	%r13d, %ecx	# ivtmp.87, ivtmp.87
	movq	%rax, 32(%rsp)	# tmp835, %sfp
	movq	%r12, 16(%rsp)	# tmp694, %sfp
	movl	$14, %eax	#, pretmp_510
.L21:
	movslq	28(%rsp), %rdx	# %sfp,
# crypt_util.c:519: 	sb[sg][inx]  =
	movq	32(%rsp), %rdi	# %sfp, ivtmp.81
# crypt_util.c:497:     for(j1 = 0; j1 < 64; j1++) {
	movl	$0, 4(%rsp)	#, %sfp
# crypt_util.c:519: 	sb[sg][inx]  =
	movq	(%rdi), %r14	# MEM[base: _454, offset: 0B], _78
	leal	1(%rdx), %r15d	#, _451
	leaq	0(,%rdx,4), %rdi	#, tmp712
	movslq	%r15d, %r15	# _451, _451
	movq	%rdi, 8(%rsp)	# tmp712, %sfp
	salq	$2, %r15	#, tmp707
	.p2align 4,,10
	.p2align 3
.L26:
	movl	4(%rsp), %ebp	# %sfp, _465
# crypt_util.c:503: 	to_permute = (((ufc_long)s1 << 4)  |
	movl	24(%rsp), %edx	# %sfp, pretmp_524
	movslq	%eax, %r11	# pretmp_510, pretmp_510
	salq	$4, %r11	#, _64
# crypt_util.c:499:       for(j2 = 0; j2 < 64; j2++) {
	xorl	%r9d, %r9d	# j2
	sall	$6, %ebp	#, _465
	movslq	%edx, %rax	# pretmp_524,
	jmp	.L23	#
	.p2align 4,,10
	.p2align 3
.L56:
	movl	%r9d, %eax	# j2, tmp680
	movl	%r9d, %esi	# j2, tmp686
	sarl	%eax	# tmp680
	andl	$1, %esi	#, tmp686
	movl	%eax, %edx	# tmp680, tmp682
	movl	%r9d, %eax	# j2, tmp684
	sarl	$4, %eax	#, tmp684
	andl	$15, %edx	#, tmp682
	andl	$2, %eax	#, tmp685
	orl	%esi, %eax	# tmp686, tmp687
	cltq
	addq	%r15, %rax	# tmp707, tmp690
	salq	$4, %rax	#, tmp691
	addq	%rdx, %rax	# tmp682, tmp692
	movslq	(%r12,%rax,4), %rax	# sbox,
.L23:
# crypt_util.c:503: 	to_permute = (((ufc_long)s1 << 4)  |
	orq	%r11, %rax	# _64, tmp626
# crypt_util.c:518: 	inx = ((j1 << 6)  | j2);
	movl	%r9d, %esi	# j2, tmp630
# crypt_util.c:499:       for(j2 = 0; j2 < 64; j2++) {
	addl	$1, %r9d	#, j2
# crypt_util.c:503: 	to_permute = (((ufc_long)s1 << 4)  |
	salq	%cl, %rax	# ivtmp.87, to_permute
# crypt_util.c:518: 	inx = ((j1 << 6)  | j2);
	orl	%ebp, %esi	# _465, tmp630
# crypt_util.c:523: 	  ((long64)eperm32tab[1][(to_permute >> 16) & 0xff][0] << 32) |
	movq	%rax, %rdx	# to_permute, tmp628
# crypt_util.c:520: 	  ((long64)eperm32tab[0][(to_permute >> 24) & 0xff][0] << 32) |
	movq	%rax, %r10	# to_permute, _74
# crypt_util.c:526: 	  ((long64)eperm32tab[2][(to_permute >>  8) & 0xff][0] << 32) |
	movzbl	%ah, %edi	# to_permute, _90
# crypt_util.c:523: 	  ((long64)eperm32tab[1][(to_permute >> 16) & 0xff][0] << 32) |
	shrq	$16, %rdx	#, tmp628
# crypt_util.c:520: 	  ((long64)eperm32tab[0][(to_permute >> 24) & 0xff][0] << 32) |
	shrq	$20, %r10	#, _74
# crypt_util.c:529: 	  ((long64)eperm32tab[3][(to_permute)       & 0xff][0] << 32) |
	movzbl	%al, %eax	# to_permute, _96
# crypt_util.c:523: 	  ((long64)eperm32tab[1][(to_permute >> 16) & 0xff][0] << 32) |
	movzbl	%dl, %edx	# tmp628, _83
# crypt_util.c:521: 	   (long64)eperm32tab[0][(to_permute >> 24) & 0xff][1];
	andl	$4080, %r10d	#, tmp633
# crypt_util.c:518: 	inx = ((j1 << 6)  | j2);
	movslq	%esi, %rsi	# tmp630, inx
# crypt_util.c:524: 	   (long64)eperm32tab[1][(to_permute >> 16) & 0xff][1];
	movq	%rdx, %r13	# _83, tmp637
# crypt_util.c:521: 	   (long64)eperm32tab[0][(to_permute >> 24) & 0xff][1];
	addq	%rbx, %r10	# tmp698, tmp634
# crypt_util.c:523: 	  ((long64)eperm32tab[1][(to_permute >> 16) & 0xff][0] << 32) |
	addq	$256, %rdx	#, tmp666
# crypt_util.c:524: 	   (long64)eperm32tab[1][(to_permute >> 16) & 0xff][1];
	salq	$4, %r13	#, tmp637
# crypt_util.c:528: 	sb[sg][inx] |=
	movq	8(%r10), %r8	# eperm32tab, tmp641
# crypt_util.c:523: 	  ((long64)eperm32tab[1][(to_permute >> 16) & 0xff][0] << 32) |
	salq	$4, %rdx	#, tmp667
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	4104(%rbx,%r13), %r8	# eperm32tab, tmp640
# crypt_util.c:530: 	   (long64)eperm32tab[3][(to_permute)       & 0xff][1];
	movq	%rax, %r13	# _96, tmp643
# crypt_util.c:529: 	  ((long64)eperm32tab[3][(to_permute)       & 0xff][0] << 32) |
	addq	$768, %rax	#, tmp653
# crypt_util.c:530: 	   (long64)eperm32tab[3][(to_permute)       & 0xff][1];
	salq	$4, %r13	#, tmp643
# crypt_util.c:529: 	  ((long64)eperm32tab[3][(to_permute)       & 0xff][0] << 32) |
	salq	$4, %rax	#, tmp654
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	12296(%rbx,%r13), %r8	# eperm32tab, tmp646
# crypt_util.c:527: 	   (long64)eperm32tab[2][(to_permute >>  8) & 0xff][1];
	movq	%rdi, %r13	# _90, tmp648
# crypt_util.c:529: 	  ((long64)eperm32tab[3][(to_permute)       & 0xff][0] << 32) |
	movq	(%rbx,%rax), %rax	# eperm32tab, tmp657
# crypt_util.c:527: 	   (long64)eperm32tab[2][(to_permute >>  8) & 0xff][1];
	salq	$4, %r13	#, tmp648
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	8200(%rbx,%r13), %r8	# eperm32tab, tmp651
# crypt_util.c:529: 	  ((long64)eperm32tab[3][(to_permute)       & 0xff][0] << 32) |
	salq	$32, %rax	#, tmp656
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	%rax, %r8	# tmp656, tmp658
# crypt_util.c:520: 	  ((long64)eperm32tab[0][(to_permute >> 24) & 0xff][0] << 32) |
	movq	(%r10), %rax	# eperm32tab, tmp663
	salq	$32, %rax	#, tmp662
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	%rax, %r8	# tmp662, tmp664
# crypt_util.c:523: 	  ((long64)eperm32tab[1][(to_permute >> 16) & 0xff][0] << 32) |
	movq	(%rbx,%rdx), %rax	# eperm32tab, tmp670
	salq	$32, %rax	#, tmp669
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	%rax, %r8	# tmp669, tmp671
# crypt_util.c:526: 	  ((long64)eperm32tab[2][(to_permute >>  8) & 0xff][0] << 32) |
	leaq	512(%rdi), %rax	#, tmp673
	salq	$4, %rax	#, tmp674
	movq	(%rbx,%rax), %rax	# eperm32tab, tmp677
	salq	$32, %rax	#, tmp676
# crypt_util.c:528: 	sb[sg][inx] |=
	orq	%rax, %r8	# tmp676, tmp678
# crypt_util.c:499:       for(j2 = 0; j2 < 64; j2++) {
	cmpl	$64, %r9d	#, j2
# crypt_util.c:528: 	sb[sg][inx] |=
	movq	%r8, (%r14,%rsi,8)	# tmp678, *_80
# crypt_util.c:499:       for(j2 = 0; j2 < 64; j2++) {
	jne	.L56	#,
# crypt_util.c:497:     for(j1 = 0; j1 < 64; j1++) {
	addl	$1, 4(%rsp)	#, %sfp
	movl	4(%rsp), %eax	# %sfp, j1
	cmpl	$64, %eax	#, j1
	je	.L57	#,
	movl	4(%rsp), %edi	# %sfp, j1
	movl	%edi, %eax	# j1, tmp611
	sarl	%eax	# tmp611
	movl	%eax, %edx	# tmp611, tmp613
	movl	%edi, %eax	# j1, tmp615
	andl	$1, %edi	#, tmp617
	sarl	$4, %eax	#, tmp615
	andl	$15, %edx	#, tmp613
	andl	$2, %eax	#, tmp616
	orl	%edi, %eax	# tmp617, tmp618
	cltq
	addq	8(%rsp), %rax	# %sfp, tmp621
	salq	$4, %rax	#, tmp622
	addq	%rdx, %rax	# tmp613, tmp623
	movl	(%r12,%rax,4), %eax	# sbox, pretmp_510
	jmp	.L26	#
.L57:
	subl	$8, %ecx	#, ivtmp.87
	addq	$512, 16(%rsp)	#, %sfp
	addq	$8, 32(%rsp)	#, %sfp
	movq	16(%rsp), %rax	# %sfp, ivtmp.85
	addl	$2, 28(%rsp)	#, %sfp
# crypt_util.c:493:   for(sg = 0; sg < 4; sg++) {
	cmpl	$-8, %ecx	#, ivtmp.87
	je	.L25	#,
	movl	256(%rax), %edi	# MEM[base: _450, offset: 256B], prephitmp_487
	movl	(%rax), %eax	# MEM[base: _450, offset: 0B], pretmp_510
	movl	%edi, 24(%rsp)	# prephitmp_487, %sfp
	jmp	.L21	#
.L25:
# crypt_util.c:537:   __data->current_salt[0] = 0;
	movq	40(%rsp), %rax	# %sfp, __data
# crypt_util.c:536:   __data->current_saltbits = 0;
	xorl	%edx, %edx	#
# crypt_util.c:537:   __data->current_salt[0] = 0;
	movq	$0, 131214(%rax)	#, MEM[(void *)__data_129(D) + 131214B]
# crypt_util.c:536:   __data->current_saltbits = 0;
	movw	%dx, 131222(%rax)	#, MEM[(long int *)__data_129(D) + 131222B]
# crypt_util.c:539:   __data->initialized++;
	addl	$1, 131228(%rax)	#, *__data_129(D).initialized
# crypt_util.c:540: }
	addq	$344, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
.L55:
	.cfi_restore_state
# crypt_util.c:362:     _ufc_clearmem((char*)do_pc1, (int)sizeof(do_pc1));
	leaq	do_pc1(%rip), %rdi	#,
	xorl	%esi, %esi	#
	movl	$16384, %edx	#,
	leaq	bytemask(%rip), %r14	#, tmp697
	leaq	longmask(%rip), %r13	#, tmp699
	call	memset@PLT	#
	leaq	pc1(%rip), %r9	#, tmp701
	leaq	do_pc1(%rip), %r8	#, tmp704
	xorl	%esi, %esi	# ivtmp.164
# crypt_util.c:366:       mask2 = longmask[bit % 28 + 4];
	movl	$613566757, %edi	#, tmp426
.L8:
# crypt_util.c:364:       comes_from_bit  = pc1[bit] - 1;
	movl	(%r9,%rsi,4), %r11d	# MEM[symbol: pc1, index: _384, offset: 0B], MEM[symbol: pc1, index: _384, offset: 0B]
# crypt_util.c:366:       mask2 = longmask[bit % 28 + 4];
	movl	%esi, %r15d	# ivtmp.164, tmp428
# crypt_util.c:364:       comes_from_bit  = pc1[bit] - 1;
	leal	-1(%r11), %ebx	#, comes_from_bit
# crypt_util.c:365:       mask1 = bytemask[comes_from_bit % 8 + 1];
	movl	%ebx, %edx	# comes_from_bit, tmp415
	sarl	$31, %edx	#, tmp415
	shrl	$29, %edx	#, tmp416
	leal	(%rbx,%rdx), %eax	#, tmp417
	andl	$7, %eax	#, tmp418
	subl	%edx, %eax	# tmp416, tmp419
# crypt_util.c:366:       mask2 = longmask[bit % 28 + 4];
	movl	%esi, %edx	# ivtmp.164, tmp424
# crypt_util.c:365:       mask1 = bytemask[comes_from_bit % 8 + 1];
	addl	$1, %eax	#, tmp420
# crypt_util.c:366:       mask2 = longmask[bit % 28 + 4];
	shrl	$2, %edx	#, tmp424
# crypt_util.c:365:       mask1 = bytemask[comes_from_bit % 8 + 1];
	cltq
	movzbl	(%r14,%rax), %ecx	# bytemask, mask1
# crypt_util.c:366:       mask2 = longmask[bit % 28 + 4];
	movl	%edx, %eax	# tmp424, tmp424
	mull	%edi	# tmp426
	imull	$28, %edx, %eax	#, tmp425, tmp427
	movslq	%edx, %rdx	# tmp425, tmp455
	subl	%eax, %r15d	# tmp427, tmp428
	movl	%r15d, %eax	# tmp428, tmp428
	addl	$4, %eax	#, tmp437
# crypt_util.c:369: 	  do_pc1[comes_from_bit / 8][bit / 28][j] |= mask2;
	testl	%ebx, %ebx	# comes_from_bit
# crypt_util.c:366:       mask2 = longmask[bit % 28 + 4];
	cltq
	movq	0(%r13,%rax,8), %r10	# longmask, mask2
# crypt_util.c:369: 	  do_pc1[comes_from_bit / 8][bit / 28][j] |= mask2;
	leal	6(%r11), %eax	#, tmp441
	cmovns	%ebx, %eax	# tmp441,, comes_from_bit, comes_from_bit
	sarl	$3, %eax	#, tmp442
	cltq
	leaq	(%rdx,%rax,2), %rdx	#, tmp456
# crypt_util.c:367:       for(j = 0; j < 128; j++) {
	xorl	%eax, %eax	# j
	salq	$10, %rdx	#, tmp457
	addq	%r8, %rdx	# tmp704, _394
	.p2align 4,,10
	.p2align 3
.L5:
# crypt_util.c:367:       for(j = 0; j < 128; j++) {
	addq	$1, %rax	#, j
	cmpq	$128, %rax	#, j
	je	.L58	#,
.L7:
# crypt_util.c:368: 	if(j & mask1)
	testq	%rax, %rcx	# j, mask1
	je	.L5	#,
# crypt_util.c:369: 	  do_pc1[comes_from_bit / 8][bit / 28][j] |= mask2;
	orq	%r10, (%rdx,%rax,8)	# mask2, MEM[base: _394, index: _395, offset: 0B]
# crypt_util.c:367:       for(j = 0; j < 128; j++) {
	addq	$1, %rax	#, j
	cmpq	$128, %rax	#, j
	jne	.L7	#,
.L58:
	addq	$1, %rsi	#, ivtmp.164
# crypt_util.c:363:     for(bit = 0; bit < 56; bit++) {
	cmpq	$56, %rsi	#, ivtmp.164
	jne	.L8	#,
# crypt_util.c:378:     _ufc_clearmem((char*)do_pc2, (int)sizeof(do_pc2));
	leaq	do_pc2(%rip), %r8	#, tmp705
	movl	$1024, %ecx	#, tmp463
	xorl	%eax, %eax	# tmp462
	leaq	pc2(%rip), %r10	#, tmp702
	leaq	BITMASK(%rip), %r12	#, tmp703
	xorl	%esi, %esi	# ivtmp.149
	movq	%r8, %rdi	# tmp705, tmp461
# crypt_util.c:381:       mask1 = bytemask[comes_from_bit % 7 + 1];
	movl	$-1840700269, %r9d	#, tmp470
# crypt_util.c:378:     _ufc_clearmem((char*)do_pc2, (int)sizeof(do_pc2));
	rep stosq
# crypt_util.c:382:       mask2 = BITMASK[bit % 24];
	movl	$-1431655765, %edi	#, tmp484
.L12:
# crypt_util.c:380:       comes_from_bit  = pc2[bit] - 1;
	movl	(%r10,%rsi,4), %eax	# MEM[symbol: pc2, index: _401, offset: 0B], tmp764
# crypt_util.c:382:       mask2 = BITMASK[bit % 24];
	movl	%esi, %ebx	# ivtmp.149, tmp489
# crypt_util.c:380:       comes_from_bit  = pc2[bit] - 1;
	leal	-1(%rax), %r11d	#, comes_from_bit
# crypt_util.c:381:       mask1 = bytemask[comes_from_bit % 7 + 1];
	movl	%r11d, %eax	# comes_from_bit, tmp751
	imull	%r9d	# tmp470
	movl	%r11d, %eax	# comes_from_bit, tmp473
	sarl	$31, %eax	#, tmp473
	leal	(%rdx,%r11), %ecx	#, tmp471
	sarl	$2, %ecx	#, tmp472
	subl	%eax, %ecx	# tmp473, tmp468
	leal	0(,%rcx,8), %eax	#, tmp475
	subl	%ecx, %eax	# tmp468, tmp476
	subl	%eax, %r11d	# tmp476, comes_from_bit
	movl	%r11d, %eax	# comes_from_bit, tmp477
	addl	$1, %eax	#, tmp478
	cltq
	movzbl	(%r14,%rax), %r11d	# bytemask, mask1
# crypt_util.c:382:       mask2 = BITMASK[bit % 24];
	movl	%esi, %eax	# ivtmp.149, tmp752
	mull	%edi	# tmp484
	movl	%edx, %eax	# tmp483, tmp483
	movslq	%ecx, %rdx	# tmp468, tmp508
	shrl	$4, %eax	#, tmp483
	salq	$10, %rdx	#, tmp509
	leal	(%rax,%rax,2), %eax	#, tmp487
	addq	%r8, %rdx	# tmp705, _408
	sall	$3, %eax	#, tmp488
	subl	%eax, %ebx	# tmp488, tmp489
	movslq	%ebx, %rax	# tmp489,
	movq	(%r12,%rax,8), %rbx	# BITMASK, mask2
# crypt_util.c:383:       for(j = 0; j < 128; j++) {
	xorl	%eax, %eax	# j
	.p2align 4,,10
	.p2align 3
.L9:
# crypt_util.c:383:       for(j = 0; j < 128; j++) {
	addq	$1, %rax	#, j
	cmpq	$128, %rax	#, j
	je	.L59	#,
.L11:
# crypt_util.c:384: 	if(j & mask1)
	testq	%rax, %r11	# j, mask1
	je	.L9	#,
# crypt_util.c:385: 	  do_pc2[comes_from_bit / 7][j] |= mask2;
	orq	%rbx, (%rdx,%rax,8)	# mask2, MEM[base: _408, index: _409, offset: 0B]
# crypt_util.c:383:       for(j = 0; j < 128; j++) {
	addq	$1, %rax	#, j
	cmpq	$128, %rax	#, j
	jne	.L11	#,
.L59:
	addq	$1, %rsi	#, ivtmp.149
# crypt_util.c:379:     for(bit = 0; bit < 48; bit++) {
	cmpq	$48, %rsi	#, ivtmp.149
	jne	.L12	#,
# crypt_util.c:401:     _ufc_clearmem((char*)eperm32tab, (int)sizeof(eperm32tab));
	leaq	eperm32tab(%rip), %rdi	#,
	xorl	%esi, %esi	#
	movl	$16384, %edx	#,
	leaq	eperm32tab(%rip), %rbx	#, tmp698
	call	memset@PLT	#
	leaq	perm32(%rip), %r9	#, tmp693
	leaq	esel(%rip), %rdi	#, tmp696
	xorl	%esi, %esi	# ivtmp.134
# crypt_util.c:408: 	  eperm32tab[comes_from / 8][j][bit / 24] |= BITMASK[bit % 24];
	movl	$-1431655765, %r8d	#, tmp528
.L15:
# crypt_util.c:404:       comes_from = perm32[esel[bit]-1]-1;
	movl	(%rdi,%rsi,4), %eax	# MEM[symbol: esel, index: _412, offset: 0B], tmp771
	subl	$1, %eax	#, tmp520
	cltq
	movl	(%r9,%rax,4), %r10d	# perm32, tmp523
	subl	$1, %r10d	#, _27
# crypt_util.c:405:       mask1      = bytemask[comes_from % 8];
	movl	%r10d, %eax	# _27, tmp526
	andl	$7, %eax	#, tmp526
	movzbl	(%r14,%rax), %ecx	# bytemask, mask1
# crypt_util.c:408: 	  eperm32tab[comes_from / 8][j][bit / 24] |= BITMASK[bit % 24];
	movl	%esi, %eax	# ivtmp.134, tmp753
	mull	%r8d	# tmp528
	movl	%edx, %r15d	# tmp527, tmp527
	shrl	$4, %r15d	#, _32
	movslq	%r15d, %rdx	# _32,
	movl	%esi, %r15d	# ivtmp.134, _32
	leal	(%rdx,%rdx,2), %eax	#, tmp531
	sall	$3, %eax	#, tmp532
	subl	%eax, %r15d	# tmp532, _32
# crypt_util.c:404:       comes_from = perm32[esel[bit]-1]-1;
	movslq	%r10d, %rax	# _27, comes_from
# crypt_util.c:408: 	  eperm32tab[comes_from / 8][j][bit / 24] |= BITMASK[bit % 24];
	shrq	$3, %rax	#, tmp542
	movslq	%r15d, %r15	# _32, _32
	salq	$9, %rax	#, tmp543
	addq	%rdx, %rax	# _32, tmp553
	leaq	(%rbx,%rax,8), %r10	#, _421
# crypt_util.c:406:       for(j = 256; j--;) {
	movl	$255, %eax	#, j
	.p2align 4,,10
	.p2align 3
.L14:
# crypt_util.c:407: 	if(j & mask1)
	testq	%rax, %rcx	# j, mask1
	je	.L13	#,
	movq	%rax, %rdx	# j, _422
# crypt_util.c:408: 	  eperm32tab[comes_from / 8][j][bit / 24] |= BITMASK[bit % 24];
	movq	(%r12,%r15,8), %r11	# BITMASK, tmp559
	salq	$4, %rdx	#, _422
	orq	%r11, (%r10,%rdx)	# tmp559, MEM[base: _421, index: _422, offset: 0B]
.L13:
# crypt_util.c:406:       for(j = 256; j--;) {
	subq	$1, %rax	#, j
	cmpq	$-1, %rax	#, j
	jne	.L14	#,
	addq	$1, %rsi	#, ivtmp.134
# crypt_util.c:402:     for(bit = 0; bit < 48; bit++) {
	cmpq	$48, %rsi	#, ivtmp.134
	jne	.L15	#,
	movl	$47, %eax	#, ivtmp.119
.L16:
# crypt_util.c:417:       e_inverse[esel[bit] - 1     ] = bit;
	movl	(%rdi,%rax,4), %edx	# MEM[symbol: esel, index: _430, offset: 0B], _36
	leal	-1(%rdx), %ecx	#, tmp562
# crypt_util.c:418:       e_inverse[esel[bit] - 1 + 32] = bit + 48;
	addl	$31, %edx	#, tmp564
	movslq	%edx, %rdx	# tmp564, tmp565
# crypt_util.c:417:       e_inverse[esel[bit] - 1     ] = bit;
	movslq	%ecx, %rcx	# tmp562, tmp563
	movl	%eax, 80(%rsp,%rcx,4)	# ivtmp.119, e_inverse
# crypt_util.c:418:       e_inverse[esel[bit] - 1 + 32] = bit + 48;
	leal	48(%rax), %ecx	#, tmp566
	subq	$1, %rax	#, ivtmp.119
# crypt_util.c:416:     for(bit=48; bit--;) {
	cmpq	$-1, %rax	#, ivtmp.119
# crypt_util.c:418:       e_inverse[esel[bit] - 1 + 32] = bit + 48;
	movl	%ecx, 80(%rsp,%rdx,4)	# tmp566, e_inverse
# crypt_util.c:416:     for(bit=48; bit--;) {
	jne	.L16	#,
# crypt_util.c:425:     _ufc_clearmem((char*)efp, (int)sizeof efp);
	leaq	efp(%rip), %rdi	#,
	movl	$16384, %edx	#,
	xorl	%esi, %esi	#
	call	memset@PLT	#
	leaq	final_perm(%rip), %r11	#, tmp700
	leaq	efp(%rip), %r10	#, tmp695
	xorl	%r8d, %r8d	# ivtmp.108
# crypt_util.c:446:       bit_within_word  = comes_from_e_bit % 6;        /* 0..5  */
	movl	$715827883, %r9d	#, tmp580
.L19:
# crypt_util.c:443:       comes_from_f_bit = final_perm[bit] - 1;         /* 0..63 */
	movl	(%r11,%r8,4), %eax	# MEM[symbol: final_perm, index: _433, offset: 0B], tmp777
	subl	$1, %eax	#, comes_from_f_bit
# crypt_util.c:444:       comes_from_e_bit = e_inverse[comes_from_f_bit]; /* 0..95 */
	cltq
	movl	80(%rsp,%rax,4), %ecx	# e_inverse, comes_from_e_bit
# crypt_util.c:446:       bit_within_word  = comes_from_e_bit % 6;        /* 0..5  */
	movl	%ecx, %eax	# comes_from_e_bit, tmp754
	imull	%r9d	# tmp580
	movl	%ecx, %eax	# comes_from_e_bit, tmp581
	sarl	$31, %eax	#, tmp581
	subl	%eax, %edx	# tmp581, tmp579
	movslq	%edx, %rax	# tmp579,
	leal	(%rax,%rax,2), %edx	#, tmp584
	salq	$7, %rax	#, tmp597
	addl	%edx, %edx	# tmp585
	subl	%edx, %ecx	# tmp585, comes_from_e_bit
	movl	%ecx, %edx	# comes_from_e_bit, bit_within_word
# crypt_util.c:448:       mask1 = longmask[bit_within_word + 26];
	addl	$26, %edx	#, tmp587
	movslq	%edx, %rdx	# tmp587, tmp588
	movq	0(%r13,%rdx,8), %rsi	# longmask, mask1
# crypt_util.c:449:       mask2 = longmask[o_bit];
	movq	%r8, %rdx	# ivtmp.108, o_bit
	andl	$31, %edx	#, o_bit
	movq	0(%r13,%rdx,8), %rdi	# longmask, mask2
# crypt_util.c:433:       o_long = bit / 32; /* 0..1  */
	movl	%r8d, %edx	# ivtmp.108, o_long
	sarl	$5, %edx	#, o_long
	movslq	%edx, %rdx	# o_long, o_long
	addq	%rdx, %rax	# o_long, tmp600
	leaq	(%r10,%rax,8), %rcx	#, _443
# crypt_util.c:451:       for(word_value = 64; word_value--;) {
	movl	$63, %eax	#, word_value
	.p2align 4,,10
	.p2align 3
.L18:
# crypt_util.c:452: 	if(word_value & mask1)
	testq	%rax, %rsi	# word_value, mask1
	je	.L17	#,
	movq	%rax, %rdx	# word_value, _444
	salq	$4, %rdx	#, _444
# crypt_util.c:453: 	  efp[comes_from_word][word_value][o_long] |= mask2;
	orq	%rdi, (%rcx,%rdx)	# mask2, MEM[base: _443, index: _444, offset: 0B]
.L17:
# crypt_util.c:451:       for(word_value = 64; word_value--;) {
	subq	$1, %rax	#, word_value
	cmpq	$-1, %rax	#, word_value
	jne	.L18	#,
	addq	$1, %r8	#, ivtmp.108
# crypt_util.c:426:     for(bit = 0; bit < 64; bit++) {
	cmpq	$64, %r8	#, ivtmp.108
	jne	.L19	#,
# crypt_util.c:456:     atomic_write_barrier ();
# crypt_util.c:457:     small_tables_initialized = 1;
	movl	$1, small_tables_initialized.7490(%rip)	#, small_tables_initialized
	jmp	.L4	#
.L2:
# crypt_util.c:463:     atomic_read_barrier ();
	leaq	eperm32tab(%rip), %rbx	#, tmp698
	jmp	.L20	#
	.cfi_endproc
.LFE47:
	.size	__init_des_r, .-__init_des_r
	.p2align 4,,15
	.globl	__init_des
	.type	__init_des, @function
__init_des:
.LFB48:
	.cfi_startproc
# crypt_util.c:545:   __init_des_r(&_ufc_foobar);
	movq	_ufc_foobar@GOTPCREL(%rip), %rdi	#,
	jmp	__init_des_r@PLT	#
	.cfi_endproc
.LFE48:
	.size	__init_des, .-__init_des
	.p2align 4,,15
	.globl	_ufc_setup_salt_r
	.type	_ufc_setup_salt_r, @function
_ufc_setup_salt_r:
.LFB51:
	.cfi_startproc
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rdi, %rbx	# s, s
	subq	$16, %rsp	#,
	.cfi_def_cfa_offset 32
# crypt_util.c:612:   if(__data->initialized == 0)
	movl	131228(%rsi), %eax	# *__data_10(D).initialized,
	testl	%eax, %eax	#
	je	.L92	#,
.L62:
# crypt_util.c:615:   s0 = s[0];
	movzbl	(%rbx), %edx	# *s_12(D), s0
# crypt_util.c:587:   switch (c)
	cmpb	$90, %dl	#, s0
	jg	.L64	#,
	cmpb	$65, %dl	#, s0
	jge	.L65	#,
	leal	-46(%rdx), %eax	#, tmp191
	cmpb	$11, %al	#, tmp191
	jbe	.L65	#,
.L80:
# crypt_util.c:617:     return false;
	xorl	%eax, %eax	# <retval>
.L61:
# crypt_util.c:663: }
	addq	$16, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 16
	popq	%rbx	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
# crypt_util.c:587:   switch (c)
	leal	-97(%rdx), %eax	#, tmp192
	cmpb	$25, %al	#, tmp192
	ja	.L80	#,
.L65:
# crypt_util.c:619:   s1 = s[1];
	movzbl	1(%rbx), %ecx	# MEM[(const char *)s_12(D) + 1B], s1
# crypt_util.c:587:   switch (c)
	cmpb	$90, %cl	#, s1
	jg	.L66	#,
	cmpb	$65, %cl	#, s1
	jge	.L67	#,
	leal	-46(%rcx), %eax	#, tmp193
	cmpb	$11, %al	#, tmp193
	ja	.L80	#,
.L67:
# crypt_util.c:623:   if(s0 == __data->current_salt[0] && s1 == __data->current_salt[1])
	cmpb	%dl, 131214(%rsi)	# s0, *__data_10(D).current_salt
	je	.L93	#,
.L68:
# crypt_util.c:639: 	saltbits |= BITMASK[6 * i + j];
	leaq	BITMASK(%rip), %r9	#, tmp232
# crypt_util.c:627:   __data->current_salt[1] = s1;
	movb	%cl, 131215(%rsi)	# s1, *__data_10(D).current_salt
# crypt_util.c:626:   __data->current_salt[0] = s0;
	movb	%dl, 131214(%rsi)	# s0, *__data_10(D).current_salt
# crypt_util.c:627:   __data->current_salt[1] = s1;
	xorl	%edi, %edi	# ivtmp.224
# crypt_util.c:634:   saltbits = 0;
	xorl	%ecx, %ecx	# saltbits
.L74:
# crypt_util.c:636:     long c=ascii_to_bin(s[i]);
	movsbl	(%rbx), %eax	# MEM[base: _146, offset: 0B],
	cmpb	$96, %al	#, _19
	jle	.L69	#,
	subl	$59, %eax	#, tmp195
	movslq	%eax, %rdx	# tmp195, iftmp.7_22
.L70:
# crypt_util.c:637:     for(j = 0; j < 6; j++) {
	xorl	%eax, %eax	# j
	.p2align 4,,10
	.p2align 3
.L73:
# crypt_util.c:638:       if((c >> j) & 0x1)
	btq	%rax, %rdx	# j, iftmp.7_22
	jnc	.L72	#,
# crypt_util.c:639: 	saltbits |= BITMASK[6 * i + j];
	leaq	(%rax,%rdi), %r8	#, tmp201
	orq	(%r9,%r8,8), %rcx	# MEM[symbol: BITMASK, index: _152, offset: 0B], saltbits
.L72:
# crypt_util.c:637:     for(j = 0; j < 6; j++) {
	addq	$1, %rax	#, j
	cmpq	$6, %rax	#, j
	jne	.L73	#,
	addq	$6, %rdi	#, ivtmp.224
	addq	$1, %rbx	#, ivtmp.223
# crypt_util.c:635:   for(i = 0; i < 2; i++) {
	cmpq	$12, %rdi	#, ivtmp.224
	jne	.L74	#,
# crypt_util.c:655:   shuffle_sb((LONGG)__data->sb0, __data->current_saltbits ^ saltbits);
	movq	131216(%rsi), %r10	# *__data_10(D).current_saltbits, _44
	leaq	128(%rsi), %rax	#, k
	leaq	32896(%rsi), %r9	#, k
	xorq	%rcx, %r10	# saltbits, _44
	.p2align 4,,10
	.p2align 3
.L75:
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	(%rax), %r8	# MEM[base: k_129, offset: 0B], _47
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	addq	$8, %rax	#, k
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	%r8, %rdx	# _47, tmp204
	shrq	$32, %rdx	#, tmp204
	xorq	%r8, %rdx	# _47, tmp205
	andq	%r10, %rdx	# _44, x
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, %rdi	# x, tmp206
	salq	$32, %rdi	#, tmp206
	orq	%rdi, %rdx	# tmp206, tmp207
	xorq	%r8, %rdx	# _47, tmp208
# crypt_util.c:573:   for(j=4096; j--;) {
	cmpq	%r9, %rax	# k, k
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, -8(%rax)	# tmp208, MEM[base: k_51, offset: -8B]
# crypt_util.c:573:   for(j=4096; j--;) {
	jne	.L75	#,
# crypt_util.c:656:   shuffle_sb((LONGG)__data->sb1, __data->current_saltbits ^ saltbits);
	movq	131216(%rsi), %r10	# *__data_10(D).current_saltbits, _59
	leaq	65664(%rsi), %r9	#, k
	xorq	%rcx, %r10	# saltbits, _59
	.p2align 4,,10
	.p2align 3
.L76:
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	(%rax), %r8	# MEM[base: k_128, offset: 0B], _62
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	addq	$8, %rax	#, k
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	%r8, %rdx	# _62, tmp209
	shrq	$32, %rdx	#, tmp209
	xorq	%r8, %rdx	# _62, tmp210
	andq	%r10, %rdx	# _59, x
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, %rdi	# x, tmp211
	salq	$32, %rdi	#, tmp211
	orq	%rdi, %rdx	# tmp211, tmp212
	xorq	%r8, %rdx	# _62, tmp213
# crypt_util.c:573:   for(j=4096; j--;) {
	cmpq	%r9, %rax	# k, k
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, -8(%rax)	# tmp213, MEM[base: k_66, offset: -8B]
# crypt_util.c:573:   for(j=4096; j--;) {
	jne	.L76	#,
# crypt_util.c:657:   shuffle_sb((LONGG)__data->sb2, __data->current_saltbits ^ saltbits);
	movq	131216(%rsi), %r10	# *__data_10(D).current_saltbits, _74
	leaq	98432(%rsi), %r9	#, k
	xorq	%rcx, %r10	# saltbits, _74
	.p2align 4,,10
	.p2align 3
.L77:
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	(%rax), %r8	# MEM[base: k_126, offset: 0B], _77
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	addq	$8, %rax	#, k
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	%r8, %rdx	# _77, tmp214
	shrq	$32, %rdx	#, tmp214
	xorq	%r8, %rdx	# _77, tmp215
	andq	%r10, %rdx	# _74, x
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, %rdi	# x, tmp216
	salq	$32, %rdi	#, tmp216
	orq	%rdi, %rdx	# tmp216, tmp217
	xorq	%r8, %rdx	# _77, tmp218
# crypt_util.c:573:   for(j=4096; j--;) {
	cmpq	%r9, %rax	# k, k
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, -8(%rax)	# tmp218, MEM[base: k_81, offset: -8B]
# crypt_util.c:573:   for(j=4096; j--;) {
	jne	.L77	#,
# crypt_util.c:658:   shuffle_sb((LONGG)__data->sb3, __data->current_saltbits ^ saltbits);
	movq	131216(%rsi), %r10	# *__data_10(D).current_saltbits, _89
	leaq	131200(%rsi), %r9	#, _157
	xorq	%rcx, %r10	# saltbits, _89
	.p2align 4,,10
	.p2align 3
.L78:
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	(%rax), %r8	# MEM[base: k_124, offset: 0B], _92
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	addq	$8, %rax	#, k
# crypt_util.c:574:     x = ((*k >> 32) ^ *k) & (long64)saltbits;
	movq	%r8, %rdx	# _92, tmp219
	shrq	$32, %rdx	#, tmp219
	xorq	%r8, %rdx	# _92, tmp220
	andq	%r10, %rdx	# _89, x
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, %rdi	# x, tmp221
	salq	$32, %rdi	#, tmp221
	orq	%rdi, %rdx	# tmp221, tmp222
	xorq	%r8, %rdx	# _92, tmp223
# crypt_util.c:573:   for(j=4096; j--;) {
	cmpq	%r9, %rax	# _157, k
# crypt_util.c:575:     *k++ ^= (x << 32) | x;
	movq	%rdx, -8(%rax)	# tmp223, MEM[base: k_96, offset: -8B]
# crypt_util.c:573:   for(j=4096; j--;) {
	jne	.L78	#,
# crypt_util.c:660:   __data->current_saltbits = saltbits;
	movq	%rcx, 131216(%rsi)	# saltbits, *__data_10(D).current_saltbits
# crypt_util.c:663: }
	addq	$16, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 16
# crypt_util.c:660:   __data->current_saltbits = saltbits;
	movl	$1, %eax	#, <retval>
# crypt_util.c:663: }
	popq	%rbx	#
	.cfi_def_cfa_offset 8
	ret
.L66:
	.cfi_restore_state
# crypt_util.c:587:   switch (c)
	leal	-97(%rcx), %eax	#, tmp194
	cmpb	$25, %al	#, tmp194
	ja	.L80	#,
	jmp	.L67	#
	.p2align 4,,10
	.p2align 3
.L92:
# crypt_util.c:613:     __init_des_r(__data);
	movq	%rsi, %rdi	# __data,
	movq	%rsi, 8(%rsp)	# __data, %sfp
	call	__init_des_r@PLT	#
	movq	8(%rsp), %rsi	# %sfp, __data
	jmp	.L62	#
	.p2align 4,,10
	.p2align 3
.L69:
# crypt_util.c:636:     long c=ascii_to_bin(s[i]);
	cmpb	$64, %al	#, _19
	jle	.L71	#,
	subl	$53, %eax	#, tmp196
	movslq	%eax, %rdx	# tmp196, iftmp.7_22
	jmp	.L70	#
.L71:
	subl	$46, %eax	#, tmp197
	movslq	%eax, %rdx	# tmp197, iftmp.7_22
	jmp	.L70	#
.L93:
# crypt_util.c:623:   if(s0 == __data->current_salt[0] && s1 == __data->current_salt[1])
	cmpb	%cl, 131215(%rsi)	# s1, *__data_10(D).current_salt
# crypt_util.c:624:     return true;
	movl	$1, %eax	#, <retval>
# crypt_util.c:623:   if(s0 == __data->current_salt[0] && s1 == __data->current_salt[1])
	jne	.L68	#,
	jmp	.L61	#
	.cfi_endproc
.LFE51:
	.size	_ufc_setup_salt_r, .-_ufc_setup_salt_r
	.p2align 4,,15
	.globl	_ufc_mk_keytab_r
	.type	_ufc_mk_keytab_r, @function
_ufc_mk_keytab_r:
.LFB52:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	leaq	8(%rdi), %r9	#, _311
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
# crypt_util.c:679:   v1 = v2 = 0; k1 = &do_pc1[0][0][0];
	leaq	do_pc1(%rip), %rcx	#, k1
# crypt_util.c:667: {
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
# crypt_util.c:679:   v1 = v2 = 0; k1 = &do_pc1[0][0][0];
	xorl	%r8d, %r8d	# v2
	xorl	%edx, %edx	# v1
	.p2align 4,,10
	.p2align 3
.L95:
# crypt_util.c:681:     v1 |= k1[*key   & 0x7f]; k1 += 128;
	movzbl	(%rdi), %eax	# MEM[base: key_96, offset: 0B], MEM[base: key_96, offset: 0B]
# crypt_util.c:682:     v2 |= k1[*key++ & 0x7f]; k1 += 128;
	addq	$1, %rdi	#, key
# crypt_util.c:681:     v1 |= k1[*key   & 0x7f]; k1 += 128;
	andl	$127, %eax	#, tmp178
	orq	(%rcx,%rax,8), %rdx	# *_4, v1
# crypt_util.c:682:     v2 |= k1[*key++ & 0x7f]; k1 += 128;
	orq	1024(%rcx,%rax,8), %r8	# *_6, v2
	addq	$2048, %rcx	#, k1
# crypt_util.c:680:   for(i = 8; i--;) {
	cmpq	%r9, %rdi	# _311, key
	jne	.L95	#,
	leaq	1024+do_pc2(%rip), %r15	#, tmp219
	xorl	%edi, %edi	# ivtmp.249
	movl	$1, %r9d	#, pretmp_321
	leaq	-1024(%r15), %r14	#, tmp217
	leaq	2048(%r14), %r12	#, tmp218
	leaq	3072(%r14), %r13	#, tmp216
	leaq	2048(%r12), %rbp	#, tmp215
	leaq	3072(%rbp), %rbx	#, tmp212
	jmp	.L97	#
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	rots(%rip), %rax	#, tmp249
	movl	(%rax,%rdi), %r9d	# MEM[symbol: rots, index: ivtmp.249_312, offset: 0B], pretmp_321
.L97:
# crypt_util.c:688:     v1 = (v1 << rots[i]) | (v1 >> (28 - rots[i]));
	movl	$28, %eax	#, _9
	movl	%r9d, %ecx	# pretmp_321, tmp233
	movq	%rdx, %r10	# v1, _8
	subl	%r9d, %eax	# pretmp_321, _9
	salq	%cl, %r10	# tmp233, _8
# crypt_util.c:702:     v2 = (v2 << rots[i]) | (v2 >> (28 - rots[i]));
	movq	%r8, %r11	# v2, _29
# crypt_util.c:688:     v1 = (v1 << rots[i]) | (v1 >> (28 - rots[i]));
	movl	%eax, %ecx	# _9, tmp235
	shrq	%cl, %rdx	# tmp235, _10
	orq	%r10, %rdx	# _8, v1
# crypt_util.c:690:     v |= k1[(v1 >> 14) & 0x7f]; k1 += 128;
	movq	%rdx, %r10	# v1, tmp180
# crypt_util.c:689:     v  = k1[(v1 >> 21) & 0x7f]; k1 += 128;
	movq	%rdx, %rcx	# v1, tmp183
# crypt_util.c:690:     v |= k1[(v1 >> 14) & 0x7f]; k1 += 128;
	shrq	$14, %r10	#, tmp180
# crypt_util.c:689:     v  = k1[(v1 >> 21) & 0x7f]; k1 += 128;
	shrq	$21, %rcx	#, tmp183
	andl	$127, %ecx	#, tmp184
# crypt_util.c:690:     v |= k1[(v1 >> 14) & 0x7f]; k1 += 128;
	andl	$127, %r10d	#, tmp181
	movq	(%r15,%r10,8), %r10	# *_18, *_18
	orq	(%r14,%rcx,8), %r10	# *_14, tmp186
# crypt_util.c:692:     v |= k1[(v1      ) & 0x7f]; k1 += 128;
	movq	%rdx, %rcx	# v1, tmp188
	andl	$127, %ecx	#, tmp188
	orq	0(%r13,%rcx,8), %r10	# *_27, _52
# crypt_util.c:691:     v |= k1[(v1 >>  7) & 0x7f]; k1 += 128;
	movq	%rdx, %rcx	# v1, tmp190
	shrq	$7, %rcx	#, tmp190
	andl	$127, %ecx	#, tmp191
# crypt_util.c:692:     v |= k1[(v1      ) & 0x7f]; k1 += 128;
	orq	(%r12,%rcx,8), %r10	# *_23, v
# crypt_util.c:702:     v2 = (v2 << rots[i]) | (v2 >> (28 - rots[i]));
	movl	%r9d, %ecx	# pretmp_321, tmp241
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	leaq	5120+do_pc2(%rip), %r9	#, tmp250
# crypt_util.c:702:     v2 = (v2 << rots[i]) | (v2 >> (28 - rots[i]));
	salq	%cl, %r11	# tmp241, _29
	movl	%eax, %ecx	# _9, tmp242
	shrq	%cl, %r8	# tmp242, _30
	orq	%r11, %r8	# _29, v2
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	movabsq	$140737488388096, %r11	#, tmp198
# crypt_util.c:703:     v |= k1[(v2 >> 21) & 0x7f]; k1 += 128;
	movq	%r8, %rcx	# v2, tmp193
# crypt_util.c:706:     v |= k1[(v2      ) & 0x7f];
	movq	%r8, %rax	# v2, tmp196
# crypt_util.c:699:     v = (v << 32);
	salq	$32, %r10	#, v
# crypt_util.c:706:     v |= k1[(v2      ) & 0x7f];
	andl	$127, %eax	#, tmp196
# crypt_util.c:703:     v |= k1[(v2 >> 21) & 0x7f]; k1 += 128;
	shrq	$21, %rcx	#, tmp193
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	orq	(%rbx,%rax,8), %r11	# *_48, tmp198
# crypt_util.c:703:     v |= k1[(v2 >> 21) & 0x7f]; k1 += 128;
	andl	$127, %ecx	#, tmp194
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	movq	0(%rbp,%rcx,8), %rax	# *_34, tmp198
# crypt_util.c:704:     v |= k1[(v2 >> 14) & 0x7f]; k1 += 128;
	movq	%r8, %rcx	# v2, tmp201
	shrq	$14, %rcx	#, tmp201
	andl	$127, %ecx	#, tmp202
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	orq	%r11, %rax	# tmp198, tmp198
	orq	(%r9,%rcx,8), %rax	# *_39, tmp204
# crypt_util.c:705:     v |= k1[(v2 >>  7) & 0x7f]; k1 += 128;
	movq	%r8, %rcx	# v2, tmp205
	shrq	$7, %rcx	#, tmp205
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	leaq	1024(%r9), %r11	#, tmp248
# crypt_util.c:705:     v |= k1[(v2 >>  7) & 0x7f]; k1 += 128;
	andl	$127, %ecx	#, tmp206
# crypt_util.c:712:     *k2++ = v | 0x0000800000008000l;
	orq	(%r11,%rcx,8), %rax	# *_44, tmp208
	orq	%r10, %rax	# v, tmp210
	movq	%rax, (%rsi,%rdi,2)	# tmp210, MEM[base: k2_62, index: ivtmp.249_313, step: 2, offset: 0B]
	addq	$4, %rdi	#, ivtmp.249
# crypt_util.c:685:   for(i = 0; i < 16; i++) {
	cmpq	$64, %rdi	#, ivtmp.249
	jne	.L100	#,
# crypt_util.c:717: }
	popq	%rbx	#
	.cfi_def_cfa_offset 48
# crypt_util.c:716:   __data->direction = 0;
	movl	$0, 131224(%rsi)	#, *__data_61(D).direction
# crypt_util.c:717: }
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE52:
	.size	_ufc_mk_keytab_r, .-_ufc_mk_keytab_r
	.p2align 4,,15
	.globl	_ufc_dofinalperm_r
	.type	_ufc_dofinalperm_r, @function
_ufc_dofinalperm_r:
.LFB53:
	.cfi_startproc
# crypt_util.c:729:   l1 = res[0]; l2 = res[1];
	movq	(%rdi), %rcx	# *res_42(D), l1
	movq	8(%rdi), %r10	# MEM[(ufc_long *)res_42(D) + 8B], l2
# crypt_util.c:737:   v1 |= efp[15][ r2         & 0x3f][0]; v2 |= efp[15][ r2 & 0x3f][1];
	leaq	efp(%rip), %rax	#, tmp201
# crypt_util.c:725: {
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
# crypt_util.c:732:   x = (l1 ^ l2) & __data->current_saltbits; l1 ^= x; l2 ^= x;
	movq	131216(%rsi), %rsi	# *__data_47(D).current_saltbits, _3
# crypt_util.c:730:   r1 = res[2]; r2 = res[3];
	movq	16(%rdi), %r9	# MEM[(ufc_long *)res_42(D) + 16B], r1
	movq	24(%rdi), %rdx	# MEM[(ufc_long *)res_42(D) + 24B], r2
# crypt_util.c:732:   x = (l1 ^ l2) & __data->current_saltbits; l1 ^= x; l2 ^= x;
	movq	%rcx, %r8	# l1, tmp197
	xorq	%r10, %r8	# l2, tmp197
	andq	%rsi, %r8	# _3, x
	xorq	%r8, %rcx	# x, l1
	xorq	%r10, %r8	# l2, l2
# crypt_util.c:733:   x = (r1 ^ r2) & __data->current_saltbits; r1 ^= x; r2 ^= x;
	movq	%r9, %r10	# r1, tmp198
	xorq	%rdx, %r10	# r2, tmp198
	andq	%rsi, %r10	# _3, x
	xorq	%r10, %r9	# x, r1
	xorq	%rdx, %r10	# r2, r2
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	movq	%r10, %r11	# r2, r2
# crypt_util.c:738:   v1 |= efp[14][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[14][ r2 & 0x3f][1];
	movq	%r10, %rdx	# r2, r2
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	shrq	$3, %r11	#, r2
# crypt_util.c:738:   v1 |= efp[14][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[14][ r2 & 0x3f][1];
	shrq	$9, %rdx	#, r2
# crypt_util.c:737:   v1 |= efp[15][ r2         & 0x3f][0]; v2 |= efp[15][ r2 & 0x3f][1];
	andl	$63, %r11d	#, _5
# crypt_util.c:738:   v1 |= efp[14][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[14][ r2 & 0x3f][1];
	andl	$63, %edx	#, _8
# crypt_util.c:737:   v1 |= efp[15][ r2         & 0x3f][0]; v2 |= efp[15][ r2 & 0x3f][1];
	leaq	960(%r11), %rsi	#, tmp202
# crypt_util.c:738:   v1 |= efp[14][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[14][ r2 & 0x3f][1];
	leaq	896(%rdx), %rbx	#, tmp206
	salq	$4, %rdx	#, _8
	salq	$4, %rbx	#, tmp207
# crypt_util.c:737:   v1 |= efp[15][ r2         & 0x3f][0]; v2 |= efp[15][ r2 & 0x3f][1];
	salq	$4, %rsi	#, tmp203
# crypt_util.c:738:   v1 |= efp[14][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[14][ r2 & 0x3f][1];
	movq	(%rax,%rsi), %rsi	# efp, tmp209
	orq	(%rax,%rbx), %rsi	# efp, v1
# crypt_util.c:737:   v1 |= efp[15][ r2         & 0x3f][0]; v2 |= efp[15][ r2 & 0x3f][1];
	movq	%r11, %rbx	# _5, _5
	salq	$4, %rbx	#, _5
# crypt_util.c:738:   v1 |= efp[14][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[14][ r2 & 0x3f][1];
	movq	%rdx, %r11	# _8, tmp215
	movq	15368(%rax,%rbx), %rdx	# efp, tmp218
	orq	14344(%rax,%r11), %rdx	# efp, v2
# crypt_util.c:739:   v1 |= efp[13][(r2 >>= 10) & 0x3f][0]; v2 |= efp[13][ r2 & 0x3f][1];
	movq	%r10, %r11	# r2, r2
	shrq	$19, %r11	#, r2
# crypt_util.c:740:   v1 |= efp[12][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[12][ r2 & 0x3f][1];
	shrq	$25, %r10	#, r2
# crypt_util.c:739:   v1 |= efp[13][(r2 >>= 10) & 0x3f][0]; v2 |= efp[13][ r2 & 0x3f][1];
	andl	$63, %r11d	#, _10
# crypt_util.c:740:   v1 |= efp[12][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[12][ r2 & 0x3f][1];
	andl	$63, %r10d	#, _12
# crypt_util.c:739:   v1 |= efp[13][(r2 >>= 10) & 0x3f][0]; v2 |= efp[13][ r2 & 0x3f][1];
	leaq	832(%r11), %rbx	#, tmp221
	salq	$4, %r11	#, tmp225
	orq	13320(%rax,%r11), %rdx	# efp, v2
# crypt_util.c:740:   v1 |= efp[12][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[12][ r2 & 0x3f][1];
	leaq	768(%r10), %r11	#, tmp230
	salq	$4, %r10	#, tmp234
	orq	12296(%rax,%r10), %rdx	# efp, v2
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	movq	%r9, %r10	# r1, r1
# crypt_util.c:739:   v1 |= efp[13][(r2 >>= 10) & 0x3f][0]; v2 |= efp[13][ r2 & 0x3f][1];
	salq	$4, %rbx	#, tmp222
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	shrq	$3, %r10	#, r1
# crypt_util.c:739:   v1 |= efp[13][(r2 >>= 10) & 0x3f][0]; v2 |= efp[13][ r2 & 0x3f][1];
	orq	(%rax,%rbx), %rsi	# efp, v1
# crypt_util.c:740:   v1 |= efp[12][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[12][ r2 & 0x3f][1];
	salq	$4, %r11	#, tmp231
# crypt_util.c:742:   v1 |= efp[11][ r1         & 0x3f][0]; v2 |= efp[11][ r1 & 0x3f][1];
	andl	$63, %r10d	#, _14
# crypt_util.c:740:   v1 |= efp[12][(r2 >>= 6)  & 0x3f][0]; v2 |= efp[12][ r2 & 0x3f][1];
	orq	(%rax,%r11), %rsi	# efp, v1
# crypt_util.c:742:   v1 |= efp[11][ r1         & 0x3f][0]; v2 |= efp[11][ r1 & 0x3f][1];
	leaq	704(%r10), %r11	#, tmp239
	salq	$4, %r11	#, tmp240
	salq	$4, %r10	#, tmp243
	orq	11272(%rax,%r10), %rdx	# efp, v2
# crypt_util.c:743:   v1 |= efp[10][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[10][ r1 & 0x3f][1];
	movq	%r9, %r10	# r1, r1
# crypt_util.c:742:   v1 |= efp[11][ r1         & 0x3f][0]; v2 |= efp[11][ r1 & 0x3f][1];
	orq	(%rax,%r11), %rsi	# efp, v1
# crypt_util.c:743:   v1 |= efp[10][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[10][ r1 & 0x3f][1];
	shrq	$9, %r10	#, r1
	andl	$63, %r10d	#, _17
	leaq	640(%r10), %r11	#, tmp248
	salq	$4, %r10	#, tmp252
	orq	10248(%rax,%r10), %rdx	# efp, v2
# crypt_util.c:744:   v1 |= efp[ 9][(r1 >>= 10) & 0x3f][0]; v2 |= efp[ 9][ r1 & 0x3f][1];
	movq	%r9, %r10	# r1, r1
# crypt_util.c:745:   v1 |= efp[ 8][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[ 8][ r1 & 0x3f][1];
	shrq	$25, %r9	#, r1
# crypt_util.c:744:   v1 |= efp[ 9][(r1 >>= 10) & 0x3f][0]; v2 |= efp[ 9][ r1 & 0x3f][1];
	shrq	$19, %r10	#, r1
# crypt_util.c:743:   v1 |= efp[10][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[10][ r1 & 0x3f][1];
	salq	$4, %r11	#, tmp249
# crypt_util.c:745:   v1 |= efp[ 8][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[ 8][ r1 & 0x3f][1];
	andl	$63, %r9d	#, _21
# crypt_util.c:744:   v1 |= efp[ 9][(r1 >>= 10) & 0x3f][0]; v2 |= efp[ 9][ r1 & 0x3f][1];
	andl	$63, %r10d	#, _19
# crypt_util.c:743:   v1 |= efp[10][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[10][ r1 & 0x3f][1];
	orq	(%rax,%r11), %rsi	# efp, v1
# crypt_util.c:744:   v1 |= efp[ 9][(r1 >>= 10) & 0x3f][0]; v2 |= efp[ 9][ r1 & 0x3f][1];
	leaq	576(%r10), %r11	#, tmp257
	salq	$4, %r10	#, tmp261
	orq	9224(%rax,%r10), %rdx	# efp, v2
# crypt_util.c:745:   v1 |= efp[ 8][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[ 8][ r1 & 0x3f][1];
	leaq	512(%r9), %r10	#, tmp266
	salq	$4, %r9	#, tmp270
	orq	8200(%rax,%r9), %rdx	# efp, v2
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	movq	%r8, %r9	# l2, l2
# crypt_util.c:744:   v1 |= efp[ 9][(r1 >>= 10) & 0x3f][0]; v2 |= efp[ 9][ r1 & 0x3f][1];
	salq	$4, %r11	#, tmp258
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	shrq	$3, %r9	#, l2
# crypt_util.c:744:   v1 |= efp[ 9][(r1 >>= 10) & 0x3f][0]; v2 |= efp[ 9][ r1 & 0x3f][1];
	orq	(%rax,%r11), %rsi	# efp, v1
# crypt_util.c:745:   v1 |= efp[ 8][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[ 8][ r1 & 0x3f][1];
	salq	$4, %r10	#, tmp267
# crypt_util.c:747:   v1 |= efp[ 7][ l2         & 0x3f][0]; v2 |= efp[ 7][ l2 & 0x3f][1];
	andl	$63, %r9d	#, _23
# crypt_util.c:745:   v1 |= efp[ 8][(r1 >>= 6)  & 0x3f][0]; v2 |= efp[ 8][ r1 & 0x3f][1];
	orq	(%rax,%r10), %rsi	# efp, v1
# crypt_util.c:747:   v1 |= efp[ 7][ l2         & 0x3f][0]; v2 |= efp[ 7][ l2 & 0x3f][1];
	leaq	448(%r9), %r10	#, tmp275
	salq	$4, %r9	#, tmp279
	orq	7176(%rax,%r9), %rdx	# efp, v2
# crypt_util.c:748:   v1 |= efp[ 6][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 6][ l2 & 0x3f][1];
	movq	%r8, %r9	# l2, l2
	shrq	$9, %r9	#, l2
# crypt_util.c:747:   v1 |= efp[ 7][ l2         & 0x3f][0]; v2 |= efp[ 7][ l2 & 0x3f][1];
	salq	$4, %r10	#, tmp276
# crypt_util.c:748:   v1 |= efp[ 6][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 6][ l2 & 0x3f][1];
	andl	$63, %r9d	#, _26
# crypt_util.c:747:   v1 |= efp[ 7][ l2         & 0x3f][0]; v2 |= efp[ 7][ l2 & 0x3f][1];
	orq	(%rax,%r10), %rsi	# efp, v1
# crypt_util.c:748:   v1 |= efp[ 6][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 6][ l2 & 0x3f][1];
	leaq	384(%r9), %r10	#, tmp284
	salq	$4, %r10	#, tmp285
	orq	(%rax,%r10), %rsi	# efp, v1
	salq	$4, %r9	#, tmp288
	orq	6152(%rax,%r9), %rdx	# efp, v2
# crypt_util.c:749:   v1 |= efp[ 5][(l2 >>= 10) & 0x3f][0]; v2 |= efp[ 5][ l2 & 0x3f][1];
	movq	%r8, %r9	# l2, l2
# crypt_util.c:750:   v1 |= efp[ 4][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 4][ l2 & 0x3f][1];
	shrq	$25, %r8	#, l2
# crypt_util.c:749:   v1 |= efp[ 5][(l2 >>= 10) & 0x3f][0]; v2 |= efp[ 5][ l2 & 0x3f][1];
	shrq	$19, %r9	#, l2
# crypt_util.c:750:   v1 |= efp[ 4][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 4][ l2 & 0x3f][1];
	andl	$63, %r8d	#, _30
# crypt_util.c:749:   v1 |= efp[ 5][(l2 >>= 10) & 0x3f][0]; v2 |= efp[ 5][ l2 & 0x3f][1];
	andl	$63, %r9d	#, _28
	leaq	320(%r9), %r10	#, tmp293
	salq	$4, %r9	#, tmp297
	orq	5128(%rax,%r9), %rdx	# efp, v2
# crypt_util.c:750:   v1 |= efp[ 4][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 4][ l2 & 0x3f][1];
	leaq	256(%r8), %r9	#, tmp302
	salq	$4, %r8	#, tmp306
	orq	4104(%rax,%r8), %rdx	# efp, v2
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	movq	%rcx, %r8	# l1, l1
# crypt_util.c:749:   v1 |= efp[ 5][(l2 >>= 10) & 0x3f][0]; v2 |= efp[ 5][ l2 & 0x3f][1];
	salq	$4, %r10	#, tmp294
# crypt_util.c:735:   v1=v2=0; l1 >>= 3; l2 >>= 3; r1 >>= 3; r2 >>= 3;
	shrq	$3, %r8	#, l1
# crypt_util.c:749:   v1 |= efp[ 5][(l2 >>= 10) & 0x3f][0]; v2 |= efp[ 5][ l2 & 0x3f][1];
	orq	(%rax,%r10), %rsi	# efp, v1
# crypt_util.c:750:   v1 |= efp[ 4][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 4][ l2 & 0x3f][1];
	salq	$4, %r9	#, tmp303
# crypt_util.c:752:   v1 |= efp[ 3][ l1         & 0x3f][0]; v2 |= efp[ 3][ l1 & 0x3f][1];
	andl	$63, %r8d	#, _32
# crypt_util.c:750:   v1 |= efp[ 4][(l2 >>= 6)  & 0x3f][0]; v2 |= efp[ 4][ l2 & 0x3f][1];
	orq	(%rax,%r9), %rsi	# efp, v1
# crypt_util.c:752:   v1 |= efp[ 3][ l1         & 0x3f][0]; v2 |= efp[ 3][ l1 & 0x3f][1];
	leaq	192(%r8), %r9	#, tmp311
	salq	$4, %r8	#, tmp315
	orq	3080(%rax,%r8), %rdx	# efp, v2
# crypt_util.c:753:   v1 |= efp[ 2][(l1 >>= 6)  & 0x3f][0]; v2 |= efp[ 2][ l1 & 0x3f][1];
	movq	%rcx, %r8	# l1, l1
	shrq	$9, %r8	#, l1
# crypt_util.c:752:   v1 |= efp[ 3][ l1         & 0x3f][0]; v2 |= efp[ 3][ l1 & 0x3f][1];
	salq	$4, %r9	#, tmp312
# crypt_util.c:753:   v1 |= efp[ 2][(l1 >>= 6)  & 0x3f][0]; v2 |= efp[ 2][ l1 & 0x3f][1];
	andl	$63, %r8d	#, _35
# crypt_util.c:752:   v1 |= efp[ 3][ l1         & 0x3f][0]; v2 |= efp[ 3][ l1 & 0x3f][1];
	orq	(%rax,%r9), %rsi	# efp, v1
# crypt_util.c:753:   v1 |= efp[ 2][(l1 >>= 6)  & 0x3f][0]; v2 |= efp[ 2][ l1 & 0x3f][1];
	leaq	128(%r8), %r9	#, tmp320
	salq	$4, %r8	#, tmp324
	salq	$4, %r9	#, tmp321
	orq	(%rax,%r9), %rsi	# efp, v1
	orq	2056(%rax,%r8), %rdx	# efp, v2
# crypt_util.c:754:   v1 |= efp[ 1][(l1 >>= 10) & 0x3f][0]; v2 |= efp[ 1][ l1 & 0x3f][1];
	movq	%rcx, %r8	# l1, l1
	shrq	$19, %r8	#, l1
	andl	$63, %r8d	#, _37
	leaq	64(%r8), %r9	#, tmp329
	salq	$4, %r8	#, tmp333
	orq	1032(%rax,%r8), %rdx	# efp, v2
	salq	$4, %r9	#, tmp330
	orq	(%rax,%r9), %rsi	# efp, v1
# crypt_util.c:755:   v1 |= efp[ 0][(l1 >>= 6)  & 0x3f][0]; v2 |= efp[ 0][ l1 & 0x3f][1];
	shrq	$21, %rcx	#, _39
	andl	$1008, %ecx	#, tmp338
	addq	%rcx, %rax	# tmp338, tmp339
	orq	8(%rax), %rdx	# efp, v2
	orq	(%rax), %rsi	# efp, v1
# crypt_util.c:758: }
	popq	%rbx	#
	.cfi_def_cfa_offset 8
# crypt_util.c:757:   res[0] = v1; res[1] = v2;
	movq	%rsi, (%rdi)	# v1, *res_42(D)
	movq	%rdx, 8(%rdi)	# v2, MEM[(ufc_long *)res_42(D) + 8B]
# crypt_util.c:758: }
	ret
	.cfi_endproc
.LFE53:
	.size	_ufc_dofinalperm_r, .-_ufc_dofinalperm_r
	.p2align 4,,15
	.globl	_ufc_output_conversion_r
	.type	_ufc_output_conversion_r, @function
_ufc_output_conversion_r:
.LFB54:
	.cfi_startproc
# crypt_util.c:771:   __data->crypt_3_buf[0] = salt[0];
	movzbl	(%rdx), %eax	# *salt_50(D), _1
# crypt_util.c:772:   __data->crypt_3_buf[1] = salt[1] ? salt[1] : salt[0];
	movzbl	1(%rdx), %edx	# MEM[(const char *)salt_50(D) + 1B], _2
# crypt_util.c:768: {
	movq	%rcx, %r8	# __data, __data
# crypt_util.c:772:   __data->crypt_3_buf[1] = salt[1] ? salt[1] : salt[0];
	testb	%dl, %dl	# _2
# crypt_util.c:771:   __data->crypt_3_buf[0] = salt[0];
	movb	%al, 131200(%rcx)	# _1, *__data_51(D).crypt_3_buf
# crypt_util.c:772:   __data->crypt_3_buf[1] = salt[1] ? salt[1] : salt[0];
	cmovne	%edx, %eax	# _1,, _2, _1
	leaq	131202(%rcx), %rdx	#, ivtmp.285
	movb	%al, 131201(%rcx)	# _1, *__data_51(D).crypt_3_buf
	movl	$26, %ecx	#, ivtmp.283
.L108:
# crypt_util.c:776:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v1 >> shf) & 0x3f);
	movq	%rdi, %rax	# v1, _4
	shrq	%cl, %rax	# ivtmp.283, _4
	andl	$63, %eax	#, _5
	cmpq	$37, %rax	#, _5
	jbe	.L105	#,
# crypt_util.c:776:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v1 >> shf) & 0x3f);
	addl	$59, %eax	#, iftmp.17_44
.L106:
	subl	$6, %ecx	#, ivtmp.283
# crypt_util.c:776:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v1 >> shf) & 0x3f);
	movb	%al, (%rdx)	# iftmp.17_44, MEM[base: _80, offset: 0B]
	addq	$1, %rdx	#, ivtmp.285
# crypt_util.c:774:   for(i = 0; i < 5; i++) {
	cmpl	$-4, %ecx	#, ivtmp.283
	jne	.L108	#,
# crypt_util.c:779:   s  = (v2 & 0xf) << 2;
	leal	0(,%rsi,4), %r9d	#, tmp208
# crypt_util.c:780:   v2 = (v2 >> 2) | ((v1 & 0x3) << 30);
	movq	%rsi, %rax	# v2, v2
	salq	$30, %rdi	#, tmp209
	shrq	$2, %rax	#, v2
	movl	%edi, %esi	# tmp209, tmp210
	leaq	131207(%r8), %rdx	#, ivtmp.275
# crypt_util.c:779:   s  = (v2 & 0xf) << 2;
	andl	$60, %r9d	#, s
# crypt_util.c:780:   v2 = (v2 >> 2) | ((v1 & 0x3) << 30);
	orq	%rax, %rsi	# _19, v2
	movl	$26, %ecx	#, ivtmp.273
.L112:
# crypt_util.c:784:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v2 >> shf) & 0x3f);
	movq	%rsi, %rax	# v2, _23
	shrq	%cl, %rax	# ivtmp.273, _23
	andl	$63, %eax	#, _24
	cmpq	$37, %rax	#, _24
	jbe	.L109	#,
# crypt_util.c:784:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v2 >> shf) & 0x3f);
	addl	$59, %eax	#, iftmp.19_45
.L110:
	subl	$6, %ecx	#, ivtmp.273
# crypt_util.c:784:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v2 >> shf) & 0x3f);
	movb	%al, (%rdx)	# iftmp.19_45, MEM[base: _42, offset: 0B]
	addq	$1, %rdx	#, ivtmp.275
# crypt_util.c:782:   for(i = 5; i < 10; i++) {
	cmpl	$-4, %ecx	#, ivtmp.273
	jne	.L112	#,
# crypt_util.c:787:   __data->crypt_3_buf[12] = bin_to_ascii(s);
	cmpl	$37, %r9d	#, s
	jg	.L120	#,
# crypt_util.c:787:   __data->crypt_3_buf[12] = bin_to_ascii(s);
	leal	53(%r9), %edx	#, tmp216
	leal	46(%r9), %eax	#, tmp215
	cmpl	$12, %r9d	#, s
# crypt_util.c:788:   __data->crypt_3_buf[13] = 0;
	movb	$0, 131213(%r8)	#, *__data_51(D).crypt_3_buf
# crypt_util.c:787:   __data->crypt_3_buf[12] = bin_to_ascii(s);
	cmovl	%eax, %edx	# tmp215,, tmp216
	movl	%edx, %r9d	# tmp216, iftmp.21_46
	movb	%r9b, 131212(%r8)	# iftmp.21_46, *__data_51(D).crypt_3_buf
# crypt_util.c:789: }
	ret
	.p2align 4,,10
	.p2align 3
.L105:
# crypt_util.c:776:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v1 >> shf) & 0x3f);
	leal	53(%rax), %r10d	#, tmp212
	leal	46(%rax), %r9d	#, tmp211
	cmpq	$12, %rax	#, _5
	movl	%r10d, %eax	# tmp212, tmp212
	cmovb	%r9d, %eax	# tmp211,, tmp212
	jmp	.L106	#
	.p2align 4,,10
	.p2align 3
.L109:
# crypt_util.c:784:     __data->crypt_3_buf[i + 2] = bin_to_ascii((v2 >> shf) & 0x3f);
	leal	53(%rax), %r10d	#, tmp214
	leal	46(%rax), %edi	#, tmp213
	cmpq	$12, %rax	#, _24
	movl	%r10d, %eax	# tmp214, tmp214
	cmovb	%edi, %eax	# tmp213,, tmp214
	jmp	.L110	#
	.p2align 4,,10
	.p2align 3
.L120:
# crypt_util.c:787:   __data->crypt_3_buf[12] = bin_to_ascii(s);
	addl	$59, %r9d	#, iftmp.21_46
# crypt_util.c:788:   __data->crypt_3_buf[13] = 0;
	movb	$0, 131213(%r8)	#, *__data_51(D).crypt_3_buf
# crypt_util.c:787:   __data->crypt_3_buf[12] = bin_to_ascii(s);
	movb	%r9b, 131212(%r8)	# iftmp.21_46, *__data_51(D).crypt_3_buf
# crypt_util.c:789: }
	ret
	.cfi_endproc
.LFE54:
	.size	_ufc_output_conversion_r, .-_ufc_output_conversion_r
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	".."
	.text
	.p2align 4,,15
	.globl	__encrypt_r
	.type	__encrypt_r, @function
__encrypt_r:
.LFB55:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movl	%esi, %r12d	# __edflag, __edflag
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rbx	# __block, __block
# crypt_util.c:817:   _ufc_setup_salt_r("..", __data);
	leaq	.LC0(%rip), %rdi	#,
# crypt_util.c:802: {
	movq	%rdx, %rbp	# __data, __data
# crypt_util.c:817:   _ufc_setup_salt_r("..", __data);
	movq	%rdx, %rsi	# __data,
# crypt_util.c:802: {
	subq	$32, %rsp	#,
	.cfi_def_cfa_offset 64
# crypt_util.c:817:   _ufc_setup_salt_r("..", __data);
	call	_ufc_setup_salt_r@PLT	#
# crypt_util.c:823:   if((__edflag == 0) != (__data->direction == 0)) {
	movl	131224(%rbp), %eax	# *__data_79(D).direction,
	testl	%r12d, %r12d	# __edflag
	sete	%dl	#, tmp252
	testl	%eax, %eax	#
	sete	%al	#, tmp254
	cmpb	%al, %dl	# tmp254, tmp252
	je	.L122	#,
	leaq	120(%rbp), %rax	#, ivtmp.425
	leaq	56(%rbp), %rdi	#, _61
	movq	%rbp, %rdx	# __data, ivtmp.427
	.p2align 4,,10
	.p2align 3
.L123:
# crypt_util.c:838:       kt[15-i] = kt[i];
	movq	(%rdx), %rsi	# MEM[base: _67, offset: 0B], _14
# crypt_util.c:837:       x = kt[15-i];
	movq	(%rax), %rcx	# MEM[base: _69, offset: 0B], x
	subq	$8, %rax	#, ivtmp.425
	addq	$8, %rdx	#, ivtmp.427
# crypt_util.c:838:       kt[15-i] = kt[i];
	movq	%rsi, 8(%rax)	# _14, MEM[base: _69, offset: 0B]
# crypt_util.c:839:       kt[i] = x;
	movq	%rcx, -8(%rdx)	# x, MEM[base: _67, offset: 0B]
# crypt_util.c:824:     for(i = 0; i < 8; i++) {
	cmpq	%rax, %rdi	# ivtmp.425, _61
	jne	.L123	#,
# crypt_util.c:842:     __data->direction = __edflag;
	movl	%r12d, 131224(%rbp)	# __edflag, *__data_79(D).direction
.L122:
# crypt_util.c:802: {
	xorl	%edx, %edx	# ivtmp.417
	movl	$8, %eax	#,
# crypt_util.c:849:   for(l1 = 0; i < 24; i++) {
	xorl	%ecx, %ecx	# l1
# crypt_util.c:851:       l1 |= BITMASK[i];
	leaq	BITMASK(%rip), %r8	#, tmp308
	leaq	initial_perm(%rip), %rdi	#, tmp309
	leaq	esel(%rip), %rsi	#, tmp310
	jmp	.L126	#
	.p2align 4,,10
	.p2align 3
.L142:
	movl	(%rsi,%rdx), %eax	# MEM[symbol: esel, index: ivtmp.417_113, offset: 0B], tmp311
	subl	$1, %eax	#, tmp260
	cltq
	movslq	(%rdi,%rax,4), %rax	# initial_perm,
.L126:
# crypt_util.c:850:     if(__block[initial_perm[esel[i]-1]-1])
	cmpb	$0, -1(%rbx,%rax)	#, *_21
	je	.L124	#,
# crypt_util.c:851:       l1 |= BITMASK[i];
	orq	(%r8,%rdx,2), %rcx	# MEM[symbol: BITMASK, index: _78, offset: 0B], l1
.L124:
	addq	$4, %rdx	#, ivtmp.417
# crypt_util.c:849:   for(l1 = 0; i < 24; i++) {
	cmpq	$96, %rdx	#, ivtmp.417
	jne	.L142	#,
	xorl	%edx, %edx	# ivtmp.397
	movl	$4, %eax	#,
# crypt_util.c:853:   for(l2 = 0; i < 48; i++) {
	xorl	%esi, %esi	# l2
# crypt_util.c:855:       l2 |= BITMASK[i-24];
	leaq	BITMASK(%rip), %r9	#, tmp305
	leaq	initial_perm(%rip), %r8	#, tmp306
	leaq	96+esel(%rip), %rdi	#, tmp307
	jmp	.L125	#
	.p2align 4,,10
	.p2align 3
.L143:
	movl	(%rdi,%rdx), %eax	# MEM[symbol: esel, index: ivtmp.397_116, offset: 96B], tmp312
	subl	$1, %eax	#, tmp268
	cltq
	movslq	(%r8,%rax,4), %rax	# initial_perm,
.L125:
# crypt_util.c:854:     if(__block[initial_perm[esel[i]-1]-1])
	cmpb	$0, -1(%rbx,%rax)	#, *_29
	je	.L127	#,
# crypt_util.c:855:       l2 |= BITMASK[i-24];
	orq	(%r9,%rdx,2), %rsi	# MEM[symbol: BITMASK, index: _115, offset: 0B], l2
.L127:
	addq	$4, %rdx	#, ivtmp.397
# crypt_util.c:853:   for(l2 = 0; i < 48; i++) {
	cmpq	$96, %rdx	#, ivtmp.397
	jne	.L143	#,
	xorl	%edx, %edx	# ivtmp.375
	movl	$7, %eax	#,
# crypt_util.c:859:   for(r1 = 0; i < 24; i++) {
	xorl	%edi, %edi	# r1
# crypt_util.c:861:       r1 |= BITMASK[i];
	leaq	BITMASK(%rip), %r10	#, tmp302
	leaq	initial_perm(%rip), %r9	#, tmp303
	leaq	esel(%rip), %r8	#, tmp304
	jmp	.L128	#
	.p2align 4,,10
	.p2align 3
.L144:
	movl	(%r8,%rdx), %eax	# MEM[symbol: esel, index: ivtmp.375_119, offset: 0B], tmp313
	addl	$31, %eax	#, tmp276
	cltq
	movslq	(%r9,%rax,4), %rax	# initial_perm,
.L128:
# crypt_util.c:860:     if(__block[initial_perm[esel[i]-1+32]-1])
	cmpb	$0, -1(%rbx,%rax)	#, *_38
	je	.L129	#,
# crypt_util.c:861:       r1 |= BITMASK[i];
	orq	(%r10,%rdx,2), %rdi	# MEM[symbol: BITMASK, index: _118, offset: 0B], r1
.L129:
	addq	$4, %rdx	#, ivtmp.375
# crypt_util.c:859:   for(r1 = 0; i < 24; i++) {
	cmpq	$96, %rdx	#, ivtmp.375
	jne	.L144	#,
	xorl	%edx, %edx	# ivtmp.355
	movl	$3, %eax	#,
# crypt_util.c:863:   for(r2 = 0; i < 48; i++) {
	xorl	%r8d, %r8d	# r2
# crypt_util.c:865:       r2 |= BITMASK[i-24];
	leaq	BITMASK(%rip), %r11	#, tmp299
	leaq	initial_perm(%rip), %r10	#, tmp300
	leaq	96+esel(%rip), %r9	#, tmp301
	jmp	.L130	#
	.p2align 4,,10
	.p2align 3
.L145:
	movl	(%r9,%rdx), %eax	# MEM[symbol: esel, index: ivtmp.355_122, offset: 96B], tmp314
	addl	$31, %eax	#, tmp284
	cltq
	movslq	(%r10,%rax,4), %rax	# initial_perm,
.L130:
# crypt_util.c:864:     if(__block[initial_perm[esel[i]-1+32]-1])
	cmpb	$0, -1(%rbx,%rax)	#, *_46
	je	.L131	#,
# crypt_util.c:865:       r2 |= BITMASK[i-24];
	orq	(%r11,%rdx,2), %r8	# MEM[symbol: BITMASK, index: _121, offset: 0B], r2
.L131:
	addq	$4, %rdx	#, ivtmp.355
# crypt_util.c:863:   for(r2 = 0; i < 48; i++) {
	cmpq	$96, %rdx	#, ivtmp.355
	jne	.L145	#,
# crypt_util.c:873:   _ufc_doit_r((ufc_long)1, __data, &res[0]);
	movq	%rsp, %r12	#, tmp287
# crypt_util.c:871:   res[0] = l1; res[1] = l2;
	movq	%rsi, 8(%rsp)	# l2, res
# crypt_util.c:872:   res[2] = r1; res[3] = r2;
	movq	%rdi, 16(%rsp)	# r1, res
# crypt_util.c:873:   _ufc_doit_r((ufc_long)1, __data, &res[0]);
	movq	%r12, %rdx	# tmp287,
	movq	%rbp, %rsi	# __data,
	movl	$1, %edi	#,
# crypt_util.c:871:   res[0] = l1; res[1] = l2;
	movq	%rcx, (%rsp)	# l1, res
# crypt_util.c:872:   res[2] = r1; res[3] = r2;
	movq	%r8, 24(%rsp)	# r2, res
# crypt_util.c:873:   _ufc_doit_r((ufc_long)1, __data, &res[0]);
	call	_ufc_doit_r@PLT	#
# crypt_util.c:878:   _ufc_dofinalperm_r(res, __data);
	movq	%rbp, %rsi	# __data,
	movq	%r12, %rdi	# tmp287,
	call	_ufc_dofinalperm_r@PLT	#
# crypt_util.c:883:   l1 = res[0]; r1 = res[1];
	movq	(%rsp), %rsi	# res, l1
	movq	8(%rsp), %rcx	# res, r1
	xorl	%eax, %eax	# ivtmp.315
	movl	$2147483648, %edx	#, pretmp_196
	leaq	longmask(%rip), %rdi	#, tmp298
	jmp	.L134	#
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%rdi,%rax,8), %rdx	# MEM[symbol: longmask, index: _124, offset: 0B], pretmp_196
.L134:
# crypt_util.c:885:     *__block++ = (l1 & longmask[i]) != 0;
	testq	%rdx, %rsi	# pretmp_196, l1
	setne	(%rbx,%rax)	#, MEM[base: __block_97(D), index: ivtmp.315_126, offset: 0B]
	addq	$1, %rax	#, ivtmp.315
# crypt_util.c:884:   for(i = 0; i < 32; i++) {
	cmpq	$32, %rax	#, ivtmp.315
	jne	.L146	#,
	xorl	%eax, %eax	# ivtmp.291
	movl	$2147483648, %edx	#, pretmp_199
	leaq	longmask(%rip), %rsi	#, tmp297
	jmp	.L133	#
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rsi,%rax,8), %rdx	# MEM[symbol: longmask, index: _127, offset: 0B], pretmp_199
.L133:
# crypt_util.c:888:     *__block++ = (r1 & longmask[i]) != 0;
	testq	%rdx, %rcx	# pretmp_199, r1
	setne	32(%rbx,%rax)	#, MEM[base: __block_97(D), index: ivtmp.291_129, offset: 32B]
	addq	$1, %rax	#, ivtmp.291
# crypt_util.c:887:   for(i = 0; i < 32; i++) {
	cmpq	$32, %rax	#, ivtmp.291
	jne	.L147	#,
# crypt_util.c:890: }
	addq	$32, %rsp	#,
	.cfi_def_cfa_offset 32
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE55:
	.size	__encrypt_r, .-__encrypt_r
	.weak	encrypt_r
	.set	encrypt_r,__encrypt_r
	.p2align 4,,15
	.globl	encrypt
	.type	encrypt, @function
encrypt:
.LFB56:
	.cfi_startproc
# crypt_util.c:897:   __encrypt_r(__block, __edflag, &_ufc_foobar);
	movq	_ufc_foobar@GOTPCREL(%rip), %rdx	#,
	jmp	__encrypt_r@PLT	#
	.cfi_endproc
.LFE56:
	.size	encrypt, .-encrypt
	.p2align 4,,15
	.globl	__setkey_r
	.type	__setkey_r, @function
__setkey_r:
.LFB57:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbx	# __key, __key
# crypt_util.c:914:   _ufc_setup_salt_r("..", __data); /* be sure we're initialized */
	leaq	.LC0(%rip), %rdi	#,
# crypt_util.c:909: {
	movq	%rsi, %rbp	# __data, __data
	subq	$24, %rsp	#,
	.cfi_def_cfa_offset 48
# crypt_util.c:914:   _ufc_setup_salt_r("..", __data); /* be sure we're initialized */
	call	_ufc_setup_salt_r@PLT	#
	leaq	8(%rsp), %rdi	#, tmp122
	leaq	64(%rbx), %r8	#, _9
	movq	%rdi, %rsi	# tmp122, ivtmp.450
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	8(%rbx), %rcx	#, __key
# crypt_util.c:909: {
	movq	%rbx, %rax	# __key, __key
# crypt_util.c:917:     for(j = 0, c = 0; j < 8; j++)
	xorl	%edx, %edx	# c
	.p2align 4,,10
	.p2align 3
.L151:
# crypt_util.c:918:       c = c << 1 | *__key++;
	addq	$1, %rax	#, __key
	addl	%edx, %edx	# tmp119
	orb	-1(%rax), %dl	# MEM[base: __key_23, offset: -1B], c
# crypt_util.c:917:     for(j = 0, c = 0; j < 8; j++)
	cmpq	%rcx, %rax	# __key, __key
	jne	.L151	#,
# crypt_util.c:919:     ktab[i] = c >> 1;
	shrb	%dl	# tmp120
	addq	$1, %rsi	#, ivtmp.450
	movq	%rax, %rbx	# __key, __key
	movb	%dl, -1(%rsi)	# tmp120, MEM[base: _10, offset: 0B]
# crypt_util.c:916:   for(i = 0; i < 8; i++) {
	cmpq	%rax, %r8	# __key, _9
	jne	.L150	#,
# crypt_util.c:921:   _ufc_mk_keytab_r((char *) ktab, __data);
	movq	%rbp, %rsi	# __data,
	call	_ufc_mk_keytab_r@PLT	#
# crypt_util.c:922: }
	addq	$24, %rsp	#,
	.cfi_def_cfa_offset 24
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE57:
	.size	__setkey_r, .-__setkey_r
	.weak	setkey_r
	.set	setkey_r,__setkey_r
	.p2align 4,,15
	.globl	setkey
	.type	setkey, @function
setkey:
.LFB58:
	.cfi_startproc
# crypt_util.c:929:   __setkey_r(__key, &_ufc_foobar);
	movq	_ufc_foobar@GOTPCREL(%rip), %rsi	#,
	jmp	__setkey_r@PLT	#
	.cfi_endproc
.LFE58:
	.size	setkey, .-setkey
	.p2align 4,,15
	.globl	__b64_from_24bit
	.type	__b64_from_24bit, @function
__b64_from_24bit:
.LFB59:
	.cfi_startproc
# crypt_util.c:939:   unsigned int w = (b2 << 16) | (b1 << 8) | b0;
	sall	$16, %edx	#, b2
	sall	$8, %ecx	#, tmp107
	orl	%r8d, %ecx	# b0, tmp108
	movl	%edx, %r8d	# b2, tmp109
# crypt_util.c:940:   while (n-- > 0 && (*buflen) > 0)
	leal	-1(%r9), %edx	#, n
# crypt_util.c:939:   unsigned int w = (b2 << 16) | (b1 << 8) | b0;
	orl	%ecx, %r8d	# tmp108, w
# crypt_util.c:940:   while (n-- > 0 && (*buflen) > 0)
	testl	%r9d, %r9d	# n
	jle	.L156	#,
	movl	(%rsi), %eax	# *buflen_21(D),
	testl	%eax, %eax	#
	jle	.L156	#,
	leaq	b64t(%rip), %r9	#, tmp116
	jmp	.L158	#
	.p2align 4,,10
	.p2align 3
.L163:
# crypt_util.c:940:   while (n-- > 0 && (*buflen) > 0)
	testl	%eax, %eax	# _9
	jle	.L156	#,
.L158:
# crypt_util.c:942:       *(*cp)++ = b64t[w & 0x3f];
	movq	(%rdi), %rax	# *cp_22(D), _5
# crypt_util.c:940:   while (n-- > 0 && (*buflen) > 0)
	subl	$1, %edx	#, n
# crypt_util.c:942:       *(*cp)++ = b64t[w & 0x3f];
	leaq	1(%rax), %rcx	#, tmp110
	movq	%rcx, (%rdi)	# tmp110, *cp_22(D)
	movl	%r8d, %ecx	# w, tmp113
# crypt_util.c:944:       w >>= 6;
	shrl	$6, %r8d	#, w
# crypt_util.c:942:       *(*cp)++ = b64t[w & 0x3f];
	andl	$63, %ecx	#, tmp113
	movzbl	(%r9,%rcx), %ecx	# b64t, tmp114
	movb	%cl, (%rax)	# tmp114, *_5
# crypt_util.c:943:       --(*buflen);
	movl	(%rsi), %eax	# *buflen_21(D), tmp119
	subl	$1, %eax	#, _9
# crypt_util.c:940:   while (n-- > 0 && (*buflen) > 0)
	cmpl	$-1, %edx	#, n
# crypt_util.c:943:       --(*buflen);
	movl	%eax, (%rsi)	# _9, *buflen_21(D)
# crypt_util.c:940:   while (n-- > 0 && (*buflen) > 0)
	jne	.L163	#,
.L156:
# crypt_util.c:946: }
	rep ret
	.cfi_endproc
.LFE59:
	.size	__b64_from_24bit, .-__b64_from_24bit
	.local	small_tables_initialized.7490
	.comm	small_tables_initialized.7490,4,4
	.local	_ufc_tables_lock
	.comm	_ufc_tables_lock,40,32
	.comm	_ufc_foobar,131232,32
	.section	.rodata
	.align 32
	.type	b64t, @object
	.size	b64t, 64
b64t:
	.ascii	"./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv"
	.ascii	"wxyz"
	.local	efp
	.comm	efp,16384,32
	.local	eperm32tab
	.comm	eperm32tab,16384,32
	.local	do_pc2
	.comm	do_pc2,8192,32
	.local	do_pc1
	.comm	do_pc1,16384,32
	.align 32
	.type	longmask, @object
	.size	longmask, 256
longmask:
	.quad	2147483648
	.quad	1073741824
	.quad	536870912
	.quad	268435456
	.quad	134217728
	.quad	67108864
	.quad	33554432
	.quad	16777216
	.quad	8388608
	.quad	4194304
	.quad	2097152
	.quad	1048576
	.quad	524288
	.quad	262144
	.quad	131072
	.quad	65536
	.quad	32768
	.quad	16384
	.quad	8192
	.quad	4096
	.quad	2048
	.quad	1024
	.quad	512
	.quad	256
	.quad	128
	.quad	64
	.quad	32
	.quad	16
	.quad	8
	.quad	4
	.quad	2
	.quad	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	bytemask, @object
	.size	bytemask, 8
bytemask:
	.byte	-128
	.byte	64
	.byte	32
	.byte	16
	.byte	8
	.byte	4
	.byte	2
	.byte	1
	.section	.rodata
	.align 32
	.type	BITMASK, @object
	.size	BITMASK, 192
BITMASK:
	.quad	1073741824
	.quad	536870912
	.quad	268435456
	.quad	134217728
	.quad	67108864
	.quad	33554432
	.quad	16777216
	.quad	8388608
	.quad	4194304
	.quad	2097152
	.quad	1048576
	.quad	524288
	.quad	16384
	.quad	8192
	.quad	4096
	.quad	2048
	.quad	1024
	.quad	512
	.quad	256
	.quad	128
	.quad	64
	.quad	32
	.quad	16
	.quad	8
	.align 32
	.type	final_perm, @object
	.size	final_perm, 256
final_perm:
	.long	40
	.long	8
	.long	48
	.long	16
	.long	56
	.long	24
	.long	64
	.long	32
	.long	39
	.long	7
	.long	47
	.long	15
	.long	55
	.long	23
	.long	63
	.long	31
	.long	38
	.long	6
	.long	46
	.long	14
	.long	54
	.long	22
	.long	62
	.long	30
	.long	37
	.long	5
	.long	45
	.long	13
	.long	53
	.long	21
	.long	61
	.long	29
	.long	36
	.long	4
	.long	44
	.long	12
	.long	52
	.long	20
	.long	60
	.long	28
	.long	35
	.long	3
	.long	43
	.long	11
	.long	51
	.long	19
	.long	59
	.long	27
	.long	34
	.long	2
	.long	42
	.long	10
	.long	50
	.long	18
	.long	58
	.long	26
	.long	33
	.long	1
	.long	41
	.long	9
	.long	49
	.long	17
	.long	57
	.long	25
	.align 32
	.type	initial_perm, @object
	.size	initial_perm, 256
initial_perm:
	.long	58
	.long	50
	.long	42
	.long	34
	.long	26
	.long	18
	.long	10
	.long	2
	.long	60
	.long	52
	.long	44
	.long	36
	.long	28
	.long	20
	.long	12
	.long	4
	.long	62
	.long	54
	.long	46
	.long	38
	.long	30
	.long	22
	.long	14
	.long	6
	.long	64
	.long	56
	.long	48
	.long	40
	.long	32
	.long	24
	.long	16
	.long	8
	.long	57
	.long	49
	.long	41
	.long	33
	.long	25
	.long	17
	.long	9
	.long	1
	.long	59
	.long	51
	.long	43
	.long	35
	.long	27
	.long	19
	.long	11
	.long	3
	.long	61
	.long	53
	.long	45
	.long	37
	.long	29
	.long	21
	.long	13
	.long	5
	.long	63
	.long	55
	.long	47
	.long	39
	.long	31
	.long	23
	.long	15
	.long	7
	.align 32
	.type	sbox, @object
	.size	sbox, 2048
sbox:
	.long	14
	.long	4
	.long	13
	.long	1
	.long	2
	.long	15
	.long	11
	.long	8
	.long	3
	.long	10
	.long	6
	.long	12
	.long	5
	.long	9
	.long	0
	.long	7
	.long	0
	.long	15
	.long	7
	.long	4
	.long	14
	.long	2
	.long	13
	.long	1
	.long	10
	.long	6
	.long	12
	.long	11
	.long	9
	.long	5
	.long	3
	.long	8
	.long	4
	.long	1
	.long	14
	.long	8
	.long	13
	.long	6
	.long	2
	.long	11
	.long	15
	.long	12
	.long	9
	.long	7
	.long	3
	.long	10
	.long	5
	.long	0
	.long	15
	.long	12
	.long	8
	.long	2
	.long	4
	.long	9
	.long	1
	.long	7
	.long	5
	.long	11
	.long	3
	.long	14
	.long	10
	.long	0
	.long	6
	.long	13
	.long	15
	.long	1
	.long	8
	.long	14
	.long	6
	.long	11
	.long	3
	.long	4
	.long	9
	.long	7
	.long	2
	.long	13
	.long	12
	.long	0
	.long	5
	.long	10
	.long	3
	.long	13
	.long	4
	.long	7
	.long	15
	.long	2
	.long	8
	.long	14
	.long	12
	.long	0
	.long	1
	.long	10
	.long	6
	.long	9
	.long	11
	.long	5
	.long	0
	.long	14
	.long	7
	.long	11
	.long	10
	.long	4
	.long	13
	.long	1
	.long	5
	.long	8
	.long	12
	.long	6
	.long	9
	.long	3
	.long	2
	.long	15
	.long	13
	.long	8
	.long	10
	.long	1
	.long	3
	.long	15
	.long	4
	.long	2
	.long	11
	.long	6
	.long	7
	.long	12
	.long	0
	.long	5
	.long	14
	.long	9
	.long	10
	.long	0
	.long	9
	.long	14
	.long	6
	.long	3
	.long	15
	.long	5
	.long	1
	.long	13
	.long	12
	.long	7
	.long	11
	.long	4
	.long	2
	.long	8
	.long	13
	.long	7
	.long	0
	.long	9
	.long	3
	.long	4
	.long	6
	.long	10
	.long	2
	.long	8
	.long	5
	.long	14
	.long	12
	.long	11
	.long	15
	.long	1
	.long	13
	.long	6
	.long	4
	.long	9
	.long	8
	.long	15
	.long	3
	.long	0
	.long	11
	.long	1
	.long	2
	.long	12
	.long	5
	.long	10
	.long	14
	.long	7
	.long	1
	.long	10
	.long	13
	.long	0
	.long	6
	.long	9
	.long	8
	.long	7
	.long	4
	.long	15
	.long	14
	.long	3
	.long	11
	.long	5
	.long	2
	.long	12
	.long	7
	.long	13
	.long	14
	.long	3
	.long	0
	.long	6
	.long	9
	.long	10
	.long	1
	.long	2
	.long	8
	.long	5
	.long	11
	.long	12
	.long	4
	.long	15
	.long	13
	.long	8
	.long	11
	.long	5
	.long	6
	.long	15
	.long	0
	.long	3
	.long	4
	.long	7
	.long	2
	.long	12
	.long	1
	.long	10
	.long	14
	.long	9
	.long	10
	.long	6
	.long	9
	.long	0
	.long	12
	.long	11
	.long	7
	.long	13
	.long	15
	.long	1
	.long	3
	.long	14
	.long	5
	.long	2
	.long	8
	.long	4
	.long	3
	.long	15
	.long	0
	.long	6
	.long	10
	.long	1
	.long	13
	.long	8
	.long	9
	.long	4
	.long	5
	.long	11
	.long	12
	.long	7
	.long	2
	.long	14
	.long	2
	.long	12
	.long	4
	.long	1
	.long	7
	.long	10
	.long	11
	.long	6
	.long	8
	.long	5
	.long	3
	.long	15
	.long	13
	.long	0
	.long	14
	.long	9
	.long	14
	.long	11
	.long	2
	.long	12
	.long	4
	.long	7
	.long	13
	.long	1
	.long	5
	.long	0
	.long	15
	.long	10
	.long	3
	.long	9
	.long	8
	.long	6
	.long	4
	.long	2
	.long	1
	.long	11
	.long	10
	.long	13
	.long	7
	.long	8
	.long	15
	.long	9
	.long	12
	.long	5
	.long	6
	.long	3
	.long	0
	.long	14
	.long	11
	.long	8
	.long	12
	.long	7
	.long	1
	.long	14
	.long	2
	.long	13
	.long	6
	.long	15
	.long	0
	.long	9
	.long	10
	.long	4
	.long	5
	.long	3
	.long	12
	.long	1
	.long	10
	.long	15
	.long	9
	.long	2
	.long	6
	.long	8
	.long	0
	.long	13
	.long	3
	.long	4
	.long	14
	.long	7
	.long	5
	.long	11
	.long	10
	.long	15
	.long	4
	.long	2
	.long	7
	.long	12
	.long	9
	.long	5
	.long	6
	.long	1
	.long	13
	.long	14
	.long	0
	.long	11
	.long	3
	.long	8
	.long	9
	.long	14
	.long	15
	.long	5
	.long	2
	.long	8
	.long	12
	.long	3
	.long	7
	.long	0
	.long	4
	.long	10
	.long	1
	.long	13
	.long	11
	.long	6
	.long	4
	.long	3
	.long	2
	.long	12
	.long	9
	.long	5
	.long	15
	.long	10
	.long	11
	.long	14
	.long	1
	.long	7
	.long	6
	.long	0
	.long	8
	.long	13
	.long	4
	.long	11
	.long	2
	.long	14
	.long	15
	.long	0
	.long	8
	.long	13
	.long	3
	.long	12
	.long	9
	.long	7
	.long	5
	.long	10
	.long	6
	.long	1
	.long	13
	.long	0
	.long	11
	.long	7
	.long	4
	.long	9
	.long	1
	.long	10
	.long	14
	.long	3
	.long	5
	.long	12
	.long	2
	.long	15
	.long	8
	.long	6
	.long	1
	.long	4
	.long	11
	.long	13
	.long	12
	.long	3
	.long	7
	.long	14
	.long	10
	.long	15
	.long	6
	.long	8
	.long	0
	.long	5
	.long	9
	.long	2
	.long	6
	.long	11
	.long	13
	.long	8
	.long	1
	.long	4
	.long	10
	.long	7
	.long	9
	.long	5
	.long	0
	.long	15
	.long	14
	.long	2
	.long	3
	.long	12
	.long	13
	.long	2
	.long	8
	.long	4
	.long	6
	.long	15
	.long	11
	.long	1
	.long	10
	.long	9
	.long	3
	.long	14
	.long	5
	.long	0
	.long	12
	.long	7
	.long	1
	.long	15
	.long	13
	.long	8
	.long	10
	.long	3
	.long	7
	.long	4
	.long	12
	.long	5
	.long	6
	.long	11
	.long	0
	.long	14
	.long	9
	.long	2
	.long	7
	.long	11
	.long	4
	.long	1
	.long	9
	.long	12
	.long	14
	.long	2
	.long	0
	.long	6
	.long	10
	.long	13
	.long	15
	.long	3
	.long	5
	.long	8
	.long	2
	.long	1
	.long	14
	.long	7
	.long	4
	.long	10
	.long	8
	.long	13
	.long	15
	.long	12
	.long	9
	.long	0
	.long	3
	.long	5
	.long	6
	.long	11
	.align 32
	.type	perm32, @object
	.size	perm32, 128
perm32:
	.long	16
	.long	7
	.long	20
	.long	21
	.long	29
	.long	12
	.long	28
	.long	17
	.long	1
	.long	15
	.long	23
	.long	26
	.long	5
	.long	18
	.long	31
	.long	10
	.long	2
	.long	8
	.long	24
	.long	14
	.long	32
	.long	27
	.long	3
	.long	9
	.long	19
	.long	13
	.long	30
	.long	6
	.long	22
	.long	11
	.long	4
	.long	25
	.align 32
	.type	esel, @object
	.size	esel, 192
esel:
	.long	32
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	1
	.align 32
	.type	pc2, @object
	.size	pc2, 192
pc2:
	.long	14
	.long	17
	.long	11
	.long	24
	.long	1
	.long	5
	.long	3
	.long	28
	.long	15
	.long	6
	.long	21
	.long	10
	.long	23
	.long	19
	.long	12
	.long	4
	.long	26
	.long	8
	.long	16
	.long	7
	.long	27
	.long	20
	.long	13
	.long	2
	.long	41
	.long	52
	.long	31
	.long	37
	.long	47
	.long	55
	.long	30
	.long	40
	.long	51
	.long	45
	.long	33
	.long	48
	.long	44
	.long	49
	.long	39
	.long	56
	.long	34
	.long	53
	.long	46
	.long	42
	.long	50
	.long	36
	.long	29
	.long	32
	.align 32
	.type	rots, @object
	.size	rots, 64
rots:
	.long	1
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	1
	.align 32
	.type	pc1, @object
	.size	pc1, 224
pc1:
	.long	57
	.long	49
	.long	41
	.long	33
	.long	25
	.long	17
	.long	9
	.long	1
	.long	58
	.long	50
	.long	42
	.long	34
	.long	26
	.long	18
	.long	10
	.long	2
	.long	59
	.long	51
	.long	43
	.long	35
	.long	27
	.long	19
	.long	11
	.long	3
	.long	60
	.long	52
	.long	44
	.long	36
	.long	63
	.long	55
	.long	47
	.long	39
	.long	31
	.long	23
	.long	15
	.long	7
	.long	62
	.long	54
	.long	46
	.long	38
	.long	30
	.long	22
	.long	14
	.long	6
	.long	61
	.long	53
	.long	45
	.long	37
	.long	29
	.long	21
	.long	13
	.long	5
	.long	28
	.long	20
	.long	12
	.long	4
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
