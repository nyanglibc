	.text
#APP
	.symver encrypt_r,encrypt_r@GLIBC_2.2.5
	.symver encrypt,encrypt@GLIBC_2.2.5
	.symver setkey_r,setkey_r@GLIBC_2.2.5
	.symver setkey,setkey@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__init_des_r
	.type	__init_des_r, @function
__init_des_r:
	pushq	%r15
	pushq	%r14
	leaq	32896(%rdi), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leaq	128(%rdi), %rbp
	subq	$344, %rsp
	movq	%rax, 56(%rsp)
	leaq	65664(%rdi), %rax
	movq	%rdi, 40(%rsp)
	movq	%rbp, 48(%rsp)
	movq	%rax, 64(%rsp)
	leaq	98432(%rdi), %rax
	movq	%rax, 72(%rsp)
	movl	small_tables_initialized.7490(%rip), %eax
	testl	%eax, %eax
	jne	.L2
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L3
	leaq	_ufc_tables_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L3:
	movl	small_tables_initialized.7490(%rip), %eax
	leaq	eperm32tab(%rip), %rbx
	testl	%eax, %eax
	je	.L55
.L4:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L20
	leaq	_ufc_tables_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L20:
	movl	$131072, %edx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	memset@PLT
	leaq	sbox(%rip), %r12
	leaq	48(%rsp), %rax
	movl	$24, %r13d
	movl	$0, 28(%rsp)
	movl	$15, 24(%rsp)
	movl	%r13d, %ecx
	movq	%rax, 32(%rsp)
	movq	%r12, 16(%rsp)
	movl	$14, %eax
.L21:
	movslq	28(%rsp), %rdx
	movq	32(%rsp), %rdi
	movl	$0, 4(%rsp)
	movq	(%rdi), %r14
	leal	1(%rdx), %r15d
	leaq	0(,%rdx,4), %rdi
	movslq	%r15d, %r15
	movq	%rdi, 8(%rsp)
	salq	$2, %r15
	.p2align 4,,10
	.p2align 3
.L26:
	movl	4(%rsp), %ebp
	movl	24(%rsp), %edx
	movslq	%eax, %r11
	salq	$4, %r11
	xorl	%r9d, %r9d
	sall	$6, %ebp
	movslq	%edx, %rax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L56:
	movl	%r9d, %eax
	movl	%r9d, %esi
	sarl	%eax
	andl	$1, %esi
	movl	%eax, %edx
	movl	%r9d, %eax
	sarl	$4, %eax
	andl	$15, %edx
	andl	$2, %eax
	orl	%esi, %eax
	cltq
	addq	%r15, %rax
	salq	$4, %rax
	addq	%rdx, %rax
	movslq	(%r12,%rax,4), %rax
.L23:
	orq	%r11, %rax
	movl	%r9d, %esi
	addl	$1, %r9d
	salq	%cl, %rax
	orl	%ebp, %esi
	movq	%rax, %rdx
	movq	%rax, %r10
	movzbl	%ah, %edi
	shrq	$16, %rdx
	shrq	$20, %r10
	movzbl	%al, %eax
	movzbl	%dl, %edx
	andl	$4080, %r10d
	movslq	%esi, %rsi
	movq	%rdx, %r13
	addq	%rbx, %r10
	addq	$256, %rdx
	salq	$4, %r13
	movq	8(%r10), %r8
	salq	$4, %rdx
	orq	4104(%rbx,%r13), %r8
	movq	%rax, %r13
	addq	$768, %rax
	salq	$4, %r13
	salq	$4, %rax
	orq	12296(%rbx,%r13), %r8
	movq	%rdi, %r13
	movq	(%rbx,%rax), %rax
	salq	$4, %r13
	orq	8200(%rbx,%r13), %r8
	salq	$32, %rax
	orq	%rax, %r8
	movq	(%r10), %rax
	salq	$32, %rax
	orq	%rax, %r8
	movq	(%rbx,%rdx), %rax
	salq	$32, %rax
	orq	%rax, %r8
	leaq	512(%rdi), %rax
	salq	$4, %rax
	movq	(%rbx,%rax), %rax
	salq	$32, %rax
	orq	%rax, %r8
	cmpl	$64, %r9d
	movq	%r8, (%r14,%rsi,8)
	jne	.L56
	addl	$1, 4(%rsp)
	movl	4(%rsp), %eax
	cmpl	$64, %eax
	je	.L57
	movl	4(%rsp), %edi
	movl	%edi, %eax
	sarl	%eax
	movl	%eax, %edx
	movl	%edi, %eax
	andl	$1, %edi
	sarl	$4, %eax
	andl	$15, %edx
	andl	$2, %eax
	orl	%edi, %eax
	cltq
	addq	8(%rsp), %rax
	salq	$4, %rax
	addq	%rdx, %rax
	movl	(%r12,%rax,4), %eax
	jmp	.L26
.L57:
	subl	$8, %ecx
	addq	$512, 16(%rsp)
	addq	$8, 32(%rsp)
	movq	16(%rsp), %rax
	addl	$2, 28(%rsp)
	cmpl	$-8, %ecx
	je	.L25
	movl	256(%rax), %edi
	movl	(%rax), %eax
	movl	%edi, 24(%rsp)
	jmp	.L21
.L25:
	movq	40(%rsp), %rax
	xorl	%edx, %edx
	movq	$0, 131214(%rax)
	movw	%dx, 131222(%rax)
	addl	$1, 131228(%rax)
	addq	$344, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L55:
	leaq	do_pc1(%rip), %rdi
	xorl	%esi, %esi
	movl	$16384, %edx
	leaq	bytemask(%rip), %r14
	leaq	longmask(%rip), %r13
	call	memset@PLT
	leaq	pc1(%rip), %r9
	leaq	do_pc1(%rip), %r8
	xorl	%esi, %esi
	movl	$613566757, %edi
.L8:
	movl	(%r9,%rsi,4), %r11d
	movl	%esi, %r15d
	leal	-1(%r11), %ebx
	movl	%ebx, %edx
	sarl	$31, %edx
	shrl	$29, %edx
	leal	(%rbx,%rdx), %eax
	andl	$7, %eax
	subl	%edx, %eax
	movl	%esi, %edx
	addl	$1, %eax
	shrl	$2, %edx
	cltq
	movzbl	(%r14,%rax), %ecx
	movl	%edx, %eax
	mull	%edi
	imull	$28, %edx, %eax
	movslq	%edx, %rdx
	subl	%eax, %r15d
	movl	%r15d, %eax
	addl	$4, %eax
	testl	%ebx, %ebx
	cltq
	movq	0(%r13,%rax,8), %r10
	leal	6(%r11), %eax
	cmovns	%ebx, %eax
	sarl	$3, %eax
	cltq
	leaq	(%rdx,%rax,2), %rdx
	xorl	%eax, %eax
	salq	$10, %rdx
	addq	%r8, %rdx
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$1, %rax
	cmpq	$128, %rax
	je	.L58
.L7:
	testq	%rax, %rcx
	je	.L5
	orq	%r10, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	$128, %rax
	jne	.L7
.L58:
	addq	$1, %rsi
	cmpq	$56, %rsi
	jne	.L8
	leaq	do_pc2(%rip), %r8
	movl	$1024, %ecx
	xorl	%eax, %eax
	leaq	pc2(%rip), %r10
	leaq	BITMASK(%rip), %r12
	xorl	%esi, %esi
	movq	%r8, %rdi
	movl	$-1840700269, %r9d
	rep stosq
	movl	$-1431655765, %edi
.L12:
	movl	(%r10,%rsi,4), %eax
	movl	%esi, %ebx
	leal	-1(%rax), %r11d
	movl	%r11d, %eax
	imull	%r9d
	movl	%r11d, %eax
	sarl	$31, %eax
	leal	(%rdx,%r11), %ecx
	sarl	$2, %ecx
	subl	%eax, %ecx
	leal	0(,%rcx,8), %eax
	subl	%ecx, %eax
	subl	%eax, %r11d
	movl	%r11d, %eax
	addl	$1, %eax
	cltq
	movzbl	(%r14,%rax), %r11d
	movl	%esi, %eax
	mull	%edi
	movl	%edx, %eax
	movslq	%ecx, %rdx
	shrl	$4, %eax
	salq	$10, %rdx
	leal	(%rax,%rax,2), %eax
	addq	%r8, %rdx
	sall	$3, %eax
	subl	%eax, %ebx
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %rbx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$1, %rax
	cmpq	$128, %rax
	je	.L59
.L11:
	testq	%rax, %r11
	je	.L9
	orq	%rbx, (%rdx,%rax,8)
	addq	$1, %rax
	cmpq	$128, %rax
	jne	.L11
.L59:
	addq	$1, %rsi
	cmpq	$48, %rsi
	jne	.L12
	leaq	eperm32tab(%rip), %rdi
	xorl	%esi, %esi
	movl	$16384, %edx
	leaq	eperm32tab(%rip), %rbx
	call	memset@PLT
	leaq	perm32(%rip), %r9
	leaq	esel(%rip), %rdi
	xorl	%esi, %esi
	movl	$-1431655765, %r8d
.L15:
	movl	(%rdi,%rsi,4), %eax
	subl	$1, %eax
	cltq
	movl	(%r9,%rax,4), %r10d
	subl	$1, %r10d
	movl	%r10d, %eax
	andl	$7, %eax
	movzbl	(%r14,%rax), %ecx
	movl	%esi, %eax
	mull	%r8d
	movl	%edx, %r15d
	shrl	$4, %r15d
	movslq	%r15d, %rdx
	movl	%esi, %r15d
	leal	(%rdx,%rdx,2), %eax
	sall	$3, %eax
	subl	%eax, %r15d
	movslq	%r10d, %rax
	shrq	$3, %rax
	movslq	%r15d, %r15
	salq	$9, %rax
	addq	%rdx, %rax
	leaq	(%rbx,%rax,8), %r10
	movl	$255, %eax
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rax, %rcx
	je	.L13
	movq	%rax, %rdx
	movq	(%r12,%r15,8), %r11
	salq	$4, %rdx
	orq	%r11, (%r10,%rdx)
.L13:
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L14
	addq	$1, %rsi
	cmpq	$48, %rsi
	jne	.L15
	movl	$47, %eax
.L16:
	movl	(%rdi,%rax,4), %edx
	leal	-1(%rdx), %ecx
	addl	$31, %edx
	movslq	%edx, %rdx
	movslq	%ecx, %rcx
	movl	%eax, 80(%rsp,%rcx,4)
	leal	48(%rax), %ecx
	subq	$1, %rax
	cmpq	$-1, %rax
	movl	%ecx, 80(%rsp,%rdx,4)
	jne	.L16
	leaq	efp(%rip), %rdi
	movl	$16384, %edx
	xorl	%esi, %esi
	call	memset@PLT
	leaq	final_perm(%rip), %r11
	leaq	efp(%rip), %r10
	xorl	%r8d, %r8d
	movl	$715827883, %r9d
.L19:
	movl	(%r11,%r8,4), %eax
	subl	$1, %eax
	cltq
	movl	80(%rsp,%rax,4), %ecx
	movl	%ecx, %eax
	imull	%r9d
	movl	%ecx, %eax
	sarl	$31, %eax
	subl	%eax, %edx
	movslq	%edx, %rax
	leal	(%rax,%rax,2), %edx
	salq	$7, %rax
	addl	%edx, %edx
	subl	%edx, %ecx
	movl	%ecx, %edx
	addl	$26, %edx
	movslq	%edx, %rdx
	movq	0(%r13,%rdx,8), %rsi
	movq	%r8, %rdx
	andl	$31, %edx
	movq	0(%r13,%rdx,8), %rdi
	movl	%r8d, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	addq	%rdx, %rax
	leaq	(%r10,%rax,8), %rcx
	movl	$63, %eax
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%rax, %rsi
	je	.L17
	movq	%rax, %rdx
	salq	$4, %rdx
	orq	%rdi, (%rcx,%rdx)
.L17:
	subq	$1, %rax
	cmpq	$-1, %rax
	jne	.L18
	addq	$1, %r8
	cmpq	$64, %r8
	jne	.L19
	movl	$1, small_tables_initialized.7490(%rip)
	jmp	.L4
.L2:
	leaq	eperm32tab(%rip), %rbx
	jmp	.L20
	.size	__init_des_r, .-__init_des_r
	.p2align 4,,15
	.globl	__init_des
	.type	__init_des, @function
__init_des:
	movq	_ufc_foobar@GOTPCREL(%rip), %rdi
	jmp	__init_des_r@PLT
	.size	__init_des, .-__init_des
	.p2align 4,,15
	.globl	_ufc_setup_salt_r
	.type	_ufc_setup_salt_r, @function
_ufc_setup_salt_r:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	131228(%rsi), %eax
	testl	%eax, %eax
	je	.L92
.L62:
	movzbl	(%rbx), %edx
	cmpb	$90, %dl
	jg	.L64
	cmpb	$65, %dl
	jge	.L65
	leal	-46(%rdx), %eax
	cmpb	$11, %al
	jbe	.L65
.L80:
	xorl	%eax, %eax
.L61:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	leal	-97(%rdx), %eax
	cmpb	$25, %al
	ja	.L80
.L65:
	movzbl	1(%rbx), %ecx
	cmpb	$90, %cl
	jg	.L66
	cmpb	$65, %cl
	jge	.L67
	leal	-46(%rcx), %eax
	cmpb	$11, %al
	ja	.L80
.L67:
	cmpb	%dl, 131214(%rsi)
	je	.L93
.L68:
	leaq	BITMASK(%rip), %r9
	movb	%cl, 131215(%rsi)
	movb	%dl, 131214(%rsi)
	xorl	%edi, %edi
	xorl	%ecx, %ecx
.L74:
	movsbl	(%rbx), %eax
	cmpb	$96, %al
	jle	.L69
	subl	$59, %eax
	movslq	%eax, %rdx
.L70:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L73:
	btq	%rax, %rdx
	jnc	.L72
	leaq	(%rax,%rdi), %r8
	orq	(%r9,%r8,8), %rcx
.L72:
	addq	$1, %rax
	cmpq	$6, %rax
	jne	.L73
	addq	$6, %rdi
	addq	$1, %rbx
	cmpq	$12, %rdi
	jne	.L74
	movq	131216(%rsi), %r10
	leaq	128(%rsi), %rax
	leaq	32896(%rsi), %r9
	xorq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L75:
	movq	(%rax), %r8
	addq	$8, %rax
	movq	%r8, %rdx
	shrq	$32, %rdx
	xorq	%r8, %rdx
	andq	%r10, %rdx
	movq	%rdx, %rdi
	salq	$32, %rdi
	orq	%rdi, %rdx
	xorq	%r8, %rdx
	cmpq	%r9, %rax
	movq	%rdx, -8(%rax)
	jne	.L75
	movq	131216(%rsi), %r10
	leaq	65664(%rsi), %r9
	xorq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L76:
	movq	(%rax), %r8
	addq	$8, %rax
	movq	%r8, %rdx
	shrq	$32, %rdx
	xorq	%r8, %rdx
	andq	%r10, %rdx
	movq	%rdx, %rdi
	salq	$32, %rdi
	orq	%rdi, %rdx
	xorq	%r8, %rdx
	cmpq	%r9, %rax
	movq	%rdx, -8(%rax)
	jne	.L76
	movq	131216(%rsi), %r10
	leaq	98432(%rsi), %r9
	xorq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L77:
	movq	(%rax), %r8
	addq	$8, %rax
	movq	%r8, %rdx
	shrq	$32, %rdx
	xorq	%r8, %rdx
	andq	%r10, %rdx
	movq	%rdx, %rdi
	salq	$32, %rdi
	orq	%rdi, %rdx
	xorq	%r8, %rdx
	cmpq	%r9, %rax
	movq	%rdx, -8(%rax)
	jne	.L77
	movq	131216(%rsi), %r10
	leaq	131200(%rsi), %r9
	xorq	%rcx, %r10
	.p2align 4,,10
	.p2align 3
.L78:
	movq	(%rax), %r8
	addq	$8, %rax
	movq	%r8, %rdx
	shrq	$32, %rdx
	xorq	%r8, %rdx
	andq	%r10, %rdx
	movq	%rdx, %rdi
	salq	$32, %rdi
	orq	%rdi, %rdx
	xorq	%r8, %rdx
	cmpq	%r9, %rax
	movq	%rdx, -8(%rax)
	jne	.L78
	movq	%rcx, 131216(%rsi)
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
.L66:
	leal	-97(%rcx), %eax
	cmpb	$25, %al
	ja	.L80
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	__init_des_r@PLT
	movq	8(%rsp), %rsi
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L69:
	cmpb	$64, %al
	jle	.L71
	subl	$53, %eax
	movslq	%eax, %rdx
	jmp	.L70
.L71:
	subl	$46, %eax
	movslq	%eax, %rdx
	jmp	.L70
.L93:
	cmpb	%cl, 131215(%rsi)
	movl	$1, %eax
	jne	.L68
	jmp	.L61
	.size	_ufc_setup_salt_r, .-_ufc_setup_salt_r
	.p2align 4,,15
	.globl	_ufc_mk_keytab_r
	.type	_ufc_mk_keytab_r, @function
_ufc_mk_keytab_r:
	pushq	%r15
	leaq	8(%rdi), %r9
	pushq	%r14
	leaq	do_pc1(%rip), %rcx
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L95:
	movzbl	(%rdi), %eax
	addq	$1, %rdi
	andl	$127, %eax
	orq	(%rcx,%rax,8), %rdx
	orq	1024(%rcx,%rax,8), %r8
	addq	$2048, %rcx
	cmpq	%r9, %rdi
	jne	.L95
	leaq	1024+do_pc2(%rip), %r15
	xorl	%edi, %edi
	movl	$1, %r9d
	leaq	-1024(%r15), %r14
	leaq	2048(%r14), %r12
	leaq	3072(%r14), %r13
	leaq	2048(%r12), %rbp
	leaq	3072(%rbp), %rbx
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L100:
	leaq	rots(%rip), %rax
	movl	(%rax,%rdi), %r9d
.L97:
	movl	$28, %eax
	movl	%r9d, %ecx
	movq	%rdx, %r10
	subl	%r9d, %eax
	salq	%cl, %r10
	movq	%r8, %r11
	movl	%eax, %ecx
	shrq	%cl, %rdx
	orq	%r10, %rdx
	movq	%rdx, %r10
	movq	%rdx, %rcx
	shrq	$14, %r10
	shrq	$21, %rcx
	andl	$127, %ecx
	andl	$127, %r10d
	movq	(%r15,%r10,8), %r10
	orq	(%r14,%rcx,8), %r10
	movq	%rdx, %rcx
	andl	$127, %ecx
	orq	0(%r13,%rcx,8), %r10
	movq	%rdx, %rcx
	shrq	$7, %rcx
	andl	$127, %ecx
	orq	(%r12,%rcx,8), %r10
	movl	%r9d, %ecx
	leaq	5120+do_pc2(%rip), %r9
	salq	%cl, %r11
	movl	%eax, %ecx
	shrq	%cl, %r8
	orq	%r11, %r8
	movabsq	$140737488388096, %r11
	movq	%r8, %rcx
	movq	%r8, %rax
	salq	$32, %r10
	andl	$127, %eax
	shrq	$21, %rcx
	orq	(%rbx,%rax,8), %r11
	andl	$127, %ecx
	movq	0(%rbp,%rcx,8), %rax
	movq	%r8, %rcx
	shrq	$14, %rcx
	andl	$127, %ecx
	orq	%r11, %rax
	orq	(%r9,%rcx,8), %rax
	movq	%r8, %rcx
	shrq	$7, %rcx
	leaq	1024(%r9), %r11
	andl	$127, %ecx
	orq	(%r11,%rcx,8), %rax
	orq	%r10, %rax
	movq	%rax, (%rsi,%rdi,2)
	addq	$4, %rdi
	cmpq	$64, %rdi
	jne	.L100
	popq	%rbx
	movl	$0, 131224(%rsi)
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	_ufc_mk_keytab_r, .-_ufc_mk_keytab_r
	.p2align 4,,15
	.globl	_ufc_dofinalperm_r
	.type	_ufc_dofinalperm_r, @function
_ufc_dofinalperm_r:
	movq	(%rdi), %rcx
	movq	8(%rdi), %r10
	leaq	efp(%rip), %rax
	pushq	%rbx
	movq	131216(%rsi), %rsi
	movq	16(%rdi), %r9
	movq	24(%rdi), %rdx
	movq	%rcx, %r8
	xorq	%r10, %r8
	andq	%rsi, %r8
	xorq	%r8, %rcx
	xorq	%r10, %r8
	movq	%r9, %r10
	xorq	%rdx, %r10
	andq	%rsi, %r10
	xorq	%r10, %r9
	xorq	%rdx, %r10
	movq	%r10, %r11
	movq	%r10, %rdx
	shrq	$3, %r11
	shrq	$9, %rdx
	andl	$63, %r11d
	andl	$63, %edx
	leaq	960(%r11), %rsi
	leaq	896(%rdx), %rbx
	salq	$4, %rdx
	salq	$4, %rbx
	salq	$4, %rsi
	movq	(%rax,%rsi), %rsi
	orq	(%rax,%rbx), %rsi
	movq	%r11, %rbx
	salq	$4, %rbx
	movq	%rdx, %r11
	movq	15368(%rax,%rbx), %rdx
	orq	14344(%rax,%r11), %rdx
	movq	%r10, %r11
	shrq	$19, %r11
	shrq	$25, %r10
	andl	$63, %r11d
	andl	$63, %r10d
	leaq	832(%r11), %rbx
	salq	$4, %r11
	orq	13320(%rax,%r11), %rdx
	leaq	768(%r10), %r11
	salq	$4, %r10
	orq	12296(%rax,%r10), %rdx
	movq	%r9, %r10
	salq	$4, %rbx
	shrq	$3, %r10
	orq	(%rax,%rbx), %rsi
	salq	$4, %r11
	andl	$63, %r10d
	orq	(%rax,%r11), %rsi
	leaq	704(%r10), %r11
	salq	$4, %r11
	salq	$4, %r10
	orq	11272(%rax,%r10), %rdx
	movq	%r9, %r10
	orq	(%rax,%r11), %rsi
	shrq	$9, %r10
	andl	$63, %r10d
	leaq	640(%r10), %r11
	salq	$4, %r10
	orq	10248(%rax,%r10), %rdx
	movq	%r9, %r10
	shrq	$25, %r9
	shrq	$19, %r10
	salq	$4, %r11
	andl	$63, %r9d
	andl	$63, %r10d
	orq	(%rax,%r11), %rsi
	leaq	576(%r10), %r11
	salq	$4, %r10
	orq	9224(%rax,%r10), %rdx
	leaq	512(%r9), %r10
	salq	$4, %r9
	orq	8200(%rax,%r9), %rdx
	movq	%r8, %r9
	salq	$4, %r11
	shrq	$3, %r9
	orq	(%rax,%r11), %rsi
	salq	$4, %r10
	andl	$63, %r9d
	orq	(%rax,%r10), %rsi
	leaq	448(%r9), %r10
	salq	$4, %r9
	orq	7176(%rax,%r9), %rdx
	movq	%r8, %r9
	shrq	$9, %r9
	salq	$4, %r10
	andl	$63, %r9d
	orq	(%rax,%r10), %rsi
	leaq	384(%r9), %r10
	salq	$4, %r10
	orq	(%rax,%r10), %rsi
	salq	$4, %r9
	orq	6152(%rax,%r9), %rdx
	movq	%r8, %r9
	shrq	$25, %r8
	shrq	$19, %r9
	andl	$63, %r8d
	andl	$63, %r9d
	leaq	320(%r9), %r10
	salq	$4, %r9
	orq	5128(%rax,%r9), %rdx
	leaq	256(%r8), %r9
	salq	$4, %r8
	orq	4104(%rax,%r8), %rdx
	movq	%rcx, %r8
	salq	$4, %r10
	shrq	$3, %r8
	orq	(%rax,%r10), %rsi
	salq	$4, %r9
	andl	$63, %r8d
	orq	(%rax,%r9), %rsi
	leaq	192(%r8), %r9
	salq	$4, %r8
	orq	3080(%rax,%r8), %rdx
	movq	%rcx, %r8
	shrq	$9, %r8
	salq	$4, %r9
	andl	$63, %r8d
	orq	(%rax,%r9), %rsi
	leaq	128(%r8), %r9
	salq	$4, %r8
	salq	$4, %r9
	orq	(%rax,%r9), %rsi
	orq	2056(%rax,%r8), %rdx
	movq	%rcx, %r8
	shrq	$19, %r8
	andl	$63, %r8d
	leaq	64(%r8), %r9
	salq	$4, %r8
	orq	1032(%rax,%r8), %rdx
	salq	$4, %r9
	orq	(%rax,%r9), %rsi
	shrq	$21, %rcx
	andl	$1008, %ecx
	addq	%rcx, %rax
	orq	8(%rax), %rdx
	orq	(%rax), %rsi
	popq	%rbx
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	ret
	.size	_ufc_dofinalperm_r, .-_ufc_dofinalperm_r
	.p2align 4,,15
	.globl	_ufc_output_conversion_r
	.type	_ufc_output_conversion_r, @function
_ufc_output_conversion_r:
	movzbl	(%rdx), %eax
	movzbl	1(%rdx), %edx
	movq	%rcx, %r8
	testb	%dl, %dl
	movb	%al, 131200(%rcx)
	cmovne	%edx, %eax
	leaq	131202(%rcx), %rdx
	movb	%al, 131201(%rcx)
	movl	$26, %ecx
.L108:
	movq	%rdi, %rax
	shrq	%cl, %rax
	andl	$63, %eax
	cmpq	$37, %rax
	jbe	.L105
	addl	$59, %eax
.L106:
	subl	$6, %ecx
	movb	%al, (%rdx)
	addq	$1, %rdx
	cmpl	$-4, %ecx
	jne	.L108
	leal	0(,%rsi,4), %r9d
	movq	%rsi, %rax
	salq	$30, %rdi
	shrq	$2, %rax
	movl	%edi, %esi
	leaq	131207(%r8), %rdx
	andl	$60, %r9d
	orq	%rax, %rsi
	movl	$26, %ecx
.L112:
	movq	%rsi, %rax
	shrq	%cl, %rax
	andl	$63, %eax
	cmpq	$37, %rax
	jbe	.L109
	addl	$59, %eax
.L110:
	subl	$6, %ecx
	movb	%al, (%rdx)
	addq	$1, %rdx
	cmpl	$-4, %ecx
	jne	.L112
	cmpl	$37, %r9d
	jg	.L120
	leal	53(%r9), %edx
	leal	46(%r9), %eax
	cmpl	$12, %r9d
	movb	$0, 131213(%r8)
	cmovl	%eax, %edx
	movl	%edx, %r9d
	movb	%r9b, 131212(%r8)
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	leal	53(%rax), %r10d
	leal	46(%rax), %r9d
	cmpq	$12, %rax
	movl	%r10d, %eax
	cmovb	%r9d, %eax
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L109:
	leal	53(%rax), %r10d
	leal	46(%rax), %edi
	cmpq	$12, %rax
	movl	%r10d, %eax
	cmovb	%edi, %eax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L120:
	addl	$59, %r9d
	movb	$0, 131213(%r8)
	movb	%r9b, 131212(%r8)
	ret
	.size	_ufc_output_conversion_r, .-_ufc_output_conversion_r
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	".."
	.text
	.p2align 4,,15
	.globl	__encrypt_r
	.type	__encrypt_r, @function
__encrypt_r:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	movq	%rdx, %rbp
	movq	%rdx, %rsi
	subq	$32, %rsp
	call	_ufc_setup_salt_r@PLT
	movl	131224(%rbp), %eax
	testl	%r12d, %r12d
	sete	%dl
	testl	%eax, %eax
	sete	%al
	cmpb	%al, %dl
	je	.L122
	leaq	120(%rbp), %rax
	leaq	56(%rbp), %rdi
	movq	%rbp, %rdx
	.p2align 4,,10
	.p2align 3
.L123:
	movq	(%rdx), %rsi
	movq	(%rax), %rcx
	subq	$8, %rax
	addq	$8, %rdx
	movq	%rsi, 8(%rax)
	movq	%rcx, -8(%rdx)
	cmpq	%rax, %rdi
	jne	.L123
	movl	%r12d, 131224(%rbp)
.L122:
	xorl	%edx, %edx
	movl	$8, %eax
	xorl	%ecx, %ecx
	leaq	BITMASK(%rip), %r8
	leaq	initial_perm(%rip), %rdi
	leaq	esel(%rip), %rsi
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L142:
	movl	(%rsi,%rdx), %eax
	subl	$1, %eax
	cltq
	movslq	(%rdi,%rax,4), %rax
.L126:
	cmpb	$0, -1(%rbx,%rax)
	je	.L124
	orq	(%r8,%rdx,2), %rcx
.L124:
	addq	$4, %rdx
	cmpq	$96, %rdx
	jne	.L142
	xorl	%edx, %edx
	movl	$4, %eax
	xorl	%esi, %esi
	leaq	BITMASK(%rip), %r9
	leaq	initial_perm(%rip), %r8
	leaq	96+esel(%rip), %rdi
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L143:
	movl	(%rdi,%rdx), %eax
	subl	$1, %eax
	cltq
	movslq	(%r8,%rax,4), %rax
.L125:
	cmpb	$0, -1(%rbx,%rax)
	je	.L127
	orq	(%r9,%rdx,2), %rsi
.L127:
	addq	$4, %rdx
	cmpq	$96, %rdx
	jne	.L143
	xorl	%edx, %edx
	movl	$7, %eax
	xorl	%edi, %edi
	leaq	BITMASK(%rip), %r10
	leaq	initial_perm(%rip), %r9
	leaq	esel(%rip), %r8
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L144:
	movl	(%r8,%rdx), %eax
	addl	$31, %eax
	cltq
	movslq	(%r9,%rax,4), %rax
.L128:
	cmpb	$0, -1(%rbx,%rax)
	je	.L129
	orq	(%r10,%rdx,2), %rdi
.L129:
	addq	$4, %rdx
	cmpq	$96, %rdx
	jne	.L144
	xorl	%edx, %edx
	movl	$3, %eax
	xorl	%r8d, %r8d
	leaq	BITMASK(%rip), %r11
	leaq	initial_perm(%rip), %r10
	leaq	96+esel(%rip), %r9
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L145:
	movl	(%r9,%rdx), %eax
	addl	$31, %eax
	cltq
	movslq	(%r10,%rax,4), %rax
.L130:
	cmpb	$0, -1(%rbx,%rax)
	je	.L131
	orq	(%r11,%rdx,2), %r8
.L131:
	addq	$4, %rdx
	cmpq	$96, %rdx
	jne	.L145
	movq	%rsp, %r12
	movq	%rsi, 8(%rsp)
	movq	%rdi, 16(%rsp)
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	$1, %edi
	movq	%rcx, (%rsp)
	movq	%r8, 24(%rsp)
	call	_ufc_doit_r@PLT
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	_ufc_dofinalperm_r@PLT
	movq	(%rsp), %rsi
	movq	8(%rsp), %rcx
	xorl	%eax, %eax
	movl	$2147483648, %edx
	leaq	longmask(%rip), %rdi
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%rdi,%rax,8), %rdx
.L134:
	testq	%rdx, %rsi
	setne	(%rbx,%rax)
	addq	$1, %rax
	cmpq	$32, %rax
	jne	.L146
	xorl	%eax, %eax
	movl	$2147483648, %edx
	leaq	longmask(%rip), %rsi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rsi,%rax,8), %rdx
.L133:
	testq	%rdx, %rcx
	setne	32(%rbx,%rax)
	addq	$1, %rax
	cmpq	$32, %rax
	jne	.L147
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__encrypt_r, .-__encrypt_r
	.weak	encrypt_r
	.set	encrypt_r,__encrypt_r
	.p2align 4,,15
	.globl	encrypt
	.type	encrypt, @function
encrypt:
	movq	_ufc_foobar@GOTPCREL(%rip), %rdx
	jmp	__encrypt_r@PLT
	.size	encrypt, .-encrypt
	.p2align 4,,15
	.globl	__setkey_r
	.type	__setkey_r, @function
__setkey_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	movq	%rsi, %rbp
	subq	$24, %rsp
	call	_ufc_setup_salt_r@PLT
	leaq	8(%rsp), %rdi
	leaq	64(%rbx), %r8
	movq	%rdi, %rsi
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	8(%rbx), %rcx
	movq	%rbx, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L151:
	addq	$1, %rax
	addl	%edx, %edx
	orb	-1(%rax), %dl
	cmpq	%rcx, %rax
	jne	.L151
	shrb	%dl
	addq	$1, %rsi
	movq	%rax, %rbx
	movb	%dl, -1(%rsi)
	cmpq	%rax, %r8
	jne	.L150
	movq	%rbp, %rsi
	call	_ufc_mk_keytab_r@PLT
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__setkey_r, .-__setkey_r
	.weak	setkey_r
	.set	setkey_r,__setkey_r
	.p2align 4,,15
	.globl	setkey
	.type	setkey, @function
setkey:
	movq	_ufc_foobar@GOTPCREL(%rip), %rsi
	jmp	__setkey_r@PLT
	.size	setkey, .-setkey
	.p2align 4,,15
	.globl	__b64_from_24bit
	.type	__b64_from_24bit, @function
__b64_from_24bit:
	sall	$16, %edx
	sall	$8, %ecx
	orl	%r8d, %ecx
	movl	%edx, %r8d
	leal	-1(%r9), %edx
	orl	%ecx, %r8d
	testl	%r9d, %r9d
	jle	.L156
	movl	(%rsi), %eax
	testl	%eax, %eax
	jle	.L156
	leaq	b64t(%rip), %r9
	jmp	.L158
	.p2align 4,,10
	.p2align 3
.L163:
	testl	%eax, %eax
	jle	.L156
.L158:
	movq	(%rdi), %rax
	subl	$1, %edx
	leaq	1(%rax), %rcx
	movq	%rcx, (%rdi)
	movl	%r8d, %ecx
	shrl	$6, %r8d
	andl	$63, %ecx
	movzbl	(%r9,%rcx), %ecx
	movb	%cl, (%rax)
	movl	(%rsi), %eax
	subl	$1, %eax
	cmpl	$-1, %edx
	movl	%eax, (%rsi)
	jne	.L163
.L156:
	rep ret
	.size	__b64_from_24bit, .-__b64_from_24bit
	.local	small_tables_initialized.7490
	.comm	small_tables_initialized.7490,4,4
	.local	_ufc_tables_lock
	.comm	_ufc_tables_lock,40,32
	.comm	_ufc_foobar,131232,32
	.section	.rodata
	.align 32
	.type	b64t, @object
	.size	b64t, 64
b64t:
	.ascii	"./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuv"
	.ascii	"wxyz"
	.local	efp
	.comm	efp,16384,32
	.local	eperm32tab
	.comm	eperm32tab,16384,32
	.local	do_pc2
	.comm	do_pc2,8192,32
	.local	do_pc1
	.comm	do_pc1,16384,32
	.align 32
	.type	longmask, @object
	.size	longmask, 256
longmask:
	.quad	2147483648
	.quad	1073741824
	.quad	536870912
	.quad	268435456
	.quad	134217728
	.quad	67108864
	.quad	33554432
	.quad	16777216
	.quad	8388608
	.quad	4194304
	.quad	2097152
	.quad	1048576
	.quad	524288
	.quad	262144
	.quad	131072
	.quad	65536
	.quad	32768
	.quad	16384
	.quad	8192
	.quad	4096
	.quad	2048
	.quad	1024
	.quad	512
	.quad	256
	.quad	128
	.quad	64
	.quad	32
	.quad	16
	.quad	8
	.quad	4
	.quad	2
	.quad	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	bytemask, @object
	.size	bytemask, 8
bytemask:
	.byte	-128
	.byte	64
	.byte	32
	.byte	16
	.byte	8
	.byte	4
	.byte	2
	.byte	1
	.section	.rodata
	.align 32
	.type	BITMASK, @object
	.size	BITMASK, 192
BITMASK:
	.quad	1073741824
	.quad	536870912
	.quad	268435456
	.quad	134217728
	.quad	67108864
	.quad	33554432
	.quad	16777216
	.quad	8388608
	.quad	4194304
	.quad	2097152
	.quad	1048576
	.quad	524288
	.quad	16384
	.quad	8192
	.quad	4096
	.quad	2048
	.quad	1024
	.quad	512
	.quad	256
	.quad	128
	.quad	64
	.quad	32
	.quad	16
	.quad	8
	.align 32
	.type	final_perm, @object
	.size	final_perm, 256
final_perm:
	.long	40
	.long	8
	.long	48
	.long	16
	.long	56
	.long	24
	.long	64
	.long	32
	.long	39
	.long	7
	.long	47
	.long	15
	.long	55
	.long	23
	.long	63
	.long	31
	.long	38
	.long	6
	.long	46
	.long	14
	.long	54
	.long	22
	.long	62
	.long	30
	.long	37
	.long	5
	.long	45
	.long	13
	.long	53
	.long	21
	.long	61
	.long	29
	.long	36
	.long	4
	.long	44
	.long	12
	.long	52
	.long	20
	.long	60
	.long	28
	.long	35
	.long	3
	.long	43
	.long	11
	.long	51
	.long	19
	.long	59
	.long	27
	.long	34
	.long	2
	.long	42
	.long	10
	.long	50
	.long	18
	.long	58
	.long	26
	.long	33
	.long	1
	.long	41
	.long	9
	.long	49
	.long	17
	.long	57
	.long	25
	.align 32
	.type	initial_perm, @object
	.size	initial_perm, 256
initial_perm:
	.long	58
	.long	50
	.long	42
	.long	34
	.long	26
	.long	18
	.long	10
	.long	2
	.long	60
	.long	52
	.long	44
	.long	36
	.long	28
	.long	20
	.long	12
	.long	4
	.long	62
	.long	54
	.long	46
	.long	38
	.long	30
	.long	22
	.long	14
	.long	6
	.long	64
	.long	56
	.long	48
	.long	40
	.long	32
	.long	24
	.long	16
	.long	8
	.long	57
	.long	49
	.long	41
	.long	33
	.long	25
	.long	17
	.long	9
	.long	1
	.long	59
	.long	51
	.long	43
	.long	35
	.long	27
	.long	19
	.long	11
	.long	3
	.long	61
	.long	53
	.long	45
	.long	37
	.long	29
	.long	21
	.long	13
	.long	5
	.long	63
	.long	55
	.long	47
	.long	39
	.long	31
	.long	23
	.long	15
	.long	7
	.align 32
	.type	sbox, @object
	.size	sbox, 2048
sbox:
	.long	14
	.long	4
	.long	13
	.long	1
	.long	2
	.long	15
	.long	11
	.long	8
	.long	3
	.long	10
	.long	6
	.long	12
	.long	5
	.long	9
	.long	0
	.long	7
	.long	0
	.long	15
	.long	7
	.long	4
	.long	14
	.long	2
	.long	13
	.long	1
	.long	10
	.long	6
	.long	12
	.long	11
	.long	9
	.long	5
	.long	3
	.long	8
	.long	4
	.long	1
	.long	14
	.long	8
	.long	13
	.long	6
	.long	2
	.long	11
	.long	15
	.long	12
	.long	9
	.long	7
	.long	3
	.long	10
	.long	5
	.long	0
	.long	15
	.long	12
	.long	8
	.long	2
	.long	4
	.long	9
	.long	1
	.long	7
	.long	5
	.long	11
	.long	3
	.long	14
	.long	10
	.long	0
	.long	6
	.long	13
	.long	15
	.long	1
	.long	8
	.long	14
	.long	6
	.long	11
	.long	3
	.long	4
	.long	9
	.long	7
	.long	2
	.long	13
	.long	12
	.long	0
	.long	5
	.long	10
	.long	3
	.long	13
	.long	4
	.long	7
	.long	15
	.long	2
	.long	8
	.long	14
	.long	12
	.long	0
	.long	1
	.long	10
	.long	6
	.long	9
	.long	11
	.long	5
	.long	0
	.long	14
	.long	7
	.long	11
	.long	10
	.long	4
	.long	13
	.long	1
	.long	5
	.long	8
	.long	12
	.long	6
	.long	9
	.long	3
	.long	2
	.long	15
	.long	13
	.long	8
	.long	10
	.long	1
	.long	3
	.long	15
	.long	4
	.long	2
	.long	11
	.long	6
	.long	7
	.long	12
	.long	0
	.long	5
	.long	14
	.long	9
	.long	10
	.long	0
	.long	9
	.long	14
	.long	6
	.long	3
	.long	15
	.long	5
	.long	1
	.long	13
	.long	12
	.long	7
	.long	11
	.long	4
	.long	2
	.long	8
	.long	13
	.long	7
	.long	0
	.long	9
	.long	3
	.long	4
	.long	6
	.long	10
	.long	2
	.long	8
	.long	5
	.long	14
	.long	12
	.long	11
	.long	15
	.long	1
	.long	13
	.long	6
	.long	4
	.long	9
	.long	8
	.long	15
	.long	3
	.long	0
	.long	11
	.long	1
	.long	2
	.long	12
	.long	5
	.long	10
	.long	14
	.long	7
	.long	1
	.long	10
	.long	13
	.long	0
	.long	6
	.long	9
	.long	8
	.long	7
	.long	4
	.long	15
	.long	14
	.long	3
	.long	11
	.long	5
	.long	2
	.long	12
	.long	7
	.long	13
	.long	14
	.long	3
	.long	0
	.long	6
	.long	9
	.long	10
	.long	1
	.long	2
	.long	8
	.long	5
	.long	11
	.long	12
	.long	4
	.long	15
	.long	13
	.long	8
	.long	11
	.long	5
	.long	6
	.long	15
	.long	0
	.long	3
	.long	4
	.long	7
	.long	2
	.long	12
	.long	1
	.long	10
	.long	14
	.long	9
	.long	10
	.long	6
	.long	9
	.long	0
	.long	12
	.long	11
	.long	7
	.long	13
	.long	15
	.long	1
	.long	3
	.long	14
	.long	5
	.long	2
	.long	8
	.long	4
	.long	3
	.long	15
	.long	0
	.long	6
	.long	10
	.long	1
	.long	13
	.long	8
	.long	9
	.long	4
	.long	5
	.long	11
	.long	12
	.long	7
	.long	2
	.long	14
	.long	2
	.long	12
	.long	4
	.long	1
	.long	7
	.long	10
	.long	11
	.long	6
	.long	8
	.long	5
	.long	3
	.long	15
	.long	13
	.long	0
	.long	14
	.long	9
	.long	14
	.long	11
	.long	2
	.long	12
	.long	4
	.long	7
	.long	13
	.long	1
	.long	5
	.long	0
	.long	15
	.long	10
	.long	3
	.long	9
	.long	8
	.long	6
	.long	4
	.long	2
	.long	1
	.long	11
	.long	10
	.long	13
	.long	7
	.long	8
	.long	15
	.long	9
	.long	12
	.long	5
	.long	6
	.long	3
	.long	0
	.long	14
	.long	11
	.long	8
	.long	12
	.long	7
	.long	1
	.long	14
	.long	2
	.long	13
	.long	6
	.long	15
	.long	0
	.long	9
	.long	10
	.long	4
	.long	5
	.long	3
	.long	12
	.long	1
	.long	10
	.long	15
	.long	9
	.long	2
	.long	6
	.long	8
	.long	0
	.long	13
	.long	3
	.long	4
	.long	14
	.long	7
	.long	5
	.long	11
	.long	10
	.long	15
	.long	4
	.long	2
	.long	7
	.long	12
	.long	9
	.long	5
	.long	6
	.long	1
	.long	13
	.long	14
	.long	0
	.long	11
	.long	3
	.long	8
	.long	9
	.long	14
	.long	15
	.long	5
	.long	2
	.long	8
	.long	12
	.long	3
	.long	7
	.long	0
	.long	4
	.long	10
	.long	1
	.long	13
	.long	11
	.long	6
	.long	4
	.long	3
	.long	2
	.long	12
	.long	9
	.long	5
	.long	15
	.long	10
	.long	11
	.long	14
	.long	1
	.long	7
	.long	6
	.long	0
	.long	8
	.long	13
	.long	4
	.long	11
	.long	2
	.long	14
	.long	15
	.long	0
	.long	8
	.long	13
	.long	3
	.long	12
	.long	9
	.long	7
	.long	5
	.long	10
	.long	6
	.long	1
	.long	13
	.long	0
	.long	11
	.long	7
	.long	4
	.long	9
	.long	1
	.long	10
	.long	14
	.long	3
	.long	5
	.long	12
	.long	2
	.long	15
	.long	8
	.long	6
	.long	1
	.long	4
	.long	11
	.long	13
	.long	12
	.long	3
	.long	7
	.long	14
	.long	10
	.long	15
	.long	6
	.long	8
	.long	0
	.long	5
	.long	9
	.long	2
	.long	6
	.long	11
	.long	13
	.long	8
	.long	1
	.long	4
	.long	10
	.long	7
	.long	9
	.long	5
	.long	0
	.long	15
	.long	14
	.long	2
	.long	3
	.long	12
	.long	13
	.long	2
	.long	8
	.long	4
	.long	6
	.long	15
	.long	11
	.long	1
	.long	10
	.long	9
	.long	3
	.long	14
	.long	5
	.long	0
	.long	12
	.long	7
	.long	1
	.long	15
	.long	13
	.long	8
	.long	10
	.long	3
	.long	7
	.long	4
	.long	12
	.long	5
	.long	6
	.long	11
	.long	0
	.long	14
	.long	9
	.long	2
	.long	7
	.long	11
	.long	4
	.long	1
	.long	9
	.long	12
	.long	14
	.long	2
	.long	0
	.long	6
	.long	10
	.long	13
	.long	15
	.long	3
	.long	5
	.long	8
	.long	2
	.long	1
	.long	14
	.long	7
	.long	4
	.long	10
	.long	8
	.long	13
	.long	15
	.long	12
	.long	9
	.long	0
	.long	3
	.long	5
	.long	6
	.long	11
	.align 32
	.type	perm32, @object
	.size	perm32, 128
perm32:
	.long	16
	.long	7
	.long	20
	.long	21
	.long	29
	.long	12
	.long	28
	.long	17
	.long	1
	.long	15
	.long	23
	.long	26
	.long	5
	.long	18
	.long	31
	.long	10
	.long	2
	.long	8
	.long	24
	.long	14
	.long	32
	.long	27
	.long	3
	.long	9
	.long	19
	.long	13
	.long	30
	.long	6
	.long	22
	.long	11
	.long	4
	.long	25
	.align 32
	.type	esel, @object
	.size	esel, 192
esel:
	.long	32
	.long	1
	.long	2
	.long	3
	.long	4
	.long	5
	.long	4
	.long	5
	.long	6
	.long	7
	.long	8
	.long	9
	.long	8
	.long	9
	.long	10
	.long	11
	.long	12
	.long	13
	.long	12
	.long	13
	.long	14
	.long	15
	.long	16
	.long	17
	.long	16
	.long	17
	.long	18
	.long	19
	.long	20
	.long	21
	.long	20
	.long	21
	.long	22
	.long	23
	.long	24
	.long	25
	.long	24
	.long	25
	.long	26
	.long	27
	.long	28
	.long	29
	.long	28
	.long	29
	.long	30
	.long	31
	.long	32
	.long	1
	.align 32
	.type	pc2, @object
	.size	pc2, 192
pc2:
	.long	14
	.long	17
	.long	11
	.long	24
	.long	1
	.long	5
	.long	3
	.long	28
	.long	15
	.long	6
	.long	21
	.long	10
	.long	23
	.long	19
	.long	12
	.long	4
	.long	26
	.long	8
	.long	16
	.long	7
	.long	27
	.long	20
	.long	13
	.long	2
	.long	41
	.long	52
	.long	31
	.long	37
	.long	47
	.long	55
	.long	30
	.long	40
	.long	51
	.long	45
	.long	33
	.long	48
	.long	44
	.long	49
	.long	39
	.long	56
	.long	34
	.long	53
	.long	46
	.long	42
	.long	50
	.long	36
	.long	29
	.long	32
	.align 32
	.type	rots, @object
	.size	rots, 64
rots:
	.long	1
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	1
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	2
	.long	1
	.align 32
	.type	pc1, @object
	.size	pc1, 224
pc1:
	.long	57
	.long	49
	.long	41
	.long	33
	.long	25
	.long	17
	.long	9
	.long	1
	.long	58
	.long	50
	.long	42
	.long	34
	.long	26
	.long	18
	.long	10
	.long	2
	.long	59
	.long	51
	.long	43
	.long	35
	.long	27
	.long	19
	.long	11
	.long	3
	.long	60
	.long	52
	.long	44
	.long	36
	.long	63
	.long	55
	.long	47
	.long	39
	.long	31
	.long	23
	.long	15
	.long	7
	.long	62
	.long	54
	.long	46
	.long	38
	.long	30
	.long	22
	.long	14
	.long	6
	.long	61
	.long	53
	.long	45
	.long	37
	.long	29
	.long	21
	.long	13
	.long	5
	.long	28
	.long	20
	.long	12
	.long	4
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
