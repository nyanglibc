	.file	"crypt.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/crypt.shared.v.d -MF /run/asm/crypt/crypt.os.dt -MP
# -MT /run/asm/crypt/.os -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC
# -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h crypt.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/crypt/crypt.shared.v.s -O2 -Wall -Wwrite-strings
# -Wundef -Werror -Wstrict-prototypes -Wold-style-definition -std=gnu11
# -fverbose-asm -fgnu89-inline -fmerge-all-constants -frounding-math
# -fno-stack-protector -fmath-errno -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.globl	_ufc_doit_r
	.type	_ufc_doit_r, @function
_ufc_doit_r:
.LFB0:
	.cfi_startproc
# crypt.c:90:   l = (((long64)res[0]) << 32) | ((long64)res[1]);
	movq	(%rdx), %rcx	# *res_41(D), tmp171
# crypt.c:84: {
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
# crypt.c:93:   while(itr--) {
	subq	$1, %rdi	#, itr
# crypt.c:84: {
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
# crypt.c:91:   r = (((long64)res[2]) << 32) | ((long64)res[3]);
	movq	16(%rdx), %r11	# MEM[(ufc_long *)res_41(D) + 16B], tmp173
# crypt.c:87:   long64 *sb01 = (long64*)__data->sb0;
	leaq	128(%rsi), %r9	#, _147
# crypt.c:90:   l = (((long64)res[0]) << 32) | ((long64)res[1]);
	movq	8(%rdx), %rbx	# MEM[(ufc_long *)res_41(D) + 8B], tmp171
# crypt.c:88:   long64 *sb23 = (long64*)__data->sb2;
	leaq	65664(%rsi), %r10	#, sb23
# crypt.c:90:   l = (((long64)res[0]) << 32) | ((long64)res[1]);
	salq	$32, %rcx	#, tmp171
# crypt.c:91:   r = (((long64)res[2]) << 32) | ((long64)res[3]);
	salq	$32, %r11	#, tmp173
	orq	24(%rdx), %r11	# MEM[(ufc_long *)res_41(D) + 24B], l
# crypt.c:90:   l = (((long64)res[0]) << 32) | ((long64)res[1]);
	orq	%rcx, %rbx	# tmp171, tmp171
# crypt.c:93:   while(itr--) {
	cmpq	$-1, %rdi	#, itr
	je	.L8	#,
	.p2align 4,,10
	.p2align 3
.L4:
# crypt.c:94:     k = (long64*)__data->keysched;
	movq	%r11, %rax	# l, l
	movq	%rsi, %r8	# __data, k
	movq	%rbx, %r11	# r, l
	movq	%rax, %rbx	# l, r
	.p2align 4,,10
	.p2align 3
.L3:
# crypt.c:96:       s = *k++ ^ r;
	movq	(%r8), %rax	# MEM[base: k_83, offset: 0B], s
	addq	$16, %r8	#, k
	xorq	%rbx, %rax	# r, s
# crypt.c:100:       l ^= SBA(sb01, (s >>= 16)         );
	movq	%rax, %rbp	# s, s
# crypt.c:97:       l ^= SBA(sb23, (s       ) & 0xffff);
	movzwl	%ax, %ecx	# s, s
# crypt.c:100:       l ^= SBA(sb01, (s >>= 16)         );
	shrq	$48, %rbp	#, s
	movq	(%r10,%rcx), %rcx	# *_9, *_9
	xorq	(%r9,%rbp), %rcx	# *_15, tmp177
# crypt.c:98:       l ^= SBA(sb23, (s >>= 16) & 0xffff);
	movq	%rax, %rbp	# s, s
# crypt.c:99:       l ^= SBA(sb01, (s >>= 16) & 0xffff);
	shrq	$32, %rax	#, s
# crypt.c:98:       l ^= SBA(sb23, (s >>= 16) & 0xffff);
	shrq	$16, %rbp	#, s
# crypt.c:99:       l ^= SBA(sb01, (s >>= 16) & 0xffff);
	movzwl	%ax, %eax	# s, s
# crypt.c:98:       l ^= SBA(sb23, (s >>= 16) & 0xffff);
	movzwl	%bp, %ebp	# s, s
# crypt.c:100:       l ^= SBA(sb01, (s >>= 16)         );
	xorq	(%r10,%rbp), %rcx	# *_12, tmp181
	xorq	(%r9,%rax), %rcx	# *_14, tmp181
# crypt.c:102:       s = *k++ ^ l;
	movq	-8(%r8), %rax	# MEM[base: _144, offset: -8B], s
# crypt.c:100:       l ^= SBA(sb01, (s >>= 16)         );
	xorq	%rcx, %r11	# tmp184, l
# crypt.c:102:       s = *k++ ^ l;
	xorq	%r11, %rax	# l, s
# crypt.c:106:       r ^= SBA(sb01, (s >>= 16)         );
	movq	%rax, %rbp	# s, s
# crypt.c:103:       r ^= SBA(sb23, (s       ) & 0xffff);
	movzwl	%ax, %ecx	# s, s
# crypt.c:106:       r ^= SBA(sb01, (s >>= 16)         );
	shrq	$48, %rbp	#, s
	movq	(%r10,%rcx), %rcx	# *_18, *_18
	xorq	(%r9,%rbp), %rcx	# *_24, tmp187
	xorq	%rbx, %rcx	# r, tmp189
# crypt.c:104:       r ^= SBA(sb23, (s >>= 16) & 0xffff);
	movq	%rax, %rbx	# s, s
# crypt.c:105:       r ^= SBA(sb01, (s >>= 16) & 0xffff);
	shrq	$32, %rax	#, s
# crypt.c:104:       r ^= SBA(sb23, (s >>= 16) & 0xffff);
	shrq	$16, %rbx	#, s
# crypt.c:105:       r ^= SBA(sb01, (s >>= 16) & 0xffff);
	movzwl	%ax, %eax	# s, s
# crypt.c:104:       r ^= SBA(sb23, (s >>= 16) & 0xffff);
	movzwl	%bx, %ebx	# s, s
	xorq	(%r10,%rbx), %rcx	# *_21, _36
# crypt.c:106:       r ^= SBA(sb01, (s >>= 16)         );
	movq	(%r9,%rax), %rbx	# *_23, _36
	xorq	%rcx, %rbx	# _36, _36
# crypt.c:95:     for(i=8; i--; ) {
	cmpq	%r9, %r8	# _147, k
	jne	.L3	#,
# crypt.c:93:   while(itr--) {
	subq	$1, %rdi	#, itr
	cmpq	$-1, %rdi	#, itr
	jne	.L4	#,
.L8:
# crypt.c:111:   res[0] = l >> 32; res[1] = l & 0xffffffff;
	movq	%rbx, %rax	# r, tmp194
	movl	%ebx, %ecx	# r, tmp195
	shrq	$32, %rax	#, tmp194
	movq	%rcx, 8(%rdx)	# tmp195, MEM[(ufc_long *)res_41(D) + 8B]
	movq	%rax, (%rdx)	# tmp194, *res_41(D)
# crypt.c:112:   res[2] = r >> 32; res[3] = r & 0xffffffff;
	movq	%r11, %rax	# l, tmp196
	andl	$4294967295, %r11d	#, tmp197
	shrq	$32, %rax	#, tmp196
	movq	%r11, 24(%rdx)	# tmp197, MEM[(ufc_long *)res_41(D) + 24B]
# crypt.c:113: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
# crypt.c:112:   res[2] = r >> 32; res[3] = r & 0xffffffff;
	movq	%rax, 16(%rdx)	# tmp196, MEM[(ufc_long *)res_41(D) + 16B]
# crypt.c:113: }
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE0:
	.size	_ufc_doit_r, .-_ufc_doit_r
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
