	.file	"crypt-entry.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/crypt-entry.v.d -MF /run/asm/crypt/crypt-entry.o.dt
# -MP -MT /run/asm/crypt/.o -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt
# -D PIC -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h crypt-entry.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/crypt/crypt-entry.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fpie -ftls-model=initial-exec
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fpic -fpie
# -fplt -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/sys/crypto/fips_enabled"
	.text
	.p2align 4,,15
	.type	fips_enabled_p, @function
fips_enabled_p:
.LFB48:
	.cfi_startproc
# ../sysdeps/unix/sysv/linux/fips-private.h:43:   if (checked == FIPS_UNTESTED)
	movl	checked.8336(%rip), %eax	# checked, checked.0_1
	testl	%eax, %eax	# checked.0_1
	je	.L2	#,
	cmpl	$1, %eax	#, checked.0_1
	sete	%al	#, <retval>
# ../sysdeps/unix/sysv/linux/fips-private.h:72: }
	ret
	.p2align 4,,10
	.p2align 3
.L2:
# ../sysdeps/unix/sysv/linux/fips-private.h:34: {
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
# ../sysdeps/unix/sysv/linux/fips-private.h:45:       int fd = __open_nocancel ("/proc/sys/crypto/fips_enabled", O_RDONLY);
	leaq	.LC0(%rip), %rdi	#,
# ../sysdeps/unix/sysv/linux/fips-private.h:34: {
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
# ../sysdeps/unix/sysv/linux/fips-private.h:45:       int fd = __open_nocancel ("/proc/sys/crypto/fips_enabled", O_RDONLY);
	xorl	%esi, %esi	#
	xorl	%eax, %eax	#
# ../sysdeps/unix/sysv/linux/fips-private.h:34: {
	subq	$48, %rsp	#,
	.cfi_def_cfa_offset 80
# ../sysdeps/unix/sysv/linux/fips-private.h:45:       int fd = __open_nocancel ("/proc/sys/crypto/fips_enabled", O_RDONLY);
	call	__open_nocancel@PLT	#
# ../sysdeps/unix/sysv/linux/fips-private.h:47:       if (fd != -1)
	cmpl	$-1, %eax	#, fd
# ../sysdeps/unix/sysv/linux/fips-private.h:45:       int fd = __open_nocancel ("/proc/sys/crypto/fips_enabled", O_RDONLY);
	movl	%eax, %ebp	#, fd
	leaq	16(%rsp), %r12	#, tmp111
# ../sysdeps/unix/sysv/linux/fips-private.h:47:       if (fd != -1)
	jne	.L4	#,
	jmp	.L27	#
	.p2align 4,,10
	.p2align 3
.L30:
# ../sysdeps/unix/sysv/linux/fips-private.h:52: 	  n = TEMP_FAILURE_RETRY (__read_nocancel (fd, buf, sizeof (buf) - 1));
	movq	errno@gottpoff(%rip), %rax	#, tmp101
	cmpl	$4, %fs:(%rax)	#, errno
	jne	.L29	#,
.L4:
# ../sysdeps/unix/sysv/linux/fips-private.h:52: 	  n = TEMP_FAILURE_RETRY (__read_nocancel (fd, buf, sizeof (buf) - 1));
	movl	$31, %edx	#,
	movq	%r12, %rsi	# tmp111,
	movl	%ebp, %edi	# fd,
	call	__read_nocancel@PLT	#
	cmpq	$-1, %rax	#, __result
	movq	%rax, %rbx	#, __result
	je	.L30	#,
# ../sysdeps/unix/sysv/linux/not-cancel.h:60:   __close_nocancel (fd);
	movl	%ebp, %edi	# fd,
	call	__close_nocancel@PLT	#
# ../sysdeps/unix/sysv/linux/fips-private.h:55: 	  if (n > 0)
	testq	%rbx, %rbx	# __result
	jle	.L27	#,
# ../sysdeps/unix/sysv/linux/fips-private.h:61: 	      long int res = strtol (buf, &endp, 10);
	leaq	8(%rsp), %rsi	#, tmp102
	movl	$10, %edx	#,
	movq	%r12, %rdi	# tmp111,
# ../sysdeps/unix/sysv/linux/fips-private.h:58: 	      buf[n] = '\0';
	movb	$0, 16(%rsp,%rbx)	#, buf
# ../sysdeps/unix/sysv/linux/fips-private.h:61: 	      long int res = strtol (buf, &endp, 10);
	call	strtol@PLT	#
# ../sysdeps/unix/sysv/linux/fips-private.h:62: 	      if (endp != buf && (*endp == '\0' || *endp == '\n'))
	movq	8(%rsp), %rdx	# endp, endp.2_3
	cmpq	%r12, %rdx	# tmp111, endp.2_3
	je	.L27	#,
# ../sysdeps/unix/sysv/linux/fips-private.h:62: 	      if (endp != buf && (*endp == '\0' || *endp == '\n'))
	movzbl	(%rdx), %edx	# *endp.2_3, _24
	testb	%dl, %dl	# _24
	je	.L16	#,
	cmpb	$10, %dl	#, _24
	je	.L16	#,
	.p2align 4,,10
	.p2align 3
.L27:
	movl	checked.8336(%rip), %eax	# checked, prephitmp_42
# ../sysdeps/unix/sysv/linux/fips-private.h:67:       if (checked == FIPS_UNTESTED)
	testl	%eax, %eax	# prephitmp_42
	jne	.L14	#,
.L15:
# ../sysdeps/unix/sysv/linux/fips-private.h:68: 	checked = FIPS_TEST_FAILED;
	movl	$-2, checked.8336(%rip)	#, checked
# ../sysdeps/unix/sysv/linux/fips-private.h:72: }
	addq	$48, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
# ../sysdeps/unix/sysv/linux/fips-private.h:68: 	checked = FIPS_TEST_FAILED;
	xorl	%eax, %eax	# <retval>
# ../sysdeps/unix/sysv/linux/fips-private.h:72: }
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	.cfi_restore_state
# ../sysdeps/unix/sysv/linux/fips-private.h:63: 		checked = (res > 0) ? FIPS_ENABLED : FIPS_DISABLED;
	testq	%rax, %rax	# res
	jle	.L13	#,
	movl	$1, checked.8336(%rip)	#, checked
	movl	$1, %eax	#, prephitmp_42
.L14:
	cmpl	$1, %eax	#, prephitmp_42
	sete	%al	#, <retval>
# ../sysdeps/unix/sysv/linux/fips-private.h:72: }
	addq	$48, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
# ../sysdeps/unix/sysv/linux/not-cancel.h:60:   __close_nocancel (fd);
	movl	%ebp, %edi	# fd,
	call	__close_nocancel@PLT	#
	movl	checked.8336(%rip), %eax	# checked, prephitmp_42
# ../sysdeps/unix/sysv/linux/fips-private.h:67:       if (checked == FIPS_UNTESTED)
	testl	%eax, %eax	# prephitmp_42
	je	.L15	#,
	jmp	.L14	#
.L13:
# ../sysdeps/unix/sysv/linux/fips-private.h:63: 		checked = (res > 0) ? FIPS_ENABLED : FIPS_DISABLED;
	movl	$-1, checked.8336(%rip)	#, checked
	movl	$-1, %eax	#, prephitmp_42
	jmp	.L14	#
	.cfi_endproc
.LFE48:
	.size	fips_enabled_p, .-fips_enabled_p
	.p2align 4,,15
	.globl	__crypt_r
	.type	__crypt_r, @function
__crypt_r:
.LFB49:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r12	# key, key
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
# crypt-entry.c:84:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0)
	leaq	md5_salt_prefix(%rip), %rdi	#,
# crypt-entry.c:77: {
	movq	%rdx, %rbx	# data, data
# crypt-entry.c:84:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0)
	movl	$3, %edx	#,
# crypt-entry.c:77: {
	movq	%rsi, %rbp	# salt, salt
	subq	$56, %rsp	#,
	.cfi_def_cfa_offset 96
# crypt-entry.c:84:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0)
	call	strncmp@PLT	#
	testl	%eax, %eax	# _1
	je	.L42	#,
# crypt-entry.c:97:   if (strncmp (sha256_salt_prefix, salt, sizeof (sha256_salt_prefix) - 1) == 0)
	leaq	sha256_salt_prefix(%rip), %rdi	#,
	movl	$3, %edx	#,
	movq	%rbp, %rsi	# salt,
	call	strncmp@PLT	#
	testl	%eax, %eax	# _3
	je	.L43	#,
# crypt-entry.c:102:   if (strncmp (sha512_salt_prefix, salt, sizeof (sha512_salt_prefix) - 1) == 0)
	leaq	sha512_salt_prefix(%rip), %rdi	#,
	movl	$3, %edx	#,
	movq	%rbp, %rsi	# salt,
	call	strncmp@PLT	#
	testl	%eax, %eax	# _18
	je	.L44	#,
# crypt-entry.c:110:   if (!_ufc_setup_salt_r (salt, data))
	movq	%rbx, %rsi	# data,
	movq	%rbp, %rdi	# salt,
	call	_ufc_setup_salt_r@PLT	#
	testb	%al, %al	# _20
	je	.L45	#,
# crypt-entry.c:117:   if (fips_enabled_p ())
	call	fips_enabled_p	#
	testb	%al, %al	# _21
	jne	.L41	#,
# crypt-entry.c:126:   _ufc_clearmem (ktab, (int) sizeof (ktab));
	leaq	7(%rsp), %r13	#, tmp106
# crypt-entry.c:127:   (void) strncpy (ktab, key, 8);
	movl	$8, %edx	#,
	movq	%r12, %rsi	# key,
# crypt-entry.c:126:   _ufc_clearmem (ktab, (int) sizeof (ktab));
	movq	$0, 7(%rsp)	#, MEM[(void *)&ktab]
	movb	$0, 15(%rsp)	#, MEM[(void *)&ktab]
# crypt-entry.c:133:   _ufc_clearmem ((char*) res, (int) sizeof (res));
	leaq	16(%rsp), %r12	#, tmp109
# crypt-entry.c:127:   (void) strncpy (ktab, key, 8);
	movq	%r13, %rdi	# tmp106,
	call	strncpy@PLT	#
# crypt-entry.c:128:   _ufc_mk_keytab_r (ktab, data);
	movq	%rbx, %rsi	# data,
	movq	%r13, %rdi	# tmp106,
	call	_ufc_mk_keytab_r@PLT	#
# crypt-entry.c:133:   _ufc_clearmem ((char*) res, (int) sizeof (res));
	pxor	%xmm0, %xmm0	# tmp117
# crypt-entry.c:134:   _ufc_doit_r (xx,  data, &res[0]);
	movq	%r12, %rdx	# tmp109,
	movq	%rbx, %rsi	# data,
	movl	$25, %edi	#,
# crypt-entry.c:133:   _ufc_clearmem ((char*) res, (int) sizeof (res));
	movaps	%xmm0, 16(%rsp)	# tmp117, MEM[(void *)&res]
	movaps	%xmm0, 32(%rsp)	# tmp117, MEM[(void *)&res]
# crypt-entry.c:134:   _ufc_doit_r (xx,  data, &res[0]);
	call	_ufc_doit_r@PLT	#
# crypt-entry.c:139:   _ufc_dofinalperm_r (res, data);
	movq	%rbx, %rsi	# data,
	movq	%r12, %rdi	# tmp109,
	call	_ufc_dofinalperm_r@PLT	#
# crypt-entry.c:144:   _ufc_output_conversion_r (res[0], res[1], salt, data);
	movq	24(%rsp), %rsi	# res,
	movq	16(%rsp), %rdi	# res,
	movq	%rbx, %rcx	# data,
	movq	%rbp, %rdx	# salt,
	call	_ufc_output_conversion_r@PLT	#
# crypt-entry.c:150:   explicit_bzero (ktab, sizeof (ktab));
	movq	%r13, %rdi	# tmp106,
	movl	$9, %edx	#,
	movl	$9, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# crypt-entry.c:151:   explicit_bzero (data->keysched, sizeof (data->keysched));
	movq	%rbx, %rdi	# data,
	movq	$-1, %rdx	#,
	movl	$128, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# crypt-entry.c:152:   explicit_bzero (res, sizeof (res));
	movq	%r12, %rdi	# tmp109,
	movl	$32, %edx	#,
	movl	$32, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# crypt-entry.c:155: }
	addq	$56, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
# crypt-entry.c:154:   return data->crypt_3_buf;
	leaq	131200(%rbx), %rax	#, <retval>
# crypt-entry.c:155: }
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	.cfi_restore_state
# crypt-entry.c:87:       if (fips_enabled_p ())
	call	fips_enabled_p	#
	testb	%al, %al	# _2
	jne	.L41	#,
# crypt-entry.c:92:       return __md5_crypt_r (key, salt, (char *) data,
	movl	$131232, %ecx	#,
	movq	%rbx, %rdx	# data,
	movq	%rbp, %rsi	# salt,
	movq	%r12, %rdi	# key,
	call	__md5_crypt_r@PLT	#
.L31:
# crypt-entry.c:155: }
	addq	$56, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	.cfi_restore_state
# crypt-entry.c:103:     return __sha512_crypt_r (key, salt, (char *) data,
	movq	%rbx, %rdx	# data,
	movq	%rbp, %rsi	# salt,
	movq	%r12, %rdi	# key,
	movl	$131232, %ecx	#,
	call	__sha512_crypt_r@PLT	#
# crypt-entry.c:155: }
	addq	$56, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	.cfi_restore_state
# crypt-entry.c:98:     return __sha256_crypt_r (key, salt, (char *) data,
	movq	%rbx, %rdx	# data,
	movq	%rbp, %rsi	# salt,
	movq	%r12, %rdi	# key,
	movl	$131232, %ecx	#,
	call	__sha256_crypt_r@PLT	#
# crypt-entry.c:155: }
	addq	$56, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	.cfi_restore_state
# crypt-entry.c:119:       __set_errno (EPERM);
	movq	errno@gottpoff(%rip), %rax	#, tmp105
	movl	$1, %fs:(%rax)	#, errno
# crypt-entry.c:120:       return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L31	#
	.p2align 4,,10
	.p2align 3
.L45:
# crypt-entry.c:112:       __set_errno (EINVAL);
	movq	errno@gottpoff(%rip), %rax	#, tmp104
	movl	$22, %fs:(%rax)	#, errno
# crypt-entry.c:113:       return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L31	#
	.cfi_endproc
.LFE49:
	.size	__crypt_r, .-__crypt_r
	.weak	crypt_r
	.set	crypt_r,__crypt_r
	.p2align 4,,15
	.globl	crypt
	.type	crypt, @function
crypt:
.LFB50:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbp	# key, key
# crypt-entry.c:163:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0
	leaq	md5_salt_prefix(%rip), %rdi	#,
	movl	$3, %edx	#,
# crypt-entry.c:160: {
	movq	%rsi, %rbx	# salt, salt
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 32
# crypt-entry.c:163:   if (strncmp (md5_salt_prefix, salt, sizeof (md5_salt_prefix) - 1) == 0
	call	strncmp@PLT	#
	testl	%eax, %eax	# _1
	je	.L51	#,
.L47:
# crypt-entry.c:169:   if (strncmp (sha256_salt_prefix, salt, sizeof (sha256_salt_prefix) - 1) == 0)
	leaq	sha256_salt_prefix(%rip), %rdi	#,
	movl	$3, %edx	#,
	movq	%rbx, %rsi	# salt,
	call	strncmp@PLT	#
	testl	%eax, %eax	# _3
	je	.L52	#,
# crypt-entry.c:173:   if (strncmp (sha512_salt_prefix, salt, sizeof (sha512_salt_prefix) - 1) == 0)
	leaq	sha512_salt_prefix(%rip), %rdi	#,
	movl	$3, %edx	#,
	movq	%rbx, %rsi	# salt,
	call	strncmp@PLT	#
	testl	%eax, %eax	# _4
	je	.L53	#,
# crypt-entry.c:178: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# crypt-entry.c:177:   return __crypt_r (key, salt, &_ufc_foobar);
	movq	%rbx, %rsi	# salt,
	movq	%rbp, %rdi	# key,
# crypt-entry.c:178: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# crypt-entry.c:177:   return __crypt_r (key, salt, &_ufc_foobar);
	leaq	_ufc_foobar(%rip), %rdx	#,
	jmp	__crypt_r	#
	.p2align 4,,10
	.p2align 3
.L53:
	.cfi_restore_state
# crypt-entry.c:178: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# crypt-entry.c:174:     return __sha512_crypt (key, salt);
	movq	%rbx, %rsi	# salt,
	movq	%rbp, %rdi	# key,
# crypt-entry.c:178: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# crypt-entry.c:174:     return __sha512_crypt (key, salt);
	jmp	__sha512_crypt@PLT	#
	.p2align 4,,10
	.p2align 3
.L51:
	.cfi_restore_state
# crypt-entry.c:165:       && !fips_enabled_p ())
	call	fips_enabled_p	#
	testb	%al, %al	# _2
	jne	.L47	#,
# crypt-entry.c:178: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# crypt-entry.c:166:     return __md5_crypt (key, salt);
	movq	%rbx, %rsi	# salt,
	movq	%rbp, %rdi	# key,
# crypt-entry.c:178: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# crypt-entry.c:166:     return __md5_crypt (key, salt);
	jmp	__md5_crypt@PLT	#
	.p2align 4,,10
	.p2align 3
.L52:
	.cfi_restore_state
# crypt-entry.c:178: }
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 24
# crypt-entry.c:170:     return __sha256_crypt (key, salt);
	movq	%rbx, %rsi	# salt,
	movq	%rbp, %rdi	# key,
# crypt-entry.c:178: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# crypt-entry.c:170:     return __sha256_crypt (key, salt);
	jmp	__sha256_crypt@PLT	#
	.cfi_endproc
.LFE50:
	.size	crypt, .-crypt
	.local	checked.8336
	.comm	checked.8336,4,4
	.section	.rodata.str1.1
	.type	sha512_salt_prefix, @object
	.size	sha512_salt_prefix, 4
sha512_salt_prefix:
	.string	"$6$"
	.type	sha256_salt_prefix, @object
	.size	sha256_salt_prefix, 4
sha256_salt_prefix:
	.string	"$5$"
	.type	md5_salt_prefix, @object
	.size	md5_salt_prefix, 4
md5_salt_prefix:
	.string	"$1$"
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
