	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/sys/crypto/fips_enabled"
	.text
	.p2align 4,,15
	.type	fips_enabled_p, @function
fips_enabled_p:
	movl	checked.8336(%rip), %eax
	testl	%eax, %eax
	je	.L2
	cmpl	$1, %eax
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	leaq	.LC0(%rip), %rdi
	pushq	%rbx
	xorl	%esi, %esi
	xorl	%eax, %eax
	subq	$48, %rsp
	call	__open_nocancel@PLT
	cmpl	$-1, %eax
	movl	%eax, %ebp
	leaq	16(%rsp), %r12
	jne	.L4
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L30:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L29
.L4:
	movl	$31, %edx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	__read_nocancel@PLT
	cmpq	$-1, %rax
	movq	%rax, %rbx
	je	.L30
	movl	%ebp, %edi
	call	__close_nocancel@PLT
	testq	%rbx, %rbx
	jle	.L27
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	movq	%r12, %rdi
	movb	$0, 16(%rsp,%rbx)
	call	strtol@PLT
	movq	8(%rsp), %rdx
	cmpq	%r12, %rdx
	je	.L27
	movzbl	(%rdx), %edx
	testb	%dl, %dl
	je	.L16
	cmpb	$10, %dl
	je	.L16
	.p2align 4,,10
	.p2align 3
.L27:
	movl	checked.8336(%rip), %eax
	testl	%eax, %eax
	jne	.L14
.L15:
	movl	$-2, checked.8336(%rip)
	addq	$48, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	testq	%rax, %rax
	jle	.L13
	movl	$1, checked.8336(%rip)
	movl	$1, %eax
.L14:
	cmpl	$1, %eax
	sete	%al
	addq	$48, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%ebp, %edi
	call	__close_nocancel@PLT
	movl	checked.8336(%rip), %eax
	testl	%eax, %eax
	je	.L15
	jmp	.L14
.L13:
	movl	$-1, checked.8336(%rip)
	movl	$-1, %eax
	jmp	.L14
	.size	fips_enabled_p, .-fips_enabled_p
	.p2align 4,,15
	.globl	__crypt_r
	.type	__crypt_r, @function
__crypt_r:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	md5_salt_prefix(%rip), %rdi
	movq	%rdx, %rbx
	movl	$3, %edx
	movq	%rsi, %rbp
	subq	$56, %rsp
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L42
	leaq	sha256_salt_prefix(%rip), %rdi
	movl	$3, %edx
	movq	%rbp, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L43
	leaq	sha512_salt_prefix(%rip), %rdi
	movl	$3, %edx
	movq	%rbp, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L44
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	_ufc_setup_salt_r@PLT
	testb	%al, %al
	je	.L45
	call	fips_enabled_p
	testb	%al, %al
	jne	.L41
	leaq	7(%rsp), %r13
	movl	$8, %edx
	movq	%r12, %rsi
	movq	$0, 7(%rsp)
	movb	$0, 15(%rsp)
	leaq	16(%rsp), %r12
	movq	%r13, %rdi
	call	strncpy@PLT
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_ufc_mk_keytab_r@PLT
	pxor	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	$25, %edi
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	call	_ufc_doit_r@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_ufc_dofinalperm_r@PLT
	movq	24(%rsp), %rsi
	movq	16(%rsp), %rdi
	movq	%rbx, %rcx
	movq	%rbp, %rdx
	call	_ufc_output_conversion_r@PLT
	movq	%r13, %rdi
	movl	$9, %edx
	movl	$9, %esi
	call	__explicit_bzero_chk@PLT
	movq	%rbx, %rdi
	movq	$-1, %rdx
	movl	$128, %esi
	call	__explicit_bzero_chk@PLT
	movq	%r12, %rdi
	movl	$32, %edx
	movl	$32, %esi
	call	__explicit_bzero_chk@PLT
	addq	$56, %rsp
	leaq	131200(%rbx), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	call	fips_enabled_p
	testb	%al, %al
	jne	.L41
	movl	$131232, %ecx
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__md5_crypt_r@PLT
.L31:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	movl	$131232, %ecx
	call	__sha512_crypt_r@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	movl	$131232, %ecx
	call	__sha256_crypt_r@PLT
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movq	errno@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L45:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L31
	.size	__crypt_r, .-__crypt_r
	.weak	crypt_r
	.set	crypt_r,__crypt_r
	.p2align 4,,15
	.globl	crypt
	.type	crypt, @function
crypt:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	leaq	md5_salt_prefix(%rip), %rdi
	movl	$3, %edx
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L51
.L47:
	leaq	sha256_salt_prefix(%rip), %rdi
	movl	$3, %edx
	movq	%rbx, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L52
	leaq	sha512_salt_prefix(%rip), %rdi
	movl	$3, %edx
	movq	%rbx, %rsi
	call	strncmp@PLT
	testl	%eax, %eax
	je	.L53
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	leaq	_ufc_foobar(%rip), %rdx
	jmp	__crypt_r
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__sha512_crypt@PLT
	.p2align 4,,10
	.p2align 3
.L51:
	call	fips_enabled_p
	testb	%al, %al
	jne	.L47
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__md5_crypt@PLT
	.p2align 4,,10
	.p2align 3
.L52:
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__sha256_crypt@PLT
	.size	crypt, .-crypt
	.local	checked.8336
	.comm	checked.8336,4,4
	.section	.rodata.str1.1
	.type	sha512_salt_prefix, @object
	.size	sha512_salt_prefix, 4
sha512_salt_prefix:
	.string	"$6$"
	.type	sha256_salt_prefix, @object
	.size	sha256_salt_prefix, 4
sha256_salt_prefix:
	.string	"$5$"
	.type	md5_salt_prefix, @object
	.size	md5_salt_prefix, 4
md5_salt_prefix:
	.string	"$1$"
