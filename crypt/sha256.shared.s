	.text
	.p2align 4,,15
	.globl	__sha256_init_ctx
	.type	__sha256_init_ctx, @function
__sha256_init_ctx:
	movabsq	$-4942790177982912921, %rax
	movq	$0, 32(%rdi)
	movl	$0, 40(%rdi)
	movq	%rax, (%rdi)
	movabsq	$-6534734903820487822, %rax
	movq	%rax, 8(%rdi)
	movabsq	$-7276294671082564993, %rax
	movq	%rax, 16(%rdi)
	movabsq	$6620516960021240235, %rax
	movq	%rax, 24(%rdi)
	ret
	.size	__sha256_init_ctx, .-__sha256_init_ctx
	.p2align 4,,15
	.globl	__sha256_process_block
	.type	__sha256_process_block, @function
__sha256_process_block:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	shrq	$2, %rbx
	subq	$208, %rsp
	addq	%rsi, 32(%rdx)
	testq	%rbx, %rbx
	movq	%rdi, -88(%rsp)
	movl	(%rdx), %edi
	movq	%rdx, -64(%rsp)
	movq	%rbx, -80(%rsp)
	movl	%edi, -108(%rsp)
	movl	4(%rdx), %edi
	movl	%edi, -104(%rsp)
	movl	8(%rdx), %edi
	movl	%edi, -100(%rsp)
	movl	12(%rdx), %edi
	movl	%edi, -112(%rsp)
	movl	16(%rdx), %edi
	movl	%edi, -96(%rsp)
	movl	20(%rdx), %edi
	movl	%edi, -116(%rsp)
	movl	24(%rdx), %edi
	movl	%edi, -92(%rsp)
	movl	28(%rdx), %edi
	movl	%edi, -120(%rsp)
	je	.L4
	leaq	-56(%rsp), %r14
	leaq	K(%rip), %r15
	leaq	192(%r14), %rax
	movq	%rax, -72(%rsp)
	.p2align 4,,10
	.p2align 3
.L9:
	movq	-88(%rsp), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L5:
	movl	(%rcx,%rax), %edx
	bswap	%edx
	movl	%edx, (%r14,%rax)
	addq	$4, %rax
	cmpq	$64, %rax
	jne	.L5
	addq	$64, -88(%rsp)
	movq	-72(%rsp), %rdi
	movq	%r14, %rsi
	.p2align 4,,10
	.p2align 3
.L6:
	movl	56(%rsi), %eax
	movl	4(%rsi), %ecx
	addq	$4, %rsi
	movl	%eax, %edx
	movl	%eax, %r8d
	shrl	$10, %eax
	roll	$13, %r8d
	roll	$15, %edx
	xorl	%r8d, %edx
	movl	%ecx, %r8d
	xorl	%eax, %edx
	movl	-4(%rsi), %eax
	addl	32(%rsi), %eax
	roll	$14, %r8d
	addl	%eax, %edx
	movl	%ecx, %eax
	shrl	$3, %ecx
	rorl	$7, %eax
	xorl	%r8d, %eax
	xorl	%ecx, %eax
	addl	%edx, %eax
	movl	%eax, 60(%rsi)
	cmpq	%rsi, %rdi
	jne	.L6
	movl	-120(%rsp), %eax
	movl	-92(%rsp), %ebx
	xorl	%r9d, %r9d
	movl	-116(%rsp), %ebp
	movl	-96(%rsp), %edi
	movl	$1116352408, %r12d
	movl	-112(%rsp), %r13d
	movl	-100(%rsp), %r10d
	movl	-104(%rsp), %r11d
	movl	-108(%rsp), %r8d
	movl	%eax, %esi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L17:
	movl	(%r15,%r9), %r12d
	movl	%r10d, %r13d
	movl	%ebx, %esi
	movl	%r11d, %r10d
	movl	%ebp, %ebx
	movl	%r8d, %r11d
	movl	%edi, %ebp
	movl	%eax, %r8d
	movl	%ecx, %edi
.L8:
	movl	%edi, %eax
	movl	%edi, %edx
	movl	%edi, %ecx
	rorl	$11, %edx
	rorl	$6, %eax
	andl	%ebp, %ecx
	xorl	%edx, %eax
	movl	%edi, %edx
	roll	$7, %edx
	xorl	%eax, %edx
	movl	%edi, %eax
	notl	%eax
	andl	%ebx, %eax
	xorl	%ecx, %eax
	movl	%r8d, %ecx
	addl	%edx, %eax
	movl	%r8d, %edx
	addl	(%r14,%r9), %eax
	rorl	$13, %ecx
	rorl	$2, %edx
	addq	$4, %r9
	xorl	%ecx, %edx
	movl	%r8d, %ecx
	roll	$10, %ecx
	xorl	%edx, %ecx
	movl	%r11d, %edx
	addl	%esi, %eax
	xorl	%r10d, %edx
	movl	%r11d, %esi
	addl	%r12d, %eax
	andl	%r8d, %edx
	andl	%r10d, %esi
	xorl	%esi, %edx
	addl	%ecx, %edx
	leal	(%rax,%r13), %ecx
	addl	%edx, %eax
	cmpq	$256, %r9
	jne	.L17
	addl	%eax, -108(%rsp)
	addl	%r8d, -104(%rsp)
	addl	%r11d, -100(%rsp)
	addl	%r10d, -112(%rsp)
	addl	%ecx, -96(%rsp)
	addl	%edi, -116(%rsp)
	addl	%ebp, -92(%rsp)
	addl	%ebx, -120(%rsp)
	subq	$16, -80(%rsp)
	jne	.L9
.L4:
	movq	-64(%rsp), %rax
	movl	-108(%rsp), %ebx
	movl	%ebx, (%rax)
	movl	-104(%rsp), %ebx
	movl	%ebx, 4(%rax)
	movl	-100(%rsp), %ebx
	movl	%ebx, 8(%rax)
	movl	-112(%rsp), %ebx
	movl	%ebx, 12(%rax)
	movl	-96(%rsp), %ebx
	movl	%ebx, 16(%rax)
	movl	-116(%rsp), %ebx
	movl	%ebx, 20(%rax)
	movl	-92(%rsp), %ebx
	movl	%ebx, 24(%rax)
	movl	-120(%rsp), %ebx
	movl	%ebx, 28(%rax)
	addq	$208, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__sha256_process_block, .-__sha256_process_block
	.p2align 4,,15
	.globl	__sha256_finish_ctx
	.type	__sha256_finish_ctx, @function
__sha256_finish_ctx:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	40(%rdi), %r12d
	addq	%r12, 32(%rdi)
	cmpl	$55, %r12d
	jbe	.L19
	movl	$120, %r13d
	subl	%r12d, %r13d
.L20:
	leaq	48(%rbx,%r12), %rdi
	leaq	fillbuf(%rip), %rsi
	movq	%r13, %rdx
	addq	%r13, %r12
	call	memcpy@PLT
	movq	32(%rbx), %rax
	movq	%r12, %rdx
	leaq	8(%r12), %rsi
	shrq	$3, %rdx
	leaq	48(%rbx), %rdi
	salq	$3, %rax
	bswap	%rax
	movq	%rax, 48(%rbx,%rdx,8)
	movq	%rbx, %rdx
	call	__sha256_process_block@PLT
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L21:
	movl	(%rbx,%rax), %edx
	bswap	%edx
	movl	%edx, 0(%rbp,%rax)
	addq	$4, %rax
	cmpq	$32, %rax
	jne	.L21
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$56, %r13d
	subl	%r12d, %r13d
	jmp	.L20
	.size	__sha256_finish_ctx, .-__sha256_finish_ctx
	.p2align 4,,15
	.globl	__sha256_process_bytes
	.type	__sha256_process_bytes, @function
__sha256_process_bytes:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	40(%rdx), %eax
	testl	%eax, %eax
	jne	.L63
.L25:
	cmpq	$63, %rbx
	ja	.L64
.L33:
	testq	%rbx, %rbx
	jne	.L65
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movl	40(%rbp), %esi
	cmpl	$8, %ebx
	movl	%ebx, %eax
	leaq	48(%rbp,%rsi), %rcx
	jnb	.L35
	testb	$4, %bl
	jne	.L66
	testl	%ebx, %ebx
	je	.L36
	movzbl	(%r12), %edx
	testb	$2, %al
	movb	%dl, (%rcx)
	jne	.L67
.L36:
	addq	%rsi, %rbx
	cmpq	$63, %rbx
	jbe	.L41
	leaq	48(%rbp), %r12
	movq	%rbp, %rdx
	movl	$64, %esi
	subq	$64, %rbx
	movq	%r12, %rdi
	call	__sha256_process_block@PLT
	leaq	112(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
.L41:
	movl	%ebx, 40(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rbx, %r13
	movq	%r12, %rdi
	movq	%rbp, %rdx
	andq	$-64, %r13
	andl	$63, %ebx
	movq	%r13, %rsi
	addq	%r13, %r12
	call	__sha256_process_block@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L63:
	movl	%eax, %r13d
	movl	$128, %edx
	subq	%r13, %rdx
	leaq	48(%rbp,%r13), %rdi
	cmpq	%rsi, %rdx
	cmova	%rsi, %rdx
	movq	%r12, %rsi
	movq	%rdx, %r14
	call	memcpy@PLT
	movl	40(%rbp), %esi
	addl	%r14d, %esi
	cmpl	$64, %esi
	movl	%esi, 40(%rbp)
	ja	.L68
.L26:
	addq	%r14, %r12
	subq	%r14, %rbx
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L35:
	movq	(%r12), %rax
	movq	%rax, (%rcx)
	movl	%ebx, %eax
	movq	-8(%r12,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	leaq	8(%rcx), %rdx
	andq	$-8, %rdx
	subq	%rdx, %rcx
	leal	(%rbx,%rcx), %eax
	subq	%rcx, %r12
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L36
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L39:
	movl	%ecx, %edi
	addl	$8, %ecx
	movq	(%r12,%rdi), %r8
	cmpl	%eax, %ecx
	movq	%r8, (%rdx,%rdi)
	jb	.L39
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L68:
	leaq	48(%rbp), %r15
	andl	$-64, %esi
	movq	%rbp, %rdx
	movq	%r15, %rdi
	call	__sha256_process_block@PLT
	movl	40(%rbp), %ecx
	leaq	0(%r13,%r14), %rax
	andq	$-64, %rax
	movl	%ecx, %edx
	leaq	48(%rbp,%rax), %rax
	andl	$63, %edx
	cmpl	$8, %edx
	movl	%edx, 40(%rbp)
	jnb	.L27
	testb	$4, %cl
	jne	.L69
	testl	%edx, %edx
	je	.L26
	movzbl	(%rax), %esi
	andl	$2, %ecx
	movb	%sil, 48(%rbp)
	je	.L26
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%r15,%rdx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rax), %rcx
	movq	%rcx, 48(%rbp)
	movl	%edx, %ecx
	movq	-8(%rax,%rcx), %rsi
	movq	%rsi, -8(%r15,%rcx)
	leaq	56(%rbp), %rcx
	andq	$-8, %rcx
	subq	%rcx, %r15
	addl	%r15d, %edx
	subq	%r15, %rax
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L26
	andl	$-8, %edx
	xorl	%esi, %esi
.L31:
	movl	%esi, %edi
	addl	$8, %esi
	movq	(%rax,%rdi), %r8
	cmpl	%edx, %esi
	movq	%r8, (%rcx,%rdi)
	jb	.L31
	jmp	.L26
.L69:
	movl	(%rax), %ecx
	movl	%ecx, 48(%rbp)
	movl	-4(%rax,%rdx), %eax
	movl	%eax, -4(%r15,%rdx)
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L66:
	movl	(%r12), %edx
	movl	%edx, (%rcx)
	movl	%ebx, %edx
	movl	-4(%r12,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L36
.L67:
	movl	%ebx, %edx
	movzwl	-2(%r12,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L36
	.size	__sha256_process_bytes, .-__sha256_process_bytes
	.section	.rodata
	.align 32
	.type	K, @object
	.size	K, 256
K:
	.long	1116352408
	.long	1899447441
	.long	-1245643825
	.long	-373957723
	.long	961987163
	.long	1508970993
	.long	-1841331548
	.long	-1424204075
	.long	-670586216
	.long	310598401
	.long	607225278
	.long	1426881987
	.long	1925078388
	.long	-2132889090
	.long	-1680079193
	.long	-1046744716
	.long	-459576895
	.long	-272742522
	.long	264347078
	.long	604807628
	.long	770255983
	.long	1249150122
	.long	1555081692
	.long	1996064986
	.long	-1740746414
	.long	-1473132947
	.long	-1341970488
	.long	-1084653625
	.long	-958395405
	.long	-710438585
	.long	113926993
	.long	338241895
	.long	666307205
	.long	773529912
	.long	1294757372
	.long	1396182291
	.long	1695183700
	.long	1986661051
	.long	-2117940946
	.long	-1838011259
	.long	-1564481375
	.long	-1474664885
	.long	-1035236496
	.long	-949202525
	.long	-778901479
	.long	-694614492
	.long	-200395387
	.long	275423344
	.long	430227734
	.long	506948616
	.long	659060556
	.long	883997877
	.long	958139571
	.long	1322822218
	.long	1537002063
	.long	1747873779
	.long	1955562222
	.long	2024104815
	.long	-2067236844
	.long	-1933114872
	.long	-1866530822
	.long	-1538233109
	.long	-1090935817
	.long	-965641998
	.align 32
	.type	fillbuf, @object
	.size	fillbuf, 64
fillbuf:
	.byte	-128
	.byte	0
	.zero	62
