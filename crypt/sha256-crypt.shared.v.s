	.file	"sha256-crypt.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/sha256-crypt.shared.v.d
# -MF /run/asm/crypt/sha256-crypt.os.dt -MP -MT /run/asm/crypt/.os
# -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC -D SHARED
# -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h sha256-crypt.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/crypt/sha256-crypt.shared.v.s -O2
# -Wall -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"$"
.LC1:
	.string	"%s%zu$"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__sha256_crypt_r
	.type	__sha256_crypt_r, @function
__sha256_crypt_r:
.LFB41:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%r15	#
	pushq	%r14	#
	pushq	%r13	#
	pushq	%r12	#
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rdi, %r12	# key, key
	pushq	%rbx	#
# sha256-crypt.c:125:   if (strncmp (sha256_salt_prefix, salt, sizeof (sha256_salt_prefix) - 1) == 0)
	leaq	sha256_salt_prefix(%rip), %rdi	#,
	.cfi_offset 3, -56
# sha256-crypt.c:103: {
	movq	%rsi, %rbx	# salt, salt
	subq	$552, %rsp	#,
# sha256-crypt.c:103: {
	movq	%rdx, -560(%rbp)	# buffer, %sfp
# sha256-crypt.c:125:   if (strncmp (sha256_salt_prefix, salt, sizeof (sha256_salt_prefix) - 1) == 0)
	movl	$3, %edx	#,
# sha256-crypt.c:103: {
	movl	%ecx, -484(%rbp)	# buflen, buflen
# sha256-crypt.c:125:   if (strncmp (sha256_salt_prefix, salt, sizeof (sha256_salt_prefix) - 1) == 0)
	call	strncmp@PLT	#
# sha256-crypt.c:127:     salt += sizeof (sha256_salt_prefix) - 1;
	leaq	3(%rbx), %rdx	#, tmp628
	testl	%eax, %eax	# _1
# sha256-crypt.c:129:   if (strncmp (salt, sha256_rounds_prefix, sizeof (sha256_rounds_prefix) - 1)
	leaq	sha256_rounds_prefix(%rip), %rsi	#,
# sha256-crypt.c:127:     salt += sizeof (sha256_salt_prefix) - 1;
	cmovne	%rbx, %rdx	# tmp628,, salt, tmp628
	movq	%rdx, %rdi	# tmp628, salt
	movq	%rdx, -512(%rbp)	# salt, %sfp
# sha256-crypt.c:129:   if (strncmp (salt, sha256_rounds_prefix, sizeof (sha256_rounds_prefix) - 1)
	movl	$7, %edx	#,
	call	strncmp@PLT	#
	testl	%eax, %eax	# _2
# sha256-crypt.c:118:   bool rounds_custom = false;
	movb	$0, -485(%rbp)	#, %sfp
# sha256-crypt.c:117:   size_t rounds = ROUNDS_DEFAULT;
	movq	$5000, -528(%rbp)	#, %sfp
# sha256-crypt.c:129:   if (strncmp (salt, sha256_rounds_prefix, sizeof (sha256_rounds_prefix) - 1)
	je	.L113	#,
.L3:
# sha256-crypt.c:143:   salt_len = MIN (strcspn (salt, "$"), SALT_LEN_MAX);
	movq	-512(%rbp), %rdi	# %sfp,
	leaq	.LC0(%rip), %rsi	#,
	call	strcspn@PLT	#
# sha256-crypt.c:144:   key_len = strlen (key);
	movq	%r12, %rdi	# key,
# sha256-crypt.c:143:   salt_len = MIN (strcspn (salt, "$"), SALT_LEN_MAX);
	cmpq	$16, %rax	#, _6
	movq	%rax, %r14	#, _6
	movl	$16, %eax	#, tmp630
	cmovnb	%rax, %r14	# _6,, tmp630, _6
# sha256-crypt.c:144:   key_len = strlen (key);
	call	strlen@PLT	#
# sha256-crypt.c:146:   if ((key - (char *) 0) % __alignof__ (uint32_t) != 0)
	testb	$3, %r12b	#, key
# sha256-crypt.c:144:   key_len = strlen (key);
	movq	%rax, %r15	#, tmp286
# sha256-crypt.c:146:   if ((key - (char *) 0) % __alignof__ (uint32_t) != 0)
	je	.L60	#,
# sha256-crypt.c:150:       if (__libc_use_alloca (alloca_used + key_len + __alignof__ (uint32_t)))
	leaq	4(%rax), %rbx	#, _9
# ../sysdeps/pthread/allocalim.h:27:   return (__glibc_likely (__libc_alloca_cutoff (size))
	movq	%rbx, %rdi	# _9,
	call	__libc_alloca_cutoff@PLT	#
# ../sysdeps/pthread/allocalim.h:29:           || __glibc_likely (size <= PTHREAD_STACK_MIN / 4)
	testl	%eax, %eax	# _305
	jne	.L7	#,
	cmpq	$4096, %rbx	#, _9
	ja	.L114	#,
.L7:
# sha256-crypt.c:151: 	tmp = alloca_account (key_len + __alignof__ (uint32_t), alloca_used);
#APP
# 151 "sha256-crypt.c" 1
	mov %rsp, %rax	# p__
# 0 "" 2
#NO_APP
	addq	$30, %rbx	#, tmp297
	andq	$-16, %rbx	#, tmp301
	subq	%rbx, %rsp	# tmp301,
	leaq	15(%rsp), %rdi	#, tmp303
	andq	$-16, %rdi	#, m__
#APP
# 151 "sha256-crypt.c" 1
	sub %rsp , %rax	# d__
# 0 "" 2
#NO_APP
	movq	%rax, -496(%rbp)	# d__, %sfp
# sha256-crypt.c:120:   char *free_key = NULL;
	movq	$0, -576(%rbp)	#, %sfp
.L9:
# sha256-crypt.c:159:       key = copied_key =
	movq	%r12, %rsi	# key,
# sha256-crypt.c:161: 		- (tmp - (char *) 0) % __alignof__ (uint32_t),
	addq	$4, %rdi	#, tmp307
# sha256-crypt.c:159:       key = copied_key =
	movq	%r15, %rdx	# tmp286,
	call	memcpy@PLT	#
	movq	%rax, %r12	#, key
	movq	%rax, -584(%rbp)	# key, %sfp
	jmp	.L6	#
	.p2align 4,,10
	.p2align 3
.L60:
# sha256-crypt.c:120:   char *free_key = NULL;
	movq	$0, -576(%rbp)	#, %sfp
# sha256-crypt.c:119:   size_t alloca_used = 0;
	movq	$0, -496(%rbp)	#, %sfp
# sha256-crypt.c:112:   char *copied_key = NULL;
	movq	$0, -584(%rbp)	#, %sfp
.L6:
# sha256-crypt.c:166:   if ((salt - (char *) 0) % __alignof__ (uint32_t) != 0)
	testb	$3, -512(%rbp)	#, %sfp
# sha256-crypt.c:113:   char *copied_salt = NULL;
	movq	$0, -568(%rbp)	#, %sfp
# sha256-crypt.c:166:   if ((salt - (char *) 0) % __alignof__ (uint32_t) != 0)
	jne	.L115	#,
.L10:
# sha256-crypt.c:193:   sha256_init_ctx (&ctx, nss_ctx);
	leaq	-400(%rbp), %rbx	#, tmp624
# sha256-crypt.c:205:   sha256_init_ctx (&alt_ctx, nss_alt_ctx);
	leaq	-224(%rbp), %r13	#, tmp625
# sha256-crypt.c:193:   sha256_init_ctx (&ctx, nss_ctx);
	movq	%rbx, %rdi	# tmp624,
	call	__sha256_init_ctx@PLT	#
# sha256-crypt.c:196:   sha256_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	%rbx, %rdx	# tmp624,
	movq	%r15, %rsi	# tmp286,
	movq	%r12, %rdi	# key,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:200:   sha256_process_bytes (salt, salt_len, &ctx, nss_ctx);
	movq	-512(%rbp), %rdi	# %sfp,
	movq	%rbx, %rdx	# tmp624,
	movq	%r14, %rsi	# _6,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:205:   sha256_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	%r13, %rdi	# tmp625,
	movq	%r13, -520(%rbp)	# tmp625, %sfp
	call	__sha256_init_ctx@PLT	#
# sha256-crypt.c:208:   sha256_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%r13, %rdx	# tmp625,
	movq	%r15, %rsi	# tmp286,
	movq	%r12, %rdi	# key,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:211:   sha256_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	movq	-512(%rbp), %rdi	# %sfp,
	movq	%r13, %rdx	# tmp625,
	movq	%r14, %rsi	# _6,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:214:   sha256_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%r13, %rdx	# tmp625,
	movq	%r15, %rsi	# tmp286,
	movq	%r12, %rdi	# key,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:218:   sha256_finish_ctx (&alt_ctx, nss_alt_ctx, alt_result);
	leaq	-464(%rbp), %rax	#, tmp626
	movq	%r13, %rdi	# tmp625,
	movq	%rax, %rsi	# tmp626,
	movq	%rax, -504(%rbp)	# tmp626, %sfp
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:221:   for (cnt = key_len; cnt > 32; cnt -= 32)
	cmpq	$32, %r15	#, tmp286
	jbe	.L17	#,
	leaq	-33(%r15), %rcx	#, _15
	leaq	-32(%r15), %rsi	#, _319
	movq	%r14, -552(%rbp)	# _6, %sfp
	movq	-504(%rbp), %r14	# %sfp, tmp626
	movq	%r12, -592(%rbp)	# key, %sfp
	movq	%r15, %r12	# cnt, cnt
	movq	%rcx, -536(%rbp)	# _15, %sfp
	andq	$-32, %rcx	#, tmp370
	movq	%rsi, -544(%rbp)	# _319, %sfp
	movq	%rcx, %rax	# tmp370, tmp370
	movq	%rsi, %rcx	# _319, _331
	subq	%rax, %rcx	# tmp370, _331
	movq	%rcx, %r13	# _331, _331
	.p2align 4,,10
	.p2align 3
.L18:
# sha256-crypt.c:222:     sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	movq	%rbx, %rdx	# tmp624,
	movl	$32, %esi	#,
	movq	%r14, %rdi	# tmp626,
# sha256-crypt.c:221:   for (cnt = key_len; cnt > 32; cnt -= 32)
	subq	$32, %r12	#, cnt
# sha256-crypt.c:222:     sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:221:   for (cnt = key_len; cnt > 32; cnt -= 32)
	cmpq	%r13, %r12	# _331, cnt
	jne	.L18	#,
# sha256-crypt.c:223:   sha256_process_bytes (alt_result, cnt, &ctx, nss_ctx);
	movq	-536(%rbp), %rax	# %sfp, _15
	movq	-544(%rbp), %rsi	# %sfp, _319
	movq	%rbx, %rdx	# tmp624,
	movq	-504(%rbp), %rdi	# %sfp,
	movq	-552(%rbp), %r14	# %sfp, _6
	movq	-592(%rbp), %r12	# %sfp, key
	andq	$-32, %rax	#, _15
	subq	%rax, %rsi	# tmp376, _319
	call	__sha256_process_bytes@PLT	#
.L19:
# sha256-crypt.c:221:   for (cnt = key_len; cnt > 32; cnt -= 32)
	movq	%r15, %r13	# tmp286, cnt
	jmp	.L23	#
	.p2align 4,,10
	.p2align 3
.L117:
# sha256-crypt.c:229:       sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	movq	-504(%rbp), %rdi	# %sfp,
	movl	$32, %esi	#,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:227:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	shrq	%r13	# cnt
	je	.L116	#,
.L23:
# sha256-crypt.c:228:     if ((cnt & 1) != 0)
	testb	$1, %r13b	#, cnt
# sha256-crypt.c:229:       sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	movq	%rbx, %rdx	# tmp624,
# sha256-crypt.c:228:     if ((cnt & 1) != 0)
	jne	.L117	#,
# sha256-crypt.c:231:       sha256_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	%r15, %rsi	# tmp286,
	movq	%r12, %rdi	# key,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:227:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	shrq	%r13	# cnt
	jne	.L23	#,
.L116:
# sha256-crypt.c:234:   sha256_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	-504(%rbp), %rsi	# %sfp,
	movq	%rbx, %rdi	# tmp624,
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:237:   sha256_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	-520(%rbp), %rdi	# %sfp,
	call	__sha256_init_ctx@PLT	#
	movq	%rbx, -536(%rbp)	# tmp624, %sfp
	movq	%r13, %rbx	# cnt, cnt
	movq	-520(%rbp), %r13	# %sfp, tmp625
	.p2align 4,,10
	.p2align 3
.L25:
# sha256-crypt.c:241:     sha256_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%r13, %rdx	# tmp625,
	movq	%r15, %rsi	# tmp286,
	movq	%r12, %rdi	# key,
# sha256-crypt.c:240:   for (cnt = 0; cnt < key_len; ++cnt)
	addq	$1, %rbx	#, cnt
# sha256-crypt.c:241:     sha256_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:240:   for (cnt = 0; cnt < key_len; ++cnt)
	cmpq	%rbx, %r15	# cnt, tmp286
	jne	.L25	#,
	movq	-536(%rbp), %rbx	# %sfp, tmp624
.L57:
# sha256-crypt.c:244:   sha256_finish_ctx (&alt_ctx, nss_alt_ctx, temp_result);
	leaq	-432(%rbp), %rax	#, tmp627
	movq	-520(%rbp), %rdi	# %sfp,
	movq	%rax, %rsi	# tmp627,
	movq	%rax, -544(%rbp)	# tmp627, %sfp
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:247:   if (__libc_use_alloca (alloca_used + key_len))
	movq	-496(%rbp), %r12	# %sfp, alloca_used
	addq	%r15, %r12	# tmp286, alloca_used
# ../sysdeps/pthread/allocalim.h:27:   return (__glibc_likely (__libc_alloca_cutoff (size))
	movq	%r12, %rdi	# _18,
	call	__libc_alloca_cutoff@PLT	#
# ../sysdeps/pthread/allocalim.h:29:           || __glibc_likely (size <= PTHREAD_STACK_MIN / 4)
	cmpq	$4096, %r12	#, _18
	jbe	.L26	#,
	testl	%eax, %eax	# _311
	je	.L118	#,
.L26:
# sha256-crypt.c:248:     cp = p_bytes = (char *) alloca (key_len);
	leaq	30(%r15), %rax	#, tmp396
# sha256-crypt.c:121:   char *free_pbytes = NULL;
	movq	$0, -592(%rbp)	#, %sfp
# sha256-crypt.c:248:     cp = p_bytes = (char *) alloca (key_len);
	andq	$-16, %rax	#, tmp400
	subq	%rax, %rsp	# tmp400,
	leaq	15(%rsp), %rax	#, tmp402
	andq	$-16, %rax	#, tmp402
	movq	%rax, -496(%rbp)	# p_bytes, %sfp
	movq	%rax, -472(%rbp)	# p_bytes, cp
.L28:
# sha256-crypt.c:259:   for (cnt = key_len; cnt >= 32; cnt -= 32)
	cmpq	$31, %r15	#, tmp286
	jbe	.L63	#,
	leaq	-32(%r15), %rcx	#, _139
	movq	-496(%rbp), %rsi	# %sfp, p_bytes
	movq	%rcx, %rdx	# _139, tmp406
	andq	$-32, %rdx	#, tmp406
	leaq	32(%rsi), %rax	#, ivtmp.55
	leaq	64(%rsi,%rdx), %rdx	#, _26
	.p2align 4,,10
	.p2align 3
.L30:
# sha256-crypt.c:260:     cp = mempcpy (cp, temp_result, 32);
	movdqa	-432(%rbp), %xmm0	# MEM[(char * {ref-all})&temp_result], MEM[(char * {ref-all})&temp_result]
	movups	%xmm0, -32(%rax)	# MEM[(char * {ref-all})&temp_result], MEM[base: _140, offset: -32B]
	movdqa	-416(%rbp), %xmm0	# MEM[(char * {ref-all})&temp_result], MEM[(char * {ref-all})&temp_result]
	movups	%xmm0, -16(%rax)	# MEM[(char * {ref-all})&temp_result], MEM[base: _140, offset: -32B]
	movq	%rax, -472(%rbp)	# ivtmp.55, cp
	addq	$32, %rax	#, ivtmp.55
# sha256-crypt.c:259:   for (cnt = key_len; cnt >= 32; cnt -= 32)
	cmpq	%rax, %rdx	# ivtmp.55, _26
	jne	.L30	#,
	movq	-496(%rbp), %rax	# %sfp, p_bytes
	andq	$-32, %rcx	#, tmp410
	movq	%r15, %rdx	# tmp286, cnt
	andl	$31, %edx	#, cnt
	leaq	32(%rax,%rcx), %rcx	#, _309
.L29:
# sha256-crypt.c:261:   memcpy (cp, temp_result, cnt);
	cmpl	$8, %edx	#, cnt
	movl	%edx, %eax	# cnt, cnt
	jnb	.L31	#,
	andl	$4, %edx	#, cnt
	jne	.L119	#,
	testl	%eax, %eax	# cnt
	je	.L32	#,
	movq	-544(%rbp), %rsi	# %sfp, tmp627
	testb	$2, %al	#, cnt
	movzbl	(%rsi), %edx	#, tmp425
	movb	%dl, (%rcx)	# tmp425,* _309
	jne	.L120	#,
.L32:
# sha256-crypt.c:264:   sha256_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	-520(%rbp), %rdi	# %sfp,
# sha256-crypt.c:267:   for (cnt = 0; cnt < 16 + alt_result[0]; ++cnt)
	xorl	%r12d, %r12d	# cnt
# sha256-crypt.c:264:   sha256_init_ctx (&alt_ctx, nss_alt_ctx);
	call	__sha256_init_ctx@PLT	#
# sha256-crypt.c:267:   for (cnt = 0; cnt < 16 + alt_result[0]; ++cnt)
	movq	%rbx, -536(%rbp)	# tmp624, %sfp
	movq	-512(%rbp), %r13	# %sfp, salt
	movq	%r12, %rbx	# cnt, cnt
	movq	-520(%rbp), %r12	# %sfp, tmp625
	.p2align 4,,10
	.p2align 3
.L37:
# sha256-crypt.c:268:     sha256_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	movq	%r12, %rdx	# tmp625,
	movq	%r14, %rsi	# _6,
	movq	%r13, %rdi	# salt,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:267:   for (cnt = 0; cnt < 16 + alt_result[0]; ++cnt)
	movzbl	-464(%rbp), %edx	# alt_result, tmp451
	addq	$1, %rbx	#, cnt
	addq	$16, %rdx	#, tmp452
	cmpq	%rbx, %rdx	# cnt, tmp452
	ja	.L37	#,
# sha256-crypt.c:271:   sha256_finish_ctx (&alt_ctx, nss_alt_ctx, temp_result);
	movq	-544(%rbp), %r13	# %sfp, tmp627
	movq	-520(%rbp), %rdi	# %sfp,
	movq	-536(%rbp), %rbx	# %sfp, tmp624
	movq	%r13, %rsi	# tmp627,
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:274:   cp = s_bytes = alloca (salt_len);
	leaq	30(%r14), %rax	#, tmp457
# sha256-crypt.c:277:   memcpy (cp, temp_result, cnt);
	movl	%r14d, %ecx	# _6, _6
# sha256-crypt.c:274:   cp = s_bytes = alloca (salt_len);
	andq	$-16, %rax	#, tmp461
	subq	%rax, %rsp	# tmp461,
	leaq	15(%rsp), %rax	#, tmp463
	andq	$-16, %rax	#, tmp463
# sha256-crypt.c:277:   memcpy (cp, temp_result, cnt);
	cmpl	$8, %r14d	#, _6
# sha256-crypt.c:274:   cp = s_bytes = alloca (salt_len);
	movq	%rax, %rsi	# tmp463, tmp465
	movq	%rax, -536(%rbp)	# tmp465, %sfp
	movq	%rax, -472(%rbp)	# tmp465, cp
# sha256-crypt.c:277:   memcpy (cp, temp_result, cnt);
	movq	%r13, %rax	# tmp627, tmp469
	jnb	.L121	#,
.L38:
	xorl	%edx, %edx	# tmp475
	testb	$4, %cl	#, _6
	jne	.L122	#,
	testb	$2, %cl	#, _6
	jne	.L123	#,
.L42:
	andl	$1, %ecx	#, _6
	jne	.L124	#,
.L43:
# sha256-crypt.c:297:       if (cnt % 7 != 0)
	movq	%r14, -552(%rbp)	# _6, %sfp
# sha256-crypt.c:281:   for (cnt = 0; cnt < rounds; ++cnt)
	xorl	%r12d, %r12d	# cnt
# sha256-crypt.c:297:       if (cnt % 7 != 0)
	movq	-504(%rbp), %r14	# %sfp, tmp626
	jmp	.L50	#
	.p2align 4,,10
	.p2align 3
.L128:
# sha256-crypt.c:288: 	sha256_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	-496(%rbp), %rdi	# %sfp,
	movq	%r15, %rsi	# tmp286,
	call	__sha256_process_bytes@PLT	#
.L45:
# sha256-crypt.c:293:       if (cnt % 3 != 0)
	movabsq	$-6148914691236517205, %rax	#, tmp688
	mulq	%r12	# cnt
	shrq	%rdx	# tmp488
	leaq	(%rdx,%rdx,2), %rax	#, tmp493
	cmpq	%rax, %r12	# tmp493, cnt
	jne	.L125	#,
.L46:
# sha256-crypt.c:297:       if (cnt % 7 != 0)
	movabsq	$5270498306774157605, %rax	#, tmp689
	imulq	%r12	# cnt
	movq	%r12, %rax	# cnt, tmp518
	sarq	$63, %rax	#, tmp518
	sarq	%rdx	# tmp517
	subq	%rax, %rdx	# tmp518, tmp514
	leaq	0(,%rdx,8), %rax	#, tmp520
	subq	%rdx, %rax	# tmp514, tmp521
	cmpq	%rax, %r12	# tmp521, cnt
	jne	.L126	#,
.L47:
# sha256-crypt.c:301:       if ((cnt & 1) != 0)
	testq	%r13, %r13	# _27
# sha256-crypt.c:302: 	sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	movq	%rbx, %rdx	# tmp624,
# sha256-crypt.c:301:       if ((cnt & 1) != 0)
	je	.L48	#,
# sha256-crypt.c:302: 	sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	movl	$32, %esi	#,
	movq	%r14, %rdi	# tmp626,
	call	__sha256_process_bytes@PLT	#
.L49:
# sha256-crypt.c:307:       sha256_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	%r14, %rsi	# tmp626,
	movq	%rbx, %rdi	# tmp624,
# sha256-crypt.c:281:   for (cnt = 0; cnt < rounds; ++cnt)
	addq	$1, %r12	#, cnt
# sha256-crypt.c:307:       sha256_finish_ctx (&ctx, nss_ctx, alt_result);
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:281:   for (cnt = 0; cnt < rounds; ++cnt)
	cmpq	%r12, -528(%rbp)	# cnt, %sfp
	je	.L127	#,
.L50:
# sha256-crypt.c:284:       sha256_init_ctx (&ctx, nss_ctx);
	movq	%rbx, %rdi	# tmp624,
# sha256-crypt.c:287:       if ((cnt & 1) != 0)
	movq	%r12, %r13	# cnt, _27
# sha256-crypt.c:284:       sha256_init_ctx (&ctx, nss_ctx);
	call	__sha256_init_ctx@PLT	#
# sha256-crypt.c:287:       if ((cnt & 1) != 0)
	andl	$1, %r13d	#, _27
# sha256-crypt.c:288: 	sha256_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	%rbx, %rdx	# tmp624,
# sha256-crypt.c:287:       if ((cnt & 1) != 0)
	jne	.L128	#,
# sha256-crypt.c:290: 	sha256_process_bytes (alt_result, 32, &ctx, nss_ctx);
	movl	$32, %esi	#,
	movq	%r14, %rdi	# tmp626,
	call	__sha256_process_bytes@PLT	#
	jmp	.L45	#
	.p2align 4,,10
	.p2align 3
.L31:
# sha256-crypt.c:261:   memcpy (cp, temp_result, cnt);
	movq	-432(%rbp), %rax	#, tmp434
	movq	%rax, (%rcx)	# tmp434,* _309
	movq	-544(%rbp), %rdi	# %sfp, tmp627
	movl	%edx, %eax	# cnt, cnt
	movq	-8(%rdi,%rax), %rsi	#, tmp441
	movq	%rsi, -8(%rcx,%rax)	# tmp441,
	leaq	8(%rcx), %rsi	#, tmp442
	andq	$-8, %rsi	#, tmp442
	subq	%rsi, %rcx	# tmp442, _309
	leal	(%rdx,%rcx), %eax	#, cnt
	subq	%rcx, %rdi	# _309, tmp415
	andl	$-8, %eax	#, cnt
	cmpl	$8, %eax	#, cnt
	jb	.L32	#,
	andl	$-8, %eax	#, tmp444
	xorl	%edx, %edx	# tmp443
.L35:
	movl	%edx, %ecx	# tmp443, tmp445
	addl	$8, %edx	#, tmp443
	movq	(%rdi,%rcx), %r8	#, tmp446
	cmpl	%eax, %edx	# tmp444, tmp443
	movq	%r8, (%rsi,%rcx)	# tmp446,
	jb	.L35	#,
	jmp	.L32	#
	.p2align 4,,10
	.p2align 3
.L48:
# sha256-crypt.c:304: 	sha256_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	-496(%rbp), %rdi	# %sfp,
	movq	%r15, %rsi	# tmp286,
	call	__sha256_process_bytes@PLT	#
	jmp	.L49	#
	.p2align 4,,10
	.p2align 3
.L126:
# sha256-crypt.c:298: 	sha256_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	-496(%rbp), %rdi	# %sfp,
	movq	%rbx, %rdx	# tmp624,
	movq	%r15, %rsi	# tmp286,
	call	__sha256_process_bytes@PLT	#
	jmp	.L47	#
	.p2align 4,,10
	.p2align 3
.L125:
# sha256-crypt.c:294: 	sha256_process_bytes (s_bytes, salt_len, &ctx, nss_ctx);
	movq	-552(%rbp), %rsi	# %sfp,
	movq	-536(%rbp), %rdi	# %sfp,
	movq	%rbx, %rdx	# tmp624,
	call	__sha256_process_bytes@PLT	#
	jmp	.L46	#
	.p2align 4,,10
	.p2align 3
.L127:
# sha256-crypt.c:317:   cp = __stpncpy (buffer, sha256_salt_prefix, MAX (0, buflen));
	movl	-484(%rbp), %edx	# buflen,
	xorl	%r12d, %r12d	# tmp530
	movq	-560(%rbp), %rdi	# %sfp,
	leaq	sha256_salt_prefix(%rip), %rsi	#,
	movq	-552(%rbp), %r14	# %sfp, _6
	testl	%edx, %edx	#
	movl	%r12d, %edx	# tmp530, tmp529
	cmovns	-484(%rbp), %edx	# buflen,, tmp529
	movslq	%edx, %rdx	# tmp529, tmp531
	call	__stpncpy@PLT	#
	movq	%rax, %rdi	#, _33
	movq	%rax, -472(%rbp)	# _33, cp
# sha256-crypt.c:318:   buflen -= sizeof (sha256_salt_prefix) - 1;
	movl	-484(%rbp), %eax	# buflen, tmp750
# sha256-crypt.c:320:   if (rounds_custom)
	cmpb	$0, -485(%rbp)	#, %sfp
# sha256-crypt.c:318:   buflen -= sizeof (sha256_salt_prefix) - 1;
	leal	-3(%rax), %edx	#, _37
	movl	%edx, -484(%rbp)	# _37, buflen
# sha256-crypt.c:320:   if (rounds_custom)
	jne	.L129	#,
.L51:
# sha256-crypt.c:328:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	xorl	%r12d, %r12d	#
	testl	%edx, %edx	# _37
	movq	-512(%rbp), %rsi	# %sfp,
	cmovs	%r12d, %edx	# _37,, tmp541, tmp540
	movslq	%edx, %rdx	# tmp540, tmp542
	cmpq	%r14, %rdx	# _6, tmp542
	cmova	%r14, %rdx	# tmp542,, _6, tmp539
	call	__stpncpy@PLT	#
# sha256-crypt.c:329:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	movslq	-484(%rbp), %rdx	# buflen,
# sha256-crypt.c:328:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	movq	%rax, -472(%rbp)	# _50, cp
# sha256-crypt.c:329:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	testl	%edx, %edx	# buflen.23_51
	cmovns	%rdx, %r12	#,,
	cmpq	%r14, %r12	# _6, tmp546
	cmova	%r14, %r12	# tmp546,, _6, tmp543
	subl	%r12d, %edx	# tmp543, _58
# sha256-crypt.c:331:   if (buflen > 0)
	testl	%edx, %edx	# _58
# sha256-crypt.c:329:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	movl	%edx, -484(%rbp)	# _58, buflen
# sha256-crypt.c:331:   if (buflen > 0)
	jle	.L52	#,
# sha256-crypt.c:333:       *cp++ = '$';
	leaq	1(%rax), %rdx	#, tmp547
	movq	%rdx, -472(%rbp)	# tmp547, cp
	movb	$36, (%rax)	#, *_50
# sha256-crypt.c:334:       --buflen;
	subl	$1, -484(%rbp)	#, buflen
.L52:
# sha256-crypt.c:337:   __b64_from_24bit (&cp, &buflen,
	movzbl	-454(%rbp), %ecx	# alt_result, alt_result
	movzbl	-464(%rbp), %edx	# alt_result, alt_result
	leaq	-484(%rbp), %r13	#, tmp551
	movzbl	-444(%rbp), %r8d	# alt_result,
	leaq	-472(%rbp), %r12	#, tmp552
	movl	$4, %r9d	#,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:339:   __b64_from_24bit (&cp, &buflen,
	movzbl	-463(%rbp), %ecx	# alt_result, alt_result
	movzbl	-443(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-453(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:341:   __b64_from_24bit (&cp, &buflen,
	movzbl	-442(%rbp), %ecx	# alt_result, alt_result
	movzbl	-452(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-462(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:343:   __b64_from_24bit (&cp, &buflen,
	movzbl	-451(%rbp), %ecx	# alt_result, alt_result
	movzbl	-461(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-441(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:345:   __b64_from_24bit (&cp, &buflen,
	movzbl	-460(%rbp), %ecx	# alt_result, alt_result
	movzbl	-440(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-450(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:347:   __b64_from_24bit (&cp, &buflen,
	movzbl	-439(%rbp), %ecx	# alt_result, alt_result
	movzbl	-449(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-459(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:349:   __b64_from_24bit (&cp, &buflen,
	movzbl	-448(%rbp), %ecx	# alt_result, alt_result
	movzbl	-458(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-438(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:351:   __b64_from_24bit (&cp, &buflen,
	movzbl	-457(%rbp), %ecx	# alt_result, alt_result
	movzbl	-437(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-447(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:353:   __b64_from_24bit (&cp, &buflen,
	movzbl	-436(%rbp), %ecx	# alt_result, alt_result
	movzbl	-446(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-456(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:355:   __b64_from_24bit (&cp, &buflen,
	movzbl	-445(%rbp), %ecx	# alt_result, alt_result
	movzbl	-455(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-435(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:357:   __b64_from_24bit (&cp, &buflen,
	movzbl	-433(%rbp), %ecx	# alt_result, alt_result
	movzbl	-434(%rbp), %r8d	# alt_result,
	xorl	%edx, %edx	#
	movl	$3, %r9d	#,
	movq	%r13, %rsi	# tmp551,
	movq	%r12, %rdi	# tmp552,
	call	__b64_from_24bit@PLT	#
# sha256-crypt.c:359:   if (buflen <= 0)
	movl	-484(%rbp), %eax	# buflen,
	testl	%eax, %eax	#
	jle	.L130	#,
# sha256-crypt.c:365:     *cp = '\0';		/* Terminate the string.  */
	movq	-472(%rbp), %rax	# cp, cp.31_127
	movq	-560(%rbp), %r12	# %sfp, <retval>
	movb	$0, (%rax)	#, *cp.31_127
.L54:
# sha256-crypt.c:372:   __sha256_init_ctx (&ctx);
	movq	%rbx, %rdi	# tmp624,
	call	__sha256_init_ctx@PLT	#
# sha256-crypt.c:373:   __sha256_finish_ctx (&ctx, alt_result);
	movq	-504(%rbp), %rsi	# %sfp,
	movq	%rbx, %rdi	# tmp624,
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:374:   explicit_bzero (&ctx, sizeof (ctx));
	movl	$176, %edx	#,
	movl	$176, %esi	#,
	movq	%rbx, %rdi	# tmp624,
	call	__explicit_bzero_chk@PLT	#
# sha256-crypt.c:375:   explicit_bzero (&alt_ctx, sizeof (alt_ctx));
	movq	-520(%rbp), %rdi	# %sfp,
	movl	$176, %edx	#,
	movl	$176, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# sha256-crypt.c:377:   explicit_bzero (temp_result, sizeof (temp_result));
	movq	-544(%rbp), %rdi	# %sfp,
	movl	$32, %edx	#,
	movl	$32, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# sha256-crypt.c:378:   explicit_bzero (p_bytes, key_len);
	movq	-496(%rbp), %rdi	# %sfp,
	movq	$-1, %rdx	#,
	movq	%r15, %rsi	# tmp286,
	call	__explicit_bzero_chk@PLT	#
# sha256-crypt.c:379:   explicit_bzero (s_bytes, salt_len);
	movq	-536(%rbp), %rdi	# %sfp,
	movq	$-1, %rdx	#,
	movq	%r14, %rsi	# _6,
	call	__explicit_bzero_chk@PLT	#
# sha256-crypt.c:380:   if (copied_key != NULL)
	movq	-584(%rbp), %rax	# %sfp, copied_key
	testq	%rax, %rax	# copied_key
	je	.L55	#,
# sha256-crypt.c:381:     explicit_bzero (copied_key, key_len);
	movq	$-1, %rdx	#,
	movq	%r15, %rsi	# tmp286,
	movq	%rax, %rdi	# copied_key,
	call	__explicit_bzero_chk@PLT	#
.L55:
# sha256-crypt.c:382:   if (copied_salt != NULL)
	movq	-568(%rbp), %rax	# %sfp, copied_salt
	testq	%rax, %rax	# copied_salt
	je	.L56	#,
# sha256-crypt.c:383:     explicit_bzero (copied_salt, salt_len);
	movq	$-1, %rdx	#,
	movq	%r14, %rsi	# _6,
	movq	%rax, %rdi	# copied_salt,
	call	__explicit_bzero_chk@PLT	#
.L56:
# sha256-crypt.c:385:   free (free_key);
	movq	-576(%rbp), %rdi	# %sfp,
	call	free@PLT	#
# sha256-crypt.c:386:   free (free_pbytes);
	movq	-592(%rbp), %rdi	# %sfp,
	call	free@PLT	#
.L1:
# sha256-crypt.c:388: }
	leaq	-40(%rbp), %rsp	#,
	movq	%r12, %rax	# <retval>,
	popq	%rbx	#
	popq	%r12	#
	popq	%r13	#
	popq	%r14	#
	popq	%r15	#
	popq	%rbp	#
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
# sha256-crypt.c:277:   memcpy (cp, temp_result, cnt);
	movl	(%rax), %edx	#, tmp477
	testb	$2, %cl	#, _6
	movl	%edx, (%rsi)	# tmp477,* s_bytes
	movl	$4, %edx	#, tmp475
	je	.L42	#,
	jmp	.L123	#
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%rax,%rdx), %eax	#, tmp483
	movb	%al, (%rsi,%rdx)	# tmp483,
	jmp	.L43	#
	.p2align 4,,10
	.p2align 3
.L123:
	movzwl	(%rax,%rdx), %edi	#, tmp480
	movw	%di, (%rsi,%rdx)	# tmp480,
	addq	$2, %rdx	#, tmp475
	andl	$1, %ecx	#, _6
	je	.L43	#,
	jmp	.L124	#
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%r14d, %esi	# _6, tmp471
	xorl	%eax, %eax	# tmp470
	movq	%r13, %r8	# tmp627, tmp627
	andl	$-8, %esi	#, tmp471
.L39:
	movl	%eax, %edx	# tmp470, tmp472
	movq	-536(%rbp), %r10	# %sfp, tmp465
	addl	$8, %eax	#,
	movq	(%r8,%rdx), %rdi	# MEM[(void *)&temp_result], tmp473
	cmpl	%esi, %eax	# tmp471, tmp470
	movq	%rdi, (%r10,%rdx)	# tmp473, MEM[(void *)s_bytes_233]
	jb	.L39	#,
	movq	%r10, %rsi	# tmp465, tmp465
	addq	%rax, %rsi	# tmp474, s_bytes
	addq	-544(%rbp), %rax	# %sfp, tmp469
	jmp	.L38	#
	.p2align 4,,10
	.p2align 3
.L130:
# sha256-crypt.c:361:       __set_errno (ERANGE);
	movq	errno@gottpoff(%rip), %rax	#, tmp602
# sha256-crypt.c:362:       buffer = NULL;
	xorl	%r12d, %r12d	# <retval>
# sha256-crypt.c:361:       __set_errno (ERANGE);
	movl	$34, %fs:(%rax)	#, errno
	jmp	.L54	#
	.p2align 4,,10
	.p2align 3
.L129:
# sha256-crypt.c:322:       int n = __snprintf (cp, MAX (0, buflen), "%s%zu$",
	testl	%edx, %edx	# _37
	movq	-528(%rbp), %r8	# %sfp,
	leaq	sha256_rounds_prefix(%rip), %rcx	#,
	cmovs	%r12d, %edx	# _37,, tmp530, tmp533
	xorl	%eax, %eax	#
	movslq	%edx, %rsi	# tmp533, tmp535
	leaq	.LC1(%rip), %rdx	#,
	call	__snprintf@PLT	#
# sha256-crypt.c:325:       buflen -= n;
	movl	-484(%rbp), %edx	# buflen, _37
# sha256-crypt.c:324:       cp += n;
	movslq	%eax, %rdi	# n, n
	addq	-472(%rbp), %rdi	# cp, _33
# sha256-crypt.c:325:       buflen -= n;
	subl	%eax, %edx	# n, _37
# sha256-crypt.c:324:       cp += n;
	movq	%rdi, -472(%rbp)	# _33, cp
# sha256-crypt.c:325:       buflen -= n;
	movl	%edx, -484(%rbp)	# _37, buflen
	jmp	.L51	#
	.p2align 4,,10
	.p2align 3
.L113:
# sha256-crypt.c:132:       const char *num = salt + sizeof (sha256_rounds_prefix) - 1;
	movq	-512(%rbp), %rax	# %sfp, salt
# sha256-crypt.c:134:       unsigned long int srounds = strtoul (num, &endp, 10);
	leaq	-224(%rbp), %rsi	#, tmp281
	movl	$10, %edx	#,
# sha256-crypt.c:132:       const char *num = salt + sizeof (sha256_rounds_prefix) - 1;
	leaq	7(%rax), %rdi	#, num
# sha256-crypt.c:134:       unsigned long int srounds = strtoul (num, &endp, 10);
	call	strtoul@PLT	#
# sha256-crypt.c:135:       if (*endp == '$')
	movq	-224(%rbp), %rdx	# endp, endp.0_3
	cmpb	$36, (%rdx)	#, *endp.0_3
	jne	.L3	#,
# sha256-crypt.c:138: 	  rounds = MAX (ROUNDS_MIN, MIN (srounds, ROUNDS_MAX));
	cmpq	$999999999, %rax	#, srounds
# sha256-crypt.c:137: 	  salt = endp + 1;
	leaq	1(%rdx), %rcx	#, salt
# sha256-crypt.c:138: 	  rounds = MAX (ROUNDS_MIN, MIN (srounds, ROUNDS_MAX));
	movl	$999999999, %edx	#, tmp283
	cmovbe	%rax, %rdx	# srounds,, tmp283
	movl	$1000, %eax	#, tmp284
# sha256-crypt.c:139: 	  rounds_custom = true;
	movb	$1, -485(%rbp)	#, %sfp
	cmpq	$1000, %rdx	#, rounds
# sha256-crypt.c:137: 	  salt = endp + 1;
	movq	%rcx, -512(%rbp)	# salt, %sfp
	cmovnb	%rdx, %rax	# rounds,, tmp284
	movq	%rax, -528(%rbp)	# tmp284, %sfp
	jmp	.L3	#
	.p2align 4,,10
	.p2align 3
.L115:
# sha256-crypt.c:168:       char *tmp = (char *) alloca (salt_len + __alignof__ (uint32_t));
	leaq	34(%r14), %rax	#, tmp316
# sha256-crypt.c:169:       alloca_used += salt_len + __alignof__ (uint32_t);
	movq	-496(%rbp), %rcx	# %sfp, alloca_used
# sha256-crypt.c:170:       salt = copied_salt =
	movl	%r14d, %edx	# _6,
# sha256-crypt.c:168:       char *tmp = (char *) alloca (salt_len + __alignof__ (uint32_t));
	andq	$-16, %rax	#, tmp320
	subq	%rax, %rsp	# tmp320,
# sha256-crypt.c:169:       alloca_used += salt_len + __alignof__ (uint32_t);
	leaq	4(%r14,%rcx), %rcx	#, alloca_used
# sha256-crypt.c:168:       char *tmp = (char *) alloca (salt_len + __alignof__ (uint32_t));
	leaq	15(%rsp), %rax	#, tmp322
# sha256-crypt.c:169:       alloca_used += salt_len + __alignof__ (uint32_t);
	movq	%rcx, -496(%rbp)	# alloca_used, %sfp
# sha256-crypt.c:168:       char *tmp = (char *) alloca (salt_len + __alignof__ (uint32_t));
	andq	$-16, %rax	#, tmp324
# sha256-crypt.c:170:       salt = copied_salt =
	cmpl	$8, %r14d	#, _6
# sha256-crypt.c:172: 		- (tmp - (char *) 0) % __alignof__ (uint32_t),
	leaq	4(%rax), %rcx	#, tmp325
# sha256-crypt.c:170:       salt = copied_salt =
	jnb	.L11	#,
	testb	$4, %r14b	#, _6
	jne	.L131	#,
	testl	%edx, %edx	# _6
	je	.L12	#,
	movq	-512(%rbp), %rbx	# %sfp, salt
	testb	$2, %dl	#, _6
	movzbl	(%rbx), %esi	#* salt, tmp338
	movb	%sil, 4(%rax)	# tmp338,
	jne	.L132	#,
.L12:
	movq	%rcx, -512(%rbp)	# salt, %sfp
	movq	%rcx, -568(%rbp)	# salt, %sfp
	jmp	.L10	#
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-512(%rbp), %rbx	# %sfp, salt
	addq	$8, %rax	#, tmp355
	movq	(%rbx), %rdx	#* salt, tmp347
	movq	%rdx, -4(%rax)	# tmp347,
	movl	%r14d, %edx	# _6, _6
	movq	-8(%rbx,%rdx), %rsi	#, tmp354
	movq	%rsi, -8(%rcx,%rdx)	# tmp354,
	movq	%rcx, %rdx	# tmp325, tmp327
	subq	%rax, %rdx	# tmp355, tmp327
	subq	%rdx, %rbx	# tmp327, salt
	addl	%r14d, %edx	# _6, _6
	andl	$-8, %edx	#, _6
	movq	%rbx, %r8	# salt, salt
	cmpl	$8, %edx	#, _6
	jb	.L12	#,
	andl	$-8, %edx	#, tmp357
	xorl	%esi, %esi	# tmp356
.L15:
	movl	%esi, %edi	# tmp356, tmp358
	addl	$8, %esi	#, tmp356
	movq	(%r8,%rdi), %r9	#, tmp359
	cmpl	%edx, %esi	# tmp357, tmp356
	movq	%r9, (%rax,%rdi)	# tmp359,
	jb	.L15	#,
	jmp	.L12	#
.L119:
# sha256-crypt.c:261:   memcpy (cp, temp_result, cnt);
	movq	-544(%rbp), %rsi	# %sfp, tmp627
	movl	(%rsi), %edx	#, tmp417
	movl	%edx, (%rcx)	# tmp417,* _309
	movl	%eax, %edx	# cnt, cnt
	movl	-4(%rsi,%rdx), %eax	#, tmp424
	movl	%eax, -4(%rcx,%rdx)	# tmp424,
	jmp	.L32	#
	.p2align 4,,10
	.p2align 3
.L17:
# sha256-crypt.c:223:   sha256_process_bytes (alt_result, cnt, &ctx, nss_ctx);
	movq	-504(%rbp), %rdi	# %sfp,
	movq	%rbx, %rdx	# tmp624,
	movq	%r15, %rsi	# tmp286,
	call	__sha256_process_bytes@PLT	#
# sha256-crypt.c:227:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	testq	%r15, %r15	# tmp286
	jne	.L19	#,
# sha256-crypt.c:234:   sha256_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	-504(%rbp), %rsi	# %sfp,
	movq	%rbx, %rdi	# tmp624,
	call	__sha256_finish_ctx@PLT	#
# sha256-crypt.c:237:   sha256_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	-520(%rbp), %rdi	# %sfp,
	call	__sha256_init_ctx@PLT	#
	jmp	.L57	#
	.p2align 4,,10
	.p2align 3
.L63:
# sha256-crypt.c:259:   for (cnt = key_len; cnt >= 32; cnt -= 32)
	movq	-496(%rbp), %rcx	# %sfp, _309
	movq	%r15, %rdx	# tmp286, cnt
	jmp	.L29	#
.L120:
# sha256-crypt.c:261:   memcpy (cp, temp_result, cnt);
	movl	%eax, %edx	# cnt, cnt
	movq	-544(%rbp), %rax	# %sfp, tmp627
	movzwl	-2(%rax,%rdx), %eax	#, tmp433
	movw	%ax, -2(%rcx,%rdx)	# tmp433,
	jmp	.L32	#
.L118:
# sha256-crypt.c:251:       free_pbytes = cp = p_bytes = (char *)malloc (key_len);
	movq	%r15, %rdi	# tmp286,
	call	malloc@PLT	#
# sha256-crypt.c:252:       if (free_pbytes == NULL)
	testq	%rax, %rax	# p_bytes
# sha256-crypt.c:251:       free_pbytes = cp = p_bytes = (char *)malloc (key_len);
	movq	%rax, -496(%rbp)	# p_bytes, %sfp
	movq	%rax, -472(%rbp)	# p_bytes, cp
# sha256-crypt.c:252:       if (free_pbytes == NULL)
	je	.L27	#,
	movq	%rax, -592(%rbp)	# p_bytes, %sfp
	jmp	.L28	#
.L131:
# sha256-crypt.c:170:       salt = copied_salt =
	movq	-512(%rbp), %rbx	# %sfp, salt
	movl	(%rbx), %esi	#* salt, tmp330
	movl	%esi, 4(%rax)	# tmp330,
	movl	-4(%rbx,%rdx), %eax	#, tmp337
	movl	%eax, -4(%rcx,%rdx)	# tmp337,
	jmp	.L12	#
.L132:
	movq	-512(%rbp), %rax	# %sfp, salt
	movzwl	-2(%rax,%rdx), %eax	#, tmp346
	movw	%ax, -2(%rcx,%rdx)	# tmp346,
	jmp	.L12	#
.L114:
# sha256-crypt.c:154: 	  free_key = tmp = (char *) malloc (key_len + __alignof__ (uint32_t));
	movq	%rbx, %rdi	# _9,
	call	malloc@PLT	#
# sha256-crypt.c:155: 	  if (tmp == NULL)
	testq	%rax, %rax	# free_key
# sha256-crypt.c:154: 	  free_key = tmp = (char *) malloc (key_len + __alignof__ (uint32_t));
	movq	%rax, -576(%rbp)	# free_key, %sfp
# sha256-crypt.c:155: 	  if (tmp == NULL)
	je	.L61	#,
	movq	%rax, %rdi	# free_key, m__
# sha256-crypt.c:119:   size_t alloca_used = 0;
	movq	$0, -496(%rbp)	#, %sfp
	jmp	.L9	#
.L27:
# sha256-crypt.c:254: 	  free (free_key);
	movq	-576(%rbp), %rdi	# %sfp,
# sha256-crypt.c:255: 	  return NULL;
	xorl	%r12d, %r12d	# <retval>
# sha256-crypt.c:254: 	  free (free_key);
	call	free@PLT	#
# sha256-crypt.c:255: 	  return NULL;
	jmp	.L1	#
.L61:
# sha256-crypt.c:156: 	    return NULL;
	xorl	%r12d, %r12d	# <retval>
	jmp	.L1	#
	.cfi_endproc
.LFE41:
	.size	__sha256_crypt_r, .-__sha256_crypt_r
	.p2align 4,,15
	.globl	__sha256_crypt
	.type	__sha256_crypt, @function
__sha256_crypt:
.LFB42:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %r12	# key, key
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
# sha256-crypt.c:407: 		+ strlen (salt) + 1 + 43 + 1);
	movq	%rsi, %rdi	# salt,
# sha256-crypt.c:399: {
	movq	%rsi, %rbp	# salt, salt
# sha256-crypt.c:407: 		+ strlen (salt) + 1 + 43 + 1);
	call	strlen@PLT	#
# sha256-crypt.c:409:   if (buflen < needed)
	movl	buflen.5422(%rip), %ecx	# buflen, buflen.33_4
# sha256-crypt.c:407: 		+ strlen (salt) + 1 + 43 + 1);
	leal	66(%rax), %ebx	#, needed
	movq	buffer(%rip), %rdx	# buffer, <retval>
# sha256-crypt.c:409:   if (buflen < needed)
	cmpl	%ebx, %ecx	# needed, buflen.33_4
	jge	.L134	#,
# sha256-crypt.c:411:       char *new_buffer = (char *) realloc (buffer, needed);
	movq	%rdx, %rdi	# <retval>,
	movslq	%ebx, %rsi	# needed, needed
	call	realloc@PLT	#
# sha256-crypt.c:412:       if (new_buffer == NULL)
	testq	%rax, %rax	# <retval>
# sha256-crypt.c:411:       char *new_buffer = (char *) realloc (buffer, needed);
	movq	%rax, %rdx	#, <retval>
# sha256-crypt.c:412:       if (new_buffer == NULL)
	je	.L133	#,
# sha256-crypt.c:415:       buffer = new_buffer;
	movq	%rax, buffer(%rip)	# <retval>, buffer
# sha256-crypt.c:416:       buflen = needed;
	movl	%ebx, buflen.5422(%rip)	# needed, buflen
	movl	%ebx, %ecx	# needed, buflen.33_4
.L134:
# sha256-crypt.c:420: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# sha256-crypt.c:419:   return __sha256_crypt_r (key, salt, buffer, buflen);
	movq	%rbp, %rsi	# salt,
	movq	%r12, %rdi	# key,
# sha256-crypt.c:420: }
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
# sha256-crypt.c:419:   return __sha256_crypt_r (key, salt, buffer, buflen);
	jmp	__sha256_crypt_r@PLT	#
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
# sha256-crypt.c:420: }
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax	#
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE42:
	.size	__sha256_crypt, .-__sha256_crypt
	.local	buflen.5422
	.comm	buflen.5422,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	sha256_rounds_prefix, @object
	.size	sha256_rounds_prefix, 8
sha256_rounds_prefix:
	.string	"rounds="
	.section	.rodata.str1.1
	.type	sha256_salt_prefix, @object
	.size	sha256_salt_prefix, 4
sha256_salt_prefix:
	.string	"$5$"
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
