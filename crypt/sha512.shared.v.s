	.file	"sha512.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/sha512.shared.v.d -MF /run/asm/crypt/sha512.os.dt -MP
# -MT /run/asm/crypt/.os -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC
# -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h sha512.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/crypt/sha512.shared.v.s -O2 -Wall -Wwrite-strings
# -Wundef -Werror -Wstrict-prototypes -Wold-style-definition -std=gnu11
# -fverbose-asm -fgnu89-inline -fmerge-all-constants -frounding-math
# -fno-stack-protector -fmath-errno -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.globl	__sha512_init_ctx
	.type	__sha512_init_ctx, @function
__sha512_init_ctx:
.LFB32:
	.cfi_startproc
# sha512.c:112:   ctx->H[0] = UINT64_C (0x6a09e667f3bcc908);
	movabsq	$7640891576956012808, %rax	#, tmp96
# sha512.c:121:   ctx->total[0] = ctx->total[1] = 0;
	movq	$0, 72(%rdi)	#, ctx_2(D)->D.4694.total
	movq	$0, 64(%rdi)	#, ctx_2(D)->D.4694.total
# sha512.c:112:   ctx->H[0] = UINT64_C (0x6a09e667f3bcc908);
	movq	%rax, (%rdi)	# tmp96, ctx_2(D)->H
# sha512.c:113:   ctx->H[1] = UINT64_C (0xbb67ae8584caa73b);
	movabsq	$-4942790177534073029, %rax	#, tmp97
# sha512.c:122:   ctx->buflen = 0;
	movq	$0, 80(%rdi)	#, ctx_2(D)->buflen
# sha512.c:113:   ctx->H[1] = UINT64_C (0xbb67ae8584caa73b);
	movq	%rax, 8(%rdi)	# tmp97, ctx_2(D)->H
# sha512.c:114:   ctx->H[2] = UINT64_C (0x3c6ef372fe94f82b);
	movabsq	$4354685564936845355, %rax	#, tmp98
	movq	%rax, 16(%rdi)	# tmp98, ctx_2(D)->H
# sha512.c:115:   ctx->H[3] = UINT64_C (0xa54ff53a5f1d36f1);
	movabsq	$-6534734903238641935, %rax	#, tmp99
	movq	%rax, 24(%rdi)	# tmp99, ctx_2(D)->H
# sha512.c:116:   ctx->H[4] = UINT64_C (0x510e527fade682d1);
	movabsq	$5840696475078001361, %rax	#, tmp100
	movq	%rax, 32(%rdi)	# tmp100, ctx_2(D)->H
# sha512.c:117:   ctx->H[5] = UINT64_C (0x9b05688c2b3e6c1f);
	movabsq	$-7276294671716946913, %rax	#, tmp101
	movq	%rax, 40(%rdi)	# tmp101, ctx_2(D)->H
# sha512.c:118:   ctx->H[6] = UINT64_C (0x1f83d9abfb41bd6b);
	movabsq	$2270897969802886507, %rax	#, tmp102
	movq	%rax, 48(%rdi)	# tmp102, ctx_2(D)->H
# sha512.c:119:   ctx->H[7] = UINT64_C (0x5be0cd19137e2179);
	movabsq	$6620516959819538809, %rax	#, tmp103
	movq	%rax, 56(%rdi)	# tmp103, ctx_2(D)->H
# sha512.c:123: }
	ret
	.cfi_endproc
.LFE32:
	.size	__sha512_init_ctx, .-__sha512_init_ctx
	.p2align 4,,15
	.globl	__sha512_process_block
	.type	__sha512_process_block, @function
__sha512_process_block:
.LFB35:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdx, %rbx	# ctx, ctx
	subq	$624, %rsp	#,
	.cfi_def_cfa_offset 680
# ./sha512-block.c:10:   uint64_t a = ctx->H[0];
	movq	(%rdx), %rax	# ctx_81(D)->H, a
# ./sha512-block.c:7: {
	movq	%rdi, -56(%rsp)	# buffer, %sfp
# ./sha512-block.c:9:   size_t nwords = len / sizeof (uint64_t);
	movq	%rsi, %rdi	# len, nwords
# ./sha512-block.c:7: {
	movq	%rdx, -32(%rsp)	# ctx, %sfp
# ./sha512-block.c:9:   size_t nwords = len / sizeof (uint64_t);
	shrq	$3, %rdi	#, nwords
# ./sha512-block.c:10:   uint64_t a = ctx->H[0];
	movq	%rax, -96(%rsp)	# a, %sfp
# ./sha512-block.c:11:   uint64_t b = ctx->H[1];
	movq	8(%rdx), %rax	# ctx_81(D)->H, b
# ./sha512-block.c:9:   size_t nwords = len / sizeof (uint64_t);
	movq	%rdi, -48(%rsp)	# nwords, %sfp
# ./sha512-block.c:11:   uint64_t b = ctx->H[1];
	movq	%rax, -88(%rsp)	# b, %sfp
# ./sha512-block.c:12:   uint64_t c = ctx->H[2];
	movq	16(%rdx), %rax	# ctx_81(D)->H, c
	movq	%rax, -80(%rsp)	# c, %sfp
# ./sha512-block.c:13:   uint64_t d = ctx->H[3];
	movq	24(%rdx), %rax	# ctx_81(D)->H, d
	movq	%rax, -104(%rsp)	# d, %sfp
# ./sha512-block.c:14:   uint64_t e = ctx->H[4];
	movq	32(%rdx), %rax	# ctx_81(D)->H, e
	movq	%rax, -72(%rsp)	# e, %sfp
# ./sha512-block.c:15:   uint64_t f = ctx->H[5];
	movq	40(%rdx), %rax	# ctx_81(D)->H, f
	movq	%rax, -112(%rsp)	# f, %sfp
# ./sha512-block.c:16:   uint64_t g = ctx->H[6];
	movq	48(%rdx), %rax	# ctx_81(D)->H, g
	movq	%rax, -64(%rsp)	# g, %sfp
# ./sha512-block.c:17:   uint64_t h = ctx->H[7];
	movq	56(%rdx), %rax	# ctx_81(D)->H, h
# ./sha512-block.c:23:   ctx->total128 += len;
	xorl	%edx, %edx	# len
	addq	%rsi, 64(%rbx)	# len, ctx_81(D)->D.4694.total128
	adcq	%rdx, 72(%rbx)	# len, ctx_81(D)->D.4694.total128
# ./sha512-block.c:33:   while (nwords > 0)
	testq	%rdi, %rdi	# nwords
# ./sha512-block.c:17:   uint64_t h = ctx->H[7];
	movq	%rax, -120(%rsp)	# h, %sfp
# ./sha512-block.c:33:   while (nwords > 0)
	je	.L4	#,
	leaq	-24(%rsp), %r14	#, tmp243
	leaq	K(%rip), %r15	#, tmp244
	leaq	512(%r14), %rax	#, _215
	movq	%rax, -40(%rsp)	# _215, %sfp
	.p2align 4,,10
	.p2align 3
.L9:
# ./sha512-block.c:7: {
	movq	-56(%rsp), %rcx	# %sfp, buffer
	xorl	%eax, %eax	# ivtmp.42
	.p2align 4,,10
	.p2align 3
.L5:
# ../bits/byteswap.h:73:   return __builtin_bswap64 (__bsx);
	movq	(%rcx,%rax), %rdx	# MEM[base: words_140, index: ivtmp.42_214, offset: 0B], MEM[base: words_140, index: ivtmp.42_214, offset: 0B]
	bswap	%rdx	# _130
# ./sha512-block.c:60: 	  W[t] = SWAP (*words);
	movq	%rdx, (%r14,%rax)	# _130, MEM[symbol: W, index: ivtmp.42_214, offset: 0B]
	addq	$8, %rax	#, ivtmp.42
# ./sha512-block.c:58:       for (unsigned int t = 0; t < 16; ++t)
	cmpq	$128, %rax	#, ivtmp.42
	jne	.L5	#,
	subq	$-128, -56(%rsp)	#, %sfp
	movq	-40(%rsp), %rdi	# %sfp, _215
	movq	%r14, %rsi	# tmp243, ivtmp.38
	.p2align 4,,10
	.p2align 3
.L6:
# ./sha512-block.c:64: 	W[t] = R1 (W[t - 2]) + W[t - 7] + R0 (W[t - 15]) + W[t - 16];
	movq	112(%rsi), %rax	# MEM[base: _218, offset: 112B], _6
	movq	8(%rsi), %rcx	# MEM[base: _218, offset: 8B], _19
	addq	$8, %rsi	#, ivtmp.38
	movq	%rax, %rdx	# _6, tmp204
	movq	%rax, %r8	# _6, tmp205
	shrq	$6, %rax	#, tmp207
	rolq	$3, %r8	#, tmp205
	rorq	$19, %rdx	#, tmp204
	xorq	%r8, %rdx	# tmp205, tmp206
	movq	%rcx, %r8	# _19, tmp214
	xorq	%rax, %rdx	# tmp207, tmp208
	movq	-8(%rsi), %rax	# MEM[base: _218, offset: 0B], MEM[base: _218, offset: 0B]
	addq	64(%rsi), %rax	# MEM[base: _218, offset: 72B], tmp209
	rorq	$8, %r8	#, tmp214
	addq	%rax, %rdx	# tmp209, tmp212
	movq	%rcx, %rax	# _19, tmp213
	shrq	$7, %rcx	#, tmp216
	rorq	%rax	# tmp213
	xorq	%r8, %rax	# tmp214, tmp215
	xorq	%rcx, %rax	# tmp216, tmp217
	addq	%rdx, %rax	# tmp212, tmp218
	movq	%rax, 120(%rsi)	# tmp218, MEM[base: _218, offset: 128B]
# ./sha512-block.c:63:       for (unsigned int t = 16; t < 80; ++t)
	cmpq	%rsi, %rdi	# ivtmp.38, _215
	jne	.L6	#,
	movq	-120(%rsp), %rax	# %sfp, h
	movq	-64(%rsp), %rbx	# %sfp, g
	xorl	%r9d, %r9d	# ivtmp.28
	movq	-112(%rsp), %rbp	# %sfp, f
	movq	-72(%rsp), %rdi	# %sfp, e
	movabsq	$4794697086780616226, %r12	#, pretmp_256
	movq	-104(%rsp), %r13	# %sfp, d
	movq	-80(%rsp), %r10	# %sfp, c
	movq	-88(%rsp), %r11	# %sfp, b
	movq	-96(%rsp), %r8	# %sfp, a
	movq	%rax, %rsi	# h, h
	jmp	.L8	#
	.p2align 4,,10
	.p2align 3
.L17:
	movq	(%r15,%r9), %r12	# MEM[symbol: K, index: ivtmp.28_225, offset: 0B], pretmp_256
	movq	%r10, %r13	# c, d
	movq	%rbx, %rsi	# g, h
# ./sha512-block.c:74: 	  e = d + T1;
	movq	%r11, %r10	# b, c
	movq	%rbp, %rbx	# f, g
	movq	%r8, %r11	# a, b
	movq	%rdi, %rbp	# e, f
# ./sha512-block.c:78: 	  a = T1 + T2;
	movq	%rax, %r8	# a, a
# ./sha512-block.c:74: 	  e = d + T1;
	movq	%rcx, %rdi	# e, e
.L8:
# ./sha512-block.c:69: 	  uint64_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	movq	%rdi, %rax	# e, tmp219
	movq	%rdi, %rdx	# e, tmp220
	movq	%rdi, %rcx	# e, tmp226
	rorq	$18, %rdx	#, tmp220
	rorq	$14, %rax	#, tmp219
	andq	%rbp, %rcx	# f, tmp226
	xorq	%rdx, %rax	# tmp220, tmp221
	movq	%rdi, %rdx	# e, tmp222
	rolq	$23, %rdx	#, tmp222
	xorq	%rax, %rdx	# tmp221, tmp223
	movq	%rdi, %rax	# e, tmp224
	notq	%rax	# tmp224
	andq	%rbx, %rax	# g, tmp225
	xorq	%rcx, %rax	# tmp226, tmp227
# ./sha512-block.c:70: 	  uint64_t T2 = S0 (a) + Maj (a, b, c);
	movq	%r8, %rcx	# a, tmp234
# ./sha512-block.c:69: 	  uint64_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addq	%rdx, %rax	# tmp223, tmp228
# ./sha512-block.c:70: 	  uint64_t T2 = S0 (a) + Maj (a, b, c);
	movq	%r8, %rdx	# a, tmp233
# ./sha512-block.c:69: 	  uint64_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addq	(%r14,%r9), %rax	# MEM[symbol: W, index: ivtmp.28_226, offset: 0B], tmp230
# ./sha512-block.c:70: 	  uint64_t T2 = S0 (a) + Maj (a, b, c);
	rolq	$30, %rcx	#, tmp234
	rorq	$28, %rdx	#, tmp233
	addq	$8, %r9	#, ivtmp.28
	xorq	%rcx, %rdx	# tmp234, tmp235
	movq	%r8, %rcx	# a, tmp236
	rolq	$25, %rcx	#, tmp236
	xorq	%rdx, %rcx	# tmp235, tmp237
	movq	%r11, %rdx	# b, tmp238
# ./sha512-block.c:69: 	  uint64_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addq	%rsi, %rax	# h, tmp232
# ./sha512-block.c:70: 	  uint64_t T2 = S0 (a) + Maj (a, b, c);
	xorq	%r10, %rdx	# c, tmp238
	movq	%r11, %rsi	# b, tmp240
# ./sha512-block.c:69: 	  uint64_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addq	%r12, %rax	# pretmp_256, T1
# ./sha512-block.c:70: 	  uint64_t T2 = S0 (a) + Maj (a, b, c);
	andq	%r8, %rdx	# a, tmp239
	andq	%r10, %rsi	# c, tmp240
	xorq	%rsi, %rdx	# tmp240, tmp241
	addq	%rcx, %rdx	# tmp237, T2
# ./sha512-block.c:74: 	  e = d + T1;
	leaq	(%rax,%r13), %rcx	#, e
# ./sha512-block.c:78: 	  a = T1 + T2;
	addq	%rdx, %rax	# T2, a
# ./sha512-block.c:67:       for (unsigned int t = 0; t < 80; ++t)
	cmpq	$640, %r9	#, ivtmp.28
	jne	.L17	#,
# ./sha512-block.c:83:       a += a_save;
	addq	%rax, -96(%rsp)	# a, %sfp
# ./sha512-block.c:84:       b += b_save;
	addq	%r8, -88(%rsp)	# a, %sfp
# ./sha512-block.c:85:       c += c_save;
	addq	%r11, -80(%rsp)	# b, %sfp
# ./sha512-block.c:86:       d += d_save;
	addq	%r10, -104(%rsp)	# c, %sfp
# ./sha512-block.c:87:       e += e_save;
	addq	%rcx, -72(%rsp)	# e, %sfp
# ./sha512-block.c:88:       f += f_save;
	addq	%rdi, -112(%rsp)	# e, %sfp
# ./sha512-block.c:89:       g += g_save;
	addq	%rbp, -64(%rsp)	# f, %sfp
# ./sha512-block.c:90:       h += h_save;
	addq	%rbx, -120(%rsp)	# g, %sfp
# ./sha512-block.c:33:   while (nwords > 0)
	subq	$16, -48(%rsp)	#, %sfp
	jne	.L9	#,
.L4:
# ./sha512-block.c:97:   ctx->H[0] = a;
	movq	-32(%rsp), %rax	# %sfp, ctx
	movq	-96(%rsp), %rbx	# %sfp, a
	movq	%rbx, (%rax)	# a, ctx_81(D)->H
# ./sha512-block.c:98:   ctx->H[1] = b;
	movq	-88(%rsp), %rbx	# %sfp, b
	movq	%rbx, 8(%rax)	# b, ctx_81(D)->H
# ./sha512-block.c:99:   ctx->H[2] = c;
	movq	-80(%rsp), %rbx	# %sfp, c
	movq	%rbx, 16(%rax)	# c, ctx_81(D)->H
# ./sha512-block.c:100:   ctx->H[3] = d;
	movq	-104(%rsp), %rbx	# %sfp, d
	movq	%rbx, 24(%rax)	# d, ctx_81(D)->H
# ./sha512-block.c:101:   ctx->H[4] = e;
	movq	-72(%rsp), %rbx	# %sfp, e
	movq	%rbx, 32(%rax)	# e, ctx_81(D)->H
# ./sha512-block.c:102:   ctx->H[5] = f;
	movq	-112(%rsp), %rbx	# %sfp, f
	movq	%rbx, 40(%rax)	# f, ctx_81(D)->H
# ./sha512-block.c:103:   ctx->H[6] = g;
	movq	-64(%rsp), %rbx	# %sfp, g
	movq	%rbx, 48(%rax)	# g, ctx_81(D)->H
# ./sha512-block.c:104:   ctx->H[7] = h;
	movq	-120(%rsp), %rbx	# %sfp, h
	movq	%rbx, 56(%rax)	# h, ctx_81(D)->H
# ./sha512-block.c:105: }
	addq	$624, %rsp	#,
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE35:
	.size	__sha512_process_block, .-__sha512_process_block
	.p2align 4,,15
	.globl	__sha512_finish_ctx
	.type	__sha512_finish_ctx, @function
__sha512_finish_ctx:
.LFB33:
	.cfi_startproc
	pushq	%r14	#
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13	#
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	pushq	%r12	#
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp	#
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	movq	%rsi, %rbp	# resbuf, resbuf
	pushq	%rbx	#
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
# sha512.c:135:   uint64_t bytes = ctx->buflen;
	movq	80(%rdi), %rax	# ctx_25(D)->buflen, bytes
# sha512.c:133: {
	movq	%rdi, %rbx	# ctx, ctx
# sha512.c:140:   ctx->total128 += bytes;
	xorl	%edi, %edi	# bytes
	addq	%rax, 64(%rbx)	# bytes, ctx_25(D)->D.4694.total128
	adcq	%rdi, 72(%rbx)	# bytes, ctx_25(D)->D.4694.total128
# sha512.c:147:   pad = bytes >= 112 ? 128 + 112 - bytes : 112 - bytes;
	cmpq	$111, %rax	#, bytes
	jbe	.L19	#,
# sha512.c:147:   pad = bytes >= 112 ? 128 + 112 - bytes : 112 - bytes;
	movl	$240, %edx	#, tmp117
	movl	$256, %r12d	#, prephitmp_49
	movl	$30, %r13d	#, prephitmp_48
	subq	%rax, %rdx	# bytes, iftmp.0_22
	movl	$31, %r14d	#, prephitmp_41
.L20:
# sha512.c:148:   memcpy (&ctx->buffer[bytes], fillbuf, pad);
	leaq	88(%rbx,%rax), %rdi	#, tmp120
	leaq	fillbuf(%rip), %rsi	#,
	call	memcpy@PLT	#
# sha512.c:151:   ctx->buffer64[(bytes + pad + 8) / 8] = SWAP (ctx->total[TOTAL128_low] << 3);
	movq	64(%rbx), %rdx	# ctx_25(D)->D.4694.total, _5
# sha512.c:156:   __sha512_process_block (ctx->buffer, bytes + pad + 16, ctx);
	leaq	88(%rbx), %rdi	#, tmp134
	movq	%r12, %rsi	# prephitmp_49,
# sha512.c:151:   ctx->buffer64[(bytes + pad + 8) / 8] = SWAP (ctx->total[TOTAL128_low] << 3);
	leaq	0(,%rdx,8), %rax	#, tmp127
# sha512.c:152:   ctx->buffer64[(bytes + pad) / 8] = SWAP ((ctx->total[TOTAL128_high] << 3)
	shrq	$61, %rdx	#, tmp131
# ../bits/byteswap.h:73:   return __builtin_bswap64 (__bsx);
	bswap	%rax	# _33
# sha512.c:151:   ctx->buffer64[(bytes + pad + 8) / 8] = SWAP (ctx->total[TOTAL128_low] << 3);
	movq	%rax, 88(%rbx,%r14,8)	# _33, ctx_25(D)->D.4699.buffer64
# sha512.c:152:   ctx->buffer64[(bytes + pad) / 8] = SWAP ((ctx->total[TOTAL128_high] << 3)
	movq	72(%rbx), %rax	# ctx_25(D)->D.4694.total, tmp137
	salq	$3, %rax	#, tmp129
	orq	%rdx, %rax	# tmp131, tmp132
# sha512.c:156:   __sha512_process_block (ctx->buffer, bytes + pad + 16, ctx);
	movq	%rbx, %rdx	# ctx,
# ../bits/byteswap.h:73:   return __builtin_bswap64 (__bsx);
	bswap	%rax	# _37
# sha512.c:152:   ctx->buffer64[(bytes + pad) / 8] = SWAP ((ctx->total[TOTAL128_high] << 3)
	movq	%rax, 88(%rbx,%r13,8)	# _37, ctx_25(D)->D.4699.buffer64
# sha512.c:156:   __sha512_process_block (ctx->buffer, bytes + pad + 16, ctx);
	call	__sha512_process_block@PLT	#
	xorl	%eax, %eax	# ivtmp.67
	.p2align 4,,10
	.p2align 3
.L21:
# ../bits/byteswap.h:73:   return __builtin_bswap64 (__bsx);
	movq	(%rbx,%rax), %rdx	# MEM[base: ctx_25(D), index: ivtmp.67_40, offset: 0B], MEM[base: ctx_25(D), index: ivtmp.67_40, offset: 0B]
	bswap	%rdx	# _31
# sha512.c:160:     ((uint64_t *) resbuf)[i] = SWAP (ctx->H[i]);
	movq	%rdx, 0(%rbp,%rax)	# _31, MEM[base: resbuf_36(D), index: ivtmp.67_40, offset: 0B]
	addq	$8, %rax	#, ivtmp.67
# sha512.c:159:   for (unsigned int i = 0; i < 8; ++i)
	cmpq	$64, %rax	#, ivtmp.67
	jne	.L21	#,
# sha512.c:163: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	movq	%rbp, %rax	# resbuf,
	popq	%rbp	#
	.cfi_def_cfa_offset 32
	popq	%r12	#
	.cfi_def_cfa_offset 24
	popq	%r13	#
	.cfi_def_cfa_offset 16
	popq	%r14	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
# sha512.c:147:   pad = bytes >= 112 ? 128 + 112 - bytes : 112 - bytes;
	movl	$112, %edx	#, tmp118
	movl	$128, %r12d	#, prephitmp_49
	movl	$14, %r13d	#, prephitmp_48
	subq	%rax, %rdx	# bytes, iftmp.0_22
	movl	$15, %r14d	#, prephitmp_41
	jmp	.L20	#
	.cfi_endproc
.LFE33:
	.size	__sha512_finish_ctx, .-__sha512_finish_ctx
	.p2align 4,,15
	.globl	__sha512_process_bytes
	.type	__sha512_process_bytes, @function
__sha512_process_bytes:
.LFB34:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdi, %r12	# buffer, buffer
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdx, %rbp	# ctx, ctx
	movq	%rsi, %rbx	# len, len
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 64
# sha512.c:171:   if (ctx->buflen != 0)
	movq	80(%rdx), %r14	# ctx_28(D)->buflen, _1
	testq	%r14, %r14	# _1
	jne	.L54	#,
.L25:
# sha512.c:194:   if (len >= 128)
	cmpq	$127, %rbx	#, len
	ja	.L55	#,
.L31:
# sha512.c:222:   if (len > 0)
	testq	%rbx, %rbx	# len
	jne	.L56	#,
# sha512.c:236: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	.cfi_restore_state
# sha512.c:224:       size_t left_over = ctx->buflen;
	movq	80(%rbp), %r8	# ctx_28(D)->buflen, left_over
# sha512.c:226:       memcpy (&ctx->buffer[left_over], buffer, len);
	cmpl	$8, %ebx	#, len
	leaq	88(%rbp,%r8), %rdx	#, tmp161
	jnb	.L33	#,
	testb	$4, %bl	#, len
	jne	.L57	#,
	testl	%ebx, %ebx	# len
	je	.L34	#,
	movzbl	(%r12), %eax	#* buffer, tmp174
	testb	$2, %bl	#, len
	movb	%al, (%rdx)	# tmp174,
	jne	.L58	#,
.L34:
# sha512.c:227:       left_over += len;
	addq	%r8, %rbx	# left_over, left_over
# sha512.c:228:       if (left_over >= 128)
	cmpq	$127, %rbx	#, left_over
	ja	.L59	#,
.L37:
# sha512.c:234:       ctx->buflen = left_over;
	movq	%rbx, 80(%rbp)	# left_over, ctx_28(D)->buflen
# sha512.c:236: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	.cfi_restore_state
# sha512.c:215: 	  __sha512_process_block (buffer, len & ~127, ctx);
	movq	%rbx, %r13	# len, _13
	movq	%r12, %rdi	# buffer,
	movq	%rbp, %rdx	# ctx,
	andq	$-128, %r13	#, _13
# sha512.c:217: 	  len &= 127;
	andl	$127, %ebx	#, len
# sha512.c:215: 	  __sha512_process_block (buffer, len & ~127, ctx);
	movq	%r13, %rsi	# _13,
# sha512.c:216: 	  buffer = (const char *) buffer + (len & ~127);
	addq	%r13, %r12	# _13, buffer
# sha512.c:215: 	  __sha512_process_block (buffer, len & ~127, ctx);
	call	__sha512_process_block@PLT	#
	jmp	.L31	#
	.p2align 4,,10
	.p2align 3
.L54:
# sha512.c:174:       size_t add = 256 - left_over > len ? len : 256 - left_over;
	movl	$256, %r13d	#, tmp113
# sha512.c:176:       memcpy (&ctx->buffer[left_over], buffer, add);
	leaq	88(%rdx,%r14), %rdi	#, tmp115
# sha512.c:174:       size_t add = 256 - left_over > len ? len : 256 - left_over;
	subq	%r14, %r13	# _1, tmp112
	cmpq	%rsi, %r13	# len, tmp112
	cmova	%rsi, %r13	# tmp112,, len, add
# sha512.c:176:       memcpy (&ctx->buffer[left_over], buffer, add);
	movq	%r12, %rsi	# buffer,
	movq	%r13, %rdx	# add,
	call	memcpy@PLT	#
# sha512.c:177:       ctx->buflen += add;
	movq	80(%rbp), %rsi	# ctx_28(D)->buflen, _5
	addq	%r13, %rsi	# add, _5
# sha512.c:179:       if (ctx->buflen > 128)
	cmpq	$128, %rsi	#, _5
# sha512.c:177:       ctx->buflen += add;
	movq	%rsi, 80(%rbp)	# _5, ctx_28(D)->buflen
# sha512.c:179:       if (ctx->buflen > 128)
	ja	.L60	#,
.L26:
# sha512.c:189:       buffer = (const char *) buffer + add;
	addq	%r13, %r12	# add, buffer
# sha512.c:190:       len -= add;
	subq	%r13, %rbx	# add, len
	jmp	.L25	#
	.p2align 4,,10
	.p2align 3
.L33:
# sha512.c:226:       memcpy (&ctx->buffer[left_over], buffer, len);
	movq	(%r12), %rax	#* buffer, tmp183
	leaq	8(%rdx), %rdi	#, tmp191
	movq	%r12, %rsi	# buffer, buffer
	andq	$-8, %rdi	#, tmp191
	movq	%rax, (%rdx)	# tmp183,
	movl	%ebx, %eax	# len, len
	movq	-8(%r12,%rax), %rcx	#, tmp190
	movq	%rcx, -8(%rdx,%rax)	# tmp190,
	subq	%rdi, %rdx	# tmp191, tmp163
	leal	(%rbx,%rdx), %ecx	#, len
# sha512.c:227:       left_over += len;
	addq	%r8, %rbx	# left_over, left_over
# sha512.c:226:       memcpy (&ctx->buffer[left_over], buffer, len);
	subq	%rdx, %rsi	# tmp163, buffer
	shrl	$3, %ecx	#,
# sha512.c:228:       if (left_over >= 128)
	cmpq	$127, %rbx	#, left_over
# sha512.c:226:       memcpy (&ctx->buffer[left_over], buffer, len);
	rep movsq
# sha512.c:228:       if (left_over >= 128)
	jbe	.L37	#,
.L59:
# sha512.c:230: 	  __sha512_process_block (ctx->buffer, 128, ctx);
	leaq	88(%rbp), %r12	#, _15
	movq	%rbp, %rdx	# ctx,
	movl	$128, %esi	#,
# sha512.c:231: 	  left_over -= 128;
	addq	$-128, %rbx	#, left_over
# sha512.c:230: 	  __sha512_process_block (ctx->buffer, 128, ctx);
	movq	%r12, %rdi	# _15,
	call	__sha512_process_block@PLT	#
# sha512.c:232: 	  memcpy (ctx->buffer, &ctx->buffer[128], left_over);
	leaq	216(%rbp), %rsi	#, tmp194
	movq	%rbx, %rdx	# left_over,
	movq	%r12, %rdi	# _15,
	call	memcpy@PLT	#
	jmp	.L37	#
	.p2align 4,,10
	.p2align 3
.L60:
# sha512.c:181: 	  __sha512_process_block (ctx->buffer, ctx->buflen & ~127, ctx);
	leaq	88(%rbp), %r15	#, _7
	andq	$-128, %rsi	#, tmp122
	movq	%rbp, %rdx	# ctx,
# sha512.c:185: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~127],
	addq	%r13, %r14	# add, tmp124
# sha512.c:181: 	  __sha512_process_block (ctx->buffer, ctx->buflen & ~127, ctx);
	movq	%r15, %rdi	# _7,
# sha512.c:185: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~127],
	andq	$-128, %r14	#, tmp125
# sha512.c:181: 	  __sha512_process_block (ctx->buffer, ctx->buflen & ~127, ctx);
	call	__sha512_process_block@PLT	#
# sha512.c:183: 	  ctx->buflen &= 127;
	movq	80(%rbp), %rax	# ctx_28(D)->buflen, _9
# sha512.c:185: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~127],
	leaq	88(%rbp,%r14), %rsi	#, tmp127
# sha512.c:183: 	  ctx->buflen &= 127;
	andl	$127, %eax	#, _9
# sha512.c:185: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~127],
	cmpl	$8, %eax	#, _9
# sha512.c:183: 	  ctx->buflen &= 127;
	movq	%rax, 80(%rbp)	# _9, ctx_28(D)->buflen
# sha512.c:185: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~127],
	jnb	.L27	#,
	testb	$4, %al	#, _9
	jne	.L61	#,
	testl	%eax, %eax	# _9
	je	.L26	#,
	movzbl	(%rsi), %edx	#, tmp140
	testb	$2, %al	#, _9
	movb	%dl, 88(%rbp)	# tmp140,
	je	.L26	#,
	movl	%eax, %eax	# _9, _9
	movzwl	-2(%rsi,%rax), %edx	#, tmp148
	movw	%dx, -2(%r15,%rax)	# tmp148,
	jmp	.L26	#
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rsi), %rdx	#, tmp149
	leaq	96(%rbp), %rdi	#, tmp157
	andq	$-8, %rdi	#, tmp157
	movq	%rdx, 88(%rbp)	# tmp149,
	movl	%eax, %edx	# _9, _9
	movq	-8(%rsi,%rdx), %rcx	#, tmp156
	movq	%rcx, -8(%r15,%rdx)	# tmp156,
	subq	%rdi, %r15	# tmp157, _7
	leal	(%rax,%r15), %ecx	#, _9
	subq	%r15, %rsi	# _7, tmp130
	movl	%ecx, %eax	# _9, _9
	shrl	$3, %eax	#, _9
	movl	%eax, %ecx	# tmp158, tmp159
	rep movsq
	jmp	.L26	#
.L61:
	movl	(%rsi), %edx	#, tmp132
	movl	%eax, %eax	# _9, _9
	movl	%edx, 88(%rbp)	# tmp132,
	movl	-4(%rsi,%rax), %edx	#, tmp139
	movl	%edx, -4(%r15,%rax)	# tmp139,
	jmp	.L26	#
	.p2align 4,,10
	.p2align 3
.L57:
# sha512.c:226:       memcpy (&ctx->buffer[left_over], buffer, len);
	movl	(%r12), %eax	#* buffer, tmp166
	movl	%eax, (%rdx)	# tmp166,
	movl	%ebx, %eax	# len, len
	movl	-4(%r12,%rax), %ecx	#, tmp173
	movl	%ecx, -4(%rdx,%rax)	# tmp173,
	jmp	.L34	#
.L58:
	movl	%ebx, %eax	# len, len
	movzwl	-2(%r12,%rax), %ecx	#, tmp182
	movw	%cx, -2(%rdx,%rax)	# tmp182,
	jmp	.L34	#
	.cfi_endproc
.LFE34:
	.size	__sha512_process_bytes, .-__sha512_process_bytes
	.section	.rodata
	.align 32
	.type	K, @object
	.size	K, 640
K:
	.quad	4794697086780616226
	.quad	8158064640168781261
	.quad	-5349999486874862801
	.quad	-1606136188198331460
	.quad	4131703408338449720
	.quad	6480981068601479193
	.quad	-7908458776815382629
	.quad	-6116909921290321640
	.quad	-2880145864133508542
	.quad	1334009975649890238
	.quad	2608012711638119052
	.quad	6128411473006802146
	.quad	8268148722764581231
	.quad	-9160688886553864527
	.quad	-7215885187991268811
	.quad	-4495734319001033068
	.quad	-1973867731355612462
	.quad	-1171420211273849373
	.quad	1135362057144423861
	.quad	2597628984639134821
	.quad	3308224258029322869
	.quad	5365058923640841347
	.quad	6679025012923562964
	.quad	8573033837759648693
	.quad	-7476448914759557205
	.quad	-6327057829258317296
	.quad	-5763719355590565569
	.quad	-4658551843659510044
	.quad	-4116276920077217854
	.quad	-3051310485924567259
	.quad	489312712824947311
	.quad	1452737877330783856
	.quad	2861767655752347644
	.quad	3322285676063803686
	.quad	5560940570517711597
	.quad	5996557281743188959
	.quad	7280758554555802590
	.quad	8532644243296465576
	.quad	-9096487096722542874
	.quad	-7894198246740708037
	.quad	-6719396339535248540
	.quad	-6333637450476146687
	.quad	-4446306890439682159
	.quad	-4076793802049405392
	.quad	-3345356375505022440
	.quad	-2983346525034927856
	.quad	-860691631967231958
	.quad	1182934255886127544
	.quad	1847814050463011016
	.quad	2177327727835720531
	.quad	2830643537854262169
	.quad	3796741975233480872
	.quad	4115178125766777443
	.quad	5681478168544905931
	.quad	6601373596472566643
	.quad	7507060721942968483
	.quad	8399075790359081724
	.quad	8693463985226723168
	.quad	-8878714635349349518
	.quad	-8302665154208450068
	.quad	-8016688836872298968
	.quad	-6606660893046293015
	.quad	-4685533653050689259
	.quad	-4147400797238176981
	.quad	-3880063495543823972
	.quad	-3348786107499101689
	.quad	-1523767162380948706
	.quad	-757361751448694408
	.quad	500013540394364858
	.quad	748580250866718886
	.quad	1242879168328830382
	.quad	1977374033974150939
	.quad	2944078676154940804
	.quad	3659926193048069267
	.quad	4368137639120453308
	.quad	4836135668995329356
	.quad	5532061633213252278
	.quad	6448918945643986474
	.quad	6902733635092675308
	.quad	7801388544844847127
	.align 32
	.type	fillbuf, @object
	.size	fillbuf, 128
fillbuf:
	.byte	-128
	.byte	0
	.zero	126
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
