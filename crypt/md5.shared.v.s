	.file	"md5.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/md5.shared.v.d -MF /run/asm/crypt/md5.os.dt -MP
# -MT /run/asm/crypt/.os -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC
# -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h md5.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/crypt/md5.shared.v.s -O2 -Wall -Wwrite-strings
# -Wundef -Werror -Wstrict-prototypes -Wold-style-definition -std=gnu11
# -fverbose-asm -fgnu89-inline -fmerge-all-constants -frounding-math
# -fno-stack-protector -fmath-errno -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.globl	__md5_init_ctx
	.type	__md5_init_ctx, @function
__md5_init_ctx:
.LFB32:
	.cfi_startproc
# md5.c:72:   ctx->A = 0x67452301;
	movabsq	$-1167088121787636991, %rax	#, tmp90
# md5.c:75:   ctx->D = 0x10325476;
	movq	$0, 16(%rdi)	#, MEM[(unsigned int *)ctx_2(D) + 16B]
# md5.c:77:   ctx->total[0] = ctx->total[1] = 0;
	movl	$0, 24(%rdi)	#, MEM[(unsigned int *)ctx_2(D) + 24B]
# md5.c:72:   ctx->A = 0x67452301;
	movq	%rax, (%rdi)	# tmp90, MEM[(unsigned int *)ctx_2(D)]
# md5.c:73:   ctx->B = 0xefcdab89;
	movabsq	$1167088121787636990, %rax	#, tmp91
	movq	%rax, 8(%rdi)	# tmp91, MEM[(unsigned int *)ctx_2(D) + 8B]
# md5.c:79: }
	ret
	.cfi_endproc
.LFE32:
	.size	__md5_init_ctx, .-__md5_init_ctx
	.p2align 4,,15
	.globl	__md5_read_ctx
	.type	__md5_read_ctx, @function
__md5_read_ctx:
.LFB33:
	.cfi_startproc
# md5.c:89:   ((md5_uint32 *) resbuf)[0] = SWAP (ctx->A);
	movl	(%rdi), %edx	# ctx_6(D)->A, _1
# md5.c:88: {
	movq	%rsi, %rax	# resbuf, resbuf
# md5.c:89:   ((md5_uint32 *) resbuf)[0] = SWAP (ctx->A);
	movl	%edx, (%rsi)	# _1, MEM[(md5_uint32 *)resbuf_7(D)]
# md5.c:90:   ((md5_uint32 *) resbuf)[1] = SWAP (ctx->B);
	movl	4(%rdi), %edx	# ctx_6(D)->B, _2
	movl	%edx, 4(%rsi)	# _2, MEM[(md5_uint32 *)resbuf_7(D) + 4B]
# md5.c:91:   ((md5_uint32 *) resbuf)[2] = SWAP (ctx->C);
	movl	8(%rdi), %edx	# ctx_6(D)->C, _3
	movl	%edx, 8(%rsi)	# _3, MEM[(md5_uint32 *)resbuf_7(D) + 8B]
# md5.c:92:   ((md5_uint32 *) resbuf)[3] = SWAP (ctx->D);
	movl	12(%rdi), %edx	# ctx_6(D)->D, _4
	movl	%edx, 12(%rsi)	# _4, MEM[(md5_uint32 *)resbuf_7(D) + 12B]
# md5.c:95: }
	ret
	.cfi_endproc
.LFE33:
	.size	__md5_read_ctx, .-__md5_read_ctx
	.p2align 4,,15
	.globl	__md5_process_block
	.type	__md5_process_block, @function
__md5_process_block:
.LFB38:
	.cfi_startproc
# ./md5-block.c:19:   const md5_uint32 *endp = words + nwords;
	movq	%rsi, %rax	# len, tmp644
# ./md5-block.c:15: {
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
# ./md5-block.c:19:   const md5_uint32 *endp = words + nwords;
	andq	$-4, %rax	#, tmp644
# ./md5-block.c:15: {
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
# ./md5-block.c:15: {
	movq	%rdi, %rbx	# buffer, buffer
# ./md5-block.c:19:   const md5_uint32 *endp = words + nwords;
	leaq	(%rbx,%rax), %rcx	#, endp
# ./md5-block.c:20:   md5_uint32 A = ctx->A;
	movl	(%rdx), %eax	# ctx_326(D)->A, A
# ./md5-block.c:15: {
	movq	%rdx, %rdi	# ctx, ctx
# ./md5-block.c:21:   md5_uint32 B = ctx->B;
	movl	4(%rdx), %r11d	# ctx_326(D)->B, B
# ./md5-block.c:22:   md5_uint32 C = ctx->C;
	movl	8(%rdx), %r10d	# ctx_326(D)->C, C
# ./md5-block.c:15: {
	movq	%rdx, -8(%rsp)	# ctx, %sfp
# ./md5-block.c:19:   const md5_uint32 *endp = words + nwords;
	movq	%rcx, -16(%rsp)	# endp, %sfp
# ./md5-block.c:20:   md5_uint32 A = ctx->A;
	movl	%eax, -24(%rsp)	# A, %sfp
# ./md5-block.c:23:   md5_uint32 D = ctx->D;
	movl	12(%rdx), %eax	# ctx_326(D)->D, D
	movl	%esi, %edx	# len, tmp645
	movl	%eax, -32(%rsp)	# D, %sfp
	xorl	%eax, %eax	# _403
	addl	16(%rdi), %edx	# ctx_326(D)->total, tmp645
	setc	%al	#, _403
# ./md5-block.c:30:   ctx->total[1] += (len >> 31 >> 1) + (ctx->total[0] < lolen);
	shrq	$32, %rsi	#, tmp647
	addl	20(%rdi), %esi	# ctx_326(D)->total, tmp648
# ./md5-block.c:29:   ctx->total[0] += lolen;
	movl	%edx, 16(%rdi)	# tmp645, ctx_326(D)->total
# ./md5-block.c:30:   ctx->total[1] += (len >> 31 >> 1) + (ctx->total[0] < lolen);
	addl	%eax, %esi	# _403, tmp652
# ./md5-block.c:34:   while (words < endp)
	cmpq	%rcx, %rbx	# endp, buffer
# ./md5-block.c:30:   ctx->total[1] += (len >> 31 >> 1) + (ctx->total[0] < lolen);
	movl	%esi, 20(%rdi)	# tmp652, ctx_326(D)->total
# ./md5-block.c:34:   while (words < endp)
	jnb	.L7	#,
	movl	%r10d, %r15d	# C, C
	.p2align 4,,10
	.p2align 3
.L8:
# ./md5-block.c:70:       OP (A, B, C, D,  7, 0xd76aa478);
	movl	(%rbx), %r14d	# MEM[base: words_556, offset: 0B], _15
	movl	-32(%rsp), %edi	# %sfp, D
	movl	-24(%rsp), %eax	# %sfp, A
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	movl	4(%rbx), %esi	# MEM[base: words_556, offset: 4B], _20
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	movl	12(%rbx), %r8d	# MEM[base: words_556, offset: 12B], _30
# ./md5-block.c:75:       OP (D, A, B, C, 12, 0x4787c62a);
	movl	20(%rbx), %r10d	# MEM[base: words_556, offset: 20B], _40
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	movl	24(%rbx), %ebp	# MEM[base: words_556, offset: 24B], _45
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	movl	28(%rbx), %r13d	# MEM[base: words_556, offset: 28B], _50
	leal	-680876936(%r14,%rax), %edx	#, _696
# ./md5-block.c:70:       OP (A, B, C, D,  7, 0xd76aa478);
	movl	%edi, %eax	# D, tmp654
	leal	-389564586(%rsi,%rdi), %ecx	#, _694
	xorl	%r15d, %eax	# C, tmp654
	movl	%esi, -68(%rsp)	# _20, %sfp
	movl	%r10d, -52(%rsp)	# _40, %sfp
	andl	%r11d, %eax	# B, tmp655
# ./md5-block.c:79:       OP (D, A, B, C, 12, 0x8b44f7af);
	movl	36(%rbx), %r12d	# MEM[base: words_556, offset: 36B], _60
	movl	%r8d, -60(%rsp)	# _30, %sfp
# ./md5-block.c:70:       OP (A, B, C, D,  7, 0xd76aa478);
	xorl	%edi, %eax	# D, tmp656
	leal	-1044525330(%r8,%r11), %edi	#, _690
# ./md5-block.c:81:       OP (B, C, D, A, 22, 0x895cd7be);
	movl	44(%rbx), %r8d	# MEM[base: words_556, offset: 44B], _70
# ./md5-block.c:70:       OP (A, B, C, D,  7, 0xd76aa478);
	addl	%edx, %eax	# _696, A
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	movl	%r11d, %edx	# B, tmp658
# ./md5-block.c:70:       OP (A, B, C, D,  7, 0xd76aa478);
	roll	$7, %eax	#, A
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	xorl	%r15d, %edx	# C, tmp658
	movl	%r12d, -44(%rsp)	# _60, %sfp
# ./md5-block.c:70:       OP (A, B, C, D,  7, 0xd76aa478);
	addl	%r11d, %eax	# B, A
	movl	%r8d, -36(%rsp)	# _70, %sfp
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	andl	%eax, %edx	# A, tmp659
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	movl	%eax, %r9d	# A, tmp662
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	xorl	%r15d, %edx	# C, tmp660
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	xorl	%r11d, %r9d	# B, tmp662
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	addl	%ecx, %edx	# _694, D
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	movl	8(%rbx), %ecx	# MEM[base: words_556, offset: 8B], _25
# ./md5-block.c:71:       OP (D, A, B, C, 12, 0xe8c7b756);
	roll	$12, %edx	#, D
	addl	%eax, %edx	# A, D
	leal	606105819(%rcx,%r15), %esi	#, _692
	movl	%ecx, -64(%rsp)	# _25, %sfp
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	movl	%r9d, %ecx	# tmp662, tmp662
	andl	%edx, %ecx	# D, tmp663
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	movl	%eax, %r9d	# A, tmp666
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	xorl	%r11d, %ecx	# B, tmp664
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	xorl	%edx, %r9d	# D, tmp666
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	addl	%esi, %ecx	# _692, C
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	movl	%r9d, %esi	# tmp666, tmp666
# ./md5-block.c:74:       OP (A, B, C, D,  7, 0xf57c0faf);
	movl	16(%rbx), %r9d	# MEM[base: words_556, offset: 16B], _35
# ./md5-block.c:72:       OP (C, D, A, B, 17, 0x242070db);
	rorl	$15, %ecx	#, C
	addl	%edx, %ecx	# D, C
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	andl	%ecx, %esi	# C, tmp667
	movl	%r9d, -56(%rsp)	# _35, %sfp
	xorl	%eax, %esi	# A, tmp668
	addl	%edi, %esi	# _690, B
	leal	-176418897(%r9,%rax), %edi	#, _688
# ./md5-block.c:74:       OP (A, B, C, D,  7, 0xf57c0faf);
	movl	%edx, %eax	# D, tmp670
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	rorl	$10, %esi	#, B
# ./md5-block.c:74:       OP (A, B, C, D,  7, 0xf57c0faf);
	xorl	%ecx, %eax	# C, tmp670
# ./md5-block.c:85:       OP (B, C, D, A, 22, 0x49b40821);
	movl	60(%rbx), %r9d	# MEM[base: words_556, offset: 60B], _90
# ./md5-block.c:73:       OP (B, C, D, A, 22, 0xc1bdceee);
	addl	%ecx, %esi	# C, B
# ./md5-block.c:74:       OP (A, B, C, D,  7, 0xf57c0faf);
	andl	%esi, %eax	# B, tmp671
	xorl	%edx, %eax	# D, tmp672
	addl	%edi, %eax	# _688, A
	leal	1200080426(%r10,%rdx), %edi	#, _686
# ./md5-block.c:75:       OP (D, A, B, C, 12, 0x4787c62a);
	movl	%ecx, %edx	# C, tmp674
# ./md5-block.c:74:       OP (A, B, C, D,  7, 0xf57c0faf);
	roll	$7, %eax	#, A
# ./md5-block.c:75:       OP (D, A, B, C, 12, 0x4787c62a);
	xorl	%esi, %edx	# B, tmp674
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	movl	%ebp, %r10d	# _45, _45
# ./md5-block.c:74:       OP (A, B, C, D,  7, 0xf57c0faf);
	addl	%esi, %eax	# B, A
	movl	%r10d, -28(%rsp)	# _45, %sfp
# ./md5-block.c:75:       OP (D, A, B, C, 12, 0x4787c62a);
	andl	%eax, %edx	# A, tmp675
	xorl	%ecx, %edx	# C, tmp676
	addl	%edi, %edx	# _686, D
	leal	-1473231341(%rbp,%rcx), %edi	#, _684
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	movl	%esi, %ebp	# B, tmp678
	xorl	%eax, %ebp	# A, tmp678
# ./md5-block.c:75:       OP (D, A, B, C, 12, 0x4787c62a);
	roll	$12, %edx	#, D
	addl	%eax, %edx	# A, D
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	movl	%ebp, %ecx	# tmp678, tmp678
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	movl	%eax, %ebp	# A, tmp682
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	andl	%edx, %ecx	# D, tmp679
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	xorl	%edx, %ebp	# D, tmp682
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	xorl	%esi, %ecx	# B, tmp680
	addl	%edi, %ecx	# _684, C
	leal	-45705983(%r13,%rsi), %edi	#, _682
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	movl	%ebp, %esi	# tmp682, tmp682
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	rorl	$15, %ecx	#, C
# ./md5-block.c:78:       OP (A, B, C, D,  7, 0x698098d8);
	movl	32(%rbx), %ebp	# MEM[base: words_556, offset: 32B], _55
# ./md5-block.c:76:       OP (C, D, A, B, 17, 0xa8304613);
	addl	%edx, %ecx	# D, C
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	andl	%ecx, %esi	# C, tmp683
	xorl	%eax, %esi	# A, tmp684
	movl	%ebp, -48(%rsp)	# _55, %sfp
	addl	%edi, %esi	# _682, B
	leal	1770035416(%rbp,%rax), %edi	#, _680
# ./md5-block.c:78:       OP (A, B, C, D,  7, 0x698098d8);
	movl	%edx, %eax	# D, tmp686
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	rorl	$10, %esi	#, B
# ./md5-block.c:78:       OP (A, B, C, D,  7, 0x698098d8);
	xorl	%ecx, %eax	# C, tmp686
# ./md5-block.c:82:       OP (A, B, C, D,  7, 0x6b901122);
	movl	48(%rbx), %ebp	# MEM[base: words_556, offset: 48B], _75
# ./md5-block.c:77:       OP (B, C, D, A, 22, 0xfd469501);
	addl	%ecx, %esi	# C, B
# ./md5-block.c:78:       OP (A, B, C, D,  7, 0x698098d8);
	andl	%esi, %eax	# B, tmp687
	xorl	%edx, %eax	# D, tmp688
	addl	%edi, %eax	# _680, A
	leal	-1958414417(%r12,%rdx), %edi	#, _678
# ./md5-block.c:79:       OP (D, A, B, C, 12, 0x8b44f7af);
	movl	%ecx, %edx	# C, tmp690
# ./md5-block.c:78:       OP (A, B, C, D,  7, 0x698098d8);
	roll	$7, %eax	#, A
# ./md5-block.c:79:       OP (D, A, B, C, 12, 0x8b44f7af);
	xorl	%esi, %edx	# B, tmp690
# ./md5-block.c:84:       OP (C, D, A, B, 17, 0xa679438e);
	movl	56(%rbx), %r12d	# MEM[base: words_556, offset: 56B], _85
# ./md5-block.c:78:       OP (A, B, C, D,  7, 0x698098d8);
	addl	%esi, %eax	# B, A
# ./md5-block.c:79:       OP (D, A, B, C, 12, 0x8b44f7af);
	andl	%eax, %edx	# A, tmp691
	xorl	%ecx, %edx	# C, tmp692
	addl	%edi, %edx	# _678, D
# ./md5-block.c:80:       OP (C, D, A, B, 17, 0xffff5bb1);
	movl	40(%rbx), %edi	# MEM[base: words_556, offset: 40B], _65
# ./md5-block.c:79:       OP (D, A, B, C, 12, 0x8b44f7af);
	roll	$12, %edx	#, D
	addl	%eax, %edx	# A, D
	movl	%edi, -40(%rsp)	# _65, %sfp
	leal	-42063(%rdi,%rcx), %edi	#, _676
# ./md5-block.c:80:       OP (C, D, A, B, 17, 0xffff5bb1);
	movl	%esi, %ecx	# B, tmp694
	xorl	%eax, %ecx	# A, tmp694
	andl	%edx, %ecx	# D, tmp695
	xorl	%esi, %ecx	# B, tmp696
	addl	%edi, %ecx	# _676, C
	leal	-1990404162(%r8,%rsi), %edi	#, _674
# ./md5-block.c:81:       OP (B, C, D, A, 22, 0x895cd7be);
	movl	%eax, %esi	# A, tmp698
# ./md5-block.c:80:       OP (C, D, A, B, 17, 0xffff5bb1);
	rorl	$15, %ecx	#, C
# ./md5-block.c:81:       OP (B, C, D, A, 22, 0x895cd7be);
	xorl	%edx, %esi	# D, tmp698
# ./md5-block.c:83:       OP (D, A, B, C, 12, 0xfd987193);
	movl	52(%rbx), %r8d	# MEM[base: words_556, offset: 52B], _80
# ./md5-block.c:80:       OP (C, D, A, B, 17, 0xffff5bb1);
	addl	%edx, %ecx	# D, C
	addq	$64, %rbx	#, buffer
# ./md5-block.c:81:       OP (B, C, D, A, 22, 0x895cd7be);
	andl	%ecx, %esi	# C, tmp699
	xorl	%eax, %esi	# A, tmp700
	addl	%edi, %esi	# _674, B
	leal	1804603682(%rbp,%rax), %edi	#, _672
# ./md5-block.c:82:       OP (A, B, C, D,  7, 0x6b901122);
	movl	%edx, %eax	# D, tmp702
# ./md5-block.c:81:       OP (B, C, D, A, 22, 0x895cd7be);
	rorl	$10, %esi	#, B
# ./md5-block.c:82:       OP (A, B, C, D,  7, 0x6b901122);
	xorl	%ecx, %eax	# C, tmp702
# ./md5-block.c:81:       OP (B, C, D, A, 22, 0x895cd7be);
	addl	%ecx, %esi	# C, B
# ./md5-block.c:82:       OP (A, B, C, D,  7, 0x6b901122);
	andl	%esi, %eax	# B, tmp703
	xorl	%edx, %eax	# D, tmp704
	addl	%edi, %eax	# _672, A
	leal	-40341101(%r8,%rdx), %edi	#, _670
# ./md5-block.c:83:       OP (D, A, B, C, 12, 0xfd987193);
	movl	%ecx, %edx	# C, tmp706
# ./md5-block.c:82:       OP (A, B, C, D,  7, 0x6b901122);
	roll	$7, %eax	#, A
# ./md5-block.c:83:       OP (D, A, B, C, 12, 0xfd987193);
	xorl	%esi, %edx	# B, tmp706
# ./md5-block.c:82:       OP (A, B, C, D,  7, 0x6b901122);
	addl	%esi, %eax	# B, A
# ./md5-block.c:83:       OP (D, A, B, C, 12, 0xfd987193);
	andl	%eax, %edx	# A, tmp707
	xorl	%ecx, %edx	# C, tmp708
	addl	%edi, %edx	# _670, D
	leal	-1502002290(%r12,%rcx), %edi	#, _668
# ./md5-block.c:84:       OP (C, D, A, B, 17, 0xa679438e);
	movl	%esi, %ecx	# B, tmp710
# ./md5-block.c:83:       OP (D, A, B, C, 12, 0xfd987193);
	roll	$12, %edx	#, D
# ./md5-block.c:84:       OP (C, D, A, B, 17, 0xa679438e);
	xorl	%eax, %ecx	# A, tmp710
# ./md5-block.c:83:       OP (D, A, B, C, 12, 0xfd987193);
	addl	%eax, %edx	# A, D
# ./md5-block.c:84:       OP (C, D, A, B, 17, 0xa679438e);
	andl	%edx, %ecx	# D, tmp711
	xorl	%esi, %ecx	# B, tmp712
	addl	%edi, %ecx	# _668, C
	leal	1236535329(%r9,%rsi), %edi	#, _666
# ./md5-block.c:85:       OP (B, C, D, A, 22, 0x49b40821);
	movl	%eax, %esi	# A, tmp714
# ./md5-block.c:84:       OP (C, D, A, B, 17, 0xa679438e);
	rorl	$15, %ecx	#, C
# ./md5-block.c:85:       OP (B, C, D, A, 22, 0x49b40821);
	xorl	%edx, %esi	# D, tmp714
# ./md5-block.c:84:       OP (C, D, A, B, 17, 0xa679438e);
	addl	%edx, %ecx	# D, C
# ./md5-block.c:85:       OP (B, C, D, A, 22, 0x49b40821);
	andl	%ecx, %esi	# C, tmp715
	xorl	%eax, %esi	# A, tmp716
	addl	%edi, %esi	# _666, B
	movl	-68(%rsp), %edi	# %sfp, _20
	rorl	$10, %esi	#, B
	addl	%ecx, %esi	# C, B
	leal	-165796510(%rdi,%rax), %edi	#, _664
# ./md5-block.c:101:       OP (FG, A, B, C, D,  1,  5, 0xf61e2562);
	movl	%ecx, %eax	# C, tmp718
	xorl	%esi, %eax	# B, tmp718
	andl	%edx, %eax	# D, tmp719
	xorl	%ecx, %eax	# C, tmp720
	addl	%edi, %eax	# _664, A
	leal	-1069501632(%r10,%rdx), %edi	#, _662
# ./md5-block.c:102:       OP (FG, D, A, B, C,  6,  9, 0xc040b340);
	movl	%esi, %edx	# B, tmp722
# ./md5-block.c:101:       OP (FG, A, B, C, D,  1,  5, 0xf61e2562);
	roll	$5, %eax	#, A
	addl	%esi, %eax	# B, A
# ./md5-block.c:102:       OP (FG, D, A, B, C,  6,  9, 0xc040b340);
	xorl	%eax, %edx	# A, tmp722
	andl	%ecx, %edx	# C, tmp723
	xorl	%esi, %edx	# B, tmp724
	addl	%edi, %edx	# _662, D
	movl	-36(%rsp), %edi	# %sfp, _70
	roll	$9, %edx	#, D
	addl	%eax, %edx	# A, D
	leal	643717713(%rdi,%rcx), %edi	#, _660
# ./md5-block.c:103:       OP (FG, C, D, A, B, 11, 14, 0x265e5a51);
	movl	%eax, %ecx	# A, tmp726
	xorl	%edx, %ecx	# D, tmp726
	andl	%esi, %ecx	# B, tmp727
	xorl	%eax, %ecx	# A, tmp728
	addl	%edi, %ecx	# _660, C
	leal	-373897302(%r14,%rsi), %edi	#, _658
# ./md5-block.c:104:       OP (FG, B, C, D, A,  0, 20, 0xe9b6c7aa);
	movl	%edx, %esi	# D, tmp730
# ./md5-block.c:103:       OP (FG, C, D, A, B, 11, 14, 0x265e5a51);
	roll	$14, %ecx	#, C
	addl	%edx, %ecx	# D, C
# ./md5-block.c:104:       OP (FG, B, C, D, A,  0, 20, 0xe9b6c7aa);
	xorl	%ecx, %esi	# C, tmp730
	andl	%eax, %esi	# A, tmp731
	xorl	%edx, %esi	# D, tmp732
	addl	%edi, %esi	# _658, B
	movl	-52(%rsp), %edi	# %sfp, _40
	rorl	$12, %esi	#, B
	addl	%ecx, %esi	# C, B
	leal	-701558691(%rdi,%rax), %edi	#, _656
# ./md5-block.c:105:       OP (FG, A, B, C, D,  5,  5, 0xd62f105d);
	movl	%ecx, %eax	# C, tmp734
	xorl	%esi, %eax	# B, tmp734
	andl	%edx, %eax	# D, tmp735
	xorl	%ecx, %eax	# C, tmp736
	addl	%edi, %eax	# _656, A
	movl	-40(%rsp), %edi	# %sfp, _65
	roll	$5, %eax	#, A
	addl	%esi, %eax	# B, A
	leal	38016083(%rdi,%rdx), %edi	#, _654
# ./md5-block.c:106:       OP (FG, D, A, B, C, 10,  9, 0x02441453);
	movl	%esi, %edx	# B, tmp738
	xorl	%eax, %edx	# A, tmp738
	andl	%ecx, %edx	# C, tmp739
	xorl	%esi, %edx	# B, tmp740
	addl	%edi, %edx	# _654, D
	leal	-660478335(%r9,%rcx), %edi	#, _652
# ./md5-block.c:107:       OP (FG, C, D, A, B, 15, 14, 0xd8a1e681);
	movl	%eax, %ecx	# A, tmp742
# ./md5-block.c:106:       OP (FG, D, A, B, C, 10,  9, 0x02441453);
	roll	$9, %edx	#, D
	addl	%eax, %edx	# A, D
# ./md5-block.c:107:       OP (FG, C, D, A, B, 15, 14, 0xd8a1e681);
	xorl	%edx, %ecx	# D, tmp742
	andl	%esi, %ecx	# B, tmp743
	xorl	%eax, %ecx	# A, tmp744
	addl	%edi, %ecx	# _652, C
	movl	-56(%rsp), %edi	# %sfp, _35
	movl	-44(%rsp), %r10d	# %sfp, _60
	roll	$14, %ecx	#, C
	addl	%edx, %ecx	# D, C
	leal	-405537848(%rdi,%rsi), %edi	#, _650
# ./md5-block.c:108:       OP (FG, B, C, D, A,  4, 20, 0xe7d3fbc8);
	movl	%edx, %esi	# D, tmp746
	xorl	%ecx, %esi	# C, tmp746
	andl	%eax, %esi	# A, tmp747
	xorl	%edx, %esi	# D, tmp748
	addl	%edi, %esi	# _650, B
	leal	568446438(%r10,%rax), %edi	#, _648
# ./md5-block.c:109:       OP (FG, A, B, C, D,  9,  5, 0x21e1cde6);
	movl	%ecx, %eax	# C, tmp750
# ./md5-block.c:108:       OP (FG, B, C, D, A,  4, 20, 0xe7d3fbc8);
	rorl	$12, %esi	#, B
	movl	-48(%rsp), %r10d	# %sfp, _55
	addl	%ecx, %esi	# C, B
# ./md5-block.c:109:       OP (FG, A, B, C, D,  9,  5, 0x21e1cde6);
	xorl	%esi, %eax	# B, tmp750
	andl	%edx, %eax	# D, tmp751
	xorl	%ecx, %eax	# C, tmp752
	addl	%edi, %eax	# _648, A
	leal	-1019803690(%r12,%rdx), %edi	#, _646
# ./md5-block.c:110:       OP (FG, D, A, B, C, 14,  9, 0xc33707d6);
	movl	%esi, %edx	# B, tmp754
# ./md5-block.c:109:       OP (FG, A, B, C, D,  9,  5, 0x21e1cde6);
	roll	$5, %eax	#, A
	addl	%esi, %eax	# B, A
# ./md5-block.c:110:       OP (FG, D, A, B, C, 14,  9, 0xc33707d6);
	xorl	%eax, %edx	# A, tmp754
	andl	%ecx, %edx	# C, tmp755
	xorl	%esi, %edx	# B, tmp756
	addl	%edi, %edx	# _646, D
	movl	-60(%rsp), %edi	# %sfp, _30
	roll	$9, %edx	#, D
	addl	%eax, %edx	# A, D
	leal	-187363961(%rdi,%rcx), %edi	#, _644
# ./md5-block.c:111:       OP (FG, C, D, A, B,  3, 14, 0xf4d50d87);
	movl	%eax, %ecx	# A, tmp758
	xorl	%edx, %ecx	# D, tmp758
	andl	%esi, %ecx	# B, tmp759
	leal	1163531501(%r10,%rsi), %esi	#, _642
# ./md5-block.c:112:       OP (FG, B, C, D, A,  8, 20, 0x455a14ed);
	movl	%edx, %r10d	# D, tmp762
# ./md5-block.c:111:       OP (FG, C, D, A, B,  3, 14, 0xf4d50d87);
	xorl	%eax, %ecx	# A, tmp760
	addl	%edi, %ecx	# _644, C
	roll	$14, %ecx	#, C
	addl	%edx, %ecx	# D, C
# ./md5-block.c:112:       OP (FG, B, C, D, A,  8, 20, 0x455a14ed);
	xorl	%ecx, %r10d	# C, tmp762
	movl	%r10d, %edi	# tmp762, tmp762
	andl	%eax, %edi	# A, tmp763
	xorl	%edx, %edi	# D, tmp764
	addl	%esi, %edi	# _642, B
	leal	-1444681467(%r8,%rax), %esi	#, _640
# ./md5-block.c:113:       OP (FG, A, B, C, D, 13,  5, 0xa9e3e905);
	movl	%ecx, %eax	# C, tmp766
# ./md5-block.c:112:       OP (FG, B, C, D, A,  8, 20, 0x455a14ed);
	rorl	$12, %edi	#, B
	addl	%ecx, %edi	# C, B
# ./md5-block.c:113:       OP (FG, A, B, C, D, 13,  5, 0xa9e3e905);
	xorl	%edi, %eax	# B, tmp766
	leal	-1926607734(%rbp,%rdi), %r10d	#, _634
	andl	%edx, %eax	# D, tmp767
	xorl	%ecx, %eax	# C, tmp768
	movl	%r10d, -20(%rsp)	# _634, %sfp
	addl	%esi, %eax	# _640, A
	movl	-64(%rsp), %esi	# %sfp, _25
	roll	$5, %eax	#, A
	addl	%edi, %eax	# B, A
	leal	-51403784(%rsi,%rdx), %esi	#, _638
# ./md5-block.c:114:       OP (FG, D, A, B, C,  2,  9, 0xfcefa3f8);
	movl	%edi, %edx	# B, tmp770
	xorl	%eax, %edx	# A, tmp770
	andl	%ecx, %edx	# C, tmp771
	leal	1735328473(%r13,%rcx), %ecx	#, _636
	xorl	%edi, %edx	# B, tmp772
	addl	%esi, %edx	# _638, D
# ./md5-block.c:115:       OP (FG, C, D, A, B,  7, 14, 0x676f02d9);
	movl	%eax, %esi	# A, tmp774
# ./md5-block.c:114:       OP (FG, D, A, B, C,  2,  9, 0xfcefa3f8);
	roll	$9, %edx	#, D
	addl	%eax, %edx	# A, D
# ./md5-block.c:115:       OP (FG, C, D, A, B,  7, 14, 0x676f02d9);
	xorl	%edx, %esi	# D, tmp774
	andl	%edi, %esi	# B, tmp775
	xorl	%eax, %esi	# A, tmp776
	addl	%ecx, %esi	# _636, C
# ./md5-block.c:116:       OP (FG, B, C, D, A, 12, 20, 0x8d2a4c8a);
	movl	%edx, %ecx	# D, _167
# ./md5-block.c:115:       OP (FG, C, D, A, B,  7, 14, 0x676f02d9);
	roll	$14, %esi	#, C
	addl	%edx, %esi	# D, C
# ./md5-block.c:116:       OP (FG, B, C, D, A, 12, 20, 0x8d2a4c8a);
	xorl	%esi, %ecx	# C, _167
	movl	%ecx, %r10d	# _167, tmp778
	andl	%eax, %r10d	# A, tmp778
	movl	%r10d, %edi	# tmp778, tmp778
	movl	-52(%rsp), %r10d	# %sfp, _40
	xorl	%edx, %edi	# D, tmp779
	addl	-20(%rsp), %edi	# %sfp, B
	leal	-378558(%r10,%rax), %eax	#, _631
	rorl	$12, %edi	#, B
	addl	%esi, %edi	# C, B
# ./md5-block.c:119:       OP (FH, A, B, C, D,  5,  4, 0xfffa3942);
	xorl	%edi, %ecx	# B, tmp781
	addl	%eax, %ecx	# _631, A
	movl	-48(%rsp), %eax	# %sfp, _55
	roll	$4, %ecx	#, A
	addl	%edi, %ecx	# B, A
	leal	-2022574463(%rax,%rdx), %eax	#, _628
# ./md5-block.c:120:       OP (FH, D, A, B, C,  8, 11, 0x8771f681);
	movl	%esi, %edx	# C, tmp783
	xorl	%edi, %edx	# B, tmp783
	xorl	%ecx, %edx	# A, tmp784
	addl	%eax, %edx	# _628, D
	movl	-36(%rsp), %eax	# %sfp, _70
	roll	$11, %edx	#, D
	addl	%ecx, %edx	# A, D
	leal	1839030562(%rax,%rsi), %eax	#, _625
# ./md5-block.c:121:       OP (FH, C, D, A, B, 11, 16, 0x6d9d6122);
	movl	%edi, %esi	# B, tmp786
	leal	-35309556(%r12,%rdi), %edi	#, _622
	xorl	%ecx, %esi	# A, tmp786
	xorl	%edx, %esi	# D, tmp787
	addl	%eax, %esi	# _625, C
# ./md5-block.c:122:       OP (FH, B, C, D, A, 14, 23, 0xfde5380c);
	movl	%ecx, %eax	# A, tmp789
# ./md5-block.c:121:       OP (FH, C, D, A, B, 11, 16, 0x6d9d6122);
	roll	$16, %esi	#, C
# ./md5-block.c:122:       OP (FH, B, C, D, A, 14, 23, 0xfde5380c);
	xorl	%edx, %eax	# D, tmp789
# ./md5-block.c:121:       OP (FH, C, D, A, B, 11, 16, 0x6d9d6122);
	addl	%edx, %esi	# D, C
# ./md5-block.c:122:       OP (FH, B, C, D, A, 14, 23, 0xfde5380c);
	xorl	%esi, %eax	# C, tmp790
# ./md5-block.c:124:       OP (FH, D, A, B, C,  4, 11, 0x4bdecfa9);
	movl	%esi, %r10d	# C, tmp795
# ./md5-block.c:122:       OP (FH, B, C, D, A, 14, 23, 0xfde5380c);
	addl	%edi, %eax	# _622, B
	movl	-68(%rsp), %edi	# %sfp, _20
	rorl	$9, %eax	#, B
	addl	%esi, %eax	# C, B
	leal	-1530992060(%rdi,%rcx), %edi	#, _619
# ./md5-block.c:123:       OP (FH, A, B, C, D,  1,  4, 0xa4beea44);
	movl	%edx, %ecx	# D, tmp792
# ./md5-block.c:124:       OP (FH, D, A, B, C,  4, 11, 0x4bdecfa9);
	xorl	%eax, %r10d	# B, tmp795
# ./md5-block.c:123:       OP (FH, A, B, C, D,  1,  4, 0xa4beea44);
	xorl	%esi, %ecx	# C, tmp792
	leal	-155497632(%r13,%rsi), %esi	#, _613
	xorl	%eax, %ecx	# B, tmp793
	addl	%edi, %ecx	# _619, A
	movl	-56(%rsp), %edi	# %sfp, _35
	roll	$4, %ecx	#, A
	addl	%eax, %ecx	# B, A
	leal	1272893353(%rdi,%rdx), %edx	#, _616
# ./md5-block.c:124:       OP (FH, D, A, B, C,  4, 11, 0x4bdecfa9);
	movl	%r10d, %edi	# tmp795, tmp795
	xorl	%ecx, %edi	# A, tmp796
	addl	%edx, %edi	# _616, D
# ./md5-block.c:125:       OP (FH, C, D, A, B,  7, 16, 0xf6bb4b60);
	movl	%eax, %edx	# B, tmp798
# ./md5-block.c:124:       OP (FH, D, A, B, C,  4, 11, 0x4bdecfa9);
	roll	$11, %edi	#, D
# ./md5-block.c:125:       OP (FH, C, D, A, B,  7, 16, 0xf6bb4b60);
	xorl	%ecx, %edx	# A, tmp798
# ./md5-block.c:124:       OP (FH, D, A, B, C,  4, 11, 0x4bdecfa9);
	addl	%ecx, %edi	# A, D
# ./md5-block.c:125:       OP (FH, C, D, A, B,  7, 16, 0xf6bb4b60);
	xorl	%edi, %edx	# D, tmp799
	addl	%esi, %edx	# _613, C
	movl	-40(%rsp), %esi	# %sfp, _65
	roll	$16, %edx	#, C
	addl	%edi, %edx	# D, C
	leal	-1094730640(%rsi,%rax), %eax	#, _610
# ./md5-block.c:126:       OP (FH, B, C, D, A, 10, 23, 0xbebfbc70);
	movl	%ecx, %esi	# A, tmp801
	xorl	%edi, %esi	# D, tmp801
	xorl	%edx, %esi	# C, tmp802
	addl	%eax, %esi	# _610, B
	leal	681279174(%r8,%rcx), %eax	#, _607
# ./md5-block.c:127:       OP (FH, A, B, C, D, 13,  4, 0x289b7ec6);
	movl	%edi, %ecx	# D, tmp804
# ./md5-block.c:126:       OP (FH, B, C, D, A, 10, 23, 0xbebfbc70);
	rorl	$9, %esi	#, B
# ./md5-block.c:127:       OP (FH, A, B, C, D, 13,  4, 0x289b7ec6);
	xorl	%edx, %ecx	# C, tmp804
	leal	-358537222(%r14,%rdi), %edi	#, _604
# ./md5-block.c:126:       OP (FH, B, C, D, A, 10, 23, 0xbebfbc70);
	addl	%edx, %esi	# C, B
# ./md5-block.c:127:       OP (FH, A, B, C, D, 13,  4, 0x289b7ec6);
	xorl	%esi, %ecx	# B, tmp805
# ./md5-block.c:129:       OP (FH, C, D, A, B,  3, 16, 0xd4ef3085);
	movl	%esi, %r10d	# B, tmp810
# ./md5-block.c:127:       OP (FH, A, B, C, D, 13,  4, 0x289b7ec6);
	addl	%eax, %ecx	# _607, A
# ./md5-block.c:128:       OP (FH, D, A, B, C,  0, 11, 0xeaa127fa);
	movl	%edx, %eax	# C, tmp807
# ./md5-block.c:127:       OP (FH, A, B, C, D, 13,  4, 0x289b7ec6);
	roll	$4, %ecx	#, A
# ./md5-block.c:128:       OP (FH, D, A, B, C,  0, 11, 0xeaa127fa);
	xorl	%esi, %eax	# B, tmp807
# ./md5-block.c:127:       OP (FH, A, B, C, D, 13,  4, 0x289b7ec6);
	addl	%esi, %ecx	# B, A
# ./md5-block.c:128:       OP (FH, D, A, B, C,  0, 11, 0xeaa127fa);
	xorl	%ecx, %eax	# A, tmp808
# ./md5-block.c:129:       OP (FH, C, D, A, B,  3, 16, 0xd4ef3085);
	xorl	%ecx, %r10d	# A, tmp810
# ./md5-block.c:128:       OP (FH, D, A, B, C,  0, 11, 0xeaa127fa);
	addl	%edi, %eax	# _604, D
	movl	-60(%rsp), %edi	# %sfp, _30
	roll	$11, %eax	#, D
	addl	%ecx, %eax	# A, D
	leal	-722521979(%rdi,%rdx), %edx	#, _601
# ./md5-block.c:129:       OP (FH, C, D, A, B,  3, 16, 0xd4ef3085);
	movl	%r10d, %edi	# tmp810, tmp810
	xorl	%eax, %edi	# D, tmp811
	addl	%edx, %edi	# _601, C
	movl	-28(%rsp), %edx	# %sfp, _45
	roll	$16, %edi	#, C
	addl	%eax, %edi	# D, C
	leal	76029189(%rdx,%rsi), %edx	#, _598
# ./md5-block.c:130:       OP (FH, B, C, D, A,  6, 23, 0x04881d05);
	movl	%ecx, %esi	# A, tmp813
	xorl	%eax, %esi	# D, tmp813
	xorl	%edi, %esi	# C, tmp814
	addl	%edx, %esi	# _598, B
	movl	-44(%rsp), %edx	# %sfp, _60
	rorl	$9, %esi	#, B
	addl	%edi, %esi	# C, B
	leal	-640364487(%rdx,%rcx), %ecx	#, _595
# ./md5-block.c:131:       OP (FH, A, B, C, D,  9,  4, 0xd9d4d039);
	movl	%eax, %edx	# D, tmp816
	leal	-421815835(%rbp,%rax), %eax	#, _592
	xorl	%edi, %edx	# C, tmp816
# ./md5-block.c:133:       OP (FH, C, D, A, B, 15, 16, 0x1fa27cf8);
	movl	%esi, %r10d	# B, tmp822
# ./md5-block.c:131:       OP (FH, A, B, C, D,  9,  4, 0xd9d4d039);
	xorl	%esi, %edx	# B, tmp817
	addl	%ecx, %edx	# _595, A
# ./md5-block.c:132:       OP (FH, D, A, B, C, 12, 11, 0xe6db99e5);
	movl	%edi, %ecx	# C, tmp819
# ./md5-block.c:131:       OP (FH, A, B, C, D,  9,  4, 0xd9d4d039);
	roll	$4, %edx	#, A
	addl	%esi, %edx	# B, A
# ./md5-block.c:132:       OP (FH, D, A, B, C, 12, 11, 0xe6db99e5);
	xorl	%esi, %ecx	# B, tmp819
	xorl	%edx, %ecx	# A, tmp820
# ./md5-block.c:133:       OP (FH, C, D, A, B, 15, 16, 0x1fa27cf8);
	xorl	%edx, %r10d	# A, tmp822
# ./md5-block.c:132:       OP (FH, D, A, B, C, 12, 11, 0xe6db99e5);
	addl	%eax, %ecx	# _592, D
	leal	530742520(%r9,%rdi), %eax	#, _589
# ./md5-block.c:133:       OP (FH, C, D, A, B, 15, 16, 0x1fa27cf8);
	movl	%r10d, %edi	# tmp822, tmp822
# ./md5-block.c:132:       OP (FH, D, A, B, C, 12, 11, 0xe6db99e5);
	roll	$11, %ecx	#, D
	movl	-52(%rsp), %r10d	# %sfp, _40
	addl	%edx, %ecx	# A, D
# ./md5-block.c:133:       OP (FH, C, D, A, B, 15, 16, 0x1fa27cf8);
	xorl	%ecx, %edi	# D, tmp823
	addl	%eax, %edi	# _589, C
	movl	-64(%rsp), %eax	# %sfp, _25
	roll	$16, %edi	#, C
	addl	%ecx, %edi	# D, C
	leal	-995338651(%rax,%rsi), %esi	#, _586
# ./md5-block.c:134:       OP (FH, B, C, D, A,  2, 23, 0xc4ac5665);
	movl	%edx, %eax	# A, tmp825
	xorl	%ecx, %eax	# D, tmp825
	xorl	%edi, %eax	# C, tmp826
	addl	%esi, %eax	# _586, B
	leal	-198630844(%r14,%rdx), %esi	#, _584
# ./md5-block.c:137:       OP (FI, A, B, C, D,  0,  6, 0xf4292244);
	movl	%ecx, %edx	# D, tmp828
# ./md5-block.c:134:       OP (FH, B, C, D, A,  2, 23, 0xc4ac5665);
	rorl	$9, %eax	#, B
# ./md5-block.c:137:       OP (FI, A, B, C, D,  0,  6, 0xf4292244);
	notl	%edx	# tmp828
	movl	-68(%rsp), %r14d	# %sfp, _20
# ./md5-block.c:134:       OP (FH, B, C, D, A,  2, 23, 0xc4ac5665);
	addl	%edi, %eax	# C, B
# ./md5-block.c:137:       OP (FI, A, B, C, D,  0,  6, 0xf4292244);
	orl	%eax, %edx	# B, tmp829
	xorl	%edi, %edx	# C, tmp830
	addl	%esi, %edx	# _584, A
	leal	1126891415(%r13,%rcx), %esi	#, _582
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	movl	%edi, %r13d	# C, tmp832
# ./md5-block.c:137:       OP (FI, A, B, C, D,  0,  6, 0xf4292244);
	roll	$6, %edx	#, A
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	notl	%r13d	# tmp832
	leal	-1416354905(%r12,%rdi), %edi	#, _580
# ./md5-block.c:137:       OP (FI, A, B, C, D,  0,  6, 0xf4292244);
	addl	%eax, %edx	# B, A
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	movl	%r13d, %ecx	# tmp832, tmp832
	orl	%edx, %ecx	# A, tmp833
# ./md5-block.c:140:       OP (FI, B, C, D, A,  5, 21, 0xfc93a039);
	movl	%edx, %r13d	# A, tmp840
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	xorl	%eax, %ecx	# B, tmp834
# ./md5-block.c:140:       OP (FI, B, C, D, A,  5, 21, 0xfc93a039);
	notl	%r13d	# tmp840
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	addl	%esi, %ecx	# _582, D
# ./md5-block.c:139:       OP (FI, C, D, A, B, 14, 15, 0xab9423a7);
	movl	%eax, %esi	# B, tmp836
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	roll	$10, %ecx	#, D
# ./md5-block.c:139:       OP (FI, C, D, A, B, 14, 15, 0xab9423a7);
	notl	%esi	# tmp836
# ./md5-block.c:138:       OP (FI, D, A, B, C,  7, 10, 0x432aff97);
	addl	%edx, %ecx	# A, D
# ./md5-block.c:139:       OP (FI, C, D, A, B, 14, 15, 0xab9423a7);
	orl	%ecx, %esi	# D, tmp837
# ./md5-block.c:141:       OP (FI, A, B, C, D, 12,  6, 0x655b59c3);
	movl	%ecx, %r12d	# D, tmp844
# ./md5-block.c:139:       OP (FI, C, D, A, B, 14, 15, 0xab9423a7);
	xorl	%edx, %esi	# A, tmp838
# ./md5-block.c:141:       OP (FI, A, B, C, D, 12,  6, 0x655b59c3);
	notl	%r12d	# tmp844
# ./md5-block.c:139:       OP (FI, C, D, A, B, 14, 15, 0xab9423a7);
	addl	%edi, %esi	# _580, C
	leal	-57434055(%r10,%rax), %edi	#, _578
# ./md5-block.c:140:       OP (FI, B, C, D, A,  5, 21, 0xfc93a039);
	movl	%r13d, %eax	# tmp840, tmp840
# ./md5-block.c:139:       OP (FI, C, D, A, B, 14, 15, 0xab9423a7);
	roll	$15, %esi	#, C
	movl	-28(%rsp), %r10d	# %sfp, _45
	addl	%ecx, %esi	# D, C
# ./md5-block.c:140:       OP (FI, B, C, D, A,  5, 21, 0xfc93a039);
	orl	%esi, %eax	# C, tmp841
	xorl	%ecx, %eax	# D, tmp842
	addl	%edi, %eax	# _578, B
	leal	1700485571(%rbp,%rdx), %edi	#, _576
	movl	-60(%rsp), %ebp	# %sfp, _30
	rorl	$11, %eax	#, B
# ./md5-block.c:141:       OP (FI, A, B, C, D, 12,  6, 0x655b59c3);
	movl	%r12d, %edx	# tmp844, tmp844
# ./md5-block.c:140:       OP (FI, B, C, D, A,  5, 21, 0xfc93a039);
	addl	%esi, %eax	# C, B
# ./md5-block.c:141:       OP (FI, A, B, C, D, 12,  6, 0x655b59c3);
	orl	%eax, %edx	# B, tmp845
	xorl	%esi, %edx	# C, tmp846
	addl	%edi, %edx	# _576, A
	leal	-1894986606(%rbp,%rcx), %edi	#, _574
# ./md5-block.c:142:       OP (FI, D, A, B, C,  3, 10, 0x8f0ccc92);
	movl	%esi, %ebp	# C, tmp848
# ./md5-block.c:141:       OP (FI, A, B, C, D, 12,  6, 0x655b59c3);
	roll	$6, %edx	#, A
# ./md5-block.c:142:       OP (FI, D, A, B, C,  3, 10, 0x8f0ccc92);
	notl	%ebp	# tmp848
# ./md5-block.c:141:       OP (FI, A, B, C, D, 12,  6, 0x655b59c3);
	addl	%eax, %edx	# B, A
# ./md5-block.c:142:       OP (FI, D, A, B, C,  3, 10, 0x8f0ccc92);
	movl	%ebp, %ecx	# tmp848, tmp848
	orl	%edx, %ecx	# A, tmp849
# ./md5-block.c:144:       OP (FI, B, C, D, A,  1, 21, 0x85845dd1);
	movl	%edx, %ebp	# A, tmp856
# ./md5-block.c:142:       OP (FI, D, A, B, C,  3, 10, 0x8f0ccc92);
	xorl	%eax, %ecx	# B, tmp850
# ./md5-block.c:144:       OP (FI, B, C, D, A,  1, 21, 0x85845dd1);
	notl	%ebp	# tmp856
# ./md5-block.c:142:       OP (FI, D, A, B, C,  3, 10, 0x8f0ccc92);
	addl	%edi, %ecx	# _574, D
	movl	-40(%rsp), %edi	# %sfp, _65
	roll	$10, %ecx	#, D
	addl	%edx, %ecx	# A, D
	leal	-1051523(%rdi,%rsi), %edi	#, _572
# ./md5-block.c:143:       OP (FI, C, D, A, B, 10, 15, 0xffeff47d);
	movl	%eax, %esi	# B, tmp852
	notl	%esi	# tmp852
	orl	%ecx, %esi	# D, tmp853
	xorl	%edx, %esi	# A, tmp854
	addl	%edi, %esi	# _572, C
	leal	-2054922799(%r14,%rax), %edi	#, _570
# ./md5-block.c:144:       OP (FI, B, C, D, A,  1, 21, 0x85845dd1);
	movl	%ebp, %eax	# tmp856, tmp856
# ./md5-block.c:143:       OP (FI, C, D, A, B, 10, 15, 0xffeff47d);
	roll	$15, %esi	#, C
	movl	-48(%rsp), %r14d	# %sfp, _55
# ./md5-block.c:145:       OP (FI, A, B, C, D,  8,  6, 0x6fa87e4f);
	movl	%ecx, %ebp	# D, tmp860
# ./md5-block.c:143:       OP (FI, C, D, A, B, 10, 15, 0xffeff47d);
	addl	%ecx, %esi	# D, C
# ./md5-block.c:145:       OP (FI, A, B, C, D,  8,  6, 0x6fa87e4f);
	notl	%ebp	# tmp860
# ./md5-block.c:144:       OP (FI, B, C, D, A,  1, 21, 0x85845dd1);
	orl	%esi, %eax	# C, tmp857
	xorl	%ecx, %eax	# D, tmp858
	addl	%edi, %eax	# _570, B
	leal	1873313359(%r14,%rdx), %edi	#, _568
# ./md5-block.c:145:       OP (FI, A, B, C, D,  8,  6, 0x6fa87e4f);
	movl	%ebp, %edx	# tmp860, tmp860
# ./md5-block.c:144:       OP (FI, B, C, D, A,  1, 21, 0x85845dd1);
	rorl	$11, %eax	#, B
# ./md5-block.c:146:       OP (FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
	movl	%esi, %ebp	# C, tmp864
	movl	-36(%rsp), %r14d	# %sfp, _70
# ./md5-block.c:144:       OP (FI, B, C, D, A,  1, 21, 0x85845dd1);
	addl	%esi, %eax	# C, B
# ./md5-block.c:146:       OP (FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
	notl	%ebp	# tmp864
# ./md5-block.c:145:       OP (FI, A, B, C, D,  8,  6, 0x6fa87e4f);
	orl	%eax, %edx	# B, tmp861
	xorl	%esi, %edx	# C, tmp862
	addl	%edi, %edx	# _568, A
	leal	-30611744(%r9,%rcx), %edi	#, _7
# ./md5-block.c:146:       OP (FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
	movl	%ebp, %ecx	# tmp864, tmp864
# ./md5-block.c:145:       OP (FI, A, B, C, D,  8,  6, 0x6fa87e4f);
	roll	$6, %edx	#, A
	movl	-56(%rsp), %r9d	# %sfp, _35
	addl	%eax, %edx	# B, A
# ./md5-block.c:146:       OP (FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
	orl	%edx, %ecx	# A, tmp865
	xorl	%eax, %ecx	# B, tmp866
	addl	%edi, %ecx	# _7, D
	leal	-1560198380(%r10,%rsi), %edi	#, _339
# ./md5-block.c:147:       OP (FI, C, D, A, B,  6, 15, 0xa3014314);
	movl	%eax, %esi	# B, tmp868
# ./md5-block.c:146:       OP (FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
	roll	$10, %ecx	#, D
# ./md5-block.c:147:       OP (FI, C, D, A, B,  6, 15, 0xa3014314);
	notl	%esi	# tmp868
	leal	1309151649(%r8,%rax), %eax	#, _555
# ./md5-block.c:146:       OP (FI, D, A, B, C, 15, 10, 0xfe2ce6e0);
	addl	%edx, %ecx	# A, D
# ./md5-block.c:148:       OP (FI, B, C, D, A, 13, 21, 0x4e0811a1);
	movl	%edx, %r8d	# A, tmp872
	movl	-44(%rsp), %r10d	# %sfp, _60
# ./md5-block.c:147:       OP (FI, C, D, A, B,  6, 15, 0xa3014314);
	orl	%ecx, %esi	# D, tmp869
# ./md5-block.c:148:       OP (FI, B, C, D, A, 13, 21, 0x4e0811a1);
	notl	%r8d	# tmp872
# ./md5-block.c:147:       OP (FI, C, D, A, B,  6, 15, 0xa3014314);
	xorl	%edx, %esi	# A, tmp870
	leal	-145523070(%r9,%rdx), %edx	#, _316
# ./md5-block.c:149:       OP (FI, A, B, C, D,  4,  6, 0xf7537e82);
	movl	%ecx, %r9d	# D, tmp876
# ./md5-block.c:147:       OP (FI, C, D, A, B,  6, 15, 0xa3014314);
	addl	%edi, %esi	# _339, C
# ./md5-block.c:148:       OP (FI, B, C, D, A, 13, 21, 0x4e0811a1);
	movl	%r8d, %edi	# tmp872, tmp872
# ./md5-block.c:149:       OP (FI, A, B, C, D,  4,  6, 0xf7537e82);
	notl	%r9d	# tmp876
# ./md5-block.c:147:       OP (FI, C, D, A, B,  6, 15, 0xa3014314);
	roll	$15, %esi	#, C
	addl	%ecx, %esi	# D, C
# ./md5-block.c:148:       OP (FI, B, C, D, A, 13, 21, 0x4e0811a1);
	orl	%esi, %edi	# C, tmp873
	xorl	%ecx, %edi	# D, tmp874
	leal	-1120210379(%r14,%rcx), %ecx	#, _318
	addl	%eax, %edi	# _555, B
# ./md5-block.c:149:       OP (FI, A, B, C, D,  4,  6, 0xf7537e82);
	movl	%r9d, %eax	# tmp876, tmp876
# ./md5-block.c:148:       OP (FI, B, C, D, A, 13, 21, 0x4e0811a1);
	rorl	$11, %edi	#, B
	addl	%esi, %edi	# C, B
# ./md5-block.c:149:       OP (FI, A, B, C, D,  4,  6, 0xf7537e82);
	orl	%edi, %eax	# B, tmp877
	xorl	%esi, %eax	# C, tmp878
	addl	%edx, %eax	# _316, A
# ./md5-block.c:150:       OP (FI, D, A, B, C, 11, 10, 0xbd3af235);
	movl	%esi, %edx	# C, tmp880
# ./md5-block.c:149:       OP (FI, A, B, C, D,  4,  6, 0xf7537e82);
	roll	$6, %eax	#, A
# ./md5-block.c:150:       OP (FI, D, A, B, C, 11, 10, 0xbd3af235);
	notl	%edx	# tmp880
# ./md5-block.c:149:       OP (FI, A, B, C, D,  4,  6, 0xf7537e82);
	addl	%edi, %eax	# B, A
# ./md5-block.c:150:       OP (FI, D, A, B, C, 11, 10, 0xbd3af235);
	orl	%eax, %edx	# A, tmp881
	xorl	%edi, %edx	# B, tmp882
	addl	%ecx, %edx	# _318, D
	movl	-64(%rsp), %ecx	# %sfp, _25
	roll	$10, %edx	#, D
	addl	%eax, %edx	# A, D
	leal	718787259(%rcx,%rsi), %esi	#, _320
# ./md5-block.c:151:       OP (FI, C, D, A, B,  2, 15, 0x2ad7d2bb);
	movl	%edi, %ecx	# B, tmp884
	leal	-343485551(%r10,%rdi), %edi	#, _565
	notl	%ecx	# tmp884
	orl	%edx, %ecx	# D, tmp885
	xorl	%eax, %ecx	# A, tmp886
	addl	%esi, %ecx	# _320, C
# ./md5-block.c:152:       OP (FI, B, C, D, A,  9, 21, 0xeb86d391);
	movl	%eax, %esi	# A, tmp888
# ./md5-block.c:151:       OP (FI, C, D, A, B,  2, 15, 0x2ad7d2bb);
	roll	$15, %ecx	#, C
# ./md5-block.c:152:       OP (FI, B, C, D, A,  9, 21, 0xeb86d391);
	notl	%esi	# tmp888
# ./md5-block.c:151:       OP (FI, C, D, A, B,  2, 15, 0x2ad7d2bb);
	addl	%edx, %ecx	# D, C
# ./md5-block.c:152:       OP (FI, B, C, D, A,  9, 21, 0xeb86d391);
	orl	%ecx, %esi	# C, tmp889
	xorl	%edx, %esi	# D, tmp890
	addl	%edi, %esi	# _565, B
	rorl	$11, %esi	#, B
	addl	%ecx, %esi	# C, B
# ./md5-block.c:155:       A += A_save;
	addl	%eax, -24(%rsp)	# A, %sfp
# ./md5-block.c:157:       C += C_save;
	addl	%ecx, %r15d	# C, C
# ./md5-block.c:156:       B += B_save;
	addl	%esi, %r11d	# B, B
# ./md5-block.c:158:       D += D_save;
	addl	%edx, -32(%rsp)	# D, %sfp
# ./md5-block.c:34:   while (words < endp)
	cmpq	%rbx, -16(%rsp)	# buffer, %sfp
	ja	.L8	#,
	movl	%r15d, %r10d	# C, C
.L7:
# ./md5-block.c:162:   ctx->A = A;
	movq	-8(%rsp), %rax	# %sfp, ctx
	movl	-24(%rsp), %ebx	# %sfp, A
	movl	%ebx, (%rax)	# A, ctx_326(D)->A
# ./md5-block.c:165:   ctx->D = D;
	movl	-32(%rsp), %ebx	# %sfp, D
# ./md5-block.c:163:   ctx->B = B;
	movl	%r11d, 4(%rax)	# B, ctx_326(D)->B
# ./md5-block.c:164:   ctx->C = C;
	movl	%r10d, 8(%rax)	# C, ctx_326(D)->C
# ./md5-block.c:165:   ctx->D = D;
	movl	%ebx, 12(%rax)	# D, ctx_326(D)->D
# ./md5-block.c:166: }
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE38:
	.size	__md5_process_block, .-__md5_process_block
	.p2align 4,,15
	.globl	__md5_finish_ctx
	.type	__md5_finish_ctx, @function
__md5_finish_ctx:
.LFB34:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	xorl	%edx, %edx	# _43
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %r12	# resbuf, resbuf
	movq	%rdi, %rbx	# ctx, ctx
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
# md5.c:106:   md5_uint32 bytes = ctx->buflen;
	movl	24(%rdi), %eax	# ctx_24(D)->buflen, bytes
	movl	%eax, %ecx	# bytes, tmp116
	addl	16(%rdi), %ecx	# ctx_24(D)->total, tmp116
	setc	%dl	#, _43
# md5.c:110:   ctx->total[0] += bytes;
	movl	%ecx, 16(%rdi)	# tmp116, ctx_24(D)->total
# md5.c:111:   if (ctx->total[0] < bytes)
	testl	%edx, %edx	# _43
	je	.L14	#,
# md5.c:112:     ++ctx->total[1];
	addl	$1, 20(%rdi)	#, ctx_24(D)->total
.L14:
# md5.c:114:   pad = bytes >= 56 ? 64 + 56 - bytes : 56 - bytes;
	cmpl	$55, %eax	#, bytes
	ja	.L21	#,
# md5.c:114:   pad = bytes >= 56 ? 64 + 56 - bytes : 56 - bytes;
	movl	$56, %esi	#, tmp121
	movl	%esi, %ebp	# tmp121, iftmp.0_21
	subl	%eax, %ebp	# bytes, iftmp.0_21
.L16:
	movl	%eax, %r13d	# bytes, _42
# md5.c:115:   memcpy (&ctx->buffer[bytes], fillbuf, pad);
	leaq	fillbuf(%rip), %rsi	#,
	movq	%rbp, %rdx	# iftmp.0_21,
	leaq	28(%rbx,%r13), %rdi	#, tmp123
	call	memcpy@PLT	#
# md5.c:118:   ctx->buffer32[(bytes + pad) / 4] = SWAP (ctx->total[0] << 3);
	movl	16(%rbx), %edx	# ctx_24(D)->total, _8
	leaq	0(%rbp,%r13), %rsi	#, _10
# md5.c:123:   __md5_process_block (ctx->buffer, bytes + pad + 8, ctx);
	leaq	28(%rbx), %rdi	#, tmp141
# md5.c:118:   ctx->buffer32[(bytes + pad) / 4] = SWAP (ctx->total[0] << 3);
	movq	%rsi, %rax	# _10, tmp130
	leal	0(,%rdx,8), %ecx	#, tmp132
	shrq	$2, %rax	#, tmp130
# md5.c:119:   ctx->buffer32[(bytes + pad + 4) / 4] = SWAP ((ctx->total[1] << 3)
	shrl	$29, %edx	#, tmp138
# md5.c:118:   ctx->buffer32[(bytes + pad) / 4] = SWAP (ctx->total[0] << 3);
	movl	%ecx, 28(%rbx,%rax,4)	# tmp132, ctx_24(D)->D.4701.buffer32
# md5.c:119:   ctx->buffer32[(bytes + pad + 4) / 4] = SWAP ((ctx->total[1] << 3)
	movl	20(%rbx), %eax	# ctx_24(D)->total, tmp154
	leaq	4(%rsi), %rcx	#, tmp133
# md5.c:123:   __md5_process_block (ctx->buffer, bytes + pad + 8, ctx);
	addq	$8, %rsi	#, tmp140
# md5.c:119:   ctx->buffer32[(bytes + pad + 4) / 4] = SWAP ((ctx->total[1] << 3)
	shrq	$2, %rcx	#, tmp134
	sall	$3, %eax	#, tmp136
	orl	%edx, %eax	# tmp138, tmp139
# md5.c:123:   __md5_process_block (ctx->buffer, bytes + pad + 8, ctx);
	movq	%rbx, %rdx	# ctx,
# md5.c:119:   ctx->buffer32[(bytes + pad + 4) / 4] = SWAP ((ctx->total[1] << 3)
	movl	%eax, 28(%rbx,%rcx,4)	# tmp139, ctx_24(D)->D.4701.buffer32
# md5.c:123:   __md5_process_block (ctx->buffer, bytes + pad + 8, ctx);
	call	__md5_process_block@PLT	#
# md5.c:126: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
# md5.c:125:   return md5_read_ctx (ctx, resbuf);
	movq	%r12, %rsi	# resbuf,
	movq	%rbx, %rdi	# ctx,
# md5.c:126: }
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
# md5.c:125:   return md5_read_ctx (ctx, resbuf);
	jmp	__md5_read_ctx@PLT	#
	.p2align 4,,10
	.p2align 3
.L21:
	.cfi_restore_state
# md5.c:114:   pad = bytes >= 56 ? 64 + 56 - bytes : 56 - bytes;
	movl	$120, %esi	#, tmp119
	movl	%esi, %ebp	# tmp119, iftmp.0_21
	subl	%eax, %ebp	# bytes, iftmp.0_21
	jmp	.L16	#
	.cfi_endproc
.LFE34:
	.size	__md5_finish_ctx, .-__md5_finish_ctx
	.p2align 4,,15
	.globl	__md5_process_bytes
	.type	__md5_process_bytes, @function
__md5_process_bytes:
.LFB37:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdi, %r12	# buffer, buffer
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdx, %rbp	# ctx, ctx
	movq	%rsi, %rbx	# len, len
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 64
# md5.c:207:   if (ctx->buflen != 0)
	movl	24(%rdx), %eax	# ctx_32(D)->buflen, _1
	testl	%eax, %eax	# _1
	jne	.L61	#,
.L23:
# md5.c:230:   if (len >= 64)
	cmpq	$63, %rbx	#, len
	ja	.L62	#,
.L31:
# md5.c:257:   if (len > 0)
	testq	%rbx, %rbx	# len
	jne	.L63	#,
# md5.c:271: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	.cfi_restore_state
# md5.c:259:       size_t left_over = ctx->buflen;
	movl	24(%rbp), %esi	# ctx_32(D)->buflen, left_over
# md5.c:261:       memcpy (&ctx->buffer[left_over], buffer, len);
	cmpl	$8, %ebx	#, len
	movl	%ebx, %eax	# len, len
	leaq	28(%rbp,%rsi), %rcx	#, tmp172
	jnb	.L33	#,
	testb	$4, %bl	#, len
	jne	.L64	#,
	testl	%ebx, %ebx	# len
	je	.L34	#,
	movzbl	(%r12), %edx	#* buffer, tmp185
	testb	$2, %al	#, len
	movb	%dl, (%rcx)	# tmp185,
	jne	.L65	#,
.L34:
# md5.c:262:       left_over += len;
	addq	%rsi, %rbx	# left_over, left_over
# md5.c:263:       if (left_over >= 64)
	cmpq	$63, %rbx	#, left_over
	jbe	.L39	#,
# md5.c:265: 	  __md5_process_block (ctx->buffer, 64, ctx);
	leaq	28(%rbp), %r12	#, _18
	movq	%rbp, %rdx	# ctx,
	movl	$64, %esi	#,
# md5.c:266: 	  left_over -= 64;
	subq	$64, %rbx	#, left_over
# md5.c:265: 	  __md5_process_block (ctx->buffer, 64, ctx);
	movq	%r12, %rdi	# _18,
	call	__md5_process_block@PLT	#
# md5.c:267: 	  memcpy (ctx->buffer, &ctx->buffer[64], left_over);
	leaq	92(%rbp), %rsi	#, tmp208
	movq	%rbx, %rdx	# left_over,
	movq	%r12, %rdi	# _18,
	call	memcpy@PLT	#
.L39:
# md5.c:269:       ctx->buflen = left_over;
	movl	%ebx, 24(%rbp)	# left_over, ctx_32(D)->buflen
# md5.c:271: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	.cfi_restore_state
# md5.c:250: 	  __md5_process_block (buffer, len & ~63, ctx);
	movq	%rbx, %r13	# len, _15
	movq	%r12, %rdi	# buffer,
	movq	%rbp, %rdx	# ctx,
	andq	$-64, %r13	#, _15
# md5.c:252: 	  len &= 63;
	andl	$63, %ebx	#, len
# md5.c:250: 	  __md5_process_block (buffer, len & ~63, ctx);
	movq	%r13, %rsi	# _15,
# md5.c:251: 	  buffer = (const char *) buffer + (len & ~63);
	addq	%r13, %r12	# _15, buffer
# md5.c:250: 	  __md5_process_block (buffer, len & ~63, ctx);
	call	__md5_process_block@PLT	#
	jmp	.L31	#
	.p2align 4,,10
	.p2align 3
.L61:
# md5.c:209:       size_t left_over = ctx->buflen;
	movl	%eax, %r13d	# _1, left_over
# md5.c:210:       size_t add = 128 - left_over > len ? len : 128 - left_over;
	movl	$128, %edx	#, tmp119
	subq	%r13, %rdx	# left_over, tmp118
# md5.c:212:       memcpy (&ctx->buffer[left_over], buffer, add);
	leaq	28(%rbp,%r13), %rdi	#, tmp121
# md5.c:210:       size_t add = 128 - left_over > len ? len : 128 - left_over;
	cmpq	%rsi, %rdx	# len, tmp118
	cmova	%rsi, %rdx	# tmp118,, len, tmp118
# md5.c:212:       memcpy (&ctx->buffer[left_over], buffer, add);
	movq	%r12, %rsi	# buffer,
# md5.c:210:       size_t add = 128 - left_over > len ? len : 128 - left_over;
	movq	%rdx, %r14	# tmp118, add
# md5.c:212:       memcpy (&ctx->buffer[left_over], buffer, add);
	call	memcpy@PLT	#
# md5.c:213:       ctx->buflen += add;
	movl	24(%rbp), %esi	# ctx_32(D)->buflen, _6
	addl	%r14d, %esi	# add, _6
# md5.c:215:       if (ctx->buflen > 64)
	cmpl	$64, %esi	#, _6
# md5.c:213:       ctx->buflen += add;
	movl	%esi, 24(%rbp)	# _6, ctx_32(D)->buflen
# md5.c:215:       if (ctx->buflen > 64)
	ja	.L66	#,
.L24:
# md5.c:225:       buffer = (const char *) buffer + add;
	addq	%r14, %r12	# add, buffer
# md5.c:226:       len -= add;
	subq	%r14, %rbx	# add, len
	jmp	.L23	#
	.p2align 4,,10
	.p2align 3
.L33:
# md5.c:261:       memcpy (&ctx->buffer[left_over], buffer, len);
	movq	(%r12), %rax	#* buffer, tmp194
	movq	%rax, (%rcx)	# tmp194,
	movl	%ebx, %eax	# len, len
	movq	-8(%r12,%rax), %rdx	#, tmp201
	movq	%rdx, -8(%rcx,%rax)	# tmp201,
	leaq	8(%rcx), %rdx	#, tmp202
	andq	$-8, %rdx	#, tmp202
	subq	%rdx, %rcx	# tmp202, tmp174
	leal	(%rbx,%rcx), %eax	#, len
	subq	%rcx, %r12	# tmp174, buffer
	andl	$-8, %eax	#, len
	cmpl	$8, %eax	#, len
	jb	.L34	#,
	andl	$-8, %eax	#, tmp204
	xorl	%ecx, %ecx	# tmp203
.L37:
	movl	%ecx, %edi	# tmp203, tmp205
	addl	$8, %ecx	#, tmp203
	movq	(%r12,%rdi), %r8	#, tmp206
	cmpl	%eax, %ecx	# tmp204, tmp203
	movq	%r8, (%rdx,%rdi)	# tmp206,
	jb	.L37	#,
	jmp	.L34	#
	.p2align 4,,10
	.p2align 3
.L66:
# md5.c:217: 	  __md5_process_block (ctx->buffer, ctx->buflen & ~63, ctx);
	leaq	28(%rbp), %r15	#, _8
	andl	$-64, %esi	#, tmp129
	movq	%rbp, %rdx	# ctx,
	movq	%r15, %rdi	# _8,
	call	__md5_process_block@PLT	#
# md5.c:219: 	  ctx->buflen &= 63;
	movl	24(%rbp), %ecx	# ctx_32(D)->buflen, ctx_32(D)->buflen
# md5.c:221: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	leaq	0(%r13,%r14), %rax	#, tmp132
	andq	$-64, %rax	#, tmp133
# md5.c:219: 	  ctx->buflen &= 63;
	movl	%ecx, %edx	# ctx_32(D)->buflen, _10
# md5.c:221: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	leaq	28(%rbp,%rax), %rax	#, tmp135
# md5.c:219: 	  ctx->buflen &= 63;
	andl	$63, %edx	#,
# md5.c:221: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	cmpl	$8, %edx	#, _10
# md5.c:219: 	  ctx->buflen &= 63;
	movl	%edx, 24(%rbp)	# _10, ctx_32(D)->buflen
# md5.c:221: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	jnb	.L25	#,
	testb	$4, %cl	#, ctx_32(D)->buflen
	jne	.L67	#,
	testl	%edx, %edx	# _10
	je	.L24	#,
	movzbl	(%rax), %esi	#, tmp148
	andl	$2, %ecx	#, ctx_32(D)->buflen
	movb	%sil, 28(%rbp)	# tmp148,
	je	.L24	#,
	movzwl	-2(%rax,%rdx), %eax	#, tmp156
	movw	%ax, -2(%r15,%rdx)	# tmp156,
	jmp	.L24	#
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rax), %rcx	#, tmp157
	movq	%rcx, 28(%rbp)	# tmp157,
	movl	%edx, %ecx	# _10, _10
	movq	-8(%rax,%rcx), %rsi	#, tmp164
	movq	%rsi, -8(%r15,%rcx)	# tmp164,
	leaq	36(%rbp), %rcx	#, tmp165
	andq	$-8, %rcx	#, tmp165
	subq	%rcx, %r15	# tmp165, _8
	addl	%r15d, %edx	# _8, _10
	subq	%r15, %rax	# _8, tmp138
	andl	$-8, %edx	#, _10
	cmpl	$8, %edx	#, _10
	jb	.L24	#,
	andl	$-8, %edx	#, tmp167
	xorl	%esi, %esi	# tmp166
.L29:
	movl	%esi, %edi	# tmp166, tmp168
	addl	$8, %esi	#, tmp166
	movq	(%rax,%rdi), %r8	#, tmp169
	cmpl	%edx, %esi	# tmp167, tmp166
	movq	%r8, (%rcx,%rdi)	# tmp169,
	jb	.L29	#,
	jmp	.L24	#
.L67:
	movl	(%rax), %ecx	#, tmp140
	movl	%ecx, 28(%rbp)	# tmp140,
	movl	-4(%rax,%rdx), %eax	#, tmp147
	movl	%eax, -4(%r15,%rdx)	# tmp147,
	jmp	.L24	#
	.p2align 4,,10
	.p2align 3
.L64:
# md5.c:261:       memcpy (&ctx->buffer[left_over], buffer, len);
	movl	(%r12), %edx	#* buffer, tmp177
	movl	%edx, (%rcx)	# tmp177,
	movl	%ebx, %edx	# len, len
	movl	-4(%r12,%rdx), %eax	#, tmp184
	movl	%eax, -4(%rcx,%rdx)	# tmp184,
	jmp	.L34	#
.L65:
	movl	%ebx, %edx	# len, len
	movzwl	-2(%r12,%rdx), %eax	#, tmp193
	movw	%ax, -2(%rcx,%rdx)	# tmp193,
	jmp	.L34	#
	.cfi_endproc
.LFE37:
	.size	__md5_process_bytes, .-__md5_process_bytes
	.p2align 4,,15
	.globl	__md5_buffer
	.type	__md5_buffer, @function
__md5_buffer:
.LFB36:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rsi, %r13	# len, len
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rdi, %r12	# buffer, buffer
	movq	%rdx, %rbp	# resblock, resblock
	subq	$168, %rsp	#,
	.cfi_def_cfa_offset 208
# md5.c:192:   md5_init_ctx (&ctx);
	movq	%rsp, %rbx	#, tmp91
	movq	%rbx, %rdi	# tmp91,
	call	__md5_init_ctx@PLT	#
# md5.c:195:   md5_process_bytes (buffer, len, &ctx);
	movq	%rbx, %rdx	# tmp91,
	movq	%r13, %rsi	# len,
	movq	%r12, %rdi	# buffer,
	call	__md5_process_bytes@PLT	#
# md5.c:198:   return md5_finish_ctx (&ctx, resblock);
	movq	%rbp, %rsi	# resblock,
	movq	%rbx, %rdi	# tmp91,
	call	__md5_finish_ctx@PLT	#
# md5.c:199: }
	addq	$168, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE36:
	.size	__md5_buffer, .-__md5_buffer
	.p2align 4,,15
	.globl	__md5_stream
	.type	__md5_stream, @function
__md5_stream:
.LFB35:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	movq	%rsi, %r15	# resblock, resblock
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
# md5.c:155: 	  n = fread (buffer + sum, 1, BLOCKSIZE - sum, stream);
	movl	$4096, %r13d	#, tmp99
# md5.c:133: {
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdi, %r12	# stream, stream
	subq	$4344, %rsp	#,
	.cfi_def_cfa_offset 4400
# md5.c:141:   md5_init_ctx (&ctx);
	movq	%rsp, %r14	#, tmp112
	leaq	160(%rsp), %rbp	#, tmp113
	movq	%r14, %rdi	# tmp112,
	call	__md5_init_ctx@PLT	#
	.p2align 4,,10
	.p2align 3
.L76:
# md5.c:150:       sum = 0;
	xorl	%ebx, %ebx	# sum
	jmp	.L71	#
	.p2align 4,,10
	.p2align 3
.L87:
# md5.c:159:       while (sum < BLOCKSIZE && n != 0);
	testq	%rax, %rax	# n
	je	.L80	#,
.L71:
# md5.c:155: 	  n = fread (buffer + sum, 1, BLOCKSIZE - sum, stream);
	leaq	0(%rbp,%rbx), %rdi	#, tmp101
	movq	%r13, %rdx	# tmp99, tmp98
	movq	%r12, %rcx	# stream,
	subq	%rbx, %rdx	# sum, tmp98
	movl	$1, %esi	#,
	call	fread@PLT	#
# md5.c:157: 	  sum += n;
	addq	%rax, %rbx	# n, sum
# md5.c:159:       while (sum < BLOCKSIZE && n != 0);
	cmpq	$4095, %rbx	#, sum
	jbe	.L87	#,
.L80:
# md5.c:160:       if (n == 0 && ferror (stream))
	testq	%rax, %rax	# n
	je	.L88	#,
# md5.c:170:       __md5_process_block (buffer, BLOCKSIZE, &ctx);
	movq	%r14, %rdx	# tmp112,
	movl	$4096, %esi	#,
	movq	%rbp, %rdi	# tmp113,
	call	__md5_process_block@PLT	#
# md5.c:145:     {
	jmp	.L76	#
	.p2align 4,,10
	.p2align 3
.L88:
# md5.c:160:       if (n == 0 && ferror (stream))
	movq	%r12, %rdi	# stream,
	call	ferror@PLT	#
	testl	%eax, %eax	# <retval>
	movl	%eax, %r12d	#, <retval>
	jne	.L89	#,
# md5.c:174:   if (sum > 0)
	testq	%rbx, %rbx	# sum
	jne	.L90	#,
.L78:
# md5.c:178:   md5_finish_ctx (&ctx, resblock);
	movq	%r15, %rsi	# resblock,
	movq	%r14, %rdi	# tmp112,
	call	__md5_finish_ctx@PLT	#
.L70:
# md5.c:180: }
	addq	$4344, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	movl	%r12d, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
.L89:
	.cfi_restore_state
# md5.c:161: 	return 1;
	movl	$1, %r12d	#, <retval>
	jmp	.L70	#
	.p2align 4,,10
	.p2align 3
.L90:
# md5.c:175:     md5_process_bytes (buffer, sum, &ctx);
	movq	%r14, %rdx	# tmp112,
	movq	%rbx, %rsi	# sum,
	movq	%rbp, %rdi	# tmp113,
	call	__md5_process_bytes@PLT	#
	jmp	.L78	#
	.cfi_endproc
.LFE35:
	.size	__md5_stream, .-__md5_stream
	.section	.rodata
	.align 32
	.type	fillbuf, @object
	.size	fillbuf, 64
fillbuf:
	.byte	-128
	.byte	0
	.zero	62
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
