	.file	"sha512-crypt.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/sha512-crypt.v.d -MF /run/asm/crypt/sha512-crypt.o.dt
# -MP -MT /run/asm/crypt/.o -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt
# -D PIC -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h sha512-crypt.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/crypt/sha512-crypt.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fpie -ftls-model=initial-exec
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fpic -fpie
# -fplt -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"$"
.LC1:
	.string	"%s%zu$"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__sha512_crypt_r
	.type	__sha512_crypt_r, @function
__sha512_crypt_r:
.LFB41:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp	#,
	.cfi_def_cfa_register 6
	pushq	%r15	#
	pushq	%r14	#
	pushq	%r13	#
	pushq	%r12	#
	.cfi_offset 15, -24
	.cfi_offset 14, -32
	.cfi_offset 13, -40
	.cfi_offset 12, -48
	movq	%rsi, %r15	# salt, salt
	pushq	%rbx	#
	.cfi_offset 3, -56
	movq	%rdi, %rbx	# key, key
# sha512-crypt.c:125:   if (strncmp (sha512_salt_prefix, salt, sizeof (sha512_salt_prefix) - 1) == 0)
	leaq	sha512_salt_prefix(%rip), %rdi	#,
# sha512-crypt.c:103: {
	subq	$968, %rsp	#,
# sha512-crypt.c:103: {
	movq	%rdx, -968(%rbp)	# buffer, %sfp
# sha512-crypt.c:125:   if (strncmp (sha512_salt_prefix, salt, sizeof (sha512_salt_prefix) - 1) == 0)
	movl	$3, %edx	#,
# sha512-crypt.c:103: {
	movl	%ecx, -900(%rbp)	# buflen, buflen
# sha512-crypt.c:125:   if (strncmp (sha512_salt_prefix, salt, sizeof (sha512_salt_prefix) - 1) == 0)
	call	strncmp@PLT	#
# sha512-crypt.c:127:     salt += sizeof (sha512_salt_prefix) - 1;
	leaq	3(%r15), %rdx	#, tmp747
	testl	%eax, %eax	# _1
# sha512-crypt.c:129:   if (strncmp (salt, sha512_rounds_prefix, sizeof (sha512_rounds_prefix) - 1)
	leaq	sha512_rounds_prefix(%rip), %rsi	#,
# sha512-crypt.c:127:     salt += sizeof (sha512_salt_prefix) - 1;
	cmovne	%r15, %rdx	# tmp747,, salt, tmp747
	movq	%rdx, %rdi	# tmp747, salt
	movq	%rdx, -944(%rbp)	# salt, %sfp
# sha512-crypt.c:129:   if (strncmp (salt, sha512_rounds_prefix, sizeof (sha512_rounds_prefix) - 1)
	movl	$7, %edx	#,
	call	strncmp@PLT	#
	testl	%eax, %eax	# _2
# sha512-crypt.c:118:   bool rounds_custom = false;
	movb	$0, -901(%rbp)	#, %sfp
# sha512-crypt.c:117:   size_t rounds = ROUNDS_DEFAULT;
	movq	$5000, -936(%rbp)	#, %sfp
# sha512-crypt.c:129:   if (strncmp (salt, sha512_rounds_prefix, sizeof (sha512_rounds_prefix) - 1)
	je	.L113	#,
.L3:
# sha512-crypt.c:143:   salt_len = MIN (strcspn (salt, "$"), SALT_LEN_MAX);
	movq	-944(%rbp), %rdi	# %sfp,
	leaq	.LC0(%rip), %rsi	#,
	call	strcspn@PLT	#
	cmpq	$16, %rax	#, _6
	movq	%rax, %rcx	#, _6
	movl	$16, %eax	#, tmp749
	cmovb	%rcx, %rax	# _6,, tmp749
# sha512-crypt.c:144:   key_len = strlen (key);
	movq	%rbx, %rdi	# key,
# sha512-crypt.c:143:   salt_len = MIN (strcspn (salt, "$"), SALT_LEN_MAX);
	movq	%rax, -920(%rbp)	# tmp749, %sfp
# sha512-crypt.c:144:   key_len = strlen (key);
	call	strlen@PLT	#
# sha512-crypt.c:146:   if ((key - (char *) 0) % __alignof__ (uint64_t) != 0)
	testb	$7, %bl	#, key
# sha512-crypt.c:144:   key_len = strlen (key);
	movq	%rax, %r14	#, tmp350
# sha512-crypt.c:146:   if ((key - (char *) 0) % __alignof__ (uint64_t) != 0)
	je	.L60	#,
# sha512-crypt.c:150:       if (__libc_use_alloca (alloca_used + key_len + __alignof__ (uint64_t)))
	leaq	8(%rax), %r12	#, _9
# ../sysdeps/pthread/allocalim.h:27:   return (__glibc_likely (__libc_alloca_cutoff (size))
	movq	%r12, %rdi	# _9,
	call	__libc_alloca_cutoff@PLT	#
# ../sysdeps/pthread/allocalim.h:29:           || __glibc_likely (size <= PTHREAD_STACK_MIN / 4)
	testl	%eax, %eax	# _377
	jne	.L7	#,
	cmpq	$4096, %r12	#, _9
	ja	.L114	#,
.L7:
# sha512-crypt.c:151: 	tmp = alloca_account (key_len + __alignof__ (uint64_t), alloca_used);
#APP
# 151 "sha512-crypt.c" 1
	mov %rsp, %rax	# p__
# 0 "" 2
#NO_APP
	addq	$30, %r12	#, tmp361
	andq	$-16, %r12	#, tmp365
	subq	%r12, %rsp	# tmp365,
	leaq	15(%rsp), %rdi	#, tmp367
	andq	$-16, %rdi	#, m__
#APP
# 151 "sha512-crypt.c" 1
	sub %rsp , %rax	# d__
# 0 "" 2
#NO_APP
	addq	%r14, %rax	# tmp350, prephitmp_391
# sha512-crypt.c:120:   char *free_key = NULL;
	movq	$0, -984(%rbp)	#, %sfp
	movq	%rax, -912(%rbp)	# prephitmp_391, %sfp
.L9:
# sha512-crypt.c:159:       key = copied_key =
	movq	%rbx, %rsi	# key,
# sha512-crypt.c:161: 		- (tmp - (char *) 0) % __alignof__ (uint64_t),
	addq	$8, %rdi	#, tmp371
# sha512-crypt.c:159:       key = copied_key =
	movq	%r14, %rdx	# tmp350,
	call	memcpy@PLT	#
	movq	%rax, %rbx	#, key
	movq	%rax, -992(%rbp)	# key, %sfp
	jmp	.L6	#
	.p2align 4,,10
	.p2align 3
.L60:
# sha512-crypt.c:144:   key_len = strlen (key);
	movq	%rax, -912(%rbp)	# tmp350, %sfp
# sha512-crypt.c:120:   char *free_key = NULL;
	movq	$0, -984(%rbp)	#, %sfp
# sha512-crypt.c:112:   char *copied_key = NULL;
	movq	$0, -992(%rbp)	#, %sfp
.L6:
# sha512-crypt.c:166:   if ((salt - (char *) 0) % __alignof__ (uint64_t) != 0)
	testb	$7, -944(%rbp)	#, %sfp
# sha512-crypt.c:113:   char *copied_salt = NULL;
	movq	$0, -976(%rbp)	#, %sfp
# sha512-crypt.c:166:   if ((salt - (char *) 0) % __alignof__ (uint64_t) != 0)
	jne	.L115	#,
.L10:
# sha512-crypt.c:192:   sha512_init_ctx (&ctx, nss_ctx);
	leaq	-752(%rbp), %r12	#, tmp745
	movq	%r12, %rdi	# tmp745,
	call	__sha512_init_ctx@PLT	#
# sha512-crypt.c:195:   sha512_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp745,
	movq	%r14, %rsi	# tmp350,
	movq	%rbx, %rdi	# key,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:199:   sha512_process_bytes (salt, salt_len, &ctx, nss_ctx);
	movq	-944(%rbp), %r15	# %sfp, salt
	movq	-920(%rbp), %rsi	# %sfp,
	movq	%r12, %rdx	# tmp745,
	movq	%r15, %rdi	# salt,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:204:   sha512_init_ctx (&alt_ctx, nss_alt_ctx);
	leaq	-400(%rbp), %rax	#, tmp746
	movq	%rax, %r13	# tmp746, tmp746
	movq	%rax, %rdi	# tmp746,
	movq	%rax, -928(%rbp)	# tmp746, %sfp
	call	__sha512_init_ctx@PLT	#
# sha512-crypt.c:207:   sha512_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%r13, %rdx	# tmp746,
	movq	%r14, %rsi	# tmp350,
	movq	%rbx, %rdi	# key,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:210:   sha512_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	movq	-920(%rbp), %rsi	# %sfp,
	movq	%r15, %rdi	# salt,
	movq	%r13, %rdx	# tmp746,
# sha512-crypt.c:217:   sha512_finish_ctx (&alt_ctx, nss_alt_ctx, alt_result);
	leaq	-880(%rbp), %r15	#, tmp743
# sha512-crypt.c:210:   sha512_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:213:   sha512_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%r13, %rdx	# tmp746,
	movq	%r14, %rsi	# tmp350,
	movq	%rbx, %rdi	# key,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:217:   sha512_finish_ctx (&alt_ctx, nss_alt_ctx, alt_result);
	movq	%r15, %rsi	# tmp743,
	movq	%r13, %rdi	# tmp746,
	call	__sha512_finish_ctx@PLT	#
# sha512-crypt.c:220:   for (cnt = key_len; cnt > 64; cnt -= 64)
	cmpq	$64, %r14	#, tmp350
	jbe	.L17	#,
	leaq	-65(%r14), %rax	#, _201
	leaq	-64(%r14), %rcx	#, _191
	movq	%rbx, -1000(%rbp)	# key, %sfp
	movq	%r14, %rbx	# cnt, cnt
	movq	%rax, -960(%rbp)	# _201, %sfp
	andq	$-64, %rax	#, tmp433
	movq	%rcx, -952(%rbp)	# _191, %sfp
	subq	%rax, %rcx	# tmp433, _192
	movq	%rcx, %r13	# _192, _192
	.p2align 4,,10
	.p2align 3
.L18:
# sha512-crypt.c:221:     sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp745,
	movl	$64, %esi	#,
	movq	%r15, %rdi	# tmp743,
# sha512-crypt.c:220:   for (cnt = key_len; cnt > 64; cnt -= 64)
	subq	$64, %rbx	#, cnt
# sha512-crypt.c:221:     sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:220:   for (cnt = key_len; cnt > 64; cnt -= 64)
	cmpq	%rbx, %r13	# cnt, _192
	jne	.L18	#,
# sha512-crypt.c:222:   sha512_process_bytes (alt_result, cnt, &ctx, nss_ctx);
	movq	-960(%rbp), %rax	# %sfp, _201
	movq	-952(%rbp), %rsi	# %sfp, _191
	movq	%r12, %rdx	# tmp745,
	movq	%r15, %rdi	# tmp743,
	movq	-1000(%rbp), %rbx	# %sfp, key
	andq	$-64, %rax	#, _201
	subq	%rax, %rsi	# tmp439, _191
	call	__sha512_process_bytes@PLT	#
.L19:
# sha512-crypt.c:220:   for (cnt = key_len; cnt > 64; cnt -= 64)
	movq	%r14, %r13	# tmp350, cnt
	jmp	.L23	#
	.p2align 4,,10
	.p2align 3
.L117:
# sha512-crypt.c:228:       sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	movl	$64, %esi	#,
	movq	%r15, %rdi	# tmp743,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:226:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	shrq	%r13	# cnt
	je	.L116	#,
.L23:
# sha512-crypt.c:227:     if ((cnt & 1) != 0)
	testb	$1, %r13b	#, cnt
# sha512-crypt.c:228:       sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp745,
# sha512-crypt.c:227:     if ((cnt & 1) != 0)
	jne	.L117	#,
# sha512-crypt.c:230:       sha512_process_bytes (key, key_len, &ctx, nss_ctx);
	movq	%r14, %rsi	# tmp350,
	movq	%rbx, %rdi	# key,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:226:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	shrq	%r13	# cnt
	jne	.L23	#,
.L116:
# sha512-crypt.c:233:   sha512_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	%r12, %rdi	# tmp745,
	movq	%r15, %rsi	# tmp743,
	call	__sha512_finish_ctx@PLT	#
# sha512-crypt.c:236:   sha512_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	-928(%rbp), %rdi	# %sfp,
	call	__sha512_init_ctx@PLT	#
	movq	%r12, -952(%rbp)	# tmp745, %sfp
	movq	%r13, %r12	# cnt, cnt
	movq	-928(%rbp), %r13	# %sfp, tmp746
	.p2align 4,,10
	.p2align 3
.L25:
# sha512-crypt.c:240:     sha512_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	movq	%r13, %rdx	# tmp746,
	movq	%r14, %rsi	# tmp350,
	movq	%rbx, %rdi	# key,
# sha512-crypt.c:239:   for (cnt = 0; cnt < key_len; ++cnt)
	addq	$1, %r12	#, cnt
# sha512-crypt.c:240:     sha512_process_bytes (key, key_len, &alt_ctx, nss_alt_ctx);
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:239:   for (cnt = 0; cnt < key_len; ++cnt)
	cmpq	%r12, %r14	# cnt, tmp350
	jne	.L25	#,
	movq	-952(%rbp), %r12	# %sfp, tmp745
.L57:
# sha512-crypt.c:243:   sha512_finish_ctx (&alt_ctx, nss_alt_ctx, temp_result);
	leaq	-816(%rbp), %rax	#, tmp744
	movq	-928(%rbp), %rdi	# %sfp,
	movq	%rax, %rsi	# tmp744,
	movq	%rax, -960(%rbp)	# tmp744, %sfp
	call	__sha512_finish_ctx@PLT	#
# ../sysdeps/pthread/allocalim.h:27:   return (__glibc_likely (__libc_alloca_cutoff (size))
	movq	-912(%rbp), %rbx	# %sfp, prephitmp_391
	movq	%rbx, %rdi	# prephitmp_391,
	call	__libc_alloca_cutoff@PLT	#
# ../sysdeps/pthread/allocalim.h:29:           || __glibc_likely (size <= PTHREAD_STACK_MIN / 4)
	cmpq	$4096, %rbx	#, prephitmp_391
	jbe	.L26	#,
	testl	%eax, %eax	# _383
	je	.L118	#,
.L26:
# sha512-crypt.c:247:     cp = p_bytes = (char *) alloca (key_len);
	leaq	30(%r14), %rax	#, tmp459
# sha512-crypt.c:121:   char *free_pbytes = NULL;
	movq	$0, -1000(%rbp)	#, %sfp
# sha512-crypt.c:247:     cp = p_bytes = (char *) alloca (key_len);
	andq	$-16, %rax	#, tmp463
	subq	%rax, %rsp	# tmp463,
	leaq	15(%rsp), %rax	#, tmp465
	andq	$-16, %rax	#, tmp465
	movq	%rax, -912(%rbp)	# p_bytes, %sfp
	movq	%rax, -888(%rbp)	# p_bytes, cp
.L28:
# sha512-crypt.c:258:   for (cnt = key_len; cnt >= 64; cnt -= 64)
	cmpq	$63, %r14	#, tmp350
	jbe	.L63	#,
	leaq	-64(%r14), %rcx	#, _230
	movq	-912(%rbp), %rsi	# %sfp, p_bytes
	movq	%rcx, %rdx	# _230, tmp469
	andq	$-64, %rdx	#, tmp469
	leaq	64(%rsi), %rax	#, ivtmp.53
	leaq	128(%rsi,%rdx), %rdx	#, _202
	.p2align 4,,10
	.p2align 3
.L30:
# sha512-crypt.c:259:     cp = mempcpy (cp, temp_result, 64);
	movdqa	-816(%rbp), %xmm0	# MEM[(char * {ref-all})&temp_result], MEM[(char * {ref-all})&temp_result]
	movups	%xmm0, -64(%rax)	# MEM[(char * {ref-all})&temp_result], MEM[base: _233, offset: -64B]
	movdqa	-800(%rbp), %xmm0	# MEM[(char * {ref-all})&temp_result], MEM[(char * {ref-all})&temp_result]
	movups	%xmm0, -48(%rax)	# MEM[(char * {ref-all})&temp_result], MEM[base: _233, offset: -64B]
	movdqa	-784(%rbp), %xmm0	# MEM[(char * {ref-all})&temp_result], MEM[(char * {ref-all})&temp_result]
	movups	%xmm0, -32(%rax)	# MEM[(char * {ref-all})&temp_result], MEM[base: _233, offset: -64B]
	movdqa	-768(%rbp), %xmm0	# MEM[(char * {ref-all})&temp_result], MEM[(char * {ref-all})&temp_result]
	movups	%xmm0, -16(%rax)	# MEM[(char * {ref-all})&temp_result], MEM[base: _233, offset: -64B]
	movq	%rax, -888(%rbp)	# ivtmp.53, cp
	addq	$64, %rax	#, ivtmp.53
# sha512-crypt.c:258:   for (cnt = key_len; cnt >= 64; cnt -= 64)
	cmpq	%rax, %rdx	# ivtmp.53, _202
	jne	.L30	#,
	movq	-912(%rbp), %rax	# %sfp, p_bytes
	andq	$-64, %rcx	#, tmp475
	movq	%r14, %rdx	# tmp350, cnt
	andl	$63, %edx	#, cnt
	leaq	64(%rax,%rcx), %rcx	#, _215
.L29:
# sha512-crypt.c:260:   memcpy (cp, temp_result, cnt);
	cmpl	$8, %edx	#, cnt
	movl	%edx, %eax	# cnt, cnt
	jnb	.L31	#,
	andl	$4, %edx	#, cnt
	jne	.L119	#,
	testl	%eax, %eax	# cnt
	je	.L32	#,
	movq	-960(%rbp), %rsi	# %sfp, tmp744
	testb	$2, %al	#, cnt
	movzbl	(%rsi), %edx	#, tmp490
	movb	%dl, (%rcx)	# tmp490,* _215
	jne	.L120	#,
.L32:
# sha512-crypt.c:263:   sha512_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	-928(%rbp), %rdi	# %sfp,
# sha512-crypt.c:266:   for (cnt = 0; cnt < 16 + alt_result[0]; ++cnt)
	xorl	%ebx, %ebx	# cnt
# sha512-crypt.c:263:   sha512_init_ctx (&alt_ctx, nss_alt_ctx);
	call	__sha512_init_ctx@PLT	#
# sha512-crypt.c:266:   for (cnt = 0; cnt < 16 + alt_result[0]; ++cnt)
	movq	%r14, -952(%rbp)	# tmp350, %sfp
	movq	%r12, -1008(%rbp)	# tmp745, %sfp
	movq	%rbx, %r14	# cnt, cnt
	movq	-944(%rbp), %r13	# %sfp, salt
	movq	-920(%rbp), %r12	# %sfp, _6
	movq	-928(%rbp), %rbx	# %sfp, tmp746
	.p2align 4,,10
	.p2align 3
.L37:
# sha512-crypt.c:267:     sha512_process_bytes (salt, salt_len, &alt_ctx, nss_alt_ctx);
	movq	%rbx, %rdx	# tmp746,
	movq	%r12, %rsi	# _6,
	movq	%r13, %rdi	# salt,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:266:   for (cnt = 0; cnt < 16 + alt_result[0]; ++cnt)
	movzbl	-880(%rbp), %edx	# alt_result, tmp516
	addq	$1, %r14	#, cnt
	addq	$16, %rdx	#, tmp517
	cmpq	%r14, %rdx	# cnt, tmp517
	ja	.L37	#,
# sha512-crypt.c:270:   sha512_finish_ctx (&alt_ctx, nss_alt_ctx, temp_result);
	movq	-960(%rbp), %rbx	# %sfp, tmp744
	movq	-928(%rbp), %rdi	# %sfp,
	movq	-952(%rbp), %r14	# %sfp, tmp350
	movq	-1008(%rbp), %r12	# %sfp, tmp745
	movq	%rbx, %rsi	# tmp744,
	call	__sha512_finish_ctx@PLT	#
# sha512-crypt.c:273:   cp = s_bytes = alloca (salt_len);
	movq	-920(%rbp), %rdi	# %sfp, _6
	leaq	30(%rdi), %rax	#, tmp522
	movq	%rdi, %rcx	# _6, _6
	andq	$-16, %rax	#, tmp526
	subq	%rax, %rsp	# tmp526,
	leaq	15(%rsp), %rax	#, tmp528
	andq	$-16, %rax	#, tmp528
# sha512-crypt.c:276:   memcpy (cp, temp_result, cnt);
	cmpl	$8, %edi	#, _6
# sha512-crypt.c:273:   cp = s_bytes = alloca (salt_len);
	movq	%rax, %rsi	# tmp528, tmp530
	movq	%rax, -952(%rbp)	# tmp530, %sfp
	movq	%rax, -888(%rbp)	# tmp530, cp
# sha512-crypt.c:276:   memcpy (cp, temp_result, cnt);
	movq	%rbx, %rax	# tmp744, tmp534
	jnb	.L121	#,
.L38:
	xorl	%edx, %edx	# tmp540
	testb	$4, %cl	#, _6
	jne	.L122	#,
	testb	$2, %cl	#, _6
	jne	.L123	#,
.L42:
	andl	$1, %ecx	#, _6
	jne	.L124	#,
.L43:
# sha512-crypt.c:280:   for (cnt = 0; cnt < rounds; ++cnt)
	xorl	%ebx, %ebx	# cnt
	jmp	.L50	#
	.p2align 4,,10
	.p2align 3
.L128:
# sha512-crypt.c:287: 	sha512_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	-912(%rbp), %rdi	# %sfp,
	movq	%r14, %rsi	# tmp350,
	call	__sha512_process_bytes@PLT	#
.L45:
# sha512-crypt.c:292:       if (cnt % 3 != 0)
	movabsq	$-6148914691236517205, %rax	#, tmp804
	mulq	%rbx	# cnt
	shrq	%rdx	# tmp553
	leaq	(%rdx,%rdx,2), %rax	#, tmp558
	cmpq	%rax, %rbx	# tmp558, cnt
	jne	.L125	#,
.L46:
# sha512-crypt.c:296:       if (cnt % 7 != 0)
	movabsq	$5270498306774157605, %rax	#, tmp805
	imulq	%rbx	# cnt
	movq	%rbx, %rax	# cnt, tmp583
	sarq	$63, %rax	#, tmp583
	sarq	%rdx	# tmp582
	subq	%rax, %rdx	# tmp583, tmp579
	leaq	0(,%rdx,8), %rax	#, tmp585
	subq	%rdx, %rax	# tmp579, tmp586
	cmpq	%rax, %rbx	# tmp586, cnt
	jne	.L126	#,
.L47:
# sha512-crypt.c:300:       if ((cnt & 1) != 0)
	testq	%r13, %r13	# _26
# sha512-crypt.c:301: 	sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp745,
# sha512-crypt.c:300:       if ((cnt & 1) != 0)
	je	.L48	#,
# sha512-crypt.c:301: 	sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	movl	$64, %esi	#,
	movq	%r15, %rdi	# tmp743,
	call	__sha512_process_bytes@PLT	#
.L49:
# sha512-crypt.c:306:       sha512_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	%r15, %rsi	# tmp743,
	movq	%r12, %rdi	# tmp745,
# sha512-crypt.c:280:   for (cnt = 0; cnt < rounds; ++cnt)
	addq	$1, %rbx	#, cnt
# sha512-crypt.c:306:       sha512_finish_ctx (&ctx, nss_ctx, alt_result);
	call	__sha512_finish_ctx@PLT	#
# sha512-crypt.c:280:   for (cnt = 0; cnt < rounds; ++cnt)
	cmpq	%rbx, -936(%rbp)	# cnt, %sfp
	je	.L127	#,
.L50:
# sha512-crypt.c:283:       sha512_init_ctx (&ctx, nss_ctx);
	movq	%r12, %rdi	# tmp745,
# sha512-crypt.c:286:       if ((cnt & 1) != 0)
	movq	%rbx, %r13	# cnt, _26
# sha512-crypt.c:283:       sha512_init_ctx (&ctx, nss_ctx);
	call	__sha512_init_ctx@PLT	#
# sha512-crypt.c:286:       if ((cnt & 1) != 0)
	andl	$1, %r13d	#, _26
# sha512-crypt.c:287: 	sha512_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp745,
# sha512-crypt.c:286:       if ((cnt & 1) != 0)
	jne	.L128	#,
# sha512-crypt.c:289: 	sha512_process_bytes (alt_result, 64, &ctx, nss_ctx);
	movl	$64, %esi	#,
	movq	%r15, %rdi	# tmp743,
	call	__sha512_process_bytes@PLT	#
	jmp	.L45	#
	.p2align 4,,10
	.p2align 3
.L31:
# sha512-crypt.c:260:   memcpy (cp, temp_result, cnt);
	movq	-816(%rbp), %rax	#, tmp499
	movq	%rax, (%rcx)	# tmp499,* _215
	movq	-960(%rbp), %rdi	# %sfp, tmp744
	movl	%edx, %eax	# cnt, cnt
	movq	-8(%rdi,%rax), %rsi	#, tmp506
	movq	%rsi, -8(%rcx,%rax)	# tmp506,
	leaq	8(%rcx), %rsi	#, tmp507
	andq	$-8, %rsi	#, tmp507
	subq	%rsi, %rcx	# tmp507, _215
	leal	(%rdx,%rcx), %eax	#, cnt
	subq	%rcx, %rdi	# _215, tmp480
	andl	$-8, %eax	#, cnt
	cmpl	$8, %eax	#, cnt
	jb	.L32	#,
	andl	$-8, %eax	#, tmp509
	xorl	%edx, %edx	# tmp508
.L35:
	movl	%edx, %ecx	# tmp508, tmp510
	addl	$8, %edx	#, tmp508
	movq	(%rdi,%rcx), %r8	#, tmp511
	cmpl	%eax, %edx	# tmp509, tmp508
	movq	%r8, (%rsi,%rcx)	# tmp511,
	jb	.L35	#,
	jmp	.L32	#
	.p2align 4,,10
	.p2align 3
.L48:
# sha512-crypt.c:303: 	sha512_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	-912(%rbp), %rdi	# %sfp,
	movq	%r14, %rsi	# tmp350,
	call	__sha512_process_bytes@PLT	#
	jmp	.L49	#
	.p2align 4,,10
	.p2align 3
.L126:
# sha512-crypt.c:297: 	sha512_process_bytes (p_bytes, key_len, &ctx, nss_ctx);
	movq	-912(%rbp), %rdi	# %sfp,
	movq	%r12, %rdx	# tmp745,
	movq	%r14, %rsi	# tmp350,
	call	__sha512_process_bytes@PLT	#
	jmp	.L47	#
	.p2align 4,,10
	.p2align 3
.L125:
# sha512-crypt.c:293: 	sha512_process_bytes (s_bytes, salt_len, &ctx, nss_ctx);
	movq	-920(%rbp), %rsi	# %sfp,
	movq	-952(%rbp), %rdi	# %sfp,
	movq	%r12, %rdx	# tmp745,
	call	__sha512_process_bytes@PLT	#
	jmp	.L46	#
	.p2align 4,,10
	.p2align 3
.L127:
# sha512-crypt.c:316:   cp = __stpncpy (buffer, sha512_salt_prefix, MAX (0, buflen));
	movl	-900(%rbp), %edx	# buflen,
	xorl	%ebx, %ebx	# tmp595
	movq	-968(%rbp), %rdi	# %sfp,
	leaq	sha512_salt_prefix(%rip), %rsi	#,
	testl	%edx, %edx	#
	movl	%ebx, %edx	# tmp595, tmp594
	cmovns	-900(%rbp), %edx	# buflen,, tmp594
	movslq	%edx, %rdx	# tmp594, tmp596
	call	__stpncpy@PLT	#
	movq	%rax, %rdi	#, _32
	movq	%rax, -888(%rbp)	# _32, cp
# sha512-crypt.c:317:   buflen -= sizeof (sha512_salt_prefix) - 1;
	movl	-900(%rbp), %eax	# buflen, tmp868
# sha512-crypt.c:319:   if (rounds_custom)
	cmpb	$0, -901(%rbp)	#, %sfp
# sha512-crypt.c:317:   buflen -= sizeof (sha512_salt_prefix) - 1;
	leal	-3(%rax), %edx	#, _36
	movl	%edx, -900(%rbp)	# _36, buflen
# sha512-crypt.c:319:   if (rounds_custom)
	jne	.L129	#,
.L51:
# sha512-crypt.c:327:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	movq	-920(%rbp), %r13	# %sfp, _6
	xorl	%ebx, %ebx	#
	testl	%edx, %edx	# _36
	cmovs	%ebx, %edx	# _36,, tmp606, tmp605
	movq	-944(%rbp), %rsi	# %sfp,
	movslq	%edx, %rdx	# tmp605, tmp607
	cmpq	%r13, %rdx	# _6, tmp607
	cmova	%r13, %rdx	# tmp607,, _6, tmp604
	call	__stpncpy@PLT	#
# sha512-crypt.c:328:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	movslq	-900(%rbp), %rdx	# buflen,
# sha512-crypt.c:327:   cp = __stpncpy (cp, salt, MIN ((size_t) MAX (0, buflen), salt_len));
	movq	%rax, -888(%rbp)	# _49, cp
# sha512-crypt.c:328:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	testl	%edx, %edx	# buflen.23_50
	cmovns	%rdx, %rbx	#,,
	cmpq	%r13, %rbx	# _6, tmp611
	cmova	%r13, %rbx	# tmp611,, _6, tmp608
	subl	%ebx, %edx	# tmp608, _57
# sha512-crypt.c:330:   if (buflen > 0)
	testl	%edx, %edx	# _57
# sha512-crypt.c:328:   buflen -= MIN ((size_t) MAX (0, buflen), salt_len);
	movl	%edx, -900(%rbp)	# _57, buflen
# sha512-crypt.c:330:   if (buflen > 0)
	jle	.L52	#,
# sha512-crypt.c:332:       *cp++ = '$';
	leaq	1(%rax), %rdx	#, tmp612
	movq	%rdx, -888(%rbp)	# tmp612, cp
	movb	$36, (%rax)	#, *_49
# sha512-crypt.c:333:       --buflen;
	subl	$1, -900(%rbp)	#, buflen
.L52:
# sha512-crypt.c:336:   __b64_from_24bit (&cp, &buflen,
	movzbl	-859(%rbp), %ecx	# alt_result, alt_result
	movzbl	-880(%rbp), %edx	# alt_result, alt_result
	leaq	-900(%rbp), %r13	#, tmp616
	movzbl	-838(%rbp), %r8d	# alt_result,
	leaq	-888(%rbp), %rbx	#, tmp617
	movl	$4, %r9d	#,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:338:   __b64_from_24bit (&cp, &buflen,
	movzbl	-837(%rbp), %ecx	# alt_result, alt_result
	movzbl	-858(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-879(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:340:   __b64_from_24bit (&cp, &buflen,
	movzbl	-878(%rbp), %ecx	# alt_result, alt_result
	movzbl	-836(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-857(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:342:   __b64_from_24bit (&cp, &buflen,
	movzbl	-856(%rbp), %ecx	# alt_result, alt_result
	movzbl	-877(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-835(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:344:   __b64_from_24bit (&cp, &buflen,
	movzbl	-834(%rbp), %ecx	# alt_result, alt_result
	movzbl	-855(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-876(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:346:   __b64_from_24bit (&cp, &buflen,
	movzbl	-875(%rbp), %ecx	# alt_result, alt_result
	movzbl	-833(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-854(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:348:   __b64_from_24bit (&cp, &buflen,
	movzbl	-853(%rbp), %ecx	# alt_result, alt_result
	movzbl	-874(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-832(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:350:   __b64_from_24bit (&cp, &buflen,
	movzbl	-831(%rbp), %ecx	# alt_result, alt_result
	movzbl	-852(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-873(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:352:   __b64_from_24bit (&cp, &buflen,
	movzbl	-872(%rbp), %ecx	# alt_result, alt_result
	movzbl	-830(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-851(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:354:   __b64_from_24bit (&cp, &buflen,
	movzbl	-850(%rbp), %ecx	# alt_result, alt_result
	movzbl	-871(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-829(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:356:   __b64_from_24bit (&cp, &buflen,
	movzbl	-828(%rbp), %ecx	# alt_result, alt_result
	movzbl	-849(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-870(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:358:   __b64_from_24bit (&cp, &buflen,
	movzbl	-869(%rbp), %ecx	# alt_result, alt_result
	movzbl	-827(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-848(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:360:   __b64_from_24bit (&cp, &buflen,
	movzbl	-847(%rbp), %ecx	# alt_result, alt_result
	movzbl	-868(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-826(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:362:   __b64_from_24bit (&cp, &buflen,
	movzbl	-825(%rbp), %ecx	# alt_result, alt_result
	movzbl	-846(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-867(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:364:   __b64_from_24bit (&cp, &buflen,
	movzbl	-866(%rbp), %ecx	# alt_result, alt_result
	movzbl	-824(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-845(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:366:   __b64_from_24bit (&cp, &buflen,
	movzbl	-844(%rbp), %ecx	# alt_result, alt_result
	movzbl	-865(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-823(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:368:   __b64_from_24bit (&cp, &buflen,
	movzbl	-822(%rbp), %ecx	# alt_result, alt_result
	movzbl	-843(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-864(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:370:   __b64_from_24bit (&cp, &buflen,
	movzbl	-863(%rbp), %ecx	# alt_result, alt_result
	movzbl	-821(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-842(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:372:   __b64_from_24bit (&cp, &buflen,
	movzbl	-841(%rbp), %ecx	# alt_result, alt_result
	movzbl	-862(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-820(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:374:   __b64_from_24bit (&cp, &buflen,
	movzbl	-819(%rbp), %ecx	# alt_result, alt_result
	movzbl	-840(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-861(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:376:   __b64_from_24bit (&cp, &buflen,
	movzbl	-860(%rbp), %ecx	# alt_result, alt_result
	movzbl	-818(%rbp), %edx	# alt_result, alt_result
	movl	$4, %r9d	#,
	movzbl	-839(%rbp), %r8d	# alt_result,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:378:   __b64_from_24bit (&cp, &buflen,
	movzbl	-817(%rbp), %r8d	# alt_result,
	xorl	%ecx, %ecx	#
	xorl	%edx, %edx	#
	movl	$2, %r9d	#,
	movq	%r13, %rsi	# tmp616,
	movq	%rbx, %rdi	# tmp617,
	call	__b64_from_24bit@PLT	#
# sha512-crypt.c:381:   if (buflen <= 0)
	movl	-900(%rbp), %eax	# buflen,
	testl	%eax, %eax	#
	jle	.L130	#,
# sha512-crypt.c:387:     *cp = '\0';		/* Terminate the string.  */
	movq	-888(%rbp), %rax	# cp, cp.31_190
	movq	-968(%rbp), %rbx	# %sfp, <retval>
	movb	$0, (%rax)	#, *cp.31_190
.L54:
# sha512-crypt.c:394:   __sha512_init_ctx (&ctx);
	movq	%r12, %rdi	# tmp745,
	call	__sha512_init_ctx@PLT	#
# sha512-crypt.c:395:   __sha512_finish_ctx (&ctx, alt_result);
	movq	%r15, %rsi	# tmp743,
	movq	%r12, %rdi	# tmp745,
	call	__sha512_finish_ctx@PLT	#
# sha512-crypt.c:396:   explicit_bzero (&ctx, sizeof (ctx));
	movl	$352, %edx	#,
	movl	$352, %esi	#,
	movq	%r12, %rdi	# tmp745,
	call	__explicit_bzero_chk@PLT	#
# sha512-crypt.c:397:   explicit_bzero (&alt_ctx, sizeof (alt_ctx));
	movq	-928(%rbp), %rdi	# %sfp,
	movl	$352, %edx	#,
	movl	$352, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# sha512-crypt.c:399:   explicit_bzero (temp_result, sizeof (temp_result));
	movq	-960(%rbp), %rdi	# %sfp,
	movl	$64, %edx	#,
	movl	$64, %esi	#,
	call	__explicit_bzero_chk@PLT	#
# sha512-crypt.c:400:   explicit_bzero (p_bytes, key_len);
	movq	-912(%rbp), %rdi	# %sfp,
	movq	$-1, %rdx	#,
	movq	%r14, %rsi	# tmp350,
	call	__explicit_bzero_chk@PLT	#
# sha512-crypt.c:401:   explicit_bzero (s_bytes, salt_len);
	movq	-920(%rbp), %rsi	# %sfp,
	movq	-952(%rbp), %rdi	# %sfp,
	movq	$-1, %rdx	#,
	call	__explicit_bzero_chk@PLT	#
# sha512-crypt.c:402:   if (copied_key != NULL)
	movq	-992(%rbp), %rax	# %sfp, copied_key
	testq	%rax, %rax	# copied_key
	je	.L55	#,
# sha512-crypt.c:403:     explicit_bzero (copied_key, key_len);
	movq	$-1, %rdx	#,
	movq	%r14, %rsi	# tmp350,
	movq	%rax, %rdi	# copied_key,
	call	__explicit_bzero_chk@PLT	#
.L55:
# sha512-crypt.c:404:   if (copied_salt != NULL)
	movq	-976(%rbp), %rax	# %sfp, copied_salt
	testq	%rax, %rax	# copied_salt
	je	.L56	#,
# sha512-crypt.c:405:     explicit_bzero (copied_salt, salt_len);
	movq	-920(%rbp), %rsi	# %sfp,
	movq	$-1, %rdx	#,
	movq	%rax, %rdi	# copied_salt,
	call	__explicit_bzero_chk@PLT	#
.L56:
# sha512-crypt.c:407:   free (free_key);
	movq	-984(%rbp), %rdi	# %sfp,
	call	free@PLT	#
# sha512-crypt.c:408:   free (free_pbytes);
	movq	-1000(%rbp), %rdi	# %sfp,
	call	free@PLT	#
.L1:
# sha512-crypt.c:410: }
	leaq	-40(%rbp), %rsp	#,
	movq	%rbx, %rax	# <retval>,
	popq	%rbx	#
	popq	%r12	#
	popq	%r13	#
	popq	%r14	#
	popq	%r15	#
	popq	%rbp	#
	.cfi_remember_state
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	.cfi_restore_state
# sha512-crypt.c:276:   memcpy (cp, temp_result, cnt);
	movl	(%rax), %edx	#, tmp542
	testb	$2, %cl	#, _6
	movl	%edx, (%rsi)	# tmp542,* s_bytes
	movl	$4, %edx	#, tmp540
	je	.L42	#,
	jmp	.L123	#
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%rax,%rdx), %eax	#, tmp548
	movb	%al, (%rsi,%rdx)	# tmp548,
	jmp	.L43	#
	.p2align 4,,10
	.p2align 3
.L123:
	movzwl	(%rax,%rdx), %edi	#, tmp545
	movw	%di, (%rsi,%rdx)	# tmp545,
	addq	$2, %rdx	#, tmp540
	andl	$1, %ecx	#, _6
	je	.L43	#,
	jmp	.L124	#
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%edi, %esi	# _6, tmp536
	xorl	%eax, %eax	# tmp535
	movq	%rbx, %r8	# tmp744, tmp744
	andl	$-8, %esi	#, tmp536
.L39:
	movl	%eax, %edx	# tmp535, tmp537
	movq	-952(%rbp), %rbx	# %sfp, tmp530
	addl	$8, %eax	#,
	movq	(%r8,%rdx), %rdi	# MEM[(void *)&temp_result], tmp538
	cmpl	%esi, %eax	# tmp536, tmp535
	movq	%rdi, (%rbx,%rdx)	# tmp538, MEM[(void *)s_bytes_294]
	jb	.L39	#,
	movq	%rbx, %rsi	# tmp530, tmp530
	addq	%rax, %rsi	# tmp539, s_bytes
	addq	-960(%rbp), %rax	# %sfp, tmp534
	jmp	.L38	#
	.p2align 4,,10
	.p2align 3
.L130:
# sha512-crypt.c:383:       __set_errno (ERANGE);
	movq	errno@gottpoff(%rip), %rax	#, tmp721
# sha512-crypt.c:384:       buffer = NULL;
	xorl	%ebx, %ebx	# <retval>
# sha512-crypt.c:383:       __set_errno (ERANGE);
	movl	$34, %fs:(%rax)	#, errno
	jmp	.L54	#
	.p2align 4,,10
	.p2align 3
.L129:
# sha512-crypt.c:321:       int n = __snprintf (cp, MAX (0, buflen), "%s%zu$",
	testl	%edx, %edx	# _36
	movq	-936(%rbp), %r8	# %sfp,
	leaq	sha512_rounds_prefix(%rip), %rcx	#,
	cmovs	%ebx, %edx	# _36,, tmp595, tmp598
	xorl	%eax, %eax	#
	movslq	%edx, %rsi	# tmp598, tmp600
	leaq	.LC1(%rip), %rdx	#,
	call	__snprintf@PLT	#
# sha512-crypt.c:324:       buflen -= n;
	movl	-900(%rbp), %edx	# buflen, _36
# sha512-crypt.c:323:       cp += n;
	movslq	%eax, %rdi	# n, n
	addq	-888(%rbp), %rdi	# cp, _32
# sha512-crypt.c:324:       buflen -= n;
	subl	%eax, %edx	# n, _36
# sha512-crypt.c:323:       cp += n;
	movq	%rdi, -888(%rbp)	# _32, cp
# sha512-crypt.c:324:       buflen -= n;
	movl	%edx, -900(%rbp)	# _36, buflen
	jmp	.L51	#
	.p2align 4,,10
	.p2align 3
.L113:
# sha512-crypt.c:132:       const char *num = salt + sizeof (sha512_rounds_prefix) - 1;
	movq	-944(%rbp), %rax	# %sfp, salt
# sha512-crypt.c:134:       unsigned long int srounds = strtoul (num, &endp, 10);
	leaq	-400(%rbp), %rsi	#, tmp345
	movl	$10, %edx	#,
# sha512-crypt.c:132:       const char *num = salt + sizeof (sha512_rounds_prefix) - 1;
	leaq	7(%rax), %rdi	#, num
# sha512-crypt.c:134:       unsigned long int srounds = strtoul (num, &endp, 10);
	call	strtoul@PLT	#
# sha512-crypt.c:135:       if (*endp == '$')
	movq	-400(%rbp), %rdx	# endp, endp.0_3
	cmpb	$36, (%rdx)	#, *endp.0_3
	jne	.L3	#,
# sha512-crypt.c:138: 	  rounds = MAX (ROUNDS_MIN, MIN (srounds, ROUNDS_MAX));
	cmpq	$999999999, %rax	#, srounds
# sha512-crypt.c:137: 	  salt = endp + 1;
	leaq	1(%rdx), %rcx	#, salt
# sha512-crypt.c:138: 	  rounds = MAX (ROUNDS_MIN, MIN (srounds, ROUNDS_MAX));
	movl	$999999999, %edx	#, tmp347
	cmovbe	%rax, %rdx	# srounds,, tmp347
	movl	$1000, %eax	#, tmp348
# sha512-crypt.c:139: 	  rounds_custom = true;
	movb	$1, -901(%rbp)	#, %sfp
	cmpq	$1000, %rdx	#, rounds
# sha512-crypt.c:137: 	  salt = endp + 1;
	movq	%rcx, -944(%rbp)	# salt, %sfp
	cmovnb	%rdx, %rax	# rounds,, tmp348
	movq	%rax, -936(%rbp)	# tmp348, %sfp
	jmp	.L3	#
	.p2align 4,,10
	.p2align 3
.L115:
# sha512-crypt.c:168:       char *tmp = (char *) alloca (salt_len + __alignof__ (uint64_t));
	movq	-920(%rbp), %rsi	# %sfp, _6
	leaq	38(%rsi), %rax	#, tmp381
# sha512-crypt.c:169:       salt = copied_salt =
	movl	%esi, %edx	# _6,
# sha512-crypt.c:168:       char *tmp = (char *) alloca (salt_len + __alignof__ (uint64_t));
	andq	$-16, %rax	#, tmp385
	subq	%rax, %rsp	# tmp385,
	leaq	15(%rsp), %rax	#, tmp387
	andq	$-16, %rax	#, tmp389
# sha512-crypt.c:169:       salt = copied_salt =
	cmpl	$8, %esi	#, _6
# sha512-crypt.c:171: 		- (tmp - (char *) 0) % __alignof__ (uint64_t),
	leaq	8(%rax), %rcx	#, tmp390
# sha512-crypt.c:169:       salt = copied_salt =
	jnb	.L11	#,
	andl	$4, %esi	#, _6
	jne	.L131	#,
	testl	%edx, %edx	# _6
	je	.L12	#,
	movq	-944(%rbp), %rsi	# %sfp, salt
	testb	$2, %dl	#, _6
	movzbl	(%rsi), %esi	#* salt, tmp403
	movb	%sil, 8(%rax)	# tmp403,
	jne	.L132	#,
.L12:
	movq	%rcx, -944(%rbp)	# salt, %sfp
	movq	%rcx, -976(%rbp)	# salt, %sfp
	jmp	.L10	#
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-920(%rbp), %rsi	# %sfp, _6
	movq	-944(%rbp), %r8	# %sfp, salt
	movl	%esi, %eax	# _6, _6
	movq	-8(%r8,%rax), %rdx	#, tmp418
	movq	%rdx, -8(%rcx,%rax)	# tmp418,
	leal	-1(%rsi), %edx	#, _6
	cmpl	$8, %edx	#, _6
	jb	.L12	#,
	andl	$-8, %edx	#, tmp420
	xorl	%eax, %eax	# tmp419
.L15:
	movl	%eax, %esi	# tmp419, tmp421
	addl	$8, %eax	#, tmp419
	movq	(%r8,%rsi), %rdi	#, tmp422
	cmpl	%edx, %eax	# tmp420, tmp419
	movq	%rdi, (%rcx,%rsi)	# tmp422,
	jb	.L15	#,
	jmp	.L12	#
.L119:
# sha512-crypt.c:260:   memcpy (cp, temp_result, cnt);
	movq	-960(%rbp), %rsi	# %sfp, tmp744
	movl	(%rsi), %edx	#, tmp482
	movl	%edx, (%rcx)	# tmp482,* _215
	movl	%eax, %edx	# cnt, cnt
	movl	-4(%rsi,%rdx), %eax	#, tmp489
	movl	%eax, -4(%rcx,%rdx)	# tmp489,
	jmp	.L32	#
	.p2align 4,,10
	.p2align 3
.L17:
# sha512-crypt.c:222:   sha512_process_bytes (alt_result, cnt, &ctx, nss_ctx);
	movq	%r12, %rdx	# tmp745,
	movq	%r14, %rsi	# tmp350,
	movq	%r15, %rdi	# tmp743,
	call	__sha512_process_bytes@PLT	#
# sha512-crypt.c:226:   for (cnt = key_len; cnt > 0; cnt >>= 1)
	testq	%r14, %r14	# tmp350
	jne	.L19	#,
# sha512-crypt.c:233:   sha512_finish_ctx (&ctx, nss_ctx, alt_result);
	movq	%r12, %rdi	# tmp745,
	movq	%r15, %rsi	# tmp743,
	call	__sha512_finish_ctx@PLT	#
# sha512-crypt.c:236:   sha512_init_ctx (&alt_ctx, nss_alt_ctx);
	movq	-928(%rbp), %rdi	# %sfp,
	call	__sha512_init_ctx@PLT	#
	jmp	.L57	#
	.p2align 4,,10
	.p2align 3
.L63:
# sha512-crypt.c:258:   for (cnt = key_len; cnt >= 64; cnt -= 64)
	movq	-912(%rbp), %rcx	# %sfp, _215
	movq	%r14, %rdx	# tmp350, cnt
	jmp	.L29	#
.L120:
# sha512-crypt.c:260:   memcpy (cp, temp_result, cnt);
	movl	%eax, %edx	# cnt, cnt
	movq	-960(%rbp), %rax	# %sfp, tmp744
	movzwl	-2(%rax,%rdx), %eax	#, tmp498
	movw	%ax, -2(%rcx,%rdx)	# tmp498,
	jmp	.L32	#
.L118:
# sha512-crypt.c:250:       free_pbytes = cp = p_bytes = (char *)malloc (key_len);
	movq	%r14, %rdi	# tmp350,
	call	malloc@PLT	#
# sha512-crypt.c:251:       if (free_pbytes == NULL)
	testq	%rax, %rax	# p_bytes
# sha512-crypt.c:250:       free_pbytes = cp = p_bytes = (char *)malloc (key_len);
	movq	%rax, -912(%rbp)	# p_bytes, %sfp
	movq	%rax, -888(%rbp)	# p_bytes, cp
# sha512-crypt.c:251:       if (free_pbytes == NULL)
	je	.L27	#,
	movq	%rax, -1000(%rbp)	# p_bytes, %sfp
	jmp	.L28	#
.L131:
# sha512-crypt.c:169:       salt = copied_salt =
	movq	-944(%rbp), %rdi	# %sfp, salt
	movl	(%rdi), %esi	#* salt, tmp395
	movl	%esi, 8(%rax)	# tmp395,
	movl	-4(%rdi,%rdx), %eax	#, tmp402
	movl	%eax, -4(%rcx,%rdx)	# tmp402,
	jmp	.L12	#
.L132:
	movq	-944(%rbp), %rax	# %sfp, salt
	movzwl	-2(%rax,%rdx), %eax	#, tmp411
	movw	%ax, -2(%rcx,%rdx)	# tmp411,
	jmp	.L12	#
.L114:
# sha512-crypt.c:154: 	  free_key = tmp = (char *) malloc (key_len + __alignof__ (uint64_t));
	movq	%r12, %rdi	# _9,
	call	malloc@PLT	#
# sha512-crypt.c:155: 	  if (tmp == NULL)
	testq	%rax, %rax	# free_key
# sha512-crypt.c:154: 	  free_key = tmp = (char *) malloc (key_len + __alignof__ (uint64_t));
	movq	%rax, %rdi	#, free_key
	movq	%rax, -984(%rbp)	# free_key, %sfp
# sha512-crypt.c:155: 	  if (tmp == NULL)
	je	.L61	#,
	movq	%r14, -912(%rbp)	# tmp350, %sfp
	jmp	.L9	#
.L27:
# sha512-crypt.c:253: 	  free (free_key);
	movq	-984(%rbp), %rdi	# %sfp,
# sha512-crypt.c:254: 	  return NULL;
	xorl	%ebx, %ebx	# <retval>
# sha512-crypt.c:253: 	  free (free_key);
	call	free@PLT	#
# sha512-crypt.c:254: 	  return NULL;
	jmp	.L1	#
.L61:
# sha512-crypt.c:156: 	    return NULL;
	xorl	%ebx, %ebx	# <retval>
	jmp	.L1	#
	.cfi_endproc
.LFE41:
	.size	__sha512_crypt_r, .-__sha512_crypt_r
	.p2align 4,,15
	.globl	__sha512_crypt
	.type	__sha512_crypt, @function
__sha512_crypt:
.LFB42:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %r12	# key, key
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
# sha512-crypt.c:429: 		+ strlen (salt) + 1 + 86 + 1);
	movq	%rsi, %rdi	# salt,
# sha512-crypt.c:421: {
	movq	%rsi, %rbp	# salt, salt
# sha512-crypt.c:429: 		+ strlen (salt) + 1 + 86 + 1);
	call	strlen@PLT	#
# sha512-crypt.c:431:   if (buflen < needed)
	movl	buflen.5421(%rip), %ecx	# buflen, buflen.33_4
# sha512-crypt.c:429: 		+ strlen (salt) + 1 + 86 + 1);
	leal	109(%rax), %ebx	#, needed
	movq	buffer(%rip), %rdx	# buffer, <retval>
# sha512-crypt.c:431:   if (buflen < needed)
	cmpl	%ebx, %ecx	# needed, buflen.33_4
	jge	.L134	#,
# sha512-crypt.c:433:       char *new_buffer = (char *) realloc (buffer, needed);
	movq	%rdx, %rdi	# <retval>,
	movslq	%ebx, %rsi	# needed, needed
	call	realloc@PLT	#
# sha512-crypt.c:434:       if (new_buffer == NULL)
	testq	%rax, %rax	# <retval>
# sha512-crypt.c:433:       char *new_buffer = (char *) realloc (buffer, needed);
	movq	%rax, %rdx	#, <retval>
# sha512-crypt.c:434:       if (new_buffer == NULL)
	je	.L133	#,
# sha512-crypt.c:437:       buffer = new_buffer;
	movq	%rax, buffer(%rip)	# <retval>, buffer
# sha512-crypt.c:438:       buflen = needed;
	movl	%ebx, buflen.5421(%rip)	# needed, buflen
	movl	%ebx, %ecx	# needed, buflen.33_4
.L134:
# sha512-crypt.c:442: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# sha512-crypt.c:441:   return __sha512_crypt_r (key, salt, buffer, buflen);
	movq	%rbp, %rsi	# salt,
	movq	%r12, %rdi	# key,
# sha512-crypt.c:442: }
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
# sha512-crypt.c:441:   return __sha512_crypt_r (key, salt, buffer, buflen);
	jmp	__sha512_crypt_r	#
	.p2align 4,,10
	.p2align 3
.L133:
	.cfi_restore_state
# sha512-crypt.c:442: }
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax	#
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE42:
	.size	__sha512_crypt, .-__sha512_crypt
	.local	buflen.5421
	.comm	buflen.5421,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	sha512_rounds_prefix, @object
	.size	sha512_rounds_prefix, 8
sha512_rounds_prefix:
	.string	"rounds="
	.section	.rodata.str1.1
	.type	sha512_salt_prefix, @object
	.size	sha512_salt_prefix, 4
sha512_salt_prefix:
	.string	"$6$"
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
