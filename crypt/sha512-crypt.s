	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"$"
.LC1:
	.string	"%s%zu$"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__sha512_crypt_r
	.type	__sha512_crypt_r, @function
__sha512_crypt_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r15
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	sha512_salt_prefix(%rip), %rdi
	subq	$968, %rsp
	movq	%rdx, -968(%rbp)
	movl	$3, %edx
	movl	%ecx, -900(%rbp)
	call	strncmp@PLT
	leaq	3(%r15), %rdx
	testl	%eax, %eax
	leaq	sha512_rounds_prefix(%rip), %rsi
	cmovne	%r15, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -944(%rbp)
	movl	$7, %edx
	call	strncmp@PLT
	testl	%eax, %eax
	movb	$0, -901(%rbp)
	movq	$5000, -936(%rbp)
	je	.L113
.L3:
	movq	-944(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	call	strcspn@PLT
	cmpq	$16, %rax
	movq	%rax, %rcx
	movl	$16, %eax
	cmovb	%rcx, %rax
	movq	%rbx, %rdi
	movq	%rax, -920(%rbp)
	call	strlen@PLT
	testb	$7, %bl
	movq	%rax, %r14
	je	.L60
	leaq	8(%rax), %r12
	movq	%r12, %rdi
	call	__libc_alloca_cutoff@PLT
	testl	%eax, %eax
	jne	.L7
	cmpq	$4096, %r12
	ja	.L114
.L7:
#APP
# 151 "sha512-crypt.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %r12
	andq	$-16, %r12
	subq	%r12, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
#APP
# 151 "sha512-crypt.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%r14, %rax
	movq	$0, -984(%rbp)
	movq	%rax, -912(%rbp)
.L9:
	movq	%rbx, %rsi
	addq	$8, %rdi
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	%rax, %rbx
	movq	%rax, -992(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rax, -912(%rbp)
	movq	$0, -984(%rbp)
	movq	$0, -992(%rbp)
.L6:
	testb	$7, -944(%rbp)
	movq	$0, -976(%rbp)
	jne	.L115
.L10:
	leaq	-752(%rbp), %r12
	movq	%r12, %rdi
	call	__sha512_init_ctx@PLT
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__sha512_process_bytes@PLT
	movq	-944(%rbp), %r15
	movq	-920(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	__sha512_process_bytes@PLT
	leaq	-400(%rbp), %rax
	movq	%rax, %r13
	movq	%rax, %rdi
	movq	%rax, -928(%rbp)
	call	__sha512_init_ctx@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__sha512_process_bytes@PLT
	movq	-920(%rbp), %rsi
	movq	%r15, %rdi
	movq	%r13, %rdx
	leaq	-880(%rbp), %r15
	call	__sha512_process_bytes@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__sha512_process_bytes@PLT
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__sha512_finish_ctx@PLT
	cmpq	$64, %r14
	jbe	.L17
	leaq	-65(%r14), %rax
	leaq	-64(%r14), %rcx
	movq	%rbx, -1000(%rbp)
	movq	%r14, %rbx
	movq	%rax, -960(%rbp)
	andq	$-64, %rax
	movq	%rcx, -952(%rbp)
	subq	%rax, %rcx
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%r12, %rdx
	movl	$64, %esi
	movq	%r15, %rdi
	subq	$64, %rbx
	call	__sha512_process_bytes@PLT
	cmpq	%rbx, %r13
	jne	.L18
	movq	-960(%rbp), %rax
	movq	-952(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	-1000(%rbp), %rbx
	andq	$-64, %rax
	subq	%rax, %rsi
	call	__sha512_process_bytes@PLT
.L19:
	movq	%r14, %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$64, %esi
	movq	%r15, %rdi
	call	__sha512_process_bytes@PLT
	shrq	%r13
	je	.L116
.L23:
	testb	$1, %r13b
	movq	%r12, %rdx
	jne	.L117
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__sha512_process_bytes@PLT
	shrq	%r13
	jne	.L23
.L116:
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	__sha512_finish_ctx@PLT
	movq	-928(%rbp), %rdi
	call	__sha512_init_ctx@PLT
	movq	%r12, -952(%rbp)
	movq	%r13, %r12
	movq	-928(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addq	$1, %r12
	call	__sha512_process_bytes@PLT
	cmpq	%r12, %r14
	jne	.L25
	movq	-952(%rbp), %r12
.L57:
	leaq	-816(%rbp), %rax
	movq	-928(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -960(%rbp)
	call	__sha512_finish_ctx@PLT
	movq	-912(%rbp), %rbx
	movq	%rbx, %rdi
	call	__libc_alloca_cutoff@PLT
	cmpq	$4096, %rbx
	jbe	.L26
	testl	%eax, %eax
	je	.L118
.L26:
	leaq	30(%r14), %rax
	movq	$0, -1000(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -912(%rbp)
	movq	%rax, -888(%rbp)
.L28:
	cmpq	$63, %r14
	jbe	.L63
	leaq	-64(%r14), %rcx
	movq	-912(%rbp), %rsi
	movq	%rcx, %rdx
	andq	$-64, %rdx
	leaq	64(%rsi), %rax
	leaq	128(%rsi,%rdx), %rdx
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	-816(%rbp), %xmm0
	movups	%xmm0, -64(%rax)
	movdqa	-800(%rbp), %xmm0
	movups	%xmm0, -48(%rax)
	movdqa	-784(%rbp), %xmm0
	movups	%xmm0, -32(%rax)
	movdqa	-768(%rbp), %xmm0
	movups	%xmm0, -16(%rax)
	movq	%rax, -888(%rbp)
	addq	$64, %rax
	cmpq	%rax, %rdx
	jne	.L30
	movq	-912(%rbp), %rax
	andq	$-64, %rcx
	movq	%r14, %rdx
	andl	$63, %edx
	leaq	64(%rax,%rcx), %rcx
.L29:
	cmpl	$8, %edx
	movl	%edx, %eax
	jnb	.L31
	andl	$4, %edx
	jne	.L119
	testl	%eax, %eax
	je	.L32
	movq	-960(%rbp), %rsi
	testb	$2, %al
	movzbl	(%rsi), %edx
	movb	%dl, (%rcx)
	jne	.L120
.L32:
	movq	-928(%rbp), %rdi
	xorl	%ebx, %ebx
	call	__sha512_init_ctx@PLT
	movq	%r14, -952(%rbp)
	movq	%r12, -1008(%rbp)
	movq	%rbx, %r14
	movq	-944(%rbp), %r13
	movq	-920(%rbp), %r12
	movq	-928(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__sha512_process_bytes@PLT
	movzbl	-880(%rbp), %edx
	addq	$1, %r14
	addq	$16, %rdx
	cmpq	%r14, %rdx
	ja	.L37
	movq	-960(%rbp), %rbx
	movq	-928(%rbp), %rdi
	movq	-952(%rbp), %r14
	movq	-1008(%rbp), %r12
	movq	%rbx, %rsi
	call	__sha512_finish_ctx@PLT
	movq	-920(%rbp), %rdi
	leaq	30(%rdi), %rax
	movq	%rdi, %rcx
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	cmpl	$8, %edi
	movq	%rax, %rsi
	movq	%rax, -952(%rbp)
	movq	%rax, -888(%rbp)
	movq	%rbx, %rax
	jnb	.L121
.L38:
	xorl	%edx, %edx
	testb	$4, %cl
	jne	.L122
	testb	$2, %cl
	jne	.L123
.L42:
	andl	$1, %ecx
	jne	.L124
.L43:
	xorl	%ebx, %ebx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-912(%rbp), %rdi
	movq	%r14, %rsi
	call	__sha512_process_bytes@PLT
.L45:
	movabsq	$-6148914691236517205, %rax
	mulq	%rbx
	shrq	%rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rbx
	jne	.L125
.L46:
	movabsq	$5270498306774157605, %rax
	imulq	%rbx
	movq	%rbx, %rax
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rbx
	jne	.L126
.L47:
	testq	%r13, %r13
	movq	%r12, %rdx
	je	.L48
	movl	$64, %esi
	movq	%r15, %rdi
	call	__sha512_process_bytes@PLT
.L49:
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	__sha512_finish_ctx@PLT
	cmpq	%rbx, -936(%rbp)
	je	.L127
.L50:
	movq	%r12, %rdi
	movq	%rbx, %r13
	call	__sha512_init_ctx@PLT
	andl	$1, %r13d
	movq	%r12, %rdx
	jne	.L128
	movl	$64, %esi
	movq	%r15, %rdi
	call	__sha512_process_bytes@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L31:
	movq	-816(%rbp), %rax
	movq	%rax, (%rcx)
	movq	-960(%rbp), %rdi
	movl	%edx, %eax
	movq	-8(%rdi,%rax), %rsi
	movq	%rsi, -8(%rcx,%rax)
	leaq	8(%rcx), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rcx
	leal	(%rdx,%rcx), %eax
	subq	%rcx, %rdi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L32
	andl	$-8, %eax
	xorl	%edx, %edx
.L35:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rdi,%rcx), %r8
	cmpl	%eax, %edx
	movq	%r8, (%rsi,%rcx)
	jb	.L35
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-912(%rbp), %rdi
	movq	%r14, %rsi
	call	__sha512_process_bytes@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-912(%rbp), %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	__sha512_process_bytes@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-920(%rbp), %rsi
	movq	-952(%rbp), %rdi
	movq	%r12, %rdx
	call	__sha512_process_bytes@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L127:
	movl	-900(%rbp), %edx
	xorl	%ebx, %ebx
	movq	-968(%rbp), %rdi
	leaq	sha512_salt_prefix(%rip), %rsi
	testl	%edx, %edx
	movl	%ebx, %edx
	cmovns	-900(%rbp), %edx
	movslq	%edx, %rdx
	call	__stpncpy@PLT
	movq	%rax, %rdi
	movq	%rax, -888(%rbp)
	movl	-900(%rbp), %eax
	cmpb	$0, -901(%rbp)
	leal	-3(%rax), %edx
	movl	%edx, -900(%rbp)
	jne	.L129
.L51:
	movq	-920(%rbp), %r13
	xorl	%ebx, %ebx
	testl	%edx, %edx
	cmovs	%ebx, %edx
	movq	-944(%rbp), %rsi
	movslq	%edx, %rdx
	cmpq	%r13, %rdx
	cmova	%r13, %rdx
	call	__stpncpy@PLT
	movslq	-900(%rbp), %rdx
	movq	%rax, -888(%rbp)
	testl	%edx, %edx
	cmovns	%rdx, %rbx
	cmpq	%r13, %rbx
	cmova	%r13, %rbx
	subl	%ebx, %edx
	testl	%edx, %edx
	movl	%edx, -900(%rbp)
	jle	.L52
	leaq	1(%rax), %rdx
	movq	%rdx, -888(%rbp)
	movb	$36, (%rax)
	subl	$1, -900(%rbp)
.L52:
	movzbl	-859(%rbp), %ecx
	movzbl	-880(%rbp), %edx
	leaq	-900(%rbp), %r13
	movzbl	-838(%rbp), %r8d
	leaq	-888(%rbp), %rbx
	movl	$4, %r9d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-837(%rbp), %ecx
	movzbl	-858(%rbp), %edx
	movl	$4, %r9d
	movzbl	-879(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-878(%rbp), %ecx
	movzbl	-836(%rbp), %edx
	movl	$4, %r9d
	movzbl	-857(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-856(%rbp), %ecx
	movzbl	-877(%rbp), %edx
	movl	$4, %r9d
	movzbl	-835(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-834(%rbp), %ecx
	movzbl	-855(%rbp), %edx
	movl	$4, %r9d
	movzbl	-876(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-875(%rbp), %ecx
	movzbl	-833(%rbp), %edx
	movl	$4, %r9d
	movzbl	-854(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-853(%rbp), %ecx
	movzbl	-874(%rbp), %edx
	movl	$4, %r9d
	movzbl	-832(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-831(%rbp), %ecx
	movzbl	-852(%rbp), %edx
	movl	$4, %r9d
	movzbl	-873(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-872(%rbp), %ecx
	movzbl	-830(%rbp), %edx
	movl	$4, %r9d
	movzbl	-851(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-850(%rbp), %ecx
	movzbl	-871(%rbp), %edx
	movl	$4, %r9d
	movzbl	-829(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-828(%rbp), %ecx
	movzbl	-849(%rbp), %edx
	movl	$4, %r9d
	movzbl	-870(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-869(%rbp), %ecx
	movzbl	-827(%rbp), %edx
	movl	$4, %r9d
	movzbl	-848(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-847(%rbp), %ecx
	movzbl	-868(%rbp), %edx
	movl	$4, %r9d
	movzbl	-826(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-825(%rbp), %ecx
	movzbl	-846(%rbp), %edx
	movl	$4, %r9d
	movzbl	-867(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-866(%rbp), %ecx
	movzbl	-824(%rbp), %edx
	movl	$4, %r9d
	movzbl	-845(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-844(%rbp), %ecx
	movzbl	-865(%rbp), %edx
	movl	$4, %r9d
	movzbl	-823(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-822(%rbp), %ecx
	movzbl	-843(%rbp), %edx
	movl	$4, %r9d
	movzbl	-864(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-863(%rbp), %ecx
	movzbl	-821(%rbp), %edx
	movl	$4, %r9d
	movzbl	-842(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-841(%rbp), %ecx
	movzbl	-862(%rbp), %edx
	movl	$4, %r9d
	movzbl	-820(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-819(%rbp), %ecx
	movzbl	-840(%rbp), %edx
	movl	$4, %r9d
	movzbl	-861(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-860(%rbp), %ecx
	movzbl	-818(%rbp), %edx
	movl	$4, %r9d
	movzbl	-839(%rbp), %r8d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-817(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %r9d
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__b64_from_24bit@PLT
	movl	-900(%rbp), %eax
	testl	%eax, %eax
	jle	.L130
	movq	-888(%rbp), %rax
	movq	-968(%rbp), %rbx
	movb	$0, (%rax)
.L54:
	movq	%r12, %rdi
	call	__sha512_init_ctx@PLT
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sha512_finish_ctx@PLT
	movl	$352, %edx
	movl	$352, %esi
	movq	%r12, %rdi
	call	__explicit_bzero_chk@PLT
	movq	-928(%rbp), %rdi
	movl	$352, %edx
	movl	$352, %esi
	call	__explicit_bzero_chk@PLT
	movq	-960(%rbp), %rdi
	movl	$64, %edx
	movl	$64, %esi
	call	__explicit_bzero_chk@PLT
	movq	-912(%rbp), %rdi
	movq	$-1, %rdx
	movq	%r14, %rsi
	call	__explicit_bzero_chk@PLT
	movq	-920(%rbp), %rsi
	movq	-952(%rbp), %rdi
	movq	$-1, %rdx
	call	__explicit_bzero_chk@PLT
	movq	-992(%rbp), %rax
	testq	%rax, %rax
	je	.L55
	movq	$-1, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	__explicit_bzero_chk@PLT
.L55:
	movq	-976(%rbp), %rax
	testq	%rax, %rax
	je	.L56
	movq	-920(%rbp), %rsi
	movq	$-1, %rdx
	movq	%rax, %rdi
	call	__explicit_bzero_chk@PLT
.L56:
	movq	-984(%rbp), %rdi
	call	free@PLT
	movq	-1000(%rbp), %rdi
	call	free@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movl	(%rax), %edx
	testb	$2, %cl
	movl	%edx, (%rsi)
	movl	$4, %edx
	je	.L42
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%rax,%rdx), %eax
	movb	%al, (%rsi,%rdx)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L123:
	movzwl	(%rax,%rdx), %edi
	movw	%di, (%rsi,%rdx)
	addq	$2, %rdx
	andl	$1, %ecx
	je	.L43
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%edi, %esi
	xorl	%eax, %eax
	movq	%rbx, %r8
	andl	$-8, %esi
.L39:
	movl	%eax, %edx
	movq	-952(%rbp), %rbx
	addl	$8, %eax
	movq	(%r8,%rdx), %rdi
	cmpl	%esi, %eax
	movq	%rdi, (%rbx,%rdx)
	jb	.L39
	movq	%rbx, %rsi
	addq	%rax, %rsi
	addq	-960(%rbp), %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L130:
	movq	errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$34, %fs:(%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L129:
	testl	%edx, %edx
	movq	-936(%rbp), %r8
	leaq	sha512_rounds_prefix(%rip), %rcx
	cmovs	%ebx, %edx
	xorl	%eax, %eax
	movslq	%edx, %rsi
	leaq	.LC1(%rip), %rdx
	call	__snprintf@PLT
	movl	-900(%rbp), %edx
	movslq	%eax, %rdi
	addq	-888(%rbp), %rdi
	subl	%eax, %edx
	movq	%rdi, -888(%rbp)
	movl	%edx, -900(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-944(%rbp), %rax
	leaq	-400(%rbp), %rsi
	movl	$10, %edx
	leaq	7(%rax), %rdi
	call	strtoul@PLT
	movq	-400(%rbp), %rdx
	cmpb	$36, (%rdx)
	jne	.L3
	cmpq	$999999999, %rax
	leaq	1(%rdx), %rcx
	movl	$999999999, %edx
	cmovbe	%rax, %rdx
	movl	$1000, %eax
	movb	$1, -901(%rbp)
	cmpq	$1000, %rdx
	movq	%rcx, -944(%rbp)
	cmovnb	%rdx, %rax
	movq	%rax, -936(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L115:
	movq	-920(%rbp), %rsi
	leaq	38(%rsi), %rax
	movl	%esi, %edx
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	cmpl	$8, %esi
	leaq	8(%rax), %rcx
	jnb	.L11
	andl	$4, %esi
	jne	.L131
	testl	%edx, %edx
	je	.L12
	movq	-944(%rbp), %rsi
	testb	$2, %dl
	movzbl	(%rsi), %esi
	movb	%sil, 8(%rax)
	jne	.L132
.L12:
	movq	%rcx, -944(%rbp)
	movq	%rcx, -976(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-920(%rbp), %rsi
	movq	-944(%rbp), %r8
	movl	%esi, %eax
	movq	-8(%r8,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	leal	-1(%rsi), %edx
	cmpl	$8, %edx
	jb	.L12
	andl	$-8, %edx
	xorl	%eax, %eax
.L15:
	movl	%eax, %esi
	addl	$8, %eax
	movq	(%r8,%rsi), %rdi
	cmpl	%edx, %eax
	movq	%rdi, (%rcx,%rsi)
	jb	.L15
	jmp	.L12
.L119:
	movq	-960(%rbp), %rsi
	movl	(%rsi), %edx
	movl	%edx, (%rcx)
	movl	%eax, %edx
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__sha512_process_bytes@PLT
	testq	%r14, %r14
	jne	.L19
	movq	%r12, %rdi
	movq	%r15, %rsi
	call	__sha512_finish_ctx@PLT
	movq	-928(%rbp), %rdi
	call	__sha512_init_ctx@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-912(%rbp), %rcx
	movq	%r14, %rdx
	jmp	.L29
.L120:
	movl	%eax, %edx
	movq	-960(%rbp), %rax
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L32
.L118:
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -912(%rbp)
	movq	%rax, -888(%rbp)
	je	.L27
	movq	%rax, -1000(%rbp)
	jmp	.L28
.L131:
	movq	-944(%rbp), %rdi
	movl	(%rdi), %esi
	movl	%esi, 8(%rax)
	movl	-4(%rdi,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L12
.L132:
	movq	-944(%rbp), %rax
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L12
.L114:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, -984(%rbp)
	je	.L61
	movq	%r14, -912(%rbp)
	jmp	.L9
.L27:
	movq	-984(%rbp), %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L1
.L61:
	xorl	%ebx, %ebx
	jmp	.L1
	.size	__sha512_crypt_r, .-__sha512_crypt_r
	.p2align 4,,15
	.globl	__sha512_crypt
	.type	__sha512_crypt, @function
__sha512_crypt:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	call	strlen@PLT
	movl	buflen.5421(%rip), %ecx
	leal	109(%rax), %ebx
	movq	buffer(%rip), %rdx
	cmpl	%ebx, %ecx
	jge	.L134
	movq	%rdx, %rdi
	movslq	%ebx, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L133
	movq	%rax, buffer(%rip)
	movl	%ebx, buflen.5421(%rip)
	movl	%ebx, %ecx
.L134:
	popq	%rbx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	popq	%rbp
	popq	%r12
	jmp	__sha512_crypt_r
	.p2align 4,,10
	.p2align 3
.L133:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	__sha512_crypt, .-__sha512_crypt
	.local	buflen.5421
	.comm	buflen.5421,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	sha512_rounds_prefix, @object
	.size	sha512_rounds_prefix, 8
sha512_rounds_prefix:
	.string	"rounds="
	.section	.rodata.str1.1
	.type	sha512_salt_prefix, @object
	.size	sha512_salt_prefix, 4
sha512_salt_prefix:
	.string	"$6$"
