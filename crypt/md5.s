	.text
	.p2align 4,,15
	.globl	__md5_init_ctx
	.type	__md5_init_ctx, @function
__md5_init_ctx:
	movabsq	$-1167088121787636991, %rax
	movq	$0, 16(%rdi)
	movl	$0, 24(%rdi)
	movq	%rax, (%rdi)
	movabsq	$1167088121787636990, %rax
	movq	%rax, 8(%rdi)
	ret
	.size	__md5_init_ctx, .-__md5_init_ctx
	.p2align 4,,15
	.globl	__md5_read_ctx
	.type	__md5_read_ctx, @function
__md5_read_ctx:
	movl	(%rdi), %edx
	movq	%rsi, %rax
	movl	%edx, (%rsi)
	movl	4(%rdi), %edx
	movl	%edx, 4(%rsi)
	movl	8(%rdi), %edx
	movl	%edx, 8(%rsi)
	movl	12(%rdi), %edx
	movl	%edx, 12(%rsi)
	ret
	.size	__md5_read_ctx, .-__md5_read_ctx
	.p2align 4,,15
	.globl	__md5_process_block
	.type	__md5_process_block, @function
__md5_process_block:
	movq	%rsi, %rax
	pushq	%r15
	pushq	%r14
	andq	$-4, %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	(%rbx,%rax), %rcx
	movl	(%rdx), %eax
	movq	%rdx, %rdi
	movl	4(%rdx), %r11d
	movl	8(%rdx), %r10d
	movq	%rdx, -8(%rsp)
	movq	%rcx, -16(%rsp)
	movl	%eax, -24(%rsp)
	movl	12(%rdx), %eax
	movl	%esi, %edx
	movl	%eax, -32(%rsp)
	xorl	%eax, %eax
	addl	16(%rdi), %edx
	setc	%al
	shrq	$32, %rsi
	addl	20(%rdi), %esi
	movl	%edx, 16(%rdi)
	addl	%eax, %esi
	cmpq	%rcx, %rbx
	movl	%esi, 20(%rdi)
	jnb	.L7
	movl	%r10d, %r15d
	.p2align 4,,10
	.p2align 3
.L8:
	movl	(%rbx), %r14d
	movl	-32(%rsp), %edi
	movl	-24(%rsp), %eax
	movl	4(%rbx), %esi
	movl	12(%rbx), %r8d
	movl	20(%rbx), %r10d
	movl	24(%rbx), %ebp
	movl	28(%rbx), %r13d
	leal	-680876936(%r14,%rax), %edx
	movl	%edi, %eax
	leal	-389564586(%rsi,%rdi), %ecx
	xorl	%r15d, %eax
	movl	%esi, -68(%rsp)
	movl	%r10d, -52(%rsp)
	andl	%r11d, %eax
	movl	36(%rbx), %r12d
	movl	%r8d, -60(%rsp)
	xorl	%edi, %eax
	leal	-1044525330(%r8,%r11), %edi
	movl	44(%rbx), %r8d
	addl	%edx, %eax
	movl	%r11d, %edx
	roll	$7, %eax
	xorl	%r15d, %edx
	movl	%r12d, -44(%rsp)
	addl	%r11d, %eax
	movl	%r8d, -36(%rsp)
	andl	%eax, %edx
	movl	%eax, %r9d
	xorl	%r15d, %edx
	xorl	%r11d, %r9d
	addl	%ecx, %edx
	movl	8(%rbx), %ecx
	roll	$12, %edx
	addl	%eax, %edx
	leal	606105819(%rcx,%r15), %esi
	movl	%ecx, -64(%rsp)
	movl	%r9d, %ecx
	andl	%edx, %ecx
	movl	%eax, %r9d
	xorl	%r11d, %ecx
	xorl	%edx, %r9d
	addl	%esi, %ecx
	movl	%r9d, %esi
	movl	16(%rbx), %r9d
	rorl	$15, %ecx
	addl	%edx, %ecx
	andl	%ecx, %esi
	movl	%r9d, -56(%rsp)
	xorl	%eax, %esi
	addl	%edi, %esi
	leal	-176418897(%r9,%rax), %edi
	movl	%edx, %eax
	rorl	$10, %esi
	xorl	%ecx, %eax
	movl	60(%rbx), %r9d
	addl	%ecx, %esi
	andl	%esi, %eax
	xorl	%edx, %eax
	addl	%edi, %eax
	leal	1200080426(%r10,%rdx), %edi
	movl	%ecx, %edx
	roll	$7, %eax
	xorl	%esi, %edx
	movl	%ebp, %r10d
	addl	%esi, %eax
	movl	%r10d, -28(%rsp)
	andl	%eax, %edx
	xorl	%ecx, %edx
	addl	%edi, %edx
	leal	-1473231341(%rbp,%rcx), %edi
	movl	%esi, %ebp
	xorl	%eax, %ebp
	roll	$12, %edx
	addl	%eax, %edx
	movl	%ebp, %ecx
	movl	%eax, %ebp
	andl	%edx, %ecx
	xorl	%edx, %ebp
	xorl	%esi, %ecx
	addl	%edi, %ecx
	leal	-45705983(%r13,%rsi), %edi
	movl	%ebp, %esi
	rorl	$15, %ecx
	movl	32(%rbx), %ebp
	addl	%edx, %ecx
	andl	%ecx, %esi
	xorl	%eax, %esi
	movl	%ebp, -48(%rsp)
	addl	%edi, %esi
	leal	1770035416(%rbp,%rax), %edi
	movl	%edx, %eax
	rorl	$10, %esi
	xorl	%ecx, %eax
	movl	48(%rbx), %ebp
	addl	%ecx, %esi
	andl	%esi, %eax
	xorl	%edx, %eax
	addl	%edi, %eax
	leal	-1958414417(%r12,%rdx), %edi
	movl	%ecx, %edx
	roll	$7, %eax
	xorl	%esi, %edx
	movl	56(%rbx), %r12d
	addl	%esi, %eax
	andl	%eax, %edx
	xorl	%ecx, %edx
	addl	%edi, %edx
	movl	40(%rbx), %edi
	roll	$12, %edx
	addl	%eax, %edx
	movl	%edi, -40(%rsp)
	leal	-42063(%rdi,%rcx), %edi
	movl	%esi, %ecx
	xorl	%eax, %ecx
	andl	%edx, %ecx
	xorl	%esi, %ecx
	addl	%edi, %ecx
	leal	-1990404162(%r8,%rsi), %edi
	movl	%eax, %esi
	rorl	$15, %ecx
	xorl	%edx, %esi
	movl	52(%rbx), %r8d
	addl	%edx, %ecx
	addq	$64, %rbx
	andl	%ecx, %esi
	xorl	%eax, %esi
	addl	%edi, %esi
	leal	1804603682(%rbp,%rax), %edi
	movl	%edx, %eax
	rorl	$10, %esi
	xorl	%ecx, %eax
	addl	%ecx, %esi
	andl	%esi, %eax
	xorl	%edx, %eax
	addl	%edi, %eax
	leal	-40341101(%r8,%rdx), %edi
	movl	%ecx, %edx
	roll	$7, %eax
	xorl	%esi, %edx
	addl	%esi, %eax
	andl	%eax, %edx
	xorl	%ecx, %edx
	addl	%edi, %edx
	leal	-1502002290(%r12,%rcx), %edi
	movl	%esi, %ecx
	roll	$12, %edx
	xorl	%eax, %ecx
	addl	%eax, %edx
	andl	%edx, %ecx
	xorl	%esi, %ecx
	addl	%edi, %ecx
	leal	1236535329(%r9,%rsi), %edi
	movl	%eax, %esi
	rorl	$15, %ecx
	xorl	%edx, %esi
	addl	%edx, %ecx
	andl	%ecx, %esi
	xorl	%eax, %esi
	addl	%edi, %esi
	movl	-68(%rsp), %edi
	rorl	$10, %esi
	addl	%ecx, %esi
	leal	-165796510(%rdi,%rax), %edi
	movl	%ecx, %eax
	xorl	%esi, %eax
	andl	%edx, %eax
	xorl	%ecx, %eax
	addl	%edi, %eax
	leal	-1069501632(%r10,%rdx), %edi
	movl	%esi, %edx
	roll	$5, %eax
	addl	%esi, %eax
	xorl	%eax, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	movl	-36(%rsp), %edi
	roll	$9, %edx
	addl	%eax, %edx
	leal	643717713(%rdi,%rcx), %edi
	movl	%eax, %ecx
	xorl	%edx, %ecx
	andl	%esi, %ecx
	xorl	%eax, %ecx
	addl	%edi, %ecx
	leal	-373897302(%r14,%rsi), %edi
	movl	%edx, %esi
	roll	$14, %ecx
	addl	%edx, %ecx
	xorl	%ecx, %esi
	andl	%eax, %esi
	xorl	%edx, %esi
	addl	%edi, %esi
	movl	-52(%rsp), %edi
	rorl	$12, %esi
	addl	%ecx, %esi
	leal	-701558691(%rdi,%rax), %edi
	movl	%ecx, %eax
	xorl	%esi, %eax
	andl	%edx, %eax
	xorl	%ecx, %eax
	addl	%edi, %eax
	movl	-40(%rsp), %edi
	roll	$5, %eax
	addl	%esi, %eax
	leal	38016083(%rdi,%rdx), %edi
	movl	%esi, %edx
	xorl	%eax, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	leal	-660478335(%r9,%rcx), %edi
	movl	%eax, %ecx
	roll	$9, %edx
	addl	%eax, %edx
	xorl	%edx, %ecx
	andl	%esi, %ecx
	xorl	%eax, %ecx
	addl	%edi, %ecx
	movl	-56(%rsp), %edi
	movl	-44(%rsp), %r10d
	roll	$14, %ecx
	addl	%edx, %ecx
	leal	-405537848(%rdi,%rsi), %edi
	movl	%edx, %esi
	xorl	%ecx, %esi
	andl	%eax, %esi
	xorl	%edx, %esi
	addl	%edi, %esi
	leal	568446438(%r10,%rax), %edi
	movl	%ecx, %eax
	rorl	$12, %esi
	movl	-48(%rsp), %r10d
	addl	%ecx, %esi
	xorl	%esi, %eax
	andl	%edx, %eax
	xorl	%ecx, %eax
	addl	%edi, %eax
	leal	-1019803690(%r12,%rdx), %edi
	movl	%esi, %edx
	roll	$5, %eax
	addl	%esi, %eax
	xorl	%eax, %edx
	andl	%ecx, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	movl	-60(%rsp), %edi
	roll	$9, %edx
	addl	%eax, %edx
	leal	-187363961(%rdi,%rcx), %edi
	movl	%eax, %ecx
	xorl	%edx, %ecx
	andl	%esi, %ecx
	leal	1163531501(%r10,%rsi), %esi
	movl	%edx, %r10d
	xorl	%eax, %ecx
	addl	%edi, %ecx
	roll	$14, %ecx
	addl	%edx, %ecx
	xorl	%ecx, %r10d
	movl	%r10d, %edi
	andl	%eax, %edi
	xorl	%edx, %edi
	addl	%esi, %edi
	leal	-1444681467(%r8,%rax), %esi
	movl	%ecx, %eax
	rorl	$12, %edi
	addl	%ecx, %edi
	xorl	%edi, %eax
	leal	-1926607734(%rbp,%rdi), %r10d
	andl	%edx, %eax
	xorl	%ecx, %eax
	movl	%r10d, -20(%rsp)
	addl	%esi, %eax
	movl	-64(%rsp), %esi
	roll	$5, %eax
	addl	%edi, %eax
	leal	-51403784(%rsi,%rdx), %esi
	movl	%edi, %edx
	xorl	%eax, %edx
	andl	%ecx, %edx
	leal	1735328473(%r13,%rcx), %ecx
	xorl	%edi, %edx
	addl	%esi, %edx
	movl	%eax, %esi
	roll	$9, %edx
	addl	%eax, %edx
	xorl	%edx, %esi
	andl	%edi, %esi
	xorl	%eax, %esi
	addl	%ecx, %esi
	movl	%edx, %ecx
	roll	$14, %esi
	addl	%edx, %esi
	xorl	%esi, %ecx
	movl	%ecx, %r10d
	andl	%eax, %r10d
	movl	%r10d, %edi
	movl	-52(%rsp), %r10d
	xorl	%edx, %edi
	addl	-20(%rsp), %edi
	leal	-378558(%r10,%rax), %eax
	rorl	$12, %edi
	addl	%esi, %edi
	xorl	%edi, %ecx
	addl	%eax, %ecx
	movl	-48(%rsp), %eax
	roll	$4, %ecx
	addl	%edi, %ecx
	leal	-2022574463(%rax,%rdx), %eax
	movl	%esi, %edx
	xorl	%edi, %edx
	xorl	%ecx, %edx
	addl	%eax, %edx
	movl	-36(%rsp), %eax
	roll	$11, %edx
	addl	%ecx, %edx
	leal	1839030562(%rax,%rsi), %eax
	movl	%edi, %esi
	leal	-35309556(%r12,%rdi), %edi
	xorl	%ecx, %esi
	xorl	%edx, %esi
	addl	%eax, %esi
	movl	%ecx, %eax
	roll	$16, %esi
	xorl	%edx, %eax
	addl	%edx, %esi
	xorl	%esi, %eax
	movl	%esi, %r10d
	addl	%edi, %eax
	movl	-68(%rsp), %edi
	rorl	$9, %eax
	addl	%esi, %eax
	leal	-1530992060(%rdi,%rcx), %edi
	movl	%edx, %ecx
	xorl	%eax, %r10d
	xorl	%esi, %ecx
	leal	-155497632(%r13,%rsi), %esi
	xorl	%eax, %ecx
	addl	%edi, %ecx
	movl	-56(%rsp), %edi
	roll	$4, %ecx
	addl	%eax, %ecx
	leal	1272893353(%rdi,%rdx), %edx
	movl	%r10d, %edi
	xorl	%ecx, %edi
	addl	%edx, %edi
	movl	%eax, %edx
	roll	$11, %edi
	xorl	%ecx, %edx
	addl	%ecx, %edi
	xorl	%edi, %edx
	addl	%esi, %edx
	movl	-40(%rsp), %esi
	roll	$16, %edx
	addl	%edi, %edx
	leal	-1094730640(%rsi,%rax), %eax
	movl	%ecx, %esi
	xorl	%edi, %esi
	xorl	%edx, %esi
	addl	%eax, %esi
	leal	681279174(%r8,%rcx), %eax
	movl	%edi, %ecx
	rorl	$9, %esi
	xorl	%edx, %ecx
	leal	-358537222(%r14,%rdi), %edi
	addl	%edx, %esi
	xorl	%esi, %ecx
	movl	%esi, %r10d
	addl	%eax, %ecx
	movl	%edx, %eax
	roll	$4, %ecx
	xorl	%esi, %eax
	addl	%esi, %ecx
	xorl	%ecx, %eax
	xorl	%ecx, %r10d
	addl	%edi, %eax
	movl	-60(%rsp), %edi
	roll	$11, %eax
	addl	%ecx, %eax
	leal	-722521979(%rdi,%rdx), %edx
	movl	%r10d, %edi
	xorl	%eax, %edi
	addl	%edx, %edi
	movl	-28(%rsp), %edx
	roll	$16, %edi
	addl	%eax, %edi
	leal	76029189(%rdx,%rsi), %edx
	movl	%ecx, %esi
	xorl	%eax, %esi
	xorl	%edi, %esi
	addl	%edx, %esi
	movl	-44(%rsp), %edx
	rorl	$9, %esi
	addl	%edi, %esi
	leal	-640364487(%rdx,%rcx), %ecx
	movl	%eax, %edx
	leal	-421815835(%rbp,%rax), %eax
	xorl	%edi, %edx
	movl	%esi, %r10d
	xorl	%esi, %edx
	addl	%ecx, %edx
	movl	%edi, %ecx
	roll	$4, %edx
	addl	%esi, %edx
	xorl	%esi, %ecx
	xorl	%edx, %ecx
	xorl	%edx, %r10d
	addl	%eax, %ecx
	leal	530742520(%r9,%rdi), %eax
	movl	%r10d, %edi
	roll	$11, %ecx
	movl	-52(%rsp), %r10d
	addl	%edx, %ecx
	xorl	%ecx, %edi
	addl	%eax, %edi
	movl	-64(%rsp), %eax
	roll	$16, %edi
	addl	%ecx, %edi
	leal	-995338651(%rax,%rsi), %esi
	movl	%edx, %eax
	xorl	%ecx, %eax
	xorl	%edi, %eax
	addl	%esi, %eax
	leal	-198630844(%r14,%rdx), %esi
	movl	%ecx, %edx
	rorl	$9, %eax
	notl	%edx
	movl	-68(%rsp), %r14d
	addl	%edi, %eax
	orl	%eax, %edx
	xorl	%edi, %edx
	addl	%esi, %edx
	leal	1126891415(%r13,%rcx), %esi
	movl	%edi, %r13d
	roll	$6, %edx
	notl	%r13d
	leal	-1416354905(%r12,%rdi), %edi
	addl	%eax, %edx
	movl	%r13d, %ecx
	orl	%edx, %ecx
	movl	%edx, %r13d
	xorl	%eax, %ecx
	notl	%r13d
	addl	%esi, %ecx
	movl	%eax, %esi
	roll	$10, %ecx
	notl	%esi
	addl	%edx, %ecx
	orl	%ecx, %esi
	movl	%ecx, %r12d
	xorl	%edx, %esi
	notl	%r12d
	addl	%edi, %esi
	leal	-57434055(%r10,%rax), %edi
	movl	%r13d, %eax
	roll	$15, %esi
	movl	-28(%rsp), %r10d
	addl	%ecx, %esi
	orl	%esi, %eax
	xorl	%ecx, %eax
	addl	%edi, %eax
	leal	1700485571(%rbp,%rdx), %edi
	movl	-60(%rsp), %ebp
	rorl	$11, %eax
	movl	%r12d, %edx
	addl	%esi, %eax
	orl	%eax, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	leal	-1894986606(%rbp,%rcx), %edi
	movl	%esi, %ebp
	roll	$6, %edx
	notl	%ebp
	addl	%eax, %edx
	movl	%ebp, %ecx
	orl	%edx, %ecx
	movl	%edx, %ebp
	xorl	%eax, %ecx
	notl	%ebp
	addl	%edi, %ecx
	movl	-40(%rsp), %edi
	roll	$10, %ecx
	addl	%edx, %ecx
	leal	-1051523(%rdi,%rsi), %edi
	movl	%eax, %esi
	notl	%esi
	orl	%ecx, %esi
	xorl	%edx, %esi
	addl	%edi, %esi
	leal	-2054922799(%r14,%rax), %edi
	movl	%ebp, %eax
	roll	$15, %esi
	movl	-48(%rsp), %r14d
	movl	%ecx, %ebp
	addl	%ecx, %esi
	notl	%ebp
	orl	%esi, %eax
	xorl	%ecx, %eax
	addl	%edi, %eax
	leal	1873313359(%r14,%rdx), %edi
	movl	%ebp, %edx
	rorl	$11, %eax
	movl	%esi, %ebp
	movl	-36(%rsp), %r14d
	addl	%esi, %eax
	notl	%ebp
	orl	%eax, %edx
	xorl	%esi, %edx
	addl	%edi, %edx
	leal	-30611744(%r9,%rcx), %edi
	movl	%ebp, %ecx
	roll	$6, %edx
	movl	-56(%rsp), %r9d
	addl	%eax, %edx
	orl	%edx, %ecx
	xorl	%eax, %ecx
	addl	%edi, %ecx
	leal	-1560198380(%r10,%rsi), %edi
	movl	%eax, %esi
	roll	$10, %ecx
	notl	%esi
	leal	1309151649(%r8,%rax), %eax
	addl	%edx, %ecx
	movl	%edx, %r8d
	movl	-44(%rsp), %r10d
	orl	%ecx, %esi
	notl	%r8d
	xorl	%edx, %esi
	leal	-145523070(%r9,%rdx), %edx
	movl	%ecx, %r9d
	addl	%edi, %esi
	movl	%r8d, %edi
	notl	%r9d
	roll	$15, %esi
	addl	%ecx, %esi
	orl	%esi, %edi
	xorl	%ecx, %edi
	leal	-1120210379(%r14,%rcx), %ecx
	addl	%eax, %edi
	movl	%r9d, %eax
	rorl	$11, %edi
	addl	%esi, %edi
	orl	%edi, %eax
	xorl	%esi, %eax
	addl	%edx, %eax
	movl	%esi, %edx
	roll	$6, %eax
	notl	%edx
	addl	%edi, %eax
	orl	%eax, %edx
	xorl	%edi, %edx
	addl	%ecx, %edx
	movl	-64(%rsp), %ecx
	roll	$10, %edx
	addl	%eax, %edx
	leal	718787259(%rcx,%rsi), %esi
	movl	%edi, %ecx
	leal	-343485551(%r10,%rdi), %edi
	notl	%ecx
	orl	%edx, %ecx
	xorl	%eax, %ecx
	addl	%esi, %ecx
	movl	%eax, %esi
	roll	$15, %ecx
	notl	%esi
	addl	%edx, %ecx
	orl	%ecx, %esi
	xorl	%edx, %esi
	addl	%edi, %esi
	rorl	$11, %esi
	addl	%ecx, %esi
	addl	%eax, -24(%rsp)
	addl	%ecx, %r15d
	addl	%esi, %r11d
	addl	%edx, -32(%rsp)
	cmpq	%rbx, -16(%rsp)
	ja	.L8
	movl	%r15d, %r10d
.L7:
	movq	-8(%rsp), %rax
	movl	-24(%rsp), %ebx
	movl	%ebx, (%rax)
	movl	-32(%rsp), %ebx
	movl	%r11d, 4(%rax)
	movl	%r10d, 8(%rax)
	movl	%ebx, 12(%rax)
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__md5_process_block, .-__md5_process_block
	.p2align 4,,15
	.globl	__md5_finish_ctx
	.type	__md5_finish_ctx, @function
__md5_finish_ctx:
	pushq	%r13
	pushq	%r12
	xorl	%edx, %edx
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	24(%rdi), %eax
	movl	%eax, %ecx
	addl	16(%rdi), %ecx
	setc	%dl
	movl	%ecx, 16(%rdi)
	testl	%edx, %edx
	je	.L14
	addl	$1, 20(%rdi)
.L14:
	cmpl	$55, %eax
	ja	.L21
	movl	$56, %esi
	movl	%esi, %ebp
	subl	%eax, %ebp
.L16:
	movl	%eax, %r12d
	leaq	fillbuf(%rip), %rsi
	movq	%rbp, %rdx
	leaq	28(%rbx,%r12), %rdi
	call	memcpy@PLT
	movl	16(%rbx), %edx
	leaq	0(%rbp,%r12), %rsi
	leaq	28(%rbx), %rdi
	movq	%rsi, %rax
	leal	0(,%rdx,8), %ecx
	shrq	$2, %rax
	shrl	$29, %edx
	movl	%ecx, 28(%rbx,%rax,4)
	movl	20(%rbx), %eax
	leaq	4(%rsi), %rcx
	addq	$8, %rsi
	shrq	$2, %rcx
	sall	$3, %eax
	orl	%edx, %eax
	movq	%rbx, %rdx
	movl	%eax, 28(%rbx,%rcx,4)
	call	__md5_process_block
	movl	(%rbx), %eax
	movl	%eax, 0(%r13)
	movl	4(%rbx), %eax
	movl	%eax, 4(%r13)
	movl	8(%rbx), %eax
	movl	%eax, 8(%r13)
	movl	12(%rbx), %eax
	movl	%eax, 12(%r13)
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$120, %esi
	movl	%esi, %ebp
	subl	%eax, %ebp
	jmp	.L16
	.size	__md5_finish_ctx, .-__md5_finish_ctx
	.p2align 4,,15
	.globl	__md5_process_bytes
	.type	__md5_process_bytes, @function
__md5_process_bytes:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	24(%rdx), %eax
	testl	%eax, %eax
	jne	.L61
.L23:
	cmpq	$63, %rbx
	ja	.L62
.L31:
	testq	%rbx, %rbx
	jne	.L63
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	24(%rbp), %esi
	cmpl	$8, %ebx
	movl	%ebx, %eax
	leaq	28(%rbp,%rsi), %rcx
	jnb	.L33
	testb	$4, %bl
	jne	.L64
	testl	%ebx, %ebx
	je	.L34
	movzbl	(%r12), %edx
	testb	$2, %al
	movb	%dl, (%rcx)
	jne	.L65
.L34:
	addq	%rsi, %rbx
	cmpq	$63, %rbx
	jbe	.L39
	leaq	28(%rbp), %r12
	movq	%rbp, %rdx
	movl	$64, %esi
	subq	$64, %rbx
	movq	%r12, %rdi
	call	__md5_process_block
	leaq	92(%rbp), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	memcpy@PLT
.L39:
	movl	%ebx, 24(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%rbx, %r13
	movq	%r12, %rdi
	movq	%rbp, %rdx
	andq	$-64, %r13
	andl	$63, %ebx
	movq	%r13, %rsi
	addq	%r13, %r12
	call	__md5_process_block
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L61:
	movl	%eax, %r13d
	movl	$128, %edx
	subq	%r13, %rdx
	leaq	28(%rbp,%r13), %rdi
	cmpq	%rsi, %rdx
	cmova	%rsi, %rdx
	movq	%r12, %rsi
	movq	%rdx, %r14
	call	memcpy@PLT
	movl	24(%rbp), %esi
	addl	%r14d, %esi
	cmpl	$64, %esi
	movl	%esi, 24(%rbp)
	ja	.L66
.L24:
	addq	%r14, %r12
	subq	%r14, %rbx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L33:
	movq	(%r12), %rax
	movq	%rax, (%rcx)
	movl	%ebx, %eax
	movq	-8(%r12,%rax), %rdx
	movq	%rdx, -8(%rcx,%rax)
	leaq	8(%rcx), %rdx
	andq	$-8, %rdx
	subq	%rdx, %rcx
	leal	(%rbx,%rcx), %eax
	subq	%rcx, %r12
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L34
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L37:
	movl	%ecx, %edi
	addl	$8, %ecx
	movq	(%r12,%rdi), %r8
	cmpl	%eax, %ecx
	movq	%r8, (%rdx,%rdi)
	jb	.L37
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	28(%rbp), %r15
	andl	$-64, %esi
	movq	%rbp, %rdx
	movq	%r15, %rdi
	call	__md5_process_block
	movl	24(%rbp), %ecx
	leaq	0(%r13,%r14), %rax
	andq	$-64, %rax
	movl	%ecx, %edx
	leaq	28(%rbp,%rax), %rax
	andl	$63, %edx
	cmpl	$8, %edx
	movl	%edx, 24(%rbp)
	jnb	.L25
	testb	$4, %cl
	jne	.L67
	testl	%edx, %edx
	je	.L24
	movzbl	(%rax), %esi
	andl	$2, %ecx
	movb	%sil, 28(%rbp)
	je	.L24
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%r15,%rdx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rax), %rcx
	movq	%rcx, 28(%rbp)
	movl	%edx, %ecx
	movq	-8(%rax,%rcx), %rsi
	movq	%rsi, -8(%r15,%rcx)
	leaq	36(%rbp), %rcx
	andq	$-8, %rcx
	subq	%rcx, %r15
	addl	%r15d, %edx
	subq	%r15, %rax
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L24
	andl	$-8, %edx
	xorl	%esi, %esi
.L29:
	movl	%esi, %edi
	addl	$8, %esi
	movq	(%rax,%rdi), %r8
	cmpl	%edx, %esi
	movq	%r8, (%rcx,%rdi)
	jb	.L29
	jmp	.L24
.L67:
	movl	(%rax), %ecx
	movl	%ecx, 28(%rbp)
	movl	-4(%rax,%rdx), %eax
	movl	%eax, -4(%r15,%rdx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L64:
	movl	(%r12), %edx
	movl	%edx, (%rcx)
	movl	%ebx, %edx
	movl	-4(%r12,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L34
.L65:
	movl	%ebx, %edx
	movzwl	-2(%r12,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L34
	.size	__md5_process_bytes, .-__md5_process_bytes
	.p2align 4,,15
	.globl	__md5_buffer
	.type	__md5_buffer, @function
__md5_buffer:
	pushq	%rbp
	pushq	%rbx
	movabsq	$-1167088121787636991, %rax
	movq	%rdx, %rbp
	subq	$168, %rsp
	movq	%rsp, %rbx
	movq	%rax, (%rsp)
	movabsq	$1167088121787636990, %rax
	movq	%rbx, %rdx
	movq	%rax, 8(%rsp)
	movq	$0, 16(%rsp)
	movl	$0, 24(%rsp)
	call	__md5_process_bytes
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__md5_finish_ctx
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__md5_buffer, .-__md5_buffer
	.p2align 4,,15
	.globl	__md5_stream
	.type	__md5_stream, @function
__md5_stream:
	pushq	%r15
	pushq	%r14
	movabsq	$-1167088121787636991, %rax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r15
	subq	$4344, %rsp
	movq	%rax, (%rsp)
	leaq	160(%rsp), %rbp
	movabsq	$1167088121787636990, %rax
	movq	%rax, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	%rsp, %r14
	movl	$0, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L76:
	xorl	%ebx, %ebx
	movl	$4096, %r12d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L87:
	testq	%rax, %rax
	je	.L80
.L71:
	leaq	0(%rbp,%rbx), %rdi
	movq	%r12, %rdx
	movq	%r13, %rcx
	subq	%rbx, %rdx
	movl	$1, %esi
	call	fread@PLT
	addq	%rax, %rbx
	cmpq	$4095, %rbx
	jbe	.L87
.L80:
	testq	%rax, %rax
	je	.L88
	movq	%r14, %rdx
	movl	$4096, %esi
	movq	%rbp, %rdi
	call	__md5_process_block
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%r13, %rdi
	call	ferror@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	jne	.L89
	testq	%rbx, %rbx
	movq	%rsp, %r13
	jne	.L77
.L78:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__md5_finish_ctx
.L70:
	addq	$4344, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L89:
	movl	$1, %r12d
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L77:
	movq	%r13, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__md5_process_bytes
	jmp	.L78
	.size	__md5_stream, .-__md5_stream
	.section	.rodata
	.align 32
	.type	fillbuf, @object
	.size	fillbuf, 64
fillbuf:
	.byte	-128
	.byte	0
	.zero	62
