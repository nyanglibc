	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"$"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__md5_crypt_r
	.type	__md5_crypt_r, @function
__md5_crypt_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	leaq	md5_salt_prefix(%rip), %rdi
	movq	%rsi, %rbx
	subq	$456, %rsp
	movq	%rdx, -440(%rbp)
	movl	$3, %edx
	movl	%ecx, -404(%rbp)
	call	strncmp@PLT
	leaq	3(%rbx), %rdx
	testl	%eax, %eax
	leaq	.LC0(%rip), %rsi
	cmovne	%rbx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -432(%rbp)
	call	strcspn@PLT
	cmpq	$8, %rax
	movq	%rax, %rcx
	movl	$8, %eax
	cmovb	%rcx, %rax
	movq	%r15, %rdi
	movq	%rax, -424(%rbp)
	call	strlen@PLT
	testb	$3, %r15b
	movq	%rax, -416(%rbp)
	je	.L34
	leaq	4(%rax), %rbx
	movq	%rbx, %rdi
	call	__libc_alloca_cutoff@PLT
	testl	%eax, %eax
	jne	.L5
	cmpq	$4096, %rbx
	ja	.L68
.L5:
	addq	$30, %rbx
	movq	$0, -456(%rbp)
	andq	$-16, %rbx
	subq	%rbx, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
.L7:
	movq	-416(%rbp), %rdx
	leaq	4(%rbx), %rdi
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	%rax, %r15
	movq	%rax, -464(%rbp)
.L4:
	testb	$3, -432(%rbp)
	movq	$0, -448(%rbp)
	jne	.L69
.L8:
	leaq	-368(%rbp), %r12
	leaq	-208(%rbp), %rbx
	movq	%r12, %rdi
	call	__md5_init_ctx@PLT
	movq	-416(%rbp), %r14
	movq	%r12, %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	call	__md5_process_bytes@PLT
	leaq	md5_salt_prefix(%rip), %rdi
	movq	%r12, %rdx
	movl	$3, %esi
	call	__md5_process_bytes@PLT
	movq	-432(%rbp), %r13
	movq	-424(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	call	__md5_process_bytes@PLT
	movq	%rbx, %rdi
	movq	%rbx, -472(%rbp)
	call	__md5_init_ctx@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__md5_process_bytes@PLT
	movq	-424(%rbp), %rsi
	movq	%r13, %rdi
	movq	%rbx, %rdx
	leaq	-384(%rbp), %r13
	call	__md5_process_bytes@PLT
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	__md5_process_bytes@PLT
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__md5_finish_ctx@PLT
	cmpq	$16, %r14
	jbe	.L15
	leaq	-17(%r14), %rax
	leaq	-16(%r14), %rcx
	movq	%rax, -488(%rbp)
	andq	$-16, %rax
	movq	%rcx, -480(%rbp)
	subq	%rax, %rcx
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rdx
	movl	$16, %esi
	movq	%r13, %rdi
	subq	$16, %r14
	call	__md5_process_bytes@PLT
	cmpq	%rbx, %r14
	jne	.L16
	movq	-488(%rbp), %r14
	movq	-480(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r13, %rdi
	andq	$-16, %r14
	subq	%r14, %rsi
	call	__md5_process_bytes@PLT
	movb	$0, -384(%rbp)
.L17:
	movq	-416(%rbp), %rbx
	.p2align 4,,10
	.p2align 3
.L20:
	testb	$1, %bl
	movq	%r15, %rdi
	movq	%r12, %rdx
	cmovne	%r13, %rdi
	movl	$1, %esi
	call	__md5_process_bytes@PLT
	shrq	%rbx
	jne	.L20
.L18:
	movq	%r13, %rsi
	movq	%r12, %rdi
	xorl	%ebx, %ebx
	call	__md5_finish_ctx@PLT
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L73:
	movq	-416(%rbp), %rsi
	movq	%r15, %rdi
	call	__md5_process_bytes@PLT
.L22:
	movabsq	$-6148914691236517205, %rax
	mulq	%rbx
	shrq	%rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rbx
	jne	.L70
.L23:
	movabsq	$5270498306774157605, %rax
	imulq	%rbx
	movq	%rbx, %rax
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	cmpq	%rax, %rbx
	jne	.L71
.L24:
	testq	%r14, %r14
	movq	%r12, %rdx
	je	.L25
	movl	$16, %esi
	movq	%r13, %rdi
	call	__md5_process_bytes@PLT
.L26:
	movq	%r13, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	__md5_finish_ctx@PLT
	cmpq	$1000, %rbx
	je	.L72
.L27:
	movq	%r12, %rdi
	movq	%rbx, %r14
	call	__md5_init_ctx@PLT
	andl	$1, %r14d
	movq	%r12, %rdx
	jne	.L73
	movl	$16, %esi
	movq	%r13, %rdi
	call	__md5_process_bytes@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L25:
	movq	-416(%rbp), %rsi
	movq	%r15, %rdi
	call	__md5_process_bytes@PLT
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L71:
	movq	-416(%rbp), %rsi
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	__md5_process_bytes@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L70:
	movq	-424(%rbp), %rsi
	movq	-432(%rbp), %rdi
	movq	%r12, %rdx
	call	__md5_process_bytes@PLT
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L72:
	movl	-404(%rbp), %edx
	xorl	%ebx, %ebx
	movq	-440(%rbp), %rdi
	leaq	md5_salt_prefix(%rip), %rsi
	testl	%edx, %edx
	movl	%ebx, %edx
	cmovns	-404(%rbp), %edx
	movslq	%edx, %rdx
	call	__stpncpy@PLT
	movl	-404(%rbp), %ecx
	movq	-424(%rbp), %r15
	movq	%rax, %rdi
	movq	-432(%rbp), %rsi
	movq	%rax, -392(%rbp)
	leal	-3(%rcx), %edx
	testl	%edx, %edx
	movl	%edx, -404(%rbp)
	cmovs	%ebx, %edx
	movslq	%edx, %rdx
	cmpq	%r15, %rdx
	cmova	%r15, %rdx
	call	__stpncpy@PLT
	movl	-404(%rbp), %ecx
	movq	%rax, -392(%rbp)
	testl	%ecx, %ecx
	cmovns	%ecx, %ebx
	movslq	%ebx, %rdx
	cmpq	%r15, %rdx
	cmova	%r15, %rdx
	subl	%edx, %ecx
	testl	%ecx, %ecx
	movl	%ecx, -404(%rbp)
	jle	.L28
	leaq	1(%rax), %rdx
	movq	%rdx, -392(%rbp)
	movb	$36, (%rax)
	subl	$1, -404(%rbp)
.L28:
	movzbl	-378(%rbp), %ecx
	movzbl	-384(%rbp), %edx
	leaq	-404(%rbp), %rbx
	movzbl	-372(%rbp), %r8d
	leaq	-392(%rbp), %r14
	movl	$4, %r9d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-377(%rbp), %ecx
	movzbl	-383(%rbp), %edx
	movl	$4, %r9d
	movzbl	-371(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-376(%rbp), %ecx
	movzbl	-382(%rbp), %edx
	movl	$4, %r9d
	movzbl	-370(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-375(%rbp), %ecx
	movzbl	-381(%rbp), %edx
	movl	$4, %r9d
	movzbl	-369(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-374(%rbp), %ecx
	movzbl	-380(%rbp), %edx
	movl	$4, %r9d
	movzbl	-379(%rbp), %r8d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-373(%rbp), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %r9d
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	__b64_from_24bit@PLT
	movl	-404(%rbp), %eax
	testl	%eax, %eax
	jle	.L74
	movq	-392(%rbp), %rax
	movq	-440(%rbp), %rbx
	movb	$0, (%rax)
.L30:
	movq	%r12, %rdi
	call	__md5_init_ctx@PLT
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__md5_finish_ctx@PLT
	movl	$156, %edx
	movl	$156, %esi
	movq	%r12, %rdi
	call	__explicit_bzero_chk@PLT
	movq	-472(%rbp), %rdi
	movl	$156, %edx
	movl	$156, %esi
	call	__explicit_bzero_chk@PLT
	movq	-464(%rbp), %rax
	testq	%rax, %rax
	je	.L31
	movq	-416(%rbp), %rsi
	movq	$-1, %rdx
	movq	%rax, %rdi
	call	__explicit_bzero_chk@PLT
.L31:
	movq	-448(%rbp), %rax
	testq	%rax, %rax
	je	.L32
	movq	-424(%rbp), %rsi
	movq	$-1, %rdx
	movq	%rax, %rdi
	call	__explicit_bzero_chk@PLT
.L32:
	movq	-456(%rbp), %rdi
	call	free@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L34:
	movq	$0, -456(%rbp)
	movq	$0, -464(%rbp)
	jmp	.L4
.L74:
	movq	errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$34, %fs:(%rax)
	jmp	.L30
.L69:
	movq	-424(%rbp), %rsi
	leaq	34(%rsi), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movl	%esi, %eax
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	cmpl	$8, %esi
	leaq	4(%rdx), %rcx
	jnb	.L9
	andl	$4, %esi
	jne	.L75
	testl	%eax, %eax
	je	.L10
	movq	-432(%rbp), %rsi
	testb	$2, %al
	movzbl	(%rsi), %esi
	movb	%sil, 4(%rdx)
	jne	.L76
.L10:
	movq	%rcx, -432(%rbp)
	movq	%rcx, -448(%rbp)
	jmp	.L8
.L9:
	movq	-432(%rbp), %rdi
	addq	$8, %rdx
	movq	(%rdi), %rax
	movq	%rax, -4(%rdx)
	movq	-424(%rbp), %rbx
	movl	%ebx, %eax
	movq	-8(%rdi,%rax), %rsi
	movq	%rsi, -8(%rcx,%rax)
	movq	%rcx, %rax
	subq	%rdx, %rax
	subq	%rax, %rdi
	addl	%ebx, %eax
	andl	$-8, %eax
	movq	%rdi, %r8
	cmpl	$8, %eax
	jb	.L10
	andl	$-8, %eax
	xorl	%esi, %esi
.L13:
	movl	%esi, %edi
	addl	$8, %esi
	movq	(%r8,%rdi), %r9
	cmpl	%eax, %esi
	movq	%r9, (%rdx,%rdi)
	jb	.L13
	jmp	.L10
.L15:
	movq	-416(%rbp), %rbx
	movq	%r12, %rdx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	__md5_process_bytes@PLT
	testq	%rbx, %rbx
	movb	$0, -384(%rbp)
	je	.L18
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-432(%rbp), %rdi
	movl	(%rdi), %esi
	movl	%esi, 4(%rdx)
	movl	-4(%rdi,%rax), %edx
	movl	%edx, -4(%rcx,%rax)
	jmp	.L10
.L68:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1
	movq	%rax, -456(%rbp)
	jmp	.L7
.L76:
	movq	-432(%rbp), %rsi
	movzwl	-2(%rsi,%rax), %edx
	movw	%dx, -2(%rcx,%rax)
	jmp	.L10
	.size	__md5_crypt_r, .-__md5_crypt_r
	.p2align 4,,15
	.globl	__md5_crypt
	.type	__md5_crypt, @function
__md5_crypt:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	call	strlen@PLT
	movl	buflen.5407(%rip), %ecx
	leal	31(%rax), %ebx
	movq	buffer(%rip), %rdx
	cmpl	%ebx, %ecx
	jge	.L78
	movq	%rdx, %rdi
	movslq	%ebx, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L77
	movq	%rax, buffer(%rip)
	movl	%ebx, buflen.5407(%rip)
	movl	%ebx, %ecx
.L78:
	popq	%rbx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	popq	%rbp
	popq	%r12
	jmp	__md5_crypt_r@PLT
	.p2align 4,,10
	.p2align 3
.L77:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	__md5_crypt, .-__md5_crypt
	.local	buflen.5407
	.comm	buflen.5407,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.section	.rodata.str1.1
	.type	md5_salt_prefix, @object
	.size	md5_salt_prefix, 4
md5_salt_prefix:
	.string	"$1$"
