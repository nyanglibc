	.file	"sha256.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/crypt
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/crypt/sha256.v.d -MF /run/asm/crypt/sha256.o.dt -MP
# -MT /run/asm/crypt/.o -D _LIBC_REENTRANT -D MODULE_NAME=libcrypt -D PIC
# -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h sha256.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/crypt/sha256.v.s -O2 -Wall -Wwrite-strings
# -Wundef -Werror -Wstrict-prototypes -Wold-style-definition -std=gnu11
# -fverbose-asm -fgnu89-inline -fmerge-all-constants -frounding-math
# -fno-stack-protector -fmath-errno -fpie -ftls-model=initial-exec
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fpic -fpie
# -fplt -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.globl	__sha256_init_ctx
	.type	__sha256_init_ctx, @function
__sha256_init_ctx:
.LFB32:
	.cfi_startproc
# sha256.c:91:   ctx->H[0] = 0x6a09e667;
	movabsq	$-4942790177982912921, %rax	#, tmp92
# sha256.c:98:   ctx->H[7] = 0x5be0cd19;
	movq	$0, 32(%rdi)	#, MEM[(void *)ctx_2(D) + 32B]
# sha256.c:100:   ctx->total64 = 0;
	movl	$0, 40(%rdi)	#, MEM[(void *)ctx_2(D) + 40B]
# sha256.c:91:   ctx->H[0] = 0x6a09e667;
	movq	%rax, (%rdi)	# tmp92, MEM[(unsigned int *)ctx_2(D)]
# sha256.c:92:   ctx->H[1] = 0xbb67ae85;
	movabsq	$-6534734903820487822, %rax	#, tmp93
	movq	%rax, 8(%rdi)	# tmp93, MEM[(unsigned int *)ctx_2(D) + 8B]
# sha256.c:94:   ctx->H[3] = 0xa54ff53a;
	movabsq	$-7276294671082564993, %rax	#, tmp94
	movq	%rax, 16(%rdi)	# tmp94, MEM[(unsigned int *)ctx_2(D) + 16B]
# sha256.c:96:   ctx->H[5] = 0x9b05688c;
	movabsq	$6620516960021240235, %rax	#, tmp95
	movq	%rax, 24(%rdi)	# tmp95, MEM[(void *)ctx_2(D) + 24B]
# sha256.c:102: }
	ret
	.cfi_endproc
.LFE32:
	.size	__sha256_init_ctx, .-__sha256_init_ctx
	.p2align 4,,15
	.globl	__sha256_process_block
	.type	__sha256_process_block, @function
__sha256_process_block:
.LFB35:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
# ./sha256-block.c:9:   size_t nwords = len / sizeof (uint32_t);
	movq	%rsi, %rbx	# len, nwords
	shrq	$2, %rbx	#, nwords
# ./sha256-block.c:7: {
	subq	$208, %rsp	#,
	.cfi_def_cfa_offset 264
# ./sha256-block.c:22:   ctx->total64 += len;
	addq	%rsi, 32(%rdx)	# len, ctx_80(D)->D.4694.total64
# ./sha256-block.c:26:   while (nwords > 0)
	testq	%rbx, %rbx	# nwords
# ./sha256-block.c:7: {
	movq	%rdi, -88(%rsp)	# buffer, %sfp
# ./sha256-block.c:10:   uint32_t a = ctx->H[0];
	movl	(%rdx), %edi	# ctx_80(D)->H, a
# ./sha256-block.c:7: {
	movq	%rdx, -64(%rsp)	# ctx, %sfp
# ./sha256-block.c:9:   size_t nwords = len / sizeof (uint32_t);
	movq	%rbx, -80(%rsp)	# nwords, %sfp
# ./sha256-block.c:10:   uint32_t a = ctx->H[0];
	movl	%edi, -108(%rsp)	# a, %sfp
# ./sha256-block.c:11:   uint32_t b = ctx->H[1];
	movl	4(%rdx), %edi	# ctx_80(D)->H, b
	movl	%edi, -104(%rsp)	# b, %sfp
# ./sha256-block.c:12:   uint32_t c = ctx->H[2];
	movl	8(%rdx), %edi	# ctx_80(D)->H, c
	movl	%edi, -100(%rsp)	# c, %sfp
# ./sha256-block.c:13:   uint32_t d = ctx->H[3];
	movl	12(%rdx), %edi	# ctx_80(D)->H, d
	movl	%edi, -112(%rsp)	# d, %sfp
# ./sha256-block.c:14:   uint32_t e = ctx->H[4];
	movl	16(%rdx), %edi	# ctx_80(D)->H, e
	movl	%edi, -96(%rsp)	# e, %sfp
# ./sha256-block.c:15:   uint32_t f = ctx->H[5];
	movl	20(%rdx), %edi	# ctx_80(D)->H, f
	movl	%edi, -116(%rsp)	# f, %sfp
# ./sha256-block.c:16:   uint32_t g = ctx->H[6];
	movl	24(%rdx), %edi	# ctx_80(D)->H, g
	movl	%edi, -92(%rsp)	# g, %sfp
# ./sha256-block.c:17:   uint32_t h = ctx->H[7];
	movl	28(%rdx), %edi	# ctx_80(D)->H, h
	movl	%edi, -120(%rsp)	# h, %sfp
# ./sha256-block.c:26:   while (nwords > 0)
	je	.L4	#,
	leaq	-56(%rsp), %r14	#, tmp241
	leaq	K(%rip), %r15	#, tmp242
	leaq	192(%r14), %rax	#, _214
	movq	%rax, -72(%rsp)	# _214, %sfp
	.p2align 4,,10
	.p2align 3
.L9:
# ./sha256-block.c:7: {
	movq	-88(%rsp), %rcx	# %sfp, buffer
	xorl	%eax, %eax	# ivtmp.43
	.p2align 4,,10
	.p2align 3
.L5:
# ../bits/byteswap.h:52:   return __builtin_bswap32 (__bsx);
	movl	(%rcx,%rax), %edx	# MEM[base: words_139, index: ivtmp.43_213, offset: 0B], MEM[base: words_139, index: ivtmp.43_213, offset: 0B]
	bswap	%edx	# _129
# ./sha256-block.c:53: 	  W[t] = SWAP (*words);
	movl	%edx, (%r14,%rax)	# _129, MEM[symbol: W, index: ivtmp.43_213, offset: 0B]
	addq	$4, %rax	#, ivtmp.43
# ./sha256-block.c:51:       for (unsigned int t = 0; t < 16; ++t)
	cmpq	$64, %rax	#, ivtmp.43
	jne	.L5	#,
	addq	$64, -88(%rsp)	#, %sfp
	movq	-72(%rsp), %rdi	# %sfp, _214
	movq	%r14, %rsi	# tmp241, ivtmp.39
	.p2align 4,,10
	.p2align 3
.L6:
# ./sha256-block.c:57: 	W[t] = R1 (W[t - 2]) + W[t - 7] + R0 (W[t - 15]) + W[t - 16];
	movl	56(%rsi), %eax	# MEM[base: _217, offset: 56B], _5
	movl	4(%rsi), %ecx	# MEM[base: _217, offset: 4B], _18
	addq	$4, %rsi	#, ivtmp.39
	movl	%eax, %edx	# _5, tmp202
	movl	%eax, %r8d	# _5, tmp203
	shrl	$10, %eax	#, tmp205
	roll	$13, %r8d	#, tmp203
	roll	$15, %edx	#, tmp202
	xorl	%r8d, %edx	# tmp203, tmp204
	movl	%ecx, %r8d	# _18, tmp212
	xorl	%eax, %edx	# tmp205, tmp206
	movl	-4(%rsi), %eax	# MEM[base: _217, offset: 0B], MEM[base: _217, offset: 0B]
	addl	32(%rsi), %eax	# MEM[base: _217, offset: 36B], tmp207
	roll	$14, %r8d	#, tmp212
	addl	%eax, %edx	# tmp207, tmp210
	movl	%ecx, %eax	# _18, tmp211
	shrl	$3, %ecx	#, tmp214
	rorl	$7, %eax	#, tmp211
	xorl	%r8d, %eax	# tmp212, tmp213
	xorl	%ecx, %eax	# tmp214, tmp215
	addl	%edx, %eax	# tmp210, tmp216
	movl	%eax, 60(%rsi)	# tmp216, MEM[base: _217, offset: 64B]
# ./sha256-block.c:56:       for (unsigned int t = 16; t < 64; ++t)
	cmpq	%rsi, %rdi	# ivtmp.39, _214
	jne	.L6	#,
	movl	-120(%rsp), %eax	# %sfp, h
	movl	-92(%rsp), %ebx	# %sfp, g
	xorl	%r9d, %r9d	# ivtmp.29
	movl	-116(%rsp), %ebp	# %sfp, f
	movl	-96(%rsp), %edi	# %sfp, e
	movl	$1116352408, %r12d	#, pretmp_255
	movl	-112(%rsp), %r13d	# %sfp, d
	movl	-100(%rsp), %r10d	# %sfp, c
	movl	-104(%rsp), %r11d	# %sfp, b
	movl	-108(%rsp), %r8d	# %sfp, a
	movl	%eax, %esi	# h, h
	jmp	.L8	#
	.p2align 4,,10
	.p2align 3
.L17:
	movl	(%r15,%r9), %r12d	# MEM[symbol: K, index: ivtmp.29_224, offset: 0B], pretmp_255
	movl	%r10d, %r13d	# c, d
	movl	%ebx, %esi	# g, h
# ./sha256-block.c:67: 	  e = d + T1;
	movl	%r11d, %r10d	# b, c
	movl	%ebp, %ebx	# f, g
	movl	%r8d, %r11d	# a, b
	movl	%edi, %ebp	# e, f
# ./sha256-block.c:71: 	  a = T1 + T2;
	movl	%eax, %r8d	# a, a
# ./sha256-block.c:67: 	  e = d + T1;
	movl	%ecx, %edi	# e, e
.L8:
# ./sha256-block.c:62: 	  uint32_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	movl	%edi, %eax	# e, tmp217
	movl	%edi, %edx	# e, tmp218
	movl	%edi, %ecx	# e, tmp224
	rorl	$11, %edx	#, tmp218
	rorl	$6, %eax	#, tmp217
	andl	%ebp, %ecx	# f, tmp224
	xorl	%edx, %eax	# tmp218, tmp219
	movl	%edi, %edx	# e, tmp220
	roll	$7, %edx	#, tmp220
	xorl	%eax, %edx	# tmp219, tmp221
	movl	%edi, %eax	# e, tmp222
	notl	%eax	# tmp222
	andl	%ebx, %eax	# g, tmp223
	xorl	%ecx, %eax	# tmp224, tmp225
# ./sha256-block.c:63: 	  uint32_t T2 = S0 (a) + Maj (a, b, c);
	movl	%r8d, %ecx	# a, tmp232
# ./sha256-block.c:62: 	  uint32_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addl	%edx, %eax	# tmp221, tmp226
# ./sha256-block.c:63: 	  uint32_t T2 = S0 (a) + Maj (a, b, c);
	movl	%r8d, %edx	# a, tmp231
# ./sha256-block.c:62: 	  uint32_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addl	(%r14,%r9), %eax	# MEM[symbol: W, index: ivtmp.29_225, offset: 0B], tmp228
# ./sha256-block.c:63: 	  uint32_t T2 = S0 (a) + Maj (a, b, c);
	rorl	$13, %ecx	#, tmp232
	rorl	$2, %edx	#, tmp231
	addq	$4, %r9	#, ivtmp.29
	xorl	%ecx, %edx	# tmp232, tmp233
	movl	%r8d, %ecx	# a, tmp234
	roll	$10, %ecx	#, tmp234
	xorl	%edx, %ecx	# tmp233, tmp235
	movl	%r11d, %edx	# b, tmp236
# ./sha256-block.c:62: 	  uint32_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addl	%esi, %eax	# h, tmp230
# ./sha256-block.c:63: 	  uint32_t T2 = S0 (a) + Maj (a, b, c);
	xorl	%r10d, %edx	# c, tmp236
	movl	%r11d, %esi	# b, tmp238
# ./sha256-block.c:62: 	  uint32_t T1 = h + S1 (e) + Ch (e, f, g) + K[t] + W[t];
	addl	%r12d, %eax	# pretmp_255, T1
# ./sha256-block.c:63: 	  uint32_t T2 = S0 (a) + Maj (a, b, c);
	andl	%r8d, %edx	# a, tmp237
	andl	%r10d, %esi	# c, tmp238
	xorl	%esi, %edx	# tmp238, tmp239
	addl	%ecx, %edx	# tmp235, T2
# ./sha256-block.c:67: 	  e = d + T1;
	leal	(%rax,%r13), %ecx	#, e
# ./sha256-block.c:71: 	  a = T1 + T2;
	addl	%edx, %eax	# T2, a
# ./sha256-block.c:60:       for (unsigned int t = 0; t < 64; ++t)
	cmpq	$256, %r9	#, ivtmp.29
	jne	.L17	#,
# ./sha256-block.c:76:       a += a_save;
	addl	%eax, -108(%rsp)	# a, %sfp
# ./sha256-block.c:77:       b += b_save;
	addl	%r8d, -104(%rsp)	# a, %sfp
# ./sha256-block.c:78:       c += c_save;
	addl	%r11d, -100(%rsp)	# b, %sfp
# ./sha256-block.c:79:       d += d_save;
	addl	%r10d, -112(%rsp)	# c, %sfp
# ./sha256-block.c:80:       e += e_save;
	addl	%ecx, -96(%rsp)	# e, %sfp
# ./sha256-block.c:81:       f += f_save;
	addl	%edi, -116(%rsp)	# e, %sfp
# ./sha256-block.c:82:       g += g_save;
	addl	%ebp, -92(%rsp)	# f, %sfp
# ./sha256-block.c:83:       h += h_save;
	addl	%ebx, -120(%rsp)	# g, %sfp
# ./sha256-block.c:26:   while (nwords > 0)
	subq	$16, -80(%rsp)	#, %sfp
	jne	.L9	#,
.L4:
# ./sha256-block.c:90:   ctx->H[0] = a;
	movq	-64(%rsp), %rax	# %sfp, ctx
	movl	-108(%rsp), %ebx	# %sfp, a
	movl	%ebx, (%rax)	# a, ctx_80(D)->H
# ./sha256-block.c:91:   ctx->H[1] = b;
	movl	-104(%rsp), %ebx	# %sfp, b
	movl	%ebx, 4(%rax)	# b, ctx_80(D)->H
# ./sha256-block.c:92:   ctx->H[2] = c;
	movl	-100(%rsp), %ebx	# %sfp, c
	movl	%ebx, 8(%rax)	# c, ctx_80(D)->H
# ./sha256-block.c:93:   ctx->H[3] = d;
	movl	-112(%rsp), %ebx	# %sfp, d
	movl	%ebx, 12(%rax)	# d, ctx_80(D)->H
# ./sha256-block.c:94:   ctx->H[4] = e;
	movl	-96(%rsp), %ebx	# %sfp, e
	movl	%ebx, 16(%rax)	# e, ctx_80(D)->H
# ./sha256-block.c:95:   ctx->H[5] = f;
	movl	-116(%rsp), %ebx	# %sfp, f
	movl	%ebx, 20(%rax)	# f, ctx_80(D)->H
# ./sha256-block.c:96:   ctx->H[6] = g;
	movl	-92(%rsp), %ebx	# %sfp, g
	movl	%ebx, 24(%rax)	# g, ctx_80(D)->H
# ./sha256-block.c:97:   ctx->H[7] = h;
	movl	-120(%rsp), %ebx	# %sfp, h
	movl	%ebx, 28(%rax)	# h, ctx_80(D)->H
# ./sha256-block.c:98: }
	addq	$208, %rsp	#,
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE35:
	.size	__sha256_process_block, .-__sha256_process_block
	.p2align 4,,15
	.globl	__sha256_finish_ctx
	.type	__sha256_finish_ctx, @function
__sha256_finish_ctx:
.LFB33:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbp	# resbuf, resbuf
	movq	%rdi, %rbx	# ctx, ctx
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 48
# sha256.c:114:   uint32_t bytes = ctx->buflen;
	movl	40(%rdi), %r12d	# ctx_21(D)->buflen,
# sha256.c:118:   ctx->total64 += bytes;
	addq	%r12, 32(%rdi)	# _2, ctx_21(D)->D.4694.total64
# sha256.c:120:   pad = bytes >= 56 ? 64 + 56 - bytes : 56 - bytes;
	cmpl	$55, %r12d	#, bytes
	jbe	.L19	#,
# sha256.c:120:   pad = bytes >= 56 ? 64 + 56 - bytes : 56 - bytes;
	movl	$120, %r13d	#, tmp114
	subl	%r12d, %r13d	# bytes, iftmp.0_18
.L20:
# sha256.c:121:   memcpy (&ctx->buffer[bytes], fillbuf, pad);
	leaq	48(%rbx,%r12), %rdi	#, tmp118
	leaq	fillbuf(%rip), %rsi	#,
	movq	%r13, %rdx	# iftmp.0_18,
# sha256.c:125:   ctx->buffer64[(bytes + pad) / 8] = SWAP64 (ctx->total64 << 3);
	addq	%r13, %r12	# iftmp.0_18, _9
# sha256.c:121:   memcpy (&ctx->buffer[bytes], fillbuf, pad);
	call	memcpy@PLT	#
# sha256.c:125:   ctx->buffer64[(bytes + pad) / 8] = SWAP64 (ctx->total64 << 3);
	movq	32(%rbx), %rax	# ctx_21(D)->D.4694.total64, tmp137
	movq	%r12, %rdx	# _9, tmp127
# sha256.c:133:   __sha256_process_block (ctx->buffer, bytes + pad + 8, ctx);
	leaq	8(%r12), %rsi	#, tmp129
# sha256.c:125:   ctx->buffer64[(bytes + pad) / 8] = SWAP64 (ctx->total64 << 3);
	shrq	$3, %rdx	#, tmp127
# sha256.c:133:   __sha256_process_block (ctx->buffer, bytes + pad + 8, ctx);
	leaq	48(%rbx), %rdi	#, tmp130
# sha256.c:125:   ctx->buffer64[(bytes + pad) / 8] = SWAP64 (ctx->total64 << 3);
	salq	$3, %rax	#, tmp125
# ../bits/byteswap.h:73:   return __builtin_bswap64 (__bsx);
	bswap	%rax	# _31
# sha256.c:125:   ctx->buffer64[(bytes + pad) / 8] = SWAP64 (ctx->total64 << 3);
	movq	%rax, 48(%rbx,%rdx,8)	# _31, ctx_21(D)->D.4700.buffer64
# sha256.c:133:   __sha256_process_block (ctx->buffer, bytes + pad + 8, ctx);
	movq	%rbx, %rdx	# ctx,
	call	__sha256_process_block	#
	xorl	%eax, %eax	# ivtmp.68
	.p2align 4,,10
	.p2align 3
.L21:
# ../bits/byteswap.h:52:   return __builtin_bswap32 (__bsx);
	movl	(%rbx,%rax), %edx	# MEM[base: ctx_21(D), index: ivtmp.68_36, offset: 0B], MEM[base: ctx_21(D), index: ivtmp.68_36, offset: 0B]
	bswap	%edx	# _27
# sha256.c:137:     ((uint32_t *) resbuf)[i] = SWAP (ctx->H[i]);
	movl	%edx, 0(%rbp,%rax)	# _27, MEM[base: resbuf_30(D), index: ivtmp.68_36, offset: 0B]
	addq	$4, %rax	#, ivtmp.68
# sha256.c:136:   for (unsigned int i = 0; i < 8; ++i)
	cmpq	$32, %rax	#, ivtmp.68
	jne	.L21	#,
# sha256.c:140: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	movq	%rbp, %rax	# resbuf,
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	.cfi_restore_state
# sha256.c:120:   pad = bytes >= 56 ? 64 + 56 - bytes : 56 - bytes;
	movl	$56, %r13d	#, tmp116
	subl	%r12d, %r13d	# bytes, iftmp.0_18
	jmp	.L20	#
	.cfi_endproc
.LFE33:
	.size	__sha256_finish_ctx, .-__sha256_finish_ctx
	.p2align 4,,15
	.globl	__sha256_process_bytes
	.type	__sha256_process_bytes, @function
__sha256_process_bytes:
.LFB34:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdi, %r12	# buffer, buffer
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rdx, %rbp	# ctx, ctx
	movq	%rsi, %rbx	# len, len
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 64
# sha256.c:148:   if (ctx->buflen != 0)
	movl	40(%rdx), %eax	# ctx_32(D)->buflen, _1
	testl	%eax, %eax	# _1
	jne	.L63	#,
.L25:
# sha256.c:171:   if (len >= 64)
	cmpq	$63, %rbx	#, len
	ja	.L64	#,
.L33:
# sha256.c:198:   if (len > 0)
	testq	%rbx, %rbx	# len
	jne	.L65	#,
# sha256.c:212: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	.cfi_restore_state
# sha256.c:200:       size_t left_over = ctx->buflen;
	movl	40(%rbp), %esi	# ctx_32(D)->buflen, left_over
# sha256.c:202:       memcpy (&ctx->buffer[left_over], buffer, len);
	cmpl	$8, %ebx	#, len
	movl	%ebx, %eax	# len, len
	leaq	48(%rbp,%rsi), %rcx	#, tmp172
	jnb	.L35	#,
	testb	$4, %bl	#, len
	jne	.L66	#,
	testl	%ebx, %ebx	# len
	je	.L36	#,
	movzbl	(%r12), %edx	#* buffer, tmp185
	testb	$2, %al	#, len
	movb	%dl, (%rcx)	# tmp185,
	jne	.L67	#,
.L36:
# sha256.c:203:       left_over += len;
	addq	%rsi, %rbx	# left_over, left_over
# sha256.c:204:       if (left_over >= 64)
	cmpq	$63, %rbx	#, left_over
	jbe	.L41	#,
# sha256.c:206: 	  __sha256_process_block (ctx->buffer, 64, ctx);
	leaq	48(%rbp), %r12	#, _18
	movq	%rbp, %rdx	# ctx,
	movl	$64, %esi	#,
# sha256.c:207: 	  left_over -= 64;
	subq	$64, %rbx	#, left_over
# sha256.c:206: 	  __sha256_process_block (ctx->buffer, 64, ctx);
	movq	%r12, %rdi	# _18,
	call	__sha256_process_block	#
# sha256.c:208: 	  memcpy (ctx->buffer, &ctx->buffer[64], left_over);
	leaq	112(%rbp), %rsi	#, tmp208
	movq	%rbx, %rdx	# left_over,
	movq	%r12, %rdi	# _18,
	call	memcpy@PLT	#
.L41:
# sha256.c:210:       ctx->buflen = left_over;
	movl	%ebx, 40(%rbp)	# left_over, ctx_32(D)->buflen
# sha256.c:212: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	.cfi_restore_state
# sha256.c:191: 	  __sha256_process_block (buffer, len & ~63, ctx);
	movq	%rbx, %r13	# len, _15
	movq	%r12, %rdi	# buffer,
	movq	%rbp, %rdx	# ctx,
	andq	$-64, %r13	#, _15
# sha256.c:193: 	  len &= 63;
	andl	$63, %ebx	#, len
# sha256.c:191: 	  __sha256_process_block (buffer, len & ~63, ctx);
	movq	%r13, %rsi	# _15,
# sha256.c:192: 	  buffer = (const char *) buffer + (len & ~63);
	addq	%r13, %r12	# _15, buffer
# sha256.c:191: 	  __sha256_process_block (buffer, len & ~63, ctx);
	call	__sha256_process_block	#
	jmp	.L33	#
	.p2align 4,,10
	.p2align 3
.L63:
# sha256.c:150:       size_t left_over = ctx->buflen;
	movl	%eax, %r13d	# _1, left_over
# sha256.c:151:       size_t add = 128 - left_over > len ? len : 128 - left_over;
	movl	$128, %edx	#, tmp119
	subq	%r13, %rdx	# left_over, tmp118
# sha256.c:153:       memcpy (&ctx->buffer[left_over], buffer, add);
	leaq	48(%rbp,%r13), %rdi	#, tmp121
# sha256.c:151:       size_t add = 128 - left_over > len ? len : 128 - left_over;
	cmpq	%rsi, %rdx	# len, tmp118
	cmova	%rsi, %rdx	# tmp118,, len, tmp118
# sha256.c:153:       memcpy (&ctx->buffer[left_over], buffer, add);
	movq	%r12, %rsi	# buffer,
# sha256.c:151:       size_t add = 128 - left_over > len ? len : 128 - left_over;
	movq	%rdx, %r14	# tmp118, add
# sha256.c:153:       memcpy (&ctx->buffer[left_over], buffer, add);
	call	memcpy@PLT	#
# sha256.c:154:       ctx->buflen += add;
	movl	40(%rbp), %esi	# ctx_32(D)->buflen, _6
	addl	%r14d, %esi	# add, _6
# sha256.c:156:       if (ctx->buflen > 64)
	cmpl	$64, %esi	#, _6
# sha256.c:154:       ctx->buflen += add;
	movl	%esi, 40(%rbp)	# _6, ctx_32(D)->buflen
# sha256.c:156:       if (ctx->buflen > 64)
	ja	.L68	#,
.L26:
# sha256.c:166:       buffer = (const char *) buffer + add;
	addq	%r14, %r12	# add, buffer
# sha256.c:167:       len -= add;
	subq	%r14, %rbx	# add, len
	jmp	.L25	#
	.p2align 4,,10
	.p2align 3
.L35:
# sha256.c:202:       memcpy (&ctx->buffer[left_over], buffer, len);
	movq	(%r12), %rax	#* buffer, tmp194
	movq	%rax, (%rcx)	# tmp194,
	movl	%ebx, %eax	# len, len
	movq	-8(%r12,%rax), %rdx	#, tmp201
	movq	%rdx, -8(%rcx,%rax)	# tmp201,
	leaq	8(%rcx), %rdx	#, tmp202
	andq	$-8, %rdx	#, tmp202
	subq	%rdx, %rcx	# tmp202, tmp174
	leal	(%rbx,%rcx), %eax	#, len
	subq	%rcx, %r12	# tmp174, buffer
	andl	$-8, %eax	#, len
	cmpl	$8, %eax	#, len
	jb	.L36	#,
	andl	$-8, %eax	#, tmp204
	xorl	%ecx, %ecx	# tmp203
.L39:
	movl	%ecx, %edi	# tmp203, tmp205
	addl	$8, %ecx	#, tmp203
	movq	(%r12,%rdi), %r8	#, tmp206
	cmpl	%eax, %ecx	# tmp204, tmp203
	movq	%r8, (%rdx,%rdi)	# tmp206,
	jb	.L39	#,
	jmp	.L36	#
	.p2align 4,,10
	.p2align 3
.L68:
# sha256.c:158: 	  __sha256_process_block (ctx->buffer, ctx->buflen & ~63, ctx);
	leaq	48(%rbp), %r15	#, _8
	andl	$-64, %esi	#, tmp129
	movq	%rbp, %rdx	# ctx,
	movq	%r15, %rdi	# _8,
	call	__sha256_process_block	#
# sha256.c:160: 	  ctx->buflen &= 63;
	movl	40(%rbp), %ecx	# ctx_32(D)->buflen, ctx_32(D)->buflen
# sha256.c:162: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	leaq	0(%r13,%r14), %rax	#, tmp132
	andq	$-64, %rax	#, tmp133
# sha256.c:160: 	  ctx->buflen &= 63;
	movl	%ecx, %edx	# ctx_32(D)->buflen, _10
# sha256.c:162: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	leaq	48(%rbp,%rax), %rax	#, tmp135
# sha256.c:160: 	  ctx->buflen &= 63;
	andl	$63, %edx	#,
# sha256.c:162: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	cmpl	$8, %edx	#, _10
# sha256.c:160: 	  ctx->buflen &= 63;
	movl	%edx, 40(%rbp)	# _10, ctx_32(D)->buflen
# sha256.c:162: 	  memcpy (ctx->buffer, &ctx->buffer[(left_over + add) & ~63],
	jnb	.L27	#,
	testb	$4, %cl	#, ctx_32(D)->buflen
	jne	.L69	#,
	testl	%edx, %edx	# _10
	je	.L26	#,
	movzbl	(%rax), %esi	#, tmp148
	andl	$2, %ecx	#, ctx_32(D)->buflen
	movb	%sil, 48(%rbp)	# tmp148,
	je	.L26	#,
	movzwl	-2(%rax,%rdx), %eax	#, tmp156
	movw	%ax, -2(%r15,%rdx)	# tmp156,
	jmp	.L26	#
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rax), %rcx	#, tmp157
	movq	%rcx, 48(%rbp)	# tmp157,
	movl	%edx, %ecx	# _10, _10
	movq	-8(%rax,%rcx), %rsi	#, tmp164
	movq	%rsi, -8(%r15,%rcx)	# tmp164,
	leaq	56(%rbp), %rcx	#, tmp165
	andq	$-8, %rcx	#, tmp165
	subq	%rcx, %r15	# tmp165, _8
	addl	%r15d, %edx	# _8, _10
	subq	%r15, %rax	# _8, tmp138
	andl	$-8, %edx	#, _10
	cmpl	$8, %edx	#, _10
	jb	.L26	#,
	andl	$-8, %edx	#, tmp167
	xorl	%esi, %esi	# tmp166
.L31:
	movl	%esi, %edi	# tmp166, tmp168
	addl	$8, %esi	#, tmp166
	movq	(%rax,%rdi), %r8	#, tmp169
	cmpl	%edx, %esi	# tmp167, tmp166
	movq	%r8, (%rcx,%rdi)	# tmp169,
	jb	.L31	#,
	jmp	.L26	#
.L69:
	movl	(%rax), %ecx	#, tmp140
	movl	%ecx, 48(%rbp)	# tmp140,
	movl	-4(%rax,%rdx), %eax	#, tmp147
	movl	%eax, -4(%r15,%rdx)	# tmp147,
	jmp	.L26	#
	.p2align 4,,10
	.p2align 3
.L66:
# sha256.c:202:       memcpy (&ctx->buffer[left_over], buffer, len);
	movl	(%r12), %edx	#* buffer, tmp177
	movl	%edx, (%rcx)	# tmp177,
	movl	%ebx, %edx	# len, len
	movl	-4(%r12,%rdx), %eax	#, tmp184
	movl	%eax, -4(%rcx,%rdx)	# tmp184,
	jmp	.L36	#
.L67:
	movl	%ebx, %edx	# len, len
	movzwl	-2(%r12,%rdx), %eax	#, tmp193
	movw	%ax, -2(%rcx,%rdx)	# tmp193,
	jmp	.L36	#
	.cfi_endproc
.LFE34:
	.size	__sha256_process_bytes, .-__sha256_process_bytes
	.section	.rodata
	.align 32
	.type	K, @object
	.size	K, 256
K:
	.long	1116352408
	.long	1899447441
	.long	-1245643825
	.long	-373957723
	.long	961987163
	.long	1508970993
	.long	-1841331548
	.long	-1424204075
	.long	-670586216
	.long	310598401
	.long	607225278
	.long	1426881987
	.long	1925078388
	.long	-2132889090
	.long	-1680079193
	.long	-1046744716
	.long	-459576895
	.long	-272742522
	.long	264347078
	.long	604807628
	.long	770255983
	.long	1249150122
	.long	1555081692
	.long	1996064986
	.long	-1740746414
	.long	-1473132947
	.long	-1341970488
	.long	-1084653625
	.long	-958395405
	.long	-710438585
	.long	113926993
	.long	338241895
	.long	666307205
	.long	773529912
	.long	1294757372
	.long	1396182291
	.long	1695183700
	.long	1986661051
	.long	-2117940946
	.long	-1838011259
	.long	-1564481375
	.long	-1474664885
	.long	-1035236496
	.long	-949202525
	.long	-778901479
	.long	-694614492
	.long	-200395387
	.long	275423344
	.long	430227734
	.long	506948616
	.long	659060556
	.long	883997877
	.long	958139571
	.long	1322822218
	.long	1537002063
	.long	1747873779
	.long	1955562222
	.long	2024104815
	.long	-2067236844
	.long	-1933114872
	.long	-1866530822
	.long	-1538233109
	.long	-1090935817
	.long	-965641998
	.align 32
	.type	fillbuf, @object
	.size	fillbuf, 64
fillbuf:
	.byte	-128
	.byte	0
	.zero	62
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
