	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"$"
.LC1:
	.string	"%s%zu$"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__sha256_crypt_r
	.type	__sha256_crypt_r, @function
__sha256_crypt_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbx
	leaq	sha256_salt_prefix(%rip), %rdi
	movq	%rsi, %rbx
	subq	$552, %rsp
	movq	%rdx, -560(%rbp)
	movl	$3, %edx
	movl	%ecx, -484(%rbp)
	call	strncmp@PLT
	leaq	3(%rbx), %rdx
	testl	%eax, %eax
	leaq	sha256_rounds_prefix(%rip), %rsi
	cmovne	%rbx, %rdx
	movq	%rdx, %rdi
	movq	%rdx, -512(%rbp)
	movl	$7, %edx
	call	strncmp@PLT
	testl	%eax, %eax
	movb	$0, -485(%rbp)
	movq	$5000, -528(%rbp)
	je	.L113
.L3:
	movq	-512(%rbp), %rdi
	leaq	.LC0(%rip), %rsi
	call	strcspn@PLT
	movq	%r12, %rdi
	cmpq	$16, %rax
	movq	%rax, %r14
	movl	$16, %eax
	cmovnb	%rax, %r14
	call	strlen@PLT
	testb	$3, %r12b
	movq	%rax, %r15
	je	.L60
	leaq	4(%rax), %rbx
	movq	%rbx, %rdi
	call	__libc_alloca_cutoff@PLT
	testl	%eax, %eax
	jne	.L7
	cmpq	$4096, %rbx
	ja	.L114
.L7:
#APP
# 151 "sha256-crypt.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %rbx
	andq	$-16, %rbx
	subq	%rbx, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
#APP
# 151 "sha256-crypt.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	movq	%rax, -496(%rbp)
	movq	$0, -576(%rbp)
.L9:
	movq	%r12, %rsi
	addq	$4, %rdi
	movq	%r15, %rdx
	call	memcpy@PLT
	movq	%rax, %r12
	movq	%rax, -584(%rbp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L60:
	movq	$0, -576(%rbp)
	movq	$0, -496(%rbp)
	movq	$0, -584(%rbp)
.L6:
	testb	$3, -512(%rbp)
	movq	$0, -568(%rbp)
	jne	.L115
.L10:
	leaq	-400(%rbp), %rbx
	leaq	-224(%rbp), %r13
	movq	%rbx, %rdi
	call	__sha256_init_ctx@PLT
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sha256_process_bytes@PLT
	movq	-512(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	__sha256_process_bytes@PLT
	movq	%r13, %rdi
	movq	%r13, -520(%rbp)
	call	__sha256_init_ctx@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sha256_process_bytes@PLT
	movq	-512(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	__sha256_process_bytes@PLT
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sha256_process_bytes@PLT
	leaq	-464(%rbp), %rax
	movq	%r13, %rdi
	movq	%rax, %rsi
	movq	%rax, -504(%rbp)
	call	__sha256_finish_ctx@PLT
	cmpq	$32, %r15
	jbe	.L17
	leaq	-33(%r15), %rcx
	leaq	-32(%r15), %rsi
	movq	%r14, -552(%rbp)
	movq	-504(%rbp), %r14
	movq	%r12, -592(%rbp)
	movq	%r15, %r12
	movq	%rcx, -536(%rbp)
	andq	$-32, %rcx
	movq	%rsi, -544(%rbp)
	movq	%rcx, %rax
	movq	%rsi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %r13
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rbx, %rdx
	movl	$32, %esi
	movq	%r14, %rdi
	subq	$32, %r12
	call	__sha256_process_bytes@PLT
	cmpq	%r13, %r12
	jne	.L18
	movq	-536(%rbp), %rax
	movq	-544(%rbp), %rsi
	movq	%rbx, %rdx
	movq	-504(%rbp), %rdi
	movq	-552(%rbp), %r14
	movq	-592(%rbp), %r12
	andq	$-32, %rax
	subq	%rax, %rsi
	call	__sha256_process_bytes@PLT
.L19:
	movq	%r15, %r13
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L117:
	movq	-504(%rbp), %rdi
	movl	$32, %esi
	call	__sha256_process_bytes@PLT
	shrq	%r13
	je	.L116
.L23:
	testb	$1, %r13b
	movq	%rbx, %rdx
	jne	.L117
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__sha256_process_bytes@PLT
	shrq	%r13
	jne	.L23
.L116:
	movq	-504(%rbp), %rsi
	movq	%rbx, %rdi
	call	__sha256_finish_ctx@PLT
	movq	-520(%rbp), %rdi
	call	__sha256_init_ctx@PLT
	movq	%rbx, -536(%rbp)
	movq	%r13, %rbx
	movq	-520(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r13, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	addq	$1, %rbx
	call	__sha256_process_bytes@PLT
	cmpq	%rbx, %r15
	jne	.L25
	movq	-536(%rbp), %rbx
.L57:
	leaq	-432(%rbp), %rax
	movq	-520(%rbp), %rdi
	movq	%rax, %rsi
	movq	%rax, -544(%rbp)
	call	__sha256_finish_ctx@PLT
	movq	-496(%rbp), %r12
	addq	%r15, %r12
	movq	%r12, %rdi
	call	__libc_alloca_cutoff@PLT
	cmpq	$4096, %r12
	jbe	.L26
	testl	%eax, %eax
	je	.L118
.L26:
	leaq	30(%r15), %rax
	movq	$0, -592(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -472(%rbp)
.L28:
	cmpq	$31, %r15
	jbe	.L63
	leaq	-32(%r15), %rcx
	movq	-496(%rbp), %rsi
	movq	%rcx, %rdx
	andq	$-32, %rdx
	leaq	32(%rsi), %rax
	leaq	64(%rsi,%rdx), %rdx
	.p2align 4,,10
	.p2align 3
.L30:
	movdqa	-432(%rbp), %xmm0
	movups	%xmm0, -32(%rax)
	movdqa	-416(%rbp), %xmm0
	movups	%xmm0, -16(%rax)
	movq	%rax, -472(%rbp)
	addq	$32, %rax
	cmpq	%rax, %rdx
	jne	.L30
	movq	-496(%rbp), %rax
	andq	$-32, %rcx
	movq	%r15, %rdx
	andl	$31, %edx
	leaq	32(%rax,%rcx), %rcx
.L29:
	cmpl	$8, %edx
	movl	%edx, %eax
	jnb	.L31
	andl	$4, %edx
	jne	.L119
	testl	%eax, %eax
	je	.L32
	movq	-544(%rbp), %rsi
	testb	$2, %al
	movzbl	(%rsi), %edx
	movb	%dl, (%rcx)
	jne	.L120
.L32:
	movq	-520(%rbp), %rdi
	xorl	%r12d, %r12d
	call	__sha256_init_ctx@PLT
	movq	%rbx, -536(%rbp)
	movq	-512(%rbp), %r13
	movq	%r12, %rbx
	movq	-520(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__sha256_process_bytes@PLT
	movzbl	-464(%rbp), %edx
	addq	$1, %rbx
	addq	$16, %rdx
	cmpq	%rbx, %rdx
	ja	.L37
	movq	-544(%rbp), %r13
	movq	-520(%rbp), %rdi
	movq	-536(%rbp), %rbx
	movq	%r13, %rsi
	call	__sha256_finish_ctx@PLT
	leaq	30(%r14), %rax
	movl	%r14d, %ecx
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	cmpl	$8, %r14d
	movq	%rax, %rsi
	movq	%rax, -536(%rbp)
	movq	%rax, -472(%rbp)
	movq	%r13, %rax
	jnb	.L121
.L38:
	xorl	%edx, %edx
	testb	$4, %cl
	jne	.L122
	testb	$2, %cl
	jne	.L123
.L42:
	andl	$1, %ecx
	jne	.L124
.L43:
	movq	%r14, -552(%rbp)
	xorl	%r12d, %r12d
	movq	-504(%rbp), %r14
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L128:
	movq	-496(%rbp), %rdi
	movq	%r15, %rsi
	call	__sha256_process_bytes@PLT
.L45:
	movabsq	$-6148914691236517205, %rax
	mulq	%r12
	shrq	%rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %r12
	jne	.L125
.L46:
	movabsq	$5270498306774157605, %rax
	imulq	%r12
	movq	%r12, %rax
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	cmpq	%rax, %r12
	jne	.L126
.L47:
	testq	%r13, %r13
	movq	%rbx, %rdx
	je	.L48
	movl	$32, %esi
	movq	%r14, %rdi
	call	__sha256_process_bytes@PLT
.L49:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	addq	$1, %r12
	call	__sha256_finish_ctx@PLT
	cmpq	%r12, -528(%rbp)
	je	.L127
.L50:
	movq	%rbx, %rdi
	movq	%r12, %r13
	call	__sha256_init_ctx@PLT
	andl	$1, %r13d
	movq	%rbx, %rdx
	jne	.L128
	movl	$32, %esi
	movq	%r14, %rdi
	call	__sha256_process_bytes@PLT
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L31:
	movq	-432(%rbp), %rax
	movq	%rax, (%rcx)
	movq	-544(%rbp), %rdi
	movl	%edx, %eax
	movq	-8(%rdi,%rax), %rsi
	movq	%rsi, -8(%rcx,%rax)
	leaq	8(%rcx), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rcx
	leal	(%rdx,%rcx), %eax
	subq	%rcx, %rdi
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L32
	andl	$-8, %eax
	xorl	%edx, %edx
.L35:
	movl	%edx, %ecx
	addl	$8, %edx
	movq	(%rdi,%rcx), %r8
	cmpl	%eax, %edx
	movq	%r8, (%rsi,%rcx)
	jb	.L35
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L48:
	movq	-496(%rbp), %rdi
	movq	%r15, %rsi
	call	__sha256_process_bytes@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L126:
	movq	-496(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	__sha256_process_bytes@PLT
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L125:
	movq	-552(%rbp), %rsi
	movq	-536(%rbp), %rdi
	movq	%rbx, %rdx
	call	__sha256_process_bytes@PLT
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L127:
	movl	-484(%rbp), %edx
	xorl	%r12d, %r12d
	movq	-560(%rbp), %rdi
	leaq	sha256_salt_prefix(%rip), %rsi
	movq	-552(%rbp), %r14
	testl	%edx, %edx
	movl	%r12d, %edx
	cmovns	-484(%rbp), %edx
	movslq	%edx, %rdx
	call	__stpncpy@PLT
	movq	%rax, %rdi
	movq	%rax, -472(%rbp)
	movl	-484(%rbp), %eax
	cmpb	$0, -485(%rbp)
	leal	-3(%rax), %edx
	movl	%edx, -484(%rbp)
	jne	.L129
.L51:
	xorl	%r12d, %r12d
	testl	%edx, %edx
	movq	-512(%rbp), %rsi
	cmovs	%r12d, %edx
	movslq	%edx, %rdx
	cmpq	%r14, %rdx
	cmova	%r14, %rdx
	call	__stpncpy@PLT
	movslq	-484(%rbp), %rdx
	movq	%rax, -472(%rbp)
	testl	%edx, %edx
	cmovns	%rdx, %r12
	cmpq	%r14, %r12
	cmova	%r14, %r12
	subl	%r12d, %edx
	testl	%edx, %edx
	movl	%edx, -484(%rbp)
	jle	.L52
	leaq	1(%rax), %rdx
	movq	%rdx, -472(%rbp)
	movb	$36, (%rax)
	subl	$1, -484(%rbp)
.L52:
	movzbl	-454(%rbp), %ecx
	movzbl	-464(%rbp), %edx
	leaq	-484(%rbp), %r13
	movzbl	-444(%rbp), %r8d
	leaq	-472(%rbp), %r12
	movl	$4, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-463(%rbp), %ecx
	movzbl	-443(%rbp), %edx
	movl	$4, %r9d
	movzbl	-453(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-442(%rbp), %ecx
	movzbl	-452(%rbp), %edx
	movl	$4, %r9d
	movzbl	-462(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-451(%rbp), %ecx
	movzbl	-461(%rbp), %edx
	movl	$4, %r9d
	movzbl	-441(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-460(%rbp), %ecx
	movzbl	-440(%rbp), %edx
	movl	$4, %r9d
	movzbl	-450(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-439(%rbp), %ecx
	movzbl	-449(%rbp), %edx
	movl	$4, %r9d
	movzbl	-459(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-448(%rbp), %ecx
	movzbl	-458(%rbp), %edx
	movl	$4, %r9d
	movzbl	-438(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-457(%rbp), %ecx
	movzbl	-437(%rbp), %edx
	movl	$4, %r9d
	movzbl	-447(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-436(%rbp), %ecx
	movzbl	-446(%rbp), %edx
	movl	$4, %r9d
	movzbl	-456(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-445(%rbp), %ecx
	movzbl	-455(%rbp), %edx
	movl	$4, %r9d
	movzbl	-435(%rbp), %r8d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movzbl	-433(%rbp), %ecx
	movzbl	-434(%rbp), %r8d
	xorl	%edx, %edx
	movl	$3, %r9d
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__b64_from_24bit@PLT
	movl	-484(%rbp), %eax
	testl	%eax, %eax
	jle	.L130
	movq	-472(%rbp), %rax
	movq	-560(%rbp), %r12
	movb	$0, (%rax)
.L54:
	movq	%rbx, %rdi
	call	__sha256_init_ctx@PLT
	movq	-504(%rbp), %rsi
	movq	%rbx, %rdi
	call	__sha256_finish_ctx@PLT
	movl	$176, %edx
	movl	$176, %esi
	movq	%rbx, %rdi
	call	__explicit_bzero_chk@PLT
	movq	-520(%rbp), %rdi
	movl	$176, %edx
	movl	$176, %esi
	call	__explicit_bzero_chk@PLT
	movq	-544(%rbp), %rdi
	movl	$32, %edx
	movl	$32, %esi
	call	__explicit_bzero_chk@PLT
	movq	-496(%rbp), %rdi
	movq	$-1, %rdx
	movq	%r15, %rsi
	call	__explicit_bzero_chk@PLT
	movq	-536(%rbp), %rdi
	movq	$-1, %rdx
	movq	%r14, %rsi
	call	__explicit_bzero_chk@PLT
	movq	-584(%rbp), %rax
	testq	%rax, %rax
	je	.L55
	movq	$-1, %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	__explicit_bzero_chk@PLT
.L55:
	movq	-568(%rbp), %rax
	testq	%rax, %rax
	je	.L56
	movq	$-1, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	__explicit_bzero_chk@PLT
.L56:
	movq	-576(%rbp), %rdi
	call	free@PLT
	movq	-592(%rbp), %rdi
	call	free@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movl	(%rax), %edx
	testb	$2, %cl
	movl	%edx, (%rsi)
	movl	$4, %edx
	je	.L42
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L124:
	movzbl	(%rax,%rdx), %eax
	movb	%al, (%rsi,%rdx)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L123:
	movzwl	(%rax,%rdx), %edi
	movw	%di, (%rsi,%rdx)
	addq	$2, %rdx
	andl	$1, %ecx
	je	.L43
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L121:
	movl	%r14d, %esi
	xorl	%eax, %eax
	movq	%r13, %r8
	andl	$-8, %esi
.L39:
	movl	%eax, %edx
	movq	-536(%rbp), %r10
	addl	$8, %eax
	movq	(%r8,%rdx), %rdi
	cmpl	%esi, %eax
	movq	%rdi, (%r10,%rdx)
	jb	.L39
	movq	%r10, %rsi
	addq	%rax, %rsi
	addq	-544(%rbp), %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L130:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r12d, %r12d
	movl	$34, %fs:(%rax)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L129:
	testl	%edx, %edx
	movq	-528(%rbp), %r8
	leaq	sha256_rounds_prefix(%rip), %rcx
	cmovs	%r12d, %edx
	xorl	%eax, %eax
	movslq	%edx, %rsi
	leaq	.LC1(%rip), %rdx
	call	__snprintf@PLT
	movl	-484(%rbp), %edx
	movslq	%eax, %rdi
	addq	-472(%rbp), %rdi
	subl	%eax, %edx
	movq	%rdi, -472(%rbp)
	movl	%edx, -484(%rbp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L113:
	movq	-512(%rbp), %rax
	leaq	-224(%rbp), %rsi
	movl	$10, %edx
	leaq	7(%rax), %rdi
	call	strtoul@PLT
	movq	-224(%rbp), %rdx
	cmpb	$36, (%rdx)
	jne	.L3
	cmpq	$999999999, %rax
	leaq	1(%rdx), %rcx
	movl	$999999999, %edx
	cmovbe	%rax, %rdx
	movl	$1000, %eax
	movb	$1, -485(%rbp)
	cmpq	$1000, %rdx
	movq	%rcx, -512(%rbp)
	cmovnb	%rdx, %rax
	movq	%rax, -528(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L115:
	leaq	34(%r14), %rax
	movq	-496(%rbp), %rcx
	movl	%r14d, %edx
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	4(%r14,%rcx), %rcx
	leaq	15(%rsp), %rax
	movq	%rcx, -496(%rbp)
	andq	$-16, %rax
	cmpl	$8, %r14d
	leaq	4(%rax), %rcx
	jnb	.L11
	testb	$4, %r14b
	jne	.L131
	testl	%edx, %edx
	je	.L12
	movq	-512(%rbp), %rbx
	testb	$2, %dl
	movzbl	(%rbx), %esi
	movb	%sil, 4(%rax)
	jne	.L132
.L12:
	movq	%rcx, -512(%rbp)
	movq	%rcx, -568(%rbp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movq	-512(%rbp), %rbx
	addq	$8, %rax
	movq	(%rbx), %rdx
	movq	%rdx, -4(%rax)
	movl	%r14d, %edx
	movq	-8(%rbx,%rdx), %rsi
	movq	%rsi, -8(%rcx,%rdx)
	movq	%rcx, %rdx
	subq	%rax, %rdx
	subq	%rdx, %rbx
	addl	%r14d, %edx
	andl	$-8, %edx
	movq	%rbx, %r8
	cmpl	$8, %edx
	jb	.L12
	andl	$-8, %edx
	xorl	%esi, %esi
.L15:
	movl	%esi, %edi
	addl	$8, %esi
	movq	(%r8,%rdi), %r9
	cmpl	%edx, %esi
	movq	%r9, (%rax,%rdi)
	jb	.L15
	jmp	.L12
.L119:
	movq	-544(%rbp), %rsi
	movl	(%rsi), %edx
	movl	%edx, (%rcx)
	movl	%eax, %edx
	movl	-4(%rsi,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L17:
	movq	-504(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r15, %rsi
	call	__sha256_process_bytes@PLT
	testq	%r15, %r15
	jne	.L19
	movq	-504(%rbp), %rsi
	movq	%rbx, %rdi
	call	__sha256_finish_ctx@PLT
	movq	-520(%rbp), %rdi
	call	__sha256_init_ctx@PLT
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L63:
	movq	-496(%rbp), %rcx
	movq	%r15, %rdx
	jmp	.L29
.L120:
	movl	%eax, %edx
	movq	-544(%rbp), %rax
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L32
.L118:
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -496(%rbp)
	movq	%rax, -472(%rbp)
	je	.L27
	movq	%rax, -592(%rbp)
	jmp	.L28
.L131:
	movq	-512(%rbp), %rbx
	movl	(%rbx), %esi
	movl	%esi, 4(%rax)
	movl	-4(%rbx,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L12
.L132:
	movq	-512(%rbp), %rax
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L12
.L114:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -576(%rbp)
	je	.L61
	movq	%rax, %rdi
	movq	$0, -496(%rbp)
	jmp	.L9
.L27:
	movq	-576(%rbp), %rdi
	xorl	%r12d, %r12d
	call	free@PLT
	jmp	.L1
.L61:
	xorl	%r12d, %r12d
	jmp	.L1
	.size	__sha256_crypt_r, .-__sha256_crypt_r
	.p2align 4,,15
	.globl	__sha256_crypt
	.type	__sha256_crypt, @function
__sha256_crypt:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	call	strlen@PLT
	movl	buflen.5422(%rip), %ecx
	leal	66(%rax), %ebx
	movq	buffer(%rip), %rdx
	cmpl	%ebx, %ecx
	jge	.L134
	movq	%rdx, %rdi
	movslq	%ebx, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L133
	movq	%rax, buffer(%rip)
	movl	%ebx, buflen.5422(%rip)
	movl	%ebx, %ecx
.L134:
	popq	%rbx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	popq	%rbp
	popq	%r12
	jmp	__sha256_crypt_r
	.p2align 4,,10
	.p2align 3
.L133:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	__sha256_crypt, .-__sha256_crypt
	.local	buflen.5422
	.comm	buflen.5422,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	sha256_rounds_prefix, @object
	.size	sha256_rounds_prefix, 8
sha256_rounds_prefix:
	.string	"rounds="
	.section	.rodata.str1.1
	.type	sha256_salt_prefix, @object
	.size	sha256_salt_prefix, 4
sha256_salt_prefix:
	.string	"$5$"
