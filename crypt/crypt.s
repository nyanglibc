	.text
	.p2align 4,,15
	.globl	_ufc_doit_r
	.type	_ufc_doit_r, @function
_ufc_doit_r:
	movq	(%rdx), %rcx
	pushq	%rbp
	subq	$1, %rdi
	pushq	%rbx
	movq	16(%rdx), %r11
	leaq	128(%rsi), %r9
	movq	8(%rdx), %rbx
	leaq	65664(%rsi), %r10
	salq	$32, %rcx
	salq	$32, %r11
	orq	24(%rdx), %r11
	orq	%rcx, %rbx
	cmpq	$-1, %rdi
	je	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r11, %rax
	movq	%rsi, %r8
	movq	%rbx, %r11
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%r8), %rax
	addq	$16, %r8
	xorq	%rbx, %rax
	movq	%rax, %rbp
	movzwl	%ax, %ecx
	shrq	$48, %rbp
	movq	(%r10,%rcx), %rcx
	xorq	(%r9,%rbp), %rcx
	movq	%rax, %rbp
	shrq	$32, %rax
	shrq	$16, %rbp
	movzwl	%ax, %eax
	movzwl	%bp, %ebp
	xorq	(%r10,%rbp), %rcx
	xorq	(%r9,%rax), %rcx
	movq	-8(%r8), %rax
	xorq	%rcx, %r11
	xorq	%r11, %rax
	movq	%rax, %rbp
	movzwl	%ax, %ecx
	shrq	$48, %rbp
	movq	(%r10,%rcx), %rcx
	xorq	(%r9,%rbp), %rcx
	xorq	%rbx, %rcx
	movq	%rax, %rbx
	shrq	$32, %rax
	shrq	$16, %rbx
	movzwl	%ax, %eax
	movzwl	%bx, %ebx
	xorq	(%r10,%rbx), %rcx
	movq	(%r9,%rax), %rbx
	xorq	%rcx, %rbx
	cmpq	%r9, %r8
	jne	.L3
	subq	$1, %rdi
	cmpq	$-1, %rdi
	jne	.L4
.L8:
	movq	%rbx, %rax
	movl	%ebx, %ecx
	shrq	$32, %rax
	movq	%rcx, 8(%rdx)
	movq	%rax, (%rdx)
	movq	%r11, %rax
	andl	$4294967295, %r11d
	shrq	$32, %rax
	movq	%r11, 24(%rdx)
	popq	%rbx
	movq	%rax, 16(%rdx)
	popq	%rbp
	ret
	.size	_ufc_doit_r, .-_ufc_doit_r
