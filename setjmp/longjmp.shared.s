	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_siglongjmp
	.type	__libc_siglongjmp, @function
__libc_siglongjmp:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_longjmp_unwind@PLT
	movl	64(%rbp), %eax
	testl	%eax, %eax
	jne	.L6
.L2:
	testl	%ebx, %ebx
	movl	$1, %eax
	movq	%rbp, %rdi
	cmove	%eax, %ebx
	movl	%ebx, %esi
	call	__longjmp
.L6:
	leaq	72(%rbp), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigprocmask
	jmp	.L2
	.size	__libc_siglongjmp, .-__libc_siglongjmp
	.weak	siglongjmp
	.set	siglongjmp,__libc_siglongjmp
	.weak	longjmp
	.set	longjmp,__libc_siglongjmp
	.weak	_longjmp
	.set	_longjmp,__libc_siglongjmp
	.p2align 4,,15
	.globl	__libc_longjmp
	.type	__libc_longjmp, @function
__libc_longjmp:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %ebx
	subq	$8, %rsp
	call	_longjmp_unwind@PLT
	movl	64(%rbp), %eax
	testl	%eax, %eax
	jne	.L11
.L8:
	testl	%ebx, %ebx
	movl	$1, %eax
	movq	%rbp, %rdi
	cmove	%eax, %ebx
	movl	%ebx, %esi
	call	__longjmp_cancel
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	72(%rbp), %rsi
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigprocmask
	jmp	.L8
	.size	__libc_longjmp, .-__libc_longjmp
	.hidden	__longjmp_cancel
	.hidden	__longjmp
