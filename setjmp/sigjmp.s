	.text
	.p2align 4,,15
	.globl	__sigjmp_save
	.type	__sigjmp_save, @function
__sigjmp_save:
	testl	%esi, %esi
	pushq	%rbx
	movq	%rdi, %rbx
	je	.L2
	leaq	72(%rdi), %rdx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__sigprocmask
	xorl	%esi, %esi
	testl	%eax, %eax
	sete	%sil
.L2:
	movl	%esi, 64(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__sigjmp_save, .-__sigjmp_save
	.hidden	__sigprocmask
