 .text
.globl __longjmp_cancel
.type __longjmp_cancel,@function
.align 1<<4
__longjmp_cancel:
 mov (6*8)(%rdi),%r8
 mov (1*8)(%rdi),%r9
 mov (7*8)(%rdi),%rdx
 ror $2*8 +1, %r8
 xor %fs:48, %r8
 ror $2*8 +1, %r9
 xor %fs:48, %r9
 ror $2*8 +1, %rdx
 xor %fs:48, %rdx

 movq (0*8)(%rdi),%rbx
 movq (2*8)(%rdi),%r12
 movq (3*8)(%rdi),%r13
 movq (4*8)(%rdi),%r14
 movq (5*8)(%rdi),%r15
 mov %esi, %eax
 mov %r8,%rsp
 movq %r9,%rbp

 jmpq *%rdx
.size __longjmp_cancel,.-__longjmp_cancel
