.globl setjmp
.type setjmp,@function
.align 1<<4
setjmp:
 movl $1, %esi
 jmp __GI___sigsetjmp
.size setjmp,.-setjmp
