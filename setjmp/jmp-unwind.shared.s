	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_longjmp_unwind
	.type	_longjmp_unwind, @function
_longjmp_unwind:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
	movq	120+__libc_pthread_functions(%rip), %rax
	movq	%rsp, %rsi
#APP
# 30 "../sysdeps/nptl/jmp-unwind.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	_longjmp_unwind, .-_longjmp_unwind
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
