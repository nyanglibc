.globl _setjmp
.type _setjmp,@function
.align 1<<4
_setjmp:
 xorl %esi, %esi
 jmp __GI___sigsetjmp
.size _setjmp,.-_setjmp;
.globl __GI__setjmp
.set __GI__setjmp,_setjmp
