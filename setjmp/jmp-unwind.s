	.text
	.p2align 4,,15
	.globl	_longjmp_unwind
	.type	_longjmp_unwind, @function
_longjmp_unwind:
	cmpq	$0, __pthread_cleanup_upto@GOTPCREL(%rip)
	je	.L1
	movq	%rsp, %rsi
	jmp	__pthread_cleanup_upto@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	_longjmp_unwind, .-_longjmp_unwind
	.weak	__pthread_cleanup_upto
