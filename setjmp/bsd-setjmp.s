.globl setjmp
.type setjmp,@function
.align 1<<4
setjmp:
 movl $1, %esi
 jmp __sigsetjmp
.size setjmp,.-setjmp
