.globl _setjmp
.type _setjmp,@function
.align 1<<4
_setjmp:
 xorl %esi, %esi
 jmp __sigsetjmp
.size _setjmp,.-_setjmp
