.globl __sigsetjmp
.type __sigsetjmp,@function
.align 1<<4
__sigsetjmp:
 movq %rbx, (0*8)(%rdi)
 mov %rbp, %rax
 xor %fs:48, %rax
 rol $2*8 +1, %rax
 mov %rax, (1*8)(%rdi)
 movq %r12, (2*8)(%rdi)
 movq %r13, (3*8)(%rdi)
 movq %r14, (4*8)(%rdi)
 movq %r15, (5*8)(%rdi)
 lea 8(%rsp), %rdx
 xor %fs:48, %rdx
 rol $2*8 +1, %rdx
 movq %rdx, (6*8)(%rdi)
 mov (%rsp), %rax

 xor %fs:48, %rax
 rol $2*8 +1, %rax
 movq %rax, (7*8)(%rdi)
 jmp __sigjmp_save
.size __sigsetjmp,.-__sigsetjmp
