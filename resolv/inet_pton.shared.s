	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	inet_pton4, @function
inet_pton4:
	cmpq	%rsi, %rdi
	movb	$0, -4(%rsp)
	jnb	.L7
	leaq	-4(%rsp), %r8
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L19:
	movzbl	(%r8), %eax
	testl	%r9d, %r9d
	movl	%eax, %r11d
	leal	(%rax,%rax,4), %eax
	leal	(%rcx,%rax,2), %eax
	je	.L17
	cmpl	$255, %eax
	ja	.L7
	testb	%r11b, %r11b
	je	.L7
	movb	%al, (%r8)
.L8:
	cmpq	%rdi, %rsi
	je	.L18
.L2:
	addq	$1, %rdi
	movsbl	-1(%rdi), %eax
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	jbe	.L19
	cmpl	$46, %eax
	sete	%al
	testb	%r9b, %al
	je	.L7
	cmpl	$4, %r10d
	je	.L7
	movb	$0, 1(%r8)
	xorl	%r9d, %r9d
	addq	$1, %r8
	cmpq	%rdi, %rsi
	jne	.L2
.L18:
	cmpl	$3, %r10d
	jle	.L7
	movl	-4(%rsp), %eax
	movl	%eax, (%rdx)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$255, %eax
	ja	.L7
	addl	$1, %r10d
	movb	%al, (%r8)
	cmpl	$4, %r10d
	jg	.L7
	movl	$1, %r9d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.size	inet_pton4, .-inet_pton4
	.p2align 4,,15
	.globl	__GI___inet_pton_length
	.hidden	__GI___inet_pton_length
	.type	__GI___inet_pton_length, @function
__GI___inet_pton_length:
	pushq	%r14
	pushq	%r12
	movq	%rsi, %rax
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	cmpl	$2, %edi
	je	.L22
	cmpl	$10, %edi
	jne	.L49
	leaq	(%rsi,%rdx), %rsi
	pxor	%xmm0, %xmm0
	leaq	16(%rsp), %rdi
	cmpq	%rax, %rsi
	movaps	%xmm0, 16(%rsp)
	movq	%rdi, %r11
	je	.L28
	cmpb	$58, (%rax)
	je	.L50
.L27:
	cmpq	%rax, %rsi
	leaq	16(%rdi), %r14
	jbe	.L30
	movq	%rax, %rdi
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	xorl	%r12d, %r12d
.L29:
	addq	$1, %rax
	movsbl	-1(%rax), %r9d
	leal	-48(%r9), %r10d
	movl	%r9d, %r8d
	cmpb	$9, %r10b
	ja	.L31
	leal	-48(%r9), %r8d
.L32:
	testl	%r8d, %r8d
	js	.L34
	cmpq	$4, %r12
	je	.L28
	sall	$4, %edx
	orl	%r8d, %edx
	cmpl	$65535, %edx
	ja	.L28
	addq	$1, %r12
.L35:
	cmpq	%rax, %rsi
	jne	.L29
	testq	%r12, %r12
	je	.L42
	leaq	2(%r11), %r12
	cmpq	%r12, %r14
	jb	.L28
	movzbl	%dh, %eax
	movb	%dl, 1(%r11)
	movb	%al, (%r11)
.L39:
	testq	%rbp, %rbp
	je	.L43
	cmpq	%r12, %r14
	movq	%rcx, 8(%rsp)
	je	.L28
	movq	%r12, %rdx
	movq	%r14, %rbx
	movq	%rbp, %rsi
	subq	%rbp, %rdx
	subq	%rdx, %rbx
	movq	%rbx, %rdi
	subq	%rbp, %rbx
	call	__GI_memmove
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	__GI_memset@PLT
	movq	8(%rsp), %rcx
.L40:
	movdqa	16(%rsp), %xmm0
	movl	$1, %eax
	movups	%xmm0, (%rcx)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L49:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$97, %fs:(%rax)
	movl	$-1, %eax
.L20:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	(%rsi,%rdx), %rsi
	movq	%rax, %rdi
	movq	%rcx, %rdx
	call	inet_pton4
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r14
	ret
.L43:
	movq	%r12, %r11
.L30:
	cmpq	%r11, %r14
	je	.L40
	.p2align 4,,10
	.p2align 3
.L28:
	xorl	%eax, %eax
.L52:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	1(%rax), %rdx
	cmpq	%rdx, %rsi
	je	.L28
	cmpb	$58, 1(%rax)
	jne	.L28
	movq	%rdx, %rax
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L31:
	leal	-97(%r9), %r10d
	cmpb	$5, %r10b
	ja	.L33
	leal	-87(%r9), %r8d
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	subl	$65, %r8d
	cmpb	$5, %r8b
	jbe	.L51
.L34:
	cmpl	$58, %r9d
	jne	.L36
	testq	%r12, %r12
	jne	.L37
	testq	%rbp, %rbp
	jne	.L28
	movq	%rax, %rdi
	movq	%r11, %rbp
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L37:
	cmpq	%rax, %rsi
	je	.L28
	leaq	2(%r11), %r8
	cmpq	%r8, %r14
	jb	.L28
	movzbl	%dh, %ebx
	movb	%dl, 1(%r11)
	movq	%rax, %rdi
	movb	%bl, (%r11)
	xorl	%edx, %edx
	movq	%r8, %r11
	xorl	%r12d, %r12d
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L51:
	leal	-55(%r9), %r8d
	jmp	.L32
.L36:
	cmpl	$46, %r9d
	jne	.L28
	leaq	4(%r11), %r12
	cmpq	%r12, %r14
	jb	.L28
	movq	%r11, %rdx
	movq	%rcx, 8(%rsp)
	call	inet_pton4
	testl	%eax, %eax
	movq	8(%rsp), %rcx
	jg	.L39
	xorl	%eax, %eax
	jmp	.L52
.L42:
	movq	%r11, %r12
	jmp	.L39
	.size	__GI___inet_pton_length, .-__GI___inet_pton_length
	.globl	__inet_pton_length
	.set	__inet_pton_length,__GI___inet_pton_length
	.p2align 4,,15
	.globl	__GI___inet_pton
	.hidden	__GI___inet_pton
	.type	__GI___inet_pton, @function
__GI___inet_pton:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movl	%edi, %ebp
	movq	%rsi, %rbx
	movq	%rsi, %rdi
	call	__GI_strlen
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	movq	%rax, %rdx
	jmp	__GI___inet_pton_length
	.size	__GI___inet_pton, .-__GI___inet_pton
	.globl	__inet_pton
	.set	__inet_pton,__GI___inet_pton
	.weak	__GI_inet_pton
	.hidden	__GI_inet_pton
	.set	__GI_inet_pton,__inet_pton
	.weak	inet_pton
	.set	inet_pton,__GI_inet_pton
