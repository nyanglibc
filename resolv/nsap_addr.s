	.text
	.p2align 4,,15
	.globl	inet_nsap_addr
	.type	inet_nsap_addr, @function
inet_nsap_addr:
	movl	%edx, %edx
	xorl	%r8d, %r8d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L20:
	cmpb	$43, %cl
	je	.L8
	testb	$-128, %cl
	jne	.L13
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r10
	movl	(%r10,%rcx,4), %eax
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rcx
	movq	%fs:(%rcx), %r11
	movzbl	%al, %ecx
	testb	$16, 1(%r11,%rcx,2)
	je	.L13
	movzbl	%al, %ecx
	leaq	2(%rdi), %r9
	subl	$48, %ecx
	cmpl	$10, %ecx
	sbbl	%ecx, %ecx
	andl	$-7, %ecx
	addl	$55, %ecx
	subl	%ecx, %eax
	movzbl	1(%rdi), %ecx
	testb	%cl, %cl
	je	.L13
	movl	(%r10,%rcx,4), %ecx
	movzbl	%cl, %edi
	testb	$16, 1(%r11,%rdi,2)
	je	.L13
	movzbl	%cl, %edi
	sall	$4, %eax
	subl	$48, %edi
	cmpl	$10, %edi
	sbbl	%edi, %edi
	andl	$-7, %edi
	addl	$55, %edi
	subl	%edi, %ecx
	movq	%r9, %rdi
	orl	%ecx, %eax
	movb	%al, (%rsi,%r8)
	addq	$1, %r8
.L2:
	leaq	1(%rdi), %r9
	movzbl	-1(%r9), %ecx
	movl	%r8d, %eax
	testb	%cl, %cl
	je	.L1
.L21:
	cmpq	%r8, %rdx
	je	.L19
	leal	-46(%rcx), %r10d
	cmpb	$1, %r10b
	ja	.L20
.L8:
	movq	%r9, %rdi
	leaq	1(%rdi), %r9
	movzbl	-1(%r9), %ecx
	testb	%cl, %cl
	jne	.L21
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	rep ret
	.size	inet_nsap_addr, .-inet_nsap_addr
	.p2align 4,,15
	.globl	inet_nsap_ntoa
	.type	inet_nsap_ntoa, @function
inet_nsap_ntoa:
	leaq	tmpbuf.9620(%rip), %rax
	testq	%rdx, %rdx
	movl	$255, %r11d
	cmovne	%rdx, %rax
	cmpl	$255, %edi
	cmovle	%edi, %r11d
	xorl	%r9d, %r9d
	testl	%edi, %edi
	movq	%rax, %r8
	movq	%rax, %r10
	jle	.L25
	.p2align 4,,10
	.p2align 3
.L24:
	movzbl	(%rsi), %ecx
	leaq	2(%r8), %r10
	shrb	$4, %cl
	cmpb	$10, %cl
	sbbl	%edi, %edi
	addq	$1, %rsi
	andl	$-7, %edi
	leal	55(%rcx,%rdi), %ecx
	movb	%cl, (%r8)
	movzbl	-1(%rsi), %edi
	andl	$15, %edi
	cmpb	$10, %dil
	sbbl	%ecx, %ecx
	andl	$-7, %ecx
	leal	55(%rcx,%rdi), %ecx
	movb	%cl, 1(%r8)
	movl	%r9d, %ecx
	addl	$1, %r9d
	andl	$1, %ecx
	testl	%ecx, %ecx
	jne	.L28
	cmpl	%r9d, %r11d
	jle	.L25
	movb	$46, 2(%r8)
	addq	$3, %r8
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	%r9d, %r11d
	jle	.L25
	movq	%r10, %r8
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	movb	$0, (%r10)
	ret
	.size	inet_nsap_ntoa, .-inet_nsap_ntoa
	.local	tmpbuf.9620
	.comm	tmpbuf.9620,638,32
