	.text
	.p2align 4,,15
	.type	gaih_getanswer_slice.isra.0, @function
gaih_getanswer_slice.isra.0:
	pushq	%r15
	pushq	%r14
	movslq	%esi, %rsi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leaq	(%rdi,%rsi), %rbp
	subq	$1416, %rsp
	movzwl	6(%rdi), %r13d
	movq	(%r8), %rax
	movq	(%rcx), %r15
	movq	%rax, 24(%rsp)
	movq	(%rdx), %rax
	rolw	$8, %r13w
	cmpw	$256, 4(%rdi)
	movq	%rax, 48(%rsp)
	jne	.L100
	leaq	12(%rdi), %r12
	movq	%rcx, 88(%rsp)
	leaq	112(%rsp), %rcx
	movq	%r8, 96(%rsp)
	movq	%rdx, 80(%rsp)
	movl	$255, %r8d
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r9, 72(%rsp)
	movq	%rdi, 8(%rsp)
	movq	%rcx, (%rsp)
	call	__ns_name_unpack@PLT
	movslq	%eax, %rbx
	cmpl	$-1, %ebx
	jne	.L4
.L98:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L5:
	movq	72(%rsp), %rdx
	movl	%eax, (%rdx)
.L100:
	movq	1472(%rsp), %rax
	movl	$3, (%rax)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movq	24(%rsp), %rdx
	movq	(%rsp), %rdi
	movq	%r15, %rsi
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	je	.L102
	testl	%ebx, %ebx
	js	.L98
	movq	%r15, %rdi
	call	__res_hnok@PLT
	testl	%eax, %eax
	je	.L103
	movzwl	%r13w, %r13d
	testl	%r13d, %r13d
	je	.L10
	leaq	368(%rsp), %rax
	leaq	4(%r12,%rbx), %rbx
	movq	$0, 64(%rsp)
	movq	$0, 40(%rsp)
	xorl	%r14d, %r14d
	movl	$0, 108(%rsp)
	movq	%rax, 56(%rsp)
	.p2align 4,,10
	.p2align 3
.L11:
	subl	$1, %r13d
	cmpl	$-1, %r13d
	je	.L42
.L109:
	cmpq	%rbx, %rbp
	jbe	.L42
	andl	$1, %r14d
	jne	.L42
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdi
	movl	$255, %r8d
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movl	$1, %r14d
	call	__ns_name_unpack@PLT
	movslq	%eax, %r12
	cmpl	$-1, %r12d
	je	.L11
	movq	24(%rsp), %rdx
	movq	(%rsp), %rdi
	movq	%r15, %rsi
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	movl	%eax, 104(%rsp)
	je	.L104
	testl	%r12d, %r12d
	js	.L11
	movq	%r15, %rdi
	call	__res_hnok@PLT
	testl	%eax, %eax
	je	.L11
	movq	1488(%rsp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	je	.L15
	cmpq	$0, 40(%rsp)
	je	.L105
.L15:
	addq	%r12, %rbx
	leaq	10(%rbx), %r14
	cmpq	%r14, %rbp
	jb	.L55
	movq	%rbx, %rdi
	call	__ns_get16@PLT
	leaq	2(%rbx), %rdi
	movl	%eax, %r12d
	call	__ns_get16@PLT
	leaq	4(%rbx), %rdi
	movl	%eax, 20(%rsp)
	call	__ns_get32@PLT
	leaq	8(%rbx), %rdi
	movq	%rax, 32(%rsp)
	movq	%r14, %rbx
	call	__ns_get16@PLT
	movq	%rbp, %rcx
	movslq	%eax, %rdx
	subq	%r14, %rcx
	cmpq	%rdx, %rcx
	jl	.L55
	cmpl	$1, 20(%rsp)
	jne	.L24
	cmpl	$5, %r12d
	je	.L106
	cmpl	$1, %r12d
	je	.L58
	cmpl	$28, %r12d
	jne	.L24
.L58:
	cmpl	$1, %r12d
	movl	$4, %ecx
	je	.L27
	cmpl	$28, %r12d
	movl	$16, %ecx
	movl	$-1, %esi
	cmovne	%esi, %ecx
.L27:
	cmpl	%ecx, %eax
	jne	.L55
	movq	48(%rsp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L107
.L30:
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	1488(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L31
	movq	48(%rsp), %rax
	movq	(%rax), %rax
.L32:
	xorl	%ecx, %ecx
	cmpl	$1, %r12d
	setne	%cl
	cmpq	$8, %rdx
	leal	2(,%rcx,8), %ecx
	movl	%ecx, 16(%rax)
	leaq	20(%rax), %rcx
	jnb	.L36
	testb	$4, %dl
	jne	.L108
	testq	%rdx, %rdx
	je	.L37
	movzbl	(%r14), %eax
	testb	$2, %dl
	movb	%al, (%rcx)
	jne	.L95
.L99:
	movq	48(%rsp), %rax
	movq	(%rax), %rax
.L37:
	subl	$1, %r13d
	leaq	(%r14,%rdx), %rbx
	xorl	%r14d, %r14d
	cmpl	$-1, %r13d
	movl	$0, 36(%rax)
	movq	%rax, 48(%rsp)
	movl	$1, 108(%rsp)
	jne	.L109
	.p2align 4,,10
	.p2align 3
.L42:
	movl	108(%rsp), %eax
	testl	%eax, %eax
	jne	.L110
	cmpq	$0, 40(%rsp)
	je	.L111
.L10:
	movq	1472(%rsp), %rax
	movl	$1, (%rax)
	xorl	%eax, %eax
.L1:
	addq	$1416, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$1, %r14d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L24:
	addq	%rdx, %rbx
	xorl	%r14d, %r14d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L104:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$90, %fs:(%rax)
	jne	.L11
.L7:
	movq	72(%rsp), %rax
	movl	$34, (%rax)
	movq	1472(%rsp), %rax
	movl	$-1, (%rax)
	movl	$-2, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L105:
	movslq	104(%rsp), %rax
	movq	%r15, 64(%rsp)
	subq	%rax, 24(%rsp)
	addq	%rax, %r15
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L106:
	cmpq	$0, 1480(%rsp)
	je	.L18
	movq	1480(%rsp), %rsi
	movq	32(%rsp), %rax
	cmpl	(%rsi), %eax
	jl	.L112
.L18:
	movq	56(%rsp), %rcx
	movq	8(%rsp), %rdi
	movl	$1025, %r8d
	movq	%r14, %rdx
	movq	%rbp, %rsi
	call	__dn_expand@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L55
	movq	56(%rsp), %rdi
	call	__res_hnok@PLT
	testl	%eax, %eax
	je	.L55
	movq	1488(%rsp), %rax
	movslq	%r12d, %rbx
	addq	%r14, %rbx
	movl	(%rax), %r14d
	testl	%r14d, %r14d
	je	.L11
	movslq	104(%rsp), %rax
	movq	64(%rsp), %rsi
	leaq	(%rsi,%rax), %rdx
	cmpq	%rdx, %r15
	je	.L113
.L20:
	movq	56(%rsp), %rax
.L21:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L21
	movl	%edx, %ecx
	movq	24(%rsp), %r14
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	movl	%edx, %esi
	cmove	%rcx, %rax
	addb	%dl, %sil
	movq	56(%rsp), %rsi
	sbbq	$3, %rax
	subq	%rsi, %rax
	addl	$1, %eax
	movslq	%eax, %r12
	cmpq	%r14, %r12
	ja	.L7
	cmpl	$255, %eax
	jg	.L55
	movq	%r15, %rdi
	subq	%r12, %r14
	movq	%r12, %rdx
	call	__mempcpy@PLT
	movq	%r14, 24(%rsp)
	movq	%r15, 40(%rsp)
	xorl	%r14d, %r14d
	movq	%rax, %r15
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L112:
	movl	%eax, (%rsi)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L110:
	movq	80(%rsp), %rax
	movq	48(%rsp), %rsi
	movq	%rsi, (%rax)
	movq	88(%rsp), %rax
	movq	24(%rsp), %rsi
	movq	%r15, (%rax)
	movq	96(%rsp), %rax
	movq	%rsi, (%rax)
	movq	1472(%rsp), %rax
	movl	$0, (%rax)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	$0, 1480(%rsp)
	je	.L33
	movq	1480(%rsp), %rsi
	movq	32(%rsp), %rax
	cmpl	(%rsi), %eax
	jge	.L33
	movl	%eax, (%rsi)
.L33:
	movq	40(%rsp), %rax
	movq	1488(%rsp), %rsi
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	48(%rsp), %rax
	cmove	64(%rsp), %rcx
	movq	(%rax), %rax
	movq	%rcx, 8(%rax)
	movl	$0, (%rsi)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%r15, %rcx
	movq	24(%rsp), %rsi
	negq	%rcx
	andl	$7, %ecx
	cmpq	%rcx, %rsi
	leaq	(%r15,%rcx), %rax
	jbe	.L7
	subq	%rcx, %rsi
	cmpq	$39, %rsi
	movq	%rsi, %rcx
	jbe	.L7
	movq	48(%rsp), %rsi
	leaq	40(%rax), %r15
	movq	%rax, (%rsi)
	leaq	-40(%rcx), %rsi
	movq	%rsi, 24(%rsp)
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L113:
	addq	%rax, 24(%rsp)
	movq	%rsi, %r15
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L102:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$90, %eax
	jne	.L5
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L36:
	movq	(%r14), %rsi
	addq	$28, %rax
	movq	%r14, %rdi
	movq	%rsi, -8(%rax)
	movq	-8(%r14,%rdx), %rsi
	andq	$-8, %rax
	movq	%rsi, -8(%rcx,%rdx)
	subq	%rax, %rcx
	subq	%rcx, %rdi
	addq	%rdx, %rcx
	andq	$-8, %rcx
	cmpq	$8, %rcx
	jb	.L99
	andq	$-8, %rcx
	xorl	%esi, %esi
.L40:
	movq	(%rdi,%rsi), %r8
	movq	%r8, (%rax,%rsi)
	addq	$8, %rsi
	cmpq	%rcx, %rsi
	jb	.L40
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L103:
	movq	errno@gottpoff(%rip), %rax
	movl	$74, %fs:(%rax)
	movq	72(%rsp), %rax
	movl	$74, (%rax)
	movq	1472(%rsp), %rax
	movl	$3, (%rax)
	movl	$-1, %eax
	jmp	.L1
.L111:
	movq	1472(%rsp), %rax
	movl	$-1, (%rax)
	movl	$-2, %eax
	jmp	.L1
.L108:
	movl	(%r14), %eax
	movl	%eax, (%rcx)
	movl	-4(%r14,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	movq	48(%rsp), %rax
	movq	(%rax), %rax
	jmp	.L37
.L95:
	movzwl	-2(%r14,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	movq	48(%rsp), %rax
	movq	(%rax), %rax
	jmp	.L37
	.size	gaih_getanswer_slice.isra.0, .-gaih_getanswer_slice.isra.0
	.p2align 4,,15
	.type	getanswer_r.constprop.2, @function
getanswer_r.constprop.2:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1544, %rsp
	movq	1600(%rsp), %r12
	movq	1608(%rsp), %rax
	movq	%rdi, 88(%rsp)
	movq	%rsi, (%rsp)
	movq	%rcx, 72(%rsp)
	movl	%r8d, 36(%rsp)
	negq	%r12
	movq	%r9, 48(%rsp)
	andl	$7, %r12d
	cmpq	%rax, %r12
	jnb	.L117
	subq	%r12, %rax
	cmpq	$399, %rax
	jbe	.L117
	leal	-400(%rax), %r9d
	leaq	-400(%rax), %rsi
	movslq	%r9d, %rcx
	cmpq	%rcx, %rsi
	movl	$2147483647, %ecx
	movl	36(%rsp), %esi
	cmovne	%ecx, %r9d
	movq	48(%rsp), %rcx
	cmpl	$12, %esi
	movq	$0, (%rcx)
	je	.L183
	cmpl	$28, %esi
	je	.L121
	cmpl	$1, %esi
	je	.L121
	movq	1616(%rsp), %rax
	movl	$2, (%rax)
	movl	$-1, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L121:
	movq	__res_hnok@GOTPCREL(%rip), %rsi
	movq	%rsi, 40(%rsp)
.L120:
	movq	(%rsp), %rsi
	movzwl	6(%rsi), %ecx
	leaq	12(%rsi), %rbp
	rolw	$8, %cx
	cmpw	$256, 4(%rsi)
	movzwl	%cx, %r13d
	jne	.L297
	leal	1(%r13), %r10d
	movl	%r9d, 24(%rsp)
	movslq	%r10d, %rcx
	movl	%r10d, 16(%rsp)
	leaq	400(,%rcx,8), %r15
	cmpq	%r15, %rax
	jbe	.L117
	movq	(%rsp), %rdi
	movslq	%edx, %rdx
	leaq	240(%rsp), %rax
	movl	$255, %r8d
	movq	%rax, %rcx
	movq	%rax, 8(%rsp)
	leaq	(%rdi,%rdx), %r14
	movq	%rbp, %rdx
	movq	%r14, %rsi
	call	__ns_name_unpack@PLT
	movslq	%eax, %rbx
	movl	16(%rsp), %r10d
	movl	24(%rsp), %r9d
	cmpl	$-1, %ebx
	je	.L293
	addq	1600(%rsp), %r12
	leal	0(,%r10,8), %eax
	movq	8(%rsp), %rdi
	subl	%eax, %r9d
	movslq	%r9d, %rdx
	movl	%r9d, 60(%rsp)
	addq	%r12, %r15
	movq	%r12, 96(%rsp)
	movq	%r15, %rsi
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	jne	.L125
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$90, %eax
	jne	.L124
	.p2align 4,,10
	.p2align 3
.L117:
	movq	1616(%rsp), %rax
	movl	$34, (%rax)
	movq	1624(%rsp), %rax
	movl	$-1, (%rax)
	movl	$-2, %eax
.L114:
	addq	$1544, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L183:
	movq	__res_dnok@GOTPCREL(%rip), %rsi
	movq	%rsi, 40(%rsp)
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L293:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L124:
	movq	1616(%rsp), %rdx
	movl	%eax, (%rdx)
.L297:
	movq	1624(%rsp), %rax
	movl	$3, (%rax)
	movl	$-1, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L125:
	testl	%ebx, %ebx
	js	.L293
	movq	%r15, %rdi
	movq	40(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L298
	movl	36(%rsp), %eax
	leaq	4(%rbp,%rbx), %rbx
	cmpl	$1, %eax
	sete	123(%rsp)
	movzbl	123(%rsp), %edx
	cmpl	$28, %eax
	sete	%al
	orb	%dl, %al
	movb	%al, 122(%rsp)
	je	.L184
	movq	%r15, %rdi
	call	strlen@PLT
	addl	$1, %eax
	cmpl	$255, %eax
	jg	.L299
	movslq	%eax, %rdx
	movq	48(%rsp), %rcx
	addq	%r15, %rdx
	subl	%eax, 60(%rsp)
	movq	%r15, (%rcx)
	js	.L117
	movq	%r15, 128(%rsp)
	movq	%rdx, %r15
.L128:
	movq	96(%rsp), %rax
	movq	48(%rsp), %rcx
	xorl	%r12d, %r12d
	movl	$0, 124(%rsp)
	leaq	400(%rax), %rdx
	movq	$0, (%rax)
	movq	%rax, 104(%rsp)
	movq	%rax, 8(%rcx)
	movq	$0, 400(%rax)
	leaq	496(%rsp), %rax
	movq	%rdx, 136(%rsp)
	movq	%rdx, 24(%rcx)
	movq	%rax, 64(%rsp)
	movl	%r13d, %eax
	movq	%rdx, 112(%rsp)
	movq	%r15, %r13
	movl	%eax, %r15d
	.p2align 4,,10
	.p2align 3
.L130:
	subl	$1, %r15d
	cmpl	$-1, %r15d
	je	.L157
	cmpq	%rbx, %r14
	jbe	.L157
	andl	$1, %r12d
	jne	.L157
	movq	8(%rsp), %rcx
	movq	(%rsp), %rdi
	movl	$255, %r8d
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movl	$1, %r12d
	call	__ns_name_unpack@PLT
	movslq	%eax, %rbp
	cmpl	$-1, %ebp
	je	.L130
	movslq	60(%rsp), %rax
	movq	8(%rsp), %rdi
	movq	%r13, %rsi
	movq	%rax, %rdx
	movq	%rax, 80(%rsp)
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	je	.L292
	testl	%ebp, %ebp
	js	.L130
	movq	%r13, %rdi
	movq	40(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L130
	addq	%rbp, %rbx
	leaq	10(%rbx), %rbp
	cmpq	%rbp, %r14
	jb	.L130
	movq	%rbx, %rdi
	call	__ns_get16@PLT
	leaq	2(%rbx), %rdi
	movl	%eax, 16(%rsp)
	call	__ns_get16@PLT
	leaq	4(%rbx), %rdi
	movl	%eax, 56(%rsp)
	call	__ns_get32@PLT
	leaq	8(%rbx), %rdi
	movq	%rax, 24(%rsp)
	movq	%rbp, %rbx
	call	__ns_get16@PLT
	movslq	%eax, %rcx
	movq	%r14, %rax
	subq	%rbp, %rax
	movq	%rcx, %r8
	cmpq	%rcx, %rax
	jl	.L130
	cmpl	$1, 56(%rsp)
	jne	.L296
	cmpb	$0, 122(%rsp)
	movl	24(%rsp), %eax
	movl	%eax, 56(%rsp)
	je	.L134
	cmpl	$5, 16(%rsp)
	je	.L300
.L135:
	movl	16(%rsp), %edx
	cmpl	%edx, 36(%rsp)
	jne	.L296
	movl	36(%rsp), %eax
	cmpl	$12, %eax
	je	.L144
	cmpl	$28, %eax
	je	.L145
	cmpl	$1, %eax
	je	.L145
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	cmpl	$12, 36(%rsp)
	jne	.L135
	cmpl	$5, 16(%rsp)
	jne	.L135
	cmpq	$0, 1632(%rsp)
	je	.L139
	movq	1632(%rsp), %rdx
	movq	24(%rsp), %rax
	cmpl	(%rdx), %eax
	jge	.L139
	movl	%eax, (%rdx)
.L139:
	movq	64(%rsp), %rcx
	movq	(%rsp), %rdi
	movl	$1025, %r8d
	movq	%rbp, %rdx
	movq	%r14, %rsi
	call	__dn_expand@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L202
	movq	64(%rsp), %rdi
	call	__res_dnok@PLT
	testl	%eax, %eax
	je	.L202
	movq	64(%rsp), %rax
	movslq	%r12d, %rbx
	addq	%rbp, %rbx
.L140:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L140
	movl	%edx, %ecx
	movl	60(%rsp), %r12d
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	movl	%edx, %esi
	cmove	%rcx, %rax
	addb	%dl, %sil
	movq	64(%rsp), %rsi
	sbbq	$3, %rax
	subq	%rsi, %rax
	leal	1(%rax), %ebp
	cmpl	%r12d, %ebp
	jg	.L117
	cmpl	$255, %ebp
	jg	.L202
	movq	%r13, %rdi
	subl	%ebp, %r12d
	movslq	%ebp, %rdx
	call	__mempcpy@PLT
	movl	%r12d, 60(%rsp)
	movq	%r13, 72(%rsp)
	xorl	%r12d, %r12d
	movq	%rax, %r13
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L292:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$90, %fs:(%rax)
	jne	.L130
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L202:
	movl	$1, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L296:
	leaq	0(%rbp,%rcx), %rbx
	xorl	%r12d, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L145:
	movq	48(%rsp), %rax
	movq	%r13, %rsi
	movq	%rcx, 80(%rsp)
	movl	%r8d, 56(%rsp)
	movq	(%rax), %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	movl	56(%rsp), %r8d
	movq	80(%rsp), %rcx
	jne	.L296
	movl	16(%rsp), %edx
	movl	$4, %eax
	cmpl	$1, %edx
	je	.L152
	cmpl	$28, %edx
	movl	$16, %eax
	movl	$-1, %esi
	cmovne	%esi, %eax
.L152:
	cmpl	%eax, %r8d
	jne	.L202
	movq	48(%rsp), %rax
	leaq	0(%rbp,%rcx), %rbx
	cmpl	20(%rax), %r8d
	jne	.L130
	movl	124(%rsp), %eax
	testl	%eax, %eax
	jne	.L154
	cmpq	$0, 1632(%rsp)
	je	.L155
	movq	1632(%rsp), %rsi
	movq	24(%rsp), %rax
	cmpl	(%rsi), %eax
	jge	.L155
	movl	%eax, (%rsi)
.L155:
	cmpq	$0, 1640(%rsp)
	je	.L156
	movq	1640(%rsp), %rax
	movq	%r13, (%rax)
.L156:
	movq	48(%rsp), %rax
	movq	%r13, %rdi
	movq	%rcx, 24(%rsp)
	movl	%r8d, 16(%rsp)
	movq	%r13, (%rax)
	call	strlen@PLT
	addl	$1, %eax
	subl	%eax, 60(%rsp)
	movq	24(%rsp), %rcx
	movl	16(%rsp), %r8d
	movslq	%eax, %rdx
	addq	%rdx, %r13
.L154:
	leaq	3(%r13), %rdi
	movl	60(%rsp), %esi
	movl	%r8d, 60(%rsp)
	andq	$-4, %rdi
	movq	%rdi, %rax
	subq	%r13, %rax
	subl	%eax, %esi
	cmpl	%esi, %r8d
	movl	%esi, 56(%rsp)
	jg	.L117
	movq	112(%rsp), %rdx
	movq	%rbp, %rsi
	movq	%rcx, 24(%rsp)
	leaq	8(%rdx), %rbx
	movq	%rdi, (%rdx)
	movq	%rcx, %rdx
	movq	%rbx, 16(%rsp)
	call	__mempcpy@PLT
	movl	60(%rsp), %r8d
	movq	%rax, %r13
	movl	56(%rsp), %eax
	movq	24(%rsp), %rcx
	addl	$1, 124(%rsp)
	subl	%r8d, %eax
	movl	%eax, 60(%rsp)
	leaq	0(%rbp,%rcx), %rbx
	movq	16(%rsp), %rax
	movq	%rax, 112(%rsp)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L144:
	movq	72(%rsp), %rdi
	movq	%r13, %rsi
	movq	%rcx, 16(%rsp)
	call	strcasecmp@PLT
	testl	%eax, %eax
	movq	16(%rsp), %rcx
	jne	.L296
	movq	8(%rsp), %rcx
	movq	(%rsp), %rdi
	movq	%rbp, %rdx
	movl	$255, %r8d
	movq	%r14, %rsi
	movl	$1, %r12d
	call	__ns_name_unpack@PLT
	cmpl	$-1, %eax
	movl	%eax, %ebp
	je	.L130
	movq	80(%rsp), %rdx
	movq	8(%rsp), %rdi
	movq	%r13, %rsi
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	je	.L292
	testl	%ebp, %ebp
	js	.L130
	movq	%r13, %rdi
	call	__res_hnok@PLT
	testl	%eax, %eax
	je	.L130
	cmpq	$0, 1632(%rsp)
	movq	%r13, %r15
	je	.L148
	movq	1632(%rsp), %rcx
	movl	56(%rsp), %eax
	cmpl	(%rcx), %eax
	jge	.L148
	movl	%eax, (%rcx)
.L148:
	movq	48(%rsp), %rax
	movq	%r15, (%rax)
	movq	1624(%rsp), %rax
	movl	$0, (%rax)
	movl	$1, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L300:
	cmpq	$0, 1632(%rsp)
	je	.L136
	movq	1632(%rsp), %rcx
	movq	24(%rsp), %rax
	cmpl	(%rcx), %eax
	jge	.L136
	movl	%eax, (%rcx)
.L136:
	movq	96(%rsp), %rax
	addq	$376, %rax
	cmpq	%rax, 104(%rsp)
	jnb	.L190
	movq	64(%rsp), %rcx
	movq	(%rsp), %rdi
	movl	$1025, %r8d
	movq	%rbp, %rdx
	movq	%r14, %rsi
	call	__dn_expand@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L202
	movq	64(%rsp), %rdi
	movq	40(%rsp), %rsi
	call	*%rsi
	testl	%eax, %eax
	je	.L202
	movq	104(%rsp), %rdx
	movslq	%r12d, %rbx
	movq	%r13, %rdi
	addq	%rbp, %rbx
	movq	%r13, (%rdx)
	leaq	8(%rdx), %rbp
	call	strlen@PLT
	addl	$1, %eax
	cmpl	$255, %eax
	jg	.L194
	movslq	%eax, %rdx
	subl	%eax, 60(%rsp)
	movq	64(%rsp), %rax
	addq	%rdx, %r13
.L137:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L137
	movl	%edx, %ecx
	movq	64(%rsp), %rsi
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	cmove	%rcx, %rax
	movl	%edx, %ecx
	addb	%dl, %cl
	sbbq	$3, %rax
	subq	%rsi, %rax
	leal	1(%rax), %r12d
	cmpl	%r12d, 60(%rsp)
	jl	.L117
	cmpl	$255, %r12d
	jg	.L194
	movq	48(%rsp), %rdx
	movq	%r13, %rdi
	movq	%r13, (%rdx)
	movslq	%r12d, %rdx
	call	__mempcpy@PLT
	subl	%r12d, 60(%rsp)
	movq	%rax, %r13
	movq	%rbp, 104(%rsp)
	xorl	%r12d, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L184:
	movq	72(%rsp), %rax
	movq	%rax, 128(%rsp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L157:
	movl	124(%rsp), %eax
	movq	%r13, %r15
	testl	%eax, %eax
	je	.L160
	movq	104(%rsp), %rcx
	movq	112(%rsp), %rdx
	cmpl	$1, %eax
	movq	$0, (%rcx)
	movq	$0, (%rdx)
	jle	.L161
	cmpb	$0, 123(%rsp)
	je	.L161
	movq	88(%rsp), %rax
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.L162
	movq	48(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L161
.L163:
	movl	124(%rsp), %eax
	movl	$48, %r12d
	movq	%r15, (%rsp)
	leaq	144(%rsp), %rbp
	movq	136(%rsp), %r15
	movq	88(%rsp), %r14
	cmpl	$48, %eax
	cmovle	%eax, %r12d
	xorl	%r8d, %r8d
	xorl	%ebx, %ebx
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L169:
	testl	%ebx, %ebx
	movw	%ax, 0(%rbp,%r8,2)
	jne	.L170
	testl	%r11d, %r11d
	jle	.L170
	movswl	-2(%rbp,%r8,2), %eax
	movl	%r11d, %ebx
	cmpl	%eax, %ecx
	movl	$0, %eax
	cmovge	%eax, %ebx
.L170:
	addq	$1, %r8
	cmpl	%r8d, %r12d
	jle	.L171
.L164:
	movq	(%r15,%r8,8), %rax
	movl	%r8d, %r11d
	movl	(%rax), %r10d
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L168:
	testq	%rdx, %rdx
	movl	%eax, %ecx
	je	.L165
	cmpq	48(%rdx), %rax
	jnb	.L169
	movq	40(%rdx), %rsi
	leaq	(%rsi,%rax,8), %r9
	movl	4(%r9), %r13d
	movl	(%r9), %esi
	andl	%r10d, %r13d
	movl	%r13d, %r9d
.L167:
	cmpl	%r9d, %esi
	je	.L169
	leal	1(%rax), %ecx
	addq	$1, %rax
	cmpq	%rdi, %rax
	jne	.L168
	movl	%ecx, %eax
	jmp	.L169
.L162:
	movq	88(%rsp), %rax
	movq	(%rax), %rax
	movzbl	392(%rax), %edi
	shrb	$4, %dil
	movzbl	%dil, %edi
	testq	%rdi, %rdi
	jne	.L163
	.p2align 4,,10
	.p2align 3
.L161:
	movq	48(%rsp), %rax
	cmpq	$0, (%rax)
	je	.L301
.L177:
	movq	1624(%rsp), %rax
	movl	$0, (%rax)
	movl	$1, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L190:
	xorl	%r12d, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L160:
	movq	1624(%rsp), %rax
	movq	104(%rsp), %rbx
	cmpq	%rbx, 96(%rsp)
	movl	$3, (%rax)
	movq	1616(%rsp), %rax
	movl	$2, (%rax)
	je	.L204
	cmpb	$0, 122(%rsp)
	je	.L204
	xorl	%eax, %eax
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L165:
	movq	(%r14), %r9
	movzbl	392(%r9), %esi
	shrb	$4, %sil
	movzbl	%sil, %esi
	cmpq	%rsi, %rax
	jnb	.L169
	movl	400(%r9,%rax,8), %r13d
	movl	396(%r9,%rax,8), %esi
	andl	%r10d, %r13d
	movl	%r13d, %r9d
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L298:
	movq	errno@gottpoff(%rip), %rax
	movl	$74, %fs:(%rax)
	movq	1616(%rsp), %rax
	movl	$74, (%rax)
	movq	1624(%rsp), %rax
	movl	$3, (%rax)
	movl	$-1, %eax
	jmp	.L114
.L301:
	movq	128(%rsp), %rdi
	call	strlen@PLT
	addl	$1, %eax
	cmpl	60(%rsp), %eax
	jg	.L117
	cmpl	$255, %eax
	jg	.L160
	movq	48(%rsp), %rbx
	movslq	%eax, %rdx
	cmpq	$8, %rdx
	movq	%r15, (%rbx)
	jnb	.L178
	testb	$4, %dl
	jne	.L302
	testq	%rdx, %rdx
	je	.L177
	movq	128(%rsp), %rax
	testb	$2, %dl
	movzbl	(%rax), %eax
	movb	%al, (%r15)
	je	.L177
	movq	128(%rsp), %rax
	movzwl	-2(%rax,%rdx), %eax
	movw	%ax, -2(%r15,%rdx)
	jmp	.L177
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%rbp, 104(%rsp)
	movl	$1, %r12d
	jmp	.L130
.L171:
	testl	%ebx, %ebx
	movq	(%rsp), %r15
	je	.L161
	movq	136(%rsp), %rsi
	movslq	%ebx, %rax
	leaq	142(%rsp,%rax,2), %r8
	leaq	(%rsi,%rax,8), %r9
	.p2align 4,,10
	.p2align 3
.L173:
	cmpl	%r12d, %ebx
	jge	.L161
	leal	-1(%rbx), %eax
	leaq	-2(%r8), %rdi
	movq	%r9, %rdx
	addq	%rax, %rax
	subq	%rax, %rdi
	movq	%r8, %rax
	.p2align 4,,10
	.p2align 3
.L175:
	movzwl	(%rax), %ecx
	movzwl	2(%rax), %esi
	cmpw	%si, %cx
	jle	.L174
	movw	%si, (%rax)
	movw	%cx, 2(%rax)
	subq	$2, %rax
	movq	-8(%rdx), %rcx
	movq	(%rdx), %rsi
	subq	$8, %rdx
	movq	%rsi, (%rdx)
	movq	%rcx, 8(%rdx)
	cmpq	%rdi, %rax
	jne	.L175
.L174:
	addl	$1, %ebx
	addq	$8, %r9
	addq	$2, %r8
	jmp	.L173
.L299:
	movq	1624(%rsp), %rax
	movl	$3, (%rax)
	movq	1616(%rsp), %rax
	movl	$2, (%rax)
	movl	$-2, %eax
	jmp	.L114
.L178:
	movq	128(%rsp), %rcx
	leaq	8(%r15), %rdi
	andq	$-8, %rdi
	movq	(%rcx), %rax
	movq	%rcx, %rsi
	movq	%rax, (%r15)
	movq	-8(%rcx,%rdx), %rax
	movq	%rax, -8(%r15,%rdx)
	movq	%r15, %rax
	subq	%rdi, %rax
	subq	%rax, %rsi
	addq	%rdx, %rax
	shrq	$3, %rax
	movq	%rax, %rcx
	rep movsq
	jmp	.L177
.L204:
	movl	$-2, %eax
	jmp	.L114
.L302:
	movq	128(%rsp), %rcx
	movl	(%rcx), %eax
	movl	%eax, (%r15)
	movl	-4(%rcx,%rdx), %eax
	movl	%eax, -4(%r15,%rdx)
	jmp	.L177
	.size	getanswer_r.constprop.2, .-getanswer_r.constprop.2
	.p2align 4,,15
	.type	gethostbyname3_context, @function
gethostbyname3_context:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbx
	movq	%rcx, %r12
	movq	%rsi, %rbx
	subq	$1096, %rsp
	cmpl	$2, %edx
	movq	%r8, -1112(%rbp)
	movq	%r9, -1120(%rbp)
	je	.L305
	cmpl	$10, %edx
	jne	.L324
	movl	$28, %r15d
	movl	$16, %eax
.L306:
	movq	errno@gottpoff(%rip), %rsi
	movl	%edx, 16(%r12)
	movq	%rbx, %rdi
	movl	%eax, 20(%r12)
	movl	%fs:(%rsi), %ecx
	movl	$46, %esi
	movl	%ecx, -1124(%rbp)
	call	strchr@PLT
	testq	%rax, %rax
	je	.L325
.L309:
	subq	$1040, %rsp
	leaq	-1096(%rbp), %rax
	movl	$1024, %r9d
	leaq	15(%rsp), %r10
	subq	$8, %rsp
	movl	%r15d, %ecx
	pushq	$0
	pushq	$0
	movl	$1, %edx
	andq	$-16, %r10
	pushq	$0
	pushq	$0
	pushq	%rax
	movq	%r10, %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%r10, %r14
	movq	%r10, -1096(%rbp)
	call	__res_context_search@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	jns	.L310
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$24, %eax
	jg	.L312
	cmpl	$23, %eax
	jge	.L313
	cmpl	$3, %eax
	jne	.L311
	movq	__h_errno@gottpoff(%rip), %rdx
	movl	$2, %ecx
	movl	$-2, %eax
	movl	$2, %fs:(%rdx)
.L316:
	movq	24(%rbp), %rbx
	movl	%ecx, (%rbx)
	cmpl	$2, %fs:(%rdx)
	je	.L326
	movq	errno@gottpoff(%rip), %rbx
	movl	-1124(%rbp), %esi
	movl	%esi, %fs:(%rbx)
.L318:
	movq	-1096(%rbp), %rdi
	cmpq	%r14, %rdi
	je	.L303
.L323:
	movl	%eax, -1112(%rbp)
	call	free@PLT
	movl	-1112(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L324:
	movq	24(%rbp), %rax
	movl	$4, (%rax)
	movq	16(%rbp), %rax
	movl	$97, (%rax)
	movl	$-1, %eax
.L303:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	movl	$1, %r15d
	movl	$4, %eax
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L310:
	movq	-1096(%rbp), %rsi
	pushq	40(%rbp)
	movq	%r13, %rdi
	pushq	32(%rbp)
	pushq	24(%rbp)
	movq	%r12, %r9
	pushq	16(%rbp)
	pushq	-1120(%rbp)
	movl	%r15d, %r8d
	pushq	-1112(%rbp)
	movq	%rbx, %rcx
	movl	%eax, %edx
	call	getanswer_r.constprop.2
	movq	-1096(%rbp), %rdi
	addq	$48, %rsp
	cmpq	%r14, %rdi
	jne	.L323
	jmp	.L303
	.p2align 4,,10
	.p2align 3
.L313:
	movq	__h_errno@gottpoff(%rip), %rdx
	movl	$-1, %ecx
	movl	%ecx, %eax
	movl	$-1, %fs:(%rdx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L312:
	subl	$110, %eax
	cmpl	$1, %eax
	ja	.L311
	movq	__h_errno@gottpoff(%rip), %rdx
	movl	$-1, %eax
	movl	%fs:(%rdx), %ecx
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L326:
	movq	16(%rbp), %rbx
	movl	$11, (%rbx)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L325:
	leaq	-1088(%rbp), %rdx
	movq	%rbx, %rsi
	movl	$1025, %ecx
	movq	%r13, %rdi
	call	__res_context_hostalias@PLT
	testq	%rax, %rax
	cmovne	%rax, %rbx
	jmp	.L309
	.p2align 4,,10
	.p2align 3
.L311:
	movq	__h_errno@gottpoff(%rip), %rdx
	xorl	%eax, %eax
	movl	%fs:(%rdx), %ecx
	jmp	.L316
	.size	gethostbyname3_context, .-gethostbyname3_context
	.p2align 4,,15
	.globl	_nss_dns_gethostbyname3_r
	.type	_nss_dns_gethostbyname3_r, @function
_nss_dns_gethostbyname3_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %r14
	movq	%r9, %rbp
	subq	$24, %rsp
	movq	%r8, 8(%rsp)
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	movq	8(%rsp), %r8
	je	.L331
	pushq	96(%rsp)
	pushq	96(%rsp)
	movq	%rax, %rbx
	pushq	96(%rsp)
	pushq	%rbp
	movq	%r8, %r9
	movq	%rax, %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	movl	%r13d, %edx
	movq	%r12, %rsi
	call	gethostbyname3_context
	addq	$32, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L327:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 0(%rbp)
	movq	80(%rsp), %rax
	movl	$-1, %ebp
	movl	$-1, (%rax)
	jmp	.L327
	.size	_nss_dns_gethostbyname3_r, .-_nss_dns_gethostbyname3_r
	.p2align 4,,15
	.globl	_nss_dns_gethostbyname2_r
	.type	_nss_dns_gethostbyname2_r, @function
_nss_dns_gethostbyname2_r:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	%r8, %r14
	subq	$8, %rsp
	call	__res_hnok@PLT
	testl	%eax, %eax
	jne	.L336
	movq	64(%rsp), %rax
	movl	$1, (%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L336:
	subq	$8, %rsp
	movq	%r15, %r9
	movq	%r14, %r8
	pushq	$0
	pushq	$0
	movq	%r13, %rcx
	pushq	88(%rsp)
	movq	%r12, %rdx
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	_nss_dns_gethostbyname3_r@PLT
	addq	$32, %rsp
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	_nss_dns_gethostbyname2_r, .-_nss_dns_gethostbyname2_r
	.p2align 4,,15
	.globl	_nss_dns_gethostbyname_r
	.type	_nss_dns_gethostbyname_r, @function
_nss_dns_gethostbyname_r:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rcx, %r14
	movq	%r8, %rbp
	subq	$24, %rsp
	call	__res_hnok@PLT
	testl	%eax, %eax
	jne	.L344
	movl	$1, (%r15)
	xorl	%eax, %eax
.L337:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L345
	pushq	$0
	pushq	$0
	movq	%r14, %r9
	pushq	%r15
	pushq	%rbp
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbx, %rsi
	movl	$2, %edx
	movq	%rax, %rdi
	movq	%rax, 40(%rsp)
	call	gethostbyname3_context
	movl	%eax, 36(%rsp)
	addq	$32, %rsp
	movq	8(%rsp), %r10
	movq	%r10, %rdi
	call	__resolv_context_put@PLT
	movl	4(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L345:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 0(%rbp)
	movl	$-1, (%r15)
	movl	$-1, %eax
	jmp	.L337
	.size	_nss_dns_gethostbyname_r, .-_nss_dns_gethostbyname_r
	.p2align 4,,15
	.globl	_nss_dns_gethostbyname4_r
	.type	_nss_dns_gethostbyname4_r, @function
_nss_dns_gethostbyname4_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r15
	pushq	%rbx
	movq	%r9, %r13
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	%rsi, -120(%rbp)
	movq	%rdx, -128(%rbp)
	movq	%rcx, -136(%rbp)
	call	__res_hnok@PLT
	testl	%eax, %eax
	jne	.L386
	movl	$1, 0(%r13)
	xorl	%ebx, %ebx
.L346:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L387
	movl	$46, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L388
.L352:
	movq	errno@gottpoff(%rip), %rax
	subq	$2064, %rsp
	movq	%rbx, %rsi
	leaq	15(%rsp), %r12
	subq	$8, %rsp
	movl	$2048, %r9d
	movl	$439963904, %ecx
	movl	$1, %edx
	movq	%r14, %rdi
	movl	%fs:(%rax), %eax
	andq	$-16, %r12
	movq	$0, -80(%rbp)
	movq	%r12, %r8
	movq	%r12, -88(%rbp)
	movl	$0, -104(%rbp)
	movl	$0, -100(%rbp)
	movl	$0, -96(%rbp)
	movl	%eax, -140(%rbp)
	leaq	-96(%rbp), %rax
	pushq	%rax
	leaq	-100(%rbp), %rax
	pushq	%rax
	leaq	-104(%rbp), %rax
	pushq	%rax
	leaq	-80(%rbp), %rax
	pushq	%rax
	leaq	-88(%rbp), %rax
	pushq	%rax
	call	__res_context_search@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	movl	%eax, %ebx
	jns	.L389
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$24, %eax
	jg	.L361
	cmpl	$23, %eax
	jge	.L362
	cmpl	$3, %eax
	jne	.L360
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$2, %edx
	movl	$-2, %ebx
	movl	$2, %fs:(%rax)
.L365:
	movl	%edx, 0(%r13)
	cmpl	$2, %fs:(%rax)
	je	.L390
	movq	errno@gottpoff(%rip), %rax
	movl	-140(%rbp), %ecx
	movl	%ecx, %fs:(%rax)
.L359:
	movl	-96(%rbp), %eax
	testl	%eax, %eax
	jne	.L391
.L367:
	movq	-88(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L368
	call	free@PLT
.L368:
	movq	%r14, %rdi
	call	__resolv_context_put@PLT
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L389:
	movq	-120(%rbp), %rax
	movl	-100(%rbp), %r11d
	movq	-80(%rbp), %r10
	movq	-88(%rbp), %rdi
	movl	$1, -92(%rbp)
	movq	%rax, -72(%rbp)
	movq	-128(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-136(%rbp), %rax
	movq	%rax, -56(%rbp)
	jne	.L392
.L354:
	testq	%r10, %r10
	je	.L359
	testl	%r11d, %r11d
	jle	.L359
	leaq	-92(%rbp), %rax
	subq	$8, %rsp
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	-56(%rbp), %r8
	movq	%r15, %r9
	pushq	%rax
	pushq	16(%rbp)
	movl	%r11d, %esi
	pushq	%r13
	movq	%r10, %rdi
	call	gaih_getanswer_slice.isra.0
	addq	$32, %rsp
	cmpl	$1, %ebx
	je	.L357
	testl	%eax, %eax
	cmovne	%eax, %ebx
.L357:
	cmpl	$1, %ebx
	jne	.L359
	cmpl	$-2, %eax
	jne	.L359
	cmpl	$34, (%r15)
	jne	.L359
	cmpl	$3, 0(%r13)
	je	.L359
.L356:
	movl	$-2, %ebx
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L391:
	movq	-80(%rbp), %rdi
	call	free@PLT
	jmp	.L367
	.p2align 4,,10
	.p2align 3
.L392:
	leaq	-92(%rbp), %rax
	subq	$8, %rsp
	leaq	-64(%rbp), %rcx
	leaq	-72(%rbp), %rdx
	leaq	-56(%rbp), %r8
	movl	%ebx, %esi
	pushq	%rax
	pushq	16(%rbp)
	movq	%r15, %r9
	pushq	%r13
	movq	%r10, -128(%rbp)
	movl	%r11d, -120(%rbp)
	call	gaih_getanswer_slice.isra.0
	addq	$32, %rsp
	cmpl	$1, %eax
	movl	%eax, %ebx
	movl	-120(%rbp), %r11d
	movq	-128(%rbp), %r10
	jbe	.L354
	cmpl	$-2, %eax
	jne	.L359
	cmpl	$34, (%r15)
	jne	.L354
	cmpl	$3, 0(%r13)
	je	.L354
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L361:
	subl	$110, %eax
	cmpl	$1, %eax
	ja	.L360
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	%fs:(%rax), %edx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L390:
	movl	$11, (%r15)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L388:
	subq	$1040, %rsp
	movq	%rbx, %rsi
	movl	$1025, %ecx
	leaq	15(%rsp), %rdx
	movq	%r14, %rdi
	andq	$-16, %rdx
	call	__res_context_hostalias@PLT
	testq	%rax, %rax
	cmovne	%rax, %rbx
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L360:
	movq	__h_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	%fs:(%rax), %edx
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L362:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %edx
	movl	%edx, %ebx
	movl	$-1, %fs:(%rax)
	jmp	.L365
.L387:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	%fs:(%rax), %eax
	movl	%eax, (%r15)
	movl	$-1, 0(%r13)
	jmp	.L346
	.size	_nss_dns_gethostbyname4_r, .-_nss_dns_gethostbyname4_r
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%u.%u.%u.%u.in-addr.arpa"
	.text
	.p2align 4,,15
	.globl	__GI__nss_dns_gethostbyaddr2_r
	.hidden	__GI__nss_dns_gethostbyaddr2_r
	.type	__GI__nss_dns_gethostbyaddr2_r, @function
__GI__nss_dns_gethostbyaddr2_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%r8, %rbx
	negq	%rbx
	subq	$1112, %rsp
	movq	errno@gottpoff(%rip), %rax
	andl	$7, %ebx
	cmpq	%r9, %rbx
	movl	%esi, -1108(%rbp)
	movq	%rcx, -1120(%rbp)
	movl	%fs:(%rax), %eax
	movl	%eax, -1112(%rbp)
	jb	.L431
.L394:
	movq	16(%rbp), %rax
	movl	$-2, %r14d
	movl	$34, (%rax)
	movq	24(%rbp), %rax
	movl	$-1, (%rax)
.L393:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L431:
	subq	%rbx, %r9
	cmpq	$791, %r9
	movq	%r9, -1144(%rbp)
	jbe	.L394
	movq	%rdi, %r12
	movl	%edx, %r15d
	movq	%r8, %r14
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L432
	cmpl	$10, %r15d
	jne	.L398
	cmpl	$16, -1108(%rbp)
	jne	.L398
	movq	(%r12), %rax
	cmpq	mapped.11725(%rip), %rax
	je	.L433
.L399:
	cmpq	tunnelled.11726(%rip), %rax
	je	.L434
.L427:
	movq	$16, -1136(%rbp)
	movl	$16, -1108(%rbp)
.L405:
	subq	$1040, %rsp
	leaq	-1088(%rbp), %r15
	leaq	15(%r12), %rcx
	leaq	15(%rsp), %r8
	leaq	nibblechar.11755(%rip), %rsi
	leaq	64(%r15), %rdi
	movq	%r15, %rax
	andq	$-16, %r8
	movq	%r8, -1128(%rbp)
	movq	%r8, -1096(%rbp)
	.p2align 4,,10
	.p2align 3
.L411:
	movzbl	(%rcx), %edx
	movb	$46, 1(%rax)
	addq	$4, %rax
	subq	$1, %rcx
	movq	%rdx, %r9
	shrb	$4, %dl
	andl	$15, %r9d
	andl	$15, %edx
	movzbl	(%rsi,%r9), %r9d
	movzbl	(%rsi,%rdx), %edx
	movb	%r9b, -4(%rax)
	movb	%dl, -2(%rax)
	cmpq	%rdi, %rax
	movb	$46, -1(%rax)
	jne	.L411
	movabsq	$7021237580783317097, %rdi
	movb	$0, 8(%rax)
	movl	$10, -1148(%rbp)
	movq	%rdi, (%rax)
.L410:
	leaq	-1096(%rbp), %rax
	subq	$8, %rsp
	movl	$1024, %r9d
	pushq	$0
	pushq	$0
	movl	$12, %ecx
	pushq	$0
	pushq	$0
	movl	$1, %edx
	pushq	%rax
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__res_context_query@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	jns	.L412
	movq	__h_errno@gottpoff(%rip), %rax
	movq	24(%rbp), %rdi
	movl	-1112(%rbp), %esi
	movl	%fs:(%rax), %eax
	movl	%eax, (%rdi)
	movq	-1096(%rbp), %rdi
	cmpq	-1128(%rbp), %rdi
	movq	errno@gottpoff(%rip), %rax
	movl	%esi, %fs:(%rax)
	je	.L413
	call	free@PLT
.L413:
	movq	%r13, %rdi
	xorl	%r14d, %r14d
	call	__resolv_context_put@PLT
	movq	errno@gottpoff(%rip), %rax
	cmpl	$111, %fs:(%rax)
	sete	%r14b
	negl	%r14d
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L398:
	cmpl	$2, %r15d
	je	.L408
	cmpl	$10, %r15d
	je	.L409
.L416:
	movq	16(%rbp), %rax
	movq	%r13, %rdi
	movl	$-1, %r14d
	movl	$97, (%rax)
	movq	24(%rbp), %rax
	movl	$-1, (%rax)
	call	__resolv_context_put@PLT
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L412:
	addq	%r14, %rbx
	movq	-1120(%rbp), %r9
	movq	-1096(%rbp), %rsi
	pushq	$0
	pushq	32(%rbp)
	movq	%r13, %rdi
	pushq	24(%rbp)
	pushq	16(%rbp)
	movl	$12, %r8d
	pushq	-1144(%rbp)
	pushq	%rbx
	movq	%r15, %rcx
	movl	%eax, %edx
	call	getanswer_r.constprop.2
	movq	-1096(%rbp), %rdi
	addq	$48, %rsp
	cmpq	-1128(%rbp), %rdi
	movl	%eax, %r14d
	je	.L414
	call	free@PLT
.L414:
	cmpl	$1, %r14d
	jne	.L430
	movq	-1120(%rbp), %rax
	movl	-1148(%rbp), %edi
	leaq	384(%rbx), %rcx
	movl	-1108(%rbp), %esi
	movq	-1136(%rbp), %rdx
	movl	%edi, 16(%rax)
	movq	%rcx, %rdi
	movl	%esi, 20(%rax)
	movq	%r12, %rsi
	call	memcpy@PLT
	movq	%rax, 400(%rbx)
	movq	24(%rbp), %rax
	movq	$0, 408(%rbx)
	movl	$0, (%rax)
.L430:
	movq	%r13, %rdi
	call	__resolv_context_put@PLT
	jmp	.L393
	.p2align 4,,10
	.p2align 3
.L433:
	movl	8+mapped.11725(%rip), %esi
	cmpl	%esi, 8(%r12)
	jne	.L399
.L406:
	subq	$1040, %rsp
	addq	$12, %r12
	movl	$4, -1108(%rbp)
	leaq	15(%rsp), %rax
	movq	$4, -1136(%rbp)
	andq	$-16, %rax
	movq	%rax, -1128(%rbp)
	movq	%rax, -1096(%rbp)
.L402:
	movzbl	1(%r12), %r8d
	movzbl	2(%r12), %ecx
	leaq	-1088(%rbp), %r15
	movzbl	3(%r12), %edx
	movzbl	(%r12), %r9d
	leaq	.LC0(%rip), %rsi
	movq	%r15, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	-1096(%rbp), %r8
	movl	$2, -1148(%rbp)
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L409:
	movl	-1108(%rbp), %eax
	cmpq	$15, %rax
	movq	%rax, -1136(%rbp)
	jbe	.L416
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L434:
	movl	8+tunnelled.11726(%rip), %eax
	cmpl	%eax, 8(%r12)
	jne	.L427
	movl	12(%r12), %eax
	cmpl	%eax, v6local.11727(%rip)
	jne	.L406
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L408:
	movl	-1108(%rbp), %eax
	cmpq	$3, %rax
	movq	%rax, -1136(%rbp)
	jbe	.L416
	subq	$1040, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -1128(%rbp)
	movq	%rax, -1096(%rbp)
	jmp	.L402
.L432:
	movq	errno@gottpoff(%rip), %rax
	movq	16(%rbp), %rbx
	movl	$-1, %r14d
	movl	%fs:(%rax), %eax
	movl	%eax, (%rbx)
	movq	24(%rbp), %rax
	movl	$-1, (%rax)
	jmp	.L393
	.size	__GI__nss_dns_gethostbyaddr2_r, .-__GI__nss_dns_gethostbyaddr2_r
	.globl	_nss_dns_gethostbyaddr2_r
	.set	_nss_dns_gethostbyaddr2_r,__GI__nss_dns_gethostbyaddr2_r
	.p2align 4,,15
	.globl	_nss_dns_gethostbyaddr_r
	.type	_nss_dns_gethostbyaddr_r, @function
_nss_dns_gethostbyaddr_r:
	subq	$16, %rsp
	pushq	$0
	pushq	40(%rsp)
	pushq	40(%rsp)
	call	__GI__nss_dns_gethostbyaddr2_r
	addq	$40, %rsp
	ret
	.size	_nss_dns_gethostbyaddr_r, .-_nss_dns_gethostbyaddr_r
	.section	.rodata
	.align 16
	.type	nibblechar.11755, @object
	.size	nibblechar.11755, 16
nibblechar.11755:
	.ascii	"0123456789abcdef"
	.type	v6local.11727, @object
	.size	v6local.11727, 4
v6local.11727:
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.align 8
	.type	tunnelled.11726, @object
	.size	tunnelled.11726, 12
tunnelled.11726:
	.zero	12
	.align 8
	.type	mapped.11725, @object
	.size	mapped.11725, 12
mapped.11725:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-1
	.byte	-1
