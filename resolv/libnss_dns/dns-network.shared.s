	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"in-addr.arpa"
	.text
	.p2align 4,,15
	.type	getanswer_r, @function
getanswer_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	negq	%r14
	pushq	%r13
	pushq	%r12
	andl	$7, %r14d
	pushq	%rbp
	pushq	%rbx
	leaq	384(%r14), %rax
	subq	$344, %rsp
	cmpq	%r8, %rax
	movq	%rdx, 24(%rsp)
	movq	%rcx, 8(%rsp)
	movq	%r9, 32(%rsp)
	ja	.L15
	subq	%r14, %r8
	movzwl	4(%rdi), %edx
	movzwl	6(%rdi), %ebp
	leal	-384(%r8), %r13d
	subq	$384, %r8
	movq	%rdi, %r12
	movslq	%r13d, %rax
	cmpq	%rax, %r8
	movl	$2147483647, %eax
	cmovne	%eax, %r13d
	rolw	$8, %dx
	movl	%ebp, %eax
	movzwl	%dx, %edx
	rolw	$8, %ax
	testl	%edx, %edx
	movw	%ax, 16(%rsp)
	je	.L5
	movslq	%esi, %rsi
	leaq	12(%rdi), %r15
	leal	-1(%rdx), %ebp
	leaq	(%rdi,%rsi), %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L83:
	cltq
	movq	%rbx, %rcx
	leaq	(%r15,%rax), %rdx
	subq	%rdx, %rcx
	cmpq	$3, %rcx
	jle	.L8
	subl	$1, %ebp
	leaq	4(%r15,%rax), %r15
	cmpl	$-1, %ebp
	je	.L82
.L6:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	__dn_skipname@PLT
	testl	%eax, %eax
	jns	.L83
.L8:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	testb	$4, 2(%rdi)
	movq	__h_errno@gottpoff(%rip), %rax
	je	.L80
	movl	$1, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$344, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	addq	8(%rsp), %r14
	movq	24(%rsp), %rsi
	leaq	80(%rsp), %rax
	movzwl	16(%rsp), %ebp
	movl	$0, 40(%rsp)
	movq	%rax, 16(%rsp)
	leaq	384(%r14), %rdi
	movq	%r14, 8(%rsi)
	movq	%r14, 48(%rsp)
	movq	$0, (%r14)
	movq	%rdi, 64(%rsp)
	movq	%rdi, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L10:
	subl	$1, %ebp
	cmpl	$-1, %ebp
	je	.L16
	cmpq	%rbx, %r15
	jnb	.L16
	movq	16(%rsp), %rcx
	movl	$255, %r8d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__ns_name_unpack@PLT
	cmpl	$-1, %eax
	movl	%eax, %r14d
	je	.L16
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	movslq	%r13d, %rax
	movq	%rax, %rdx
	movq	%rax, 56(%rsp)
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	je	.L84
	testl	%r14d, %r14d
	js	.L16
	movq	8(%rsp), %rdi
	call	__res_dnok@PLT
	testl	%eax, %eax
	je	.L16
	movslq	%r14d, %rcx
	movq	%rbx, %rax
	addq	%rcx, %r15
	subq	%r15, %rax
	cmpq	$9, %rax
	jle	.L8
	movzwl	(%r15), %eax
	movzwl	8(%r15), %r9d
	movq	%rbx, %rcx
	addq	$10, %r15
	movl	%eax, %edx
	movzwl	-8(%r15), %eax
	rolw	$8, %r9w
	subq	%r15, %rcx
	movzwl	%r9w, %r14d
	rolw	$8, %dx
	rolw	$8, %ax
	cmpq	%r14, %rcx
	jl	.L8
	cmpw	$1, %ax
	jne	.L17
	cmpw	$12, %dx
	jne	.L17
	movq	16(%rsp), %rcx
	movl	$255, %r8d
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__ns_name_unpack@PLT
	cmpl	$-1, %eax
	jne	.L18
.L20:
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	addq	%r14, %r15
	jmp	.L10
.L84:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$90, %fs:(%rax)
	je	.L15
	.p2align 4,,10
	.p2align 3
.L16:
	movl	40(%rsp), %eax
	testl	%eax, %eax
	je	.L13
	movq	24(%rsp), %rdi
	cmpl	$1, 408(%rsp)
	movq	48(%rsp), %rax
	movq	8(%rdi), %rsi
	movq	$0, (%rax)
	movq	%rsi, 40(%rsp)
	jne	.L85
	movq	40(%rsp), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L13
	movq	%r14, %r13
	.p2align 4,,10
	.p2align 3
.L36:
	movsbl	0(%r13), %ebx
	xorl	%ebp, %ebp
	movl	$0, 32(%rsp)
.L35:
	cmpb	$48, %bl
	movzbl	1(%r13), %r14d
	movl	$10, %r12d
	je	.L86
.L26:
	call	__ctype_b_loc@PLT
	xorl	%edx, %edx
	movq	(%rax), %rsi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L87:
	leal	-48(%rbx), %edi
	cmpl	%r12d, %edi
	jnb	.L30
	imull	%r12d, %edx
	leal	-48(%rbx,%rdx), %edx
.L31:
	testb	%r14b, %r14b
	leaq	1(%r13), %rax
	je	.L32
.L88:
	cmpb	$46, %r14b
	je	.L32
	movsbl	%r14b, %ebx
	movzbl	1(%rax), %r14d
	movq	%rax, %r13
.L33:
	movsbq	%bl, %r15
	movzwl	(%rsi,%r15,2), %eax
	testb	$8, %ah
	jne	.L87
.L30:
	cmpl	$16, %r12d
	jne	.L31
	testb	$16, %ah
	je	.L31
	movl	%edx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	__ctype_tolower_loc@PLT
	movq	(%rax), %rax
	movl	16(%rsp), %edx
	movq	8(%rsp), %rsi
	movl	(%rax,%r15,4), %eax
	sall	$4, %edx
	testb	%r14b, %r14b
	leal	-87(%rdx,%rax), %edx
	leaq	1(%r13), %rax
	jne	.L88
	.p2align 4,,10
	.p2align 3
.L32:
	cmpb	$46, %r14b
	jne	.L29
	movsbl	1(%rax), %ebx
	movl	%ebp, %ecx
	addq	$2, %r13
	sall	%cl, %edx
	addl	$8, %ebp
	orl	%edx, 32(%rsp)
	movsbq	%bl, %rax
	testb	$8, 1(%rsi,%rax,2)
	jne	.L34
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L89
.L34:
	cmpl	$32, %ebp
	jne	.L35
.L29:
	addq	$8, 40(%rsp)
	movq	40(%rsp), %rax
	movq	(%rax), %r13
	testq	%r13, %r13
	jne	.L36
.L13:
	movq	__h_errno@gottpoff(%rip), %rax
.L80:
	movl	$2, %fs:(%rax)
	movl	$-2, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movq	56(%rsp), %rdx
	movq	8(%rsp), %rsi
	movq	16(%rsp), %rdi
	movl	%eax, 76(%rsp)
	call	__ns_name_ntop@PLT
	cmpl	$-1, %eax
	movl	76(%rsp), %ecx
	je	.L90
	testl	%ecx, %ecx
	js	.L20
	movq	8(%rsp), %rdi
	call	__res_hnok@PLT
	testl	%eax, %eax
	je	.L20
	movq	48(%rsp), %rsi
	addq	%r14, %r15
	leaq	16(%rsi), %rax
	cmpq	%rax, 64(%rsp)
	jbe	.L10
	movq	8(%rsp), %rax
	leaq	8(%rsi), %r14
	movq	%rax, (%rsi)
	movq	%rax, %rdi
	call	strlen@PLT
	addl	$1, %eax
	addl	$1, 40(%rsp)
	movq	%r14, 48(%rsp)
	movslq	%eax, %rdx
	subl	%eax, %r13d
	movq	24(%rsp), %rax
	addq	%rdx, 8(%rsp)
	movl	$2, 16(%rax)
	jmp	.L10
.L90:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$90, %fs:(%rax)
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L15:
	movq	32(%rsp), %rax
	movl	$34, (%rax)
	movq	400(%rsp), %rax
	movl	$-1, (%rax)
	movl	$-2, %eax
	jmp	.L1
.L85:
	leaq	8(%rsi), %rax
	movq	%rax, 8(%rdi)
	movq	(%rsi), %rax
	movl	$0, 20(%rdi)
	movq	%rax, (%rdi)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L86:
	cmpb	$46, %r14b
	je	.L26
	movl	%r14d, %eax
	andl	$-33, %eax
	cmpb	$88, %al
	je	.L27
	leaq	1(%r13), %rax
	movsbl	%r14b, %ebx
	movl	$8, %r12d
.L28:
	testb	%bl, %bl
	je	.L29
	movzbl	1(%rax), %r14d
	movq	%rax, %r13
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	movsbl	2(%r13), %ebx
	leaq	2(%r13), %rax
	cmpb	$46, %bl
	je	.L29
	movl	$16, %r12d
	jmp	.L28
.L89:
	movq	24(%rsp), %rax
	movl	32(%rsp), %edi
	movl	%edi, 20(%rax)
	movl	$1, %eax
	jmp	.L1
	.size	getanswer_r, .-getanswer_r
	.p2align 4,,15
	.globl	_nss_dns_getnetbyname_r
	.type	_nss_dns_getnetbyname_r, @function
_nss_dns_getnetbyname_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r15
	pushq	%rbx
	movq	%r8, %r12
	movq	%r9, %r13
	subq	$56, %rsp
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%rdx, -88(%rbp)
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	movq	-72(%rbp), %rdi
	je	.L98
	subq	$1040, %rsp
	movq	%rax, %rbx
	leaq	-56(%rbp), %rax
	leaq	15(%rsp), %r10
	subq	$8, %rsp
	movq	%rdi, %rsi
	pushq	$0
	pushq	$0
	movl	$1024, %r9d
	andq	$-16, %r10
	pushq	$0
	pushq	$0
	pushq	%rax
	movq	%r10, %r8
	movl	$12, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r10, %r14
	movq	%r10, -56(%rbp)
	call	__res_context_search@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	jns	.L94
	movq	errno@gottpoff(%rip), %r13
	movq	-56(%rbp), %rdi
	movl	%fs:0(%r13), %eax
	cmpq	%r14, %rdi
	movl	%eax, (%r12)
	je	.L95
	call	free@PLT
.L95:
	movq	%rbx, %rdi
	call	__resolv_context_put@PLT
	movl	%fs:0(%r13), %eax
	leal	-96(%rax), %edx
	cmpl	$1, %edx
	setbe	%r12b
	cmpl	$111, %eax
	sete	%al
	orl	%eax, %r12d
	movzbl	%r12b, %r12d
	negl	%r12d
.L91:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	-80(%rbp), %rdx
	movq	-56(%rbp), %rdi
	movq	%r12, %r9
	movq	-88(%rbp), %rcx
	pushq	$1
	movq	%r15, %r8
	pushq	%r13
	movl	%eax, %esi
	call	getanswer_r
	movq	-56(%rbp), %rdi
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	cmpq	%r14, %rdi
	je	.L96
	call	free@PLT
.L96:
	movq	%rbx, %rdi
	call	__resolv_context_put@PLT
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L98:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%r12)
	movl	$-1, 0(%r13)
	movl	$-1, %r12d
	jmp	.L91
	.size	_nss_dns_getnetbyname_r, .-_nss_dns_getnetbyname_r
	.section	.rodata.str1.1
.LC1:
	.string	"0.0.0.%u.in-addr.arpa"
.LC2:
	.string	"0.0.%u.%u.in-addr.arpa"
.LC3:
	.string	"0.%u.%u.%u.in-addr.arpa"
.LC4:
	.string	"%u.%u.%u.%u.in-addr.arpa"
	.text
	.p2align 4,,15
	.globl	_nss_dns_getnetbyaddr_r
	.type	_nss_dns_getnetbyaddr_r, @function
_nss_dns_getnetbyaddr_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	$-1, %r12d
	pushq	%rbx
	subq	$1112, %rsp
	movq	errno@gottpoff(%rip), %rax
	cmpl	$2, %esi
	movq	%rdx, -1128(%rbp)
	movq	%rcx, -1144(%rbp)
	movq	%r8, -1152(%rbp)
	movq	%r9, -1136(%rbp)
	movl	%fs:(%rax), %r15d
	jne	.L99
	movl	%edi, %ebx
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L101
	testl	%ebx, %ebx
	leaq	-1088(%rbp), %r12
	je	.L103
	leaq	-1104(%rbp), %rdi
	movl	%ebx, %edx
	movl	$3, %eax
.L102:
	movzbl	%dl, %ecx
	shrl	$8, %edx
	movl	%eax, %esi
	movl	%ecx, (%rdi,%rax,4)
	subq	$1, %rax
	testl	%edx, %edx
	jne	.L102
	cmpl	$1, %esi
	leaq	-1088(%rbp), %r12
	je	.L104
	jle	.L125
	cmpl	$2, %esi
	je	.L107
	cmpl	$3, %esi
	jne	.L103
	movl	-1092(%rbp), %edx
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	.p2align 4,,10
	.p2align 3
.L103:
	subq	$1040, %rsp
	leaq	-1112(%rbp), %rax
	movl	$1024, %r9d
	leaq	15(%rsp), %r10
	subq	$8, %rsp
	movl	$12, %ecx
	pushq	$0
	pushq	$0
	movl	$1, %edx
	andq	$-16, %r10
	pushq	$0
	pushq	$0
	pushq	%rax
	movq	%r10, %r8
	movq	%r12, %rsi
	movq	%r13, %rdi
	movq	%r10, %r14
	movq	%r10, -1112(%rbp)
	call	__res_context_query@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	jns	.L109
	movq	-1112(%rbp), %rdi
	movq	errno@gottpoff(%rip), %rax
	cmpq	%r14, %rdi
	movl	%fs:(%rax), %ebx
	movl	%r15d, %fs:(%rax)
	je	.L110
	call	free@PLT
.L110:
	movq	%r13, %rdi
	call	__resolv_context_put@PLT
	leal	-96(%rbx), %eax
	cmpl	$1, %eax
	setbe	%r12b
	cmpl	$111, %ebx
	sete	%al
	orl	%eax, %r12d
	movzbl	%r12b, %r12d
	negl	%r12d
.L99:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L109:
	movq	-1128(%rbp), %rdx
	movq	-1112(%rbp), %rdi
	movl	%eax, %esi
	movq	-1136(%rbp), %r9
	movq	-1152(%rbp), %r8
	movq	-1144(%rbp), %rcx
	pushq	$0
	pushq	16(%rbp)
	call	getanswer_r
	movq	-1112(%rbp), %rdi
	movl	%eax, %r12d
	popq	%rax
	popq	%rdx
	cmpq	%r14, %rdi
	je	.L111
	call	free@PLT
.L111:
	cmpl	$1, %r12d
	je	.L126
.L112:
	movq	%r13, %rdi
	call	__resolv_context_put@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L125:
	testl	%esi, %esi
	jne	.L103
	movl	-1104(%rbp), %r9d
	movl	-1100(%rbp), %r8d
	leaq	.LC4(%rip), %rsi
	movl	-1096(%rbp), %ecx
	movl	-1092(%rbp), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L107:
	movl	-1096(%rbp), %ecx
	movl	-1092(%rbp), %edx
	leaq	.LC2(%rip), %rsi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L104:
	movl	-1100(%rbp), %r8d
	movl	-1096(%rbp), %ecx
	leaq	.LC3(%rip), %rsi
	movl	-1092(%rbp), %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L126:
	testb	%bl, %bl
	je	.L124
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L127:
	shrl	$8, %ebx
	testb	%bl, %bl
	jne	.L113
.L124:
	testl	%ebx, %ebx
	jne	.L127
.L113:
	movq	-1128(%rbp), %rax
	movl	%ebx, 20(%rax)
	jmp	.L112
.L101:
	movq	errno@gottpoff(%rip), %rax
	movq	-1136(%rbp), %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, (%rcx)
	movq	16(%rbp), %rax
	movl	%r12d, (%rax)
	jmp	.L99
	.size	_nss_dns_getnetbyaddr_r, .-_nss_dns_getnetbyaddr_r
