	.text
	.p2align 4,,15
	.globl	_nss_dns_getcanonname_r
	.type	_nss_dns_getcanonname_r, @function
_nss_dns_getcanonname_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$136, %rsp
	leaq	96(%rsp), %rbp
	movq	%rdi, 8(%rsp)
	movq	%rsi, 32(%rsp)
	movq	%rdx, 40(%rsp)
	movq	%rcx, 48(%rsp)
	movq	%r8, 56(%rsp)
	movq	%r9, 24(%rsp)
	movq	%rbp, 88(%rsp)
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L29
	movq	%rax, %r13
	leaq	88(%rsp), %rax
	xorl	%ebx, %ebx
	movl	$1, %r12d
	movq	%r13, (%rsp)
	movq	%rax, 16(%rsp)
.L2:
	subq	$8, %rsp
	movswl	%r12w, %ecx
	movl	$20, %r9d
	pushq	$0
	pushq	$0
	movq	%rbp, %r8
	pushq	$0
	pushq	$0
	movl	$1, %edx
	pushq	56(%rsp)
	movq	56(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	__res_context_query@PLT
	addq	$48, %rsp
	testl	%eax, %eax
	jle	.L4
	movq	88(%rsp), %r15
	cmpw	$256, 4(%r15)
	je	.L30
.L6:
	addq	$2, %rbx
	cmpq	$4, %rbx
	je	.L7
.L18:
	leaq	qtypes(%rip), %rax
	movswq	(%rax,%rbx), %r12
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L32:
	movq	72(%rsp), %rbx
	movq	64(%rsp), %rbp
	.p2align 4,,10
	.p2align 3
.L4:
	movq	88(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.L6
	addq	$2, %rbx
	call	free@PLT
	cmpq	$4, %rbx
	movq	%rbp, 88(%rsp)
	jne	.L18
.L7:
	movq	(%rsp), %r13
.L27:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	%fs:(%rax), %eax
.L9:
	movq	88(%rsp), %rdi
	movq	24(%rsp), %rdx
	cmpq	%rbp, %rdi
	movl	%eax, (%rdx)
	je	.L19
	call	free@PLT
.L19:
	movq	%r13, %rdi
	call	__resolv_context_put@PLT
.L1:
	addq	$136, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movzwl	6(%r15), %r14d
	leaq	12(%r15), %r13
	cltq
	addq	%rax, %r15
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__dn_skipname@PLT
	rolw	$8, %r14w
	testl	%eax, %eax
	movzwl	%r14w, %r14d
	js	.L7
	cltq
	testl	%r14d, %r14d
	leaq	4(%r13,%rax), %r13
	leal	-1(%r14), %eax
	je	.L4
	addq	$1, %rax
	movq	%rbx, 72(%rsp)
	xorl	%r14d, %r14d
	movq	%rax, %rbx
	movq	%rbp, 64(%rsp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L8:
	cltq
	leaq	0(%r13,%rax), %rbp
	movq	%r15, %rax
	subq	%rbp, %rax
	cmpq	$9, %rax
	jle	.L26
	movzwl	0(%rbp), %eax
	leaq	2(%rbp), %rdi
	rolw	$8, %ax
	movzwl	%ax, %eax
	cmpq	%rax, %r12
	je	.L31
	cmpq	$5, %rax
	jne	.L26
	call	__ns_get16@PLT
	cmpl	$1, %eax
	jne	.L26
	leaq	8(%rbp), %rdi
	call	__ns_get16@PLT
	leaq	10(%rbp), %rcx
	movq	%r15, %rsi
	movzwl	%ax, %edx
	subq	%rcx, %rsi
	cmpq	%rdx, %rsi
	jl	.L26
	addq	$1, %r14
	leaq	(%rcx,%rdx), %r13
	cmpq	%rbx, %r14
	je	.L32
.L16:
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__dn_skipname@PLT
	testl	%eax, %eax
	jns	.L8
.L26:
	movq	__h_errno@gottpoff(%rip), %rax
	movq	(%rsp), %r13
	movl	$-1, %ebx
	movq	64(%rsp), %rbp
	movl	%fs:(%rax), %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L31:
	movl	40(%rsp), %r8d
	movq	32(%rsp), %rcx
	movq	%r13, %rdx
	movq	88(%rsp), %rdi
	movq	%r15, %rsi
	movq	(%rsp), %r13
	movq	64(%rsp), %rbp
	call	__dn_expand@PLT
	testl	%eax, %eax
	js	.L33
	movq	48(%rsp), %rax
	movq	32(%rsp), %rdx
	movl	$1, %ebx
	movq	%rdx, (%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	jmp	.L9
.L33:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$90, %fs:(%rax)
	jne	.L27
	movq	56(%rsp), %rax
	movl	$-2, %ebx
	movl	$34, (%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L9
.L29:
	movq	errno@gottpoff(%rip), %rax
	movq	56(%rsp), %rdx
	movl	$-1, %ebx
	movl	%fs:(%rax), %eax
	movl	%eax, (%rdx)
	movq	24(%rsp), %rax
	movl	$-1, (%rax)
	jmp	.L1
	.size	_nss_dns_getcanonname_r, .-_nss_dns_getcanonname_r
	.section	.rodata
	.align 2
	.type	qtypes, @object
	.size	qtypes, 4
qtypes:
	.value	1
	.value	28
