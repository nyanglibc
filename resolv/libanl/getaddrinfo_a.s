	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"getaddrinfo_a.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"status == 0 || status == EAGAIN"
	.text
	.p2align 4,,15
	.globl	getaddrinfo_a
	.type	getaddrinfo_a, @function
getaddrinfo_a:
	pushq	%rbp
	movslq	%edx, %rax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rax, %r12
	pushq	%rbx
	subq	$120, %rsp
	movq	%rax, -152(%rbp)
	leaq	22(,%rax,8), %rax
	movl	%edi, -136(%rbp)
	movl	$0, -120(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	cmpl	$1, %edi
	movq	%rsp, -144(%rbp)
	ja	.L51
	testq	%rcx, %rcx
	movq	%rsi, %r14
	movq	%rcx, %r13
	je	.L52
.L4:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	testl	%r12d, %r12d
	jle	.L26
	leal	-1(%r12), %eax
	movq	-144(%rbp), %r15
	movl	$0, -132(%rbp)
	leaq	8(%r14,%rax,8), %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L53:
	call	__gai_enqueue_request@PLT
	testq	%rax, %rax
	movq	%rax, (%r15)
	je	.L27
	movl	-120(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -120(%rbp)
.L7:
	addq	$8, %r14
	addq	$8, %r15
	cmpq	%rbx, %r14
	je	.L5
.L8:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L53
	addq	$8, %r14
	movq	$0, (%r15)
	addq	$8, %r15
	cmpq	%rbx, %r14
	jne	.L8
.L5:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	je	.L54
	movq	-152(%rbp), %rdi
	movl	-136(%rbp), %ecx
	salq	$5, %rdi
	testl	%ecx, %ecx
	jne	.L11
	addq	$16, %rdi
	movq	%rsp, %rbx
	movl	$0, -120(%rbp)
	subq	%rdi, %rsp
	testl	%r12d, %r12d
	movq	%rsp, %rax
	jle	.L12
	leal	-1(%r12), %edx
	movq	-144(%rbp), %rcx
	leaq	-120(%rbp), %r8
	salq	$5, %rdx
	leaq	32(%rax,%rdx), %rdi
	.p2align 4,,10
	.p2align 3
.L14:
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L13
	movq	24(%rdx), %rsi
	movq	%r8, 8(%rax)
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movq	%rsi, (%rax)
	movq	%rax, 24(%rdx)
	movl	-120(%rbp), %edx
	addl	$1, %edx
	movl	%edx, -120(%rbp)
.L13:
	addq	$32, %rax
	addq	$8, %rcx
	cmpq	%rdi, %rax
	jne	.L14
.L12:
	leaq	-116(%rbp), %rsi
	movl	$1, %edi
	leaq	-120(%rbp), %r12
	call	pthread_setcancelstate@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L20:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	jne	.L55
.L16:
	movl	-120(%rbp), %eax
	testl	%eax, %eax
	jne	.L20
	movl	-116(%rbp), %edi
	xorl	%esi, %esi
	call	pthread_setcancelstate@PLT
	movq	%rbx, %rsp
.L21:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L1:
	movl	-132(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	$-11, -132(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$72, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L29
	movl	12(%r13), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	je	.L56
.L22:
	testl	%r12d, %r12d
	movl	$0, -120(%rbp)
	jle	.L23
	leal	-1(%r12), %ecx
	movq	-144(%rbp), %rsi
	leaq	72(%rbx), %rdx
	leaq	8(%rbx), %r9
	salq	$5, %rcx
	leaq	104(%rbx,%rcx), %r8
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L24
	movq	24(%rcx), %rdi
	movq	%rbx, 8(%rdx)
	movq	%r9, 16(%rdx)
	movl	%eax, 24(%rdx)
	movq	%rdi, (%rdx)
	movq	%rdx, 24(%rcx)
	movl	-120(%rbp), %ecx
	addl	$1, %ecx
	movl	%ecx, -120(%rbp)
.L24:
	addq	$32, %rdx
	addq	$8, %rsi
	cmpq	%r8, %rdx
	jne	.L25
.L23:
	movdqu	0(%r13), %xmm0
	movl	-120(%rbp), %eax
	movups	%xmm0, 8(%rbx)
	movdqu	16(%r13), %xmm0
	movl	%eax, (%rbx)
	movups	%xmm0, 24(%rbx)
	movdqu	32(%r13), %xmm0
	movups	%xmm0, 40(%rbx)
	movdqu	48(%r13), %xmm0
	movups	%xmm0, 56(%rbx)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	__gai_requests_mutex(%rip), %rdi
	movl	%esi, -144(%rbp)
	call	pthread_mutex_unlock@PLT
	movl	-144(%rbp), %esi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L57:
	movl	-120(%rbp), %esi
	testl	%esi, %esi
	je	.L19
.L18:
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	call	__futex_abstimed_wait_cancelable64@PLT
	cmpl	$11, %eax
	je	.L57
	cmpl	$4, %eax
	je	.L19
	cmpl	$75, %eax
	je	.L19
	cmpl	$110, %eax
	jne	.L58
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	cmpl	$1, -136(%rbp)
	jne	.L1
	movl	12(%r13), %edi
	xorl	%esi, %esi
	testl	%edi, %edi
	jne	.L10
	call	getpid@PLT
	movl	%eax, %esi
.L10:
	movq	%r13, %rdi
	call	__gai_notify_only@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, -100(%rbp)
	leaq	-112(%rbp), %r13
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$-3, -132(%rbp)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$0, -132(%rbp)
	jmp	.L5
.L56:
	call	getpid@PLT
	jmp	.L22
.L51:
	movq	errno@gottpoff(%rip), %rax
	movl	$-11, -132(%rbp)
	movl	$22, %fs:(%rax)
	jmp	.L1
.L58:
	testl	%eax, %eax
	je	.L19
	leaq	__PRETTY_FUNCTION__.12860(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$128, %edx
	call	__assert_fail@PLT
	.size	getaddrinfo_a, .-getaddrinfo_a
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12860, @object
	.size	__PRETTY_FUNCTION__.12860, 14
__PRETTY_FUNCTION__.12860:
	.string	"getaddrinfo_a"
