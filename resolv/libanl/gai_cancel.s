	.text
	.p2align 4,,15
	.globl	gai_cancel
	.type	gai_cancel, @function
gai_cancel:
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	movq	%rbx, %rdi
	movl	$-101, %ebx
	call	__gai_remove_request@PLT
	cmpl	$0, %eax
	je	.L2
	setg	%bl
	movzbl	%bl, %ebx
	subl	$103, %ebx
.L2:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	gai_cancel, .-gai_cancel
