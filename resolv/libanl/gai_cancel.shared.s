	.text
	.p2align 4,,15
	.globl	gai_cancel
	.type	gai_cancel, @function
gai_cancel:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	__gai_requests_mutex@GOTPCREL(%rip), %rbp
	movq	%rbp, %rdi
	call	pthread_mutex_lock@PLT
	movq	%rbx, %rdi
	movl	$-101, %ebx
	call	__gai_remove_request
	cmpl	$0, %eax
	je	.L2
	setg	%bl
	movzbl	%bl, %ebx
	subl	$103, %ebx
.L2:
	movq	%rbp, %rdi
	call	pthread_mutex_unlock@PLT
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	gai_cancel, .-gai_cancel
	.hidden	__gai_remove_request
