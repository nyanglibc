	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gai_misc.c"
.LC1:
	.string	"runp->running == 1"
	.text
	.p2align 4,,15
	.type	handle_requests, @function
handle_requests:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	leaq	__gai_requests_mutex(%rip), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$80, %rsp
	testq	%rbx, %rbx
	movq	%rsp, %r12
	leaq	16(%rsp), %r13
	je	.L43
	.p2align 4,,10
	.p2align 3
.L2:
	movq	16(%rbx), %r14
	movq	16(%r14), %rdx
	movq	8(%r14), %rsi
	leaq	24(%r14), %rcx
	movq	(%r14), %rdi
	call	getaddrinfo@PLT
	movq	%rbp, %rdi
	movl	%eax, 32(%r14)
	call	pthread_mutex_lock@PLT
	movq	%rbx, %rdi
	call	__gai_notify@PLT
	movq	requests(%rip), %rax
	cmpq	%rax, %rbx
	je	.L23
	movq	%rax, %rcx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rdx, %rcx
.L5:
	movq	8(%rcx), %rdx
	cmpq	%rdx, %rbx
	jne	.L24
	cmpl	$1, (%rbx)
	jne	.L44
.L6:
	cmpq	%rbx, requests_tail(%rip)
	je	.L45
.L7:
	testq	%rcx, %rcx
	je	.L46
	movq	8(%rbx), %rdx
	movq	%rdx, 8(%rcx)
.L9:
	movq	freelist(%rip), %rdx
	testq	%rax, %rax
	movq	%rbx, freelist(%rip)
	movq	%rdx, 8(%rbx)
	jne	.L40
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L47:
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L10
.L40:
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L47
	movl	$1, (%rax)
	movq	%rax, %rbx
	movl	idle_thread_count(%rip), %eax
	testl	%eax, %eax
	jg	.L48
.L16:
	cmpl	$19, nthreads(%rip)
	jle	.L49
.L17:
	movq	%rbp, %rdi
	call	pthread_mutex_unlock@PLT
	testq	%rbx, %rbx
	jne	.L2
.L43:
	movq	%rbp, %rdi
	call	pthread_mutex_lock@PLT
	movq	requests(%rip), %rax
	testq	%rax, %rax
	jne	.L40
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%edi, %edi
	movq	%r12, %rsi
	addl	$1, idle_thread_count(%rip)
	call	__clock_gettime@PLT
	movq	(%rsp), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, 16(%rsp)
	movq	8(%rsp), %rax
	cmpq	$999999999, %rax
	jle	.L50
	subq	$1000000000, %rax
	addq	$2, %rdx
	movq	%rax, 24(%rsp)
	movq	%rdx, 16(%rsp)
.L20:
	leaq	__gai_new_request_notification(%rip), %rdi
	movq	%r13, %rdx
	movq	%rbp, %rsi
	call	pthread_cond_timedwait@PLT
	movq	requests(%rip), %rbx
	subl	$1, idle_thread_count(%rip)
	testq	%rbx, %rbx
	jne	.L41
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L51:
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L13
.L41:
	movl	(%rbx), %edx
	testl	%edx, %edx
	jne	.L51
	movl	idle_thread_count(%rip), %eax
	movl	$1, (%rbx)
	testl	%eax, %eax
	jle	.L16
.L48:
	leaq	__gai_new_request_notification(%rip), %rdi
	call	pthread_cond_signal@PLT
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	__gai_requests_mutex(%rip), %rdi
	subl	$1, nthreads(%rip)
	call	pthread_mutex_unlock@PLT
	xorl	%edi, %edi
	call	pthread_exit@PLT
	.p2align 4,,10
	.p2align 3
.L50:
	movq	%rax, 24(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L46:
	movq	8(%rax), %rax
	movq	%rax, requests(%rip)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rcx, requests_tail(%rip)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%ecx, %ecx
	cmpl	$1, (%rbx)
	je	.L6
.L44:
	leaq	__PRETTY_FUNCTION__.11970(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$338, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%r13, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%r13, %rdi
	call	pthread_attr_setdetachstate@PLT
	leaq	handle_requests(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	pthread_create@PLT
	testl	%eax, %eax
	jne	.L17
	addl	$1, nthreads(%rip)
	jmp	.L17
	.size	handle_requests, .-handle_requests
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_res, @function
free_res:
	movq	pool_max_size(%rip), %rax
	pushq	%r12
	movq	pool(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	testq	%rax, %rax
	je	.L53
	leaq	(%r12,%rax,8), %rbp
	movq	%r12, %rbx
	.p2align 4,,10
	.p2align 3
.L54:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	free@PLT
	cmpq	%rbp, %rbx
	jne	.L54
.L53:
	popq	%rbx
	movq	%r12, %rdi
	popq	%rbp
	popq	%r12
	jmp	free@PLT
	.size	free_res, .-free_res
	.text
	.p2align 4,,15
	.globl	__gai_find_request
	.type	__gai_find_request, @function
__gai_find_request:
	movq	requests(%rip), %rax
	testq	%rax, %rax
	je	.L60
	cmpq	16(%rax), %rdi
	jne	.L62
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	%rdi, 16(%rax)
	je	.L60
.L62:
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L63
.L60:
	rep ret
	.p2align 4,,10
	.p2align 3
.L67:
	rep ret
	.size	__gai_find_request, .-__gai_find_request
	.p2align 4,,15
	.globl	__gai_remove_request
	.type	__gai_remove_request, @function
__gai_remove_request:
	movq	requests(%rip), %rcx
	movl	$-1, %eax
	testq	%rcx, %rcx
	je	.L68
	cmpq	16(%rcx), %rdi
	jne	.L71
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L73:
	cmpq	%rdi, 16(%rdx)
	je	.L72
	movq	%rdx, %rcx
.L71:
	movq	8(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L73
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	movl	(%rdx), %esi
	movl	$1, %eax
	testl	%esi, %esi
	jne	.L68
	movq	8(%rdx), %rax
	movq	%rax, 8(%rcx)
.L74:
	xorl	%eax, %eax
	cmpq	%rdx, requests_tail(%rip)
	je	.L83
.L68:
	rep ret
	.p2align 4,,10
	.p2align 3
.L83:
	movq	%rcx, requests_tail(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movl	(%rcx), %edi
	movl	$1, %eax
	testl	%edi, %edi
	jne	.L68
	movq	8(%rcx), %rax
	movq	%rcx, %rdx
	xorl	%ecx, %ecx
	movq	%rax, requests(%rip)
	jmp	.L74
	.size	__gai_remove_request, .-__gai_remove_request
	.section	.rodata.str1.1
.LC2:
	.string	"../sysdeps/nptl/gai_misc.h"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"requests == newp || lastp->next == newp"
	.text
	.p2align 4,,15
	.globl	__gai_enqueue_request
	.type	__gai_enqueue_request, @function
__gai_enqueue_request:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	leaq	__gai_requests_mutex(%rip), %rdi
	subq	$336, %rsp
	call	pthread_mutex_lock@PLT
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	je	.L115
.L85:
	movq	requests_tail(%rip), %r12
	movq	8(%rbx), %rax
	movl	$0, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 8(%rbx)
	testq	%r12, %r12
	movq	%rax, freelist(%rip)
	je	.L116
	movq	%rbx, 8(%r12)
	movq	%rbx, requests_tail(%rip)
.L93:
	cmpl	$19, nthreads(%rip)
	movl	$-100, 32(%rbp)
	movl	idle_thread_count(%rip), %eax
	jle	.L117
.L94:
	testl	%eax, %eax
	jle	.L104
	leaq	__gai_new_request_notification(%rip), %rdi
	call	pthread_cond_signal@PLT
.L104:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
.L84:
	addq	$336, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	testl	%eax, %eax
	jne	.L94
	leaq	16(%rsp), %rbp
	movl	$1, (%rbx)
	leaq	80(%rsp), %r14
	leaq	208(%rsp), %r13
	movq	%rbp, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%rbp, %rdi
	call	pthread_attr_setdetachstate@PLT
	movq	%rbp, %rdi
	call	__pthread_get_minstack@PLT
	leaq	65536(%rax), %rsi
	movq	%rbp, %rdi
	call	pthread_attr_setstacksize@PLT
	movq	%r14, %rdi
	call	sigfillset@PLT
	movq	%r13, %rdx
	movq	%r14, %rsi
	movl	$2, %edi
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L118
	leaq	8(%rsp), %rdi
	leaq	handle_requests(%rip), %rdx
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	call	pthread_create@PLT
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	$2, %edi
	movl	%eax, %r14d
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L119
	movq	%rbp, %rdi
	call	pthread_attr_destroy@PLT
	testl	%r14d, %r14d
	jne	.L97
	addl	$1, nthreads(%rip)
.L98:
	movl	idle_thread_count(%rip), %eax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L115:
	movq	pool_size(%rip), %r13
	movq	pool_max_size(%rip), %rax
	leaq	1(%r13), %r14
	cmpq	%rax, %r14
	jnb	.L120
.L86:
	cmpq	$1, %r13
	movl	$32, %esi
	sbbq	%rdi, %rdi
	andl	$32, %edi
	addq	$32, %rdi
	cmpq	$1, %r13
	sbbl	%r12d, %r12d
	andl	$32, %r12d
	addl	$32, %r12d
	call	calloc@PLT
	testq	%rax, %rax
	je	.L87
	leal	-1(%r12), %esi
	movq	pool(%rip), %rdx
	movq	%r14, pool_size(%rip)
	movq	%rsi, %r12
	addq	$1, %rsi
	salq	$5, %rsi
	movq	%rax, (%rdx,%r13,8)
	movq	%rax, %rdx
	addq	%rax, %rsi
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%rcx, %rdx
.L89:
	leaq	32(%rdx), %rcx
	movq	%rbx, 8(%rdx)
	movq	%rdx, %rbx
	cmpq	%rcx, %rsi
	jne	.L106
	movslq	%r12d, %rbx
	salq	$5, %rbx
	addq	%rax, %rbx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%rbx, requests_tail(%rip)
	movq	%rbx, requests(%rip)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	8(%rax), %r12
	movq	pool(%rip), %rdi
	leaq	0(,%r12,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L87
	movq	%r12, pool_max_size(%rip)
	movq	%rax, pool(%rip)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L97:
	movl	nthreads(%rip), %eax
	testl	%eax, %eax
	jne	.L99
	cmpq	%rbx, requests(%rip)
	je	.L100
	cmpq	%rbx, 8(%r12)
	jne	.L121
.L101:
	movq	$0, 8(%r12)
.L103:
	movq	freelist(%rip), %rax
	movq	%r12, requests_tail(%rip)
	movq	%rbx, freelist(%rip)
	movq	%rax, 8(%rbx)
	xorl	%ebx, %ebx
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$0, (%rbx)
	jmp	.L98
.L87:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	movq	errno@gottpoff(%rip), %rax
	movl	$11, %fs:(%rax)
	jmp	.L84
.L100:
	testq	%r12, %r12
	jne	.L101
	movq	$0, requests(%rip)
	jmp	.L103
.L121:
	leaq	__PRETTY_FUNCTION__.11959(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$264, %edx
	call	__assert_fail@PLT
.L119:
	leaq	__PRETTY_FUNCTION__.8790(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$117, %edx
	movl	%eax, %edi
	call	__assert_perror_fail@PLT
.L118:
	leaq	__PRETTY_FUNCTION__.8790(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	movl	$111, %edx
	movl	%eax, %edi
	call	__assert_perror_fail@PLT
	.size	__gai_enqueue_request, .-__gai_enqueue_request
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.8790, @object
	.size	__PRETTY_FUNCTION__.8790, 27
__PRETTY_FUNCTION__.8790:
	.string	"__gai_create_helper_thread"
	.align 16
	.type	__PRETTY_FUNCTION__.11970, @object
	.size	__PRETTY_FUNCTION__.11970, 16
__PRETTY_FUNCTION__.11970:
	.string	"handle_requests"
	.align 16
	.type	__PRETTY_FUNCTION__.11959, @object
	.size	__PRETTY_FUNCTION__.11959, 22
__PRETTY_FUNCTION__.11959:
	.string	"__gai_enqueue_request"
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_res__, @object
	.size	__elf_set___libc_subfreeres_element_free_res__, 8
__elf_set___libc_subfreeres_element_free_res__:
	.quad	free_res
	.globl	__gai_new_request_notification
	.bss
	.align 32
	.type	__gai_new_request_notification, @object
	.size	__gai_new_request_notification, 48
__gai_new_request_notification:
	.zero	48
	.globl	__gai_requests_mutex
	.data
	.align 32
	.type	__gai_requests_mutex, @object
	.size	__gai_requests_mutex, 40
__gai_requests_mutex:
	.long	0
	.long	0
	.long	0
	.long	0
	.long	1
	.value	0
	.value	0
	.quad	0
	.quad	0
	.local	idle_thread_count
	.comm	idle_thread_count,4,4
	.local	nthreads
	.comm	nthreads,4,4
	.local	requests_tail
	.comm	requests_tail,8,8
	.local	requests
	.comm	requests,8,8
	.local	freelist
	.comm	freelist,8,8
	.local	pool_size
	.comm	pool_size,8,8
	.local	pool_max_size
	.comm	pool_max_size,8,8
	.local	pool
	.comm	pool,8,8
