	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"gai_suspend.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"status == 0 || status == EAGAIN"
	.text
	.p2align 4,,15
	.globl	__gai_suspend
	.type	__gai_suspend, @function
__gai_suspend:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	movslq	%esi, %rdx
	pushq	%r13
	movq	%rdx, %rax
	pushq	%r12
	pushq	%rbx
	salq	$5, %rax
	movq	%rdi, %r12
	leaq	__gai_requests_mutex(%rip), %rdi
	subq	$88, %rsp
	addq	$16, %rax
	movl	%esi, %ebx
	subq	%rax, %rsp
	leaq	22(,%rdx,8), %rax
	movl	$1, -72(%rbp)
	movq	%rsp, %r15
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %r13
	movq	%r13, -96(%rbp)
	call	pthread_mutex_lock@PLT
	testl	%ebx, %ebx
	jle	.L2
	leal	-1(%rbx), %eax
	movq	%r13, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movl	$1, %r13d
	leaq	8(%r12,%rax,8), %r8
	leaq	-72(%rbp), %rax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L4:
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L3
	cmpl	$-100, 32(%rdi)
	je	.L62
.L3:
	addq	$8, %rdx
	addq	$8, %rsi
	addq	$32, %rcx
	cmpq	%r8, %rdx
	jne	.L4
	testq	%r14, %r14
	jne	.L22
.L5:
	testl	%r13d, %r13d
	jne	.L23
	leaq	-68(%rbp), %rsi
	movl	$1, %edi
	call	pthread_setcancelstate@PLT
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	jne	.L9
	testl	%ebx, %ebx
	movl	-68(%rbp), %edi
	jle	.L63
.L10:
	xorl	%eax, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$1, %rax
	cmpl	%eax, %ebx
	jle	.L16
.L21:
	movq	(%r12,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.L17
	cmpl	$-100, 32(%rdx)
	jne	.L17
	movq	-96(%rbp), %rcx
	movq	(%rcx,%rax,8), %rdx
	testq	%rdx, %rdx
	je	.L17
	movq	24(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L17
	movq	%rax, %r8
	salq	$5, %r8
	leaq	(%r15,%r8), %rsi
	cmpq	%rsi, %rcx
	jne	.L19
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rdx, %rcx
.L19:
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	je	.L17
	cmpq	%rsi, %rdx
	jne	.L28
.L20:
	movq	(%r15,%r8), %rdx
	addq	$1, %rax
	cmpl	%eax, %ebx
	movq	%rdx, (%rcx)
	jg	.L21
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%esi, %esi
	call	pthread_setcancelstate@PLT
	testl	%r13d, %r13d
	je	.L8
	cmpl	$4, %r13d
	movl	$-11, %eax
	movl	$-104, %r13d
	cmovne	%eax, %r13d
.L8:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_unlock@PLT
	leaq	-40(%rbp), %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%r14, %r14
	movl	$1, %r13d
	je	.L23
.L22:
	leaq	-64(%rbp), %rsi
	movl	$1, %edi
	call	__clock_gettime@PLT
	movq	8(%r14), %rax
	addq	-56(%rbp), %rax
	movq	(%r14), %rdx
	addq	-64(%rbp), %rdx
	cmpq	$999999999, %rax
	movq	%rdx, -64(%rbp)
	jle	.L65
	subq	$1000000000, %rax
	addq	$1, %rdx
	movq	%rax, -56(%rbp)
	movq	%rdx, -64(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L62:
	movq	%r8, -120(%rbp)
	movq	%rdx, -112(%rbp)
	movq	%rsi, -104(%rbp)
	movq	%rcx, -88(%rbp)
	call	__gai_find_request@PLT
	movq	-104(%rbp), %rsi
	testq	%rax, %rax
	movq	-88(%rbp), %rcx
	movq	-112(%rbp), %rdx
	movq	-120(%rbp), %r8
	movq	%rax, (%rsi)
	je	.L3
	movq	24(%rax), %rdi
	xorl	%r13d, %r13d
	movq	$0, 16(%rcx)
	movl	$0, 24(%rcx)
	movq	%rdi, (%rcx)
	movq	-128(%rbp), %rdi
	movq	%rdi, 8(%rcx)
	movq	%rcx, 24(%rax)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rax, -56(%rbp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	__gai_requests_mutex(%rip), %rdi
	movl	%esi, -104(%rbp)
	call	pthread_mutex_unlock@PLT
	leaq	-64(%rbp), %rax
	testq	%r14, %r14
	movl	$0, %r14d
	movl	-104(%rbp), %esi
	cmovne	%rax, %r14
	leaq	-72(%rbp), %rax
	movq	%rax, -88(%rbp)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L66:
	movl	-72(%rbp), %esi
	testl	%esi, %esi
	je	.L14
.L13:
	movq	-88(%rbp), %rdi
	xorl	%r8d, %r8d
	movq	%r14, %rcx
	movl	$1, %edx
	call	__futex_abstimed_wait_cancelable64@PLT
	cmpl	$11, %eax
	je	.L66
	cmpl	$4, %eax
	je	.L24
	cmpl	$110, %eax
	movl	$11, %r13d
	jne	.L67
.L14:
	leaq	__gai_requests_mutex(%rip), %rdi
	call	pthread_mutex_lock@PLT
	testl	%ebx, %ebx
	movl	-68(%rbp), %edi
	jg	.L10
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%esi, %esi
	call	pthread_setcancelstate@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$4, %r13d
	jmp	.L14
.L64:
	leaq	24(%rdx), %rcx
	jmp	.L20
.L67:
	cmpl	$75, %eax
	jne	.L68
	movl	$75, %r13d
	jmp	.L14
.L23:
	movl	$-103, %r13d
	jmp	.L8
.L68:
	testl	%eax, %eax
	je	.L27
	leaq	__PRETTY_FUNCTION__.11906(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$98, %edx
	call	__assert_fail@PLT
.L27:
	xorl	%r13d, %r13d
	jmp	.L14
	.size	__gai_suspend, .-__gai_suspend
	.weak	gai_suspend
	.set	gai_suspend,__gai_suspend
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11906, @object
	.size	__PRETTY_FUNCTION__.11906, 14
__PRETTY_FUNCTION__.11906:
	.string	"__gai_suspend"
