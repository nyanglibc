	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../sysdeps/nptl/gai_misc.h"
	.text
	.p2align 4,,15
	.type	notify_func_wrapper, @function
notify_func_wrapper:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rdi
	call	sigemptyset@PLT
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movl	$2, %edi
	call	pthread_sigmask@PLT
	testl	%eax, %eax
	jne	.L5
	movq	8(%rbx), %r12
	movq	%rbx, %rdi
	movq	(%rbx), %rbp
	call	free@PLT
	movq	%r12, %rdi
	call	*%rbp
	subq	$-128, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L5:
	leaq	__PRETTY_FUNCTION__.11837(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	movl	$86, %edx
	movl	%eax, %edi
	call	__assert_perror_fail@PLT
	.size	notify_func_wrapper, .-notify_func_wrapper
	.p2align 4,,15
	.globl	__gai_notify_only
	.type	__gai_notify_only, @function
__gai_notify_only:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$80, %rsp
	movl	12(%rdi), %ecx
	cmpl	$2, %ecx
	je	.L16
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L17
.L6:
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%esi, %edx
	movl	8(%rdi), %edi
	movq	(%rbx), %rsi
	call	__gai_sigqueue@PLT
	addq	$80, %rsp
	sarl	$31, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	24(%rdi), %r12
	testq	%r12, %r12
	je	.L18
.L8:
	movl	$16, %edi
	call	malloc@PLT
	movq	%rax, %rbp
	movl	$-1, %eax
	testq	%rbp, %rbp
	je	.L6
	movq	16(%rbx), %rax
	leaq	notify_func_wrapper(%rip), %rdx
	leaq	8(%rsp), %rdi
	movq	%rbp, %rcx
	movq	%r12, %rsi
	movq	%rax, 0(%rbp)
	movq	(%rbx), %rax
	movq	%rax, 8(%rbp)
	call	pthread_create@PLT
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	jns	.L6
	movq	%rbp, %rdi
	call	free@PLT
	movl	$-1, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	16(%rsp), %r12
	movq	%r12, %rdi
	call	pthread_attr_init@PLT
	movl	$1, %esi
	movq	%r12, %rdi
	call	pthread_attr_setdetachstate@PLT
	jmp	.L8
	.size	__gai_notify_only, .-__gai_notify_only
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__gai_notify
	.type	__gai_notify, @function
__gai_notify:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	24(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L19
	movl	$202, %ebp
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L21:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, (%rcx)
	je	.L32
.L23:
	testq	%r12, %r12
	movq	%r12, %rbx
	je	.L19
.L26:
	movq	16(%rbx), %rdi
	movq	8(%rbx), %rcx
	movq	(%rbx), %r12
	testq	%rdi, %rdi
	movl	(%rcx), %eax
	jne	.L21
	testl	%eax, %eax
	je	.L23
	subl	$1, (%rcx)
	jne	.L23
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rcx, %rdi
	movl	%ebp, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L23
	cmpl	$-22, %eax
	je	.L23
	cmpl	$-14, %eax
	je	.L23
	leaq	.LC1(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	movl	24(%rbx), %esi
	call	__gai_notify_only
	movq	8(%rbx), %rdi
	movq	%r12, %rbx
	call	free@PLT
	testq	%r12, %r12
	jne	.L26
.L19:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__gai_notify, .-__gai_notify
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11837, @object
	.size	__PRETTY_FUNCTION__.11837, 26
__PRETTY_FUNCTION__.11837:
	.string	"__gai_start_notify_thread"
