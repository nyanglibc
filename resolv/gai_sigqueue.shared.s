	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gai_sigqueue
	.type	__gai_sigqueue, @function
__gai_sigqueue:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movl	%edi, %ebx
	movl	$15, %ecx
	movq	%rsi, %r12
	addq	$-128, %rsp
	movq	%rsp, %rbp
	movq	$0, 4(%rsp)
	movq	$0, 120(%rsp)
	leaq	8(%rbp), %rdi
	rep stosq
	movl	%edx, 16(%rsp)
	movl	%ebx, (%rsp)
	movl	$-60, 8(%rsp)
	call	__getuid
	movq	%r12, 24(%rsp)
	movl	%eax, 20(%rsp)
	movq	%rbp, %rdx
	movl	%ebx, %esi
	movl	16(%rsp), %edi
	movl	$129, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/gai_sigqueue.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L6
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	subq	$-128, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__gai_sigqueue, .-__gai_sigqueue
	.hidden	__getuid
