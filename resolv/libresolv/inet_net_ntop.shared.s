	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%u"
.LC1:
	.string	"/%u"
	.text
	.p2align 4,,15
	.globl	inet_net_ntop
	.type	inet_net_ntop, @function
inet_net_ntop:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	$2, %edi
	movq	%rcx, (%rsp)
	jne	.L16
	cmpl	$32, %edx
	movl	%edx, %r15d
	ja	.L19
	testl	%edx, %edx
	movq	%r8, %r12
	jne	.L6
	cmpq	$1, %r8
	jbe	.L7
	movq	(%rsp), %rax
	movl	$48, %edx
	subq	$1, %r12
	leaq	1(%rax), %rdi
	movw	%dx, (%rax)
.L8:
	cmpq	$3, %r12
	jbe	.L7
	leaq	.LC1(%rip), %rsi
	movl	%r15d, %edx
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, %eax
	movq	%rsi, %rbp
	sarl	$3, %eax
	testl	%eax, %eax
	je	.L13
	cmpq	$4, %r8
	jbe	.L7
	subl	$1, %eax
	movq	(%rsp), %rbx
	leaq	.LC0(%rip), %r13
	leaq	1(%rsi,%rax), %r14
	.p2align 4,,10
	.p2align 3
.L11:
	addq	$1, %rbp
	movzbl	-1(%rbp), %edx
	xorl	%eax, %eax
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	sprintf@PLT
	cmpq	%rbp, %r14
	cltq
	movq	%rbx, %rdx
	leaq	(%rbx,%rax), %r9
	je	.L10
	leaq	1(%r9), %rbx
	movl	$46, %eax
	movw	%ax, (%r9)
	movq	%rbx, %rax
	subq	%rdx, %rax
	subq	%rax, %r12
	cmpq	$4, %r12
	ja	.L11
.L7:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	errno@gottpoff(%rip), %rax
	movl	$97, %fs:(%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	subq	%rax, %r12
.L9:
	movl	%r15d, %ecx
	andl	$7, %ecx
	je	.L14
	cmpq	$4, %r12
	jbe	.L7
	movq	(%rsp), %rax
	cmpq	%r9, %rax
	movq	%rax, %rbx
	je	.L12
	leaq	1(%r9), %rbx
	movb	$46, (%r9)
.L12:
	movl	$8, %eax
	movl	$1, %edx
	leaq	.LC0(%rip), %rsi
	subl	%ecx, %eax
	sall	%cl, %edx
	movq	%rbx, %rdi
	movl	%eax, %ecx
	movzbl	0(%rbp), %eax
	subl	$1, %edx
	sall	%cl, %edx
	movq	%r9, 8(%rsp)
	andl	%eax, %edx
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	8(%rsp), %r9
	movslq	%eax, %rdi
	addq	%rbx, %rdi
	movq	%rdi, %rax
	subq	%r9, %rax
	subq	%rax, %r12
	jmp	.L8
.L14:
	movq	%r9, %rdi
	jmp	.L8
.L13:
	movq	(%rsp), %r9
	jmp	.L9
	.size	inet_net_ntop, .-inet_net_ntop
