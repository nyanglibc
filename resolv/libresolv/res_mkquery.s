	.text
	.p2align 4,,15
	.globl	__res_context_mkquery
	.type	__res_context_mkquery, @function
__res_context_mkquery:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	cmpl	$65535, %ecx
	movq	256(%rsp), %r14
	ja	.L17
	cmpl	$65535, %r8d
	ja	.L17
	testq	%r14, %r14
	je	.L17
	cmpl	$11, 264(%rsp)
	jle	.L17
	leaq	32(%rsp), %r15
	movl	%esi, %r13d
	movq	%rdi, %rbx
	movq	$0, (%r14)
	movl	$0, 8(%r14)
	movl	$1, %edi
	movq	%r15, %rsi
	movl	%ecx, %ebp
	movq	%r9, 8(%rsp)
	movl	%r8d, %r12d
	movq	%rdx, 16(%rsp)
	call	__clock_gettime@PLT
	movq	40(%rsp), %rax
	xorl	32(%rsp), %eax
	movl	%eax, %ecx
	rorl	$8, %ecx
	xorl	%ecx, %eax
	movw	%ax, (%r14)
	movl	%r13d, %eax
	andl	$15, %eax
	leal	0(,%rax,8), %ecx
	movzbl	2(%r14), %eax
	andl	$-121, %eax
	orl	%ecx, %eax
	movb	%al, 2(%r14)
	movq	(%rbx), %rax
	movq	8(%rax), %rax
	testl	$67108864, %eax
	jne	.L20
.L6:
	movzbl	2(%r14), %edx
	shrq	$6, %rax
	andb	$-16, 3(%r14)
	andl	$1, %eax
	movq	%r14, 32(%rsp)
	movq	$0, 40(%rsp)
	andl	$-2, %edx
	orl	%edx, %eax
	testl	%r13d, %r13d
	movb	%al, 2(%r14)
	je	.L7
	cmpl	$4, %r13d
	jne	.L17
	cmpq	$1, 8(%rsp)
	movl	264(%rsp), %ebx
	sbbl	%eax, %eax
	andl	$-10, %eax
	subl	%eax, %ebx
	subl	$26, %ebx
	movl	%ebx, 28(%rsp)
	js	.L17
.L10:
	movslq	28(%rsp), %rdx
	leaq	12(%r14), %rbx
	movq	16(%rsp), %rdi
	leaq	192(%rsp), %r8
	movq	%r15, %rcx
	movq	%rbx, %rsi
	call	ns_name_compress@PLT
	testl	%eax, %eax
	js	.L17
	movslq	%eax, %rsi
	rolw	$8, %r12w
	rolw	$8, %bp
	addq	%rbx, %rsi
	movl	$256, %edi
	testl	%r13d, %r13d
	movw	%r12w, (%rsi)
	movw	%bp, 2(%rsi)
	leaq	4(%rsi), %r12
	movw	%di, 4(%r14)
	je	.L11
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.L21
.L11:
	movl	%r12d, %eax
	subl	%r14d, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	orb	$32, 3(%r14)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movl	264(%rsp), %eax
	subl	$16, %eax
	movl	%eax, 28(%rsp)
	jns	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$-1, %eax
.L1:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	28(%rsp), %ebx
	leaq	192(%rsp), %r8
	movq	%r15, %rcx
	movq	%r12, %rsi
	subl	%eax, %ebx
	movslq	%ebx, %rdx
	call	ns_name_compress@PLT
	testl	%eax, %eax
	js	.L17
	cltq
	movl	$2560, %edx
	xorl	%ecx, %ecx
	addq	%r12, %rax
	movl	$256, %esi
	movw	%dx, (%rax)
	movw	%bp, 2(%rax)
	leaq	10(%rax), %r12
	movl	$0, 4(%rax)
	movw	%cx, 8(%rax)
	movw	%si, 10(%r14)
	jmp	.L11
	.size	__res_context_mkquery, .-__res_context_mkquery
	.p2align 4,,15
	.globl	__res_nmkquery
	.type	__res_nmkquery, @function
__res_nmkquery:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movl	%ecx, %r14d
	subq	$24, %rsp
	movq	%r9, 8(%rsp)
	movq	96(%rsp), %r12
	call	__resolv_context_get_override@PLT
	testq	%rax, %rax
	je	.L25
	movq	%rax, %rbx
	movl	104(%rsp), %eax
	movq	%r13, %rdx
	movl	%ebp, %esi
	movl	%r15d, %r8d
	movl	%r14d, %ecx
	movq	%rbx, %rdi
	pushq	%rax
	pushq	%r12
	movq	24(%rsp), %r9
	call	__res_context_mkquery
	movl	%eax, %ebp
	cmpl	$1, %ebp
	popq	%rax
	popq	%rdx
	jle	.L24
	movzwl	(%r12), %edx
	movq	(%rbx), %rax
	movw	%dx, 68(%rax)
.L24:
	movq	%rbx, %rdi
	call	__resolv_context_put@PLT
.L22:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$-1, %ebp
	jmp	.L22
	.size	__res_nmkquery, .-__res_nmkquery
	.p2align 4,,15
	.globl	__res_mkquery
	.type	__res_mkquery, @function
__res_mkquery:
	pushq	%r15
	pushq	%r14
	movl	%ecx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movl	%edx, %r14d
	movq	%r8, %r12
	subq	$8, %rsp
	call	__resolv_context_get_preinit@PLT
	testq	%rax, %rax
	je	.L30
	movq	%rax, %rbx
	movl	80(%rsp), %eax
	movq	%r13, %rdx
	movl	%ebp, %esi
	movq	%r12, %r9
	movl	%r15d, %r8d
	movl	%r14d, %ecx
	movq	%rbx, %rdi
	pushq	%rax
	pushq	80(%rsp)
	call	__res_context_mkquery
	movl	%eax, %ebp
	cmpl	$1, %ebp
	popq	%rax
	popq	%rdx
	jle	.L29
	movq	72(%rsp), %rcx
	movq	(%rbx), %rax
	movzwl	(%rcx), %edx
	movw	%dx, 68(%rax)
.L29:
	movq	%rbx, %rdi
	call	__resolv_context_put@PLT
.L27:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %ebp
	jmp	.L27
	.size	__res_mkquery, .-__res_mkquery
	.p2align 4,,15
	.globl	__res_nopt
	.type	__res_nopt, @function
__res_nopt:
	movslq	%esi, %rsi
	movslq	%ecx, %rcx
	subq	%rsi, %rcx
	leaq	(%rdx,%rsi), %rax
	cmpq	$10, %rcx
	jle	.L36
	movl	$10496, %r9d
	cmpl	$511, %r8d
	movb	$0, (%rax)
	movw	%r9w, 1(%rax)
	movl	$2, %ecx
	jg	.L40
.L34:
	movw	%cx, 3(%rax)
	xorl	%ecx, %ecx
	movw	%cx, 5(%rax)
	movq	(%rdi), %rcx
	testb	$-128, 10(%rcx)
	setne	%cl
	xorl	%esi, %esi
	addq	$11, %rax
	movzbl	%cl, %ecx
	movw	%si, -2(%rax)
	sall	$7, %ecx
	movw	%cx, -4(%rax)
	movzwl	10(%rdx), %ecx
	subl	%edx, %eax
	rolw	$8, %cx
	addl	$1, %ecx
	rolw	$8, %cx
	movw	%cx, 10(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movl	%r8d, %ecx
	movl	$-20476, %esi
	rolw	$8, %cx
	cmpl	$1201, %r8d
	cmovge	%esi, %ecx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$-1, %eax
	ret
	.size	__res_nopt, .-__res_nopt
