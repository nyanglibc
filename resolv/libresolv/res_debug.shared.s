	.text
#APP
	.symver __p_secstodate,__p_secstodate@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%ld.%.2ld"
#NO_APP
	.text
	.p2align 4,,15
	.type	precsize_ntoa, @function
precsize_ntoa:
	movl	%edi, %ecx
	movl	$-858993459, %esi
	shrb	$4, %dil
	andl	$15, %ecx
	subq	$8, %rsp
	movl	%ecx, %eax
	mull	%esi
	shrl	$3, %edx
	leal	0(,%rdx,4), %eax
	addl	%eax, %edx
	addl	%edx, %edx
	subl	%edx, %ecx
	movslq	%ecx, %r8
	movzbl	%dil, %ecx
	leaq	retbuf.13600(%rip), %rdi
	movl	%ecx, %eax
	mull	%esi
	leaq	poweroften(%rip), %rax
	shrl	$3, %edx
	leal	0(,%rdx,4), %esi
	addl	%esi, %edx
	movabsq	$2951479051793528259, %rsi
	addl	%edx, %edx
	subl	%edx, %ecx
	imull	(%rax,%r8,4), %ecx
	movq	%rcx, %rdx
	shrq	$2, %rdx
	movq	%rdx, %rax
	mulq	%rsi
	leaq	.LC0(%rip), %rsi
	shrq	$2, %rdx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	subq	%rax, %rcx
	xorl	%eax, %eax
	call	sprintf@PLT
	leaq	retbuf.13600(%rip), %rax
	addq	$8, %rsp
	ret
	.size	precsize_ntoa, .-precsize_ntoa
	.p2align 4,,15
	.type	latlon2ul, @function
latlon2ul:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rbp
	call	__ctype_b_loc@PLT
	xorl	%esi, %esi
	movq	(%rax), %r8
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	leal	(%rsi,%rsi,4), %edx
	movq	%rax, %rbp
	leal	-48(%rcx,%rdx,2), %esi
.L5:
	movsbl	0(%rbp), %ecx
	movsbq	%cl, %rax
	movzwl	(%r8,%rax,2), %edi
	leaq	1(%rbp), %rax
	movq	%rax, %rdx
	testw	$2048, %di
	jne	.L6
	testw	$8192, %di
	jne	.L8
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rax, %rbp
	addq	$1, %rax
.L8:
	movsbq	(%rax), %rdx
	movq	%rax, %r9
	movq	%rdx, %rcx
	movzwl	(%r8,%rdx,2), %edx
	testb	$32, %dh
	jne	.L32
	xorl	%r10d, %r10d
	andb	$8, %dh
	jne	.L9
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L34:
	movq	%rax, %r9
.L9:
	leal	(%r10,%r10,4), %edx
	leaq	1(%r9), %rax
	leal	-48(%rcx,%rdx,2), %r10d
	movsbl	1(%r9), %ecx
	movsbq	%cl, %r11
	movzwl	(%r8,%r11,2), %edi
	testw	$2048, %di
	jne	.L34
	testw	$8192, %di
	leaq	2(%r9), %rdx
	jne	.L11
	movq	%rax, %r9
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%r9, %rax
.L11:
	movsbq	1(%rax), %r11
	leaq	1(%rax), %r9
	movzwl	(%r8,%r11,2), %edi
	movq	%r11, %rcx
	testw	$8192, %di
	jne	.L36
	leaq	2(%rax), %rdx
.L10:
	testw	$2048, %di
	je	.L37
	testb	$8, 1(%r8,%r11,2)
	je	.L38
	xorl	%edi, %edi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rax, %r9
.L13:
	leal	(%rdi,%rdi,4), %edx
	leaq	1(%r9), %rax
	leal	-48(%rcx,%rdx,2), %edi
	movsbl	1(%r9), %ecx
	movsbq	%cl, %rdx
	testb	$8, 1(%r8,%rdx,2)
	jne	.L39
	leaq	2(%r9), %rdx
.L12:
	movsbq	1(%rax), %r11
	cmpb	$46, %cl
	movq	%r11, %r9
	movzwl	(%r8,%r11,2), %r11d
	je	.L62
	movsbq	(%rax), %rdx
	xorl	%ebp, %ebp
	movzwl	(%r8,%rdx,2), %ecx
.L15:
	andb	$32, %ch
	movl	%r9d, %ecx
	jne	.L30
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %rax
	movsbq	1(%rax), %r9
	movl	%r11d, %edx
	andw	$8192, %dx
	testw	%dx, %dx
	movq	%r9, %rcx
	movzwl	(%r8,%r9,2), %r11d
	je	.L18
	testw	$8192, %r11w
	leaq	1(%rax), %r9
	je	.L63
	.p2align 4,,10
	.p2align 3
.L20:
	movsbq	1(%r9), %rax
	movq	%rax, %rcx
	movzwl	(%r8,%rax,2), %r11d
	movq	%r9, %rax
.L30:
	testw	$8192, %r11w
	leaq	1(%rax), %r9
	jne	.L20
.L63:
	subl	$69, %ecx
	leaq	2(%rax), %rdx
	cmpb	$50, %cl
	jbe	.L64
.L41:
	xorl	%eax, %eax
.L21:
	movl	$0, (%rbx)
.L24:
	movsbq	1(%r9), %rcx
	testb	$32, 1(%r8,%rcx,2)
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L26:
	addq	$1, %rdx
	movsbq	(%rdx), %rcx
	testb	$32, 1(%r8,%rcx,2)
	je	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$1, %rdx
	movsbq	(%rdx), %rcx
	testb	$32, 1(%r8,%rcx,2)
	jne	.L27
	movq	%rdx, (%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	2(%rbp), %rdx
	xorl	%edi, %edi
	xorl	%ebp, %ebp
.L7:
	subl	$69, %ecx
	cmpb	$50, %cl
	ja	.L41
.L64:
	movl	$1, %r11d
	movabsq	$1196268651298816, %rax
	salq	%cl, %r11
	testq	%rax, %r11
	jne	.L22
	movabsq	$2203318223361, %rcx
	xorl	%eax, %eax
	testq	%rcx, %r11
	jne	.L65
.L23:
	movabsq	$1125904202072065, %rcx
	testq	%rcx, %r11
	jne	.L28
	movabsq	$72567767450112, %rcx
	testq	%rcx, %r11
	je	.L21
	movl	$1, (%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$2, (%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L22:
	imull	$60, %esi, %ecx
	movl	$-2147483648, %eax
	subl	%ebp, %eax
	addl	%r10d, %ecx
	imull	$60, %ecx, %ecx
	addl	%edi, %ecx
	imull	$1000, %ecx, %ecx
	subl	%ecx, %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%ebp, %ebp
	xorl	%edi, %edi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L65:
	imull	$60, %esi, %ecx
	addl	%r10d, %ecx
	imull	$60, %ecx, %ecx
	addl	%edi, %ecx
	imull	$1000, %ecx, %ecx
	leal	-2147483648(%rbp,%rcx), %eax
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L62:
	movsbq	2(%rax), %rcx
	testw	$2048, %r11w
	movq	%rcx, %r13
	movzwl	(%r8,%rcx,2), %ecx
	je	.L40
	movsbq	3(%rax), %rdx
	movsbl	%r9b, %ebp
	subl	$48, %ebp
	imull	$100, %ebp, %ebp
	testb	$8, %ch
	movq	%rdx, %r9
	movzwl	(%r8,%rdx,2), %r11d
	jne	.L16
	addq	$2, %rax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%rbp, %r9
	xorl	%edi, %edi
	xorl	%ebp, %ebp
	xorl	%r10d, %r10d
	jmp	.L7
.L40:
	movl	%ecx, %eax
	movl	%r13d, %r9d
	movl	%r11d, %ecx
	xorl	%ebp, %ebp
	movl	%eax, %r11d
	movq	%rdx, %rax
	jmp	.L15
.L38:
	movq	%r9, %rax
	xorl	%edi, %edi
	jmp	.L12
.L16:
	movsbq	4(%rax), %rcx
	leal	-240(%r13,%r13,4), %edx
	testw	$2048, %r11w
	leal	0(%rbp,%rdx,2), %ebp
	movq	%rcx, %rdx
	movzwl	(%r8,%rcx,2), %ecx
	jne	.L17
	movl	%edx, %r9d
	movl	%ecx, %edx
	addq	$3, %rax
	movl	%r11d, %ecx
	movl	%edx, %r11d
	jmp	.L15
.L17:
	movsbq	5(%rax), %rdx
	leal	-48(%rbp,%r9), %ebp
	addq	$4, %rax
	movq	%rdx, %r9
	movzwl	(%r8,%rdx,2), %r11d
	jmp	.L15
	.size	latlon2ul, .-latlon2ul
	.p2align 4,,15
	.type	precsize_aton, @function
precsize_aton:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	(%rdi), %rbx
	call	__ctype_b_loc@PLT
	xorl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L68:
	leal	(%rcx,%rcx,4), %eax
	addq	$1, %rbx
	leal	-48(%rsi,%rax,2), %ecx
.L67:
	movsbl	(%rbx), %esi
	movsbq	%sil, %rax
	testb	$8, 1(%rdi,%rax,2)
	jne	.L68
	xorl	%edx, %edx
	cmpb	$46, %sil
	je	.L78
.L69:
	imull	$100, %ecx, %eax
	leaq	4+poweroften(%rip), %rdi
	movl	$10, %ecx
	addl	%edx, %eax
	xorl	%edx, %edx
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L79:
	movl	(%rdi,%rdx,4), %ecx
.L74:
	cmpl	%ecx, %eax
	movslq	%edx, %rsi
	jb	.L72
	addq	$1, %rdx
	cmpq	$9, %rdx
	jne	.L79
	movl	$9, %esi
	movl	$1000000000, %edi
.L73:
	xorl	%edx, %edx
	movq	%rbx, 0(%rbp)
	divl	%edi
	movl	$9, %edx
	cmpl	$9, %eax
	cmovg	%edx, %eax
	addq	$8, %rsp
	sall	$4, %eax
	orl	%esi, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	poweroften(%rip), %rcx
	movl	(%rcx,%rsi,4), %edi
	movl	%edx, %esi
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L78:
	movsbq	1(%rbx), %rsi
	testb	$8, 1(%rdi,%rsi,2)
	je	.L80
	leal	-240(%rsi,%rsi,4), %eax
	movsbq	2(%rbx), %rsi
	leal	(%rax,%rax), %edx
	testb	$8, 1(%rdi,%rsi,2)
	jne	.L71
	addq	$2, %rbx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$1, %rbx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$3, %rbx
	leal	-48(%rdx,%rsi), %edx
	jmp	.L69
	.size	precsize_aton, .-precsize_aton
	.section	.rodata.str1.1
.LC1:
	.string	"ZONE"
.LC2:
	.string	"QUERY"
.LC3:
	.string	"%d"
	.text
	.p2align 4,,15
	.type	p_section, @function
p_section:
	leaq	.LC2(%rip), %rdx
	leaq	.LC1(%rip), %rax
	cmpl	$5, %esi
	leaq	__p_update_section_syms(%rip), %rcx
	cmovne	%rdx, %rax
	leaq	__p_default_section_syms(%rip), %rdx
	cmovne	%rdx, %rcx
	testl	%edi, %edi
	jne	.L85
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	(%rcx), %edi
	je	.L92
.L85:
	addq	$24, %rcx
	movq	8(%rcx), %rax
	testq	%rax, %rax
	jne	.L86
	movl	%edi, %edx
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	subq	$8, %rsp
	call	sprintf@PLT
	leaq	unname.13530(%rip), %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L92:
	rep ret
	.p2align 4,,10
	.p2align 3
.L95:
	rep ret
	.size	p_section, .-p_section
	.section	.rodata.str1.1
.LC4:
	.string	"CHAOS"
.LC5:
	.string	"IN"
.LC6:
	.string	"NS"
.LC7:
	.string	"A"
.LC8:
	.string	"."
.LC9:
	.string	";; memory allocation failure\n"
.LC10:
	.string	";; ns_parserr: %s\n"
.LC11:
	.string	";; %s SECTION:\n"
.LC12:
	.string	";;\t%s, type = %s, class = %s\n"
.LC13:
	.string	";; ns_sprintrr: %s\n"
	.text
	.p2align 4,,15
	.type	do_section, @function
do_section:
	pushq	%r15
	pushq	%r14
	andl	%edi, %ecx
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r12d
	pushq	%rbp
	pushq	%rbx
	subq	$1096, %rsp
	testl	%edi, %edi
	je	.L123
	testl	%ecx, %ecx
	jne	.L123
.L96:
	addq	$1096, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movl	%edi, 24(%rsp)
	movslq	buflen.13439(%rip), %rdi
	movl	%edx, %ebp
	movq	%r8, 8(%rsp)
	movq	%rsi, %r13
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L117
	movzwl	18(%r13), %eax
	movl	12+_ns_flagdata(%rip), %ecx
	leaq	32(%rsp), %r14
	andl	8+_ns_flagdata(%rip), %eax
	xorl	%ebx, %ebx
	sarl	%cl, %eax
	movl	%eax, 28(%rsp)
	.p2align 4,,10
	.p2align 3
.L100:
	movq	%r14, %rcx
	movl	%ebx, %edx
	movl	%ebp, %esi
	movq	%r13, %rdi
	call	__GI_ns_parserr
	testl	%eax, %eax
	jne	.L151
	testl	%ebx, %ebx
	jne	.L104
	testl	%r12d, %r12d
	je	.L104
	testl	$256, 24(%rsp)
	jne	.L152
.L104:
	testl	%ebp, %ebp
	jne	.L105
	movzwl	1060(%rsp), %edx
	cmpl	$1, %edx
	je	.L120
	leaq	.LC4(%rip), %r8
	leaq	__GI___p_class_syms(%rip), %rax
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L108:
	cmpl	(%rax), %edx
	je	.L106
	movq	32(%rax), %r8
.L107:
	addq	$24, %rax
	testq	%r8, %r8
	jne	.L108
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	leaq	unname.13530(%rip), %r8
.L106:
	movzwl	1058(%rsp), %edx
	cmpl	$1, %edx
	je	.L121
	leaq	.LC6(%rip), %rcx
	leaq	__GI___p_type_syms(%rip), %rax
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	cmpl	(%rax), %edx
	je	.L109
	movq	32(%rax), %rcx
.L110:
	addq	$24, %rax
	testq	%rcx, %rcx
	jne	.L111
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	xorl	%eax, %eax
	movq	%r8, 16(%rsp)
	call	sprintf@PLT
	movq	16(%rsp), %r8
	leaq	unname.13530(%rip), %rcx
.L109:
	cmpb	$0, 32(%rsp)
	leaq	.LC8(%rip), %rdx
	movq	8(%rsp), %rdi
	leaq	.LC12(%rip), %rsi
	cmovne	%r14, %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
.L113:
	addl	$1, %ebx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L105:
	movslq	buflen.13439(%rip), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__GI_ns_sprintrr
	testl	%eax, %eax
	jns	.L114
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edi
	cmpl	$28, %edi
	jne	.L115
	movq	%r15, %rdi
	call	free@PLT
	movl	buflen.13439(%rip), %edi
	cmpl	$131071, %edi
	jg	.L117
	addl	$1024, %edi
	movl	%edi, buflen.13439(%rip)
	movslq	%edi, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L100
.L117:
	movq	8(%rsp), %rcx
	leaq	.LC9(%rip), %rdi
	movl	$29, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%rsp), %rsi
	movq	%r15, %rdi
	call	fputs@PLT
	movq	8(%rsp), %rsi
	movl	$10, %edi
	call	fputc@PLT
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L152:
	movl	28(%rsp), %esi
	movl	%ebp, %edi
	call	p_section
	movq	8(%rsp), %rdi
	leaq	.LC11(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L151:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edi
	cmpl	$19, %edi
	jne	.L153
	testl	%ebx, %ebx
	jle	.L103
	testl	%r12d, %r12d
	je	.L103
	testl	$256, 24(%rsp)
	je	.L103
	movq	8(%rsp), %rsi
	movl	$10, %edi
	call	putc@PLT
.L103:
	addq	$1096, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L121:
	leaq	.LC7(%rip), %rcx
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	.LC5(%rip), %r8
	jmp	.L106
.L153:
	call	strerror@PLT
	movq	8(%rsp), %rdi
	leaq	.LC10(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	jmp	.L103
.L115:
	call	strerror@PLT
	movq	8(%rsp), %rdi
	leaq	.LC13(%rip), %rsi
	movq	%rax, %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	jmp	.L103
	.size	do_section, .-do_section
	.section	.rodata.str1.1
.LC14:
	.string	"FORMERR"
.LC15:
	.string	";; ns_initparse: %s\n"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	";; ->>HEADER<<- opcode: %s, status: %s, id: %d\n"
	.section	.rodata.str1.1
.LC17:
	.string	"; flags:"
.LC18:
	.string	" qr"
.LC19:
	.string	" aa"
.LC20:
	.string	" tc"
.LC21:
	.string	" rd"
.LC22:
	.string	" ra"
.LC23:
	.string	" ??"
.LC24:
	.string	" ad"
.LC25:
	.string	" cd"
.LC26:
	.string	"; %s: %d"
.LC27:
	.string	", %s: %d"
.LC28:
	.string	"NOERROR"
	.text
	.p2align 4,,15
	.globl	__GI___fp_nquery
	.hidden	__GI___fp_nquery
	.type	__GI___fp_nquery, @function
__GI___fp_nquery:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$120, %rsp
	movq	__resp@gottpoff(%rip), %rax
	leaq	32(%rsp), %r13
	movq	%fs:(%rax), %rax
	movq	%r13, %rdx
	movq	384(%rax), %rbp
	call	__GI_ns_initparse
	testl	%eax, %eax
	js	.L217
	movzwl	52(%rsp), %eax
	movzwl	50(%rsp), %edx
	movl	8+_ns_flagdata(%rip), %r12d
	movl	12+_ns_flagdata(%rip), %ecx
	movzwl	48(%rsp), %r8d
	movw	%ax, 8(%rsp)
	movl	%eax, 12(%rsp)
	andl	%edx, %r12d
	movzwl	54(%rsp), %eax
	andl	72+_ns_flagdata(%rip), %edx
	sarl	%cl, %r12d
	movl	76+_ns_flagdata(%rip), %ecx
	movw	%ax, 10(%rsp)
	movl	%eax, 16(%rsp)
	sarl	%cl, %edx
	movzwl	56(%rsp), %eax
	testl	%ebp, %ebp
	movl	%eax, %r14d
	movl	%eax, 20(%rsp)
	movzwl	58(%rsp), %eax
	movl	%eax, %r15d
	movl	%eax, 24(%rsp)
	je	.L157
	movl	%ebp, %eax
	andl	$2048, %eax
	orl	%edx, %eax
	je	.L167
	testl	%edx, %edx
	je	.L181
.L216:
	leaq	.LC14(%rip), %rcx
	leaq	__p_rcode_syms(%rip), %rax
	jmp	.L164
	.p2align 4,,10
	.p2align 3
.L165:
	cmpl	(%rax), %edx
	je	.L163
	movq	32(%rax), %rcx
.L164:
	addq	$24, %rax
	testq	%rcx, %rcx
	jne	.L165
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	xorl	%eax, %eax
	movl	%r8d, 28(%rsp)
	call	sprintf@PLT
	movl	28(%rsp), %r8d
	leaq	unname.13530(%rip), %rcx
.L163:
	leaq	res_opcodes(%rip), %rdx
	movl	%r12d, %eax
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	testl	%ebp, %ebp
	je	.L166
.L180:
	testl	$2048, %ebp
	je	.L167
	movq	%rbx, %rsi
	movl	$59, %edi
	call	putc@PLT
.L167:
	testl	$512, %ebp
	je	.L160
.L159:
	leaq	.LC17(%rip), %rdi
	movq	%rbx, %rcx
	movl	$8, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	movl	_ns_flagdata(%rip), %edx
	movl	4+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L218
.L168:
	movl	16+_ns_flagdata(%rip), %edx
	movl	20+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L219
.L169:
	movl	24+_ns_flagdata(%rip), %edx
	movl	28+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L220
.L170:
	movl	32+_ns_flagdata(%rip), %edx
	movl	36+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L221
.L171:
	movl	40+_ns_flagdata(%rip), %edx
	movl	44+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L222
.L172:
	movl	48+_ns_flagdata(%rip), %edx
	movl	52+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L223
.L173:
	movl	56+_ns_flagdata(%rip), %edx
	movl	60+_ns_flagdata(%rip), %ecx
	andl	%eax, %edx
	sarl	%cl, %edx
	testl	%edx, %edx
	jne	.L224
.L174:
	andl	64+_ns_flagdata(%rip), %eax
	movl	68+_ns_flagdata(%rip), %ecx
	sarl	%cl, %eax
	testl	%eax, %eax
	jne	.L225
.L175:
	testl	%ebp, %ebp
	jne	.L160
	movl	%r12d, %esi
	xorl	%edi, %edi
	call	p_section
	movl	12(%rsp), %ecx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	movl	%r12d, %esi
	movl	$1, %edi
	call	p_section
	movl	16(%rsp), %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	movl	%r12d, %esi
	movl	$2, %edi
	call	p_section
	movl	20(%rsp), %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	movl	%r12d, %esi
	movl	$3, %edi
	call	p_section
	movl	24(%rsp), %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
.L179:
	movq	%rbx, %rsi
	movl	$10, %edi
	call	putc@PLT
.L177:
	xorl	%edx, %edx
	movq	%rbx, %r8
	movl	$16, %ecx
	movq	%r13, %rsi
	movl	%ebp, %edi
	call	do_section
	movq	%rbx, %r8
	movl	$32, %ecx
	movl	$1, %edx
	movq	%r13, %rsi
	movl	%ebp, %edi
	call	do_section
	movq	%rbx, %r8
	movl	$64, %ecx
	movl	$2, %edx
	movq	%r13, %rsi
	movl	%ebp, %edi
	call	do_section
	movq	%rbx, %r8
	movl	$128, %ecx
	movl	$3, %edx
	movq	%r13, %rsi
	movl	%ebp, %edi
	call	do_section
	movzwl	8(%rsp), %eax
	orw	10(%rsp), %ax
	orl	%eax, %r14d
	orw	%r15w, %r14w
	je	.L226
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L157:
	testl	%edx, %edx
	jne	.L216
	leaq	res_opcodes(%rip), %rdx
	movl	%r12d, %eax
	leaq	.LC28(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	.p2align 4,,10
	.p2align 3
.L166:
	movq	%rbx, %rsi
	movl	$59, %edi
	call	putc@PLT
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L160:
	testl	$256, %ebp
	je	.L182
	movl	%r12d, %esi
	xorl	%edi, %edi
	call	p_section
	movl	12(%rsp), %ecx
	leaq	.LC26(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	movl	%r12d, %esi
	movl	$1, %edi
	call	p_section
	movl	16(%rsp), %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	movl	%r12d, %esi
	movl	$2, %edi
	call	p_section
	movl	20(%rsp), %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	movl	%r12d, %esi
	movl	$3, %edi
	call	p_section
	movl	24(%rsp), %ecx
	leaq	.LC27(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
.L182:
	testl	$2816, %ebp
	je	.L177
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L226:
	movq	%rbx, %rsi
	movl	$10, %edi
	call	putc@PLT
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L217:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edi
	call	strerror@PLT
	leaq	.LC15(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC25(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L224:
	leaq	.LC24(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L174
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	.LC23(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L222:
	leaq	.LC22(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L221:
	leaq	.LC21(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L220:
	leaq	.LC20(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L170
	.p2align 4,,10
	.p2align 3
.L219:
	leaq	.LC19(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L218:
	leaq	.LC18(%rip), %rdi
	movq	%rbx, %rcx
	movl	$3, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movzwl	50(%rsp), %eax
	jmp	.L168
.L181:
	leaq	res_opcodes(%rip), %rdx
	movl	%r12d, %eax
	leaq	.LC28(%rip), %rcx
	leaq	.LC16(%rip), %rsi
	movq	%rbx, %rdi
	movq	(%rdx,%rax,8), %rdx
	xorl	%eax, %eax
	call	fprintf@PLT
	jmp	.L180
	.size	__GI___fp_nquery, .-__GI___fp_nquery
	.globl	__fp_nquery
	.set	__fp_nquery,__GI___fp_nquery
	.p2align 4,,15
	.globl	__GI___fp_query
	.hidden	__GI___fp_query
	.type	__GI___fp_query, @function
__GI___fp_query:
	movq	%rsi, %rdx
	movl	$512, %esi
	jmp	__GI___fp_nquery
	.size	__GI___fp_query, .-__GI___fp_query
	.globl	__fp_query
	.set	__fp_query,__GI___fp_query
	.p2align 4,,15
	.globl	__p_query
	.type	__p_query, @function
__p_query:
	movq	stdout@GOTPCREL(%rip), %rax
	movl	$512, %esi
	movq	(%rax), %rdx
	jmp	__GI___fp_nquery
	.size	__p_query, .-__p_query
	.p2align 4,,15
	.globl	__GI___p_cdnname
	.hidden	__GI___p_cdnname
	.type	__GI___p_cdnname, @function
__GI___p_cdnname:
	pushq	%r13
	pushq	%r12
	movl	$1025, %r8d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movslq	%edx, %rsi
	movq	%rcx, %r12
	subq	$1048, %rsp
	addq	%rdi, %rsi
	movq	%rbx, %rdx
	movq	%rsp, %r13
	movq	%r13, %rcx
	call	__GI___dn_expand
	testl	%eax, %eax
	js	.L233
	cmpb	$0, (%rsp)
	movl	%eax, %ebp
	movq	%r12, %rsi
	je	.L235
	movq	%r13, %rdi
	call	fputs@PLT
.L232:
	movslq	%ebp, %rax
	addq	%rbx, %rax
.L229:
	addq	$1048, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$46, %edi
	call	putc@PLT
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L233:
	xorl	%eax, %eax
	jmp	.L229
	.size	__GI___p_cdnname, .-__GI___p_cdnname
	.globl	__p_cdnname
	.set	__p_cdnname,__GI___p_cdnname
	.p2align 4,,15
	.globl	__p_cdname
	.type	__p_cdname, @function
__p_cdname:
	movq	%rdx, %rcx
	movl	$512, %edx
	jmp	__GI___p_cdnname
	.size	__p_cdname, .-__p_cdname
	.p2align 4,,15
	.globl	__GI___p_fqnname
	.hidden	__GI___p_fqnname
	.type	__GI___p_fqnname, @function
__GI___p_fqnname:
	pushq	%r13
	pushq	%r12
	movl	%r8d, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	movslq	%edx, %rsi
	movq	%rbx, %rdx
	subq	$8, %rsp
	addq	%rbx, %rsi
	movq	%rcx, %rbp
	call	__GI___dn_expand
	testl	%eax, %eax
	js	.L242
	movq	%rbp, %rdi
	movl	%eax, %r13d
	call	strlen@PLT
	testl	%eax, %eax
	jne	.L244
	leal	1(%rax), %edx
	cmpl	%r12d, %edx
	jge	.L242
.L245:
	cltq
	movl	$46, %edx
	movw	%dx, 0(%rbp,%rax)
.L240:
	addq	$8, %rsp
	movslq	%r13d, %rax
	addq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L244:
	movslq	%eax, %rdx
	cmpb	$46, -1(%rbp,%rdx)
	je	.L240
	leal	1(%rax), %edx
	cmpl	%r12d, %edx
	jl	.L245
.L242:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___p_fqnname, .-__GI___p_fqnname
	.globl	__p_fqnname
	.set	__p_fqnname,__GI___p_fqnname
	.p2align 4,,15
	.globl	__p_fqname
	.type	__p_fqname, @function
__p_fqname:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movl	$1025, %r8d
	movl	$255, %edx
	subq	$1040, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rcx
	call	__GI___p_fqnname
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L246
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	fputs@PLT
.L246:
	addq	$1040, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__p_fqname, .-__p_fqname
	.p2align 4,,15
	.globl	__sym_ston
	.type	__sym_ston, @function
__sym_ston:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	8(%rdi), %rsi
	movq	%rdi, %rbx
	movq	%rdx, %r12
	testq	%rsi, %rsi
	jne	.L257
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L254:
	addq	$24, %rbx
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L253
.L257:
	movq	%rbp, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L254
	testq	%r12, %r12
	je	.L258
	movl	$1, (%r12)
.L258:
	movl	(%rbx), %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L253:
	testq	%r12, %r12
	je	.L258
	movl	$0, (%r12)
	movl	(%rbx), %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__sym_ston, .-__sym_ston
	.p2align 4,,15
	.globl	__GI___sym_ntos
	.hidden	__GI___sym_ntos
	.type	__GI___sym_ntos, @function
__GI___sym_ntos:
	pushq	%rbx
	movq	8(%rdi), %rax
	movq	%rdx, %rbx
	testq	%rax, %rax
	je	.L271
	cmpl	(%rdi), %esi
	jne	.L273
	jmp	.L272
	.p2align 4,,10
	.p2align 3
.L275:
	cmpl	%esi, (%rdi)
	je	.L272
.L273:
	addq	$24, %rdi
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L275
.L271:
	movl	%esi, %edx
	leaq	unname.13530(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	sprintf@PLT
	testq	%rbx, %rbx
	leaq	unname.13530(%rip), %rax
	je	.L270
	movl	$0, (%rbx)
.L270:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L272:
	testq	%rbx, %rbx
	je	.L270
	movl	$1, (%rbx)
	popq	%rbx
	ret
	.size	__GI___sym_ntos, .-__GI___sym_ntos
	.globl	__sym_ntos
	.set	__sym_ntos,__GI___sym_ntos
	.p2align 4,,15
	.globl	__sym_ntop
	.type	__sym_ntop, @function
__sym_ntop:
	pushq	%rbx
	cmpq	$0, 8(%rdi)
	movq	%rdx, %rbx
	je	.L287
	cmpl	(%rdi), %esi
	jne	.L289
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L292:
	cmpl	%esi, (%rdi)
	je	.L288
.L289:
	addq	$24, %rdi
	cmpq	$0, 8(%rdi)
	jne	.L292
.L287:
	movl	%esi, %edx
	leaq	unname.13541(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	sprintf@PLT
	testq	%rbx, %rbx
	leaq	unname.13541(%rip), %rax
	je	.L286
	movl	$0, (%rbx)
.L286:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	testq	%rbx, %rbx
	je	.L290
	movl	$1, (%rbx)
.L290:
	movq	16(%rdi), %rax
	popq	%rbx
	ret
	.size	__sym_ntop, .-__sym_ntop
	.p2align 4,,15
	.globl	__GI___p_type
	.hidden	__GI___p_type
	.type	__GI___p_type, @function
__GI___p_type:
	cmpl	$1, %edi
	je	.L304
	leaq	.LC6(%rip), %rax
	leaq	__GI___p_type_syms(%rip), %rcx
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L303:
	cmpl	(%rcx), %edi
	je	.L306
	movq	32(%rcx), %rax
.L302:
	addq	$24, %rcx
	testq	%rax, %rax
	jne	.L303
	movl	%edi, %edx
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	subq	$8, %rsp
	call	sprintf@PLT
	leaq	unname.13530(%rip), %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	rep ret
	.p2align 4,,10
	.p2align 3
.L304:
	leaq	.LC7(%rip), %rax
	ret
	.size	__GI___p_type, .-__GI___p_type
	.globl	__p_type
	.set	__p_type,__GI___p_type
	.p2align 4,,15
	.globl	__GI___p_class
	.hidden	__GI___p_class
	.type	__GI___p_class, @function
__GI___p_class:
	cmpl	$1, %edi
	je	.L313
	leaq	.LC4(%rip), %rax
	leaq	__GI___p_class_syms(%rip), %rcx
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L312:
	cmpl	(%rcx), %edi
	je	.L315
	movq	32(%rcx), %rax
.L311:
	addq	$24, %rcx
	testq	%rax, %rax
	jne	.L312
	movl	%edi, %edx
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	subq	$8, %rsp
	call	sprintf@PLT
	leaq	unname.13530(%rip), %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L315:
	rep ret
	.p2align 4,,10
	.p2align 3
.L313:
	leaq	.LC5(%rip), %rax
	ret
	.size	__GI___p_class, .-__GI___p_class
	.globl	__p_class
	.set	__p_class,__GI___p_class
	.section	.rodata.str1.1
.LC29:
	.string	"init"
.LC30:
	.string	"debug"
.LC31:
	.string	"use-vc"
.LC32:
	.string	"igntc"
.LC33:
	.string	"recurs"
.LC34:
	.string	"defnam"
.LC35:
	.string	"styopn"
.LC36:
	.string	"dnsrch"
.LC37:
	.string	"noaliases"
.LC38:
	.string	"rotate"
.LC39:
	.string	"edns0"
.LC40:
	.string	"single-request"
.LC41:
	.string	"single-request-reopen"
.LC42:
	.string	"dnssec"
.LC43:
	.string	"no-tld-query"
.LC44:
	.string	"no-reload"
.LC45:
	.string	"trust-ad"
.LC46:
	.string	"?0x%lx?"
	.text
	.p2align 4,,15
	.globl	__GI___p_option
	.hidden	__GI___p_option
	.type	__GI___p_option, @function
__GI___p_option:
	cmpq	$4096, %rdi
	je	.L320
	jbe	.L362
	cmpq	$4194304, %rdi
	je	.L332
	jbe	.L363
	cmpq	$16777216, %rdi
	leaq	.LC43(%rip), %rax
	je	.L358
	jbe	.L364
	cmpq	$33554432, %rdi
	leaq	.LC44(%rip), %rax
	je	.L358
	cmpq	$67108864, %rdi
	leaq	.LC45(%rip), %rax
	jne	.L319
.L358:
	rep ret
	.p2align 4,,10
	.p2align 3
.L362:
	cmpq	$32, %rdi
	je	.L322
	jbe	.L365
	cmpq	$128, %rdi
	leaq	.LC34(%rip), %rax
	je	.L358
	jbe	.L366
	cmpq	$256, %rdi
	leaq	.LC35(%rip), %rax
	je	.L358
	cmpq	$512, %rdi
	leaq	.LC36(%rip), %rax
	je	.L358
.L319:
	movq	%rdi, %rdx
	leaq	.LC46(%rip), %rsi
	leaq	nbuf.13566(%rip), %rdi
	subq	$8, %rsp
	xorl	%eax, %eax
	call	sprintf@PLT
	leaq	nbuf.13566(%rip), %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L365:
	cmpq	$2, %rdi
	leaq	.LC30(%rip), %rax
	je	.L358
	cmpq	$8, %rdi
	leaq	.LC31(%rip), %rax
	je	.L358
	cmpq	$1, %rdi
	leaq	.LC29(%rip), %rax
	jne	.L319
	rep ret
	.p2align 4,,10
	.p2align 3
.L363:
	cmpq	$1048576, %rdi
	leaq	.LC39(%rip), %rax
	je	.L358
	cmpq	$2097152, %rdi
	leaq	.LC40(%rip), %rax
	je	.L358
	cmpq	$16384, %rdi
	leaq	.LC38(%rip), %rax
	jne	.L319
	rep ret
	.p2align 4,,10
	.p2align 3
.L364:
	cmpq	$8388608, %rdi
	leaq	.LC42(%rip), %rax
	jne	.L319
	rep ret
	.p2align 4,,10
	.p2align 3
.L366:
	cmpq	$64, %rdi
	leaq	.LC33(%rip), %rax
	jne	.L319
	rep ret
	.p2align 4,,10
	.p2align 3
.L332:
	leaq	.LC41(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L322:
	leaq	.LC32(%rip), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	.LC37(%rip), %rax
	ret
	.size	__GI___p_option, .-__GI___p_option
	.globl	__p_option
	.set	__p_option,__GI___p_option
	.section	.rodata.str1.1
.LC47:
	.string	";; res options:"
.LC48:
	.string	" %s"
	.text
	.p2align 4,,15
	.globl	__fp_resstat
	.type	__fp_resstat, @function
__fp_resstat:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %rcx
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	leaq	.LC47(%rip), %rdi
	pushq	%rbx
	movq	%rsi, %r13
	movl	$15, %edx
	movl	$1, %esi
	movl	$64, %ebp
	call	fwrite@PLT
	movl	$1, %ebx
	leaq	.LC48(%rip), %r14
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L368:
	addq	%rbx, %rbx
	subl	$1, %ebp
	je	.L375
.L369:
	testq	%rbx, 8(%r12)
	je	.L368
	movq	%rbx, %rdi
	addq	%rbx, %rbx
	call	__GI___p_option
	movq	%r14, %rsi
	movq	%rax, %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	fprintf@PLT
	subl	$1, %ebp
	jne	.L369
.L375:
	popq	%rbx
	movq	%r13, %rsi
	movl	$10, %edi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	putc@PLT
	.size	__fp_resstat, .-__fp_resstat
	.section	.rodata.str1.1
.LC49:
	.string	"%u"
	.text
	.p2align 4,,15
	.globl	__p_time
	.type	__p_time, @function
__p_time:
	pushq	%rbx
	leaq	nbuf.13590(%rip), %rsi
	movl	%edi, %edi
	movl	$40, %edx
	movq	%rdi, %rbx
	call	__GI_ns_format_ttl
	testl	%eax, %eax
	js	.L379
	leaq	nbuf.13590(%rip), %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L379:
	leaq	.LC49(%rip), %rsi
	leaq	nbuf.13590(%rip), %rdi
	movl	%ebx, %edx
	xorl	%eax, %eax
	call	sprintf@PLT
	leaq	nbuf.13590(%rip), %rax
	popq	%rbx
	ret
	.size	__p_time, .-__p_time
	.p2align 4,,15
	.globl	__GI___p_rcode
	.hidden	__GI___p_rcode
	.type	__GI___p_rcode, @function
__GI___p_rcode:
	testl	%edi, %edi
	je	.L384
	leaq	.LC14(%rip), %rax
	leaq	__p_rcode_syms(%rip), %rcx
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L383:
	cmpl	(%rcx), %edi
	je	.L386
	movq	32(%rcx), %rax
.L382:
	addq	$24, %rcx
	testq	%rax, %rax
	jne	.L383
	movl	%edi, %edx
	leaq	.LC3(%rip), %rsi
	leaq	unname.13530(%rip), %rdi
	subq	$8, %rsp
	call	sprintf@PLT
	leaq	unname.13530(%rip), %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L386:
	rep ret
	.p2align 4,,10
	.p2align 3
.L384:
	leaq	.LC28(%rip), %rax
	ret
	.size	__GI___p_rcode, .-__GI___p_rcode
	.globl	__p_rcode
	.set	__p_rcode,__GI___p_rcode
	.p2align 4,,15
	.globl	__loc_aton
	.type	__loc_aton, @function
__loc_aton:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$56, %rsp
	leaq	40(%rsp), %rbp
	movq	%rdi, 40(%rsp)
	movl	$0, 32(%rsp)
	movl	$0, 36(%rsp)
	call	strlen@PLT
	leaq	32(%rsp), %rsi
	movq	%rbp, %rdi
	movq	%rax, %r14
	call	latlon2ul
	leaq	36(%rsp), %rsi
	movq	%rbp, %rdi
	movl	%eax, %r13d
	call	latlon2ul
	movl	36(%rsp), %edx
	movl	%eax, %r15d
	movl	32(%rsp), %eax
	leal	(%rax,%rdx), %ecx
	cmpl	$3, %ecx
	jne	.L418
	cmpl	$1, %eax
	je	.L490
	cmpl	$1, %edx
	jne	.L418
	cmpl	$2, %eax
	je	.L393
	.p2align 4,,10
	.p2align 3
.L418:
	xorl	%eax, %eax
.L389:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L490:
	cmpl	$2, %edx
	jne	.L418
	movl	%r15d, %eax
	movl	%r13d, %r15d
	movl	%eax, %r13d
.L393:
	movq	40(%rsp), %r11
	movl	$1, %r9d
	movzbl	(%r11), %r8d
	cmpb	$45, %r8b
	je	.L491
.L394:
	cmpb	$43, %r8b
	je	.L492
.L395:
	movq	%r11, 24(%rsp)
	movl	%r9d, 20(%rsp)
	movb	%r8b, 8(%rsp)
	call	__ctype_b_loc@PLT
	movq	24(%rsp), %r11
	movq	%rax, %rsi
	movq	(%rax), %rdi
	movzbl	8(%rsp), %r8d
	xorl	%eax, %eax
	movl	20(%rsp), %r9d
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L397:
	leal	(%rax,%rax,4), %ecx
	movq	%r11, 40(%rsp)
	movsbl	-1(%r11), %eax
	movzbl	1(%rdx), %r8d
	leal	-48(%rax,%rcx,2), %eax
.L396:
	movsbq	%r8b, %rcx
	movq	%r11, %rdx
	leaq	1(%r11), %r11
	movzwl	(%rdi,%rcx,2), %ecx
	testb	$8, %ch
	jne	.L397
	xorl	%r10d, %r10d
	cmpb	$46, %r8b
	je	.L493
.L398:
	addq	%r14, %rbx
	imull	$100, %eax, %r14d
	addl	%r10d, %r14d
	imull	%r14d, %r9d
	cmpq	%rdx, %rbx
	seta	%al
	andw	$8192, %cx
	movl	%eax, %r8d
	leal	10000000(%r9), %r14d
	jne	.L399
	testb	%al, %al
	je	.L399
	leaq	1(%rdx), %rax
	jmp	.L400
	.p2align 4,,10
	.p2align 3
.L494:
	testb	%r8b, %r8b
	je	.L399
.L400:
	movq	%rax, 40(%rsp)
	movsbq	(%rax), %rcx
	cmpq	%rbx, %rax
	setb	%r8b
	movq	%rax, %rdx
	addq	$1, %rax
	movzwl	(%rdi,%rcx,2), %ecx
	andw	$8192, %cx
	je	.L494
.L399:
	testw	%cx, %cx
	je	.L402
	testb	%r8b, %r8b
	je	.L402
	leaq	1(%rdx), %rax
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L495:
	cmpq	%rbx, %rdx
	jnb	.L402
.L403:
	movq	%rax, %rdx
	movq	%rax, 40(%rsp)
	addq	$1, %rax
	movsbq	(%rdx), %rcx
	testb	$32, 1(%rdi,%rcx,2)
	jne	.L495
.L402:
	cmpq	%rdx, %rbx
	ja	.L496
	movl	$18, %ecx
	movl	$19, %eax
	movl	$22, %r8d
.L405:
	bswap	%r15d
	bswap	%r13d
	bswap	%r14d
	movb	%al, 3(%r12)
	movb	$0, (%r12)
	movl	$16, %eax
	movb	%cl, 1(%r12)
	movb	%r8b, 2(%r12)
	movl	%r15d, 4(%r12)
	movl	%r13d, 8(%r12)
	movl	%r14d, 12(%r12)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L493:
	movq	%r11, 40(%rsp)
	movsbq	1(%rdx), %rcx
	movzwl	(%rdi,%rcx,2), %ecx
	testb	$8, %ch
	je	.L421
	leaq	2(%rdx), %r8
	movq	%r8, 40(%rsp)
	movsbl	1(%rdx), %ecx
	leal	-240(%rcx,%rcx,4), %r10d
	movsbq	2(%rdx), %rcx
	addl	%r10d, %r10d
	movzwl	(%rdi,%rcx,2), %ecx
	testb	$8, %ch
	je	.L422
	leaq	3(%rdx), %r8
	movq	%r8, 40(%rsp)
	movsbl	2(%rdx), %ecx
	movsbq	3(%rdx), %rdx
	leal	-48(%r10,%rcx), %r10d
	movzwl	(%rdi,%rdx,2), %ecx
	movq	%r8, %rdx
	jmp	.L398
	.p2align 4,,10
	.p2align 3
.L492:
	leaq	1(%r11), %rax
	movq	%rax, 40(%rsp)
	movzbl	1(%r11), %r8d
	movq	%rax, %r11
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L491:
	leaq	1(%r11), %rax
	movl	$-1, %r9d
	movq	%rax, 40(%rsp)
	movzbl	1(%r11), %r8d
	movq	%rax, %r11
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L496:
	movq	%rbp, %rdi
	movq	%rsi, 8(%rsp)
	call	precsize_aton
	movq	40(%rsp), %rdi
	movq	8(%rsp), %rsi
	movl	%eax, %ecx
	movsbq	(%rdi), %rax
	movq	(%rsi), %r8
	movzwl	(%r8,%rax,2), %edx
	andw	$8192, %dx
	cmpq	%rdi, %rbx
	jbe	.L406
	testw	%dx, %dx
	jne	.L406
	leaq	1(%rdi), %rax
	jmp	.L407
	.p2align 4,,10
	.p2align 3
.L497:
	testw	%dx, %dx
	jne	.L406
.L407:
	movq	%rax, 40(%rsp)
	movsbq	(%rax), %rdx
	movq	%rax, %rdi
	addq	$1, %rax
	movzwl	(%r8,%rdx,2), %edx
	andw	$8192, %dx
	cmpq	%rbx, %rdi
	jb	.L497
.L406:
	testw	%dx, %dx
	je	.L409
	cmpq	%rdi, %rbx
	jbe	.L425
	leaq	1(%rdi), %rax
	leaq	1(%rbx), %r9
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L498:
	addq	$1, %rax
	cmpq	%rax, %r9
	je	.L425
.L410:
	movq	%rax, 40(%rsp)
	movsbq	(%rax), %rdx
	movq	%rax, %rdi
	testb	$32, 1(%r8,%rdx,2)
	jne	.L498
.L409:
	cmpq	%rdi, %rbx
	movq	%rsi, 8(%rsp)
	jbe	.L425
	movq	%rbp, %rdi
	movb	%cl, 20(%rsp)
	call	precsize_aton
	movq	8(%rsp), %rsi
	movl	%eax, %r8d
	movzbl	20(%rsp), %ecx
	movq	(%rsi), %rdi
	movq	40(%rsp), %rsi
	movsbq	(%rsi), %rax
	movzwl	(%rdi,%rax,2), %edx
	andw	$8192, %dx
	jne	.L411
	cmpq	%rsi, %rbx
	jbe	.L411
	leaq	1(%rsi), %rax
	jmp	.L412
	.p2align 4,,10
	.p2align 3
.L499:
	cmpq	%rbx, %rsi
	jnb	.L411
.L412:
	movq	%rax, 40(%rsp)
	movsbq	(%rax), %rdx
	movq	%rax, %rsi
	addq	$1, %rax
	movzwl	(%rdi,%rdx,2), %edx
	andw	$8192, %dx
	je	.L499
.L411:
	testw	%dx, %dx
	je	.L414
	cmpq	%rsi, %rbx
	jbe	.L426
	leaq	1(%rsi), %rax
	leaq	1(%rbx), %r9
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L500:
	addq	$1, %rax
	cmpq	%rax, %r9
	je	.L426
.L415:
	movq	%rax, 40(%rsp)
	movsbq	(%rax), %rdx
	movq	%rax, %rsi
	testb	$32, 1(%rdi,%rdx,2)
	jne	.L500
.L414:
	cmpq	%rsi, %rbx
	movl	$19, %eax
	jbe	.L405
	movq	%rbp, %rdi
	movb	%cl, 20(%rsp)
	movb	%r8b, 8(%rsp)
	call	precsize_aton
	movzbl	8(%rsp), %r8d
	movzbl	20(%rsp), %ecx
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L425:
	movl	$19, %eax
	movl	$22, %r8d
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L421:
	movq	%r11, %rdx
	jmp	.L398
.L422:
	movq	%r8, %rdx
	jmp	.L398
.L426:
	movl	$19, %eax
	jmp	.L405
	.size	__loc_aton, .-__loc_aton
	.section	.rodata.str1.8
	.align 8
.LC50:
	.string	"%d %.2d %.2d.%.3d %c %d %.2d %.2d.%.3d %c %d.%.2dm %sm %sm %sm"
	.text
	.p2align 4,,15
	.globl	__GI___loc_ntoa
	.hidden	__GI___loc_ntoa
	.type	__GI___loc_ntoa, @function
__GI___loc_ntoa:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	tmpbuf.13730(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	testq	%rsi, %rsi
	cmovne	%rsi, %r12
	cmpb	$0, (%rdi)
	jne	.L521
	movzbl	3(%rdi), %eax
	movl	12(%rdi), %r10d
	movl	4(%rdi), %esi
	movl	8(%rdi), %ecx
	movzbl	1(%rdi), %r11d
	movzbl	2(%rdi), %r13d
	movb	%al, 23(%rsp)
	movl	%r10d, %eax
	bswap	%esi
	bswap	%ecx
	bswap	%eax
	addl	$-2147483648, %esi
	addl	$-2147483648, %ecx
	cmpl	$9999999, %eax
	jbe	.L522
	leal	-10000000(%rax), %r10d
	movl	$1, %edi
.L506:
	testl	%esi, %esi
	movl	$78, 12(%rsp)
	jns	.L507
	negl	%esi
	movl	$83, 12(%rsp)
.L507:
	movl	%esi, %eax
	movl	$274877907, %edx
	movl	$-2004318071, %ebp
	mull	%edx
	movl	%esi, %eax
	movl	$69, 16(%rsp)
	movl	%edx, %r9d
	movl	%edx, %ebx
	shrl	$6, %r9d
	shrl	$6, %ebx
	imull	$1000, %r9d, %r9d
	subl	%r9d, %eax
	movl	%eax, %r9d
	movl	%ebx, %eax
	imull	%ebp
	movl	%esi, %eax
	leal	(%rdx,%rbx), %r8d
	movl	$1172812403, %edx
	mull	%edx
	sarl	$5, %r8d
	imull	$60, %r8d, %r8d
	subl	%r8d, %ebx
	movl	%ebx, %r8d
	movl	%edx, %ebx
	shrl	$14, %ebx
	movl	%ebx, %eax
	imull	%ebp
	movl	%esi, %eax
	addl	%ebx, %edx
	sarl	$5, %edx
	imull	$60, %edx, %edx
	subl	%edx, %ebx
	movl	$-1792967503, %edx
	mull	%edx
	movl	%ebx, 24(%rsp)
	shrl	$21, %edx
	testl	%ecx, %ecx
	movl	%edx, 28(%rsp)
	jns	.L508
	negl	%ecx
	movl	$87, 16(%rsp)
.L508:
	movl	%ecx, %eax
	movl	$274877907, %edx
	movl	%ecx, %esi
	mull	%edx
	movl	$-2004318071, %ebx
	movl	%r8d, 48(%rsp)
	movl	%r9d, (%rsp)
	movl	%edx, %eax
	shrl	$6, %eax
	imull	$1000, %eax, %eax
	subl	%eax, %esi
	movl	%esi, 32(%rsp)
	movl	%edx, %esi
	shrl	$6, %esi
	movl	%esi, %eax
	imull	%ebx
	movl	%ecx, %eax
	leal	(%rdx,%rsi), %ebp
	movl	$1172812403, %edx
	mull	%edx
	sarl	$5, %ebp
	imull	$60, %ebp, %ebp
	subl	%ebp, %esi
	movl	%esi, %ebp
	movl	%edx, %esi
	shrl	$14, %esi
	movl	%esi, %eax
	imull	%ebx
	movl	%ecx, %eax
	leal	(%rdx,%rsi), %ebx
	movl	$-1792967503, %edx
	mull	%edx
	movl	%r10d, %eax
	sarl	$5, %ebx
	shrl	$21, %edx
	movl	%edx, 36(%rsp)
	movl	$1374389535, %edx
	imull	%edx
	movl	%r10d, %eax
	sarl	$31, %eax
	sarl	$5, %edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	imull	$100, %ecx, %r14d
	movl	%ecx, %edx
	imull	$60, %ebx, %ebx
	subl	%r14d, %r10d
	imull	%edi, %edx
	subl	%ebx, %esi
	movzbl	%r11b, %edi
	movl	%esi, %ebx
	movl	%r10d, 40(%rsp)
	movl	%edx, 44(%rsp)
	call	precsize_ntoa
	movq	%rax, %rdi
	call	strdup@PLT
	movq	%rax, %r15
	leaq	error.13729(%rip), %rax
	movl	(%rsp), %r9d
	testq	%r15, %r15
	movl	48(%rsp), %r8d
	movq	%rax, (%rsp)
	je	.L517
.L509:
	movzbl	%r13b, %edi
	movl	%r8d, 60(%rsp)
	movl	%r9d, 56(%rsp)
	call	precsize_ntoa
	movq	%rax, %rdi
	call	strdup@PLT
	movzbl	23(%rsp), %edi
	leaq	error.13729(%rip), %rdx
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rdx, 48(%rsp)
	cmove	%rdx, %r14
	call	precsize_ntoa
	movq	%rax, %rdi
	call	strdup@PLT
	movq	48(%rsp), %rdx
	testq	%rax, %rax
	movq	%rax, %r13
	leaq	.LC50(%rip), %rsi
	movq	%r12, %rdi
	cmove	%rdx, %r13
	subq	$8, %rsp
	pushq	%r13
	pushq	%r14
	pushq	%r15
	movl	72(%rsp), %eax
	pushq	%rax
	movl	84(%rsp), %eax
	pushq	%rax
	movl	64(%rsp), %eax
	pushq	%rax
	movl	88(%rsp), %eax
	pushq	%rax
	pushq	%rbp
	pushq	%rbx
	movl	116(%rsp), %eax
	pushq	%rax
	movl	100(%rsp), %eax
	pushq	%rax
	movl	152(%rsp), %r9d
	xorl	%eax, %eax
	movl	156(%rsp), %r8d
	movl	120(%rsp), %ecx
	movl	124(%rsp), %edx
	call	sprintf@PLT
	addq	$96, %rsp
	cmpq	(%rsp), %r15
	je	.L512
	movq	%r15, %rdi
	call	free@PLT
.L512:
	cmpq	(%rsp), %r14
	je	.L513
	movq	%r14, %rdi
	call	free@PLT
.L513:
	cmpq	(%rsp), %r13
	je	.L501
	movq	%r13, %rdi
	call	free@PLT
.L501:
	addq	$72, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L522:
	movl	$10000000, %r10d
	movl	$-1, %edi
	subl	%eax, %r10d
	jmp	.L506
	.p2align 4,,10
	.p2align 3
.L521:
	movdqa	.LC51(%rip), %xmm0
	movups	%xmm0, (%r12)
	movdqa	.LC52(%rip), %xmm0
	movups	%xmm0, 16(%r12)
	jmp	.L501
	.p2align 4,,10
	.p2align 3
.L517:
	movq	%rax, %r15
	jmp	.L509
	.size	__GI___loc_ntoa, .-__GI___loc_ntoa
	.globl	__loc_ntoa
	.set	__loc_ntoa,__GI___loc_ntoa
	.p2align 4,,15
	.globl	__GI___dn_count_labels
	.hidden	__GI___dn_count_labels
	.type	__GI___dn_count_labels, @function
__GI___dn_count_labels:
	pushq	%rbx
	movq	%rdi, %rbx
	call	strlen@PLT
	testl	%eax, %eax
	jle	.L529
	leal	-1(%rax), %ecx
	movq	%rbx, %rdx
	leaq	1(%rbx,%rcx), %rdi
	xorl	%ecx, %ecx
	.p2align 4,,10
	.p2align 3
.L526:
	xorl	%esi, %esi
	cmpb	$46, (%rdx)
	sete	%sil
	addq	$1, %rdx
	addl	%esi, %ecx
	cmpq	%rdi, %rdx
	jne	.L526
	cmpb	$42, (%rbx)
	jne	.L527
	cmpl	$1, %ecx
	adcl	$-1, %ecx
.L527:
	cltq
	cmpb	$46, -1(%rbx,%rax)
	je	.L523
	addl	$1, %ecx
.L523:
	movl	%ecx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L529:
	xorl	%ecx, %ecx
	movl	%ecx, %eax
	popq	%rbx
	ret
	.size	__GI___dn_count_labels, .-__GI___dn_count_labels
	.globl	__dn_count_labels
	.set	__dn_count_labels,__GI___dn_count_labels
	.section	.rodata.str1.1
.LC53:
	.string	"%04d%02d%02d%02d%02d%02d"
	.text
	.p2align 4,,15
	.globl	__p_secstodate
	.type	__p_secstodate, @function
__p_secstodate:
	subq	$88, %rsp
	cmpq	$2147483647, %rdi
	movq	%rdi, 8(%rsp)
	ja	.L537
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %rdi
	call	__gmtime_r@PLT
	testq	%rax, %rax
	je	.L537
	movl	20(%rax), %edx
	movl	16(%rax), %ecx
	leaq	output.13776(%rip), %rdi
	movl	(%rax), %esi
	addl	$1900, %edx
	addl	$1, %ecx
	movl	%edx, 20(%rax)
	movl	%ecx, 16(%rax)
	pushq	%rsi
	movl	4(%rax), %esi
	pushq	%rsi
	movl	8(%rax), %r9d
	leaq	.LC53(%rip), %rsi
	movl	12(%rax), %r8d
	xorl	%eax, %eax
	call	sprintf@PLT
	popq	%rax
	popq	%rdx
	leaq	output.13776(%rip), %rax
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L537:
	movabsq	$8028904877209317180, %rax
	movl	$15991, %ecx
	movb	$0, 10+output.13776(%rip)
	movq	%rax, output.13776(%rip)
	movq	errno@gottpoff(%rip), %rax
	movw	%cx, 8+output.13776(%rip)
	movl	$75, %fs:(%rax)
	leaq	output.13776(%rip), %rax
	addq	$88, %rsp
	ret
	.size	__p_secstodate, .-__p_secstodate
	.local	output.13776
	.comm	output.13776,15,8
	.local	retbuf.13600
	.comm	retbuf.13600,12,8
	.section	.rodata.str1.1
	.type	error.13729, @object
	.size	error.13729, 2
error.13729:
	.string	"?"
	.local	tmpbuf.13730
	.comm	tmpbuf.13730,87,32
	.local	nbuf.13590
	.comm	nbuf.13590,40,32
	.local	nbuf.13566
	.comm	nbuf.13566,40,32
	.local	unname.13541
	.comm	unname.13541,20,16
	.local	unname.13530
	.comm	unname.13530,20,16
	.data
	.align 4
	.type	buflen.13439, @object
	.size	buflen.13439, 4
buflen.13439:
	.long	2048
	.section	.rodata
	.align 32
	.type	poweroften, @object
	.size	poweroften, 40
poweroften:
	.long	1
	.long	10
	.long	100
	.long	1000
	.long	10000
	.long	100000
	.long	1000000
	.long	10000000
	.long	100000000
	.long	1000000000
	.hidden	__p_rcode_syms
	.globl	__p_rcode_syms
	.section	.rodata.str1.1
.LC54:
	.string	"no error"
.LC55:
	.string	"format error"
.LC56:
	.string	"SERVFAIL"
.LC57:
	.string	"server failed"
.LC58:
	.string	"NXDOMAIN"
.LC59:
	.string	"no such domain name"
.LC60:
	.string	"NOTIMP"
.LC61:
	.string	"not implemented"
.LC62:
	.string	"REFUSED"
.LC63:
	.string	"refused"
.LC64:
	.string	"YXDOMAIN"
.LC65:
	.string	"domain name exists"
.LC66:
	.string	"YXRRSET"
.LC67:
	.string	"rrset exists"
.LC68:
	.string	"NXRRSET"
.LC69:
	.string	"rrset doesn't exist"
.LC70:
	.string	"NOTAUTH"
.LC71:
	.string	"not authoritative"
.LC72:
	.string	"NOTZONE"
.LC73:
	.string	"Not in zone"
.LC74:
	.string	""
.LC75:
	.string	"BADSIG"
.LC76:
	.string	"bad signature"
.LC77:
	.string	"BADKEY"
.LC78:
	.string	"bad key"
.LC79:
	.string	"BADTIME"
.LC80:
	.string	"bad time"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	__p_rcode_syms, @object
	.size	__p_rcode_syms, 384
__p_rcode_syms:
	.long	0
	.zero	4
	.quad	.LC28
	.quad	.LC54
	.long	1
	.zero	4
	.quad	.LC14
	.quad	.LC55
	.long	2
	.zero	4
	.quad	.LC56
	.quad	.LC57
	.long	3
	.zero	4
	.quad	.LC58
	.quad	.LC59
	.long	4
	.zero	4
	.quad	.LC60
	.quad	.LC61
	.long	5
	.zero	4
	.quad	.LC62
	.quad	.LC63
	.long	6
	.zero	4
	.quad	.LC64
	.quad	.LC65
	.long	7
	.zero	4
	.quad	.LC66
	.quad	.LC67
	.long	8
	.zero	4
	.quad	.LC68
	.quad	.LC69
	.long	9
	.zero	4
	.quad	.LC70
	.quad	.LC71
	.long	10
	.zero	4
	.quad	.LC72
	.quad	.LC73
	.long	11
	.zero	4
	.quad	.LC74
	.quad	.LC74
	.long	16
	.zero	4
	.quad	.LC75
	.quad	.LC76
	.long	17
	.zero	4
	.quad	.LC77
	.quad	.LC78
	.long	18
	.zero	4
	.quad	.LC79
	.quad	.LC80
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.hidden	__GI___p_type_syms
	.globl	__GI___p_type_syms
	.section	.rodata.str1.1
.LC81:
	.string	"address"
.LC82:
	.string	"name server"
.LC83:
	.string	"MD"
.LC84:
	.string	"mail destination (deprecated)"
.LC85:
	.string	"MF"
.LC86:
	.string	"mail forwarder (deprecated)"
.LC87:
	.string	"CNAME"
.LC88:
	.string	"canonical name"
.LC89:
	.string	"SOA"
.LC90:
	.string	"start of authority"
.LC91:
	.string	"MB"
.LC92:
	.string	"mailbox"
.LC93:
	.string	"MG"
.LC94:
	.string	"mail group member"
.LC95:
	.string	"MR"
.LC96:
	.string	"mail rename"
.LC97:
	.string	"NULL"
.LC98:
	.string	"null"
.LC99:
	.string	"WKS"
	.section	.rodata.str1.8
	.align 8
.LC100:
	.string	"well-known service (deprecated)"
	.section	.rodata.str1.1
.LC101:
	.string	"PTR"
.LC102:
	.string	"domain name pointer"
.LC103:
	.string	"HINFO"
.LC104:
	.string	"host information"
.LC105:
	.string	"MINFO"
.LC106:
	.string	"mailbox information"
.LC107:
	.string	"MX"
.LC108:
	.string	"mail exchanger"
.LC109:
	.string	"TXT"
.LC110:
	.string	"text"
.LC111:
	.string	"RP"
.LC112:
	.string	"responsible person"
.LC113:
	.string	"AFSDB"
.LC114:
	.string	"DCE or AFS server"
.LC115:
	.string	"X25"
.LC116:
	.string	"X25 address"
.LC117:
	.string	"ISDN"
.LC118:
	.string	"ISDN address"
.LC119:
	.string	"RT"
.LC120:
	.string	"router"
.LC121:
	.string	"NSAP"
.LC122:
	.string	"nsap address"
.LC123:
	.string	"NSAP_PTR"
.LC124:
	.string	"SIG"
.LC125:
	.string	"signature"
.LC126:
	.string	"KEY"
.LC127:
	.string	"key"
.LC128:
	.string	"PX"
.LC129:
	.string	"mapping information"
.LC130:
	.string	"GPOS"
	.section	.rodata.str1.8
	.align 8
.LC131:
	.string	"geographical position (withdrawn)"
	.section	.rodata.str1.1
.LC132:
	.string	"AAAA"
.LC133:
	.string	"IPv6 address"
.LC134:
	.string	"LOC"
.LC135:
	.string	"location"
.LC136:
	.string	"NXT"
	.section	.rodata.str1.8
	.align 8
.LC137:
	.string	"next valid name (unimplemented)"
	.section	.rodata.str1.1
.LC138:
	.string	"EID"
	.section	.rodata.str1.8
	.align 8
.LC139:
	.string	"endpoint identifier (unimplemented)"
	.section	.rodata.str1.1
.LC140:
	.string	"NIMLOC"
	.section	.rodata.str1.8
	.align 8
.LC141:
	.string	"NIMROD locator (unimplemented)"
	.section	.rodata.str1.1
.LC142:
	.string	"SRV"
.LC143:
	.string	"server selection"
.LC144:
	.string	"ATMA"
.LC145:
	.string	"ATM address (unimplemented)"
.LC146:
	.string	"DNAME"
.LC147:
	.string	"Non-terminal DNAME (for IPv6)"
.LC148:
	.string	"TSIG"
.LC149:
	.string	"transaction signature"
.LC150:
	.string	"IXFR"
.LC151:
	.string	"incremental zone transfer"
.LC152:
	.string	"AXFR"
.LC153:
	.string	"zone transfer"
.LC154:
	.string	"MAILB"
	.section	.rodata.str1.8
	.align 8
.LC155:
	.string	"mailbox-related data (deprecated)"
	.section	.rodata.str1.1
.LC156:
	.string	"MAILA"
.LC157:
	.string	"mail agent (deprecated)"
.LC158:
	.string	"NAPTR"
.LC159:
	.string	"URN Naming Authority"
.LC160:
	.string	"KX"
.LC161:
	.string	"Key Exchange"
.LC162:
	.string	"CERT"
.LC163:
	.string	"Certificate"
.LC164:
	.string	"ANY"
.LC165:
	.string	"\"any\""
	.section	.data.rel.ro.local
	.align 32
	.type	__GI___p_type_syms, @object
	.size	__GI___p_type_syms, 1104
__GI___p_type_syms:
	.long	1
	.zero	4
	.quad	.LC7
	.quad	.LC81
	.long	2
	.zero	4
	.quad	.LC6
	.quad	.LC82
	.long	3
	.zero	4
	.quad	.LC83
	.quad	.LC84
	.long	4
	.zero	4
	.quad	.LC85
	.quad	.LC86
	.long	5
	.zero	4
	.quad	.LC87
	.quad	.LC88
	.long	6
	.zero	4
	.quad	.LC89
	.quad	.LC90
	.long	7
	.zero	4
	.quad	.LC91
	.quad	.LC92
	.long	8
	.zero	4
	.quad	.LC93
	.quad	.LC94
	.long	9
	.zero	4
	.quad	.LC95
	.quad	.LC96
	.long	10
	.zero	4
	.quad	.LC97
	.quad	.LC98
	.long	11
	.zero	4
	.quad	.LC99
	.quad	.LC100
	.long	12
	.zero	4
	.quad	.LC101
	.quad	.LC102
	.long	13
	.zero	4
	.quad	.LC103
	.quad	.LC104
	.long	14
	.zero	4
	.quad	.LC105
	.quad	.LC106
	.long	15
	.zero	4
	.quad	.LC107
	.quad	.LC108
	.long	16
	.zero	4
	.quad	.LC109
	.quad	.LC110
	.long	17
	.zero	4
	.quad	.LC111
	.quad	.LC112
	.long	18
	.zero	4
	.quad	.LC113
	.quad	.LC114
	.long	19
	.zero	4
	.quad	.LC115
	.quad	.LC116
	.long	20
	.zero	4
	.quad	.LC117
	.quad	.LC118
	.long	21
	.zero	4
	.quad	.LC119
	.quad	.LC120
	.long	22
	.zero	4
	.quad	.LC121
	.quad	.LC122
	.long	23
	.zero	4
	.quad	.LC123
	.quad	.LC102
	.long	24
	.zero	4
	.quad	.LC124
	.quad	.LC125
	.long	25
	.zero	4
	.quad	.LC126
	.quad	.LC127
	.long	26
	.zero	4
	.quad	.LC128
	.quad	.LC129
	.long	27
	.zero	4
	.quad	.LC130
	.quad	.LC131
	.long	28
	.zero	4
	.quad	.LC132
	.quad	.LC133
	.long	29
	.zero	4
	.quad	.LC134
	.quad	.LC135
	.long	30
	.zero	4
	.quad	.LC136
	.quad	.LC137
	.long	31
	.zero	4
	.quad	.LC138
	.quad	.LC139
	.long	32
	.zero	4
	.quad	.LC140
	.quad	.LC141
	.long	33
	.zero	4
	.quad	.LC142
	.quad	.LC143
	.long	34
	.zero	4
	.quad	.LC144
	.quad	.LC145
	.long	39
	.zero	4
	.quad	.LC146
	.quad	.LC147
	.long	250
	.zero	4
	.quad	.LC148
	.quad	.LC149
	.long	251
	.zero	4
	.quad	.LC150
	.quad	.LC151
	.long	252
	.zero	4
	.quad	.LC152
	.quad	.LC153
	.long	253
	.zero	4
	.quad	.LC154
	.quad	.LC155
	.long	254
	.zero	4
	.quad	.LC156
	.quad	.LC157
	.long	35
	.zero	4
	.quad	.LC158
	.quad	.LC159
	.long	36
	.zero	4
	.quad	.LC160
	.quad	.LC161
	.long	37
	.zero	4
	.quad	.LC162
	.quad	.LC163
	.long	255
	.zero	4
	.quad	.LC164
	.quad	.LC165
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.globl	__p_type_syms
	.set	__p_type_syms,__GI___p_type_syms
	.hidden	__p_update_section_syms
	.globl	__p_update_section_syms
	.section	.rodata.str1.1
.LC166:
	.string	"PREREQUISITE"
.LC167:
	.string	"UPDATE"
.LC168:
	.string	"ADDITIONAL"
	.section	.data.rel.ro.local
	.align 32
	.type	__p_update_section_syms, @object
	.size	__p_update_section_syms, 120
__p_update_section_syms:
	.long	0
	.zero	4
	.quad	.LC1
	.zero	8
	.long	1
	.zero	4
	.quad	.LC166
	.zero	8
	.long	2
	.zero	4
	.quad	.LC167
	.zero	8
	.long	3
	.zero	4
	.quad	.LC168
	.zero	8
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.hidden	__p_default_section_syms
	.globl	__p_default_section_syms
	.section	.rodata.str1.1
.LC169:
	.string	"ANSWER"
.LC170:
	.string	"AUTHORITY"
	.section	.data.rel.ro.local
	.align 32
	.type	__p_default_section_syms, @object
	.size	__p_default_section_syms, 120
__p_default_section_syms:
	.long	0
	.zero	4
	.quad	.LC2
	.zero	8
	.long	1
	.zero	4
	.quad	.LC169
	.zero	8
	.long	2
	.zero	4
	.quad	.LC170
	.zero	8
	.long	3
	.zero	4
	.quad	.LC168
	.zero	8
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.hidden	__GI___p_class_syms
	.globl	__GI___p_class_syms
	.section	.rodata.str1.1
.LC171:
	.string	"HS"
.LC172:
	.string	"HESIOD"
.LC173:
	.string	"NONE"
	.section	.data.rel.ro.local
	.align 32
	.type	__GI___p_class_syms, @object
	.size	__GI___p_class_syms, 168
__GI___p_class_syms:
	.long	1
	.zero	4
	.quad	.LC5
	.zero	8
	.long	3
	.zero	4
	.quad	.LC4
	.zero	8
	.long	4
	.zero	4
	.quad	.LC171
	.zero	8
	.long	4
	.zero	4
	.quad	.LC172
	.zero	8
	.long	255
	.zero	4
	.quad	.LC164
	.zero	8
	.long	254
	.zero	4
	.quad	.LC173
	.zero	8
	.long	1
	.zero	4
	.quad	0
	.quad	0
	.globl	__p_class_syms
	.set	__p_class_syms,__GI___p_class_syms
	.section	.rodata.str1.1
.LC174:
	.string	"IQUERY"
.LC175:
	.string	"CQUERYM"
.LC176:
	.string	"CQUERYU"
.LC177:
	.string	"NOTIFY"
.LC178:
	.string	"6"
.LC179:
	.string	"7"
.LC180:
	.string	"8"
.LC181:
	.string	"9"
.LC182:
	.string	"10"
.LC183:
	.string	"11"
.LC184:
	.string	"12"
.LC185:
	.string	"13"
.LC186:
	.string	"ZONEINIT"
.LC187:
	.string	"ZONEREF"
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	res_opcodes, @object
	.size	res_opcodes, 128
res_opcodes:
	.quad	.LC2
	.quad	.LC174
	.quad	.LC175
	.quad	.LC176
	.quad	.LC177
	.quad	.LC167
	.quad	.LC178
	.quad	.LC179
	.quad	.LC180
	.quad	.LC181
	.quad	.LC182
	.quad	.LC183
	.quad	.LC184
	.quad	.LC185
	.quad	.LC186
	.quad	.LC187
	.globl	_res_opcodes
	.set	_res_opcodes,res_opcodes
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC51:
	.quad	4211551138881019963
	.quad	7959953386440127776
	.align 16
.LC52:
	.quad	2329014255825669152
	.quad	31084746137298294
	.hidden	_ns_flagdata
