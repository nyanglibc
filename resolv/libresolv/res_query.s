	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"res_query.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"answerp == NULL || (void *) *answerp == (void *) answer"
	.section	.rodata.str1.1
.LC2:
	.string	"(hp != NULL) && (hp2 != NULL)"
	.text
	.p2align 4,,15
	.globl	__res_context_query
	.type	__res_context_query, @function
__res_context_query:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbx
	movl	$272, %ebx
	subq	$72, %rsp
	movq	(%rdi), %rax
	cmpl	$439963904, %ecx
	movq	%rsi, -72(%rbp)
	movl	%edx, -56(%rbp)
	movl	%r9d, -92(%rbp)
	movl	$0, -80(%rbp)
	movq	%rax, -64(%rbp)
	movl	$544, %eax
	movq	$0, -88(%rbp)
	cmove	%rax, %rbx
	movl	$0, -76(%rbp)
	movl	%ecx, -52(%rbp)
	leaq	30(%rbx), %rax
	andl	$2032, %eax
	subq	%rax, %rsp
	movl	$1200, %eax
	leaq	15(%rsp), %r14
	andq	$-16, %r14
	cmpq	$0, 16(%rbp)
	movq	%r14, %r15
	movq	%rdi, %r14
	cmove	%r9d, %eax
	movl	%eax, -96(%rbp)
.L3:
	andb	$-16, 3(%r13)
	cmpl	$439963904, -52(%rbp)
	je	.L103
	movl	-52(%rbp), %r8d
	movl	-56(%rbp), %ecx
	xorl	%r9d, %r9d
	movq	-72(%rbp), %rdx
	pushq	%rbx
	xorl	%esi, %esi
	pushq	%r15
	movq	%r14, %rdi
	call	__res_context_mkquery@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	popq	%rdi
	popq	%r8
	jle	.L10
	movq	-64(%rbp), %rax
	testq	$9437184, 8(%rax)
	je	.L8
	movl	-96(%rbp), %r8d
	movl	%r12d, %esi
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	__res_nopt@PLT
	movl	%eax, %r12d
.L9:
	testl	%eax, %eax
	jle	.L104
.L8:
	cmpq	$0, 16(%rbp)
	movq	%r14, %rax
	movq	%r15, %r14
	movq	%rax, %r15
	je	.L15
	movq	16(%rbp), %rax
	cmpq	%r13, (%rax)
	jne	.L105
.L15:
	movl	-92(%rbp), %eax
	movl	-80(%rbp), %r8d
	movl	%r12d, %edx
	movq	-88(%rbp), %rcx
	pushq	48(%rbp)
	movq	%r14, %rsi
	pushq	40(%rbp)
	pushq	32(%rbp)
	movq	%r13, %r9
	pushq	24(%rbp)
	pushq	16(%rbp)
	movq	%r15, %rdi
	pushq	%rax
	call	__res_context_send@PLT
	movl	-76(%rbp), %esi
	addq	$48, %rsp
	movl	%eax, %r12d
	testl	%esi, %esi
	jne	.L106
	testl	%r12d, %r12d
	js	.L107
.L17:
	cmpq	$0, 16(%rbp)
	je	.L18
	movq	16(%rbp), %rax
	cmpq	$0, 24(%rbp)
	movq	(%rax), %r13
	movq	%r13, %rdx
	je	.L19
	movq	40(%rbp), %rax
	cmpl	$11, (%rax)
	jle	.L19
.L47:
	movq	24(%rbp), %rax
	cmpl	$11, %r12d
	movq	(%rax), %rdx
	cmovle	%rdx, %r13
.L19:
	testq	%r13, %r13
	je	.L61
	testq	%rdx, %rdx
	je	.L61
.L20:
	movzbl	3(%r13), %eax
	andl	$15, %eax
	je	.L108
	movzbl	3(%rdx), %ecx
	andl	$15, %ecx
	je	.L109
	cmpb	$2, %al
	je	.L29
	cmpb	$3, %al
	je	.L31
	testb	%al, %al
	je	.L100
.L25:
	movq	-64(%rbp), %rax
	movl	$-1, %r12d
	movl	$3, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movl	-76(%rbp), %eax
	testl	%eax, %eax
	jne	.L101
	movl	$65536, %ebx
.L12:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movl	$1, -76(%rbp)
	jne	.L3
	movq	-64(%rbp), %rax
	movl	$3, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L103:
	movl	-56(%rbp), %ecx
	movq	-72(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	%rbx
	pushq	%r15
	xorl	%esi, %esi
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	%ebx, -100(%rbp)
	call	__res_context_mkquery@PLT
	movl	%eax, %r12d
	testl	%r12d, %r12d
	popq	%r11
	popq	%rax
	jle	.L5
	movq	-64(%rbp), %rax
	movl	-100(%rbp), %r11d
	testq	$9437184, 8(%rax)
	jne	.L110
.L6:
	leal	3(%r12), %eax
	movq	%rbx, %rsi
	movslq	%r12d, %rdx
	andl	$-4, %eax
	subl	%r12d, %eax
	movslq	%eax, %rcx
	subq	%rcx, %rsi
	cmpq	%rsi, %rdx
	jbe	.L111
	movl	$-1, %r12d
.L5:
	movl	-76(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L58
	movq	-64(%rbp), %rax
	movq	%r15, %r14
	movl	$3, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
.L36:
	movq	%r14, %rdi
	call	free@PLT
.L1:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	movzwl	6(%r13), %eax
	testl	%eax, %eax
	jne	.L1
	movzbl	3(%rdx), %ecx
	andl	$15, %ecx
	jne	.L112
	cmpw	$0, 6(%rdx)
	jne	.L1
	movslq	.L27(%rip), %rsi
	leaq	.L27(%rip), %rdi
	addq	%rdi, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L27:
	.long	.L26-.L27
	.long	.L28-.L27
	.long	.L29-.L27
	.long	.L71-.L27
	.long	.L28-.L27
	.text
	.p2align 4,,10
	.p2align 3
.L110:
	movl	%r12d, %esi
	movl	$1200, %r8d
	movl	%ebx, %ecx
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	__res_nopt@PLT
	testl	%eax, %eax
	movl	%eax, %r12d
	movl	-100(%rbp), %r11d
	jns	.L6
	movl	-76(%rbp), %edx
	testl	%edx, %edx
	je	.L58
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r15, %r14
.L11:
	movq	-64(%rbp), %rax
	movl	$3, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L111:
	addl	%r12d, %eax
	movl	-56(%rbp), %ecx
	xorl	%r9d, %r9d
	movslq	%eax, %rdx
	subl	%eax, %ebx
	xorl	%esi, %esi
	leaq	(%r15,%rdx), %rdi
	movq	-72(%rbp), %rdx
	pushq	%rbx
	movl	$28, %r8d
	movl	%r11d, -100(%rbp)
	pushq	%rdi
	movq	%rdi, -88(%rbp)
	movq	%r14, %rdi
	call	__res_context_mkquery@PLT
	testl	%eax, %eax
	movl	%eax, -80(%rbp)
	popq	%r9
	popq	%r10
	jle	.L50
	movq	-64(%rbp), %rax
	movl	-100(%rbp), %r11d
	testq	$9437184, 8(%rax)
	je	.L8
	movq	-88(%rbp), %rdx
	movl	-80(%rbp), %esi
	movl	$1200, %r8d
	movl	%r11d, %ecx
	movq	%r14, %rdi
	call	__res_nopt@PLT
	movl	%eax, -80(%rbp)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L106:
	movq	%r14, %rdi
	call	free@PLT
	testl	%r12d, %r12d
	jns	.L17
.L107:
	movq	-64(%rbp), %rax
	movl	$2, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	cmpq	$0, 24(%rbp)
	je	.L60
	movq	40(%rbp), %rax
	cmpl	$11, (%rax)
	jg	.L47
.L60:
	movq	%r13, %rdx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L109:
	movzwl	6(%rdx), %ecx
	testl	%ecx, %ecx
	jne	.L1
	cmpb	$4, %al
	ja	.L25
	leaq	.L46(%rip), %rsi
	movzbl	%al, %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L46:
	.long	.L100-.L46
	.long	.L35-.L46
	.long	.L29-.L46
	.long	.L33-.L46
	.long	.L35-.L46
	.text
	.p2align 4,,10
	.p2align 3
.L58:
	movl	$131072, %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-80(%rbp), %r12d
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-64(%rbp), %rax
	movl	$-1, %r12d
	movl	$2, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L100:
	movzwl	6(%r13), %eax
.L26:
	testl	%eax, %eax
	jne	.L1
.L34:
	cmpw	$0, 6(%rdx)
	jne	.L1
	movq	-64(%rbp), %rax
	orl	$-1, %r12d
	movl	$4, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$4, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	testl	%eax, %eax
	jne	.L1
	testb	%cl, %cl
	jne	.L25
	movzwl	6(%rdx), %ecx
.L35:
	testl	%ecx, %ecx
	jne	.L1
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L71:
	testl	%eax, %eax
	jne	.L1
.L31:
	testb	%cl, %cl
	jne	.L32
	movzwl	6(%rdx), %ecx
.L33:
	testl	%ecx, %ecx
	jne	.L1
.L32:
	movq	-64(%rbp), %rax
	movl	$-1, %r12d
	movl	$1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$4, %cl
	ja	.L25
	leaq	.L45(%rip), %r8
	movzbl	%cl, %edi
	movslq	(%r8,%rdi,4), %rsi
	addq	%r8, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L45:
	.long	.L34-.L45
	.long	.L28-.L45
	.long	.L29-.L45
	.long	.L71-.L45
	.long	.L28-.L45
	.text
.L105:
	leaq	__PRETTY_FUNCTION__.11568(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$207, %edx
	call	__assert_fail@PLT
.L61:
	leaq	__PRETTY_FUNCTION__.11568(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$239, %edx
	call	__assert_fail@PLT
.L104:
	cmpl	$0, -76(%rbp)
	jne	.L51
	cmpl	$439963904, -52(%rbp)
	movl	%eax, %r12d
	movl	$65536, %ebx
	movl	$131072, %eax
	cmove	%rax, %rbx
	jmp	.L12
.L51:
	movq	%r15, %r14
	movl	%eax, %r12d
	jmp	.L11
	.size	__res_context_query, .-__res_context_query
	.section	.rodata.str1.1
.LC3:
	.string	"%s.%s"
	.text
	.p2align 4,,15
	.type	__res_context_querydomain, @function
__res_context_querydomain:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	movl	%r8d, %r13d
	subq	$1064, %rsp
	movq	(%rdi), %rax
	movq	%rsi, %rdi
	movl	%ecx, 4(%rsp)
	movq	%rax, 8(%rsp)
	call	strlen@PLT
	testq	%r12, %r12
	movq	%rax, %r15
	je	.L120
	movq	%r12, %rdi
	call	strlen@PLT
	leaq	1(%r15,%rax), %rax
	cmpq	$1024, %rax
	ja	.L119
	leaq	16(%rsp), %r15
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r15, %rbx
	call	sprintf@PLT
.L115:
	subq	$8, %rsp
	movq	%r14, %r8
	movl	%r13d, %ecx
	pushq	1168(%rsp)
	pushq	1168(%rsp)
	movq	%rbx, %rsi
	pushq	1168(%rsp)
	pushq	1168(%rsp)
	movq	%rbp, %rdi
	pushq	1168(%rsp)
	movl	1168(%rsp), %r9d
	movl	52(%rsp), %edx
	call	__res_context_query
	addq	$48, %rsp
.L113:
	addq	$1064, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	leaq	-1(%rax), %rcx
	cmpq	$1023, %rcx
	jbe	.L115
	.p2align 4,,10
	.p2align 3
.L119:
	movq	8(%rsp), %rax
	movl	$3, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L113
	.size	__res_context_querydomain, .-__res_context_querydomain
	.p2align 4,,15
	.globl	__res_nquery
	.type	__res_nquery, @function
__res_nquery:
	pushq	%r15
	pushq	%r14
	movl	%r9d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edx, %r12d
	movq	%r8, %r14
	subq	$8, %rsp
	call	__resolv_context_get_override@PLT
	testq	%rax, %rax
	je	.L125
	subq	$8, %rsp
	movq	%rax, %rbx
	movq	%rbp, %rsi
	pushq	$0
	pushq	$0
	movq	%rax, %rdi
	pushq	$0
	pushq	$0
	movl	%r15d, %r9d
	pushq	$0
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	%r12d, %edx
	call	__res_context_query
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L121:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L121
	.size	__res_nquery, .-__res_nquery
	.p2align 4,,15
	.globl	__res_query
	.type	__res_query, @function
__res_query:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %r12d
	movq	%rcx, %r14
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L130
	subq	$8, %rsp
	movq	%rax, %rbx
	movq	%rbp, %rsi
	pushq	$0
	pushq	$0
	movq	%rax, %rdi
	pushq	$0
	pushq	$0
	movl	%r15d, %r9d
	pushq	$0
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	%r12d, %edx
	call	__res_context_query
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L126:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L126
	.size	__res_query, .-__res_query
	.p2align 4,,15
	.globl	__res_nquerydomain
	.type	__res_nquerydomain, @function
__res_nquerydomain:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movl	%r8d, %r14d
	subq	$8, %rsp
	call	__resolv_context_get_override@PLT
	testq	%rax, %rax
	je	.L135
	pushq	$0
	movq	%rax, %rbx
	pushq	$0
	pushq	$0
	pushq	$0
	movq	%rbp, %rsi
	pushq	$0
	movl	104(%rsp), %eax
	movq	%rbx, %rdi
	movq	%r15, %r9
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%r12, %rdx
	pushq	%rax
	call	__res_context_querydomain
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L131:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L131
	.size	__res_nquerydomain, .-__res_nquerydomain
	.p2align 4,,15
	.globl	__res_querydomain
	.type	__res_querydomain, @function
__res_querydomain:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movl	%ecx, %r14d
	subq	$24, %rsp
	movl	%r9d, 12(%rsp)
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	movl	12(%rsp), %r9d
	je	.L140
	pushq	$0
	pushq	$0
	movq	%rax, %rbx
	pushq	$0
	pushq	$0
	movq	%rbp, %rsi
	pushq	$0
	pushq	%r9
	movq	%rax, %rdi
	movq	%r15, %r9
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%r12, %rdx
	call	__res_context_querydomain
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L136:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L136
	.size	__res_querydomain, .-__res_querydomain
	.section	.rodata.str1.1
.LC4:
	.string	"HOSTALIASES"
.LC5:
	.string	"rce"
	.text
	.p2align 4,,15
	.globl	__res_context_hostalias
	.type	__res_context_hostalias, @function
__res_context_hostalias:
	movq	(%rdi), %rdi
	testb	$16, 9(%rdi)
	jne	.L187
	pushq	%r15
	pushq	%r14
	leaq	.LC4(%rip), %rdi
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	subq	$8216, %rsp
	movq	%rcx, 8(%rsp)
	movq	%rdx, (%rsp)
	call	getenv@PLT
	testq	%rax, %rax
	je	.L186
	leaq	.LC5(%rip), %rsi
	movq	%rax, %rdi
	call	fopen@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L186
	xorl	%esi, %esi
	movq	%rax, %rdi
	leaq	16(%rsp), %rbx
	call	setbuf@PLT
	movb	$0, 8207(%rsp)
	.p2align 4,,10
	.p2align 3
.L145:
	movq	%rbp, %rdx
	movl	$8192, %esi
	movq	%rbx, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L146
	movzbl	16(%rsp), %r13d
	testb	%r13b, %r13b
	je	.L146
	call	__ctype_b_loc@PLT
	movq	%rbx, %r15
	movq	%rax, %r12
	movq	(%rax), %rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L147:
	addq	$1, %r15
	movzbl	(%r15), %r13d
	testb	%r13b, %r13b
	je	.L146
.L152:
	movsbq	%r13b, %rdx
	testb	$32, 1(%rax,%rdx,2)
	je	.L147
	movb	$0, (%r15)
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	ns_samename@PLT
	cmpl	$1, %eax
	jne	.L145
	movq	(%r12), %rdi
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L154:
	movq	%rsi, %r15
.L149:
	movsbq	1(%r15), %rdx
	leaq	1(%r15), %rsi
	testb	$32, 1(%rdi,%rdx,2)
	jne	.L154
	testb	%dl, %dl
	je	.L146
	movsbq	2(%r15), %rax
	leaq	2(%r15), %rdx
	testb	%al, %al
	jne	.L185
.L150:
	movq	8(%rsp), %r14
	movq	(%rsp), %rbx
	movb	$0, (%rdx)
	leaq	-1(%r14), %rdx
	movq	%rbx, %rdi
	call	strncpy@PLT
	movb	$0, -1(%rbx,%r14)
	movq	%rbp, %rdi
	call	fclose@PLT
	movq	%rbx, %rax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%rbp, %rdi
	call	fclose@PLT
.L186:
	xorl	%eax, %eax
.L141:
	addq	$8216, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	addq	$1, %rdx
	movsbq	(%rdx), %rax
	testb	%al, %al
	je	.L150
.L185:
	testb	$32, 1(%rdi,%rax,2)
	je	.L188
	jmp	.L150
.L187:
	xorl	%eax, %eax
	ret
	.size	__res_context_hostalias, .-__res_context_hostalias
	.p2align 4,,15
	.globl	__res_context_search
	.type	__res_context_search, @function
__res_context_search:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%r9d, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$1112, %rsp
	movq	errno@gottpoff(%rip), %rax
	movq	(%rdi), %r15
	movl	%edx, 24(%rsp)
	movl	%ecx, 28(%rsp)
	movq	%r8, 32(%rsp)
	movq	1176(%rsp), %r14
	movl	$0, %fs:(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$1, 496(%r15)
	movl	$1, %fs:(%rax)
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	je	.L230
	movq	%rsi, %rax
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L191:
	cmpb	$46, %dl
	sete	%dl
	addq	$1, %rax
	movzbl	%dl, %edx
	addl	%edx, %ebx
	movzbl	(%rax), %edx
	testb	%dl, %dl
	jne	.L191
	cmpq	%rax, %r12
	movl	$0, 20(%rsp)
	jnb	.L192
	cmpb	$46, -1(%rax)
	sete	%al
	movzbl	%al, %eax
	movl	%eax, 20(%rsp)
.L192:
	testl	%ebx, %ebx
	je	.L190
.L193:
	movzbl	392(%r15), %eax
	andl	$15, %eax
	cmpl	%ebx, %eax
	jbe	.L245
	testb	$1, 20(%rsp)
	jne	.L245
	movq	32(%rsp), %rax
	movl	$0, 56(%rsp)
	movl	$-1, 40(%rsp)
	movq	%rax, 8(%rsp)
.L195:
	testl	%ebx, %ebx
	jne	.L199
	movq	8(%r15), %rcx
	testb	$-128, %cl
	je	.L239
	xorl	%r11d, %r11d
	xorl	%esi, %esi
	movl	$0, 44(%rsp)
	movl	$0, 52(%rsp)
	movl	$0, 20(%rsp)
.L302:
	movl	%r13d, %eax
	movq	%r12, %r13
	movq	%rbp, %r12
	movl	%eax, %ebp
	movq	8(%r12), %rax
	movl	%ebx, 48(%rsp)
	movq	%r11, %rbx
	testq	%rax, %rax
	je	.L304
	.p2align 4,,10
	.p2align 3
.L203:
	cmpq	%rbx, 32(%rax)
	jbe	.L246
	movq	24(%rax), %rax
	movq	(%rax,%rbx,8), %rdx
	testq	%rdx, %rdx
	je	.L246
.L206:
	movzbl	(%rdx), %eax
	cmpb	$46, %al
	jne	.L211
	movzbl	1(%rdx), %eax
	addq	$1, %rdx
.L211:
	cmpb	$1, %al
	adcl	$0, 44(%rsp)
	pushq	1200(%rsp)
	pushq	1200(%rsp)
	pushq	1200(%rsp)
	movq	%r13, %rsi
	pushq	%r14
	pushq	1200(%rsp)
	movq	%r12, %rdi
	pushq	%rbp
	movq	56(%rsp), %r9
	movl	76(%rsp), %r8d
	movl	72(%rsp), %ecx
	call	__res_context_querydomain
	addq	$48, %rsp
	cmpl	$0, %eax
	jg	.L244
	jne	.L213
	cmpq	$0, 1192(%rsp)
	je	.L213
	movq	1192(%rsp), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jg	.L214
.L213:
	cmpq	$0, 1168(%rsp)
	je	.L215
	movq	1168(%rsp), %rax
	movq	(%rax), %rax
	cmpq	8(%rsp), %rax
	je	.L215
	movq	%rax, 8(%rsp)
	movl	$65536, %ebp
.L215:
	testq	%r14, %r14
	je	.L216
	movq	1200(%rsp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L305
.L216:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$111, %fs:(%rax)
	je	.L306
	movq	8(%r15), %rcx
	movl	496(%r15), %eax
	movq	%rcx, %rdx
	andl	$512, %edx
	cmpl	$2, %eax
	je	.L219
	cmpl	$4, %eax
	je	.L220
	cmpl	$1, %eax
	je	.L303
.L218:
	movl	%ebp, %eax
	movq	%r12, %rbp
	movq	%r13, %r12
	movl	%eax, %r13d
	xorl	%eax, %eax
	testq	%rdx, %rdx
	movl	48(%rsp), %ebx
	je	.L222
.L205:
	testl	%ebx, %ebx
	jne	.L247
	testb	%al, %al
	jne	.L247
	testl	$16777216, %ecx
	jne	.L226
	movl	44(%rsp), %esi
	orl	%esi, 56(%rsp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L245:
	pushq	1200(%rsp)
	pushq	1200(%rsp)
	xorl	%edx, %edx
	pushq	1200(%rsp)
	pushq	%r14
	movq	%r12, %rsi
	pushq	1200(%rsp)
	pushq	%r13
	movq	%rbp, %rdi
	movq	80(%rsp), %r9
	movl	76(%rsp), %r8d
	movl	72(%rsp), %ecx
	call	__res_context_querydomain
	movl	%eax, 88(%rsp)
	addq	$48, %rsp
	testl	%eax, %eax
	jg	.L189
	testb	$1, 20(%rsp)
	jne	.L189
	testl	%eax, %eax
	jne	.L197
	cmpq	$0, 1192(%rsp)
	je	.L197
	movq	1192(%rsp), %rax
	movl	(%rax), %r11d
	testl	%r11d, %r11d
	jle	.L197
.L214:
	movl	$0, 40(%rsp)
.L189:
	movl	40(%rsp), %eax
	addq	$1112, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L197:
	movq	__h_errno@gottpoff(%rip), %rax
	cmpq	$0, 1168(%rsp)
	movl	%fs:(%rax), %eax
	movl	%eax, 40(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 8(%rsp)
	je	.L198
	movq	1168(%rsp), %rdi
	movq	(%rdi), %rsi
	cmpq	%rax, %rsi
	movl	$65536, %eax
	movq	%rsi, 8(%rsp)
	cmovne	%eax, %r13d
.L198:
	testq	%r14, %r14
	movl	$1, 56(%rsp)
	je	.L195
	movq	1200(%rsp), %rax
	movl	(%rax), %r10d
	testl	%r10d, %r10d
	je	.L195
	movq	(%r14), %rdi
	call	free@PLT
	movq	1184(%rsp), %rax
	movq	$0, (%r14)
	movl	$0, (%rax)
	movq	1200(%rsp), %rax
	movl	$0, (%rax)
	jmp	.L195
	.p2align 4,,10
	.p2align 3
.L230:
	movl	$0, 20(%rsp)
.L190:
	leaq	64(%rsp), %rdx
	movl	$1025, %ecx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	xorl	%ebx, %ebx
	call	__res_context_hostalias
	testq	%rax, %rax
	je	.L193
	subq	$8, %rsp
	movl	%r13d, %r9d
	movq	%rax, %rsi
	pushq	1208(%rsp)
	pushq	1208(%rsp)
	movq	%rbp, %rdi
	pushq	1208(%rsp)
	pushq	%r14
	pushq	1208(%rsp)
	movq	80(%rsp), %r8
	movl	76(%rsp), %ecx
	movl	72(%rsp), %edx
	call	__res_context_query
	movl	%eax, 88(%rsp)
	addq	$48, %rsp
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L199:
	movl	20(%rsp), %eax
	cmpl	$1, %eax
	je	.L239
	movq	8(%r15), %rcx
	testb	$2, %ch
	jne	.L240
	movl	%eax, 52(%rsp)
.L201:
	movl	56(%rsp), %edi
	testl	%edi, %edi
	je	.L307
.L226:
	testq	%r14, %r14
	je	.L227
	movq	1200(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L308
.L227:
	cmpl	$-1, 40(%rsp)
	jne	.L309
	movl	20(%rsp), %edx
	testl	%edx, %edx
	jne	.L310
	movl	52(%rsp), %eax
	testl	%eax, %eax
	je	.L189
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$2, 496(%r15)
	movl	$2, %fs:(%rax)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L239:
	movl	$0, 52(%rsp)
	movl	$0, 20(%rsp)
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L219:
	movq	32(%rsp), %rax
	movzbl	3(%rax), %eax
	movb	%al, 63(%rsp)
	andl	$15, %eax
	cmpb	$2, %al
	jne	.L218
	addl	$1, 52(%rsp)
.L303:
	testq	%rdx, %rdx
	jne	.L223
.L300:
	movl	48(%rsp), %ebx
	movl	%ebp, %eax
	movq	%r12, %rbp
	movq	%r13, %r12
	movl	%eax, %r13d
.L222:
	xorl	%eax, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L220:
	addl	$1, 20(%rsp)
	testq	%rdx, %rdx
	je	.L300
.L223:
	movq	8(%r12), %rax
	addq	$1, %rbx
	movl	$1, %esi
	testq	%rax, %rax
	jne	.L203
.L304:
	movq	(%r12), %rdi
	movq	72(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L246
	testq	%rbx, %rbx
	je	.L206
	xorl	%eax, %eax
.L207:
	addq	$1, %rax
	cmpq	$6, %rax
	movq	72(%rdi,%rax,8), %rdx
	je	.L246
	testq	%rdx, %rdx
	je	.L246
	cmpq	%rbx, %rax
	jne	.L207
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L305:
	movq	(%r14), %rdi
	call	free@PLT
	movq	1184(%rsp), %rax
	movq	$0, (%r14)
	movl	$0, (%rax)
	movq	1200(%rsp), %rax
	movl	$0, (%rax)
	jmp	.L216
	.p2align 4,,10
	.p2align 3
.L246:
	movl	%ebp, %eax
	movq	%r12, %rbp
	movq	%r13, %r12
	movl	%eax, %r13d
	movl	%esi, %eax
	movl	48(%rsp), %ebx
	xorl	$1, %eax
	andl	$1, %eax
	jmp	.L205
	.p2align 4,,10
	.p2align 3
.L307:
	pushq	1200(%rsp)
	pushq	1200(%rsp)
	xorl	%edx, %edx
	pushq	1200(%rsp)
	pushq	%r14
	movq	%r12, %rsi
	pushq	1200(%rsp)
	pushq	%r13
	movq	%rbp, %rdi
	movq	56(%rsp), %r9
	movl	76(%rsp), %r8d
	movl	72(%rsp), %ecx
	call	__res_context_querydomain
	addq	$48, %rsp
	cmpl	$0, %eax
	jle	.L311
.L244:
	movl	%eax, 40(%rsp)
	jmp	.L189
	.p2align 4,,10
	.p2align 3
.L240:
	movl	20(%rsp), %eax
	xorl	%r11d, %r11d
	movl	%eax, %esi
	movl	%eax, 44(%rsp)
	movl	%eax, 52(%rsp)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L308:
	movq	(%r14), %rdi
	call	free@PLT
	movq	1184(%rsp), %rax
	movq	$0, (%r14)
	movl	$0, (%rax)
	movq	1200(%rsp), %rax
	movl	$0, (%rax)
	jmp	.L227
.L311:
	jne	.L226
	cmpq	$0, 1192(%rsp)
	je	.L226
	movq	1192(%rsp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	jg	.L214
	jmp	.L226
.L306:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$2, 496(%r15)
	movl	$-1, 40(%rsp)
	movl	$2, %fs:(%rax)
	jmp	.L189
.L309:
	movl	40(%rsp), %eax
	movq	__h_errno@gottpoff(%rip), %rdi
	movl	$-1, 40(%rsp)
	movl	%eax, 496(%r15)
	movl	%eax, %fs:(%rdi)
	jmp	.L189
.L310:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$4, 496(%r15)
	movl	$4, %fs:(%rax)
	jmp	.L189
.L247:
	movl	44(%rsp), %edi
	orl	%edi, 56(%rsp)
	jmp	.L201
	.size	__res_context_search, .-__res_context_search
	.p2align 4,,15
	.globl	__res_nsearch
	.type	__res_nsearch, @function
__res_nsearch:
	pushq	%r15
	pushq	%r14
	movl	%r9d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edx, %r12d
	movq	%r8, %r14
	subq	$8, %rsp
	call	__resolv_context_get_override@PLT
	testq	%rax, %rax
	je	.L316
	subq	$8, %rsp
	movq	%rax, %rbx
	movq	%rbp, %rsi
	pushq	$0
	pushq	$0
	movq	%rax, %rdi
	pushq	$0
	pushq	$0
	movl	%r15d, %r9d
	pushq	$0
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	%r12d, %edx
	call	__res_context_search
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L312:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L316:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L312
	.size	__res_nsearch, .-__res_nsearch
	.p2align 4,,15
	.globl	__res_search
	.type	__res_search, @function
__res_search:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r15d
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %r12d
	movq	%rcx, %r14
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L321
	subq	$8, %rsp
	movq	%rax, %rbx
	movq	%rbp, %rsi
	pushq	$0
	pushq	$0
	movq	%rax, %rdi
	pushq	$0
	pushq	$0
	movl	%r15d, %r9d
	pushq	$0
	movq	%r14, %r8
	movl	%r13d, %ecx
	movl	%r12d, %edx
	call	__res_context_search
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L317:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L317
	.size	__res_search, .-__res_search
	.p2align 4,,15
	.globl	__res_hostalias
	.type	__res_hostalias, @function
__res_hostalias:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	subq	$8, %rsp
	call	__resolv_context_get_override@PLT
	testq	%rax, %rax
	je	.L326
	movq	%rax, %rbx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movq	%r13, %rcx
	movq	%r12, %rdx
	call	__res_context_hostalias
	movq	%rbx, %rdi
	movq	%rax, %rbp
	call	__resolv_context_put@PLT
.L322:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L326:
	movq	__resp@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L322
	.size	__res_hostalias, .-__res_hostalias
	.p2align 4,,15
	.globl	__hostalias
	.type	__hostalias, @function
__hostalias:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L331
	leaq	abuf.11751(%rip), %rdx
	movq	%rax, %rbx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movl	$1025, %ecx
	call	__res_context_hostalias
	movq	%rbx, %rdi
	movq	%rax, %rbp
	call	__resolv_context_put@PLT
.L327:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L331:
	movq	__resp@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L327
	.size	__hostalias, .-__hostalias
	.local	abuf.11751
	.comm	abuf.11751,1025,32
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11568, @object
	.size	__PRETTY_FUNCTION__.11568, 20
__PRETTY_FUNCTION__.11568:
	.string	"__res_context_query"
