	.text
	.p2align 4,,15
	.globl	__GI_ns_get16
	.hidden	__GI_ns_get16
	.type	__GI_ns_get16, @function
__GI_ns_get16:
	movzwl	(%rdi), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	ret
	.size	__GI_ns_get16, .-__GI_ns_get16
	.globl	ns_get16
	.set	ns_get16,__GI_ns_get16
	.globl	__ns_get16
	.set	__ns_get16,ns_get16
	.p2align 4,,15
	.globl	__GI_ns_get32
	.hidden	__GI_ns_get32
	.type	__GI_ns_get32, @function
__GI_ns_get32:
	movl	(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	ret
	.size	__GI_ns_get32, .-__GI_ns_get32
	.globl	ns_get32
	.set	ns_get32,__GI_ns_get32
	.globl	__ns_get32
	.set	__ns_get32,ns_get32
	.p2align 4,,15
	.globl	__GI_ns_put16
	.hidden	__GI_ns_put16
	.type	__GI_ns_put16, @function
__GI_ns_put16:
	rolw	$8, %di
	movw	%di, (%rsi)
	ret
	.size	__GI_ns_put16, .-__GI_ns_put16
	.globl	ns_put16
	.set	ns_put16,__GI_ns_put16
	.p2align 4,,15
	.globl	__GI_ns_put32
	.hidden	__GI_ns_put32
	.type	__GI_ns_put32, @function
__GI_ns_put32:
	bswap	%edi
	movl	%edi, (%rsi)
	ret
	.size	__GI_ns_put32, .-__GI_ns_put32
	.globl	ns_put32
	.set	ns_put32,__GI_ns_put32
