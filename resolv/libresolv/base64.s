	.text
	.p2align 4,,15
	.globl	__b64_ntop
	.type	__b64_ntop, @function
__b64_ntop:
	cmpq	$2, %rsi
	pushq	%rbp
	pushq	%rbx
	jbe	.L11
	movzbl	(%rdi), %r8d
	movzbl	1(%rdi), %eax
	leaq	3(%rdi), %r9
	movzbl	2(%rdi), %r11d
	subq	$3, %rsi
	leaq	Base64(%rip), %r10
	movl	%r8d, %ebx
	movl	%eax, %edi
	sall	$4, %r8d
	shrb	$4, %dil
	andl	$48, %r8d
	sall	$2, %eax
	addl	%edi, %r8d
	movl	%r11d, %edi
	andl	$60, %eax
	shrb	$6, %dil
	shrb	$2, %bl
	andl	$63, %r11d
	addl	%eax, %edi
	cmpq	$3, %rcx
	movl	$4, %eax
	ja	.L3
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	movzbl	(%r9), %r8d
	movzbl	1(%r9), %edi
	addq	$3, %r9
	movzbl	-1(%r9), %r11d
	addq	$4, %rax
	subq	$3, %rsi
	movl	%r8d, %ebx
	movl	%edi, %ebp
	sall	$4, %r8d
	shrb	$4, %bpl
	andl	$48, %r8d
	sall	$2, %edi
	addl	%ebp, %r8d
	movl	%r11d, %ebp
	andl	$60, %edi
	shrb	$6, %bpl
	shrb	$2, %bl
	andl	$63, %r11d
	addl	%ebp, %edi
	cmpq	%rax, %rcx
	jb	.L5
.L3:
	andl	$127, %r8d
	andl	$127, %edi
	andl	$63, %ebx
	movzbl	(%r10,%r8), %r8d
	movzbl	(%r10,%rbx), %ebx
	movb	%r8b, -3(%rdx,%rax)
	movzbl	(%r10,%rdi), %r8d
	movq	%r11, %rdi
	andl	$63, %edi
	cmpq	$2, %rsi
	movb	%bl, -4(%rdx,%rax)
	movzbl	(%r10,%rdi), %edi
	movb	%r8b, -2(%rdx,%rax)
	movb	%dil, -1(%rdx,%rax)
	ja	.L6
.L2:
	testq	%rsi, %rsi
	jne	.L20
.L7:
	cmpq	%rcx, %rax
	jnb	.L5
	popq	%rbx
	movb	$0, (%rdx,%rax)
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	cmpq	$2, %rsi
	movzbl	(%r9), %edi
	jne	.L13
	movzbl	1(%r9), %r8d
	movl	%r8d, %r10d
	sall	$2, %r8d
	shrb	$4, %r10b
	andl	$60, %r8d
.L8:
	movl	%edi, %r9d
	leaq	4(%rax), %r11
	sall	$4, %edi
	andl	$48, %edi
	shrb	$2, %r9b
	addl	%r10d, %edi
	cmpq	%rcx, %r11
	ja	.L5
	leaq	Base64(%rip), %r10
	andl	$63, %r9d
	andl	$127, %edi
	movzbl	(%r10,%rdi), %edi
	movzbl	(%r10,%r9), %r9d
	movb	%dil, 1(%rdx,%rax)
	movb	%r9b, (%rdx,%rax)
	leaq	2(%rdx,%rax), %rdi
	addq	$3, %rax
	cmpq	$1, %rsi
	je	.L21
	movzbl	%r8b, %r8d
	movzbl	(%r10,%r8), %esi
	movb	%sil, (%rdi)
.L10:
	movb	$61, (%rdx,%rax)
	movq	%r11, %rax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L21:
	movb	$61, (%rdi)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rdi, %r9
	xorl	%eax, %eax
	jmp	.L2
	.size	__b64_ntop, .-__b64_ntop
	.p2align 4,,15
	.globl	__b64_pton
	.type	__b64_pton, @function
__b64_pton:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%rdx, 8(%rsp)
	movl	$0, 4(%rsp)
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	1(%r12), %r13
	movsbl	-1(%r13), %esi
	testl	%esi, %esi
	movl	%esi, %r14d
	je	.L125
	movl	%esi, (%rsp)
	call	__ctype_b_loc@PLT
	movsbq	%r14b, %rdi
	movq	(%rax), %r14
	movq	%rax, %r15
	testb	$32, 1(%r14,%rdi,2)
	jne	.L54
	movl	(%rsp), %esi
	cmpl	$61, %esi
	je	.L25
	leaq	Base64(%rip), %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L26
	movl	4(%rsp), %edx
	cmpl	$2, %edx
	je	.L56
	cmpl	$3, %edx
	je	.L29
	cmpl	$1, %edx
	je	.L30
	movq	%r13, %r12
.L27:
	testq	%rbx, %rbx
	je	.L31
	movslq	%ebp, %r14
	cmpq	8(%rsp), %r14
	jnb	.L26
	leaq	Base64(%rip), %rcx
	leaq	1(%r12), %r13
	subq	%rcx, %rax
	sall	$2, %eax
	movb	%al, (%rbx,%r14)
	movsbq	(%r12), %rsi
	testl	%esi, %esi
	je	.L26
	movq	(%r15), %rcx
	testb	$32, 1(%rcx,%rsi,2)
	jne	.L60
	cmpl	$61, %esi
	je	.L26
	leaq	Base64(%rip), %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L26
.L44:
	leaq	1(%r14), %rcx
	cmpq	8(%rsp), %rcx
	jnb	.L26
	leaq	Base64(%rip), %rdi
	subq	%rdi, %rax
	movq	%rax, %rcx
	salq	$4, %rax
	sarq	$4, %rcx
	orb	%cl, (%rbx,%r14)
	movb	%al, 1(%rbx,%r14)
.L33:
	movsbq	0(%r13), %rsi
	addl	$1, %ebp
	leaq	1(%r13), %r12
	testl	%esi, %esi
	je	.L26
	movq	(%r15), %r14
	testb	$32, 1(%r14,%rsi,2)
	jne	.L59
	cmpl	$61, %esi
	je	.L43
	leaq	Base64(%rip), %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L26
.L28:
	testq	%rbx, %rbx
	je	.L35
	movslq	%ebp, %rdx
	leaq	1(%rdx), %rsi
	cmpq	8(%rsp), %rsi
	jnb	.L26
	leaq	Base64(%rip), %rcx
	subq	%rcx, %rax
	movq	%rax, %rsi
	salq	$6, %rax
	sarq	$2, %rsi
	orb	%sil, (%rbx,%rdx)
	movb	%al, 1(%rbx,%rdx)
.L35:
	movsbq	(%r12), %rsi
	addl	$1, %ebp
	leaq	1(%r12), %r13
	testl	%esi, %esi
	je	.L26
	movq	(%r15), %r14
	testb	$32, 1(%r14,%rsi,2)
	jne	.L58
	cmpl	$61, %esi
	je	.L42
	leaq	Base64(%rip), %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L26
.L29:
	testq	%rbx, %rbx
	je	.L37
	movslq	%ebp, %rcx
	cmpq	8(%rsp), %rcx
	jnb	.L26
	leaq	Base64(%rip), %rdi
	subq	%rdi, %rax
	orb	%al, (%rbx,%rcx)
.L37:
	movsbq	0(%r13), %rsi
	addl	$1, %ebp
	leaq	1(%r13), %r12
	testl	%esi, %esi
	je	.L22
	movq	(%r15), %r14
	testb	$32, 1(%r14,%rsi,2)
	jne	.L61
	cmpl	$61, %esi
	je	.L26
	leaq	Base64(%rip), %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L27
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$-1, %ebp
.L22:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	movq	%r13, %r12
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L30:
	testq	%rbx, %rbx
	je	.L33
	movslq	%ebp, %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L56:
	movq	%r13, %r12
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$0, 4(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L59:
	movl	$2, 4(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%r13, %r12
	movl	$3, 4(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L60:
	movq	%r13, %r12
	movl	$1, 4(%rsp)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L31:
	movsbq	(%r12), %rsi
	leaq	1(%r12), %r13
	testl	%esi, %esi
	je	.L26
	testb	$32, 1(%r14,%rsi,2)
	jne	.L60
	cmpl	$61, %esi
	je	.L26
	leaq	Base64(%rip), %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L33
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L125:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	je	.L22
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L25:
	movl	4(%rsp), %ecx
	leaq	2(%r12), %rdx
	movsbq	1(%r12), %rax
	cmpl	$2, %ecx
	je	.L63
	cmpl	$3, %ecx
	je	.L124
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L51:
	testb	$32, 1(%r14,%rax,2)
	je	.L26
	addq	$1, %rdx
	movsbq	-1(%rdx), %rax
.L124:
	testl	%eax, %eax
	jne	.L51
	testq	%rbx, %rbx
	je	.L22
	movslq	%ebp, %rax
	cmpb	$0, (%rbx,%rax)
	je	.L22
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L42:
	leaq	2(%r12), %rdx
	movsbq	1(%r12), %rax
	jmp	.L124
.L43:
	movsbl	1(%r13), %eax
	leaq	2(%r13), %rcx
.L52:
	testl	%eax, %eax
	jne	.L48
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L126:
	addq	$1, %rcx
	movsbl	-1(%rcx), %eax
	testl	%eax, %eax
	je	.L26
.L48:
	movslq	%eax, %rdx
	testb	$32, 1(%r14,%rdx,2)
	jne	.L126
	cmpl	$61, %eax
	jne	.L26
	leaq	1(%rcx), %rdx
	movsbq	(%rcx), %rax
	jmp	.L124
.L63:
	movq	%rdx, %rcx
	jmp	.L52
	.size	__b64_pton, .-__b64_pton
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	Base64, @object
	.size	Base64, 65
Base64:
	.string	"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
