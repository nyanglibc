	.text
	.p2align 4,,15
	.globl	__res_close
	.type	__res_close, @function
__res_close:
	movq	__resp@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdi
	testb	$1, 8(%rdi)
	jne	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%esi, %esi
	jmp	__res_iclose@PLT
	.size	__res_close, .-__res_close
