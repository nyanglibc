	.text
	.p2align 4,,15
	.globl	__GI_ns_samedomain
	.hidden	__GI_ns_samedomain
	.type	__GI_ns_samedomain, @function
__GI_ns_samedomain:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	movq	%rbx, %rdi
	movq	%rax, %r13
	movq	%rax, %rbp
	call	strlen@PLT
	testq	%r13, %r13
	movq	%rax, %rcx
	movq	%rax, %rdx
	je	.L2
	cmpb	$46, -1(%r12,%r13)
	leaq	-1(%r13), %r8
	je	.L34
.L2:
	testq	%rcx, %rcx
	movl	$1, %eax
	je	.L1
	cmpb	$46, -1(%rbx,%rcx)
	leaq	-1(%rcx), %r8
	je	.L35
.L7:
	xorl	%eax, %eax
	cmpq	%rdx, %rbp
	jb	.L1
	je	.L36
	subl	%edx, %ebp
	cmpl	$1, %ebp
	jle	.L1
	movslq	%ebp, %rdi
	cmpb	$46, -1(%r12,%rdi)
	jne	.L1
	subl	$2, %ebp
	xorl	%esi, %esi
	movslq	%ebp, %rcx
	movl	%ebp, %ebp
	leaq	-1(%r12,%rcx), %r8
	leaq	(%r12,%rcx), %rax
	subq	%rbp, %r8
	.p2align 4,,10
	.p2align 3
.L14:
	cmpb	$92, (%rax)
	jne	.L25
	subq	$1, %rax
	xorl	$1, %esi
	cmpq	%r8, %rax
	movl	%esi, %ecx
	jne	.L14
.L13:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jne	.L1
	addq	%r12, %rdi
	movq	%rbx, %rsi
	call	strncasecmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movl	%ecx, %eax
	subl	$2, %eax
	js	.L8
	cltq
	cmpb	$92, (%rbx,%rax)
	jne	.L8
	leal	-3(%rcx), %ecx
	xorl	%esi, %esi
	movslq	%ecx, %rcx
	subq	%rcx, %rax
	addq	%rbx, %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	subq	$1, %rcx
	cmpb	$92, (%rax,%rcx)
	jne	.L11
	movl	%edi, %esi
.L9:
	movl	%esi, %edi
	xorl	$1, %edi
	testl	%ecx, %ecx
	jns	.L10
.L11:
	testl	%esi, %esi
	je	.L7
.L8:
	testq	%r8, %r8
	je	.L21
	movq	%r8, %rdx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L34:
	movl	%r13d, %edi
	movl	%r13d, %eax
	subl	$2, %edi
	js	.L16
	movslq	%edi, %rdi
	cmpb	$92, (%r12,%rdi)
	jne	.L16
	subl	$3, %eax
	xorl	%esi, %esi
	cltq
	subq	%rax, %rdi
	addq	%r12, %rdi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	subq	$1, %rax
	cmpb	$92, (%rdi,%rax)
	jne	.L5
	movl	%r9d, %esi
.L3:
	movl	%esi, %r9d
	xorl	$1, %r9d
	testl	%eax, %eax
	jns	.L4
.L5:
	testl	%esi, %esi
	cmove	%r13, %r8
	movq	%r8, %rbp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L36:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	strncasecmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	movl	%esi, %ecx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r8, %rbp
	jmp	.L2
.L21:
	movl	$1, %eax
	jmp	.L1
	.size	__GI_ns_samedomain, .-__GI_ns_samedomain
	.globl	ns_samedomain
	.set	ns_samedomain,__GI_ns_samedomain
	.p2align 4,,15
	.globl	__GI_ns_makecanon
	.hidden	__GI_ns_makecanon
	.type	__GI_ns_makecanon, @function
__GI_ns_makecanon:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	strlen@PLT
	movq	%rax, %rbx
	addq	$2, %rax
	cmpq	%r13, %rax
	ja	.L54
	leaq	1(%rbx), %r13
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movq	%r13, %rdx
	call	memcpy@PLT
	testq	%rbx, %rbx
	je	.L40
	cmpb	$46, -1(%rbp,%rbx)
	je	.L41
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L58:
	cmpb	$92, -2(%rbp,%rbx)
	je	.L56
.L44:
	movb	$0, -1(%rbp,%rbx)
	cmpb	$46, -1(%rbp,%rax)
	jne	.L57
	movq	%rax, %rbx
.L41:
	cmpq	$1, %rbx
	leaq	-1(%rbx), %rax
	jne	.L58
	movb	$0, 0(%rbp)
.L40:
	movq	%rbp, %rax
	movl	$1, %ebx
.L42:
	movb	$46, (%rax)
	movb	$0, 0(%rbp,%rbx)
	xorl	%eax, %eax
.L37:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	cmpq	$2, %rbx
	je	.L59
	cmpb	$92, -3(%rbp,%rbx)
	je	.L44
	leaq	0(%rbp,%rbx), %rax
	addq	$1, %rbx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L57:
	addq	%rbp, %rax
	jmp	.L42
.L59:
	leaq	2(%rbp), %rax
	movl	$3, %ebx
	jmp	.L42
.L54:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L37
.L55:
	leaq	0(%rbp,%rbx), %rax
	movq	%r13, %rbx
	jmp	.L42
	.size	__GI_ns_makecanon, .-__GI_ns_makecanon
	.globl	ns_makecanon
	.set	ns_makecanon,__GI_ns_makecanon
	.p2align 4,,15
	.globl	__GI_ns_samename
	.hidden	__GI_ns_samename
	.type	__GI_ns_samename, @function
__GI_ns_samename:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movl	$1025, %edx
	subq	$2080, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__GI_ns_makecanon
	testl	%eax, %eax
	js	.L63
	leaq	1040(%rsp), %rbp
	movl	$1025, %edx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	call	__GI_ns_makecanon
	testl	%eax, %eax
	js	.L63
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
.L60:
	addq	$2080, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movl	$-1, %eax
	jmp	.L60
	.size	__GI_ns_samename, .-__GI_ns_samename
	.globl	ns_samename
	.set	ns_samename,__GI_ns_samename
	.p2align 4,,15
	.globl	ns_subdomain
	.type	ns_subdomain, @function
ns_subdomain:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__GI_ns_samename
	xorl	%edx, %edx
	cmpl	$1, %eax
	je	.L65
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_ns_samedomain
	xorl	%edx, %edx
	testl	%eax, %eax
	setne	%dl
.L65:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	ns_subdomain, .-ns_subdomain
