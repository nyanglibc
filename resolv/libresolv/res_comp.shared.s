	.text
	.p2align 4,,15
	.type	binary_hnok, @function
binary_hnok:
	movabsq	$1125899906850809, %rsi
.L5:
	movzbl	(%rdi), %ecx
	testq	%rcx, %rcx
	je	.L6
	addq	$1, %rdi
	addq	%rdi, %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	btq	%rdx, %rsi
	jnc	.L8
.L3:
	addq	$1, %rdi
	cmpq	%rdi, %rcx
	jbe	.L5
.L4:
	movzbl	(%rdi), %edx
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L3
	subl	$45, %edx
	cmpb	$50, %dl
	jbe	.L11
.L8:
	xorl	%eax, %eax
	ret
.L6:
	movl	$1, %eax
	ret
	.size	binary_hnok, .-binary_hnok
	.p2align 4,,15
	.globl	__GI___dn_expand
	.hidden	__GI___dn_expand
	.type	__GI___dn_expand, @function
__GI___dn_expand:
	pushq	%rbx
	movslq	%r8d, %r8
	movq	%rcx, %rbx
	call	__GI_ns_name_uncompress
	testl	%eax, %eax
	jle	.L12
	cmpb	$46, (%rbx)
	je	.L15
.L12:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movb	$0, (%rbx)
	popq	%rbx
	ret
	.size	__GI___dn_expand, .-__GI___dn_expand
	.globl	__dn_expand
	.set	__dn_expand,__GI___dn_expand
	.p2align 4,,15
	.globl	__GI___dn_comp
	.hidden	__GI___dn_comp
	.type	__GI___dn_comp, @function
__GI___dn_comp:
	movslq	%edx, %rdx
	jmp	__GI_ns_name_compress
	.size	__GI___dn_comp, .-__GI___dn_comp
	.globl	__dn_comp
	.set	__dn_comp,__GI___dn_comp
	.p2align 4,,15
	.globl	__GI___dn_skipname
	.hidden	__GI___dn_skipname
	.type	__GI___dn_skipname, @function
__GI___dn_skipname:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %rdi
	call	__GI_ns_name_skip
	cmpl	$-1, %eax
	je	.L17
	movl	8(%rsp), %eax
	subl	%ebx, %eax
.L17:
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI___dn_skipname, .-__GI___dn_skipname
	.globl	__dn_skipname
	.set	__dn_skipname,__GI___dn_skipname
	.p2align 4,,15
	.globl	__GI___res_hnok
	.hidden	__GI___res_hnok
	.type	__GI___res_hnok, @function
__GI___res_hnok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L24
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L32
	movq	%rdi, %rdx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L40:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L32
.L26:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L40
.L24:
	pushq	%rbx
	movl	$255, %edx
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__GI_ns_name_pton
	movl	%eax, %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	js	.L23
	cmpb	$0, (%rsp)
	je	.L28
	cmpb	$45, 1(%rsp)
	je	.L23
.L28:
	movq	%rbx, %rdi
	call	binary_hnok
	movzbl	%al, %eax
.L23:
	addq	$256, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	xorl	%eax, %eax
	ret
	.size	__GI___res_hnok, .-__GI___res_hnok
	.globl	__res_hnok
	.set	__res_hnok,__GI___res_hnok
	.p2align 4,,15
	.globl	__res_ownok
	.type	__res_ownok, @function
__res_ownok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L46
	subl	$33, %eax
	movq	%rdi, %rdx
	cmpb	$93, %al
	jbe	.L47
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L69:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L73
.L47:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L69
.L46:
	pushq	%rbx
	movl	$255, %edx
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__GI_ns_name_pton
	testl	%eax, %eax
	js	.L49
	movzbl	(%rsp), %eax
	testb	%al, %al
	je	.L51
	movzbl	1(%rsp), %edx
	cmpb	$45, %dl
	je	.L49
	cmpb	$1, %al
	jne	.L51
	cmpb	$42, %dl
	je	.L75
.L51:
	movq	%rbx, %rdi
	call	binary_hnok
	addq	$256, %rsp
	movzbl	%al, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%eax, %eax
.L45:
	addq	$256, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	ret
.L75:
	leaq	2(%rbx), %rdi
	call	binary_hnok
	movzbl	%al, %eax
	jmp	.L45
	.size	__res_ownok, .-__res_ownok
	.p2align 4,,15
	.globl	__res_mailok
	.type	__res_mailok, @function
__res_mailok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L77
	subl	$33, %eax
	movq	%rdi, %rdx
	cmpb	$93, %al
	jbe	.L78
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L93:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L96
.L78:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L93
.L77:
	pushq	%rbx
	movl	$255, %edx
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__GI_ns_name_pton
	testl	%eax, %eax
	js	.L80
	movzbl	(%rsp), %edx
	movl	$1, %eax
	testb	%dl, %dl
	je	.L76
	leaq	1(%rbx,%rdx), %rdi
	cmpb	$0, (%rdi)
	jne	.L98
.L80:
	xorl	%eax, %eax
.L76:
	addq	$256, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	xorl	%eax, %eax
	ret
.L98:
	call	binary_hnok
	movzbl	%al, %eax
	jmp	.L76
	.size	__res_mailok, .-__res_mailok
	.p2align 4,,15
	.globl	__GI___res_dnok
	.hidden	__GI___res_dnok
	.type	__GI___res_dnok, @function
__GI___res_dnok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L100
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L105
	movq	%rdi, %rdx
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L112:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L105
.L102:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L112
.L100:
	subq	$264, %rsp
	movl	$255, %edx
	movq	%rsp, %rsi
	call	__GI_ns_name_pton
	notl	%eax
	addq	$264, %rsp
	shrl	$31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	xorl	%eax, %eax
	ret
	.size	__GI___res_dnok, .-__GI___res_dnok
	.globl	__res_dnok
	.set	__res_dnok,__GI___res_dnok
	.p2align 4,,15
	.globl	__GI___putlong
	.hidden	__GI___putlong
	.type	__GI___putlong, @function
__GI___putlong:
	movl	%edi, %edi
	jmp	__GI_ns_put32
	.size	__GI___putlong, .-__GI___putlong
	.globl	__putlong
	.set	__putlong,__GI___putlong
	.p2align 4,,15
	.globl	__GI___putshort
	.hidden	__GI___putshort
	.type	__GI___putshort, @function
__GI___putshort:
	movzwl	%di, %edi
	jmp	__GI_ns_put16
	.size	__GI___putshort, .-__GI___putshort
	.globl	__putshort
	.set	__putshort,__GI___putshort
	.p2align 4,,15
	.globl	_getlong
	.type	_getlong, @function
_getlong:
	subq	$8, %rsp
	call	__GI_ns_get32
	addq	$8, %rsp
	ret
	.size	_getlong, .-_getlong
	.p2align 4,,15
	.globl	_getshort
	.type	_getshort, @function
_getshort:
	subq	$8, %rsp
	call	__GI_ns_get16
	addq	$8, %rsp
	ret
	.size	_getshort, .-_getshort
