	.text
	.p2align 4,,15
	.globl	__res_enable_icmp
	.type	__res_enable_icmp, @function
__res_enable_icmp:
	subq	$24, %rsp
	cmpl	$2, %edi
	movl	%esi, %eax
	movl	$1, 12(%rsp)
	je	.L3
	cmpl	$10, %edi
	je	.L4
	movq	errno@gottpoff(%rip), %rax
	movl	$97, %fs:(%rax)
	movl	$-1, %eax
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	12(%rsp), %rcx
	movl	$4, %r8d
	movl	$25, %edx
	movl	$41, %esi
	movl	%eax, %edi
	call	setsockopt@PLT
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	12(%rsp), %rcx
	movl	$4, %r8d
	movl	$11, %edx
	xorl	%esi, %esi
	movl	%eax, %edi
	call	setsockopt@PLT
	addq	$24, %rsp
	ret
	.size	__res_enable_icmp, .-__res_enable_icmp
