	.text
	.p2align 4,,15
	.type	prune_origin, @function
prune_origin:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movzbl	(%rdi), %edx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	testb	%dl, %dl
	je	.L5
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%rbp, %rbp
	je	.L12
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_ns_samename
	cmpl	$1, %eax
	je	.L24
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L12
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	cmpb	$46, %dl
	je	.L9
	movl	%ecx, %edx
	movq	%rax, %rbx
	testb	%dl, %dl
	je	.L25
.L12:
	cmpb	$92, %dl
	movzbl	1(%rbx), %ecx
	leaq	1(%rbx), %rax
	jne	.L8
	testb	%cl, %cl
	je	.L9
	movzbl	2(%rbx), %edx
	leaq	2(%rbx), %rax
	movq	%rax, %rbx
	testb	%dl, %dl
	jne	.L12
.L25:
	xorl	%ecx, %ecx
.L9:
	movl	%ecx, %edx
	movq	%rax, %rbx
	testb	%dl, %dl
	jne	.L13
.L5:
	movq	%rbx, %rax
	subq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L24:
	movq	%rbx, %rax
	xorl	%edx, %edx
	subq	%r12, %rax
	cmpq	%r12, %rbx
	seta	%dl
	popq	%rbx
	subq	%rdx, %rax
	popq	%rbp
	popq	%r12
	ret
	.size	prune_origin, .-prune_origin
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ns_print.c"
.LC1:
	.string	"len <= *buflen"
	.section	.text.unlikely,"ax",@progbits
	.type	addlen.part.0, @function
addlen.part.0:
	leaq	__PRETTY_FUNCTION__.5542(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$733, %edx
	call	__assert_fail@PLT
	.size	addlen.part.0, .-addlen.part.0
	.text
	.p2align 4,,15
	.type	addstr, @function
addstr:
	cmpq	%rsi, (%rcx)
	jbe	.L35
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rsi
	movq	(%rdx), %rdi
	movq	%rcx, %rbp
	movq	%rbx, %rdx
	call	memcpy@PLT
	movq	0(%rbp), %rax
	cmpq	%rax, %rbx
	ja	.L36
	movq	(%r12), %rdx
	subq	%rbx, %rax
	addq	%rbx, %rdx
	movq	%rdx, (%r12)
	movq	%rax, 0(%rbp)
	xorl	%eax, %eax
	movb	$0, (%rdx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	movq	errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	movl	$-1, %eax
	ret
.L36:
	call	addlen.part.0
	.size	addstr, .-addstr
	.section	.rodata.str1.1
.LC2:
	.string	"\""
.LC3:
	.string	"\n\"\\"
.LC4:
	.string	"\\"
	.text
	.p2align 4,,15
	.type	charstr, @function
charstr:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	leaq	.LC2(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movl	$1, %esi
	movq	%rdx, %r12
	movq	%rcx, %rbp
	subq	$24, %rsp
	movq	(%rcx), %rax
	movq	%rax, (%rsp)
	movq	(%rdx), %rax
	movq	%rax, 8(%rsp)
	call	addstr
	testl	%eax, %eax
	js	.L38
	cmpq	%rbx, %r14
	jnb	.L46
	movzbl	(%r14), %edx
	movq	%rdx, %rax
	leaq	1(%r14,%rdx), %rdx
	cmpq	%rdx, %rbx
	jb	.L46
	testb	%al, %al
	leaq	1(%r14), %rbx
	je	.L47
	subl	$1, %eax
	leaq	.LC3(%rip), %r15
	leaq	2(%r14,%rax), %r13
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %rbx
	cmpq	%rbx, %r13
	je	.L39
.L43:
	movzbl	(%rbx), %esi
	movq	%r15, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L42
	leaq	.LC4(%rip), %rdi
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L38
.L42:
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	addstr
	testl	%eax, %eax
	jns	.L49
.L38:
	movq	errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	movq	8(%rsp), %rax
	movq	%rax, (%r12)
	movq	(%rsp), %rax
	movq	%rax, 0(%rbp)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%r14, %r13
.L39:
	leaq	.LC2(%rip), %rdi
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L38
	addq	$24, %rsp
	movl	%r13d, %eax
	popq	%rbx
	subl	%r14d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L47:
	movq	%rbx, %r13
	jmp	.L39
	.size	charstr, .-charstr
	.p2align 4,,15
	.type	addname, @function
addname:
	pushq	%r15
	pushq	%r14
	addq	%rdi, %rsi
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%r8, %rbx
	movq	%r9, %r15
	subq	$24, %rsp
	movq	(%r9), %rax
	movq	(%r8), %r14
	movq	(%rdx), %rdx
	movl	%eax, %r8d
	movq	%r14, %rcx
	movq	%rax, 8(%rsp)
	call	__GI___dn_expand
	testl	%eax, %eax
	js	.L52
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movl	%eax, %r13d
	call	prune_origin
	movq	(%rbx), %rsi
	movq	(%r15), %rcx
	cmpb	$0, (%rsi)
	jne	.L53
.L54:
	leaq	2(%rax), %rdx
	cmpq	%rcx, %rdx
	ja	.L52
	movb	$46, (%rsi,%rax)
	movq	(%rbx), %rcx
	leaq	1(%rax), %rdx
	movb	$0, 1(%rcx,%rax)
	movq	(%r15), %rcx
.L56:
	movslq	%r13d, %r8
	addq	%r8, 0(%rbp)
	cmpq	%rcx, %rdx
	ja	.L71
	movq	(%rbx), %rax
	subq	%rdx, %rcx
	addq	%rdx, %rax
	movq	%rax, (%rbx)
	movq	%rcx, (%r15)
	movb	$0, (%rax)
	movl	%edx, %eax
.L51:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	testq	%rax, %rax
	jne	.L55
	cmpq	$1, %rcx
	jbe	.L52
	movb	$64, (%rsi)
	movq	(%rbx), %rax
	movl	$1, %edx
	movb	$0, 1(%rax)
	movq	(%r15), %rcx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L55:
	testq	%r12, %r12
	je	.L57
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L57
	cmpb	$46, %dl
	je	.L63
	cmpb	$0, 1(%r12)
	je	.L63
	cmpb	$0, (%rsi,%rax)
	jne	.L63
	.p2align 4,,10
	.p2align 3
.L57:
	cmpb	$46, -1(%rsi,%rax)
	jne	.L54
.L63:
	movq	%rax, %rdx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L52:
	movq	errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	movq	8(%rsp), %rax
	movq	%r14, (%rbx)
	movq	%rax, (%r15)
	movl	$-1, %eax
	jmp	.L51
.L71:
	call	addlen.part.0
	.size	addname, .-addname
	.section	.rodata.str1.1
.LC5:
	.string	"  "
.LC6:
	.string	"\t"
	.text
	.p2align 4,,15
	.type	addtab, @function
addtab:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	subq	$8, %rsp
	testl	%edx, %edx
	jne	.L73
	leaq	-1(%rsi), %rax
	cmpq	%rdi, %rax
	jbe	.L73
	subq	%rdi, %rsi
	subq	$1, %rsi
	shrq	$3, %rsi
	testl	%esi, %esi
	movl	%esi, %ebx
	js	.L79
	movq	(%r8), %r15
	movq	(%rcx), %r14
	leaq	.LC6(%rip), %r13
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L77:
	subl	$1, %ebx
	cmpl	$-1, %ebx
	je	.L79
.L78:
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	call	addstr
	testl	%eax, %eax
	jns	.L77
	movq	%r15, (%r12)
	movq	%r14, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	movl	$-1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	.LC5(%rip), %rdi
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movl	$2, %esi
	call	addstr
	addq	$8, %rsp
	sarl	$31, %eax
	popq	%rbx
	orl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	addtab, .-addtab
	.section	.rodata.str1.1
.LC7:
	.string	"\n\t\t"
.LC8:
	.string	" "
.LC9:
	.string	"RR format error"
.LC10:
	.string	" ("
.LC11:
	.string	""
.LC12:
	.string	"\t\t\t"
.LC13:
	.string	"."
.LC14:
	.string	"@\t\t\t"
.LC15:
	.string	" %s %s"
.LC16:
	.string	" (\n"
.LC17:
	.string	"\t\t\t\t\t"
.LC18:
	.string	"%lu"
.LC19:
	.string	"; serial\n"
.LC20:
	.string	"; refresh\n"
.LC21:
	.string	"; retry\n"
.LC22:
	.string	"; expiry\n"
.LC23:
	.string	" )"
.LC24:
	.string	"; minimum\n"
.LC25:
	.string	"%u "
.LC26:
	.string	"%u %u "
.LC27:
	.string	"%u %u %u "
.LC28:
	.string	" %u ( "
.LC29:
	.string	"\n\t\t\t\t"
.LC30:
	.string	"%d "
.LC31:
	.string	")"
.LC32:
	.string	"%d %d %d "
.LC33:
	.string	"record too long to print"
.LC34:
	.string	"%lu "
.LC35:
	.string	"%d"
.LC36:
	.string	"%u bytes"
.LC37:
	.string	"unknown RR type %d"
.LC38:
	.string	"\\# %u%s\t; %s"
.LC39:
	.string	"%02x "
	.text
	.p2align 4,,15
	.globl	__GI_ns_sprintrrf
	.hidden	__GI_ns_sprintrrf
	.type	__GI_ns_sprintrrf, @function
__GI_ns_sprintrrf:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %rbp
	movl	%r8d, %ebx
	subq	$8408, %rsp
	movq	8496(%rsp), %rax
	movq	8480(%rsp), %rdi
	movq	%rsi, 40(%rsp)
	movl	%ecx, 20(%rsp)
	movq	%rax, 32(%rsp)
	movq	8464(%rsp), %rax
	addq	8472(%rsp), %rax
	testq	%rdi, %rdi
	movq	%rax, 24(%rsp)
	je	.L84
	movq	%rdx, %rsi
	call	__GI_ns_samename
	cmpl	$1, %eax
	je	.L261
.L84:
	movq	8488(%rsp), %rsi
	movq	%r12, %rdi
	call	prune_origin
	cmpb	$0, (%r12)
	movq	%rax, %r14
	je	.L262
	testl	%eax, %eax
	jne	.L90
	leaq	8496(%rsp), %rax
	leaq	8504(%rsp), %rcx
	leaq	.LC14(%rip), %rdi
	movl	$4, %esi
	movq	%rax, %rdx
	movq	%rcx, (%rsp)
	movq	%rax, 8(%rsp)
	call	addstr
	testl	%eax, %eax
	js	.L160
.L91:
	xorl	%r12d, %r12d
.L86:
	movq	8504(%rsp), %rdx
	movq	8496(%rsp), %rsi
	movq	%rbp, %rdi
	call	__GI_ns_format_ttl
	testl	%eax, %eax
	movl	%eax, %r13d
	js	.L160
	movslq	%eax, %rdx
	movq	8504(%rsp), %rax
	cmpq	%rax, %rdx
	ja	.L119
	subq	%rdx, %rax
	movl	%ebx, %edi
	addq	%rdx, 8496(%rsp)
	movq	%rax, 8504(%rsp)
	leaq	96(%rsp), %rbp
	call	__GI___p_type
	movl	20(%rsp), %edi
	movq	%rax, %r14
	call	__GI___p_class
	leaq	.LC15(%rip), %rsi
	movq	%r14, %rcx
	movq	%rax, %rdx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	movq	%rsi, %r14
	call	addstr
	testl	%eax, %eax
	js	.L160
	leal	0(%r13,%r14), %edi
	movq	(%rsp), %r8
	movq	8(%rsp), %rcx
	movl	%r12d, %edx
	movl	$16, %esi
	movslq	%edi, %rdi
	call	addtab
	testl	%eax, %eax
	js	.L160
	cmpl	$250, %ebx
	ja	.L96
	leaq	.L98(%rip), %rcx
	movl	%ebx, %edx
	movslq	(%rcx,%rdx,4), %rax
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L98:
	.long	.L96-.L98
	.long	.L97-.L98
	.long	.L99-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L99-.L98
	.long	.L100-.L98
	.long	.L99-.L98
	.long	.L99-.L98
	.long	.L99-.L98
	.long	.L96-.L98
	.long	.L101-.L98
	.long	.L99-.L98
	.long	.L102-.L98
	.long	.L103-.L98
	.long	.L104-.L98
	.long	.L214-.L98
	.long	.L103-.L98
	.long	.L104-.L98
	.long	.L106-.L98
	.long	.L102-.L98
	.long	.L104-.L98
	.long	.L107-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L108-.L98
	.long	.L96-.L98
	.long	.L109-.L98
	.long	.L110-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L111-.L98
	.long	.L96-.L98
	.long	.L112-.L98
	.long	.L96-.L98
	.long	.L113-.L98
	.long	.L114-.L98
	.long	.L99-.L98
	.long	.L96-.L98
	.long	.L115-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L96-.L98
	.long	.L116-.L98
	.long	.L117-.L98
	.text
	.p2align 4,,10
	.p2align 3
.L90:
	movslq	%eax, %r13
	leaq	8496(%rsp), %rax
	leaq	8504(%rsp), %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rax, %rdx
	movq	%rcx, (%rsp)
	movq	%rax, 8(%rsp)
	call	addstr
	testl	%eax, %eax
	js	.L160
	cmpq	$0, 8488(%rsp)
	je	.L92
	movq	8488(%rsp), %rax
	movzbl	(%rax), %eax
	testb	%al, %al
	je	.L92
	cmpb	$46, %al
	je	.L93
	movq	8488(%rsp), %rax
	cmpb	$0, 1(%rax)
	je	.L93
	cmpb	$0, (%r12,%r13)
	jne	.L93
.L92:
	cmpb	$46, -1(%r12,%r13)
	je	.L93
.L94:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	leaq	.LC13(%rip), %rdi
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	leal	1(%r14), %r8d
	movslq	%r8d, %r13
.L93:
	movq	(%rsp), %r8
	movq	8(%rsp), %rcx
	xorl	%edx, %edx
	movl	$24, %esi
	movq	%r13, %rdi
	call	addtab
	testl	%eax, %eax
	movl	%eax, %r12d
	jns	.L86
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$-1, %eax
.L83:
	addq	$8408, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	leaq	8504(%rsp), %rax
	movq	%rax, (%rsp)
	leaq	8496(%rsp), %rax
	movq	%rax, 8(%rsp)
	jmp	.L94
.L261:
	leaq	8496(%rsp), %rax
	leaq	8504(%rsp), %rcx
	leaq	.LC12(%rip), %rdi
	movl	$3, %esi
	movq	%rax, %rdx
	movq	%rcx, (%rsp)
	movq	%rax, 8(%rsp)
	call	addstr
	testl	%eax, %eax
	jns	.L91
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	leaq	8464(%rsp), %rdx
	movq	8488(%rsp), %rcx
	movq	40(%rsp), %rsi
	movq	%r15, %rdi
	movq	%r14, %r9
	movq	%rbx, %r8
	call	addname
	testl	%eax, %eax
	js	.L160
	leaq	.LC8(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rax
	leaq	8(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	movq	8464(%rsp), %rdx
	cltq
	leaq	2(%rdx,%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	movq	8464(%rsp), %rax
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	movq	8496(%rsp), %rdi
	leaq	.LC35(%rip), %rsi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	sprintf@PLT
	addq	$2, 8464(%rsp)
.L248:
	movq	8496(%rsp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpq	8504(%rsp), %rax
	ja	.L119
	addq	%rbx, %rax
	.p2align 4,,10
	.p2align 3
.L245:
	subl	32(%rsp), %eax
	jmp	.L83
.L97:
	movq	8464(%rsp), %rsi
	movq	24(%rsp), %rdx
	leaq	.LC9(%rip), %r8
	subq	%rsi, %rdx
	cmpq	$4, 8472(%rsp)
	je	.L263
	.p2align 4,,10
	.p2align 3
.L123:
	cmpq	$0, 8472(%rsp)
	leaq	.LC10(%rip), %rax
	leaq	.LC11(%rip), %rcx
	leaq	.LC38(%rip), %rsi
	movq	%rbp, %rdi
	cmovne	%rax, %rcx
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %r12
	cmpq	%r12, 24(%rsp)
	jbe	.L170
	.p2align 4,,10
	.p2align 3
.L180:
	movq	24(%rsp), %r13
	movl	$16, %eax
	movl	$2314, %esi
	movw	%si, 0(%rbp)
	movb	$0, 2(%rbp)
	subq	%r12, %r13
	cmpq	$16, %r13
	cmovg	%rax, %r13
	testl	%r13d, %r13d
	movl	%r13d, 20(%rsp)
	jle	.L184
	leal	-1(%r13), %ebx
	leaq	2(%rbp), %r14
	xorl	%r15d, %r15d
	addq	$1, %rbx
	.p2align 4,,10
	.p2align 3
.L172:
	movq	8464(%rsp), %rax
	leaq	.LC39(%rip), %rsi
	movq	%r14, %rdi
	addq	$3, %r14
	movzbl	(%rax,%r15), %edx
	xorl	%eax, %eax
	addq	$1, %r15
	call	sprintf@PLT
	cmpq	%r15, %rbx
	jne	.L172
.L171:
	movq	%r14, %rbx
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	subq	%rbp, %rbx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	call	addstr
	testl	%eax, %eax
	js	.L160
	cmpl	$15, 20(%rsp)
	jg	.L264
	movq	(%rsp), %r14
	movq	8(%rsp), %r15
	leaq	.LC31(%rip), %rdi
	movl	$1, %esi
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	leaq	1(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$48, %esi
	call	addtab
	testl	%eax, %eax
	js	.L160
	movl	20(%rsp), %ecx
	movl	$8251, %edx
	movb	$0, 2(%rbp)
	movw	%dx, 0(%rbp)
	leaq	2(%rbp), %rsi
	testl	%ecx, %ecx
	jle	.L176
.L175:
	leal	-1(%r13), %ebx
	movq	8464(%rsp), %r15
	leaq	2(%rbp), %r14
	xorl	%r13d, %r13d
	addq	$1, %rbx
	.p2align 4,,10
	.p2align 3
.L179:
	movzbl	(%r15,%r13), %r12d
	movl	$46, %eax
	testb	$-128, %r12b
	jne	.L178
	call	__ctype_b_loc@PLT
	movq	(%rax), %rax
	movzbl	%r12b, %edx
	testb	$64, 1(%rax,%rdx,2)
	movl	$46, %eax
	cmovne	%r12d, %eax
.L178:
	movb	%al, (%r14,%r13)
	addq	$1, %r13
	cmpq	%rbx, %r13
	jne	.L179
	leaq	(%r14,%r13), %rsi
.L176:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	subq	%rbp, %rsi
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movslq	20(%rsp), %r12
	addq	8464(%rsp), %r12
	cmpq	24(%rsp), %r12
	movq	%r12, 8464(%rsp)
	jb	.L180
.L170:
	movl	8496(%rsp), %eax
	jmp	.L245
.L99:
	movq	(%rsp), %r9
	movq	8(%rsp), %r8
.L256:
	movq	8488(%rsp), %rcx
	movq	40(%rsp), %rsi
	leaq	8464(%rsp), %rdx
	movq	%r15, %rdi
	call	addname
	testl	%eax, %eax
	js	.L160
.L236:
	movq	8496(%rsp), %rax
	jmp	.L245
.L100:
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	leaq	8464(%rsp), %rdx
	movq	40(%rsp), %r12
	movq	8488(%rsp), %rcx
	movq	%r15, %rdi
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%r12, %rsi
	call	addname
	testl	%eax, %eax
	js	.L160
	leaq	.LC8(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8488(%rsp), %rcx
	leaq	8464(%rsp), %rdx
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	addname
	testl	%eax, %eax
	js	.L160
	leaq	.LC16(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$3, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	movq	24(%rsp), %rdx
	leaq	.LC9(%rip), %r8
	subq	%rdi, %rdx
	cmpq	$20, %rdx
	jne	.L123
	call	__GI_ns_get32
	movq	(%rsp), %r14
	movq	8(%rsp), %r15
	leaq	.LC17(%rip), %rdi
	movl	$5, %esi
	addq	$4, 8464(%rsp)
	movq	%rax, %rbx
	movq	%r14, %rcx
	movq	%r15, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	leaq	.LC18(%rip), %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	movslq	%eax, %rbx
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$16, %esi
	movq	%rbx, %rdi
	call	addtab
	testl	%eax, %eax
	js	.L160
	leaq	.LC19(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$9, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	call	__GI_ns_get32
	leaq	.LC17(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$5, %esi
	addq	$4, 8464(%rsp)
	movq	%rax, %rbx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8504(%rsp), %rdx
	movq	%rbx, %rdi
	movq	8496(%rsp), %rsi
	call	__GI_ns_format_ttl
	testl	%eax, %eax
	js	.L160
	movslq	%eax, %rdi
	movq	8504(%rsp), %rax
	cmpq	%rax, %rdi
	ja	.L119
	subq	%rdi, %rax
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$16, %esi
	addq	%rdi, 8496(%rsp)
	movq	%rax, 8504(%rsp)
	call	addtab
	testl	%eax, %eax
	js	.L160
	leaq	.LC20(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$10, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	call	__GI_ns_get32
	leaq	.LC17(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$5, %esi
	addq	$4, 8464(%rsp)
	movq	%rax, %rbx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8504(%rsp), %rdx
	movq	%rbx, %rdi
	movq	8496(%rsp), %rsi
	call	__GI_ns_format_ttl
	testl	%eax, %eax
	js	.L160
	movslq	%eax, %rdi
	movq	8504(%rsp), %rax
	cmpq	%rax, %rdi
	ja	.L119
	subq	%rdi, %rax
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$16, %esi
	addq	%rdi, 8496(%rsp)
	movq	%rax, 8504(%rsp)
	call	addtab
	testl	%eax, %eax
	js	.L160
	leaq	.LC21(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$8, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	call	__GI_ns_get32
	leaq	.LC17(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$5, %esi
	addq	$4, 8464(%rsp)
	movq	%rax, %rbx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8504(%rsp), %rdx
	movq	%rbx, %rdi
	movq	8496(%rsp), %rsi
	call	__GI_ns_format_ttl
	testl	%eax, %eax
	js	.L160
	movslq	%eax, %rdi
	movq	8504(%rsp), %rax
	cmpq	%rax, %rdi
	ja	.L119
	subq	%rdi, %rax
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$16, %esi
	addq	%rdi, 8496(%rsp)
	movq	%rax, 8504(%rsp)
	call	addtab
	testl	%eax, %eax
	js	.L160
	leaq	.LC22(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$9, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	call	__GI_ns_get32
	leaq	.LC17(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$5, %esi
	addq	$4, 8464(%rsp)
	movq	%rax, %rbx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8504(%rsp), %rdx
	movq	%rbx, %rdi
	movq	8496(%rsp), %rsi
	call	__GI_ns_format_ttl
	testl	%eax, %eax
	js	.L160
	movslq	%eax, %rbx
	movq	8504(%rsp), %rax
	cmpq	%rax, %rbx
	ja	.L119
	leaq	.LC23(%rip), %rdi
	subq	%rbx, %rax
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$2, %esi
	addq	%rbx, 8496(%rsp)
	movq	%rax, 8504(%rsp)
	call	addstr
	testl	%eax, %eax
	js	.L160
	xorl	%edx, %edx
	movq	%r14, %r8
	movq	%r15, %rcx
	movl	$16, %esi
	movq	%rbx, %rdi
	call	addtab
	testl	%eax, %eax
	js	.L160
	leaq	.LC24(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movl	$10, %esi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L101:
	cmpq	$4, 8472(%rsp)
	movq	8464(%rsp), %rsi
	ja	.L145
.L244:
	movq	24(%rsp), %rdx
	leaq	.LC9(%rip), %r8
	subq	%rsi, %rdx
	jmp	.L123
.L102:
	movq	24(%rsp), %r15
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movq	8464(%rsp), %rdi
	movq	%r15, %rsi
	call	charstr
	testl	%eax, %eax
	js	.L160
	jne	.L122
	movq	%r15, %rdx
	leaq	.LC9(%rip), %r8
	subq	8464(%rsp), %rdx
	jmp	.L123
.L109:
	cmpq	$16, 8472(%rsp)
	movq	8464(%rsp), %rsi
	jne	.L244
	movl	8504(%rsp), %ecx
	movq	8496(%rsp), %rdx
	movl	$10, %edi
	call	inet_ntop@PLT
	jmp	.L248
.L214:
	movq	24(%rsp), %r14
	movq	8(%rsp), %r12
	leaq	.LC8(%rip), %rbx
	movq	(%rsp), %r13
.L105:
	movq	8464(%rsp), %rdi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L266:
	movq	8464(%rsp), %rdx
	je	.L159
	cltq
	leaq	(%rdx,%rax), %rdi
	cmpq	%r14, %rdi
	movq	%rdi, 8464(%rsp)
	jb	.L265
.L130:
	cmpq	%rdi, %r14
	jbe	.L236
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	charstr
	testl	%eax, %eax
	jns	.L266
	jmp	.L160
.L106:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movq	24(%rsp), %rsi
	movq	8464(%rsp), %rdi
	call	charstr
	testl	%eax, %eax
	js	.L160
.L246:
	jne	.L236
.L155:
	movq	24(%rsp), %rdx
	leaq	.LC9(%rip), %r8
	subq	8464(%rsp), %rdx
	jmp	.L123
.L107:
	leaq	208(%rsp), %r13
	movq	8464(%rsp), %rsi
	movl	8472(%rsp), %edi
	movq	%r13, %rdx
	call	inet_nsap_ntoa@PLT
	movq	%r13, %rsi
.L132:
	movl	(%rsi), %edx
	addq	$4, %rsi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L132
	jmp	.L254
.L108:
	cmpq	$1, 8472(%rsp)
	movq	8464(%rsp), %rdi
	ja	.L127
.L139:
	movq	24(%rsp), %rdx
	leaq	.LC9(%rip), %r8
	subq	%rdi, %rdx
	jmp	.L123
.L103:
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	movq	%r14, %r9
.L259:
	movq	40(%rsp), %rbp
	movq	8488(%rsp), %rcx
	leaq	8464(%rsp), %rdx
	movq	%rbx, %r8
	movq	%r15, %rdi
	movq	%rbp, %rsi
	call	addname
	testl	%eax, %eax
	js	.L160
	leaq	.LC8(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8488(%rsp), %rcx
	leaq	8464(%rsp), %rdx
	movq	%r14, %r9
	movq	%rbx, %r8
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	addname
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L113:
	movq	8464(%rsp), %rdi
	leaq	48(%rsp), %r12
	call	__GI_ns_get16
	movl	%eax, %ebx
	movq	8464(%rsp), %rax
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	movq	8464(%rsp), %rdx
	leaq	.LC32(%rip), %rsi
	movq	%r12, %rdi
	leaq	3(%rdx), %rcx
	movq	%rcx, 8464(%rsp)
	movzbl	2(%rdx), %r8d
	movl	%eax, %ecx
	movl	%ebx, %edx
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movslq	%eax, %rsi
	movq	%r12, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	movq	24(%rsp), %rsi
	movabsq	$6148914691236517206, %rdx
	subq	%rdi, %rsi
	leaq	0(,%rsi,4), %rcx
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movq	%rdx, %rax
	subq	%rcx, %rax
	addl	$4, %eax
	cmpl	$6144, %eax
	jbe	.L152
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	leaq	.LC33(%rip), %rdi
	movl	$24, %esi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	$0, 8472(%rsp)
	movq	8464(%rsp), %rdx
	jne	.L267
.L159:
	movq	24(%rsp), %rax
	leaq	.LC9(%rip), %r8
	subq	%rdx, %rax
	movq	%rax, %rdx
	jmp	.L123
.L104:
	cmpq	$1, 8472(%rsp)
	movq	8464(%rsp), %rdi
	jbe	.L139
	call	__GI_ns_get16
	leaq	.LC25(%rip), %rsi
	movl	%eax, %edx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	addq	$2, 8464(%rsp)
	call	sprintf@PLT
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
.L251:
	movq	%r14, %r9
	movq	%rbx, %r8
	jmp	.L256
.L111:
	movq	8464(%rsp), %rdi
	movq	24(%rsp), %rdx
	leaq	.LC9(%rip), %r8
	subq	%rdi, %rdx
	cmpq	$5, 8472(%rsp)
	jbe	.L123
	call	__GI_ns_get16
	movl	%eax, %ebp
	movq	8464(%rsp), %rax
	leaq	208(%rsp), %r12
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	movl	%eax, %ebx
	movq	8464(%rsp), %rax
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	leaq	.LC27(%rip), %rsi
	movl	%eax, %r8d
	movl	%ebx, %ecx
	movl	%ebp, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$2, 8464(%rsp)
	call	sprintf@PLT
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	movslq	%eax, %rsi
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	jns	.L251
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L112:
	cmpq	$3, 8472(%rsp)
	movq	8464(%rsp), %rdi
	jbe	.L139
	call	__GI_ns_get16
	movl	%eax, %ebx
	movq	8464(%rsp), %rax
	leaq	208(%rsp), %r12
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	leaq	.LC26(%rip), %rsi
	movl	%eax, %ecx
	movl	%ebx, %edx
	movq	%r12, %rdi
	xorl	%eax, %eax
	addq	$2, 8464(%rsp)
	call	sprintf@PLT
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	movslq	%eax, %rsi
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	24(%rsp), %rsi
	movq	8464(%rsp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	charstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	je	.L139
	cltq
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	addq	%rax, %rdi
	movl	$1, %esi
	movq	%rdi, 8464(%rsp)
	leaq	.LC8(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	24(%rsp), %rsi
	movq	8464(%rsp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	charstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	je	.L139
	cltq
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	addq	%rax, %rdi
	movl	$1, %esi
	movq	%rdi, 8464(%rsp)
	leaq	.LC8(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	24(%rsp), %rsi
	movq	8464(%rsp), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	charstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	je	.L139
	cltq
	addq	%rax, %rdi
	movq	%rdi, 8464(%rsp)
.L260:
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	leaq	.LC8(%rip), %rdi
	movl	$1, %esi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	jns	.L251
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L110:
	leaq	208(%rsp), %r13
	movq	8464(%rsp), %rdi
	movq	%r13, %rsi
	call	__GI___loc_ntoa
	movq	%r13, %rsi
.L136:
	movl	(%rsi), %edx
	addq	$4, %rsi
	leal	-16843009(%rdx), %eax
	notl	%edx
	andl	%edx, %eax
	andl	$-2139062144, %eax
	je	.L136
.L254:
	movl	%eax, %edx
	movq	%r13, %rdi
	shrl	$16, %edx
	testl	$32896, %eax
	cmove	%edx, %eax
	leaq	2(%rsi), %rdx
	movl	%eax, %ecx
	cmove	%rdx, %rsi
	addb	%al, %cl
	movq	8(%rsp), %rdx
	movq	(%rsp), %rcx
	sbbq	$3, %rsi
	subq	%r13, %rsi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L115:
	movl	20(%rsp), %edx
	leaq	.LC36(%rip), %rsi
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L116:
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	leaq	8464(%rsp), %rdx
	movq	8488(%rsp), %rcx
	movq	40(%rsp), %rsi
	movq	%r15, %rdi
	movq	%r14, %r9
	movq	%rbx, %r8
	call	addname
	testl	%eax, %eax
	js	.L160
	leaq	.LC8(%rip), %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	movq	%rbx, %r15
	call	__GI_ns_get32
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	addq	$4, 8464(%rsp)
	call	sprintf@PLT
	movq	%r14, %rcx
	movslq	%eax, %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	call	__GI_ns_get32
	leaq	.LC34(%rip), %rsi
	movq	%rax, %rdx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	addq	$4, 8464(%rsp)
	call	sprintf@PLT
	movq	%r14, %rcx
	movslq	%eax, %rsi
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdi
	call	__GI_ns_get16
	movl	%eax, %ebx
	movq	8464(%rsp), %rax
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	movl	%eax, %r12d
	movq	8464(%rsp), %rax
	leaq	2(%rax), %rdi
	movq	%rdi, 8464(%rsp)
	call	__GI_ns_get16
	leaq	.LC27(%rip), %rsi
	movl	%eax, %r8d
	movl	%r12d, %ecx
	movl	%ebx, %edx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	addq	$2, 8464(%rsp)
	call	sprintf@PLT
	movq	%r14, %rcx
	movslq	%eax, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	208(%rsp), %r12
	leaq	.LC37(%rip), %rdx
	movl	%ebx, %ecx
	movl	$40, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	call	snprintf@PLT
	movq	24(%rsp), %rdx
	movq	%r12, %r8
	subq	8464(%rsp), %rdx
	jmp	.L123
	.p2align 4,,10
	.p2align 3
.L264:
	movl	$8251, %eax
	movb	$0, 2(%rbp)
	movw	%ax, 0(%rbp)
	jmp	.L175
	.p2align 4,,10
	.p2align 3
.L184:
	leaq	2(%rbp), %r14
	jmp	.L171
.L152:
	leaq	208(%rsp), %r13
	movl	%eax, %ecx
	movq	%r13, %rdx
	call	__GI___b64_ntop
	testl	%eax, %eax
	movl	%eax, %r12d
	js	.L155
	cmpl	$15, %eax
	jg	.L268
	testl	%eax, %eax
	je	.L236
	leaq	.LC8(%rip), %rbp
	movl	$1, %esi
.L181:
	movq	%r13, %rbx
	leal	(%r12,%r13), %r14d
	movq	(%rsp), %r15
	jmp	.L158
.L269:
	movl	%r14d, %eax
	movl	$48, %edx
	movq	%r15, %rcx
	subl	%ebx, %eax
	movq	%rbx, %rdi
	cmpl	$48, %eax
	cmovg	%edx, %eax
	movq	8(%rsp), %rdx
	movslq	%eax, %rsi
	call	addstr
	testl	%eax, %eax
	js	.L160
	addq	$48, %rbx
	movl	%ebx, %eax
	subl	%r13d, %eax
	cmpl	%eax, %r12d
	jle	.L157
	movq	%rbp, %rdi
	call	strlen@PLT
	movq	%rax, %rsi
.L158:
	movq	8(%rsp), %rdx
	movq	%r15, %rcx
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	jns	.L269
	jmp	.L160
.L127:
	call	__GI_ns_get16
	leaq	.LC25(%rip), %rsi
	movl	%eax, %edx
	movq	%rbp, %rdi
	xorl	%eax, %eax
	addq	$2, 8464(%rsp)
	call	sprintf@PLT
	movq	(%rsp), %r14
	movq	8(%rsp), %rbx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	movq	%r14, %rcx
	movq	%rbx, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	%r14, %r9
	jmp	.L259
.L145:
	movq	8496(%rsp), %rdx
	movl	8504(%rsp), %ecx
	movl	$2, %edi
	call	inet_ntop@PLT
	movq	8496(%rsp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	8504(%rsp), %rdx
	cmpq	%rdx, %rax
	ja	.L119
	subq	%rax, %rdx
	addq	%rax, %rbx
	movq	8464(%rsp), %rax
	movq	%rdx, 8504(%rsp)
	movq	%rbx, 8496(%rsp)
	leaq	.LC28(%rip), %rsi
	movq	%rbp, %rdi
	leaq	4(%rax), %rdx
	movq	%rdx, 8464(%rsp)
	movzbl	4(%rax), %edx
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	8(%rsp), %r15
	movq	(%rsp), %rcx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rax
	leaq	.LC30(%rip), %rbx
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	addq	$1, %rax
	movq	%rax, 8464(%rsp)
.L146:
	cmpq	%rax, 24(%rsp)
	jbe	.L270
	leaq	1(%rax), %rdx
	movq	%rdx, 8464(%rsp)
	movzbl	(%rax), %r13d
	.p2align 4,,10
	.p2align 3
.L149:
	testb	$-128, %r13b
	je	.L147
	testl	%r14d, %r14d
	jne	.L148
	movq	(%rsp), %rcx
	leaq	.LC29(%rip), %rdi
	movq	%r15, %rdx
	movl	$5, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movl	$10, %r14d
.L148:
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rcx
	movslq	%eax, %rsi
	movq	%r15, %rdx
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	subl	$1, %r14d
.L147:
	addl	$1, %r12d
	addl	%r13d, %r13d
	testb	$7, %r12b
	jne	.L149
	movq	8464(%rsp), %rax
	jmp	.L146
.L122:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	leaq	.LC8(%rip), %rdi
	movl	$1, %esi
	cltq
	addq	%rax, 8464(%rsp)
	call	addstr
	testl	%eax, %eax
	js	.L160
	cmpl	$20, %ebx
	movq	8464(%rsp), %rdi
	jne	.L124
	cmpq	%rdi, 24(%rsp)
	je	.L236
.L124:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movq	24(%rsp), %rsi
	call	charstr
	testl	%eax, %eax
	jns	.L246
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L267:
	movzbl	(%rdx), %edx
	leaq	.LC30(%rip), %rsi
	movq	%rbp, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	movslq	%eax, %rsi
	movq	%rbp, %rdi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movq	8464(%rsp), %rdx
	movzbl	(%rdx), %r14d
	cmpl	$128, %r14d
	jg	.L159
	addq	$1, %rdx
	cmpl	$128, %r14d
	movq	%rdx, 8464(%rsp)
	jne	.L271
.L161:
	cmpq	%rdx, 24(%rsp)
	jbe	.L159
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L265:
	movq	%r13, %rcx
	movq	%r12, %rdx
	movl	$1, %esi
	movq	%rbx, %rdi
	call	addstr
	testl	%eax, %eax
	jns	.L105
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L263:
	movl	8504(%rsp), %ecx
	movq	8496(%rsp), %rdx
	movl	$2, %edi
	call	inet_ntop@PLT
	jmp	.L248
.L157:
	cmpl	$15, %r12d
	jle	.L236
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	leaq	.LC23(%rip), %rdi
	movl	$2, %esi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L268:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	leaq	.LC10(%rip), %rdi
	movl	$2, %esi
	call	addstr
	testl	%eax, %eax
	js	.L160
	movl	$3, %esi
	leaq	.LC7(%rip), %rbp
	jmp	.L181
.L271:
	movl	%r14d, %eax
	sarl	$3, %eax
	cltq
	leaq	(%rdx,%rax), %rcx
	cmpq	%rcx, 24(%rsp)
	jbe	.L159
	leaq	208(%rsp), %r13
	pxor	%xmm0, %xmm0
	movl	$16, %r12d
	subq	%rax, %r12
	addq	%r13, %rax
	cmpl	$8, %r12d
	movaps	%xmm0, 208(%rsp)
	jnb	.L162
	testb	$4, %r12b
	jne	.L272
	testl	%r12d, %r12d
	je	.L163
	movzbl	(%rdx), %ecx
	testb	$2, %r12b
	movb	%cl, (%rax)
	jne	.L273
.L163:
	movl	8504(%rsp), %ecx
	movq	8496(%rsp), %rdx
	movq	%r13, %rsi
	movl	$10, %edi
	call	inet_ntop@PLT
	movq	8496(%rsp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	8504(%rsp), %rdx
	movq	%rax, %rcx
	cmpq	%rdx, %rax
	ja	.L119
	subq	%rcx, %rdx
	leaq	(%rbx,%rax), %rax
	movq	%rdx, 8504(%rsp)
	movq	8464(%rsp), %rdx
	movq	%rax, 8496(%rsp)
	addq	%r12, %rdx
	testl	%r14d, %r14d
	movq	%rdx, 8464(%rsp)
	je	.L245
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L162:
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	%r12d, %ecx
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rax,%rcx)
	leaq	8(%rax), %rsi
	andq	$-8, %rsi
	subq	%rsi, %rax
	subq	%rax, %rdx
	addl	%r12d, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L163
	andl	$-8, %eax
	xorl	%ecx, %ecx
.L166:
	movl	%ecx, %edi
	addl	$8, %ecx
	movq	(%rdx,%rdi), %r8
	cmpl	%eax, %ecx
	movq	%r8, (%rsi,%rdi)
	jb	.L166
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L270:
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	leaq	.LC31(%rip), %rdi
	movl	$1, %esi
	call	addstr
	testl	%eax, %eax
	jns	.L236
	jmp	.L160
.L119:
	call	addlen.part.0
.L272:
	movl	(%rdx), %ecx
	movl	%ecx, (%rax)
	movl	%r12d, %ecx
	movl	-4(%rdx,%rcx), %edx
	movl	%edx, -4(%rax,%rcx)
	jmp	.L163
.L273:
	movl	%r12d, %ecx
	movzwl	-2(%rdx,%rcx), %edx
	movw	%dx, -2(%rax,%rcx)
	jmp	.L163
	.size	__GI_ns_sprintrrf, .-__GI_ns_sprintrrf
	.globl	ns_sprintrrf
	.set	ns_sprintrrf,__GI_ns_sprintrrf
	.p2align 4,,15
	.globl	__GI_ns_sprintrr
	.hidden	__GI_ns_sprintrr
	.type	__GI_ns_sprintrr, @function
__GI_ns_sprintrr:
	pushq	%rbp
	pushq	%rbx
	leaq	.LC13(%rip), %r10
	movq	%rsi, %rax
	subq	$8, %rsp
	cmpb	$0, (%rsi)
	movzwl	1036(%rsi), %ebp
	movzwl	1028(%rsi), %ebx
	movq	(%rdi), %r11
	cmovne	%rsi, %r10
	movq	8(%rdi), %rsi
	pushq	%r9
	pushq	%r8
	pushq	%rcx
	movq	%r11, %rdi
	pushq	%rdx
	pushq	%rbp
	movl	%ebx, %ecx
	pushq	1040(%rax)
	movl	1032(%rax), %r9d
	subq	%r11, %rsi
	movzwl	1026(%rax), %r8d
	movq	%r10, %rdx
	call	__GI_ns_sprintrrf
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_ns_sprintrr, .-__GI_ns_sprintrr
	.globl	ns_sprintrr
	.set	ns_sprintrr,__GI_ns_sprintrr
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.5542, @object
	.size	__PRETTY_FUNCTION__.5542, 7
__PRETTY_FUNCTION__.5542:
	.string	"addlen"
