	.text
	.p2align 4,,15
	.type	datepart, @function
datepart:
	pushq	%r14
	pushq	%r13
	movl	%esi, %r14d
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movl	%ecx, %ebp
	movq	%rdi, %rbx
	movq	%r8, %r13
	call	__ctype_b_loc@PLT
	movq	(%rax), %r9
	leal	-1(%r14), %eax
	movq	%rbx, %rdi
	leaq	1(%rbx,%rax), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3:
	movzbl	(%rdi), %edx
	testb	$8, 1(%r9,%rdx,2)
	movq	%rdx, %rsi
	jne	.L2
	movl	$1, 0(%r13)
	movzbl	(%rdi), %esi
.L2:
	leal	(%rax,%rax,4), %eax
	addq	$1, %rdi
	movsbl	%sil, %esi
	cmpq	%rdi, %rcx
	leal	-48(%rsi,%rax,2), %eax
	jne	.L3
	cmpl	%eax, %r12d
	jle	.L4
	movl	$1, 0(%r13)
.L4:
	cmpl	%eax, %ebp
	jge	.L1
	movl	$1, 0(%r13)
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	datepart, .-datepart
	.p2align 4,,15
	.globl	ns_datetosecs
	.type	ns_datetosecs, @function
ns_datetosecs:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	strlen@PLT
	cmpq	$14, %rax
	je	.L10
	movl	$1, 0(%rbp)
	xorl	%esi, %esi
.L9:
	addq	$24, %rsp
	movl	%esi, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rbp, %r8
	movl	$9999, %ecx
	movl	$1990, %edx
	movl	$4, %esi
	movq	%rbx, %rdi
	movl	$0, 0(%rbp)
	call	datepart
	leaq	4(%rbx), %rdi
	movq	%rbp, %r8
	movl	$12, %ecx
	movl	$1, %edx
	movl	$2, %esi
	movl	%eax, %r12d
	call	datepart
	leaq	6(%rbx), %rdi
	movq	%rbp, %r8
	movl	$31, %ecx
	movl	$1, %edx
	movl	$2, %esi
	movl	%eax, %r15d
	call	datepart
	leaq	8(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rbp, %r8
	movl	$23, %ecx
	movl	$2, %esi
	movl	%eax, %r13d
	call	datepart
	leaq	10(%rbx), %rdi
	xorl	%edx, %edx
	movq	%rbp, %r8
	movl	$59, %ecx
	movl	$2, %esi
	movl	%eax, %r14d
	call	datepart
	leaq	12(%rbx), %rdi
	movl	$59, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%rbp, %r8
	movl	%eax, 12(%rsp)
	call	datepart
	movl	0(%rbp), %ecx
	xorl	%esi, %esi
	testl	%ecx, %ecx
	jne	.L9
	imull	$60, 12(%rsp), %esi
	imull	$3600, %r14d, %r14d
	leal	-1(%r15), %r10d
	subl	$1, %r13d
	addl	%r14d, %esi
	addl	%eax, %esi
	testl	%r10d, %r10d
	jle	.L12
	leaq	4+days_per_month.5330(%rip), %rdx
	leal	-2(%r15), %eax
	movl	$31, %edi
	leaq	(%rdx,%rax,4), %rax
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L13:
	movl	(%rdx), %edi
	addq	$4, %rdx
.L16:
	addl	%edi, %ecx
	cmpq	%rax, %rdx
	jne	.L13
	addl	%r13d, %ecx
	leal	-1900(%r12), %r8d
	imull	$86400, %ecx, %ecx
	addl	%esi, %ecx
	cmpl	$1, %r10d
	jle	.L15
	testb	$3, %r8b
	jne	.L17
	movl	%r12d, %eax
	movl	$1374389535, %edx
	imull	%edx
	movl	%r12d, %eax
	sarl	$31, %eax
	sarl	$5, %edx
	subl	%eax, %edx
	imull	$100, %edx, %eax
	cmpl	%eax, %r12d
	je	.L17
.L18:
	addl	$86400, %ecx
.L15:
	leal	-1970(%r12), %esi
	imull	$31536000, %esi, %esi
	addl	%ecx, %esi
	cmpl	$70, %r8d
	jle	.L9
	movl	$70, %ecx
	movl	$1374389535, %r9d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%edi, %eax
	mull	%r9d
	shrl	$5, %edx
	imull	$100, %edx, %edx
	cmpl	%edx, %edi
	je	.L19
.L20:
	addl	$86400, %esi
.L21:
	addl	$1, %ecx
	cmpl	%r8d, %ecx
	je	.L9
.L22:
	testb	$3, %cl
	leal	1900(%rcx), %edi
	je	.L26
.L19:
	movl	%edi, %eax
	mull	%r9d
	shrl	$7, %edx
	imull	$400, %edx, %edx
	cmpl	%edx, %edi
	jne	.L21
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%r12d, %eax
	movl	$1374389535, %edx
	imull	%edx
	movl	%r12d, %eax
	sarl	$31, %eax
	sarl	$7, %edx
	subl	%eax, %edx
	imull	$400, %edx, %eax
	cmpl	%eax, %r12d
	jne	.L15
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	imull	$86400, %r13d, %ecx
	leal	-1900(%r12), %r8d
	addl	%esi, %ecx
	jmp	.L15
	.size	ns_datetosecs, .-ns_datetosecs
	.section	.rodata
	.align 32
	.type	days_per_month.5330, @object
	.size	days_per_month.5330, 48
days_per_month.5330:
	.long	31
	.long	28
	.long	31
	.long	30
	.long	31
	.long	30
	.long	31
	.long	31
	.long	30
	.long	31
	.long	30
	.long	31
