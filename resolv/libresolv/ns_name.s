	.text
	.p2align 4,,15
	.globl	ns_name_ntop
	.type	ns_name_ntop, @function
ns_name_ntop:
	leaq	digits(%rip), %r10
	pushq	%r13
	addq	%rsi, %rdx
	pushq	%r12
	movq	%rsi, %rcx
	pushq	%rbp
	movl	$100, %r12d
	pushq	%rbx
	movl	$1, %ebp
	movabsq	$288230377259012293, %rbx
.L2:
	movzbl	(%rdi), %eax
	leaq	1(%rdi), %r9
	testb	%al, %al
	je	.L24
	movl	%eax, %r8d
	andl	$-64, %r8d
	cmpb	$-64, %r8b
	je	.L6
	cmpq	%rsi, %rcx
	je	.L16
	cmpq	%rdx, %rcx
	jnb	.L6
	movb	$46, (%rcx)
	movzbl	(%rdi), %eax
	leaq	1(%rcx), %r8
.L5:
	cmpb	$63, %al
	ja	.L6
	movzbl	%al, %ecx
	addq	%r8, %rcx
	cmpq	%rcx, %rdx
	jbe	.L6
	testb	%al, %al
	je	.L7
	subl	$1, %eax
	leaq	2(%rdi,%rax), %r11
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rbp, %r13
	salq	%cl, %r13
	testq	%rbx, %r13
	je	.L8
	leaq	1(%r8), %rax
	cmpq	%rax, %rdx
	jbe	.L6
	movb	$92, (%r8)
	movb	%dil, 1(%r8)
	addq	$2, %r8
.L12:
	cmpq	%r11, %r9
	je	.L7
.L13:
	addq	$1, %r9
	movzbl	-1(%r9), %eax
	leal	-34(%rax), %ecx
	movl	%eax, %edi
	cmpl	$58, %ecx
	jbe	.L25
.L8:
	subl	$33, %eax
	cmpl	$93, %eax
	jbe	.L26
	leaq	3(%r8), %rax
	cmpq	%rax, %rdx
	jbe	.L6
	movzbl	%dil, %eax
	movb	$92, (%r8)
	addq	$4, %r8
	leal	(%rax,%rax,4), %ecx
	leal	(%rax,%rcx,8), %ecx
	movl	%ecx, %eax
	leal	(%rcx,%rcx,4), %ecx
	shrw	$8, %ax
	shrb	$4, %al
	shrw	$11, %cx
	movq	%rax, %r13
	leal	(%rcx,%rcx,4), %ecx
	andl	$7, %r13d
	movzbl	(%r10,%r13), %r13d
	addl	%ecx, %ecx
	imull	%r12d, %eax
	movb	%r13b, -3(%r8)
	movl	%edi, %r13d
	subl	%ecx, %edi
	subl	%eax, %r13d
	movzbl	%dil, %edi
	movsbw	%r13b, %ax
	sarb	$7, %r13b
	imull	$103, %eax, %eax
	sarw	$10, %ax
	subl	%r13d, %eax
	movzbl	%al, %eax
	movzbl	(%r10,%rax), %eax
	movb	%al, -2(%r8)
	movzbl	(%r10,%rdi), %eax
	movb	%al, -1(%r8)
	cmpq	%r11, %r9
	jne	.L13
.L7:
	movq	%r8, %rcx
	movq	%r9, %rdi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L26:
	cmpq	%r8, %rdx
	jbe	.L6
	movb	%dil, (%r8)
	addq	$1, %r8
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rsi, %r8
	jmp	.L5
.L24:
	cmpq	%rsi, %rcx
	je	.L27
.L15:
	cmpq	%rdx, %rcx
	jnb	.L6
	leaq	1(%rcx), %rax
	movb	$0, (%rcx)
	subl	%esi, %eax
	jmp	.L1
.L27:
	cmpq	%rdx, %rcx
	jnb	.L6
	movb	$46, (%rcx)
	addq	$1, %rcx
	jmp	.L15
	.size	ns_name_ntop, .-ns_name_ntop
	.globl	__ns_name_ntop
	.set	__ns_name_ntop,ns_name_ntop
	.p2align 4,,15
	.globl	ns_name_pton
	.type	ns_name_pton, @function
ns_name_pton:
	pushq	%r15
	pushq	%r14
	leaq	1(%rsi), %r14
	pushq	%r13
	pushq	%r12
	leaq	(%rsi,%rdx), %r12
	pushq	%rbp
	pushq	%rbx
	leaq	digits(%rip), %rbp
	movq	%rdi, %r15
	xorl	%eax, %eax
	subq	$24, %rsp
	movsbl	(%rdi), %ebx
	movq	%rsi, 8(%rsp)
	movq	%rsi, (%rsp)
.L29:
	testl	%ebx, %ebx
	leaq	1(%r15), %r13
	je	.L60
.L39:
	testl	%eax, %eax
	je	.L30
	movl	%ebx, %esi
	movq	%rbp, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L31
	movsbl	1(%r15), %esi
	subq	%rbp, %rax
	imull	$100, %eax, %ebx
	testl	%esi, %esi
	je	.L32
	movq	%rbp, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L32
	movsbl	2(%r15), %esi
	subq	%rbp, %rax
	leaq	3(%r15), %r13
	leal	(%rax,%rax,4), %eax
	leal	(%rbx,%rax,2), %ebx
	testl	%esi, %esi
	je	.L32
	movq	%rbp, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L32
	subq	%rbp, %rax
	addl	%eax, %ebx
	cmpl	$255, %ebx
	jg	.L32
.L31:
	cmpq	%r12, %r14
	jnb	.L32
	movb	%bl, (%r14)
	movsbl	0(%r13), %ebx
	addq	$1, %r14
	movq	%r13, %r15
	xorl	%eax, %eax
	leaq	1(%r15), %r13
	testl	%ebx, %ebx
	jne	.L39
.L60:
	testl	%eax, %eax
	jne	.L32
	movq	(%rsp), %rdi
	movq	%r14, %rcx
	movq	%r14, %rdx
	subq	%rdi, %rcx
	subl	$1, %ecx
	movl	%ecx, %eax
	andl	$192, %eax
	jne	.L32
	cmpq	%r12, %rdi
	jnb	.L32
	testl	%ecx, %ecx
	movb	%cl, (%rdi)
	je	.L40
	cmpq	%r12, %r14
	jnb	.L32
	movb	$0, (%r14)
	addq	$1, %rdx
.L40:
	subq	8(%rsp), %rdx
	cmpq	$255, %rdx
	jle	.L28
	.p2align 4,,10
	.p2align 3
.L32:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
.L28:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	cmpl	$92, %ebx
	je	.L61
	cmpl	$46, %ebx
	jne	.L31
	movq	(%rsp), %rcx
	movq	%r14, %rsi
	movq	%r14, %rdi
	subq	%rcx, %rsi
	subl	$1, %esi
	movl	%esi, %eax
	andl	$192, %eax
	jne	.L32
	cmpq	%r12, %rcx
	jnb	.L32
	movb	%sil, (%rcx)
	movsbl	1(%r15), %ebx
	testb	%bl, %bl
	je	.L62
	cmpb	$46, %bl
	je	.L32
	testl	%esi, %esi
	je	.L32
	movq	%r14, (%rsp)
	movq	%r13, %r15
	addq	$1, %r14
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L61:
	movsbl	1(%r15), %ebx
	movl	$1, %eax
	movq	%r13, %r15
	jmp	.L29
.L62:
	testl	%esi, %esi
	je	.L38
	cmpq	%r12, %r14
	jnb	.L32
	movb	$0, (%r14)
	addq	$1, %rdi
.L38:
	subq	8(%rsp), %rdi
	cmpq	$255, %rdi
	jg	.L32
	movl	$1, %eax
	jmp	.L28
	.size	ns_name_pton, .-ns_name_pton
	.p2align 4,,15
	.globl	ns_name_ntol
	.type	ns_name_ntol, @function
ns_name_ntol:
	pushq	%r15
	pushq	%r14
	addq	%rsi, %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	cmpq	%rdx, %rsi
	movq	%rsi, 40(%rsp)
	movq	%rdx, 32(%rsp)
	jnb	.L66
	movq	40(%rsp), %r15
	movq	%rdi, %r14
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	1(%r14), %rax
	movq	%rax, 8(%rsp)
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L79
	movl	%eax, %edx
	andl	$-64, %edx
	cmpb	$-64, %dl
	je	.L66
	movb	%al, (%r15)
	movzbl	(%r14), %eax
	leaq	1(%r15), %rcx
	movq	%rcx, 16(%rsp)
	cmpb	$63, %al
	ja	.L66
	movzbl	%al, %edx
	movzbl	%al, %ebx
	addq	%rcx, %rdx
	cmpq	%rdx, 32(%rsp)
	jbe	.L66
	testb	%al, %al
	je	.L73
	leal	-1(%rbx), %ebp
	movl	$1, %r13d
	call	__ctype_b_loc@PLT
	movl	%ebp, 28(%rsp)
	movq	%rax, %r12
	addq	$2, %rbp
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L81:
	call	__ctype_tolower_loc@PLT
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %eax
	movb	%al, (%r15,%r13)
	addq	$1, %r13
	cmpq	%r13, %rbp
	je	.L80
.L70:
	movzbl	(%r14,%r13), %ebx
	movq	(%r12), %rsi
	testb	$1, 1(%rsi,%rbx,2)
	jne	.L81
	movb	%bl, (%r15,%r13)
	addq	$1, %r13
	cmpq	%r13, %rbp
	jne	.L70
.L80:
	movslq	28(%rsp), %r8
	movq	8(%rsp), %r14
	movq	16(%rsp), %r15
	addq	$1, %r8
	addq	%r8, %r14
	addq	%r8, %r15
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L73:
	movq	16(%rsp), %r15
	movq	8(%rsp), %r14
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L66:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L79:
	movb	$0, (%r15)
	leaq	1(%r15), %rax
	subl	40(%rsp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	ns_name_ntol, .-ns_name_ntol
	.p2align 4,,15
	.globl	ns_name_unpack
	.type	ns_name_unpack, @function
ns_name_unpack:
	addq	%rcx, %r8
	cmpq	%rdi, %rdx
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	jb	.L104
	cmpq	%rsi, %rdx
	jnb	.L104
	movq	%rsi, %r13
	movq	%rdx, %r9
	xorl	%ebx, %ebx
	movl	$-1, %eax
	subq	%rdi, %r13
.L83:
	movzbl	(%r9), %ebp
	leaq	1(%r9), %r10
	testl	%ebp, %ebp
	movl	%ebp, %r14d
	je	.L106
	movl	%r14d, %r11d
	andl	$192, %r11d
	je	.L87
	cmpb	$-64, %r11b
	jne	.L104
	cmpq	%r10, %rsi
	jbe	.L104
	subq	%rdx, %r10
	movzbl	1(%r9), %r9d
	addl	$1, %r10d
	testl	%eax, %eax
	cmovs	%r10d, %eax
	sall	$8, %ebp
	andl	$16128, %ebp
	orl	%ebp, %r9d
	movslq	%r9d, %r9
	addq	%rdi, %r9
	jc	.L104
	cmpq	%r9, %rsi
	jbe	.L104
	addl	$2, %ebx
	movslq	%ebx, %r10
	cmpq	%r13, %r10
	jl	.L83
	.p2align 4,,10
	.p2align 3
.L104:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
.L82:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movzbl	%r14b, %r11d
	leaq	1(%rcx,%r11), %r9
	cmpq	%r9, %r8
	jbe	.L104
	leaq	(%r10,%r11), %r9
	cmpq	%rsi, %r9
	jnb	.L104
	cmpl	$8, %r11d
	leal	1(%rbx,%rbp), %ebx
	leaq	1(%rcx), %r12
	movb	%r14b, (%rcx)
	jb	.L107
	movq	(%r10), %rbp
	addq	$9, %rcx
	movq	%rbp, -8(%rcx)
	movl	%r11d, %ebp
	andq	$-8, %rcx
	movq	-8(%r10,%rbp), %r14
	movq	%r14, -8(%r12,%rbp)
	movq	%r12, %rbp
	subq	%rcx, %rbp
	subq	%rbp, %r10
	addl	%r11d, %ebp
	andl	$-8, %ebp
	cmpl	$8, %ebp
	jb	.L91
	andl	$-8, %ebp
	xorl	%r14d, %r14d
	movl	%eax, -4(%rsp)
.L94:
	movl	%r14d, %r15d
	addl	$8, %r14d
	movq	(%r10,%r15), %rax
	cmpl	%ebp, %r14d
	movq	%rax, (%rcx,%r15)
	jb	.L94
	movl	-4(%rsp), %eax
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	(%r12,%r11), %rcx
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L107:
	testb	$4, %r11b
	jne	.L108
	testl	%r11d, %r11d
	je	.L91
	movzbl	(%r10), %ecx
	testb	$2, %r11b
	movb	%cl, (%r12)
	je	.L91
	movl	%r11d, %ecx
	movzwl	-2(%r10,%rcx), %r10d
	movw	%r10w, -2(%r12,%rcx)
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L106:
	subq	%rdx, %r10
	testl	%eax, %eax
	movb	$0, (%rcx)
	cmovs	%r10d, %eax
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L108:
	movl	(%r10), %ecx
	movl	%ecx, (%r12)
	movl	%r11d, %ecx
	movl	-4(%r10,%rcx), %r10d
	movl	%r10d, -4(%r12,%rcx)
	jmp	.L91
	.size	ns_name_unpack, .-ns_name_unpack
	.globl	__ns_name_unpack
	.set	__ns_name_unpack,ns_name_unpack
	.p2align 4,,15
	.globl	ns_name_pack
	.type	ns_name_pack, @function
ns_name_pack:
	pushq	%r15
	pushq	%r14
	testq	%rcx, %rcx
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, -40(%rsp)
	movq	%r8, -56(%rsp)
	je	.L148
	movq	(%rcx), %r14
	leaq	8(%rcx), %r15
	testq	%r14, %r14
	je	.L149
	cmpq	$0, 8(%rcx)
	movq	%r15, %rcx
	je	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$8, %rcx
	cmpq	$0, (%rcx)
	jne	.L111
.L110:
	movq	%rdi, %rsi
	xorl	%r8d, %r8d
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L112:
	cmpb	$63, %r10b
	ja	.L193
	leal	1(%r8,%r10), %r8d
	cmpl	$255, %r8d
	jg	.L115
	movzbl	%r10b, %r9d
	testb	%r10b, %r10b
	leaq	1(%rsi,%r9), %rsi
	je	.L194
.L116:
	movzbl	(%rsi), %r10d
	movl	%r10d, %r9d
	andl	$-64, %r9d
	cmpb	$-64, %r9b
	jne	.L112
.L115:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
.L109:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%ecx, %ecx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L193:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	movq	-40(%rsp), %rax
	movq	-56(%rsp), %rbx
	movslq	%edx, %rdx
	movzbl	(%rdi), %r9d
	movq	%rcx, -24(%rsp)
	movl	$1, -4(%rsp)
	addq	%rax, %rdx
	testq	%r14, %r14
	setne	-5(%rsp)
	subq	$8, %rbx
	movq	%rdx, -64(%rsp)
	movq	%rbx, -16(%rsp)
	.p2align 4,,10
	.p2align 3
.L117:
	testb	%r9b, %r9b
	je	.L118
	cmpb	$0, -5(%rsp)
	je	.L118
	cmpq	%rcx, %r15
	jnb	.L119
	movq	%r15, -48(%rsp)
.L134:
	movq	-48(%rsp), %rbx
	movq	(%rbx), %r12
	movzbl	(%r12), %ebp
	testb	%bpl, %bpl
	je	.L120
.L192:
	testb	$-64, %bpl
	jne	.L120
	movq	%r12, %rbx
	subq	%r14, %rbx
	cmpq	$16383, %rbx
	movq	%rbx, -32(%rsp)
	jg	.L120
	movl	%ebp, %r8d
	movq	%r12, %rdx
	movq	%rdi, %r10
	movzbl	%r8b, %esi
	testl	%esi, %esi
	je	.L125
.L132:
	movl	%r8d, %r11d
	andl	$192, %r11d
	je	.L123
	cmpb	$-64, %r11b
	jne	.L195
	movzbl	1(%rdx), %edx
	sall	$8, %esi
	andl	$16128, %esi
	orl	%esi, %edx
	addq	%r14, %rdx
.L129:
	movzbl	(%rdx), %r8d
	movzbl	%r8b, %esi
	testl	%esi, %esi
	jne	.L132
	.p2align 4,,10
	.p2align 3
.L125:
	leaq	1(%r12,%rbp), %r12
	movzbl	(%r12), %ebp
	testb	%bpl, %bpl
	jne	.L192
	.p2align 4,,10
	.p2align 3
.L120:
	addq	$8, -48(%rsp)
	movq	-48(%rsp), %rbx
	cmpq	%rbx, %rcx
	ja	.L134
.L119:
	movq	errno@gottpoff(%rip), %rdx
	movl	$2, %fs:(%rdx)
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L195:
	movq	errno@gottpoff(%rip), %rdx
	movl	$90, %fs:(%rdx)
.L131:
	cmpq	$0, -56(%rsp)
	je	.L118
	movq	-24(%rsp), %rbx
	movq	-16(%rsp), %rdx
	cmpq	%rdx, %rbx
	jnb	.L118
	movq	%rax, %rdx
	subq	%r14, %rdx
	cmpq	$16383, %rdx
	jg	.L118
	movl	-4(%rsp), %edx
	testl	%edx, %edx
	je	.L118
	movq	%rax, (%rbx)
	movq	$0, 8(%rbx)
	addq	$8, %rbx
	movq	%rbx, -24(%rsp)
	movl	$0, -4(%rsp)
	.p2align 4,,10
	.p2align 3
.L118:
	andl	$-64, %r9d
	cmpb	$-64, %r9b
	je	.L139
	movzbl	(%rdi), %r9d
	movq	-64(%rsp), %rsi
	subq	%rax, %rsi
	cmpb	$63, %r9b
	ja	.L138
	movzbl	%r9b, %edx
	addl	$1, %edx
	movslq	%edx, %r8
	cmpq	%rsi, %r8
	jg	.L139
	cmpl	$8, %edx
	jnb	.L140
	testb	$4, %dl
	jne	.L196
	testl	%edx, %edx
	je	.L141
	testb	$2, %dl
	movb	%r9b, (%rax)
	jne	.L197
.L141:
	movzbl	%r9b, %edx
	addq	$1, %rdx
	addq	%rdx, %rdi
	addq	%rdx, %rax
	testb	%r9b, %r9b
	je	.L198
	movzbl	(%rdi), %r9d
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L138:
	testq	%rsi, %rsi
	jns	.L117
.L139:
	testq	%r14, %r14
	je	.L115
.L137:
	movq	$0, (%rcx)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L140:
	movq	(%rdi), %rsi
	movq	%rdi, %r11
	movq	%rsi, (%rax)
	movl	%edx, %esi
	movq	-8(%rdi,%rsi), %r8
	movq	%r8, -8(%rax,%rsi)
	leaq	8(%rax), %rsi
	movq	%rax, %r8
	andq	$-8, %rsi
	subq	%rsi, %r8
	addl	%r8d, %edx
	subq	%r8, %r11
	andl	$-8, %edx
	cmpl	$8, %edx
	jb	.L141
	andl	$-8, %edx
	xorl	%r8d, %r8d
.L144:
	movl	%r8d, %r10d
	addl	$8, %r8d
	movq	(%r11,%r10), %rbx
	cmpl	%edx, %r8d
	movq	%rbx, (%rsi,%r10)
	jb	.L144
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L123:
	cmpb	%r8b, (%r10)
	leaq	1(%r10), %r11
	jne	.L125
	subl	$1, %esi
	movq	%r11, %r10
	leaq	2(%rdx,%rsi), %r13
	addq	$1, %rdx
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L200:
	cmpq	%r13, %rdx
	je	.L199
.L128:
	addq	$1, %r10
	movzbl	-1(%r10), %esi
	leal	-65(%rsi), %r11d
	leal	32(%rsi), %r8d
	cmpl	$26, %r11d
	cmovb	%r8d, %esi
	addq	$1, %rdx
	movzbl	-1(%rdx), %r8d
	leal	-65(%r8), %ebx
	leal	32(%r8), %r11d
	cmpl	$26, %ebx
	cmovb	%r11d, %r8d
	cmpl	%esi, %r8d
	je	.L200
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L199:
	cmpb	$0, (%r10)
	jne	.L129
	cmpb	$0, (%rdx)
	jne	.L125
	movq	-32(%rsp), %rbx
	testl	%ebx, %ebx
	movl	%ebx, %edx
	js	.L131
	leaq	1(%rax), %rsi
	cmpq	%rsi, -64(%rsp)
	jbe	.L137
	movzbl	-32(%rsp), %edi
	sarl	$8, %edx
	addq	$2, %rax
	orl	$-64, %edx
	movb	%dl, -2(%rax)
	movb	%dil, -1(%rax)
	subl	-40(%rsp), %eax
	jmp	.L109
.L198:
	cmpq	%rax, -64(%rsp)
	jb	.L139
	subl	-40(%rsp), %eax
	jmp	.L109
.L197:
	movl	%edx, %edx
	movzwl	-2(%rdi,%rdx), %esi
	movw	%si, -2(%rax,%rdx)
	jmp	.L141
.L196:
	movl	(%rdi), %esi
	movl	%edx, %edx
	movl	%esi, (%rax)
	movl	-4(%rdi,%rdx), %esi
	movl	%esi, -4(%rax,%rdx)
	jmp	.L141
	.size	ns_name_pack, .-ns_name_pack
	.p2align 4,,15
	.globl	ns_name_uncompress
	.type	ns_name_uncompress, @function
ns_name_uncompress:
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movl	$255, %r8d
	subq	$264, %rsp
	movq	%rsp, %rbp
	movq	%rbp, %rcx
	call	ns_name_unpack
	cmpl	$-1, %eax
	je	.L204
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movl	%eax, %ebx
	call	ns_name_ntop
	cmpl	$-1, %eax
	je	.L204
.L201:
	addq	$264, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$-1, %ebx
	jmp	.L201
	.size	ns_name_uncompress, .-ns_name_uncompress
	.p2align 4,,15
	.globl	ns_name_compress
	.type	ns_name_compress, @function
ns_name_compress:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	$255, %edx
	movq	%rcx, %r13
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	ns_name_pton
	cmpl	$-1, %eax
	je	.L209
	movq	%r14, %r8
	movq	%r13, %rcx
	movl	%r12d, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	ns_name_pack
.L209:
	addq	$256, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	ns_name_compress, .-ns_name_compress
	.p2align 4,,15
	.globl	ns_name_rollback
	.type	ns_name_rollback, @function
ns_name_rollback:
	cmpq	%rdx, %rsi
	jnb	.L215
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L215
	cmpq	%rax, %rdi
	ja	.L218
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L224:
	movq	(%rsi), %rax
	testq	%rax, %rax
	je	.L215
	cmpq	%rdi, %rax
	jnb	.L217
.L218:
	addq	$8, %rsi
	cmpq	%rsi, %rdx
	ja	.L224
.L215:
	rep ret
	.p2align 4,,10
	.p2align 3
.L217:
	movq	$0, (%rsi)
	ret
	.size	ns_name_rollback, .-ns_name_rollback
	.p2align 4,,15
	.globl	ns_name_skip
	.type	ns_name_skip, @function
ns_name_skip:
	movq	(%rdi), %rax
	cmpq	%rsi, %rax
	jnb	.L227
.L231:
	movzbl	(%rax), %edx
	leaq	1(%rax), %r8
	testb	%dl, %dl
	je	.L241
	movl	%edx, %ecx
	andl	$192, %ecx
	je	.L229
	cmpb	$-64, %cl
	jne	.L239
	addq	$2, %rax
.L227:
	cmpq	%rsi, %rax
	ja	.L239
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L239:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L229:
	movzbl	%dl, %eax
	addq	%r8, %rax
	cmpq	%rax, %rsi
	ja	.L231
	jmp	.L227
	.p2align 4,,10
	.p2align 3
.L241:
	movq	%r8, %rax
	jmp	.L227
	.size	ns_name_skip, .-ns_name_skip
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	digits, @object
	.size	digits, 11
digits:
	.string	"0123456789"
