	.text
#APP
	.symver res_send_setqhook,res_send_setqhook@GLIBC_2.2.5
	.symver res_send_setrhook,res_send_setrhook@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	res_send_setqhook
	.type	res_send_setqhook, @function
res_send_setqhook:
	movq	__resp@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	%rdi, 480(%rax)
	ret
	.size	res_send_setqhook, .-res_send_setqhook
	.p2align 4,,15
	.globl	res_send_setrhook
	.type	res_send_setrhook, @function
res_send_setrhook:
	movq	__resp@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	%rdi, 488(%rax)
	ret
	.size	res_send_setrhook, .-res_send_setrhook
