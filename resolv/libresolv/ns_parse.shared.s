	.text
	.p2align 4,,15
	.globl	ns_msg_getflag
	.type	ns_msg_getflag, @function
ns_msg_getflag:
	leaq	_ns_flagdata(%rip), %rdx
	movslq	%edi, %rdi
	movzwl	26(%rsp), %eax
	andl	(%rdx,%rdi,8), %eax
	movl	4(%rdx,%rdi,8), %ecx
	sarl	%cl, %eax
	ret
	.size	ns_msg_getflag, .-ns_msg_getflag
	.p2align 4,,15
	.globl	__GI_ns_skiprr
	.hidden	__GI_ns_skiprr
	.type	__GI_ns_skiprr, @function
__GI_ns_skiprr:
	testl	%ecx, %ecx
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	jle	.L10
	movl	%edx, %r13d
	movl	%ecx, %ebp
	movq	%rdi, %rbx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	addl	$2, %eax
	testl	%r13d, %r13d
	cltq
	leaq	2(%rbx,%rax), %rbx
	je	.L7
	leaq	6(%rbx), %rax
	cmpq	%rax, %r12
	jb	.L8
	movzwl	4(%rbx), %ebx
	rolw	$8, %bx
	movzwl	%bx, %ebx
	addq	%rax, %rbx
.L7:
	subl	$1, %ebp
	je	.L4
.L9:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI___dn_skipname
	testl	%eax, %eax
	jns	.L5
.L8:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L10:
	movq	%rdi, %rbx
	.p2align 4,,10
	.p2align 3
.L4:
	cmpq	%rbx, %r12
	jb	.L8
	movl	%ebx, %eax
	subl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI_ns_skiprr, .-__GI_ns_skiprr
	.globl	ns_skiprr
	.set	ns_skiprr,__GI_ns_skiprr
	.p2align 4,,15
	.globl	__GI_ns_initparse
	.hidden	__GI_ns_initparse
	.type	__GI_ns_initparse, @function
__GI_ns_initparse:
	pushq	%r13
	pushq	%r12
	movslq	%esi, %rsi
	pushq	%rbp
	pushq	%rbx
	leaq	(%rdi,%rsi), %r13
	leaq	2(%rdi), %rax
	subq	$8, %rsp
	movq	%rdi, (%rdx)
	movq	%r13, 8(%rdx)
	movdqa	.LC0(%rip), %xmm0
	cmpq	%rax, %r13
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 64(%rdx)
	jb	.L19
	leaq	4(%rdi), %rbx
	movzwl	(%rdi), %eax
	rolw	$8, %ax
	cmpq	%r13, %rbx
	movw	%ax, 16(%rdx)
	ja	.L19
	movzwl	2(%rdi), %eax
	leaq	20(%rdx), %rcx
	addq	$12, %rdi
	rolw	$8, %ax
	movw	%ax, 18(%rdx)
.L20:
	addq	$2, %rbx
	cmpq	%r13, %rbx
	ja	.L19
	movzwl	-2(%rbx), %eax
	addq	$2, %rcx
	rolw	$8, %ax
	movw	%ax, -2(%rcx)
	cmpq	%rdi, %rbx
	jne	.L20
	movq	%rdx, %rbp
	xorl	%r12d, %r12d
.L23:
	movzwl	20(%rbp,%r12,2), %ecx
	testw	%cx, %cx
	jne	.L21
	movq	$0, 32(%rbp,%r12,8)
.L22:
	addq	$1, %r12
	cmpq	$4, %r12
	jne	.L23
	cmpq	%r13, %rbx
	jne	.L19
	movl	$4, 64(%rbp)
	movl	$-1, 68(%rbp)
	xorl	%eax, %eax
	movq	$0, 72(%rbp)
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%r12d, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	__GI_ns_skiprr
	testl	%eax, %eax
	js	.L24
	cltq
	movq	%rbx, 32(%rbp,%r12,8)
	addq	%rax, %rbx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L19:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$-1, %eax
	jmp	.L16
	.size	__GI_ns_initparse, .-__GI_ns_initparse
	.globl	ns_initparse
	.set	ns_initparse,__GI_ns_initparse
	.p2align 4,,15
	.globl	__GI_ns_parserr
	.hidden	__GI_ns_parserr
	.type	__GI_ns_parserr, @function
__GI_ns_parserr:
	cmpl	$3, %esi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	ja	.L33
	cmpl	%esi, 64(%rdi)
	movq	%rdi, %rbx
	movl	%esi, %ebp
	movl	%edx, %r13d
	movq	%rcx, %r14
	je	.L31
	movslq	%esi, %rax
	movl	%esi, 64(%rbx)
	movl	$0, 68(%rdi)
	movq	32(%rdi,%rax,8), %rax
	movq	%rax, 72(%rdi)
.L31:
	cmpl	$-1, %r13d
	jne	.L32
	movl	68(%rbx), %r13d
.L32:
	testl	%r13d, %r13d
	js	.L33
	movslq	%ebp, %rdx
	leaq	(%rbx,%rdx,2), %r12
	movzwl	20(%r12), %eax
	cmpl	%r13d, %eax
	jle	.L33
	movl	68(%rbx), %eax
	cmpl	%r13d, %eax
	jg	.L34
	movq	72(%rbx), %rdi
.L35:
	cmpl	%r13d, %eax
	movq	8(%rbx), %rsi
	jge	.L36
	movl	%r13d, %ecx
	movl	%ebp, %edx
	subl	%eax, %ecx
	call	__GI_ns_skiprr
	testl	%eax, %eax
	js	.L47
	movslq	%eax, %rdi
	addq	72(%rbx), %rdi
	movq	8(%rbx), %rsi
	movl	%r13d, 68(%rbx)
	movq	%rdi, 72(%rbx)
.L36:
	movq	%rdi, %rdx
	movq	(%rbx), %rdi
	movl	$1025, %r8d
	movq	%r14, %rcx
	call	__GI___dn_expand
	testl	%eax, %eax
	js	.L47
	cltq
	addq	72(%rbx), %rax
	movq	8(%rbx), %rsi
	leaq	4(%rax), %rcx
	movq	%rax, 72(%rbx)
	cmpq	%rsi, %rcx
	ja	.L42
	movzwl	(%rax), %edx
	rolw	$8, %dx
	movw	%dx, 1026(%r14)
	movzwl	2(%rax), %edx
	rolw	$8, %dx
	testl	%ebp, %ebp
	movw	%dx, 1028(%r14)
	movq	%rcx, 72(%rbx)
	jne	.L40
	xorl	%eax, %eax
	movzwl	20(%rbx), %edx
	movl	$0, 1032(%r14)
	movw	%ax, 1036(%r14)
	movl	68(%rbx), %eax
	movq	$0, 1040(%r14)
	addl	$1, %eax
	cmpl	%eax, %edx
	movl	%eax, 68(%rbx)
	jge	.L46
	movl	$1, 64(%rbx)
	movl	$1, %ebp
.L44:
	movslq	%ebp, %rbp
	movl	$0, 68(%rbx)
	movq	32(%rbx,%rbp,8), %rax
	movq	%rax, 72(%rbx)
	xorl	%eax, %eax
.L28:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movq	32(%rbx,%rdx,8), %rdi
	movl	%ebp, 64(%rbx)
	xorl	%eax, %eax
	movl	$0, 68(%rbx)
	movq	%rdi, 72(%rbx)
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	10(%rax), %rdx
	cmpq	%rdx, %rsi
	jb	.L42
	movl	4(%rax), %ecx
	movzwl	8(%rax), %eax
	bswap	%ecx
	rolw	$8, %ax
	movl	%ecx, 1032(%r14)
	movw	%ax, 1036(%r14)
	movzwl	%ax, %eax
	movq	%rdx, 72(%rbx)
	addq	%rdx, %rax
	cmpq	%rax, %rsi
	jb	.L42
	movq	%rdx, 1040(%r14)
	movq	%rax, 72(%rbx)
	movl	68(%rbx), %eax
	addl	$1, %eax
	movl	%eax, 68(%rbx)
	movzwl	20(%r12), %edx
	cmpl	%edx, %eax
	jle	.L46
	addl	$1, %ebp
	cmpl	$4, %ebp
	movl	%ebp, 64(%rbx)
	jne	.L44
	movl	$-1, 68(%rbx)
	movq	$0, 72(%rbx)
.L46:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
.L47:
	movl	$-1, %eax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L33:
	movq	errno@gottpoff(%rip), %rax
	movl	$19, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L28
	.size	__GI_ns_parserr, .-__GI_ns_parserr
	.globl	ns_parserr
	.set	ns_parserr,__GI_ns_parserr
	.hidden	_ns_flagdata
	.globl	_ns_flagdata
	.section	.rodata
	.align 32
	.type	_ns_flagdata, @object
	.size	_ns_flagdata, 128
_ns_flagdata:
	.long	32768
	.long	15
	.long	30720
	.long	11
	.long	1024
	.long	10
	.long	512
	.long	9
	.long	256
	.long	8
	.long	128
	.long	7
	.long	64
	.long	6
	.long	32
	.long	5
	.long	16
	.long	4
	.long	15
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	6799976246779207262
	.quad	6799976246779207262
