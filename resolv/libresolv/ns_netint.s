	.text
	.p2align 4,,15
	.globl	ns_get16
	.type	ns_get16, @function
ns_get16:
	movzwl	(%rdi), %eax
	rolw	$8, %ax
	movzwl	%ax, %eax
	ret
	.size	ns_get16, .-ns_get16
	.globl	__ns_get16
	.set	__ns_get16,ns_get16
	.p2align 4,,15
	.globl	ns_get32
	.type	ns_get32, @function
ns_get32:
	movl	(%rdi), %eax
	bswap	%eax
	movl	%eax, %eax
	ret
	.size	ns_get32, .-ns_get32
	.globl	__ns_get32
	.set	__ns_get32,ns_get32
	.p2align 4,,15
	.globl	ns_put16
	.type	ns_put16, @function
ns_put16:
	rolw	$8, %di
	movw	%di, (%rsi)
	ret
	.size	ns_put16, .-ns_put16
	.p2align 4,,15
	.globl	ns_put32
	.type	ns_put32, @function
ns_put32:
	bswap	%edi
	movl	%edi, (%rsi)
	ret
	.size	ns_put32, .-ns_put32
