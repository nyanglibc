	.text
	.p2align 4,,15
	.type	binary_hnok, @function
binary_hnok:
	movabsq	$1125899906850809, %rsi
.L5:
	movzbl	(%rdi), %ecx
	testq	%rcx, %rcx
	je	.L6
	addq	$1, %rdi
	addq	%rdi, %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	btq	%rdx, %rsi
	jnc	.L8
.L3:
	addq	$1, %rdi
	cmpq	%rdi, %rcx
	jbe	.L5
.L4:
	movzbl	(%rdi), %edx
	movl	%edx, %eax
	andl	$-33, %eax
	subl	$65, %eax
	cmpb	$25, %al
	jbe	.L3
	subl	$45, %edx
	cmpb	$50, %dl
	jbe	.L11
.L8:
	xorl	%eax, %eax
	ret
.L6:
	movl	$1, %eax
	ret
	.size	binary_hnok, .-binary_hnok
	.p2align 4,,15
	.globl	__dn_expand
	.type	__dn_expand, @function
__dn_expand:
	pushq	%rbx
	movslq	%r8d, %r8
	movq	%rcx, %rbx
	call	ns_name_uncompress@PLT
	testl	%eax, %eax
	jle	.L12
	cmpb	$46, (%rbx)
	je	.L15
.L12:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movb	$0, (%rbx)
	popq	%rbx
	ret
	.size	__dn_expand, .-__dn_expand
	.p2align 4,,15
	.globl	__dn_comp
	.type	__dn_comp, @function
__dn_comp:
	movslq	%edx, %rdx
	jmp	ns_name_compress@PLT
	.size	__dn_comp, .-__dn_comp
	.p2align 4,,15
	.globl	__dn_skipname
	.type	__dn_skipname, @function
__dn_skipname:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	%rdi, 8(%rsp)
	leaq	8(%rsp), %rdi
	call	ns_name_skip@PLT
	cmpl	$-1, %eax
	je	.L17
	movl	8(%rsp), %eax
	subl	%ebx, %eax
.L17:
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__dn_skipname, .-__dn_skipname
	.p2align 4,,15
	.globl	__res_hnok
	.type	__res_hnok, @function
__res_hnok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L24
	subl	$33, %eax
	movq	%rdi, %rdx
	cmpb	$93, %al
	jbe	.L25
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L39:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L42
.L25:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L39
.L24:
	pushq	%rbx
	movl	$255, %edx
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	ns_name_pton@PLT
	testl	%eax, %eax
	js	.L27
	cmpb	$0, (%rsp)
	je	.L29
	cmpb	$45, 1(%rsp)
	je	.L27
.L29:
	movq	%rbx, %rdi
	call	binary_hnok
	addq	$256, %rsp
	movzbl	%al, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$256, %rsp
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%eax, %eax
	ret
	.size	__res_hnok, .-__res_hnok
	.p2align 4,,15
	.globl	__res_ownok
	.type	__res_ownok, @function
__res_ownok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L45
	subl	$33, %eax
	movq	%rdi, %rdx
	cmpb	$93, %al
	jbe	.L46
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L68:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L72
.L46:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L68
.L45:
	pushq	%rbx
	movl	$255, %edx
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	ns_name_pton@PLT
	testl	%eax, %eax
	js	.L48
	movzbl	(%rsp), %eax
	testb	%al, %al
	je	.L50
	movzbl	1(%rsp), %edx
	cmpb	$45, %dl
	je	.L48
	cmpb	$1, %al
	jne	.L50
	cmpb	$42, %dl
	je	.L74
.L50:
	movq	%rbx, %rdi
	call	binary_hnok
	addq	$256, %rsp
	movzbl	%al, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%eax, %eax
.L44:
	addq	$256, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%eax, %eax
	ret
.L74:
	leaq	2(%rbx), %rdi
	call	binary_hnok
	movzbl	%al, %eax
	jmp	.L44
	.size	__res_ownok, .-__res_ownok
	.p2align 4,,15
	.globl	__res_mailok
	.type	__res_mailok, @function
__res_mailok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L76
	subl	$33, %eax
	movq	%rdi, %rdx
	cmpb	$93, %al
	jbe	.L77
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L92:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L95
.L77:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L92
.L76:
	pushq	%rbx
	movl	$255, %edx
	subq	$256, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	ns_name_pton@PLT
	testl	%eax, %eax
	js	.L79
	movzbl	(%rsp), %edx
	movl	$1, %eax
	testb	%dl, %dl
	je	.L75
	leaq	1(%rbx,%rdx), %rdi
	cmpb	$0, (%rdi)
	jne	.L97
.L79:
	xorl	%eax, %eax
.L75:
	addq	$256, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	xorl	%eax, %eax
	ret
.L97:
	call	binary_hnok
	movzbl	%al, %eax
	jmp	.L75
	.size	__res_mailok, .-__res_mailok
	.p2align 4,,15
	.globl	__res_dnok
	.type	__res_dnok, @function
__res_dnok:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L99
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L104
	movq	%rdi, %rdx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L111:
	subl	$33, %eax
	cmpb	$93, %al
	ja	.L104
.L101:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L111
.L99:
	subq	$264, %rsp
	movl	$255, %edx
	movq	%rsp, %rsi
	call	ns_name_pton@PLT
	notl	%eax
	addq	$264, %rsp
	shrl	$31, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	xorl	%eax, %eax
	ret
	.size	__res_dnok, .-__res_dnok
	.p2align 4,,15
	.globl	__putlong
	.type	__putlong, @function
__putlong:
	movl	%edi, %edi
	jmp	ns_put32@PLT
	.size	__putlong, .-__putlong
	.p2align 4,,15
	.globl	__putshort
	.type	__putshort, @function
__putshort:
	movzwl	%di, %edi
	jmp	ns_put16@PLT
	.size	__putshort, .-__putshort
	.p2align 4,,15
	.globl	_getlong
	.type	_getlong, @function
_getlong:
	subq	$8, %rsp
	call	ns_get32@PLT
	addq	$8, %rsp
	ret
	.size	_getlong, .-_getlong
	.p2align 4,,15
	.globl	_getshort
	.type	_getshort, @function
_getshort:
	subq	$8, %rsp
	call	ns_get16@PLT
	addq	$8, %rsp
	ret
	.size	_getshort, .-_getshort
