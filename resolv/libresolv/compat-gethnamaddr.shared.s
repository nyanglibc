	.text
#APP
	.symver res_gethostbyname,res_gethostbyname@GLIBC_2.2.5
	.symver res_gethostbyname2,res_gethostbyname2@GLIBC_2.2.5
	.symver res_gethostbyaddr,res_gethostbyaddr@GLIBC_2.2.5
	.symver _sethtent,_sethtent@GLIBC_2.2.5
	.symver _gethtent,_gethtent@GLIBC_2.2.5
	.symver _gethtbyname,_gethtbyname@GLIBC_2.2.5
	.symver _gethtbyname2,_gethtbyname2@GLIBC_2.2.5
	.symver _gethtbyaddr,_gethtbyaddr@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	_endhtent, @function
_endhtent:
	movq	hostf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L7
	movl	stayopen(%rip), %eax
	testl	%eax, %eax
	je	.L11
.L7:
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$8, %rsp
	call	fclose@PLT
	movq	$0, hostf(%rip)
	addq	$8, %rsp
	ret
	.size	_endhtent, .-_endhtent
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"/etc/hosts"
	.text
	.p2align 4,,15
	.globl	__GI__sethtent
	.hidden	__GI__sethtent
	.type	__GI__sethtent, @function
__GI__sethtent:
	pushq	%rbx
	movl	%edi, %ebx
	movq	hostf(%rip), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	rewind@PLT
	movl	%ebx, stayopen(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	fopen@PLT
	movl	%ebx, stayopen(%rip)
	movq	%rax, hostf(%rip)
	popq	%rbx
	ret
	.size	__GI__sethtent, .-__GI__sethtent
	.globl	_sethtent
	.set	_sethtent,__GI__sethtent
	.section	.rodata.str1.1
.LC2:
	.string	"#\n"
.LC3:
	.string	" \t"
	.text
	.p2align 4,,15
	.globl	__GI__gethtent
	.hidden	__GI__gethtent
	.type	__GI__gethtent, @function
__GI__gethtent:
	movq	hostf(%rip), %rdx
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	testq	%rdx, %rdx
	je	.L18
.L34:
	leaq	hostbuf(%rip), %r12
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	cmpb	$35, (%rax)
	je	.L22
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L22
	leaq	.LC3(%rip), %rsi
	movb	$0, (%rax)
	movq	%rbx, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L22
	leaq	host_addr(%rip), %rdx
	movb	$0, (%rax)
	movq	%rbx, %rsi
	movl	$10, %edi
	call	inet_pton@PLT
	testl	%eax, %eax
	jg	.L35
	leaq	host_addr(%rip), %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	inet_pton@PLT
	testl	%eax, %eax
	jg	.L64
.L22:
	movq	hostf(%rip), %rdx
.L19:
	movl	$8192, %esi
	movq	%r12, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L20
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	xorl	%eax, %eax
.L17:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	fopen@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	movq	%rax, hostf(%rip)
	jne	.L34
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$16, %edx
	movl	$10, %eax
.L23:
	leaq	host_addr(%rip), %rcx
	movq	$0, 8+h_addr_ptrs(%rip)
	movl	%edx, 20+host(%rip)
	movl	%eax, 16+host(%rip)
	leaq	1(%rbp), %rdi
	movq	%rcx, h_addr_ptrs(%rip)
	leaq	h_addr_ptrs(%rip), %rcx
	movq	%rcx, 24+host(%rip)
	movzbl	1(%rbp), %eax
	cmpb	$9, %al
	je	.L54
	cmpb	$32, %al
	jne	.L24
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	cmpb	$32, %al
	je	.L54
	cmpb	$9, %al
	je	.L54
.L24:
	leaq	host_aliases(%rip), %rbx
	leaq	.LC3(%rip), %rsi
	movq	%rdi, host(%rip)
	movq	%rbx, 8+host(%rip)
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L27
	movq	%rax, %rdi
	movb	$0, (%rax)
	addq	$1, %rdi
	je	.L27
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L27
	leaq	272(%rbx), %r12
	leaq	.LC3(%rip), %rbp
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	1(%rax), %rdi
	movb	$0, (%rax)
.L30:
	testq	%rdi, %rdi
	je	.L32
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L32
.L33:
	cmpb	$32, %al
	je	.L36
	cmpb	$9, %al
	je	.L36
	cmpq	%r12, %rbx
	jnb	.L31
	movq	%rdi, (%rbx)
	addq	$8, %rbx
.L31:
	movq	%rbp, %rsi
	call	strpbrk@PLT
	testq	%rax, %rax
	jne	.L65
.L32:
	movq	__h_errno@gottpoff(%rip), %rax
	movq	$0, (%rbx)
	movl	$0, %fs:(%rax)
	leaq	host(%rip), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$1, %rdi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L64:
	movl	$4, %edx
	movl	$2, %eax
	jmp	.L23
.L27:
	leaq	host_aliases(%rip), %rbx
	jmp	.L32
	.size	__GI__gethtent, .-__GI__gethtent
	.globl	_gethtent
	.set	_gethtent,__GI__gethtent
	.p2align 4,,15
	.globl	__GI__gethtbyname2
	.hidden	__GI__gethtbyname2
	.type	__GI__gethtbyname2, @function
__GI__gethtbyname2:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	xorl	%edi, %edi
	movl	%esi, %r13d
	subq	$8, %rsp
	call	__GI__sethtent
.L77:
	call	__GI__gethtent
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L68
	cmpl	%r13d, 16(%rbp)
	jne	.L77
	movq	0(%rbp), %rdi
	movq	%r12, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L68
	movq	8(%rbp), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L69
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$8, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L77
.L69:
	movq	%r12, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L82
.L68:
	call	_endhtent
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI__gethtbyname2, .-__GI__gethtbyname2
	.globl	_gethtbyname2
	.set	_gethtbyname2,__GI__gethtbyname2
	.p2align 4,,15
	.globl	_gethtbyname
	.type	_gethtbyname, @function
_gethtbyname:
	movl	$2, %esi
	jmp	__GI__gethtbyname2
	.size	_gethtbyname, .-_gethtbyname
	.p2align 4,,15
	.globl	__GI__gethtbyaddr
	.hidden	__GI__gethtbyaddr
	.type	__GI__gethtbyaddr, @function
__GI__gethtbyaddr:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%edi, %edi
	movq	%rsi, %r12
	movl	%edx, %ebp
	subq	$8, %rsp
	call	__GI__sethtent
	.p2align 4,,10
	.p2align 3
.L90:
	call	__GI__gethtent
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L86
	cmpl	%ebp, 16(%rbx)
	jne	.L90
	movq	24(%rbx), %rax
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	(%rax), %rdi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L90
.L86:
	call	_endhtent
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI__gethtbyaddr, .-__GI__gethtbyaddr
	.globl	_gethtbyaddr
	.set	_gethtbyaddr,__GI__gethtbyaddr
	.p2align 4,,15
	.globl	addrsort
	.type	addrsort, @function
addrsort:
	testl	%esi, %esi
	jle	.L122
	movq	__resp@gottpoff(%rip), %rax
	pushq	%r15
	leal	-1(%rsi), %r15d
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	xorl	%r12d, %r12d
	pushq	%rbx
	movq	%fs:(%rax), %rdx
	xorl	%ebx, %ebx
	leaq	-88(%rsp), %r14
	movl	%r15d, -92(%rsp)
	addq	$1, %r15
	movzbl	392(%rdx), %r13d
	shrb	$4, %r13b
	movzbl	%r13b, %r13d
	leal	-1(%r13), %r10d
	addq	$1, %r10
	.p2align 4,,10
	.p2align 3
.L95:
	testl	%r13d, %r13d
	movl	%ebx, %r11d
	je	.L108
	movq	(%rdi,%rbx,8), %rax
	movl	(%rax), %ebp
	movl	400(%rdx), %eax
	andl	%ebp, %eax
	cmpl	396(%rdx), %eax
	je	.L108
	movl	$1, %eax
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L98:
	movl	400(%rdx,%rax,8), %ecx
	leaq	1(%rax), %r8
	andl	%ebp, %ecx
	cmpl	%ecx, 396(%rdx,%rax,8)
	je	.L97
	movq	%r8, %rax
.L96:
	cmpq	%r10, %rax
	movl	%eax, %r9d
	jne	.L98
	movl	%r10d, %eax
.L97:
	testl	%r12d, %r12d
	movw	%ax, (%r14,%rbx,2)
	jne	.L99
	testl	%r11d, %r11d
	jle	.L99
	movswl	-2(%r14,%rbx,2), %eax
	cmpl	%r9d, %eax
	cmovg	%r11d, %r12d
.L99:
	addq	$1, %rbx
	cmpq	%r15, %rbx
	jne	.L95
	testl	%r12d, %r12d
	je	.L93
	cmpl	%esi, %r12d
	jge	.L93
	movl	-92(%rsp), %eax
	movslq	%r12d, %rdx
	leal	-1(%r12), %r10d
	leaq	-90(%rsp,%rdx,2), %r9
	leaq	(%rdi,%rdx,8), %r8
	subl	%r12d, %eax
	addq	%rdx, %rax
	leaq	8(%rdi,%rax,8), %r11
	.p2align 4,,10
	.p2align 3
.L104:
	leaq	-2(%r9), %rdi
	movl	%r10d, %eax
	movq	%r8, %rdx
	addq	%rax, %rax
	subq	%rax, %rdi
	movq	%r9, %rax
	.p2align 4,,10
	.p2align 3
.L103:
	movzwl	(%rax), %ecx
	movzwl	2(%rax), %esi
	cmpw	%si, %cx
	jle	.L102
	movw	%si, (%rax)
	movw	%cx, 2(%rax)
	subq	$2, %rax
	movq	-8(%rdx), %rcx
	movq	(%rdx), %rsi
	subq	$8, %rdx
	movq	%rsi, (%rdx)
	movq	%rcx, 8(%rdx)
	cmpq	%rdi, %rax
	jne	.L103
.L102:
	addq	$8, %r8
	addl	$1, %r10d
	addq	$2, %r9
	cmpq	%r11, %r8
	jne	.L104
.L93:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	xorl	%eax, %eax
	xorl	%r9d, %r9d
	jmp	.L97
.L122:
	rep ret
	.size	addrsort, .-addrsort
	.p2align 4,,15
	.type	getanswer, @function
getanswer:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1144, %rsp
	cmpl	$12, %ecx
	movq	$0, host(%rip)
	movq	%rdi, 8(%rsp)
	movq	%rdx, 48(%rsp)
	movl	%ecx, 36(%rsp)
	je	.L127
	cmpl	$28, %ecx
	je	.L158
	cmpl	$1, %ecx
	je	.L158
	xorl	%eax, %eax
.L125:
	addq	$1144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	leaq	__GI___res_hnok(%rip), %rax
	movq	%rax, 16(%rsp)
.L128:
	movq	8(%rsp), %rax
	movslq	%esi, %rsi
	leaq	(%rax,%rsi), %rbp
	movzwl	6(%rax), %r12d
	movq	%rax, %rsi
	movzwl	4(%rax), %eax
	leaq	12(%rsi), %rbx
	rolw	$8, %r12w
	rolw	$8, %ax
	cmpq	%rbx, %rbp
	jb	.L154
	cmpw	$1, %ax
	jne	.L154
	movq	8(%rsp), %rdi
	leaq	hostbuf(%rip), %rcx
	movl	$8192, %r8d
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	call	__GI___dn_expand
	movslq	%eax, %r13
	testl	%r13d, %r13d
	js	.L154
	leaq	hostbuf(%rip), %rdi
	movq	16(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L154
	leaq	4(%rbx,%r13), %rbx
	cmpq	%rbx, %rbp
	jb	.L154
	movl	36(%rsp), %eax
	cmpl	$1, %eax
	sete	67(%rsp)
	movzbl	67(%rsp), %ecx
	cmpl	$28, %eax
	sete	%al
	orb	%cl, %al
	movb	%al, 66(%rsp)
	jne	.L228
	movq	48(%rsp), %rax
	leaq	hostbuf(%rip), %r13
	movl	$8192, 32(%rsp)
	movq	%rax, 80(%rsp)
.L133:
	leaq	host_aliases(%rip), %rax
	leaq	h_addr_ptrs(%rip), %rdx
	movzwl	%r12w, %r12d
	movq	$0, host_aliases(%rip)
	movq	$0, h_addr_ptrs(%rip)
	xorl	%r14d, %r14d
	movq	%rax, 8+host(%rip)
	movq	%rax, 56(%rsp)
	leaq	96(%rsp), %rax
	movq	%rdx, 24+host(%rip)
	movl	$0, 68(%rsp)
	movq	%rdx, 72(%rsp)
	movq	%rax, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L136:
	subl	$1, %r12d
	cmpl	$-1, %r12d
	je	.L151
.L231:
	cmpq	%rbp, %rbx
	jnb	.L151
	andl	$1, %r14d
	jne	.L151
	movl	32(%rsp), %r8d
	movq	8(%rsp), %rdi
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	call	__GI___dn_expand
	movslq	%eax, %r14
	testl	%r14d, %r14d
	js	.L227
	movq	%r13, %rdi
	movq	16(%rsp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L227
	addq	%r14, %rbx
	leaq	10(%rbx), %r15
	cmpq	%r15, %rbp
	jb	.L154
	movq	%rbx, %rdi
	call	__GI_ns_get16
	leaq	2(%rbx), %rdi
	movl	%eax, 24(%rsp)
	call	__GI_ns_get16
	leaq	8(%rbx), %rdi
	movl	%eax, %r14d
	call	__GI_ns_get16
	movslq	%eax, %rdx
	leaq	(%r15,%rdx), %rbx
	movq	%rdx, %rcx
	cmpq	%rbp, %rbx
	ja	.L154
	cmpl	$1, %r14d
	jne	.L226
	cmpb	$0, 66(%rsp)
	je	.L138
	cmpl	$5, 24(%rsp)
	je	.L229
.L139:
	movl	36(%rsp), %eax
	movl	24(%rsp), %esi
	cmpl	%esi, %eax
	jne	.L226
	cmpl	$12, %eax
	je	.L145
	cmpl	$28, %eax
	je	.L146
	cmpl	$1, %eax
	je	.L146
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L154:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	__GI___res_dnok(%rip), %rax
	movq	%rax, 16(%rsp)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L228:
	leaq	hostbuf(%rip), %rsi
	movq	%rsi, %r13
	movq	%rsi, %rax
.L134:
	movl	(%rax), %ecx
	addq	$4, %rax
	leal	-16843009(%rcx), %edx
	notl	%ecx
	andl	%ecx, %edx
	andl	$-2139062144, %edx
	je	.L134
	movl	%edx, %ecx
	shrl	$16, %ecx
	testl	$32896, %edx
	cmove	%ecx, %edx
	leaq	2(%rax), %rcx
	cmove	%rcx, %rax
	movl	%edx, %ecx
	addb	%dl, %cl
	sbbq	$3, %rax
	subq	%rsi, %rax
	addl	$1, %eax
	cmpl	$255, %eax
	jg	.L154
	movslq	%eax, %rdx
	movq	%r13, host(%rip)
	addq	%rdx, %r13
	movl	$8192, %edx
	subl	%eax, %edx
	leaq	hostbuf(%rip), %rax
	movl	%edx, 32(%rsp)
	movq	%rax, 80(%rsp)
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L145:
	movq	48(%rsp), %rdi
	movq	%r13, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L226
	movl	32(%rsp), %r8d
	movq	8(%rsp), %rdi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	call	__GI___dn_expand
	movslq	%eax, %r14
	testl	%r14d, %r14d
	js	.L177
	movq	%r13, %rdi
	call	__GI___res_hnok
	testl	%eax, %eax
	jne	.L230
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%r15, %rbx
	.p2align 4,,10
	.p2align 3
.L227:
	subl	$1, %r12d
	movl	$1, %r14d
	cmpl	$-1, %r12d
	jne	.L231
.L151:
	movl	68(%rsp), %edx
	testl	%edx, %edx
	je	.L154
	movq	56(%rsp), %rax
	movq	$0, (%rax)
	movq	72(%rsp), %rax
	movq	$0, (%rax)
	movq	__resp@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	testb	$-16, 392(%rax)
	je	.L155
	cmpl	$1, %edx
	jle	.L155
	cmpb	$0, 67(%rsp)
	jne	.L232
	.p2align 4,,10
	.p2align 3
.L155:
	cmpq	$0, host(%rip)
	je	.L233
.L156:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	leaq	host(%rip), %rax
	jmp	.L125
.L230:
	leaq	(%r15,%r14), %r9
	cmpq	%r9, %rbx
	jne	.L154
	movl	68(%rsp), %edx
	testl	%edx, %edx
	jne	.L147
	movq	%r13, host(%rip)
.L148:
	movq	%r13, %rdi
	movl	$1, %r14d
	call	strlen@PLT
	addl	$1, %eax
	cmpl	$255, %eax
	jg	.L136
	subl	%eax, 32(%rsp)
	movslq	%eax, %rdx
	addq	%rdx, %r13
.L149:
	addl	$1, 68(%rsp)
	.p2align 4,,10
	.p2align 3
.L226:
	xorl	%r14d, %r14d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L138:
	cmpl	$12, 36(%rsp)
	jne	.L139
	cmpl	$5, 24(%rsp)
	jne	.L139
	movq	40(%rsp), %rcx
	movq	8(%rsp), %rdi
	movl	$1025, %r8d
	movq	%r15, %rdx
	movq	%rbp, %rsi
	call	__GI___dn_expand
	movslq	%eax, %r14
	testl	%r14d, %r14d
	js	.L177
	movq	40(%rsp), %rdi
	call	__GI___res_dnok
	testl	%eax, %eax
	je	.L177
	leaq	(%r15,%r14), %r9
	cmpq	%r9, %rbx
	jne	.L154
	movq	40(%rsp), %rdx
.L142:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L142
	movl	%eax, %ecx
	movq	40(%rsp), %rsi
	movl	32(%rsp), %r15d
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%rsi, %rdx
	leal	1(%rdx), %r14d
	cmpl	%r15d, %r14d
	jg	.L227
	cmpl	$255, %r14d
	jg	.L227
	movq	%r13, %rdi
	addq	$1, %rdx
	subl	%r14d, %r15d
	call	memcpy@PLT
	movslq	%r14d, %rax
	movq	%r13, 48(%rsp)
	movl	%r15d, 32(%rsp)
	addq	%rax, %r13
	xorl	%r14d, %r14d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	272+host_aliases(%rip), %rcx
	cmpq	%rcx, 56(%rsp)
	jnb	.L163
	movq	40(%rsp), %rcx
	movq	8(%rsp), %rdi
	movl	$1025, %r8d
	movq	%r15, %rdx
	movq	%rbp, %rsi
	call	__GI___dn_expand
	movslq	%eax, %r14
	testl	%r14d, %r14d
	js	.L177
	movq	40(%rsp), %rdi
	movq	16(%rsp), %rcx
	call	*%rcx
	testl	%eax, %eax
	je	.L177
	leaq	(%r15,%r14), %r9
	cmpq	%r9, %rbx
	jne	.L154
	movq	56(%rsp), %rax
	movq	%r13, %rdi
	movq	%r13, (%rax)
	leaq	8(%rax), %r14
	call	strlen@PLT
	addl	$1, %eax
	cmpl	$255, %eax
	jg	.L167
	movslq	%eax, %rdx
	subl	%eax, 32(%rsp)
	addq	%rdx, %r13
	movq	40(%rsp), %rdx
.L140:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L140
	movl	%eax, %ecx
	movq	40(%rsp), %rsi
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	cmove	%rcx, %rdx
	movl	%eax, %ecx
	addb	%al, %cl
	sbbq	$3, %rdx
	subq	%rsi, %rdx
	leal	1(%rdx), %r15d
	cmpl	%r15d, 32(%rsp)
	jl	.L167
	cmpl	$255, %r15d
	jg	.L167
	movq	%r13, %rdi
	addq	$1, %rdx
	call	memcpy@PLT
	movslq	%r15d, %rax
	movq	%r13, host(%rip)
	movq	%r14, 56(%rsp)
	addq	%rax, %r13
	subl	%r15d, 32(%rsp)
	xorl	%r14d, %r14d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L146:
	movq	host(%rip), %rdi
	movq	%r13, %rsi
	movl	%ecx, 24(%rsp)
	movq	%rdx, 88(%rsp)
	call	strcasecmp@PLT
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L226
	movl	24(%rsp), %ecx
	cmpl	%ecx, 20+host(%rip)
	jne	.L136
	movl	68(%rsp), %eax
	movq	88(%rsp), %rdx
	testl	%eax, %eax
	je	.L234
.L150:
	movl	%ecx, 88(%rsp)
	movl	32(%rsp), %ecx
	movl	%r13d, %eax
	andq	$-4, %r13
	andl	$3, %eax
	addq	$4, %r13
	leal	-4(%rcx,%rax), %eax
	leaq	0(%r13,%rdx), %r8
	leaq	8192+hostbuf(%rip), %rcx
	cmpq	%rcx, %r8
	movl	%eax, 32(%rsp)
	movq	%r8, 24(%rsp)
	jnb	.L177
	movq	72(%rsp), %rcx
	leaq	272+h_addr_ptrs(%rip), %rsi
	cmpq	%rsi, %rcx
	jnb	.L136
	movq	%r13, (%rcx)
	movq	%r13, %rdi
	movq	%r15, %rsi
	leaq	8(%rcx), %r14
	call	memmove@PLT
	movq	24(%rsp), %r8
	movl	88(%rsp), %ecx
	movq	%r14, 72(%rsp)
	subl	%ecx, 32(%rsp)
	movq	%r8, %r13
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r15, %rbx
	xorl	%r14d, %r14d
	jmp	.L136
.L167:
	movq	%r14, 56(%rsp)
	movl	$1, %r14d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L233:
	movq	80(%rsp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	leal	1(%rax), %edx
	cmpl	32(%rsp), %edx
	jg	.L154
	cmpl	$255, %edx
	jg	.L154
	leaq	1(%rax), %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	%r13, host(%rip)
	jmp	.L156
.L234:
	movq	%r13, %rdi
	movl	%ecx, 88(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%r13, host(%rip)
	call	strlen@PLT
	addl	$1, %eax
	movl	88(%rsp), %ecx
	subl	%eax, 32(%rsp)
	movslq	%eax, %rsi
	movq	24(%rsp), %rdx
	addq	%rsi, %r13
	jmp	.L150
.L232:
	leaq	h_addr_ptrs(%rip), %rdi
	movl	%edx, %esi
	call	addrsort@PLT
	jmp	.L155
.L147:
	movq	56(%rsp), %rax
	leaq	272+host_aliases(%rip), %rdx
	cmpq	%rdx, %rax
	jnb	.L149
	movq	%r13, (%rax)
	addq	$8, %rax
	movq	%rax, 56(%rsp)
	jmp	.L148
	.size	getanswer, .-getanswer
	.section	.rodata.str1.1
.LC4:
	.string	"%u.%u.%u.%u.in-addr.arpa"
.LC5:
	.string	"%x.%x."
	.text
	.p2align 4,,15
	.type	res_gethostbyaddr_context, @function
res_gethostbyaddr_context:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbx
	subq	$1080, %rsp
	cmpl	$10, %ecx
	movq	%rdi, -1112(%rbp)
	jne	.L236
	cmpl	$16, %edx
	jne	.L236
	movq	(%rsi), %rax
	cmpq	mapped.11655(%rip), %rax
	je	.L267
.L237:
	cmpq	tunnelled.11656(%rip), %rax
	je	.L268
.L242:
	leaq	-1088(%rbp), %rax
	leaq	15(%r13), %r15
	leaq	-1(%r13), %r12
	leaq	.LC5(%rip), %rbx
	movq	%rax, -1120(%rbp)
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L249:
	movzbl	(%r15), %edx
	movq	%r14, %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	subq	$1, %r15
	addq	$4, %r14
	movl	%edx, %ecx
	andl	$15, %edx
	shrb	$4, %cl
	movzbl	%cl, %ecx
	call	sprintf@PLT
	cmpq	%r15, %r12
	jne	.L249
	movabsq	$7021237580783317097, %rax
	movb	$0, 8(%r14)
	movl	$10, %r15d
	movq	%rax, (%r14)
	movl	$16, %ebx
.L248:
	subq	$1040, %rsp
	leaq	-1096(%rbp), %rax
	movq	-1120(%rbp), %rsi
	leaq	15(%rsp), %r10
	movq	-1112(%rbp), %rdi
	subq	$8, %rsp
	pushq	$0
	pushq	$0
	movl	$1024, %r9d
	andq	$-16, %r10
	pushq	$0
	pushq	$0
	pushq	%rax
	movq	%r10, %r8
	movl	$12, %ecx
	movl	$1, %edx
	movq	%r10, %r12
	movq	%r10, -1096(%rbp)
	call	__GI___res_context_query
	addq	$48, %rsp
	testl	%eax, %eax
	jns	.L250
	movq	-1096(%rbp), %rdi
	cmpq	%r12, %rdi
	je	.L251
	call	free@PLT
.L251:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	cmpl	$111, %fs:(%rax)
	jne	.L235
	movl	%ebx, %esi
	movl	%r15d, %edx
	movq	%r13, %rdi
	call	__GI__gethtbyaddr
	movq	%rax, %r14
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L236:
	cmpl	$2, %ecx
	je	.L245
	cmpl	$10, %ecx
	je	.L246
	movq	errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$97, %fs:(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
.L235:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	movq	-1096(%rbp), %rdi
	movq	-1120(%rbp), %rdx
	movl	$12, %ecx
	movl	%eax, %esi
	call	getanswer
	movq	-1096(%rbp), %rdi
	movq	%rax, %r14
	cmpq	%r12, %rdi
	je	.L252
	call	free@PLT
.L252:
	testq	%r14, %r14
	je	.L235
	leaq	host_addr(%rip), %rdi
	movl	%r15d, 16(%r14)
	movl	%ebx, 20(%r14)
	movl	%ebx, %edx
	movq	%r13, %rsi
	call	memmove@PLT
	leaq	host_addr(%rip), %rax
	movq	$0, 8+h_addr_ptrs(%rip)
	movq	%rax, h_addr_ptrs(%rip)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L246:
	cmpl	$16, %edx
	je	.L242
.L253:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$22, %fs:(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L267:
	movl	8+mapped.11655(%rip), %ebx
	cmpl	%ebx, 8(%rsi)
	jne	.L237
.L239:
	addq	$12, %r13
.L243:
	movzbl	2(%r13), %ecx
	movzbl	3(%r13), %edx
	leaq	-1088(%rbp), %rax
	movzbl	0(%r13), %r9d
	movzbl	1(%r13), %r8d
	leaq	.LC4(%rip), %rsi
	movq	%rax, -1120(%rbp)
	movq	%rax, %rdi
	xorl	%eax, %eax
	movl	$2, %r15d
	movl	$4, %ebx
	call	sprintf@PLT
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L245:
	cmpl	$4, %edx
	je	.L243
	jmp	.L253
	.p2align 4,,10
	.p2align 3
.L268:
	movl	8+tunnelled.11656(%rip), %eax
	cmpl	%eax, 8(%r13)
	jne	.L242
	jmp	.L239
	.size	res_gethostbyaddr_context, .-res_gethostbyaddr_context
	.p2align 4,,15
	.globl	res_gethostbyaddr
	.type	res_gethostbyaddr, @function
res_gethostbyaddr:
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %r12d
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L273
	movq	%rax, %rbx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movl	%r13d, %ecx
	movl	%r12d, %edx
	call	res_gethostbyaddr_context
	movq	%rbx, %rdi
	movq	%rax, %rbp
	call	__resolv_context_put@PLT
.L269:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	movq	__h_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$-1, %fs:(%rax)
	jmp	.L269
	.size	res_gethostbyaddr, .-res_gethostbyaddr
	.p2align 4,,15
	.type	res_gethostbyname2_context.constprop.2, @function
res_gethostbyname2_context.constprop.2:
	pushq	%rbp
	movabsq	$17179869186, %rax
	movq	%rsp, %rbp
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	$46, %esi
	movq	%rbx, %rdi
	subq	$1072, %rsp
	movq	%rax, 16+host(%rip)
	call	strchr@PLT
	testq	%rax, %rax
	je	.L276
	leaq	-1072(%rbp), %rdx
	movq	%rbx, %rsi
	movl	$1025, %ecx
	movq	%r12, %rdi
	call	__GI___res_context_hostalias
	testq	%rax, %rax
	cmovne	%rax, %rbx
.L276:
	call	__ctype_b_loc@PLT
	movq	(%rax), %r13
	movsbq	(%rbx), %rax
	movzwl	0(%r13,%rax,2), %esi
	movq	%rax, %r14
	testw	$2048, %si
	je	.L277
	testb	%al, %al
	movq	%rbx, %rdx
	je	.L282
	.p2align 4,,10
	.p2align 3
.L278:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L282
	movsbq	%al, %rcx
	testb	$8, 1(%r13,%rcx,2)
	jne	.L278
	cmpb	$46, %al
	je	.L278
.L277:
	testw	$4096, %si
	jne	.L333
.L284:
	cmpb	$58, %r14b
	je	.L334
.L287:
	subq	$1040, %rsp
	leaq	-1080(%rbp), %rax
	movl	$1024, %r9d
	leaq	15(%rsp), %r13
	subq	$8, %rsp
	movl	$1, %ecx
	pushq	$0
	pushq	$0
	movl	$1, %edx
	pushq	$0
	andq	$-16, %r13
	pushq	$0
	pushq	%rax
	movq	%r13, %r8
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r13, -1080(%rbp)
	call	__GI___res_context_search
	addq	$48, %rsp
	testl	%eax, %eax
	jns	.L292
	movq	-1080(%rbp), %rdi
	cmpq	%rdi, %r13
	je	.L293
	call	free@PLT
.L293:
	movq	errno@gottpoff(%rip), %rdx
	xorl	%eax, %eax
	cmpl	$111, %fs:(%rdx)
	jne	.L274
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__GI__gethtbyname2
.L274:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L333:
	movl	$58, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L284
	testb	%r14b, %r14b
	movq	%rbx, %rdx
	je	.L285
	.p2align 4,,10
	.p2align 3
.L290:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L285
.L327:
	movsbq	%al, %rcx
	testb	$16, 1(%r13,%rcx,2)
	jne	.L290
	cmpb	$46, %al
	setne	%cl
	cmpb	$58, %al
	setne	%al
	testb	%al, %cl
	jne	.L287
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	jne	.L327
.L285:
	cmpb	$46, -1(%rdx)
	je	.L287
.L330:
	leaq	host_addr(%rip), %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	inet_pton@PLT
	testl	%eax, %eax
	jle	.L335
	leaq	hostbuf(%rip), %rdi
	movq	%rbx, %rsi
	movl	$1025, %edx
	call	strncpy@PLT
	leaq	hostbuf(%rip), %rax
	movb	$0, 1025+hostbuf(%rip)
	movq	$0, host_aliases(%rip)
	movq	$0, 8+h_addr_ptrs(%rip)
	movq	%rax, host(%rip)
	leaq	host_aliases(%rip), %rax
	movq	%rax, 8+host(%rip)
	leaq	host_addr(%rip), %rax
	movq	%rax, h_addr_ptrs(%rip)
	leaq	h_addr_ptrs(%rip), %rax
	movq	%rax, 24+host(%rip)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$0, %fs:(%rax)
	leaq	-32(%rbp), %rsp
	leaq	host(%rip), %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L282:
	cmpb	$46, -1(%rdx)
	jne	.L330
	testw	$4096, %si
	je	.L284
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L292:
	movq	-1080(%rbp), %rdi
	movl	$1, %ecx
	movq	%rbx, %rdx
	movl	%eax, %esi
	call	getanswer
	movq	-1080(%rbp), %rdi
	cmpq	%rdi, %r13
	je	.L274
	movq	%rax, -1096(%rbp)
	call	free@PLT
	movq	-1096(%rbp), %rax
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L334:
	movq	%rbx, %rdx
	jmp	.L290
.L335:
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$1, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L274
	.size	res_gethostbyname2_context.constprop.2, .-res_gethostbyname2_context.constprop.2
	.p2align 4,,15
	.globl	__GI_res_gethostbyname2
	.hidden	__GI_res_gethostbyname2
	.type	__GI_res_gethostbyname2, @function
__GI_res_gethostbyname2:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L340
	movq	%rax, %rbx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	res_gethostbyname2_context.constprop.2
	movq	%rbx, %rdi
	movq	%rax, %rbp
	call	__resolv_context_put@PLT
.L336:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L340:
	movq	__h_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$-1, %fs:(%rax)
	jmp	.L336
	.size	__GI_res_gethostbyname2, .-__GI_res_gethostbyname2
	.globl	res_gethostbyname2
	.set	res_gethostbyname2,__GI_res_gethostbyname2
	.p2align 4,,15
	.globl	res_gethostbyname
	.type	res_gethostbyname, @function
res_gethostbyname:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	testq	%rax, %rax
	je	.L345
	movq	%rax, %rbx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	res_gethostbyname2_context.constprop.2
	movq	%rbx, %rdi
	movq	%rax, %rbp
	call	__resolv_context_put@PLT
.L341:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L345:
	movq	__h_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$-1, %fs:(%rax)
	jmp	.L341
	.size	res_gethostbyname, .-res_gethostbyname
	.section	.rodata
	.align 8
	.type	tunnelled.11656, @object
	.size	tunnelled.11656, 12
tunnelled.11656:
	.zero	12
	.align 8
	.type	mapped.11655, @object
	.size	mapped.11655, 12
mapped.11655:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-1
	.byte	-1
	.local	stayopen
	.comm	stayopen,4,4
	.local	hostf
	.comm	hostf,8,8
	.local	host_addr
	.comm	host_addr,16,16
	.local	hostbuf
	.comm	hostbuf,8192,32
	.local	host_aliases
	.comm	host_aliases,280,32
	.local	host
	.comm	host,32,32
	.local	h_addr_ptrs
	.comm	h_addr_ptrs,288,32
