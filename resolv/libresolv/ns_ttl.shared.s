	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%d%c"
	.text
	.p2align 4,,15
	.type	fmt1, @function
fmt1:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movsbl	%sil, %ecx
	leaq	.LC0(%rip), %rsi
	movq	%rdx, %r12
	subq	$72, %rsp
	movl	%edi, %edx
	movq	%rsp, %r13
	movq	%r13, %rdi
	call	sprintf@PLT
	movslq	%eax, %rbx
	leaq	1(%rbx), %rax
	cmpq	0(%rbp), %rax
	ja	.L3
	movq	(%r12), %rdi
	movq	%r13, %rsi
	call	strcpy@PLT
	addq	%rbx, (%r12)
	subq	%rbx, 0(%rbp)
	xorl	%eax, %eax
.L1:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	fmt1, .-fmt1
	.p2align 4,,15
	.globl	__GI_ns_format_ttl
	.hidden	__GI_ns_format_ttl
	.type	__GI_ns_format_ttl, @function
__GI_ns_format_ttl:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movabsq	$-8608480567731124087, %rbp
	movabsq	$3997770567508694361, %rbx
	subq	$56, %rsp
	movq	%rdx, 32(%rsp)
	movq	%rsi, 40(%rsp)
	mulq	%rbp
	movq	%rdx, %r8
	movq	%rdx, %rcx
	shrq	$5, %r8
	shrq	$5, %rcx
	movq	%r8, %rax
	salq	$4, %rax
	subq	%r8, %rax
	movq	%rdi, %r8
	salq	$2, %rax
	subq	%rax, %r8
	movq	%rcx, %rax
	mulq	%rbp
	movq	%rdx, %rbp
	movq	%rdi, %rdx
	shrq	$5, %rbp
	shrq	$4, %rdx
	movq	%rbp, %rax
	salq	$4, %rax
	subq	%rbp, %rax
	salq	$2, %rax
	subq	%rax, %rcx
	movq	%rdx, %rax
	movq	%rcx, %rbp
	movabsq	$655884233731895169, %rcx
	mulq	%rcx
	movq	%rdx, %rcx
	movabsq	$-6148914691236517205, %rdx
	shrq	$3, %rcx
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$4, %rdx
	leaq	(%rdx,%rdx,2), %rax
	movabsq	$-4454547087429121353, %rdx
	salq	$3, %rax
	subq	%rax, %rcx
	movq	%rdi, %rax
	mulq	%rdx
	movq	%rcx, %r13
	movl	%ecx, %r15d
	movq	%rdx, %rcx
	movabsq	$5270498306774157605, %rdx
	shrq	$16, %rcx
	movq	%rcx, %rax
	imulq	%rdx
	sarq	%rdx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	subq	%rax, %rcx
	movq	%rdi, %rax
	mulq	%rbx
	xorl	%ebx, %ebx
	movq	%rcx, %r12
	movl	%ecx, 8(%rsp)
	shrq	$17, %rdx
	testl	%edx, %edx
	movq	%rdx, %r9
	jne	.L38
	testq	%r12, %r12
	jne	.L39
.L9:
	testq	%r13, %r13
	jne	.L40
.L11:
	testq	%rbp, %rbp
	jne	.L41
.L12:
	testq	%r8, %r8
	jne	.L37
	orl	8(%rsp), %r15d
	orl	%r9d, %r15d
	jne	.L14
.L37:
	leaq	32(%rsp), %r11
	leaq	40(%rsp), %r10
.L13:
	movq	%r11, %rcx
	movq	%r10, %rdx
	movl	$83, %esi
	movl	%r8d, %edi
	call	fmt1
	testl	%eax, %eax
	js	.L10
	addl	$1, %ebx
.L14:
	cmpl	$1, %ebx
	jle	.L15
	movsbq	(%r14), %rbx
	testb	%bl, %bl
	je	.L15
	movq	%r14, %rbp
	.p2align 4,,10
	.p2align 3
.L17:
	testb	%bl, %bl
	js	.L16
	call	__ctype_b_loc@PLT
	movq	(%rax), %rax
	testb	$1, 1(%rax,%rbx,2)
	je	.L16
	call	__ctype_tolower_loc@PLT
	movq	(%rax), %rax
	movl	(%rax,%rbx,4), %eax
	movb	%al, 0(%rbp)
.L16:
	addq	$1, %rbp
	movsbq	0(%rbp), %rbx
	testb	%bl, %bl
	jne	.L17
.L15:
	movl	40(%rsp), %eax
	subl	%r14d, %eax
.L6:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	32(%rsp), %r11
	leaq	40(%rsp), %r10
	movl	%r9d, %edi
	movl	$87, %esi
	movq	%r8, 24(%rsp)
	movq	%r9, 16(%rsp)
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	fmt1
	testl	%eax, %eax
	movq	16(%rsp), %r9
	movq	24(%rsp), %r8
	js	.L10
	testq	%r12, %r12
	movl	$1, %ebx
	je	.L9
.L39:
	leaq	32(%rsp), %r11
	leaq	40(%rsp), %r10
	movl	8(%rsp), %edi
	movl	$68, %esi
	movq	%r9, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	fmt1
	testl	%eax, %eax
	js	.L10
	addl	$1, %ebx
	testq	%r13, %r13
	movq	24(%rsp), %r9
	movq	16(%rsp), %r8
	je	.L11
.L40:
	leaq	32(%rsp), %r11
	leaq	40(%rsp), %r10
	movl	$72, %esi
	movl	%r15d, %edi
	movq	%r9, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r11, %rcx
	movq	%r10, %rdx
	call	fmt1
	testl	%eax, %eax
	js	.L10
	addl	$1, %ebx
	testq	%rbp, %rbp
	movq	24(%rsp), %r9
	movq	16(%rsp), %r8
	je	.L12
.L41:
	leaq	32(%rsp), %r11
	leaq	40(%rsp), %r10
	movl	$77, %esi
	movl	%ebp, %edi
	movq	%r8, 24(%rsp)
	movq	%r11, %rcx
	movq	%r10, %rdx
	movq	%r11, 16(%rsp)
	movq	%r10, 8(%rsp)
	call	fmt1
	testl	%eax, %eax
	js	.L10
	movq	24(%rsp), %r8
	addl	$1, %ebx
	movq	8(%rsp), %r10
	movq	16(%rsp), %r11
	testq	%r8, %r8
	je	.L14
	jmp	.L13
.L10:
	movl	$-1, %eax
	jmp	.L6
	.size	__GI_ns_format_ttl, .-__GI_ns_format_ttl
	.globl	ns_format_ttl
	.set	ns_format_ttl,__GI_ns_format_ttl
	.p2align 4,,15
	.globl	ns_parse_ttl
	.type	ns_parse_ttl, @function
ns_parse_ttl:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	xorl	%r14d, %r14d
	pushq	%rbp
	pushq	%rbx
	leaq	.L49(%rip), %rbp
	xorl	%r13d, %r13d
	subq	$40, %rsp
	movq	%rsi, 24(%rsp)
	movl	$0, 20(%rsp)
	movq	$0, 8(%rsp)
.L43:
	addq	$1, %r15
	movsbq	-1(%r15), %r12
	movsbl	%r12b, %ebx
	testl	%ebx, %ebx
	je	.L71
	testb	%r12b, %r12b
	js	.L44
	call	__ctype_b_loc@PLT
	movq	(%rax), %rax
	movzwl	(%rax,%r12,2), %eax
	testb	$64, %ah
	je	.L44
	testb	$8, %ah
	jne	.L72
	testl	%r14d, %r14d
	je	.L44
	testb	$2, %ah
	je	.L47
	call	__ctype_toupper_loc@PLT
	movq	(%rax), %rax
	movl	(%rax,%r12,4), %ebx
.L47:
	subl	$68, %ebx
	cmpl	$19, %ebx
	ja	.L44
	movslq	0(%rbp,%rbx,4), %rax
	addq	%rbp, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L49:
	.long	.L48-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L50-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L51-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L52-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L44-.L49
	.long	.L53-.L49
	.text
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	0(,%r13,8), %rax
	subq	%r13, %rax
	movq	%rax, %r13
.L48:
	leaq	0(%r13,%r13,2), %rcx
	leaq	0(,%rcx,8), %r13
.L50:
	movq	%r13, %rax
	salq	$4, %rax
	subq	%r13, %rax
	leaq	0(,%rax,4), %r13
.L51:
	movq	%r13, %rax
	salq	$4, %rax
	subq	%r13, %rax
	leaq	0(,%rax,4), %r13
.L52:
	addq	%r13, 8(%rsp)
	movl	$1, 20(%rsp)
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	movl	20(%rsp), %eax
	testl	%eax, %eax
	jne	.L56
	.p2align 4,,10
	.p2align 3
.L44:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$22, %fs:(%rax)
.L42:
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	leaq	0(%r13,%r13,4), %rax
	subl	$48, %ebx
	addl	$1, %r14d
	movslq	%ebx, %rbx
	leaq	(%rbx,%rax,2), %r13
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L71:
	testl	%r14d, %r14d
	je	.L55
	movl	20(%rsp), %edx
	testl	%edx, %edx
	jne	.L44
	addq	%r13, 8(%rsp)
.L56:
	movq	24(%rsp), %rax
	movq	8(%rsp), %rdx
	movq	%rdx, (%rax)
	jmp	.L42
	.size	ns_parse_ttl, .-ns_parse_ttl
