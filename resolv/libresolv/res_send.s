	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"res_send.c"
.LC1:
	.string	"n < statp->nscount"
	.text
	.p2align 4,,15
	.type	get_nsaddr.part.1, @function
get_nsaddr.part.1:
	leaq	__PRETTY_FUNCTION__.12837(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$614, %edx
	call	__assert_fail@PLT
	.size	get_nsaddr.part.1, .-get_nsaddr.part.1
	.p2align 4,,15
	.type	reopen, @function
reopen:
	movslq	%edx, %rax
	pushq	%r15
	pushq	%r14
	pushq	%r13
	leaq	(%rdi,%rax,4), %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	520(%r13), %r12d
	cmpl	$-1, %r12d
	je	.L5
.L15:
	movl	$1, %r12d
.L4:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	16(%rdi), %edx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	jnb	.L22
	movl	%edx, %edx
	leaq	1(%rdx), %rax
	salq	$4, %rax
	cmpw	$0, 4(%rdi,%rax)
	je	.L23
.L8:
	salq	$4, %rdx
	leaq	20(%rbx,%rdx), %r14
	movzwl	(%r14), %eax
	cmpw	$10, %ax
	je	.L24
.L10:
	cmpw	$2, %ax
	je	.L25
.L20:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L16:
	movl	%eax, 0(%rbp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L23:
	movq	536(%rdi,%rdx,8), %r14
	testq	%r14, %r14
	je	.L8
	movzwl	(%r14), %eax
	cmpw	$10, %ax
	jne	.L10
.L24:
	testb	$1, 393(%rbx)
	jne	.L20
	xorl	%edx, %edx
	movl	$526338, %esi
	movl	$10, %edi
	call	socket@PLT
	testl	%eax, %eax
	movl	%eax, %esi
	movl	%eax, 520(%r13)
	movl	$28, %r15d
	jns	.L12
	movq	errno@gottpoff(%rip), %rax
	movzbl	393(%rbx), %edx
	movl	%fs:(%rax), %eax
	cmpl	$97, %eax
	sete	%cl
	andl	$-2, %edx
	orl	%ecx, %edx
	movb	%dl, 393(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%edx, %edx
	movl	$526338, %esi
	movl	$2, %edi
	call	socket@PLT
	testl	%eax, %eax
	movl	%eax, %esi
	movl	%eax, 520(%r13)
	js	.L20
	movl	$16, %r15d
.L12:
	movzwl	(%r14), %edi
	call	__res_enable_icmp@PLT
	testl	%eax, %eax
	js	.L26
	movl	520(%r13), %edi
	movl	%r15d, %edx
	movq	%r14, %rsi
	call	connect@PLT
	testl	%eax, %eax
	jns	.L15
	xorl	%esi, %esi
	movq	%rbx, %rdi
	xorl	%r12d, %r12d
	call	__res_iclose@PLT
	jmp	.L4
.L26:
	movq	errno@gottpoff(%rip), %r14
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%fs:(%r14), %r13d
	call	__res_iclose@PLT
	movl	%r13d, %fs:(%r14)
	movl	%r13d, 0(%rbp)
	jmp	.L4
.L22:
	call	get_nsaddr.part.1
	.size	reopen, .-reopen
	.p2align 4,,15
	.type	sock_eq, @function
sock_eq:
	movzwl	(%rdi), %eax
	movzwl	2(%rdi), %edx
	cmpw	(%rsi), %ax
	movzwl	2(%rsi), %ecx
	je	.L43
	cmpw	$2, %ax
	je	.L33
	movl	%ecx, %eax
	movl	%edx, %ecx
	movl	%eax, %edx
	movq	%rsi, %rax
	movq	%rdi, %rsi
	movq	%rax, %rdi
.L33:
	xorl	%eax, %eax
	cmpw	%dx, %cx
	je	.L44
.L27:
	rep ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	jne	.L27
	movl	12(%rsi), %edx
	testl	%edx, %edx
	jne	.L27
	cmpl	$-65536, 16(%rsi)
	jne	.L27
	movl	4(%rdi), %eax
	cmpl	%eax, 20(%rsi)
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	cmpw	$2, %ax
	movl	$0, %eax
	je	.L45
	cmpw	%dx, %cx
	jne	.L27
	movq	8(%rdi), %rax
	movq	16(%rdi), %rdx
	xorq	8(%rsi), %rax
	xorq	16(%rsi), %rdx
	orq	%rax, %rdx
	sete	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpw	%dx, %cx
	jne	.L27
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
	sete	%al
	movzbl	%al, %eax
	ret
	.size	sock_eq, .-sock_eq
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"anscp != NULL || ansp2 == NULL"
	.text
	.p2align 4,,15
	.type	send_vc, @function
send_vc:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$696, %rsp
	movl	768(%rsp), %eax
	movq	%rsi, 32(%rsp)
	cmpl	16(%rdi), %eax
	movl	%edx, 72(%rsp)
	movq	%rcx, 24(%rsp)
	movl	%r8d, 76(%rsp)
	movq	%r9, 16(%rsp)
	jnb	.L138
	leaq	1(%rax), %rdx
	movq	%rdi, %r15
	salq	$4, %rdx
	cmpw	$0, 4(%rdi,%rdx)
	jne	.L48
	movq	536(%rdi,%rax,8), %rcx
	testq	%rcx, %rcx
	movq	%rcx, 56(%rsp)
	je	.L48
.L49:
	leaq	176(%rsp), %rax
	movl	$0, 44(%rsp)
	movq	%rax, 80(%rsp)
	leaq	108(%rsp), %rax
	movq	%rax, 88(%rsp)
	leaq	104(%rsp), %rax
	movq	%rax, 48(%rsp)
.L50:
	movl	500(%r15), %edi
	testl	%edi, %edi
	js	.L52
	testb	$1, 504(%r15)
	jne	.L139
.L53:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
.L52:
	movq	56(%rsp), %rax
	xorl	%edx, %edx
	movl	$524289, %esi
	movzwl	(%rax), %edi
	call	socket@PLT
	testl	%eax, %eax
	movl	%eax, 500(%r15)
	js	.L140
	movq	56(%rsp), %rcx
	movq	errno@gottpoff(%rip), %rbx
	movl	$16, %esi
	movl	$28, %edx
	movl	%eax, %edi
	cmpw	$2, (%rcx)
	movl	$0, %fs:(%rbx)
	cmove	%esi, %edx
	movq	%rcx, %rsi
	call	connect@PLT
	testl	%eax, %eax
	js	.L141
	orl	$1, 504(%r15)
.L57:
	movl	72(%rsp), %ecx
	movq	24(%rsp), %rsi
	movl	$2, %ebx
	movq	$2, 120(%rsp)
	movl	%ecx, %eax
	leal	2(%rcx), %r12d
	rolw	$8, %ax
	testq	%rsi, %rsi
	movw	%ax, 98(%rsp)
	leaq	98(%rsp), %rax
	movslq	%r12d, %r12
	movq	%rax, 112(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 128(%rsp)
	movslq	%ecx, %rax
	movq	%rax, 136(%rsp)
	je	.L63
	movl	76(%rsp), %ecx
	movq	$2, 152(%rsp)
	movl	$4, %ebx
	movq	%rsi, 160(%rsp)
	movl	%ecx, %eax
	rolw	$8, %ax
	movw	%ax, 100(%rsp)
	leaq	100(%rsp), %rax
	movq	%rax, 144(%rsp)
	movslq	%ecx, %rax
	movq	%rax, 168(%rsp)
	leal	2(%rcx), %eax
	cltq
	addq	%rax, %r12
.L63:
	leaq	112(%rsp), %rbp
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L142:
	movq	errno@gottpoff(%rip), %rdx
	cmpl	$4, %fs:(%rdx)
	jne	.L64
.L65:
	movl	500(%r15), %edi
	movl	%ebx, %edx
	movq	%rbp, %rsi
	call	writev@PLT
	cmpq	$-1, %rax
	je	.L142
.L64:
	cmpq	%rax, %r12
	jne	.L137
	cmpq	$0, 24(%rsp)
	movl	$0, 4(%rsp)
	movq	%r15, %r13
	sete	43(%rsp)
	movzbl	43(%rsp), %eax
	xorl	%r12d, %r12d
	movl	%eax, (%rsp)
	leaq	102(%rsp), %rax
	movq	%rax, 8(%rsp)
.L68:
	movq	8(%rsp), %rbx
	movl	$2, %edi
	movl	$2, %edx
	movw	%di, 98(%rsp)
	.p2align 4,,10
	.p2align 3
.L69:
	movl	500(%r13), %edi
	movzwl	%dx, %edx
	movq	%rbx, %rsi
	call	read@PLT
	cmpq	$-1, %rax
	je	.L143
	testl	%eax, %eax
	jle	.L144
	movslq	%eax, %rdx
	addq	%rdx, %rbx
	movzwl	98(%rsp), %edx
	subl	%eax, %edx
	testw	%dx, %dx
	movw	%dx, 98(%rsp)
	jne	.L69
	movzwl	102(%rsp), %ebx
	movl	(%rsp), %ecx
	rolw	$8, %bx
	orl	%r12d, %ecx
	movzwl	%bx, %eax
	je	.L94
	cmpb	$0, 43(%rsp)
	jne	.L94
	movq	800(%rsp), %rdx
	movq	784(%rsp), %rsi
	movq	792(%rsp), %r15
.L75:
	movq	(%rsi), %r14
	movl	%eax, (%rdx)
	movl	(%r15), %edi
	cmpl	%eax, %edi
	jge	.L77
	cmpq	16(%rsp), %rsi
	movq	%rsi, 64(%rsp)
	jne	.L145
.L78:
	movl	%edi, %edx
	movw	%di, 98(%rsp)
	movq	16(%rsp), %rsi
	cmpw	$11, %dx
	movl	$1, 4(%rsp)
	jbe	.L146
.L122:
	movq	(%rsi), %rbp
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L83:
	movslq	%eax, %rdx
	addq	%rdx, %rbp
	movzwl	98(%rsp), %edx
	subl	%eax, %edx
	testw	%dx, %dx
	movw	%dx, 98(%rsp)
	je	.L82
.L99:
	movl	500(%r13), %edi
	movzwl	%dx, %edx
	movq	%rbp, %rsi
	call	read@PLT
	testl	%eax, %eax
	jg	.L83
	movq	%r13, %r15
.L137:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L133:
	movq	760(%rsp), %rcx
	movl	%eax, (%rcx)
.L130:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	cmpq	$0, 800(%rsp)
	je	.L136
	movq	800(%rsp), %rax
	movl	$0, (%rax)
.L136:
	xorl	%eax, %eax
.L46:
	addq	$696, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	salq	$4, %rax
	leaq	20(%r15,%rax), %rax
	movq	%rax, 56(%rsp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L143:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	je	.L147
	movq	%r13, %r15
.L72:
	movl	44(%rsp), %edx
	movq	760(%rsp), %rcx
	testl	%edx, %edx
	movl	%eax, (%rcx)
	jne	.L130
	cmpl	$104, %eax
	jne	.L130
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	movl	$1, 44(%rsp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L147:
	movzwl	98(%rsp), %edx
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L82:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jne	.L148
.L86:
	testl	%r12d, %r12d
	jne	.L89
	movq	32(%rsp), %rax
	movzwl	(%r14), %ecx
	cmpw	%cx, (%rax)
	je	.L90
.L89:
	movl	(%rsp), %esi
	testl	%esi, %esi
	jne	.L68
	movq	24(%rsp), %rax
	movzwl	(%rax), %eax
	cmpw	(%r14), %ax
	jne	.L68
	testl	%r12d, %r12d
	je	.L149
.L93:
	movl	104(%rsp), %eax
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L145:
	movl	$65536, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movq	64(%rsp), %rsi
	je	.L150
	cmpq	784(%rsp), %rsi
	movl	$65536, (%r15)
	movq	%rax, (%rsi)
	jne	.L77
	movq	808(%rsp), %rax
	movl	$1, (%rax)
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%ebx, %edx
	movw	%bx, 98(%rsp)
	cmpw	$11, %dx
	ja	.L122
.L146:
	movq	760(%rsp), %rax
	movq	%r13, %r15
	movl	$90, (%rax)
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L94:
	cmpq	$0, 776(%rsp)
	je	.L151
	movq	776(%rsp), %rsi
	movq	752(%rsp), %r15
	movq	48(%rsp), %rdx
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L151:
	cmpq	$0, 784(%rsp)
	jne	.L152
	movq	752(%rsp), %rcx
	movq	16(%rsp), %rsi
	movl	%eax, 104(%rsp)
	movl	(%rcx), %edi
	movq	(%rsi), %r14
	movq	%rcx, %r15
	cmpl	%eax, %edi
	jge	.L77
	movq	752(%rsp), %r15
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L90:
	movl	(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L93
	movl	$1, %r12d
	jmp	.L68
.L139:
	movq	80(%rsp), %rbx
	movq	88(%rsp), %rdx
	movl	$28, 108(%rsp)
	movq	%rbx, %rsi
	call	getpeername@PLT
	testl	%eax, %eax
	js	.L54
	movq	56(%rsp), %rsi
	movq	%rbx, %rdi
	call	sock_eq
	testl	%eax, %eax
	jne	.L55
.L54:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	andl	$-2, 504(%r15)
.L55:
	movl	500(%r15), %r8d
	testl	%r8d, %r8d
	js	.L52
	testb	$1, 504(%r15)
	jne	.L57
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L148:
	subw	(%r15), %bx
	orb	$2, 2(%r14)
	movl	$512, %r15d
	testw	%bx, %bx
	movw	%bx, 98(%rsp)
	je	.L86
	movq	80(%rsp), %rbp
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L153:
	movzwl	98(%rsp), %ebx
	subl	%eax, %ebx
	testw	%bx, %bx
	movw	%bx, 98(%rsp)
	je	.L86
.L85:
	cmpw	$512, %bx
	movl	500(%r13), %edi
	movq	%rbp, %rsi
	cmova	%r15d, %ebx
	movzwl	%bx, %edx
	call	read@PLT
	testl	%eax, %eax
	jg	.L153
	jmp	.L86
.L150:
	movq	760(%rsp), %rax
	movq	%r13, %r15
	movl	$12, (%rax)
	jmp	.L130
.L140:
	movq	errno@gottpoff(%rip), %rax
	cmpq	$0, 800(%rsp)
	movq	760(%rsp), %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, (%rcx)
	movl	$-1, %eax
	je	.L46
	movq	800(%rsp), %rcx
	movl	$0, (%rcx)
	jmp	.L46
.L141:
	movl	%fs:(%rbx), %eax
	jmp	.L133
.L138:
	call	get_nsaddr.part.1
.L152:
	leaq	__PRETTY_FUNCTION__.12894(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$846, %edx
	call	__assert_fail@PLT
.L144:
	movq	errno@gottpoff(%rip), %rax
	movq	%r13, %r15
	movl	%fs:(%rax), %eax
	jmp	.L72
.L149:
	movq	32(%rsp), %rcx
	xorl	%esi, %esi
	cmpw	(%rcx), %ax
	setne	%sil
	sete	%r12b
	movl	%esi, (%rsp)
	movzbl	%r12b, %r12d
	jmp	.L68
	.size	send_vc, .-send_vc
	.p2align 4,,15
	.globl	res_ourserver_p
	.type	res_ourserver_p, @function
res_ourserver_p:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movzwl	(%rsi), %edx
	cmpw	$2, %dx
	je	.L180
	xorl	%eax, %eax
	cmpw	$10, %dx
	je	.L181
.L154:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L181:
	movl	16(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.L154
	leal	-1(%r8), %r9d
	leaq	20(%rdi), %rdx
	movl	$1, %ecx
	leaq	8(%rsi), %rbp
	addq	$2, %r9
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L164:
	cmpl	%ecx, %r8d
	jle	.L170
	addq	$1, %rcx
	addq	$16, %rdx
	cmpq	%r9, %rcx
	je	.L162
.L161:
	cmpw	$0, (%rdx)
	movq	%rdx, %rax
	jne	.L163
	movq	528(%rdi,%rcx,8), %rax
	testq	%rax, %rax
	cmove	%rdx, %rax
.L163:
	cmpw	$10, (%rax)
	jne	.L164
	movzwl	2(%rsi), %ebx
	cmpw	%bx, 2(%rax)
	jne	.L164
	movq	8(%rax), %r10
	movq	8+in6addr_any(%rip), %rbx
	movq	16(%rax), %rax
	movq	in6addr_any(%rip), %r11
	xorq	%rax, %rbx
	xorq	%r10, %r11
	orq	%r11, %rbx
	je	.L172
	xorq	8(%rbp), %rax
	xorq	0(%rbp), %r10
	orq	%r10, %rax
	jne	.L164
.L172:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	movzwl	2(%rsi), %r9d
	movl	4(%rsi), %r10d
	movl	16(%rdi), %esi
	testl	%esi, %esi
	jle	.L170
	leaq	20(%rdi), %rdx
	leal	-1(%rsi), %r8d
	xorl	%ecx, %ecx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L159:
	cmpq	%r8, %rcx
	je	.L170
	addq	$1, %rcx
	addq	$16, %rdx
	cmpl	%ecx, %esi
	jbe	.L162
.L157:
	cmpw	$0, (%rdx)
	movq	%rdx, %rax
	jne	.L158
	movq	536(%rdi,%rcx,8), %rax
	testq	%rax, %rax
	cmove	%rdx, %rax
.L158:
	cmpw	$2, (%rax)
	jne	.L159
	cmpw	%r9w, 2(%rax)
	jne	.L159
	movl	4(%rax), %eax
	cmpl	%r10d, %eax
	je	.L172
	testl	%eax, %eax
	jne	.L159
	jmp	.L172
.L162:
	call	get_nsaddr.part.1
	.size	res_ourserver_p, .-res_ourserver_p
	.p2align 4,,15
	.globl	__res_isourserver
	.type	__res_isourserver, @function
__res_isourserver:
	movq	__resp@gottpoff(%rip), %rax
	movq	%rdi, %rsi
	movq	%fs:(%rax), %rdi
	jmp	res_ourserver_p
	.size	__res_isourserver, .-__res_isourserver
	.p2align 4,,15
	.globl	__res_nameinquery
	.type	__res_nameinquery, @function
__res_nameinquery:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1064, %rsp
	movzwl	4(%rcx), %eax
	movq	%rdi, 8(%rsp)
	movl	%edx, 4(%rsp)
	rolw	$8, %ax
	movzwl	%ax, %eax
	testl	%eax, %eax
	je	.L189
	movl	%esi, %r15d
	movq	%rcx, %r14
	movq	%r8, %r12
	leaq	12(%rcx), %rbx
	leal	-1(%rax), %ebp
	leaq	16(%rsp), %r13
	jmp	.L184
	.p2align 4,,10
	.p2align 3
.L201:
	cltq
	leaq	(%rbx,%rax), %rdx
	leaq	4(%rdx), %rbx
	cmpq	%rbx, %r12
	jb	.L192
	movzwl	(%rdx), %eax
	movzwl	2(%rdx), %edx
	rolw	$8, %ax
	rolw	$8, %dx
	movzwl	%ax, %eax
	cmpl	%r15d, %eax
	jne	.L190
	movzwl	%dx, %edx
	cmpl	4(%rsp), %edx
	jne	.L190
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	call	ns_samename@PLT
	cmpl	$1, %eax
	je	.L183
.L190:
	subl	$1, %ebp
	cmpl	$-1, %ebp
	je	.L189
.L184:
	movl	$1026, %r8d
	movq	%r13, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	__dn_expand@PLT
	testl	%eax, %eax
	jns	.L201
.L192:
	movl	$-1, %eax
.L183:
	addq	$1064, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	xorl	%eax, %eax
	jmp	.L183
	.size	__res_nameinquery, .-__res_nameinquery
	.p2align 4,,15
	.globl	__res_queriesmatch
	.type	__res_queriesmatch, @function
__res_queriesmatch:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leaq	12(%rdi), %rbx
	subq	$1064, %rsp
	cmpq	%rsi, %rbx
	ja	.L209
	leaq	12(%rdx), %rax
	cmpq	%rax, %rcx
	jb	.L209
	movzbl	2(%rdi), %eax
	andl	$120, %eax
	cmpb	$40, %al
	je	.L221
.L204:
	movzwl	4(%rdi), %r8d
	xorl	%eax, %eax
	cmpw	4(%rdx), %r8w
	je	.L222
.L202:
	addq	$1064, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	movzbl	2(%rdx), %eax
	andl	$120, %eax
	cmpb	$40, %al
	jne	.L204
.L205:
	movl	$1, %eax
	jmp	.L202
	.p2align 4,,10
	.p2align 3
.L222:
	rolw	$8, %r8w
	movzwl	%r8w, %r8d
	testl	%r8d, %r8d
	leal	-1(%r8), %r13d
	je	.L205
	movq	%rcx, %r14
	movq	%rdx, 8(%rsp)
	movq	%rsi, %rbp
	movq	%rdi, %r12
	leaq	16(%rsp), %r15
	jmp	.L207
	.p2align 4,,10
	.p2align 3
.L223:
	cltq
	leaq	(%rbx,%rax), %rdx
	leaq	4(%rdx), %rbx
	cmpq	%rbx, %rbp
	jb	.L209
	movzwl	(%rdx), %esi
	movzwl	2(%rdx), %edx
	movq	%r14, %r8
	movq	8(%rsp), %rcx
	movq	%r15, %rdi
	rolw	$8, %si
	rolw	$8, %dx
	movzwl	%dx, %edx
	movzwl	%si, %esi
	call	__res_nameinquery
	testl	%eax, %eax
	je	.L202
	subl	$1, %r13d
	cmpl	$-1, %r13d
	je	.L205
.L207:
	movl	$1026, %r8d
	movq	%r15, %rcx
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__dn_expand@PLT
	testl	%eax, %eax
	jns	.L223
.L209:
	movl	$-1, %eax
	jmp	.L202
	.size	__res_queriesmatch, .-__res_queriesmatch
	.p2align 4,,15
	.globl	__res_context_send
	.type	__res_context_send, @function
__res_context_send:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$440, %rsp
	movq	(%rdi), %r15
	movq	%r9, 200(%rsp)
	movl	16(%r15), %ebp
	testl	%ebp, %ebp
	je	.L484
	cmpq	$1, %rcx
	sbbl	%eax, %eax
	andl	$-12, %eax
	addl	$24, %eax
	cmpl	%eax, 496(%rsp)
	jl	.L485
	movq	8(%r15), %r11
	movl	$1, %r14d
	testb	$8, %r11b
	je	.L486
.L229:
	movzwl	512(%r15), %eax
	movl	%r8d, 128(%rsp)
	movq	%rcx, (%rsp)
	movl	%edx, 108(%rsp)
	movq	%rsi, 96(%rsp)
	movq	%rdi, 144(%rsp)
	movl	$110, 208(%rsp)
	testw	%ax, %ax
	je	.L230
	cmpl	%eax, %ebp
	jne	.L231
	leaq	20(%r15), %r8
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L233:
	cmpw	$0, (%r8)
	je	.L232
	movq	536(%r15,%r9,8), %rsi
	movq	%r8, %rdi
	call	sock_eq
	testl	%eax, %eax
	je	.L231
.L232:
	addq	$1, %r9
	addq	$16, %r8
	cmpl	%r9d, %ebp
	ja	.L233
.L234:
	cmpl	$1, %ebp
	movl	$0, 36(%rsp)
	jbe	.L240
	testl	$16384, %r11d
	jne	.L487
.L240:
	movl	4(%r15), %r13d
	testl	%r13d, %r13d
	jle	.L488
	movl	16(%r15), %ebp
	movl	%r14d, 48(%rsp)
	movq	%r15, %r14
	movl	$0, 80(%rsp)
	movl	$0, 172(%rsp)
.L246:
	leaq	208(%rsp), %rax
	testl	%ebp, %ebp
	movq	$0, 64(%rsp)
	movq	%rax, 56(%rsp)
	je	.L343
.L341:
.L248:
	movq	64(%rsp), %rax
	movl	36(%rsp), %r13d
	movl	48(%rsp), %r12d
	addl	%eax, %r13d
	movl	%eax, 164(%rsp)
	movl	%r13d, %eax
	subl	%ebp, %eax
	cmpl	%r13d, %ebp
	cmovbe	%eax, %r13d
	testl	%r12d, %r12d
	jne	.L329
	movl	(%r14), %eax
	movl	%r13d, %ecx
	movl	$0, 212(%rsp)
	sall	%cl, %eax
	testl	%r13d, %r13d
	jle	.L254
	cltd
	idivl	16(%r14)
.L254:
	testl	%eax, %eax
	movl	$1, %edx
	movq	%r14, %r15
	cmovg	%eax, %edx
	movq	8(%r14), %rax
	movl	%r13d, 52(%rsp)
	movl	%edx, 72(%rsp)
	movq	%rax, %rdx
	shrq	$22, %rdx
	movl	%edx, %ecx
	andl	$1, %ecx
	testl	$6291456, %eax
	leaq	224(%rsp), %rax
	movb	%cl, 171(%rsp)
	setne	163(%rsp)
	movq	%rax, 24(%rsp)
.L255:
	movl	52(%rsp), %ebx
	movq	56(%rsp), %rsi
	movq	%r15, %rdi
	movl	%ebx, %edx
	call	reopen
	testl	%eax, %eax
	jle	.L256
	movslq	72(%rsp), %rax
	cmpq	$0, (%rsp)
	leaq	216(%rsp), %rdi
	movq	%r15, 112(%rsp)
	movq	%rdi, 16(%rsp)
	movq	%rax, 8(%rsp)
	sete	35(%rsp)
	movzbl	35(%rsp), %eax
	movl	%eax, 76(%rsp)
	movslq	%ebx, %rax
	leaq	(%r15,%rax,4), %r14
	leaq	272(%rsp), %rax
	movq	%r14, %r13
	movq	%rax, 88(%rsp)
.L257:
	movq	24(%rsp), %rsi
	xorl	%edi, %edi
	call	__clock_gettime@PLT
	movq	8(%rsp), %rax
	movq	232(%rsp), %rbx
	addq	224(%rsp), %rax
	cmpq	$999999999, %rbx
	movq	%rax, 40(%rsp)
	jle	.L259
	addq	$1, %rax
	subq	$1000000000, %rbx
	movq	%rax, 40(%rsp)
.L259:
	movl	520(%r13), %eax
	movq	8(%rsp), %r14
	movl	$4, %ebp
	movw	%bp, 220(%rsp)
	movl	$0, 84(%rsp)
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%r13, 176(%rsp)
	movl	%eax, 216(%rsp)
	movl	80(%rsp), %eax
	movl	%eax, 132(%rsp)
	movl	76(%rsp), %eax
	movl	%eax, 104(%rsp)
	leaq	304(%rsp), %rax
	movq	%rax, 120(%rsp)
	leaq	212(%rsp), %rax
	movq	%rax, 136(%rsp)
.L260:
	testl	%r12d, %r12d
	je	.L269
.L272:
	movabsq	$4835703278458516699, %rax
	movq	16(%rsp), %rdi
	movl	$1, %esi
	imulq	%r15
	movq	%r15, %rax
	sarq	$63, %rax
	imull	$1000, %r14d, %ecx
	sarq	$18, %rdx
	subq	%rax, %rdx
	addl	%ecx, %edx
	call	__poll@PLT
	testl	%eax, %eax
	je	.L270
	testl	%eax, %eax
	movl	$1, %r13d
	movq	errno@gottpoff(%rip), %rax
	jns	.L278
.L489:
	cmpl	$4, %fs:(%rax)
	jne	.L478
.L262:
	movq	24(%rsp), %rsi
	xorl	%edi, %edi
	call	__clock_gettime@PLT
	movq	40(%rsp), %r14
	subq	224(%rsp), %r14
	movq	232(%rsp), %rax
	jne	.L263
	movq	%rbx, %rdx
	subq	%rax, %rdx
	testq	%rdx, %rdx
	jle	.L478
.L265:
	cmpq	%rbx, %rax
	jg	.L268
.L494:
	movq	%rbx, %r15
	subq	%rax, %r15
	testl	%r12d, %r12d
	jne	.L272
	.p2align 4,,10
	.p2align 3
.L269:
	movq	16(%rsp), %rdi
	xorl	%edx, %edx
	movl	$1, %esi
	call	__poll@PLT
	testl	%eax, %eax
	je	.L272
	xorl	%r13d, %r13d
	testl	%eax, %eax
	movq	errno@gottpoff(%rip), %rax
	js	.L489
.L278:
	movl	$0, %fs:(%rax)
	movzwl	222(%rsp), %eax
	testb	$4, %al
	je	.L279
	testl	%r12d, %r12d
	movl	216(%rsp), %r8d
	jne	.L280
	cmpq	$0, (%rsp)
	je	.L280
	cmpb	$0, 163(%rsp)
	movslq	108(%rsp), %rbp
	je	.L490
	movq	96(%rsp), %rsi
	movl	$16384, %ecx
	movq	%rbp, %rdx
	movl	%r8d, %edi
	call	send@PLT
	cmpq	%rbp, %rax
	je	.L351
.L482:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	je	.L262
	cmpl	$11, %eax
	je	.L262
.L478:
	movq	112(%rsp), %r14
.L475:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	__res_iclose@PLT
	cmpq	$0, 528(%rsp)
	je	.L267
.L471:
	movq	528(%rsp), %rax
	movl	$0, (%rax)
.L267:
	cmpq	$0, (%rsp)
	je	.L472
	movq	528(%rsp), %rax
	movl	(%rax), %eax
.L289:
	testl	%eax, %eax
	je	.L491
	xorl	%ecx, %ecx
	cmpq	$0, 528(%rsp)
	movq	%r14, %r15
	movq	8(%r15), %rdx
	movl	48(%rsp), %r14d
	je	.L339
.L349:
	movq	528(%rsp), %rax
	cmpl	$12, (%rax)
	jg	.L492
.L338:
	testl	%ecx, %ecx
	je	.L339
.L348:
	testb	$8, %dl
	je	.L340
.L339:
	andb	$1, %dh
	jne	.L224
.L340:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L231:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	xorl	%eax, %eax
	movw	%ax, 512(%r15)
	movl	16(%r15), %eax
	testl	%eax, %eax
	je	.L235
.L230:
	leaq	20(%r15), %r12
	movl	$1, %ebx
	jmp	.L238
	.p2align 4,,10
	.p2align 3
.L237:
	movdqu	(%r12), %xmm0
	movq	$0, 16(%rax)
	movl	$0, 24(%rax)
	movups	%xmm0, (%rax)
.L236:
	movl	16(%r15), %ebp
	movl	%ebx, %eax
	addq	$16, %r12
	addq	$1, %rbx
	cmpl	%eax, %ebp
	jbe	.L493
.L238:
	movl	$-1, 516(%r15,%rbx,4)
	cmpw	$0, (%r12)
	je	.L236
	movq	528(%r15,%rbx,8), %rax
	testq	%rax, %rax
	jne	.L237
	movl	$28, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 528(%r15,%rbx,8)
	jne	.L237
.L473:
	movl	$-1, %r14d
.L224:
	addq	$440, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L486:
	cmpl	$512, %edx
	setg	%al
	cmpl	$512, %r8d
	setg	%r9b
	orl	%r9d, %eax
	movzbl	%al, %r14d
	jmp	.L229
	.p2align 4,,10
	.p2align 3
.L493:
	movw	%bp, 512(%r15)
	movq	8(%r15), %r11
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L263:
	js	.L478
	cmpq	%rbx, %rax
	jle	.L494
.L268:
	movq	%rbx, %rdi
	subq	$1, %r14
	subq	%rax, %rdi
	leaq	1000000000(%rdi), %r15
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L235:
	movl	$0, 36(%rsp)
	jmp	.L240
	.p2align 4,,10
	.p2align 3
.L487:
	movl	$2, %eax
	lock xaddl	%eax, global_offset.12742(%rip)
	testb	$1, %al
	je	.L495
.L241:
	shrl	%eax
	cmpl	$3, %ebp
	movl	%eax, %ecx
	je	.L243
	andl	$3, %eax
	cmpl	$4, %ebp
	movl	%eax, 36(%rsp)
	je	.L240
	movl	%ecx, %eax
	andl	$1, %eax
	cmpl	$2, %ebp
	movl	%eax, 36(%rsp)
	je	.L240
	movl	%ecx, %eax
	xorl	%edx, %edx
	divl	%ebp
	movl	%edx, 36(%rsp)
	jmp	.L240
.L502:
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	movl	52(%rsp), %r13d
	call	__res_iclose@PLT
	cmpq	$0, 528(%rsp)
	je	.L328
	movq	528(%rsp), %rax
	movl	$1, 80(%rsp)
	movl	$0, (%rax)
	.p2align 4,,10
	.p2align 3
.L329:
	movl	4(%r14), %eax
	movq	%r14, %rdi
	movl	%eax, 172(%rsp)
	pushq	536(%rsp)
	pushq	536(%rsp)
	pushq	536(%rsp)
	pushq	536(%rsp)
	pushq	536(%rsp)
	pushq	%r13
	pushq	104(%rsp)
	leaq	552(%rsp), %rax
	pushq	%rax
	movq	64(%rsp), %rbx
	movl	192(%rsp), %r8d
	movl	172(%rsp), %edx
	movq	160(%rsp), %rsi
	leaq	264(%rsp), %r9
	movq	%rbx, %rcx
	call	send_vc
	addq	$64, %rsp
	testl	%eax, %eax
	js	.L473
	jne	.L357
	testq	%rbx, %rbx
	je	.L359
	movq	528(%rsp), %rax
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L496
.L359:
	movl	$1, 48(%rsp)
.L252:
	movl	164(%rsp), %eax
	movl	16(%r14), %ebp
	addq	$1, 64(%rsp)
	addl	$1, %eax
	cmpl	%eax, %ebp
	ja	.L341
.L343:
	addl	$1, 172(%rsp)
	movl	172(%rsp), %eax
	cmpl	%eax, 4(%r14)
	jg	.L246
	movq	%r14, %r15
	movl	48(%rsp), %r14d
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	testl	%r14d, %r14d
	jne	.L344
	cmpl	$0, 80(%rsp)
	je	.L345
	movq	errno@gottpoff(%rip), %rax
	movl	$110, %fs:(%rax)
	jmp	.L473
	.p2align 4,,10
	.p2align 3
.L495:
	leaq	304(%rsp), %rsi
	movl	$1, %edi
	call	__clock_gettime@PLT
	movq	304(%rsp), %rax
	xorl	312(%rsp), %eax
	movl	%eax, %edx
	rorl	$8, %edx
	xorl	%edx, %eax
	addl	%eax, %eax
	movl	%eax, %edx
	orl	$1, %edx
	addl	$2, %edx
	movl	%edx, global_offset.12742(%rip)
	jmp	.L241
.L279:
	testb	$1, %al
	je	.L297
	movl	84(%rsp), %eax
	orl	104(%rsp), %eax
	testb	$1, %al
	je	.L373
	cmpb	$0, 35(%rsp)
	jne	.L373
	movq	520(%rsp), %rax
	movslq	(%rax), %rdx
	cmpl	$65535, %edx
	jg	.L497
	cmpq	$0, 512(%rsp)
	jne	.L360
	movq	528(%rsp), %rax
	movq	0, %rbp
	movq	$0, 192(%rsp)
	movq	%rax, 152(%rsp)
	movq	520(%rsp), %rax
	movq	%rax, 184(%rsp)
.L302:
	movq	120(%rsp), %r9
	movq	88(%rsp), %r8
	xorl	%ecx, %ecx
	movl	216(%rsp), %edi
	movq	%rbp, %rsi
	movl	$28, 304(%rsp)
	call	recvfrom@PLT
	movq	152(%rsp), %rsi
	testl	%eax, %eax
	movl	%eax, (%rsi)
	jle	.L482
	cmpl	$11, %eax
	jle	.L498
	movq	88(%rsp), %rsi
	movq	112(%rsp), %rdi
	call	res_ourserver_p
	testl	%eax, %eax
	je	.L312
	movl	84(%rsp), %edi
	testl	%edi, %edi
	jne	.L313
	movq	96(%rsp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, 0(%rbp)
	je	.L499
.L313:
	movl	104(%rsp), %eax
	testl	%eax, %eax
	jne	.L316
	movq	(%rsp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, 0(%rbp)
	je	.L500
.L316:
	movl	$1, 132(%rsp)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L373:
	cmpq	$0, 504(%rsp)
	je	.L501
	movslq	496(%rsp), %rdx
	movq	504(%rsp), %rax
	cmpl	$65535, %edx
	jle	.L369
	movq	(%rax), %rbp
	movq	%rax, 192(%rsp)
	movq	136(%rsp), %rax
	movq	%rax, 152(%rsp)
	leaq	496(%rsp), %rax
	movq	%rax, 184(%rsp)
	jmp	.L302
.L499:
	movq	192(%rsp), %rax
	movslq	108(%rsp), %rsi
	movq	96(%rsp), %rdi
	movq	(%rax), %rdx
	movq	184(%rsp), %rax
	addq	%rdi, %rsi
	movslq	(%rax), %rcx
	addq	%rdx, %rcx
	call	__res_queriesmatch
	movl	104(%rsp), %esi
	xorl	%r8d, %r8d
	testl	%eax, %eax
	setne	%r8b
	testl	%esi, %esi
	jne	.L314
	movq	(%rsp), %rax
	movzwl	(%rax), %eax
	cmpw	%ax, 0(%rbp)
	je	.L346
.L314:
	testl	%r8d, %r8d
	je	.L316
.L315:
	movzbl	3(%rbp), %eax
	andl	$15, %eax
	leal	-4(%rax), %edx
	cmpb	$1, %dl
	jbe	.L319
	cmpb	$2, %al
	je	.L319
.L317:
	testb	%al, %al
	jne	.L326
	cmpw	$0, 6(%rbp)
	jne	.L326
	testw	$-32764, 2(%rbp)
	jne	.L326
	cmpw	$0, 10(%rbp)
	je	.L319
.L326:
	movq	112(%rsp), %rax
	testb	$32, 8(%rax)
	jne	.L327
	testb	$2, 2(%rbp)
	jne	.L502
.L327:
	movl	84(%rsp), %ecx
	cmpl	$1, %r8d
	movl	$1, %eax
	cmove	%eax, %ecx
	cmove	104(%rsp), %eax
	movl	%ecx, 84(%rsp)
	testl	%eax, %ecx
	movl	%eax, 104(%rsp)
	jne	.L331
	cmpb	$0, 163(%rsp)
	je	.L312
	cmpb	$0, 171(%rsp)
	movl	$4, %edx
	movw	%dx, 220(%rsp)
	je	.L312
	movq	112(%rsp), %rbp
	xorl	%esi, %esi
	movq	%rbp, %rdi
	call	__res_iclose@PLT
	movl	52(%rsp), %edx
	movq	56(%rsp), %rsi
	movq	%rbp, %rdi
	call	reopen
	testl	%eax, %eax
	jle	.L503
	movq	176(%rsp), %rax
	movl	520(%rax), %eax
	movl	%eax, 216(%rsp)
	.p2align 4,,10
	.p2align 3
.L312:
	movl	$1, 132(%rsp)
.L284:
	testl	%r13d, %r13d
	jne	.L262
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L501:
	cmpq	$0, 512(%rsp)
	jne	.L301
	movq	136(%rsp), %rax
	movq	200(%rsp), %rbp
	movslq	496(%rsp), %rdx
	movq	%rax, 152(%rsp)
	leaq	200(%rsp), %rax
	movq	%rax, 192(%rsp)
	leaq	496(%rsp), %rax
	movq	%rax, 184(%rsp)
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L490:
	movq	96(%rsp), %rax
	leaq	304(%rsp), %rdi
	movq	%rbp, 248(%rsp)
	movl	$16, %ecx
	movq	120(%rsp), %rsi
	movl	$2, %edx
	movq	%rax, 240(%rsp)
	movq	(%rsp), %rax
	movq	%rax, 256(%rsp)
	movslq	128(%rsp), %rax
	movq	%rax, 264(%rsp)
	movq	%rax, %rbp
	xorl	%eax, %eax
	rep stosq
	leaq	240(%rsp), %rax
	movl	$16384, %ecx
	movl	%r8d, %edi
	movq	$1, 328(%rsp)
	movq	$1, 392(%rsp)
	movq	%rax, 320(%rsp)
	leaq	256(%rsp), %rax
	movq	%rax, 384(%rsp)
	call	__sendmmsg@PLT
	cmpl	$2, %eax
	jne	.L282
	movl	108(%rsp), %eax
	cmpl	%eax, 360(%rsp)
	jne	.L283
	cmpl	%ebp, 424(%rsp)
	jne	.L283
	movl	$1, %r10d
	movl	$2, %r12d
	movw	%r10w, 220(%rsp)
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L369:
	movq	%rax, 192(%rsp)
	movq	136(%rsp), %rax
	movq	%rax, 152(%rsp)
	leaq	496(%rsp), %rax
	movq	%rax, 184(%rsp)
.L304:
	movq	152(%rsp), %rbp
	movl	216(%rsp), %edi
	xorl	%eax, %eax
	movl	$21531, %esi
	movq	%rbp, %rdx
	call	ioctl@PLT
	testl	%eax, %eax
	js	.L305
	movq	184(%rsp), %rax
	movslq	(%rax), %rdx
	cmpl	0(%rbp), %edx
	jl	.L305
	movq	192(%rsp), %rax
	movq	(%rax), %rbp
	jmp	.L302
.L280:
	testl	%r12d, %r12d
	jne	.L291
	movslq	108(%rsp), %rbp
	movq	96(%rsp), %rsi
	movl	$16384, %ecx
	movl	%r8d, %edi
	movq	%rbp, %rdx
	call	send@PLT
	cmpq	%rbp, %rax
	jne	.L482
.L293:
	cmpb	$0, 35(%rsp)
	jne	.L296
.L351:
	cmpb	$0, 163(%rsp)
	jne	.L296
.L290:
	movl	$5, %r8d
	movw	%r8w, 220(%rsp)
.L294:
	addl	$1, %r12d
	jmp	.L284
.L291:
	movslq	128(%rsp), %rbp
	movq	(%rsp), %rsi
	movl	$16384, %ecx
	movl	%r8d, %edi
	movq	%rbp, %rdx
	call	send@PLT
	cmpq	%rbp, %rax
	jne	.L482
.L296:
	movl	$1, %r9d
	movw	%r9w, 220(%rsp)
	jmp	.L294
.L497:
	movq	512(%rsp), %rax
	movq	(%rax), %rbp
	movq	528(%rsp), %rax
	movq	%rax, 152(%rsp)
	movq	512(%rsp), %rax
	movq	%rax, 192(%rsp)
	movq	520(%rsp), %rax
	movq	%rax, 184(%rsp)
	jmp	.L302
.L243:
	movl	$-1431655765, %edx
	mull	%edx
	shrl	%edx
	leal	(%rdx,%rdx,2), %eax
	subl	%eax, %ecx
	movl	%ecx, 36(%rsp)
	jmp	.L240
.L305:
	movl	$65536, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L504
	movq	184(%rsp), %rax
	movl	$65536, %edx
	movl	$65536, (%rax)
	movq	192(%rsp), %rax
	cmpq	%rax, 512(%rsp)
	movq	%rbp, (%rax)
	jne	.L302
	movq	536(%rsp), %rax
	movl	$1, (%rax)
	movq	184(%rsp), %rax
	movslq	(%rax), %rdx
	jmp	.L302
.L498:
	movq	112(%rsp), %r14
	xorl	%esi, %esi
	movl	$90, 208(%rsp)
	movq	%r14, %rdi
	call	__res_iclose@PLT
	cmpq	$0, 528(%rsp)
	movl	$1, 132(%rsp)
	jne	.L471
	jmp	.L267
	.p2align 4,,10
	.p2align 3
.L270:
	movl	212(%rsp), %eax
	movq	176(%rsp), %r13
	cmpl	$1, %eax
	jle	.L273
	movl	84(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L274
	cmpq	$0, (%rsp)
	je	.L273
	testb	$1, 104(%rsp)
	je	.L273
.L274:
	cmpb	$0, 163(%rsp)
	je	.L505
	cmpb	$0, 171(%rsp)
	movq	112(%rsp), %r15
	je	.L506
	movq	528(%rsp), %rsi
	xorl	%r14d, %r14d
	movl	$1, (%rsi)
.L251:
	cmpl	$12, %eax
	jle	.L336
	movq	144(%rsp), %rdi
	movq	(%rdi), %rdx
	movq	8(%rdx), %rdx
	andl	$67108864, %edx
	cmpq	$0, 504(%rsp)
	je	.L337
	testq	%rdx, %rdx
	jne	.L336
	movq	504(%rsp), %rsi
	movq	(%rsi), %rdx
	andb	$-33, 3(%rdx)
.L336:
	cmpq	$0, 528(%rsp)
	movl	%r14d, %ecx
	movq	8(%r15), %rdx
	movl	%eax, %r14d
	jne	.L349
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L484:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r14d
	movl	$3, %fs:(%rax)
	jmp	.L224
.L485:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r14d
	movl	$22, %fs:(%rax)
	jmp	.L224
.L488:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	testl	%r14d, %r14d
	jne	.L344
.L345:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r14d
	movl	$111, %fs:(%rax)
	jmp	.L224
	.p2align 4,,10
	.p2align 3
.L319:
	movl	84(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L320
	cmpq	$0, (%rsp)
	je	.L321
	testb	$1, 104(%rsp)
	jne	.L320
.L321:
	cmpq	$0, (%rsp)
	je	.L323
	xorl	%eax, %eax
	cmpl	$1, %r8d
	movl	$0, 212(%rsp)
	sete	%al
	movl	%eax, 84(%rsp)
	movl	$1, %eax
	cmove	104(%rsp), %eax
	movl	%eax, 104(%rsp)
	jmp	.L312
.L500:
	xorl	%r8d, %r8d
.L346:
	movq	192(%rsp), %rax
	movslq	128(%rsp), %rsi
	movq	(%rsp), %rdi
	movl	%r8d, 132(%rsp)
	movq	(%rax), %rdx
	movq	184(%rsp), %rax
	addq	%rdi, %rsi
	movslq	(%rax), %rcx
	addq	%rdx, %rcx
	call	__res_queriesmatch
	testl	%eax, %eax
	movl	132(%rsp), %r8d
	je	.L314
	movl	$2, %r8d
	jmp	.L315
.L323:
	movq	112(%rsp), %rax
	cmpq	$0, 384(%rax)
	je	.L507
	movq	112(%rsp), %rdi
	xorl	%esi, %esi
	movl	%r8d, 132(%rsp)
	call	__res_iclose@PLT
	movzbl	3(%rbp), %eax
	movl	132(%rsp), %r8d
	andl	$15, %eax
	jmp	.L317
.L256:
	cmpq	$0, 528(%rsp)
	movq	%r15, %r14
	je	.L258
	movq	528(%rsp), %rsi
	movl	$0, (%rsi)
.L258:
	testl	%eax, %eax
	js	.L473
	je	.L508
	movq	%r14, %r15
	movl	48(%rsp), %r14d
	jmp	.L251
.L504:
	movq	192(%rsp), %rax
	movq	(%rax), %rbp
	movq	184(%rsp), %rax
	movslq	(%rax), %rdx
	jmp	.L302
.L297:
	testb	$56, %al
	movq	112(%rsp), %r14
	jne	.L475
	call	abort@PLT
	.p2align 4,,10
	.p2align 3
.L282:
	cmpl	$1, %eax
	je	.L509
	testl	%eax, %eax
	jns	.L283
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	je	.L287
	cmpl	$11, %eax
	jne	.L283
.L287:
	xorl	%r12d, %r12d
	jmp	.L262
.L505:
	movq	112(%rsp), %rax
	movb	$1, 163(%rsp)
	orq	$2097152, 8(%rax)
	jmp	.L257
.L506:
	orq	$4194304, 8(%r15)
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__res_iclose@PLT
	movzbl	163(%rsp), %eax
	movb	%al, 171(%rsp)
	jmp	.L255
.L509:
	movl	108(%rsp), %eax
	cmpl	%eax, 360(%rsp)
	je	.L290
.L283:
	movq	112(%rsp), %r14
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	__res_iclose@PLT
	cmpq	$0, 528(%rsp)
	je	.L510
	movq	528(%rsp), %rax
	movl	$0, (%rax)
	movl	132(%rsp), %eax
	movl	%eax, 80(%rsp)
	jmp	.L252
.L320:
	movq	528(%rsp), %rax
	movq	112(%rsp), %r14
	movl	$0, (%rax)
	movl	212(%rsp), %eax
.L322:
	movl	$1, 80(%rsp)
	jmp	.L258
.L492:
	movq	144(%rsp), %rax
	movq	(%rax), %rax
	testb	$4, 11(%rax)
	jne	.L338
.L350:
	movq	512(%rsp), %rax
	movq	(%rax), %rax
	andb	$-33, 3(%rax)
	jmp	.L338
.L357:
	movq	%r14, %r15
	movl	$1, %r14d
	jmp	.L251
.L491:
	movl	$0, 48(%rsp)
.L472:
	movl	132(%rsp), %eax
	movl	%eax, 80(%rsp)
	jmp	.L252
.L496:
	movq	%r14, %r15
	movq	8(%r14), %rdx
	xorl	%r14d, %r14d
	cmpl	$12, %eax
	jle	.L348
	movq	144(%rsp), %rax
	xorl	%r14d, %r14d
	movl	$1, %ecx
	movq	(%rax), %rax
	testb	$4, 11(%rax)
	je	.L350
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L331:
	movq	112(%rsp), %r14
	movl	212(%rsp), %eax
	jmp	.L322
.L337:
	testq	%rdx, %rdx
	jne	.L336
	movq	200(%rsp), %rdx
	andb	$-33, 3(%rdx)
	jmp	.L336
.L510:
	movl	0, %eax
	jmp	.L289
.L344:
	movq	errno@gottpoff(%rip), %rax
	movl	208(%rsp), %edx
	orl	$-1, %r14d
	movl	%edx, %fs:(%rax)
	jmp	.L224
.L507:
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %r14
	call	__res_iclose@PLT
	cmpq	$0, 528(%rsp)
	je	.L325
	movq	528(%rsp), %rax
	movl	$0, 48(%rsp)
	movl	$1, 80(%rsp)
	movl	$0, (%rax)
	jmp	.L252
.L273:
	cmpq	$0, 528(%rsp)
	movq	112(%rsp), %r14
	je	.L277
	movq	528(%rsp), %rax
	movl	$1, 132(%rsp)
	movl	$0, (%rax)
	jmp	.L267
.L360:
	movq	528(%rsp), %rax
	movq	%rax, 152(%rsp)
	movq	512(%rsp), %rax
	movq	%rax, 192(%rsp)
	movq	520(%rsp), %rax
	movq	%rax, 184(%rsp)
	jmp	.L304
.L508:
	movl	80(%rsp), %eax
	movl	%eax, 132(%rsp)
	jmp	.L267
.L277:
	movl	$1, 132(%rsp)
	jmp	.L267
.L503:
	cmpq	$0, 528(%rsp)
	movq	112(%rsp), %r14
	je	.L322
	movq	528(%rsp), %rdi
	movl	$0, (%rdi)
	jmp	.L322
.L328:
	movl	$1, 80(%rsp)
	jmp	.L329
.L301:
	leaq	__PRETTY_FUNCTION__.12963(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$1285, %edx
	call	__assert_fail@PLT
.L325:
	movl	$0, 48(%rsp)
	movl	$1, 80(%rsp)
	jmp	.L252
	.size	__res_context_send, .-__res_context_send
	.p2align 4,,15
	.type	context_send_common, @function
context_send_common:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L515
	pushq	$0
	pushq	$0
	movq	%rcx, %r9
	pushq	$0
	pushq	$0
	movq	%rdi, %rbx
	pushq	$0
	pushq	%r8
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	call	__res_context_send
	addq	$48, %rsp
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	__resolv_context_put@PLT
.L511:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L515:
	movq	__resp@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movq	%fs:(%rax), %rax
	movl	$-1, 496(%rax)
	movq	__h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L511
	.size	context_send_common, .-context_send_common
	.p2align 4,,15
	.globl	__res_nsend
	.type	__res_nsend, @function
__res_nsend:
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r12
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	%r8d, 12(%rsp)
	call	__resolv_context_get_override@PLT
	movl	12(%rsp), %r8d
	addq	$16, %rsp
	movq	%r12, %rcx
	movl	%ebp, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	context_send_common
	.size	__res_nsend, .-__res_nsend
	.p2align 4,,15
	.globl	__res_send
	.type	__res_send, @function
__res_send:
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	%rdx, %r12
	subq	$8, %rsp
	call	__resolv_context_get@PLT
	addq	$8, %rsp
	movl	%r13d, %r8d
	movq	%r12, %rcx
	movl	%ebp, %edx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	context_send_common
	.size	__res_send, .-__res_send
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12963, @object
	.size	__PRETTY_FUNCTION__.12963, 8
__PRETTY_FUNCTION__.12963:
	.string	"send_dg"
	.align 8
	.type	__PRETTY_FUNCTION__.12894, @object
	.size	__PRETTY_FUNCTION__.12894, 8
__PRETTY_FUNCTION__.12894:
	.string	"send_vc"
	.local	global_offset.12742
	.comm	global_offset.12742,4,4
	.align 8
	.type	__PRETTY_FUNCTION__.12837, @object
	.size	__PRETTY_FUNCTION__.12837, 11
__PRETTY_FUNCTION__.12837:
	.string	"get_nsaddr"
