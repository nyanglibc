	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%u"
	.text
	.p2align 4,,15
	.globl	inet_neta
	.type	inet_neta, @function
inet_neta:
	testl	%edi, %edi
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	je	.L8
	leaq	.LC0(%rip), %rbp
	movl	%edi, %ebx
	movq	%rsi, %r13
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%ebx, %edx
	sall	$8, %ebx
	shrl	$24, %edx
	testl	%edx, %edx
	je	.L3
	cmpq	$4, %r14
	jbe	.L4
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	sprintf@PLT
	cltq
	addq	%r13, %rax
	testl	%ebx, %ebx
	je	.L5
	movl	$46, %edx
	addq	$1, %rax
	movw	%dx, -1(%rax)
.L5:
	movq	%rax, %rdx
	subq	%r13, %rdx
	movq	%rax, %r13
	subq	%rdx, %r14
.L3:
	testl	%ebx, %ebx
	jne	.L6
	cmpq	%r13, %r12
	je	.L2
	movq	%r12, %r13
	popq	%rbx
	movq	%r13, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L8:
	movq	%rsi, %r13
.L2:
	cmpq	$7, %r14
	jbe	.L4
	movabsq	$13561583350328880, %rax
	movq	%rax, 0(%r13)
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r13d, %r13d
	movl	$90, %fs:(%rax)
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	inet_neta, .-inet_neta
