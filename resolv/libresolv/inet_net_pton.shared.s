	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"inet_net_pton.c"
.LC1:
	.string	"n >= 0 && n <= 15"
.LC2:
	.string	"n >= 0 && n <= 9"
	.text
	.p2align 4,,15
	.globl	inet_net_pton
	.type	inet_net_pton, @function
inet_net_pton:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	cmpl	$2, %edi
	movq	%rdx, 16(%rsp)
	movq	%rcx, 8(%rsp)
	jne	.L68
	movq	%rsi, %rbp
	leaq	1(%rsi), %r15
	movsbq	(%rsi), %rsi
	cmpl	$48, %esi
	movq	%rsi, %rbx
	movl	%esi, 24(%rsp)
	je	.L71
	testb	%sil, %sil
	js	.L12
	call	__ctype_b_loc@PLT
	movl	24(%rsp), %esi
	movq	(%rax), %rcx
	movq	%rax, %r14
.L5:
	testb	$8, 1(%rcx,%rbx,2)
	je	.L12
	subq	$1, 8(%rsp)
	leaq	xdigits.5560(%rip), %rbx
	movq	16(%rsp), %r13
	movq	%rbx, %rbp
.L16:
	movq	%r15, %r12
	xorl	%r15d, %r15d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L73:
	leaq	1(%r12), %r8
	movsbq	-1(%r8), %rsi
	testb	%sil, %sil
	js	.L14
	movq	(%r14), %rdi
	testb	$8, 1(%rdi,%rsi,2)
	je	.L14
	movq	%r8, %r12
.L15:
	movq	%rbp, %rdi
	call	__rawmemchr@PLT
	subq	%rbx, %rax
	cmpl	$9, %eax
	ja	.L72
	leal	(%r15,%r15,4), %esi
	leal	(%rax,%rsi,2), %r15d
	cmpl	$255, %r15d
	jle	.L73
.L12:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$2, %fs:(%rax)
.L1:
	addq	$56, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	8(%rsp), %rdx
	cmpq	$-1, %rdx
	je	.L6
	addq	$1, %r13
	testl	%esi, %esi
	movb	%r15b, -1(%r13)
	je	.L11
	cmpl	$47, %esi
	je	.L11
	cmpl	$46, %esi
	jne	.L12
	movsbq	1(%r12), %rsi
	leaq	2(%r12), %r15
	testb	%sil, %sil
	js	.L12
	movq	(%r14), %rcx
	subq	$1, %rdx
	movq	%rdx, 8(%rsp)
	testb	$8, 1(%rcx,%rsi,2)
	jne	.L16
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L71:
	call	__ctype_b_loc@PLT
	movq	%rax, %r14
	movq	(%rax), %rcx
	movzbl	1(%rbp), %eax
	movl	24(%rsp), %esi
	andl	$-33, %eax
	cmpb	$88, %al
	jne	.L5
	movzbl	2(%rbp), %eax
	testb	%al, %al
	js	.L5
	movsbq	%al, %rdi
	movzwl	(%rcx,%rdi,2), %r9d
	testw	$4096, %r9w
	je	.L5
	cmpq	$0, 8(%rsp)
	je	.L6
	leaq	2(%rbp), %r8
	movq	16(%rsp), %r13
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	leaq	xdigits.5560(%rip), %rbx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L76:
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L6
	sall	$4, %r15d
	subq	$1, %rcx
	addq	$1, %r13
	movl	%r15d, %esi
	movq	%rcx, 8(%rsp)
	xorl	%ebp, %ebp
	orl	%eax, %esi
	movb	%sil, -1(%r13)
	movq	(%r14), %rcx
.L9:
	movsbq	(%r8), %rdi
	movl	%esi, %r15d
	movzwl	(%rcx,%rdi,2), %r9d
	movq	%rdi, %rax
.L7:
	addq	$1, %r8
	testw	$4096, %r9w
	movsbl	%al, %esi
	je	.L74
	movq	%rcx, 32(%rsp)
	movq	%r8, 24(%rsp)
	movq	%rdi, 40(%rsp)
	call	__ctype_tolower_loc@PLT
	movq	40(%rsp), %rdi
	movq	(%rax), %rax
	movl	(%rax,%rdi,4), %esi
	movq	%rbx, %rdi
	call	__rawmemchr@PLT
	subq	%rbx, %rax
	movq	24(%rsp), %r8
	movq	32(%rsp), %rcx
	cmpl	$15, %eax
	movl	%eax, %esi
	ja	.L75
	testl	%ebp, %ebp
	jne	.L76
	movl	$1, %ebp
	jmp	.L9
.L74:
	testl	%ebp, %ebp
	je	.L11
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	leaq	-1(%rcx), %rax
	je	.L6
	sall	$4, %r15d
	movq	%rax, 8(%rsp)
	addq	$1, %r13
	movb	%r15b, -1(%r13)
.L11:
	cmpl	$47, %esi
	je	.L17
	testl	%esi, %esi
	jne	.L12
	cmpq	%r13, 16(%rsp)
	je	.L12
.L18:
	movq	16(%rsp), %rax
	movq	%r13, %rcx
	movzbl	(%rax), %ebx
	subq	%rax, %rcx
	leaq	0(,%rcx,8), %rax
	cmpb	$-17, %bl
	ja	.L30
	cmpb	$-33, %bl
	ja	.L31
	cmpb	$-65, %bl
	ja	.L32
	movq	%rbx, %rdx
	shrl	$4, %ebx
	shrq	$4, %rdx
	andl	$8, %ebx
	andl	$8, %edx
	addl	$8, %ebx
	addq	$8, %rdx
.L23:
	cmpq	%rax, %rdx
	jge	.L24
	leal	0(,%rcx,8), %ebx
	movslq	%ebx, %rdx
.L24:
	cmpq	%rax, %rdx
	jle	.L1
	movq	8(%rsp), %rax
	testq	%rax, %rax
	je	.L6
	movq	%rax, %r12
	addq	%r13, %r12
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$1, %r13
	movb	$0, -1(%r13)
	movq	%r13, %rax
	subq	16(%rsp), %rax
	salq	$3, %rax
	cmpq	%rdx, %rax
	jge	.L1
	cmpq	%r12, %r13
	jne	.L27
.L6:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$90, %fs:(%rax)
	jmp	.L1
.L17:
	movzbl	(%r8), %ebx
	movq	%r8, 24(%rsp)
	testb	%bl, %bl
	js	.L12
	call	__ctype_b_loc@PLT
	movq	(%rax), %r15
	movsbq	%bl, %rax
	testb	$8, 1(%r15,%rax,2)
	je	.L12
	cmpq	%r13, 16(%rsp)
	jnb	.L12
	movq	24(%rsp), %r8
	leaq	xdigits.5560(%rip), %rbp
	movsbl	%bl, %esi
	xorl	%ebx, %ebx
	movq	%rbp, %r12
	addq	$1, %r8
	jmp	.L21
.L78:
	testb	$8, 1(%r15,%rsi,2)
	je	.L20
.L21:
	movq	%r12, %rdi
	movq	%r8, 24(%rsp)
	call	__rawmemchr@PLT
	subq	%rbp, %rax
	movq	24(%rsp), %r8
	cmpl	$9, %eax
	ja	.L77
	addq	$1, %r8
	movsbq	-1(%r8), %rsi
	leal	(%rbx,%rbx,4), %ecx
	leal	(%rax,%rcx,2), %ebx
	testb	%sil, %sil
	jns	.L78
.L20:
	testl	%esi, %esi
	jne	.L12
	cmpl	$32, %ebx
	jg	.L6
	movq	16(%rsp), %rcx
	cmpq	%r13, %rcx
	je	.L12
	movq	%r13, %rax
	movslq	%ebx, %rdx
	subq	%rcx, %rax
	salq	$3, %rax
	cmpl	$-1, %ebx
	jne	.L24
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L68:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$97, %fs:(%rax)
	jmp	.L1
.L30:
	movl	$32, %edx
	movl	$32, %ebx
	jmp	.L23
.L72:
	leaq	__PRETTY_FUNCTION__.5568(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$123, %edx
	call	__assert_fail@PLT
.L31:
	movl	$4, %edx
	movl	$4, %ebx
	jmp	.L24
.L32:
	movl	$24, %edx
	movl	$24, %ebx
	jmp	.L23
.L77:
	leaq	__PRETTY_FUNCTION__.5568(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$150, %edx
	call	__assert_fail@PLT
.L75:
	leaq	__PRETTY_FUNCTION__.5568(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$99, %edx
	call	__assert_fail@PLT
	.size	inet_net_pton, .-inet_net_pton
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.5568, @object
	.size	__PRETTY_FUNCTION__.5568, 19
__PRETTY_FUNCTION__.5568:
	.string	"inet_net_pton_ipv4"
	.align 16
	.type	xdigits.5560, @object
	.size	xdigits.5560, 17
xdigits.5560:
	.string	"0123456789abcdef"
