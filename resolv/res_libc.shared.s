	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver _res,_res@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__res_init
	.type	__res_init, @function
__res_init:
	pushq	%rbx
	movq	__libc_resp@gottpoff(%rip), %rbx
	movq	%fs:(%rbx), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L2
	movl	$5, (%rdi)
.L2:
	movl	4(%rdi), %edx
	testl	%edx, %edx
	jne	.L3
	movl	$2, 4(%rdi)
.L3:
	testb	$1, 8(%rdi)
	jne	.L4
	movq	$704, 8(%rdi)
.L5:
	cmpw	$0, 68(%rdi)
	jne	.L6
	call	__GI___res_randomid
	movq	%fs:(%rbx), %rdi
	movw	%ax, 68(%rdi)
.L6:
	popq	%rbx
	movl	$1, %esi
	jmp	__res_vinit
	.p2align 4,,10
	.p2align 3
.L4:
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jle	.L5
	movl	$1, %esi
	call	__GI___res_iclose
	movq	%fs:(%rbx), %rdi
	jmp	.L5
	.size	__res_init, .-__res_init
	.globl	__resp
	.section	.tdata,"awT",@progbits
	.align 8
	.type	__resp, @object
	.size	__resp, 8
__resp:
	.quad	_res
	.globl	__libc_resp
	.hidden	__libc_resp
	.set	__libc_resp,__resp
	.globl	_res
	.bss
	.align 32
	.type	_res, @object
	.size	_res, 568
_res:
	.zero	568
	.hidden	__res_vinit
