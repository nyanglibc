	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Resolver internal error"
.LC1:
	.string	"Unknown resolver error"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_hstrerror
	.hidden	__GI_hstrerror
	.type	__GI_hstrerror, @function
__GI_hstrerror:
	testl	%edi, %edi
	js	.L5
	cmpl	$4, %edi
	jle	.L6
	leaq	.LC1(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	jmp	__GI___dcgettext
	.p2align 4,,10
	.p2align 3
.L6:
	movq	h_errlist@GOTPCREL(%rip), %rax
	movslq	%edi, %rdi
	movl	$5, %edx
	movq	(%rax,%rdi,8), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	jmp	__GI___dcgettext
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	jmp	__GI___dcgettext
	.size	__GI_hstrerror, .-__GI_hstrerror
	.globl	hstrerror
	.set	hstrerror,__GI_hstrerror
	.section	.rodata.str1.1
.LC2:
	.string	": "
.LC3:
	.string	"\n"
	.text
	.p2align 4,,15
	.globl	herror
	.type	herror, @function
herror:
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	testq	%rdi, %rdi
	je	.L10
	cmpb	$0, (%rdi)
	je	.L10
	movq	%rdi, (%rsp)
	movq	%rsp, %rbp
	call	__GI_strlen
	movq	%rax, 8(%rsp)
	leaq	.LC2(%rip), %rax
	movq	$2, 24(%rsp)
	leaq	32(%rbp), %rbx
	movq	%rax, 16(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsp, %rbp
	movq	%rbp, %rbx
.L8:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edi
	call	__GI_hstrerror
	movq	%rax, (%rbx)
	movq	%rax, %rdi
	call	__GI_strlen
	leaq	16(%rbx), %rdx
	movq	%rax, 8(%rbx)
	leaq	.LC3(%rip), %rax
	movq	$1, 24(%rbx)
	movq	%rbp, %rsi
	movl	$2, %edi
	subq	%rbp, %rdx
	movq	%rax, 16(%rbx)
	movl	$20, %eax
	sarq	$4, %rdx
	addl	$1, %edx
#APP
# 68 "../sysdeps/unix/sysv/linux/not-cancel.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	herror, .-herror
	.globl	h_nerr
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
	.type	h_nerr, @object
	.size	h_nerr, 4
h_nerr:
	.long	5
	.globl	h_errlist
	.section	.rodata.str1.1
.LC4:
	.string	"Resolver Error 0 (no error)"
.LC5:
	.string	"Unknown host"
.LC6:
	.string	"Host name lookup failure"
.LC7:
	.string	"Unknown server error"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC8:
	.string	"No address associated with name"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	h_errlist, @object
	.size	h_errlist, 40
h_errlist:
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
