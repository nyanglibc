	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	put_locked_global.isra.2, @function
put_locked_global.isra.2:
#APP
# 107 "resolv_conf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	subl	$1, lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
#APP
# 107 "resolv_conf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L5
	rep ret
.L5:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 107 "resolv_conf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	ret
	.size	put_locked_global.isra.2, .-put_locked_global.isra.2
	.p2align 4,,15
	.type	get_locked_global, @function
get_locked_global:
	subq	$8, %rsp
#APP
# 88 "resolv_conf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L8:
	movq	global(%rip), %rax
	testq	%rax, %rax
	je	.L15
.L6:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$88, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	je	.L6
	movq	%rax, global(%rip)
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movq	$0, 16(%rax)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L8
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L8
	.size	get_locked_global, .-get_locked_global
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"resolv_conf.c"
.LC1:
	.string	"conf->__refcount > 0"
	.text
	.p2align 4,,15
	.type	conf_decrement, @function
conf_decrement:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L24
	subq	$1, %rax
	je	.L18
	movq	%rax, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	jmp	free@PLT
.L24:
	leaq	__PRETTY_FUNCTION__.9515(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$115, %edx
	call	__GI___assert_fail
	.size	conf_decrement, .-conf_decrement
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	freeres, @function
freeres:
	pushq	%rbx
	movq	global(%rip), %rbx
	testq	%rbx, %rbx
	je	.L25
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L27
	call	conf_decrement
	movq	global(%rip), %rbx
	movq	$0, 32(%rbx)
.L27:
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	global(%rip), %rdi
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 16(%rbx)
	call	free@PLT
	movq	$0, global(%rip)
.L25:
	popq	%rbx
	ret
	.size	freeres, .-freeres
	.text
	.p2align 4,,15
	.type	decrement_at_index.part.8, @function
decrement_at_index.part.8:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	16(%rdi), %rax
	leaq	(%rax,%rsi,8), %r12
	movq	(%r12), %rax
	testb	$1, %al
	jne	.L35
	movq	%rdi, %rbp
	movq	%rax, %rdi
	movq	%rsi, %rbx
	call	conf_decrement
	movq	24(%rbp), %rax
	leaq	1(%rbx,%rbx), %rsi
	movq	%rax, (%r12)
	movq	%rsi, 24(%rbp)
.L35:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	decrement_at_index.part.8, .-decrement_at_index.part.8
	.p2align 4,,15
	.type	same_address, @function
same_address:
	movzwl	(%rdi), %edx
	xorl	%eax, %eax
	cmpw	(%rsi), %dx
	je	.L49
.L38:
	rep ret
	.p2align 4,,10
	.p2align 3
.L49:
	cmpw	$2, %dx
	je	.L40
	cmpw	$10, %dx
	jne	.L50
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rcx
	xorq	8(%rsi), %rdx
	xorq	16(%rsi), %rcx
	orq	%rdx, %rcx
	jne	.L38
	movzwl	2(%rsi), %ecx
	cmpw	%cx, 2(%rdi)
	jne	.L38
	movl	24(%rsi), %eax
	cmpl	%eax, 24(%rdi)
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	rep ret
	.p2align 4,,10
	.p2align 3
.L40:
	movzwl	2(%rsi), %eax
	movl	4(%rsi), %ecx
	cmpw	%ax, 2(%rdi)
	sete	%al
	cmpl	%ecx, 4(%rdi)
	sete	%dl
	andl	%edx, %eax
	ret
	.size	same_address, .-same_address
	.p2align 4,,15
	.type	resolv_conf_matches, @function
resolv_conf_matches:
	movq	16(%rsi), %rax
	movslq	16(%rdi), %r11
	movl	$3, %edx
	cmpq	$3, %rax
	cmovbe	%rax, %rdx
	xorl	%eax, %eax
	cmpq	%rdx, %r11
	je	.L100
	rep ret
	.p2align 4,,10
	.p2align 3
.L100:
	movzwl	512(%rdi), %edx
	testw	%dx, %dx
	jne	.L101
.L53:
	pushq	%r15
	pushq	%r14
	xorl	%r8d, %r8d
	pushq	%r13
	pushq	%r12
	leaq	20(%rdi), %r9
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	testq	%r11, %r11
	jne	.L56
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L102:
	movq	536(%rbx,%r8,8), %rdi
	cmpw	$10, (%rdi)
	jne	.L80
	movq	8(%rbp), %rax
	movq	(%rax,%r8,8), %rsi
	call	same_address
	testb	%al, %al
	je	.L51
	leaq	1(%r8), %r10
.L55:
	addq	$16, %r9
	cmpq	%r10, %r11
	movq	%r10, %r8
	jbe	.L57
.L56:
	movzwl	(%r9), %eax
	testw	%ax, %ax
	je	.L102
	cmpw	$2, %ax
	jne	.L80
	movq	8(%rbp), %rax
	movq	%r9, %rdi
	leaq	1(%r8), %r10
	movq	(%rax,%r8,8), %rsi
	call	same_address
	testb	%al, %al
	jne	.L55
.L51:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	cmpq	%rdx, %r11
	je	.L53
	rep ret
	.p2align 4,,10
	.p2align 3
.L57:
	movq	72(%rbx), %r12
	testq	%r12, %r12
	je	.L103
	leaq	128(%rbx), %rdx
	xorl	%eax, %eax
	cmpq	%rdx, %r12
	jne	.L51
	movq	32(%rbp), %rax
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L61
	movq	24(%rbp), %r15
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %r14
	cmpq	8(%rsp), %r14
	je	.L61
	movq	72(%rbx,%r14,8), %r12
	testq	%r12, %r12
	je	.L63
.L62:
	movq	%r12, %rdi
	call	__GI_strlen
	movq	(%r15,%r14,8), %rsi
	movq	%r12, %rdi
	leaq	1(%r13,%rax), %r13
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L95
	.p2align 4,,10
	.p2align 3
.L80:
	xorl	%eax, %eax
	jmp	.L51
.L103:
	xorl	%eax, %eax
	cmpq	$0, 32(%rbp)
	jne	.L51
	cmpb	$0, 128(%rbx)
	sete	%al
	jmp	.L51
.L63:
	cmpq	$6, %r14
	sete	%dl
	cmpq	$56, %r13
	seta	%al
	orb	%dl, %al
	je	.L51
.L61:
	movzbl	392(%rbx), %edx
	movq	48(%rbp), %rax
	movl	$10, %ecx
	shrb	$4, %dl
	cmpq	$10, %rax
	cmovbe	%rax, %rcx
	movzbl	%dl, %edx
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jne	.L51
	testq	%rdx, %rdx
	je	.L76
	movq	40(%rbp), %rcx
	movl	(%rcx), %edi
	cmpl	%edi, 396(%rbx)
	jne	.L51
	movl	4(%rcx), %edi
	cmpl	%edi, 400(%rbx)
	jne	.L51
	xorl	%eax, %eax
	jmp	.L66
.L67:
	movl	(%rcx,%rax,8), %edi
	cmpl	%edi, 396(%rbx,%rax,8)
	jne	.L80
	movl	4(%rcx,%rax,8), %edi
	cmpl	%edi, 400(%rbx,%rax,8)
	jne	.L80
.L66:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L67
.L76:
	movl	$1, %eax
	jmp	.L51
	.size	resolv_conf_matches, .-resolv_conf_matches
	.section	.rodata.str1.1
.LC2:
	.string	"/etc/resolv.conf"
	.text
	.p2align 4,,15
	.globl	__resolv_conf_get_current
	.hidden	__resolv_conf_get_current
	.type	__resolv_conf_get_current, @function
__resolv_conf_get_current:
	pushq	%r13
	pushq	%r12
	leaq	.LC2(%rip), %rsi
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movq	%rsp, %r12
	movq	%r12, %rdi
	call	__GI___file_change_detection_for_path
	testb	%al, %al
	je	.L107
	call	get_locked_global
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L107
	cmpq	$0, 32(%rax)
	je	.L108
	leaq	40(%rax), %rsi
	movq	%r12, %rdi
	call	__GI___file_is_unchanged
	testb	%al, %al
	je	.L108
	movq	32(%rbp), %rbx
.L109:
	testq	%rbx, %rbx
	je	.L113
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L131
	addq	$1, %rax
	testq	%rax, %rax
	movq	%rax, (%rbx)
	je	.L132
.L113:
	call	put_locked_global.isra.2
	addq	$104, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L108:
	leaq	48(%rsp), %r13
	xorl	%edi, %edi
	movq	%r13, %rsi
	call	__resolv_conf_load
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L113
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L111
	call	conf_decrement
.L111:
	movq	%rbx, 32(%rbp)
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	__GI___file_is_unchanged
	testb	%al, %al
	je	.L112
	movdqa	48(%rsp), %xmm0
	movups	%xmm0, 40(%rbp)
	movdqa	64(%rsp), %xmm0
	movups	%xmm0, 56(%rbp)
	movdqa	80(%rsp), %xmm0
	movups	%xmm0, 72(%rbp)
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L107:
	addq	$104, %rsp
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movq	$-1, 40(%rbp)
	jmp	.L109
.L132:
	leaq	__PRETTY_FUNCTION__.9523(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$166, %edx
	call	__GI___assert_fail
.L131:
	leaq	__PRETTY_FUNCTION__.9523(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$164, %edx
	call	__GI___assert_fail
	.size	__resolv_conf_get_current, .-__resolv_conf_get_current
	.p2align 4,,15
	.globl	__resolv_conf_put
	.hidden	__resolv_conf_put
	.type	__resolv_conf_put, @function
__resolv_conf_put:
	testq	%rdi, %rdi
	je	.L141
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 355 "resolv_conf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L136
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L137:
	movq	%rbx, %rdi
	call	conf_decrement
#APP
# 357 "resolv_conf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L138
	subl	$1, lock(%rip)
.L133:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	rep ret
	.p2align 4,,10
	.p2align 3
.L138:
	xorl	%eax, %eax
#APP
# 357 "resolv_conf.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L133
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 357 "resolv_conf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L137
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L137
	.size	__resolv_conf_put, .-__resolv_conf_put
	.p2align 4,,15
	.globl	__resolv_conf_get
	.hidden	__resolv_conf_get
	.type	__resolv_conf_get, @function
__resolv_conf_get:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testb	$1, 8(%rdi)
	je	.L155
	movq	%rdi, %rbp
	call	get_locked_global
	testq	%rax, %rax
	je	.L155
	movabsq	$2785751652381982817, %rdx
	xorq	560(%rbp), %rdx
	cmpq	(%rax), %rdx
	jnb	.L149
	movq	16(%rax), %rax
	movq	(%rax,%rdx,8), %rbx
	testb	$1, %bl
	jne	.L149
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L156
	addq	$1, %rax
	movq	%rax, (%rbx)
	call	put_locked_global.isra.2
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	resolv_conf_matches
	testb	%al, %al
	jne	.L144
	movq	%rbx, %rdi
	call	__resolv_conf_put
.L155:
	xorl	%ebx, %ebx
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	xorl	%ebx, %ebx
	call	put_locked_global.isra.2
.L144:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
.L156:
	leaq	__PRETTY_FUNCTION__.9531(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$194, %edx
	call	__GI___assert_fail
	.size	__resolv_conf_get, .-__resolv_conf_get
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"init->nameserver_list[i]->sa_family == AF_INET6"
	.section	.rodata.str1.1
.LC4:
	.string	"conf == ptr"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"!alloc_buffer_has_failed (&buffer)"
	.text
	.p2align 4,,15
	.globl	__resolv_conf_allocate
	.hidden	__resolv_conf_allocate
	.type	__resolv_conf_allocate, @function
__resolv_conf_allocate:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	16(%rdi), %r12
	testq	%r12, %r12
	je	.L179
	movq	8(%rdi), %rcx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L159:
	cmpw	$10, %dx
	jne	.L214
	addq	$1, %rax
	addq	$28, %rbx
	cmpq	%r12, %rax
	je	.L158
.L162:
	movq	(%rcx,%rax,8), %rdx
	movzwl	(%rdx), %edx
	cmpw	$2, %dx
	jne	.L159
	addq	$1, %rax
	addq	$16, %rbx
	cmpq	%r12, %rax
	jne	.L162
.L158:
	movq	32(%r13), %rax
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L163
	movq	24(%r13), %rbp
	xorl	%r15d, %r15d
	leaq	0(%rbp,%rax,8), %r14
	.p2align 4,,10
	.p2align 3
.L164:
	movq	0(%rbp), %rdi
	addq	$8, %rbp
	call	__GI_strlen
	cmpq	%rbp, %r14
	leaq	1(%r15,%rax), %r15
	jne	.L164
	addq	%r15, %rbx
.L163:
	movq	48(%r13), %rax
	movq	8(%rsp), %r15
	leaq	24(%rsp), %rsi
	leaq	9(%r12,%rax), %rax
	addq	%rax, %r15
	leaq	(%rbx,%r15,8), %rdi
	call	__GI___libc_alloc_buffer_allocate
	leaq	7(%rax), %rcx
	movq	%rdx, 40(%rsp)
	andq	$-8, %rcx
	leaq	72(%rcx), %rsi
	cmpq	%rsi, %rdx
	setnb	%dil
	cmpq	$71, %rsi
	seta	%dl
	testb	%dl, %dil
	je	.L165
	cmpq	%rcx, %rax
	ja	.L165
	testq	%rcx, %rcx
	movq	%rsi, 32(%rsp)
	je	.L165
	movq	24(%rsp), %rbx
	cmpq	%rcx, %rbx
	jne	.L215
	movl	60(%r13), %eax
	leaq	32(%rsp), %r12
	movl	$8, %edx
	movl	$8, %esi
	movq	$1, (%rbx)
	movq	%r12, %rdi
	movl	%eax, 60(%rbx)
	movl	64(%r13), %eax
	movl	%eax, 64(%rbx)
	movl	56(%r13), %eax
	movl	%eax, 56(%rbx)
	movl	68(%r13), %eax
	movl	%eax, 68(%rbx)
	movq	16(%r13), %rax
	movq	%rax, 16(%rbx)
	movq	16(%r13), %rcx
	call	__GI___libc_alloc_buffer_alloc_array
	movq	%rax, 8(%rbx)
	movq	%rax, %r14
	movq	32(%r13), %rax
	movl	$8, %edx
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%rax, 32(%rbx)
	movq	32(%r13), %rcx
	call	__GI___libc_alloc_buffer_alloc_array
	cmpq	$0, 16(%r13)
	movq	%rax, %rbp
	movq	%rax, 24(%rbx)
	je	.L167
	xorl	%ecx, %ecx
	movq	8(%r13), %r8
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	28(%rax), %rsi
	cmpq	$27, %rsi
	jbe	.L171
	testb	%dil, %dil
	je	.L171
	cmpq	40(%rsp), %rsi
	ja	.L171
	movdqu	(%rdx), %xmm0
	movq	%rsi, 32(%rsp)
	movups	%xmm0, (%rax)
	movq	16(%rdx), %rsi
	movq	%rsi, 16(%rax)
	movl	24(%rdx), %edx
	movl	%edx, 24(%rax)
	movq	%rax, (%r14,%rcx,8)
	addq	$1, %rcx
	cmpq	%rcx, 16(%r13)
	jbe	.L167
.L172:
	movq	32(%rsp), %rsi
	movq	(%r8,%rcx,8), %rdx
	leaq	3(%rsi), %rax
	andq	$-4, %rax
	cmpq	%rax, %rsi
	setbe	%dil
	cmpw	$2, (%rdx)
	jne	.L168
	leaq	16(%rax), %rsi
	cmpq	$15, %rsi
	jbe	.L169
	testb	%dil, %dil
	je	.L169
	cmpq	40(%rsp), %rsi
	ja	.L169
	movdqu	(%rdx), %xmm0
	movq	%rsi, 32(%rsp)
	movups	%xmm0, (%rax)
	movq	%rax, (%r14,%rcx,8)
	addq	$1, %rcx
	cmpq	%rcx, 16(%r13)
	ja	.L172
.L167:
	movq	48(%r13), %rax
	movl	$8, %esi
	movl	$4, %edx
	movq	%r12, %rdi
	movq	%rax, 48(%rbx)
	movq	48(%r13), %rcx
	call	__GI___libc_alloc_buffer_alloc_array
	movq	48(%r13), %rsi
	movq	%rax, 40(%rbx)
	testq	%rsi, %rsi
	je	.L173
	movq	40(%r13), %rdi
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L174:
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rax,%rdx,8)
	addq	$1, %rdx
	cmpq	%rsi, %rdx
	jne	.L174
.L173:
	cmpq	$0, 32(%r13)
	movq	32(%rsp), %r15
	je	.L180
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L177:
	movq	24(%r13), %rax
	movq	32(%rsp), %rdi
	movq	40(%rsp), %rsi
	movq	(%rax,%r14,8), %rdx
	call	__GI___libc_alloc_buffer_copy_string
	testq	%rax, %rax
	movq	%rax, 32(%rsp)
	movq	%rdx, 40(%rsp)
	cmove	%r12, %r15
	movq	%r15, 0(%rbp,%r14,8)
	addq	$1, %r14
	cmpq	%r14, 32(%r13)
	movq	%rax, %r15
	ja	.L177
.L175:
	testq	%rax, %rax
	je	.L216
.L157:
	addq	$56, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	movdqu	(%rdx), %xmm0
	movaps	%xmm0, 0
	movq	16(%rdx), %rax
	movq	%rax, 16
	movl	24(%rdx), %eax
	movl	%eax, 24
	ud2
	.p2align 4,,10
	.p2align 3
.L169:
	movdqu	(%rdx), %xmm0
	movaps	%xmm0, 0
	ud2
	.p2align 4,,10
	.p2align 3
.L165:
	xorl	%ebx, %ebx
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L180:
	movq	%r15, %rax
	jmp	.L175
.L179:
	xorl	%ebx, %ebx
	jmp	.L158
.L214:
	leaq	__PRETTY_FUNCTION__.9622(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$380, %edx
	call	__GI___assert_fail
.L216:
	leaq	__PRETTY_FUNCTION__.9622(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$460, %edx
	call	__GI___assert_fail
.L215:
	leaq	__PRETTY_FUNCTION__.9622(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$404, %edx
	call	__GI___assert_fail
	.size	__resolv_conf_allocate, .-__resolv_conf_allocate
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"global_copy->free_list_start == 0 || global_copy->free_list_start & 1"
	.align 8
.LC7:
	.string	"conf->nameserver_list[i]->sa_family == AF_INET6"
	.align 8
.LC8:
	.string	"resolv_conf_matches (resp, conf)"
	.text
	.p2align 4,,15
	.globl	__resolv_conf_attach
	.hidden	__resolv_conf_attach
	.type	__resolv_conf_attach, @function
__resolv_conf_attach:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpq	$0, (%rsi)
	je	.L278
	movq	%rdi, %rbp
	movq	%rsi, %r15
	call	get_locked_global
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L277
	movq	24(%rax), %rax
	movq	(%rbx), %r12
	testb	$1, %al
	je	.L220
	shrq	%rax
	cmpq	%r12, %rax
	movq	%rax, 8(%rsp)
	jnb	.L279
	movq	16(%rbx), %rax
	movq	8(%rsp), %rcx
	leaq	(%rax,%rcx,8), %rdx
	movq	(%rdx), %rax
	testq	%rax, %rax
	movq	%rax, 24(%rbx)
	je	.L222
	testb	$1, %al
	je	.L280
.L222:
	movq	%r15, (%rdx)
.L223:
	addq	$1, (%r15)
	je	.L281
	call	put_locked_global.isra.2
	andb	$-2, 393(%rbp)
	movl	$4294967295, %eax
	movb	$0, 128(%rbp)
	movq	%rax, 500(%rbp)
	movl	60(%r15), %eax
	xorl	%ecx, %ecx
	movzbl	68(%r15), %edx
	movl	$3, %r13d
	movq	$0, 384(%rbp)
	movq	$0, 480(%rbp)
	movl	%eax, 0(%rbp)
	movl	64(%r15), %eax
	movq	$0, 488(%rbp)
	andl	$15, %edx
	movl	%eax, 4(%rbp)
	movl	56(%r15), %eax
	movl	$0, 16(%rbp)
	movw	%cx, 512(%rbp)
	movq	%rax, 8(%rbp)
	movzbl	392(%rbp), %eax
	andl	$-16, %eax
	orl	%edx, %eax
	movb	%al, 392(%rbp)
	movq	16(%r15), %rax
	cmpq	$3, %rax
	cmovbe	%rax, %r13
	testq	%rax, %rax
	je	.L229
	xorl	%ebx, %ebx
	jmp	.L236
	.p2align 4,,10
	.p2align 3
.L230:
	cmpw	$10, %ax
	jne	.L282
	movq	%rbx, %rax
	xorl	%edx, %edx
	movl	$28, %edi
	salq	$4, %rax
	movw	%dx, 20(%rbp,%rax)
	call	malloc@PLT
	testq	%rax, %rax
	je	.L283
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rax)
	movq	16(%r12), %rdx
	movq	%rdx, 16(%rax)
	movl	24(%r12), %edx
	movq	%rax, 536(%rbp,%rbx,8)
	movl	%edx, 24(%rax)
.L231:
	movl	$-1, 520(%rbp,%rbx,4)
	addq	$1, %rbx
	cmpq	%rbx, %r13
	jbe	.L229
.L236:
	movq	8(%r15), %rax
	leaq	0(,%rbx,8), %r14
	movq	(%rax,%rbx,8), %r12
	movzwl	(%r12), %eax
	cmpw	$2, %ax
	jne	.L230
	movdqu	(%r12), %xmm0
	movq	%rbx, %rax
	salq	$4, %rax
	movups	%xmm0, 20(%rbp,%rax)
	movq	$0, 536(%rbp,%rbx,8)
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L220:
	movq	8(%rbx), %rax
	cmpq	$-1, %rax
	je	.L224
	cmpq	%r12, %rax
	je	.L284
	movq	16(%rbx), %rax
	leaq	1(%r12), %rdx
	movq	%rdx, (%rbx)
	movq	%r15, (%rax,%r12,8)
.L227:
	cmpq	$-1, 8(%rbx)
	je	.L224
	movq	%r12, 8(%rsp)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L229:
	leaq	128(%rbp), %rbx
	movl	%r13d, 16(%rbp)
	movq	%rbx, %r12
	addq	$256, %r12
	jc	.L285
.L239:
	movq	%r12, %rsi
	movq	32(%r15), %r12
	xorl	%r13d, %r13d
	testq	%r12, %r12
	jne	.L242
.L240:
	movq	48(%r15), %rax
	movl	$10, %esi
	movq	$0, 72(%rbp,%r12,8)
	cmpq	$10, %rax
	cmovbe	%rax, %rsi
	testq	%rax, %rax
	je	.L244
	movq	40(%r15), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L245:
	movl	(%rdx,%rax,8), %edi
	movl	%edi, 396(%rbp,%rax,8)
	movl	4(%rdx,%rax,8), %edi
	movl	%edi, 400(%rbp,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %rsi
	ja	.L245
.L244:
	movzbl	392(%rbp), %eax
	sall	$4, %esi
	movq	%rbp, %rdi
	andl	$15, %eax
	orl	%eax, %esi
	movb	%sil, 392(%rbp)
	movq	%r15, %rsi
	call	resolv_conf_matches
	testb	%al, %al
	je	.L286
	movq	8(%rsp), %r15
	movabsq	$2785751652381982817, %rdx
	xorq	%rdx, %r15
	movq	%r15, 560(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L288:
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	$0, 16(%rbx)
	movq	$0, (%rbx)
	movq	$-1, 8(%rbx)
	.p2align 4,,10
	.p2align 3
.L224:
	call	put_locked_global.isra.2
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L277:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L283:
	testq	%rbx, %rbx
	je	.L234
	addq	%rbp, %r14
.L235:
	movq	536(%rbp), %rdi
	addq	$8, %rbp
	call	free@PLT
	cmpq	%rbp, %r14
	jne	.L235
.L234:
	call	get_locked_global
	movq	8(%rsp), %rcx
	cmpq	(%rax), %rcx
	jb	.L287
.L247:
	call	put_locked_global.isra.2
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L287:
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	decrement_at_index.part.8
	jmp	.L247
.L241:
	movq	%rbx, 72(%rbp,%r13,8)
	addq	$1, %r13
	cmpq	%r13, %r12
	jbe	.L250
	cmpq	$5, %r13
	ja	.L250
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L242:
	movq	24(%r15), %rax
	movq	%rbx, %rdi
	movq	(%rax,%r13,8), %rdx
	call	__GI___libc_alloc_buffer_copy_string
	testq	%rax, %rax
	movq	%rdx, %rsi
	jne	.L241
	movq	$0, 72(%rbp,%r13,8)
	movq	%r13, %r12
	jmp	.L240
.L250:
	movq	%r13, %r12
	jmp	.L240
.L279:
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	__GI___libc_dynarray_at_failure
.L284:
	xorl	%esi, %esi
	movl	$8, %edx
	movq	%rbx, %rdi
	call	__GI___libc_dynarray_emplace_enlarge
	testb	%al, %al
	je	.L288
	movq	(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rsi
	movq	%rsi, (%rbx)
	movq	%r15, (%rdx,%rax,8)
	jmp	.L227
.L282:
	leaq	__PRETTY_FUNCTION__.9657(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$498, %edx
	call	__GI___assert_fail
.L278:
	leaq	__PRETTY_FUNCTION__.9687(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$580, %edx
	call	__GI___assert_fail
.L280:
	leaq	__PRETTY_FUNCTION__.9687(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$596, %edx
	call	__GI___assert_fail
.L285:
	movl	$256, %esi
	movq	%rbx, %rdi
	call	__GI___libc_alloc_buffer_create_failure
	jmp	.L239
.L286:
	leaq	__PRETTY_FUNCTION__.9657(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$551, %edx
	call	__GI___assert_fail
.L281:
	leaq	__PRETTY_FUNCTION__.9687(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$618, %edx
	call	__GI___assert_fail
	.size	__resolv_conf_attach, .-__resolv_conf_attach
	.p2align 4,,15
	.globl	__resolv_conf_detach
	.hidden	__resolv_conf_detach
	.type	__resolv_conf_detach, @function
__resolv_conf_detach:
	movq	global(%rip), %rax
	testq	%rax, %rax
	jne	.L295
	rep ret
	.p2align 4,,10
	.p2align 3
.L295:
	pushq	%rbx
	movq	%rdi, %rbx
	call	get_locked_global
	movabsq	$2785751652381982817, %rsi
	xorq	560(%rbx), %rsi
	cmpq	(%rax), %rsi
	jb	.L296
.L291:
	movq	$0, 560(%rbx)
	popq	%rbx
	jmp	put_locked_global.isra.2
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%rax, %rdi
	call	decrement_at_index.part.8
	jmp	.L291
	.size	__resolv_conf_detach, .-__resolv_conf_detach
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9657, @object
	.size	__PRETTY_FUNCTION__.9657, 17
__PRETTY_FUNCTION__.9657:
	.string	"update_from_conf"
	.align 16
	.type	__PRETTY_FUNCTION__.9687, @object
	.size	__PRETTY_FUNCTION__.9687, 21
__PRETTY_FUNCTION__.9687:
	.string	"__resolv_conf_attach"
	.align 16
	.type	__PRETTY_FUNCTION__.9622, @object
	.size	__PRETTY_FUNCTION__.9622, 23
__PRETTY_FUNCTION__.9622:
	.string	"__resolv_conf_allocate"
	.align 16
	.type	__PRETTY_FUNCTION__.9531, @object
	.size	__PRETTY_FUNCTION__.9531, 18
__PRETTY_FUNCTION__.9531:
	.string	"resolv_conf_get_1"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9515, @object
	.size	__PRETTY_FUNCTION__.9515, 15
__PRETTY_FUNCTION__.9515:
	.string	"conf_decrement"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.9523, @object
	.size	__PRETTY_FUNCTION__.9523, 26
__PRETTY_FUNCTION__.9523:
	.string	"__resolv_conf_get_current"
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_freeres__, @object
	.size	__elf_set___libc_subfreeres_element_freeres__, 8
__elf_set___libc_subfreeres_element_freeres__:
	.quad	freeres
	.local	lock
	.comm	lock,4,4
	.local	global
	.comm	global,8,8
	.hidden	__resolv_conf_load
	.hidden	__lll_lock_wait_private
