	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	search_list_add__, @function
search_list_add__:
	pushq	%r12
	pushq	%rbp
	leaq	24(%rdi), %rbp
	pushq	%rbx
	movq	%rsi, %r12
	movl	$8, %edx
	movq	%rbp, %rsi
	movq	%rdi, %rbx
	call	__GI___libc_dynarray_emplace_enlarge
	testb	%al, %al
	je	.L7
	movq	(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movq	%r12, (%rdx,%rax,8)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %rbp
	je	.L3
	call	free@PLT
.L3:
	movq	%rbp, 16(%rbx)
	movq	$0, (%rbx)
	movq	$-1, 8(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	search_list_add__, .-search_list_add__
	.p2align 4,,15
	.type	nameserver_list_emplace, @function
nameserver_list_emplace:
	movq	8(%rdi), %rax
	cmpq	$-1, %rax
	je	.L15
	movq	(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.L24
	movq	16(%rdi), %rax
	leaq	(%rax,%rdx,8), %rax
	addq	$1, %rdx
	movq	%rdx, (%rdi)
	movq	$0, (%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%r13
	pushq	%r12
	leaq	24(%rdi), %r12
	pushq	%rbp
	pushq	%rbx
	movl	$8, %edx
	movq	%r12, %rsi
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__GI___libc_dynarray_emplace_enlarge
	testb	%al, %al
	je	.L25
	movq	(%rbx), %rdx
	movq	16(%rbx), %rax
	leaq	(%rax,%rdx,8), %rax
	addq	$1, %rdx
	movq	%rdx, (%rbx)
	movq	$0, (%rax)
.L8:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	ret
.L25:
	movq	(%rbx), %rax
	movq	16(%rbx), %rbp
	testq	%rax, %rax
	je	.L12
	leaq	0(%rbp,%rax,8), %r13
	.p2align 4,,10
	.p2align 3
.L13:
	movq	0(%rbp), %rdi
	addq	$8, %rbp
	call	free@PLT
	cmpq	%rbp, %r13
	jne	.L13
	movq	16(%rbx), %rbp
.L12:
	cmpq	%rbp, %r12
	je	.L14
	movq	%rbp, %rdi
	call	free@PLT
.L14:
	movq	%r12, 16(%rbx)
	movq	$0, (%rbx)
	xorl	%eax, %eax
	movq	$-1, 8(%rbx)
	jmp	.L8
	.size	nameserver_list_emplace, .-nameserver_list_emplace
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ndots:"
.LC1:
	.string	"timeout:"
.LC2:
	.string	"attempts:"
	.text
	.p2align 4,,15
	.type	res_setoptions, @function
res_setoptions:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	.LC1(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rbp
	movq	%rdi, %r12
	movq	%rsi, %rbx
	subq	$8, %rsp
	movzbl	(%rsi), %eax
.L27:
	testb	%al, %al
	je	.L26
.L70:
	cmpb	$32, %al
	jne	.L59
	.p2align 4,,10
	.p2align 3
.L54:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	cmpb	$32, %al
	je	.L54
.L59:
	cmpb	$9, %al
	je	.L54
	movl	$6, %ecx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	repz cmpsb
	je	.L65
	movl	$8, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	repz cmpsb
	je	.L66
	leaq	.LC2(%rip), %rdi
	movl	$9, %ecx
	movq	%rbx, %rsi
	repz cmpsb
	je	.L34
	leaq	options.13954(%rip), %r14
	movl	$6, %edx
	xorl	%r15d, %r15d
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L68:
	movzbl	22(%r14), %edx
.L35:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__GI_strncmp
	testl	%eax, %eax
	je	.L67
	addl	$1, %r15d
	addq	$32, %r14
	cmpl	$9, %r15d
	jne	.L68
	movzbl	(%rbx), %eax
	testb	$-33, %al
	je	.L27
	.p2align 4,,10
	.p2align 3
.L69:
	cmpb	$9, %al
	je	.L27
	addq	$1, %rbx
.L62:
	movzbl	(%rbx), %eax
	testb	$-33, %al
	jne	.L69
	testb	%al, %al
	jne	.L70
.L26:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	leaq	6(%rbx), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	__GI_strtol
	movl	$15, %edx
	cmpl	$15, %eax
	cmovg	%edx, %eax
	movl	%eax, 228(%r12)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L66:
	leaq	8(%rbx), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	__GI_strtol
	movl	$30, %edx
	cmpl	$30, %eax
	cmovg	%edx, %eax
	movl	%eax, 220(%r12)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	9(%rbx), %rdi
	movl	$10, %edx
	xorl	%esi, %esi
	call	__GI_strtol
	movl	$5, %edx
	cmpl	$5, %eax
	cmovg	%edx, %eax
	movl	%eax, 224(%r12)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L67:
	movslq	%r15d, %r8
	leaq	options.13954(%rip), %rax
	movl	216(%r12), %edx
	salq	$5, %r8
	addq	%r8, %rax
	movq	24(%rax), %rcx
	movl	%edx, %esi
	orl	%ecx, %esi
	andl	%ecx, %edx
	cmpb	$0, 23(%rax)
	movl	%esi, %eax
	cmovne	%edx, %eax
	movl	%eax, 216(%r12)
	jmp	.L62
	.size	res_setoptions, .-res_setoptions
	.section	.rodata.str1.1
.LC3:
	.string	"rce"
.LC4:
	.string	"/etc/resolv.conf"
.LC5:
	.string	"LOCALDOMAIN"
.LC6:
	.string	"domain"
.LC7:
	.string	" \t\n"
.LC8:
	.string	"search"
.LC9:
	.string	"nameserver"
.LC10:
	.string	"sortlist"
.LC11:
	.string	"options"
.LC12:
	.string	"RES_OPTIONS"
	.text
	.p2align 4,,15
	.globl	__resolv_conf_load
	.hidden	__resolv_conf_load
	.type	__resolv_conf_load, @function
__resolv_conf_load:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$568, %rsp
	movq	%rsi, 16(%rsp)
	call	_res_hconf_init
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L331
.L72:
	leaq	64(%rsp), %rbx
	testq	%r12, %r12
	movq	$0, 64(%rsp)
	movq	$0, 120(%rsp)
	movq	$0, 72(%rsp)
	leaq	32(%rbx), %rax
	movq	$3, 80(%rsp)
	movq	$0, 128(%rsp)
	movq	$6, 136(%rsp)
	movq	$0, 200(%rsp)
	movq	%rax, 88(%rsp)
	leaq	88(%rbx), %rax
	movq	$0, 208(%rsp)
	movq	$0, 216(%rsp)
	movq	%rax, 144(%rsp)
	je	.L74
	movl	(%r12), %eax
	movl	%eax, 284(%rsp)
	movl	4(%r12), %eax
	movl	%eax, 288(%rsp)
	movq	8(%r12), %rax
	orl	$1, %eax
	movl	%eax, 280(%rsp)
.L75:
	leaq	.LC5(%rip), %rdi
	movl	$1, 292(%rsp)
	movq	$0, 56(%rsp)
	call	__GI_getenv
	testq	%rax, %rax
	je	.L171
	movq	%rax, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L330
	movq	120(%rsp), %rdi
	call	free@PLT
	movq	136(%rsp), %rax
	movq	%r12, 120(%rsp)
	cmpq	$-1, %rax
	je	.L78
	movq	128(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L332
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	movq	144(%rsp), %rax
	movq	%r12, (%rax,%rdx,8)
.L78:
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L328
	cmpb	$10, %dl
	je	.L81
	movl	$1, %r13d
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L334:
	cmpb	$9, %dl
	je	.L185
	testb	%r13b, %r13b
	je	.L333
.L85:
	addq	$1, %r12
	movzbl	(%r12), %edx
	testb	%dl, %dl
	je	.L328
.L87:
	cmpb	$10, %dl
	je	.L81
.L82:
	cmpb	$32, %dl
	jne	.L334
.L185:
	movb	$0, (%r12)
	addq	$1, %r12
	movzbl	(%r12), %edx
	xorl	%r13d, %r13d
	testb	%dl, %dl
	jne	.L87
.L328:
	movb	$1, 35(%rsp)
.L351:
	testq	%rbp, %rbp
	jne	.L335
.L89:
	cmpq	$0, 72(%rsp)
	je	.L336
.L143:
	cmpq	$0, 128(%rsp)
	jne	.L146
	leaq	304(%rsp), %r12
	movl	$255, %esi
	movb	$0, 559(%rsp)
	movq	%r12, %rdi
	call	__gethostname
	testl	%eax, %eax
	jne	.L146
	movl	$46, %esi
	movq	%r12, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L146
	leaq	1(%rax), %rdi
	xorl	%r13d, %r13d
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L122
	movq	120(%rsp), %rdi
	call	free@PLT
	movq	136(%rsp), %rdx
	movq	%r12, 120(%rsp)
	cmpq	$-1, %rdx
	je	.L146
	movq	128(%rsp), %rax
	cmpq	%rax, %rdx
	je	.L337
	leaq	1(%rax), %rdx
	movq	%rdx, 128(%rsp)
	movq	144(%rsp), %rdx
	movq	%r12, (%rdx,%rax,8)
	.p2align 4,,10
	.p2align 3
.L146:
	leaq	.LC12(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	je	.L153
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	res_setoptions
.L153:
	cmpq	$-1, 80(%rsp)
	je	.L154
	cmpq	$-1, 136(%rsp)
	je	.L154
	cmpq	$-1, 208(%rsp)
	je	.L154
	cmpq	$0, 16(%rsp)
	jne	.L156
	.p2align 4,,10
	.p2align 3
.L157:
	movq	88(%rsp), %rax
	leaq	160(%rbx), %rdi
	movq	%rax, 232(%rsp)
	movq	72(%rsp), %rax
	movq	%rax, 240(%rsp)
	movq	144(%rsp), %rax
	movq	%rax, 248(%rsp)
	movq	128(%rsp), %rax
	movq	%rax, 256(%rsp)
	movq	216(%rsp), %rax
	movq	%rax, 264(%rsp)
	movq	200(%rsp), %rax
	movq	%rax, 272(%rsp)
	call	__resolv_conf_allocate
	movq	%rax, %r13
	jmp	.L122
.L344:
	movq	%r14, %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L330:
	xorl	%r13d, %r13d
.L122:
	movq	64(%rsp), %rdi
	call	free@PLT
	movq	120(%rsp), %rdi
	call	free@PLT
	movq	72(%rsp), %rax
	movq	88(%rsp), %rdi
	testq	%rax, %rax
	je	.L158
	leaq	(%rdi,%rax,8), %r14
	movq	%rdi, %r12
	.p2align 4,,10
	.p2align 3
.L159:
	movq	(%r12), %rdi
	addq	$8, %r12
	call	free@PLT
	cmpq	%r12, %r14
	jne	.L159
	movq	88(%rsp), %rdi
.L158:
	leaq	32(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L160
	call	free@PLT
.L160:
	leaq	32(%rbx), %rax
	movq	144(%rsp), %rdi
	movq	$0, 72(%rsp)
	movq	$3, 80(%rsp)
	movq	%rax, 88(%rsp)
	leaq	88(%rbx), %rax
	cmpq	%rax, %rdi
	je	.L161
	call	free@PLT
.L161:
	movq	216(%rsp), %rdi
	addq	$88, %rbx
	movq	$0, 128(%rsp)
	movq	$6, 136(%rsp)
	movq	%rbx, 144(%rsp)
	call	free@PLT
	testq	%rbp, %rbp
	movq	$0, 200(%rsp)
	movq	$0, 208(%rsp)
	movq	$0, 216(%rsp)
	je	.L71
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%rbp, %rdi
	movl	%fs:(%rbx), %r12d
	call	_IO_new_fclose@PLT
	movl	%r12d, %fs:(%rbx)
.L71:
	addq	$568, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	testq	%rbp, %rbp
	movb	$0, 35(%rsp)
	je	.L89
.L335:
	orl	$32768, 0(%rbp)
	leaq	56(%rsp), %r12
	leaq	.LC6(%rip), %r13
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__getline
	testq	%rax, %rax
	jle	.L338
	movq	64(%rsp), %r15
	movzbl	(%r15), %edx
	cmpb	$59, %dl
	je	.L90
	cmpb	$35, %dl
	je	.L90
	movl	$6, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	repz cmpsb
	jne	.L93
	movzbl	6(%r15), %edx
	cmpb	$32, %dl
	je	.L186
	cmpb	$9, %dl
	je	.L186
.L94:
	leaq	.LC9(%rip), %rdi
	movl	$10, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L110
	movzbl	10(%r15), %edx
	cmpb	$32, %dl
	je	.L188
	cmpb	$9, %dl
	je	.L188
.L110:
	leaq	.LC10(%rip), %rdi
	movl	$8, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L123
	movzbl	8(%r15), %r14d
	cmpb	$32, %r14b
	je	.L189
	cmpb	$9, %r14b
	je	.L189
.L123:
	leaq	.LC11(%rip), %rdi
	movl	$7, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L90
	movzbl	7(%r15), %edx
	cmpb	$32, %dl
	je	.L190
	cmpb	$9, %dl
	jne	.L90
.L190:
	leaq	7(%r15), %rsi
	movq	%rbx, %rdi
	call	res_setoptions
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	.LC8(%rip), %rdi
	movl	$6, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L94
	movzbl	6(%r15), %edx
	cmpb	$32, %dl
	sete	%r14b
	cmpb	$9, %dl
	sete	%dl
	orb	%dl, %r14b
	je	.L94
	cmpb	$0, 35(%rsp)
	jne	.L90
	addq	$6, %r15
	.p2align 4,,10
	.p2align 3
.L269:
	addq	$1, %r15
	movzbl	(%r15), %eax
	cmpb	$32, %al
	je	.L269
	cmpb	$9, %al
	je	.L269
	testb	%al, %al
	sete	%dl
	cmpb	$10, %al
	sete	%al
	orb	%al, %dl
	jne	.L90
	movl	$10, %esi
	movq	%r15, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L102
	movb	$0, (%rax)
.L102:
	movq	%r15, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L330
	movq	120(%rsp), %rdi
	call	free@PLT
	movq	136(%rsp), %rax
	movq	%r15, 120(%rsp)
	movq	$0, 128(%rsp)
	cmpq	$-1, %rax
	je	.L103
	testq	%rax, %rax
	je	.L339
	movq	144(%rsp), %rax
	movq	$1, 128(%rsp)
	movq	%r15, (%rax)
.L103:
	movzbl	(%r15), %eax
	movl	%r14d, %edx
	testb	%al, %al
	jne	.L105
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L341:
	cmpb	$9, %al
	je	.L187
	testb	%dl, %dl
	je	.L340
.L108:
	addq	$1, %r15
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L90
.L105:
	cmpb	$32, %al
	jne	.L341
.L187:
	movb	$0, (%r15)
	xorl	%edx, %edx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L186:
	cmpb	$0, 35(%rsp)
	jne	.L90
	leaq	6(%r15), %rdi
	.p2align 4,,10
	.p2align 3
.L268:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	cmpb	$32, %al
	je	.L268
	cmpb	$9, %al
	je	.L268
	testb	%al, %al
	je	.L90
	cmpb	$10, %al
	je	.L90
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L330
	movq	120(%rsp), %rdi
	call	free@PLT
	movq	136(%rsp), %rax
	movq	%r14, 120(%rsp)
	movq	$0, 128(%rsp)
	cmpq	$-1, %rax
	je	.L98
	testq	%rax, %rax
	je	.L342
	movq	144(%rsp), %rax
	movq	$1, 128(%rsp)
	movq	%r14, (%rax)
.L98:
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	__GI_strpbrk
	testq	%rax, %rax
	je	.L90
	movb	$0, (%rax)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L188:
	leaq	10(%r15), %r14
	.p2align 4,,10
	.p2align 3
.L270:
	addq	$1, %r14
	movzbl	(%r14), %eax
	cmpb	$32, %al
	je	.L270
	cmpb	$9, %al
	je	.L270
	leaq	.LC7(%rip), %rsi
	movq	%r14, %rdi
	call	__GI_strpbrk
	testq	%rax, %rax
	je	.L113
	movb	$0, (%rax)
.L113:
	movzbl	(%r14), %eax
	testb	%al, %al
	je	.L114
	cmpb	$10, %al
	jne	.L343
.L114:
	movl	$37, %esi
	movq	%r14, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L117
	movb	$0, (%rax)
.L117:
	cmpb	$0, (%r14)
	je	.L90
	leaq	304(%rsp), %rax
	movq	%r14, %rsi
	movl	$10, %edi
	movq	%rax, %rdx
	movq	%rax, 8(%rsp)
	call	__GI___inet_pton
	testl	%eax, %eax
	jle	.L90
	movl	$28, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L330
	movdqa	304(%rsp), %xmm0
	testq	%r15, %r15
	movq	$889192458, (%rax)
	movl	$0, 24(%rax)
	movups	%xmm0, 8(%rax)
	je	.L116
	leaq	24(%rax), %rdx
	leaq	1(%r15), %rsi
	movq	8(%rsp), %rdi
	call	__GI___inet6_scopeid_pton
.L116:
	leaq	8(%rbx), %rdi
	call	nameserver_list_emplace
	testq	%rax, %rax
	je	.L344
	movq	%r14, (%rax)
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L331:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$40, %eax
	ja	.L170
	movabsq	$1099514781702, %rdx
	btq	%rax, %rdx
	jc	.L72
.L170:
	xorl	%r13d, %r13d
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L333:
	movq	136(%rsp), %rax
	movl	$1, %r13d
	cmpq	$-1, %rax
	je	.L85
	movq	128(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L345
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	movq	144(%rsp), %rax
	movq	%r12, (%rax,%rdx,8)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L74:
	movabsq	$21474837185, %rax
	movl	$2, 288(%rsp)
	movq	%rax, 280(%rsp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	304(%rsp), %rax
	addq	$8, %r15
	movq	%r15, %rcx
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L125:
	cmpb	$32, %r14b
	je	.L126
.L348:
	cmpb	$9, %r14b
	je	.L126
	cmpb	$59, %r14b
	jbe	.L346
.L127:
	testb	%r14b, %r14b
	je	.L178
	cmpb	$47, %r14b
	je	.L176
	cmpb	$38, %r14b
	je	.L176
	cmpb	$59, %r14b
	je	.L178
	testb	%r14b, %r14b
	js	.L178
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%rcx, %r15
	movq	%fs:(%rax), %rdx
	movsbq	%r14b, %rax
	testb	$32, 1(%rdx,%rax,2)
	je	.L131
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L347:
	cmpb	$47, %r14b
	je	.L130
	cmpb	$38, %r14b
	je	.L130
	cmpb	$59, %r14b
	je	.L129
	testb	%r14b, %r14b
	js	.L129
	movsbq	%r14b, %rax
	testb	$32, 1(%rdx,%rax,2)
	jne	.L129
.L131:
	addq	$1, %r15
	movzbl	(%r15), %r14d
	testb	%r14b, %r14b
	jne	.L347
.L129:
	movq	8(%rsp), %rsi
	movb	$0, (%r15)
	movq	%rcx, %rdi
	call	__GI___inet_aton_exact
	testl	%eax, %eax
	jne	.L165
.L329:
	movq	%r15, %rcx
.L138:
	cmpb	$32, %r14b
	movb	%r14b, (%rcx)
	jne	.L348
.L126:
	movzbl	1(%rcx), %r14d
	addq	$1, %rcx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L346:
	movabsq	$576460752303424513, %rax
	btq	%r14, %rax
	jnc	.L127
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L165:
	movl	304(%rsp), %r8d
	movl	$255, %r9d
	movl	%r8d, %eax
	bswap	%eax
	testl	%eax, %eax
	jns	.L137
	andl	$-1073741824, %eax
	movl	$16777215, %r9d
	cmpl	$-2147483648, %eax
	movl	$65535, %eax
	cmove	%eax, %r9d
.L137:
	movq	%r15, %rcx
.L135:
	movq	208(%rsp), %rax
	cmpq	$-1, %rax
	je	.L138
	movq	200(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L349
	leaq	1(%rdx), %rax
	movq	%rax, 200(%rsp)
	movq	216(%rsp), %rax
	leaq	(%rax,%rdx,8), %rax
	movl	%r8d, (%rax)
	movl	%r9d, 4(%rax)
	jmp	.L138
.L176:
	movq	%rcx, %r15
	.p2align 4,,10
	.p2align 3
.L130:
	movq	8(%rsp), %rsi
	movb	$0, (%r15)
	movq	%rcx, %rdi
	call	__GI___inet_aton_exact
	testl	%eax, %eax
	je	.L329
	movl	304(%rsp), %r8d
	movb	%r14b, (%r15)
	leaq	1(%r15), %rdi
	movzbl	1(%r15), %r14d
	testb	%r14b, %r14b
	jle	.L191
	cmpb	$59, %r14b
	je	.L191
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%rdi, %rcx
	movq	%fs:(%rax), %rdx
	movsbq	%r14b, %rax
	testb	$32, 1(%rdx,%rax,2)
	je	.L133
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L350:
	cmpb	$59, %r14b
	je	.L132
	movsbq	%r14b, %rax
	testb	$32, 1(%rdx,%rax,2)
	jne	.L132
.L133:
	addq	$1, %rcx
	movzbl	(%rcx), %r14d
	testb	%r14b, %r14b
	jg	.L350
.L132:
	movq	8(%rsp), %rsi
	movb	$0, (%rcx)
	movl	%r8d, 36(%rsp)
	movq	%rcx, 24(%rsp)
	call	__GI___inet_aton_exact
	testl	%eax, %eax
	movq	24(%rsp), %rcx
	movl	36(%rsp), %r8d
	je	.L134
	movl	304(%rsp), %r9d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L343:
	leaq	52(%rsp), %rsi
	movq	%r14, %rdi
	call	__GI___inet_aton_exact
	testl	%eax, %eax
	je	.L114
	movl	$16, %edi
	movl	52(%rsp), %r15d
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L330
	movl	%r15d, 4(%rax)
	movl	$889192450, (%rax)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L154:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r13d, %r13d
	movl	$12, %fs:(%rax)
	jmp	.L122
.L156:
	movq	16(%rsp), %rdi
	movq	%rbp, %rsi
	call	__GI___file_change_detection_for_fp
	testb	%al, %al
	jne	.L157
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L338:
	testb	$32, 0(%rbp)
	je	.L89
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L81:
	movb	$0, (%r12)
	movb	$1, 35(%rsp)
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rcx, %r15
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L134:
	movl	%r8d, %eax
	movl	$255, %r9d
	bswap	%eax
	testl	%eax, %eax
	jns	.L135
	andl	$-1073741824, %eax
	movl	$16777215, %r9d
	cmpl	$-2147483648, %eax
	movl	$65535, %eax
	cmove	%eax, %r9d
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L340:
	movq	136(%rsp), %rax
	movl	%r14d, %edx
	cmpq	$-1, %rax
	je	.L108
	movq	128(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L352
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	movq	144(%rsp), %rax
	movq	%r15, (%rax,%rdx,8)
	movl	%r14d, %edx
	jmp	.L108
.L332:
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	call	search_list_add__
	jmp	.L78
.L191:
	movq	%rdi, %rcx
	jmp	.L132
.L349:
	leaq	136(%rbx), %rdi
	xorl	%esi, %esi
	movl	$8, %edx
	movq	%rcx, 40(%rsp)
	movl	%r9d, 36(%rsp)
	movl	%r8d, 24(%rsp)
	call	__GI___libc_dynarray_emplace_enlarge
	testb	%al, %al
	movl	24(%rsp), %r8d
	movl	36(%rsp), %r9d
	movq	40(%rsp), %rcx
	je	.L353
	movq	200(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 200(%rsp)
	movq	216(%rsp), %rdx
	leaq	(%rdx,%rax,8), %rax
	movl	%r8d, (%rax)
	movl	%r9d, 4(%rax)
	jmp	.L138
.L336:
	leaq	8(%rbx), %rdi
	call	nameserver_list_emplace
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L330
	movl	$1, %esi
	movl	$127, %edi
	call	__GI___inet_makeaddr
	movl	$16, %edi
	movl	%eax, %r13d
	call	malloc@PLT
	testq	%rax, %rax
	je	.L354
	movl	%r13d, 4(%rax)
	movl	$889192450, (%rax)
	movq	%rax, (%r12)
	jmp	.L143
.L345:
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	call	search_list_add__
	jmp	.L85
.L342:
	leaq	64(%rbx), %rdi
	movq	%r14, %rsi
	call	search_list_add__
	jmp	.L98
.L339:
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	call	search_list_add__
	jmp	.L103
.L352:
	leaq	64(%rbx), %rdi
	movq	%r15, %rsi
	call	search_list_add__
	movl	%r14d, %edx
	jmp	.L108
.L354:
	movq	$0, (%r12)
	jmp	.L330
.L353:
	movq	216(%rsp), %rdi
	movq	%rcx, 24(%rsp)
	call	free@PLT
	movq	$0, 216(%rsp)
	movq	$0, 200(%rsp)
	movq	$-1, 208(%rsp)
	movq	24(%rsp), %rcx
	jmp	.L138
.L337:
	leaq	64(%rbx), %rdi
	movq	%r12, %rsi
	call	search_list_add__
	jmp	.L146
	.size	__resolv_conf_load, .-__resolv_conf_load
	.p2align 4,,15
	.globl	__res_vinit
	.hidden	__res_vinit
	.type	__res_vinit, @function
__res_vinit:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebx
	subq	$8, %rsp
	testl	%esi, %esi
	je	.L356
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L363
	cmpl	$5, %eax
	je	.L363
.L357:
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	__resolv_conf_load
	movq	%rax, %rbp
.L362:
	testq	%rbp, %rbp
	je	.L361
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	__resolv_conf_attach
	movq	%rbp, %rdi
	movl	%eax, %r13d
	call	__resolv_conf_put
	testb	%r13b, %r13b
	je	.L361
	testl	%ebx, %ebx
	je	.L369
	call	__GI___res_randomid
	xorl	%ebx, %ebx
	movw	%ax, 68(%r12)
.L369:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L363:
	testl	$-3, 4(%r12)
	jne	.L357
	movq	8(%r12), %rax
	testq	%rax, %rax
	je	.L356
	andq	$-2, %rax
	cmpq	$704, %rax
	jne	.L357
	.p2align 4,,10
	.p2align 3
.L356:
	call	__resolv_conf_get_current
	movq	%rax, %rbp
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L361:
	movl	$-1, %ebx
	jmp	.L369
	.size	__res_vinit, .-__res_vinit
	.p2align 4,,15
	.globl	__GI___res_ninit
	.hidden	__GI___res_ninit
	.type	__GI___res_ninit, @function
__GI___res_ninit:
	xorl	%esi, %esi
	jmp	__res_vinit
	.size	__GI___res_ninit, .-__GI___res_ninit
	.globl	__res_ninit
	.set	__res_ninit,__GI___res_ninit
	.section	.rodata
	.align 32
	.type	options.13954, @object
	.size	options.13954, 288
options.13954:
	.string	"rotate"
	.zero	15
	.byte	6
	.byte	0
	.quad	16384
	.string	"edns0"
	.zero	16
	.byte	5
	.byte	0
	.quad	1048576
	.string	"single-request-reopen"
	.byte	21
	.byte	0
	.quad	4194304
	.string	"single-request"
	.zero	7
	.byte	14
	.byte	0
	.quad	2097152
	.string	"no_tld_query"
	.zero	9
	.byte	12
	.byte	0
	.quad	16777216
	.string	"no-tld-query"
	.zero	9
	.byte	12
	.byte	0
	.quad	16777216
	.string	"no-reload"
	.zero	12
	.byte	9
	.byte	0
	.quad	33554432
	.string	"use-vc"
	.zero	15
	.byte	6
	.byte	0
	.quad	8
	.string	"trust-ad"
	.zero	13
	.byte	8
	.byte	0
	.quad	67108864
	.hidden	__resolv_conf_get_current
	.hidden	__resolv_conf_put
	.hidden	__resolv_conf_attach
	.hidden	__getline
	.hidden	__resolv_conf_allocate
	.hidden	__gethostname
	.hidden	_res_hconf_init
