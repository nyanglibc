	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%s: line %d: cannot specify more than %d trim domains"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%s"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"%s: line %d: list delimiter not followed by domain"
#NO_APP
	.text
	.p2align 4,,15
	.type	arg_trimdomain_list, @function
arg_trimdomain_list:
.LFB86:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rdi
	movl	%esi, %r13d
	movabsq	$864708720641179648, %rbp
	subq	$24, %rsp
	movzbl	(%rdx), %edx
	movq	_res_hconf@GOTPCREL(%rip), %r14
	.p2align 4,,10
	.p2align 3
.L16:
	testb	%dl, %dl
	je	.L18
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	%dl, %rcx
	movq	%rdi, %rbx
	movq	%fs:(%rax), %rax
	testb	$32, 1(%rax,%rcx,2)
	jne	.L19
	cmpb	$35, %dl
	setne	%cl
	cmpb	$44, %dl
	setne	%dl
	testb	%dl, %cl
	jne	.L4
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L54:
	cmpb	$35, %cl
	setne	%sil
	cmpb	$44, %cl
	setne	%cl
	testb	%cl, %sil
	je	.L22
.L4:
	addq	$1, %rbx
	movzbl	(%rbx), %ecx
	testb	%cl, %cl
	je	.L22
	movsbq	%cl, %rsi
	testb	$32, 1(%rax,%rsi,2)
	je	.L54
.L22:
	movq	%rbx, %rsi
	subq	%rdi, %rsi
.L2:
	movslq	24(%r14), %r15
	cmpl	$3, %r15d
	jg	.L55
.L6:
	leal	1(%r15), %eax
	movl	%eax, 24(%r14)
	call	__GI___strndup
	movq	%rax, 32(%r14,%r15,8)
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rsi
	movsbq	(%rbx), %rax
	testb	$32, 1(%rsi,%rax,2)
	movq	%rax, %rcx
	je	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$1, %rbx
	movsbq	(%rbx), %rax
	testb	$32, 1(%rsi,%rax,2)
	movq	%rax, %rcx
	jne	.L10
.L9:
	cmpb	$59, %cl
	movq	%rbx, %rdi
	ja	.L11
	btq	%rcx, %rbp
	jc	.L56
.L11:
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.L24
	cmpb	$35, %dl
	jne	.L16
.L24:
	addq	$24, %rsp
	movq	%rdi, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movsbq	1(%rbx), %rdx
	addq	$1, %rdi
	testb	$32, 1(%rsi,%rdx,2)
	movq	%rdx, %rax
	je	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %rdi
	movsbq	(%rdi), %rdx
	testb	$32, 1(%rsi,%rdx,2)
	movq	%rdx, %rax
	jne	.L13
.L12:
	testb	%al, %al
	je	.L23
	cmpb	$35, %al
	jne	.L11
.L23:
	leaq	.LC2(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movl	%r13d, %ecx
	xorl	%eax, %eax
	movq	%r12, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L52
.L15:
	xorl	%eax, %eax
.L57:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movslq	24(%r14), %r15
	xorl	%esi, %esi
	cmpl	$3, %r15d
	jle	.L6
.L55:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movl	$4, %r8d
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movq	%r12, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L15
.L52:
	movq	8(%rsp), %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	8(%rsp), %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L57
.L18:
	movq	%rdi, %rbx
	xorl	%esi, %esi
	jmp	.L2
.LFE86:
	.size	arg_trimdomain_list, .-arg_trimdomain_list
	.section	.rodata.str1.1
.LC3:
	.string	"on"
.LC4:
	.string	"off"
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"%s: line %d: expected `on' or `off', found `%s'\n"
	.text
	.p2align 4,,15
	.type	arg_bool, @function
arg_bool:
.LFB87:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	leaq	.LC3(%rip), %rsi
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movl	$2, %edx
	subq	$24, %rsp
	movq	%rbx, %rdi
	movl	%ecx, %ebp
	call	__strncasecmp@PLT
	testl	%eax, %eax
	je	.L64
	leaq	.LC4(%rip), %rsi
	movl	$3, %edx
	movq	%rbx, %rdi
	call	__strncasecmp@PLT
	testl	%eax, %eax
	jne	.L61
	movq	_res_hconf@GOTPCREL(%rip), %rdx
	notl	%ebp
	leaq	3(%rbx), %rax
	andl	%ebp, 64(%rdx)
.L58:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movq	_res_hconf@GOTPCREL(%rip), %rdx
	leaq	2(%rbx), %rax
	orl	%ebp, 64(%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	.LC5(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	movq	%rbx, %r8
	xorl	%eax, %eax
	movl	%r13d, %ecx
	movq	%r12, %rdx
	call	__GI___asprintf
	testl	%eax, %eax
	jns	.L65
	xorl	%eax, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L65:
	movq	8(%rsp), %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	8(%rsp), %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L58
.LFE87:
	.size	arg_bool, .-arg_bool
	.section	.rodata.str1.1
.LC6:
	.string	"/etc/host.conf"
.LC7:
	.string	"RESOLV_HOST_CONF"
.LC8:
	.string	"rce"
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"%s: line %d: ignoring trailing garbage `%s'\n"
	.section	.rodata.str1.1
.LC10:
	.string	"RESOLV_MULTI"
.LC11:
	.string	"RESOLV_REORDER"
.LC12:
	.string	"RESOLV_ADD_TRIM_DOMAINS"
.LC13:
	.string	"RESOLV_OVERRIDE_TRIM_DOMAINS"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"%s: line %d: bad command `%s'\n"
	.text
	.p2align 4,,15
	.type	do_init, @function
do_init:
.LFB89:
	pushq	%r15
	pushq	%r14
	leaq	.LC7(%rip), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	pxor	%xmm0, %xmm0
	subq	$312, %rsp
	movq	_res_hconf@GOTPCREL(%rip), %rax
	movups	%xmm0, (%rax)
	movq	$0, 64(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 48(%rax)
	call	__GI_getenv
	testq	%rax, %rax
	movq	%rax, %rsi
	leaq	.LC6(%rip), %rax
	cmovne	%rsi, %rax
	leaq	.LC8(%rip), %rsi
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L68
	orl	$32768, (%rax)
	leaq	40(%rsp), %rax
	leaq	48(%rsp), %rbx
	movl	$0, 4(%rsp)
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L69:
	movq	8(%rsp), %rdx
	movl	$256, %esi
	movq	%rbx, %rdi
	call	__GI___fgets_unlocked
	testq	%rax, %rax
	je	.L158
	movl	$10, %esi
	movq	%rbx, %rdi
	addl	$1, 4(%rsp)
	call	__strchrnul@PLT
	movb	$0, (%rax)
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%rbx, %rbp
	movsbq	48(%rsp), %rdx
	movq	%fs:(%rax), %r13
	movq	%rdx, %rax
	testb	$32, 1(%r13,%rdx,2)
	je	.L70
	.p2align 4,,10
	.p2align 3
.L71:
	addq	$1, %rbp
	movsbq	0(%rbp), %rdx
	testb	$32, 1(%r13,%rdx,2)
	movq	%rdx, %rax
	jne	.L71
.L70:
	testb	%al, %al
	je	.L69
	cmpb	$35, %al
	je	.L69
	movzbl	0(%rbp), %eax
	testb	%al, %al
	je	.L102
	movsbq	%al, %rdx
	movq	%rbp, %r12
	testb	$32, 1(%r13,%rdx,2)
	jne	.L103
	cmpb	$44, %al
	setne	%dl
	cmpb	$35, %al
	setne	%al
	testb	%al, %dl
	jne	.L77
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L159:
	cmpb	$35, %al
	setne	%dl
	cmpb	$44, %al
	setne	%al
	testb	%al, %dl
	je	.L104
.L77:
	addq	$1, %r12
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L104
	movsbq	%al, %rdx
	testb	$32, 1(%r13,%rdx,2)
	je	.L159
.L104:
	movq	%r12, %r15
	subq	%rbp, %r15
.L75:
	leaq	cmd(%rip), %r14
.L84:
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	__strncasecmp@PLT
	testl	%eax, %eax
	jne	.L79
	movq	%r14, %rdx
.L80:
	movl	(%rdx), %ecx
	addq	$4, %rdx
	leal	-16843009(%rcx), %eax
	notl	%ecx
	andl	%ecx, %eax
	andl	$-2139062144, %eax
	je	.L80
	movl	%eax, %ecx
	shrl	$16, %ecx
	testl	$32896, %eax
	cmove	%ecx, %eax
	leaq	2(%rdx), %rcx
	movl	%eax, %esi
	cmove	%rcx, %rdx
	addb	%al, %sil
	sbbq	$3, %rdx
	subq	%r14, %rdx
	cmpq	%r15, %rdx
	je	.L155
.L79:
	leaq	64+cmd(%rip), %rax
	addq	$16, %r14
	cmpq	%r14, %rax
	jne	.L84
	leaq	.LC14(%rip), %rsi
	movl	$5, %edx
.L157:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	call	__GI___dcgettext
	movl	4(%rsp), %ecx
	movq	16(%rsp), %rdx
	movq	%rax, %rsi
	movq	24(%rsp), %rdi
	xorl	%eax, %eax
	movq	%rbp, %r8
	call	__GI___asprintf
	testl	%eax, %eax
	js	.L69
	movq	40(%rsp), %rdx
	leaq	.LC1(%rip), %rsi
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	40(%rsp), %rdi
	call	free@PLT
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$1, %r12
.L155:
	movsbq	(%r12), %rax
	testb	$32, 1(%r13,%rax,2)
	jne	.L82
	movzbl	11(%r14), %eax
	cmpb	$1, %al
	je	.L160
	cmpb	$2, %al
	jne	.L69
	movl	12(%r14), %ecx
	movl	4(%rsp), %esi
	movq	%r12, %rdx
	movq	16(%rsp), %rdi
	call	arg_bool
	movq	%rax, %rbp
.L87:
	testq	%rbp, %rbp
	je	.L69
	movzbl	0(%rbp), %eax
	testb	%al, %al
	je	.L69
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdi
	movsbq	%al, %rdx
	movq	%fs:(%rdi), %rcx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L89
	.p2align 4,,10
	.p2align 3
.L90:
	addq	$1, %rbp
	movzbl	0(%rbp), %eax
	testb	%al, %al
	je	.L69
	movsbq	%al, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L90
.L89:
	cmpb	$35, %al
	je	.L69
	movl	$5, %edx
	leaq	.LC9(%rip), %rsi
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L158:
	movq	8(%rsp), %rdi
	call	_IO_new_fclose@PLT
.L68:
	leaq	.LC10(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	je	.L94
	leaq	.LC10(%rip), %rdi
	movl	$16, %ecx
	movq	%rax, %rdx
	movl	$1, %esi
	call	arg_bool
.L94:
	leaq	.LC11(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	je	.L95
	leaq	.LC11(%rip), %rdi
	movl	$8, %ecx
	movq	%rax, %rdx
	movl	$1, %esi
	call	arg_bool
.L95:
	leaq	.LC12(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	je	.L96
	leaq	.LC12(%rip), %rdi
	movq	%rax, %rdx
	movl	$1, %esi
	call	arg_trimdomain_list
.L96:
	leaq	.LC13(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	je	.L97
	movq	_res_hconf@GOTPCREL(%rip), %rdi
	movq	%rax, %rdx
	movl	$1, %esi
	movl	$0, 24(%rdi)
	leaq	.LC13(%rip), %rdi
	call	arg_trimdomain_list
.L97:
	movq	_res_hconf@GOTPCREL(%rip), %rax
	movl	$1, (%rax)
	addq	$312, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	movl	4(%rsp), %esi
	movq	16(%rsp), %rdi
	movq	%r12, %rdx
	call	arg_trimdomain_list
	movq	%rax, %rbp
	jmp	.L87
.L103:
	xorl	%r15d, %r15d
	jmp	.L75
.L102:
	movq	%rbp, %r12
	xorl	%r15d, %r15d
	jmp	.L75
.LFE89:
	.size	do_init, .-do_init
	.p2align 4,,15
	.globl	_res_hconf_init
	.hidden	_res_hconf_init
	.type	_res_hconf_init, @function
_res_hconf_init:
.LFB90:
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L162
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	do_init(%rip), %rsi
	leaq	once.14232(%rip), %rdi
#APP
# 327 "res_hconf.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L162:
	movl	once.14232(%rip), %eax
	testl	%eax, %eax
	je	.L168
	rep ret
	.p2align 4,,10
	.p2align 3
.L168:
	subq	$8, %rsp
	call	do_init
	orl	$2, once.14232(%rip)
	addq	$8, %rsp
	ret
.LFE90:
	.size	_res_hconf_init, .-_res_hconf_init
	.section	.rodata.str1.1
.LC15:
	.string	"res_hconf.c"
.LC16:
	.string	"ifaddrs != NULL"
	.text
	.p2align 4,,15
	.globl	_res_hconf_reorder_addrs
	.type	_res_hconf_reorder_addrs, @function
_res_hconf_reorder_addrs:
.LFB91:
	movq	_res_hconf@GOTPCREL(%rip), %rax
	testb	$8, 64(%rax)
	je	.L200
	cmpl	$2, 16(%rdi)
	je	.L203
.L200:
	rep ret
	.p2align 4,,10
	.p2align 3
.L203:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$40, %rsp
	movl	num_ifs.14249(%rip), %ebp
	testl	%ebp, %ebp
	jle	.L204
.L173:
	movq	24(%rbx), %r9
	movq	(%r9), %r8
	testq	%r8, %r8
	je	.L169
	movq	ifaddrs(%rip), %rax
	movq	%r8, %rdi
	movq	%r9, %r10
	leaq	4(%rax), %r11
.L188:
	movl	(%rdi), %esi
	movq	%r11, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L187:
	movl	(%rax), %ecx
	xorl	%esi, %ecx
	testl	%ecx, 4(%rax)
	je	.L205
	addl	$1, %edx
	addq	$12, %rax
	cmpl	%edx, %ebp
	jg	.L187
	addq	$8, %r10
	movq	(%r10), %rdi
	testq	%rdi, %rdi
	jne	.L188
.L169:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	movq	%r8, (%r10)
	movq	%rdi, (%r9)
	jmp	.L169
	.p2align 4,,10
	.p2align 3
.L204:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%edx, %edx
	movl	$524290, %esi
	movl	$2, %edi
	movl	%fs:(%rax), %r13d
	call	__GI___socket
	testl	%eax, %eax
	movl	%eax, %r14d
	js	.L169
#APP
# 396 "res_hconf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L175
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock.14251(%rip)
# 0 "" 2
#NO_APP
.L176:
	movl	num_ifs.14249(%rip), %ebp
	testl	%ebp, %ebp
	jle	.L206
.L177:
#APP
# 469 "res_hconf.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L184
	subl	$1, lock.14251(%rip)
.L185:
	movl	%r14d, %edi
	call	__GI___close
	testl	%ebp, %ebp
	je	.L169
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L206:
	leaq	20(%rsp), %rsi
	leaq	24(%rsp), %rdi
	movl	%r14d, %edx
	call	__ifreq
	movq	24(%rsp), %r15
	testq	%r15, %r15
	je	.L190
	movslq	20(%rsp), %rax
	leaq	(%rax,%rax,2), %rdi
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, ifaddrs(%rip)
	je	.L191
	movl	20(%rsp), %edx
	testl	%edx, %edx
	jle	.L192
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L181:
	addq	$40, %r15
	addl	$1, %r8d
	cmpl	%r8d, 20(%rsp)
	jle	.L180
.L183:
	movslq	%ebp, %rdx
	leaq	(%rdx,%rdx,2), %rcx
	salq	$2, %rcx
	cmpw	$2, 16(%r15)
	movq	%rcx, %r12
	jne	.L181
	movl	20(%r15), %edx
	addq	%rcx, %rax
	movl	$35099, %esi
	movl	$2, (%rax)
	movl	%r14d, %edi
	movq	%rcx, 8(%rsp)
	movl	%r8d, 4(%rsp)
	movl	%edx, 4(%rax)
	xorl	%eax, %eax
	movq	%r15, %rdx
	call	__GI___ioctl
	testl	%eax, %eax
	movl	4(%rsp), %r8d
	movq	8(%rsp), %rcx
	movq	ifaddrs(%rip), %rax
	js	.L181
	movl	20(%r15), %edx
	addl	$1, %ebp
	addq	$12, %r12
	movl	%edx, 8(%rax,%rcx)
	jmp	.L181
.L192:
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
.L180:
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, ifaddrs(%rip)
	je	.L207
.L179:
	movq	24(%rsp), %rdi
	call	free@PLT
	movl	%ebp, %eax
.L178:
	movq	__libc_errno@gottpoff(%rip), %rsi
	movl	%r13d, %fs:(%rsi)
	movl	%eax, num_ifs.14249(%rip)
	jmp	.L177
.L175:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock.14251(%rip)
	je	.L176
	leaq	lock.14251(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L176
.L184:
	xorl	%eax, %eax
#APP
# 469 "res_hconf.c" 1
	xchgl %eax, lock.14251(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L185
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock.14251(%rip), %rdi
	movl	$202, %eax
#APP
# 469 "res_hconf.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L185
.L190:
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.L178
.L191:
	xorl	%ebp, %ebp
	jmp	.L179
.L207:
	leaq	__PRETTY_FUNCTION__.14275(%rip), %rcx
	leaq	.LC15(%rip), %rsi
	leaq	.LC16(%rip), %rdi
	movl	$450, %edx
	call	__GI___assert_fail
.LFE91:
	.size	_res_hconf_reorder_addrs, .-_res_hconf_reorder_addrs
	.p2align 4,,15
	.globl	_res_hconf_trim_domain
	.type	_res_hconf_trim_domain, @function
_res_hconf_trim_domain:
.LFB92:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	%rdi, 8(%rsp)
	call	__GI_strlen
	movq	_res_hconf@GOTPCREL(%rip), %r13
	movl	24(%r13), %r14d
	testl	%r14d, %r14d
	jle	.L208
	movq	%rax, %r12
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L211:
	movq	32(%r13,%rbx,8), %r15
	movq	%r15, %rdi
	call	__GI_strlen
	cmpq	%rax, %r12
	jbe	.L210
	movq	%r12, %rbp
	movq	%r15, %rsi
	subq	%rax, %rbp
	addq	8(%rsp), %rbp
	movq	%rbp, %rdi
	call	__GI___strcasecmp
	testl	%eax, %eax
	je	.L214
.L210:
	addq	$1, %rbx
	cmpl	%ebx, %r14d
	jg	.L211
.L208:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L214:
	movb	$0, 0(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.LFE92:
	.size	_res_hconf_trim_domain, .-_res_hconf_trim_domain
	.p2align 4,,15
	.globl	_res_hconf_trim_domains
	.type	_res_hconf_trim_domains, @function
_res_hconf_trim_domains:
.LFB93:
	movq	_res_hconf@GOTPCREL(%rip), %rax
	movl	24(%rax), %eax
	testl	%eax, %eax
	jne	.L226
	rep ret
	.p2align 4,,10
	.p2align 3
.L226:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	_res_hconf_trim_domain@PLT
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L215
	movl	$8, %ebx
	.p2align 4,,10
	.p2align 3
.L217:
	call	_res_hconf_trim_domain@PLT
	movq	8(%rbp), %rax
	movq	(%rax,%rbx), %rdi
	addq	$8, %rbx
	testq	%rdi, %rdi
	jne	.L217
.L215:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.LFE93:
	.size	_res_hconf_trim_domains, .-_res_hconf_trim_domains
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.14275, @object
	.size	__PRETTY_FUNCTION__.14275, 25
__PRETTY_FUNCTION__.14275:
	.string	"_res_hconf_reorder_addrs"
	.local	lock.14251
	.comm	lock.14251,4,4
	.data
	.align 4
	.type	num_ifs.14249, @object
	.size	num_ifs.14249, 4
num_ifs.14249:
	.long	-1
	.local	once.14232
	.comm	once.14232,4,4
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	ifaddrs, @object
	.size	ifaddrs, 8
ifaddrs:
	.zero	8
	.comm	_res_hconf,72,32
	.section	.rodata
	.align 32
	.type	cmd, @object
	.size	cmd, 64
cmd:
	.string	"order"
	.zero	5
	.byte	0
	.long	0
	.string	"trim"
	.zero	6
	.byte	1
	.long	0
	.string	"multi"
	.zero	5
	.byte	2
	.long	16
	.string	"reorder"
	.zero	3
	.byte	2
	.long	8
	.hidden	__lll_lock_wait_private
	.hidden	__ifreq
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__fxprintf
