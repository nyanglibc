	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___res_randomid
	.hidden	__GI___res_randomid
	.type	__GI___res_randomid, @function
__GI___res_randomid:
	subq	$8, %rsp
	call	__GI___getpid
	addq	$8, %rsp
	movzwl	%ax, %eax
	ret
	.size	__GI___res_randomid, .-__GI___res_randomid
	.globl	__res_randomid
	.set	__res_randomid,__GI___res_randomid
