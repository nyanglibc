	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"resolv_context.c"
.LC1:
	.string	"current->__from_res"
.LC2:
	.string	"current->__refcount > 0"
	.text
	.p2align 4,,15
	.type	context_reuse, @function
context_reuse:
	subq	$8, %rsp
	movq	current@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	cmpb	$0, 24(%rax)
	je	.L6
	addq	$1, 16(%rax)
	je	.L7
	addq	$8, %rsp
	ret
.L6:
	leaq	__PRETTY_FUNCTION__.6949(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$162, %edx
	call	__assert_fail
.L7:
	leaq	__PRETTY_FUNCTION__.6949(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$168, %edx
	call	__assert_fail
	.size	context_reuse, .-context_reuse
	.p2align 4,,15
	.type	context_alloc, @function
context_alloc:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$40, %edi
	subq	$8, %rsp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L8
	movq	%rbp, (%rax)
	movq	%rbp, %rdi
	call	__resolv_conf_get
	movq	%rax, 8(%rbx)
	movq	current@gottpoff(%rip), %rax
	movq	$1, 16(%rbx)
	movb	$1, 24(%rbx)
	movq	%fs:(%rax), %rdx
	movq	%rbx, %fs:(%rax)
	movq	%rdx, 32(%rbx)
.L8:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	context_alloc, .-context_alloc
	.p2align 4,,15
	.type	context_free, @function
context_free:
	pushq	%r12
	pushq	%rbp
	movq	__libc_errno@gottpoff(%rip), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rdi), %rdx
	movq	current@gottpoff(%rip), %rax
	movq	8(%rdi), %rdi
	movl	%fs:0(%rbp), %r12d
	movq	%rdx, %fs:(%rax)
	call	__resolv_conf_put
	movq	%rbx, %rdi
	call	free@PLT
	movl	%r12d, %fs:0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	context_free, .-context_free
	.section	.rodata.str1.1
.LC3:
	.string	"ctx->conf == NULL"
	.text
	.p2align 4,,15
	.type	context_get.part.1, @function
context_get.part.1:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$24, %rsp
	movq	__libc_resp@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdi
	call	context_alloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L16
	movq	(%rax), %rdi
	movq	8(%rdi), %rax
	testb	$1, %al
	jne	.L44
	cmpq	$0, 8(%rbx)
	jne	.L45
	testb	%bpl, %bpl
	je	.L24
	movl	(%rdi), %edx
	testl	%edx, %edx
	jne	.L25
	movl	$5, (%rdi)
.L25:
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jne	.L26
	movl	$2, 4(%rdi)
.L26:
	cmpw	$0, 68(%rdi)
	movq	$704, 8(%rdi)
	je	.L46
.L24:
	movzbl	%bpl, %esi
	call	__res_vinit
	testl	%eax, %eax
	js	.L28
	movq	(%rbx), %rdi
	call	__resolv_conf_get
	movq	%rax, 8(%rbx)
.L16:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	testl	$33554432, %eax
	jne	.L16
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L16
	movl	56(%rdx), %ecx
	cmpq	%rcx, %rax
	jne	.L16
	movl	60(%rdx), %eax
	cmpl	%eax, (%rdi)
	jne	.L16
	movl	64(%rdx), %eax
	cmpl	%eax, 4(%rdi)
	jne	.L16
	movzbl	392(%rdi), %eax
	andl	$15, %eax
	cmpl	68(%rdx), %eax
	jne	.L16
	movq	%rdi, 8(%rsp)
	call	__resolv_conf_get_current
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	8(%rsp), %rdi
	je	.L28
	cmpq	8(%rbx), %rbp
	je	.L20
	movl	16(%rdi), %ecx
	testl	%ecx, %ecx
	jle	.L21
	movl	$1, %esi
	call	__res_iclose
.L21:
	movq	(%rbx), %rdi
	movq	%rbp, %rsi
	call	__resolv_conf_attach
	testb	%al, %al
	je	.L16
	movq	8(%rbx), %rdi
	call	__resolv_conf_put
	movq	%rbp, 8(%rbx)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	context_free
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rdi, 8(%rsp)
	call	__res_randomid
	movq	8(%rsp), %rdi
	movw	%ax, 68(%rdi)
	jmp	.L24
.L20:
	movq	%rbp, %rdi
	call	__resolv_conf_put
	jmp	.L16
.L45:
	leaq	__PRETTY_FUNCTION__.6937(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$110, %edx
	call	__assert_fail
	.size	context_get.part.1, .-context_get.part.1
	.p2align 4,,15
	.globl	__resolv_context_get
	.hidden	__resolv_context_get
	.type	__resolv_context_get, @function
__resolv_context_get:
	movq	current@gottpoff(%rip), %rax
	cmpq	$0, %fs:(%rax)
	je	.L48
	jmp	context_reuse
	.p2align 4,,10
	.p2align 3
.L48:
	xorl	%edi, %edi
	jmp	context_get.part.1
	.size	__resolv_context_get, .-__resolv_context_get
	.p2align 4,,15
	.globl	__resolv_context_get_preinit
	.hidden	__resolv_context_get_preinit
	.type	__resolv_context_get_preinit, @function
__resolv_context_get_preinit:
	movq	current@gottpoff(%rip), %rax
	cmpq	$0, %fs:(%rax)
	je	.L50
	jmp	context_reuse
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$1, %edi
	jmp	context_get.part.1
	.size	__resolv_context_get_preinit, .-__resolv_context_get_preinit
	.p2align 4,,15
	.globl	__resolv_context_get_override
	.hidden	__resolv_context_get_override
	.type	__resolv_context_get_override, @function
__resolv_context_get_override:
	subq	$8, %rsp
	call	context_alloc
	testq	%rax, %rax
	je	.L51
	movb	$0, 24(%rax)
.L51:
	addq	$8, %rsp
	ret
	.size	__resolv_context_get_override, .-__resolv_context_get_override
	.section	.rodata.str1.1
.LC4:
	.string	"current == ctx"
.LC5:
	.string	"ctx->__refcount > 0"
	.text
	.p2align 4,,15
	.globl	__resolv_context_put
	.hidden	__resolv_context_put
	.type	__resolv_context_put, @function
__resolv_context_put:
	testq	%rdi, %rdi
	je	.L66
	subq	$8, %rsp
	movq	current@gottpoff(%rip), %rax
	cmpq	%rdi, %fs:(%rax)
	jne	.L69
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.L70
	cmpb	$0, 24(%rdi)
	je	.L64
	subq	$1, %rax
	testq	%rax, %rax
	movq	%rax, 16(%rdi)
	je	.L64
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	addq	$8, %rsp
	jmp	context_free
	.p2align 4,,10
	.p2align 3
.L66:
	rep ret
.L69:
	leaq	__PRETTY_FUNCTION__.6967(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$229, %edx
	call	__assert_fail
.L70:
	leaq	__PRETTY_FUNCTION__.6967(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$230, %edx
	call	__assert_fail
	.size	__resolv_context_put, .-__resolv_context_put
	.p2align 4,,15
	.globl	__resolv_context_freeres
	.hidden	__resolv_context_freeres
	.type	__resolv_context_freeres, @function
__resolv_context_freeres:
	movq	current@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdi
	movq	$0, %fs:(%rax)
	testq	%rdi, %rdi
	je	.L79
	pushq	%rbx
	.p2align 4,,10
	.p2align 3
.L73:
	movq	32(%rdi), %rbx
	call	context_free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L73
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L79:
	rep ret
	.size	__resolv_context_freeres, .-__resolv_context_freeres
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.6967, @object
	.size	__PRETTY_FUNCTION__.6967, 21
__PRETTY_FUNCTION__.6967:
	.string	"__resolv_context_put"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.6937, @object
	.size	__PRETTY_FUNCTION__.6937, 11
__PRETTY_FUNCTION__.6937:
	.string	"maybe_init"
	.align 8
	.type	__PRETTY_FUNCTION__.6949, @object
	.size	__PRETTY_FUNCTION__.6949, 14
__PRETTY_FUNCTION__.6949:
	.string	"context_reuse"
	.section	.tbss,"awT",@nobits
	.align 8
	.type	current, @object
	.size	current, 8
current:
	.zero	8
	.hidden	__res_randomid
	.hidden	__resolv_conf_attach
	.hidden	__res_iclose
	.hidden	__resolv_conf_get_current
	.hidden	__res_vinit
	.hidden	__resolv_conf_put
	.hidden	__resolv_conf_get
	.hidden	__assert_fail
