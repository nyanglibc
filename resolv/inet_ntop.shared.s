	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	inet_ntop4, @function
inet_ntop4:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movl	%edx, %ebx
	leaq	fmt.5076(%rip), %rsi
	xorl	%eax, %eax
	subq	$16, %rsp
	movzbl	1(%rdi), %ecx
	movzbl	(%rdi), %edx
	movzbl	3(%rdi), %r9d
	movzbl	2(%rdi), %r8d
	movq	%rsp, %r12
	movq	%r12, %rdi
	call	__GI_sprintf
	cltq
	cmpq	%rbx, %rax
	jnb	.L6
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	__GI_strcpy
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	inet_ntop4, .-inet_ntop4
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%x"
	.text
	.p2align 4,,15
	.globl	__GI_inet_ntop
	.hidden	__GI_inet_ntop
	.type	__GI_inet_ntop, @function
__GI_inet_ntop:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	cmpl	$2, %edi
	movq	%rdx, 8(%rsp)
	movl	%ecx, 16(%rsp)
	je	.L9
	cmpl	$10, %edi
	jne	.L67
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L12:
	movl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movzbl	0(%r13,%rax), %edx
	movzbl	1(%r13,%rax), %esi
	addq	$2, %rax
	sall	$8, %edx
	orl	%esi, %edx
	cmpq	$16, %rax
	movl	%edx, 32(%rsp,%rcx,4)
	jne	.L12
	movl	$-1, %r15d
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	movl	%r15d, %edx
	leaq	32(%rsp), %rbp
	movl	%r15d, %esi
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L69:
	addl	$1, %ecx
	cmpl	$-1, %edx
	jne	.L14
	movl	%eax, %edx
	movl	$1, %ecx
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$1, %rax
	cmpq	$8, %rax
	je	.L68
.L16:
	movl	0(%rbp,%rax,4), %edi
	testl	%edi, %edi
	je	.L69
	cmpl	$-1, %edx
	je	.L14
	cmpl	%ebx, %ecx
	jg	.L36
	cmpl	$-1, %r15d
	je	.L36
	addq	$1, %rax
	movl	%esi, %edx
	cmpq	$8, %rax
	jne	.L16
	.p2align 4,,10
	.p2align 3
.L68:
	cmpl	$-1, %edx
	je	.L17
	cmpl	%ecx, %ebx
	jl	.L37
	cmpl	$-1, %r15d
	je	.L37
.L32:
	movl	%r15d, %edx
.L18:
	cmpl	$2, %ebx
	movl	$-1, %r8d
	cmovge	%edx, %r8d
	setge	7(%rsp)
	movl	%r8d, %r15d
.L20:
	leaq	64(%rsp), %rax
	leal	(%r15,%rbx), %esi
	xorl	%r12d, %r12d
	movq	%rax, 24(%rsp)
	movl	%esi, 20(%rsp)
	.p2align 4,,10
	.p2align 3
.L27:
	cmpl	%r12d, %r15d
	movl	%r12d, %edx
	jg	.L21
	cmpb	$0, 7(%rsp)
	je	.L21
	cmpl	%r12d, 20(%rsp)
	jg	.L70
.L21:
	testq	%r12, %r12
	je	.L35
	testl	%r15d, %r15d
	leaq	1(%rax), %r14
	movb	$58, (%rax)
	jne	.L23
	cmpl	$6, %edx
	je	.L71
.L23:
	movl	0(%rbp,%r12,4), %edx
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	xorl	%eax, %eax
	call	__GI_sprintf
	cltq
	addq	%r14, %rax
.L22:
	addq	$1, %r12
	cmpq	$8, %r12
	jne	.L27
	cmpl	$-1, %r15d
	leaq	1(%rax), %rdx
	je	.L28
.L26:
	addl	%r15d, %ebx
	cmpl	$8, %ebx
	jne	.L28
	movb	$58, (%rax)
	movq	%rdx, %rax
	addq	$1, %rdx
.L28:
	subq	24(%rsp), %rdx
	movb	$0, (%rax)
	cmpl	%edx, 16(%rsp)
	jb	.L72
	movq	24(%rsp), %rsi
	movq	8(%rsp), %rdi
	call	__GI_strcpy
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$97, %fs:(%rax)
	xorl	%eax, %eax
.L7:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	%edx, %r15d
	movl	%ecx, %ebx
	movl	%esi, %edx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L9:
	movl	16(%rsp), %edx
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	call	inet_ntop4
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%ecx, %ebx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L70:
	cmpl	%r12d, %r15d
	jne	.L22
	movb	$58, (%rax)
	addq	$1, %rax
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$-1, %r15d
	jne	.L32
	movb	$0, 7(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L72:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, %r14
	jmp	.L23
.L71:
	cmpl	$6, %ebx
	je	.L24
	cmpl	$5, %ebx
	jne	.L23
	cmpl	$65535, 52(%rsp)
	jne	.L23
.L24:
	movl	24(%rsp), %eax
	leaq	12(%r13), %rdi
	movq	%r14, %rsi
	subl	%r14d, %eax
	leal	46(%rax), %edx
	call	inet_ntop4
	testq	%rax, %rax
	je	.L7
	movq	%r14, %rdi
	call	__GI_strlen
	addq	%r14, %rax
	leaq	1(%rax), %rdx
	jmp	.L26
	.size	__GI_inet_ntop, .-__GI_inet_ntop
	.globl	inet_ntop
	.set	inet_ntop,__GI_inet_ntop
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	fmt.5076, @object
	.size	fmt.5076, 12
fmt.5076:
	.string	"%u.%u.%u.%u"
