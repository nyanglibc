	.text
	.p2align 4,,15
	.globl	__res_iclose
	.hidden	__res_iclose
	.type	__res_iclose, @function
__res_iclose:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %r12d
	subq	$8, %rsp
	movl	500(%rdi), %edi
	testl	%edi, %edi
	js	.L2
	call	__close_nocancel
	andl	$-4, 504(%rbp)
	movl	$-1, 500(%rbp)
.L2:
	movl	16(%rbp), %eax
	testl	%eax, %eax
	jle	.L3
	movl	$1, %ebx
.L8:
	cmpq	$0, 528(%rbp,%rbx,8)
	je	.L5
	movl	516(%rbp,%rbx,4), %edi
	cmpl	$-1, %edi
	je	.L6
	call	__close_nocancel
	movl	$-1, 516(%rbp,%rbx,4)
.L6:
	testb	%r12b, %r12b
	jne	.L19
.L5:
	movl	%ebx, %eax
	addq	$1, %rbx
	cmpl	%eax, 16(%rbp)
	jg	.L8
.L3:
	testb	%r13b, %r13b
	jne	.L20
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movq	528(%rbp,%rbx,8), %rdi
	call	free@PLT
	movq	$0, 528(%rbp,%rbx,8)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__resolv_conf_detach
	.size	__res_iclose, .-__res_iclose
	.p2align 4,,15
	.globl	__res_thread_freeres
	.hidden	__res_thread_freeres
	.type	__res_thread_freeres, @function
__res_thread_freeres:
	pushq	%rbx
	call	__resolv_context_freeres
	movq	__libc_resp@gottpoff(%rip), %rbx
	movq	%fs:(%rbx), %rdi
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jne	.L24
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movl	$1, %esi
	call	__res_iclose
	movq	%fs:(%rbx), %rax
	movq	$0, 8(%rax)
	popq	%rbx
	ret
	.size	__res_thread_freeres, .-__res_thread_freeres
	.p2align 4,,15
	.globl	__res_nclose
	.hidden	__res_nclose
	.type	__res_nclose, @function
__res_nclose:
	movl	$1, %esi
	jmp	__res_iclose
	.size	__res_nclose, .-__res_nclose
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element___res_thread_freeres__, @object
	.size	__elf_set___libc_subfreeres_element___res_thread_freeres__, 8
__elf_set___libc_subfreeres_element___res_thread_freeres__:
	.quad	__res_thread_freeres
	.hidden	__resolv_context_freeres
	.hidden	__resolv_conf_detach
	.hidden	__close_nocancel
