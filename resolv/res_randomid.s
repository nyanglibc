	.text
	.p2align 4,,15
	.globl	__res_randomid
	.hidden	__res_randomid
	.type	__res_randomid, @function
__res_randomid:
	subq	$8, %rsp
	call	__getpid
	addq	$8, %rsp
	movzwl	%ax, %eax
	ret
	.size	__res_randomid, .-__res_randomid
	.hidden	__getpid
