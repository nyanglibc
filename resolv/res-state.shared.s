	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___res_state
	.hidden	__GI___res_state
	.type	__GI___res_state, @function
__GI___res_state:
	movq	__libc_resp@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	ret
	.size	__GI___res_state, .-__GI___res_state
	.globl	__res_state
	.set	__res_state,__GI___res_state
