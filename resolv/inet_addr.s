	.text
	.p2align 4,,15
	.type	inet_aton_end, @function
inet_aton_end:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rsi, (%rsp)
	movq	%rdx, 8(%rsp)
	movl	$0, 20(%rsp)
	movl	%fs:(%rax), %r15d
	movl	$0, %fs:(%rax)
	movsbl	(%rdi), %eax
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L6
	leaq	20(%rsp), %rbp
	movq	%rdi, %rbx
	leaq	24(%rsp), %r13
	movl	$4294967295, %r12d
	movq	%rbp, %r14
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L23:
	movq	24(%rsp), %rdx
	movsbq	(%rdx), %rsi
	cmpb	$46, %sil
	jne	.L20
	cmpq	$255, %rax
	ja	.L6
	leaq	2(%rbp), %rsi
	cmpq	%rsi, %r14
	ja	.L6
	addq	$1, %r14
	movb	%al, -1(%r14)
	movsbl	1(%rdx), %eax
	leaq	1(%rdx), %rbx
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L6
.L7:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	strtoul
	cmpq	%r12, %rax
	jbe	.L23
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r15d, %fs:(%rax)
	xorl	%eax, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	testb	%sil, %sil
	je	.L8
	js	.L6
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdi
	movq	%fs:(%rdi), %rdi
	testb	$32, 1(%rdi,%rsi,2)
	je	.L6
.L8:
	cmpq	%rbx, %rdx
	je	.L6
	movq	%r14, %rcx
	leaq	max.8115(%rip), %rsi
	subq	%rbp, %rcx
	cmpl	%eax, (%rsi,%rcx,4)
	jb	.L6
	movq	(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L9
	bswap	%eax
	orl	20(%rsp), %eax
	movl	%eax, (%rcx)
.L9:
	movq	8(%rsp), %rax
	movq	%rdx, (%rax)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r15d, %fs:(%rax)
	movl	$1, %eax
	jmp	.L1
	.size	inet_aton_end, .-inet_aton_end
	.p2align 4,,15
	.globl	__inet_aton_exact
	.hidden	__inet_aton_exact
	.type	__inet_aton_exact, @function
__inet_aton_exact:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	leaq	8(%rsp), %rdx
	leaq	4(%rsp), %rsi
	call	inet_aton_end
	testl	%eax, %eax
	je	.L24
	movq	8(%rsp), %rdx
	xorl	%eax, %eax
	cmpb	$0, (%rdx)
	jne	.L24
	movl	4(%rsp), %eax
	movl	%eax, (%rbx)
	movl	$1, %eax
.L24:
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__inet_aton_exact, .-__inet_aton_exact
	.p2align 4,,15
	.globl	__inet_aton_ignore_trailing
	.type	__inet_aton_ignore_trailing, @function
__inet_aton_ignore_trailing:
	subq	$24, %rsp
	leaq	8(%rsp), %rdx
	call	inet_aton_end
	addq	$24, %rsp
	ret
	.size	__inet_aton_ignore_trailing, .-__inet_aton_ignore_trailing
	.weak	inet_aton
	.set	inet_aton,__inet_aton_ignore_trailing
	.p2align 4,,15
	.globl	__inet_addr
	.type	__inet_addr, @function
__inet_addr:
	subq	$24, %rsp
	leaq	8(%rsp), %rdx
	leaq	4(%rsp), %rsi
	call	inet_aton_end
	testl	%eax, %eax
	movl	$-1, %edx
	je	.L33
	movl	4(%rsp), %edx
.L33:
	movl	%edx, %eax
	addq	$24, %rsp
	ret
	.size	__inet_addr, .-__inet_addr
	.weak	inet_addr
	.set	inet_addr,__inet_addr
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
	.type	max.8115, @object
	.size	max.8115, 16
max.8115:
	.long	-1
	.long	16777215
	.long	65535
	.long	255
	.hidden	strtoul
