	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __sigvec,sigvec@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__sigvec
	.type	__sigvec, @function
__sigvec:
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdx, %rbx
	subq	$320, %rsp
	testq	%rsi, %rsi
	je	.L2
	movl	12(%rsi), %eax
	movl	8(%rsi), %edx
	movq	(%rsi), %r8
	movl	$134217728, %esi
	movl	%eax, %ecx
	andl	$1, %ecx
	movq	%r8, 160(%rsp)
	cmovne	%esi, %ecx
	movl	%ecx, %esi
	orl	$268435456, %esi
	testb	$2, %al
	cmove	%esi, %ecx
	movl	%ecx, %esi
	orl	$-2147483648, %esi
	testb	$4, %al
	movl	%edx, %eax
	cmovne	%esi, %ecx
	leaq	160(%rsp), %rsi
	movq	%rax, 168(%rsp)
	leaq	136(%rsi), %rdx
	leaq	16(%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$8, %rax
	movq	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L6
	movl	%ecx, 296(%rsp)
	movq	%rsi, %rax
.L2:
	movq	%rsp, %rdx
	movq	%rax, %rsi
	call	__GI___sigaction
	testl	%eax, %eax
	js	.L7
	testq	%rbx, %rbx
	je	.L25
	movl	136(%rsp), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	sall	$2, %eax
	movl	%eax, %ecx
	orl	$1, %ecx
	testl	$134217728, %edx
	cmovne	%ecx, %eax
	movl	%eax, %ecx
	orl	$2, %ecx
	andl	$268435456, %edx
	movq	(%rsp), %rdx
	cmove	%ecx, %eax
	movl	%eax, 12(%rbx)
	movq	%rdx, (%rbx)
	movq	8(%rsp), %rdx
	movl	%edx, 8(%rbx)
.L25:
	xorl	%eax, %eax
.L1:
	addq	$320, %rsp
	popq	%rbx
	ret
.L7:
	movl	$-1, %eax
	jmp	.L1
	.size	__sigvec, .-__sigvec
