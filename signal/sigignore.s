	.text
	.p2align 4,,15
	.globl	sigignore
	.type	sigignore, @function
sigignore:
	subq	$168, %rsp
	xorl	%edx, %edx
	movq	%rsp, %rsi
	movq	$1, (%rsp)
	movq	$0, 8(%rsp)
	movl	$0, 136(%rsp)
	call	__sigaction
	addq	$168, %rsp
	ret
	.size	sigignore, .-sigignore
	.hidden	__sigaction
