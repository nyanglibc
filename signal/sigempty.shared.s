	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_sigemptyset
	.hidden	__GI_sigemptyset
	.type	__GI_sigemptyset, @function
__GI_sigemptyset:
	testq	%rdi, %rdi
	je	.L5
	movq	$0, (%rdi)
	xorl	%eax, %eax
	ret
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI_sigemptyset, .-__GI_sigemptyset
	.globl	sigemptyset
	.set	sigemptyset,__GI_sigemptyset
