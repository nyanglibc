	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __sigismember_compat,__sigismember@GLIBC_2.2.5
	.symver __sigaddset_compat,__sigaddset@GLIBC_2.2.5
	.symver __sigdelset_compat,__sigdelset@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__sigismember_compat
	.type	__sigismember_compat, @function
__sigismember_compat:
	leal	-1(%rsi), %eax
	addl	$62, %esi
	cltd
	shrl	$26, %edx
	leal	(%rax,%rdx), %ecx
	andl	$63, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	testl	%eax, %eax
	cmovns	%eax, %esi
	xorl	%eax, %eax
	sarl	$6, %esi
	movslq	%esi, %rsi
	testq	%rdx, (%rdi,%rsi,8)
	setne	%al
	ret
	.size	__sigismember_compat, .-__sigismember_compat
	.p2align 4,,15
	.globl	__sigaddset_compat
	.type	__sigaddset_compat, @function
__sigaddset_compat:
	leal	62(%rsi), %eax
	subl	$1, %esi
	movl	%esi, %edx
	cmovns	%esi, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	sarl	$6, %eax
	leal	(%rsi,%rdx), %ecx
	cltq
	andl	$63, %ecx
	subl	%edx, %ecx
	movl	$1, %edx
	salq	%cl, %rdx
	orq	%rdx, (%rdi,%rax,8)
	xorl	%eax, %eax
	ret
	.size	__sigaddset_compat, .-__sigaddset_compat
	.p2align 4,,15
	.globl	__sigdelset_compat
	.type	__sigdelset_compat, @function
__sigdelset_compat:
	leal	62(%rsi), %eax
	subl	$1, %esi
	movl	%esi, %edx
	cmovns	%esi, %eax
	sarl	$31, %edx
	shrl	$26, %edx
	sarl	$6, %eax
	leal	(%rsi,%rdx), %ecx
	cltq
	andl	$63, %ecx
	subl	%edx, %ecx
	movq	$-2, %rdx
	rolq	%cl, %rdx
	andq	%rdx, (%rdi,%rax,8)
	xorl	%eax, %eax
	ret
	.size	__sigdelset_compat, .-__sigdelset_compat
