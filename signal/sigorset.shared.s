	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	sigorset
	.type	sigorset, @function
sigorset:
	testq	%rsi, %rsi
	sete	%cl
	testq	%rdx, %rdx
	sete	%al
	orb	%al, %cl
	jne	.L5
	testq	%rdi, %rdi
	je	.L5
	movq	(%rsi), %rax
	orq	(%rdx), %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	sigorset, .-sigorset
