	.text
	.p2align 4,,15
	.globl	sigaddset
	.hidden	sigaddset
	.type	sigaddset, @function
sigaddset:
	leal	-1(%rsi), %ecx
	cmpl	$63, %ecx
	ja	.L2
	testq	%rdi, %rdi
	je	.L2
	subl	$32, %esi
	cmpl	$1, %esi
	jbe	.L2
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	sigaddset, .-sigaddset
