	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	sigisemptyset
	.type	sigisemptyset, @function
sigisemptyset:
	testq	%rdi, %rdi
	je	.L5
	movl	(%rdi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	ret
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	sigisemptyset, .-sigisemptyset
