	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sigprocmask
	.hidden	__GI___sigprocmask
	.type	__GI___sigprocmask, @function
__GI___sigprocmask:
	subq	$8, %rsp
	call	__GI___pthread_sigmask
	testl	%eax, %eax
	jne	.L8
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
	.size	__GI___sigprocmask, .-__GI___sigprocmask
	.globl	__sigprocmask
	.set	__sigprocmask,__GI___sigprocmask
	.weak	sigprocmask
	.set	sigprocmask,__sigprocmask
