	.text
	.p2align 4,,15
	.globl	sigismember
	.hidden	sigismember
	.type	sigismember, @function
sigismember:
	leal	-1(%rsi), %ecx
	cmpl	$63, %ecx
	ja	.L5
	testq	%rdi, %rdi
	je	.L5
	movl	$1, %eax
	salq	%cl, %rax
	testq	%rax, (%rdi)
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	sigismember, .-sigismember
