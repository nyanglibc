.text
.globl __sigaltstack
.type __sigaltstack,@function
.align 1<<4
__sigaltstack:
	movl $131, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sigaltstack,.-__sigaltstack
.globl __GI___sigaltstack
.set __GI___sigaltstack,__sigaltstack
.weak sigaltstack
sigaltstack = __sigaltstack
.globl __GI_sigaltstack
.set __GI_sigaltstack,sigaltstack
