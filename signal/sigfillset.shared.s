	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_sigfillset
	.hidden	__GI_sigfillset
	.type	__GI_sigfillset, @function
__GI_sigfillset:
	testq	%rdi, %rdi
	je	.L5
	movabsq	$-6442450945, %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI_sigfillset, .-__GI_sigfillset
	.globl	sigfillset
	.set	sigfillset,__GI_sigfillset
