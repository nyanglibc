	.text
	.p2align 4,,15
	.globl	siginterrupt
	.type	siginterrupt, @function
siginterrupt:
	pushq	%r12
	pushq	%rbp
	movl	%edi, %ebp
	pushq	%rbx
	movl	%esi, %ebx
	xorl	%esi, %esi
	subq	$160, %rsp
	movq	%rsp, %r12
	movq	%r12, %rdx
	call	__sigaction
	testl	%eax, %eax
	js	.L5
	leal	-1(%rbp), %edi
	movl	$1, %edx
	movl	136(%rsp), %esi
	movl	%edi, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rdi,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	leal	62(%rbp), %eax
	salq	%cl, %rdx
	testl	%edi, %edi
	leaq	_sigintr(%rip), %rcx
	cmovns	%edi, %eax
	sarl	$6, %eax
	testl	%ebx, %ebx
	cltq
	movq	(%rcx,%rax,8), %rdi
	jne	.L8
	notq	%rdx
	orl	$268435456, %esi
	andq	%rdi, %rdx
	movl	%esi, 136(%rsp)
	movq	%rdx, (%rcx,%rax,8)
.L4:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	__sigaction
	sarl	$31, %eax
.L1:
	addq	$160, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	orq	%rdi, %rdx
	andl	$-268435457, %esi
	movq	%rdx, (%rcx,%rax,8)
	movl	%esi, 136(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %eax
	jmp	.L1
	.size	siginterrupt, .-siginterrupt
	.hidden	_sigintr
	.hidden	__sigaction
