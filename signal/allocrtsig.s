	.text
	.p2align 4,,15
	.globl	__libc_current_sigrtmin
	.hidden	__libc_current_sigrtmin
	.type	__libc_current_sigrtmin, @function
__libc_current_sigrtmin:
	movl	current_rtmin(%rip), %eax
	ret
	.size	__libc_current_sigrtmin, .-__libc_current_sigrtmin
	.globl	__libc_current_sigrtmin_private
	.set	__libc_current_sigrtmin_private,__libc_current_sigrtmin
	.p2align 4,,15
	.globl	__libc_current_sigrtmax
	.hidden	__libc_current_sigrtmax
	.type	__libc_current_sigrtmax, @function
__libc_current_sigrtmax:
	movl	current_rtmax(%rip), %eax
	ret
	.size	__libc_current_sigrtmax, .-__libc_current_sigrtmax
	.globl	__libc_current_sigrtmax_private
	.set	__libc_current_sigrtmax_private,__libc_current_sigrtmax
	.p2align 4,,15
	.globl	__libc_allocate_rtsig
	.type	__libc_allocate_rtsig, @function
__libc_allocate_rtsig:
	movl	current_rtmin(%rip), %edx
	cmpl	$-1, %edx
	je	.L7
	movl	current_rtmax(%rip), %eax
	cmpl	%eax, %edx
	jg	.L8
	testl	%edi, %edi
	jne	.L9
	leal	-1(%rax), %edx
	movl	%edx, current_rtmax(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leal	1(%rdx), %eax
	movl	%eax, current_rtmin(%rip)
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%edx, %eax
	ret
	.size	__libc_allocate_rtsig, .-__libc_allocate_rtsig
	.globl	__libc_allocate_rtsig_private
	.set	__libc_allocate_rtsig_private,__libc_allocate_rtsig
	.data
	.align 4
	.type	current_rtmax, @object
	.size	current_rtmax, 4
current_rtmax:
	.long	64
	.align 4
	.type	current_rtmin, @object
	.size	current_rtmin, 4
current_rtmin:
	.long	34
