	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sigpause
	.hidden	__GI___sigpause
	.type	__GI___sigpause, @function
__GI___sigpause:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$136, %rsp
	testl	%esi, %esi
	jne	.L11
	movl	%edi, %eax
	movq	%rsp, %rbx
	movq	%rax, (%rsp)
	leaq	8(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	128(%rsp), %rcx
	addq	$8, %rax
	movq	$0, -8(%rax)
	cmpq	%rcx, %rax
	jne	.L7
.L6:
	movq	%rbx, %rdi
	call	__GI___sigsuspend
.L1:
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rsp, %rbx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rbx, %rdx
	call	__GI___sigprocmask
	testl	%eax, %eax
	js	.L5
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__GI_sigdelset
	testl	%eax, %eax
	jns	.L6
.L5:
	movl	$-1, %eax
	jmp	.L1
	.size	__GI___sigpause, .-__GI___sigpause
	.globl	__sigpause
	.set	__sigpause,__GI___sigpause
	.p2align 4,,15
	.weak	__default_sigpause
	.type	__default_sigpause, @function
__default_sigpause:
	xorl	%esi, %esi
	jmp	__GI___sigpause
	.size	__default_sigpause, .-__default_sigpause
	.globl	__libc_sigpause
	.set	__libc_sigpause,__default_sigpause
	.weak	sigpause
	.set	sigpause,__default_sigpause
	.p2align 4,,15
	.weak	__xpg_sigpause
	.type	__xpg_sigpause, @function
__xpg_sigpause:
	movl	$1, %esi
	jmp	__GI___sigpause
	.size	__xpg_sigpause, .-__xpg_sigpause
	.globl	__libc___xpg_sigpause
	.set	__libc___xpg_sigpause,__xpg_sigpause
