	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sigwaitinfo
	.hidden	__GI___sigwaitinfo
	.type	__GI___sigwaitinfo, @function
__GI___sigwaitinfo:
	xorl	%edx, %edx
	jmp	__GI___sigtimedwait
	.size	__GI___sigwaitinfo, .-__GI___sigwaitinfo
	.globl	__sigwaitinfo
	.set	__sigwaitinfo,__GI___sigwaitinfo
	.globl	__libc_sigwaitinfo
	.set	__libc_sigwaitinfo,__sigwaitinfo
	.weak	sigwaitinfo
	.set	sigwaitinfo,__sigwaitinfo
