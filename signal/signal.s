	.text
	.p2align 4,,15
	.type	__bsd_signal.part.1, @function
__bsd_signal.part.1:
	subq	$328, %rsp
	movl	$1, %edx
	movq	%rsi, (%rsp)
	leal	-1(%rdi), %esi
	movq	$0, 8(%rsp)
	movl	%esi, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rsi,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	leal	62(%rdi), %eax
	salq	%cl, %rdx
	testl	%esi, %esi
	leaq	_sigintr(%rip), %rcx
	cmovns	%esi, %eax
	movq	%rsp, %rsi
	sarl	$6, %eax
	cltq
	orq	%rdx, 8(%rsp,%rax,8)
	testq	%rdx, (%rcx,%rax,8)
	leaq	160(%rsp), %rdx
	sete	%al
	movzbl	%al, %eax
	sall	$28, %eax
	movl	%eax, 136(%rsp)
	call	__sigaction
	testl	%eax, %eax
	movq	$-1, %rdx
	js	.L1
	movq	160(%rsp), %rdx
.L1:
	movq	%rdx, %rax
	addq	$328, %rsp
	ret
	.size	__bsd_signal.part.1, .-__bsd_signal.part.1
	.p2align 4,,15
	.globl	__bsd_signal
	.type	__bsd_signal, @function
__bsd_signal:
	leal	-1(%rdi), %eax
	cmpl	$63, %eax
	ja	.L10
	cmpq	$-1, %rsi
	jne	.L14
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leal	-32(%rdi), %eax
	cmpl	$1, %eax
	jbe	.L10
	jmp	__bsd_signal.part.1
	.size	__bsd_signal, .-__bsd_signal
	.weak	ssignal
	.set	ssignal,__bsd_signal
	.weak	signal
	.set	signal,__bsd_signal
	.weak	bsd_signal
	.set	bsd_signal,__bsd_signal
	.hidden	_sigintr
	.comm	_sigintr,128,32
	.hidden	__sigaction
