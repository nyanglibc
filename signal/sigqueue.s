	.text
	.p2align 4,,15
	.globl	__sigqueue
	.type	__sigqueue, @function
__sigqueue:
	pushq	%r13
	pushq	%r12
	movl	%edi, %r12d
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movl	$15, %ecx
	movq	%rdx, %r13
	movl	%esi, %ebx
	subq	$136, %rsp
	movq	%rsp, %rbp
	movq	$0, 4(%rsp)
	movq	$0, 120(%rsp)
	leaq	8(%rbp), %rdi
	rep stosq
	movl	%esi, (%rsp)
	movl	$-1, 8(%rsp)
	call	__getpid
	movl	%eax, 16(%rsp)
	call	__getuid
	movq	%r13, 24(%rsp)
	movl	%eax, 20(%rsp)
	movq	%rbp, %rdx
	movl	%ebx, %esi
	movl	%r12d, %edi
	movl	$129, %eax
#APP
# 40 "../sysdeps/unix/sysv/linux/sigqueue.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L6
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	addq	$136, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__sigqueue, .-__sigqueue
	.weak	sigqueue
	.set	sigqueue,__sigqueue
	.hidden	__getuid
	.hidden	__getpid
