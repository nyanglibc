	.text
	.p2align 4,,15
	.globl	sigset
	.type	sigset, @function
sigset:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movl	%edi, %esi
	movl	%edi, %ebx
	subq	$576, %rsp
	movq	%rsp, %rbp
	movq	$0, (%rsp)
	movq	%rbp, %rdi
	call	sigaddset
	testl	%eax, %eax
	js	.L5
	cmpq	$2, %r12
	je	.L11
	leaq	416(%rsp), %rdx
	leaq	256(%rsp), %rsi
	movl	%ebx, %edi
	movq	%r12, 256(%rsp)
	movq	$0, 264(%rsp)
	movl	$0, 392(%rsp)
	call	__sigaction
	testl	%eax, %eax
	js	.L5
	leaq	128(%rsp), %rdx
	movq	%rbp, %rsi
	movl	$1, %edi
	call	__sigprocmask
	testl	%eax, %eax
	js	.L5
	leal	-1(%rbx), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rdx,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	testl	%edx, %edx
	movq	%rax, %rcx
	leal	62(%rbx), %eax
	cmovns	%edx, %eax
	sarl	$6, %eax
	cltq
	testq	%rcx, 128(%rsp,%rax,8)
	jne	.L7
.L9:
	movq	416(%rsp), %rax
	addq	$576, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	416(%rsp), %rdx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	__sigaction
	testl	%eax, %eax
	jns	.L9
.L5:
	addq	$576, %rsp
	movq	$-1, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	128(%rsp), %rdx
	xorl	%edi, %edi
	movq	%rbp, %rsi
	call	__sigprocmask
	testl	%eax, %eax
	js	.L5
	leal	-1(%rbx), %edx
	movl	%edx, %eax
	sarl	$31, %eax
	shrl	$26, %eax
	leal	(%rdx,%rax), %ecx
	andl	$63, %ecx
	subl	%eax, %ecx
	movl	$1, %eax
	salq	%cl, %rax
	testl	%edx, %edx
	movq	%rax, %rcx
	leal	62(%rbx), %eax
	cmovns	%edx, %eax
	sarl	$6, %eax
	cltq
	testq	%rcx, 128(%rsp,%rax,8)
	je	.L6
.L7:
	addq	$576, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	sigset, .-sigset
	.hidden	__sigprocmask
	.hidden	__sigaction
	.hidden	sigaddset
