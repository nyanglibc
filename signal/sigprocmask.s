	.text
	.p2align 4,,15
	.globl	__sigprocmask
	.hidden	__sigprocmask
	.type	__sigprocmask, @function
__sigprocmask:
	subq	$8, %rsp
	call	__pthread_sigmask
	testl	%eax, %eax
	jne	.L8
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
	.size	__sigprocmask, .-__sigprocmask
	.weak	sigprocmask
	.set	sigprocmask,__sigprocmask
	.hidden	__pthread_sigmask
