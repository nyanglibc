	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.warning.siggetmask
	.previous
#NO_APP
	.p2align 4,,15
	.globl	siggetmask
	.type	siggetmask, @function
siggetmask:
	xorl	%edi, %edi
	jmp	__GI___sigblock
	.size	siggetmask, .-siggetmask
	.section	.gnu.warning.siggetmask
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_siggetmask, @object
	.size	__evoke_link_warning_siggetmask, 57
__evoke_link_warning_siggetmask:
	.string	"warning: `siggetmask' is obsolete; `sigprocmask' is best"
