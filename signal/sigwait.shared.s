	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sigwait
	.hidden	__GI___sigwait
	.type	__GI___sigwait, @function
__GI___sigwait:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	addq	$-128, %rsp
	movq	%rsp, %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	jne	.L1
.L3:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI___sigtimedwait
	testl	%eax, %eax
	js	.L9
	movl	(%rsp), %eax
	movl	%eax, (%r12)
	xorl	%eax, %eax
.L1:
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI___sigwait, .-__GI___sigwait
	.globl	__sigwait
	.set	__sigwait,__GI___sigwait
	.globl	__libc_sigwait
	.set	__libc_sigwait,__sigwait
	.weak	sigwait
	.set	sigwait,__sigwait
