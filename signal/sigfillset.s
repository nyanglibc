	.text
	.p2align 4,,15
	.globl	sigfillset
	.hidden	sigfillset
	.type	sigfillset, @function
sigfillset:
	testq	%rdi, %rdi
	je	.L5
	movabsq	$-6442450945, %rax
	movq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	sigfillset, .-sigfillset
