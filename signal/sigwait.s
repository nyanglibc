	.text
	.p2align 4,,15
	.globl	__sigwait
	.hidden	__sigwait
	.type	__sigwait, @function
__sigwait:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	addq	$-128, %rsp
	movq	%rsp, %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$4, %eax
	jne	.L1
.L3:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__sigtimedwait
	testl	%eax, %eax
	js	.L9
	movl	(%rsp), %eax
	movl	%eax, (%r12)
	xorl	%eax, %eax
.L1:
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__sigwait, .-__sigwait
	.globl	__libc_sigwait
	.set	__libc_sigwait,__sigwait
	.weak	sigwait
	.set	sigwait,__sigwait
	.hidden	__sigtimedwait
