	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	killpg
	.type	killpg, @function
killpg:
	testl	%edi, %edi
	js	.L5
	negl	%edi
	jmp	__GI___kill
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	killpg, .-killpg
