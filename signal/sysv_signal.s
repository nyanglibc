	.text
	.p2align 4,,15
	.type	__sysv_signal.part.1, @function
__sysv_signal.part.1:
	subq	$328, %rsp
	leaq	160(%rsp), %rdx
	movq	%rsi, (%rsp)
	movq	%rsp, %rsi
	movq	$0, 8(%rsp)
	movl	$-536870912, 136(%rsp)
	call	__sigaction
	testl	%eax, %eax
	movq	$-1, %rdx
	js	.L1
	movq	160(%rsp), %rdx
.L1:
	movq	%rdx, %rax
	addq	$328, %rsp
	ret
	.size	__sysv_signal.part.1, .-__sysv_signal.part.1
	.p2align 4,,15
	.globl	__sysv_signal
	.type	__sysv_signal, @function
__sysv_signal:
	leal	-1(%rdi), %eax
	cmpl	$63, %eax
	ja	.L11
	cmpq	$-1, %rsi
	jne	.L13
.L11:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movq	$-1, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	jmp	__sysv_signal.part.1
	.size	__sysv_signal, .-__sysv_signal
	.weak	sysv_signal
	.set	sysv_signal,__sysv_signal
	.hidden	__sigaction
