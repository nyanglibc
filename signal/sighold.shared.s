	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	sighold
	.type	sighold, @function
sighold:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$136, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__GI_sigemptyset
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__GI_sigaddset
	testl	%eax, %eax
	js	.L3
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	call	__GI___sigprocmask
.L1:
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	sighold, .-sighold
