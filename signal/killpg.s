	.text
	.p2align 4,,15
	.globl	killpg
	.type	killpg, @function
killpg:
	testl	%edi, %edi
	js	.L5
	negl	%edi
	jmp	__kill
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	killpg, .-killpg
	.hidden	__kill
