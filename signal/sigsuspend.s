	.text
	.p2align 4,,15
	.globl	__sigsuspend
	.hidden	__sigsuspend
	.type	__sigsuspend, @function
__sigsuspend:
#APP
# 26 "../sysdeps/unix/sysv/linux/sigsuspend.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$8, %esi
	movl	$130, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/sigsuspend.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movl	$8, %esi
	movl	%eax, %edx
	movq	%rbx, %rdi
	movl	$130, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/sigsuspend.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%edx, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rcx
	negl	%eax
	movl	%eax, %fs:(%rcx)
	movl	$-1, %eax
	jmp	.L6
	.size	__sigsuspend, .-__sigsuspend
	.globl	__libc_sigsuspend
	.set	__libc_sigsuspend,__sigsuspend
	.weak	sigsuspend
	.set	sigsuspend,__sigsuspend
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
