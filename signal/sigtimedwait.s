	.text
	.p2align 4,,15
	.globl	__sigtimedwait
	.hidden	__sigtimedwait
	.type	__sigtimedwait, @function
__sigtimedwait:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$24, %rsp
#APP
# 28 "../sysdeps/unix/sysv/linux/sigtimedwait.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$128, %ecx
	movl	$8, %r10d
	movl	%ecx, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/sigtimedwait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L3
	cmpl	$-1, %eax
	setne	%bpl
	testq	%rsi, %rsi
	setne	%dl
	andl	%edx, %ebp
.L4:
	testb	%bpl, %bpl
	je	.L1
	cmpl	$-6, 8(%rbx)
	je	.L12
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$0, 8(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rdx, 8(%rsp)
	movq	%rdi, (%rsp)
	call	__libc_enable_asynccancel
	movl	$128, %ecx
	movl	%eax, %r8d
	movl	$8, %r10d
	movq	8(%rsp), %rdx
	movq	%rbx, %rsi
	movq	(%rsp), %rdi
	movl	%ecx, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/sigtimedwait.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	movq	%rax, %rdx
	setne	%cl
	cmpq	$-4096, %rax
	ja	.L6
	cmpl	$-1, %eax
	movl	%r8d, %edi
	movl	%eax, (%rsp)
	setne	%bpl
	andl	%ecx, %ebp
	call	__libc_disable_asynccancel
	movl	(%rsp), %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
.L6:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%r8d, %edi
	movl	%edx, %fs:(%rax)
	call	__libc_disable_asynccancel
	movl	$-1, %eax
	jmp	.L1
	.size	__sigtimedwait, .-__sigtimedwait
	.weak	sigtimedwait
	.set	sigtimedwait,__sigtimedwait
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
