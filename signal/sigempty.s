	.text
	.p2align 4,,15
	.globl	sigemptyset
	.hidden	sigemptyset
	.type	sigemptyset, @function
sigemptyset:
	testq	%rdi, %rdi
	je	.L5
	movq	$0, (%rdi)
	xorl	%eax, %eax
	ret
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	sigemptyset, .-sigemptyset
