	.text
#APP
	.section .gnu.warning.sigstack
	.previous
#NO_APP
	.p2align 4,,15
	.globl	sigstack
	.type	sigstack, @function
sigstack:
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rax
	movl	$0, %edx
	subq	$64, %rsp
	testq	%rbx, %rbx
	leaq	32(%rsp), %rsi
	cmove	%rdx, %rsi
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.L3
	movq	(%rax), %rdx
	movl	8(%rax), %eax
	movq	%rsp, %rdi
	testl	%eax, %eax
	movq	%rdx, (%rsp)
	movq	%rdx, 16(%rsp)
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 8(%rsp)
.L3:
	call	__sigaltstack
	testl	%eax, %eax
	jne	.L1
	testq	%rbx, %rbx
	je	.L1
	movq	32(%rsp), %rdx
	movq	%rdx, (%rbx)
	movl	40(%rsp), %edx
	andl	$1, %edx
	movl	%edx, 8(%rbx)
.L1:
	addq	$64, %rsp
	popq	%rbx
	ret
	.size	sigstack, .-sigstack
	.section	.gnu.warning.sigstack
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_sigstack, @object
	.size	__evoke_link_warning_sigstack, 77
__evoke_link_warning_sigstack:
	.string	"the `sigstack' function is dangerous.  `sigaltstack' should be used instead."
	.hidden	__sigaltstack
