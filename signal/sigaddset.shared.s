	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_sigaddset
	.hidden	__GI_sigaddset
	.type	__GI_sigaddset, @function
__GI_sigaddset:
	leal	-1(%rsi), %ecx
	cmpl	$63, %ecx
	ja	.L2
	testq	%rdi, %rdi
	je	.L2
	subl	$32, %esi
	cmpl	$1, %esi
	jbe	.L2
	movl	$1, %eax
	salq	%cl, %rax
	orq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI_sigaddset, .-__GI_sigaddset
	.globl	sigaddset
	.set	sigaddset,__GI_sigaddset
