	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_sigdelset
	.hidden	__GI_sigdelset
	.type	__GI_sigdelset, @function
__GI_sigdelset:
	leal	-1(%rsi), %ecx
	cmpl	$63, %ecx
	ja	.L2
	testq	%rdi, %rdi
	je	.L2
	subl	$32, %esi
	cmpl	$1, %esi
	jbe	.L2
	movq	$-2, %rax
	rolq	%cl, %rax
	andq	%rax, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI_sigdelset, .-__GI_sigdelset
	.globl	sigdelset
	.set	sigdelset,__GI_sigdelset
