	.text
	.p2align 4,,15
	.globl	__sigwaitinfo
	.hidden	__sigwaitinfo
	.type	__sigwaitinfo, @function
__sigwaitinfo:
	xorl	%edx, %edx
	jmp	__sigtimedwait
	.size	__sigwaitinfo, .-__sigwaitinfo
	.globl	__libc_sigwaitinfo
	.set	__libc_sigwaitinfo,__sigwaitinfo
	.weak	sigwaitinfo
	.set	sigwaitinfo,__sigwaitinfo
	.hidden	__sigtimedwait
