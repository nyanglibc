	.text
	.p2align 4,,15
	.globl	sigrelse
	.type	sigrelse, @function
sigrelse:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$136, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	sigemptyset
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	sigaddset
	testl	%eax, %eax
	js	.L3
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	$1, %edi
	call	__sigprocmask
.L1:
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$-1, %eax
	jmp	.L1
	.size	sigrelse, .-sigrelse
	.hidden	__sigprocmask
	.hidden	sigaddset
	.hidden	sigemptyset
