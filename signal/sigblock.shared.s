	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___sigblock
	.hidden	__GI___sigblock
	.type	__GI___sigblock, @function
__GI___sigblock:
	subq	$264, %rsp
	movl	%edi, %eax
	movq	%rsp, %rsi
	movq	%rax, (%rsp)
	leaq	128(%rsi), %rdx
	leaq	8(%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rax
	movq	$0, -8(%rax)
	cmpq	%rdx, %rax
	jne	.L2
	leaq	128(%rsp), %rdx
	xorl	%edi, %edi
	call	__GI___sigprocmask
	testl	%eax, %eax
	js	.L4
	movl	128(%rsp), %eax
.L1:
	addq	$264, %rsp
	ret
.L4:
	movl	$-1, %eax
	jmp	.L1
	.size	__GI___sigblock, .-__GI___sigblock
	.globl	__sigblock
	.set	__sigblock,__GI___sigblock
	.weak	sigblock
	.set	sigblock,__sigblock
