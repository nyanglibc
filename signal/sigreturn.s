	.text
#APP
	.section .gnu.glibc-stub.sigreturn
	.previous
	.section .gnu.warning.sigreturn
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__sigreturn
	.type	__sigreturn, @function
__sigreturn:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__sigreturn, .-__sigreturn
	.weak	sigreturn
	.set	sigreturn,__sigreturn
	.section	.gnu.warning.sigreturn
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_sigreturn, @object
	.size	__evoke_link_warning_sigreturn, 50
__evoke_link_warning_sigreturn:
	.string	"sigreturn is not implemented and will always fail"
