	.text
#APP
	nop
.align 16
.LSTART_restore_rt:
	.type __restore_rt,@function
__restore_rt:
	movq $15, %rax
	syscall
.LEND_restore_rt:
.section .eh_frame,"a",@progbits
.LSTARTFRAME_restore_rt:
	.long .LENDCIE_restore_rt-.LSTARTCIE_restore_rt
.LSTARTCIE_restore_rt:
	.long 0
	.byte 1
	.string "zRS"
	.uleb128 1
	.sleb128 -8
	.uleb128 16
	.uleb128 .LENDAUGMNT_restore_rt-.LSTARTAUGMNT_restore_rt
.LSTARTAUGMNT_restore_rt:
	.byte 0x1b
.LENDAUGMNT_restore_rt:
	.align 8
.LENDCIE_restore_rt:
	.long .LENDFDE_restore_rt-.LSTARTFDE_restore_rt
.LSTARTFDE_restore_rt:
	.long .LSTARTFDE_restore_rt-.LSTARTFRAME_restore_rt
	.long (.LSTART_restore_rt-1)-.
	.long .LEND_restore_rt-(.LSTART_restore_rt-1)
	.uleb128 0
	.byte 0x0f
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 160
	.byte 0x06
2:	.byte 0x10
	.uleb128 8
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 40
2:	.byte 0x10
	.uleb128 9
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 48
2:	.byte 0x10
	.uleb128 10
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 56
2:	.byte 0x10
	.uleb128 11
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 64
2:	.byte 0x10
	.uleb128 12
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 72
2:	.byte 0x10
	.uleb128 13
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 80
2:	.byte 0x10
	.uleb128 14
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 88
2:	.byte 0x10
	.uleb128 15
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 96
2:	.byte 0x10
	.uleb128 5
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 104
2:	.byte 0x10
	.uleb128 4
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 112
2:	.byte 0x10
	.uleb128 6
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 120
2:	.byte 0x10
	.uleb128 3
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 128
2:	.byte 0x10
	.uleb128 1
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 136
2:	.byte 0x10
	.uleb128 0
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 144
2:	.byte 0x10
	.uleb128 2
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 152
2:	.byte 0x10
	.uleb128 7
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 160
2:	.byte 0x10
	.uleb128 16
	.uleb128 2f-1f
1:	.byte 0x77
	.sleb128 168
2:	.align 8
.LENDFDE_restore_rt:
	.previous

#NO_APP
	.p2align 4,,15
	.globl	__libc_sigaction
	.hidden	__libc_sigaction
	.type	__libc_sigaction, @function
__libc_sigaction:
	subq	$208, %rsp
	testq	%rsi, %rsi
	movq	%rdx, %r8
	je	.L2
	movdqu	8(%rsi), %xmm0
	leaq	-120(%rsp), %rcx
	movq	(%rsi), %rax
	movups	%xmm0, -96(%rsp)
	movdqu	24(%rsi), %xmm0
	movq	%rax, -120(%rsp)
	movl	136(%rsi), %eax
	movups	%xmm0, 40(%rcx)
	orl	$67108864, %eax
	testq	%rdx, %rdx
	cltq
	movdqu	40(%rsi), %xmm0
	movq	%rax, -112(%rsp)
	leaq	__restore_rt(%rip), %rax
	movups	%xmm0, 56(%rcx)
	movq	%rax, -104(%rsp)
	movdqu	56(%rsi), %xmm0
	movups	%xmm0, 72(%rcx)
	movdqu	72(%rsi), %xmm0
	movups	%xmm0, 88(%rcx)
	movdqu	88(%rsi), %xmm0
	movups	%xmm0, 104(%rcx)
	movdqu	104(%rsi), %xmm0
	movups	%xmm0, 120(%rcx)
	movdqu	120(%rsi), %xmm0
	movups	%xmm0, 136(%rcx)
	je	.L14
	leaq	40(%rsp), %rdx
.L8:
	movq	%rcx, %rsi
.L7:
	movl	$13, %ecx
	movl	$8, %r10d
	movl	%ecx, %eax
#APP
# 58 "../sysdeps/unix/sysv/linux/sigaction.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L15
	testq	%r8, %r8
	je	.L1
	testl	%eax, %eax
	js	.L1
	movdqu	64(%rsp), %xmm0
	movq	40(%rsp), %rdx
	movups	%xmm0, 8(%r8)
	movdqu	80(%rsp), %xmm0
	movq	%rdx, (%r8)
	movq	48(%rsp), %rdx
	movups	%xmm0, 24(%r8)
	movdqu	96(%rsp), %xmm0
	movl	%edx, 136(%r8)
	movq	56(%rsp), %rdx
	movups	%xmm0, 40(%r8)
	movdqu	112(%rsp), %xmm0
	movq	%rdx, 144(%r8)
	movups	%xmm0, 56(%r8)
	movdqu	128(%rsp), %xmm0
	movups	%xmm0, 72(%r8)
	movdqu	144(%rsp), %xmm0
	movups	%xmm0, 88(%r8)
	movdqu	160(%rsp), %xmm0
	movups	%xmm0, 104(%r8)
	movdqu	176(%rsp), %xmm0
	movups	%xmm0, 120(%r8)
.L1:
	addq	$208, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testq	%rdx, %rdx
	jne	.L6
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	40(%rsp), %rdx
	xorl	%esi, %esi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L15:
	negl	%edx
	movl	$-1, %eax
	movl	%edx, rtld_errno(%rip)
	addq	$208, %rsp
	ret
.L14:
	xorl	%edx, %edx
	jmp	.L8
	.size	__libc_sigaction, .-__libc_sigaction
	.p2align 4,,15
	.globl	__sigaction
	.hidden	__sigaction
	.type	__sigaction, @function
__sigaction:
	leal	-1(%rdi), %eax
	cmpl	$63, %eax
	ja	.L17
	leal	-32(%rdi), %eax
	cmpl	$1, %eax
	jbe	.L17
	jmp	__libc_sigaction
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$22, rtld_errno(%rip)
	movl	$-1, %eax
	ret
	.size	__sigaction, .-__sigaction
	.weak	sigaction
	.set	sigaction,__sigaction
	.hidden	rtld_errno
