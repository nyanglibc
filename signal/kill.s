.text
.globl __kill
.type __kill,@function
.align 1<<4
__kill:
	movl $62, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __kill,.-__kill
.weak kill
kill = __kill
