	.text
	.p2align 4,,15
	.globl	__offtime
	.hidden	__offtime
	.type	__offtime, @function
__offtime:
	movq	%rdi, %rax
	movq	%rdx, %r11
	movabsq	$1749024623285053783, %rdx
	imulq	%rdx
	movq	%rdi, %rax
	pushq	%r14
	sarq	$63, %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	sarq	$13, %rdx
	subq	%rax, %rdx
	imulq	$86400, %rdx, %rax
	movq	%rdx, %r8
	subq	%rax, %rdi
	addq	%rdi, %rsi
	jns	.L27
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$1, %r8
	addq	$86400, %rsi
	js	.L3
.L4:
	movq	%rsi, %rdx
	movabsq	$655884233731895169, %rcx
	movabsq	$-6640827866535438581, %r9
	shrq	$4, %rdx
	movabsq	$3234497591006606311, %r10
	movq	%rdx, %rax
	mulq	%rcx
	leaq	4(%r8), %rcx
	shrq	$3, %rdx
	movl	%edx, 8(%r11)
	imulq	$3600, %rdx, %rdx
	subq	%rdx, %rsi
	movabsq	$-8608480567731124087, %rdx
	movq	%rsi, %rax
	mulq	%rdx
	shrq	$5, %rdx
	movq	%rdx, %rax
	movl	%edx, 4(%r11)
	salq	$4, %rax
	subq	%rdx, %rax
	movabsq	$5270498306774157605, %rdx
	salq	$2, %rax
	subq	%rax, %rsi
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rcx, %rax
	movl	%esi, (%r11)
	sarq	$63, %rax
	sarq	%rdx
	subq	%rax, %rdx
	leaq	0(,%rdx,8), %rax
	subq	%rdx, %rax
	subq	%rax, %rcx
	leal	7(%rcx), %eax
	movq	%rcx, %rdx
	testq	%rcx, %rcx
	movl	$1970, %ecx
	cmovs	%eax, %edx
	movl	%edx, 24(%r11)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%r8, %rax
	movq	%r8, %rbx
	imulq	%r10
	movq	%r8, %rax
	sarq	$63, %rax
	sarq	$6, %rdx
	subq	%rax, %rdx
	leaq	(%rdx,%rdx,8), %rax
	leaq	(%rdx,%rcx), %rdi
	leaq	(%rdx,%rax,8), %rax
	leaq	(%rax,%rax,4), %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	shrq	$63, %rax
	subq	%rax, %rdi
	movq	%rdi, %rbx
	leaq	2(%rdi), %r12
	subq	$1, %rbx
	movq	%rbx, %rbp
	cmovns	%rbx, %r12
	movq	%rbx, %r14
	sarq	$63, %rbp
	sarq	$2, %r12
	movq	%rbp, %rdx
	shrq	$62, %rdx
	leaq	(%rbx,%rdx), %rax
	andl	$3, %eax
	subq	%rdx, %rax
	shrq	$63, %rax
	subq	%rax, %r12
	movq	%rbx, %rax
	imulq	%r9
	leaq	(%rdx,%rbx), %rsi
	movq	%rsi, %rax
	sarq	$8, %rsi
	sarq	$6, %rax
	subq	%rbp, %rsi
	subq	%rbp, %rax
	leaq	(%rax,%rax,4), %rdx
	leaq	(%rdx,%rdx,4), %rdx
	salq	$2, %rdx
	subq	%rdx, %r14
	movq	%r14, %rdx
	shrq	$63, %rdx
	subq	%rdx, %rax
	subq	%rax, %r12
	leaq	(%rsi,%rsi,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	subq	%rax, %rbx
	movq	%rbx, %rax
	shrq	$63, %rax
	subq	%rax, %rsi
	movq	%rdi, %rax
	subq	%rcx, %rax
	addq	%r12, %rsi
	leaq	(%rax,%rax,8), %rdx
	leaq	(%rax,%rdx,8), %rax
	leaq	2(%rcx), %rdx
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rsi
	subq	$1, %rcx
	movq	%rcx, %rbx
	cmovns	%rcx, %rdx
	movq	%rcx, %r14
	sarq	$63, %rbx
	sarq	$2, %rdx
	movq	%rbx, %rbp
	shrq	$62, %rbp
	leaq	(%rcx,%rbp), %rax
	andl	$3, %eax
	subq	%rbp, %rax
	shrq	$63, %rax
	subq	%rax, %rdx
	movq	%rcx, %rax
	movq	%rdx, %rbp
	imulq	%r9
	addq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$8, %rdx
	sarq	$6, %rax
	subq	%rbx, %rdx
	subq	%rbx, %rax
	leaq	(%rax,%rax,4), %r12
	leaq	(%r12,%r12,4), %r12
	salq	$2, %r12
	subq	%r12, %r14
	movq	%r14, %r12
	shrq	$63, %r12
	subq	%r12, %rax
	subq	%rax, %rbp
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	subq	%rax, %rcx
	shrq	$63, %rcx
	subq	%rcx, %rdx
	movq	%rdi, %rcx
	addq	%rbp, %rdx
	subq	%rdx, %rsi
	subq	%rsi, %r8
.L8:
	testq	%r8, %r8
	js	.L9
	movq	%rcx, %rsi
	movl	$365, %eax
	andl	$3, %esi
	jne	.L10
	movq	%rcx, %rax
	movq	%rcx, %rbx
	imulq	%r9
	sarq	$63, %rbx
	addq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$6, %rax
	subq	%rbx, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rdi
	movl	$366, %eax
	salq	$2, %rdi
	cmpq	%rdi, %rcx
	jne	.L10
	sarq	$8, %rdx
	subq	%rbx, %rdx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	cmpq	%rax, %rcx
	sete	%al
	movzbl	%al, %eax
	addq	$365, %rax
.L10:
	cmpq	%rax, %r8
	jge	.L9
	leal	-1900(%rcx), %eax
	leaq	-1900(%rcx), %rdx
	movl	%eax, 20(%r11)
	cltq
	cmpq	%rdx, %rax
	je	.L11
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	subq	$86400, %rsi
	addq	$1, %r8
.L27:
	cmpq	$86399, %rsi
	jg	.L5
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%rsi, %rsi
	movl	%r8d, 28(%r11)
	leaq	__mon_yday(%rip), %rdi
	jne	.L13
	movq	%rcx, %rax
	movabsq	$-6640827866535438581, %rdx
	movq	%rcx, %rsi
	imulq	%rdx
	sarq	$63, %rsi
	addq	$26, %rdi
	addq	%rcx, %rdx
	movq	%rdx, %rax
	sarq	$6, %rax
	subq	%rsi, %rax
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	cmpq	%rax, %rcx
	jne	.L13
	sarq	$8, %rdx
	subq	%rsi, %rdx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rax,%rax,4), %rax
	salq	$4, %rax
	cmpq	%rax, %rcx
	leaq	-26(%rdi), %rax
	cmovne	%rax, %rdi
.L13:
	movzwl	22(%rdi), %edx
	movl	$11, %eax
	cmpq	%r8, %rdx
	jg	.L29
.L14:
	subq	%rdx, %r8
	movl	%eax, 16(%r11)
	movl	$1, %eax
	addl	$1, %r8d
	popq	%rbx
	movl	%r8d, 12(%r11)
	popq	%rbp
	popq	%r12
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$11, %eax
.L15:
	subq	$1, %rax
	movzwl	(%rdi,%rax,2), %edx
	cmpq	%r8, %rdx
	jle	.L14
	jmp	.L15
	.size	__offtime, .-__offtime
	.hidden	__mon_yday
