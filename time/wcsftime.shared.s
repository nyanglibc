	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_wcsftime
	.hidden	__GI_wcsftime
	.type	__GI_wcsftime, @function
__GI_wcsftime:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__GI___wcsftime_l
	.size	__GI_wcsftime, .-__GI_wcsftime
	.globl	wcsftime
	.set	wcsftime,__GI_wcsftime
