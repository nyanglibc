	.text
	.p2align 4,,15
	.globl	strftime
	.hidden	strftime
	.type	strftime, @function
strftime:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__strftime_l
	.size	strftime, .-strftime
	.hidden	__strftime_l
