	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_strftime
	.hidden	__GI_strftime
	.type	__GI_strftime, @function
__GI_strftime:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__GI___strftime_l
	.size	__GI_strftime, .-__GI_strftime
	.globl	strftime
	.set	strftime,__GI_strftime
