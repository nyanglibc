	.text
	.p2align 4,,15
	.type	_nl_init_era_entries.part.0, @function
_nl_init_era_entries.part.0:
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L2
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L2:
	movq	40(%rbp), %r13
	testq	%r13, %r13
	je	.L28
.L3:
	movl	16(%r13), %eax
	testl	%eax, %eax
	jne	.L5
	movl	464(%rbp), %r14d
	movq	0(%r13), %rdi
	testq	%r14, %r14
	jne	.L7
	testq	%rdi, %rdi
	je	.L9
	call	free@PLT
	movq	$0, 0(%r13)
.L9:
	movl	$1, 16(%r13)
.L5:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L1
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	leaq	__libc_setlocale_lock(%rip), %rdi
	jmp	__pthread_rwlock_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpq	8(%r13), %r14
	jne	.L29
	testq	%rdi, %rdi
	je	.L30
.L11:
	leaq	(%r14,%r14,8), %rax
	movq	472(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r14, 8(%r13)
	movq	%rdi, 0(%r13)
	leaq	(%rdi,%rax,8), %rbp
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L32:
	je	.L31
.L13:
	xorl	%eax, %eax
	cmpl	$43, (%rbx)
	setne	%al
	leal	-1(%rax,%rax), %eax
	movl	%eax, 64(%rbx)
.L15:
	leaq	32(%r12), %r14
	addq	$72, %rbx
	movq	%r14, -40(%rbx)
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	1(%r14,%rax), %r14
	movq	%r14, -32(%rbx)
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	1(%r14,%rax), %rax
	xorl	%esi, %esi
	movq	%rax, %rdi
	subq	%r12, %rdi
	addq	$3, %rdi
	notq	%rdi
	andl	$3, %edi
	addq	%rax, %rdi
	movq	%rdi, -24(%rbx)
	call	__wcschr
	leaq	4(%rax), %rdi
	xorl	%esi, %esi
	movq	%rdi, -16(%rbx)
	call	__wcschr
	cmpq	%rbx, %rbp
	leaq	4(%rax), %r12
	je	.L9
.L17:
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%r12), %xmm0
	movups	%xmm0, 16(%rbx)
	movl	20(%rbx), %eax
	cmpl	%eax, 8(%rbx)
	jge	.L32
.L12:
	xorl	%eax, %eax
	cmpl	$43, (%rbx)
	sete	%al
	leal	-1(%rax,%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	(%r14,%r14,8), %rsi
	salq	$3, %rsi
	call	realloc@PLT
	movq	%rax, %rdi
	testq	%rdi, %rdi
	jne	.L11
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movl	24(%rbx), %eax
	cmpl	%eax, 12(%rbx)
	jl	.L12
	jne	.L13
	movl	28(%rbx), %eax
	cmpl	%eax, 16(%rbx)
	jg	.L13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L28:
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, 40(%rbp)
	je	.L5
	leaq	_nl_cleanup_time(%rip), %rax
	movq	%rax, 32(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L30:
	movq	0(%r13), %rdi
	call	free@PLT
	movq	$0, 8(%r13)
	movq	$0, 0(%r13)
	jmp	.L9
	.size	_nl_init_era_entries.part.0, .-_nl_init_era_entries.part.0
	.p2align 4,,15
	.globl	_nl_get_era_entry
	.hidden	_nl_get_era_entry
	.type	_nl_get_era_entry, @function
_nl_get_era_entry:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L34
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	je	.L35
.L36:
	movq	8(%rax), %r8
	movl	20(%rbx), %esi
	movl	16(%rbx), %r9d
	movl	12(%rbx), %r10d
	testq	%r8, %r8
	je	.L37
	movq	(%rax), %rdx
	xorl	%ecx, %ecx
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L63:
	je	.L60
.L39:
	cmpl	20(%rdx), %esi
	jle	.L61
.L44:
	cmpl	%edi, %esi
	jl	.L33
.L65:
	je	.L62
.L45:
	addq	$1, %rcx
	addq	$72, %rdx
	cmpq	%r8, %rcx
	je	.L37
.L46:
	movl	8(%rdx), %edi
	movq	%rdx, %rax
	cmpl	%edi, %esi
	jle	.L63
.L38:
	cmpl	20(%rdx), %esi
	jl	.L33
	je	.L64
.L42:
	jle	.L45
	cmpl	%edi, %esi
	jge	.L65
.L33:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	cmpl	24(%rdx), %r9d
	jl	.L33
	jne	.L42
	cmpl	28(%rdx), %r10d
	jle	.L33
.L43:
	cmpl	28(%rdx), %r10d
	jge	.L44
	addq	$1, %rcx
	addq	$72, %rdx
	cmpq	%r8, %rcx
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L37:
	xorl	%eax, %eax
.L66:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	cmpl	12(%rdx), %r9d
	jl	.L33
	jne	.L45
	cmpl	16(%rdx), %r10d
	jg	.L45
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L60:
	cmpl	12(%rdx), %r9d
	jg	.L38
	jne	.L39
	cmpl	16(%rdx), %r10d
	jge	.L38
	cmpl	20(%rdx), %esi
	jg	.L44
	.p2align 4,,10
	.p2align 3
.L61:
	jne	.L45
	cmpl	24(%rdx), %r9d
	jg	.L44
	jne	.L45
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L35:
	movl	464(%rsi), %edx
	testl	%edx, %edx
	je	.L36
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L34:
	movl	464(%rsi), %eax
	testl	%eax, %eax
	je	.L37
.L47:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	_nl_init_era_entries.part.0
	movq	8(%rsp), %rsi
	movq	40(%rsi), %rax
	testq	%rax, %rax
	jne	.L36
	xorl	%eax, %eax
	jmp	.L66
	.size	_nl_get_era_entry, .-_nl_get_era_entry
	.p2align 4,,15
	.globl	_nl_select_era_entry
	.hidden	_nl_select_era_entry
	.type	_nl_select_era_entry, @function
_nl_select_era_entry:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L68
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	je	.L69
.L70:
	movslq	%ebx, %rdi
	movq	(%rax), %rax
	leaq	(%rdi,%rdi,8), %rdx
	leaq	(%rax,%rdx,8), %rax
.L67:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movl	464(%rsi), %edx
	testl	%edx, %edx
	je	.L70
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L68:
	movl	464(%rsi), %eax
	testl	%eax, %eax
	je	.L73
.L72:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	_nl_init_era_entries.part.0
	movq	8(%rsp), %rsi
	movq	40(%rsi), %rax
	testq	%rax, %rax
	jne	.L70
	.p2align 4,,10
	.p2align 3
.L73:
	xorl	%eax, %eax
	jmp	.L67
	.size	_nl_select_era_entry, .-_nl_select_era_entry
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	_nl_cleanup_time
	.hidden	__wcschr
	.hidden	__libc_setlocale_lock
