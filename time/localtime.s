	.text
	.p2align 4,,15
	.globl	__localtime_r
	.hidden	__localtime_r
	.type	__localtime_r, @function
__localtime_r:
	movq	(%rdi), %rdi
	movq	%rsi, %rdx
	movl	$1, %esi
	jmp	__tz_convert
	.size	__localtime_r, .-__localtime_r
	.weak	localtime_r
	.set	localtime_r,__localtime_r
	.p2align 4,,15
	.globl	localtime
	.hidden	localtime
	.type	localtime, @function
localtime:
	movq	(%rdi), %rdi
	leaq	_tmbuf(%rip), %rdx
	movl	$1, %esi
	jmp	__tz_convert
	.size	localtime, .-localtime
	.hidden	_tmbuf
	.comm	_tmbuf,56,32
	.hidden	__tz_convert
