	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __clock_getres,clock_getres@@GLIBC_2.17
	.symver __clock_getres_2,clock_getres@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__clock_getres
	.type	__clock_getres, @function
__clock_getres:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$8, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	720(%rax), %rax
	testq	%rax, %rax
	je	.L6
	call	*%rax
	movslq	%eax, %rdx
	cmpq	$-4096, %rdx
	jbe	.L1
	cmpq	$-38, %rdx
	je	.L6
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
.L5:
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$229, %eax
#APP
# 40 "../sysdeps/unix/sysv/linux/clock_getres.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L3
	jmp	.L1
	.size	__clock_getres, .-__clock_getres
	.globl	__clock_getres_2
	.set	__clock_getres_2,__clock_getres
