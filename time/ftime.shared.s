	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	ftime
	.type	ftime, @function
ftime:
	pushq	%rbx
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$16, %rsp
	movq	%rsp, %rsi
	call	__GI___clock_gettime
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
	movabsq	$4835703278458516699, %rdx
	movl	$0, 10(%rbx)
	movq	%rax, (%rbx)
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	xorl	%eax, %eax
	sarq	$18, %rdx
	subq	%rcx, %rdx
	movw	%dx, 8(%rbx)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	ftime, .-ftime
