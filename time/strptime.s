	.text
	.p2align 4,,15
	.globl	strptime
	.hidden	strptime
	.type	strptime, @function
strptime:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	__strptime_internal
	.size	strptime, .-strptime
	.hidden	__strptime_internal
