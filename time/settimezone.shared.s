	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__settimezone
	.hidden	__settimezone
	.type	__settimezone, @function
__settimezone:
	movq	%rdi, %rsi
	movl	$164, %eax
	xorl	%edi, %edi
#APP
# 32 "../sysdeps/unix/sysv/linux/settimezone.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__settimezone, .-__settimezone
