	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	ctime_r
	.type	ctime_r, @function
ctime_r:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$64, %rsp
	movq	%rsp, %rsi
	call	__localtime_r
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__asctime_r
	addq	$64, %rsp
	popq	%rbx
	ret
	.size	ctime_r, .-ctime_r
	.hidden	__asctime_r
	.hidden	__localtime_r
