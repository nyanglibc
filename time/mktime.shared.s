	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	ydhms_diff, @function
ydhms_diff:
	movq	%rdi, %r10
	pushq	%r14
	xorl	%eax, %eax
	sarq	$2, %r10
	pushq	%r13
	pushq	%r12
	addl	$475, %r10d
	pushq	%rbp
	testb	$3, %dil
	pushq	%rbx
	movslq	%r9d, %rbx
	sete	%al
	movq	%rbx, %r12
	subl	%eax, %r10d
	xorl	%eax, %eax
	sarq	$2, %r12
	movl	%r10d, %r13d
	movslq	%edx, %r11
	addl	$475, %r12d
	andl	$3, %r9d
	movl	$1374389535, %r9d
	sete	%al
	shrl	$31, %r13d
	subq	%rbx, %rdi
	leal	0(%r13,%r10), %r14d
	subl	%eax, %r12d
	movslq	%ecx, %rcx
	subl	%r12d, %r10d
	movslq	%r8d, %r8
	movl	%r14d, %eax
	sarl	$31, %r14d
	imull	%r9d
	sarl	$3, %edx
	movl	%edx, %ebp
	subl	%r14d, %ebp
	subl	%r13d, %ebp
	movl	%r12d, %r13d
	shrl	$31, %r13d
	leal	0(%r13,%r12), %r14d
	movl	%r14d, %eax
	sarl	$31, %r14d
	imull	%r9d
	movl	%ebp, %eax
	movl	%edx, %r9d
	movslq	56(%rsp), %rdx
	sarl	$3, %r9d
	subl	%r14d, %r9d
	subl	%r13d, %r9d
	subl	%r9d, %eax
	movslq	%r9d, %r9
	subl	%eax, %r10d
	sarq	$2, %r9
	movl	%r10d, %eax
	movslq	%ebp, %r10
	sarq	$2, %r10
	subl	%r9d, %r10d
	addl	%eax, %r10d
	leaq	(%rdi,%rdi,8), %rax
	movslq	%r10d, %r10
	leaq	(%rdi,%rax,8), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rsi
	movslq	48(%rsp), %rax
	subq	%rax, %rsi
	addq	%r10, %rsi
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r11,%rax,8), %rax
	subq	%rdx, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx,4), %rax
	movslq	64(%rsp), %rdx
	subq	%rdx, %rax
	movq	%rax, %rdx
	salq	$4, %rdx
	subq	%rax, %rdx
	leaq	(%r8,%rdx,4), %rax
	movslq	72(%rsp), %rdx
	popq	%rbx
	popq	%rbp
	popq	%r12
	subq	%rdx, %rax
	popq	%r13
	popq	%r14
	ret
	.size	ydhms_diff, .-ydhms_diff
	.p2align 4,,15
	.type	ranged_convert, @function
ranged_convert:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$104, %rsp
	movq	(%rsi), %r13
	leaq	88(%rsp), %rbp
	movq	%rsi, 72(%rsp)
	movq	%rdx, %rsi
	movq	%r13, 88(%rsp)
	movq	%rbp, %rdi
	call	*%rbx
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L5
	movq	%r13, (%r14)
.L4:
	movq	16(%rsp), %rax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$-1, 12(%rsp)
	cmpl	$75, %fs:(%rax)
	jne	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%r14, %r12
	movq	%r13, %rax
	sarq	%r12
	sarq	%rax
	addq	%r12, %rax
	movq	%r13, %r12
	orq	%r14, %r12
	andl	$1, %r12d
	addq	%rax, %r12
	cmpq	%r12, %r14
	je	.L8
	cmpq	%r12, %r13
	je	.L8
	movq	%r12, 88(%rsp)
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	*%rbx
	testq	%rax, %rax
	je	.L9
	movl	(%r15), %eax
	movq	%r12, %r14
	movl	%eax, 12(%rsp)
	movl	4(%r15), %eax
	movl	%eax, 32(%rsp)
	movl	8(%r15), %eax
	movl	%eax, 36(%rsp)
	movl	12(%r15), %eax
	movl	%eax, 40(%rsp)
	movl	16(%r15), %eax
	movl	%eax, 68(%rsp)
	movl	20(%r15), %eax
	movl	%eax, 64(%rsp)
	movl	24(%r15), %eax
	movl	%eax, 44(%rsp)
	movl	28(%r15), %eax
	movl	%eax, 24(%rsp)
	movl	32(%r15), %eax
	movl	%eax, 28(%rsp)
	movq	40(%r15), %rax
	movq	%rax, 48(%rsp)
	movq	48(%r15), %rax
	movq	%rax, 56(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$75, %fs:(%rax)
	jne	.L4
	movq	%r12, %r13
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	movl	12(%rsp), %eax
	testl	%eax, %eax
	js	.L4
	movq	72(%rsp), %rcx
	movq	%r15, 16(%rsp)
	movq	%r14, (%rcx)
	movl	%eax, (%r15)
	movl	32(%rsp), %eax
	movl	%eax, 4(%r15)
	movl	36(%rsp), %eax
	movl	%eax, 8(%r15)
	movl	40(%rsp), %eax
	movl	%eax, 12(%r15)
	movl	68(%rsp), %eax
	movl	%eax, 16(%r15)
	movl	64(%rsp), %eax
	movl	%eax, 20(%r15)
	movl	44(%rsp), %eax
	movl	%eax, 24(%r15)
	movl	24(%rsp), %eax
	movl	%eax, 28(%r15)
	movl	28(%rsp), %eax
	movl	%eax, 32(%r15)
	movq	48(%rsp), %rax
	movq	%rax, 40(%r15)
	movq	56(%rsp), %rax
	movq	%rax, 48(%r15)
	jmp	.L4
	.size	ranged_convert, .-ranged_convert
	.p2align 4,,15
	.globl	__mktime_internal
	.hidden	__mktime_internal
	.type	__mktime_internal, @function
__mktime_internal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$280, %rsp
	movl	(%rdi), %eax
	movq	%rsi, 32(%rsp)
	movslq	12(%rdi), %rsi
	movq	%rdx, 56(%rsp)
	movl	$715827883, %edx
	movl	%eax, 52(%rsp)
	movl	4(%rdi), %eax
	movl	%eax, 8(%rsp)
	movl	8(%rdi), %eax
	movl	16(%rdi), %edi
	movl	%eax, 12(%rsp)
	movl	32(%r13), %eax
	movl	%eax, 72(%rsp)
	movl	%edi, %eax
	imull	%edx
	movl	%edi, %eax
	sarl	$31, %eax
	sarl	%edx
	movl	%edx, %ecx
	subl	%eax, %ecx
	leal	(%rcx,%rcx,2), %edx
	movl	%ecx, %eax
	movl	%edi, %ecx
	sall	$2, %edx
	subl	%edx, %ecx
	movslq	20(%r13), %rdx
	movl	%ecx, %edi
	shrl	$31, %edi
	subl	%edi, %eax
	cltq
	leaq	(%rax,%rdx), %r15
	xorl	%eax, %eax
	testb	$3, %r15b
	jne	.L16
	movq	%r15, %rax
	movabsq	$-6640827866535438581, %rdx
	imulq	%rdx
	movq	%r15, %rax
	sarq	$63, %rax
	addq	%r15, %rdx
	sarq	$6, %rdx
	subq	%rax, %rdx
	leaq	(%rdx,%rdx,4), %rax
	leaq	(%rax,%rax,4), %r8
	movl	$1, %eax
	salq	$2, %r8
	cmpq	%r8, %r15
	je	.L62
.L16:
	leal	(%rdi,%rdi), %edx
	movl	52(%rsp), %ebx
	movl	$70, %r9d
	addl	%edx, %edi
	leal	(%rcx,%rdi,4), %edx
	leaq	(%rax,%rax), %rcx
	movq	%r15, %rdi
	addq	%rax, %rcx
	movslq	%edx, %rdx
	leaq	(%rax,%rcx,4), %rax
	addq	%rdx, %rax
	leaq	__mon_yday(%rip), %rdx
	movzwl	(%rdx,%rax,2), %eax
	movl	$0, %edx
	subl	$1, %eax
	cltq
	addq	%rax, %rsi
	cmpl	$59, %ebx
	movl	$59, %eax
	cmovle	%ebx, %eax
	movq	%rsi, 24(%rsp)
	movl	$6, %ebx
	testl	%eax, %eax
	cmovns	%eax, %edx
	movq	56(%rsp), %rax
	xorl	%r14d, %r14d
	movl	%edx, 48(%rsp)
	movl	%edx, %r8d
	movq	(%rax), %rax
	movq	%rax, 16(%rsp)
	movl	16(%rsp), %eax
	negl	%eax
	movl	%eax, 76(%rsp)
	pushq	%rax
	pushq	$0
	pushq	$0
	pushq	$0
	movl	40(%rsp), %ecx
	movl	44(%rsp), %edx
	call	ydhms_diff
	addq	$32, %rsp
	movq	%rax, %r12
	movq	%rax, 80(%rsp)
	movq	%rax, 120(%rsp)
	movq	%rax, %rbp
	leaq	144(%rsp), %rax
	movq	%r13, 64(%rsp)
	movq	%rax, 16(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 40(%rsp)
.L24:
	movq	16(%rsp), %rdx
	movq	40(%rsp), %rsi
	movq	32(%rsp), %rdi
	call	ranged_convert
	testq	%rax, %rax
	je	.L28
	movl	144(%rsp), %r13d
	movq	%r15, %rdi
	pushq	%r13
	movl	156(%rsp), %eax
	pushq	%rax
	movl	168(%rsp), %eax
	pushq	%rax
	movl	196(%rsp), %eax
	pushq	%rax
	movl	196(%rsp), %r9d
	movl	80(%rsp), %r8d
	movl	40(%rsp), %ecx
	movl	44(%rsp), %edx
	movq	56(%rsp), %rsi
	call	ydhms_diff
	addq	$32, %rsp
	testq	%rax, %rax
	movq	120(%rsp), %rdx
	je	.L19
	cmpq	%rdx, %r12
	jne	.L20
	cmpq	%rdx, %rbp
	jne	.L63
.L20:
	subl	$1, %ebx
	je	.L33
	addq	%rdx, %rax
	xorl	%r14d, %r14d
	movq	%rbp, %r12
	movq	%rax, 120(%rsp)
	movl	176(%rsp), %eax
	movq	%rdx, %rbp
	testl	%eax, %eax
	setne	%r14b
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L63:
	movl	176(%rsp), %edi
	testl	%edi, %edi
	js	.L60
	movl	72(%rsp), %esi
	setne	%cl
	testl	%esi, %esi
	js	.L64
	setne	%sil
	cmpb	%cl, %sil
	je	.L20
.L60:
	movl	%r13d, %r14d
	movq	64(%rsp), %r13
.L21:
	movq	%rdx, %rax
	movslq	76(%rsp), %rcx
	subq	80(%rsp), %rax
	movq	56(%rsp), %rsi
	subq	%rcx, %rax
	movq	%rax, (%rsi)
	movl	52(%rsp), %esi
	cmpl	%r14d, %esi
	je	.L35
	testl	%esi, %esi
	setle	%cl
	xorl	%eax, %eax
	cmpl	$60, %r14d
	sete	%al
	andq	%rcx, %rax
	movslq	48(%rsp), %rcx
	subq	%rcx, %rax
	movq	%rax, %rcx
	movslq	%esi, %rax
	addq	%rcx, %rax
	addq	%rax, %rdx
	movq	%rdx, 120(%rsp)
	jo	.L33
	movq	%rdx, 208(%rsp)
	leaq	208(%rsp), %rdi
	movq	16(%rsp), %rsi
	movq	32(%rsp), %rax
	call	*%rax
	testq	%rax, %rax
	je	.L28
	movq	120(%rsp), %rdx
.L35:
	movdqa	144(%rsp), %xmm0
	movq	192(%rsp), %rax
	movups	%xmm0, 0(%r13)
	movdqa	160(%rsp), %xmm0
	movq	%rax, 48(%r13)
	movups	%xmm0, 16(%r13)
	movdqa	176(%rsp), %xmm0
	movups	%xmm0, 32(%r13)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L33:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rdx
	movl	$75, %fs:(%rax)
.L15:
	addq	$280, %rsp
	movq	%rdx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L62:
	andl	$3, %edx
	xorl	%eax, %eax
	cmpq	$1, %rdx
	sete	%al
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L64:
	movzbl	%cl, %ecx
	cmpl	%r14d, %ecx
	jl	.L20
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L19:
	movl	72(%rsp), %edi
	movl	176(%rsp), %eax
	movl	%r13d, %r14d
	movq	64(%rsp), %r13
	testl	%edi, %edi
	sete	72(%rsp)
	movzbl	72(%rsp), %ebx
	testl	%eax, %eax
	sete	%cl
	cmpb	%cl, %bl
	je	.L21
	testl	%edi, %edi
	js	.L21
	testl	%eax, %eax
	js	.L21
	leaq	208(%rsp), %rax
	movq	%r13, 104(%rsp)
	movq	32(%rsp), %r13
	movl	$601200, %ebp
	movq	%r15, 96(%rsp)
	movq	%rax, 64(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 88(%rsp)
	.p2align 4,,10
	.p2align 3
.L34:
	leaq	136(%rsp), %r12
	movl	%ebp, %ebx
	leal	(%rbp,%rbp), %r15d
	negl	%ebx
	movl	$2, %r14d
	movl	%r14d, %eax
	movq	%r12, 40(%rsp)
	movl	%ebx, %r12d
	movl	%r15d, %r14d
	movq	%rdx, %rbx
	movl	%eax, %r15d
	movslq	%r12d, %rax
	addq	%rax, %rbx
	movq	%rbx, 128(%rsp)
	jno	.L65
.L27:
	addl	%r14d, %r12d
	cmpl	$1, %r15d
	je	.L31
	movq	120(%rsp), %rbx
	movslq	%r12d, %rax
	movl	$1, %r15d
	addq	%rax, %rbx
	movq	%rbx, 128(%rsp)
	jo	.L27
.L65:
	movq	64(%rsp), %rdx
	movq	88(%rsp), %rsi
	movq	%r13, %rdi
	call	ranged_convert
	testq	%rax, %rax
	je	.L28
	movl	240(%rsp), %eax
	testl	%eax, %eax
	sete	%dl
	cmpb	%dl, 72(%rsp)
	je	.L29
	testl	%eax, %eax
	jns	.L27
.L29:
	movl	208(%rsp), %eax
	pushq	%rax
	movl	220(%rsp), %eax
	pushq	%rax
	movl	232(%rsp), %eax
	pushq	%rax
	movl	260(%rsp), %eax
	pushq	%rax
	movl	260(%rsp), %r9d
	movl	80(%rsp), %r8d
	movl	40(%rsp), %ecx
	movl	44(%rsp), %edx
	movq	56(%rsp), %rsi
	movq	128(%rsp), %rdi
	call	ydhms_diff
	addq	$32, %rsp
	addq	128(%rsp), %rax
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rdi
	movq	%rax, %rbx
	movq	%rax, 136(%rsp)
	call	*%r13
	testq	%rax, %rax
	jne	.L66
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$75, %fs:(%rax)
	je	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movq	$-1, %rdx
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L31:
	addl	$601200, %ebp
	cmpl	$269337600, %ebp
	je	.L33
	movq	120(%rsp), %rdx
	jmp	.L34
.L66:
	movq	%rbx, %rdx
	movq	104(%rsp), %r13
	movl	144(%rsp), %r14d
	jmp	.L21
	.size	__mktime_internal, .-__mktime_internal
	.p2align 4,,15
	.globl	__GI_mktime
	.hidden	__GI_mktime
	.type	__GI_mktime, @function
__GI_mktime:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__tzset
	movq	%rbx, %rdi
	leaq	localtime_offset.4530(%rip), %rdx
	leaq	__localtime_r(%rip), %rsi
	popq	%rbx
	jmp	__mktime_internal
	.size	__GI_mktime, .-__GI_mktime
	.weak	__GI_timelocal
	.hidden	__GI_timelocal
	.set	__GI_timelocal,__GI_mktime
	.weak	timelocal
	.set	timelocal,__GI_timelocal
	.globl	mktime
	.set	mktime,__GI_mktime
	.local	localtime_offset.4530
	.comm	localtime_offset.4530,8,8
	.hidden	__mon_yday
	.globl	__mon_yday
	.section	.rodata
	.align 32
	.type	__mon_yday, @object
	.size	__mon_yday, 52
__mon_yday:
	.value	0
	.value	31
	.value	59
	.value	90
	.value	120
	.value	151
	.value	181
	.value	212
	.value	243
	.value	273
	.value	304
	.value	334
	.value	365
	.value	0
	.value	31
	.value	60
	.value	91
	.value	121
	.value	152
	.value	182
	.value	213
	.value	244
	.value	274
	.value	305
	.value	335
	.value	366
	.hidden	__localtime_r
	.hidden	__tzset
