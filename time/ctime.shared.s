	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	ctime
	.type	ctime, @function
ctime:
	subq	$8, %rsp
	call	__GI_localtime
	addq	$8, %rsp
	movq	%rax, %rdi
	jmp	__GI_asctime
	.size	ctime, .-ctime
