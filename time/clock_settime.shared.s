	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __clock_settime,clock_settime@@GLIBC_2.17
	.symver __clock_settime_2,clock_settime@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___clock_settime
	.hidden	__GI___clock_settime
	.type	__GI___clock_settime, @function
__GI___clock_settime:
	cmpq	$999999999, 8(%rsi)
	ja	.L7
	movl	$227, %eax
#APP
# 38 "../sysdeps/unix/sysv/linux/clock_settime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI___clock_settime, .-__GI___clock_settime
	.globl	__clock_settime
	.set	__clock_settime,__GI___clock_settime
	.globl	__clock_settime_2
	.set	__clock_settime_2,__clock_settime
