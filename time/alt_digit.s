	.text
	.p2align 4,,15
	.type	_nl_init_alt_digit, @function
_nl_init_alt_digit:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L17
.L2:
	movl	40(%rbx), %eax
	testl	%eax, %eax
	jne	.L1
	movq	440(%rbp), %rbp
	movl	$1, 40(%rbx)
	testq	%rbp, %rbp
	je	.L1
	movl	$800, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rbx)
	je	.L1
	leaq	800(%rax), %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, (%rbx)
	movq	%rbp, %rdi
	addq	$8, %rbx
	call	strlen@PLT
	cmpq	%r12, %rbx
	leaq	1(%rbp,%rax), %rbp
	jne	.L4
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L17:
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, 40(%rbp)
	je	.L1
	leaq	_nl_cleanup_time(%rip), %rax
	movq	%rax, 32(%rbp)
	jmp	.L2
	.size	_nl_init_alt_digit, .-_nl_init_alt_digit
	.p2align 4,,15
	.globl	_nl_get_alt_digit
	.hidden	_nl_get_alt_digit
	.type	_nl_get_alt_digit, @function
_nl_get_alt_digit:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$99, %edi
	ja	.L26
	movq	440(%rsi), %rax
	cmpb	$0, (%rax)
	je	.L26
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	movq	%rsi, %rbx
	movl	%edi, %ebp
	je	.L20
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L20:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L21
	movl	40(%rax), %edx
	testl	%edx, %edx
	jne	.L22
.L21:
	movq	%rbx, %rdi
	call	_nl_init_alt_digit
	movq	40(%rbx), %rax
	testq	%rax, %rax
	jne	.L22
.L24:
	xorl	%ebx, %ebx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L22:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L24
	movl	%ebp, %edi
	movq	(%rax,%rdi,8), %rbx
.L23:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L18
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L18:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%ebx, %ebx
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	_nl_get_alt_digit, .-_nl_get_alt_digit
	.p2align 4,,15
	.globl	_nl_get_walt_digit
	.hidden	_nl_get_walt_digit
	.type	_nl_get_walt_digit, @function
_nl_get_walt_digit:
	cmpl	$99, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	ja	.L51
	movq	848(%rsi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	je	.L51
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	movq	%rsi, %rbx
	movl	%edi, %r13d
	je	.L42
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L42:
	movq	40(%rbx), %rbp
	testq	%rbp, %rbp
	je	.L69
.L43:
	movl	44(%rbp), %eax
	testl	%eax, %eax
	jne	.L46
	movq	848(%rbx), %rbx
	movl	$1, 44(%rbp)
	testq	%rbx, %rbx
	je	.L46
	movl	$800, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, 32(%rbp)
	je	.L47
	leaq	800(%rax), %r12
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rbx, 0(%rbp)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	addq	$8, %rbp
	call	__wcschr
	cmpq	%r12, %rbp
	leaq	4(%rax), %rbx
	jne	.L48
.L49:
	movq	(%r14,%r13,8), %rbx
.L45:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L40
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L40:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	32(%rbp), %r14
	testq	%r14, %r14
	jne	.L49
.L47:
	xorl	%ebx, %ebx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L69:
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, 40(%rbx)
	je	.L47
	leaq	_nl_cleanup_time(%rip), %rax
	movq	%rax, 32(%rbx)
	jmp	.L43
	.size	_nl_get_walt_digit, .-_nl_get_walt_digit
	.p2align 4,,15
	.globl	_nl_parse_alt_digit
	.hidden	_nl_parse_alt_digit
	.type	_nl_parse_alt_digit, @function
_nl_parse_alt_digit:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rax, (%rsp)
	movq	848(%rsi), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	je	.L104
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	movq	%rsi, %rbx
	movq	%rdi, 8(%rsp)
	je	.L73
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L73:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L74
	movl	40(%rax), %edx
	testl	%edx, %edx
	jne	.L75
.L74:
	movq	%rbx, %rdi
	call	_nl_init_alt_digit
	movq	40(%rbx), %rax
	testq	%rax, %rax
	jne	.L75
.L76:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L104
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L104:
	movl	$-1, %r12d
.L70:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	24(%rax), %r13
	testq	%r13, %r13
	je	.L76
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movl	$-1, %r12d
	.p2align 4,,10
	.p2align 3
.L78:
	movq	0(%r13,%rbx,8), %r14
	movq	%r14, %rdi
	call	strlen
	cmpq	%rbp, %rax
	movq	%rax, %r15
	jbe	.L77
	movq	(%rsp), %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	strncmp
	testl	%eax, %eax
	cmove	%ebx, %r12d
	cmove	%r15, %rbp
.L77:
	addq	$1, %rbx
	cmpq	$100, %rbx
	jne	.L78
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L79
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L79:
	cmpl	$-1, %r12d
	je	.L70
	movq	8(%rsp), %rax
	addq	%rbp, (%rax)
	jmp	.L70
	.size	_nl_parse_alt_digit, .-_nl_parse_alt_digit
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	strncmp
	.hidden	strlen
	.hidden	__wcschr
	.hidden	__libc_setlocale_lock
	.hidden	_nl_cleanup_time
