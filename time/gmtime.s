	.text
	.p2align 4,,15
	.globl	__gmtime_r
	.hidden	__gmtime_r
	.type	__gmtime_r, @function
__gmtime_r:
	movq	(%rdi), %rdi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	__tz_convert
	.size	__gmtime_r, .-__gmtime_r
	.weak	gmtime_r
	.set	gmtime_r,__gmtime_r
	.p2align 4,,15
	.globl	gmtime
	.type	gmtime, @function
gmtime:
	movq	(%rdi), %rdi
	leaq	_tmbuf(%rip), %rdx
	xorl	%esi, %esi
	jmp	__tz_convert
	.size	gmtime, .-gmtime
	.hidden	_tmbuf
	.hidden	__tz_convert
