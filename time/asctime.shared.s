	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"???"
#NO_APP
	.text
	.p2align 4,,15
	.type	asctime_internal, @function
asctime_internal:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	je	.L13
	movl	20(%rdi), %eax
	cmpl	$2147481747, %eax
	jg	.L7
	movq	%rdx, %rbp
	leal	1900(%rax), %edx
	movl	16(%rdi), %eax
	movq	%rsi, %rbx
	movl	(%rdi), %r11d
	movl	4(%rdi), %r10d
	movl	8(%rdi), %esi
	movl	12(%rdi), %r9d
	leaq	.LC0(%rip), %r8
	cmpl	$11, %eax
	ja	.L5
	addl	$14, %eax
	leaq	_nl_C_LC_TIME(%rip), %rcx
	cltq
	movq	64(%rcx,%rax,8), %r8
.L5:
	movslq	24(%rdi), %rax
	leaq	.LC0(%rip), %rcx
	cmpl	$6, %eax
	jbe	.L14
.L6:
	pushq	%rdx
	leaq	format(%rip), %rdx
	pushq	%r11
	pushq	%r10
	pushq	%rsi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI___snprintf
	addq	$32, %rsp
	testl	%eax, %eax
	js	.L10
	cltq
	cmpq	%rbp, %rax
	jnb	.L7
.L3:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	_nl_C_LC_TIME(%rip), %rcx
	movq	64(%rcx,%rax,8), %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$75, %fs:(%rax)
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%ebx, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$22, %fs:(%rax)
	jmp	.L3
	.size	asctime_internal, .-asctime_internal
	.p2align 4,,15
	.globl	__asctime_r
	.hidden	__asctime_r
	.type	__asctime_r, @function
__asctime_r:
	movl	$26, %edx
	jmp	asctime_internal
	.size	__asctime_r, .-__asctime_r
	.weak	asctime_r
	.set	asctime_r,__asctime_r
	.p2align 4,,15
	.globl	__GI_asctime
	.hidden	__GI_asctime
	.type	__GI_asctime, @function
__GI_asctime:
	leaq	result(%rip), %rsi
	movl	$114, %edx
	jmp	asctime_internal
	.size	__GI_asctime, .-__GI_asctime
	.globl	asctime
	.set	asctime,__GI_asctime
	.local	result
	.comm	result,114,32
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	format, @object
	.size	format, 32
format:
	.string	"%.3s %.3s%3d %.2d:%.2d:%.2d %d\n"
	.hidden	_nl_C_LC_TIME
