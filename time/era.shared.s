	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	_nl_init_era_entries.part.0, @function
_nl_init_era_entries.part.0:
	movl	__libc_pthread_functions_init(%rip), %ecx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	testl	%ecx, %ecx
	je	.L2
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 48 "era.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L2:
	movq	40(%rbp), %r13
	testq	%r13, %r13
	je	.L25
.L3:
	movl	16(%r13), %edx
	testl	%edx, %edx
	jne	.L5
	movl	464(%rbp), %r14d
	movq	0(%r13), %rdi
	testq	%r14, %r14
	jne	.L7
	testq	%rdi, %rdi
	je	.L9
	call	free@PLT
	movq	$0, 0(%r13)
.L9:
	movl	$1, 16(%r13)
.L5:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 138 "era.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L7:
	cmpq	8(%r13), %r14
	jne	.L26
	testq	%rdi, %rdi
	je	.L27
.L11:
	leaq	(%r14,%r14,8), %rax
	movq	472(%rbp), %r12
	movq	%rdi, %rbx
	movq	%r14, 8(%r13)
	movq	%rdi, 0(%r13)
	leaq	(%rdi,%rax,8), %rbp
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L29:
	je	.L28
.L13:
	xorl	%eax, %eax
	cmpl	$43, (%rbx)
	setne	%al
	leal	-1(%rax,%rax), %eax
	movl	%eax, 64(%rbx)
.L15:
	leaq	32(%r12), %r14
	addq	$72, %rbx
	movq	%r14, -40(%rbx)
	movq	%r14, %rdi
	call	__GI_strlen@PLT
	leaq	1(%r14,%rax), %r14
	movq	%r14, -32(%rbx)
	movq	%r14, %rdi
	call	__GI_strlen@PLT
	leaq	1(%r14,%rax), %rax
	xorl	%esi, %esi
	movq	%rax, %rdi
	subq	%r12, %rdi
	addq	$3, %rdi
	notq	%rdi
	andl	$3, %edi
	addq	%rax, %rdi
	movq	%rdi, -24(%rbx)
	call	__GI___wcschr
	leaq	4(%rax), %rdi
	xorl	%esi, %esi
	movq	%rdi, -16(%rbx)
	call	__GI___wcschr
	cmpq	%rbx, %rbp
	leaq	4(%rax), %r12
	je	.L9
.L17:
	movdqu	(%r12), %xmm0
	movups	%xmm0, (%rbx)
	movdqu	16(%r12), %xmm0
	movups	%xmm0, 16(%rbx)
	movl	20(%rbx), %eax
	cmpl	%eax, 8(%rbx)
	jge	.L29
.L12:
	xorl	%eax, %eax
	cmpl	$43, (%rbx)
	sete	%al
	leal	-1(%rax,%rax), %eax
	movl	%eax, 64(%rbx)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	(%r14,%r14,8), %rsi
	salq	$3, %rsi
	call	realloc@PLT
	movq	%rax, %rdi
	testq	%rdi, %rdi
	jne	.L11
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	movl	24(%rbx), %eax
	cmpl	%eax, 12(%rbx)
	jl	.L12
	jne	.L13
	movl	28(%rbx), %eax
	cmpl	%eax, 16(%rbx)
	jg	.L13
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, 40(%rbp)
	je	.L5
	leaq	_nl_cleanup_time(%rip), %rax
	movq	%rax, 32(%rbp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	0(%r13), %rdi
	call	free@PLT
	movq	$0, 8(%r13)
	movq	$0, 0(%r13)
	jmp	.L9
	.size	_nl_init_era_entries.part.0, .-_nl_init_era_entries.part.0
	.p2align 4,,15
	.globl	_nl_get_era_entry
	.hidden	_nl_get_era_entry
	.type	_nl_get_era_entry, @function
_nl_get_era_entry:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L31
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	je	.L32
.L33:
	movq	8(%rax), %r8
	movl	20(%rbx), %esi
	movl	16(%rbx), %r9d
	movl	12(%rbx), %r10d
	testq	%r8, %r8
	je	.L34
	movq	(%rax), %rdx
	xorl	%ecx, %ecx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L60:
	je	.L57
.L36:
	cmpl	20(%rdx), %esi
	jle	.L58
.L41:
	cmpl	%edi, %esi
	jl	.L30
.L62:
	je	.L59
.L42:
	addq	$1, %rcx
	addq	$72, %rdx
	cmpq	%r8, %rcx
	je	.L34
.L43:
	movl	8(%rdx), %edi
	movq	%rdx, %rax
	cmpl	%edi, %esi
	jle	.L60
.L35:
	cmpl	20(%rdx), %esi
	jl	.L30
	je	.L61
.L39:
	jle	.L42
	cmpl	%edi, %esi
	jge	.L62
.L30:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L61:
	cmpl	24(%rdx), %r9d
	jl	.L30
	jne	.L39
	cmpl	28(%rdx), %r10d
	jle	.L30
.L40:
	cmpl	28(%rdx), %r10d
	jge	.L41
	addq	$1, %rcx
	addq	$72, %rdx
	cmpq	%r8, %rcx
	jne	.L43
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%eax, %eax
.L63:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	12(%rdx), %r9d
	jl	.L30
	jne	.L42
	cmpl	16(%rdx), %r10d
	jg	.L42
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	12(%rdx), %r9d
	jg	.L35
	jne	.L36
	cmpl	16(%rdx), %r10d
	jge	.L35
	cmpl	20(%rdx), %esi
	jg	.L41
	.p2align 4,,10
	.p2align 3
.L58:
	jne	.L42
	cmpl	24(%rdx), %r9d
	jg	.L41
	jne	.L42
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L32:
	movl	464(%rsi), %edx
	testl	%edx, %edx
	je	.L33
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L31:
	movl	464(%rsi), %eax
	testl	%eax, %eax
	je	.L34
.L44:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	_nl_init_era_entries.part.0
	movq	8(%rsp), %rsi
	movq	40(%rsi), %rax
	testq	%rax, %rax
	jne	.L33
	xorl	%eax, %eax
	jmp	.L63
	.size	_nl_get_era_entry, .-_nl_get_era_entry
	.p2align 4,,15
	.globl	_nl_select_era_entry
	.hidden	_nl_select_era_entry
	.type	_nl_select_era_entry, @function
_nl_select_era_entry:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	movq	40(%rsi), %rax
	testq	%rax, %rax
	je	.L65
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	je	.L66
.L67:
	movslq	%ebx, %rdi
	movq	(%rax), %rax
	leaq	(%rdi,%rdi,8), %rdx
	leaq	(%rax,%rdx,8), %rax
.L64:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movl	464(%rsi), %edx
	testl	%edx, %edx
	je	.L67
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L65:
	movl	464(%rsi), %eax
	testl	%eax, %eax
	je	.L70
.L69:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	_nl_init_era_entries.part.0
	movq	8(%rsp), %rsi
	movq	40(%rsi), %rax
	testq	%rax, %rax
	jne	.L67
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%eax, %eax
	jmp	.L64
	.size	_nl_select_era_entry, .-_nl_select_era_entry
	.hidden	_nl_cleanup_time
	.hidden	__libc_setlocale_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
