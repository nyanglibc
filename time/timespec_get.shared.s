	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__timespec_get
	.type	__timespec_get, @function
__timespec_get:
	cmpl	$1, %esi
	je	.L10
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	subq	$8, %rsp
	movq	%rdi, %rsi
	xorl	%edi, %edi
	call	__GI___clock_gettime
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	__timespec_get, .-__timespec_get
	.globl	timespec_get
	.set	timespec_get,__timespec_get
