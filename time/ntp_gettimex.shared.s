	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ntp_gettimex
	.type	__ntp_gettimex, @function
__ntp_gettimex:
	pushq	%rbx
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$208, %rsp
	movq	%rsp, %rsi
	movl	$0, (%rsp)
	call	__GI___clock_adjtime
	movq	24(%rsp), %rdx
	movq	$0, 40(%rbx)
	movdqu	72(%rsp), %xmm0
	movq	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movq	%rdx, 16(%rbx)
	movq	32(%rsp), %rdx
	movups	%xmm0, (%rbx)
	movq	$0, 64(%rbx)
	movq	%rdx, 24(%rbx)
	movslq	160(%rsp), %rdx
	movq	%rdx, 32(%rbx)
	addq	$208, %rsp
	popq	%rbx
	ret
	.size	__ntp_gettimex, .-__ntp_gettimex
	.globl	ntp_gettimex
	.set	ntp_gettimex,__ntp_gettimex
