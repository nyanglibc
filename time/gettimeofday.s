	.text
	.p2align 4,,15
	.globl	__gettimeofday
	.type	__gettimeofday, @function
__gettimeofday:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	jne	.L14
.L2:
	movq	_dl_vdso_gettimeofday(%rip), %rax
	testq	%rax, %rax
	je	.L7
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	*%rax
	movslq	%eax, %rdx
	cmpq	$-4096, %rdx
	jbe	.L1
	cmpq	$-38, %rdx
	je	.L7
.L4:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
.L6:
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movl	$96, %eax
#APP
# 53 "../sysdeps/unix/sysv/linux/gettimeofday.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movb	$0, (%rsi)
	jmp	.L2
	.size	__gettimeofday, .-__gettimeofday
	.weak	gettimeofday
	.set	gettimeofday,__gettimeofday
