	.text
	.p2align 4,,15
	.globl	time
	.type	time, @function
time:
	movq	_dl_vdso_time(%rip), %rax
	pushq	%rbx
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L5
	call	*%rax
	cmpq	$-4096, %rax
	jbe	.L1
	cmpq	$-38, %rax
	jne	.L3
.L5:
	movq	%rbx, %rdi
	movl	$201, %eax
#APP
# 46 "../sysdeps/unix/sysv/linux/time.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L3
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	popq	%rbx
	ret
	.size	time, .-time
