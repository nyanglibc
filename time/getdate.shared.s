	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"DATEMSK"
.LC1:
	.string	"rce"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__getdate_r
	.hidden	__getdate_r
	.type	__getdate_r, @function
__getdate_r:
.LFB74:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	movq	%rsi, %rbx
	subq	$344, %rsp
	call	__GI_getenv
	movq	%rax, %r13
	movl	$1, %eax
	testq	%r13, %r13
	je	.L1
	cmpb	$0, 0(%r13)
	jne	.L96
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	leaq	-192(%rbp), %rsi
	movq	%r13, %rdi
	call	__GI___stat64
	testl	%eax, %eax
	js	.L52
	movl	-168(%rbp), %edx
	movl	$4, %eax
	andl	$61440, %edx
	cmpl	$32768, %edx
	jne	.L1
	movl	$4, %esi
	movq	%r13, %rdi
	call	__GI___access
	testl	%eax, %eax
	js	.L4
	leaq	.LC1(%rip), %rsi
	movq	%r13, %rdi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L4
	orl	$32768, (%rax)
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r15
	movsbq	(%r12), %rax
	testb	$32, 1(%r15,%rax,2)
	je	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %r12
	movsbq	(%r12), %rax
	testb	$32, 1(%r15,%rax,2)
	jne	.L6
.L5:
	movq	%r12, %rdi
	call	__GI_strlen
	testq	%rax, %rax
	je	.L55
	movsbq	-1(%r12,%rax), %rdx
	leaq	-1(%rax), %r13
	testb	$32, 1(%r15,%rdx,2)
	jne	.L8
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L10:
	movsbq	-1(%r12,%r13), %rdx
	leaq	-1(%r13), %rcx
	testb	$32, 1(%r15,%rdx,2)
	je	.L9
	movq	%rcx, %r13
.L8:
	testq	%r13, %r13
	jne	.L10
	movl	$1, %r15d
.L11:
	addq	$30, %r15
	movq	%r12, %rsi
	movq	%r13, %rdx
	andq	$-16, %r15
	subq	%r15, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	movq	%rcx, %rdi
	call	__GI_memcpy@PLT
	movq	$0, -376(%rbp)
	movb	$0, (%rax,%r13)
	movq	%rax, %r12
.L7:
	leaq	-336(%rbp), %rax
	movq	$0, -344(%rbp)
	movq	$0, -336(%rbp)
	xorl	%r15d, %r15d
	movabsq	$-9223372034707292160, %r13
	movq	%rax, -368(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, -360(%rbp)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L97:
	movq	-344(%rbp), %rsi
	leaq	-1(%rsi,%rax), %rax
	cmpb	$10, (%rax)
	jne	.L15
	movb	$0, (%rax)
	movq	-344(%rbp), %rsi
.L15:
	movq	%r13, (%rbx)
	movq	%r13, 8(%rbx)
	movq	%rbx, %rdx
	movq	%r13, 16(%rbx)
	movl	$-2147483648, 24(%rbx)
	movq	%r12, %rdi
	movl	$-1, 32(%rbx)
	movq	$0, 40(%rbx)
	movq	$0, 48(%rbx)
	call	__GI_strptime
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L16
	cmpb	$0, (%rax)
	je	.L17
.L16:
	testb	$16, (%r14)
	jne	.L14
.L18:
	movq	-368(%rbp), %rsi
	movq	-360(%rbp), %rdi
	movq	%r14, %rdx
	call	__getline
	testq	%rax, %rax
	jns	.L97
.L14:
	movq	-376(%rbp), %rdi
	call	free@PLT
	movq	-344(%rbp), %rdi
	call	free@PLT
	testb	$32, (%r14)
	jne	.L47
	movq	%r14, %rdi
	call	_IO_new_fclose@PLT
	testq	%r15, %r15
	je	.L21
.L20:
	cmpb	$0, (%r15)
	jne	.L21
	leaq	-256(%rbp), %r13
	movl	$5, %edi
	movq	%r13, %rsi
	call	__GI___clock_gettime
	movq	-256(%rbp), %rax
	leaq	-320(%rbp), %rsi
	leaq	-328(%rbp), %rdi
	movq	%rax, -328(%rbp)
	call	__localtime_r
	movl	24(%rbx), %r12d
	xorl	%esi, %esi
	cmpl	$6, %r12d
	ja	.L22
	movabsq	$-9223372034707292160, %rax
	cmpq	%rax, 16(%rbx)
	je	.L98
.L22:
	movl	16(%rbx), %edx
	movl	20(%rbx), %ecx
	cmpl	$11, %edx
	ja	.L24
	cmpl	$-2147483648, 12(%rbx)
	je	.L99
.L24:
	movl	8(%rbx), %eax
	cmpl	$-2147483648, %eax
	je	.L27
	movl	4(%rbx), %r9d
	movl	(%rbx), %r8d
	movl	%eax, %edi
.L28:
	cmpl	$-2147483648, %r9d
	jne	.L31
	movl	$0, 4(%rbx)
.L31:
	cmpl	$-2147483648, %r8d
	jne	.L32
	movl	$0, (%rbx)
.L32:
	cmpl	$23, %edi
	ja	.L33
	cmpl	$-2147483648, %edx
	je	.L100
	cmpl	$-2147483648, %ecx
	je	.L49
.L40:
	testl	%esi, %esi
	jne	.L38
	movl	16(%rbx), %ecx
	cmpl	$11, %ecx
	ja	.L41
	movl	$1, %eax
	movl	12(%rbx), %esi
	salq	%cl, %rax
	testl	$2773, %eax
	jne	.L42
	testl	$1320, %eax
	jne	.L43
	testb	$2, %al
	je	.L41
	testl	%esi, %esi
	jle	.L41
	movl	20(%rbx), %eax
	leal	1900(%rax), %ecx
	movl	$28, %eax
	testb	$3, %cl
	jne	.L46
	movl	%ecx, %eax
	movl	$100, %edi
	cltd
	idivl	%edi
	movl	$29, %eax
	testl	%edx, %edx
	jne	.L46
	movl	%ecx, %eax
	movl	$400, %edi
	cltd
	idivl	%edi
	cmpl	$1, %edx
	sbbl	%eax, %eax
	notl	%eax
	addl	$29, %eax
.L46:
	cmpl	%eax, %esi
	jle	.L38
.L41:
	movl	$8, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$3, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$2, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	%r13, %rax
	jbe	.L55
	leaq	1(%r13), %r15
	movq	%r15, %rdi
	call	__GI___libc_alloca_cutoff
	testl	%eax, %eax
	jne	.L11
	cmpq	$4096, %r15
	jbe	.L11
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -376(%rbp)
	je	.L101
	movq	-376(%rbp), %r15
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r15, %rdi
	movq	%r15, %r12
	call	__GI_memcpy@PLT
	movb	$0, (%r15,%r13)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L21:
	movl	$7, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L55:
	movq	$0, -376(%rbp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r14, %rdi
	call	_IO_new_fclose@PLT
	movl	$5, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movq	-376(%rbp), %rdi
	call	free@PLT
	movq	-344(%rbp), %rdi
	call	free@PLT
	testb	$32, (%r14)
	jne	.L47
	movq	%r14, %rdi
	call	_IO_new_fclose@PLT
	jmp	.L20
.L42:
	subl	$1, %esi
	cmpl	$30, %esi
	ja	.L41
.L38:
	movq	%rbx, %rdi
	call	__GI_mktime
	movq	%rax, %rdx
	xorl	%eax, %eax
	cmpq	$-1, %rdx
	jne	.L1
	jmp	.L41
.L33:
	cmpl	$-2147483648, %ecx
	je	.L49
.L39:
	cmpl	$-2147483648, %edx
	jne	.L40
.L94:
	movl	-304(%rbp), %edx
.L37:
	movl	%edx, 16(%rbx)
	jmp	.L40
.L27:
	movabsq	$-9223372034707292160, %rax
	cmpq	%rax, (%rbx)
	je	.L29
	movl	4(%rbx), %r9d
	movl	(%rbx), %r8d
.L30:
	movl	$0, 8(%rbx)
	xorl	%edi, %edi
	xorl	%eax, %eax
	jmp	.L28
.L49:
	movl	-300(%rbp), %eax
	movl	%eax, 20(%rbx)
	jmp	.L39
.L98:
	cmpl	$-2147483648, 12(%rbx)
	je	.L102
	movl	16(%rbx), %edx
	movl	20(%rbx), %ecx
	jmp	.L24
.L100:
	cmpl	$-2147483648, 12(%rbx)
	je	.L103
.L35:
	cmpl	$-2147483648, %ecx
	jne	.L94
	movl	-300(%rbp), %eax
	movl	%eax, 20(%rbx)
	jmp	.L94
.L99:
	cmpl	$-2147483648, %ecx
	je	.L104
.L25:
	cmpl	$-2147483648, %r12d
	movl	$1, %eax
	je	.L26
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	$0, -208(%rbp)
	movaps	%xmm0, -240(%rbp)
	movl	%ecx, -236(%rbp)
	movl	%edx, -240(%rbp)
	movaps	%xmm0, -256(%rbp)
	movaps	%xmm0, -224(%rbp)
	movl	$1, -244(%rbp)
	call	__GI_mktime
	movl	%r12d, %ecx
	subl	-232(%rbp), %ecx
	movl	$-1840700269, %edx
	addl	$7, %ecx
	movl	%ecx, %eax
	imull	%edx
	leal	(%rdx,%rcx), %eax
	movl	%ecx, %edx
	sarl	$31, %edx
	sarl	$2, %eax
	subl	%edx, %eax
	leal	0(,%rax,8), %edx
	subl	%eax, %edx
	subl	%edx, %ecx
	movl	16(%rbx), %edx
	leal	1(%rcx), %eax
	movl	20(%rbx), %ecx
.L26:
	movl	%eax, 12(%rbx)
	movl	$1, %esi
	jmp	.L24
.L29:
	movl	-312(%rbp), %eax
	movl	-316(%rbp), %r9d
	movl	-320(%rbp), %r8d
	cmpl	$-2147483648, %eax
	movl	%eax, 8(%rbx)
	movl	%r9d, 4(%rbx)
	movl	%r8d, (%rbx)
	je	.L30
	movl	%eax, %edi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L43:
	subl	$1, %esi
	cmpl	$29, %esi
	ja	.L41
	jmp	.L38
.L102:
	movl	-300(%rbp), %eax
	movl	-304(%rbp), %edx
	movl	$7, %ecx
	movl	$1, %esi
	movl	%eax, 20(%rbx)
	movl	%r12d, %eax
	subl	-296(%rbp), %eax
	movl	%edx, 16(%rbx)
	addl	$7, %eax
	cltd
	idivl	%ecx
	movl	-308(%rbp), %eax
	addl	%edx, %eax
	movl	%eax, 12(%rbx)
	jmp	.L22
.L104:
	movl	%edx, %ecx
	subl	-304(%rbp), %ecx
	shrl	$31, %ecx
	addl	-300(%rbp), %ecx
	movl	%ecx, 20(%rbx)
	jmp	.L25
.L103:
	cmpl	$-2147483648, 24(%rbx)
	jne	.L35
	subl	-312(%rbp), %eax
	movl	-304(%rbp), %edx
	movl	%edx, 16(%rbx)
	shrl	$31, %eax
	addl	-308(%rbp), %eax
	cmpl	$-2147483648, %ecx
	movl	%eax, 12(%rbx)
	jne	.L38
	movl	-300(%rbp), %eax
	cmpl	$-2147483648, %edx
	movl	%eax, 20(%rbx)
	jne	.L38
	movl	$1, %esi
	jmp	.L37
.L101:
	movq	%r14, %rdi
	call	_IO_new_fclose@PLT
	movl	$6, %eax
	jmp	.L1
.LFE74:
	.size	__getdate_r, .-__getdate_r
	.weak	getdate_r
	.set	getdate_r,__getdate_r
	.p2align 4,,15
	.globl	getdate
	.type	getdate, @function
getdate:
.LFB75:
	leaq	tmbuf.10015(%rip), %rsi
	subq	$8, %rsp
	call	__getdate_r
	testl	%eax, %eax
	leaq	tmbuf.10015(%rip), %rdx
	jne	.L111
.L105:
	movq	%rdx, %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	movq	getdate_err@GOTPCREL(%rip), %rdx
	movl	%eax, (%rdx)
	xorl	%edx, %edx
	jmp	.L105
.LFE75:
	.size	getdate, .-getdate
	.local	tmbuf.10015
	.comm	tmbuf.10015,56,32
	.comm	getdate_err,4,4
	.hidden	__localtime_r
	.hidden	__getline
