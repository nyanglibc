	.text
	.p2align 4,,15
	.globl	__clock_settime
	.hidden	__clock_settime
	.type	__clock_settime, @function
__clock_settime:
	cmpq	$999999999, 8(%rsi)
	ja	.L7
	movl	$227, %eax
#APP
# 38 "../sysdeps/unix/sysv/linux/clock_settime.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__clock_settime, .-__clock_settime
	.weak	clock_settime
	.set	clock_settime,__clock_settime
