	.text
	.p2align 4,,15
	.globl	ctime
	.type	ctime, @function
ctime:
	subq	$8, %rsp
	call	localtime
	addq	$8, %rsp
	movq	%rax, %rdi
	jmp	asctime
	.size	ctime, .-ctime
	.hidden	asctime
	.hidden	localtime
