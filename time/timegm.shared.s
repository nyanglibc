	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	timegm
	.type	timegm, @function
timegm:
	leaq	gmtime_offset.2323(%rip), %rdx
	leaq	__GI___gmtime_r(%rip), %rsi
	movl	$0, 32(%rdi)
	jmp	__mktime_internal
	.size	timegm, .-timegm
	.local	gmtime_offset.2323
	.comm	gmtime_offset.2323,8,8
	.hidden	__mktime_internal
