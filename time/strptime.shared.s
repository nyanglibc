	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_strptime
	.hidden	__GI_strptime
	.type	__GI_strptime, @function
__GI_strptime:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movq	%fs:(%rax), %r8
	jmp	__strptime_internal
	.size	__GI_strptime, .-__GI_strptime
	.globl	strptime
	.set	strptime,__GI_strptime
	.hidden	__strptime_internal
