	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	time_syscall, @function
time_syscall:
	movl	$201, %eax
#APP
# 32 "../sysdeps/unix/sysv/linux/time.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	time_syscall, .-time_syscall
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"LINUX_2.6"
.LC1:
	.string	"__vdso_time"
	.text
	.p2align 4,,15
	.type	time_ifunc, @function
time_ifunc:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	680(%rax), %rsi
	testq	%rsi, %rsi
	je	.L24
	subq	$88, %rsp
	pxor	%xmm0, %xmm0
	leaq	16(%rsp), %rdx
	leaq	.LC0(%rip), %rdi
	movabsq	$4356732406, %rcx
	movq	%rcx, 56(%rsp)
	movq	$0, 32(%rsp)
	leaq	928(%rsi), %rcx
	movaps	%xmm0, 16(%rsp)
	movq	%rdi, 48(%rsp)
	movq	%rdx, 8(%rsp)
	leaq	8(%rsp), %rdx
	xorl	%r9d, %r9d
	leaq	.LC1(%rip), %rdi
	movb	$32, 20(%rsp)
	movq	$0, 64(%rsp)
	pushq	$0
	pushq	$0
	leaq	64(%rsp), %r8
	call	*752(%rax)
	movq	24(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	testq	%rdx, %rdx
	je	.L8
	cmpw	$-15, 6(%rdx)
	je	.L10
	testq	%rax, %rax
	je	.L10
	movq	(%rax), %rax
.L9:
	addq	8(%rdx), %rax
	leaq	time_syscall(%rip), %rdx
	cmove	%rdx, %rax
.L5:
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	time_syscall(%rip), %rax
	jmp	.L5
.L24:
	leaq	time_syscall(%rip), %rax
	ret
	.size	time_ifunc, .-time_ifunc
	.globl	time
	.type	time, @gnu_indirect_function
	.set	time,time_ifunc
