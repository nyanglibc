	.text
	.p2align 4,,15
	.globl	__clock_getcpuclockid
	.type	__clock_getcpuclockid, @function
__clock_getcpuclockid:
	notl	%edi
	movl	$229, %ecx
	movq	%rsi, %r8
	leal	2(,%rdi,8), %edx
	xorl	%esi, %esi
	movl	%ecx, %eax
	movl	%edx, %edi
#APP
# 37 "../sysdeps/unix/sysv/linux/clock_getcpuclockid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	%rax, %rsi
	je	.L6
	negl	%eax
	movl	$3, %edx
	cmpl	$-22, %esi
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, (%r8)
	ret
	.size	__clock_getcpuclockid, .-__clock_getcpuclockid
	.weak	clock_getcpuclockid
	.set	clock_getcpuclockid,__clock_getcpuclockid
