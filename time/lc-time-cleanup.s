	.text
	.p2align 4,,15
	.globl	_nl_cleanup_time
	.hidden	_nl_cleanup_time
	.type	_nl_cleanup_time, @function
_nl_cleanup_time:
	pushq	%rbx
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L1
	movq	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	(%rbx), %rdi
	call	free@PLT
	movq	24(%rbx), %rdi
	call	free@PLT
	movq	32(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	popq	%rbx
	ret
	.size	_nl_cleanup_time, .-_nl_cleanup_time
