	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__difftime
	.type	__difftime, @function
__difftime:
	movq	%rdi, -32(%rsp)
	movq	%rsi, -24(%rsp)
	fildq	-32(%rsp)
	fildq	-24(%rsp)
	fsubrp	%st, %st(1)
	fstpl	-16(%rsp)
	movsd	-16(%rsp), %xmm0
	ret
	.size	__difftime, .-__difftime
	.globl	difftime
	.set	difftime,__difftime
