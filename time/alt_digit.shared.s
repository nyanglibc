	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	_nl_init_alt_digit, @function
_nl_init_alt_digit:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	40(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L17
.L2:
	movl	40(%rbx), %eax
	testl	%eax, %eax
	jne	.L1
	movq	440(%rbp), %rbp
	movl	$1, 40(%rbx)
	testq	%rbp, %rbp
	je	.L1
	movl	$800, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rbx)
	je	.L1
	leaq	800(%rax), %r12
	movq	%rax, %rbx
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, (%rbx)
	movq	%rbp, %rdi
	addq	$8, %rbx
	call	__GI_strlen@PLT
	cmpq	%r12, %rbx
	leaq	1(%rbp,%rax), %rbp
	jne	.L4
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L17:
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, 40(%rbp)
	je	.L1
	leaq	_nl_cleanup_time(%rip), %rax
	movq	%rax, 32(%rbp)
	jmp	.L2
	.size	_nl_init_alt_digit, .-_nl_init_alt_digit
	.p2align 4,,15
	.globl	_nl_get_alt_digit
	.hidden	_nl_get_alt_digit
	.type	_nl_get_alt_digit, @function
_nl_get_alt_digit:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$99, %edi
	ja	.L26
	movq	440(%rsi), %rax
	cmpb	$0, (%rax)
	je	.L26
	movl	__libc_pthread_functions_init(%rip), %ecx
	movq	%rsi, %rbx
	movl	%edi, %ebp
	testl	%ecx, %ecx
	je	.L20
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 79 "alt_digit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L20:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L21
	movl	40(%rax), %edx
	testl	%edx, %edx
	jne	.L22
.L21:
	movq	%rbx, %rdi
	call	_nl_init_alt_digit
	movq	40(%rbx), %rax
	testq	%rax, %rax
	jne	.L22
.L24:
	xorl	%ebx, %ebx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L22:
	movq	24(%rax), %rax
	testq	%rax, %rax
	je	.L24
	movl	%ebp, %edi
	movq	(%rax,%rdi,8), %rbx
.L23:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L18
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 90 "alt_digit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L18:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%ebx, %ebx
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	_nl_get_alt_digit, .-_nl_get_alt_digit
	.p2align 4,,15
	.globl	_nl_get_walt_digit
	.hidden	_nl_get_walt_digit
	.type	_nl_get_walt_digit, @function
_nl_get_walt_digit:
	cmpl	$99, %edi
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	ja	.L45
	movq	848(%rsi), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	je	.L45
	movl	__libc_pthread_functions_init(%rip), %ecx
	movq	%rsi, %rbx
	movl	%edi, %r13d
	testl	%ecx, %ecx
	je	.L36
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 105 "alt_digit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L36:
	movq	40(%rbx), %rbp
	testq	%rbp, %rbp
	je	.L57
.L37:
	movl	44(%rbp), %edx
	testl	%edx, %edx
	jne	.L40
	movq	848(%rbx), %rbx
	movl	$1, 44(%rbp)
	testq	%rbx, %rbx
	je	.L40
	movl	$800, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, 32(%rbp)
	je	.L41
	leaq	800(%rax), %r12
	movq	%rax, %rbp
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rbx, 0(%rbp)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	addq	$8, %rbp
	call	__GI___wcschr
	cmpq	%r12, %rbp
	leaq	4(%rax), %rbx
	jne	.L42
.L43:
	movq	(%r14,%r13,8), %rbx
.L39:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L34
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 142 "alt_digit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L34:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	32(%rbp), %r14
	testq	%r14, %r14
	jne	.L43
.L41:
	xorl	%ebx, %ebx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L45:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, 40(%rbx)
	je	.L41
	leaq	_nl_cleanup_time(%rip), %rax
	movq	%rax, 32(%rbx)
	jmp	.L37
	.size	_nl_get_walt_digit, .-_nl_get_walt_digit
	.p2align 4,,15
	.globl	_nl_parse_alt_digit
	.hidden	_nl_parse_alt_digit
	.type	_nl_parse_alt_digit, @function
_nl_parse_alt_digit:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	(%rdi), %rax
	movq	%rax, (%rsp)
	movq	848(%rsi), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	je	.L69
	movq	%rsi, %rbx
	movl	__libc_pthread_functions_init(%rip), %esi
	movq	%rdi, 8(%rsp)
	testl	%esi, %esi
	je	.L61
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 159 "alt_digit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L61:
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.L62
	movl	40(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L63
.L62:
	movq	%rbx, %rdi
	call	_nl_init_alt_digit
	movq	40(%rbx), %rax
	testq	%rax, %rax
	jne	.L63
.L64:
	movl	__libc_pthread_functions_init(%rip), %eax
	xorl	%ebp, %ebp
	movl	$-1, %r12d
	testl	%eax, %eax
	je	.L69
.L68:
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 184 "alt_digit.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L67:
	cmpl	$-1, %r12d
	je	.L58
	movq	8(%rsp), %rax
	addq	%rbp, (%rax)
.L58:
	addq	$24, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movq	24(%rax), %r13
	testq	%r13, %r13
	je	.L64
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movl	$-1, %r12d
	.p2align 4,,10
	.p2align 3
.L66:
	movq	0(%r13,%rbx,8), %r14
	movq	%r14, %rdi
	call	__GI_strlen
	cmpq	%rbp, %rax
	movq	%rax, %r15
	jbe	.L65
	movq	(%rsp), %rsi
	movq	%rax, %rdx
	movq	%r14, %rdi
	call	__GI_strncmp
	testl	%eax, %eax
	cmove	%ebx, %r12d
	cmove	%r15, %rbp
.L65:
	addq	$1, %rbx
	cmpq	$100, %rbx
	jne	.L66
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	jne	.L68
	jmp	.L67
.L69:
	movl	$-1, %r12d
	jmp	.L58
	.size	_nl_parse_alt_digit, .-_nl_parse_alt_digit
	.hidden	__libc_setlocale_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	_nl_cleanup_time
