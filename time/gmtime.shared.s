	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___gmtime_r
	.hidden	__GI___gmtime_r
	.type	__GI___gmtime_r, @function
__GI___gmtime_r:
	movq	(%rdi), %rdi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	__tz_convert
	.size	__GI___gmtime_r, .-__GI___gmtime_r
	.globl	__gmtime_r
	.set	__gmtime_r,__GI___gmtime_r
	.weak	gmtime_r
	.set	gmtime_r,__gmtime_r
	.p2align 4,,15
	.globl	gmtime
	.type	gmtime, @function
gmtime:
	movq	(%rdi), %rdi
	leaq	_tmbuf(%rip), %rdx
	xorl	%esi, %esi
	jmp	__tz_convert
	.size	gmtime, .-gmtime
	.hidden	_tmbuf
	.hidden	__tz_convert
