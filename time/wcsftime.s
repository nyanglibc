	.text
	.p2align 4,,15
	.globl	wcsftime
	.hidden	wcsftime
	.type	wcsftime, @function
wcsftime:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r8
	jmp	__wcsftime_l
	.size	wcsftime, .-wcsftime
	.hidden	__wcsftime_l
