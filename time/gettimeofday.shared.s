	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	__gettimeofday_syscall, @function
__gettimeofday_syscall:
	testq	%rsi, %rsi
	jne	.L9
.L2:
	movl	$96, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/gettimeofday.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
	rep ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movb	$0, (%rsi)
	jmp	.L2
	.size	__gettimeofday_syscall, .-__gettimeofday_syscall
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"LINUX_2.6"
.LC1:
	.string	"__vdso_gettimeofday"
	.text
	.p2align 4,,15
	.type	__gettimeofday_ifunc, @function
__gettimeofday_ifunc:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	680(%rax), %rsi
	testq	%rsi, %rsi
	je	.L30
	subq	$88, %rsp
	pxor	%xmm0, %xmm0
	leaq	16(%rsp), %rdx
	leaq	.LC0(%rip), %rdi
	movabsq	$4356732406, %rcx
	movq	%rcx, 56(%rsp)
	movq	$0, 32(%rsp)
	leaq	928(%rsi), %rcx
	movaps	%xmm0, 16(%rsp)
	movq	%rdi, 48(%rsp)
	movq	%rdx, 8(%rsp)
	leaq	8(%rsp), %rdx
	xorl	%r9d, %r9d
	leaq	.LC1(%rip), %rdi
	movb	$32, 20(%rsp)
	movq	$0, 64(%rsp)
	pushq	$0
	pushq	$0
	leaq	64(%rsp), %r8
	call	*752(%rax)
	movq	24(%rsp), %rdx
	popq	%rcx
	popq	%rsi
	testq	%rdx, %rdx
	je	.L14
	cmpw	$-15, 6(%rdx)
	je	.L16
	testq	%rax, %rax
	je	.L16
	movq	(%rax), %rax
.L15:
	addq	8(%rdx), %rax
	leaq	__gettimeofday_syscall(%rip), %rdx
	cmove	%rdx, %rax
.L11:
	addq	$88, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	__gettimeofday_syscall(%rip), %rax
	jmp	.L11
.L30:
	leaq	__gettimeofday_syscall(%rip), %rax
	ret
	.size	__gettimeofday_ifunc, .-__gettimeofday_ifunc
	.globl	__gettimeofday
	.type	__gettimeofday, @gnu_indirect_function
	.set	__gettimeofday,__gettimeofday_ifunc
	.weak	gettimeofday
	.set	gettimeofday,__gettimeofday
