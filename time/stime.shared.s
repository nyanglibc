	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __stime,stime@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__stime
	.type	__stime, @function
__stime:
	subq	$24, %rsp
	movq	(%rdi), %rax
	xorl	%edi, %edi
	movq	%rsp, %rsi
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	call	__GI___clock_settime
	addq	$24, %rsp
	ret
	.size	__stime, .-__stime
