	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__adjtime
	.type	__adjtime, @function
__adjtime:
	pushq	%rbx
	subq	$208, %rsp
	testq	%rdi, %rdi
	je	.L2
	movq	8(%rdi), %r8
	movabsq	$4835703278458516699, %rdx
	movq	(%rdi), %rcx
	movq	%r8, %rax
	imulq	%rdx
	movq	%r8, %rax
	sarq	$63, %rax
	sarq	$18, %rdx
	subq	%rax, %rdx
	addq	%rdx, %rcx
	imulq	$1000000, %rdx, %rdx
	leaq	2145(%rcx), %rax
	subq	%rdx, %r8
	cmpq	$4290, %rax
	ja	.L12
	imulq	$1000000, %rcx, %rcx
	movl	$32769, (%rsp)
	addq	%r8, %rcx
	movq	%rcx, 8(%rsp)
.L5:
	xorl	%edi, %edi
	movq	%rsi, %rbx
	movq	%rsp, %rsi
	call	__GI___clock_adjtime
	testl	%eax, %eax
	movl	$-1, %r8d
	js	.L1
	xorl	%r8d, %r8d
	testq	%rbx, %rbx
	je	.L1
	movq	8(%rsp), %rcx
	movabsq	$4835703278458516699, %rdi
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rcx, %rax
	sarq	$63, %rax
	sarq	$18, %rdx
	movq	%rdx, %rsi
	subq	%rax, %rsi
	testq	%rcx, %rcx
	js	.L13
	imulq	$1000000, %rsi, %rax
	movq	%rsi, (%rbx)
	subq	%rax, %rcx
	movq	%rcx, 8(%rbx)
.L1:
	addq	$208, %rsp
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	negq	%rcx
	movq	%rsi, (%rbx)
	movq	%rcx, %rax
	mulq	%rdi
	movl	%r8d, %eax
	movq	%rdx, %rdi
	shrq	$18, %rdi
	imulq	$1000000, %rdi, %rdi
	subq	%rcx, %rdi
	movq	%rdi, 8(%rbx)
	addq	$208, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$40961, (%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %r8d
	movl	$22, %fs:(%rax)
	jmp	.L1
	.size	__adjtime, .-__adjtime
	.weak	adjtime
	.set	adjtime,__adjtime
