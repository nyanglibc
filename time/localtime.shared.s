	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__localtime_r
	.hidden	__localtime_r
	.type	__localtime_r, @function
__localtime_r:
	movq	(%rdi), %rdi
	movq	%rsi, %rdx
	movl	$1, %esi
	jmp	__tz_convert
	.size	__localtime_r, .-__localtime_r
	.weak	localtime_r
	.set	localtime_r,__localtime_r
	.p2align 4,,15
	.globl	__GI_localtime
	.hidden	__GI_localtime
	.type	__GI_localtime, @function
__GI_localtime:
	movq	(%rdi), %rdi
	leaq	_tmbuf(%rip), %rdx
	movl	$1, %esi
	jmp	__tz_convert
	.size	__GI_localtime, .-__GI_localtime
	.globl	localtime
	.set	localtime,__GI_localtime
	.hidden	_tmbuf
	.comm	_tmbuf,56,32
	.hidden	__tz_convert
