	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ntp_gettime
	.type	__ntp_gettime, @function
__ntp_gettime:
	pushq	%rbx
	movq	%rdi, %rbx
	xorl	%edi, %edi
	subq	$208, %rsp
	movq	%rsp, %rsi
	movl	$0, (%rsp)
	call	__GI___clock_adjtime
	movq	24(%rsp), %rdx
	movdqu	72(%rsp), %xmm0
	movq	%rdx, 16(%rbx)
	movq	32(%rsp), %rdx
	movups	%xmm0, (%rbx)
	movq	%rdx, 24(%rbx)
	movslq	160(%rsp), %rdx
	movq	%rdx, 32(%rbx)
	addq	$208, %rsp
	popq	%rbx
	ret
	.size	__ntp_gettime, .-__ntp_gettime
	.globl	ntp_gettime
	.set	ntp_gettime,__ntp_gettime
