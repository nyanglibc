	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	dysize
	.type	dysize, @function
dysize:
	testb	$3, %dil
	movl	$365, %eax
	jne	.L1
	movl	%edi, %eax
	movl	$1374389535, %edx
	movl	%edi, %esi
	imull	%edx
	sarl	$31, %esi
	movl	$366, %eax
	movl	%edx, %ecx
	sarl	$5, %ecx
	subl	%esi, %ecx
	imull	$100, %ecx, %ecx
	cmpl	%ecx, %edi
	jne	.L1
	sarl	$7, %edx
	xorl	%eax, %eax
	subl	%esi, %edx
	imull	$400, %edx, %edx
	cmpl	%edx, %edi
	sete	%al
	addl	$365, %eax
.L1:
	rep ret
	.size	dysize, .-dysize
