	.text
	.p2align 4,,15
	.globl	__settimeofday
	.type	__settimeofday, @function
__settimeofday:
	subq	$24, %rsp
	testq	%rsi, %rsi
	jne	.L7
	imulq	$1000, 8(%rdi), %rax
	movq	(%rdi), %rdx
	movq	%rsp, %rsi
	xorl	%edi, %edi
	movq	%rdx, (%rsp)
	movq	%rax, 8(%rsp)
	call	__clock_settime
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	testq	%rdi, %rdi
	jne	.L8
	movq	%rsi, %rdi
	call	__settimezone
	jmp	.L1
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.size	__settimeofday, .-__settimeofday
	.weak	settimeofday
	.set	settimeofday,__settimeofday
	.hidden	__settimezone
	.hidden	__clock_settime
