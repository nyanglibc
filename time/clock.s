	.text
	.p2align 4,,15
	.globl	clock
	.type	clock, @function
clock:
	subq	$24, %rsp
	movl	$2, %edi
	movq	%rsp, %rsi
	call	__clock_gettime
	testl	%eax, %eax
	movq	$-1, %rdx
	jne	.L1
	movq	8(%rsp), %rsi
	movabsq	$2361183241434822607, %rdx
	imulq	$1000000, (%rsp), %rcx
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rsi, %rdx
	addq	%rcx, %rdx
.L1:
	movq	%rdx, %rax
	addq	$24, %rsp
	ret
	.size	clock, .-clock
	.hidden	__clock_gettime
