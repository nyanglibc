	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __clock_nanosleep,clock_nanosleep@@GLIBC_2.17
	.symver __clock_nanosleep_2,clock_nanosleep@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___clock_nanosleep
	.hidden	__GI___clock_nanosleep
	.type	__GI___clock_nanosleep, @function
__GI___clock_nanosleep:
	cmpl	$3, %edi
	je	.L6
	cmpl	$2, %edi
	movl	$-6, %eax
	movq	%rcx, %r10
	cmove	%eax, %edi
#APP
# 43 "../sysdeps/unix/sysv/linux/clock_nanosleep.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L14
	movl	$230, %eax
#APP
# 43 "../sysdeps/unix/sysv/linux/clock_nanosleep.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	subq	$40, %rsp
	movq	%rdx, 16(%rsp)
	movl	%esi, 12(%rsp)
	movl	%edi, (%rsp)
	movq	%rcx, 24(%rsp)
	call	__libc_enable_asynccancel
	movq	24(%rsp), %r10
	movl	%eax, %r8d
	movq	16(%rsp), %rdx
	movl	12(%rsp), %esi
	movl	(%rsp), %edi
	movl	$230, %eax
#APP
# 43 "../sysdeps/unix/sysv/linux/clock_nanosleep.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%r8d, %edi
	movq	%rax, (%rsp)
	call	__libc_disable_asynccancel
	movq	(%rsp), %rax
	addq	$40, %rsp
	negl	%eax
	ret
	.size	__GI___clock_nanosleep, .-__GI___clock_nanosleep
	.globl	__clock_nanosleep
	.set	__clock_nanosleep,__GI___clock_nanosleep
	.globl	__clock_nanosleep_2
	.set	__clock_nanosleep_2,__clock_nanosleep
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
