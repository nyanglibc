	.text
	.p2align 4,,15
	.globl	__dgettext
	.type	__dgettext, @function
__dgettext:
	movl	$5, %edx
	jmp	__dcgettext
	.size	__dgettext, .-__dgettext
	.weak	dgettext
	.set	dgettext,__dgettext
	.hidden	__dcgettext
