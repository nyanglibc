	.text
	.p2align 4,,15
	.globl	_nl_find_domain
	.hidden	_nl_find_domain
	.type	_nl_find_domain, @function
_nl_find_domain:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rdx, %r12
	subq	$72, %rsp
	cmpq	$0, __pthread_rwlock_rdlock@GOTPCREL(%rip)
	je	.L2
	leaq	lock.10646(%rip), %rdi
	call	__pthread_rwlock_rdlock@PLT
.L2:
	movq	%rbx, %rdi
	call	strlen
	subq	$8, %rsp
	leaq	1(%rax), %rdx
	leaq	_nl_loaded_domains(%rip), %rdi
	pushq	$0
	pushq	%r12
	xorl	%r9d, %r9d
	pushq	$0
	pushq	$0
	xorl	%ecx, %ecx
	pushq	$0
	movq	%rbp, %r8
	movq	%rbx, %rsi
	call	_nl_make_l10nflist@PLT
	addq	$48, %rsp
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	movq	%rax, %r14
	je	.L3
	leaq	lock.10646(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L3:
	testq	%r14, %r14
	je	.L4
	movl	8(%r14), %esi
	testl	%esi, %esi
	jle	.L55
.L5:
	cmpq	$0, 16(%r14)
	je	.L56
.L1:
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1
	leaq	32(%r14), %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rbx), %rax
	cmpq	$0, 16(%rax)
	jne	.L1
	addq	$8, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1
.L8:
	movl	8(%rdi), %ecx
	testl	%ecx, %ecx
	jg	.L7
	movq	%r13, %rsi
	call	_nl_load_domain
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_nl_load_domain
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, %rdi
	call	_nl_expand_alias@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L9
	movq	%rax, %rdi
	call	strlen
	leaq	1(%rax), %r15
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L1
	movq	8(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
.L9:
	leaq	40(%rsp), %rcx
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rsi
	leaq	56(%rsp), %r9
	leaq	48(%rsp), %r8
	movq	%rbp, %rdi
	call	_nl_explode_name@PLT
	cmpl	$-1, %eax
	movl	%eax, %r15d
	je	.L1
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	je	.L12
	leaq	lock.10646(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L12:
	movq	%rbx, %rdi
	call	strlen
	subq	$8, %rsp
	leaq	1(%rax), %rdx
	leaq	_nl_loaded_domains(%rip), %rdi
	pushq	$1
	pushq	%r12
	movl	%r15d, %ecx
	pushq	56(%rsp)
	pushq	88(%rsp)
	movq	%rbx, %rsi
	pushq	88(%rsp)
	movq	88(%rsp), %r9
	movq	72(%rsp), %r8
	call	_nl_make_l10nflist@PLT
	addq	$48, %rsp
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	movq	%rax, %r14
	je	.L13
	leaq	lock.10646(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L13:
	testq	%r14, %r14
	je	.L15
	movl	8(%r14), %edx
	testl	%edx, %edx
	jle	.L57
.L16:
	cmpq	$0, 16(%r14)
	je	.L58
.L17:
	cmpq	$0, 8(%rsp)
	je	.L15
	movq	%rbp, %rdi
	call	free@PLT
.L15:
	andl	$1, %r15d
	je	.L1
	movq	56(%rsp), %rdi
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L58:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L17
	leaq	32(%r14), %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rbx), %rax
	cmpq	$0, 16(%rax)
	jne	.L17
	addq	$8, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L17
.L19:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L18
	movq	%r13, %rsi
	call	_nl_load_domain
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_nl_load_domain
	jmp	.L16
	.size	_nl_find_domain, .-_nl_find_domain
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	_nl_finddomain_subfreeres
	.hidden	_nl_finddomain_subfreeres
	.type	_nl_finddomain_subfreeres, @function
_nl_finddomain_subfreeres:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	_nl_loaded_domains(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L62
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%rbp, %rbx
.L62:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L61
	call	_nl_unload_domain
.L61:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rbp
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L63
.L59:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	_nl_finddomain_subfreeres, .-_nl_finddomain_subfreeres
	.local	lock.10646
	.comm	lock.10646,56,32
	.local	_nl_loaded_domains
	.comm	_nl_loaded_domains,8,8
	.weak	__pthread_rwlock_wrlock
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_rdlock
	.hidden	_nl_unload_domain
	.hidden	_nl_load_domain
	.hidden	strlen
