	.text
	.p2align 4,,15
	.globl	_nl_explode_name
	.type	_nl_explode_name, @function
_nl_explode_name:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$40, %rsp
	movq	$0, (%rdx)
	movq	$0, (%rcx)
	movq	$0, (%r8)
	movq	$0, (%r9)
	movq	%rdi, (%rsi)
	movzbl	(%rdi), %eax
	testb	$-65, %al
	je	.L2
	cmpb	$95, %al
	je	.L2
	cmpb	$46, %al
	je	.L2
	movq	%rdi, %rax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L63:
	cmpb	$64, %sil
	je	.L6
.L4:
	movzbl	1(%rax), %esi
	leaq	1(%rax), %rbx
	testb	%sil, %sil
	je	.L3
	cmpb	$95, %sil
	je	.L3
	cmpb	$46, %sil
	movq	%rbx, %rax
	jne	.L63
.L6:
	cmpq	%rbx, %rbp
	je	.L2
.L60:
	xorl	%ebp, %ebp
	cmpb	$46, %sil
	je	.L64
.L8:
	cmpb	$64, %sil
	je	.L65
.L15:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L16
	cmpb	$0, (%rax)
	jne	.L16
	andl	$-5, %ebp
.L16:
	movq	(%r8), %rax
	testq	%rax, %rax
	je	.L1
	cmpb	$0, (%rax)
	jne	.L1
	andl	$-3, %ebp
.L1:
	addq	$40, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbp, %rdi
	movq	%r8, 8(%rsp)
	call	strlen@PLT
	leaq	0(%rbp,%rax), %rbx
	xorl	%ebp, %ebp
	movq	8(%rsp), %r8
	movzbl	(%rbx), %esi
	cmpb	$64, %sil
	jne	.L15
.L65:
	leaq	1(%rbx), %rax
	movb	$0, (%rbx)
	movq	%rax, 0(%r13)
	cmpb	$0, 1(%rbx)
	je	.L15
	orl	$8, %ebp
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L3:
	cmpq	%rbx, %rbp
	je	.L2
	cmpb	$95, %sil
	jne	.L60
	addq	$2, %rax
	movb	$0, (%rbx)
	movq	%rax, (%r14)
	movzbl	1(%rbx), %esi
	movq	%rax, %rbx
	testb	$-65, %sil
	jne	.L61
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L66:
	addq	$1, %rbx
	movzbl	(%rbx), %esi
	testb	$-65, %sil
	je	.L18
.L61:
	cmpb	$46, %sil
	jne	.L66
.L18:
	cmpb	$46, %sil
	movl	$4, %ebp
	jne	.L8
.L64:
	leaq	1(%rbx), %rdi
	movb	$0, (%rbx)
	movl	%ebp, %r15d
	orl	$2, %r15d
	movq	%rdi, (%r8)
	movzbl	1(%rbx), %esi
	testb	$-65, %sil
	je	.L19
	movq	%rdi, %r12
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%rbx, %r12
.L12:
	testb	$-65, 1(%r12)
	leaq	1(%r12), %rbx
	jne	.L20
	cmpq	%rbx, %rdi
	movq	%r9, 8(%rsp)
	je	.L21
	movq	%rbx, %rsi
	movq	%r8, 24(%rsp)
	subq	%rdi, %rsi
	call	_nl_normalize_codeset@PLT
	movq	8(%rsp), %r9
	testq	%rax, %rax
	movq	%rax, (%r9)
	je	.L22
	movq	24(%rsp), %r8
	movq	%rax, %rsi
	movq	%rax, 16(%rsp)
	movq	(%r8), %rdi
	movq	%r8, 8(%rsp)
	call	strcmp
	testl	%eax, %eax
	movq	8(%rsp), %r8
	movq	16(%rsp), %rdx
	je	.L67
	orl	$3, %ebp
	movzbl	1(%r12), %esi
	jmp	.L8
.L21:
	movl	%r15d, %ebp
	jmp	.L8
.L67:
	movq	%rdx, %rdi
	movl	%r15d, %ebp
	call	free@PLT
	movzbl	1(%r12), %esi
	movq	8(%rsp), %r8
	jmp	.L8
.L19:
	movl	%r15d, %ebp
	movq	%rdi, %rbx
	jmp	.L8
.L22:
	orl	$-1, %ebp
	jmp	.L1
	.size	_nl_explode_name, .-_nl_explode_name
	.hidden	strcmp
