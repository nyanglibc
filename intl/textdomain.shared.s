	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__textdomain
	.type	__textdomain, @function
__textdomain:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	movq	_nl_current_default_domain(%rip), %rbx
	je	.L1
	movl	__libc_pthread_functions_init(%rip), %edx
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.L4
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	_nl_state_lock(%rip), %rdi
#APP
# 73 "textdomain.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L4:
	cmpb	$0, (%rbx)
	movq	_nl_current_default_domain(%rip), %rbp
	jne	.L20
.L5:
	leaq	_nl_default_default_domain(%rip), %rax
	movq	%rax, _nl_current_default_domain(%rip)
	movq	%rax, %rbx
.L7:
	movq	_nl_msg_cat_cntr@GOTPCREL(%rip), %rdx
	addl	$1, (%rdx)
	cmpq	%rax, %rbp
	je	.L9
	cmpq	%rbx, %rbp
	je	.L9
	movq	%rbp, %rdi
	call	free@PLT
.L9:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	_nl_state_lock(%rip), %rdi
#APP
# 117 "textdomain.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	_nl_default_default_domain(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L5
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L21
	movq	_nl_msg_cat_cntr@GOTPCREL(%rip), %rax
	movq	%rbp, %rbx
	addl	$1, (%rax)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rbx, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L9
	movq	%rax, _nl_current_default_domain(%rip)
	leaq	_nl_default_default_domain(%rip), %rax
	jmp	.L7
	.size	__textdomain, .-__textdomain
	.weak	textdomain
	.set	textdomain,__textdomain
	.hidden	_nl_default_default_domain
	.hidden	_nl_state_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	_nl_current_default_domain
