	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"d"
.LC1:
	.string	"i"
.LC2:
	.string	"o"
.LC3:
	.string	"u"
.LC4:
	.string	"x"
.LC5:
	.string	"X"
.LC6:
	.string	"I"
.LC7:
	.string	"lo"
.LC8:
	.string	"ld"
.LC9:
	.string	"li"
.LC10:
	.string	"lu"
.LC11:
	.string	"lx"
.LC12:
	.string	"lX"
.LC13:
	.string	""
	.text
	.p2align 4,,15
	.globl	_nl_load_domain
	.hidden	_nl_load_domain
	.type	_nl_load_domain, @function
_nl_load_domain:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$312, %rsp
	movq	%fs:16, %r12
	cmpq	%r12, 8+lock.10211(%rip)
	je	.L2
#APP
# 770 "loadmsgcat.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock.10211(%rip)
# 0 "" 2
#NO_APP
.L4:
	movq	%r12, 8+lock.10211(%rip)
.L2:
	movl	4+lock.10211(%rip), %eax
	movl	8(%rbx), %r10d
	addl	$1, %eax
	testl	%r10d, %r10d
	movl	%eax, 4+lock.10211(%rip)
	jne	.L5
	movq	(%rbx), %rdi
	movl	$-1, 8(%rbx)
	movq	$0, 16(%rbx)
	testq	%rdi, %rdi
	je	.L6
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %r12d
	jne	.L7
.L324:
	movl	4+lock.10211(%rip), %eax
.L6:
	movl	$1, 8(%rbx)
.L5:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+lock.10211(%rip)
	jne	.L1
	movq	$0, 8+lock.10211(%rip)
#APP
# 1281 "loadmsgcat.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L122
	subl	$1, lock.10211(%rip)
.L1:
	addq	$312, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	160(%rsp), %rsi
	movl	%eax, %edi
	call	__fstat64
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L8
	movq	208(%rsp), %rax
	cmpq	$47, %rax
	movq	%rax, (%rsp)
	jbe	.L8
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r12d, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rax, %rsi
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L9
	movl	%r12d, %edi
	call	__close_nocancel
	movl	(%r15), %r13d
	cmpl	$-1794895138, %r13d
	setne	%al
	cmpl	$-569244523, %r13d
	movzbl	%al, %r12d
	je	.L246
	testb	%al, %al
	jne	.L10
.L246:
	movl	$1, %r14d
.L12:
	movl	$200, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r10
	je	.L324
	movq	%r15, (%rax)
	movl	%r14d, 8(%rax)
	cmpl	$-1794895138, %r13d
	movq	%rax, 16(%rbx)
	movq	(%rsp), %rax
	movl	%r12d, 24(%r10)
	movq	$0, 32(%r10)
	movq	%rax, 16(%r10)
	movl	4(%r15), %eax
	jne	.L326
	cmpl	$131071, %eax
	ja	.L129
	movl	8(%r15), %edx
	movl	20(%r15), %ecx
	movl	%edx, 40(%r10)
	movl	12(%r15), %edx
	movl	%ecx, 8(%rsp)
	addq	%r15, %rdx
	movq	%rdx, 48(%r10)
	movl	16(%r15), %edx
	movl	%ecx, 88(%r10)
	addq	%r15, %rdx
	cmpl	$2, %ecx
	movq	%rdx, 56(%r10)
	jbe	.L24
	movl	24(%r15), %edx
.L26:
	addq	%r15, %rdx
	testw	%ax, %ax
	movl	%r12d, 104(%r10)
	movq	%rdx, 96(%r10)
	je	.L33
	testq	%rdx, %rdx
	je	.L323
	cmpl	$-1794895138, %r13d
	movl	36(%r15), %eax
	je	.L31
	bswap	%eax
	testl	%eax, %eax
	movl	%eax, 32(%rsp)
	je	.L33
	movl	28(%r15), %eax
	movl	32(%r15), %r12d
	bswap	%eax
	bswap	%r12d
	movl	%eax, 24(%rsp)
	movl	%r12d, %r12d
.L35:
	movl	24(%rsp), %edi
	movl	$8, %esi
	movq	%r10, 40(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	movq	40(%rsp), %r10
	je	.L323
	movl	24(%rsp), %eax
	testl	%eax, %eax
	je	.L37
	subl	$1, %eax
	movq	%r10, 40(%rsp)
	xorl	%r11d, %r11d
	leaq	8(,%rax,8), %rsi
	addq	%r15, %r12
	leaq	.LC6(%rip), %rcx
	movq	16(%rsp), %r10
	movq	%rbx, 48(%rsp)
	movq	%rbp, 56(%rsp)
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L329:
	movl	4(%r12,%r11), %eax
	movl	(%r12,%r11), %edx
	bswap	%eax
	bswap	%edx
	movl	%eax, %eax
	addq	%r15, %rax
.L39:
	testl	%edx, %edx
	je	.L40
	subl	$1, %edx
	cmpb	$0, (%rax,%rdx)
	jne	.L40
	movzbl	(%rax), %edx
	cmpb	$80, %dl
	je	.L327
	cmpb	$73, %dl
	jne	.L240
	cmpb	$0, 1(%rax)
	movl	$0, %edx
	cmove	%rcx, %rdx
.L43:
	movq	%rdx, (%r10,%r11)
	addq	$8, %r11
	cmpq	%r11, %rsi
	je	.L328
.L58:
	cmpl	$-1794895138, %r13d
	jne	.L329
	movl	4(%r12,%r11), %eax
	movl	(%r12,%r11), %edx
	addq	%r15, %rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L122:
#APP
# 1281 "loadmsgcat.c" 1
	xchgl %eax, lock.10211(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock.10211(%rip), %rdi
	movl	$202, %eax
#APP
# 1281 "loadmsgcat.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L331:
	cmpq	$-1, %rax
	jne	.L317
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	je	.L124
.L317:
	movl	%ebp, %r12d
.L8:
	movl	%r12d, %edi
	call	__close_nocancel
	movl	4+lock.10211(%rip), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock.10211(%rip)
	je	.L4
	leaq	lock.10211(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L326:
	bswap	%eax
	cmpl	$131071, %eax
	ja	.L129
	movl	8(%r15), %edx
	bswap	%edx
	movl	%edx, 40(%r10)
	movl	12(%r15), %edx
	bswap	%edx
	movl	%edx, %edx
	addq	%r15, %rdx
	movq	%rdx, 48(%r10)
	movl	16(%r15), %edx
	bswap	%edx
	movl	%edx, %edx
	addq	%r15, %rdx
	movq	%rdx, 56(%r10)
	movl	20(%r15), %edx
	bswap	%edx
	cmpl	$2, %edx
	movl	%edx, 8(%rsp)
	movl	%edx, 88(%r10)
	jbe	.L24
	movl	24(%r15), %edx
	bswap	%edx
	movl	%edx, %edx
	jmp	.L26
.L10:
	movq	(%rsp), %rsi
	movq	%r15, %rdi
	call	__munmap
	movl	4+lock.10211(%rip), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$0, 64(%r10)
	movq	$0, 72(%r10)
	movq	$0, 80(%r10)
.L29:
	cmpq	$0, __pthread_rwlock_init@GOTPCREL(%rip)
	movq	$0, 112(%r10)
	movq	$0, 120(%r10)
	je	.L117
	leaq	128(%r10), %rdi
	xorl	%esi, %esi
	movq	%r10, 8(%rsp)
	call	__pthread_rwlock_init@PLT
	movq	8(%rsp), %r10
.L117:
	leaq	144(%rsp), %r8
	leaq	.LC13(%rip), %rdx
	xorl	%ecx, %ecx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%r10, 8(%rsp)
	call	_nl_find_msg
	cmpq	$-1, %rax
	movq	8(%rsp), %r10
	je	.L330
	leaq	192(%r10), %rdx
	leaq	184(%r10), %rsi
	movq	%rax, %rdi
	call	__gettext_extract_plural
	movl	4+lock.10211(%rip), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L24:
	testw	%ax, %ax
	movq	$0, 96(%r10)
	movl	%r12d, 104(%r10)
	je	.L33
.L323:
	movq	32(%r10), %rdi
.L20:
	movq	%r10, 8(%rsp)
	call	free@PLT
	testl	%r14d, %r14d
	movq	8(%rsp), %r10
	je	.L115
	movq	(%rsp), %rsi
	movq	%r15, %rdi
	call	__munmap
	movq	8(%rsp), %r10
.L116:
	movq	%r10, %rdi
	call	free@PLT
	movq	$0, 16(%rbx)
	movl	4+lock.10211(%rip), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%edi, %edi
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rsp), %r13
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L8
	movq	%rbp, 8(%rsp)
	movl	%r12d, %ebp
	movq	%rax, %r12
.L124:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebp, %edi
	call	__read_nocancel
	testq	%rax, %rax
	jle	.L331
	addq	%rax, %r12
	subq	%rax, %r13
	jne	.L124
	movl	%ebp, %r12d
	movq	8(%rsp), %rbp
	movl	%r12d, %edi
	call	__close_nocancel
	movl	(%r15), %r13d
	cmpl	$-1794895138, %r13d
	setne	%al
	cmpl	$-569244523, %r13d
	movzbl	%al, %r12d
	je	.L12
	testb	%al, %al
	je	.L12
	movq	%r15, %rdi
	call	free@PLT
	movl	4+lock.10211(%rip), %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L115:
	movq	%r15, %rdi
	movq	%r10, (%rsp)
	call	free@PLT
	movq	(%rsp), %r10
	jmp	.L116
.L327:
	xorl	%edx, %edx
	cmpb	$82, 1(%rax)
	jne	.L43
	cmpb	$73, 2(%rax)
	jne	.L43
	movzbl	3(%rax), %edi
	cmpb	$100, %dil
	sete	%r8b
	cmpb	$105, %dil
	sete	%bpl
	orl	%r8d, %ebp
	cmpb	$111, %dil
	sete	%r9b
	orl	%ebp, %r9d
	cmpb	$117, %dil
	sete	%r8b
	movl	%r8d, %ebx
	orl	%r9d, %ebx
	cmpb	$120, %dil
	sete	%r8b
	orl	%ebx, %r8d
	cmpb	$88, %dil
	je	.L247
	testb	%r8b, %r8b
	je	.L43
.L247:
	movzbl	4(%rax), %edx
	cmpb	$56, %dl
	je	.L332
	cmpb	$49, %dl
	je	.L333
	cmpb	$51, %dl
	jne	.L47
	xorl	%edx, %edx
	cmpb	$50, 5(%rax)
	jne	.L43
	cmpb	$0, 6(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L31:
	testl	%eax, %eax
	movl	%eax, 32(%rsp)
	je	.L33
	movl	28(%r15), %eax
	movl	32(%r15), %r12d
	movl	%eax, 24(%rsp)
	jmp	.L35
.L40:
	movq	40(%rsp), %r10
	movq	16(%rsp), %rdi
	movq	48(%rsp), %rbx
	movq	%r10, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %r10
	movq	32(%r10), %rdi
	jmp	.L20
.L330:
	cmpq	$0, __pthread_rwlock_destroy@GOTPCREL(%rip)
	je	.L323
	leaq	128(%r10), %rdi
	movq	%r10, 8(%rsp)
	call	__pthread_rwlock_destroy@PLT
	movq	8(%rsp), %r10
	jmp	.L323
.L328:
	movq	40(%rsp), %r10
	movq	48(%rsp), %rbx
	movq	56(%rsp), %rbp
.L37:
	cmpl	$-1794895138, %r13d
	je	.L59
	movl	40(%r15), %eax
	bswap	%eax
	movl	%eax, %eax
	addq	%r15, %rax
	movq	%rax, 80(%rsp)
	movl	44(%r15), %eax
	bswap	%eax
	movl	%eax, %eax
.L60:
	leaq	(%r15,%rax), %rcx
	movl	8(%rsp), %eax
	movq	80(%rsp), %rdi
	movq	%rbp, 128(%rsp)
	movq	16(%rsp), %r12
	movl	24(%rsp), %ebp
	movq	%rcx, 88(%rsp)
	movq	%rdi, 8(%rsp)
	movq	%rcx, 40(%rsp)
	leaq	0(,%rax,4), %rsi
	movl	32(%rsp), %eax
	movl	$0, 56(%rsp)
	movl	%r14d, 96(%rsp)
	movq	%r15, 48(%rsp)
	movq	%rsi, 120(%rsp)
	movq	%rsi, 64(%rsp)
	subl	$1, %eax
	movq	%r10, 104(%rsp)
	movq	%rbx, 112(%rsp)
	leaq	4(%rdi,%rax,4), %rax
	movq	%rax, 72(%rsp)
.L73:
	xorl	%ebx, %ebx
.L126:
	cmpl	$-1794895138, %r13d
	je	.L61
	testq	%rbx, %rbx
	je	.L334
	movq	40(%rsp), %rax
	movl	(%rax), %eax
.L63:
	bswap	%eax
	movl	%eax, %r15d
	addq	48(%rsp), %r15
	cmpl	$-1, 8(%r15)
	setne	%al
.L64:
	xorl	%r14d, %r14d
	testb	%al, %al
	jne	.L335
.L67:
	cmpq	$1, %rbx
	movq	%r14, 144(%rsp,%rbx,8)
	jne	.L244
	movq	152(%rsp), %rax
	addq	144(%rsp), %rax
	addl	$1, 56(%rsp)
	addq	%rax, 64(%rsp)
.L71:
	addq	$4, 8(%rsp)
	addq	$4, 40(%rsp)
	movq	8(%rsp), %rax
	cmpq	%rax, 72(%rsp)
	jne	.L73
	movl	56(%rsp), %eax
	movl	96(%rsp), %r14d
	movq	48(%rsp), %r15
	movq	104(%rsp), %r10
	movq	112(%rsp), %rbx
	movq	128(%rsp), %rbp
	testl	%eax, %eax
	jne	.L336
	movl	$0, 64(%r10)
	movq	$0, 72(%r10)
	movq	$0, 80(%r10)
.L114:
	movq	16(%rsp), %rdi
	movq	%r10, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %r10
	jmp	.L29
.L61:
	testq	%rbx, %rbx
	je	.L337
	movq	40(%rsp), %rax
	movl	(%rax), %r15d
.L66:
	addq	48(%rsp), %r15
	cmpl	$-1, 8(%r15)
	setne	%al
	jmp	.L64
.L244:
	movl	$1, %ebx
	jmp	.L126
.L335:
	addq	$4, %r15
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L339:
	movl	(%r15), %eax
	bswap	%eax
	movl	%eax, %eax
	addq	%rax, %r14
	movl	4(%r15), %eax
	bswap	%eax
.L69:
	cmpl	$-1, %eax
	je	.L67
	cmpl	%ebp, %eax
	jnb	.L338
	movl	%eax, %eax
	movq	(%r12,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.L71
	call	strlen
	addq	$8, %r15
	addq	%rax, %r14
.L72:
	cmpl	$-1794895138, %r13d
	jne	.L339
	movl	(%r15), %eax
	addq	%rax, %r14
	movl	4(%r15), %eax
	jmp	.L69
.L338:
	movq	104(%rsp), %r10
	movq	16(%rsp), %rdi
	movl	96(%rsp), %r14d
	movq	48(%rsp), %r15
	movq	112(%rsp), %rbx
	movq	%r10, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %r10
	movq	32(%r10), %rdi
	jmp	.L20
.L59:
	movl	40(%r15), %eax
	addq	%r15, %rax
	movq	%rax, 80(%rsp)
	movl	44(%r15), %eax
	jmp	.L60
.L333:
	xorl	%edx, %edx
	cmpb	$54, 5(%rax)
	jne	.L43
	cmpb	$0, 6(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
.L332:
	xorl	%edx, %edx
	cmpb	$0, 5(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
.L336:
	leal	(%rax,%rax), %edi
	movq	%r10, 24(%rsp)
	salq	$4, %rdi
	addq	64(%rsp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	movq	24(%rsp), %r10
	je	.L323
	movl	56(%rsp), %eax
	movq	8(%rsp), %rcx
	xorl	%r9d, %r9d
	movq	120(%rsp), %r8
	movl	$0, 24(%rsp)
	movq	%r9, %r12
	movl	%r14d, 104(%rsp)
	movq	%rbx, 112(%rsp)
	movq	%rcx, 32(%r10)
	movq	%rbp, 120(%rsp)
	salq	$4, %rax
	addq	%rax, %rcx
	addq	%rcx, %rax
	movq	%rcx, 48(%rsp)
	movq	%rax, 40(%rsp)
	addq	%rax, %r8
	jmp	.L76
.L344:
	testl	%edx, %edx
	je	.L340
	movl	(%rsi,%r12,4), %eax
.L79:
	bswap	%eax
	movl	%eax, %eax
	addq	%r15, %rax
	cmpl	$-1, 8(%rax)
	setne	%r11b
.L80:
	testb	%r11b, %r11b
	jne	.L341
.L83:
	cmpl	$1, %edx
	jne	.L342
	movl	24(%rsp), %eax
	movq	8(%rsp), %rcx
	xorl	%r11d, %r11d
	movq	%r10, 96(%rsp)
	salq	$4, %rax
	addq	%rax, %rcx
	addq	48(%rsp), %rax
	movq	%rcx, 64(%rsp)
	movq	%rax, 72(%rsp)
.L101:
	testl	%r9d, %r9d
	je	.L87
	testl	%r11d, %r11d
	je	.L343
	movq	88(%rsp), %rax
	movq	72(%rsp), %r13
	movl	(%rax,%r12,4), %ebx
	bswap	%ebx
	movl	%ebx, %ebx
	addq	%r15, %rbx
	movl	(%rbx), %ebp
	bswap	%ebp
	movl	%ebp, %ebp
	addq	%r15, %rbp
.L89:
	cmpl	$-1, 8(%rbx)
	movl	4(%rbx), %eax
	jne	.L93
	bswap	%eax
	movl	%eax, %eax
.L95:
	movq	%rax, 0(%r13)
	movq	%rbp, 8(%r13)
.L96:
	subl	$1, %r11d
	jne	.L243
	addl	$1, 24(%rsp)
	movq	96(%rsp), %r10
.L85:
	addq	$1, %r12
	cmpl	%r12d, 32(%rsp)
	jbe	.L102
.L76:
	movl	24(%r10), %r9d
	movq	16(%rsp), %rcx
	xorl	%edx, %edx
	movq	88(%rsp), %rsi
	movq	80(%rsp), %rdi
.L103:
	testl	%r9d, %r9d
	jne	.L344
	testl	%edx, %edx
	je	.L345
	movl	(%rsi,%r12,4), %eax
.L82:
	addq	%r15, %rax
	cmpl	$-1, 8(%rax)
	setne	%r11b
	jmp	.L80
.L342:
	movl	$1, %edx
	jmp	.L103
.L341:
	addq	$4, %rax
	jmp	.L86
.L346:
	movl	%r11d, %r11d
	cmpq	$0, (%rcx,%r11,8)
	je	.L85
	addq	$8, %rax
.L86:
	movl	4(%rax), %r11d
	testl	%r9d, %r9d
	movl	%r11d, %ebx
	bswap	%ebx
	cmovne	%ebx, %r11d
	cmpl	$-1, %r11d
	jne	.L346
	jmp	.L83
.L87:
	testl	%r11d, %r11d
	je	.L347
	movq	88(%rsp), %rax
	movq	72(%rsp), %r13
	movl	(%rax,%r12,4), %ebx
	addq	%r15, %rbx
	movl	(%rbx), %ebp
	addq	%r15, %rbp
.L91:
	cmpl	$-1, 8(%rbx)
	movl	4(%rbx), %eax
	je	.L95
.L93:
	movq	%r12, 128(%rsp)
	movq	%r8, 8(%r13)
	movq	%rbp, %r12
	addq	$4, %rbx
	movl	%r11d, 136(%rsp)
	movl	%r9d, %ebp
	jmp	.L100
.L98:
	cmpl	$-1, %ecx
	je	.L99
	movq	16(%rsp), %rax
	movl	%ecx, %ecx
	addq	$8, %rbx
	movq	(%rax,%rcx,8), %rsi
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%rsi, %rdi
	repnz scasb
	movq	%r8, %rdi
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %r14
	movq	%r14, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	addq	%r14, %r8
.L100:
	testl	%ebp, %ebp
	movl	(%rbx), %eax
	movl	4(%rbx), %ecx
	je	.L97
	bswap	%eax
	bswap	%ecx
.L97:
	testl	%eax, %eax
	je	.L98
	movl	%eax, %r14d
	movq	%r12, %rsi
	movq	%r8, %rdi
	movq	%r14, %rdx
	movl	%ecx, 140(%rsp)
	addq	%r14, %r12
	call	memcpy@PLT
	movq	%rax, %r8
	movl	140(%rsp), %ecx
	addq	%r14, %r8
	jmp	.L98
.L99:
	movq	%r8, %rax
	subq	8(%r13), %rax
	movq	128(%rsp), %r12
	movl	136(%rsp), %r11d
	movq	%rax, 0(%r13)
	jmp	.L96
.L334:
	movq	8(%rsp), %rax
	movl	(%rax), %eax
	jmp	.L63
.L337:
	movq	8(%rsp), %rax
	movl	(%rax), %r15d
	jmp	.L66
.L102:
	movl	24(%rsp), %ecx
	cmpl	%ecx, 56(%rsp)
	movl	104(%rsp), %r14d
	movq	112(%rsp), %rbx
	movq	120(%rsp), %rbp
	jne	.L104
	movl	88(%r10), %ecx
	xorl	%eax, %eax
	jmp	.L105
.L107:
	cmpl	$0, 104(%r10)
	movq	96(%r10), %rdx
	movl	(%rdx,%rax,4), %edx
	je	.L106
	bswap	%edx
.L106:
	movq	40(%rsp), %rsi
	movl	%edx, (%rsi,%rax,4)
	addq	$1, %rax
.L105:
	cmpl	%eax, %ecx
	ja	.L107
	movq	40(%rsp), %r12
	movq	%rbp, %r8
	xorl	%r13d, %r13d
	movq	%rbx, %rbp
.L108:
	cmpl	%r13d, 56(%rsp)
	movl	%r13d, %ebx
	jbe	.L348
	movq	8(%rsp), %rcx
	movq	%r13, %rax
	movq	%r8, 32(%rsp)
	salq	$4, %rax
	movq	%r10, 24(%rsp)
	movq	8(%rcx,%rax), %rdi
	call	__hash_string
	movq	24(%rsp), %r10
	xorl	%edx, %edx
	movl	%eax, %esi
	movq	32(%rsp), %r8
	movl	88(%r10), %edi
	divl	%edi
	leal	-2(%rdi), %r9d
	movl	%esi, %eax
	movl	%edi, %esi
	movl	%edx, %ecx
	xorl	%edx, %edx
	divl	%r9d
	addl	$1, %edx
	movl	%edx, %eax
	subl	%edx, %esi
	subl	%edi, %eax
	movl	%eax, %edi
	jmp	.L109
.L110:
	leal	(%rcx,%rdi), %eax
	leal	(%rcx,%rdx), %r9d
	cmpl	%ecx, %esi
	cmova	%r9d, %eax
	movl	%eax, %ecx
.L109:
	movl	%ecx, %eax
	leaq	(%r12,%rax,4), %rax
	movl	(%rax), %r9d
	testl	%r9d, %r9d
	jne	.L110
	movl	40(%r10), %edx
	addq	$1, %r13
	leal	1(%rbx,%rdx), %edx
	movl	%edx, (%rax)
	jmp	.L108
.L348:
	movl	56(%rsp), %eax
	movq	%rbp, %rbx
	movl	$0, 104(%r10)
	movq	%r8, %rbp
	movl	%eax, 64(%r10)
	movq	8(%rsp), %rax
	movq	%rax, 72(%r10)
	movq	48(%rsp), %rax
	movq	%rax, 80(%r10)
	movq	40(%rsp), %rax
	movq	%rax, 96(%r10)
	jmp	.L114
.L343:
	movq	80(%rsp), %rax
	movq	64(%rsp), %r13
	movl	(%rax,%r12,4), %ebx
	bswap	%ebx
	movl	%ebx, %ebx
	addq	%r15, %rbx
	movl	(%rbx), %ebp
	bswap	%ebp
	movl	%ebp, %ebp
	addq	%r15, %rbp
	jmp	.L89
.L347:
	movq	80(%rsp), %rax
	movq	64(%rsp), %r13
	movl	(%rax,%r12,4), %ebx
	addq	%r15, %rbx
	movl	(%rbx), %ebp
	addq	%r15, %rbp
	jmp	.L91
.L104:
	call	abort
.L243:
	movq	96(%rsp), %rax
	movl	$1, %r11d
	movl	24(%rax), %r9d
	jmp	.L101
.L47:
	cmpb	$54, %dl
	jne	.L48
	xorl	%edx, %edx
	cmpb	$52, 5(%rax)
	jne	.L43
	cmpb	$0, 6(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L240:
	xorl	%edx, %edx
	jmp	.L43
.L48:
	cmpb	$76, %dl
	jne	.L49
	xorl	%edx, %edx
	cmpb	$69, 5(%rax)
	jne	.L43
	cmpb	$65, 6(%rax)
	jne	.L43
	cmpb	$83, 7(%rax)
	jne	.L43
	cmpb	$84, 8(%rax)
	jne	.L43
	movzbl	9(%rax), %edx
	cmpb	$56, %dl
	movb	%dl, 64(%rsp)
	movl	$0, %edx
	je	.L349
	cmpb	$49, 64(%rsp)
	je	.L350
	cmpb	$51, 64(%rsp)
	je	.L351
	cmpb	$54, 64(%rsp)
	jne	.L43
	cmpb	$52, 10(%rax)
	jne	.L43
	cmpb	$0, 11(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L340:
	movl	(%rdi,%r12,4), %eax
	jmp	.L79
.L345:
	movl	(%rdi,%r12,4), %eax
	jmp	.L82
.L351:
	cmpb	$50, 10(%rax)
	jne	.L43
	cmpb	$0, 11(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
.L350:
	cmpb	$54, 10(%rax)
	jne	.L43
	cmpb	$0, 11(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
.L349:
	cmpb	$0, 10(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
.L49:
	cmpb	$70, %dl
	jne	.L53
	xorl	%edx, %edx
	cmpb	$65, 5(%rax)
	jne	.L43
	cmpb	$83, 6(%rax)
	jne	.L43
	cmpb	$84, 7(%rax)
	jne	.L43
	movzbl	8(%rax), %edx
	cmpb	$56, %dl
	movb	%dl, 64(%rsp)
	movl	$0, %edx
	je	.L352
	cmpb	$49, 64(%rsp)
	je	.L353
	cmpb	$51, 64(%rsp)
	je	.L354
	cmpb	$54, 64(%rsp)
	jne	.L43
	cmpb	$52, 9(%rax)
	jne	.L43
	cmpb	$0, 10(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L53:
	cmpb	$77, %dl
	jne	.L57
	xorl	%edx, %edx
	cmpb	$65, 5(%rax)
	jne	.L43
	cmpb	$88, 6(%rax)
	jne	.L43
	cmpb	$0, 7(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L354:
	cmpb	$50, 9(%rax)
	jne	.L43
	cmpb	$0, 10(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L57:
	cmpb	$80, %dl
	movl	$0, %edx
	jne	.L43
	cmpb	$84, 5(%rax)
	jne	.L43
	cmpb	$82, 6(%rax)
	jne	.L43
	cmpb	$0, 7(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L353:
	cmpb	$54, 9(%rax)
	jne	.L43
	cmpb	$0, 10(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC8(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC9(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC7(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC10(%rip), %rdx
	jne	.L43
	leaq	.LC11(%rip), %rdx
	leaq	.LC12(%rip), %rax
	testb	%r8b, %r8b
	cmove	%rax, %rdx
	jmp	.L43
.L352:
	cmpb	$0, 9(%rax)
	jne	.L43
	cmpb	$100, %dil
	leaq	.LC0(%rip), %rdx
	je	.L43
	testb	%bpl, %bpl
	leaq	.LC1(%rip), %rdx
	jne	.L43
	testb	%r9b, %r9b
	leaq	.LC2(%rip), %rdx
	jne	.L43
	testb	%bl, %bl
	leaq	.LC3(%rip), %rdx
	jne	.L43
	leaq	.LC5(%rip), %rdx
	leaq	.LC4(%rip), %rax
	testb	%r8b, %r8b
	cmovne	%rax, %rdx
	jmp	.L43
	.size	_nl_load_domain, .-_nl_load_domain
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	_nl_unload_domain
	.hidden	_nl_unload_domain
	.type	_nl_unload_domain, @function
_nl_unload_domain:
	pushq	%r13
	pushq	%r12
	leaq	__gettext_germanic_plural(%rip), %rax
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	184(%rdi), %rdi
	cmpq	%rax, %rdi
	je	.L356
	call	__gettext_free_exp
.L356:
	cmpq	$0, 120(%rbp)
	je	.L357
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L358:
	movq	8(%rbx), %rdi
	cmpq	$-1, %rdi
	je	.L359
	call	__gconv_close
.L359:
	addq	$1, %r12
	addq	$24, %r13
	cmpq	%r12, 120(%rbp)
	jbe	.L357
.L360:
	movq	112(%rbp), %rbx
	addq	%r13, %rbx
	movq	(%rbx), %rdi
	call	free@PLT
	movq	16(%rbx), %rdi
	leaq	-1(%rdi), %rax
	cmpq	$-3, %rax
	ja	.L358
	call	free@PLT
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L357:
	movq	112(%rbp), %rdi
	call	free@PLT
	cmpq	$0, __pthread_rwlock_destroy@GOTPCREL(%rip)
	je	.L361
	leaq	128(%rbp), %rdi
	call	__pthread_rwlock_destroy@PLT
.L361:
	movq	32(%rbp), %rdi
	call	free@PLT
	movl	8(%rbp), %eax
	testl	%eax, %eax
	jne	.L372
	movq	0(%rbp), %rdi
	call	free@PLT
.L363:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L372:
	movq	16(%rbp), %rsi
	movq	0(%rbp), %rdi
	call	__munmap
	jmp	.L363
	.size	_nl_unload_domain, .-_nl_unload_domain
	.local	lock.10211
	.comm	lock.10211,16,16
	.comm	_nl_msg_cat_cntr,4,4
	.weak	__pthread_rwlock_destroy
	.weak	__pthread_rwlock_init
	.hidden	__gconv_close
	.hidden	__gettext_free_exp
	.hidden	__gettext_germanic_plural
	.hidden	abort
	.hidden	__hash_string
	.hidden	strlen
	.hidden	__read_nocancel
	.hidden	__gettext_extract_plural
	.hidden	_nl_find_msg
	.hidden	__munmap
	.hidden	__lll_lock_wait_private
	.hidden	__close_nocancel
	.hidden	__mmap
	.hidden	__fstat64
	.hidden	__open_nocancel
