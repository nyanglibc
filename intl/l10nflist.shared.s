	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_nl_make_l10nflist
	.type	_nl_make_l10nflist, @function
_nl_make_l10nflist:
	pushq	%r15
	pushq	%r14
	movl	%ecx, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	leaq	2(%r12), %r13
	subq	$72, %rsp
	movq	%rdi, 24(%rsp)
	movq	%r8, %rdi
	movq	%rsi, 16(%rsp)
	movl	%ecx, 56(%rsp)
	movq	%r8, 8(%rsp)
	movq	%r9, 32(%rsp)
	call	__GI_strlen
	andl	$4, %r15d
	movq	%rax, %rbx
	movl	%r15d, %r14d
	je	.L97
	movq	32(%rsp), %rdi
	call	__GI_strlen
	leaq	1(%rax), %rbp
.L2:
	movl	56(%rsp), %eax
	addq	%r13, %rbx
	xorl	%r13d, %r13d
	andl	$2, %eax
	movl	%eax, 40(%rsp)
	je	.L3
	movq	128(%rsp), %rdi
	call	__GI_strlen
	leaq	1(%rax), %r13
.L3:
	movl	56(%rsp), %eax
	addq	%rbp, %rbx
	xorl	%ebp, %ebp
	andl	$1, %eax
	movl	%eax, 48(%rsp)
	je	.L4
	movq	136(%rsp), %rdi
	call	__GI_strlen
	leaq	1(%rax), %rbp
.L4:
	movl	56(%rsp), %eax
	addq	%r13, %rbx
	xorl	%r13d, %r13d
	andl	$8, %eax
	movl	%eax, 60(%rsp)
	je	.L5
	movq	144(%rsp), %rdi
	call	__GI_strlen
	leaq	1(%rax), %r13
.L5:
	movq	152(%rsp), %rdi
	call	__GI_strlen
	leaq	(%rbx,%rbp), %rdi
	movq	%rax, %r15
	addq	%r13, %rdi
	addq	%rax, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L40
	movq	16(%rsp), %rsi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	testq	%r12, %r12
	je	.L7
	movq	%r12, %rbx
	movq	%rbp, %r13
.L8:
	movq	%r13, %rdi
	call	__GI_strlen
	movq	%rax, %rdx
	notq	%rdx
	addq	%rdx, %rbx
	je	.L7
	addq	%r13, %rax
	leaq	1(%rax), %r13
	movb	$58, (%rax)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L7:
	movq	8(%rsp), %rsi
	leaq	0(%rbp,%r12), %rdi
	movb	$47, -1(%rbp,%r12)
	call	__GI_stpcpy@PLT
	testl	%r14d, %r14d
	jne	.L100
.L9:
	movl	40(%rsp), %esi
	leaq	1(%rax), %rdi
	testl	%esi, %esi
	jne	.L101
.L10:
	movl	48(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L102
.L11:
	movl	60(%rsp), %edx
	testl	%edx, %edx
	jne	.L103
.L12:
	movq	152(%rsp), %rsi
	leaq	1(%r15), %rdx
	movb	$47, (%rax)
	call	__GI_memcpy@PLT
	movq	24(%rsp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L41
	movq	%rax, %r13
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L16:
	movq	0(%r13), %rdi
	testq	%rdi, %rdi
	je	.L14
	movq	%rbp, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L15
	js	.L13
	movq	%r13, %r14
.L14:
	movq	24(%r13), %r13
	testq	%r13, %r13
	jne	.L16
.L13:
	movl	160(%rsp), %eax
	testl	%eax, %eax
	je	.L42
	movl	56(%rsp), %ecx
	movl	$1, %r13d
	movl	%ecx, %eax
	andl	$21845, %ecx
	sarl	%eax
	andl	$-10923, %eax
	addl	%ecx, %eax
	movl	%eax, %ecx
	andl	$13107, %eax
	sarl	$2, %ecx
	andl	$-3277, %ecx
	addl	%eax, %ecx
	movl	%ecx, %eax
	sarl	$4, %eax
	addl	%ecx, %eax
	andl	$3855, %eax
	movl	%eax, %ecx
	sarl	$8, %ecx
	addl	%eax, %ecx
	sall	%cl, %r13d
	testq	%r12, %r12
	movslq	%r13d, %rax
	movq	%rax, 40(%rsp)
	je	.L104
	movq	16(%rsp), %r15
	movq	%r12, %rbx
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r15, %rdi
	addq	$1, %r13
	call	__GI_strlen
	leaq	1(%r15,%rax), %r15
	notq	%rax
	addq	%rax, %rbx
	jne	.L17
	imulq	40(%rsp), %r13
	leaq	40(,%r13,8), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L34
	movq	16(%rsp), %r15
	movq	%rbp, (%rax)
	movq	%r12, %rbp
	.p2align 4,,10
	.p2align 3
.L20:
	movq	%r15, %rdi
	addq	$1, %rbx
	call	__GI_strlen
	leaq	1(%r15,%rax), %r15
	notq	%rax
	addq	%rax, %rbp
	jne	.L20
	cmpq	$1, %rbx
	movl	$1, %eax
	je	.L105
.L21:
	testq	%r14, %r14
	movl	%eax, 8(%r13)
	movq	$0, 16(%r13)
	je	.L106
	movq	24(%r14), %rax
	movq	%rax, 24(%r13)
	movq	%r13, 24(%r14)
.L23:
	testq	%r12, %r12
	je	.L45
	movq	16(%rsp), %r14
	movq	%r12, %rbx
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%r14, %rdi
	addq	$1, %rbp
	call	__GI_strlen
	leaq	1(%r14,%rax), %r14
	notq	%rax
	addq	%rax, %rbx
	jne	.L25
	movl	56(%rsp), %r15d
	xorl	%eax, %eax
	cmpq	$1, %rbp
	sete	%al
	subl	%eax, %r15d
.L24:
	movl	56(%rsp), %eax
	xorl	%ebx, %ebx
	testl	%r15d, %r15d
	notl	%eax
	movl	%eax, 56(%rsp)
	movq	16(%rsp), %rax
	leaq	(%rax,%r12), %r14
	js	.L32
	movq	%r12, %rax
	movl	%r15d, %r12d
	movq	%rax, %r15
.L31:
	testl	%r12d, 56(%rsp)
	je	.L107
.L27:
	subl	$1, %r12d
	cmpl	$-1, %r12d
	jne	.L31
.L32:
	movq	$0, 32(%r13,%rbx,8)
.L1:
	addq	$72, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%ebp, %ebp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L105:
	movl	56(%rsp), %eax
	andl	$3, %eax
	cmpl	$3, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L103:
	movq	144(%rsp), %rsi
	movb	$64, (%rax)
	call	__GI_stpcpy@PLT
	leaq	1(%rax), %rdi
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L102:
	movq	136(%rsp), %rsi
	movb	$46, (%rax)
	call	__GI_stpcpy@PLT
	leaq	1(%rax), %rdi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L101:
	movq	128(%rsp), %rsi
	movb	$46, (%rax)
	call	__GI_stpcpy@PLT
	leaq	1(%rax), %rdi
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L100:
	movq	32(%rsp), %rsi
	leaq	1(%rax), %rdi
	movb	$95, (%rax)
	call	__GI_stpcpy@PLT
	jmp	.L9
.L42:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L107:
	xorl	%ebp, %ebp
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L108:
	cmpq	%r14, %rbp
	jnb	.L27
	movq	%rbp, %rdi
	call	__GI_strlen@PLT
	leaq	1(%rbp,%rax), %rbp
	cmpq	%r14, %rbp
	jnb	.L27
	testq	%rbp, %rbp
	je	.L27
.L29:
	movq	%rbp, %rdi
	addq	$1, %rbx
	call	__GI_strlen
	subq	$8, %rsp
	leaq	1(%rax), %rdx
	movl	%r12d, %ecx
	pushq	$1
	pushq	168(%rsp)
	movq	%rbp, %rsi
	pushq	168(%rsp)
	pushq	168(%rsp)
	pushq	168(%rsp)
	movq	80(%rsp), %r9
	movq	56(%rsp), %r8
	movq	72(%rsp), %rdi
	call	_nl_make_l10nflist@PLT
	addq	$48, %rsp
	movq	%rax, 24(%r13,%rbx,8)
.L26:
	testq	%rbp, %rbp
	jne	.L108
	testq	%r15, %r15
	je	.L27
	movq	16(%rsp), %rbp
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L106:
	movq	48(%rsp), %rax
	movq	%rax, 24(%r13)
	movq	24(%rsp), %rax
	movq	%r13, (%rax)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L104:
	movl	$40, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L34
	movq	%rbp, 0(%r13)
	movl	$1, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L45:
	movl	56(%rsp), %r15d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r13d, %r13d
	jmp	.L1
.L41:
	xorl	%r14d, %r14d
	jmp	.L13
.L34:
	movq	%rbp, %rdi
	xorl	%r13d, %r13d
	call	free@PLT
	jmp	.L1
	.size	_nl_make_l10nflist, .-_nl_make_l10nflist
	.p2align 4,,15
	.globl	_nl_normalize_codeset
	.type	_nl_normalize_codeset, @function
_nl_normalize_codeset:
	testq	%rsi, %rsi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L110
	movq	104+_nl_C_locobj(%rip), %rbx
	leaq	(%rsi,%rdi), %r8
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rdi, %rdx
	movl	$1, %esi
	xorl	%edi, %edi
	xorl	%r9d, %r9d
	.p2align 4,,10
	.p2align 3
.L112:
	movzbl	(%rdx), %ecx
	testb	$8, (%rbx,%rcx,2)
	movq	%rcx, %rax
	je	.L111
	subl	$48, %eax
	addq	$1, %rdi
	cmpl	$10, %eax
	cmovnb	%r9d, %esi
.L111:
	addq	$1, %rdx
	cmpq	%rdx, %r8
	jne	.L112
	testl	%esi, %esi
	jne	.L131
	addq	$1, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L115
	movq	%rcx, %rax
.L119:
	movq	112+_nl_C_locobj(%rip), %r9
	xorl	%edx, %edx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L132:
	movl	(%r9,%r8,4), %esi
	addq	$1, %rcx
	movb	%sil, -1(%rcx)
.L117:
	addq	$1, %rdx
	cmpq	%rdx, %r12
	jbe	.L121
.L118:
	movzbl	0(%rbp,%rdx), %r8d
	testb	$4, 1(%rbx,%r8,2)
	jne	.L132
	movzbl	%r8b, %esi
	subl	$48, %esi
	cmpl	$9, %esi
	ja	.L117
	addq	$1, %rdx
	movb	%r8b, (%rcx)
	addq	$1, %rcx
	cmpq	%rdx, %r12
	ja	.L118
.L121:
	movb	$0, (%rcx)
.L134:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L131:
	addq	$4, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L133
.L115:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	movl	$7304041, (%rax)
	leaq	3(%rax), %rcx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L110:
	movl	$4, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L115
	leaq	3(%rax), %rcx
	movl	$7304041, (%rax)
	movb	$0, (%rcx)
	jmp	.L134
	.size	_nl_normalize_codeset, .-_nl_normalize_codeset
	.hidden	_nl_C_locobj
