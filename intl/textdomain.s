	.text
	.p2align 4,,15
	.globl	__textdomain
	.type	__textdomain, @function
__textdomain:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdi, %rdi
	movq	_nl_current_default_domain(%rip), %rbx
	je	.L1
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	movq	%rdi, %rbx
	je	.L4
	leaq	_nl_state_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L4:
	cmpb	$0, (%rbx)
	movq	_nl_current_default_domain(%rip), %rbp
	jne	.L26
.L5:
	leaq	_nl_default_default_domain(%rip), %rax
	movq	%rax, _nl_current_default_domain(%rip)
	movq	%rax, %rbx
.L7:
	addl	$1, _nl_msg_cat_cntr(%rip)
	cmpq	%rax, %rbp
	je	.L9
	cmpq	%rbx, %rbp
	je	.L9
	movq	%rbp, %rdi
	call	free@PLT
.L9:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L1
.L28:
	leaq	_nl_state_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L1:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	_nl_default_default_domain(%rip), %rsi
	movq	%rbx, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L5
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L27
	addl	$1, _nl_msg_cat_cntr(%rip)
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	movq	%rbp, %rbx
	jne	.L28
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rbx, %rdi
	call	__strdup
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L9
	movq	%rax, _nl_current_default_domain(%rip)
	leaq	_nl_default_default_domain(%rip), %rax
	jmp	.L7
	.size	__textdomain, .-__textdomain
	.weak	textdomain
	.set	textdomain,__textdomain
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	__strdup
	.hidden	strcmp
	.hidden	_nl_default_default_domain
	.hidden	_nl_state_lock
	.hidden	_nl_current_default_domain
