	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gettext_free_exp
	.hidden	__gettext_free_exp
	.type	__gettext_free_exp, @function
__gettext_free_exp:
	testq	%rdi, %rdi
	je	.L1
	pushq	%rbx
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	cmpl	$2, %eax
	je	.L4
	cmpl	$3, %eax
	je	.L5
	cmpl	$1, %eax
	je	.L6
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	movq	24(%rdi), %rdi
	call	__gettext_free_exp
.L4:
	movq	16(%rbx), %rdi
	call	__gettext_free_exp
.L6:
	movq	8(%rbx), %rdi
	call	__gettext_free_exp
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	__gettext_free_exp, .-__gettext_free_exp
	.p2align 4,,15
	.type	new_exp, @function
new_exp:
	pushq	%r14
	pushq	%r13
	movl	%esi, %r14d
	pushq	%r12
	movq	%rdx, %r12
	leal	-1(%rdi), %edx
	pushq	%rbp
	pushq	%rbx
	cmpl	$-1, %edx
	je	.L18
	movslq	%edx, %rbx
	movq	(%r12,%rbx,8), %rbp
	leaq	0(,%rbx,8), %rcx
	testq	%rbp, %rbp
	je	.L26
	leaq	-8(%r12,%rcx), %rax
	movl	%edx, %edx
	movl	%edi, %r13d
	salq	$3, %rdx
	movq	%rax, %rcx
	subq	%rdx, %rcx
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	subq	$8, %rax
	cmpq	$0, 8(%rax)
	je	.L26
.L20:
	cmpq	%rcx, %rax
	jne	.L21
	movl	$32, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L26
	movl	%r13d, (%rax)
	movl	%r14d, 4(%rax)
	movq	%rbp, 8(%rax,%rbx,8)
	subq	$1, %rbx
	cmpl	$-1, %ebx
	je	.L17
.L36:
	movq	(%r12,%rbx,8), %rbp
	movq	%rbp, 8(%rax,%rbx,8)
	subq	$1, %rbx
	cmpl	$-1, %ebx
	jne	.L36
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%r12,%rbx,8), %rbp
.L26:
	movq	%rbp, %rdi
	subq	$1, %rbx
	call	__gettext_free_exp
	cmpl	$-1, %ebx
	jne	.L25
.L29:
	xorl	%eax, %eax
.L17:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$32, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L29
	movl	$0, (%rax)
	movl	%r14d, 4(%rax)
	jmp	.L17
	.size	new_exp, .-new_exp
	.p2align 4,,15
	.globl	__gettextparse
	.hidden	__gettextparse
	.type	__gettextparse, @function
__gettextparse:
	pushq	%r15
	pushq	%r14
	xorl	%ecx, %ecx
	pushq	%r13
	pushq	%r12
	leaq	yypact(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	movl	$200, %ebp
	subq	$2088, %rsp
	leaq	80(%rsp), %rax
	leaq	480(%rsp), %rbx
	movw	%cx, 80(%rsp)
	xorl	%ecx, %ecx
	movq	%rdi, 32(%rsp)
	movl	$0, 20(%rsp)
	movl	$-2, 16(%rsp)
	movq	%rbx, (%rsp)
	movq	%rax, %r14
	movq	%rax, 8(%rsp)
	movq	%rax, %r15
	movl	%ecx, %r12d
.L38:
	movslq	%r12d, %rcx
	movsbl	0(%r13,%rcx), %eax
	cmpl	$-10, %eax
	je	.L44
	cmpl	$-2, 16(%rsp)
	je	.L147
.L45:
	movl	16(%rsp), %edx
	testl	%edx, %edx
	jle	.L148
	movslq	16(%rsp), %rdx
	leaq	yytranslate(%rip), %rsi
	movzbl	(%rsi,%rdx), %edx
.L70:
	addl	%edx, %eax
.L50:
	cmpl	$54, %eax
	ja	.L44
	leaq	yycheck(%rip), %rdi
	cltq
	movsbl	(%rdi,%rax), %esi
	cmpl	%edx, %esi
	je	.L149
.L44:
	leaq	yydefact(%rip), %rax
	movzbl	(%rax,%rcx), %eax
	testl	%eax, %eax
	movq	%rax, %rdx
	je	.L80
.L81:
	movslq	%eax, %rcx
	leaq	yyr2(%rip), %rax
	movzbl	(%rax,%rcx), %edi
	movl	$1, %eax
	subl	%edi, %eax
	cmpb	$13, %dl
	movq	%rdi, %rsi
	cltq
	movq	(%rbx,%rax,8), %rax
	ja	.L82
	leaq	.L84(%rip), %rdi
	movslq	(%rdi,%rdx,4), %rdx
	addq	%rdi, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L84:
	.long	.L82-.L84
	.long	.L82-.L84
	.long	.L83-.L84
	.long	.L85-.L84
	.long	.L86-.L84
	.long	.L87-.L84
	.long	.L91-.L84
	.long	.L91-.L84
	.long	.L91-.L84
	.long	.L91-.L84
	.long	.L92-.L84
	.long	.L93-.L84
	.long	.L94-.L84
	.long	.L95-.L84
	.text
	.p2align 4,,10
	.p2align 3
.L80:
	cmpl	$3, 20(%rsp)
	jne	.L100
	cmpl	$0, 16(%rsp)
	jle	.L150
.L97:
	movsbl	0(%r13,%rcx), %eax
	movl	$-2, 16(%rsp)
	cmpl	$-10, %eax
	jne	.L151
	.p2align 4,,10
	.p2align 3
.L98:
	cmpq	%r15, %r14
	je	.L111
	subq	$2, %r14
	movswq	(%r14), %rcx
	subq	$8, %rbx
.L100:
	movsbl	0(%r13,%rcx), %eax
	cmpl	$-10, %eax
	je	.L98
.L151:
	addl	$1, %eax
	cmpl	$54, %eax
	ja	.L98
	leaq	yycheck(%rip), %rsi
	cltq
	cmpb	$1, (%rsi,%rax)
	jne	.L98
	leaq	yytable(%rip), %rdi
	movzbl	(%rdi,%rax), %r12d
	testl	%r12d, %r12d
	je	.L98
	movq	40(%rsp), %rax
	addq	$8, %rbx
	movq	%r14, %rdx
	movl	$3, 20(%rsp)
	movq	%rax, (%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L111:
	movl	$1, %ebx
.L41:
.L101:
	cmpq	8(%rsp), %r14
	je	.L37
.L43:
	movq	%r14, %rdi
	call	free@PLT
.L37:
	addq	$2088, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-16(%rbx), %rdx
	movq	(%rbx), %rax
	movl	-8(%rbx), %esi
	movq	%rdx, 48(%rsp)
	leaq	48(%rsp), %rdx
	movq	%rax, 56(%rsp)
.L141:
	movl	$2, %edi
	movl	$-1, %r12d
	call	new_exp
	leaq	-24(%rbx), %r9
	leaq	-6(%r14), %rdx
	movl	$2, %edi
.L78:
	movq	%rax, 8(%r9)
	movswl	(%rdx), %eax
	leaq	8(%r9), %rbx
	leal	(%rax,%r12), %ecx
	movl	%eax, %esi
	cmpl	$54, %ecx
	ja	.L96
	leaq	yycheck(%rip), %rax
	movslq	%ecx, %rcx
	movsbw	(%rax,%rcx), %ax
	cmpw	%ax, %si
	je	.L152
.L96:
	leaq	yydefgoto(%rip), %rax
	movslq	%edi, %rdi
	movsbl	(%rax,%rdi), %r12d
.L39:
	leaq	(%rbp,%rbp), %rax
	leaq	2(%rdx), %r14
	movw	%r12w, 2(%rdx)
	leaq	-2(%r15,%rax), %rdx
	cmpq	%rdx, %r14
	jb	.L40
	subq	%r15, %r14
	sarq	%r14
	cmpq	$9999, %rbp
	leaq	1(%r14), %rbx
	ja	.L104
	cmpq	$10000, %rax
	movl	$10000, %ebp
	cmovbe	%rax, %rbp
	leaq	0(%rbp,%rbp,4), %rax
	leaq	7(%rax,%rax), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L104
	leaq	(%rbx,%rbx), %r14
	movq	%r15, %rsi
	movq	%rax, %rdi
	salq	$3, %rbx
	movq	%r14, %rdx
	call	__GI_memcpy@PLT
	movq	(%rsp), %rsi
	leaq	(%rax,%rbp,2), %r10
	movq	%rbx, %rdx
	movq	%rax, 24(%rsp)
	movq	%r10, %rdi
	call	__GI_memcpy@PLT
	cmpq	8(%rsp), %r15
	movq	%rax, %r10
	movq	24(%rsp), %r9
	je	.L42
	movq	%r15, %rdi
	movq	%rax, (%rsp)
	call	free@PLT
	movq	24(%rsp), %r9
	movq	(%rsp), %r10
.L42:
	leaq	(%rbp,%rbp), %rax
	leaq	-2(%r9,%r14), %r14
	leaq	-8(%r10,%rbx), %rbx
	leaq	-2(%r9,%rax), %rax
	cmpq	%rax, %r14
	jnb	.L105
	movq	%r10, (%rsp)
	movq	%r9, %r15
.L40:
	cmpl	$9, %r12d
	jne	.L38
	movq	%r15, %r14
	xorl	%ebx, %ebx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L149:
	leaq	yytable(%rip), %rsi
	movzbl	(%rsi,%rax), %r12d
	testl	%r12d, %r12d
	je	.L153
	movl	20(%rsp), %eax
	movq	%r14, %rdx
	movl	$-2, 16(%rsp)
	cmpl	$1, %eax
	adcl	$-1, %eax
	addq	$8, %rbx
	movl	%eax, 20(%rsp)
	movq	40(%rsp), %rax
	movq	%rax, (%rbx)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L148:
	xorl	%edx, %edx
	movl	$0, 16(%rsp)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L147:
	movq	32(%rsp), %rsi
	movq	(%rsi), %rsi
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	je	.L54
	cmpb	$9, %dl
	je	.L49
	cmpb	$32, %dl
	jne	.L47
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %rsi
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	je	.L54
	cmpb	$32, %dl
	je	.L49
	cmpb	$9, %dl
	je	.L49
.L47:
	movsbl	%dl, %edi
	cmpb	$124, %dl
	leaq	1(%rsi), %r9
	movl	%edi, 16(%rsp)
	ja	.L71
	leaq	.L55(%rip), %r10
	movzbl	%dl, %edi
	movslq	(%r10,%rdi,4), %rdi
	addq	%r10, %rdi
	jmp	*%rdi
	.section	.rodata
	.align 4
	.align 4
.L55:
	.long	.L54-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L54-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L56-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L57-.L55
	.long	.L58-.L55
	.long	.L71-.L55
	.long	.L59-.L55
	.long	.L59-.L55
	.long	.L60-.L55
	.long	.L61-.L55
	.long	.L71-.L55
	.long	.L62-.L55
	.long	.L71-.L55
	.long	.L63-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L64-.L55
	.long	.L59-.L55
	.long	.L54-.L55
	.long	.L65-.L55
	.long	.L66-.L55
	.long	.L67-.L55
	.long	.L59-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L59-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L71-.L55
	.long	.L58-.L55
	.text
.L153:
	movq	8(%rbx), %rax
	movq	%r14, %rdx
	movq	%rbx, %r9
	movl	$-16, %edi
	jmp	.L78
.L83:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L107
	movq	32(%rsp), %rsi
	leaq	-8(%rbx), %r9
	movl	$-10, %r12d
	movl	$1, %edi
	movq	%rdx, 8(%rsi)
	leaq	-2(%r14), %rdx
	jmp	.L78
.L92:
	movq	(%rbx), %rax
	leaq	48(%rsp), %rdx
	movl	$1, %edi
	movl	$2, %esi
	movl	$-1, %r12d
	movq	%rax, 48(%rsp)
	call	new_exp
	leaq	-16(%rbx), %r9
	leaq	-4(%r14), %rdx
	movl	$2, %edi
	jmp	.L78
.L94:
	xorl	%edx, %edx
	xorl	%edi, %edi
	movl	$1, %esi
	call	new_exp
	testq	%rax, %rax
	leaq	-2(%r14), %rdx
	leaq	-8(%rbx), %r9
	je	.L108
	movq	(%rbx), %rcx
	movl	$-1, %r12d
	movl	$2, %edi
	movq	%rcx, 8(%rax)
	jmp	.L78
.L93:
	xorl	%edx, %edx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	new_exp
	movl	$-1, %r12d
	leaq	-8(%rbx), %r9
	leaq	-2(%r14), %rdx
	movl	$2, %edi
	jmp	.L78
.L85:
	movq	-16(%rbx), %rdx
	movq	(%rbx), %rax
	movl	$3, %edi
	movq	-32(%rbx), %rcx
	movl	$16, %esi
	movl	$-1, %r12d
	movq	%rdx, 56(%rsp)
	leaq	48(%rsp), %rdx
	movq	%rax, 64(%rsp)
	movq	%rcx, 48(%rsp)
	call	new_exp
	leaq	-40(%rbx), %r9
	leaq	-10(%r14), %rdx
	movl	$2, %edi
	jmp	.L78
.L86:
	movq	-16(%rbx), %rdx
	movq	(%rbx), %rax
	movl	$15, %esi
	movq	%rdx, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	48(%rsp), %rdx
	jmp	.L141
.L87:
	movq	-16(%rbx), %rdx
	movq	(%rbx), %rax
	movl	$14, %esi
	movq	%rdx, 48(%rsp)
	movq	%rax, 56(%rsp)
	leaq	48(%rsp), %rdx
	jmp	.L141
.L95:
	movq	-8(%rbx), %rax
	leaq	-24(%rbx), %r9
	leaq	-6(%r14), %rdx
	movl	$-1, %r12d
	movl	$2, %edi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L54:
	movq	32(%rsp), %rdi
	xorl	%edx, %edx
	movl	$0, 16(%rsp)
	movq	%rsi, (%rdi)
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L152:
	leaq	yytable(%rip), %rax
	movzbl	(%rax,%rcx), %r12d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L150:
	jne	.L100
.L107:
	movq	%r15, %r14
	movl	$1, %ebx
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L104:
	movq	%r15, %r14
	movl	$2, %ebx
	jmp	.L101
.L67:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	cmpb	$61, 1(%rsi)
	je	.L154
	orq	$9, %rdx
.L144:
	movq	32(%rsp), %rsi
	movq	%rdx, 40(%rsp)
	movl	$7, %edx
	movl	$259, 16(%rsp)
	movq	%r9, (%rsi)
	jmp	.L70
.L66:
	cmpb	$61, 1(%rsi)
	je	.L155
.L71:
	movq	32(%rsp), %rsi
	movl	$1, %edx
	movl	$256, 16(%rsp)
	movq	%r9, (%rsi)
	jmp	.L70
.L60:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$3, %rdx
.L146:
	movq	32(%rsp), %rsi
	movq	%rdx, 40(%rsp)
	movl	$9, %edx
	movl	$261, 16(%rsp)
	movq	%r9, (%rsi)
	jmp	.L70
.L56:
	cmpb	$61, 1(%rsi)
	je	.L156
	movq	32(%rsp), %rsi
	movl	$10, %edx
	movl	$33, 16(%rsp)
	movq	%r9, (%rsi)
	jmp	.L70
.L58:
	cmpb	1(%rsi), %dl
	jne	.L71
	leaq	2(%rsi), %r9
.L59:
	movq	32(%rsp), %rsi
	movq	%r9, (%rsi)
	jmp	.L45
.L57:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$5, %rdx
	jmp	.L146
.L64:
	movsbl	1(%rsi), %edx
	movl	16(%rsp), %edi
	subl	$48, %edi
	leal	-48(%rdx), %esi
	movslq	%edi, %rdi
	cmpb	$9, %sil
	ja	.L68
.L69:
	leaq	(%rdi,%rdi,4), %rsi
	subl	$48, %edx
	addq	$1, %r9
	movslq	%edx, %rdx
	leaq	(%rdx,%rsi,2), %rdi
	movsbl	(%r9), %edx
	leal	-48(%rdx), %esi
	cmpb	$9, %sil
	jbe	.L69
.L68:
	movq	32(%rsp), %rsi
	movq	%rdi, 40(%rsp)
	movl	$11, %edx
	movl	$262, 16(%rsp)
	movq	%r9, (%rsi)
	jmp	.L70
.L63:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$4, %rdx
	jmp	.L146
.L62:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$7, %rdx
.L145:
	movq	32(%rsp), %rsi
	movq	%rdx, 40(%rsp)
	movl	$8, %edx
	movl	$260, 16(%rsp)
	movq	%r9, (%rsi)
	jmp	.L70
.L61:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$6, %rdx
	jmp	.L145
.L65:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	cmpb	$61, 1(%rsi)
	je	.L157
	orq	$8, %rdx
	jmp	.L144
.L82:
	leaq	yyr1(%rip), %rdx
	movzbl	(%rdx,%rcx), %edi
	leaq	yypgoto(%rip), %rcx
	subl	$16, %edi
	movslq	%edi, %rdx
	movsbl	(%rcx,%rdx), %r12d
	leaq	0(,%rsi,8), %rdx
	addq	%rsi, %rsi
	subq	%rdx, %rbx
	movq	%r14, %rdx
	movq	%rbx, %r9
	subq	%rsi, %rdx
	jmp	.L78
.L108:
	movl	$-1, %r12d
	movl	$2, %edi
	jmp	.L78
.L105:
	movq	%r9, %r14
	movl	$1, %ebx
	jmp	.L43
.L154:
	orq	$11, %rdx
.L143:
	movq	32(%rsp), %rdi
	addq	$2, %rsi
	movq	%rdx, 40(%rsp)
	movl	$259, 16(%rsp)
	movl	$7, %edx
	movq	%rsi, (%rdi)
	jmp	.L70
.L157:
	orq	$10, %rdx
	jmp	.L143
.L155:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$12, %rdx
.L142:
	movq	32(%rsp), %rdi
	addq	$2, %rsi
	movq	%rdx, 40(%rsp)
	movl	$258, 16(%rsp)
	movl	$6, %edx
	movq	%rsi, (%rdi)
	jmp	.L70
.L156:
	movabsq	$-4294967296, %rdx
	andq	40(%rsp), %rdx
	orq	$13, %rdx
	jmp	.L142
	.size	__gettextparse, .-__gettextparse
	.section	.rodata
	.align 8
	.type	yyr2, @object
	.size	yyr2, 14
yyr2:
	.byte	0
	.byte	2
	.byte	1
	.byte	5
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	3
	.byte	2
	.byte	1
	.byte	1
	.byte	3
	.align 8
	.type	yyr1, @object
	.size	yyr1, 14
yyr1:
	.byte	0
	.byte	16
	.byte	17
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.byte	18
	.align 32
	.type	yycheck, @object
	.size	yycheck, 55
yycheck:
	.byte	1
	.byte	10
	.byte	11
	.byte	4
	.byte	13
	.byte	14
	.byte	8
	.byte	9
	.byte	0
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	9
	.byte	25
	.byte	7
	.byte	8
	.byte	9
	.byte	15
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	-1
	.byte	-1
	.byte	12
	.byte	3
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.align 32
	.type	yytable, @object
	.size	yytable, 55
yytable:
	.byte	7
	.byte	1
	.byte	2
	.byte	8
	.byte	3
	.byte	4
	.byte	15
	.byte	16
	.byte	9
	.byte	18
	.byte	19
	.byte	20
	.byte	21
	.byte	22
	.byte	23
	.byte	24
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	16
	.byte	26
	.byte	14
	.byte	15
	.byte	16
	.byte	17
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	0
	.byte	0
	.byte	25
	.byte	10
	.byte	11
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	12
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.byte	13
	.byte	14
	.byte	15
	.byte	16
	.type	yydefgoto, @object
	.size	yydefgoto, 3
yydefgoto:
	.byte	-1
	.byte	5
	.byte	6
	.type	yypgoto, @object
	.size	yypgoto, 3
yypgoto:
	.byte	-10
	.byte	-10
	.byte	-1
	.align 16
	.type	yydefact, @object
	.size	yydefact, 27
yydefact:
	.byte	0
	.byte	0
	.byte	12
	.byte	11
	.byte	0
	.byte	0
	.byte	2
	.byte	10
	.byte	0
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	13
	.byte	0
	.byte	4
	.byte	5
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	0
	.byte	3
	.align 16
	.type	yypact, @object
	.size	yypact, 27
yypact:
	.byte	-9
	.byte	-9
	.byte	-10
	.byte	-10
	.byte	-9
	.byte	8
	.byte	36
	.byte	-10
	.byte	13
	.byte	-10
	.byte	-9
	.byte	-9
	.byte	-9
	.byte	-9
	.byte	-9
	.byte	-9
	.byte	-9
	.byte	-10
	.byte	26
	.byte	41
	.byte	45
	.byte	18
	.byte	-2
	.byte	14
	.byte	-10
	.byte	-9
	.byte	36
	.align 32
	.type	yytranslate, @object
	.size	yytranslate, 263
yytranslate:
	.byte	0
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	10
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	5
	.byte	2
	.byte	14
	.byte	15
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	12
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	3
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	13
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	4
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	2
	.byte	1
	.byte	2
	.byte	6
	.byte	7
	.byte	8
	.byte	9
	.byte	11
