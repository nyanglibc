	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ngettext
	.type	__ngettext, @function
__ngettext:
	movq	%rdx, %rcx
	movl	$5, %r8d
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%edi, %edi
	jmp	__dcngettext
	.size	__ngettext, .-__ngettext
	.weak	ngettext
	.set	ngettext,__ngettext
	.hidden	__dcngettext
