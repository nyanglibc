	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__dcngettext
	.hidden	__dcngettext
	.type	__dcngettext, @function
__dcngettext:
	movl	%r8d, %r9d
	movq	%rcx, %r8
	movl	$1, %ecx
	jmp	__dcigettext
	.size	__dcngettext, .-__dcngettext
	.weak	dcngettext
	.set	dcngettext,__dcngettext
	.hidden	__dcigettext
