	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"plural="
.LC1:
	.string	"nplurals="
#NO_APP
	.text
	.p2align 4,,15
	.globl	__gettext_extract_plural
	.hidden	__gettext_extract_plural
	.type	__gettext_extract_plural, @function
__gettext_extract_plural:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	subq	$40, %rsp
	testq	%rdi, %rdi
	je	.L2
	leaq	.LC0(%rip), %rsi
	movq	%rdi, %rbx
	call	__GI_strstr
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rbp
	movq	%rbx, %rdi
	call	__GI_strstr
	testq	%rbp, %rbp
	je	.L2
	testq	%rax, %rax
	je	.L2
	leaq	9(%rax), %rbx
	movzbl	9(%rax), %eax
	testb	%al, %al
	je	.L2
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rcx
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L23:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L2
.L21:
	movzbl	%al, %edx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L23
	subl	$48, %eax
	cmpb	$9, %al
	ja	.L2
	leaq	8(%rsp), %rsi
	movl	$10, %edx
	movq	%rbx, %rdi
	call	__GI_strtoul
	cmpq	%rbx, 8(%rsp)
	je	.L2
	leaq	16(%rsp), %rdi
	addq	$7, %rbp
	movq	%rax, (%r12)
	movq	%rbp, 16(%rsp)
	call	__gettextparse
	testl	%eax, %eax
	je	.L24
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	__gettext_germanic_plural(%rip), %rax
	movq	%rax, 0(%r13)
	movq	$2, (%r12)
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	24(%rsp), %rax
	movq	%rax, 0(%r13)
	jmp	.L1
	.size	__gettext_extract_plural, .-__gettext_extract_plural
	.hidden	__gettext_germanic_plural
	.globl	__gettext_germanic_plural
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	__gettext_germanic_plural, @object
	.size	__gettext_germanic_plural, 32
__gettext_germanic_plural:
	.long	2
	.long	13
	.quad	plvar
	.quad	plone
	.zero	8
	.section	.rodata
	.align 32
	.type	plone, @object
	.size	plone, 32
plone:
	.long	0
	.long	1
	.quad	1
	.zero	16
	.align 32
	.type	plvar, @object
	.size	plvar, 32
plvar:
	.zero	32
	.hidden	__gettextparse
