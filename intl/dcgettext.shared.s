	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___dcgettext
	.hidden	__GI___dcgettext
	.type	__GI___dcgettext, @function
__GI___dcgettext:
	movl	%edx, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	__dcigettext
	.size	__GI___dcgettext, .-__GI___dcgettext
	.weak	dcgettext
	.set	dcgettext,__GI___dcgettext
	.globl	__dcgettext
	.set	__dcgettext,__GI___dcgettext
	.hidden	__dcigettext
