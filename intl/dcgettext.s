	.text
	.p2align 4,,15
	.globl	__dcgettext
	.hidden	__dcgettext
	.type	__dcgettext, @function
__dcgettext:
	movl	%edx, %r9d
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	__dcigettext
	.size	__dcgettext, .-__dcgettext
	.weak	dcgettext
	.set	dcgettext,__dcgettext
	.hidden	__dcigettext
