	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__dgettext
	.type	__dgettext, @function
__dgettext:
	movl	$5, %edx
	jmp	__GI___dcgettext
	.size	__dgettext, .-__dgettext
	.weak	dgettext
	.set	dgettext,__dgettext
