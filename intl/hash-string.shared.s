	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__hash_string
	.hidden	__hash_string
	.type	__hash_string, @function
__hash_string:
	xorl	%eax, %eax
	movabsq	$-4026531841, %rcx
.L2:
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.L9
.L4:
	salq	$4, %rax
	addq	$1, %rdi
	addq	%rdx, %rax
	movq	%rax, %rdx
	andl	$4026531840, %edx
	je	.L2
	shrq	$24, %rdx
	andq	%rcx, %rax
	xorq	%rdx, %rax
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	jne	.L4
.L9:
	rep ret
	.size	__hash_string, .-__hash_string
