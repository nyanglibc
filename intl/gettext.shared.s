	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__gettext
	.type	__gettext, @function
__gettext:
	movq	%rdi, %rsi
	movl	$5, %edx
	xorl	%edi, %edi
	jmp	__GI___dcgettext
	.size	__gettext, .-__gettext
	.weak	gettext
	.set	gettext,__gettext
