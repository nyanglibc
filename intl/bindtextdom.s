	.text
	.p2align 4,,15
	.type	set_binding_values.part.0, @function
set_binding_values.part.0:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$24, %rsp
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	je	.L2
	leaq	_nl_state_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L2:
	movq	_nl_domain_bindings(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L5
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L119:
	js	.L3
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3
.L5:
	leaq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L119
	testq	%r13, %r13
	je	.L7
	movq	0(%r13), %r12
	movq	8(%rbx), %r15
	testq	%r12, %r12
	je	.L120
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L121
	leaq	_nl_default_dirname(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp
	leaq	_nl_default_dirname(%rip), %rcx
	testl	%eax, %eax
	movq	%rcx, %rdx
	jne	.L122
.L13:
	cmpq	%rcx, %r15
	je	.L15
	movq	%r15, %rdi
	movq	%rdx, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rdx
.L15:
	testq	%r14, %r14
	movq	%rdx, 8(%rbx)
	movq	%rdx, 0(%r13)
	je	.L17
	movq	(%r14), %r13
	movq	16(%rbx), %r12
	movl	$1, %ebp
	testq	%r13, %r13
	jne	.L12
	movq	%r12, (%r14)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%r13, %r13
	je	.L123
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	je	.L124
	movq	%r12, %rdi
	call	strlen
	leaq	25(%rax), %rdi
	leaq	1(%rax), %rbp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L45
	leaq	24(%rax), %rdi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
.L48:
	leaq	_nl_default_dirname(%rip), %rsi
	movq	%rbx, %rdi
	call	strcmp
	testl	%eax, %eax
	leaq	_nl_default_dirname(%rip), %rdx
	jne	.L125
.L27:
	testq	%r14, %r14
	movq	%rdx, 0(%r13)
	movq	%rdx, 8(%r15)
	je	.L30
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L53
.L32:
	movq	%rax, (%r14)
	movq	%rax, 16(%r15)
.L34:
	movq	_nl_domain_bindings(%rip), %rbp
	testq	%rbp, %rbp
	je	.L35
	leaq	24(%rbp), %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	jns	.L36
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L126:
	leaq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	jle	.L37
	movq	%rbx, %rbp
.L36:
	movq	0(%rbp), %rbx
	testq	%rbx, %rbx
	jne	.L126
.L37:
	movq	%rbx, (%r15)
	movq	%r15, 0(%rbp)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%r15, 0(%r13)
.L7:
	testq	%r14, %r14
	je	.L10
.L117:
	movq	(%r14), %r13
	movq	16(%rbx), %r12
	testq	%r13, %r13
	je	.L127
.L12:
	testq	%r12, %r12
	je	.L20
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L21
.L20:
	movq	%r13, %rdi
	call	__strdup
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, %r12
	je	.L21
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	%r13, 16(%rbx)
	movq	%r13, (%r14)
.L17:
	addl	$1, _nl_msg_cat_cntr(%rip)
.L10:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L1
	addq	$24, %rsp
	leaq	_nl_state_lock(%rip), %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__pthread_rwlock_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L123:
	testq	%r14, %r14
	je	.L10
	cmpq	$0, (%r14)
	je	.L40
	movq	%r12, %rdi
	call	strlen
	leaq	25(%rax), %rdi
	leaq	1(%rax), %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L40
.L54:
	leaq	24(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	testq	%r13, %r13
	jne	.L128
	leaq	_nl_default_dirname(%rip), %rax
	testq	%r14, %r14
	movq	%rax, 8(%r15)
	je	.L30
	movq	(%r14), %rax
.L53:
	movq	%rax, %rdi
	call	__strdup
	testq	%rax, %rax
	jne	.L32
.L33:
	movq	8(%r15), %rdi
	leaq	_nl_default_dirname(%rip), %rax
	cmpq	%rax, %rdi
	je	.L38
	call	free@PLT
.L38:
	movq	%r15, %rdi
	call	free@PLT
	testq	%r13, %r13
	jne	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	movq	$0, (%r14)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	testq	%r14, %r14
	movq	%r15, 0(%r13)
	je	.L10
	movq	(%r14), %r13
	movq	16(%rbx), %r12
	testq	%r13, %r13
	jne	.L12
.L127:
	movq	%r12, (%r14)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L21:
	testl	%ebp, %ebp
	movq	%r12, (%r14)
	jne	.L17
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	movq	$0, 16(%r15)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rbp, (%r15)
	movq	%r15, _nl_domain_bindings(%rip)
	jmp	.L17
.L122:
	movq	%r12, %rdi
	movq	%rcx, 8(%rsp)
	call	__strdup
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L14
	movq	8(%rbx), %r15
	movq	8(%rsp), %rcx
	jmp	.L13
.L124:
	testq	%r14, %r14
	je	.L129
	cmpq	$0, (%r14)
	je	.L130
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r12, %rdi
	repnz scasb
	notq	%rcx
	leaq	24(%rcx), %rdi
	movq	%rcx, %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L54
.L45:
	movq	$0, 0(%r13)
.L39:
	testq	%r14, %r14
	jne	.L40
	jmp	.L10
.L14:
	testq	%r14, %r14
	movq	$0, 0(%r13)
	jne	.L117
	jmp	.L10
.L129:
	leaq	_nl_default_dirname(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L10
.L125:
	movq	%rbx, %rdi
	call	__strdup
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L27
.L28:
	movq	%r15, %rdi
	call	free@PLT
	testq	%r13, %r13
	je	.L39
	jmp	.L45
.L130:
	leaq	_nl_default_dirname(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L40
.L128:
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L48
	leaq	_nl_default_dirname(%rip), %rdx
	jmp	.L27
	.size	set_binding_values.part.0, .-set_binding_values.part.0
	.p2align 4,,15
	.globl	__bindtextdomain
	.type	__bindtextdomain, @function
__bindtextdomain:
	subq	$24, %rsp
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rsi, 8(%rsp)
	je	.L131
	cmpb	$0, (%rdi)
	jne	.L137
.L131:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L137:
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	call	set_binding_values.part.0
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	__bindtextdomain, .-__bindtextdomain
	.weak	bindtextdomain
	.set	bindtextdomain,__bindtextdomain
	.p2align 4,,15
	.globl	__bind_textdomain_codeset
	.type	__bind_textdomain_codeset, @function
__bind_textdomain_codeset:
	subq	$24, %rsp
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rsi, 8(%rsp)
	je	.L138
	cmpb	$0, (%rdi)
	jne	.L144
.L138:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	call	set_binding_values.part.0
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	__bind_textdomain_codeset, .-__bind_textdomain_codeset
	.weak	bind_textdomain_codeset
	.set	bind_textdomain_codeset,__bind_textdomain_codeset
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	__strdup
	.hidden	strlen
	.hidden	_nl_default_dirname
	.hidden	strcmp
	.hidden	_nl_state_lock
