	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_nl_find_domain
	.hidden	_nl_find_domain
	.type	_nl_find_domain, @function
_nl_find_domain:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rdx, %r12
	subq	$72, %rsp
	movl	__libc_pthread_functions_init(%rip), %r11d
	testl	%r11d, %r11d
	je	.L2
	movq	136+__libc_pthread_functions(%rip), %rax
	leaq	lock.10632(%rip), %rdi
#APP
# 86 "finddomain.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L2:
	movq	%rbx, %rdi
	call	__GI_strlen
	subq	$8, %rsp
	leaq	1(%rax), %rdx
	leaq	_nl_loaded_domains(%rip), %rdi
	pushq	$0
	pushq	%r12
	xorl	%r9d, %r9d
	pushq	$0
	pushq	$0
	xorl	%ecx, %ecx
	pushq	$0
	movq	%rbp, %r8
	movq	%rbx, %rsi
	call	_nl_make_l10nflist@PLT
	movl	__libc_pthread_functions_init(%rip), %r10d
	addq	$48, %rsp
	movq	%rax, %r14
	testl	%r10d, %r10d
	je	.L3
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	lock.10632(%rip), %rdi
#APP
# 94 "finddomain.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L3:
	testq	%r14, %r14
	je	.L4
	movl	8(%r14), %r9d
	testl	%r9d, %r9d
	jle	.L43
.L5:
	cmpq	$0, 16(%r14)
	je	.L44
.L1:
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L1
	leaq	32(%r14), %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rbx), %rax
	cmpq	$0, 16(%rax)
	jne	.L1
	addq	$8, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1
.L8:
	movl	8(%rdi), %r8d
	testl	%r8d, %r8d
	jg	.L7
	movq	%r13, %rsi
	call	_nl_load_domain
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_nl_load_domain
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, %rdi
	call	_nl_expand_alias@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L9
	movq	%rax, %rdi
	call	__GI_strlen
	leaq	1(%rax), %r15
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L1
	movq	8(%rsp), %rsi
	movq	%r15, %rdx
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
.L9:
	leaq	40(%rsp), %rcx
	leaq	32(%rsp), %rdx
	leaq	24(%rsp), %rsi
	leaq	56(%rsp), %r9
	leaq	48(%rsp), %r8
	movq	%rbp, %rdi
	call	_nl_explode_name@PLT
	cmpl	$-1, %eax
	movl	%eax, %r15d
	je	.L1
	movl	__libc_pthread_functions_init(%rip), %esi
	testl	%esi, %esi
	je	.L12
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	lock.10632(%rip), %rdi
#APP
# 143 "finddomain.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L12:
	movq	%rbx, %rdi
	call	__GI_strlen
	subq	$8, %rsp
	leaq	1(%rax), %rdx
	leaq	_nl_loaded_domains(%rip), %rdi
	pushq	$1
	pushq	%r12
	movl	%r15d, %ecx
	pushq	56(%rsp)
	pushq	88(%rsp)
	movq	%rbx, %rsi
	pushq	88(%rsp)
	movq	88(%rsp), %r9
	movq	72(%rsp), %r8
	call	_nl_make_l10nflist@PLT
	movl	__libc_pthread_functions_init(%rip), %ecx
	addq	$48, %rsp
	movq	%rax, %r14
	testl	%ecx, %ecx
	je	.L13
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	lock.10632(%rip), %rdi
#APP
# 152 "finddomain.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L13:
	testq	%r14, %r14
	je	.L15
	movl	8(%r14), %edx
	testl	%edx, %edx
	jle	.L45
.L16:
	cmpq	$0, 16(%r14)
	je	.L46
.L17:
	cmpq	$0, 8(%rsp)
	je	.L15
	movq	%rbp, %rdi
	call	free@PLT
.L15:
	andl	$1, %r15d
	je	.L1
	movq	56(%rsp), %rdi
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L46:
	movq	32(%r14), %rdi
	testq	%rdi, %rdi
	je	.L17
	leaq	32(%r14), %rbx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%rbx), %rax
	cmpq	$0, 16(%rax)
	jne	.L17
	addq	$8, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L17
.L19:
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jg	.L18
	movq	%r13, %rsi
	call	_nl_load_domain
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	_nl_load_domain
	jmp	.L16
	.size	_nl_find_domain, .-_nl_find_domain
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	_nl_finddomain_subfreeres
	.hidden	_nl_finddomain_subfreeres
	.type	_nl_finddomain_subfreeres, @function
_nl_finddomain_subfreeres:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	_nl_loaded_domains(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L50
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rbp, %rbx
.L50:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L49
	call	_nl_unload_domain
.L49:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rbp
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L51
.L47:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	_nl_finddomain_subfreeres, .-_nl_finddomain_subfreeres
	.local	lock.10632
	.comm	lock.10632,56,32
	.local	_nl_loaded_domains
	.comm	_nl_loaded_domains,8,8
	.hidden	_nl_unload_domain
	.hidden	_nl_load_domain
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
