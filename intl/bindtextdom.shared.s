	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	set_binding_values.part.0, @function
set_binding_values.part.0:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$24, %rsp
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L2
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	_nl_state_lock(%rip), %rdi
#APP
# 91 "bindtextdom.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L2:
	movq	_nl_domain_bindings@GOTPCREL(%rip), %r15
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	jne	.L5
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L116:
	js	.L3
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L3
.L5:
	leaq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L116
	testq	%r13, %r13
	je	.L7
	movq	0(%r13), %r12
	movq	8(%rbx), %r15
	testq	%r12, %r12
	je	.L117
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L118
	leaq	__GI__nl_default_dirname(%rip), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	leaq	__GI__nl_default_dirname(%rip), %rcx
	testl	%eax, %eax
	movq	%rcx, %rdx
	jne	.L119
.L13:
	cmpq	%rcx, %r15
	je	.L15
	movq	%r15, %rdi
	movq	%rdx, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rdx
.L15:
	testq	%r14, %r14
	movq	%rdx, 8(%rbx)
	movq	%rdx, 0(%r13)
	je	.L17
	movq	(%r14), %r13
	movq	16(%rbx), %r12
	movl	$1, %ebp
	testq	%r13, %r13
	jne	.L12
	movq	%r12, (%r14)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L3:
	testq	%r13, %r13
	je	.L120
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	je	.L121
	movq	%r12, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rdx
	leaq	25(%rax), %rdi
	movq	%rdx, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	8(%rsp), %rdx
	je	.L45
	leaq	24(%rax), %rdi
	movq	%r12, %rsi
	call	__GI_memcpy@PLT
.L48:
	leaq	__GI__nl_default_dirname(%rip), %rsi
	movq	%rbx, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	leaq	__GI__nl_default_dirname(%rip), %rdx
	jne	.L122
.L27:
	testq	%r14, %r14
	movq	%rdx, 0(%r13)
	movq	%rdx, 8(%rbp)
	je	.L30
	movq	(%r14), %rax
	testq	%rax, %rax
	jne	.L53
.L32:
	movq	%rax, (%r14)
	movq	%rax, 16(%rbp)
.L34:
	movq	(%r15), %r13
	testq	%r13, %r13
	je	.L35
	leaq	24(%r13), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jns	.L36
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L123:
	leaq	24(%rbx), %rsi
	movq	%r12, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	jle	.L37
	movq	%rbx, %r13
.L36:
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L123
.L37:
	movq	%rbx, 0(%rbp)
	movq	%rbp, 0(%r13)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%r15, 0(%r13)
.L7:
	testq	%r14, %r14
	je	.L10
.L114:
	movq	(%r14), %r13
	movq	16(%rbx), %r12
	testq	%r13, %r13
	je	.L124
.L12:
	testq	%r12, %r12
	je	.L20
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L21
.L20:
	movq	%r13, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, %r12
	je	.L21
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	%r13, 16(%rbx)
	movq	%r13, (%r14)
.L17:
	movq	_nl_msg_cat_cntr@GOTPCREL(%rip), %rax
	addl	$1, (%rax)
.L10:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
.L126:
	movq	152+__libc_pthread_functions(%rip), %rax
	addq	$24, %rsp
	leaq	_nl_state_lock(%rip), %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
#APP
# 312 "bindtextdom.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L120:
	testq	%r14, %r14
	je	.L10
	cmpq	$0, (%r14)
	je	.L40
	movq	%r12, %rdi
	call	__GI_strlen
	leaq	25(%rax), %rdi
	leaq	1(%rax), %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L40
.L54:
	leaq	24(%rbp), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	__GI_memcpy@PLT
	testq	%r13, %r13
	jne	.L125
	leaq	__GI__nl_default_dirname(%rip), %rax
	testq	%r14, %r14
	movq	%rax, 8(%rbp)
	je	.L30
	movq	(%r14), %rax
.L53:
	movq	%rax, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	jne	.L32
.L33:
	movq	8(%rbp), %rdi
	leaq	__GI__nl_default_dirname(%rip), %rax
	cmpq	%rax, %rdi
	je	.L38
	call	free@PLT
.L38:
	movq	%rbp, %rdi
	call	free@PLT
	testq	%r13, %r13
	jne	.L45
	.p2align 4,,10
	.p2align 3
.L40:
	movq	$0, (%r14)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L118:
	testq	%r14, %r14
	movq	%r15, 0(%r13)
	je	.L10
	movq	(%r14), %r13
	movq	16(%rbx), %r12
	testq	%r13, %r13
	jne	.L12
.L124:
	movl	__libc_pthread_functions_init(%rip), %eax
	movq	%r12, (%r14)
	testl	%eax, %eax
	jne	.L126
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	testl	%ebp, %ebp
	movq	%r12, (%r14)
	jne	.L17
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	movq	$0, 16(%rbp)
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%r13, 0(%rbp)
	movq	%rbp, (%r15)
	jmp	.L17
.L119:
	movq	%r12, %rdi
	movq	%rcx, 8(%rsp)
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L14
	movq	8(%rbx), %r15
	movq	8(%rsp), %rcx
	jmp	.L13
.L121:
	testq	%r14, %r14
	je	.L127
	cmpq	$0, (%r14)
	je	.L128
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r12, %rdi
	repnz scasb
	notq	%rcx
	leaq	24(%rcx), %rdi
	movq	%rcx, %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L54
.L45:
	movq	$0, 0(%r13)
.L39:
	testq	%r14, %r14
	jne	.L40
	jmp	.L10
.L14:
	testq	%r14, %r14
	movq	$0, 0(%r13)
	jne	.L114
	jmp	.L10
.L127:
	leaq	__GI__nl_default_dirname(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L10
.L122:
	movq	%rbx, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rdx
	jne	.L27
.L28:
	movq	%rbp, %rdi
	call	free@PLT
	testq	%r13, %r13
	je	.L39
	jmp	.L45
.L128:
	leaq	__GI__nl_default_dirname(%rip), %rax
	movq	%rax, 0(%r13)
	jmp	.L40
.L125:
	movq	0(%r13), %rbx
	testq	%rbx, %rbx
	jne	.L48
	leaq	__GI__nl_default_dirname(%rip), %rdx
	jmp	.L27
	.size	set_binding_values.part.0, .-set_binding_values.part.0
	.p2align 4,,15
	.globl	__bindtextdomain
	.type	__bindtextdomain, @function
__bindtextdomain:
	subq	$24, %rsp
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rsi, 8(%rsp)
	je	.L129
	cmpb	$0, (%rdi)
	jne	.L135
.L129:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	call	set_binding_values.part.0
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	__bindtextdomain, .-__bindtextdomain
	.weak	bindtextdomain
	.set	bindtextdomain,__bindtextdomain
	.p2align 4,,15
	.globl	__bind_textdomain_codeset
	.type	__bind_textdomain_codeset, @function
__bind_textdomain_codeset:
	subq	$24, %rsp
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movq	%rsi, 8(%rsp)
	je	.L136
	cmpb	$0, (%rdi)
	jne	.L142
.L136:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	call	set_binding_values.part.0
	movq	8(%rsp), %rax
	addq	$24, %rsp
	ret
	.size	__bind_textdomain_codeset, .-__bind_textdomain_codeset
	.weak	bind_textdomain_codeset
	.set	bind_textdomain_codeset,__bind_textdomain_codeset
	.hidden	_nl_state_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
