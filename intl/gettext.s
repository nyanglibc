	.text
	.p2align 4,,15
	.globl	__gettext
	.type	__gettext, @function
__gettext:
	movq	%rdi, %rsi
	movl	$5, %edx
	xorl	%edi, %edi
	jmp	__dcgettext
	.size	__gettext, .-__gettext
	.weak	gettext
	.set	gettext,__gettext
	.hidden	__dcgettext
