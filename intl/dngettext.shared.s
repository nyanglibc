	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__dngettext
	.type	__dngettext, @function
__dngettext:
	movl	$5, %r8d
	jmp	__dcngettext
	.size	__dngettext, .-__dngettext
	.weak	dngettext
	.set	dngettext,__dngettext
	.hidden	__dcngettext
