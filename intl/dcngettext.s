	.text
	.p2align 4,,15
	.globl	__dcngettext
	.hidden	__dcngettext
	.type	__dcngettext, @function
__dcngettext:
	movl	%r8d, %r9d
	movq	%rcx, %r8
	movl	$1, %ecx
	jmp	__dcigettext
	.size	__dcngettext, .-__dcngettext
	.weak	dcngettext
	.set	dcngettext,__dcngettext
	.hidden	__dcigettext
