	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_tsd\n"
.LC1:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	td_thr_tsd
	.type	td_thr_tsd, @function
td_thr_tsd:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	%rdx, %r13
	subq	$48, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L20
	movq	(%rbx), %rdi
	movq	352(%rdi), %r8
	testq	%r8, %r8
	je	.L21
.L3:
	leaq	360(%rdi), %rsi
	leaq	-72(%rbp), %r9
	movl	%r12d, %ecx
	movl	$32, %edx
	call	_td_fetch_value
	cmpl	$16, %eax
	je	.L11
	testl	%eax, %eax
	jne	.L1
	testb	$1, -72(%rbp)
	movl	$9, %eax
	je	.L1
	movq	(%rbx), %rdi
	leaq	-56(%rbp), %r14
	movl	$1, %ecx
	movl	$40, %edx
	movq	$0, -56(%rbp)
	movq	%r14, %r8
	leaq	432(%rdi), %rsi
	call	_td_locate_field
	testl	%eax, %eax
	je	.L22
.L1:
	leaq	-32(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC0(%rip), %rsi
	movl	$2, %edi
	movl	$11, %edx
	call	write@PLT
	movq	(%rbx), %rdi
	movq	352(%rdi), %r8
	testq	%r8, %r8
	jne	.L3
.L21:
	leaq	352(%rdi), %rcx
	movq	16(%rdi), %rdi
	leaq	.LC1(%rip), %rsi
	movl	$31, %edx
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1
	movq	(%rbx), %rdi
	movq	352(%rdi), %r8
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L22:
	movq	(%rbx), %rdi
	movl	%r12d, %eax
	xorl	%edx, %edx
	movq	8(%rbx), %r8
	leaq	-64(%rbp), %r9
	divl	436(%rdi)
	leaq	112(%rdi), %rsi
	movl	%edx, %r12d
	movl	%eax, %ecx
	movl	$8, %edx
	call	_td_fetch_value
	cmpl	$16, %eax
	je	.L7
	testl	%eax, %eax
	jne	.L1
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L9
	movq	(%rbx), %rdi
	movl	%r12d, %ecx
	movq	%r14, %r8
	movl	$40, %edx
	movq	%rax, -56(%rbp)
	leaq	432(%rdi), %rsi
	call	_td_locate_field
	cmpl	$16, %eax
	je	.L7
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	movl	400(%rdi), %eax
	testl	%eax, %eax
	jne	.L8
	leaq	400(%rdi), %rsi
	movl	$36, %edx
	call	_td_check_sizeof
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	movl	400(%rdi), %eax
.L8:
	movl	%eax, %ecx
	movq	16(%rdi), %rdi
	movq	-56(%rbp), %rsi
	leaq	30(%rcx), %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	movq	%r12, %rdx
	call	ps_pdread@PLT
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1
	movq	(%rbx), %rdi
	leaq	-48(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r12, %r8
	movl	$37, %edx
	leaq	404(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movq	-72(%rbp), %rax
	cmpq	%rax, -48(%rbp)
	je	.L23
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$17, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	leaq	-32(%rbp), %rsp
	movl	$9, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$15, %eax
	jmp	.L1
.L23:
	movq	(%rbx), %rdi
	leaq	-40(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r12, %r8
	movl	$38, %edx
	leaq	416(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movq	-40(%rbp), %rdx
	movq	%rdx, 0(%r13)
	jmp	.L1
	.size	td_thr_tsd, .-td_thr_tsd
	.hidden	_td_fetch_value_local
	.hidden	_td_check_sizeof
	.hidden	td_mod_lookup
	.hidden	_td_locate_field
	.hidden	_td_fetch_value
	.hidden	__td_debug
