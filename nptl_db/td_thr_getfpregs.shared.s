	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_getfpregs\n"
	.text
	.p2align 4,,15
	.globl	td_thr_getfpregs
	.type	td_thr_getfpregs, @function
td_thr_getfpregs:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	movq	8(%rbp), %r8
	movq	0(%rbp), %rdi
	testq	%r8, %r8
	je	.L9
.L3:
	leaq	76(%rdi), %rsi
	xorl	%ecx, %ecx
	movl	$5, %edx
	movq	%rsp, %r9
	call	_td_fetch_value
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1
	testb	$32, (%rsp)
	jne	.L10
	movq	0(%rbp), %rdi
	movq	8(%rbp), %r8
	leaq	8(%rsp), %r9
	xorl	%ecx, %ecx
	movl	$3, %edx
	leaq	52(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	movl	%eax, %edx
	je	.L11
.L1:
	addq	$24, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	movl	$2, %edi
	movl	$17, %edx
	call	write@PLT
	movq	8(%rbp), %r8
	movq	0(%rbp), %rdi
	testq	%r8, %r8
	jne	.L3
.L9:
	movq	16(%rdi), %rdi
	call	ps_getpid@PLT
	movq	0(%rbp), %rdx
	movl	%eax, %esi
	movq	16(%rdx), %rdi
	movq	%rbx, %rdx
	call	ps_lgetfpregs@PLT
	xorl	%edx, %edx
	testl	%eax, %eax
	setne	%dl
	addq	$24, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	0(%rbp), %rax
	movl	8(%rsp), %esi
	movq	%rbx, %rdx
	movq	16(%rax), %rdi
	call	ps_lgetfpregs@PLT
	xorl	%edx, %edx
	testl	%eax, %eax
	setne	%dl
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	8(%rbx), %rdi
	movq	$0, (%rbx)
	movq	$0, 504(%rbx)
	xorl	%eax, %eax
	andq	$-8, %rdi
	subq	%rdi, %rbx
	leal	512(%rbx), %ecx
	shrl	$3, %ecx
	rep stosq
	addq	$24, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	td_thr_getfpregs, .-td_thr_getfpregs
	.hidden	_td_fetch_value
	.hidden	__td_debug
