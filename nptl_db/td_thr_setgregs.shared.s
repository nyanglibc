	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_setgregs\n"
	.text
	.p2align 4,,15
	.globl	td_thr_setgregs
	.type	td_thr_setgregs, @function
td_thr_setgregs:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L7
	movq	8(%rbx), %r8
	movq	(%rbx), %rdi
	testq	%r8, %r8
	je	.L8
.L3:
	leaq	76(%rdi), %rsi
	xorl	%ecx, %ecx
	movq	%rsp, %r9
	movl	$5, %edx
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	testb	$32, (%rsp)
	je	.L9
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	.LC0(%rip), %rsi
	movl	$2, %edi
	movl	$16, %edx
	call	write@PLT
	movq	8(%rbx), %r8
	movq	(%rbx), %rdi
	testq	%r8, %r8
	jne	.L3
.L8:
	movq	16(%rdi), %rdi
	call	ps_getpid@PLT
	movq	(%rbx), %rdx
	movl	%eax, %esi
	movq	16(%rdx), %rdi
	movq	%rbp, %rdx
	call	ps_lsetregs@PLT
	testl	%eax, %eax
	setne	%al
	addq	$24, %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	(%rbx), %rdi
	movq	8(%rbx), %r8
	leaq	8(%rsp), %r9
	xorl	%ecx, %ecx
	movl	$3, %edx
	leaq	52(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rax
	movl	8(%rsp), %esi
	movq	%rbp, %rdx
	movq	16(%rax), %rdi
	call	ps_lsetregs@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	jmp	.L1
	.size	td_thr_setgregs, .-td_thr_setgregs
	.hidden	_td_fetch_value
	.hidden	__td_debug
