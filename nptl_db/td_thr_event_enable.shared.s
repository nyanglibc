	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_event_enable\n"
.LC1:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	td_thr_event_enable
	.type	td_thr_event_enable, @function
td_thr_event_enable:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L12
.L2:
	movq	8(%rbx), %r8
	movq	(%rbx), %rdi
	testq	%r8, %r8
	je	.L3
	xorl	%r9d, %r9d
	leaq	40(%rdi), %rsi
	testl	%ebp, %ebp
	setne	%r9b
	movl	$2, %edx
	xorl	%ecx, %ecx
	call	_td_store_value
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
.L3:
	movq	328(%rdi), %r8
	testq	%r8, %r8
	je	.L13
.L5:
	xorl	%r9d, %r9d
	testl	%ebp, %ebp
	leaq	336(%rdi), %rsi
	setne	%r9b
	addq	$8, %rsp
	xorl	%ecx, %ecx
	popq	%rbx
	popq	%rbp
	movl	$30, %edx
	jmp	_td_store_value
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	.LC0(%rip), %rsi
	movl	$20, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	328(%rdi), %rcx
	movq	16(%rdi), %rdi
	leaq	.LC1(%rip), %rsi
	movl	$29, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L6
	movq	(%rbx), %rdi
	movq	328(%rdi), %r8
	jmp	.L5
	.size	td_thr_event_enable, .-td_thr_event_enable
	.hidden	td_mod_lookup
	.hidden	_td_store_value
	.hidden	__td_debug
