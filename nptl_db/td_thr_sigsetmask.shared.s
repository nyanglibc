	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_sigsetmask\n"
	.text
	.p2align 4,,15
	.globl	td_thr_sigsetmask
	.type	td_thr_sigsetmask, @function
td_thr_sigsetmask:
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	movl	$18, %edx
	movl	$2, %edi
	call	write@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	td_thr_sigsetmask, .-td_thr_sigsetmask
	.hidden	__td_debug
