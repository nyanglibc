	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_set_event\n"
.LC1:
	.string	"td_thr_set_event.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(th->th_ta_p)->ta_sizeof_td_thr_events_t != 0"
	.text
	.p2align 4,,15
	.globl	td_thr_set_event
	.type	td_thr_set_event, @function
td_thr_set_event:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %r15
	subq	$40, %rsp
	movl	__td_debug(%rip), %esi
	testl	%esi, %esi
	jne	.L18
.L2:
	movq	(%r15), %rdi
	movq	8(%r15), %rax
	leaq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$10, %edx
	leaq	136(%rdi), %rsi
	movq	%rax, -64(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L1
	movq	(%r15), %rdi
	movl	200(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L19
.L4:
	leaq	30(%rcx), %rax
	movq	16(%rdi), %rdi
	movq	-64(%rbp), %rsi
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L11
	leaq	-56(%rbp), %r13
	xorl	%r14d, %r14d
.L6:
	movq	(%r15), %rdi
	movq	%r13, %r9
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$17, %edx
	movl	%r14d, -68(%rbp)
	leaq	204(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L5
	movq	(%r15), %rdi
	movq	-56(%rbp), %r9
	movq	%rbx, %r8
	orl	(%r12,%r14,4), %r9d
	movq	%r14, %rcx
	movl	$17, %edx
	leaq	204(%rdi), %rsi
	movq	%r9, -56(%rbp)
	call	_td_store_value_local
	testl	%eax, %eax
	jne	.L5
	cmpq	$1, %r14
	jne	.L12
.L9:
	movq	(%r15), %rax
	movl	200(%rax), %ecx
	testl	%ecx, %ecx
	je	.L20
	movq	16(%rax), %rdi
	movq	-64(%rbp), %rsi
	movq	%rbx, %rdx
	call	ps_pdwrite@PLT
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	200(%rdi), %rsi
	movl	$16, %edx
	call	_td_check_sizeof
	testl	%eax, %eax
	jne	.L1
	movq	(%r15), %rdi
	movl	200(%rdi), %ecx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$1, %r14d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC0(%rip), %rsi
	movl	$17, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	$16, %eax
	jne	.L1
	movl	-68(%rbp), %eax
	movl	(%r12,%rax,4), %ecx
	testl	%ecx, %ecx
	jne	.L14
	testl	%eax, %eax
	jne	.L9
	movl	4(%r12), %eax
	testl	%eax, %eax
	je	.L9
.L14:
	movl	$13, %eax
	jmp	.L1
.L20:
	leaq	__PRETTY_FUNCTION__.9038(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$71, %edx
	call	__assert_fail@PLT
.L11:
	movl	$1, %eax
	jmp	.L1
	.size	td_thr_set_event, .-td_thr_set_event
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9038, @object
	.size	__PRETTY_FUNCTION__.9038, 17
__PRETTY_FUNCTION__.9038:
	.string	"td_thr_set_event"
	.hidden	_td_check_sizeof
	.hidden	_td_store_value_local
	.hidden	_td_fetch_value_local
	.hidden	_td_locate_field
	.hidden	__td_debug
