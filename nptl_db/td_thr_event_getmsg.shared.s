	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_event_getmsg\n"
.LC1:
	.string	"td_thr_event_getmsg.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"(th->th_ta_p)->ta_sizeof_td_eventbuf_t != 0"
	.section	.rodata.str1.1
.LC3:
	.string	"libpthread.so.0"
.LC4:
	.string	"err == TD_OK"
	.text
	.p2align 4,,15
	.globl	td_thr_event_getmsg
	.type	td_thr_event_getmsg, @function
td_thr_event_getmsg:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	__td_debug(%rip), %edx
	testl	%edx, %edx
	jne	.L28
.L2:
	movq	(%rbx), %rdi
	movq	8(%rbx), %rax
	leaq	-88(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$9, %edx
	leaq	124(%rdi), %rsi
	movq	%rax, -88(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	movl	216(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L29
.L4:
	leaq	30(%rcx), %rax
	movq	16(%rdi), %rdi
	movq	-88(%rbp), %rsi
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	movq	%r13, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L19
	movq	(%rbx), %rdi
	leaq	-80(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$19, %edx
	leaq	220(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L18
	movq	(%rbx), %rdi
	leaq	-72(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$20, %edx
	leaq	232(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	je	.L30
.L1:
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	216(%rdi), %rsi
	movl	$18, %edx
	call	_td_check_sizeof
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	movl	216(%rdi), %ecx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	.LC0(%rip), %rsi
	movl	$20, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$10, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L30:
	movq	-72(%rbp), %rax
	movq	%rbx, 8(%r12)
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%rax, 16(%r12)
	movq	-80(%rbp), %rax
	movl	%eax, (%r12)
	movq	(%rbx), %r12
	movl	216(%r12), %edx
	call	memset@PLT
	movl	216(%r12), %ecx
	testl	%ecx, %ecx
	je	.L31
	movq	16(%r12), %rdi
	movq	-88(%rbp), %rsi
	movq	%r13, %rdx
	call	ps_pdwrite@PLT
	testl	%eax, %eax
	jne	.L19
	movq	(%rbx), %rdi
	movq	304(%rdi), %r8
	testq	%r8, %r8
	je	.L32
	movq	%r8, -56(%rbp)
.L9:
	leaq	312(%rdi), %rsi
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$28, %edx
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	cmpq	$0, -64(%rbp)
	leaq	-48(%rbp), %r12
	leaq	-56(%rbp), %r13
	jne	.L16
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	movq	-48(%rbp), %rdx
	movq	-64(%rbp), %rax
	cmpq	%rax, %rdx
	je	.L20
	cmpq	8(%rbx), %rax
	movq	(%rbx), %rdi
	leaq	160(%rdi), %rsi
	je	.L33
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$12, %edx
	movq	%rax, -56(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L34
	movq	-48(%rbp), %rax
	testq	%rax, %rax
	movq	%rax, -64(%rbp)
	je	.L20
.L16:
	movq	(%rbx), %rdi
	movq	8(%rbx), %r8
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movl	$12, %edx
	leaq	160(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	je	.L35
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %eax
	jmp	.L1
.L20:
	movl	$15, %eax
	jmp	.L1
.L32:
	leaq	304(%rdi), %rcx
	movq	16(%rdi), %rdi
	leaq	.LC3(%rip), %rsi
	movl	$27, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L19
	movq	(%rbx), %rdi
	movq	304(%rdi), %r8
	testq	%r8, %r8
	movq	%r8, -56(%rbp)
	jne	.L9
	leaq	304(%rdi), %rcx
	movq	16(%rdi), %rdi
	leaq	.LC3(%rip), %rsi
	movl	$27, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L19
	movq	(%rbx), %rdi
	movq	304(%rdi), %r8
	jmp	.L9
.L33:
	leaq	-40(%rbp), %r8
	movq	%rdx, -40(%rbp)
	xorl	%ecx, %ecx
	movl	$12, %edx
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L36
	movq	-56(%rbp), %r8
	cmpq	-40(%rbp), %r8
	movl	$15, %eax
	je	.L1
	movq	(%rbx), %rdi
	movq	-48(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$-1, %edx
	leaq	312(%rdi), %rsi
	call	_td_store_value
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	movq	-64(%rbp), %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$12, %edx
	leaq	160(%rdi), %rsi
	call	_td_store_value
	jmp	.L1
.L31:
	leaq	__PRETTY_FUNCTION__.9063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$63, %edx
	call	__assert_fail@PLT
.L34:
	leaq	__PRETTY_FUNCTION__.9063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$112, %edx
	call	__assert_fail@PLT
.L36:
	leaq	__PRETTY_FUNCTION__.9063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$95, %edx
	call	__assert_fail@PLT
	.size	td_thr_event_getmsg, .-td_thr_event_getmsg
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9063, @object
	.size	__PRETTY_FUNCTION__.9063, 20
__PRETTY_FUNCTION__.9063:
	.string	"td_thr_event_getmsg"
	.hidden	_td_store_value
	.hidden	td_mod_lookup
	.hidden	_td_fetch_value
	.hidden	_td_check_sizeof
	.hidden	_td_fetch_value_local
	.hidden	_td_locate_field
	.hidden	__td_debug
