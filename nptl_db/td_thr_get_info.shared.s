	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_get_info\n"
.LC1:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	td_thr_get_info
	.type	td_thr_get_info, @function
td_thr_get_info:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L32
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	testq	%rsi, %rsi
	je	.L33
.L3:
	movl	24(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L8
	leaq	24(%rdi), %rsi
	xorl	%edx, %edx
	call	_td_check_sizeof
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movl	24(%rdi), %ecx
.L8:
	leaq	30(%rcx), %rax
	movq	16(%rdi), %rdi
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	movq	%r13, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	je	.L34
.L9:
	movl	$1, %r14d
.L1:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movq	(%rbx), %rdi
	movq	8(%rbx), %rax
	leaq	-104(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$8, %edx
	leaq	112(%rdi), %rsi
	movq	%rax, -104(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	(%rbx), %rdi
	leaq	-96(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$6, %edx
	leaq	88(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	(%rbx), %rdi
	leaq	-88(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$7, %edx
	leaq	100(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	(%rbx), %rdi
	leaq	-72(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$3, %edx
	leaq	52(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	(%rbx), %rdi
	leaq	-80(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$5, %edx
	leaq	76(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	(%rbx), %rdi
	leaq	-64(%rbp), %r9
	movq	%r13, %r8
	xorl	%ecx, %ecx
	movl	$2, %edx
	leaq	40(%rdi), %rsi
	call	_td_fetch_value_local
	movl	%eax, %r14d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	.LC0(%rip), %rsi
	movl	$2, %edi
	movl	$16, %edx
	call	write@PLT
	movq	8(%rbx), %rsi
	movq	(%rbx), %rdi
	testq	%rsi, %rsi
	jne	.L3
.L33:
	movq	328(%rdi), %r8
	movq	$0, -104(%rbp)
	movq	$0, -80(%rbp)
	movq	$0, -96(%rbp)
	movq	$0, -88(%rbp)
	movq	$0, -72(%rbp)
	testq	%r8, %r8
	je	.L35
.L4:
	leaq	336(%rdi), %rsi
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$30, %edx
	xorl	%r13d, %r13d
	call	_td_fetch_value
	movl	%eax, %r14d
.L7:
	testl	%r14d, %r14d
	jne	.L1
	leaq	8(%r12), %rdi
	movq	%r12, %rcx
	xorl	%eax, %eax
	movq	$0, (%r12)
	movq	$0, 376(%r12)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$384, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	8(%rbx), %rax
	movq	%rax, 16(%r12)
	movq	-104(%rbp), %rax
	movq	%rax, 24(%r12)
	xorl	%eax, %eax
	cmpq	$0, -96(%rbp)
	je	.L10
	movl	-88(%rbp), %eax
.L10:
	movl	%eax, 100(%r12)
	movq	-80(%rbp), %rax
	movl	$1, 76(%r12)
	testb	$16, %al
	jne	.L11
	movl	$4, 68(%r12)
.L12:
	movq	-72(%rbp), %rdx
	movq	(%rbx), %rcx
	testq	%rdx, %rdx
	movq	%rcx, (%r12)
	movl	%edx, %eax
	je	.L36
.L15:
	cmpq	$0, -64(%rbp)
	movl	%eax, 104(%r12)
	setne	240(%r12)
	testq	%r13, %r13
	je	.L1
	movq	(%rbx), %rdi
	leaq	32(%r12), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$4, %edx
	leaq	64(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L26
	movq	(%rbx), %rdi
	leaq	-56(%rbp), %r15
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$11, %edx
	movq	%r15, %r9
	leaq	148(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L27
	movq	(%rbx), %rdi
	movq	-56(%rbp), %rax
	movq	%r15, %r9
	movq	%r13, %r8
	movl	$1, %ecx
	movl	$11, %edx
	leaq	148(%rdi), %rsi
	movl	%eax, 376(%r12)
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L28
	movq	-56(%rbp), %rax
	movl	%eax, 380(%r12)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	328(%rdi), %rcx
	movq	16(%rdi), %rdi
	leaq	.LC1(%rip), %rsi
	movl	$29, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L9
	movq	(%rbx), %rdi
	movq	328(%rdi), %r8
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L11:
	andl	$32, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$4, %eax
	addl	$1, %eax
	movl	%eax, 68(%r12)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L36:
	movq	16(%rcx), %rdi
	call	ps_getpid@PLT
	jmp	.L15
.L27:
	movl	%eax, %r14d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
.L17:
	cmpl	$16, %r14d
	jne	.L1
	movl	$2, %eax
	addq	$94, %rdx
	xorl	%esi, %esi
	subl	%ecx, %eax
	leaq	(%r12,%rdx,4), %rdx
	salq	$2, %rax
	cmpl	$8, %eax
	jnb	.L19
	testb	$4, %al
	jne	.L37
	testl	%eax, %eax
	je	.L1
	movb	$0, (%rdx)
	jmp	.L1
.L28:
	movl	%eax, %r14d
	movl	$1, %ecx
	movl	$1, %edx
	jmp	.L17
.L19:
	movl	%eax, %ecx
	movq	$0, (%rdx)
	movq	$0, -8(%rdx,%rcx)
	leaq	8(%rdx), %rcx
	andq	$-8, %rcx
	subq	%rcx, %rdx
	addl	%edx, %eax
	andl	$-8, %eax
	cmpl	$8, %eax
	jb	.L1
	andl	$-8, %eax
	xorl	%edx, %edx
.L23:
	movl	%edx, %edi
	addl	$8, %edx
	cmpl	%eax, %edx
	movq	%rsi, (%rcx,%rdi)
	jb	.L23
	jmp	.L1
.L37:
	movl	%eax, %eax
	movl	$0, (%rdx)
	movl	$0, -4(%rdx,%rax)
	jmp	.L1
.L26:
	movl	%eax, %r14d
	jmp	.L1
	.size	td_thr_get_info, .-td_thr_get_info
	.hidden	td_mod_lookup
	.hidden	_td_fetch_value
	.hidden	_td_fetch_value_local
	.hidden	_td_locate_field
	.hidden	_td_check_sizeof
	.hidden	__td_debug
