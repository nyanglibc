	.text
	.p2align 4,,15
	.globl	td_thr_tlsbase
	.type	td_thr_tlsbase, @function
td_thr_tlsbase:
	testq	%rsi, %rsi
	movl	$23, %eax
	je	.L31
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	movq	8(%rdi), %rcx
	movq	%rdx, -160(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%rdi, -144(%rbp)
	movq	(%rdi), %r12
	testq	%rcx, %rcx
	movq	%rcx, -152(%rbp)
	je	.L35
	movq	%r12, %rdi
	call	__td_ta_rtld_global
	testb	%al, %al
	jne	.L36
.L6:
	movq	552(%r12), %r8
	testq	%r8, %r8
	je	.L37
.L10:
	leaq	-64(%rbp), %r13
	leaq	560(%r12), %rsi
	xorl	%ecx, %ecx
	movl	$52, %edx
	movq	%r12, %rdi
	movq	%r13, %r9
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
.L12:
	leaq	640(%r12), %rax
	movq	-64(%rbp), %rbx
	xorl	%r14d, %r14d
	movq	%rax, -128(%rbp)
	leaq	652(%r12), %rax
	testq	%rbx, %rbx
	movq	%rax, -136(%rbp)
	jne	.L15
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L38:
	movq	-64(%rbp), %r15
	addq	%r14, %r15
	cmpq	%r15, -120(%rbp)
	jb	.L14
	movq	-136(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %r9
	movq	%rbx, %r8
	movl	$61, %edx
	movq	%r12, %rdi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	-64(%rbp), %rbx
	movq	%r15, %r14
	testq	%rbx, %rbx
	je	.L9
.L15:
	movq	-128(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %r9
	movq	%rbx, %r8
	movl	$60, %edx
	movq	%r12, %rdi
	call	_td_fetch_value
	testl	%eax, %eax
	je	.L38
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	-64(%rbp), %r13
	movq	520(%r12), %r8
	leaq	540(%r12), %rsi
	xorl	%ecx, %ecx
	movl	$50, %edx
	movq	%r12, %rdi
	movq	%r13, %r9
	call	_td_fetch_value
	testl	%eax, %eax
	je	.L12
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	movq	16(%r12), %rdi
	call	ps_getpid@PLT
	movl	%eax, %esi
	movq	-144(%rbp), %rax
	leaq	-64(%rbp), %rdx
	movq	(%rax), %rdi
	call	__td_ta_lookup_th_unique@PLT
	testl	%eax, %eax
	jne	.L23
	movq	-56(%rbp), %rax
	testq	%rax, %rax
	movq	%rax, -152(%rbp)
	je	.L23
	movq	-144(%rbp), %rax
	movq	(%rax), %r12
	movq	%r12, %rdi
	call	__td_ta_rtld_global
	testb	%al, %al
	je	.L6
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L31:
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	-120(%rbp), %rcx
	leaq	664(%r12), %rsi
	movq	%r13, %r8
	movl	$62, %edx
	movq	%r12, %rdi
	movq	%rbx, -64(%rbp)
	subq	%r14, %rcx
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L1
	movq	-144(%rbp), %rbx
	movq	-64(%rbp), %rsi
	movq	(%rbx), %rdi
	movq	%rsi, -80(%rbp)
	movl	676(%rdi), %ecx
	testl	%ecx, %ecx
	jne	.L16
	leaq	676(%rdi), %rsi
	movl	$63, %edx
	call	_td_check_sizeof
	testl	%eax, %eax
	jne	.L1
	movq	(%rbx), %rdi
	movq	-80(%rbp), %rsi
	movl	676(%rdi), %ecx
.L16:
	leaq	30(%rcx), %rax
	movq	16(%rdi), %rdi
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L9
	movq	-144(%rbp), %r14
	leaq	-72(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$65, %edx
	movq	(%r14), %rdi
	leaq	692(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	cmpq	$0, -72(%rbp)
	jne	.L39
.L9:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	16(%r12), %rdi
	leaq	552(%r12), %rcx
	movl	$51, %edx
	xorl	%esi, %esi
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1
	movq	552(%r12), %r8
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	movl	$21, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L39:
	movq	(%r14), %rdi
	leaq	-80(%rbp), %r12
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$64, %edx
	movq	%r12, %r9
	leaq	680(%rdi), %rsi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movq	-144(%rbp), %rax
	movq	-152(%rbp), %r8
	leaq	-104(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$46, %edx
	movq	-80(%rbp), %rbx
	movq	(%rax), %rdi
	leaq	504(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	-104(%rbp), %rax
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$43, %edx
	movq	%rax, -64(%rbp)
	movq	-144(%rbp), %rax
	movq	(%rax), %rdi
	leaq	468(%rdi), %rsi
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L1
	movq	-144(%rbp), %rax
	movq	-64(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movl	$45, %edx
	movq	(%rax), %rdi
	leaq	492(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	cmpq	-80(%rbp), %rbx
	jbe	.L19
.L20:
	movq	-144(%rbp), %rax
	movq	-72(%rbp), %r8
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movl	$42, %edx
	movq	(%rax), %rdi
	leaq	456(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	-80(%rbp), %rdx
	leaq	1(%rdx), %rcx
	cmpq	$1, %rcx
	jbe	.L23
	movq	-152(%rbp), %rdi
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	movq	-160(%rbp), %rdi
	movq	%rdx, (%rdi)
	jmp	.L1
.L19:
	movq	-144(%rbp), %rax
	movq	-120(%rbp), %rcx
	leaq	-96(%rbp), %r8
	movl	$43, %edx
	movq	(%rax), %rdi
	movq	-104(%rbp), %rax
	leaq	468(%rdi), %rsi
	movq	%rax, -96(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L1
	movq	-144(%rbp), %rax
	movq	-96(%rbp), %r8
	leaq	-88(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$44, %edx
	movq	(%rax), %rdi
	leaq	480(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	-88(%rbp), %rdx
	testb	$1, %dl
	jne	.L20
	movq	-160(%rbp), %rdi
	movq	%rdx, (%rdi)
	jmp	.L1
	.size	td_thr_tlsbase, .-td_thr_tlsbase
	.hidden	td_mod_lookup
	.hidden	_td_fetch_value_local
	.hidden	_td_check_sizeof
	.hidden	_td_locate_field
	.hidden	_td_fetch_value
	.hidden	__td_ta_rtld_global
