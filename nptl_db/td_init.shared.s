	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_init\n"
	.text
	.p2align 4,,15
	.globl	td_init
	.type	td_init, @function
td_init:
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	movl	$8, %edx
	movl	$2, %edi
	call	write@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	td_init, .-td_init
	.section	.rodata.str1.1
.LC1:
	.string	"ld-linux-x86-64.so.2"
	.text
	.p2align 4,,15
	.globl	__td_ta_rtld_global
	.hidden	__td_ta_rtld_global
	.type	__td_ta_rtld_global, @function
__td_ta_rtld_global:
	movq	520(%rdi), %rax
	testq	%rax, %rax
	je	.L19
	cmpq	$-1, %rax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	pushq	%rbx
	leaq	520(%rdi), %rcx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	leaq	.LC1(%rip), %rsi
	movl	$48, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L20
	movq	520(%rbx), %rax
	popq	%rbx
	cmpq	$-1, %rax
	setne	%al
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	movq	$-1, 520(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__td_ta_rtld_global, .-__td_ta_rtld_global
	.hidden	__td_debug
	.comm	__td_debug,4,4
	.hidden	td_mod_lookup
