	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_event_getmsg\n"
.LC1:
	.string	"libpthread.so.0"
.LC2:
	.string	"td_ta_event_getmsg.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"(ta)->ta_sizeof_td_eventbuf_t != 0"
	.text
	.p2align 4,,15
	.globl	td_ta_event_getmsg
	.type	td_ta_event_getmsg, @function
td_ta_event_getmsg:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	__td_debug(%rip), %edx
	testl	%edx, %edx
	jne	.L27
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L28:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L16
.L6:
	cmpq	%rax, %rbx
	jne	.L28
	movq	304(%rbx), %r8
	testq	%r8, %r8
	je	.L29
.L14:
	leaq	312(%rbx), %r13
	leaq	-64(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$28, %edx
	movq	%rbx, %rdi
	movq	%r13, %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	-64(%rbp), %rax
	testq	%rax, %rax
	je	.L17
	leaq	124(%rbx), %rsi
	leaq	-88(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$9, %edx
	movq	%rbx, %rdi
	movq	%rax, -88(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L1
	movl	216(%rbx), %eax
	testl	%eax, %eax
	jne	.L8
	leaq	216(%rbx), %rsi
	movl	$18, %edx
	movq	%rbx, %rdi
	call	_td_check_sizeof
	testl	%eax, %eax
	jne	.L1
	movl	216(%rbx), %eax
.L8:
	movl	%eax, %ecx
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %rsi
	leaq	30(%rcx), %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r14
	andq	$-16, %r14
	movq	%r14, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L9
	leaq	220(%rbx), %rsi
	leaq	-80(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movl	$19, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	je	.L18
	leaq	232(%rbx), %rsi
	leaq	-72(%rbp), %r9
	xorl	%ecx, %ecx
	movq	%r14, %r8
	movl	$20, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movq	-64(%rbp), %rax
	movq	%rbx, th.9032(%rip)
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rax, 8+th.9032(%rip)
	movq	-72(%rbp), %rax
	movq	%rax, 16(%r12)
	movq	-80(%rbp), %rax
	movl	%eax, (%r12)
	leaq	th.9032(%rip), %rax
	movq	%rax, 8(%r12)
	movl	216(%rbx), %r12d
	movq	%r12, %rdx
	call	memset@PLT
	testl	%r12d, %r12d
	je	.L30
	movq	16(%rbx), %rdi
	movq	-88(%rbp), %rsi
	movq	%r14, %rdx
	movq	%r12, %rcx
	call	ps_pdwrite@PLT
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1
	leaq	160(%rbx), %r12
	movq	-64(%rbp), %r8
	leaq	-56(%rbp), %r9
	xorl	%ecx, %ecx
	movl	$12, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	cmpq	$0, 304(%rbx)
	je	.L11
.L13:
	movq	-56(%rbp), %r9
	movq	304(%rbx), %r8
	xorl	%ecx, %ecx
	movl	$28, %edx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_td_store_value
	testl	%eax, %eax
	jne	.L1
	cmpq	$0, -56(%rbp)
	je	.L1
	movq	-64(%rbp), %r8
	xorl	%r9d, %r9d
	xorl	%ecx, %ecx
	movl	$12, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	_td_store_value
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	-40(%rbp), %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	16(%rbx), %rdi
	leaq	304(%rbx), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$27, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L9
	movq	304(%rbx), %r8
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	.LC0(%rip), %rsi
	movl	$20, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
.L11:
	movq	16(%rbx), %rdi
	leaq	304(%rbx), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$27, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	je	.L13
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$10, %eax
	jmp	.L1
.L18:
	movl	$15, %eax
	jmp	.L1
.L30:
	leaq	__PRETTY_FUNCTION__.9035(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$85, %edx
	call	__assert_fail@PLT
	.size	td_ta_event_getmsg, .-td_ta_event_getmsg
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9035, @object
	.size	__PRETTY_FUNCTION__.9035, 19
__PRETTY_FUNCTION__.9035:
	.string	"td_ta_event_getmsg"
	.local	th.9032
	.comm	th.9032,16,16
	.hidden	td_mod_lookup
	.hidden	_td_store_value
	.hidden	_td_fetch_value_local
	.hidden	_td_check_sizeof
	.hidden	_td_locate_field
	.hidden	_td_fetch_value
	.hidden	__td_agent_list
	.hidden	__td_debug
