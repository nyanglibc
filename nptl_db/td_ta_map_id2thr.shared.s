	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_map_id2thr\n"
	.text
	.p2align 4,,15
	.globl	td_ta_map_id2thr
	.type	td_ta_map_id2thr, @function
td_ta_map_id2thr:
	movl	__td_debug(%rip), %eax
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdx, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L10
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L7
.L6:
	cmpq	%rax, %rbx
	jne	.L11
	movq	%rbx, 0(%rbp)
	movq	%r12, 8(%rbp)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	popq	%rbx
	movl	$8, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC0(%rip), %rsi
	movl	$17, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.size	td_ta_map_id2thr, .-td_ta_map_id2thr
	.hidden	__td_agent_list
	.hidden	__td_debug
