	.text
	.p2align 4,,15
	.globl	td_thr_tls_get_addr
	.type	td_thr_tls_get_addr, @function
td_thr_tls_get_addr:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r8
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rdx, %r12
	movq	%rcx, %rbp
	movl	$41, %edx
	xorl	%ecx, %ecx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	leaq	8(%rsp), %r9
	leaq	444(%rdi), %rsi
	call	_td_fetch_value
	cmpl	$14, %eax
	je	.L3
	testl	%eax, %eax
	je	.L6
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	8(%rsp), %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	call	td_thr_tlsbase@PLT
	testl	%eax, %eax
	jne	.L1
	addq	%r12, 0(%rbp)
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$16, %rsp
	movl	$16, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	td_thr_tls_get_addr, .-td_thr_tls_get_addr
	.hidden	_td_fetch_value
