	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_log\n"
	.text
	.p2align 4,,15
	.globl	td_log
	.type	td_log, @function
td_log:
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	movl	$7, %edx
	movl	$2, %edi
	call	write@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	td_log, .-td_log
	.hidden	__td_debug
