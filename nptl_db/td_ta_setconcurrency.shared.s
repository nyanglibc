	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_setconcurrency\n"
	.text
	.p2align 4,,15
	.globl	td_ta_setconcurrency
	.type	td_ta_setconcurrency, @function
td_ta_setconcurrency:
	movl	__td_debug(%rip), %eax
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L12
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L5
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L6
.L5:
	cmpq	%rax, %rbx
	jne	.L13
	movl	$14, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$8, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	.LC0(%rip), %rsi
	movl	$21, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.size	td_ta_setconcurrency, .-td_ta_setconcurrency
	.hidden	__td_agent_list
	.hidden	__td_debug
