	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_get_ph\n"
	.text
	.p2align 4,,15
	.globl	td_ta_get_ph
	.type	td_ta_get_ph, @function
td_ta_get_ph:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L10
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L7
.L6:
	cmpq	%rax, %rbx
	jne	.L11
	movq	16(%rbx), %rax
	movq	%rax, 0(%rbp)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$8, %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC0(%rip), %rsi
	movl	$13, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.size	td_ta_get_ph, .-td_ta_get_ph
	.hidden	__td_agent_list
	.hidden	__td_debug
