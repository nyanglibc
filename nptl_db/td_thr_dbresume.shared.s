	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_dbresume\n"
	.text
	.p2align 4,,15
	.globl	td_thr_dbresume
	.type	td_thr_dbresume, @function
td_thr_dbresume:
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	movl	$14, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	movl	$16, %edx
	movl	$2, %edi
	call	write@PLT
	movl	$14, %eax
	addq	$8, %rsp
	ret
	.size	td_thr_dbresume, .-td_thr_dbresume
	.hidden	__td_debug
