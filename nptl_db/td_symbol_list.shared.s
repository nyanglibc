	.text
	.p2align 4,,15
	.globl	td_symbol_list
	.type	td_symbol_list, @function
td_symbol_list:
	leaq	symbol_list_arr(%rip), %rax
	ret
	.size	td_symbol_list, .-td_symbol_list
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_symbol_list.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"idx >= 0 && idx < SYM_NUM_MESSAGES"
	.text
	.p2align 4,,15
	.globl	td_mod_lookup
	.hidden	td_mod_lookup
	.type	td_mod_lookup, @function
td_mod_lookup:
	cmpl	$70, %edx
	ja	.L8
	leaq	symbol_list_arr(%rip), %rax
	movslq	%edx, %rdx
	movq	(%rax,%rdx,8), %rdx
	jmp	ps_pglobal_lookup@PLT
.L8:
	leaq	__PRETTY_FUNCTION__.9061(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$47, %edx
	call	__assert_fail@PLT
	.size	td_mod_lookup, .-td_mod_lookup
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9061, @object
	.size	__PRETTY_FUNCTION__.9061, 14
__PRETTY_FUNCTION__.9061:
	.string	"td_mod_lookup"
	.section	.rodata.str1.1
.LC2:
	.string	"_thread_db_sizeof_pthread"
.LC3:
	.string	"_thread_db_pthread_list"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"_thread_db_pthread_report_events"
	.section	.rodata.str1.1
.LC5:
	.string	"_thread_db_pthread_tid"
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"_thread_db_pthread_start_routine"
	.align 8
.LC7:
	.string	"_thread_db_pthread_cancelhandling"
	.align 8
.LC8:
	.string	"_thread_db_pthread_schedpolicy"
	.align 8
.LC9:
	.string	"_thread_db_pthread_schedparam_sched_priority"
	.section	.rodata.str1.1
.LC10:
	.string	"_thread_db_pthread_specific"
.LC11:
	.string	"_thread_db_pthread_eventbuf"
	.section	.rodata.str1.8
	.align 8
.LC12:
	.string	"_thread_db_pthread_eventbuf_eventmask"
	.align 8
.LC13:
	.string	"_thread_db_pthread_eventbuf_eventmask_event_bits"
	.section	.rodata.str1.1
.LC14:
	.string	"_thread_db_pthread_nextevent"
.LC15:
	.string	"_thread_db_sizeof_list_t"
.LC16:
	.string	"_thread_db_list_t_next"
.LC17:
	.string	"_thread_db_list_t_prev"
	.section	.rodata.str1.8
	.align 8
.LC18:
	.string	"_thread_db_sizeof_td_thr_events_t"
	.align 8
.LC19:
	.string	"_thread_db_td_thr_events_t_event_bits"
	.align 8
.LC20:
	.string	"_thread_db_sizeof_td_eventbuf_t"
	.align 8
.LC21:
	.string	"_thread_db_td_eventbuf_t_eventnum"
	.align 8
.LC22:
	.string	"_thread_db_td_eventbuf_t_eventdata"
	.section	.rodata.str1.1
.LC23:
	.string	"nptl_version"
.LC24:
	.string	"__nptl_create_event"
.LC25:
	.string	"__nptl_death_event"
.LC26:
	.string	"__nptl_threads_events"
.LC27:
	.string	"__nptl_nthreads"
.LC28:
	.string	"_thread_db___nptl_nthreads"
.LC29:
	.string	"__nptl_last_event"
.LC30:
	.string	"_thread_db___nptl_last_event"
.LC31:
	.string	"__nptl_initial_report_events"
	.section	.rodata.str1.8
	.align 8
.LC32:
	.string	"_thread_db___nptl_initial_report_events"
	.section	.rodata.str1.1
.LC33:
	.string	"__pthread_keys"
.LC34:
	.string	"_thread_db___pthread_keys"
	.section	.rodata.str1.8
	.align 8
.LC35:
	.string	"_thread_db_sizeof_pthread_key_struct"
	.align 8
.LC36:
	.string	"_thread_db_pthread_key_struct_seq"
	.align 8
.LC37:
	.string	"_thread_db_pthread_key_struct_destr"
	.align 8
.LC38:
	.string	"_thread_db_sizeof_pthread_key_data"
	.align 8
.LC39:
	.string	"_thread_db_pthread_key_data_seq"
	.align 8
.LC40:
	.string	"_thread_db_pthread_key_data_data"
	.align 8
.LC41:
	.string	"_thread_db_sizeof_pthread_key_data_level2"
	.align 8
.LC42:
	.string	"_thread_db_pthread_key_data_level2_data"
	.align 8
.LC43:
	.string	"_thread_db_link_map_l_tls_modid"
	.align 8
.LC44:
	.string	"_thread_db_link_map_l_tls_offset"
	.section	.rodata.str1.1
.LC45:
	.string	"_thread_db_dtv_dtv"
.LC46:
	.string	"_thread_db_dtv_t_pointer_val"
.LC47:
	.string	"_thread_db_dtv_t_counter"
.LC48:
	.string	"_thread_db_pthread_dtvp"
.LC49:
	.string	"_thread_db_sizeof_rtld_global"
.LC50:
	.string	"_rtld_global"
.LC51:
	.string	"_thread_db__rtld_global"
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"_thread_db_rtld_global__dl_tls_dtv_slotinfo_list"
	.section	.rodata.str1.1
.LC53:
	.string	"_dl_tls_dtv_slotinfo_list"
	.section	.rodata.str1.8
	.align 8
.LC54:
	.string	"_thread_db__dl_tls_dtv_slotinfo_list"
	.align 8
.LC55:
	.string	"_thread_db_rtld_global__dl_stack_user"
	.section	.rodata.str1.1
.LC56:
	.string	"_dl_stack_user"
.LC57:
	.string	"_thread_db__dl_stack_user"
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"_thread_db_rtld_global__dl_stack_used"
	.section	.rodata.str1.1
.LC59:
	.string	"_dl_stack_used"
.LC60:
	.string	"_thread_db__dl_stack_used"
	.section	.rodata.str1.8
	.align 8
.LC61:
	.string	"_thread_db_sizeof_dtv_slotinfo_list"
	.align 8
.LC62:
	.string	"_thread_db_dtv_slotinfo_list_len"
	.align 8
.LC63:
	.string	"_thread_db_dtv_slotinfo_list_next"
	.align 8
.LC64:
	.string	"_thread_db_dtv_slotinfo_list_slotinfo"
	.align 8
.LC65:
	.string	"_thread_db_sizeof_dtv_slotinfo"
	.section	.rodata.str1.1
.LC66:
	.string	"_thread_db_dtv_slotinfo_gen"
.LC67:
	.string	"_thread_db_dtv_slotinfo_map"
.LC68:
	.string	"_thread_db_const_thread_area"
.LC69:
	.string	"_thread_db_register64"
.LC70:
	.string	"_thread_db_register32"
	.section	.rodata.str1.8
	.align 8
.LC71:
	.string	"_thread_db_register64_thread_area"
	.align 8
.LC72:
	.string	"_thread_db_register32_thread_area"
	.section	.data.rel.local,"aw",@progbits
	.align 32
	.type	symbol_list_arr, @object
	.size	symbol_list_arr, 576
symbol_list_arr:
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	.LC15
	.quad	.LC16
	.quad	.LC17
	.quad	.LC18
	.quad	.LC19
	.quad	.LC20
	.quad	.LC21
	.quad	.LC22
	.quad	.LC23
	.quad	.LC24
	.quad	.LC25
	.quad	.LC26
	.quad	.LC27
	.quad	.LC28
	.quad	.LC29
	.quad	.LC30
	.quad	.LC31
	.quad	.LC32
	.quad	.LC33
	.quad	.LC34
	.quad	.LC35
	.quad	.LC36
	.quad	.LC37
	.quad	.LC38
	.quad	.LC39
	.quad	.LC40
	.quad	.LC41
	.quad	.LC42
	.quad	.LC43
	.quad	.LC44
	.quad	.LC45
	.quad	.LC46
	.quad	.LC47
	.quad	.LC48
	.quad	.LC49
	.quad	.LC50
	.quad	.LC51
	.quad	.LC52
	.quad	.LC53
	.quad	.LC54
	.quad	.LC55
	.quad	.LC56
	.quad	.LC57
	.quad	.LC58
	.quad	.LC59
	.quad	.LC60
	.quad	.LC61
	.quad	.LC62
	.quad	.LC63
	.quad	.LC64
	.quad	.LC65
	.quad	.LC66
	.quad	.LC67
	.quad	.LC68
	.quad	.LC69
	.quad	.LC70
	.quad	.LC71
	.quad	.LC72
	.quad	0
