	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_setxregs\n"
	.text
	.p2align 4,,15
	.globl	td_thr_setxregs
	.type	td_thr_setxregs, @function
td_thr_setxregs:
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L8
	movl	$20, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	subq	$8, %rsp
	movl	$16, %edx
	movl	$2, %edi
	call	write@PLT
	movl	$20, %eax
	addq	$8, %rsp
	ret
	.size	td_thr_setxregs, .-td_thr_setxregs
	.hidden	__td_debug
