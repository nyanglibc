	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_event_addr\n"
.LC1:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	td_ta_event_addr
	.type	td_ta_event_addr, @function
td_ta_event_addr:
	movl	__td_debug(%rip), %eax
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L19
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L14
.L6:
	cmpq	%rax, %rbx
	jne	.L20
	cmpl	$8, %ebp
	je	.L7
	cmpl	$9, %ebp
	jne	.L21
	movq	264(%rbx), %rax
	testq	%rax, %rax
	je	.L22
.L10:
	movq	%rax, 8(%r12)
	movl	$0, (%r12)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$8, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	256(%rbx), %rax
	testq	%rax, %rax
	jne	.L10
	movq	16(%rbx), %rdi
	leaq	256(%rbx), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$22, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L13
	movq	256(%rbx), %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L19:
	leaq	.LC0(%rip), %rsi
	movl	$17, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L21:
	popq	%rbx
	movl	$13, %eax
	popq	%rbp
	popq	%r12
	ret
.L22:
	movq	16(%rbx), %rdi
	leaq	264(%rbx), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$23, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L13
	movq	264(%rbx), %rax
	jmp	.L10
.L13:
	movl	$1, %eax
	jmp	.L1
	.size	td_ta_event_addr, .-td_ta_event_addr
	.hidden	td_mod_lookup
	.hidden	__td_agent_list
	.hidden	__td_debug
