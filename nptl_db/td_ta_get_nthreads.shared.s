	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_get_nthreads\n"
.LC1:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	td_ta_get_nthreads
	.type	td_ta_get_nthreads, @function
td_ta_get_nthreads:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L14
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L9
.L6:
	cmpq	%rax, %rbx
	jne	.L15
	movq	280(%rbx), %r8
	testq	%r8, %r8
	je	.L16
.L7:
	leaq	288(%rbx), %rsi
	leaq	8(%rsp), %r9
	xorl	%ecx, %ecx
	movl	$26, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L1
	movq	8(%rsp), %rdx
	movl	%edx, 0(%rbp)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$24, %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	16(%rbx), %rdi
	leaq	280(%rbx), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$25, %edx
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L1
	movq	280(%rbx), %r8
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L14:
	leaq	.LC0(%rip), %rsi
	movl	$19, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.size	td_ta_get_nthreads, .-td_ta_get_nthreads
	.hidden	td_mod_lookup
	.hidden	_td_fetch_value
	.hidden	__td_agent_list
	.hidden	__td_debug
