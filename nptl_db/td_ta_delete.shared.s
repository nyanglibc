	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_delete\n"
	.text
	.p2align 4,,15
	.globl	td_ta_delete
	.type	td_ta_delete, @function
td_ta_delete:
	movl	__td_debug(%rip), %eax
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	jne	.L10
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L11:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L7
.L6:
	cmpq	%rax, %rbx
	jne	.L11
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	%rbx, %rdi
	movq	%rdx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	%rax, (%rdx)
	call	free@PLT
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$8, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	leaq	.LC0(%rip), %rsi
	movl	$13, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.size	td_ta_delete, .-td_ta_delete
	.hidden	__td_agent_list
	.hidden	__td_debug
