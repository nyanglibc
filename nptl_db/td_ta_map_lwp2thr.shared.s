	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	__td_ta_lookup_th_unique
	.type	__td_ta_lookup_th_unique, @function
__td_ta_lookup_th_unique:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$248, %rsp
	movl	704(%rdi), %eax
	testl	%eax, %eax
	je	.L36
.L2:
	cmpl	$2, %eax
	je	.L15
	cmpl	$3, %eax
	je	.L6
	cmpl	$1, %eax
	je	.L37
.L32:
	movl	$15, %eax
.L1:
	addq	$248, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	16(%rsp), %r13
	movq	16(%rdi), %rdi
	leaq	.LC0(%rip), %rsi
	movl	$66, %edx
	movq	%r13, %rcx
	call	td_mod_lookup
	testl	%eax, %eax
	je	.L38
	movq	16(%rbx), %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rcx
	movl	$67, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L7
	movl	$1, 704(%rbx)
.L8:
	movq	16(%rbx), %rdi
	movq	16(%rsp), %rsi
	leaq	708(%rbx), %rdx
	movl	$12, %ecx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L19
	movl	708(%rbx), %eax
	testl	%eax, %eax
	je	.L32
	testl	$-16777216, %eax
	je	.L34
	movl	716(%rbx), %eax
	bswap	%eax
	movl	%eax, 716(%rbx)
	movl	712(%rbx), %eax
	bswap	%eax
	movl	%eax, 712(%rbx)
.L34:
	movl	704(%rbx), %eax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	$0, ps_get_thread_area@GOTPCREL(%rip)
	movl	$14, %eax
	je	.L1
	leaq	16(%rsp), %r13
	movq	16(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r13, %rdx
	call	ps_lgetregs@PLT
	testl	%eax, %eax
	je	.L39
.L19:
	addq	$248, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	16(%rbx), %rdi
	movq	16(%rsp), %rsi
	leaq	708(%rbx), %rdx
	movl	$4, %ecx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L19
	movl	708(%rbx), %eax
	movl	$3, 704(%rbx)
	testl	$-16777216, %eax
	jne	.L40
.L6:
	cmpq	$0, ps_get_thread_area@GOTPCREL(%rip)
	movl	$14, %eax
	je	.L1
	movl	708(%rbx), %edx
	movq	16(%rbx), %rdi
	leaq	8(%r12), %rcx
	movl	%ebp, %esi
	call	ps_get_thread_area@PLT
	testl	%eax, %eax
	jne	.L19
.L18:
	movq	%rbx, (%r12)
	addq	$248, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	leaq	16(%rsp), %r13
	movq	16(%rbx), %rdi
	movl	%ebp, %esi
	movq	%r13, %rdx
	call	ps_lgetregs@PLT
	testl	%eax, %eax
	jne	.L19
	leaq	708(%rbx), %rsi
	leaq	8(%rsp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$-1, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movslq	712(%rbx), %rax
	addq	8(%rsp), %rax
	movq	%rax, 8(%r12)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$5, %eax
	jne	.L32
	movq	16(%rbx), %rdi
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rcx
	movl	$69, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L32
	movl	$2, 704(%rbx)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L39:
	leaq	708(%rbx), %rsi
	leaq	8(%rsp), %r9
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$-1, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L1
	movl	712(%rbx), %ecx
	movq	8(%rsp), %rdx
	leaq	8(%r12), %rax
	movq	16(%rbx), %rdi
	movl	%ebp, %esi
	sarq	%cl, %rdx
	movq	%rax, %rcx
	call	ps_get_thread_area@PLT
	testl	%eax, %eax
	je	.L18
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L40:
	bswap	%eax
	movl	%eax, 708(%rbx)
	jmp	.L6
	.size	__td_ta_lookup_th_unique, .-__td_ta_lookup_th_unique
	.section	.rodata.str1.1
.LC1:
	.string	"td_ta_map_lwp2thr\n"
	.text
	.p2align 4,,15
	.globl	td_ta_map_lwp2thr
	.type	td_ta_map_lwp2thr, @function
td_ta_map_lwp2thr:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L53
.L42:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L46
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L49
.L46:
	cmpq	%rax, %rbx
	jne	.L54
	leaq	8(%rsp), %r14
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	__td_ta_stack_user
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L51
.L41:
	addq	$16, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$16, %rsp
	movl	$8, %ebp
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L53:
	leaq	.LC1(%rip), %rsi
	movl	$18, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L51:
	movq	8(%rsp), %r8
	leaq	176(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %r9
	movl	$14, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L41
	cmpq	$0, 8(%rsp)
	je	.L55
	movq	%r13, %rdx
	movl	%r12d, %esi
	movq	%rbx, %rdi
	call	__td_ta_lookup_th_unique@PLT
	movl	%eax, %ebp
	jmp	.L41
.L55:
	movq	16(%rbx), %rdi
	call	ps_getpid@PLT
	cmpl	%r12d, %eax
	je	.L56
	movl	$1, %ebp
	jmp	.L41
.L56:
	movq	%rbx, 0(%r13)
	movq	$0, 8(%r13)
	jmp	.L41
	.size	td_ta_map_lwp2thr, .-td_ta_map_lwp2thr
	.weak	ps_get_thread_area
	.hidden	_td_fetch_value
	.hidden	__td_ta_stack_user
	.hidden	__td_agent_list
	.hidden	__td_debug
	.hidden	_td_fetch_value_local
	.hidden	td_mod_lookup
