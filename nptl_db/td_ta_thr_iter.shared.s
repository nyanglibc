	.text
	.p2align 4,,15
	.type	iterate_thread_list, @function
iterate_thread_list:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	testl	%ecx, %ecx
	movl	16(%rbp), %r13d
	je	.L31
.L2:
	xorl	%edx, %edx
.L1:
	leaq	-40(%rbp), %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	176(%rdi), %rax
	leaq	-96(%rbp), %r15
	movq	%r9, %r12
	movl	%r8d, -108(%rbp)
	movq	%rdx, -104(%rbp)
	xorl	%ecx, %ecx
	movq	%rsi, -128(%rbp)
	movl	$14, %edx
	movq	%rsi, %r14
	movq	%r15, %r9
	movq	%r12, %r8
	movq	%rax, %rsi
	movq	%rdi, %rbx
	movq	%rax, -120(%rbp)
	call	_td_fetch_value
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1
	cmpq	$0, -96(%rbp)
	movl	%eax, -136(%rbp)
	jne	.L4
	testb	%r13b, %r13b
	je	.L4
	movq	%rbx, -64(%rbp)
	movq	$0, -56(%rbp)
	leaq	-64(%rbp), %rdi
	movq	-104(%rbp), %rsi
	call	*%r14
	movl	-136(%rbp), %edx
	testl	%eax, %eax
	movl	$15, %eax
	cmovne	%eax, %edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	28(%rbx), %rsi
	leaq	-88(%rbp), %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	$0, -88(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1
	movl	24(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L32
.L6:
	leaq	30(%rcx), %rax
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	movq	-96(%rbp), %rax
	leaq	15(%rsp), %r13
	andq	$-16, %r13
	cmpq	%rax, %r12
	je	.L2
	movq	%rax, %r14
	subq	-88(%rbp), %r14
	testq	%rax, %rax
	je	.L8
	testq	%r14, %r14
	je	.L8
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	leaq	88(%rbx), %rax
	movq	%rax, -144(%rbp)
	leaq	-72(%rbp), %rax
	movq	%rax, -152(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rax, -160(%rbp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L33:
	movq	-136(%rbp), %r9
	movq	-144(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$6, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L28
	movq	-152(%rbp), %r9
	leaq	100(%rbx), %rsi
	xorl	%ecx, %ecx
	movq	%r13, %r8
	movl	$7, %edx
	movq	%rbx, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L28
	cmpq	$0, -80(%rbp)
	je	.L11
	movl	-72(%rbp), %eax
.L11:
	cmpl	-108(%rbp), %eax
	jl	.L12
	movq	%rbx, -64(%rbp)
	movq	%r14, -56(%rbp)
	movq	-104(%rbp), %rsi
	movq	-160(%rbp), %rdi
	movq	-128(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	jne	.L8
.L12:
	movq	-88(%rbp), %r8
	movq	-120(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r15, %r9
	movl	$14, %edx
	movq	%rbx, %rdi
	addq	%r13, %r8
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L28
	movq	-96(%rbp), %rax
	cmpq	%r12, %rax
	je	.L2
	movq	%rax, %r14
	subq	-88(%rbp), %r14
	je	.L8
	testq	%rax, %rax
	je	.L8
	movl	24(%rbx), %ecx
.L15:
	movq	16(%rbx), %rdi
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	ps_pdread@PLT
	testl	%eax, %eax
	je	.L33
	movl	$1, %edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L32:
	leaq	24(%rbx), %rsi
	movq	%rbx, %rdi
	call	_td_check_sizeof
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L1
	movl	24(%rbx), %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$15, %edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L28:
	movl	%eax, %edx
	jmp	.L1
	.size	iterate_thread_list, .-iterate_thread_list
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_thr_iter\n"
	.text
	.p2align 4,,15
	.globl	td_ta_thr_iter
	.type	td_ta_thr_iter, @function
td_ta_thr_iter:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r14d
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rdx, %r12
	subq	$24, %rsp
	movl	__td_debug(%rip), %r8d
	movq	$0, 8(%rsp)
	testl	%r8d, %r8d
	jne	.L43
.L35:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L39
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L44:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L41
.L39:
	cmpq	%rax, %rbx
	jne	.L44
	leaq	8(%rsp), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	__td_ta_stack_user
	testl	%eax, %eax
	je	.L40
.L34:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$24, %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	.LC0(%rip), %rsi
	movl	$15, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L40:
	subq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	pushq	$1
	movq	24(%rsp), %r9
	movl	%r14d, %r8d
	movl	%r13d, %ecx
	movq	%r12, %rdx
	call	iterate_thread_list
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	jne	.L34
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	__td_ta_stack_used
	testl	%eax, %eax
	jne	.L34
	subq	$8, %rsp
	movl	%r13d, %ecx
	movq	%r12, %rdx
	pushq	$0
	movq	24(%rsp), %r9
	movl	%r14d, %r8d
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	iterate_thread_list
	popq	%rdx
	popq	%rcx
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	td_ta_thr_iter, .-td_ta_thr_iter
	.hidden	__td_ta_stack_used
	.hidden	__td_ta_stack_user
	.hidden	__td_agent_list
	.hidden	__td_debug
	.hidden	_td_check_sizeof
	.hidden	_td_fetch_value_local
	.hidden	_td_locate_field
	.hidden	_td_fetch_value
