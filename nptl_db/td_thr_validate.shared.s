	.text
	.p2align 4,,15
	.type	check_thread_list, @function
check_thread_list:
	pushq	%r14
	pushq	%r13
	xorl	%ecx, %ecx
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rdx, %r14
	movq	%rbp, %r8
	movl	$14, %edx
	subq	$16, %rsp
	movq	(%rdi), %rdi
	movq	%rsp, %r12
	movq	%r12, %r9
	leaq	176(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L1
	cmpq	$0, (%rsp)
	je	.L13
	movq	(%rbx), %rdi
	leaq	8(%rsp), %r8
	xorl	%ecx, %ecx
	movl	$1, %edx
	movq	$0, 8(%rsp)
	leaq	28(%rdi), %rsi
	call	_td_locate_field
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L1
	movq	(%rsp), %r8
	cmpq	%r8, %rbp
	jne	.L11
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rbx), %rdi
	xorl	%ecx, %ecx
	movq	%r12, %r9
	movl	$14, %edx
	leaq	176(%rdi), %rsi
	call	_td_fetch_value
	testl	%eax, %eax
	jne	.L14
	movq	(%rsp), %r8
	cmpq	%rbp, %r8
	je	.L6
.L11:
	movq	%r8, %rax
	subq	8(%rsp), %rax
	cmpq	%rax, 8(%rbx)
	jne	.L15
.L1:
	addq	$16, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$16, %rsp
	movl	$2, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movb	$1, (%r14)
	movl	$2, %r13d
	addq	$16, %rsp
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L14:
	movl	%eax, %r13d
	jmp	.L1
	.size	check_thread_list, .-check_thread_list
	.p2align 4,,15
	.globl	__td_ta_stack_user
	.hidden	__td_ta_stack_user
	.type	__td_ta_stack_user, @function
__td_ta_stack_user:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__td_ta_rtld_global
	testb	%al, %al
	jne	.L22
	movq	584(%rbx), %rax
	testq	%rax, %rax
	je	.L23
.L18:
	movq	%rax, 0(%rbp)
	xorl	%eax, %eax
.L16:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	520(%rbx), %rax
	leaq	572(%rbx), %rsi
	movq	%rbp, %r8
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movl	$53, %edx
	movq	%rax, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_td_locate_field
	.p2align 4,,10
	.p2align 3
.L23:
	movq	16(%rbx), %rdi
	leaq	584(%rbx), %rcx
	movl	$54, %edx
	xorl	%esi, %esi
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L16
	movq	584(%rbx), %rax
	jmp	.L18
	.size	__td_ta_stack_user, .-__td_ta_stack_user
	.p2align 4,,15
	.globl	__td_ta_stack_used
	.hidden	__td_ta_stack_used
	.type	__td_ta_stack_used, @function
__td_ta_stack_used:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__td_ta_rtld_global
	testb	%al, %al
	jne	.L30
	movq	616(%rbx), %rax
	testq	%rax, %rax
	je	.L31
.L26:
	movq	%rax, 0(%rbp)
	xorl	%eax, %eax
.L24:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	520(%rbx), %rax
	leaq	604(%rbx), %rsi
	movq	%rbp, %r8
	movq	%rbx, %rdi
	xorl	%ecx, %ecx
	movl	$56, %edx
	movq	%rax, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	_td_locate_field
	.p2align 4,,10
	.p2align 3
.L31:
	movq	16(%rbx), %rdi
	leaq	616(%rbx), %rcx
	movl	$57, %edx
	xorl	%esi, %esi
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	jne	.L24
	movq	616(%rbx), %rax
	jmp	.L26
	.size	__td_ta_stack_used, .-__td_ta_stack_used
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_thr_validate\n"
	.text
	.p2align 4,,15
	.globl	td_thr_validate
	.type	td_thr_validate, @function
td_thr_validate:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L38
.L33:
	leaq	8(%rsp), %rbp
	movq	(%rbx), %rdi
	movb	$0, 7(%rsp)
	movq	%rbp, %rsi
	call	__td_ta_stack_user
	testl	%eax, %eax
	je	.L39
	cmpl	$2, %eax
	je	.L40
.L32:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	.LC0(%rip), %rsi
	movl	$16, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L39:
	movq	8(%rsp), %rsi
	leaq	7(%rsp), %rdx
	movq	%rbx, %rdi
	call	check_thread_list
	cmpl	$2, %eax
	jne	.L32
.L40:
	movq	(%rbx), %rdi
	movq	%rbp, %rsi
	call	__td_ta_stack_used
	testl	%eax, %eax
	je	.L41
.L36:
	cmpl	$2, %eax
	jne	.L32
	cmpb	$0, 7(%rsp)
	je	.L32
	cmpq	$0, 8(%rbx)
	movl	$0, %edx
	cmove	%edx, %eax
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L41:
	movq	8(%rsp), %rsi
	leaq	7(%rsp), %rdx
	movq	%rbx, %rdi
	call	check_thread_list
	jmp	.L36
	.size	td_thr_validate, .-td_thr_validate
	.hidden	__td_debug
	.hidden	td_mod_lookup
	.hidden	__td_ta_rtld_global
	.hidden	_td_locate_field
	.hidden	_td_fetch_value
