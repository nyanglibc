	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	_td_check_sizeof
	.hidden	_td_check_sizeof
	.type	_td_check_sizeof, @function
_td_check_sizeof:
	movl	(%rsi), %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	je	.L18
	rep ret
	.p2align 4,,10
	.p2align 3
.L18:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	leaq	.LC0(%rip), %rsi
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	leaq	8(%rsp), %rcx
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$14, %eax
	cmpl	$5, %edx
	je	.L1
	testl	%edx, %edx
	je	.L4
.L5:
	movl	$1, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	movl	$4, %ecx
	movq	%rbx, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L5
	movl	(%rbx), %eax
	testl	$-16777216, %eax
	je	.L6
	bswap	%eax
	movl	%eax, (%rbx)
.L6:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_td_check_sizeof, .-_td_check_sizeof
	.p2align 4,,15
	.globl	_td_locate_field
	.hidden	_td_locate_field
	.type	_td_locate_field, @function
_td_locate_field:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rcx, %rbx
	subq	$32, %rsp
	movl	(%rsi), %eax
	testl	%eax, %eax
	je	.L45
.L20:
	testq	%rbx, %rbx
	je	.L26
	movl	4(%rbp), %eax
	testl	%eax, %eax
	jne	.L46
.L26:
	movl	0(%rbp), %ecx
	movl	%ecx, %eax
	testl	$-16777216, %ecx
	bswap	%eax
	cmovne	%eax, %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	imulq	%rbx, %rcx
	movslq	8(%rbp), %rbx
	addq	%rbx, %rcx
	addq	%rcx, (%r8)
.L19:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movl	%eax, %edx
	movl	$16, %eax
	cmpq	%rbx, %rdx
	jge	.L26
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%rdi, %r12
	movq	16(%rdi), %rdi
	leaq	24(%rsp), %rcx
	leaq	.LC0(%rip), %rsi
	movq	%r8, 8(%rsp)
	call	td_mod_lookup
	movl	%eax, %edx
	movl	$14, %eax
	cmpl	$5, %edx
	je	.L19
	testl	%edx, %edx
	movq	8(%rsp), %r8
	je	.L22
.L23:
	addq	$32, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	16(%r12), %rdi
	movq	24(%rsp), %rsi
	movl	$12, %ecx
	movq	%rbp, %rdx
	movq	%r8, 8(%rsp)
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L23
	movl	0(%rbp), %edx
	movl	$15, %eax
	testl	%edx, %edx
	je	.L19
	andl	$-16777216, %edx
	movq	8(%rsp), %r8
	je	.L20
	movl	8(%rbp), %eax
	bswap	%eax
	movl	%eax, 8(%rbp)
	movl	4(%rbp), %eax
	bswap	%eax
	movl	%eax, 4(%rbp)
	jmp	.L20
	.size	_td_locate_field, .-_td_locate_field
	.p2align 4,,15
	.globl	_td_fetch_value
	.hidden	_td_fetch_value
	.type	_td_fetch_value, @function
_td_fetch_value:
	pushq	%r12
	pushq	%rbp
	movq	%r9, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%r8, 8(%rsp)
	leaq	8(%rsp), %r8
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L47
	movl	(%rbx), %edx
	cmpl	$8, %edx
	je	.L56
	cmpl	$134217728, %edx
	je	.L56
	cmpl	$32, %edx
	je	.L58
	cmpl	$64, %edx
	je	.L59
	cmpl	$536870912, %edx
	je	.L60
	cmpl	$1073741824, %edx
	movl	$15, %eax
	jne	.L47
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$8, %ecx
	call	ps_pdread@PLT
	movq	24(%rsp), %rdx
	bswap	%rdx
	movq	%rdx, (%r12)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L56:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$1, %ecx
	call	ps_pdread@PLT
	movzbl	24(%rsp), %edx
	movq	%rdx, (%r12)
.L51:
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L47:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rsp), %rsi
	movq	16(%rbp), %rdi
	leaq	24(%rsp), %rdx
	movl	$4, %ecx
	call	ps_pdread@PLT
	movl	24(%rsp), %esi
	movq	%rsi, (%r12)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L59:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$8, %ecx
	call	ps_pdread@PLT
	movq	24(%rsp), %rdx
	movq	%rdx, (%r12)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L60:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$4, %ecx
	call	ps_pdread@PLT
	movl	24(%rsp), %edx
	bswap	%edx
	movl	%edx, %ebx
	movq	%rbx, (%r12)
	jmp	.L51
	.size	_td_fetch_value, .-_td_fetch_value
	.p2align 4,,15
	.globl	_td_store_value
	.hidden	_td_store_value
	.type	_td_store_value, @function
_td_store_value:
	pushq	%r12
	pushq	%rbp
	movq	%r9, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$32, %rsp
	movq	%r8, 8(%rsp)
	leaq	8(%rsp), %r8
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L61
	movl	(%rbx), %edx
	cmpl	$8, %edx
	je	.L70
	cmpl	$134217728, %edx
	je	.L70
	cmpl	$32, %edx
	je	.L72
	cmpl	$64, %edx
	je	.L73
	cmpl	$536870912, %edx
	je	.L74
	cmpl	$1073741824, %edx
	movl	$15, %eax
	jne	.L61
	bswap	%r12
.L73:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$8, %ecx
	movq	%r12, 24(%rsp)
	call	ps_pdwrite@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L70:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$1, %ecx
	movb	%r12b, 24(%rsp)
	call	ps_pdwrite@PLT
.L65:
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
.L61:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	bswap	%r12d
.L72:
	movq	16(%rbp), %rdi
	movq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	movl	$4, %ecx
	movl	%r12d, 24(%rsp)
	call	ps_pdwrite@PLT
	jmp	.L65
	.size	_td_store_value, .-_td_store_value
	.p2align 4,,15
	.globl	_td_fetch_value_local
	.hidden	_td_fetch_value_local
	.type	_td_fetch_value_local, @function
_td_fetch_value_local:
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%r8, 8(%rsp)
	leaq	8(%rsp), %r8
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L75
	movl	(%rbx), %edx
	cmpl	$8, %edx
	je	.L83
	cmpl	$134217728, %edx
	je	.L83
	cmpl	$32, %edx
	je	.L85
	cmpl	$64, %edx
	je	.L86
	cmpl	$536870912, %edx
	je	.L87
	cmpl	$1073741824, %edx
	jne	.L82
	movq	8(%rsp), %rdx
	movq	(%rdx), %rdx
	bswap	%rdx
	movq	%rdx, 0(%rbp)
.L75:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L83:
	movq	8(%rsp), %rdx
	movzbl	(%rdx), %edx
	movq	%rdx, 0(%rbp)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	movq	8(%rsp), %rdx
	movl	(%rdx), %esi
	movq	%rsi, 0(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L86:
	movq	8(%rsp), %rdx
	movq	(%rdx), %rdx
	movq	%rdx, 0(%rbp)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L82:
	movl	$15, %eax
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L87:
	movq	8(%rsp), %rdx
	movl	(%rdx), %edx
	bswap	%edx
	movl	%edx, %esi
	movq	%rsi, 0(%rbp)
	jmp	.L75
	.size	_td_fetch_value_local, .-_td_fetch_value_local
	.p2align 4,,15
	.globl	_td_store_value_local
	.hidden	_td_store_value_local
	.type	_td_store_value_local, @function
_td_store_value_local:
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	%r8, 8(%rsp)
	leaq	8(%rsp), %r8
	call	_td_locate_field
	testl	%eax, %eax
	jne	.L88
	movl	(%rbx), %edx
	cmpl	$8, %edx
	je	.L96
	cmpl	$134217728, %edx
	je	.L96
	cmpl	$32, %edx
	je	.L98
	cmpl	$64, %edx
	je	.L99
	cmpl	$536870912, %edx
	je	.L100
	cmpl	$1073741824, %edx
	jne	.L95
	bswap	%rbp
.L99:
	movq	8(%rsp), %rdx
	movq	%rbp, (%rdx)
.L88:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	movq	8(%rsp), %rdx
	movb	%bpl, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	bswap	%ebp
.L98:
	movq	8(%rsp), %rdx
	movl	%ebp, (%rdx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	movl	$15, %eax
	jmp	.L88
	.size	_td_store_value_local, .-_td_store_value_local
	.hidden	td_mod_lookup
