	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_clear_event\n"
.LC1:
	.string	"libpthread.so.0"
.LC2:
	.string	"td_ta_clear_event.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"(ta)->ta_sizeof_td_thr_events_t != 0"
	.text
	.p2align 4,,15
	.globl	td_ta_clear_event
	.type	td_ta_clear_event, @function
td_ta_clear_event:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	movq	%rsi, %r13
	subq	$56, %rsp
	movl	__td_debug(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L26
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L18
.L6:
	cmpq	%rax, %r15
	jne	.L27
	movq	272(%r15), %r12
	testq	%r12, %r12
	je	.L28
.L15:
	movl	200(%r15), %ecx
	testl	%ecx, %ecx
	je	.L29
.L8:
	leaq	30(%rcx), %rax
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	shrq	$4, %rax
	salq	$4, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rbx
	andq	$-16, %rbx
	movq	%rbx, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L9
	leaq	204(%r15), %rax
	xorl	%r14d, %r14d
	movq	%rax, -72(%rbp)
	leaq	-56(%rbp), %rax
	movq	%rax, -80(%rbp)
.L11:
	movq	-80(%rbp), %r9
	movq	-72(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$17, %edx
	movq	%r15, %rdi
	movl	%r14d, -84(%rbp)
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L10
	movl	0(%r13,%r14,4), %r9d
	movq	-72(%rbp), %rsi
	movq	%rbx, %r8
	movq	%r14, %rcx
	movl	$17, %edx
	movq	%r15, %rdi
	notl	%r9d
	andl	-56(%rbp), %r9d
	movq	%r9, -56(%rbp)
	call	_td_store_value_local
	testl	%eax, %eax
	jne	.L10
	cmpq	$1, %r14
	jne	.L19
.L14:
	movl	200(%r15), %ecx
	testl	%ecx, %ecx
	je	.L30
	movq	16(%r15), %rdi
	movq	%rbx, %rdx
	movq	%r12, %rsi
	call	ps_pdwrite@PLT
	testl	%eax, %eax
	setne	%al
	leaq	-40(%rbp), %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$8, %eax
.L1:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	200(%r15), %rsi
	movl	$16, %edx
	movq	%r15, %rdi
	call	_td_check_sizeof
	testl	%eax, %eax
	jne	.L1
	movl	200(%r15), %ecx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	.LC0(%rip), %rsi
	movl	$18, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L19:
	movl	$1, %r14d
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	16(%r15), %rdi
	leaq	272(%r15), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$24, %edx
	call	td_mod_lookup
	testl	%eax, %eax
	jne	.L9
	movq	272(%r15), %r12
	jmp	.L15
.L10:
	cmpl	$16, %eax
	jne	.L1
	movl	-84(%rbp), %eax
	movl	0(%r13,%rax,4), %edx
	testl	%edx, %edx
	jne	.L21
	testl	%eax, %eax
	jne	.L14
	movl	4(%r13), %eax
	testl	%eax, %eax
	je	.L14
.L21:
	movl	$13, %eax
	jmp	.L1
.L30:
	leaq	__PRETTY_FUNCTION__.9039(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$73, %edx
	call	__assert_fail@PLT
	.size	td_ta_clear_event, .-td_ta_clear_event
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9039, @object
	.size	__PRETTY_FUNCTION__.9039, 18
__PRETTY_FUNCTION__.9039:
	.string	"td_ta_clear_event"
	.hidden	td_mod_lookup
	.hidden	_td_check_sizeof
	.hidden	_td_store_value_local
	.hidden	_td_fetch_value_local
	.hidden	__td_agent_list
	.hidden	__td_debug
