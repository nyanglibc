	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_new\n"
.LC1:
	.string	"libpthread.so.0"
.LC2:
	.string	"2.33"
	.text
	.p2align 4,,15
	.globl	td_ta_new
	.type	td_ta_new, @function
td_ta_new:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	__td_debug(%rip), %eax
	testl	%eax, %eax
	jne	.L13
.L2:
	leaq	8(%rsp), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$21, %edx
	movq	%rbx, %rdi
	call	td_mod_lookup
	testl	%eax, %eax
	movl	$12, %edx
	jne	.L1
	leaq	3(%rsp), %rbp
	movq	8(%rsp), %rsi
	movl	$5, %ecx
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	call	ps_pdread@PLT
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1
	cmpl	$858992178, 3(%rsp)
	je	.L14
.L4:
	movl	$22, %edx
.L1:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	.LC0(%rip), %rsi
	movl	$10, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L14:
	cmpb	$0, 4(%rbp)
	jne	.L4
	movl	$720, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L15
	movq	__td_agent_list(%rip), %rdx
	movq	%rbx, 16(%rax)
	leaq	__td_agent_list(%rip), %rbx
	movq	%rbx, 8(%rax)
	movq	%rdx, (%rax)
	movq	%rax, 8(%rdx)
	xorl	%edx, %edx
	movq	%rax, __td_agent_list(%rip)
	addq	$16, %rsp
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	ret
.L15:
	movl	$18, %edx
	jmp	.L1
	.size	td_ta_new, .-td_ta_new
	.hidden	__td_agent_list
	.globl	__td_agent_list
	.section	.data.rel.local,"aw",@progbits
	.align 16
	.type	__td_agent_list, @object
	.size	__td_agent_list, 16
__td_agent_list:
	.quad	__td_agent_list
	.quad	__td_agent_list
	.hidden	td_mod_lookup
	.hidden	__td_debug
