	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"td_ta_tsd_iter\n"
.LC1:
	.string	"libpthread.so.0"
	.text
	.p2align 4,,15
	.globl	td_ta_tsd_iter
	.type	td_ta_tsd_iter, @function
td_ta_tsd_iter:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	subq	$88, %rsp
	movq	%rdx, -104(%rbp)
	movl	__td_debug(%rip), %edx
	movq	%rsi, -96(%rbp)
	testl	%edx, %edx
	jne	.L29
.L2:
	movq	__td_agent_list(%rip), %rax
	leaq	__td_agent_list(%rip), %rdx
	cmpq	%rdx, %rax
	jne	.L6
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L18
.L6:
	cmpq	%rax, %r15
	jne	.L30
	leaq	360(%r15), %rsi
	leaq	-72(%rbp), %r8
	movl	$1, %ecx
	movl	$32, %edx
	movq	%r15, %rdi
	movq	$0, -72(%rbp)
	call	_td_locate_field
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L1
	movq	-72(%rbp), %rax
	movl	364(%r15), %r8d
	movq	352(%r15), %rsi
	movq	16(%r15), %rdi
	leaq	7(%rax), %r13
	testq	%rax, %rax
	cmovns	%rax, %r13
	sarq	$3, %r13
	imulq	%r13, %r8
	leaq	30(%r8), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	testq	%rsi, %rsi
	movq	%r12, %rbx
	je	.L31
.L7:
	movq	%r8, %rcx
	movq	%r12, %rdx
	movq	%rsi, -72(%rbp)
	call	ps_pdread@PLT
	testl	%eax, %eax
	jne	.L9
	movl	364(%r15), %eax
	testl	%eax, %eax
	je	.L1
	leaq	376(%r15), %rax
	leaq	-56(%rbp), %rcx
	movl	%r14d, -116(%rbp)
	xorl	%r12d, %r12d
	movq	%rax, -88(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rcx, -112(%rbp)
	movq	%rax, %r14
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L15:
	addq	%r13, %rbx
	addl	$1, %r12d
	cmpl	%r12d, 364(%r15)
	jbe	.L32
.L13:
	movq	-88(%rbp), %rsi
	xorl	%ecx, %ecx
	movq	%r14, %r9
	movq	%rbx, %r8
	movl	$34, %edx
	movq	%r15, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L11
	testb	$1, -64(%rbp)
	je	.L15
	movq	-112(%rbp), %r9
	leaq	388(%r15), %rsi
	xorl	%ecx, %ecx
	movq	%rbx, %r8
	movl	$35, %edx
	movq	%r15, %rdi
	call	_td_fetch_value_local
	testl	%eax, %eax
	jne	.L11
	movq	-104(%rbp), %rdx
	movq	-56(%rbp), %rsi
	movl	%r12d, %edi
	movq	-96(%rbp), %rax
	call	*%rax
	testl	%eax, %eax
	je	.L15
	movl	$15, %eax
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%eax, %r14d
.L1:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$8, %r14d
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	.LC0(%rip), %rsi
	movl	$15, %edx
	movl	$2, %edi
	call	write@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	352(%r15), %rcx
	leaq	.LC1(%rip), %rsi
	movl	$31, %edx
	movq	%r8, -88(%rbp)
	call	td_mod_lookup
	testl	%eax, %eax
	movq	-88(%rbp), %r8
	je	.L8
.L9:
	movl	$1, %r14d
	jmp	.L1
.L8:
	movq	352(%r15), %rsi
	movq	16(%r15), %rdi
	jmp	.L7
.L32:
	movl	-116(%rbp), %r14d
	jmp	.L1
	.size	td_ta_tsd_iter, .-td_ta_tsd_iter
	.hidden	td_mod_lookup
	.hidden	_td_fetch_value_local
	.hidden	_td_locate_field
	.hidden	__td_agent_list
	.hidden	__td_debug
