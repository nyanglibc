	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/protocols"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	rewind@PLT
	movl	$1, %eax
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.section	.rodata.str1.1
.LC1:
	.string	"#\n"
	.text
	.p2align 4,,15
	.globl	__GI__nss_files_parse_protoent
	.hidden	__GI__nss_files_parse_protoent
	.type	__GI__nss_files_parse_protoent, @function
__GI__nss_files_parse_protoent:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	leaq	(%rdx,%rcx), %rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$40, %rsp
	cmpq	%rdi, %rbp
	movq	%r8, 8(%rsp)
	jbe	.L37
	cmpq	%rdi, %rdx
	jbe	.L71
.L37:
	movq	%r15, (%rsp)
.L10:
	leaq	.LC1(%rip), %rsi
	movq	%r14, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L11
	movb	$0, (%rax)
.L11:
	movq	%r14, (%r12)
	movzbl	(%r14), %ebx
	testb	%bl, %bl
	je	.L38
	call	__ctype_b_loc@PLT
	movq	(%rax), %rsi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	testb	%bl, %bl
	movq	%r13, %r14
	je	.L12
.L13:
	movsbq	%bl, %rdx
	leaq	1(%r14), %r13
	movsbq	1(%r14), %rbx
	testb	$32, 1(%rsi,%rdx,2)
	je	.L14
	movb	$0, (%r14)
	movq	%r14, %r13
	movq	(%rax), %rax
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L16:
	movsbq	1(%r13), %rbx
.L34:
	addq	$1, %r13
	testb	$32, 1(%rax,%rbx,2)
	jne	.L16
.L12:
	leaq	24(%rsp), %rsi
	movl	$10, %edx
	movq	%r13, %rdi
	call	strtoul@PLT
	movq	24(%rsp), %rbx
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cmpq	%r13, %rbx
	movl	%eax, 16(%r12)
	je	.L17
	call	__ctype_b_loc@PLT
	movsbq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%rax, %r13
	testb	$32, 1(%rcx,%rdx,2)
	je	.L18
	leaq	1(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rax, %rbx
	movq	%rax, 24(%rsp)
	addq	$1, %rax
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L19
.L20:
	cmpq	$0, (%rsp)
	je	.L72
.L22:
	movq	(%rsp), %r14
	addq	$7, %r14
	andq	$-8, %r14
	leaq	16(%r14), %r9
	movq	%r14, %rdi
.L24:
	cmpq	%r9, %rbp
	jb	.L73
.L25:
	movsbq	(%rbx), %rax
	testb	%al, %al
	je	.L26
	movq	0(%r13), %rcx
	testb	$32, 1(%rcx,%rax,2)
	je	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L28
	testb	%dl, %dl
	je	.L24
.L27:
	movq	%rbx, %rdx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L69:
	testb	$32, 1(%rcx,%rax,2)
	jne	.L31
	movq	%rsi, %rdx
.L30:
	movsbq	1(%rdx), %rax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L69
	cmpq	%rbx, %rsi
	ja	.L35
	cmpq	%r9, %rbp
	movq	%rsi, %rbx
	jnb	.L25
.L73:
	movq	8(%rsp), %rax
	movl	$34, (%rax)
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	testb	%dl, %dl
	je	.L20
.L17:
	xorl	%eax, %eax
.L9:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	call	strlen@PLT
	leaq	1(%r14,%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L31:
	cmpq	%rbx, %rsi
	jbe	.L74
.L35:
	movq	%rbx, (%rdi)
	cmpb	$0, 1(%rdx)
	leaq	8(%rdi), %rax
	je	.L40
.L36:
	leaq	2(%rdx), %rbx
	movb	$0, (%rsi)
	movq	%rax, %rdi
	leaq	16(%rax), %r9
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	testq	%r14, %r14
	movq	$0, (%rdi)
	je	.L41
	movq	%r14, 8(%r12)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rax, %rdi
	movq	%rsi, %rbx
	leaq	16(%rax), %r9
	jmp	.L24
.L72:
	cmpq	%rbx, %r15
	ja	.L39
	cmpq	%rbx, %rbp
	ja	.L75
.L39:
	movq	%r15, (%rsp)
	jmp	.L22
.L38:
	movq	%r14, %r13
	jmp	.L12
.L75:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L22
.L41:
	movl	$-1, %eax
	jmp	.L9
.L74:
	movq	%rdi, %rax
	jmp	.L36
	.size	__GI__nss_files_parse_protoent, .-__GI__nss_files_parse_protoent
	.globl	_nss_files_parse_protoent
	.set	_nss_files_parse_protoent,__GI__nss_files_parse_protoent
	.p2align 4,,15
	.type	internal_getent, @function
internal_getent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	errno@gottpoff(%rip), %rax
	cmpq	$1, %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, 12(%rsp)
	jbe	.L85
	leaq	24(%rsp), %r15
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, (%rsp)
	movq	%rdi, %r12
.L77:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__nss_readline@PLT
	cmpl	$2, %eax
	je	.L87
	testl	%eax, %eax
	jne	.L81
	movq	(%rsp), %rsi
	movq	%r13, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	__GI__nss_files_parse_protoent
	movq	24(%rsp), %rsi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	__nss_parse_line_result@PLT
	testl	%eax, %eax
	je	.L88
	cmpl	$22, %eax
	je	.L77
.L81:
	cmpl	$34, %eax
	movl	%eax, 0(%r13)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L76:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L88:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	movl	$1, %eax
	jmp	.L76
.L85:
	movl	$34, (%r8)
	movl	$-2, %eax
	jmp	.L76
	.size	internal_getent, .-internal_getent
	.p2align 4,,15
	.globl	_nss_files_setprotoent
	.type	_nss_files_setprotoent, @function
_nss_files_setprotoent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L90
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L90:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L89
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L89:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_setprotoent, .-_nss_files_setprotoent
	.p2align 4,,15
	.globl	_nss_files_endprotoent
	.type	_nss_files_endprotoent, @function
_nss_files_endprotoent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L100
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L100:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L101
	call	fclose@PLT
	movq	$0, stream(%rip)
.L101:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L102
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L102:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endprotoent, .-_nss_files_endprotoent
	.p2align 4,,15
	.globl	_nss_files_getprotoent_r
	.type	_nss_files_getprotoent_r, @function
_nss_files_getprotoent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L114
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L114:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L125
.L115:
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	movl	%eax, %ebx
.L116:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L113
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L113:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L125:
	movq	errno@gottpoff(%rip), %rax
	leaq	stream(%rip), %rdi
	movl	%fs:(%rax), %r15d
	call	internal_setent
	movl	%eax, %ebx
	movq	errno@gottpoff(%rip), %rax
	cmpl	$1, %ebx
	movl	%r15d, %fs:(%rax)
	jne	.L116
	movq	stream(%rip), %rdi
	jmp	.L115
	.size	_nss_files_getprotoent_r, .-_nss_files_getprotoent_r
	.p2align 4,,15
	.globl	_nss_files_getprotobyname_r
	.type	_nss_files_getprotobyname_r, @function
_nss_files_getprotobyname_r:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$40, %rsp
	leaq	24(%rsp), %rdi
	movq	%rdx, 8(%rsp)
	movq	$0, 24(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %r12d
	je	.L141
.L126:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movq	8(%rsp), %rdx
	movq	24(%rsp), %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rbp, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %r12d
	jne	.L129
	movq	0(%rbp), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L129
	movq	8(%rbp), %r13
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L130
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L148:
	addq	$8, %r13
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L141
.L130:
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L148
.L129:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L126
	call	fclose@PLT
	jmp	.L126
	.size	_nss_files_getprotobyname_r, .-_nss_files_getprotobyname_r
	.p2align 4,,15
	.globl	_nss_files_getprotobynumber_r
	.type	_nss_files_getprotobynumber_r, @function
_nss_files_getprotobynumber_r:
	pushq	%r15
	pushq	%r14
	movl	%edi, %r15d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movq	%r8, %r14
	subq	$24, %rsp
	leaq	8(%rsp), %rdi
	movq	$0, 8(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L150
.L149:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	cmpl	%r15d, 16(%rbp)
	je	.L152
.L150:
	movq	8(%rsp), %rdi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L153
.L152:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L149
	call	fclose@PLT
	jmp	.L149
	.size	_nss_files_getprotobynumber_r, .-_nss_files_getprotobynumber_r
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
