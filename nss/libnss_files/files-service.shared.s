	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/services"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	rewind@PLT
	movl	$1, %eax
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.section	.rodata.str1.1
.LC1:
	.string	"#\n"
	.text
	.p2align 4,,15
	.globl	__GI__nss_files_parse_servent
	.hidden	__GI__nss_files_parse_servent
	.type	__GI__nss_files_parse_servent, @function
__GI__nss_files_parse_servent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	(%rdx,%rcx), %rbx
	movq	%rdi, %r12
	movq	%rdx, %rbp
	subq	$56, %rsp
	cmpq	%rdi, %rbx
	movq	%r8, 8(%rsp)
	jbe	.L42
	cmpq	%rdi, %rdx
	jbe	.L77
.L42:
	movq	%rbp, (%rsp)
.L10:
	leaq	.LC1(%rip), %rsi
	movq	%r12, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L11
	movb	$0, (%rax)
.L11:
	movq	%r12, 0(%r13)
	movzbl	(%r12), %r15d
	testb	%r15b, %r15b
	je	.L43
	call	__ctype_b_loc@PLT
	movq	(%rax), %rsi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	testb	%r15b, %r15b
	movq	%r14, %r12
	je	.L12
.L13:
	movsbq	%r15b, %rdx
	leaq	1(%r12), %r14
	movsbq	1(%r12), %r15
	testb	$32, 1(%rsi,%rdx,2)
	je	.L14
	movb	$0, (%r12)
	movq	%r12, %r14
	movq	(%rax), %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L16:
	movsbq	1(%r14), %r15
.L38:
	addq	$1, %r14
	testb	$32, 1(%rax,%r15,2)
	jne	.L16
.L12:
	leaq	40(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	strtoul@PLT
	movl	$4294967295, %edx
	movq	40(%rsp), %rsi
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	rolw	$8, %ax
	cmpq	%r14, %rsi
	movzwl	%ax, %eax
	movl	%eax, 16(%r13)
	je	.L17
	movzbl	(%rsi), %eax
	cmpb	$47, %al
	je	.L78
	testb	%al, %al
	jne	.L17
.L20:
	movq	%rsi, 24(%r13)
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	movb	%dl, 23(%rsp)
	je	.L44
	movq	%rsi, 24(%rsp)
	call	__ctype_b_loc@PLT
	movq	24(%rsp), %rsi
	movq	(%rax), %rdi
	movzbl	23(%rsp), %edx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L24:
	testb	%dl, %dl
	movq	%r15, %rsi
	je	.L21
.L22:
	movsbq	%dl, %rcx
	leaq	1(%rsi), %r15
	movsbq	1(%rsi), %rdx
	testb	$32, 1(%rdi,%rcx,2)
	je	.L24
	movb	$0, (%rsi)
	movq	%rsi, %r15
	movq	(%rax), %rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L26:
	movsbq	1(%r15), %rdx
.L39:
	addq	$1, %r15
	testb	$32, 1(%rax,%rdx,2)
	jne	.L26
.L21:
	cmpq	$0, (%rsp)
	je	.L79
.L27:
	movq	(%rsp), %r12
	addq	$7, %r12
	andq	$-8, %r12
	leaq	16(%r12), %rbp
	movq	%r12, (%rsp)
	movq	%r12, %r14
.L28:
	cmpq	%rbp, %rbx
	jb	.L80
.L29:
	movzbl	(%r15), %r12d
	testb	%r12b, %r12b
	je	.L30
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$1, %r15
	movzbl	(%r15), %r12d
.L31:
	movsbq	%r12b, %rax
	testb	$32, 1(%rcx,%rax,2)
	jne	.L32
	testb	%r12b, %r12b
	je	.L28
	movq	%r15, %rdx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L75:
	testb	$32, 1(%rcx,%rax,2)
	jne	.L35
	movq	%rsi, %rdx
.L34:
	movsbq	1(%rdx), %rax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L75
	cmpq	%rsi, %r15
	jb	.L40
	cmpq	%rbp, %rbx
	movq	%rsi, %r15
	jnb	.L29
.L80:
	movq	8(%rsp), %rax
	movl	$34, (%rax)
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	call	strlen@PLT
	leaq	1(%r12,%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	1(%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rax, 40(%rsp)
	movq	%rax, %rsi
	addq	$1, %rax
	cmpb	$47, -1(%rax)
	je	.L19
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%rsi, %r15
	jnb	.L81
.L40:
	movq	%r15, (%r14)
	cmpb	$0, 1(%rdx)
	leaq	8(%r14), %rax
	je	.L46
.L41:
	leaq	2(%rdx), %r15
	movb	$0, (%rsi)
	movq	%rax, %r14
	leaq	16(%rax), %rbp
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L17:
	xorl	%eax, %eax
.L9:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L30:
	movq	(%rsp), %rax
	movq	$0, (%r14)
	testq	%rax, %rax
	je	.L47
	movq	%rax, 8(%r13)
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L79:
	cmpq	%r15, %rbp
	ja	.L45
	cmpq	%r15, %rbx
	ja	.L82
.L45:
	movq	%rbp, (%rsp)
	jmp	.L27
.L43:
	movq	%r12, %r14
	jmp	.L12
.L82:
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%r15,%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L27
.L44:
	movq	%rsi, %r15
	jmp	.L21
.L47:
	movl	$-1, %eax
	jmp	.L9
.L46:
	movq	%rax, %r14
	movq	%rsi, %r15
	leaq	16(%rax), %rbp
	jmp	.L28
.L81:
	movq	%r14, %rax
	jmp	.L41
	.size	__GI__nss_files_parse_servent, .-__GI__nss_files_parse_servent
	.globl	_nss_files_parse_servent
	.set	_nss_files_parse_servent,__GI__nss_files_parse_servent
	.p2align 4,,15
	.type	internal_getent, @function
internal_getent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	errno@gottpoff(%rip), %rax
	cmpq	$1, %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, 12(%rsp)
	jbe	.L92
	leaq	24(%rsp), %r15
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, (%rsp)
	movq	%rdi, %r12
.L84:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__nss_readline@PLT
	cmpl	$2, %eax
	je	.L94
	testl	%eax, %eax
	jne	.L88
	movq	(%rsp), %rsi
	movq	%r13, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	__GI__nss_files_parse_servent
	movq	24(%rsp), %rsi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	__nss_parse_line_result@PLT
	testl	%eax, %eax
	je	.L95
	cmpl	$22, %eax
	je	.L84
.L88:
	cmpl	$34, %eax
	movl	%eax, 0(%r13)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L83:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L95:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	movl	$1, %eax
	jmp	.L83
.L92:
	movl	$34, (%r8)
	movl	$-2, %eax
	jmp	.L83
	.size	internal_getent, .-internal_getent
	.p2align 4,,15
	.globl	_nss_files_setservent
	.type	_nss_files_setservent, @function
_nss_files_setservent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L97
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L97:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L96
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L96:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_setservent, .-_nss_files_setservent
	.p2align 4,,15
	.globl	_nss_files_endservent
	.type	_nss_files_endservent, @function
_nss_files_endservent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L107
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L107:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	fclose@PLT
	movq	$0, stream(%rip)
.L108:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L109
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L109:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endservent, .-_nss_files_endservent
	.p2align 4,,15
	.globl	_nss_files_getservent_r
	.type	_nss_files_getservent_r, @function
_nss_files_getservent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L121
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L121:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L132
.L122:
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	movl	%eax, %ebx
.L123:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L120
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L120:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	movq	errno@gottpoff(%rip), %rax
	leaq	stream(%rip), %rdi
	movl	%fs:(%rax), %r15d
	call	internal_setent
	movl	%eax, %ebx
	movq	errno@gottpoff(%rip), %rax
	cmpl	$1, %ebx
	movl	%r15d, %fs:(%rax)
	jne	.L123
	movq	stream(%rip), %rdi
	jmp	.L122
	.size	_nss_files_getservent_r, .-_nss_files_getservent_r
	.p2align 4,,15
	.globl	_nss_files_getservbyname_r
	.type	_nss_files_getservbyname_r, @function
_nss_files_getservbyname_r:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rdx, %rbp
	subq	$40, %rsp
	leaq	24(%rsp), %rdi
	movq	%rcx, (%rsp)
	movq	%r8, 8(%rsp)
	movq	$0, 24(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %r12d
	je	.L151
.L133:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L151:
	movq	8(%rsp), %rcx
	movq	(%rsp), %rdx
	movq	%r15, %r8
	movq	24(%rsp), %rdi
	movq	%rbp, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %r12d
	jne	.L137
	testq	%r13, %r13
	je	.L136
	movq	24(%rbp), %rdi
	movq	%r13, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L151
.L136:
	movq	0(%rbp), %rsi
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L137
	movq	8(%rbp), %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.L138
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L159:
	addq	$8, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L151
.L138:
	movq	%rbx, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L159
.L137:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	fclose@PLT
	jmp	.L133
	.size	_nss_files_getservbyname_r, .-_nss_files_getservbyname_r
	.p2align 4,,15
	.globl	_nss_files_getservbyport_r
	.type	_nss_files_getservbyport_r, @function
_nss_files_getservbyport_r:
	pushq	%r15
	pushq	%r14
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%r8, %r12
	movq	%r9, %r13
	subq	$40, %rsp
	leaq	24(%rsp), %rdi
	movq	$0, 24(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %edx
	je	.L172
.L160:
	addq	$40, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	testq	%r15, %r15
	je	.L163
	movq	24(%rbx), %rdi
	movq	%r15, %rsi
	movl	%edx, 12(%rsp)
	call	strcmp@PLT
	testl	%eax, %eax
	movl	12(%rsp), %edx
	je	.L163
.L172:
	movq	24(%rsp), %rdi
	movq	%rbp, %rdx
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbx, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %edx
	jne	.L163
	cmpl	%r14d, 16(%rbx)
	jne	.L172
	jmp	.L178
	.p2align 4,,10
	.p2align 3
.L163:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L160
	movl	%edx, 12(%rsp)
	call	fclose@PLT
	movl	12(%rsp), %edx
	jmp	.L160
	.size	_nss_files_getservbyport_r, .-_nss_files_getservbyport_r
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
