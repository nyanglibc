	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"#\n"
.LC1:
	.string	":include:"
	.text
	.p2align 4,,15
	.type	get_next_alias, @function
get_next_alias:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbx
	andq	$-8, %rbx
	leaq	-1(%rbx), %rax
	subq	$56, %rsp
	movq	$0, 8(%rdx)
	movq	%rdi, 8(%rsp)
	movq	%rsi, 32(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r9, 40(%rsp)
	movq	%rax, 24(%rsp)
.L2:
	cmpq	$1, %rbx
	jbe	.L4
.L128:
	movq	24(%rsp), %rax
	movq	8(%rsp), %rdx
	movl	%ebx, %esi
	movq	%r13, %rdi
	leaq	0(%r13,%rax), %rbp
	movb	$-1, 0(%rbp)
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L54
	cmpb	$-1, 0(%rbp)
	jne	.L4
	call	__ctype_b_loc@PLT
	testl	%r14d, %r14d
	movq	%rax, %rbp
	je	.L8
	movsbq	0(%r13), %rdx
	movq	(%rax), %rax
	testb	$32, 1(%rax,%rdx,2)
	je	.L8
.L9:
	cmpq	$1, %rbx
	movl	$1, %r14d
	ja	.L128
	.p2align 4,,10
	.p2align 3
.L4:
	movq	40(%rsp), %rax
	movl	$34, (%rax)
	movl	$-2, %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L10
	movb	$0, (%rax)
.L10:
	movq	0(%rbp), %rdx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$1, %r15
.L11:
	movsbq	(%r15), %rax
	testb	$32, 1(%rdx,%rax,2)
	jne	.L12
	movq	16(%rsp), %rax
	movq	%r13, (%rax)
	movzbl	(%r15), %eax
	cmpb	$58, %al
	je	.L2
	testb	%al, %al
	je	.L2
	movq	%r13, %rcx
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L55:
	movq	%r8, %r15
	movq	%rdx, %rcx
.L14:
	leaq	1(%rcx), %rdx
	movb	%al, -1(%rdx)
	movzbl	1(%r15), %eax
	leaq	1(%r15), %r8
	testb	%al, %al
	setne	%dil
	cmpb	$58, %al
	setne	%sil
	testb	%sil, %dil
	jne	.L55
	testb	%al, %al
	je	.L2
	movq	16(%rsp), %rax
	cmpq	%rdx, (%rax)
	je	.L2
	leaq	2(%rcx), %r12
	movb	$0, (%rdx)
	movq	(%rax), %rdi
	movq	%r12, %r14
	subq	%rdi, %r14
	cmpq	%rbx, %r14
	ja	.L4
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rsi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L9
.L15:
	subq	%r14, %rbx
	movzbl	2(%r15), %r14d
	movq	%r12, %rax
	movq	%rbx, %r13
	leaq	2(%r15), %rbx
	movq	%r13, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L50:
	movq	0(%rbp), %rdx
	movsbq	%r14b, %rax
	testb	$32, 1(%rdx,%rax,2)
	je	.L51
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$1, %rbx
	movsbq	(%rbx), %rax
	testb	$32, 1(%rdx,%rax,2)
	movq	%rax, %r14
	jne	.L16
.L51:
	testb	%r14b, %r14b
	je	.L17
	cmpb	$44, %r14b
	je	.L17
	movq	%r13, %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L129:
	cmpb	$44, %r14b
	je	.L60
	movq	%r15, %rbx
	movq	%rax, %rdx
.L18:
	leaq	1(%rdx), %rax
	movb	%r14b, -1(%rax)
	movzbl	1(%rbx), %r14d
	leaq	1(%rbx), %r15
	testb	%r14b, %r14b
	jne	.L129
.L60:
	cmpq	%rax, %r13
	je	.L57
	leaq	.LC1(%rip), %rdi
	addq	$2, %rbx
	movb	$0, (%rax)
	testb	%r14b, %r14b
	movl	$9, %ecx
	movq	%r13, %rsi
	cmovne	%rbx, %r15
	repz cmpsb
	je	.L21
	addq	$2, %rdx
	movq	%rdx, %rax
	subq	%r13, %rax
	leaq	8(%rax), %rcx
	cmpq	%r12, %rcx
	ja	.L4
	leaq	-8(%r12), %r8
	movq	%r15, %rbx
	movq	%rdx, %r13
	subq	%rax, %r8
	movq	16(%rsp), %rax
	movq	%r8, %r12
	addq	$1, 8(%rax)
	movzbl	(%r15), %r14d
.L17:
	testb	%r14b, %r14b
	jne	.L50
	movq	8(%rsp), %rsi
	movq	8(%rsi), %rax
	cmpq	16(%rsi), %rax
	jnb	.L130
	movq	8(%rsp), %rdi
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
	xorl	%edx, %edx
.L42:
	cmpl	$10, %eax
	je	.L43
	testb	%dl, %dl
	jne	.L43
	movq	0(%rbp), %rcx
	movslq	%eax, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L131
	leaq	-1(%r13,%r12), %r15
	movq	8(%rsp), %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movb	$-1, (%r15)
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L132
	cmpb	$-1, (%r15)
	jne	.L4
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L126
	movb	$0, (%rax)
.L126:
	movzbl	(%rbx), %r14d
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	9(%r13), %rdi
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L125
	movq	%r15, %rdi
	call	strdup@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L125
.L23:
	testb	$16, (%r14)
	jne	.L133
	cmpq	$1, %r12
	jbe	.L124
	leaq	-1(%r13,%r12), %rcx
	movq	%r14, %rdx
	movl	%r12d, %esi
	movq	%r13, %rdi
	movb	$-1, (%rcx)
	movq	%rcx, 24(%rsp)
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	24(%rsp), %rcx
	je	.L25
	cmpb	$-1, (%rcx)
	jne	.L124
	leaq	.LC0(%rip), %rsi
	movq	%rax, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L27
	movb	$0, (%rax)
.L27:
	movzbl	(%r15), %eax
	movq	0(%rbp), %rcx
	.p2align 4,,10
	.p2align 3
.L28:
	movsbq	%al, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L29
.L135:
	cmpb	$44, %al
	je	.L30
	testb	%al, %al
	je	.L30
	movq	%r13, %rdx
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$1, %r15
	addq	$1, %rdx
	movb	%al, -1(%rdx)
	movzbl	(%r15), %eax
	testb	%al, %al
	je	.L61
	cmpb	$44, %al
	jne	.L31
.L61:
	testb	%al, %al
	je	.L33
.L52:
	addq	$1, %r15
.L33:
	cmpq	%r13, %rdx
	je	.L134
	leaq	1(%rdx), %rcx
	movb	$0, (%rdx)
	movq	%rcx, %rax
	subq	%r13, %rax
	leaq	8(%rax), %rdx
	cmpq	%r12, %rdx
	ja	.L124
	leaq	-8(%r12), %r8
	movq	%rcx, %r13
	subq	%rax, %r8
	movq	16(%rsp), %rax
	movq	%r8, %r12
	addq	$1, 8(%rax)
	movzbl	(%r15), %eax
.L35:
	testb	%al, %al
	je	.L23
	movq	0(%rbp), %rcx
	movsbq	%al, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	je	.L135
.L29:
	movzbl	1(%r15), %eax
	addq	$1, %r15
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L134:
	movzbl	(%r15), %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L125:
	movzbl	(%r15), %r14d
	movq	%r15, %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r15, %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L30:
	testb	%al, %al
	je	.L23
	movq	%r13, %rdx
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L132:
	movb	$0, 0(%r13)
	movq	%r13, %rbx
	jmp	.L50
	.p2align 4,,10
	.p2align 3
.L124:
	movq	%rbx, %rdi
	call	free@PLT
	movq	%r14, %rdi
	call	fclose@PLT
	jmp	.L4
.L130:
	movq	%rsi, %rdi
	call	__uflow@PLT
	cmpl	$-1, %eax
	sete	%dl
	jmp	.L42
.L131:
	movq	%r13, %r12
.L44:
	movq	8(%rsp), %rsi
	movl	%eax, %edi
	call	ungetc@PLT
.L46:
	movq	16(%rsp), %rax
	leaq	7(%r12), %rbx
	andq	$-8, %rbx
	movq	%rbx, 16(%rax)
	movq	(%rax), %r12
	movq	8(%rax), %rax
	testq	%rax, %rax
	je	.L58
	leaq	(%rbx,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L47:
	movq	%r12, %rdi
	addq	$8, %rbx
	call	strlen@PLT
	leaq	1(%r12,%rax), %r12
	movq	%r12, -8(%rbx)
	cmpq	%rbx, %rbp
	jne	.L47
	addq	$56, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L43:
	cmpl	$-1, %eax
	movq	%r13, %r12
	je	.L46
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	-1(%r13,%r12), %rcx
.L25:
	movq	%r14, %rdi
	movq	%rcx, 24(%rsp)
	call	fclose@PLT
	movq	24(%rsp), %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movb	$0, (%rcx)
	call	strncpy@PLT
	movq	%rbx, %rdi
	call	free@PLT
	movq	24(%rsp), %rcx
	cmpb	$0, (%rcx)
	jne	.L4
	movzbl	0(%r13), %r14d
	movq	%r13, %rbx
	jmp	.L17
.L54:
	xorl	%eax, %eax
	jmp	.L1
.L58:
	movl	$2, %eax
	jmp	.L1
	.size	get_next_alias, .-get_next_alias
	.section	.rodata.str1.1
.LC2:
	.string	"/etc/aliases"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L142
	call	rewind@PLT
	movl	$1, %eax
.L136:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	.LC2(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L136
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.p2align 4,,15
	.globl	_nss_files_setaliasent
	.type	_nss_files_setaliasent, @function
_nss_files_setaliasent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L144
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L144:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L143
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L143:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_setaliasent, .-_nss_files_setaliasent
	.p2align 4,,15
	.globl	_nss_files_endaliasent
	.type	_nss_files_endaliasent, @function
_nss_files_endaliasent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L154
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L154:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L155
	call	fclose@PLT
	movq	$0, stream(%rip)
.L155:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L156
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L156:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endaliasent, .-_nss_files_endaliasent
	.p2align 4,,15
	.globl	_nss_files_getaliasent_r
	.type	_nss_files_getaliasent_r, @function
_nss_files_getaliasent_r:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L168
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L168:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L180
.L169:
	movl	$1, 24(%rbp)
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L181:
	movq	stream(%rip), %rdi
.L171:
	xorl	%esi, %esi
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbp, %rdx
	call	get_next_alias
	cmpl	$2, %eax
	movl	%eax, %ebx
	je	.L181
.L170:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L167
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L167:
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L170
	movq	stream(%rip), %rdi
	jmp	.L169
	.size	_nss_files_getaliasent_r, .-_nss_files_getaliasent_r
	.p2align 4,,15
	.globl	_nss_files_getaliasbyname_r
	.type	_nss_files_getaliasbyname_r, @function
_nss_files_getaliasbyname_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testq	%rdi, %rdi
	movq	$0, 8(%rsp)
	je	.L193
	movq	%rdi, %rbp
	leaq	8(%rsp), %rdi
	movq	%r8, %r15
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsi, %r12
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L194
.L185:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L182
	call	fclose@PLT
.L182:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L194:
	movl	$1, 24(%r12)
	.p2align 4,,10
	.p2align 3
.L186:
	movq	8(%rsp), %rdi
	movq	%r15, %r9
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	get_next_alias
	cmpl	$2, %eax
	movl	%eax, %ebx
	je	.L186
	jmp	.L185
.L193:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebx
	movl	$22, %fs:(%rax)
	jmp	.L182
	.size	_nss_files_getaliasbyname_r, .-_nss_files_getaliasbyname_r
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
