	.text
	.p2align 4,,15
	.type	strip_whitespace, @function
strip_whitespace:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$1, %rbx
.L2:
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L3
	testb	%dl, %dl
	movq	%rbx, %rax
	jne	.L5
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L16:
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L4
.L5:
	addq	$1, %rax
	movsbq	(%rax), %rdx
	testb	%dl, %dl
	jne	.L16
.L4:
	movb	$0, (%rax)
	cmpb	$0, (%rbx)
	movl	$0, %eax
	cmovne	%rbx, %rax
	popq	%rbx
	ret
	.size	strip_whitespace, .-strip_whitespace
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/netgroup"
	.text
	.p2align 4,,15
	.globl	_nss_files_setnetgrent
	.type	_nss_files_setnetgrent, @function
_nss_files_setnetgrent:
	cmpb	$0, (%rdi)
	je	.L58
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	%rdi, 24(%rsp)
	leaq	.LC0(%rip), %rdi
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L63
	movq	24(%rsp), %rdi
	movq	$0, 32(%rsp)
	leaq	40(%rsp), %r14
	movq	$0, 40(%rsp)
	call	strlen@PLT
	movq	%rax, 16(%rsp)
	movq	32(%r15), %rax
	movq	%rax, 48(%r15)
	leaq	32(%rsp), %rax
	movq	%rax, (%rsp)
.L21:
	movl	(%r12), %ebp
	andl	$16, %ebp
	jne	.L22
	movq	(%rsp), %rdi
	movq	%r12, %rcx
	movl	$10, %edx
	movq	%r14, %rsi
	call	__getdelim@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	js	.L22
	movq	16(%rsp), %rcx
	cmpq	%rax, %rcx
	jge	.L23
	movq	32(%rsp), %r13
	movq	24(%rsp), %rsi
	movq	%rcx, %rdx
	movq	%r13, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L23
	call	__ctype_b_loc@PLT
	movq	16(%rsp), %rcx
	movq	(%rax), %rax
	movsbq	0(%r13,%rcx), %rdx
	testb	$32, 1(%rax,%rdx,2)
	jne	.L64
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	$1, %rbx
	jle	.L21
	movq	32(%rsp), %rdi
	cmpb	$10, -1(%rdi,%rbx)
	jne	.L21
.L41:
	cmpb	$92, -2(%rdi,%rbx)
	je	.L35
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L66:
	subq	$2, 48(%r15)
	movq	(%rsp), %rdi
	movq	%r12, %rcx
	movl	$10, %edx
	movq	%r14, %rsi
	call	__getdelim@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	jle	.L61
	movq	32(%r15), %rbx
	movq	48(%r15), %rax
	leaq	3(%r13), %rsi
	subq	%rbx, %rax
	cmpq	$508, %r13
	movq	%rbx, %rdi
	movq	%rax, 8(%rsp)
	movl	$512, %eax
	cmovle	%rax, %rsi
	addq	40(%r15), %rsi
	movq	%rsi, 40(%r15)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, 32(%r15)
	je	.L65
	addq	8(%rsp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 48(%r15)
	movb	$32, (%rax)
	leaq	1(%r13), %rdx
	movq	48(%r15), %rdi
	movq	32(%rsp), %rsi
	call	memcpy@PLT
	addq	%r13, 48(%r15)
	cmpq	$1, %r13
	movq	%r13, %rax
	je	.L61
	movq	32(%rsp), %rdi
	cmpb	$10, -1(%rdi,%r13)
	jne	.L29
	cmpb	$92, -2(%rdi,%rax)
	jne	.L36
.L35:
	testl	%ebp, %ebp
	jne	.L66
	movq	(%rsp), %rdi
	movq	%r12, %rcx
	movl	$10, %edx
	movq	%r14, %rsi
	call	__getdelim@PLT
	testq	%rax, %rax
	jle	.L21
	cmpq	$1, %rax
	je	.L21
	movq	32(%rsp), %rdi
	cmpb	$10, -1(%rdi,%rax)
	jne	.L21
	cmpb	$92, -2(%rdi,%rax)
	je	.L35
.L36:
	testl	%ebp, %ebp
	je	.L21
.L29:
	movq	32(%r15), %rax
	movl	$1, 56(%r15)
	movq	%rax, 48(%r15)
	call	free@PLT
	movq	%r12, %rdi
	call	fclose@PLT
	movl	$1, %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L64:
	leaq	(%rbx,%rbx), %rsi
	movl	$512, %eax
	movq	32(%r15), %rbp
	movq	48(%r15), %r13
	subq	%rcx, %rsi
	cmpq	$512, %rsi
	movq	%rbp, %rdi
	cmovl	%rax, %rsi
	addq	40(%r15), %rsi
	movq	%rsi, 40(%r15)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, 32(%r15)
	jne	.L24
	movq	%rbp, %rdi
.L62:
	call	free@PLT
	movq	32(%rsp), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	fclose@PLT
	movl	$-1, %eax
.L25:
	movq	32(%r15), %rdi
	movl	%eax, (%rsp)
	call	free@PLT
	movl	(%rsp), %eax
	movq	$0, 32(%r15)
	movq	$0, 40(%r15)
	movq	$0, 48(%r15)
.L17:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	movq	errno@gottpoff(%rip), %rdx
	movl	$-2, %eax
	cmpl	$11, %fs:(%rdx)
	je	.L17
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movq	%rbx, %rdi
	jmp	.L62
.L22:
	movq	32(%rsp), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	fclose@PLT
	xorl	%eax, %eax
	jmp	.L25
.L61:
	movq	32(%rsp), %rdi
	jmp	.L29
.L24:
	movq	%r13, %rdx
	movq	16(%rsp), %rcx
	subq	%rbp, %rdx
	movq	%rbx, %rbp
	leaq	(%rax,%rdx), %rdi
	movq	32(%rsp), %rax
	subq	%rcx, %rbp
	movq	%rdi, 48(%r15)
	movq	%rbp, %rdx
	leaq	1(%rax,%rcx), %rsi
	call	memcpy@PLT
	leaq	-1(%rbp), %rax
	addq	%rax, 48(%r15)
	cmpq	$1, %rbx
	movq	32(%rsp), %rdi
	je	.L29
	cmpb	$10, -1(%rdi,%rbx)
	jne	.L29
	movl	$1, %ebp
	jmp	.L41
.L58:
	movl	$-1, %eax
	ret
	.size	_nss_files_setnetgrent, .-_nss_files_setnetgrent
	.p2align 4,,15
	.globl	__GI__nss_files_endnetgrent
	.hidden	__GI__nss_files_endnetgrent
	.type	__GI__nss_files_endnetgrent, @function
__GI__nss_files_endnetgrent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	free@PLT
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movl	$1, %eax
	movq	$0, 48(%rbx)
	popq	%rbx
	ret
	.size	__GI__nss_files_endnetgrent, .-__GI__nss_files_endnetgrent
	.globl	_nss_files_endnetgrent
	.set	_nss_files_endnetgrent,__GI__nss_files_endnetgrent
	.p2align 4,,15
	.globl	__GI__nss_netgroup_parseline
	.hidden	__GI__nss_netgroup_parseline
	.type	__GI__nss_netgroup_parseline, @function
__GI__nss_netgroup_parseline:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L69
	movq	%rcx, %r13
	movq	%r8, %r14
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %rbp
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L72:
	addq	$1, %rbx
.L71:
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L72
	cmpb	$40, %dl
	je	.L73
	testb	%dl, %dl
	je	.L74
	movq	%rbx, %rdx
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L107:
	testb	$32, 1(%rcx,%rax,2)
	jne	.L106
	movq	%rsi, %rdx
.L76:
	movsbq	1(%rdx), %rax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L107
	cmpq	%rsi, %rbx
	je	.L74
	movl	$1, (%r15)
	movq	%rbx, 8(%r15)
	movb	$0, (%rsi)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L106:
	cmpq	%rsi, %rbx
	je	.L74
	movl	$1, (%r15)
	movq	%rbx, 8(%r15)
	movb	$0, (%rsi)
	leaq	2(%rdx), %rsi
.L85:
	movq	%rsi, 0(%rbp)
	movl	$1, %eax
	movl	$0, 56(%r15)
.L69:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	addq	$1, %rbx
	movq	%rbx, %rcx
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L79:
	testb	%al, %al
	je	.L74
.L78:
	movzbl	(%rcx), %eax
	addq	$1, %rcx
	cmpb	$44, %al
	jne	.L79
	movq	%rcx, %r10
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	testb	%al, %al
	je	.L74
.L80:
	movzbl	(%r10), %eax
	addq	$1, %r10
	cmpb	$44, %al
	jne	.L81
	movq	%r10, %r8
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L83:
	testb	%al, %al
	je	.L74
.L82:
	movzbl	(%r8), %eax
	addq	$1, %r8
	cmpb	$41, %al
	jne	.L83
	movq	%r8, %r9
	subq	%rbx, %r9
	cmpq	%r13, %r9
	ja	.L108
	movq	%r9, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	movq	%r8, 24(%rsp)
	movq	%r9, (%rsp)
	movq	%r10, 16(%rsp)
	movq	%rcx, 8(%rsp)
	call	memcpy@PLT
	movq	8(%rsp), %rcx
	movl	$0, (%r15)
	movq	%r12, %rdi
	subq	%rbx, %rcx
	movb	$0, -1(%r12,%rcx)
	movq	%rcx, %r13
	call	strip_whitespace
	movq	16(%rsp), %r10
	leaq	(%r12,%r13), %rdi
	movq	%rax, 8(%r15)
	subq	%rbx, %r10
	movb	$0, -1(%r12,%r10)
	movq	%r10, %rbx
	call	strip_whitespace
	movq	(%rsp), %r9
	leaq	(%r12,%rbx), %rdi
	movq	%rax, 16(%r15)
	movb	$0, -1(%r12,%r9)
	call	strip_whitespace
	movq	24(%rsp), %r8
	movq	%rax, 24(%r15)
	movl	$1, %eax
	movq	%r8, 0(%rbp)
	movl	$0, 56(%r15)
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L74:
	cmpl	$1, 56(%r15)
	sbbl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	andl	$2, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L108:
	movl	$34, (%r14)
	movl	$-2, %eax
	jmp	.L69
	.size	__GI__nss_netgroup_parseline, .-__GI__nss_netgroup_parseline
	.globl	_nss_netgroup_parseline
	.set	_nss_netgroup_parseline,__GI__nss_netgroup_parseline
	.p2align 4,,15
	.globl	_nss_files_getnetgrent_r
	.type	_nss_files_getnetgrent_r, @function
_nss_files_getnetgrent_r:
	movq	%rdi, %rax
	leaq	48(%rdi), %rdi
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rax, %rsi
	jmp	__GI__nss_netgroup_parseline
	.size	_nss_files_getnetgrent_r, .-_nss_files_getnetgrent_r
