	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/group"
	.text
	.p2align 4,,15
	.globl	_nss_files_initgroups_dyn
	.type	_nss_files_initgroups_dyn, @function
_nss_files_initgroups_dyn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	leaq	.LC0(%rip), %rdi
	subq	$1192, %rsp
	movl	%esi, 40(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%r8, 64(%rsp)
	movq	%r9, 72(%rsp)
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	je	.L36
	movq	%rax, %rbx
	leaq	144(%rsp), %rax
	leaq	96(%rsp), %r12
	leaq	88(%rsp), %r15
	movq	$0, 80(%rsp)
	movq	$0, 88(%rsp)
	movq	%rax, 32(%rsp)
	leaq	160(%rsp), %rax
	movq	$1024, 152(%rsp)
	movb	$0, 47(%rsp)
	movq	%rax, 144(%rsp)
	movq	64(%rsp), %rax
	movq	(%rax), %rax
	movq	%rax, 48(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 8(%rsp)
	leaq	112(%rsp), %rax
	movq	%rax, 16(%rsp)
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	fgetpos@PLT
	movq	8(%rsp), %rdi
	movq	%rbx, %rcx
	movl	$10, %edx
	movq	%r15, %rsi
	call	__getdelim@PLT
	testq	%rax, %rax
	js	.L37
	movq	1248(%rsp), %r8
	movq	152(%rsp), %rcx
	movq	144(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	80(%rsp), %rdi
	call	_nss_files_parse_grent@PLT
	cmpl	$-1, %eax
	je	.L38
	testl	%eax, %eax
	jle	.L4
	movl	128(%rsp), %r13d
	cmpl	40(%rsp), %r13d
	je	.L4
	movq	136(%rsp), %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	jne	.L18
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	addq	$8, %r14
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.L4
.L18:
	movq	%rbp, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L12
	movq	24(%rsp), %rax
	movq	56(%rsp), %rdx
	movq	(%rax), %rax
	cmpq	(%rdx), %rax
	je	.L39
.L13:
	movq	48(%rsp), %rcx
	movb	$1, 47(%rsp)
	movl	%r13d, (%rcx,%rax,4)
	movq	24(%rsp), %rcx
	addq	$1, %rax
	movq	%rax, (%rcx)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L38:
	movq	32(%rsp), %rdi
	call	__libc_scratch_buffer_grow@PLT
	testb	%al, %al
	je	.L17
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	fsetpos@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L39:
	movq	72(%rsp), %rdx
	testq	%rdx, %rdx
	jle	.L14
	cmpq	%rdx, %rax
	je	.L40
	movq	72(%rsp), %rdx
	addq	%rax, %rax
	cmpq	%rdx, %rax
	cmovg	%rdx, %rax
	movq	%rax, %r13
.L16:
	movq	48(%rsp), %rdi
	leaq	0(,%r13,4), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L17
	movq	64(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	56(%rsp), %rax
	movq	%r13, (%rax)
	movq	24(%rsp), %rax
	movl	128(%rsp), %r13d
	movq	(%rax), %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L37:
	testb	$16, (%rbx)
	movl	$1, %ebp
	jne	.L19
	movq	errno@gottpoff(%rip), %rax
	movq	1248(%rsp), %rcx
	xorl	%ebp, %ebp
	movb	$1, 47(%rsp)
	movl	%fs:(%rax), %eax
	cmpl	$12, %eax
	movl	%eax, (%rcx)
	setne	%bpl
	subl	$2, %ebp
.L19:
	movq	32(%rsp), %rax
	movq	144(%rsp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L20
	call	free@PLT
.L20:
	movq	80(%rsp), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	fclose@PLT
	cmpb	$0, 47(%rsp)
	movl	$0, %eax
	cmove	%eax, %ebp
.L1:
	addq	$1192, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movq	errno@gottpoff(%rip), %rax
	movq	1248(%rsp), %rcx
	xorl	%ebp, %ebp
	movl	%fs:(%rax), %eax
	cmpl	$12, %eax
	movl	%eax, (%rcx)
	setne	%bpl
	subl	$2, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movq	1248(%rsp), %rax
	movb	$1, 47(%rsp)
	movl	$-2, %ebp
	movl	$12, (%rax)
	jmp	.L19
.L14:
	leaq	(%rax,%rax), %r13
	jmp	.L16
.L40:
	movl	$1, %ebp
	jmp	.L19
	.size	_nss_files_initgroups_dyn, .-_nss_files_initgroups_dyn
