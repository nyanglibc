	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"#\n"
	.text
	.p2align 4,,15
	.type	internal_getent, @function
internal_getent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	(%rdx,%rcx), %r12
	pushq	%rbp
	leaq	32(%rdx), %rbp
	pushq	%rbx
	movq	%r12, %rax
	subq	$104, %rsp
	subq	%rbp, %rax
	cmpq	$33, %rcx
	movq	%rax, 40(%rsp)
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 76(%rsp)
	jbe	.L75
	leaq	88(%rsp), %rax
	movq	%r9, 64(%rsp)
	movq	%r8, 32(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rsi, 24(%rsp)
	movq	%rdi, 16(%rsp)
	movq	%rax, 48(%rsp)
.L2:
	movq	48(%rsp), %rcx
	movq	40(%rsp), %rdx
	movq	%rbp, %rsi
	movq	16(%rsp), %rdi
	call	__nss_readline@PLT
	cmpl	$2, %eax
	movl	%eax, %r14d
	je	.L85
	testl	%eax, %eax
	jne	.L6
	cmpq	%rbp, %r12
	movq	%rbp, %r13
	jbe	.L7
	movq	%rbp, %rdi
	call	strlen@PLT
	leaq	1(%rbp,%rax), %r13
.L7:
	leaq	.LC0(%rip), %rsi
	movq	%rbp, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L8
	movb	$0, (%rax)
.L8:
	movq	8(%rsp), %rax
	movzbl	32(%rax), %edx
	testb	%dl, %dl
	movb	%dl, 56(%rsp)
	je	.L49
	call	__ctype_b_loc@PLT
	movq	%rbp, %rcx
	movq	(%rax), %rdi
	movzbl	56(%rsp), %edx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L11:
	testb	%dl, %dl
	movq	%rbx, %rcx
	je	.L9
.L10:
	movsbq	%dl, %rsi
	leaq	1(%rcx), %rbx
	movsbq	1(%rcx), %rdx
	testb	$32, 1(%rdi,%rsi,2)
	je	.L11
	movb	$0, (%rcx)
	movq	%rcx, %rbx
	movq	(%rax), %rax
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L13:
	movsbq	1(%rbx), %rdx
.L39:
	addq	$1, %rbx
	testb	$32, 1(%rax,%rdx,2)
	jne	.L13
.L9:
	movl	160(%rsp), %ecx
	movq	8(%rsp), %rdx
	movq	%rbp, %rsi
	testl	%ecx, %ecx
	je	.L14
	movl	160(%rsp), %edi
	call	inet_pton@PLT
	testl	%eax, %eax
	jle	.L86
	movq	24(%rsp), %rax
	movl	160(%rsp), %edi
	movl	$4, %edx
	movl	%edi, 16(%rax)
	cmpl	$2, %edi
	movl	$16, %eax
	cmove	%edx, %eax
.L21:
	movq	24(%rsp), %rdi
	movl	%eax, 20(%rdi)
	movq	8(%rsp), %rax
	movq	$0, 24(%rax)
	movq	%rax, 16(%rax)
	addq	$16, %rax
	movq	%rax, 24(%rdi)
	movq	%rbx, (%rdi)
	movzbl	(%rbx), %ecx
	testb	%cl, %cl
	movb	%cl, 56(%rsp)
	je	.L51
	call	__ctype_b_loc@PLT
	movzbl	56(%rsp), %ecx
	movq	(%rax), %rdi
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	testb	%cl, %cl
	movq	%r14, %rbx
	je	.L22
.L23:
	movsbq	%cl, %rdx
	leaq	1(%rbx), %r14
	movzbl	1(%rbx), %ecx
	testb	$32, 1(%rdi,%rdx,2)
	je	.L24
	movb	$0, (%rbx)
	movq	%rbx, %r14
	movq	(%rax), %rax
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L26:
	movzbl	1(%r14), %ecx
.L42:
	movsbq	%cl, %rdx
	addq	$1, %r14
	testb	$32, 1(%rax,%rdx,2)
	jne	.L26
.L22:
	testq	%r13, %r13
	je	.L87
.L27:
	addq	$7, %r13
	andq	$-8, %r13
	leaq	16(%r13), %rbx
	movq	%r13, 56(%rsp)
.L28:
	cmpq	%rbx, %r12
	jb	.L88
.L29:
	movzbl	(%r14), %r15d
	testb	%r15b, %r15b
	je	.L30
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L32:
	addq	$1, %r14
	movzbl	(%r14), %r15d
.L31:
	movsbq	%r15b, %rax
	testb	$32, 1(%rcx,%rax,2)
	jne	.L32
	testb	%r15b, %r15b
	je	.L28
	movq	%r14, %rdx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L79:
	testb	$32, 1(%rcx,%rax,2)
	jne	.L35
	movq	%rsi, %rdx
.L34:
	movsbq	1(%rdx), %rax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L79
	cmpq	%rsi, %r14
	jb	.L46
	cmpq	%rbx, %r12
	movq	%rsi, %r14
	jnb	.L29
.L88:
	movq	32(%rsp), %rax
	movl	$-1, %r14d
	movl	$34, (%rax)
.L18:
	movq	88(%rsp), %rsi
	movq	16(%rsp), %rdi
	movl	%r14d, %edx
	call	__nss_parse_line_result@PLT
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L89
	cmpl	$22, %eax
	je	.L2
.L6:
	movq	32(%rsp), %rax
	movl	%r14d, (%rax)
	movq	64(%rsp), %rax
	movl	$-1, (%rax)
	xorl	%eax, %eax
	cmpl	$34, %r14d
	setne	%al
	subl	$2, %eax
.L1:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$2, %edi
	call	inet_pton@PLT
	testl	%eax, %eax
	jg	.L90
	movq	8(%rsp), %rdx
	movq	%rbp, %rsi
	movl	$10, %edi
	call	inet_pton@PLT
	testl	%eax, %eax
	jle	.L18
	movq	24(%rsp), %rax
	movl	$10, 16(%rax)
	movl	$16, %eax
	jmp	.L21
.L90:
	movq	24(%rsp), %rax
	movl	$2, 16(%rax)
	movl	$4, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L35:
	cmpq	%rsi, %r14
	jnb	.L91
.L46:
	movq	%r14, 0(%r13)
	cmpb	$0, 1(%rdx)
	leaq	8(%r13), %rax
	je	.L53
.L47:
	leaq	2(%rdx), %r14
	movb	$0, (%rsi)
	movq	%rax, %r13
	leaq	16(%rax), %rbx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L30:
	movq	56(%rsp), %rax
	movq	$0, 0(%r13)
	testq	%rax, %rax
	je	.L54
	movq	24(%rsp), %rdi
	movl	$1, %r14d
	movq	%rax, 8(%rdi)
	jmp	.L18
.L87:
	cmpq	%r14, %rbp
	ja	.L52
	cmpq	%r14, %r12
	ja	.L92
.L52:
	movq	%rbp, %r13
	jmp	.L27
.L86:
	cmpl	$2, 160(%rsp)
	jne	.L18
	movq	8(%rsp), %r15
	movq	%rbp, %rsi
	movl	$10, %edi
	movq	%r15, %rdx
	call	inet_pton@PLT
	testl	%eax, %eax
	jle	.L18
	movl	(%r15), %edx
	testl	%edx, %edx
	jne	.L18
	movl	4(%r15), %eax
	testl	%eax, %eax
	jne	.L18
	movl	8(%r15), %eax
	cmpl	$-65536, %eax
	je	.L93
	testl	%eax, %eax
	jne	.L18
	movq	8(%rsp), %rax
	cmpl	$16777216, 12(%rax)
	jne	.L18
	movl	$16777343, (%rax)
	movq	24(%rsp), %rax
	movl	$2, 16(%rax)
	movl	$4, %eax
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rbp, %rbx
	jmp	.L9
.L51:
	movq	%rbx, %r14
	jmp	.L22
.L92:
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	1(%r14,%rax), %r13
	jmp	.L27
.L85:
	movq	64(%rsp), %rax
	movl	76(%rsp), %edi
	movl	$1, (%rax)
	movq	errno@gottpoff(%rip), %rax
	movl	%edi, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1
.L89:
	movq	errno@gottpoff(%rip), %rax
	movl	76(%rsp), %edi
	movl	%edi, %fs:(%rax)
	movl	$1, %eax
	jmp	.L1
.L75:
	movl	$34, (%r8)
	movl	$-2, %eax
	movl	$-1, (%r9)
	jmp	.L1
.L93:
	movq	8(%rsp), %rdi
	movl	12(%rdi), %eax
	movl	%eax, (%rdi)
	movq	24(%rsp), %rax
	movl	$2, 16(%rax)
	movl	$4, %eax
	jmp	.L21
.L54:
	movl	$-1, %r14d
	jmp	.L18
.L91:
	movq	%r13, %rax
	jmp	.L47
.L53:
	movq	%rax, %r13
	movq	%rsi, %r14
	leaq	16(%rax), %rbx
	jmp	.L28
	.size	internal_getent, .-internal_getent
	.p2align 4,,15
	.type	array_add__, @function
array_add__:
	pushq	%r12
	pushq	%rbp
	leaq	24(%rdi), %rbp
	pushq	%rbx
	movq	%rsi, %r12
	movl	$8, %edx
	movq	%rbp, %rsi
	movq	%rdi, %rbx
	call	__libc_dynarray_emplace_enlarge@PLT
	testb	%al, %al
	je	.L99
	movq	(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movq	%r12, (%rdx,%rax,8)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %rbp
	je	.L96
	call	free@PLT
.L96:
	movq	%rbp, 16(%rbx)
	movq	$0, (%rbx)
	movq	$-1, 8(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	array_add__, .-array_add__
	.section	.rodata.str1.1
.LC1:
	.string	"/etc/hosts"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L106
	call	rewind@PLT
	movl	$1, %eax
.L100:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L106:
	leaq	.LC1(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L100
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.section	.rodata.str1.1
.LC2:
	.string	"nss_files/files-hosts.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"af == AF_INET || af == AF_INET6"
	.section	.rodata.str1.1
.LC4:
	.string	"tmp_result_buf.h_length == 4"
.LC5:
	.string	"tmp_result_buf.h_length == 16"
	.text
	.p2align 4,,15
	.type	gethostbyname3_multi, @function
gethostbyname3_multi:
	pushq	%r15
	movl	%edx, %eax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	andl	$-9, %eax
	pushq	%rbp
	pushq	%rbx
	subq	$1464, %rsp
	cmpl	$2, %eax
	movq	%rdi, 8(%rsp)
	movq	%rcx, 16(%rsp)
	jne	.L219
	leaq	416(%rsp), %rax
	movq	%rsi, %r12
	movq	$0, 96(%rsp)
	movq	$16, 104(%rsp)
	movq	$0, 256(%rsp)
	movl	%edx, %ebx
	movq	%rax, 40(%rsp)
	leaq	432(%rsp), %rax
	movq	$16, 264(%rsp)
	movq	$1024, 424(%rsp)
	movq	%rax, 416(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 24(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 112(%rsp)
	leaq	256(%rsp), %rax
	movq	%rax, 32(%rsp)
	leaq	280(%rsp), %rax
	movq	%rax, 272(%rsp)
	movq	16(%rsp), %rax
	movq	24(%rax), %rcx
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L109
	movl	$8, %ebp
	movl	$16, %eax
	movq	%r8, %r13
	movq	%r9, %r14
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L221:
	movq	104(%rsp), %rax
.L112:
	cmpq	$-1, %rax
	je	.L110
	movq	96(%rsp), %rdx
	cmpq	%rax, %rdx
	je	.L220
	leaq	1(%rdx), %rax
	movq	%rax, 96(%rsp)
	movq	112(%rsp), %rax
	movq	%rsi, (%rax,%rdx,8)
.L110:
	movq	(%rcx,%rbp), %rsi
	addq	$8, %rbp
	testq	%rsi, %rsi
	jne	.L221
	movq	%r13, %r8
	movq	%r14, %r9
.L109:
	movq	16(%rsp), %rax
	movq	8(%rax), %rcx
	movq	(%rcx), %rsi
	testq	%rsi, %rsi
	je	.L113
	movl	$8, %ebp
	movq	%r8, %r13
	movq	%r9, %r14
	.p2align 4,,10
	.p2align 3
.L116:
	movq	264(%rsp), %rax
	cmpq	$-1, %rax
	je	.L114
	movq	256(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L222
	leaq	1(%rdx), %rax
	movq	%rax, 256(%rsp)
	movq	272(%rsp), %rax
	movq	%rsi, (%rax,%rdx,8)
.L114:
	movq	(%rcx,%rbp), %rsi
	addq	$8, %rbp
	testq	%rsi, %rsi
	jne	.L116
	movq	%r13, %r8
	movq	%r14, %r9
.L113:
	leaq	(%r8,%r9), %rsi
	movq	%rcx, %rbp
	subq	%rcx, %rsi
	addq	%rsi, %rbp
	jc	.L223
.L119:
	leaq	64(%rsp), %rax
	movq	%rbp, 56(%rsp)
	movq	%rcx, 48(%rsp)
	xorl	%ebp, %ebp
	movq	%rax, (%rsp)
	.p2align 4,,10
	.p2align 3
.L120:
	subq	$8, %rsp
	pushq	%rbx
	movq	1544(%rsp), %r9
	movq	1536(%rsp), %r8
	movq	440(%rsp), %rcx
	movq	432(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdi
	call	internal_getent
	movl	%eax, %r15d
	cmpl	$-2, %r15d
	popq	%rax
	popq	%rdx
	je	.L224
	cmpl	$1, %r15d
	jne	.L124
	movq	64(%rsp), %rsi
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L125
	movq	88(%rsp), %rax
	xorl	%r15d, %r15d
	cmpq	$0, (%rax)
	jne	.L141
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L130:
	cmpl	$16, %edx
	jne	.L225
	movq	48(%rsp), %rdi
	leaq	3(%rdi), %rcx
	andq	$-4, %rcx
	cmpq	%rcx, %rdi
	leaq	16(%rcx), %rsi
	ja	.L132
	cmpq	$15, %rsi
	jbe	.L132
.L218:
	cmpq	56(%rsp), %rsi
	ja	.L132
	testq	%rcx, %rcx
	movq	%rsi, 48(%rsp)
	je	.L134
	movq	(%rax,%r15), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
	movq	104(%rsp), %rax
	cmpq	$-1, %rax
	je	.L139
	movq	96(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L226
	leaq	1(%rdx), %rax
	movq	%rax, 96(%rsp)
	movq	112(%rsp), %rax
	movq	%rcx, (%rax,%rdx,8)
.L139:
	movq	88(%rsp), %rax
	addq	$8, %r15
	cmpq	$0, (%rax,%r15)
	je	.L127
.L141:
	cmpl	$2, %ebx
	movslq	84(%rsp), %rdx
	jne	.L130
	cmpl	$4, %edx
	jne	.L227
	movq	48(%rsp), %rdi
	leaq	3(%rdi), %rcx
	andq	$-4, %rcx
	cmpq	%rcx, %rdi
	leaq	4(%rcx), %rsi
	ja	.L132
	cmpq	$3, %rsi
	ja	.L218
	.p2align 4,,10
	.p2align 3
.L132:
	movq	%rbp, 48(%rsp)
	movq	%rbp, 56(%rsp)
.L134:
	movq	1520(%rsp), %rax
	movl	$34, (%rax)
	movq	1528(%rsp), %rax
	movl	$-1, (%rax)
.L127:
	movq	72(%rsp), %rax
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.L138
.L137:
	movl	$8, %r13d
	movq	%r14, %rdx
	.p2align 4,,10
	.p2align 3
.L146:
	movq	48(%rsp), %r15
	movq	56(%rsp), %rsi
	movq	%r15, %rdi
	call	__libc_alloc_buffer_copy_string@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	movq	264(%rsp), %rax
	cmove	%rbp, %r15
	movq	%rdx, 56(%rsp)
	cmpq	$-1, %rax
	je	.L144
	movq	256(%rsp), %rdx
	cmpq	%rdx, %rax
	je	.L228
	leaq	1(%rdx), %rax
	movq	%rax, 256(%rsp)
	movq	272(%rsp), %rax
	movq	%r15, (%rax,%rdx,8)
.L144:
	movq	72(%rsp), %rax
	movq	(%rax,%r13), %rdx
	addq	$8, %r13
	testq	%rdx, %rdx
	jne	.L146
.L138:
	movq	16(%rsp), %rax
	movq	64(%rsp), %r15
	movq	(%rax), %rdi
	movq	%r15, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L229
.L147:
	cmpq	$-1, 104(%rsp)
	je	.L152
	cmpq	$-1, 264(%rsp)
	je	.L152
	cmpq	$0, 48(%rsp)
	jne	.L120
.L155:
	movq	1520(%rsp), %rax
	movl	$-2, %edx
	movl	$34, (%rax)
	movq	1528(%rsp), %rax
	movl	$-1, (%rax)
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L226:
	movq	24(%rsp), %rdi
	movq	%rcx, %rsi
	call	array_add__
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L125:
	movq	72(%rsp), %r15
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.L120
	movq	%r14, %rsi
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L230:
	addq	$8, %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L120
.L129:
	movq	%r12, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L230
	movq	88(%rsp), %rax
	xorl	%r15d, %r15d
	cmpq	$0, (%rax)
	jne	.L141
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L224:
	movq	1528(%rsp), %rax
	cmpl	$-1, (%rax)
	je	.L231
.L211:
	movl	%r15d, %edx
.L122:
	movq	40(%rsp), %rax
	movq	416(%rsp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L156
	movl	%edx, (%rsp)
	call	free@PLT
	movl	(%rsp), %edx
.L156:
	movq	24(%rsp), %rax
	movq	112(%rsp), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L157
	movl	%edx, (%rsp)
	call	free@PLT
	movl	(%rsp), %edx
.L157:
	movq	24(%rsp), %rax
	movq	272(%rsp), %rdi
	movq	$0, 96(%rsp)
	movq	$16, 104(%rsp)
	addq	$24, %rax
	movq	%rax, 112(%rsp)
	movq	32(%rsp), %rax
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L107
	movl	%edx, (%rsp)
	call	free@PLT
	movl	(%rsp), %edx
.L107:
	addq	$1464, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	movq	32(%rsp), %rdi
	movq	%r15, %rsi
	call	array_add__
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L229:
	movq	48(%rsp), %r14
	movq	56(%rsp), %rsi
	movq	%r15, %rdx
	movq	%r14, %rdi
	call	__libc_alloc_buffer_copy_string@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	movq	264(%rsp), %rax
	cmove	%rbp, %r14
	movq	%rdx, 56(%rsp)
	movq	256(%rsp), %r8
	cmpq	$-1, %rax
	je	.L149
	cmpq	%r8, %rax
	je	.L232
	leaq	1(%r8), %rax
	movq	%rax, 256(%rsp)
	movq	272(%rsp), %rax
	movq	%r14, (%rax,%r8,8)
	jmp	.L147
.L231:
	movq	1520(%rsp), %rax
	cmpl	$34, (%rax)
	jne	.L211
	movq	40(%rsp), %rdi
	call	__libc_scratch_buffer_grow@PLT
	testb	%al, %al
	jne	.L120
	movq	1520(%rsp), %rax
	movl	%r15d, %edx
	movl	$12, (%rax)
	jmp	.L122
.L152:
	movq	256(%rsp), %r8
.L149:
	movq	1520(%rsp), %rax
	movl	$12, (%rax)
	movq	1528(%rsp), %rax
	movl	$-1, (%rax)
	movq	(%rsp), %rax
	movq	%rax, 16(%rsp)
.L154:
	movq	96(%rsp), %r12
	leaq	48(%rsp), %rbp
	movl	$8, %edx
	movl	$8, %esi
	movq	%r8, (%rsp)
	movq	%rbp, %rdi
	leaq	1(%r12), %rcx
	call	__libc_alloc_buffer_alloc_array@PLT
	movq	(%rsp), %r8
	movq	%rax, %rbx
	movq	%rbp, %rdi
	movl	$8, %edx
	movl	$8, %esi
	leaq	1(%r8), %rcx
	call	__libc_alloc_buffer_alloc_array@PLT
	testq	%rbx, %rbx
	movq	%rax, %rbp
	je	.L155
	testq	%rax, %rax
	je	.L155
	movq	112(%rsp), %rsi
	leaq	0(,%r12,8), %rdx
	movq	%rbx, %rdi
	call	memcpy@PLT
	movq	(%rsp), %r8
	movq	$0, (%rbx,%r12,8)
	movq	%rbp, %rdi
	movq	272(%rsp), %rsi
	leaq	0(,%r8,8), %rdx
	call	memcpy@PLT
	movq	16(%rsp), %rax
	movq	(%rsp), %r8
	movl	$1, %edx
	movq	$0, 0(%rbp,%r8,8)
	movq	%rbx, 24(%rax)
	movq	%rbp, 8(%rax)
	jmp	.L122
.L222:
	movq	32(%rsp), %rdi
	call	array_add__
	movq	16(%rsp), %rax
	movq	8(%rax), %rcx
	jmp	.L114
.L220:
	movq	24(%rsp), %rdi
	call	array_add__
	movq	16(%rsp), %rax
	movq	24(%rax), %rcx
	jmp	.L110
.L232:
	movq	32(%rsp), %rdi
	movq	%r14, %rsi
	call	array_add__
	jmp	.L147
.L225:
	leaq	__PRETTY_FUNCTION__.12157(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$205, %edx
	call	__assert_fail@PLT
.L227:
	leaq	__PRETTY_FUNCTION__.12157(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$200, %edx
	call	__assert_fail@PLT
.L124:
	movq	256(%rsp), %r8
	jmp	.L154
.L223:
	movq	%rcx, %rdi
	movq	%rcx, (%rsp)
	call	__libc_alloc_buffer_create_failure@PLT
	movq	(%rsp), %rcx
	jmp	.L119
.L219:
	leaq	__PRETTY_FUNCTION__.12157(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$124, %edx
	call	__assert_fail@PLT
	.size	gethostbyname3_multi, .-gethostbyname3_multi
	.p2align 4,,15
	.globl	_nss_files_sethostent
	.type	_nss_files_sethostent, @function
_nss_files_sethostent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L234
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L234:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L233
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L233:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_sethostent, .-_nss_files_sethostent
	.p2align 4,,15
	.globl	_nss_files_endhostent
	.type	_nss_files_endhostent, @function
_nss_files_endhostent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L244
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L244:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L245
	call	fclose@PLT
	movq	$0, stream(%rip)
.L245:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L246
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L246:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endhostent, .-_nss_files_endhostent
	.p2align 4,,15
	.globl	_nss_files_gethostent_r
	.type	_nss_files_gethostent_r, @function
_nss_files_gethostent_r:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rcx, %r14
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L258
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L258:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L269
.L259:
	subq	$8, %rsp
	movq	%r12, %rdx
	movq	%r15, %r9
	pushq	$2
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rbp, %rsi
	call	internal_getent
	movl	%eax, %ebx
	popq	%rax
	popq	%rdx
.L260:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L257
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L257:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L269:
	movq	errno@gottpoff(%rip), %rax
	leaq	stream(%rip), %rdi
	movl	%fs:(%rax), %edx
	movl	%edx, 12(%rsp)
	call	internal_setent
	movl	12(%rsp), %edx
	movl	%eax, %ebx
	movq	errno@gottpoff(%rip), %rax
	cmpl	$1, %ebx
	movl	%edx, %fs:(%rax)
	jne	.L260
	movq	stream(%rip), %rdi
	jmp	.L259
	.size	_nss_files_gethostent_r, .-_nss_files_gethostent_r
	.p2align 4,,15
	.globl	_nss_files_gethostbyaddr_r
	.type	_nss_files_gethostbyaddr_r, @function
_nss_files_gethostbyaddr_r:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movl	%edx, %r12d
	movq	%rcx, %rbx
	subq	$40, %rsp
	movq	%rdi, (%rsp)
	leaq	24(%rsp), %rdi
	movq	$0, 24(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %r15d
	je	.L284
.L270:
	addq	$40, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	movl	%ebp, %eax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L280:
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%r14, %rcx
	pushq	%r12
	movq	120(%rsp), %r9
	movq	%rbx, %rsi
	movq	112(%rsp), %r8
	movq	40(%rsp), %rdi
	call	internal_getent
	movl	%eax, %r15d
	cmpl	$1, %r15d
	popq	%rax
	popq	%rdx
	jne	.L273
	cmpl	%ebp, 20(%rbx)
	jne	.L280
	movq	24(%rbx), %rax
	movq	8(%rsp), %rdx
	movq	(%rsp), %rdi
	movq	(%rax), %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L280
.L273:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L270
	call	fclose@PLT
	jmp	.L270
	.size	_nss_files_gethostbyaddr_r, .-_nss_files_gethostbyaddr_r
	.p2align 4,,15
	.globl	_nss_files_gethostbyname3_r
	.type	_nss_files_gethostbyname3_r, @function
_nss_files_gethostbyname3_r:
	pushq	%r15
	pushq	%r14
	movl	$0, %eax
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rcx, %rdx
	movq	%rdi, %rbx
	movl	%esi, %r14d
	subq	$56, %rsp
	negq	%rdx
	andl	$7, %edx
	leaq	40(%rsp), %rdi
	movq	%rcx, 24(%rsp)
	subq	%rdx, %r13
	cmpq	%r8, %rdx
	movq	%rdx, 16(%rsp)
	cmovnb	%rax, %r13
	movq	%r9, 8(%rsp)
	movq	$0, 40(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %r12d
	movq	16(%rsp), %rdx
	movq	24(%rsp), %rcx
	je	.L321
.L285:
	addq	$56, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L321:
	leaq	(%rcx,%rdx), %rax
	movq	%rax, 16(%rsp)
.L311:
	subq	$8, %rsp
	movq	%r13, %rcx
	movq	%rbp, %rsi
	pushq	%r14
	movq	128(%rsp), %r9
	movq	24(%rsp), %r8
	movq	32(%rsp), %rdx
	movq	56(%rsp), %rdi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %r12d
	popq	%rcx
	popq	%rsi
	jne	.L322
	movq	0(%rbp), %rsi
	movq	%rbx, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L289
	movq	8(%rbp), %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L290
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L323:
	addq	$8, %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L311
.L290:
	movq	%rbx, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L323
.L289:
	movq	_res_hconf@GOTPCREL(%rip), %rax
	movq	40(%rsp), %rdi
	movl	$1, %edx
	testb	$16, 64(%rax)
	je	.L293
	pushq	112(%rsp)
	pushq	16(%rsp)
	movl	%r14d, %edx
	movq	32(%rsp), %r8
	movq	%r13, %r9
	movq	%rbp, %rcx
	movq	%rbx, %rsi
	call	gethostbyname3_multi
	movl	%eax, %r12d
	movq	56(%rsp), %rdi
	cmpl	$1, %r12d
	popq	%rax
	popq	%rdx
	sete	%dl
.L293:
	testq	%rdi, %rdi
	je	.L294
.L295:
	movb	%dl, 8(%rsp)
	call	fclose@PLT
	movzbl	8(%rsp), %edx
.L294:
	cmpq	$0, 128(%rsp)
	je	.L285
	testb	%dl, %dl
	je	.L285
	movq	0(%rbp), %rax
	movq	128(%rsp), %rcx
	movq	%rax, (%rcx)
	jmp	.L285
.L322:
	movq	40(%rsp), %rdi
	xorl	%edx, %edx
	testq	%rdi, %rdi
	jne	.L295
	jmp	.L285
	.size	_nss_files_gethostbyname3_r, .-_nss_files_gethostbyname3_r
	.p2align 4,,15
	.globl	_nss_files_gethostbyname_r
	.type	_nss_files_gethostbyname_r, @function
_nss_files_gethostbyname_r:
	subq	$16, %rsp
	pushq	$0
	pushq	$0
	pushq	%r9
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movl	$2, %esi
	call	_nss_files_gethostbyname3_r@PLT
	addq	$40, %rsp
	ret
	.size	_nss_files_gethostbyname_r, .-_nss_files_gethostbyname_r
	.p2align 4,,15
	.globl	_nss_files_gethostbyname2_r
	.type	_nss_files_gethostbyname2_r, @function
_nss_files_gethostbyname2_r:
	subq	$16, %rsp
	pushq	$0
	pushq	$0
	pushq	40(%rsp)
	call	_nss_files_gethostbyname3_r@PLT
	addq	$40, %rsp
	ret
	.size	_nss_files_gethostbyname2_r, .-_nss_files_gethostbyname2_r
	.section	.rodata.str1.1
.LC6:
	.string	"buflen >= bufferend - buffer"
.LC7:
	.string	"result.h_addr_list[1] == NULL"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"(_res_hconf.flags & HCONF_FLAG_MULTI) != 0"
	.text
	.p2align 4,,15
	.globl	_nss_files_gethostbyname4_r
	.type	_nss_files_gethostbyname4_r, @function
_nss_files_gethostbyname4_r:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rcx, %rbx
	subq	$104, %rsp
	leaq	56(%rsp), %rdi
	movq	%r8, 16(%rsp)
	movq	%r9, 24(%rsp)
	movq	$0, 56(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, 44(%rsp)
	je	.L377
	movq	errno@gottpoff(%rip), %rax
	cmpl	$-2, 44(%rsp)
	movq	16(%rsp), %rdx
	movl	%fs:(%rax), %eax
	movl	%eax, (%rdx)
	movq	24(%rsp), %rax
	je	.L378
	movl	$4, (%rax)
.L328:
	movl	44(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L377:
	leaq	64(%rsp), %rax
	movb	$0, 43(%rsp)
	movq	%rax, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L329:
	movq	%r15, %rax
	movq	%rbx, %rdx
	negq	%rax
	andl	$7, %eax
	leaq	(%r15,%rax), %rdi
	subq	%rax, %rdx
	cmpq	%rax, %rbx
	movl	$0, %ebx
	movq	%rdi, 8(%rsp)
	cmova	%rdx, %rbx
	subq	$8, %rsp
	pushq	$0
	movq	%rdi, %rdx
	movq	40(%rsp), %r9
	movq	32(%rsp), %r8
	movq	48(%rsp), %rsi
	movq	%rbx, %rcx
	movq	72(%rsp), %rdi
	call	internal_getent
	cmpl	$1, %eax
	popq	%rdx
	popq	%rcx
	jne	.L332
	movq	64(%rsp), %r12
	movq	%r13, %rdi
	movq	%r12, %rsi
	call	__strcasecmp@PLT
	movq	72(%rsp), %r14
	testl	%eax, %eax
	movl	%eax, %ecx
	movq	(%r14), %rsi
	je	.L352
	testq	%rsi, %rsi
	je	.L334
	movl	$1, %r15d
	jmp	.L335
	.p2align 4,,10
	.p2align 3
.L379:
	addq	$1, %r15
	testq	%rsi, %rsi
	je	.L334
.L335:
	movq	%r13, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	movl	%r15d, %ecx
	movq	(%r14,%r15,8), %rsi
	jne	.L379
.L333:
	addq	$1, %r15
	testq	%rsi, %rsi
	je	.L349
	movslq	%ecx, %r15
	addq	$1, %r15
	.p2align 4,,10
	.p2align 3
.L336:
	addq	$1, %r15
	cmpq	$0, -8(%r14,%r15,8)
	jne	.L336
.L349:
	leaq	(%r14,%r15,8), %r15
	movq	%r15, %rax
	subq	8(%rsp), %rax
	cmpq	%rbx, %rax
	ja	.L380
	movq	88(%rsp), %rcx
	subq	%rax, %rbx
	cmpq	$0, 8(%rcx)
	jne	.L381
	movq	0(%rbp), %rax
	testq	%rax, %rax
	je	.L382
.L339:
	cmpb	$0, 43(%rsp)
	movq	$0, (%rax)
	movl	$0, %eax
	movq	0(%rbp), %rdi
	movslq	84(%rsp), %rdx
	cmovne	%rax, %r12
	movl	80(%rsp), %eax
	addq	$20, %rdi
	movq	%r12, -12(%rdi)
	movq	(%rcx), %rsi
	movl	%eax, -4(%rdi)
	call	memcpy@PLT
	movq	_res_hconf@GOTPCREL(%rip), %rax
	movq	0(%rbp), %rbp
	testb	$16, 64(%rax)
	movl	$0, 36(%rbp)
	je	.L354
	movb	$1, 43(%rsp)
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L352:
	xorl	%r15d, %r15d
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L334:
	movq	8(%rsp), %r15
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%r15, %rdx
	negq	%rdx
	andl	$7, %edx
	cmpq	%rdx, %rbx
	leaq	(%r15,%rdx), %rax
	ja	.L383
.L340:
	movq	16(%rsp), %rax
	movl	$-2, 44(%rsp)
	movl	$34, (%rax)
	movq	24(%rsp), %rax
	movl	$-1, (%rax)
.L345:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L328
.L384:
	call	fclose@PLT
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L383:
	subq	%rdx, %rbx
	cmpq	$39, %rbx
	jbe	.L340
	movq	%rax, 0(%rbp)
	leaq	40(%rax), %r15
	subq	$40, %rbx
	jmp	.L339
.L332:
	testl	%eax, %eax
	jne	.L355
	cmpb	$0, 43(%rsp)
	je	.L355
	movq	_res_hconf@GOTPCREL(%rip), %rax
	testb	$16, 64(%rax)
	jne	.L345
	leaq	__PRETTY_FUNCTION__.12261(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$474, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L354:
	movq	56(%rsp), %rdi
	movl	$1, 44(%rsp)
	testq	%rdi, %rdi
	jne	.L384
	jmp	.L328
.L378:
	movl	$2, (%rax)
	jmp	.L328
.L380:
	leaq	__PRETTY_FUNCTION__.12261(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$425, %edx
	call	__assert_fail@PLT
.L381:
	leaq	__PRETTY_FUNCTION__.12261(%rip), %rcx
	leaq	.LC2(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$434, %edx
	call	__assert_fail@PLT
.L355:
	movl	%eax, 44(%rsp)
	jmp	.L345
	.size	_nss_files_gethostbyname4_r, .-_nss_files_gethostbyname4_r
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12261, @object
	.size	__PRETTY_FUNCTION__.12261, 28
__PRETTY_FUNCTION__.12261:
	.string	"_nss_files_gethostbyname4_r"
	.align 16
	.type	__PRETTY_FUNCTION__.12157, @object
	.size	__PRETTY_FUNCTION__.12157, 21
__PRETTY_FUNCTION__.12157:
	.string	"gethostbyname3_multi"
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
