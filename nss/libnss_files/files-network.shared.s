	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/networks"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	rewind@PLT
	movl	$1, %eax
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.section	.rodata.str1.1
.LC1:
	.string	"#\n"
	.text
	.p2align 4,,15
	.globl	__GI__nss_files_parse_netent
	.hidden	__GI__nss_files_parse_netent
	.type	__GI__nss_files_parse_netent, @function
__GI__nss_files_parse_netent:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	leaq	(%rdx,%rcx), %rbx
	movq	%rdx, %r14
	subq	$40, %rsp
	cmpq	%rdi, %rbx
	movq	%rsi, -56(%rbp)
	movq	%r8, -72(%rbp)
	jbe	.L41
	cmpq	%rdi, %rdx
	jbe	.L78
.L41:
	movq	%r14, %r13
.L10:
	leaq	.LC1(%rip), %rsi
	movq	%r15, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L11
	movb	$0, (%rax)
.L11:
	movq	-56(%rbp), %rax
	movq	%r15, (%rax)
	movzbl	(%r15), %edx
	testb	%dl, %dl
	movb	%dl, -64(%rbp)
	je	.L42
	call	__ctype_b_loc@PLT
	movzbl	-64(%rbp), %edx
	movq	(%rax), %rsi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	testb	%dl, %dl
	movq	%r12, %r15
	je	.L12
.L13:
	movsbq	%dl, %rcx
	leaq	1(%r15), %r12
	movzbl	1(%r15), %edx
	testb	$32, 1(%rsi,%rcx,2)
	je	.L14
	movb	$0, (%r15)
	movq	%r15, %r12
	movq	(%rax), %rdi
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L16:
	movzbl	1(%r12), %edx
.L37:
	movsbq	%dl, %rcx
	addq	$1, %r12
	movzwl	(%rdi,%rcx,2), %r9d
	testw	$8192, %r9w
	jne	.L16
	testb	%dl, %dl
	je	.L12
	movq	%r12, %rsi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	testb	%cl, %cl
	je	.L18
	movzwl	(%rdi,%rcx,2), %r9d
	movq	%r15, %rsi
.L17:
	testw	$8192, %r9w
	leaq	1(%rsi), %r15
	movsbq	1(%rsi), %rcx
	je	.L19
	movb	$0, (%rsi)
	movq	%rsi, %r15
	movq	(%rax), %rax
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L21:
	movsbq	1(%r15), %rcx
.L38:
	addq	$1, %r15
	testb	$32, 1(%rax,%rcx,2)
	jne	.L21
	jmp	.L18
.L42:
	movq	%r15, %r12
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%r12, %r15
.L18:
	movl	$46, %esi
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L44
	leaq	1(%rax), %rdi
	movl	$46, %esi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L45
	leaq	1(%rax), %rdi
	movl	$46, %esi
	call	strchr@PLT
	testq	%rax, %rax
	je	.L79
.L23:
	movq	%r12, %rdi
	call	inet_network@PLT
	movq	-56(%rbp), %rdi
	testq	%r13, %r13
	movl	%eax, 20(%rdi)
	movl	$2, 16(%rdi)
	je	.L80
.L25:
	leaq	7(%r13), %r12
	andq	$-8, %r12
	leaq	16(%r12), %r14
	movq	%r12, -64(%rbp)
.L26:
	cmpq	%r14, %rbx
	jb	.L81
.L27:
	movzbl	(%r15), %r13d
	testb	%r13b, %r13b
	je	.L29
	call	__ctype_b_loc@PLT
	movq	(%rax), %rsi
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$1, %r15
	movzbl	(%r15), %r13d
.L30:
	movsbq	%r13b, %rax
	testb	$32, 1(%rsi,%rax,2)
	jne	.L31
	testb	%r13b, %r13b
	je	.L26
	movq	%r15, %rcx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L76:
	testb	$32, 1(%rsi,%rax,2)
	jne	.L34
	movq	%r8, %rcx
.L33:
	movsbq	1(%rcx), %rax
	leaq	1(%rcx), %r8
	testb	%al, %al
	jne	.L76
	cmpq	%r8, %r15
	jb	.L39
	cmpq	%r14, %rbx
	movq	%r8, %r15
	jnb	.L27
.L81:
	movq	-72(%rbp), %rax
	movl	$34, (%rax)
	movl	$-1, %eax
.L9:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	cmpq	%r8, %r15
	jnb	.L82
.L39:
	movq	%r15, (%r12)
	cmpb	$0, 1(%rcx)
	leaq	8(%r12), %rax
	je	.L47
.L40:
	leaq	2(%rcx), %r15
	movb	$0, (%r8)
	movq	%rax, %r12
	leaq	16(%rax), %r14
	jmp	.L26
.L78:
	call	strlen@PLT
	leaq	1(%r15,%rax), %r13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L29:
	movq	-64(%rbp), %rax
	movq	$0, (%r12)
	testq	%rax, %rax
	je	.L48
	movq	-56(%rbp), %rbx
	movq	%rax, 8(%rbx)
	leaq	-40(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L80:
	cmpq	%r15, %r14
	ja	.L46
	cmpq	%r15, %rbx
	jbe	.L46
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%r15,%rax), %r13
	jmp	.L25
.L46:
	movq	%r14, %r13
	jmp	.L25
.L45:
	movl	$4, %ecx
	movl	$2, -64(%rbp)
.L22:
	movq	%r12, %rdi
	movq	%rcx, -80(%rbp)
	call	strlen@PLT
	movq	-80(%rbp), %rcx
	movq	%r12, %rsi
	leaq	31(%rcx,%rax), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
	movq	%r9, %rdi
	movq	%r9, -80(%rbp)
	call	__stpcpy@PLT
	movl	$3, %esi
	subl	-64(%rbp), %esi
	movq	-80(%rbp), %r9
	movq	%rax, %rcx
	addq	%rsi, %rsi
	leaq	2(%rsi,%rax), %rdi
.L24:
	movb	$46, (%rcx)
	movb	$48, 1(%rcx)
	addq	$2, %rcx
	cmpq	%rcx, %rdi
	jne	.L24
	movb	$0, 2(%rsi,%rax)
	movq	%r9, %r12
	jmp	.L23
.L79:
	movl	$2, %ecx
	movl	$3, -64(%rbp)
	jmp	.L22
.L44:
	movl	$6, %ecx
	movl	$1, -64(%rbp)
	jmp	.L22
.L48:
	movl	$-1, %eax
	jmp	.L9
.L47:
	movq	%rax, %r12
	movq	%r8, %r15
	leaq	16(%rax), %r14
	jmp	.L26
.L82:
	movq	%r12, %rax
	jmp	.L40
	.size	__GI__nss_files_parse_netent, .-__GI__nss_files_parse_netent
	.globl	_nss_files_parse_netent
	.set	_nss_files_parse_netent,__GI__nss_files_parse_netent
	.p2align 4,,15
	.type	internal_getent, @function
internal_getent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movq	errno@gottpoff(%rip), %rax
	cmpq	$1, %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, 28(%rsp)
	jbe	.L92
	leaq	40(%rsp), %r15
	movq	%rcx, %r14
	movq	%r9, %r13
	movq	%r8, %r12
	movq	%rcx, 16(%rsp)
	movq	%rdx, %rbx
	movq	%rsi, 8(%rsp)
	movq	%rdi, %rbp
.L84:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__nss_readline@PLT
	cmpl	$2, %eax
	je	.L94
	testl	%eax, %eax
	jne	.L88
	movq	16(%rsp), %rcx
	movq	8(%rsp), %rsi
	movq	%r12, %r8
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	__GI__nss_files_parse_netent
	movq	40(%rsp), %rsi
	movl	%eax, %edx
	movq	%rbp, %rdi
	call	__nss_parse_line_result@PLT
	testl	%eax, %eax
	je	.L95
	cmpl	$22, %eax
	je	.L84
.L88:
	cmpl	$34, %eax
	movl	%eax, (%r12)
	movl	$-1, 0(%r13)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L83:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	errno@gottpoff(%rip), %rax
	movl	28(%rsp), %ebx
	movl	$1, 0(%r13)
	movl	%ebx, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L95:
	movq	errno@gottpoff(%rip), %rax
	movl	28(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	movl	$1, %eax
	jmp	.L83
.L92:
	movl	$34, (%r8)
	movl	$-2, %eax
	movl	$-1, (%r9)
	jmp	.L83
	.size	internal_getent, .-internal_getent
	.p2align 4,,15
	.globl	_nss_files_setnetent
	.type	_nss_files_setnetent, @function
_nss_files_setnetent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L97
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L97:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L96
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L96:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_setnetent, .-_nss_files_setnetent
	.p2align 4,,15
	.globl	_nss_files_endnetent
	.type	_nss_files_endnetent, @function
_nss_files_endnetent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L107
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L107:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L108
	call	fclose@PLT
	movq	$0, stream(%rip)
.L108:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L109
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L109:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endnetent, .-_nss_files_endnetent
	.p2align 4,,15
	.globl	_nss_files_getnetent_r
	.type	_nss_files_getnetent_r, @function
_nss_files_getnetent_r:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rcx, %r14
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L121
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L121:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L132
.L122:
	movq	%r15, %r9
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	movl	%eax, %ebx
.L123:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L120
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L120:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	movq	errno@gottpoff(%rip), %rax
	leaq	stream(%rip), %rdi
	movl	%fs:(%rax), %edx
	movl	%edx, 12(%rsp)
	call	internal_setent
	movl	12(%rsp), %edx
	movl	%eax, %ebx
	movq	errno@gottpoff(%rip), %rax
	cmpl	$1, %ebx
	movl	%edx, %fs:(%rax)
	jne	.L123
	movq	stream(%rip), %rdi
	jmp	.L122
	.size	_nss_files_getnetent_r, .-_nss_files_getnetent_r
	.p2align 4,,15
	.globl	_nss_files_getnetbyname_r
	.type	_nss_files_getnetbyname_r, @function
_nss_files_getnetbyname_r:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r15
	pushq	%r13
	pushq	%r12
	movq	%r8, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$40, %rsp
	leaq	24(%rsp), %rdi
	movq	%rdx, (%rsp)
	movq	%rcx, 8(%rsp)
	movq	$0, 24(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %r12d
	je	.L148
.L133:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L148:
	movq	8(%rsp), %rcx
	movq	(%rsp), %rdx
	movq	%r15, %r9
	movq	24(%rsp), %rdi
	movq	%r14, %r8
	movq	%rbp, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %r12d
	jne	.L136
	movq	0(%rbp), %rsi
	movq	%rbx, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L136
	movq	8(%rbp), %r13
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	jne	.L137
	jmp	.L148
	.p2align 4,,10
	.p2align 3
.L155:
	addq	$8, %r13
	movq	0(%r13), %rsi
	testq	%rsi, %rsi
	je	.L148
.L137:
	movq	%rbx, %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L155
.L136:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L133
	call	fclose@PLT
	jmp	.L133
	.size	_nss_files_getnetbyname_r, .-_nss_files_getnetbyname_r
	.p2align 4,,15
	.globl	_nss_files_getnetbyaddr_r
	.type	_nss_files_getnetbyaddr_r, @function
_nss_files_getnetbyaddr_r:
	pushq	%r15
	pushq	%r14
	movl	%edi, %r15d
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdx, %rbx
	movq	%rcx, %r12
	movq	%r9, %r14
	subq	$40, %rsp
	leaq	24(%rsp), %rdi
	movq	$0, 24(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %edx
	je	.L167
.L156:
	addq	$40, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	cmpl	%r15d, 20(%rbx)
	je	.L160
.L167:
	movq	96(%rsp), %r9
	movq	24(%rsp), %rdi
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%rbx, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %edx
	jne	.L160
	testl	%ebp, %ebp
	je	.L159
	cmpl	%ebp, 16(%rbx)
	jne	.L167
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L160:
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L156
	movl	%edx, 12(%rsp)
	call	fclose@PLT
	movl	12(%rsp), %edx
	jmp	.L156
	.size	_nss_files_getnetbyaddr_r, .-_nss_files_getnetbyaddr_r
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
