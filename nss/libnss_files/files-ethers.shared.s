	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/ethers"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L8
	call	rewind@PLT
	movl	$1, %eax
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leaq	.LC0(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L1
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.section	.rodata.str1.1
.LC1:
	.string	"#\n"
	.text
	.p2align 4,,15
	.globl	__GI__nss_files_parse_etherent
	.hidden	__GI__nss_files_parse_etherent
	.type	__GI__nss_files_parse_etherent, @function
__GI__nss_files_parse_etherent:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	.LC1(%rip), %rsi
	subq	$24, %rsp
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L10
	movb	$0, (%rax)
.L10:
	leaq	8(%rsp), %r14
	xorl	%ebx, %ebx
	movl	$4294967295, %r12d
.L22:
	movl	$16, %edx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	strtoul@PLT
	movq	8(%rsp), %rbp
	cmpq	%r12, %rax
	movq	%r12, %rcx
	cmovbe	%rax, %rcx
	cmpq	%r15, %rbp
	je	.L16
	movzbl	0(%rbp), %edx
	cmpb	$58, %dl
	je	.L43
	testb	%dl, %dl
	jne	.L16
.L13:
	cmpq	$255, %rax
	ja	.L16
	movb	%cl, 8(%r13,%rbx)
	addq	$1, %rbx
	movq	%rbp, %r15
	cmpq	$5, %rbx
	jne	.L22
	movq	%r14, %rsi
	movl	$16, %edx
	movq	%rbp, %rdi
	call	strtoul@PLT
	movq	8(%rsp), %rbx
	movq	%rax, %r14
	movl	$4294967295, %eax
	cmpq	%rax, %r14
	cmovbe	%r14, %rax
	cmpq	%rbp, %rbx
	movq	%rax, %r12
	je	.L16
	call	__ctype_b_loc@PLT
	movsbq	(%rbx), %rcx
	movq	(%rax), %rdi
	testb	$32, 1(%rdi,%rcx,2)
	jne	.L44
	testb	%cl, %cl
	jne	.L16
.L19:
	cmpq	$255, %r14
	ja	.L16
	movb	%r12b, 13(%r13)
	movq	%rbx, 0(%r13)
	movzbl	(%rbx), %edx
	testb	%dl, %dl
	jne	.L29
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L25:
	testb	%dl, %dl
	movq	%rcx, %rbx
	je	.L24
.L29:
	movsbq	%dl, %rsi
	leaq	1(%rbx), %rcx
	movsbq	1(%rbx), %rdx
	testb	$32, 1(%rdi,%rsi,2)
	je	.L25
	movb	$0, (%rbx)
	movq	(%rax), %rax
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L27:
	movsbq	1(%rcx), %rdx
	addq	$1, %rcx
.L28:
	testb	$32, 1(%rax,%rdx,2)
	jne	.L27
.L24:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	addq	$1, %rbp
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	1(%rbx), %rdx
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rdx, %rbx
	movq	%rdx, 8(%rsp)
	addq	$1, %rdx
	movsbq	(%rbx), %rcx
	testb	$32, 1(%rdi,%rcx,2)
	jne	.L18
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI__nss_files_parse_etherent, .-__GI__nss_files_parse_etherent
	.globl	_nss_files_parse_etherent
	.set	_nss_files_parse_etherent,__GI__nss_files_parse_etherent
	.p2align 4,,15
	.type	internal_getent, @function
internal_getent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	errno@gottpoff(%rip), %rax
	cmpq	$1, %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, 12(%rsp)
	jbe	.L54
	leaq	24(%rsp), %r15
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, (%rsp)
	movq	%rdi, %r12
.L46:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__nss_readline@PLT
	cmpl	$2, %eax
	je	.L56
	testl	%eax, %eax
	jne	.L50
	movq	(%rsp), %rsi
	movq	%r13, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	__GI__nss_files_parse_etherent
	movq	24(%rsp), %rsi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	__nss_parse_line_result@PLT
	testl	%eax, %eax
	je	.L57
	cmpl	$22, %eax
	je	.L46
.L50:
	cmpl	$34, %eax
	movl	%eax, 0(%r13)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L45:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L57:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	movl	$1, %eax
	jmp	.L45
.L54:
	movl	$34, (%r8)
	movl	$-2, %eax
	jmp	.L45
	.size	internal_getent, .-internal_getent
	.p2align 4,,15
	.globl	_nss_files_setetherent
	.type	_nss_files_setetherent, @function
_nss_files_setetherent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L59
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L59:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L58
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L58:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_setetherent, .-_nss_files_setetherent
	.p2align 4,,15
	.globl	_nss_files_endetherent
	.type	_nss_files_endetherent, @function
_nss_files_endetherent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L69
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L69:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L70
	call	fclose@PLT
	movq	$0, stream(%rip)
.L70:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L71
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L71:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endetherent, .-_nss_files_endetherent
	.p2align 4,,15
	.globl	_nss_files_getetherent_r
	.type	_nss_files_getetherent_r, @function
_nss_files_getetherent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L83
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L83:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L94
.L84:
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	movl	%eax, %ebx
.L85:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L82
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L82:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	movq	errno@gottpoff(%rip), %rax
	leaq	stream(%rip), %rdi
	movl	%fs:(%rax), %r15d
	call	internal_setent
	movl	%eax, %ebx
	movq	errno@gottpoff(%rip), %rax
	cmpl	$1, %ebx
	movl	%r15d, %fs:(%rax)
	jne	.L85
	movq	stream(%rip), %rdi
	jmp	.L84
	.size	_nss_files_getetherent_r, .-_nss_files_getetherent_r
	.p2align 4,,15
	.globl	_nss_files_gethostton_r
	.type	_nss_files_gethostton_r, @function
_nss_files_gethostton_r:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movq	%r8, %r14
	subq	$24, %rsp
	leaq	8(%rsp), %rdi
	movq	$0, 8(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L96
.L95:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	movq	0(%rbp), %rdi
	movq	%r15, %rsi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	je	.L98
.L96:
	movq	8(%rsp), %rdi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L99
.L98:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L95
	call	fclose@PLT
	jmp	.L95
	.size	_nss_files_gethostton_r, .-_nss_files_gethostton_r
	.p2align 4,,15
	.globl	_nss_files_getntohost_r
	.type	_nss_files_getntohost_r, @function
_nss_files_getntohost_r:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rcx, %r14
	subq	$24, %rsp
	leaq	8(%rsp), %rdi
	movq	$0, 8(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %ebp
	je	.L110
.L109:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L124:
	movzwl	4(%r12), %ecx
	cmpw	%cx, 12(%rbx)
	je	.L114
.L110:
	movq	8(%rsp), %rdi
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %ebp
	jne	.L114
	movl	(%r12), %edx
	cmpl	%edx, 8(%rbx)
	jne	.L110
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L109
	call	fclose@PLT
	jmp	.L109
	.size	_nss_files_getntohost_r, .-_nss_files_getntohost_r
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
