	.text
	.p2align 4,,15
	.type	internal_getent, @function
internal_getent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	errno@gottpoff(%rip), %rax
	cmpq	$1, %rcx
	movl	%fs:(%rax), %eax
	movl	%eax, 12(%rsp)
	jbe	.L10
	leaq	24(%rsp), %r15
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, (%rsp)
	movq	%rdi, %r12
.L2:
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__nss_readline@PLT
	cmpl	$2, %eax
	je	.L13
	testl	%eax, %eax
	jne	.L6
	movq	(%rsp), %rsi
	movq	%r13, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%rbx, %rdi
	call	_nss_files_parse_spent@PLT
	movq	24(%rsp), %rsi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	__nss_parse_line_result@PLT
	testl	%eax, %eax
	je	.L14
	cmpl	$22, %eax
	je	.L2
.L6:
	cmpl	$34, %eax
	movl	%eax, 0(%r13)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movq	errno@gottpoff(%rip), %rax
	movl	12(%rsp), %ebx
	movl	%ebx, %fs:(%rax)
	movl	$1, %eax
	jmp	.L1
.L10:
	movl	$34, (%r8)
	movl	$-2, %eax
	jmp	.L1
	.size	internal_getent, .-internal_getent
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/etc/shadow"
	.text
	.p2align 4,,15
	.type	internal_setent, @function
internal_setent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L21
	call	rewind@PLT
	movl	$1, %eax
.L15:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC0(%rip), %rdi
	call	__nss_files_fopen@PLT
	movq	%rax, %rdx
	movq	%rax, (%rbx)
	movl	$1, %eax
	testq	%rdx, %rdx
	jne	.L15
	movq	errno@gottpoff(%rip), %rax
	popq	%rbx
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	ret
	.size	internal_setent, .-internal_setent
	.p2align 4,,15
	.globl	_nss_files_setspent
	.type	_nss_files_setspent, @function
_nss_files_setspent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L23
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L23:
	leaq	stream(%rip), %rdi
	call	internal_setent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L22
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L22:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_files_setspent, .-_nss_files_setspent
	.p2align 4,,15
	.globl	_nss_files_endspent
	.type	_nss_files_endspent, @function
_nss_files_endspent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L33
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L33:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L34
	call	fclose@PLT
	movq	$0, stream(%rip)
.L34:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L35
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L35:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_files_endspent, .-_nss_files_endspent
	.p2align 4,,15
	.globl	_nss_files_getspent_r
	.type	_nss_files_getspent_r, @function
_nss_files_getspent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %r12
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L47
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L47:
	movq	stream(%rip), %rdi
	testq	%rdi, %rdi
	je	.L58
.L48:
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	internal_getent
	movl	%eax, %ebx
.L49:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L46
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L46:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movq	errno@gottpoff(%rip), %rax
	leaq	stream(%rip), %rdi
	movl	%fs:(%rax), %r15d
	call	internal_setent
	movl	%eax, %ebx
	movq	errno@gottpoff(%rip), %rax
	cmpl	$1, %ebx
	movl	%r15d, %fs:(%rax)
	jne	.L49
	movq	stream(%rip), %rdi
	jmp	.L48
	.size	_nss_files_getspent_r, .-_nss_files_getspent_r
	.p2align 4,,15
	.globl	_nss_files_getspnam_r
	.type	_nss_files_getspnam_r, @function
_nss_files_getspnam_r:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	movq	%rdx, %r12
	subq	$24, %rsp
	leaq	8(%rsp), %rdi
	movq	$0, 8(%rsp)
	call	internal_setent
	cmpl	$1, %eax
	movl	%eax, %r15d
	je	.L70
.L59:
	addq	$24, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movq	8(%rsp), %rdi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	call	internal_getent
	cmpl	$1, %eax
	movl	%eax, %r15d
	jne	.L62
	movzbl	0(%rbp), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L70
	movq	(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L70
.L62:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L59
	call	fclose@PLT
	jmp	.L59
	.size	_nss_files_getspnam_r, .-_nss_files_getspnam_r
	.local	stream
	.comm	stream,8,8
	.local	lock
	.comm	lock,40,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
