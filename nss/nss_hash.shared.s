	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___nss_hash
	.hidden	__GI___nss_hash
	.type	__GI___nss_hash, @function
__GI___nss_hash:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.L1
	leaq	.L5(%rip), %rcx
	leaq	7(%rsi), %rdx
	andl	$7, %esi
	movslq	(%rcx,%rsi,4), %rax
	shrq	$3, %rdx
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L5:
	.long	.L14-.L5
	.long	.L15-.L5
	.long	.L16-.L5
	.long	.L17-.L5
	.long	.L18-.L5
	.long	.L19-.L5
	.long	.L20-.L5
	.long	.L13-.L5
	.text
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L20:
	movzbl	(%rdi), %eax
	xorl	%ecx, %ecx
	leaq	1(%rdi), %rsi
	addl	%ecx, %eax
	imull	$65599, %eax, %ecx
.L9:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdi
	addl	%ecx, %eax
	imull	$65599, %eax, %eax
.L8:
	movzbl	(%rdi), %ecx
	leaq	1(%rdi), %r8
	addl	%eax, %ecx
	imull	$65599, %ecx, %eax
.L7:
	movzbl	(%r8), %ecx
	leaq	1(%r8), %rsi
	addl	%eax, %ecx
	imull	$65599, %ecx, %ecx
.L6:
	movzbl	(%rsi), %eax
	leaq	1(%rsi), %rdi
	addl	%ecx, %eax
	imull	$65599, %eax, %ecx
.L4:
	movzbl	(%rdi), %eax
	addl	%ecx, %eax
	subq	$1, %rdx
	je	.L1
	imull	$65599, %eax, %eax
	addq	$1, %rdi
.L3:
	movzbl	(%rdi), %ecx
	addq	$1, %rdi
	addl	%ecx, %eax
	imull	$65599, %eax, %eax
.L11:
	movzbl	(%rdi), %ecx
	addq	$1, %rdi
	leaq	1(%rdi), %rsi
	addl	%eax, %ecx
	movzbl	(%rdi), %eax
	imull	$65599, %ecx, %ecx
	addl	%ecx, %eax
	imull	$65599, %eax, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%ecx, %ecx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rdi, %r8
	xorl	%eax, %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	jmp	.L11
	.size	__GI___nss_hash, .-__GI___nss_hash
	.globl	__nss_hash
	.set	__nss_hash,__GI___nss_hash
