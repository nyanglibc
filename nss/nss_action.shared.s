	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__nss_action_allocate
	.hidden	__nss_action_allocate
	.type	__nss_action_allocate, @function
__nss_action_allocate:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
#APP
# 84 "nss_action.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, nss_actions_lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	nss_actions(%rip), %rbp
	testq	%rbp, %rbp
	je	.L4
	movq	%rbp, %rax
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L4
.L9:
	cmpq	8(%rax), %rsi
	jne	.L5
	testq	%rsi, %rsi
	leaq	16(%rax), %r8
	je	.L7
	movq	16(%rax), %rdx
	cmpq	%rdx, (%rbx)
	jne	.L5
	movl	8(%rbx), %edi
	cmpl	%edi, 24(%rax)
	jne	.L5
	movl	$16, %ecx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %rdx
	cmpq	%rdx, %rsi
	je	.L7
	movq	(%rbx,%rcx), %rdi
	cmpq	%rdi, (%r8,%rcx)
	jne	.L5
	movl	8(%r8,%rcx), %edi
	addq	$16, %rcx
	cmpl	-8(%rbx,%rcx), %edi
	je	.L8
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L9
.L4:
	leaq	1(%rsi), %r12
	movq	%rsi, 8(%rsp)
	salq	$4, %r12
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L14
	movq	8(%rsp), %rsi
	leaq	16(%rax), %r8
	leaq	-16(%r12), %rdx
	movq	%rbp, (%rax)
	movq	%r8, %rdi
	movq	%rsi, 8(%rax)
	movq	%rbx, %rsi
	call	__GI_memcpy@PLT
	movq	%r13, nss_actions(%rip)
	movq	%rax, %r8
	.p2align 4,,10
	.p2align 3
.L7:
#APP
# 101 "nss_action.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	subl	$1, nss_actions_lock(%rip)
.L1:
	addq	$24, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L10:
	xorl	%eax, %eax
#APP
# 101 "nss_action.c" 1
	xchgl %eax, nss_actions_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	nss_actions_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 101 "nss_action.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, nss_actions_lock(%rip)
	je	.L3
	leaq	nss_actions_lock(%rip), %rdi
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rsi
	jmp	.L3
.L14:
	xorl	%r8d, %r8d
	jmp	.L7
	.size	__nss_action_allocate, .-__nss_action_allocate
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	__nss_action_freeres
	.hidden	__nss_action_freeres
	.type	__nss_action_freeres, @function
__nss_action_freeres:
	movq	nss_actions(%rip), %rdi
	testq	%rdi, %rdi
	je	.L31
	pushq	%rbx
	.p2align 4,,10
	.p2align 3
.L25:
	movq	(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L25
	movq	$0, nss_actions(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movq	$0, nss_actions(%rip)
	ret
	.size	__nss_action_freeres, .-__nss_action_freeres
	.local	nss_actions_lock
	.comm	nss_actions_lock,4,4
	.local	nss_actions
	.comm	nss_actions,8,8
	.hidden	__lll_lock_wait_private
