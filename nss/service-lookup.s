	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"services"
	.text
	.p2align 4,,15
	.globl	__nss_services_lookup2
	.hidden	__nss_services_lookup2
	.type	__nss_services_lookup2, @function
__nss_services_lookup2:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	__nss_services_database(%rip), %rcx
	movq	%rdi, %rbx
	leaq	.LC0(%rip), %rdi
	movq	%rsi, %rbp
	subq	$8, %rsp
	xorl	%esi, %esi
	movq	%rdx, %r12
	xorl	%edx, %edx
	call	__nss_database_lookup2
	testl	%eax, %eax
	js	.L2
	movq	__nss_services_database(%rip), %rax
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__nss_lookup
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__nss_services_lookup2, .-__nss_services_lookup2
	.hidden	__nss_lookup
	.hidden	__nss_database_lookup2
	.hidden	__nss_services_database
