	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"nss_parse_line_result.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"parse_line_result >= -1 && parse_line_result <= 1"
	.text
	.p2align 4,,15
	.globl	__nss_parse_line_result
	.hidden	__nss_parse_line_result
	.type	__nss_parse_line_result, @function
__nss_parse_line_result:
	leal	1(%rdx), %eax
	cmpl	$2, %eax
	ja	.L12
	testl	%edx, %edx
	je	.L4
	cmpl	$1, %edx
	jne	.L13
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	jmp	__nss_readline_seek
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$22, %eax
	ret
.L12:
	leaq	__PRETTY_FUNCTION__.2851(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movl	$27, %edx
	call	__assert_fail
	.size	__nss_parse_line_result, .-__nss_parse_line_result
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.2851, @object
	.size	__PRETTY_FUNCTION__.2851, 24
__PRETTY_FUNCTION__.2851:
	.string	"__nss_parse_line_result"
	.hidden	__assert_fail
	.hidden	__nss_readline_seek
