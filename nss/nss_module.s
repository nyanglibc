	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libnss_files.so.2"
.LC1:
	.string	"libnss_%s.so%s"
.LC2:
	.string	"_nss_%s_%s"
	.text
	.p2align 4,,15
	.type	module_load, @function
module_load:
	pushq	%r15
	pushq	%r14
	leaq	15+.LC0(%rip), %rcx
	pushq	%r13
	pushq	%r12
	leaq	536(%rdi), %r12
	pushq	%rbp
	pushq	%rbx
	leaq	.LC1(%rip), %rsi
	xorl	%eax, %eax
	movq	%r12, %rdx
	subq	$552, %rsp
	leaq	32(%rsp), %r13
	movq	%rdi, 8(%rsp)
	movq	%r13, %rdi
	call	__asprintf
	testl	%eax, %eax
	js	.L27
	movq	32(%rsp), %rdi
	movl	$-2147483646, %esi
	call	__libc_dlopen_mode
	movq	32(%rsp), %rdi
	movq	%rax, %r14
	call	free@PLT
	testq	%r14, %r14
	je	.L30
	leaq	nss_function_name_array(%rip), %rbp
	xorl	%ebx, %ebx
	leaq	24(%rsp), %r15
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L12:
	movq	24(%rsp), %rsi
	movq	%r14, %rdi
	addq	$19, %rbp
	call	__libc_dlsym
	movq	24(%rsp), %rdi
	movq	%rax, 0(%r13,%rbx,8)
	movq	%rax, (%rsp)
	call	free@PLT
	movq	(%rsp), %rax
#APP
# 187 "nss_module.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rsp,%rbx,8)
	addq	$1, %rbx
	cmpq	$64, %rbx
	je	.L31
.L13:
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r15, %rdi
	call	__asprintf
	testl	%eax, %eax
	jns	.L12
	movq	%r14, %rdi
	call	__libc_dlclose
.L27:
	xorl	%r8d, %r8d
.L1:
	addq	$552, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L30:
#APP
# 150 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
.L6:
	movq	8(%rsp), %rax
	movl	$1, %r8d
	movl	(%rax), %eax
	cmpl	$1, %eax
	je	.L7
	jb	.L9
	cmpl	$2, %eax
	setne	%r8b
.L7:
#APP
# 165 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L11
	subl	$1, nss_module_list_lock(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
#APP
# 222 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L14
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
.L15:
	movq	8(%rsp), %rax
	movl	(%rax), %eax
	cmpl	$1, %eax
	je	.L17
	jb	.L18
	cmpl	$2, %eax
	je	.L18
.L16:
#APP
# 240 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L19
	subl	$1, nss_module_list_lock(%rip)
	movl	$1, %r8d
	jmp	.L1
.L17:
	movq	%r14, %rdi
	call	__libc_dlclose
	jmp	.L16
.L9:
	movq	8(%rsp), %rax
	xorl	%r8d, %r8d
	movl	$2, (%rax)
	jmp	.L7
.L18:
	movq	8(%rsp), %rcx
	movq	32(%rsp), %rdx
	movq	%r13, %rsi
	leaq	16(%rcx), %rdi
	leaq	8(%rcx), %rax
	movq	%rdx, 8(%rcx)
	movq	536(%rsp), %rdx
	andq	$-8, %rdi
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	$512, %eax
	movq	%rdx, 512(%rcx)
	shrl	$3, %eax
	movq	%rcx, %rdx
	movl	%eax, %ecx
	rep movsq
	movq	%r14, 520(%rdx)
	movl	$1, (%rdx)
	jmp	.L16
.L5:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, nss_module_list_lock(%rip)
	je	.L6
	leaq	nss_module_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L6
.L11:
	xorl	%eax, %eax
#APP
# 165 "nss_module.c" 1
	xchgl %eax, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
.L28:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	nss_module_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 240 "nss_module.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L19:
	xorl	%eax, %eax
#APP
# 240 "nss_module.c" 1
	xchgl %eax, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	$1, %r8d
	jle	.L1
	jmp	.L28
.L14:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, nss_module_list_lock(%rip)
	je	.L15
	leaq	nss_module_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L15
	.size	module_load, .-module_load
	.p2align 4,,15
	.globl	__nss_module_allocate
	.hidden	__nss_module_allocate
	.type	__nss_module_allocate, @function
__nss_module_allocate:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
#APP
# 77 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L33
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
.L34:
	movq	nss_module_list(%rip), %r13
	testq	%r13, %r13
	je	.L35
	movq	%r13, %rbx
	.p2align 4,,10
	.p2align 3
.L38:
	leaq	536(%rbx), %rdi
	movq	%rbp, %rdx
	movq	%r12, %rsi
	call	strncmp
	testl	%eax, %eax
	jne	.L36
	cmpb	$0, 536(%rbx,%rbp)
	je	.L37
.L36:
	movq	528(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L38
.L35:
	leaq	537(%rbp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L37
	leaq	536(%rbx), %rdi
	movl	$0, (%rbx)
	movq	%rbp, %rdx
	movq	%r12, %rsi
	call	memcpy@PLT
	movb	$0, 536(%rbx,%rbp)
	movq	%rbx, nss_module_list(%rip)
	movq	$0, 520(%rbx)
	movq	%r13, 528(%rbx)
	.p2align 4,,10
	.p2align 3
.L37:
#APP
# 105 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L39
	subl	$1, nss_module_list_lock(%rip)
.L32:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	xorl	%eax, %eax
#APP
# 105 "nss_module.c" 1
	xchgl %eax, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L32
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	nss_module_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 105 "nss_module.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, nss_module_list_lock(%rip)
	je	.L34
	leaq	nss_module_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L34
	.size	__nss_module_allocate, .-__nss_module_allocate
	.p2align 4,,15
	.globl	__nss_module_load
	.hidden	__nss_module_load
	.type	__nss_module_load, @function
__nss_module_load:
	movl	(%rdi), %eax
	cmpl	$1, %eax
	je	.L52
	jb	.L50
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	jmp	module_load
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, %eax
	ret
	.size	__nss_module_load, .-__nss_module_load
	.section	.rodata.str1.1
.LC3:
	.string	"nss_module.c"
.LC4:
	.string	"name_entry != NULL"
	.text
	.p2align 4,,15
	.globl	__nss_module_get_function
	.hidden	__nss_module_get_function
	.type	__nss_module_get_function, @function
__nss_module_get_function:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	movl	$64, %r15d
	leaq	nss_function_name_array(%rip), %r12
	subq	$8, %rsp
	call	__nss_module_load
	testb	%al, %al
	jne	.L54
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L56:
	cmpq	%rbp, %rbx
	jbe	.L59
.L58:
	movq	%rbx, %r15
.L54:
	leaq	0(%rbp,%r15), %rbx
	movq	%r13, %rdi
	shrq	%rbx
	leaq	(%rbx,%rbx,8), %rax
	leaq	(%rbx,%rax,2), %rsi
	addq	%r12, %rsi
	call	strcmp
	testl	%eax, %eax
	js	.L56
	je	.L57
	leaq	1(%rbx), %rbp
	cmpq	%r15, %rbp
	jnb	.L59
	movq	%r15, %rbx
	jmp	.L58
.L57:
	movq	8(%r14,%rbx,8), %rax
#APP
# 287 "nss_module.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
.L53:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	__PRETTY_FUNCTION__.12733(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$283, %edx
	call	__assert_fail
.L65:
	xorl	%eax, %eax
	jmp	.L53
	.size	__nss_module_get_function, .-__nss_module_get_function
	.p2align 4,,15
	.globl	__nss_module_disable_loading
	.type	__nss_module_disable_loading, @function
__nss_module_disable_loading:
	subq	$8, %rsp
#APP
# 356 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L67
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
.L68:
	movq	nss_module_list(%rip), %rax
	testq	%rax, %rax
	je	.L69
	.p2align 4,,10
	.p2align 3
.L71:
	movl	(%rax), %edx
	testl	%edx, %edx
	jne	.L70
	movl	$2, (%rax)
.L70:
	movq	528(%rax), %rax
	testq	%rax, %rax
	jne	.L71
.L69:
#APP
# 362 "nss_module.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L72
	subl	$1, nss_module_list_lock(%rip)
.L66:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, nss_module_list_lock(%rip)
	je	.L68
	leaq	nss_module_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L72:
	xorl	%eax, %eax
#APP
# 362 "nss_module.c" 1
	xchgl %eax, nss_module_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L66
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	nss_module_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 362 "nss_module.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L66
	.size	__nss_module_disable_loading, .-__nss_module_disable_loading
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	__nss_module_freeres
	.hidden	__nss_module_freeres
	.type	__nss_module_freeres, @function
__nss_module_freeres:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	nss_module_list(%rip), %rbx
	testq	%rbx, %rbx
	jne	.L82
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L81:
	movq	528(%rbx), %rbp
	movq	%rbx, %rdi
	call	free@PLT
	testq	%rbp, %rbp
	movq	%rbp, %rbx
	je	.L80
.L82:
	cmpl	$1, (%rbx)
	jne	.L81
	movq	520(%rbx), %rdi
	call	__libc_dlclose
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L80:
	movq	$0, nss_module_list(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__nss_module_freeres, .-__nss_module_freeres
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12733, @object
	.size	__PRETTY_FUNCTION__.12733, 26
__PRETTY_FUNCTION__.12733:
	.string	"__nss_module_get_function"
	.section	.rodata
	.align 32
	.type	nss_function_name_array, @object
	.size	nss_function_name_array, 1216
nss_function_name_array:
	.string	"endaliasent"
	.zero	7
	.string	"endetherent"
	.zero	7
	.string	"endgrent"
	.zero	10
	.string	"endhostent"
	.zero	8
	.string	"endnetent"
	.zero	9
	.string	"endnetgrent"
	.zero	7
	.string	"endprotoent"
	.zero	7
	.string	"endpwent"
	.zero	10
	.string	"endrpcent"
	.zero	9
	.string	"endservent"
	.zero	8
	.string	"endsgent"
	.zero	10
	.string	"endspent"
	.zero	10
	.string	"getaliasbyname_r"
	.zero	2
	.string	"getaliasent_r"
	.zero	5
	.string	"getcanonname_r"
	.zero	4
	.string	"getetherent_r"
	.zero	5
	.string	"getgrent_r"
	.zero	8
	.string	"getgrgid_r"
	.zero	8
	.string	"getgrnam_r"
	.zero	8
	.string	"gethostbyaddr2_r"
	.zero	2
	.string	"gethostbyaddr_r"
	.zero	3
	.string	"gethostbyname2_r"
	.zero	2
	.string	"gethostbyname3_r"
	.zero	2
	.string	"gethostbyname4_r"
	.zero	2
	.string	"gethostbyname_r"
	.zero	3
	.string	"gethostent_r"
	.zero	6
	.string	"gethostton_r"
	.zero	6
	.string	"getnetbyaddr_r"
	.zero	4
	.string	"getnetbyname_r"
	.zero	4
	.string	"getnetent_r"
	.zero	7
	.string	"getnetgrent_r"
	.zero	5
	.string	"getntohost_r"
	.zero	6
	.string	"getprotobyname_r"
	.zero	2
	.string	"getprotobynumber_r"
	.string	"getprotoent_r"
	.zero	5
	.string	"getpublickey"
	.zero	6
	.string	"getpwent_r"
	.zero	8
	.string	"getpwnam_r"
	.zero	8
	.string	"getpwuid_r"
	.zero	8
	.string	"getrpcbyname_r"
	.zero	4
	.string	"getrpcbynumber_r"
	.zero	2
	.string	"getrpcent_r"
	.zero	7
	.string	"getsecretkey"
	.zero	6
	.string	"getservbyname_r"
	.zero	3
	.string	"getservbyport_r"
	.zero	3
	.string	"getservent_r"
	.zero	6
	.string	"getsgent_r"
	.zero	8
	.string	"getsgnam_r"
	.zero	8
	.string	"getspent_r"
	.zero	8
	.string	"getspnam_r"
	.zero	8
	.string	"initgroups_dyn"
	.zero	4
	.string	"netname2user"
	.zero	6
	.string	"setaliasent"
	.zero	7
	.string	"setetherent"
	.zero	7
	.string	"setgrent"
	.zero	10
	.string	"sethostent"
	.zero	8
	.string	"setnetent"
	.zero	9
	.string	"setnetgrent"
	.zero	7
	.string	"setprotoent"
	.zero	7
	.string	"setpwent"
	.zero	10
	.string	"setrpcent"
	.zero	9
	.string	"setservent"
	.zero	8
	.string	"setsgent"
	.zero	10
	.string	"setspent"
	.zero	10
	.local	nss_module_list_lock
	.comm	nss_module_list_lock,4,4
	.local	nss_module_list
	.comm	nss_module_list,8,8
	.hidden	__assert_fail
	.hidden	strcmp
	.hidden	strncmp
	.hidden	__lll_lock_wait_private
	.hidden	__libc_dlclose
	.hidden	__libc_dlsym
	.hidden	__libc_dlopen_mode
	.hidden	__asprintf
