	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
#NO_APP
	.text
	.p2align 4,,15
	.globl	__nss_rewrite_field
	.hidden	__nss_rewrite_field
	.type	__nss_rewrite_field, @function
__nss_rewrite_field:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leaq	.LC0(%rip), %rbp
	subq	$8, %rsp
	testq	%rdi, %rdi
	movq	$0, (%rsi)
	je	.L1
	movq	%rsi, %r13
	leaq	__nss_invalid_field_characters(%rip), %rsi
	movq	%rdi, %r12
	movq	%r12, %rbp
	call	__GI_strpbrk
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1
	movq	%r12, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, 0(%r13)
	je	.L1
	subq	%r12, %rbx
	leaq	(%rax,%rbx), %rax
	leaq	__nss_invalid_field_characters(%rip), %rbx
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	1(%rax), %rdi
	movb	$32, (%rax)
	movq	%rbx, %rsi
	call	__GI_strpbrk
	testq	%rax, %rax
	jne	.L3
.L1:
	addq	$8, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__nss_rewrite_field, .-__nss_rewrite_field
	.hidden	__nss_invalid_field_characters
