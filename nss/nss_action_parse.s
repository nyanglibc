	.text
	.p2align 4,,15
	.type	action_list_add__, @function
action_list_add__:
	pushq	%r13
	leaq	24(%rdi), %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdx, %rbp
	movq	%r13, %rsi
	movl	$16, %edx
	subq	$8, %rsp
	movq	%rdi, %rbx
	call	__libc_dynarray_emplace_enlarge
	testb	%al, %al
	je	.L7
	movq	(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	salq	$4, %rax
	addq	%rdx, %rax
	movq	%rcx, (%rbx)
	movq	%r12, (%rax)
	movq	%rbp, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %r13
	je	.L3
	call	free@PLT
.L3:
	movq	%r13, 16(%rbx)
	movq	$0, (%rbx)
	movq	$-1, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	action_list_add__, .-action_list_add__
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"SUCCESS"
.LC1:
	.string	"UNAVAIL"
.LC2:
	.string	"NOTFOUND"
.LC3:
	.string	"TRYAGAIN"
.LC4:
	.string	"RETURN"
.LC5:
	.string	"CONTINUE"
.LC6:
	.string	"MERGE"
	.text
	.p2align 4,,15
	.globl	__nss_action_parse
	.type	__nss_action_parse, @function
__nss_action_parse:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	leaq	32(%rsp), %rax
	movq	$0, 32(%rsp)
	movq	$8, 40(%rsp)
	movq	%rax, 24(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 48(%rsp)
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L9
.L148:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rbx
	movq	%fs:(%rbx), %rdx
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L144:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L9
.L141:
	movsbq	%al, %rcx
	testb	$32, 1(%rdx,%rcx,2)
	jne	.L144
	cmpb	$91, %al
	je	.L9
	movq	%rdi, %rbp
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L145:
	movsbq	%al, %rcx
	testb	$32, 1(%rdx,%rcx,2)
	jne	.L12
	cmpb	$91, %al
	je	.L12
.L13:
	addq	$1, %rbp
	movzbl	0(%rbp), %eax
	testb	%al, %al
	jne	.L145
.L12:
	cmpq	%rdi, %rbp
	je	.L9
	movq	%rbp, %rsi
	subq	%rdi, %rsi
	call	__nss_module_allocate
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L15
	movzbl	0(%rbp), %eax
	testb	%al, %al
	je	.L60
	movq	%fs:(%rbx), %r15
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	jne	.L17
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L146:
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	je	.L18
.L17:
	addq	$1, %rbp
	movzbl	0(%rbp), %eax
	testb	%al, %al
	jne	.L146
.L60:
	movl	$320, %ebx
.L16:
	movq	8(%rsp), %r15
	movq	40(%rsp), %rdx
	movabsq	$-4294967296, %rax
	andq	%rax, %r15
	orq	%rbx, %r15
	cmpq	$-1, %rdx
	movq	%r15, 8(%rsp)
	je	.L52
	movq	32(%rsp), %rax
	cmpq	%rax, %rdx
	je	.L147
	leaq	1(%rax), %rdx
	salq	$4, %rax
	addq	48(%rsp), %rax
	movq	16(%rsp), %rbx
	movq	%rdx, 32(%rsp)
	movq	%rbx, (%rax)
	movq	8(%rsp), %rbx
	movq	%rbx, 8(%rax)
.L52:
	movq	%rbp, %rdi
	movzbl	(%rdi), %eax
	testb	%al, %al
	jne	.L148
	.p2align 4,,10
	.p2align 3
.L9:
	movq	40(%rsp), %rcx
	xorl	%edx, %edx
	movq	32(%rsp), %rax
	cmpq	$-1, %rcx
	je	.L149
	cmpq	%rcx, %rax
	je	.L150
	movq	48(%rsp), %rdi
	leaq	1(%rax), %rsi
	salq	$4, %rax
	movq	%rsi, 32(%rsp)
	addq	%rdi, %rax
	movq	$0, (%rax)
	movq	%rdx, 8(%rax)
.L56:
	call	__nss_action_allocate
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L18:
	cmpb	$91, %al
	jne	.L60
	movzbl	1(%rbp), %r14d
	leaq	1(%rbp), %r13
	testb	%r14b, %r14b
	jne	.L142
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L151:
	addq	$1, %r13
	movzbl	0(%r13), %r14d
	testb	%r14b, %r14b
	je	.L21
.L142:
	movsbq	%r14b, %rax
	testb	$32, 1(%r15,%rax,2)
	jne	.L151
.L21:
	movl	$320, %ebx
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$33, %r14b
	movl	%r14d, %eax
	jne	.L24
	movzbl	1(%r13), %eax
	addq	$1, %r13
.L24:
	testb	%al, %al
	je	.L25
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	jne	.L25
	subl	$61, %eax
	testb	$-33, %al
	je	.L25
	movq	%r13, %r12
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L152:
	subl	$61, %eax
	testb	$-33, %al
	je	.L71
.L28:
	addq	$1, %r12
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L71
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	je	.L152
.L71:
	movq	%r12, %rax
	subq	%r13, %rax
	cmpq	$7, %rax
	je	.L153
	cmpq	$8, %rax
	jne	.L25
	leaq	.LC2(%rip), %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	call	__strncasecmp@PLT
	testl	%eax, %eax
	je	.L64
	leaq	.LC3(%rip), %rsi
	movl	$8, %edx
	movq	%r13, %rdi
	call	__strncasecmp@PLT
	testl	%eax, %eax
	jne	.L25
	movl	$-2, %ebp
.L31:
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L32
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$1, %r12
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L25
.L32:
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	jne	.L33
	cmpb	$61, %al
	jne	.L25
	movzbl	1(%r12), %eax
	leaq	1(%r12), %rdi
	testb	%al, %al
	jne	.L143
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L154:
	addq	$1, %rdi
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L25
.L143:
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	jne	.L154
	subl	$61, %eax
	testb	$-33, %al
	je	.L25
	movq	%rdi, %r13
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L155:
	subl	$61, %eax
	testb	$-33, %al
	je	.L37
.L38:
	addq	$1, %r13
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L37
	movsbq	%al, %rdx
	testb	$32, 1(%r15,%rdx,2)
	je	.L155
.L37:
	movq	%r13, %rax
	subq	%rdi, %rax
	cmpq	$6, %rax
	je	.L156
	cmpq	$8, %rax
	jne	.L43
	leaq	.LC5(%rip), %rsi
	movl	$8, %edx
	call	__strncasecmp@PLT
	testl	%eax, %eax
	je	.L67
	.p2align 4,,10
	.p2align 3
.L25:
	cmpq	$-1, 40(%rsp)
	je	.L19
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
.L8:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	leaq	.LC0(%rip), %rsi
	movl	$7, %edx
	movq	%r13, %rdi
	call	__strncasecmp@PLT
	testl	%eax, %eax
	je	.L62
	leaq	.LC1(%rip), %rsi
	movl	$7, %edx
	movq	%r13, %rdi
	call	__strncasecmp@PLT
	testl	%eax, %eax
	jne	.L25
	movl	$-1, %ebp
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L156:
	leaq	.LC4(%rip), %rsi
	movl	$6, %edx
	call	__strncasecmp@PLT
	testl	%eax, %eax
	jne	.L25
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L42:
	leal	4(%rbp,%rbp), %ecx
	movl	$3, %edx
	sall	%cl, %edx
	cmpb	$33, %r14b
	notl	%edx
	je	.L157
	andl	%edx, %ebx
	sall	%cl, %eax
	orl	%eax, %ebx
.L45:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L25
	movsbq	%al, %rdx
	movl	%eax, %r14d
	testb	$32, 1(%r15,%rdx,2)
	je	.L50
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$1, %r13
	movzbl	0(%r13), %r14d
	testb	%r14b, %r14b
	je	.L23
	movsbq	%r14b, %rax
	testb	$32, 1(%r15,%rax,2)
	jne	.L49
.L50:
	cmpb	$93, %r14b
	je	.L51
	movzbl	0(%r13), %r14d
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%ebp, %ebp
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L157:
	leal	0(,%rax,4), %esi
	movl	%eax, %edi
	shrl	%cl, %ebx
	sall	$4, %edi
	andl	$3, %ebx
	orl	%edi, %esi
	movl	%eax, %edi
	sall	%cl, %ebx
	orl	%eax, %esi
	sall	$6, %edi
	sall	$8, %eax
	orl	%edi, %esi
	orl	%esi, %eax
	andl	%eax, %edx
	orl	%edx, %ebx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L62:
	movl	$1, %ebp
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L67:
	xorl	%eax, %eax
	jmp	.L42
.L15:
	movq	24(%rsp), %rax
	movq	48(%rsp), %rdi
	addq	$24, %rax
	cmpq	%rax, %rdi
	je	.L19
	call	free@PLT
.L19:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L8
.L51:
	leaq	1(%r13), %rbp
	jmp	.L16
.L147:
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdi
	movq	%r15, %rdx
	call	action_list_add__
	jmp	.L52
.L149:
	movq	48(%rsp), %rdi
	movq	%rax, %rsi
	jmp	.L56
.L150:
	movq	24(%rsp), %rdi
	xorl	%esi, %esi
	call	action_list_add__
	movq	32(%rsp), %rsi
	movq	48(%rsp), %rdi
	jmp	.L56
.L43:
	cmpq	$5, %rax
	jne	.L25
	leaq	.LC6(%rip), %rsi
	movl	$5, %edx
	call	__strncasecmp@PLT
	testl	%eax, %eax
	jne	.L25
	movl	$2, %eax
	jmp	.L42
	.size	__nss_action_parse, .-__nss_action_parse
	.hidden	__nss_action_allocate
	.hidden	__nss_module_allocate
	.hidden	__libc_dynarray_emplace_enlarge
