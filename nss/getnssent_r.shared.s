	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__nss_setent
	.hidden	__nss_setent
	.type	__nss_setent, @function
__nss_setent:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movq	%rdx, %rbx
	movq	%r8, %r14
	subq	$56, %rsp
	movl	120(%rsp), %eax
	movl	%r9d, 28(%rsp)
	movq	112(%rsp), %rbp
	movq	$0, 16(%rsp)
	testl	%eax, %eax
	jne	.L23
.L2:
	leaq	40(%rsp), %rax
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, 8(%rsp)
	movq	%rax, %rcx
	call	*%r13
	testl	%eax, %eax
	jne	.L4
	movq	(%rbx), %rax
	testq	%rbp, %rbp
	movq	40(%rsp), %rdi
	movq	%rax, (%r12)
	movq	(%rbx), %r13
	movq	(%r14), %r12
	je	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	call	__GI__dl_mcount_wrapper_check
	movl	0(%rbp), %edi
	call	*40(%rsp)
.L6:
	movq	(%rbx), %rsi
	leal	4(%rax,%rax), %ecx
	movl	8(%rsi), %edx
	shrl	%cl, %edx
	andl	$3, %edx
	cmpl	$2, %edx
	jne	.L7
	cmpq	%r12, %r13
	jne	.L9
	movq	%rsi, (%r14)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L4:
	movq	$-1, (%r12)
	.p2align 4,,10
	.p2align 3
.L9:
	movq	16(%rsp), %rdi
	call	__GI___resolv_context_put
	testq	%rbp, %rbp
	je	.L1
	movl	28(%rsp), %eax
	movl	%eax, 0(%rbp)
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	8(%rsp), %rcx
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%eax, %r8d
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	__GI___nss_next2
	cmpq	%r12, %r13
	je	.L24
.L10:
	testl	%eax, %eax
	jne	.L9
	testq	%rbp, %rbp
	movq	(%rbx), %r13
	movq	(%r14), %r12
	movq	40(%rsp), %rdi
	jne	.L25
.L5:
	call	__GI__dl_mcount_wrapper_check
	xorl	%edi, %edi
	call	*40(%rsp)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L24:
	movq	(%rbx), %rdx
	movq	%rdx, (%r14)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L23:
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	jne	.L2
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L1
	.size	__nss_setent, .-__nss_setent
	.p2align 4,,15
	.globl	__nss_endent
	.hidden	__nss_endent
	.type	__nss_endent, @function
__nss_endent:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r10
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rbx
	movq	%rcx, %r15
	movq	%r8, %rbp
	subq	$40, %rsp
	testl	%r9d, %r9d
	jne	.L39
.L27:
	leaq	24(%rsp), %r13
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r13, %rcx
	call	*%r10
	testl	%eax, %eax
	jne	.L29
	movq	(%rbx), %rax
	movq	%rax, (%r15)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	movl	$1, %r9d
	movq	%r13, %rcx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI___nss_next2
	testl	%eax, %eax
	jne	.L32
.L31:
	movq	24(%rsp), %rdi
	call	__GI__dl_mcount_wrapper_check
	call	*24(%rsp)
	movq	0(%rbp), %rax
	cmpq	%rax, (%rbx)
	jne	.L40
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L29:
	movq	$-1, (%r15)
.L32:
	movq	$0, (%rbx)
	movq	%r14, %rdi
	movq	$0, 0(%rbp)
	call	__GI___resolv_context_put
.L26:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%rsi, 8(%rsp)
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	%rax, %r14
	movq	8(%rsp), %r10
	jne	.L27
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movl	$-1, %fs:(%rax)
	jmp	.L26
	.size	__nss_endent, .-__nss_endent
	.p2align 4,,15
	.globl	__nss_getent_r
	.hidden	__nss_getent_r
	.type	__nss_getent_r, @function
__nss_getent_r:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%r8, %rbx
	subq	$72, %rsp
	movl	136(%rsp), %eax
	movq	%rsi, 24(%rsp)
	movq	%r9, 16(%rsp)
	testl	%eax, %eax
	je	.L68
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L98
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L44
.L102:
	leaq	48(%rsp), %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	movq	%rcx, 8(%rsp)
	call	*%r12
	testl	%eax, %eax
	jne	.L45
	movq	0(%rbp), %rax
	movq	%rax, (%rbx)
.L50:
	movq	%fs:0, %rax
	movq	%rax, 40(%rsp)
	leaq	56(%rsp), %rax
	movq	%rax, 32(%rsp)
.L46:
	movq	16(%rsp), %rax
	movq	48(%rsp), %rdi
	movq	0(%rbp), %r13
	movq	(%rax), %r12
	call	__GI__dl_mcount_wrapper_check
	movq	40(%rsp), %rax
	movq	__libc_h_errno@gottpoff(%rip), %rsi
	movq	__libc_errno@gottpoff(%rip), %rcx
	movq	160(%rsp), %rdx
	movq	144(%rsp), %rdi
	leaq	(%rax,%rsi), %r8
	movq	152(%rsp), %rsi
	addq	%rax, %rcx
	call	*48(%rsp)
	cmpl	$-2, %eax
	movl	%eax, %ebx
	je	.L99
	cmpl	$1, %eax
	jne	.L81
	movq	0(%rbp), %rdx
	movl	8(%rdx), %eax
	shrl	$6, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L56
.L81:
	movq	8(%rsp), %rcx
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	%ebx, %r8d
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	__GI___nss_next2
	cmpq	%r12, %r13
	je	.L100
.L57:
	testl	%eax, %eax
	je	.L101
	movq	%r14, %rdi
	call	__GI___resolv_context_put
	cmpl	$1, %ebx
	je	.L65
	movq	168(%rsp), %rax
	cmpl	$-2, %ebx
	movq	$0, (%rax)
	jne	.L96
.L66:
	cmpq	$0, 176(%rsp)
	je	.L63
	movq	176(%rsp), %rcx
	movl	$11, %eax
	cmpl	$-1, (%rcx)
	jne	.L41
.L63:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movq	(%rbx), %rax
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.L102
.L44:
	cmpq	$-1, %rax
	je	.L48
	cmpq	$0, 0(%rbp)
	je	.L103
.L49:
	leaq	48(%rsp), %rcx
	xorl	%edx, %edx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	movq	%rcx, 8(%rsp)
	call	__GI___nss_lookup
	testl	%eax, %eax
	je	.L50
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L45:
	movq	$-1, (%rbx)
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%r14, %rdi
	call	__GI___resolv_context_put
	movq	168(%rsp), %rax
	movq	$0, (%rax)
.L96:
	movl	$2, %eax
.L41:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	movq	32(%rsp), %rcx
	movq	24(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	call	__GI___nss_lookup
	testl	%eax, %eax
	jne	.L48
	cmpq	$0, 128(%rsp)
	movq	56(%rsp), %rdi
	je	.L60
	call	__GI__dl_mcount_wrapper_check
	movq	128(%rsp), %rax
	movl	(%rax), %edi
	call	*56(%rsp)
	movl	%eax, %ebx
.L61:
	cmpl	$1, %ebx
	jne	.L81
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L100:
	movq	0(%rbp), %rdx
	movq	16(%rsp), %rsi
	movq	%rdx, (%rsi)
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L56:
	cmpq	%r12, %r13
	je	.L64
.L94:
	movq	%r14, %rdi
	call	__GI___resolv_context_put
.L65:
	movq	168(%rsp), %rax
	movq	144(%rsp), %rcx
	movq	%rcx, (%rax)
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	movq	16(%rsp), %rax
	movq	%rdx, (%rax)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L99:
	cmpq	$0, 176(%rsp)
	je	.L53
	movq	176(%rsp), %rax
	cmpl	$-1, (%rax)
	jne	.L81
.L53:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L81
	movq	%r14, %rdi
	call	__GI___resolv_context_put
	movq	168(%rsp), %rax
	movq	$0, (%rax)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L60:
	call	__GI__dl_mcount_wrapper_check
	xorl	%edi, %edi
	call	*56(%rsp)
	movl	%eax, %ebx
	jmp	.L61
.L98:
	movq	176(%rsp), %rax
	movl	$-1, (%rax)
	movq	168(%rsp), %rax
	movq	$0, (%rax)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L103:
	movq	%rax, 0(%rbp)
	jmp	.L49
	.size	__nss_getent_r, .-__nss_getent_r
