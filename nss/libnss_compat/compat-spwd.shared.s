	.text
	.p2align 4,,15
	.type	give_spwd_free, @function
give_spwd_free:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	free@PLT
	movq	8(%rbx), %rdi
	call	free@PLT
	pxor	%xmm0, %xmm0
	movq	$-1, %rax
	movq	$0, 32(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rax, 56(%rbx)
	movaps	%xmm0, (%rbx)
	movq	%rax, 64(%rbx)
	movaps	%xmm0, 16(%rbx)
	popq	%rbx
	ret
	.size	give_spwd_free, .-give_spwd_free
	.p2align 4,,15
	.type	internal_endspent, @function
internal_endspent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	fclose@PLT
	movq	$0, 8(%rbx)
.L5:
	cmpb	$0, (%rbx)
	jne	.L13
.L6:
	movq	16(%rbx), %rax
	movb	$0, (%rbx)
	movb	$0, 2(%rbx)
	movb	$1, 1(%rbx)
	testq	%rax, %rax
	je	.L7
	movl	$1, 24(%rbx)
	movb	$124, (%rax)
	leaq	32(%rbx), %rdi
	movq	16(%rbx), %rax
	movb	$0, 1(%rax)
	call	give_spwd_free
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	32(%rbx), %rdi
	movl	$0, 24(%rbx)
	call	give_spwd_free
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	104(%rbx), %rdi
	call	__internal_endnetgrent@PLT
	jmp	.L6
	.size	internal_endspent, .-internal_endspent
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"nis"
.LC1:
	.string	"passwd_compat"
.LC2:
	.string	"shadow_compat"
.LC3:
	.string	"setspent"
.LC4:
	.string	"getspnam_r"
.LC5:
	.string	"getspent_r"
.LC6:
	.string	"endspent"
	.text
	.p2align 4,,15
	.type	init_nss_interface, @function
init_nss_interface:
	leaq	ni(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	call	__nss_database_lookup2@PLT
	testl	%eax, %eax
	js	.L14
	movq	ni(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, setspent_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, getspnam_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, getspent_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	%rax, endspent_impl(%rip)
.L14:
	addq	$8, %rsp
	ret
	.size	init_nss_interface, .-init_nss_interface
	.section	.rodata.str1.1
.LC7:
	.string	"/etc/shadow"
	.text
	.p2align 4,,15
	.type	internal_setspent, @function
internal_setspent:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movb	$0, (%rdi)
	movb	$0, 2(%rdi)
	movb	$1, 1(%rdi)
	testq	%rax, %rax
	je	.L18
	movl	$1, 24(%rdi)
	movb	$124, (%rax)
	movq	16(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L29
.L20:
	call	rewind@PLT
.L21:
	leaq	32(%rbx), %rdi
	call	give_spwd_free
	testl	%ebp, %ebp
	movl	$1, %eax
	je	.L17
	movq	setspent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L26
	movl	%r12d, %edi
	call	*%rax
	movl	%eax, 4(%rbx)
	movl	%ebp, %eax
.L17:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$0, 24(%rdi)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L20
.L29:
	leaq	.LC7(%rip), %rdi
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	jne	.L21
	movq	errno@gottpoff(%rip), %rax
	leaq	32(%rbx), %rdi
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	movl	%eax, 8(%rsp)
	movl	%eax, 12(%rsp)
	call	give_spwd_free
	movl	8(%rsp), %eax
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%ebp, %eax
	jmp	.L17
	.size	internal_setspent, .-internal_setspent
	.p2align 4,,15
	.type	in_blacklist.isra.3, @function
in_blacklist.isra.3:
	pushq	%rbp
	leal	3(%rsi), %eax
	movq	%rsp, %rbp
	pushq	%rbx
	cltq
	addq	$15, %rax
	subq	$8, %rsp
	movq	(%rdx), %rbx
	andq	$-16, %rax
	subq	%rax, %rsp
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L30
	movq	%rdi, %rsi
	leaq	1(%rsp), %rdi
	movb	$124, (%rsp)
	call	__stpcpy@PLT
	movl	$124, %edx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movw	%dx, (%rax)
	call	strstr@PLT
	testq	%rax, %rax
	setne	%al
.L30:
	movq	-8(%rbp), %rbx
	leave
	ret
	.size	in_blacklist.isra.3, .-in_blacklist.isra.3
	.p2align 4,,15
	.type	copy_spwd_changes.isra.4, @function
copy_spwd_changes.isra.4:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	8(%rsi), %r12
	testq	%r12, %r12
	je	.L37
	cmpb	$0, (%r12)
	jne	.L75
.L37:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L40
	movq	%rax, 16(%rbp)
.L40:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.L41
	movq	%rax, 24(%rbp)
.L41:
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.L42
	movq	%rax, 32(%rbp)
.L42:
	movq	40(%rbx), %rax
	cmpq	$-1, %rax
	je	.L43
	movq	%rax, 40(%rbp)
.L43:
	movq	48(%rbx), %rax
	cmpq	$-1, %rax
	je	.L44
	movq	%rax, 48(%rbp)
.L44:
	movq	56(%rbx), %rax
	cmpq	$-1, %rax
	je	.L45
	movq	%rax, 56(%rbp)
.L45:
	movq	64(%rbx), %rax
	cmpq	$-1, %rax
	je	.L36
	movq	%rax, 64(%rbp)
.L36:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	testq	%rdx, %rdx
	je	.L76
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.L39
	movq	%r13, %rdi
	movq	%rdx, 8(%rsp)
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	cmpq	%rax, %r14
	movq	8(%rsp), %rdx
	jnb	.L77
.L39:
	movq	%rdx, 8(%rbp)
	movq	8(%rbx), %rsi
	movq	%rdx, %rdi
	call	strcpy@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L77:
	leaq	1(%rax), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r12, %rdi
	call	strdup@PLT
	movq	%rax, 8(%rbp)
	jmp	.L37
	.size	copy_spwd_changes.isra.4, .-copy_spwd_changes.isra.4
	.section	.text.unlikely,"ax",@progbits
	.type	getspnam_plususer, @function
getspnam_plususer:
	pushq	%r15
	pushq	%r14
	orl	$-1, %r14d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	movq	getspnam_r_impl(%rip), %r13
	testq	%r13, %r13
	je	.L78
	leaq	32(%rsp), %r12
	xorl	%eax, %eax
	movq	%rcx, %rbp
	movq	%rdi, %r14
	movl	$10, %ecx
	movq	%rdx, 8(%rsp)
	movq	%r12, %rdi
	xorl	%edx, %edx
	movq	%r8, %r15
	rep stosl
	orq	$-1, %rcx
	movq	%r12, %rdi
	movq	%r9, 24(%rsp)
	movq	%rsi, %rbx
	movq	%rcx, 72(%rsp)
	movq	%rcx, 80(%rsp)
	movq	%rcx, 88(%rsp)
	movq	%rcx, 96(%rsp)
	movq	%rcx, 16(%rsp)
	call	copy_spwd_changes.isra.4
	movq	40(%rsp), %rdi
	xorl	%eax, %eax
	movq	24(%rsp), %r8
	testq	%rdi, %rdi
	je	.L80
	movq	16(%rsp), %rcx
	repnz scasb
	movq	%rcx, %rax
	notl	%eax
.L80:
	cltq
	cmpq	%r15, %rax
	jbe	.L81
	movl	$34, (%r8)
	movl	$-2, %r14d
	jmp	.L78
.L81:
	subq	%rax, %r15
	movq	%r14, %rdi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	%rbx, %rsi
	call	*%r13
	cmpl	$1, %eax
	movl	%eax, %r14d
	jne	.L78
	movq	(%rbx), %r8
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	8(%rsp), %rdx
	movq	%r8, %rdi
	repnz scasb
	addq	$16, %rdx
	movq	%r8, %rdi
	movq	%rcx, %rsi
	notq	%rsi
	leaq	-1(%rsi), %rsi
	call	in_blacklist.isra.3
	testb	%al, %al
	jne	.L84
	leaq	0(%rbp,%r15), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	call	copy_spwd_changes.isra.4
	movq	%r12, %rdi
	call	give_spwd_free
	jmp	.L78
.L84:
	xorl	%r14d, %r14d
.L78:
	addq	$120, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	getspnam_plususer, .-getspnam_plususer
	.text
	.p2align 4,,15
	.type	getspent_next_nss.constprop.7, @function
getspent_next_nss.constprop.7:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	getspent_r_impl(%rip), %rbx
	testq	%rbx, %rbx
	je	.L92
	movq	%rdi, %rbp
	movq	40+ext_ent(%rip), %rdi
	movq	%rsi, %r13
	movq	%rdx, %r12
	movq	%rcx, %r14
	testq	%rdi, %rdi
	je	.L91
	call	strlen@PLT
	addl	$1, %eax
	cltq
	cmpq	%r12, %rax
	ja	.L89
	subq	%rax, %r12
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L97:
	movq	0(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%r15, %rdi
	call	in_blacklist.isra.3
	testb	%al, %al
	je	.L90
	movq	getspent_r_impl(%rip), %rbx
.L91:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	*%rbx
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L97
.L86:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	0(%r13,%r12), %rdx
	leaq	32+ext_ent(%rip), %rsi
	movq	%rbp, %rdi
	call	copy_spwd_changes.isra.4
	jmp	.L86
.L89:
	movl	$34, (%r14)
	movl	$-2, %ebx
	jmp	.L86
.L92:
	movl	$-1, %ebx
	jmp	.L86
	.size	getspent_next_nss.constprop.7, .-getspent_next_nss.constprop.7
	.p2align 4,,15
	.type	blacklist_store_name.constprop.9, @function
blacklist_store_name.constprop.9:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	movl	28+ext_ent(%rip), %esi
	movq	%rax, %rbp
	testl	%esi, %esi
	jne	.L99
	leal	(%rax,%rax), %edi
	movl	$512, %eax
	cmpl	$512, %edi
	cmovl	%eax, %edi
	movl	%edi, 28+ext_ent(%rip)
	movslq	%edi, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16+ext_ent(%rip)
	je	.L98
	movl	$124, %ecx
	movl	$1, 24+ext_ent(%rip)
	movl	$1, %edi
	movw	%cx, (%rax)
.L102:
	addq	%rax, %rdi
	movq	%rbx, %rsi
	call	__stpcpy@PLT
	movl	$124, %edx
	movw	%dx, (%rax)
	movl	24+ext_ent(%rip), %eax
	leal	1(%rax,%rbp), %eax
	movl	%eax, 24+ext_ent(%rip)
.L98:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	in_blacklist.isra.3
	testb	%al, %al
	jne	.L98
	movslq	24+ext_ent(%rip), %rdi
	movl	28+ext_ent(%rip), %edx
	movq	16+ext_ent(%rip), %rax
	leal	1(%rdi,%rbp), %ecx
	cmpl	%edx, %ecx
	jl	.L102
	leal	(%rbp,%rbp), %esi
	movl	$256, %ecx
	movq	%rax, %rdi
	cmpl	$256, %esi
	cmovl	%ecx, %esi
	addl	%edx, %esi
	movl	%esi, 28+ext_ent(%rip)
	movslq	%esi, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L107
	movq	%rax, 16+ext_ent(%rip)
	movslq	24+ext_ent(%rip), %rdi
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L107:
	movq	16+ext_ent(%rip), %rdi
	call	free@PLT
	movl	$0, 28+ext_ent(%rip)
	jmp	.L98
	.size	blacklist_store_name.constprop.9, .-blacklist_store_name.constprop.9
	.p2align 4,,15
	.type	getspent_next_nss_netgr.constprop.8, @function
getspent_next_nss_netgr.constprop.8:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	cmpq	$0, getspnam_r_impl(%rip)
	movq	%rdi, (%rsp)
	movq	$0, 32(%rsp)
	je	.L121
	movq	%rcx, %rbx
	movl	4+ext_ent(%rip), %ecx
	cmpl	$1, %ecx
	je	.L126
.L108:
	addq	$72, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L126:
	cmpb	$0, 2+ext_ent(%rip)
	movq	%rdx, %r12
	movq	%r8, %rbp
	jne	.L127
.L110:
	leaq	32(%rsp), %rax
	leaq	56(%rsp), %r15
	leaq	48(%rsp), %r14
	leaq	40(%rsp), %r13
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L111:
	subq	$8, %rsp
	leaq	104+ext_ent(%rip), %rcx
	movq	%r15, %rdx
	pushq	%rbp
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__internal_getnetgrent_r@PLT
	cmpl	$1, %eax
	popq	%rdx
	popq	%rcx
	jne	.L128
	movq	48(%rsp), %rax
	testq	%rax, %rax
	je	.L111
	cmpb	$45, (%rax)
	je	.L111
	movq	56(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L119
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L129
.L117:
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L111
.L119:
	movq	40+ext_ent(%rip), %rdi
	testq	%rdi, %rdi
	je	.L116
	call	strlen@PLT
	addl	$1, %eax
	cltq
	cmpq	%rax, %rbx
	jb	.L120
	subq	%rax, %rbx
.L116:
	movq	%rbp, %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	*getspnam_r_impl(%rip)
	cmpl	$1, %eax
	jne	.L111
	movl	%eax, 20(%rsp)
	movq	(%rsp), %rax
	movq	(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	strlen@PLT
	movq	8(%rsp), %r8
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%r8, %rdi
	call	in_blacklist.isra.3
	testb	%al, %al
	movl	20(%rsp), %ecx
	jne	.L111
	movq	(%rsp), %r14
	movl	%ecx, 8(%rsp)
	movq	(%r14), %rdi
	call	blacklist_store_name.constprop.9
	leaq	(%r12,%rbx), %rdx
	leaq	32+ext_ent(%rip), %rsi
	movq	%r14, %rdi
	call	copy_spwd_changes.isra.4
	movl	8(%rsp), %ecx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L129:
	movq	24(%rsp), %rdi
	call	__nss_get_default_domain@PLT
	testl	%eax, %eax
	jne	.L118
	movq	56(%rsp), %rsi
	movq	32(%rsp), %rdi
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L128:
	leaq	104+ext_ent(%rip), %rdi
	call	__internal_endnetgrent@PLT
	leaq	32+ext_ent(%rip), %rdi
	movb	$0, ext_ent(%rip)
	call	give_spwd_free
	movl	$2, %ecx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rsi, %r9
	leaq	104+ext_ent(%rip), %rsi
	xorl	%eax, %eax
	movl	$11, %ecx
	movq	%rsi, %rdi
	rep stosq
	movq	%r9, %rdi
	call	__internal_setnetgrent@PLT
	movb	$0, 2+ext_ent(%rip)
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L120:
	movl	$34, 0(%rbp)
	movl	$-2, %ecx
	jmp	.L108
.L118:
	leaq	104+ext_ent(%rip), %rdi
	call	__internal_endnetgrent@PLT
	leaq	32+ext_ent(%rip), %rdi
	movb	$0, ext_ent(%rip)
	call	give_spwd_free
	movl	$-1, %ecx
	jmp	.L108
.L121:
	movl	$-1, %ecx
	jmp	.L108
	.size	getspent_next_nss_netgr.constprop.8, .-getspent_next_nss_netgr.constprop.8
	.p2align 4,,15
	.type	getspent_next_file.constprop.6, @function
getspent_next_file.constprop.6:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	-1184(%rbp), %r13
	pushq	%rbx
	leaq	-1(%rsi,%rdx), %rbx
	movq	%rsi, %r12
	subq	$1224, %rsp
	movq	%rdi, -1240(%rbp)
	movq	%rdx, -1224(%rbp)
	movq	%rcx, -1232(%rbp)
.L155:
	movl	-1224(%rbp), %r14d
.L140:
	cmpq	$2, -1224(%rbp)
	jbe	.L132
.L165:
	movq	8+ext_ent(%rip), %rdi
	movq	%r13, %rsi
	call	fgetpos@PLT
	movb	$-1, (%rbx)
	movq	8+ext_ent(%rip), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L173
	cmpb	$-1, (%rbx)
	jne	.L172
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L138:
	addq	$1, %r15
.L137:
	movsbq	(%r15), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L138
	testb	%dl, %dl
	je	.L165
	cmpb	$35, %dl
	je	.L165
	movq	-1232(%rbp), %r8
	movq	-1224(%rbp), %rcx
	movq	%r12, %rdx
	movq	-1240(%rbp), %rsi
	movq	%r15, %rdi
	call	_nss_files_parse_spent@PLT
	testl	%eax, %eax
	je	.L140
	cmpl	$-1, %eax
	je	.L172
	movq	-1240(%rbp), %rax
	movq	(%rax), %rsi
	movzbl	(%rsi), %eax
	leal	-43(%rax), %edx
	andl	$253, %edx
	jne	.L142
	cmpb	$45, %al
	je	.L174
	cmpb	$43, %al
	jne	.L155
	movzbl	1(%rsi), %eax
	cmpb	$64, %al
	je	.L175
	testb	$-65, %al
	jne	.L176
	testb	%al, %al
	jne	.L155
	movq	-1240(%rbp), %rbx
	leaq	32+ext_ent(%rip), %rdi
	xorl	%edx, %edx
	movb	$0, 1+ext_ent(%rip)
	movb	$1, 2+ext_ent(%rip)
	movq	%rbx, %rsi
	call	copy_spwd_changes.isra.4
	movq	-1232(%rbp), %rcx
	movq	-1224(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	getspent_next_nss.constprop.7
	movl	%eax, %r14d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L173:
	movq	8+ext_ent(%rip), %rdi
	testb	$16, (%rdi)
	jne	.L177
.L135:
	movq	%r13, %rsi
	call	fsetpos@PLT
.L132:
	movq	-1232(%rbp), %rax
	movl	$-2, %r14d
	movl	$34, (%rax)
.L130:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movq	8+ext_ent(%rip), %rdi
	jmp	.L135
.L177:
	xorl	%r14d, %r14d
	jmp	.L130
.L176:
	xorl	%eax, %eax
	movq	%rsi, %rdi
	orq	$-1, %rcx
	repnz scasb
	leaq	1(%rsi), %r14
	movq	%rsp, %r15
	movq	%rcx, %rax
	notq	%rax
	movq	%r14, %rsi
	leaq	-1(%rax), %rdx
	addq	$14, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-1232(%rbp), %r9
	movq	-1224(%rbp), %r8
	leaq	ext_ent(%rip), %rdx
	movq	-1240(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r12, %rcx
	call	getspnam_plususer
	movq	%rsp, %rdi
	movl	%eax, %r14d
	call	blacklist_store_name.constprop.9
	cmpl	$1, %r14d
	je	.L152
	testl	$-3, %r14d
	je	.L178
	cmpl	$-2, %r14d
	jne	.L154
	movq	8+ext_ent(%rip), %rdi
	movq	%r13, %rsi
	call	fsetpos@PLT
	movq	-1232(%rbp), %rax
	movl	$34, (%rax)
.L154:
	movq	%r15, %rsp
	jmp	.L130
.L178:
	movq	%r15, %rsp
	jmp	.L155
.L152:
	movq	%r15, %rsp
.L142:
	movl	$1, %r14d
	jmp	.L130
.L174:
	movzbl	1(%rsi), %eax
	cmpb	$64, %al
	je	.L179
	testb	$-65, %al
	je	.L155
	leaq	1(%rsi), %rdi
	call	blacklist_store_name.constprop.9
	jmp	.L155
.L175:
	cmpb	$0, 2(%rsi)
	je	.L155
	movq	-1240(%rbp), %r14
	leaq	32+ext_ent(%rip), %rdi
	xorl	%edx, %edx
	movb	$1, ext_ent(%rip)
	movb	$1, 2+ext_ent(%rip)
	movq	%r14, %rsi
	call	copy_spwd_changes.isra.4
	movq	(%r14), %rax
	movq	-1232(%rbp), %r8
	movq	%r14, %rdi
	movq	-1224(%rbp), %rcx
	movq	%r12, %rdx
	leaq	2(%rax), %rsi
	movq	%rax, -1248(%rbp)
	call	getspent_next_nss_netgr.constprop.8
	cmpl	$2, %eax
	movl	%eax, %r14d
	jne	.L130
	jmp	.L155
.L179:
	cmpb	$0, 2(%rsi)
	je	.L155
	leaq	-1168(%rbp), %r14
	xorl	%eax, %eax
	movl	$22, %ecx
	leaq	-1072(%rbp), %r15
	movq	%r14, %rdi
	rep stosl
	leaq	2(%rsi), %rdi
	movq	%r14, %rsi
	call	__internal_setnetgrent@PLT
	leaq	-1192(%rbp), %rax
	movq	%rax, -1264(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, -1248(%rbp)
	leaq	-1200(%rbp), %rax
	movq	%rax, -1256(%rbp)
.L146:
	movq	-1264(%rbp), %rdx
	movq	-1248(%rbp), %rsi
	subq	$8, %rsp
	movq	-1256(%rbp), %rdi
	pushq	-1232(%rbp)
	movq	%r14, %rcx
	movl	$1024, %r9d
	movq	%r15, %r8
	call	__internal_getnetgrent_r@PLT
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L180
	movq	-1208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L146
	cmpb	$45, (%rdi)
	je	.L146
	call	blacklist_store_name.constprop.9
	jmp	.L146
.L180:
	movq	%r14, %rdi
	call	__internal_endnetgrent@PLT
	jmp	.L155
	.size	getspent_next_file.constprop.6, .-getspent_next_file.constprop.6
	.p2align 4,,15
	.globl	_nss_compat_setspent
	.type	_nss_compat_setspent, @function
_nss_compat_setspent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	movl	%edi, %ebx
	je	.L182
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L182:
	cmpq	$0, ni(%rip)
	je	.L192
.L183:
	leaq	ext_ent(%rip), %rdi
	movl	%ebx, %esi
	movl	$1, %edx
	call	internal_setspent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L181
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L181:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	call	init_nss_interface
	jmp	.L183
	.size	_nss_compat_setspent, .-_nss_compat_setspent
	.p2align 4,,15
	.globl	_nss_compat_endspent
	.type	_nss_compat_endspent, @function
_nss_compat_endspent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L194
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L194:
	movq	endspent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L195
	call	*%rax
.L195:
	leaq	ext_ent(%rip), %rdi
	call	internal_endspent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L193
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L193:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_compat_endspent, .-_nss_compat_endspent
	.p2align 4,,15
	.globl	_nss_compat_getspent_r
	.type	_nss_compat_getspent_r, @function
_nss_compat_getspent_r:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L208
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L208:
	cmpq	$0, ni(%rip)
	je	.L228
.L209:
	cmpq	$0, 8+ext_ent(%rip)
	je	.L210
	cmpb	$0, ext_ent(%rip)
	jne	.L229
.L226:
	cmpb	$0, 1+ext_ent(%rip)
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	jne	.L230
	call	getspent_next_nss.constprop.7
	movl	%eax, %ebx
.L214:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L207
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L207:
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	call	getspent_next_file.constprop.6
	movl	%eax, %ebx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L210:
	leaq	ext_ent(%rip), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	internal_setspent
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L214
	cmpb	$0, ext_ent(%rip)
	je	.L226
	.p2align 4,,10
	.p2align 3
.L229:
	xorl	%esi, %esi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rdi
	call	getspent_next_nss_netgr.constprop.8
	cmpl	$2, %eax
	movl	%eax, %ebx
	jne	.L214
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	getspent_next_file.constprop.6
	movl	%eax, %ebx
	jmp	.L214
	.p2align 4,,10
	.p2align 3
.L228:
	call	init_nss_interface
	jmp	.L209
	.size	_nss_compat_getspent_r, .-_nss_compat_getspent_r
	.p2align 4,,15
	.globl	_nss_compat_getspnam_r
	.type	_nss_compat_getspnam_r, @function
_nss_compat_getspnam_r:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r9
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r13
	movl	$24, %ecx
	xorl	%ebp, %ebp
	subq	$264, %rsp
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	rep stosq
	movzbl	(%r9), %eax
	movb	$1, 65(%rsp)
	movl	$1, 68(%rsp)
	subl	$43, %eax
	testb	$-3, %al
	je	.L231
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%r8, 24(%rsp)
	movq	%rdx, %r12
	movq	%rsi, 32(%rsp)
	movq	%r9, 40(%rsp)
	je	.L233
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L233:
	cmpq	$0, ni(%rip)
	je	.L294
.L234:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L235
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L235:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	internal_setspent
	cmpl	$1, %eax
	movl	%eax, %ebp
	je	.L295
.L236:
	movq	errno@gottpoff(%rip), %r12
	movq	%rbx, %rdi
	movl	%fs:(%r12), %r13d
	call	internal_endspent
	movl	%r13d, %fs:(%r12)
.L231:
	addq	$264, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L295:
	leaq	48(%rsp), %rax
	leaq	-1(%r12,%r13), %r15
	movq	%rax, 8(%rsp)
.L256:
	movl	%r13d, 20(%rsp)
.L247:
	cmpq	$2, %r13
	jbe	.L238
	.p2align 4,,10
	.p2align 3
.L277:
	movq	8(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	fgetpos@PLT
	movq	72(%rsp), %rdx
	movl	20(%rsp), %esi
	movq	%r12, %rdi
	movb	$-1, (%r15)
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L296
	cmpb	$-1, (%r15)
	jne	.L291
	movb	$0, (%r15)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L245:
	addq	$1, %r14
.L244:
	movsbq	(%r14), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L245
	testb	%dl, %dl
	je	.L277
	cmpb	$35, %dl
	je	.L277
	movq	24(%rsp), %r8
	movq	32(%rsp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_nss_files_parse_spent@PLT
	testl	%eax, %eax
	je	.L247
	addl	$1, %eax
	je	.L291
	movq	32(%rsp), %rax
	movq	(%rax), %rdi
	movzbl	(%rdi), %eax
	leal	-43(%rax), %edx
	andb	$-3, %dl
	je	.L249
	movq	40(%rsp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L236
	jmp	.L256
	.p2align 4,,10
	.p2align 3
.L294:
	call	init_nss_interface
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L296:
	movq	72(%rsp), %rdi
	testb	$16, (%rdi)
	jne	.L241
.L242:
	movq	8(%rsp), %rsi
	call	fsetpos@PLT
.L238:
	movq	24(%rsp), %rax
	movl	$-2, %ebp
	movl	$34, (%rax)
	jmp	.L236
.L291:
	movq	72(%rsp), %rdi
	jmp	.L242
.L298:
	movzbl	1(%rdi), %eax
	cmpb	$64, %al
	je	.L297
	testb	$-65, %al
	je	.L256
	movq	40(%rsp), %rsi
	addq	$1, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L256
.L241:
	xorl	%ebp, %ebp
	jmp	.L236
.L249:
	cmpb	$45, %al
	je	.L298
	cmpb	$43, %al
	jne	.L256
	movzbl	1(%rdi), %eax
	cmpb	$64, %al
	je	.L299
	testb	$-65, %al
	jne	.L300
	testb	%al, %al
	jne	.L256
	movq	24(%rsp), %r9
	movq	32(%rsp), %rsi
	movq	%r13, %r8
	movq	40(%rsp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	call	getspnam_plususer
	cmpl	$1, %eax
	movl	%eax, %ebp
	je	.L236
.L292:
	cmpl	$2, %ebp
	jne	.L236
	xorl	%ebp, %ebp
	jmp	.L236
.L300:
	leaq	1(%rdi), %rsi
	movq	40(%rsp), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L256
	movq	24(%rsp), %r9
	movq	32(%rsp), %rsi
	movq	%r13, %r8
	movq	40(%rsp), %rdi
	movq	%r12, %rcx
	movq	%rbx, %rdx
	call	getspnam_plususer
	movl	%eax, %ebp
	jmp	.L292
.L299:
	cmpb	$0, 2(%rdi)
	je	.L256
	movq	40(%rsp), %r14
	addq	$2, %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r14, %rdx
	call	innetgr@PLT
	testl	%eax, %eax
	je	.L256
	movq	24(%rsp), %r9
	movq	32(%rsp), %rsi
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	getspnam_plususer
	cmpl	$2, %eax
	je	.L256
	movl	%eax, %ebp
	jmp	.L236
.L297:
	cmpb	$0, 2(%rdi)
	je	.L256
	movq	40(%rsp), %rdx
	addq	$2, %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	innetgr@PLT
	testl	%eax, %eax
	je	.L256
	xorl	%ebp, %ebp
	jmp	.L236
	.size	_nss_compat_getspnam_r, .-_nss_compat_getspnam_r
	.local	lock
	.comm	lock,40,32
	.data
	.align 32
	.type	ext_ent, @object
	.size	ext_ent, 192
ext_ent:
	.byte	0
	.byte	1
	.byte	0
	.zero	1
	.long	1
	.quad	0
	.quad	0
	.long	0
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	88
	.local	endspent_impl
	.comm	endspent_impl,8,8
	.local	getspent_r_impl
	.comm	getspent_r_impl,8,8
	.local	getspnam_r_impl
	.comm	getspnam_r_impl,8,8
	.local	setspent_impl
	.comm	setspent_impl,8,8
	.local	ni
	.comm	ni,8,8
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
