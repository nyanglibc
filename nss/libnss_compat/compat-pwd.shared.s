	.text
	.p2align 4,,15
	.type	give_pwd_free, @function
give_pwd_free:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	free@PLT
	movq	8(%rbx), %rdi
	call	free@PLT
	movq	24(%rbx), %rdi
	call	free@PLT
	movq	32(%rbx), %rdi
	call	free@PLT
	movq	40(%rbx), %rdi
	call	free@PLT
	pxor	%xmm0, %xmm0
	movaps	%xmm0, (%rbx)
	movaps	%xmm0, 16(%rbx)
	movaps	%xmm0, 32(%rbx)
	popq	%rbx
	ret
	.size	give_pwd_free, .-give_pwd_free
	.p2align 4,,15
	.type	internal_endpwent, @function
internal_endpwent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L5
	call	fclose@PLT
	movq	$0, 8(%rbx)
.L5:
	cmpb	$0, (%rbx)
	jne	.L13
.L6:
	movq	16(%rbx), %rax
	movb	$0, (%rbx)
	movb	$0, 1(%rbx)
	testq	%rax, %rax
	je	.L7
	movl	$1, 24(%rbx)
	movb	$124, (%rax)
	leaq	32(%rbx), %rdi
	movq	16(%rbx), %rax
	movb	$0, 1(%rax)
	call	give_pwd_free
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	32(%rbx), %rdi
	movl	$0, 24(%rbx)
	call	give_pwd_free
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leaq	80(%rbx), %rdi
	call	__internal_endnetgrent@PLT
	jmp	.L6
	.size	internal_endpwent, .-internal_endpwent
	.p2align 4,,15
	.type	pwd_need_buflen, @function
pwd_need_buflen:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	xorl	%ebx, %ebx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L15
	call	strlen@PLT
	leaq	1(%rax), %rbx
.L15:
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L16
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
.L16:
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L17
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
.L17:
	movq	40(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rbx
.L14:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.size	pwd_need_buflen, .-pwd_need_buflen
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"nis"
.LC1:
	.string	"passwd_compat"
.LC2:
	.string	"setpwent"
.LC3:
	.string	"getpwnam_r"
.LC4:
	.string	"getpwuid_r"
.LC5:
	.string	"getpwent_r"
.LC6:
	.string	"endpwent"
	.text
	.p2align 4,,15
	.type	init_nss_interface, @function
init_nss_interface:
	leaq	ni(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	xorl	%esi, %esi
	call	__nss_database_lookup2@PLT
	testl	%eax, %eax
	js	.L32
	movq	ni(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, setpwent_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, getpwnam_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, getpwuid_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, getpwent_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	%rax, endpwent_impl(%rip)
.L32:
	addq	$8, %rsp
	ret
	.size	init_nss_interface, .-init_nss_interface
	.section	.rodata.str1.1
.LC7:
	.string	"/etc/passwd"
	.text
	.p2align 4,,15
	.type	internal_setpwent, @function
internal_setpwent:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %rax
	movb	$0, (%rdi)
	movb	$0, 1(%rdi)
	movb	$1, 2(%rdi)
	movl	$1, 4(%rdi)
	testq	%rax, %rax
	je	.L36
	movl	$1, 24(%rdi)
	movb	$124, (%rax)
	movq	16(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L47
.L38:
	call	rewind@PLT
.L39:
	leaq	32(%rbx), %rdi
	call	give_pwd_free
	testl	%ebp, %ebp
	movl	$1, %eax
	je	.L35
	movq	setpwent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L44
	movl	%r12d, %edi
	call	*%rax
	movl	%eax, 4(%rbx)
	movl	%ebp, %eax
.L35:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$0, 24(%rdi)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L38
.L47:
	leaq	.LC7(%rip), %rdi
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	jne	.L39
	movq	errno@gottpoff(%rip), %rax
	leaq	32(%rbx), %rdi
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	movl	%eax, 8(%rsp)
	movl	%eax, 12(%rsp)
	call	give_pwd_free
	movl	8(%rsp), %eax
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L44:
	movl	%ebp, %eax
	jmp	.L35
	.size	internal_setpwent, .-internal_setpwent
	.p2align 4,,15
	.type	in_blacklist.isra.2, @function
in_blacklist.isra.2:
	pushq	%rbp
	leal	3(%rsi), %eax
	movq	%rsp, %rbp
	pushq	%rbx
	cltq
	addq	$15, %rax
	subq	$8, %rsp
	movq	(%rdx), %rbx
	andq	$-16, %rax
	subq	%rax, %rsp
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L48
	movq	%rdi, %rsi
	leaq	1(%rsp), %rdi
	movb	$124, (%rsp)
	call	__stpcpy@PLT
	movl	$124, %edx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movw	%dx, (%rax)
	call	strstr@PLT
	testq	%rax, %rax
	setne	%al
.L48:
	movq	-8(%rbp), %rbx
	leave
	ret
	.size	in_blacklist.isra.2, .-in_blacklist.isra.2
	.p2align 4,,15
	.type	copy_pwd_changes.isra.3, @function
copy_pwd_changes.isra.3:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	8(%rsi), %r13
	testq	%r13, %r13
	je	.L55
	cmpb	$0, 0(%r13)
	je	.L55
	testq	%rdx, %rdx
	je	.L130
	movq	8(%rdi), %r14
	testq	%r14, %r14
	je	.L64
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	cmpq	%rax, %r15
	jnb	.L131
.L64:
	movq	%rbp, 8(%r12)
	movq	8(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcpy@PLT
	movq	8(%r12), %rdi
	call	strlen@PLT
	leaq	1(%rbp,%rax), %rbp
.L55:
	movq	24(%rbx), %r14
	testq	%r14, %r14
	je	.L65
	cmpb	$0, (%r14)
	je	.L65
	testq	%rbp, %rbp
	je	.L132
	movq	24(%r12), %r13
	testq	%r13, %r13
	je	.L70
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	cmpq	%rax, %r15
	jnb	.L133
.L70:
	movq	%rbp, 24(%r12)
	movq	24(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcpy@PLT
	movq	24(%r12), %rdi
	call	strlen@PLT
	leaq	1(%rbp,%rax), %rbp
.L65:
	movq	32(%rbx), %r15
	testq	%r15, %r15
	je	.L118
	cmpb	$0, (%r15)
	je	.L118
	testq	%rbp, %rbp
	je	.L134
	movq	32(%r12), %r13
	testq	%r13, %r13
	je	.L75
	movq	%r13, %rdi
	call	strlen@PLT
	movq	%r15, %rdi
	movq	%rax, %r14
	call	strlen@PLT
	cmpq	%rax, %r14
	jnb	.L135
.L75:
	movq	%rbp, 32(%r12)
	movq	32(%rbx), %rsi
	movq	%rbp, %rdi
	call	strcpy@PLT
	movq	32(%r12), %rdi
	call	strlen@PLT
	leaq	1(%rbp,%rax), %rbp
.L118:
	movq	40(%rbx), %r13
.L72:
	testq	%r13, %r13
	je	.L54
	cmpb	$0, 0(%r13)
	je	.L54
	testq	%rbp, %rbp
	je	.L60
	movq	40(%r12), %r14
	testq	%r14, %r14
	je	.L77
	movq	%r14, %rdi
	call	strlen@PLT
	movq	%r13, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	cmpq	%rax, %r15
	jnb	.L136
.L77:
	movq	%rbp, 40(%r12)
	movq	40(%rbx), %rsi
	addq	$8, %rsp
	popq	%rbx
	movq	%rbp, %rdi
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	strcpy@PLT
	.p2align 4,,10
	.p2align 3
.L134:
	movq	40(%rbx), %r13
.L61:
	movq	%r15, %rdi
	call	strdup@PLT
	movq	%rax, 32(%r12)
.L121:
	testq	%r13, %r13
	je	.L54
	cmpb	$0, 0(%r13)
	je	.L54
.L60:
	movq	%r13, %rdi
	call	strdup@PLT
	movq	%rax, 40(%r12)
.L54:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	addq	$8, %rsp
	movq	%r13, %rsi
	movq	%r14, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	leaq	1(%rax), %rdx
	jmp	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L131:
	leaq	1(%rax), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	memcpy@PLT
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L135:
	leaq	1(%rax), %rdx
	movq	%r13, %rdi
	movq	%r15, %rsi
	call	memcpy@PLT
	movq	40(%rbx), %r13
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L133:
	leaq	1(%rax), %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	memcpy@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L130:
	movq	%r13, %rdi
	call	strdup@PLT
	movq	24(%rbx), %r14
	movq	%rax, 8(%r12)
	movq	32(%rbx), %r15
	movq	40(%rbx), %r13
	testq	%r14, %r14
	je	.L129
	cmpb	$0, (%r14)
	je	.L129
.L62:
	movq	%r14, %rdi
	call	strdup@PLT
	movq	%rax, 24(%r12)
.L129:
	testq	%r15, %r15
	je	.L121
	cmpb	$0, (%r15)
	jne	.L61
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L132:
	movq	32(%rbx), %r15
	movq	40(%rbx), %r13
	jmp	.L62
	.size	copy_pwd_changes.isra.3, .-copy_pwd_changes.isra.3
	.section	.text.unlikely,"ax",@progbits
	.type	getpwnam_plususer, @function
getpwnam_plususer:
	pushq	%r15
	pushq	%r14
	orl	$-1, %r15d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	getpwnam_r_impl(%rip), %r14
	testq	%r14, %r14
	je	.L137
	leaq	16(%rsp), %rbp
	xorl	%eax, %eax
	movq	%rcx, %r13
	movq	%rdi, %r15
	movl	$12, %ecx
	movq	%rdx, (%rsp)
	movq	%rbp, %rdi
	xorl	%edx, %edx
	movq	%r8, %rbx
	rep stosl
	movq	%rbp, %rdi
	movq	%r9, 8(%rsp)
	movq	%rsi, %r12
	call	copy_pwd_changes.isra.3
	movq	%rbp, %rdi
	call	pwd_need_buflen
	cmpq	%rbx, %rax
	movq	8(%rsp), %r8
	jbe	.L139
	movl	$34, (%r8)
	movl	$-2, %r15d
	jmp	.L137
.L139:
	subq	%rax, %rbx
	movq	%r15, %rdi
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%r12, %rsi
	call	*%r14
	cmpl	$1, %eax
	movl	%eax, %r15d
	jne	.L137
	movq	(%r12), %r8
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	(%rsp), %rdx
	movq	%r8, %rdi
	repnz scasb
	addq	$16, %rdx
	movq	%r8, %rdi
	movq	%rcx, %rsi
	notq	%rsi
	leaq	-1(%rsi), %rsi
	call	in_blacklist.isra.2
	testb	%al, %al
	jne	.L141
	leaq	0(%r13,%rbx), %rdx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	call	copy_pwd_changes.isra.3
	movq	%rbp, %rdi
	call	give_pwd_free
	jmp	.L137
.L141:
	xorl	%r15d, %r15d
.L137:
	addq	$72, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	getpwnam_plususer, .-getpwnam_plususer
	.type	getpwuid_plususer, @function
getpwuid_plususer:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	orl	$-1, %ebp
	subq	$72, %rsp
	movq	getpwuid_r_impl(%rip), %r9
	testq	%r9, %r9
	movq	%r9, 8(%rsp)
	je	.L143
	leaq	16(%rsp), %rbx
	xorl	%eax, %eax
	movq	%rcx, %r12
	movl	%edi, %ebp
	movl	$12, %ecx
	movq	%rdx, %r13
	movq	%rbx, %rdi
	xorl	%edx, %edx
	movq	%r8, %r15
	rep stosl
	movq	%rbx, %rdi
	movq	%rsi, %r14
	call	copy_pwd_changes.isra.3
	movq	%rbx, %rdi
	call	pwd_need_buflen
	cmpq	%r12, %rax
	movq	8(%rsp), %r9
	jbe	.L145
	movl	$34, (%r15)
	movl	$-2, %ebp
	jmp	.L143
.L145:
	subq	%rax, %r12
	movl	%ebp, %edi
	movq	%r15, %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	call	*%r9
	cmpl	$1, %eax
	movl	%eax, %ebp
	jne	.L146
	leaq	0(%r13,%r12), %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	call	copy_pwd_changes.isra.3
	movq	%rbx, %rdi
	call	give_pwd_free
	jmp	.L143
.L146:
	movq	%rbx, %rdi
	movl	$2, %ebp
	call	give_pwd_free
.L143:
	addq	$72, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	getpwuid_plususer, .-getpwuid_plususer
	.text
	.p2align 4,,15
	.type	blacklist_store_name.constprop.9, @function
blacklist_store_name.constprop.9:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	strlen@PLT
	movl	28+ext_ent(%rip), %esi
	movq	%rax, %rbp
	testl	%esi, %esi
	jne	.L150
	leal	(%rax,%rax), %edi
	movl	$512, %eax
	cmpl	$512, %edi
	cmovl	%eax, %edi
	movl	%edi, 28+ext_ent(%rip)
	movslq	%edi, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16+ext_ent(%rip)
	je	.L149
	movl	$124, %ecx
	movl	$1, 24+ext_ent(%rip)
	movl	$1, %edi
	movw	%cx, (%rax)
.L153:
	addq	%rax, %rdi
	movq	%rbx, %rsi
	call	__stpcpy@PLT
	movl	$124, %edx
	movw	%dx, (%rax)
	movl	24+ext_ent(%rip), %eax
	leal	1(%rax,%rbp), %eax
	movl	%eax, 24+ext_ent(%rip)
.L149:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	in_blacklist.isra.2
	testb	%al, %al
	jne	.L149
	movslq	24+ext_ent(%rip), %rdi
	movl	28+ext_ent(%rip), %edx
	movq	16+ext_ent(%rip), %rax
	leal	1(%rdi,%rbp), %ecx
	cmpl	%edx, %ecx
	jl	.L153
	leal	(%rbp,%rbp), %esi
	movl	$256, %ecx
	movq	%rax, %rdi
	cmpl	$256, %esi
	cmovl	%ecx, %esi
	addl	%edx, %esi
	movl	%esi, 28+ext_ent(%rip)
	movslq	%esi, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L158
	movq	%rax, 16+ext_ent(%rip)
	movslq	24+ext_ent(%rip), %rdi
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L158:
	movq	16+ext_ent(%rip), %rdi
	call	free@PLT
	movl	$0, 28+ext_ent(%rip)
	jmp	.L149
	.size	blacklist_store_name.constprop.9, .-blacklist_store_name.constprop.9
	.p2align 4,,15
	.type	getpwent_next_nss_netgr.constprop.8, @function
getpwent_next_nss_netgr.constprop.8:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	cmpq	$0, getpwnam_r_impl(%rip)
	movq	%rdi, (%rsp)
	movq	$0, 32(%rsp)
	je	.L171
	cmpb	$0, 1+ext_ent(%rip)
	movq	%rdx, %r12
	movq	%rcx, %rbx
	movq	%r8, %rbp
	jne	.L177
.L161:
	leaq	32(%rsp), %rax
	leaq	56(%rsp), %r15
	leaq	48(%rsp), %r14
	leaq	40(%rsp), %r13
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L162:
	subq	$8, %rsp
	leaq	80+ext_ent(%rip), %rcx
	movq	%r15, %rdx
	pushq	%rbp
	movq	%rbx, %r9
	movq	%r12, %r8
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__internal_getnetgrent_r@PLT
	cmpl	$1, %eax
	popq	%rdx
	popq	%rcx
	jne	.L178
	movq	48(%rsp), %rax
	testq	%rax, %rax
	je	.L162
	cmpb	$45, (%rax)
	je	.L162
	movq	56(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L170
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L179
.L168:
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L162
.L170:
	leaq	32+ext_ent(%rip), %rdi
	call	pwd_need_buflen
	cmpq	%rax, %rbx
	jb	.L180
	subq	%rax, %rbx
	movq	%rbp, %r8
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	*getpwnam_r_impl(%rip)
	cmpl	$1, %eax
	jne	.L162
	movl	%eax, 20(%rsp)
	movq	(%rsp), %rax
	movq	(%rax), %r8
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	strlen@PLT
	movq	8(%rsp), %r8
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%r8, %rdi
	call	in_blacklist.isra.2
	testb	%al, %al
	movl	20(%rsp), %ecx
	jne	.L162
	movq	(%rsp), %r14
	movl	%ecx, 8(%rsp)
	movq	(%r14), %rdi
	call	blacklist_store_name.constprop.9
	leaq	(%r12,%rbx), %rdx
	leaq	32+ext_ent(%rip), %rsi
	movq	%r14, %rdi
	call	copy_pwd_changes.isra.3
	movl	8(%rsp), %ecx
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L179:
	movq	24(%rsp), %rdi
	call	__nss_get_default_domain@PLT
	testl	%eax, %eax
	jne	.L169
	movq	56(%rsp), %rsi
	movq	32(%rsp), %rdi
	jmp	.L168
	.p2align 4,,10
	.p2align 3
.L178:
	leaq	80+ext_ent(%rip), %rdi
	call	__internal_endnetgrent@PLT
	leaq	32+ext_ent(%rip), %rdi
	movb	$0, ext_ent(%rip)
	call	give_pwd_free
	movl	$2, %ecx
.L159:
	addq	$72, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%rsi, %r9
	leaq	80+ext_ent(%rip), %rsi
	xorl	%eax, %eax
	movl	$11, %ecx
	movq	%rsi, %rdi
	rep stosq
	movq	%r9, %rdi
	call	__internal_setnetgrent@PLT
	movb	$0, 1+ext_ent(%rip)
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$34, 0(%rbp)
	movl	$-2, %ecx
	jmp	.L159
.L169:
	leaq	80+ext_ent(%rip), %rdi
	call	__internal_endnetgrent@PLT
	leaq	32+ext_ent(%rip), %rdi
	movb	$0, ext_ent(%rip)
	call	give_pwd_free
	movl	$-1, %ecx
	jmp	.L159
.L171:
	movl	$-1, %ecx
	jmp	.L159
	.size	getpwent_next_nss_netgr.constprop.8, .-getpwent_next_nss_netgr.constprop.8
	.p2align 4,,15
	.type	getpwent_next_nss.part.4.constprop.10, @function
getpwent_next_nss.part.4.constprop.10:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	leaq	32+ext_ent(%rip), %rdi
	movq	%rsi, %r13
	subq	$8, %rsp
	call	pwd_need_buflen
	cmpq	%r12, %rax
	ja	.L188
	subq	%rax, %r12
	cmpb	$0, 1+ext_ent(%rip)
	je	.L185
	movb	$0, 1+ext_ent(%rip)
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L190:
	movq	0(%rbp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%r15, %rdi
	call	in_blacklist.isra.2
	testb	%al, %al
	je	.L189
.L185:
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	*getpwent_r_impl(%rip)
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L190
.L181:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	0(%r13,%r12), %rdx
	leaq	32+ext_ent(%rip), %rsi
	movq	%rbp, %rdi
	call	copy_pwd_changes.isra.3
	jmp	.L181
.L188:
	movl	$34, (%r14)
	movl	$-2, %ebx
	jmp	.L181
	.size	getpwent_next_nss.part.4.constprop.10, .-getpwent_next_nss.part.4.constprop.10
	.p2align 4,,15
	.type	getpwent_next_file.constprop.7, @function
getpwent_next_file.constprop.7:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	-1184(%rbp), %r13
	pushq	%rbx
	leaq	-1(%rsi,%rdx), %rbx
	movq	%rsi, %r12
	subq	$1224, %rsp
	movq	%rdi, -1240(%rbp)
	movq	%rdx, -1224(%rbp)
	movq	%rcx, -1232(%rbp)
.L216:
	movl	-1224(%rbp), %r14d
.L201:
	cmpq	$2, -1224(%rbp)
	jbe	.L193
.L227:
	movq	8+ext_ent(%rip), %rdi
	movq	%r13, %rsi
	call	fgetpos@PLT
	movb	$-1, (%rbx)
	movq	8+ext_ent(%rip), %rdx
	movl	%r14d, %esi
	movq	%r12, %rdi
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L235
	cmpb	$-1, (%rbx)
	jne	.L234
	movb	$0, (%rbx)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L199:
	addq	$1, %r15
.L198:
	movsbq	(%r15), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L199
	testb	%dl, %dl
	je	.L227
	cmpb	$35, %dl
	je	.L227
	movq	-1232(%rbp), %r8
	movq	-1224(%rbp), %rcx
	movq	%r12, %rdx
	movq	-1240(%rbp), %rsi
	movq	%r15, %rdi
	call	_nss_files_parse_pwent@PLT
	testl	%eax, %eax
	je	.L201
	cmpl	$-1, %eax
	je	.L234
	movq	-1240(%rbp), %rax
	movq	(%rax), %rsi
	movzbl	(%rsi), %eax
	leal	-43(%rax), %edx
	andl	$253, %edx
	jne	.L203
	cmpb	$45, %al
	je	.L236
	cmpb	$43, %al
	jne	.L216
	movzbl	1(%rsi), %eax
	cmpb	$64, %al
	je	.L237
	testb	$-65, %al
	jne	.L238
	testb	%al, %al
	jne	.L216
	movq	-1240(%rbp), %rsi
	leaq	32+ext_ent(%rip), %rdi
	xorl	%edx, %edx
	movb	$0, 2+ext_ent(%rip)
	movb	$1, 1+ext_ent(%rip)
	orl	$-1, %r14d
	call	copy_pwd_changes.isra.3
	cmpq	$0, getpwent_r_impl(%rip)
	je	.L191
	movl	4+ext_ent(%rip), %r14d
	cmpl	$1, %r14d
	jne	.L191
	movq	-1232(%rbp), %rcx
	movq	-1224(%rbp), %rdx
	movq	%r12, %rsi
	movq	-1240(%rbp), %rdi
	call	getpwent_next_nss.part.4.constprop.10
	movl	%eax, %r14d
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L235:
	movq	8+ext_ent(%rip), %rdi
	testb	$16, (%rdi)
	jne	.L239
.L196:
	movq	%r13, %rsi
	call	fsetpos@PLT
.L193:
	movq	-1232(%rbp), %rax
	movl	$-2, %r14d
	movl	$34, (%rax)
.L191:
	leaq	-40(%rbp), %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	movq	8+ext_ent(%rip), %rdi
	jmp	.L196
.L239:
	xorl	%r14d, %r14d
	jmp	.L191
.L238:
	xorl	%eax, %eax
	movq	%rsi, %rdi
	orq	$-1, %rcx
	repnz scasb
	leaq	1(%rsi), %r14
	movq	%rsp, %r15
	movq	%rcx, %rax
	notq	%rax
	movq	%r14, %rsi
	leaq	-1(%rax), %rdx
	addq	$14, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-1232(%rbp), %r9
	movq	-1224(%rbp), %r8
	leaq	ext_ent(%rip), %rdx
	movq	-1240(%rbp), %rsi
	movq	%r14, %rdi
	movq	%r12, %rcx
	call	getpwnam_plususer
	movq	%rsp, %rdi
	movl	%eax, %r14d
	call	blacklist_store_name.constprop.9
	cmpl	$1, %r14d
	je	.L213
	testl	$-3, %r14d
	je	.L240
	cmpl	$-2, %r14d
	jne	.L215
	movq	8+ext_ent(%rip), %rdi
	movq	%r13, %rsi
	call	fsetpos@PLT
	movq	-1232(%rbp), %rax
	movl	$34, (%rax)
.L215:
	movq	%r15, %rsp
	jmp	.L191
.L240:
	movq	%r15, %rsp
	jmp	.L216
.L213:
	movq	%r15, %rsp
.L203:
	movl	$1, %r14d
	jmp	.L191
.L236:
	movzbl	1(%rsi), %eax
	cmpb	$64, %al
	je	.L241
	testb	$-65, %al
	je	.L216
	leaq	1(%rsi), %rdi
	call	blacklist_store_name.constprop.9
	jmp	.L216
.L237:
	cmpb	$0, 2(%rsi)
	je	.L216
	movq	-1240(%rbp), %r14
	leaq	32+ext_ent(%rip), %rdi
	xorl	%edx, %edx
	movb	$1, ext_ent(%rip)
	movb	$1, 1+ext_ent(%rip)
	movq	%r14, %rsi
	call	copy_pwd_changes.isra.3
	movq	(%r14), %rax
	movq	-1232(%rbp), %r8
	movq	%r14, %rdi
	movq	-1224(%rbp), %rcx
	movq	%r12, %rdx
	leaq	2(%rax), %rsi
	movq	%rax, -1248(%rbp)
	call	getpwent_next_nss_netgr.constprop.8
	cmpl	$2, %eax
	movl	%eax, %r14d
	jne	.L191
	jmp	.L216
.L241:
	cmpb	$0, 2(%rsi)
	je	.L216
	leaq	-1168(%rbp), %r14
	xorl	%eax, %eax
	movl	$22, %ecx
	leaq	-1072(%rbp), %r15
	movq	%r14, %rdi
	rep stosl
	leaq	2(%rsi), %rdi
	movq	%r14, %rsi
	call	__internal_setnetgrent@PLT
	leaq	-1192(%rbp), %rax
	movq	%rax, -1264(%rbp)
	leaq	-1208(%rbp), %rax
	movq	%rax, -1248(%rbp)
	leaq	-1200(%rbp), %rax
	movq	%rax, -1256(%rbp)
.L207:
	movq	-1264(%rbp), %rdx
	movq	-1248(%rbp), %rsi
	subq	$8, %rsp
	movq	-1256(%rbp), %rdi
	pushq	-1232(%rbp)
	movq	%r14, %rcx
	movl	$1024, %r9d
	movq	%r15, %r8
	call	__internal_getnetgrent_r@PLT
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L242
	movq	-1208(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L207
	cmpb	$45, (%rdi)
	je	.L207
	call	blacklist_store_name.constprop.9
	jmp	.L207
.L242:
	movq	%r14, %rdi
	call	__internal_endnetgrent@PLT
	jmp	.L216
	.size	getpwent_next_file.constprop.7, .-getpwent_next_file.constprop.7
	.p2align 4,,15
	.globl	_nss_compat_setpwent
	.type	_nss_compat_setpwent, @function
_nss_compat_setpwent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	movl	%edi, %ebx
	je	.L244
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L244:
	cmpq	$0, ni(%rip)
	je	.L254
.L245:
	leaq	ext_ent(%rip), %rdi
	movl	%ebx, %esi
	movl	$1, %edx
	call	internal_setpwent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L243
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L243:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L254:
	call	init_nss_interface
	jmp	.L245
	.size	_nss_compat_setpwent, .-_nss_compat_setpwent
	.p2align 4,,15
	.globl	_nss_compat_endpwent
	.type	_nss_compat_endpwent, @function
_nss_compat_endpwent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L256
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L256:
	movq	endpwent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L257
	call	*%rax
.L257:
	leaq	ext_ent(%rip), %rdi
	call	internal_endpwent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L255
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L255:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_compat_endpwent, .-_nss_compat_endpwent
	.p2align 4,,15
	.globl	_nss_compat_getpwent_r
	.type	_nss_compat_getpwent_r, @function
_nss_compat_getpwent_r:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	je	.L270
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L270:
	cmpq	$0, ni(%rip)
	je	.L292
.L271:
	cmpq	$0, 8+ext_ent(%rip)
	je	.L272
.L275:
	cmpb	$0, ext_ent(%rip)
	jne	.L293
	cmpb	$0, 2+ext_ent(%rip)
	jne	.L291
	cmpq	$0, getpwent_r_impl(%rip)
	je	.L279
	movl	4+ext_ent(%rip), %ebx
	cmpl	$1, %ebx
	je	.L294
.L276:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L269
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L269:
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L293:
	xorl	%esi, %esi
	movq	%r14, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rdi
	call	getpwent_next_nss_netgr.constprop.8
	cmpl	$2, %eax
	movl	%eax, %ebx
	jne	.L276
.L291:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	getpwent_next_file.constprop.7
	movl	%eax, %ebx
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L272:
	leaq	ext_ent(%rip), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	internal_setpwent
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L276
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L292:
	call	init_nss_interface
	jmp	.L271
	.p2align 4,,10
	.p2align 3
.L294:
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	getpwent_next_nss.part.4.constprop.10
	movl	%eax, %ebx
	jmp	.L276
.L279:
	movl	$-1, %ebx
	jmp	.L276
	.size	_nss_compat_getpwent_r, .-_nss_compat_getpwent_r
	.p2align 4,,15
	.globl	_nss_compat_getpwnam_r
	.type	_nss_compat_getpwnam_r, @function
_nss_compat_getpwnam_r:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r9
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movl	$21, %ecx
	xorl	%r14d, %r14d
	subq	$248, %rsp
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	rep stosq
	movzbl	(%r9), %eax
	movb	$1, 66(%rsp)
	movl	$1, 68(%rsp)
	subl	$43, %eax
	testb	$-3, %al
	je	.L295
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%r8, 8(%rsp)
	movq	%rdx, %rbp
	movq	%rsi, 24(%rsp)
	movq	%r9, 32(%rsp)
	je	.L297
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L297:
	cmpq	$0, ni(%rip)
	je	.L357
.L298:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L299
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L299:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	internal_setpwent
	cmpl	$1, %eax
	movl	%eax, %r14d
	je	.L358
.L300:
	movq	errno@gottpoff(%rip), %rbp
	movq	%rbx, %rdi
	movl	%fs:0(%rbp), %r12d
	call	internal_endpwent
	movl	%r12d, %fs:0(%rbp)
.L295:
	addq	$248, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L358:
	leaq	48(%rsp), %rax
	leaq	-1(%rbp,%r12), %r15
	movq	%rax, 16(%rsp)
.L320:
	movl	%r12d, 44(%rsp)
.L311:
	cmpq	$2, %r12
	jbe	.L302
.L341:
	movq	16(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	fgetpos@PLT
	movq	72(%rsp), %rdx
	movl	44(%rsp), %esi
	movq	%rbp, %rdi
	movb	$-1, (%r15)
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L359
	cmpb	$-1, (%r15)
	jne	.L355
	movb	$0, (%r15)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L308
	.p2align 4,,10
	.p2align 3
.L309:
	addq	$1, %r13
.L308:
	movsbq	0(%r13), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L309
	testb	%dl, %dl
	je	.L341
	cmpb	$35, %dl
	je	.L341
	movq	8(%rsp), %r8
	movq	24(%rsp), %rsi
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%r13, %rdi
	call	_nss_files_parse_pwent@PLT
	testl	%eax, %eax
	je	.L311
	addl	$1, %eax
	je	.L355
	movq	24(%rsp), %rax
	movq	(%rax), %rdi
	movzbl	(%rdi), %eax
	leal	-43(%rax), %edx
	andb	$-3, %dl
	je	.L313
	movq	32(%rsp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L300
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L357:
	call	init_nss_interface
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L359:
	movq	72(%rsp), %rdi
	testb	$16, (%rdi)
	jne	.L305
.L306:
	movq	16(%rsp), %rsi
	call	fsetpos@PLT
.L302:
	movq	8(%rsp), %rax
	movl	$-2, %r14d
	movl	$34, (%rax)
	jmp	.L300
	.p2align 4,,10
	.p2align 3
.L355:
	movq	72(%rsp), %rdi
	jmp	.L306
.L361:
	movzbl	1(%rdi), %eax
	cmpb	$64, %al
	je	.L360
	testb	$-65, %al
	je	.L320
	movq	32(%rsp), %rsi
	addq	$1, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L320
.L305:
	xorl	%r14d, %r14d
	jmp	.L300
.L313:
	cmpb	$45, %al
	je	.L361
	cmpb	$43, %al
	jne	.L320
	movzbl	1(%rdi), %eax
	cmpb	$64, %al
	je	.L362
	testb	$-65, %al
	jne	.L363
	testb	%al, %al
	jne	.L320
	movq	8(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%r12, %r8
	movq	32(%rsp), %rdi
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	call	getpwnam_plususer
	cmpl	$1, %eax
	movl	%eax, %r14d
	je	.L300
.L356:
	cmpl	$2, %r14d
	jne	.L300
	jmp	.L305
.L363:
	leaq	1(%rdi), %rsi
	movq	32(%rsp), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L320
	movq	8(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%r12, %r8
	movq	32(%rsp), %rdi
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	call	getpwnam_plususer
	movl	%eax, %r14d
	jmp	.L356
.L362:
	cmpb	$0, 2(%rdi)
	je	.L320
	movq	32(%rsp), %r13
	addq	$2, %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r13, %rdx
	call	innetgr@PLT
	testl	%eax, %eax
	je	.L320
	movq	8(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%r12, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rdi
	call	getpwnam_plususer
	cmpl	$2, %eax
	je	.L320
	movl	%eax, %r14d
	jmp	.L300
.L360:
	cmpb	$0, 2(%rdi)
	je	.L320
	movq	32(%rsp), %rdx
	addq	$2, %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	call	innetgr@PLT
	testl	%eax, %eax
	jne	.L305
	jmp	.L320
	.size	_nss_compat_getpwnam_r, .-_nss_compat_getpwnam_r
	.p2align 4,,15
	.globl	_nss_compat_getpwuid_r
	.type	_nss_compat_getpwuid_r, @function
_nss_compat_getpwuid_r:
	pushq	%rbp
	xorl	%eax, %eax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r15
	pushq	%rbx
	leaq	-224(%rbp), %rbx
	movl	$21, %ecx
	movq	%rdx, %r14
	subq	$248, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movl	%edi, -268(%rbp)
	movq	%rbx, %rdi
	movq	%rsi, -264(%rbp)
	movq	%r8, -248(%rbp)
	rep stosq
	movb	$1, -222(%rbp)
	movl	$1, -220(%rbp)
	je	.L365
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L365:
	cmpq	$0, ni(%rip)
	je	.L436
.L366:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L367
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L367:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	internal_setpwent
	cmpl	$1, %eax
	movl	%eax, %r12d
	je	.L437
.L368:
	movq	errno@gottpoff(%rip), %r13
	movq	%rbx, %rdi
	movl	%fs:0(%r13), %r14d
	call	internal_endpwent
	movl	%r12d, %eax
	movl	%r14d, %fs:0(%r13)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L437:
	leaq	-240(%rbp), %rax
	leaq	-1(%r14,%r15), %r13
	movl	%r12d, -272(%rbp)
	movq	%rbx, -280(%rbp)
	movq	%rax, -256(%rbp)
.L395:
	movl	%r15d, %r12d
.L379:
	cmpq	$2, %r15
	jbe	.L438
.L417:
	movq	-256(%rbp), %rsi
	movq	-216(%rbp), %rdi
	call	fgetpos@PLT
	movq	-216(%rbp), %rdx
	movb	$-1, 0(%r13)
	movl	%r12d, %esi
	movq	%r14, %rdi
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L439
	cmpb	$-1, 0(%r13)
	jne	.L430
	movb	$0, 0(%r13)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L376
	.p2align 4,,10
	.p2align 3
.L377:
	addq	$1, %rbx
.L376:
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L377
	testb	%dl, %dl
	je	.L417
	cmpb	$35, %dl
	je	.L417
	movq	-248(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	%rbx, %rdi
	call	_nss_files_parse_pwent@PLT
	testl	%eax, %eax
	je	.L379
	addl	$1, %eax
	je	.L430
	movq	-264(%rbp), %rdi
	movq	(%rdi), %rsi
	movzbl	(%rsi), %edx
	leal	-43(%rdx), %eax
	andb	$-3, %al
	je	.L381
	movl	-268(%rbp), %eax
	cmpl	16(%rdi), %eax
	jne	.L395
	movl	-272(%rbp), %r12d
	movq	-280(%rbp), %rbx
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L439:
	movq	-216(%rbp), %rdi
	movq	-280(%rbp), %rbx
	testb	$16, (%rdi)
	jne	.L373
.L374:
	movq	-256(%rbp), %rsi
	call	fsetpos@PLT
.L370:
	movq	-248(%rbp), %rax
	movl	$-2, %r12d
	movl	$34, (%rax)
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L436:
	call	init_nss_interface
	jmp	.L366
	.p2align 4,,10
	.p2align 3
.L430:
	movq	-280(%rbp), %rbx
	movq	-216(%rbp), %rdi
	jmp	.L374
.L438:
	movq	-280(%rbp), %rbx
	jmp	.L370
.L381:
	cmpb	$45, %dl
	je	.L440
	cmpb	$43, %dl
	jne	.L395
	movzbl	1(%rsi), %eax
	cmpb	$64, %al
	je	.L441
	testb	$-65, %al
	jne	.L442
	testb	%al, %al
	jne	.L395
	movq	-248(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%r15, %rcx
	movl	-268(%rbp), %edi
	movq	%r14, %rdx
	movq	-280(%rbp), %rbx
	call	getpwuid_plususer
	cmpl	$1, %eax
	movl	%eax, %r12d
	je	.L368
	cmpl	$2, %eax
	jne	.L368
.L373:
	xorl	%r12d, %r12d
	jmp	.L368
.L440:
	movzbl	1(%rsi), %edx
	cmpb	$64, %dl
	je	.L443
	andb	$-65, %dl
	je	.L395
	movq	%rsp, -288(%rbp)
	movq	%rsi, %rdi
	orq	$-1, %rcx
	repnz scasb
	addq	$1, %rsi
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$14, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %r12
.L435:
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-248(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%r15, %rcx
	movl	-268(%rbp), %edi
	movq	%r14, %rdx
	call	getpwuid_plususer
	subl	$1, %eax
	je	.L444
.L392:
	movq	-288(%rbp), %rsp
	jmp	.L395
.L442:
	movq	%rsp, -288(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rdi
	orq	$-1, %rcx
	addq	$1, %rsi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$14, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-264(%rbp), %rbx
	movq	-248(%rbp), %r8
	movq	%r15, %rcx
	movl	-268(%rbp), %edi
	movq	%r14, %rdx
	movq	%rbx, %rsi
	call	getpwuid_plususer
	cmpl	$2, %eax
	movl	%eax, %r12d
	je	.L392
	cmpl	$1, %eax
	jne	.L393
	movq	(%rbx), %rsi
	movq	%rsp, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L392
.L393:
	movq	-280(%rbp), %rbx
	movq	-288(%rbp), %rsp
	jmp	.L368
.L441:
	cmpb	$0, 2(%rsi)
	je	.L395
	movq	%rsp, -288(%rbp)
	xorl	%eax, %eax
	movq	%rsi, %rdi
	orq	$-1, %rcx
	addq	$2, %rsi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-2(%rax), %rdx
	addq	$13, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-248(%rbp), %r8
	movq	-264(%rbp), %rsi
	movq	%r15, %rcx
	movl	-268(%rbp), %edi
	movq	%r14, %rdx
	call	getpwuid_plususer
	cmpl	$2, %eax
	movl	%eax, %r12d
	je	.L392
	cmpl	$1, %eax
	jne	.L388
	movq	-264(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rdi
	movq	(%rax), %rdx
	call	innetgr@PLT
	testl	%eax, %eax
	je	.L392
.L388:
	movq	-280(%rbp), %rbx
	movq	-288(%rbp), %rsp
	jmp	.L368
.L444:
	movq	-264(%rbp), %rax
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	(%rax), %rdx
	call	innetgr@PLT
	testl	%eax, %eax
	je	.L392
	movq	-280(%rbp), %rbx
	xorl	%r12d, %r12d
	movq	-288(%rbp), %rsp
	jmp	.L368
.L443:
	cmpb	$0, 2(%rsi)
	je	.L395
	movq	%rsp, -288(%rbp)
	movq	%rsi, %rdi
	orq	$-1, %rcx
	repnz scasb
	addq	$2, %rsi
	movq	%rcx, %rax
	notq	%rax
	leaq	-2(%rax), %rdx
	addq	$13, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %r12
	jmp	.L435
	.size	_nss_compat_getpwuid_r, .-_nss_compat_getpwuid_r
	.local	lock
	.comm	lock,40,32
	.data
	.align 32
	.type	ext_ent, @object
	.size	ext_ent, 168
ext_ent:
	.byte	0
	.byte	0
	.byte	1
	.zero	1
	.long	1
	.quad	0
	.quad	0
	.long	0
	.long	0
	.quad	0
	.quad	0
	.long	0
	.long	0
	.quad	0
	.quad	0
	.quad	0
	.zero	88
	.local	endpwent_impl
	.comm	endpwent_impl,8,8
	.local	getpwent_r_impl
	.comm	getpwent_r_impl,8,8
	.local	getpwuid_r_impl
	.comm	getpwuid_r_impl,8,8
	.local	getpwnam_r_impl
	.comm	getpwnam_r_impl,8,8
	.local	setpwent_impl
	.comm	setpwent_impl,8,8
	.local	ni
	.comm	ni,8,8
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
