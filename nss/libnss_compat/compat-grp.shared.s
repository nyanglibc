	.text
	.p2align 4,,15
	.type	internal_endgrent, @function
internal_endgrent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L2
	call	fclose@PLT
	movq	$0, 8(%rbx)
.L2:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L3
	movl	$1, 24(%rbx)
	movb	$124, (%rax)
	movq	16(%rbx), %rax
	movb	$0, 1(%rax)
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$0, 24(%rbx)
	movl	$1, %eax
	popq	%rbx
	ret
	.size	internal_endgrent, .-internal_endgrent
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"nis"
.LC1:
	.string	"group_compat"
.LC2:
	.string	"setgrent"
.LC3:
	.string	"getgrnam_r"
.LC4:
	.string	"getgrgid_r"
.LC5:
	.string	"getgrent_r"
.LC6:
	.string	"endgrent"
	.text
	.p2align 4,,15
	.type	init_nss_interface, @function
init_nss_interface:
	leaq	ni(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	xorl	%esi, %esi
	call	__nss_database_lookup2@PLT
	testl	%eax, %eax
	js	.L10
	movq	ni(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, setgrent_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, getgrnam_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, getgrgid_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, getgrent_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	%rax, endgrent_impl(%rip)
.L10:
	addq	$8, %rsp
	ret
	.size	init_nss_interface, .-init_nss_interface
	.section	.rodata.str1.1
.LC7:
	.string	"/etc/group"
	.text
	.p2align 4,,15
	.type	internal_setgrent, @function
internal_setgrent:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	16(%rdi), %rax
	movq	%rdi, %rbx
	movl	%edx, %ebp
	movb	$1, (%rdi)
	testq	%rax, %rax
	je	.L14
	movl	$1, 24(%rdi)
	movb	$124, (%rax)
	movq	16(%rdi), %rax
	movb	$0, 1(%rax)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L24
.L16:
	call	rewind@PLT
.L17:
	testl	%ebp, %ebp
	movl	$1, %eax
	je	.L13
	movq	setgrent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L21
	movl	%r12d, %edi
	call	*%rax
	movl	%eax, 4(%rbx)
	movl	%ebp, %eax
.L13:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$0, 24(%rdi)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L16
.L24:
	leaq	.LC7(%rip), %rdi
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	jne	.L17
	movq	errno@gottpoff(%rip), %rax
	cmpl	$11, %fs:(%rax)
	setne	%al
	movzbl	%al, %eax
	subl	$2, %eax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L21:
	movl	%ebp, %eax
	jmp	.L13
	.size	internal_setgrent, .-internal_setgrent
	.p2align 4,,15
	.type	in_blacklist.isra.2, @function
in_blacklist.isra.2:
	pushq	%rbp
	leal	3(%rsi), %eax
	movq	%rsp, %rbp
	pushq	%rbx
	cltq
	addq	$15, %rax
	subq	$8, %rsp
	movq	(%rdx), %rbx
	andq	$-16, %rax
	subq	%rax, %rsp
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L25
	movq	%rdi, %rsi
	leaq	1(%rsp), %rdi
	movb	$124, (%rsp)
	call	__stpcpy@PLT
	movl	$124, %edx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movw	%dx, (%rax)
	call	strstr@PLT
	testq	%rax, %rax
	setne	%al
.L25:
	movq	-8(%rbp), %rbx
	leave
	ret
	.size	in_blacklist.isra.2, .-in_blacklist.isra.2
	.section	.text.unlikely,"ax",@progbits
	.type	blacklist_store_name, @function
blacklist_store_name:
	pushq	%r12
	pushq	%rbp
	orq	$-1, %rcx
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	repnz scasb
	cmpl	$0, 28(%rsi)
	notq	%rcx
	leaq	-1(%rcx), %rbp
	jne	.L32
	leal	(%rbp,%rbp), %edi
	movl	$512, %eax
	cmpl	$512, %edi
	cmovl	%eax, %edi
	movl	%edi, 28(%rsi)
	movslq	%edi, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rbx)
	je	.L31
	movw	$124, (%rax)
	movl	$1, 24(%rbx)
	jmp	.L35
.L32:
	leaq	16(%rsi), %rdx
	movq	%r12, %rdi
	movl	%ebp, %esi
	call	in_blacklist.isra.2
	testb	%al, %al
	jne	.L31
	movl	24(%rbx), %eax
	movl	28(%rbx), %edx
	addl	%ebp, %eax
	incl	%eax
	cmpl	%edx, %eax
	jl	.L35
	leal	(%rbp,%rbp), %esi
	movl	$256, %eax
	movq	16(%rbx), %rdi
	cmpl	$256, %esi
	cmovl	%eax, %esi
	addl	%edx, %esi
	movl	%esi, 28(%rbx)
	movslq	%esi, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L37
	movq	16(%rbx), %rdi
	call	free@PLT
	movl	$0, 28(%rbx)
	jmp	.L31
.L37:
	movq	%rax, 16(%rbx)
.L35:
	movslq	24(%rbx), %rdi
	movq	%r12, %rsi
	incl	%ebp
	addq	16(%rbx), %rdi
	call	__stpcpy@PLT
	movw	$124, (%rax)
	addl	%ebp, 24(%rbx)
.L31:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	blacklist_store_name, .-blacklist_store_name
	.type	getgrnam_plusgroup, @function
getgrnam_plusgroup:
	movq	getgrnam_r_impl(%rip), %r11
	orl	$-1, %eax
	testq	%r11, %r11
	je	.L43
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r10
	movq	%rdx, %rbx
	movq	%r8, %rcx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	%r9, %r8
	movq	%r10, %rdx
	call	*%r11
	cmpl	$1, %eax
	jne	.L39
	movq	0(%rbp), %r8
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r8, %rdi
	repnz scasb
	movq	%r8, %rdi
	movq	%rcx, %rdx
	notq	%rdx
	leaq	-1(%rdx), %rsi
	leaq	16(%rbx), %rdx
	call	in_blacklist.isra.2
	xorl	$1, %eax
	movzbl	%al, %eax
.L39:
	popq	%rdx
	popq	%rbx
	popq	%rbp
	ret
.L43:
	ret
	.size	getgrnam_plusgroup, .-getgrnam_plusgroup
	.text
	.p2align 4,,15
	.type	getgrent_next_nss.part.4.constprop.6, @function
getgrent_next_nss.part.4.constprop.6:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rcx, %r12
	subq	$8, %rsp
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L51:
	movq	0(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	16+ext_ent(%rip), %rdx
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	in_blacklist.isra.2
	testb	%al, %al
	je	.L46
.L48:
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%rbp, %rdi
	call	*getgrent_r_impl(%rip)
	cmpl	$1, %eax
	movl	%eax, %r15d
	je	.L51
.L46:
	addq	$8, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	getgrent_next_nss.part.4.constprop.6, .-getgrent_next_nss.part.4.constprop.6
	.p2align 4,,15
	.globl	_nss_compat_setgrent
	.type	_nss_compat_setgrent, @function
_nss_compat_setgrent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	movl	%edi, %ebx
	je	.L53
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L53:
	cmpq	$0, ni(%rip)
	je	.L63
.L54:
	leaq	ext_ent(%rip), %rdi
	movl	%ebx, %esi
	movl	$1, %edx
	call	internal_setgrent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L52
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L52:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L63:
	call	init_nss_interface
	jmp	.L54
	.size	_nss_compat_setgrent, .-_nss_compat_setgrent
	.p2align 4,,15
	.globl	_nss_compat_endgrent
	.type	_nss_compat_endgrent, @function
_nss_compat_endgrent:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	pushq	%rbx
	je	.L65
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L65:
	movq	endgrent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L66
	call	*%rax
.L66:
	leaq	ext_ent(%rip), %rdi
	call	internal_endgrent
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	%eax, %ebx
	je	.L64
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L64:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.size	_nss_compat_endgrent, .-_nss_compat_endgrent
	.p2align 4,,15
	.globl	_nss_compat_getgrent_r
	.type	_nss_compat_getgrent_r, @function
_nss_compat_getgrent_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rcx, %r15
	subq	$56, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, -88(%rbp)
	je	.L79
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L79:
	cmpq	$0, ni(%rip)
	je	.L129
	cmpq	$0, 8+ext_ent(%rip)
	je	.L81
.L84:
	cmpb	$0, ext_ent(%rip)
	jne	.L130
	cmpq	$0, getgrent_r_impl(%rip)
	je	.L107
.L128:
	movl	4+ext_ent(%rip), %edx
	cmpl	$1, %edx
	je	.L131
.L85:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L78
	leaq	lock(%rip), %rdi
	movl	%edx, -72(%rbp)
	call	__pthread_mutex_unlock@PLT
	movl	-72(%rbp), %edx
.L78:
	leaq	-40(%rbp), %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L130:
	leaq	-64(%rbp), %rax
	leaq	-1(%r12,%r13), %rbx
	movq	%rax, -72(%rbp)
.L82:
	movl	%r13d, -80(%rbp)
.L95:
	cmpq	$2, %r13
	jbe	.L87
.L118:
	movq	-72(%rbp), %rsi
	movq	8+ext_ent(%rip), %rdi
	call	fgetpos@PLT
	movb	$-1, (%rbx)
	movl	-80(%rbp), %esi
	movq	%r12, %rdi
	movq	8+ext_ent(%rip), %rdx
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L132
	cmpb	$-1, (%rbx)
	jne	.L126
	movb	$0, (%rbx)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$1, %r14
.L92:
	movsbq	(%r14), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L93
	testb	%dl, %dl
	je	.L118
	cmpb	$35, %dl
	je	.L118
	movq	-88(%rbp), %rsi
	movq	%r15, %r8
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%r14, %rdi
	call	_nss_files_parse_grent@PLT
	testl	%eax, %eax
	je	.L95
	addl	$1, %eax
	je	.L126
	movq	-88(%rbp), %rax
	movq	(%rax), %r11
	movzbl	(%r11), %edx
	leal	-43(%rdx), %eax
	andb	$-3, %al
	jne	.L97
	cmpb	$45, %dl
	je	.L133
	cmpb	$43, %dl
	jne	.L82
	movzbl	1(%r11), %edx
	testb	$-65, %dl
	jne	.L134
	testb	%dl, %dl
	jne	.L82
	orl	$-1, %edx
	cmpq	$0, getgrent_r_impl(%rip)
	movb	$0, ext_ent(%rip)
	jne	.L128
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L132:
	movq	8+ext_ent(%rip), %rdi
	testb	$16, (%rdi)
	jne	.L135
.L90:
	movq	-72(%rbp), %rsi
	call	fsetpos@PLT
.L87:
	movl	$34, (%r15)
	movl	$-2, %edx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L131:
	movq	-88(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	movq	%r12, %rsi
	call	getgrent_next_nss.part.4.constprop.6
	movl	%eax, %edx
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L129:
	call	init_nss_interface
	cmpq	$0, 8+ext_ent(%rip)
	jne	.L84
	.p2align 4,,10
	.p2align 3
.L81:
	leaq	ext_ent(%rip), %rdi
	movl	$1, %edx
	movl	$1, %esi
	call	internal_setgrent
	cmpl	$1, %eax
	movl	%eax, %edx
	jne	.L85
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L126:
	movq	8+ext_ent(%rip), %rdi
	jmp	.L90
.L135:
	xorl	%edx, %edx
	jmp	.L85
.L107:
	movl	$-1, %edx
	jmp	.L85
.L134:
	movq	%rsp, %r10
	movq	%r11, %rdi
	orq	$-1, %rcx
	movq	%r10, -96(%rbp)
	addq	$1, %r11
	repnz scasb
	movq	%r11, %rsi
	movq	%r11, -80(%rbp)
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$14, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-80(%rbp), %r11
	movq	-88(%rbp), %rsi
	leaq	ext_ent(%rip), %rdx
	movq	%r15, %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%r11, %rdi
	call	getgrnam_plusgroup
	leaq	ext_ent(%rip), %rsi
	movq	%rsp, %rdi
	movl	%eax, -80(%rbp)
	call	blacklist_store_name
	movl	-80(%rbp), %edx
	movq	-96(%rbp), %r10
	cmpl	$1, %edx
	je	.L101
	testl	$-3, %edx
	je	.L136
	cmpl	$-2, %edx
	je	.L103
	movq	%r10, %rsp
	jmp	.L85
.L133:
	testb	$-65, 1(%r11)
	je	.L82
	leaq	1(%r11), %rdi
	leaq	ext_ent(%rip), %rsi
	call	blacklist_store_name
	jmp	.L82
.L101:
	movq	%r10, %rsp
.L97:
	movl	$1, %edx
	jmp	.L85
.L103:
	movq	%r10, %rsp
	movq	8+ext_ent(%rip), %rdi
	jmp	.L90
.L136:
	movq	%r10, %rsp
	jmp	.L82
	.size	_nss_compat_getgrent_r, .-_nss_compat_getgrent_r
	.p2align 4,,15
	.globl	_nss_compat_getgrnam_r
	.type	_nss_compat_getgrnam_r, @function
_nss_compat_getgrnam_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	xorl	%r13d, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movzbl	(%rdi), %eax
	movb	$1, 64(%rsp)
	movq	$1, 68(%rsp)
	movq	$0, 76(%rsp)
	movq	$0, 84(%rsp)
	movl	$0, 92(%rsp)
	subl	$43, %eax
	testb	$-3, %al
	je	.L137
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%r8, 8(%rsp)
	movq	%rcx, %rbp
	movq	%rdx, %rbx
	movq	%rsi, 24(%rsp)
	movq	%rdi, 32(%rsp)
	je	.L139
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L139:
	cmpq	$0, ni(%rip)
	je	.L188
.L140:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L141
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L141:
	leaq	64(%rsp), %r14
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	internal_setgrent
	cmpl	$1, %eax
	movl	%eax, %r13d
	je	.L189
.L142:
	movq	errno@gottpoff(%rip), %rbx
	movq	%r14, %rdi
	movl	%fs:(%rbx), %ebp
	call	internal_endgrent
	movl	%ebp, %fs:(%rbx)
.L137:
	addq	$104, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	48(%rsp), %rax
	leaq	-1(%rbx,%rbp), %r15
	movq	%rax, 16(%rsp)
.L159:
	movl	%ebp, 44(%rsp)
.L153:
	cmpq	$2, %rbp
	jbe	.L144
.L174:
	movq	16(%rsp), %rsi
	movq	72(%rsp), %rdi
	call	fgetpos@PLT
	movq	72(%rsp), %rdx
	movl	44(%rsp), %esi
	movq	%rbx, %rdi
	movb	$-1, (%r15)
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L190
	cmpb	$-1, (%r15)
	jne	.L185
	movb	$0, (%r15)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	addq	$1, %r12
.L150:
	movsbq	(%r12), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L151
	testb	%dl, %dl
	je	.L174
	cmpb	$35, %dl
	je	.L174
	movq	8(%rsp), %r8
	movq	24(%rsp), %rsi
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	_nss_files_parse_grent@PLT
	testl	%eax, %eax
	je	.L153
	addl	$1, %eax
	je	.L185
	movq	24(%rsp), %rax
	movq	(%rax), %rdi
	movzbl	(%rdi), %eax
	leal	-43(%rax), %edx
	andb	$-3, %dl
	je	.L155
	movq	32(%rsp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L142
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L188:
	call	init_nss_interface
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L190:
	movq	72(%rsp), %rdi
	testb	$16, (%rdi)
	jne	.L147
.L148:
	movq	16(%rsp), %rsi
	call	fsetpos@PLT
.L144:
	movq	8(%rsp), %rax
	movl	$-2, %r13d
	movl	$34, (%rax)
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L185:
	movq	72(%rsp), %rdi
	jmp	.L148
.L191:
	cmpb	$0, 1(%rdi)
	je	.L159
	movq	32(%rsp), %rsi
	addq	$1, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L159
.L147:
	xorl	%r13d, %r13d
	jmp	.L142
.L155:
	cmpb	$45, %al
	je	.L191
	cmpb	$43, %al
	jne	.L159
	cmpb	$0, 1(%rdi)
	je	.L158
	leaq	1(%rdi), %rsi
	movq	32(%rsp), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L159
.L158:
	movq	8(%rsp), %r9
	movq	24(%rsp), %rsi
	movq	%rbp, %r8
	movq	32(%rsp), %rdi
	movq	%rbx, %rcx
	movq	%r14, %rdx
	call	getgrnam_plusgroup
	cmpl	$2, %eax
	je	.L159
	movl	%eax, %r13d
	jmp	.L142
	.size	_nss_compat_getgrnam_r, .-_nss_compat_getgrnam_r
	.p2align 4,,15
	.globl	_nss_compat_getgrgid_r
	.type	_nss_compat_getgrgid_r, @function
_nss_compat_getgrgid_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbx
	movq	%rcx, %r15
	subq	$104, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movl	%edi, -124(%rbp)
	movq	%rsi, -120(%rbp)
	movq	%r8, -112(%rbp)
	movb	$1, -80(%rbp)
	movq	$1, -76(%rbp)
	movq	$0, -68(%rbp)
	movq	$0, -60(%rbp)
	movl	$0, -52(%rbp)
	je	.L193
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L193:
	cmpq	$0, ni(%rip)
	je	.L231
.L194:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L195
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L195:
	leaq	-80(%rbp), %r12
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	internal_setgrent
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L232
.L196:
	movq	errno@gottpoff(%rip), %r13
	movq	%r12, %rdi
	movl	%fs:0(%r13), %r14d
	call	internal_endgrent
	movl	%ebx, %eax
	movl	%r14d, %fs:0(%r13)
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	-96(%rbp), %rax
	leaq	-1(%r14,%r15), %r13
	movl	%ebx, -128(%rbp)
	movq	%r15, -136(%rbp)
	movq	%rax, -104(%rbp)
.L215:
	movl	-136(%rbp), %ebx
.L207:
	cmpq	$2, -136(%rbp)
	jbe	.L198
.L224:
	movq	-104(%rbp), %rsi
	movq	-72(%rbp), %rdi
	call	fgetpos@PLT
	movq	-72(%rbp), %rdx
	movb	$-1, 0(%r13)
	movl	%ebx, %esi
	movq	%r14, %rdi
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L233
	cmpb	$-1, 0(%r13)
	jne	.L230
	movb	$0, 0(%r13)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L205:
	addq	$1, %r15
.L204:
	movsbq	(%r15), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L205
	testb	%dl, %dl
	je	.L224
	cmpb	$35, %dl
	je	.L224
	movq	-112(%rbp), %r8
	movq	-136(%rbp), %rcx
	movq	%r14, %rdx
	movq	-120(%rbp), %rsi
	movq	%r15, %rdi
	call	_nss_files_parse_grent@PLT
	testl	%eax, %eax
	je	.L207
	addl	$1, %eax
	je	.L230
	movq	-120(%rbp), %rsi
	movq	(%rsi), %r10
	movzbl	(%r10), %edx
	leal	-43(%rdx), %eax
	andb	$-3, %al
	je	.L209
	movl	-124(%rbp), %eax
	cmpl	16(%rsi), %eax
	jne	.L215
	movl	-128(%rbp), %ebx
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L233:
	movq	-72(%rbp), %rdi
	testb	$16, (%rdi)
	jne	.L201
.L202:
	movq	-104(%rbp), %rsi
	call	fsetpos@PLT
.L198:
	movq	-112(%rbp), %rax
	movl	$-2, %ebx
	movl	$34, (%rax)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L231:
	call	init_nss_interface
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L230:
	movq	-72(%rbp), %rdi
	jmp	.L202
.L209:
	cmpb	$45, %dl
	je	.L234
	cmpb	$43, %dl
	jne	.L215
	cmpb	$0, 1(%r10)
	jne	.L235
	movq	getgrgid_r_impl(%rip), %rax
	movq	-136(%rbp), %r15
	testq	%rax, %rax
	je	.L216
	movq	-112(%rbp), %r8
	movq	%r15, %rcx
	movq	%r14, %rdx
	movq	-120(%rbp), %rsi
	movl	-124(%rbp), %edi
	call	*%rax
	cmpl	$2, %eax
	movl	%eax, %ebx
	jne	.L196
.L201:
	xorl	%ebx, %ebx
	jmp	.L196
.L234:
	cmpb	$0, 1(%r10)
	je	.L215
	leaq	1(%r10), %rdi
	movq	%r12, %rsi
	call	blacklist_store_name
	jmp	.L215
.L216:
	orl	$-1, %ebx
	jmp	.L196
.L235:
	movq	%r10, %rdi
	orq	$-1, %rcx
	addq	$1, %r10
	repnz scasb
	movq	%rsp, %rbx
	movq	%r10, %rsi
	movq	%r10, -144(%rbp)
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rdx
	addq	$14, %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	movq	%rsp, %rdi
	call	memcpy@PLT
	movq	-144(%rbp), %r10
	movq	-120(%rbp), %r15
	movq	%r14, %rcx
	movq	-112(%rbp), %r9
	movq	-136(%rbp), %r8
	movq	%r12, %rdx
	movq	%r10, %rdi
	movq	%r15, %rsi
	call	getgrnam_plusgroup
	movq	%r12, %rsi
	movq	%rsp, %rdi
	movl	%eax, -144(%rbp)
	call	blacklist_store_name
	movl	-144(%rbp), %eax
	subl	$1, %eax
	jne	.L213
	movl	-124(%rbp), %eax
	cmpl	16(%r15), %eax
	je	.L214
.L213:
	movq	%rbx, %rsp
	jmp	.L215
.L214:
	movq	%rbx, %rsp
	movl	$1, %ebx
	jmp	.L196
	.size	_nss_compat_getgrgid_r, .-_nss_compat_getgrgid_r
	.local	lock
	.comm	lock,40,32
	.data
	.align 32
	.type	ext_ent, @object
	.size	ext_ent, 32
ext_ent:
	.byte	1
	.zero	3
	.long	1
	.quad	0
	.quad	0
	.long	0
	.long	0
	.local	endgrent_impl
	.comm	endgrent_impl,8,8
	.local	getgrent_r_impl
	.comm	getgrent_r_impl,8,8
	.local	getgrgid_r_impl
	.comm	getgrgid_r_impl,8,8
	.local	getgrnam_r_impl
	.comm	getgrnam_r_impl,8,8
	.local	setgrent_impl
	.comm	setgrent_impl,8,8
	.local	ni
	.comm	ni,8,8
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
