	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"(none)"
	.text
	.p2align 4,,15
	.globl	__nss_get_default_domain
	.type	__nss_get_default_domain, @function
__nss_get_default_domain:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	$0, (%rdi)
	je	.L2
	leaq	domainname_lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	cmpb	$0, domainname(%rip)
	leaq	domainname(%rip), %rbp
	jne	.L15
.L3:
	movq	%rbp, (%rbx)
	xorl	%ebx, %ebx
.L5:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	domainname_lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$1024, %esi
	movq	%rbp, %rdi
	call	getdomainname@PLT
	testl	%eax, %eax
	js	.L16
	leaq	.LC0(%rip), %rdi
	movl	$7, %ecx
	movq	%rbp, %rsi
	repz cmpsb
	jne	.L3
	movb	$0, domainname(%rip)
	movl	$2, %ebx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L16:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	jmp	.L5
	.size	__nss_get_default_domain, .-__nss_get_default_domain
	.local	domainname_lock
	.comm	domainname_lock,40,32
	.local	domainname
	.comm	domainname,1024,32
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
