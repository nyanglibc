	.text
	.p2align 4,,15
	.type	add_group, @function
add_group:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$16, %rsp
	movq	(%rdi), %rsi
	cmpq	(%r12), %rsi
	movq	(%rdx), %rax
	je	.L13
.L2:
	movl	%r8d, (%rax,%rsi,4)
	addq	$1, %rsi
	movq	%rsi, 0(%rbp)
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testq	%rcx, %rcx
	jle	.L3
	cmpq	%rcx, %rsi
	je	.L1
	addq	%rsi, %rsi
	movq	%rcx, %rbx
	cmpq	%rcx, %rsi
	cmovle	%rsi, %rbx
.L6:
	leaq	0(,%rbx,4), %rsi
	movq	%rax, %rdi
	movl	%r8d, 12(%rsp)
	movq	%rdx, (%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1
	movq	(%rsp), %rdx
	movl	12(%rsp), %r8d
	movq	%rax, (%rdx)
	movq	%rbx, (%r12)
	movq	0(%rbp), %rsi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	(%rsi,%rsi), %rbx
	jmp	.L6
	.size	add_group, .-add_group
	.p2align 4,,15
	.type	in_blacklist.isra.0, @function
in_blacklist.isra.0:
	pushq	%rbp
	leal	3(%rsi), %eax
	movq	%rsp, %rbp
	pushq	%rbx
	cltq
	addq	$15, %rax
	subq	$8, %rsp
	movq	(%rdx), %rbx
	andq	$-16, %rax
	subq	%rax, %rsp
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L14
	movq	%rdi, %rsi
	leaq	1(%rsp), %rdi
	movb	$124, (%rsp)
	call	__stpcpy@PLT
	movl	$124, %edx
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movw	%dx, (%rax)
	call	strstr@PLT
	testq	%rax, %rax
	setne	%al
.L14:
	movq	-8(%rbp), %rbx
	leave
	ret
	.size	in_blacklist.isra.0, .-in_blacklist.isra.0
	.section	.text.unlikely,"ax",@progbits
	.type	blacklist_store_name, @function
blacklist_store_name:
	pushq	%r12
	pushq	%rbp
	orq	$-1, %rcx
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %r12
	movq	%rsi, %rbx
	repnz scasb
	cmpl	$0, 28(%rsi)
	notq	%rcx
	leaq	-1(%rcx), %rbp
	jne	.L21
	leal	(%rbp,%rbp), %edi
	movl	$512, %eax
	cmpl	$512, %edi
	cmovl	%eax, %edi
	movl	%edi, 28(%rsi)
	movslq	%edi, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rbx)
	je	.L20
	movw	$124, (%rax)
	movl	$1, 24(%rbx)
	jmp	.L24
.L21:
	leaq	16(%rsi), %rdx
	movq	%r12, %rdi
	movl	%ebp, %esi
	call	in_blacklist.isra.0
	testb	%al, %al
	jne	.L20
	movl	24(%rbx), %eax
	movl	28(%rbx), %edx
	addl	%ebp, %eax
	incl	%eax
	cmpl	%edx, %eax
	jl	.L24
	leal	(%rbp,%rbp), %esi
	movl	$256, %eax
	movq	16(%rbx), %rdi
	cmpl	$256, %esi
	cmovl	%eax, %esi
	addl	%edx, %esi
	movl	%esi, 28(%rbx)
	movslq	%esi, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L26
	movq	16(%rbx), %rdi
	call	free@PLT
	movl	$0, 28(%rbx)
	jmp	.L20
.L26:
	movq	%rax, 16(%rbx)
.L24:
	movslq	24(%rbx), %rdi
	movq	%r12, %rsi
	incl	%ebp
	addq	16(%rbx), %rdi
	call	__stpcpy@PLT
	movw	$124, (%rax)
	addl	%ebp, 24(%rbx)
.L20:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	blacklist_store_name, .-blacklist_store_name
	.text
	.p2align 4,,15
	.type	check_and_add_group.isra.1, @function
check_and_add_group.isra.1:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	cmpl	80(%rsp), %esi
	je	.L28
	movq	88(%rsp), %rax
	movq	%rdi, %rbx
	movq	%r9, %r14
	movq	%r8, %r13
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movq	(%rax), %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	jne	.L31
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L30:
	addq	$8, %r15
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.L33
.L31:
	movq	%rbx, %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L30
	movl	80(%rsp), %r8d
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movl	%eax, 12(%rsp)
	call	add_group
	movl	12(%rsp), %eax
.L28:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	check_and_add_group.isra.1, .-check_and_add_group.isra.1
	.p2align 4,,15
	.type	getgrent_next_nss, @function
getgrent_next_nss:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$136, %rsp
	cmpb	$0, 2(%rdi)
	movq	%rdi, 8(%rsp)
	movq	%rcx, 16(%rsp)
	movl	%r8d, 28(%rsp)
	movq	%r9, 32(%rsp)
	movq	216(%rsp), %rbp
	jne	.L82
	movq	208(%rsp), %rdi
	movq	$0, 72(%rsp)
	testq	%rdi, %rdi
	jle	.L83
.L38:
	movq	%rdi, 80(%rsp)
	salq	$2, %rdi
	movl	$-2, %ebx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 88(%rsp)
	je	.L36
	leaq	80(%rsp), %rcx
	leaq	72(%rsp), %rdx
	subq	$8, %rsp
	pushq	%rbp
	movq	224(%rsp), %r9
	movl	44(%rsp), %esi
	movq	32(%rsp), %rdi
	leaq	104(%rsp), %r8
	call	*initgroups_dyn_impl(%rip)
	cmpl	$1, %eax
	popq	%rdi
	popq	%r8
	jne	.L40
	movq	8(%rsp), %rcx
	movq	72(%rsp), %rax
	cmpl	$1, 24(%rcx)
	jle	.L41
	testq	%rax, %rax
	jle	.L44
	leaq	96(%rsp), %r12
	movq	%r14, %r15
	movq	$0, 48(%rsp)
	movq	%r14, 56(%rsp)
	movq	%r12, 40(%rsp)
	movq	%r13, %r12
.L42:
	movq	48(%rsp), %rax
	leaq	0(,%rax,4), %r14
.L53:
	movq	88(%rsp), %rax
	movq	%rbp, %r8
	movq	%r15, %rcx
	movq	%r12, %rdx
	movq	40(%rsp), %rsi
	movl	(%rax,%r14), %edi
	call	*getgrgid_r_impl(%rip)
	cmpl	$-2, %eax
	movl	%eax, %ebx
	jne	.L49
	cmpl	$34, 0(%rbp)
	jne	.L79
	leaq	(%r15,%r15), %rax
	cmpq	%r15, %rax
	jb	.L84
	cmpq	$1024, %rax
	movl	$1024, %r15d
	cmovnb	%rax, %r15
	cmpq	%r13, %r12
	je	.L48
	movq	%r12, %rdi
	call	free@PLT
.L48:
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L53
.L79:
	movq	%r12, %r10
.L47:
	cmpq	%r13, %r10
	je	.L43
	movq	%r10, %rdi
	call	free@PLT
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L41:
	testq	%rax, %rax
	jle	.L44
	movq	32(%rsp), %r12
	movq	208(%rsp), %r13
	movl	$1, %ebp
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L45:
	movq	88(%rsp), %rax
	movq	200(%rsp), %rdx
	movq	%r13, %rcx
	movq	192(%rsp), %rsi
	movq	%r12, %rdi
	movl	(%rax,%rbx), %r8d
	addq	$4, %rbx
	call	add_group
	movq	%rbp, %rax
	addq	$1, %rbp
	cmpq	72(%rsp), %rax
	jl	.L45
.L44:
	xorl	%ebx, %ebx
.L43:
	movq	88(%rsp), %rdi
	call	free@PLT
.L36:
	addq	$136, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	88(%rsp), %rdi
	call	free@PLT
.L82:
	leaq	96(%rsp), %r12
.L37:
	movq	8(%rsp), %rax
	addq	$16, %rax
	movq	%rax, 8(%rsp)
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L86:
	movq	96(%rsp), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	movq	8(%rsp), %rdx
	movl	%eax, %esi
	movq	%r15, %rdi
	call	in_blacklist.isra.0
	testb	%al, %al
	je	.L85
.L55:
	movq	%rbp, %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*getgrent_r_impl(%rip)
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L86
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$24, %r12
	pushq	%r12
	movl	120(%rsp), %eax
	pushq	%rax
	movq	224(%rsp), %r9
	movq	216(%rsp), %r8
	movq	208(%rsp), %rcx
	movq	48(%rsp), %rdx
	movl	44(%rsp), %esi
	movq	32(%rsp), %rdi
	call	check_and_add_group.isra.1
	popq	%rax
	popq	%rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L83:
	movq	192(%rsp), %rax
	movq	(%rax), %rdi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L84:
	movq	errno@gottpoff(%rip), %rax
	movq	%r12, %r10
	movl	$12, %fs:(%rax)
	jmp	.L47
.L49:
	testl	%eax, %eax
	je	.L51
	cmpl	$1, %eax
	jne	.L79
	movq	96(%rsp), %rbx
	movq	%rbx, %rdi
	call	strlen@PLT
	movq	8(%rsp), %rcx
	movl	%eax, %esi
	movq	%rbx, %rdi
	leaq	16(%rcx), %rdx
	call	in_blacklist.isra.0
	testb	%al, %al
	je	.L87
.L51:
	addq	$1, 48(%rsp)
	movq	48(%rsp), %rax
	cmpq	72(%rsp), %rax
	jl	.L42
	movq	%r12, %r10
	xorl	%ebx, %ebx
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L87:
	movq	40(%rsp), %rax
	addq	$24, %rax
	pushq	%rax
	movl	120(%rsp), %eax
	pushq	%rax
	movq	224(%rsp), %r9
	movq	216(%rsp), %r8
	movq	208(%rsp), %rcx
	movq	48(%rsp), %rdx
	movl	44(%rsp), %esi
	movq	32(%rsp), %rdi
	call	check_and_add_group.isra.1
	testl	%eax, %eax
	popq	%rcx
	popq	%rsi
	je	.L51
	movq	setgrent_impl(%rip), %rax
	movq	56(%rsp), %r14
	movq	40(%rsp), %r12
	testq	%rax, %rax
	je	.L52
	movl	$1, %edi
	call	*%rax
	movq	8(%rsp), %rax
	movb	$1, 1(%rax)
.L52:
	movq	8(%rsp), %rax
	movb	$1, 2(%rax)
	jmp	.L37
	.size	getgrent_next_nss, .-getgrent_next_nss
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"nis"
.LC1:
	.string	"group_compat"
.LC2:
	.string	"initgroups_dyn"
.LC3:
	.string	"getgrnam_r"
.LC4:
	.string	"getgrgid_r"
.LC5:
	.string	"setgrent"
.LC6:
	.string	"getgrent_r"
.LC7:
	.string	"endgrent"
.LC8:
	.string	"/etc/group"
	.text
	.p2align 4,,15
	.globl	_nss_compat_initgroups_dyn
	.type	_nss_compat_initgroups_dyn, @function
_nss_compat_initgroups_dyn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1208, %rsp
	cmpq	$0, ni(%rip)
	movq	%rdi, 8(%rsp)
	movl	%esi, 20(%rsp)
	movq	%rdx, 24(%rsp)
	movq	%rcx, 32(%rsp)
	movq	%r8, 40(%rsp)
	movq	%r9, 48(%rsp)
	movb	$1, 96(%rsp)
	movb	$0, 97(%rsp)
	movb	$0, 98(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 120(%rsp)
	je	.L166
.L89:
	movl	$0, 120(%rsp)
.L96:
	leaq	.LC8(%rip), %rdi
	call	__nss_files_fopen@PLT
	testq	%rax, %rax
	movq	%rax, 104(%rsp)
	je	.L97
	leaq	160(%rsp), %rax
	leaq	176(%rsp), %rbp
	leaq	80(%rsp), %r14
	movq	$1024, 168(%rsp)
	movl	$1024, %r12d
	movq	%rax, 56(%rsp)
	leaq	128(%rsp), %rax
	movq	%rbp, 160(%rsp)
	movq	%rax, 64(%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 72(%rsp)
	.p2align 4,,10
	.p2align 3
.L98:
	cmpb	$0, 96(%rsp)
	leaq	-1(%rbp,%r12), %rbx
	je	.L165
.L113:
	cmpq	$2, %r12
	jbe	.L104
	movl	%r12d, %r15d
.L152:
	movq	104(%rsp), %rdi
	movq	%r14, %rsi
	call	fgetpos@PLT
	movb	$-1, (%rbx)
	movq	104(%rsp), %rdx
	movl	%r15d, %esi
	movq	%rbp, %rdi
	call	fgets_unlocked@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L167
	cmpb	$-1, (%rbx)
	jne	.L164
	movb	$0, (%rbx)
	call	__ctype_b_loc@PLT
	movq	(%rax), %rcx
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$1, %r13
.L110:
	movsbq	0(%r13), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L111
	testb	%dl, %dl
	je	.L152
	cmpb	$35, %dl
	je	.L152
	movq	1264(%rsp), %r8
	movq	64(%rsp), %rsi
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%r13, %rdi
	call	_nss_files_parse_grent@PLT
	testl	%eax, %eax
	je	.L113
	cmpl	$-1, %eax
	je	.L164
	movq	128(%rsp), %rdx
	movzbl	(%rdx), %ecx
	leal	-43(%rcx), %eax
	andl	$253, %eax
	jne	.L115
	cmpb	$45, %cl
	je	.L168
	cmpb	$43, %cl
	jne	.L113
	movzbl	1(%rdx), %ecx
	testb	$-65, %cl
	jne	.L169
	testb	%cl, %cl
	jne	.L113
	cmpq	$0, initgroups_dyn_impl(%rip)
	je	.L122
	cmpq	$0, getgrgid_r_impl(%rip)
	je	.L122
.L124:
	movb	$0, 96(%rsp)
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	96(%rsp), %rdi
	pushq	1264(%rsp)
	pushq	56(%rsp)
	pushq	56(%rsp)
	pushq	56(%rsp)
	movq	%r12, %rdx
	movq	56(%rsp), %r9
	movl	52(%rsp), %r8d
	movq	%rbp, %rsi
	movq	40(%rsp), %rcx
	call	getgrent_next_nss
	addq	$32, %rsp
	cmpl	$-2, %eax
	je	.L170
	cmpl	$1, %eax
	jne	.L150
.L121:
	movq	168(%rsp), %r12
	movq	160(%rsp), %rbp
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L167:
	movq	104(%rsp), %rdi
	testb	$16, (%rdi)
	jne	.L150
.L108:
	movq	%r14, %rsi
	call	fsetpos@PLT
.L104:
	movq	1264(%rsp), %rax
	movl	$34, (%rax)
.L105:
	movq	56(%rsp), %rdi
	call	__libc_scratch_buffer_grow@PLT
	testb	%al, %al
	jne	.L121
	movl	$-2, %ebx
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L164:
	movq	104(%rsp), %rdi
	jmp	.L108
.L97:
	movq	errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	cmpl	$11, %fs:(%rax)
	setne	%bl
	subl	$2, %ebx
.L88:
	addq	$1208, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L166:
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L93
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
	cmpq	$0, ni(%rip)
	je	.L93
.L94:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L95
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L95:
	movq	112(%rsp), %rax
	testq	%rax, %rax
	je	.L89
	movl	$1, 120(%rsp)
	movb	$124, (%rax)
	movq	112(%rsp), %rax
	movb	$0, 1(%rax)
	jmp	.L96
.L170:
	movq	1264(%rsp), %rax
	cmpl	$34, (%rax)
	je	.L105
.L150:
	movl	$1, %ebx
.L100:
	movq	56(%rsp), %rax
	movq	160(%rsp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L127
	call	free@PLT
.L127:
	movq	104(%rsp), %rdi
	movq	errno@gottpoff(%rip), %rbp
	testq	%rdi, %rdi
	movl	%fs:0(%rbp), %r12d
	je	.L128
	call	fclose@PLT
	movq	$0, 104(%rsp)
.L128:
	movq	112(%rsp), %rax
	testq	%rax, %rax
	je	.L129
	movl	$1, 120(%rsp)
	movb	$124, (%rax)
	movq	112(%rsp), %rax
	movb	$0, 1(%rax)
.L130:
	cmpb	$0, 97(%rsp)
	je	.L131
	movq	endgrent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L131
	call	*%rax
.L131:
	movl	%r12d, %fs:0(%rbp)
	jmp	.L88
.L129:
	movl	$0, 120(%rsp)
	jmp	.L130
.L93:
	leaq	ni(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rdi
	xorl	%esi, %esi
	call	__nss_database_lookup2@PLT
	testl	%eax, %eax
	js	.L94
	movq	ni(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	movq	%rax, initgroups_dyn_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%rax, getgrnam_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC5(%rip), %rsi
	movq	%rax, getgrgid_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	movq	%rax, setgrent_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	ni(%rip), %rdi
	leaq	.LC7(%rip), %rsi
	movq	%rax, getgrent_r_impl(%rip)
	call	__nss_lookup_function@PLT
	movq	%rax, endgrent_impl(%rip)
	jmp	.L94
.L122:
	movq	setgrent_impl(%rip), %rax
	testq	%rax, %rax
	je	.L123
	movl	$1, %edi
	call	*%rax
	movb	$1, 97(%rsp)
.L123:
	cmpq	$0, getgrent_r_impl(%rip)
	movb	$1, 98(%rsp)
	jne	.L124
	jmp	.L150
.L169:
	leaq	1(%rdx), %r8
	orq	$-1, %rcx
	leaq	112(%rsp), %rdx
	leaq	96(%rsp), %r13
	movq	%r8, %rdi
	repnz scasb
	movq	%r8, %rdi
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rsi
	call	in_blacklist.isra.0
	testb	%al, %al
	jne	.L113
	movq	128(%rsp), %rax
	movq	%r13, %rsi
	leaq	1(%rax), %rdi
	call	blacklist_store_name
	movq	getgrnam_r_impl(%rip), %rax
	testq	%rax, %rax
	je	.L150
	movq	128(%rsp), %rsi
	movq	1264(%rsp), %r8
	movq	%r12, %rcx
	movq	%rbp, %rdx
	leaq	1(%rsi), %rdi
	movq	64(%rsp), %rsi
	call	*%rax
	subl	$1, %eax
	jne	.L113
.L115:
	pushq	72(%rsp)
	movl	152(%rsp), %eax
	pushq	%rax
	movq	64(%rsp), %r9
	movq	56(%rsp), %r8
	movq	48(%rsp), %rcx
	movq	40(%rsp), %rdx
	movl	36(%rsp), %esi
	movq	24(%rsp), %rdi
	call	check_and_add_group.isra.1
	popq	%rax
	popq	%rdx
	jmp	.L121
.L168:
	testb	$-65, 1(%rdx)
	je	.L113
	leaq	96(%rsp), %rsi
	leaq	1(%rdx), %rdi
	call	blacklist_store_name
	jmp	.L113
	.size	_nss_compat_initgroups_dyn, .-_nss_compat_initgroups_dyn
	.local	lock
	.comm	lock,40,32
	.local	endgrent_impl
	.comm	endgrent_impl,8,8
	.local	getgrent_r_impl
	.comm	getgrent_r_impl,8,8
	.local	setgrent_impl
	.comm	setgrent_impl,8,8
	.local	getgrgid_r_impl
	.comm	getgrgid_r_impl,8,8
	.local	getgrnam_r_impl
	.comm	getgrnam_r_impl,8,8
	.local	initgroups_dyn_impl
	.comm	initgroups_dyn_impl,8,8
	.local	ni
	.comm	ni,8,8
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
