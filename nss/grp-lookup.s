	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"files"
.LC1:
	.string	"group"
	.text
	.p2align 4,,15
	.globl	__nss_group_lookup2
	.hidden	__nss_group_lookup2
	.type	__nss_group_lookup2, @function
__nss_group_lookup2:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rdi, %rbx
	leaq	__nss_group_database(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movq	%rsi, %rbp
	xorl	%esi, %esi
	call	__nss_database_lookup2
	testl	%eax, %eax
	js	.L2
	movq	__nss_group_database(%rip), %rax
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__nss_lookup
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__nss_group_lookup2, .-__nss_group_lookup2
	.hidden	__nss_lookup
	.hidden	__nss_database_lookup2
	.hidden	__nss_group_database
