	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"digits_dots.c"
.LC1:
	.string	"af == AF_INET6"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__nss_hostname_digits_dots_context
	.hidden	__nss_hostname_digits_dots_context
	.type	__nss_hostname_digits_dots_context, @function
__nss_hostname_digits_dots_context:
	movsbl	(%rsi), %eax
	movl	%eax, %edi
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L2
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %r10
	movsbq	%dil, %rax
	movq	%fs:(%r10), %r10
	testb	$16, 1(%r10,%rax,2)
	jne	.L2
	cmpb	$58, %dil
	jne	.L82
.L2:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	$16, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	cmpl	$10, 112(%rsp)
	jne	.L86
.L5:
	movq	%r8, %rbp
	movq	%rsi, %rdi
	movq	%rdx, %r12
	movq	%r9, %r15
	movq	%rcx, %r14
	movq	%rsi, %rbx
	call	__GI_strlen
	testq	%rbp, %rbp
	leaq	41(%rax), %rdx
	je	.L87
	cmpq	%rdx, 0(%rbp)
	movq	(%r14), %rdi
	jb	.L88
.L9:
	xorl	%esi, %esi
	call	__GI_memset@PLT
	movq	(%r14), %r14
	leaq	16(%r14), %rax
	leaq	32(%r14), %r15
	movq	%rax, 8(%rsp)
	leaq	40(%r14), %rax
	movq	%rax, 16(%rsp)
	movsbl	(%rbx), %eax
	movl	%eax, %r9d
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L14
	testb	%r9b, %r9b
	movq	%rbx, %rcx
	je	.L23
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$1, %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L23
	movsbl	%dl, %eax
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L15
	cmpb	$46, %dl
	je	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r13
	movsbq	%r9b, %rax
	testb	$16, 1(%r13,%rax,2)
	je	.L25
	movl	$58, %esi
	movq	%rbx, %rdi
	movb	%r9b, 31(%rsp)
	call	__GI_strchr
	testq	%rax, %rax
	movzbl	31(%rsp), %r9d
	je	.L25
	cmpl	$10, 112(%rsp)
	jne	.L32
	testb	%r9b, %r9b
	movq	%rbx, %rdx
	je	.L36
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L36
	movsbq	%al, %rcx
	testb	$16, 1(%r13,%rcx,2)
	jne	.L35
	cmpb	$58, %al
	setne	%cl
	cmpb	$46, %al
	setne	%al
	testb	%al, %cl
	je	.L35
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$4, %r13d
	movl	$2, 112(%rsp)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	cmpb	$58, %r9b
	jne	.L3
	cmpl	$10, 112(%rsp)
	jne	.L32
	movq	%rbx, %rdx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L82:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	cmpq	%r15, %rdx
	jbe	.L89
	cmpq	$0, 120(%rsp)
	movq	104(%rsp), %rax
	movl	$-2, (%rax)
	je	.L10
	movq	120(%rsp), %rax
	movl	$-1, (%rax)
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$46, -1(%rcx)
	je	.L14
	cmpl	$2, 112(%rsp)
	je	.L90
	cmpl	$10, 112(%rsp)
	jne	.L91
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$10, %edi
	call	__GI_inet_pton
	testl	%eax, %eax
	setg	%al
	movzbl	%al, %eax
.L17:
	testl	%eax, %eax
	jne	.L19
.L32:
	movq	120(%rsp), %rax
	testq	%rbp, %rbp
	movl	$1, (%rax)
	je	.L92
.L13:
	movq	96(%rsp), %rax
	movq	$0, (%rax)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movq	%rdx, 0(%rbp)
	movq	%rdx, %rsi
	movq	%rdx, 8(%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	8(%rsp), %rdx
	je	.L93
	movq	%rax, (%r14)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L89:
	movq	(%r14), %rdi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L36:
	cmpb	$46, -1(%rdx)
	je	.L3
	movq	%r14, %rdx
	movq	%rbx, %rsi
	movl	$10, %edi
	call	__GI_inet_pton
	testl	%eax, %eax
	jle	.L32
	movq	16(%rsp), %rdi
	movq	%rbx, %rsi
	call	__GI_strcpy
	movq	%rax, (%r12)
	movq	8(%rsp), %rax
	testq	%rbp, %rbp
	movq	$0, 32(%r14)
	movq	%r15, 8(%r12)
	movq	%r14, 16(%r14)
	movq	$0, 24(%r14)
	movq	%rax, 24(%r12)
	movabsq	$68719476746, %rax
	movq	%rax, 16(%r12)
	movq	120(%rsp), %rax
	movl	$0, (%rax)
	jne	.L21
.L33:
	movq	104(%rsp), %rax
	movl	$1, (%rax)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L92:
	movq	104(%rsp), %rax
	movl	$0, (%rax)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movq	16(%rsp), %rdi
	movq	%rbx, %rsi
	call	__GI_strcpy
	movq	%rax, (%r12)
	movq	8(%rsp), %rax
	cmpq	$0, 120(%rsp)
	movq	$0, 32(%r14)
	movq	%r15, 8(%r12)
	movq	%r14, 16(%r14)
	movq	$0, 24(%r14)
	movq	%rax, 24(%r12)
	movl	112(%rsp), %eax
	movl	%r13d, 20(%r12)
	movl	%eax, 16(%r12)
	je	.L20
	movq	120(%rsp), %rax
	movl	$0, (%rax)
.L20:
	testq	%rbp, %rbp
	je	.L33
.L21:
	movq	96(%rsp), %rax
	movq	%r12, (%rax)
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L93:
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	(%r14), %rdi
	movl	%fs:(%rbx), %r12d
	call	free@PLT
	cmpq	$0, 120(%rsp)
	movq	$0, (%r14)
	movq	$0, 0(%rbp)
	movl	%r12d, %fs:(%rbx)
	je	.L13
	movq	120(%rsp), %rax
	movl	$-1, (%rax)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L90:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	__GI___inet_aton_exact
	jmp	.L17
.L91:
	leaq	__PRETTY_FUNCTION__.12675(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$165, %edx
	call	__GI___assert_fail
	.size	__nss_hostname_digits_dots_context, .-__nss_hostname_digits_dots_context
	.p2align 4,,15
	.globl	__GI___nss_hostname_digits_dots
	.hidden	__GI___nss_hostname_digits_dots
	.type	__GI___nss_hostname_digits_dots, @function
__GI___nss_hostname_digits_dots:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rcx, %r12
	movq	%r9, %r13
	subq	$24, %rsp
	movq	%r8, 8(%rsp)
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	8(%rsp), %r8
	je	.L103
	pushq	96(%rsp)
	movq	%rax, %rbx
	movl	96(%rsp), %eax
	movq	%r8, %r9
	movq	%r15, %rcx
	movq	%r12, %r8
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	pushq	%rax
	pushq	96(%rsp)
	pushq	%r13
	call	__nss_hostname_digits_dots_context
	movq	%rbx, %rdi
	movl	%eax, 40(%rsp)
	addq	$32, %rsp
	call	__GI___resolv_context_put
	movl	8(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	cmpq	$0, 96(%rsp)
	je	.L96
	movq	96(%rsp), %rax
	movl	$-1, (%rax)
.L96:
	testq	%r12, %r12
	je	.L104
	movq	$0, 0(%r13)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L104:
	movq	80(%rsp), %rax
	movl	$-2, (%rax)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__GI___nss_hostname_digits_dots, .-__GI___nss_hostname_digits_dots
	.globl	__nss_hostname_digits_dots
	.set	__nss_hostname_digits_dots,__GI___nss_hostname_digits_dots
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	__PRETTY_FUNCTION__.12675, @object
	.size	__PRETTY_FUNCTION__.12675, 35
__PRETTY_FUNCTION__.12675:
	.string	"__nss_hostname_digits_dots_context"
