	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__nss_valid_field
	.hidden	__nss_valid_field
	.type	__nss_valid_field, @function
__nss_valid_field:
	testq	%rdi, %rdi
	je	.L5
	leaq	__nss_invalid_field_characters(%rip), %rsi
	subq	$8, %rsp
	call	__GI_strpbrk
	testq	%rax, %rax
	sete	%al
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$1, %eax
	ret
	.size	__nss_valid_field, .-__nss_valid_field
	.hidden	__nss_invalid_field_characters
	.globl	__nss_invalid_field_characters
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	__nss_invalid_field_characters, @object
	.size	__nss_invalid_field_characters, 3
__nss_invalid_field_characters:
	.string	":\n"
