	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
	.text
	.p2align 4,,15
	.globl	__nss_files_fopen
	.hidden	__nss_files_fopen
	.type	__nss_files_fopen, @function
__nss_files_fopen:
	pushq	%rbx
	leaq	.LC0(%rip), %rsi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L1
	orl	$32768, (%rax)
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__fseeko64
	testl	%eax, %eax
	js	.L8
.L1:
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	_IO_new_fclose@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$29, %fs:(%rax)
	jmp	.L1
	.size	__nss_files_fopen, .-__nss_files_fopen
	.hidden	__fseeko64
