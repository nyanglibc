	.text
	.p2align 4,,15
	.globl	__nss_getent
	.hidden	__nss_getent
	.type	__nss_getent, @function
__nss_getent:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rbp
	movq	%r9, %rbx
	subq	$40, %rsp
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	je	.L16
	movq	(%r8), %rdx
.L3:
	leaq	24(%rsp), %r14
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%r15), %rdx
	movq	%rax, 0(%rbp)
.L9:
	movq	%rbx, %r8
	movq	%r14, %rcx
	movq	%r13, %rdi
	call	*%r12
	cmpl	$34, %eax
	jne	.L6
	testq	%rbx, %rbx
	je	.L7
	cmpl	$-1, (%rbx)
	jne	.L6
.L7:
	movq	(%r15), %rax
	movq	0(%rbp), %rdi
	leaq	(%rax,%rax), %rsi
	movq	%rsi, (%r15)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.L5
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	0(%rbp), %rdi
	movl	%fs:(%rbx), %r12d
	call	free@PLT
	movl	%r12d, %fs:(%rbx)
	movq	$0, 0(%rbp)
.L8:
	xorl	%eax, %eax
.L17:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	$0, 0(%rbp)
	je	.L8
	movq	24(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rcx, (%r15)
	movq	%rcx, %rdi
	movq	%rcx, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rsi
	movq	%rax, 0(%rbp)
	movq	8(%rsp), %rdx
	jne	.L3
	xorl	%eax, %eax
	jmp	.L17
	.size	__nss_getent, .-__nss_getent
