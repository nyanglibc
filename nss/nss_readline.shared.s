	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__nss_readline_seek
	.hidden	__nss_readline_seek
	.type	__nss_readline_seek, @function
__nss_readline_seek:
	testq	%rsi, %rsi
	pushq	%rbx
	movq	%rdi, %rbx
	js	.L4
	xorl	%edx, %edx
	call	__GI___fseeko64
	testl	%eax, %eax
	js	.L4
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %fs:(%rax)
	movl	$34, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$32, (%rbx)
	movl	$29, %fs:(%rax)
	movl	$29, %eax
	popq	%rbx
	ret
	.size	__nss_readline_seek, .-__nss_readline_seek
	.p2align 4,,15
	.globl	__GI___nss_readline
	.hidden	__GI___nss_readline
	.type	__GI___nss_readline, @function
__GI___nss_readline:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$2, %rdx
	jbe	.L8
	leaq	-1(%rsi,%rdx), %r15
	movq	%rcx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movl	%edx, %r12d
.L21:
	movq	%r13, %rdi
	call	__GI___ftello64
	movq	%r13, %rdx
	movq	%rax, (%r14)
	movl	%r12d, %esi
	movb	$-1, (%r15)
	movq	%rbp, %rdi
	call	__GI_fgets_unlocked
	testq	%rax, %rax
	je	.L23
	cmpb	$-1, (%r15)
	jne	.L13
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	0(%rbp), %rdx
	movq	%rbp, %rbx
	movq	%fs:(%rax), %rcx
	movq	%rdx, %rax
	testb	$32, 1(%rcx,%rdx,2)
	je	.L15
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	movq	%rdx, %rax
	jne	.L14
.L15:
	testb	%al, %al
	je	.L21
	cmpb	$35, %al
	je	.L21
	xorl	%r12d, %r12d
	cmpq	%rbx, %rbp
	je	.L7
	movq	%rbx, %rdi
	call	__GI_strlen
	movq	%rbx, %rsi
	movq	%rax, %rdx
	movq	%rbp, %rdi
	call	__GI_memmove
.L7:
	addq	$8, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, (%rcx)
	movl	$34, %r12d
	movl	$34, %fs:(%rax)
	jmp	.L7
.L23:
	testb	$16, 0(%r13)
	movq	__libc_errno@gottpoff(%rip), %rax
	jne	.L24
	movl	%fs:(%rax), %r12d
	cmpl	$34, %r12d
	jne	.L7
	movl	$22, %fs:(%rax)
	movl	$22, %r12d
	jmp	.L7
.L13:
	movq	(%r14), %rsi
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__nss_readline_seek
.L24:
	movl	$2, %fs:(%rax)
	movl	$2, %r12d
	jmp	.L7
	.size	__GI___nss_readline, .-__GI___nss_readline
	.globl	__nss_readline
	.set	__nss_readline,__GI___nss_readline
