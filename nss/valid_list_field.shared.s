	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__nss_valid_list_field
	.hidden	__nss_valid_list_field
	.type	__nss_valid_list_field, @function
__nss_valid_list_field:
	testq	%rdi, %rdi
	movl	$1, %eax
	je	.L10
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	leaq	invalid_characters(%rip), %rbp
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L15:
	addq	$8, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L14
.L3:
	movq	%rbp, %rsi
	call	__GI_strpbrk
	testq	%rax, %rax
	je	.L15
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	rep ret
	.size	__nss_valid_list_field, .-__nss_valid_list_field
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	invalid_characters, @object
	.size	invalid_characters, 4
invalid_characters:
	.string	":\n,"
