	.text
	.p2align 4,,15
	.type	name_to_database_index, @function
name_to_database_index:
	pushq	%r14
	pushq	%r13
	xorl	%r14d, %r14d
	pushq	%r12
	leaq	nss_database_name_array(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movl	$14, %ebp
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	(%r14,%rbp), %rbx
	movq	%r13, %rdi
	shrq	%rbx
	leaq	(%rbx,%rbx,4), %rax
	leaq	(%rbx,%rax,2), %rsi
	addq	%r12, %rsi
	call	strcmp
	testl	%eax, %eax
	js	.L3
	je	.L4
	leaq	1(%rbx), %r14
	cmpq	%rbp, %r14
	jb	.L2
.L9:
	popq	%rbx
	movl	$-1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpq	%r14, %rbx
	jbe	.L9
	movq	%rbx, %rbp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	name_to_database_index, .-name_to_database_index
	.p2align 4,,15
	.type	global_state_allocate, @function
global_state_allocate:
	subq	$8, %rsp
	movl	$192, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L11
	leaq	48(%rax), %rsi
	movq	$-1, (%rax)
	movl	$14, %ecx
	xorl	%eax, %eax
	movq	%rsi, %rdi
	rep stosq
	movb	$1, 164(%rdx)
	movl	$0, 160(%rdx)
	movl	$0, 168(%rdx)
	movq	$0, 176(%rdx)
	movq	$0, 184(%rdx)
.L11:
	movq	%rdx, %rax
	addq	$8, %rsp
	ret
	.size	global_state_allocate, .-global_state_allocate
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"files"
.LC1:
	.string	"nis"
.LC2:
	.string	"nis nisplus"
.LC3:
	.string	"dns [!UNAVAIL=return] files"
.LC4:
	.string	"/etc/nsswitch.conf"
.LC5:
	.string	"/"
.LC6:
	.string	"rce"
.LC7:
	.string	"nss_database.c"
.LC8:
	.string	"ret > 0"
.LC9:
	.string	"errno == ENOMEM"
	.text
	.p2align 4,,15
	.type	nss_database_check_reload_and_get, @function
nss_database_check_reload_and_get:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$456, %rsp
	movl	160(%rdi), %r14d
	testl	%r14d, %r14d
	je	.L18
	movl	%edx, %edx
	movl	$1, %r14d
	movq	48(%rdi,%rdx,8), %rax
	movq	%rax, (%rsi)
.L17:
	addq	$456, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	80(%rsp), %r12
	movq	%rsi, %rbp
	leaq	.LC4(%rip), %rsi
	movq	%rdi, %r15
	movl	%edx, %ebx
	movq	%r12, %rdi
	call	__file_change_detection_for_path
	testb	%al, %al
	movl	%eax, %r13d
	je	.L34
#APP
# 382 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	leaq	168(%r15), %rcx
	testl	%eax, %eax
	movl	$1, %edx
	movq	%rcx, 16(%rsp)
	jne	.L21
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 168(%r15)
# 0 "" 2
#NO_APP
.L22:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	__file_is_unchanged
	testb	%al, %al
	movl	%eax, %r14d
	je	.L23
.L64:
	movq	48(%r15,%rbx,8), %rax
	movq	%rax, 0(%rbp)
#APP
# 431 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L65
	subl	$1, 168(%r15)
	jmp	.L17
.L70:
	leaq	.LC1(%rip), %rdi
.L53:
	movq	%rsi, 40(%rsp)
	movq	%rcx, 32(%rsp)
	call	__nss_action_parse@PLT
	testq	%rax, %rax
	movq	%rax, 48(%rbx,%r14,8)
	movq	32(%rsp), %rcx
	movq	40(%rsp), %rsi
	jne	.L55
	movq	__libc_errno@gottpoff(%rip), %rbx
	movzbl	60(%rsp), %r14d
	cmpl	$12, %fs:(%rbx)
	jne	.L120
	cmpq	$0, 8(%rsp)
	jne	.L60
	.p2align 4,,10
	.p2align 3
.L34:
	xorl	%r14d, %r14d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L23:
	leaq	128(%rsp), %rsi
	leaq	.LC5(%rip), %rdi
	call	__stat64
	testl	%eax, %eax
	jne	.L26
	movq	176(%r15), %rax
	movq	136(%rsp), %rdx
	testq	%rax, %rax
	je	.L27
	cmpq	%rdx, %rax
	je	.L121
.L26:
	movl	$1, 160(%r15)
#APP
# 403 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L29
	subl	$1, 168(%r15)
.L30:
	call	__nss_module_disable_loading@PLT
.L117:
	movl	%r13d, %r14d
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L27:
	movq	128(%rsp), %rax
.L28:
	movq	%rdx, 176(%r15)
	movq	%rax, 184(%r15)
#APP
# 409 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L31
	subl	$1, 168(%r15)
.L32:
	leaq	272(%rsp), %rdi
	xorl	%eax, %eax
	movl	$21, %ecx
	leaq	.LC6(%rip), %rsi
	movq	%rdi, 48(%rsp)
	rep stosq
	leaq	.LC4(%rip), %rdi
	movb	$1, 436(%rsp)
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L122
	movq	8(%rsp), %rax
	orl	$32768, (%rax)
	leaq	72(%rsp), %rax
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	%rax, 32(%rsp)
	leaq	64(%rsp), %rax
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L36:
	movq	8(%rsp), %rdx
	movq	32(%rsp), %rsi
	movq	24(%rsp), %rdi
	call	__getline
	movq	8(%rsp), %rcx
	movl	(%rcx), %edx
	testb	$32, %dl
	jne	.L37
	andl	$16, %edx
	jne	.L38
	testq	%rax, %rax
	jle	.L123
	movq	64(%rsp), %rdi
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %r8
	movsbq	(%rdi), %rdx
	movq	%fs:(%r8), %rsi
	testb	$32, 1(%rsi,%rdx,2)
	movq	%rdx, %rax
	je	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$1, %rdi
	movsbq	(%rdi), %rdx
	testb	$32, 1(%rsi,%rdx,2)
	movq	%rdx, %rax
	jne	.L41
.L40:
	testb	%al, %al
	je	.L36
	testb	$32, 1(%rsi,%rdx,2)
	jne	.L36
	cmpb	$58, %al
	je	.L36
	movq	%rdi, %rdx
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L124:
	cmpb	$58, %al
	je	.L45
.L43:
	addq	$1, %rdx
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L36
	movsbq	%al, %rcx
	movzwl	(%rsi,%rcx,2), %ecx
	andw	$8192, %cx
	je	.L124
.L45:
	cmpq	%rdx, %rdi
	je	.L36
	testw	%cx, %cx
	jne	.L95
	cmpb	$58, %al
	jne	.L46
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$1, %rdx
	movzbl	(%rdx), %ecx
	movb	$0, -1(%rdx)
	testb	%cl, %cl
	je	.L46
	movq	%fs:(%r8), %rsi
	movsbq	%cl, %rax
	testb	$32, 1(%rsi,%rax,2)
	jne	.L95
	cmpb	$58, %cl
	je	.L95
.L46:
	movq	%rdx, 40(%rsp)
	call	name_to_database_index
	testl	%eax, %eax
	movl	%eax, 60(%rsp)
	js	.L36
	movq	40(%rsp), %rdx
	movq	%rdx, %rdi
	call	__nss_action_parse@PLT
	testq	%rax, %rax
	je	.L37
	movslq	60(%rsp), %rcx
	movq	%rax, 320(%rsp,%rcx,8)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	168(%r15), %rcx
	movl	%r14d, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L22
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L65:
	xorl	%eax, %eax
#APP
# 431 "nss_database.c" 1
	xchgl %eax, 168(%r15)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L117
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	16(%rsp), %rdi
	movl	$202, %eax
#APP
# 431 "nss_database.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L17
.L37:
	movq	64(%rsp), %rdi
	call	free@PLT
	movq	__libc_errno@gottpoff(%rip), %rbx
.L60:
	movl	%fs:(%rbx), %ebp
	movq	8(%rsp), %rdi
	call	_IO_new_fclose@PLT
	movl	%ebp, %fs:(%rbx)
	jmp	.L17
.L121:
	movq	184(%r15), %rax
	cmpq	%rax, 128(%rsp)
	jne	.L26
	jmp	.L28
.L122:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$40, %eax
	ja	.L34
	movabsq	$1099514781702, %rdx
	btq	%rax, %rdx
	jnc	.L34
.L35:
	xorl	%edx, %edx
	movb	%r14b, 60(%rsp)
	movl	%ebx, 24(%rsp)
	leaq	per_database_defaults(%rip), %rcx
	leaq	.L52(%rip), %rsi
	movq	%rdx, %r14
	movq	48(%rsp), %rbx
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$1, %r14
	cmpq	$14, %r14
	je	.L125
.L58:
	cmpq	$0, 48(%rbx,%r14,8)
	jne	.L55
	cmpb	$7, (%rcx,%r14)
	ja	.L50
	movzbl	(%rcx,%r14), %eax
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L52:
	.long	.L50-.L52
	.long	.L50-.L52
	.long	.L51-.L52
	.long	.L50-.L52
	.long	.L70-.L52
	.long	.L54-.L52
	.long	.L55-.L52
	.long	.L56-.L52
	.text
.L51:
	leaq	.LC3(%rip), %rdi
	jmp	.L53
.L54:
	leaq	.LC2(%rip), %rdi
	jmp	.L53
.L50:
	leaq	.LC0(%rip), %rdi
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L125:
	movq	48(%rsp), %rdi
	movq	8(%rsp), %rsi
	movl	24(%rsp), %ebx
	call	__file_change_detection_for_fp
	movq	8(%rsp), %rdi
	movl	%eax, %r14d
	testq	%rdi, %rdi
	je	.L61
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %edx
	movl	%edx, 8(%rsp)
	call	_IO_new_fclose@PLT
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	8(%rsp), %edx
	movl	%edx, %fs:(%rax)
.L61:
	testb	%r14b, %r14b
	je	.L34
	movq	48(%rsp), %rdi
	movq	%r12, %rsi
	call	__file_is_unchanged
	testb	%al, %al
	jne	.L62
	movq	$-1, 272(%rsp)
.L62:
#APP
# 421 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L126
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 168(%r15)
# 0 "" 2
#NO_APP
.L63:
	movl	160(%r15), %eax
	testl	%eax, %eax
	jne	.L64
	movdqa	272(%rsp), %xmm0
	movq	432(%rsp), %rax
	movups	%xmm0, (%r15)
	movdqa	288(%rsp), %xmm0
	movq	%rax, 160(%r15)
	movups	%xmm0, 16(%r15)
	movdqa	304(%rsp), %xmm0
	movups	%xmm0, 32(%r15)
	movdqa	320(%rsp), %xmm0
	movups	%xmm0, 48(%r15)
	movdqa	336(%rsp), %xmm0
	movups	%xmm0, 64(%r15)
	movdqa	352(%rsp), %xmm0
	movups	%xmm0, 80(%r15)
	movdqa	368(%rsp), %xmm0
	movups	%xmm0, 96(%r15)
	movdqa	384(%rsp), %xmm0
	movups	%xmm0, 112(%r15)
	movdqa	400(%rsp), %xmm0
	movups	%xmm0, 128(%r15)
	movdqa	416(%rsp), %xmm0
	movups	%xmm0, 144(%r15)
	jmp	.L64
.L29:
	xorl	%eax, %eax
#APP
# 403 "nss_database.c" 1
	xchgl %eax, 168(%r15)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L30
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	16(%rsp), %rdi
	movl	$202, %eax
#APP
# 403 "nss_database.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L30
.L38:
	movq	64(%rsp), %rdi
	call	free@PLT
	jmp	.L35
.L31:
	xorl	%eax, %eax
#APP
# 409 "nss_database.c" 1
	xchgl %eax, 168(%r15)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L32
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	168(%r15), %rdi
	movl	$202, %eax
#APP
# 409 "nss_database.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L32
.L126:
	movq	16(%rsp), %rcx
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rcx)
	je	.L63
	movq	16(%rsp), %rdi
	call	__lll_lock_wait_private
	jmp	.L63
.L123:
	leaq	__PRETTY_FUNCTION__.12399(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$289, %edx
	call	__assert_fail
.L56:
.L120:
	leaq	__PRETTY_FUNCTION__.12355(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$166, %edx
	call	__assert_fail
	.size	nss_database_check_reload_and_get, .-nss_database_check_reload_and_get
	.p2align 4,,15
	.globl	__nss_database_get
	.hidden	__nss_database_get
	.type	__nss_database_get, @function
__nss_database_get:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	movq	%rsi, %rbx
	subq	$8, %rsp
	movq	global_database_state(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L129
	leaq	global_database_state(%rip), %rdi
	leaq	global_state_allocate(%rip), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__libc_allocate_once_slow
	movq	%rax, %rdi
.L129:
	addq	$8, %rsp
	movl	%ebp, %edx
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	jmp	nss_database_check_reload_and_get
	.size	__nss_database_get, .-__nss_database_get
	.p2align 4,,15
	.globl	__nss_configure_lookup
	.type	__nss_configure_lookup, @function
__nss_configure_lookup:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	call	name_to_database_index
	movslq	%eax, %rbx
	testl	%ebx, %ebx
	js	.L136
	leaq	8(%rsp), %rsi
	movl	%ebx, %edi
	call	__nss_database_get
	movq	global_database_state(%rip), %rbp
	testq	%rbp, %rbp
	jne	.L135
	leaq	global_state_allocate(%rip), %rsi
	leaq	global_database_state(%rip), %rdi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__libc_allocate_once_slow
	movq	%rax, %rbp
.L135:
	movq	%r12, %rdi
	call	__nss_action_parse@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L136
	movl	$1, 160(%rbp)
	movq	8(%rsp), %rax
	movq	%rax, 48(%rbp,%rbx,8)
	xorl	%eax, %eax
.L131:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L136:
	movl	$-1, %eax
	jmp	.L131
	.size	__nss_configure_lookup, .-__nss_configure_lookup
	.section	.rodata.str1.1
.LC10:
	.string	"local != NULL"
	.text
	.p2align 4,,15
	.globl	__nss_database_get_noreload
	.hidden	__nss_database_get_noreload
	.type	__nss_database_get_noreload, @function
__nss_database_get_noreload:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	global_database_state(%rip), %rbx
	testq	%rbx, %rbx
	je	.L148
	movl	%edi, %ebp
#APP
# 451 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	168(%rbx), %r12
	jne	.L143
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 168(%rbx)
# 0 "" 2
#NO_APP
.L144:
	movq	48(%rbx,%rbp,8), %r8
#APP
# 453 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L145
	subl	$1, 168(%rbx)
.L141:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L144
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L145:
	xorl	%eax, %eax
#APP
# 453 "nss_database.c" 1
	xchgl %eax, 168(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L141
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 453 "nss_database.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L141
.L148:
	leaq	__PRETTY_FUNCTION__.12600(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$449, %edx
	call	__assert_fail
	.size	__nss_database_get_noreload, .-__nss_database_get_noreload
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	__nss_database_freeres
	.hidden	__nss_database_freeres
	.type	__nss_database_freeres, @function
__nss_database_freeres:
	subq	$8, %rsp
	movq	global_database_state(%rip), %rdi
	call	free@PLT
	movq	$0, global_database_state(%rip)
	addq	$8, %rsp
	ret
	.size	__nss_database_freeres, .-__nss_database_freeres
	.text
	.p2align 4,,15
	.globl	__nss_database_fork_prepare_parent
	.hidden	__nss_database_fork_prepare_parent
	.type	__nss_database_fork_prepare_parent, @function
__nss_database_fork_prepare_parent:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	global_database_state(%rip), %rbx
	testq	%rbx, %rbx
	je	.L158
	movq	%rdi, %rbp
#APP
# 476 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	168(%rbx), %r12
	jne	.L154
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r12)
# 0 "" 2
#NO_APP
.L155:
	movdqu	(%rbx), %xmm0
	movups	%xmm0, 0(%rbp)
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 16(%rbp)
	movdqu	32(%rbx), %xmm0
	movups	%xmm0, 32(%rbp)
	movdqu	48(%rbx), %xmm0
	movups	%xmm0, 48(%rbp)
	movdqu	64(%rbx), %xmm0
	movups	%xmm0, 64(%rbp)
	movdqu	80(%rbx), %xmm0
	movups	%xmm0, 80(%rbp)
	movdqu	96(%rbx), %xmm0
	movups	%xmm0, 96(%rbp)
	movdqu	112(%rbx), %xmm0
	movups	%xmm0, 112(%rbp)
	movdqu	128(%rbx), %xmm0
	movups	%xmm0, 128(%rbp)
	movdqu	144(%rbx), %xmm0
	movups	%xmm0, 144(%rbp)
	movq	160(%rbx), %rax
	movq	%rax, 160(%rbp)
#APP
# 478 "nss_database.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L156
	subl	$1, 168(%rbx)
.L151:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L158:
	movb	$0, 164(%rdi)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L155
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L155
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%eax, %eax
#APP
# 478 "nss_database.c" 1
	xchgl %eax, (%r12)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L151
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 478 "nss_database.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L151
	.size	__nss_database_fork_prepare_parent, .-__nss_database_fork_prepare_parent
	.p2align 4,,15
	.globl	__nss_database_fork_subprocess
	.hidden	__nss_database_fork_subprocess
	.type	__nss_database_fork_subprocess, @function
__nss_database_fork_subprocess:
	movq	global_database_state(%rip), %rax
	cmpb	$0, 164(%rdi)
	je	.L160
	testq	%rax, %rax
	je	.L169
	movdqu	(%rdi), %xmm0
	movups	%xmm0, (%rax)
	movdqu	16(%rdi), %xmm0
	movups	%xmm0, 16(%rax)
	movdqu	32(%rdi), %xmm0
	movups	%xmm0, 32(%rax)
	movdqu	48(%rdi), %xmm0
	movups	%xmm0, 48(%rax)
	movdqu	64(%rdi), %xmm0
	movups	%xmm0, 64(%rax)
	movdqu	80(%rdi), %xmm0
	movups	%xmm0, 80(%rax)
	movdqu	96(%rdi), %xmm0
	movups	%xmm0, 96(%rax)
	movdqu	112(%rdi), %xmm0
	movups	%xmm0, 112(%rax)
	movdqu	128(%rdi), %xmm0
	movups	%xmm0, 128(%rax)
	movdqu	144(%rdi), %xmm0
	movups	%xmm0, 144(%rax)
	movq	160(%rdi), %rdx
	movl	$0, 168(%rax)
	movq	%rdx, 160(%rax)
	ret
	.p2align 4,,10
	.p2align 3
.L160:
	testq	%rax, %rax
	je	.L159
	movq	$0, global_database_state(%rip)
.L159:
	rep ret
.L169:
	leaq	__PRETTY_FUNCTION__.12697(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	subq	$8, %rsp
	movl	$489, %edx
	call	__assert_fail
	.size	__nss_database_fork_subprocess, .-__nss_database_fork_subprocess
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12697, @object
	.size	__PRETTY_FUNCTION__.12697, 31
__PRETTY_FUNCTION__.12697:
	.string	"__nss_database_fork_subprocess"
	.align 16
	.type	__PRETTY_FUNCTION__.12600, @object
	.size	__PRETTY_FUNCTION__.12600, 28
__PRETTY_FUNCTION__.12600:
	.string	"__nss_database_get_noreload"
	.align 16
	.type	__PRETTY_FUNCTION__.12355, @object
	.size	__PRETTY_FUNCTION__.12355, 28
__PRETTY_FUNCTION__.12355:
	.string	"nss_database_select_default"
	.align 16
	.type	__PRETTY_FUNCTION__.12399, @object
	.size	__PRETTY_FUNCTION__.12399, 22
__PRETTY_FUNCTION__.12399:
	.string	"nss_database_reload_1"
	.section	.rodata
	.align 32
	.type	nss_database_name_array, @object
	.size	nss_database_name_array, 154
nss_database_name_array:
	.string	"aliases"
	.zero	3
	.string	"ethers"
	.zero	4
	.string	"group"
	.zero	5
	.string	"gshadow"
	.zero	3
	.string	"hosts"
	.zero	5
	.string	"initgroups"
	.string	"netgroup"
	.zero	2
	.string	"networks"
	.zero	2
	.string	"passwd"
	.zero	4
	.string	"protocols"
	.zero	1
	.string	"publickey"
	.zero	1
	.string	"rpc"
	.zero	7
	.string	"services"
	.zero	2
	.string	"shadow"
	.zero	4
	.align 8
	.type	per_database_defaults, @object
	.size	per_database_defaults, 14
per_database_defaults:
	.zero	2
	.byte	1
	.byte	3
	.byte	2
	.byte	6
	.zero	1
	.byte	2
	.byte	1
	.zero	1
	.byte	5
	.zero	2
	.byte	1
	.local	global_database_state
	.comm	global_database_state,8,8
	.hidden	__libc_allocate_once_slow
	.hidden	__assert_fail
	.hidden	__file_change_detection_for_fp
	.hidden	__lll_lock_wait_private
	.hidden	__getline
	.hidden	__stat64
	.hidden	__file_is_unchanged
	.hidden	__file_change_detection_for_path
	.hidden	strcmp
