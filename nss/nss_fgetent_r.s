	.text
	.p2align 4,,15
	.globl	__nss_fgetent_r
	.hidden	__nss_fgetent_r
	.type	__nss_fgetent_r, @function
__nss_fgetent_r:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%r8, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rbx
	movq	%rcx, %rbp
	subq	$24, %rsp
	leaq	8(%rsp), %r13
.L3:
	movq	%r13, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__nss_readline
	testl	%eax, %eax
	jne	.L1
	movq	__libc_errno@gottpoff(%rip), %r8
	addq	%fs:0, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	*%r14
	movq	8(%rsp), %rsi
	movl	%eax, %edx
	movq	%r12, %rdi
	call	__nss_parse_line_result
	cmpl	$22, %eax
	je	.L3
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__nss_fgetent_r, .-__nss_fgetent_r
	.hidden	__nss_parse_line_result
	.hidden	__nss_readline
