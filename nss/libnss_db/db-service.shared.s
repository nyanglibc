	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/db/services.db"
	.text
	.p2align 4,,15
	.globl	_nss_db_setservent
	.type	_nss_db_setservent, @function
_nss_db_setservent:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L3
	orl	%ebp, keep_db(%rip)
	movq	$0, entidx(%rip)
.L3:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_nss_db_setservent, .-_nss_db_setservent
	.p2align 4,,15
	.globl	_nss_db_endservent
	.type	_nss_db_endservent, @function
_nss_db_endservent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L14
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L14:
	leaq	state(%rip), %rdi
	call	internal_endent@PLT
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	$0, keep_db(%rip)
	je	.L15
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L15:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_db_endservent, .-_nss_db_endservent
	.p2align 4,,15
	.globl	_nss_db_getservent_r
	.type	_nss_db_getservent_r, @function
_nss_db_getservent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, 8(%rsp)
	je	.L24
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L24:
	movq	state(%rip), %rax
	testq	%rax, %rax
	je	.L47
	cmpq	$0, entidx(%rip)
	je	.L28
.L29:
	cmpq	$-1, %rax
	je	.L30
	movq	16(%rax), %r13
	movq	entidx(%rip), %r15
	addq	%rax, %r13
	addq	8(%rax), %r13
	cmpq	%r15, %r13
	ja	.L33
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	8(%rsp), %rsi
	movq	%r14, %r8
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r12, %rdi
	call	_nss_files_parse_servent@PLT
	cmpl	$0, %eax
	jg	.L48
	jne	.L35
	cmpq	%rbx, %r13
	movq	%rbx, entidx(%rip)
	movq	%rbx, %r15
	jbe	.L30
.L33:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	rawmemchr@PLT
	leaq	1(%rax), %rbx
	movq	%rbx, %rdx
	subq	%r15, %rdx
	cmpq	%rbp, %rdx
	jbe	.L31
	movl	$34, (%r14)
	movl	$-2, %ebx
.L27:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L23
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L23:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L26:
	movq	state(%rip), %rax
.L28:
	movq	8(%rax), %rdx
	addq	%rax, %rdx
	movq	%rdx, entidx(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rbx, entidx(%rip)
	movl	$1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$-2, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L26
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%r14)
	jmp	.L27
	.size	_nss_db_getservent_r, .-_nss_db_getservent_r
	.section	.rodata.str1.1
.LC1:
	.string	""
.LC2:
	.string	"%s/%s"
	.text
	.p2align 4,,15
	.globl	_nss_db_getservbyname_r
	.type	_nss_db_getservbyname_r, @function
_nss_db_getservbyname_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %r13
	leaq	.LC0(%rip), %rdi
	movq	%rdx, %r14
	movq	%r9, %rbx
	subq	$104, %rsp
	movq	%rsi, -112(%rbp)
	movq	%rax, %rsi
	movq	%rcx, -88(%rbp)
	movq	%r8, -72(%rbp)
	movq	%r9, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -144(%rbp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, -132(%rbp)
	je	.L50
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%rbx)
.L49:
	movl	-132(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	-64(%rbp), %r12
	movl	4(%r12), %edx
	testl	%edx, %edx
	je	.L67
	cmpb	$58, 32(%r12)
	je	.L68
	leaq	64(%r12), %rax
	xorl	%ebx, %ebx
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$32, %rax
	cmpb	$58, -32(%rax)
	je	.L66
.L54:
	addl	$1, %ebx
	cmpl	%ebx, %edx
	jne	.L55
.L67:
	movl	$-1, -132(%rbp)
.L52:
	movq	-144(%rbp), %rdi
	call	internal_endent@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r13, %rdi
	call	strlen@PLT
	movq	-112(%rbp), %r15
	leaq	3(%rax), %rsi
	testq	%r15, %r15
	je	.L56
	movq	%r15, %rdi
	movq	%rsi, -96(%rbp)
	call	strlen@PLT
	movq	-96(%rbp), %rsi
	movq	%r15, %r8
	addq	%rax, %rsi
	leaq	30(%rsi), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
.L65:
	movslq	%ebx, %rbx
	leaq	.LC2(%rip), %rdx
	movq	%r13, %rcx
	salq	$5, %rbx
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	%r12, %rbx
	movq	%r9, -128(%rbp)
	call	snprintf@PLT
	movq	40(%rbx), %rax
	movq	-128(%rbp), %r9
	movq	%rbx, -120(%rbp)
	movq	%rax, -96(%rbp)
	movq	8(%r12), %rax
	movq	%r9, %rdi
	movq	%rax, -104(%rbp)
	call	__hash_string
	movl	36(%rbx), %esi
	xorl	%edx, %edx
	movl	%eax, %ecx
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %ebx
	xorl	%edx, %edx
	divl	%esi
	leal	1(%rdx), %eax
	movq	%rax, -128(%rbp)
	.p2align 4,,10
	.p2align 3
.L57:
	movq	-96(%rbp), %rcx
	leaq	(%r12,%rbx,4), %rax
	movl	(%rax,%rcx), %esi
	cmpl	$-1, %esi
	je	.L87
	addq	-104(%rbp), %rsi
	leaq	(%r12,%rsi), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	-72(%rbp), %rdx
	ja	.L88
	movq	%r15, %rsi
	movq	-88(%rbp), %r15
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %rcx
	movq	-80(%rbp), %r8
	movq	%r15, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	_nss_files_parse_servent@PLT
	movq	-120(%rbp), %rcx
	addq	-128(%rbp), %rbx
	movl	36(%rcx), %edx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rbx, %rdx
	cmovbe	%rcx, %rbx
	testl	%eax, %eax
	jle	.L60
	movq	-112(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L61
	movq	24(%r14), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L57
.L61:
	movq	(%r14), %rsi
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L52
	movq	8(%r14), %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	jne	.L63
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$8, %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.L57
.L63:
	movq	%r13, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L89
	movq	-144(%rbp), %rdi
	call	internal_endent@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L56:
	addq	$33, %rax
	leaq	.LC1(%rip), %r8
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L60:
	cmpl	$-1, %eax
	jne	.L57
	movl	$-2, -132(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$0, -132(%rbp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L88:
	movq	-80(%rbp), %rax
	movl	$-2, -132(%rbp)
	movl	$34, (%rax)
	jmp	.L52
	.size	_nss_db_getservbyname_r, .-_nss_db_getservbyname_r
	.section	.rodata.str1.1
.LC3:
	.string	"%zd/%s"
	.text
	.p2align 4,,15
	.globl	_nss_db_getservbyport_r
	.type	_nss_db_getservbyport_r, @function
_nss_db_getservbyport_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	-64(%rbp), %rax
	pushq	%rbx
	movq	%rdx, %r15
	movq	%rcx, %r14
	movq	%r8, %r13
	movq	%r9, %rbx
	subq	$104, %rsp
	movl	%edi, -116(%rbp)
	leaq	.LC0(%rip), %rdi
	movq	%rsi, -128(%rbp)
	movq	%rax, %rsi
	movq	%r9, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -136(%rbp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, -120(%rbp)
	je	.L91
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%rbx)
.L90:
	movl	-120(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L91:
	movq	-64(%rbp), %r12
	movl	4(%r12), %edx
	testl	%edx, %edx
	je	.L106
	cmpb	$61, 32(%r12)
	je	.L107
	leaq	64(%r12), %rax
	xorl	%ebx, %ebx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L96:
	addq	$32, %rax
	cmpb	$61, -32(%rax)
	je	.L105
.L95:
	addl	$1, %ebx
	cmpl	%ebx, %edx
	jne	.L96
.L106:
	movl	$-1, -120(%rbp)
.L93:
	movq	-136(%rbp), %rdi
	call	internal_endent@PLT
	jmp	.L90
.L107:
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-128(%rbp), %rax
	testq	%rax, %rax
	je	.L97
	movq	%rax, %rdi
	call	strlen@PLT
	leaq	22(%rax), %rsi
	addq	$52, %rax
	movq	-128(%rbp), %r8
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
.L104:
	movzwl	-116(%rbp), %ecx
	movslq	%ebx, %rbx
	leaq	.LC3(%rip), %rdx
	salq	$5, %rbx
	movq	%r9, %rdi
	xorl	%eax, %eax
	addq	%r12, %rbx
	movq	%r9, -72(%rbp)
	rolw	$8, %cx
	movzwl	%cx, %ecx
	call	snprintf@PLT
	movq	40(%rbx), %rax
	movq	-72(%rbp), %r9
	movq	%rbx, -104(%rbp)
	movq	%rax, -88(%rbp)
	movq	8(%r12), %rax
	movq	%r9, %rdi
	movq	%rax, -96(%rbp)
	call	__hash_string
	movl	36(%rbx), %esi
	xorl	%edx, %edx
	movl	%eax, %ecx
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %ebx
	xorl	%edx, %edx
	divl	%esi
	leal	1(%rdx), %eax
	movq	%rax, -112(%rbp)
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-88(%rbp), %rdi
	leaq	(%r12,%rbx,4), %rax
	movl	(%rax,%rdi), %esi
	cmpl	$-1, %esi
	je	.L118
	addq	-96(%rbp), %rsi
	addq	%r12, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -72(%rbp)
	call	strlen@PLT
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rsi
	cmpq	%r13, %rdx
	ja	.L119
	movq	%r14, %rdi
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	_nss_files_parse_servent@PLT
	movq	-104(%rbp), %rcx
	addq	-112(%rbp), %rbx
	movl	36(%rcx), %edx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rbx, %rdx
	cmovbe	%rcx, %rbx
	testl	%eax, %eax
	jle	.L101
	movl	-116(%rbp), %eax
	cmpl	%eax, 16(%r15)
	jne	.L98
	movq	-128(%rbp), %rsi
	testq	%rsi, %rsi
	je	.L93
	movq	24(%r15), %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L98
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L97:
	subq	$48, %rsp
	movl	$22, %esi
	leaq	.LC1(%rip), %r8
	leaq	15(%rsp), %r9
	andq	$-16, %r9
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L101:
	cmpl	$-1, %eax
	jne	.L98
	movl	$-2, -120(%rbp)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L118:
	movl	$0, -120(%rbp)
	jmp	.L93
	.p2align 4,,10
	.p2align 3
.L119:
	movq	-80(%rbp), %rax
	movl	$-2, -120(%rbp)
	movl	$34, (%rax)
	jmp	.L93
	.size	_nss_db_getservbyport_r, .-_nss_db_getservbyport_r
	.local	entidx
	.comm	entidx,8,8
	.local	keep_db
	.comm	keep_db,4,4
	.local	lock
	.comm	lock,40,32
	.local	state
	.comm	state,16,16
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__hash_string
