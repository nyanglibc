	.text
	.p2align 4,,15
	.globl	internal_setent
	.type	internal_setent, @function
internal_setent:
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rsi, %rbp
	movl	$524288, %esi
	subq	$40, %rsp
	call	__open_nocancel@PLT
	cmpl	$-1, %eax
	movl	%eax, %ebx
	je	.L6
	movq	%rsp, %rsi
	movl	$32, %edx
	movl	%eax, %edi
	call	read@PLT
	cmpq	$32, %rax
	je	.L3
.L5:
	movl	$-1, %ebp
.L4:
	movl	%ebx, %edi
	call	__close_nocancel@PLT
.L1:
	addq	$40, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	24(%rsp), %rsi
	xorl	%r9d, %r9d
	movl	$1, %edx
	xorl	%edi, %edi
	movl	%ebx, %r8d
	movl	$2, %ecx
	call	mmap@PLT
	movq	24(%rsp), %rdx
	cmpq	$-1, %rax
	movq	%rax, 0(%rbp)
	movq	%rdx, 8(%rbp)
	movl	$1, %ebp
	jne	.L4
	movq	errno@gottpoff(%rip), %rax
	cmpl	$12, %fs:(%rax)
	jne	.L5
	movl	$-2, %ebp
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%eax, %ebp
	jmp	.L1
	.size	internal_setent, .-internal_setent
	.p2align 4,,15
	.globl	internal_endent
	.type	internal_endent, @function
internal_endent:
	movq	(%rdi), %rax
	testq	%rax, %rax
	je	.L16
	pushq	%rbx
	movq	8(%rdi), %rsi
	movq	%rdi, %rbx
	movq	%rax, %rdi
	call	munmap@PLT
	movq	$0, (%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	rep ret
	.size	internal_endent, .-internal_endent
