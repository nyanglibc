	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/db/protocols.db"
	.text
	.p2align 4,,15
	.globl	_nss_db_setprotoent
	.type	_nss_db_setprotoent, @function
_nss_db_setprotoent:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L3
	orl	%ebp, keep_db(%rip)
	movq	$0, entidx(%rip)
.L3:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_nss_db_setprotoent, .-_nss_db_setprotoent
	.p2align 4,,15
	.globl	_nss_db_endprotoent
	.type	_nss_db_endprotoent, @function
_nss_db_endprotoent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L14
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L14:
	leaq	state(%rip), %rdi
	call	internal_endent@PLT
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	$0, keep_db(%rip)
	je	.L15
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L15:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_db_endprotoent, .-_nss_db_endprotoent
	.p2align 4,,15
	.globl	_nss_db_getprotoent_r
	.type	_nss_db_getprotoent_r, @function
_nss_db_getprotoent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, 8(%rsp)
	je	.L24
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L24:
	movq	state(%rip), %rax
	testq	%rax, %rax
	je	.L47
	cmpq	$0, entidx(%rip)
	je	.L28
.L29:
	cmpq	$-1, %rax
	je	.L30
	movq	16(%rax), %r13
	movq	entidx(%rip), %r15
	addq	%rax, %r13
	addq	8(%rax), %r13
	cmpq	%r15, %r13
	ja	.L33
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	8(%rsp), %rsi
	movq	%r14, %r8
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r12, %rdi
	call	_nss_files_parse_protoent@PLT
	cmpl	$0, %eax
	jg	.L48
	jne	.L35
	cmpq	%rbx, %r13
	movq	%rbx, entidx(%rip)
	movq	%rbx, %r15
	jbe	.L30
.L33:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	rawmemchr@PLT
	leaq	1(%rax), %rbx
	movq	%rbx, %rdx
	subq	%r15, %rdx
	cmpq	%rbp, %rdx
	jbe	.L31
	movl	$34, (%r14)
	movl	$-2, %ebx
.L27:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L23
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L23:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L26:
	movq	state(%rip), %rax
.L28:
	movq	8(%rax), %rdx
	addq	%rax, %rdx
	movq	%rdx, entidx(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rbx, entidx(%rip)
	movl	$1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$-2, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L26
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%r14)
	jmp	.L27
	.size	_nss_db_getprotoent_r, .-_nss_db_getprotoent_r
	.p2align 4,,15
	.globl	_nss_db_getprotobyname_r
	.type	_nss_db_getprotobyname_r, @function
_nss_db_getprotobyname_r:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	leaq	.LC0(%rip), %rdi
	movq	%r8, %rbx
	subq	$88, %rsp
	leaq	64(%rsp), %rsi
	movq	%rdx, 8(%rsp)
	movq	%r8, (%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	%rsi, 56(%rsp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, 20(%rsp)
	je	.L50
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%rbx)
.L49:
	movl	20(%rsp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	64(%rsp), %r12
	movl	4(%r12), %ecx
	testl	%ecx, %ecx
	je	.L64
	cmpb	$46, 32(%r12)
	je	.L65
	leaq	64(%r12), %rdx
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$32, %rdx
	cmpb	$46, -32(%rdx)
	je	.L63
.L54:
	addl	$1, %eax
	cmpl	%eax, %ecx
	jne	.L55
.L64:
	movl	$-1, 20(%rsp)
.L52:
	movq	56(%rsp), %rdi
	call	internal_endent@PLT
	jmp	.L49
.L65:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L63:
	cltq
	movq	%rbp, %rdi
	salq	$5, %rax
	leaq	(%r12,%rax), %rbx
	movq	40(%rbx), %rax
	movq	%rbx, 40(%rsp)
	movq	%rax, 24(%rsp)
	movq	8(%r12), %rax
	movq	%rax, 32(%rsp)
	call	__hash_string
	movl	36(%rbx), %esi
	xorl	%edx, %edx
	movl	%eax, %ecx
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %ebx
	xorl	%edx, %edx
	divl	%esi
	leal	1(%rdx), %eax
	movq	%rax, 48(%rsp)
	.p2align 4,,10
	.p2align 3
.L56:
	movq	24(%rsp), %rdi
	leaq	(%r12,%rbx,4), %rax
	movl	(%rax,%rdi), %esi
	cmpl	$-1, %esi
	je	.L80
	addq	32(%rsp), %rsi
	leaq	(%r12,%rsi), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%r13, %rdx
	ja	.L81
	movq	%r14, %rsi
	movq	8(%rsp), %r14
	movq	%r14, %rdi
	call	memcpy@PLT
	movq	(%rsp), %r8
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	call	_nss_files_parse_protoent@PLT
	movq	40(%rsp), %rdi
	addq	48(%rsp), %rbx
	movl	36(%rdi), %edx
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rbx, %rdx
	cmovbe	%rcx, %rbx
	testl	%eax, %eax
	jle	.L59
	movq	(%r15), %rsi
	movq	%rbp, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	je	.L52
	movq	8(%r15), %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	jne	.L61
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$8, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.L56
.L61:
	movq	%rbp, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L82
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	$-1, %eax
	jne	.L56
	movl	$-2, 20(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$0, 20(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L81:
	movq	(%rsp), %rax
	movl	$-2, 20(%rsp)
	movl	$34, (%rax)
	jmp	.L52
	.size	_nss_db_getprotobyname_r, .-_nss_db_getprotobyname_r
	.section	.rodata.str1.1
.LC1:
	.string	"%zd"
	.text
	.p2align 4,,15
	.globl	_nss_db_getprotobynumber_r
	.type	_nss_db_getprotobynumber_r, @function
_nss_db_getprotobynumber_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	-64(%rbp), %rax
	pushq	%rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	%rax, %rsi
	movq	%rcx, %r12
	movq	%r8, %rbx
	subq	$88, %rsp
	movl	%edi, -88(%rbp)
	leaq	.LC0(%rip), %rdi
	movq	%r8, -80(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -128(%rbp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, -84(%rbp)
	je	.L84
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%rbx)
.L83:
	movl	-84(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movq	-64(%rbp), %rbx
	movl	4(%rbx), %edx
	testl	%edx, %edx
	je	.L97
	cmpb	$61, 32(%rbx)
	je	.L98
	leaq	64(%rbx), %rax
	xorl	%r8d, %r8d
	jmp	.L88
	.p2align 4,,10
	.p2align 3
.L89:
	addq	$32, %rax
	cmpb	$61, -32(%rax)
	je	.L96
.L88:
	addl	$1, %r8d
	cmpl	%r8d, %edx
	jne	.L89
.L97:
	movl	$-1, -84(%rbp)
.L86:
	movq	-128(%rbp), %rdi
	call	internal_endent@PLT
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L98:
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L96:
	subq	$48, %rsp
	movslq	-88(%rbp), %rcx
	leaq	.LC1(%rip), %rdx
	leaq	15(%rsp), %r9
	movl	$21, %esi
	xorl	%eax, %eax
	movl	%r8d, -72(%rbp)
	andq	$-16, %r9
	movq	%r9, %rdi
	movq	%r9, %r15
	call	snprintf@PLT
	movslq	-72(%rbp), %r8
	salq	$5, %r8
	leaq	(%rbx,%r8), %rax
	movq	40(%rax), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movq	8(%rbx), %rdi
	movq	%rdi, -112(%rbp)
	movq	%r15, %rdi
	call	__hash_string
	movl	%eax, %ecx
	movq	-96(%rbp), %rax
	xorl	%edx, %edx
	movl	36(%rax), %esi
	movl	%ecx, %eax
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %r15d
	xorl	%edx, %edx
	divl	%esi
	leal	1(%rdx), %eax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L90:
	movq	-104(%rbp), %rdi
	leaq	(%rbx,%r15,4), %rax
	movl	(%rax,%rdi), %esi
	cmpl	$-1, %esi
	je	.L103
	addq	-112(%rbp), %rsi
	addq	%rbx, %rsi
	movq	%rsi, %rdi
	movq	%rsi, -72(%rbp)
	call	strlen@PLT
	leaq	1(%rax), %rdx
	movq	-72(%rbp), %rsi
	cmpq	%r12, %rdx
	ja	.L104
	movq	%r13, %rdi
	call	memcpy@PLT
	movq	-80(%rbp), %r8
	movq	%r12, %rcx
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	_nss_files_parse_protoent@PLT
	movq	-96(%rbp), %rcx
	addq	-120(%rbp), %r15
	movl	36(%rcx), %edx
	movq	%r15, %rcx
	subq	%rdx, %rcx
	cmpq	%r15, %rdx
	cmovbe	%rcx, %r15
	testl	%eax, %eax
	jle	.L93
	movl	-88(%rbp), %eax
	cmpl	%eax, 16(%r14)
	jne	.L90
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L93:
	cmpl	$-1, %eax
	jne	.L90
	movl	$-2, -84(%rbp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L103:
	movl	$0, -84(%rbp)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L104:
	movq	-80(%rbp), %rax
	movl	$-2, -84(%rbp)
	movl	$34, (%rax)
	jmp	.L86
	.size	_nss_db_getprotobynumber_r, .-_nss_db_getprotobynumber_r
	.local	entidx
	.comm	entidx,8,8
	.local	keep_db
	.comm	keep_db,4,4
	.local	lock
	.comm	lock,40,32
	.local	state
	.comm	state,16,16
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__hash_string
