	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/db/ethers.db"
	.text
	.p2align 4,,15
	.globl	_nss_db_setetherent
	.type	_nss_db_setetherent, @function
_nss_db_setetherent:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L3
	orl	%ebp, keep_db(%rip)
	movq	$0, entidx(%rip)
.L3:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_nss_db_setetherent, .-_nss_db_setetherent
	.p2align 4,,15
	.globl	_nss_db_endetherent
	.type	_nss_db_endetherent, @function
_nss_db_endetherent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L14
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L14:
	leaq	state(%rip), %rdi
	call	internal_endent@PLT
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	$0, keep_db(%rip)
	je	.L15
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L15:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_db_endetherent, .-_nss_db_endetherent
	.p2align 4,,15
	.globl	_nss_db_getetherent_r
	.type	_nss_db_getetherent_r, @function
_nss_db_getetherent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, 8(%rsp)
	je	.L24
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L24:
	movq	state(%rip), %rax
	testq	%rax, %rax
	je	.L47
	cmpq	$0, entidx(%rip)
	je	.L28
.L29:
	cmpq	$-1, %rax
	je	.L30
	movq	16(%rax), %r13
	movq	entidx(%rip), %r15
	addq	%rax, %r13
	addq	8(%rax), %r13
	cmpq	%r15, %r13
	ja	.L33
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	8(%rsp), %rsi
	movq	%r14, %r8
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r12, %rdi
	call	_nss_files_parse_etherent@PLT
	cmpl	$0, %eax
	jg	.L48
	jne	.L35
	cmpq	%rbx, %r13
	movq	%rbx, entidx(%rip)
	movq	%rbx, %r15
	jbe	.L30
.L33:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	rawmemchr@PLT
	leaq	1(%rax), %rbx
	movq	%rbx, %rdx
	subq	%r15, %rdx
	cmpq	%rbp, %rdx
	jbe	.L31
	movl	$34, (%r14)
	movl	$-2, %ebx
.L27:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L23
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L23:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L26:
	movq	state(%rip), %rax
.L28:
	movq	8(%rax), %rdx
	addq	%rax, %rdx
	movq	%rdx, entidx(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rbx, entidx(%rip)
	movl	$1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$-2, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L26
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%r14)
	jmp	.L27
	.size	_nss_db_getetherent_r, .-_nss_db_getetherent_r
	.p2align 4,,15
	.globl	_nss_db_gethostton_r
	.type	_nss_db_gethostton_r, @function
_nss_db_gethostton_r:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rcx, %rbp
	subq	$88, %rsp
	movq	%rdi, 8(%rsp)
	leaq	64(%rsp), %rsi
	leaq	.LC0(%rip), %rdi
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	%rsi, 56(%rsp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, 4(%rsp)
	je	.L50
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%r15)
.L49:
	movl	4(%rsp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	64(%rsp), %rbx
	movl	4(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L63
	cmpb	$46, 32(%rbx)
	je	.L64
	leaq	64(%rbx), %rdx
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$32, %rdx
	cmpb	$46, -32(%rdx)
	je	.L62
.L54:
	addl	$1, %eax
	cmpl	%ecx, %eax
	jne	.L55
.L63:
	movl	$-1, 4(%rsp)
.L52:
	movq	56(%rsp), %rdi
	call	internal_endent@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L62:
	cltq
	movq	8(%rsp), %rdi
	salq	$5, %rax
	leaq	(%rbx,%rax), %r14
	movq	40(%r14), %rax
	movq	%r14, 48(%rsp)
	movq	%rax, 16(%rsp)
	movq	8(%rbx), %rax
	movq	%rax, 24(%rsp)
	call	__hash_string
	movl	36(%r14), %esi
	xorl	%edx, %edx
	movl	%eax, %ecx
	movq	%r15, 32(%rsp)
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %r14d
	xorl	%edx, %edx
	divl	%esi
	movq	%r14, %r15
	leal	1(%rdx), %eax
	movq	%rax, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L56:
	movq	16(%rsp), %rdi
	leaq	(%rbx,%r15,4), %rax
	movl	(%rax,%rdi), %esi
	cmpl	$-1, %esi
	je	.L72
	addq	24(%rsp), %rsi
	leaq	(%rbx,%rsi), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%rbp, %rdx
	ja	.L73
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	32(%rsp), %r8
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	_nss_files_parse_etherent@PLT
	movq	48(%rsp), %rcx
	addq	40(%rsp), %r15
	movl	36(%rcx), %edx
	movq	%r15, %rcx
	subq	%rdx, %rcx
	cmpq	%r15, %rdx
	cmovbe	%rcx, %r15
	testl	%eax, %eax
	jle	.L59
	movq	8(%rsp), %rsi
	movq	0(%r13), %rdi
	call	__strcasecmp@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	$-1, %eax
	jne	.L56
	movl	$-2, 4(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L72:
	movl	$0, 4(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L73:
	movq	32(%rsp), %r15
	movl	$-2, 4(%rsp)
	movl	$34, (%r15)
	jmp	.L52
	.size	_nss_db_gethostton_r, .-_nss_db_gethostton_r
	.section	.rodata.str1.1
.LC1:
	.string	"%x:%x:%x:%x:%x:%x"
	.text
	.p2align 4,,15
	.globl	_nss_db_getntohost_r
	.type	_nss_db_getntohost_r, @function
_nss_db_getntohost_r:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	leaq	-64(%rbp), %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdi, %r14
	leaq	.LC0(%rip), %rdi
	movq	%rsi, %r12
	movq	%rax, %rsi
	movq	%rcx, %r13
	subq	$104, %rsp
	movq	%r8, %rbx
	movq	%rdx, -80(%rbp)
	movq	%r8, -72(%rbp)
	movq	$0, -64(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -136(%rbp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, -84(%rbp)
	je	.L75
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%rbx)
.L74:
	movl	-84(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	movq	-64(%rbp), %rbx
	movl	4(%rbx), %edx
	testl	%edx, %edx
	je	.L90
	cmpb	$61, 32(%rbx)
	je	.L91
	leaq	64(%rbx), %rax
	xorl	%r10d, %r10d
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L80:
	addq	$32, %rax
	cmpb	$61, -32(%rax)
	je	.L89
.L79:
	addl	$1, %r10d
	cmpl	%r10d, %edx
	jne	.L80
.L90:
	movl	$-1, -84(%rbp)
.L77:
	movq	-136(%rbp), %rdi
	call	internal_endent@PLT
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%r10d, %r10d
	.p2align 4,,10
	.p2align 3
.L89:
	movzbl	5(%r14), %eax
	subq	$48, %rsp
	movzbl	(%r14), %ecx
	leaq	15(%rsp), %r11
	subq	$8, %rsp
	leaq	.LC1(%rip), %rdx
	movl	$19, %esi
	movl	%r10d, -96(%rbp)
	andq	$-16, %r11
	pushq	%rax
	movzbl	4(%r14), %eax
	movq	%r11, %rdi
	movq	%r11, %r15
	pushq	%rax
	movzbl	3(%r14), %eax
	pushq	%rax
	movzbl	2(%r14), %r9d
	xorl	%eax, %eax
	movzbl	1(%r14), %r8d
	call	snprintf@PLT
	movslq	-96(%rbp), %r10
	movq	8(%rbx), %rsi
	addq	$32, %rsp
	movq	%rsi, -112(%rbp)
	salq	$5, %r10
	leaq	(%rbx,%r10), %rax
	movq	40(%rax), %rdi
	movq	%rax, -96(%rbp)
	movq	%rdi, -104(%rbp)
	movq	%r15, %rdi
	call	__hash_string
	movl	%eax, %ecx
	movq	-96(%rbp), %rax
	xorl	%edx, %edx
	movq	%r14, -128(%rbp)
	movl	36(%rax), %esi
	movl	%ecx, %eax
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %r15d
	xorl	%edx, %edx
	divl	%esi
	movq	%r15, %r14
	leal	1(%rdx), %eax
	movq	%rax, -120(%rbp)
	.p2align 4,,10
	.p2align 3
.L81:
	movq	-104(%rbp), %rdi
	leaq	(%rbx,%r14,4), %rax
	movl	(%rax,%rdi), %esi
	cmpl	$-1, %esi
	je	.L97
	movl	%esi, %r15d
	addq	-112(%rbp), %r15
	addq	%rbx, %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%r13, %rdx
	ja	.L98
	movq	%r15, %rsi
	movq	-80(%rbp), %r15
	movq	%r15, %rdi
	call	memcpy@PLT
	movq	-72(%rbp), %r8
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r15, %rdi
	call	_nss_files_parse_etherent@PLT
	movq	-96(%rbp), %rsi
	addq	-120(%rbp), %r14
	movl	36(%rsi), %edx
	movq	%r14, %rcx
	subq	%rdx, %rcx
	cmpq	%r14, %rdx
	cmovbe	%rcx, %r14
	testl	%eax, %eax
	jle	.L84
	movq	-128(%rbp), %rcx
	movl	(%rcx), %ecx
	cmpl	%ecx, 8(%r12)
	jne	.L81
	movq	-128(%rbp), %rdi
	movzwl	4(%rdi), %esi
	cmpw	%si, 12(%r12)
	jne	.L81
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L84:
	cmpl	$-1, %eax
	jne	.L81
	movl	$-2, -84(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$0, -84(%rbp)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L98:
	movq	-72(%rbp), %rax
	movl	$-2, -84(%rbp)
	movl	$34, (%rax)
	jmp	.L77
	.size	_nss_db_getntohost_r, .-_nss_db_getntohost_r
	.local	entidx
	.comm	entidx,8,8
	.local	keep_db
	.comm	keep_db,4,4
	.local	lock
	.comm	lock,40,32
	.local	state
	.comm	state,16,16
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__hash_string
