	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/db/group.db"
	.text
	.p2align 4,,15
	.globl	_nss_db_initgroups_dyn
	.type	_nss_db_initgroups_dyn, @function
_nss_db_initgroups_dyn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$120, %rsp
	leaq	96(%rsp), %rax
	movq	%rdi, (%rsp)
	leaq	.LC0(%rip), %rdi
	movq	%rdx, 56(%rsp)
	movq	%rcx, 64(%rsp)
	movq	%rax, %rsi
	movq	%r8, 48(%rsp)
	movq	%r9, 72(%rsp)
	movq	$0, 96(%rsp)
	movq	$0, 104(%rsp)
	movq	%rax, 40(%rsp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, 36(%rsp)
	je	.L2
	movq	errno@gottpoff(%rip), %rax
	movq	176(%rsp), %rsi
	movl	%fs:(%rax), %eax
	movl	%eax, (%rsi)
.L1:
	movl	36(%rsp), %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	96(%rsp), %rbp
	movl	4(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L31
	cmpb	$58, 32(%rbp)
	je	.L32
	leaq	64(%rbp), %rdx
	xorl	%eax, %eax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$32, %rdx
	cmpb	$58, -32(%rdx)
	je	.L30
.L6:
	addl	$1, %eax
	cmpl	%ecx, %eax
	jne	.L7
.L31:
	movl	$-1, 36(%rsp)
.L4:
	movq	40(%rsp), %rdi
	call	internal_endent@PLT
	jmp	.L1
.L32:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L30:
	cltq
	movq	(%rsp), %r14
	salq	$5, %rax
	leaq	0(%rbp,%rax), %rbx
	movq	8(%rbp), %rax
	movq	%r14, %rdi
	movq	40(%rbx), %r15
	movq	%rax, 8(%rsp)
	call	strlen@PLT
	movq	%r14, %rdi
	movq	%rax, %r12
	leaq	0(%rbp,%r15), %r14
	call	__hash_string
	movl	36(%rbx), %ebx
	xorl	%edx, %edx
	movl	%eax, %ecx
	movq	%rbp, 16(%rsp)
	divl	%ebx
	leal	-2(%rbx), %esi
	movl	%ecx, %eax
	movl	%edx, %r13d
	xorl	%edx, %edx
	divl	%esi
	movl	(%r14,%r13,4), %ecx
	cmpl	$-1, %ecx
	leal	1(%rdx), %eax
	movq	%rax, 24(%rsp)
	je	.L53
	.p2align 4,,10
	.p2align 3
.L29:
	addq	8(%rsp), %rcx
	movq	16(%rsp), %rax
	leaq	(%rax,%rcx), %rbp
	call	__ctype_b_loc@PLT
	movq	(%rax), %r15
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$1, %rbp
.L9:
	movsbq	0(%rbp), %rax
	testb	$1, (%r15,%rax,2)
	jne	.L10
	movq	(%rsp), %rsi
	movq	%r12, %rdx
	movq	%rbp, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L11
	movsbq	0(%rbp,%r12), %rax
	testb	$1, (%r15,%rax,2)
	jne	.L54
.L11:
	addq	24(%rsp), %r13
	movq	%r13, %rdx
	movq	%r13, %rax
	subq	%rbx, %rdx
	cmpq	%r13, %rbx
	cmovbe	%rdx, %rax
	movq	%rax, %r13
	movl	(%r14,%r13,4), %ecx
	cmpl	$-1, %ecx
	jne	.L29
.L53:
	movl	$0, 36(%rsp)
	jmp	.L4
.L54:
	leaq	1(%rbp,%r12), %r13
	movsbq	0(%r13), %rdx
	testb	$1, (%r15,%rdx,2)
	movq	%rdx, %rax
	je	.L12
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %r13
	movsbq	0(%r13), %rdx
	testb	$1, (%r15,%rdx,2)
	movq	%rdx, %rax
	jne	.L13
.L12:
	testb	%al, %al
	je	.L4
	movq	48(%rsp), %rax
	movq	%r13, %rcx
	movq	errno@gottpoff(%rip), %rbp
	leaq	88(%rsp), %r12
	movq	56(%rsp), %r14
	movq	64(%rsp), %r15
	movq	72(%rsp), %r13
	movq	(%rax), %rbx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L18:
	movq	(%r14), %rax
	cmpq	(%r15), %rax
	je	.L55
.L20:
	movl	%r8d, (%rbx,%rax,4)
	addq	$1, %rax
	movq	%rax, (%r14)
.L19:
	cmpb	$0, (%rcx)
	je	.L4
.L26:
	movq	%rcx, %rdi
	movl	$10, %edx
	movq	%r12, %rsi
	movl	$0, %fs:0(%rbp)
	call	strtoul@PLT
	movq	88(%rsp), %rcx
	movq	%rax, %r8
	movzbl	(%rcx), %eax
	cmpb	$44, %al
	je	.L34
	testb	%al, %al
	jne	.L4
.L34:
	cmpb	$1, %al
	sbbq	$-1, %rcx
	cmpq	$-1, %r8
	jne	.L18
	cmpl	$34, %fs:0(%rbp)
	je	.L19
	movq	(%r14), %rax
	cmpq	(%r15), %rax
	jne	.L20
.L55:
	testq	%r13, %r13
	jle	.L21
	cmpq	%r13, %rax
	je	.L4
	addq	%rax, %rax
	cmpq	%r13, %rax
	cmovg	%r13, %rax
	movq	%rax, %rdx
.L24:
	leaq	0(,%rdx,4), %rsi
	movq	%rbx, %rdi
	movq	%rcx, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%rdx, (%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	(%rsp), %rdx
	movq	8(%rsp), %r8
	movq	16(%rsp), %rcx
	je	.L56
	movq	48(%rsp), %rax
	movq	%rbx, (%rax)
	movq	%rdx, (%r15)
	movq	(%r14), %rax
	jmp	.L20
.L21:
	leaq	(%rax,%rax), %rdx
	jmp	.L24
.L56:
	movq	176(%rsp), %rax
	movl	$-2, 36(%rsp)
	movl	$12, (%rax)
	jmp	.L4
	.size	_nss_db_initgroups_dyn, .-_nss_db_initgroups_dyn
	.hidden	__hash_string
