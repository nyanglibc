	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/db/netgroup.db"
	.text
	.p2align 4,,15
	.globl	_nss_db_setnetgrent
	.type	_nss_db_setnetgrent, @function
_nss_db_setnetgrent:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movq	%rdi, 8(%rsp)
	movq	%rsi, 48(%rsp)
	leaq	.LC0(%rip), %rdi
	leaq	64(%rsp), %rsi
	movq	%rsi, 40(%rsp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, 36(%rsp)
	je	.L24
.L1:
	movl	36(%rsp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	64(%rsp), %r14
	movq	8(%rsp), %rbp
	movq	8(%r14), %rax
	movq	%rbp, %rdi
	movq	40(%r14), %r15
	movq	%rax, 16(%rsp)
	addq	%r14, %r15
	call	__hash_string
	movq	%rbp, %rdi
	movl	%eax, %ebx
	call	strlen@PLT
	movl	36(%r14), %ebp
	movq	%rax, %r12
	xorl	%edx, %edx
	movl	%ebx, %eax
	divl	%ebp
	leal	-2(%rbp), %esi
	movl	%ebx, %eax
	movl	%edx, %r13d
	xorl	%edx, %edx
	divl	%esi
	leal	1(%rdx), %eax
	movq	%rax, 24(%rsp)
	leaq	1(%r12), %rax
	movq	%rax, 56(%rsp)
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	addq	24(%rsp), %r13
	movq	%r13, %rdx
	movq	%r13, %rax
	subq	%rbp, %rdx
	cmpq	%r13, %rbp
	cmovbe	%rdx, %rax
	movq	%rax, %r13
.L8:
	movl	(%r15,%r13,4), %ebx
	cmpl	$-1, %ebx
	je	.L25
	addq	16(%rsp), %rbx
	movq	8(%rsp), %rsi
	movq	%r12, %rdx
	addq	%r14, %rbx
	movq	%rbx, %rdi
	call	strncmp@PLT
	testl	%eax, %eax
	jne	.L4
	call	__ctype_b_loc@PLT
	movq	(%rax), %rdx
	movsbq	(%rbx,%r12), %rax
	testb	$1, (%rdx,%rax,2)
	je	.L4
	addq	56(%rsp), %rbx
	movsbq	(%rbx), %rsi
	testb	$1, (%rdx,%rsi,2)
	movq	%rsi, %rax
	je	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %rbx
	movsbq	(%rbx), %rcx
	testb	$1, (%rdx,%rcx,2)
	movq	%rcx, %rax
	jne	.L6
.L5:
	testb	%al, %al
	je	.L4
	movq	%rbx, %rdi
	call	strdup@PLT
	movq	48(%rsp), %rdi
	testq	%rax, %rax
	movq	%rax, 32(%rdi)
	je	.L10
	movq	%rax, 48(%rdi)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L25:
	movl	$0, 36(%rsp)
.L7:
	movq	40(%rsp), %rdi
	call	internal_endent@PLT
	jmp	.L1
.L10:
	movl	$-2, 36(%rsp)
	jmp	.L7
	.size	_nss_db_setnetgrent, .-_nss_db_setnetgrent
	.p2align 4,,15
	.globl	_nss_db_endnetgrent
	.type	_nss_db_endnetgrent, @function
_nss_db_endnetgrent:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	free@PLT
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	movl	$1, %eax
	movq	$0, 48(%rbx)
	popq	%rbx
	ret
	.size	_nss_db_endnetgrent, .-_nss_db_endnetgrent
	.p2align 4,,15
	.globl	_nss_db_getnetgrent_r
	.type	_nss_db_getnetgrent_r, @function
_nss_db_getnetgrent_r:
	movq	%rdi, %rax
	leaq	48(%rdi), %rdi
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	%rax, %rsi
	jmp	_nss_netgroup_parseline@PLT
	.size	_nss_db_getnetgrent_r, .-_nss_db_getnetgrent_r
	.hidden	__hash_string
