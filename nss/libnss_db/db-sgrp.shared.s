	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/db/gshadow.db"
	.text
	.p2align 4,,15
	.globl	_nss_db_setsgent
	.type	_nss_db_setsgent, @function
_nss_db_setsgent:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L2
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L2:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	jne	.L3
	orl	%ebp, keep_db(%rip)
	movq	$0, entidx(%rip)
.L3:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L1
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L1:
	addq	$8, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	_nss_db_setsgent, .-_nss_db_setsgent
	.p2align 4,,15
	.globl	_nss_db_endsgent
	.type	_nss_db_endsgent, @function
_nss_db_endsgent:
	subq	$8, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	je	.L14
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L14:
	leaq	state(%rip), %rdi
	call	internal_endent@PLT
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	movl	$0, keep_db(%rip)
	je	.L15
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L15:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.size	_nss_db_endsgent, .-_nss_db_endsgent
	.p2align 4,,15
	.globl	_nss_db_getsgent_r
	.type	_nss_db_getsgent_r, @function
_nss_db_getsgent_r:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$24, %rsp
	cmpq	$0, __pthread_mutex_lock@GOTPCREL(%rip)
	movq	%rdi, 8(%rsp)
	je	.L24
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_lock@PLT
.L24:
	movq	state(%rip), %rax
	testq	%rax, %rax
	je	.L47
	cmpq	$0, entidx(%rip)
	je	.L28
.L29:
	cmpq	$-1, %rax
	je	.L30
	movq	16(%rax), %r13
	movq	entidx(%rip), %r15
	addq	%rax, %r13
	addq	8(%rax), %r13
	cmpq	%r15, %r13
	ja	.L33
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	8(%rsp), %rsi
	movq	%r14, %r8
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r12, %rdi
	call	_nss_files_parse_sgent@PLT
	cmpl	$0, %eax
	jg	.L48
	jne	.L35
	cmpq	%rbx, %r13
	movq	%rbx, entidx(%rip)
	movq	%rbx, %r15
	jbe	.L30
.L33:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	rawmemchr@PLT
	leaq	1(%rax), %rbx
	movq	%rbx, %rdx
	subq	%r15, %rdx
	cmpq	%rbp, %rdx
	jbe	.L31
	movl	$34, (%r14)
	movl	$-2, %ebx
.L27:
	cmpq	$0, __pthread_mutex_unlock@GOTPCREL(%rip)
	je	.L23
	leaq	lock(%rip), %rdi
	call	__pthread_mutex_unlock@PLT
.L23:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L26:
	movq	state(%rip), %rax
.L28:
	movq	8(%rax), %rdx
	addq	%rax, %rdx
	movq	%rdx, entidx(%rip)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L30:
	movl	$-1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L48:
	movq	%rbx, entidx(%rip)
	movl	$1, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L35:
	movl	$-2, %ebx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	state(%rip), %rsi
	leaq	.LC0(%rip), %rdi
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, %ebx
	je	.L26
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, (%r14)
	jmp	.L27
	.size	_nss_db_getsgent_r, .-_nss_db_getsgent_r
	.p2align 4,,15
	.globl	_nss_db_getsgnam_r
	.type	_nss_db_getsgnam_r, @function
_nss_db_getsgnam_r:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rcx, %rbp
	movq	%r8, %r13
	subq	$88, %rsp
	movq	%rsi, (%rsp)
	leaq	64(%rsp), %rsi
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	%rsi, 56(%rsp)
	call	internal_setent@PLT
	cmpl	$1, %eax
	movl	%eax, 12(%rsp)
	je	.L50
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	movl	%eax, 0(%r13)
.L49:
	movl	12(%rsp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	64(%rsp), %rbx
	movl	4(%rbx), %ecx
	testl	%ecx, %ecx
	je	.L63
	cmpb	$46, 32(%rbx)
	je	.L64
	leaq	64(%rbx), %rdx
	xorl	%eax, %eax
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L55:
	addq	$32, %rdx
	cmpb	$46, -32(%rdx)
	je	.L62
.L54:
	addl	$1, %eax
	cmpl	%eax, %ecx
	jne	.L55
.L63:
	movl	$-1, 12(%rsp)
.L52:
	movq	56(%rsp), %rdi
	call	internal_endent@PLT
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L64:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L62:
	cltq
	movq	%r15, %rdi
	salq	$5, %rax
	leaq	(%rbx,%rax), %r14
	movq	40(%r14), %rax
	movq	%r14, 48(%rsp)
	movq	%rax, 16(%rsp)
	movq	8(%rbx), %rax
	movq	%rax, 24(%rsp)
	call	__hash_string
	movl	36(%r14), %esi
	xorl	%edx, %edx
	movl	%eax, %ecx
	movq	%r13, 32(%rsp)
	divl	%esi
	subl	$2, %esi
	movl	%ecx, %eax
	movl	%edx, %r14d
	xorl	%edx, %edx
	divl	%esi
	movq	%r14, %r13
	leal	1(%rdx), %eax
	movq	%rax, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L56:
	movq	16(%rsp), %rdi
	leaq	(%rbx,%r13,4), %rax
	movl	(%rax,%rdi), %esi
	cmpl	$-1, %esi
	je	.L75
	addq	24(%rsp), %rsi
	leaq	(%rbx,%rsi), %r14
	movq	%r14, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%rbp, %rdx
	ja	.L76
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movq	(%rsp), %r14
	movq	32(%rsp), %r8
	movq	%rbp, %rcx
	movq	%r12, %rdx
	movq	%r12, %rdi
	movq	%r14, %rsi
	call	_nss_files_parse_sgent@PLT
	movq	48(%rsp), %rcx
	addq	40(%rsp), %r13
	movl	36(%rcx), %edx
	movq	%r13, %rcx
	subq	%rdx, %rcx
	cmpq	%r13, %rdx
	cmovbe	%rcx, %r13
	testl	%eax, %eax
	jle	.L59
	movzbl	(%r15), %eax
	subl	$43, %eax
	testb	$-3, %al
	je	.L56
	movq	(%r14), %rsi
	movq	%r15, %rdi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L56
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	$-1, %eax
	jne	.L56
	movl	$-2, 12(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L75:
	movl	$0, 12(%rsp)
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L76:
	movq	32(%rsp), %r13
	movl	$-2, 12(%rsp)
	movl	$34, 0(%r13)
	jmp	.L52
	.size	_nss_db_getsgnam_r, .-_nss_db_getsgnam_r
	.local	entidx
	.comm	entidx,8,8
	.local	keep_db
	.comm	keep_db,4,4
	.local	lock
	.comm	lock,40,32
	.local	state
	.comm	state,16,16
	.weak	__pthread_mutex_unlock
	.weak	__pthread_mutex_lock
	.hidden	__hash_string
