	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"aliases"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___nss_database_lookup2
	.hidden	__GI___nss_database_lookup2
	.type	__GI___nss_database_lookup2, @function
__GI___nss_database_lookup2:
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	leaq	database_names(%rip), %r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	leaq	.LC0(%rip), %rdi
	pushq	%rbx
	xorl	%ebx, %ebx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$1, %rbx
	movq	0(%r13,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.L4
.L3:
	movq	%r12, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L10
	movq	%r14, %rsi
	movl	%ebx, %edi
	call	__nss_database_get
	testb	%al, %al
	je	.L4
	cmpq	$0, (%r14)
	je	.L4
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$-1, %ebp
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI___nss_database_lookup2, .-__GI___nss_database_lookup2
	.globl	__nss_database_lookup2
	.set	__nss_database_lookup2,__GI___nss_database_lookup2
	.p2align 4,,15
	.globl	__GI___nss_lookup
	.hidden	__GI___nss_lookup
	.type	__GI___nss_lookup, @function
__GI___nss_lookup:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	(%rdi), %rax
	movq	%rsi, %rbp
	movq	%rcx, %rbx
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L39
	call	__nss_module_get_function
	testq	%rax, %rax
	movq	%rax, %r14
	movq	%rax, (%rbx)
	je	.L13
.L18:
	testq	%r14, %r14
	jne	.L25
.L42:
	movq	0(%r13), %rax
	testb	$12, 8(%rax)
	movq	16(%rax), %rdi
	jne	.L21
	testq	%rdi, %rdi
	je	.L40
	addq	$16, %rax
	movq	%rbp, %rsi
	movq	%rax, 0(%r13)
	call	__nss_module_get_function
	testq	%rax, %rax
	movq	%rax, (%rbx)
	jne	.L24
	testq	%r12, %r12
	jne	.L41
.L24:
	movq	%rax, %r14
	testq	%r14, %r14
	je	.L42
.L25:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	movq	0(%r13), %rax
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	je	.L19
.L38:
	movq	%r12, %rsi
	call	__nss_module_get_function
	movq	%rax, %r14
.L19:
	movq	%r14, (%rbx)
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L39:
	movq	$0, (%rcx)
.L13:
	testq	%r12, %r12
	je	.L43
	movq	0(%r13), %rax
	xorl	%r14d, %r14d
	movq	(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L38
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	cmpq	$1, %rdi
	sbbl	%eax, %eax
	andl	$2, %eax
	popq	%rbx
	subl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	(%rbx), %r14
	jmp	.L18
	.size	__GI___nss_lookup, .-__GI___nss_lookup
	.globl	__nss_lookup
	.set	__nss_lookup,__GI___nss_lookup
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"Illegal status in __nss_next.\n"
	.text
	.p2align 4,,15
	.globl	__GI___nss_next2
	.hidden	__GI___nss_next2
	.type	__GI___nss_next2, @function
__GI___nss_next2:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	%rcx, %rbp
	subq	$8, %rsp
	testl	%r9d, %r9d
	je	.L45
	movq	(%rdi), %rdx
	movl	8(%rdx), %eax
	movl	%eax, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	je	.L65
.L46:
	movq	16(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L54
	.p2align 4,,10
	.p2align 3
.L49:
	addq	$16, %rdx
	movq	%r12, %rsi
	movq	%rdx, (%rbx)
	call	__nss_module_get_function
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L50
.L53:
	xorl	%eax, %eax
.L44:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movq	(%rbx), %rdx
	testq	%r13, %r13
	movq	%rdx, %rax
	je	.L56
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L52
	movq	%r13, %rsi
	call	__nss_module_get_function
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	jne	.L53
	movq	(%rbx), %rax
.L52:
	movq	%rax, %rdx
.L56:
	testb	$12, 8(%rax)
	jne	.L54
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	jne	.L49
.L54:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	addl	$2, %r8d
	cmpl	$4, %r8d
	ja	.L66
	movq	(%rdi), %rdx
	leal	(%r8,%r8), %ecx
	movl	8(%rdx), %eax
	shrl	%cl, %eax
	movl	%eax, %ecx
	movl	$1, %eax
	andl	$3, %ecx
	cmpl	$1, %ecx
	jne	.L46
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	movl	%eax, %ecx
	shrl	$2, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	jne	.L46
	movl	%eax, %ecx
	shrl	$4, %ecx
	andl	$3, %ecx
	cmpl	$1, %ecx
	jne	.L46
	shrl	$6, %eax
	andl	$3, %eax
	movl	%eax, %ecx
	movl	$1, %eax
	cmpl	$1, %ecx
	jne	.L46
	jmp	.L44
.L66:
	leaq	.LC1(%rip), %rdi
	call	__GI___libc_fatal
	.size	__GI___nss_next2, .-__GI___nss_next2
	.globl	__nss_next2
	.set	__nss_next2,__GI___nss_next2
	.p2align 4,,15
	.globl	__GI___nss_lookup_function
	.hidden	__GI___nss_lookup_function
	.type	__GI___nss_lookup_function, @function
__GI___nss_lookup_function:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L68
	jmp	__nss_module_get_function
	.p2align 4,,10
	.p2align 3
.L68:
	xorl	%eax, %eax
	ret
	.size	__GI___nss_lookup_function, .-__GI___nss_lookup_function
	.globl	__nss_lookup_function
	.set	__nss_lookup_function,__GI___nss_lookup_function
	.section	.rodata.str1.1
.LC2:
	.string	"ethers"
.LC3:
	.string	"group"
.LC4:
	.string	"gshadow"
.LC5:
	.string	"hosts"
.LC6:
	.string	"initgroups"
.LC7:
	.string	"netgroup"
.LC8:
	.string	"networks"
.LC9:
	.string	"passwd"
.LC10:
	.string	"protocols"
.LC11:
	.string	"publickey"
.LC12:
	.string	"rpc"
.LC13:
	.string	"services"
.LC14:
	.string	"shadow"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	database_names, @object
	.size	database_names, 120
database_names:
	.quad	.LC0
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	.LC12
	.quad	.LC13
	.quad	.LC14
	.quad	0
	.hidden	__nss_shadow_database
	.weak	__nss_shadow_database
	.bss
	.align 8
	.type	__nss_shadow_database, @object
	.size	__nss_shadow_database, 8
__nss_shadow_database:
	.zero	8
	.hidden	__nss_services_database
	.weak	__nss_services_database
	.align 8
	.type	__nss_services_database, @object
	.size	__nss_services_database, 8
__nss_services_database:
	.zero	8
	.hidden	__nss_rpc_database
	.weak	__nss_rpc_database
	.align 8
	.type	__nss_rpc_database, @object
	.size	__nss_rpc_database, 8
__nss_rpc_database:
	.zero	8
	.hidden	__nss_publickey_database
	.weak	__nss_publickey_database
	.align 8
	.type	__nss_publickey_database, @object
	.size	__nss_publickey_database, 8
__nss_publickey_database:
	.zero	8
	.hidden	__nss_protocols_database
	.weak	__nss_protocols_database
	.align 8
	.type	__nss_protocols_database, @object
	.size	__nss_protocols_database, 8
__nss_protocols_database:
	.zero	8
	.hidden	__nss_passwd_database
	.weak	__nss_passwd_database
	.align 8
	.type	__nss_passwd_database, @object
	.size	__nss_passwd_database, 8
__nss_passwd_database:
	.zero	8
	.hidden	__nss_networks_database
	.weak	__nss_networks_database
	.align 8
	.type	__nss_networks_database, @object
	.size	__nss_networks_database, 8
__nss_networks_database:
	.zero	8
	.hidden	__nss_netgroup_database
	.weak	__nss_netgroup_database
	.align 8
	.type	__nss_netgroup_database, @object
	.size	__nss_netgroup_database, 8
__nss_netgroup_database:
	.zero	8
	.hidden	__nss_initgroups_database
	.weak	__nss_initgroups_database
	.align 8
	.type	__nss_initgroups_database, @object
	.size	__nss_initgroups_database, 8
__nss_initgroups_database:
	.zero	8
	.hidden	__nss_hosts_database
	.weak	__nss_hosts_database
	.align 8
	.type	__nss_hosts_database, @object
	.size	__nss_hosts_database, 8
__nss_hosts_database:
	.zero	8
	.hidden	__nss_gshadow_database
	.weak	__nss_gshadow_database
	.align 8
	.type	__nss_gshadow_database, @object
	.size	__nss_gshadow_database, 8
__nss_gshadow_database:
	.zero	8
	.hidden	__nss_group_database
	.weak	__nss_group_database
	.align 8
	.type	__nss_group_database, @object
	.size	__nss_group_database, 8
__nss_group_database:
	.zero	8
	.hidden	__nss_ethers_database
	.weak	__nss_ethers_database
	.align 8
	.type	__nss_ethers_database, @object
	.size	__nss_ethers_database, 8
__nss_ethers_database:
	.zero	8
	.hidden	__nss_aliases_database
	.weak	__nss_aliases_database
	.align 8
	.type	__nss_aliases_database, @object
	.size	__nss_aliases_database, 8
__nss_aliases_database:
	.zero	8
	.hidden	__nss_module_get_function
	.hidden	__nss_database_get
