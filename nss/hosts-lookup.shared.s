	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"dns [!UNAVAIL=return] files"
.LC1:
	.string	"hosts"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___nss_hosts_lookup2
	.hidden	__GI___nss_hosts_lookup2
	.type	__GI___nss_hosts_lookup2, @function
__GI___nss_hosts_lookup2:
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rdi, %rbx
	leaq	__nss_hosts_database(%rip), %rcx
	leaq	.LC0(%rip), %rdx
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	movq	%rsi, %rbp
	xorl	%esi, %esi
	call	__GI___nss_database_lookup2
	testl	%eax, %eax
	js	.L2
	movq	__nss_hosts_database(%rip), %rax
	movq	%r13, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI___nss_lookup
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___nss_hosts_lookup2, .-__GI___nss_hosts_lookup2
	.globl	__nss_hosts_lookup2
	.set	__nss_hosts_lookup2,__GI___nss_hosts_lookup2
	.hidden	__nss_hosts_database
