	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __nss_passwd_lookup,__nss_passwd_lookup@GLIBC_2.2.5
	.symver __nss_group_lookup,__nss_group_lookup@GLIBC_2.2.5
	.symver __nss_hosts_lookup,__nss_hosts_lookup@GLIBC_2.2.5
	.symver __nss_next,__nss_next@GLIBC_2.2.5
	.symver __nss_database_lookup,__nss_database_lookup@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__nss_passwd_lookup
	.type	__nss_passwd_lookup, @function
__nss_passwd_lookup:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__nss_passwd_lookup, .-__nss_passwd_lookup
	.globl	__nss_hosts_lookup
	.set	__nss_hosts_lookup,__nss_passwd_lookup
	.globl	__nss_group_lookup
	.set	__nss_group_lookup,__nss_passwd_lookup
	.p2align 4,,15
	.globl	__nss_next
	.type	__nss_next, @function
__nss_next:
	movl	$-1, %eax
	ret
	.size	__nss_next, .-__nss_next
	.p2align 4,,15
	.globl	__nss_database_lookup
	.type	__nss_database_lookup, @function
__nss_database_lookup:
	movq	$0, (%rcx)
	movl	$-1, %eax
	ret
	.size	__nss_database_lookup, .-__nss_database_lookup
