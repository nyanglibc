	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.weak	__ctype_get_mb_cur_max
	.type	__ctype_get_mb_cur_max, @function
__ctype_get_mb_cur_max:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	ret
	.size	__ctype_get_mb_cur_max, .-__ctype_get_mb_cur_max
