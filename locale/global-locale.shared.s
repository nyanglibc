	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_global_locale
	.globl	_nl_global_locale
	.section	.data.rel,"aw",@progbits
	.align 32
	.type	_nl_global_locale, @object
	.size	_nl_global_locale, 232
_nl_global_locale:
	.quad	_nl_C_LC_CTYPE
	.quad	_nl_C_LC_NUMERIC
	.quad	_nl_C_LC_TIME
	.quad	_nl_C_LC_COLLATE
	.quad	_nl_C_LC_MONETARY
	.quad	_nl_C_LC_MESSAGES
	.zero	8
	.quad	_nl_C_LC_PAPER
	.quad	_nl_C_LC_NAME
	.quad	_nl_C_LC_ADDRESS
	.quad	_nl_C_LC_TELEPHONE
	.quad	_nl_C_LC_MEASUREMENT
	.quad	_nl_C_LC_IDENTIFICATION
	.quad	_nl_C_LC_CTYPE_class+256
	.quad	_nl_C_LC_CTYPE_tolower+512
	.quad	_nl_C_LC_CTYPE_toupper+512
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.globl	__libc_tsd_LOCALE
	.section	.tdata,"awT",@progbits
	.align 8
	.type	__libc_tsd_LOCALE, @object
	.size	__libc_tsd_LOCALE, 8
__libc_tsd_LOCALE:
	.quad	_nl_global_locale
	.weak	_nl_C_LC_CTYPE_toupper
	.weak	_nl_C_LC_CTYPE_tolower
	.weak	_nl_C_LC_CTYPE_class
	.weak	_nl_C_LC_IDENTIFICATION
	.weak	_nl_C_LC_MEASUREMENT
	.weak	_nl_C_LC_TELEPHONE
	.weak	_nl_C_LC_ADDRESS
	.weak	_nl_C_LC_NAME
	.weak	_nl_C_LC_PAPER
	.weak	_nl_C_LC_MESSAGES
	.weak	_nl_C_LC_MONETARY
	.weak	_nl_C_LC_COLLATE
	.weak	_nl_C_LC_TIME
	.weak	_nl_C_LC_NUMERIC
	.weak	_nl_C_LC_CTYPE
	.hidden	_nl_C_name
	.hidden	_nl_C_LC_CTYPE_toupper
	.hidden	_nl_C_LC_CTYPE_tolower
	.hidden	_nl_C_LC_CTYPE_class
