	.text
	.hidden	_nl_C_LC_MEASUREMENT
	.globl	_nl_C_LC_MEASUREMENT
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"\001"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_MEASUREMENT, @object
	.size	_nl_C_LC_MEASUREMENT, 80
_nl_C_LC_MEASUREMENT:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	2
	.zero	4
	.quad	.LC0
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
