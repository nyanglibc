	.text
	.p2align 4,,15
	.globl	__freelocale
	.type	__freelocale, @function
__freelocale:
	leaq	_nl_C_locobj(%rip), %rax
	cmpq	%rax, %rdi
	je	.L1
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	je	.L3
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L3:
	xorl	%ebx, %ebx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rbx
.L7:
	cmpq	$6, %rbx
	je	.L4
	movq	0(%rbp,%rbx,8), %rsi
	cmpl	$-1, 48(%rsi)
	je	.L5
	movl	%ebx, %edi
	call	_nl_remove_locale
.L5:
	cmpl	$12, %ebx
	jne	.L4
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L6
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L6:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	__freelocale, .-__freelocale
	.weak	freelocale
	.set	freelocale,__freelocale
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	_nl_remove_locale
	.hidden	__libc_setlocale_lock
	.hidden	_nl_C_locobj
