	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	__GI__libc_intl_domainname
	.globl	__GI__libc_intl_domainname
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	__GI__libc_intl_domainname, @object
	.size	__GI__libc_intl_domainname, 5
__GI__libc_intl_domainname:
	.string	"libc"
	.globl	_libc_intl_domainname
	.set	_libc_intl_domainname,__GI__libc_intl_domainname
