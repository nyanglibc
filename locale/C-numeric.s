	.text
	.hidden	_nl_C_LC_NUMERIC
	.globl	_nl_C_LC_NUMERIC
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"."
.LC1:
	.string	""
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_NUMERIC, @object
	.size	_nl_C_LC_NUMERIC, 112
_nl_C_LC_NUMERIC:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	6
	.zero	4
	.quad	.LC0
	.quad	.LC1
	.quad	.LC1
	.long	46
	.zero	4
	.long	0
	.zero	4
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
