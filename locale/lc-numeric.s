	.text
#APP
	.globl _nl_current_LC_NUMERIC_used
.set _nl_current_LC_NUMERIC_used, 2
#NO_APP
	.hidden	_nl_current_LC_NUMERIC
	.globl	_nl_current_LC_NUMERIC
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_NUMERIC, @object
	.size	_nl_current_LC_NUMERIC, 8
_nl_current_LC_NUMERIC:
	.quad	_nl_global_locale+8
	.hidden	_nl_global_locale
