	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__duplocale
	.type	__duplocale, @function
__duplocale:
	leaq	_nl_C_locobj(%rip), %rax
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	cmpq	%rax, %rdi
	pushq	%rbx
	je	.L1
	leaq	_nl_global_locale(%rip), %rax
	cmpq	$-1, %rdi
	movq	%rdi, %rbp
	leaq	_nl_C_name(%rip), %r12
	cmove	%rax, %rbp
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$1, %rbx
.L4:
	cmpq	$6, %rbx
	je	.L5
	movq	128(%rbp,%rbx,8), %rdi
	cmpq	%r12, %rdi
	je	.L6
	call	__GI_strlen
	leaq	1(%r13,%rax), %r13
.L6:
	cmpl	$12, %ebx
	jne	.L5
	leaq	232(%r13), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1
	movl	__libc_pthread_functions_init(%rip), %edx
	leaq	232(%rax), %r14
	testl	%edx, %edx
	je	.L7
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 60 "duplocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L7:
	xorl	%ebx, %ebx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r14, 128(%r13,%rbx,8)
	movq	%r14, %rdi
	call	__GI_stpcpy@PLT
	cmpl	$12, %ebx
	leaq	1(%rax), %r14
	je	.L26
.L9:
	addq	$1, %rbx
.L8:
	cmpq	$6, %rbx
	je	.L9
	movq	0(%rbp,%rbx,8), %rdx
	movl	48(%rdx), %ecx
	movq	%rdx, 0(%r13,%rbx,8)
	cmpl	$-3, %ecx
	ja	.L10
	addl	$1, %ecx
	movl	%ecx, 48(%rdx)
.L10:
	movq	128(%rbp,%rbx,8), %rsi
	cmpq	%r12, %rsi
	jne	.L11
	cmpl	$12, %ebx
	movq	%r12, 128(%r13,%rbx,8)
	jne	.L9
.L26:
	movq	104(%rbp), %rax
	movq	%rax, 104(%r13)
	movq	112(%rbp), %rax
	movq	%rax, 112(%r13)
	movq	120(%rbp), %rax
	movq	%rax, 120(%r13)
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 84 "duplocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L1:
	popq	%rbx
	movq	%r13, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__duplocale, .-__duplocale
	.weak	duplocale
	.set	duplocale,__duplocale
	.hidden	__libc_setlocale_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	_nl_C_name
	.hidden	_nl_global_locale
	.hidden	_nl_C_locobj
