	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_LC_IDENTIFICATION
	.globl	_nl_C_LC_IDENTIFICATION
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"ISO/IEC 14652 i18n FDCC-set"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"ISO/IEC JTC1/SC22/WG20 - internationalization"
	.align 8
.LC2:
	.string	"C/o Keld Simonsen, Skt. Jorgens Alle 8, DK-1615 Kobenhavn V"
	.section	.rodata.str1.1
.LC3:
	.string	"Keld Simonsen"
.LC4:
	.string	"keld@dkuug.dk"
.LC5:
	.string	"+45 3122-6543"
.LC6:
	.string	"+45 3325-6543"
.LC7:
	.string	""
.LC8:
	.string	"ISO"
.LC9:
	.string	"1.0"
.LC10:
	.string	"1997-12-20"
	.section	.rodata
	.align 8
.LC11:
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	""
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.string	"i18n:1999"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_IDENTIFICATION, @object
	.size	_nl_C_LC_IDENTIFICATION, 192
_nl_C_LC_IDENTIFICATION:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	16
	.zero	4
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC3
	.quad	.LC4
	.quad	.LC5
	.quad	.LC6
	.quad	.LC7
	.quad	.LC8
	.quad	.LC7
	.quad	.LC7
	.quad	.LC7
	.quad	.LC9
	.quad	.LC10
	.quad	.LC11
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
