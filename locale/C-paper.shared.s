	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_LC_PAPER
	.globl	_nl_C_LC_PAPER
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_PAPER, @object
	.size	_nl_C_LC_PAPER, 88
_nl_C_LC_PAPER:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	3
	.zero	4
	.long	297
	.zero	4
	.long	210
	.zero	4
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
