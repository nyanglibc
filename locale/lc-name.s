	.text
#APP
	.globl _nl_current_LC_NAME_used
.set _nl_current_LC_NAME_used, 2
#NO_APP
	.hidden	_nl_current_LC_NAME
	.globl	_nl_current_LC_NAME
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_NAME, @object
	.size	_nl_current_LC_NAME, 8
_nl_current_LC_NAME:
	.quad	_nl_global_locale+64
	.hidden	_nl_global_locale
