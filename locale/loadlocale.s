	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"loadlocale.c"
.LC1:
	.string	"category == LC_CTYPE"
	.text
	.p2align 4,,15
	.globl	_nl_intern_locale_data
	.hidden	_nl_intern_locale_data
	.type	_nl_intern_locale_data, @function
_nl_intern_locale_data:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$7, %rdx
	jbe	.L37
	cmpl	$3, %edi
	movl	(%rsi), %ecx
	movl	$537202711, %eax
	je	.L3
	movl	%edi, %eax
	movl	$537462560, %r8d
	xorl	$537071893, %eax
	testl	%edi, %edi
	cmove	%r8d, %eax
.L3:
	cmpl	%eax, %ecx
	jne	.L37
	leaq	_nl_category_num_items(%rip), %rax
	movl	4(%rsi), %r13d
	movslq	%edi, %r14
	cmpq	(%rax,%r14,8), %r13
	jb	.L37
	leaq	8(,%r13,4), %rax
	cmpq	%rdx, %rax
	jnb	.L37
	movl	%edi, %ebx
	leaq	64(,%r13,8), %rdi
	movq	%rdx, %r12
	movq	%rsi, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L39
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpq	$4, %rdx
	jbe	.L20
.L6:
	movq	%rax, %rdi
	call	free@PLT
.L37:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	testq	%r13, %r13
	movq	%rbp, 8(%rax)
	movq	%r12, 16(%rax)
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movl	%r13d, 56(%rax)
	je	.L1
	movl	8(%rbp), %ecx
	cmpq	%r12, %rcx
	movq	%rcx, %rdi
	ja	.L6
	leaq	_nl_value_types(%rip), %r10
	leaq	.L10(%rip), %r8
	xorl	%edx, %edx
	movl	%ebx, %r9d
.L7:
	cmpl	$12, %ebx
	ja	.L8
	movslq	(%r8,%r9,4), %rsi
	addq	%r8, %rsi
	jmp	*%rsi
	.section	.rodata
	.align 4
	.align 4
.L10:
	.long	.L8-.L10
	.long	.L9-.L10
	.long	.L11-.L10
	.long	.L12-.L10
	.long	.L13-.L10
	.long	.L14-.L10
	.long	.L8-.L10
	.long	.L15-.L10
	.long	.L16-.L10
	.long	.L17-.L10
	.long	.L14-.L10
	.long	.L18-.L10
	.long	.L19-.L10
	.text
	.p2align 4,,10
	.p2align 3
.L11:
	cmpq	$158, %rdx
	ja	.L6
	.p2align 4,,10
	.p2align 3
.L20:
	movq	(%r10,%r14,8), %rsi
	cmpl	$5, (%rsi,%rdx,4)
	je	.L23
.L22:
	addq	%rbp, %rcx
	movq	%rcx, 64(%rax,%rdx,8)
.L24:
	addq	$1, %rdx
	cmpq	%rdx, %r13
	je	.L1
	movl	8(%rbp,%rdx,4), %ecx
	cmpq	%rcx, %r12
	movq	%rcx, %rdi
	jnb	.L7
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L18:
	cmpq	$1, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L17:
	cmpq	$12, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L16:
	cmpq	$6, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L15:
	cmpq	$2, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	cmpq	$15, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	cmpq	$45, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L12:
	cmpq	$18, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	$5, %rdx
	jbe	.L20
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%ebx, %ebx
	jne	.L40
	cmpq	$85, %rdx
	ja	.L22
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L23:
	andl	$3, %edi
	jne	.L6
	movl	0(%rbp,%rcx), %ecx
	movl	%ecx, 64(%rax,%rdx,8)
	jmp	.L24
.L40:
	leaq	__PRETTY_FUNCTION__.9364(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$144, %edx
	call	__assert_fail
	.size	_nl_intern_locale_data, .-_nl_intern_locale_data
	.p2align 4,,15
	.globl	_nl_load_locale
	.hidden	_nl_load_locale
	.type	_nl_load_locale, @function
_nl_load_locale:
	pushq	%rbp
	xorl	%eax, %eax
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r13d
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$524288, %esi
	subq	$168, %rsp
	movl	$1, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	(%rdi), %rdi
	call	__open_nocancel
	testl	%eax, %eax
	js	.L41
	leaq	-192(%rbp), %r12
	movl	%eax, %edi
	movl	%eax, %r14d
	movq	%r12, %rsi
	call	__fstat64
	testl	%eax, %eax
	js	.L53
.L44:
	movl	-168(%rbp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	je	.L77
.L46:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	-144(%rbp), %rsi
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%r14d, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movl	%fs:(%rax), %r15d
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %r12
	je	.L78
	movl	%r14d, %edi
	call	__close_nocancel
	testq	%r12, %r12
	je	.L41
	movq	-144(%rbp), %rdx
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	_nl_intern_locale_data
	testq	%rax, %rax
	je	.L60
	movl	$1, %edx
.L57:
	movq	$0, (%rax)
	movl	%edx, 24(%rax)
	movq	%rax, 16(%rbx)
.L41:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L78:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$38, %fs:(%rax)
	jne	.L53
	movq	-144(%rbp), %r12
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -208(%rbp)
	je	.L53
	testq	%r12, %r12
	jle	.L54
	movq	%rax, %rcx
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L55:
	subq	%rax, %r12
	addq	%rax, %rcx
	testq	%r12, %r12
	jle	.L54
.L56:
	movq	%rcx, %rsi
	movq	%r12, %rdx
	movl	%r14d, %edi
	movq	%rcx, -200(%rbp)
	call	__read_nocancel
	testq	%rax, %rax
	movq	-200(%rbp), %rcx
	jg	.L55
	movq	-208(%rbp), %rdi
	movq	%rax, -200(%rbp)
	call	free@PLT
	movq	-200(%rbp), %rax
	testq	%rax, %rax
	jne	.L53
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L53:
	movl	%r14d, %edi
	call	__close_nocancel
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%r14d, %edi
	call	__close_nocancel
	movq	(%rbx), %rsi
	movq	%rsi, %rdi
	movq	%rsi, -208(%rbp)
	call	strlen
	leaq	_nl_category_name_sizes(%rip), %rdx
	movslq	%r13d, %rcx
	movq	-208(%rbp), %rsi
	movzbl	(%rdx,%rcx), %edx
	leaq	36(%rax,%rdx), %rdi
	movq	%rdx, %r14
	leaq	_nl_category_name_idxs(%rip), %rdx
	andq	$-16, %rdi
	movzbl	(%rdx,%rcx), %r8d
	leaq	_nl_category_names(%rip), %rdx
	subq	%rdi, %rsp
	leaq	15(%rsp), %r15
	andq	$-16, %r15
	addq	%rdx, %r8
	movq	%rax, %rdx
	movq	%r15, %rdi
	movq	%r8, -200(%rbp)
	call	__mempcpy@PLT
	leal	1(%r14), %edx
	movl	$1398362927, (%rax)
	movb	$95, 4(%rax)
	leaq	5(%rax), %rcx
	movq	-200(%rbp), %r8
	cmpl	$8, %edx
	jnb	.L47
	testb	$4, %dl
	jne	.L79
	testl	%edx, %edx
	je	.L48
	movzbl	(%r8), %esi
	testb	$2, %dl
	movb	%sil, 5(%rax)
	jne	.L80
.L48:
	xorl	%eax, %eax
	movl	$524288, %esi
	movq	%r15, %rdi
	call	__open_nocancel
	testl	%eax, %eax
	movl	%eax, %r14d
	js	.L41
	movq	%r12, %rsi
	movl	%eax, %edi
	call	__fstat64
	testl	%eax, %eax
	jns	.L46
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%r8), %rsi
	movq	%rsi, 5(%rax)
	movl	%edx, %esi
	movq	-8(%r8,%rsi), %rdi
	movq	%rdi, -8(%rcx,%rsi)
	leaq	13(%rax), %rdi
	movq	%r8, %rsi
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%edx, %ecx
	shrl	$3, %ecx
	movl	%ecx, %ecx
	rep movsq
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L54:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r14d, %edi
	movl	%r15d, %fs:(%rax)
	call	__close_nocancel
	movq	-144(%rbp), %rdx
	movq	-208(%rbp), %rsi
	movl	%r13d, %edi
	call	_nl_intern_locale_data
	testq	%rax, %rax
	je	.L41
	xorl	%edx, %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L60:
	movq	-144(%rbp), %rsi
	movq	%r12, %rdi
	call	__munmap
	jmp	.L41
.L79:
	movl	(%r8), %esi
	movl	%esi, 5(%rax)
	movl	-4(%r8,%rdx), %eax
	movl	%eax, -4(%rcx,%rdx)
	jmp	.L48
.L80:
	movzwl	-2(%r8,%rdx), %eax
	movw	%ax, -2(%rcx,%rdx)
	jmp	.L48
	.size	_nl_load_locale, .-_nl_load_locale
	.p2align 4,,15
	.globl	_nl_unload_locale
	.hidden	_nl_unload_locale
	.type	_nl_unload_locale, @function
_nl_unload_locale:
	pushq	%rbx
	movq	32(%rdi), %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L82
	call	*%rax
.L82:
	movl	24(%rbx), %eax
	testl	%eax, %eax
	je	.L84
	cmpl	$1, %eax
	je	.L85
.L83:
	cmpl	$2, %eax
	je	.L86
	movq	(%rbx), %rdi
	call	free@PLT
.L86:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L85:
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	call	__munmap
	movl	24(%rbx), %eax
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L84:
	movq	8(%rbx), %rdi
	call	free@PLT
	movl	24(%rbx), %eax
	jmp	.L83
	.size	_nl_unload_locale, .-_nl_unload_locale
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.9364, @object
	.size	__PRETTY_FUNCTION__.9364, 23
__PRETTY_FUNCTION__.9364:
	.string	"_nl_intern_locale_data"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_value_types, @object
	.size	_nl_value_types, 104
_nl_value_types:
	.quad	_nl_value_type_LC_CTYPE
	.quad	_nl_value_type_LC_NUMERIC
	.quad	_nl_value_type_LC_TIME
	.quad	_nl_value_type_LC_COLLATE
	.quad	_nl_value_type_LC_MONETARY
	.quad	_nl_value_type_LC_MESSAGES
	.zero	8
	.quad	_nl_value_type_LC_PAPER
	.quad	_nl_value_type_LC_NAME
	.quad	_nl_value_type_LC_ADDRESS
	.quad	_nl_value_type_LC_TELEPHONE
	.quad	_nl_value_type_LC_MEASUREMENT
	.quad	_nl_value_type_LC_IDENTIFICATION
	.section	.rodata
	.align 32
	.type	_nl_value_type_LC_IDENTIFICATION, @object
	.size	_nl_value_type_LC_IDENTIFICATION, 64
_nl_value_type_LC_IDENTIFICATION:
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	2
	.long	1
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	_nl_value_type_LC_MEASUREMENT, @object
	.size	_nl_value_type_LC_MEASUREMENT, 8
_nl_value_type_LC_MEASUREMENT:
	.long	3
	.long	1
	.section	.rodata
	.align 32
	.type	_nl_value_type_LC_ADDRESS, @object
	.size	_nl_value_type_LC_ADDRESS, 52
_nl_value_type_LC_ADDRESS:
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	5
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.align 16
	.type	_nl_value_type_LC_NAME, @object
	.size	_nl_value_type_LC_NAME, 28
_nl_value_type_LC_NAME:
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.align 8
	.type	_nl_value_type_LC_PAPER, @object
	.size	_nl_value_type_LC_PAPER, 12
_nl_value_type_LC_PAPER:
	.long	5
	.long	5
	.long	1
	.align 16
	.type	_nl_value_type_LC_MESSAGES, @object
	.size	_nl_value_type_LC_MESSAGES, 20
_nl_value_type_LC_MESSAGES:
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.set	_nl_value_type_LC_TELEPHONE,_nl_value_type_LC_MESSAGES
	.align 32
	.type	_nl_value_type_LC_TIME, @object
	.size	_nl_value_type_LC_TIME, 636
_nl_value_type_LC_TIME:
	.long	2
	.zero	24
	.long	2
	.zero	24
	.long	2
	.zero	44
	.long	2
	.zero	44
	.long	2
	.zero	4
	.long	1
	.long	1
	.long	1
	.long	1
	.long	6
	.long	1
	.long	1
	.long	6
	.long	1
	.long	1
	.long	5
	.long	1
	.long	9
	.zero	24
	.long	9
	.zero	24
	.long	9
	.zero	44
	.long	9
	.zero	44
	.long	9
	.zero	4
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	10
	.long	8
	.long	8
	.long	3
	.long	5
	.long	3
	.long	3
	.long	3
	.long	3
	.long	1
	.long	1
	.long	8
	.long	1
	.long	2
	.zero	44
	.long	9
	.zero	44
	.long	2
	.zero	44
	.long	9
	.zero	44
	.align 16
	.type	_nl_value_type_LC_NUMERIC, @object
	.size	_nl_value_type_LC_NUMERIC, 24
_nl_value_type_LC_NUMERIC:
	.long	1
	.long	1
	.long	4
	.long	5
	.long	5
	.long	1
	.align 32
	.type	_nl_value_type_LC_MONETARY, @object
	.size	_nl_value_type_LC_MONETARY, 184
_nl_value_type_LC_MONETARY:
	.long	1
	.long	1
	.long	1
	.long	1
	.long	4
	.long	1
	.long	1
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	1
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	1
	.long	1
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	3
	.long	5
	.long	5
	.long	5
	.long	5
	.long	7
	.long	5
	.long	5
	.long	1
	.align 32
	.type	_nl_value_type_LC_CTYPE, @object
	.size	_nl_value_type_LC_CTYPE, 344
_nl_value_type_LC_CTYPE:
	.long	8
	.long	8
	.zero	4
	.long	8
	.zero	4
	.long	8
	.zero	16
	.long	6
	.long	6
	.long	4
	.long	5
	.long	1
	.long	8
	.long	8
	.long	5
	.long	5
	.long	5
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	5
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	8
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	1
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	5
	.long	8
	.long	8
	.long	8
	.long	8
	.long	5
	.long	8
	.long	5
	.long	1
	.long	5
	.long	5
	.zero	56
	.align 32
	.type	_nl_value_type_LC_COLLATE, @object
	.size	_nl_value_type_LC_COLLATE, 76
_nl_value_type_LC_COLLATE:
	.long	5
	.long	1
	.long	8
	.long	8
	.long	8
	.long	8
	.zero	12
	.long	8
	.long	8
	.long	8
	.long	8
	.long	5
	.long	8
	.long	8
	.long	8
	.long	8
	.long	1
	.align 32
	.type	_nl_category_num_items, @object
	.size	_nl_category_num_items, 104
_nl_category_num_items:
	.quad	86
	.quad	6
	.quad	159
	.quad	19
	.quad	46
	.quad	5
	.zero	8
	.quad	3
	.quad	7
	.quad	13
	.quad	5
	.quad	2
	.quad	16
	.hidden	__munmap
	.hidden	_nl_category_names
	.hidden	_nl_category_name_idxs
	.hidden	_nl_category_name_sizes
	.hidden	strlen
	.hidden	__read_nocancel
	.hidden	__close_nocancel
	.hidden	__mmap
	.hidden	__fstat64
	.hidden	__open_nocancel
	.hidden	__assert_fail
