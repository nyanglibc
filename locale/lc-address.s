	.text
#APP
	.globl _nl_current_LC_ADDRESS_used
.set _nl_current_LC_ADDRESS_used, 2
#NO_APP
	.hidden	_nl_current_LC_ADDRESS
	.globl	_nl_current_LC_ADDRESS
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_ADDRESS, @object
	.size	_nl_current_LC_ADDRESS, 8
_nl_current_LC_ADDRESS:
	.quad	_nl_global_locale+72
	.hidden	_nl_global_locale
