	.text
	.p2align 4,,15
	.globl	__duplocale
	.type	__duplocale, @function
__duplocale:
	leaq	_nl_C_locobj(%rip), %rax
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	cmpq	%rax, %rdi
	pushq	%rbx
	je	.L1
	leaq	_nl_global_locale(%rip), %rax
	cmpq	$-1, %rdi
	movq	%rdi, %rbp
	leaq	_nl_C_name(%rip), %r12
	cmove	%rax, %rbp
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$1, %rbx
.L4:
	cmpq	$6, %rbx
	je	.L5
	movq	128(%rbp,%rbx,8), %rdi
	cmpq	%r12, %rdi
	je	.L6
	call	strlen
	leaq	1(%r13,%rax), %r13
.L6:
	cmpl	$12, %ebx
	jne	.L5
	leaq	232(%r13), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	leaq	232(%rax), %r14
	je	.L7
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L7:
	xorl	%ebx, %ebx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%r14, 128(%r13,%rbx,8)
	movq	%r14, %rdi
	call	__stpcpy@PLT
	cmpl	$12, %ebx
	leaq	1(%rax), %r14
	je	.L32
.L9:
	addq	$1, %rbx
.L8:
	cmpq	$6, %rbx
	je	.L9
	movq	0(%rbp,%rbx,8), %rdx
	movl	48(%rdx), %ecx
	movq	%rdx, 0(%r13,%rbx,8)
	cmpl	$-3, %ecx
	ja	.L10
	addl	$1, %ecx
	movl	%ecx, 48(%rdx)
.L10:
	movq	128(%rbp,%rbx,8), %rsi
	cmpq	%r12, %rsi
	jne	.L11
	cmpl	$12, %ebx
	movq	%r12, 128(%r13,%rbx,8)
	jne	.L9
.L32:
	movq	104(%rbp), %rax
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	movq	%rax, 104(%r13)
	movq	112(%rbp), %rax
	movq	%rax, 112(%r13)
	movq	120(%rbp), %rax
	movq	%rax, 120(%r13)
	je	.L1
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L1:
	popq	%rbx
	movq	%r13, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__duplocale, .-__duplocale
	.weak	duplocale
	.set	duplocale,__duplocale
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	__libc_setlocale_lock
	.hidden	strlen
	.hidden	_nl_C_name
	.hidden	_nl_global_locale
	.hidden	_nl_C_locobj
