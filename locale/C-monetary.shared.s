	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_LC_MONETARY
	.globl	_nl_C_LC_MONETARY
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"-"
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_MONETARY, @object
	.size	_nl_C_LC_MONETARY, 432
_nl_C_LC_MONETARY:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	46
	.zero	4
	.quad	.LC0
	.quad	.LC0
	.quad	.LC0
	.quad	.LC0
	.quad	.LC0
	.quad	.LC0
	.quad	.LC0
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	.LC1
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	.LC0
	.quad	.LC0
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.quad	not_available
	.long	10101
	.zero	4
	.long	99991231
	.zero	4
	.long	10101
	.zero	4
	.long	99991231
	.zero	4
	.quad	conversion_rate
	.long	0
	.zero	4
	.long	0
	.zero	4
	.quad	_nl_C_codeset
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	conversion_rate, @object
	.size	conversion_rate, 8
conversion_rate:
	.long	1
	.long	1
	.section	.rodata.str1.1
	.type	not_available, @object
	.size	not_available, 2
not_available:
	.string	"\377"
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
