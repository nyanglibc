	.text
	.p2align 4,,15
	.globl	__collidx_table_lookup
	.type	__collidx_table_lookup, @function
__collidx_table_lookup:
	movl	(%rdi), %ecx
	movl	%esi, %edx
	xorl	%eax, %eax
	shrl	%cl, %edx
	cmpl	4(%rdi), %edx
	jnb	.L1
	addl	$5, %edx
	movl	(%rdi,%rdx,4), %edx
	testl	%edx, %edx
	je	.L1
	movl	8(%rdi), %ecx
	movl	%esi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdi), %ecx
	leaq	(%rdi,%rcx,4), %rcx
	movl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L1
	andl	16(%rdi), %esi
	leaq	(%rdi,%rsi,4), %rax
	movl	(%rax,%rdx), %eax
.L1:
	rep ret
	.size	__collidx_table_lookup, .-__collidx_table_lookup
	.p2align 4,,15
	.globl	__collseq_table_lookup
	.type	__collseq_table_lookup, @function
__collseq_table_lookup:
	movl	(%rdi), %ecx
	movl	%esi, %edx
	movl	$-1, %eax
	shrl	%cl, %edx
	cmpl	4(%rdi), %edx
	jnb	.L9
	addl	$5, %edx
	movl	(%rdi,%rdx,4), %edx
	testl	%edx, %edx
	je	.L9
	movl	8(%rdi), %ecx
	movl	%esi, %r8d
	shrl	%cl, %r8d
	movl	%r8d, %ecx
	andl	12(%rdi), %ecx
	leaq	(%rdi,%rcx,4), %rcx
	movl	(%rcx,%rdx), %edx
	testl	%edx, %edx
	je	.L9
	andl	16(%rdi), %esi
	leaq	(%rdi,%rsi,4), %rax
	movl	(%rax,%rdx), %eax
.L9:
	rep ret
	.size	__collseq_table_lookup, .-__collseq_table_lookup
