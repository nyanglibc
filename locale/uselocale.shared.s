	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___uselocale
	.hidden	__GI___uselocale
	.type	__GI___uselocale, @function
__GI___uselocale:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	testq	%rdi, %rdi
	leaq	_nl_global_locale(%rip), %rdx
	movq	%fs:(%rcx), %rax
	je	.L2
	cmpq	$-1, %rdi
	cmove	%rdx, %rdi
	movq	%rdi, %fs:(%rcx)
	movq	104(%rdi), %rsi
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rcx
	movq	%rsi, %fs:(%rcx)
	movq	112(%rdi), %rsi
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rcx
	movq	%rsi, %fs:(%rcx)
	movq	120(%rdi), %rsi
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	movq	%rsi, %fs:(%rcx)
.L2:
	cmpq	%rdx, %rax
	movq	$-1, %rdx
	cmove	%rdx, %rax
	ret
	.size	__GI___uselocale, .-__GI___uselocale
	.globl	__uselocale
	.set	__uselocale,__GI___uselocale
	.weak	uselocale
	.set	uselocale,__uselocale
	.hidden	_nl_global_locale
