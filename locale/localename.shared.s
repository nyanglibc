	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__current_locale_name
	.hidden	__current_locale_name
	.type	__current_locale_name, @function
__current_locale_name:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movslq	%edi, %rdi
	movq	%fs:(%rax), %rax
	movq	128(%rax,%rdi,8), %rax
	ret
	.size	__current_locale_name, .-__current_locale_name
