	.text
#APP
	.globl _nl_current_LC_IDENTIFICATION_used
.set _nl_current_LC_IDENTIFICATION_used, 2
#NO_APP
	.hidden	_nl_current_LC_IDENTIFICATION
	.globl	_nl_current_LC_IDENTIFICATION
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_IDENTIFICATION, @object
	.size	_nl_current_LC_IDENTIFICATION, 8
_nl_current_LC_IDENTIFICATION:
	.quad	_nl_global_locale+96
	.hidden	_nl_global_locale
