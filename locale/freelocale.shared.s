	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__freelocale
	.type	__freelocale, @function
__freelocale:
	leaq	_nl_C_locobj(%rip), %rax
	cmpq	%rax, %rdi
	je	.L1
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L3
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 41 "freelocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L3:
	xorl	%ebx, %ebx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$1, %rbx
.L7:
	cmpq	$6, %rbx
	je	.L4
	movq	0(%rbp,%rbx,8), %rsi
	cmpl	$-1, 48(%rsi)
	je	.L5
	movl	%ebx, %edi
	call	_nl_remove_locale
.L5:
	cmpl	$12, %ebx
	jne	.L4
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L6
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 49 "freelocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L6:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	__freelocale, .-__freelocale
	.weak	freelocale
	.set	freelocale,__freelocale
	.hidden	_nl_remove_locale
	.hidden	__libc_setlocale_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	_nl_C_locobj
