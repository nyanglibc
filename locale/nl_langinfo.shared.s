	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_nl_langinfo
	.hidden	__GI_nl_langinfo
	.type	__GI_nl_langinfo, @function
__GI_nl_langinfo:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rsi
	jmp	__GI___nl_langinfo_l
	.size	__GI_nl_langinfo, .-__GI_nl_langinfo
	.globl	nl_langinfo
	.set	nl_langinfo,__GI_nl_langinfo
