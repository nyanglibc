	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_codeset
	.globl	_nl_C_codeset
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	_nl_C_codeset, @object
	.size	_nl_C_codeset, 15
_nl_C_codeset:
	.string	"ANSI_X3.4-1968"
	.hidden	_nl_POSIX_name
	.globl	_nl_POSIX_name
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	_nl_POSIX_name, @object
	.size	_nl_POSIX_name, 6
_nl_POSIX_name:
	.string	"POSIX"
	.hidden	_nl_C_name
	.globl	_nl_C_name
	.type	_nl_C_name, @object
	.size	_nl_C_name, 2
_nl_C_name:
	.string	"C"
