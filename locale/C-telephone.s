	.text
	.hidden	_nl_C_LC_TELEPHONE
	.globl	_nl_C_LC_TELEPHONE
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"+%c %a %l"
.LC1:
	.string	""
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_TELEPHONE, @object
	.size	_nl_C_LC_TELEPHONE, 104
_nl_C_LC_TELEPHONE:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	5
	.zero	4
	.quad	.LC0
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
