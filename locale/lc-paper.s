	.text
#APP
	.globl _nl_current_LC_PAPER_used
.set _nl_current_LC_PAPER_used, 2
#NO_APP
	.hidden	_nl_current_LC_PAPER
	.globl	_nl_current_LC_PAPER
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_PAPER, @object
	.size	_nl_current_LC_PAPER, 8
_nl_current_LC_PAPER:
	.quad	_nl_global_locale+56
	.hidden	_nl_global_locale
