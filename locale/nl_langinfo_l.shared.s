	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___nl_langinfo_l
	.hidden	__GI___nl_langinfo_l
	.type	__GI___nl_langinfo_l, @function
__GI___nl_langinfo_l:
	movl	%edi, %eax
	sarl	$16, %eax
	cmpl	$6, %eax
	je	.L4
	cmpl	$12, %eax
	jbe	.L7
.L4:
	leaq	.LC0(%rip), %rax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L7:
	movzwl	%di, %edi
	cltq
	cmpl	$65535, %edi
	je	.L8
	movq	(%rsi,%rax,8), %rdx
	leaq	.LC0(%rip), %rax
	cmpl	%edi, 56(%rdx)
	jbe	.L1
	movq	64(%rdx,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	128(%rsi,%rax,8), %rax
	ret
	.size	__GI___nl_langinfo_l, .-__GI___nl_langinfo_l
	.globl	__nl_langinfo_l
	.set	__nl_langinfo_l,__GI___nl_langinfo_l
	.weak	nl_langinfo_l
	.set	nl_langinfo_l,__nl_langinfo_l
