	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_LC_NAME
	.globl	_nl_C_LC_NAME
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%p%t%g%t%m%t%f"
.LC1:
	.string	""
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_NAME, @object
	.size	_nl_C_LC_NAME, 120
_nl_C_LC_NAME:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	7
	.zero	4
	.quad	.LC0
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
