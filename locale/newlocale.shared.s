	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"C"
.LC1:
	.string	"LOCPATH"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__newlocale
	.type	__newlocale, @function
__newlocale:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$408, %rsp
	cmpl	$64, %edi
	je	.L48
	testl	$-8128, %edi
	movl	%edi, %r12d
	jne	.L4
.L2:
	testq	%r14, %r14
	je	.L4
	leaq	_nl_C_locobj(%rip), %rax
	cmpq	%rax, %rbx
	je	.L49
	testq	%rbx, %rbx
	je	.L5
	cmpl	$8127, %r12d
	je	.L5
	movdqu	(%rbx), %xmm0
	testl	%r12d, %r12d
	movq	224(%rbx), %rax
	movaps	%xmm0, -288(%rbp)
	movdqu	16(%rbx), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -272(%rbp)
	movdqu	32(%rbx), %xmm0
	movaps	%xmm0, -256(%rbp)
	movdqu	48(%rbx), %xmm0
	movaps	%xmm0, -240(%rbp)
	movdqu	64(%rbx), %xmm0
	movaps	%xmm0, -224(%rbp)
	movdqu	80(%rbx), %xmm0
	movaps	%xmm0, -208(%rbp)
	movdqu	96(%rbx), %xmm0
	movaps	%xmm0, -192(%rbp)
	movdqu	112(%rbx), %xmm0
	movaps	%xmm0, -176(%rbp)
	movdqu	128(%rbx), %xmm0
	movaps	%xmm0, -160(%rbp)
	movdqu	144(%rbx), %xmm0
	movaps	%xmm0, -144(%rbp)
	movdqu	160(%rbx), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqu	176(%rbx), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqu	192(%rbx), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqu	208(%rbx), %xmm0
	movaps	%xmm0, -80(%rbp)
	jne	.L11
	movl	$232, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L97
	movdqa	-288(%rbp), %xmm0
	movups	%xmm0, (%rax)
	movdqa	-272(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	movdqa	-256(%rbp), %xmm0
	movups	%xmm0, 32(%rax)
	movdqa	-240(%rbp), %xmm0
	movups	%xmm0, 48(%rax)
	movdqa	-224(%rbp), %xmm0
	movups	%xmm0, 64(%rax)
	movdqa	-208(%rbp), %xmm0
	movups	%xmm0, 80(%rax)
	movdqa	-192(%rbp), %xmm0
	movups	%xmm0, 96(%rax)
	movdqa	-176(%rbp), %xmm0
	movups	%xmm0, 112(%rax)
	movdqa	-160(%rbp), %xmm0
	movups	%xmm0, 128(%rax)
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, 144(%rax)
	movdqa	-128(%rbp), %xmm0
	movups	%xmm0, 160(%rax)
	movdqa	-112(%rbp), %xmm0
	movups	%xmm0, 176(%rax)
	movdqa	-96(%rbp), %xmm0
	movups	%xmm0, 192(%rax)
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 208(%rax)
	movq	-64(%rbp), %rax
	movq	%rax, 224(%r15)
.L13:
	movq	(%r15), %rax
	movq	64(%rax), %rcx
	leaq	256(%rcx), %rdx
	movq	88(%rax), %rcx
	movq	72(%rax), %rax
	movq	%rdx, 104(%r15)
	leaq	512(%rcx), %rdx
	addq	$512, %rax
	movq	%rax, 120(%r15)
	movq	%rdx, 112(%r15)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L92:
	movl	-436(%rbp), %eax
	movl	-440(%rbp), %r12d
	movq	-448(%rbp), %rbx
	notl	%eax
	testl	%r12d, %eax
	je	.L19
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r15d, %r15d
	movl	$22, %fs:(%rax)
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	xorl	%ebx, %ebx
.L5:
	testl	%r12d, %r12d
	leaq	_nl_C_locobj(%rip), %r15
	je	.L1
	leaq	.LC0(%rip), %rdi
	movl	$2, %ecx
	movq	%r14, %rsi
	repz cmpsb
	je	.L1
	testq	%rbx, %rbx
	jne	.L100
	movdqu	_nl_C_locobj(%rip), %xmm0
	movq	224+_nl_C_locobj(%rip), %rax
	movaps	%xmm0, -288(%rbp)
	movdqu	16+_nl_C_locobj(%rip), %xmm0
	movq	%rax, -64(%rbp)
	movaps	%xmm0, -272(%rbp)
	movdqu	32+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -256(%rbp)
	movdqu	48+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -240(%rbp)
	movdqu	64+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -224(%rbp)
	movdqu	80+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -208(%rbp)
	movdqu	96+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -192(%rbp)
	movdqu	112+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -176(%rbp)
	movdqu	128+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -160(%rbp)
	movdqu	144+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -144(%rbp)
	movdqu	160+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -128(%rbp)
	movdqu	176+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -112(%rbp)
	movdqu	192+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -96(%rbp)
	movdqu	208+_nl_C_locobj(%rip), %xmm0
	movaps	%xmm0, -80(%rbp)
.L11:
	leaq	.LC1(%rip), %rdi
	movq	$0, -416(%rbp)
	movq	$0, -408(%rbp)
	call	__GI_getenv
	testq	%rax, %rax
	je	.L16
	cmpb	$0, (%rax)
	je	.L16
	leaq	-408(%rbp), %r15
	leaq	-416(%rbp), %r13
	movl	$58, %esi
	movq	%rax, %rdi
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	__argz_create_sep
	testl	%eax, %eax
	jne	.L97
	leaq	_nl_default_locale_path(%rip), %rdx
	movl	$58, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__argz_add_sep
	testl	%eax, %eax
	jne	.L97
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	-400(%rbp), %rdx
	xorl	%eax, %eax
	movq	%rdx, -424(%rbp)
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$1, %rax
.L15:
	cmpq	$6, %rax
	je	.L18
	cmpl	$12, %eax
	movq	%r14, (%rdx,%rax,8)
	jne	.L18
	movl	$59, %esi
	movq	%r14, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L19
	movq	%r14, %rdi
	leaq	_nl_category_name_sizes(%rip), %r13
	call	__GI_strlen
	leaq	1(%rax), %rdx
	addq	$31, %rax
	movq	%r14, %rsi
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	call	__GI_memcpy@PLT
	movl	%r12d, -440(%rbp)
	movl	$0, -436(%rbp)
	movq	%rax, %r12
	movq	%rbx, -448(%rbp)
	.p2align 4,,10
	.p2align 3
.L20:
	movl	$61, %esi
	movq	%r12, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, -432(%rbp)
	je	.L92
	subq	%r12, %rax
	xorl	%r15d, %r15d
	movq	%rax, %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L22:
	cmpl	$12, %r15d
	je	.L4
.L21:
	addq	$1, %r15
.L25:
	cmpq	$6, %r15
	je	.L21
	movzbl	0(%r13,%r15), %eax
	cmpq	%r14, %rax
	jne	.L22
	leaq	_nl_category_name_idxs(%rip), %rax
	movq	%r14, %rdx
	movq	%r12, %rdi
	movzbl	(%rax,%r15), %esi
	leaq	_nl_category_names(%rip), %rax
	addq	%rax, %rsi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	jne	.L22
	movq	-432(%rbp), %rdi
	movl	%r15d, %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	$59, %esi
	movslq	%r15d, %rcx
	orl	%eax, -436(%rbp)
	addq	$1, %rdi
	movq	%rdi, -400(%rbp,%rcx,8)
	call	__GI_strchr
	testq	%rax, %rax
	je	.L92
	movb	$0, (%rax)
	leaq	1(%rax), %r12
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$8127, %r12d
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L19:
	movl	__libc_pthread_functions_init(%rip), %ecx
	testl	%ecx, %ecx
	je	.L26
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 162 "newlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L26:
	xorl	%r15d, %r15d
	movq	%rbx, -432(%rbp)
	xorl	%r14d, %r14d
	leaq	-288(%rbp), %r13
	movq	%r15, %rbx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L28:
	cmpq	$6, %rbx
	leal	1(%rbx), %r15d
	je	.L34
	movq	128(%r13,%rbx,8), %rdi
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rdi
	je	.L33
.L95:
	call	__GI_strlen
	leaq	1(%r14,%rax), %r14
.L33:
	cmpl	$13, %r15d
	je	.L101
.L34:
	addq	$1, %rbx
.L27:
	btl	%ebx, %r12d
	jnc	.L28
	movq	-424(%rbp), %rax
	movq	-408(%rbp), %rsi
	movl	%ebx, %edx
	movq	-416(%rbp), %rdi
	leaq	(%rax,%rbx,8), %rcx
	call	_nl_find_locale
	testq	%rax, %rax
	movq	%rax, 0(%r13,%rbx,8)
	je	.L102
	movq	-424(%rbp), %rax
	leal	1(%rbx), %r15d
	movq	(%rax,%rbx,8), %rdi
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rdi
	jne	.L95
	cmpl	$13, %r15d
	jne	.L34
	.p2align 4,,10
	.p2align 3
.L101:
	movq	%r14, %rax
	movq	-432(%rbp), %rbx
	movl	%r15d, %r14d
	addq	$232, %rax
	movq	%rax, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L29
	xorl	%edx, %edx
	testq	%rbx, %rbx
	leaq	232(%rax), %rax
	je	.L103
	movq	%rbx, -432(%rbp)
	movq	%rdx, %r14
	movq	%r13, %rbx
	movl	%r12d, %r13d
	movq	%rax, %r12
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L105:
	movq	-432(%rbp), %rax
	movq	(%rax,%r14,8), %rsi
	cmpl	$-1, 48(%rsi)
	je	.L42
	movl	%r14d, %edi
	call	_nl_remove_locale
.L42:
	movq	(%rbx,%r14,8), %rax
	movq	%rax, (%r15,%r14,8)
	movq	-424(%rbp), %rax
	movq	(%rax,%r14,8), %rsi
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rsi
	je	.L96
.L46:
	movq	%r12, 128(%r15,%r14,8)
	movq	%r12, %rdi
	call	__GI_stpcpy@PLT
	leaq	1(%rax), %r12
.L44:
	cmpl	$12, %r14d
	je	.L104
.L45:
	addq	$1, %r14
.L37:
	btl	%r14d, %r13d
	jc	.L105
	cmpq	$6, %r14
	je	.L45
	movq	(%rbx,%r14,8), %rax
	movq	128(%rbx,%r14,8), %rsi
	movq	%rax, (%r15,%r14,8)
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rsi
	jne	.L46
.L96:
	movq	%rax, 128(%r15,%r14,8)
	jmp	.L44
.L102:
	movl	%ebx, %r14d
.L29:
	leal	-1(%r14), %r9d
	movslq	%r9d, %rbx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L32:
	btl	%ebx, %r12d
	jnc	.L31
	movq	0(%r13,%rbx,8), %rsi
	cmpl	$-1, 48(%rsi)
	je	.L31
	movl	%ebx, %edi
	call	_nl_remove_locale
.L31:
	subq	$1, %rbx
.L36:
	leal	1(%rbx), %eax
	testl	%eax, %eax
	jg	.L32
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L97
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 183 "newlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L97:
	xorl	%r15d, %r15d
	jmp	.L1
.L104:
	movq	-432(%rbp), %rbx
	movq	%rbx, %rdi
	call	free@PLT
.L40:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L13
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 263 "newlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L13
.L103:
	movq	-424(%rbp), %r14
	movq	%rax, %rdi
	.p2align 4,,10
	.p2align 3
.L39:
	btl	%ebx, %r12d
	jnc	.L38
	movq	(%r14,%rbx,8), %rsi
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rsi
	je	.L38
	movq	%rdi, 128(%r13,%rbx,8)
	call	__GI_stpcpy@PLT
	leaq	1(%rax), %rdi
.L38:
	addq	$1, %rbx
	cmpq	$13, %rbx
	jne	.L39
	movdqa	-288(%rbp), %xmm0
	movq	-64(%rbp), %rax
	movups	%xmm0, (%r15)
	movdqa	-272(%rbp), %xmm0
	movq	%rax, 224(%r15)
	movups	%xmm0, 16(%r15)
	movdqa	-256(%rbp), %xmm0
	movups	%xmm0, 32(%r15)
	movdqa	-240(%rbp), %xmm0
	movups	%xmm0, 48(%r15)
	movdqa	-224(%rbp), %xmm0
	movups	%xmm0, 64(%r15)
	movdqa	-208(%rbp), %xmm0
	movups	%xmm0, 80(%r15)
	movdqa	-192(%rbp), %xmm0
	movups	%xmm0, 96(%r15)
	movdqa	-176(%rbp), %xmm0
	movups	%xmm0, 112(%r15)
	movdqa	-160(%rbp), %xmm0
	movups	%xmm0, 128(%r15)
	movdqa	-144(%rbp), %xmm0
	movups	%xmm0, 144(%r15)
	movdqa	-128(%rbp), %xmm0
	movups	%xmm0, 160(%r15)
	movdqa	-112(%rbp), %xmm0
	movups	%xmm0, 176(%r15)
	movdqa	-96(%rbp), %xmm0
	movups	%xmm0, 192(%r15)
	movdqa	-80(%rbp), %xmm0
	movups	%xmm0, 208(%r15)
	jmp	.L40
.L100:
	leaq	-288(%rbp), %r13
	movl	$58, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	rep movsl
	jmp	.L11
	.size	__newlocale, .-__newlocale
	.weak	newlocale
	.set	newlocale,__newlocale
	.hidden	_nl_remove_locale
	.hidden	_nl_find_locale
	.hidden	_nl_C_name
	.hidden	__libc_setlocale_lock
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	_nl_category_names
	.hidden	_nl_category_name_idxs
	.hidden	_nl_category_name_sizes
	.hidden	__argz_add_sep
	.hidden	_nl_default_locale_path
	.hidden	__argz_create_sep
	.hidden	_nl_C_locobj
