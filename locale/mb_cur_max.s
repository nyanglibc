	.text
	.p2align 4,,15
	.weak	__ctype_get_mb_cur_max
	.type	__ctype_get_mb_cur_max, @function
__ctype_get_mb_cur_max:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	168(%rax), %eax
	ret
	.size	__ctype_get_mb_cur_max, .-__ctype_get_mb_cur_max
	.hidden	_nl_current_LC_CTYPE
