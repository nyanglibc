	.text
	.p2align 4,,15
	.globl	nl_langinfo
	.hidden	nl_langinfo
	.type	nl_langinfo, @function
nl_langinfo:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rsi
	jmp	__nl_langinfo_l
	.size	nl_langinfo, .-nl_langinfo
	.hidden	__nl_langinfo_l
