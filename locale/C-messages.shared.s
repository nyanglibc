	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_LC_MESSAGES
	.globl	_nl_C_LC_MESSAGES
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"^[yY]"
.LC1:
	.string	"^[nN]"
.LC2:
	.string	""
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_MESSAGES, @object
	.size	_nl_C_LC_MESSAGES, 104
_nl_C_LC_MESSAGES:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	5
	.zero	4
	.quad	.LC0
	.quad	.LC1
	.quad	.LC2
	.quad	.LC2
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
