	.text
#APP
	.globl _nl_current_LC_CTYPE_used
.set _nl_current_LC_CTYPE_used, 2
#NO_APP
	.p2align 4,,15
	.globl	_nl_postload_ctype
	.type	_nl_postload_ctype, @function
_nl_postload_ctype:
	movq	_nl_global_locale(%rip), %rdx
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rdi
	leaq	_nl_global_locale(%rip), %rsi
	movq	64(%rdx), %rax
	leaq	256(%rax), %rcx
	movq	88(%rdx), %rax
	movq	72(%rdx), %rdx
	movq	%rcx, 104+_nl_global_locale(%rip)
	addq	$512, %rax
	addq	$512, %rdx
	cmpq	%rsi, %fs:(%rdi)
	movq	%rax, 112+_nl_global_locale(%rip)
	movq	%rdx, 120+_nl_global_locale(%rip)
	je	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rsi
	movq	%rcx, %fs:(%rsi)
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	movq	%rdx, %fs:(%rcx)
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%rax, %fs:(%rdx)
	ret
	.size	_nl_postload_ctype, .-_nl_postload_ctype
	.hidden	_nl_current_LC_CTYPE
	.globl	_nl_current_LC_CTYPE
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_CTYPE, @object
	.size	_nl_current_LC_CTYPE, 8
_nl_current_LC_CTYPE:
	.quad	_nl_global_locale
	.hidden	_nl_global_locale
