	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_LC_ADDRESS
	.globl	_nl_C_LC_ADDRESS
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%a%N%f%N%d%N%b%N%s %h %e %r%N%C-%z %T%N%c%N"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	""
	.section	.data.rel.ro.local,"aw",@progbits
	.align 32
	.type	_nl_C_LC_ADDRESS, @object
	.size	_nl_C_LC_ADDRESS, 168
_nl_C_LC_ADDRESS:
	.quad	_nl_C_name
	.quad	0
	.quad	0
	.long	0
	.zero	4
	.quad	0
	.zero	8
	.long	-1
	.long	0
	.long	13
	.zero	4
	.quad	.LC0
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.long	0
	.zero	4
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	.LC1
	.quad	_nl_C_codeset
	.hidden	_nl_C_codeset
	.hidden	_nl_C_name
