	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4,,15
	.globl	__localeconv
	.type	__localeconv, @function
__localeconv:
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	64(%rax), %rdx
	movq	%rdx, result.7920(%rip)
	movq	72(%rax), %rdx
	movq	80(%rax), %rax
	movq	%rdx, 8+result.7920(%rip)
	movq	%rax, 16+result.7920(%rip)
	movzbl	(%rax), %eax
	andl	$127, %eax
	cmpb	$127, %al
	jne	.L2
	leaq	.LC0(%rip), %rax
	movq	%rax, 16+result.7920(%rip)
.L2:
	movq	_nl_current_LC_MONETARY@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movq	64(%rax), %rdx
	movq	%rdx, 24+result.7920(%rip)
	movq	72(%rax), %rdx
	movq	%rdx, 32+result.7920(%rip)
	movq	80(%rax), %rdx
	movq	%rdx, 40+result.7920(%rip)
	movq	88(%rax), %rdx
	movq	%rdx, 48+result.7920(%rip)
	movq	96(%rax), %rdx
	movq	%rdx, 56+result.7920(%rip)
	movzbl	(%rdx), %edx
	andl	$127, %edx
	cmpb	$127, %dl
	jne	.L3
	leaq	.LC0(%rip), %rsi
	movq	%rsi, 56+result.7920(%rip)
.L3:
	movq	104(%rax), %rdx
	movq	%rdx, 64+result.7920(%rip)
	movq	112(%rax), %rdx
	movq	%rdx, 72+result.7920(%rip)
	movq	120(%rax), %rdx
	movzbl	(%rdx), %ecx
	movl	$127, %edx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 80+result.7920(%rip)
	movq	128(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 81+result.7920(%rip)
	movq	136(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 82+result.7920(%rip)
	movq	144(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 83+result.7920(%rip)
	movq	152(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 84+result.7920(%rip)
	movq	160(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 85+result.7920(%rip)
	movq	168(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 86+result.7920(%rip)
	movq	176(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 87+result.7920(%rip)
	movq	192(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 88+result.7920(%rip)
	movq	200(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 89+result.7920(%rip)
	movq	208(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 90+result.7920(%rip)
	movq	216(%rax), %rcx
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 91+result.7920(%rip)
	movq	224(%rax), %rcx
	movq	232(%rax), %rax
	movzbl	(%rcx), %ecx
	cmpb	$-1, %cl
	cmove	%edx, %ecx
	movb	%cl, 92+result.7920(%rip)
	movzbl	(%rax), %eax
	cmpb	$-1, %al
	cmovne	%eax, %edx
	leaq	result.7920(%rip), %rax
	movb	%dl, 93+result.7920(%rip)
	ret
	.size	__localeconv, .-__localeconv
	.weak	localeconv
	.set	localeconv,__localeconv
	.local	result.7920
	.comm	result.7920,96,32
	.hidden	_nl_current_LC_MONETARY
	.hidden	_nl_current_LC_NUMERIC
