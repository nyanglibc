	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_nl_postload_ctype
	.type	_nl_postload_ctype, @function
_nl_postload_ctype:
	movq	_nl_global_locale(%rip), %rdx
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rsi
	leaq	_nl_global_locale(%rip), %rdi
	movq	64(%rdx), %rax
	leaq	256(%rax), %rcx
	movq	88(%rdx), %rax
	movq	72(%rdx), %rdx
	movq	%rcx, 104+_nl_global_locale(%rip)
	addq	$512, %rax
	addq	$512, %rdx
	cmpq	%rdi, %fs:(%rsi)
	movq	%rax, 112+_nl_global_locale(%rip)
	movq	%rdx, 120+_nl_global_locale(%rip)
	jne	.L2
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdi
	movq	%rcx, %fs:(%rdi)
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	movq	%rdx, %fs:(%rcx)
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%rax, %fs:(%rdx)
.L2:
#APP
# 96 "lc-ctype.c" 1
	.symver __ctype_b,__ctype_b@GLIBC_2.2.5
# 0 "" 2
# 97 "lc-ctype.c" 1
	.symver __ctype_tolower,__ctype_tolower@GLIBC_2.2.5
# 0 "" 2
# 98 "lc-ctype.c" 1
	.symver __ctype_toupper,__ctype_toupper@GLIBC_2.2.5
# 0 "" 2
# 99 "lc-ctype.c" 1
	.symver __ctype32_b,__ctype32_b@GLIBC_2.2.5
# 0 "" 2
# 100 "lc-ctype.c" 1
	.symver __ctype32_tolower,__ctype32_tolower@GLIBC_2.2.5
# 0 "" 2
# 101 "lc-ctype.c" 1
	.symver __ctype32_toupper,__ctype32_toupper@GLIBC_2.2.5
# 0 "" 2
#NO_APP
	movq	%fs:(%rsi), %rax
	movq	__ctype_b@GOTPCREL(%rip), %rcx
	movq	(%rax), %rax
	movq	64(%rax), %rdi
	movq	72(%rax), %rsi
	leaq	256(%rdi), %rdx
	movq	88(%rax), %rdi
	movq	%rdx, (%rcx)
	movq	__ctype_toupper@GOTPCREL(%rip), %rcx
	leaq	512(%rsi), %rdx
	movq	%rdx, (%rcx)
	movq	__ctype_tolower@GOTPCREL(%rip), %rcx
	leaq	512(%rdi), %rdx
	movq	%rdx, (%rcx)
	movq	104(%rax), %rcx
	movq	__ctype32_b@GOTPCREL(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	184(%rax), %rcx
	movq	__ctype32_toupper@GOTPCREL(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	192(%rax), %rdx
	movq	__ctype32_tolower@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
	ret
	.size	_nl_postload_ctype, .-_nl_postload_ctype
	.hidden	_nl_global_locale
