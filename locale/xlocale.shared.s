	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.hidden	_nl_C_locobj
	.globl	_nl_C_locobj
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	_nl_C_locobj, @object
	.size	_nl_C_locobj, 232
_nl_C_locobj:
	.quad	_nl_C_LC_CTYPE
	.quad	_nl_C_LC_NUMERIC
	.quad	_nl_C_LC_TIME
	.quad	_nl_C_LC_COLLATE
	.quad	_nl_C_LC_MONETARY
	.quad	_nl_C_LC_MESSAGES
	.zero	8
	.quad	_nl_C_LC_PAPER
	.quad	_nl_C_LC_NAME
	.quad	_nl_C_LC_ADDRESS
	.quad	_nl_C_LC_TELEPHONE
	.quad	_nl_C_LC_MEASUREMENT
	.quad	_nl_C_LC_IDENTIFICATION
	.quad	_nl_C_LC_CTYPE_class+256
	.quad	_nl_C_LC_CTYPE_tolower+512
	.quad	_nl_C_LC_CTYPE_toupper+512
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.quad	_nl_C_name
	.hidden	_nl_C_name
	.hidden	_nl_C_LC_CTYPE_toupper
	.hidden	_nl_C_LC_CTYPE_tolower
	.hidden	_nl_C_LC_CTYPE_class
