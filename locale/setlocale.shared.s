	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	new_composite_name, @function
new_composite_name:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movl	$1, %r13d
	pushq	%rbp
	pushq	%rbx
	xorl	%r12d, %r12d
	movl	%edi, %ebx
	subq	$24, %rsp
	movq	%rsi, (%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	%r14d, %ebx
	je	.L34
	leaq	128+_nl_global_locale(%rip), %rax
	movq	(%rax,%r14,8), %rbp
.L5:
	movq	%rbp, %rdi
	call	__GI_strlen
	movq	%rax, %r15
	leaq	_nl_category_name_sizes(%rip), %rax
	movzbl	(%rax,%r14), %eax
	leaq	2(%r12,%rax), %r12
	addq	%r15, %r12
	testl	%r13d, %r13d
	je	.L7
	movq	(%rsp), %rax
	movq	(%rax), %rsi
	cmpq	%rbp, %rsi
	je	.L7
	movq	%rbp, %rdi
	xorl	%r13d, %r13d
	call	__GI_strcmp
	testl	%eax, %eax
	sete	%r13b
.L7:
	cmpl	$12, %r14d
	je	.L35
.L3:
	addq	$1, %r14
.L2:
	cmpq	$6, %r14
	je	.L3
	cmpl	$6, %ebx
	jne	.L4
	movq	(%rsp), %rax
	movq	(%rax,%r14,8), %rbp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L35:
	testl	%r13d, %r13d
	je	.L8
	movq	(%rsp), %rax
	leaq	_nl_C_name(%rip), %rsi
	movq	(%rax), %rbp
	movq	%rbp, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L17
	leaq	_nl_POSIX_name(%rip), %rsi
	movq	%rbp, %rdi
	call	__GI_strcmp
	leaq	_nl_C_name(%rip), %rcx
	testl	%eax, %eax
	movq	%rcx, 8(%rsp)
	jne	.L36
.L1:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	_nl_C_name(%rip), %rax
	movq	%rax, 8(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, %rdi
	je	.L11
	xorl	%r15d, %r15d
	leaq	_nl_category_name_idxs(%rip), %r14
	leaq	_nl_category_names(%rip), %r13
	leaq	128+_nl_global_locale(%rip), %rbp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	%ebx, %r15d
	je	.L37
	movq	0(%rbp,%r15,8), %r12
.L15:
	movzbl	(%r14,%r15), %esi
	addq	%r13, %rsi
	call	__GI_stpcpy@PLT
	leaq	1(%rax), %rdi
	movb	$61, (%rax)
	movq	%r12, %rsi
	call	__GI_stpcpy@PLT
	cmpl	$12, %r15d
	leaq	1(%rax), %rdi
	movb	$59, (%rax)
	je	.L38
.L13:
	addq	$1, %r15
.L12:
	cmpq	$6, %r15
	je	.L13
	cmpl	$6, %ebx
	jne	.L14
	movq	(%rsp), %rax
	movq	(%rax,%r15,8), %r12
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L38:
	movb	$0, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	1(%r15), %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L11
	addq	$24, %rsp
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	movq	%rax, %rdi
	jmp	__GI_memcpy@PLT
.L34:
	movq	(%rsp), %rax
	movq	(%rax), %rbp
	jmp	.L5
.L37:
	movq	(%rsp), %rax
	movq	(%rax), %r12
	jmp	.L15
.L11:
	movq	$0, 8(%rsp)
	jmp	.L1
	.size	new_composite_name, .-new_composite_name
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"LOCPATH"
	.text
	.p2align 4,,15
	.globl	__GI_setlocale
	.hidden	__GI_setlocale
	.type	__GI_setlocale, @function
__GI_setlocale:
	pushq	%r15
	pushq	%r14
	movslq	%edi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$280, %rsp
	cmpq	$12, %r14
	ja	.L150
	testq	%rsi, %rsi
	je	.L154
	movl	__libc_pthread_functions_init(%rip), %r8d
	movq	%rsi, %rbx
	movl	%edi, %ebp
	testl	%r8d, %r8d
	je	.L43
	leaq	_nl_global_locale(%rip), %r12
	leaq	16(%r14), %r13
	movq	144+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 234 "setlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	movq	(%r12,%r13,8), %r15
	movq	%rbx, %rdi
	movq	%r15, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	jne	.L44
	movl	__libc_pthread_functions_init(%rip), %edi
	testl	%edi, %edi
	je	.L39
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 239 "setlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	movq	(%r12,%r13,8), %r15
.L39:
	addq	$280, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	_nl_global_locale(%rip), %r12
	movq	%rbx, %rdi
	movq	128(%r12,%r14,8), %r15
	movq	%r15, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L39
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC0(%rip), %rdi
	movq	$0, 32(%rsp)
	movq	$0, 40(%rsp)
	call	__GI_getenv
	testq	%rax, %rax
	je	.L46
	cmpb	$0, (%rax)
	jne	.L155
.L46:
	cmpl	$6, %ebp
	jne	.L51
	xorl	%eax, %eax
	leaq	48(%rsp), %rbp
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L52:
	addq	$1, %rax
.L102:
	cmpq	$6, %rax
	je	.L52
	cmpl	$12, %eax
	movq	%rbx, 0(%rbp,%rax,8)
	jne	.L52
	movl	$59, %esi
	movq	%rbx, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	jne	.L156
.L53:
	movl	$13, %ebx
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L159:
	movq	(%rsp), %rcx
	movq	%rcx, 48(%rsp,%r13,8)
.L70:
	movslq	%r15d, %rbx
.L71:
	testl	%ebx, %ebx
	leal	-1(%rbx), %r15d
	je	.L157
	cmpl	$6, %r15d
	je	.L158
.L65:
	movslq	%r15d, %r13
	movq	40(%rsp), %rsi
	movq	32(%rsp), %rdi
	leaq	0(%rbp,%r13,8), %rcx
	movl	%r15d, %edx
	call	_nl_find_locale
	testq	%rax, %rax
	movq	%rax, 160(%rsp,%r13,8)
	je	.L66
	cmpl	$-1, 48(%rax)
	je	.L67
	movl	$-1, 48(%rax)
.L67:
	movq	48(%rsp,%r13,8), %r14
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %r14
	je	.L70
	movq	128(%r12,%r13,8), %rcx
	movq	%r14, %rdi
	movq	%rcx, %rsi
	movq	%rcx, (%rsp)
	call	__GI_strcmp
	testl	%eax, %eax
	je	.L159
	movq	%r14, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, 48(%rsp,%r13,8)
	jne	.L70
	.p2align 4,,10
	.p2align 3
.L66:
	cmpl	$13, %ebx
	je	.L84
.L103:
	leaq	_nl_C_name(%rip), %r13
	leaq	128+_nl_global_locale(%rip), %r12
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L82:
	addq	$1, %rbx
.L74:
	cmpl	$6, %ebx
	je	.L82
	movq	0(%rbp,%rbx,8), %rdi
	cmpq	%r13, %rdi
	je	.L83
	cmpq	(%r12,%rbx,8), %rdi
	je	.L83
	call	free@PLT
.L83:
	cmpl	$12, %ebx
	jne	.L82
.L84:
	xorl	%r15d, %r15d
.L75:
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L85
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 398 "setlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L85:
	movq	32(%rsp), %rdi
	call	free@PLT
	movq	16(%rsp), %rdi
	call	free@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	_nl_global_locale(%rip), %rax
	movq	128(%rax,%r14,8), %r15
	jmp	.L39
.L60:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L160:
	cmpl	$13, %edx
	je	.L53
.L63:
	addq	$1, %rax
	addl	$1, %edx
.L62:
	cmpq	$6, %rax
	je	.L63
	cmpq	0(%rbp,%rax,8), %rbx
	jne	.L160
	.p2align 4,,10
	.p2align 3
.L64:
	movl	__libc_pthread_functions_init(%rip), %ecx
	testl	%ecx, %ecx
	je	.L59
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 306 "setlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L59:
	movq	16(%rsp), %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L150:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L152:
	xorl	%r15d, %r15d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rbx, 160(%rsp)
	movq	40(%rsp), %rsi
	leaq	160(%rsp), %rbx
	movq	32(%rsp), %rdi
	movl	%ebp, %edx
	movq	%rbx, %rcx
	call	_nl_find_locale
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L87
	cmpl	$-1, 48(%rax)
	je	.L88
	movl	$-1, 48(%rax)
.L88:
	movq	160(%rsp), %rdi
	leaq	_nl_C_name(%rip), %r13
	cmpq	%r13, %rdi
	je	.L93
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, 160(%rsp)
	je	.L87
.L93:
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	new_composite_name
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L161
	leaq	_nl_category_postload(%rip), %rax
	movq	%r15, (%r12,%r14,8)
	movq	(%rax,%r14,8), %rax
	testq	%rax, %rax
	je	.L95
	call	*%rax
.L95:
	movq	160(%rsp), %rbp
	movq	128(%r12,%r14,8), %rdi
	cmpq	%rdi, %rbp
	je	.L96
	cmpq	%r13, %rdi
	je	.L97
	call	free@PLT
.L97:
	movq	%rbp, 128(%r12,%r14,8)
.L96:
	movq	176+_nl_global_locale(%rip), %rdi
	cmpq	%rdi, %rbx
	je	.L98
	cmpq	%r13, %rdi
	je	.L99
	call	free@PLT
.L99:
	movq	%rbx, 176+_nl_global_locale(%rip)
.L98:
	movq	_nl_msg_cat_cntr@GOTPCREL(%rip), %rax
	addl	$1, (%rax)
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L161:
	movq	160(%rsp), %rdi
	cmpq	%r13, %rdi
	je	.L87
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L87:
	movq	$0, 160(%rsp)
.L94:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L100
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 461 "setlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L100:
	movq	32(%rsp), %rdi
	call	free@PLT
	movq	160(%rsp), %r15
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L155:
	leaq	40(%rsp), %r15
	leaq	32(%rsp), %r13
	movl	$58, %esi
	movq	%rax, %rdi
	movq	%r15, %rcx
	movq	%r13, %rdx
	call	__argz_create_sep
	testl	%eax, %eax
	je	.L47
.L153:
	movl	__libc_pthread_functions_init(%rip), %esi
	testl	%esi, %esi
	je	.L152
	movq	152+__libc_pthread_functions(%rip), %rax
	leaq	__libc_setlocale_lock(%rip), %rdi
#APP
# 260 "setlocale.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	_nl_default_locale_path(%rip), %rdx
	movl	$58, %ecx
	movq	%r15, %rsi
	movq	%r13, %rdi
	call	__argz_add_sep
	testl	%eax, %eax
	jne	.L153
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L157:
	movq	%rbp, %rsi
	movl	$6, %edi
	call	new_composite_name
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L103
	xorl	%r14d, %r14d
	leaq	160(%rsp), %rbx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L76:
	addq	$1, %r14
.L73:
	cmpq	$6, %r14
	je	.L76
	movq	(%rbx,%r14,8), %rcx
	leaq	_nl_category_postload(%rip), %rax
	leaq	0(,%r14,8), %r13
	movq	%rcx, (%r12,%r14,8)
	movq	(%rax,%r14,8), %rcx
	testq	%rcx, %rcx
	je	.L77
	call	*%rcx
.L77:
	leaq	128+_nl_global_locale(%rip), %rax
	movq	0(%rbp,%r14,8), %rcx
	movq	(%rax,%r13), %rdi
	cmpq	%rdi, %rcx
	je	.L78
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rdi
	je	.L79
	movq	%rcx, (%rsp)
	call	free@PLT
	movq	(%rsp), %rcx
.L79:
	leaq	128+_nl_global_locale(%rip), %rax
	movq	%rcx, (%rax,%r13)
.L78:
	cmpl	$12, %r14d
	jne	.L76
	movq	176+_nl_global_locale(%rip), %rdi
	cmpq	%rdi, %r15
	je	.L80
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rdi
	je	.L81
	call	free@PLT
.L81:
	movq	%r15, 176+_nl_global_locale(%rip)
.L80:
	movq	_nl_msg_cat_cntr@GOTPCREL(%rip), %rax
	addl	$1, (%rax)
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L158:
	movl	$6, %ebx
	movl	$5, %r15d
	jmp	.L65
.L156:
	movq	%rbx, %rdi
	leaq	_nl_category_name_sizes(%rip), %r14
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	movq	%rax, 8(%rsp)
	je	.L153
	.p2align 4,,10
	.p2align 3
.L54:
	movq	8(%rsp), %r15
	movl	$61, %esi
	movq	%r15, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r8
	movq	%rax, 24(%rsp)
	je	.L60
	subq	%r15, %r8
	xorl	%r13d, %r13d
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$12, %r13d
	je	.L64
.L56:
	addq	$1, %r13
.L61:
	cmpq	$6, %r13
	je	.L56
	movzbl	(%r14,%r13), %eax
	cmpq	%r8, %rax
	jne	.L57
	leaq	_nl_category_name_idxs(%rip), %rax
	movq	8(%rsp), %rdi
	movq	%r8, %rdx
	movq	%r8, (%rsp)
	movzbl	(%rax,%r13), %esi
	leaq	_nl_category_names(%rip), %rax
	addq	%rax, %rsi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	movq	(%rsp), %r8
	jne	.L57
	movq	24(%rsp), %rdi
	movslq	%r13d, %r9
	movl	$59, %esi
	addq	$1, %rdi
	movq	%rdi, 48(%rsp,%r9,8)
	call	__GI_strchr
	testq	%rax, %rax
	je	.L60
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, 8(%rsp)
	jmp	.L54
	.size	__GI_setlocale, .-__GI_setlocale
	.globl	setlocale
	.set	setlocale,__GI_setlocale
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_category, @function
free_category:
	leaq	_nl_locale_file_list(%rip), %rax
	pushq	%r14
	cmpq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movslq	%edi, %rbp
	pushq	%rbx
	movq	(%rax,%rbp,8), %rbx
	je	.L164
	leaq	_nl_category_postload(%rip), %rax
	leaq	_nl_global_locale(%rip), %r13
	movq	(%rax,%rbp,8), %rax
	movq	%rdx, 0(%r13,%rbp,8)
	testq	%rax, %rax
	je	.L165
	call	*%rax
.L165:
	addq	$16, %rbp
	leaq	_nl_C_name(%rip), %r14
	movq	0(%r13,%rbp,8), %rdi
	cmpq	%r14, %rdi
	je	.L164
	call	free@PLT
	movq	%r14, 0(%r13,%rbp,8)
.L164:
	testq	%rbx, %rbx
	jne	.L169
	jmp	.L162
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%rbp, %rbx
.L169:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L168
	cmpq	%rdi, %r12
	je	.L168
	call	_nl_unload_locale
.L168:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rbp
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L170
.L162:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	free_category, .-free_category
	.p2align 4,,15
	.globl	_nl_locale_subfreeres
	.hidden	_nl_locale_subfreeres
	.type	_nl_locale_subfreeres, @function
_nl_locale_subfreeres:
	pushq	%r12
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %r12
	pushq	%rbp
	leaq	_nl_C_locobj(%rip), %rbp
	pushq	%rbx
	xorl	%ebx, %ebx
	movq	%fs:(%r12), %rcx
	jmp	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%fs:(%r12), %rcx
.L186:
	addq	$1, %rbx
.L185:
	cmpq	$6, %rbx
	je	.L186
	movslq	%ebx, %rax
	movq	0(%rbp,%rbx,8), %rdx
	movl	%ebx, %edi
	movq	(%rcx,%rax,8), %rsi
	call	free_category
	cmpl	$12, %ebx
	jne	.L187
	movq	176+_nl_global_locale(%rip), %rdi
	leaq	_nl_C_name(%rip), %rbx
	cmpq	%rbx, %rdi
	je	.L188
	call	free@PLT
	movq	%rbx, 176+_nl_global_locale(%rip)
.L188:
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	_nl_archive_subfreeres
	.size	_nl_locale_subfreeres, .-_nl_locale_subfreeres
	.hidden	__libc_setlocale_lock
	.globl	__libc_setlocale_lock
	.bss
	.align 32
	.type	__libc_setlocale_lock, @object
	.size	__libc_setlocale_lock, 56
__libc_setlocale_lock:
	.zero	56
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	_nl_category_postload, @object
	.size	_nl_category_postload, 104
_nl_category_postload:
	.quad	_nl_postload_ctype
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	8
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.hidden	_nl_category_name_sizes
	.globl	_nl_category_name_sizes
	.section	.rodata
	.align 8
	.type	_nl_category_name_sizes, @object
	.size	_nl_category_name_sizes, 13
_nl_category_name_sizes:
	.byte	8
	.byte	10
	.byte	7
	.byte	10
	.byte	11
	.byte	11
	.byte	6
	.byte	8
	.byte	7
	.byte	10
	.byte	12
	.byte	14
	.byte	17
	.hidden	_nl_category_name_idxs
	.globl	_nl_category_name_idxs
	.align 8
	.type	_nl_category_name_idxs, @object
	.size	_nl_category_name_idxs, 13
_nl_category_name_idxs:
	.byte	11
	.byte	32
	.byte	43
	.byte	0
	.byte	20
	.byte	51
	.zero	1
	.byte	63
	.byte	72
	.byte	80
	.byte	91
	.byte	104
	.byte	119
	.hidden	_nl_category_names
	.globl	_nl_category_names
	.align 32
	.type	_nl_category_names, @object
	.size	_nl_category_names, 137
_nl_category_names:
	.string	"LC_COLLATE"
	.string	"LC_CTYPE"
	.string	"LC_MONETARY"
	.string	"LC_NUMERIC"
	.string	"LC_TIME"
	.string	"LC_MESSAGES"
	.string	"LC_PAPER"
	.string	"LC_NAME"
	.string	"LC_ADDRESS"
	.string	"LC_TELEPHONE"
	.string	"LC_MEASUREMENT"
	.string	"LC_IDENTIFICATION"
	.hidden	_nl_archive_subfreeres
	.hidden	_nl_C_locobj
	.hidden	_nl_unload_locale
	.hidden	_nl_locale_file_list
	.hidden	__argz_add_sep
	.hidden	_nl_default_locale_path
	.hidden	__argz_create_sep
	.hidden	_nl_find_locale
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	_nl_POSIX_name
	.hidden	_nl_C_name
	.hidden	_nl_global_locale
