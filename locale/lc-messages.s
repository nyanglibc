	.text
#APP
	.globl _nl_current_LC_MESSAGES_used
.set _nl_current_LC_MESSAGES_used, 2
#NO_APP
	.hidden	_nl_current_LC_MESSAGES
	.globl	_nl_current_LC_MESSAGES
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_MESSAGES, @object
	.size	_nl_current_LC_MESSAGES, 8
_nl_current_LC_MESSAGES:
	.quad	_nl_global_locale+40
	.hidden	_nl_global_locale
