	.text
	.p2align 4,,15
	.type	new_composite_name, @function
new_composite_name:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movl	$1, %r13d
	pushq	%rbp
	pushq	%rbx
	xorl	%r12d, %r12d
	movl	%edi, %ebx
	subq	$24, %rsp
	movq	%rsi, (%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	cmpl	%r14d, %ebx
	je	.L34
	leaq	128+_nl_global_locale(%rip), %rax
	movq	(%rax,%r14,8), %rbp
.L5:
	movq	%rbp, %rdi
	call	strlen
	movq	%rax, %r15
	leaq	_nl_category_name_sizes(%rip), %rax
	movzbl	(%rax,%r14), %eax
	leaq	2(%r12,%rax), %r12
	addq	%r15, %r12
	testl	%r13d, %r13d
	je	.L7
	movq	(%rsp), %rax
	movq	(%rax), %rsi
	cmpq	%rbp, %rsi
	je	.L7
	movq	%rbp, %rdi
	xorl	%r13d, %r13d
	call	strcmp
	testl	%eax, %eax
	sete	%r13b
.L7:
	cmpl	$12, %r14d
	je	.L35
.L3:
	addq	$1, %r14
.L2:
	cmpq	$6, %r14
	je	.L3
	cmpl	$6, %ebx
	jne	.L4
	movq	(%rsp), %rax
	movq	(%rax,%r14,8), %rbp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L35:
	testl	%r13d, %r13d
	je	.L8
	movq	(%rsp), %rax
	leaq	_nl_C_name(%rip), %rsi
	movq	(%rax), %rbp
	movq	%rbp, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L17
	leaq	_nl_POSIX_name(%rip), %rsi
	movq	%rbp, %rdi
	call	strcmp
	leaq	_nl_C_name(%rip), %rcx
	testl	%eax, %eax
	movq	%rcx, 8(%rsp)
	jne	.L36
.L1:
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	_nl_C_name(%rip), %rax
	movq	%rax, 8(%rsp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	movq	%rax, %rdi
	je	.L11
	xorl	%r15d, %r15d
	leaq	_nl_category_name_idxs(%rip), %r14
	leaq	_nl_category_names(%rip), %r13
	leaq	128+_nl_global_locale(%rip), %rbp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	%ebx, %r15d
	je	.L37
	movq	0(%rbp,%r15,8), %r12
.L15:
	movzbl	(%r14,%r15), %esi
	addq	%r13, %rsi
	call	__stpcpy@PLT
	leaq	1(%rax), %rdi
	movb	$61, (%rax)
	movq	%r12, %rsi
	call	__stpcpy@PLT
	cmpl	$12, %r15d
	leaq	1(%rax), %rdi
	movb	$59, (%rax)
	je	.L38
.L13:
	addq	$1, %r15
.L12:
	cmpq	$6, %r15
	je	.L13
	cmpl	$6, %ebx
	jne	.L14
	movq	(%rsp), %rax
	movq	(%rax,%r15,8), %r12
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L38:
	movb	$0, (%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	1(%r15), %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L11
	addq	$24, %rsp
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	movq	%rax, %rdi
	jmp	memcpy@PLT
.L34:
	movq	(%rsp), %rax
	movq	(%rax), %rbp
	jmp	.L5
.L37:
	movq	(%rsp), %rax
	movq	(%rax), %r12
	jmp	.L15
.L11:
	movq	$0, 8(%rsp)
	jmp	.L1
	.size	new_composite_name, .-new_composite_name
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"LOCPATH"
	.text
	.p2align 4,,15
	.globl	setlocale
	.hidden	setlocale
	.type	setlocale, @function
setlocale:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movslq	%edi, %rbp
	subq	$280, %rsp
	cmpq	$12, %rbp
	ja	.L171
	testq	%rsi, %rsi
	movq	%rsi, %r15
	je	.L173
	cmpq	$0, __pthread_rwlock_wrlock@GOTPCREL(%rip)
	movq	%rbp, %r12
	je	.L43
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_wrlock@PLT
.L43:
	leaq	_nl_global_locale(%rip), %rbx
	leaq	16(%rbp), %r13
	movq	%r15, %rdi
	movq	(%rbx,%r13,8), %r14
	movq	%r14, %rsi
	call	strcmp
	testl	%eax, %eax
	jne	.L44
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L39
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
	movq	(%rbx,%r13,8), %r14
.L39:
	addq	$280, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	leaq	.LC0(%rip), %rdi
	movq	$0, 32(%rsp)
	movq	$0, 40(%rsp)
	call	getenv
	testq	%rax, %rax
	je	.L46
	cmpb	$0, (%rax)
	jne	.L174
.L46:
	cmpl	$6, %r12d
	je	.L175
	leaq	_nl_current_used(%rip), %rax
	movq	%r15, 160(%rsp)
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	movq	%rax, (%rsp)
	je	.L113
	movq	40(%rsp), %rsi
	movq	32(%rsp), %rdi
	leaq	160(%rsp), %rcx
	movl	%r12d, %edx
	call	_nl_find_locale
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L91
	cmpl	$-1, 48(%rax)
	je	.L170
	movl	$-1, 48(%rax)
.L170:
	movq	160(%rsp), %r15
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L173:
	leaq	_nl_global_locale(%rip), %rax
	movq	128(%rax,%rbp,8), %r14
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L181:
	movq	160(%rsp), %rdi
	cmpq	%r13, %rdi
	je	.L91
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L91:
	movq	$0, 160(%rsp)
.L98:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L106
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L106:
	movq	32(%rsp), %rdi
	call	free@PLT
	movq	160(%rsp), %r14
	jmp	.L39
.L59:
	movl	$1, %edx
	xorl	%eax, %eax
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L176:
	cmpl	$13, %edx
	je	.L52
.L62:
	addq	$1, %rax
	addl	$1, %edx
.L61:
	cmpq	$6, %rax
	je	.L62
	cmpq	%r15, 0(%rbp,%rax,8)
	jne	.L176
	.p2align 4,,10
	.p2align 3
.L63:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L58
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
	.p2align 4,,10
	.p2align 3
.L58:
	movq	16(%rsp), %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L171:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$22, %fs:(%rax)
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L175:
	xorl	%eax, %eax
	leaq	48(%rsp), %rbp
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L51:
	addq	$1, %rax
.L108:
	cmpq	$6, %rax
	je	.L51
	cmpl	$12, %eax
	movq	%r15, 0(%rbp,%rax,8)
	jne	.L51
	movl	$59, %esi
	movq	%r15, %rdi
	call	strchr
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	jne	.L177
.L52:
	movl	$13, %r12d
	testl	%r12d, %r12d
	leal	-1(%r12), %r15d
	je	.L178
.L72:
	cmpl	$6, %r15d
	je	.L179
.L64:
	movslq	%r15d, %r14
	movq	40(%rsp), %rsi
	movq	32(%rsp), %rdi
	leaq	0(%rbp,%r14,8), %rcx
	movl	%r15d, %edx
	call	_nl_find_locale
	testq	%rax, %rax
	movq	%rax, 160(%rsp,%r14,8)
	je	.L180
	cmpl	$-1, 48(%rax)
	je	.L68
	movl	$-1, 48(%rax)
.L68:
	movq	48(%rsp,%r14,8), %rdx
	leaq	_nl_C_name(%rip), %r13
	cmpq	%r13, %rdx
	je	.L70
	movq	128(%rbx,%r14,8), %rcx
	movq	%rdx, %rdi
	movq	%rdx, (%rsp)
	movq	%rcx, %rsi
	movq	%rcx, 8(%rsp)
	call	strcmp
	testl	%eax, %eax
	movq	(%rsp), %rdx
	jne	.L69
	movq	8(%rsp), %rcx
	movq	%rcx, 48(%rsp,%r14,8)
.L70:
	movl	%r15d, %r12d
	testl	%r12d, %r12d
	leal	-1(%r12), %r15d
	jne	.L72
.L178:
	movq	%rbp, %rsi
	movl	$6, %edi
	leaq	_nl_C_name(%rip), %r13
	call	new_composite_name
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L76
	xorl	%r15d, %r15d
	leaq	128+_nl_global_locale(%rip), %r12
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L77:
	addq	$1, %r15
.L109:
	cmpq	$6, %r15
	je	.L77
	leaq	_nl_current_used(%rip), %rax
	leaq	0(,%r15,8), %r13
	cmpq	$0, (%rax,%r15,8)
	je	.L79
	movq	160(%rsp,%r15,8), %rsi
	leaq	_nl_category_postload(%rip), %rax
	movq	%rsi, (%rbx,%r15,8)
	movq	(%rax,%r15,8), %rsi
	testq	%rsi, %rsi
	je	.L79
	call	*%rsi
.L79:
	movq	0(%rbp,%r15,8), %rsi
	movq	(%r12,%r13), %rdi
	cmpq	%rdi, %rsi
	je	.L81
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rdi
	je	.L82
	movq	%rsi, (%rsp)
	call	free@PLT
	movq	(%rsp), %rsi
.L82:
	movq	%rsi, (%r12,%r13)
.L81:
	cmpl	$12, %r15d
	jne	.L77
	movq	176+_nl_global_locale(%rip), %rdi
	cmpq	%rdi, %r14
	je	.L83
	leaq	_nl_C_name(%rip), %rax
	cmpq	%rax, %rdi
	je	.L84
	call	free@PLT
.L84:
	movq	%r14, 176+_nl_global_locale(%rip)
.L83:
	addl	$1, _nl_msg_cat_cntr(%rip)
	.p2align 4,,10
	.p2align 3
.L75:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L88
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L88:
	movq	32(%rsp), %rdi
	call	free@PLT
	movq	16(%rsp), %rdi
	call	free@PLT
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L113:
	xorl	%r14d, %r14d
.L89:
	leaq	_nl_C_name(%rip), %r13
	cmpq	%r13, %r15
	je	.L97
	movq	%r15, %rdi
	call	__strdup
	testq	%rax, %rax
	movq	%rax, 160(%rsp)
	je	.L91
.L97:
	leaq	160(%rsp), %rsi
	movl	%r12d, %edi
	call	new_composite_name
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L181
	cmpq	$0, (%rsp)
	je	.L100
	leaq	_nl_category_postload(%rip), %rax
	movq	%r14, (%rbx,%rbp,8)
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.L100
	call	*%rax
.L100:
	movq	160(%rsp), %r14
	movq	128(%rbx,%rbp,8), %rdi
	cmpq	%rdi, %r14
	je	.L102
	cmpq	%r13, %rdi
	je	.L103
	call	free@PLT
.L103:
	movq	%r14, 128(%rbx,%rbp,8)
.L102:
	movq	176+_nl_global_locale(%rip), %rdi
	cmpq	%rdi, %r12
	je	.L104
	cmpq	%r13, %rdi
	je	.L105
	call	free@PLT
.L105:
	movq	%r12, 176+_nl_global_locale(%rip)
.L104:
	addl	$1, _nl_msg_cat_cntr(%rip)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L174:
	leaq	40(%rsp), %r14
	leaq	32(%rsp), %r13
	movl	$58, %esi
	movq	%rax, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	call	__argz_create_sep
	testl	%eax, %eax
	je	.L47
.L49:
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L172
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
.L172:
	xorl	%r14d, %r14d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L47:
	leaq	_nl_default_locale_path(%rip), %rdx
	movl	$58, %ecx
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	__argz_add_sep
	testl	%eax, %eax
	jne	.L49
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L180:
	leaq	_nl_C_name(%rip), %r13
	cmpq	%r13, 48(%rsp,%r14,8)
	je	.L70
.L67:
	cmpl	$13, %r12d
	je	.L87
.L76:
	movslq	%r12d, %rbx
	leaq	128+_nl_global_locale(%rip), %r12
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L85:
	addq	$1, %rbx
.L74:
	cmpl	$6, %ebx
	je	.L85
	movq	0(%rbp,%rbx,8), %rdi
	cmpq	%r13, %rdi
	je	.L86
	cmpq	(%r12,%rbx,8), %rdi
	je	.L86
	call	free@PLT
.L86:
	cmpl	$12, %ebx
	jne	.L85
.L87:
	xorl	%r14d, %r14d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L69:
	movq	%rdx, %rdi
	call	__strdup
	testq	%rax, %rax
	movq	%rax, 48(%rsp,%r14,8)
	jne	.L70
	jmp	.L67
.L177:
	movq	%r15, %rdi
	leaq	_nl_category_name_sizes(%rip), %r13
	call	__strdup
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	movq	%rax, 8(%rsp)
	je	.L182
	.p2align 4,,10
	.p2align 3
.L53:
	movq	8(%rsp), %r14
	movl	$61, %esi
	movq	%r14, %rdi
	call	strchr
	testq	%rax, %rax
	movq	%rax, %r9
	movq	%rax, 24(%rsp)
	je	.L59
	subq	%r14, %r9
	xorl	%r12d, %r12d
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	$12, %r12d
	je	.L63
.L55:
	addq	$1, %r12
.L60:
	cmpq	$6, %r12
	je	.L55
	movzbl	0(%r13,%r12), %eax
	cmpq	%r9, %rax
	jne	.L56
	leaq	_nl_category_name_idxs(%rip), %rax
	movq	8(%rsp), %rdi
	movq	%r9, %rdx
	movq	%r9, (%rsp)
	movzbl	(%rax,%r12), %esi
	leaq	_nl_category_names(%rip), %rax
	addq	%rax, %rsi
	call	memcmp@PLT
	testl	%eax, %eax
	movq	(%rsp), %r9
	jne	.L56
	movq	24(%rsp), %rdi
	movslq	%r12d, %r10
	movl	$59, %esi
	addq	$1, %rdi
	movq	%rdi, 48(%rsp,%r10,8)
	call	strchr
	testq	%rax, %rax
	je	.L59
	movb	$0, (%rax)
	addq	$1, %rax
	movq	%rax, 8(%rsp)
	jmp	.L53
.L179:
	movl	$6, %r12d
	movl	$5, %r15d
	jmp	.L64
.L182:
	xorl	%r14d, %r14d
	cmpq	$0, __pthread_rwlock_unlock@GOTPCREL(%rip)
	je	.L39
	leaq	__libc_setlocale_lock(%rip), %rdi
	call	__pthread_rwlock_unlock@PLT
	jmp	.L39
	.size	setlocale, .-setlocale
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_category, @function
free_category:
	leaq	_nl_locale_file_list(%rip), %rax
	pushq	%r14
	cmpq	%rdx, %rsi
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movslq	%edi, %rbp
	pushq	%rbx
	movq	(%rax,%rbp,8), %rbx
	je	.L185
	leaq	_nl_current_used(%rip), %rax
	leaq	_nl_global_locale(%rip), %r13
	cmpq	$0, (%rax,%rbp,8)
	je	.L187
	leaq	_nl_category_postload(%rip), %rax
	movq	%rdx, 0(%r13,%rbp,8)
	movq	(%rax,%rbp,8), %rax
	testq	%rax, %rax
	je	.L187
	call	*%rax
.L187:
	addq	$16, %rbp
	leaq	_nl_C_name(%rip), %r14
	movq	0(%r13,%rbp,8), %rdi
	cmpq	%r14, %rdi
	je	.L185
	call	free@PLT
	movq	%r14, 0(%r13,%rbp,8)
.L185:
	testq	%rbx, %rbx
	jne	.L192
	jmp	.L183
	.p2align 4,,10
	.p2align 3
.L193:
	movq	%rbp, %rbx
.L192:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L191
	cmpq	%rdi, %r12
	je	.L191
	call	_nl_unload_locale
.L191:
	movq	(%rbx), %rdi
	movq	24(%rbx), %rbp
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L193
.L183:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	free_category, .-free_category
	.p2align 4,,15
	.globl	_nl_locale_subfreeres
	.hidden	_nl_locale_subfreeres
	.type	_nl_locale_subfreeres, @function
_nl_locale_subfreeres:
	cmpq	$0, _nl_current_LC_COLLATE_used@GOTPCREL(%rip)
	pushq	%rbx
	je	.L208
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movq	_nl_C_LC_COLLATE@GOTPCREL(%rip), %rdx
	movl	$3, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L208:
	cmpq	$0, _nl_current_LC_CTYPE_used@GOTPCREL(%rip)
	je	.L209
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	_nl_C_LC_CTYPE@GOTPCREL(%rip), %rdx
	xorl	%edi, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L209:
	cmpq	$0, _nl_current_LC_MONETARY_used@GOTPCREL(%rip)
	je	.L210
	movq	_nl_current_LC_MONETARY@gottpoff(%rip), %rax
	movq	_nl_C_LC_MONETARY@GOTPCREL(%rip), %rdx
	movl	$4, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L210:
	cmpq	$0, _nl_current_LC_NUMERIC_used@GOTPCREL(%rip)
	je	.L211
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	_nl_C_LC_NUMERIC@GOTPCREL(%rip), %rdx
	movl	$1, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L211:
	cmpq	$0, _nl_current_LC_TIME_used@GOTPCREL(%rip)
	je	.L212
	movq	_nl_current_LC_TIME@gottpoff(%rip), %rax
	movq	_nl_C_LC_TIME@GOTPCREL(%rip), %rdx
	movl	$2, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L212:
	cmpq	$0, _nl_current_LC_MESSAGES_used@GOTPCREL(%rip)
	je	.L213
	movq	_nl_current_LC_MESSAGES@gottpoff(%rip), %rax
	movq	_nl_C_LC_MESSAGES@GOTPCREL(%rip), %rdx
	movl	$5, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L213:
	cmpq	$0, _nl_current_LC_PAPER_used@GOTPCREL(%rip)
	je	.L214
	movq	_nl_current_LC_PAPER@gottpoff(%rip), %rax
	movq	_nl_C_LC_PAPER@GOTPCREL(%rip), %rdx
	movl	$7, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L214:
	cmpq	$0, _nl_current_LC_NAME_used@GOTPCREL(%rip)
	je	.L215
	movq	_nl_current_LC_NAME@gottpoff(%rip), %rax
	movq	_nl_C_LC_NAME@GOTPCREL(%rip), %rdx
	movl	$8, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L215:
	cmpq	$0, _nl_current_LC_ADDRESS_used@GOTPCREL(%rip)
	je	.L216
	movq	_nl_current_LC_ADDRESS@gottpoff(%rip), %rax
	movq	_nl_C_LC_ADDRESS@GOTPCREL(%rip), %rdx
	movl	$9, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L216:
	cmpq	$0, _nl_current_LC_TELEPHONE_used@GOTPCREL(%rip)
	je	.L217
	movq	_nl_current_LC_TELEPHONE@gottpoff(%rip), %rax
	movq	_nl_C_LC_TELEPHONE@GOTPCREL(%rip), %rdx
	movl	$10, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L217:
	cmpq	$0, _nl_current_LC_MEASUREMENT_used@GOTPCREL(%rip)
	je	.L218
	movq	_nl_current_LC_MEASUREMENT@gottpoff(%rip), %rax
	movq	_nl_C_LC_MEASUREMENT@GOTPCREL(%rip), %rdx
	movl	$11, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L218:
	cmpq	$0, _nl_current_LC_IDENTIFICATION_used@GOTPCREL(%rip)
	je	.L219
	movq	_nl_current_LC_IDENTIFICATION@gottpoff(%rip), %rax
	movq	_nl_C_LC_IDENTIFICATION@GOTPCREL(%rip), %rdx
	movl	$12, %edi
	movq	%fs:(%rax), %rax
	movq	(%rax), %rsi
	call	free_category
.L219:
	movq	176+_nl_global_locale(%rip), %rdi
	leaq	_nl_C_name(%rip), %rbx
	cmpq	%rbx, %rdi
	je	.L220
	call	free@PLT
	movq	%rbx, 176+_nl_global_locale(%rip)
.L220:
	popq	%rbx
	jmp	_nl_archive_subfreeres
	.size	_nl_locale_subfreeres, .-_nl_locale_subfreeres
	.hidden	__libc_setlocale_lock
	.globl	__libc_setlocale_lock
	.bss
	.align 32
	.type	__libc_setlocale_lock, @object
	.size	__libc_setlocale_lock, 56
__libc_setlocale_lock:
	.zero	56
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	_nl_category_postload, @object
	.size	_nl_category_postload, 104
_nl_category_postload:
	.quad	_nl_postload_ctype
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.zero	8
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.hidden	_nl_category_name_sizes
	.globl	_nl_category_name_sizes
	.section	.rodata
	.align 8
	.type	_nl_category_name_sizes, @object
	.size	_nl_category_name_sizes, 13
_nl_category_name_sizes:
	.byte	8
	.byte	10
	.byte	7
	.byte	10
	.byte	11
	.byte	11
	.byte	6
	.byte	8
	.byte	7
	.byte	10
	.byte	12
	.byte	14
	.byte	17
	.hidden	_nl_category_name_idxs
	.globl	_nl_category_name_idxs
	.align 8
	.type	_nl_category_name_idxs, @object
	.size	_nl_category_name_idxs, 13
_nl_category_name_idxs:
	.byte	11
	.byte	32
	.byte	43
	.byte	0
	.byte	20
	.byte	51
	.zero	1
	.byte	63
	.byte	72
	.byte	80
	.byte	91
	.byte	104
	.byte	119
	.hidden	_nl_category_names
	.globl	_nl_category_names
	.align 32
	.type	_nl_category_names, @object
	.size	_nl_category_names, 137
_nl_category_names:
	.string	"LC_COLLATE"
	.string	"LC_CTYPE"
	.string	"LC_MONETARY"
	.string	"LC_NUMERIC"
	.string	"LC_TIME"
	.string	"LC_MESSAGES"
	.string	"LC_PAPER"
	.string	"LC_NAME"
	.string	"LC_ADDRESS"
	.string	"LC_TELEPHONE"
	.string	"LC_MEASUREMENT"
	.string	"LC_IDENTIFICATION"
	.section	.data.rel.ro
	.align 32
	.type	_nl_current_used, @object
	.size	_nl_current_used, 104
_nl_current_used:
	.quad	_nl_current_LC_CTYPE_used
	.quad	_nl_current_LC_NUMERIC_used
	.quad	_nl_current_LC_TIME_used
	.quad	_nl_current_LC_COLLATE_used
	.quad	_nl_current_LC_MONETARY_used
	.quad	_nl_current_LC_MESSAGES_used
	.zero	8
	.quad	_nl_current_LC_PAPER_used
	.quad	_nl_current_LC_NAME_used
	.quad	_nl_current_LC_ADDRESS_used
	.quad	_nl_current_LC_TELEPHONE_used
	.quad	_nl_current_LC_MEASUREMENT_used
	.quad	_nl_current_LC_IDENTIFICATION_used
	.weak	_nl_postload_ctype
	.weak	_nl_C_LC_IDENTIFICATION
	.weak	_nl_current_LC_IDENTIFICATION
	.weak	_nl_current_LC_IDENTIFICATION_used
	.weak	_nl_C_LC_MEASUREMENT
	.weak	_nl_current_LC_MEASUREMENT
	.weak	_nl_current_LC_MEASUREMENT_used
	.weak	_nl_C_LC_TELEPHONE
	.weak	_nl_current_LC_TELEPHONE
	.weak	_nl_current_LC_TELEPHONE_used
	.weak	_nl_C_LC_ADDRESS
	.weak	_nl_current_LC_ADDRESS
	.weak	_nl_current_LC_ADDRESS_used
	.weak	_nl_C_LC_NAME
	.weak	_nl_current_LC_NAME
	.weak	_nl_current_LC_NAME_used
	.weak	_nl_C_LC_PAPER
	.weak	_nl_current_LC_PAPER
	.weak	_nl_current_LC_PAPER_used
	.weak	_nl_C_LC_MESSAGES
	.weak	_nl_current_LC_MESSAGES
	.weak	_nl_current_LC_MESSAGES_used
	.weak	_nl_C_LC_TIME
	.weak	_nl_current_LC_TIME
	.weak	_nl_current_LC_TIME_used
	.weak	_nl_C_LC_NUMERIC
	.weak	_nl_current_LC_NUMERIC
	.weak	_nl_current_LC_NUMERIC_used
	.weak	_nl_C_LC_MONETARY
	.weak	_nl_current_LC_MONETARY
	.weak	_nl_current_LC_MONETARY_used
	.weak	_nl_C_LC_CTYPE
	.weak	_nl_current_LC_CTYPE
	.weak	_nl_current_LC_CTYPE_used
	.weak	_nl_C_LC_COLLATE
	.weak	_nl_current_LC_COLLATE
	.weak	_nl_current_LC_COLLATE_used
	.weak	__pthread_rwlock_unlock
	.weak	__pthread_rwlock_wrlock
	.hidden	_nl_archive_subfreeres
	.hidden	_nl_current_LC_IDENTIFICATION
	.hidden	_nl_current_LC_MEASUREMENT
	.hidden	_nl_current_LC_TELEPHONE
	.hidden	_nl_current_LC_ADDRESS
	.hidden	_nl_current_LC_NAME
	.hidden	_nl_current_LC_PAPER
	.hidden	_nl_current_LC_MESSAGES
	.hidden	_nl_current_LC_TIME
	.hidden	_nl_current_LC_NUMERIC
	.hidden	_nl_current_LC_MONETARY
	.hidden	_nl_current_LC_CTYPE
	.hidden	_nl_current_LC_COLLATE
	.hidden	_nl_unload_locale
	.hidden	_nl_locale_file_list
	.hidden	__argz_add_sep
	.hidden	_nl_default_locale_path
	.hidden	__argz_create_sep
	.hidden	__strdup
	.hidden	strchr
	.hidden	_nl_find_locale
	.hidden	getenv
	.hidden	_nl_POSIX_name
	.hidden	_nl_C_name
	.hidden	strcmp
	.hidden	strlen
	.hidden	_nl_global_locale
