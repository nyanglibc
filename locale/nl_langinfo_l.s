	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4,,15
	.globl	__nl_langinfo_l
	.hidden	__nl_langinfo_l
	.type	__nl_langinfo_l, @function
__nl_langinfo_l:
	movl	%edi, %eax
	sarl	$16, %eax
	cmpl	$6, %eax
	je	.L19
	cmpl	$12, %eax
	jbe	.L24
.L19:
	leaq	.LC0(%rip), %rax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L24:
	movzwl	%di, %edi
	cmpl	$65535, %edi
	je	.L25
	cmpl	$12, %eax
	ja	.L4
	leaq	.L6(%rip), %rdx
	movl	%eax, %eax
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L6:
	.long	.L5-.L6
	.long	.L7-.L6
	.long	.L8-.L6
	.long	.L9-.L6
	.long	.L10-.L6
	.long	.L11-.L6
	.long	.L4-.L6
	.long	.L12-.L6
	.long	.L13-.L6
	.long	.L14-.L6
	.long	.L15-.L6
	.long	.L16-.L6
	.long	.L17-.L6
	.text
	.p2align 4,,10
	.p2align 3
.L25:
	cltq
	movq	128(%rsi,%rax,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	_nl_current_LC_IDENTIFICATION@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	%edi, 56(%rdx)
	leaq	.LC0(%rip), %rax
	jbe	.L1
	movq	64(%rdx,%rdi,8), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L7:
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L8:
	movq	_nl_current_LC_TIME@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L9:
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L10:
	movq	_nl_current_LC_MONETARY@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L11:
	movq	_nl_current_LC_MESSAGES@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L12:
	movq	_nl_current_LC_PAPER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L13:
	movq	_nl_current_LC_NAME@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L14:
	movq	_nl_current_LC_ADDRESS@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L15:
	movq	_nl_current_LC_TELEPHONE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L16:
	movq	_nl_current_LC_MEASUREMENT@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %rdx
	jmp	.L18
.L4:
	subq	$8, %rsp
	call	abort
	.size	__nl_langinfo_l, .-__nl_langinfo_l
	.weak	nl_langinfo_l
	.set	nl_langinfo_l,__nl_langinfo_l
	.hidden	abort
	.hidden	_nl_current_LC_MEASUREMENT
	.hidden	_nl_current_LC_TELEPHONE
	.hidden	_nl_current_LC_ADDRESS
	.hidden	_nl_current_LC_NAME
	.hidden	_nl_current_LC_PAPER
	.hidden	_nl_current_LC_MESSAGES
	.hidden	_nl_current_LC_MONETARY
	.hidden	_nl_current_LC_COLLATE
	.hidden	_nl_current_LC_TIME
	.hidden	_nl_current_LC_NUMERIC
	.hidden	_nl_current_LC_CTYPE
	.hidden	_nl_current_LC_IDENTIFICATION
