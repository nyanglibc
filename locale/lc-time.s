	.text
#APP
	.globl _nl_current_LC_TIME_used
.set _nl_current_LC_TIME_used, 2
#NO_APP
	.hidden	_nl_current_LC_TIME
	.globl	_nl_current_LC_TIME
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_TIME, @object
	.size	_nl_current_LC_TIME, 8
_nl_current_LC_TIME:
	.quad	_nl_global_locale+16
	.hidden	_nl_global_locale
