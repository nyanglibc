	.text
	.p2align 4,,15
	.globl	__uselocale
	.hidden	__uselocale
	.type	__uselocale, @function
__uselocale:
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	testq	%rdi, %rdi
	leaq	_nl_global_locale(%rip), %rdx
	movq	%fs:(%rcx), %rax
	je	.L2
	cmpq	$-1, %rdi
	cmove	%rdx, %rdi
	cmpq	$0, _nl_current_LC_COLLATE_used@GOTPCREL(%rip)
	movq	%rdi, %fs:(%rcx)
	je	.L4
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rcx
	leaq	24(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L4:
	cmpq	$0, _nl_current_LC_CTYPE_used@GOTPCREL(%rip)
	je	.L5
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rcx
	movq	%rdi, %fs:(%rcx)
.L5:
	cmpq	$0, _nl_current_LC_MONETARY_used@GOTPCREL(%rip)
	je	.L6
	movq	_nl_current_LC_MONETARY@gottpoff(%rip), %rcx
	leaq	32(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L6:
	cmpq	$0, _nl_current_LC_NUMERIC_used@GOTPCREL(%rip)
	je	.L7
	movq	_nl_current_LC_NUMERIC@gottpoff(%rip), %rcx
	leaq	8(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L7:
	cmpq	$0, _nl_current_LC_TIME_used@GOTPCREL(%rip)
	je	.L8
	movq	_nl_current_LC_TIME@gottpoff(%rip), %rcx
	leaq	16(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L8:
	cmpq	$0, _nl_current_LC_MESSAGES_used@GOTPCREL(%rip)
	je	.L9
	movq	_nl_current_LC_MESSAGES@gottpoff(%rip), %rcx
	leaq	40(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L9:
	cmpq	$0, _nl_current_LC_PAPER_used@GOTPCREL(%rip)
	je	.L10
	movq	_nl_current_LC_PAPER@gottpoff(%rip), %rcx
	leaq	56(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L10:
	cmpq	$0, _nl_current_LC_NAME_used@GOTPCREL(%rip)
	je	.L11
	movq	_nl_current_LC_NAME@gottpoff(%rip), %rcx
	leaq	64(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L11:
	cmpq	$0, _nl_current_LC_ADDRESS_used@GOTPCREL(%rip)
	je	.L12
	movq	_nl_current_LC_ADDRESS@gottpoff(%rip), %rcx
	leaq	72(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L12:
	cmpq	$0, _nl_current_LC_TELEPHONE_used@GOTPCREL(%rip)
	je	.L13
	movq	_nl_current_LC_TELEPHONE@gottpoff(%rip), %rcx
	leaq	80(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L13:
	cmpq	$0, _nl_current_LC_MEASUREMENT_used@GOTPCREL(%rip)
	je	.L14
	movq	_nl_current_LC_MEASUREMENT@gottpoff(%rip), %rcx
	leaq	88(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L14:
	cmpq	$0, _nl_current_LC_IDENTIFICATION_used@GOTPCREL(%rip)
	je	.L15
	movq	_nl_current_LC_IDENTIFICATION@gottpoff(%rip), %rcx
	leaq	96(%rdi), %rsi
	movq	%rsi, %fs:(%rcx)
.L15:
	movq	104(%rdi), %rsi
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rcx
	movq	%rsi, %fs:(%rcx)
	movq	112(%rdi), %rsi
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rcx
	movq	%rsi, %fs:(%rcx)
	movq	120(%rdi), %rsi
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %rcx
	movq	%rsi, %fs:(%rcx)
.L2:
	cmpq	%rdx, %rax
	movq	$-1, %rdx
	cmove	%rdx, %rax
	ret
	.size	__uselocale, .-__uselocale
	.weak	uselocale
	.set	uselocale,__uselocale
	.weak	_nl_current_LC_IDENTIFICATION
	.weak	_nl_current_LC_IDENTIFICATION_used
	.weak	_nl_current_LC_MEASUREMENT
	.weak	_nl_current_LC_MEASUREMENT_used
	.weak	_nl_current_LC_TELEPHONE
	.weak	_nl_current_LC_TELEPHONE_used
	.weak	_nl_current_LC_ADDRESS
	.weak	_nl_current_LC_ADDRESS_used
	.weak	_nl_current_LC_NAME
	.weak	_nl_current_LC_NAME_used
	.weak	_nl_current_LC_PAPER
	.weak	_nl_current_LC_PAPER_used
	.weak	_nl_current_LC_MESSAGES
	.weak	_nl_current_LC_MESSAGES_used
	.weak	_nl_current_LC_TIME
	.weak	_nl_current_LC_TIME_used
	.weak	_nl_current_LC_NUMERIC
	.weak	_nl_current_LC_NUMERIC_used
	.weak	_nl_current_LC_MONETARY
	.weak	_nl_current_LC_MONETARY_used
	.weak	_nl_current_LC_CTYPE
	.weak	_nl_current_LC_CTYPE_used
	.weak	_nl_current_LC_COLLATE
	.weak	_nl_current_LC_COLLATE_used
	.hidden	_nl_current_LC_IDENTIFICATION
	.hidden	_nl_current_LC_MEASUREMENT
	.hidden	_nl_current_LC_TELEPHONE
	.hidden	_nl_current_LC_ADDRESS
	.hidden	_nl_current_LC_NAME
	.hidden	_nl_current_LC_PAPER
	.hidden	_nl_current_LC_MESSAGES
	.hidden	_nl_current_LC_TIME
	.hidden	_nl_current_LC_NUMERIC
	.hidden	_nl_current_LC_MONETARY
	.hidden	_nl_current_LC_CTYPE
	.hidden	_nl_current_LC_COLLATE
	.hidden	_nl_global_locale
