	.text
#APP
	.globl _nl_current_LC_COLLATE_used
.set _nl_current_LC_COLLATE_used, 2
#NO_APP
	.hidden	_nl_current_LC_COLLATE
	.globl	_nl_current_LC_COLLATE
	.section	.tdata,"awT",@progbits
	.align 8
	.type	_nl_current_LC_COLLATE, @object
	.size	_nl_current_LC_COLLATE, 8
_nl_current_LC_COLLATE:
	.quad	_nl_global_locale+24
	.hidden	_nl_global_locale
