	.text
	.p2align 4,,15
	.globl	__libc_alloc_buffer_allocate
	.hidden	__libc_alloc_buffer_allocate
	.type	__libc_alloc_buffer_allocate, @function
__libc_alloc_buffer_allocate:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	malloc@PLT
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L3
	movq	%rbx, %rdx
	addq	%rax, %rdx
	jc	.L10
.L3:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L10:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rdx, 8(%rsp)
	movq	%rax, (%rsp)
	call	__libc_alloc_buffer_create_failure
	movq	8(%rsp), %rdx
	movq	(%rsp), %rax
	jmp	.L3
	.size	__libc_alloc_buffer_allocate, .-__libc_alloc_buffer_allocate
	.hidden	__libc_alloc_buffer_create_failure
