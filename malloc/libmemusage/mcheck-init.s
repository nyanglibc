	.text
#APP
	.symver __malloc_initialize_hook,__malloc_initialize_hook@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	turn_on_mcheck, @function
turn_on_mcheck:
	xorl	%edi, %edi
	jmp	mcheck@PLT
	.size	turn_on_mcheck, .-turn_on_mcheck
	.globl	__malloc_initialize_hook
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__malloc_initialize_hook, @object
	.size	__malloc_initialize_hook, 8
__malloc_initialize_hook:
	.quad	turn_on_mcheck
