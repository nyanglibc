	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"memusage.c"
.LC1:
	.string	"idx < 2 * DEFAULT_BUFFER_SIZE"
	.text
	.p2align 4,,15
	.type	update_data, @function
update_data:
	testq	%rdi, %rdi
	pushq	%rbx
	je	.L2
	movl	$4276993711, %eax
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
.L2:
	subq	%rdx, %rsi
	movq	%rsi, %rbx
#APP
# 133 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddq %rbx, current_heap(%rip)
# 0 "" 2
#NO_APP
	leaq	peak_use(%rip), %rcx
	addq	%rsi, %rbx
.L4:
	movq	(%rcx), %rdx
	cmpq	%rdx, %rbx
	jbe	.L3
	movq	%rdx, %rax
#APP
# 134 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, peak_use(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rax, %rdx
	jne	.L4
.L3:
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT
	movq	start_sp@dtpoff(%rax), %rcx
	testq	%rcx, %rcx
	je	.L25
.L5:
	cmpq	%rsp, %rcx
	jb	.L6
	subq	%rsp, %rcx
	addq	%rcx, %rbx
.L7:
	movq	8+peak_use(%rip), %rdx
	cmpq	%rdx, %rcx
	jbe	.L10
	movq	%rdx, %rax
#APP
# 157 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rcx, 8+peak_use(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rax, %rdx
	jne	.L7
.L10:
	movq	16+peak_use(%rip), %rdx
	cmpq	%rdx, %rbx
	jbe	.L9
	movq	%rdx, %rax
#APP
# 160 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, 16+peak_use(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rax, %rdx
	jne	.L10
.L9:
	cmpl	$-1, fd(%rip)
	je	.L1
	movl	$1, %edi
#APP
# 165 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	xaddl %edi, buffer_cnt(%rip)
# 0 "" 2
#NO_APP
	movq	buffer_size(%rip), %r9
	leal	1(%rdi), %eax
	movq	%rax, %rsi
	leaq	(%r9,%r9), %r8
	cmpq	%r8, %rax
	jnb	.L26
.L13:
	cmpl	$65535, %edi
	ja	.L27
	movl	%edi, %eax
	leaq	buffer(%rip), %rsi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rsi,%rax,8), %r8
	movq	current_heap(%rip), %rax
	movq	%rcx, 8(%r8)
	movq	%rax, (%r8)
#APP
# 181 "memusage.c" 1
	rdtsc
# 0 "" 2
#NO_APP
	movl	%edx, 20(%r8)
	leal	1(%rdi), %edx
	movl	%eax, 16(%r8)
	cmpq	%r9, %rdx
	je	.L28
	leaq	(%r9,%r9), %rax
	cmpq	%rax, %rdx
	je	.L29
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	xorl	%edx, %edx
	divq	%r8
	movl	%esi, %eax
#APP
# 173 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgl %edx, buffer_cnt(%rip)
# 0 "" 2
#NO_APP
	movl	%edi, %esi
	leal	-1(%rdx), %eax
	cmpq	%r8, %rsi
	cmovnb	%eax, %edi
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L29:
	popq	%rbx
	addq	%r9, %rdx
	movl	fd(%rip), %edi
	salq	$3, %rdx
	addq	%rdx, %rsi
	jmp	write@PLT
	.p2align 4,,10
	.p2align 3
.L28:
	popq	%rbx
	leaq	(%r9,%r9,2), %rdx
	movl	fd(%rip), %edi
	salq	$3, %rdx
	jmp	write@PLT
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT
	xorl	%ecx, %ecx
	movq	%rsp, start_sp@dtpoff(%rax)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L25:
	movq	%rsp, %rcx
	movq	%rsp, start_sp@dtpoff(%rax)
	jmp	.L5
.L27:
	leaq	__PRETTY_FUNCTION__.10129(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$177, %edx
	call	__assert_fail@PLT
	.size	update_data, .-update_data
	.p2align 4,,15
	.type	int_handler, @function
int_handler:
	xorl	%edx, %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	update_data
	.size	int_handler, .-int_handler
	.section	.rodata.str1.1
.LC2:
	.string	"MEMUSAGE_PROG_NAME"
.LC3:
	.string	"malloc"
.LC4:
	.string	"realloc"
.LC5:
	.string	"calloc"
.LC6:
	.string	"free"
.LC7:
	.string	"mmap"
.LC8:
	.string	"mmap64"
.LC9:
	.string	"mremap"
.LC10:
	.string	"munmap"
.LC11:
	.string	"MEMUSAGE_OUTPUT"
.LC12:
	.string	"MEMUSAGE_BUFFER_SIZE"
.LC13:
	.string	"MEMUSAGE_NO_TIMER"
.LC14:
	.string	"MEMUSAGE_TRACE_MMAP"
	.text
	.p2align 4,,15
	.type	me, @function
me:
	pushq	%r14
	pushq	%r13
	leaq	.LC2(%rip), %rdi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$192, %rsp
	call	getenv@PLT
	movq	__progname@GOTPCREL(%rip), %rbp
	movq	%rax, %rbx
	movq	0(%rbp), %rdi
	call	strlen@PLT
	leaq	.LC3(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, %r12
	movl	$-1, initialized(%rip)
	call	dlsym@PLT
	leaq	.LC4(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, mallocp(%rip)
	call	dlsym@PLT
	leaq	.LC5(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, reallocp(%rip)
	call	dlsym@PLT
	leaq	.LC6(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, callocp(%rip)
	call	dlsym@PLT
	leaq	.LC7(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, freep(%rip)
	call	dlsym@PLT
	leaq	.LC8(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, mmapp(%rip)
	call	dlsym@PLT
	leaq	.LC9(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, mmap64p(%rip)
	call	dlsym@PLT
	leaq	.LC10(%rip), %rsi
	movq	$-1, %rdi
	movq	%rax, mremapp(%rip)
	call	dlsym@PLT
	testq	%rbx, %rbx
	movq	%rax, munmapp(%rip)
	movl	$1, initialized(%rip)
	je	.L32
	movq	%rbx, %rdi
	call	strlen@PLT
	cmpq	%rax, %r12
	movq	%rax, %r13
	jnb	.L60
.L33:
	movb	$1, not_me(%rip)
.L31:
	addq	$192, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	movq	0(%rbp), %r14
	movq	%r12, %rbp
	movq	%rbx, %rdi
	subq	%rax, %rbp
	leaq	(%r14,%rbp), %rsi
	call	strcmp@PLT
	testl	%eax, %eax
	jne	.L33
	cmpq	%r13, %r12
	je	.L32
	cmpb	$47, -1(%r14,%rbp)
	jne	.L33
	.p2align 4,,10
	.p2align 3
.L32:
	cmpb	$0, not_me(%rip)
	jne	.L31
	cmpl	$-1, fd(%rip)
	jne	.L31
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT
	cmpq	$0, start_sp@dtpoff(%rax)
	jne	.L37
	movq	%rsp, start_sp@dtpoff(%rax)
.L37:
	leaq	.LC11(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L39
	cmpb	$0, (%rax)
	jne	.L61
.L39:
	cmpb	$0, not_me(%rip)
	jne	.L31
	leaq	.LC14(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	je	.L31
	movb	$1, trace_mmap(%rip)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L61:
	movl	$6, %esi
	movq	%rax, %rdi
	call	access@PLT
	testl	%eax, %eax
	je	.L41
	call	__errno_location@PLT
	cmpl	$2, (%rax)
	jne	.L39
.L41:
	movq	%rbx, %rdi
	movl	$438, %esi
	call	creat64@PLT
	cmpl	$-1, %eax
	movl	%eax, %edi
	movl	%eax, fd(%rip)
	je	.L33
	leaq	first(%rip), %rsi
	movq	$0, first(%rip)
	movq	$0, 8+first(%rip)
#APP
# 271 "memusage.c" 1
	rdtsc
# 0 "" 2
#NO_APP
	movl	%edx, 20+first(%rip)
	movl	$24, %edx
	movl	%eax, 16+first(%rip)
	call	write@PLT
	movl	fd(%rip), %edi
	leaq	first(%rip), %rsi
	movl	$24, %edx
	call	write@PLT
	leaq	.LC12(%rip), %rdi
	movq	$32768, buffer_size(%rip)
	call	getenv@PLT
	testq	%rax, %rax
	je	.L43
	movl	$10, %edx
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	strtol@PLT
	cltq
	leaq	-1(%rax), %rdx
	cmpq	$32767, %rdx
	movl	$32768, %edx
	cmova	%rdx, %rax
	movq	%rax, buffer_size(%rip)
.L43:
	leaq	.LC13(%rip), %rdi
	call	getenv@PLT
	testq	%rax, %rax
	jne	.L39
	leaq	int_handler(%rip), %rax
	leaq	40(%rsp), %rdi
	leaq	32(%rsp), %rbx
	movl	$268435456, 168(%rsp)
	movq	%rax, 32(%rsp)
	call	sigfillset@PLT
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	$27, %edi
	call	sigaction@PLT
	testl	%eax, %eax
	js	.L39
	movq	$0, 16(%rsp)
	movq	$1, 24(%rsp)
	movq	%rsp, %rsi
	movdqa	16(%rsp), %xmm0
	xorl	%edx, %edx
	movl	$2, %edi
	movaps	%xmm0, (%rsp)
	call	setitimer@PLT
	jmp	.L39
	.size	me, .-me
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	init, @function
init:
	subq	$8, %rsp
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT
	movl	initialized(%rip), %ecx
	movq	%rsp, start_sp@dtpoff(%rax)
	testl	%ecx, %ecx
	je	.L65
	popq	%rax
	ret
.L65:
	popq	%rdx
	jmp	me
	.size	init, .-init
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	init
	.text
	.p2align 4,,15
	.globl	malloc
	.type	malloc, @function
malloc:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L75
.L67:
	cmpb	$0, not_me(%rip)
	je	.L69
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*mallocp(%rip)
	.p2align 4,,10
	.p2align 3
.L69:
#APP
# 347 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq calls(%rip)
# 0 "" 2
# 349 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, total(%rip)
# 0 "" 2
# 351 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)
# 0 "" 2
#NO_APP
	cmpq	$65535, %rbx
	ja	.L70
	movq	%rbx, %rdx
	leaq	histogram(%rip), %rax
	shrq	$4, %rdx
#APP
# 354 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rax,%rdx,8)
# 0 "" 2
#NO_APP
.L71:
#APP
# 358 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq calls_total(%rip)
# 0 "" 2
#NO_APP
	leaq	16(%rbx), %rdi
	call	*mallocp(%rip)
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L76
	movq	%rax, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	call	update_data
	leaq	16(%rbp), %rax
.L66:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	cmpl	$-1, %eax
	je	.L73
	call	me
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L70:
#APP
# 356 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq large(%rip)
# 0 "" 2
#NO_APP
	jmp	.L71
	.p2align 4,,10
	.p2align 3
.L76:
#APP
# 364 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq failed(%rip)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	jmp	.L66
.L73:
	xorl	%eax, %eax
	jmp	.L66
	.size	malloc, .-malloc
	.p2align 4,,15
	.globl	realloc
	.type	realloc, @function
realloc:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$24, %rsp
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L100
.L78:
	cmpb	$0, not_me(%rip)
	je	.L80
.L99:
	addq	$24, %rsp
	movq	%rbx, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	*reallocp(%rip)
	.p2align 4,,10
	.p2align 3
.L80:
	testq	%rdi, %rdi
	je	.L91
	movl	$4276993711, %eax
	cmpq	%rax, -8(%rdi)
	jne	.L99
	movq	-16(%rdi), %r12
	leaq	-16(%rdi), %r13
	.p2align 4,,10
	.p2align 3
.L81:
#APP
# 415 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 8+calls(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rbx, %r12
	jnb	.L83
	movq	%rbx, %rax
	subq	%r12, %rax
#APP
# 419 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rax, 8+total(%rip)
# 0 "" 2
# 421 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rax, grand_total(%rip)
# 0 "" 2
#NO_APP
.L83:
	testq	%rbx, %rbx
	jne	.L84
	testq	%rdi, %rdi
	jne	.L101
.L84:
	cmpq	$65535, %rbx
	ja	.L85
	movq	%rbx, %rdx
	leaq	histogram(%rip), %rax
	shrq	$4, %rdx
#APP
# 442 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rax,%rdx,8)
# 0 "" 2
#NO_APP
.L86:
#APP
# 446 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq calls_total(%rip)
# 0 "" 2
#NO_APP
	leaq	16(%rbx), %rsi
	movq	%r13, %rdi
	call	*reallocp(%rip)
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L102
	cmpq	%rax, %r13
	je	.L103
.L88:
	cmpq	%rbx, %r12
	jbe	.L89
#APP
# 461 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq decreasing(%rip)
# 0 "" 2
#NO_APP
.L89:
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	update_data
	leaq	16(%rbp), %rax
.L77:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	cmpl	$-1, %eax
	movq	%rdi, 8(%rsp)
	je	.L90
	call	me
	movq	8(%rsp), %rdi
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L85:
#APP
# 444 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq large(%rip)
# 0 "" 2
#NO_APP
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L103:
#APP
# 458 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq inplace(%rip)
# 0 "" 2
#NO_APP
	jmp	.L88
.L90:
	xorl	%eax, %eax
	jmp	.L77
.L101:
#APP
# 427 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq realloc_free(%rip)
# 0 "" 2
#NO_APP
	movq	0(%r13), %rax
#APP
# 429 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rax, 24+total(%rip)
# 0 "" 2
#NO_APP
	xorl	%esi, %esi
	movq	%r12, %rdx
	xorl	%edi, %edi
	call	update_data
	movq	%r13, %rdi
	call	*freep(%rip)
	xorl	%eax, %eax
	jmp	.L77
.L102:
#APP
# 452 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 8+failed(%rip)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
	jmp	.L77
	.size	realloc, .-realloc
	.p2align 4,,15
	.globl	calloc
	.type	calloc, @function
calloc:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L114
.L105:
	cmpb	$0, not_me(%rip)
	je	.L107
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	jmp	*callocp(%rip)
	.p2align 4,,10
	.p2align 3
.L107:
	movq	%rdi, %rbx
	imulq	%rsi, %rbx
#APP
# 493 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 16+calls(%rip)
# 0 "" 2
# 495 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, 16+total(%rip)
# 0 "" 2
# 497 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)
# 0 "" 2
#NO_APP
	cmpq	$65535, %rbx
	ja	.L108
	movq	%rbx, %rdx
	leaq	histogram(%rip), %rax
	shrq	$4, %rdx
#APP
# 500 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rax,%rdx,8)
# 0 "" 2
#NO_APP
.L109:
	addq	$1, calls_total(%rip)
	leaq	16(%rbx), %rdi
	call	*mallocp(%rip)
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L115
	movq	%rbx, %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	update_data
	addq	$24, %rsp
	leaq	16(%rbp), %rdi
	movq	%rbx, %rdx
	popq	%rbx
	popq	%rbp
	xorl	%esi, %esi
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L114:
	cmpl	$-1, %eax
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	je	.L104
	call	me
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L108:
#APP
# 502 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq large(%rip)
# 0 "" 2
#NO_APP
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L115:
#APP
# 510 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 16+failed(%rip)
# 0 "" 2
#NO_APP
.L104:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	calloc, .-calloc
	.p2align 4,,15
	.globl	free
	.type	free, @function
free:
	movl	initialized(%rip), %eax
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	jle	.L127
.L117:
	cmpb	$0, not_me(%rip)
	je	.L119
.L126:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	*freep(%rip)
	.p2align 4,,10
	.p2align 3
.L119:
	testq	%rbx, %rbx
	je	.L128
	movl	$4276993711, %eax
	cmpq	%rax, -8(%rbx)
	jne	.L126
#APP
# 562 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 24+calls(%rip)
# 0 "" 2
#NO_APP
	movq	-16(%rbx), %rax
#APP
# 564 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rax, 24+total(%rip)
# 0 "" 2
#NO_APP
	movq	-16(%rbx), %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	update_data
	leaq	-16(%rbx), %rdi
	popq	%rbx
	jmp	*freep(%rip)
	.p2align 4,,10
	.p2align 3
.L127:
	cmpl	$-1, %eax
	je	.L116
	call	me
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L128:
#APP
# 548 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 24+calls(%rip)
# 0 "" 2
#NO_APP
.L116:
	popq	%rbx
	ret
	.size	free, .-free
	.p2align 4,,15
	.globl	mmap
	.type	mmap, @function
mmap:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movl	%ecx, %ebp
	movq	%rsi, %rbx
	subq	$32, %rsp
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L140
.L130:
	movl	%ebp, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rsi
	call	*mmapp(%rip)
	cmpb	$0, not_me(%rip)
	jne	.L129
	cmpb	$0, trace_mmap(%rip)
	je	.L129
	andl	$32, %ebp
	movl	$6, %ecx
	jne	.L132
	xorl	%ecx, %ecx
	andl	$2, %r12d
	setne	%cl
	addl	$4, %ecx
.L132:
	leaq	calls(%rip), %rsi
	movslq	%ecx, %rdx
#APP
# 599 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rsi,%rdx,8)
# 0 "" 2
#NO_APP
	leaq	total(%rip), %rsi
#APP
# 601 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, (%rsi,%rdx,8)
# 0 "" 2
# 603 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)
# 0 "" 2
#NO_APP
	cmpq	$65535, %rbx
	jbe	.L141
#APP
# 608 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq large(%rip)
# 0 "" 2
#NO_APP
.L134:
#APP
# 610 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq calls_total(%rip)
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L142
	cmpl	$5, %ecx
	je	.L143
.L129:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	movq	%rbx, %rdi
	leaq	histogram(%rip), %rsi
	shrq	$4, %rdi
#APP
# 606 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rsi,%rdi,8)
# 0 "" 2
#NO_APP
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L140:
	cmpl	$-1, %eax
	movq	%r9, 24(%rsp)
	movl	%r8d, 20(%rsp)
	movq	%rdi, 8(%rsp)
	je	.L136
	call	me
	movq	24(%rsp), %r9
	movl	20(%rsp), %r8d
	movq	8(%rsp), %rdi
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	failed(%rip), %rcx
#APP
# 614 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rcx,%rdx,8)
# 0 "" 2
#NO_APP
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	%rax, 8(%rsp)
	call	update_data
	movq	8(%rsp), %rax
	jmp	.L129
.L136:
	xorl	%eax, %eax
	jmp	.L129
	.size	mmap, .-mmap
	.p2align 4,,15
	.globl	mmap64
	.type	mmap64, @function
mmap64:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movl	%ecx, %ebp
	movq	%rsi, %rbx
	subq	$32, %rsp
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L155
.L145:
	movl	%ebp, %ecx
	movl	%r12d, %edx
	movq	%rbx, %rsi
	call	*mmap64p(%rip)
	cmpb	$0, not_me(%rip)
	jne	.L144
	cmpb	$0, trace_mmap(%rip)
	je	.L144
	andl	$32, %ebp
	movl	$6, %ecx
	jne	.L147
	xorl	%ecx, %ecx
	andl	$2, %r12d
	setne	%cl
	addl	$4, %ecx
.L147:
	leaq	calls(%rip), %rsi
	movslq	%ecx, %rdx
#APP
# 652 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rsi,%rdx,8)
# 0 "" 2
#NO_APP
	leaq	total(%rip), %rsi
#APP
# 654 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, (%rsi,%rdx,8)
# 0 "" 2
# 656 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)
# 0 "" 2
#NO_APP
	cmpq	$65535, %rbx
	jbe	.L156
#APP
# 661 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq large(%rip)
# 0 "" 2
#NO_APP
.L149:
#APP
# 663 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq calls_total(%rip)
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L157
	cmpl	$5, %ecx
	je	.L158
.L144:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%rbx, %rdi
	leaq	histogram(%rip), %rsi
	shrq	$4, %rdi
#APP
# 659 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rsi,%rdi,8)
# 0 "" 2
#NO_APP
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L155:
	cmpl	$-1, %eax
	movq	%r9, 24(%rsp)
	movl	%r8d, 20(%rsp)
	movq	%rdi, 8(%rsp)
	je	.L151
	call	me
	movq	24(%rsp), %r9
	movl	20(%rsp), %r8d
	movq	8(%rsp), %rdi
	jmp	.L145
	.p2align 4,,10
	.p2align 3
.L157:
	leaq	failed(%rip), %rcx
#APP
# 667 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rcx,%rdx,8)
# 0 "" 2
#NO_APP
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L158:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	%rax, 8(%rsp)
	call	update_data
	movq	8(%rsp), %rax
	jmp	.L144
.L151:
	xorl	%eax, %eax
	jmp	.L144
	.size	mmap64, .-mmap64
	.p2align 4,,15
	.globl	mremap
	.type	mremap, @function
mremap:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	subq	$96, %rsp
	leaq	128(%rsp), %rax
	movq	%r8, 80(%rsp)
	xorl	%r8d, %r8d
	testb	$2, %cl
	movl	$32, 24(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	je	.L160
	movq	32(%rax), %r8
	.p2align 4,,10
	.p2align 3
.L160:
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L176
.L163:
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	*mremapp(%rip)
	cmpb	$0, not_me(%rip)
	jne	.L159
	cmpb	$0, trace_mmap(%rip)
	je	.L159
#APP
# 707 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 56+calls(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rbx, %rbp
	jnb	.L165
	movq	%rbx, %rdx
	subq	%rbp, %rdx
#APP
# 711 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rdx, 56+total(%rip)
# 0 "" 2
# 713 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rdx, grand_total(%rip)
# 0 "" 2
#NO_APP
.L165:
	cmpq	$65535, %rbx
	jbe	.L177
#APP
# 719 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq large(%rip)
# 0 "" 2
#NO_APP
.L167:
#APP
# 721 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq calls_total(%rip)
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L178
	cmpq	%rax, %r12
	je	.L179
.L169:
	cmpq	%rbx, %rbp
	jbe	.L170
#APP
# 733 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq decreasing_mremap(%rip)
# 0 "" 2
#NO_APP
.L170:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	xorl	%edi, %edi
	movq	%rax, (%rsp)
	call	update_data
	movq	(%rsp), %rax
.L159:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	movq	%rbx, %rcx
	leaq	histogram(%rip), %rdx
	shrq	$4, %rcx
#APP
# 717 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq (%rdx,%rcx,8)
# 0 "" 2
#NO_APP
	jmp	.L167
	.p2align 4,,10
	.p2align 3
.L176:
	cmpl	$-1, %eax
	movl	%ecx, 12(%rsp)
	movq	%r8, (%rsp)
	je	.L172
	call	me
	movl	12(%rsp), %ecx
	movq	(%rsp), %r8
	jmp	.L163
	.p2align 4,,10
	.p2align 3
.L178:
#APP
# 725 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 56+failed(%rip)
# 0 "" 2
#NO_APP
	jmp	.L159
	.p2align 4,,10
	.p2align 3
.L179:
#APP
# 730 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq inplace_mremap(%rip)
# 0 "" 2
#NO_APP
	jmp	.L169
.L172:
	xorl	%eax, %eax
	jmp	.L159
	.size	mremap, .-mremap
	.p2align 4,,15
	.globl	munmap
	.type	munmap, @function
munmap:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movl	initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L188
.L181:
	movq	%rbx, %rsi
	call	*munmapp(%rip)
	cmpb	$0, not_me(%rip)
	jne	.L180
	cmpb	$0, trace_mmap(%rip)
	je	.L180
#APP
# 768 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 64+calls(%rip)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L183
	movl	%eax, 8(%rsp)
#APP
# 773 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	addq %rbx, 64+total(%rip)
# 0 "" 2
#NO_APP
	xorl	%esi, %esi
	movq	%rbx, %rdx
	xorl	%edi, %edi
	call	update_data
	movl	8(%rsp), %eax
.L180:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	cmpl	$-1, %eax
	movq	%rdi, 8(%rsp)
	je	.L180
	call	me
	movq	8(%rsp), %rdi
	jmp	.L181
	.p2align 4,,10
	.p2align 3
.L183:
#APP
# 780 "memusage.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	incq 64+failed(%rip)
# 0 "" 2
#NO_APP
	jmp	.L180
	.size	munmap, .-munmap
	.section	.rodata.str1.1
.LC15:
	.string	"\033[01;41m"
.LC16:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.ascii	"\n\033[01;32mMemory usage summary:\033[0;0m heap total: %llu"
	.ascii	", heap peak: %lu, stack peak: %lu\n\033[04;34m         total"
	.ascii	" calls   total m"
	.string	"emory   failed calls\033[0m\n\033[00;34m malloc|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34mrealloc|\033[0m %10lu   %12llu   %s%12lu\033[00;00m  (nomove:%ld, dec:%ld, free:%ld)\n\033[00;34m calloc|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34m   free|\033[0m %10lu   %12llu\n"
	.align 8
.LC18:
	.ascii	"\033[00;34mmmap(r)|\033[0m %10lu   %12llu   "
	.string	"%s%12lu\033[00;00m\n\033[00;34mmmap(w)|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34mmmap(a)|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34m mremap|\033[0m %10lu   %12llu   %s%12lu\033[00;00m  (nomove: %ld, dec:%ld)\n\033[00;34m munmap|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n"
	.align 8
.LC19:
	.string	"\033[01;32mHistogram for block sizes:\033[0;0m\n"
	.section	.rodata.str1.1
.LC20:
	.string	"%5d-%-5d%12lu "
.LC21:
	.string	" <1% \033[41;37m"
.LC22:
	.string	"%3d%% \033[41;37m"
.LC23:
	.string	"\033[0;0m\n"
.LC24:
	.string	"   large   %12lu "
	.section	.text.exit,"ax",@progbits
	.p2align 4,,15
	.type	dest, @function
dest:
	cmpb	$0, not_me(%rip)
	jne	.L237
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	fd(%rip), %edi
	movb	$1, not_me(%rip)
	cmpl	$-1, %edi
	je	.L191
	movl	buffer_cnt(%rip), %eax
	movq	buffer_size(%rip), %rcx
	cmpq	%rcx, %rax
	ja	.L240
	imulq	$24, %rax, %rdx
	leaq	buffer(%rip), %rsi
	call	write@PLT
.L193:
	movl	fd(%rip), %edi
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	lseek@PLT
	movq	16+peak_use(%rip), %rax
	movl	fd(%rip), %edi
	leaq	first(%rip), %rsi
	movl	$24, %edx
	movq	%rax, 8+first(%rip)
	call	write@PLT
	movq	peak_use(%rip), %rax
	movl	fd(%rip), %edi
	leaq	first(%rip), %rsi
	movq	%rax, first(%rip)
	movq	8+peak_use(%rip), %rax
	movq	%rax, 8+first(%rip)
#APP
# 822 "memusage.c" 1
	rdtsc
# 0 "" 2
#NO_APP
	movl	%edx, 20+first(%rip)
	movl	$24, %edx
	movl	%eax, 16+first(%rip)
	call	write@PLT
	movl	fd(%rip), %edi
	call	close@PLT
	movl	$-1, fd(%rip)
.L191:
	movq	16+failed(%rip), %rcx
	movq	8+failed(%rip), %rdx
	leaq	.LC16(%rip), %rbx
	movq	failed(%rip), %rax
	leaq	.LC15(%rip), %rbp
	movq	stderr@GOTPCREL(%rip), %r13
	movq	%rbx, %r8
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	testq	%rcx, %rcx
	pushq	24+total(%rip)
	movq	calls(%rip), %r9
	cmovne	%rbp, %r8
	testq	%rdx, %rdx
	pushq	24+calls(%rip)
	cmovne	%rbp, %rdi
	testq	%rax, %rax
	pushq	%rcx
	cmovne	%rbp, %rsi
	pushq	%r8
	movq	8+peak_use(%rip), %r8
	pushq	16+total(%rip)
	pushq	16+calls(%rip)
	pushq	realloc_free(%rip)
	pushq	decreasing(%rip)
	pushq	inplace(%rip)
	pushq	%rdx
	pushq	%rdi
	pushq	8+total(%rip)
	pushq	8+calls(%rip)
	pushq	%rax
	xorl	%eax, %eax
	pushq	%rsi
	pushq	total(%rip)
	leaq	.LC17(%rip), %rsi
	movq	peak_use(%rip), %rcx
	movq	grand_total(%rip), %rdx
	movq	0(%r13), %rdi
	call	fprintf@PLT
	subq	$-128, %rsp
	cmpb	$0, trace_mmap(%rip)
	jne	.L241
.L197:
	movq	0(%r13), %rcx
	leaq	.LC19(%rip), %rdi
	movl	$41, %edx
	movl	$1, %esi
	leaq	histogram(%rip), %r12
	call	fwrite@PLT
	movq	large(%rip), %rbp
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L203:
	movl	%eax, %edx
	sarl	$4, %edx
	movslq	%edx, %rdx
	movq	(%r12,%rdx,8), %rdx
	cmpq	%rdx, %rbp
	cmovb	%rdx, %rbp
	addl	$16, %eax
	cmpl	$65536, %eax
	jne	.L203
	xorl	%ebx, %ebx
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L204:
	addl	$16, %ebx
	cmpl	$65536, %ebx
	je	.L242
.L209:
	movl	%ebx, %r9d
	sarl	$4, %r9d
	movslq	%r9d, %r15
	movq	(%r12,%r15,8), %r8
	testq	%r8, %r8
	je	.L204
	leaq	(%r8,%r8,4), %rax
	xorl	%edx, %edx
	movq	0(%r13), %rdi
	leal	15(%rbx), %ecx
	leaq	.LC20(%rip), %rsi
	leaq	(%rax,%rax,4), %rax
	salq	$2, %rax
	divq	calls_total(%rip)
	movl	%ebx, %edx
	movq	%rax, %r14
	xorl	%eax, %eax
	call	fprintf@PLT
	testl	%r14d, %r14d
	jne	.L205
	movq	0(%r13), %rcx
	leaq	.LC21(%rip), %rdi
	movl	$13, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L206:
	movq	(%r12,%r15,8), %rax
	xorl	%edx, %edx
	leaq	(%rax,%rax,4), %rax
	leaq	(%rax,%rax,4), %rax
	addq	%rax, %rax
	divq	%rbp
	testl	%eax, %eax
	leal	-1(%rax), %r14d
	jle	.L207
	.p2align 4,,10
	.p2align 3
.L208:
	movq	0(%r13), %rsi
	movl	$61, %edi
	subl	$1, %r14d
	call	fputc@PLT
	cmpl	$-1, %r14d
	jne	.L208
.L207:
	movq	0(%r13), %rcx
	leaq	.LC23(%rip), %rdi
	movl	$7, %edx
	movl	$1, %esi
	addl	$16, %ebx
	call	fwrite@PLT
	cmpl	$65536, %ebx
	jne	.L209
.L242:
	movq	large(%rip), %rcx
	testq	%rcx, %rcx
	jne	.L243
.L210:
	movb	$0, not_me(%rip)
	popq	%rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	movq	0(%r13), %rdi
	leaq	.LC22(%rip), %rsi
	movl	%r14d, %edx
	xorl	%eax, %eax
	call	fprintf@PLT
	jmp	.L206
.L241:
	movq	64+failed(%rip), %rsi
	movq	56+failed(%rip), %rcx
	movq	%rbx, %r11
	movq	48+failed(%rip), %rdx
	movq	40+failed(%rip), %rax
	movq	%rbx, %r10
	movq	32+failed(%rip), %r9
	movq	%rbx, %r8
	movq	%rbx, %rdi
	testq	%rsi, %rsi
	pushq	%rsi
	leaq	.LC18(%rip), %rsi
	cmovne	%rbp, %r11
	testq	%rcx, %rcx
	cmovne	%rbp, %r10
	testq	%rdx, %rdx
	pushq	%r11
	cmovne	%rbp, %r8
	testq	%rax, %rax
	pushq	64+total(%rip)
	cmovne	%rbp, %rdi
	pushq	64+calls(%rip)
	testq	%r9, %r9
	pushq	decreasing_mremap(%rip)
	pushq	inplace_mremap(%rip)
	cmovne	%rbp, %rbx
	pushq	%rcx
	pushq	%r10
	pushq	56+total(%rip)
	pushq	56+calls(%rip)
	pushq	%rdx
	pushq	%r8
	movq	%rbx, %r8
	pushq	48+total(%rip)
	pushq	48+calls(%rip)
	pushq	%rax
	pushq	%rdi
	xorl	%eax, %eax
	pushq	40+total(%rip)
	pushq	40+calls(%rip)
	movq	32+total(%rip), %rcx
	movq	32+calls(%rip), %rdx
	movq	0(%r13), %rdi
	call	fprintf@PLT
	addq	$144, %rsp
	jmp	.L197
.L237:
	ret
.L240:
	subq	%rcx, %rax
	leaq	buffer(%rip), %rsi
	imulq	$24, %rcx, %rcx
	imulq	$24, %rax, %rdx
	addq	%rcx, %rsi
	call	write@PLT
	jmp	.L193
.L243:
	imulq	$100, %rcx, %rax
	xorl	%edx, %edx
	movq	0(%r13), %rdi
	leaq	.LC24(%rip), %rsi
	divq	calls_total(%rip)
	movq	%rcx, %rdx
	movq	%rax, %rbx
	xorl	%eax, %eax
	call	fprintf@PLT
	testl	%ebx, %ebx
	jne	.L211
	movq	0(%r13), %rcx
	leaq	.LC21(%rip), %rdi
	movl	$13, %edx
	movl	$1, %esi
	call	fwrite@PLT
.L212:
	imulq	$50, large(%rip), %rax
	xorl	%edx, %edx
	divq	%rbp
	testl	%eax, %eax
	leal	-1(%rax), %ebx
	jle	.L213
	.p2align 4,,10
	.p2align 3
.L214:
	movq	0(%r13), %rsi
	movl	$61, %edi
	subl	$1, %ebx
	call	fputc@PLT
	cmpl	$-1, %ebx
	jne	.L214
.L213:
	movq	0(%r13), %rcx
	leaq	.LC23(%rip), %rdi
	movl	$7, %edx
	movl	$1, %esi
	call	fwrite@PLT
	jmp	.L210
.L211:
	movq	0(%r13), %rdi
	leaq	.LC22(%rip), %rsi
	movl	%ebx, %edx
	xorl	%eax, %eax
	call	fprintf@PLT
	jmp	.L212
	.size	dest, .-dest
	.section	.dtors,"aw",@progbits
	.align 8
	.quad	dest
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10129, @object
	.size	__PRETTY_FUNCTION__.10129, 12
__PRETTY_FUNCTION__.10129:
	.string	"update_data"
	.local	first
	.comm	first,24,16
	.local	buffer_cnt
	.comm	buffer_cnt,4,4
	.local	buffer
	.comm	buffer,1572864,32
	.local	trace_mmap
	.comm	trace_mmap,1,1
	.local	initialized
	.comm	initialized,4,4
	.local	not_me
	.comm	not_me,1,1
	.data
	.align 4
	.type	fd, @object
	.size	fd, 4
fd:
	.long	-1
	.local	buffer_size
	.comm	buffer_size,8,8
	.section	.tbss,"awT",@nobits
	.align 8
	.type	start_sp, @object
	.size	start_sp, 8
start_sp:
	.zero	8
	.local	peak_use
	.comm	peak_use,24,16
	.local	current_heap
	.comm	current_heap,8,8
	.local	decreasing_mremap
	.comm	decreasing_mremap,8,8
	.local	inplace_mremap
	.comm	inplace_mremap,8,8
	.local	realloc_free
	.comm	realloc_free,8,8
	.local	decreasing
	.comm	decreasing,8,8
	.local	inplace
	.comm	inplace,8,8
	.local	calls_total
	.comm	calls_total,8,8
	.local	large
	.comm	large,8,8
	.local	histogram
	.comm	histogram,32768,32
	.local	grand_total
	.comm	grand_total,8,8
	.local	total
	.comm	total,72,32
	.local	failed
	.comm	failed,72,32
	.local	calls
	.comm	calls,72,32
	.local	mremapp
	.comm	mremapp,8,8
	.local	munmapp
	.comm	munmapp,8,8
	.local	mmap64p
	.comm	mmap64p,8,8
	.local	mmapp
	.comm	mmapp,8,8
	.local	freep
	.comm	freep,8,8
	.local	callocp
	.comm	callocp,8,8
	.local	reallocp
	.comm	reallocp,8,8
	.local	mallocp
	.comm	mallocp,8,8
