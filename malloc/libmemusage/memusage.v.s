	.file	"memusage.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/malloc
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/malloc/libmemusage/memusage.v.d
# -MF /run/asm/malloc/libmemusage/memusage.os.dt -MP
# -MT /run/asm/malloc/libmemusage/memusage.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libmemusage -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h memusage.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/malloc/libmemusage/memusage.v.s -O2
# -Wall -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fgnu89-inline -fmerge-all-constants
# -frounding-math -fno-stack-protector -fmath-errno -fPIC -fverbose-asm
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"memusage.c"
.LC1:
	.string	"idx < 2 * DEFAULT_BUFFER_SIZE"
	.text
	.p2align 4,,15
	.type	update_data, @function
update_data:
.LFB66:
	.cfi_startproc
# memusage.c:123:   if (result != NULL)
	testq	%rdi, %rdi	# result
# memusage.c:122: {
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
# memusage.c:123:   if (result != NULL)
	je	.L2	#,
# memusage.c:128:       result->magic = MAGIC;
	movl	$4276993711, %eax	#, tmp212
# memusage.c:127:       result->length = len;
	movq	%rsi, (%rdi)	# len, result_58(D)->length
# memusage.c:128:       result->magic = MAGIC;
	movq	%rax, 8(%rdi)	# tmp212, result_58(D)->magic
.L2:
# memusage.c:133:     = catomic_exchange_and_add (&current_heap, len - old_len) + len - old_len;
	subq	%rdx, %rsi	# old_len, __addval
	movq	%rsi, %rbx	# __addval, __result
#APP
# 133 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	xaddq %rbx, current_heap(%rip)	# __result, current_heap
# 0 "" 2
#NO_APP
	leaq	peak_use(%rip), %rcx	#, tmp209
# memusage.c:132:   memusage_size_t heap
	addq	%rsi, %rbx	# __addval, heap
.L4:
# memusage.c:134:   catomic_max (&peak_heap, heap);
	movq	(%rcx), %rdx	# MEM[(uatomic64_t *)&peak_use], __atg9_oldv
	cmpq	%rdx, %rbx	# __atg9_oldv, heap
	jbe	.L3	#,
# memusage.c:134:   catomic_max (&peak_heap, heap);
	movq	%rdx, %rax	# __atg9_oldv, ret
#APP
# 134 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	cmpxchgq %rbx, peak_use(%rip)	# heap, MEM[(uatomic64_t *)&peak_use]
# 0 "" 2
#NO_APP
	cmpq	%rax, %rdx	# ret, __atg9_oldv
	jne	.L4	#,
.L3:
# memusage.c:140:   if (__glibc_unlikely (!start_sp))
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT	#
	movq	start_sp@dtpoff(%rax), %rcx	# start_sp, prephitmp_5
	testq	%rcx, %rcx	# prephitmp_5
	je	.L25	#,
.L5:
# memusage.c:153:   if (__glibc_unlikely (sp > start_sp))
	cmpq	%rsp, %rcx	#, prephitmp_5
	jb	.L6	#,
	subq	%rsp, %rcx	#, prephitmp_10
	addq	%rcx, %rbx	# prephitmp_10, heap
.L7:
# memusage.c:157:   catomic_max (&peak_stack, current_stack);
	movq	8+peak_use(%rip), %rdx	# MEM[(uatomic64_t *)&peak_use + 8B], __atg9_oldv
	cmpq	%rdx, %rcx	# __atg9_oldv, prephitmp_10
	jbe	.L10	#,
# memusage.c:157:   catomic_max (&peak_stack, current_stack);
	movq	%rdx, %rax	# __atg9_oldv, ret
#APP
# 157 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	cmpxchgq %rcx, 8+peak_use(%rip)	# prephitmp_10, MEM[(uatomic64_t *)&peak_use + 8B]
# 0 "" 2
#NO_APP
	cmpq	%rax, %rdx	# ret, __atg9_oldv
	jne	.L7	#,
.L10:
# memusage.c:160:   catomic_max (&peak_total, heap + current_stack);
	movq	16+peak_use(%rip), %rdx	# MEM[(uatomic64_t *)&peak_use + 16B], __atg9_oldv
	cmpq	%rdx, %rbx	# __atg9_oldv, heap
	jbe	.L9	#,
# memusage.c:160:   catomic_max (&peak_total, heap + current_stack);
	movq	%rdx, %rax	# __atg9_oldv, ret
#APP
# 160 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	cmpxchgq %rbx, 16+peak_use(%rip)	# heap, MEM[(uatomic64_t *)&peak_use + 16B]
# 0 "" 2
#NO_APP
	cmpq	%rax, %rdx	# ret, __atg9_oldv
	jne	.L10	#,
.L9:
# memusage.c:163:   if (fd != -1)
	cmpl	$-1, fd(%rip)	#, fd
	je	.L1	#,
# memusage.c:165:       uatomic32_t idx = catomic_exchange_and_add (&buffer_cnt, 1);
	movl	$1, %edi	#, __result
#APP
# 165 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	xaddl %edi, buffer_cnt(%rip)	# __result, buffer_cnt
# 0 "" 2
# memusage.c:166:       if (idx + 1 >= 2 * buffer_size)
#NO_APP
	movq	buffer_size(%rip), %r9	# buffer_size, buffer_size.27_45
	leal	1(%rdi), %eax	#,
	movq	%rax, %rsi	#,
	leaq	(%r9,%r9), %r8	#, _32
	cmpq	%r8, %rax	# _32, _30
	jnb	.L26	#,
.L13:
# memusage.c:177:       assert (idx < 2 * DEFAULT_BUFFER_SIZE);
	cmpl	$65535, %edi	#, __result
	ja	.L27	#,
# memusage.c:179:       buffer[idx].heap = current_heap;
	movl	%edi, %eax	# __result, __result
	leaq	buffer(%rip), %rsi	#, tmp159
	leaq	(%rax,%rax,2), %rax	#, tmp164
	leaq	(%rsi,%rax,8), %r8	#, tmp166
	movq	current_heap(%rip), %rax	# current_heap, current_heap
# memusage.c:180:       buffer[idx].stack = current_stack;
	movq	%rcx, 8(%r8)	# prephitmp_10, buffer[idx_36].stack
# memusage.c:179:       buffer[idx].heap = current_heap;
	movq	%rax, (%r8)	# current_heap, buffer[idx_36].heap
# memusage.c:181:       GETTIME (buffer[idx].time_low, buffer[idx].time_high);
#APP
# 181 "memusage.c" 1
	rdtsc
# 0 "" 2
#NO_APP
	movl	%edx, 20(%r8)	# tmp185, buffer[idx_36].time_high
# memusage.c:184:       if (idx + 1 == buffer_size)
	leal	1(%rdi), %edx	#, _44
# memusage.c:181:       GETTIME (buffer[idx].time_low, buffer[idx].time_high);
	movl	%eax, 16(%r8)	# tmp176, buffer[idx_36].time_low
# memusage.c:184:       if (idx + 1 == buffer_size)
	cmpq	%r9, %rdx	# buffer_size.27_45, _44
	je	.L28	#,
# memusage.c:186:       else if (idx + 1 == 2 * buffer_size)
	leaq	(%r9,%r9), %rax	#, tmp200
	cmpq	%rax, %rdx	# tmp200, _44
	je	.L29	#,
.L1:
# memusage.c:189: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	.cfi_restore_state
# memusage.c:172:           uatomic32_t reset = (idx + 1) % (2 * buffer_size);
	xorl	%edx, %edx	# tmp153
	divq	%r8	# _32
# memusage.c:173:           catomic_compare_and_exchange_val_acq (&buffer_cnt, reset, idx + 1);
	movl	%esi, %eax	# ret, ret
#APP
# 173 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	cmpxchgl %edx, buffer_cnt(%rip)	# tmp153, buffer_cnt
# 0 "" 2
# memusage.c:174:           if (idx >= 2 * buffer_size)
#NO_APP
	movl	%edi, %esi	# __result, __result
# memusage.c:175:             idx = reset - 1;
	leal	-1(%rdx), %eax	#, tmp210
	cmpq	%r8, %rsi	# _32, __result
	cmovnb	%eax, %edi	# tmp210,, __result
	jmp	.L13	#
	.p2align 4,,10
	.p2align 3
.L29:
# memusage.c:189: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
# memusage.c:187:         write (fd, &buffer[buffer_size], buffer_size * sizeof (struct entry));
	addq	%r9, %rdx	# buffer_size.27_45, tmp203
	movl	fd(%rip), %edi	# fd,
	salq	$3, %rdx	#, tmp204
	addq	%rdx, %rsi	# tmp204, tmp205
	jmp	write@PLT	#
	.p2align 4,,10
	.p2align 3
.L28:
	.cfi_restore_state
# memusage.c:189: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
# memusage.c:185:         write (fd, buffer, buffer_size * sizeof (struct entry));
	leaq	(%r9,%r9,2), %rdx	#, tmp197
	movl	fd(%rip), %edi	# fd,
	salq	$3, %rdx	#, tmp198
	jmp	write@PLT	#
	.p2align 4,,10
	.p2align 3
.L6:
	.cfi_restore_state
# memusage.c:154:     start_sp = sp;
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT	#
	xorl	%ecx, %ecx	# prephitmp_10
	movq	%rsp, start_sp@dtpoff(%rax)	#, start_sp
	jmp	.L10	#
	.p2align 4,,10
	.p2align 3
.L25:
# memusage.c:141:     start_sp = GETSP ();
	movq	%rsp, %rcx	# stack_ptr, prephitmp_5
	movq	%rsp, start_sp@dtpoff(%rax)	#, start_sp
	jmp	.L5	#
.L27:
# memusage.c:177:       assert (idx < 2 * DEFAULT_BUFFER_SIZE);
	leaq	__PRETTY_FUNCTION__.10129(%rip), %rcx	#,
	leaq	.LC0(%rip), %rsi	#,
	leaq	.LC1(%rip), %rdi	#,
	movl	$177, %edx	#,
	call	__assert_fail@PLT	#
	.cfi_endproc
.LFE66:
	.size	update_data, .-update_data
	.p2align 4,,15
	.type	int_handler, @function
int_handler:
.LFB67:
	.cfi_startproc
# memusage.c:197:   update_data (NULL, 0, 0);
	xorl	%edx, %edx	#
	xorl	%esi, %esi	#
	xorl	%edi, %edi	#
	jmp	update_data	#
	.cfi_endproc
.LFE67:
	.size	int_handler, .-int_handler
	.section	.rodata.str1.1
.LC2:
	.string	"MEMUSAGE_PROG_NAME"
.LC3:
	.string	"malloc"
.LC4:
	.string	"realloc"
.LC5:
	.string	"calloc"
.LC6:
	.string	"free"
.LC7:
	.string	"mmap"
.LC8:
	.string	"mmap64"
.LC9:
	.string	"mremap"
.LC10:
	.string	"munmap"
.LC11:
	.string	"MEMUSAGE_OUTPUT"
.LC12:
	.string	"MEMUSAGE_BUFFER_SIZE"
.LC13:
	.string	"MEMUSAGE_NO_TIMER"
.LC14:
	.string	"MEMUSAGE_TRACE_MMAP"
	.text
	.p2align 4,,15
	.type	me, @function
me:
.LFB68:
	.cfi_startproc
	pushq	%r14	#
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	pushq	%r13	#
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
# memusage.c:220:   const char *env = getenv ("MEMUSAGE_PROG_NAME");
	leaq	.LC2(%rip), %rdi	#,
# memusage.c:219: {
	pushq	%r12	#
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	pushq	%rbp	#
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	pushq	%rbx	#
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
	subq	$192, %rsp	#,
	.cfi_def_cfa_offset 240
# memusage.c:220:   const char *env = getenv ("MEMUSAGE_PROG_NAME");
	call	getenv@PLT	#
# memusage.c:221:   size_t prog_len = strlen (__progname);
	movq	__progname@GOTPCREL(%rip), %rbp	#, tmp136
# memusage.c:220:   const char *env = getenv ("MEMUSAGE_PROG_NAME");
	movq	%rax, %rbx	#, env
# memusage.c:221:   size_t prog_len = strlen (__progname);
	movq	0(%rbp), %rdi	# __progname,
	call	strlen@PLT	#
# memusage.c:224:   mallocp = (void *(*)(size_t))dlsym (RTLD_NEXT, "malloc");
	leaq	.LC3(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:221:   size_t prog_len = strlen (__progname);
	movq	%rax, %r12	#, tmp137
# memusage.c:223:   initialized = -1;
	movl	$-1, initialized(%rip)	#, initialized
# memusage.c:224:   mallocp = (void *(*)(size_t))dlsym (RTLD_NEXT, "malloc");
	call	dlsym@PLT	#
# memusage.c:225:   reallocp = (void *(*)(void *, size_t))dlsym (RTLD_NEXT, "realloc");
	leaq	.LC4(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:224:   mallocp = (void *(*)(size_t))dlsym (RTLD_NEXT, "malloc");
	movq	%rax, mallocp(%rip)	# _2, mallocp
# memusage.c:225:   reallocp = (void *(*)(void *, size_t))dlsym (RTLD_NEXT, "realloc");
	call	dlsym@PLT	#
# memusage.c:226:   callocp = (void *(*)(size_t, size_t))dlsym (RTLD_NEXT, "calloc");
	leaq	.LC5(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:225:   reallocp = (void *(*)(void *, size_t))dlsym (RTLD_NEXT, "realloc");
	movq	%rax, reallocp(%rip)	# _4, reallocp
# memusage.c:226:   callocp = (void *(*)(size_t, size_t))dlsym (RTLD_NEXT, "calloc");
	call	dlsym@PLT	#
# memusage.c:227:   freep = (void (*)(void *))dlsym (RTLD_NEXT, "free");
	leaq	.LC6(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:226:   callocp = (void *(*)(size_t, size_t))dlsym (RTLD_NEXT, "calloc");
	movq	%rax, callocp(%rip)	# _6, callocp
# memusage.c:227:   freep = (void (*)(void *))dlsym (RTLD_NEXT, "free");
	call	dlsym@PLT	#
# memusage.c:229:   mmapp = (void *(*)(void *, size_t, int, int, int, off_t))dlsym (RTLD_NEXT,
	leaq	.LC7(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:227:   freep = (void (*)(void *))dlsym (RTLD_NEXT, "free");
	movq	%rax, freep(%rip)	# _8, freep
# memusage.c:229:   mmapp = (void *(*)(void *, size_t, int, int, int, off_t))dlsym (RTLD_NEXT,
	call	dlsym@PLT	#
# memusage.c:232:     (void *(*)(void *, size_t, int, int, int, off64_t))dlsym (RTLD_NEXT,
	leaq	.LC8(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:229:   mmapp = (void *(*)(void *, size_t, int, int, int, off_t))dlsym (RTLD_NEXT,
	movq	%rax, mmapp(%rip)	# _10, mmapp
# memusage.c:232:     (void *(*)(void *, size_t, int, int, int, off64_t))dlsym (RTLD_NEXT,
	call	dlsym@PLT	#
# memusage.c:234:   mremapp = (void *(*)(void *, size_t, size_t, int, void *))dlsym (RTLD_NEXT,
	leaq	.LC9(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:231:   mmap64p =
	movq	%rax, mmap64p(%rip)	# _12, mmap64p
# memusage.c:234:   mremapp = (void *(*)(void *, size_t, size_t, int, void *))dlsym (RTLD_NEXT,
	call	dlsym@PLT	#
# memusage.c:236:   munmapp = (int (*)(void *, size_t))dlsym (RTLD_NEXT, "munmap");
	leaq	.LC10(%rip), %rsi	#,
	movq	$-1, %rdi	#,
# memusage.c:234:   mremapp = (void *(*)(void *, size_t, size_t, int, void *))dlsym (RTLD_NEXT,
	movq	%rax, mremapp(%rip)	# _14, mremapp
# memusage.c:236:   munmapp = (int (*)(void *, size_t))dlsym (RTLD_NEXT, "munmap");
	call	dlsym@PLT	#
# memusage.c:239:   if (env != NULL)
	testq	%rbx, %rbx	# env
# memusage.c:236:   munmapp = (int (*)(void *, size_t))dlsym (RTLD_NEXT, "munmap");
	movq	%rax, munmapp(%rip)	# _16, munmapp
# memusage.c:237:   initialized = 1;
	movl	$1, initialized(%rip)	#, initialized
# memusage.c:239:   if (env != NULL)
	je	.L32	#,
# memusage.c:242:       size_t len = strlen (env);
	movq	%rbx, %rdi	# env,
	call	strlen@PLT	#
# memusage.c:243:       if (len > prog_len || strcmp (env, &__progname[prog_len - len]) != 0
	cmpq	%rax, %r12	# tmp140, tmp137
# memusage.c:242:       size_t len = strlen (env);
	movq	%rax, %r13	#, tmp140
# memusage.c:243:       if (len > prog_len || strcmp (env, &__progname[prog_len - len]) != 0
	jnb	.L60	#,
.L33:
# memusage.c:245:         not_me = true;
	movb	$1, not_me(%rip)	#, not_me
.L31:
# memusage.c:312: }
	addq	$192, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 48
	popq	%rbx	#
	.cfi_def_cfa_offset 40
	popq	%rbp	#
	.cfi_def_cfa_offset 32
	popq	%r12	#
	.cfi_def_cfa_offset 24
	popq	%r13	#
	.cfi_def_cfa_offset 16
	popq	%r14	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L60:
	.cfi_restore_state
# memusage.c:243:       if (len > prog_len || strcmp (env, &__progname[prog_len - len]) != 0
	movq	0(%rbp), %r14	# __progname, __progname.2_18
	movq	%r12, %rbp	# tmp137, _19
	movq	%rbx, %rdi	# env,
	subq	%rax, %rbp	# tmp140, _19
	leaq	(%r14,%rbp), %rsi	#, tmp144
	call	strcmp@PLT	#
	testl	%eax, %eax	# _21
	jne	.L33	#,
# memusage.c:244:           || (prog_len != len && __progname[prog_len - len - 1] != '/'))
	cmpq	%r13, %r12	# tmp140, tmp137
	je	.L32	#,
# memusage.c:244:           || (prog_len != len && __progname[prog_len - len - 1] != '/'))
	cmpb	$47, -1(%r14,%rbp)	#, *_23
	jne	.L33	#,
	.p2align 4,,10
	.p2align 3
.L32:
# memusage.c:249:   if (!not_me && fd == -1)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L31	#,
# memusage.c:249:   if (!not_me && fd == -1)
	cmpl	$-1, fd(%rip)	#, fd
	jne	.L31	#,
# memusage.c:253:       if (!start_sp)
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT	#
	cmpq	$0, start_sp@dtpoff(%rax)	#, start_sp
	jne	.L37	#,
# memusage.c:254:         start_sp = GETSP ();
	movq	%rsp, start_sp@dtpoff(%rax)	#, start_sp
.L37:
# memusage.c:256:       outname = getenv ("MEMUSAGE_OUTPUT");
	leaq	.LC11(%rip), %rdi	#,
	call	getenv@PLT	#
# memusage.c:257:       if (outname != NULL && outname[0] != '\0'
	testq	%rax, %rax	# outname
# memusage.c:256:       outname = getenv ("MEMUSAGE_OUTPUT");
	movq	%rax, %rbx	#, outname
# memusage.c:257:       if (outname != NULL && outname[0] != '\0'
	je	.L39	#,
# memusage.c:257:       if (outname != NULL && outname[0] != '\0'
	cmpb	$0, (%rax)	#, *outname_74
	jne	.L61	#,
.L39:
# memusage.c:309:       if (!not_me && getenv ("MEMUSAGE_TRACE_MMAP") != NULL)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L31	#,
# memusage.c:309:       if (!not_me && getenv ("MEMUSAGE_TRACE_MMAP") != NULL)
	leaq	.LC14(%rip), %rdi	#,
	call	getenv@PLT	#
	testq	%rax, %rax	# _40
	je	.L31	#,
# memusage.c:310:         trace_mmap = true;
	movb	$1, trace_mmap(%rip)	#, trace_mmap
# memusage.c:312: }
	jmp	.L31	#
	.p2align 4,,10
	.p2align 3
.L61:
# memusage.c:258:           && (access (outname, R_OK | W_OK) == 0 || errno == ENOENT))
	movl	$6, %esi	#,
	movq	%rax, %rdi	# outname,
	call	access@PLT	#
	testl	%eax, %eax	# _29
	je	.L41	#,
# memusage.c:258:           && (access (outname, R_OK | W_OK) == 0 || errno == ENOENT))
	call	__errno_location@PLT	#
	cmpl	$2, (%rax)	#, *_30
	jne	.L39	#,
.L41:
# memusage.c:260:           fd = creat64 (outname, 0666);
	movq	%rbx, %rdi	# outname,
	movl	$438, %esi	#,
	call	creat64@PLT	#
# memusage.c:262:           if (fd == -1)
	cmpl	$-1, %eax	#, _32
# memusage.c:260:           fd = creat64 (outname, 0666);
	movl	%eax, %edi	#, _32
	movl	%eax, fd(%rip)	# _32, fd
# memusage.c:262:           if (fd == -1)
	je	.L33	#,
# memusage.c:273:               write (fd, &first, sizeof (first));
	leaq	first(%rip), %rsi	#,
# memusage.c:269:               first.heap = 0;
	movq	$0, first(%rip)	#, first.heap
# memusage.c:270:               first.stack = 0;
	movq	$0, 8+first(%rip)	#, first.stack
# memusage.c:271:               GETTIME (first.time_low, first.time_high);
#APP
# 271 "memusage.c" 1
	rdtsc
# 0 "" 2
#NO_APP
	movl	%edx, 20+first(%rip)	# tmp153, first.time_high
# memusage.c:273:               write (fd, &first, sizeof (first));
	movl	$24, %edx	#,
# memusage.c:271:               GETTIME (first.time_low, first.time_high);
	movl	%eax, 16+first(%rip)	# tmp151, first.time_low
# memusage.c:273:               write (fd, &first, sizeof (first));
	call	write@PLT	#
# memusage.c:274:               write (fd, &first, sizeof (first));
	movl	fd(%rip), %edi	# fd,
	leaq	first(%rip), %rsi	#,
	movl	$24, %edx	#,
	call	write@PLT	#
# memusage.c:279:               const char *str_buffer_size = getenv ("MEMUSAGE_BUFFER_SIZE");
	leaq	.LC12(%rip), %rdi	#,
# memusage.c:278:               buffer_size = DEFAULT_BUFFER_SIZE;
	movq	$32768, buffer_size(%rip)	#, buffer_size
# memusage.c:279:               const char *str_buffer_size = getenv ("MEMUSAGE_BUFFER_SIZE");
	call	getenv@PLT	#
# memusage.c:280:               if (str_buffer_size != NULL)
	testq	%rax, %rax	# str_buffer_size
	je	.L43	#,
# ../stdlib/stdlib.h:363:   return (int) strtol (__nptr, (char **) NULL, 10);
	movl	$10, %edx	#,
	xorl	%esi, %esi	#
	movq	%rax, %rdi	# str_buffer_size,
	call	strtol@PLT	#
# memusage.c:282:                   buffer_size = atoi (str_buffer_size);
	cltq
# memusage.c:283:                   if (buffer_size == 0 || buffer_size > DEFAULT_BUFFER_SIZE)
	leaq	-1(%rax), %rdx	#, tmp157
# memusage.c:282:                   buffer_size = atoi (str_buffer_size);
	cmpq	$32767, %rdx	#, tmp157
	movl	$32768, %edx	#, tmp165
	cmova	%rdx, %rax	# _35,, tmp165, tmp164
	movq	%rax, buffer_size(%rip)	# tmp164, buffer_size
.L43:
# memusage.c:288:               if (getenv ("MEMUSAGE_NO_TIMER") == NULL)
	leaq	.LC13(%rip), %rdi	#,
	call	getenv@PLT	#
	testq	%rax, %rax	# _37
	jne	.L39	#,
# memusage.c:292:                   act.sa_handler = (sighandler_t) &int_handler;
	leaq	int_handler(%rip), %rax	#, tmp171
# memusage.c:294:                   sigfillset (&act.sa_mask);
	leaq	40(%rsp), %rdi	#, tmp160
	leaq	32(%rsp), %rbx	#, tmp159
# memusage.c:293:                   act.sa_flags = SA_RESTART;
	movl	$268435456, 168(%rsp)	#, act.sa_flags
# memusage.c:292:                   act.sa_handler = (sighandler_t) &int_handler;
	movq	%rax, 32(%rsp)	# tmp171, act.__sigaction_handler.sa_handler
# memusage.c:294:                   sigfillset (&act.sa_mask);
	call	sigfillset@PLT	#
# memusage.c:296:                   if (sigaction (SIGPROF, &act, NULL) >= 0)
	xorl	%edx, %edx	#
	movq	%rbx, %rsi	# tmp159,
	movl	$27, %edi	#,
	call	sigaction@PLT	#
	testl	%eax, %eax	# _38
	js	.L39	#,
# memusage.c:300:                       timer.it_value.tv_sec = 0;
	movq	$0, 16(%rsp)	#, timer.it_value.tv_sec
# memusage.c:301:                       timer.it_value.tv_usec = 1;
	movq	$1, 24(%rsp)	#, timer.it_value.tv_usec
# memusage.c:303:                       setitimer (ITIMER_PROF, &timer, NULL);
	movq	%rsp, %rsi	#, tmp163
# memusage.c:302:                       timer.it_interval = timer.it_value;
	movdqa	16(%rsp), %xmm0	# timer.it_value, timer.it_value
# memusage.c:303:                       setitimer (ITIMER_PROF, &timer, NULL);
	xorl	%edx, %edx	#
	movl	$2, %edi	#,
# memusage.c:302:                       timer.it_interval = timer.it_value;
	movaps	%xmm0, (%rsp)	# timer.it_value, timer.it_interval
# memusage.c:303:                       setitimer (ITIMER_PROF, &timer, NULL);
	call	setitimer@PLT	#
	jmp	.L39	#
	.cfi_endproc
.LFE68:
	.size	me, .-me
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.type	init, @function
init:
.LFB69:
	.cfi_startproc
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 16
# memusage.c:320:   start_sp = GETSP ();
	leaq	start_sp@tlsld(%rip), %rdi
	call	__tls_get_addr@PLT	#
# memusage.c:321:   if (!initialized)
	movl	initialized(%rip), %ecx	# initialized,
# memusage.c:320:   start_sp = GETSP ();
	movq	%rsp, start_sp@dtpoff(%rax)	#, start_sp
# memusage.c:321:   if (!initialized)
	testl	%ecx, %ecx	#
	je	.L65	#,
# memusage.c:323: }
	popq	%rax	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L65:
	.cfi_restore_state
	popq	%rdx	#
	.cfi_def_cfa_offset 8
# memusage.c:322:     me ();
	jmp	me	#
	.cfi_endproc
.LFE69:
	.size	init, .-init
	.section	.ctors,"aw",@progbits
	.align 8
	.quad	init
	.text
	.p2align 4,,15
	.globl	malloc
	.type	malloc, @function
malloc:
.LFB70:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rdi, %rbx	# len, len
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 32
# memusage.c:334:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.34_1
	testl	%eax, %eax	# initialized.34_1
	jle	.L75	#,
.L67:
# memusage.c:343:   if (not_me)
	cmpb	$0, not_me(%rip)	#, not_me
	je	.L69	#,
# memusage.c:373: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# memusage.c:344:     return (*mallocp)(len);
	movq	%rbx, %rdi	# len,
# memusage.c:373: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# memusage.c:344:     return (*mallocp)(len);
	jmp	*mallocp(%rip)	# mallocp
	.p2align 4,,10
	.p2align 3
.L69:
	.cfi_restore_state
# memusage.c:347:   catomic_increment (&calls[idx_malloc]);
#APP
# 347 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq calls(%rip)	# calls
# 0 "" 2
# memusage.c:349:   catomic_add (&total[idx_malloc], len);
# 349 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, total(%rip)	# len, total
# 0 "" 2
# memusage.c:351:   catomic_add (&grand_total, len);
# 351 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)	# len, grand_total
# 0 "" 2
# memusage.c:353:   if (len < 65536)
#NO_APP
	cmpq	$65535, %rbx	#, len
	ja	.L70	#,
# memusage.c:354:     catomic_increment (&histogram[len / 16]);
	movq	%rbx, %rdx	# len, _16
	leaq	histogram(%rip), %rax	#, tmp102
	shrq	$4, %rdx	#, _16
#APP
# 354 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rax,%rdx,8)	# histogram
# 0 "" 2
#NO_APP
.L71:
# memusage.c:358:   catomic_increment (&calls_total);
#APP
# 358 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq calls_total(%rip)	# calls_total
# 0 "" 2
# memusage.c:361:   result = (struct header *) (*mallocp)(len + sizeof (struct header));
#NO_APP
	leaq	16(%rbx), %rdi	#, tmp104
	call	*mallocp(%rip)	# mallocp
# memusage.c:362:   if (result == NULL)
	testq	%rax, %rax	# result
# memusage.c:361:   result = (struct header *) (*mallocp)(len + sizeof (struct header));
	movq	%rax, %rbp	#, result
# memusage.c:362:   if (result == NULL)
	je	.L76	#,
# memusage.c:369:   update_data (result, len, 0);
	movq	%rax, %rdi	# result,
	xorl	%edx, %edx	#
	movq	%rbx, %rsi	# len,
	call	update_data	#
# memusage.c:372:   return (void *) (result + 1);
	leaq	16(%rbp), %rax	#, <retval>
.L66:
# memusage.c:373: }
	addq	$8, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L75:
	.cfi_restore_state
# memusage.c:336:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.34_1
	je	.L73	#,
# memusage.c:339:       me ();
	call	me	#
	jmp	.L67	#
	.p2align 4,,10
	.p2align 3
.L70:
# memusage.c:356:     catomic_increment (&large);
#APP
# 356 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq large(%rip)	# large
# 0 "" 2
#NO_APP
	jmp	.L71	#
	.p2align 4,,10
	.p2align 3
.L76:
# memusage.c:364:       catomic_increment (&failed[idx_malloc]);
#APP
# 364 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq failed(%rip)	# failed
# 0 "" 2
# memusage.c:365:       return NULL;
#NO_APP
	xorl	%eax, %eax	# <retval>
	jmp	.L66	#
.L73:
# memusage.c:337:         return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L66	#
	.cfi_endproc
.LFE70:
	.size	malloc, .-malloc
	.p2align 4,,15
	.globl	realloc
	.type	realloc, @function
realloc:
.LFB71:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
	movq	%rsi, %rbx	# len, len
	subq	$24, %rsp	#,
	.cfi_def_cfa_offset 64
# memusage.c:386:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.41_1
	testl	%eax, %eax	# initialized.41_1
	jle	.L100	#,
.L78:
# memusage.c:395:   if (not_me)
	cmpb	$0, not_me(%rip)	#, not_me
	je	.L80	#,
.L99:
# memusage.c:468: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
# memusage.c:409:         return (*reallocp)(old, len);
	movq	%rbx, %rsi	# len,
# memusage.c:468: }
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
# memusage.c:409:         return (*reallocp)(old, len);
	jmp	*reallocp(%rip)	# reallocp
	.p2align 4,,10
	.p2align 3
.L80:
	.cfi_restore_state
# memusage.c:398:   if (old == NULL)
	testq	%rdi, %rdi	# old
	je	.L91	#,
# memusage.c:407:       if (real->magic != MAGIC)
	movl	$4276993711, %eax	#, tmp110
	cmpq	%rax, -8(%rdi)	# tmp110, MEM[(struct header *)old_36(D) + -16B].magic
	jne	.L99	#,
# memusage.c:411:       old_len = real->length;
	movq	-16(%rdi), %r12	# MEM[(struct header *)old_36(D) + -16B].length, old_len
# memusage.c:406:       real = ((struct header *) old) - 1;
	leaq	-16(%rdi), %r13	#, real
	.p2align 4,,10
	.p2align 3
.L81:
# memusage.c:415:   catomic_increment (&calls[idx_realloc]);
#APP
# 415 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 8+calls(%rip)	# calls
# 0 "" 2
# memusage.c:416:   if (len > old_len)
#NO_APP
	cmpq	%rbx, %r12	# len, old_len
	jnb	.L83	#,
# memusage.c:419:       catomic_add (&total[idx_realloc], len - old_len);
	movq	%rbx, %rax	# len, _13
	subq	%r12, %rax	# old_len, _13
#APP
# 419 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rax, 8+total(%rip)	# _13, total
# 0 "" 2
# memusage.c:421:       catomic_add (&grand_total, len - old_len);
# 421 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rax, grand_total(%rip)	# _13, grand_total
# 0 "" 2
#NO_APP
.L83:
# memusage.c:424:   if (len == 0 && old != NULL)
	testq	%rbx, %rbx	# len
	jne	.L84	#,
	testq	%rdi, %rdi	# old
	jne	.L101	#,
.L84:
# memusage.c:441:   if (len < 65536)
	cmpq	$65535, %rbx	#, len
	ja	.L85	#,
# memusage.c:442:     catomic_increment (&histogram[len / 16]);
	movq	%rbx, %rdx	# len, _33
	leaq	histogram(%rip), %rax	#, tmp124
	shrq	$4, %rdx	#, _33
#APP
# 442 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rax,%rdx,8)	# histogram
# 0 "" 2
#NO_APP
.L86:
# memusage.c:446:   catomic_increment (&calls_total);
#APP
# 446 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq calls_total(%rip)	# calls_total
# 0 "" 2
# memusage.c:449:   result = (struct header *) (*reallocp)(real, len + sizeof (struct header));
#NO_APP
	leaq	16(%rbx), %rsi	#, tmp126
	movq	%r13, %rdi	# real,
	call	*reallocp(%rip)	# reallocp
# memusage.c:450:   if (result == NULL)
	testq	%rax, %rax	# result
# memusage.c:449:   result = (struct header *) (*reallocp)(real, len + sizeof (struct header));
	movq	%rax, %rbp	#, result
# memusage.c:450:   if (result == NULL)
	je	.L102	#,
# memusage.c:457:   if (real == result)
	cmpq	%rax, %r13	# result, real
	je	.L103	#,
.L88:
# memusage.c:460:   if (old_len > len)
	cmpq	%rbx, %r12	# len, old_len
	jbe	.L89	#,
# memusage.c:461:     catomic_increment (&decreasing);
#APP
# 461 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq decreasing(%rip)	# decreasing
# 0 "" 2
#NO_APP
.L89:
# memusage.c:464:   update_data (result, len, old_len);
	movq	%r12, %rdx	# old_len,
	movq	%rbx, %rsi	# len,
	movq	%rbp, %rdi	# result,
	call	update_data	#
# memusage.c:467:   return (void *) (result + 1);
	leaq	16(%rbp), %rax	#, <retval>
.L77:
# memusage.c:468: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	.cfi_restore_state
# memusage.c:388:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.41_1
	movq	%rdi, 8(%rsp)	# old, %sfp
	je	.L90	#,
# memusage.c:391:       me ();
	call	me	#
	movq	8(%rsp), %rdi	# %sfp, old
	jmp	.L78	#
	.p2align 4,,10
	.p2align 3
.L91:
# memusage.c:402:       old_len = 0;
	xorl	%r12d, %r12d	# old_len
# memusage.c:401:       real = NULL;
	xorl	%r13d, %r13d	# real
	jmp	.L81	#
	.p2align 4,,10
	.p2align 3
.L85:
# memusage.c:444:     catomic_increment (&large);
#APP
# 444 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq large(%rip)	# large
# 0 "" 2
#NO_APP
	jmp	.L86	#
	.p2align 4,,10
	.p2align 3
.L103:
# memusage.c:458:     catomic_increment (&inplace);
#APP
# 458 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq inplace(%rip)	# inplace
# 0 "" 2
#NO_APP
	jmp	.L88	#
.L90:
# memusage.c:389:         return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L77	#
.L101:
# memusage.c:427:       catomic_increment (&realloc_free);
#APP
# 427 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq realloc_free(%rip)	# realloc_free
# 0 "" 2
# memusage.c:429:       catomic_add (&total[idx_free], real->length);
#NO_APP
	movq	0(%r13), %rax	# real_31->length, real_31->length
#APP
# 429 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rax, 24+total(%rip)	# real_31->length, total
# 0 "" 2
# memusage.c:432:       update_data (NULL, 0, old_len);
#NO_APP
	xorl	%esi, %esi	#
	movq	%r12, %rdx	# old_len,
	xorl	%edi, %edi	#
	call	update_data	#
# memusage.c:435:       (*freep) (real);
	movq	%r13, %rdi	# real,
	call	*freep(%rip)	# freep
# memusage.c:437:       return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L77	#
.L102:
# memusage.c:452:       catomic_increment (&failed[idx_realloc]);
#APP
# 452 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 8+failed(%rip)	# failed
# 0 "" 2
# memusage.c:453:       return NULL;
#NO_APP
	xorl	%eax, %eax	# <retval>
	jmp	.L77	#
	.cfi_endproc
.LFE71:
	.size	realloc, .-realloc
	.p2align 4,,15
	.globl	calloc
	.type	calloc, @function
calloc:
.LFB72:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	subq	$24, %rsp	#,
	.cfi_def_cfa_offset 48
# memusage.c:480:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.48_1
	testl	%eax, %eax	# initialized.48_1
	jle	.L114	#,
.L105:
# memusage.c:489:   if (not_me)
	cmpb	$0, not_me(%rip)	#, not_me
	je	.L107	#,
# memusage.c:519: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# memusage.c:490:     return (*callocp)(n, len);
	jmp	*callocp(%rip)	# callocp
	.p2align 4,,10
	.p2align 3
.L107:
	.cfi_restore_state
# memusage.c:477:   size_t size = n * len;
	movq	%rdi, %rbx	# n, n
	imulq	%rsi, %rbx	# len, n
# memusage.c:493:   catomic_increment (&calls[idx_calloc]);
#APP
# 493 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 16+calls(%rip)	# calls
# 0 "" 2
# memusage.c:495:   catomic_add (&total[idx_calloc], size);
# 495 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, 16+total(%rip)	# size, total
# 0 "" 2
# memusage.c:497:   catomic_add (&grand_total, size);
# 497 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)	# size, grand_total
# 0 "" 2
# memusage.c:499:   if (size < 65536)
#NO_APP
	cmpq	$65535, %rbx	#, size
	ja	.L108	#,
# memusage.c:500:     catomic_increment (&histogram[size / 16]);
	movq	%rbx, %rdx	# size, _16
	leaq	histogram(%rip), %rax	#, tmp107
	shrq	$4, %rdx	#, _16
#APP
# 500 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rax,%rdx,8)	# histogram
# 0 "" 2
#NO_APP
.L109:
# memusage.c:504:   ++calls_total;
	addq	$1, calls_total(%rip)	#, calls_total
# memusage.c:507:   result = (struct header *) (*mallocp)(size + sizeof (struct header));
	leaq	16(%rbx), %rdi	#, tmp109
	call	*mallocp(%rip)	# mallocp
# memusage.c:508:   if (result == NULL)
	testq	%rax, %rax	# result
# memusage.c:507:   result = (struct header *) (*mallocp)(size + sizeof (struct header));
	movq	%rax, %rbp	#, result
# memusage.c:508:   if (result == NULL)
	je	.L115	#,
# memusage.c:515:   update_data (result, size, 0);
	movq	%rbx, %rsi	# size,
	xorl	%edx, %edx	#
	movq	%rax, %rdi	# result,
	call	update_data	#
# memusage.c:519: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
# memusage.c:518:   return memset (result + 1, '\0', size);
	leaq	16(%rbp), %rdi	#, tmp113
	movq	%rbx, %rdx	# size,
# memusage.c:519: }
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
# memusage.c:518:   return memset (result + 1, '\0', size);
	xorl	%esi, %esi	#
	jmp	memset@PLT	#
	.p2align 4,,10
	.p2align 3
.L114:
	.cfi_restore_state
# memusage.c:482:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.48_1
	movq	%rsi, 8(%rsp)	# len, %sfp
	movq	%rdi, (%rsp)	# n, %sfp
	je	.L104	#,
# memusage.c:485:       me ();
	call	me	#
	movq	8(%rsp), %rsi	# %sfp, len
	movq	(%rsp), %rdi	# %sfp, n
	jmp	.L105	#
	.p2align 4,,10
	.p2align 3
.L108:
# memusage.c:502:     catomic_increment (&large);
#APP
# 502 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq large(%rip)	# large
# 0 "" 2
#NO_APP
	jmp	.L109	#
	.p2align 4,,10
	.p2align 3
.L115:
# memusage.c:510:       catomic_increment (&failed[idx_calloc]);
#APP
# 510 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 16+failed(%rip)	# failed
# 0 "" 2
#NO_APP
.L104:
# memusage.c:519: }
	addq	$24, %rsp	#,
	.cfi_def_cfa_offset 24
	xorl	%eax, %eax	#
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE72:
	.size	calloc, .-calloc
	.p2align 4,,15
	.globl	free
	.type	free, @function
free:
.LFB73:
	.cfi_startproc
# memusage.c:530:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.56_1
# memusage.c:526: {
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
# memusage.c:526: {
	movq	%rdi, %rbx	# ptr, ptr
# memusage.c:530:   if (__glibc_unlikely (initialized <= 0))
	testl	%eax, %eax	# initialized.56_1
	jle	.L127	#,
.L117:
# memusage.c:539:   if (not_me)
	cmpb	$0, not_me(%rip)	#, not_me
	je	.L119	#,
.L126:
# memusage.c:557:       (*freep) (ptr);
	movq	%rbx, %rdi	# ptr,
# memusage.c:571: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
# memusage.c:557:       (*freep) (ptr);
	jmp	*freep(%rip)	# freep
	.p2align 4,,10
	.p2align 3
.L119:
	.cfi_restore_state
# memusage.c:546:   if (ptr == NULL)
	testq	%rbx, %rbx	# ptr
	je	.L128	#,
# memusage.c:554:   if (real->magic != MAGIC)
	movl	$4276993711, %eax	#, tmp101
	cmpq	%rax, -8(%rbx)	# tmp101, MEM[(struct header *)ptr_19(D) + -16B].magic
	jne	.L126	#,
# memusage.c:562:   catomic_increment (&calls[idx_free]);
#APP
# 562 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 24+calls(%rip)	# calls
# 0 "" 2
# memusage.c:564:   catomic_add (&total[idx_free], real->length);
#NO_APP
	movq	-16(%rbx), %rax	# MEM[(struct header *)ptr_19(D) + -16B].length, MEM[(struct header *)ptr_19(D) + -16B].length
#APP
# 564 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rax, 24+total(%rip)	# MEM[(struct header *)ptr_19(D) + -16B].length, total
# 0 "" 2
# memusage.c:567:   update_data (NULL, 0, real->length);
#NO_APP
	movq	-16(%rbx), %rdx	# MEM[(struct header *)ptr_19(D) + -16B].length, MEM[(struct header *)ptr_19(D) + -16B].length
	xorl	%edi, %edi	#
	xorl	%esi, %esi	#
	call	update_data	#
# memusage.c:553:   real = ((struct header *) ptr) - 1;
	leaq	-16(%rbx), %rdi	#, real
# memusage.c:571: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 8
# memusage.c:570:   (*freep) (real);
	jmp	*freep(%rip)	# freep
	.p2align 4,,10
	.p2align 3
.L127:
	.cfi_restore_state
# memusage.c:532:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.56_1
	je	.L116	#,
# memusage.c:535:       me ();
	call	me	#
	jmp	.L117	#
	.p2align 4,,10
	.p2align 3
.L128:
# memusage.c:548:       catomic_increment (&calls[idx_free]);
#APP
# 548 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 24+calls(%rip)	# calls
# 0 "" 2
#NO_APP
.L116:
# memusage.c:571: }
	popq	%rbx	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE73:
	.size	free, .-free
	.p2align 4,,15
	.globl	mmap
	.type	mmap, @function
mmap:
.LFB74:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movl	%edx, %r12d	# prot, prot
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	%ecx, %ebp	# flags, flags
	movq	%rsi, %rbx	# len, len
	subq	$32, %rsp	#,
	.cfi_def_cfa_offset 64
# memusage.c:582:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.62_1
	testl	%eax, %eax	# initialized.62_1
	jle	.L140	#,
.L130:
# memusage.c:591:   result = (*mmapp)(start, len, prot, flags, fd, offset);
	movl	%ebp, %ecx	# flags,
	movl	%r12d, %edx	# prot,
	movq	%rbx, %rsi	# len,
	call	*mmapp(%rip)	# mmapp
# memusage.c:593:   if (!not_me && trace_mmap)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L129	#,
# memusage.c:593:   if (!not_me && trace_mmap)
	cmpb	$0, trace_mmap(%rip)	#, trace_mmap
	je	.L129	#,
# memusage.c:596:                  ? idx_mmap_a : prot & PROT_WRITE ? idx_mmap_w : idx_mmap_r);
	andl	$32, %ebp	#, flags
	movl	$6, %ecx	#, iftmp.67_17
	jne	.L132	#,
# memusage.c:596:                  ? idx_mmap_a : prot & PROT_WRITE ? idx_mmap_w : idx_mmap_r);
	xorl	%ecx, %ecx	# iftmp.67_17
	andl	$2, %r12d	#, prot
	setne	%cl	#, iftmp.67_17
	addl	$4, %ecx	#, iftmp.67_17
.L132:
# memusage.c:599:       catomic_increment (&calls[idx]);
	leaq	calls(%rip), %rsi	#, tmp106
	movslq	%ecx, %rdx	# iftmp.67_17, iftmp.67_17
#APP
# 599 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rsi,%rdx,8)	# calls
# 0 "" 2
# memusage.c:601:       catomic_add (&total[idx], len);
#NO_APP
	leaq	total(%rip), %rsi	#, tmp110
#APP
# 601 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, (%rsi,%rdx,8)	# len, total
# 0 "" 2
# memusage.c:603:       catomic_add (&grand_total, len);
# 603 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)	# len, grand_total
# 0 "" 2
# memusage.c:605:       if (len < 65536)
#NO_APP
	cmpq	$65535, %rbx	#, len
	jbe	.L141	#,
# memusage.c:608:         catomic_increment (&large);
#APP
# 608 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq large(%rip)	# large
# 0 "" 2
#NO_APP
.L134:
# memusage.c:610:       catomic_increment (&calls_total);
#APP
# 610 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq calls_total(%rip)	# calls_total
# 0 "" 2
# memusage.c:613:       if (result == NULL)
#NO_APP
	testq	%rax, %rax	# <retval>
	je	.L142	#,
# memusage.c:615:       else if (idx == idx_mmap_w)
	cmpl	$5, %ecx	#, iftmp.67_17
	je	.L143	#,
.L129:
# memusage.c:624: }
	addq	$32, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L141:
	.cfi_restore_state
# memusage.c:606:         catomic_increment (&histogram[len / 16]);
	movq	%rbx, %rdi	# len, _18
	leaq	histogram(%rip), %rsi	#, tmp114
	shrq	$4, %rdi	#, _18
#APP
# 606 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rsi,%rdi,8)	# histogram
# 0 "" 2
#NO_APP
	jmp	.L134	#
	.p2align 4,,10
	.p2align 3
.L140:
# memusage.c:584:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.62_1
	movq	%r9, 24(%rsp)	# offset, %sfp
	movl	%r8d, 20(%rsp)	# fd, %sfp
	movq	%rdi, 8(%rsp)	# start, %sfp
	je	.L136	#,
# memusage.c:587:       me ();
	call	me	#
	movq	24(%rsp), %r9	# %sfp, offset
	movl	20(%rsp), %r8d	# %sfp, fd
	movq	8(%rsp), %rdi	# %sfp, start
	jmp	.L130	#
	.p2align 4,,10
	.p2align 3
.L142:
# memusage.c:614:         catomic_increment (&failed[idx]);
	leaq	failed(%rip), %rcx	#, tmp116
#APP
# 614 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rcx,%rdx,8)	# failed
# 0 "" 2
#NO_APP
	jmp	.L129	#
	.p2align 4,,10
	.p2align 3
.L143:
# memusage.c:619:         update_data (NULL, len, 0);
	xorl	%edx, %edx	#
	movq	%rbx, %rsi	# len,
	xorl	%edi, %edi	#
	movq	%rax, 8(%rsp)	# <retval>, %sfp
	call	update_data	#
	movq	8(%rsp), %rax	# %sfp, <retval>
	jmp	.L129	#
.L136:
# memusage.c:585:         return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L129	#
	.cfi_endproc
.LFE74:
	.size	mmap, .-mmap
	.p2align 4,,15
	.globl	mmap64
	.type	mmap64, @function
mmap64:
.LFB75:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movl	%edx, %r12d	# prot, prot
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movl	%ecx, %ebp	# flags, flags
	movq	%rsi, %rbx	# len, len
	subq	$32, %rsp	#,
	.cfi_def_cfa_offset 64
# memusage.c:635:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.71_1
	testl	%eax, %eax	# initialized.71_1
	jle	.L155	#,
.L145:
# memusage.c:644:   result = (*mmap64p)(start, len, prot, flags, fd, offset);
	movl	%ebp, %ecx	# flags,
	movl	%r12d, %edx	# prot,
	movq	%rbx, %rsi	# len,
	call	*mmap64p(%rip)	# mmap64p
# memusage.c:646:   if (!not_me && trace_mmap)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L144	#,
# memusage.c:646:   if (!not_me && trace_mmap)
	cmpb	$0, trace_mmap(%rip)	#, trace_mmap
	je	.L144	#,
# memusage.c:649:                  ? idx_mmap_a : prot & PROT_WRITE ? idx_mmap_w : idx_mmap_r);
	andl	$32, %ebp	#, flags
	movl	$6, %ecx	#, iftmp.76_17
	jne	.L147	#,
# memusage.c:649:                  ? idx_mmap_a : prot & PROT_WRITE ? idx_mmap_w : idx_mmap_r);
	xorl	%ecx, %ecx	# iftmp.76_17
	andl	$2, %r12d	#, prot
	setne	%cl	#, iftmp.76_17
	addl	$4, %ecx	#, iftmp.76_17
.L147:
# memusage.c:652:       catomic_increment (&calls[idx]);
	leaq	calls(%rip), %rsi	#, tmp106
	movslq	%ecx, %rdx	# iftmp.76_17, iftmp.76_17
#APP
# 652 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rsi,%rdx,8)	# calls
# 0 "" 2
# memusage.c:654:       catomic_add (&total[idx], len);
#NO_APP
	leaq	total(%rip), %rsi	#, tmp110
#APP
# 654 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, (%rsi,%rdx,8)	# len, total
# 0 "" 2
# memusage.c:656:       catomic_add (&grand_total, len);
# 656 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, grand_total(%rip)	# len, grand_total
# 0 "" 2
# memusage.c:658:       if (len < 65536)
#NO_APP
	cmpq	$65535, %rbx	#, len
	jbe	.L156	#,
# memusage.c:661:         catomic_increment (&large);
#APP
# 661 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq large(%rip)	# large
# 0 "" 2
#NO_APP
.L149:
# memusage.c:663:       catomic_increment (&calls_total);
#APP
# 663 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq calls_total(%rip)	# calls_total
# 0 "" 2
# memusage.c:666:       if (result == NULL)
#NO_APP
	testq	%rax, %rax	# <retval>
	je	.L157	#,
# memusage.c:668:       else if (idx == idx_mmap_w)
	cmpl	$5, %ecx	#, iftmp.76_17
	je	.L158	#,
.L144:
# memusage.c:677: }
	addq	$32, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	.cfi_restore_state
# memusage.c:659:         catomic_increment (&histogram[len / 16]);
	movq	%rbx, %rdi	# len, _18
	leaq	histogram(%rip), %rsi	#, tmp114
	shrq	$4, %rdi	#, _18
#APP
# 659 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rsi,%rdi,8)	# histogram
# 0 "" 2
#NO_APP
	jmp	.L149	#
	.p2align 4,,10
	.p2align 3
.L155:
# memusage.c:637:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.71_1
	movq	%r9, 24(%rsp)	# offset, %sfp
	movl	%r8d, 20(%rsp)	# fd, %sfp
	movq	%rdi, 8(%rsp)	# start, %sfp
	je	.L151	#,
# memusage.c:640:       me ();
	call	me	#
	movq	24(%rsp), %r9	# %sfp, offset
	movl	20(%rsp), %r8d	# %sfp, fd
	movq	8(%rsp), %rdi	# %sfp, start
	jmp	.L145	#
	.p2align 4,,10
	.p2align 3
.L157:
# memusage.c:667:         catomic_increment (&failed[idx]);
	leaq	failed(%rip), %rcx	#, tmp116
#APP
# 667 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rcx,%rdx,8)	# failed
# 0 "" 2
#NO_APP
	jmp	.L144	#
	.p2align 4,,10
	.p2align 3
.L158:
# memusage.c:672:         update_data (NULL, len, 0);
	xorl	%edx, %edx	#
	movq	%rbx, %rsi	# len,
	xorl	%edi, %edi	#
	movq	%rax, 8(%rsp)	# <retval>, %sfp
	call	update_data	#
	movq	8(%rsp), %rax	# %sfp, <retval>
	jmp	.L144	#
.L151:
# memusage.c:638:         return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L144	#
	.cfi_endproc
.LFE75:
	.size	mmap64, .-mmap64
	.p2align 4,,15
	.globl	mremap
	.type	mremap, @function
mremap:
.LFB76:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %r12	# start, start
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rsi, %rbp	# old_len, old_len
	movq	%rdx, %rbx	# len, len
	subq	$96, %rsp	#,
	.cfi_def_cfa_offset 128
# memusage.c:688:   va_start (ap, flags);
	leaq	128(%rsp), %rax	#, tmp119
# memusage.c:684: {
	movq	%r8, 80(%rsp)	#,
	xorl	%r8d, %r8d	# iftmp.80_21
# memusage.c:689:   void *newaddr = (flags & MREMAP_FIXED) ? va_arg (ap, void *) : NULL;
	testb	$2, %cl	#, flags
# memusage.c:688:   va_start (ap, flags);
	movl	$32, 24(%rsp)	#, MEM[(struct [1] *)&ap].gp_offset
	movq	%rax, 32(%rsp)	# tmp119, MEM[(struct [1] *)&ap].overflow_arg_area
	leaq	48(%rsp), %rax	#, tmp120
	movq	%rax, 40(%rsp)	# tmp120, MEM[(struct [1] *)&ap].reg_save_area
# memusage.c:689:   void *newaddr = (flags & MREMAP_FIXED) ? va_arg (ap, void *) : NULL;
	je	.L160	#,
	movq	32(%rax), %r8	# MEM[(void * * {ref-all})addr.177_3], iftmp.80_21
	.p2align 4,,10
	.p2align 3
.L160:
# memusage.c:693:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, initialized.81_2
	testl	%eax, %eax	# initialized.81_2
	jle	.L176	#,
.L163:
# memusage.c:702:   result = (*mremapp)(start, old_len, len, flags, newaddr);
	movq	%rbx, %rdx	# len,
	movq	%rbp, %rsi	# old_len,
	movq	%r12, %rdi	# start,
	call	*mremapp(%rip)	# mremapp
# memusage.c:704:   if (!not_me && trace_mmap)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L159	#,
# memusage.c:704:   if (!not_me && trace_mmap)
	cmpb	$0, trace_mmap(%rip)	#, trace_mmap
	je	.L159	#,
# memusage.c:707:       catomic_increment (&calls[idx_mremap]);
#APP
# 707 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 56+calls(%rip)	# calls
# 0 "" 2
# memusage.c:708:       if (len > old_len)
#NO_APP
	cmpq	%rbx, %rbp	# len, old_len
	jnb	.L165	#,
# memusage.c:711:           catomic_add (&total[idx_mremap], len - old_len);
	movq	%rbx, %rdx	# len, _12
	subq	%rbp, %rdx	# old_len, _12
#APP
# 711 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rdx, 56+total(%rip)	# _12, total
# 0 "" 2
# memusage.c:713:           catomic_add (&grand_total, len - old_len);
# 713 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rdx, grand_total(%rip)	# _12, grand_total
# 0 "" 2
#NO_APP
.L165:
# memusage.c:716:       if (len < 65536)
	cmpq	$65535, %rbx	#, len
	jbe	.L177	#,
# memusage.c:719:         catomic_increment (&large);
#APP
# 719 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq large(%rip)	# large
# 0 "" 2
#NO_APP
.L167:
# memusage.c:721:       catomic_increment (&calls_total);
#APP
# 721 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq calls_total(%rip)	# calls_total
# 0 "" 2
# memusage.c:724:       if (result == NULL)
#NO_APP
	testq	%rax, %rax	# <retval>
	je	.L178	#,
# memusage.c:729:           if (start == result)
	cmpq	%rax, %r12	# <retval>, start
	je	.L179	#,
.L169:
# memusage.c:732:           if (old_len > len)
	cmpq	%rbx, %rbp	# len, old_len
	jbe	.L170	#,
# memusage.c:733:             catomic_increment (&decreasing_mremap);
#APP
# 733 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq decreasing_mremap(%rip)	# decreasing_mremap
# 0 "" 2
#NO_APP
.L170:
# memusage.c:738:           update_data (NULL, len, old_len);
	movq	%rbp, %rdx	# old_len,
	movq	%rbx, %rsi	# len,
	xorl	%edi, %edi	#
	movq	%rax, (%rsp)	# <retval>, %sfp
	call	update_data	#
	movq	(%rsp), %rax	# %sfp, <retval>
.L159:
# memusage.c:744: }
	addq	$96, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L177:
	.cfi_restore_state
# memusage.c:717:         catomic_increment (&histogram[len / 16]);
	movq	%rbx, %rcx	# len, _23
	leaq	histogram(%rip), %rdx	#, tmp114
	shrq	$4, %rcx	#, _23
#APP
# 717 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq (%rdx,%rcx,8)	# histogram
# 0 "" 2
#NO_APP
	jmp	.L167	#
	.p2align 4,,10
	.p2align 3
.L176:
# memusage.c:695:       if (initialized == -1)
	cmpl	$-1, %eax	#, initialized.81_2
	movl	%ecx, 12(%rsp)	# flags, %sfp
	movq	%r8, (%rsp)	# iftmp.80_21, %sfp
	je	.L172	#,
# memusage.c:698:       me ();
	call	me	#
	movl	12(%rsp), %ecx	# %sfp, flags
	movq	(%rsp), %r8	# %sfp, iftmp.80_21
	jmp	.L163	#
	.p2align 4,,10
	.p2align 3
.L178:
# memusage.c:725:         catomic_increment (&failed[idx_mremap]);
#APP
# 725 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 56+failed(%rip)	# failed
# 0 "" 2
#NO_APP
	jmp	.L159	#
	.p2align 4,,10
	.p2align 3
.L179:
# memusage.c:730:             catomic_increment (&inplace_mremap);
#APP
# 730 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq inplace_mremap(%rip)	# inplace_mremap
# 0 "" 2
#NO_APP
	jmp	.L169	#
.L172:
# memusage.c:696:         return NULL;
	xorl	%eax, %eax	# <retval>
	jmp	.L159	#
	.cfi_endproc
.LFE76:
	.size	mremap, .-mremap
	.p2align 4,,15
	.globl	munmap
	.type	munmap, @function
munmap:
.LFB77:
	.cfi_startproc
	pushq	%rbx	#
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	movq	%rsi, %rbx	# len, len
	subq	$16, %rsp	#,
	.cfi_def_cfa_offset 32
# memusage.c:754:   if (__glibc_unlikely (initialized <= 0))
	movl	initialized(%rip), %eax	# initialized, <retval>
	testl	%eax, %eax	# <retval>
	jle	.L188	#,
.L181:
# memusage.c:763:   result = (*munmapp)(start, len);
	movq	%rbx, %rsi	# len,
	call	*munmapp(%rip)	# munmapp
# memusage.c:765:   if (!not_me && trace_mmap)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L180	#,
# memusage.c:765:   if (!not_me && trace_mmap)
	cmpb	$0, trace_mmap(%rip)	#, trace_mmap
	je	.L180	#,
# memusage.c:768:       catomic_increment (&calls[idx_munmap]);
#APP
# 768 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 64+calls(%rip)	# calls
# 0 "" 2
# memusage.c:770:       if (__glibc_likely (result == 0))
#NO_APP
	testl	%eax, %eax	# <retval>
	jne	.L183	#,
	movl	%eax, 8(%rsp)	# <retval>, %sfp
# memusage.c:773:           catomic_add (&total[idx_munmap], len);
#APP
# 773 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	addq %rbx, 64+total(%rip)	# len, total
# 0 "" 2
# memusage.c:777:           update_data (NULL, 0, len);
#NO_APP
	xorl	%esi, %esi	#
	movq	%rbx, %rdx	# len,
	xorl	%edi, %edi	#
	call	update_data	#
	movl	8(%rsp), %eax	# %sfp, <retval>
.L180:
# memusage.c:784: }
	addq	$16, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 16
	popq	%rbx	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L188:
	.cfi_restore_state
# memusage.c:756:       if (initialized == -1)
	cmpl	$-1, %eax	#, <retval>
	movq	%rdi, 8(%rsp)	# start, %sfp
	je	.L180	#,
# memusage.c:759:       me ();
	call	me	#
	movq	8(%rsp), %rdi	# %sfp, start
	jmp	.L181	#
	.p2align 4,,10
	.p2align 3
.L183:
# memusage.c:780:         catomic_increment (&failed[idx_munmap]);
#APP
# 780 "memusage.c" 1
	cmpl $0, %fs:24	#
	je 0f
	lock
0:	incq 64+failed(%rip)	# failed
# 0 "" 2
#NO_APP
	jmp	.L180	#
	.cfi_endproc
.LFE77:
	.size	munmap, .-munmap
	.section	.rodata.str1.1
.LC15:
	.string	"\033[01;41m"
.LC16:
	.string	""
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC17:
	.ascii	"\n\033[01;32mMemory usage summary:\033[0;0m heap total: %llu"
	.ascii	", heap peak: %lu, stack peak: %lu\n\033[04;34m         total"
	.ascii	" calls   total m"
	.string	"emory   failed calls\033[0m\n\033[00;34m malloc|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34mrealloc|\033[0m %10lu   %12llu   %s%12lu\033[00;00m  (nomove:%ld, dec:%ld, free:%ld)\n\033[00;34m calloc|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34m   free|\033[0m %10lu   %12llu\n"
	.align 8
.LC18:
	.ascii	"\033[00;34mmmap(r)|\033[0m %10lu   %12llu   "
	.string	"%s%12lu\033[00;00m\n\033[00;34mmmap(w)|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34mmmap(a)|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n\033[00;34m mremap|\033[0m %10lu   %12llu   %s%12lu\033[00;00m  (nomove: %ld, dec:%ld)\n\033[00;34m munmap|\033[0m %10lu   %12llu   %s%12lu\033[00;00m\n"
	.align 8
.LC19:
	.string	"\033[01;32mHistogram for block sizes:\033[0;0m\n"
	.section	.rodata.str1.1
.LC20:
	.string	"%5d-%-5d%12lu "
.LC21:
	.string	" <1% \033[41;37m"
.LC22:
	.string	"%3d%% \033[41;37m"
.LC23:
	.string	"\033[0;0m\n"
.LC24:
	.string	"   large   %12lu "
	.section	.text.exit,"ax",@progbits
	.p2align 4,,15
	.type	dest, @function
dest:
.LFB78:
	.cfi_startproc
# memusage.c:796:   if (not_me)
	cmpb	$0, not_me(%rip)	#, not_me
	jne	.L237	#,
# memusage.c:791: {
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 64
# memusage.c:803:   if (fd != -1)
	movl	fd(%rip), %edi	# fd, fd.93_2
# memusage.c:800:   not_me = true;
	movb	$1, not_me(%rip)	#, not_me
# memusage.c:803:   if (fd != -1)
	cmpl	$-1, %edi	#, fd.93_2
	je	.L191	#,
# memusage.c:806:       if (buffer_cnt > buffer_size)
	movl	buffer_cnt(%rip), %eax	# buffer_cnt, _4
	movq	buffer_size(%rip), %rcx	# buffer_size, buffer_size.95_5
	cmpq	%rcx, %rax	# buffer_size.95_5, _4
	ja	.L240	#,
# memusage.c:810:         write (fd, buffer, buffer_cnt * sizeof (struct entry));
	imulq	$24, %rax, %rdx	#, _4, tmp204
	leaq	buffer(%rip), %rsi	#,
	call	write@PLT	#
.L193:
# memusage.c:814:       lseek (fd, 0, SEEK_SET);
	movl	fd(%rip), %edi	# fd,
	xorl	%edx, %edx	#
	xorl	%esi, %esi	#
	call	lseek@PLT	#
# memusage.c:816:       first.stack = peak_total;
	movq	16+peak_use(%rip), %rax	# peak_use, peak_use
# memusage.c:817:       write (fd, &first, sizeof (struct entry));
	movl	fd(%rip), %edi	# fd,
	leaq	first(%rip), %rsi	#,
	movl	$24, %edx	#,
# memusage.c:816:       first.stack = peak_total;
	movq	%rax, 8+first(%rip)	# peak_use, first.stack
# memusage.c:817:       write (fd, &first, sizeof (struct entry));
	call	write@PLT	#
# memusage.c:820:       first.heap = peak_heap;
	movq	peak_use(%rip), %rax	# peak_use, peak_use
# memusage.c:823:       write (fd, &first, sizeof (struct entry));
	movl	fd(%rip), %edi	# fd,
	leaq	first(%rip), %rsi	#,
# memusage.c:820:       first.heap = peak_heap;
	movq	%rax, first(%rip)	# peak_use, first.heap
# memusage.c:821:       first.stack = peak_stack;
	movq	8+peak_use(%rip), %rax	# peak_use, peak_use
	movq	%rax, 8+first(%rip)	# peak_use, first.stack
# memusage.c:822:       GETTIME (first.time_low, first.time_high);
#APP
# 822 "memusage.c" 1
	rdtsc
# 0 "" 2
#NO_APP
	movl	%edx, 20+first(%rip)	# tmp218, first.time_high
# memusage.c:823:       write (fd, &first, sizeof (struct entry));
	movl	$24, %edx	#,
# memusage.c:822:       GETTIME (first.time_low, first.time_high);
	movl	%eax, 16+first(%rip)	# tmp216, first.time_low
# memusage.c:823:       write (fd, &first, sizeof (struct entry));
	call	write@PLT	#
# memusage.c:826:       close (fd);
	movl	fd(%rip), %edi	# fd,
	call	close@PLT	#
# memusage.c:827:       fd = -1;
	movl	$-1, fd(%rip)	#, fd
.L191:
# memusage.c:854:            (unsigned long int) failed[idx_calloc],
	movq	16+failed(%rip), %rcx	# failed, _21
# memusage.c:847:            (unsigned long int) failed[idx_realloc],
	movq	8+failed(%rip), %rdx	# failed, _27
# memusage.c:831:   fprintf (stderr, "\n\
	leaq	.LC16(%rip), %rbx	#, tmp312
# memusage.c:843:            (unsigned long int) failed[idx_malloc],
	movq	failed(%rip), %rax	# failed, _30
# memusage.c:831:   fprintf (stderr, "\n\
	leaq	.LC15(%rip), %rbp	#, tmp311
	movq	stderr@GOTPCREL(%rip), %r13	#, tmp306
	movq	%rbx, %r8	# tmp312, iftmp.106_91
	movq	%rbx, %rdi	# tmp312, iftmp.110_92
	movq	%rbx, %rsi	# tmp312, iftmp.111_93
	testq	%rcx, %rcx	# _21
	pushq	24+total(%rip)	# total
	.cfi_def_cfa_offset 72
	movq	calls(%rip), %r9	# calls,
	cmovne	%rbp, %r8	# tmp311,, iftmp.106_91
	testq	%rdx, %rdx	# _27
	pushq	24+calls(%rip)	# calls
	.cfi_def_cfa_offset 80
	cmovne	%rbp, %rdi	# tmp311,, iftmp.110_92
	testq	%rax, %rax	# _30
	pushq	%rcx	# _21
	.cfi_def_cfa_offset 88
	cmovne	%rbp, %rsi	# tmp311,, iftmp.111_93
	pushq	%r8	# iftmp.106_91
	.cfi_def_cfa_offset 96
	movq	8+peak_use(%rip), %r8	# peak_use,
	pushq	16+total(%rip)	# total
	.cfi_def_cfa_offset 104
	pushq	16+calls(%rip)	# calls
	.cfi_def_cfa_offset 112
	pushq	realloc_free(%rip)	# realloc_free
	.cfi_def_cfa_offset 120
	pushq	decreasing(%rip)	# decreasing
	.cfi_def_cfa_offset 128
	pushq	inplace(%rip)	# inplace
	.cfi_def_cfa_offset 136
	pushq	%rdx	# _27
	.cfi_def_cfa_offset 144
	pushq	%rdi	# iftmp.110_92
	.cfi_def_cfa_offset 152
	pushq	8+total(%rip)	# total
	.cfi_def_cfa_offset 160
	pushq	8+calls(%rip)	# calls
	.cfi_def_cfa_offset 168
	pushq	%rax	# _30
	.cfi_def_cfa_offset 176
	xorl	%eax, %eax	#
	pushq	%rsi	# iftmp.111_93
	.cfi_def_cfa_offset 184
	pushq	total(%rip)	# total
	.cfi_def_cfa_offset 192
	leaq	.LC17(%rip), %rsi	#,
	movq	peak_use(%rip), %rcx	# peak_use,
	movq	grand_total(%rip), %rdx	# grand_total,
	movq	0(%r13), %rdi	# stderr,
	call	fprintf@PLT	#
# memusage.c:858:   if (trace_mmap)
	subq	$-128, %rsp	#,
	.cfi_def_cfa_offset 64
	cmpb	$0, trace_mmap(%rip)	#, trace_mmap
	jne	.L241	#,
.L197:
# memusage.c:889:   fprintf (stderr, "\e[01;32mHistogram for block sizes:\e[0;0m\n");
	movq	0(%r13), %rcx	# stderr,
	leaq	.LC19(%rip), %rdi	#,
	movl	$41, %edx	#,
	movl	$1, %esi	#,
	leaq	histogram(%rip), %r12	#, tmp304
	call	fwrite@PLT	#
# memusage.c:892:   maxcalls = large;
	movq	large(%rip), %rbp	# large, maxcalls
# memusage.c:893:   for (cnt = 0; cnt < 65536; cnt += 16)
	xorl	%eax, %eax	# cnt
	.p2align 4,,10
	.p2align 3
.L203:
# memusage.c:894:     if (histogram[cnt / 16] > maxcalls)
	movl	%eax, %edx	# cnt, tmp258
	sarl	$4, %edx	#, tmp258
	movslq	%edx, %rdx	# tmp258, tmp259
	movq	(%r12,%rdx,8), %rdx	# histogram, tmp260
	cmpq	%rdx, %rbp	# tmp260, maxcalls
	cmovb	%rdx, %rbp	# maxcalls,, tmp260, maxcalls
# memusage.c:893:   for (cnt = 0; cnt < 65536; cnt += 16)
	addl	$16, %eax	#, cnt
	cmpl	$65536, %eax	#, cnt
	jne	.L203	#,
# memusage.c:897:   for (cnt = 0; cnt < 65536; cnt += 16)
	xorl	%ebx, %ebx	# cnt
	jmp	.L209	#
	.p2align 4,,10
	.p2align 3
.L204:
# memusage.c:897:   for (cnt = 0; cnt < 65536; cnt += 16)
	addl	$16, %ebx	#, cnt
	cmpl	$65536, %ebx	#, cnt
	je	.L242	#,
.L209:
# memusage.c:899:     if (histogram[cnt / 16] != 0)
	movl	%ebx, %r9d	# cnt, _59
	sarl	$4, %r9d	#, _59
	movslq	%r9d, %r15	# _59, _59
	movq	(%r12,%r15,8), %r8	# histogram, _60
	testq	%r8, %r8	# _60
	je	.L204	#,
# memusage.c:901:         percent = (histogram[cnt / 16] * 100) / calls_total;
	leaq	(%r8,%r8,4), %rax	#, tmp265
	xorl	%edx, %edx	# tmp271
# memusage.c:902:         fprintf (stderr, "%5d-%-5d%12lu ", cnt, cnt + 15,
	movq	0(%r13), %rdi	# stderr,
	leal	15(%rbx), %ecx	#, tmp272
	leaq	.LC20(%rip), %rsi	#,
# memusage.c:901:         percent = (histogram[cnt / 16] * 100) / calls_total;
	leaq	(%rax,%rax,4), %rax	#, tmp267
	salq	$2, %rax	#, tmp268
	divq	calls_total(%rip)	# calls_total
# memusage.c:902:         fprintf (stderr, "%5d-%-5d%12lu ", cnt, cnt + 15,
	movl	%ebx, %edx	# cnt,
# memusage.c:901:         percent = (histogram[cnt / 16] * 100) / calls_total;
	movq	%rax, %r14	# tmp268, tmp270
# memusage.c:902:         fprintf (stderr, "%5d-%-5d%12lu ", cnt, cnt + 15,
	xorl	%eax, %eax	#
	call	fprintf@PLT	#
# memusage.c:904:         if (percent == 0)
	testl	%r14d, %r14d	# tmp270
	jne	.L205	#,
# memusage.c:905:           fputs (" <1% \e[41;37m", stderr);
	movq	0(%r13), %rcx	# stderr,
	leaq	.LC21(%rip), %rdi	#,
	movl	$13, %edx	#,
	movl	$1, %esi	#,
	call	fwrite@PLT	#
.L206:
# memusage.c:911:         percent = (histogram[cnt / 16] * 50) / maxcalls;
	movq	(%r12,%r15,8), %rax	# histogram, tmp278
	xorl	%edx, %edx	# tmp287
	leaq	(%rax,%rax,4), %rax	#, tmp281
	leaq	(%rax,%rax,4), %rax	#, tmp283
	addq	%rax, %rax	# tmp284
	divq	%rbp	# maxcalls
# memusage.c:912:         while (percent-- > 0)
	testl	%eax, %eax	# tmp286
	leal	-1(%rax), %r14d	#, percent
	jle	.L207	#,
	.p2align 4,,10
	.p2align 3
.L208:
# memusage.c:913:           fputc ('=', stderr);
	movq	0(%r13), %rsi	# stderr,
	movl	$61, %edi	#,
# memusage.c:912:         while (percent-- > 0)
	subl	$1, %r14d	#, percent
# memusage.c:913:           fputc ('=', stderr);
	call	fputc@PLT	#
# memusage.c:912:         while (percent-- > 0)
	cmpl	$-1, %r14d	#, percent
	jne	.L208	#,
.L207:
# memusage.c:914:         fputs ("\e[0;0m\n", stderr);
	movq	0(%r13), %rcx	# stderr,
	leaq	.LC23(%rip), %rdi	#,
	movl	$7, %edx	#,
	movl	$1, %esi	#,
# memusage.c:897:   for (cnt = 0; cnt < 65536; cnt += 16)
	addl	$16, %ebx	#, cnt
# memusage.c:914:         fputs ("\e[0;0m\n", stderr);
	call	fwrite@PLT	#
# memusage.c:897:   for (cnt = 0; cnt < 65536; cnt += 16)
	cmpl	$65536, %ebx	#, cnt
	jne	.L209	#,
.L242:
# memusage.c:917:   if (large != 0)
	movq	large(%rip), %rcx	# large, large.131_73
	testq	%rcx, %rcx	# large.131_73
	jne	.L243	#,
.L210:
# memusage.c:935:   not_me = false;
	movb	$0, not_me(%rip)	#, not_me
# memusage.c:936: }
	popq	%rax	#
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	.cfi_restore_state
# memusage.c:907:           fprintf (stderr, "%3d%% \e[41;37m", percent);
	movq	0(%r13), %rdi	# stderr,
	leaq	.LC22(%rip), %rsi	#,
	movl	%r14d, %edx	# tmp270,
	xorl	%eax, %eax	#
	call	fprintf@PLT	#
	jmp	.L206	#
.L241:
# memusage.c:886:              (unsigned long int) failed[idx_munmap]);
	movq	64+failed(%rip), %rsi	# failed, _38
# memusage.c:880:              (unsigned long int) failed[idx_mremap],
	movq	56+failed(%rip), %rcx	# failed, _43
# memusage.c:859:     fprintf (stderr, "\
	movq	%rbx, %r11	# tmp312, iftmp.115_94
# memusage.c:876:              (unsigned long int) failed[idx_mmap_a],
	movq	48+failed(%rip), %rdx	# failed, _46
# memusage.c:872:              (unsigned long int) failed[idx_mmap_w],
	movq	40+failed(%rip), %rax	# failed, _49
# memusage.c:859:     fprintf (stderr, "\
	movq	%rbx, %r10	# tmp312, iftmp.118_95
# memusage.c:868:              (unsigned long int) failed[idx_mmap_r],
	movq	32+failed(%rip), %r9	# failed, _52
# memusage.c:859:     fprintf (stderr, "\
	movq	%rbx, %r8	# tmp312, iftmp.119_96
	movq	%rbx, %rdi	# tmp312, iftmp.120_97
	testq	%rsi, %rsi	# _38
	pushq	%rsi	# _38
	.cfi_def_cfa_offset 72
	leaq	.LC18(%rip), %rsi	#,
	cmovne	%rbp, %r11	# tmp311,, iftmp.115_94
	testq	%rcx, %rcx	# _43
	cmovne	%rbp, %r10	# tmp311,, iftmp.118_95
	testq	%rdx, %rdx	# _46
	pushq	%r11	# iftmp.115_94
	.cfi_def_cfa_offset 80
	cmovne	%rbp, %r8	# tmp311,, iftmp.119_96
	testq	%rax, %rax	# _49
	pushq	64+total(%rip)	# total
	.cfi_def_cfa_offset 88
	cmovne	%rbp, %rdi	# tmp311,, iftmp.120_97
	pushq	64+calls(%rip)	# calls
	.cfi_def_cfa_offset 96
	testq	%r9, %r9	# _52
	pushq	decreasing_mremap(%rip)	# decreasing_mremap
	.cfi_def_cfa_offset 104
	pushq	inplace_mremap(%rip)	# inplace_mremap
	.cfi_def_cfa_offset 112
	cmovne	%rbp, %rbx	# tmp311,, tmp312
	pushq	%rcx	# _43
	.cfi_def_cfa_offset 120
	pushq	%r10	# iftmp.118_95
	.cfi_def_cfa_offset 128
	pushq	56+total(%rip)	# total
	.cfi_def_cfa_offset 136
	pushq	56+calls(%rip)	# calls
	.cfi_def_cfa_offset 144
	pushq	%rdx	# _46
	.cfi_def_cfa_offset 152
	pushq	%r8	# iftmp.119_96
	.cfi_def_cfa_offset 160
	movq	%rbx, %r8	# tmp312,
	pushq	48+total(%rip)	# total
	.cfi_def_cfa_offset 168
	pushq	48+calls(%rip)	# calls
	.cfi_def_cfa_offset 176
	pushq	%rax	# _49
	.cfi_def_cfa_offset 184
	pushq	%rdi	# iftmp.120_97
	.cfi_def_cfa_offset 192
	xorl	%eax, %eax	#
	pushq	40+total(%rip)	# total
	.cfi_def_cfa_offset 200
	pushq	40+calls(%rip)	# calls
	.cfi_def_cfa_offset 208
	movq	32+total(%rip), %rcx	# total,
	movq	32+calls(%rip), %rdx	# calls,
	movq	0(%r13), %rdi	# stderr,
	call	fprintf@PLT	#
	addq	$144, %rsp	#,
	.cfi_def_cfa_offset 64
	jmp	.L197	#
.L237:
	.cfi_def_cfa_offset 8
	.cfi_restore 3
	.cfi_restore 6
	.cfi_restore 12
	.cfi_restore 13
	.cfi_restore 14
	.cfi_restore 15
	ret
.L240:
	.cfi_def_cfa_offset 64
	.cfi_offset 3, -56
	.cfi_offset 6, -48
	.cfi_offset 12, -40
	.cfi_offset 13, -32
	.cfi_offset 14, -24
	.cfi_offset 15, -16
# memusage.c:808:                (buffer_cnt - buffer_size) * sizeof (struct entry));
	subq	%rcx, %rax	# buffer_size.95_5, tmp199
# memusage.c:807:         write (fd, buffer + buffer_size,
	leaq	buffer(%rip), %rsi	#, tmp203
	imulq	$24, %rcx, %rcx	#, buffer_size.95_5, tmp201
	imulq	$24, %rax, %rdx	#, tmp199, tmp200
	addq	%rcx, %rsi	# tmp201, tmp202
	call	write@PLT	#
	jmp	.L193	#
.L243:
# memusage.c:919:       percent = (large * 100) / calls_total;
	imulq	$100, %rcx, %rax	#, large.131_73, tmp290
	xorl	%edx, %edx	# tmp293
# memusage.c:920:       fprintf (stderr, "   large   %12lu ", (unsigned long int) large);
	movq	0(%r13), %rdi	# stderr,
	leaq	.LC24(%rip), %rsi	#,
# memusage.c:919:       percent = (large * 100) / calls_total;
	divq	calls_total(%rip)	# calls_total
# memusage.c:920:       fprintf (stderr, "   large   %12lu ", (unsigned long int) large);
	movq	%rcx, %rdx	# large.131_73,
# memusage.c:919:       percent = (large * 100) / calls_total;
	movq	%rax, %rbx	# tmp290, tmp292
# memusage.c:920:       fprintf (stderr, "   large   %12lu ", (unsigned long int) large);
	xorl	%eax, %eax	#
	call	fprintf@PLT	#
# memusage.c:921:       if (percent == 0)
	testl	%ebx, %ebx	# tmp292
	jne	.L211	#,
# memusage.c:922:         fputs (" <1% \e[41;37m", stderr);
	movq	0(%r13), %rcx	# stderr,
	leaq	.LC21(%rip), %rdi	#,
	movl	$13, %edx	#,
	movl	$1, %esi	#,
	call	fwrite@PLT	#
.L212:
# memusage.c:925:       percent = (large * 50) / maxcalls;
	imulq	$50, large(%rip), %rax	#, large, tmp297
	xorl	%edx, %edx	# tmp301
	divq	%rbp	# maxcalls
# memusage.c:926:       while (percent-- > 0)
	testl	%eax, %eax	# tmp300
	leal	-1(%rax), %ebx	#, percent
	jle	.L213	#,
	.p2align 4,,10
	.p2align 3
.L214:
# memusage.c:927:         fputc ('=', stderr);
	movq	0(%r13), %rsi	# stderr,
	movl	$61, %edi	#,
# memusage.c:926:       while (percent-- > 0)
	subl	$1, %ebx	#, percent
# memusage.c:927:         fputc ('=', stderr);
	call	fputc@PLT	#
# memusage.c:926:       while (percent-- > 0)
	cmpl	$-1, %ebx	#, percent
	jne	.L214	#,
.L213:
# memusage.c:928:       fputs ("\e[0;0m\n", stderr);
	movq	0(%r13), %rcx	# stderr,
	leaq	.LC23(%rip), %rdi	#,
	movl	$7, %edx	#,
	movl	$1, %esi	#,
	call	fwrite@PLT	#
	jmp	.L210	#
.L211:
# memusage.c:924:         fprintf (stderr, "%3d%% \e[41;37m", percent);
	movq	0(%r13), %rdi	# stderr,
	leaq	.LC22(%rip), %rsi	#,
	movl	%ebx, %edx	# tmp292,
	xorl	%eax, %eax	#
	call	fprintf@PLT	#
	jmp	.L212	#
	.cfi_endproc
.LFE78:
	.size	dest, .-dest
	.section	.dtors,"aw",@progbits
	.align 8
	.quad	dest
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.10129, @object
	.size	__PRETTY_FUNCTION__.10129, 12
__PRETTY_FUNCTION__.10129:
	.string	"update_data"
	.local	first
	.comm	first,24,16
	.local	buffer_cnt
	.comm	buffer_cnt,4,4
	.local	buffer
	.comm	buffer,1572864,32
	.local	trace_mmap
	.comm	trace_mmap,1,1
	.local	initialized
	.comm	initialized,4,4
	.local	not_me
	.comm	not_me,1,1
	.data
	.align 4
	.type	fd, @object
	.size	fd, 4
fd:
	.long	-1
	.local	buffer_size
	.comm	buffer_size,8,8
	.section	.tbss,"awT",@nobits
	.align 8
	.type	start_sp, @object
	.size	start_sp, 8
start_sp:
	.zero	8
	.local	peak_use
	.comm	peak_use,24,16
	.local	current_heap
	.comm	current_heap,8,8
	.local	decreasing_mremap
	.comm	decreasing_mremap,8,8
	.local	inplace_mremap
	.comm	inplace_mremap,8,8
	.local	realloc_free
	.comm	realloc_free,8,8
	.local	decreasing
	.comm	decreasing,8,8
	.local	inplace
	.comm	inplace,8,8
	.local	calls_total
	.comm	calls_total,8,8
	.local	large
	.comm	large,8,8
	.local	histogram
	.comm	histogram,32768,32
	.local	grand_total
	.comm	grand_total,8,8
	.local	total
	.comm	total,72,32
	.local	failed
	.comm	failed,72,32
	.local	calls
	.comm	calls,72,32
	.local	mremapp
	.comm	mremapp,8,8
	.local	munmapp
	.comm	munmapp,8,8
	.local	mmap64p
	.comm	mmap64p,8,8
	.local	mmapp
	.comm	mmapp,8,8
	.local	freep
	.comm	freep,8,8
	.local	callocp
	.comm	callocp,8,8
	.local	reallocp
	.comm	reallocp,8,8
	.local	mallocp
	.comm	mallocp,8,8
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
