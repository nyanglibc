	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __malloc_initialize_hook,__malloc_initialize_hook@GLIBC_2.2.5
	.symver malloc_get_state,malloc_get_state@GLIBC_2.2.5
	.symver malloc_set_state,malloc_set_state@GLIBC_2.2.5
	.symver __libc_free,cfree@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_dl_tunable_set_mmap_threshold
	.type	_dl_tunable_set_mmap_threshold, @function
_dl_tunable_set_mmap_threshold:
	movq	(%rdi), %rax
	cmpq	$33554432, %rax
	ja	.L1
	movq	%rax, 16+mp_(%rip)
	movl	$1, 52+mp_(%rip)
.L1:
	rep ret
	.size	_dl_tunable_set_mmap_threshold, .-_dl_tunable_set_mmap_threshold
	.p2align 4,,15
	.globl	_dl_tunable_set_mmaps_max
	.type	_dl_tunable_set_mmaps_max, @function
_dl_tunable_set_mmaps_max:
	movq	(%rdi), %rax
	movl	$1, 52+mp_(%rip)
	movl	%eax, 44+mp_(%rip)
	ret
	.size	_dl_tunable_set_mmaps_max, .-_dl_tunable_set_mmaps_max
	.p2align 4,,15
	.globl	_dl_tunable_set_top_pad
	.type	_dl_tunable_set_top_pad, @function
_dl_tunable_set_top_pad:
	movq	(%rdi), %rax
	movl	$1, 52+mp_(%rip)
	movq	%rax, 8+mp_(%rip)
	ret
	.size	_dl_tunable_set_top_pad, .-_dl_tunable_set_top_pad
	.p2align 4,,15
	.globl	_dl_tunable_set_perturb_byte
	.type	_dl_tunable_set_perturb_byte, @function
_dl_tunable_set_perturb_byte:
	movq	(%rdi), %rax
	movl	%eax, perturb_byte(%rip)
	ret
	.size	_dl_tunable_set_perturb_byte, .-_dl_tunable_set_perturb_byte
	.p2align 4,,15
	.globl	_dl_tunable_set_trim_threshold
	.type	_dl_tunable_set_trim_threshold, @function
_dl_tunable_set_trim_threshold:
	movq	(%rdi), %rax
	movl	$1, 52+mp_(%rip)
	movq	%rax, mp_(%rip)
	ret
	.size	_dl_tunable_set_trim_threshold, .-_dl_tunable_set_trim_threshold
	.p2align 4,,15
	.globl	_dl_tunable_set_arena_max
	.type	_dl_tunable_set_arena_max, @function
_dl_tunable_set_arena_max:
	movq	(%rdi), %rax
	movq	%rax, 32+mp_(%rip)
	ret
	.size	_dl_tunable_set_arena_max, .-_dl_tunable_set_arena_max
	.p2align 4,,15
	.globl	_dl_tunable_set_arena_test
	.type	_dl_tunable_set_arena_test, @function
_dl_tunable_set_arena_test:
	movq	(%rdi), %rax
	movq	%rax, 24+mp_(%rip)
	ret
	.size	_dl_tunable_set_arena_test, .-_dl_tunable_set_arena_test
	.p2align 4,,15
	.globl	_dl_tunable_set_tcache_max
	.type	_dl_tunable_set_tcache_max, @function
_dl_tunable_set_tcache_max:
	movq	(%rdi), %rax
	cmpq	$1032, %rax
	ja	.L10
	movq	%rax, 88+mp_(%rip)
	addq	$23, %rax
	movl	$1, %edx
	cmpq	$31, %rax
	jbe	.L12
	andq	$-16, %rax
	subq	$17, %rax
	shrq	$4, %rax
	leaq	1(%rax), %rdx
.L12:
	movq	%rdx, 80+mp_(%rip)
.L10:
	rep ret
	.size	_dl_tunable_set_tcache_max, .-_dl_tunable_set_tcache_max
	.p2align 4,,15
	.globl	_dl_tunable_set_tcache_count
	.type	_dl_tunable_set_tcache_count, @function
_dl_tunable_set_tcache_count:
	movq	(%rdi), %rax
	cmpq	$65535, %rax
	ja	.L14
	movq	%rax, 96+mp_(%rip)
.L14:
	rep ret
	.size	_dl_tunable_set_tcache_count, .-_dl_tunable_set_tcache_count
	.p2align 4,,15
	.globl	_dl_tunable_set_tcache_unsorted_limit
	.type	_dl_tunable_set_tcache_unsorted_limit, @function
_dl_tunable_set_tcache_unsorted_limit:
	movq	(%rdi), %rax
	movq	%rax, 104+mp_(%rip)
	ret
	.size	_dl_tunable_set_tcache_unsorted_limit, .-_dl_tunable_set_tcache_unsorted_limit
	.p2align 4,,15
	.globl	_dl_tunable_set_mxfast
	.type	_dl_tunable_set_mxfast, @function
_dl_tunable_set_mxfast:
	movq	(%rdi), %rax
	cmpq	$160, %rax
	ja	.L17
	cmpq	$7, %rax
	movl	$16, %edx
	jbe	.L19
	leaq	8(%rax), %rdx
	andq	$-16, %rdx
.L19:
	movq	%rdx, global_max_fast(%rip)
.L17:
	rep ret
	.size	_dl_tunable_set_mxfast, .-_dl_tunable_set_mxfast
	.p2align 4,,15
	.type	__failing_morecore, @function
__failing_morecore:
	xorl	%eax, %eax
	ret
	.size	__failing_morecore, .-__failing_morecore
	.p2align 4,,15
	.type	mem2mem_check, @function
mem2mem_check:
	testq	%rdi, %rdi
	movq	%rdi, %rax
	je	.L23
	leaq	-16(%rdi), %rdx
	movq	%rdx, %rdi
	shrq	$11, %rdx
	shrq	$3, %rdi
	xorl	%edx, %edi
	movl	$2, %edx
	cmpb	$1, %dil
	cmove	%edx, %edi
	movq	-8(%rax), %rdx
	movq	%rdx, %rcx
	andq	$-8, %rcx
	andl	$2, %edx
	sete	%dl
	movzbl	%dl, %edx
	leaq	-17(%rcx,%rdx,8), %rcx
	cmpq	%rsi, %rcx
	jbe	.L26
	movzbl	%dil, %r8d
	movl	$255, %r10d
	leaq	-1(%r8), %r9
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	cmpq	$255, %rdx
	cmova	%r10, %rdx
	cmpq	%r8, %rdx
	cmove	%r9, %rdx
	movb	%dl, (%rax,%rcx)
	subq	%rdx, %rcx
	cmpq	%rcx, %rsi
	jb	.L28
.L26:
	movb	%dil, (%rax,%rsi)
.L23:
	rep ret
	.size	mem2mem_check, .-mem2mem_check
	.p2align 4,,15
	.type	mem2chunk_check, @function
mem2chunk_check:
	testb	$15, %dil
	jne	.L70
	leaq	-16(%rdi), %rax
	movq	-8(%rdi), %r9
	movq	%rax, %r8
	movq	%rax, %rdx
	shrq	$11, %rdx
	shrq	$3, %r8
	movq	%r9, %rcx
	xorl	%edx, %r8d
	andq	$-8, %rcx
	movl	$2, %edx
	cmpb	$1, %r8b
	cmove	%edx, %r8d
	testb	$2, %r9b
	jne	.L38
	movl	4+main_arena(%rip), %edx
	andl	$2, %edx
	jne	.L39
	movq	72+mp_(%rip), %r10
	cmpq	%rax, %r10
	ja	.L70
	addq	2184+main_arena(%rip), %r10
	leaq	(%rax,%rcx), %r11
	cmpq	%r10, %r11
	jnb	.L70
.L39:
	cmpq	$31, %rcx
	jbe	.L70
	testb	$8, %r9b
	jne	.L70
	testb	$1, -8(%rdi,%rcx)
	je	.L70
	andl	$1, %r9d
	je	.L90
.L40:
	leaq	7(%rcx), %rdi
	leaq	(%rax,%rdi), %rcx
	movzbl	(%rcx), %edx
	cmpb	%dl, %r8b
	je	.L42
	testq	%rdx, %rdx
	je	.L70
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rdi
	jnb	.L43
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L44:
	testq	%rdx, %rdx
	je	.L70
	leaq	16(%rdx), %rcx
	cmpq	%rdi, %rcx
	ja	.L70
.L43:
	subq	%rdx, %rdi
	leaq	(%rax,%rdi), %rcx
	movzbl	(%rcx), %edx
	cmpb	%r8b, %dl
	jne	.L44
.L42:
	movl	%r8d, %edx
	testq	%rsi, %rsi
	notl	%edx
	movb	%dl, (%rcx)
	je	.L35
	movq	%rcx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L90:
	movq	-16(%rdi), %rdi
	testb	$15, %dil
	jne	.L70
	movq	%rax, %r9
	subq	%rdi, %r9
	testl	%edx, %edx
	jne	.L41
	cmpq	%r9, 72+mp_(%rip)
	ja	.L70
.L41:
	movq	8(%r9), %rdx
	andq	$-8, %rdx
	cmpq	%rdx, %rdi
	je	.L40
	.p2align 4,,10
	.p2align 3
.L70:
	xorl	%eax, %eax
.L35:
	rep ret
	.p2align 4,,10
	.p2align 3
.L38:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	movq	%rdi, %r10
	movq	24(%rdx), %rdx
	subq	$1, %rdx
	andq	%rdx, %r10
	leaq	-16(%r10), %r11
	testq	$-17, %r11
	je	.L45
	leaq	-1(%r10), %r11
	cmpq	$8190, %r11
	ja	.L45
	leaq	-64(%r10), %r11
	testq	$-65, %r11
	jne	.L91
.L45:
	andl	$3, %r9d
	cmpq	$2, %r9
	jne	.L70
	movq	-16(%rdi), %rdi
	movq	%rax, %r9
	subq	%rdi, %r9
	addq	%rcx, %rdi
	orq	%r9, %rdi
	testq	%rdx, %rdi
	jne	.L70
	leaq	-1(%rcx), %rdx
	leaq	(%rax,%rdx), %rcx
	movzbl	(%rcx), %edi
	cmpb	%dil, %r8b
	je	.L42
	testq	%rdi, %rdi
	je	.L70
	leaq	16(%rdi), %rcx
	cmpq	%rcx, %rdx
	jnb	.L46
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L47:
	testq	%rdi, %rdi
	je	.L70
	leaq	16(%rdi), %rcx
	cmpq	%rdx, %rcx
	ja	.L70
.L46:
	subq	%rdi, %rdx
	leaq	(%rax,%rdx), %rcx
	movzbl	(%rcx), %edi
	cmpb	%r8b, %dil
	jne	.L47
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	-256(%r10), %r11
	testq	$-257, %r11
	je	.L45
	leaq	-1024(%r10), %r11
	testq	$-1025, %r11
	je	.L45
	cmpq	$4096, %r10
	je	.L45
	xorl	%eax, %eax
	jmp	.L35
	.size	mem2chunk_check, .-mem2chunk_check
	.p2align 4,,15
	.type	malloc_init_state, @function
malloc_init_state:
	leaq	96(%rdi), %rcx
	leaq	2128(%rdi), %rdx
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L93:
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L93
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rdi
	je	.L94
	orl	$2, 4(%rdi)
.L95:
	movl	$0, 8(%rdi)
	movq	%rcx, 96(%rdi)
	ret
.L94:
	movq	$128, global_max_fast(%rip)
	jmp	.L95
	.size	malloc_init_state, .-malloc_init_state
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s\n"
	.text
	.p2align 4,,15
	.type	malloc_printerr, @function
malloc_printerr:
	leaq	.LC0(%rip), %rsi
	movq	%rdi, %rdx
	subq	$8, %rsp
	movl	$1, %edi
	xorl	%eax, %eax
	call	__libc_message
	.size	malloc_printerr, .-malloc_printerr
	.section	.rodata.str1.1
.LC1:
	.string	"malloc: top chunk is corrupt"
	.text
	.p2align 4,,15
	.type	top_check, @function
top_check:
	movq	96+main_arena(%rip), %rax
	leaq	96+main_arena(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L100
	movq	8(%rax), %rdx
	testb	$2, %dl
	jne	.L102
	movq	%rdx, %rcx
	andq	$-8, %rcx
	cmpq	$31, %rcx
	jbe	.L102
	andl	$1, %edx
	je	.L102
	testb	$2, 4+main_arena(%rip)
	jne	.L100
	movq	2184+main_arena(%rip), %rdx
	addq	72+mp_(%rip), %rdx
	addq	%rcx, %rax
	cmpq	%rdx, %rax
	jne	.L102
.L100:
	rep ret
.L102:
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	call	malloc_printerr
	.size	top_check, .-top_check
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"int_mallinfo(): unaligned fastbin chunk detected"
	.text
	.p2align 4,,15
	.type	int_mallinfo, @function
int_mallinfo:
	pushq	%rbp
	pushq	%rbx
	leaq	16(%rdi), %r8
	leaq	96(%rdi), %r9
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	subq	$8, %rsp
.L113:
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L112
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L111:
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	addl	$1, %r11d
	andq	$-8, %rdx
	addq	%rdx, %r10
	leaq	16(%rax), %rdx
	shrq	$12, %rdx
	movq	%rdx, %rax
	xorq	%rcx, %rax
	cmpq	%rcx, %rdx
	je	.L109
.L112:
	testb	$15, %al
	je	.L111
	leaq	.LC2(%rip), %rdi
	call	malloc_printerr
.L109:
	addq	$8, %r8
	cmpq	%r8, %r9
	jne	.L113
	movq	96(%rdi), %rax
	leaq	2128(%rdi), %rbx
	movl	$1, %ecx
	movq	8(%rax), %rbp
	andq	$-8, %rbp
	leaq	(%r10,%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L116:
	movq	24(%r9), %rax
	cmpq	%rax, %r9
	je	.L114
	.p2align 4,,10
	.p2align 3
.L115:
	movq	8(%rax), %rdx
	movq	24(%rax), %rax
	addl	$1, %ecx
	andq	$-8, %rdx
	addq	%rdx, %r8
	cmpq	%rax, %r9
	jne	.L115
.L114:
	addq	$16, %r9
	cmpq	%r9, %rbx
	jne	.L116
	movq	2184(%rdi), %rax
	addq	%rax, (%rsi)
	movslq	%r11d, %r11
	movslq	%ecx, %rcx
	addq	%r11, 16(%rsi)
	addq	%rcx, 8(%rsi)
	movq	%rax, %rdx
	leaq	main_arena(%rip), %rax
	addq	%r8, 64(%rsi)
	subq	%r8, %rdx
	addq	%r10, 48(%rsi)
	addq	%rdx, 56(%rsi)
	cmpq	%rax, %rdi
	jne	.L108
	movslq	40+mp_(%rip), %rax
	movq	%rbp, 72(%rsi)
	movq	%rax, 24(%rsi)
	movq	56+mp_(%rip), %rax
	movq	$0, 40(%rsi)
	movq	%rax, 32(%rsi)
.L108:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	int_mallinfo, .-int_mallinfo
	.section	.rodata.str1.1
.LC3:
	.string	": "
.LC4:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"%s%s%s:%u: %s%sAssertion `%s' failed.\n"
	.text
	.p2align 4,,15
	.type	__malloc_assert, @function
__malloc_assert:
	subq	$16, %rsp
	movq	__progname@GOTPCREL(%rip), %rax
	movl	%edx, %r9d
	leaq	.LC3(%rip), %r10
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rsi
	movq	(%rax), %rdx
	leaq	.LC4(%rip), %rax
	cmpb	$0, (%rdx)
	pushq	%rdi
	pushq	%r10
	pushq	%rcx
	cmovne	%r10, %rax
	xorl	%edi, %edi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fxprintf
	movq	stderr@GOTPCREL(%rip), %rax
	addq	$32, %rsp
	movq	(%rax), %rdi
	call	__GI_fflush
	call	__GI_abort
	.size	__malloc_assert, .-__malloc_assert
	.p2align 4,,15
	.type	new_heap, @function
new_heap:
	pushq	%r13
	pushq	%r12
	addq	%rdi, %rsi
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	cmpq	$32767, %rsi
	movq	24(%rax), %rbp
	jbe	.L144
	cmpq	$67108864, %rsi
	ja	.L161
.L133:
	movq	aligned_heap_area(%rip), %rdi
	leaq	-1(%rsi,%rbp), %rax
	negq	%rbp
	andq	%rax, %rbp
	testq	%rdi, %rdi
	je	.L136
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	$-1, %r8d
	movl	$16418, %ecx
	movl	$67108864, %esi
	call	__GI___mmap
	cmpq	$-1, %rax
	movq	%rax, %rbx
	movq	$0, aligned_heap_area(%rip)
	je	.L136
	testl	$67108863, %eax
	jne	.L162
.L138:
	movl	$3, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI___mprotect
	testl	%eax, %eax
	jne	.L159
	movq	%rbp, 16(%rbx)
	movq	%rbp, 24(%rbx)
.L132:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	movl	$67108864, %esi
	movq	%rax, %rdi
	call	__GI___munmap
.L136:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$16418, %ecx
	movl	$134217728, %esi
	call	__GI___mmap
	cmpq	$-1, %rax
	je	.L139
	leaq	67108863(%rax), %rbx
	andq	$-67108864, %rbx
	movq	%rbx, %r13
	leaq	67108864(%rbx), %r12
	subq	%rax, %r13
	jne	.L163
	movq	%r12, aligned_heap_area(%rip)
.L141:
	movl	$67108864, %esi
	movq	%r12, %rdi
	subq	%r13, %rsi
	call	__GI___munmap
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$32768, %esi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L161:
	cmpq	$67108864, %rdi
	movl	$67108864, %esi
	jbe	.L133
.L160:
	xorl	%ebx, %ebx
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L163:
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__GI___munmap
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L139:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%edi, %edi
	movl	%eax, %r8d
	movl	$16418, %ecx
	movl	$67108864, %esi
	call	__GI___mmap
	cmpq	$-1, %rax
	movq	%rax, %rbx
	je	.L160
	testl	$67108863, %eax
	je	.L138
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rbx, %rdi
	movl	$67108864, %esi
	xorl	%ebx, %ebx
	call	__GI___munmap
	jmp	.L132
	.size	new_heap, .-new_heap
	.section	.rodata.str1.1
.LC6:
	.string	"malloc.c"
.LC7:
	.string	"chunk_is_mmapped (p)"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"munmap_chunk(): invalid pointer"
	.text
	.p2align 4,,15
	.type	munmap_chunk, @function
munmap_chunk:
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andq	$-8, %rdx
	testb	$2, %al
	je	.L171
	cmpq	%rdi, dumped_main_arena_start(%rip)
	jbe	.L172
.L166:
	movq	(%rdi), %rsi
	leaq	16(%rdi), %rcx
	subq	%rsi, %rdi
	addq	%rdx, %rsi
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	movq	24(%rdx), %rdx
	leaq	-1(%rdx), %rax
	movq	%rdi, %rdx
	orq	%rsi, %rdx
	testq	%rax, %rdx
	jne	.L168
	andq	%rcx, %rax
	leaq	-1(%rax), %rdx
	testq	%rax, %rdx
	jne	.L168
#APP
# 2997 "malloc.c" 1
	lock;decl 40+mp_(%rip)
# 0 "" 2
#NO_APP
	movq	%rsi, %rax
	negq	%rax
#APP
# 2998 "malloc.c" 1
	lock;addq %rax, 56+mp_(%rip)
# 0 "" 2
#NO_APP
	addq	$8, %rsp
	jmp	__GI___munmap
	.p2align 4,,10
	.p2align 3
.L172:
	cmpq	%rdi, dumped_main_arena_end(%rip)
	jbe	.L166
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L168:
	leaq	.LC8(%rip), %rdi
	call	malloc_printerr
.L171:
	leaq	__PRETTY_FUNCTION__.12819(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$2978, %edx
	call	__malloc_assert
	.size	munmap_chunk, .-munmap_chunk
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"mremap_chunk(): invalid pointer"
	.section	.rodata.str1.1
.LC10:
	.string	"aligned_OK (chunk2rawmem (p))"
.LC11:
	.string	"prev_size (p) == offset"
	.text
	.p2align 4,,15
	.type	mremap_chunk, @function
mremap_chunk:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	(%rdi), %rbp
	movq	24(%rax), %rcx
	movq	8(%rdi), %rax
	movq	%rax, %r12
	andq	$-8, %r12
	testb	$2, %al
	je	.L186
	movq	%rdi, %r9
	leaq	0(%rbp,%r12), %r8
	leaq	-1(%rcx), %rax
	subq	%rbp, %r9
	leaq	16(%rdi), %r10
	movq	%r9, %rdx
	orq	%r8, %rdx
	testq	%rax, %rdx
	jne	.L175
	andq	%r10, %rax
	leaq	-1(%rax), %rdx
	testq	%rax, %rdx
	jne	.L175
	leaq	7(%rcx,%rsi), %rbx
	negq	%rcx
	addq	%rbp, %rbx
	andq	%rcx, %rbx
	cmpq	%rbx, %r8
	je	.L173
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	__GI___mremap
	cmpq	$-1, %rax
	je	.L182
	leaq	(%rax,%rbp), %rdi
	testb	$15, %dil
	jne	.L187
	cmpq	%rbp, (%rdi)
	jne	.L188
	movq	%rbx, %rax
	subq	%r12, %rbx
	subq	%rbp, %rax
	subq	%rbp, %rbx
	orq	$2, %rax
	movq	%rax, 8(%rdi)
	movq	%rbx, %rax
	lock xaddq	%rax, 56+mp_(%rip)
	addq	%rax, %rbx
.L180:
	movq	64+mp_(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.L173
	lock cmpxchgq	%rbx, 64+mp_(%rip)
	jne	.L180
.L173:
	popq	%rbx
	movq	%rdi, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	.LC9(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L182:
	xorl	%edi, %edi
	jmp	.L173
.L186:
	leaq	__PRETTY_FUNCTION__.12831(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$3016, %edx
	call	__malloc_assert
.L187:
	leaq	__PRETTY_FUNCTION__.12831(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$3040, %edx
	call	__malloc_assert
.L188:
	leaq	__PRETTY_FUNCTION__.12831(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$3042, %edx
	call	__malloc_assert
	.size	mremap_chunk, .-mremap_chunk
	.p2align 4,,15
	.type	ptmalloc_init.part.0, @function
ptmalloc_init.part.0:
	pushq	%rbx
	subq	$16, %rsp
	cmpb	$0, __libc_initial(%rip)
	movl	$0, __libc_malloc_initialized(%rip)
	jne	.L190
	movq	__morecore@GOTPCREL(%rip), %rax
	leaq	__failing_morecore(%rip), %rcx
	movq	%rcx, (%rax)
.L190:
	movq	thread_arena@gottpoff(%rip), %rax
	leaq	main_arena(%rip), %rdi
	leaq	8(%rsp), %rbx
	movq	%rdi, %fs:(%rax)
	call	malloc_init_state
	movq	_dl_tunable_set_mallopt_check@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$30, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_top_pad@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$11, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_perturb_byte@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$3, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_mmap_threshold@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$23, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_trim_threshold@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_mmaps_max@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$16, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_arena_max@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$22, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_arena_test@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$26, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_tcache_max@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$29, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_tcache_count@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$25, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_tcache_unsorted_limit@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$18, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_mxfast@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$9, %edi
	call	__tunable_get_val@PLT
	movq	__malloc_initialize_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L191
	call	*%rax
.L191:
	movl	$1, __libc_malloc_initialized(%rip)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	ptmalloc_init.part.0, .-ptmalloc_init.part.0
	.section	.rodata.str1.1
.LC12:
	.string	"corrupted size vs. prev_size"
.LC13:
	.string	"corrupted double-linked list"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"corrupted double-linked list (not small)"
	.text
	.p2align 4,,15
	.type	unlink_chunk.isra.2, @function
unlink_chunk.isra.2:
	subq	$8, %rsp
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	andq	$-8, %rax
	cmpq	(%rdi,%rax), %rax
	jne	.L209
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	cmpq	24(%rax), %rdi
	jne	.L198
	cmpq	16(%rdx), %rdi
	jne	.L198
	cmpq	$1023, %rcx
	movq	%rdx, 24(%rax)
	movq	%rax, 16(%rdx)
	jbe	.L196
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L196
	cmpq	40(%rdx), %rdi
	jne	.L201
	movq	40(%rdi), %rcx
	cmpq	32(%rcx), %rdi
	jne	.L201
	cmpq	$0, 32(%rax)
	je	.L210
	movq	%rcx, 40(%rdx)
	movq	40(%rdi), %rax
	movq	%rdx, 32(%rax)
.L196:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L198:
	leaq	.LC13(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L210:
	cmpq	%rdx, %rdi
	je	.L211
	movq	%rdx, 32(%rax)
	movq	32(%rdi), %rdx
	movq	%rcx, 40(%rax)
	movq	%rax, 40(%rdx)
	movq	40(%rdi), %rdx
	movq	%rax, 32(%rdx)
	jmp	.L196
	.p2align 4,,10
	.p2align 3
.L211:
	movq	%rax, 40(%rax)
	movq	%rax, 32(%rax)
	jmp	.L196
.L209:
	leaq	.LC12(%rip), %rdi
	call	malloc_printerr
.L201:
	leaq	.LC14(%rip), %rdi
	call	malloc_printerr
	.size	unlink_chunk.isra.2, .-unlink_chunk.isra.2
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"malloc_consolidate(): unaligned fastbin chunk detected"
	.align 8
.LC16:
	.string	"malloc_consolidate(): invalid chunk size"
	.align 8
.LC17:
	.string	"corrupted size vs. prev_size in fastbins"
	.text
	.p2align 4,,15
	.type	malloc_consolidate, @function
malloc_consolidate:
	pushq	%r15
	pushq	%r14
	leaq	96(%rdi), %rax
	pushq	%r13
	pushq	%r12
	leaq	16(%rdi), %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	subq	$56, %rsp
	movl	$0, 8(%rdi)
	movq	%rax, 32(%rsp)
	leaq	88(%rdi), %rax
	movq	%rax, 40(%rsp)
.L224:
	xorl	%ebx, %ebx
#APP
# 4702 "malloc.c" 1
	xchgq %rbx, (%r14)
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	jne	.L223
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L235:
	testb	$1, 8(%rax,%rcx)
	je	.L231
	andq	$-2, 8(%rax)
.L220:
	movq	112(%r13), %rax
	cmpq	$1023, %rbp
	movq	%rbx, 112(%r13)
	movq	%rbx, 24(%rax)
	jbe	.L221
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
.L221:
	movq	32(%rsp), %rdx
	movq	%rbp, %rcx
	movq	%rax, 16(%rbx)
	orq	$1, %rcx
	cmpq	%r12, %r15
	movq	%rcx, 8(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rbp, (%rbx,%rbp)
	movq	%r9, %rbx
	je	.L213
.L223:
	testb	$15, %bl
	jne	.L232
	movq	8(%rbx), %rdi
	movl	%edi, %eax
	shrl	$4, %eax
	subl	$2, %eax
	leaq	16(%r13,%rax,8), %rax
	cmpq	%rax, %r14
	jne	.L233
	movq	%rdi, %rbp
	leaq	16(%rbx), %r15
	movq	16(%rbx), %r12
	andq	$-8, %rbp
	leaq	(%rbx,%rbp), %rax
	shrq	$12, %r15
	movq	%r15, %r9
	movq	8(%rax), %rcx
	xorq	%r12, %r9
	andq	$-8, %rcx
	andl	$1, %edi
	jne	.L216
	movq	(%rbx), %rdi
	subq	%rdi, %rbx
	addq	%rdi, %rbp
	movq	8(%rbx), %r10
	andq	$-8, %r10
	cmpq	%rdi, %r10
	jne	.L234
	movq	%rbx, %rdi
	movq	%rcx, 24(%rsp)
	movq	%rax, 16(%rsp)
	movq	%r9, 8(%rsp)
	call	unlink_chunk.isra.2
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rax
	movq	8(%rsp), %r9
.L216:
	cmpq	%rax, 96(%r13)
	jne	.L235
	addq	%rcx, %rbp
	orq	$1, %rbp
	cmpq	%r12, %r15
	movq	%rbp, 8(%rbx)
	movq	%rbx, 96(%r13)
	movq	%r9, %rbx
	jne	.L223
	.p2align 4,,10
	.p2align 3
.L213:
	addq	$8, %r14
	leaq	-8(%r14), %rax
	cmpq	%rax, 40(%rsp)
	jne	.L224
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	movq	%rax, %rdi
	movq	%r9, 8(%rsp)
	addq	%rcx, %rbp
	call	unlink_chunk.isra.2
	movq	8(%rsp), %r9
	jmp	.L220
	.p2align 4,,10
	.p2align 3
.L232:
	leaq	.LC15(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L234:
	leaq	.LC17(%rip), %rdi
	call	malloc_printerr
.L233:
	leaq	.LC16(%rip), %rdi
	call	malloc_printerr
	.size	malloc_consolidate, .-malloc_consolidate
	.section	.rodata.str1.1
.LC18:
	.string	"arena.c"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"replaced_arena->attached_threads > 0"
	.section	.text.unlikely,"ax",@progbits
	.type	detach_arena.part.3, @function
detach_arena.part.3:
	leaq	__PRETTY_FUNCTION__.12007(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	subq	$8, %rsp
	movl	$726, %edx
	call	__malloc_assert
	.size	detach_arena.part.3, .-detach_arena.part.3
	.section	.rodata.str1.1
.LC20:
	.string	"result->attached_threads == 0"
	.text
	.p2align 4,,15
	.type	get_free_list, @function
get_free_list:
	cmpq	$0, free_list(%rip)
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L250
	movq	thread_arena@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %r12
#APP
# 813 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L241
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L242:
	movq	free_list(%rip), %rbx
	testq	%rbx, %rbx
	je	.L244
	cmpq	$0, 2176(%rbx)
	movq	2168(%rbx), %rax
	movq	%rax, free_list(%rip)
	jne	.L260
	testq	%r12, %r12
	movq	$1, 2176(%rbx)
	je	.L244
	movq	2176(%r12), %rax
	testq	%rax, %rax
	je	.L261
	subq	$1, %rax
	movq	%rax, 2176(%r12)
.L244:
#APP
# 825 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L248
	subl	$1, free_list_lock(%rip)
.L249:
	testq	%rbx, %rbx
	je	.L250
#APP
# 830 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L251
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L252:
	movq	%rbx, %fs:0(%rbp)
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L250:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L251:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L252
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L252
	.p2align 4,,10
	.p2align 3
.L241:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L242
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L248:
	xorl	%eax, %eax
#APP
# 825 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L249
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 825 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L249
.L260:
	leaq	__PRETTY_FUNCTION__.12111(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$820, %edx
	call	__malloc_assert
.L261:
	call	detach_arena.part.3
	.size	get_free_list, .-get_free_list
	.section	.rodata.str1.1
.LC21:
	.string	"p->attached_threads == 0"
	.text
	.p2align 4,,15
	.type	arena_get2.part.4, @function
arena_get2.part.4:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	narenas_limit.12225(%rip), %rax
	movq	narenas(%rip), %rdx
	testq	%rax, %rax
	jne	.L268
	movq	32+mp_(%rip), %rax
	testq	%rax, %rax
	je	.L264
	movq	%rax, narenas_limit.12225(%rip)
.L268:
	subq	$1, %rax
	cmpq	%rdx, %rax
	jnb	.L329
	movq	next_to_use.12166(%rip), %rbx
	testq	%rbx, %rbx
	je	.L330
.L285:
	xorl	%ecx, %ecx
	movl	$1, %edx
	jmp	.L289
	.p2align 4,,10
	.p2align 3
.L332:
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L288
.L333:
	movq	2160(%rbx), %rbx
	cmpq	next_to_use.12166(%rip), %rbx
	je	.L331
.L289:
#APP
# 875 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L332
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rbx)
	setne	%al
	movzbl	%al, %eax
	testl	%eax, %eax
	jne	.L333
	.p2align 4,,10
	.p2align 3
.L288:
	movq	thread_arena@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %r12
#APP
# 897 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L292
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L293:
	testq	%r12, %r12
	je	.L294
	movq	2176(%r12), %rax
	testq	%rax, %rax
	je	.L295
	subq	$1, %rax
	movq	%rax, 2176(%r12)
.L294:
	movq	free_list(%rip), %rdx
	testq	%rdx, %rdx
	je	.L296
	cmpq	$0, 2176(%rdx)
	jne	.L297
	cmpq	%rdx, %rbx
	jne	.L299
	jmp	.L334
	.p2align 4,,10
	.p2align 3
.L301:
	cmpq	$0, 2176(%rax)
	jne	.L297
	cmpq	%rax, %rbx
	je	.L335
	movq	%rax, %rdx
.L299:
	movq	2168(%rdx), %rax
	testq	%rax, %rax
	jne	.L301
.L296:
	addq	$1, 2176(%rbx)
#APP
# 912 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L302
	subl	$1, free_list_lock(%rip)
.L303:
	movq	2160(%rbx), %rax
	movq	%rbx, %fs:0(%rbp)
	movq	%rax, next_to_use.12166(%rip)
.L262:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L264:
	cmpq	%rdx, 24+mp_(%rip)
	jnb	.L268
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	__GI___get_nprocs
	testl	%eax, %eax
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	jle	.L265
	sall	$3, %eax
	movq	narenas(%rip), %rdx
	cltq
	movq	%rax, narenas_limit.12225(%rip)
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L331:
	cmpq	%rbx, %rsi
	je	.L336
.L290:
#APP
# 890 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L291
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L335:
	leaq	2168(%rdx), %rcx
	movq	%rbx, %rdx
.L298:
	movq	2168(%rdx), %rax
	movq	%rax, (%rcx)
	jmp	.L296
	.p2align 4,,10
	.p2align 3
.L330:
	leaq	main_arena(%rip), %rbx
	movq	%rbx, next_to_use.12166(%rip)
	jmp	.L285
	.p2align 4,,10
	.p2align 3
.L336:
	movq	2160(%rbx), %rbx
	jmp	.L290
	.p2align 4,,10
	.p2align 3
.L329:
	leaq	1(%rdx), %rcx
	movq	%rdx, %rax
#APP
# 960 "arena.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rcx, narenas(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rax
	je	.L267
	movq	narenas(%rip), %rdx
	movq	narenas_limit.12225(%rip), %rax
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L292:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L293
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L302:
	xorl	%eax, %eax
#APP
# 912 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L303
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 912 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L303
.L334:
	leaq	free_list(%rip), %rcx
	jmp	.L298
.L291:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L288
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L288
.L265:
	movq	$16, narenas_limit.12225(%rip)
	movq	narenas(%rip), %rdx
	movl	$16, %eax
	jmp	.L268
.L267:
	movq	8+mp_(%rip), %rsi
	addq	$2248, %rdi
	call	new_heap
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L337
.L269:
	leaq	32(%rbp), %r12
	movq	%r12, 0(%rbp)
	movq	%r12, %rdi
	movq	%r12, %rbx
	call	malloc_init_state
	movq	16(%rbp), %rax
	leaq	2248(%rbp), %rcx
	movq	$1, 2208(%rbp)
	leaq	2232(%rbp), %rdx
	andl	$15, %ecx
	movq	%rax, 2224(%rbp)
	movq	%rax, 2216(%rbp)
	je	.L271
	subq	%rcx, %rdx
	addq	$16, %rdx
.L271:
	addq	%rbp, %rax
	movq	%rdx, 128(%rbp)
	subq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, 8(%rdx)
	movq	thread_arena@gottpoff(%rip), %rax
	movl	$0, 32(%rbp)
	movq	%fs:(%rax), %r13
	movq	%r12, %fs:(%rax)
#APP
# 773 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L272
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_lock(%rip)
# 0 "" 2
#NO_APP
.L273:
	movq	2160+main_arena(%rip), %rax
	movq	%rax, 2192(%rbp)
	movq	%r12, 2160+main_arena(%rip)
#APP
# 783 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L274
	subl	$1, list_lock(%rip)
.L275:
#APP
# 785 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L276
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L277:
	testq	%r13, %r13
	je	.L278
	movq	2176(%r13), %rax
	testq	%rax, %rax
	je	.L295
	subq	$1, %rax
	movq	%rax, 2176(%r13)
.L278:
#APP
# 787 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L280
	subl	$1, free_list_lock(%rip)
.L281:
#APP
# 799 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L282
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 32(%rbp)
# 0 "" 2
#NO_APP
.L283:
	testq	%r12, %r12
	jne	.L262
.L284:
#APP
# 964 "arena.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	decq narenas(%rip)
# 0 "" 2
#NO_APP
	xorl	%ebx, %ebx
	jmp	.L262
.L297:
	leaq	__PRETTY_FUNCTION__.12158(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$846, %edx
	call	__malloc_assert
.L337:
	movq	8+mp_(%rip), %rsi
	movl	$2248, %edi
	call	new_heap
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L269
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L276:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L277
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L277
	.p2align 4,,10
	.p2align 3
.L274:
	xorl	%eax, %eax
#APP
# 783 "arena.c" 1
	xchgl %eax, list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L275
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 783 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L272:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_lock(%rip)
	je	.L273
	leaq	list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L282:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L283
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L283
	.p2align 4,,10
	.p2align 3
.L280:
	xorl	%eax, %eax
#APP
# 787 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L281
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 787 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L281
.L295:
	call	detach_arena.part.3
	.size	arena_get2.part.4, .-arena_get2.part.4
	.p2align 4,,15
	.type	arena_get_retry, @function
arena_get_retry:
	pushq	%rbx
	leaq	main_arena(%rip), %rax
	subq	$16, %rsp
	cmpq	%rax, %rdi
	je	.L339
#APP
# 982 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L340
	subl	$1, (%rdi)
.L341:
#APP
# 984 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L342
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
	leaq	main_arena(%rip), %rax
.L338:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%rsi, %rbx
#APP
# 988 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L344
	subl	$1, (%rdi)
.L345:
	call	get_free_list
	testq	%rax, %rax
	jne	.L338
	addq	$16, %rsp
	movq	%rbx, %rdi
	leaq	main_arena(%rip), %rsi
	popq	%rbx
	jmp	arena_get2.part.4
	.p2align 4,,10
	.p2align 3
.L340:
	xorl	%eax, %eax
#APP
# 982 "arena.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L341
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 982 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L342:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	leaq	main_arena(%rip), %rax
	je	.L338
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rax
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L344:
	xorl	%eax, %eax
#APP
# 988 "arena.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L345
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 988 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L345
	.size	arena_get_retry, .-arena_get_retry
	.p2align 4,,15
	.globl	_dl_tunable_set_mallopt_check
	.type	_dl_tunable_set_mallopt_check, @function
_dl_tunable_set_mallopt_check:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L348
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	leaq	malloc_check(%rip), %rdx
	leaq	free_check(%rip), %rcx
	leaq	realloc_check(%rip), %rsi
	movl	$1, using_malloc_checking(%rip)
	movq	%rdx, (%rax)
	movq	__free_hook@GOTPCREL(%rip), %rax
	leaq	memalign_check(%rip), %rdx
	movq	%rcx, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	%rsi, (%rax)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
.L348:
	rep ret
	.size	_dl_tunable_set_mallopt_check, .-_dl_tunable_set_mallopt_check
	.section	.rodata.str1.1
.LC22:
	.string	"<heap nr=\"%d\">\n<sizes>\n"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"__malloc_info(): unaligned fastbin chunk detected"
	.align 8
.LC24:
	.string	"  <size from=\"%zu\" to=\"%zu\" total=\"%zu\" count=\"%zu\"/>\n"
	.align 8
.LC25:
	.string	"  <unsorted from=\"%zu\" to=\"%zu\" total=\"%zu\" count=\"%zu\"/>\n"
	.align 8
.LC26:
	.string	"</sizes>\n<total type=\"fast\" count=\"%zu\" size=\"%zu\"/>\n<total type=\"rest\" count=\"%zu\" size=\"%zu\"/>\n<system type=\"current\" size=\"%zu\"/>\n<system type=\"max\" size=\"%zu\"/>\n"
	.align 8
.LC27:
	.string	"<aspace type=\"total\" size=\"%zu\"/>\n<aspace type=\"mprotect\" size=\"%zu\"/>\n<aspace type=\"subheaps\" size=\"%zu\"/>\n"
	.align 8
.LC28:
	.string	"<aspace type=\"total\" size=\"%zu\"/>\n<aspace type=\"mprotect\" size=\"%zu\"/>\n"
	.section	.rodata.str1.1
.LC29:
	.string	"</heap>\n"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.ascii	"<total type=\"fast\" count"
	.string	"=\"%zu\" size=\"%zu\"/>\n<total type=\"rest\" count=\"%zu\" size=\"%zu\"/>\n<total type=\"mmap\" count=\"%d\" size=\"%zu\"/>\n<system type=\"current\" size=\"%zu\"/>\n<system type=\"max\" size=\"%zu\"/>\n<aspace type=\"total\" size=\"%zu\"/>\n<aspace type=\"mprotect\" size=\"%zu\"/>\n</malloc>\n"
	.text
	.p2align 4,,15
	.type	__malloc_info.part.11, @function
__malloc_info.part.11:
	pushq	%r15
	pushq	%r14
	leaq	main_arena(%rip), %rax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%edx, %edx
	movq	$-1, %r14
	subq	$4520, %rsp
	movq	%rax, (%rsp)
	leaq	128(%rsp), %rax
	movq	$0, 96(%rsp)
	movq	$0, 88(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 72(%rsp)
	movq	$0, 48(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 40(%rsp)
	movq	$0, 56(%rsp)
	movq	%rax, 24(%rsp)
.L371:
	leal	1(%rdx), %eax
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, 36(%rsp)
	xorl	%eax, %eax
	call	__GI_fprintf
#APP
# 5701 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	(%rsp), %rbx
	jne	.L351
	movl	$1, %esi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %esi, (%rbx)
# 0 "" 2
#NO_APP
.L352:
	movq	(%rsp), %rax
	movq	24(%rsp), %rdi
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	96(%rax), %r15
	leaq	16(%rax), %r8
	leaq	320(%rdi), %r10
	movq	%rdi, %rbx
	movq	8(%r15), %r11
.L357:
	movq	(%r8), %r9
	testq	%r9, %r9
	je	.L353
	movq	%r9, %rdx
	xorl	%ecx, %ecx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L354:
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rsi
	addq	$1, %rcx
	shrq	$12, %rax
	movq	%rax, %rdx
	xorq	%rsi, %rdx
	cmpq	%rsi, %rax
	je	.L396
.L355:
	testb	$15, %dl
	je	.L354
	leaq	.LC23(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L353:
	movq	$0, 24(%rdi)
	movq	$0, 8(%rdi)
	xorl	%ecx, %ecx
	movq	$0, (%rdi)
.L356:
	imulq	8(%rdi), %rcx
	addq	$32, %rdi
	addq	$8, %r8
	movq	%rcx, -16(%rdi)
	cmpq	%rdi, %r10
	jne	.L357
	movq	24(%rsp), %rax
	movq	(%rsp), %rsi
	andq	$-8, %r11
	movq	%r11, %r10
	movl	$1, %r12d
	leaq	320(%rax), %rcx
	leaq	96(%rsi), %r9
	leaq	4384(%rax), %r11
	.p2align 4,,10
	.p2align 3
.L361:
	movq	16(%r9), %rdx
	movq	%r14, (%rcx)
	movq	$0, 24(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	testq	%rdx, %rdx
	je	.L358
	cmpq	%r9, %rdx
	je	.L358
	xorl	%esi, %esi
	movq	$-1, %rdi
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L359:
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	addq	$1, %r8
	addq	%rax, %rbp
	cmpq	%rax, %rdi
	cmova	%rax, %rdi
	cmpq	%rax, %rsi
	cmovb	%rax, %rsi
	cmpq	%r9, %rdx
	jne	.L359
	testq	%r8, %r8
	movq	%r8, 24(%rcx)
	movq	%rbp, 16(%rcx)
	movq	%rdi, (%rcx)
	movq	%rsi, 8(%rcx)
	je	.L358
	addq	%r8, %r12
.L360:
	addq	16(%rcx), %r10
	addq	$32, %rcx
	addq	$16, %r9
	cmpq	%r11, %rcx
	movq	%r10, %rbp
	jne	.L361
	leaq	main_arena(%rip), %rsi
	cmpq	%rsi, (%rsp)
	je	.L372
	movq	%r15, %rax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	andq	$-67108864, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L363:
	addq	16(%rax), %rdx
	addq	24(%rax), %rcx
	addq	$1, %rsi
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L363
	movq	%rdx, 104(%rsp)
	movq	%rcx, 112(%rsp)
	movq	%rsi, 120(%rsp)
.L362:
#APP
# 5787 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L364
	movq	(%rsp), %rax
	subl	$1, (%rax)
.L365:
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	xorl	%r15d, %r15d
	addq	%rdi, 40(%rsp)
	addq	%rsi, 48(%rsp)
	addq	%r12, 56(%rsp)
	addq	%rbp, 64(%rsp)
	.p2align 4,,10
	.p2align 3
.L367:
	cmpq	$10, %r15
	movq	24(%rbx), %r9
	je	.L366
	testq	%r9, %r9
	je	.L366
	movq	8(%rbx), %rcx
	movq	16(%rbx), %r8
	leaq	.LC24(%rip), %rsi
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__GI_fprintf
.L366:
	addq	$1, %r15
	addq	$32, %rbx
	cmpq	$137, %r15
	jne	.L367
	movq	472(%rsp), %r9
	testq	%r9, %r9
	jne	.L397
.L368:
	movq	(%rsp), %rbx
	leaq	.LC26(%rip), %rsi
	movq	%rbp, %r9
	movq	%r12, %r8
	movq	%r13, %rdi
	movq	2184(%rbx), %rax
	movq	2192(%rbx), %rdx
	addq	%rax, 72(%rsp)
	addq	%rdx, 80(%rsp)
	pushq	%rdx
	pushq	%rax
	xorl	%eax, %eax
	movq	32(%rsp), %rcx
	movq	24(%rsp), %rdx
	call	__GI_fprintf
	popq	%rax
	leaq	main_arena(%rip), %rax
	popq	%rdx
	cmpq	%rax, %rbx
	je	.L369
	movq	112(%rsp), %r15
	movq	104(%rsp), %rbx
	leaq	.LC27(%rip), %rsi
	movq	120(%rsp), %r8
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%r15, %rcx
	movq	%rbx, %rdx
	call	__GI_fprintf
	addq	%rbx, 88(%rsp)
	addq	%r15, 96(%rsp)
.L370:
	leaq	.LC29(%rip), %rdi
	movq	%r13, %rsi
	leaq	main_arena(%rip), %rbx
	call	__GI__IO_fputs
	movq	(%rsp), %rax
	movl	36(%rsp), %edx
	movq	2160(%rax), %rax
	cmpq	%rbx, %rax
	movq	%rax, (%rsp)
	jne	.L371
	movl	40+mp_(%rip), %eax
	pushq	96(%rsp)
	leaq	.LC30(%rip), %rsi
	pushq	96(%rsp)
	pushq	96(%rsp)
	movq	%r13, %rdi
	pushq	96(%rsp)
	pushq	56+mp_(%rip)
	pushq	%rax
	movq	112(%rsp), %r9
	xorl	%eax, %eax
	movq	104(%rsp), %r8
	movq	96(%rsp), %rcx
	movq	88(%rsp), %rdx
	call	__GI_fprintf
	addq	$4568, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movq	8(%r9), %rax
	addq	%rcx, 8(%rsp)
	movq	%rcx, 24(%rdi)
	andq	$-8, %rax
	movq	%rax, %rdx
	movq	%rax, 8(%rdi)
	imulq	%rcx, %rdx
	addq	%rdx, 16(%rsp)
	leaq	-15(%rax), %rdx
	movq	%rdx, (%rdi)
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L358:
	movq	$0, (%rcx)
	jmp	.L360
.L372:
	movq	$0, 120(%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 104(%rsp)
	jmp	.L362
.L351:
	xorl	%eax, %eax
	movl	$1, %edi
	lock cmpxchgl	%edi, (%rbx)
	je	.L352
	movq	(%rsp), %rdi
	call	__lll_lock_wait_private
	jmp	.L352
.L397:
	movq	464(%rsp), %r8
	movq	456(%rsp), %rcx
	leaq	.LC25(%rip), %rsi
	movq	448(%rsp), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	__GI_fprintf
	jmp	.L368
.L369:
	movq	(%rsp), %rbx
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	2184(%rbx), %rdx
	movq	%rdx, %rcx
	call	__GI_fprintf
	movq	2184(%rbx), %rax
	addq	%rax, 88(%rsp)
	addq	%rax, 96(%rsp)
	jmp	.L370
.L364:
	xorl	%eax, %eax
	movq	(%rsp), %rdi
#APP
# 5787 "malloc.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L365
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 5787 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L365
	.size	__malloc_info.part.11, .-__malloc_info.part.11
	.p2align 4,,15
	.type	systrim.isra.1.constprop.12, @function
systrim.isra.1.constprop.12:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	96+main_arena(%rip), %rax
	movq	8(%rax), %rbx
	andq	$-8, %rbx
	leaq	-33(%rbx), %rbp
	cmpq	%rdi, %rbp
	jbe	.L401
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	subq	%rdi, %rbp
	movq	24(%rax), %rax
	negq	%rax
	andq	%rax, %rbp
	je	.L401
	movq	__morecore@GOTPCREL(%rip), %r13
	xorl	%edi, %edi
	call	*0(%r13)
	movq	%rax, %r12
	movq	96+main_arena(%rip), %rax
	addq	%rbx, %rax
	cmpq	%rax, %r12
	jne	.L401
	movq	%rbp, %rdi
	negq	%rdi
	call	*0(%r13)
	movq	__after_morecore_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L416
.L402:
	xorl	%edi, %edi
	call	*0(%r13)
	testq	%rax, %rax
	je	.L401
	subq	%rax, %r12
	je	.L401
	movq	96+main_arena(%rip), %rax
	subq	%r12, %rbx
	subq	%r12, 2184+main_arena(%rip)
	orq	$1, %rbx
	movq	%rbx, 8(%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L401:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L416:
	call	*%rax
	jmp	.L402
	.size	systrim.isra.1.constprop.12, .-systrim.isra.1.constprop.12
	.section	.rodata.str1.1
.LC31:
	.string	"free(): invalid pointer"
.LC32:
	.string	"free(): invalid size"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"free(): too many chunks detected in tcache"
	.align 8
.LC34:
	.string	"free(): unaligned chunk detected in tcache 2"
	.align 8
.LC35:
	.string	"free(): double free detected in tcache 2"
	.align 8
.LC36:
	.string	"free(): invalid next size (fast)"
	.align 8
.LC37:
	.string	"double free or corruption (fasttop)"
	.section	.rodata.str1.1
.LC38:
	.string	"invalid fastbin entry (free)"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"double free or corruption (top)"
	.align 8
.LC40:
	.string	"double free or corruption (out)"
	.align 8
.LC41:
	.string	"double free or corruption (!prev)"
	.align 8
.LC42:
	.string	"free(): invalid next size (normal)"
	.align 8
.LC43:
	.string	"corrupted size vs. prev_size while consolidating"
	.align 8
.LC44:
	.string	"free(): corrupted unsorted chunks"
	.section	.rodata.str1.1
.LC45:
	.string	"heap->ar_ptr == av"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"chunksize_nomask (p) == (0 | PREV_INUSE)"
	.align 8
.LC47:
	.string	"new_size > 0 && new_size < (long) (2 * MINSIZE)"
	.align 8
.LC48:
	.string	"new_size > 0 && new_size < HEAP_MAX_SIZE"
	.align 8
.LC49:
	.string	"((unsigned long) ((char *) p + new_size) & (pagesz - 1)) == 0"
	.align 8
.LC50:
	.string	"((char *) p + new_size) == ((char *) heap + heap->size)"
	.align 8
.LC51:
	.string	"/proc/sys/vm/overcommit_memory"
	.text
	.p2align 4,,15
	.type	_int_free, @function
_int_free:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	8(%rsi), %rax
	movq	%rax, %r12
	andq	$-8, %r12
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rsi
	ja	.L418
	testb	$15, %sil
	movq	%rsi, %rbx
	jne	.L418
	cmpq	$31, %r12
	jbe	.L420
	testb	$8, %al
	jne	.L420
	movq	tcache@gottpoff(%rip), %rdx
	movq	%rdi, %rbp
	movq	%fs:(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L422
	leaq	-17(%r12), %rdx
	shrq	$4, %rdx
	cmpq	%rdx, 80+mp_(%rip)
	jbe	.L422
	cmpq	24(%rsi), %rcx
	leaq	16(%rsi), %r8
	movq	96+mp_(%rip), %rdi
	je	.L542
.L423:
	leaq	(%rcx,%rdx,2), %r9
	movzwl	(%r9), %r10d
	cmpq	%rdi, %r10
	movq	%r10, %rsi
	jb	.L543
.L422:
	cmpq	global_max_fast(%rip), %r12
	ja	.L432
	leaq	(%rbx,%r12), %r14
	movq	8(%r14), %rax
	cmpq	$16, %rax
	jbe	.L433
	andq	$-8, %rax
	cmpq	2184(%rbp), %rax
	jnb	.L433
.L434:
	movl	perturb_byte(%rip), %eax
	testl	%eax, %eax
	jne	.L544
.L442:
	shrl	$4, %r12d
	leal	-2(%r12), %eax
	movl	$1, 8(%rbp)
	leaq	0(%rbp,%rax,8), %rcx
	movq	%rax, %r12
	movq	16(%rcx), %rdx
#APP
# 4491 "malloc.c" 1
	movl %fs:24,%esi
# 0 "" 2
#NO_APP
	testl	%esi, %esi
	jne	.L443
	cmpq	%rdx, %rbx
	je	.L448
	leaq	16(%rbx), %rax
	shrq	$12, %rax
	xorq	%rdx, %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, 16(%rcx)
.L445:
	testq	%rdx, %rdx
	je	.L417
	andl	$1, %r13d
	je	.L417
	movq	8(%rdx), %rax
	shrl	$4, %eax
	subl	$2, %eax
	cmpl	%r12d, %eax
	jne	.L545
.L417:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L432:
	testb	$2, %al
	jne	.L453
#APP
# 4529 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L500
	andl	$1, %r13d
	je	.L546
.L500:
	movl	$1, 20(%rsp)
.L454:
	movq	96(%rbp), %rax
	leaq	(%rbx,%r12), %r13
	cmpq	%rbx, %rax
	je	.L547
	testb	$2, 4(%rbp)
	je	.L548
.L457:
	movq	8(%r13), %rax
	testb	$1, %al
	je	.L549
	movq	%rax, %r14
	andq	$-8, %r14
	cmpq	$16, %rax
	jbe	.L459
	cmpq	%r14, 2184(%rbp)
	jbe	.L459
	movl	perturb_byte(%rip), %esi
	testl	%esi, %esi
	jne	.L550
.L461:
	testb	$1, 8(%rbx)
	jne	.L462
	movq	(%rbx), %rax
	subq	%rax, %rbx
	addq	%rax, %r12
	movq	8(%rbx), %rdx
	andq	$-8, %rdx
	cmpq	%rax, %rdx
	jne	.L551
	movq	%rbx, %rdi
	call	unlink_chunk.isra.2
.L462:
	cmpq	%r13, 96(%rbp)
	je	.L464
	testb	$1, 8(%r13,%r14)
	je	.L552
	andq	$-2, 8(%r13)
.L466:
	movq	112(%rbp), %rax
	leaq	96(%rbp), %rcx
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L553
	cmpq	$1023, %r12
	movq	%rax, 16(%rbx)
	movq	%rdx, 24(%rbx)
	ja	.L554
.L468:
	movq	%rbx, 112(%rbp)
	movq	%rbx, 24(%rax)
	movq	%r12, %rax
	orq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	%r12, (%rbx,%r12)
.L469:
	cmpq	$65535, %r12
	ja	.L555
.L471:
	movl	20(%rsp), %eax
	testl	%eax, %eax
	jne	.L417
#APP
# 4650 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L497
	subl	$1, 0(%rbp)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L543:
	leaq	(%rcx,%rdx,8), %rdx
	movq	%r8, %rax
	addl	$1, %esi
	shrq	$12, %rax
	movq	%rcx, 24(%rbx)
	xorq	128(%rdx), %rax
	movq	%rax, 16(%rbx)
	movq	%r8, 128(%rdx)
	movw	%si, (%r9)
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L453:
	movq	%rbx, %rdi
	call	munmap_chunk
	jmp	.L417
	.p2align 4,,10
	.p2align 3
.L418:
	leaq	.LC31(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L420:
	leaq	.LC32(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L433:
	testl	%r13d, %r13d
	jne	.L441
#APP
# 4472 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L436
	movl	%r13d, %eax
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%rbp)
# 0 "" 2
#NO_APP
.L437:
	movq	8(%r14), %rax
	movl	$1, %r8d
	cmpq	$16, %rax
	jbe	.L438
	xorl	%r8d, %r8d
	andq	$-8, %rax
	cmpq	2184(%rbp), %rax
	setnb	%r8b
.L438:
#APP
# 4475 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L439
	subl	$1, 0(%rbp)
.L440:
	testl	%r8d, %r8d
	je	.L434
.L441:
	leaq	.LC36(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L554:
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L545:
	leaq	.LC38(%rip), %rdi
	call	malloc_printerr
.L436:
	movl	$1, %edx
	movl	%r13d, %eax
	lock cmpxchgl	%edx, 0(%rbp)
	je	.L437
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L437
	.p2align 4,,10
	.p2align 3
.L443:
	cmpq	%rdx, %rbx
	je	.L448
	leaq	16(%rbx), %rdi
	leaq	16(%rbp,%rax,8), %rsi
	movq	%rdx, %rax
	shrq	$12, %rdi
	xorq	%rdi, %rax
	movq	%rax, 16(%rbx)
	movq	%rdx, %rax
#APP
# 4510 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rax
	movq	%rax, %rcx
	jne	.L447
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L539:
	movq	%rcx, %rax
	xorq	%rdi, %rax
	movq	%rax, 16(%rbx)
	movq	%rcx, %rax
#APP
# 4510 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rax, %rcx
	je	.L499
	movq	%rax, %rcx
.L447:
	cmpq	%rbx, %rcx
	jne	.L539
.L448:
	leaq	.LC37(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L555:
	movl	8(%rbp), %eax
	testl	%eax, %eax
	jne	.L556
.L472:
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rbp
	je	.L557
	movq	96(%rbp), %rbx
	movq	%rbx, %rdi
	andq	$-67108864, %rdi
	movq	(%rdi), %r15
	cmpq	%rbp, %r15
	jne	.L558
	movq	8+mp_(%rip), %rax
	leaq	32(%rdi), %rsi
	cmpq	%rbx, %rsi
	movq	%rax, 24(%rsp)
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	24(%rax), %rax
	movq	%rax, 32(%rsp)
	jne	.L559
	movq	8(%rdi), %r14
	movq	16(%r14), %rcx
	leaq	-16(%rcx), %rdx
	leaq	(%r14,%rdx), %rax
	andl	$15, %eax
	subq	%rax, %rdx
	addq	%r14, %rdx
	cmpq	$1, 8(%rdx)
	jne	.L479
	movq	32(%rsp), %rbx
	movq	24(%rsp), %r8
	movl	$67108864, %r13d
	movq	%rbp, 40(%rsp)
	movq	%r14, %rbp
	movq	%r15, %r14
	leaq	32(%r8,%rbx), %r12
	subq	$1, %rbx
	movq	%rbx, 8(%rsp)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L485:
	leaq	(%rbx,%r15), %rax
	testq	%rax, 8(%rsp)
	jne	.L560
	movq	16(%rbp), %rdx
	addq	%rbp, %rdx
	cmpq	%rdx, %rax
	jne	.L561
	leaq	32(%rbp), %rax
	orq	$1, %r15
	movq	%rbx, 96(%r14)
	movq	%r15, 8(%rbx)
	cmpq	%rax, %rbx
	jne	.L562
	movq	8(%rbp), %r10
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	16(%r10), %rcx
	leaq	-16(%rcx), %rdx
	leaq	(%r10,%rdx), %rax
	andl	$15, %eax
	subq	%rax, %rdx
	addq	%r10, %rdx
	cmpq	$1, 8(%rdx)
	jne	.L479
	movq	%r10, %rbp
.L478:
	movq	%rdx, %rbx
	subq	(%rdx), %rbx
	movq	8(%rbx), %rdx
	movq	%rdx, %r10
	andq	$-8, %r10
	addq	%r10, %rax
	leaq	16(%rax), %r15
	addq	$15, %rax
	cmpq	$62, %rax
	ja	.L563
	andl	$1, %edx
	jne	.L481
	addq	(%rbx), %r15
.L481:
	leaq	-1(%r15), %rax
	cmpq	$67108862, %rax
	ja	.L564
	movq	%r13, %rax
	subq	%rcx, %rax
	addq	%r15, %rax
	cmpq	%r12, %rax
	jb	.L565
	movq	16(%rdi), %rax
	subq	%rax, 2184(%r14)
	leaq	67108864(%rdi), %rax
	cmpq	%rax, aligned_heap_area(%rip)
	je	.L566
.L484:
	movl	$67108864, %esi
	call	__GI___munmap
	testb	$1, 8(%rbx)
	jne	.L485
	subq	(%rbx), %rbx
	movq	%rbx, %rdi
	call	unlink_chunk.isra.2
	jmp	.L485
	.p2align 4,,10
	.p2align 3
.L566:
	movq	$0, aligned_heap_area(%rip)
	jmp	.L484
	.p2align 4,,10
	.p2align 3
.L464:
	addq	%r14, %r12
	movq	%r12, %rax
	orq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, 96(%rbp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L459:
	leaq	.LC42(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L544:
	leaq	24(%rbx), %rdi
	leaq	16(%rbx), %rcx
	leaq	-16(%r12), %rdx
	movabsq	$72340172838076673, %rsi
	movzbl	%al, %eax
	andq	$-8, %rdi
	imulq	%rsi, %rax
	subq	%rdi, %rcx
	movl	%edx, %esi
	addl	%edx, %ecx
	shrl	$3, %ecx
	movl	%ecx, %ecx
	movq	%rax, 16(%rbx)
	movq	%rax, 8(%rbx,%rsi)
	rep stosq
	jmp	.L442
	.p2align 4,,10
	.p2align 3
.L552:
	movq	%r13, %rdi
	addq	%r14, %r12
	call	unlink_chunk.isra.2
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L542:
	movq	128(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L423
	testq	%rdi, %rdi
	je	.L424
	testb	$15, %sil
	jne	.L425
	cmpq	%rsi, %r8
	je	.L426
	xorl	%r9d, %r9d
	jmp	.L427
	.p2align 4,,10
	.p2align 3
.L429:
	cmpq	%rsi, %r8
	je	.L426
.L427:
	movq	(%rsi), %r10
	movq	%rsi, %r11
	addq	$1, %r9
	shrq	$12, %r11
	movq	%r11, %rsi
	xorq	%r10, %rsi
	cmpq	%r10, %r11
	je	.L423
	cmpq	%rdi, %r9
	je	.L424
	testb	$15, %sil
	je	.L429
.L425:
	leaq	.LC34(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L547:
	leaq	.LC39(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L548:
	movq	8(%rax), %rdx
	andq	$-8, %rdx
	addq	%rdx, %rax
	cmpq	%rax, %r13
	jb	.L457
	leaq	.LC40(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L549:
	leaq	.LC41(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L550:
	leaq	-16(%r12), %rdx
	leaq	16(%rbx), %rdi
	call	__GI_memset
	jmp	.L461
	.p2align 4,,10
	.p2align 3
.L565:
	movq	40(%rsp), %rbp
	movq	8(%rsi), %rdx
	movq	%r14, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
.L477:
	andq	$-8, %rdx
	cmpq	%rdx, mp_(%rip)
	movq	%rdx, %r13
	ja	.L471
	movq	%rdx, %r12
	subq	$33, %r12
	js	.L471
	movq	24(%rsp), %rax
	cmpq	%r12, %rax
	jnb	.L471
	subq	%rax, %r12
	movq	32(%rsp), %rax
	negq	%rax
	andq	%rax, %r12
	je	.L471
	movq	16(%r14), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	movq	%rax, 8(%rsp)
	jle	.L471
	movl	may_shrink_heap.11323(%rip), %edx
	testl	%edx, %edx
	js	.L489
	setne	%al
.L490:
	movq	8(%rsp), %rcx
	testb	%al, %al
	leaq	(%r14,%rcx), %rdi
	jne	.L567
	movl	$4, %edx
	movq	%r12, %rsi
	call	__GI___madvise
.L496:
	movq	8(%rsp), %rax
	subq	%r12, %r13
	orq	$1, %r13
	movq	%rax, 16(%r14)
	subq	%r12, 2184(%r15)
	movq	%r13, 8(%rbx)
	jmp	.L471
	.p2align 4,,10
	.p2align 3
.L556:
	movq	%rbp, %rdi
	call	malloc_consolidate
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L562:
	movq	%r15, %rdx
	movq	%r14, %r15
	movq	%rbp, %r14
	movq	40(%rsp), %rbp
	jmp	.L477
	.p2align 4,,10
	.p2align 3
.L557:
	movq	96(%rbp), %rax
	movq	8(%rax), %rax
	andq	$-8, %rax
	cmpq	mp_(%rip), %rax
	jb	.L471
	movq	8+mp_(%rip), %rdi
	call	systrim.isra.1.constprop.12
	jmp	.L471
.L499:
	movq	%rcx, %rdx
	jmp	.L445
.L546:
#APP
# 4533 "malloc.c" 1
	movl %fs:24,%r15d
# 0 "" 2
#NO_APP
	testl	%r15d, %r15d
	movl	%r15d, %eax
	movl	%r15d, 20(%rsp)
	jne	.L455
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%rbp)
# 0 "" 2
#NO_APP
	jmp	.L454
.L553:
	leaq	.LC44(%rip), %rdi
	call	malloc_printerr
.L551:
	leaq	.LC43(%rip), %rdi
	call	malloc_printerr
.L497:
	movl	20(%rsp), %r15d
#APP
# 4650 "malloc.c" 1
	xchgl %r15d, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %r15d
	jle	.L417
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 4650 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L417
.L439:
	xorl	%eax, %eax
#APP
# 4475 "malloc.c" 1
	xchgl %eax, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L440
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 4475 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L440
.L455:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	movl	$0, 20(%rsp)
	je	.L454
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L454
.L559:
	movq	8(%rbx), %rdx
	movq	%rdi, %r14
	jmp	.L477
.L564:
	leaq	__PRETTY_FUNCTION__.12000(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$669, %edx
	call	__malloc_assert
.L563:
	leaq	__PRETTY_FUNCTION__.12000(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$666, %edx
	call	__malloc_assert
.L479:
	leaq	__PRETTY_FUNCTION__.12000(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$663, %edx
	call	__malloc_assert
.L567:
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edx, %edx
	movl	$50, %ecx
	movq	%r12, %rsi
	call	__GI___mmap
	addq	$1, %rax
	je	.L471
	movq	8(%rsp), %rax
	movq	%rax, 24(%r14)
	jmp	.L496
.L489:
	movq	__libc_enable_secure@GOTPCREL(%rip), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	movl	%ecx, may_shrink_heap.11323(%rip)
	movl	%ecx, 32(%rsp)
	jne	.L492
	leaq	.LC51(%rip), %rdi
	xorl	%eax, %eax
	movl	$524288, %esi
	call	__GI___open_nocancel
	testl	%eax, %eax
	jns	.L568
.L492:
	cmpl	$0, may_shrink_heap.11323(%rip)
	setne	%al
	jmp	.L490
.L561:
	leaq	__PRETTY_FUNCTION__.12000(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$682, %edx
	call	__malloc_assert
.L560:
	leaq	__PRETTY_FUNCTION__.12000(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$681, %edx
	call	__malloc_assert
.L426:
	leaq	.LC35(%rip), %rdi
	call	malloc_printerr
.L568:
	leaq	63(%rsp), %rsi
	movl	$1, %edx
	movl	%eax, %edi
	movl	%eax, 24(%rsp)
	call	__GI___read_nocancel
	testq	%rax, %rax
	movl	24(%rsp), %r8d
	movl	32(%rsp), %ecx
	jle	.L494
	xorl	%ecx, %ecx
	cmpb	$50, 63(%rsp)
	sete	%cl
.L494:
	movl	%r8d, %edi
	movl	%ecx, may_shrink_heap.11323(%rip)
	call	__GI___close_nocancel
	jmp	.L492
.L424:
	leaq	.LC33(%rip), %rdi
	call	malloc_printerr
.L558:
	leaq	__PRETTY_FUNCTION__.13383(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC45(%rip), %rdi
	movl	$4644, %edx
	call	__malloc_assert
	.size	_int_free, .-_int_free
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"((INTERNAL_SIZE_T) chunk2rawmem (mm) & MALLOC_ALIGN_MASK) == 0"
	.align 8
.LC53:
	.string	"(old_top == initial_top (av) && old_size == 0) || ((unsigned long) (old_size) >= MINSIZE && prev_inuse (old_top) && ((unsigned long) old_end & (pagesize - 1)) == 0)"
	.align 8
.LC54:
	.string	"(unsigned long) (old_size) < (unsigned long) (nb + MINSIZE)"
	.align 8
.LC55:
	.string	"break adjusted to free malloc space"
	.section	.rodata.str1.1
.LC56:
	.string	"correction >= 0"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"((unsigned long) chunk2rawmem (brk) & MALLOC_ALIGN_MASK) == 0"
	.text
	.p2align 4,,15
	.type	sysmalloc, @function
sysmalloc:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	testq	%rsi, %rsi
	movq	24(%rax), %r15
	je	.L570
	cmpq	%rdi, 16+mp_(%rip)
	movq	%rsi, %rbp
	jbe	.L669
.L618:
	movb	$0, 16(%rsp)
.L571:
	movq	%r15, %rdi
	leaq	7(%r15,%rbx), %rax
	leaq	96(%rbp), %r10
	negq	%rdi
	andq	%rdi, %rax
	movq	%rdi, 8(%rsp)
	movq	%r10, %r14
	movq	%rax, 32(%rsp)
.L617:
	movq	96(%rbp), %r13
	movq	8(%r13), %rax
	movq	%rax, %r12
	andq	$-8, %r12
	cmpq	%r14, %r13
	leaq	0(%r13,%r12), %r11
	jne	.L625
	testq	%r12, %r12
	je	.L580
.L625:
	cmpq	$31, %r12
	jbe	.L582
	testb	$1, %al
	je	.L582
	leaq	-1(%r15), %rax
	testq	%rax, %r11
	jne	.L582
.L580:
	leaq	32(%rbx), %r10
	cmpq	%r12, %r10
	jbe	.L670
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rbp
	je	.L584
	movq	%r10, %rax
	movq	%r13, %r8
	subq	%r12, %rax
	andq	$-67108864, %r8
	testq	%rax, %rax
	jle	.L585
	leaq	-1(%rax,%r15), %rcx
	andq	8(%rsp), %rcx
	movq	16(%r8), %r9
	addq	%r9, %rcx
	cmpq	$67108864, %rcx
	ja	.L585
	movq	24(%r8), %rdi
	cmpq	%rdi, %rcx
	ja	.L671
	movq	%r10, %r14
	movq	%r13, %rsi
.L586:
	movq	2184(%rbp), %rdx
	movq	%rcx, 16(%r8)
	subq	%r9, %rdx
	addq	%rcx, %rdx
	addq	%r8, %rcx
	subq	%r13, %rcx
	movq	%rdx, 2184(%rbp)
	orq	$1, %rcx
	movq	%rcx, 8(%r13)
.L587:
	cmpq	%rdx, 2192(%rbp)
	jnb	.L611
	movq	%rdx, 2192(%rbp)
.L611:
	movq	8(%rsi), %rdx
	andq	$-8, %rdx
	cmpq	%rdx, %r14
	ja	.L612
	leaq	main_arena(%rip), %rax
	subq	%rbx, %rdx
	leaq	(%rsi,%rbx), %rcx
	cmpq	%rax, %rbp
	movq	%rcx, 96(%rbp)
	setne	%al
	orq	$1, %rbx
	orq	$1, %rdx
	movzbl	%al, %eax
	addq	$16, %rsi
	salq	$2, %rax
	orq	%rax, %rbx
	movq	%rbx, -8(%rsi)
	movq	%rdx, 8(%rcx)
.L569:
	addq	$72, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L669:
	movl	44+mp_(%rip), %eax
	cmpl	%eax, 40+mp_(%rip)
	jge	.L618
	leaq	7(%r15,%rdi), %r12
	movq	%r15, %rax
	negq	%rax
	andq	%rax, %r12
	cmpq	%r12, %rdi
	jb	.L672
.L624:
	movb	$1, 16(%rsp)
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L585:
	movq	8+mp_(%rip), %rsi
	leaq	64(%rbx), %rdi
	movq	%r10, 24(%rsp)
	movq	%r8, 40(%rsp)
	call	new_heap
	testq	%rax, %rax
	movq	24(%rsp), %r10
	je	.L588
	movq	16(%rax), %rcx
	movq	2184(%rbp), %rdx
	leaq	32(%rax), %rsi
	movq	40(%rsp), %r8
	subq	$32, %r12
	movq	%rbp, (%rax)
	andq	$-16, %r12
	movq	%r10, %r14
	addq	%rcx, %rdx
	subq	$32, %rcx
	orq	$1, %rcx
	movq	%r8, 8(%rax)
	movq	%rdx, 2184(%rbp)
	movq	%rsi, 96(%rbp)
	movq	%rcx, 40(%rax)
	leaq	16(%r12), %rax
	cmpq	$31, %r12
	leaq	0(%r13,%rax), %rcx
	movq	$1, 8(%rcx)
	ja	.L673
	movq	%rax, %rdi
	orq	$1, %rdi
	movq	%rdi, 8(%r13)
	movq	%rax, (%rcx)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L570:
	leaq	7(%r15,%rdi), %r12
	negq	%r15
	andq	%r15, %r12
	cmpq	%rdi, %r12
	jbe	.L668
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$34, %ecx
	movl	$3, %edx
	movq	%r12, %rsi
	call	__GI___mmap
	cmpq	$-1, %rax
	je	.L668
.L614:
	leaq	16(%rax), %rsi
	testb	$15, %sil
	jne	.L674
	movq	%r12, %rdx
	movq	$0, (%rax)
	orq	$2, %rdx
	movq	%rdx, 8(%rax)
	movl	$1, %edx
	lock xaddl	%edx, 40+mp_(%rip)
	addl	$1, %edx
.L577:
	movl	48+mp_(%rip), %eax
	cmpl	%eax, %edx
	jle	.L576
	lock cmpxchgl	%edx, 48+mp_(%rip)
	jne	.L577
.L576:
	movq	%r12, %r15
	lock xaddq	%r15, 56+mp_(%rip)
	addq	%r15, %r12
.L579:
	movq	64+mp_(%rip), %rax
	cmpq	%rax, %r12
	jbe	.L569
	lock cmpxchgq	%r12, 64+mp_(%rip)
	je	.L569
	jmp	.L579
	.p2align 4,,10
	.p2align 3
.L584:
	testb	$2, 4(%rbp)
	movq	8+mp_(%rip), %rax
	movq	%r10, %r14
	leaq	32(%rbx,%rax), %rdx
	je	.L675
	leaq	-1(%r15), %rdi
	movq	%r15, %rax
	negq	%rax
	leaq	(%rdx,%rdi), %r15
	movq	%rdi, 24(%rsp)
	movq	%rax, 32(%rsp)
	andq	%rax, %r15
	testq	%r15, %r15
	movq	%r15, 16(%rsp)
	jle	.L593
.L616:
	movq	__morecore@GOTPCREL(%rip), %rax
	movq	%r11, 8(%rsp)
	movq	%r15, %rdi
	call	*(%rax)
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	8(%rsp), %r11
	je	.L676
	movq	__after_morecore_hook@GOTPCREL(%rip), %rax
	movl	$1, %edi
	movq	(%rax), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	jne	.L677
.L594:
	cmpq	$0, 72+mp_(%rip)
	je	.L678
.L598:
	movq	2184+main_arena(%rip), %rdx
	addq	%r15, %rdx
	cmpq	%rcx, %r11
	movq	%rdx, 2184+main_arena(%rip)
	jne	.L599
	testb	%dil, %dil
	je	.L599
	addq	%r12, %r15
	movq	96+main_arena(%rip), %rsi
	orq	$1, %r15
	movq	%r15, 8(%r13)
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L675:
	leaq	-1(%r15), %rsi
	subq	%r12, %rdx
	movq	%r15, %rax
	negq	%rax
	leaq	(%rdx,%rsi), %r15
	movq	%rsi, 24(%rsp)
	movq	%rax, 32(%rsp)
	andq	%rax, %r15
	testq	%r15, %r15
	movq	%r15, 16(%rsp)
	jg	.L616
	.p2align 4,,10
	.p2align 3
.L591:
	movq	24(%rsp), %rax
	addq	%r12, %rax
	addq	%rax, %r15
	andq	32(%rsp), %r15
	movq	%r15, 16(%rsp)
.L593:
	movq	16(%rsp), %rsi
	movl	$1048576, %eax
	cmpq	$1048576, %rsi
	cmovbe	%rax, %rsi
	cmpq	%rsi, %rbx
	movq	%rsi, %r15
	jb	.L595
.L666:
	movq	2184+main_arena(%rip), %rdx
.L667:
	movq	96+main_arena(%rip), %rsi
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L673:
	movq	$17, 8(%r13,%r12)
	orq	$5, %r12
	movq	$16, (%rcx)
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, 8(%r13)
	movq	%rbp, %rdi
	call	_int_free
	movq	2184(%rbp), %rdx
	movq	96(%rbp), %rsi
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L671:
	movq	%rcx, %rsi
	movl	$3, %edx
	movq	%r10, 40(%rsp)
	subq	%rdi, %rsi
	addq	%r8, %rdi
	movq	%r8, 24(%rsp)
	movq	%r9, 56(%rsp)
	movq	%rcx, 48(%rsp)
	call	__GI___mprotect
	testl	%eax, %eax
	movq	24(%rsp), %r8
	movq	40(%rsp), %r10
	jne	.L585
	movq	48(%rsp), %rcx
	movq	%r10, %r14
	movq	56(%rsp), %r9
	movq	96(%rbp), %rsi
	movq	%rcx, 24(%r8)
	jmp	.L586
	.p2align 4,,10
	.p2align 3
.L588:
	cmpb	$0, 16(%rsp)
	jne	.L679
.L573:
	movq	32(%rsp), %rax
	cmpq	%rbx, %rax
	movq	%rax, %r12
	ja	.L680
.L574:
	movb	$1, 16(%rsp)
	jmp	.L617
	.p2align 4,,10
	.p2align 3
.L612:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L668:
	xorl	%esi, %esi
	jmp	.L569
	.p2align 4,,10
	.p2align 3
.L599:
	testb	$2, 4+main_arena(%rip)
	jne	.L600
	testq	%r12, %r12
	je	.L601
	cmpq	%rcx, %r11
	ja	.L681
.L601:
	testq	%r12, %r12
	je	.L602
	movq	%rcx, %rax
	subq	%r11, %rax
	addq	%rax, %rdx
	movq	%rdx, 2184+main_arena(%rip)
.L602:
	movq	%rcx, %rdx
	andl	$15, %edx
	je	.L621
	movl	$16, %eax
	subq	%rdx, %rax
	leaq	(%rcx,%rax), %rsi
	addq	%r12, %rax
.L603:
	addq	%rax, %r15
	addq	%r15, %rcx
	movq	24(%rsp), %r15
	subq	%rcx, %rax
	addq	%rcx, %r15
	andq	32(%rsp), %r15
	addq	%rax, %r15
	testq	%r15, %r15
	js	.L682
	movq	__morecore@GOTPCREL(%rip), %rax
	movq	%rsi, 16(%rsp)
	movq	%r15, %rdi
	movq	%r15, 8(%rsp)
	call	*(%rax)
	testq	%rax, %rax
	movq	8(%rsp), %rcx
	movq	16(%rsp), %rsi
	jne	.L605
	movq	__morecore@GOTPCREL(%rip), %rax
	movq	%rsi, 8(%rsp)
	xorl	%edi, %edi
	call	*(%rax)
	movq	8(%rsp), %rsi
	jmp	.L606
	.p2align 4,,10
	.p2align 3
.L676:
	testb	$2, 4+main_arena(%rip)
	je	.L591
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L679:
	movq	%r10, %r14
	movq	2184(%rbp), %rdx
	movq	96(%rbp), %rsi
	jmp	.L587
	.p2align 4,,10
	.p2align 3
.L678:
	movq	%rcx, 72+mp_(%rip)
	jmp	.L598
	.p2align 4,,10
	.p2align 3
.L600:
	testb	$15, %cl
	jne	.L683
	testq	%rax, %rax
	jne	.L622
	movq	__morecore@GOTPCREL(%rip), %rax
	movq	%rcx, 8(%rsp)
	xorl	%edi, %edi
	call	*(%rax)
	movq	8(%rsp), %rcx
	movq	%rcx, %rsi
.L606:
	testq	%rax, %rax
	movq	2184+main_arena(%rip), %rdx
	je	.L667
.L665:
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L608:
	subq	%rsi, %rax
	addq	%r15, %rdx
	movq	%rsi, 96+main_arena(%rip)
	addq	%rcx, %rax
	orq	$1, %rax
	testq	%r12, %r12
	movq	%rax, 8(%rsi)
	movq	%rdx, 2184+main_arena(%rip)
	je	.L587
	subq	$32, %r12
	andq	$-16, %r12
	movq	%r12, %rax
	orq	$1, %rax
	cmpq	$31, %r12
	movq	%rax, 8(%r13)
	movq	$17, 8(%r13,%r12)
	movq	$17, 24(%r13,%r12)
	jbe	.L587
	leaq	main_arena(%rip), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_int_free
	jmp	.L666
	.p2align 4,,10
	.p2align 3
.L595:
	xorl	%r9d, %r9d
	movl	$34, %ecx
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$3, %edx
	movq	%r11, 8(%rsp)
	call	__GI___mmap
	cmpq	$-1, %rax
	movq	%rax, %rcx
	movq	8(%rsp), %r11
	je	.L666
	orl	$2, 4+main_arena(%rip)
	testq	%rax, %rax
	je	.L666
	addq	%r15, %rax
	sete	%dil
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L621:
	movq	%r12, %rax
	movq	%rcx, %rsi
	jmp	.L603
	.p2align 4,,10
	.p2align 3
.L677:
	movb	%dil, 48(%rsp)
	movq	%rcx, 40(%rsp)
	movq	%r11, 16(%rsp)
	movq	%rax, 8(%rsp)
	call	*%rsi
	movq	8(%rsp), %rax
	movq	16(%rsp), %r11
	movq	40(%rsp), %rcx
	movzbl	48(%rsp), %edi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L605:
	movq	__after_morecore_hook@GOTPCREL(%rip), %rdx
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L607
	movq	2184+main_arena(%rip), %rdx
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L622:
	movq	%rcx, %rsi
	jmp	.L665
.L607:
	movq	%rsi, 24(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rax, 8(%rsp)
	call	*%rdx
	movq	2184+main_arena(%rip), %rdx
	movq	8(%rsp), %rax
	movq	16(%rsp), %rcx
	movq	24(%rsp), %rsi
	jmp	.L608
.L582:
	leaq	__PRETTY_FUNCTION__.12774(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC53(%rip), %rdi
	movl	$2542, %edx
	call	__malloc_assert
.L670:
	leaq	__PRETTY_FUNCTION__.12774(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC54(%rip), %rdi
	movl	$2545, %edx
	call	__malloc_assert
.L674:
	leaq	__PRETTY_FUNCTION__.12774(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	movl	$2487, %edx
	call	__malloc_assert
.L680:
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	movq	%rax, %rsi
	call	__GI___mmap
	cmpq	$-1, %rax
	jne	.L614
	jmp	.L574
.L683:
	leaq	__PRETTY_FUNCTION__.12774(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC57(%rip), %rdi
	movl	$2796, %edx
	call	__malloc_assert
.L672:
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	movq	%r12, %rsi
	call	__GI___mmap
	cmpq	$-1, %rax
	jne	.L614
	jmp	.L624
.L681:
	leaq	.LC55(%rip), %rdi
	call	malloc_printerr
.L682:
	leaq	__PRETTY_FUNCTION__.12774(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	movl	$2764, %edx
	call	__malloc_assert
	.size	sysmalloc, .-sysmalloc
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"malloc(): unaligned fastbin chunk detected 2"
	.align 8
.LC59:
	.string	"malloc(): unaligned fastbin chunk detected"
	.align 8
.LC60:
	.string	"malloc(): memory corruption (fast)"
	.align 8
.LC61:
	.string	"malloc(): unaligned fastbin chunk detected 3"
	.align 8
.LC62:
	.string	"malloc(): smallbin double linked list corrupted"
	.align 8
.LC63:
	.string	"malloc(): invalid size (unsorted)"
	.align 8
.LC64:
	.string	"malloc(): invalid next size (unsorted)"
	.align 8
.LC65:
	.string	"malloc(): mismatching next->prev_size (unsorted)"
	.align 8
.LC66:
	.string	"malloc(): unsorted double linked list corrupted"
	.align 8
.LC67:
	.string	"malloc(): invalid next->prev_inuse (unsorted)"
	.section	.rodata.str1.1
.LC68:
	.string	"chunk_main_arena (bck->bk)"
.LC69:
	.string	"chunk_main_arena (fwd)"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"malloc(): largebin double linked list corrupted (nextsize)"
	.align 8
.LC71:
	.string	"malloc(): largebin double linked list corrupted (bk)"
	.align 8
.LC72:
	.string	"malloc(): unaligned tcache chunk detected"
	.align 8
.LC73:
	.string	"malloc(): corrupted unsorted chunks"
	.section	.rodata.str1.1
.LC74:
	.string	"bit != 0"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"(unsigned long) (size) >= (unsigned long) (nb)"
	.align 8
.LC76:
	.string	"malloc(): corrupted unsorted chunks 2"
	.section	.rodata.str1.1
.LC77:
	.string	"malloc(): corrupted top size"
	.text
	.p2align 4,,15
	.type	_int_malloc, @function
_int_malloc:
	testq	%rsi, %rsi
	js	.L911
	pushq	%r15
	leaq	23(%rsi), %rax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$152, %rsp
	cmpq	$31, %rax
	jbe	.L912
	andq	$-16, %rax
	testq	%rdi, %rdi
	movq	%rax, %r14
	je	.L688
	movl	%eax, %ebx
	shrl	$4, %ebx
	cmpq	global_max_fast(%rip), %rax
	ja	.L693
	leal	-2(%rbx), %eax
	leaq	(%r12,%rax,8), %rdi
	movq	%rax, %r8
	leaq	16(,%rax,8), %rsi
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L698
.L694:
	testb	$15, %cl
	jne	.L913
	addq	%r12, %rsi
#APP
# 3807 "malloc.c" 1
	movl %fs:24,%r9d
# 0 "" 2
#NO_APP
	leaq	16(%rcx), %rdx
	movq	16(%rcx), %r10
	movq	%rdx, %rax
	shrq	$12, %rax
	xorq	%r10, %rax
	testl	%r9d, %r9d
	jne	.L696
	movq	%rax, 16(%rdi)
.L697:
	movl	8(%rcx), %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpl	%r8d, %eax
	jne	.L914
	movq	tcache@gottpoff(%rip), %r15
	movq	%fs:(%r15), %rax
	testq	%rax, %rax
	je	.L719
	leaq	-17(%r14), %r9
	shrq	$4, %r9
	cmpq	%r9, 80+mp_(%rip)
	jbe	.L719
	movzwl	(%rax,%r9,2), %eax
	cmpq	%rax, 96+mp_(%rip)
	jbe	.L719
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L719
	testb	$15, %dl
	jne	.L704
	leaq	0(,%r9,8), %r10
	addq	%r9, %r9
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L915:
	movq	%r12, 16(%rdi)
.L707:
	movq	%fs:(%r15), %rax
	leaq	(%rax,%r10), %r8
	movq	%rax, 24(%rdx)
	xorq	128(%r8), %r11
	movq	%r11, 16(%rdx)
	leaq	(%rax,%r9), %rdx
	movq	%rbp, 128(%r8)
	movzwl	(%rdx), %eax
	addl	$1, %eax
	movw	%ax, (%rdx)
	movzwl	%ax, %eax
	cmpq	96+mp_(%rip), %rax
	jnb	.L719
	movq	16(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L719
	testb	$15, %dl
	jne	.L704
.L705:
#APP
# 3831 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	leaq	16(%rdx), %rbp
	movq	16(%rdx), %rbx
	movq	%rbp, %r11
	movq	%rbp, %r8
	shrq	$12, %r11
	movq	%rbx, %r12
	xorq	%r11, %r12
	testl	%eax, %eax
	je	.L915
	jmp	.L706
	.p2align 4,,10
	.p2align 3
.L912:
	testq	%rdi, %rdi
	je	.L798
	cmpq	$31, global_max_fast(%rip)
	ja	.L799
	movl	$2, %ebx
	movl	$128, %eax
	movl	$32, %r14d
.L690:
	addq	%r12, %rax
	movq	8(%rax), %rcx
	leaq	-16(%rax), %r8
	cmpq	%rcx, %r8
	je	.L916
	movq	24(%rcx), %rdx
	cmpq	%rcx, 16(%rdx)
	jne	.L917
	leaq	main_arena(%rip), %r10
	orq	$1, 8(%rcx,%r14)
	movq	%rdx, 8(%rax)
	movq	%r8, 16(%rdx)
	cmpq	%r10, %r12
	je	.L716
	orq	$4, 8(%rcx)
.L716:
	movq	tcache@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %r9
	testq	%r9, %r9
	je	.L719
	leaq	-17(%r14), %rdx
	shrq	$4, %rdx
	cmpq	%rdx, 80+mp_(%rip)
	jbe	.L719
	leaq	(%r9,%rdx,2), %rbx
	movq	96+mp_(%rip), %rdi
	leaq	(%r9,%rdx,8), %rbp
	movzwl	(%rbx), %r11d
.L720:
	movzwl	%r11w, %esi
	jmp	.L721
	.p2align 4,,10
	.p2align 3
.L919:
	movq	8(%rax), %rdx
	cmpq	%rdx, %r8
	je	.L719
	testq	%rdx, %rdx
	jne	.L918
.L721:
	cmpq	%rdi, %rsi
	jb	.L919
	.p2align 4,,10
	.p2align 3
.L719:
	movl	perturb_byte(%rip), %esi
	addq	$16, %rcx
	testl	%esi, %esi
	jne	.L902
.L684:
	addq	$152, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L920:
	testb	$15, %r9b
	jne	.L709
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%rcx, %rax
#APP
# 3810 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %r9, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rcx, %rax
	je	.L697
	testq	%rax, %rax
	je	.L698
	movq	16(%rax), %r10
	leaq	16(%rax), %rdx
	movq	%rax, %rcx
.L696:
	shrq	$12, %rdx
	movq	%rdx, %r9
	xorq	%r10, %r9
	cmpq	%r10, %rdx
	je	.L699
	jmp	.L920
	.p2align 4,,10
	.p2align 3
.L693:
	cmpq	$1023, %rax
	jbe	.L698
	movq	%rax, %rcx
	movq	%rax, %rdi
	movq	%r14, %rax
	shrq	$18, %rax
	movq	%r14, %rsi
	movq	%r14, %r15
	shrq	$12, %rsi
	shrq	$15, %r15
	shrq	$6, %rcx
	shrq	$9, %rdi
	movl	$2, %edx
	cmpq	$2, %rax
	cmova	%rdx, %rax
	leal	48(%rcx), %r9d
	leal	119(%r15), %r10d
	leal	124(%rax), %r8d
	movq	%rsi, 104(%rsp)
	movq	%r15, 112(%rsp)
	movq	%r15, %rdx
	movq	%rsi, %rax
	leal	110(%rsi), %r15d
	movl	%edi, %esi
	movq	%rcx, 80(%rsp)
	movq	%rdi, 96(%rsp)
	addl	$91, %esi
	cmpq	$48, %rcx
	movl	%r9d, 92(%rsp)
	movl	%r8d, 124(%rsp)
	movl	%r10d, 128(%rsp)
	movl	%r15d, 132(%rsp)
	movl	%esi, 120(%rsp)
	movl	%r9d, 12(%rsp)
	jbe	.L724
	cmpq	$20, %rdi
	movl	%esi, 12(%rsp)
	jbe	.L724
	cmpq	$10, %rax
	movl	%r15d, 12(%rsp)
	ja	.L921
	.p2align 4,,10
	.p2align 3
.L724:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L922
.L714:
	movq	tcache@gottpoff(%rip), %r15
	leaq	-17(%r14), %rax
	shrq	$4, %rax
	movq	%fs:(%r15), %r10
	movq	%rax, %r11
	testq	%r10, %r10
	je	.L804
	cmpq	%rax, 80+mp_(%rip)
	movl	$0, %eax
	cmova	%r14, %rax
	movq	%rax, 32(%rsp)
.L725:
	leaq	96(%r12), %rbp
	movq	$0, (%rsp)
	movl	%ebx, 88(%rsp)
	movq	%r15, 72(%rsp)
	movq	%r13, 48(%rsp)
.L726:
	movq	96+mp_(%rip), %rax
	movq	104+mp_(%rip), %r15
	xorl	%r9d, %r9d
	movq	120(%r12), %rdx
	xorl	%r13d, %r13d
	movq	%r11, 40(%rsp)
	movq	%rax, 24(%rsp)
	leaq	32(%r14), %rax
	movq	%rax, 64(%rsp)
	leaq	(%r10,%r11,2), %rax
	movq	%rax, 16(%rsp)
	leaq	(%r10,%r11,8), %rax
	movq	%rax, 56(%rsp)
.L727:
	cmpq	%rdx, %rbp
	je	.L923
	movq	8(%rdx), %rax
	andq	$-8, %rax
	cmpq	$16, %rax
	leaq	(%rdx,%rax), %rdi
	jbe	.L728
	movq	2184(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L728
	movq	8(%rdi), %rcx
	cmpq	$15, %rcx
	jbe	.L816
	cmpq	%rcx, %rsi
	jb	.L816
	movq	(%rdi), %rsi
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	jne	.L924
	movq	24(%rdx), %r8
	cmpq	%rdx, 16(%r8)
	jne	.L733
	movq	16(%rdx), %rbx
	cmpq	%rbp, %rbx
	jne	.L733
	testb	$1, %cl
	jne	.L925
	cmpq	$1023, %r14
	ja	.L736
	cmpq	%rbp, %r8
	je	.L926
.L736:
	cmpq	%rax, %r14
	movq	%r8, 120(%r12)
	movq	%rbp, 16(%r8)
	je	.L927
	cmpq	$1023, %rax
	ja	.L743
	movl	%eax, %ecx
	shrl	$4, %ecx
	leal	-2(%rcx,%rcx), %eax
	cltq
	leaq	112(%r12,%rax,8), %rax
	movq	(%rax), %rdi
	leaq	-16(%rax), %r8
.L744:
	movl	%ecx, %eax
	movl	$1, %ebx
	addq	$1, (%rsp)
	sarl	$5, %eax
	sall	%cl, %ebx
	cltq
	orl	%ebx, 2144(%r12,%rax,4)
	testl	%r13d, %r13d
	movq	%r8, 24(%rdx)
	movq	%rdi, 16(%rdx)
	movq	(%rsp), %rax
	movq	%rdx, 24(%rdi)
	movq	%rdx, 16(%r8)
	je	.L758
	testq	%r15, %r15
	je	.L759
	cmpq	%rax, %r15
	jnb	.L759
.L910:
	movq	40(%rsp), %r11
.L909:
	movq	72(%rsp), %r15
	movq	%fs:(%r15), %rax
	leaq	(%rax,%r11,8), %rsi
	movq	128(%rsi), %rcx
	testb	$15, %cl
	jne	.L928
	movq	%rcx, %rdx
	shrq	$12, %rdx
	xorq	(%rcx), %rdx
	movq	%rdx, 128(%rsi)
	subw	$1, (%rax,%r11,2)
	movq	$0, 8(%rcx)
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L799:
	xorl	%eax, %eax
	xorl	%r8d, %r8d
	movl	$16, %esi
	leaq	(%r12,%rax,8), %rdi
	movl	$2, %ebx
	movl	$32, %r14d
	movq	16(%rdi), %rcx
	testq	%rcx, %rcx
	jne	.L694
.L698:
	leal	2147483647(%rbx), %eax
	addl	%eax, %eax
	leaq	112(,%rax,8), %rax
	jmp	.L690
	.p2align 4,,10
	.p2align 3
.L708:
	movq	%rdx, %rax
#APP
# 3835 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %r11, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rax
	je	.L929
	testq	%rax, %rax
	je	.L719
	movq	16(%rax), %rbx
	leaq	16(%rax), %r8
	movq	%rax, %rdx
.L706:
	shrq	$12, %r8
	movq	%r8, %r11
	xorq	%rbx, %r11
	cmpq	%rbx, %r8
	je	.L708
	testb	$15, %r11b
	je	.L708
.L709:
	leaq	.LC59(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L922:
	movq	%r12, %rdi
	call	malloc_consolidate
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L916:
	movq	%r14, %rdi
	movq	%r14, %rax
	movl	%ebx, 12(%rsp)
	shrq	$9, %rdi
	shrq	$6, %rax
	movl	$110, 132(%rsp)
	leal	91(%rdi), %ecx
	movq	%rax, 80(%rsp)
	addl	$48, %eax
	movq	%rdi, 96(%rsp)
	movl	%eax, 92(%rsp)
	movl	%ecx, 120(%rsp)
	movl	$119, 128(%rsp)
	movl	$124, 124(%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 104(%rsp)
	jmp	.L714
	.p2align 4,,10
	.p2align 3
.L927:
	leaq	main_arena(%rip), %rax
	orq	$1, %rcx
	movq	%rcx, 8(%rdi)
	cmpq	%rax, %r12
	je	.L740
	orq	$4, 8(%rdx)
.L740:
	cmpq	$0, 32(%rsp)
	leaq	16(%rdx), %rcx
	je	.L741
	movq	16(%rsp), %rax
	movzwl	(%rax), %esi
	cmpq	24(%rsp), %rsi
	movq	%rsi, %rax
	jb	.L930
.L741:
	movq	48(%rsp), %r13
.L905:
	movl	perturb_byte(%rip), %esi
	testl	%esi, %esi
	je	.L684
.L902:
	movq	%rcx, %rdi
	xorb	$-1, %sil
	movq	%r13, %rdx
	call	__GI_memset
	movq	%rax, %rcx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L759:
	addl	$1, %r9d
	cmpl	$9999, %r9d
	jg	.L910
.L899:
	movq	120(%r12), %rdx
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L728:
	leaq	.LC63(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L911:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movl	$12, %fs:(%rax)
	movq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L798:
	movl	$32, %r14d
.L688:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	sysmalloc
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L905
.L794:
	xorl	%ecx, %ecx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L816:
	leaq	.LC64(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L743:
	movq	%rax, %rcx
	shrq	$6, %rcx
	cmpq	$48, %rcx
	jbe	.L931
	movq	%rax, %rcx
	shrq	$9, %rcx
	cmpq	$20, %rcx
	ja	.L747
	addl	$91, %ecx
.L746:
	leal	-2(%rcx,%rcx), %esi
	movslq	%esi, %rsi
	leaq	112(%r12,%rsi,8), %r8
	movq	(%r8), %rsi
	leaq	-16(%r8), %rdi
	cmpq	%rsi, %rdi
	je	.L750
	movq	8(%r8), %r8
	orq	$1, %rax
	movq	8(%r8), %rbx
	testb	$4, %bl
	jne	.L932
	cmpq	%rax, %rbx
	jbe	.L752
	movq	40(%rsi), %rax
	movq	%rsi, 32(%rdx)
	movq	%rax, 40(%rdx)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rsi)
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L758:
	addl	$1, %r9d
	cmpl	$9999, %r9d
	jle	.L899
	movq	40(%rsp), %r11
.L762:
	cmpq	$1023, %r14
	jbe	.L765
	movl	12(%rsp), %eax
	addl	$2147483647, %eax
	addl	%eax, %eax
	leaq	112(%r12,%rax,8), %rax
	movq	(%rax), %rdx
	leaq	-16(%rax), %rcx
	cmpq	%rcx, %rdx
	je	.L765
	cmpq	%r14, 8(%rdx)
	jnb	.L933
.L765:
	movl	12(%rsp), %eax
	leal	1(%rax), %ecx
	addl	%eax, %eax
	leaq	96(%r12,%rax,8), %rdx
	movl	$1, %eax
	movl	%ecx, %edi
	sall	%cl, %eax
	shrl	$5, %edi
	movl	%edi, %r9d
	movl	2144(%r12,%r9,4), %esi
.L781:
	cmpl	%esi, %eax
	ja	.L777
	testl	%eax, %eax
	jne	.L778
	jmp	.L777
	.p2align 4,,10
	.p2align 3
.L779:
	addq	$16, %rdx
	addl	%eax, %eax
	je	.L934
.L778:
	testl	%esi, %eax
	je	.L779
	movq	24(%rdx), %r8
	cmpq	%r8, %rdx
	jne	.L780
	movl	%eax, %ecx
	addq	$16, %rdx
	addl	%eax, %eax
	notl	%ecx
	andl	%ecx, %esi
	movl	%esi, 2144(%r12,%r9,4)
	jmp	.L781
	.p2align 4,,10
	.p2align 3
.L804:
	movq	$0, 32(%rsp)
	jmp	.L725
	.p2align 4,,10
	.p2align 3
.L733:
	leaq	.LC66(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L931:
	addl	$48, %ecx
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L921:
	cmpq	$4, %rdx
	cmova	%r8d, %r10d
	movl	%r10d, 12(%rsp)
	jmp	.L724
	.p2align 4,,10
	.p2align 3
.L926:
	movq	104(%r12), %r11
	cmpq	%rdx, %r11
	movq	%r11, 136(%rsp)
	jne	.L736
	cmpq	%rax, 64(%rsp)
	jnb	.L736
	subq	%r14, %rsi
	addq	%r14, %rdx
	movq	48(%rsp), %r13
	cmpq	$1023, %rsi
	movq	%rdx, 112(%r12)
	movq	%rdx, 120(%r12)
	movq	%rdx, 104(%r12)
	movq	%rbx, 16(%rdx)
	movq	%rbx, 24(%rdx)
	jbe	.L737
	movq	$0, 32(%rdx)
	movq	$0, 40(%rdx)
.L737:
	leaq	main_arena(%rip), %rax
	xorl	%ecx, %ecx
	movq	136(%rsp), %rbx
	cmpq	%rax, %r12
	movq	%r14, %rax
	setne	%cl
	orq	$1, %rax
	salq	$2, %rcx
	orq	%rcx, %rax
	leaq	16(%rbx), %rcx
	movq	%rax, 8(%rbx)
	movq	%rsi, %rax
	orq	$1, %rax
	movq	%rax, 8(%rdx)
	movl	perturb_byte(%rip), %eax
	movq	%rsi, (%rdi)
	testl	%eax, %eax
	je	.L684
.L901:
	xorb	$-1, %al
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movl	%eax, %esi
	call	__GI_memset
	movq	%rax, %rcx
	jmp	.L684
	.p2align 4,,10
	.p2align 3
.L936:
	movl	%edi, %r9d
	movl	2144(%r12,%r9,4), %esi
	testl	%esi, %esi
	jne	.L935
.L777:
	addl	$1, %edi
	cmpl	$4, %edi
	jne	.L936
.L776:
	movq	96(%r12), %rcx
	movq	8(%rcx), %rax
	andq	$-8, %rax
	cmpq	%rax, 2184(%r12)
	jb	.L937
	leaq	32(%r14), %rdx
	cmpq	%rax, %rdx
	jbe	.L938
	movq	%r11, 16(%rsp)
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L792
	movq	%r12, %rdi
	call	malloc_consolidate
	movl	88(%rsp), %eax
	cmpq	$1023, %r14
	movq	16(%rsp), %r11
	movl	%eax, 12(%rsp)
	jbe	.L793
	cmpq	$48, 80(%rsp)
	movl	92(%rsp), %eax
	movl	%eax, 12(%rsp)
	jbe	.L793
	cmpq	$20, 96(%rsp)
	movl	120(%rsp), %eax
	movl	%eax, 12(%rsp)
	jbe	.L793
	cmpq	$10, 104(%rsp)
	movl	132(%rsp), %eax
	movl	%eax, 12(%rsp)
	jbe	.L793
	cmpq	$5, 112(%rsp)
	movl	124(%rsp), %eax
	cmovb	128(%rsp), %eax
	movl	%eax, 12(%rsp)
.L793:
	movq	72(%rsp), %rax
	movq	%fs:(%rax), %r10
	jmp	.L726
	.p2align 4,,10
	.p2align 3
.L704:
	leaq	.LC61(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L924:
	leaq	.LC65(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L913:
	leaq	.LC58(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L918:
	movq	24(%rdx), %rsi
	orq	$1, 8(%rdx,%r14)
	cmpq	%r10, %r12
	je	.L722
	orq	$4, 8(%rdx)
.L722:
	leaq	16(%rdx), %r15
	movq	%rsi, 8(%rax)
	addl	$1, %r11d
	movq	%r8, 16(%rsi)
	movq	%r15, %rsi
	movq	%r9, 24(%rdx)
	shrq	$12, %rsi
	xorq	128(%rbp), %rsi
	movq	%rsi, 16(%rdx)
	movq	%r15, 128(%rbp)
	movw	%r11w, (%rbx)
	jmp	.L720
	.p2align 4,,10
	.p2align 3
.L925:
	leaq	.LC67(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L930:
	movq	56(%rsp), %rbx
	movq	%rcx, %rsi
	movq	%r10, 24(%rdx)
	shrq	$12, %rsi
	addl	$1, %eax
	movl	$1, %r13d
	xorq	128(%rbx), %rsi
	movq	%rsi, 16(%rdx)
	movq	%rcx, 128(%rbx)
	movq	%r8, %rdx
	movq	16(%rsp), %rbx
	movw	%ax, (%rbx)
	jmp	.L727
	.p2align 4,,10
	.p2align 3
.L914:
	leaq	.LC60(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L750:
	movq	%rdx, 40(%rdx)
	movq	%rdx, 32(%rdx)
	movq	%rdi, %r8
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L917:
	leaq	.LC62(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L747:
	movq	%rax, %rcx
	shrq	$12, %rcx
	cmpq	$10, %rcx
	ja	.L748
	addl	$110, %ecx
	jmp	.L746
	.p2align 4,,10
	.p2align 3
.L752:
	movq	8(%rsi), %rdi
	testb	$4, %dil
	jne	.L939
	movq	40(%rsp), %r11
	jmp	.L753
	.p2align 4,,10
	.p2align 3
.L754:
	movq	32(%rsi), %rsi
	movq	8(%rsi), %rdi
	testb	$4, %dil
	jne	.L940
.L753:
	cmpq	%rdi, %rax
	jb	.L754
	movq	%r11, 40(%rsp)
	je	.L941
	movq	40(%rsi), %rax
	movq	%rsi, 32(%rdx)
	movq	%rax, 40(%rdx)
	movq	40(%rsi), %rax
	cmpq	%rsi, 32(%rax)
	jne	.L942
	movq	%rdx, 40(%rsi)
	movq	40(%rdx), %rax
	movq	%rsi, %rdi
	movq	%rdx, 32(%rax)
.L756:
	movq	24(%rdi), %r8
	cmpq	%rdi, 16(%r8)
	je	.L744
	leaq	.LC71(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L929:
	leaq	16(%rdx), %rbp
	movq	%rbp, %r11
	shrq	$12, %r11
	jmp	.L707
	.p2align 4,,10
	.p2align 3
.L923:
	testl	%r13d, %r13d
	movq	40(%rsp), %r11
	je	.L762
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L935:
	movl	%edi, %eax
	sall	$6, %eax
	subl	$2, %eax
	leaq	96(%r12,%rax,8), %rdx
	movl	$1, %eax
	jmp	.L778
.L748:
	movq	%rax, %rcx
	shrq	$15, %rcx
	cmpq	$4, %rcx
	ja	.L749
	addl	$119, %ecx
	jmp	.L746
.L941:
	movq	16(%rsi), %rdi
	jmp	.L756
.L749:
	movq	%rax, %rcx
	movl	$2, %ebx
	shrq	$18, %rcx
	cmpq	$2, %rcx
	cmova	%rbx, %rcx
	addl	$124, %ecx
	jmp	.L746
.L780:
	movq	8(%r8), %r15
	movq	48(%rsp), %r13
	andq	$-8, %r15
	cmpq	%r15, %r14
	ja	.L943
	movq	%r15, %rbx
	movq	%r8, %rdi
	movq	%r8, (%rsp)
	subq	%r14, %rbx
	call	unlink_chunk.isra.2
	cmpq	$31, %rbx
	movq	(%rsp), %r8
	ja	.L783
	leaq	main_arena(%rip), %rax
	orq	$1, 8(%r8,%r15)
	cmpq	%rax, %r12
	je	.L784
	orq	$4, 8(%r8)
.L784:
	movl	perturb_byte(%rip), %eax
	leaq	16(%r8), %rcx
	testl	%eax, %eax
	je	.L684
	jmp	.L901
.L783:
	movq	112(%r12), %rdx
	leaq	(%r8,%r14), %rax
	cmpq	%rbp, 24(%rdx)
	jne	.L944
	cmpq	$1023, %r14
	movq	%rbp, 24(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 112(%r12)
	movq	%rax, 24(%rdx)
	ja	.L786
	movq	%rax, 104(%r12)
.L786:
	cmpq	$1023, %rbx
	jbe	.L787
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
.L787:
	leaq	main_arena(%rip), %rdx
	xorl	%ecx, %ecx
	cmpq	%rdx, %r12
	movq	%r14, %rdx
	setne	%cl
	orq	$1, %rdx
	salq	$2, %rcx
	orq	%rcx, %rdx
	movq	%rdx, 8(%r8)
	movq	%rbx, %rdx
	orq	$1, %rdx
	movq	%rdx, 8(%rax)
	movq	%rbx, (%r8,%r15)
	jmp	.L784
.L942:
	leaq	.LC70(%rip), %rdi
	call	malloc_printerr
.L937:
	leaq	.LC77(%rip), %rdi
	call	malloc_printerr
.L792:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	48(%rsp), %r13
	call	sysmalloc
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L794
	movl	perturb_byte(%rip), %eax
	testl	%eax, %eax
	je	.L684
	jmp	.L901
.L938:
	leaq	main_arena(%rip), %rdx
	subq	%r14, %rax
	xorl	%esi, %esi
	leaq	(%rcx,%r14), %rdi
	movq	48(%rsp), %r13
	cmpq	%rdx, %r12
	movq	%r14, %rdx
	setne	%sil
	orq	$1, %rdx
	orq	$1, %rax
	salq	$2, %rsi
	movq	%rdi, 96(%r12)
	addq	$16, %rcx
	orq	%rsi, %rdx
	movq	%rdx, -8(%rcx)
	movq	%rax, 8(%rdi)
	movl	perturb_byte(%rip), %eax
	testl	%eax, %eax
	je	.L684
	jmp	.L901
.L934:
	leaq	__PRETTY_FUNCTION__.13274(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC74(%rip), %rdi
	movl	$4240, %edx
	call	__malloc_assert
.L933:
	movq	48(%rsp), %r13
	movq	40(%rdx), %rbx
	jmp	.L766
.L767:
	movq	40(%rbx), %rbx
.L766:
	movq	8(%rbx), %rdx
	movq	%rdx, %r15
	andq	$-8, %r15
	cmpq	%r15, %r14
	ja	.L767
	cmpq	%rbx, 8(%rax)
	je	.L768
	movq	16(%rbx), %rax
	cmpq	8(%rax), %rdx
	cmove	%rax, %rbx
.L768:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	subq	%r14, %rsi
	movq	%rsi, (%rsp)
	call	unlink_chunk.isra.2
	movq	(%rsp), %rsi
	cmpq	$31, %rsi
	ja	.L769
	leaq	main_arena(%rip), %rax
	orq	$1, 8(%rbx,%r15)
	cmpq	%rax, %r12
	je	.L770
	orq	$4, 8(%rbx)
.L770:
	movl	perturb_byte(%rip), %eax
	leaq	16(%rbx), %rcx
	testl	%eax, %eax
	je	.L684
	jmp	.L901
.L940:
	leaq	__PRETTY_FUNCTION__.13274(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC69(%rip), %rdi
	movl	$4079, %edx
	call	__malloc_assert
.L928:
	leaq	.LC72(%rip), %rdi
	call	malloc_printerr
.L944:
	leaq	.LC76(%rip), %rdi
	call	malloc_printerr
.L932:
	leaq	__PRETTY_FUNCTION__.13274(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC68(%rip), %rdi
	movl	$4062, %edx
	call	__malloc_assert
.L943:
	leaq	__PRETTY_FUNCTION__.13274(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC75(%rip), %rdi
	movl	$4259, %edx
	call	__malloc_assert
.L769:
	movq	112(%r12), %rdx
	leaq	(%rbx,%r14), %rax
	cmpq	%rbp, 24(%rdx)
	jne	.L945
	cmpq	$1023, %rsi
	movq	%rbp, 24(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 112(%r12)
	movq	%rax, 24(%rdx)
	jbe	.L772
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
.L772:
	leaq	main_arena(%rip), %rdx
	xorl	%ecx, %ecx
	cmpq	%rdx, %r12
	movq	%r14, %rdx
	setne	%cl
	orq	$1, %rdx
	salq	$2, %rcx
	orq	%rcx, %rdx
	movq	%rdx, 8(%rbx)
	movq	%rsi, %rdx
	orq	$1, %rdx
	movq	%rdx, 8(%rax)
	movq	%rsi, (%rbx,%r15)
	jmp	.L770
.L945:
	leaq	.LC73(%rip), %rdi
	call	malloc_printerr
.L939:
	leaq	__PRETTY_FUNCTION__.13274(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC69(%rip), %rdi
	movl	$4075, %edx
	call	__malloc_assert
	.size	_int_malloc, .-_int_malloc
	.p2align 4,,15
	.type	malloc_check, @function
malloc_check:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	addq	$1, %rbp
	jc	.L948
	movq	%rdi, %rbx
#APP
# 248 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L951
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L952:
	call	top_check
	leaq	main_arena(%rip), %rdi
	movq	%rbp, %rsi
	call	_int_malloc
	movq	%rax, %r8
#APP
# 251 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L953
	subl	$1, main_arena(%rip)
.L954:
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%rbp
	jmp	mem2mem_check
	.p2align 4,,10
	.p2align 3
.L951:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L952
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L952
	.p2align 4,,10
	.p2align 3
.L953:
	xorl	%eax, %eax
#APP
# 251 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L954
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 251 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L954
.L948:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rdx
	popq	%rbx
	popq	%rbp
	ret
	.size	malloc_check, .-malloc_check
	.p2align 4,,15
	.type	tcache_init.part.6, @function
tcache_init.part.6:
	movq	thread_arena@gottpoff(%rip), %rax
	pushq	%rbx
	movq	%fs:(%rax), %rbx
	testq	%rbx, %rbx
	je	.L959
#APP
# 3151 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L960
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L961:
	movl	$640, %esi
	movq	%rbx, %rdi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L966
.L967:
#APP
# 3161 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L964
	subl	$1, (%rbx)
.L963:
	testq	%r8, %r8
	je	.L958
	movq	tcache@gottpoff(%rip), %rax
	leaq	8(%r8), %rdi
	andq	$-8, %rdi
	movq	%r8, %fs:(%rax)
	movq	$0, (%r8)
	xorl	%eax, %eax
	movq	$0, 632(%r8)
	subq	%rdi, %r8
	leal	640(%r8), %ecx
	shrl	$3, %ecx
	rep stosq
.L958:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L959:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L961
	xorl	%esi, %esi
	movl	$640, %edi
	call	arena_get2.part.4
	movl	$640, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L962
	testq	%rbx, %rbx
	je	.L962
	.p2align 4,,10
	.p2align 3
.L966:
	movq	%rbx, %rdi
	movl	$640, %esi
	call	arena_get_retry
	movl	$640, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_int_malloc
	movq	%rax, %r8
.L962:
	testq	%rbx, %rbx
	je	.L963
	jmp	.L967
	.p2align 4,,10
	.p2align 3
.L960:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L961
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L961
	.p2align 4,,10
	.p2align 3
.L964:
	xorl	%eax, %eax
#APP
# 3161 "malloc.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L963
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 3161 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L963
	.size	tcache_init.part.6, .-tcache_init.part.6
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"newsize >= nb && (((unsigned long) (chunk2rawmem (p))) % alignment) == 0"
	.text
	.p2align 4,,15
	.type	_int_memalign, @function
_int_memalign:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	js	.L1005
	addq	$23, %rdx
	movl	$32, %eax
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movq	%rdi, %r13
	andq	$-16, %r12
	cmpq	$31, %rdx
	cmovbe	%rax, %r12
	leaq	32(%r12,%rsi), %rsi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L984
	xorl	%edx, %edx
	leaq	-16(%rax), %rcx
	divq	%rbp
	testq	%rdx, %rdx
	je	.L988
	leaq	-1(%rbx,%rbp), %rax
	movq	%rbp, %rdx
	negq	%rdx
	andq	%rdx, %rax
	subq	$16, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rbp), %r14
	subq	%rcx, %rdx
	cmpq	$32, %rdx
	cmovnb	%rax, %r14
	movq	-8(%rbx), %rax
	movq	%r14, %rsi
	subq	%rcx, %rsi
	movq	%rax, %r15
	andq	$-8, %r15
	subq	%rsi, %r15
	andl	$2, %eax
	jne	.L1006
	leaq	main_arena(%rip), %rdx
	movq	%r13, %rdi
	cmpq	%rdx, %r13
	movl	$4, %edx
	cmovne	%rdx, %rax
	movq	%r15, %rdx
	orq	$1, %rdx
	orq	%rax, %rdx
	movq	%rdx, 8(%r14)
	orq	$1, 8(%r14,%r15)
	movq	-8(%rbx), %rdx
	andl	$7, %edx
	orq	%rsi, %rdx
	movq	%rcx, %rsi
	orq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, -8(%rbx)
	call	_int_free
	cmpq	%r12, %r15
	jb	.L992
	leaq	16(%r14), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rax
	divq	%rbp
	testq	%rdx, %rdx
	jne	.L992
	movq	%r14, %rcx
.L988:
	movq	8(%rcx), %rax
	testb	$2, %al
	jne	.L984
	leaq	32(%r12), %rdx
	andq	$-8, %rax
	cmpq	%rax, %rdx
	jnb	.L984
	leaq	main_arena(%rip), %rdx
	subq	%r12, %rax
	leaq	(%rcx,%r12), %rsi
	movq	%r13, %rdi
	cmpq	%rdx, %r13
	setne	%dl
	orq	$1, %rax
	movzbl	%dl, %edx
	salq	$2, %rdx
	orq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, 8(%rsi)
	movq	8(%rcx), %rax
	andl	$7, %eax
	orq	%rax, %r12
	movq	%r12, 8(%rcx)
	call	_int_free
.L984:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1006:
	addq	-16(%rbx), %rsi
	orq	$2, %r15
	leaq	16(%r14), %rbx
	movq	%r15, 8(%r14)
	movq	%rsi, (%r14)
	jmp	.L984
	.p2align 4,,10
	.p2align 3
.L1005:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$12, %fs:(%rax)
	jmp	.L984
.L992:
	leaq	__PRETTY_FUNCTION__.13472(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC78(%rip), %rdi
	movl	$4967, %edx
	call	__malloc_assert
	.size	_int_memalign, .-_int_memalign
	.p2align 4,,15
	.type	memalign_check, @function
memalign_check:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	cmpq	$16, %rdi
	jbe	.L1024
	cmpq	$31, %rdi
	ja	.L1025
	cmpq	$-65, %rsi
	ja	.L1019
	movl	$32, %ebx
.L1013:
#APP
# 416 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1015
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1016:
	call	top_check
	leaq	1(%rbp), %rdx
	leaq	main_arena(%rip), %rdi
	movq	%rbx, %rsi
	call	_int_memalign
	movq	%rax, %r8
#APP
# 419 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1017
	subl	$1, main_arena(%rip)
.L1018:
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%rbp
	jmp	mem2mem_check
	.p2align 4,,10
	.p2align 3
.L1025:
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rdi
	ja	.L1026
	movq	$-33, %rdx
	subq	%rdi, %rdx
	cmpq	%rsi, %rdx
	jb	.L1019
	leaq	-1(%rdi), %rdx
	testq	%rdi, %rdx
	je	.L1020
	cmpq	$32, %rdi
	movl	$32, %ebx
	je	.L1013
	.p2align 4,,10
	.p2align 3
.L1014:
	addq	%rbx, %rbx
	cmpq	%rbx, %rdi
	ja	.L1014
	jmp	.L1013
	.p2align 4,,10
	.p2align 3
.L1024:
	addq	$8, %rsp
	movq	%rbp, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	jmp	malloc_check
	.p2align 4,,10
	.p2align 3
.L1015:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1016
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1016
	.p2align 4,,10
	.p2align 3
.L1017:
	xorl	%eax, %eax
#APP
# 419 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1018
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 419 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1018
	.p2align 4,,10
	.p2align 3
.L1019:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L1007:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1026:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L1007
.L1020:
	movq	%rdi, %rbx
	jmp	.L1013
	.size	memalign_check, .-memalign_check
	.p2align 4,,15
	.type	free_check, @function
free_check:
	testq	%rdi, %rdi
	je	.L1041
	pushq	%r12
	pushq	%rbp
	movq	__libc_errno@gottpoff(%rip), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%fs:0(%rbp), %r12d
#APP
# 271 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1029
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1030:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	mem2chunk_check
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1044
	testb	$2, 8(%rax)
	je	.L1032
#APP
# 277 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1033
	subl	$1, main_arena(%rip)
.L1034:
	movq	%r8, %rdi
	call	munmap_chunk
.L1035:
	movl	%r12d, %fs:0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1032:
	leaq	main_arena(%rip), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_int_free
#APP
# 286 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1036
	movl	%r12d, %fs:0(%rbp)
	subl	$1, main_arena(%rip)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1041:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1029:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1030
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1030
	.p2align 4,,10
	.p2align 3
.L1033:
	xorl	%eax, %eax
#APP
# 277 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1034
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 277 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1034
	.p2align 4,,10
	.p2align 3
.L1036:
	xorl	%eax, %eax
#APP
# 286 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1035
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 286 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1035
.L1044:
	leaq	.LC31(%rip), %rdi
	call	malloc_printerr
	.size	free_check, .-free_check
	.section	.rodata.str1.1
.LC79:
	.string	"realloc(): invalid old size"
.LC80:
	.string	"!chunk_is_mmapped (oldp)"
.LC81:
	.string	"realloc(): invalid next size"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"(unsigned long) (newsize) >= (unsigned long) (nb)"
	.text
	.p2align 4,,15
	.type	_int_realloc, @function
_int_realloc:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	movq	8(%rsi), %rax
	cmpq	$16, %rax
	jbe	.L1046
	movq	%rdi, %rbp
	movq	2184(%rdi), %rdi
	cmpq	%rdx, %rdi
	jbe	.L1046
	movq	%rax, %r12
	andl	$2, %r12d
	jne	.L1072
	leaq	(%rsi,%rdx), %r13
	movq	%rsi, %rbx
	movq	8(%r13), %r8
	movq	%r8, %rsi
	andq	$-8, %rsi
	cmpq	%rsi, %rdi
	jbe	.L1066
	cmpq	$16, %r8
	jbe	.L1066
	cmpq	%rcx, %rdx
	jb	.L1073
.L1064:
	movq	%rdx, %rdi
	andl	$7, %eax
	subq	%rcx, %rdi
	cmpq	$31, %rdi
	ja	.L1060
	leaq	main_arena(%rip), %rcx
	cmpq	%rcx, %rbp
	movl	$4, %ecx
	cmovne	%rcx, %r12
	orq	%rax, %rdx
	orq	%r12, %rdx
	movq	%rdx, 8(%rbx)
	orq	$1, 8(%r13)
.L1062:
	leaq	16(%rbx), %r14
.L1045:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1073:
	cmpq	%r13, 96(%rbp)
	je	.L1074
	testb	$1, 8(%r13,%rsi)
	je	.L1075
.L1053:
	leaq	-15(%rcx), %rsi
	movq	%rbp, %rdi
	movq	%rdx, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1045
	leaq	-16(%rax), %rax
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	cmpq	%rax, %r13
	je	.L1076
	movq	8(%rbx), %rax
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	andq	$-8, %rdx
	testb	$2, %al
	sete	%al
	movzbl	%al, %eax
	leaq	-16(%rdx,%rax,8), %rdx
	call	__GI_memcpy@PLT
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movl	$1, %edx
	call	_int_free
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1060:
	leaq	main_arena(%rip), %rdx
	leaq	(%rbx,%rcx), %rsi
	cmpq	%rdx, %rbp
	movl	$4, %edx
	cmovne	%rdx, %r12
	orq	$1, %rdi
	orq	%rcx, %rax
	movq	%rdi, %rdx
	orq	%r12, %rax
	movq	%rbp, %rdi
	orq	%r12, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, 8(%rsi)
	orq	$1, 8(%r13)
	movl	$1, %edx
	call	_int_free
	jmp	.L1062
	.p2align 4,,10
	.p2align 3
.L1075:
	leaq	(%rdx,%rsi), %r14
	cmpq	%r14, %rcx
	ja	.L1053
	movq	%r13, %rdi
	movq	%rcx, (%rsp)
	leaq	(%rbx,%r14), %r13
	call	unlink_chunk.isra.2
	movq	8(%rbx), %rax
	movq	%r14, %rdx
	movq	(%rsp), %rcx
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1076:
	movq	-8(%r14), %rax
	andq	$-8, %rax
	addq	%rax, %rdx
	cmpq	%rdx, %rcx
	ja	.L1077
	movq	8(%rbx), %rax
	leaq	(%rbx,%rdx), %r13
	jmp	.L1064
	.p2align 4,,10
	.p2align 3
.L1046:
	leaq	.LC79(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1066:
	leaq	.LC81(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1074:
	leaq	(%rdx,%rsi), %rdi
	leaq	32(%rcx), %rsi
	cmpq	%rdi, %rsi
	ja	.L1053
	andl	$7, %eax
	leaq	16(%rbx), %r14
	movq	%rax, %rsi
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rbp
	movl	$4, %eax
	cmovne	%rax, %r12
	orq	%rcx, %rsi
	subq	%rcx, %rdi
	orq	%r12, %rsi
	leaq	(%rbx,%rcx), %rax
	movq	%rsi, 8(%rbx)
	movq	%rdi, %rsi
	orq	$1, %rsi
	movq	%rax, 96(%rbp)
	movq	%rsi, 8(%rax)
	jmp	.L1045
.L1072:
	leaq	__PRETTY_FUNCTION__.13454(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC80(%rip), %rdi
	movl	$4793, %edx
	call	__malloc_assert
.L1077:
	leaq	__PRETTY_FUNCTION__.13454(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC82(%rip), %rdi
	movl	$4866, %edx
	call	__malloc_assert
	.size	_int_realloc, .-_int_realloc
	.section	.rodata.str1.1
.LC83:
	.string	"realloc(): invalid pointer"
	.text
	.p2align 4,,15
	.type	realloc_check, @function
realloc_check:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	addq	$1, %r13
	jc	.L1080
	testq	%rdi, %rdi
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	je	.L1110
	testq	%rsi, %rsi
	je	.L1111
#APP
# 319 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1085
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1086:
	leaq	8(%rsp), %rsi
	movq	%rbp, %rdi
	call	mem2chunk_check
	movq	%rax, %r12
#APP
# 321 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1087
	subl	$1, main_arena(%rip)
.L1088:
	testq	%r12, %r12
	je	.L1112
	testq	%r13, %r13
	js	.L1098
	leaq	23(%r13), %rax
	movl	$32, %ecx
	movq	8(%r12), %r15
	movq	%rax, %rsi
	andq	$-16, %rsi
	cmpq	$31, %rax
	cmova	%rsi, %rcx
	movq	%rcx, %r14
#APP
# 329 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1093
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1094:
	andq	$-8, %r15
	testb	$2, 8(%r12)
	je	.L1095
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	mremap_chunk
	testq	%rax, %rax
	je	.L1096
	leaq	16(%rax), %rbp
	testq	%rbp, %rbp
	jne	.L1091
.L1098:
	movq	8(%rsp), %rax
	xorl	%ebp, %ebp
	notb	(%rax)
.L1091:
#APP
# 376 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1099
	subl	$1, main_arena(%rip)
.L1100:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	mem2mem_check
.L1078:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1095:
.L1097:
	call	top_check
	leaq	main_arena(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_int_realloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.L1091
	jmp	.L1098
	.p2align 4,,10
	.p2align 3
.L1110:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	malloc_check
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1085:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1086
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1087:
	xorl	%eax, %eax
#APP
# 321 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1088
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 321 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1088
	.p2align 4,,10
	.p2align 3
.L1099:
	xorl	%eax, %eax
#APP
# 376 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1100
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 376 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1100
	.p2align 4,,10
	.p2align 3
.L1096:
	leaq	-8(%r15), %rax
	cmpq	%rax, %r14
	jbe	.L1091
	call	top_check
	leaq	main_arena(%rip), %rdi
	movq	%r13, %rsi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1098
	leaq	-16(%r15), %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movq	%r13, %rbp
	call	__GI_memcpy@PLT
	movq	%r12, %rdi
	call	munmap_chunk
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1093:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, main_arena(%rip)
	je	.L1094
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1094
	.p2align 4,,10
	.p2align 3
.L1111:
	xorl	%esi, %esi
	call	free_check
	xorl	%eax, %eax
	jmp	.L1078
.L1080:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1078
.L1112:
	leaq	.LC83(%rip), %rdi
	call	malloc_printerr
	.size	realloc_check, .-realloc_check
	.p2align 4,,15
	.globl	__malloc_fork_lock_parent
	.hidden	__malloc_fork_lock_parent
	.type	__malloc_fork_lock_parent, @function
__malloc_fork_lock_parent:
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L1122
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
#APP
# 152 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1115
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_lock(%rip)
# 0 "" 2
#NO_APP
.L1116:
	leaq	main_arena(%rip), %rbp
	xorl	%r12d, %r12d
	movq	%rbp, %rbx
	.p2align 4,,10
	.p2align 3
.L1119:
#APP
# 156 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1117
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L1118:
	movq	2160(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.L1119
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	%r12d, %eax
	lock cmpxchgl	%edx, (%rbx)
	je	.L1118
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1122:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1115:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_lock(%rip)
	je	.L1116
	leaq	list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1116
	.size	__malloc_fork_lock_parent, .-__malloc_fork_lock_parent
	.p2align 4,,15
	.globl	__malloc_fork_unlock_parent
	.hidden	__malloc_fork_unlock_parent
	.type	__malloc_fork_unlock_parent, @function
__malloc_fork_unlock_parent:
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L1136
	leaq	main_arena(%rip), %r9
	pushq	%rbp
	movl	$202, %ebp
	pushq	%rbx
	xorl	%ebx, %ebx
	movq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L1126:
#APP
# 171 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1128
	subl	$1, (%r8)
.L1129:
	movq	2160(%r8), %r8
	cmpq	%r9, %r8
	jne	.L1126
#APP
# 176 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1130
	subl	$1, list_lock(%rip)
.L1125:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1128:
	movl	%ebx, %eax
#APP
# 171 "arena.c" 1
	xchgl %eax, (%r8)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1129
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r8, %rdi
	movl	%ebp, %eax
#APP
# 171 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1129
	.p2align 4,,10
	.p2align 3
.L1136:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1130:
	xorl	%eax, %eax
#APP
# 176 "arena.c" 1
	xchgl %eax, list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1125
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 176 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1125
	.size	__malloc_fork_unlock_parent, .-__malloc_fork_unlock_parent
	.p2align 4,,15
	.globl	__malloc_fork_unlock_child
	.hidden	__malloc_fork_unlock_child
	.type	__malloc_fork_unlock_child, @function
__malloc_fork_unlock_child:
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L1139
	movq	thread_arena@gottpoff(%rip), %rax
	movl	$0, free_list_lock(%rip)
	movq	%fs:(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1141
	movq	$1, 2176(%rcx)
.L1141:
	leaq	main_arena(%rip), %rsi
	movq	$0, free_list(%rip)
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1143:
	cmpq	%rax, %rcx
	movl	$0, (%rax)
	je	.L1142
	movq	%rdx, 2168(%rax)
	movq	$0, 2176(%rax)
	movq	%rax, %rdx
	movl	$1, %edi
.L1142:
	movq	2160(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1143
	testb	%dil, %dil
	jne	.L1152
.L1144:
	movl	$0, list_lock(%rip)
.L1139:
	rep ret
.L1152:
	movq	%rdx, free_list(%rip)
	jmp	.L1144
	.size	__malloc_fork_unlock_child, .-__malloc_fork_unlock_child
	.p2align 4,,15
	.globl	__malloc_check_init
	.hidden	__malloc_check_init
	.type	__malloc_check_init, @function
__malloc_check_init:
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	leaq	malloc_check(%rip), %rdx
	leaq	free_check(%rip), %rcx
	leaq	realloc_check(%rip), %rsi
	leaq	memalign_check(%rip), %rdi
	movl	$1, using_malloc_checking(%rip)
	movq	%rdx, (%rax)
	movq	__free_hook@GOTPCREL(%rip), %rax
	movq	%rcx, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	%rsi, (%rax)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdi, (%rax)
	ret
	.size	__malloc_check_init, .-__malloc_check_init
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	malloc_get_state
	.type	malloc_get_state, @function
malloc_get_state:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	malloc_get_state, .-malloc_get_state
	.p2align 4,,15
	.globl	malloc_set_state
	.type	malloc_set_state, @function
malloc_set_state:
	cmpq	$1145849153, (%rdi)
	jne	.L1163
	testq	$-256, 8(%rdi)
	jg	.L1164
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movslq	2088(%rdi), %rdx
	movq	2080(%rdi), %r8
	movl	$0, using_malloc_checking(%rip)
	movq	$0, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	addq	%r8, %rdx
	cmpq	%rdx, %r8
	movq	$0, (%rax)
	movq	__free_hook@GOTPCREL(%rip), %rax
	movq	$0, (%rax)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	$0, (%rax)
	jnb	.L1165
	cmpq	$0, (%r8)
	movq	%r8, %rax
	je	.L1158
	jmp	.L1157
	.p2align 4,,10
	.p2align 3
.L1161:
	cmpq	$0, (%rax)
	jne	.L1157
.L1158:
	addq	$8, %rax
	cmpq	%rax, %rdx
	ja	.L1161
.L1165:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1157:
	movq	32(%rdi), %rsi
	subq	$8, %rax
	cmpq	%rsi, %rax
	jb	.L1159
	jmp	.L1160
	.p2align 4,,10
	.p2align 3
.L1171:
	orq	$2, %rdx
	cmpq	%rcx, %rsi
	movq	%rdx, 8(%rax)
	movq	%rcx, %rax
	jbe	.L1160
.L1159:
	movq	8(%rax), %rdx
	andq	$-8, %rdx
	leaq	(%rax,%rdx), %rcx
	testb	$1, 8(%rcx)
	jne	.L1171
	cmpq	%rcx, %rsi
	movq	%rcx, %rax
	ja	.L1159
.L1160:
	movq	%r8, dumped_main_arena_start(%rip)
	movq	%rsi, dumped_main_arena_end(%rip)
	xorl	%eax, %eax
	ret
.L1163:
	movl	$-1, %eax
	ret
.L1164:
	movl	$-2, %eax
	ret
	.size	malloc_set_state, .-malloc_set_state
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"!victim || chunk_is_mmapped (mem2chunk (victim)) || &main_arena == arena_for_chunk (mem2chunk (victim))"
	.align 8
.LC85:
	.string	"!victim || chunk_is_mmapped (mem2chunk (victim)) || ar_ptr == arena_for_chunk (mem2chunk (victim))"
	.text
	.p2align 4,,15
	.globl	__GI___libc_malloc
	.hidden	__GI___libc_malloc
	.type	__GI___libc_malloc, @function
__GI___libc_malloc:
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1224
	testq	%rdi, %rdi
	js	.L1225
	leaq	23(%rdi), %rax
	xorl	%ebp, %ebp
	cmpq	$31, %rax
	ja	.L1226
.L1176:
	movq	tcache@gottpoff(%rip), %r12
	movq	%rdi, %rbx
	movq	%fs:(%r12), %rax
	testq	%rax, %rax
	je	.L1227
	cmpq	80+mp_(%rip), %rbp
	jb	.L1192
.L1178:
#APP
# 3227 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1181
	leaq	main_arena(%rip), %rdi
	movq	%rbx, %rsi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1182
	movq	-8(%rax), %rax
	testb	$2, %al
	jne	.L1172
	testb	$4, %al
	je	.L1172
	leaq	-16(%r8), %rax
	leaq	main_arena(%rip), %rdx
	andq	$-67108864, %rax
	cmpq	%rdx, (%rax)
	jne	.L1228
.L1172:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1226:
	andq	$-16, %rax
	leaq	-17(%rax), %rbp
	shrq	$4, %rbp
	jmp	.L1176
	.p2align 4,,10
	.p2align 3
.L1227:
	movq	tcache_shutting_down@gottpoff(%rip), %rax
	cmpb	$0, %fs:(%rax)
	jne	.L1178
	call	tcache_init.part.6
	cmpq	%rbp, 80+mp_(%rip)
	jbe	.L1178
	movq	%fs:(%r12), %rax
	testq	%rax, %rax
	je	.L1178
	.p2align 4,,10
	.p2align 3
.L1192:
	leaq	(%rax,%rbp,2), %rcx
	movzwl	(%rcx), %edx
	testw	%dx, %dx
	je	.L1178
	leaq	(%rax,%rbp,8), %rsi
	movq	128(%rsi), %r8
	testb	$15, %r8b
	jne	.L1229
	movq	%r8, %rax
	subl	$1, %edx
	shrq	$12, %rax
	xorq	(%r8), %rax
	movq	%rax, 128(%rsi)
	movw	%dx, (%rcx)
	movq	%r8, %rax
	movq	$0, 8(%r8)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1182:
	xorl	%r8d, %r8d
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	24(%rsp), %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1225:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	$12, %fs:(%rax)
	jmp	.L1172
	.p2align 4,,10
	.p2align 3
.L1181:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rbp
	testq	%rbp, %rbp
	je	.L1183
#APP
# 3235 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1184
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%rbp)
# 0 "" 2
#NO_APP
.L1185:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1190
.L1191:
#APP
# 3248 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1188
	subl	$1, 0(%rbp)
.L1187:
	testq	%r8, %r8
	je	.L1182
	movq	-8(%r8), %rax
	testb	$2, %al
	jne	.L1172
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1189
	leaq	-16(%r8), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1189:
	cmpq	%rbp, %rdx
	je	.L1172
	leaq	__PRETTY_FUNCTION__.12933(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC85(%rip), %rdi
	movl	$3253, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1183:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L1185
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	arena_get2.part.4
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L1186
	testq	%rbp, %rbp
	je	.L1186
	.p2align 4,,10
	.p2align 3
.L1190:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	call	arena_get_retry
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	_int_malloc
	movq	%rax, %r8
.L1186:
	testq	%rbp, %rbp
	je	.L1187
	jmp	.L1191
.L1228:
	leaq	__PRETTY_FUNCTION__.12933(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC84(%rip), %rdi
	movl	$3231, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1229:
	leaq	.LC72(%rip), %rdi
	call	malloc_printerr
.L1184:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	je	.L1185
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L1185
.L1188:
	xorl	%eax, %eax
#APP
# 3248 "malloc.c" 1
	xchgl %eax, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1187
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 3248 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1187
	.size	__GI___libc_malloc, .-__GI___libc_malloc
	.globl	__libc_malloc
	.set	__libc_malloc,__GI___libc_malloc
	.globl	malloc
	.set	malloc,__libc_malloc
	.globl	__malloc
	.set	__malloc,__libc_malloc
	.p2align 4,,15
	.type	malloc_hook_ini, @function
malloc_hook_ini:
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movq	$0, (%rax)
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1236
	jmp	__GI___libc_malloc
	.p2align 4,,10
	.p2align 3
.L1236:
	subq	$24, %rsp
	movq	%rdi, 8(%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rdi
	addq	$24, %rsp
	jmp	__GI___libc_malloc
	.size	malloc_hook_ini, .-malloc_hook_ini
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"!p || chunk_is_mmapped (mem2chunk (p)) || &main_arena == arena_for_chunk (mem2chunk (p))"
	.align 8
.LC87:
	.string	"!p || chunk_is_mmapped (mem2chunk (p)) || ar_ptr == arena_for_chunk (mem2chunk (p))"
	.text
	.p2align 4,,15
	.type	_mid_memalign, @function
_mid_memalign:
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1284
	cmpq	$16, %rdi
	jbe	.L1285
	cmpq	$31, %rdi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	ja	.L1286
	movl	$32, %ebx
.L1240:
	movq	%rsi, %rbp
#APP
# 3502 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1244
	leaq	main_arena(%rip), %rdi
	movq	%rsi, %rdx
	movq	%rbx, %rsi
	call	_int_memalign
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1245
	movq	-8(%rax), %rax
	testb	$2, %al
	jne	.L1237
	testb	$4, %al
	je	.L1237
	leaq	-16(%r8), %rax
	leaq	main_arena(%rip), %rdx
	andq	$-67108864, %rax
	cmpq	%rdx, (%rax)
	jne	.L1287
.L1237:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1286:
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rdi
	ja	.L1288
	leaq	-1(%rdi), %rax
	testq	%rdi, %rax
	je	.L1256
	cmpq	$32, %rdi
	movl	$32, %ebx
	je	.L1240
	.p2align 4,,10
	.p2align 3
.L1243:
	addq	%rbx, %rbx
	cmpq	%rbx, %rdi
	ja	.L1243
	jmp	.L1240
	.p2align 4,,10
	.p2align 3
.L1285:
	movq	%rsi, %rdi
	jmp	__GI___libc_malloc
	.p2align 4,,10
	.p2align 3
.L1245:
	xorl	%r8d, %r8d
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1284:
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1244:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r12
	testq	%r12, %r12
	je	.L1246
#APP
# 3510 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1247
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r12)
# 0 "" 2
#NO_APP
.L1248:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_int_memalign
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1253
.L1254:
#APP
# 3521 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1251
	subl	$1, (%r12)
.L1250:
	testq	%r8, %r8
	je	.L1245
	movq	-8(%r8), %rax
	testb	$2, %al
	jne	.L1237
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1252
	leaq	-16(%r8), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1252:
	cmpq	%r12, %rdx
	je	.L1237
	leaq	__PRETTY_FUNCTION__.13078(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC87(%rip), %rdi
	movl	$3524, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1247:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L1248
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L1248
	.p2align 4,,10
	.p2align 3
.L1288:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	$22, %fs:(%rax)
	jmp	.L1237
.L1246:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L1248
	leaq	32(%rbx,%rbp), %rdi
	xorl	%esi, %esi
	call	arena_get2.part.4
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_int_memalign
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L1249
	testq	%r12, %r12
	je	.L1249
	.p2align 4,,10
	.p2align 3
.L1253:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	call	arena_get_retry
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_int_memalign
	movq	%rax, %r8
.L1249:
	testq	%r12, %r12
	je	.L1250
	jmp	.L1254
.L1251:
	xorl	%eax, %eax
#APP
# 3521 "malloc.c" 1
	xchgl %eax, (%r12)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1250
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 3521 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1250
.L1287:
	leaq	__PRETTY_FUNCTION__.13078(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC86(%rip), %rdi
	movl	$3506, %edx
	call	__malloc_assert
.L1256:
	movq	%rdi, %rbx
	jmp	.L1240
	.size	_mid_memalign, .-_mid_memalign
	.p2align 4,,15
	.type	memalign_hook_ini, @function
memalign_hook_ini:
	subq	$24, %rsp
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	$0, (%rax)
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1292
	movq	24(%rsp), %rdx
	addq	$24, %rsp
	jmp	_mid_memalign
	.p2align 4,,10
	.p2align 3
.L1292:
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	movq	24(%rsp), %rdx
	addq	$24, %rsp
	jmp	_mid_memalign
	.size	memalign_hook_ini, .-memalign_hook_ini
	.p2align 4,,15
	.globl	__GI___libc_free
	.hidden	__GI___libc_free
	.type	__GI___libc_free, @function
__GI___libc_free:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	__free_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1314
	testq	%rdi, %rdi
	je	.L1293
	movq	-8(%rdi), %rax
	movq	__libc_errno@gottpoff(%rip), %rbx
	leaq	-16(%rdi), %rsi
	testb	$2, %al
	movl	%fs:(%rbx), %ebp
	jne	.L1315
	movq	tcache@gottpoff(%rip), %rdx
	cmpq	$0, %fs:(%rdx)
	je	.L1316
.L1300:
	testb	$4, %al
	leaq	main_arena(%rip), %rdi
	je	.L1301
	movq	%rsi, %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdi
.L1301:
	xorl	%edx, %edx
	call	_int_free
	movl	%ebp, %fs:(%rbx)
.L1293:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1315:
	movl	52+mp_(%rip), %edx
	testl	%edx, %edx
	jne	.L1297
	cmpq	16+mp_(%rip), %rax
	jbe	.L1297
	cmpq	$33554432, %rax
	jbe	.L1317
.L1297:
	movq	%rsi, %rdi
	call	munmap_chunk
	movl	%ebp, %fs:(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1314:
	movq	40(%rsp), %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1317:
	cmpq	dumped_main_arena_start(%rip), %rsi
	jb	.L1298
	cmpq	dumped_main_arena_end(%rip), %rsi
	jb	.L1297
.L1298:
	andq	$-8, %rax
	movq	%rax, 16+mp_(%rip)
	addq	%rax, %rax
	movq	%rax, mp_(%rip)
	jmp	.L1297
	.p2align 4,,10
	.p2align 3
.L1316:
	movq	tcache_shutting_down@gottpoff(%rip), %rdx
	cmpb	$0, %fs:(%rdx)
	jne	.L1300
	movq	%rdi, 8(%rsp)
	movq	%rsi, (%rsp)
	call	tcache_init.part.6
	movq	8(%rsp), %rdi
	movq	(%rsp), %rsi
	movq	-8(%rdi), %rax
	jmp	.L1300
	.size	__GI___libc_free, .-__GI___libc_free
	.globl	__libc_free
	.set	__libc_free,__GI___libc_free
	.globl	free
	.set	free,__libc_free
	.globl	__free
	.set	__free,__libc_free
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"tcache_thread_shutdown(): unaligned tcache chunk detected"
	.section	.rodata.str1.1
.LC89:
	.string	"a->attached_threads > 0"
	.text
	.p2align 4,,15
	.globl	__malloc_arena_thread_freeres
	.hidden	__malloc_arena_thread_freeres
	.type	__malloc_arena_thread_freeres, @function
__malloc_arena_thread_freeres:
	movq	tcache@gottpoff(%rip), %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%fs:(%rax), %rbp
	testq	%rbp, %rbp
	je	.L1319
	movq	$0, %fs:(%rax)
	movq	tcache_shutting_down@gottpoff(%rip), %rax
	leaq	128(%rbp), %rbx
	leaq	640(%rbp), %r12
	movb	$1, %fs:(%rax)
	jmp	.L1344
	.p2align 4,,10
	.p2align 3
.L1321:
	movq	%rdi, %rax
	shrq	$12, %rax
	xorq	(%rdi), %rax
	movq	%rax, (%rbx)
	call	__GI___libc_free
.L1344:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1345
	testb	$15, %dil
	je	.L1321
	leaq	.LC88(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1345:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L1344
	movq	%rbp, %rdi
	call	__GI___libc_free
	.p2align 4,,10
	.p2align 3
.L1319:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rbx
	movq	$0, %fs:(%rax)
	testq	%rbx, %rbx
	je	.L1318
#APP
# 1008 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1328
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L1329:
	movq	2176(%rbx), %rax
	testq	%rax, %rax
	je	.L1346
	subq	$1, %rax
	testq	%rax, %rax
	movq	%rax, 2176(%rbx)
	jne	.L1331
	movq	free_list(%rip), %rax
	movq	%rbx, free_list(%rip)
	movq	%rax, 2168(%rbx)
.L1331:
#APP
# 1017 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1332
	subl	$1, free_list_lock(%rip)
.L1318:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L1328:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L1329
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1329
.L1332:
	xorl	%eax, %eax
#APP
# 1017 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1318
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 1017 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1318
.L1346:
	leaq	__PRETTY_FUNCTION__.12331(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC89(%rip), %rdi
	movl	$1011, %edx
	call	__malloc_assert
	.size	__malloc_arena_thread_freeres, .-__malloc_arena_thread_freeres
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"!newp || chunk_is_mmapped (mem2chunk (newp)) || ar_ptr == arena_for_chunk (mem2chunk (newp))"
	.text
	.p2align 4,,15
	.globl	__GI___libc_realloc
	.hidden	__GI___libc_realloc
	.type	__GI___libc_realloc, @function
__GI___libc_realloc:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L1401
	testq	%rsi, %rsi
	movq	%rsi, %r15
	movq	%rdi, %rbx
	jne	.L1349
	testq	%rdi, %rdi
	jne	.L1402
.L1349:
	testq	%rbx, %rbx
	je	.L1403
	movq	-8(%rbx), %rax
	xorl	%r8d, %r8d
	leaq	-16(%rbx), %rbp
	movq	%rax, %r14
	andq	$-8, %r14
	testb	$2, %al
	je	.L1404
.L1352:
	movq	%r14, %rdx
	negq	%rdx
	cmpq	%rbp, %rdx
	jb	.L1354
	testb	$15, %bpl
	jne	.L1354
.L1355:
	testq	%r15, %r15
	js	.L1405
	leaq	23(%r15), %rdx
	movl	$32, %ecx
	movq	%rdx, %r13
	andq	$-16, %r13
	cmpq	$31, %rdx
	cmovbe	%rcx, %r13
	testb	$2, %al
	jne	.L1406
#APP
# 3425 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1362
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	_int_realloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1347
	movq	-8(%rax), %rax
	testb	$2, %al
	jne	.L1347
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	movq	8(%rsp), %r8
	je	.L1363
	leaq	-16(%r12), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1363:
	cmpq	%rdx, %r8
	jne	.L1407
.L1347:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1404:
	movq	tcache@gottpoff(%rip), %rdx
	cmpq	$0, %fs:(%rdx)
	je	.L1408
.L1353:
	testb	$4, %al
	leaq	main_arena(%rip), %r8
	je	.L1352
	movq	%rbp, %rdx
	andq	$-67108864, %rdx
	movq	(%rdx), %r8
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1406:
	cmpq	%rbp, dumped_main_arena_start(%rip)
	ja	.L1360
	cmpq	%rbp, dumped_main_arena_end(%rip)
	ja	.L1409
.L1360:
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	mremap_chunk
	testq	%rax, %rax
	leaq	16(%rax), %r12
	jne	.L1347
	leaq	-8(%r14), %rax
	movq	%rbx, %r12
	cmpq	%r13, %rax
	jnb	.L1347
	movq	%r15, %rdi
	call	__GI___libc_malloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1347
	leaq	-16(%r14), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	movq	%rbp, %rdi
	call	munmap_chunk
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1409:
	movq	%r15, %rdi
	call	__GI___libc_malloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1347
	subq	$8, %r14
	movq	%r15, %rdx
	movq	%rbx, %rsi
	cmpq	%r15, %r14
	movq	%rax, %rdi
	cmovbe	%r14, %rdx
	call	__GI_memcpy@PLT
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1354:
	cmpq	%rbp, dumped_main_arena_start(%rip)
	ja	.L1356
	cmpq	%rbp, dumped_main_arena_end(%rip)
	ja	.L1355
.L1356:
	leaq	.LC83(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1403:
	addq	$24, %rsp
	movq	%r15, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__GI___libc_malloc
	.p2align 4,,10
	.p2align 3
.L1401:
	movq	72(%rsp), %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1405:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r12d, %r12d
	movl	$12, %fs:(%rax)
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1402:
	call	__GI___libc_free
	xorl	%r12d, %r12d
	jmp	.L1347
	.p2align 4,,10
	.p2align 3
.L1408:
	movq	tcache_shutting_down@gottpoff(%rip), %rdx
	cmpb	$0, %fs:(%rdx)
	jne	.L1353
	call	tcache_init.part.6
	movq	-8(%rbx), %rax
	jmp	.L1353
	.p2align 4,,10
	.p2align 3
.L1362:
#APP
# 3434 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1364
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r8)
# 0 "" 2
#NO_APP
.L1365:
	movq	%r13, %rcx
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	_int_realloc
	movq	%rax, %r12
#APP
# 3438 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	8(%rsp), %r8
	jne	.L1366
	subl	$1, (%r8)
.L1367:
	testq	%r12, %r12
	je	.L1368
	movq	-8(%r12), %rax
	testb	$2, %al
	jne	.L1347
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1369
	leaq	-16(%r12), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1369:
	cmpq	%rdx, %r8
	je	.L1347
	leaq	__PRETTY_FUNCTION__.13010(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	movl	$3440, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	%r15, %rdi
	movq	%r8, 8(%rsp)
	call	__GI___libc_malloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1347
	leaq	-8(%r14), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	movq	8(%rsp), %r8
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%r8, %rdi
	call	_int_free
	jmp	.L1347
.L1366:
	xorl	%eax, %eax
#APP
# 3438 "malloc.c" 1
	xchgl %eax, (%r8)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1367
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r8, %rdi
	movl	$202, %eax
#APP
# 3438 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1367
	.p2align 4,,10
	.p2align 3
.L1364:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r8)
	je	.L1365
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %r8
	jmp	.L1365
.L1407:
	leaq	__PRETTY_FUNCTION__.13010(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	movl	$3429, %edx
	call	__malloc_assert
	.size	__GI___libc_realloc, .-__GI___libc_realloc
	.globl	__libc_realloc
	.set	__libc_realloc,__GI___libc_realloc
	.globl	realloc
	.set	realloc,__libc_realloc
	.globl	__realloc
	.set	__realloc,__libc_realloc
	.p2align 4,,15
	.type	realloc_hook_ini, @function
realloc_hook_ini:
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movq	$0, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	$0, (%rax)
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1416
	jmp	__GI___libc_realloc
	.p2align 4,,10
	.p2align 3
.L1416:
	subq	$24, %rsp
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	addq	$24, %rsp
	jmp	__GI___libc_realloc
	.size	realloc_hook_ini, .-realloc_hook_ini
	.p2align 4,,15
	.globl	__GI___libc_memalign
	.hidden	__GI___libc_memalign
	.type	__GI___libc_memalign, @function
__GI___libc_memalign:
	movq	(%rsp), %rdx
	jmp	_mid_memalign
	.size	__GI___libc_memalign, .-__GI___libc_memalign
	.weak	aligned_alloc
	.set	aligned_alloc,__GI___libc_memalign
	.globl	__libc_memalign
	.set	__libc_memalign,__GI___libc_memalign
	.weak	memalign
	.set	memalign,__libc_memalign
	.globl	__memalign
	.set	__memalign,__libc_memalign
	.p2align 4,,15
	.globl	__libc_valloc
	.type	__libc_valloc, @function
__libc_valloc:
	subq	$24, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	%rdi, %rsi
	testl	%eax, %eax
	js	.L1421
.L1419:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	24(%rsp), %rdx
	movq	24(%rax), %rdi
	addq	$24, %rsp
	jmp	_mid_memalign
	.p2align 4,,10
	.p2align 3
.L1421:
	movq	%rdi, 8(%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rsi
	jmp	.L1419
	.size	__libc_valloc, .-__libc_valloc
	.weak	valloc
	.set	valloc,__libc_valloc
	.globl	__valloc
	.set	__valloc,__libc_valloc
	.p2align 4,,15
	.globl	__libc_pvalloc
	.type	__libc_pvalloc, @function
__libc_pvalloc:
	movl	__libc_malloc_initialized(%rip), %eax
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	js	.L1429
.L1423:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	8(%rsp), %rdx
	movq	24(%rax), %rdi
	leaq	-1(%rdi), %rsi
	addq	%rsi, %rbx
	jc	.L1425
	movl	$1, %esi
	subq	%rdi, %rsi
	andq	%rbx, %rsi
	popq	%rbx
	jmp	_mid_memalign
	.p2align 4,,10
	.p2align 3
.L1429:
	call	ptmalloc_init.part.0
	jmp	.L1423
.L1425:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__libc_pvalloc, .-__libc_pvalloc
	.weak	pvalloc
	.set	pvalloc,__libc_pvalloc
	.globl	__pvalloc
	.set	__pvalloc,__libc_pvalloc
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"!mem || chunk_is_mmapped (mem2chunk (mem)) || av == arena_for_chunk (mem2chunk (mem))"
	.section	.rodata.str1.1
.LC92:
	.string	"nclears >= 3"
	.text
	.p2align 4,,15
	.globl	__libc_calloc
	.type	__libc_calloc, @function
__libc_calloc:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %rax
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	mulq	%rsi
	movq	%rax, %rbx
	seto	%al
	testq	%rbx, %rbx
	js	.L1483
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L1483
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rdx
	testq	%rdx, %rdx
	jne	.L1486
	movq	tcache@gottpoff(%rip), %rax
	cmpq	$0, %fs:(%rax)
	je	.L1487
.L1438:
#APP
# 3605 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1488
	movq	96+main_arena(%rip), %r12
	leaq	main_arena(%rip), %r13
	movq	8(%r12), %rbp
	andq	$-8, %rbp
.L1443:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1444
	movq	-8(%rax), %rax
	testb	$2, %al
	je	.L1489
.L1444:
#APP
# 3642 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1490
.L1446:
	testq	%r8, %r8
	je	.L1449
	movq	-8(%r8), %rdx
	movl	perturb_byte(%rip), %eax
	testb	$2, %dl
	je	.L1450
	testl	%eax, %eax
	jne	.L1491
.L1430:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L1450:
	andq	$-8, %rdx
	testl	%eax, %eax
	je	.L1492
.L1451:
	subq	$8, %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	cmpq	$2, %rax
	jbe	.L1493
	cmpq	$9, %rax
	ja	.L1494
	cmpq	$4, %rax
	movq	$0, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	jbe	.L1430
	cmpq	$6, %rax
	movq	$0, 24(%r8)
	movq	$0, 32(%r8)
	jbe	.L1430
	cmpq	$9, %rax
	movq	$0, 40(%r8)
	movq	$0, 48(%r8)
	jne	.L1430
	movq	$0, 56(%r8)
	movq	$0, 64(%r8)
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1489:
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1445
	leaq	-16(%r8), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1445:
	cmpq	%r13, %rdx
	je	.L1444
	leaq	__PRETTY_FUNCTION__.13161(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC91(%rip), %rdi
	movl	$3640, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1492:
	leaq	-16(%r8), %rax
	cmpq	%rax, %r12
	jne	.L1451
	cmpq	%rdx, %rbp
	cmovb	%rbp, %rdx
	jmp	.L1451
	.p2align 4,,10
	.p2align 3
.L1494:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	__GI_memset@PLT
	addq	$8, %rsp
	movq	%rax, %r8
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L1483:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	$12, %fs:(%rax)
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	40(%rsp), %rsi
	movq	%rbx, %rdi
	call	*%rdx
	xorl	%esi, %esi
	testq	%rax, %rax
	movq	%rbx, %rdx
	movq	%rax, %rdi
	je	.L1449
.L1485:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	__GI_memset@PLT
	.p2align 4,,10
	.p2align 3
.L1487:
	movq	tcache_shutting_down@gottpoff(%rip), %rax
	cmpb	$0, %fs:(%rax)
	jne	.L1438
	call	tcache_init.part.6
	jmp	.L1438
	.p2align 4,,10
	.p2align 3
.L1488:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r13
	testq	%r13, %r13
	je	.L1440
#APP
# 3608 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1441
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%r13)
# 0 "" 2
#NO_APP
.L1442:
	movq	96(%r13), %r12
	leaq	main_arena(%rip), %rax
	movq	8(%r12), %rbp
	andq	$-8, %rbp
	cmpq	%rax, %r13
	je	.L1443
	movq	%r12, %rax
	andq	$-67108864, %rax
	addq	24(%rax), %rax
	subq	%r12, %rax
	cmpq	%rax, %rbp
	cmovb	%rax, %rbp
	jmp	.L1443
	.p2align 4,,10
	.p2align 3
.L1490:
	testq	%r8, %r8
	jne	.L1447
	testq	%r13, %r13
	jne	.L1495
.L1447:
	testq	%r13, %r13
	je	.L1446
#APP
# 3652 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1448
	subl	$1, 0(%r13)
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1495:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	arena_get_retry
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_int_malloc
	movq	%rax, %r8
	jmp	.L1447
	.p2align 4,,10
	.p2align 3
.L1449:
	xorl	%r8d, %r8d
	jmp	.L1430
	.p2align 4,,10
	.p2align 3
.L1491:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	jmp	.L1485
	.p2align 4,,10
	.p2align 3
.L1440:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L1442
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	arena_get2.part.4
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L1442
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	jmp	.L1443
.L1441:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%r13)
	je	.L1442
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L1442
.L1448:
	xorl	%eax, %eax
#APP
# 3652 "malloc.c" 1
	xchgl %eax, 0(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1446
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 3652 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1446
.L1493:
	leaq	__PRETTY_FUNCTION__.13161(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC92(%rip), %rdi
	movl	$3691, %edx
	call	__malloc_assert
	.size	__libc_calloc, .-__libc_calloc
	.weak	calloc
	.set	calloc,__libc_calloc
	.globl	__calloc
	.set	__calloc,__libc_calloc
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"(char *) chunk2rawmem (p) + 2 * CHUNK_HDR_SZ <= paligned_mem"
	.align 8
.LC94:
	.string	"(char *) p + size > paligned_mem"
	.text
	.p2align 4,,15
	.globl	__malloc_trim
	.type	__malloc_trim, @function
__malloc_trim:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	%rdi, 24(%rsp)
	testl	%eax, %eax
	js	.L1525
.L1497:
	leaq	main_arena(%rip), %rax
	movl	$0, 20(%rsp)
	movq	%rax, 8(%rsp)
.L1516:
#APP
# 5063 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	movq	8(%rsp), %rcx
	jne	.L1498
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L1499:
	movq	8(%rsp), %rdi
	call	malloc_consolidate
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	24(%rax), %rcx
	cmpq	$1023, %rcx
	ja	.L1500
	movl	%ecx, %eax
	shrl	$4, %eax
	movl	%eax, 16(%rsp)
.L1501:
	movq	8(%rsp), %rax
	leaq	-1(%rcx), %rbp
	leaq	47(%rcx), %r13
	negq	%rcx
	xorl	%edx, %edx
	movl	$1, %r15d
	movq	%rcx, %r12
	leaq	96(%rax), %r14
	jmp	.L1512
	.p2align 4,,10
	.p2align 3
.L1527:
	cmpl	%r15d, 16(%rsp)
	jle	.L1517
.L1506:
	addl	$1, %r15d
	addq	$16, %r14
	cmpl	$128, %r15d
	je	.L1526
.L1512:
	cmpl	$1, %r15d
	jne	.L1527
.L1517:
	movq	24(%r14), %rbx
	cmpq	%rbx, %r14
	jne	.L1511
	jmp	.L1506
	.p2align 4,,10
	.p2align 3
.L1508:
	movq	24(%rbx), %rbx
	cmpq	%rbx, %r14
	je	.L1506
.L1511:
	movq	8(%rbx), %rsi
	andq	$-8, %rsi
	cmpq	%rsi, %r13
	jnb	.L1508
	leaq	(%rbx,%r13), %rdi
	leaq	48(%rbx), %rax
	andq	%r12, %rdi
	cmpq	%rax, %rdi
	jb	.L1528
	leaq	(%rbx,%rsi), %rax
	cmpq	%rax, %rdi
	jnb	.L1529
	movq	%rdi, %rax
	subq	%rbx, %rax
	subq	%rax, %rsi
	cmpq	%rsi, %rbp
	jnb	.L1508
	movl	$4, %edx
	andq	%r12, %rsi
	call	__GI___madvise
	movl	$1, %edx
	jmp	.L1508
.L1526:
	leaq	main_arena(%rip), %rcx
	cmpq	%rcx, 8(%rsp)
	je	.L1530
.L1513:
	orl	%edx, 20(%rsp)
#APP
# 5065 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1514
	movq	8(%rsp), %rax
	subl	$1, (%rax)
.L1515:
	movq	8(%rsp), %rax
	leaq	main_arena(%rip), %rcx
	movq	2160(%rax), %rax
	cmpq	%rcx, %rax
	movq	%rax, 8(%rsp)
	jne	.L1516
	movl	20(%rsp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L1500:
	movq	%rcx, %rbp
	shrq	$6, %rbp
	cmpq	$48, %rbp
	ja	.L1502
	leal	48(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1501
.L1502:
	movq	%rcx, %rbp
	shrq	$9, %rbp
	cmpq	$20, %rbp
	ja	.L1503
	leal	91(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1501
.L1530:
	movq	24(%rsp), %rdi
	movl	%edx, 16(%rsp)
	call	systrim.isra.1.constprop.12
	movl	16(%rsp), %edx
	orl	%eax, %edx
	jmp	.L1513
.L1529:
	leaq	__PRETTY_FUNCTION__.13486(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC94(%rip), %rdi
	movl	$5023, %edx
	call	__malloc_assert
.L1528:
	leaq	__PRETTY_FUNCTION__.13486(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC93(%rip), %rdi
	movl	$5022, %edx
	call	__malloc_assert
.L1498:
	xorl	%eax, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L1499
	movq	8(%rsp), %rdi
	call	__lll_lock_wait_private
	jmp	.L1499
	.p2align 4,,10
	.p2align 3
.L1503:
	movq	%rcx, %rbp
	shrq	$12, %rbp
	cmpq	$10, %rbp
	ja	.L1504
	leal	110(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1501
.L1514:
	xorl	%eax, %eax
	movq	8(%rsp), %rcx
#APP
# 5065 "malloc.c" 1
	xchgl %eax, (%rcx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1515
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rcx, %rdi
	movl	$202, %eax
#APP
# 5065 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1515
	.p2align 4,,10
	.p2align 3
.L1525:
	call	ptmalloc_init.part.0
	jmp	.L1497
.L1504:
	movq	%rcx, %rbp
	shrq	$15, %rbp
	cmpq	$4, %rbp
	ja	.L1505
	leal	119(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1501
.L1505:
	movq	%rcx, %rbp
	movl	$2, %eax
	shrq	$18, %rbp
	cmpq	$2, %rbp
	cmova	%rax, %rbp
	leal	124(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1501
	.size	__malloc_trim, .-__malloc_trim
	.weak	malloc_trim
	.set	malloc_trim,__malloc_trim
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"malloc_check_get_size: memory corruption"
	.text
	.p2align 4,,15
	.globl	__malloc_usable_size
	.type	__malloc_usable_size, @function
__malloc_usable_size:
	testq	%rdi, %rdi
	je	.L1542
	movq	-8(%rdi), %rdx
	leaq	-16(%rdi), %rcx
	movq	%rdx, %rax
	andq	$-8, %rdx
	andl	$2, %eax
	cmpl	$1, using_malloc_checking(%rip)
	je	.L1556
	testq	%rax, %rax
	je	.L1540
	cmpq	dumped_main_arena_start(%rip), %rcx
	jb	.L1541
	cmpq	dumped_main_arena_end(%rip), %rcx
	jnb	.L1541
	leaq	-8(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1540:
	testb	$1, -8(%rdi,%rdx)
	leaq	-8(%rdx), %rcx
	cmovne	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1541:
	leaq	-16(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1542:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1556:
	movq	%rcx, %r8
	movq	%rcx, %rsi
	shrq	$11, %rsi
	shrq	$3, %r8
	xorl	%esi, %r8d
	movl	$2, %esi
	cmpb	$1, %r8b
	cmove	%esi, %r8d
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	leaq	-1(%rdx,%rax,8), %rax
	movzbl	-16(%rdi,%rax), %edx
	cmpb	%r8b, %dl
	je	.L1536
	testb	%dl, %dl
	je	.L1537
	leaq	16(%rdx), %rsi
	cmpq	%rsi, %rax
	jnb	.L1538
	jmp	.L1537
	.p2align 4,,10
	.p2align 3
.L1539:
	testb	%dl, %dl
	je	.L1537
	leaq	16(%rdx), %rsi
	cmpq	%rax, %rsi
	ja	.L1537
.L1538:
	subq	%rdx, %rax
	movzbl	(%rcx,%rax), %edx
	cmpb	%r8b, %dl
	jne	.L1539
.L1536:
	subq	$16, %rax
	ret
.L1537:
	leaq	.LC95(%rip), %rdi
	subq	$8, %rsp
	call	malloc_printerr
	.size	__malloc_usable_size, .-__malloc_usable_size
	.weak	malloc_usable_size
	.set	malloc_usable_size,__malloc_usable_size
	.p2align 4,,15
	.globl	__GI___libc_mallinfo2
	.hidden	__GI___libc_mallinfo2
	.type	__GI___libc_mallinfo2, @function
__GI___libc_mallinfo2:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$80, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1566
.L1558:
	pxor	%xmm0, %xmm0
	leaq	main_arena(%rip), %rbp
	movq	%rsp, %r12
	xorl	%r14d, %r14d
	movq	%rbp, %rbx
	movaps	%xmm0, (%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 64(%rsp)
	.p2align 4,,10
	.p2align 3
.L1563:
#APP
# 5205 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1559
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L1560:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	int_mallinfo
#APP
# 5207 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1561
	subl	$1, (%rbx)
.L1562:
	movq	2160(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.L1563
	movdqa	(%rsp), %xmm0
	movq	%r13, %rax
	movups	%xmm0, 0(%r13)
	movdqa	16(%rsp), %xmm0
	movups	%xmm0, 16(%r13)
	movdqa	32(%rsp), %xmm0
	movups	%xmm0, 32(%r13)
	movdqa	48(%rsp), %xmm0
	movups	%xmm0, 48(%r13)
	movdqa	64(%rsp), %xmm0
	movups	%xmm0, 64(%r13)
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1561:
	movl	%r14d, %eax
#APP
# 5207 "malloc.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1562
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 5207 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1562
	.p2align 4,,10
	.p2align 3
.L1559:
	movl	%r14d, %eax
	lock cmpxchgl	%edx, (%rbx)
	je	.L1560
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L1560
	.p2align 4,,10
	.p2align 3
.L1566:
	call	ptmalloc_init.part.0
	jmp	.L1558
	.size	__GI___libc_mallinfo2, .-__GI___libc_mallinfo2
	.globl	__libc_mallinfo2
	.set	__libc_mallinfo2,__GI___libc_mallinfo2
	.weak	mallinfo2
	.set	mallinfo2,__libc_mallinfo2
	.globl	__mallinfo2
	.set	__mallinfo2,__libc_mallinfo2
	.p2align 4,,15
	.globl	__libc_mallinfo
	.type	__libc_mallinfo, @function
__libc_mallinfo:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%rsp, %rdi
	call	__GI___libc_mallinfo2
	movq	(%rsp), %rax
	movl	%eax, (%rbx)
	movq	8(%rsp), %rax
	movl	%eax, 4(%rbx)
	movq	16(%rsp), %rax
	movl	%eax, 8(%rbx)
	movq	24(%rsp), %rax
	movl	%eax, 12(%rbx)
	movq	32(%rsp), %rax
	movl	%eax, 16(%rbx)
	movq	40(%rsp), %rax
	movl	%eax, 20(%rbx)
	movq	48(%rsp), %rax
	movl	%eax, 24(%rbx)
	movq	56(%rsp), %rax
	movl	%eax, 28(%rbx)
	movq	64(%rsp), %rax
	movl	%eax, 32(%rbx)
	movq	72(%rsp), %rax
	movl	%eax, 36(%rbx)
	addq	$80, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	__libc_mallinfo, .-__libc_mallinfo
	.weak	mallinfo
	.set	mallinfo,__libc_mallinfo
	.globl	__mallinfo
	.set	__mallinfo,__libc_mallinfo
	.section	.rodata.str1.1
.LC96:
	.string	"Arena %d:\n"
.LC97:
	.string	"system bytes     = %10u\n"
.LC98:
	.string	"in use bytes     = %10u\n"
.LC99:
	.string	"Total (incl. mmap):\n"
.LC100:
	.string	"max mmap regions = %10u\n"
.LC101:
	.string	"max mmap bytes   = %10lu\n"
	.text
	.p2align 4,,15
	.globl	__malloc_stats
	.type	__malloc_stats, @function
__malloc_stats:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	56+mp_(%rip), %r12
	testl	%eax, %eax
	movl	%r12d, %r13d
	js	.L1578
.L1570:
	movq	stderr@GOTPCREL(%rip), %rbx
	leaq	main_arena(%rip), %r14
	xorl	%ebp, %ebp
	leaq	16(%rsp), %r15
	movq	(%rbx), %rax
	movl	116(%rax), %ecx
	movl	%ecx, %edx
	movl	%ecx, 12(%rsp)
	orl	$2, %edx
	movl	%edx, 116(%rax)
	jmp	.L1576
	.p2align 4,,10
	.p2align 3
.L1579:
	addl	$1, %ebp
.L1576:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, (%r15)
	movaps	%xmm0, 16(%r15)
	movaps	%xmm0, 32(%r15)
	movaps	%xmm0, 48(%r15)
	movaps	%xmm0, 64(%r15)
#APP
# 5259 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1571
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r14)
# 0 "" 2
#NO_APP
.L1572:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	int_mallinfo
	movq	(%rbx), %rdi
	leaq	.LC96(%rip), %rsi
	movl	%ebp, %edx
	xorl	%eax, %eax
	call	__GI_fprintf
	movl	16(%rsp), %edx
	movq	(%rbx), %rdi
	leaq	.LC97(%rip), %rsi
	xorl	%eax, %eax
	call	__GI_fprintf
	movl	72(%rsp), %edx
	movq	(%rbx), %rdi
	leaq	.LC98(%rip), %rsi
	xorl	%eax, %eax
	call	__GI_fprintf
	addl	16(%rsp), %r13d
	addl	72(%rsp), %r12d
#APP
# 5270 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1573
	subl	$1, (%r14)
.L1574:
	movq	2160(%r14), %r14
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %r14
	jne	.L1579
	movq	(%rbx), %rcx
	leaq	.LC99(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	call	__GI_fwrite@PLT
	movq	(%rbx), %rdi
	leaq	.LC97(%rip), %rsi
	movl	%r13d, %edx
	xorl	%eax, %eax
	call	__GI_fprintf
	movq	(%rbx), %rdi
	leaq	.LC98(%rip), %rsi
	movl	%r12d, %edx
	xorl	%eax, %eax
	call	__GI_fprintf
	movq	(%rbx), %rdi
	movl	48+mp_(%rip), %edx
	leaq	.LC100(%rip), %rsi
	xorl	%eax, %eax
	call	__GI_fprintf
	movq	(%rbx), %rdi
	movq	64+mp_(%rip), %rdx
	leaq	.LC101(%rip), %rsi
	xorl	%eax, %eax
	call	__GI_fprintf
	movq	(%rbx), %rax
	movl	12(%rsp), %ecx
	movl	%ecx, 116(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1573:
	xorl	%eax, %eax
#APP
# 5270 "malloc.c" 1
	xchgl %eax, (%r14)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1574
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r14, %rdi
	movl	$202, %eax
#APP
# 5270 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1574
	.p2align 4,,10
	.p2align 3
.L1571:
	xorl	%eax, %eax
	lock cmpxchgl	%edx, (%r14)
	je	.L1572
	movq	%r14, %rdi
	call	__lll_lock_wait_private
	jmp	.L1572
	.p2align 4,,10
	.p2align 3
.L1578:
	call	ptmalloc_init.part.0
	jmp	.L1570
	.size	__malloc_stats, .-__malloc_stats
	.weak	malloc_stats
	.set	malloc_stats,__malloc_stats
	.p2align 4,,15
	.globl	__GI___libc_mallopt
	.hidden	__GI___libc_mallopt
	.type	__GI___libc_mallopt, @function
__GI___libc_mallopt:
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rbp
	movl	%edi, %ebx
	subq	$8, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1604
.L1581:
#APP
# 5420 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1582
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1583:
	leaq	main_arena(%rip), %rdi
	call	malloc_consolidate
	leal	8(%rbx), %eax
	cmpl	$9, %eax
	ja	.L1597
	leaq	.L1586(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1586:
	.long	.L1585-.L1586
	.long	.L1587-.L1586
	.long	.L1588-.L1586
	.long	.L1597-.L1586
	.long	.L1589-.L1586
	.long	.L1590-.L1586
	.long	.L1591-.L1586
	.long	.L1592-.L1586
	.long	.L1597-.L1586
	.long	.L1593-.L1586
	.text
	.p2align 4,,10
	.p2align 3
.L1604:
	call	ptmalloc_init.part.0
	jmp	.L1581
	.p2align 4,,10
	.p2align 3
.L1592:
	movq	%rbp, mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1584:
#APP
# 5474 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1595
	subl	$1, main_arena(%rip)
.L1580:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1585:
	testl	%ebp, %ebp
	movl	$1, %r8d
	jle	.L1584
	movq	%rbp, 32+mp_(%rip)
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1593:
	xorl	%r8d, %r8d
	cmpq	$160, %rbp
	ja	.L1584
	cmpq	$7, %rbp
	movl	$16, %eax
	jbe	.L1594
	leaq	8(%rbp), %rax
	andq	$-16, %rax
.L1594:
	movq	%rax, global_max_fast(%rip)
	movl	%ebx, %r8d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1589:
	movl	%ebp, 44+mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1587:
	testl	%ebp, %ebp
	movl	$1, %r8d
	jle	.L1584
	movq	%rbp, 24+mp_(%rip)
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1588:
	movl	%ebp, perturb_byte(%rip)
	movl	$1, %r8d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1590:
	xorl	%r8d, %r8d
	cmpq	$33554432, %rbp
	ja	.L1584
	movq	%rbp, 16+mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1591:
	movq	%rbp, 8+mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1597:
	movl	$1, %r8d
	jmp	.L1584
	.p2align 4,,10
	.p2align 3
.L1595:
	xorl	%eax, %eax
#APP
# 5474 "malloc.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1580
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 5474 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1580
	.p2align 4,,10
	.p2align 3
.L1582:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1583
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1583
	.size	__GI___libc_mallopt, .-__GI___libc_mallopt
	.globl	__libc_mallopt
	.set	__libc_mallopt,__GI___libc_mallopt
	.weak	mallopt
	.set	mallopt,__libc_mallopt
	.globl	__mallopt
	.set	__mallopt,__libc_mallopt
	.p2align 4,,15
	.globl	__posix_memalign
	.type	__posix_memalign, @function
__posix_memalign:
	testb	$7, %sil
	movl	$22, %eax
	jne	.L1611
	movq	%rsi, %rax
	shrq	$3, %rax
	leaq	-1(%rax), %rcx
	testq	%rax, %rcx
	jne	.L1608
	testq	%rsi, %rsi
	je	.L1608
	pushq	%rbx
	movq	%rdx, %rcx
	movq	8(%rsp), %rdx
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_mid_memalign
	testq	%rax, %rax
	je	.L1609
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L1608:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1611:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1609:
	movl	$12, %eax
	popq	%rbx
	ret
	.size	__posix_memalign, .-__posix_memalign
	.weak	posix_memalign
	.set	posix_memalign,__posix_memalign
	.section	.rodata.str1.1
.LC102:
	.string	"<malloc version=\"1\">\n"
	.text
	.p2align 4,,15
	.globl	__malloc_info
	.type	__malloc_info, @function
__malloc_info:
	testl	%edi, %edi
	jne	.L1615
	movl	__libc_malloc_initialized(%rip), %eax
	pushq	%rbx
	movq	%rsi, %rbx
	testl	%eax, %eax
	js	.L1620
.L1616:
	leaq	.LC102(%rip), %rdi
	movq	%rbx, %rsi
	call	__GI__IO_fputs
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__malloc_info.part.11
	.p2align 4,,10
	.p2align 3
.L1620:
	call	ptmalloc_init.part.0
	jmp	.L1616
.L1615:
	movl	$22, %eax
	ret
	.size	__malloc_info, .-__malloc_info
	.weak	malloc_info
	.set	malloc_info,__malloc_info
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.13486, @object
	.size	__PRETTY_FUNCTION__.13486, 6
__PRETTY_FUNCTION__.13486:
	.string	"mtrim"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.13161, @object
	.size	__PRETTY_FUNCTION__.13161, 14
__PRETTY_FUNCTION__.13161:
	.string	"__libc_calloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13078, @object
	.size	__PRETTY_FUNCTION__.13078, 14
__PRETTY_FUNCTION__.13078:
	.string	"_mid_memalign"
	.align 8
	.type	__PRETTY_FUNCTION__.13010, @object
	.size	__PRETTY_FUNCTION__.13010, 15
__PRETTY_FUNCTION__.13010:
	.string	"__libc_realloc"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12158, @object
	.size	__PRETTY_FUNCTION__.12158, 22
__PRETTY_FUNCTION__.12158:
	.string	"remove_from_free_list"
	.local	next_to_use.12166
	.comm	next_to_use.12166,8,8
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12007, @object
	.size	__PRETTY_FUNCTION__.12007, 13
__PRETTY_FUNCTION__.12007:
	.string	"detach_arena"
	.align 8
	.type	__PRETTY_FUNCTION__.12111, @object
	.size	__PRETTY_FUNCTION__.12111, 14
__PRETTY_FUNCTION__.12111:
	.string	"get_free_list"
	.local	narenas_limit.12225
	.comm	narenas_limit.12225,8,8
	.align 8
	.type	__PRETTY_FUNCTION__.12933, @object
	.size	__PRETTY_FUNCTION__.12933, 14
__PRETTY_FUNCTION__.12933:
	.string	"__libc_malloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13454, @object
	.size	__PRETTY_FUNCTION__.13454, 13
__PRETTY_FUNCTION__.13454:
	.string	"_int_realloc"
	.align 8
	.type	__PRETTY_FUNCTION__.12831, @object
	.size	__PRETTY_FUNCTION__.12831, 13
__PRETTY_FUNCTION__.12831:
	.string	"mremap_chunk"
	.align 8
	.type	__PRETTY_FUNCTION__.12819, @object
	.size	__PRETTY_FUNCTION__.12819, 13
__PRETTY_FUNCTION__.12819:
	.string	"munmap_chunk"
	.data
	.align 4
	.type	may_shrink_heap.11323, @object
	.size	may_shrink_heap.11323, 4
may_shrink_heap.11323:
	.long	-1
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.12000, @object
	.size	__PRETTY_FUNCTION__.12000, 10
__PRETTY_FUNCTION__.12000:
	.string	"heap_trim"
	.align 8
	.type	__PRETTY_FUNCTION__.13383, @object
	.size	__PRETTY_FUNCTION__.13383, 10
__PRETTY_FUNCTION__.13383:
	.string	"_int_free"
	.align 8
	.type	__PRETTY_FUNCTION__.12774, @object
	.size	__PRETTY_FUNCTION__.12774, 10
__PRETTY_FUNCTION__.12774:
	.string	"sysmalloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13274, @object
	.size	__PRETTY_FUNCTION__.13274, 12
__PRETTY_FUNCTION__.13274:
	.string	"_int_malloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13472, @object
	.size	__PRETTY_FUNCTION__.13472, 14
__PRETTY_FUNCTION__.13472:
	.string	"_int_memalign"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.12331, @object
	.size	__PRETTY_FUNCTION__.12331, 30
__PRETTY_FUNCTION__.12331:
	.string	"__malloc_arena_thread_freeres"
	.section	.tbss,"awT",@nobits
	.align 8
	.type	tcache, @object
	.size	tcache, 8
tcache:
	.zero	8
	.type	tcache_shutting_down, @object
	.size	tcache_shutting_down, 1
tcache_shutting_down:
	.zero	1
	.local	using_malloc_checking
	.comm	using_malloc_checking,4,4
	.local	aligned_heap_area
	.comm	aligned_heap_area,8,8
	.hidden	__libc_malloc_initialized
	.globl	__libc_malloc_initialized
	.data
	.align 4
	.type	__libc_malloc_initialized, @object
	.size	__libc_malloc_initialized, 4
__libc_malloc_initialized:
	.long	-1
	.local	list_lock
	.comm	list_lock,4,4
	.local	free_list
	.comm	free_list,8,8
	.align 8
	.type	narenas, @object
	.size	narenas, 8
narenas:
	.quad	1
	.local	free_list_lock
	.comm	free_list_lock,4,4
	.section	.tbss
	.align 8
	.type	thread_arena, @object
	.size	thread_arena, 8
thread_arena:
	.zero	8
	.local	perturb_byte
	.comm	perturb_byte,4,4
	.weak	__after_morecore_hook
	.bss
	.align 8
	.type	__after_morecore_hook, @object
	.size	__after_morecore_hook, 8
__after_morecore_hook:
	.zero	8
	.weak	__memalign_hook
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__memalign_hook, @object
	.size	__memalign_hook, 8
__memalign_hook:
	.quad	memalign_hook_ini
	.weak	__realloc_hook
	.align 8
	.type	__realloc_hook, @object
	.size	__realloc_hook, 8
__realloc_hook:
	.quad	realloc_hook_ini
	.weak	__malloc_hook
	.align 8
	.type	__malloc_hook, @object
	.size	__malloc_hook, 8
__malloc_hook:
	.quad	malloc_hook_ini
	.weak	__free_hook
	.bss
	.align 8
	.type	__free_hook, @object
	.size	__free_hook, 8
__free_hook:
	.zero	8
	.weak	__malloc_initialize_hook
	.align 8
	.type	__malloc_initialize_hook, @object
	.size	__malloc_initialize_hook, 8
__malloc_initialize_hook:
	.zero	8
	.data
	.align 32
	.type	mp_, @object
	.size	mp_, 112
mp_:
	.quad	131072
	.quad	131072
	.quad	131072
	.quad	8
	.zero	12
	.long	65536
	.zero	32
	.quad	64
	.quad	1032
	.quad	7
	.quad	0
	.local	dumped_main_arena_end
	.comm	dumped_main_arena_end,8,8
	.local	dumped_main_arena_start
	.comm	dumped_main_arena_start,8,8
	.section	.data.rel.local
	.align 32
	.type	main_arena, @object
	.size	main_arena, 2200
main_arena:
	.long	0
	.zero	2156
	.quad	main_arena
	.zero	8
	.quad	1
	.zero	16
	.local	global_max_fast
	.comm	global_max_fast,8,8
	.globl	__morecore
	.align 8
	.type	__morecore, @object
	.size	__morecore, 8
__morecore:
	.quad	__GI___default_morecore
	.hidden	__lll_lock_wait_private
	.hidden	__libc_initial
	.hidden	__fxprintf
	.hidden	__libc_message
