	.text
	.p2align 4,,15
	.globl	__libc_alloc_buffer_copy_bytes
	.hidden	__libc_alloc_buffer_copy_bytes
	.type	__libc_alloc_buffer_copy_bytes, @function
__libc_alloc_buffer_copy_bytes:
	pushq	%rbp
	movq	%rsi, %rax
	pushq	%rbx
	subq	%rdi, %rax
	subq	$8, %rsp
	cmpq	%rax, %rcx
	ja	.L3
	testq	%rdi, %rdi
	movq	%rsi, %rbx
	leaq	(%rcx,%rdi), %rbp
	je	.L2
	movq	%rdx, %rsi
	movq	%rcx, %rdx
	call	memcpy@PLT
.L2:
	addq	$8, %rsp
	movq	%rbp, %rax
	movq	%rbx, %rdx
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	addq	$8, %rsp
	movq	%rbp, %rax
	movq	%rbx, %rdx
	popq	%rbx
	popq	%rbp
	ret
	.size	__libc_alloc_buffer_copy_bytes, .-__libc_alloc_buffer_copy_bytes
