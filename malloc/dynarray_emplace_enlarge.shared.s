	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_dynarray_emplace_enlarge
	.hidden	__GI___libc_dynarray_emplace_enlarge
	.type	__GI___libc_dynarray_emplace_enlarge, @function
__GI___libc_dynarray_emplace_enlarge:
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rcx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	testq	%rax, %rax
	jne	.L2
	cmpq	$3, %rdx
	movl	$16, %ebx
	ja	.L25
.L3:
	movq	%rbx, %rax
	mulq	%rcx
	jo	.L9
	movq	%rdi, %rbp
	movq	16(%rdi), %rdi
	cmpq	%rsi, %rdi
	je	.L26
	movq	%rax, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L9
.L10:
	movq	%r8, 16(%rbp)
	movq	%rbx, 8(%rbp)
	addq	$24, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rax, %rdx
	shrq	%rdx
	leaq	1(%rax,%rdx), %rbx
	cmpq	%rbx, %rax
	jb	.L3
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	cmpq	$8, %rdx
	sbbq	%rbx, %rbx
	andl	$4, %ebx
	addq	$4, %rbx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movq	%rax, %rdi
	movq	%rcx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L9
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.L10
	movq	8(%rsp), %rcx
	movq	%rax, %rdi
	imulq	0(%rbp), %rcx
	movq	%rcx, %rdx
	call	__GI_memcpy@PLT
	movq	%rax, %r8
	jmp	.L10
	.size	__GI___libc_dynarray_emplace_enlarge, .-__GI___libc_dynarray_emplace_enlarge
	.globl	__libc_dynarray_emplace_enlarge
	.set	__libc_dynarray_emplace_enlarge,__GI___libc_dynarray_emplace_enlarge
