	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_scratch_buffer_dupfree
	.hidden	__GI___libc_scratch_buffer_dupfree
	.type	__GI___libc_scratch_buffer_dupfree, @function
__GI___libc_scratch_buffer_dupfree:
	pushq	%rbx
	addq	$16, %rdi
	subq	$16, %rsp
	movq	-16(%rdi), %rbx
	cmpq	%rdi, %rbx
	je	.L10
	movq	%rbx, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	cmove	%rbx, %rax
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1
	movq	8(%rsp), %rsi
	addq	$16, %rsp
	movq	%rsi, %rdx
	movq	%rbx, %rsi
	popq	%rbx
	jmp	__GI_memcpy@PLT
	.size	__GI___libc_scratch_buffer_dupfree, .-__GI___libc_scratch_buffer_dupfree
	.globl	__libc_scratch_buffer_dupfree
	.set	__libc_scratch_buffer_dupfree,__GI___libc_scratch_buffer_dupfree
