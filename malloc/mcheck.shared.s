	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"memory is consistent, library is buggy\n"
	.align 8
.LC1:
	.string	"memory clobbered before allocated block\n"
	.align 8
.LC2:
	.string	"memory clobbered past end of allocated block\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"block freed twice\n"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"bogus mcheck_status, library is buggy\n"
#NO_APP
	.text
	.p2align 4,,15
	.type	mabort, @function
mabort:
	subq	$8, %rsp
	cmpl	$1, %edi
	je	.L3
	jle	.L11
	cmpl	$2, %edi
	je	.L6
	cmpl	$3, %edi
	jne	.L2
	leaq	.LC2(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
.L8:
	movq	%rax, %rdi
	call	__GI___libc_fatal
.L6:
	leaq	.LC1(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
.L11:
	testl	%edi, %edi
	jne	.L2
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
.L2:
	leaq	.LC4(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
.L3:
	leaq	.LC3(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
	jmp	.L8
	.size	mabort, .-mabort
	.p2align 4,,15
	.type	checkhdr.part.0, @function
checkhdr.part.0:
	pushq	%rbx
	movq	24(%rdi), %rdx
	movl	$3630650121, %eax
	addq	16(%rdi), %rdx
	xorq	8(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.L14
	addq	$645098466, %rax
	cmpq	%rax, %rdx
	je	.L15
.L20:
	movl	$2, %ebx
.L13:
	movl	$0, mcheck_used(%rip)
	movl	%ebx, %edi
	call	*abortfunc(%rip)
	movl	$1, mcheck_used(%rip)
.L12:
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	(%rdi), %rax
	movl	$3, %ebx
	cmpb	$-41, 48(%rdi,%rax)
	jne	.L13
	movq	32(%rdi), %rax
	xorq	40(%rdi), %rax
	xorl	%ebx, %ebx
	cmpq	%rdx, %rax
	jne	.L20
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$1, %ebx
	jmp	.L13
	.size	checkhdr.part.0, .-checkhdr.part.0
	.p2align 4,,15
	.globl	__GI_mcheck_check_all
	.hidden	__GI_mcheck_check_all
	.type	__GI_mcheck_check_all, @function
__GI_mcheck_check_all:
	pushq	%rbx
	movq	root(%rip), %rbx
	movl	$0, pedantic(%rip)
	testq	%rbx, %rbx
	jne	.L25
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L23
.L25:
	movl	mcheck_used(%rip), %eax
	testl	%eax, %eax
	je	.L24
	movq	%rbx, %rdi
	call	checkhdr.part.0
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L25
.L23:
	movl	$1, pedantic(%rip)
	popq	%rbx
	ret
	.size	__GI_mcheck_check_all, .-__GI_mcheck_check_all
	.globl	mcheck_check_all
	.set	mcheck_check_all,__GI_mcheck_check_all
	.p2align 4,,15
	.type	memalignhook, @function
memalignhook:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	subq	$16, %rsp
	movl	pedantic(%rip), %eax
	testl	%eax, %eax
	jne	.L43
.L32:
	leaq	47(%rdi), %rcx
	movq	%rdi, %rbx
	negq	%rbx
	andq	%rcx, %rbx
	leaq	1(%rbx), %rax
	notq	%rax
	cmpq	%rbp, %rax
	jb	.L44
	movq	old_memalign_hook(%rip), %rax
	movq	__memalign_hook@GOTPCREL(%rip), %r12
	leaq	1(%rbx,%rbp), %rsi
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L35
	call	*%rax
.L36:
	leaq	memalignhook(%rip), %rcx
	testq	%rax, %rax
	movq	%rcx, (%r12)
	je	.L38
	movq	root(%rip), %rsi
	addq	%rax, %rbx
	movl	$4275748587, %edi
	leaq	-48(%rbx), %rdx
	movq	%rbp, -48(%rbx)
	movq	$0, -32(%rbx)
	movq	%rsi, %r8
	movq	%rsi, -24(%rbx)
	movq	%rdx, root(%rip)
	xorq	%rdi, %r8
	testq	%rsi, %rsi
	movq	%r8, -40(%rbx)
	je	.L37
	movq	%rdx, 16(%rsi)
	addq	24(%rsi), %rdx
	xorq	%rdi, %rdx
	movq	%rdx, 8(%rsi)
.L37:
	movl	$4275748587, %edx
	movq	%rax, -16(%rbx)
	movl	$-109, %esi
	xorq	%rdx, %rax
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movq	%rax, -8(%rbx)
	movb	$-41, (%rbx,%rbp)
	call	__GI_memset
.L31:
	addq	$16, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rdx, 8(%rsp)
	movq	%rdi, (%rsp)
	call	__GI_mcheck_check_all
	movq	8(%rsp), %rdx
	movq	(%rsp), %rdi
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L44:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$12, %fs:(%rax)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L38:
	xorl	%ebx, %ebx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L35:
	call	memalign@PLT
	jmp	.L36
	.size	memalignhook, .-memalignhook
	.p2align 4,,15
	.type	mallochook, @function
mallochook:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	pedantic(%rip), %eax
	testl	%eax, %eax
	jne	.L57
.L46:
	cmpq	$-50, %rbx
	ja	.L58
	movq	old_malloc_hook(%rip), %rax
	movq	__malloc_hook@GOTPCREL(%rip), %rbp
	leaq	49(%rbx), %rdi
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L49
	call	*%rax
.L50:
	leaq	mallochook(%rip), %rdi
	testq	%rax, %rax
	movq	%rdi, 0(%rbp)
	je	.L52
	movq	root(%rip), %rdx
	movl	$4275748587, %esi
	movq	%rbx, (%rax)
	movq	$0, 16(%rax)
	movq	%rax, root(%rip)
	movq	%rdx, %rcx
	movq	%rdx, 24(%rax)
	xorq	%rsi, %rcx
	testq	%rdx, %rdx
	movq	%rcx, 8(%rax)
	je	.L51
	movq	24(%rdx), %rcx
	movq	%rax, 16(%rdx)
	addq	%rax, %rcx
	xorq	%rsi, %rcx
	movq	%rcx, 8(%rdx)
.L51:
	leaq	48(%rax), %rcx
	movl	$4275748587, %edx
	movq	%rax, 32(%rax)
	xorq	%rax, %rdx
	movl	$-109, %esi
	movq	%rdx, 40(%rax)
	movq	%rcx, %rdi
	movb	$-41, 48(%rax,%rbx)
	movq	%rbx, %rdx
	call	__GI_memset
	movq	%rax, %rcx
.L45:
	addq	$24, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%rsi, 8(%rsp)
	call	__GI_mcheck_check_all
	movq	8(%rsp), %rsi
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L58:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movl	$12, %fs:(%rax)
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L52:
	xorl	%ecx, %ecx
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L49:
	call	malloc@PLT
	jmp	.L50
	.size	mallochook, .-mallochook
	.p2align 4,,15
	.type	freehook, @function
freehook:
	movl	pedantic(%rip), %edx
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%edx, %edx
	jne	.L75
.L60:
	testq	%rbx, %rbx
	je	.L61
	movl	mcheck_used(%rip), %eax
	testl	%eax, %eax
	jne	.L76
.L62:
	movq	-24(%rbx), %rdx
	movl	$3630650121, %eax
	movq	%rax, -40(%rbx)
	movq	%rax, -8(%rbx)
	movq	-32(%rbx), %rax
	testq	%rdx, %rdx
	je	.L63
	movq	%rax, 16(%rdx)
	addq	24(%rdx), %rax
	movl	$4275748587, %ecx
	xorq	%rcx, %rax
	movq	%rax, 8(%rdx)
	movq	-32(%rbx), %rax
.L63:
	testq	%rax, %rax
	je	.L64
	movq	%rdx, 24(%rax)
	addq	16(%rax), %rdx
	movl	$4275748587, %ecx
	xorq	%rcx, %rdx
	movq	%rdx, 8(%rax)
.L65:
	movq	-48(%rbx), %rdx
	movq	$0, -24(%rbx)
	movq	%rbx, %rdi
	movq	$0, -32(%rbx)
	movl	$-107, %esi
	call	__GI_memset
	movq	-16(%rbx), %rbx
.L61:
	movq	old_free_hook(%rip), %rax
	movq	__free_hook@GOTPCREL(%rip), %r12
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L66
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L67:
	leaq	freehook(%rip), %rax
	movq	%rax, (%r12)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	leaq	-48(%rbx), %rdi
	call	checkhdr.part.0
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rdx, root(%rip)
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L75:
	call	__GI_mcheck_check_all
	jmp	.L60
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%rbx, %rdi
	call	free@PLT
	jmp	.L67
	.size	freehook, .-freehook
	.p2align 4,,15
	.type	reallochook, @function
reallochook:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L99
	movl	pedantic(%rip), %edx
	movq	%rsi, %rbx
	testl	%edx, %edx
	jne	.L100
.L80:
	cmpq	$-50, %rbx
	ja	.L101
	testq	%rbp, %rbp
	je	.L90
	movl	mcheck_used(%rip), %eax
	leaq	-48(%rbp), %rcx
	movq	-48(%rbp), %r15
	testl	%eax, %eax
	jne	.L102
	movq	-24(%rbp), %rdx
	movq	-32(%rbp), %rax
	testq	%rdx, %rdx
	je	.L84
.L104:
	movq	%rax, 16(%rdx)
	addq	24(%rdx), %rax
	movl	$4275748587, %esi
	xorq	%rsi, %rax
	movq	%rax, 8(%rdx)
	movq	-32(%rbp), %rax
.L84:
	testq	%rax, %rax
	je	.L85
	movq	%rdx, 24(%rax)
	addq	16(%rax), %rdx
	movl	$4275748587, %esi
	xorq	%rsi, %rdx
	movq	%rdx, 8(%rax)
.L86:
	cmpq	%r15, %rbx
	jb	.L103
.L82:
	movq	old_free_hook(%rip), %rax
	movq	__free_hook@GOTPCREL(%rip), %r14
	leaq	49(%rbx), %rsi
	movq	__malloc_hook@GOTPCREL(%rip), %r13
	movq	__memalign_hook@GOTPCREL(%rip), %rdi
	movq	__realloc_hook@GOTPCREL(%rip), %rbp
	movq	%rax, (%r14)
	movq	old_malloc_hook(%rip), %rax
	movq	%rax, 0(%r13)
	movq	old_memalign_hook(%rip), %rax
	movq	%rax, (%rdi)
	movq	old_realloc_hook(%rip), %rax
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L87
	movq	%r12, %rdx
	movq	%rcx, %rdi
	call	*%rax
.L88:
	leaq	freehook(%rip), %rdi
	movq	__memalign_hook@GOTPCREL(%rip), %rsi
	testq	%rax, %rax
	movq	%rdi, (%r14)
	leaq	mallochook(%rip), %rdi
	movq	%rdi, 0(%r13)
	leaq	memalignhook(%rip), %rdi
	movq	%rdi, (%rsi)
	leaq	reallochook(%rip), %rsi
	movq	%rsi, 0(%rbp)
	je	.L91
	movq	root(%rip), %rdx
	movl	$4275748587, %esi
	movq	%rbx, (%rax)
	movq	$0, 16(%rax)
	movq	%rax, root(%rip)
	movq	%rdx, %rcx
	movq	%rdx, 24(%rax)
	xorq	%rsi, %rcx
	testq	%rdx, %rdx
	movq	%rcx, 8(%rax)
	je	.L89
	movq	24(%rdx), %rcx
	movq	%rax, 16(%rdx)
	addq	%rax, %rcx
	xorq	%rsi, %rcx
	movq	%rcx, 8(%rdx)
.L89:
	movl	$4275748587, %edx
	movq	%rax, 32(%rax)
	leaq	48(%rax), %rbp
	xorq	%rax, %rdx
	cmpq	%rbx, %r15
	movq	%rdx, 40(%rax)
	movb	$-41, 48(%rax,%rbx)
	jnb	.L77
	subq	%r15, %rbx
	leaq	48(%rax,%r15), %rdi
	movl	$-109, %esi
	movq	%rbx, %rdx
	call	__GI_memset
.L77:
	addq	$24, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movq	%rcx, %rdi
	movq	%rcx, 8(%rsp)
	call	checkhdr.part.0
	movq	-24(%rbp), %rdx
	movq	8(%rsp), %rcx
	movq	-32(%rbp), %rax
	testq	%rdx, %rdx
	jne	.L104
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L100:
	call	__GI_mcheck_check_all
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L90:
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L103:
	movq	%r15, %rdx
	leaq	0(%rbp,%rbx), %rdi
	movl	$-107, %esi
	subq	%rbx, %rdx
	movq	%rcx, 8(%rsp)
	call	__GI_memset
	movq	8(%rsp), %rcx
	jmp	.L82
	.p2align 4,,10
	.p2align 3
.L85:
	movq	%rdx, root(%rip)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L101:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebp, %ebp
	movl	$12, %fs:(%rax)
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rdx, %rsi
	xorl	%ebp, %ebp
	call	freehook
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L91:
	xorl	%ebp, %ebp
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L87:
	movq	%rcx, %rdi
	call	realloc@PLT
	jmp	.L88
	.size	reallochook, .-reallochook
	.p2align 4,,15
	.globl	__GI_mcheck
	.hidden	__GI_mcheck
	.type	__GI_mcheck, @function
__GI_mcheck:
	pushq	%rbx
	leaq	mabort(%rip), %rax
	subq	$16, %rsp
	testq	%rdi, %rdi
	movl	mcheck_used(%rip), %ebx
	cmove	%rax, %rdi
	movl	__libc_malloc_initialized(%rip), %eax
	movq	%rdi, abortfunc(%rip)
	testl	%eax, %eax
	jle	.L107
	testl	%ebx, %ebx
	sete	%bl
	addq	$16, %rsp
	movzbl	%bl, %ebx
	negl	%ebx
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L107:
	testl	%ebx, %ebx
	je	.L112
	xorl	%ebx, %ebx
	addq	$16, %rsp
	movl	%ebx, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	xorl	%edi, %edi
	call	malloc@PLT
	movq	%rax, 8(%rsp)
	movq	8(%rsp), %rdi
	call	free@PLT
	movq	__free_hook@GOTPCREL(%rip), %rax
	leaq	freehook(%rip), %rcx
	leaq	mallochook(%rip), %rsi
	movl	$1, mcheck_used(%rip)
	movq	(%rax), %rdx
	movq	%rcx, (%rax)
	leaq	memalignhook(%rip), %rcx
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movq	%rdx, old_free_hook(%rip)
	movq	(%rax), %rdx
	movq	%rsi, (%rax)
	leaq	reallochook(%rip), %rsi
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdx, old_malloc_hook(%rip)
	movq	(%rax), %rdx
	movq	%rcx, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	%rdx, old_memalign_hook(%rip)
	movq	(%rax), %rdx
	movq	%rsi, (%rax)
	movl	%ebx, %eax
	movq	%rdx, old_realloc_hook(%rip)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	__GI_mcheck, .-__GI_mcheck
	.globl	mcheck
	.set	mcheck,__GI_mcheck
	.p2align 4,,15
	.globl	mcheck_pedantic
	.type	mcheck_pedantic, @function
mcheck_pedantic:
	subq	$8, %rsp
	call	__GI_mcheck
	testl	%eax, %eax
	jne	.L113
	movl	$1, pedantic(%rip)
.L113:
	addq	$8, %rsp
	ret
	.size	mcheck_pedantic, .-mcheck_pedantic
	.p2align 4,,15
	.globl	mprobe
	.type	mprobe, @function
mprobe:
	movl	mcheck_used(%rip), %eax
	testl	%eax, %eax
	je	.L117
	subq	$48, %rdi
	jmp	checkhdr.part.0
	.p2align 4,,10
	.p2align 3
.L117:
	movl	$-1, %eax
	ret
	.size	mprobe, .-mprobe
	.local	pedantic
	.comm	pedantic,4,4
	.local	mcheck_used
	.comm	mcheck_used,4,4
	.local	root
	.comm	root,8,8
	.local	abortfunc
	.comm	abortfunc,8,8
	.local	old_realloc_hook
	.comm	old_realloc_hook,8,8
	.local	old_memalign_hook
	.comm	old_memalign_hook,8,8
	.local	old_malloc_hook
	.comm	old_malloc_hook,8,8
	.local	old_free_hook
	.comm	old_free_hook,8,8
	.hidden	__libc_malloc_initialized
