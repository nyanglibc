	.text
	.p2align 4,,15
	.globl	__libc_alloc_buffer_alloc_array
	.hidden	__libc_alloc_buffer_alloc_array
	.type	__libc_alloc_buffer_alloc_array, @function
__libc_alloc_buffer_alloc_array:
	movq	(%rdi), %r9
	movq	%rsi, %rax
	leaq	-1(%r9,%rdx), %r8
	negq	%rdx
	andq	%rdx, %r8
	mulq	%rcx
	movq	%rax, %rsi
	seto	%al
	xorl	%edx, %edx
	addq	%r8, %rsi
	movzbl	%al, %eax
	setc	%dl
	xorl	$1, %eax
	testq	%rdx, %rdx
	sete	%dl
	testb	%dl, %al
	je	.L6
	cmpq	%r8, %r9
	ja	.L6
	cmpq	%rsi, 8(%rdi)
	jb	.L6
	movq	%rsi, (%rdi)
	movq	%r8, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	xorl	%eax, %eax
	ret
	.size	__libc_alloc_buffer_alloc_array, .-__libc_alloc_buffer_alloc_array
