	.text
	.p2align 4,,15
	.globl	__libc_scratch_buffer_grow
	.hidden	__libc_scratch_buffer_grow
	.type	__libc_scratch_buffer_grow, @function
__libc_scratch_buffer_grow:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	leaq	16(%rbx), %r12
	leaq	(%rax,%rax), %rbp
	cmpq	%r12, %rdi
	je	.L2
	call	free@PLT
	movq	8(%rbx), %rax
.L2:
	cmpq	%rbp, %rax
	ja	.L3
	movq	%rbp, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L4
	movq	%rax, (%rbx)
	movq	%rbp, 8(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L4:
	movq	%r12, (%rbx)
	movq	$1024, 8(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__libc_scratch_buffer_grow, .-__libc_scratch_buffer_grow
