	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal glibc error: array index %zu not less than array length %zu\n"
	.text
	.p2align 4,,15
	.globl	__libc_dynarray_at_failure
	.hidden	__libc_dynarray_at_failure
	.type	__libc_dynarray_at_failure, @function
__libc_dynarray_at_failure:
	pushq	%rbx
	leaq	.LC0(%rip), %rdx
	movq	%rdi, %r8
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movl	$200, %esi
	subq	$208, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__snprintf
	movq	%rbx, %rdi
	call	__libc_fatal
	.size	__libc_dynarray_at_failure, .-__libc_dynarray_at_failure
	.hidden	__libc_fatal
	.hidden	__snprintf
