	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal glibc error: invalid allocation buffer of size %zu\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___libc_alloc_buffer_create_failure
	.hidden	__GI___libc_alloc_buffer_create_failure
	.type	__GI___libc_alloc_buffer_create_failure, @function
__GI___libc_alloc_buffer_create_failure:
	pushq	%rbx
	leaq	.LC0(%rip), %rdx
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movl	$200, %esi
	subq	$208, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__GI___snprintf
	movq	%rbx, %rdi
	call	__GI___libc_fatal
	.size	__GI___libc_alloc_buffer_create_failure, .-__GI___libc_alloc_buffer_create_failure
	.globl	__libc_alloc_buffer_create_failure
	.set	__libc_alloc_buffer_create_failure,__GI___libc_alloc_buffer_create_failure
