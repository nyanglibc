	.text
	.p2align 4,,15
	.globl	__libc_alloc_buffer_copy_string
	.hidden	__libc_alloc_buffer_copy_string
	.type	__libc_alloc_buffer_copy_string, @function
__libc_alloc_buffer_copy_string:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	strlen
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	1(%rax), %rcx
	jmp	__libc_alloc_buffer_copy_bytes
	.size	__libc_alloc_buffer_copy_string, .-__libc_alloc_buffer_copy_string
	.hidden	__libc_alloc_buffer_copy_bytes
	.hidden	strlen
