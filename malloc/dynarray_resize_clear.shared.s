	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_dynarray_resize_clear
	.hidden	__GI___libc_dynarray_resize_clear
	.type	__GI___libc_dynarray_resize_clear, @function
__GI___libc_dynarray_resize_clear:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	(%rdi), %r12
	call	__GI___libc_dynarray_resize
	testb	%al, %al
	movl	%eax, %r13d
	je	.L1
	subq	%r12, %rbx
	movq	16(%rbp), %rdi
	xorl	%esi, %esi
	movq	%rbx, %rdx
	imulq	%r14, %r12
	imulq	%r14, %rdx
	addq	%r12, %rdi
	call	__GI_memset@PLT
.L1:
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__GI___libc_dynarray_resize_clear, .-__GI___libc_dynarray_resize_clear
	.globl	__libc_dynarray_resize_clear
	.set	__libc_dynarray_resize_clear,__GI___libc_dynarray_resize_clear
