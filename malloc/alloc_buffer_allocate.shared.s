	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_alloc_buffer_allocate
	.hidden	__GI___libc_alloc_buffer_allocate
	.type	__GI___libc_alloc_buffer_allocate, @function
__GI___libc_alloc_buffer_allocate:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	call	malloc@PLT
	xorl	%edx, %edx
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L3
	movq	%rbx, %rdx
	addq	%rax, %rdx
	jc	.L10
.L3:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L10:
	movq	%rax, %rdi
	movq	%rbx, %rsi
	movq	%rdx, 8(%rsp)
	movq	%rax, (%rsp)
	call	__GI___libc_alloc_buffer_create_failure
	movq	8(%rsp), %rdx
	movq	(%rsp), %rax
	jmp	.L3
	.size	__GI___libc_alloc_buffer_allocate, .-__GI___libc_alloc_buffer_allocate
	.globl	__libc_alloc_buffer_allocate
	.set	__libc_alloc_buffer_allocate,__GI___libc_alloc_buffer_allocate
