	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_scratch_buffer_grow
	.hidden	__GI___libc_scratch_buffer_grow
	.type	__GI___libc_scratch_buffer_grow, @function
__GI___libc_scratch_buffer_grow:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	8(%rdi), %rax
	movq	(%rdi), %rdi
	leaq	16(%rbx), %r12
	leaq	(%rax,%rax), %rbp
	cmpq	%r12, %rdi
	je	.L2
	call	free@PLT
	movq	8(%rbx), %rax
.L2:
	cmpq	%rbp, %rax
	ja	.L3
	movq	%rbp, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L4
	movq	%rax, (%rbx)
	movq	%rbp, 8(%rbx)
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L4:
	movq	%r12, (%rbx)
	movq	$1024, 8(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__GI___libc_scratch_buffer_grow, .-__GI___libc_scratch_buffer_grow
	.globl	__libc_scratch_buffer_grow
	.set	__libc_scratch_buffer_grow,__GI___libc_scratch_buffer_grow
