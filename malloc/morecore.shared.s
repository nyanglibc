	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___default_morecore
	.hidden	__GI___default_morecore
	.type	__GI___default_morecore, @function
__GI___default_morecore:
	subq	$8, %rsp
	call	__GI___sbrk
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	addq	$8, %rsp
	ret
	.size	__GI___default_morecore, .-__GI___default_morecore
	.globl	__default_morecore
	.set	__default_morecore,__GI___default_morecore
