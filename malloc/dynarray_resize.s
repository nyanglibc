	.text
	.p2align 4,,15
	.globl	__libc_dynarray_resize
	.hidden	__libc_dynarray_resize
	.type	__libc_dynarray_resize, @function
__libc_dynarray_resize:
	cmpq	%rsi, 8(%rdi)
	jnb	.L23
	movq	%rsi, %rax
	movq	%rdx, %r8
	mulq	%rcx
	jo	.L5
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	movq	16(%rdi), %r12
	cmpq	%r8, %r12
	je	.L24
	movq	%rax, %rsi
	movq	%r12, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L10
.L9:
	movq	%r8, 16(%rbp)
	movq	%rbx, 8(%rbp)
	movl	$1, %eax
	movq	%rbx, 0(%rbp)
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rax, %rdi
	movq	%rcx, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r8
	movq	8(%rsp), %rcx
	je	.L10
	testq	%r12, %r12
	je	.L9
	movq	0(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	imulq	%rcx, %rdx
	call	memcpy@PLT
	movq	%rax, %r8
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	jmp	.L1
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__libc_dynarray_resize, .-__libc_dynarray_resize
