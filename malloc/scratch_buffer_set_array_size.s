	.text
	.p2align 4,,15
	.globl	__libc_scratch_buffer_set_array_size
	.hidden	__libc_scratch_buffer_set_array_size
	.type	__libc_scratch_buffer_set_array_size, @function
__libc_scratch_buffer_set_array_size:
	movq	%rsi, %rax
	pushq	%r12
	pushq	%rbp
	orq	%rdx, %rax
	movq	%rsi, %rbp
	pushq	%rbx
	shrq	$32, %rax
	movq	%rdi, %rbx
	imulq	%rdx, %rbp
	testq	%rax, %rax
	je	.L2
	testq	%rsi, %rsi
	jne	.L16
.L2:
	cmpq	%rbp, 8(%rbx)
	movl	$1, %eax
	jnb	.L1
	movq	(%rbx), %rdi
	leaq	16(%rbx), %r12
	cmpq	%r12, %rdi
	je	.L5
	call	free@PLT
.L5:
	movq	%rbp, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L17
	movq	%rax, (%rbx)
	movq	%rbp, 8(%rbx)
	movl	$1, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rdx, %rcx
	movq	%rbp, %rax
	xorl	%edx, %edx
	divq	%rsi
	cmpq	%rcx, %rax
	je	.L2
	movq	(%rdi), %rdi
	leaq	16(%rbx), %rbp
	cmpq	%rbp, %rdi
	je	.L3
	call	free@PLT
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%rbp, (%rbx)
	movq	$1024, 8(%rbx)
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r12, (%rbx)
	movq	$1024, 8(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__libc_scratch_buffer_set_array_size, .-__libc_scratch_buffer_set_array_size
