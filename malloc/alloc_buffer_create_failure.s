	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"Fatal glibc error: invalid allocation buffer of size %zu\n"
	.text
	.p2align 4,,15
	.globl	__libc_alloc_buffer_create_failure
	.hidden	__libc_alloc_buffer_create_failure
	.type	__libc_alloc_buffer_create_failure, @function
__libc_alloc_buffer_create_failure:
	pushq	%rbx
	leaq	.LC0(%rip), %rdx
	movq	%rsi, %rcx
	xorl	%eax, %eax
	movl	$200, %esi
	subq	$208, %rsp
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	__snprintf
	movq	%rbx, %rdi
	call	__libc_fatal
	.size	__libc_alloc_buffer_create_failure, .-__libc_alloc_buffer_create_failure
	.hidden	__libc_fatal
	.hidden	__snprintf
