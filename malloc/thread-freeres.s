	.text
	.p2align 4,,15
	.globl	__libc_thread_freeres
	.type	__libc_thread_freeres, @function
__libc_thread_freeres:
	subq	$8, %rsp
	cmpq	$0, __res_thread_freeres@GOTPCREL(%rip)
	je	.L2
	call	__res_thread_freeres@PLT
.L2:
	movq	%fs:16, %rax
	movq	2424(%rax), %rdi
	call	free@PLT
	movq	%fs:16, %rax
	movq	2432(%rax), %rdi
	call	free@PLT
	cmpq	$0, __malloc_arena_thread_freeres@GOTPCREL(%rip)
	je	.L1
	addq	$8, %rsp
	jmp	__malloc_arena_thread_freeres@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	ret
	.size	__libc_thread_freeres, .-__libc_thread_freeres
	.weak	__malloc_arena_thread_freeres
	.weak	__res_thread_freeres
	.hidden	__malloc_arena_thread_freeres
	.hidden	__res_thread_freeres
