	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	__GI___libc_freeres
	.hidden	__GI___libc_freeres
	.type	__GI___libc_freeres, @function
__GI___libc_freeres:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgq	%rdx, already_called.12654(%rip)
	jne	.L20
	pushq	%rbp
	pushq	%rbx
	leaq	__start___libc_subfreeres(%rip), %rbx
	subq	$8, %rsp
	call	__nss_module_freeres
	call	__nss_action_freeres
	call	__nss_database_freeres
	call	_IO_cleanup@PLT
	leaq	__stop___libc_subfreeres(%rip), %rax
	cmpq	%rax, %rbx
	jnb	.L3
	leaq	8(%rbx), %rdx
	addq	$7, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L4:
	call	*(%rbx)
	addq	$8, %rbx
	cmpq	%rbp, %rbx
	jne	.L4
.L3:
	cmpq	$0, __libdl_freeres@GOTPCREL(%rip)
	je	.L5
	call	__libdl_freeres@PLT
.L5:
	cmpq	$0, __libpthread_freeres@GOTPCREL(%rip)
	je	.L6
	call	__libpthread_freeres@PLT
.L6:
	leaq	__start___libc_freeres_ptrs(%rip), %rbx
	leaq	__stop___libc_freeres_ptrs(%rip), %rax
	cmpq	%rax, %rbx
	jnb	.L1
	leaq	8(%rbx), %rdx
	addq	$7, %rax
	subq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L7:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	free@PLT
	cmpq	%rbx, %rbp
	jne	.L7
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L20:
	ret
	.size	__GI___libc_freeres, .-__GI___libc_freeres
	.globl	__libc_freeres
	.set	__libc_freeres,__GI___libc_freeres
	.local	already_called.12654
	.comm	already_called.12654,8,8
	.weak	__libpthread_freeres
	.weak	__libdl_freeres
	.hidden	__stop___libc_freeres_ptrs
	.hidden	__start___libc_freeres_ptrs
	.hidden	__stop___libc_subfreeres
	.hidden	__nss_database_freeres
	.hidden	__nss_action_freeres
	.hidden	__nss_module_freeres
	.hidden	__start___libc_subfreeres
