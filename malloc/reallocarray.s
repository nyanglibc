	.text
	.p2align 4,,15
	.globl	__libc_reallocarray
	.hidden	__libc_reallocarray
	.type	__libc_reallocarray, @function
__libc_reallocarray:
	movq	%rsi, %rax
	mulq	%rdx
	movq	%rax, %rsi
	jo	.L3
	jmp	realloc@PLT
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__libc_reallocarray, .-__libc_reallocarray
	.weak	reallocarray
	.set	reallocarray,__libc_reallocarray
