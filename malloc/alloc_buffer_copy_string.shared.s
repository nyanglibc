	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_alloc_buffer_copy_string
	.hidden	__GI___libc_alloc_buffer_copy_string
	.type	__GI___libc_alloc_buffer_copy_string, @function
__GI___libc_alloc_buffer_copy_string:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	movq	%rdx, %rdi
	call	__GI_strlen
	movq	%rbx, %rdx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	popq	%rbx
	popq	%rbp
	popq	%r12
	leaq	1(%rax), %rcx
	jmp	__GI___libc_alloc_buffer_copy_bytes
	.size	__GI___libc_alloc_buffer_copy_string, .-__GI___libc_alloc_buffer_copy_string
	.globl	__libc_alloc_buffer_copy_string
	.set	__libc_alloc_buffer_copy_string,__GI___libc_alloc_buffer_copy_string
