	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_reallocarray
	.hidden	__GI___libc_reallocarray
	.type	__GI___libc_reallocarray, @function
__GI___libc_reallocarray:
	movq	%rsi, %rax
	mulq	%rdx
	movq	%rax, %rsi
	jo	.L3
	jmp	realloc@PLT
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__GI___libc_reallocarray, .-__GI___libc_reallocarray
	.globl	__libc_reallocarray
	.set	__libc_reallocarray,__GI___libc_reallocarray
	.weak	reallocarray
	.set	reallocarray,__libc_reallocarray
