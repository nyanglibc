	.text
	.p2align 4,,15
	.globl	_dl_tunable_set_mmap_threshold
	.type	_dl_tunable_set_mmap_threshold, @function
_dl_tunable_set_mmap_threshold:
	movq	(%rdi), %rax
	cmpq	$33554432, %rax
	ja	.L1
	movq	%rax, 16+mp_(%rip)
	movl	$1, 52+mp_(%rip)
.L1:
	rep ret
	.size	_dl_tunable_set_mmap_threshold, .-_dl_tunable_set_mmap_threshold
	.p2align 4,,15
	.globl	_dl_tunable_set_mmaps_max
	.type	_dl_tunable_set_mmaps_max, @function
_dl_tunable_set_mmaps_max:
	movq	(%rdi), %rax
	movl	$1, 52+mp_(%rip)
	movl	%eax, 44+mp_(%rip)
	ret
	.size	_dl_tunable_set_mmaps_max, .-_dl_tunable_set_mmaps_max
	.p2align 4,,15
	.globl	_dl_tunable_set_top_pad
	.type	_dl_tunable_set_top_pad, @function
_dl_tunable_set_top_pad:
	movq	(%rdi), %rax
	movl	$1, 52+mp_(%rip)
	movq	%rax, 8+mp_(%rip)
	ret
	.size	_dl_tunable_set_top_pad, .-_dl_tunable_set_top_pad
	.p2align 4,,15
	.globl	_dl_tunable_set_perturb_byte
	.type	_dl_tunable_set_perturb_byte, @function
_dl_tunable_set_perturb_byte:
	movq	(%rdi), %rax
	movl	%eax, perturb_byte(%rip)
	ret
	.size	_dl_tunable_set_perturb_byte, .-_dl_tunable_set_perturb_byte
	.p2align 4,,15
	.globl	_dl_tunable_set_trim_threshold
	.type	_dl_tunable_set_trim_threshold, @function
_dl_tunable_set_trim_threshold:
	movq	(%rdi), %rax
	movl	$1, 52+mp_(%rip)
	movq	%rax, mp_(%rip)
	ret
	.size	_dl_tunable_set_trim_threshold, .-_dl_tunable_set_trim_threshold
	.p2align 4,,15
	.globl	_dl_tunable_set_arena_max
	.type	_dl_tunable_set_arena_max, @function
_dl_tunable_set_arena_max:
	movq	(%rdi), %rax
	movq	%rax, 32+mp_(%rip)
	ret
	.size	_dl_tunable_set_arena_max, .-_dl_tunable_set_arena_max
	.p2align 4,,15
	.globl	_dl_tunable_set_arena_test
	.type	_dl_tunable_set_arena_test, @function
_dl_tunable_set_arena_test:
	movq	(%rdi), %rax
	movq	%rax, 24+mp_(%rip)
	ret
	.size	_dl_tunable_set_arena_test, .-_dl_tunable_set_arena_test
	.p2align 4,,15
	.globl	_dl_tunable_set_tcache_max
	.type	_dl_tunable_set_tcache_max, @function
_dl_tunable_set_tcache_max:
	movq	(%rdi), %rax
	cmpq	$1032, %rax
	ja	.L10
	movq	%rax, 88+mp_(%rip)
	addq	$23, %rax
	movl	$1, %edx
	cmpq	$31, %rax
	jbe	.L12
	andq	$-16, %rax
	subq	$17, %rax
	shrq	$4, %rax
	leaq	1(%rax), %rdx
.L12:
	movq	%rdx, 80+mp_(%rip)
.L10:
	rep ret
	.size	_dl_tunable_set_tcache_max, .-_dl_tunable_set_tcache_max
	.p2align 4,,15
	.globl	_dl_tunable_set_tcache_count
	.type	_dl_tunable_set_tcache_count, @function
_dl_tunable_set_tcache_count:
	movq	(%rdi), %rax
	cmpq	$65535, %rax
	ja	.L14
	movq	%rax, 96+mp_(%rip)
.L14:
	rep ret
	.size	_dl_tunable_set_tcache_count, .-_dl_tunable_set_tcache_count
	.p2align 4,,15
	.globl	_dl_tunable_set_tcache_unsorted_limit
	.type	_dl_tunable_set_tcache_unsorted_limit, @function
_dl_tunable_set_tcache_unsorted_limit:
	movq	(%rdi), %rax
	movq	%rax, 104+mp_(%rip)
	ret
	.size	_dl_tunable_set_tcache_unsorted_limit, .-_dl_tunable_set_tcache_unsorted_limit
	.p2align 4,,15
	.globl	_dl_tunable_set_mxfast
	.type	_dl_tunable_set_mxfast, @function
_dl_tunable_set_mxfast:
	movq	(%rdi), %rax
	cmpq	$160, %rax
	ja	.L17
	cmpq	$7, %rax
	movl	$16, %edx
	jbe	.L19
	leaq	8(%rax), %rdx
	andq	$-16, %rdx
.L19:
	movq	%rdx, global_max_fast(%rip)
.L17:
	rep ret
	.size	_dl_tunable_set_mxfast, .-_dl_tunable_set_mxfast
	.p2align 4,,15
	.type	mem2mem_check, @function
mem2mem_check:
	testq	%rdi, %rdi
	movq	%rdi, %rax
	je	.L22
	leaq	-16(%rdi), %rdx
	movq	%rdx, %rdi
	shrq	$11, %rdx
	shrq	$3, %rdi
	xorl	%edx, %edi
	movl	$2, %edx
	cmpb	$1, %dil
	cmove	%edx, %edi
	movq	-8(%rax), %rdx
	movq	%rdx, %rcx
	andq	$-8, %rcx
	andl	$2, %edx
	sete	%dl
	movzbl	%dl, %edx
	leaq	-17(%rcx,%rdx,8), %rcx
	cmpq	%rsi, %rcx
	jbe	.L25
	movzbl	%dil, %r8d
	movl	$255, %r10d
	leaq	-1(%r8), %r9
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	cmpq	$255, %rdx
	cmova	%r10, %rdx
	cmpq	%r8, %rdx
	cmove	%r9, %rdx
	movb	%dl, (%rax,%rcx)
	subq	%rdx, %rcx
	cmpq	%rcx, %rsi
	jb	.L27
.L25:
	movb	%dil, (%rax,%rsi)
.L22:
	rep ret
	.size	mem2mem_check, .-mem2mem_check
	.p2align 4,,15
	.type	mem2chunk_check, @function
mem2chunk_check:
	testb	$15, %dil
	jne	.L69
	leaq	-16(%rdi), %rax
	movq	-8(%rdi), %r9
	movq	%rax, %r8
	movq	%rax, %rdx
	shrq	$11, %rdx
	shrq	$3, %r8
	movq	%r9, %rcx
	xorl	%edx, %r8d
	andq	$-8, %rcx
	movl	$2, %edx
	cmpb	$1, %r8b
	cmove	%edx, %r8d
	testb	$2, %r9b
	jne	.L37
	movl	4+main_arena(%rip), %edx
	andl	$2, %edx
	jne	.L38
	movq	72+mp_(%rip), %r10
	cmpq	%rax, %r10
	ja	.L69
	addq	2184+main_arena(%rip), %r10
	leaq	(%rax,%rcx), %r11
	cmpq	%r10, %r11
	jnb	.L69
.L38:
	cmpq	$31, %rcx
	jbe	.L69
	testb	$8, %r9b
	jne	.L69
	testb	$1, -8(%rdi,%rcx)
	je	.L69
	andl	$1, %r9d
	je	.L89
.L39:
	leaq	7(%rcx), %rdi
	leaq	(%rax,%rdi), %rcx
	movzbl	(%rcx), %edx
	cmpb	%dl, %r8b
	je	.L41
	testq	%rdx, %rdx
	je	.L69
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rdi
	jnb	.L42
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L43:
	testq	%rdx, %rdx
	je	.L69
	leaq	16(%rdx), %rcx
	cmpq	%rdi, %rcx
	ja	.L69
.L42:
	subq	%rdx, %rdi
	leaq	(%rax,%rdi), %rcx
	movzbl	(%rcx), %edx
	cmpb	%r8b, %dl
	jne	.L43
.L41:
	movl	%r8d, %edx
	testq	%rsi, %rsi
	notl	%edx
	movb	%dl, (%rcx)
	je	.L34
	movq	%rcx, (%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	-16(%rdi), %rdi
	testb	$15, %dil
	jne	.L69
	movq	%rax, %r9
	subq	%rdi, %r9
	testl	%edx, %edx
	jne	.L40
	cmpq	%r9, 72+mp_(%rip)
	ja	.L69
.L40:
	movq	8(%r9), %rdx
	andq	$-8, %rdx
	cmpq	%rdx, %rdi
	je	.L39
	.p2align 4,,10
	.p2align 3
.L69:
	xorl	%eax, %eax
.L34:
	rep ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	_dl_pagesize(%rip), %rdx
	movq	%rdi, %r10
	subq	$1, %rdx
	andq	%rdx, %r10
	leaq	-16(%r10), %r11
	testq	$-17, %r11
	je	.L44
	leaq	-1(%r10), %r11
	cmpq	$8190, %r11
	ja	.L44
	leaq	-64(%r10), %r11
	testq	$-65, %r11
	jne	.L90
.L44:
	andl	$3, %r9d
	cmpq	$2, %r9
	jne	.L69
	movq	-16(%rdi), %rdi
	movq	%rax, %r9
	subq	%rdi, %r9
	addq	%rcx, %rdi
	orq	%r9, %rdi
	testq	%rdx, %rdi
	jne	.L69
	leaq	-1(%rcx), %rdx
	leaq	(%rax,%rdx), %rcx
	movzbl	(%rcx), %edi
	cmpb	%dil, %r8b
	je	.L41
	testq	%rdi, %rdi
	je	.L69
	leaq	16(%rdi), %rcx
	cmpq	%rcx, %rdx
	jnb	.L45
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L46:
	testq	%rdi, %rdi
	je	.L69
	leaq	16(%rdi), %rcx
	cmpq	%rdx, %rcx
	ja	.L69
.L45:
	subq	%rdi, %rdx
	leaq	(%rax,%rdx), %rcx
	movzbl	(%rcx), %edi
	cmpb	%r8b, %dil
	jne	.L46
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L90:
	leaq	-256(%r10), %r11
	testq	$-257, %r11
	je	.L44
	leaq	-1024(%r10), %r11
	testq	$-1025, %r11
	je	.L44
	cmpq	$4096, %r10
	je	.L44
	xorl	%eax, %eax
	jmp	.L34
	.size	mem2chunk_check, .-mem2chunk_check
	.p2align 4,,15
	.type	malloc_init_state, @function
malloc_init_state:
	leaq	96(%rdi), %rcx
	leaq	2128(%rdi), %rdx
	movq	%rcx, %rax
	.p2align 4,,10
	.p2align 3
.L92:
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	addq	$16, %rax
	cmpq	%rdx, %rax
	jne	.L92
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rdi
	je	.L93
	orl	$2, 4(%rdi)
.L94:
	movl	$0, 8(%rdi)
	movq	%rcx, 96(%rdi)
	ret
.L93:
	movq	$128, global_max_fast(%rip)
	jmp	.L94
	.size	malloc_init_state, .-malloc_init_state
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"%s\n"
	.text
	.p2align 4,,15
	.type	malloc_printerr, @function
malloc_printerr:
	leaq	.LC0(%rip), %rsi
	movq	%rdi, %rdx
	subq	$8, %rsp
	movl	$1, %edi
	xorl	%eax, %eax
	call	__libc_message
	.size	malloc_printerr, .-malloc_printerr
	.section	.rodata.str1.1
.LC1:
	.string	"malloc: top chunk is corrupt"
	.text
	.p2align 4,,15
	.type	top_check, @function
top_check:
	movq	96+main_arena(%rip), %rax
	leaq	96+main_arena(%rip), %rdx
	cmpq	%rdx, %rax
	je	.L99
	movq	8(%rax), %rdx
	testb	$2, %dl
	jne	.L101
	movq	%rdx, %rcx
	andq	$-8, %rcx
	cmpq	$31, %rcx
	jbe	.L101
	andl	$1, %edx
	je	.L101
	testb	$2, 4+main_arena(%rip)
	jne	.L99
	movq	2184+main_arena(%rip), %rdx
	addq	72+mp_(%rip), %rdx
	addq	%rcx, %rax
	cmpq	%rdx, %rax
	jne	.L101
.L99:
	rep ret
.L101:
	leaq	.LC1(%rip), %rdi
	subq	$8, %rsp
	call	malloc_printerr
	.size	top_check, .-top_check
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"int_mallinfo(): unaligned fastbin chunk detected"
	.text
	.p2align 4,,15
	.type	int_mallinfo, @function
int_mallinfo:
	pushq	%rbp
	pushq	%rbx
	leaq	16(%rdi), %r8
	leaq	96(%rdi), %r9
	xorl	%r11d, %r11d
	xorl	%r10d, %r10d
	subq	$8, %rsp
.L112:
	movq	(%r8), %rax
	testq	%rax, %rax
	jne	.L111
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L110:
	movq	8(%rax), %rdx
	movq	16(%rax), %rcx
	addl	$1, %r11d
	andq	$-8, %rdx
	addq	%rdx, %r10
	leaq	16(%rax), %rdx
	shrq	$12, %rdx
	movq	%rdx, %rax
	xorq	%rcx, %rax
	cmpq	%rcx, %rdx
	je	.L108
.L111:
	testb	$15, %al
	je	.L110
	leaq	.LC2(%rip), %rdi
	call	malloc_printerr
.L108:
	addq	$8, %r8
	cmpq	%r8, %r9
	jne	.L112
	movq	96(%rdi), %rax
	leaq	2128(%rdi), %rbx
	movl	$1, %ecx
	movq	8(%rax), %rbp
	andq	$-8, %rbp
	leaq	(%r10,%rbp), %r8
	.p2align 4,,10
	.p2align 3
.L115:
	movq	24(%r9), %rax
	cmpq	%rax, %r9
	je	.L113
	.p2align 4,,10
	.p2align 3
.L114:
	movq	8(%rax), %rdx
	movq	24(%rax), %rax
	addl	$1, %ecx
	andq	$-8, %rdx
	addq	%rdx, %r8
	cmpq	%rax, %r9
	jne	.L114
.L113:
	addq	$16, %r9
	cmpq	%r9, %rbx
	jne	.L115
	movq	2184(%rdi), %rax
	addq	%rax, (%rsi)
	movslq	%r11d, %r11
	movslq	%ecx, %rcx
	addq	%r11, 16(%rsi)
	addq	%rcx, 8(%rsi)
	movq	%rax, %rdx
	leaq	main_arena(%rip), %rax
	addq	%r8, 64(%rsi)
	subq	%r8, %rdx
	addq	%r10, 48(%rsi)
	addq	%rdx, 56(%rsi)
	cmpq	%rax, %rdi
	jne	.L107
	movslq	40+mp_(%rip), %rax
	movq	%rbp, 72(%rsi)
	movq	%rax, 24(%rsi)
	movq	56+mp_(%rip), %rax
	movq	$0, 40(%rsi)
	movq	%rax, 32(%rsi)
.L107:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	int_mallinfo, .-int_mallinfo
	.section	.rodata.str1.1
.LC3:
	.string	": "
.LC4:
	.string	""
	.section	.rodata.str1.8
	.align 8
.LC5:
	.string	"%s%s%s:%u: %s%sAssertion `%s' failed.\n"
	.text
	.p2align 4,,15
	.type	__malloc_assert, @function
__malloc_assert:
	subq	$16, %rsp
	movl	%edx, %r9d
	movq	__progname(%rip), %rdx
	leaq	.LC3(%rip), %r10
	leaq	.LC4(%rip), %rax
	movq	%rsi, %r8
	leaq	.LC5(%rip), %rsi
	cmpb	$0, (%rdx)
	pushq	%rdi
	pushq	%r10
	pushq	%rcx
	cmovne	%r10, %rax
	xorl	%edi, %edi
	movq	%rax, %rcx
	xorl	%eax, %eax
	call	__fxprintf
	movq	stderr(%rip), %rdi
	addq	$32, %rsp
	call	fflush
	call	abort
	.size	__malloc_assert, .-__malloc_assert
	.p2align 4,,15
	.type	new_heap, @function
new_heap:
	pushq	%r13
	pushq	%r12
	addq	%rdi, %rsi
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$32767, %rsi
	movq	_dl_pagesize(%rip), %rbp
	jbe	.L143
	cmpq	$67108864, %rsi
	ja	.L160
.L132:
	movq	aligned_heap_area(%rip), %rdi
	leaq	-1(%rsi,%rbp), %rax
	negq	%rbp
	andq	%rax, %rbp
	testq	%rdi, %rdi
	je	.L135
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	movl	$-1, %r8d
	movl	$16418, %ecx
	movl	$67108864, %esi
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %rbx
	movq	$0, aligned_heap_area(%rip)
	je	.L135
	testl	$67108863, %eax
	jne	.L161
.L137:
	movl	$3, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__mprotect
	testl	%eax, %eax
	jne	.L158
	movq	%rbp, 16(%rbx)
	movq	%rbp, 24(%rbx)
.L131:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$67108864, %esi
	movq	%rax, %rdi
	call	__munmap
.L135:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$16418, %ecx
	movl	$134217728, %esi
	call	__mmap
	cmpq	$-1, %rax
	je	.L138
	leaq	67108863(%rax), %rbx
	andq	$-67108864, %rbx
	movq	%rbx, %r13
	leaq	67108864(%rbx), %r12
	subq	%rax, %r13
	jne	.L162
	movq	%r12, aligned_heap_area(%rip)
.L140:
	movl	$67108864, %esi
	movq	%r12, %rdi
	subq	%r13, %rsi
	call	__munmap
	jmp	.L137
	.p2align 4,,10
	.p2align 3
.L143:
	movl	$32768, %esi
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L160:
	cmpq	$67108864, %rdi
	movl	$67108864, %esi
	jbe	.L132
.L159:
	xorl	%ebx, %ebx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L162:
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__munmap
	jmp	.L140
	.p2align 4,,10
	.p2align 3
.L138:
	xorl	%r9d, %r9d
	xorl	%edx, %edx
	xorl	%edi, %edi
	movl	%eax, %r8d
	movl	$16418, %ecx
	movl	$67108864, %esi
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %rbx
	je	.L159
	testl	$67108863, %eax
	je	.L137
	.p2align 4,,10
	.p2align 3
.L158:
	movq	%rbx, %rdi
	movl	$67108864, %esi
	xorl	%ebx, %ebx
	call	__munmap
	jmp	.L131
	.size	new_heap, .-new_heap
	.section	.rodata.str1.1
.LC6:
	.string	"malloc.c"
.LC7:
	.string	"chunk_is_mmapped (p)"
	.section	.rodata.str1.8
	.align 8
.LC8:
	.string	"munmap_chunk(): invalid pointer"
	.text
	.p2align 4,,15
	.type	munmap_chunk, @function
munmap_chunk:
	subq	$8, %rsp
	movq	8(%rdi), %rax
	movq	%rax, %rdx
	andq	$-8, %rdx
	testb	$2, %al
	je	.L168
	movq	(%rdi), %rsi
	movq	_dl_pagesize(%rip), %rax
	leaq	16(%rdi), %rcx
	subq	%rsi, %rdi
	addq	%rdx, %rsi
	subq	$1, %rax
	movq	%rdi, %rdx
	orq	%rsi, %rdx
	testq	%rax, %rdx
	jne	.L165
	andq	%rcx, %rax
	leaq	-1(%rax), %rdx
	testq	%rax, %rdx
	jne	.L165
#APP
# 2997 "malloc.c" 1
	lock;decl 40+mp_(%rip)
# 0 "" 2
#NO_APP
	movq	%rsi, %rax
	negq	%rax
#APP
# 2998 "malloc.c" 1
	lock;addq %rax, 56+mp_(%rip)
# 0 "" 2
#NO_APP
	addq	$8, %rsp
	jmp	__munmap
	.p2align 4,,10
	.p2align 3
.L165:
	leaq	.LC8(%rip), %rdi
	call	malloc_printerr
.L168:
	leaq	__PRETTY_FUNCTION__.12735(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$2978, %edx
	call	__malloc_assert
	.size	munmap_chunk, .-munmap_chunk
	.section	.rodata.str1.8
	.align 8
.LC9:
	.string	"mremap_chunk(): invalid pointer"
	.section	.rodata.str1.1
.LC10:
	.string	"aligned_OK (chunk2rawmem (p))"
.LC11:
	.string	"prev_size (p) == offset"
	.text
	.p2align 4,,15
	.type	mremap_chunk, @function
mremap_chunk:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	8(%rdi), %rax
	movq	_dl_pagesize(%rip), %rcx
	movq	(%rdi), %rbp
	movq	%rax, %r12
	andq	$-8, %r12
	testb	$2, %al
	je	.L182
	movq	%rdi, %r9
	leaq	0(%rbp,%r12), %r8
	leaq	-1(%rcx), %rax
	subq	%rbp, %r9
	leaq	16(%rdi), %r10
	movq	%r9, %rdx
	orq	%r8, %rdx
	testq	%rax, %rdx
	jne	.L171
	andq	%r10, %rax
	leaq	-1(%rax), %rdx
	testq	%rax, %rdx
	jne	.L171
	leaq	7(%rcx,%rsi), %rbx
	negq	%rcx
	addq	%rbp, %rbx
	andq	%rcx, %rbx
	cmpq	%rbx, %r8
	je	.L169
	xorl	%eax, %eax
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r8, %rsi
	movq	%r9, %rdi
	call	__mremap
	cmpq	$-1, %rax
	je	.L178
	leaq	(%rax,%rbp), %rdi
	testb	$15, %dil
	jne	.L183
	cmpq	%rbp, (%rdi)
	jne	.L184
	movq	%rbx, %rax
	subq	%r12, %rbx
	subq	%rbp, %rax
	subq	%rbp, %rbx
	orq	$2, %rax
	movq	%rax, 8(%rdi)
	movq	%rbx, %rax
	lock xaddq	%rax, 56+mp_(%rip)
	addq	%rax, %rbx
.L176:
	movq	64+mp_(%rip), %rax
	cmpq	%rax, %rbx
	jbe	.L169
	lock cmpxchgq	%rbx, 64+mp_(%rip)
	jne	.L176
.L169:
	popq	%rbx
	movq	%rdi, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	leaq	.LC9(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L178:
	xorl	%edi, %edi
	jmp	.L169
.L182:
	leaq	__PRETTY_FUNCTION__.12747(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$3016, %edx
	call	__malloc_assert
.L183:
	leaq	__PRETTY_FUNCTION__.12747(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$3040, %edx
	call	__malloc_assert
.L184:
	leaq	__PRETTY_FUNCTION__.12747(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$3042, %edx
	call	__malloc_assert
	.size	mremap_chunk, .-mremap_chunk
	.p2align 4,,15
	.type	ptmalloc_init.part.0, @function
ptmalloc_init.part.0:
	pushq	%rbx
	leaq	main_arena(%rip), %rdi
	subq	$16, %rsp
	movq	thread_arena@gottpoff(%rip), %rax
	movl	$0, __libc_malloc_initialized(%rip)
	leaq	8(%rsp), %rbx
	movq	%rdi, %fs:(%rax)
	call	malloc_init_state
	leaq	_dl_tunable_set_mallopt_check(%rip), %rdx
	movq	%rbx, %rsi
	movl	$30, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_top_pad(%rip), %rdx
	movq	%rbx, %rsi
	movl	$11, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_perturb_byte(%rip), %rdx
	movq	%rbx, %rsi
	movl	$3, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_mmap_threshold(%rip), %rdx
	movq	%rbx, %rsi
	movl	$23, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_trim_threshold(%rip), %rdx
	movq	%rbx, %rsi
	movl	$2, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_mmaps_max(%rip), %rdx
	movq	%rbx, %rsi
	movl	$16, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_arena_max(%rip), %rdx
	movq	%rbx, %rsi
	movl	$22, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_arena_test(%rip), %rdx
	movq	%rbx, %rsi
	movl	$26, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_tcache_max(%rip), %rdx
	movq	%rbx, %rsi
	movl	$29, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_tcache_count(%rip), %rdx
	movq	%rbx, %rsi
	movl	$25, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_tcache_unsorted_limit(%rip), %rdx
	movq	%rbx, %rsi
	movl	$18, %edi
	call	__tunable_get_val@PLT
	leaq	_dl_tunable_set_mxfast(%rip), %rdx
	movq	%rbx, %rsi
	movl	$9, %edi
	call	__tunable_get_val@PLT
	movl	$1, __libc_malloc_initialized(%rip)
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	ptmalloc_init.part.0, .-ptmalloc_init.part.0
	.section	.rodata.str1.1
.LC12:
	.string	"corrupted size vs. prev_size"
.LC13:
	.string	"corrupted double-linked list"
	.section	.rodata.str1.8
	.align 8
.LC14:
	.string	"corrupted double-linked list (not small)"
	.text
	.p2align 4,,15
	.type	unlink_chunk.isra.2, @function
unlink_chunk.isra.2:
	subq	$8, %rsp
	movq	8(%rdi), %rcx
	movq	%rcx, %rax
	andq	$-8, %rax
	cmpq	(%rdi,%rax), %rax
	jne	.L200
	movq	16(%rdi), %rax
	movq	24(%rdi), %rdx
	cmpq	24(%rax), %rdi
	jne	.L189
	cmpq	16(%rdx), %rdi
	jne	.L189
	cmpq	$1023, %rcx
	movq	%rdx, 24(%rax)
	movq	%rax, 16(%rdx)
	jbe	.L187
	movq	32(%rdi), %rdx
	testq	%rdx, %rdx
	je	.L187
	cmpq	40(%rdx), %rdi
	jne	.L192
	movq	40(%rdi), %rcx
	cmpq	32(%rcx), %rdi
	jne	.L192
	cmpq	$0, 32(%rax)
	je	.L201
	movq	%rcx, 40(%rdx)
	movq	40(%rdi), %rax
	movq	%rdx, 32(%rax)
.L187:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L189:
	leaq	.LC13(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L201:
	cmpq	%rdx, %rdi
	je	.L202
	movq	%rdx, 32(%rax)
	movq	32(%rdi), %rdx
	movq	%rcx, 40(%rax)
	movq	%rax, 40(%rdx)
	movq	40(%rdi), %rdx
	movq	%rax, 32(%rdx)
	jmp	.L187
	.p2align 4,,10
	.p2align 3
.L202:
	movq	%rax, 40(%rax)
	movq	%rax, 32(%rax)
	jmp	.L187
.L200:
	leaq	.LC12(%rip), %rdi
	call	malloc_printerr
.L192:
	leaq	.LC14(%rip), %rdi
	call	malloc_printerr
	.size	unlink_chunk.isra.2, .-unlink_chunk.isra.2
	.section	.rodata.str1.8
	.align 8
.LC15:
	.string	"malloc_consolidate(): unaligned fastbin chunk detected"
	.align 8
.LC16:
	.string	"malloc_consolidate(): invalid chunk size"
	.align 8
.LC17:
	.string	"corrupted size vs. prev_size in fastbins"
	.text
	.p2align 4,,15
	.type	malloc_consolidate, @function
malloc_consolidate:
	pushq	%r15
	pushq	%r14
	leaq	96(%rdi), %rax
	pushq	%r13
	pushq	%r12
	leaq	16(%rdi), %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	subq	$56, %rsp
	movl	$0, 8(%rdi)
	movq	%rax, 32(%rsp)
	leaq	88(%rdi), %rax
	movq	%rax, 40(%rsp)
.L215:
	xorl	%ebx, %ebx
#APP
# 4702 "malloc.c" 1
	xchgq %rbx, (%r14)
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	jne	.L214
	jmp	.L204
	.p2align 4,,10
	.p2align 3
.L226:
	testb	$1, 8(%rax,%rcx)
	je	.L222
	andq	$-2, 8(%rax)
.L211:
	movq	112(%r13), %rax
	cmpq	$1023, %rbp
	movq	%rbx, 112(%r13)
	movq	%rbx, 24(%rax)
	jbe	.L212
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
.L212:
	movq	32(%rsp), %rdx
	movq	%rbp, %rcx
	movq	%rax, 16(%rbx)
	orq	$1, %rcx
	cmpq	%r12, %r15
	movq	%rcx, 8(%rbx)
	movq	%rdx, 24(%rbx)
	movq	%rbp, (%rbx,%rbp)
	movq	%r9, %rbx
	je	.L204
.L214:
	testb	$15, %bl
	jne	.L223
	movq	8(%rbx), %rdi
	movl	%edi, %eax
	shrl	$4, %eax
	subl	$2, %eax
	leaq	16(%r13,%rax,8), %rax
	cmpq	%rax, %r14
	jne	.L224
	movq	%rdi, %rbp
	leaq	16(%rbx), %r15
	movq	16(%rbx), %r12
	andq	$-8, %rbp
	leaq	(%rbx,%rbp), %rax
	shrq	$12, %r15
	movq	%r15, %r9
	movq	8(%rax), %rcx
	xorq	%r12, %r9
	andq	$-8, %rcx
	andl	$1, %edi
	jne	.L207
	movq	(%rbx), %rdi
	subq	%rdi, %rbx
	addq	%rdi, %rbp
	movq	8(%rbx), %r10
	andq	$-8, %r10
	cmpq	%rdi, %r10
	jne	.L225
	movq	%rbx, %rdi
	movq	%rcx, 24(%rsp)
	movq	%rax, 16(%rsp)
	movq	%r9, 8(%rsp)
	call	unlink_chunk.isra.2
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rax
	movq	8(%rsp), %r9
.L207:
	cmpq	%rax, 96(%r13)
	jne	.L226
	addq	%rcx, %rbp
	orq	$1, %rbp
	cmpq	%r12, %r15
	movq	%rbp, 8(%rbx)
	movq	%rbx, 96(%r13)
	movq	%r9, %rbx
	jne	.L214
	.p2align 4,,10
	.p2align 3
.L204:
	addq	$8, %r14
	leaq	-8(%r14), %rax
	cmpq	%rax, 40(%rsp)
	jne	.L215
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L222:
	movq	%rax, %rdi
	movq	%r9, 8(%rsp)
	addq	%rcx, %rbp
	call	unlink_chunk.isra.2
	movq	8(%rsp), %r9
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L223:
	leaq	.LC15(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L225:
	leaq	.LC17(%rip), %rdi
	call	malloc_printerr
.L224:
	leaq	.LC16(%rip), %rdi
	call	malloc_printerr
	.size	malloc_consolidate, .-malloc_consolidate
	.section	.rodata.str1.1
.LC18:
	.string	"arena.c"
	.section	.rodata.str1.8
	.align 8
.LC19:
	.string	"replaced_arena->attached_threads > 0"
	.section	.text.unlikely,"ax",@progbits
	.type	detach_arena.part.3, @function
detach_arena.part.3:
	leaq	__PRETTY_FUNCTION__.11963(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	subq	$8, %rsp
	movl	$726, %edx
	call	__malloc_assert
	.size	detach_arena.part.3, .-detach_arena.part.3
	.section	.rodata.str1.1
.LC20:
	.string	"result->attached_threads == 0"
	.text
	.p2align 4,,15
	.type	get_free_list, @function
get_free_list:
	cmpq	$0, free_list(%rip)
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	je	.L241
	movq	thread_arena@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %r12
#APP
# 813 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L232
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L233:
	movq	free_list(%rip), %rbx
	testq	%rbx, %rbx
	je	.L235
	cmpq	$0, 2176(%rbx)
	movq	2168(%rbx), %rax
	movq	%rax, free_list(%rip)
	jne	.L251
	testq	%r12, %r12
	movq	$1, 2176(%rbx)
	je	.L235
	movq	2176(%r12), %rax
	testq	%rax, %rax
	je	.L252
	subq	$1, %rax
	movq	%rax, 2176(%r12)
.L235:
#APP
# 825 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L239
	subl	$1, free_list_lock(%rip)
.L240:
	testq	%rbx, %rbx
	je	.L241
#APP
# 830 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L242
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L243:
	movq	%rbx, %fs:0(%rbp)
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	xorl	%ebx, %ebx
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L242:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L243
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L232:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L233
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L239:
	xorl	%eax, %eax
#APP
# 825 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L240
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 825 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L240
.L251:
	leaq	__PRETTY_FUNCTION__.12067(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$820, %edx
	call	__malloc_assert
.L252:
	call	detach_arena.part.3
	.size	get_free_list, .-get_free_list
	.section	.rodata.str1.1
.LC21:
	.string	"p->attached_threads == 0"
	.text
	.p2align 4,,15
	.type	arena_get2.part.4, @function
arena_get2.part.4:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	narenas_limit.12181(%rip), %rax
	movq	narenas(%rip), %rdx
	testq	%rax, %rax
	jne	.L259
	movq	32+mp_(%rip), %rax
	testq	%rax, %rax
	je	.L255
	movq	%rax, narenas_limit.12181(%rip)
.L259:
	subq	$1, %rax
	cmpq	%rdx, %rax
	jnb	.L320
	movq	next_to_use.12122(%rip), %rbx
	testq	%rbx, %rbx
	je	.L321
.L276:
	xorl	%ecx, %ecx
	movl	$1, %edx
	jmp	.L280
	.p2align 4,,10
	.p2align 3
.L323:
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L279
.L324:
	movq	2160(%rbx), %rbx
	cmpq	next_to_use.12122(%rip), %rbx
	je	.L322
.L280:
#APP
# 875 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L323
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rbx)
	setne	%al
	movzbl	%al, %eax
	testl	%eax, %eax
	jne	.L324
	.p2align 4,,10
	.p2align 3
.L279:
	movq	thread_arena@gottpoff(%rip), %rbp
	movq	%fs:0(%rbp), %r12
#APP
# 897 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L283
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L284:
	testq	%r12, %r12
	je	.L285
	movq	2176(%r12), %rax
	testq	%rax, %rax
	je	.L286
	subq	$1, %rax
	movq	%rax, 2176(%r12)
.L285:
	movq	free_list(%rip), %rdx
	testq	%rdx, %rdx
	je	.L287
	cmpq	$0, 2176(%rdx)
	jne	.L288
	cmpq	%rdx, %rbx
	jne	.L290
	jmp	.L325
	.p2align 4,,10
	.p2align 3
.L292:
	cmpq	$0, 2176(%rax)
	jne	.L288
	cmpq	%rax, %rbx
	je	.L326
	movq	%rax, %rdx
.L290:
	movq	2168(%rdx), %rax
	testq	%rax, %rax
	jne	.L292
.L287:
	addq	$1, 2176(%rbx)
#APP
# 912 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L293
	subl	$1, free_list_lock(%rip)
.L294:
	movq	2160(%rbx), %rax
	movq	%rbx, %fs:0(%rbp)
	movq	%rax, next_to_use.12122(%rip)
.L253:
	addq	$24, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L255:
	cmpq	%rdx, 24+mp_(%rip)
	jnb	.L259
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	__get_nprocs
	testl	%eax, %eax
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	jle	.L256
	sall	$3, %eax
	movq	narenas(%rip), %rdx
	cltq
	movq	%rax, narenas_limit.12181(%rip)
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L322:
	cmpq	%rbx, %rsi
	je	.L327
.L281:
#APP
# 890 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L282
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L326:
	leaq	2168(%rdx), %rcx
	movq	%rbx, %rdx
.L289:
	movq	2168(%rdx), %rax
	movq	%rax, (%rcx)
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L321:
	leaq	main_arena(%rip), %rbx
	movq	%rbx, next_to_use.12122(%rip)
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L327:
	movq	2160(%rbx), %rbx
	jmp	.L281
	.p2align 4,,10
	.p2align 3
.L320:
	leaq	1(%rdx), %rcx
	movq	%rdx, %rax
#APP
# 960 "arena.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rcx, narenas(%rip)
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rax
	je	.L258
	movq	narenas(%rip), %rdx
	movq	narenas_limit.12181(%rip), %rax
	jmp	.L259
	.p2align 4,,10
	.p2align 3
.L283:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L284
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L293:
	xorl	%eax, %eax
#APP
# 912 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L294
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 912 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L294
.L325:
	leaq	free_list(%rip), %rcx
	jmp	.L289
.L282:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L279
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L279
.L256:
	movq	$16, narenas_limit.12181(%rip)
	movq	narenas(%rip), %rdx
	movl	$16, %eax
	jmp	.L259
.L258:
	movq	8+mp_(%rip), %rsi
	addq	$2248, %rdi
	call	new_heap
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L328
.L260:
	leaq	32(%rbp), %r12
	movq	%r12, 0(%rbp)
	movq	%r12, %rdi
	movq	%r12, %rbx
	call	malloc_init_state
	movq	16(%rbp), %rax
	leaq	2248(%rbp), %rcx
	movq	$1, 2208(%rbp)
	leaq	2232(%rbp), %rdx
	andl	$15, %ecx
	movq	%rax, 2224(%rbp)
	movq	%rax, 2216(%rbp)
	je	.L262
	subq	%rcx, %rdx
	addq	$16, %rdx
.L262:
	addq	%rbp, %rax
	movq	%rdx, 128(%rbp)
	subq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, 8(%rdx)
	movq	thread_arena@gottpoff(%rip), %rax
	movl	$0, 32(%rbp)
	movq	%fs:(%rax), %r13
	movq	%r12, %fs:(%rax)
#APP
# 773 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L263
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_lock(%rip)
# 0 "" 2
#NO_APP
.L264:
	movq	2160+main_arena(%rip), %rax
	movq	%rax, 2192(%rbp)
	movq	%r12, 2160+main_arena(%rip)
#APP
# 783 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L265
	subl	$1, list_lock(%rip)
.L266:
#APP
# 785 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L267
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L268:
	testq	%r13, %r13
	je	.L269
	movq	2176(%r13), %rax
	testq	%rax, %rax
	je	.L286
	subq	$1, %rax
	movq	%rax, 2176(%r13)
.L269:
#APP
# 787 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L271
	subl	$1, free_list_lock(%rip)
.L272:
#APP
# 799 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L273
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 32(%rbp)
# 0 "" 2
#NO_APP
.L274:
	testq	%r12, %r12
	jne	.L253
.L275:
#APP
# 964 "arena.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	decq narenas(%rip)
# 0 "" 2
#NO_APP
	xorl	%ebx, %ebx
	jmp	.L253
.L288:
	leaq	__PRETTY_FUNCTION__.12114(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$846, %edx
	call	__malloc_assert
.L328:
	movq	8+mp_(%rip), %rsi
	movl	$2248, %edi
	call	new_heap
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L260
	jmp	.L275
	.p2align 4,,10
	.p2align 3
.L267:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L268
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L265:
	xorl	%eax, %eax
#APP
# 783 "arena.c" 1
	xchgl %eax, list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L266
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 783 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L263:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_lock(%rip)
	je	.L264
	leaq	list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L273:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L274
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L274
	.p2align 4,,10
	.p2align 3
.L271:
	xorl	%eax, %eax
#APP
# 787 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L272
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 787 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L272
.L286:
	call	detach_arena.part.3
	.size	arena_get2.part.4, .-arena_get2.part.4
	.p2align 4,,15
	.type	arena_get_retry, @function
arena_get_retry:
	pushq	%rbx
	leaq	main_arena(%rip), %rax
	subq	$16, %rsp
	cmpq	%rax, %rdi
	je	.L330
#APP
# 982 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L331
	subl	$1, (%rdi)
.L332:
#APP
# 984 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L333
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
	leaq	main_arena(%rip), %rax
.L329:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L330:
	movq	%rsi, %rbx
#APP
# 988 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L335
	subl	$1, (%rdi)
.L336:
	call	get_free_list
	testq	%rax, %rax
	jne	.L329
	addq	$16, %rsp
	movq	%rbx, %rdi
	leaq	main_arena(%rip), %rsi
	popq	%rbx
	jmp	arena_get2.part.4
	.p2align 4,,10
	.p2align 3
.L331:
	xorl	%eax, %eax
#APP
# 982 "arena.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L332
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 982 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L332
	.p2align 4,,10
	.p2align 3
.L333:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	leaq	main_arena(%rip), %rax
	je	.L329
	movq	%rax, %rdi
	movq	%rax, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rax
	jmp	.L329
	.p2align 4,,10
	.p2align 3
.L335:
	xorl	%eax, %eax
#APP
# 988 "arena.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L336
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 988 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L336
	.size	arena_get_retry, .-arena_get_retry
	.p2align 4,,15
	.globl	_dl_tunable_set_mallopt_check
	.type	_dl_tunable_set_mallopt_check, @function
_dl_tunable_set_mallopt_check:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L339
	leaq	malloc_check(%rip), %rax
	movl	$1, using_malloc_checking(%rip)
	movq	%rax, __malloc_hook(%rip)
	leaq	free_check(%rip), %rax
	movq	%rax, __free_hook(%rip)
	leaq	realloc_check(%rip), %rax
	movq	%rax, __realloc_hook(%rip)
	leaq	memalign_check(%rip), %rax
	movq	%rax, __memalign_hook(%rip)
.L339:
	rep ret
	.size	_dl_tunable_set_mallopt_check, .-_dl_tunable_set_mallopt_check
	.section	.rodata.str1.1
.LC22:
	.string	"<heap nr=\"%d\">\n<sizes>\n"
	.section	.rodata.str1.8
	.align 8
.LC23:
	.string	"__malloc_info(): unaligned fastbin chunk detected"
	.align 8
.LC24:
	.string	"  <size from=\"%zu\" to=\"%zu\" total=\"%zu\" count=\"%zu\"/>\n"
	.align 8
.LC25:
	.string	"  <unsorted from=\"%zu\" to=\"%zu\" total=\"%zu\" count=\"%zu\"/>\n"
	.align 8
.LC26:
	.string	"</sizes>\n<total type=\"fast\" count=\"%zu\" size=\"%zu\"/>\n<total type=\"rest\" count=\"%zu\" size=\"%zu\"/>\n<system type=\"current\" size=\"%zu\"/>\n<system type=\"max\" size=\"%zu\"/>\n"
	.align 8
.LC27:
	.string	"<aspace type=\"total\" size=\"%zu\"/>\n<aspace type=\"mprotect\" size=\"%zu\"/>\n<aspace type=\"subheaps\" size=\"%zu\"/>\n"
	.align 8
.LC28:
	.string	"<aspace type=\"total\" size=\"%zu\"/>\n<aspace type=\"mprotect\" size=\"%zu\"/>\n"
	.section	.rodata.str1.1
.LC29:
	.string	"</heap>\n"
	.section	.rodata.str1.8
	.align 8
.LC30:
	.ascii	"<total type=\"fast\" count"
	.string	"=\"%zu\" size=\"%zu\"/>\n<total type=\"rest\" count=\"%zu\" size=\"%zu\"/>\n<total type=\"mmap\" count=\"%d\" size=\"%zu\"/>\n<system type=\"current\" size=\"%zu\"/>\n<system type=\"max\" size=\"%zu\"/>\n<aspace type=\"total\" size=\"%zu\"/>\n<aspace type=\"mprotect\" size=\"%zu\"/>\n</malloc>\n"
	.text
	.p2align 4,,15
	.type	__malloc_info.part.11, @function
__malloc_info.part.11:
	pushq	%r15
	pushq	%r14
	leaq	main_arena(%rip), %rax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%edx, %edx
	movq	$-1, %r14
	subq	$4520, %rsp
	movq	%rax, (%rsp)
	leaq	128(%rsp), %rax
	movq	$0, 96(%rsp)
	movq	$0, 88(%rsp)
	movq	$0, 80(%rsp)
	movq	$0, 72(%rsp)
	movq	$0, 48(%rsp)
	movq	$0, 64(%rsp)
	movq	$0, 40(%rsp)
	movq	$0, 56(%rsp)
	movq	%rax, 24(%rsp)
.L362:
	leal	1(%rdx), %eax
	leaq	.LC22(%rip), %rsi
	movq	%r13, %rdi
	movl	%eax, 36(%rsp)
	xorl	%eax, %eax
	call	fprintf
#APP
# 5701 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	(%rsp), %rbx
	jne	.L342
	movl	$1, %esi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %esi, (%rbx)
# 0 "" 2
#NO_APP
.L343:
	movq	(%rsp), %rax
	movq	24(%rsp), %rdi
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	96(%rax), %r15
	leaq	16(%rax), %r8
	leaq	320(%rdi), %r10
	movq	%rdi, %rbx
	movq	8(%r15), %r11
.L348:
	movq	(%r8), %r9
	testq	%r9, %r9
	je	.L344
	movq	%r9, %rdx
	xorl	%ecx, %ecx
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L345:
	leaq	16(%rdx), %rax
	movq	16(%rdx), %rsi
	addq	$1, %rcx
	shrq	$12, %rax
	movq	%rax, %rdx
	xorq	%rsi, %rdx
	cmpq	%rsi, %rax
	je	.L387
.L346:
	testb	$15, %dl
	je	.L345
	leaq	.LC23(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L344:
	movq	$0, 24(%rdi)
	movq	$0, 8(%rdi)
	xorl	%ecx, %ecx
	movq	$0, (%rdi)
.L347:
	imulq	8(%rdi), %rcx
	addq	$32, %rdi
	addq	$8, %r8
	movq	%rcx, -16(%rdi)
	cmpq	%rdi, %r10
	jne	.L348
	movq	24(%rsp), %rax
	movq	(%rsp), %rsi
	andq	$-8, %r11
	movq	%r11, %r10
	movl	$1, %r12d
	leaq	320(%rax), %rcx
	leaq	96(%rsi), %r9
	leaq	4384(%rax), %r11
	.p2align 4,,10
	.p2align 3
.L352:
	movq	16(%r9), %rdx
	movq	%r14, (%rcx)
	movq	$0, 24(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, 8(%rcx)
	testq	%rdx, %rdx
	je	.L349
	cmpq	%r9, %rdx
	je	.L349
	xorl	%esi, %esi
	movq	$-1, %rdi
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	.p2align 4,,10
	.p2align 3
.L350:
	movq	8(%rdx), %rax
	movq	16(%rdx), %rdx
	addq	$1, %r8
	addq	%rax, %rbp
	cmpq	%rax, %rdi
	cmova	%rax, %rdi
	cmpq	%rax, %rsi
	cmovb	%rax, %rsi
	cmpq	%r9, %rdx
	jne	.L350
	testq	%r8, %r8
	movq	%r8, 24(%rcx)
	movq	%rbp, 16(%rcx)
	movq	%rdi, (%rcx)
	movq	%rsi, 8(%rcx)
	je	.L349
	addq	%r8, %r12
.L351:
	addq	16(%rcx), %r10
	addq	$32, %rcx
	addq	$16, %r9
	cmpq	%r11, %rcx
	movq	%r10, %rbp
	jne	.L352
	leaq	main_arena(%rip), %rsi
	cmpq	%rsi, (%rsp)
	je	.L363
	movq	%r15, %rax
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	andq	$-67108864, %rax
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L354:
	addq	16(%rax), %rdx
	addq	24(%rax), %rcx
	addq	$1, %rsi
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L354
	movq	%rdx, 104(%rsp)
	movq	%rcx, 112(%rsp)
	movq	%rsi, 120(%rsp)
.L353:
#APP
# 5787 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L355
	movq	(%rsp), %rax
	subl	$1, (%rax)
.L356:
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	xorl	%r15d, %r15d
	addq	%rdi, 40(%rsp)
	addq	%rsi, 48(%rsp)
	addq	%r12, 56(%rsp)
	addq	%rbp, 64(%rsp)
	.p2align 4,,10
	.p2align 3
.L358:
	cmpq	$10, %r15
	movq	24(%rbx), %r9
	je	.L357
	testq	%r9, %r9
	je	.L357
	movq	8(%rbx), %rcx
	movq	16(%rbx), %r8
	leaq	.LC24(%rip), %rsi
	movq	(%rbx), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	fprintf
.L357:
	addq	$1, %r15
	addq	$32, %rbx
	cmpq	$137, %r15
	jne	.L358
	movq	472(%rsp), %r9
	testq	%r9, %r9
	jne	.L388
.L359:
	movq	(%rsp), %rbx
	leaq	.LC26(%rip), %rsi
	movq	%rbp, %r9
	movq	%r12, %r8
	movq	%r13, %rdi
	movq	2184(%rbx), %rax
	movq	2192(%rbx), %rdx
	addq	%rax, 72(%rsp)
	addq	%rdx, 80(%rsp)
	pushq	%rdx
	pushq	%rax
	xorl	%eax, %eax
	movq	32(%rsp), %rcx
	movq	24(%rsp), %rdx
	call	fprintf
	popq	%rax
	leaq	main_arena(%rip), %rax
	popq	%rdx
	cmpq	%rax, %rbx
	je	.L360
	movq	112(%rsp), %r15
	movq	104(%rsp), %rbx
	leaq	.LC27(%rip), %rsi
	movq	120(%rsp), %r8
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	%r15, %rcx
	movq	%rbx, %rdx
	call	fprintf
	addq	%rbx, 88(%rsp)
	addq	%r15, 96(%rsp)
.L361:
	leaq	.LC29(%rip), %rdi
	movq	%r13, %rsi
	leaq	main_arena(%rip), %rbx
	call	_IO_fputs
	movq	(%rsp), %rax
	movl	36(%rsp), %edx
	movq	2160(%rax), %rax
	cmpq	%rbx, %rax
	movq	%rax, (%rsp)
	jne	.L362
	movl	40+mp_(%rip), %eax
	pushq	96(%rsp)
	leaq	.LC30(%rip), %rsi
	pushq	96(%rsp)
	pushq	96(%rsp)
	movq	%r13, %rdi
	pushq	96(%rsp)
	pushq	56+mp_(%rip)
	pushq	%rax
	movq	112(%rsp), %r9
	xorl	%eax, %eax
	movq	104(%rsp), %r8
	movq	96(%rsp), %rcx
	movq	88(%rsp), %rdx
	call	fprintf
	addq	$4568, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L387:
	movq	8(%r9), %rax
	addq	%rcx, 8(%rsp)
	movq	%rcx, 24(%rdi)
	andq	$-8, %rax
	movq	%rax, %rdx
	movq	%rax, 8(%rdi)
	imulq	%rcx, %rdx
	addq	%rdx, 16(%rsp)
	leaq	-15(%rax), %rdx
	movq	%rdx, (%rdi)
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L349:
	movq	$0, (%rcx)
	jmp	.L351
.L363:
	movq	$0, 120(%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 104(%rsp)
	jmp	.L353
.L342:
	xorl	%eax, %eax
	movl	$1, %edi
	lock cmpxchgl	%edi, (%rbx)
	je	.L343
	movq	(%rsp), %rdi
	call	__lll_lock_wait_private
	jmp	.L343
.L388:
	movq	464(%rsp), %r8
	movq	456(%rsp), %rcx
	leaq	.LC25(%rip), %rsi
	movq	448(%rsp), %rdx
	movq	%r13, %rdi
	xorl	%eax, %eax
	call	fprintf
	jmp	.L359
.L360:
	movq	(%rsp), %rbx
	leaq	.LC28(%rip), %rsi
	movq	%r13, %rdi
	xorl	%eax, %eax
	movq	2184(%rbx), %rdx
	movq	%rdx, %rcx
	call	fprintf
	movq	2184(%rbx), %rax
	addq	%rax, 88(%rsp)
	addq	%rax, 96(%rsp)
	jmp	.L361
.L355:
	xorl	%eax, %eax
	movq	(%rsp), %rdi
#APP
# 5787 "malloc.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L356
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 5787 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L356
	.size	__malloc_info.part.11, .-__malloc_info.part.11
	.p2align 4,,15
	.type	systrim.isra.1.constprop.12, @function
systrim.isra.1.constprop.12:
	movq	96+main_arena(%rip), %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	8(%rax), %rbx
	andq	$-8, %rbx
	leaq	-33(%rbx), %rbp
	cmpq	%rdi, %rbp
	jbe	.L392
	movq	_dl_pagesize(%rip), %rax
	subq	%rdi, %rbp
	negq	%rax
	andq	%rax, %rbp
	je	.L392
	xorl	%edi, %edi
	call	*__morecore(%rip)
	movq	%rax, %r12
	movq	96+main_arena(%rip), %rax
	addq	%rbx, %rax
	cmpq	%rax, %r12
	jne	.L392
	movq	%rbp, %rdi
	negq	%rdi
	call	*__morecore(%rip)
	movq	__after_morecore_hook(%rip), %rax
	testq	%rax, %rax
	jne	.L407
.L393:
	xorl	%edi, %edi
	call	*__morecore(%rip)
	testq	%rax, %rax
	je	.L392
	subq	%rax, %r12
	je	.L392
	movq	96+main_arena(%rip), %rax
	subq	%r12, %rbx
	subq	%r12, 2184+main_arena(%rip)
	orq	$1, %rbx
	movq	%rbx, 8(%rax)
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L392:
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L407:
	call	*%rax
	jmp	.L393
	.size	systrim.isra.1.constprop.12, .-systrim.isra.1.constprop.12
	.section	.rodata.str1.1
.LC31:
	.string	"free(): invalid pointer"
.LC32:
	.string	"free(): invalid size"
	.section	.rodata.str1.8
	.align 8
.LC33:
	.string	"free(): too many chunks detected in tcache"
	.align 8
.LC34:
	.string	"free(): unaligned chunk detected in tcache 2"
	.align 8
.LC35:
	.string	"free(): double free detected in tcache 2"
	.align 8
.LC36:
	.string	"free(): invalid next size (fast)"
	.align 8
.LC37:
	.string	"double free or corruption (fasttop)"
	.section	.rodata.str1.1
.LC38:
	.string	"invalid fastbin entry (free)"
	.section	.rodata.str1.8
	.align 8
.LC39:
	.string	"double free or corruption (top)"
	.align 8
.LC40:
	.string	"double free or corruption (out)"
	.align 8
.LC41:
	.string	"double free or corruption (!prev)"
	.align 8
.LC42:
	.string	"free(): invalid next size (normal)"
	.align 8
.LC43:
	.string	"corrupted size vs. prev_size while consolidating"
	.align 8
.LC44:
	.string	"free(): corrupted unsorted chunks"
	.section	.rodata.str1.1
.LC45:
	.string	"heap->ar_ptr == av"
	.section	.rodata.str1.8
	.align 8
.LC46:
	.string	"chunksize_nomask (p) == (0 | PREV_INUSE)"
	.align 8
.LC47:
	.string	"new_size > 0 && new_size < (long) (2 * MINSIZE)"
	.align 8
.LC48:
	.string	"new_size > 0 && new_size < HEAP_MAX_SIZE"
	.align 8
.LC49:
	.string	"((unsigned long) ((char *) p + new_size) & (pagesz - 1)) == 0"
	.align 8
.LC50:
	.string	"((char *) p + new_size) == ((char *) heap + heap->size)"
	.align 8
.LC51:
	.string	"/proc/sys/vm/overcommit_memory"
	.text
	.p2align 4,,15
	.type	_int_free, @function
_int_free:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r13d
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	8(%rsi), %rax
	movq	%rax, %r12
	andq	$-8, %r12
	movq	%r12, %rdx
	negq	%rdx
	cmpq	%rdx, %rsi
	ja	.L409
	testb	$15, %sil
	movq	%rsi, %rbx
	jne	.L409
	cmpq	$31, %r12
	jbe	.L411
	testb	$8, %al
	jne	.L411
	movq	%fs:tcache@tpoff, %rcx
	movq	%rdi, %rbp
	testq	%rcx, %rcx
	je	.L413
	leaq	-17(%r12), %rdx
	shrq	$4, %rdx
	cmpq	%rdx, 80+mp_(%rip)
	jbe	.L413
	cmpq	24(%rsi), %rcx
	leaq	16(%rsi), %r8
	movq	96+mp_(%rip), %rdi
	je	.L533
.L414:
	leaq	(%rcx,%rdx,2), %r9
	movzwl	(%r9), %r10d
	cmpq	%rdi, %r10
	movq	%r10, %rsi
	jb	.L534
.L413:
	cmpq	global_max_fast(%rip), %r12
	ja	.L423
	leaq	(%rbx,%r12), %r14
	movq	8(%r14), %rax
	cmpq	$16, %rax
	jbe	.L424
	andq	$-8, %rax
	cmpq	2184(%rbp), %rax
	jnb	.L424
.L425:
	movl	perturb_byte(%rip), %eax
	testl	%eax, %eax
	jne	.L535
.L433:
	shrl	$4, %r12d
	leal	-2(%r12), %eax
	movl	$1, 8(%rbp)
	leaq	0(%rbp,%rax,8), %rcx
	movq	%rax, %r12
	movq	16(%rcx), %rdx
#APP
# 4491 "malloc.c" 1
	movl %fs:24,%esi
# 0 "" 2
#NO_APP
	testl	%esi, %esi
	jne	.L434
	cmpq	%rdx, %rbx
	je	.L439
	leaq	16(%rbx), %rax
	shrq	$12, %rax
	xorq	%rdx, %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, 16(%rcx)
.L436:
	testq	%rdx, %rdx
	je	.L408
	andl	$1, %r13d
	je	.L408
	movq	8(%rdx), %rax
	shrl	$4, %eax
	subl	$2, %eax
	cmpl	%r12d, %eax
	jne	.L536
.L408:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L423:
	testb	$2, %al
	jne	.L444
#APP
# 4529 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L491
	andl	$1, %r13d
	je	.L537
.L491:
	movl	$1, 20(%rsp)
.L445:
	movq	96(%rbp), %rax
	leaq	(%rbx,%r12), %r13
	cmpq	%rbx, %rax
	je	.L538
	testb	$2, 4(%rbp)
	je	.L539
.L448:
	movq	8(%r13), %rax
	testb	$1, %al
	je	.L540
	movq	%rax, %r14
	andq	$-8, %r14
	cmpq	$16, %rax
	jbe	.L450
	cmpq	%r14, 2184(%rbp)
	jbe	.L450
	movl	perturb_byte(%rip), %esi
	testl	%esi, %esi
	jne	.L541
.L452:
	testb	$1, 8(%rbx)
	jne	.L453
	movq	(%rbx), %rax
	subq	%rax, %rbx
	addq	%rax, %r12
	movq	8(%rbx), %rdx
	andq	$-8, %rdx
	cmpq	%rax, %rdx
	jne	.L542
	movq	%rbx, %rdi
	call	unlink_chunk.isra.2
.L453:
	cmpq	%r13, 96(%rbp)
	je	.L455
	testb	$1, 8(%r13,%r14)
	je	.L543
	andq	$-2, 8(%r13)
.L457:
	movq	112(%rbp), %rax
	leaq	96(%rbp), %rcx
	movq	24(%rax), %rdx
	cmpq	%rcx, %rdx
	jne	.L544
	cmpq	$1023, %r12
	movq	%rax, 16(%rbx)
	movq	%rdx, 24(%rbx)
	ja	.L545
.L459:
	movq	%rbx, 112(%rbp)
	movq	%rbx, 24(%rax)
	movq	%r12, %rax
	orq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	%r12, (%rbx,%r12)
.L460:
	cmpq	$65535, %r12
	ja	.L546
.L462:
	movl	20(%rsp), %eax
	testl	%eax, %eax
	jne	.L408
#APP
# 4650 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L488
	subl	$1, 0(%rbp)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L534:
	leaq	(%rcx,%rdx,8), %rdx
	movq	%r8, %rax
	addl	$1, %esi
	shrq	$12, %rax
	movq	%rcx, 24(%rbx)
	xorq	128(%rdx), %rax
	movq	%rax, 16(%rbx)
	movq	%r8, 128(%rdx)
	movw	%si, (%r9)
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L444:
	movq	%rbx, %rdi
	call	munmap_chunk
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L409:
	leaq	.LC31(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L411:
	leaq	.LC32(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L424:
	testl	%r13d, %r13d
	jne	.L432
#APP
# 4472 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L427
	movl	%r13d, %eax
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%rbp)
# 0 "" 2
#NO_APP
.L428:
	movq	8(%r14), %rax
	movl	$1, %r8d
	cmpq	$16, %rax
	jbe	.L429
	xorl	%r8d, %r8d
	andq	$-8, %rax
	cmpq	2184(%rbp), %rax
	setnb	%r8b
.L429:
#APP
# 4475 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L430
	subl	$1, 0(%rbp)
.L431:
	testl	%r8d, %r8d
	je	.L425
.L432:
	leaq	.LC36(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L545:
	movq	$0, 32(%rbx)
	movq	$0, 40(%rbx)
	jmp	.L459
	.p2align 4,,10
	.p2align 3
.L536:
	leaq	.LC38(%rip), %rdi
	call	malloc_printerr
.L427:
	movl	$1, %edx
	movl	%r13d, %eax
	lock cmpxchgl	%edx, 0(%rbp)
	je	.L428
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L428
	.p2align 4,,10
	.p2align 3
.L434:
	cmpq	%rdx, %rbx
	je	.L439
	leaq	16(%rbx), %rdi
	leaq	16(%rbp,%rax,8), %rsi
	movq	%rdx, %rax
	shrq	$12, %rdi
	xorq	%rdi, %rax
	movq	%rax, 16(%rbx)
	movq	%rdx, %rax
#APP
# 4510 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rax
	movq	%rax, %rcx
	jne	.L438
	jmp	.L436
	.p2align 4,,10
	.p2align 3
.L530:
	movq	%rcx, %rax
	xorq	%rdi, %rax
	movq	%rax, 16(%rbx)
	movq	%rcx, %rax
#APP
# 4510 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rax, %rcx
	je	.L490
	movq	%rax, %rcx
.L438:
	cmpq	%rbx, %rcx
	jne	.L530
.L439:
	leaq	.LC37(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L546:
	movl	8(%rbp), %eax
	testl	%eax, %eax
	jne	.L547
.L463:
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rbp
	je	.L548
	movq	96(%rbp), %rbx
	movq	%rbx, %rdi
	andq	$-67108864, %rdi
	movq	(%rdi), %r15
	cmpq	%rbp, %r15
	jne	.L549
	movq	8+mp_(%rip), %rax
	leaq	32(%rdi), %rsi
	cmpq	%rbx, %rsi
	movq	%rax, 24(%rsp)
	movq	_dl_pagesize(%rip), %rax
	movq	%rax, 32(%rsp)
	jne	.L550
	movq	8(%rdi), %r14
	movq	16(%r14), %rcx
	leaq	-16(%rcx), %rdx
	leaq	(%r14,%rdx), %rax
	andl	$15, %eax
	subq	%rax, %rdx
	addq	%r14, %rdx
	cmpq	$1, 8(%rdx)
	jne	.L470
	movq	32(%rsp), %rbx
	movq	24(%rsp), %r8
	movl	$67108864, %r13d
	movq	%rbp, 40(%rsp)
	movq	%r14, %rbp
	movq	%r15, %r14
	leaq	32(%r8,%rbx), %r12
	subq	$1, %rbx
	movq	%rbx, 8(%rsp)
	jmp	.L469
	.p2align 4,,10
	.p2align 3
.L476:
	leaq	(%rbx,%r15), %rax
	testq	%rax, 8(%rsp)
	jne	.L551
	movq	16(%rbp), %rdx
	addq	%rbp, %rdx
	cmpq	%rdx, %rax
	jne	.L552
	leaq	32(%rbp), %rax
	orq	$1, %r15
	movq	%rbx, 96(%r14)
	movq	%r15, 8(%rbx)
	cmpq	%rax, %rbx
	jne	.L553
	movq	8(%rbp), %r10
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	16(%r10), %rcx
	leaq	-16(%rcx), %rdx
	leaq	(%r10,%rdx), %rax
	andl	$15, %eax
	subq	%rax, %rdx
	addq	%r10, %rdx
	cmpq	$1, 8(%rdx)
	jne	.L470
	movq	%r10, %rbp
.L469:
	movq	%rdx, %rbx
	subq	(%rdx), %rbx
	movq	8(%rbx), %rdx
	movq	%rdx, %r10
	andq	$-8, %r10
	addq	%r10, %rax
	leaq	16(%rax), %r15
	addq	$15, %rax
	cmpq	$62, %rax
	ja	.L554
	andl	$1, %edx
	jne	.L472
	addq	(%rbx), %r15
.L472:
	leaq	-1(%r15), %rax
	cmpq	$67108862, %rax
	ja	.L555
	movq	%r13, %rax
	subq	%rcx, %rax
	addq	%r15, %rax
	cmpq	%r12, %rax
	jb	.L556
	movq	16(%rdi), %rax
	subq	%rax, 2184(%r14)
	leaq	67108864(%rdi), %rax
	cmpq	%rax, aligned_heap_area(%rip)
	je	.L557
.L475:
	movl	$67108864, %esi
	call	__munmap
	testb	$1, 8(%rbx)
	jne	.L476
	subq	(%rbx), %rbx
	movq	%rbx, %rdi
	call	unlink_chunk.isra.2
	jmp	.L476
	.p2align 4,,10
	.p2align 3
.L557:
	movq	$0, aligned_heap_area(%rip)
	jmp	.L475
	.p2align 4,,10
	.p2align 3
.L455:
	addq	%r14, %r12
	movq	%r12, %rax
	orq	$1, %rax
	movq	%rax, 8(%rbx)
	movq	%rbx, 96(%rbp)
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L450:
	leaq	.LC42(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L535:
	leaq	24(%rbx), %rdi
	leaq	16(%rbx), %rcx
	leaq	-16(%r12), %rdx
	movabsq	$72340172838076673, %rsi
	movzbl	%al, %eax
	andq	$-8, %rdi
	imulq	%rsi, %rax
	subq	%rdi, %rcx
	movl	%edx, %esi
	addl	%edx, %ecx
	shrl	$3, %ecx
	movl	%ecx, %ecx
	movq	%rax, 16(%rbx)
	movq	%rax, 8(%rbx,%rsi)
	rep stosq
	jmp	.L433
	.p2align 4,,10
	.p2align 3
.L543:
	movq	%r13, %rdi
	addq	%r14, %r12
	call	unlink_chunk.isra.2
	jmp	.L457
	.p2align 4,,10
	.p2align 3
.L533:
	movq	128(%rcx,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L414
	testq	%rdi, %rdi
	je	.L415
	testb	$15, %sil
	jne	.L416
	cmpq	%rsi, %r8
	je	.L417
	xorl	%r9d, %r9d
	jmp	.L418
	.p2align 4,,10
	.p2align 3
.L420:
	cmpq	%rsi, %r8
	je	.L417
.L418:
	movq	(%rsi), %r10
	movq	%rsi, %r11
	addq	$1, %r9
	shrq	$12, %r11
	movq	%r11, %rsi
	xorq	%r10, %rsi
	cmpq	%r10, %r11
	je	.L414
	cmpq	%rdi, %r9
	je	.L415
	testb	$15, %sil
	je	.L420
.L416:
	leaq	.LC34(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L538:
	leaq	.LC39(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L539:
	movq	8(%rax), %rdx
	andq	$-8, %rdx
	addq	%rdx, %rax
	cmpq	%rax, %r13
	jb	.L448
	leaq	.LC40(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L540:
	leaq	.LC41(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L541:
	leaq	-16(%r12), %rdx
	leaq	16(%rbx), %rdi
	call	memset
	jmp	.L452
	.p2align 4,,10
	.p2align 3
.L556:
	movq	40(%rsp), %rbp
	movq	8(%rsi), %rdx
	movq	%r14, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
.L468:
	andq	$-8, %rdx
	cmpq	%rdx, mp_(%rip)
	movq	%rdx, %r13
	ja	.L462
	movq	%rdx, %r12
	subq	$33, %r12
	js	.L462
	movq	24(%rsp), %rax
	cmpq	%r12, %rax
	jnb	.L462
	subq	%rax, %r12
	movq	32(%rsp), %rax
	negq	%rax
	andq	%rax, %r12
	je	.L462
	movq	16(%r14), %rax
	subq	%r12, %rax
	cmpq	$31, %rax
	movq	%rax, 8(%rsp)
	jle	.L462
	movl	may_shrink_heap.11292(%rip), %edx
	testl	%edx, %edx
	js	.L480
	setne	%al
.L481:
	movq	8(%rsp), %rcx
	testb	%al, %al
	leaq	(%r14,%rcx), %rdi
	jne	.L558
	movl	$4, %edx
	movq	%r12, %rsi
	call	__madvise
.L487:
	movq	8(%rsp), %rax
	subq	%r12, %r13
	orq	$1, %r13
	movq	%rax, 16(%r14)
	subq	%r12, 2184(%r15)
	movq	%r13, 8(%rbx)
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L547:
	movq	%rbp, %rdi
	call	malloc_consolidate
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L553:
	movq	%r15, %rdx
	movq	%r14, %r15
	movq	%rbp, %r14
	movq	40(%rsp), %rbp
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L548:
	movq	96(%rbp), %rax
	movq	8(%rax), %rax
	andq	$-8, %rax
	cmpq	mp_(%rip), %rax
	jb	.L462
	movq	8+mp_(%rip), %rdi
	call	systrim.isra.1.constprop.12
	jmp	.L462
.L490:
	movq	%rcx, %rdx
	jmp	.L436
.L537:
#APP
# 4533 "malloc.c" 1
	movl %fs:24,%r15d
# 0 "" 2
#NO_APP
	testl	%r15d, %r15d
	movl	%r15d, %eax
	movl	%r15d, 20(%rsp)
	jne	.L446
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%rbp)
# 0 "" 2
#NO_APP
	jmp	.L445
.L544:
	leaq	.LC44(%rip), %rdi
	call	malloc_printerr
.L542:
	leaq	.LC43(%rip), %rdi
	call	malloc_printerr
.L488:
	movl	20(%rsp), %r15d
#APP
# 4650 "malloc.c" 1
	xchgl %r15d, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %r15d
	jle	.L408
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 4650 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L408
.L430:
	xorl	%eax, %eax
#APP
# 4475 "malloc.c" 1
	xchgl %eax, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L431
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 4475 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L431
.L446:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	movl	$0, 20(%rsp)
	je	.L445
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L445
.L550:
	movq	8(%rbx), %rdx
	movq	%rdi, %r14
	jmp	.L468
.L555:
	leaq	__PRETTY_FUNCTION__.11956(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC48(%rip), %rdi
	movl	$669, %edx
	call	__malloc_assert
.L554:
	leaq	__PRETTY_FUNCTION__.11956(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC47(%rip), %rdi
	movl	$666, %edx
	call	__malloc_assert
.L470:
	leaq	__PRETTY_FUNCTION__.11956(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC46(%rip), %rdi
	movl	$663, %edx
	call	__malloc_assert
.L558:
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edx, %edx
	movl	$50, %ecx
	movq	%r12, %rsi
	call	__mmap
	addq	$1, %rax
	je	.L462
	movq	8(%rsp), %rax
	movq	%rax, 24(%r14)
	jmp	.L487
.L480:
	movl	__libc_enable_secure(%rip), %ecx
	testl	%ecx, %ecx
	movl	%ecx, may_shrink_heap.11292(%rip)
	movl	%ecx, 32(%rsp)
	jne	.L483
	leaq	.LC51(%rip), %rdi
	xorl	%eax, %eax
	movl	$524288, %esi
	call	__open_nocancel
	testl	%eax, %eax
	jns	.L559
.L483:
	cmpl	$0, may_shrink_heap.11292(%rip)
	setne	%al
	jmp	.L481
.L552:
	leaq	__PRETTY_FUNCTION__.11956(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC50(%rip), %rdi
	movl	$682, %edx
	call	__malloc_assert
.L551:
	leaq	__PRETTY_FUNCTION__.11956(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC49(%rip), %rdi
	movl	$681, %edx
	call	__malloc_assert
.L417:
	leaq	.LC35(%rip), %rdi
	call	malloc_printerr
.L559:
	leaq	63(%rsp), %rsi
	movl	$1, %edx
	movl	%eax, %edi
	movl	%eax, 24(%rsp)
	call	__read_nocancel
	testq	%rax, %rax
	movl	24(%rsp), %r8d
	movl	32(%rsp), %ecx
	jle	.L485
	xorl	%ecx, %ecx
	cmpb	$50, 63(%rsp)
	sete	%cl
.L485:
	movl	%r8d, %edi
	movl	%ecx, may_shrink_heap.11292(%rip)
	call	__close_nocancel
	jmp	.L483
.L415:
	leaq	.LC33(%rip), %rdi
	call	malloc_printerr
.L549:
	leaq	__PRETTY_FUNCTION__.13291(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC45(%rip), %rdi
	movl	$4644, %edx
	call	__malloc_assert
	.size	_int_free, .-_int_free
	.section	.rodata.str1.8
	.align 8
.LC52:
	.string	"((INTERNAL_SIZE_T) chunk2rawmem (mm) & MALLOC_ALIGN_MASK) == 0"
	.align 8
.LC53:
	.string	"(old_top == initial_top (av) && old_size == 0) || ((unsigned long) (old_size) >= MINSIZE && prev_inuse (old_top) && ((unsigned long) old_end & (pagesize - 1)) == 0)"
	.align 8
.LC54:
	.string	"(unsigned long) (old_size) < (unsigned long) (nb + MINSIZE)"
	.align 8
.LC55:
	.string	"break adjusted to free malloc space"
	.section	.rodata.str1.1
.LC56:
	.string	"correction >= 0"
	.section	.rodata.str1.8
	.align 8
.LC57:
	.string	"((unsigned long) chunk2rawmem (brk) & MALLOC_ALIGN_MASK) == 0"
	.text
	.p2align 4,,15
	.type	sysmalloc, @function
sysmalloc:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$56, %rsp
	testq	%rsi, %rsi
	movq	_dl_pagesize(%rip), %r15
	je	.L561
	cmpq	%rdi, 16+mp_(%rip)
	movq	%rsi, %rbp
	jbe	.L660
.L609:
	movb	$0, (%rsp)
.L562:
	leaq	7(%r15,%rbx), %rax
	movq	%r15, %rdx
	leaq	96(%rbp), %r10
	negq	%rdx
	movq	%r15, %r14
	andq	%rdx, %rax
	movq	%r10, %r15
	movq	%rax, 16(%rsp)
.L608:
	movq	96(%rbp), %r13
	movq	8(%r13), %rax
	movq	%rax, %r12
	andq	$-8, %r12
	cmpq	%r15, %r13
	leaq	0(%r13,%r12), %r11
	jne	.L616
	testq	%r12, %r12
	je	.L571
.L616:
	cmpq	$31, %r12
	jbe	.L573
	testb	$1, %al
	je	.L573
	leaq	-1(%r14), %rax
	testq	%rax, %r11
	jne	.L573
.L571:
	leaq	32(%rbx), %r10
	cmpq	%r12, %r10
	jbe	.L661
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rbp
	je	.L575
	movq	%r10, %rdx
	movq	%r13, %r8
	subq	%r12, %rdx
	andq	$-67108864, %r8
	testq	%rdx, %rdx
	jle	.L576
	movq	_dl_pagesize(%rip), %rax
	movq	16(%r8), %r9
	leaq	-1(%rdx,%rax), %rcx
	negq	%rax
	andq	%rcx, %rax
	leaq	(%rax,%r9), %rcx
	cmpq	$67108864, %rcx
	ja	.L576
	movq	24(%r8), %rdi
	cmpq	%rdi, %rcx
	ja	.L662
	movq	%r10, %r14
	movq	%r13, %rsi
.L577:
	movq	2184(%rbp), %rdx
	movq	%rcx, 16(%r8)
	subq	%r9, %rdx
	addq	%rcx, %rdx
	addq	%r8, %rcx
	subq	%r13, %rcx
	movq	%rdx, 2184(%rbp)
	orq	$1, %rcx
	movq	%rcx, 8(%r13)
.L578:
	cmpq	%rdx, 2192(%rbp)
	jnb	.L602
	movq	%rdx, 2192(%rbp)
.L602:
	movq	8(%rsi), %rdx
	andq	$-8, %rdx
	cmpq	%rdx, %r14
	ja	.L603
	leaq	main_arena(%rip), %rax
	subq	%rbx, %rdx
	leaq	(%rsi,%rbx), %rcx
	cmpq	%rax, %rbp
	movq	%rcx, 96(%rbp)
	setne	%al
	orq	$1, %rbx
	orq	$1, %rdx
	movzbl	%al, %eax
	addq	$16, %rsi
	salq	$2, %rax
	orq	%rax, %rbx
	movq	%rbx, -8(%rsi)
	movq	%rdx, 8(%rcx)
.L560:
	addq	$56, %rsp
	movq	%rsi, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L660:
	movl	44+mp_(%rip), %eax
	cmpl	%eax, 40+mp_(%rip)
	jge	.L609
	leaq	7(%r15,%rdi), %r12
	movq	%r15, %rax
	negq	%rax
	andq	%rax, %r12
	cmpq	%r12, %rdi
	jb	.L663
.L615:
	movb	$1, (%rsp)
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L576:
	movq	8+mp_(%rip), %rsi
	leaq	64(%rbx), %rdi
	movq	%r10, 8(%rsp)
	movq	%r8, 24(%rsp)
	call	new_heap
	testq	%rax, %rax
	movq	8(%rsp), %r10
	je	.L579
	movq	16(%rax), %rcx
	movq	2184(%rbp), %rdx
	leaq	32(%rax), %rsi
	movq	24(%rsp), %r8
	subq	$32, %r12
	movq	%rbp, (%rax)
	andq	$-16, %r12
	movq	%r10, %r14
	addq	%rcx, %rdx
	subq	$32, %rcx
	orq	$1, %rcx
	movq	%r8, 8(%rax)
	movq	%rdx, 2184(%rbp)
	movq	%rsi, 96(%rbp)
	movq	%rcx, 40(%rax)
	leaq	16(%r12), %rax
	cmpq	$31, %r12
	leaq	0(%r13,%rax), %rcx
	movq	$1, 8(%rcx)
	ja	.L664
	movq	%rax, %rdi
	orq	$1, %rdi
	movq	%rdi, 8(%r13)
	movq	%rax, (%rcx)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L561:
	leaq	7(%r15,%rdi), %r12
	negq	%r15
	andq	%r15, %r12
	cmpq	%rdi, %r12
	jbe	.L659
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$34, %ecx
	movl	$3, %edx
	movq	%r12, %rsi
	call	__mmap
	cmpq	$-1, %rax
	je	.L659
.L605:
	leaq	16(%rax), %rsi
	testb	$15, %sil
	jne	.L665
	movq	%r12, %rdx
	movq	$0, (%rax)
	orq	$2, %rdx
	movq	%rdx, 8(%rax)
	movl	$1, %edx
	lock xaddl	%edx, 40+mp_(%rip)
	addl	$1, %edx
.L568:
	movl	48+mp_(%rip), %eax
	cmpl	%eax, %edx
	jle	.L567
	lock cmpxchgl	%edx, 48+mp_(%rip)
	jne	.L568
.L567:
	movq	%r12, %r15
	lock xaddq	%r15, 56+mp_(%rip)
	addq	%r15, %r12
.L570:
	movq	64+mp_(%rip), %rax
	cmpq	%rax, %r12
	jbe	.L560
	lock cmpxchgq	%r12, 64+mp_(%rip)
	je	.L560
	jmp	.L570
	.p2align 4,,10
	.p2align 3
.L575:
	testb	$2, 4(%rbp)
	movq	%r14, %r15
	movq	8+mp_(%rip), %rax
	leaq	-1(%r15), %rdi
	movq	%r10, %r14
	leaq	32(%rbx,%rax), %rdx
	movq	%rdi, 16(%rsp)
	movq	%r15, %rax
	je	.L666
	leaq	(%rdx,%rdi), %r15
	negq	%rax
	movq	%rax, 24(%rsp)
	andq	%rax, %r15
	testq	%r15, %r15
	movq	%r15, 8(%rsp)
	jle	.L584
.L607:
	movq	%r11, (%rsp)
	movq	%r15, %rdi
	call	*__morecore(%rip)
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	(%rsp), %r11
	je	.L667
	movq	__after_morecore_hook(%rip), %rsi
	xorl	%eax, %eax
	movl	$1, %edi
	testq	%rsi, %rsi
	jne	.L668
.L585:
	cmpq	$0, 72+mp_(%rip)
	je	.L669
.L589:
	movq	2184+main_arena(%rip), %rdx
	addq	%r15, %rdx
	cmpq	%rcx, %r11
	movq	%rdx, 2184+main_arena(%rip)
	jne	.L590
	testb	%dil, %dil
	je	.L590
	addq	%r12, %r15
	movq	96+main_arena(%rip), %rsi
	orq	$1, %r15
	movq	%r15, 8(%r13)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L666:
	subq	%r12, %rdx
	negq	%rax
	leaq	(%rdx,%rdi), %r15
	movq	%rax, 24(%rsp)
	andq	%rax, %r15
	testq	%r15, %r15
	movq	%r15, 8(%rsp)
	jg	.L607
	.p2align 4,,10
	.p2align 3
.L582:
	movq	16(%rsp), %rax
	addq	%r12, %rax
	addq	%rax, %r15
	andq	24(%rsp), %r15
	movq	%r15, 8(%rsp)
.L584:
	movq	8(%rsp), %rdi
	movl	$1048576, %eax
	cmpq	$1048576, %rdi
	cmovbe	%rax, %rdi
	cmpq	%rdi, %rbx
	movq	%rdi, %r15
	jb	.L586
.L657:
	movq	2184+main_arena(%rip), %rdx
.L658:
	movq	96+main_arena(%rip), %rsi
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L664:
	movq	$17, 8(%r13,%r12)
	orq	$5, %r12
	movq	$16, (%rcx)
	movl	$1, %edx
	movq	%r13, %rsi
	movq	%r12, 8(%r13)
	movq	%rbp, %rdi
	call	_int_free
	movq	2184(%rbp), %rdx
	movq	96(%rbp), %rsi
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L662:
	movq	%rcx, %rsi
	movl	$3, %edx
	movq	%r10, 24(%rsp)
	subq	%rdi, %rsi
	addq	%r8, %rdi
	movq	%r8, 8(%rsp)
	movq	%r9, 40(%rsp)
	movq	%rcx, 32(%rsp)
	call	__mprotect
	testl	%eax, %eax
	movq	8(%rsp), %r8
	movq	24(%rsp), %r10
	jne	.L576
	movq	32(%rsp), %rcx
	movq	%r10, %r14
	movq	40(%rsp), %r9
	movq	96(%rbp), %rsi
	movq	%rcx, 24(%r8)
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L579:
	cmpb	$0, (%rsp)
	jne	.L670
.L564:
	movq	16(%rsp), %rax
	cmpq	%rbx, %rax
	movq	%rax, %r12
	ja	.L671
.L565:
	movb	$1, (%rsp)
	jmp	.L608
	.p2align 4,,10
	.p2align 3
.L603:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L659:
	xorl	%esi, %esi
	jmp	.L560
	.p2align 4,,10
	.p2align 3
.L590:
	testb	$2, 4+main_arena(%rip)
	jne	.L591
	testq	%r12, %r12
	je	.L592
	cmpq	%rcx, %r11
	ja	.L672
.L592:
	testq	%r12, %r12
	je	.L593
	movq	%rcx, %rax
	subq	%r11, %rax
	addq	%rax, %rdx
	movq	%rdx, 2184+main_arena(%rip)
.L593:
	movq	%rcx, %rax
	andl	$15, %eax
	je	.L612
	movl	$16, %edx
	subq	%rax, %rdx
	leaq	(%rcx,%rdx), %rsi
	addq	%r12, %rdx
.L594:
	addq	%rdx, %r15
	leaq	(%rcx,%r15), %rax
	movq	16(%rsp), %r15
	subq	%rax, %rdx
	addq	%rax, %r15
	andq	24(%rsp), %r15
	addq	%rdx, %r15
	testq	%r15, %r15
	js	.L673
	movq	%rsi, 8(%rsp)
	movq	%r15, (%rsp)
	movq	%r15, %rdi
	call	*__morecore(%rip)
	testq	%rax, %rax
	movq	(%rsp), %rcx
	movq	8(%rsp), %rsi
	jne	.L596
	movq	%rsi, (%rsp)
	xorl	%edi, %edi
	call	*__morecore(%rip)
	movq	(%rsp), %rsi
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L667:
	testb	$2, 4+main_arena(%rip)
	je	.L582
	jmp	.L584
	.p2align 4,,10
	.p2align 3
.L670:
	movq	%r10, %r14
	movq	2184(%rbp), %rdx
	movq	96(%rbp), %rsi
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L669:
	movq	%rcx, 72+mp_(%rip)
	jmp	.L589
	.p2align 4,,10
	.p2align 3
.L591:
	testb	$15, %cl
	jne	.L674
	testq	%rax, %rax
	jne	.L613
	movq	%rcx, (%rsp)
	xorl	%edi, %edi
	call	*__morecore(%rip)
	movq	(%rsp), %rcx
	movq	%rcx, %rsi
.L597:
	testq	%rax, %rax
	movq	2184+main_arena(%rip), %rdx
	je	.L658
.L656:
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.L599:
	subq	%rsi, %rax
	addq	%r15, %rdx
	movq	%rsi, 96+main_arena(%rip)
	addq	%rcx, %rax
	orq	$1, %rax
	testq	%r12, %r12
	movq	%rax, 8(%rsi)
	movq	%rdx, 2184+main_arena(%rip)
	je	.L578
	subq	$32, %r12
	andq	$-16, %r12
	movq	%r12, %rax
	orq	$1, %rax
	cmpq	$31, %r12
	movq	%rax, 8(%r13)
	movq	$17, 8(%r13,%r12)
	movq	$17, 24(%r13,%r12)
	jbe	.L578
	leaq	main_arena(%rip), %rdi
	movl	$1, %edx
	movq	%r13, %rsi
	call	_int_free
	jmp	.L657
	.p2align 4,,10
	.p2align 3
.L586:
	xorl	%r9d, %r9d
	movl	$34, %ecx
	movq	%rdi, %rsi
	movl	$-1, %r8d
	xorl	%edi, %edi
	movl	$3, %edx
	movq	%r11, (%rsp)
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %rcx
	movq	(%rsp), %r11
	je	.L657
	orl	$2, 4+main_arena(%rip)
	testq	%rax, %rax
	je	.L657
	addq	%r15, %rax
	sete	%dil
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L612:
	movq	%r12, %rdx
	movq	%rcx, %rsi
	jmp	.L594
	.p2align 4,,10
	.p2align 3
.L668:
	movb	%dil, 40(%rsp)
	movq	%rcx, 32(%rsp)
	movq	%r11, 8(%rsp)
	movq	%rax, (%rsp)
	call	*%rsi
	movq	(%rsp), %rax
	movq	8(%rsp), %r11
	movq	32(%rsp), %rcx
	movzbl	40(%rsp), %edi
	jmp	.L585
	.p2align 4,,10
	.p2align 3
.L596:
	movq	__after_morecore_hook(%rip), %rdx
	testq	%rdx, %rdx
	jne	.L598
	movq	2184+main_arena(%rip), %rdx
	jmp	.L599
	.p2align 4,,10
	.p2align 3
.L613:
	movq	%rcx, %rsi
	jmp	.L656
.L598:
	movq	%rsi, 16(%rsp)
	movq	%rcx, 8(%rsp)
	movq	%rax, (%rsp)
	call	*%rdx
	movq	2184+main_arena(%rip), %rdx
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
	movq	16(%rsp), %rsi
	jmp	.L599
.L573:
	leaq	__PRETTY_FUNCTION__.12690(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC53(%rip), %rdi
	movl	$2542, %edx
	call	__malloc_assert
.L661:
	leaq	__PRETTY_FUNCTION__.12690(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC54(%rip), %rdi
	movl	$2545, %edx
	call	__malloc_assert
.L665:
	leaq	__PRETTY_FUNCTION__.12690(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC52(%rip), %rdi
	movl	$2487, %edx
	call	__malloc_assert
.L671:
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	movq	%rax, %rsi
	call	__mmap
	cmpq	$-1, %rax
	jne	.L605
	jmp	.L565
.L674:
	leaq	__PRETTY_FUNCTION__.12690(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC57(%rip), %rdi
	movl	$2796, %edx
	call	__malloc_assert
.L663:
	xorl	%r9d, %r9d
	orl	$-1, %r8d
	xorl	%edi, %edi
	movl	$34, %ecx
	movl	$3, %edx
	movq	%r12, %rsi
	call	__mmap
	cmpq	$-1, %rax
	jne	.L605
	jmp	.L615
.L672:
	leaq	.LC55(%rip), %rdi
	call	malloc_printerr
.L673:
	leaq	__PRETTY_FUNCTION__.12690(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC56(%rip), %rdi
	movl	$2764, %edx
	call	__malloc_assert
	.size	sysmalloc, .-sysmalloc
	.section	.rodata.str1.8
	.align 8
.LC58:
	.string	"malloc(): unaligned fastbin chunk detected 2"
	.align 8
.LC59:
	.string	"malloc(): unaligned fastbin chunk detected"
	.align 8
.LC60:
	.string	"malloc(): memory corruption (fast)"
	.align 8
.LC61:
	.string	"malloc(): unaligned fastbin chunk detected 3"
	.align 8
.LC62:
	.string	"malloc(): smallbin double linked list corrupted"
	.align 8
.LC63:
	.string	"malloc(): invalid size (unsorted)"
	.align 8
.LC64:
	.string	"malloc(): invalid next size (unsorted)"
	.align 8
.LC65:
	.string	"malloc(): mismatching next->prev_size (unsorted)"
	.align 8
.LC66:
	.string	"malloc(): unsorted double linked list corrupted"
	.align 8
.LC67:
	.string	"malloc(): invalid next->prev_inuse (unsorted)"
	.section	.rodata.str1.1
.LC68:
	.string	"chunk_main_arena (bck->bk)"
.LC69:
	.string	"chunk_main_arena (fwd)"
	.section	.rodata.str1.8
	.align 8
.LC70:
	.string	"malloc(): largebin double linked list corrupted (nextsize)"
	.align 8
.LC71:
	.string	"malloc(): largebin double linked list corrupted (bk)"
	.align 8
.LC72:
	.string	"malloc(): unaligned tcache chunk detected"
	.align 8
.LC73:
	.string	"malloc(): corrupted unsorted chunks"
	.section	.rodata.str1.1
.LC74:
	.string	"bit != 0"
	.section	.rodata.str1.8
	.align 8
.LC75:
	.string	"(unsigned long) (size) >= (unsigned long) (nb)"
	.align 8
.LC76:
	.string	"malloc(): corrupted unsorted chunks 2"
	.section	.rodata.str1.1
.LC77:
	.string	"malloc(): corrupted top size"
	.text
	.p2align 4,,15
	.type	_int_malloc, @function
_int_malloc:
	testq	%rsi, %rsi
	js	.L900
	pushq	%r15
	leaq	23(%rsi), %rax
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	subq	$152, %rsp
	cmpq	$31, %rax
	jbe	.L901
	andq	$-16, %rax
	testq	%rdi, %rdi
	movq	%rax, %r14
	je	.L679
	movl	%eax, %ebx
	shrl	$4, %ebx
	cmpq	global_max_fast(%rip), %rax
	ja	.L684
	leal	-2(%rbx), %eax
	leaq	(%r12,%rax,8), %r8
	movq	%rax, %rdi
	leaq	16(,%rax,8), %rsi
	movq	16(%r8), %rcx
	testq	%rcx, %rcx
	je	.L689
.L685:
	testb	$15, %cl
	jne	.L902
	addq	%r12, %rsi
#APP
# 3807 "malloc.c" 1
	movl %fs:24,%r9d
# 0 "" 2
#NO_APP
	leaq	16(%rcx), %rdx
	movq	16(%rcx), %r10
	movq	%rdx, %rax
	shrq	$12, %rax
	xorq	%r10, %rax
	testl	%r9d, %r9d
	jne	.L687
	movq	%rax, 16(%r8)
.L688:
	movl	8(%rcx), %eax
	shrl	$4, %eax
	subl	$2, %eax
	cmpl	%edi, %eax
	jne	.L903
	movq	%fs:tcache@tpoff, %r15
	testq	%r15, %r15
	je	.L710
	leaq	-17(%r14), %rax
	shrq	$4, %rax
	cmpq	%rax, 80+mp_(%rip)
	jbe	.L710
	movzwl	(%r15,%rax,2), %edx
	cmpq	%rdx, 96+mp_(%rip)
	jbe	.L710
	movq	16(%r8), %rdx
	testq	%rdx, %rdx
	je	.L710
	testb	$15, %dl
	jne	.L695
	leaq	(%r15,%rax,8), %r10
	leaq	(%r15,%rax,2), %r9
	jmp	.L696
	.p2align 4,,10
	.p2align 3
.L904:
	movq	%r12, 16(%r8)
.L698:
	xorq	128(%r10), %rax
	movq	%r15, 24(%rdx)
	movq	%rax, 16(%rdx)
	movq	%rbx, 128(%r10)
	movzwl	(%r9), %eax
	addl	$1, %eax
	movw	%ax, (%r9)
	movzwl	%ax, %eax
	cmpq	96+mp_(%rip), %rax
	jnb	.L710
	movq	16(%r8), %rdx
	testq	%rdx, %rdx
	je	.L710
	testb	$15, %dl
	jne	.L695
.L696:
#APP
# 3831 "malloc.c" 1
	movl %fs:24,%ebp
# 0 "" 2
#NO_APP
	leaq	16(%rdx), %rbx
	movq	16(%rdx), %r11
	movq	%rbx, %rax
	movq	%rbx, %rdi
	shrq	$12, %rax
	movq	%r11, %r12
	xorq	%rax, %r12
	testl	%ebp, %ebp
	je	.L904
	jmp	.L697
	.p2align 4,,10
	.p2align 3
.L901:
	testq	%rdi, %rdi
	je	.L789
	cmpq	$31, global_max_fast(%rip)
	ja	.L790
	movl	$2, %ebx
	movl	$128, %eax
	movl	$32, %r14d
.L681:
	addq	%r12, %rax
	movq	8(%rax), %rcx
	leaq	-16(%rax), %r8
	cmpq	%rcx, %r8
	je	.L905
	movq	24(%rcx), %rdx
	cmpq	%rcx, 16(%rdx)
	jne	.L906
	leaq	main_arena(%rip), %r10
	orq	$1, 8(%rcx,%r14)
	movq	%rdx, 8(%rax)
	movq	%r8, 16(%rdx)
	cmpq	%r10, %r12
	je	.L707
	orq	$4, 8(%rcx)
.L707:
	movq	%fs:tcache@tpoff, %r9
	testq	%r9, %r9
	je	.L710
	leaq	-17(%r14), %rdx
	shrq	$4, %rdx
	cmpq	%rdx, 80+mp_(%rip)
	jbe	.L710
	leaq	(%r9,%rdx,2), %rbx
	movq	96+mp_(%rip), %rdi
	leaq	(%r9,%rdx,8), %rbp
	movzwl	(%rbx), %r11d
.L711:
	movzwl	%r11w, %esi
	jmp	.L712
	.p2align 4,,10
	.p2align 3
.L908:
	movq	8(%rax), %rdx
	cmpq	%rdx, %r8
	je	.L710
	testq	%rdx, %rdx
	jne	.L907
.L712:
	cmpq	%rdi, %rsi
	jb	.L908
	.p2align 4,,10
	.p2align 3
.L710:
	movl	perturb_byte(%rip), %esi
	addq	$16, %rcx
	testl	%esi, %esi
	jne	.L892
.L675:
	addq	$152, %rsp
	movq	%rcx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L909:
	testb	$15, %r9b
	jne	.L700
	.p2align 4,,10
	.p2align 3
.L690:
	movq	%rcx, %rax
#APP
# 3810 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %r9, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rcx, %rax
	je	.L688
	testq	%rax, %rax
	je	.L689
	movq	16(%rax), %r10
	leaq	16(%rax), %rdx
	movq	%rax, %rcx
.L687:
	shrq	$12, %rdx
	movq	%rdx, %r9
	xorq	%r10, %r9
	cmpq	%r10, %rdx
	je	.L690
	jmp	.L909
	.p2align 4,,10
	.p2align 3
.L684:
	cmpq	$1023, %rax
	jbe	.L689
	movq	%rax, %rcx
	movq	%rax, %rdi
	movq	%r14, %rax
	shrq	$18, %rax
	movq	%r14, %rsi
	movq	%r14, %r15
	shrq	$12, %rsi
	shrq	$6, %rcx
	shrq	$9, %rdi
	shrq	$15, %r15
	movl	$2, %edx
	cmpq	$2, %rax
	cmova	%rdx, %rax
	leal	48(%rcx), %r9d
	leal	110(%rsi), %r10d
	leal	124(%rax), %r8d
	movq	%rsi, 104(%rsp)
	leal	119(%r15), %r11d
	movq	%rsi, %rax
	movl	%edi, %esi
	movq	%rcx, 80(%rsp)
	addl	$91, %esi
	cmpq	$48, %rcx
	movq	%rdi, 96(%rsp)
	movl	%r9d, 92(%rsp)
	movq	%r15, 112(%rsp)
	movl	%r8d, 124(%rsp)
	movl	%r11d, 128(%rsp)
	movl	%r10d, 132(%rsp)
	movl	%esi, 120(%rsp)
	movl	%r9d, 8(%rsp)
	jbe	.L715
	cmpq	$20, %rdi
	movl	%esi, 8(%rsp)
	jbe	.L715
	cmpq	$10, %rax
	movl	%r10d, 8(%rsp)
	ja	.L910
	.p2align 4,,10
	.p2align 3
.L715:
	movl	8(%r12), %eax
	testl	%eax, %eax
	jne	.L911
.L705:
	movq	%fs:tcache@tpoff, %r15
	leaq	-17(%r14), %rax
	shrq	$4, %rax
	testq	%r15, %r15
	movq	%rax, %r11
	je	.L795
	cmpq	%rax, 80+mp_(%rip)
	movl	$0, %eax
	cmova	%r14, %rax
	movq	%rax, 40(%rsp)
.L716:
	leaq	96(%r12), %rbp
	xorl	%r10d, %r10d
	movl	%ebx, 88(%rsp)
	movq	%r13, 56(%rsp)
.L717:
	movq	104+mp_(%rip), %rax
	movq	120(%r12), %rdx
	xorl	%r9d, %r9d
	xorl	%r13d, %r13d
	movq	%r11, 48(%rsp)
	movq	%rax, 32(%rsp)
	movq	96+mp_(%rip), %rax
	movq	%rax, 24(%rsp)
	leaq	32(%r14), %rax
	movq	%rax, 72(%rsp)
	leaq	(%r15,%r11,2), %rax
	movq	%rax, 16(%rsp)
	leaq	(%r15,%r11,8), %rax
	movq	%rax, 64(%rsp)
.L718:
	cmpq	%rdx, %rbp
	je	.L912
	movq	8(%rdx), %rax
	andq	$-8, %rax
	cmpq	$16, %rax
	leaq	(%rdx,%rax), %rdi
	jbe	.L719
	movq	2184(%r12), %rsi
	cmpq	%rax, %rsi
	jb	.L719
	movq	8(%rdi), %rcx
	cmpq	$15, %rcx
	jbe	.L807
	cmpq	%rcx, %rsi
	jb	.L807
	movq	(%rdi), %rsi
	andq	$-8, %rsi
	cmpq	%rax, %rsi
	jne	.L913
	movq	24(%rdx), %r8
	cmpq	%rdx, 16(%r8)
	jne	.L724
	movq	16(%rdx), %rbx
	cmpq	%rbp, %rbx
	jne	.L724
	testb	$1, %cl
	jne	.L914
	cmpq	$1023, %r14
	ja	.L727
	cmpq	%rbp, %r8
	je	.L915
.L727:
	cmpq	%rax, %r14
	movq	%r8, 120(%r12)
	movq	%rbp, 16(%r8)
	je	.L916
	cmpq	$1023, %rax
	ja	.L734
	movl	%eax, %ecx
	shrl	$4, %ecx
	leal	-2(%rcx,%rcx), %eax
	cltq
	leaq	112(%r12,%rax,8), %rax
	movq	(%rax), %rdi
	leaq	-16(%rax), %r8
.L735:
	movl	%ecx, %eax
	movl	$1, %ebx
	addq	$1, %r10
	sarl	$5, %eax
	sall	%cl, %ebx
	cltq
	orl	%ebx, 2144(%r12,%rax,4)
	testl	%r13d, %r13d
	movq	%r8, 24(%rdx)
	movq	%rdi, 16(%rdx)
	movq	%rdx, 24(%rdi)
	movq	%rdx, 16(%r8)
	je	.L749
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.L750
	cmpq	%r10, %rax
	jnb	.L750
.L899:
	movq	48(%rsp), %r11
.L897:
	leaq	(%r15,%r11,8), %rdx
	movq	128(%rdx), %rcx
	testb	$15, %cl
	jne	.L917
	movq	%rcx, %rax
	shrq	$12, %rax
	xorq	(%rcx), %rax
	movq	%rax, 128(%rdx)
	subw	$1, (%r15,%r11,2)
	movq	$0, 8(%rcx)
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L790:
	xorl	%eax, %eax
	xorl	%edi, %edi
	movl	$16, %esi
	leaq	(%r12,%rax,8), %r8
	movl	$2, %ebx
	movl	$32, %r14d
	movq	16(%r8), %rcx
	testq	%rcx, %rcx
	jne	.L685
.L689:
	leal	2147483647(%rbx), %eax
	addl	%eax, %eax
	leaq	112(,%rax,8), %rax
	jmp	.L681
	.p2align 4,,10
	.p2align 3
.L699:
	movq	%rdx, %rax
#APP
# 3835 "malloc.c" 1
	cmpl $0, %fs:24
	je 0f
	lock
0:	cmpxchgq %rbx, (%rsi)
# 0 "" 2
#NO_APP
	cmpq	%rdx, %rax
	je	.L918
	testq	%rax, %rax
	je	.L710
	movq	16(%rax), %r11
	leaq	16(%rax), %rdi
	movq	%rax, %rdx
.L697:
	shrq	$12, %rdi
	movq	%rdi, %rbx
	xorq	%r11, %rbx
	cmpq	%r11, %rdi
	je	.L699
	testb	$15, %bl
	je	.L699
.L700:
	leaq	.LC59(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L911:
	movq	%r12, %rdi
	call	malloc_consolidate
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L905:
	movq	%r14, %rcx
	movq	%r14, %rax
	movl	%ebx, 8(%rsp)
	shrq	$9, %rcx
	shrq	$6, %rax
	movl	$110, 132(%rsp)
	leal	91(%rcx), %edi
	movq	%rax, 80(%rsp)
	addl	$48, %eax
	movq	%rcx, 96(%rsp)
	movl	%eax, 92(%rsp)
	movl	%edi, 120(%rsp)
	movl	$119, 128(%rsp)
	movl	$124, 124(%rsp)
	movq	$0, 112(%rsp)
	movq	$0, 104(%rsp)
	jmp	.L705
	.p2align 4,,10
	.p2align 3
.L916:
	leaq	main_arena(%rip), %rax
	orq	$1, %rcx
	movq	%rcx, 8(%rdi)
	cmpq	%rax, %r12
	je	.L731
	orq	$4, 8(%rdx)
.L731:
	cmpq	$0, 40(%rsp)
	leaq	16(%rdx), %rcx
	je	.L732
	movq	16(%rsp), %rax
	movzwl	(%rax), %esi
	cmpq	24(%rsp), %rsi
	movq	%rsi, %rax
	jb	.L919
.L732:
	movq	56(%rsp), %r13
.L895:
	movl	perturb_byte(%rip), %esi
	testl	%esi, %esi
	je	.L675
.L892:
	movq	%rcx, %rdi
	xorb	$-1, %sil
	movq	%r13, %rdx
	call	memset
	movq	%rax, %rcx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L750:
	addl	$1, %r9d
	cmpl	$9999, %r9d
	jg	.L899
.L890:
	movq	120(%r12), %rdx
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L719:
	leaq	.LC63(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L900:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	movl	$12, %fs:(%rax)
	movq	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L789:
	movl	$32, %r14d
.L679:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	sysmalloc
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.L895
.L785:
	xorl	%ecx, %ecx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L807:
	leaq	.LC64(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L734:
	movq	%rax, %rcx
	shrq	$6, %rcx
	cmpq	$48, %rcx
	jbe	.L920
	movq	%rax, %rcx
	shrq	$9, %rcx
	cmpq	$20, %rcx
	ja	.L738
	addl	$91, %ecx
.L737:
	leal	-2(%rcx,%rcx), %esi
	movslq	%esi, %rsi
	leaq	112(%r12,%rsi,8), %r8
	movq	(%r8), %rsi
	leaq	-16(%r8), %rdi
	cmpq	%rsi, %rdi
	je	.L741
	movq	8(%r8), %r8
	orq	$1, %rax
	movq	8(%r8), %rbx
	testb	$4, %bl
	jne	.L921
	cmpq	%rax, %rbx
	jbe	.L743
	movq	40(%rsi), %rax
	movq	%rsi, 32(%rdx)
	movq	%rax, 40(%rdx)
	movq	%rdx, 32(%rax)
	movq	%rdx, 40(%rsi)
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L749:
	addl	$1, %r9d
	cmpl	$9999, %r9d
	jle	.L890
	movq	48(%rsp), %r11
.L753:
	cmpq	$1023, %r14
	jbe	.L756
	movl	8(%rsp), %eax
	addl	$2147483647, %eax
	addl	%eax, %eax
	leaq	112(%r12,%rax,8), %rax
	movq	(%rax), %rdx
	leaq	-16(%rax), %rcx
	cmpq	%rcx, %rdx
	je	.L756
	cmpq	%r14, 8(%rdx)
	jnb	.L922
.L756:
	movl	8(%rsp), %eax
	leal	1(%rax), %ecx
	addl	%eax, %eax
	leaq	96(%r12,%rax,8), %rdx
	movl	$1, %eax
	movl	%ecx, %edi
	sall	%cl, %eax
	shrl	$5, %edi
	movl	%edi, %r9d
	movl	2144(%r12,%r9,4), %esi
.L772:
	cmpl	%esi, %eax
	ja	.L768
	testl	%eax, %eax
	jne	.L769
	jmp	.L768
	.p2align 4,,10
	.p2align 3
.L770:
	addq	$16, %rdx
	addl	%eax, %eax
	je	.L923
.L769:
	testl	%esi, %eax
	je	.L770
	movq	24(%rdx), %r8
	cmpq	%r8, %rdx
	jne	.L771
	movl	%eax, %ecx
	addq	$16, %rdx
	addl	%eax, %eax
	notl	%ecx
	andl	%ecx, %esi
	movl	%esi, 2144(%r12,%r9,4)
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L795:
	movq	$0, 40(%rsp)
	jmp	.L716
	.p2align 4,,10
	.p2align 3
.L724:
	leaq	.LC66(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L920:
	addl	$48, %ecx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L910:
	cmpq	$4, %r15
	cmova	%r8d, %r11d
	movl	%r11d, 8(%rsp)
	jmp	.L715
	.p2align 4,,10
	.p2align 3
.L915:
	movq	104(%r12), %r11
	cmpq	%rdx, %r11
	movq	%r11, 136(%rsp)
	jne	.L727
	cmpq	%rax, 72(%rsp)
	jnb	.L727
	subq	%r14, %rsi
	addq	%r14, %rdx
	movq	56(%rsp), %r13
	cmpq	$1023, %rsi
	movq	%rdx, 112(%r12)
	movq	%rdx, 120(%r12)
	movq	%rdx, 104(%r12)
	movq	%rbx, 16(%rdx)
	movq	%rbx, 24(%rdx)
	jbe	.L728
	movq	$0, 32(%rdx)
	movq	$0, 40(%rdx)
.L728:
	leaq	main_arena(%rip), %rax
	xorl	%ecx, %ecx
	movq	136(%rsp), %rbx
	cmpq	%rax, %r12
	movq	%r14, %rax
	setne	%cl
	orq	$1, %rax
	salq	$2, %rcx
	orq	%rcx, %rax
	leaq	16(%rbx), %rcx
	movq	%rax, 8(%rbx)
	movq	%rsi, %rax
	orq	$1, %rax
	movq	%rax, 8(%rdx)
	movl	perturb_byte(%rip), %eax
	movq	%rsi, (%rdi)
	testl	%eax, %eax
	je	.L675
.L891:
	xorb	$-1, %al
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movl	%eax, %esi
	call	memset
	movq	%rax, %rcx
	jmp	.L675
	.p2align 4,,10
	.p2align 3
.L925:
	movl	%edi, %r9d
	movl	2144(%r12,%r9,4), %esi
	testl	%esi, %esi
	jne	.L924
.L768:
	addl	$1, %edi
	cmpl	$4, %edi
	jne	.L925
.L767:
	movq	96(%r12), %rcx
	movq	8(%rcx), %rax
	andq	$-8, %rax
	cmpq	%rax, 2184(%r12)
	jb	.L926
	leaq	32(%r14), %rdx
	cmpq	%rax, %rdx
	jbe	.L927
	movq	%r11, 24(%rsp)
	movq	%r10, 16(%rsp)
	movl	8(%r12), %eax
	testl	%eax, %eax
	je	.L783
	movq	%r12, %rdi
	call	malloc_consolidate
	movl	88(%rsp), %eax
	cmpq	$1023, %r14
	movq	16(%rsp), %r10
	movq	24(%rsp), %r11
	movl	%eax, 8(%rsp)
	jbe	.L784
	cmpq	$48, 80(%rsp)
	movl	92(%rsp), %eax
	movl	%eax, 8(%rsp)
	jbe	.L784
	cmpq	$20, 96(%rsp)
	movl	120(%rsp), %eax
	movl	%eax, 8(%rsp)
	jbe	.L784
	cmpq	$10, 104(%rsp)
	movl	132(%rsp), %eax
	movl	%eax, 8(%rsp)
	jbe	.L784
	cmpq	$5, 112(%rsp)
	movl	124(%rsp), %eax
	cmovb	128(%rsp), %eax
	movl	%eax, 8(%rsp)
.L784:
	movq	%fs:tcache@tpoff, %r15
	jmp	.L717
	.p2align 4,,10
	.p2align 3
.L695:
	leaq	.LC61(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L913:
	leaq	.LC65(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L902:
	leaq	.LC58(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L907:
	movq	24(%rdx), %rsi
	orq	$1, 8(%rdx,%r14)
	cmpq	%r10, %r12
	je	.L713
	orq	$4, 8(%rdx)
.L713:
	leaq	16(%rdx), %r15
	movq	%rsi, 8(%rax)
	addl	$1, %r11d
	movq	%r8, 16(%rsi)
	movq	%r15, %rsi
	movq	%r9, 24(%rdx)
	shrq	$12, %rsi
	xorq	128(%rbp), %rsi
	movq	%rsi, 16(%rdx)
	movq	%r15, 128(%rbp)
	movw	%r11w, (%rbx)
	jmp	.L711
	.p2align 4,,10
	.p2align 3
.L914:
	leaq	.LC67(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L919:
	movq	64(%rsp), %rbx
	movq	%rcx, %rsi
	movq	%r15, 24(%rdx)
	shrq	$12, %rsi
	addl	$1, %eax
	movl	$1, %r13d
	xorq	128(%rbx), %rsi
	movq	%rsi, 16(%rdx)
	movq	%rcx, 128(%rbx)
	movq	%r8, %rdx
	movq	16(%rsp), %rbx
	movw	%ax, (%rbx)
	jmp	.L718
	.p2align 4,,10
	.p2align 3
.L903:
	leaq	.LC60(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L741:
	movq	%rdx, 40(%rdx)
	movq	%rdx, 32(%rdx)
	movq	%rdi, %r8
	jmp	.L735
	.p2align 4,,10
	.p2align 3
.L906:
	leaq	.LC62(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L738:
	movq	%rax, %rcx
	shrq	$12, %rcx
	cmpq	$10, %rcx
	ja	.L739
	addl	$110, %ecx
	jmp	.L737
	.p2align 4,,10
	.p2align 3
.L743:
	movq	8(%rsi), %rdi
	testb	$4, %dil
	jne	.L928
	movq	48(%rsp), %r11
	jmp	.L744
	.p2align 4,,10
	.p2align 3
.L745:
	movq	32(%rsi), %rsi
	movq	8(%rsi), %rdi
	testb	$4, %dil
	jne	.L929
.L744:
	cmpq	%rdi, %rax
	jb	.L745
	movq	%r11, 48(%rsp)
	je	.L930
	movq	40(%rsi), %rax
	movq	%rsi, 32(%rdx)
	movq	%rax, 40(%rdx)
	movq	40(%rsi), %rax
	cmpq	%rsi, 32(%rax)
	jne	.L931
	movq	%rdx, 40(%rsi)
	movq	40(%rdx), %rax
	movq	%rsi, %rdi
	movq	%rdx, 32(%rax)
.L747:
	movq	24(%rdi), %r8
	cmpq	%rdi, 16(%r8)
	je	.L735
	leaq	.LC71(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L918:
	leaq	16(%rdx), %rbx
	movq	%rbx, %rax
	shrq	$12, %rax
	jmp	.L698
	.p2align 4,,10
	.p2align 3
.L912:
	testl	%r13d, %r13d
	movq	48(%rsp), %r11
	je	.L753
	jmp	.L897
	.p2align 4,,10
	.p2align 3
.L924:
	movl	%edi, %eax
	sall	$6, %eax
	subl	$2, %eax
	leaq	96(%r12,%rax,8), %rdx
	movl	$1, %eax
	jmp	.L769
.L739:
	movq	%rax, %rcx
	shrq	$15, %rcx
	cmpq	$4, %rcx
	ja	.L740
	addl	$119, %ecx
	jmp	.L737
.L930:
	movq	16(%rsi), %rdi
	jmp	.L747
.L740:
	movq	%rax, %rcx
	movl	$2, %ebx
	shrq	$18, %rcx
	cmpq	$2, %rcx
	cmova	%rbx, %rcx
	addl	$124, %ecx
	jmp	.L737
.L771:
	movq	8(%r8), %r15
	movq	56(%rsp), %r13
	andq	$-8, %r15
	cmpq	%r15, %r14
	ja	.L932
	movq	%r15, %rbx
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	subq	%r14, %rbx
	call	unlink_chunk.isra.2
	cmpq	$31, %rbx
	movq	8(%rsp), %r8
	ja	.L774
	leaq	main_arena(%rip), %rax
	orq	$1, 8(%r8,%r15)
	cmpq	%rax, %r12
	je	.L775
	orq	$4, 8(%r8)
.L775:
	movl	perturb_byte(%rip), %eax
	leaq	16(%r8), %rcx
	testl	%eax, %eax
	je	.L675
	jmp	.L891
.L774:
	movq	112(%r12), %rdx
	leaq	(%r8,%r14), %rax
	cmpq	%rbp, 24(%rdx)
	jne	.L933
	cmpq	$1023, %r14
	movq	%rbp, 24(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 112(%r12)
	movq	%rax, 24(%rdx)
	ja	.L777
	movq	%rax, 104(%r12)
.L777:
	cmpq	$1023, %rbx
	jbe	.L778
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
.L778:
	leaq	main_arena(%rip), %rdx
	xorl	%ecx, %ecx
	cmpq	%rdx, %r12
	movq	%r14, %rdx
	setne	%cl
	orq	$1, %rdx
	salq	$2, %rcx
	orq	%rcx, %rdx
	movq	%rdx, 8(%r8)
	movq	%rbx, %rdx
	orq	$1, %rdx
	movq	%rdx, 8(%rax)
	movq	%rbx, (%r8,%r15)
	jmp	.L775
.L931:
	leaq	.LC70(%rip), %rdi
	call	malloc_printerr
.L926:
	leaq	.LC77(%rip), %rdi
	call	malloc_printerr
.L783:
	movq	%r12, %rsi
	movq	%r14, %rdi
	movq	56(%rsp), %r13
	call	sysmalloc
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L785
	movl	perturb_byte(%rip), %eax
	testl	%eax, %eax
	je	.L675
	jmp	.L891
.L927:
	leaq	main_arena(%rip), %rdx
	subq	%r14, %rax
	xorl	%esi, %esi
	leaq	(%rcx,%r14), %rdi
	movq	56(%rsp), %r13
	cmpq	%rdx, %r12
	movq	%r14, %rdx
	setne	%sil
	orq	$1, %rdx
	orq	$1, %rax
	salq	$2, %rsi
	movq	%rdi, 96(%r12)
	addq	$16, %rcx
	orq	%rsi, %rdx
	movq	%rdx, -8(%rcx)
	movq	%rax, 8(%rdi)
	movl	perturb_byte(%rip), %eax
	testl	%eax, %eax
	je	.L675
	jmp	.L891
.L923:
	leaq	__PRETTY_FUNCTION__.13182(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC74(%rip), %rdi
	movl	$4240, %edx
	call	__malloc_assert
.L922:
	movq	56(%rsp), %r13
	movq	40(%rdx), %rbx
	jmp	.L757
.L758:
	movq	40(%rbx), %rbx
.L757:
	movq	8(%rbx), %rdx
	movq	%rdx, %r15
	andq	$-8, %r15
	cmpq	%r15, %r14
	ja	.L758
	cmpq	%rbx, 8(%rax)
	je	.L759
	movq	16(%rbx), %rax
	cmpq	8(%rax), %rdx
	cmove	%rax, %rbx
.L759:
	movq	%r15, %rsi
	movq	%rbx, %rdi
	subq	%r14, %rsi
	movq	%rsi, 8(%rsp)
	call	unlink_chunk.isra.2
	movq	8(%rsp), %rsi
	cmpq	$31, %rsi
	ja	.L760
	leaq	main_arena(%rip), %rax
	orq	$1, 8(%rbx,%r15)
	cmpq	%rax, %r12
	je	.L761
	orq	$4, 8(%rbx)
.L761:
	movl	perturb_byte(%rip), %eax
	leaq	16(%rbx), %rcx
	testl	%eax, %eax
	je	.L675
	jmp	.L891
.L929:
	leaq	__PRETTY_FUNCTION__.13182(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC69(%rip), %rdi
	movl	$4079, %edx
	call	__malloc_assert
.L917:
	leaq	.LC72(%rip), %rdi
	call	malloc_printerr
.L933:
	leaq	.LC76(%rip), %rdi
	call	malloc_printerr
.L921:
	leaq	__PRETTY_FUNCTION__.13182(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC68(%rip), %rdi
	movl	$4062, %edx
	call	__malloc_assert
.L932:
	leaq	__PRETTY_FUNCTION__.13182(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC75(%rip), %rdi
	movl	$4259, %edx
	call	__malloc_assert
.L760:
	movq	112(%r12), %rdx
	leaq	(%rbx,%r14), %rax
	cmpq	%rbp, 24(%rdx)
	jne	.L934
	cmpq	$1023, %rsi
	movq	%rbp, 24(%rax)
	movq	%rdx, 16(%rax)
	movq	%rax, 112(%r12)
	movq	%rax, 24(%rdx)
	jbe	.L763
	movq	$0, 32(%rax)
	movq	$0, 40(%rax)
.L763:
	leaq	main_arena(%rip), %rdx
	xorl	%ecx, %ecx
	cmpq	%rdx, %r12
	movq	%r14, %rdx
	setne	%cl
	orq	$1, %rdx
	salq	$2, %rcx
	orq	%rcx, %rdx
	movq	%rdx, 8(%rbx)
	movq	%rsi, %rdx
	orq	$1, %rdx
	movq	%rdx, 8(%rax)
	movq	%rsi, (%rbx,%r15)
	jmp	.L761
.L934:
	leaq	.LC73(%rip), %rdi
	call	malloc_printerr
.L928:
	leaq	__PRETTY_FUNCTION__.13182(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC69(%rip), %rdi
	movl	$4075, %edx
	call	__malloc_assert
	.size	_int_malloc, .-_int_malloc
	.p2align 4,,15
	.type	malloc_check, @function
malloc_check:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	addq	$1, %rbp
	jc	.L937
	movq	%rdi, %rbx
#APP
# 248 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L940
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L941:
	call	top_check
	leaq	main_arena(%rip), %rdi
	movq	%rbp, %rsi
	call	_int_malloc
	movq	%rax, %r8
#APP
# 251 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L942
	subl	$1, main_arena(%rip)
.L943:
	addq	$8, %rsp
	movq	%rbx, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%rbp
	jmp	mem2mem_check
	.p2align 4,,10
	.p2align 3
.L940:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L941
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L941
	.p2align 4,,10
	.p2align 3
.L942:
	xorl	%eax, %eax
#APP
# 251 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L943
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 251 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L943
.L937:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rdx
	popq	%rbx
	popq	%rbp
	ret
	.size	malloc_check, .-malloc_check
	.p2align 4,,15
	.type	tcache_init.part.6, @function
tcache_init.part.6:
	movq	thread_arena@gottpoff(%rip), %rax
	pushq	%rbx
	movq	%fs:(%rax), %rbx
	testq	%rbx, %rbx
	je	.L948
#APP
# 3151 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L949
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L950:
	movl	$640, %esi
	movq	%rbx, %rdi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L955
.L956:
#APP
# 3161 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L953
	subl	$1, (%rbx)
.L952:
	testq	%r8, %r8
	je	.L947
	leaq	8(%r8), %rdi
	movq	%r8, %fs:tcache@tpoff
	xorl	%eax, %eax
	movq	$0, (%r8)
	andq	$-8, %rdi
	movq	$0, 632(%r8)
	subq	%rdi, %r8
	leal	640(%r8), %ecx
	shrl	$3, %ecx
	rep stosq
.L947:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L948:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L950
	xorl	%esi, %esi
	movl	$640, %edi
	call	arena_get2.part.4
	movl	$640, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L951
	testq	%rbx, %rbx
	je	.L951
	.p2align 4,,10
	.p2align 3
.L955:
	movq	%rbx, %rdi
	movl	$640, %esi
	call	arena_get_retry
	movl	$640, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	call	_int_malloc
	movq	%rax, %r8
.L951:
	testq	%rbx, %rbx
	je	.L952
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L949:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L950
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L950
	.p2align 4,,10
	.p2align 3
.L953:
	xorl	%eax, %eax
#APP
# 3161 "malloc.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L952
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 3161 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L952
	.size	tcache_init.part.6, .-tcache_init.part.6
	.section	.rodata.str1.8
	.align 8
.LC78:
	.string	"newsize >= nb && (((unsigned long) (chunk2rawmem (p))) % alignment) == 0"
	.text
	.p2align 4,,15
	.type	_int_memalign, @function
_int_memalign:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	js	.L994
	addq	$23, %rdx
	movl	$32, %eax
	movq	%rsi, %rbp
	movq	%rdx, %r12
	movq	%rdi, %r13
	andq	$-16, %r12
	cmpq	$31, %rdx
	cmovbe	%rax, %r12
	leaq	32(%r12,%rsi), %rsi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L973
	xorl	%edx, %edx
	leaq	-16(%rax), %rcx
	divq	%rbp
	testq	%rdx, %rdx
	je	.L977
	leaq	-1(%rbx,%rbp), %rax
	movq	%rbp, %rdx
	negq	%rdx
	andq	%rdx, %rax
	subq	$16, %rax
	movq	%rax, %rdx
	leaq	(%rax,%rbp), %r14
	subq	%rcx, %rdx
	cmpq	$32, %rdx
	cmovnb	%rax, %r14
	movq	-8(%rbx), %rax
	movq	%r14, %rsi
	subq	%rcx, %rsi
	movq	%rax, %r15
	andq	$-8, %r15
	subq	%rsi, %r15
	andl	$2, %eax
	jne	.L995
	leaq	main_arena(%rip), %rdx
	movq	%r13, %rdi
	cmpq	%rdx, %r13
	movl	$4, %edx
	cmovne	%rdx, %rax
	movq	%r15, %rdx
	orq	$1, %rdx
	orq	%rax, %rdx
	movq	%rdx, 8(%r14)
	orq	$1, 8(%r14,%r15)
	movq	-8(%rbx), %rdx
	andl	$7, %edx
	orq	%rsi, %rdx
	movq	%rcx, %rsi
	orq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, -8(%rbx)
	call	_int_free
	cmpq	%r12, %r15
	jb	.L981
	leaq	16(%r14), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rax
	divq	%rbp
	testq	%rdx, %rdx
	jne	.L981
	movq	%r14, %rcx
.L977:
	movq	8(%rcx), %rax
	testb	$2, %al
	jne	.L973
	leaq	32(%r12), %rdx
	andq	$-8, %rax
	cmpq	%rax, %rdx
	jnb	.L973
	leaq	main_arena(%rip), %rdx
	subq	%r12, %rax
	leaq	(%rcx,%r12), %rsi
	movq	%r13, %rdi
	cmpq	%rdx, %r13
	setne	%dl
	orq	$1, %rax
	movzbl	%dl, %edx
	salq	$2, %rdx
	orq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, 8(%rsi)
	movq	8(%rcx), %rax
	andl	$7, %eax
	orq	%rax, %r12
	movq	%r12, 8(%rcx)
	call	_int_free
.L973:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L995:
	addq	-16(%rbx), %rsi
	orq	$2, %r15
	leaq	16(%r14), %rbx
	movq	%r15, 8(%r14)
	movq	%rsi, (%r14)
	jmp	.L973
	.p2align 4,,10
	.p2align 3
.L994:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	movl	$12, %fs:(%rax)
	jmp	.L973
.L981:
	leaq	__PRETTY_FUNCTION__.13380(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC78(%rip), %rdi
	movl	$4967, %edx
	call	__malloc_assert
	.size	_int_memalign, .-_int_memalign
	.p2align 4,,15
	.type	memalign_check, @function
memalign_check:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	cmpq	$16, %rdi
	jbe	.L1013
	cmpq	$31, %rdi
	ja	.L1014
	cmpq	$-65, %rsi
	ja	.L1008
	movl	$32, %ebx
.L1002:
#APP
# 416 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1004
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1005:
	call	top_check
	leaq	1(%rbp), %rdx
	leaq	main_arena(%rip), %rdi
	movq	%rbx, %rsi
	call	_int_memalign
	movq	%rax, %r8
#APP
# 419 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1006
	subl	$1, main_arena(%rip)
.L1007:
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%r8, %rdi
	popq	%rbx
	popq	%rbp
	jmp	mem2mem_check
	.p2align 4,,10
	.p2align 3
.L1014:
	movabsq	$-9223372036854775808, %rdx
	cmpq	%rdx, %rdi
	ja	.L1015
	movq	$-33, %rdx
	subq	%rdi, %rdx
	cmpq	%rsi, %rdx
	jb	.L1008
	leaq	-1(%rdi), %rdx
	testq	%rdi, %rdx
	je	.L1009
	cmpq	$32, %rdi
	movl	$32, %ebx
	je	.L1002
	.p2align 4,,10
	.p2align 3
.L1003:
	addq	%rbx, %rbx
	cmpq	%rbx, %rdi
	ja	.L1003
	jmp	.L1002
	.p2align 4,,10
	.p2align 3
.L1013:
	addq	$8, %rsp
	movq	%rbp, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	jmp	malloc_check
	.p2align 4,,10
	.p2align 3
.L1004:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1005
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1005
	.p2align 4,,10
	.p2align 3
.L1006:
	xorl	%eax, %eax
#APP
# 419 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1007
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 419 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1007
	.p2align 4,,10
	.p2align 3
.L1008:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L996:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1015:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L996
.L1009:
	movq	%rdi, %rbx
	jmp	.L1002
	.size	memalign_check, .-memalign_check
	.p2align 4,,15
	.type	free_check, @function
free_check:
	testq	%rdi, %rdi
	je	.L1030
	pushq	%r12
	pushq	%rbp
	movq	__libc_errno@gottpoff(%rip), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%fs:0(%rbp), %r12d
#APP
# 271 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1018
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1019:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	mem2chunk_check
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1033
	testb	$2, 8(%rax)
	je	.L1021
#APP
# 277 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1022
	subl	$1, main_arena(%rip)
.L1023:
	movq	%r8, %rdi
	call	munmap_chunk
.L1024:
	movl	%r12d, %fs:0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1021:
	leaq	main_arena(%rip), %rdi
	movl	$1, %edx
	movq	%rax, %rsi
	call	_int_free
#APP
# 286 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1025
	movl	%r12d, %fs:0(%rbp)
	subl	$1, main_arena(%rip)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1030:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1018:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1019
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1019
	.p2align 4,,10
	.p2align 3
.L1022:
	xorl	%eax, %eax
#APP
# 277 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1023
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 277 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1023
	.p2align 4,,10
	.p2align 3
.L1025:
	xorl	%eax, %eax
#APP
# 286 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1024
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 286 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1024
.L1033:
	leaq	.LC31(%rip), %rdi
	call	malloc_printerr
	.size	free_check, .-free_check
	.section	.rodata.str1.1
.LC79:
	.string	"realloc(): invalid old size"
.LC80:
	.string	"!chunk_is_mmapped (oldp)"
.LC81:
	.string	"realloc(): invalid next size"
	.section	.rodata.str1.8
	.align 8
.LC82:
	.string	"(unsigned long) (newsize) >= (unsigned long) (nb)"
	.text
	.p2align 4,,15
	.type	_int_realloc, @function
_int_realloc:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	movq	8(%rsi), %rax
	cmpq	$16, %rax
	jbe	.L1035
	movq	%rdi, %rbp
	movq	2184(%rdi), %rdi
	cmpq	%rdx, %rdi
	jbe	.L1035
	movq	%rax, %r12
	andl	$2, %r12d
	jne	.L1061
	leaq	(%rsi,%rdx), %r13
	movq	%rsi, %rbx
	movq	8(%r13), %r8
	movq	%r8, %rsi
	andq	$-8, %rsi
	cmpq	%rsi, %rdi
	jbe	.L1055
	cmpq	$16, %r8
	jbe	.L1055
	cmpq	%rcx, %rdx
	jb	.L1062
.L1053:
	movq	%rdx, %rdi
	andl	$7, %eax
	subq	%rcx, %rdi
	cmpq	$31, %rdi
	ja	.L1049
	leaq	main_arena(%rip), %rcx
	cmpq	%rcx, %rbp
	movl	$4, %ecx
	cmovne	%rcx, %r12
	orq	%rax, %rdx
	orq	%r12, %rdx
	movq	%rdx, 8(%rbx)
	orq	$1, 8(%r13)
.L1051:
	leaq	16(%rbx), %r14
.L1034:
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1062:
	cmpq	%r13, 96(%rbp)
	je	.L1063
	testb	$1, 8(%r13,%rsi)
	je	.L1064
.L1042:
	leaq	-15(%rcx), %rsi
	movq	%rbp, %rdi
	movq	%rdx, 8(%rsp)
	movq	%rcx, (%rsp)
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1034
	leaq	-16(%rax), %rax
	movq	(%rsp), %rcx
	movq	8(%rsp), %rdx
	cmpq	%rax, %r13
	je	.L1065
	movq	8(%rbx), %rax
	leaq	16(%rbx), %rsi
	movq	%r14, %rdi
	movq	%rax, %rdx
	andq	$-8, %rdx
	testb	$2, %al
	sete	%al
	movzbl	%al, %eax
	leaq	-16(%rdx,%rax,8), %rdx
	call	memcpy@PLT
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	movl	$1, %edx
	call	_int_free
	addq	$16, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1049:
	leaq	main_arena(%rip), %rdx
	leaq	(%rbx,%rcx), %rsi
	cmpq	%rdx, %rbp
	movl	$4, %edx
	cmovne	%rdx, %r12
	orq	$1, %rdi
	orq	%rcx, %rax
	movq	%rdi, %rdx
	orq	%r12, %rax
	movq	%rbp, %rdi
	orq	%r12, %rdx
	movq	%rax, 8(%rbx)
	movq	%rdx, 8(%rsi)
	orq	$1, 8(%r13)
	movl	$1, %edx
	call	_int_free
	jmp	.L1051
	.p2align 4,,10
	.p2align 3
.L1064:
	leaq	(%rdx,%rsi), %r14
	cmpq	%r14, %rcx
	ja	.L1042
	movq	%r13, %rdi
	movq	%rcx, (%rsp)
	leaq	(%rbx,%r14), %r13
	call	unlink_chunk.isra.2
	movq	8(%rbx), %rax
	movq	%r14, %rdx
	movq	(%rsp), %rcx
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1065:
	movq	-8(%r14), %rax
	andq	$-8, %rax
	addq	%rax, %rdx
	cmpq	%rdx, %rcx
	ja	.L1066
	movq	8(%rbx), %rax
	leaq	(%rbx,%rdx), %r13
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1035:
	leaq	.LC79(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1055:
	leaq	.LC81(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1063:
	leaq	(%rdx,%rsi), %rdi
	leaq	32(%rcx), %rsi
	cmpq	%rdi, %rsi
	ja	.L1042
	andl	$7, %eax
	leaq	16(%rbx), %r14
	movq	%rax, %rsi
	leaq	main_arena(%rip), %rax
	cmpq	%rax, %rbp
	movl	$4, %eax
	cmovne	%rax, %r12
	orq	%rcx, %rsi
	subq	%rcx, %rdi
	orq	%r12, %rsi
	leaq	(%rbx,%rcx), %rax
	movq	%rsi, 8(%rbx)
	movq	%rdi, %rsi
	orq	$1, %rsi
	movq	%rax, 96(%rbp)
	movq	%rsi, 8(%rax)
	jmp	.L1034
.L1061:
	leaq	__PRETTY_FUNCTION__.13362(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC80(%rip), %rdi
	movl	$4793, %edx
	call	__malloc_assert
.L1066:
	leaq	__PRETTY_FUNCTION__.13362(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC82(%rip), %rdi
	movl	$4866, %edx
	call	__malloc_assert
	.size	_int_realloc, .-_int_realloc
	.section	.rodata.str1.1
.LC83:
	.string	"realloc(): invalid pointer"
	.text
	.p2align 4,,15
	.type	realloc_check, @function
realloc_check:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	addq	$1, %r13
	jc	.L1069
	testq	%rdi, %rdi
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	je	.L1099
	testq	%rsi, %rsi
	je	.L1100
#APP
# 319 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1074
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1075:
	leaq	8(%rsp), %rsi
	movq	%rbp, %rdi
	call	mem2chunk_check
	movq	%rax, %r12
#APP
# 321 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1076
	subl	$1, main_arena(%rip)
.L1077:
	testq	%r12, %r12
	je	.L1101
	testq	%r13, %r13
	js	.L1087
	leaq	23(%r13), %rax
	movl	$32, %ecx
	movq	8(%r12), %r15
	movq	%rax, %rsi
	andq	$-16, %rsi
	cmpq	$31, %rax
	cmova	%rsi, %rcx
	movq	%rcx, %r14
#APP
# 329 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1082
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1083:
	andq	$-8, %r15
	testb	$2, 8(%r12)
	je	.L1084
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	mremap_chunk
	testq	%rax, %rax
	je	.L1085
	leaq	16(%rax), %rbp
	testq	%rbp, %rbp
	jne	.L1080
.L1087:
	movq	8(%rsp), %rax
	xorl	%ebp, %ebp
	notb	(%rax)
.L1080:
#APP
# 376 "hooks.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1088
	subl	$1, main_arena(%rip)
.L1089:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	mem2mem_check
.L1067:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1084:
.L1086:
	call	top_check
	leaq	main_arena(%rip), %rdi
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%r12, %rsi
	call	_int_realloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.L1080
	jmp	.L1087
	.p2align 4,,10
	.p2align 3
.L1099:
	movq	%rbx, %rdi
	xorl	%esi, %esi
	call	malloc_check
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1074:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1075
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1075
	.p2align 4,,10
	.p2align 3
.L1076:
	xorl	%eax, %eax
#APP
# 321 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1077
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 321 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1077
	.p2align 4,,10
	.p2align 3
.L1088:
	xorl	%eax, %eax
#APP
# 376 "hooks.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1089
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 376 "hooks.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1089
	.p2align 4,,10
	.p2align 3
.L1085:
	leaq	-8(%r15), %rax
	cmpq	%rax, %r14
	jbe	.L1080
	call	top_check
	leaq	main_arena(%rip), %rdi
	movq	%r13, %rsi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L1087
	leaq	-16(%r15), %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movq	%r13, %rbp
	call	memcpy@PLT
	movq	%r12, %rdi
	call	munmap_chunk
	jmp	.L1080
	.p2align 4,,10
	.p2align 3
.L1082:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, main_arena(%rip)
	je	.L1083
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1083
	.p2align 4,,10
	.p2align 3
.L1100:
	xorl	%esi, %esi
	call	free_check
	xorl	%eax, %eax
	jmp	.L1067
.L1069:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1067
.L1101:
	leaq	.LC83(%rip), %rdi
	call	malloc_printerr
	.size	realloc_check, .-realloc_check
	.p2align 4,,15
	.globl	__malloc_fork_lock_parent
	.hidden	__malloc_fork_lock_parent
	.type	__malloc_fork_lock_parent, @function
__malloc_fork_lock_parent:
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L1111
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
#APP
# 152 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1104
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_lock(%rip)
# 0 "" 2
#NO_APP
.L1105:
	leaq	main_arena(%rip), %rbp
	xorl	%r12d, %r12d
	movq	%rbp, %rbx
	.p2align 4,,10
	.p2align 3
.L1108:
#APP
# 156 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1106
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L1107:
	movq	2160(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.L1108
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1106:
	movl	%r12d, %eax
	lock cmpxchgl	%edx, (%rbx)
	je	.L1107
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L1107
	.p2align 4,,10
	.p2align 3
.L1111:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1104:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_lock(%rip)
	je	.L1105
	leaq	list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1105
	.size	__malloc_fork_lock_parent, .-__malloc_fork_lock_parent
	.p2align 4,,15
	.globl	__malloc_fork_unlock_parent
	.hidden	__malloc_fork_unlock_parent
	.type	__malloc_fork_unlock_parent, @function
__malloc_fork_unlock_parent:
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L1125
	leaq	main_arena(%rip), %r9
	pushq	%rbp
	movl	$202, %ebp
	pushq	%rbx
	xorl	%ebx, %ebx
	movq	%r9, %r8
	.p2align 4,,10
	.p2align 3
.L1115:
#APP
# 171 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1117
	subl	$1, (%r8)
.L1118:
	movq	2160(%r8), %r8
	cmpq	%r9, %r8
	jne	.L1115
#APP
# 176 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1119
	subl	$1, list_lock(%rip)
.L1114:
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1117:
	movl	%ebx, %eax
#APP
# 171 "arena.c" 1
	xchgl %eax, (%r8)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1118
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r8, %rdi
	movl	%ebp, %eax
#APP
# 171 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1118
	.p2align 4,,10
	.p2align 3
.L1125:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1119:
	xorl	%eax, %eax
#APP
# 176 "arena.c" 1
	xchgl %eax, list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1114
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 176 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1114
	.size	__malloc_fork_unlock_parent, .-__malloc_fork_unlock_parent
	.p2align 4,,15
	.globl	__malloc_fork_unlock_child
	.hidden	__malloc_fork_unlock_child
	.type	__malloc_fork_unlock_child, @function
__malloc_fork_unlock_child:
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	jle	.L1128
	movq	thread_arena@gottpoff(%rip), %rax
	movl	$0, free_list_lock(%rip)
	movq	%fs:(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1130
	movq	$1, 2176(%rcx)
.L1130:
	leaq	main_arena(%rip), %rsi
	movq	$0, free_list(%rip)
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	%rsi, %rax
	.p2align 4,,10
	.p2align 3
.L1132:
	cmpq	%rax, %rcx
	movl	$0, (%rax)
	je	.L1131
	movq	%rdx, 2168(%rax)
	movq	$0, 2176(%rax)
	movq	%rax, %rdx
	movl	$1, %edi
.L1131:
	movq	2160(%rax), %rax
	cmpq	%rsi, %rax
	jne	.L1132
	testb	%dil, %dil
	jne	.L1141
.L1133:
	movl	$0, list_lock(%rip)
.L1128:
	rep ret
.L1141:
	movq	%rdx, free_list(%rip)
	jmp	.L1133
	.size	__malloc_fork_unlock_child, .-__malloc_fork_unlock_child
	.p2align 4,,15
	.globl	__malloc_check_init
	.hidden	__malloc_check_init
	.type	__malloc_check_init, @function
__malloc_check_init:
	leaq	malloc_check(%rip), %rax
	movl	$1, using_malloc_checking(%rip)
	movq	%rax, __malloc_hook(%rip)
	leaq	free_check(%rip), %rax
	movq	%rax, __free_hook(%rip)
	leaq	realloc_check(%rip), %rax
	movq	%rax, __realloc_hook(%rip)
	leaq	memalign_check(%rip), %rax
	movq	%rax, __memalign_hook(%rip)
	ret
	.size	__malloc_check_init, .-__malloc_check_init
	.section	.rodata.str1.8
	.align 8
.LC84:
	.string	"!victim || chunk_is_mmapped (mem2chunk (victim)) || &main_arena == arena_for_chunk (mem2chunk (victim))"
	.align 8
.LC85:
	.string	"!victim || chunk_is_mmapped (mem2chunk (victim)) || ar_ptr == arena_for_chunk (mem2chunk (victim))"
	.text
	.p2align 4,,15
	.globl	__libc_malloc
	.hidden	__libc_malloc
	.type	__libc_malloc, @function
__libc_malloc:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	__malloc_hook(%rip), %rax
	testq	%rax, %rax
	jne	.L1195
	testq	%rdi, %rdi
	js	.L1196
	leaq	23(%rdi), %rax
	xorl	%ebp, %ebp
	cmpq	$31, %rax
	ja	.L1197
.L1147:
	movq	%fs:tcache@tpoff, %rax
	movq	%rdi, %rbx
	testq	%rax, %rax
	je	.L1198
	cmpq	80+mp_(%rip), %rbp
	jb	.L1163
.L1149:
#APP
# 3227 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1152
	leaq	main_arena(%rip), %rdi
	movq	%rbx, %rsi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1153
	movq	-8(%rax), %rax
	testb	$2, %al
	jne	.L1143
	testb	$4, %al
	je	.L1143
	leaq	-16(%r8), %rax
	leaq	main_arena(%rip), %rdx
	andq	$-67108864, %rax
	cmpq	%rdx, (%rax)
	jne	.L1199
.L1143:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1197:
	andq	$-16, %rax
	leaq	-17(%rax), %rbp
	shrq	$4, %rbp
	jmp	.L1147
	.p2align 4,,10
	.p2align 3
.L1198:
	cmpb	$0, %fs:tcache_shutting_down@tpoff
	jne	.L1149
	call	tcache_init.part.6
	cmpq	%rbp, 80+mp_(%rip)
	jbe	.L1149
	movq	%fs:tcache@tpoff, %rax
	testq	%rax, %rax
	je	.L1149
	.p2align 4,,10
	.p2align 3
.L1163:
	leaq	(%rax,%rbp,2), %rcx
	movzwl	(%rcx), %edx
	testw	%dx, %dx
	je	.L1149
	leaq	(%rax,%rbp,8), %rsi
	movq	128(%rsi), %r8
	testb	$15, %r8b
	jne	.L1200
	movq	%r8, %rax
	subl	$1, %edx
	shrq	$12, %rax
	xorq	(%r8), %rax
	movq	%rax, 128(%rsi)
	movw	%dx, (%rcx)
	movq	%r8, %rax
	movq	$0, 8(%r8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1153:
	addq	$8, %rsp
	xorl	%r8d, %r8d
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1195:
	movq	24(%rsp), %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1196:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	$12, %fs:(%rax)
	jmp	.L1143
	.p2align 4,,10
	.p2align 3
.L1152:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rbp
	testq	%rbp, %rbp
	je	.L1154
#APP
# 3235 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1155
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%rbp)
# 0 "" 2
#NO_APP
.L1156:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1161
.L1162:
#APP
# 3248 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1159
	subl	$1, 0(%rbp)
.L1158:
	testq	%r8, %r8
	je	.L1153
	movq	-8(%r8), %rax
	testb	$2, %al
	jne	.L1143
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1160
	leaq	-16(%r8), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1160:
	cmpq	%rbp, %rdx
	je	.L1143
	leaq	__PRETTY_FUNCTION__.12849(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC85(%rip), %rdi
	movl	$3253, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1154:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L1156
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	arena_get2.part.4
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L1157
	testq	%rbp, %rbp
	je	.L1157
	.p2align 4,,10
	.p2align 3
.L1161:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	call	arena_get_retry
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	_int_malloc
	movq	%rax, %r8
.L1157:
	testq	%rbp, %rbp
	je	.L1158
	jmp	.L1162
.L1199:
	leaq	__PRETTY_FUNCTION__.12849(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC84(%rip), %rdi
	movl	$3231, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1200:
	leaq	.LC72(%rip), %rdi
	call	malloc_printerr
.L1155:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	je	.L1156
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L1156
.L1159:
	xorl	%eax, %eax
#APP
# 3248 "malloc.c" 1
	xchgl %eax, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1158
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 3248 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1158
	.size	__libc_malloc, .-__libc_malloc
	.globl	malloc
	.set	malloc,__libc_malloc
	.globl	__malloc
	.set	__malloc,__libc_malloc
	.p2align 4,,15
	.type	malloc_hook_ini, @function
malloc_hook_ini:
	movl	__libc_malloc_initialized(%rip), %eax
	movq	$0, __malloc_hook(%rip)
	testl	%eax, %eax
	js	.L1207
	jmp	__libc_malloc
	.p2align 4,,10
	.p2align 3
.L1207:
	subq	$24, %rsp
	movq	%rdi, 8(%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rdi
	addq	$24, %rsp
	jmp	__libc_malloc
	.size	malloc_hook_ini, .-malloc_hook_ini
	.section	.rodata.str1.8
	.align 8
.LC86:
	.string	"!p || chunk_is_mmapped (mem2chunk (p)) || &main_arena == arena_for_chunk (mem2chunk (p))"
	.align 8
.LC87:
	.string	"!p || chunk_is_mmapped (mem2chunk (p)) || ar_ptr == arena_for_chunk (mem2chunk (p))"
	.text
	.p2align 4,,15
	.type	_mid_memalign, @function
_mid_memalign:
	movq	__memalign_hook(%rip), %rax
	testq	%rax, %rax
	jne	.L1255
	cmpq	$16, %rdi
	jbe	.L1256
	cmpq	$31, %rdi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	ja	.L1257
	movl	$32, %ebx
.L1211:
	movq	%rsi, %rbp
#APP
# 3502 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1215
	leaq	main_arena(%rip), %rdi
	movq	%rsi, %rdx
	movq	%rbx, %rsi
	call	_int_memalign
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1216
	movq	-8(%rax), %rax
	testb	$2, %al
	jne	.L1208
	testb	$4, %al
	je	.L1208
	leaq	-16(%r8), %rax
	leaq	main_arena(%rip), %rdx
	andq	$-67108864, %rax
	cmpq	%rdx, (%rax)
	jne	.L1258
.L1208:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1257:
	movabsq	$-9223372036854775808, %rax
	cmpq	%rax, %rdi
	ja	.L1259
	leaq	-1(%rdi), %rax
	testq	%rdi, %rax
	je	.L1227
	cmpq	$32, %rdi
	movl	$32, %ebx
	je	.L1211
	.p2align 4,,10
	.p2align 3
.L1214:
	addq	%rbx, %rbx
	cmpq	%rbx, %rdi
	ja	.L1214
	jmp	.L1211
	.p2align 4,,10
	.p2align 3
.L1256:
	movq	%rsi, %rdi
	jmp	__libc_malloc
	.p2align 4,,10
	.p2align 3
.L1216:
	xorl	%r8d, %r8d
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L1255:
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1215:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r12
	testq	%r12, %r12
	je	.L1217
#APP
# 3510 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1218
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r12)
# 0 "" 2
#NO_APP
.L1219:
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	_int_memalign
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1224
.L1225:
#APP
# 3521 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1222
	subl	$1, (%r12)
.L1221:
	testq	%r8, %r8
	je	.L1216
	movq	-8(%r8), %rax
	testb	$2, %al
	jne	.L1208
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1223
	leaq	-16(%r8), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1223:
	cmpq	%r12, %rdx
	je	.L1208
	leaq	__PRETTY_FUNCTION__.12988(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC87(%rip), %rdi
	movl	$3524, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1218:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	je	.L1219
	movq	%r12, %rdi
	call	__lll_lock_wait_private
	jmp	.L1219
	.p2align 4,,10
	.p2align 3
.L1259:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	$22, %fs:(%rax)
	jmp	.L1208
.L1217:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L1219
	leaq	32(%rbx,%rbp), %rdi
	xorl	%esi, %esi
	call	arena_get2.part.4
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_int_memalign
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L1220
	testq	%r12, %r12
	je	.L1220
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	call	arena_get_retry
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r12
	call	_int_memalign
	movq	%rax, %r8
.L1220:
	testq	%r12, %r12
	je	.L1221
	jmp	.L1225
.L1222:
	xorl	%eax, %eax
#APP
# 3521 "malloc.c" 1
	xchgl %eax, (%r12)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1221
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 3521 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1221
.L1258:
	leaq	__PRETTY_FUNCTION__.12988(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC86(%rip), %rdi
	movl	$3506, %edx
	call	__malloc_assert
.L1227:
	movq	%rdi, %rbx
	jmp	.L1211
	.size	_mid_memalign, .-_mid_memalign
	.p2align 4,,15
	.type	memalign_hook_ini, @function
memalign_hook_ini:
	subq	$24, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	$0, __memalign_hook(%rip)
	testl	%eax, %eax
	js	.L1263
	movq	24(%rsp), %rdx
	addq	$24, %rsp
	jmp	_mid_memalign
	.p2align 4,,10
	.p2align 3
.L1263:
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	movq	24(%rsp), %rdx
	addq	$24, %rsp
	jmp	_mid_memalign
	.size	memalign_hook_ini, .-memalign_hook_ini
	.p2align 4,,15
	.globl	__libc_free
	.hidden	__libc_free
	.type	__libc_free, @function
__libc_free:
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	__free_hook(%rip), %rax
	testq	%rax, %rax
	jne	.L1284
	testq	%rdi, %rdi
	je	.L1264
	movq	-8(%rdi), %rax
	movq	__libc_errno@gottpoff(%rip), %rbx
	leaq	-16(%rdi), %rsi
	testb	$2, %al
	movl	%fs:(%rbx), %ebp
	jne	.L1285
	cmpq	$0, %fs:tcache@tpoff
	je	.L1286
.L1270:
	testb	$4, %al
	leaq	main_arena(%rip), %rdi
	je	.L1271
	movq	%rsi, %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdi
.L1271:
	xorl	%edx, %edx
	call	_int_free
	movl	%ebp, %fs:(%rbx)
.L1264:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1285:
	movl	52+mp_(%rip), %edx
	testl	%edx, %edx
	jne	.L1268
	cmpq	16+mp_(%rip), %rax
	jbe	.L1268
	cmpq	$33554432, %rax
	jbe	.L1287
.L1268:
	movq	%rsi, %rdi
	call	munmap_chunk
	movl	%ebp, %fs:(%rbx)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1284:
	movq	40(%rsp), %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1287:
	andq	$-8, %rax
	movq	%rax, 16+mp_(%rip)
	addq	%rax, %rax
	movq	%rax, mp_(%rip)
	jmp	.L1268
	.p2align 4,,10
	.p2align 3
.L1286:
	cmpb	$0, %fs:tcache_shutting_down@tpoff
	jne	.L1270
	movq	%rdi, 8(%rsp)
	movq	%rsi, (%rsp)
	call	tcache_init.part.6
	movq	8(%rsp), %rdi
	movq	(%rsp), %rsi
	movq	-8(%rdi), %rax
	jmp	.L1270
	.size	__libc_free, .-__libc_free
	.globl	free
	.set	free,__libc_free
	.globl	__free
	.set	__free,__libc_free
	.section	.rodata.str1.8
	.align 8
.LC88:
	.string	"tcache_thread_shutdown(): unaligned tcache chunk detected"
	.section	.rodata.str1.1
.LC89:
	.string	"a->attached_threads > 0"
	.text
	.p2align 4,,15
	.globl	__malloc_arena_thread_freeres
	.hidden	__malloc_arena_thread_freeres
	.type	__malloc_arena_thread_freeres, @function
__malloc_arena_thread_freeres:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%fs:tcache@tpoff, %rbp
	testq	%rbp, %rbp
	je	.L1289
	movq	$0, %fs:tcache@tpoff
	movb	$1, %fs:tcache_shutting_down@tpoff
	leaq	128(%rbp), %rbx
	leaq	640(%rbp), %r12
	jmp	.L1314
	.p2align 4,,10
	.p2align 3
.L1291:
	movq	%rdi, %rax
	shrq	$12, %rax
	xorq	(%rdi), %rax
	movq	%rax, (%rbx)
	call	__libc_free
.L1314:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1315
	testb	$15, %dil
	je	.L1291
	leaq	.LC88(%rip), %rdi
	call	malloc_printerr
	.p2align 4,,10
	.p2align 3
.L1315:
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.L1314
	movq	%rbp, %rdi
	call	__libc_free
	.p2align 4,,10
	.p2align 3
.L1289:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rbx
	movq	$0, %fs:(%rax)
	testq	%rbx, %rbx
	je	.L1288
#APP
# 1008 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1298
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, free_list_lock(%rip)
# 0 "" 2
#NO_APP
.L1299:
	movq	2176(%rbx), %rax
	testq	%rax, %rax
	je	.L1316
	subq	$1, %rax
	testq	%rax, %rax
	movq	%rax, 2176(%rbx)
	jne	.L1301
	movq	free_list(%rip), %rax
	movq	%rbx, free_list(%rip)
	movq	%rax, 2168(%rbx)
.L1301:
#APP
# 1017 "arena.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1302
	subl	$1, free_list_lock(%rip)
.L1288:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L1298:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, free_list_lock(%rip)
	je	.L1299
	leaq	free_list_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1299
.L1302:
	xorl	%eax, %eax
#APP
# 1017 "arena.c" 1
	xchgl %eax, free_list_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1288
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	free_list_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 1017 "arena.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1288
.L1316:
	leaq	__PRETTY_FUNCTION__.12287(%rip), %rcx
	leaq	.LC18(%rip), %rsi
	leaq	.LC89(%rip), %rdi
	movl	$1011, %edx
	call	__malloc_assert
	.size	__malloc_arena_thread_freeres, .-__malloc_arena_thread_freeres
	.section	.rodata.str1.8
	.align 8
.LC90:
	.string	"!newp || chunk_is_mmapped (mem2chunk (newp)) || ar_ptr == arena_for_chunk (mem2chunk (newp))"
	.text
	.p2align 4,,15
	.globl	__libc_realloc
	.hidden	__libc_realloc
	.type	__libc_realloc, @function
__libc_realloc:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	__realloc_hook(%rip), %rax
	testq	%rax, %rax
	jne	.L1363
	testq	%rsi, %rsi
	movq	%rsi, %r14
	movq	%rdi, %rbx
	jne	.L1319
	testq	%rdi, %rdi
	jne	.L1364
.L1319:
	testq	%rbx, %rbx
	je	.L1365
	movq	-8(%rbx), %rax
	xorl	%r8d, %r8d
	leaq	-16(%rbx), %rbp
	movq	%rax, %r15
	andq	$-8, %r15
	testb	$2, %al
	je	.L1366
.L1322:
	movq	%r15, %rdx
	negq	%rdx
	cmpq	%rbp, %rdx
	jb	.L1324
	testb	$15, %bpl
	jne	.L1324
	testq	%r14, %r14
	js	.L1367
	leaq	23(%r14), %rdx
	movl	$32, %ecx
	movq	%rdx, %r13
	andq	$-16, %r13
	cmpq	$31, %rdx
	cmovbe	%rcx, %r13
	testb	$2, %al
	jne	.L1368
#APP
# 3425 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1330
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	_int_realloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1317
	movq	-8(%rax), %rax
	testb	$2, %al
	jne	.L1317
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	movq	8(%rsp), %r8
	je	.L1331
	leaq	-16(%r12), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1331:
	cmpq	%rdx, %r8
	jne	.L1369
.L1317:
	addq	$24, %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1366:
	cmpq	$0, %fs:tcache@tpoff
	je	.L1370
.L1323:
	testb	$4, %al
	leaq	main_arena(%rip), %r8
	je	.L1322
	movq	%rbp, %rdx
	andq	$-67108864, %rdx
	movq	(%rdx), %r8
	jmp	.L1322
	.p2align 4,,10
	.p2align 3
.L1368:
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	mremap_chunk
	testq	%rax, %rax
	leaq	16(%rax), %r12
	jne	.L1317
	leaq	-8(%r15), %rax
	movq	%rbx, %r12
	cmpq	%r13, %rax
	jnb	.L1317
	movq	%r14, %rdi
	call	__libc_malloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1317
	leaq	-16(%r15), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%rbp, %rdi
	call	munmap_chunk
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1365:
	addq	$24, %rsp
	movq	%r14, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__libc_malloc
	.p2align 4,,10
	.p2align 3
.L1363:
	movq	72(%rsp), %rdx
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1367:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r12d, %r12d
	movl	$12, %fs:(%rax)
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1364:
	call	__libc_free
	xorl	%r12d, %r12d
	jmp	.L1317
	.p2align 4,,10
	.p2align 3
.L1370:
	cmpb	$0, %fs:tcache_shutting_down@tpoff
	jne	.L1323
	call	tcache_init.part.6
	movq	-8(%rbx), %rax
	jmp	.L1323
	.p2align 4,,10
	.p2align 3
.L1330:
#APP
# 3434 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1332
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r8)
# 0 "" 2
#NO_APP
.L1333:
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	_int_realloc
	movq	%rax, %r12
#APP
# 3438 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movq	8(%rsp), %r8
	jne	.L1334
	subl	$1, (%r8)
.L1335:
	testq	%r12, %r12
	je	.L1336
	movq	-8(%r12), %rax
	testb	$2, %al
	jne	.L1317
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1337
	leaq	-16(%r12), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1337:
	cmpq	%rdx, %r8
	je	.L1317
	leaq	__PRETTY_FUNCTION__.12922(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	movl	$3440, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1336:
	movq	%r14, %rdi
	movq	%r8, 8(%rsp)
	call	__libc_malloc
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L1317
	leaq	-8(%r15), %rdx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	8(%rsp), %r8
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%r8, %rdi
	call	_int_free
	jmp	.L1317
.L1334:
	xorl	%eax, %eax
#APP
# 3438 "malloc.c" 1
	xchgl %eax, (%r8)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1335
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r8, %rdi
	movl	$202, %eax
#APP
# 3438 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1335
	.p2align 4,,10
	.p2align 3
.L1332:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r8)
	je	.L1333
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %r8
	jmp	.L1333
.L1324:
	leaq	.LC83(%rip), %rdi
	call	malloc_printerr
.L1369:
	leaq	__PRETTY_FUNCTION__.12922(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC90(%rip), %rdi
	movl	$3429, %edx
	call	__malloc_assert
	.size	__libc_realloc, .-__libc_realloc
	.globl	realloc
	.set	realloc,__libc_realloc
	.globl	__realloc
	.set	__realloc,__libc_realloc
	.p2align 4,,15
	.type	realloc_hook_ini, @function
realloc_hook_ini:
	movl	__libc_malloc_initialized(%rip), %eax
	movq	$0, __malloc_hook(%rip)
	movq	$0, __realloc_hook(%rip)
	testl	%eax, %eax
	js	.L1377
	jmp	__libc_realloc
	.p2align 4,,10
	.p2align 3
.L1377:
	subq	$24, %rsp
	movq	%rsi, 8(%rsp)
	movq	%rdi, (%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rsi
	movq	(%rsp), %rdi
	addq	$24, %rsp
	jmp	__libc_realloc
	.size	realloc_hook_ini, .-realloc_hook_ini
	.p2align 4,,15
	.globl	__libc_memalign
	.hidden	__libc_memalign
	.type	__libc_memalign, @function
__libc_memalign:
	movq	(%rsp), %rdx
	jmp	_mid_memalign
	.size	__libc_memalign, .-__libc_memalign
	.weak	memalign
	.set	memalign,__libc_memalign
	.globl	__memalign
	.set	__memalign,__libc_memalign
	.weak	aligned_alloc
	.set	aligned_alloc,__libc_memalign
	.p2align 4,,15
	.globl	__libc_valloc
	.type	__libc_valloc, @function
__libc_valloc:
	subq	$24, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	%rdi, %rsi
	testl	%eax, %eax
	js	.L1382
.L1380:
	movq	24(%rsp), %rdx
	movq	_dl_pagesize(%rip), %rdi
	addq	$24, %rsp
	jmp	_mid_memalign
	.p2align 4,,10
	.p2align 3
.L1382:
	movq	%rdi, 8(%rsp)
	call	ptmalloc_init.part.0
	movq	8(%rsp), %rsi
	jmp	.L1380
	.size	__libc_valloc, .-__libc_valloc
	.weak	valloc
	.set	valloc,__libc_valloc
	.globl	__valloc
	.set	__valloc,__libc_valloc
	.p2align 4,,15
	.globl	__libc_pvalloc
	.type	__libc_pvalloc, @function
__libc_pvalloc:
	movl	__libc_malloc_initialized(%rip), %eax
	pushq	%rbx
	movq	%rdi, %rbx
	testl	%eax, %eax
	js	.L1390
.L1384:
	movq	_dl_pagesize(%rip), %rdi
	movq	8(%rsp), %rdx
	leaq	-1(%rdi), %rsi
	addq	%rsi, %rbx
	jc	.L1386
	movl	$1, %esi
	subq	%rdi, %rsi
	andq	%rbx, %rsi
	popq	%rbx
	jmp	_mid_memalign
	.p2align 4,,10
	.p2align 3
.L1390:
	call	ptmalloc_init.part.0
	jmp	.L1384
.L1386:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__libc_pvalloc, .-__libc_pvalloc
	.weak	pvalloc
	.set	pvalloc,__libc_pvalloc
	.globl	__pvalloc
	.set	__pvalloc,__libc_pvalloc
	.section	.rodata.str1.8
	.align 8
.LC91:
	.string	"!mem || chunk_is_mmapped (mem2chunk (mem)) || av == arena_for_chunk (mem2chunk (mem))"
	.section	.rodata.str1.1
.LC92:
	.string	"nclears >= 3"
	.text
	.p2align 4,,15
	.globl	__libc_calloc
	.type	__libc_calloc, @function
__libc_calloc:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %rax
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	mulq	%rsi
	movq	%rax, %rbx
	seto	%al
	testq	%rbx, %rbx
	js	.L1444
	movzbl	%al, %eax
	testq	%rax, %rax
	jne	.L1444
	movq	__malloc_hook(%rip), %rax
	testq	%rax, %rax
	jne	.L1447
	cmpq	$0, %fs:tcache@tpoff
	je	.L1448
.L1399:
#APP
# 3605 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1449
	movq	96+main_arena(%rip), %r12
	leaq	main_arena(%rip), %r13
	movq	8(%r12), %rbp
	andq	$-8, %rbp
.L1404:
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	_int_malloc
	testq	%rax, %rax
	movq	%rax, %r8
	je	.L1405
	movq	-8(%rax), %rax
	testb	$2, %al
	je	.L1450
.L1405:
#APP
# 3642 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1451
.L1407:
	testq	%r8, %r8
	je	.L1410
	movq	-8(%r8), %rdx
	movl	perturb_byte(%rip), %eax
	testb	$2, %dl
	je	.L1411
	testl	%eax, %eax
	jne	.L1452
.L1391:
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L1411:
	andq	$-8, %rdx
	testl	%eax, %eax
	je	.L1453
.L1412:
	subq	$8, %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	cmpq	$2, %rax
	jbe	.L1454
	cmpq	$9, %rax
	ja	.L1455
	cmpq	$4, %rax
	movq	$0, (%r8)
	movq	$0, 8(%r8)
	movq	$0, 16(%r8)
	jbe	.L1391
	cmpq	$6, %rax
	movq	$0, 24(%r8)
	movq	$0, 32(%r8)
	jbe	.L1391
	cmpq	$9, %rax
	movq	$0, 40(%r8)
	movq	$0, 48(%r8)
	jne	.L1391
	movq	$0, 56(%r8)
	movq	$0, 64(%r8)
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1450:
	testb	$4, %al
	leaq	main_arena(%rip), %rdx
	je	.L1406
	leaq	-16(%r8), %rax
	andq	$-67108864, %rax
	movq	(%rax), %rdx
.L1406:
	cmpq	%r13, %rdx
	je	.L1405
	leaq	__PRETTY_FUNCTION__.13069(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC91(%rip), %rdi
	movl	$3640, %edx
	call	__malloc_assert
	.p2align 4,,10
	.p2align 3
.L1453:
	leaq	-16(%r8), %rax
	cmpq	%rax, %r12
	jne	.L1412
	cmpq	%rdx, %rbp
	cmovb	%rbp, %rdx
	jmp	.L1412
	.p2align 4,,10
	.p2align 3
.L1455:
	movq	%r8, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	addq	$8, %rsp
	movq	%rax, %r8
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L1444:
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%r8d, %r8d
	movl	$12, %fs:(%rax)
	addq	$8, %rsp
	movq	%r8, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L1447:
	movq	40(%rsp), %rsi
	movq	%rbx, %rdi
	call	*%rax
	xorl	%esi, %esi
	testq	%rax, %rax
	movq	%rbx, %rdx
	movq	%rax, %rdi
	je	.L1410
.L1446:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	memset@PLT
	.p2align 4,,10
	.p2align 3
.L1448:
	cmpb	$0, %fs:tcache_shutting_down@tpoff
	jne	.L1399
	call	tcache_init.part.6
	jmp	.L1399
	.p2align 4,,10
	.p2align 3
.L1449:
	movq	thread_arena@gottpoff(%rip), %rax
	movq	%fs:(%rax), %r13
	testq	%r13, %r13
	je	.L1401
#APP
# 3608 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1402
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%r13)
# 0 "" 2
#NO_APP
.L1403:
	movq	96(%r13), %r12
	leaq	main_arena(%rip), %rax
	movq	8(%r12), %rbp
	andq	$-8, %rbp
	cmpq	%rax, %r13
	je	.L1404
	movq	%r12, %rax
	andq	$-67108864, %rax
	addq	24(%rax), %rax
	subq	%r12, %rax
	cmpq	%rax, %rbp
	cmovb	%rax, %rbp
	jmp	.L1404
	.p2align 4,,10
	.p2align 3
.L1451:
	testq	%r8, %r8
	jne	.L1408
	testq	%r13, %r13
	jne	.L1456
.L1408:
	testq	%r13, %r13
	je	.L1407
#APP
# 3652 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1409
	subl	$1, 0(%r13)
	jmp	.L1407
	.p2align 4,,10
	.p2align 3
.L1456:
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	arena_get_retry
	movq	%rbx, %rsi
	movq	%rax, %rdi
	movq	%rax, %r13
	call	_int_malloc
	movq	%rax, %r8
	jmp	.L1408
	.p2align 4,,10
	.p2align 3
.L1410:
	xorl	%r8d, %r8d
	jmp	.L1391
	.p2align 4,,10
	.p2align 3
.L1452:
	movq	%rbx, %rdx
	xorl	%esi, %esi
	movq	%r8, %rdi
	jmp	.L1446
	.p2align 4,,10
	.p2align 3
.L1401:
	call	get_free_list
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L1403
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	arena_get2.part.4
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.L1403
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	jmp	.L1404
.L1402:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%r13)
	je	.L1403
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L1403
.L1409:
	xorl	%eax, %eax
#APP
# 3652 "malloc.c" 1
	xchgl %eax, 0(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1407
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 3652 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1407
.L1454:
	leaq	__PRETTY_FUNCTION__.13069(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC92(%rip), %rdi
	movl	$3691, %edx
	call	__malloc_assert
	.size	__libc_calloc, .-__libc_calloc
	.weak	calloc
	.set	calloc,__libc_calloc
	.globl	__calloc
	.set	__calloc,__libc_calloc
	.section	.rodata.str1.8
	.align 8
.LC93:
	.string	"(char *) chunk2rawmem (p) + 2 * CHUNK_HDR_SZ <= paligned_mem"
	.align 8
.LC94:
	.string	"(char *) p + size > paligned_mem"
	.text
	.p2align 4,,15
	.globl	__malloc_trim
	.type	__malloc_trim, @function
__malloc_trim:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	%rdi, 24(%rsp)
	testl	%eax, %eax
	js	.L1486
.L1458:
	leaq	main_arena(%rip), %rax
	movl	$0, 20(%rsp)
	movq	%rax, 8(%rsp)
.L1477:
#APP
# 5063 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	movq	8(%rsp), %rcx
	jne	.L1459
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L1460:
	movq	8(%rsp), %rdi
	call	malloc_consolidate
	movq	_dl_pagesize(%rip), %rcx
	cmpq	$1023, %rcx
	ja	.L1461
	movl	%ecx, %eax
	shrl	$4, %eax
	movl	%eax, 16(%rsp)
.L1462:
	movq	8(%rsp), %rax
	leaq	-1(%rcx), %rbp
	leaq	47(%rcx), %r13
	negq	%rcx
	xorl	%edx, %edx
	movl	$1, %r15d
	movq	%rcx, %r12
	leaq	96(%rax), %r14
	jmp	.L1473
	.p2align 4,,10
	.p2align 3
.L1488:
	cmpl	%r15d, 16(%rsp)
	jle	.L1478
.L1467:
	addl	$1, %r15d
	addq	$16, %r14
	cmpl	$128, %r15d
	je	.L1487
.L1473:
	cmpl	$1, %r15d
	jne	.L1488
.L1478:
	movq	24(%r14), %rbx
	cmpq	%rbx, %r14
	jne	.L1472
	jmp	.L1467
	.p2align 4,,10
	.p2align 3
.L1469:
	movq	24(%rbx), %rbx
	cmpq	%rbx, %r14
	je	.L1467
.L1472:
	movq	8(%rbx), %rsi
	andq	$-8, %rsi
	cmpq	%rsi, %r13
	jnb	.L1469
	leaq	(%rbx,%r13), %rdi
	leaq	48(%rbx), %rax
	andq	%r12, %rdi
	cmpq	%rax, %rdi
	jb	.L1489
	leaq	(%rbx,%rsi), %rax
	cmpq	%rax, %rdi
	jnb	.L1490
	movq	%rdi, %rax
	subq	%rbx, %rax
	subq	%rax, %rsi
	cmpq	%rsi, %rbp
	jnb	.L1469
	movl	$4, %edx
	andq	%r12, %rsi
	call	__madvise
	movl	$1, %edx
	jmp	.L1469
.L1487:
	leaq	main_arena(%rip), %rcx
	cmpq	%rcx, 8(%rsp)
	je	.L1491
.L1474:
	orl	%edx, 20(%rsp)
#APP
# 5065 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1475
	movq	8(%rsp), %rax
	subl	$1, (%rax)
.L1476:
	movq	8(%rsp), %rax
	leaq	main_arena(%rip), %rcx
	movq	2160(%rax), %rax
	cmpq	%rcx, %rax
	movq	%rax, 8(%rsp)
	jne	.L1477
	movl	20(%rsp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L1461:
	movq	%rcx, %rbp
	shrq	$6, %rbp
	cmpq	$48, %rbp
	ja	.L1463
	leal	48(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1462
.L1463:
	movq	%rcx, %rbp
	shrq	$9, %rbp
	cmpq	$20, %rbp
	ja	.L1464
	leal	91(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1462
.L1491:
	movq	24(%rsp), %rdi
	movl	%edx, 16(%rsp)
	call	systrim.isra.1.constprop.12
	movl	16(%rsp), %edx
	orl	%eax, %edx
	jmp	.L1474
.L1490:
	leaq	__PRETTY_FUNCTION__.13394(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC94(%rip), %rdi
	movl	$5023, %edx
	call	__malloc_assert
.L1489:
	leaq	__PRETTY_FUNCTION__.13394(%rip), %rcx
	leaq	.LC6(%rip), %rsi
	leaq	.LC93(%rip), %rdi
	movl	$5022, %edx
	call	__malloc_assert
.L1459:
	xorl	%eax, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L1460
	movq	8(%rsp), %rdi
	call	__lll_lock_wait_private
	jmp	.L1460
	.p2align 4,,10
	.p2align 3
.L1464:
	movq	%rcx, %rbp
	shrq	$12, %rbp
	cmpq	$10, %rbp
	ja	.L1465
	leal	110(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1462
.L1475:
	xorl	%eax, %eax
	movq	8(%rsp), %rcx
#APP
# 5065 "malloc.c" 1
	xchgl %eax, (%rcx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1476
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rcx, %rdi
	movl	$202, %eax
#APP
# 5065 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1476
	.p2align 4,,10
	.p2align 3
.L1486:
	call	ptmalloc_init.part.0
	jmp	.L1458
.L1465:
	movq	%rcx, %rbp
	shrq	$15, %rbp
	cmpq	$4, %rbp
	ja	.L1466
	leal	119(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1462
.L1466:
	movq	%rcx, %rbp
	movl	$2, %eax
	shrq	$18, %rbp
	cmpq	$2, %rbp
	cmova	%rax, %rbp
	leal	124(%rbp), %eax
	movl	%eax, 16(%rsp)
	jmp	.L1462
	.size	__malloc_trim, .-__malloc_trim
	.weak	malloc_trim
	.set	malloc_trim,__malloc_trim
	.section	.rodata.str1.8
	.align 8
.LC95:
	.string	"malloc_check_get_size: memory corruption"
	.text
	.p2align 4,,15
	.globl	__malloc_usable_size
	.type	__malloc_usable_size, @function
__malloc_usable_size:
	testq	%rdi, %rdi
	je	.L1502
	movq	-8(%rdi), %rdx
	leaq	-16(%rdi), %rsi
	movq	%rdx, %rax
	andq	$-8, %rdx
	andl	$2, %eax
	cmpl	$1, using_malloc_checking(%rip)
	je	.L1516
	testq	%rax, %rax
	je	.L1501
	leaq	-16(%rdx), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1501:
	testb	$1, -8(%rdi,%rdx)
	leaq	-8(%rdx), %rcx
	cmovne	%rcx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L1502:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1516:
	movq	%rsi, %r8
	movq	%rsi, %rcx
	shrq	$11, %rcx
	shrq	$3, %r8
	xorl	%ecx, %r8d
	movl	$2, %ecx
	cmpb	$1, %r8b
	cmove	%ecx, %r8d
	testq	%rax, %rax
	sete	%al
	movzbl	%al, %eax
	leaq	-1(%rdx,%rax,8), %rax
	movzbl	-16(%rdi,%rax), %edx
	cmpb	%r8b, %dl
	je	.L1497
	testb	%dl, %dl
	je	.L1498
	leaq	16(%rdx), %rcx
	cmpq	%rcx, %rax
	jnb	.L1499
	jmp	.L1498
	.p2align 4,,10
	.p2align 3
.L1500:
	testb	%dl, %dl
	je	.L1498
	leaq	16(%rdx), %rcx
	cmpq	%rax, %rcx
	ja	.L1498
.L1499:
	subq	%rdx, %rax
	movzbl	(%rsi,%rax), %edx
	cmpb	%r8b, %dl
	jne	.L1500
.L1497:
	subq	$16, %rax
	ret
.L1498:
	leaq	.LC95(%rip), %rdi
	subq	$8, %rsp
	call	malloc_printerr
	.size	__malloc_usable_size, .-__malloc_usable_size
	.weak	malloc_usable_size
	.set	malloc_usable_size,__malloc_usable_size
	.p2align 4,,15
	.globl	__libc_mallinfo2
	.hidden	__libc_mallinfo2
	.type	__libc_mallinfo2, @function
__libc_mallinfo2:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$80, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1526
.L1518:
	pxor	%xmm0, %xmm0
	leaq	main_arena(%rip), %rbp
	movq	%rsp, %r12
	xorl	%r14d, %r14d
	movq	%rbp, %rbx
	movaps	%xmm0, (%rsp)
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 48(%rsp)
	movaps	%xmm0, 64(%rsp)
	.p2align 4,,10
	.p2align 3
.L1523:
#APP
# 5205 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1519
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
.L1520:
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	int_mallinfo
#APP
# 5207 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1521
	subl	$1, (%rbx)
.L1522:
	movq	2160(%rbx), %rbx
	cmpq	%rbp, %rbx
	jne	.L1523
	movdqa	(%rsp), %xmm0
	movq	%r13, %rax
	movups	%xmm0, 0(%r13)
	movdqa	16(%rsp), %xmm0
	movups	%xmm0, 16(%r13)
	movdqa	32(%rsp), %xmm0
	movups	%xmm0, 32(%r13)
	movdqa	48(%rsp), %xmm0
	movups	%xmm0, 48(%r13)
	movdqa	64(%rsp), %xmm0
	movups	%xmm0, 64(%r13)
	addq	$80, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1521:
	movl	%r14d, %eax
#APP
# 5207 "malloc.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1522
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 5207 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1522
	.p2align 4,,10
	.p2align 3
.L1519:
	movl	%r14d, %eax
	lock cmpxchgl	%edx, (%rbx)
	je	.L1520
	movq	%rbx, %rdi
	call	__lll_lock_wait_private
	jmp	.L1520
	.p2align 4,,10
	.p2align 3
.L1526:
	call	ptmalloc_init.part.0
	jmp	.L1518
	.size	__libc_mallinfo2, .-__libc_mallinfo2
	.weak	mallinfo2
	.set	mallinfo2,__libc_mallinfo2
	.globl	__mallinfo2
	.set	__mallinfo2,__libc_mallinfo2
	.p2align 4,,15
	.globl	__libc_mallinfo
	.type	__libc_mallinfo, @function
__libc_mallinfo:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$80, %rsp
	movq	%rsp, %rdi
	call	__libc_mallinfo2
	movq	(%rsp), %rax
	movl	%eax, (%rbx)
	movq	8(%rsp), %rax
	movl	%eax, 4(%rbx)
	movq	16(%rsp), %rax
	movl	%eax, 8(%rbx)
	movq	24(%rsp), %rax
	movl	%eax, 12(%rbx)
	movq	32(%rsp), %rax
	movl	%eax, 16(%rbx)
	movq	40(%rsp), %rax
	movl	%eax, 20(%rbx)
	movq	48(%rsp), %rax
	movl	%eax, 24(%rbx)
	movq	56(%rsp), %rax
	movl	%eax, 28(%rbx)
	movq	64(%rsp), %rax
	movl	%eax, 32(%rbx)
	movq	72(%rsp), %rax
	movl	%eax, 36(%rbx)
	addq	$80, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	__libc_mallinfo, .-__libc_mallinfo
	.weak	mallinfo
	.set	mallinfo,__libc_mallinfo
	.globl	__mallinfo
	.set	__mallinfo,__libc_mallinfo
	.section	.rodata.str1.1
.LC96:
	.string	"Arena %d:\n"
.LC97:
	.string	"system bytes     = %10u\n"
.LC98:
	.string	"in use bytes     = %10u\n"
.LC99:
	.string	"Total (incl. mmap):\n"
.LC100:
	.string	"max mmap regions = %10u\n"
.LC101:
	.string	"max mmap bytes   = %10lu\n"
	.text
	.p2align 4,,15
	.globl	__malloc_stats
	.type	__malloc_stats, @function
__malloc_stats:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	movq	56+mp_(%rip), %rbp
	testl	%eax, %eax
	movl	%ebp, %r12d
	js	.L1538
.L1530:
	movq	stderr(%rip), %rax
	leaq	main_arena(%rip), %r13
	xorl	%ebx, %ebx
	leaq	16(%rsp), %r15
	movq	%r13, %r14
	movl	116(%rax), %ecx
	movl	%ecx, %edx
	movl	%ecx, 12(%rsp)
	orl	$2, %edx
	movl	%edx, 116(%rax)
	jmp	.L1536
	.p2align 4,,10
	.p2align 3
.L1539:
	addl	$1, %ebx
.L1536:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, (%r15)
	movaps	%xmm0, 16(%r15)
	movaps	%xmm0, 32(%r15)
	movaps	%xmm0, 48(%r15)
	movaps	%xmm0, 64(%r15)
#APP
# 5259 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L1531
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%r14)
# 0 "" 2
#NO_APP
.L1532:
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	int_mallinfo
	movq	stderr(%rip), %rdi
	leaq	.LC96(%rip), %rsi
	movl	%ebx, %edx
	xorl	%eax, %eax
	call	fprintf
	movl	16(%rsp), %edx
	movq	stderr(%rip), %rdi
	leaq	.LC97(%rip), %rsi
	xorl	%eax, %eax
	call	fprintf
	movl	72(%rsp), %edx
	movq	stderr(%rip), %rdi
	leaq	.LC98(%rip), %rsi
	xorl	%eax, %eax
	call	fprintf
	addl	16(%rsp), %r12d
	addl	72(%rsp), %ebp
#APP
# 5270 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1533
	subl	$1, (%r14)
.L1534:
	movq	2160(%r14), %r14
	cmpq	%r13, %r14
	jne	.L1539
	movq	stderr(%rip), %rcx
	leaq	.LC99(%rip), %rdi
	movl	$20, %edx
	movl	$1, %esi
	call	fwrite@PLT
	movq	stderr(%rip), %rdi
	leaq	.LC97(%rip), %rsi
	movl	%r12d, %edx
	xorl	%eax, %eax
	call	fprintf
	movq	stderr(%rip), %rdi
	leaq	.LC98(%rip), %rsi
	movl	%ebp, %edx
	xorl	%eax, %eax
	call	fprintf
	movl	48+mp_(%rip), %edx
	movq	stderr(%rip), %rdi
	leaq	.LC100(%rip), %rsi
	xorl	%eax, %eax
	call	fprintf
	movq	64+mp_(%rip), %rdx
	movq	stderr(%rip), %rdi
	leaq	.LC101(%rip), %rsi
	xorl	%eax, %eax
	call	fprintf
	movq	stderr(%rip), %rax
	movl	12(%rsp), %ecx
	movl	%ecx, 116(%rax)
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1533:
	xorl	%eax, %eax
#APP
# 5270 "malloc.c" 1
	xchgl %eax, (%r14)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1534
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r14, %rdi
	movl	$202, %eax
#APP
# 5270 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1534
	.p2align 4,,10
	.p2align 3
.L1531:
	xorl	%eax, %eax
	lock cmpxchgl	%edx, (%r14)
	je	.L1532
	movq	%r14, %rdi
	call	__lll_lock_wait_private
	jmp	.L1532
	.p2align 4,,10
	.p2align 3
.L1538:
	call	ptmalloc_init.part.0
	jmp	.L1530
	.size	__malloc_stats, .-__malloc_stats
	.weak	malloc_stats
	.set	malloc_stats,__malloc_stats
	.p2align 4,,15
	.globl	__libc_mallopt
	.hidden	__libc_mallopt
	.type	__libc_mallopt, @function
__libc_mallopt:
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rbp
	movl	%edi, %ebx
	subq	$8, %rsp
	movl	__libc_malloc_initialized(%rip), %eax
	testl	%eax, %eax
	js	.L1564
.L1541:
#APP
# 5420 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1542
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, main_arena(%rip)
# 0 "" 2
#NO_APP
.L1543:
	leaq	main_arena(%rip), %rdi
	call	malloc_consolidate
	leal	8(%rbx), %eax
	cmpl	$9, %eax
	ja	.L1557
	leaq	.L1546(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1546:
	.long	.L1545-.L1546
	.long	.L1547-.L1546
	.long	.L1548-.L1546
	.long	.L1557-.L1546
	.long	.L1549-.L1546
	.long	.L1550-.L1546
	.long	.L1551-.L1546
	.long	.L1552-.L1546
	.long	.L1557-.L1546
	.long	.L1553-.L1546
	.text
	.p2align 4,,10
	.p2align 3
.L1564:
	call	ptmalloc_init.part.0
	jmp	.L1541
	.p2align 4,,10
	.p2align 3
.L1552:
	movq	%rbp, mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	.p2align 4,,10
	.p2align 3
.L1544:
#APP
# 5474 "malloc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L1555
	subl	$1, main_arena(%rip)
.L1540:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1545:
	testl	%ebp, %ebp
	movl	$1, %r8d
	jle	.L1544
	movq	%rbp, 32+mp_(%rip)
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1553:
	xorl	%r8d, %r8d
	cmpq	$160, %rbp
	ja	.L1544
	cmpq	$7, %rbp
	movl	$16, %eax
	jbe	.L1554
	leaq	8(%rbp), %rax
	andq	$-16, %rax
.L1554:
	movq	%rax, global_max_fast(%rip)
	movl	%ebx, %r8d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1549:
	movl	%ebp, 44+mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1547:
	testl	%ebp, %ebp
	movl	$1, %r8d
	jle	.L1544
	movq	%rbp, 24+mp_(%rip)
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1548:
	movl	%ebp, perturb_byte(%rip)
	movl	$1, %r8d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1550:
	xorl	%r8d, %r8d
	cmpq	$33554432, %rbp
	ja	.L1544
	movq	%rbp, 16+mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1551:
	movq	%rbp, 8+mp_(%rip)
	movl	$1, 52+mp_(%rip)
	movl	$1, %r8d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1557:
	movl	$1, %r8d
	jmp	.L1544
	.p2align 4,,10
	.p2align 3
.L1555:
	xorl	%eax, %eax
#APP
# 5474 "malloc.c" 1
	xchgl %eax, main_arena(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1540
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	main_arena(%rip), %rdi
	movl	$202, %eax
#APP
# 5474 "malloc.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1540
	.p2align 4,,10
	.p2align 3
.L1542:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, main_arena(%rip)
	je	.L1543
	leaq	main_arena(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L1543
	.size	__libc_mallopt, .-__libc_mallopt
	.weak	mallopt
	.set	mallopt,__libc_mallopt
	.globl	__mallopt
	.set	__mallopt,__libc_mallopt
	.p2align 4,,15
	.globl	__posix_memalign
	.type	__posix_memalign, @function
__posix_memalign:
	testb	$7, %sil
	movl	$22, %eax
	jne	.L1571
	movq	%rsi, %rax
	shrq	$3, %rax
	leaq	-1(%rax), %rcx
	testq	%rax, %rcx
	jne	.L1568
	testq	%rsi, %rsi
	je	.L1568
	pushq	%rbx
	movq	%rdx, %rcx
	movq	8(%rsp), %rdx
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	_mid_memalign
	testq	%rax, %rax
	je	.L1569
	movq	%rax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L1568:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1571:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1569:
	movl	$12, %eax
	popq	%rbx
	ret
	.size	__posix_memalign, .-__posix_memalign
	.weak	posix_memalign
	.set	posix_memalign,__posix_memalign
	.section	.rodata.str1.1
.LC102:
	.string	"<malloc version=\"1\">\n"
	.text
	.p2align 4,,15
	.globl	__malloc_info
	.type	__malloc_info, @function
__malloc_info:
	testl	%edi, %edi
	jne	.L1575
	movl	__libc_malloc_initialized(%rip), %eax
	pushq	%rbx
	movq	%rsi, %rbx
	testl	%eax, %eax
	js	.L1580
.L1576:
	leaq	.LC102(%rip), %rdi
	movq	%rbx, %rsi
	call	_IO_fputs
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__malloc_info.part.11
	.p2align 4,,10
	.p2align 3
.L1580:
	call	ptmalloc_init.part.0
	jmp	.L1576
.L1575:
	movl	$22, %eax
	ret
	.size	__malloc_info, .-__malloc_info
	.weak	malloc_info
	.set	malloc_info,__malloc_info
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.13394, @object
	.size	__PRETTY_FUNCTION__.13394, 6
__PRETTY_FUNCTION__.13394:
	.string	"mtrim"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.13069, @object
	.size	__PRETTY_FUNCTION__.13069, 14
__PRETTY_FUNCTION__.13069:
	.string	"__libc_calloc"
	.align 8
	.type	__PRETTY_FUNCTION__.12988, @object
	.size	__PRETTY_FUNCTION__.12988, 14
__PRETTY_FUNCTION__.12988:
	.string	"_mid_memalign"
	.align 8
	.type	__PRETTY_FUNCTION__.12922, @object
	.size	__PRETTY_FUNCTION__.12922, 15
__PRETTY_FUNCTION__.12922:
	.string	"__libc_realloc"
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.12114, @object
	.size	__PRETTY_FUNCTION__.12114, 22
__PRETTY_FUNCTION__.12114:
	.string	"remove_from_free_list"
	.local	next_to_use.12122
	.comm	next_to_use.12122,8,8
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11963, @object
	.size	__PRETTY_FUNCTION__.11963, 13
__PRETTY_FUNCTION__.11963:
	.string	"detach_arena"
	.align 8
	.type	__PRETTY_FUNCTION__.12067, @object
	.size	__PRETTY_FUNCTION__.12067, 14
__PRETTY_FUNCTION__.12067:
	.string	"get_free_list"
	.local	narenas_limit.12181
	.comm	narenas_limit.12181,8,8
	.align 8
	.type	__PRETTY_FUNCTION__.12849, @object
	.size	__PRETTY_FUNCTION__.12849, 14
__PRETTY_FUNCTION__.12849:
	.string	"__libc_malloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13362, @object
	.size	__PRETTY_FUNCTION__.13362, 13
__PRETTY_FUNCTION__.13362:
	.string	"_int_realloc"
	.align 8
	.type	__PRETTY_FUNCTION__.12747, @object
	.size	__PRETTY_FUNCTION__.12747, 13
__PRETTY_FUNCTION__.12747:
	.string	"mremap_chunk"
	.align 8
	.type	__PRETTY_FUNCTION__.12735, @object
	.size	__PRETTY_FUNCTION__.12735, 13
__PRETTY_FUNCTION__.12735:
	.string	"munmap_chunk"
	.data
	.align 4
	.type	may_shrink_heap.11292, @object
	.size	may_shrink_heap.11292, 4
may_shrink_heap.11292:
	.long	-1
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11956, @object
	.size	__PRETTY_FUNCTION__.11956, 10
__PRETTY_FUNCTION__.11956:
	.string	"heap_trim"
	.align 8
	.type	__PRETTY_FUNCTION__.13291, @object
	.size	__PRETTY_FUNCTION__.13291, 10
__PRETTY_FUNCTION__.13291:
	.string	"_int_free"
	.align 8
	.type	__PRETTY_FUNCTION__.12690, @object
	.size	__PRETTY_FUNCTION__.12690, 10
__PRETTY_FUNCTION__.12690:
	.string	"sysmalloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13182, @object
	.size	__PRETTY_FUNCTION__.13182, 12
__PRETTY_FUNCTION__.13182:
	.string	"_int_malloc"
	.align 8
	.type	__PRETTY_FUNCTION__.13380, @object
	.size	__PRETTY_FUNCTION__.13380, 14
__PRETTY_FUNCTION__.13380:
	.string	"_int_memalign"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.12287, @object
	.size	__PRETTY_FUNCTION__.12287, 30
__PRETTY_FUNCTION__.12287:
	.string	"__malloc_arena_thread_freeres"
	.section	.tbss,"awT",@nobits
	.align 8
	.type	tcache, @object
	.size	tcache, 8
tcache:
	.zero	8
	.type	tcache_shutting_down, @object
	.size	tcache_shutting_down, 1
tcache_shutting_down:
	.zero	1
	.local	using_malloc_checking
	.comm	using_malloc_checking,4,4
	.local	aligned_heap_area
	.comm	aligned_heap_area,8,8
	.hidden	__libc_malloc_initialized
	.globl	__libc_malloc_initialized
	.data
	.align 4
	.type	__libc_malloc_initialized, @object
	.size	__libc_malloc_initialized, 4
__libc_malloc_initialized:
	.long	-1
	.local	list_lock
	.comm	list_lock,4,4
	.local	free_list
	.comm	free_list,8,8
	.align 8
	.type	narenas, @object
	.size	narenas, 8
narenas:
	.quad	1
	.local	free_list_lock
	.comm	free_list_lock,4,4
	.section	.tbss
	.align 8
	.type	thread_arena, @object
	.size	thread_arena, 8
thread_arena:
	.zero	8
	.local	perturb_byte
	.comm	perturb_byte,4,4
	.weak	__after_morecore_hook
	.bss
	.align 8
	.type	__after_morecore_hook, @object
	.size	__after_morecore_hook, 8
__after_morecore_hook:
	.zero	8
	.weak	__memalign_hook
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__memalign_hook, @object
	.size	__memalign_hook, 8
__memalign_hook:
	.quad	memalign_hook_ini
	.weak	__realloc_hook
	.align 8
	.type	__realloc_hook, @object
	.size	__realloc_hook, 8
__realloc_hook:
	.quad	realloc_hook_ini
	.weak	__malloc_hook
	.align 8
	.type	__malloc_hook, @object
	.size	__malloc_hook, 8
__malloc_hook:
	.quad	malloc_hook_ini
	.weak	__free_hook
	.bss
	.align 8
	.type	__free_hook, @object
	.size	__free_hook, 8
__free_hook:
	.zero	8
	.data
	.align 32
	.type	mp_, @object
	.size	mp_, 112
mp_:
	.quad	131072
	.quad	131072
	.quad	131072
	.quad	8
	.zero	12
	.long	65536
	.zero	32
	.quad	64
	.quad	1032
	.quad	7
	.quad	0
	.section	.data.rel.local
	.align 32
	.type	main_arena, @object
	.size	main_arena, 2200
main_arena:
	.long	0
	.zero	2156
	.quad	main_arena
	.zero	8
	.quad	1
	.zero	16
	.local	global_max_fast
	.comm	global_max_fast,8,8
	.globl	__morecore
	.align 8
	.type	__morecore, @object
	.size	__morecore, 8
__morecore:
	.quad	__default_morecore
	.hidden	__default_morecore
	.hidden	__close_nocancel
	.hidden	__read_nocancel
	.hidden	__open_nocancel
	.hidden	__madvise
	.hidden	memset
	.hidden	_IO_fputs
	.hidden	fprintf
	.hidden	__get_nprocs
	.hidden	__lll_lock_wait_private
	.hidden	__mremap
	.hidden	__munmap
	.hidden	__mprotect
	.hidden	__mmap
	.hidden	abort
	.hidden	fflush
	.hidden	__fxprintf
	.hidden	__libc_message
