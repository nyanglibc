	.text
	.p2align 4,,15
	.globl	__default_morecore
	.hidden	__default_morecore
	.type	__default_morecore, @function
__default_morecore:
	subq	$8, %rsp
	call	__sbrk
	movl	$0, %edx
	cmpq	$-1, %rax
	cmove	%rdx, %rax
	addq	$8, %rsp
	ret
	.size	__default_morecore, .-__default_morecore
	.hidden	__sbrk
