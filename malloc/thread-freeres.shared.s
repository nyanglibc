	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_thread_freeres
	.type	__libc_thread_freeres, @function
__libc_thread_freeres:
	subq	$8, %rsp
	call	__rpc_thread_destroy
	call	__res_thread_freeres
	movq	%fs:16, %rax
	movq	2424(%rax), %rdi
	call	free@PLT
	movq	%fs:16, %rax
	movq	2432(%rax), %rdi
	call	free@PLT
	addq	$8, %rsp
	jmp	__malloc_arena_thread_freeres
	.size	__libc_thread_freeres, .-__libc_thread_freeres
	.hidden	__malloc_arena_thread_freeres
	.hidden	__res_thread_freeres
	.hidden	__rpc_thread_destroy
