	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver _obstack_compat,_obstack@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"memory exhausted"
.LC1:
	.string	"%s\n"
#NO_APP
	.text
	.p2align 4,,15
	.type	print_and_abort, @function
print_and_abort:
	leaq	.LC0(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	subq	$8, %rsp
	movl	$5, %edx
	call	__GI___dcgettext
	leaq	.LC1(%rip), %rsi
	movq	%rax, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	movq	obstack_exit_failure@GOTPCREL(%rip), %rax
	movl	(%rax), %edi
	call	__GI_exit
	.size	print_and_abort, .-print_and_abort
	.p2align 4,,15
	.globl	_obstack_begin
	.type	_obstack_begin, @function
_obstack_begin:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%edx, %edx
	je	.L10
	leal	-1(%rdx), %eax
	negl	%edx
	movslq	%edx, %r12
	movslq	%eax, %r13
.L5:
	testl	%esi, %esi
	movl	$4064, %edx
	movq	%rcx, 56(%rbx)
	cmove	%edx, %esi
	andb	$-2, 80(%rbx)
	movq	%r8, 64(%rbx)
	movslq	%esi, %rdi
	movl	%eax, 48(%rbx)
	movq	%rdi, (%rbx)
	call	*%rcx
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, 8(%rbx)
	jne	.L9
	movq	obstack_alloc_failed_handler@GOTPCREL(%rip), %rax
	call	*(%rax)
.L9:
	movq	(%rbx), %rax
	leaq	16(%rbp,%r13), %rdx
	andq	%rdx, %r12
	addq	%rbp, %rax
	movq	%r12, 16(%rbx)
	movq	%r12, 24(%rbx)
	movq	%rax, 0(%rbp)
	movq	%rax, 32(%rbx)
	movl	$1, %eax
	movq	$0, 8(%rbp)
	andb	$-7, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	$-16, %r12
	movl	$15, %r13d
	movl	$15, %eax
	jmp	.L5
	.size	_obstack_begin, .-_obstack_begin
	.p2align 4,,15
	.globl	_obstack_begin_1
	.type	_obstack_begin_1, @function
_obstack_begin_1:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%edx, %edx
	je	.L18
	leal	-1(%rdx), %eax
	negl	%edx
	movslq	%edx, %r12
	movslq	%eax, %r13
.L13:
	testl	%esi, %esi
	movl	$4064, %edx
	movq	%rcx, 56(%rbx)
	cmove	%edx, %esi
	orb	$1, 80(%rbx)
	movq	%r8, 64(%rbx)
	movslq	%esi, %rsi
	movl	%eax, 48(%rbx)
	movq	%r9, 72(%rbx)
	movq	%rsi, (%rbx)
	movq	%r9, %rdi
	call	*%rcx
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, 8(%rbx)
	jne	.L17
	movq	obstack_alloc_failed_handler@GOTPCREL(%rip), %rax
	call	*(%rax)
.L17:
	movq	(%rbx), %rax
	leaq	16(%rbp,%r13), %rdx
	andq	%rdx, %r12
	addq	%rbp, %rax
	movq	%r12, 16(%rbx)
	movq	%r12, 24(%rbx)
	movq	%rax, 0(%rbp)
	movq	%rax, 32(%rbx)
	movl	$1, %eax
	movq	$0, 8(%rbp)
	andb	$-7, 80(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	$-16, %r12
	movl	$15, %r13d
	movl	$15, %eax
	jmp	.L13
	.size	_obstack_begin_1, .-_obstack_begin_1
	.p2align 4,,15
	.globl	__GI__obstack_newchunk
	.hidden	__GI__obstack_newchunk
	.type	__GI__obstack_newchunk, @function
__GI__obstack_newchunk:
	pushq	%r14
	pushq	%r13
	movslq	%esi, %rax
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	24(%rdi), %rbp
	movq	%rdi, %rbx
	subq	16(%rdi), %rbp
	movq	8(%rdi), %r13
	movq	%rbp, %rdx
	addq	%rbp, %rax
	movq	%rbp, %r12
	sarq	$3, %rdx
	addq	%rdx, %rax
	movslq	48(%rdi), %rdx
	leaq	100(%rax,%rdx), %rbp
	cmpq	%rbp, (%rdi)
	movq	56(%rdi), %rax
	cmovge	(%rdi), %rbp
	testb	$1, 80(%rdi)
	je	.L21
	movq	72(%rdi), %rdi
	movq	%rbp, %rsi
	call	*%rax
	movq	%rax, %r14
.L22:
	testq	%r14, %r14
	jne	.L23
	movq	obstack_alloc_failed_handler@GOTPCREL(%rip), %rax
	call	*(%rax)
.L23:
	movslq	48(%rbx), %rdx
	addq	%r14, %rbp
	movq	%r14, 8(%rbx)
	movq	%r13, 8(%r14)
	movq	%rbp, 32(%rbx)
	movq	%rbp, (%r14)
	movq	%rdx, %rax
	leaq	16(%r14,%rdx), %rbp
	notl	%edx
	movslq	%edx, %rdx
	andq	%rdx, %rbp
	xorl	%edx, %edx
	cmpl	$14, %eax
	jle	.L24
	movq	%r12, %rax
	shrq	$2, %rax
	testq	%rax, %rax
	je	.L25
	movq	16(%rbx), %rcx
	leaq	-4(%rbp,%rax,4), %rdx
	leaq	-4(%rbp), %rsi
	subq	%rbp, %rcx
	.p2align 4,,10
	.p2align 3
.L26:
	movl	(%rcx,%rdx), %eax
	subq	$4, %rdx
	movl	%eax, 4(%rdx)
	cmpq	%rdx, %rsi
	jne	.L26
.L25:
	movq	%r12, %rdx
	andq	$-4, %rdx
.L24:
	cmpq	%r12, %rdx
	jge	.L30
	.p2align 4,,10
	.p2align 3
.L27:
	movq	16(%rbx), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movb	%cl, 0(%rbp,%rdx)
	addq	$1, %rdx
	cmpq	%rdx, %r12
	jne	.L27
.L30:
	movzbl	80(%rbx), %edx
	testb	$2, %dl
	jne	.L29
	movslq	48(%rbx), %rcx
	movq	%rcx, %rax
	leaq	16(%r13,%rcx), %rcx
	notl	%eax
	cltq
	andq	%rcx, %rax
	cmpq	%rax, 16(%rbx)
	je	.L40
.L29:
	movq	%rbp, 16(%rbx)
	addq	%r12, %rbp
	andb	$-3, 80(%rbx)
	movq	%rbp, 24(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rbp, %rdi
	call	*%rax
	movq	%rax, %r14
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L40:
	movq	8(%r13), %rax
	andl	$1, %edx
	movq	%rax, 8(%r14)
	movq	64(%rbx), %rax
	jne	.L41
	movq	%r13, %rdi
	call	*%rax
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L41:
	movq	72(%rbx), %rdi
	movq	%r13, %rsi
	call	*%rax
	jmp	.L29
	.size	__GI__obstack_newchunk, .-__GI__obstack_newchunk
	.globl	_obstack_newchunk
	.set	_obstack_newchunk,__GI__obstack_newchunk
	.p2align 4,,15
	.globl	_obstack_allocated_p
	.type	_obstack_allocated_p, @function
_obstack_allocated_p:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L48
	.p2align 4,,10
	.p2align 3
.L43:
	cmpq	%rax, %rsi
	jbe	.L45
	cmpq	%rsi, (%rax)
	jnb	.L49
.L45:
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.L43
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$1, %eax
	ret
.L48:
	rep ret
	.size	_obstack_allocated_p, .-_obstack_allocated_p
	.p2align 4,,15
	.globl	obstack_free
	.type	obstack_free, @function
obstack_free:
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	8(%rdi), %rsi
	movq	%rdi, %rbx
	testq	%rsi, %rsi
	jne	.L51
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L67:
	movq	72(%rbx), %rdi
	call	*%rax
	orb	$2, 80(%rbx)
	testq	%rbp, %rbp
	movq	%rbp, %rsi
	je	.L52
.L51:
	cmpq	%r12, %rsi
	jnb	.L55
	movq	(%rsi), %rax
	cmpq	%r12, %rax
	jnb	.L66
.L55:
	testb	$1, 80(%rbx)
	movq	8(%rsi), %rbp
	movq	64(%rbx), %rax
	jne	.L67
	movq	%rsi, %rdi
	call	*%rax
	orb	$2, 80(%rbx)
	testq	%rbp, %rbp
	movq	%rbp, %rsi
	jne	.L51
.L52:
	testq	%r12, %r12
	jne	.L58
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L66:
	movq	%r12, 24(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rax, 32(%rbx)
	movq	%rsi, 8(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L58:
	call	__GI_abort
	.size	obstack_free, .-obstack_free
	.globl	_obstack_free
	.set	_obstack_free,obstack_free
	.p2align 4,,15
	.globl	_obstack_memory_used
	.type	_obstack_memory_used, @function
_obstack_memory_used:
	movq	8(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L71
	.p2align 4,,10
	.p2align 3
.L70:
	addl	(%rdx), %eax
	subl	%edx, %eax
	movq	8(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.L70
	rep ret
	.p2align 4,,10
	.p2align 3
.L71:
	rep ret
	.size	_obstack_memory_used, .-_obstack_memory_used
	.globl	_obstack_compat
	.bss
	.align 8
	.type	_obstack_compat, @object
	.size	_obstack_compat, 8
_obstack_compat:
	.zero	8
	.globl	obstack_exit_failure
	.data
	.align 4
	.type	obstack_exit_failure, @object
	.size	obstack_exit_failure, 4
obstack_exit_failure:
	.long	1
	.globl	obstack_alloc_failed_handler
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	obstack_alloc_failed_handler, @object
	.size	obstack_alloc_failed_handler, 8
obstack_alloc_failed_handler:
	.quad	print_and_abort
	.hidden	__fxprintf
