	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_dynarray_finalize
	.hidden	__GI___libc_dynarray_finalize
	.type	__GI___libc_dynarray_finalize, @function
__GI___libc_dynarray_finalize:
	cmpq	$-1, 8(%rdi)
	je	.L19
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %r14
	testq	%r14, %r14
	jne	.L4
	movq	16(%rdi), %rdi
	cmpq	%rdi, %rsi
	je	.L5
	call	free@PLT
.L5:
	movq	$0, 0(%rbp)
	movq	$0, 8(%rbp)
	movl	$1, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	imulq	%r14, %rdx
	movq	%rdx, %rdi
	movq	%rdx, %r12
	call	malloc@PLT
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%r15, %r15
	je	.L1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L7
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	__GI_memcpy@PLT
.L7:
	cmpq	%r13, %rbx
	je	.L8
	movq	%rbx, %rdi
	call	free@PLT
.L8:
	movq	%r15, 0(%rbp)
	movq	%r14, 8(%rbp)
	addq	$8, %rsp
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%eax, %eax
	ret
	.size	__GI___libc_dynarray_finalize, .-__GI___libc_dynarray_finalize
	.globl	__libc_dynarray_finalize
	.set	__libc_dynarray_finalize,__GI___libc_dynarray_finalize
