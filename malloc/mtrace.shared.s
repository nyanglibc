	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	release_libc_mem, @function
release_libc_mem:
	cmpq	$0, mallstream(%rip)
	je	.L1
	jmp	__GI___libc_freeres
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	release_libc_mem, .-release_libc_mem
	.text
	.p2align 4,,15
	.type	lock_and_info, @function
lock_and_info:
	testq	%rdi, %rdi
	pushq	%rbx
	je	.L8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rsi, %rbx
	call	__GI__dl_addr
	movl	$0, %esi
	testl	%eax, %eax
	cmove	%rsi, %rbx
#APP
# 119 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
.L4:
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L4
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%ebx, %ebx
	jmp	.L4
	.size	lock_and_info, .-lock_and_info
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	":"
.LC2:
	.string	"@ %s%s%s[%p] "
.LC3:
	.string	"@ [%p] "
	.text
	.p2align 4,,15
	.type	tr_where, @function
tr_where:
	testq	%rdi, %rdi
	je	.L27
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	testq	%rsi, %rsi
	je	.L13
	movq	16(%rsi), %r15
	leaq	.LC0(%rip), %r12
	testq	%r15, %r15
	je	.L14
	movq	%r15, %rdi
	call	__GI_strlen
	leaq	52(%rax), %rcx
	movq	24(%rbx), %r14
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	cmpq	%r13, %r14
	movb	$40, (%r12)
	ja	.L30
	leaq	1(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	__GI_mempcpy@PLT
	movq	%r13, %rdi
	leaq	3(%rax), %rsi
	movl	$7876651, (%rax)
	subq	%r14, %rdi
.L18:
	movl	$16, %edx
	xorl	%ecx, %ecx
	call	_fitoa_word
	movl	$41, %edx
	movw	%dx, (%rax)
.L14:
	movq	(%rbx), %rdx
	leaq	.LC1(%rip), %rcx
	testq	%rdx, %rdx
	je	.L31
.L17:
	movq	mallstream(%rip), %rdi
	leaq	.LC2(%rip), %rsi
	movq	%r13, %r9
	movq	%r12, %r8
	xorl	%eax, %eax
	call	__GI_fprintf
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	rep ret
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	1(%r12), %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	call	__GI_mempcpy@PLT
	movq	%r14, %rdi
	movl	$7876653, (%rax)
	leaq	3(%rax), %rsi
	subq	%r13, %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L31:
	leaq	.LC0(%rip), %rcx
	movq	%rcx, %rdx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rdi, %rdx
	movq	mallstream(%rip), %rdi
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	__GI_fprintf
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.size	tr_where, .-tr_where
	.section	.rodata.str1.1
.LC4:
	.string	"+ %p %#lx\n"
	.text
	.p2align 4,,15
	.type	tr_memalignhook, @function
tr_memalignhook:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rsp, %rsi
	call	lock_and_info
	movq	__free_hook@GOTPCREL(%rip), %r12
	movq	%rax, %r15
	movq	tr_old_free_hook(%rip), %rax
	movq	__malloc_hook@GOTPCREL(%rip), %rbp
	movq	__realloc_hook@GOTPCREL(%rip), %rcx
	movq	__memalign_hook@GOTPCREL(%rip), %rdx
	movq	%rax, (%r12)
	movq	tr_old_malloc_hook(%rip), %rax
	movq	%rax, 0(%rbp)
	movq	tr_old_realloc_hook(%rip), %rax
	movq	%rax, (%rcx)
	movq	tr_old_memalign_hook(%rip), %rax
	testq	%rax, %rax
	movq	%rax, (%rdx)
	je	.L33
	movq	%r13, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	*%rax
	movq	%rax, %r13
.L34:
	leaq	tr_freehook(%rip), %rax
	leaq	tr_reallochook(%rip), %rcx
	leaq	tr_memalignhook(%rip), %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	movq	%rax, (%r12)
	leaq	tr_mallochook(%rip), %rax
	movq	%rax, 0(%rbp)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	%rcx, (%rax)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
	call	tr_where
	movq	mallstream(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
#APP
# 275 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L35
	subl	$1, lock(%rip)
.L32:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
#APP
# 275 "mtrace.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L32
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 275 "mtrace.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	memalign@PLT
	movq	%rax, %r13
	jmp	.L34
	.size	tr_memalignhook, .-tr_memalignhook
	.p2align 4,,15
	.type	tr_mallochook, @function
tr_mallochook:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rbx, %rdi
	subq	$40, %rsp
	movq	%rsp, %rsi
	call	lock_and_info
	movq	__free_hook@GOTPCREL(%rip), %r12
	movq	%rax, %r15
	movq	tr_old_free_hook(%rip), %rax
	movq	tr_old_realloc_hook(%rip), %rdx
	movq	__realloc_hook@GOTPCREL(%rip), %rcx
	movq	__malloc_hook@GOTPCREL(%rip), %rbp
	movq	%rax, (%r12)
	movq	tr_old_malloc_hook(%rip), %rax
	movq	%rdx, (%rcx)
	movq	tr_old_memalign_hook(%rip), %rdx
	movq	__memalign_hook@GOTPCREL(%rip), %rcx
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	movq	%rdx, (%rcx)
	je	.L39
	movq	%rbx, %rsi
	movq	%r14, %rdi
	call	*%rax
	movq	%rax, %r13
.L40:
	leaq	tr_freehook(%rip), %rax
	leaq	tr_reallochook(%rip), %rcx
	leaq	tr_memalignhook(%rip), %rdi
	movq	%r15, %rsi
	movq	%rax, (%r12)
	leaq	tr_mallochook(%rip), %rax
	movq	%rax, 0(%rbp)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	%rcx, (%rax)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdi, (%rax)
	movq	%rbx, %rdi
	call	tr_where
	movq	mallstream(%rip), %rdi
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rcx
	movq	%r13, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
#APP
# 204 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L41
	subl	$1, lock(%rip)
.L38:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L41:
	xorl	%eax, %eax
#APP
# 204 "mtrace.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L38
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 204 "mtrace.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L39:
	movq	%r14, %rdi
	call	malloc@PLT
	movq	%rax, %r13
	jmp	.L40
	.size	tr_mallochook, .-tr_mallochook
	.section	.rodata.str1.1
.LC5:
	.string	"! %p %#lx\n"
.LC6:
	.string	"- %p\n"
.LC7:
	.string	"< %p\n"
.LC8:
	.string	"> %p %#lx\n"
	.text
	.p2align 4,,15
	.type	tr_reallochook, @function
tr_reallochook:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rdi
	movq	%rdx, %rbx
	subq	$40, %rsp
	movq	%rsp, %rsi
	call	lock_and_info
	movq	__free_hook@GOTPCREL(%rip), %r14
	movq	%rax, %r15
	movq	tr_old_free_hook(%rip), %rax
	movq	__malloc_hook@GOTPCREL(%rip), %rcx
	movq	tr_old_memalign_hook(%rip), %rdx
	movq	%rax, (%r14)
	movq	tr_old_malloc_hook(%rip), %rax
	movq	%rax, (%rcx)
	movq	tr_old_realloc_hook(%rip), %rax
	movq	__realloc_hook@GOTPCREL(%rip), %rcx
	testq	%rax, %rax
	movq	%rax, (%rcx)
	movq	__memalign_hook@GOTPCREL(%rip), %rcx
	movq	%rdx, (%rcx)
	je	.L45
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	*%rax
	movq	%rax, %rbp
.L46:
	leaq	tr_freehook(%rip), %rax
	leaq	tr_mallochook(%rip), %rdx
	leaq	tr_reallochook(%rip), %rcx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rax, (%r14)
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	leaq	tr_memalignhook(%rip), %rdx
	movq	%rcx, (%rax)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
	call	tr_where
	testq	%rbp, %rbp
	movq	mallstream(%rip), %rdi
	je	.L54
	testq	%r12, %r12
	je	.L55
	leaq	.LC7(%rip), %rsi
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	tr_where
	movq	mallstream(%rip), %rdi
	leaq	.LC8(%rip), %rsi
	movq	%r13, %rcx
	movq	%rbp, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
.L49:
#APP
# 248 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L51
	subl	$1, lock(%rip)
.L44:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC4(%rip), %rsi
	movq	%r13, %rcx
	movq	%rbp, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	testq	%r13, %r13
	je	.L48
	leaq	.LC5(%rip), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	.LC6(%rip), %rsi
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__GI_fprintf
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L51:
	xorl	%eax, %eax
#APP
# 248 "mtrace.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L44
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 248 "mtrace.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L45:
	movq	%r13, %rsi
	movq	%r12, %rdi
	call	realloc@PLT
	movq	%rax, %rbp
	jmp	.L46
	.size	tr_reallochook, .-tr_reallochook
	.p2align 4,,15
	.type	tr_freehook, @function
tr_freehook:
	testq	%rdi, %rdi
	je	.L72
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	%rbp, %rdi
	subq	$40, %rsp
	movq	%rsp, %rsi
	call	lock_and_info
	movq	%rbp, %rdi
	movq	%rax, %rsi
	call	tr_where
	movq	mallstream(%rip), %rdi
	leaq	.LC6(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdx
	call	__GI_fprintf
	movq	mallwatch@GOTPCREL(%rip), %rax
	cmpq	(%rax), %rbx
	je	.L75
.L59:
	movq	tr_old_malloc_hook(%rip), %rdx
	movq	__malloc_hook@GOTPCREL(%rip), %r14
	movq	__realloc_hook@GOTPCREL(%rip), %r13
	movq	tr_old_free_hook(%rip), %rax
	movq	__free_hook@GOTPCREL(%rip), %r15
	movq	__memalign_hook@GOTPCREL(%rip), %r12
	movq	%rdx, (%r14)
	movq	tr_old_realloc_hook(%rip), %rdx
	testq	%rax, %rax
	movq	%rax, (%r15)
	movq	%rdx, 0(%r13)
	movq	tr_old_memalign_hook(%rip), %rdx
	movq	%rdx, (%r12)
	je	.L64
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	*%rax
.L65:
	leaq	tr_freehook(%rip), %rax
	movq	%rax, (%r15)
	leaq	tr_mallochook(%rip), %rax
	movq	%rax, (%r14)
	leaq	tr_reallochook(%rip), %rax
	movq	%rax, 0(%r13)
	leaq	tr_memalignhook(%rip), %rax
	movq	%rax, (%r12)
#APP
# 182 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L66
	subl	$1, lock(%rip)
.L56:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L72:
	rep ret
	.p2align 4,,10
	.p2align 3
.L75:
#APP
# 172 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L60
	subl	$1, lock(%rip)
.L61:
#APP
# 174 "mtrace.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L62
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock(%rip)
# 0 "" 2
#NO_APP
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L66:
	xorl	%eax, %eax
#APP
# 182 "mtrace.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L56
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 182 "mtrace.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L64:
	movq	%rbx, %rdi
	call	free@PLT
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L62:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock(%rip)
	je	.L59
	leaq	lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L60:
	xorl	%eax, %eax
#APP
# 172 "mtrace.c" 1
	xchgl %eax, lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L61
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock(%rip), %rdi
	movl	$202, %eax
#APP
# 172 "mtrace.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L61
	.size	tr_freehook, .-tr_freehook
	.p2align 4,,15
	.globl	__GI_tr_break
	.hidden	__GI_tr_break
	.type	__GI_tr_break, @function
__GI_tr_break:
	rep ret
	.size	__GI_tr_break, .-__GI_tr_break
	.globl	tr_break
	.set	tr_break,__GI_tr_break
	.section	.rodata.str1.1
.LC9:
	.string	"/dev/null"
.LC10:
	.string	"wce"
.LC11:
	.string	"= Start\n"
	.text
	.p2align 4,,15
	.globl	mtrace
	.type	mtrace, @function
mtrace:
	cmpq	$0, mallstream(%rip)
	je	.L95
	rep ret
	.p2align 4,,10
	.p2align 3
.L95:
	pushq	%rbp
	pushq	%rbx
	leaq	mallenv(%rip), %rdi
	subq	$8, %rsp
	call	__GI___libc_secure_getenv
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L96
	movl	$512, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L77
.L84:
	leaq	.LC10(%rip), %rsi
	movq	%rbx, %rdi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, mallstream(%rip)
	je	.L83
	xorl	%edx, %edx
	movl	$512, %ecx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	leaq	tr_freehook(%rip), %rbx
	call	__GI__IO_setvbuf
	movq	mallstream(%rip), %rcx
	leaq	.LC11(%rip), %rdi
	movl	$8, %edx
	movl	$1, %esi
	call	__GI_fwrite@PLT
	movq	__free_hook@GOTPCREL(%rip), %rsi
	movq	__malloc_hook@GOTPCREL(%rip), %rcx
	movq	__realloc_hook@GOTPCREL(%rip), %rdx
	movq	(%rsi), %rax
	movq	%rbx, (%rsi)
	leaq	tr_mallochook(%rip), %rsi
	movq	%rax, tr_old_free_hook(%rip)
	movq	(%rcx), %rax
	movq	%rsi, (%rcx)
	leaq	tr_reallochook(%rip), %rcx
	movq	%rax, tr_old_malloc_hook(%rip)
	movq	(%rdx), %rax
	movq	%rcx, (%rdx)
	leaq	tr_memalignhook(%rip), %rdx
	movq	%rax, tr_old_realloc_hook(%rip)
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	%rdx, (%rax)
	movl	added_atexit_handler.9682(%rip), %eax
	movq	%rdi, tr_old_memalign_hook(%rip)
	testl	%eax, %eax
	je	.L97
.L77:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L96:
	movq	mallwatch@GOTPCREL(%rip), %rax
	cmpq	$0, (%rax)
	je	.L77
	movl	$512, %edi
	leaq	.LC9(%rip), %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L84
	jmp	.L77
	.p2align 4,,10
	.p2align 3
.L83:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$1, added_atexit_handler.9682(%rip)
	movq	__dso_handle(%rip), %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	leaq	release_libc_mem(%rip), %rdi
	xorl	%esi, %esi
	jmp	__GI___cxa_atexit
	.size	mtrace, .-mtrace
	.section	.rodata.str1.1
.LC12:
	.string	"= End\n"
	.text
	.p2align 4,,15
	.globl	muntrace
	.type	muntrace, @function
muntrace:
	pushq	%rbx
	movq	mallstream(%rip), %rbx
	testq	%rbx, %rbx
	je	.L98
	movq	__free_hook@GOTPCREL(%rip), %rax
	movq	tr_old_free_hook(%rip), %rdx
	leaq	.LC12(%rip), %rdi
	movq	%rbx, %rcx
	movl	$1, %esi
	movq	$0, mallstream(%rip)
	movq	%rdx, (%rax)
	movq	__malloc_hook@GOTPCREL(%rip), %rax
	movq	tr_old_malloc_hook(%rip), %rdx
	movq	%rdx, (%rax)
	movq	__realloc_hook@GOTPCREL(%rip), %rax
	movq	tr_old_realloc_hook(%rip), %rdx
	movq	%rdx, (%rax)
	movq	tr_old_memalign_hook(%rip), %rdx
	movq	__memalign_hook@GOTPCREL(%rip), %rax
	movq	%rdx, (%rax)
	movl	$6, %edx
	call	__GI_fwrite@PLT
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_IO_new_fclose@PLT
	.p2align 4,,10
	.p2align 3
.L98:
	popq	%rbx
	ret
	.size	muntrace, .-muntrace
	.local	added_atexit_handler.9682
	.comm	added_atexit_handler.9682,4,4
	.local	tr_old_memalign_hook
	.comm	tr_old_memalign_hook,8,8
	.local	tr_old_realloc_hook
	.comm	tr_old_realloc_hook,8,8
	.local	tr_old_malloc_hook
	.comm	tr_old_malloc_hook,8,8
	.local	tr_old_free_hook
	.comm	tr_old_free_hook,8,8
	.comm	mallwatch,8,8
	.local	lock
	.comm	lock,4,4
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	mallenv, @object
	.size	mallenv, 13
mallenv:
	.string	"MALLOC_TRACE"
	.local	mallstream
	.comm	mallstream,8,8
	.hidden	__dso_handle
	.hidden	_fitoa_word
	.hidden	__lll_lock_wait_private
