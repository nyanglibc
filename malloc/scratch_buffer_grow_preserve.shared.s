	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_scratch_buffer_grow_preserve
	.hidden	__GI___libc_scratch_buffer_grow_preserve
	.type	__GI___libc_scratch_buffer_grow_preserve, @function
__GI___libc_scratch_buffer_grow_preserve:
	pushq	%r14
	pushq	%r13
	leaq	16(%rdi), %r14
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	(%rdi), %r12
	movq	%rdi, %rbx
	movq	8(%rdi), %r13
	cmpq	%r14, %r12
	leaq	(%r13,%r13), %rbp
	je	.L11
	cmpq	%rbp, %r13
	ja	.L5
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L12
.L4:
	movq	%rcx, (%rbx)
	movq	%rbp, 8(%rbx)
	movl	$1, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rbp, %rdi
	call	malloc@PLT
	movq	%rax, %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L1
	movq	%rcx, %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	__GI_memcpy@PLT
	movq	%rax, %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L6:
	movq	%r12, %rdi
	call	free@PLT
	movq	%r14, (%rbx)
	movq	$1024, 8(%rbx)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movq	(%rbx), %r12
	jmp	.L6
	.size	__GI___libc_scratch_buffer_grow_preserve, .-__GI___libc_scratch_buffer_grow_preserve
	.globl	__libc_scratch_buffer_grow_preserve
	.set	__libc_scratch_buffer_grow_preserve,__GI___libc_scratch_buffer_grow_preserve
