	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_dynarray_resize
	.hidden	__GI___libc_dynarray_resize
	.type	__GI___libc_dynarray_resize, @function
__GI___libc_dynarray_resize:
	cmpq	%rsi, 8(%rdi)
	jnb	.L23
	movq	%rsi, %rax
	movq	%rdx, %r8
	mulq	%rcx
	jo	.L5
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	cmpq	%rdi, %r8
	je	.L24
	movq	%rax, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r9
	je	.L10
.L9:
	movq	%r9, 16(%rbp)
	movq	%rbx, 8(%rbp)
	movl	$1, %eax
	movq	%rbx, 0(%rbp)
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	%rsi, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	%rax, %rdi
	movq	%rcx, 8(%rsp)
	movq	%r8, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r9
	movq	(%rsp), %r8
	movq	8(%rsp), %rcx
	je	.L10
	testq	%r8, %r8
	je	.L9
	movq	0(%rbp), %rdx
	movq	%r8, %rsi
	movq	%rax, %rdi
	imulq	%rcx, %rdx
	call	__GI_memcpy@PLT
	movq	%rax, %r9
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	jmp	.L1
.L5:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
	xorl	%eax, %eax
	ret
	.size	__GI___libc_dynarray_resize, .-__GI___libc_dynarray_resize
	.globl	__libc_dynarray_resize
	.set	__libc_dynarray_resize,__GI___libc_dynarray_resize
