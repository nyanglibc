	.text
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.globl	__libc_freeres
	.hidden	__libc_freeres
	.type	__libc_freeres, @function
__libc_freeres:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgq	%rdx, already_called.12664(%rip)
	jne	.L32
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpq	$0, __nss_module_freeres@GOTPCREL(%rip)
	je	.L3
	call	__nss_module_freeres@PLT
.L3:
	cmpq	$0, __nss_action_freeres@GOTPCREL(%rip)
	je	.L4
	call	__nss_action_freeres@PLT
.L4:
	cmpq	$0, __nss_database_freeres@GOTPCREL(%rip)
	je	.L5
	call	__nss_database_freeres@PLT
.L5:
	call	_IO_cleanup@PLT
	movq	__start___libc_subfreeres@GOTPCREL(%rip), %rbx
	movq	__stop___libc_subfreeres@GOTPCREL(%rip), %rax
	cmpq	%rax, %rbx
	jnb	.L6
	movq	%rbx, %rdx
	notq	%rdx
	addq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L7:
	call	*(%rbx)
	addq	$8, %rbx
	cmpq	%rbp, %rbx
	jne	.L7
.L6:
	cmpq	$0, __libdl_freeres@GOTPCREL(%rip)
	je	.L8
	call	__libdl_freeres@PLT
.L8:
	cmpq	$0, __libpthread_freeres@GOTPCREL(%rip)
	je	.L9
	call	__libpthread_freeres@PLT
.L9:
	movq	__start___libc_freeres_ptrs@GOTPCREL(%rip), %rbx
	movq	__stop___libc_freeres_ptrs@GOTPCREL(%rip), %rax
	cmpq	%rax, %rbx
	jnb	.L1
	movq	%rbx, %rdx
	notq	%rdx
	addq	%rdx, %rax
	shrq	$3, %rax
	leaq	8(%rbx,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L10:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	free@PLT
	cmpq	%rbp, %rbx
	jne	.L10
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
.L32:
	ret
	.size	__libc_freeres, .-__libc_freeres
	.local	already_called.12664
	.comm	already_called.12664,8,8
	.weak	__stop___libc_freeres_ptrs
	.weak	__start___libc_freeres_ptrs
	.weak	__libpthread_freeres
	.weak	__libdl_freeres
	.weak	__stop___libc_subfreeres
	.weak	__start___libc_subfreeres
	.weak	__nss_database_freeres
	.weak	__nss_action_freeres
	.weak	__nss_module_freeres
	.hidden	__nss_database_freeres
	.hidden	__nss_action_freeres
	.hidden	__nss_module_freeres
