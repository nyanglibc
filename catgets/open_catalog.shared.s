	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___open_catalog
	.hidden	__GI___open_catalog
	.type	__GI___open_catalog, @function
__GI___open_catalog:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	$47, %esi
	subq	$200, %rsp
	movq	%rdi, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rcx, 32(%rsp)
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L2
	testq	%r13, %r13
	jne	.L132
.L2:
	movq	16(%rsp), %rdi
	xorl	%esi, %esi
	xorl	%eax, %eax
	call	__GI___open_nocancel
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L4
	xorl	%r14d, %r14d
.L5:
	leaq	48(%rsp), %rsi
	movl	%ebx, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	js	.L130
	movl	72(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	jne	.L47
	movq	96(%rsp), %rsi
	cmpq	$11, %rsi
	ja	.L48
.L47:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
.L46:
	movl	%ebx, %edi
	call	__GI___close_nocancel
	movq	%r14, %rdi
	call	free@PLT
.L1:
	addq	$200, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L4
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
.L3:
	cmpb	$58, %al
	je	.L6
	movzbl	0(%r13), %ebx
	movq	%r13, %r12
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L7:
	cmpb	$58, %bl
	je	.L10
.L135:
	testb	%bl, %bl
	je	.L10
	cmpb	$37, %bl
	je	.L133
	leaq	1(%rbp), %r13
	cmpq	%r15, %r13
	jnb	.L134
.L38:
	movb	%bl, (%r14,%rbp)
	movzbl	1(%r12), %ebx
	addq	$1, %r12
	movq	%r13, %rbp
	cmpb	$58, %bl
	jne	.L135
.L10:
	leaq	1(%rbp), %rax
	cmpq	%r15, %rax
	jnb	.L136
.L41:
	testq	%rbp, %rbp
	movb	$0, (%r14,%rbp)
	jne	.L42
.L44:
	movzbl	1(%r12), %eax
	leaq	1(%r12), %r13
	testb	%al, %al
	jne	.L3
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r14, %rdi
	movl	$-1, %ebp
	call	free@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L133:
	movzbl	1(%r12), %eax
	leaq	2(%r12), %r13
	cmpb	$78, %al
	je	.L13
	jle	.L137
	cmpb	$108, %al
	je	.L69
	cmpb	$116, %al
	je	.L70
	cmpb	$99, %al
	je	.L68
.L12:
	movzbl	2(%r12), %ebx
	testb	%bl, %bl
	je	.L83
	cmpb	$58, %bl
	movq	%r13, %r12
	jne	.L20
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L138:
	cmpb	$58, %bl
	je	.L87
.L20:
	addq	$1, %r12
	movzbl	(%r12), %ebx
	testb	%bl, %bl
	jne	.L138
.L87:
	xorl	%ebp, %ebp
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L137:
	cmpb	$37, %al
	je	.L15
	cmpb	$76, %al
	jne	.L12
	movq	8(%rsp), %rdi
	call	__GI_strlen
	leaq	0(%rbp,%rax), %r11
	movq	%rax, %rdx
	cmpq	%r15, %r11
	jnb	.L139
.L24:
	movq	8(%rsp), %rsi
	leaq	(%r14,%rbp), %rdi
	movq	%r11, 24(%rsp)
.L127:
	call	__GI_memcpy@PLT
	movq	24(%rsp), %r11
	movzbl	2(%r12), %ebx
	movq	%r13, %r12
	movq	%r11, %rbp
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L134:
	cmpq	$257, %r15
	movl	$257, %eax
	movq	%r14, %rdi
	cmovnb	%r15, %rax
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L43
	movq	%rax, %r14
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L69:
	movq	8(%rsp), %rbx
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L25:
	addq	$1, %rbx
	movzbl	-1(%rbx), %eax
	movb	%al, (%r14,%rbp)
	movzbl	(%rbx), %eax
	movq	%rdx, %rbp
	testb	%al, %al
	setne	%dil
	cmpb	$95, %al
	setne	%sil
	testb	%sil, %dil
	je	.L128
	cmpb	$46, %al
	je	.L128
.L17:
	leaq	1(%rbp), %rdx
	cmpq	%r15, %rdx
	jb	.L25
	cmpq	$257, %r15
	movl	$257, %eax
	movq	%r14, %rdi
	cmovnb	%r15, %rax
	movq	%rdx, 24(%rsp)
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	je	.L43
	movq	%rax, %r14
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L15:
	leaq	1(%rbp), %rdx
	cmpq	%r15, %rdx
	jnb	.L140
.L36:
	movb	$37, (%r14,%rbp)
.L128:
	movzbl	2(%r12), %ebx
	movq	%rdx, %rbp
	movq	%r13, %r12
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L68:
	movq	8(%rsp), %rdx
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L141:
	cmpb	$46, %al
	je	.L86
	movq	%rsi, %rdx
.L19:
	movzbl	1(%rdx), %eax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L141
.L86:
	cmpb	$46, %al
	je	.L33
.L129:
	movzbl	2(%r12), %ebx
	movq	%r13, %r12
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L70:
	movq	8(%rsp), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L142:
	cmpb	$95, %al
	je	.L27
	cmpb	$46, %al
	movq	%rsi, %rdx
	je	.L129
.L18:
	movzbl	1(%rdx), %eax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L142
.L27:
	cmpb	$95, %al
	jne	.L129
	leaq	2(%rdx), %rbx
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L29:
	addq	$1, %rbx
	movzbl	-1(%rbx), %eax
	movb	%al, (%r14,%rbp)
	movzbl	(%rbx), %eax
	movq	%rdx, %rbp
	testb	%al, %al
	je	.L128
	cmpb	$46, %al
	je	.L128
.L30:
	leaq	1(%rbp), %rdx
	cmpq	%r15, %rdx
	jb	.L29
	cmpq	$257, %r15
	movl	$257, %eax
	movq	%r14, %rdi
	cmovnb	%r15, %rax
	movq	%rdx, 24(%rsp)
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	je	.L43
	movq	%rax, %r14
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L13:
	movq	16(%rsp), %rdi
	call	__GI_strlen
	leaq	0(%rbp,%rax), %r11
	movq	%rax, %rdx
	cmpq	%r15, %r11
	jnb	.L143
.L23:
	movq	%r11, 24(%rsp)
	leaq	(%r14,%rbp), %rdi
	movq	16(%rsp), %rsi
	jmp	.L127
	.p2align 4,,10
	.p2align 3
.L6:
	movq	16(%rsp), %rdi
	call	__GI_strlen
	cmpq	%rax, %r15
	movq	%rax, %rdx
	movq	%rax, %rbp
	jbe	.L144
.L8:
	movq	16(%rsp), %rsi
	movq	%r14, %rdi
	movq	%r13, %r12
	call	__GI_memcpy@PLT
	leaq	1(%rbp), %rax
	cmpq	%r15, %rax
	jb	.L41
.L136:
	cmpq	$257, %r15
	movl	$257, %eax
	movq	%r14, %rdi
	cmovnb	%r15, %rax
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L43
	movq	%rax, %r14
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	call	__GI___open_nocancel
	testl	%eax, %eax
	movl	%eax, %ebx
	js	.L44
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L48:
	movq	32(%rsp), %r15
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	%ebx, %r8d
	movl	$2, %ecx
	movl	$1, %edx
	movq	%rsi, 48(%r15)
	call	__GI___mmap
	cmpq	$-1, %rax
	movq	%rax, 40(%r15)
	je	.L49
	movl	$0, (%r15)
.L50:
	movl	(%rax), %edx
	cmpl	$-1778120482, %edx
	jne	.L145
	movl	4(%rax), %edx
	movq	32(%rsp), %rcx
	movq	%rdx, 8(%rcx)
	movl	8(%rax), %ecx
.L60:
	imulq	%rcx, %rdx
	movq	32(%rsp), %rsi
	leaq	12(%rax), %r9
	movq	%rcx, 16(%rsi)
	movq	%r9, 24(%rsi)
	leaq	(%rdx,%rdx,2), %rdi
	xorl	%edx, %edx
	leaq	(%rdi,%rdi), %r11
	cmpq	$2, %rdi
	leaq	12(%rax,%r11,4), %r8
	movq	%r8, 32(%rsi)
	jbe	.L61
	movl	$2, %ecx
	.p2align 4,,10
	.p2align 3
.L62:
	movl	(%r9,%rcx,4), %esi
	cmpq	%rsi, %rdx
	cmovb	%rsi, %rdx
	addq	$3, %rcx
	cmpq	%rcx, %rdi
	ja	.L62
.L61:
	movq	96(%rsp), %rsi
	leaq	12(%rdx,%r11), %rcx
	cmpq	%rcx, %rsi
	jbe	.L63
	leaq	(%r8,%rdx), %rcx
	leaq	-13(%rsi,%r11), %rsi
	cmpb	$0, (%rcx)
	je	.L82
	addq	%rsi, %rdx
	je	.L63
	addq	%rcx, %rdx
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L65:
	cmpq	%rdx, %rcx
	je	.L63
.L64:
	addq	$1, %rcx
	cmpb	$0, (%rcx)
	jne	.L65
.L82:
	xorl	%ebp, %ebp
	jmp	.L46
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	2(%rdx), %rbx
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L34:
	addq	$1, %rbx
	movzbl	-1(%rbx), %eax
	cmpb	$0, (%rbx)
	movb	%al, (%r14,%rbp)
	movq	%rdx, %rbp
	je	.L129
.L35:
	leaq	1(%rbp), %rdx
	cmpq	%r15, %rdx
	jb	.L34
	cmpq	$257, %r15
	movl	$257, %eax
	movq	%r14, %rdi
	cmovnb	%r15, %rax
	movq	%rdx, 24(%rsp)
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	je	.L43
	movq	%rax, %r14
	jmp	.L34
.L83:
	movq	%r13, %r12
	xorl	%ebp, %ebp
	jmp	.L7
.L59:
	movq	%rax, %rdi
	call	free@PLT
.L130:
	movl	$-1, %ebp
	jmp	.L46
.L63:
	movq	32(%rsp), %rcx
	movl	(%rcx), %edx
	testl	%edx, %edx
	jne	.L59
	movq	48(%rcx), %rsi
	movq	%rax, %rdi
	movl	$-1, %ebp
	call	__GI___munmap
	jmp	.L46
.L140:
	cmpq	$257, %r15
	movl	$257, %eax
	movq	%r14, %rdi
	cmovnb	%r15, %rax
	movq	%rdx, 24(%rsp)
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	je	.L43
	movq	%rax, %r14
	jmp	.L36
.L143:
	leaq	256(%rax), %rax
	movq	%r14, %rdi
	movq	%r11, 40(%rsp)
	movq	%rdx, 24(%rsp)
	cmpq	%r15, %rax
	cmovb	%r15, %rax
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	movq	40(%rsp), %r11
	je	.L43
	movq	%rax, %r14
	jmp	.L23
.L139:
	leaq	256(%rax), %rax
	movq	%r14, %rdi
	movq	%r11, 40(%rsp)
	movq	%rdx, 24(%rsp)
	cmpq	%r15, %rax
	cmovb	%r15, %rax
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	movq	40(%rsp), %r11
	je	.L43
	movq	%rax, %r14
	jmp	.L24
.L144:
	leaq	256(%rax), %rax
	movq	%r14, %rdi
	movq	%rdx, 24(%rsp)
	cmpq	%r15, %rax
	cmovb	%r15, %rax
	addq	%rax, %r15
	movq	%r15, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	24(%rsp), %rdx
	je	.L43
	movq	%rax, %r14
	jmp	.L8
.L4:
	xorl	%r14d, %r14d
	jmp	.L43
.L49:
	movq	96(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%rbp, %r12
	call	malloc@PLT
	movq	32(%rsp), %rcx
	testq	%rax, %rax
	movq	%rax, 40(%rcx)
	je	.L130
.L56:
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movl	%ebx, %edi
	subq	%r12, %rsi
	addq	%rax, %rsi
	call	__GI___read_nocancel
	leaq	-1(%rax), %rdx
	cmpq	$-3, %rdx
	jbe	.L52
	cmpq	$-1, %rax
	jne	.L53
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	je	.L54
.L53:
	movq	32(%rsp), %rax
	movl	$-1, %ebp
	movq	40(%rax), %rdi
	call	free@PLT
	jmp	.L46
.L52:
	subq	%rax, %r12
.L54:
	movq	32(%rsp), %rax
	testq	%r12, %r12
	movq	40(%rax), %rax
	je	.L55
	movq	96(%rsp), %rbp
	jmp	.L56
.L145:
	cmpl	$-569899882, %edx
	jne	.L63
	movl	4(%rax), %edx
	movq	32(%rsp), %rcx
	bswap	%edx
	movl	%edx, %edx
	movq	%rdx, 8(%rcx)
	movl	8(%rax), %ecx
	bswap	%ecx
	movl	%ecx, %ecx
	jmp	.L60
.L55:
	movq	32(%rsp), %rcx
	movl	$1, (%rcx)
	jmp	.L50
	.size	__GI___open_catalog, .-__GI___open_catalog
	.globl	__open_catalog
	.set	__open_catalog,__GI___open_catalog
