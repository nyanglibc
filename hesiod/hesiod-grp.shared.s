	.text
	.p2align 4,,15
	.type	lookup, @function
lookup:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	movq	%rcx, %rbx
	movq	%r9, %r13
	subq	$40, %rsp
	movq	errno@gottpoff(%rip), %rax
	leaq	24(%rsp), %rdi
	movq	%rsi, (%rsp)
	movl	%fs:(%rax), %r15d
	call	hesiod_init
	testl	%eax, %eax
	js	.L5
	movq	(%rsp), %rsi
	movq	24(%rsp), %rdi
	movq	%rsi, %rdx
	movq	%r12, %rsi
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L10
	movq	(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	strlen@PLT
	leaq	1(%rax), %rdx
	movq	24(%rsp), %rax
	movq	8(%rsp), %rsi
	cmpq	%rdx, %rbp
	movq	%rax, (%rsp)
	jb	.L11
	movq	%rbx, %rdi
	call	memcpy@PLT
	movq	(%rsp), %rdi
	movq	%r12, %rsi
	call	hesiod_free_list
	movq	24(%rsp), %rdi
	call	hesiod_end
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	%rbp, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_nss_files_parse_grent@PLT
	testl	%eax, %eax
	movl	$1, %edx
	jle	.L12
.L1:
	addq	$40, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rcx
	xorl	%edx, %edx
	cmpl	$-1, %eax
	setne	%dl
	leal	-2(%rdx,%rdx), %edx
	movl	%r15d, %fs:(%rcx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	errno@gottpoff(%rip), %rax
	movq	24(%rsp), %rdi
	movl	%fs:(%rax), %ebx
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	xorl	%edx, %edx
	cmpl	$2, %ebx
	setne	%dl
	movl	%r15d, %fs:(%rax)
	negl	%edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	hesiod_free_list
	movq	24(%rsp), %rdi
	call	hesiod_end
	movl	$34, 0(%r13)
	movl	$-2, %edx
	jmp	.L1
	.size	lookup, .-lookup
	.p2align 4,,15
	.globl	_nss_hesiod_setgrent
	.type	_nss_hesiod_setgrent, @function
_nss_hesiod_setgrent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_setgrent, .-_nss_hesiod_setgrent
	.p2align 4,,15
	.globl	_nss_hesiod_endgrent
	.type	_nss_hesiod_endgrent, @function
_nss_hesiod_endgrent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_endgrent, .-_nss_hesiod_endgrent
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"group"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getgrnam_r
	.type	_nss_hesiod_getgrnam_r, @function
_nss_hesiod_getgrnam_r:
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	jmp	lookup
	.size	_nss_hesiod_getgrnam_r, .-_nss_hesiod_getgrnam_r
	.section	.rodata.str1.1
.LC1:
	.string	"%d"
.LC2:
	.string	"gid"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getgrgid_r
	.type	_nss_hesiod_getgrgid_r, @function
_nss_hesiod_getgrgid_r:
	pushq	%r14
	pushq	%r13
	xorl	%eax, %eax
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	leaq	.LC1(%rip), %rdx
	movq	%rsi, %rbp
	movq	%rcx, %r13
	movl	$21, %esi
	movl	%edi, %ecx
	subq	$32, %rsp
	movq	%r8, %r14
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	snprintf@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	call	lookup
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	_nss_hesiod_getgrgid_r, .-_nss_hesiod_getgrgid_r
	.section	.rodata.str1.1
.LC3:
	.string	"grplist"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_initgroups_dyn
	.type	_nss_hesiod_initgroups_dyn, @function
_nss_hesiod_initgroups_dyn:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$120, %rsp
	movq	(%r8), %r12
	leaq	88(%rsp), %rdi
	movq	%rdx, 8(%rsp)
	movq	%rcx, 24(%rsp)
	movq	%r8, 56(%rsp)
	movq	%r9, 40(%rsp)
	call	hesiod_init
	testl	%eax, %eax
	js	.L56
	movq	88(%rsp), %rdi
	leaq	.LC3(%rip), %rdx
	movq	%rbx, %rsi
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L128
	movq	errno@gottpoff(%rip), %rbx
	leaq	104(%rsp), %rcx
	leaq	96(%rsp), %rbp
	movq	%rcx, 48(%rsp)
	movl	%fs:(%rbx), %eax
	movl	%eax, 36(%rsp)
	movq	16(%rsp), %rax
	movq	(%rax), %r15
	movzbl	(%r15), %eax
	movq	%r15, %r14
	testb	%al, %al
	je	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	cmpb	$58, %al
	movq	%r14, %r13
	jne	.L126
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L24:
	addq	$1, %r13
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L23
	cmpb	$58, %al
	je	.L23
.L126:
	cmpb	$44, %al
	jne	.L24
.L25:
	movb	$0, 0(%r13)
	addq	$1, %r13
.L26:
	movl	$10, %edx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	movl	$0, %fs:(%rbx)
	call	strtol@PLT
	movl	%eax, %edx
	cmpq	%rax, %rdx
	je	.L129
.L27:
	movzbl	0(%r13), %eax
	movq	%r13, %r14
	testb	%al, %al
	jne	.L21
.L22:
	movl	36(%rsp), %eax
	movl	%eax, %fs:(%rbx)
.L51:
	movq	16(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	hesiod_free_list
	movq	88(%rsp), %rdi
	call	hesiod_end
	movl	$1, %eax
.L18:
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	testb	%al, %al
	je	.L26
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L129:
	movl	%fs:(%rbx), %eax
	testl	%eax, %eax
	jne	.L27
	movq	96(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L60
	cmpq	%r14, %rax
	je	.L60
.L43:
	movq	8(%rsp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jle	.L44
	cmpl	(%r12), %edx
	je	.L27
	leaq	-1(%rsi), %rcx
	xorl	%eax, %eax
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$1, %rax
	cmpl	%edx, (%r12,%rax,4)
	je	.L27
.L45:
	cmpq	%rax, %rcx
	jne	.L46
.L44:
	movq	24(%rsp), %rax
	cmpq	(%rax), %rsi
	je	.L130
.L53:
	movq	8(%rsp), %rcx
	leaq	1(%rsi), %rax
	movq	%rax, (%rcx)
	movl	%edx, (%r12,%rsi,4)
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L60:
	movq	88(%rsp), %r15
	leaq	.LC0(%rip), %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L27
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.L27
	movzbl	(%rax), %edx
	cmpb	$58, %dl
	je	.L30
	testb	%dl, %dl
	je	.L30
	.p2align 4,,10
	.p2align 3
.L31:
	addq	$1, %rax
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L30
	cmpb	$58, %dl
	jne	.L31
.L30:
	testb	%dl, %dl
	je	.L131
	movzbl	1(%rax), %edx
	leaq	1(%rax), %rsi
	testb	%dl, %dl
	jne	.L127
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$1, %rsi
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	je	.L33
.L127:
	cmpb	$58, %dl
	jne	.L132
.L33:
	testb	%dl, %dl
	je	.L40
.L36:
	movzbl	1(%rsi), %eax
	leaq	1(%rsi), %r9
	movq	%r9, %r8
	cmpb	$58, %al
	je	.L37
	testb	%al, %al
	jne	.L38
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L133:
	cmpb	$58, %al
	je	.L37
.L38:
	addq	$1, %r8
	movzbl	(%r8), %eax
	testb	%al, %al
	jne	.L133
.L37:
	movq	48(%rsp), %rsi
	movl	$10, %edx
	movq	%r9, %rdi
	movq	%r8, 72(%rsp)
	movq	%r9, 64(%rsp)
	call	strtol@PLT
	movl	%eax, %edx
	movq	64(%rsp), %r9
	movq	72(%rsp), %r8
	cmpq	%rdx, %rax
	je	.L134
.L40:
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	hesiod_free_list
	jmp	.L27
.L134:
	cmpq	%r8, 104(%rsp)
	jne	.L40
	cmpq	%r8, %r9
	je	.L40
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%eax, 64(%rsp)
	call	hesiod_free_list
	movl	64(%rsp), %edx
	jmp	.L43
.L128:
	movq	88(%rsp), %rdi
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	cmpl	$2, %fs:(%rax)
	setne	%al
	addq	$120, %rsp
	movzbl	%al, %eax
	popq	%rbx
	negl	%eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L131:
	movzbl	(%rax), %edx
	movq	%rax, %rsi
	testb	%dl, %dl
	jne	.L36
	jmp	.L40
.L130:
	movq	40(%rsp), %rax
	testq	%rax, %rax
	jle	.L47
	cmpq	%rax, %rsi
	je	.L51
	movq	40(%rsp), %rax
	addq	%rsi, %rsi
	cmpq	%rax, %rsi
	cmovg	%rax, %rsi
	movq	%rsi, %r15
.L50:
	leaq	0(,%r15,4), %rsi
	movq	%r12, %rdi
	movl	%edx, 64(%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L51
	movq	56(%rsp), %rax
	movl	64(%rsp), %edx
	movq	%r12, (%rax)
	movq	24(%rsp), %rax
	movq	%r15, (%rax)
	movq	8(%rsp), %rax
	movq	(%rax), %rsi
	jmp	.L53
.L56:
	movl	$-1, %eax
	jmp	.L18
.L47:
	leaq	(%rsi,%rsi), %r15
	jmp	.L50
	.size	_nss_hesiod_initgroups_dyn, .-_nss_hesiod_initgroups_dyn
	.hidden	hesiod_end
	.hidden	hesiod_free_list
	.hidden	hesiod_resolve
	.hidden	hesiod_init
