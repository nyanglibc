	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"#\n"
	.text
	.p2align 4,,15
	.globl	_nss_files_parse_protoent
	.type	_nss_files_parse_protoent, @function
_nss_files_parse_protoent:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	leaq	(%rdx,%rcx), %rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$40, %rsp
	cmpq	%rdi, %rbp
	movq	%r8, 8(%rsp)
	jbe	.L29
	cmpq	%rdi, %rdx
	jbe	.L64
.L29:
	movq	%r15, (%rsp)
.L2:
	leaq	.LC0(%rip), %rsi
	movq	%r14, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L3
	movb	$0, (%rax)
.L3:
	movq	%r14, (%r12)
	movzbl	(%r14), %ebx
	testb	%bl, %bl
	je	.L30
	call	__ctype_b_loc@PLT
	movq	(%rax), %rsi
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	testb	%bl, %bl
	movq	%r13, %r14
	je	.L4
.L5:
	movsbq	%bl, %rdx
	leaq	1(%r14), %r13
	movsbq	1(%r14), %rbx
	testb	$32, 1(%rsi,%rdx,2)
	je	.L6
	movb	$0, (%r14)
	movq	%r14, %r13
	movq	(%rax), %rax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L8:
	movsbq	1(%r13), %rbx
.L26:
	addq	$1, %r13
	testb	$32, 1(%rax,%rbx,2)
	jne	.L8
.L4:
	leaq	24(%rsp), %rsi
	movl	$10, %edx
	movq	%r13, %rdi
	call	strtoul@PLT
	movq	24(%rsp), %rbx
	movl	$4294967295, %edx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	cmpq	%r13, %rbx
	movl	%eax, 16(%r12)
	je	.L9
	call	__ctype_b_loc@PLT
	movsbq	(%rbx), %rdx
	movq	(%rax), %rcx
	movq	%rax, %r13
	testb	$32, 1(%rcx,%rdx,2)
	je	.L10
	leaq	1(%rbx), %rax
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rax, %rbx
	movq	%rax, 24(%rsp)
	addq	$1, %rax
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L11
.L12:
	cmpq	$0, (%rsp)
	je	.L65
.L14:
	movq	(%rsp), %r14
	addq	$7, %r14
	andq	$-8, %r14
	leaq	16(%r14), %r9
	movq	%r14, %rdi
.L16:
	cmpq	%r9, %rbp
	jb	.L66
.L17:
	movsbq	(%rbx), %rax
	testb	%al, %al
	je	.L18
	movq	0(%r13), %rcx
	testb	$32, 1(%rcx,%rax,2)
	je	.L19
	.p2align 4,,10
	.p2align 3
.L20:
	addq	$1, %rbx
	movsbq	(%rbx), %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L20
	testb	%dl, %dl
	je	.L16
.L19:
	movq	%rbx, %rdx
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L61:
	testb	$32, 1(%rcx,%rax,2)
	jne	.L23
	movq	%rsi, %rdx
.L22:
	movsbq	1(%rdx), %rax
	leaq	1(%rdx), %rsi
	testb	%al, %al
	jne	.L61
	cmpq	%rbx, %rsi
	ja	.L27
	cmpq	%r9, %rbp
	movq	%rsi, %rbx
	jnb	.L17
.L66:
	movq	8(%rsp), %rax
	movl	$34, (%rax)
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	testb	%dl, %dl
	je	.L12
.L9:
	xorl	%eax, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L64:
	call	strlen@PLT
	leaq	1(%r14,%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L23:
	cmpq	%rbx, %rsi
	jbe	.L67
.L27:
	movq	%rbx, (%rdi)
	cmpb	$0, 1(%rdx)
	leaq	8(%rdi), %rax
	je	.L32
.L28:
	leaq	2(%rdx), %rbx
	movb	$0, (%rsi)
	movq	%rax, %rdi
	leaq	16(%rax), %r9
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L18:
	testq	%r14, %r14
	movq	$0, (%rdi)
	je	.L33
	movq	%r14, 8(%r12)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rax, %rdi
	movq	%rsi, %rbx
	leaq	16(%rax), %r9
	jmp	.L16
.L65:
	cmpq	%rbx, %r15
	ja	.L31
	cmpq	%rbx, %rbp
	ja	.L68
.L31:
	movq	%r15, (%rsp)
	jmp	.L14
.L30:
	movq	%r14, %r13
	jmp	.L4
.L68:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %rax
	movq	%rax, (%rsp)
	jmp	.L14
.L33:
	movl	$-1, %eax
	jmp	.L1
.L67:
	movq	%rdi, %rax
	jmp	.L28
	.size	_nss_files_parse_protoent, .-_nss_files_parse_protoent
	.p2align 4,,15
	.type	lookup, @function
lookup:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r15
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r13
	movq	%rcx, %rbx
	movq	%r8, %rbp
	movq	%r9, %r12
	subq	$56, %rsp
	movq	errno@gottpoff(%rip), %rax
	leaq	40(%rsp), %rdi
	movl	%fs:(%rax), %eax
	movl	%eax, 20(%rsp)
	call	hesiod_init
	testl	%eax, %eax
	js	.L77
	movq	40(%rsp), %rdi
	movq	%r15, %rdx
	movq	%r14, %rsi
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L81
	movq	(%rax), %rsi
	movq	%rbp, %r14
	movq	%rax, 24(%rsp)
.L75:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	strlen@PLT
	leaq	1(%rax), %rdx
	movq	8(%rsp), %rsi
	cmpq	%rdx, %r14
	jb	.L82
	movq	%rbx, %rdi
	call	memcpy@PLT
	movq	%r12, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	_nss_files_parse_protoent@PLT
	cmpl	$-1, %eax
	jne	.L73
	movq	40(%rsp), %rdi
	movq	%r15, %rsi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movl	$-2, %eax
.L69:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L81:
	movq	errno@gottpoff(%rip), %rax
	movq	40(%rsp), %rdi
	movl	%fs:(%rax), %ebx
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	movl	20(%rsp), %ecx
	movl	%ecx, %fs:(%rax)
	xorl	%eax, %eax
	cmpl	$2, %ebx
	setne	%al
	addq	$56, %rsp
	popq	%rbx
	negl	%eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	testl	%eax, %eax
	jle	.L83
	movq	40(%rsp), %rdi
	movq	%r15, %rsi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movl	$1, %eax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L83:
	addq	$8, 24(%rsp)
	movq	24(%rsp), %rax
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L75
	movq	40(%rsp), %rdi
	movq	%r15, %rsi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	movl	20(%rsp), %ecx
	movl	%ecx, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L82:
	movq	40(%rsp), %rdi
	movq	%r15, %rsi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movl	$34, (%r12)
	movl	$-2, %eax
	jmp	.L69
	.p2align 4,,10
	.p2align 3
.L77:
	movl	$-1, %eax
	jmp	.L69
	.size	lookup, .-lookup
	.p2align 4,,15
	.globl	_nss_hesiod_setprotoent
	.type	_nss_hesiod_setprotoent, @function
_nss_hesiod_setprotoent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_setprotoent, .-_nss_hesiod_setprotoent
	.p2align 4,,15
	.globl	_nss_hesiod_endprotoent
	.type	_nss_hesiod_endprotoent, @function
_nss_hesiod_endprotoent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_endprotoent, .-_nss_hesiod_endprotoent
	.section	.rodata.str1.1
.LC1:
	.string	"protocol"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getprotobyname_r
	.type	_nss_hesiod_getprotobyname_r, @function
_nss_hesiod_getprotobyname_r:
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	.LC1(%rip), %rsi
	jmp	lookup
	.size	_nss_hesiod_getprotobyname_r, .-_nss_hesiod_getprotobyname_r
	.section	.rodata.str1.1
.LC2:
	.string	"%d"
.LC3:
	.string	"protonum"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getprotobynumber_r
	.type	_nss_hesiod_getprotobynumber_r, @function
_nss_hesiod_getprotobynumber_r:
	pushq	%r14
	pushq	%r13
	xorl	%eax, %eax
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	leaq	.LC2(%rip), %rdx
	movq	%rsi, %rbp
	movq	%rcx, %r13
	movl	$21, %esi
	movl	%edi, %ecx
	subq	$32, %rsp
	movq	%r8, %r14
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	snprintf@PLT
	leaq	.LC3(%rip), %rsi
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	call	lookup
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	_nss_hesiod_getprotobynumber_r, .-_nss_hesiod_getprotobynumber_r
	.hidden	hesiod_end
	.hidden	hesiod_free_list
	.hidden	hesiod_resolve
	.hidden	hesiod_init
