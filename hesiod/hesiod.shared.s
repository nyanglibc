	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"lhs"
.LC2:
	.string	"rhs"
.LC3:
	.string	"classes"
.LC4:
	.string	"IN"
.LC5:
	.string	"HS"
	.text
	.p2align 4,,15
	.type	parse_config_file, @function
parse_config_file:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$1064, %rsp
	movq	8(%rdi), %rdi
	call	free@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movabsq	$17179869185, %rax
	leaq	.LC0(%rip), %rsi
	movq	$0, 0(%r13)
	movq	$0, 8(%r13)
	movq	%rax, 16(%r13)
	movq	%rbx, %rdi
	call	fopen@PLT
	testq	%rax, %rax
	movq	%rax, (%rsp)
	je	.L36
	leaq	16(%rsp), %rbp
	movabsq	$2305843013508661760, %r12
	movabsq	$4294976512, %rbx
	.p2align 4,,10
	.p2align 3
.L5:
	movq	(%rsp), %rdx
	movl	$1032, %esi
	movq	%rbp, %rdi
	call	fgets@PLT
	testq	%rax, %rax
	je	.L56
	movzbl	16(%rsp), %eax
	cmpb	$35, %al
	jbe	.L57
	movq	%rbp, %r15
.L4:
	cmpb	$61, %al
	movq	%r15, %rsi
	jbe	.L53
	.p2align 4,,10
	.p2align 3
.L10:
	addq	$1, %rsi
	movzbl	(%rsi), %eax
	cmpb	$61, %al
	ja	.L10
.L53:
	btq	%rax, %r12
	jnc	.L10
	movzbl	1(%rsi), %eax
	leaq	1(%rsi), %r14
	movb	$0, (%rsi)
	cmpb	$61, %al
	jbe	.L58
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%r14, %rax
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$1, %rax
	movzbl	(%rax), %esi
	cmpb	$32, %sil
	ja	.L16
	btq	%rsi, %rbx
	jnc	.L16
.L18:
	leaq	.LC1(%rip), %rsi
	movb	$0, (%rax)
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L20
	movq	%r13, %r15
.L21:
	movq	%r14, %rdi
	call	strdup@PLT
	testq	%rax, %rax
	movq	%rax, (%r15)
	jne	.L5
.L23:
	movq	(%rsp), %rdi
	call	fclose@PLT
	movq	8(%r13), %rdi
	call	free@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movq	$0, 0(%r13)
	movq	$0, 8(%r13)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L58:
	btq	%rax, %r12
	jnc	.L13
.L59:
	addq	$1, %r14
	movzbl	(%r14), %eax
	cmpb	$61, %al
	ja	.L17
	btq	%rax, %r12
	jc	.L59
.L13:
	cmpb	$32, %al
	ja	.L17
	btq	%rax, %rbx
	jnc	.L17
	movq	%r14, %rax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L57:
	movabsq	$34359747584, %rcx
	btq	%rax, %rcx
	jc	.L5
	cmpb	$32, %al
	je	.L41
	cmpb	$9, %al
	movq	%rbp, %r15
	jne	.L4
.L41:
	movq	%rbp, %r15
	.p2align 4,,10
	.p2align 3
.L47:
	addq	$1, %r15
	movzbl	(%r15), %eax
	cmpb	$32, %al
	je	.L47
	cmpb	$9, %al
	je	.L47
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC2(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	jne	.L22
	leaq	8(%r13), %r15
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	.LC3(%rip), %rsi
	movq	%r15, %rdi
	call	strcasecmp@PLT
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L5
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L60:
	movslq	%r15d, %rax
	addl	$1, %r15d
	movl	$1, 16(%r13,%rax,4)
.L29:
	movq	%r8, %r14
.L25:
	cmpb	$0, (%r14)
	je	.L30
	cmpl	$1, %r15d
	jg	.L32
	movl	$44, %esi
	movq	%r14, %rdi
	call	strchrnul@PLT
	cmpb	$0, (%rax)
	movq	%rax, %r8
	je	.L27
	movb	$0, (%rax)
	addq	$1, %r8
.L27:
	leaq	.LC4(%rip), %rsi
	movq	%r14, %rdi
	movq	%r8, 8(%rsp)
	call	strcasecmp@PLT
	testl	%eax, %eax
	movq	8(%rsp), %r8
	je	.L60
	leaq	.LC5(%rip), %rsi
	movq	%r14, %rdi
	movq	%r8, 8(%rsp)
	call	strcasecmp@PLT
	testl	%eax, %eax
	movq	8(%rsp), %r8
	jne	.L29
	movslq	%r15d, %rax
	addl	$1, %r15d
	movl	$4, 16(%r13,%rax,4)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rsp), %rdi
	call	fclose@PLT
	xorl	%eax, %eax
.L1:
	addq	$1064, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L30:
	testl	%r15d, %r15d
	jne	.L33
	movabsq	$17179869185, %rax
	movq	%rax, 16(%r13)
	jmp	.L5
.L33:
	cmpl	$1, %r15d
	je	.L34
.L32:
	movl	20(%r13), %eax
	cmpl	%eax, 16(%r13)
	jne	.L5
.L34:
	movl	$0, 20(%r13)
	jmp	.L5
.L36:
	movl	$-1, %eax
	jmp	.L1
	.size	parse_config_file, .-parse_config_file
	.p2align 4,,15
	.type	get_txt_records.isra.0, @function
get_txt_records.isra.0:
	pushq	%r15
	pushq	%r14
	movl	%edi, %edx
	pushq	%r13
	pushq	%r12
	xorl	%r9d, %r9d
	pushq	%rbp
	pushq	%rbx
	xorl	%r8d, %r8d
	movl	$16, %ecx
	subq	$2096, %rsp
	movl	%edi, 20(%rsp)
	pushq	$1024
	xorl	%edi, %edi
	leaq	48(%rsp), %rbx
	pushq	%rbx
	pushq	$0
	call	__res_mkquery@PLT
	addq	$32, %rsp
	testl	%eax, %eax
	js	.L65
	leaq	1056(%rsp), %r15
	movl	$1024, %ecx
	movl	%eax, %esi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	call	__res_send@PLT
	testl	%eax, %eax
	js	.L121
	cmpl	$11, %eax
	jle	.L65
	movzwl	1060(%rsp), %ebp
	movzwl	1062(%rsp), %r12d
	cltq
	leaq	(%r15,%rax), %rbx
	rolw	$8, %bp
	rolw	$8, %r12w
	movzwl	%bp, %ebp
	testl	%ebp, %ebp
	je	.L90
	xorl	%r13d, %r13d
	addq	$12, %r15
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L122:
	cltq
	leaq	4(%r15,%rax), %r15
	cmpq	%r15, %rbx
	jb	.L65
	addl	$1, %r13d
	cmpl	%r13d, %ebp
	je	.L66
.L67:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	__dn_skipname@PLT
	testl	%eax, %eax
	jns	.L122
.L65:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r13d, %r13d
	movl	$90, %fs:(%rax)
.L61:
	addq	$2088, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L90:
	addq	$12, %r15
	.p2align 4,,10
	.p2align 3
.L66:
	movzwl	%r12w, %r14d
	leal	1(%r14), %edi
	movl	%r14d, 16(%rsp)
	movslq	%edi, %rdi
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L61
	testl	%r14d, %r14d
	je	.L86
	xorl	%r12d, %r12d
	movl	$0, 20(%rsp)
	jmp	.L68
	.p2align 4,,10
	.p2align 3
.L75:
	addl	$1, %r12d
	cmpl	%r12d, 16(%rsp)
	je	.L123
.L68:
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	__dn_skipname@PLT
	testl	%eax, %eax
	js	.L73
	cltq
	leaq	(%r15,%rax), %rbp
	leaq	10(%rbp), %r15
	cmpq	%r15, %rbx
	jb	.L73
	movq	%rbp, %rdi
	call	ns_get16@PLT
	leaq	2(%rbp), %rdi
	movl	%eax, %r14d
	call	ns_get16@PLT
	leaq	8(%rbp), %rdi
	movl	%eax, (%rsp)
	call	ns_get16@PLT
	testl	%eax, %eax
	movq	%r15, %r8
	je	.L73
	movslq	%eax, %r9
	leaq	(%r15,%r9), %r15
	cmpq	%r15, %rbx
	jb	.L73
	cmpl	$16, %r14d
	jne	.L75
	movl	(%rsp), %edx
	cmpl	%edx, 12(%rsp)
	jne	.L75
	movq	%r9, %rdi
	movq	%r8, 24(%rsp)
	movq	%r9, (%rsp)
	call	malloc@PLT
	movslq	20(%rsp), %rsi
	testq	%rax, %rax
	movq	%rsi, %rdx
	movq	%rax, 0(%r13,%rsi,8)
	je	.L71
	movq	(%rsp), %r9
	movq	24(%rsp), %r8
	addl	$1, %edx
	movl	%edx, 20(%rsp)
	addq	%rax, %r9
	cmpq	%r15, %r8
	jnb	.L92
	movzbl	10(%rbp), %r10d
	leaq	11(%rbp), %rsi
	leaq	(%rsi,%r10), %r8
	cmpq	%r8, %rbx
	jb	.L120
	leaq	(%rax,%r10), %r11
	cmpq	%r11, %r9
	jnb	.L78
	jmp	.L120
	.p2align 4,,10
	.p2align 3
.L126:
	andl	$4, %r10d
	jne	.L124
	testl	%ecx, %ecx
	je	.L81
	movzbl	(%rsi), %edi
	testb	$2, %cl
	movb	%dil, (%rax)
	jne	.L125
.L81:
	cmpq	%r8, %r15
	jbe	.L76
	movzbl	(%r8), %r10d
	leaq	1(%r8), %rsi
	leaq	(%rsi,%r10), %r8
	cmpq	%r8, %rbx
	jb	.L120
	leaq	(%r11,%r10), %rcx
	movq	%r11, %rax
	cmpq	%rcx, %r9
	jb	.L120
	movq	%rcx, %r11
.L78:
	cmpl	$8, %r10d
	movl	%r10d, %ecx
	jb	.L126
	movq	(%rsi), %rcx
	movq	%rcx, (%rax)
	movl	%r10d, %ecx
	movq	-8(%rsi,%rcx), %rdi
	movq	%rdi, -8(%rax,%rcx)
	leaq	8(%rax), %rdi
	andq	$-8, %rdi
	subq	%rdi, %rax
	leal	(%r10,%rax), %ecx
	subq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	jmp	.L81
	.p2align 4,,10
	.p2align 3
.L73:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
.L71:
	movl	20(%rsp), %eax
	testl	%eax, %eax
	je	.L69
.L89:
	movl	20(%rsp), %eax
	movq	%r13, %rbx
	subl	$1, %eax
	leaq	8(%r13,%rax,8), %rbp
	.p2align 4,,10
	.p2align 3
.L88:
	movq	(%rbx), %rdi
	addq	$8, %rbx
	call	free@PLT
	cmpq	%rbx, %rbp
	jne	.L88
.L69:
	movq	%r13, %rdi
	xorl	%r13d, %r13d
	call	free@PLT
	jmp	.L61
	.p2align 4,,10
	.p2align 3
.L120:
	movq	errno@gottpoff(%rip), %rax
	movl	$90, %fs:(%rax)
	jmp	.L89
.L92:
	movq	%rax, %r11
	.p2align 4,,10
	.p2align 3
.L76:
	cmpq	%r8, %r15
	jne	.L120
	movb	$0, (%r11)
	jmp	.L75
.L121:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r13d, %r13d
	movl	$111, %fs:(%rax)
	jmp	.L61
.L124:
	movl	(%rsi), %edi
	movl	%edi, (%rax)
	movl	-4(%rsi,%rcx), %esi
	movl	%esi, -4(%rax,%rcx)
	jmp	.L81
.L123:
	movslq	20(%rsp), %rax
	testl	%eax, %eax
	movq	$0, 0(%r13,%rax,8)
	jne	.L61
.L86:
	movq	errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	jmp	.L69
.L125:
	movzwl	-2(%rsi,%rcx), %esi
	movw	%si, -2(%rax,%rcx)
	jmp	.L81
	.size	get_txt_records.isra.0, .-get_txt_records.isra.0
	.p2align 4,,15
	.globl	hesiod_end
	.hidden	hesiod_end
	.type	hesiod_end, @function
hesiod_end:
	pushq	%r12
	pushq	%rbp
	movq	errno@gottpoff(%rip), %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	8(%rdi), %rdi
	movl	%fs:0(%rbp), %r12d
	call	free@PLT
	movq	(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	movl	%r12d, %fs:0(%rbp)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	hesiod_end, .-hesiod_end
	.section	.rodata.str1.1
.LC6:
	.string	"/etc/hesiod.conf"
.LC7:
	.string	"HESIOD_CONFIG"
.LC8:
	.string	"HES_DOMAIN"
	.text
	.p2align 4,,15
	.globl	hesiod_init
	.hidden	hesiod_init
	.type	hesiod_init, @function
hesiod_init:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	$24, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L137
	movq	%rax, %rbx
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	leaq	.LC7(%rip), %rdi
	movabsq	$17179869185, %rax
	movq	%rax, 16(%rbx)
	call	__libc_secure_getenv@PLT
	leaq	.LC6(%rip), %rdx
	testq	%rax, %rax
	movq	%rbx, %rdi
	cmove	%rdx, %rax
	movq	%rax, %rsi
	call	parse_config_file
	testl	%eax, %eax
	js	.L136
	leaq	.LC8(%rip), %rdi
	call	__libc_secure_getenv@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L133
	movq	8(%rbx), %rdi
	call	free@PLT
	movq	%rbp, %rdi
	call	strlen@PLT
	leaq	2(%rax), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	je	.L136
	cmpb	$46, 0(%rbp)
	je	.L142
	leaq	1(%rax), %rdi
	movb	$46, (%rax)
	movq	%rbp, %rsi
	call	strcpy@PLT
.L135:
	movq	%rbx, (%r12)
	xorl	%eax, %eax
.L129:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L133:
	cmpq	$0, 8(%rbx)
	jne	.L135
	movq	errno@gottpoff(%rip), %rax
	movl	$8, %fs:(%rax)
.L136:
	movq	%rbx, %rdi
	call	hesiod_end
	movl	$-1, %eax
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L142:
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	strcpy@PLT
	jmp	.L135
	.p2align 4,,10
	.p2align 3
.L137:
	movl	$-1, %eax
	jmp	.L129
	.size	hesiod_init, .-hesiod_init
	.p2align 4,,15
	.globl	hesiod_free_list
	.hidden	hesiod_free_list
	.type	hesiod_free_list, @function
hesiod_free_list:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	je	.L144
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L145:
	addq	$8, %rbx
	call	free@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L145
.L144:
	addq	$8, %rsp
	movq	%rbp, %rdi
	popq	%rbx
	popq	%rbp
	jmp	free@PLT
	.size	hesiod_free_list, .-hesiod_free_list
	.section	.rodata.str1.1
.LC9:
	.string	"rhs-extension"
	.text
	.p2align 4,,15
	.globl	hesiod_to_bind
	.hidden	hesiod_to_bind
	.type	hesiod_to_bind, @function
hesiod_to_bind:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rbp, %rdi
	movl	$64, %esi
	subq	$24, %rsp
	movq	%rdx, (%rsp)
	call	strchr@PLT
	testq	%rax, %rax
	je	.L152
	leaq	1(%rax), %r12
	movl	$46, %esi
	movq	%rax, %rbx
	xorl	%r14d, %r14d
	movq	%r12, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L176
.L153:
	movq	(%rsp), %rdi
	subq	%rbp, %rbx
	call	strlen@PLT
	movq	%r12, %rdi
	movq	%rax, %r15
	call	strlen@PLT
	movq	0(%r13), %r8
	leaq	4(%r15,%rax), %rdi
	leaq	(%rdi,%rbx), %r15
	testq	%r8, %r8
	je	.L156
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	strlen@PLT
	leaq	(%rax,%r15), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movq	8(%rsp), %r8
	je	.L162
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	movq	%r8, 8(%rsp)
	call	__mempcpy@PLT
	movq	(%rsp), %rsi
	leaq	1(%rax), %rdi
	movb	$46, (%rax)
	call	__stpcpy@PLT
	movq	8(%rsp), %r8
	cmpb	$46, (%r8)
	je	.L160
	movb	$46, (%rax)
	addq	$1, %rax
.L160:
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	__stpcpy@PLT
	movq	%rax, %rdi
.L161:
	cmpb	$46, (%r12)
	je	.L158
	movb	$46, (%rdi)
	addq	$1, %rdi
.L158:
	movq	%r12, %rsi
	call	strcpy@PLT
	testq	%r14, %r14
	je	.L151
.L175:
	movq	%r14, %rsi
	movq	%r13, %rdi
	call	hesiod_free_list
.L151:
	addq	$24, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L173
.L162:
	xorl	%r15d, %r15d
	testq	%r14, %r14
	jne	.L175
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L152:
	movq	%rbp, %rdi
	movq	8(%r13), %r12
	xorl	%r14d, %r14d
	call	strlen@PLT
	leaq	0(%rbp,%rax), %rbx
	jmp	.L153
	.p2align 4,,10
	.p2align 3
.L173:
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	__mempcpy@PLT
	movq	(%rsp), %rsi
	leaq	1(%rax), %rdi
	movb	$46, (%rax)
	call	__stpcpy@PLT
	movq	%rax, %rdi
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L176:
	leaq	.LC9(%rip), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L154
	movq	(%rax), %r12
	jmp	.L153
.L154:
	movq	errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	jmp	.L151
	.size	hesiod_to_bind, .-hesiod_to_bind
	.p2align 4,,15
	.globl	hesiod_resolve
	.hidden	hesiod_resolve
	.type	hesiod_resolve, @function
hesiod_resolve:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	call	hesiod_to_bind
	testq	%rax, %rax
	je	.L181
	movl	16(%rbp), %edi
	movq	%rax, %rsi
	movq	%rax, %rbx
	call	get_txt_records.isra.0
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L190
.L179:
	movq	%rbx, %rdi
	call	free@PLT
.L177:
	movq	%r12, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L190:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	cmpl	$2, %eax
	je	.L182
	cmpl	$111, %eax
	jne	.L179
.L182:
	movl	20(%rbp), %edi
	testl	%edi, %edi
	je	.L179
	movq	%rbx, %rsi
	call	get_txt_records.isra.0
	movq	%rax, %r12
	jmp	.L179
	.p2align 4,,10
	.p2align 3
.L181:
	xorl	%r12d, %r12d
	jmp	.L177
	.size	hesiod_resolve, .-hesiod_resolve
