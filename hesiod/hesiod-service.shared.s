	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"#\n"
	.text
	.p2align 4,,15
	.globl	_nss_files_parse_servent
	.type	_nss_files_parse_servent, @function
_nss_files_parse_servent:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	leaq	(%rdx,%rcx), %rbp
	pushq	%rbx
	movq	%rsi, %r12
	subq	$40, %rsp
	cmpq	%rdi, %rbp
	movq	%r8, 8(%rsp)
	jbe	.L37
	cmpq	%rdi, %rdx
	jbe	.L95
.L37:
	movq	%r15, %r14
.L2:
	leaq	.LC0(%rip), %rsi
	movq	%r13, %rdi
	call	strpbrk@PLT
	testq	%rax, %rax
	je	.L3
	movb	$0, (%rax)
.L3:
	movq	%r13, (%r12)
	movsbq	0(%r13), %rbx
	testb	%bl, %bl
	je	.L4
	call	__ctype_b_loc@PLT
	cmpb	$59, %bl
	je	.L92
	movq	(%rax), %rcx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L7:
	movsbq	1(%r13), %rbx
	movq	%rdx, %r13
	testb	%bl, %bl
	je	.L4
	cmpb	$59, %bl
	je	.L92
.L6:
	testb	$32, 1(%rcx,%rbx,2)
	leaq	1(%r13), %rdx
	je	.L7
.L5:
	movb	$0, 0(%r13)
	movq	(%rax), %rcx
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %rdx
.L33:
	movsbq	(%rdx), %rax
	movq	%rdx, %r13
	cmpb	$59, %al
	je	.L8
	testb	$32, 1(%rcx,%rax,2)
	jne	.L8
.L4:
	movq	%r13, 24(%r12)
	movsbq	0(%r13), %rbx
	testb	%bl, %bl
	je	.L9
	call	__ctype_b_loc@PLT
	cmpb	$59, %bl
	je	.L93
	movq	(%rax), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	movsbq	1(%r13), %rbx
	movq	%rdx, %r13
	testb	%bl, %bl
	je	.L9
	cmpb	$59, %bl
	je	.L93
.L11:
	testb	$32, 1(%rcx,%rbx,2)
	leaq	1(%r13), %rdx
	je	.L12
.L10:
	movb	$0, 0(%r13)
	movq	(%rax), %rcx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L13:
	addq	$1, %rdx
.L34:
	movsbq	(%rdx), %rax
	movq	%rdx, %r13
	cmpb	$59, %al
	je	.L13
	testb	$32, 1(%rcx,%rax,2)
	jne	.L13
.L9:
	leaq	24(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r13, %rdi
	call	strtoul@PLT
	movl	$4294967295, %edx
	movq	24(%rsp), %rbx
	cmpq	%rdx, %rax
	cmova	%rdx, %rax
	rolw	$8, %ax
	cmpq	%r13, %rbx
	movzwl	%ax, %eax
	movl	%eax, 16(%r12)
	je	.L14
	movzbl	(%rbx), %ecx
	movb	%cl, 7(%rsp)
	call	__ctype_b_loc@PLT
	movzbl	7(%rsp), %ecx
	movq	%rax, %r13
	movq	(%rax), %rdx
	cmpb	$59, %cl
	je	.L17
	movsbq	%cl, %rax
	testb	$32, 1(%rdx,%rax,2)
	jne	.L17
	testb	%cl, %cl
	jne	.L14
	testq	%r14, %r14
	je	.L96
	.p2align 4,,10
	.p2align 3
.L21:
	addq	$7, %r14
	andq	$-8, %r14
	leaq	16(%r14), %r9
	movq	%r14, %r8
.L23:
	cmpq	%r9, %rbp
	jb	.L97
.L24:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L25
	movq	0(%r13), %rcx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$1, %rbx
	movzbl	(%rbx), %eax
.L26:
	movsbq	%al, %rdx
	testb	$32, 1(%rcx,%rdx,2)
	jne	.L27
	testb	%al, %al
	je	.L23
	movq	%rbx, %rdx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L90:
	testb	$32, 1(%rcx,%rax,2)
	jne	.L30
	movq	%rdi, %rdx
.L29:
	movsbq	1(%rdx), %rax
	leaq	1(%rdx), %rdi
	testb	%al, %al
	jne	.L90
	cmpq	%rdi, %rbx
	jnb	.L98
.L35:
	movq	%rbx, (%r8)
	cmpb	$0, 1(%rdx)
	leaq	8(%r8), %rax
	movq	%rdi, %rbx
	movq	%rax, %r8
	leaq	16(%rax), %r9
	je	.L23
	leaq	16(%rax), %r9
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$1, %rbx
	movq	%rbx, 24(%rsp)
	movsbq	(%rbx), %rax
	cmpb	$59, %al
	je	.L17
	testb	$32, 1(%rdx,%rax,2)
	jne	.L17
	testq	%r14, %r14
	jne	.L21
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L30:
	cmpq	%rdi, %rbx
	jb	.L35
.L36:
	cmpq	%r9, %rbp
	leaq	2(%rdx), %rbx
	movb	$0, (%rdi)
	jnb	.L24
.L97:
	movq	8(%rsp), %rax
	movl	$34, (%rax)
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L95:
	call	strlen@PLT
	leaq	1(%r13,%rax), %r14
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	leaq	1(%r13), %rdx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L92:
	leaq	1(%r13), %rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L25:
	testq	%r14, %r14
	movq	$0, (%r8)
	je	.L44
	movq	%r14, 8(%r12)
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L96:
	cmpq	%rbx, %r15
	ja	.L42
	cmpq	%rbx, %rbp
	ja	.L99
.L42:
	movq	%r15, %r14
	jmp	.L21
.L98:
	movq	%rdi, %rbx
	jmp	.L23
.L99:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rbx,%rax), %r14
	jmp	.L21
.L44:
	movl	$-1, %eax
	jmp	.L1
	.size	_nss_files_parse_servent, .-_nss_files_parse_servent
	.p2align 4,,15
	.type	lookup, @function
lookup:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rcx, %r12
	movq	%r8, %rbp
	movq	%r9, %r13
	subq	$56, %rsp
	movq	errno@gottpoff(%rip), %rax
	leaq	40(%rsp), %rdi
	movl	%fs:(%rax), %eax
	movl	%eax, 28(%rsp)
	call	hesiod_init
	testl	%eax, %eax
	js	.L110
	movq	40(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	movq	%r13, 8(%rsp)
	leaq	8(%rax), %rbx
	je	.L119
	.p2align 4,,10
	.p2align 3
.L103:
	movq	-8(%rbx), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	leaq	1(%rax), %rdx
	cmpq	%rdx, 8(%rsp)
	jb	.L120
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	memcpy@PLT
	movq	112(%rsp), %r8
	movq	%r13, %rcx
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	call	_nss_files_parse_servent@PLT
	cmpl	$-1, %eax
	je	.L121
	testl	%eax, %eax
	jle	.L118
	testq	%r14, %r14
	je	.L109
	movq	24(%r12), %rdi
	movq	%r14, %rsi
	call	strcasecmp@PLT
	testl	%eax, %eax
	je	.L109
.L118:
	cmpq	$0, (%rbx)
	je	.L122
	addq	$8, %rbx
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L109:
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rdi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movl	$1, %eax
.L100:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L120:
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rdi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movq	112(%rsp), %rax
	movl	$34, (%rax)
	addq	$56, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rdi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	movl	28(%rsp), %ecx
	movl	%ecx, %fs:(%rax)
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movq	16(%rsp), %rsi
	movq	40(%rsp), %rdi
	call	hesiod_free_list
	movq	40(%rsp), %rdi
	call	hesiod_end
	addq	$56, %rsp
	movl	$-2, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L119:
	movq	errno@gottpoff(%rip), %rax
	movq	40(%rsp), %rdi
	movl	%fs:(%rax), %ebx
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	movl	28(%rsp), %ecx
	movl	%ecx, %fs:(%rax)
	xorl	%eax, %eax
	cmpl	$2, %ebx
	setne	%al
	addq	$56, %rsp
	popq	%rbx
	negl	%eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L110:
	movl	$-1, %eax
	jmp	.L100
	.size	lookup, .-lookup
	.p2align 4,,15
	.globl	_nss_hesiod_setservent
	.type	_nss_hesiod_setservent, @function
_nss_hesiod_setservent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_setservent, .-_nss_hesiod_setservent
	.p2align 4,,15
	.globl	_nss_hesiod_endservent
	.type	_nss_hesiod_endservent, @function
_nss_hesiod_endservent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_endservent, .-_nss_hesiod_endservent
	.section	.rodata.str1.1
.LC1:
	.string	"service"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getservbyname_r
	.type	_nss_hesiod_getservbyname_r, @function
_nss_hesiod_getservbyname_r:
	subq	$16, %rsp
	pushq	%r9
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	.LC1(%rip), %rsi
	call	lookup
	addq	$24, %rsp
	ret
	.size	_nss_hesiod_getservbyname_r, .-_nss_hesiod_getservbyname_r
	.section	.rodata.str1.1
.LC2:
	.string	"%d"
.LC3:
	.string	"port"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getservbyport_r
	.type	_nss_hesiod_getservbyport_r, @function
_nss_hesiod_getservbyport_r:
	pushq	%r15
	pushq	%r14
	rolw	$8, %di
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	leaq	.LC2(%rip), %rdx
	movq	%rsi, %rbx
	movq	%rcx, %r12
	subq	$24, %rsp
	movzwl	%di, %ecx
	movl	$6, %esi
	leaq	10(%rsp), %r14
	movq	%r8, %r13
	movq	%r9, %r15
	movq	%r14, %rdi
	call	snprintf@PLT
	subq	$8, %rsp
	leaq	.LC3(%rip), %rsi
	movq	%r13, %r9
	pushq	%r15
	movq	%r12, %r8
	movq	%rbp, %rcx
	movq	%rbx, %rdx
	movq	%r14, %rdi
	call	lookup
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	_nss_hesiod_getservbyport_r, .-_nss_hesiod_getservbyport_r
	.hidden	hesiod_end
	.hidden	hesiod_free_list
	.hidden	hesiod_resolve
	.hidden	hesiod_init
