	.text
	.p2align 4,,15
	.type	lookup, @function
lookup:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%r8, %rbp
	movq	%rcx, %rbx
	movq	%r9, %r13
	subq	$40, %rsp
	movq	errno@gottpoff(%rip), %rax
	leaq	24(%rsp), %rdi
	movq	%rsi, (%rsp)
	movl	%fs:(%rax), %r15d
	call	hesiod_init
	testl	%eax, %eax
	js	.L5
	movq	(%rsp), %rsi
	movq	24(%rsp), %rdi
	movq	%rsi, %rdx
	movq	%r12, %rsi
	call	hesiod_resolve
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L10
	movq	(%rax), %rsi
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	strlen@PLT
	leaq	1(%rax), %rdx
	movq	24(%rsp), %rax
	movq	8(%rsp), %rsi
	cmpq	%rdx, %rbp
	movq	%rax, (%rsp)
	jb	.L11
	movq	%rbx, %rdi
	call	memcpy@PLT
	movq	(%rsp), %rdi
	movq	%r12, %rsi
	call	hesiod_free_list
	movq	24(%rsp), %rdi
	call	hesiod_end
	movq	%rbx, %rdx
	movq	%r13, %r8
	movq	%rbp, %rcx
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	_nss_files_parse_pwent@PLT
	testl	%eax, %eax
	movl	$1, %edx
	jle	.L12
.L1:
	addq	$40, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	errno@gottpoff(%rip), %rcx
	xorl	%edx, %edx
	cmpl	$-1, %eax
	setne	%dl
	leal	-2(%rdx,%rdx), %edx
	movl	%r15d, %fs:(%rcx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	errno@gottpoff(%rip), %rax
	movq	24(%rsp), %rdi
	movl	%fs:(%rax), %ebx
	call	hesiod_end
	movq	errno@gottpoff(%rip), %rax
	xorl	%edx, %edx
	cmpl	$2, %ebx
	setne	%dl
	movl	%r15d, %fs:(%rax)
	negl	%edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L11:
	movq	%rax, %rdi
	movq	%r12, %rsi
	call	hesiod_free_list
	movq	24(%rsp), %rdi
	call	hesiod_end
	movl	$34, 0(%r13)
	movl	$-2, %edx
	jmp	.L1
	.size	lookup, .-lookup
	.p2align 4,,15
	.globl	_nss_hesiod_setpwent
	.type	_nss_hesiod_setpwent, @function
_nss_hesiod_setpwent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_setpwent, .-_nss_hesiod_setpwent
	.p2align 4,,15
	.globl	_nss_hesiod_endpwent
	.type	_nss_hesiod_endpwent, @function
_nss_hesiod_endpwent:
	movl	$1, %eax
	ret
	.size	_nss_hesiod_endpwent, .-_nss_hesiod_endpwent
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"passwd"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getpwnam_r
	.type	_nss_hesiod_getpwnam_r, @function
_nss_hesiod_getpwnam_r:
	movq	%r8, %r9
	movq	%rcx, %r8
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	leaq	.LC0(%rip), %rsi
	jmp	lookup
	.size	_nss_hesiod_getpwnam_r, .-_nss_hesiod_getpwnam_r
	.section	.rodata.str1.1
.LC1:
	.string	"%d"
.LC2:
	.string	"uid"
	.text
	.p2align 4,,15
	.globl	_nss_hesiod_getpwuid_r
	.type	_nss_hesiod_getpwuid_r, @function
_nss_hesiod_getpwuid_r:
	pushq	%r14
	pushq	%r13
	xorl	%eax, %eax
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	leaq	.LC1(%rip), %rdx
	movq	%rsi, %rbp
	movq	%rcx, %r13
	movl	$21, %esi
	movl	%edi, %ecx
	subq	$32, %rsp
	movq	%r8, %r14
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	call	snprintf@PLT
	leaq	.LC2(%rip), %rsi
	movq	%r14, %r9
	movq	%r13, %r8
	movq	%r12, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rdi
	call	lookup
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	_nss_hesiod_getpwuid_r, .-_nss_hesiod_getpwuid_r
	.hidden	hesiod_end
	.hidden	hesiod_free_list
	.hidden	hesiod_resolve
	.hidden	hesiod_init
