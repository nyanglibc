.text
.globl __getppid
.type __getppid,@function
.align 1<<4
__getppid:
	movl $110, %eax
	syscall
	ret
.size __getppid,.-__getppid
.globl __GI___getppid
.set __GI___getppid,__getppid
.weak getppid
getppid = __getppid
.globl __GI_getppid
.set __GI_getppid,getppid
