	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __posix_spawnp,posix_spawnp@@GLIBC_2.15
	.symver __posix_spawnp_compat,posix_spawnp@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawnp
	.type	__posix_spawnp, @function
__posix_spawnp:
	subq	$16, %rsp
	pushq	$1
	call	__spawni
	addq	$24, %rsp
	ret
	.size	__posix_spawnp, .-__posix_spawnp
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__posix_spawnp_compat
	.type	__posix_spawnp_compat, @function
__posix_spawnp_compat:
	subq	$16, %rsp
	pushq	$3
	call	__spawni
	addq	$24, %rsp
	ret
	.size	__posix_spawnp_compat, .-__posix_spawnp_compat
	.hidden	__spawni
