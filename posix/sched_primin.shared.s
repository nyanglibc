.text
.globl __sched_get_priority_min
.type __sched_get_priority_min,@function
.align 1<<4
__sched_get_priority_min:
	movl $147, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_get_priority_min,.-__sched_get_priority_min
.globl __GI___sched_get_priority_min
.set __GI___sched_get_priority_min,__sched_get_priority_min
.weak sched_get_priority_min
sched_get_priority_min = __sched_get_priority_min
.globl __GI_sched_get_priority_min
.set __GI_sched_get_priority_min,sched_get_priority_min
