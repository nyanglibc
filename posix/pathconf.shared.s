	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/sys/dev/block/%u:%u"
.LC1:
	.string	"/sys/fs/ext4/%s"
.LC2:
	.string	"r"
.LC3:
	.string	"/proc/mounts"
.LC4:
	.string	"/etc/mtab"
.LC5:
	.string	"ext2"
.LC6:
	.string	"ext3"
.LC7:
	.string	"ext4"
#NO_APP
	.text
	.p2align 4,,15
	.type	distinguish_extX.isra.0, @function
distinguish_extX.isra.0:
.LFB78:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$5528, %rsp
	testq	%rdi, %rdi
	je	.L31
	leaq	-5456(%rbp), %rsi
	call	__GI___stat64
	testl	%eax, %eax
	setne	%al
.L3:
	testb	%al, %al
	jne	.L4
	movq	-5456(%rbp), %rax
	leaq	-5520(%rbp), %r12
	leaq	-4144(%rbp), %rbx
	movq	%r12, %rdi
	movq	%rax, %rcx
	movzbl	%al, %esi
	movq	%rax, %r8
	shrq	$8, %rcx
	shrq	$32, %rax
	shrq	$12, %r8
	movl	%ecx, %edx
	movq	%rax, %rcx
	xorb	%r8b, %r8b
	andl	$4095, %edx
	andl	$-4096, %ecx
	orl	%esi, %r8d
	orl	%edx, %ecx
	leaq	.LC0(%rip), %rdx
	movl	$64, %esi
	xorl	%eax, %eax
	call	__GI___snprintf
	movl	$4096, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__readlink
	cmpq	$4095, %rax
	jbe	.L32
	leaq	.LC2(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	call	__GI___setmntent
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L33
.L7:
	orl	$32768, (%rbx)
	leaq	-5168(%rbp), %r13
	leaq	-5568(%rbp), %r12
	leaq	.LC5(%rip), %r14
	leaq	-5312(%rbp), %r15
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1024, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__GI___getmntent_r
	testq	%rax, %rax
	je	.L34
	movq	-5552(%rbp), %rax
	movl	$5, %ecx
	movq	%r14, %rdi
	movq	%rax, %rsi
	repz cmpsb
	je	.L9
	leaq	.LC6(%rip), %rdi
	movl	$5, %ecx
	movq	%rax, %rsi
	repz cmpsb
	je	.L9
	leaq	.LC7(%rip), %rdi
	movl	$5, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L8
.L9:
	movq	-5560(%rbp), %rdi
	movq	%r15, %rsi
	call	__GI___stat64
	testl	%eax, %eax
	js	.L8
	movq	-5312(%rbp), %rax
	cmpq	%rax, -5456(%rbp)
	jne	.L8
	movq	-5552(%rbp), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$5, %ecx
	repz cmpsb
	seta	%al
	setb	%dl
	subl	%edx, %eax
	movsbl	%al, %eax
	cmpl	$1, %eax
	sbbq	%r12, %r12
	andl	$33000, %r12d
	addq	$32000, %r12
.L12:
	movq	%rbx, %rdi
	call	__GI___endmntent
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	leaq	.LC2(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	call	__GI___setmntent
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L7
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$32000, %r12d
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rbx, %rdi
	movb	$0, -4144(%rbp,%rax)
	call	__GI___basename
	movq	%rax, %rdi
	movq	%rax, %r12
	call	__GI_strlen
	leaq	1(%rax), %rdx
	addq	$31, %rax
	movq	%r12, %rsi
	andq	$-16, %rax
	movl	$65000, %r12d
	subq	%rax, %rsp
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	call	__GI_memcpy@PLT
	leaq	.LC1(%rip), %rdx
	movq	%rax, %rcx
	movl	$4096, %esi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	__GI___snprintf
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__GI___access
	testl	%eax, %eax
	je	.L1
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%esi, %eax
	leaq	-5456(%rbp), %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	setne	%al
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L34:
	movl	$32000, %r12d
	jmp	.L12
.LFE78:
	.size	distinguish_extX.isra.0, .-distinguish_extX.isra.0
	.p2align 4,,15
	.type	__statfs_link_max.part.1, @function
__statfs_link_max.part.1:
.LFB79:
	movq	(%rdi), %rcx
	movq	%rsi, %rax
	cmpq	$19920822, %rcx
	jg	.L37
	cmpq	$19920820, %rcx
	jge	.L38
	cmpq	$9320, %rcx
	je	.L39
	jle	.L71
	cmpq	$61267, %rcx
	je	.L42
	cmpq	$72020, %rcx
	je	.L49
	cmpq	$9336, %rcx
	je	.L39
.L36:
	movl	$127, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	cmpq	$1382369651, %rcx
	je	.L44
	jg	.L45
	cmpq	$19920823, %rcx
	movl	$10000, %eax
	je	.L35
	cmpq	$198183888, %rcx
	movl	$65000, %eax
	jne	.L36
.L35:
	rep ret
	.p2align 4,,10
	.p2align 3
.L42:
	movl	%edx, %esi
	movq	%rax, %rdi
	jmp	distinguish_extX.isra.0
	.p2align 4,,10
	.p2align 3
.L38:
	movl	$126, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	movl	$65530, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpq	$1481003842, %rcx
	movl	$2147483647, %eax
	je	.L35
	addq	$1928667153, %rax
	cmpq	%rax, %rcx
	je	.L49
	cmpq	$1410924800, %rcx
	jne	.L36
.L49:
	movl	$32000, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	cmpq	$4991, %rcx
	je	.L41
	cmpq	$5007, %rcx
	jne	.L36
.L41:
	movl	$250, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	movl	$64535, %eax
	ret
.LFE79:
	.size	__statfs_link_max.part.1, .-__statfs_link_max.part.1
	.p2align 4,,15
	.type	__statfs_filesize_max.part.2, @function
__statfs_filesize_max.part.2:
.LFB80:
	movq	(%rdi), %rdx
	cmpq	$827541066, %rdx
	je	.L74
	jle	.L114
	cmpq	$1481003842, %rdx
	je	.L74
	jle	.L115
	movl	$2768370933, %eax
	cmpq	%rax, %rdx
	je	.L74
	movl	$4076150800, %ecx
	movl	$256, %eax
	cmpq	%rcx, %rdx
	je	.L72
	subq	$1641134034, %rcx
	movl	$255, %eax
	cmpq	%rcx, %rdx
	je	.L116
.L73:
	movl	$32, %eax
.L72:
	rep ret
	.p2align 4,,10
	.p2align 3
.L114:
	cmpq	$72020, %rdx
	je	.L74
	jle	.L117
	cmpq	$198183888, %rdx
	je	.L74
	cmpq	$352400198, %rdx
	je	.L74
	cmpq	$2613483, %rdx
	jne	.L73
.L74:
	movl	$64, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L116:
	rep ret
	.p2align 4,,10
	.p2align 3
.L117:
	cmpq	$20859, %rdx
	je	.L74
	cmpq	$61267, %rdx
	jne	.L73
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L115:
	cmpq	$1397118030, %rdx
	je	.L74
	cmpq	$1410924800, %rdx
	je	.L74
	cmpq	$1382369651, %rdx
	jne	.L73
	jmp	.L74
.LFE80:
	.size	__statfs_filesize_max.part.2, .-__statfs_filesize_max.part.2
	.p2align 4,,15
	.globl	__statfs_link_max
	.hidden	__statfs_link_max
	.type	__statfs_link_max, @function
__statfs_link_max:
.LFB74:
	testl	%edi, %edi
	movq	%rsi, %rax
	movq	%rdx, %rsi
	js	.L123
	movl	%ecx, %edx
	movq	%rax, %rdi
	jmp	__statfs_link_max.part.1
	.p2align 4,,10
	.p2align 3
.L123:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rdx
	cmpl	$38, %fs:(%rax)
	movl	$127, %eax
	cmovne	%rdx, %rax
	ret
.LFE74:
	.size	__statfs_link_max, .-__statfs_link_max
	.p2align 4,,15
	.globl	__statfs_filesize_max
	.hidden	__statfs_filesize_max
	.type	__statfs_filesize_max, @function
__statfs_filesize_max:
.LFB75:
	testl	%edi, %edi
	js	.L129
	movq	%rsi, %rdi
	jmp	__statfs_filesize_max.part.2
	.p2align 4,,10
	.p2align 3
.L129:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %rdx
	cmpl	$38, %fs:(%rax)
	movl	$32, %eax
	cmovne	%rdx, %rax
	ret
.LFE75:
	.size	__statfs_filesize_max, .-__statfs_filesize_max
	.p2align 4,,15
	.globl	__statfs_symlinks
	.hidden	__statfs_symlinks
	.type	__statfs_symlinks, @function
__statfs_symlinks:
.LFB76:
	testl	%edi, %edi
	js	.L169
	movq	(%rsi), %rax
	cmpq	$44533, %rax
	je	.L138
	jle	.L170
	cmpq	$464386766, %rax
	je	.L138
	jle	.L171
	cmpq	$684539205, %rax
	je	.L138
	cmpq	$1397118030, %rax
	jne	.L133
.L138:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	cmpq	$7377, %rax
	je	.L138
	jle	.L172
	cmpq	$19780, %rax
	je	.L138
	cmpq	$29301, %rax
	je	.L138
.L133:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L169:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$38, %fs:(%rax)
	sete	%al
	movzbl	%al, %eax
	leaq	-1(%rax,%rax), %rax
	ret
	.p2align 4,,10
	.p2align 3
.L171:
	cmpq	$469337, %rax
	je	.L138
	cmpq	$4278867, %rax
	jne	.L133
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L172:
	cmpq	$47, %rax
	jne	.L133
	jmp	.L138
.LFE76:
	.size	__statfs_symlinks, .-__statfs_symlinks
	.p2align 4,,15
	.globl	__pathconf
	.type	__pathconf, @function
__pathconf:
.LFB72:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$144, %rsp
	cmpl	$6, %esi
	je	.L175
	jle	.L221
	cmpl	$13, %esi
	je	.L178
	cmpl	$20, %esi
	jne	.L174
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__GI___statfs
	movq	%rbp, %rsi
	movl	%eax, %edi
	call	__statfs_symlinks
.L173:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__GI___statfs
	testl	%eax, %eax
	js	.L222
	movq	%rbp, %rdi
	call	__statfs_filesize_max.part.2
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	testl	%esi, %esi
	jne	.L174
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__GI___statfs
	testl	%eax, %eax
	jns	.L180
	movl	$127, %eax
.L219:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$38, %fs:(%rdx)
	je	.L173
.L207:
	movq	$-1, %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L174:
	cmpb	$0, (%rbx)
	je	.L223
	cmpl	$20, %esi
	ja	.L185
	leaq	.L186(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L186:
	.long	.L207-.L186
	.long	.L217-.L186
	.long	.L217-.L186
	.long	.L188-.L186
	.long	.L189-.L186
	.long	.L189-.L186
	.long	.L185-.L186
	.long	.L190-.L186
	.long	.L191-.L186
	.long	.L207-.L186
	.long	.L192-.L186
	.long	.L207-.L186
	.long	.L207-.L186
	.long	.L193-.L186
	.long	.L207-.L186
	.long	.L207-.L186
	.long	.L194-.L186
	.long	.L196-.L186
	.long	.L196-.L186
	.long	.L207-.L186
	.long	.L190-.L186
	.text
	.p2align 4,,10
	.p2align 3
.L175:
	movq	%rsp, %rsi
	call	__GI___statfs
	movl	%eax, %edx
	movl	$1, %eax
	testl	%edx, %edx
	js	.L219
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L180:
	movl	$-1, %edx
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__statfs_link_max.part.1
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L222:
	movl	$32, %eax
	jmp	.L219
	.p2align 4,,10
	.p2align 3
.L196:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	call	__statvfs64
	movl	%eax, %edx
	movq	$-1, %rax
	testl	%edx, %edx
	js	.L173
	movq	8(%rsp), %rax
	jmp	.L173
.L224:
	cmpl	$38, %fs:0(%rbp)
	movq	$-1, %rax
	jne	.L173
	movl	%r12d, %fs:0(%rbp)
	.p2align 4,,10
	.p2align 3
.L217:
	movl	$255, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L189:
	movl	$4096, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L190:
	movl	$1, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L188:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	movl	%fs:0(%rbp), %r12d
	call	__statvfs64
	testl	%eax, %eax
	js	.L224
	movq	80(%rsp), %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L193:
	movl	$32, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	call	__GI___stat64
	movl	%eax, %edx
	movq	$-1, %rax
	testl	%edx, %edx
	js	.L173
	movl	24(%rsp), %eax
	andl	$61440, %eax
	subl	$24576, %eax
	andb	$-33, %ah
	cmpl	$1, %eax
	sbbq	%rax, %rax
	andl	$2, %eax
	subq	$1, %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L194:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	call	__statvfs64
	movl	%eax, %edx
	movq	$-1, %rax
	testl	%edx, %edx
	js	.L173
	movq	(%rsp), %rax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L191:
	xorl	%eax, %eax
	jmp	.L173
	.p2align 4,,10
	.p2align 3
.L185:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movq	$-1, %rax
	jmp	.L173
.L223:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	movq	$-1, %rax
	jmp	.L173
.LFE72:
	.size	__pathconf, .-__pathconf
	.weak	pathconf
	.set	pathconf,__pathconf
	.p2align 4,,15
	.globl	__statfs_chown_restricted
	.hidden	__statfs_chown_restricted
	.type	__statfs_chown_restricted, @function
__statfs_chown_restricted:
.LFB77:
	testl	%edi, %edi
	movl	$1, %eax
	js	.L229
	rep ret
	.p2align 4,,10
	.p2align 3
.L229:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$38, %fs:(%rax)
	sete	%al
	movzbl	%al, %eax
	leaq	-1(%rax,%rax), %rax
	ret
.LFE77:
	.size	__statfs_chown_restricted, .-__statfs_chown_restricted
	.hidden	__statvfs64
	.hidden	__readlink
