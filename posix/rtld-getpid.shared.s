.text
.globl __getpid
.type __getpid,@function
.align 1<<4
__getpid:
	movl $39, %eax
	syscall;
	ret
.size __getpid,.-__getpid
.globl __GI___getpid
.set __GI___getpid,__getpid
.weak getpid
getpid = __getpid
.globl __GI_getpid
.set __GI_getpid,getpid
