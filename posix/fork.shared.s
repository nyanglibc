	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_fork
	.type	__libc_fork, @function
__libc_fork:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$176, %rsp
#APP
# 57 "../sysdeps/nptl/fork.c" 1
	movl %fs:24,%ebx
# 0 "" 2
#NO_APP
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	setne	%bpl
	xorl	%edi, %edi
	movl	%ebp, %esi
	call	__run_fork_handlers
	testl	%ebx, %ebx
	jne	.L27
.L2:
	movq	%fs:16, %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$18874385, %edi
	leaq	720(%rax), %r10
	movl	$56, %eax
#APP
# 49 "../sysdeps/unix/sysv/linux/arch-fork.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L28
	testl	%eax, %eax
	movl	%eax, %r12d
	jne	.L4
	movq	__fork_generation_pointer(%rip), %rax
	movq	%fs:16, %rdi
	testq	%rax, %rax
	je	.L5
	addq	$4, (%rax)
.L5:
	leaq	736(%rdi), %rax
	movq	%rax, 728(%rdi)
	movq	%rax, 736(%rdi)
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	jne	.L29
.L6:
	testl	%ebx, %ebx
	jne	.L30
.L7:
	movq	_rtld_global@GOTPCREL(%rip), %rax
	pxor	%xmm0, %xmm0
	movl	%ebp, %esi
	movl	$1, %edi
	movups	%xmm0, 2456(%rax)
	movq	$0, 2472(%rax)
	movl	$1, 2456(%rax)
	movups	%xmm0, 2440(%rax)
	call	__run_fork_handlers
	addq	$176, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	$-1, %r12d
	movl	%eax, %fs:(%rdx)
.L4:
	testl	%ebx, %ebx
	jne	.L31
	movl	%ebp, %esi
	movl	$2, %edi
	call	__run_fork_handlers
.L33:
	addq	$176, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%rsp, %rdi
	call	__nss_database_fork_prepare_parent
	call	__GI__IO_list_lock
	call	__malloc_fork_lock_parent
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L30:
	call	__malloc_fork_unlock_child
	call	__GI__IO_iter_begin
	movq	%rax, %rbx
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rdi
	call	__GI__IO_iter_next
	movq	%rax, %rbx
.L8:
	call	__GI__IO_iter_end
	cmpq	%rax, %rbx
	je	.L32
	movq	%rbx, %rdi
	call	__GI__IO_iter_file
	movl	(%rax), %eax
	testb	$-128, %ah
	jne	.L9
	movq	%rbx, %rdi
	call	__GI__IO_iter_file
	movq	136(%rax), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L31:
	call	__malloc_fork_unlock_parent
	call	__GI__IO_list_unlock
	movl	%ebp, %esi
	movl	$2, %edi
	call	__run_fork_handlers
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L29:
	movq	232+__libc_pthread_functions(%rip), %rax
#APP
# 108 "../sysdeps/nptl/fork.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L32:
	call	__GI__IO_list_resetlock
	movq	%rsp, %rdi
	call	__nss_database_fork_subprocess
	jmp	.L7
	.size	__libc_fork, .-__libc_fork
	.weak	fork
	.set	fork,__libc_fork
	.weak	__GI___fork
	.hidden	__GI___fork
	.set	__GI___fork,__libc_fork
	.globl	__fork
	.set	__fork,__GI___fork
	.hidden	__nss_database_fork_subprocess
	.hidden	__libc_pthread_functions
	.hidden	__malloc_fork_unlock_parent
	.hidden	__malloc_fork_unlock_child
	.hidden	__malloc_fork_lock_parent
	.hidden	__nss_database_fork_prepare_parent
	.hidden	__libc_pthread_functions_init
	.hidden	__fork_generation_pointer
	.hidden	__run_fork_handlers
