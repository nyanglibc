.text
.globl __getppid
.type __getppid,@function
.align 1<<4
__getppid:
	movl $110, %eax
	syscall
	ret
.size __getppid,.-__getppid
.weak getppid
getppid = __getppid
