	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawn_file_actions_addopen
	.hidden	__posix_spawn_file_actions_addopen
	.type	__posix_spawn_file_actions_addopen, @function
__posix_spawn_file_actions_addopen:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r14
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r13d
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	%esi, %ebp
	movl	%r8d, %r12d
	call	__spawn_valid_fd
	testb	%al, %al
	movl	$9, %ecx
	je	.L1
	movq	%r14, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L6
	movl	4(%rbx), %edx
	cmpl	(%rbx), %edx
	je	.L10
.L3:
	movslq	%edx, %rax
	addl	$1, %edx
	xorl	%ecx, %ecx
	salq	$5, %rax
	addq	8(%rbx), %rax
	movl	$2, (%rax)
	movl	%ebp, 8(%rax)
	movq	%r14, 16(%rax)
	movl	%r13d, 24(%rax)
	movl	%r12d, 28(%rax)
	movl	%edx, 4(%rbx)
.L1:
	popq	%rbx
	movl	%ecx, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$12, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%rbx, %rdi
	call	__posix_spawn_file_actions_realloc
	testl	%eax, %eax
	jne	.L4
	movl	4(%rbx), %edx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%r14, %rdi
	call	free@PLT
	movl	$12, %ecx
	jmp	.L1
	.size	__posix_spawn_file_actions_addopen, .-__posix_spawn_file_actions_addopen
	.weak	posix_spawn_file_actions_addopen
	.set	posix_spawn_file_actions_addopen,__posix_spawn_file_actions_addopen
	.hidden	__posix_spawn_file_actions_realloc
	.hidden	__spawn_valid_fd
