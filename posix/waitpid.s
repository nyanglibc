	.text
	.p2align 4,,15
	.globl	__waitpid
	.hidden	__waitpid
	.type	__waitpid, @function
__waitpid:
	xorl	%ecx, %ecx
	jmp	__wait4
	.size	__waitpid, .-__waitpid
	.weak	waitpid
	.set	waitpid,__waitpid
	.hidden	__wait4
