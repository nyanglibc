	.text
	.p2align 4,,15
	.globl	sched_getcpu
	.type	sched_getcpu, @function
sched_getcpu:
	pushq	%rbx
	subq	$16, %rsp
	movq	_dl_vdso_getcpu(%rip), %rax
	leaq	12(%rsp), %rbx
	testq	%rax, %rax
	je	.L5
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*%rax
	movslq	%eax, %rdx
	cmpq	$-4096, %rdx
	ja	.L12
.L4:
	cmpl	$-1, %edx
	movl	%edx, %eax
	je	.L1
	movl	12(%rsp), %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	cmpq	$-38, %rdx
	je	.L5
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	addq	$16, %rsp
	movl	$-1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	$309, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/sched_getcpu.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L3
	jmp	.L4
	.size	sched_getcpu, .-sched_getcpu
