.text
.globl __getgid
.type __getgid,@function
.align 1<<4
__getgid:
	movl $104, %eax
	syscall
	ret
.size __getgid,.-__getgid
.globl __GI___getgid
.set __GI___getgid,__getgid
.weak getgid
getgid = __getgid
.globl __GI_getgid
.set __GI_getgid,getgid
