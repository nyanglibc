	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___getcpu
	.hidden	__GI___getcpu
	.type	__GI___getcpu, @function
__GI___getcpu:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	712(%rax), %rax
	testq	%rax, %rax
	je	.L6
	xorl	%edx, %edx
	call	*%rax
	movslq	%eax, %rdx
	cmpq	$-4096, %rdx
	jbe	.L1
	cmpq	$-38, %rdx
	je	.L6
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
.L5:
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$309, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/getcpu.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L3
	jmp	.L1
	.size	__GI___getcpu, .-__GI___getcpu
	.weak	getcpu
	.set	getcpu,__GI___getcpu
	.globl	__getcpu
	.set	__getcpu,__GI___getcpu
