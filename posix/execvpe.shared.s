	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/bin/sh"
#NO_APP
	.text
	.p2align 4,,15
	.type	maybe_script_execute, @function
maybe_script_execute:
	pushq	%rbp
	xorl	%ecx, %ecx
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movq	%rdx, %r12
	subq	$40, %rsp
	cmpq	$0, (%rsi)
	jne	.L2
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	$2147483646, %rax
	movq	%rax, %rcx
	je	.L17
.L2:
	leaq	1(%rcx), %rax
	cmpq	$0, (%rsi,%rax,8)
	leaq	0(,%rax,8), %rdx
	jne	.L5
	leaq	46(,%rcx,8), %rcx
	movq	%rsp, %rbx
	leaq	.LC0(%rip), %r8
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	7(%rsp), %rcx
	shrq	$3, %rcx
	cmpq	$1, %rax
	leaq	0(,%rcx,8), %r13
	movq	%r8, 0(,%rcx,8)
	movq	%rdi, 8(,%rcx,8)
	je	.L3
	leaq	16(%r13), %rdi
	addq	$8, %rsi
	call	__GI_memcpy@PLT
.L6:
	leaq	.LC0(%rip), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	__execve
	movq	%rbx, %rsp
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
.L16:
	leaq	.LC0(%rip), %rax
	leaq	-64(%rbp), %r13
	movq	%rsp, %rbx
	movq	%rdi, -56(%rbp)
	movq	%rax, -64(%rbp)
	.p2align 4,,10
	.p2align 3
.L3:
	movq	$0, 16(%r13)
	jmp	.L6
.L17:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %fs:(%rax)
	leaq	-24(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.size	maybe_script_execute, .-maybe_script_execute
	.section	.rodata.str1.1
.LC1:
	.string	"/bin:/usr/bin"
.LC2:
	.string	"PATH"
	.text
	.p2align 4,,15
	.type	__execvpe_common, @function
__execvpe_common:
	cmpb	$0, (%rdi)
	je	.L70
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbx
	movl	$47, %esi
	movl	%ecx, %r15d
	movq	%rdx, %r13
	movq	%rdi, %rbx
	subq	$56, %rsp
	call	__GI_strchr
	testq	%rax, %rax
	je	.L21
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__execve
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$8, %fs:(%rax)
	jne	.L20
	testb	%r15b, %r15b
	jne	.L71
.L20:
	leaq	-40(%rbp), %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	.LC2(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	movq	%rax, %r14
	leaq	.LC1(%rip), %rax
	movl	$255, %esi
	movq	%rbx, %rdi
	cmove	%rax, %r14
	call	__GI___strnlen
	movl	$4095, %esi
	movq	%r14, %rdi
	movq	%rax, -56(%rbp)
	call	__GI___strnlen
	movq	-56(%rbp), %rdx
	cmpq	$255, %rdx
	ja	.L25
	leaq	1(%rdx), %rcx
	addq	$1, %rax
	movq	%rax, -64(%rbp)
	movq	%rcx, -72(%rbp)
	addq	%rax, %rcx
	leaq	1(%rcx), %rdi
	movq	%rcx, -56(%rbp)
	call	__GI___libc_alloca_cutoff
	testl	%eax, %eax
	movq	-56(%rbp), %rdx
	je	.L25
	addq	$16, %rdx
	movb	$0, -73(%rbp)
	andq	$-16, %rdx
	subq	%rdx, %rsp
	movq	%rsp, -56(%rbp)
.L27:
	movl	$58, %esi
	movq	%r14, %rdi
	call	__strchrnul@PLT
	movq	%rax, %rdx
	movq	%rax, %r8
	subq	%r14, %rdx
	cmpq	-64(%rbp), %rdx
	jb	.L28
	cmpb	$0, (%rax)
	jne	.L30
.L29:
	cmpb	$0, -73(%rbp)
	je	.L20
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$13, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$36, %fs:(%rax)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L71:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	maybe_script_execute
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L28:
	movq	-56(%rbp), %rdi
	movq	%r14, %rsi
	movq	%rax, -88(%rbp)
	call	__GI_mempcpy
	movq	-88(%rbp), %r8
	movq	-72(%rbp), %rsi
	xorl	%edx, %edx
	movb	$47, (%rax)
	cmpq	%r8, %r14
	setb	%dl
	addq	%rdx, %rax
	cmpl	$8, %esi
	jnb	.L31
	andl	$4, %esi
	jne	.L72
	movq	-72(%rbp), %rsi
	testl	%esi, %esi
	je	.L32
	movzbl	(%rbx), %edx
	andl	$2, %esi
	movb	%dl, (%rax)
	jne	.L73
.L32:
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%r8, -88(%rbp)
	call	__execve
	movq	__libc_errno@gottpoff(%rip), %r14
	movq	-88(%rbp), %r8
	movl	%fs:(%r14), %eax
	cmpl	$8, %eax
	jne	.L35
	testb	%r15b, %r15b
	jne	.L74
.L35:
	cmpl	$20, %eax
	jg	.L36
	cmpl	$19, %eax
	jge	.L38
	cmpl	$2, %eax
	je	.L38
	cmpl	$13, %eax
	jne	.L20
	movb	$1, -73(%rbp)
.L38:
	cmpb	$0, (%r8)
	leaq	1(%r8), %rax
	je	.L29
	movq	%rax, %r8
.L30:
	movq	%r8, %r14
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L31:
	movq	(%rbx), %rdx
	leaq	8(%rax), %rdi
	movq	%rbx, %rsi
	andq	$-8, %rdi
	movq	%rdx, (%rax)
	movq	-72(%rbp), %r9
	movl	%r9d, %edx
	movq	-8(%rbx,%rdx), %rcx
	movq	%rcx, -8(%rax,%rdx)
	subq	%rdi, %rax
	subq	%rax, %rsi
	addl	%r9d, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L32
.L36:
	cmpl	$110, %eax
	je	.L38
	cmpl	$116, %eax
	jne	.L20
	jmp	.L38
.L74:
	movq	-56(%rbp), %rdi
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	maybe_script_execute
	movl	%fs:(%r14), %eax
	movq	-88(%rbp), %r8
	jmp	.L35
.L72:
	movl	(%rbx), %edx
	movl	%edx, (%rax)
	movl	-72(%rbp), %edx
	movl	-4(%rbx,%rdx), %ecx
	movl	%ecx, -4(%rax,%rdx)
	jmp	.L32
.L73:
	movl	-72(%rbp), %edx
	movzwl	-2(%rbx,%rdx), %ecx
	movw	%cx, -2(%rax,%rdx)
	jmp	.L32
	.size	__execvpe_common, .-__execvpe_common
	.p2align 4,,15
	.globl	__execvpe
	.hidden	__execvpe
	.type	__execvpe, @function
__execvpe:
	movl	$1, %ecx
	jmp	__execvpe_common
	.size	__execvpe, .-__execvpe
	.weak	execvpe
	.set	execvpe,__execvpe
	.p2align 4,,15
	.globl	__execvpex
	.hidden	__execvpex
	.type	__execvpex, @function
__execvpex:
	xorl	%ecx, %ecx
	jmp	__execvpe_common
	.size	__execvpex, .-__execvpex
	.hidden	__execve
