	.text
	.p2align 4,,15
	.globl	execvp
	.hidden	execvp
	.type	execvp, @function
execvp:
	movq	__environ(%rip), %rdx
	jmp	__execvpe
	.size	execvp, .-execvp
	.hidden	__execvpe
