	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	posix_spawnattr_getsigdefault
	.type	posix_spawnattr_getsigdefault, @function
posix_spawnattr_getsigdefault:
	movdqu	8(%rdi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rsi)
	movdqu	24(%rdi), %xmm0
	movups	%xmm0, 16(%rsi)
	movdqu	40(%rdi), %xmm0
	movups	%xmm0, 32(%rsi)
	movdqu	56(%rdi), %xmm0
	movups	%xmm0, 48(%rsi)
	movdqu	72(%rdi), %xmm0
	movups	%xmm0, 64(%rsi)
	movdqu	88(%rdi), %xmm0
	movups	%xmm0, 80(%rsi)
	movdqu	104(%rdi), %xmm0
	movups	%xmm0, 96(%rsi)
	movdqu	120(%rdi), %xmm0
	movups	%xmm0, 112(%rsi)
	ret
	.size	posix_spawnattr_getsigdefault, .-posix_spawnattr_getsigdefault
