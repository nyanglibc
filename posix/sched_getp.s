.text
.globl __sched_getparam
.type __sched_getparam,@function
.align 1<<4
__sched_getparam:
	movl $143, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_getparam,.-__sched_getparam
.weak sched_getparam
sched_getparam = __sched_getparam
