	.text
	.p2align 4,,15
	.globl	__libc_fork
	.type	__libc_fork, @function
__libc_fork:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$176, %rsp
#APP
# 57 "../sysdeps/nptl/fork.c" 1
	movl %fs:24,%ebx
# 0 "" 2
#NO_APP
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	setne	%bpl
	xorl	%edi, %edi
	movl	%ebp, %esi
	call	__run_fork_handlers
	testl	%ebx, %ebx
	je	.L3
	cmpq	$0, __nss_database_fork_prepare_parent@GOTPCREL(%rip)
	je	.L4
	movq	%rsp, %rdi
	call	__nss_database_fork_prepare_parent@PLT
.L4:
	call	_IO_list_lock
	cmpq	$0, __malloc_fork_lock_parent@GOTPCREL(%rip)
	je	.L3
	call	__malloc_fork_lock_parent@PLT
.L3:
	movq	%fs:16, %rax
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	$18874385, %edi
	leaq	720(%rax), %r10
	movl	$56, %eax
#APP
# 49 "../sysdeps/unix/sysv/linux/arch-fork.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L46
	testl	%eax, %eax
	movl	%eax, %r12d
	jne	.L7
	movq	__fork_generation_pointer(%rip), %rax
	movq	%fs:16, %rdi
	testq	%rax, %rax
	je	.L8
	addq	$4, (%rax)
.L8:
	cmpq	$0, __nptl_set_robust@GOTPCREL(%rip)
	leaq	736(%rdi), %rax
	movq	%rax, 728(%rdi)
	movq	%rax, 736(%rdi)
	je	.L9
	call	__nptl_set_robust@PLT
.L9:
	testl	%ebx, %ebx
	je	.L11
	cmpq	$0, __malloc_fork_unlock_child@GOTPCREL(%rip)
	je	.L12
	call	__malloc_fork_unlock_child@PLT
.L12:
	call	_IO_iter_begin
	movq	%rax, %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rbx, %rdi
	call	_IO_iter_next
	movq	%rax, %rbx
.L13:
	call	_IO_iter_end
	cmpq	%rax, %rbx
	je	.L47
	movq	%rbx, %rdi
	call	_IO_iter_file
	movl	(%rax), %eax
	testb	$-128, %ah
	jne	.L14
	movq	%rbx, %rdi
	call	_IO_iter_file
	movq	136(%rax), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L47:
	call	_IO_list_resetlock
	cmpq	$0, __nss_database_fork_subprocess@GOTPCREL(%rip)
	je	.L11
	movq	%rsp, %rdi
	call	__nss_database_fork_subprocess@PLT
.L11:
	pxor	%xmm0, %xmm0
	movl	%ebp, %esi
	movl	$1, %edi
	movq	$0, 32+_dl_load_lock(%rip)
	movups	%xmm0, 16+_dl_load_lock(%rip)
	movl	$1, 16+_dl_load_lock(%rip)
	movups	%xmm0, _dl_load_lock(%rip)
	call	__run_fork_handlers
	addq	$176, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	$-1, %r12d
	movl	%eax, %fs:(%rdx)
.L7:
	testl	%ebx, %ebx
	je	.L18
	cmpq	$0, __malloc_fork_unlock_parent@GOTPCREL(%rip)
	je	.L19
	call	__malloc_fork_unlock_parent@PLT
.L19:
	call	_IO_list_unlock
.L18:
	movl	%ebp, %esi
	movl	$2, %edi
	call	__run_fork_handlers
	addq	$176, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__libc_fork, .-__libc_fork
	.weak	fork
	.set	fork,__libc_fork
	.weak	__fork
	.hidden	__fork
	.set	__fork,__libc_fork
	.weak	__malloc_fork_unlock_parent
	.weak	__nss_database_fork_subprocess
	.weak	__malloc_fork_unlock_child
	.weak	__nptl_set_robust
	.weak	__malloc_fork_lock_parent
	.weak	__nss_database_fork_prepare_parent
	.hidden	_IO_list_unlock
	.hidden	__malloc_fork_unlock_parent
	.hidden	__nss_database_fork_subprocess
	.hidden	_IO_list_resetlock
	.hidden	_IO_iter_file
	.hidden	_IO_iter_end
	.hidden	_IO_iter_next
	.hidden	_IO_iter_begin
	.hidden	__malloc_fork_unlock_child
	.hidden	__fork_generation_pointer
	.hidden	__malloc_fork_lock_parent
	.hidden	_IO_list_lock
	.hidden	__nss_database_fork_prepare_parent
	.hidden	__run_fork_handlers
