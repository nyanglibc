	.text
	.p2align 4,,15
	.globl	execv
	.type	execv, @function
execv:
	movq	__environ(%rip), %rdx
	jmp	__execve
	.size	execv, .-execv
	.hidden	__execve
