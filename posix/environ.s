	.text
	.globl	__environ
	.bss
	.align 8
	.type	__environ, @object
	.size	__environ, 8
__environ:
	.zero	8
	.weak	_environ
	.set	_environ,__environ
	.weak	environ
	.set	environ,__environ
