.text
.globl __getpid
.type __getpid,@function
.align 1<<4
__getpid:
	movl $39, %eax
	syscall
	ret
.size __getpid,.-__getpid
.weak getpid
getpid = __getpid
