	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getopt_long
	.type	getopt_long, @function
getopt_long:
	subq	$16, %rsp
	xorl	%r9d, %r9d
	pushq	$0
	call	_getopt_internal@PLT
	addq	$24, %rsp
	ret
	.size	getopt_long, .-getopt_long
	.p2align 4,,15
	.globl	_getopt_long_r
	.type	_getopt_long_r, @function
_getopt_long_r:
	subq	$8, %rsp
	pushq	$0
	pushq	%r9
	xorl	%r9d, %r9d
	call	_getopt_internal_r@PLT
	addq	$24, %rsp
	ret
	.size	_getopt_long_r, .-_getopt_long_r
	.p2align 4,,15
	.globl	getopt_long_only
	.type	getopt_long_only, @function
getopt_long_only:
	subq	$16, %rsp
	movl	$1, %r9d
	pushq	$0
	call	_getopt_internal@PLT
	addq	$24, %rsp
	ret
	.size	getopt_long_only, .-getopt_long_only
	.p2align 4,,15
	.globl	_getopt_long_only_r
	.type	_getopt_long_only_r, @function
_getopt_long_only_r:
	subq	$8, %rsp
	pushq	$0
	pushq	%r9
	movl	$1, %r9d
	call	_getopt_internal_r@PLT
	addq	$24, %rsp
	ret
	.size	_getopt_long_only_r, .-_getopt_long_only_r
