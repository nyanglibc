	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.globl	__environ
	.bss
	.align 8
	.type	__environ, @object
	.size	__environ, 8
__environ:
	.zero	8
	.weak	_environ
	.set	_environ,__environ
	.weak	environ
	.set	environ,__environ
