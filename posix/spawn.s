	.text
	.p2align 4,,15
	.globl	__posix_spawn
	.hidden	__posix_spawn
	.type	__posix_spawn, @function
__posix_spawn:
	subq	$16, %rsp
	pushq	$0
	call	__spawni
	addq	$24, %rsp
	ret
	.size	__posix_spawn, .-__posix_spawn
	.weak	posix_spawn
	.set	posix_spawn,__posix_spawn
	.hidden	__spawni
