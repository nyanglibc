.text
.globl __geteuid
.type __geteuid,@function
.align 1<<4
__geteuid:
	movl $107, %eax
	syscall
	ret
.size __geteuid,.-__geteuid
.globl __GI___geteuid
.set __GI___geteuid,__geteuid
.weak geteuid
geteuid = __geteuid
.globl __GI_geteuid
.set __GI_geteuid,geteuid
