	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __glob,glob@@GLIBC_2.27
	.symver __glob64,glob64@@GLIBC_2.27
#NO_APP
	.p2align 4,,15
	.type	next_brace_sub, @function
next_brace_sub:
.LFB61:
	shrl	$6, %esi
	xorl	%eax, %eax
	xorl	$1, %esi
	andl	$1, %esi
	.p2align 4,,10
	.p2align 3
.L2:
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.L11
.L10:
	testb	%sil, %sil
	je	.L3
	cmpb	$92, %dl
	je	.L23
.L3:
	cmpb	$125, %dl
	je	.L24
	cmpb	$44, %dl
	jne	.L8
	testq	%rax, %rax
	je	.L13
	addq	$1, %rdi
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	jne	.L10
.L11:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	testq	%rax, %rax
	leaq	-1(%rax), %rdx
	je	.L13
	addq	$1, %rdi
	movq	%rdx, %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$1, %rdi
	cmpb	$123, %dl
	jne	.L2
	addq	$1, %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L23:
	cmpb	$0, 1(%rdi)
	je	.L11
	addq	$2, %rdi
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rdi, %rax
	ret
.LFE61:
	.size	next_brace_sub, .-next_brace_sub
	.p2align 4,,15
	.type	prefix_array, @function
prefix_array:
.LFB64:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	%rdi, 24(%rsp)
	movq	%rdx, 8(%rsp)
	call	__GI_strlen
	cmpq	$1, %rax
	movq	%rax, %r13
	jne	.L26
	movq	24(%rsp), %rax
	xorl	%r13d, %r13d
	cmpb	$47, (%rax)
	setne	%r13b
.L26:
	cmpq	$0, 8(%rsp)
	je	.L27
	leaq	1(%r13), %rax
	xorl	%r14d, %r14d
	movq	%rax, 16(%rsp)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L28:
	movq	24(%rsp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	__GI_mempcpy
	leaq	1(%rax), %rdi
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movb	$47, (%rax)
	call	__GI_memcpy@PLT
	movq	%rbp, %rdi
	call	free@PLT
	movq	%r15, (%r12,%r14,8)
	addq	$1, %r14
	cmpq	%r14, 8(%rsp)
	je	.L27
.L32:
	movq	(%r12,%r14,8), %rbp
	movq	%rbp, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rbx
	movq	16(%rsp), %rax
	leaq	(%rbx,%rax), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L28
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L29:
	subq	$1, %r14
	movq	(%r12,%r14,8), %rdi
	call	free@PLT
.L41:
	testq	%r14, %r14
	jne	.L29
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.LFE64:
	.size	prefix_array, .-prefix_array
	.p2align 4,,15
	.type	collated_compare, @function
collated_compare:
.LFB63:
	movq	(%rdi), %rdi
	movq	(%rsi), %rsi
	cmpq	%rsi, %rdi
	je	.L44
	testq	%rdi, %rdi
	je	.L45
	testq	%rsi, %rsi
	je	.L46
	jmp	__GI_strcoll
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$1, %eax
	ret
.L46:
	movl	$-1, %eax
	ret
.LFE63:
	.size	collated_compare, .-collated_compare
	.p2align 4,,15
	.type	is_dir.isra.2, @function
is_dir.isra.2:
.LFB68:
	subq	$152, %rsp
	andl	$512, %esi
	movq	%rsp, %rsi
	jne	.L56
	call	__GI___stat64
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.L47
.L55:
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	$16384, %eax
	sete	%dl
.L47:
	movl	%edx, %eax
	addq	$152, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L56:
	call	*(%rdx)
	xorl	%edx, %edx
	testl	%eax, %eax
	jne	.L47
	jmp	.L55
.LFE68:
	.size	is_dir.isra.2, .-is_dir.isra.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"../posix/glob.c"
.LC1:
	.string	"old == init_names"
	.text
	.p2align 4,,15
	.type	glob_in_dir, @function
glob_in_dir:
.LFB65:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%r9, %r14
	pushq	%rbx
	movq	%rcx, %r13
	movl	%edx, %ebx
	movq	%rsi, %r12
	subq	$776, %rsp
	movq	%rdi, -736(%rbp)
	movq	%rsi, %rdi
	movq	%r8, -752(%rbp)
	call	__GI_strlen
	movq	%rax, %r15
	leaq	528(%r14), %rax
	movq	-736(%rbp), %rcx
	movq	$0, -576(%rbp)
	movq	$64, -568(%rbp)
	movq	%rax, -776(%rbp)
	movl	%ebx, %eax
	andl	$64, %eax
	movl	%eax, %edi
	movzbl	(%rcx), %eax
	testb	%al, %al
	je	.L58
	xorl	%esi, %esi
.L67:
	cmpb	$91, %al
	je	.L60
	jle	.L194
	cmpb	$92, %al
	je	.L63
	cmpb	$93, %al
	jne	.L192
	testb	$4, %sil
	jne	.L66
.L192:
	movzbl	1(%rcx), %eax
	addq	$1, %rcx
.L65:
	testb	%al, %al
	jne	.L67
	testl	%esi, %esi
	je	.L58
.L66:
	movl	%ebx, %eax
	movq	%r12, %rdi
	andl	$512, %eax
	movl	%eax, -744(%rbp)
	jne	.L195
	call	__opendir
	movq	%rax, %r15
.L82:
	testq	%r15, %r15
	je	.L196
	xorl	%eax, %eax
	movl	%ebx, %edx
	testb	$-128, %bl
	sete	%al
	sarl	$5, %edx
	leaq	-576(%rbp), %r14
	sall	$2, %eax
	andl	$2, %edx
	xorl	%r12d, %r12d
	orl	%eax, %edx
	movl	%ebx, %eax
	movq	$0, -728(%rbp)
	orb	$1, %ah
	movl	%edx, -756(%rbp)
	movq	%r14, -808(%rbp)
	movl	%eax, -760(%rbp)
	movq	%r14, -784(%rbp)
	.p2align 4,,10
	.p2align 3
.L90:
	movl	-744(%rbp), %eax
	movq	%r15, %rdi
	testl	%eax, %eax
	jne	.L197
	call	__GI___readdir64
	testq	%rax, %rax
	je	.L92
.L193:
	testb	$32, %bh
	leaq	19(%rax), %r13
	movzbl	18(%rax), %ecx
	je	.L117
	cmpq	$10, %rcx
	ja	.L90
	movl	$1, %eax
	salq	%cl, %rax
	testl	$1041, %eax
	je	.L90
.L117:
	movl	-756(%rbp), %edx
	movq	-736(%rbp), %rdi
	movq	%r13, %rsi
	call	__GI_fnmatch
	testl	%eax, %eax
	jne	.L90
	cmpq	%r12, 8(%r14)
	jne	.L95
	leaq	(%r12,%r12), %rax
	movq	%r12, %rdx
	salq	$4, %rdx
	movq	%rax, -768(%rbp)
	movabsq	$1152921504606846974, %rax
	addq	$16, %rdx
	cmpq	%rax, %r12
	ja	.L189
	movq	-776(%rbp), %rcx
	addq	%rdx, %rcx
	jc	.L99
	movq	%rcx, %rdi
	movq	%rdx, -800(%rbp)
	movq	%rcx, -792(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-792(%rbp), %rcx
	movq	-800(%rbp), %rdx
	cmpq	$4096, %rcx
	jbe	.L100
	testl	%eax, %eax
	je	.L99
.L100:
#APP
# 1381 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rdx, -784(%rbp)
#APP
# 1381 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, -776(%rbp)
	movq	%rdx, %rax
.L118:
	movq	-768(%rbp), %rdx
	movq	%r14, (%rax)
	xorl	%r12d, %r12d
	movq	%rax, %r14
	movq	%rdx, 8(%rax)
.L95:
	movq	%r13, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, 16(%r14,%r12,8)
	je	.L189
	movq	-752(%rbp), %rax
	addq	$1, -728(%rbp)
	addq	$1, %r12
	movq	-728(%rbp), %rsi
	movq	16(%rax), %rax
	movq	%rax, -768(%rbp)
	notq	%rax
	cmpq	%rsi, %rax
	ja	.L90
	.p2align 4,,10
	.p2align 3
.L189:
	movq	-784(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L104:
	testq	%r12, %r12
	je	.L105
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L106:
	movq	16(%r14,%rbx,8), %rdi
	addq	$1, %rbx
	call	free@PLT
	cmpq	%rbx, %r12
	jne	.L106
.L105:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.L198
	cmpq	%r13, %r14
	movq	8(%rbx), %r12
	je	.L127
	movq	%r14, %rdi
	call	free@PLT
.L108:
	movq	%rbx, %r14
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L58:
	movl	%ebx, %eax
	andl	$2064, %eax
	movl	%eax, -728(%rbp)
	je	.L68
	orl	$16, %ebx
.L123:
	leaq	-576(%rbp), %r14
	movl	%ebx, -760(%rbp)
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%r14, -808(%rbp)
	movq	%r14, -784(%rbp)
.L69:
	movq	-736(%rbp), %r13
	movq	%r13, %rdi
	call	__GI_strlen
	leaq	1(%rax), %rdi
	movq	%rax, %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r14,%r12,8)
	je	.L189
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	__GI_mempcpy
	addq	$1, %r12
	movb	$0, (%rax)
	movq	$1, -728(%rbp)
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L194:
	cmpb	$42, %al
	je	.L66
	cmpb	$63, %al
	je	.L66
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L60:
	orl	$4, %esi
	jmp	.L192
	.p2align 4,,10
	.p2align 3
.L63:
	testl	%edi, %edi
	movzbl	1(%rcx), %eax
	leaq	1(%rcx), %rdx
	jne	.L120
	testb	%al, %al
	je	.L66
	movzbl	2(%rcx), %eax
	orl	$2, %esi
	addq	$2, %rcx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L120:
	movq	%rdx, %rcx
	jmp	.L65
	.p2align 4,,10
	.p2align 3
.L68:
	movq	-736(%rbp), %rdi
	call	__GI_strlen
	leaq	1(%rax), %r13
	movq	-776(%rbp), %r9
	leaq	1(%r13,%r15), %r14
	addq	%r14, %r9
	jc	.L72
	movq	%r9, %rdi
	movq	%r9, -744(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-744(%rbp), %r9
	cmpq	$4096, %r9
	jbe	.L129
	testl	%eax, %eax
	je	.L72
.L129:
#APP
# 1302 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %r14
	andq	$-16, %r14
	subq	%r14, %rsp
	leaq	15(%rsp), %r14
	andq	$-16, %r14
#APP
# 1302 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	movl	$1, -728(%rbp)
.L74:
	movq	%r15, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	__GI_mempcpy
	movq	-736(%rbp), %rsi
	leaq	1(%rax), %rdi
	movb	$47, (%rax)
	movq	%r13, %rdx
	call	__GI_memcpy@PLT
	testb	$2, %bh
	leaq	-720(%rbp), %rsi
	movq	%r14, %rdi
	jne	.L199
	call	__GI___lstat64
.L77:
	testl	%eax, %eax
	jne	.L200
.L78:
	movl	-728(%rbp), %edx
	orl	$16, %ebx
	testl	%edx, %edx
	jne	.L123
	movl	$16, %r13d
.L80:
	movq	%r14, %rdi
	call	free@PLT
.L122:
	leaq	-576(%rbp), %r14
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%r14, -808(%rbp)
	movq	%r14, -784(%rbp)
.L79:
	testl	%r13d, %r13d
	movl	%ebx, -760(%rbp)
	jne	.L69
	movl	$3, %r12d
	jmp	.L102
.L99:
	movq	%rdx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L118
	jmp	.L189
.L96:
	.p2align 4,,10
	.p2align 3
.L127:
	movq	%rbx, %r13
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L197:
	movq	-752(%rbp), %rax
	call	*40(%rax)
	testq	%rax, %rax
	jne	.L193
	.p2align 4,,10
	.p2align 3
.L92:
	cmpq	$0, -728(%rbp)
	je	.L201
.L101:
	movq	-752(%rbp), %rax
	movq	-728(%rbp), %rsi
	movq	16(%rax), %rdx
	movq	(%rax), %rcx
	movabsq	$2305843009213693951, %rax
	leaq	1(%rdx,%rsi), %rsi
	subq	%rcx, %rax
	cmpq	%rsi, %rax
	jb	.L189
	movq	-752(%rbp), %rax
	leaq	1(%rcx,%rdx), %rsi
	addq	-728(%rbp), %rsi
	movq	8(%rax), %rdi
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L189
	movq	-784(%rbp), %rbx
	movq	-752(%rbp), %r13
	movq	%r14, %rdi
	movq	%r15, -728(%rbp)
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L103:
	testq	%r12, %r12
	je	.L109
	movq	0(%r13), %rsi
	movq	16(%r13), %rax
	addq	%rsi, %rax
	leaq	(%r15,%rax,8), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L110:
	movq	16(%rdi,%rax,8), %rdx
	movq	%rdx, (%rcx,%rax,8)
	addq	$1, %rax
	cmpq	%rax, %r12
	jne	.L110
	addq	%r12, %rsi
	movq	%rsi, 0(%r13)
.L109:
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.L202
	cmpq	%rbx, %rdi
	movq	8(%r14), %r12
	je	.L128
	call	free@PLT
.L113:
	movq	%r14, %rdi
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L198:
	cmpq	-808(%rbp), %r14
	jne	.L203
	movl	$1, %r12d
.L102:
	testq	%r15, %r15
	je	.L57
	testl	$512, -760(%rbp)
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%r15, %rdi
	movl	%fs:(%rbx), %r13d
	jne	.L204
	call	__closedir
.L115:
	movl	%r13d, %fs:(%rbx)
.L57:
	leaq	-40(%rbp), %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L200:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$75, %fs:(%rax)
	je	.L78
	movl	-728(%rbp), %ecx
	movl	%ebx, %r13d
	andl	$16, %r13d
	testl	%ecx, %ecx
	jne	.L122
	jmp	.L80
	.p2align 4,,10
	.p2align 3
.L72:
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L74
	movl	$1, %r12d
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%r14, %rbx
	jmp	.L113
.L196:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %esi
	cmpl	$20, %esi
	je	.L87
	testq	%r13, %r13
	je	.L88
	movq	%r12, %rdi
	call	*%r13
	testl	%eax, %eax
	jne	.L86
.L88:
	testb	$1, %bl
	jne	.L86
.L87:
	leaq	-576(%rbp), %r14
	movl	%ebx, %r13d
	xorl	%r12d, %r12d
	andl	$16, %r13d
	movq	%r14, -808(%rbp)
	movq	%r14, -784(%rbp)
	jmp	.L79
.L202:
	cmpq	-808(%rbp), %rdi
	movq	%r15, %r13
	movq	-728(%rbp), %r15
	jne	.L205
	movq	-752(%rbp), %rsi
	xorl	%r12d, %r12d
	movq	(%rsi), %rax
	addq	16(%rsi), %rax
	movq	%r13, 8(%rsi)
	movq	$0, 0(%r13,%rax,8)
	movl	-760(%rbp), %eax
	movl	%eax, 24(%rsi)
	jmp	.L102
.L204:
	movq	-752(%rbp), %rax
	call	*32(%rax)
	jmp	.L115
.L86:
	movl	$2, %r12d
	jmp	.L57
.L195:
	movq	-752(%rbp), %rax
	call	*48(%rax)
	movq	%rax, %r15
	jmp	.L82
.L199:
	movq	-752(%rbp), %rax
	call	*56(%rax)
	jmp	.L77
.L201:
	andl	$16, %ebx
	movl	%ebx, %r13d
	movl	-760(%rbp), %ebx
	jmp	.L79
.L203:
	leaq	__PRETTY_FUNCTION__.9753(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$1442, %edx
	call	__GI___assert_fail
.L205:
	leaq	__PRETTY_FUNCTION__.9753(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$1468, %edx
	call	__GI___assert_fail
.LFE65:
	.size	glob_in_dir, .-glob_in_dir
	.section	.rodata.str1.1
.LC2:
	.string	"~"
.LC3:
	.string	"/"
.LC4:
	.string	"."
.LC5:
	.string	"next != NULL"
.LC6:
	.string	"HOME"
	.text
	.p2align 4,,15
	.globl	__glob
	.type	__glob, @function
__glob:
.LFB62:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$1224, %rsp
	testq	%rdi, %rdi
	movq	%rdi, -1160(%rbp)
	movq	%rdx, -1168(%rbp)
	je	.L207
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	je	.L207
	movl	%esi, %eax
	movl	%esi, %r15d
	andl	$-32512, %eax
	movl	%eax, -1172(%rbp)
	jne	.L207
	movq	-1160(%rbp), %r14
	cmpb	$0, (%r14)
	je	.L210
	movq	%r14, %rdi
	call	__GI_strlen
	cmpb	$47, -1(%r14,%rax)
	je	.L571
.L210:
	movl	%r15d, %eax
	andl	$32, %eax
	testb	$8, %r15b
	movl	%eax, -1184(%rbp)
	jne	.L211
	testl	%eax, %eax
	movq	$0, 16(%rbx)
	jne	.L213
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
.L213:
	testl	$1024, %r15d
	je	.L222
	testb	$64, %r15b
	je	.L218
	movq	-1160(%rbp), %rdi
	movl	$123, %esi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L222
.L219:
	movq	-1160(%rbp), %rdi
	call	__GI_strlen
	leaq	-1(%rax), %r13
	movq	%r13, %rdi
	call	__GI___libc_alloca_cutoff
	testl	%eax, %eax
	jne	.L224
	cmpq	$4096, %r13
	ja	.L572
.L224:
#APP
# 390 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %r13
	andq	$-16, %r13
	subq	%r13, %rsp
	leaq	15(%rsp), %r14
	movq	%r14, %rdi
	andq	$-16, %rdi
	movq	%rdi, -1200(%rbp)
#APP
# 390 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	movq	-1160(%rbp), %rsi
	movq	%r12, %rdx
	addq	$1, %r12
	movq	%rax, -1192(%rbp)
	subq	%rsi, %rdx
	call	__GI_mempcpy
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, -1208(%rbp)
	call	next_brace_sub
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L227
	movl	$1, -1224(%rbp)
.L228:
	movq	%r14, %rax
	jmp	.L230
	.p2align 4,,10
	.p2align 3
.L231:
	movl	%r15d, %esi
	call	next_brace_sub
	testq	%rax, %rax
	je	.L573
.L230:
	cmpb	$125, (%rax)
	leaq	1(%rax), %rdi
	jne	.L231
	movq	%rdi, %r13
	call	__GI_strlen
	addq	$1, %rax
	movq	%r12, %rcx
	movq	%r14, %r12
	movq	%rax, -1216(%rbp)
	movq	(%rbx), %rax
	movq	%r13, %r14
	movq	%rax, -1232(%rbp)
	movl	%r15d, %eax
	andl	$-2097, %eax
	orl	$32, %eax
	movl	%eax, %r13d
	jmp	.L235
	.p2align 4,,10
	.p2align 3
.L232:
	cmpb	$125, (%r12)
	je	.L234
	leaq	1(%r12), %rcx
	movl	%r15d, %esi
	movq	%rcx, %rdi
	call	next_brace_sub
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L574
.L235:
	movq	-1208(%rbp), %rdi
	movq	%r12, %rdx
	movq	%rcx, %rsi
	subq	%rcx, %rdx
	call	__GI_mempcpy
	movq	-1216(%rbp), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	__GI_memcpy@PLT
	movq	-1168(%rbp), %rdx
	movq	-1200(%rbp), %rdi
	movq	%rbx, %rcx
	movl	%r13d, %esi
	call	__glob@PLT
	testl	%eax, %eax
	je	.L232
	cmpl	$3, %eax
	je	.L232
	movl	-1224(%rbp), %edi
	testl	%edi, %edi
	je	.L575
.L233:
	movl	-1184(%rbp), %esi
	movl	%eax, -1172(%rbp)
	testl	%esi, %esi
	jne	.L206
	movq	%rbx, %rdi
	call	__GI_globfree
	movq	$0, (%rbx)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L211:
	movl	-1184(%rbp), %eax
	testl	%eax, %eax
	jne	.L213
	movq	16(%rbx), %rax
	movabsq	$2305843009213693950, %rdx
	movq	$0, (%rbx)
	cmpq	%rdx, %rax
	jbe	.L576
.L225:
	movl	$1, -1172(%rbp)
.L206:
	movl	-1172(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L218:
	movq	-1160(%rbp), %r12
	movzbl	(%r12), %eax
	testb	%al, %al
	je	.L222
	.p2align 4,,10
	.p2align 3
.L220:
	cmpb	$92, %al
	je	.L577
	cmpb	$123, %al
	je	.L219
	movzbl	1(%r12), %eax
	addq	$1, %r12
.L223:
	testb	%al, %al
	jne	.L220
	.p2align 4,,10
	.p2align 3
.L222:
	movq	(%rbx), %r13
	movq	$0, -1192(%rbp)
.L217:
	movq	16(%rbx), %r12
	movq	-1160(%rbp), %rdi
	movl	$47, %esi
	leaq	0(%r13,%r12), %rax
	movq	%rax, -1216(%rbp)
	call	__GI_strrchr
	movq	%rax, %r14
	movq	-1160(%rbp), %rax
	testq	%r14, %r14
	je	.L578
	cmpq	%r14, %rax
	je	.L242
	addq	$1, %rax
	cmpq	%rax, %r14
	je	.L579
.L243:
	movq	%r14, %rax
	subq	-1160(%rbp), %rax
	movq	-1192(%rbp), %r13
	leaq	1(%rax), %r12
	movq	%rax, -1208(%rbp)
	addq	%r12, %r13
	jc	.L247
	movq	%r13, %rdi
	call	__GI___libc_alloca_cutoff
	cmpq	$4096, %r13
	jbe	.L248
	testl	%eax, %eax
	je	.L247
.L248:
#APP
# 556 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %r12
	andq	$-16, %r12
	subq	%r12, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rdx, -1184(%rbp)
#APP
# 556 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, -1192(%rbp)
	movl	$0, -1200(%rbp)
.L359:
	movq	-1208(%rbp), %r13
	movq	-1184(%rbp), %r12
	movq	-1160(%rbp), %rsi
	movq	%r13, %rdx
	movq	%r12, %rdi
	call	__GI_mempcpy
	cmpb	$0, 1(%r14)
	movb	$0, (%rax)
	jne	.L249
	cmpq	$1, %r13
	jbe	.L249
	testb	$64, %r15b
	jne	.L250
	leaq	-1(%r12,%r13), %rdx
	cmpb	$92, (%rdx)
	jne	.L250
	movq	-1184(%rbp), %rcx
	movq	%rdx, %rax
	cmpq	%rcx, %rdx
	jbe	.L251
	cmpb	$92, -1(%rdx)
	je	.L252
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L580:
	cmpb	$92, -1(%rax)
	jne	.L251
.L252:
	subq	$1, %rax
	cmpq	%rax, %rcx
	jne	.L580
	movq	-1184(%rbp), %rax
.L251:
	movq	-1208(%rbp), %rcx
	movq	-1184(%rbp), %rdi
	addq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	testb	$1, %al
	je	.L250
	movl	%r15d, %r12d
	movb	$0, (%rdx)
	movq	-1168(%rbp), %rdx
	andl	$-2065, %r12d
	movq	%rbx, %rcx
	movl	%r12d, %esi
	orl	$2, %esi
	call	__glob@PLT
	testl	%eax, %eax
	je	.L581
	cmpl	$3, %eax
	jne	.L400
	cmpl	%r12d, %r15d
	je	.L400
	movq	(%rbx), %r13
	movq	16(%rbx), %r12
	leaq	-1088(%rbp), %rcx
	movq	$0, -1080(%rbp)
	movq	%rcx, -1224(%rbp)
	leaq	0(%r13,%r12), %rax
	movq	%rax, -1216(%rbp)
	movl	%r15d, %eax
	andl	$16, %eax
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L249:
	movl	%r15d, %r13d
	addq	$1, %r14
	andl	$20480, %r13d
	jne	.L582
.L244:
	testq	%r14, %r14
	jne	.L240
	movq	16(%rbx), %rax
	addq	(%rbx), %rax
	movabsq	$2305843009213693949, %rdx
	movq	8(%rbx), %r14
	cmpq	%rdx, %rax
	ja	.L310
	leaq	16(,%rax,8), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L583
	testb	$2, %r15b
	movq	%rax, 8(%rbx)
	leaq	-16(%r12), %r13
	jne	.L584
.L312:
	movl	-1200(%rbp), %edi
	addq	%r14, %r13
	testl	%edi, %edi
	jne	.L585
	movq	-1184(%rbp), %rdi
	call	__GI___strdup
	movq	8(%rbx), %r14
	movq	%rax, 0(%r13)
	cmpq	$0, -16(%r14,%r12)
	je	.L310
.L314:
	movq	$0, -8(%r14,%r12)
	addq	$1, (%rbx)
	movl	%r15d, 24(%rbx)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L578:
	testl	$20480, %r15d
	movzbl	(%rax), %eax
	je	.L238
	cmpb	$126, %al
	je	.L586
.L238:
	testb	%al, %al
	je	.L587
	leaq	.LC4(%rip), %rax
	movq	-1160(%rbp), %r14
	movl	$0, -1200(%rbp)
	xorl	%r13d, %r13d
	movq	$0, -1208(%rbp)
	movq	%rax, -1184(%rbp)
.L240:
	movq	-1184(%rbp), %rdx
	movl	%r15d, %esi
	andl	$64, %esi
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L316
	xorl	%ecx, %ecx
	cmpb	$91, %al
	je	.L318
.L589:
	jle	.L588
	cmpb	$92, %al
	je	.L321
	cmpb	$93, %al
	jne	.L566
	testb	$4, %cl
	jne	.L326
.L566:
	movzbl	1(%rdx), %eax
	addq	$1, %rdx
.L323:
	testb	%al, %al
	je	.L325
	cmpb	$91, %al
	jne	.L589
.L318:
	orl	$4, %ecx
	jmp	.L566
	.p2align 4,,10
	.p2align 3
.L588:
	cmpb	$42, %al
	je	.L326
	cmpb	$63, %al
	jne	.L566
.L326:
	testl	%esi, %esi
	jne	.L330
	movq	-1208(%rbp), %rax
	testq	%rax, %rax
	jne	.L590
.L330:
	testl	$512, %r15d
	jne	.L591
.L333:
	movl	%r15d, %esi
	movq	-1168(%rbp), %rdx
	movq	-1184(%rbp), %rdi
	leaq	-1088(%rbp), %rcx
	andl	$577, %esi
	orl	$8196, %esi
	movq	%rcx, -1224(%rbp)
	call	__glob@PLT
	testl	%eax, %eax
	jne	.L334
	cmpq	$0, -1088(%rbp)
	je	.L336
	movl	%r15d, %r13d
	movl	%r15d, -1232(%rbp)
	andl	$-2097, %r13d
	orl	$32, %r13d
	movl	%r13d, -1208(%rbp)
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L340:
	movq	-1080(%rbp), %rax
	movq	-1192(%rbp), %r9
	movq	%rbx, %r8
	movq	-1168(%rbp), %rcx
	movl	-1208(%rbp), %edx
	movq	%r14, %rdi
	movq	(%rbx), %r15
	leaq	0(,%r13,8), %r12
	movq	(%rax,%r13,8), %rsi
	call	glob_in_dir
	cmpl	$3, %eax
	je	.L338
	testl	%eax, %eax
	jne	.L592
	movq	16(%rbx), %r11
	movq	8(%rbx), %rax
	movq	(%rbx), %rdx
	addq	%r15, %r11
	leaq	(%rax,%r11,8), %rsi
	movq	-1080(%rbp), %rax
	subq	%r15, %rdx
	movq	(%rax,%r12), %rdi
	call	prefix_array
	testl	%eax, %eax
	jne	.L568
.L338:
	addq	$1, %r13
	cmpq	%r13, -1088(%rbp)
	ja	.L340
	movl	-1232(%rbp), %r15d
.L336:
	movq	(%rbx), %r13
	movq	16(%rbx), %r12
	movl	%r15d, %r14d
	orl	$256, %r14d
	leaq	0(%r13,%r12), %rax
	cmpq	-1216(%rbp), %rax
	jne	.L341
	movl	%r15d, %eax
	movl	%r14d, %r15d
	andl	$16, %eax
	.p2align 4,,10
	.p2align 3
.L241:
	testl	%eax, %eax
	jne	.L337
	movq	-1224(%rbp), %rdi
	call	__GI_globfree
	movl	$3, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L321:
	testl	%esi, %esi
	movzbl	1(%rdx), %eax
	leaq	1(%rdx), %rdi
	jne	.L390
	testb	%al, %al
	jne	.L324
	orl	$2, %ecx
.L325:
	testb	$5, %cl
	jne	.L326
	andl	$2, %ecx
	movq	(%rbx), %r12
	je	.L329
	movq	-1184(%rbp), %rdi
	movl	$92, %esi
	call	__GI_strchr
	movq	-1208(%rbp), %rdi
	movq	%rax, %rcx
	jmp	.L349
	.p2align 4,,10
	.p2align 3
.L347:
	movb	%sil, (%rcx)
	movq	%rdx, %rsi
	movq	%rax, %rdx
	movq	%rsi, %rax
.L348:
	addq	$1, %rcx
	cmpb	$0, (%rdx)
	je	.L593
.L349:
	movzbl	(%rax), %esi
	leaq	1(%rax), %rdx
	cmpb	$92, %sil
	jne	.L347
	movzbl	1(%rax), %esi
	subq	$1, %rdi
	addq	$2, %rax
	movb	%sil, (%rcx)
	jmp	.L348
	.p2align 4,,10
	.p2align 3
.L571:
	orl	$8192, %r15d
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L583:
	movq	8(%rbx), %r14
	.p2align 4,,10
	.p2align 3
.L310:
	movq	%r14, %rdi
	call	free@PLT
	movq	$0, 8(%rbx)
	movq	$0, (%rbx)
	movl	$1, -1172(%rbp)
.L254:
	movl	-1200(%rbp), %edx
	testl	%edx, %edx
	je	.L206
	movq	-1184(%rbp), %rdi
	call	free@PLT
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L390:
	movq	%rdi, %rdx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L576:
	leaq	8(,%rax,8), %r12
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	leaq	(%rax,%r12), %rdi
	je	.L225
	.p2align 4,,10
	.p2align 3
.L215:
	movq	$0, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdi
	jne	.L215
	jmp	.L213
	.p2align 4,,10
	.p2align 3
.L582:
	movq	-1184(%rbp), %rax
	cmpb	$126, (%rax)
	jne	.L378
.L239:
	movq	-1184(%rbp), %rax
	movzbl	1(%rax), %eax
	testb	%al, %al
	je	.L255
	cmpb	$47, %al
	je	.L255
	testb	$64, %r15b
	je	.L594
	movq	-1184(%rbp), %rdi
	movl	$47, %esi
	xorl	%r13d, %r13d
	call	__GI_strchr
	movq	%rax, %r12
.L281:
	movq	%r12, %rax
	subq	-1184(%rbp), %rax
	testq	%r12, %r12
	movq	%rax, -1224(%rbp)
	je	.L280
.L282:
	movq	-1192(%rbp), %rdx
	addq	-1224(%rbp), %rdx
	jc	.L287
	movq	%rdx, %rdi
	movq	%rdx, -1232(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-1232(%rbp), %rdx
	cmpq	$4096, %rdx
	jbe	.L288
	testl	%eax, %eax
	je	.L287
.L288:
#APP
# 768 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	movq	-1224(%rbp), %rcx
	leaq	30(%rcx), %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rdx, -1232(%rbp)
#APP
# 768 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, -1192(%rbp)
	movl	$0, -1240(%rbp)
.L365:
	movq	-1184(%rbp), %rax
	testq	%r13, %r13
	leaq	1(%rax), %rsi
	je	.L289
	movq	%r13, %rdx
	movq	-1232(%rbp), %rdi
	subq	%rax, %rdx
	subq	$1, %rdx
	call	__GI_mempcpy
	cmpq	%r13, %r12
	jne	.L293
	jmp	.L565
	.p2align 4,,10
	.p2align 3
.L291:
	addq	$1, %rax
	cmpq	%r12, %rcx
	movb	%dl, -1(%rax)
	movq	%rcx, %r13
	je	.L565
.L293:
	movzbl	0(%r13), %edx
	leaq	1(%r13), %rcx
	cmpb	$92, %dl
	jne	.L291
	cmpq	%r12, %rcx
	je	.L292
	movzbl	1(%r13), %edx
	leaq	2(%r13), %rcx
	jmp	.L291
	.p2align 4,,10
	.p2align 3
.L250:
	movq	-1168(%rbp), %rdx
	movq	-1184(%rbp), %rdi
	movl	%r15d, %esi
	orl	$2, %esi
	movq	%rbx, %rcx
	call	__glob@PLT
	testl	%eax, %eax
	jne	.L400
.L369:
	movl	24(%rbx), %eax
	movl	%r15d, %edx
	andl	$2, %edx
	andl	$-3, %eax
	orl	%edx, %eax
	movl	%eax, 24(%rbx)
	jmp	.L254
.L247:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1184(%rbp)
	je	.L225
	movl	$1, -1200(%rbp)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L577:
	cmpb	$0, 1(%r12)
	je	.L222
	movzbl	2(%r12), %eax
	addq	$2, %r12
	jmp	.L223
.L401:
	movq	$0, -1192(%rbp)
	.p2align 4,,10
	.p2align 3
.L368:
	movq	-1200(%rbp), %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L227:
	andl	$-1025, %r15d
	movq	(%rbx), %r13
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L579:
	movq	-1160(%rbp), %rax
	cmpb	$92, (%rax)
	jne	.L243
	testb	$64, %r15b
	jne	.L243
	.p2align 4,,10
	.p2align 3
.L242:
	leaq	.LC3(%rip), %rax
	addq	$1, %r14
	movl	$0, -1200(%rbp)
	xorl	%r13d, %r13d
	movq	$1, -1208(%rbp)
	movq	%rax, -1184(%rbp)
	jmp	.L244
.L316:
	movq	(%rbx), %r12
.L329:
	testl	%r13d, %r13d
	jne	.L350
	movq	-1192(%rbp), %r9
	movq	-1168(%rbp), %rcx
	movq	%rbx, %r8
	movq	-1184(%rbp), %rsi
	movl	%r15d, %edx
	movq	%r14, %rdi
	call	glob_in_dir
	testl	%eax, %eax
	je	.L352
.L400:
	movl	%eax, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L573:
.L229:
	movl	-1224(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L227
	jmp	.L368
	.p2align 4,,10
	.p2align 3
.L593:
	movq	%rdi, -1208(%rbp)
.L350:
	movl	%r15d, %r13d
	movq	-1192(%rbp), %r9
	movq	-1168(%rbp), %rcx
	movq	-1184(%rbp), %rsi
	andl	$-2065, %r13d
	movq	%rbx, %r8
	movl	%r13d, %edx
	movq	%r14, %rdi
	call	glob_in_dir
	testl	%eax, %eax
	jne	.L595
	movl	%r13d, %r15d
.L352:
	cmpq	$0, -1208(%rbp)
	jne	.L596
.L346:
	testb	$2, %r15b
	je	.L353
	movq	16(%rbx), %rax
	addq	(%rbx), %rax
	movq	-1216(%rbp), %r12
	cmpq	%rax, %r12
	jnb	.L353
	leaq	64(%rbx), %r13
	leaq	0(,%r12,8), %r14
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L354:
	movq	16(%rbx), %rax
	addq	(%rbx), %rax
	addq	$1, %r12
	addq	$8, %r14
	cmpq	%r12, %rax
	jbe	.L353
.L356:
	movq	8(%rbx), %rax
	movq	%r13, %rdx
	movl	%r15d, %esi
	movq	(%rax,%r14), %rdi
	call	is_dir.isra.2
	testb	%al, %al
	je	.L354
	movq	8(%rbx), %rax
	movq	(%rax,%r14), %rdx
	movq	%rdx, %rdi
	movq	%rdx, -1168(%rbp)
	call	__GI_strlen
	movq	-1168(%rbp), %rdx
	leaq	2(%rax), %rsi
	movq	%rax, -1160(%rbp)
	movq	%rdx, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	movq	-1160(%rbp), %rcx
	je	.L569
	movl	$47, %esi
	movw	%si, (%rax,%rcx)
	movq	8(%rbx), %rdx
	movq	%rax, (%rdx,%r14)
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L353:
	andl	$4, %r15d
	jne	.L254
	movq	16(%rbx), %rsi
	addq	(%rbx), %rsi
	movl	$8, %edx
	movq	-1216(%rbp), %rcx
	movq	8(%rbx), %rax
	subq	%rcx, %rsi
	leaq	(%rax,%rcx,8), %rdi
	leaq	collated_compare(%rip), %rcx
	call	__GI_qsort
	movl	$0, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L334:
	testb	$16, %r15b
	je	.L400
	cmpl	$3, %eax
	jne	.L400
	movq	(%rbx), %r13
	movq	16(%rbx), %r12
.L337:
	addq	%r13, %r12
	movabsq	$2305843009213693949, %rax
	cmpq	%rax, %r12
	ja	.L344
	leaq	16(,%r12,8), %r12
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L344
	movq	-1160(%rbp), %rdi
	leaq	-16(%rax,%r12), %r13
	movq	%rax, 8(%rbx)
	call	__GI___strdup
	movq	%rax, 0(%r13)
	movq	8(%rbx), %rax
	cmpq	$0, -16(%rax,%r12)
	je	.L568
	addq	$1, (%rbx)
	movl	%r15d, %r14d
	movq	$0, -8(%rax,%r12)
	movl	%r15d, 24(%rbx)
.L341:
	movq	-1224(%rbp), %rdi
	movl	%r14d, %r15d
	call	__GI_globfree
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L324:
	movzbl	2(%rdx), %eax
	orl	$2, %ecx
	addq	$2, %rdx
	jmp	.L323
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-1160(%rbp), %r13
	movq	%r13, %rdi
	call	__GI_strlen
	movq	%r13, -1184(%rbp)
	movq	%rax, -1208(%rbp)
	movl	$0, -1200(%rbp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L590:
	movq	-1184(%rbp), %rcx
	leaq	-1(%rcx,%rax), %rdx
	cmpb	$92, (%rdx)
	jne	.L330
	cmpq	%rcx, %rdx
	movq	%rdx, %rax
	jbe	.L331
	cmpb	$92, -1(%rdx)
	je	.L332
	jmp	.L331
	.p2align 4,,10
	.p2align 3
.L597:
	cmpb	$92, -1(%rax)
	jne	.L331
.L332:
	subq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L597
.L331:
	movq	-1208(%rbp), %rcx
	addq	-1184(%rbp), %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	testb	$1, %al
	je	.L330
	movb	$0, (%rdx)
	jmp	.L330
	.p2align 4,,10
	.p2align 3
.L595:
	cmpl	$3, %eax
	jne	.L400
	cmpl	%r13d, %r15d
	je	.L400
	movq	(%rbx), %r13
	movq	16(%rbx), %r12
	leaq	0(%r13,%r12), %rax
	cmpq	-1216(%rbp), %rax
	je	.L598
.L397:
	movl	$3, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L594:
	cmpb	$92, %al
	je	.L599
.L257:
	movq	-1184(%rbp), %rdi
	movl	$47, %esi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L600
	movq	-1184(%rbp), %rdi
	movq	%r12, %rax
	movl	$92, %esi
	subq	%rdi, %rax
	movq	%rax, %rdx
	movq	%rax, -1224(%rbp)
	call	__GI_memchr
	movq	%rax, %r13
	jmp	.L282
	.p2align 4,,10
	.p2align 3
.L596:
	movq	(%rbx), %rdx
	movq	8(%rbx), %rax
	movq	-1184(%rbp), %rdi
	subq	%r12, %rdx
	addq	16(%rbx), %r12
	leaq	(%rax,%r12,8), %rsi
	call	prefix_array
	testl	%eax, %eax
	je	.L346
.L569:
	movq	%rbx, %rdi
	call	__GI_globfree
	movq	$0, (%rbx)
.L567:
	movl	$1, -1172(%rbp)
	jmp	.L254
.L575:
	movq	-1200(%rbp), %rdi
	movl	%eax, -1160(%rbp)
	call	free@PLT
	movl	-1160(%rbp), %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L234:
	movl	-1224(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L601
.L236:
	movq	(%rbx), %r13
	cmpq	-1232(%rbp), %r13
	jne	.L206
	testl	$2064, %r15d
	jne	.L217
	movl	$3, -1172(%rbp)
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L207:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, -1172(%rbp)
	movl	$22, %fs:(%rax)
	jmp	.L206
.L292:
	testq	%r14, %r14
	jne	.L565
	movb	$92, (%rax)
	addq	$1, %rax
	.p2align 4,,10
	.p2align 3
.L565:
	movb	$0, (%rax)
.L284:
	leaq	-1088(%rbp), %rax
	leaq	-1136(%rbp), %rsi
	movq	%r12, -1248(%rbp)
	movq	%r14, -1256(%rbp)
	movq	%rbx, -1264(%rbp)
	movl	$1024, %ecx
	leaq	16(%rax), %rdx
	movq	%rax, -1224(%rbp)
	movq	$1024, -1080(%rbp)
	leaq	-1144(%rbp), %r13
	movq	-1232(%rbp), %rbx
	movq	%rsi, %r12
	movq	%rdx, -1088(%rbp)
	movq	%rax, %r14
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L296:
	movq	%r14, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	je	.L567
	movq	-1080(%rbp), %rcx
	movq	-1088(%rbp), %rdx
.L294:
	movq	%r13, %r8
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__getpwnam_r
	cmpl	$34, %eax
	je	.L296
	movl	-1240(%rbp), %r11d
	movq	-1248(%rbp), %r12
	movq	-1256(%rbp), %r14
	movq	-1264(%rbp), %rbx
	testl	%r11d, %r11d
	jne	.L602
.L297:
	movq	-1144(%rbp), %rax
	testq	%rax, %rax
	je	.L298
	movq	32(%rax), %rax
	movq	%rax, %rdi
	movq	%rax, -1248(%rbp)
	call	__GI_strlen
	testq	%r12, %r12
	movq	%rax, %r13
	je	.L386
	movq	%r12, %rdi
	call	__GI_strlen
	movq	%rax, -1256(%rbp)
	addq	%r13, %rax
	movq	%rax, -1208(%rbp)
.L299:
	movl	-1200(%rbp), %r10d
	movl	$0, %eax
	movq	-1192(%rbp), %rcx
	testl	%r10d, %r10d
	cmovne	-1184(%rbp), %rax
	movq	%rax, -1240(%rbp)
	movq	-1208(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	-1088(%rbp), %rax
	addq	%rdx, %rcx
	movq	%rax, -1232(%rbp)
	jc	.L303
	movq	%rcx, %rdi
	movq	%rdx, -1184(%rbp)
	movq	%rcx, -1200(%rbp)
	call	__GI___libc_alloca_cutoff
	testl	%eax, %eax
	movq	-1184(%rbp), %rdx
	jne	.L304
	movq	-1200(%rbp), %rcx
	cmpq	$4096, %rcx
	ja	.L303
.L304:
#APP
# 848 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %rdx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movq	%rdx, -1184(%rbp)
#APP
# 848 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, -1192(%rbp)
	movl	$0, -1200(%rbp)
.L366:
	movq	-1248(%rbp), %rsi
	movq	-1184(%rbp), %rdi
	movq	%r13, %rdx
	call	__GI_mempcpy
	testq	%r12, %r12
	je	.L306
	movq	-1256(%rbp), %rdx
	movq	%r12, %rsi
	movq	%rax, %rdi
	call	__GI_mempcpy
.L306:
	movq	-1240(%rbp), %rdi
	movb	$0, (%rax)
	movl	$1, %r13d
	call	free@PLT
.L307:
	movq	-1224(%rbp), %rax
	movq	-1232(%rbp), %rdi
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L244
	call	free@PLT
	jmp	.L244
.L599:
	movq	-1184(%rbp), %rax
	movzbl	2(%rax), %eax
	testb	%al, %al
	je	.L255
	cmpb	$47, %al
	jne	.L257
	.p2align 4,,10
	.p2align 3
.L255:
	leaq	.LC6(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L259
	cmpb	$0, (%rax)
	je	.L259
	xorl	%r13d, %r13d
.L260:
	movq	-1184(%rbp), %rax
	cmpb	$0, 1(%rax)
	jne	.L272
	movl	-1200(%rbp), %edx
	testl	%edx, %edx
	jne	.L603
.L273:
	movq	%r12, %rdi
	call	__GI_strlen
	movl	%r13d, -1200(%rbp)
	movq	%rax, -1208(%rbp)
	movl	$1, %r13d
	movq	%r12, -1184(%rbp)
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L344:
	movq	-1224(%rbp), %rdi
	call	__GI_globfree
	movl	$1, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L584:
	movq	-1184(%rbp), %rdi
	leaq	64(%rbx), %rdx
	movl	%r15d, %esi
	call	is_dir.isra.2
	testb	%al, %al
	movq	8(%rbx), %r14
	je	.L312
	movq	-1208(%rbp), %r13
	leaq	2(%r13), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -16(%r14,%r12)
	je	.L310
	movq	-1184(%rbp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	__GI_mempcpy
	movl	-1200(%rbp), %r9d
	movl	$47, %r8d
	movw	%r8w, (%rax)
	testl	%r9d, %r9d
	je	.L314
	movq	-1184(%rbp), %rdi
	call	free@PLT
	movq	8(%rbx), %r14
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L259:
	leaq	-1088(%rbp), %r13
	leaq	-1144(%rbp), %rax
	movq	$1024, -1080(%rbp)
	movl	$1024, %esi
	movq	%rbx, -1248(%rbp)
	leaq	16(%r13), %rdi
	movq	%rax, -1232(%rbp)
	leaq	-1136(%rbp), %rax
	movq	%r13, -1224(%rbp)
	movq	%rdi, -1088(%rbp)
	movq	%rax, -1240(%rbp)
	jmp	.L264
	.p2align 4,,10
	.p2align 3
.L261:
	cmpl	$34, %eax
	jne	.L262
	movq	%r13, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	je	.L567
	movq	-1080(%rbp), %rsi
	movq	-1088(%rbp), %rdi
.L264:
	movq	$0, -1144(%rbp)
	call	__GI___getlogin_r
	testl	%eax, %eax
	jne	.L261
	movq	-1088(%rbp), %rbx
	movq	%rbx, %rdi
	call	__GI_strlen
	movq	-1080(%rbp), %rcx
	addq	$1, %rax
	movq	-1232(%rbp), %r8
	movq	-1240(%rbp), %rsi
	leaq	(%rbx,%rax), %rdx
	movq	%rbx, %rdi
	subq	%rax, %rcx
	call	__getpwnam_r
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L378:
	xorl	%r13d, %r13d
	jmp	.L244
	.p2align 4,,10
	.p2align 3
.L568:
	movq	-1224(%rbp), %rdi
	call	__GI_globfree
	movq	%rbx, %rdi
	call	__GI_globfree
	movq	$0, (%rbx)
	movl	$1, -1172(%rbp)
	jmp	.L254
.L287:
	movq	-1224(%rbp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1232(%rbp)
	je	.L567
	movl	$1, -1240(%rbp)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L592:
	movq	-1224(%rbp), %rdi
	movl	%eax, -1160(%rbp)
	call	__GI_globfree
	movq	%rbx, %rdi
	call	__GI_globfree
	movl	-1160(%rbp), %eax
	movq	$0, (%rbx)
	movl	%eax, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L280:
	movq	-1184(%rbp), %rax
	movl	$0, -1240(%rbp)
	xorl	%r12d, %r12d
	addq	$1, %rax
	movq	%rax, -1232(%rbp)
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L587:
	leaq	-1088(%rbp), %rcx
	movl	%r15d, %eax
	movq	$0, -1080(%rbp)
	andl	$16, %eax
	movl	$0, -1200(%rbp)
	movq	$0, -1184(%rbp)
	movq	%rcx, -1224(%rbp)
	jmp	.L241
.L591:
	movq	48(%rbx), %rax
	movq	%rax, -1040(%rbp)
	movq	40(%rbx), %rax
	movq	%rax, -1048(%rbp)
	movq	32(%rbx), %rax
	movq	%rax, -1056(%rbp)
	movq	64(%rbx), %rax
	movq	%rax, -1024(%rbp)
	movq	56(%rbx), %rax
	movq	%rax, -1032(%rbp)
	jmp	.L333
.L598:
	leaq	-1088(%rbp), %rcx
	movl	%r15d, %eax
	movq	$0, -1080(%rbp)
	andl	$16, %eax
	movq	%rcx, -1224(%rbp)
	jmp	.L241
.L289:
	movq	-1224(%rbp), %rdx
	movq	-1232(%rbp), %rdi
	subq	$1, %rdx
	call	__GI_mempcpy
	jmp	.L565
.L262:
	testl	%eax, %eax
	movl	%eax, %edx
	movq	-1248(%rbp), %rbx
	je	.L604
	movq	-1224(%rbp), %rax
	movq	-1088(%rbp), %rdi
	xorl	%r13d, %r13d
	addq	$16, %rax
	cmpq	%rax, %rdi
	je	.L605
.L360:
	movl	%edx, -1224(%rbp)
	call	free@PLT
	movl	-1224(%rbp), %edx
.L266:
	testl	%edx, %edx
	jne	.L402
	testq	%r12, %r12
	je	.L567
.L402:
	testq	%r12, %r12
	jne	.L606
.L268:
	testl	%r13d, %r13d
	je	.L271
	movq	%r12, %rdi
	call	free@PLT
.L271:
	movl	%r15d, %r13d
	leaq	.LC2(%rip), %r12
	andl	$16384, %r13d
	je	.L260
	jmp	.L397
.L600:
	movq	-1184(%rbp), %rdi
	movl	$92, %esi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L280
	movq	%rax, %rdi
	call	__GI_strlen@PLT
	leaq	0(%r13,%rax), %r12
	jmp	.L281
.L303:
	movq	%rdx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1184(%rbp)
	je	.L367
	movl	$1, -1200(%rbp)
	jmp	.L366
.L298:
	movl	%r15d, %r13d
	andl	$16384, %r13d
	jne	.L397
	movq	-1088(%rbp), %rax
	movq	%rax, -1232(%rbp)
	jmp	.L307
.L585:
	movq	-1184(%rbp), %rax
	movq	%rax, 0(%r13)
	jmp	.L314
.L272:
	movq	%r12, %rdi
	call	__GI_strlen
	movq	%rax, %rdx
	movq	-1208(%rbp), %rax
	movq	-1192(%rbp), %rcx
	addq	%rdx, %rax
	addq	%rax, %rcx
	movq	%rax, -1224(%rbp)
	jc	.L276
	movq	%rcx, %rdi
	movq	%rdx, -1240(%rbp)
	movq	%rcx, -1232(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-1232(%rbp), %rcx
	movq	-1240(%rbp), %rdx
	cmpq	$4096, %rcx
	jbe	.L403
	testl	%eax, %eax
	je	.L276
.L403:
#APP
# 715 "../posix/glob.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	movq	-1224(%rbp), %rcx
	addq	$30, %rcx
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	movq	%rcx, -1232(%rbp)
#APP
# 715 "../posix/glob.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, -1192(%rbp)
	movl	$0, -1240(%rbp)
.L278:
	movq	-1232(%rbp), %rdi
	movq	%r12, %rsi
	call	__GI_mempcpy
	movq	-1184(%rbp), %rcx
	movq	-1208(%rbp), %rdx
	movq	%rax, %rdi
	leaq	1(%rcx), %rsi
	call	__GI_memcpy@PLT
	movl	-1200(%rbp), %eax
	testl	%eax, %eax
	jne	.L607
.L279:
	movq	-1224(%rbp), %rax
	subq	$1, %rax
	testl	%r13d, %r13d
	movq	%rax, -1208(%rbp)
	jne	.L608
	movl	-1240(%rbp), %eax
	movl	$1, %r13d
	movl	%eax, -1200(%rbp)
	movq	-1232(%rbp), %rax
	movq	%rax, -1184(%rbp)
	jmp	.L244
.L386:
	movq	%rax, -1208(%rbp)
	movq	$0, -1256(%rbp)
	jmp	.L299
.L601:
	movq	-1200(%rbp), %rdi
	call	free@PLT
	jmp	.L236
.L604:
	movl	%eax, -1232(%rbp)
	movq	-1144(%rbp), %rax
	movl	$1, %r13d
	movq	32(%rax), %rdi
	call	__GI___strdup
	movq	%rax, %r12
	movq	-1224(%rbp), %rax
	movq	-1088(%rbp), %rdi
	movl	-1232(%rbp), %edx
	addq	$16, %rax
	cmpq	%rax, %rdi
	jne	.L360
	jmp	.L266
.L605:
	testq	%r12, %r12
	je	.L271
	cmpb	$0, (%r12)
	je	.L271
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L572:
	movq	%r13, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1200(%rbp)
	je	.L225
	movq	-1160(%rbp), %rsi
	movq	-1200(%rbp), %rdi
	movq	%r12, %rdx
	addq	$1, %r12
	subq	%rsi, %rdx
	call	__GI_mempcpy
	movl	%r15d, %esi
	movq	%r12, %rdi
	movq	%rax, -1208(%rbp)
	call	next_brace_sub
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L401
	movl	$0, -1224(%rbp)
	movq	$0, -1192(%rbp)
	jmp	.L228
.L602:
	movq	-1232(%rbp), %rdi
	call	free@PLT
	jmp	.L297
.L276:
	movq	-1224(%rbp), %rdi
	movq	%rdx, -1248(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1232(%rbp)
	movl	$1, -1240(%rbp)
	movq	-1248(%rbp), %rdx
	jne	.L278
	testl	%r13d, %r13d
	movl	$1, -1172(%rbp)
	je	.L254
	movq	%r12, %rdi
	call	free@PLT
	movl	%r13d, -1172(%rbp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L367:
	movq	-1240(%rbp), %rdi
	call	free@PLT
	movq	-1224(%rbp), %rax
	movq	-1232(%rbp), %rcx
	movl	$1, -1172(%rbp)
	addq	$16, %rax
	cmpq	%rax, %rcx
	je	.L206
	movq	%rcx, %rdi
	call	free@PLT
	jmp	.L206
.L603:
	movq	%rax, %rdi
	call	free@PLT
	jmp	.L273
.L608:
	movq	%r12, %rdi
	call	free@PLT
	movl	-1240(%rbp), %eax
	movl	%eax, -1200(%rbp)
	movq	-1232(%rbp), %rax
	movq	%rax, -1184(%rbp)
	jmp	.L244
.L607:
	movq	-1184(%rbp), %rdi
	call	free@PLT
	jmp	.L279
.L574:
	leaq	__PRETTY_FUNCTION__.9555(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$466, %edx
	call	__GI___assert_fail
.L581:
	movl	%r12d, %r15d
	jmp	.L369
.L606:
	cmpb	$0, (%r12)
	jne	.L260
	jmp	.L268
.LFE62:
	.size	__glob, .-__glob
	.globl	__glob64
	.set	__glob64,__glob
	.globl	__GI_glob
	.set	__GI_glob,__glob
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	__PRETTY_FUNCTION__.9753, @object
	.size	__PRETTY_FUNCTION__.9753, 12
__PRETTY_FUNCTION__.9753:
	.string	"glob_in_dir"
	.section	.rodata.str1.1
	.type	__PRETTY_FUNCTION__.9555, @object
	.size	__PRETTY_FUNCTION__.9555, 7
__PRETTY_FUNCTION__.9555:
	.string	"__glob"
	.hidden	__getpwnam_r
	.hidden	__closedir
	.hidden	__opendir
