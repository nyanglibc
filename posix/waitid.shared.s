	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__waitid
	.type	__waitid, @function
__waitid:
	movl	%ecx, %r10d
#APP
# 29 "../sysdeps/unix/sysv/linux/waitid.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	xorl	%r8d, %r8d
	movl	$247, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/waitid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movl	%esi, %ebp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	xorl	%r8d, %r8d
	movl	%eax, %r9d
	movl	%r13d, %r10d
	movq	%r12, %rdx
	movl	%ebp, %esi
	movl	%ebx, %edi
	movl	$247, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/waitid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r9d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__waitid, .-__waitid
	.globl	__libc_waitid
	.set	__libc_waitid,__waitid
	.weak	waitid
	.set	waitid,__waitid
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
