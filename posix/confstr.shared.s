	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"-m32"
.LC2:
	.string	"POSIXLY_CORRECT=1"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC3:
	.string	"-m32 -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64"
	.section	.rodata.str1.1
.LC4:
	.string	"-m64"
.LC5:
	.string	"NPTL 2.33"
.LC6:
	.string	"glibc 2.33"
.LC7:
	.string	"-D_LARGEFILE64_SOURCE"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI___confstr
	.hidden	__GI___confstr
	.type	__GI___confstr, @function
__GI___confstr:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$96, %rsp
	cmpl	$1117, %edi
	jg	.L3
	cmpl	$1116, %edi
	jge	.L4
	cmpl	$1007, %edi
	je	.L5
	jg	.L6
	cmpl	$5, %edi
	je	.L7
	jg	.L8
	cmpl	$2, %edi
	je	.L9
	jle	.L100
	cmpl	$3, %edi
	je	.L13
	cmpl	$4, %edi
	jne	.L2
	movl	$125, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jg	.L101
	movl	$126, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jle	.L59
	movq	%rsp, %rbp
	movl	$34, %eax
	movl	$33, %ecx
	movl	$18, %edx
	movl	$17, %edi
	movq	%rbp, %rsi
.L54:
	movdqa	.LC13(%rip), %xmm0
	addq	%rbp, %rdx
	movb	$71, 16(%rsi)
	movups	%xmm0, (%rsi)
.L35:
	movb	$10, (%rsp,%rdi)
.L36:
	movabsq	$3913712047985214040, %rsi
	movq	%rsi, (%rdx)
	movl	$13894, %esi
	movl	$1179606836, 8(%rdx)
	movw	%si, 12(%rdx)
	movb	$52, 14(%rdx)
	movb	$0, (%rsp,%rcx)
	movq	%rbp, %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$1133, %edi
	jle	.L102
	cmpl	$1139, %edi
	jle	.L103
	cmpl	$1147, %edi
	jle	.L104
	cmpl	$1149, %edi
	movl	$18, %eax
	leaq	.LC2(%rip), %rcx
	jg	.L2
	.p2align 4,,10
	.p2align 3
.L11:
	testq	%r12, %r12
	je	.L1
	testq	%rbx, %rbx
	je	.L1
	cmpq	%r12, %rax
	jbe	.L105
	leaq	-1(%r12), %rsi
	cmpq	$8, %rsi
	jnb	.L47
	testb	$4, %sil
	jne	.L106
	testq	%rsi, %rsi
	je	.L48
	movzbl	(%rcx), %edx
	testb	$2, %sil
	movb	%dl, (%rbx)
	jne	.L107
.L48:
	movb	$0, -1(%rbx,%r12)
.L1:
	addq	$96, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1105, %edi
	je	.L16
	jg	.L17
	cmpl	$1103, %edi
	jg	.L18
	cmpl	$1102, %edi
	jge	.L15
	cmpl	$1100, %edi
	jl	.L2
.L4:
	movl	$237, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	js	.L15
.L37:
	movl	$5, %eax
	leaq	.LC1(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L102:
	cmpl	$1132, %edi
	jge	.L4
	cmpl	$1123, %edi
	jg	.L21
	cmpl	$1122, %edi
	jge	.L15
	cmpl	$1120, %edi
	je	.L18
.L97:
	jle	.L15
.L16:
	movl	$238, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jns	.L37
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$1, %eax
	leaq	.LC0(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L100:
	testl	%edi, %edi
	je	.L55
	cmpl	$1, %edi
	jne	.L2
	movl	$176, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jg	.L108
	movl	$177, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jle	.L58
	movq	%rsp, %rbp
	movl	$42, %eax
	movl	$41, %esi
	movl	$22, %edx
	movl	$21, %edi
	movq	%rbp, %rcx
.L53:
	movdqa	.LC10(%rip), %xmm0
	addq	%rbp, %rdx
	movl	$1229080134, 16(%rcx)
	movb	$71, 20(%rcx)
	movups	%xmm0, (%rcx)
.L31:
	movb	$10, (%rsp,%rdi)
.L32:
	movdqa	.LC11(%rip), %xmm0
.L98:
	movl	$13894, %edi
	movb	$52, 18(%rdx)
	movq	%rbp, %rcx
	movups	%xmm0, (%rdx)
	movw	%di, 16(%rdx)
	movb	$0, (%rsp,%rsi)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L103:
	cmpl	$1138, %edi
	jge	.L15
	cmpl	$1136, %edi
	jne	.L97
.L18:
	movl	$238, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	js	.L15
	movl	$48, %eax
	leaq	.LC3(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1004, %edi
	je	.L5
	jg	.L15
	cmpl	$1000, %edi
	jge	.L15
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L105:
	cmpl	$8, %eax
	movl	%eax, %esi
	jnb	.L41
	testb	$4, %al
	jne	.L109
	testl	%esi, %esi
	je	.L1
	movzbl	(%rcx), %edx
	testb	$2, %sil
	movb	%dl, (%rbx)
	je	.L1
	movzwl	-2(%rcx,%rsi), %edx
	movw	%dx, -2(%rbx,%rsi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L47:
	movq	(%rcx), %rdx
	leaq	8(%rbx), %rdi
	andq	$-8, %rdi
	movq	%rdx, (%rbx)
	movq	-8(%rcx,%rsi), %rdx
	movq	%rdx, -8(%rbx,%rsi)
	movq	%rbx, %rdx
	subq	%rdi, %rdx
	addq	%rdx, %rsi
	subq	%rdx, %rcx
	andq	$-8, %rsi
	cmpq	$8, %rsi
	jb	.L48
	andq	$-8, %rsi
	xorl	%edx, %edx
.L51:
	movq	(%rcx,%rdx), %r8
	movq	%r8, (%rdi,%rdx)
	addq	$8, %rdx
	cmpq	%rsi, %rdx
	jb	.L51
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L41:
	movq	(%rcx), %rdx
	movq	%rdx, (%rbx)
	movl	%eax, %edx
	movq	-8(%rcx,%rdx), %rsi
	movq	%rsi, -8(%rbx,%rdx)
	leaq	8(%rbx), %rdx
	andq	$-8, %rdx
	subq	%rdx, %rbx
	leal	(%rax,%rbx), %esi
	subq	%rbx, %rcx
	andl	$-8, %esi
	cmpl	$8, %esi
	jb	.L1
	andl	$-8, %esi
	xorl	%edi, %edi
.L45:
	movl	%edi, %r8d
	addl	$8, %edi
	movq	(%rcx,%r8), %r9
	cmpl	%esi, %edi
	movq	%r9, (%rdx,%r8)
	jb	.L45
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	cmpl	$1125, %edi
	jg	.L15
.L19:
	movl	$5, %eax
	leaq	.LC4(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$10, %eax
	leaq	.LC5(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$11, %eax
	leaq	.LC6(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L55:
	movl	$14, %eax
	leaq	cs_path.3518(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$237, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jg	.L25
	movl	$238, %edi
	call	__GI___sysconf
	testq	%rax, %rax
	jle	.L110
	movq	%rsp, %rbp
	movl	$42, %eax
	movl	$41, %esi
	movl	$22, %edx
	movl	$21, %edi
	movq	%rbp, %rcx
.L26:
	movdqa	.LC8(%rip), %xmm0
	addq	%rbp, %rdx
	movl	$1229080134, 16(%rcx)
	movb	$71, 20(%rcx)
	movups	%xmm0, (%rcx)
.L28:
	movb	$10, (%rsp,%rdi)
.L27:
	movdqa	.LC9(%rip), %xmm0
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$22, %eax
	leaq	.LC7(%rip), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L17:
	cmpl	$1109, %edi
	jg	.L15
	cmpl	$1108, %edi
	jge	.L19
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L104:
	cmpl	$1142, %edi
	jl	.L19
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L101:
	movdqa	.LC12(%rip), %xmm0
	movl	$126, %edi
	movq	%rsp, %rbp
	movaps	%xmm0, (%rsp)
	call	__GI___sysconf
	testq	%rax, %rax
	jle	.L111
	movb	$10, 16(%rsp)
	movl	$51, %eax
	movl	$50, %ecx
	movl	$35, %edx
	movl	$34, %edi
	leaq	17(%rbp), %rsi
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L108:
	movdqa	.LC10(%rip), %xmm0
	movl	$177, %edi
	movl	$842221126, 16(%rsp)
	movq	%rsp, %rbp
	movaps	%xmm0, (%rsp)
	call	__GI___sysconf
	testq	%rax, %rax
	jle	.L112
	movb	$10, 20(%rsp)
	movl	$63, %eax
	movl	$62, %esi
	movl	$43, %edx
	movl	$42, %edi
	leaq	21(%rbp), %rcx
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L25:
	movdqa	.LC8(%rip), %xmm0
	movl	$238, %edi
	movl	$842221126, 16(%rsp)
	movq	%rsp, %rbp
	movaps	%xmm0, (%rsp)
	call	__GI___sysconf
	testq	%rax, %rax
	jle	.L57
	movb	$10, 20(%rsp)
	movl	$63, %eax
	movl	$62, %esi
	movl	$43, %edx
	movl	$42, %edi
	leaq	21(%rbp), %rcx
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L106:
	movl	(%rcx), %edx
	movl	%edx, (%rbx)
	movl	-4(%rcx,%rsi), %edx
	movl	%edx, -4(%rbx,%rsi)
	jmp	.L48
	.p2align 4,,10
	.p2align 3
.L59:
	movq	%rsp, %rbp
	movl	$16, %eax
	movl	$15, %ecx
	movq	%rbp, %rdx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L110:
	movq	%rsp, %rbp
	movl	$20, %eax
	movl	$19, %esi
	movq	%rbp, %rdx
	jmp	.L27
	.p2align 4,,10
	.p2align 3
.L58:
	movq	%rsp, %rbp
	movl	$20, %eax
	movl	$19, %esi
	movq	%rbp, %rdx
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L109:
	movl	(%rcx), %edx
	movl	%edx, (%rbx)
	movl	-4(%rcx,%rsi), %edx
	movl	%edx, -4(%rbx,%rsi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L112:
	leaq	21(%rbp), %rdx
	movl	$41, %eax
	movl	$40, %esi
	movl	$20, %edi
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	21(%rbp), %rdx
	movl	$41, %eax
	movl	$40, %esi
	movl	$20, %edi
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L111:
	leaq	17(%rbp), %rdx
	movl	$33, %eax
	movl	$32, %ecx
	movl	$16, %edi
	jmp	.L35
.L107:
	movzwl	-2(%rcx,%rsi), %edx
	movw	%dx, -2(%rbx,%rsi)
	jmp	.L48
	.size	__GI___confstr, .-__GI___confstr
	.globl	__confstr
	.set	__confstr,__GI___confstr
	.globl	confstr
	.set	confstr,__confstr
	.weak	__GI_confstr
	.hidden	__GI_confstr
	.set	__GI_confstr,__confstr
	.section	.rodata.str1.8
	.align 8
	.type	cs_path.3518, @object
	.size	cs_path.3518, 14
cs_path.3518:
	.string	"/bin:/usr/bin"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC8:
	.quad	3987479352875110224
	.quad	5719345247755716959
	.align 16
.LC9:
	.quad	3987479352875110224
	.quad	5066372783669267551
	.align 16
.LC10:
	.quad	3915421758837182288
	.quad	5719345247755716959
	.align 16
.LC11:
	.quad	3915421758837182288
	.quad	5066372783669267551
	.align 16
.LC12:
	.quad	5786080294529614424
	.quad	3617312193501934131
	.align 16
.LC13:
	.quad	5786080294529614424
	.quad	5278858981024936499
