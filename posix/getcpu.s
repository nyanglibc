	.text
	.p2align 4,,15
	.globl	__getcpu
	.hidden	__getcpu
	.type	__getcpu, @function
__getcpu:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	_dl_vdso_getcpu(%rip), %rax
	testq	%rax, %rax
	je	.L6
	xorl	%edx, %edx
	call	*%rax
	movslq	%eax, %rdx
	cmpq	$-4096, %rdx
	jbe	.L1
	cmpq	$-38, %rdx
	je	.L6
.L3:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%edx
	movl	%edx, %fs:(%rax)
	movl	$-1, %eax
.L5:
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movl	$309, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/getcpu.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	ja	.L3
	jmp	.L1
	.size	__getcpu, .-__getcpu
	.weak	getcpu
	.set	getcpu,__getcpu
