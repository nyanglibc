.text
.globl __sched_yield
.type __sched_yield,@function
.align 1<<4
__sched_yield:
	movl $24, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_yield,.-__sched_yield
.globl __GI___sched_yield
.set __GI___sched_yield,__sched_yield
.weak sched_yield
sched_yield = __sched_yield
.globl __GI_sched_yield
.set __GI_sched_yield,sched_yield
