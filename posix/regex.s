	.text
	.p2align 4,,15
	.type	peek_token_bracket, @function
peek_token_bracket:
	movl	56(%rsi), %eax
	cmpl	%eax, 72(%rsi)
	jle	.L23
	movq	8(%rsi), %r9
	movslq	%eax, %r8
	cmpl	$1, 104(%rsi)
	movzbl	(%r9,%r8), %ecx
	movb	%cl, (%rdi)
	jle	.L4
	cmpl	44(%rsi), %eax
	je	.L4
	movq	16(%rsi), %r10
	cmpl	$-1, (%r10,%r8,4)
	je	.L6
.L4:
	cmpb	$92, %cl
	je	.L24
	cmpb	$91, %cl
	je	.L25
	cmpb	$93, %cl
	je	.L13
	cmpb	$94, %cl
	je	.L14
	cmpb	$45, %cl
	jne	.L6
	movb	$22, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movb	$2, 8(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L25:
	addl	$1, %eax
	cmpl	64(%rsi), %eax
	jge	.L9
	movzbl	1(%r9,%r8), %eax
	cmpb	$58, %al
	movb	%al, (%rdi)
	je	.L10
	cmpb	$61, %al
	je	.L11
	cmpb	$46, %al
	jne	.L9
	movb	$26, 8(%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	andl	$1, %edx
	je	.L6
	addl	$1, %eax
	cmpl	64(%rsi), %eax
	jl	.L26
.L6:
	movb	$1, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movb	$1, 8(%rdi)
	movb	$91, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movb	$25, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movb	$21, 8(%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%eax, 56(%rsi)
	cltq
	movzbl	(%r9,%rax), %eax
	movb	$1, 8(%rdi)
	movb	%al, (%rdi)
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movb	$28, 8(%rdi)
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	andl	$4, %edx
	je	.L9
	movb	$30, 8(%rdi)
	movl	$2, %eax
	ret
	.size	peek_token_bracket, .-peek_token_bracket
	.p2align 4,,15
	.type	mark_opt_subexp, @function
mark_opt_subexp:
	cmpb	$17, 48(%rsi)
	je	.L29
.L28:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	cmpl	%edi, 40(%rsi)
	jne	.L28
	orb	$8, 50(%rsi)
	xorl	%eax, %eax
	ret
	.size	mark_opt_subexp, .-mark_opt_subexp
	.p2align 4,,15
	.type	check_dst_limits_calc_pos_1, @function
check_dst_limits_calc_pos_1:
	pushq	%r15
	pushq	%r14
	movl	%esi, %r10d
	pushq	%r13
	pushq	%r12
	movslq	%ecx, %r12
	pushq	%rbp
	pushq	%rbx
	salq	$4, %r12
	subq	$72, %rsp
	movq	112(%rdi), %r15
	movq	%rdi, (%rsp)
	movl	%ecx, 20(%rsp)
	addq	48(%r15), %r12
	movl	4(%r12), %edi
	testl	%edi, %edi
	jle	.L31
	movslq	%r8d, %rax
	movl	%edx, %ecx
	movl	%edx, %r9d
	salq	$5, %rax
	movl	%r8d, %r11d
	xorl	%ebp, %ebp
	movq	%rax, 40(%rsp)
	movl	$1, %eax
	movl	%esi, 16(%rsp)
	salq	%cl, %rax
	movq	%rax, 24(%rsp)
	notq	%rax
	movq	%rax, 32(%rsp)
	movl	%esi, %eax
	andl	$1, %eax
	movl	%eax, 12(%rsp)
	movl	%esi, %eax
	andl	$2, %eax
	movl	%eax, 8(%rsp)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L67:
	cmpb	$9, %al
	je	.L34
	cmpb	$4, %al
	je	.L65
.L32:
	leal	1(%rbp), %eax
	addq	$1, %rbp
	cmpl	%eax, 4(%r12)
	jle	.L66
.L49:
	movq	8(%r12), %rax
	movq	(%r15), %rdx
	movslq	(%rax,%rbp,4), %rbx
	movq	%rbx, %r13
	salq	$4, %rbx
	addq	%rbx, %rdx
	movzbl	8(%rdx), %eax
	cmpb	$8, %al
	jne	.L67
	movl	12(%rsp), %ecx
	testl	%ecx, %ecx
	je	.L32
	cmpl	%r9d, (%rdx)
	jne	.L32
.L47:
	addq	$72, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L65:
	cmpl	$-1, %r11d
	je	.L32
	movq	(%rsp), %rax
	movq	40(%rsp), %r14
	movq	%rbp, 48(%rsp)
	movq	%r12, 56(%rsp)
	movl	%r11d, %ebp
	movl	%r13d, %r12d
	movq	%r15, %r13
	movl	%r9d, %r15d
	addq	160(%rax), %r14
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	%r12d, (%r14)
	jne	.L38
	cmpl	$63, %r15d
	jg	.L39
	movq	24(%rsp), %rax
	testq	%rax, 16(%r14)
	je	.L38
.L39:
	movq	40(%r13), %rax
	movq	8(%rax,%rbx), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, 20(%rsp)
	je	.L68
	movl	16(%rsp), %esi
	movq	(%rsp), %rdi
	movl	%ebp, %r8d
	movl	%r15d, %edx
	call	check_dst_limits_calc_pos_1
	cmpl	$-1, %eax
	je	.L47
	testl	%eax, %eax
	jne	.L44
	movl	8(%rsp), %esi
	testl	%esi, %esi
	jne	.L48
.L44:
	cmpl	$63, %r15d
	jg	.L38
	movq	32(%rsp), %rax
	andq	%rax, 16(%r14)
	.p2align 4,,10
	.p2align 3
.L38:
	addq	$32, %r14
	cmpb	$0, -8(%r14)
	jne	.L45
	movq	56(%rsp), %r12
	movl	%ebp, %r11d
	movq	48(%rsp), %rbp
	movl	%r15d, %r9d
	movq	%r13, %r15
	leal	1(%rbp), %eax
	addq	$1, %rbp
	cmpl	%eax, 4(%r12)
	jg	.L49
	.p2align 4,,10
	.p2align 3
.L66:
	movl	16(%rsp), %r10d
.L31:
	addq	$72, %rsp
	movl	%r10d, %eax
	popq	%rbx
	sarl	%eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movl	8(%rsp), %eax
	testl	%eax, %eax
	je	.L32
	cmpl	%r9d, (%rdx)
	jne	.L32
.L48:
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L68:
	movl	16(%rsp), %r10d
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	andl	$1, %r10d
	movl	%r10d, %eax
	popq	%r12
	negl	%eax
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	check_dst_limits_calc_pos_1, .-check_dst_limits_calc_pos_1
	.p2align 4,,15
	.type	check_dst_limits_calc_pos, @function
check_dst_limits_calc_pos:
	movslq	%esi, %rsi
	salq	$5, %rsi
	addq	160(%rdi), %rsi
	movl	8(%rsi), %r10d
	cmpl	%r8d, %r10d
	jg	.L73
	movl	12(%rsi), %eax
	cmpl	%r8d, %eax
	jl	.L74
	xorl	%esi, %esi
	cmpl	%r8d, %r10d
	sete	%sil
	orl	$2, %esi
	cmpl	%r8d, %eax
	je	.L72
	xorl	%eax, %eax
	cmpl	%r8d, %r10d
	je	.L76
	rep ret
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L76:
	movl	$1, %esi
.L72:
	movl	%r9d, %r8d
	jmp	check_dst_limits_calc_pos_1
	.p2align 4,,10
	.p2align 3
.L73:
	movl	$-1, %eax
	ret
	.size	check_dst_limits_calc_pos, .-check_dst_limits_calc_pos
	.p2align 4,,15
	.type	check_dst_limits, @function
check_dst_limits:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	subq	$40, %rsp
	movq	112(%rdi), %rax
	movl	148(%rdi), %r11d
	movq	%rax, (%rsp)
	movl	%r11d, %r10d
.L78:
	cmpl	%r10d, %ebp
	jge	.L81
	leal	0(%rbp,%r10), %eax
	movq	160(%rdi), %r12
	movl	%eax, %ebx
	shrl	$31, %ebx
	addl	%eax, %ebx
	sarl	%ebx
	movslq	%ebx, %rax
	salq	$5, %rax
	cmpl	4(%r12,%rax), %ecx
	jle	.L82
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L80:
	leal	0(%rbp,%rbx), %r10d
	movl	%r10d, %eax
	shrl	$31, %eax
	addl	%r10d, %eax
	sarl	%eax
	movslq	%eax, %r10
	movl	%eax, %r13d
	salq	$5, %r10
	cmpl	4(%r12,%r10), %ecx
	jg	.L79
	movl	%eax, %ebx
.L82:
	cmpl	%ebx, %ebp
	jl	.L80
.L81:
	cmpl	%ebp, %r11d
	jle	.L94
	movslq	%ebp, %rax
	salq	$5, %rax
	addq	160(%rdi), %rax
	cmpl	4(%rax), %ecx
	movl	$-1, %eax
	cmovne	%eax, %ebp
.L83:
	movl	%r11d, %r13d
	xorl	%ebx, %ebx
.L84:
	cmpl	%r13d, %ebx
	jge	.L87
	leal	(%rbx,%r13), %eax
	movq	160(%rdi), %r14
	movl	%eax, %r10d
	shrl	$31, %r10d
	addl	%eax, %r10d
	sarl	%r10d
	movslq	%r10d, %rax
	salq	$5, %rax
	cmpl	4(%r14,%rax), %r9d
	jle	.L88
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L86:
	leal	(%rbx,%r10), %r12d
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	movslq	%eax, %r12
	movl	%eax, %r15d
	salq	$5, %r12
	cmpl	4(%r14,%r12), %r9d
	jg	.L85
	movl	%eax, %r10d
.L88:
	cmpl	%r10d, %ebx
	jl	.L86
.L87:
	cmpl	%ebx, %r11d
	jle	.L96
	movslq	%ebx, %rax
	salq	$5, %rax
	addq	160(%rdi), %rax
	cmpl	4(%rax), %r9d
	movl	$-1, %eax
	cmovne	%eax, %ebx
.L89:
	movl	4(%rsi), %eax
	testl	%eax, %eax
	jle	.L90
	movl	%r9d, 24(%rsp)
	movl	%r8d, 20(%rsp)
	movq	%rdi, %r12
	movl	%ecx, 16(%rsp)
	movl	%edx, 12(%rsp)
	xorl	%r13d, %r13d
	movl	%ebx, 28(%rsp)
	movq	%rsi, %r15
.L92:
	movq	8(%r15), %rax
	movq	(%rsp), %rdx
	movl	%ebp, %r9d
	movl	16(%rsp), %r8d
	movl	12(%rsp), %ecx
	movq	%r12, %rdi
	movslq	(%rax,%r13,4), %rax
	movq	%rax, %rsi
	salq	$5, %rax
	addq	160(%r12), %rax
	movslq	(%rax), %rax
	salq	$4, %rax
	addq	(%rdx), %rax
	movl	(%rax), %ebx
	movl	%ebx, %edx
	call	check_dst_limits_calc_pos
	movl	%eax, %r14d
	movq	8(%r15), %rax
	movl	28(%rsp), %r9d
	movl	24(%rsp), %r8d
	movl	20(%rsp), %ecx
	movl	%ebx, %edx
	movq	%r12, %rdi
	movl	(%rax,%r13,4), %esi
	call	check_dst_limits_calc_pos
	cmpl	%eax, %r14d
	je	.L104
	movl	$1, %eax
.L77:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L102:
	movl	%ebx, %r13d
	movl	%r10d, %ebx
	.p2align 4,,10
	.p2align 3
.L79:
	leal	1(%r13), %ebp
	movl	%ebx, %r10d
	jmp	.L78
	.p2align 4,,10
	.p2align 3
.L103:
	movl	%r10d, %r15d
	movl	%r13d, %r10d
	.p2align 4,,10
	.p2align 3
.L85:
	leal	1(%r15), %ebx
	movl	%r10d, %r13d
	jmp	.L84
	.p2align 4,,10
	.p2align 3
.L94:
	movl	$-1, %ebp
	jmp	.L83
	.p2align 4,,10
	.p2align 3
.L96:
	movl	$-1, %ebx
	jmp	.L89
	.p2align 4,,10
	.p2align 3
.L104:
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, 4(%r15)
	jg	.L92
.L90:
	xorl	%eax, %eax
	jmp	.L77
	.size	check_dst_limits, .-check_dst_limits
	.p2align 4,,15
	.type	postorder, @function
postorder:
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdx, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L111:
	movq	%rax, %rbx
.L107:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.L111
	movq	16(%rbx), %rax
	testq	%rax, %rax
	jne	.L111
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	*%r12
	testl	%eax, %eax
	jne	.L105
	.p2align 4,,10
	.p2align 3
.L117:
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L105
	movq	16(%rdx), %rax
	cmpq	%rbx, %rax
	movq	%rdx, %rbx
	sete	%sil
	testq	%rax, %rax
	sete	%cl
	orb	%cl, %sil
	je	.L111
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	*%r12
	testl	%eax, %eax
	je	.L117
.L105:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	postorder, .-postorder
	.p2align 4,,15
	.type	preorder, @function
preorder:
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	movq	%rdx, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L121:
	movq	%rdx, %rbx
.L119:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	*%r12
	testl	%eax, %eax
	jne	.L118
	movq	8(%rbx), %rdx
	xorl	%ecx, %ecx
	testq	%rdx, %rdx
	jne	.L121
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L128:
	movq	%rdx, %rbx
.L122:
	movq	16(%rbx), %rdx
	cmpq	%rcx, %rdx
	je	.L123
	testq	%rdx, %rdx
	jne	.L121
.L123:
	movq	(%rbx), %rdx
	movq	%rbx, %rcx
	testq	%rdx, %rdx
	jne	.L128
.L118:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	preorder, .-preorder
	.p2align 4,,15
	.type	re_string_realloc_buffers, @function
re_string_realloc_buffers:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	cmpl	$1, 104(%rdi)
	movq	%rdi, %rbx
	jle	.L131
	movslq	%esi, %rsi
	cmpq	$2147483647, %rsi
	ja	.L134
	leaq	0(,%rsi,4), %rbp
	movq	16(%rdi), %rdi
	movq	%rbp, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L134
	movq	24(%rbx), %rdi
	movq	%rax, 16(%rbx)
	testq	%rdi, %rdi
	je	.L131
	movq	%rbp, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L134
	movq	%rax, 24(%rbx)
.L131:
	cmpb	$0, 99(%rbx)
	jne	.L150
.L136:
	movl	%r12d, 52(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	movq	8(%rbx), %rdi
	movslq	%r12d, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L134
	movq	%rax, 8(%rbx)
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L134:
	popq	%rbx
	movl	$12, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	re_string_realloc_buffers, .-re_string_realloc_buffers
	.p2align 4,,15
	.type	re_node_set_insert_last, @function
re_node_set_insert_last:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movslq	4(%rdi), %rdx
	movq	8(%rdi), %rax
	cmpl	%edx, (%rdi)
	je	.L156
.L152:
	leal	1(%rdx), %ecx
	movl	%ecx, 4(%rbx)
	movl	%ebp, (%rax,%rdx,4)
	movl	$1, %eax
.L151:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L156:
	leal	2(%rdx,%rdx), %esi
	movl	%esi, (%rdi)
	movslq	%esi, %rsi
	movq	%rax, %rdi
	salq	$2, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L154
	movq	%rax, 8(%rbx)
	movslq	4(%rbx), %rdx
	jmp	.L152
	.p2align 4,,10
	.p2align 3
.L154:
	xorl	%eax, %eax
	jmp	.L151
	.size	re_node_set_insert_last, .-re_node_set_insert_last
	.p2align 4,,15
	.type	re_node_set_insert, @function
re_node_set_insert:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edx
	testl	%edx, %edx
	je	.L172
	movl	4(%rdi), %ecx
	movq	8(%rdi), %rax
	testl	%ecx, %ecx
	jne	.L162
	movl	%esi, (%rax)
	addl	$1, 4(%rdi)
	movl	$1, %eax
.L157:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L172:
	movabsq	$4294967297, %rax
	movq	%rax, (%rdi)
	movl	$4, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	je	.L173
	movl	%ebp, (%rax)
	addq	$8, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	%ecx, %edx
	je	.L174
.L163:
	movslq	%ecx, %rdx
	salq	$2, %rdx
	cmpl	%ebp, (%rax)
	jle	.L164
	testl	%ecx, %ecx
	jle	.L165
	leaq	-4(%rax,%rdx), %rdi
	leaq	(%rax,%rdx), %rsi
	leal	-1(%rcx), %edx
	movq	%rdx, %r8
	salq	$2, %rdx
	subq	%rdx, %rdi
	.p2align 4,,10
	.p2align 3
.L166:
	movl	-4(%rsi), %edx
	subq	$4, %rsi
	movl	%edx, 4(%rsi)
	cmpq	%rsi, %rdi
	jne	.L166
	subl	%ecx, %r8d
	leal	1(%r8), %edx
	movslq	%edx, %rdx
	salq	$2, %rdx
.L165:
	movl	%ebp, (%rax,%rdx)
	movl	$1, %eax
	addl	$1, 4(%rbx)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L164:
	movl	-4(%rax,%rdx), %ecx
	cmpl	%ecx, %ebp
	jge	.L165
	.p2align 4,,10
	.p2align 3
.L167:
	movl	%ecx, (%rax,%rdx)
	subq	$4, %rdx
	movl	-4(%rax,%rdx), %ecx
	cmpl	%ebp, %ecx
	jg	.L167
	jmp	.L165
	.p2align 4,,10
	.p2align 3
.L173:
	movq	$0, (%rbx)
	jmp	.L157
	.p2align 4,,10
	.p2align 3
.L174:
	addl	%ecx, %ecx
	movl	%ecx, (%rdi)
	movslq	%ecx, %rcx
	movq	%rax, %rdi
	leaq	0(,%rcx,4), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L168
	movq	%rax, 8(%rbx)
	movl	4(%rbx), %ecx
	jmp	.L163
.L168:
	xorl	%eax, %eax
	jmp	.L157
	.size	re_node_set_insert, .-re_node_set_insert
	.p2align 4,,15
	.type	register_state, @function
register_state:
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r13
	pushq	%rbx
	movslq	12(%rsi), %rdi
	movl	%edx, %ebp
	movl	%edx, (%rsi)
	movl	$0, 28(%rsi)
	leaq	24(%rsi), %r12
	movl	%edi, 24(%rsi)
	movq	%rdi, %rbx
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 32(%r13)
	je	.L182
	testl	%ebx, %ebx
	jle	.L178
	xorl	%ebx, %ebx
	jmp	.L180
	.p2align 4,,10
	.p2align 3
.L181:
	leal	1(%rbx), %eax
	addq	$1, %rbx
	cmpl	%eax, 12(%r13)
	jle	.L178
.L180:
	movq	16(%r13), %rax
	movslq	(%rax,%rbx,4), %rax
	movq	%rax, %rsi
	salq	$4, %rax
	addq	(%r14), %rax
	testb	$8, 8(%rax)
	jne	.L181
	movq	%r12, %rdi
	call	re_node_set_insert_last
	testb	%al, %al
	jne	.L181
.L182:
	popq	%rbx
	movl	$12, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L178:
	andl	132(%r14), %ebp
	salq	$4, %rbp
	addq	64(%r14), %rbp
	movslq	0(%rbp), %rdx
	movq	8(%rbp), %rax
	cmpl	%edx, 4(%rbp)
	leal	1(%rdx), %ecx
	jle	.L189
.L183:
	movl	%ecx, 0(%rbp)
	movq	%r13, (%rax,%rdx,8)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L189:
	leal	(%rcx,%rcx), %ebx
	movq	%rax, %rdi
	movslq	%ebx, %rsi
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L182
	movslq	0(%rbp), %rdx
	movq	%rax, 8(%rbp)
	movl	%ebx, 4(%rbp)
	leal	1(%rdx), %ecx
	jmp	.L183
	.size	register_state, .-register_state
	.p2align 4,,15
	.type	check_subexp_matching_top, @function
check_subexp_matching_top:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	4(%rsi), %eax
	movq	112(%rdi), %r13
	testl	%eax, %eax
	jle	.L191
	movl	%edx, %r15d
	movq	%rsi, %rbp
	movq	%rdi, %r14
	xorl	%ebx, %ebx
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L192:
	leal	1(%rbx), %eax
	addq	$1, %rbx
	cmpl	%eax, 4(%rbp)
	jle	.L191
.L197:
	movq	8(%rbp), %rax
	movslq	(%rax,%rbx,4), %rax
	movq	%rax, %r12
	salq	$4, %rax
	addq	0(%r13), %rax
	cmpb	$8, 8(%rax)
	jne	.L192
	movl	(%rax), %eax
	cmpl	$63, %eax
	jg	.L192
	movq	144(%r13), %rdx
	btq	%rax, %rdx
	jnc	.L192
	movl	172(%r14), %edx
	cmpl	%edx, 176(%r14)
	movq	184(%r14), %rcx
	je	.L206
.L193:
	movl	$32, %esi
	movl	$1, %edi
	movl	%edx, 12(%rsp)
	movq	%rcx, (%rsp)
	call	calloc@PLT
	movslq	12(%rsp), %rsi
	movq	(%rsp), %rcx
	testq	%rax, %rax
	movq	%rsi, %rdx
	movq	%rax, (%rcx,%rsi,8)
	je	.L196
	addl	$1, %edx
	movl	%r12d, 4(%rax)
	movl	%r15d, (%rax)
	leal	1(%rbx), %eax
	addq	$1, %rbx
	cmpl	%eax, 4(%rbp)
	movl	%edx, 172(%r14)
	jg	.L197
	.p2align 4,,10
	.p2align 3
.L191:
	xorl	%eax, %eax
.L190:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L196:
	movl	$12, %eax
	jmp	.L190
.L206:
	addl	%edx, %edx
	movq	%rcx, %rdi
	movslq	%edx, %rsi
	movl	%edx, (%rsp)
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	movl	(%rsp), %edx
	je	.L196
	movl	%edx, 176(%r14)
	movq	%rax, 184(%r14)
	movl	172(%r14), %edx
	jmp	.L193
	.size	check_subexp_matching_top, .-check_subexp_matching_top
	.p2align 4,,15
	.type	build_wcs_buffer, @function
build_wcs_buffer:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movl	64(%rdi), %ebx
	cmpl	%ebx, 52(%rdi)
	cmovle	52(%rdi), %ebx
	movl	44(%rdi), %r13d
	cmpl	%r13d, %ebx
	jle	.L208
	leaq	32(%rsp), %rax
	leaq	32(%rdi), %r12
	leaq	28(%rsp), %rbp
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L220:
	movq	80(%r14), %rcx
	movl	%ebx, %edx
	movq	32(%r14), %r15
	subl	%r13d, %edx
	testq	%rcx, %rcx
	jne	.L229
	movslq	40(%r14), %rsi
	movslq	%r13d, %rax
	addq	%rax, %rsi
	addq	(%r14), %rsi
.L212:
	movslq	%edx, %rdx
	movq	%r12, %rcx
	movq	%rbp, %rdi
	call	__mbrtowc
	leaq	-1(%rax), %rdx
	cmpq	$-3, %rdx
	ja	.L213
	cmpq	$-2, %rax
	movl	28(%rsp), %edi
	je	.L230
.L217:
	movq	16(%r14), %rsi
	movslq	%r13d, %rcx
	leal	1(%r13), %edx
	leaq	0(,%rcx,4), %r10
	movl	%edi, (%rsi,%rcx,4)
	leal	0(%r13,%rax), %ecx
	cmpl	%ecx, %edx
	jge	.L221
	movl	$-2, %edi
	movslq	%edx, %rdx
	leaq	4(%rsi,%r10), %rax
	subl	%r13d, %edi
	addl	%ecx, %edi
	addq	%rdi, %rdx
	leaq	4(%rsi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L219:
	movl	$-1, (%rax)
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L219
	movl	%ecx, %r13d
.L218:
	cmpl	%r13d, %ebx
	jg	.L220
.L208:
	movl	%r13d, 44(%r14)
	movl	%r13d, 48(%r14)
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	movl	%edx, %r13d
	jmp	.L218
	.p2align 4,,10
	.p2align 3
.L230:
	movl	64(%r14), %eax
	cmpl	%eax, 52(%r14)
	jl	.L215
	.p2align 4,,10
	.p2align 3
.L213:
	movl	40(%r14), %eax
	movq	(%r14), %rdx
	addl	%r13d, %eax
	cltq
	movzbl	(%rdx,%rax), %edi
	movq	80(%r14), %rdx
	testq	%rdx, %rdx
	movl	%edi, 28(%rsp)
	jne	.L231
.L216:
	movq	%r15, 32(%r14)
	movl	$1, %eax
	jmp	.L217
	.p2align 4,,10
	.p2align 3
.L229:
	movl	104(%r14), %eax
	testl	%eax, %eax
	jle	.L210
	movq	8(%rsp), %r10
	movslq	%r13d, %rsi
	xorl	%edi, %edi
	subq	%rsi, %r10
	jmp	.L211
	.p2align 4,,10
	.p2align 3
.L232:
	movq	80(%r14), %rcx
.L211:
	movl	40(%r14), %eax
	movq	(%r14), %r11
	addl	%r13d, %eax
	addl	%edi, %eax
	addl	$1, %edi
	cltq
	movzbl	(%r11,%rax), %eax
	movzbl	(%rcx,%rax), %eax
	movq	8(%r14), %rcx
	movb	%al, (%rcx,%rsi)
	movb	%al, (%r10,%rsi)
	addq	$1, %rsi
	cmpl	%edx, 104(%r14)
	movl	%edx, %eax
	cmovle	104(%r14), %eax
	cmpl	%edi, %eax
	jg	.L232
.L210:
	movq	8(%rsp), %rsi
	jmp	.L212
	.p2align 4,,10
	.p2align 3
.L215:
	movq	%r15, 32(%r14)
	jmp	.L208
.L231:
	movzbl	(%rdx,%rdi), %edi
	movl	%edi, 28(%rsp)
	jmp	.L216
	.size	build_wcs_buffer, .-build_wcs_buffer
	.p2align 4,,15
	.type	build_wcs_upper_buffer, @function
build_wcs_upper_buffer:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movl	64(%rdi), %ebx
	cmpl	%ebx, 52(%rdi)
	cmovle	52(%rdi), %ebx
	cmpb	$0, 98(%rdi)
	movl	44(%rdi), %r15d
	jne	.L234
	cmpq	$0, 80(%rdi)
	je	.L300
.L234:
	movl	48(%r13), %r12d
.L251:
	cmpl	%ebx, %r15d
	jge	.L263
	leaq	32(%r13), %rax
	movq	%rax, 8(%rsp)
	movl	%ebx, %eax
	subl	%r15d, %eax
	movl	%eax, 36(%rsp)
	cltq
	movq	%rax, 16(%rsp)
	leaq	68(%rsp), %rax
	movq	%rax, 24(%rsp)
.L242:
	movq	80(%r13), %rdx
	movq	32(%r13), %rax
	testq	%rdx, %rdx
	movq	%rax, 72(%rsp)
	jne	.L301
	movslq	40(%r13), %rbp
	movslq	%r12d, %rax
	addq	%rax, %rbp
	addq	0(%r13), %rbp
	movq	%rbp, (%rsp)
.L254:
	movq	8(%rsp), %rcx
	movq	16(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	24(%rsp), %rdi
	call	__mbrtowc
	movq	%rax, %r14
	leaq	-1(%rax), %rax
	cmpq	$-4, %rax
	ja	.L257
	movl	68(%rsp), %edx
	movslq	%r15d, %rbp
	movl	%edx, %edi
	movl	%edx, 16(%rsp)
	call	__towupper
	movl	16(%rsp), %edx
	movl	%eax, 8(%rsp)
	movl	%eax, %esi
	cmpl	%eax, %edx
	je	.L258
	leaq	80(%rsp), %r8
	leaq	72(%rsp), %rdx
	movq	%r8, %rdi
	movq	%r8, 16(%rsp)
	call	__wcrtomb
	cmpq	%rax, %r14
	movq	%rax, %r10
	movq	16(%rsp), %r8
	jne	.L259
	movq	8(%r13), %rdi
	movq	%r14, %rdx
	movq	%r8, %rsi
	addq	%rbp, %rdi
	call	memcpy@PLT
.L260:
	cmpb	$0, 100(%r13)
	jne	.L302
.L273:
	movq	16(%r13), %rsi
	leal	1(%r15), %edx
	movl	8(%rsp), %ecx
	addl	%r14d, %r12d
	addl	%r15d, %r14d
	leaq	0(,%rbp,4), %rax
	cmpl	%r14d, %edx
	movl	%ecx, (%rsi,%rbp,4)
	jge	.L283
	movl	%r14d, %ecx
	movslq	%edx, %rdx
	leaq	4(%rsi,%rax), %rax
	subl	%r15d, %ecx
	subl	$2, %ecx
	addq	%rcx, %rdx
	leaq	4(%rsi,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L276:
	movl	$-1, (%rax)
	addq	$4, %rax
	cmpq	%rdx, %rax
	jne	.L276
	movl	%r14d, %r15d
	jmp	.L251
.L278:
	movq	72(%rsp), %rax
	movq	%rax, 32(%r13)
	.p2align 4,,10
	.p2align 3
.L263:
	movl	%r15d, 44(%r13)
	movl	%r12d, 48(%r13)
	xorl	%eax, %eax
.L233:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L300:
	cmpb	$0, 100(%rdi)
	jne	.L234
	cmpl	%ebx, %r15d
	jge	.L235
	leaq	68(%rsp), %rax
	movl	%ebx, (%rsp)
	movq	%rax, 40(%rsp)
	leaq	72(%rsp), %rax
	movq	%rax, 48(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 56(%rsp)
	.p2align 4,,10
	.p2align 3
.L249:
	movslq	40(%r13), %rbp
	movq	0(%r13), %rsi
	movslq	%r15d, %r12
	leal	0(%rbp,%r15), %eax
	cltq
	movzbl	(%rsi,%rax), %r14d
	testb	$-128, %r14b
	movl	%r14d, %ebx
	jne	.L236
	movl	32(%r13), %eax
	testl	%eax, %eax
	jne	.L236
	movl	%r14d, %edi
	movq	%rsi, 8(%rsp)
	call	__towupper
	testl	$-128, %eax
	movq	8(%rsp), %rsi
	je	.L303
.L236:
	movl	(%rsp), %edi
	movq	32(%r13), %rax
	addq	%r12, %rbp
	leaq	32(%r13), %rcx
	addq	%rbp, %rsi
	subl	%r15d, %edi
	movq	%rax, 72(%rsp)
	movq	%rcx, 8(%rsp)
	movslq	%edi, %rdx
	movl	%edi, 36(%rsp)
	movq	40(%rsp), %rdi
	movq	%rdx, 16(%rsp)
	movq	%rdi, 24(%rsp)
	call	__mbrtowc
	movq	%rax, %rbp
	leaq	-1(%rax), %rax
	cmpq	$-4, %rax
	ja	.L238
	movl	68(%rsp), %r14d
	movl	%r14d, %edi
	call	__towupper
	cmpl	%eax, %r14d
	movl	%eax, %ebx
	je	.L239
	movq	56(%rsp), %r14
	movq	48(%rsp), %rdx
	movl	%eax, %esi
	movq	%r14, %rdi
	call	__wcrtomb
	cmpq	%rax, %rbp
	jne	.L240
	movq	8(%r13), %rdi
	movq	%rbp, %rdx
	movq	%r14, %rsi
	addq	%r12, %rdi
	call	memcpy@PLT
.L241:
	movq	16(%r13), %rcx
	leal	1(%r15), %edx
	addl	%r15d, %ebp
	leaq	0(,%r12,4), %rax
	cmpl	%ebp, %edx
	movl	%ebx, (%rcx,%r12,4)
	jge	.L282
	movl	$-2, %esi
	movslq	%edx, %rdx
	leaq	4(%rcx,%rax), %rax
	subl	%r15d, %esi
	addl	%ebp, %esi
	addq	%rsi, %rdx
	leaq	4(%rcx,%rdx,4), %rdx
	.p2align 4,,10
	.p2align 3
.L244:
	movl	$-1, (%rax)
	addq	$4, %rax
	cmpq	%rax, %rdx
	jne	.L244
	movl	%ebp, %r15d
.L237:
	cmpl	%r15d, (%rsp)
	jg	.L249
.L235:
	movl	%r15d, 44(%r13)
	movl	%r15d, 48(%r13)
	xorl	%eax, %eax
	jmp	.L233
	.p2align 4,,10
	.p2align 3
.L239:
	movslq	40(%r13), %rsi
	movq	8(%r13), %rdi
	movq	%rbp, %rdx
	addq	%r12, %rdi
	addq	%r12, %rsi
	addq	0(%r13), %rsi
	call	memcpy@PLT
	jmp	.L241
	.p2align 4,,10
	.p2align 3
.L282:
	movl	%edx, %r15d
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L303:
	movq	8(%r13), %rdx
	addl	$1, %r15d
	movb	%al, (%rdx,%r12)
	movq	16(%r13), %rdx
	movl	%eax, (%rdx,%r12,4)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L238:
	cmpq	$-3, %rax
	je	.L304
	movq	8(%r13), %rax
	addl	$1, %r15d
	cmpq	$-1, %rbp
	movb	%bl, (%rax,%r12)
	movq	16(%r13), %rax
	movl	%r14d, (%rax,%r12,4)
	jne	.L237
	movq	72(%rsp), %rax
	movq	%rax, 32(%r13)
	jmp	.L237
	.p2align 4,,10
	.p2align 3
.L304:
	movl	64(%r13), %eax
	cmpl	%eax, 52(%r13)
	jl	.L305
	movq	8(%r13), %rax
	addl	$1, %r15d
	movb	%bl, (%rax,%r12)
	movq	16(%r13), %rax
	movl	%r14d, (%rax,%r12,4)
	jmp	.L237
.L301:
	movl	104(%r13), %eax
	testl	%eax, %eax
	jle	.L306
	movl	36(%rsp), %ecx
	movl	40(%r13), %esi
	leaq	80(%rsp), %r8
	addl	%r12d, %esi
	cmpl	%ecx, %eax
	cmovle	%eax, %ecx
	movslq	%esi, %rsi
	addq	0(%r13), %rsi
	movl	%ecx, %edi
	xorl	%eax, %eax
.L255:
	movzbl	(%rsi,%rax), %ecx
	movzbl	(%rdx,%rcx), %ecx
	movb	%cl, (%r8,%rax)
	addq	$1, %rax
	cmpl	%eax, %edi
	jg	.L255
.L256:
	movq	%r8, (%rsp)
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L240:
	movl	(%rsp), %ebx
	movl	%r15d, %r12d
	jmp	.L242
.L258:
	movq	8(%r13), %rdi
	movq	(%rsp), %rsi
	movq	%r14, %rdx
	addq	%rbp, %rdi
	call	memcpy@PLT
	jmp	.L260
.L283:
	movl	%edx, %r15d
	jmp	.L251
.L257:
	cmpq	$-3, %rax
	jne	.L277
	movl	64(%r13), %eax
	cmpl	%eax, 52(%r13)
	jl	.L278
.L277:
	movl	40(%r13), %eax
	movq	0(%r13), %rdx
	addl	%r12d, %eax
	cltq
	movzbl	(%rdx,%rax), %eax
	movq	80(%r13), %rdx
	testq	%rdx, %rdx
	jne	.L307
.L279:
	movq	8(%r13), %rcx
	movslq	%r15d, %rdx
	movb	%al, (%rcx,%rdx)
	cmpb	$0, 100(%r13)
	jne	.L308
.L280:
	movq	16(%r13), %rcx
	addl	$1, %r12d
	addl	$1, %r15d
	cmpq	$-1, %r14
	movl	%eax, (%rcx,%rdx,4)
	jne	.L251
	movq	72(%rsp), %rax
	movq	%rax, 32(%r13)
	jmp	.L251
.L302:
	movq	24(%r13), %rax
	leaq	(%rax,%rbp,4), %rsi
	xorl	%eax, %eax
.L274:
	leal	(%r12,%rax), %edx
	movl	%edx, (%rsi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %r14
	jne	.L274
	jmp	.L273
.L305:
	movq	72(%rsp), %rax
	movq	%rax, 32(%r13)
	jmp	.L235
.L259:
	cmpq	$-1, %rax
	je	.L258
	movslq	52(%r13), %rax
	leaq	0(%rbp,%r10), %rdx
	cmpq	%rax, %rdx
	ja	.L278
	cmpq	$0, 24(%r13)
	je	.L309
.L264:
	cmpb	$0, 100(%r13)
	jne	.L265
	testq	%rbp, %rbp
	je	.L266
	movq	24(%r13), %rdx
	xorl	%eax, %eax
.L267:
	movl	%eax, (%rdx,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %rbp
	jne	.L267
.L266:
	movb	$1, 100(%r13)
.L265:
	movq	8(%r13), %rdi
	movq	%r10, %rdx
	movq	%r8, %rsi
	movq	%r10, (%rsp)
	addq	%rbp, %rdi
	call	memcpy@PLT
	movq	16(%r13), %rdi
	leaq	0(,%rbp,4), %rdx
	movq	(%rsp), %r10
	movl	8(%rsp), %eax
	movl	%r14d, %ecx
	addq	%rdx, %rdi
	addq	24(%r13), %rdx
	cmpq	$1, %r10
	movl	%eax, (%rdi)
	movl	%r12d, (%rdx)
	jbe	.L268
	movl	$1, %eax
	subl	$1, %ecx
.L271:
	cmpq	%r14, %rax
	movl	%ecx, %esi
	cmovb	%eax, %esi
	addl	%r12d, %esi
	movl	%esi, (%rdx,%rax,4)
	movl	$-1, (%rdi,%rax,4)
	addq	$1, %rax
	cmpq	%rax, %r10
	jne	.L271
.L268:
	movl	64(%r13), %ebx
	movl	%r10d, %eax
	subl	%r14d, %eax
	addl	%eax, %ebx
	cmpl	%r12d, 68(%r13)
	movl	%ebx, 64(%r13)
	jle	.L272
	addl	%eax, 72(%r13)
.L272:
	cmpl	%ebx, 52(%r13)
	cmovle	52(%r13), %ebx
	addl	%r10d, %r15d
	addl	%r14d, %r12d
	jmp	.L251
.L306:
	leaq	80(%rsp), %r8
	jmp	.L256
.L309:
	leaq	0(,%rax,4), %rdi
	movq	%r8, 16(%rsp)
	movq	%r10, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%r13)
	movq	(%rsp), %r10
	movq	16(%rsp), %r8
	jne	.L264
	movl	$12, %eax
	jmp	.L233
.L308:
	movq	24(%r13), %rcx
	movl	%r12d, (%rcx,%rdx,4)
	jmp	.L280
.L307:
	movzbl	(%rdx,%rax), %eax
	jmp	.L279
	.size	build_wcs_upper_buffer, .-build_wcs_upper_buffer
	.p2align 4,,15
	.type	build_upper_buffer, @function
build_upper_buffer:
	movl	64(%rdi), %r9d
	cmpl	%r9d, 52(%rdi)
	cmovle	52(%rdi), %r9d
	movl	44(%rdi), %eax
	cmpl	%eax, %r9d
	jle	.L311
	movq	__libc_tsd_CTYPE_TOUPPER@gottpoff(%rip), %r10
	movslq	%eax, %rcx
	.p2align 4,,10
	.p2align 3
.L313:
	movl	40(%rdi), %edx
	movq	80(%rdi), %rsi
	movq	(%rdi), %r8
	addl	%eax, %edx
	testq	%rsi, %rsi
	movslq	%edx, %rdx
	movzbl	(%r8,%rdx), %edx
	jne	.L318
.L312:
	movq	%fs:(%r10), %r8
	movq	8(%rdi), %rsi
	addl	$1, %eax
	movl	(%r8,%rdx,4), %edx
	movb	%dl, (%rsi,%rcx)
	addq	$1, %rcx
	cmpl	%eax, %r9d
	jne	.L313
.L311:
	movl	%eax, 44(%rdi)
	movl	%eax, 48(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L318:
	movzbl	(%rsi,%rdx), %edx
	jmp	.L312
	.size	build_upper_buffer, .-build_upper_buffer
	.p2align 4,,15
	.type	extend_buffers, @function
extend_buffers:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	52(%rdi), %eax
	cmpl	$1073741822, %eax
	ja	.L323
	movl	64(%rdi), %edx
	addl	%eax, %eax
	movq	%rdi, %rbx
	cmpl	%edx, %eax
	cmovg	%edx, %eax
	cmpl	%esi, %eax
	cmovge	%eax, %esi
	call	re_string_realloc_buffers
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L319
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L322
	movl	52(%rbx), %eax
	leal	1(%rax), %esi
	movslq	%esi, %rsi
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L323
	movq	%rax, 136(%rbx)
.L322:
	cmpb	$0, 96(%rbx)
	movl	104(%rbx), %eax
	je	.L324
	cmpl	$1, %eax
	movq	%rbx, %rdi
	jle	.L325
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	build_wcs_upper_buffer
	.p2align 4,,10
	.p2align 3
.L324:
	cmpl	$1, %eax
	jg	.L339
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L319
	movl	64(%rbx), %r8d
	cmpl	%r8d, 52(%rbx)
	cmovle	52(%rbx), %r8d
	movl	44(%rbx), %eax
	cmpl	%eax, %r8d
	jle	.L327
	movslq	%eax, %rcx
	jmp	.L328
	.p2align 4,,10
	.p2align 3
.L340:
	movq	80(%rbx), %rsi
.L328:
	movl	40(%rbx), %edx
	movq	(%rbx), %rdi
	addl	%eax, %edx
	addl	$1, %eax
	movslq	%edx, %rdx
	movzbl	(%rdi,%rdx), %edx
	movzbl	(%rsi,%rdx), %esi
	movq	8(%rbx), %rdx
	movb	%sil, (%rdx,%rcx)
	addq	$1, %rcx
	cmpl	%eax, %r8d
	jne	.L340
.L327:
	movl	%eax, 44(%rbx)
	movl	%eax, 48(%rbx)
.L319:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L339:
	movq	%rbx, %rdi
	call	build_wcs_buffer
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L323:
	movl	$12, %ebp
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	call	build_upper_buffer
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	extend_buffers, .-extend_buffers
	.p2align 4,,15
	.type	clean_state_log_if_needed, @function
clean_state_log_if_needed:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movl	52(%rdi), %eax
	movq	%rdi, %rbx
	movslq	144(%rdi), %r12
	cmpl	%esi, %eax
	jg	.L342
	cmpl	64(%rdi), %eax
	jge	.L342
	leal	1(%rbp), %esi
	movq	%rbx, %rdi
	call	extend_buffers
	testl	%eax, %eax
	jne	.L341
.L347:
	cmpl	%ebp, %r12d
	jl	.L345
.L349:
	xorl	%eax, %eax
.L341:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L342:
	movl	44(%rbx), %eax
	cmpl	%ebp, %eax
	jg	.L347
	cmpl	64(%rbx), %eax
	jge	.L347
	leal	1(%rbp), %esi
	movq	%rbx, %rdi
	call	extend_buffers
	testl	%eax, %eax
	je	.L347
	jmp	.L341
	.p2align 4,,10
	.p2align 3
.L345:
	movq	136(%rbx), %rax
	movl	%ebp, %edx
	xorl	%esi, %esi
	subl	%r12d, %edx
	movslq	%edx, %rdx
	leaq	8(%rax,%r12,8), %rdi
	salq	$3, %rdx
	call	memset@PLT
	movl	%ebp, 144(%rbx)
	jmp	.L349
	.size	clean_state_log_if_needed, .-clean_state_log_if_needed
	.p2align 4,,15
	.type	free_charset, @function
free_charset:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	call	free@PLT
	movq	8(%rbx), %rdi
	call	free@PLT
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	24(%rbx), %rdi
	call	free@PLT
	movq	32(%rbx), %rdi
	call	free@PLT
	movq	40(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.size	free_charset, .-free_charset
	.p2align 4,,15
	.type	free_token, @function
free_token:
	movl	8(%rdi), %eax
	andl	$262399, %eax
	cmpl	$6, %eax
	je	.L355
	cmpl	$3, %eax
	je	.L356
	rep ret
	.p2align 4,,10
	.p2align 3
.L356:
	movq	(%rdi), %rdi
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L355:
	movq	(%rdi), %rdi
	jmp	free_charset
	.size	free_token, .-free_token
	.p2align 4,,15
	.type	free_tree, @function
free_tree:
	leaq	40(%rsi), %rdi
	subq	$8, %rsp
	call	free_token
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	free_tree, .-free_tree
	.p2align 4,,15
	.type	re_dfa_add_node, @function
re_dfa_add_node:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	16(%rdi), %rdi
	movq	8(%rbx), %r12
	cmpq	%r12, %rdi
	jnb	.L360
	movq	24(%rbx), %rbp
	movq	40(%rbx), %r12
.L361:
	movq	(%rbx), %rax
	movq	%rdi, %rsi
	movl	%edx, %ecx
	salq	$4, %rsi
	andl	$-261889, %ecx
	addq	%rsi, %rax
	movq	%rdx, 8(%rax)
	movl	%ecx, 8(%rax)
	xorl	%ecx, %ecx
	cmpb	$6, %dl
	movq	%r15, (%rax)
	sete	%cl
	cmpb	$5, %dl
	jne	.L369
	xorl	%ecx, %ecx
	cmpl	$1, 164(%rbx)
	setg	%cl
.L369:
	movzbl	10(%rax), %edx
	pxor	%xmm0, %xmm0
	sall	$4, %ecx
	andl	$-17, %edx
	orl	%ecx, %edx
	movb	%dl, 10(%rax)
	movl	$-1, 0(%rbp,%rdi,4)
	movups	%xmm0, (%r12,%rsi)
	movq	16(%rbx), %rax
	salq	$4, %rax
	addq	48(%rbx), %rax
	movups	%xmm0, (%rax)
	movq	16(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 16(%rbx)
.L359:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L360:
	leaq	(%r12,%r12), %r14
	cmpq	$2147483647, %r14
	ja	.L374
	movq	%r12, %r13
	movq	(%rbx), %rdi
	movq	%rdx, 8(%rsp)
	salq	$5, %r13
	movq	%r13, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L374
	movq	24(%rbx), %rdi
	salq	$3, %r12
	movq	%rax, (%rbx)
	movq	%r12, %rsi
	call	realloc@PLT
	movq	32(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbp
	call	realloc@PLT
	movq	40(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, (%rsp)
	call	realloc@PLT
	movq	48(%rbx), %rdi
	movq	%r13, %rsi
	movq	%rax, %r12
	call	realloc@PLT
	testq	%rbp, %rbp
	movq	(%rsp), %rcx
	je	.L365
	testq	%rcx, %rcx
	je	.L365
	testq	%r12, %r12
	je	.L365
	testq	%rax, %rax
	movq	8(%rsp), %rdx
	je	.L365
	movq	%rbp, 24(%rbx)
	movq	%rcx, 32(%rbx)
	movq	%r12, 40(%rbx)
	movq	%rax, 48(%rbx)
	movq	%r14, 8(%rbx)
	movq	16(%rbx), %rdi
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L365:
	movq	%rbp, %rdi
	movq	%rax, 8(%rsp)
	movq	%rcx, (%rsp)
	call	free@PLT
	movq	(%rsp), %rcx
	movq	%rcx, %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	movq	8(%rsp), %rax
	movq	%rax, %rdi
	call	free@PLT
.L374:
	movl	$-1, %eax
	jmp	.L359
	.size	re_dfa_add_node, .-re_dfa_add_node
	.p2align 4,,15
	.type	duplicate_node, @function
duplicate_node:
	pushq	%r13
	pushq	%r12
	movslq	%esi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r13, %rbp
	salq	$4, %r13
	movl	%edx, %r12d
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rax
	movq	(%rax,%r13), %rsi
	movq	8(%rax,%r13), %rdx
	call	re_dfa_add_node
	cmpl	$-1, %eax
	je	.L375
	movq	(%rbx), %r8
	movslq	%eax, %rdi
	andw	$1023, %r12w
	movq	%rdi, %rsi
	movl	%r12d, %ecx
	salq	$4, %rsi
	andl	$1023, %ecx
	addq	%r8, %rsi
	sall	$8, %ecx
	movl	%ecx, %edx
	movl	8(%rsi), %ecx
	andl	$-261889, %ecx
	orl	%edx, %ecx
	movl	%ecx, 8(%rsi)
	movl	8(%r8,%r13), %edx
	andl	$-261889, %ecx
	shrl	$8, %edx
	orl	%r12d, %edx
	andl	$1023, %edx
	sall	$8, %edx
	orl	%ecx, %edx
	movl	%edx, %ecx
	movl	%edx, 8(%rsi)
	shrl	$16, %ecx
	movl	%ecx, %edx
	orl	$4, %edx
	movb	%dl, 10(%rsi)
	movq	32(%rbx), %rdx
	movl	%ebp, (%rdx,%rdi,4)
.L375:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	duplicate_node, .-duplicate_node
	.p2align 4,,15
	.type	duplicate_node_closure, @function
duplicate_node_closure:
	pushq	%r15
	pushq	%r14
	movl	%esi, %r10d
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %r13d
	movl	%r8d, %r12d
	movq	$-16, %rbp
	subq	$40, %rsp
	movl	%ecx, 20(%rsp)
.L382:
	movq	(%r14), %rax
	movslq	%r10d, %r15
	movq	40(%r14), %rdi
	movq	%r15, %r9
	movslq	%r13d, %rbx
	salq	$4, %r9
	leaq	(%rax,%r9), %rsi
	cmpb	$4, 8(%rsi)
	je	.L432
	leaq	(%rdi,%r9), %r8
	movl	4(%r8), %ecx
	testl	%ecx, %ecx
	je	.L433
	movq	8(%r8), %r8
	salq	$4, %rbx
	addq	%rbx, %rdi
	cmpl	$1, %ecx
	movl	(%r8), %r15d
	movl	$0, 4(%rdi)
	je	.L434
	movl	16(%r14), %edx
	leal	-1(%rdx), %esi
	movslq	%esi, %rdx
	movq	%rdx, %rcx
	salq	$4, %rcx
	addq	%rcx, %rax
	testb	$4, 10(%rax)
	je	.L391
	testl	%esi, %esi
	jle	.L391
	movq	32(%r14), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L392:
	addq	%rbp, %rax
	subl	$1, %esi
	subq	$4, %rdx
	testb	$4, 10(%rax)
	je	.L391
	testl	%esi, %esi
	jle	.L391
.L394:
	cmpl	%r15d, (%rdx)
	jne	.L392
	movl	8(%rax), %ecx
	shrl	$8, %ecx
	andl	$1023, %ecx
	cmpl	%ecx, %r12d
	jne	.L392
	movq	%r9, 8(%rsp)
	call	re_node_set_insert
	testb	%al, %al
	movq	8(%rsp), %r9
	je	.L387
.L397:
	movq	40(%r14), %rax
	movl	%r12d, %edx
	movq	%r14, %rdi
	movq	8(%rax,%r9), %rax
	movl	4(%rax), %r10d
	movl	%r10d, %esi
	movl	%r10d, 8(%rsp)
	call	duplicate_node
	cmpl	$-1, %eax
	movl	%eax, %r13d
	je	.L387
	addq	40(%r14), %rbx
	movq	%rbx, %rdi
.L431:
	movl	%r13d, %esi
	call	re_node_set_insert
	testb	%al, %al
	movl	8(%rsp), %r10d
	jne	.L382
.L387:
	movl	$12, %eax
.L381:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L391:
	movl	%r12d, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	movq	%r9, 8(%rsp)
	call	duplicate_node
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movq	8(%rsp), %r9
	je	.L387
	movq	40(%r14), %rdi
	movl	%r13d, %esi
	movq	%r9, 8(%rsp)
	addq	%rbx, %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L387
	movl	20(%rsp), %ecx
	movl	%r12d, %r8d
	movl	%r13d, %edx
	movl	%r15d, %esi
	movq	%r14, %rdi
	call	duplicate_node_closure
	testl	%eax, %eax
	movq	8(%rsp), %r9
	je	.L397
	jmp	.L381
	.p2align 4,,10
	.p2align 3
.L434:
	cmpl	20(%rsp), %r10d
	jne	.L390
	cmpl	%r13d, %r10d
	jne	.L435
.L390:
	movl	8(%rsi), %eax
	movq	%r14, %rdi
	movl	%r15d, %esi
	shrl	$8, %eax
	andl	$1023, %eax
	orl	%eax, %r12d
	movl	%r12d, %edx
	call	duplicate_node
	cmpl	$-1, %eax
	movl	%eax, %r13d
	je	.L387
	addq	40(%r14), %rbx
	movl	%eax, %esi
	movq	%rbx, %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L387
	movl	%r15d, %r10d
	jmp	.L382
	.p2align 4,,10
	.p2align 3
.L432:
	movq	24(%r14), %rax
	movq	%rbx, %rcx
	movl	%r12d, %edx
	salq	$4, %rcx
	movq	%rcx, 24(%rsp)
	movl	(%rax,%r15,4), %r10d
	movl	$0, 4(%rdi,%rcx)
	movq	%r14, %rdi
	movl	%r10d, %esi
	movl	%r10d, 8(%rsp)
	call	duplicate_node
	cmpl	$-1, %eax
	movl	%eax, %r13d
	movl	8(%rsp), %r10d
	movq	24(%rsp), %rcx
	je	.L387
	movq	24(%r14), %rax
	movq	40(%r14), %rdi
	movl	%r10d, 8(%rsp)
	movl	(%rax,%r15,4), %esi
	addq	%rcx, %rdi
	movl	%esi, (%rax,%rbx,4)
	jmp	.L431
.L433:
	movq	24(%r14), %rax
	movl	(%rax,%r15,4), %edx
	movl	%edx, (%rax,%rbx,4)
	xorl	%eax, %eax
	jmp	.L381
.L435:
	movl	%r15d, %esi
	call	re_node_set_insert
	movl	%eax, %edx
	xorl	%eax, %eax
	testb	%dl, %dl
	jne	.L381
	jmp	.L387
	.size	duplicate_node_closure, .-duplicate_node_closure
	.p2align 4,,15
	.type	free_state, @function
free_state:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rdi), %rdi
	call	free@PLT
	movq	48(%rbx), %rdi
	call	free@PLT
	movq	56(%rbx), %rax
	leaq	8(%rbx), %rdx
	cmpq	%rdx, %rax
	je	.L437
	movq	8(%rax), %rdi
	call	free@PLT
	movq	56(%rbx), %rdi
	call	free@PLT
.L437:
	movq	16(%rbx), %rdi
	call	free@PLT
	movq	72(%rbx), %rdi
	call	free@PLT
	movq	64(%rbx), %rdi
	call	free@PLT
	movq	%rbx, %rdi
	popq	%rbx
	jmp	free@PLT
	.size	free_state, .-free_state
	.p2align 4,,15
	.type	re_string_destruct, @function
re_string_destruct:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	16(%rdi), %rdi
	call	free@PLT
	movq	24(%rbx), %rdi
	call	free@PLT
	cmpb	$0, 99(%rbx)
	jne	.L442
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L442:
	movq	8(%rbx), %rdi
	popq	%rbx
	jmp	free@PLT
	.size	re_string_destruct, .-re_string_destruct
	.p2align 4,,15
	.type	free_dfa_content, @function
free_dfa_content:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L444
	cmpq	$0, 16(%r13)
	je	.L444
	xorl	%ebx, %ebx
	jmp	.L445
	.p2align 4,,10
	.p2align 3
.L475:
	movq	0(%r13), %rdi
.L445:
	movq	%rbx, %rax
	addq	$1, %rbx
	salq	$4, %rax
	addq	%rax, %rdi
	call	free_token
	cmpq	%rbx, 16(%r13)
	ja	.L475
.L444:
	movq	24(%r13), %rdi
	call	free@PLT
	cmpq	$0, 16(%r13)
	je	.L446
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L450:
	movq	48(%r13), %rax
	testq	%rax, %rax
	je	.L447
	movq	%rbx, %rdx
	salq	$4, %rdx
	movq	8(%rax,%rdx), %rdi
	call	free@PLT
.L447:
	movq	56(%r13), %rax
	testq	%rax, %rax
	je	.L448
	movq	%rbx, %rdx
	salq	$4, %rdx
	movq	8(%rax,%rdx), %rdi
	call	free@PLT
.L448:
	movq	40(%r13), %rax
	testq	%rax, %rax
	je	.L449
	movq	%rbx, %rdx
	salq	$4, %rdx
	movq	8(%rax,%rdx), %rdi
	call	free@PLT
.L449:
	addq	$1, %rbx
	cmpq	%rbx, 16(%r13)
	ja	.L450
.L446:
	movq	40(%r13), %rdi
	call	free@PLT
	movq	48(%r13), %rdi
	call	free@PLT
	movq	56(%r13), %rdi
	call	free@PLT
	movq	0(%r13), %rdi
	call	free@PLT
	movq	64(%r13), %rdi
	testq	%rdi, %rdi
	je	.L451
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L455:
	leaq	(%rdi,%r15), %r12
	movl	(%r12), %eax
	testl	%eax, %eax
	jle	.L452
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L453:
	movq	8(%r12), %rax
	addl	$1, %ebx
	movq	(%rax,%rbp), %rdi
	addq	$8, %rbp
	call	free_state
	cmpl	%ebx, (%r12)
	jg	.L453
.L452:
	movq	8(%r12), %rdi
	addl	$1, %r14d
	addq	$16, %r15
	call	free@PLT
	cmpl	%r14d, 132(%r13)
	movq	64(%r13), %rdi
	jnb	.L455
.L451:
	call	free@PLT
	movq	120(%r13), %rdi
	leaq	utf8_sb_map(%rip), %rax
	cmpq	%rax, %rdi
	je	.L456
	call	free@PLT
.L456:
	movq	208(%r13), %rdi
	call	free@PLT
	addq	$8, %rsp
	movq	%r13, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	free@PLT
	.size	free_dfa_content, .-free_dfa_content
	.p2align 4,,15
	.type	match_ctx_clean, @function
match_ctx_clean:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	172(%rdi), %edx
	testl	%edx, %edx
	jle	.L477
	xorl	%r14d, %r14d
	movl	$0, 12(%rsp)
	.p2align 4,,10
	.p2align 3
.L481:
	movq	184(%r15), %rax
	movq	(%rax,%r14), %r13
	movl	20(%r13), %eax
	testl	%eax, %eax
	jle	.L478
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L479:
	movq	24(%r13), %rdx
	addl	$1, %ebx
	movq	(%rdx,%rbp), %r12
	addq	$8, %rbp
	movq	16(%r12), %rdi
	call	free@PLT
	movq	%r12, %rdi
	call	free@PLT
	cmpl	%ebx, 20(%r13)
	jg	.L479
.L478:
	movq	24(%r13), %rdi
	call	free@PLT
	movq	8(%r13), %rdx
	testq	%rdx, %rdx
	je	.L480
	movq	8(%rdx), %rdi
	call	free@PLT
	movq	8(%r13), %rdi
	call	free@PLT
.L480:
	movq	%r13, %rdi
	addq	$8, %r14
	call	free@PLT
	addl	$1, 12(%rsp)
	movl	12(%rsp), %eax
	cmpl	%eax, 172(%r15)
	jg	.L481
.L477:
	movl	$0, 172(%r15)
	movl	$0, 148(%r15)
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	match_ctx_clean, .-match_ctx_clean
	.p2align 4,,15
	.type	pop_fail_stack, @function
pop_fail_stack:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	(%rdi), %eax
	movslq	%edx, %rdx
	movq	%r8, %r12
	salq	$3, %rdx
	leal	-1(%rax), %ebx
	movq	8(%rdi), %rax
	movl	%ebx, (%rdi)
	movslq	%ebx, %rbx
	salq	$5, %rbx
	addq	%rbx, %rax
	movl	(%rax), %edi
	movl	%edi, (%rsi)
	movq	8(%rax), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	8(%r12), %rdi
	call	free@PLT
	movq	8(%rbp), %rax
	movq	8(%rax,%rbx), %rdi
	call	free@PLT
	addq	8(%rbp), %rbx
	movdqu	16(%rbx), %xmm0
	movl	4(%rbx), %eax
	popq	%rbx
	movaps	%xmm0, (%r12)
	popq	%rbp
	popq	%r12
	ret
	.size	pop_fail_stack, .-pop_fail_stack
	.p2align 4,,15
	.type	re_node_set_compare.part.3, @function
re_node_set_compare.part.3:
	movl	4(%rdi), %edx
	xorl	%eax, %eax
	cmpl	4(%rsi), %edx
	je	.L496
	rep ret
	.p2align 4,,10
	.p2align 3
.L496:
	movslq	%edx, %rax
	leaq	-4(,%rax,4), %rax
	jmp	.L492
	.p2align 4,,10
	.p2align 3
.L493:
	movq	8(%rdi), %rcx
	movl	(%rcx,%rax), %r8d
	movq	8(%rsi), %rcx
	movl	(%rcx,%rax), %ecx
	subq	$4, %rax
	cmpl	%ecx, %r8d
	jne	.L495
.L492:
	subl	$1, %edx
	jns	.L493
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L495:
	xorl	%eax, %eax
	ret
	.size	re_node_set_compare.part.3, .-re_node_set_compare.part.3
	.p2align 4,,15
	.type	re_node_set_contains.isra.4, @function
re_node_set_contains.isra.4:
	xorl	%eax, %eax
	testl	%edi, %edi
	jle	.L497
	movq	(%rsi), %r8
	subl	$1, %edi
	xorl	%esi, %esi
.L499:
	cmpl	%edi, %esi
	jnb	.L502
	leal	(%rsi,%rdi), %ecx
	shrl	%ecx
	movl	%ecx, %eax
	cmpl	(%r8,%rax,4), %edx
	jle	.L503
	jmp	.L507
	.p2align 4,,10
	.p2align 3
.L501:
	leal	(%rsi,%rcx), %eax
	shrl	%eax
	movl	%eax, %edi
	cmpl	%edx, (%r8,%rdi,4)
	jl	.L500
	movl	%eax, %ecx
.L503:
	cmpl	%ecx, %esi
	jb	.L501
.L502:
	movl	%esi, %ecx
	xorl	%eax, %eax
	cmpl	(%r8,%rcx,4), %edx
	je	.L508
.L497:
	rep ret
	.p2align 4,,10
	.p2align 3
.L507:
	movl	%ecx, %eax
	movl	%edi, %ecx
	.p2align 4,,10
	.p2align 3
.L500:
	leal	1(%rax), %esi
	movl	%ecx, %edi
	jmp	.L499
	.p2align 4,,10
	.p2align 3
.L508:
	leal	1(%rsi), %eax
	ret
	.size	re_node_set_contains.isra.4, .-re_node_set_contains.isra.4
	.p2align 4,,15
	.type	check_arrival_expand_ecl_sub, @function
check_arrival_expand_ecl_sub:
	pushq	%r15
	pushq	%r14
	leaq	8(%rsi), %r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r9d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rsi, %rbp
	movl	%ecx, %r15d
	movl	%r8d, %r13d
	subq	$8, %rsp
.L510:
	movl	4(%rbp), %edi
	movl	%r9d, %edx
	movq	%r14, %rsi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L512
	movq	(%r12), %rax
	movslq	%r9d, %rbx
	salq	$4, %rbx
	addq	%rbx, %rax
	movzbl	8(%rax), %edx
	cmpl	%r13d, %edx
	jne	.L511
	cmpl	%r15d, (%rax)
	je	.L524
.L511:
	movl	%r9d, %esi
	movq	%rbp, %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L514
	movq	40(%r12), %rdx
	addq	%rbx, %rdx
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L512
	cmpl	$2, %eax
	movq	8(%rdx), %rdx
	je	.L525
.L515:
	movl	(%rdx), %r9d
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L524:
	cmpl	$9, %r13d
	jne	.L512
	movl	%r9d, %esi
	movq	%rbp, %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L514
	.p2align 4,,10
	.p2align 3
.L512:
	xorl	%eax, %eax
.L509:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L514:
	addq	$8, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L525:
	movl	4(%rdx), %edx
	movl	%r13d, %r8d
	movl	%r15d, %ecx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	check_arrival_expand_ecl_sub
	testl	%eax, %eax
	jne	.L509
	movq	40(%r12), %rax
	movq	8(%rax,%rbx), %rdx
	jmp	.L515
	.size	check_arrival_expand_ecl_sub, .-check_arrival_expand_ecl_sub
	.p2align 4,,15
	.type	re_node_set_remove_at.part.5, @function
re_node_set_remove_at.part.5:
	movl	4(%rdi), %eax
	cmpl	%esi, %eax
	jle	.L526
	subl	$1, %eax
	cmpl	%eax, %esi
	movl	%eax, 4(%rdi)
	jge	.L526
	movq	8(%rdi), %rdx
	movslq	%esi, %rax
	leaq	(%rdx,%rax,4), %rax
	.p2align 4,,10
	.p2align 3
.L528:
	movl	4(%rax), %edx
	addl	$1, %esi
	addq	$4, %rax
	movl	%edx, -4(%rax)
	cmpl	4(%rdi), %esi
	jl	.L528
.L526:
	rep ret
	.size	re_node_set_remove_at.part.5, .-re_node_set_remove_at.part.5
	.p2align 4,,15
	.type	optimize_subexps, @function
optimize_subexps:
	movzbl	48(%rsi), %eax
	cmpb	$4, %al
	je	.L542
	cmpb	$17, %al
	je	.L543
.L533:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L543:
	movq	8(%rsi), %rax
	testq	%rax, %rax
	je	.L533
	cmpb	$17, 48(%rax)
	jne	.L533
	movl	40(%rax), %ecx
	movq	8(%rax), %rax
	testq	%rax, %rax
	movq	%rax, 8(%rsi)
	je	.L535
	movq	%rsi, (%rax)
.L535:
	movslq	40(%rsi), %rdx
	movq	208(%rdi), %rax
	cmpl	$63, %ecx
	movl	(%rax,%rdx,4), %esi
	movslq	%ecx, %rdx
	movl	%esi, (%rax,%rdx,4)
	jg	.L533
	movq	$-2, %rax
	rolq	%cl, %rax
	andq	%rax, 144(%rdi)
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L542:
	movq	208(%rdi), %rax
	testq	%rax, %rax
	je	.L533
	movslq	40(%rsi), %rdx
	movl	(%rax,%rdx,4), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	movl	%ecx, 40(%rsi)
	cltq
	orq	%rax, 144(%rdi)
	xorl	%eax, %eax
	ret
	.size	optimize_subexps, .-optimize_subexps
	.p2align 4,,15
	.type	calc_next, @function
calc_next:
	movzbl	48(%rsi), %eax
	movq	8(%rsi), %rdx
	cmpb	$11, %al
	je	.L546
	cmpb	$16, %al
	je	.L547
	testq	%rdx, %rdx
	je	.L549
	movq	32(%rsi), %rax
	movq	%rax, 32(%rdx)
.L549:
	movq	16(%rsi), %rax
	testq	%rax, %rax
	je	.L548
	movq	32(%rsi), %rdx
	movq	%rdx, 32(%rax)
.L548:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L547:
	movq	16(%rsi), %rax
	movq	24(%rax), %rcx
	movq	%rcx, 32(%rdx)
	movq	32(%rsi), %rdx
	movq	%rdx, 32(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L546:
	movq	%rsi, 32(%rdx)
	xorl	%eax, %eax
	ret
	.size	calc_next, .-calc_next
	.p2align 4,,15
	.type	create_token_tree.isra.12, @function
create_token_tree.isra.12:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	(%rsi), %eax
	cmpl	$15, %eax
	je	.L558
	movslq	%eax, %r9
	leal	1(%rax), %r10d
	movq	(%rdi), %rdi
	movq	%r9, %rax
	salq	$6, %rax
	addq	$8, %rax
.L559:
	salq	$6, %r9
	movl	%r10d, (%rsi)
	addq	%rdi, %rax
	leaq	(%rdi,%r9), %rsi
	movq	$0, 8(%rsi)
	movq	%rdx, 16(%rsi)
	leaq	32(%rsi), %rdi
	movq	%rcx, 24(%rsi)
	movdqu	(%r8), %xmm0
	movups	%xmm0, 48(%rsi)
	andb	$-13, 26(%rdi)
	testq	%rdx, %rdx
	movq	$0, 32(%rsi)
	movq	$0, 40(%rsi)
	movl	$-1, 64(%rsi)
	je	.L561
	movq	%rax, (%rdx)
.L561:
	testq	%rcx, %rcx
	je	.L557
	movq	%rax, (%rcx)
.L557:
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L558:
	movl	$968, %edi
	movq	%r8, 24(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L562
	movq	(%rbx), %rax
	movl	$1, %r10d
	movq	%rdi, (%rbx)
	xorl	%r9d, %r9d
	movq	24(%rsp), %r8
	movq	16(%rsp), %rcx
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%rax, (%rdi)
	movl	$8, %eax
	jmp	.L559
.L562:
	xorl	%eax, %eax
	jmp	.L557
	.size	create_token_tree.isra.12, .-create_token_tree.isra.12
	.p2align 4,,15
	.type	duplicate_tree, @function
duplicate_tree:
	pushq	%r14
	pushq	%r13
	leaq	112(%rsi), %r13
	pushq	%r12
	pushq	%rbp
	leaq	128(%rsi), %r12
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	(%rdi), %rbp
	leaq	8(%rsp), %r14
	.p2align 4,,10
	.p2align 3
.L571:
	leaq	40(%rbx), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, (%r14)
	je	.L570
	movq	%rbp, (%rax)
	movq	(%r14), %rbp
	orb	$4, 50(%rbp)
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.L576
	leaq	8(%rbp), %r14
	movq	%rax, %rbx
	jmp	.L571
	.p2align 4,,10
	.p2align 3
.L576:
	xorl	%edx, %edx
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L577:
	movq	%rax, %rbx
.L573:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.L575
	cmpq	%rdx, %rax
	jne	.L582
.L575:
	movq	(%rbx), %rax
	movq	0(%rbp), %rbp
	movq	%rbx, %rdx
	testq	%rax, %rax
	jne	.L577
	movq	8(%rsp), %rax
.L570:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L582:
	leaq	16(%rbp), %r14
	movq	%rax, %rbx
	jmp	.L571
	.size	duplicate_tree, .-duplicate_tree
	.p2align 4,,15
	.type	lower_subexp, @function
lower_subexp:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	testb	$16, 56(%rsi)
	movq	(%rsi), %rbx
	movq	%rdi, 24(%rsp)
	movq	8(%rdx), %rbp
	je	.L584
	testq	%rbp, %rbp
	je	.L585
	movl	40(%rdx), %eax
	cmpl	$63, %eax
	jg	.L583
	movq	144(%rbx), %rdx
	btq	%rax, %rdx
	jnc	.L583
	leaq	112(%rbx), %r15
	pxor	%xmm0, %xmm0
	leaq	32(%rsp), %r14
	subq	$-128, %rbx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r14, %r8
	movq	%r15, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	movb	$8, 40(%rsp)
	movaps	%xmm0, (%rsp)
	call	create_token_tree.isra.12
	movq	%r15, %rdi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %r12
	movdqa	(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movb	$9, 40(%rsp)
	call	create_token_tree.isra.12
	movq	%rax, %r15
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L585:
	leaq	112(%rbx), %r15
	pxor	%xmm0, %xmm0
	leaq	32(%rsp), %r14
	subq	$-128, %rbx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r14, %r8
	movq	%r15, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	movb	$8, 40(%rsp)
	movaps	%xmm0, (%rsp)
	call	create_token_tree.isra.12
	movq	%r15, %rdi
	movq	%r14, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%rax, %r12
	movdqa	(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movb	$9, 40(%rsp)
	call	create_token_tree.isra.12
	movq	%rax, %r11
	movq	%rax, %r15
.L588:
	pxor	%xmm0, %xmm0
	movq	16(%rsp), %rdi
	movq	%r14, %r8
	movq	%r11, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r11, (%rsp)
	movaps	%xmm0, 32(%rsp)
	movb	$16, 40(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L589
	movq	(%rsp), %r11
	testq	%r11, %r11
	je	.L589
	testq	%r12, %r12
	je	.L589
	testq	%r15, %r15
	je	.L589
	movl	40(%r13), %eax
	movl	%eax, 40(%r15)
	movl	%eax, 40(%r12)
	movzbl	50(%r13), %eax
	movzbl	50(%r15), %edx
	andl	$8, %eax
	andl	$-9, %edx
	movl	%eax, %ecx
	orl	%ecx, %edx
	movb	%dl, 50(%r15)
	movzbl	50(%r12), %eax
	andl	$-9, %eax
	orl	%ecx, %eax
	movb	%al, 50(%r12)
.L583:
	addq	$56, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L584:
	leaq	112(%rbx), %r15
	pxor	%xmm0, %xmm0
	leaq	32(%rsp), %r14
	subq	$-128, %rbx
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %r8
	movq	%r15, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	movb	$8, 40(%rsp)
	movaps	%xmm0, (%rsp)
	call	create_token_tree.isra.12
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r14, %r8
	movq	%rbx, %rsi
	movq	%rax, %r12
	movdqa	(%rsp), %xmm0
	movaps	%xmm0, 32(%rsp)
	movb	$9, 40(%rsp)
	call	create_token_tree.isra.12
	testq	%rbp, %rbp
	movq	%rax, %r15
	movq	%rax, %r11
	je	.L588
.L592:
	pxor	%xmm0, %xmm0
	movq	16(%rsp), %rdi
	movq	%r14, %r8
	movq	%r15, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movaps	%xmm0, 32(%rsp)
	movb	$16, 40(%rsp)
	call	create_token_tree.isra.12
	movq	%rax, %r11
	jmp	.L588
	.p2align 4,,10
	.p2align 3
.L589:
	movq	24(%rsp), %rax
	xorl	%ebp, %ebp
	movl	$12, (%rax)
	jmp	.L583
	.size	lower_subexp, .-lower_subexp
	.p2align 4,,15
	.type	lower_subexps, @function
lower_subexps:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$24, %rsp
	movq	8(%rsi), %rdx
	movl	$0, 12(%rsp)
	testq	%rdx, %rdx
	je	.L602
	cmpb	$17, 48(%rdx)
	je	.L614
.L602:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L605
	cmpb	$17, 48(%rdx)
	je	.L615
.L605:
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L615:
	leaq	12(%rsp), %rdi
	movq	%rbp, %rsi
	call	lower_subexp
	testq	%rax, %rax
	movq	%rax, 16(%rbx)
	je	.L605
	movq	%rbx, (%rax)
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	leaq	12(%rsp), %rdi
	movq	%rbp, %rsi
	call	lower_subexp
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	je	.L602
	movq	%rbx, (%rax)
	jmp	.L602
	.size	lower_subexps, .-lower_subexps
	.p2align 4,,15
	.type	re_node_set_merge, @function
re_node_set_merge:
	testq	%rsi, %rsi
	je	.L646
	movl	4(%rsi), %eax
	testl	%eax, %eax
	je	.L646
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	4(%rdi), %edx
	movq	%rsi, %rbp
	movl	(%rdi), %ebx
	leal	(%rdx,%rax,2), %ecx
	cmpl	%ecx, %ebx
	jl	.L651
.L620:
	testl	%edx, %edx
	je	.L652
	leal	(%rdx,%rax,2), %ebx
	subl	$1, %eax
	subl	$1, %edx
	movl	%eax, %r8d
	notl	%r8d
.L622:
	testl	%r8d, %r8d
	jns	.L633
.L654:
	testl	%edx, %edx
	js	.L633
	movq	8(%r12), %rdi
	movq	8(%rbp), %r9
	movslq	%edx, %rcx
	movslq	%eax, %rsi
	movl	(%r9,%rsi,4), %esi
	cmpl	%esi, (%rdi,%rcx,4)
	je	.L653
	jge	.L625
	subl	$1, %eax
	subl	$1, %ebx
	movl	%eax, %r8d
	movslq	%ebx, %rcx
	notl	%r8d
	movl	%esi, (%rdi,%rcx,4)
	testl	%r8d, %r8d
	js	.L654
	.p2align 4,,10
	.p2align 3
.L633:
	testl	%eax, %eax
	jns	.L655
.L628:
	movl	4(%rbp), %edx
	movl	4(%r12), %eax
	leal	-1(%rax,%rdx,2), %r9d
	movl	%r9d, %edx
	subl	%ebx, %edx
	addl	$1, %edx
	je	.L650
	movq	8(%r12), %rdi
	leal	-1(%rax), %ecx
	addl	%edx, %eax
	movl	%eax, 4(%r12)
	movslq	%r9d, %r11
	.p2align 4,,10
	.p2align 3
.L649:
	movslq	%ecx, %r10
.L629:
	movl	(%rdi,%r11,4), %r8d
	movl	(%rdi,%r10,4), %esi
	leal	(%rcx,%rdx), %eax
	cltq
	cmpl	%esi, %r8d
	leaq	(%rdi,%rax,4), %rax
	jle	.L630
	subl	$1, %r9d
	subl	$1, %edx
	movl	%r8d, (%rax)
	je	.L650
	movslq	%r9d, %r11
	jmp	.L629
	.p2align 4,,10
	.p2align 3
.L646:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L652:
	movl	%eax, 4(%r12)
	movslq	4(%rbp), %rdx
	movq	8(%r12), %rdi
	movq	8(%rbp), %rsi
	salq	$2, %rdx
	call	memcpy@PLT
	.p2align 4,,10
	.p2align 3
.L650:
	xorl	%eax, %eax
.L616:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L653:
	subl	$1, %eax
	subl	$1, %edx
	movl	%eax, %r8d
	notl	%r8d
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L625:
	subl	$1, %edx
	jmp	.L622
	.p2align 4,,10
	.p2align 3
.L655:
	addl	$1, %eax
	movq	8(%r12), %rdx
	movq	8(%rbp), %rsi
	subl	%eax, %ebx
	cltq
	movslq	%ebx, %rcx
	leaq	(%rdx,%rcx,4), %rdi
	leaq	0(,%rax,4), %rdx
	call	memcpy@PLT
	jmp	.L628
	.p2align 4,,10
	.p2align 3
.L630:
	subl	$1, %ecx
	movl	%esi, (%rax)
	jns	.L649
	movslq	%ebx, %rbx
	movslq	%edx, %rdx
	leaq	(%rdi,%rbx,4), %rsi
	salq	$2, %rdx
	call	memcpy@PLT
	xorl	%eax, %eax
	jmp	.L616
	.p2align 4,,10
	.p2align 3
.L651:
	addl	%ebx, %eax
	movq	8(%rdi), %rdi
	leal	(%rax,%rax), %ebx
	movslq	%ebx, %rsi
	salq	$2, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L632
	movq	%rax, 8(%r12)
	movl	%ebx, (%r12)
	movl	4(%r12), %edx
	movl	4(%rbp), %eax
	jmp	.L620
.L632:
	movl	$12, %eax
	jmp	.L616
	.size	re_node_set_merge, .-re_node_set_merge
	.p2align 4,,15
	.type	calc_eclosure_iter, @function
calc_eclosure_iter:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movslq	%edx, %rbx
	subq	$72, %rsp
	movq	40(%rsi), %rbp
	movl	%ebx, 24(%rsp)
	salq	$4, %rbx
	movq	%rdi, 16(%rsp)
	movl	%ecx, 28(%rsp)
	addq	%rbx, %rbp
	movl	4(%rbp), %eax
	movl	$0, 36(%rsp)
	leal	1(%rax), %edi
	movl	%edi, 32(%rsp)
	movslq	%edi, %rdi
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 40(%rsp)
	je	.L668
	movq	48(%r15), %rax
	movq	(%r15), %rdx
	movl	$-1, 4(%rax,%rbx)
	leaq	(%rdx,%rbx), %rax
	movl	8(%rax), %r8d
	testl	$261888, %r8d
	jne	.L689
.L659:
	testb	$8, 8(%rax)
	je	.L660
	movq	40(%r15), %rsi
	leaq	(%rsi,%rbx), %rax
	movl	4(%rax), %edx
	testl	%edx, %edx
	jle	.L660
	xorl	%r14d, %r14d
	movb	$0, 15(%rsp)
	leaq	32(%rsp), %r12
	leaq	48(%rsp), %rbp
	jmp	.L667
	.p2align 4,,10
	.p2align 3
.L688:
	movq	40(%r15), %rsi
.L662:
	leaq	(%rsi,%rbx), %rax
	leal	1(%r14), %edx
	addq	$1, %r14
	cmpl	%edx, 4(%rax)
	jle	.L690
.L667:
	movq	8(%rax), %rax
	movq	48(%r15), %rcx
	movslq	(%rax,%r14,4), %r8
	movq	%r8, %rdx
	salq	$4, %r8
	addq	%r8, %rcx
	movq	%r8, %r13
	movl	4(%rcx), %eax
	cmpl	$-1, %eax
	je	.L691
	testl	%eax, %eax
	je	.L692
	movdqu	(%rcx), %xmm0
	movaps	%xmm0, 48(%rsp)
.L665:
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	re_node_set_merge
	testl	%eax, %eax
	jne	.L656
	movq	48(%r15), %rax
	movl	4(%rax,%r13), %eax
	testl	%eax, %eax
	jne	.L688
	movq	56(%rsp), %rdi
	call	free@PLT
	movb	$1, 15(%rsp)
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L660:
	movl	24(%rsp), %esi
	leaq	32(%rsp), %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L668
	movq	48(%r15), %rax
.L669:
	movdqa	32(%rsp), %xmm0
	movups	%xmm0, (%rax,%rbx)
.L670:
	movq	16(%rsp), %rax
	movdqa	32(%rsp), %xmm0
	movaps	%xmm0, (%rax)
	xorl	%eax, %eax
.L656:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L691:
	movb	$1, 15(%rsp)
	jmp	.L662
	.p2align 4,,10
	.p2align 3
.L689:
	movl	4(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L660
	movq	8(%rbp), %rcx
	movslq	(%rcx), %rcx
	salq	$4, %rcx
	testb	$4, 10(%rdx,%rcx)
	jne	.L659
	movl	24(%rsp), %esi
	shrl	$8, %r8d
	movq	%r15, %rdi
	andl	$1023, %r8d
	movl	%esi, %ecx
	movl	%esi, %edx
	call	duplicate_node_closure
	testl	%eax, %eax
	jne	.L656
	movq	(%r15), %rax
	addq	%rbx, %rax
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L692:
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	movq	%rbp, %rdi
	call	calc_eclosure_iter
	testl	%eax, %eax
	je	.L665
	jmp	.L656
	.p2align 4,,10
	.p2align 3
.L690:
	movl	24(%rsp), %esi
	leaq	32(%rsp), %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L668
	cmpb	$1, 28(%rsp)
	movq	48(%r15), %rax
	leaq	(%rax,%rbx), %rdx
	je	.L669
	cmpb	$0, 15(%rsp)
	je	.L669
	movl	$0, 4(%rdx)
	jmp	.L670
	.p2align 4,,10
	.p2align 3
.L668:
	addq	$72, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	calc_eclosure_iter, .-calc_eclosure_iter
	.p2align 4,,15
	.type	check_arrival_expand_ecl, @function
check_arrival_expand_ecl:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r15
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %ebp
	movl	%ecx, %ebx
	subq	$40, %rsp
	movslq	4(%rsi), %rdi
	movl	$0, 20(%rsp)
	movl	%edi, 16(%rsp)
	movq	%rdi, %r12
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 24(%rsp)
	je	.L703
	testl	%r12d, %r12d
	jle	.L695
	leaq	16(%rsp), %r12
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L702:
	movq	8(%r15), %rax
	movslq	(%rax,%r13,4), %rsi
	movq	%rsi, %rdx
	salq	$4, %rsi
	addq	48(%r14), %rsi
	movl	4(%rsi), %eax
	testl	%eax, %eax
	jle	.L696
	movq	8(%rsi), %rcx
	subl	$1, %eax
	movq	(%r14), %r8
	leaq	4(%rcx,%rax,4), %r9
	jmp	.L699
	.p2align 4,,10
	.p2align 3
.L697:
	addq	$4, %rcx
	cmpq	%rcx, %r9
	je	.L696
.L699:
	movslq	(%rcx), %rax
	movq	%rax, %rdi
	salq	$4, %rax
	addq	%r8, %rax
	movzbl	8(%rax), %r10d
	cmpl	%r10d, %ebx
	jne	.L697
	cmpl	(%rax), %ebp
	jne	.L697
	cmpl	$-1, %edi
	je	.L696
	movl	%ebx, %r8d
	movl	%ebp, %ecx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	check_arrival_expand_ecl_sub
	testl	%eax, %eax
	jne	.L713
.L701:
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, 4(%r15)
	jg	.L702
.L695:
	movq	8(%r15), %rdi
	call	free@PLT
	movdqa	16(%rsp), %xmm0
	xorl	%eax, %eax
	movaps	%xmm0, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L696:
	movq	%r12, %rdi
	call	re_node_set_merge
	testl	%eax, %eax
	je	.L701
.L713:
	movq	24(%rsp), %rdi
	movl	%eax, 12(%rsp)
	call	free@PLT
	movl	12(%rsp), %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L703:
	addq	$40, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	check_arrival_expand_ecl, .-check_arrival_expand_ecl
	.p2align 4,,15
	.type	re_node_set_init_copy, @function
re_node_set_init_copy:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movslq	4(%rsi), %rax
	movl	%eax, 4(%rdi)
	movl	4(%rsi), %r12d
	testl	%r12d, %r12d
	jle	.L715
	movq	%rdi, %rbx
	movl	%eax, (%rdi)
	leaq	0(,%rax,4), %rdi
	movq	%rsi, %rbp
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%rbx)
	je	.L719
	movq	8(%rbp), %rsi
	movslq	%r12d, %rdx
	movq	%rax, %rdi
	salq	$2, %rdx
	call	memcpy@PLT
	xorl	%eax, %eax
.L714:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L715:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L719:
	movq	$0, (%rbx)
	movl	$12, %eax
	jmp	.L714
	.size	re_node_set_init_copy, .-re_node_set_init_copy
	.p2align 4,,15
	.type	re_node_set_init_union, @function
re_node_set_init_union:
	testq	%rsi, %rsi
	je	.L752
	testq	%rdx, %rdx
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	4(%rsi), %r12d
	je	.L722
	testl	%r12d, %r12d
	jle	.L722
	movl	4(%rdx), %r14d
	testl	%r14d, %r14d
	jg	.L759
.L758:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	jmp	re_node_set_init_copy
	.p2align 4,,10
	.p2align 3
.L722:
	testl	%r12d, %r12d
	jg	.L758
	testq	%rdx, %rdx
	je	.L726
	movl	4(%rdx), %ecx
	testl	%ecx, %ecx
	jg	.L760
.L726:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movaps	%xmm0, (%rdi)
.L720:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L760:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	movq	%rdx, %rsi
	jmp	re_node_set_init_copy
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%rdi, %r13
	leal	(%r12,%r14), %edi
	movq	%rdx, %rbx
	movq	%rsi, %rbp
	movl	%edi, 0(%r13)
	movslq	%edi, %rdi
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r13)
	je	.L761
	movq	8(%rbx), %r10
	movq	8(%rbp), %r9
	movq	%rax, %rdx
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	jmp	.L731
	.p2align 4,,10
	.p2align 3
.L727:
	sete	%dil
	movl	%r8d, (%rdx)
	addl	$1, %esi
	movzbl	%dil, %edi
	addl	%edi, %ecx
.L728:
	cmpl	%esi, %r12d
	jle	.L730
	addq	$4, %rdx
	cmpl	%ecx, %r14d
	jle	.L762
.L731:
	movslq	%esi, %rdi
	addl	$1, %ebx
	movl	(%r9,%rdi,4), %r8d
	movslq	%ecx, %rdi
	movl	(%r10,%rdi,4), %edi
	cmpl	%edi, %r8d
	jle	.L727
	addl	$1, %ecx
	movl	%edi, (%rdx)
	jmp	.L728
	.p2align 4,,10
	.p2align 3
.L752:
	testq	%rdx, %rdx
	je	.L754
	movl	4(%rdx), %eax
	testl	%eax, %eax
	jg	.L763
.L754:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movaps	%xmm0, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L730:
	cmpl	%ecx, %r14d
	jg	.L764
.L733:
	movl	%ebx, 4(%r13)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L763:
	movq	%rdx, %rsi
	jmp	re_node_set_init_copy
	.p2align 4,,10
	.p2align 3
.L762:
	subl	%esi, %r12d
	movslq	%ebx, %rdx
	movslq	%esi, %rsi
	leaq	(%rax,%rdx,4), %rdi
	leaq	(%r9,%rsi,4), %rsi
	movslq	%r12d, %rdx
	salq	$2, %rdx
	addl	%r12d, %ebx
	call	memcpy@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L764:
	subl	%ecx, %r14d
	movslq	%ebx, %rdx
	movslq	%ecx, %rcx
	leaq	(%rax,%rdx,4), %rdi
	leaq	(%r10,%rcx,4), %rsi
	movslq	%r14d, %rdx
	salq	$2, %rdx
	addl	%r14d, %ebx
	call	memcpy@PLT
	jmp	.L733
	.p2align 4,,10
	.p2align 3
.L761:
	movl	$12, %eax
	jmp	.L720
	.size	re_node_set_init_union, .-re_node_set_init_union
	.p2align 4,,15
	.type	re_acquire_state_context, @function
re_acquire_state_context:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	4(%rdx), %eax
	testl	%eax, %eax
	je	.L829
	testl	%eax, %eax
	leal	(%rcx,%rax), %r15d
	jle	.L768
	movq	8(%rdx), %r8
	leal	-1(%rax), %r9d
	leaq	4(%r8), %rax
	leaq	(%rax,%r9,4), %r9
	jmp	.L769
	.p2align 4,,10
	.p2align 3
.L830:
	addq	$4, %rax
.L769:
	addl	(%r8), %r15d
	cmpq	%rax, %r9
	movq	%rax, %r8
	jne	.L830
.L768:
	movl	%r15d, %eax
	andl	132(%rsi), %eax
	movq	%rdx, %r14
	movl	%ecx, 4(%rsp)
	movq	%rsi, %r12
	movq	%rdi, 24(%rsp)
	salq	$4, %rax
	addq	64(%rsi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L770
	movq	8(%rax), %r9
	leal	-1(%rdx), %eax
	movl	%ecx, %r11d
	leaq	8(%r9,%rax,8), %r10
	jmp	.L772
	.p2align 4,,10
	.p2align 3
.L771:
	addq	$8, %r9
	cmpq	%r9, %r10
	je	.L770
.L772:
	movq	(%r9), %rbp
	cmpl	%r15d, 0(%rbp)
	jne	.L771
	movzbl	80(%rbp), %eax
	andl	$15, %eax
	cmpl	%r11d, %eax
	jne	.L771
	movq	56(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L771
	movq	%r14, %rsi
	call	re_node_set_compare.part.3
	testb	%al, %al
	je	.L771
.L765:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L770:
	movl	$1, %esi
	movl	$88, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L773
	leaq	8(%rax), %r9
	movq	%r14, %rsi
	movq	%r9, %rdi
	movq	%r9, 8(%rsp)
	call	re_node_set_init_copy
	testl	%eax, %eax
	movq	8(%rsp), %r9
	jne	.L831
	movzbl	80(%rbp), %eax
	movl	4(%rsp), %esi
	movq	%r9, 56(%rbp)
	movl	%esi, %edx
	andl	$15, %edx
	andl	$-16, %eax
	orl	%edx, %eax
	movl	4(%r14), %edx
	movb	%al, 80(%rbp)
	testl	%edx, %edx
	jle	.L775
	movq	%r14, %rax
	andl	$4, %esi
	movl	%r15d, 20(%rsp)
	movq	%r12, %r14
	xorl	%r13d, %r13d
	xorl	%r10d, %r10d
	movl	%esi, 8(%rsp)
	movq	%r9, %r15
	movq	%rax, %r12
	.p2align 4,,10
	.p2align 3
.L789:
	movq	8(%r12), %rax
	movl	%r13d, %r8d
	movslq	(%rax,%r13,4), %rax
	salq	$4, %rax
	addq	(%r14), %rax
	movzbl	8(%rax), %ecx
	movl	8(%rax), %ebx
	shrl	$8, %ebx
	andw	$1023, %bx
	cmpl	$1, %ecx
	movzwl	%bx, %esi
	jne	.L790
	testl	%esi, %esi
	je	.L776
.L790:
	movzbl	80(%rbp), %edx
	movzbl	10(%rax), %eax
	movl	%edx, %edi
	shrb	$4, %al
	andl	$-33, %edx
	shrb	$5, %dil
	orl	%eax, %edi
	andl	$1, %edi
	sall	$5, %edi
	orl	%edi, %edx
	cmpl	$2, %ecx
	movb	%dl, 80(%rbp)
	je	.L832
	cmpl	$4, %ecx
	jne	.L779
	orl	$64, %edx
	movb	%dl, 80(%rbp)
.L779:
	testl	%esi, %esi
	je	.L776
	cmpq	56(%rbp), %r15
	je	.L833
.L780:
	testb	$1, %bl
	je	.L783
	testb	$1, 4(%rsp)
	je	.L784
	testb	$2, %bl
	je	.L786
	.p2align 4,,10
	.p2align 3
.L784:
	subl	%r10d, %r8d
	js	.L788
	movl	%r8d, %esi
	movq	%r15, %rdi
	call	re_node_set_remove_at.part.5
.L788:
	addl	$1, %r10d
.L776:
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, 4(%r12)
	jg	.L789
	movl	20(%rsp), %r15d
	movq	%r14, %r12
.L775:
	movl	%r15d, %edx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	register_state
	testl	%eax, %eax
	je	.L765
.L828:
	movq	%rbp, %rdi
	call	free_state
.L773:
	movq	24(%rsp), %rax
	xorl	%ebp, %ebp
	movl	$12, (%rax)
	jmp	.L765
	.p2align 4,,10
	.p2align 3
.L783:
	testb	$2, %bl
	je	.L786
	testb	$1, 4(%rsp)
	jne	.L784
.L786:
	testb	$16, %bl
	je	.L787
	testb	$2, 4(%rsp)
	je	.L784
.L787:
	andl	$64, %ebx
	je	.L776
	movl	8(%rsp), %eax
	testl	%eax, %eax
	jne	.L776
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L832:
	orl	$16, %edx
	movb	%dl, 80(%rbp)
	jmp	.L779
	.p2align 4,,10
	.p2align 3
.L833:
	movl	$16, %edi
	movl	%r8d, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movl	16(%rsp), %r8d
	je	.L828
	movq	%rax, 56(%rbp)
	movq	%r12, %rsi
	movq	%rax, %rdi
	movl	%r8d, 16(%rsp)
	call	re_node_set_init_copy
	testl	%eax, %eax
	movl	16(%rsp), %r8d
	jne	.L828
	orb	$-128, 80(%rbp)
	xorl	%r10d, %r10d
	jmp	.L780
.L829:
	movl	$0, (%rdi)
	xorl	%ebp, %ebp
	jmp	.L765
.L831:
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L773
	.size	re_acquire_state_context, .-re_acquire_state_context
	.p2align 4,,15
	.type	re_acquire_state, @function
re_acquire_state:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	4(%rdx), %ebp
	testl	%ebp, %ebp
	je	.L864
	testl	%ebp, %ebp
	jle	.L837
	movq	8(%rdx), %rcx
	leal	-1(%rbp), %r8d
	leaq	4(%rcx), %rax
	leaq	(%rax,%r8,4), %r8
	jmp	.L838
	.p2align 4,,10
	.p2align 3
.L865:
	addq	$4, %rax
.L838:
	addl	(%rcx), %ebp
	cmpq	%rax, %r8
	movq	%rax, %rcx
	jne	.L865
.L837:
	movl	%ebp, %eax
	andl	132(%rsi), %eax
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r13
	salq	$4, %rax
	addq	64(%rsi), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	jle	.L839
	movq	8(%rax), %r9
	leal	-1(%rdx), %eax
	leaq	8(%r9,%rax,8), %r10
	.p2align 4,,10
	.p2align 3
.L841:
	movq	(%r9), %rbx
	cmpl	%ebp, (%rbx)
	jne	.L840
	leaq	8(%rbx), %rdi
	movq	%r14, %rsi
	call	re_node_set_compare.part.3
	testb	%al, %al
	jne	.L834
.L840:
	addq	$8, %r9
	cmpq	%r9, %r10
	jne	.L841
.L839:
	movl	$1, %esi
	movl	$88, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L842
	leaq	8(%rax), %r15
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	re_node_set_init_copy
	testl	%eax, %eax
	jne	.L866
	movl	4(%r14), %edx
	movq	%r15, 56(%rbx)
	testl	%edx, %edx
	jle	.L844
	movq	8(%r14), %rax
	subl	$1, %edx
	movq	(%r12), %r8
	leaq	4(%rax), %rsi
	leaq	(%rsi,%rdx,4), %r9
	jmp	.L853
	.p2align 4,,10
	.p2align 3
.L845:
	movzbl	80(%rbx), %ecx
	movzbl	10(%rax), %edx
	movl	%ecx, %r10d
	shrb	$4, %dl
	andl	$-33, %ecx
	shrb	$5, %r10b
	orl	%r10d, %edx
	andl	$1, %edx
	sall	$5, %edx
	orl	%ecx, %edx
	cmpl	$2, %edi
	movb	%dl, 80(%rbx)
	je	.L867
	cmpl	$4, %edi
	jne	.L849
	orl	$64, %edx
	movb	%dl, 80(%rbx)
.L847:
	cmpq	%rsi, %r9
	movq	%rsi, %rax
	je	.L844
.L868:
	addq	$4, %rsi
.L853:
	movslq	(%rax), %rax
	salq	$4, %rax
	addq	%r8, %rax
	movzbl	8(%rax), %edi
	cmpl	$1, %edi
	jne	.L845
	testl	$261888, 8(%rax)
	je	.L847
	movzbl	80(%rbx), %edx
	movzbl	10(%rax), %eax
	movl	%edx, %ecx
	shrb	$4, %al
	andl	$-33, %edx
	shrb	$5, %cl
	orl	%ecx, %eax
	andl	$1, %eax
	sall	$5, %eax
	orl	%edx, %eax
	movb	%al, 80(%rbx)
.L851:
	orb	$-128, 80(%rbx)
	cmpq	%rsi, %r9
	movq	%rsi, %rax
	jne	.L868
.L844:
	movl	%ebp, %edx
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	register_state
	testl	%eax, %eax
	jne	.L869
.L834:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L867:
	orl	$16, %edx
	movb	%dl, 80(%rbx)
	jmp	.L847
	.p2align 4,,10
	.p2align 3
.L849:
	cmpl	$12, %edi
	je	.L851
	testl	$261888, 8(%rax)
	je	.L847
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L866:
	movq	%rbx, %rdi
	call	free@PLT
.L842:
	movl	$12, 0(%r13)
	xorl	%ebx, %ebx
	jmp	.L834
.L869:
	movq	%rbx, %rdi
	call	free_state
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L864:
	movl	$0, (%rdi)
	xorl	%ebx, %ebx
	jmp	.L834
	.size	re_acquire_state, .-re_acquire_state
	.p2align 4,,15
	.type	merge_state_array, @function
merge_state_array:
	testl	%ecx, %ecx
	jle	.L882
	pushq	%r15
	pushq	%r14
	leal	-1(%rcx), %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rsi, %rbx
	leaq	8(%rsi,%rax,8), %r12
	subq	$40, %rsp
	leaq	16(%rsp), %r13
	leaq	12(%rsp), %r15
	jmp	.L876
	.p2align 4,,10
	.p2align 3
.L872:
	testq	%rdx, %rdx
	je	.L873
	addq	$8, %rdx
	addq	$8, %rsi
	movq	%r13, %rdi
	call	re_node_set_init_union
	testl	%eax, %eax
	movl	%eax, 12(%rsp)
	jne	.L870
	movq	%r13, %rdx
	movq	%r14, %rsi
	movq	%r15, %rdi
	call	re_acquire_state
	movq	24(%rsp), %rdi
	movq	%rax, (%rbx)
	call	free@PLT
	movl	12(%rsp), %eax
	testl	%eax, %eax
	jne	.L870
.L873:
	addq	$8, %rbx
	addq	$8, %rbp
	cmpq	%r12, %rbx
	je	.L886
.L876:
	movq	(%rbx), %rsi
	movq	0(%rbp), %rdx
	testq	%rsi, %rsi
	jne	.L872
	movq	%rdx, (%rbx)
	addq	$8, %rbx
	addq	$8, %rbp
	cmpq	%r12, %rbx
	jne	.L876
.L886:
	xorl	%eax, %eax
.L870:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L882:
	xorl	%eax, %eax
	ret
	.size	merge_state_array, .-merge_state_array
	.p2align 4,,15
	.type	expand_bkref_cache, @function
expand_bkref_cache:
	pushq	%r15
	pushq	%r14
	xorl	%r11d, %r11d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movl	148(%rdi), %ebx
	movl	%ebx, %r9d
.L888:
	cmpl	%r9d, %r11d
	jge	.L891
	leal	(%r11,%r9), %eax
	movq	160(%rdi), %rbp
	movl	%eax, %r10d
	shrl	$31, %r10d
	addl	%eax, %r10d
	sarl	%r10d
	movslq	%r10d, %rax
	salq	$5, %rax
	cmpl	4(%rbp,%rax), %edx
	jle	.L892
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L890:
	leal	(%r11,%r10), %r9d
	movl	%r9d, %eax
	shrl	$31, %eax
	addl	%r9d, %eax
	sarl	%eax
	movslq	%eax, %r9
	movl	%eax, %r12d
	salq	$5, %r9
	cmpl	4(%rbp,%r9), %edx
	jg	.L889
	movl	%eax, %r10d
.L892:
	cmpl	%r10d, %r11d
	jl	.L890
.L891:
	cmpl	%r11d, %ebx
	jle	.L893
	movq	160(%rdi), %rax
	movslq	%r11d, %r9
	salq	$5, %r9
	movq	%r9, 40(%rsp)
	cmpl	%edx, 4(%rax,%r9)
	jne	.L893
	cmpl	$-1, %r11d
	jne	.L923
.L893:
	xorl	%eax, %eax
.L887:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L922:
	movl	%r10d, %r12d
	movl	%r9d, %r10d
	.p2align 4,,10
	.p2align 3
.L889:
	leal	1(%r12), %r11d
	movl	%r10d, %r9d
	jmp	.L888
	.p2align 4,,10
	.p2align 3
.L923:
	movl	%ecx, 56(%rsp)
	leaq	8(%rsi), %rcx
	movq	112(%rdi), %r13
	movl	%r8d, 60(%rsp)
	movl	%edx, %ebx
	movq	%rsi, %r15
	movq	%rcx, 8(%rsp)
	leaq	76(%rsp), %rcx
	movq	%rdi, %r12
	movq	%rcx, 48(%rsp)
	leaq	80(%rsp), %rcx
	movq	%rcx, 24(%rsp)
.L894:
	movq	40(%rsp), %rcx
	leaq	(%rax,%rcx), %r14
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L896:
	movq	24(%r13), %rax
	movslq	%ebp, %rbp
	movq	136(%r12), %r8
	movl	(%rax,%r9,4), %ecx
	leaq	0(,%rbp,8), %rax
	addq	%rax, %r8
	movq	%rax, 32(%rsp)
	movq	(%r8), %r9
	movl	%ecx, 16(%rsp)
	testq	%r9, %r9
	je	.L903
	movl	12(%r9), %edi
	leaq	16(%r9), %rsi
	movl	%ecx, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L895
	movq	24(%rsp), %rdi
	leaq	8(%r9), %rsi
	call	re_node_set_init_copy
	movl	16(%rsp), %esi
	movq	24(%rsp), %rdi
	movl	%eax, 76(%rsp)
	call	re_node_set_insert
	movl	76(%rsp), %edx
	testl	%edx, %edx
	jne	.L905
	cmpb	$1, %al
	jne	.L905
	movq	32(%rsp), %r8
	addq	136(%r12), %r8
.L906:
	movq	24(%rsp), %rdx
	movq	48(%rsp), %rdi
	movq	%r13, %rsi
	movq	%r8, 16(%rsp)
	call	re_acquire_state
	movq	16(%rsp), %r8
	movq	88(%rsp), %rdi
	movq	%rax, (%r8)
	call	free@PLT
	movq	136(%r12), %rax
	cmpq	$0, (%rax,%rbp,8)
	je	.L924
.L895:
	addq	$32, %r14
	cmpb	$0, -8(%r14)
	je	.L893
.L910:
	movslq	(%r14), %r9
	movl	4(%r15), %r10d
	movq	8(%rsp), %rsi
	movl	%r10d, %edi
	movl	%r9d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L895
	movl	12(%r14), %ebp
	addl	%ebx, %ebp
	subl	8(%r14), %ebp
	cmpl	%ebp, %ebx
	jne	.L896
	salq	$4, %r9
	addq	40(%r13), %r9
	movq	8(%rsp), %rsi
	movl	%r10d, %edi
	movq	8(%r9), %rax
	movl	(%rax), %ebp
	movl	%ebp, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L895
	movabsq	$4294967297, %rax
	movl	$4, %edi
	movq	%rax, 80(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 88(%rsp)
	je	.L925
	movl	%ebp, (%rax)
	xorl	%eax, %eax
.L899:
	movq	24(%rsp), %r14
	movl	56(%rsp), %edx
	movq	%r13, %rdi
	movl	60(%rsp), %ecx
	movl	%eax, 76(%rsp)
	movq	%r14, %rsi
	call	check_arrival_expand_ecl
	movq	%r14, %rsi
	movq	%r15, %rdi
	movl	%eax, %ebp
	call	re_node_set_merge
	movq	88(%rsp), %rdi
	movl	%eax, %r14d
	call	free@PLT
	movl	76(%rsp), %eax
	movl	%ebp, %edx
	orl	%r14d, %edx
	orl	%eax, %edx
	jne	.L926
	movq	160(%r12), %rax
	jmp	.L894
	.p2align 4,,10
	.p2align 3
.L903:
	movabsq	$4294967297, %rax
	movl	$4, %edi
	movq	%r8, 32(%rsp)
	movq	%rax, 80(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 88(%rsp)
	je	.L908
	movl	16(%rsp), %ecx
	movl	$0, 76(%rsp)
	movq	32(%rsp), %r8
	movl	%ecx, (%rax)
	jmp	.L906
.L924:
	movl	76(%rsp), %eax
	testl	%eax, %eax
	je	.L895
	jmp	.L887
.L905:
	movq	88(%rsp), %rdi
	call	free@PLT
	movl	76(%rsp), %eax
	testl	%eax, %eax
	jne	.L887
.L908:
	movl	$12, %eax
	jmp	.L887
.L926:
	testl	%eax, %eax
	jne	.L887
	testl	%ebp, %ebp
	movl	%ebp, %eax
	cmove	%r14d, %eax
	jmp	.L887
.L925:
	movq	$0, 80(%rsp)
	movl	$12, %eax
	jmp	.L899
	.size	expand_bkref_cache, .-expand_bkref_cache
	.p2align 4,,15
	.type	build_trtable, @function
build_trtable:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbx
	movl	$12288, %edi
	subq	$216, %rsp
	movq	%rsi, -168(%rbp)
	call	__libc_alloca_cutoff
	testl	%eax, %eax
	je	.L928
	subq	$12304, %rsp
	movb	$0, -217(%rbp)
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -176(%rbp)
.L929:
	movq	-176(%rbp), %rax
	pxor	%xmm0, %xmm0
	leaq	4096(%rax), %rdi
	movq	-168(%rbp), %rax
	movaps	%xmm0, -144(%rbp)
	movq	%rdi, -232(%rbp)
	movaps	%xmm0, -128(%rbp)
	movl	12(%rax), %r9d
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	testl	%r9d, %r9d
	jle	.L931
	movq	%r15, -208(%rbp)
	movq	$0, -184(%rbp)
	movq	%rdi, %r15
	movl	$0, -152(%rbp)
.L970:
	movq	-184(%rbp), %rdi
	leaq	0(,%rdi,4), %rax
	movq	%rax, -192(%rbp)
	movq	-168(%rbp), %rax
	movq	16(%rax), %rax
	movslq	(%rax,%rdi,4), %r14
	movq	-208(%rbp), %rax
	salq	$4, %r14
	addq	(%rax), %r14
	movzbl	8(%r14), %ebx
	movl	8(%r14), %eax
	shrl	$8, %eax
	andw	$1023, %ax
	cmpl	$1, %ebx
	movl	%ebx, -160(%rbp)
	je	.L1149
	cmpl	$3, -160(%rbp)
	je	.L1150
	cmpl	$5, -160(%rbp)
	je	.L1151
	cmpl	$7, -160(%rbp)
	je	.L1152
.L942:
	movq	-184(%rbp), %rdi
	movl	%edi, %eax
	addq	$1, %rdi
	movq	%rdi, -184(%rbp)
	movq	-168(%rbp), %rdi
	addl	$1, %eax
	cmpl	12(%rdi), %eax
	jl	.L970
	movl	-152(%rbp), %edi
	movq	-208(%rbp), %r15
	testl	%edi, %edi
	jle	.L1153
	movl	-152(%rbp), %ebx
	movl	$0, -108(%rbp)
	leal	1(%rbx), %eax
	movl	%eax, -112(%rbp)
	cltq
	leaq	0(,%rax,4), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	movq	%rax, -104(%rbp)
	je	.L977
	movslq	%ebx, %rbx
	movl	$0, -144(%rbp)
	leal	(%rbx,%rbx,2), %eax
	cltq
	leaq	12288(,%rax,8), %r12
	movq	%r12, %rdi
	call	__libc_alloca_cutoff
	testl	%eax, %eax
	leaq	-12288(%r12), %rdi
	je	.L1154
	addq	$30, %rdi
	movb	$0, -218(%rbp)
	andq	$-16, %rdi
	subq	%rdi, %rsp
	leaq	15(%rsp), %r14
	andq	$-16, %r14
	movq	%r14, -160(%rbp)
.L981:
	movq	-160(%rbp), %rax
	salq	$3, %rbx
	movq	-176(%rbp), %rdi
	pxor	%xmm0, %xmm0
	movq	-232(%rbp), %rsi
	xorl	%r12d, %r12d
	movb	$0, -219(%rbp)
	addq	%rbx, %rax
	leaq	4(%rdi), %r14
	addq	%rax, %rbx
	movq	%rax, -192(%rbp)
	movl	-152(%rbp), %eax
	movaps	%xmm0, -80(%rbp)
	movq	%rbx, -200(%rbp)
	leaq	-112(%rbp), %rbx
	movq	%rsi, -256(%rbp)
	subl	$1, %eax
	movaps	%xmm0, -64(%rbp)
	movl	%eax, -216(%rbp)
	movq	%rax, -248(%rbp)
	salq	$4, %rax
	movq	%rax, -240(%rbp)
	leaq	20(%rdi,%rax), %rax
	movq	%rsi, -152(%rbp)
	movq	%rax, -208(%rbp)
	leaq	-144(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	%r15, %rax
	movq	%r14, %r15
	movq	%rax, %r14
.L995:
	movl	$0, -108(%rbp)
	movl	(%r15), %esi
	testl	%esi, %esi
	jle	.L984
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L986:
	movq	4(%r15), %rax
	movslq	(%rax,%r13,4), %rdx
	movq	24(%r14), %rax
	movl	(%rax,%rdx,4), %eax
	cmpl	$-1, %eax
	je	.L988
	movslq	%eax, %rsi
	movq	%rbx, %rdi
	movq	%rsi, %rax
	salq	$4, %rax
	addq	48(%r14), %rax
	movq	%rax, %rsi
	call	re_node_set_merge
	testl	%eax, %eax
	movl	%eax, -144(%rbp)
	jne	.L987
.L988:
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, (%r15)
	jg	.L986
.L984:
	movq	-184(%rbp), %rdi
	xorl	%ecx, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	re_acquire_state_context
	movq	-160(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, (%rdi,%r12)
	je	.L1155
.L989:
	cmpb	$0, 80(%rax)
	js	.L1156
	movq	-192(%rbp), %rsi
	movq	-200(%rbp), %rdi
	movq	%rax, (%rsi,%r12)
	movq	-160(%rbp), %rax
	movq	(%rax,%r12), %rax
	movq	%rax, (%rdi,%r12)
.L993:
	movq	-152(%rbp), %rsi
	leaq	-80(%rbp), %rdx
	xorl	%eax, %eax
.L994:
	movq	(%rdx,%rax), %rcx
	orq	(%rsi,%rax), %rcx
	movq	%rcx, (%rdx,%rax)
	addq	$8, %rax
	cmpq	$32, %rax
	jne	.L994
	addq	$16, %r15
	addq	$32, -152(%rbp)
	addq	$8, %r12
	cmpq	%r15, -208(%rbp)
	jne	.L995
	cmpb	$0, -219(%rbp)
	movq	%r14, %r15
	movq	%rdx, -152(%rbp)
	jne	.L996
	movl	$256, %esi
	movl	$8, %edi
	call	calloc@PLT
	movq	%rax, %rbx
	movq	-168(%rbp), %rax
	testq	%rbx, %rbx
	movq	%rbx, 64(%rax)
	je	.L987
	movq	-232(%rbp), %r10
	movq	-160(%rbp), %r12
	xorl	%r11d, %r11d
	movq	-192(%rbp), %r13
	movq	-152(%rbp), %rdx
.L1003:
	movq	(%rdx,%r11), %rax
	testq	%rax, %rax
	je	.L997
	movq	%r11, %rsi
	movl	$1, %ecx
	salq	$6, %rsi
	addq	%rbx, %rsi
	.p2align 4,,10
	.p2align 3
.L1002:
	testb	$1, %al
	jne	.L1157
.L998:
	addq	%rcx, %rcx
	addq	$8, %rsi
	shrq	%rax
	jne	.L1002
.L997:
	addq	$8, %r11
	addq	$8, %r10
	cmpq	$32, %r11
	jne	.L1003
.L1004:
	testb	$4, -79(%rbp)
	je	.L1013
	movq	-248(%rbp), %rax
	xorl	%edx, %edx
	movq	-256(%rbp), %rcx
	addq	$1, %rax
	jmp	.L1014
.L1012:
	addq	$1, %rdx
	addq	$32, %rcx
	cmpq	%rdx, %rax
	je	.L1013
.L1014:
	testq	$1024, (%rcx)
	je	.L1012
	movq	-200(%rbp), %rax
	cmpb	$0, -219(%rbp)
	movq	(%rax,%rdx,8), %rax
	movq	%rax, 80(%rbx)
	je	.L1013
	movq	%rax, 2128(%rbx)
.L1013:
	cmpb	$0, -218(%rbp)
	jne	.L1158
.L1015:
	movq	-104(%rbp), %rdi
	call	free@PLT
	movq	-176(%rbp), %rax
	movq	-240(%rbp), %rdi
	leaq	8(%rax), %rbx
	leaq	24(%rax,%rdi), %r12
.L1016:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	call	free@PLT
	cmpq	%rbx, %r12
	jne	.L1016
	cmpb	$0, -217(%rbp)
	jne	.L1159
	movb	$1, -217(%rbp)
	jmp	.L927
	.p2align 4,,10
	.p2align 3
.L1149:
	movzbl	(%r14), %ecx
	leaq	-144(%rbp), %rbx
	movl	$1, %esi
	movq	%rcx, %rdx
	andl	$63, %ecx
	shrq	$3, %rdx
	salq	%cl, %rsi
	andl	$24, %edx
	orq	%rsi, (%rbx,%rdx)
.L933:
	testw	%ax, %ax
	je	.L944
	testb	$32, %al
	jne	.L1160
.L945:
	testb	$-128, %al
	jne	.L1138
	testb	$4, %al
	je	.L953
	cmpl	$1, -160(%rbp)
	je	.L1161
.L949:
	movq	-208(%rbp), %r8
	cmpl	$1, 164(%r8)
	jle	.L1019
	movq	120(%r8), %rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
.L951:
	movq	(%rdi,%rcx), %rdx
	notq	%rdx
	orq	168(%r8,%rcx), %rdx
	andq	(%rbx,%rcx), %rdx
	movq	%rdx, (%rbx,%rcx)
	addq	$8, %rcx
	orq	%rdx, %rsi
	cmpq	$32, %rcx
	jne	.L951
	testq	%rsi, %rsi
	je	.L942
.L953:
	testb	$8, %al
	je	.L944
	cmpl	$1, -160(%rbp)
	je	.L1162
	movq	-208(%rbp), %rdi
	cmpl	$1, 164(%rdi)
	jle	.L1020
.L1166:
	movq	120(%rdi), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
.L956:
	movq	168(%rdi,%rdx), %rax
	andq	(%rsi,%rdx), %rax
	notq	%rax
	andq	(%rbx,%rdx), %rax
	movq	%rax, (%rbx,%rdx)
	addq	$8, %rdx
	orq	%rax, %rcx
	cmpq	$32, %rdx
	jne	.L956
	testq	%rcx, %rcx
	je	.L942
	.p2align 4,,10
	.p2align 3
.L944:
	movl	-152(%rbp), %r8d
	xorl	%r12d, %r12d
	testl	%r8d, %r8d
	jle	.L1163
	movq	%r14, -200(%rbp)
	movq	%r15, %r14
	.p2align 4,,10
	.p2align 3
.L968:
	movq	%r12, %r10
	movl	%r12d, %r15d
	salq	$5, %r10
	cmpl	$1, -160(%rbp)
	jne	.L958
	movq	-200(%rbp), %rax
	movzbl	(%rax), %edx
	movq	%rdx, %rax
	shrq	$3, %rax
	andl	$24, %eax
	addq	%r14, %rax
	movq	(%rax,%r10), %rax
	btq	%rdx, %rax
	jnc	.L965
.L958:
	leaq	(%r14,%r10), %rdi
	leaq	-112(%rbp), %rsi
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.L960:
	movq	(%rbx,%rax), %rdx
	andq	(%rdi,%rax), %rdx
	movq	%rdx, (%rsi,%rax)
	addq	$8, %rax
	orq	%rdx, %rcx
	cmpq	$32, %rax
	jne	.L960
	testq	%rcx, %rcx
	je	.L965
	leaq	-80(%rbp), %r11
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	xorl	%r9d, %r9d
.L961:
	movq	(%rbx,%rcx), %rsi
	movq	(%rdi,%rcx), %rax
	movq	%rsi, %rdx
	notq	%rdx
	andq	%rax, %rdx
	notq	%rax
	andq	%rsi, %rax
	movq	%rdx, (%r11,%rcx)
	orq	%rdx, %r9
	movq	%rax, (%rbx,%rcx)
	addq	$8, %rcx
	orq	%rax, %r13
	cmpq	$32, %rcx
	jne	.L961
	movq	-176(%rbp), %rsi
	movq	%r12, %rdx
	salq	$4, %rdx
	addq	%rsi, %rdx
	testq	%r9, %r9
	je	.L962
	movslq	-152(%rbp), %rdi
	movq	%rdx, -216(%rbp)
	movdqa	-80(%rbp), %xmm0
	movq	%rdi, %rcx
	salq	$4, %rdi
	salq	$5, %rcx
	addq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rcx, %rax
	addq	%r14, %rax
	movups	%xmm0, (%rax)
	movdqa	-64(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	movdqa	-112(%rbp), %xmm0
	movups	%xmm0, (%r14,%r10)
	movdqa	-96(%rbp), %xmm0
	movups	%xmm0, 16(%r14,%r10)
	call	re_node_set_init_copy
	testl	%eax, %eax
	jne	.L966
	addl	$1, -152(%rbp)
	movq	-216(%rbp), %rdx
.L962:
	movq	-168(%rbp), %rax
	movq	-192(%rbp), %rdi
	movq	16(%rax), %rax
	movl	(%rax,%rdi), %esi
	movq	%rdx, %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L966
	testq	%r13, %r13
	je	.L1133
.L965:
	leal	1(%r12), %r15d
	addq	$1, %r12
	cmpl	-152(%rbp), %r15d
	jl	.L968
.L1133:
	movl	%r15d, %r8d
	movq	%r14, %r15
.L967:
	cmpl	%r8d, -152(%rbp)
	jne	.L942
	movslq	-152(%rbp), %r12
	movq	-192(%rbp), %rdi
	movdqa	-144(%rbp), %xmm0
	movq	%r12, %rax
	salq	$4, %r12
	addq	-176(%rbp), %r12
	salq	$5, %rax
	addq	%r15, %rax
	movups	%xmm0, (%rax)
	movdqa	-128(%rbp), %xmm0
	movups	%xmm0, 16(%rax)
	movq	-168(%rbp), %rax
	movq	16(%rax), %rax
	movl	(%rax,%rdi), %r13d
	movabsq	$4294967297, %rax
	movl	$4, %edi
	movq	%rax, (%r12)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r12)
	je	.L1164
	addl	$1, -152(%rbp)
	movl	%r13d, (%rax)
.L1138:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, (%rbx)
	movaps	%xmm0, 16(%rbx)
	jmp	.L942
.L1164:
	movq	$0, (%r12)
	.p2align 4,,10
	.p2align 3
.L966:
	movl	-152(%rbp), %eax
	testl	%eax, %eax
	jle	.L1147
	movq	-176(%rbp), %rsi
	subl	$1, %eax
	salq	$4, %rax
	leaq	8(%rsi), %rbx
	leaq	24(%rsi,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L973:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	call	free@PLT
	cmpq	%rbx, %r12
	jne	.L973
.L1147:
	cmpb	$0, -217(%rbp)
	jne	.L1165
.L1140:
	movb	$0, -217(%rbp)
.L927:
	movzbl	-217(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1151:
	movq	-208(%rbp), %rdi
	cmpl	$1, 164(%rdi)
	jle	.L937
	movq	120(%rdi), %rsi
	leaq	-144(%rbp), %rbx
	xorl	%edx, %edx
.L938:
	movq	(%rbx,%rdx), %rcx
	orq	(%rsi,%rdx), %rcx
	movq	%rcx, (%rbx,%rdx)
	addq	$8, %rdx
	cmpq	$32, %rdx
	jne	.L938
.L1145:
	movq	-208(%rbp), %rsi
	movq	200(%rsi), %rdx
	testb	$64, %dl
	jne	.L943
	andq	$-1025, -144(%rbp)
.L943:
	andl	$128, %edx
	je	.L933
	andq	$-2, -144(%rbp)
	jmp	.L933
	.p2align 4,,10
	.p2align 3
.L1160:
	movq	-144(%rbp), %rdx
	pxor	%xmm0, %xmm0
	andb	$4, %dh
	movaps	%xmm0, (%rbx)
	movaps	%xmm0, 16(%rbx)
	je	.L942
	movq	$1024, -144(%rbp)
	jmp	.L945
.L1020:
	movq	-208(%rbp), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
.L955:
	movq	168(%rsi,%rdx), %rax
	notq	%rax
	andq	(%rbx,%rdx), %rax
	movq	%rax, (%rbx,%rdx)
	addq	$8, %rdx
	orq	%rax, %rcx
	cmpq	$32, %rdx
	jne	.L955
	testq	%rcx, %rcx
	jne	.L944
	jmp	.L942
.L1162:
	testb	$64, 10(%r14)
	jne	.L1138
	movq	-208(%rbp), %rdi
	cmpl	$1, 164(%rdi)
	jg	.L1166
	jmp	.L1020
.L1161:
	testb	$64, 10(%r14)
	jne	.L949
	jmp	.L1138
.L937:
	pcmpeqd	%xmm0, %xmm0
	leaq	-144(%rbp), %rbx
	movaps	%xmm0, -144(%rbp)
	movaps	%xmm0, -128(%rbp)
	jmp	.L1145
.L1019:
	movq	-208(%rbp), %rdi
	xorl	%edx, %edx
	xorl	%esi, %esi
.L950:
	movq	(%rbx,%rdx), %rcx
	andq	168(%rdi,%rdx), %rcx
	movq	%rcx, (%rbx,%rdx)
	addq	$8, %rdx
	orq	%rcx, %rsi
	cmpq	$32, %rdx
	jne	.L950
	testq	%rsi, %rsi
	jne	.L953
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L1150:
	movq	(%r14), %rsi
	leaq	-144(%rbp), %rbx
	xorl	%edx, %edx
.L935:
	movq	(%rbx,%rdx), %rcx
	orq	(%rsi,%rdx), %rcx
	movq	%rcx, (%rbx,%rdx)
	addq	$8, %rdx
	cmpq	$32, %rdx
	jne	.L935
	jmp	.L933
.L1152:
	pcmpeqd	%xmm0, %xmm0
	leaq	-144(%rbp), %rbx
	movaps	%xmm0, -144(%rbp)
	jmp	.L1145
	.p2align 4,,10
	.p2align 3
.L1165:
	movq	-176(%rbp), %rdi
	call	free@PLT
	jmp	.L1140
.L1155:
	movl	-144(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L989
	.p2align 4,,10
	.p2align 3
.L987:
	cmpb	$0, -218(%rbp)
	jne	.L982
.L1139:
	movq	-104(%rbp), %r13
.L980:
	movq	%r13, %rdi
	call	free@PLT
	movl	-216(%rbp), %eax
	movq	-176(%rbp), %rsi
	leaq	8(%rsi), %rbx
	salq	$4, %rax
	leaq	24(%rsi,%rax), %r12
	.p2align 4,,10
	.p2align 3
.L983:
	movq	(%rbx), %rdi
	addq	$16, %rbx
	call	free@PLT
	cmpq	%rbx, %r12
	jne	.L983
	jmp	.L1147
.L1156:
	movq	-184(%rbp), %rdi
	movl	$1, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	re_acquire_state_context
	movq	-192(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, (%rdi,%r12)
	je	.L1167
.L991:
	movq	-160(%rbp), %rsi
	cmpq	(%rsi,%r12), %rax
	je	.L992
	movzbl	-219(%rbp), %esi
	cmpl	$2, 164(%r14)
	movl	$1, %eax
	cmovge	%eax, %esi
	movb	%sil, -219(%rbp)
.L992:
	movq	-184(%rbp), %rdi
	movl	$2, %ecx
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	re_acquire_state_context
	movq	-200(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, (%rdi,%r12)
	jne	.L993
	movl	-144(%rbp), %eax
	testl	%eax, %eax
	je	.L993
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L982:
	movq	-160(%rbp), %rdi
	call	free@PLT
	jmp	.L1139
.L928:
	movl	$12288, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -176(%rbp)
	movb	$1, -217(%rbp)
	jne	.L929
	jmp	.L1140
.L1157:
	movq	-176(%rbp), %rdi
	testq	%rcx, 4096(%rdi,%r11)
	jne	.L1023
	movl	$1, %edi
	jmp	.L1000
	.p2align 4,,10
	.p2align 3
.L1024:
	movq	%r9, %rdi
.L1000:
	movq	%rdi, %r8
	leaq	1(%rdi), %r9
	salq	$5, %r8
	testq	%rcx, (%r10,%r8)
	je	.L1024
	salq	$3, %rdi
.L999:
	testq	%rcx, 168(%r15,%r11)
	je	.L1001
	movq	0(%r13,%rdi), %rdi
	movq	%rdi, (%rsi)
	jmp	.L998
.L977:
	movl	-152(%rbp), %eax
	movl	$12, -144(%rbp)
	subl	$1, %eax
	movl	%eax, -216(%rbp)
	jmp	.L980
.L1163:
	xorl	%r8d, %r8d
	jmp	.L967
.L1001:
	movq	(%r12,%rdi), %rdi
	movq	%rdi, (%rsi)
	jmp	.L998
.L1159:
	movq	-176(%rbp), %rdi
	call	free@PLT
	jmp	.L927
.L1158:
	movq	-160(%rbp), %rdi
	call	free@PLT
	jmp	.L1015
.L931:
	cmpb	$0, -217(%rbp)
	jne	.L1168
.L1017:
	movl	$256, %esi
	movl	$8, %edi
	call	calloc@PLT
	movq	-168(%rbp), %rbx
	testq	%rax, %rax
	setne	-217(%rbp)
	movq	%rax, 64(%rbx)
	jmp	.L927
.L1167:
	movl	-144(%rbp), %edx
	testl	%edx, %edx
	je	.L991
	jmp	.L987
	.p2align 4,,10
	.p2align 3
.L1023:
	xorl	%edi, %edi
	jmp	.L999
.L1154:
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -160(%rbp)
	je	.L1169
	movb	$1, -218(%rbp)
	jmp	.L981
.L996:
	movl	$512, %esi
	movl	$8, %edi
	call	calloc@PLT
	movq	%rax, %rbx
	movq	-168(%rbp), %rax
	testq	%rbx, %rbx
	movq	%rbx, 72(%rax)
	je	.L987
	movq	-232(%rbp), %r11
	movq	-160(%rbp), %r10
	movq	%rbx, %r12
	movq	-192(%rbp), %r13
	movq	-152(%rbp), %rdx
	xorl	%r8d, %r8d
.L1010:
	movq	(%rdx,%r8), %rax
	testq	%rax, %rax
	je	.L1005
	movq	%r12, %rsi
	movl	$1, %ecx
.L1009:
	testb	$1, %al
	jne	.L1170
.L1006:
	addq	%rcx, %rcx
	addq	$8, %rsi
	shrq	%rax
	jne	.L1009
.L1005:
	addq	$8, %r8
	addq	$512, %r12
	addq	$8, %r11
	cmpq	$32, %r8
	jne	.L1010
	jmp	.L1004
.L1170:
	movq	-176(%rbp), %rdi
	testq	%rcx, 4096(%rdi,%r8)
	jne	.L1025
	movl	$1, %edi
	jmp	.L1008
.L1026:
	movq	%r14, %rdi
.L1008:
	movq	%rdi, %r9
	leaq	1(%rdi), %r14
	salq	$5, %r9
	testq	%rcx, (%r11,%r9)
	je	.L1026
.L1007:
	movq	(%r10,%rdi,8), %r9
	movq	%r9, (%rsi)
	movq	0(%r13,%rdi,8), %rdi
	movq	%rdi, 2048(%rsi)
	jmp	.L1006
.L1025:
	xorl	%edi, %edi
	jmp	.L1007
.L1168:
	movq	-176(%rbp), %rdi
	call	free@PLT
	jmp	.L1017
.L1153:
	cmpb	$0, -217(%rbp)
	je	.L975
	movq	-176(%rbp), %rdi
	call	free@PLT
.L975:
	cmpl	$0, -152(%rbp)
	je	.L1017
	jmp	.L1140
.L1169:
	movl	-152(%rbp), %eax
	subl	$1, %eax
	movl	%eax, -216(%rbp)
	jmp	.L980
	.size	build_trtable, .-build_trtable
	.p2align 4,,15
	.type	re_node_set_add_intersect, @function
re_node_set_add_intersect:
	movl	4(%rsi), %r10d
	xorl	%eax, %eax
	testl	%r10d, %r10d
	je	.L1209
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	4(%rdx), %ebx
	testl	%ebx, %ebx
	je	.L1171
	movl	4(%rdi), %ecx
	leal	(%r10,%rbx), %eax
	movq	%rdx, %r13
	movl	(%rdi), %edx
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	8(%rdi), %rdi
	leal	(%rax,%rcx), %esi
	cmpl	%edx, %esi
	jg	.L1213
.L1173:
	leal	(%rcx,%r10), %r12d
	movq	8(%r14), %rsi
	movq	8(%r13), %rdx
	subl	$1, %r10d
	subl	$1, %ecx
	addl	%ebx, %r12d
	subl	$1, %ebx
	movslq	%r10d, %rax
	movslq	%ebx, %r8
	movl	(%rsi,%rax,4), %eax
	movl	(%rdx,%r8,4), %r9d
	.p2align 4,,10
	.p2align 3
.L1174:
	cmpl	%r9d, %eax
	je	.L1214
.L1175:
	jge	.L1181
	subl	$1, %ebx
	js	.L1179
.L1212:
	movslq	%ebx, %r8
	movl	(%rdx,%r8,4), %r9d
	cmpl	%r9d, %eax
	jne	.L1175
.L1214:
	testl	%ecx, %ecx
	js	.L1176
	movslq	%ecx, %rax
	movl	(%rdi,%rax,4), %r11d
	leaq	0(,%rax,4), %r8
	cmpl	%r9d, %r11d
	jle	.L1177
	leaq	-4(%rdi,%r8), %r8
	jmp	.L1178
	.p2align 4,,10
	.p2align 3
.L1215:
	subq	$4, %r8
	movl	4(%r8), %r11d
	cmpl	%r9d, %r11d
	jle	.L1177
.L1178:
	subl	$1, %ecx
	cmpl	$-1, %ecx
	jne	.L1215
.L1176:
	subl	$1, %r12d
	movslq	%r12d, %rax
	movl	%r9d, (%rdi,%rax,4)
.L1188:
	subl	$1, %r10d
	js	.L1179
	subl	$1, %ebx
	js	.L1179
	movslq	%r10d, %rax
	movl	(%rsi,%rax,4), %eax
	jmp	.L1212
	.p2align 4,,10
	.p2align 3
.L1179:
	movl	4(%rbp), %edx
	movl	4(%r14), %eax
	addl	%edx, %eax
	addl	4(%r13), %eax
	leal	-1(%rdx), %ecx
	leal	-1(%rax), %r9d
	subl	%r12d, %eax
	addl	%eax, %edx
	testl	%eax, %eax
	movl	%edx, 4(%rbp)
	jle	.L1207
	testl	%ecx, %ecx
	movslq	%r9d, %r11
	movslq	%ecx, %r10
	jns	.L1183
.L1207:
	cltq
	leaq	0(,%rax,4), %rdx
.L1185:
	movslq	%r12d, %r12
	leaq	(%rdi,%r12,4), %rsi
	call	memcpy@PLT
	xorl	%eax, %eax
.L1171:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1181:
	subl	$1, %r10d
	js	.L1179
	movslq	%r10d, %rax
	movl	(%rsi,%rax,4), %eax
	jmp	.L1174
	.p2align 4,,10
	.p2align 3
.L1177:
	cmpl	%r9d, %r11d
	jne	.L1176
	jmp	.L1188
	.p2align 4,,10
	.p2align 3
.L1209:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1186:
	subl	$1, %ecx
	movl	%esi, (%rdx)
	cmpl	$-1, %ecx
	je	.L1207
	movslq	%ecx, %r10
.L1183:
	movl	(%rdi,%r11,4), %r8d
	movl	(%rdi,%r10,4), %esi
	leal	(%rcx,%rax), %edx
	movslq	%edx, %rdx
	cmpl	%esi, %r8d
	leaq	(%rdi,%rdx,4), %rdx
	jle	.L1186
	subl	$1, %r9d
	subl	$1, %eax
	movl	%r8d, (%rdx)
	je	.L1193
	movslq	%r9d, %r11
	jmp	.L1183
	.p2align 4,,10
	.p2align 3
.L1213:
	leal	(%rax,%rdx), %ebx
	movslq	%ebx, %rsi
	salq	$2, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L1191
	movl	%ebx, 0(%rbp)
	movq	%rax, 8(%rbp)
	movl	4(%rbp), %ecx
	movl	4(%r14), %r10d
	movl	4(%r13), %ebx
	jmp	.L1173
.L1193:
	xorl	%edx, %edx
	jmp	.L1185
.L1191:
	movl	$12, %eax
	jmp	.L1171
	.size	re_node_set_add_intersect, .-re_node_set_add_intersect
	.p2align 4,,15
	.type	sub_epsilon_src_nodes, @function
sub_epsilon_src_nodes:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rbp
	movq	%rbp, %r13
	salq	$4, %rbp
	subq	$56, %rsp
	addq	56(%rdi), %rbp
	pxor	%xmm0, %xmm0
	movq	%rcx, 16(%rsp)
	movaps	%xmm0, 32(%rsp)
	movl	4(%rbp), %r10d
	testl	%r10d, %r10d
	jle	.L1226
	leaq	32(%rsp), %rax
	movq	%rdi, %r14
	movq	%rdx, %r12
	xorl	%ebx, %ebx
	movq	%rax, 24(%rsp)
	jmp	.L1223
	.p2align 4,,10
	.p2align 3
.L1244:
	testl	%eax, %eax
	jne	.L1220
	movl	4(%r12), %edi
	leaq	8(%r12), %rsi
	movl	%r8d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L1220
.L1221:
	movq	56(%r14), %rdx
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdi
	addq	%r9, %rdx
	call	re_node_set_add_intersect
	testl	%eax, %eax
	jne	.L1239
	movl	4(%rbp), %r10d
	.p2align 4,,10
	.p2align 3
.L1218:
	leal	1(%rbx), %eax
	addq	$1, %rbx
	cmpl	%eax, %r10d
	jle	.L1243
.L1223:
	movq	8(%rbp), %rax
	movslq	(%rax,%rbx,4), %r9
	cmpl	%r9d, %r13d
	je	.L1218
	movq	(%r14), %rax
	salq	$4, %r9
	testb	$8, 8(%rax,%r9)
	je	.L1218
	movq	40(%r14), %rcx
	leaq	8(%rbp), %r11
	movl	%r10d, %edi
	movq	%r11, %rsi
	addq	%r9, %rcx
	movq	8(%rcx), %r15
	movq	%rcx, 8(%rsp)
	movl	(%r15), %r8d
	movl	%r8d, %edx
	movl	%r8d, 4(%rsp)
	call	re_node_set_contains.isra.4
	movq	8(%rsp), %rcx
	movl	4(%rsp), %r8d
	cmpl	$1, 4(%rcx)
	jg	.L1244
	testl	%eax, %eax
	jne	.L1218
	movl	4(%r12), %edi
	leaq	8(%r12), %rsi
	movl	%r8d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L1221
	jmp	.L1218
	.p2align 4,,10
	.p2align 3
.L1220:
	movl	4(%r15), %r15d
	testl	%r15d, %r15d
	jle	.L1218
	movl	%r15d, %edx
	movq	%r11, %rsi
	movl	%r10d, %edi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L1218
	movl	4(%r12), %edi
	leaq	8(%r12), %rsi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L1218
	jmp	.L1221
	.p2align 4,,10
	.p2align 3
.L1243:
	xorl	%r9d, %r9d
	testl	%r10d, %r10d
	leaq	40(%rsp), %r11
	jle	.L1242
	.p2align 4,,10
	.p2align 3
.L1224:
	movq	8(%rbp), %rax
	movl	36(%rsp), %edi
	movq	%r11, %rsi
	movl	(%rax,%r9,4), %r10d
	movl	%r10d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L1225
	movl	4(%r12), %edi
	leaq	8(%r12), %rsi
	call	re_node_set_contains.isra.4
	subl	$1, %eax
	movl	%eax, %esi
	js	.L1225
	movq	%r12, %rdi
	call	re_node_set_remove_at.part.5
.L1225:
	leal	1(%r9), %eax
	addq	$1, %r9
	cmpl	%eax, 4(%rbp)
	jg	.L1224
.L1242:
	movq	40(%rsp), %rdi
.L1217:
	call	free@PLT
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1239:
	movq	40(%rsp), %rdi
	movl	%eax, 4(%rsp)
	call	free@PLT
	movl	4(%rsp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L1226:
	xorl	%edi, %edi
	jmp	.L1217
	.size	sub_epsilon_src_nodes, .-sub_epsilon_src_nodes
	.p2align 4,,15
	.type	re_string_context_at, @function
re_string_context_at:
	testl	%esi, %esi
	js	.L1267
	cmpl	%esi, 64(%rdi)
	je	.L1268
	cmpl	$1, 104(%rdi)
	jle	.L1269
	pushq	%rbp
	pushq	%rbx
	movslq	%esi, %rdx
	salq	$2, %rdx
	movl	%esi, %esi
	subq	$8, %rsp
	movq	16(%rdi), %rcx
	salq	$2, %rsi
	leaq	(%rcx,%rdx), %rax
	leaq	-4(%rcx,%rdx), %rdx
	subq	%rsi, %rdx
	jmp	.L1252
	.p2align 4,,10
	.p2align 3
.L1253:
	subq	$4, %rax
	cmpq	%rdx, %rax
	je	.L1270
.L1252:
	movl	(%rax), %ebx
	cmpl	$-1, %ebx
	je	.L1253
	cmpb	$0, 102(%rdi)
	movq	%rdi, %rbp
	jne	.L1271
.L1254:
	xorl	%eax, %eax
	cmpl	$10, %ebx
	je	.L1272
.L1245:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1269:
	movq	8(%rdi), %rax
	movslq	%esi, %rsi
	movzbl	(%rax,%rsi), %ecx
	movq	88(%rdi), %rsi
	movl	%ecx, %eax
	sarl	$6, %eax
	cltq
	movq	(%rsi,%rax,8), %rsi
	movl	$1, %eax
	btq	%rcx, %rsi
	jc	.L1265
	xorl	%eax, %eax
	cmpl	$10, %ecx
	jne	.L1265
	xorl	%eax, %eax
	cmpb	$0, 101(%rdi)
	setne	%al
	addl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1267:
	movl	76(%rdi), %eax
.L1265:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1270:
	movl	76(%rdi), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1272:
	xorl	%eax, %eax
	cmpb	$0, 101(%rbp)
	setne	%al
	addl	%eax, %eax
	jmp	.L1245
	.p2align 4,,10
	.p2align 3
.L1268:
	andl	$2, %edx
	cmpl	$1, %edx
	sbbl	%eax, %eax
	andl	$2, %eax
	addl	$8, %eax
	ret
.L1271:
	movl	%ebx, %edi
	call	__iswalnum
	testl	%eax, %eax
	jne	.L1256
	cmpl	$95, %ebx
	jne	.L1254
.L1256:
	movl	$1, %eax
	jmp	.L1245
	.size	re_string_context_at, .-re_string_context_at
	.p2align 4,,15
	.type	check_node_accept, @function
check_node_accept:
	movq	8(%rdi), %rcx
	movslq	%edx, %rax
	movzbl	(%rcx,%rax), %ecx
	movzbl	8(%rsi), %eax
	cmpb	$3, %al
	je	.L1275
	jbe	.L1316
	cmpb	$5, %al
	je	.L1278
	cmpb	$7, %al
	jne	.L1286
	xorl	%eax, %eax
	testb	%cl, %cl
	js	.L1314
.L1278:
	cmpb	$10, %cl
	je	.L1317
	testb	%cl, %cl
	jne	.L1280
	movq	112(%rdi), %rcx
	xorl	%eax, %eax
	testb	$-128, 200(%rcx)
	jne	.L1314
	.p2align 4,,10
	.p2align 3
.L1280:
	pushq	%rbx
	movl	8(%rsi), %ebx
	movl	$1, %eax
	testl	$261888, %ebx
	je	.L1273
	movl	%edx, %esi
	movl	120(%rdi), %edx
	shrl	$8, %ebx
	call	re_string_context_at
	movl	%ebx, %ecx
	movl	%eax, %edx
	andw	$1023, %cx
	testb	$4, %bl
	je	.L1282
	xorl	%eax, %eax
	testb	$1, %dl
	je	.L1273
	andl	$8, %ebx
	jne	.L1273
.L1284:
	testb	$32, %cl
	je	.L1285
	xorl	%eax, %eax
	testb	$2, %dl
	je	.L1273
.L1285:
	shrl	$3, %edx
	movl	$1, %eax
	andl	$1, %edx
	andl	$128, %ecx
	cmovne	%edx, %eax
.L1273:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L1316:
	cmpb	$1, %al
	jne	.L1286
	xorl	%eax, %eax
	cmpb	%cl, (%rsi)
	je	.L1280
.L1314:
	rep ret
	.p2align 4,,10
	.p2align 3
.L1286:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1275:
	movq	%rcx, %rax
	movq	(%rsi), %r8
	shrq	$6, %rax
	andl	$3, %eax
	movq	(%r8,%rax,8), %r8
	xorl	%eax, %eax
	btq	%rcx, %r8
	jc	.L1280
	rep ret
	.p2align 4,,10
	.p2align 3
.L1282:
	andl	$8, %ebx
	je	.L1284
	testb	$1, %al
	je	.L1284
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L1317:
	movq	112(%rdi), %rcx
	xorl	%eax, %eax
	testb	$64, 200(%rcx)
	jne	.L1280
	rep ret
	.size	check_node_accept, .-check_node_accept
	.p2align 4,,15
	.type	check_halt_state_context, @function
check_halt_state_context:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edx, %esi
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	120(%rdi), %edx
	call	re_string_context_at
	movl	12(%rbp), %edx
	testl	%edx, %edx
	jle	.L1326
	movq	112(%rbx), %rcx
	movq	16(%rbp), %rsi
	movl	%eax, %ebx
	movl	%eax, %r11d
	subl	$1, %edx
	andl	$1, %eax
	andl	$8, %ebx
	andl	$2, %r11d
	movl	%eax, %r10d
	movq	(%rcx), %r8
	leaq	4(%rsi,%rdx,4), %r9
	jmp	.L1325
	.p2align 4,,10
	.p2align 3
.L1320:
	addq	$4, %rsi
	cmpq	%rsi, %r9
	je	.L1326
.L1325:
	movslq	(%rsi), %rdx
	movq	%rdx, %rax
	salq	$4, %rdx
	addq	%r8, %rdx
	movl	8(%rdx), %ecx
	shrl	$8, %ecx
	movl	%ecx, %edi
	andw	$1023, %di
	cmpb	$2, 8(%rdx)
	jne	.L1320
	testw	%di, %di
	je	.L1318
	testb	$4, %cl
	je	.L1321
	testl	%r10d, %r10d
	je	.L1320
	andl	$8, %ecx
	jne	.L1320
.L1323:
	testb	$32, %dil
	je	.L1324
.L1348:
	testl	%r11d, %r11d
	je	.L1320
.L1324:
	andl	$128, %edi
	je	.L1318
	testl	%ebx, %ebx
	je	.L1320
.L1318:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1321:
	andl	$8, %ecx
	je	.L1323
	testl	%r10d, %r10d
	jne	.L1320
	testb	$32, %dil
	je	.L1324
	jmp	.L1348
	.p2align 4,,10
	.p2align 3
.L1326:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	check_halt_state_context, .-check_halt_state_context
	.p2align 4,,15
	.type	re_string_reconstruct, @function
re_string_reconstruct:
	pushq	%r15
	pushq	%r14
	movl	%edx, %r9d
	pushq	%r13
	pushq	%r12
	movl	%esi, %r14d
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$88, %rsp
	movl	40(%rdi), %eax
	subl	%eax, %ebp
	cmpl	%esi, %eax
	jle	.L1351
	cmpl	$1, 104(%rdi)
	jg	.L1473
.L1352:
	movl	60(%rbx), %eax
	movq	$0, 40(%rbx)
	movl	%r14d, %ebp
	movl	$0, 48(%rbx)
	movb	$0, 100(%rbx)
	movl	%eax, 64(%rbx)
	movl	68(%rbx), %eax
	movl	%eax, 72(%rbx)
	movl	%r9d, %eax
	andl	$1, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	addl	$4, %eax
	cmpb	$0, 99(%rbx)
	movl	%eax, 76(%rbx)
	je	.L1474
.L1351:
	testl	%ebp, %ebp
	je	.L1355
	movl	48(%rbx), %eax
	movzbl	100(%rbx), %edx
	movl	44(%rbx), %r12d
	cmpl	%ebp, %eax
	jle	.L1356
	testb	%dl, %dl
	jne	.L1475
	leal	-1(%rbp), %esi
	movl	%r9d, %edx
	movq	%rbx, %rdi
	subl	%ebp, %r12d
	call	re_string_context_at
	cmpl	$1, 104(%rbx)
	movl	%eax, 76(%rbx)
	jg	.L1476
.L1375:
	cmpb	$0, 99(%rbx)
	jne	.L1477
.L1376:
	subl	%ebp, 48(%rbx)
	movzbl	99(%rbx), %eax
	movl	%r12d, 44(%rbx)
.L1363:
	testb	%al, %al
	jne	.L1355
.L1406:
	movslq	%ebp, %rax
	addq	%rax, 8(%rbx)
.L1355:
	movl	64(%rbx), %eax
	subl	%ebp, 72(%rbx)
	movl	%r14d, 40(%rbx)
	subl	%ebp, %eax
	cmpl	$1, 104(%rbx)
	movl	%eax, 64(%rbx)
	jle	.L1410
	cmpb	$0, 96(%rbx)
	movq	%rbx, %rdi
	je	.L1411
	call	build_wcs_upper_buffer
	testl	%eax, %eax
	jne	.L1349
.L1413:
	movl	$0, 56(%rbx)
	xorl	%eax, %eax
.L1349:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1474:
	movq	(%rbx), %rax
	movq	%rax, 8(%rbx)
	jmp	.L1351
	.p2align 4,,10
	.p2align 3
.L1473:
	movq	$0, 32(%rdi)
	jmp	.L1352
	.p2align 4,,10
	.p2align 3
.L1476:
	movq	16(%rbx), %rdi
	movslq	%r12d, %r12
	movslq	%ebp, %rax
	leaq	0(,%r12,4), %rdx
	leaq	(%rdi,%rax,4), %rsi
	call	memmove
	movl	44(%rbx), %r12d
	subl	%ebp, %r12d
	jmp	.L1375
	.p2align 4,,10
	.p2align 3
.L1411:
	call	build_wcs_buffer
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1410:
	cmpb	$0, 99(%rbx)
	jne	.L1478
	movl	%eax, 44(%rbx)
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1356:
	testb	%dl, %dl
	jne	.L1479
.L1377:
	movl	104(%rbx), %edx
	movl	$0, 44(%rbx)
	movl	40(%rbx), %r10d
	cmpl	$1, %edx
	jle	.L1378
	cmpb	$0, 97(%rbx)
	je	.L1379
	movq	(%rbx), %rcx
	movslq	%r10d, %r13
	movslq	%ebp, %r15
	addq	%rcx, %r13
	leaq	-1(%r13,%r15), %rsi
	movzbl	(%rsi), %edi
	testb	$-128, %dil
	jne	.L1380
	cmpq	$0, 80(%rbx)
	jne	.L1380
	movq	$0, 32(%rbx)
	movzbl	(%rsi), %edx
	xorl	%r13d, %r13d
.L1381:
	cmpb	$0, 102(%rbx)
	jne	.L1480
.L1399:
	xorl	%eax, %eax
	cmpl	$10, %edx
	jne	.L1400
	xorl	%eax, %eax
	cmpb	$0, 101(%rbx)
	setne	%al
	addl	%eax, %eax
.L1400:
	movl	%eax, 76(%rbx)
.L1398:
	cmpl	$0, %r13d
	jne	.L1401
.L1471:
	movzbl	99(%rbx), %eax
	movl	%r13d, 48(%rbx)
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1475:
	movq	24(%rbx), %r8
	movl	%r12d, %ecx
	xorl	%esi, %esi
	jmp	.L1360
	.p2align 4,,10
	.p2align 3
.L1482:
	jge	.L1359
	leal	1(%rax), %esi
.L1358:
	cmpl	%ecx, %esi
	jge	.L1481
.L1360:
	leal	(%rsi,%rcx), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %r13d
	movl	(%r8,%rdx,4), %edx
	cmpl	%ebp, %edx
	jle	.L1482
	movl	%eax, %ecx
	jmp	.L1358
	.p2align 4,,10
	.p2align 3
.L1481:
	leal	1(%rax), %r13d
	cmpl	%ebp, %edx
	cmovge	%eax, %r13d
.L1359:
	leal	-1(%r13), %r15d
	movl	%r9d, %edx
	movq	%rbx, %rdi
	movq	%r8, 8(%rsp)
	movl	%r15d, %esi
	call	re_string_context_at
	cmpl	%r13d, %ebp
	movl	%eax, 76(%rbx)
	movq	8(%rsp), %r8
	jne	.L1361
	cmpl	%r12d, %ebp
	jge	.L1361
	movslq	%r13d, %rax
	cmpl	%ebp, (%r8,%rax,4)
	jne	.L1361
	movq	16(%rbx), %rdi
	movslq	%ebp, %r15
	movl	%r12d, %edx
	leaq	0(,%r15,4), %r13
	subl	%ebp, %edx
	movslq	%edx, %rdx
	leaq	(%rdi,%r13), %rsi
	salq	$2, %rdx
	call	memmove
	movq	8(%rbx), %rdi
	movl	44(%rbx), %edx
	leaq	(%rdi,%r15), %rsi
	subl	%ebp, %edx
	movslq	%edx, %rdx
	call	memmove
	movl	44(%rbx), %eax
	subl	%ebp, 48(%rbx)
	subl	%ebp, %eax
	testl	%eax, %eax
	movl	%eax, 44(%rbx)
	jle	.L1472
	movq	24(%rbx), %rsi
	movl	$1, %eax
	leaq	(%rsi,%r13), %rcx
	.p2align 4,,10
	.p2align 3
.L1364:
	movl	-4(%rcx,%rax,4), %edx
	subl	%ebp, %edx
	movl	%edx, -4(%rsi,%rax,4)
	movl	%eax, %edx
	addq	$1, %rax
	cmpl	%edx, 44(%rbx)
	jg	.L1364
	.p2align 4,,10
	.p2align 3
.L1472:
	movzbl	99(%rbx), %eax
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1361:
	movl	60(%rbx), %eax
	movb	$0, 100(%rbx)
	addl	%ebp, %eax
	subl	%r14d, %eax
	movl	%eax, 64(%rbx)
	movl	68(%rbx), %eax
	addl	%ebp, %eax
	subl	%r14d, %eax
	testl	%r13d, %r13d
	movl	%eax, 72(%rbx)
	jle	.L1365
	movslq	%r13d, %rax
	salq	$2, %rax
	cmpl	%ebp, -4(%r8,%rax)
	jne	.L1365
	leaq	-8(%r8,%rax), %rax
	jmp	.L1366
	.p2align 4,,10
	.p2align 3
.L1483:
	subq	$4, %rax
	cmpl	%ebp, 4(%rax)
	jne	.L1365
	subl	$1, %r15d
.L1366:
	testl	%r15d, %r15d
	movl	%r15d, %r13d
	jne	.L1483
.L1365:
	cmpl	%r13d, %r12d
	jle	.L1370
	movq	16(%rbx), %rcx
	movslq	%r13d, %rax
	leaq	0(,%rax,4), %rdx
	cmpl	$-1, (%rcx,%rax,4)
	jne	.L1367
	addq	$4, %rdx
	jmp	.L1371
	.p2align 4,,10
	.p2align 3
.L1368:
	cmpl	$-1, (%rcx,%rdx)
	leaq	4(%rdx), %rax
	jne	.L1367
	movq	%rax, %rdx
.L1371:
	addl	$1, %r13d
	cmpl	%r12d, %r13d
	jne	.L1368
.L1369:
	movl	$0, 44(%rbx)
	xorl	%edx, %edx
.L1372:
	movl	%edx, 48(%rbx)
	movzbl	99(%rbx), %eax
	jmp	.L1363
	.p2align 4,,10
	.p2align 3
.L1477:
	movq	8(%rbx), %rdi
	movslq	%ebp, %rsi
	movslq	%r12d, %rdx
	addq	%rdi, %rsi
	call	memmove
	movl	44(%rbx), %r12d
	subl	%ebp, %r12d
	jmp	.L1376
.L1370:
	je	.L1369
	movslq	%r13d, %r13
	leaq	0(,%r13,4), %rdx
.L1367:
	movl	(%r8,%rdx), %edx
	subl	%ebp, %edx
	cmpl	$0, %edx
	movl	%edx, 44(%rbx)
	je	.L1372
	jle	.L1373
	movq	16(%rbx), %rsi
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1374:
	movl	$-1, -4(%rsi,%rax,4)
	movl	44(%rbx), %edx
	movl	%eax, %ecx
	addq	$1, %rax
	cmpl	%ecx, %edx
	jg	.L1374
.L1373:
	movq	8(%rbx), %rdi
	movslq	%edx, %rdx
	movl	$255, %esi
	call	memset
	movl	44(%rbx), %edx
	jmp	.L1372
.L1390:
	movslq	64(%rbx), %rax
	movq	80(%rbx), %rcx
	movq	%rsi, %r11
	addq	%r13, %rax
	subq	%rsi, %rax
	testq	%rcx, %rcx
	jne	.L1484
.L1383:
	leaq	72(%rsp), %rcx
	leaq	60(%rsp), %rdi
	movslq	%eax, %rdx
	movl	%r9d, 16(%rsp)
	movq	%r11, 8(%rsp)
	addq	%r13, %r15
	movq	$0, 72(%rsp)
	call	__mbrtowc
	movq	8(%rsp), %r11
	movl	16(%rsp), %r9d
	subq	%r11, %r15
	cmpq	%rax, %r15
	ja	.L1470
	cmpq	$-3, %rax
	jbe	.L1388
.L1470:
	movl	40(%rbx), %r10d
	movl	48(%rbx), %eax
	.p2align 4,,10
	.p2align 3
.L1379:
	leal	(%r10,%rax), %r15d
	cmpl	%r15d, %r14d
	jle	.L1391
	leaq	32(%rbx), %rax
	movq	%rax, 16(%rsp)
	leaq	72(%rsp), %rax
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L1395:
	movq	32(%rbx), %rcx
	movl	60(%rbx), %eax
	movslq	%r15d, %r13
	movq	(%rbx), %rsi
	movq	24(%rsp), %rdi
	movl	%r9d, 8(%rsp)
	movq	%rcx, 32(%rsp)
	movq	16(%rsp), %rcx
	subl	%r15d, %eax
	movslq	%eax, %rdx
	addq	%r13, %rsi
	movl	%eax, 44(%rsp)
	call	__mbrtowc
	leaq	-1(%rax), %rdx
	movl	8(%rsp), %r9d
	cmpq	$-4, %rdx
	ja	.L1485
	movl	72(%rsp), %edx
.L1394:
	addl	%eax, %r15d
	cmpl	%r15d, %r14d
	jg	.L1395
	subl	%r14d, %r15d
	cmpl	$-1, %edx
	movl	%r15d, %r13d
	movl	%r15d, 44(%rbx)
	jne	.L1381
.L1397:
	leal	-1(%r12), %esi
	movl	%r9d, %edx
	movq	%rbx, %rdi
	call	re_string_context_at
	movl	%eax, 76(%rbx)
	jmp	.L1398
	.p2align 4,,10
	.p2align 3
.L1478:
	cmpb	$0, 96(%rbx)
	jne	.L1486
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.L1413
	cmpl	%eax, 52(%rbx)
	movl	44(%rbx), %edx
	cmovle	52(%rbx), %eax
	cmpl	%edx, %eax
	jle	.L1417
	movslq	%edx, %rcx
	jmp	.L1418
	.p2align 4,,10
	.p2align 3
.L1487:
	movl	40(%rbx), %r14d
	movq	80(%rbx), %rsi
.L1418:
	leal	(%rdx,%r14), %r8d
	movq	(%rbx), %rdi
	addl	$1, %edx
	movslq	%r8d, %r8
	movzbl	(%rdi,%r8), %edi
	movzbl	(%rsi,%rdi), %edi
	movq	8(%rbx), %rsi
	movb	%dil, (%rsi,%rcx)
	addq	$1, %rcx
	cmpl	%edx, %eax
	jne	.L1487
.L1417:
	movl	%edx, 44(%rbx)
	movl	%edx, 48(%rbx)
	jmp	.L1413
	.p2align 4,,10
	.p2align 3
.L1378:
	movq	80(%rbx), %rdx
	movq	(%rbx), %rax
	addl	%ebp, %r10d
	movslq	%r10d, %r10
	testq	%rdx, %rdx
	movzbl	-1(%rax,%r10), %eax
	movl	$0, 48(%rbx)
	je	.L1407
	movzbl	(%rdx,%rax), %eax
.L1407:
	movzbl	%al, %ecx
	movq	88(%rbx), %rsi
	movl	%ecx, %edx
	sarl	$6, %edx
	movslq	%edx, %rdx
	movq	(%rsi,%rdx,8), %rsi
	movl	$1, %edx
	btq	%rax, %rsi
	jc	.L1408
	xorl	%edx, %edx
	cmpl	$10, %ecx
	jne	.L1408
	xorl	%edx, %edx
	cmpb	$0, 101(%rbx)
	setne	%dl
	addl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L1408:
	movl	%edx, 76(%rbx)
	jmp	.L1472
	.p2align 4,,10
	.p2align 3
.L1380:
	movl	%ebp, %r11d
	subl	%edx, %r11d
	movslq	%r11d, %rdx
	addq	%r13, %rdx
	cmpq	%rdx, %rcx
	cmovb	%rdx, %rcx
	cmpq	%rsi, %rcx
	ja	.L1379
	andl	$-64, %edi
	cmpb	$-128, %dil
	je	.L1382
	jmp	.L1390
	.p2align 4,,10
	.p2align 3
.L1389:
	movzbl	(%rsi), %edx
	andl	$-64, %edx
	cmpb	$-128, %dl
	jne	.L1390
.L1382:
	subq	$1, %rsi
	cmpq	%rsi, %rcx
	jbe	.L1389
	jmp	.L1379
	.p2align 4,,10
	.p2align 3
.L1486:
	movq	%rbx, %rdi
	call	build_upper_buffer
	jmp	.L1413
.L1479:
	movl	60(%rbx), %edx
	movb	$0, 100(%rbx)
	addl	%ebp, %edx
	subl	%r14d, %edx
	movl	%edx, 64(%rbx)
	movl	68(%rbx), %edx
	addl	%ebp, %edx
	subl	%r14d, %edx
	movl	%edx, 72(%rbx)
	jmp	.L1377
.L1485:
	testq	%rax, %rax
	je	.L1422
	movl	44(%rsp), %eax
	testl	%eax, %eax
	je	.L1422
	movq	(%rbx), %rax
	movzbl	(%rax,%r13), %edx
.L1393:
	movq	32(%rsp), %rax
	movq	%rax, 32(%rbx)
	movl	$1, %eax
	jmp	.L1394
.L1401:
	jle	.L1403
	movq	16(%rbx), %rcx
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L1404:
	movl	$-1, -4(%rcx,%rax,4)
	movl	44(%rbx), %r13d
	movl	%eax, %edx
	addq	$1, %rax
	cmpl	%edx, %r13d
	jg	.L1404
.L1403:
	cmpb	$0, 99(%rbx)
	jne	.L1405
	movl	44(%rbx), %eax
	movl	%eax, 48(%rbx)
	jmp	.L1406
.L1480:
	movl	%edx, %edi
	movl	%edx, 8(%rsp)
	call	__iswalnum
	movl	8(%rsp), %edx
	cmpl	$95, %edx
	je	.L1423
	testl	%eax, %eax
	je	.L1399
.L1423:
	movl	$1, %eax
	jmp	.L1400
.L1422:
	xorl	%edx, %edx
	jmp	.L1393
.L1405:
	movq	8(%rbx), %rdi
	movslq	%r13d, %rdx
	movl	$255, %esi
	call	memset
	movl	44(%rbx), %r13d
	jmp	.L1471
.L1391:
	subl	%r14d, %r15d
	movl	%r15d, %r13d
	movl	%r15d, 44(%rbx)
	jmp	.L1397
.L1388:
	movl	60(%rsp), %edx
	movl	%eax, %r13d
	movq	$0, 32(%rbx)
	subl	%r15d, %r13d
	movl	%r13d, 44(%rbx)
	cmpl	$-1, %edx
	jne	.L1381
	jmp	.L1470
.L1484:
	cmpl	$6, %eax
	movl	$6, %edx
	cmovle	%eax, %edx
	subl	$1, %edx
	js	.L1488
	leaq	66(%rsp), %r10
	movslq	%edx, %rdx
.L1385:
	movzbl	(%rsi,%rdx), %edi
	movzbl	(%rcx,%rdi), %edi
	movb	%dil, (%r10,%rdx)
	subq	$1, %rdx
	testl	%edx, %edx
	jns	.L1385
.L1386:
	movq	%r10, %rsi
	jmp	.L1383
.L1488:
	leaq	66(%rsp), %r10
	jmp	.L1386
	.size	re_string_reconstruct, .-re_string_reconstruct
	.p2align 4,,15
	.type	calc_first, @function
calc_first:
	cmpb	$16, 48(%rsi)
	je	.L1497
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	%rsi, 24(%rbx)
	movq	48(%rbx), %rdx
	movq	40(%rsi), %rsi
	call	re_dfa_add_node
	movslq	%eax, %rdx
	cmpl	$-1, %edx
	movl	%edx, 56(%rbx)
	je	.L1492
	xorl	%eax, %eax
	cmpb	$12, 48(%rbx)
	jne	.L1489
	movl	40(%rbx), %ecx
	salq	$4, %rdx
	addq	0(%rbp), %rdx
	andl	$1023, %ecx
	sall	$8, %ecx
	movl	%ecx, %esi
	movl	8(%rdx), %ecx
	andl	$-261889, %ecx
	orl	%esi, %ecx
	movl	%ecx, 8(%rdx)
.L1489:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1497:
	movq	8(%rsi), %rax
	movq	24(%rax), %rdx
	movl	56(%rax), %eax
	movq	%rdx, 24(%rsi)
	movl	%eax, 56(%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L1492:
	movl	$12, %eax
	jmp	.L1489
	.size	calc_first, .-calc_first
	.p2align 4,,15
	.type	free_fail_stack_return.part.20, @function
free_fail_stack_return.part.20:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	(%rdi), %eax
	testl	%eax, %eax
	jle	.L1499
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L1500:
	movq	8(%rbp), %rax
	addl	$1, %r12d
	movq	24(%rax,%rbx), %rdi
	call	free@PLT
	movq	8(%rbp), %rax
	movq	8(%rax,%rbx), %rdi
	addq	$32, %rbx
	call	free@PLT
	cmpl	0(%rbp), %r12d
	jl	.L1500
.L1499:
	popq	%rbx
	movq	8(%rbp), %rdi
	popq	%rbp
	popq	%r12
	jmp	free@PLT
	.size	free_fail_stack_return.part.20, .-free_fail_stack_return.part.20
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"alpha"
.LC1:
	.string	"upper"
.LC2:
	.string	"lower"
.LC3:
	.string	"alnum"
.LC4:
	.string	"cntrl"
.LC5:
	.string	"space"
.LC6:
	.string	"digit"
.LC7:
	.string	"print"
.LC8:
	.string	"blank"
.LC9:
	.string	"graph"
.LC10:
	.string	"punct"
.LC11:
	.string	"xdigit"
	.text
	.p2align 4,,15
	.type	build_charclass.isra.22, @function
build_charclass.isra.22:
	pushq	%r14
	pushq	%r13
	movq	%rcx, %r13
	pushq	%r12
	pushq	%rbp
	movq	%r9, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$16, %rsp
	testq	$4194304, 64(%rsp)
	je	.L1504
	leaq	.LC1(%rip), %rdi
	movl	$6, %ecx
	movq	%r9, %rsi
	repz cmpsb
	jne	.L1682
	leaq	.LC0(%rip), %r12
.L1504:
	movslq	0(%r13), %rcx
	movq	(%rdx), %rax
	cmpl	%ecx, (%r8)
	je	.L1683
.L1505:
	leal	1(%rcx), %edx
	movq	%r12, %rdi
	movl	%edx, 0(%r13)
	leaq	(%rax,%rcx,8), %r13
	call	__wctype@PLT
	leaq	.LC3(%rip), %rdi
	movq	%rax, 0(%r13)
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	je	.L1684
	leaq	.LC4(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	jne	.L1513
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1685
	.p2align 4,,10
	.p2align 3
.L1514:
	testb	$2, (%rdx,%rcx,2)
	je	.L1517
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1517:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1514
.L1512:
	xorl	%eax, %eax
.L1503:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1684:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1686
	.p2align 4,,10
	.p2align 3
.L1508:
	testb	$8, (%rdx,%rcx,2)
	je	.L1511
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1511:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1508
	jmp	.L1512
.L1682:
	leaq	.LC2(%rip), %rdi
	movl	$6, %ecx
	movq	%r9, %rsi
	leaq	.LC0(%rip), %rax
	repz cmpsb
	movslq	0(%r13), %rcx
	cmove	%rax, %r12
	cmpl	%ecx, (%r8)
	movq	(%rdx), %rax
	jne	.L1505
.L1683:
	leal	1(%rcx,%rcx), %r14d
	movq	%rax, %rdi
	movq	%r8, 8(%rsp)
	movq	%rdx, (%rsp)
	movslq	%r14d, %rsi
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L1570
	movq	(%rsp), %rdx
	movq	8(%rsp), %r8
	movq	%rax, (%rdx)
	movl	%r14d, (%r8)
	movslq	0(%r13), %rcx
	jmp	.L1505
.L1513:
	leaq	.LC2(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	je	.L1687
	leaq	.LC5(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	jne	.L1523
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1688
	.p2align 4,,10
	.p2align 3
.L1524:
	testb	$32, 1(%rdx,%rcx,2)
	je	.L1527
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1527:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1524
	jmp	.L1512
.L1687:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1689
	.p2align 4,,10
	.p2align 3
.L1519:
	testb	$2, 1(%rdx,%rcx,2)
	je	.L1522
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1522:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1519
	jmp	.L1512
.L1523:
	leaq	.LC0(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	jne	.L1528
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1690
	.p2align 4,,10
	.p2align 3
.L1529:
	testb	$4, 1(%rdx,%rcx,2)
	je	.L1532
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1532:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1529
	jmp	.L1512
.L1686:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1510:
	testb	$8, (%rdx,%rax,2)
	je	.L1509
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r10
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r10
	andl	$24, %esi
	orq	%r10, (%rbx,%rsi)
.L1509:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1510
	jmp	.L1512
.L1528:
	leaq	.LC6(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	seta	%cl
	setb	%al
	subl	%eax, %ecx
	movsbl	%cl, %ecx
	testl	%ecx, %ecx
	jne	.L1533
	testq	%rbp, %rbp
	movl	$1, %edx
	je	.L1534
	jmp	.L1691
	.p2align 4,,10
	.p2align 3
.L1539:
	leal	-48(%rcx), %eax
	cmpl	$9, %eax
	jbe	.L1692
.L1534:
	addl	$1, %ecx
	cmpl	$256, %ecx
	jne	.L1539
	jmp	.L1512
.L1685:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1516:
	testb	$2, (%rdx,%rax,2)
	je	.L1515
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r11
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r11
	andl	$24, %esi
	orq	%r11, (%rbx,%rsi)
.L1515:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1516
	jmp	.L1512
.L1689:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1521:
	testb	$2, 1(%rdx,%rax,2)
	je	.L1520
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r14
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r14
	andl	$24, %esi
	orq	%r14, (%rbx,%rsi)
.L1520:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1521
	jmp	.L1512
.L1533:
	leaq	.LC7(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	jne	.L1540
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1693
	.p2align 4,,10
	.p2align 3
.L1541:
	testb	$64, 1(%rdx,%rcx,2)
	je	.L1544
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1544:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1541
	jmp	.L1512
.L1540:
	leaq	.LC1(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	jne	.L1545
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edi
	movq	%fs:(%rax), %rdx
	jne	.L1694
.L1546:
	testb	$1, 1(%rdx,%rcx,2)
	je	.L1549
	movl	%ecx, %eax
	movq	%rdi, %rsi
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, (%rbx,%rax,8)
.L1549:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1546
	jmp	.L1512
.L1570:
	movl	$12, %eax
	jmp	.L1503
.L1688:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1526:
	testb	$32, 1(%rdx,%rax,2)
	je	.L1525
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r9
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r9
	andl	$24, %esi
	orq	%r9, (%rbx,%rsi)
.L1525:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1526
	jmp	.L1512
.L1690:
	xorl	%eax, %eax
.L1531:
	testb	$4, 1(%rdx,%rax,2)
	je	.L1530
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r9
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r9
	andl	$24, %esi
	orq	%r9, (%rbx,%rsi)
.L1530:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1531
	jmp	.L1512
.L1545:
	leaq	.LC8(%rip), %rdi
	movl	$6, %ecx
	movq	%r12, %rsi
	repz cmpsb
	jne	.L1550
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %esi
	movq	%fs:(%rax), %rdx
	jne	.L1695
.L1551:
	testb	$1, (%rdx,%rcx,2)
	je	.L1554
	movl	%ecx, %eax
	movq	%rsi, %rdi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	orq	%rdi, (%rbx,%rax,8)
.L1554:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1551
	jmp	.L1512
.L1695:
	xorl	%eax, %eax
	movl	$1, %edi
.L1553:
	testb	$1, (%rdx,%rax,2)
	je	.L1552
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r9
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r9
	andl	$24, %esi
	orq	%r9, (%rbx,%rsi)
.L1552:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1553
	jmp	.L1512
.L1550:
	leaq	.LC9(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L1555
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edx
	movq	%fs:(%rax), %rsi
	jne	.L1696
.L1556:
	cmpw	$0, (%rsi,%rcx,2)
	jns	.L1559
	movl	%ecx, %eax
	movq	%rdx, %rdi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	orq	%rdi, (%rbx,%rax,8)
.L1559:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1556
	jmp	.L1512
.L1696:
	xorl	%eax, %eax
	movl	$1, %edi
.L1558:
	cmpw	$0, (%rsi,%rax,2)
	jns	.L1557
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r14
	movq	%rcx, %rdx
	andl	$63, %ecx
	shrq	$3, %rdx
	salq	%cl, %r14
	andl	$24, %edx
	orq	%r14, (%rbx,%rdx)
.L1557:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1558
	jmp	.L1512
.L1555:
	leaq	.LC10(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L1560
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edx
	movq	%fs:(%rax), %rsi
	jne	.L1697
.L1561:
	testb	$4, (%rsi,%rcx,2)
	je	.L1564
	movl	%ecx, %eax
	movq	%rdx, %rdi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	orq	%rdi, (%rbx,%rax,8)
.L1564:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1561
	jmp	.L1512
.L1697:
	xorl	%eax, %eax
	movl	$1, %edi
.L1563:
	testb	$4, (%rsi,%rax,2)
	je	.L1562
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r9
	movq	%rcx, %rdx
	andl	$63, %ecx
	shrq	$3, %rdx
	salq	%cl, %r9
	andl	$24, %edx
	orq	%r9, (%rbx,%rdx)
.L1562:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1563
	jmp	.L1512
.L1560:
	leaq	.LC11(%rip), %rsi
	movq	%r12, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L1581
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	movl	$1, %edx
	movq	%fs:(%rax), %rsi
	jne	.L1698
.L1565:
	testb	$16, 1(%rsi,%rcx,2)
	je	.L1568
	movl	%ecx, %eax
	movq	%rdx, %rdi
	sarl	$6, %eax
	salq	%cl, %rdi
	cltq
	orq	%rdi, (%rbx,%rax,8)
.L1568:
	addq	$1, %rcx
	cmpq	$256, %rcx
	jne	.L1565
	jmp	.L1512
.L1698:
	xorl	%eax, %eax
	movl	$1, %edi
.L1567:
	testb	$16, 1(%rsi,%rax,2)
	je	.L1566
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r14
	movq	%rcx, %rdx
	andl	$63, %ecx
	shrq	$3, %rdx
	salq	%cl, %r14
	andl	$24, %edx
	orq	%r14, (%rbx,%rdx)
.L1566:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1567
	jmp	.L1512
.L1581:
	movl	$4, %eax
	jmp	.L1503
.L1692:
	movq	%rdx, %rax
	salq	%cl, %rax
	orq	%rax, (%rbx)
	jmp	.L1534
.L1694:
	xorl	%eax, %eax
.L1548:
	testb	$1, 1(%rdx,%rax,2)
	je	.L1547
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r14
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r14
	andl	$24, %esi
	orq	%r14, (%rbx,%rsi)
.L1547:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1548
	jmp	.L1512
.L1693:
	xorl	%eax, %eax
.L1543:
	testb	$64, 1(%rdx,%rax,2)
	je	.L1542
	movzbl	0(%rbp,%rax), %ecx
	movq	%rdi, %r14
	movq	%rcx, %rsi
	andl	$63, %ecx
	shrq	$3, %rsi
	salq	%cl, %r14
	andl	$24, %esi
	orq	%r14, (%rbx,%rsi)
.L1542:
	addq	$1, %rax
	cmpq	$256, %rax
	jne	.L1543
	jmp	.L1512
.L1691:
	addq	$1, %rbp
	movl	$-47, %eax
	movl	$1, %esi
	jmp	.L1535
	.p2align 4,,10
	.p2align 3
.L1537:
	cmpl	$9, %eax
	jbe	.L1699
.L1535:
	addl	$1, %eax
	addq	$1, %rbp
	cmpl	$208, %eax
	jne	.L1537
	jmp	.L1512
.L1699:
	movzbl	0(%rbp), %ecx
	movq	%rsi, %rdi
	movq	%rcx, %rdx
	andl	$63, %ecx
	shrq	$3, %rdx
	salq	%cl, %rdi
	andl	$24, %edx
	orq	%rdi, (%rbx,%rdx)
	jmp	.L1535
	.size	build_charclass.isra.22, .-build_charclass.isra.22
	.p2align 4,,15
	.type	build_charclass_op, @function
build_charclass_op:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	$32, %edi
	movl	%r8d, %r12d
	subq	$72, %rsp
	movq	%rsi, (%rsp)
	movl	$1, %esi
	movq	%r9, 8(%rsp)
	movl	$0, 28(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	je	.L1724
	movl	$1, %esi
	movl	$72, %edi
	movq	%rax, %rbx
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L1725
	movzbl	48(%rax), %eax
	subq	$8, %rsp
	movl	%r12d, %edx
	pushq	$0
	andl	$1, %edx
	movq	16(%rsp), %rdi
	leaq	68(%r15), %rcx
	movq	%r14, %r9
	movq	%rbx, %rsi
	leaq	44(%rsp), %r8
	andl	$-2, %eax
	orl	%edx, %eax
	leaq	40(%r15), %rdx
	movb	%al, 48(%r15)
	call	build_charclass.isra.22
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L1704
	movsbl	0(%r13), %ecx
	movl	$1, %edx
	testb	%cl, %cl
	je	.L1706
	.p2align 4,,10
	.p2align 3
.L1705:
	leal	63(%rcx), %eax
	testl	%ecx, %ecx
	movl	%ecx, %esi
	movq	%rdx, %rdi
	cmovns	%ecx, %eax
	sarl	$31, %esi
	addq	$1, %r13
	shrl	$26, %esi
	sarl	$6, %eax
	addl	%esi, %ecx
	cltq
	andl	$63, %ecx
	subl	%esi, %ecx
	salq	%cl, %rdi
	movsbl	0(%r13), %ecx
	orq	%rdi, (%rbx,%rax,8)
	testb	%cl, %cl
	jne	.L1705
.L1706:
	testb	%r12b, %r12b
	je	.L1707
	leaq	32(%rbx), %rdx
	movq	%rbx, %rax
.L1708:
	notq	(%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L1708
.L1707:
	cmpl	$1, 164(%rbp)
	jle	.L1709
	movq	120(%rbp), %rcx
	xorl	%eax, %eax
.L1710:
	movq	(%rcx,%rax), %rdx
	andq	%rdx, (%rbx,%rax)
	addq	$8, %rax
	cmpq	$32, %rax
	jne	.L1710
.L1709:
	leaq	32(%rsp), %r9
	leaq	112(%rbp), %r12
	leaq	128(%rbp), %r13
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	$0, 40(%rsp)
	movq	%r9, %r8
	movq	%r13, %rsi
	movq	%r12, %rdi
	movq	%rbx, 32(%rsp)
	movb	$3, 40(%rsp)
	movq	%r9, (%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L1711
	cmpl	$1, 164(%rbp)
	jle	.L1712
	orb	$2, 160(%rbp)
	movq	(%rsp), %r9
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movb	$6, 40(%rsp)
	movq	%r15, 32(%rsp)
	movq	%r9, %r8
	call	create_token_tree.isra.12
	testq	%rax, %rax
	je	.L1711
	pxor	%xmm0, %xmm0
	leaq	48(%rsp), %r8
	movq	%r14, %rdx
	movq	%rax, %rcx
	movq	%r13, %rsi
	movq	%r12, %rdi
	movaps	%xmm0, 48(%rsp)
	movb	$10, 56(%rsp)
	call	create_token_tree.isra.12
	movq	%rax, %r14
.L1700:
	addq	$72, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1712:
	movq	%r15, %rdi
	call	free_charset
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1711:
	movq	%rbx, %rdi
	call	free@PLT
	movq	%r15, %rdi
	call	free_charset
.L1724:
	movq	8(%rsp), %rax
	xorl	%r14d, %r14d
	movl	$12, (%rax)
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1725:
	movq	%rbx, %rdi
	xorl	%r14d, %r14d
	call	free@PLT
	movq	8(%rsp), %rax
	movl	$12, (%rax)
	jmp	.L1700
	.p2align 4,,10
	.p2align 3
.L1704:
	movq	%rbx, %rdi
	movl	%eax, (%rsp)
	xorl	%r14d, %r14d
	call	free@PLT
	movq	%r15, %rdi
	call	free_charset
	movq	8(%rsp), %rbx
	movl	(%rsp), %eax
	movl	%eax, (%rbx)
	jmp	.L1700
	.size	build_charclass_op, .-build_charclass_op
	.p2align 4,,15
	.type	re_compile_fastmap_iter.isra.23, @function
re_compile_fastmap_iter.isra.23:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebx, %ebx
	subq	$344, %rsp
	movq	(%rdi), %r13
	cmpl	$1, 164(%r13)
	jne	.L1727
	movq	24(%rdi), %rbx
	shrq	$22, %rbx
	andl	$1, %ebx
.L1727:
	movl	(%rsi), %r8d
	testl	%r8d, %r8d
	jle	.L1726
	leaq	72(%rsp), %rax
	movq	%rcx, %r14
	movq	%rdx, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rdi, 16(%rsp)
	xorl	%r12d, %r12d
	movq	%rax, 24(%rsp)
	leaq	68(%rsp), %rax
	leaq	80(%rsp), %rbp
	movq	%r13, %r15
	movq	%rax, 48(%rsp)
	jmp	.L1766
	.p2align 4,,10
	.p2align 3
.L1729:
	cmpl	$3, %eax
	je	.L1808
	cmpl	$6, %eax
	je	.L1809
	andl	$-3, %ecx
	cmpb	$5, %cl
	je	.L1763
	cmpl	$2, %eax
	je	.L1810
.L1737:
	movq	8(%rsp), %rdi
	leal	1(%r12), %eax
	addq	$1, %r12
	cmpl	%eax, (%rdi)
	jle	.L1726
.L1766:
	movq	(%rsp), %rax
	movq	(%r15), %rdx
	movq	(%rax), %rax
	movslq	(%rax,%r12,4), %r11
	movq	%r11, %rsi
	salq	$4, %r11
	leaq	(%rdx,%r11), %rdi
	movzbl	8(%rdi), %eax
	cmpl	$1, %eax
	movl	%eax, %ecx
	jne	.L1729
	movzbl	(%rdi), %eax
	testl	%ebx, %ebx
	movb	$1, (%r14,%rax)
	je	.L1730
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movslq	(%rdx,%rax,4), %rax
	movb	$1, (%r14,%rax)
.L1730:
	movq	16(%rsp), %rax
	testb	$64, 26(%rax)
	je	.L1737
	cmpl	$1, 164(%r15)
	jle	.L1737
	movq	(%r15), %rdx
	movzbl	(%rdx,%r11), %eax
	movb	%al, 80(%rsp)
	leal	1(%rsi), %eax
	cltq
	cmpq	16(%r15), %rax
	jnb	.L1770
	leaq	16(%rdx,%r11), %rsi
	movl	8(%rsi), %edx
	andl	$2097407, %edx
	cmpl	$2097153, %edx
	jne	.L1770
	addq	$32, %r11
	leaq	1(%rbp), %rdx
	jmp	.L1733
	.p2align 4,,10
	.p2align 3
.L1811:
	movq	(%r15), %rsi
	addq	%r11, %rsi
	addq	$16, %r11
	movl	8(%rsi), %ecx
	andl	$2097407, %ecx
	cmpl	$2097153, %ecx
	jne	.L1732
.L1733:
	movzbl	(%rsi), %ecx
	addq	$1, %rdx
	movb	%cl, -1(%rdx)
	movq	%rdx, %rcx
	subq	%rbp, %rcx
	leaq	-1(%rax,%rcx), %rcx
	cmpq	%rcx, 16(%r15)
	ja	.L1811
.L1732:
	movq	24(%rsp), %rcx
	movq	48(%rsp), %rdi
	subq	%rbp, %rdx
	movq	%rdx, %r13
	movq	%rbp, %rsi
	movq	$0, (%rcx)
	call	__mbrtowc
	cmpq	%rax, %r13
	jne	.L1737
	movl	68(%rsp), %edi
	call	__towlower
	movq	24(%rsp), %rdx
	movl	%eax, %esi
	movq	%rbp, %rdi
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L1737
	movzbl	80(%rsp), %eax
	movb	$1, (%r14,%rax)
	jmp	.L1737
	.p2align 4,,10
	.p2align 3
.L1809:
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movq	(%rdi), %r13
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	64(%rax), %edx
	testl	%edx, %edx
	je	.L1745
	movl	56(%r13), %r11d
	testl	%r11d, %r11d
	jne	.L1746
	movl	64(%r13), %r10d
	testl	%r10d, %r10d
	jne	.L1746
.L1745:
	cmpl	$1, 164(%r15)
	jle	.L1754
	movl	68(%r13), %edi
	testl	%edi, %edi
	jne	.L1753
	testb	$1, 48(%r13)
	jne	.L1753
	movl	64(%r13), %esi
	testl	%esi, %esi
	jne	.L1753
	movl	60(%r13), %ecx
	testl	%ecx, %ecx
	jne	.L1753
.L1754:
	movl	52(%r13), %r8d
	xorl	%ecx, %ecx
	testl	%r8d, %r8d
	jle	.L1737
	movq	%r15, 40(%rsp)
	movq	%r12, 56(%rsp)
	movq	%rcx, %r15
	movl	%ebx, 36(%rsp)
	movq	24(%rsp), %r12
	jmp	.L1752
.L1761:
	leal	1(%r15), %eax
	addq	$1, %r15
	cmpl	52(%r13), %eax
	jge	.L1812
.L1752:
	movq	$0, (%r12)
	movq	0(%r13), %rax
	movq	%r12, %rdx
	movq	%rbp, %rdi
	leaq	0(,%r15,4), %rbx
	movl	(%rax,%r15,4), %esi
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L1758
	movzbl	80(%rsp), %eax
	movl	36(%rsp), %edx
	testl	%edx, %edx
	movb	$1, (%r14,%rax)
	je	.L1758
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movslq	(%rdx,%rax,4), %rax
	movb	$1, (%r14,%rax)
.L1758:
	movq	16(%rsp), %rax
	testb	$64, 26(%rax)
	je	.L1761
	movq	40(%rsp), %rax
	cmpl	$1, 164(%rax)
	jle	.L1761
	movq	0(%r13), %rax
	movl	(%rax,%rbx), %edi
	call	__towlower
	movq	%r12, %rdx
	movl	%eax, %esi
	movq	%rbp, %rdi
	call	__wcrtomb
	cmpq	$-1, %rax
	je	.L1761
	movzbl	80(%rsp), %eax
	movb	$1, (%r14,%rax)
	leal	1(%r15), %eax
	addq	$1, %r15
	cmpl	52(%r13), %eax
	jl	.L1752
.L1812:
	movq	40(%rsp), %r15
	movl	36(%rsp), %ebx
	movq	56(%rsp), %r12
	jmp	.L1737
.L1753:
	movq	24(%rsp), %r13
	movb	$0, 72(%rsp)
	.p2align 4,,10
	.p2align 3
.L1756:
	xorl	%edi, %edi
	movq	$0, 0(%rbp)
	movq	%rbp, %rcx
	movl	$1, %edx
	movq	%r13, %rsi
	call	__mbrtowc
	cmpq	$-2, %rax
	jne	.L1755
	movzbl	72(%rsp), %eax
	movb	$1, (%r14,%rax)
.L1755:
	addb	$1, 72(%rsp)
	jne	.L1756
	jmp	.L1737
.L1746:
	movq	80(%rax), %rcx
	xorl	%eax, %eax
	jmp	.L1750
	.p2align 4,,10
	.p2align 3
.L1748:
	addq	$1, %rax
	cmpq	$256, %rax
	je	.L1745
.L1750:
	movl	(%rcx,%rax,4), %r9d
	leaq	0(,%rax,4), %rdx
	testl	%r9d, %r9d
	jns	.L1748
	testl	%ebx, %ebx
	movb	$1, (%r14,%rax)
	je	.L1748
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rsi
	movq	%fs:(%rsi), %rsi
	movslq	(%rsi,%rdx), %rdx
	movb	$1, (%r14,%rdx)
	jmp	.L1748
.L1808:
	movq	%r14, %rcx
	xorl	%r10d, %r10d
	xorl	%r13d, %r13d
.L1743:
	movq	(%rdx,%r11), %rax
	movq	(%rax,%r13), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L1742:
	btq	%rax, %rdx
	jnc	.L1740
	testl	%ebx, %ebx
	movb	$1, (%rcx,%rax)
	je	.L1740
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rsi
	leaq	(%r10,%rax), %rdi
	movq	%fs:(%rsi), %rsi
	movslq	(%rsi,%rdi,4), %rsi
	movb	$1, (%r14,%rsi)
.L1740:
	addq	$1, %rax
	cmpq	$64, %rax
	jne	.L1742
	addq	$64, %r10
	addq	$8, %r13
	addq	$64, %rcx
	cmpq	$256, %r10
	je	.L1737
	movq	(%r15), %rdx
	jmp	.L1743
.L1810:
	movdqa	.LC12(%rip), %xmm0
	movups	%xmm0, (%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 128(%r14)
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 160(%r14)
	movups	%xmm0, 176(%r14)
	movups	%xmm0, 192(%r14)
	movups	%xmm0, 208(%r14)
	movups	%xmm0, 224(%r14)
	movups	%xmm0, 240(%r14)
.L1767:
	movq	16(%rsp), %rax
	orb	$1, 56(%rax)
.L1726:
	addq	$344, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L1770:
	leaq	1(%rbp), %rdx
	jmp	.L1732
.L1763:
	movdqa	.LC12(%rip), %xmm0
	cmpl	$2, %eax
	movups	%xmm0, (%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 128(%r14)
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 160(%r14)
	movups	%xmm0, 176(%r14)
	movups	%xmm0, 192(%r14)
	movups	%xmm0, 208(%r14)
	movups	%xmm0, 224(%r14)
	movups	%xmm0, 240(%r14)
	jne	.L1726
	jmp	.L1767
	.size	re_compile_fastmap_iter.isra.23, .-re_compile_fastmap_iter.isra.23
	.p2align 4,,15
	.type	peek_token, @function
peek_token:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movl	56(%rsi), %r14d
	cmpl	%r14d, 72(%rsi)
	jle	.L1930
	movzbl	10(%rdi), %r13d
	movq	%rdx, %r15
	movq	8(%rsi), %rdx
	movslq	%r14d, %rcx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movzbl	(%rdx,%rcx), %ebp
	andl	$-97, %r13d
	movb	%r13b, 10(%rdi)
	movl	104(%rsi), %eax
	movb	%bpl, (%rdi)
	cmpl	$1, %eax
	jle	.L1816
	cmpl	44(%rsi), %r14d
	je	.L1817
	movq	16(%rsi), %rsi
	leaq	(%rsi,%rcx,4), %rsi
	cmpl	$-1, (%rsi)
	je	.L1931
	cmpb	$92, %bpl
	je	.L1867
	movb	$1, 8(%rdi)
.L1866:
	movl	(%rsi), %esi
	movq	%rcx, 24(%rsp)
	movq	%rdx, 16(%rsp)
	movl	%esi, %edi
	movl	%esi, 12(%rsp)
	call	__iswalnum
	movl	12(%rsp), %esi
	testl	%eax, %eax
	setne	%al
	cmpl	$95, %esi
	sete	%sil
	orl	%esi, %eax
	sall	$6, %eax
	orl	%eax, %r13d
	movb	%r13b, 10(%rbx)
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rdx
	jmp	.L1869
	.p2align 4,,10
	.p2align 3
.L1930:
	movb	$2, 8(%rdi)
	xorl	%eax, %eax
.L1813:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1816:
	cmpb	$92, %bpl
	je	.L1867
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rsi
	movzbl	%bpl, %eax
	movb	$1, 8(%rbx)
	movq	%fs:(%rsi), %rsi
	movzwl	(%rsi,%rax,2), %eax
	shrw	$3, %ax
	andl	$1, %eax
	cmpb	$95, %bpl
	sete	%sil
	orl	%esi, %eax
	sall	$6, %eax
	orl	%eax, %r13d
	movb	%r13b, 10(%rbx)
.L1869:
	subl	$10, %ebp
	cmpb	$115, %bpl
	ja	.L1898
	leaq	.L1849(%rip), %rsi
	movzbl	%bpl, %ebp
	movslq	(%rsi,%rbp,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1849:
	.long	.L1848-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1850-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1851-.L1849
	.long	.L1852-.L1849
	.long	.L1853-.L1849
	.long	.L1854-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1855-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1856-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1857-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1858-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1898-.L1849
	.long	.L1859-.L1849
	.long	.L1860-.L1849
	.long	.L1861-.L1849
	.text
	.p2align 4,,10
	.p2align 3
.L1931:
	orl	$32, %r13d
	movb	$1, 8(%rdi)
	movl	$1, %eax
	movb	%r13b, 10(%rdi)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1867:
	leal	1(%r14), %ecx
	cmpl	64(%r12), %ecx
	jge	.L1932
	cmpb	$0, 99(%r12)
	jne	.L1821
	movslq	%ecx, %rsi
	movzbl	(%rdx,%rsi), %ebp
	movl	%ebp, %r14d
.L1822:
	cmpl	$1, %eax
	movb	%r14b, (%rbx)
	movb	$1, 8(%rbx)
	jle	.L1827
	movq	16(%r12), %rax
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rsi
.L1825:
	movl	(%rsi), %r12d
	movl	%r12d, %edi
	call	__iswalnum
	testl	%eax, %eax
	setne	%al
	cmpl	$95, %r12d
.L1923:
	sete	%dl
	subl	$39, %r14d
	orl	%edx, %eax
	sall	$6, %eax
	orl	%eax, %r13d
	cmpb	$86, %r14b
	movb	%r13b, 10(%rbx)
	ja	.L1871
	leaq	.L1830(%rip), %rdx
	movzbl	%r14b, %r14d
	movslq	(%rdx,%r14,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1830:
	.long	.L1829-.L1830
	.long	.L1831-.L1830
	.long	.L1832-.L1830
	.long	.L1871-.L1830
	.long	.L1833-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1834-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1835-.L1830
	.long	.L1871-.L1830
	.long	.L1836-.L1830
	.long	.L1837-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1838-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1839-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1840-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1841-.L1830
	.long	.L1871-.L1830
	.long	.L1842-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1843-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1844-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1871-.L1830
	.long	.L1845-.L1830
	.long	.L1846-.L1830
	.long	.L1847-.L1830
	.text
	.p2align 4,,10
	.p2align 3
.L1817:
	cmpb	$92, %bpl
	je	.L1867
	movq	16(%r12), %rax
	movb	$1, 8(%rbx)
	leaq	(%rax,%rcx,4), %rsi
	jmp	.L1866
	.p2align 4,,10
	.p2align 3
.L1932:
	movb	$36, 8(%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1861:
	movq	%r15, %rdx
	movl	$1, %eax
	andl	$4608, %edx
	cmpq	$4608, %rdx
	jne	.L1813
.L1929:
	movb	$24, 8(%rbx)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1860:
	movq	%r15, %rdx
	andl	$33792, %edx
	cmpq	$32768, %rdx
	je	.L1862
.L1898:
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1859:
	movq	%r15, %rdx
	movl	$1, %eax
	andl	$4608, %edx
	cmpq	$4608, %rdx
	jne	.L1813
.L1928:
	movb	$23, 8(%rbx)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1858:
	testl	$8388616, %r15d
	jne	.L1863
	testl	%r14d, %r14d
	jne	.L1933
.L1863:
	movb	$12, 8(%rbx)
	movl	$16, (%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1857:
	movb	$20, 8(%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1856:
	testl	$1026, %r15d
	movl	$1, %eax
	jne	.L1813
.L1927:
	movb	$19, 8(%rbx)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1855:
	movb	$5, 8(%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1850:
	testb	$8, %r15b
	jne	.L1864
	addl	$1, %r14d
	cmpl	64(%r12), %r14d
	jne	.L1934
.L1864:
	movb	$12, 8(%rbx)
	movl	$32, (%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1848:
	testl	$2048, %r15d
	movl	$1, %eax
	je	.L1813
.L1862:
	movb	$10, 8(%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1854:
	testl	$1026, %r15d
	movl	$1, %eax
	jne	.L1813
.L1926:
	movb	$18, 8(%rbx)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1853:
	movb	$11, 8(%rbx)
	movl	$1, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1852:
	testl	$8192, %r15d
	movl	$1, %eax
	je	.L1813
.L1925:
	movb	$9, 8(%rbx)
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1851:
	testl	$8192, %r15d
	movl	$1, %eax
	je	.L1813
.L1924:
	movb	$8, 8(%rbx)
	jmp	.L1813
.L1847:
	movq	%r15, %rdx
	movl	$2, %eax
	andl	$4608, %edx
	cmpq	$512, %rdx
	jne	.L1813
	jmp	.L1929
.L1846:
	testl	$33792, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$10, 8(%rbx)
	jmp	.L1813
.L1845:
	movq	%r15, %rdx
	movl	$2, %eax
	andl	$4608, %edx
	cmpq	$512, %rdx
	jne	.L1813
	jmp	.L1928
.L1844:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$32, 8(%rbx)
	jmp	.L1813
.L1843:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$34, 8(%rbx)
	jmp	.L1813
.L1842:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$12, 8(%rbx)
	movl	$256, (%rbx)
	jmp	.L1813
.L1841:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$12, 8(%rbx)
	movl	$64, (%rbx)
	jmp	.L1813
.L1840:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$33, 8(%rbx)
	jmp	.L1813
.L1839:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$35, 8(%rbx)
	jmp	.L1813
.L1838:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$12, 8(%rbx)
	movl	$512, (%rbx)
	jmp	.L1813
.L1837:
	movq	%r15, %rdx
	movl	$2, %eax
	andl	$1026, %edx
	cmpq	$2, %rdx
	jne	.L1813
	jmp	.L1927
.L1836:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$12, 8(%rbx)
	movl	$9, (%rbx)
	jmp	.L1813
.L1835:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$12, 8(%rbx)
	movl	$6, (%rbx)
	jmp	.L1813
.L1834:
	testl	$16384, %r15d
	movl	$2, %eax
	jne	.L1813
	subl	$49, %ebp
	movb	$4, 8(%rbx)
	movl	%ebp, (%rbx)
	jmp	.L1813
.L1833:
	movq	%r15, %rdx
	movl	$2, %eax
	andl	$1026, %edx
	cmpq	$2, %rdx
	jne	.L1813
	jmp	.L1926
.L1832:
	testl	$8192, %r15d
	movl	$2, %eax
	jne	.L1813
	jmp	.L1925
.L1831:
	testl	$8192, %r15d
	movl	$2, %eax
	jne	.L1813
	jmp	.L1924
.L1829:
	testl	$524288, %r15d
	movl	$2, %eax
	jne	.L1813
	movb	$12, 8(%rbx)
	movl	$128, (%rbx)
	jmp	.L1813
.L1871:
	movl	$2, %eax
	jmp	.L1813
	.p2align 4,,10
	.p2align 3
.L1827:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdx
	movzbl	%r14b, %eax
	movq	%fs:(%rdx), %rdx
	movzwl	(%rdx,%rax,2), %eax
	shrw	$3, %ax
	andl	$1, %eax
	cmpb	$95, %r14b
	jmp	.L1923
.L1821:
	cmpl	$1, %eax
	jle	.L1823
	movq	16(%r12), %r9
	movslq	%ecx, %rdi
	leaq	0(,%rdi,4), %r8
	leaq	(%r9,%r8), %rsi
	cmpl	$-1, (%rsi)
	je	.L1824
	addl	$2, %r14d
	cmpl	%r14d, 44(%r12)
	je	.L1823
	cmpl	$-1, 4(%r9,%r8)
	je	.L1824
.L1823:
	cmpb	$0, 100(%r12)
	movq	(%r12), %rdi
	movl	40(%r12), %esi
	je	.L1826
	movq	24(%r12), %r9
	movslq	%ecx, %r8
	addl	(%r9,%r8,4), %esi
	movslq	%esi, %rsi
	movzbl	(%rdi,%rsi), %ebp
	testb	$-128, %bpl
	movl	%ebp, %r14d
	je	.L1822
	movzbl	(%rdx,%r8), %ebp
	movl	%ebp, %r14d
	jmp	.L1822
.L1933:
	cmpb	$10, -1(%rdx,%rcx)
	jne	.L1898
	testl	$2048, %r15d
	je	.L1898
	jmp	.L1863
.L1826:
	leal	(%rcx,%rsi), %edx
	movslq	%edx, %rdx
	movzbl	(%rdi,%rdx), %ebp
	movl	%ebp, %r14d
	jmp	.L1822
.L1934:
	leaq	32(%rsp), %rdi
	movq	%r15, %rdx
	movl	%r14d, 56(%r12)
	movq	%r12, %rsi
	call	peek_token
	movzbl	40(%rsp), %eax
	subl	$1, 56(%r12)
	leal	-9(%rax), %edx
	movl	$1, %eax
	cmpb	$1, %dl
	ja	.L1813
	jmp	.L1864
.L1824:
	movzbl	(%rdx,%rdi), %ebp
	movb	$1, 8(%rbx)
	movl	%ebp, %r14d
	movb	%bpl, (%rbx)
	jmp	.L1825
	.size	peek_token, .-peek_token
	.p2align 4,,15
	.type	fetch_number, @function
fetch_number:
	pushq	%r14
	movl	$-2, %r14d
	pushq	%r13
	movq	%rdx, %r13
	pushq	%r12
	movl	$-1, %r12d
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	.p2align 4,,10
	.p2align 3
.L1936:
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	peek_token
	addl	%eax, 56(%rbp)
	movzbl	8(%rbx), %eax
	movzbl	(%rbx), %edx
	cmpb	$2, %al
	je	.L1940
	cmpb	$24, %al
	je	.L1935
	cmpb	$44, %dl
	je	.L1935
	cmpb	$1, %al
	je	.L1950
.L1942:
	movl	%r14d, %r12d
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1950:
	leal	-48(%rdx), %eax
	cmpb	$9, %al
	ja	.L1942
	cmpl	$-2, %r12d
	je	.L1942
	cmpl	$-1, %r12d
	je	.L1951
	leal	(%r12,%r12,4), %eax
	leal	(%rdx,%rax,2), %r12d
	movl	$32816, %eax
	cmpl	$32816, %r12d
	cmovg	%eax, %r12d
	subl	$48, %r12d
	jmp	.L1936
	.p2align 4,,10
	.p2align 3
.L1940:
	movl	$-2, %r12d
.L1935:
	popq	%rbx
	movl	%r12d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1951:
	leal	-48(%rdx), %r12d
	jmp	.L1936
	.size	fetch_number, .-fetch_number
	.p2align 4,,15
	.type	parse_bracket_element.constprop.29, @function
parse_bracket_element.constprop.29:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$16, %rsp
	cmpl	$1, 104(%rsi)
	movl	56(%rsi), %r11d
	je	.L1953
	movl	44(%rsi), %eax
	leal	1(%r11), %r10d
	cmpl	%r10d, %eax
	jle	.L1953
	movq	16(%rsi), %rbx
	movslq	%r10d, %r10
	leaq	0(,%r10,4), %rbp
	cmpl	$-1, (%rbx,%r10,4)
	jne	.L1953
	subl	$2, %eax
	leaq	-4(%rbx,%rbp), %r8
	subl	%r11d, %eax
	leaq	2(%rax), %rdx
	movl	$2, %eax
	jmp	.L1954
	.p2align 4,,10
	.p2align 3
.L1956:
	addq	$1, %rax
	cmpl	$-1, -4(%r8,%rax,4)
	jne	.L1955
.L1954:
	cmpq	%rax, %rdx
	leal	(%r11,%rax), %r9d
	jne	.L1956
.L1955:
	movl	(%r8), %eax
	movl	$1, (%rdi)
	movl	%eax, 8(%rdi)
	movl	%r9d, 56(%rsi)
.L1986:
	xorl	%eax, %eax
.L1952:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1953:
	movzbl	8(%rdx), %ebp
	addl	%r11d, %ecx
	movl	%ecx, 56(%rsi)
	movl	%ebp, %eax
	andl	$-5, %eax
	cmpb	$26, %al
	je	.L1979
	cmpb	$28, %bpl
	je	.L1979
	cmpb	$22, %bpl
	movq	%rdx, %r11
	movq	%rdi, %rbx
	jne	.L1976
	testb	%r9b, %r9b
	je	.L1987
.L1976:
	movzbl	(%r11), %eax
	movl	$0, (%rbx)
	movb	%al, 8(%rbx)
	addq	$16, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L1979:
	movl	72(%rsi), %r9d
	cmpl	%r9d, %ecx
	jge	.L1959
	movzbl	(%rdx), %r11d
	xorl	%r10d, %r10d
	jmp	.L1960
	.p2align 4,,10
	.p2align 3
.L1971:
	movq	8(%rdi), %rax
	movb	%r8b, (%rax,%r10)
	addq	$1, %r10
	cmpq	$32, %r10
	je	.L1959
	movzbl	8(%rdx), %ebp
	movl	56(%rsi), %ecx
	movl	72(%rsi), %r9d
.L1960:
	cmpb	$30, %bpl
	movl	%r10d, %ebx
	leal	1(%rcx), %eax
	je	.L1988
.L1962:
	movq	8(%rsi), %r8
	movslq	%ecx, %rcx
	movl	%eax, 56(%rsi)
	movzbl	(%r8,%rcx), %r8d
.L1964:
	cmpl	%r9d, %eax
	jge	.L1959
	cmpb	%r8b, %r11b
	jne	.L1971
	movq	8(%rsi), %r9
	movslq	%eax, %rcx
	cmpb	$93, (%r9,%rcx)
	jne	.L1971
	movq	8(%rdi), %rcx
	addl	$1, %eax
	movl	%eax, 56(%rsi)
	movslq	%ebx, %rax
	movb	$0, (%rcx,%rax)
	movzbl	8(%rdx), %eax
	cmpb	$28, %al
	je	.L1973
	cmpb	$30, %al
	je	.L1974
	cmpb	$26, %al
	jne	.L1986
	movl	$3, (%rdi)
	xorl	%eax, %eax
	jmp	.L1952
	.p2align 4,,10
	.p2align 3
.L1988:
	cmpb	$0, 99(%rsi)
	je	.L1962
	cmpb	$0, 100(%rsi)
	je	.L1965
	movl	44(%rsi), %r12d
	movslq	%ecx, %rbp
	leaq	0(,%rbp,4), %r13
	cmpl	%r12d, %ecx
	je	.L1966
	movq	16(%rsi), %r8
	cmpl	$-1, (%r8,%rbp,4)
	je	.L1985
.L1966:
	movq	24(%rsi), %r14
	movl	40(%rsi), %r8d
	addl	(%r14,%rbp,4), %r8d
	movq	(%rsi), %r14
	movslq	%r8d, %r8
	movzbl	(%r14,%r8), %r8d
	testb	$-128, %r8b
	jne	.L1985
	cmpl	$1, 104(%rsi)
	je	.L1968
	cmpl	%eax, %r12d
	jle	.L1968
	movq	16(%rsi), %rbp
	cmpl	$-1, 4(%rbp,%r13)
	jne	.L1968
	addl	$2, %ecx
	addq	%r13, %rbp
	jmp	.L1969
.L1970:
	movl	8(%rbp), %r13d
	addl	$1, %ecx
	addq	$4, %rbp
	cmpl	$-1, %r13d
	jne	.L1968
.L1969:
	cmpl	%r12d, %ecx
	movl	%ecx, %eax
	jne	.L1970
.L1968:
	movl	%eax, 56(%rsi)
	jmp	.L1964
	.p2align 4,,10
	.p2align 3
.L1959:
	addq	$16, %rsp
	movl	$7, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L1987:
	movq	%rsp, %rdi
	movq	%r8, %rdx
	call	peek_token_bracket
	cmpb	$21, 8(%rsp)
	movl	$11, %eax
	je	.L1976
	jmp	.L1952
.L1965:
	addl	40(%rsi), %ecx
	movq	(%rsi), %r8
	movl	%eax, 56(%rsi)
	movslq	%ecx, %rcx
	movzbl	(%r8,%rcx), %r8d
	jmp	.L1964
.L1985:
	movq	8(%rsi), %rcx
	movl	%eax, 56(%rsi)
	movzbl	(%rcx,%rbp), %r8d
	jmp	.L1964
.L1974:
	movl	$4, (%rdi)
	xorl	%eax, %eax
	jmp	.L1952
.L1973:
	movl	$2, (%rdi)
	xorl	%eax, %eax
	jmp	.L1952
	.size	parse_bracket_element.constprop.29, .-parse_bracket_element.constprop.29
	.section	.rodata.str1.1
.LC13:
	.string	"_"
.LC14:
	.string	""
	.text
	.p2align 4,,15
	.type	parse_expression, @function
parse_expression:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$344, %rsp
	movzbl	8(%rdx), %r10d
	movq	(%rsi), %rax
	movq	%rax, 16(%rsp)
	cmpb	$36, %r10b
	ja	.L1990
	movq	%rcx, 40(%rsp)
	leaq	.L1992(%rip), %rcx
	movq	%rdx, 24(%rsp)
	movzbl	%r10b, %edx
	movq	%r9, 64(%rsp)
	movq	%rsi, %rbp
	movslq	(%rcx,%rdx,4), %rax
	movq	%rdi, 56(%rsp)
	addq	%rcx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L1992:
	.long	.L1990-.L1992
	.long	.L1991-.L1992
	.long	.L2429-.L1992
	.long	.L1990-.L1992
	.long	.L1994-.L1992
	.long	.L1995-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1996-.L1992
	.long	.L1997-.L1992
	.long	.L2429-.L1992
	.long	.L1998-.L1992
	.long	.L1999-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1998-.L1992
	.long	.L1998-.L1992
	.long	.L2000-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L2001-.L1992
	.long	.L2002-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L1990-.L1992
	.long	.L2003-.L1992
	.long	.L2003-.L1992
	.long	.L2004-.L1992
	.long	.L2004-.L1992
	.long	.L2005-.L1992
	.text
.L1996:
	movq	48(%rsi), %rbx
	movq	40(%rsp), %r15
	movq	56(%rsp), %r14
	movq	24(%rsp), %r13
	movl	%r8d, (%rsp)
	leaq	1(%rbx), %rax
	movq	%r15, %rdx
	orq	$8388608, %rdx
	movq	%r13, %rdi
	movq	%rax, 48(%rsi)
	movq	%r14, %rsi
	call	peek_token
	addl	%eax, 56(%r14)
	xorl	%eax, %eax
	cmpb	$9, 8(%r13)
	movq	%r14, %rdi
	movq	%r13, %rdx
	je	.L2011
	movq	64(%rsp), %r14
	movl	(%rsp), %r8d
	movq	%r15, %rcx
	movq	%rbp, %rsi
	addl	$1, %r8d
	movq	%r14, %r9
	call	parse_reg_exp
	movl	(%r14), %r8d
	testl	%r8d, %r8d
	jne	.L2429
	movq	24(%rsp), %rsi
	cmpb	$9, 8(%rsi)
	je	.L2011
	testq	%rax, %rax
	je	.L2013
	leaq	free_tree(%rip), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	postorder
.L2013:
	movq	64(%rsp), %rax
	movl	$8, (%rax)
	.p2align 4,,10
	.p2align 3
.L2429:
	movq	$0, 48(%rsp)
.L1989:
	movq	48(%rsp), %rax
	addq	$344, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L2001:
	testq	$16777216, 40(%rsp)
	jne	.L2130
.L1998:
	movq	40(%rsp), %rax
	testb	$32, %al
	jne	.L2130
	testb	$16, %al
	jne	.L2432
	cmpb	$9, %r10b
	je	.L1997
.L2002:
	movq	16(%rsp), %rax
	movq	24(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	128(%rax), %rsi
	leaq	112(%rax), %rdi
	movb	$1, 8(%r8)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L2428
.L2008:
	movq	24(%rsp), %r15
	movq	56(%rsp), %rbx
	movq	40(%rsp), %rdx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	call	peek_token
	addl	%eax, 56(%rbx)
	movzbl	8(%r15), %eax
	leaq	304(%rsp), %r15
	.p2align 4,,10
	.p2align 3
.L2144:
	cmpb	$23, %al
	ja	.L1989
	cmpb	$18, %al
	movl	$9177088, %edx
	sete	%bl
	btq	%rax, %rdx
	jnc	.L1989
	movq	56(%rsp), %rcx
	cmpb	$23, %al
	movl	56(%rcx), %ebp
	movq	24(%rsp), %rcx
	movdqa	(%rcx), %xmm0
	je	.L2433
	movq	56(%rsp), %r14
	movq	40(%rsp), %rdx
	xorl	%r12d, %r12d
	movq	24(%rsp), %rdi
	cmpb	$19, %al
	movzbl	%bl, %ebx
	sete	%r12b
	movq	%r14, %rsi
	leal	-1(%r12,%r12), %r12d
	call	peek_token
	addl	%eax, 56(%r14)
	cmpq	$0, 48(%rsp)
	je	.L2164
.L2165:
	testl	%ebx, %ebx
	movq	48(%rsp), %rbp
	movq	$0, 72(%rsp)
	jg	.L2434
.L2168:
	cmpb	$17, 48(%rbp)
	je	.L2435
.L2173:
	cmpl	$-1, %r12d
	pxor	%xmm0, %xmm0
	sete	%al
	movq	%rbp, %rdx
	xorl	%ecx, %ecx
	addl	$10, %eax
	movq	%r15, %r8
	movaps	%xmm0, 304(%rsp)
	movb	%al, 312(%rsp)
	movq	16(%rsp), %rax
	leaq	112(%rax), %r13
	leaq	128(%rax), %r14
	movq	%r13, %rdi
	movq	%r14, %rsi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, %rdx
	je	.L2170
	addl	$2, %ebx
	cmpl	%r12d, %ebx
	jg	.L2175
	movl	%r12d, 32(%rsp)
	movq	%rax, %r12
.L2176:
	movq	16(%rsp), %rsi
	movq	%rbp, %rdi
	call	duplicate_tree
	pxor	%xmm0, %xmm0
	movq	%rax, %rbp
	movq	%r15, %r8
	movq	%rax, %rcx
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, 304(%rsp)
	movb	$16, 312(%rsp)
	movaps	%xmm0, (%rsp)
	call	create_token_tree.isra.12
	testq	%rbp, %rbp
	je	.L2170
	testq	%rax, %rax
	je	.L2170
	movdqa	(%rsp), %xmm0
	xorl	%ecx, %ecx
	movq	%r15, %r8
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, 304(%rsp)
	movb	$10, 312(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L2170
	addl	$1, %ebx
	cmpl	32(%rsp), %ebx
	jle	.L2176
	movq	%rax, %rdx
.L2175:
	movq	72(%rsp), %rax
	testq	%rax, %rax
	je	.L2172
	pxor	%xmm0, %xmm0
	movq	%rdx, %rcx
	movq	%r15, %r8
	movq	%rax, %rdx
	movq	%r14, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, 304(%rsp)
	movb	$16, 312(%rsp)
	call	create_token_tree.isra.12
.L2158:
	movq	64(%rsp), %rsi
	movl	(%rsi), %edx
	testl	%edx, %edx
	je	.L2217
	testq	%rax, %rax
	je	.L2148
.L2217:
	movq	%rax, 48(%rsp)
.L2177:
	testq	$16777216, 40(%rsp)
	movq	24(%rsp), %rax
	movzbl	8(%rax), %eax
	je	.L2144
	cmpb	$11, %al
	je	.L2222
	cmpb	$23, %al
	jne	.L2144
.L2222:
	movq	48(%rsp), %rax
	testq	%rax, %rax
	je	.L2130
	leaq	free_tree(%rip), %rsi
	xorl	%edx, %edx
	movq	%rax, %rdi
	call	postorder
.L2130:
	movq	64(%rsp), %rax
	movq	$0, 48(%rsp)
	movl	$13, (%rax)
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2434:
	cmpl	$1, %ebx
	je	.L2214
	movq	16(%rsp), %rax
	movq	%rbp, %r14
	movl	%r12d, 32(%rsp)
	movl	$2, %ebp
	movl	%ebx, %r12d
	movq	%r14, %rbx
	leaq	112(%rax), %rsi
	leaq	128(%rax), %r13
	movq	%rsi, (%rsp)
	.p2align 4,,10
	.p2align 3
.L2171:
	movq	16(%rsp), %rsi
	movq	%r14, %rdi
	call	duplicate_tree
	pxor	%xmm0, %xmm0
	movq	(%rsp), %rdi
	movq	%rax, %r14
	movq	%rbx, %rdx
	movq	%r15, %r8
	movq	%rax, %rcx
	movq	%r13, %rsi
	movaps	%xmm0, 304(%rsp)
	movb	$16, 312(%rsp)
	call	create_token_tree.isra.12
	testq	%r14, %r14
	movq	%rax, %rbx
	je	.L2170
	testq	%rax, %rax
	je	.L2170
	addl	$1, %ebp
	cmpl	%r12d, %ebp
	jle	.L2171
	movl	%r12d, %ebx
	movl	32(%rsp), %r12d
	movq	%rax, 72(%rsp)
.L2169:
	cmpl	%r12d, %ebx
	je	.L2215
	movq	16(%rsp), %rsi
	movq	%r14, %rdi
	call	duplicate_tree
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.L2168
	.p2align 4,,10
	.p2align 3
.L2170:
	movq	64(%rsp), %rax
	movl	$12, (%rax)
.L2167:
	movq	48(%rsp), %rdi
	leaq	free_tree(%rip), %rsi
	xorl	%edx, %edx
	call	postorder
	movq	$0, 48(%rsp)
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2433:
	movq	40(%rsp), %rdx
	movq	56(%rsp), %rdi
	movq	%rcx, %rsi
	movaps	%xmm0, (%rsp)
	call	fetch_number
	cmpl	$-1, %eax
	movl	%eax, %ebx
	movdqa	(%rsp), %xmm0
	je	.L2436
	cmpl	$-2, %eax
	je	.L2155
	movq	24(%rsp), %rax
	movzbl	8(%rax), %eax
	cmpb	$24, %al
	je	.L2212
	cmpb	$1, %al
	je	.L2437
.L2155:
	testq	$2097152, 40(%rsp)
	je	.L2438
.L2153:
	movq	56(%rsp), %rax
	movl	%ebp, 56(%rax)
	movq	24(%rsp), %rax
	movaps	%xmm0, (%rax)
	movb	$1, 8(%rax)
	movq	48(%rsp), %rax
	jmp	.L2158
	.p2align 4,,10
	.p2align 3
.L2435:
	movslq	40(%rbp), %rdx
	leaq	mark_opt_subexp(%rip), %rsi
	movq	%rbp, %rdi
	call	postorder
	jmp	.L2173
	.p2align 4,,10
	.p2align 3
.L2164:
	movq	64(%rsp), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L2429
.L2166:
	movq	$0, 48(%rsp)
	jmp	.L2177
.L2215:
	movq	72(%rsp), %rdx
	.p2align 4,,10
	.p2align 3
.L2172:
	movq	%rdx, 48(%rsp)
	jmp	.L2177
	.p2align 4,,10
	.p2align 3
.L2436:
	movq	24(%rsp), %rax
	cmpb	$1, 8(%rax)
	jne	.L2150
	movq	24(%rsp), %rax
	cmpb	$44, (%rax)
	jne	.L2150
	xorl	%ebx, %ebx
.L2149:
	movq	40(%rsp), %rdx
	movq	24(%rsp), %rsi
	movq	56(%rsp), %rdi
	movaps	%xmm0, (%rsp)
	call	fetch_number
	cmpl	$-2, %eax
	movl	%eax, %r12d
	movdqa	(%rsp), %xmm0
	je	.L2155
	cmpl	$-1, %eax
	je	.L2221
	cmpl	%eax, %ebx
	jg	.L2150
.L2221:
	movq	24(%rsp), %rax
	cmpb	$24, 8(%rax)
	jne	.L2150
	cmpl	$-1, %r12d
	jne	.L2154
	xorl	%eax, %eax
	cmpl	$32767, %ebx
	setg	%al
	testq	%rax, %rax
	jne	.L2439
.L2161:
	movq	56(%rsp), %r14
	movq	24(%rsp), %rdi
	movq	40(%rsp), %rdx
	movq	%r14, %rsi
	call	peek_token
	movq	48(%rsp), %rdi
	addl	%eax, 56(%r14)
	testq	%rdi, %rdi
	je	.L2164
	movl	%ebx, %eax
	orl	%r12d, %eax
	jne	.L2165
	leaq	free_tree(%rip), %rsi
	xorl	%edx, %edx
	call	postorder
	movq	64(%rsp), %rax
	movl	(%rax), %esi
	testl	%esi, %esi
	je	.L2166
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L2212:
	movl	%ebx, %r12d
.L2154:
	xorl	%eax, %eax
	cmpl	$32767, %r12d
	setg	%al
	testq	%rax, %rax
	je	.L2161
.L2439:
	movq	64(%rsp), %rax
	movl	$15, (%rax)
	jmp	.L2148
.L2437:
	movq	24(%rsp), %rax
	cmpb	$44, (%rax)
	je	.L2149
	testq	$2097152, 40(%rsp)
	jne	.L2153
	.p2align 4,,10
	.p2align 3
.L2150:
	movq	64(%rsp), %rax
	movl	$10, (%rax)
.L2148:
	cmpq	$0, 48(%rsp)
	je	.L2429
	jmp	.L2167
	.p2align 4,,10
	.p2align 3
.L1999:
	movq	24(%rsp), %rax
	movl	(%rax), %r10d
	testl	$783, %r10d
	je	.L2132
	movq	16(%rsp), %rax
	movzbl	160(%rax), %eax
	testb	$16, %al
	jne	.L2132
	movq	16(%rsp), %rsi
	orl	$16, %eax
	testb	$8, %al
	movb	%al, 160(%rsi)
	jne	.L2209
	movabsq	$287948901175001088, %rcx
	testb	$4, %al
	movq	%rcx, 168(%rsi)
	movabsq	$576460745995190270, %rcx
	movq	%rcx, 176(%rsi)
	je	.L2210
	pxor	%xmm0, %xmm0
	movq	24(%rsp), %rax
	movups	%xmm0, 184(%rsi)
	movl	(%rax), %r10d
.L2132:
	movq	16(%rsp), %rax
	leaq	112(%rax), %rbx
	leaq	128(%rax), %r12
	leal	-256(%r10), %eax
	testl	$-257, %eax
	jne	.L2138
	cmpl	$256, %r10d
	movq	24(%rsp), %r15
	je	.L2440
	movl	$5, (%r15)
	movq	%r15, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	create_token_tree.isra.12
	movl	$10, (%r15)
	movq	%rax, %rbp
.L2140:
	movq	24(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	create_token_tree.isra.12
	pxor	%xmm0, %xmm0
	leaq	304(%rsp), %r8
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%rax, %rcx
	movq	%rbx, %rdi
	movq	%rax, %r13
	movaps	%xmm0, 304(%rsp)
	movb	$10, 312(%rsp)
	call	create_token_tree.isra.12
	testq	%rbp, %rbp
	movq	%rax, %rsi
	sete	%dl
	testq	%r13, %r13
	movq	%rax, 48(%rsp)
	sete	%al
	orb	%al, %dl
	jne	.L2428
	testq	%rsi, %rsi
	je	.L2428
.L2141:
	movq	56(%rsp), %rbx
	movq	40(%rsp), %rdx
	movq	24(%rsp), %rdi
	movq	%rbx, %rsi
	call	peek_token
	addl	%eax, 56(%rbx)
	jmp	.L1989
.L1994:
	movq	24(%rsp), %rax
	movq	16(%rsp), %rsi
	movl	(%rax), %ecx
	movl	$1, %eax
	sall	%cl, %eax
	cltq
	testq	%rax, 152(%rsi)
	je	.L2441
	movq	16(%rsp), %rbx
	movq	24(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	orq	%rax, 144(%rbx)
	leaq	128(%rbx), %rsi
	leaq	112(%rbx), %rdi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L2428
	addl	$1, 140(%rbx)
	orb	$2, 160(%rbx)
	jmp	.L2008
.L2004:
	movq	56(%rsp), %rax
	movq	64(%rsp), %rbx
	cmpb	$35, %r10b
	sete	%r8b
	leaq	.LC14(%rip), %rcx
	leaq	.LC5(%rip), %rdx
	movzbl	%r8b, %r8d
	movq	80(%rax), %rsi
	movq	%rbx, %r9
.L2431:
	movq	16(%rsp), %rdi
	call	build_charclass_op
	movl	(%rbx), %edi
	movq	%rax, 48(%rsp)
	testl	%edi, %edi
	je	.L2008
	testq	%rax, %rax
	jne	.L2008
	jmp	.L2429
	.p2align 4,,10
	.p2align 3
.L2005:
	movq	64(%rsp), %rax
	movq	$0, 48(%rsp)
	movl	$5, (%rax)
	jmp	.L1989
.L1991:
	movq	16(%rsp), %rax
	movq	24(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	112(%rax), %rbx
	leaq	128(%rax), %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L2428
	movq	16(%rsp), %rax
	leaq	304(%rsp), %r12
	cmpl	$1, 164(%rax)
	jle	.L2008
	movq	48(%rsp), %r15
	movq	56(%rsp), %r13
.L2007:
	movslq	56(%r13), %rax
	cmpl	%eax, 72(%r13)
	jle	.L2407
	cmpl	44(%r13), %eax
	je	.L2407
	movq	16(%r13), %rdx
	cmpl	$-1, (%rdx,%rax,4)
	jne	.L2407
	movq	24(%rsp), %r14
	movq	40(%rsp), %rdx
	movq	%r13, %rsi
	movq	%r14, %rdi
	call	peek_token
	addl	%eax, 56(%r13)
	movq	%r14, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	create_token_tree.isra.12
	pxor	%xmm0, %xmm0
	movq	%r15, %rdx
	movq	%rax, %r14
	movq	%r12, %r8
	movq	%rax, %rcx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movaps	%xmm0, 304(%rsp)
	movb	$16, 312(%rsp)
	call	create_token_tree.isra.12
	testq	%r14, %r14
	movq	%rax, %r15
	sete	%dl
	testq	%rax, %rax
	sete	%al
	orb	%al, %dl
	je	.L2007
.L2428:
	movq	64(%rsp), %rax
	movl	$12, (%rax)
	jmp	.L2429
.L2003:
	movq	56(%rsp), %rax
	movq	64(%rsp), %rbx
	cmpb	$33, %r10b
	sete	%r8b
	leaq	.LC13(%rip), %rcx
	leaq	.LC3(%rip), %rdx
	movzbl	%r8b, %r8d
	movq	80(%rax), %rsi
	movq	%rbx, %r9
	jmp	.L2431
.L2000:
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movl	$0, 220(%rsp)
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	64(%rax), %r15d
	movq	192(%rax), %rsi
	testl	%r15d, %r15d
	movq	%rsi, 48(%rsp)
	jne	.L2442
.L2015:
	movl	$1, %esi
	movl	$32, %edi
	call	calloc@PLT
	movl	$1, %esi
	movq	%rax, %r14
	movl	$72, %edi
	call	calloc@PLT
	testq	%r14, %r14
	movq	%rax, %r13
	sete	%dl
	testq	%rax, %rax
	sete	%al
	orb	%al, %dl
	jne	.L2443
	movq	24(%rsp), %rbx
	movq	40(%rsp), %rdx
	movq	56(%rsp), %rsi
	movq	%rbx, %rdi
	call	peek_token_bracket
	movl	%eax, (%rsp)
	movzbl	8(%rbx), %eax
	cmpb	$2, %al
	je	.L2021
	cmpb	$25, %al
	movb	$0, 167(%rsp)
	je	.L2444
.L2019:
	cmpb	$21, %al
	je	.L2445
.L2022:
	movl	136(%rsp), %eax
	movq	120(%rsp), %rsi
	movl	$1, %r9d
	movq	56(%rsp), %rbx
	movq	24(%rsp), %rbp
	movl	$0, 160(%rsp)
	movl	$0, 140(%rsp)
	subl	$1, %eax
	movl	$0, 148(%rsp)
	movl	$0, 144(%rsp)
	leaq	8(%rsi,%rax,8), %rax
	movq	%rax, 104(%rsp)
	leaq	272(%rsp), %rax
	movq	%rax, 88(%rsp)
	leaq	224(%rsp), %rax
	movq	%rax, 112(%rsp)
.L2115:
	movq	88(%rsp), %rax
	movq	40(%rsp), %r8
	andl	$1, %r9d
	movl	(%rsp), %ecx
	movq	112(%rsp), %rdi
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movl	$3, 224(%rsp)
	movq	%rax, 232(%rsp)
	call	parse_bracket_element.constprop.29
	testl	%eax, %eax
	jne	.L2029
	movq	40(%rsp), %rdx
	movq	%rbp, %rdi
	call	peek_token_bracket
	movl	%eax, (%rsp)
	movl	224(%rsp), %eax
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L2025
	movzbl	8(%rbp), %edx
	cmpb	$2, %dl
	je	.L2425
	cmpb	$22, %dl
	je	.L2446
.L2025:
	cmpl	$4, %eax
	ja	.L1990
	leaq	.L2071(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L2071:
	.long	.L2070-.L2071
	.long	.L2072-.L2071
	.long	.L2073-.L2071
	.long	.L2074-.L2071
	.long	.L2075-.L2071
	.text
.L1995:
	movq	16(%rsp), %rbx
	movq	24(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	leaq	128(%rbx), %rsi
	leaq	112(%rbx), %rdi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L2428
	cmpl	$1, 164(%rbx)
	jle	.L2008
	orb	$2, 160(%rbx)
	jmp	.L2008
.L1997:
	testq	$131072, 40(%rsp)
	jne	.L2002
	movq	64(%rsp), %rax
	movq	$0, 48(%rsp)
	movl	$16, (%rax)
	jmp	.L1989
	.p2align 4,,10
	.p2align 3
.L2011:
	cmpq	$8, %rbx
	jbe	.L2447
.L2014:
	movq	16(%rsp), %rcx
	pxor	%xmm0, %xmm0
	leaq	304(%rsp), %r8
	movq	%rax, %rdx
	leaq	128(%rcx), %rsi
	leaq	112(%rcx), %rdi
	xorl	%ecx, %ecx
	movaps	%xmm0, 304(%rsp)
	movb	$17, 312(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L2428
	movl	%ebx, 40(%rax)
	jmp	.L2008
.L2214:
	movq	48(%rsp), %r14
	movq	%r14, 72(%rsp)
	jmp	.L2169
.L2442:
	movq	200(%rax), %rsi
	movq	%rsi, 72(%rsp)
	movl	168(%rax), %esi
	movl	%esi, 136(%rsp)
	movq	176(%rax), %rsi
	movq	184(%rax), %rax
	movq	%rsi, 120(%rsp)
	movq	%rax, 80(%rsp)
	jmp	.L2015
.L2447:
	movq	16(%rsp), %rsi
	movl	$1, %edx
	movl	%ebx, %ecx
	sall	%cl, %edx
	movslq	%edx, %rdx
	orq	%rdx, 152(%rsi)
	jmp	.L2014
.L2074:
	movq	232(%rsp), %r12
	movq	%r12, %rdi
	call	strlen
	testl	%r15d, %r15d
	movq	%rax, %r8
	jne	.L2448
	cmpq	$1, %rax
	jne	.L2097
.L2424:
	movzbl	(%r12), %edx
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$3, %rax
	movl	%edx, %ecx
	andl	$24, %eax
	salq	%cl, %r8
	orq	%r8, (%r14,%rax)
	movq	64(%rsp), %rax
	movl	$0, (%rax)
.L2069:
	movzbl	8(%rbp), %eax
	cmpb	$2, %al
	je	.L2425
	cmpb	$21, %al
	je	.L2114
	xorl	%r9d, %r9d
	jmp	.L2115
.L2073:
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movq	232(%rsp), %rcx
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	movl	64(%rax), %edx
	movq	%rax, 176(%rsp)
	testl	%edx, %edx
	je	.L2078
	movq	80(%rax), %r8
	movq	96(%rax), %rsi
	movq	104(%rax), %rax
	movq	%rsi, 32(%rsp)
	movq	%rax, 168(%rsp)
	leaq	1(%rcx), %rax
	movq	%rax, 152(%rsp)
	movzbl	(%rcx), %eax
	movslq	(%r8,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %r12
	js	.L2449
.L2079:
	testl	%r12d, %r12d
	je	.L2097
	movq	152(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L2097
	movq	176(%rsp), %rax
	movl	%r12d, %r10d
	xorl	%ecx, %ecx
	sarl	$24, %r10d
	movl	%r15d, 176(%rsp)
	movq	%r14, 152(%rsp)
	movq	%r13, 184(%rsp)
	movq	%rbx, 192(%rsp)
	movq	%rcx, %r14
	movq	88(%rax), %r9
	movl	%r12d, %eax
	movq	168(%rsp), %r13
	andl	$16777215, %eax
	movl	%r12d, 168(%rsp)
	movq	%rbp, 200(%rsp)
	movl	%r10d, %r12d
	movq	%r8, %rbx
	movzbl	(%r9,%rax), %esi
	leaq	1(%r9,%rax), %rax
	movq	%r9, %rbp
	movq	%rax, 128(%rsp)
	movq	%rsi, 96(%rsp)
	movl	%esi, %r15d
	jmp	.L2104
	.p2align 4,,10
	.p2align 3
.L2103:
	addq	$1, %r14
	cmpq	$256, %r14
	je	.L2450
.L2104:
	movslq	(%rbx,%r14,4), %rdx
	testq	%rdx, %rdx
	movq	%rdx, %rax
	js	.L2451
.L2098:
	testl	%eax, %eax
	je	.L2103
	movl	%eax, %edx
	andl	$16777215, %edx
	cmpb	0(%rbp,%rdx), %r15b
	jne	.L2103
	sarl	$24, %eax
	cmpl	%eax, %r12d
	jne	.L2103
	leaq	1(%rbp,%rdx), %rsi
	movq	128(%rsp), %rdi
	movq	96(%rsp), %rdx
	call	memcmp@PLT
	testl	%eax, %eax
	jne	.L2103
	movl	%r14d, %ecx
	movl	$1, %edx
	movl	%r14d, %eax
	salq	%cl, %rdx
	movq	152(%rsp), %rcx
	sarl	$6, %eax
	cltq
	orq	%rdx, (%rcx,%rax,8)
	jmp	.L2103
.L2072:
	movslq	52(%r13), %rdx
	movq	0(%r13), %rax
	cmpl	144(%rsp), %edx
	je	.L2452
.L2076:
	leal	1(%rdx), %ecx
	movl	%ecx, 52(%r13)
	movl	232(%rsp), %ecx
	movl	%ecx, (%rax,%rdx,4)
	jmp	.L2069
.L2070:
	movzbl	232(%rsp), %edx
	movl	$1, %esi
	movq	%rdx, %rax
	andl	$63, %edx
	shrq	$3, %rax
	movl	%edx, %ecx
	andl	$24, %eax
	salq	%cl, %rsi
	orq	%rsi, (%r14,%rax)
	jmp	.L2069
.L2075:
	subq	$8, %rsp
	movq	80(%rbx), %rdi
	leaq	68(%r13), %rcx
	pushq	48(%rsp)
	movq	248(%rsp), %r9
	leaq	40(%r13), %rdx
	movq	%r14, %rsi
	leaq	236(%rsp), %r8
	call	build_charclass.isra.22
	movq	80(%rsp), %rcx
	testl	%eax, %eax
	movl	%eax, (%rcx)
	popq	%r10
	popq	%r11
	je	.L2069
.L2018:
	movq	%r14, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free_charset
	movq	64(%rsp), %rax
	movl	(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L2429
	movq	$0, 48(%rsp)
	jmp	.L2008
.L2445:
	movq	24(%rsp), %rax
	movb	$1, 8(%rax)
	jmp	.L2022
.L2444:
	orb	$1, 48(%r13)
	testq	$256, 40(%rsp)
	je	.L2020
	orq	$1024, (%r14)
.L2020:
	movq	56(%rsp), %rax
	movl	(%rsp), %esi
	movq	24(%rsp), %rbx
	movq	40(%rsp), %rdx
	addl	%esi, 56(%rax)
	movq	%rax, %rsi
	movq	%rbx, %rdi
	call	peek_token_bracket
	movl	%eax, (%rsp)
	movzbl	8(%rbx), %eax
	cmpb	$2, %al
	je	.L2021
	movb	$1, 167(%rsp)
	jmp	.L2019
.L2440:
	movl	$6, (%r15)
	movq	%r15, %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	create_token_tree.isra.12
	movl	$9, (%r15)
	movq	%rax, %rbp
	jmp	.L2140
	.p2align 4,,10
	.p2align 3
.L2451:
	movq	32(%rsp), %rsi
	subq	%rdx, %rsi
.L2099:
	movslq	(%rsi), %rax
	leaq	5(%rsi), %rcx
	movzbl	4(%rsi), %edx
	testl	%eax, %eax
	js	.L2100
.L2453:
	testq	%rdx, %rdx
	je	.L2098
	leaq	(%rcx,%rdx), %rsi
	addq	$1, %rdx
	andl	$3, %edx
	je	.L2099
	movl	$4, %eax
	subq	%rdx, %rax
	addq	%rax, %rsi
	movslq	(%rsi), %rax
	leaq	5(%rsi), %rcx
	movzbl	4(%rsi), %edx
	testl	%eax, %eax
	jns	.L2453
.L2100:
	testq	%rdx, %rdx
	jne	.L2454
	negq	%rax
	movl	0(%r13,%rax,4), %eax
	jmp	.L2098
.L2454:
	addq	%rdx, %rdx
	leaq	4(%rdx), %rsi
	addq	$1, %rdx
	andl	$3, %edx
	subq	%rdx, %rsi
	addq	%rcx, %rsi
	jmp	.L2099
.L2425:
	movq	64(%rsp), %rax
	movl	$7, (%rax)
	jmp	.L2018
.L2138:
	movq	24(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L2141
	jmp	.L2428
.L2407:
	movq	%r15, 48(%rsp)
	jmp	.L2008
.L2029:
	movq	64(%rsp), %rsi
	movl	%eax, (%rsi)
	jmp	.L2018
.L2432:
	movq	56(%rsp), %rbx
	movq	24(%rsp), %r15
	movq	%rax, %rdx
	movl	%r8d, (%rsp)
	movq	%rax, %r14
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	peek_token
	addl	%eax, 56(%rbx)
	movq	64(%rsp), %r9
	movq	%rbx, %rdi
	movl	(%rsp), %r8d
	movq	%r14, %rcx
	movq	%r15, %rdx
	movq	%rbp, %rsi
	call	parse_expression
	movq	%rax, 48(%rsp)
	jmp	.L1989
.L2446:
	movl	(%rsp), %eax
	movq	40(%rsp), %r12
	leaq	256(%rsp), %r11
	addl	%eax, 56(%rbx)
	movq	%r11, %rdi
	movq	%r12, %rdx
	call	peek_token_bracket
	movzbl	264(%rsp), %edx
	cmpb	$2, %dl
	je	.L2425
	cmpb	$21, %dl
	je	.L2028
	leaq	304(%rsp), %rdx
	leaq	240(%rsp), %rdi
	movl	$1, %r9d
	movq	%r12, %r8
	movl	%eax, %ecx
	movl	$3, 240(%rsp)
	movq	%rdx, 248(%rsp)
	movq	%r11, %rdx
	call	parse_bracket_element.constprop.29
	testl	%eax, %eax
	jne	.L2029
	movq	40(%rsp), %rdx
	movq	%rbp, %rdi
	call	peek_token_bracket
	movl	%eax, (%rsp)
	movl	224(%rsp), %eax
	leal	-2(%rax), %edx
	andl	$-3, %edx
	je	.L2032
	movl	240(%rsp), %r8d
	leal	-2(%r8), %edx
	andl	$-3, %edx
	je	.L2032
	testl	%eax, %eax
	jne	.L2033
	testl	%r15d, %r15d
	movzbl	232(%rsp), %eax
	jne	.L2034
	movq	48(%rsp), %rcx
	movzbl	(%rcx,%rax), %r12d
.L2035:
	testl	%r8d, %r8d
	jne	.L2046
.L2183:
	movzbl	248(%rsp), %eax
	movq	48(%rsp), %rcx
	movzbl	(%rcx,%rax), %edx
.L2048:
	cmpl	$-1, %r12d
	je	.L2097
	cmpl	$-1, %edx
	je	.L2097
	testq	$65536, 40(%rsp)
	je	.L2219
	cmpl	%r12d, %edx
	jb	.L2032
.L2219:
	testl	%r15d, %r15d
	jne	.L2060
	movq	16(%rsp), %rax
	cmpl	$1, 164(%rax)
	jle	.L2061
.L2060:
	movslq	64(%r13), %rax
	movq	24(%r13), %rcx
	cmpl	140(%rsp), %eax
	je	.L2455
.L2062:
	movl	%r12d, (%rcx,%rax,4)
	movslq	64(%r13), %rax
	movq	32(%r13), %rcx
	leal	1(%rax), %esi
	movl	%esi, 64(%r13)
	movl	%edx, (%rcx,%rax,4)
.L2061:
	movl	%r12d, %eax
	xorl	%ecx, %ecx
	movq	%r13, 32(%rsp)
	movl	%r15d, %r12d
	movq	%r14, %r13
	movq	%rbx, 96(%rsp)
	movq	%rbp, 128(%rsp)
	movl	%edx, %r14d
	movq	%rcx, %rbx
	movq	48(%rsp), %rbp
	movl	%eax, %r15d
	jmp	.L2068
	.p2align 4,,10
	.p2align 3
.L2457:
	movzbl	0(%rbp,%rbx), %eax
.L2066:
	cmpl	%r15d, %eax
	jb	.L2067
	cmpl	%eax, %r14d
	jb	.L2067
	movl	%ebx, %eax
	movl	$1, %esi
	movl	%ebx, %ecx
	sarl	$6, %eax
	salq	%cl, %rsi
	cltq
	orq	%rsi, 0(%r13,%rax,8)
.L2067:
	addq	$1, %rbx
	cmpq	$256, %rbx
	je	.L2456
.L2068:
	testl	%r12d, %r12d
	je	.L2457
	movl	%ebx, %edi
	call	__btowc
	movq	72(%rsp), %rdi
	movl	%eax, %esi
	call	__collseq_table_lookup
	jmp	.L2066
.L2078:
	movq	%rcx, %rdi
	movq	%rcx, 32(%rsp)
	call	strlen
	cmpq	$1, %rax
	jne	.L2097
	movq	32(%rsp), %rcx
	movzbl	(%rcx), %ecx
	movq	%rcx, %rdx
	andl	$63, %ecx
	shrq	$3, %rdx
	salq	%cl, %rax
	andl	$24, %edx
	orq	%rax, (%r14,%rdx)
	movq	64(%rsp), %rax
	movl	$0, (%rax)
	jmp	.L2069
.L2450:
	movq	184(%rsp), %r13
	movl	176(%rsp), %r15d
	movq	152(%rsp), %r14
	movl	168(%rsp), %r12d
	movq	192(%rsp), %rbx
	movq	200(%rsp), %rbp
	movslq	60(%r13), %rdx
	movq	16(%r13), %rax
	cmpl	160(%rsp), %edx
	je	.L2458
.L2105:
	leal	1(%rdx), %ecx
	movl	%ecx, 60(%r13)
	movl	%r12d, (%rax,%rdx,4)
	movq	64(%rsp), %rax
	movl	$0, (%rax)
	jmp	.L2069
.L2463:
	testl	%r15d, %r15d
	jne	.L2050
.L2097:
	movq	64(%rsp), %rax
	movl	$3, (%rax)
	jmp	.L2018
.L2021:
	movq	64(%rsp), %rax
	movl	$2, (%rax)
	jmp	.L2018
.L2448:
	movl	136(%rsp), %eax
	testl	%eax, %eax
	jle	.L2108
	movq	120(%rsp), %rcx
	movq	%rbp, 32(%rsp)
	movq	%rbx, %rbp
	movq	%rcx, %rbx
	jmp	.L2111
	.p2align 4,,10
	.p2align 3
.L2109:
	addq	$8, %rbx
	cmpq	%rbx, 104(%rsp)
	je	.L2459
.L2111:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L2109
	movslq	4(%rbx), %rdx
	movq	80(%rsp), %rsi
	movq	%rdx, %rax
	movzbl	(%rsi,%rdx), %edx
	leal	1(%rax,%rdx), %eax
	movl	%eax, 96(%rsp)
	cltq
	movzbl	(%rsi,%rax), %edx
	cmpq	%rdx, %r8
	jne	.L2109
	leaq	1(%rsi,%rax), %rsi
	movq	%r8, %rdx
	movq	%r12, %rdi
	movq	%r8, 128(%rsp)
	call	memcmp@PLT
	testl	%eax, %eax
	movq	128(%rsp), %r8
	jne	.L2109
	movslq	56(%r13), %rdx
	movq	%rbp, %rbx
	movq	8(%r13), %rax
	cmpl	148(%rsp), %edx
	movq	32(%rsp), %rbp
	je	.L2460
.L2186:
	movl	96(%rsp), %esi
	leal	1(%rdx), %ecx
	movl	%ecx, 56(%r13)
	movl	%esi, (%rax,%rdx,4)
	movq	64(%rsp), %rax
	movl	$0, (%rax)
	jmp	.L2069
.L2459:
	movq	%rbp, %rbx
	movq	32(%rsp), %rbp
.L2108:
	cmpq	$1, %r8
	jne	.L2097
	jmp	.L2424
.L2443:
	movq	%r14, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L2428
.L2456:
	movq	64(%rsp), %rax
	movq	%r13, %r14
	movl	%r12d, %r15d
	movq	32(%rsp), %r13
	movq	96(%rsp), %rbx
	movq	128(%rsp), %rbp
	movl	$0, (%rax)
	jmp	.L2069
.L2028:
	movl	(%rsp), %eax
	subl	%eax, 56(%rbx)
	movb	$1, 8(%rbp)
	movl	224(%rsp), %eax
	jmp	.L2025
.L2449:
	movq	%rsi, %rdx
	subq	%rax, %rdx
.L2080:
	movl	(%rdx), %r12d
	leaq	5(%rdx), %rsi
	movzbl	4(%rdx), %eax
	testl	%r12d, %r12d
	js	.L2461
	testq	%rax, %rax
	je	.L2205
	movzbl	5(%rdx), %edi
	cmpb	%dil, 1(%rcx)
	jne	.L2086
	xorl	%edx, %edx
	jmp	.L2087
.L2088:
	movzbl	1(%rcx,%rdx), %edi
	cmpb	%dil, (%rsi,%rdx)
	jne	.L2086
.L2087:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L2088
.L2085:
	addq	%rdx, 152(%rsp)
	jmp	.L2079
	.p2align 4,,10
	.p2align 3
.L2086:
	leaq	(%rsi,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L2080
	movl	$4, %esi
	subq	%rax, %rsi
	addq	%rsi, %rdx
	jmp	.L2080
.L2461:
	testq	%rax, %rax
	je	.L2203
	movzbl	1(%rcx), %edi
	movzbl	5(%rdx), %r10d
	cmpb	%r10b, %dil
	movb	%dil, 128(%rsp)
	jne	.L2204
	xorl	%edx, %edx
	jmp	.L2084
.L2090:
	movzbl	(%rsi,%rdx), %r9d
	movzbl	1(%rcx,%rdx), %edi
	cmpb	%dil, %r9b
	jne	.L2083
.L2084:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L2090
	xorl	%eax, %eax
.L2082:
	movq	168(%rsp), %rcx
	movslq	%r12d, %r10
	addq	%rdx, 152(%rsp)
	subq	%r10, %rax
	movl	(%rcx,%rax,4), %r12d
	jmp	.L2079
.L2204:
	movzbl	128(%rsp), %edi
	movl	%r10d, %r9d
.L2083:
	cmpb	%dil, %r9b
	ja	.L2191
	leaq	(%rsi,%rax), %r11
	movzbl	(%r11), %edi
	cmpb	%dil, 128(%rsp)
	movb	%dil, 96(%rsp)
	jne	.L2206
	xorl	%edx, %edx
	jmp	.L2092
.L2093:
	movzbl	(%r11,%rdx), %r9d
	movzbl	1(%rcx,%rdx), %edi
	cmpb	%dil, %r9b
	jne	.L2207
.L2092:
	addq	$1, %rdx
	cmpq	%rdx, %rax
	jne	.L2093
.L2192:
	xorl	%r11d, %r11d
	cmpb	%r10b, 96(%rsp)
	jne	.L2095
.L2094:
	addq	$1, %r11
	movzbl	1(%rcx,%r11), %eax
	movzbl	(%rsi,%r11), %r10d
	cmpb	%al, %r10b
	movb	%al, 96(%rsp)
	je	.L2094
.L2095:
	xorl	%eax, %eax
	jmp	.L2096
.L2462:
	movzbl	1(%rcx,%r11), %edi
	movzbl	(%rsi,%r11), %r10d
	movb	%dil, 96(%rsp)
.L2096:
	movzbl	96(%rsp), %edi
	salq	$8, %rax
	addq	$1, %r11
	subl	%r10d, %edi
	movslq	%edi, %rdi
	addq	%rdi, %rax
	cmpq	%rdx, %r11
	jb	.L2462
	jmp	.L2082
.L2191:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rsi, %rdx
	jmp	.L2080
.L2441:
	movq	64(%rsp), %rax
	movq	$0, 48(%rsp)
	movl	$6, (%rax)
	jmp	.L1989
.L2114:
	movq	56(%rsp), %rax
	movl	(%rsp), %esi
	addl	%esi, 56(%rax)
	cmpb	$0, 167(%rsp)
	je	.L2117
	leaq	32(%r14), %rdx
	movq	%r14, %rax
.L2118:
	notq	(%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L2118
.L2117:
	movq	16(%rsp), %rax
	movl	164(%rax), %edx
	cmpl	$1, %edx
	jle	.L2119
	movq	120(%rax), %rcx
	xorl	%eax, %eax
.L2120:
	movq	(%rcx,%rax), %rsi
	andq	%rsi, (%r14,%rax)
	addq	$8, %rax
	cmpq	$32, %rax
	jne	.L2120
.L2119:
	movl	52(%r13), %r9d
	testl	%r9d, %r9d
	jne	.L2121
	cmpq	$0, 56(%r13)
	jne	.L2121
	cmpl	$0, 64(%r13)
	jne	.L2121
	subl	$1, %edx
	jle	.L2122
	cmpl	$0, 68(%r13)
	jne	.L2121
	testb	$1, 48(%r13)
	jne	.L2121
.L2122:
	movq	%r13, %rdi
	call	free_charset
	movq	16(%rsp), %rax
	movq	88(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movb	$3, 280(%rsp)
	movq	%r14, 272(%rsp)
	leaq	128(%rax), %rsi
	leaq	112(%rax), %rdi
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L2008
.L2116:
	movq	64(%rsp), %rax
	movl	$12, (%rax)
	jmp	.L2018
	.p2align 4,,10
	.p2align 3
.L2121:
	movq	16(%rsp), %rax
	movq	88(%rsp), %r8
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	orb	$2, 160(%rax)
	leaq	112(%rax), %rbx
	leaq	128(%rax), %rbp
	movb	$6, 280(%rsp)
	movq	%r13, 272(%rsp)
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	create_token_tree.isra.12
	movq	%rax, %rsi
	movq	%rax, 48(%rsp)
	leaq	32(%r14), %rdx
	testq	%rsi, %rsi
	movq	%r14, %rax
	je	.L2116
.L2125:
	cmpq	$0, (%rax)
	jne	.L2124
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L2125
	movq	%r14, %rdi
	call	free@PLT
	jmp	.L2008
.L2207:
	movb	%r9b, 96(%rsp)
.L2091:
	cmpb	%dil, 96(%rsp)
	jb	.L2191
	movq	%rax, %rdx
	movzbl	128(%rsp), %eax
	movb	%al, 96(%rsp)
	jmp	.L2192
.L2034:
	movzbl	%al, %edi
	call	__btowc
	movq	72(%rsp), %rdi
	movl	%eax, %esi
	call	__collseq_table_lookup
	movl	%eax, %r12d
.L2036:
	movl	240(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L2047
.L2046:
	cmpl	$1, %r8d
	je	.L2463
.L2049:
	cmpl	$3, %r8d
	jne	.L2097
	movq	248(%rsp), %r8
	xorl	%eax, %eax
	orq	$-1, %rcx
	movq	%r8, %rdi
	repnz scasb
	testl	%r15d, %r15d
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rcx
	jne	.L2464
.L2053:
	subq	$1, %rcx
	jne	.L2097
	movzbl	(%r8), %eax
	movq	48(%rsp), %rsi
	movzbl	(%rsi,%rax), %edx
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2033:
	cmpl	$1, %eax
	je	.L2465
	cmpl	$3, %eax
	je	.L2466
	testl	%r8d, %r8d
	movl	$-1, %r12d
	jne	.L2046
	testl	%r15d, %r15d
	je	.L2183
.L2047:
	movzbl	248(%rsp), %edi
	call	__btowc
	movq	72(%rsp), %rdi
	movl	%eax, %esi
	call	__collseq_table_lookup
	movl	%eax, %edx
	jmp	.L2048
	.p2align 4,,10
	.p2align 3
.L2205:
	xorl	%edx, %edx
	jmp	.L2085
.L2452:
	movl	144(%rsp), %ecx
	movq	%rax, %rdi
	leal	1(%rcx,%rcx), %esi
	movl	%esi, 144(%rsp)
	movslq	%esi, %rsi
	salq	$2, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2116
	movq	%rax, 0(%r13)
	movslq	52(%r13), %rdx
	jmp	.L2076
.L2032:
	movq	64(%rsp), %rax
	movl	$11, (%rax)
	jmp	.L2018
.L2455:
	movl	140(%rsp), %eax
	movq	%rcx, %rdi
	movl	%edx, 128(%rsp)
	leal	1(%rax,%rax), %eax
	movslq	%eax, %r8
	movl	%eax, 140(%rsp)
	salq	$2, %r8
	movq	%r8, %rsi
	movq	%r8, 96(%rsp)
	call	realloc@PLT
	movq	96(%rsp), %r8
	movq	32(%r13), %rdi
	movq	%rax, 32(%rsp)
	movq	%r8, %rsi
	call	realloc@PLT
	movq	32(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L2116
	testq	%rax, %rax
	movl	128(%rsp), %edx
	je	.L2116
	movq	%rax, 32(%r13)
	movq	%rcx, 24(%r13)
	movslq	64(%r13), %rax
	jmp	.L2062
.L2438:
	movq	24(%rsp), %rax
	cmpb	$2, 8(%rax)
	jne	.L2150
	movq	64(%rsp), %rax
	movl	$9, (%rax)
	jmp	.L2148
.L2466:
	movq	232(%rsp), %r10
	orq	$-1, %r9
	xorl	%eax, %eax
	movq	%r9, %rcx
	movq	%r10, %rdi
	repnz scasb
	notq	%rcx
	addq	%r9, %rcx
	testl	%r15d, %r15d
	jne	.L2467
	subq	$1, %rcx
	je	.L2468
	testl	%r8d, %r8d
	jne	.L2469
	movl	%r9d, %r12d
	jmp	.L2183
.L2465:
	orl	$-1, %r12d
	testl	%r15d, %r15d
	je	.L2035
	movl	232(%rsp), %esi
	movq	72(%rsp), %rdi
	call	__collseq_table_lookup
	movl	%eax, %r12d
	jmp	.L2036
.L2469:
	cmpl	$1, %r8d
	je	.L2097
	cmpl	$3, %r8d
	jne	.L2097
	movq	248(%rsp), %r8
	movq	%r9, %rcx
	movl	%r9d, %r12d
	movq	%r8, %rdi
	repnz scasb
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rax), %rcx
	jmp	.L2053
.L2468:
	movzbl	(%r10), %eax
	movq	48(%rsp), %rsi
	movzbl	(%rsi,%rax), %r12d
	jmp	.L2035
.L2467:
	cmpl	$0, 136(%rsp)
	jle	.L2040
	movq	120(%rsp), %r9
	jmp	.L2043
	.p2align 4,,10
	.p2align 3
.L2041:
	addq	$8, %r9
	cmpq	%r9, 104(%rsp)
	je	.L2040
.L2043:
	movl	(%r9), %edi
	testl	%edi, %edi
	je	.L2041
	movslq	4(%r9), %rdx
	movq	80(%rsp), %rsi
	movq	%rdx, %rax
	movzbl	(%rsi,%rdx), %edx
	leal	1(%rax,%rdx), %r12d
	movslq	%r12d, %rax
	movzbl	(%rsi,%rax), %edx
	cmpq	%rdx, %rcx
	jne	.L2041
	leaq	1(%rsi,%rax), %rsi
	movb	%dl, 168(%rsp)
	movq	%r10, %rdi
	movq	%rcx, %rdx
	movq	%r9, 152(%rsp)
	movl	%r8d, 128(%rsp)
	movq	%rcx, 96(%rsp)
	movq	%r10, 32(%rsp)
	call	memcmp@PLT
	testl	%eax, %eax
	movq	32(%rsp), %r10
	movq	96(%rsp), %rcx
	movl	128(%rsp), %r8d
	movq	152(%rsp), %r9
	movzbl	168(%rsp), %r11d
	jne	.L2041
	movzbl	%r11b, %eax
	movq	80(%rsp), %rsi
	leal	4(%r12,%rax), %eax
	andl	$-4, %eax
	addl	$4, %eax
	movslq	%eax, %rdx
	movl	(%rsi,%rdx), %edx
	leal	4(%rax,%rdx,4), %eax
	cltq
	movl	(%rsi,%rax), %r12d
	jmp	.L2044
.L2040:
	subq	$1, %rcx
	je	.L2182
	orl	$-1, %r12d
.L2044:
	testl	%r8d, %r8d
	je	.L2047
	cmpl	$1, %r8d
	jne	.L2049
.L2050:
	movl	248(%rsp), %esi
	movq	72(%rsp), %rdi
	call	__collseq_table_lookup
	movl	%eax, %edx
	jmp	.L2048
.L2182:
	movzbl	(%r10), %eax
	movq	48(%rsp), %rcx
	movzbl	(%rcx,%rax), %r12d
	jmp	.L2044
.L1990:
.L2460:
	movl	148(%rsp), %ecx
	movq	%rax, %rdi
	leal	1(%rcx,%rcx), %esi
	movl	%esi, 148(%rsp)
	movslq	%esi, %rsi
	salq	$2, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2116
	movq	%rax, 8(%r13)
	movslq	56(%r13), %rdx
	jmp	.L2186
.L2458:
	movl	160(%rsp), %ecx
	movq	%rax, %rdi
	leal	1(%rcx,%rcx), %esi
	movl	%esi, 160(%rsp)
	movslq	%esi, %rsi
	salq	$2, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L2116
	movq	%rax, 16(%r13)
	movslq	60(%r13), %rdx
	jmp	.L2105
.L2124:
	movq	88(%rsp), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movb	$3, 280(%rsp)
	movq	%r14, 272(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	je	.L2116
	pxor	%xmm0, %xmm0
	movq	48(%rsp), %rcx
	leaq	304(%rsp), %r8
	movq	%rax, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	movaps	%xmm0, 304(%rsp)
	movb	$10, 312(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L2008
	jmp	.L2116
.L2206:
	movzbl	128(%rsp), %edi
	jmp	.L2091
.L2203:
	xorl	%edx, %edx
	jmp	.L2082
.L2210:
	movl	$128, %r8d
	movl	$2, %r11d
.L2133:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movslq	%r8d, %rdx
	movl	$1, %r9d
	addq	%rdx, %rdx
	addq	%fs:(%rax), %rdx
	movq	16(%rsp), %rax
	leaq	(%rax,%r11,8), %rdi
.L2137:
	xorl	%eax, %eax
	jmp	.L2136
	.p2align 4,,10
	.p2align 3
.L2471:
	leal	(%r8,%rax), %esi
	cmpl	$95, %esi
	je	.L2220
.L2134:
	addq	$1, %rax
	cmpq	$64, %rax
	je	.L2470
.L2136:
	testb	$8, (%rdx,%rax,2)
	movl	%eax, %ecx
	je	.L2471
.L2220:
	movq	%r9, %rsi
	salq	%cl, %rsi
	orq	%rsi, 168(%rdi)
	jmp	.L2134
.L2470:
	addq	$1, %r11
	addl	$64, %r8d
	subq	$-128, %rdx
	addq	$8, %rdi
	cmpl	$3, %r11d
	jle	.L2137
	jmp	.L2132
.L2209:
	xorl	%r8d, %r8d
	xorl	%r11d, %r11d
	jmp	.L2133
.L2464:
	cmpl	$0, 136(%rsp)
	jle	.L2054
	movq	120(%rsp), %r10
	jmp	.L2057
	.p2align 4,,10
	.p2align 3
.L2055:
	addq	$8, %r10
	cmpq	%r10, 104(%rsp)
	je	.L2054
.L2057:
	movl	(%r10), %esi
	testl	%esi, %esi
	je	.L2055
	movslq	4(%r10), %rdx
	movq	80(%rsp), %rsi
	movq	%rdx, %rax
	movzbl	(%rsi,%rdx), %edx
	leal	1(%rax,%rdx), %r11d
	movslq	%r11d, %rax
	movzbl	(%rsi,%rax), %edx
	cmpq	%rdx, %rcx
	jne	.L2055
	leaq	1(%rsi,%rax), %rsi
	movb	%dl, 168(%rsp)
	movq	%r8, %rdi
	movq	%rcx, %rdx
	movl	%r11d, 152(%rsp)
	movq	%r10, 128(%rsp)
	movq	%rcx, 96(%rsp)
	movq	%r8, 32(%rsp)
	call	memcmp@PLT
	testl	%eax, %eax
	movq	32(%rsp), %r8
	movq	96(%rsp), %rcx
	movq	128(%rsp), %r10
	movl	152(%rsp), %r11d
	movzbl	168(%rsp), %r9d
	jne	.L2055
	movzbl	%r9b, %eax
	movq	80(%rsp), %rsi
	leal	4(%r11,%rax), %eax
	andl	$-4, %eax
	addl	$4, %eax
	movslq	%eax, %rdx
	movl	(%rsi,%rdx), %edx
	leal	4(%rax,%rdx,4), %eax
	cltq
	movl	(%rsi,%rax), %edx
	jmp	.L2048
.L2054:
	subq	$1, %rcx
	jne	.L2097
	movzbl	(%r8), %eax
	movq	48(%rsp), %rcx
	movzbl	(%rcx,%rax), %edx
	jmp	.L2048
	.size	parse_expression, .-parse_expression
	.p2align 4,,15
	.type	parse_branch, @function
parse_branch:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%r9, %rbx
	movq	%rdx, %rbp
	movl	%r8d, %r12d
	subq	$56, %rsp
	movq	(%rsi), %rax
	movq	%rdi, (%rsp)
	movq	%rax, 16(%rsp)
	call	parse_expression
	movl	(%rbx), %edx
	movq	%rax, %r14
	testl	%edx, %edx
	je	.L2473
	testq	%rax, %rax
	je	.L2505
.L2473:
	leaq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	.p2align 4,,10
	.p2align 3
.L2503:
	movzbl	8(%rbp), %eax
	movl	%eax, %edx
	andl	$-9, %edx
	cmpb	$2, %dl
	je	.L2472
.L2508:
	cmpb	$9, %al
	jne	.L2479
	testl	%r12d, %r12d
	jne	.L2472
.L2479:
	movq	(%rsp), %rdi
	movq	%rbx, %r9
	movl	%r12d, %r8d
	movq	%r15, %rcx
	movq	%rbp, %rdx
	movq	%r13, %rsi
	call	parse_expression
	movq	%rax, %r9
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L2475
	testq	%r9, %r9
	je	.L2506
.L2475:
	testq	%r14, %r14
	je	.L2477
	testq	%r9, %r9
	je	.L2477
	movq	16(%rsp), %rax
	pxor	%xmm0, %xmm0
	movq	24(%rsp), %r8
	movq	%r9, %rcx
	movq	%r14, %rdx
	movq	%r9, 8(%rsp)
	leaq	128(%rax), %rsi
	leaq	112(%rax), %rdi
	movaps	%xmm0, 32(%rsp)
	movb	$16, 40(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	8(%rsp), %r9
	je	.L2507
	movq	%rax, %r14
	movzbl	8(%rbp), %eax
	movl	%eax, %edx
	andl	$-9, %edx
	cmpb	$2, %dl
	jne	.L2508
.L2472:
	addq	$56, %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2477:
	testq	%r14, %r14
	cmove	%r9, %r14
	jmp	.L2503
.L2506:
	testq	%r14, %r14
	je	.L2505
	leaq	free_tree(%rip), %rsi
	xorl	%edx, %edx
	movq	%r14, %rdi
	call	postorder
.L2505:
	xorl	%r14d, %r14d
	jmp	.L2472
	.p2align 4,,10
	.p2align 3
.L2507:
	leaq	free_tree(%rip), %rsi
	movq	%r9, %rdi
	xorl	%edx, %edx
	call	postorder
	leaq	free_tree(%rip), %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	xorl	%r14d, %r14d
	call	postorder
	movl	$12, (%rbx)
	jmp	.L2472
	.size	parse_branch, .-parse_branch
	.p2align 4,,15
	.type	parse_reg_exp, @function
parse_reg_exp:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%r9, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rbp
	subq	$72, %rsp
	movq	(%rsi), %rbx
	movq	%rsi, 40(%rsp)
	movl	%r8d, 20(%rsp)
	movq	152(%rbx), %rax
	movq	%rax, 32(%rsp)
	call	parse_branch
	movl	0(%r13), %ecx
	movq	%rax, %r15
	testl	%ecx, %ecx
	je	.L2510
	testq	%rax, %rax
	je	.L2536
.L2510:
	leaq	48(%rsp), %rax
	movq	%rax, 8(%rsp)
	jmp	.L2528
	.p2align 4,,10
	.p2align 3
.L2539:
	cmpb	$9, %al
	jne	.L2519
	movl	20(%rsp), %edx
	testl	%edx, %edx
	jne	.L2518
.L2519:
	movq	152(%rbx), %rax
	movl	20(%rsp), %r8d
	movq	%r14, %rcx
	movq	40(%rsp), %rsi
	movq	%r13, %r9
	movq	%rbp, %rdx
	movq	%r12, %rdi
	movq	%rax, 24(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 152(%rbx)
	call	parse_branch
	movq	%rax, %rcx
	movl	0(%r13), %eax
	testl	%eax, %eax
	je	.L2514
	testq	%rcx, %rcx
	je	.L2537
.L2514:
	movq	24(%rsp), %rax
	orq	%rax, 152(%rbx)
.L2512:
	pxor	%xmm0, %xmm0
	movq	8(%rsp), %r8
	leaq	128(%rbx), %rsi
	leaq	112(%rbx), %rdi
	movq	%r15, %rdx
	movaps	%xmm0, 48(%rsp)
	movb	$10, 56(%rsp)
	call	create_token_tree.isra.12
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L2538
.L2528:
	cmpb	$10, 8(%rbp)
	jne	.L2509
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	orq	$8388608, %rdx
	call	peek_token
	addl	%eax, 56(%r12)
	movzbl	8(%rbp), %eax
	movl	%eax, %edx
	andl	$-9, %edx
	cmpb	$2, %dl
	jne	.L2539
.L2518:
	xorl	%ecx, %ecx
	jmp	.L2512
.L2537:
	testq	%r15, %r15
	je	.L2536
	leaq	free_tree(%rip), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	postorder
.L2536:
	xorl	%r15d, %r15d
.L2509:
	addq	$72, %rsp
	movq	%r15, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2538:
	movl	$12, 0(%r13)
	jmp	.L2509
	.size	parse_reg_exp, .-parse_reg_exp
	.section	.rodata.str1.1
.LC15:
	.string	"UTF-8"
	.text
	.p2align 4,,15
	.type	re_compile_internal, @function
re_compile_internal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$232, %rsp
	movzbl	56(%rdi), %eax
	movq	%rcx, 24(%rdi)
	movq	%rdi, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rcx, 32(%rsp)
	movl	$0, 76(%rsp)
	movq	$0, 16(%rdi)
	movq	$0, 48(%rdi)
	movb	%al, 24(%rsp)
	andl	$-112, %eax
	cmpq	$223, 8(%rdi)
	movb	%al, 56(%rdi)
	movq	(%rdi), %r15
	jbe	.L2761
.L2541:
	leaq	8(%r15), %rdi
	movq	8(%rsp), %rax
	movq	%r15, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	movq	$224, 16(%rax)
	xorl	%eax, %eax
	addl	$224, %ecx
	movq	$0, (%r15)
	movq	$0, 216(%r15)
	shrl	$3, %ecx
	cmpq	$1073741822, %r13
	rep stosq
	movl	$15, 128(%r15)
	ja	.L2550
	leaq	1(%r13), %rdi
	movq	%rdi, 8(%r15)
	salq	$4, %rdi
	call	malloc@PLT
	testq	%r13, %r13
	movq	%rax, %rbp
	movq	%rax, (%r15)
	je	.L2667
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2545:
	leal	(%rax,%rax), %esi
	cmpq	%rsi, %r13
	movq	%rsi, %rax
	jnb	.L2545
	leal	-1(%rsi), %ebx
.L2544:
	movl	$16, %edi
	call	calloc@PLT
	movq	%rax, 64(%r15)
	movq	_nl_current_LC_CTYPE@gottpoff(%rip), %rax
	movl	%ebx, 132(%r15)
	movq	%fs:(%rax), %rax
	movq	(%rax), %rcx
	movl	624(%rcx), %r10d
	movl	168(%rcx), %edx
	testl	%r10d, %r10d
	movl	%edx, 164(%r15)
	setne	%al
	cmpl	$6, %edx
	je	.L2762
	leal	0(,%rax,8), %ecx
	movzbl	160(%r15), %eax
	andl	$-9, %eax
	orl	%ecx, %eax
	cmpl	$1, %edx
	movb	%al, 160(%r15)
	jg	.L2662
.L2548:
	testq	%rbp, %rbp
	je	.L2550
	cmpq	$0, 64(%r15)
	je	.L2550
	movq	32(%rsp), %rax
	leaq	120(%rsp), %rsi
	movl	$13, %ecx
	leaq	112(%rsp), %r10
	movl	$0, 76(%rsp)
	movl	$0, 216(%r15)
	movq	%rsi, %rdi
	movl	%r13d, %ebx
	andl	$4194304, %eax
	movq	%r10, 24(%rsp)
	movq	%rax, 40(%rsp)
	movq	8(%rsp), %rax
	setne	%r8b
	movq	40(%rax), %rdx
	xorl	%eax, %eax
	rep stosq
	movq	16(%rsp), %rax
	movl	164(%r15), %ecx
	testq	%rdx, %rdx
	movq	%rdx, 192(%rsp)
	movl	%r13d, 176(%rsp)
	setne	%r12b
	movl	%r13d, 172(%rsp)
	movb	%r8b, 208(%rsp)
	movq	%rax, 112(%rsp)
	movzbl	160(%r15), %eax
	orl	%r8d, %r12d
	movl	%r12d, %esi
	movb	%r12b, 211(%rsp)
	movl	%ecx, 216(%rsp)
	movl	%r13d, 184(%rsp)
	movl	%r13d, 180(%rsp)
	movl	%eax, %edx
	shrb	$3, %al
	shrb	$2, %dl
	andl	$1, %eax
	andl	$1, %edx
	testl	%r13d, %r13d
	movb	%al, 210(%rsp)
	movb	%dl, 209(%rsp)
	jne	.L2763
	testb	%sil, %sil
	je	.L2556
.L2768:
	movq	120(%rsp), %rax
	movq	%rax, 16(%rsp)
.L2556:
	cmpq	$0, 40(%rsp)
	movq	16(%rsp), %rax
	movq	%rax, 120(%rsp)
	je	.L2557
	cmpl	$1, %ecx
	jle	.L2558
	movq	24(%rsp), %rbp
.L2560:
	movq	%rbp, %rdi
	call	build_wcs_upper_buffer
	testl	%eax, %eax
	jne	.L2587
	cmpl	160(%rsp), %ebx
	jle	.L2559
	movl	164(%r15), %eax
	addl	156(%rsp), %eax
	movl	164(%rsp), %esi
	cmpl	%eax, %esi
	jg	.L2559
	addl	%esi, %esi
	movq	%rbp, %rdi
	call	re_string_realloc_buffers
	testl	%eax, %eax
	je	.L2560
	.p2align 4,,10
	.p2align 3
.L2587:
	movl	%eax, 76(%rsp)
	movq	8(%rsp), %rax
	movq	(%rax), %rbp
.L2566:
	movq	112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2567
	.p2align 4,,10
	.p2align 3
.L2568:
	movq	(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L2568
.L2567:
	movq	32(%rbp), %rdi
	movq	$0, 112(%rbp)
	movl	$15, 128(%rbp)
	movq	$0, 104(%rbp)
	call	free@PLT
	movq	24(%rsp), %rdi
	movq	$0, 32(%rbp)
	call	re_string_destruct
.L2760:
	movq	%r15, %rdi
	call	free_dfa_content
	movq	8(%rsp), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
	movl	76(%rsp), %eax
.L2540:
	addq	$232, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L2762:
	movq	176(%rcx), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$6, %ecx
	repz cmpsb
	jne	.L2547
	orb	$4, 160(%r15)
.L2547:
	movzbl	160(%r15), %edx
	sall	$3, %eax
	andl	$-9, %edx
	orl	%edx, %eax
	movb	%al, 160(%r15)
.L2662:
	testb	$4, 160(%r15)
	je	.L2549
	leaq	utf8_sb_map(%rip), %rax
	movq	(%r15), %rbp
	movq	%rax, 120(%r15)
	jmp	.L2548
.L2557:
	cmpl	$1, %ecx
	jg	.L2764
	testb	%r12b, %r12b
	movl	164(%rsp), %edi
	je	.L2563
	cmpl	%edi, 176(%rsp)
	movl	156(%rsp), %eax
	cmovle	176(%rsp), %edi
	cmpl	%eax, %edi
	jle	.L2564
	movslq	%eax, %rcx
	movq	16(%rsp), %r8
	jmp	.L2565
	.p2align 4,,10
	.p2align 3
.L2765:
	movq	120(%rsp), %r8
.L2565:
	movl	152(%rsp), %edx
	movq	112(%rsp), %rsi
	addl	%eax, %edx
	addl	$1, %eax
	movslq	%edx, %rdx
	movzbl	(%rsi,%rdx), %edx
	movq	192(%rsp), %rsi
	movzbl	(%rsi,%rdx), %edx
	movb	%dl, (%r8,%rcx)
	addq	$1, %rcx
	cmpl	%eax, %edi
	jne	.L2765
.L2564:
	movl	%eax, 156(%rsp)
	movl	%eax, 160(%rsp)
	movl	$0, 76(%rsp)
.L2561:
	movq	8(%rsp), %rbp
	movq	32(%rsp), %r14
	leaq	80(%rsp), %rax
	movq	24(%rsp), %r12
	movq	%rax, %rdi
	movq	%rax, %r13
	movq	%rax, 32(%rsp)
	movq	0(%rbp), %rbx
	movq	%r14, %rdx
	movq	$0, 48(%rbp)
	orq	$8388608, %rdx
	movq	%r12, %rsi
	movq	%r14, 200(%rbx)
	call	peek_token
	leaq	76(%rsp), %r9
	xorl	%r8d, %r8d
	movq	%r12, %rdi
	movq	%r14, %rcx
	movq	%r13, %rdx
	movq	%rbp, %rsi
	addl	%eax, 168(%rsp)
	call	parse_reg_exp
	movl	76(%rsp), %r9d
	movq	%rax, %r12
	testl	%r9d, %r9d
	jne	.L2766
	leaq	96(%rsp), %rax
	pxor	%xmm0, %xmm0
	leaq	112(%rbx), %r13
	subq	$-128, %rbx
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rax, %r8
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, 16(%rsp)
	movaps	%xmm0, 96(%rsp)
	movb	$2, 104(%rsp)
	call	create_token_tree.isra.12
	testq	%r12, %r12
	movq	%rax, %r14
	movq	%rax, %rdx
	je	.L2572
.L2663:
	pxor	%xmm0, %xmm0
	movq	16(%rsp), %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movaps	%xmm0, 96(%rsp)
	movb	$16, 104(%rsp)
	call	create_token_tree.isra.12
	movq	%r14, %rdx
	movq	%rax, %r14
.L2572:
	testq	%rdx, %rdx
	movq	8(%rsp), %rbx
	sete	%dl
	testq	%r14, %r14
	sete	%al
	orb	%al, %dl
	movq	(%rbx), %rbp
	jne	.L2767
	movq	8(%rbp), %rbx
	movq	%r14, 104(%r15)
	leaq	0(,%rbx,4), %r13
	salq	$4, %rbx
	movq	%r13, %rdi
	call	malloc@PLT
	movq	%r13, %rdi
	movq	%rax, 24(%rbp)
	movq	%rax, %r12
	call	malloc@PLT
	movq	%rbx, %rdi
	movq	%rax, 32(%rbp)
	movq	%rax, %r13
	call	malloc@PLT
	movq	%rbx, %rdi
	movq	%rax, 40(%rbp)
	movq	%rax, %r14
	call	malloc@PLT
	testq	%r12, %r12
	movq	%rax, 48(%rbp)
	je	.L2755
	testq	%r13, %r13
	je	.L2755
	testq	%r14, %r14
	je	.L2755
	testq	%rax, %rax
	je	.L2755
	movq	8(%rsp), %rax
	movq	48(%rax), %rbx
	leaq	0(,%rbx,4), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 208(%rbp)
	je	.L2579
	testq	%rbx, %rbx
	je	.L2580
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2581:
	movl	%edx, (%rax,%rdx,4)
	addq	$1, %rdx
	cmpq	%rdx, %rbx
	jne	.L2581
.L2580:
	movq	104(%rbp), %rdi
	leaq	optimize_subexps(%rip), %rsi
	movq	%rbp, %rdx
	call	preorder
	movq	8(%rsp), %rax
	movq	208(%rbp), %rdi
	movq	48(%rax), %rcx
	testq	%rcx, %rcx
	je	.L2583
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L2585
	jmp	.L2579
	.p2align 4,,10
	.p2align 3
.L2586:
	cmpl	%eax, (%rdi,%rdx,4)
	jne	.L2579
.L2585:
	addl	$1, %eax
	movslq	%eax, %rdx
	cmpq	%rcx, %rdx
	jb	.L2586
	je	.L2583
.L2579:
	movq	104(%rbp), %rdi
	movq	8(%rsp), %rdx
	leaq	lower_subexps(%rip), %rsi
	call	postorder
	testl	%eax, %eax
	jne	.L2587
	movq	104(%rbp), %rdi
	leaq	calc_first(%rip), %rsi
	movq	%rbp, %rdx
	call	postorder
	testl	%eax, %eax
	jne	.L2587
	movq	104(%rbp), %rdi
	leaq	calc_next(%rip), %rsi
	movq	%rbp, %rdx
	leaq	.L2591(%rip), %rbx
	call	preorder
	movq	104(%rbp), %r13
	.p2align 4,,10
	.p2align 3
.L2588:
	cmpb	$16, 48(%r13)
	movslq	56(%r13), %rax
	ja	.L2589
	movzbl	48(%r13), %edx
	movslq	(%rbx,%rdx,4), %rdx
	addq	%rbx, %rdx
	jmp	*%rdx
	.section	.rodata
	.align 4
	.align 4
.L2591:
	.long	.L2589-.L2591
	.long	.L2589-.L2591
	.long	.L2756-.L2591
	.long	.L2589-.L2591
	.long	.L2592-.L2591
	.long	.L2589-.L2591
	.long	.L2589-.L2591
	.long	.L2589-.L2591
	.long	.L2593-.L2591
	.long	.L2593-.L2591
	.long	.L2594-.L2591
	.long	.L2594-.L2591
	.long	.L2593-.L2591
	.long	.L2589-.L2591
	.long	.L2589-.L2591
	.long	.L2589-.L2591
	.long	.L2756-.L2591
	.text
.L2763:
	leal	1(%r13), %esi
	movq	%r10, %rdi
	call	re_string_realloc_buffers
	testl	%eax, %eax
	jne	.L2587
	movzbl	211(%rsp), %esi
	movl	164(%r15), %ecx
	testb	%sil, %sil
	je	.L2556
	jmp	.L2768
.L2550:
	movl	$12, 76(%rsp)
	jmp	.L2760
.L2549:
	movl	$1, %esi
	movl	$32, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 120(%r15)
	je	.L2550
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movl	$1, %r12d
.L2551:
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2553:
	leal	(%rbx,%r14), %edi
	call	__btowc
	cmpl	$-1, %eax
	je	.L2552
	movq	120(%r15), %rax
	movq	%r12, %rdx
	movl	%r14d, %ecx
	salq	%cl, %rdx
	addq	%rbp, %rax
	orq	%rdx, (%rax)
.L2552:
	addq	$1, %r14
	cmpq	$64, %r14
	jne	.L2553
	addl	$64, %ebx
	addq	$8, %rbp
	cmpl	$256, %ebx
	jne	.L2551
	movq	(%r15), %rbp
	jmp	.L2548
.L2589:
	movq	32(%r13), %rdx
	movl	56(%rdx), %ecx
	movq	24(%rbp), %rdx
	movl	%ecx, (%rdx,%rax,4)
.L2756:
	movq	8(%r13), %r12
.L2596:
	xorl	%ecx, %ecx
	testq	%r12, %r12
	je	.L2607
.L2606:
	movq	%r12, %r13
	jmp	.L2588
	.p2align 4,,10
	.p2align 3
.L2769:
	movq	%rdx, %r13
.L2607:
	movq	16(%r13), %r12
	cmpq	%rcx, %r12
	sete	%al
	testq	%r12, %r12
	sete	%dl
	orb	%dl, %al
	je	.L2606
	movq	0(%r13), %rdx
	movq	%r13, %rcx
	testq	%rdx, %rdx
	jne	.L2769
	movb	%al, 48(%rsp)
	movq	48(%rbp), %rax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L2611:
	salq	$4, %r14
	movl	4(%rax,%r14), %r8d
	movq	%r14, %r12
	testl	%r8d, %r8d
	je	.L2612
	addl	$1, %r13d
	movslq	%r13d, %r14
.L2613:
	movq	16(%rbp), %r12
	cmpq	%r14, %r12
	jne	.L2611
	testb	%bl, %bl
	je	.L2610
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	jmp	.L2611
.L2594:
	orb	$1, 160(%rbp)
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.L2597
	movq	24(%r12), %rdx
	movl	56(%rdx), %edx
.L2598:
	movq	16(%r13), %rcx
	testq	%rcx, %rcx
	je	.L2599
	movq	24(%rcx), %rcx
	movl	56(%rcx), %ecx
.L2600:
	salq	$4, %rax
	addq	40(%rbp), %rax
	movl	$8, %edi
	movl	%ecx, 60(%rsp)
	movl	%edx, 48(%rsp)
	movl	$2, (%rax)
	movq	%rax, %r14
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r14)
	movl	48(%rsp), %edx
	movl	60(%rsp), %ecx
	je	.L2759
	cmpl	%ecx, %edx
	je	.L2770
	movl	$2, 4(%r14)
	jl	.L2771
	movl	%ecx, (%rax)
	movl	%edx, 4(%rax)
	jmp	.L2596
.L2593:
	salq	$4, %rax
	addq	40(%rbp), %rax
	movq	32(%r13), %rdx
	movl	$4, %edi
	movl	56(%rdx), %r12d
	movq	%rax, %r14
	movabsq	$4294967297, %rax
	movq	%rax, (%r14)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r14)
	je	.L2772
	movl	%r12d, (%rax)
	movq	8(%r13), %r12
	jmp	.L2596
.L2592:
	movq	32(%r13), %rdx
	movl	56(%rdx), %r14d
	movq	24(%rbp), %rdx
	movl	%r14d, (%rdx,%rax,4)
	cmpb	$4, 48(%r13)
	jne	.L2756
	salq	$4, %rax
	addq	40(%rbp), %rax
	movl	$4, %edi
	movq	%rax, %r12
	movabsq	$4294967297, %rax
	movq	%rax, (%r12)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r12)
	je	.L2773
	movl	%r14d, (%rax)
	movq	8(%r13), %r12
	jmp	.L2596
.L2563:
	movl	%edi, 156(%rsp)
	movl	%edi, 160(%rsp)
	movl	$0, 76(%rsp)
	jmp	.L2561
	.p2align 4,,10
	.p2align 3
.L2612:
	movq	16(%rsp), %rdi
	movl	$1, %ecx
	movl	%r13d, %edx
	movq	%rbp, %rsi
	call	calc_eclosure_iter
	testl	%eax, %eax
	jne	.L2587
	movq	48(%rbp), %rax
	addl	$1, %r13d
	movslq	%r13d, %r14
	movl	4(%rax,%r12), %edi
	testl	%edi, %edi
	jne	.L2613
	movq	104(%rsp), %rdi
	call	free@PLT
	cmpq	%r14, 16(%rbp)
	movzbl	48(%rsp), %ebx
	jne	.L2616
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
.L2616:
	movq	48(%rbp), %rax
	jmp	.L2611
.L2610:
	movq	8(%rsp), %rax
	testb	$16, 56(%rax)
	jne	.L2619
	movq	8(%rsp), %rax
	cmpq	$0, 48(%rax)
	je	.L2619
	testb	$1, 160(%rbp)
	jne	.L2622
.L2619:
	movl	140(%rbp), %esi
	testl	%esi, %esi
	je	.L2623
.L2622:
	movq	%r12, %rdi
	salq	$4, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 56(%rbp)
	je	.L2624
	xorl	%edx, %edx
	testq	%r12, %r12
	jne	.L2625
	jmp	.L2623
	.p2align 4,,10
	.p2align 3
.L2774:
	movq	56(%rbp), %rax
.L2625:
	pxor	%xmm0, %xmm0
	movq	%rdx, %rcx
	addq	$1, %rdx
	salq	$4, %rcx
	movups	%xmm0, (%rax,%rcx)
	movq	16(%rbp), %rax
	cmpq	%rdx, %rax
	ja	.L2774
	testq	%rax, %rax
	je	.L2623
	movq	48(%rbp), %rax
	xorl	%esi, %esi
	movb	%bl, 60(%rsp)
	movq	%r15, 48(%rsp)
	movq	%rsi, %rbx
.L2630:
	movq	%rbx, %r12
	movl	%ebx, %r13d
	salq	$4, %r12
	leaq	(%rax,%r12), %rdx
	movl	4(%rdx), %ecx
	movq	8(%rdx), %r14
	testl	%ecx, %ecx
	jle	.L2627
	movl	$1, %r15d
	jmp	.L2629
	.p2align 4,,10
	.p2align 3
.L2775:
	movq	48(%rbp), %rax
	movl	%r15d, %esi
	addq	$1, %r15
	cmpl	4(%rax,%r12), %esi
	jge	.L2627
.L2629:
	movslq	-4(%r14,%r15,4), %rdi
	movl	%r13d, %esi
	salq	$4, %rdi
	addq	56(%rbp), %rdi
	call	re_node_set_insert_last
	testb	%al, %al
	jne	.L2775
	movq	48(%rsp), %r15
.L2759:
	movl	$12, %eax
	jmp	.L2587
.L2627:
	addq	$1, %rbx
	cmpq	%rbx, 16(%rbp)
	ja	.L2630
	movzbl	60(%rsp), %ebx
	movq	48(%rsp), %r15
.L2623:
	movzbl	160(%r15), %eax
	movl	$0, 76(%rsp)
	shrb	$2, %al
	cmpq	$0, 40(%rsp)
	sete	%dl
	andb	%dl, %al
	jne	.L2776
.L2631:
	movq	104(%r15), %rax
	movq	16(%rsp), %r14
	movq	24(%rax), %rax
	movq	%r14, %rdi
	movslq	56(%rax), %rax
	movl	%eax, 136(%r15)
	salq	$4, %rax
	addq	48(%r15), %rax
	movq	%rax, %rsi
	call	re_node_set_init_copy
	testl	%eax, %eax
	movl	%eax, %ebx
	movl	%eax, 80(%rsp)
	jne	.L2645
	movl	140(%r15), %eax
	testl	%eax, %eax
	jle	.L2646
	movl	100(%rsp), %r10d
	testl	%r10d, %r10d
	jle	.L2646
	xorl	%r9d, %r9d
	leaq	8(%r14), %rbp
	jmp	.L2653
	.p2align 4,,10
	.p2align 3
.L2758:
	addl	$1, %r9d
	cmpl	%r9d, %r10d
	jle	.L2646
.L2653:
	movq	104(%rsp), %rcx
	movslq	%r9d, %rax
	movq	(%r15), %rsi
	movslq	(%rcx,%rax,4), %rdi
	salq	$4, %rdi
	leaq	(%rsi,%rdi), %r11
	cmpb	$4, 8(%r11)
	jne	.L2758
	leal	-1(%r10), %r8d
	movl	$1, %edx
	addq	$2, %r8
	jmp	.L2651
	.p2align 4,,10
	.p2align 3
.L2649:
	movl	%edx, %eax
	addq	$1, %rdx
	cmpq	%rdx, %r8
	je	.L2777
.L2651:
	movslq	-4(%rcx,%rdx,4), %rax
	salq	$4, %rax
	addq	%rsi, %rax
	cmpb	$9, 8(%rax)
	jne	.L2649
	movl	(%r11), %r14d
	cmpl	%r14d, (%rax)
	jne	.L2649
.L2650:
	movq	40(%r15), %rax
	movq	%rbp, %rsi
	movq	8(%rax,%rdi), %rax
	movl	%r10d, %edi
	movslq	(%rax), %r11
	movl	%r11d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L2758
	movq	48(%r15), %rsi
	movq	16(%rsp), %rdi
	salq	$4, %r11
	addq	%r11, %rsi
	call	re_node_set_merge
	testl	%eax, %eax
	jne	.L2675
	movl	100(%rsp), %r10d
	movl	$1, %r9d
	cmpl	%r9d, %r10d
	jg	.L2653
	.p2align 4,,10
	.p2align 3
.L2646:
	movq	16(%rsp), %rdx
	movq	32(%rsp), %rdi
	xorl	%ecx, %ecx
	movq	%r15, %rsi
	call	re_acquire_state_context
	testq	%rax, %rax
	movq	%rax, 72(%r15)
	je	.L2656
	cmpb	$0, 80(%rax)
	js	.L2778
	movq	%rax, 96(%r15)
	movq	%rax, 88(%r15)
	movq	%rax, 80(%r15)
.L2657:
	movq	104(%rsp), %rdi
	call	free@PLT
.L2645:
	movq	8(%rsp), %rax
	movl	%ebx, 76(%rsp)
	movq	(%rax), %rbp
	movq	112(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L2659
	.p2align 4,,10
	.p2align 3
.L2660:
	movq	(%rdi), %rbx
	call	free@PLT
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.L2660
.L2659:
	movq	32(%rbp), %rdi
	movq	$0, 112(%rbp)
	movl	$15, 128(%rbp)
	movq	$0, 104(%rbp)
	call	free@PLT
	movq	24(%rsp), %rdi
	movq	$0, 32(%rbp)
	call	re_string_destruct
	movl	76(%rsp), %eax
	testl	%eax, %eax
	je	.L2540
	jmp	.L2760
	.p2align 4,,10
	.p2align 3
.L2777:
	cmpl	%eax, %r10d
	jne	.L2650
	jmp	.L2758
.L2755:
	movl	$12, 76(%rsp)
	jmp	.L2566
.L2764:
	movq	24(%rsp), %rdi
	call	build_wcs_buffer
	movl	$0, 76(%rsp)
	jmp	.L2561
.L2776:
	movq	8(%rsp), %rsi
	cmpq	$0, 40(%rsi)
	jne	.L2631
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.L2672
	movq	(%r15), %rcx
	leaq	.L2635(%rip), %r10
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	movq	%rcx, %rdx
.L2641:
	cmpb	$12, 8(%rdx)
	ja	.L2633
	movzbl	8(%rdx), %r8d
	movslq	(%r10,%r8,4), %r8
	addq	%r10, %r8
	jmp	*%r8
	.section	.rodata
	.align 4
	.align 4
.L2635:
	.long	.L2633-.L2635
	.long	.L2634-.L2635
	.long	.L2638-.L2635
	.long	.L2637-.L2635
	.long	.L2638-.L2635
	.long	.L2673-.L2635
	.long	.L2631-.L2635
	.long	.L2633-.L2635
	.long	.L2638-.L2635
	.long	.L2638-.L2635
	.long	.L2638-.L2635
	.long	.L2638-.L2635
	.long	.L2639-.L2635
	.text
.L2667:
	xorl	%ebx, %ebx
	movl	$1, %esi
	jmp	.L2544
.L2778:
	movq	16(%rsp), %rbp
	movq	32(%rsp), %r14
	movl	$1, %ecx
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r14, %rdi
	call	re_acquire_state_context
	movl	$2, %ecx
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	movq	%rax, 80(%r15)
	call	re_acquire_state_context
	movl	$6, %ecx
	movq	%rax, 88(%r15)
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movq	%r14, %rdi
	call	re_acquire_state_context
	cmpq	$0, 80(%r15)
	movq	%rax, 96(%r15)
	je	.L2656
	cmpq	$0, 88(%r15)
	je	.L2656
	testq	%rax, %rax
	jne	.L2657
.L2656:
	movl	80(%rsp), %ebx
	jmp	.L2645
.L2761:
	movq	%r15, %rdi
	movl	$224, %esi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L2666
	movq	8(%rsp), %rax
	movq	$224, 8(%rax)
	movq	%r15, (%rax)
	jmp	.L2541
.L2599:
	movq	32(%r13), %rcx
	movl	56(%rcx), %ecx
	jmp	.L2600
.L2597:
	movq	32(%r13), %rdx
	movl	56(%rdx), %edx
	jmp	.L2598
.L2771:
	movl	%edx, (%rax)
	movl	%ecx, 4(%rax)
	jmp	.L2596
.L2770:
	movl	$1, 4(%r14)
	movl	%edx, (%rax)
	jmp	.L2596
.L2559:
	movl	$0, 76(%rsp)
	jmp	.L2561
.L2558:
	movq	24(%rsp), %rdi
	call	build_upper_buffer
	movl	$0, 76(%rsp)
	jmp	.L2561
.L2767:
	movl	$12, 76(%rsp)
	movq	$0, 104(%r15)
	jmp	.L2566
.L2766:
	testq	%rax, %rax
	je	.L2779
	leaq	96(%rsp), %rax
	pxor	%xmm0, %xmm0
	leaq	112(%rbx), %r13
	subq	$-128, %rbx
	xorl	%ecx, %ecx
	movq	%rax, %r8
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rax, 16(%rsp)
	movaps	%xmm0, 96(%rsp)
	movb	$2, 104(%rsp)
	call	create_token_tree.isra.12
	movq	%rax, %r14
	jmp	.L2663
.L2583:
	call	free@PLT
	movq	$0, 208(%rbp)
	jmp	.L2579
.L2673:
	movl	%eax, %ebx
.L2638:
	addq	$1, %r9
	addq	$16, %rdx
	cmpq	%r9, %rdi
	jne	.L2641
	orb	%bl, %sil
	je	.L2632
	movq	%rdi, %rax
	salq	$4, %rax
	addq	%rcx, %rax
	jmp	.L2644
.L2642:
	cmpb	$5, %dl
	jne	.L2643
	movb	$7, 8(%rcx)
.L2643:
	addq	$16, %rcx
	cmpq	%rcx, %rax
	je	.L2780
.L2644:
	movzbl	8(%rcx), %edx
	cmpb	$1, %dl
	jne	.L2642
	cmpb	$0, (%rcx)
	jns	.L2643
	andb	$-33, 10(%rcx)
	jmp	.L2643
	.p2align 4,,10
	.p2align 3
.L2634:
	cmpb	$0, (%rdx)
	cmovs	%eax, %esi
	jmp	.L2638
.L2639:
	movl	(%rdx), %r8d
	cmpl	$32, %r8d
	je	.L2638
	jbe	.L2781
	cmpl	$64, %r8d
	je	.L2638
	addl	$-128, %r8d
	je	.L2638
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2637:
	movq	(%rdx), %r8
	cmpq	$0, 16(%r8)
	jne	.L2631
	cmpq	$0, 24(%r8)
	je	.L2638
	jmp	.L2631
	.p2align 4,,10
	.p2align 3
.L2675:
	movl	%eax, %ebx
	jmp	.L2645
.L2772:
	movq	$0, (%r14)
	jmp	.L2759
.L2666:
	movl	$12, %eax
	jmp	.L2540
.L2624:
	movq	8(%rsp), %rax
	movl	$12, 76(%rsp)
	movq	(%rax), %rbp
	jmp	.L2566
.L2773:
	movq	$0, (%r12)
	movl	$12, %eax
	jmp	.L2587
.L2779:
	movq	8(%rsp), %rax
	movq	$0, 104(%r15)
	movq	(%rax), %rbp
	jmp	.L2566
.L2781:
	cmpl	$16, %r8d
	je	.L2638
	jmp	.L2631
.L2672:
	xorl	%esi, %esi
.L2632:
	movl	140(%r15), %edx
	movl	$1, 164(%r15)
	testl	%edx, %edx
	movzbl	160(%r15), %edx
	setg	%al
	orl	%esi, %eax
	andl	$1, %eax
	addl	%eax, %eax
	andl	$-7, %edx
	orl	%edx, %eax
	movb	%al, 160(%r15)
	jmp	.L2631
.L2780:
	movl	%ebx, %esi
	jmp	.L2632
.L2633:
	call	abort
	.size	re_compile_internal, .-re_compile_internal
	.p2align 4,,15
	.type	check_node_accept_bytes.isra.24, @function
check_node_accept_bytes.isra.24:
	movslq	%edx, %rdx
	pushq	%r15
	pushq	%r14
	salq	$4, %rdx
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	addq	%rdx, %rdi
	subq	$56, %rsp
	movzbl	8(%rdi), %r10d
	cmpb	$7, %r10b
	je	.L3015
	movl	104(%rcx), %ebp
	movq	16(%rcx), %r11
	cmpl	$1, %ebp
	je	.L2799
	movl	44(%rcx), %edx
	leal	1(%r8), %eax
	cmpl	%eax, %edx
	jle	.L2799
	cltq
	cmpl	$-1, (%r11,%rax,4)
	leaq	0(,%rax,4), %r9
	jne	.L2799
	subl	%r8d, %edx
	movslq	%r8d, %r12
	leaq	-4(%r11,%r9), %r9
	subl	$2, %edx
	movl	$2, %eax
	addq	$2, %rdx
	jmp	.L2800
	.p2align 4,,10
	.p2align 3
.L2802:
	addq	$1, %rax
	cmpl	$-1, -4(%r9,%rax,4)
	jne	.L2801
.L2800:
	cmpq	%rax, %rdx
	movl	%eax, %ebx
	jne	.L2802
.L2801:
	cmpb	$5, %r10b
	je	.L2884
	movb	$0, (%rsp)
.L2798:
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movl	$1, %r12d
	movq	%fs:(%rax), %rax
	movq	(%rax), %r14
	movl	64(%r14), %eax
	testl	%eax, %eax
	movl	%eax, 24(%rsp)
	je	.L2804
	movq	8(%rcx), %rax
	movslq	%r8d, %rdx
	addq	%rax, %rdx
	movq	%rax, 16(%rsp)
	leaq	1(%rdx), %rax
	movzbl	(%rdx), %esi
	movq	%rax, 8(%rsp)
	movq	80(%r14), %rax
	movslq	(%rax,%rsi,4), %rax
	testq	%rax, %rax
	js	.L3016
.L2805:
	movq	8(%rsp), %r12
	subq	16(%rsp), %r12
	subl	%r8d, %r12d
	cmpl	$1, %r12d
	setle	%al
	andb	%al, (%rsp)
.L2804:
	cmpb	$6, %r10b
	jne	.L2784
	cmpb	$0, (%rsp)
	je	.L3017
.L2784:
	xorl	%ebx, %ebx
.L2782:
	addq	$56, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2884:
	movq	(%rsi), %rax
	testb	$64, %al
	je	.L3018
.L2803:
	testb	$-128, %al
	je	.L2782
	movq	8(%rcx), %rax
	cmpb	$0, (%rax,%r12)
	je	.L2784
	jmp	.L2782
	.p2align 4,,10
	.p2align 3
.L2799:
	cmpb	$5, %r10b
	je	.L2784
	movb	$1, (%rsp)
	movl	$1, %ebx
	jmp	.L2798
	.p2align 4,,10
	.p2align 3
.L3018:
	movq	8(%rcx), %rdx
	cmpb	$10, (%rdx,%r12)
	jne	.L2803
	jmp	.L2784
	.p2align 4,,10
	.p2align 3
.L3017:
	movq	(%rdi), %r13
	movq	8(%rcx), %r15
	movslq	%r8d, %r8
	addq	%r8, %r15
	cmpq	$0, 64(%r13)
	movl	52(%r13), %ecx
	je	.L3019
.L2821:
	cmpl	$1, %ebp
	je	.L3020
	movl	(%r11,%r8,4), %edx
.L2824:
	testl	%ecx, %ecx
	movl	%edx, %ebp
	jle	.L2822
	movq	0(%r13), %rax
	cmpl	(%rax), %edx
	je	.L2825
	addq	$4, %rax
	subl	$1, %ecx
	leaq	(%rax,%rcx,4), %rcx
	jmp	.L2827
	.p2align 4,,10
	.p2align 3
.L2829:
	addq	$4, %rax
	cmpl	-4(%rax), %edx
	je	.L2825
.L2827:
	cmpq	%rax, %rcx
	jne	.L2829
	jmp	.L2822
	.p2align 4,,10
	.p2align 3
.L3016:
	movq	96(%r14), %r9
	movq	%rdi, %r13
	subq	%rax, %r9
	movl	64(%rcx), %eax
	subl	%r8d, %eax
	cltq
	leaq	-1(%rax), %r15
	.p2align 4,,10
	.p2align 3
.L2806:
	movzbl	4(%r9), %eax
	movl	(%r9), %edi
	leaq	5(%r9), %rsi
	cmpq	%rax, %r15
	movq	%rax, %r12
	cmovbe	%r15, %r12
	testl	%edi, %edi
	js	.L3021
	testq	%r12, %r12
	je	.L2810
	movzbl	5(%r9), %edi
	cmpb	%dil, 1(%rdx)
	jne	.L2887
	xorl	%edi, %edi
	jmp	.L2811
	.p2align 4,,10
	.p2align 3
.L2812:
	movzbl	1(%rdx,%rdi), %r9d
	cmpb	%r9b, (%rsi,%rdi)
	jne	.L2888
.L2811:
	addq	$1, %rdi
	cmpq	%r12, %rdi
	jne	.L2812
.L2810:
	cmpq	%r12, %rax
	je	.L3007
	leaq	(%rsi,%rax), %r9
	addq	$1, %rax
	andl	$3, %eax
	je	.L2806
	movl	$4, %esi
	subq	%rax, %rsi
	addq	%rsi, %r9
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L3021:
	testq	%r12, %r12
	je	.L2808
	movzbl	5(%r9), %edi
	cmpb	%dil, 1(%rdx)
	jne	.L2886
	xorl	%edi, %edi
	jmp	.L2809
	.p2align 4,,10
	.p2align 3
.L2815:
	movzbl	1(%rdx,%rdi), %r9d
	cmpb	%r9b, (%rsi,%rdi)
	jne	.L2889
.L2809:
	addq	$1, %rdi
	cmpq	%r12, %rdi
	jne	.L2815
.L2808:
	cmpq	%r12, %rax
	je	.L3007
	cmpq	%r12, %r15
	je	.L2882
	movq	8(%rsp), %rdi
	movzbl	(%rdi,%r12), %edi
	cmpb	%dil, (%rsi,%r12)
	ja	.L2882
	testq	%rax, %rax
	je	.L3007
	leaq	(%rsi,%rax), %rdi
	movzbl	1(%rdx), %r9d
	movzbl	(%rdi), %r12d
	movq	%rdi, 32(%rsp)
	cmpb	%r12b, %r9b
	jne	.L2818
	movq	%r15, 40(%rsp)
	xorl	%edi, %edi
	movq	32(%rsp), %r15
	jmp	.L2819
	.p2align 4,,10
	.p2align 3
.L2820:
	movzbl	(%r15,%rdi), %r12d
	movzbl	1(%rdx,%rdi), %r9d
	cmpb	%r9b, %r12b
	jne	.L3000
.L2819:
	addq	$1, %rdi
	cmpq	%rdi, %rax
	jne	.L2820
.L3007:
	movq	%r13, %rdi
	addq	%rax, 8(%rsp)
	jmp	.L2805
	.p2align 4,,10
	.p2align 3
.L2888:
	movq	%rdi, %r12
	jmp	.L2810
	.p2align 4,,10
	.p2align 3
.L3000:
	movq	40(%rsp), %r15
.L2818:
	cmpb	%r9b, %r12b
	jnb	.L3007
	.p2align 4,,10
	.p2align 3
.L2882:
	addq	%rax, %rax
	leaq	4(%rax), %r9
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %r9
	addq	%rsi, %r9
	jmp	.L2806
	.p2align 4,,10
	.p2align 3
.L2889:
	movq	%rdi, %r12
	jmp	.L2808
	.p2align 4,,10
	.p2align 3
.L3015:
	movq	8(%rcx), %rsi
	movslq	%r8d, %rdx
	movzbl	(%rsi,%rdx), %eax
	cmpb	$-63, %al
	jbe	.L2784
	movl	64(%rcx), %r9d
	leal	1(%r8), %ecx
	cmpl	%r9d, %ecx
	jge	.L2784
	leaq	1(%rsi,%rdx), %rcx
	cmpb	$-33, %al
	movzbl	(%rcx), %edi
	ja	.L2785
	addl	$-128, %edi
	movl	$2, %ebx
	cmpb	$63, %dil
	ja	.L2784
	jmp	.L2782
	.p2align 4,,10
	.p2align 3
.L3019:
	testl	%ecx, %ecx
	jne	.L2821
	xorl	%ebp, %ebp
.L2822:
	movl	68(%r13), %ecx
	testl	%ecx, %ecx
	jle	.L2830
	xorl	%r14d, %r14d
	jmp	.L2832
	.p2align 4,,10
	.p2align 3
.L2831:
	leal	1(%r14), %eax
	addq	$1, %r14
	cmpl	%eax, 68(%r13)
	jle	.L3022
.L2832:
	movq	40(%r13), %rax
	movl	%ebp, %edi
	movq	(%rax,%r14,8), %rsi
	call	__iswctype
	testl	%eax, %eax
	je	.L2831
.L2825:
	movzbl	48(%r13), %r14d
	andl	$1, %r14d
.L2828:
	testb	%r14b, %r14b
	je	.L2782
	jmp	.L2784
	.p2align 4,,10
	.p2align 3
.L2887:
	xorl	%r12d, %r12d
	jmp	.L2810
.L2886:
	xorl	%r12d, %r12d
	jmp	.L2808
.L3022:
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movq	(%rax), %r14
	movl	64(%r14), %eax
	movl	%eax, 24(%rsp)
.L2830:
	movl	24(%rsp), %edx
	testl	%edx, %edx
	jne	.L2833
	movzbl	48(%r13), %r14d
	movl	64(%r13), %eax
	andl	$1, %r14d
	testl	%eax, %eax
	jle	.L2835
	movq	24(%r13), %rdx
	subl	$1, %eax
	leaq	4(,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L2879:
	cmpl	%ebp, (%rdx,%rax)
	ja	.L2878
	movq	32(%r13), %rsi
	cmpl	%ebp, (%rsi,%rax)
	jnb	.L2828
.L2878:
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L2879
.L2835:
	testb	%r14b, %r14b
	je	.L2784
.L2855:
	cmpl	%r12d, %ebx
	cmovl	%r12d, %ebx
	jmp	.L2782
.L3020:
	movzbl	(%r15), %edx
	jmp	.L2824
.L2833:
	movl	56(%r13), %eax
	cmpl	$0, %eax
	je	.L2836
	movq	184(%r14), %r11
	jle	.L2836
	movq	8(%r13), %rcx
	leal	-1(%r12), %r8d
	subl	$1, %eax
	addq	$1, %r8
	leaq	4(%rcx,%rax,4), %r10
	jmp	.L2842
	.p2align 4,,10
	.p2align 3
.L2837:
	addq	$4, %rcx
	cmpq	%rcx, %r10
	je	.L2836
.L2842:
	movslq	(%rcx), %rdx
	addq	%r11, %rdx
	movzbl	(%rdx), %r9d
	cmpl	%r12d, %r9d
	jne	.L2837
	testl	%r12d, %r12d
	jle	.L2892
	movzbl	(%r15), %eax
	cmpb	%al, 1(%rdx)
	jne	.L2837
	movl	$1, %eax
	jmp	.L2839
	.p2align 4,,10
	.p2align 3
.L2840:
	movzbl	(%r15,%rax), %esi
	addq	$1, %rax
	cmpb	(%rdx,%rax), %sil
	jne	.L2838
.L2839:
	cmpq	%rax, %r8
	movl	%eax, %edi
	jne	.L2840
.L2838:
	cmpl	%edi, %r9d
	jne	.L2837
.L3012:
	movzbl	48(%r13), %ebp
.L2841:
	andl	$1, %ebp
	je	.L2906
	testl	%edi, %edi
	jle	.L2855
	jmp	.L2784
	.p2align 4,,10
	.p2align 3
.L2836:
	movl	64(%r13), %eax
	testl	%eax, %eax
	je	.L2843
	cmpl	%r12d, %ebx
	jl	.L2844
	movq	200(%r14), %rdi
	movl	%ebp, %esi
	call	__collseq_table_lookup
.L2845:
	movl	64(%r13), %edx
	testl	%edx, %edx
	jle	.L2843
	movq	24(%r13), %rsi
	subl	$1, %edx
	leaq	4(,%rdx,4), %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2853:
	cmpl	%eax, (%rsi,%rdx)
	ja	.L2852
	movq	32(%r13), %rdi
	cmpl	(%rdi,%rdx), %eax
	jbe	.L2897
.L2852:
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.L2853
.L2843:
	movzbl	48(%r13), %ebp
	movl	60(%r13), %r9d
	movl	%ebp, %r14d
	andl	$1, %r14d
	testl	%r9d, %r9d
	je	.L2835
	movq	_nl_current_LC_COLLATE@gottpoff(%rip), %rax
	movzbl	(%r15), %edx
	movq	%fs:(%rax), %rax
	movq	(%rax), %r8
	movq	80(%r8), %rax
	movslq	(%rax,%rdx,4), %rax
	testq	%rax, %rax
	movq	%rax, %rsi
	js	.L3023
.L2856:
	movl	%esi, %eax
	andl	$16777215, %eax
	je	.L2835
	movq	88(%r8), %r15
	cltq
	testl	%r9d, %r9d
	movzbl	(%r15,%rax), %ecx
	movq	%rcx, %r8
	movq	%rcx, (%rsp)
	jle	.L2835
	movq	16(%r13), %r10
	leal	-1(%r9), %edx
	leaq	1(%r15,%rax), %rax
	sarl	$24, %esi
	movl	%esi, %r13d
	movq	%rax, 8(%rsp)
	leaq	4(%r10,%rdx,4), %r9
	jmp	.L2877
.L2876:
	addq	$4, %r10
	cmpq	%r10, %r9
	je	.L2835
.L2877:
	movl	(%r10), %eax
	movl	%eax, %edx
	andl	$16777215, %edx
	cmpb	%r8b, (%r15,%rdx)
	jne	.L2876
	sarl	$24, %eax
	cmpl	%r13d, %eax
	jne	.L2876
	leaq	1(%r15,%rdx), %rsi
	movq	8(%rsp), %rdi
	movq	(%rsp), %rdx
	movq	%r10, 32(%rsp)
	movq	%r9, 24(%rsp)
	movb	%r8b, 16(%rsp)
	call	memcmp@PLT
	testl	%eax, %eax
	movzbl	16(%rsp), %r8d
	movq	24(%rsp), %r9
	movq	32(%rsp), %r10
	jne	.L2876
	movl	%r12d, %edi
	jmp	.L2841
	.p2align 4,,10
	.p2align 3
.L2785:
	cmpb	$-17, %al
	ja	.L2787
	cmpb	$-32, %al
	jne	.L2907
	cmpb	$-97, %dil
	jbe	.L2784
.L2907:
	movl	$3, %ebx
.L2789:
	addl	%ebx, %r8d
	cmpl	%r8d, %r9d
	jl	.L2784
	movq	%rcx, %rax
	leaq	2(%rsi,%rdx), %rdx
	leal	-2(%rbx), %ecx
	addq	%rcx, %rdx
	jmp	.L2795
.L3024:
	addq	$1, %rax
	cmpq	%rax, %rdx
	je	.L2782
.L2795:
	movzbl	(%rax), %ecx
	addl	$-128, %ecx
	cmpb	$63, %cl
	jbe	.L3024
	jmp	.L2784
.L2892:
	xorl	%edi, %edi
	jmp	.L2838
.L2897:
	movl	%r12d, %edi
	jmp	.L3012
.L2844:
	movq	184(%r14), %rcx
	movq	192(%r14), %rdx
	movslq	%r12d, %rsi
	movl	$-1, %eax
	subq	%rcx, %rdx
	testl	%edx, %edx
	movl	%edx, %edi
	jle	.L2845
	xorl	%eax, %eax
	jmp	.L2851
.L2846:
	leal	3(%r9,%rdx), %eax
	andl	$-4, %eax
	addl	$4, %eax
	testb	%r8b, %r8b
	movslq	%eax, %rdx
	movl	(%rcx,%rdx), %edx
	leal	4(%rax,%rdx,4), %eax
	jne	.L3025
	addl	$4, %eax
	cmpl	%eax, %edi
	jle	.L3026
.L2851:
	movslq	%eax, %rdx
	xorl	%r8d, %r8d
	movzbl	(%rcx,%rdx), %edx
	addl	%edx, %eax
	leal	2(%rax), %r9d
	addl	$1, %eax
	cltq
	movzbl	(%rcx,%rax), %edx
	cmpq	%rdx, %rsi
	jne	.L2846
	testl	%edx, %edx
	je	.L2896
	movslq	%r9d, %r8
	addq	%rcx, %r8
	movzbl	(%r8), %eax
	cmpb	%al, (%r15)
	jne	.L2896
	leal	-1(%rdx), %r11d
	movl	$1, %eax
	addq	$1, %r11
	jmp	.L2848
.L2849:
	movzbl	(%r8,%rax), %ebp
	addq	$1, %rax
	cmpb	-1(%r15,%rax), %bpl
	jne	.L2847
.L2848:
	cmpq	%rax, %r11
	movl	%eax, %r10d
	jne	.L2849
.L2847:
	cmpl	%r10d, %edx
	sete	%r8b
	jmp	.L2846
.L2787:
	cmpb	$-9, %al
	ja	.L2790
	cmpb	$-16, %al
	jne	.L2908
	cmpb	$-113, %dil
	jbe	.L2784
.L2908:
	movl	$4, %ebx
	jmp	.L2789
.L2790:
	cmpb	$-5, %al
	ja	.L2792
	cmpb	$-8, %al
	jne	.L2909
	cmpb	$-121, %dil
	jbe	.L2784
.L2909:
	movl	$5, %ebx
	jmp	.L2789
.L3023:
	movq	96(%r8), %rdx
	leaq	1(%r15), %rcx
	movq	%rcx, 8(%rsp)
	subq	%rax, %rdx
	movslq	%r12d, %rax
	subq	$1, %rax
	movq	%rax, 16(%rsp)
.L2857:
	movzbl	4(%rdx), %eax
	movq	16(%rsp), %rdi
	leaq	5(%rdx), %rcx
	movslq	(%rdx), %rsi
	cmpq	%rax, %rdi
	cmova	%rax, %rdi
	testl	%esi, %esi
	js	.L3027
	testq	%rdi, %rdi
	je	.L2861
	movzbl	1(%r15), %r11d
	cmpb	%r11b, 5(%rdx)
	jne	.L2899
	xorl	%edx, %edx
	jmp	.L2862
.L2863:
	movzbl	1(%r15,%rdx), %r11d
	cmpb	%r11b, (%rcx,%rdx)
	jne	.L2900
.L2862:
	addq	$1, %rdx
	cmpq	%rdi, %rdx
	jne	.L2863
.L2861:
	cmpq	%rdi, %rax
	je	.L2856
	leaq	(%rcx,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L2857
	movl	$4, %ecx
	subq	%rax, %rcx
	addq	%rcx, %rdx
	jmp	.L2857
.L3027:
	testq	%rdi, %rdi
	je	.L2859
	movzbl	5(%rdx), %r11d
	cmpb	%r11b, 1(%r15)
	jne	.L2898
	xorl	%r10d, %r10d
	jmp	.L2860
.L2865:
	movzbl	1(%r15,%r10), %r11d
	cmpb	%r11b, (%rcx,%r10)
	jne	.L2901
.L2860:
	addq	$1, %r10
	cmpq	%rdi, %r10
	jne	.L2865
.L2859:
	cmpq	%rdi, %rax
	je	.L2902
	cmpq	%rdi, 16(%rsp)
	je	.L2867
	movq	8(%rsp), %r11
	movzbl	(%r11,%rdi), %r11d
	cmpb	%r11b, (%rcx,%rdi)
	ja	.L2867
	movzbl	1(%r15), %edi
	testq	%rax, %rax
	movb	%dil, (%rsp)
	je	.L3008
	leaq	(%rcx,%rax), %r11
	movzbl	(%r11), %edi
	movq	%r11, %r10
	cmpb	(%rsp), %dil
	movb	%dil, 24(%rsp)
	movl	$0, %edi
	jne	.L2869
.L2870:
	addq	$1, %rdi
	cmpq	%rdi, %rax
	je	.L3028
	movzbl	1(%r15,%rdi), %r10d
	cmpb	%r10b, (%r11,%rdi)
	je	.L2870
	movq	%r11, %r10
.L2869:
	movq	8(%rsp), %r11
	movzbl	(%r11,%rdi), %r11d
	cmpb	%r11b, (%r10,%rdi)
	jb	.L3029
.L3008:
	movzbl	(%rsp), %r10d
.L2868:
	xorl	%edi, %edi
	cmpb	%r10b, 5(%rdx)
	jne	.L2873
.L2872:
	addq	$1, %rdi
	movzbl	1(%r15,%rdi), %edx
	cmpb	%dl, (%rcx,%rdi)
	je	.L2872
.L2873:
	xorl	%edx, %edx
.L2874:
	movzbl	1(%r15,%rdi), %r10d
	movzbl	(%rcx,%rdi), %r11d
	salq	$8, %rdx
	addq	$1, %rdi
	subl	%r11d, %r10d
	movslq	%r10d, %r10
	addq	%r10, %rdx
	cmpq	%rdi, %rax
	ja	.L2874
.L2866:
	movq	104(%r8), %rax
	subq	%rsi, %rdx
	movl	(%rax,%rdx,4), %esi
	jmp	.L2856
.L2900:
	movq	%rdx, %rdi
	jmp	.L2861
.L2867:
	addq	%rax, %rax
.L3011:
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rcx, %rdx
	jmp	.L2857
.L2901:
	movq	%r10, %rdi
	jmp	.L2859
.L2899:
	xorl	%edi, %edi
	jmp	.L2861
.L2792:
	cmpb	$-3, %al
	ja	.L2784
	cmpb	$-4, %al
	jne	.L2910
	cmpb	$-125, %dil
	jbe	.L2784
.L2910:
	movl	$6, %ebx
	jmp	.L2789
.L3026:
	orl	$-1, %eax
	jmp	.L2845
.L3025:
	cltq
	movl	(%rcx,%rax), %eax
	jmp	.L2845
.L3029:
	addq	%rax, %rax
	jmp	.L3011
.L3028:
	movzbl	24(%rsp), %r10d
	jmp	.L2868
.L2898:
	xorl	%edi, %edi
	jmp	.L2859
.L2906:
	movl	%edi, %ebx
	jmp	.L2782
.L2902:
	xorl	%edx, %edx
	jmp	.L2866
.L2896:
	xorl	%r10d, %r10d
	jmp	.L2847
	.size	check_node_accept_bytes.isra.24, .-check_node_accept_bytes.isra.24
	.p2align 4,,15
	.type	check_arrival, @function
check_arrival:
	pushq	%r15
	pushq	%r14
	movslq	%edx, %rax
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rax, %rbp
	salq	$4, %rax
	movq	%rdi, %rbx
	subq	$168, %rsp
	movq	112(%rdi), %rcx
	movl	4(%rsi), %r12d
	movq	%rsi, 88(%rsp)
	movl	%r8d, 108(%rsp)
	movl	%r9d, 72(%rsp)
	movl	$0, 120(%rsp)
	addq	(%rcx), %rax
	movq	%rcx, 48(%rsp)
	movl	(%rax), %eax
	movl	%eax, 76(%rsp)
	movl	168(%rdi), %eax
	addl	%r9d, %eax
	cmpl	%r12d, %eax
	jge	.L3110
.L3031:
	movq	88(%rsp), %rcx
	movq	136(%rbx), %rdx
	movl	(%rcx), %eax
	movq	%rdx, 96(%rsp)
	movq	8(%rcx), %r14
	movl	56(%rbx), %edx
	testl	%eax, %eax
	movl	%eax, 32(%rsp)
	movl	%eax, %r15d
	movl	%edx, 104(%rsp)
	movq	%r14, 136(%rbx)
	movl	120(%rbx), %edx
	je	.L3035
	leal	-1(%rax), %esi
	movl	%eax, 56(%rbx)
	movq	%rbx, %rdi
	call	re_string_context_at
	cmpl	%r13d, %r15d
	movl	%eax, %r12d
	je	.L3067
	movslq	32(%rsp), %r13
	movq	(%r14,%r13,8), %r15
	testq	%r15, %r15
	je	.L3038
	testb	$64, 80(%r15)
	jne	.L3111
.L3038:
	pxor	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
.L3040:
	movl	32(%rsp), %eax
	movl	72(%rsp), %ecx
	cmpl	%ecx, %eax
	jge	.L3043
	movl	168(%rbx), %r8d
	testl	%r8d, %r8d
	js	.L3043
	leaq	8(,%r13,8), %rcx
	movq	136(%rbx), %rax
	movl	$0, 36(%rsp)
	movq	%rcx, 24(%rsp)
	leaq	128(%rsp), %rcx
	movq	%rcx, (%rsp)
	leaq	144(%rsp), %rcx
	movq	%rcx, 8(%rsp)
	leaq	124(%rsp), %rcx
	movq	%rcx, 80(%rsp)
.L3065:
	movq	24(%rsp), %rcx
	movl	$0, 132(%rsp)
	movq	(%rax,%rcx), %rsi
	testq	%rsi, %rsi
	je	.L3044
	movq	(%rsp), %rdi
	addq	$8, %rsi
	call	re_node_set_merge
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	jne	.L3109
	testq	%r15, %r15
	je	.L3060
.L3069:
	movl	28(%r15), %edi
	pxor	%xmm0, %xmm0
	movq	8(%rsp), %rax
	movq	112(%rbx), %r12
	movl	$0, 124(%rsp)
	testl	%edi, %edi
	movaps	%xmm0, (%rax)
	jle	.L3072
	leaq	200(%r12), %rcx
	movl	$4, %esi
	xorl	%ebp, %ebp
	movq	32(%r15), %rax
	movq	(%r12), %rdi
	movq	%rcx, 40(%rsp)
	movslq	(%rax), %r14
	movq	%r14, %r13
	movq	%r14, %rdx
	salq	$4, %r13
	leaq	(%rdi,%r13), %rax
	movq	%r13, %r9
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%rsi, %r12
	jmp	.L3048
	.p2align 4,,10
	.p2align 3
.L3058:
	movl	32(%rsp), %edx
	leaq	(%rdi,%r9), %rsi
	movq	%rbx, %rdi
	call	check_node_accept
	testb	%al, %al
	je	.L3051
.L3050:
	leaq	0(,%r13,4), %rcx
.L3057:
	movq	24(%r14), %rax
	movq	(%rsp), %rdi
	movl	(%rax,%rcx), %esi
	call	re_node_set_insert
	testb	%al, %al
	je	.L3108
.L3051:
	addl	$1, %ebp
	cmpl	28(%r15), %ebp
	jge	.L3112
	movq	32(%r15), %rax
	movq	(%r14), %rdi
	movslq	(%rax,%r12), %r13
	addq	$4, %r12
	movq	%r13, %r9
	movq	%r13, %rdx
	salq	$4, %r9
	leaq	(%rdi,%r9), %rax
.L3048:
	testb	$16, 10(%rax)
	je	.L3058
	movl	32(%rsp), %r8d
	movq	40(%rsp), %rsi
	movq	%rbx, %rcx
	movq	%r9, 16(%rsp)
	call	check_node_accept_bytes.isra.24
	cmpl	$1, %eax
	movq	16(%rsp), %r9
	jle	.L3052
	addl	32(%rsp), %eax
	movq	24(%r14), %rdx
	leaq	0(,%r13,4), %rcx
	movl	(%rdx,%r13,4), %edx
	movslq	%eax, %r13
	leaq	0(,%r13,8), %rax
	movq	%rax, 56(%rsp)
	movq	136(%rbx), %rax
	movq	(%rax,%r13,8), %rsi
	movl	$0, 148(%rsp)
	testq	%rsi, %rsi
	je	.L3053
	movq	8(%rsp), %rdi
	addq	$8, %rsi
	movq	%rcx, 64(%rsp)
	movl	%edx, 16(%rsp)
	call	re_node_set_merge
	testl	%eax, %eax
	movl	%eax, 124(%rsp)
	movl	16(%rsp), %edx
	movq	64(%rsp), %rcx
	jne	.L3107
.L3053:
	movq	8(%rsp), %rdi
	movl	%edx, %esi
	movq	%rcx, 16(%rsp)
	call	re_node_set_insert
	testb	%al, %al
	movq	16(%rsp), %rcx
	je	.L3108
	movq	56(%rsp), %r8
	addq	136(%rbx), %r8
	movq	%r14, %rsi
	movq	8(%rsp), %rdx
	movq	80(%rsp), %rdi
	movq	%rcx, 64(%rsp)
	movq	%r8, 16(%rsp)
	call	re_acquire_state
	movq	16(%rsp), %r8
	movq	64(%rsp), %rcx
	movq	%rax, (%r8)
	movq	136(%rbx), %rax
	cmpq	$0, (%rax,%r13,8)
	jne	.L3057
	movl	124(%rsp), %esi
	testl	%esi, %esi
	je	.L3057
	.p2align 4,,10
	.p2align 3
.L3107:
	movq	152(%rsp), %rdi
	call	free@PLT
	movl	124(%rsp), %eax
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	jne	.L3109
	.p2align 4,,10
	.p2align 3
.L3060:
	movl	32(%rsp), %eax
	movl	132(%rsp), %edx
	leal	1(%rax), %ebp
	testl	%edx, %edx
	movl	%ebp, %r12d
	je	.L3061
	movl	224(%rsp), %ecx
	movl	76(%rsp), %edx
	movq	(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	check_arrival_expand_ecl
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	jne	.L3109
	movl	224(%rsp), %r8d
	movl	76(%rsp), %ecx
	movl	%ebp, %edx
	movq	(%rsp), %rsi
	movq	%rbx, %rdi
	call	expand_bkref_cache
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	jne	.L3109
.L3061:
	movl	120(%rbx), %edx
	movl	32(%rsp), %esi
	movq	%rbx, %rdi
	call	re_string_context_at
	movq	(%rsp), %rdx
	movq	48(%rsp), %rsi
	leaq	120(%rsp), %rdi
	movl	%eax, %ecx
	call	re_acquire_state_context
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L3113
	movq	136(%rbx), %rax
	movslq	%ebp, %rdx
	movl	$0, 36(%rsp)
	movq	%r15, (%rax,%rdx,8)
.L3068:
	cmpl	%ebp, 72(%rsp)
	jle	.L3105
	addq	$8, 24(%rsp)
	movl	36(%rsp), %ecx
	cmpl	%ecx, 168(%rbx)
	movl	%ebp, 32(%rsp)
	jge	.L3065
.L3105:
	movl	%r12d, 32(%rsp)
.L3043:
	movq	136(%rsp), %rdi
	call	free@PLT
	movslq	72(%rsp), %rdx
	movq	136(%rbx), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	je	.L3114
	movq	88(%rsp), %rcx
	movl	32(%rsp), %edx
	leaq	16(%rax), %rsi
	movl	%edx, (%rcx)
	movq	96(%rsp), %rcx
	movl	108(%rsp), %edx
	movq	%rcx, 136(%rbx)
	movl	104(%rsp), %ecx
	movl	%ecx, 56(%rbx)
	movl	12(%rax), %edi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	sete	%al
	movzbl	%al, %eax
	jmp	.L3030
	.p2align 4,,10
	.p2align 3
.L3035:
	leal	-1(%r13), %esi
	movl	%r13d, 56(%rbx)
	movq	%rbx, %rdi
	call	re_string_context_at
	movl	%eax, %r12d
.L3067:
	movabsq	$4294967297, %rax
	movl	$4, %edi
	movq	%rax, 128(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 136(%rsp)
	je	.L3034
	movl	%ebp, (%rax)
	movl	224(%rsp), %ecx
	leaq	128(%rsp), %rbp
	movl	76(%rsp), %edx
	movq	48(%rsp), %rdi
	movq	%rbp, %rsi
	movl	$0, 120(%rsp)
	call	check_arrival_expand_ecl
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	movl	%r13d, 32(%rsp)
	je	.L3037
	jmp	.L3109
	.p2align 4,,10
	.p2align 3
.L3052:
	testl	%eax, %eax
	jne	.L3050
	movq	(%r14), %rdi
	jmp	.L3058
	.p2align 4,,10
	.p2align 3
.L3044:
	testq	%r15, %r15
	jne	.L3069
	movl	32(%rsp), %eax
	leal	1(%rax), %ebp
	movl	%ebp, %r12d
	jmp	.L3061
	.p2align 4,,10
	.p2align 3
.L3108:
	movq	152(%rsp), %rdi
	call	free@PLT
	movl	$12, 120(%rsp)
.L3109:
	movq	136(%rsp), %rdi
	call	free@PLT
	movl	120(%rsp), %eax
.L3030:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3112:
	movq	152(%rsp), %rdi
.L3047:
	call	free@PLT
	movl	$0, 120(%rsp)
	jmp	.L3060
	.p2align 4,,10
	.p2align 3
.L3113:
	movl	120(%rsp), %eax
	testl	%eax, %eax
	jne	.L3109
	movq	136(%rbx), %rax
	movslq	%ebp, %rdx
	addl	$1, 36(%rsp)
	movq	$0, (%rax,%rdx,8)
	jmp	.L3068
	.p2align 4,,10
	.p2align 3
.L3111:
	leaq	128(%rsp), %rbp
	leaq	8(%r15), %rsi
	movq	%rbp, %rdi
	call	re_node_set_init_copy
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	jne	.L3030
	testb	$64, 80(%r15)
	je	.L3040
.L3037:
	movl	132(%rsp), %r10d
	testl	%r10d, %r10d
	je	.L3041
	movl	224(%rsp), %r8d
	movl	76(%rsp), %ecx
	movq	%rbp, %rsi
	movl	32(%rsp), %edx
	movq	%rbx, %rdi
	call	expand_bkref_cache
	testl	%eax, %eax
	movl	%eax, 120(%rsp)
	jne	.L3109
.L3041:
	movq	48(%rsp), %rsi
	leaq	120(%rsp), %rdi
	movl	%r12d, %ecx
	movq	%rbp, %rdx
	call	re_acquire_state_context
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L3115
.L3042:
	movslq	32(%rsp), %r13
	movq	136(%rbx), %rax
	movq	%r15, (%rax,%r13,8)
	jmp	.L3040
.L3110:
	leal	1(%rax), %r15d
	movl	$2147483647, %eax
	subl	%r12d, %eax
	cmpl	%r15d, %eax
	jl	.L3034
	movl	%r12d, %r14d
	addl	%r15d, %r14d
	js	.L3034
	movq	88(%rsp), %rax
	movslq	%r14d, %rsi
	salq	$3, %rsi
	movq	8(%rax), %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L3034
	movq	88(%rsp), %rcx
	movslq	%r15d, %rdx
	xorl	%esi, %esi
	salq	$3, %rdx
	movl	%r14d, 4(%rcx)
	movq	%rax, 8(%rcx)
	movslq	%r12d, %rcx
	leaq	(%rax,%rcx,8), %rdi
	call	memset@PLT
	jmp	.L3031
.L3115:
	movl	120(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L3042
	jmp	.L3109
.L3034:
	movl	$12, %eax
	jmp	.L3030
.L3114:
	movq	88(%rsp), %rax
	movl	32(%rsp), %ecx
	movl	%ecx, (%rax)
	movq	96(%rsp), %rax
	movq	%rax, 136(%rbx)
	movl	104(%rsp), %eax
	movl	%eax, 56(%rbx)
	movl	$1, %eax
	jmp	.L3030
.L3072:
	xorl	%edi, %edi
	jmp	.L3047
	.size	check_arrival, .-check_arrival
	.p2align 4,,15
	.type	get_subexp_sub.isra.25, @function
get_subexp_sub.isra.25:
	pushq	%r15
	pushq	%r14
	movl	%ecx, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	leaq	8(%rdx), %rsi
	movq	%rdx, %rbp
	movl	%r8d, %r9d
	movl	%r8d, %r12d
	subq	$32, %rsp
	movl	4(%rdx), %ecx
	movl	%r14d, %r8d
	pushq	$8
	movl	(%rdx), %edx
	movq	%rdi, %rbx
	call	check_arrival
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L3124
.L3116:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3124:
	movl	148(%rbx), %eax
	movl	152(%rbx), %edx
	movl	4(%rbp), %r15d
	movl	0(%r13), %ecx
	movq	160(%rbx), %rdi
	cmpl	%edx, %eax
	movl	%r15d, %esi
	jge	.L3125
.L3118:
	movslq	%eax, %rdx
	salq	$5, %rdx
	testl	%eax, %eax
	jle	.L3120
	leaq	-32(%rdi,%rdx), %r8
	cmpl	4(%r8), %r12d
	je	.L3126
.L3120:
	addq	%rdx, %rdi
	xorl	%edx, %edx
	cmpl	%ecx, %r15d
	sete	%dl
	addl	$1, %eax
	movl	%r15d, 12(%rdi)
	negq	%rdx
	movl	%r14d, (%rdi)
	movl	%r12d, 4(%rdi)
	movl	%ecx, 8(%rdi)
	movq	%rdx, 16(%rdi)
	subl	%ecx, %r15d
	movl	%eax, 148(%rbx)
	movb	$0, 24(%rdi)
	cmpl	%r15d, 168(%rbx)
	jge	.L3121
	movl	%r15d, 168(%rbx)
.L3121:
	addl	%r12d, %esi
	subl	0(%r13), %esi
	addq	$24, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	clean_state_log_if_needed
	.p2align 4,,10
	.p2align 3
.L3126:
	movb	$1, 24(%r8)
	jmp	.L3120
	.p2align 4,,10
	.p2align 3
.L3125:
	leal	(%rdx,%rdx), %esi
	movl	%ecx, 12(%rsp)
	movslq	%esi, %rsi
	salq	$5, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movl	12(%rsp), %ecx
	je	.L3127
	movslq	148(%rbx), %rdi
	movslq	152(%rbx), %rdx
	xorl	%esi, %esi
	movq	%rax, 160(%rbx)
	movl	%ecx, 12(%rsp)
	salq	$5, %rdi
	salq	$5, %rdx
	addq	%rax, %rdi
	call	memset@PLT
	sall	152(%rbx)
	movl	148(%rbx), %eax
	movq	160(%rbx), %rdi
	movl	4(%rbp), %esi
	movl	12(%rsp), %ecx
	jmp	.L3118
	.p2align 4,,10
	.p2align 3
.L3127:
	movq	160(%rbx), %rdi
	call	free@PLT
	movl	$12, %eax
	jmp	.L3116
	.size	get_subexp_sub.isra.25, .-get_subexp_sub.isra.25
	.p2align 4,,15
	.type	transit_state_bkref, @function
transit_state_bkref:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$168, %rsp
	movl	4(%rsi), %r8d
	movq	112(%rdi), %rax
	movl	56(%rdi), %r15d
	testl	%r8d, %r8d
	movq	%rax, 16(%rsp)
	jle	.L3129
	movslq	%r15d, %rax
	movq	%rsi, 24(%rsp)
	movq	%rdi, %r13
	salq	$3, %rax
	movq	$0, 8(%rsp)
	movq	%rax, 104(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 112(%rsp)
	jmp	.L3179
	.p2align 4,,10
	.p2align 3
.L3252:
	testb	$1, %al
	je	.L3130
	andl	$8, %edx
	jne	.L3130
.L3134:
	testb	$32, %cl
	je	.L3135
	testb	$2, %al
	je	.L3130
.L3135:
	andl	$128, %ecx
	je	.L3131
	testb	$8, %al
	jne	.L3131
	.p2align 4,,10
	.p2align 3
.L3130:
	movq	8(%rsp), %rdi
	movq	24(%rsp), %rbx
	movl	%edi, %eax
	addq	$1, %rdi
	addl	$1, %eax
	cmpl	%eax, 4(%rbx)
	movq	%rdi, 8(%rsp)
	jle	.L3129
.L3179:
	movq	24(%rsp), %rax
	movq	8(%rsp), %rbx
	movq	16(%rsp), %rcx
	movq	8(%rax), %rax
	movslq	(%rax,%rbx,4), %rax
	movl	%eax, 56(%rsp)
	movq	%rax, 88(%rsp)
	salq	$4, %rax
	movq	%rax, 64(%rsp)
	addq	(%rcx), %rax
	cmpb	$4, 8(%rax)
	jne	.L3130
	movl	8(%rax), %ebx
	testl	$261888, %ebx
	je	.L3131
	movl	120(%r13), %edx
	shrl	$8, %ebx
	movl	%r15d, %esi
	movq	%r13, %rdi
	call	re_string_context_at
	movl	%ebx, %ecx
	movl	%ebx, %edx
	andw	$1023, %cx
	testb	$4, %bl
	jne	.L3252
	andl	$8, %edx
	je	.L3134
	testb	$1, %al
	je	.L3134
	jmp	.L3130
	.p2align 4,,10
	.p2align 3
.L3131:
	movl	148(%r13), %r14d
	movq	112(%r13), %rax
	xorl	%esi, %esi
	movq	8(%r13), %r10
	movq	%rax, 96(%rsp)
	movl	%r14d, %edx
.L3136:
	cmpl	%edx, %esi
	jge	.L3139
	leal	(%rsi,%rdx), %eax
	movq	160(%r13), %rdi
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	salq	$5, %rax
	cmpl	%r15d, 4(%rdi,%rax)
	jge	.L3140
	jmp	.L3253
	.p2align 4,,10
	.p2align 3
.L3138:
	leal	(%rcx,%rsi), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %r8d
	salq	$5, %rdx
	cmpl	4(%rdi,%rdx), %r15d
	jg	.L3137
	movl	%eax, %ecx
.L3140:
	cmpl	%esi, %ecx
	jg	.L3138
.L3139:
	cmpl	%esi, %r14d
	jle	.L3141
	movslq	%esi, %rax
	salq	$5, %rax
	addq	160(%r13), %rax
	cmpl	%r15d, 4(%rax)
	jne	.L3141
	cmpl	$-1, %esi
	jne	.L3254
.L3141:
	movq	96(%rsp), %rax
	movl	172(%r13), %edi
	movq	(%rax), %rdx
	movq	64(%rsp), %rax
	testl	%edi, %edi
	movl	(%rdx,%rax), %eax
	movl	%eax, 120(%rsp)
	jle	.L3142
	movl	%r15d, 60(%rsp)
	movq	$0, 72(%rsp)
	movq	%r10, %r15
	movl	%r14d, 124(%rsp)
.L3167:
	movq	184(%r13), %rax
	movq	72(%rsp), %rcx
	movq	(%rax,%rcx,8), %rbx
	movl	120(%rsp), %ecx
	movslq	4(%rbx), %rax
	salq	$4, %rax
	cmpl	(%rdx,%rax), %ecx
	je	.L3255
.L3144:
	movq	72(%rsp), %rdi
	movl	%edi, %eax
	addq	$1, %rdi
	addl	$1, %eax
	cmpl	%eax, 172(%r13)
	movq	%rdi, 72(%rsp)
	jle	.L3246
	movq	96(%rsp), %rax
	movq	(%rax), %rdx
	jmp	.L3167
	.p2align 4,,10
	.p2align 3
.L3255:
	movl	20(%rbx), %esi
	movl	(%rbx), %r11d
	testl	%esi, %esi
	jle	.L3182
	movl	60(%rsp), %ecx
	movq	%r15, %r8
	xorl	%ebp, %ebp
	movl	%ecx, %r15d
	jmp	.L3153
	.p2align 4,,10
	.p2align 3
.L3149:
	movslq	%r11d, %rsi
	movslq	%r15d, %rdi
	movslq	%edx, %rdx
	addq	%r8, %rsi
	addq	%r8, %rdi
	movq	%r9, 48(%rsp)
	movl	%r11d, 40(%rsp)
	movq	%r8, 32(%rsp)
	call	memcmp@PLT
	testl	%eax, %eax
	movq	32(%rsp), %r8
	movl	40(%rsp), %r11d
	movq	48(%rsp), %r9
	jne	.L3239
.L3152:
	movl	60(%rsp), %r8d
	movl	56(%rsp), %ecx
	movq	%r9, %rdx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	get_subexp_sub.isra.25
	cmpl	$1, %eax
	movq	8(%r13), %r8
	ja	.L3128
	leal	1(%rbp), %eax
	addq	$1, %rbp
	cmpl	%eax, 20(%rbx)
	movl	%r14d, %r11d
	movl	%r12d, %r15d
	jle	.L3256
.L3153:
	movq	24(%rbx), %rax
	movl	%ebp, 80(%rsp)
	movq	(%rax,%rbp,8), %r9
	movl	4(%r9), %r14d
	movl	%r14d, %edx
	subl	%r11d, %edx
	testl	%edx, %edx
	leal	(%rdx,%r15), %r12d
	jle	.L3152
	cmpl	%r12d, 44(%r13)
	jge	.L3149
	cmpl	%r12d, 64(%r13)
	movl	%edx, 40(%rsp)
	movq	%r9, 32(%rsp)
	jl	.L3239
	movl	%r12d, %esi
	movq	%r13, %rdi
	movl	%r11d, 48(%rsp)
	call	clean_state_log_if_needed
	testl	%eax, %eax
	jne	.L3128
	movq	8(%r13), %r8
	movl	40(%rsp), %edx
	movq	32(%rsp), %r9
	movl	48(%rsp), %r11d
	jmp	.L3149
	.p2align 4,,10
	.p2align 3
.L3253:
	movl	%ecx, %r8d
	movl	%edx, %ecx
	.p2align 4,,10
	.p2align 3
.L3137:
	leal	1(%r8), %esi
	movl	%ecx, %edx
	jmp	.L3136
.L3246:
	movl	124(%rsp), %r14d
	movl	60(%rsp), %r15d
.L3142:
	movq	88(%rsp), %rax
	movslq	%r14d, %r8
	movl	$0, 140(%rsp)
	salq	$5, %r8
	movl	56(%rsp), %ebp
	movq	%r8, %r12
	salq	$2, %rax
	cmpl	148(%r13), %r14d
	movq	%rax, 80(%rsp)
	leaq	140(%rsp), %rax
	movq	%rax, 72(%rsp)
	jl	.L3169
	jmp	.L3130
	.p2align 4,,10
	.p2align 3
.L3260:
	movq	64(%rsp), %rsi
	movq	40(%rdi), %rdx
	movq	8(%rdx,%rsi), %rdx
	movslq	(%rdx), %rdx
	salq	$4, %rdx
	leaq	(%rcx,%rdx), %rsi
	movq	%rsi, 32(%rsp)
.L3172:
	addl	%r15d, %ebx
	movl	120(%r13), %edx
	movq	%r13, %rdi
	subl	%eax, %ebx
	leal	-1(%rbx), %esi
	movslq	%ebx, %rbx
	call	re_string_context_at
	movq	104(%rsp), %rsi
	movl	%eax, %ecx
	movq	136(%r13), %rax
	leaq	0(,%rbx,8), %r10
	movl	$0, 56(%rsp)
	leaq	(%rax,%r10), %r11
	movq	(%rax,%rsi), %rax
	movq	(%r11), %rdx
	testq	%rax, %rax
	je	.L3173
	movl	12(%rax), %eax
	movl	%eax, 56(%rsp)
.L3173:
	testq	%rdx, %rdx
	je	.L3257
	movq	56(%rdx), %rsi
	movq	112(%rsp), %rdi
	movq	32(%rsp), %rdx
	movl	%ecx, 60(%rsp)
	movq	%r10, 48(%rsp)
	call	re_node_set_init_union
	testl	%eax, %eax
	movl	%eax, 140(%rsp)
	movq	48(%rsp), %r10
	movl	60(%rsp), %ecx
	jne	.L3258
	addq	136(%r13), %r10
	movq	112(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	%r10, 48(%rsp)
	call	re_acquire_state_context
	movq	48(%rsp), %r10
	movq	152(%rsp), %rdi
	movq	%rax, (%r10)
	call	free@PLT
	movq	136(%r13), %rdx
	cmpq	$0, (%rdx,%rbx,8)
	je	.L3251
.L3175:
	movl	40(%rsp), %eax
	testl	%eax, %eax
	jne	.L3170
	movq	104(%rsp), %rax
	movl	56(%rsp), %ebx
	movq	(%rdx,%rax), %rax
	cmpl	%ebx, 12(%rax)
	jg	.L3259
	.p2align 4,,10
	.p2align 3
.L3170:
	addl	$1, %r14d
	addq	$32, %r12
	cmpl	%r14d, 148(%r13)
	jle	.L3130
.L3169:
	movq	160(%r13), %rax
	addq	%r12, %rax
	cmpl	%ebp, (%rax)
	jne	.L3170
	cmpl	%r15d, 4(%rax)
	jne	.L3170
	movl	12(%rax), %ebx
	movl	8(%rax), %eax
	movq	16(%rsp), %rdi
	movl	%ebx, %esi
	subl	%eax, %esi
	movq	48(%rdi), %rcx
	movl	%esi, 40(%rsp)
	je	.L3260
	movq	16(%rsp), %rsi
	movq	24(%rsi), %rdx
	movq	80(%rsp), %rsi
	movslq	(%rdx,%rsi), %rdx
	salq	$4, %rdx
	addq	%rdx, %rcx
	movq	%rcx, 32(%rsp)
	jmp	.L3172
	.p2align 4,,10
	.p2align 3
.L3257:
	movq	32(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	72(%rsp), %rdi
	movq	%r11, 48(%rsp)
	call	re_acquire_state_context
	movq	48(%rsp), %r11
	movq	136(%r13), %rdx
	movq	%rax, (%r11)
	cmpq	$0, (%rdx,%rbx,8)
	jne	.L3175
.L3251:
	movl	140(%rsp), %eax
	testl	%eax, %eax
	je	.L3175
.L3128:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3259:
	movq	32(%rsp), %rbx
	movl	%r15d, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	call	check_subexp_matching_top
	testl	%eax, %eax
	movl	%eax, 140(%rsp)
	jne	.L3128
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	transit_state_bkref
	testl	%eax, %eax
	movl	%eax, 140(%rsp)
	je	.L3170
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3129:
	xorl	%eax, %eax
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3256:
	movq	%r8, %r15
.L3154:
	leal	1(%r14), %r11d
.L3145:
	cmpl	%r11d, 60(%rsp)
	jl	.L3144
	movslq	%r11d, %rax
	leaq	0(,%rax,8), %r14
	movq	%rax, 32(%rsp)
	movq	%r13, %rax
	movq	%r14, %r13
	movslq	%r12d, %r14
	movl	%r11d, %r12d
	movq	%r15, %r11
	movq	%rax, %r15
	.p2align 4,,10
	.p2align 3
.L3166:
	movl	%r12d, %eax
	subl	(%rbx), %eax
	testl	%eax, %eax
	jle	.L3155
	cmpl	%r14d, 44(%r15)
	jle	.L3156
	leal	1(%r14), %ebp
.L3157:
	movq	32(%rsp), %rax
	movzbl	-1(%r11,%rax), %eax
	cmpb	%al, (%r11,%r14)
	jne	.L3243
	movslq	%ebp, %r14
.L3155:
	movq	136(%r15), %rax
	movq	(%rax,%r13), %rax
	testq	%rax, %rax
	je	.L3158
	movl	12(%rax), %esi
	testl	%esi, %esi
	jle	.L3158
	movq	16(%rax), %rdx
	movq	96(%rsp), %rcx
	leal	-1(%rsi), %eax
	movl	120(%rsp), %edi
	movq	(%rcx), %rcx
	leaq	4(%rdx,%rax,4), %rsi
	jmp	.L3161
	.p2align 4,,10
	.p2align 3
.L3159:
	addq	$4, %rdx
	cmpq	%rdx, %rsi
	je	.L3158
.L3161:
	movslq	(%rdx), %rax
	movq	%rax, %rbp
	salq	$4, %rax
	addq	%rcx, %rax
	cmpb	$9, 8(%rax)
	jne	.L3159
	cmpl	(%rax), %edi
	jne	.L3159
	cmpl	$-1, %ebp
	je	.L3158
	movq	8(%rbx), %rsi
	movl	(%rbx), %ecx
	testq	%rsi, %rsi
	je	.L3261
.L3162:
	movq	%r11, 40(%rsp)
	movl	4(%rbx), %edx
	subq	$8, %rsp
	pushq	$9
	movl	%r12d, %r9d
	movl	%ebp, %r8d
	movq	%r15, %rdi
	call	check_arrival
	popq	%rdx
	cmpl	$1, %eax
	popq	%rcx
	movq	40(%rsp), %r11
	je	.L3158
	testl	%eax, %eax
	jne	.L3128
	movl	20(%rbx), %eax
	cmpl	16(%rbx), %eax
	je	.L3262
.L3163:
	movl	$24, %esi
	movl	$1, %edi
	call	calloc@PLT
	testq	%rax, %rax
	je	.L3184
	movslq	20(%rbx), %rsi
	movq	24(%rbx), %rcx
	movq	%r15, %rdi
	movl	60(%rsp), %r8d
	movq	%rax, (%rcx,%rsi,8)
	movq	%rsi, %rdx
	movl	56(%rsp), %ecx
	addl	$1, %edx
	movl	%ebp, (%rax)
	movl	%r12d, 4(%rax)
	movq	%rbx, %rsi
	movl	%edx, 20(%rbx)
	movq	%rax, %rdx
	call	get_subexp_sub.isra.25
	cmpl	$1, %eax
	movq	8(%r15), %r11
	ja	.L3128
.L3158:
	addl	$1, %r12d
	addq	$1, 32(%rsp)
	addq	$8, %r13
	cmpl	%r12d, 60(%rsp)
	jge	.L3166
.L3243:
	movq	%r15, %r13
	movq	%r11, %r15
	jmp	.L3144
	.p2align 4,,10
	.p2align 3
.L3261:
	movl	%r12d, %esi
	movl	$16, %edi
	movq	%r11, 48(%rsp)
	subl	%ecx, %esi
	movl	%ecx, 40(%rsp)
	addl	$1, %esi
	movslq	%esi, %rsi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rsi
	movq	%rax, 8(%rbx)
	movl	40(%rsp), %ecx
	movq	48(%rsp), %r11
	jne	.L3162
.L3184:
.L3165:
	movl	$12, %eax
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3258:
	movq	152(%rsp), %rdi
	call	free@PLT
	movl	140(%rsp), %eax
	jmp	.L3128
	.p2align 4,,10
	.p2align 3
.L3239:
	movl	80(%rsp), %eax
	cmpl	%eax, 20(%rbx)
	movl	%r15d, %ecx
	movq	%r8, %r15
	jg	.L3144
	testl	%eax, %eax
	movl	%ecx, %r12d
	je	.L3145
	movl	%r11d, %r14d
	jmp	.L3154
	.p2align 4,,10
	.p2align 3
.L3254:
	movl	56(%rsp), %edx
	jmp	.L3143
	.p2align 4,,10
	.p2align 3
.L3263:
	addq	$32, %rax
	cmpb	$0, -8(%rax)
	je	.L3141
.L3143:
	cmpl	(%rax), %edx
	jne	.L3263
	jmp	.L3142
.L3156:
	cmpl	%r14d, 64(%r15)
	jle	.L3243
	leal	1(%r14), %ebp
	movq	%r15, %rdi
	movl	%ebp, %esi
	call	extend_buffers
	testl	%eax, %eax
	jne	.L3128
	movq	8(%r15), %r11
	jmp	.L3157
.L3262:
	leal	1(%rax,%rax), %edx
	movq	24(%rbx), %rdi
	movslq	%edx, %rsi
	movl	%edx, 40(%rsp)
	salq	$3, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L3184
	movl	40(%rsp), %edx
	movq	%rax, 24(%rbx)
	movl	%edx, 16(%rbx)
	jmp	.L3163
.L3182:
	movl	60(%rsp), %r12d
	jmp	.L3145
	.size	transit_state_bkref, .-transit_state_bkref
	.p2align 4,,15
	.type	merge_state_with_log, @function
merge_state_with_log:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$56, %rsp
	movslq	56(%rsi), %r15
	movq	136(%rsi), %r14
	movq	%rdi, 8(%rsp)
	movq	112(%rsi), %r13
	leaq	0(,%r15,8), %rbp
	movq	%r15, %r12
	addq	%rbp, %r14
	cmpl	%r15d, 144(%rsi)
	jge	.L3265
	movq	%rdx, (%r14)
	movq	%rdx, %rbp
	movl	%r15d, 144(%rbx)
.L3266:
	movl	140(%r13), %eax
	testl	%eax, %eax
	jne	.L3281
.L3264:
	addq	$56, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3265:
	movq	(%r14), %rax
	testq	%rax, %rax
	je	.L3282
	testq	%rdx, %rdx
	movq	56(%rax), %rax
	je	.L3268
	movq	56(%rdx), %r14
	leaq	32(%rsp), %rdi
	movq	%rax, %rdx
	movq	%rdi, 16(%rsp)
	movq	%r14, %rsi
	call	re_node_set_init_union
	movq	8(%rsp), %rcx
	testl	%eax, %eax
	movl	%eax, (%rcx)
	jne	.L3273
	movl	56(%rbx), %eax
	movl	120(%rbx), %edx
	movq	%rbx, %rdi
	leal	-1(%rax), %esi
	call	re_string_context_at
	movq	136(%rbx), %r10
	movq	16(%rsp), %rdx
	movl	%eax, %ecx
	movq	8(%rsp), %rdi
	movq	%r13, %rsi
	addq	%rbp, %r10
	movq	%r10, 24(%rsp)
	call	re_acquire_state_context
	movq	24(%rsp), %r10
	testq	%r14, %r14
	movq	%rax, %rbp
	movq	%rax, (%r10)
	je	.L3266
	movq	40(%rsp), %rdi
	call	free@PLT
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3268:
	movl	120(%rsi), %edx
	leal	-1(%r15), %esi
	movq	%rbx, %rdi
	movdqu	(%rax), %xmm0
	movaps	%xmm0, 32(%rsp)
	call	re_string_context_at
	leaq	32(%rsp), %rdx
	movl	%eax, %ecx
	movq	%r13, %rsi
	movq	8(%rsp), %rdi
	call	re_acquire_state_context
	movq	%rax, %rbp
	movq	%rax, (%r14)
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3282:
	movq	%rdx, (%r14)
	movq	%rdx, %rbp
	jmp	.L3266
	.p2align 4,,10
	.p2align 3
.L3281:
	testq	%rbp, %rbp
	je	.L3273
	leaq	8(%rbp), %r14
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	call	check_subexp_matching_top
	movq	8(%rsp), %rcx
	testl	%eax, %eax
	movl	%eax, (%rcx)
	jne	.L3273
	testb	$64, 80(%rbp)
	je	.L3264
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	transit_state_bkref
	movq	8(%rsp), %rcx
	testl	%eax, %eax
	movl	%eax, (%rcx)
	jne	.L3273
	movq	136(%rbx), %rax
	movq	(%rax,%r15,8), %rbp
	jmp	.L3264
	.p2align 4,,10
	.p2align 3
.L3273:
	xorl	%ebp, %ebp
	jmp	.L3264
	.size	merge_state_with_log, .-merge_state_with_log
	.p2align 4,,15
	.type	sift_states_backward, @function
sift_states_backward:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movslq	20(%rsi), %rax
	movl	16(%rsi), %ebx
	movq	%rdi, 16(%rsp)
	movl	$4, %edi
	movl	%eax, 12(%rsp)
	movq	%rax, %r14
	salq	$3, %rax
	movq	%rax, 32(%rsp)
	movabsq	$4294967297, %rax
	movq	%rax, 80(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 88(%rsp)
	je	.L3303
	leaq	80(%rsp), %rcx
	movl	%ebx, (%rax)
	movl	%r14d, %edx
	movq	%r12, %rsi
	movq	%r15, %rdi
	movq	%rcx, 40(%rsp)
	call	update_cur_sifted_state
	testl	%eax, %eax
	jne	.L3285
	leaq	24(%r12), %rax
	testl	%r14d, %r14d
	movl	$0, 76(%rsp)
	movq	%r12, %r15
	movq	%rax, 64(%rsp)
	jle	.L3302
.L3286:
	movq	(%r15), %rdi
	movq	32(%rsp), %rax
	cmpq	$0, (%rdi,%rax)
	je	.L3326
	movl	$0, 76(%rsp)
.L3287:
	movq	16(%rsp), %rax
	movl	76(%rsp), %ebx
	cmpl	%ebx, 168(%rax)
	jl	.L3327
	movq	16(%rsp), %rax
	movq	32(%rsp), %rbx
	subl	$1, 12(%rsp)
	movl	$0, 84(%rsp)
	movq	136(%rax), %rax
	movq	-8(%rax,%rbx), %r13
	testq	%r13, %r13
	je	.L3291
	movl	28(%r13), %ecx
	movq	16(%rsp), %rax
	testl	%ecx, %ecx
	movq	112(%rax), %r14
	jle	.L3291
	movq	32(%r13), %rax
	movq	$4, 24(%rsp)
	xorl	%ebp, %ebp
	movslq	(%rax), %r12
	movq	(%r14), %rax
	movq	%r12, %r9
	movq	%r12, %rbx
	salq	$4, %r9
	leaq	(%rax,%r9), %rdx
	jmp	.L3292
	.p2align 4,,10
	.p2align 3
.L3298:
	movl	12(%rsp), %edx
	movq	16(%rsp), %rdi
	leaq	(%rax,%r9), %rsi
	call	check_node_accept
	testb	%al, %al
	je	.L3295
	movq	(%r15), %rax
	movq	32(%rsp), %rdi
	movq	(%rax,%rdi), %rax
	testq	%rax, %rax
	je	.L3295
	movq	24(%r14), %rdx
	movl	12(%rax), %edi
	leaq	16(%rax), %rsi
	movl	(%rdx,%r12,4), %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L3295
	movl	$1, %r10d
.L3297:
	movl	28(%r15), %edx
	testl	%edx, %edx
	je	.L3301
	movq	24(%r14), %rax
	movl	12(%rsp), %r9d
	movl	%ebx, %r8d
	movq	64(%rsp), %rsi
	movq	16(%rsp), %rdi
	movl	(%rax,%r12,4), %edx
	leal	(%r9,%r10), %ecx
	call	check_dst_limits
	testb	%al, %al
	jne	.L3295
.L3301:
	movq	40(%rsp), %rdi
	movl	%ebx, %esi
	call	re_node_set_insert
	testb	%al, %al
	je	.L3328
.L3295:
	addl	$1, %ebp
	cmpl	28(%r13), %ebp
	jge	.L3291
	movq	24(%rsp), %rcx
	movq	32(%r13), %rax
	movslq	(%rax,%rcx), %r12
	movq	(%r14), %rax
	addq	$4, %rcx
	movq	%rcx, 24(%rsp)
	movq	%r12, %r9
	movq	%r12, %rbx
	salq	$4, %r9
	leaq	(%rax,%r9), %rdx
.L3292:
	testb	$16, 10(%rdx)
	je	.L3298
	movq	16(%rsp), %rcx
	movl	20(%r15), %eax
	movl	%ebx, %edx
	movl	12(%rsp), %r8d
	movq	%r9, 48(%rsp)
	movq	112(%rcx), %r11
	movl	%eax, 72(%rsp)
	movq	(%r11), %rdi
	leaq	200(%r11), %rsi
	movq	%r11, 56(%rsp)
	call	check_node_accept_bytes.isra.24
	cmpl	$0, %eax
	movl	%eax, %r10d
	movq	48(%rsp), %r9
	jle	.L3296
	movl	12(%rsp), %eax
	addl	%r10d, %eax
	cmpl	%eax, 72(%rsp)
	jl	.L3297
	movq	(%r15), %rdx
	cltq
	movq	56(%rsp), %r11
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L3325
	movq	24(%r11), %rdx
	movl	12(%rax), %edi
	leaq	16(%rax), %rsi
	movq	%r9, 48(%rsp)
	movl	(%rdx,%r12,4), %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L3297
	movq	(%r14), %rax
	movq	48(%rsp), %r9
	jmp	.L3298
	.p2align 4,,10
	.p2align 3
.L3291:
	movq	40(%rsp), %rcx
	movl	12(%rsp), %edx
	movq	%r15, %rsi
	movq	16(%rsp), %rdi
	call	update_cur_sifted_state
	testl	%eax, %eax
	jne	.L3285
	movl	12(%rsp), %eax
	subq	$8, 32(%rsp)
	testl	%eax, %eax
	jne	.L3286
.L3302:
	xorl	%eax, %eax
	jmp	.L3285
	.p2align 4,,10
	.p2align 3
.L3296:
	jne	.L3297
.L3325:
	movq	(%r14), %rax
	jmp	.L3298
	.p2align 4,,10
	.p2align 3
.L3328:
	movl	$12, %eax
.L3285:
	movq	88(%rsp), %rdi
	movl	%eax, 12(%rsp)
	call	free@PLT
	movl	12(%rsp), %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3326:
	addl	$1, 76(%rsp)
	jmp	.L3287
.L3327:
	movq	32(%rsp), %rdx
	xorl	%esi, %esi
	call	memset@PLT
	movq	88(%rsp), %rdi
	call	free@PLT
	addq	$104, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L3303:
	addq	$104, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	sift_states_backward, .-sift_states_backward
	.p2align 4,,15
	.type	update_cur_sifted_state, @function
update_cur_sifted_state:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %rax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbx
	subq	$168, %rsp
	movq	112(%rax), %rbp
	movq	136(%rax), %rax
	movq	%rdi, (%rsp)
	movslq	%edx, %rdi
	movl	$0, 108(%rsp)
	leaq	0(,%rdi,8), %rcx
	movq	%rdi, %r15
	movq	%rdi, 16(%rsp)
	movq	(%rax,%rdi,8), %rdx
	movq	%rcx, 24(%rsp)
	movl	4(%rbx), %ecx
	testq	%rdx, %rdx
	je	.L3421
	leaq	8(%rdx), %rsi
	testl	%ecx, %ecx
	movq	%rsi, 8(%rsp)
	jne	.L3333
	movq	(%r12), %rdx
	movq	16(%rsp), %rcx
	movq	$0, (%rdx,%rcx,8)
.L3334:
	movq	16(%rsp), %rcx
	movq	(%rax,%rcx,8), %rdx
	xorl	%eax, %eax
	testb	$64, 80(%rdx)
	jne	.L3422
.L3329:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3333:
	leaq	112(%rsp), %rdi
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movl	$0, 112(%rsp)
	call	re_acquire_state
	movq	%rax, %r14
	movl	112(%rsp), %eax
	testl	%eax, %eax
	jne	.L3329
	movl	40(%r14), %edi
	leaq	40(%r14), %r13
	testl	%edi, %edi
	je	.L3423
.L3336:
	movq	8(%rsp), %rsi
	movq	%r13, %rdx
	movq	%rbx, %rdi
	call	re_node_set_add_intersect
	testl	%eax, %eax
	movl	%eax, 108(%rsp)
	jne	.L3329
	cmpl	$0, 28(%r12)
	jne	.L3339
.L3419:
	movq	24(%rsp), %r13
	addq	(%r12), %r13
.L3331:
	leaq	108(%rsp), %rdi
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	call	re_acquire_state
	movq	%rax, 0(%r13)
	movl	108(%rsp), %eax
	testl	%eax, %eax
	jne	.L3329
	cmpq	$0, 8(%rsp)
	je	.L3329
	movq	(%rsp), %rax
	movq	136(%rax), %rax
	jmp	.L3334
	.p2align 4,,10
	.p2align 3
.L3421:
	movq	24(%rsp), %r13
	addq	(%rsi), %r13
	testl	%ecx, %ecx
	jne	.L3379
	movq	$0, 0(%r13)
	xorl	%eax, %eax
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3422:
	movq	(%rsp), %rax
	xorl	%ebx, %ebx
	movl	148(%rax), %esi
	movl	%esi, %edx
.L3354:
	cmpl	%edx, %ebx
	jge	.L3357
	leal	(%rbx,%rdx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	movq	(%rsp), %rax
	sarl	%ecx
	movq	160(%rax), %r8
	movslq	%ecx, %rax
	salq	$5, %rax
	cmpl	4(%r8,%rax), %r15d
	jle	.L3358
	jmp	.L3424
	.p2align 4,,10
	.p2align 3
.L3356:
	leal	(%rbx,%rcx), %edx
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movslq	%eax, %rdx
	movl	%eax, %edi
	salq	$5, %rdx
	cmpl	4(%r8,%rdx), %r15d
	jg	.L3355
	movl	%eax, %ecx
.L3358:
	cmpl	%ecx, %ebx
	jl	.L3356
.L3357:
	cmpl	%ebx, %esi
	jle	.L3360
	movq	(%rsp), %rax
	movslq	%ebx, %rcx
	movq	%rcx, 72(%rsp)
	salq	$5, %rcx
	movq	%rcx, 88(%rsp)
	movq	160(%rax), %rax
	cmpl	4(%rax,%rcx), %r15d
	jne	.L3360
	cmpl	$-1, %ebx
	je	.L3360
	movq	8(%rsp), %rcx
	movq	(%rsp), %rax
	movq	$0, 112(%rsp)
	movl	4(%rcx), %edx
	movq	112(%rax), %rax
	testl	%edx, %edx
	jle	.L3360
	leaq	112(%rsp), %rcx
	movl	%r15d, 32(%rsp)
	movq	$0, 40(%rsp)
	movl	%ebx, 16(%rsp)
	movq	%rax, %r15
	movq	%rcx, 64(%rsp)
	jmp	.L3374
	.p2align 4,,10
	.p2align 3
.L3362:
	cmpb	$4, %dl
	je	.L3425
.L3363:
	movq	40(%rsp), %rbx
	movl	%ebx, %eax
	addq	$1, %rbx
	movq	%rbx, 40(%rsp)
	movq	8(%rsp), %rbx
	addl	$1, %eax
	cmpl	%eax, 4(%rbx)
	jle	.L3426
.L3374:
	movq	8(%rsp), %rax
	movq	40(%rsp), %rbx
	movq	(%r15), %rdx
	movq	8(%rax), %rax
	movslq	(%rax,%rbx,4), %rax
	movq	%rax, %rsi
	movq	%rax, %r13
	salq	$4, %rsi
	cmpl	16(%r12), %eax
	movq	%rsi, 48(%rsp)
	movzbl	8(%rdx,%rsi), %edx
	jne	.L3362
	movl	32(%rsp), %ebx
	cmpl	20(%r12), %ebx
	jne	.L3362
	jmp	.L3363
	.p2align 4,,10
	.p2align 3
.L3360:
	xorl	%eax, %eax
	jmp	.L3329
.L3424:
	movl	%ecx, %edi
	movl	%edx, %ecx
	.p2align 4,,10
	.p2align 3
.L3355:
	leal	1(%rdi), %ebx
	movl	%ecx, %edx
	jmp	.L3354
	.p2align 4,,10
	.p2align 3
.L3423:
	movslq	4(%rbx), %rax
	movl	$0, 44(%r14)
	leaq	0(,%rax,4), %rdi
	movl	%eax, 40(%r14)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 48(%r14)
	je	.L3381
	movl	4(%rbx), %esi
	xorl	%r14d, %r14d
	movl	$0, 112(%rsp)
	testl	%esi, %esi
	jg	.L3338
	jmp	.L3336
	.p2align 4,,10
	.p2align 3
.L3427:
	leal	1(%r14), %eax
	addq	$1, %r14
	cmpl	%eax, 4(%rbx)
	jle	.L3336
.L3338:
	movq	8(%rbx), %rax
	movq	%r13, %rdi
	movslq	(%rax,%r14,4), %rsi
	salq	$4, %rsi
	addq	56(%rbp), %rsi
	call	re_node_set_merge
	testl	%eax, %eax
	movl	%eax, 112(%rsp)
	je	.L3427
.L3381:
	movl	$12, %eax
	jmp	.L3329
	.p2align 4,,10
	.p2align 3
.L3379:
	movq	$0, 8(%rsp)
	jmp	.L3331
	.p2align 4,,10
	.p2align 3
.L3339:
	movq	(%rsp), %rax
	movq	160(%rax), %rax
	movq	%rax, 32(%rsp)
	jle	.L3340
	xorl	%r14d, %r14d
	.p2align 4,,10
	.p2align 3
.L3353:
	movq	32(%r12), %rax
	movslq	(%rax,%r14,4), %rdx
	salq	$5, %rdx
	addq	32(%rsp), %rdx
	cmpl	8(%rdx), %r15d
	jle	.L3341
	cmpl	4(%rdx), %r15d
	jg	.L3341
	movslq	(%rdx), %rcx
	movq	0(%rbp), %rax
	movl	4(%rbx), %esi
	salq	$4, %rcx
	cmpl	12(%rdx), %r15d
	movl	(%rax,%rcx), %r8d
	je	.L3342
	xorl	%r13d, %r13d
	testl	%esi, %esi
	jle	.L3341
	movq	%r12, 40(%rsp)
	movq	%rax, %rdx
	movl	%r8d, %r12d
	jmp	.L3343
	.p2align 4,,10
	.p2align 3
.L3352:
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, 4(%rbx)
	jle	.L3428
.L3351:
	movq	0(%rbp), %rdx
.L3343:
	movq	8(%rbx), %rax
	movslq	(%rax,%r13,4), %rax
	movq	%rax, %rsi
	salq	$4, %rax
	addq	%rdx, %rax
	movzbl	8(%rax), %edx
	subl	$8, %edx
	cmpl	$1, %edx
	ja	.L3352
	cmpl	(%rax), %r12d
	jne	.L3352
	movq	8(%rsp), %rcx
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	sub_epsilon_src_nodes
	testl	%eax, %eax
	jne	.L3329
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, 4(%rbx)
	jg	.L3351
	.p2align 4,,10
	.p2align 3
.L3428:
	movq	40(%rsp), %r12
.L3341:
	leal	1(%r14), %eax
	addq	$1, %r14
	cmpl	%eax, 28(%r12)
	jg	.L3353
.L3340:
	movl	$0, 108(%rsp)
	jmp	.L3419
	.p2align 4,,10
	.p2align 3
.L3342:
	testl	%esi, %esi
	jle	.L3341
	movq	8(%rbx), %r10
	leal	-1(%rsi), %edx
	movl	$-1, %r9d
	movl	%r9d, %esi
	movq	%r10, %rcx
	leaq	4(%r10,%rdx,4), %r11
	jmp	.L3346
	.p2align 4,,10
	.p2align 3
.L3344:
	cmpl	$9, %r13d
	jne	.L3345
	cmpl	(%rdx), %r8d
	cmove	%edi, %r9d
.L3345:
	addq	$4, %rcx
	cmpq	%rcx, %r11
	je	.L3429
.L3346:
	movslq	(%rcx), %rdx
	movq	%rdx, %rdi
	salq	$4, %rdx
	addq	%rax, %rdx
	movzbl	8(%rdx), %r13d
	cmpl	$8, %r13d
	jne	.L3344
	cmpl	(%rdx), %r8d
	cmove	%edi, %esi
	addq	$4, %rcx
	cmpq	%rcx, %r11
	jne	.L3346
.L3429:
	testl	%esi, %esi
	movl	%r9d, %r13d
	movl	%r9d, 40(%rsp)
	js	.L3347
	movq	8(%rsp), %rcx
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	sub_epsilon_src_nodes
	testl	%eax, %eax
	jne	.L3329
	testl	%r13d, %r13d
	js	.L3341
	movl	4(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L3341
	movq	8(%rbx), %r10
.L3378:
	xorl	%r13d, %r13d
	movq	%r12, 48(%rsp)
	movl	%r13d, %r12d
	movl	40(%rsp), %r13d
	jmp	.L3349
	.p2align 4,,10
	.p2align 3
.L3348:
	addl	$1, %r12d
	cmpl	4(%rbx), %r12d
	jge	.L3414
	movq	8(%rbx), %r10
.L3349:
	movslq	%r12d, %rax
	movl	%r13d, %edx
	movslq	(%r10,%rax,4), %r9
	movq	56(%rbp), %rax
	movq	%r9, %r10
	salq	$4, %r9
	addq	%r9, %rax
	movl	4(%rax), %edi
	leaq	8(%rax), %rsi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L3348
	addq	48(%rbp), %r9
	movl	4(%r9), %edi
	leaq	8(%r9), %rsi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L3348
	movq	8(%rsp), %rcx
	movq	%rbx, %rdx
	movl	%r10d, %esi
	movq	%rbp, %rdi
	call	sub_epsilon_src_nodes
	testl	%eax, %eax
	jne	.L3329
	subl	$1, %r12d
	jmp	.L3348
	.p2align 4,,10
	.p2align 3
.L3347:
	movl	40(%rsp), %eax
	testl	%eax, %eax
	jns	.L3378
	jmp	.L3341
	.p2align 4,,10
	.p2align 3
.L3414:
	movq	48(%rsp), %r12
	jmp	.L3341
	.p2align 4,,10
	.p2align 3
.L3425:
	movq	(%rsp), %rbx
	movq	88(%rsp), %r11
	salq	$2, %rax
	movq	%rax, 56(%rsp)
	xorl	%ebp, %ebp
	addq	160(%rbx), %r11
	movq	%r12, %rbx
	movq	%r11, %r12
	jmp	.L3373
	.p2align 4,,10
	.p2align 3
.L3432:
	movl	12(%r12), %eax
	subl	8(%r12), %eax
	movl	32(%rsp), %ecx
	testl	%eax, %eax
	leal	(%rcx,%rax), %r9d
	je	.L3365
	movq	24(%r15), %rax
	movq	56(%rsp), %rsi
	movl	(%rax,%rsi), %r10d
.L3366:
	cmpl	20(%rbx), %r9d
	jg	.L3390
	movq	(%rbx), %rdx
	movslq	%r9d, %rax
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L3390
	movl	12(%rax), %edi
	leaq	16(%rax), %rsi
	movl	%r10d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L3390
	leaq	24(%rbx), %rax
	movl	32(%rsp), %ecx
	movq	(%rsp), %rdi
	movl	%r10d, %r8d
	movl	%r13d, %edx
	movq	%rax, %rsi
	movq	%rax, 80(%rsp)
	call	check_dst_limits
	testb	%al, %al
	jne	.L3390
	cmpq	$0, 112(%rsp)
	je	.L3367
.L3370:
	movl	32(%rsp), %eax
	movl	%r14d, %esi
	movl	%r13d, 128(%rsp)
	movl	%eax, 132(%rsp)
	movq	64(%rsp), %rax
	leaq	24(%rax), %rdi
	call	re_node_set_insert
	testb	%al, %al
	je	.L3430
	movq	24(%rsp), %rsi
	movq	112(%rsp), %rax
	movq	(%rsp), %rdi
	movq	(%rax,%rsi), %r12
	movq	64(%rsp), %rsi
	call	sift_states_backward
	testl	%eax, %eax
	jne	.L3369
	movq	8(%rbx), %rsi
	movq	112(%rsp), %rdx
	testq	%rsi, %rsi
	je	.L3371
	movl	32(%rsp), %eax
	movq	%r15, %rdi
	leal	1(%rax), %ecx
	call	merge_state_array
	testl	%eax, %eax
	jne	.L3369
	movq	112(%rsp), %rdx
.L3371:
	movq	64(%rsp), %r11
	movq	24(%rsp), %rax
	movl	140(%rsp), %edi
	leaq	32(%r11), %rsi
	movq	%r12, (%rdx,%rax)
	movl	%r14d, %edx
	call	re_node_set_contains.isra.4
	subl	$1, %eax
	movl	%eax, %esi
	js	.L3372
	leaq	24(%r11), %rdi
	call	re_node_set_remove_at.part.5
.L3372:
	movq	72(%rsp), %rax
	movq	(%rsp), %rsi
	addq	%rbp, %rax
	salq	$5, %rax
	addq	160(%rsi), %rax
	.p2align 4,,10
	.p2align 3
.L3364:
	addq	$1, %rbp
	cmpb	$0, 24(%rax)
	leaq	32(%rax), %r12
	je	.L3431
.L3373:
	cmpl	(%r12), %r13d
	movl	16(%rsp), %eax
	leal	(%rax,%rbp), %r14d
	je	.L3432
.L3390:
	movq	%r12, %rax
	jmp	.L3364
	.p2align 4,,10
	.p2align 3
.L3365:
	movq	40(%r15), %rax
	movq	48(%rsp), %rdi
	movq	8(%rax,%rdi), %rax
	movl	(%rax), %r10d
	jmp	.L3366
	.p2align 4,,10
	.p2align 3
.L3431:
	movq	%rbx, %r12
	jmp	.L3363
.L3367:
	movq	32(%rbx), %rax
	movq	80(%rsp), %rsi
	movdqa	(%rbx), %xmm0
	movq	%rax, 144(%rsp)
	movq	64(%rsp), %rax
	movaps	%xmm0, 112(%rsp)
	leaq	24(%rax), %rdi
	movdqa	16(%rbx), %xmm0
	movaps	%xmm0, 128(%rsp)
	call	re_node_set_init_copy
	testl	%eax, %eax
	je	.L3370
.L3369:
	cmpq	$0, 112(%rsp)
	je	.L3329
.L3376:
	movq	144(%rsp), %rdi
	movl	%eax, (%rsp)
	call	free@PLT
	movl	(%rsp), %eax
	jmp	.L3329
.L3430:
	movl	$12, %eax
	jmp	.L3369
.L3426:
	cmpq	$0, 112(%rsp)
	je	.L3360
	xorl	%eax, %eax
	jmp	.L3376
	.size	update_cur_sifted_state, .-update_cur_sifted_state
	.p2align 4,,15
	.type	set_regs, @function
set_regs:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbx
	movq	%rsi, %r12
	subq	$152, %rsp
	movq	(%rdi), %rax
	testb	%r8b, %r8b
	movq	%rdx, -144(%rbp)
	movq	$0, -56(%rbp)
	movq	%rax, -112(%rbp)
	movabsq	$8589934592, %rax
	movq	%rax, -64(%rbp)
	je	.L3484
	movl	$64, %edi
	leaq	-64(%rbp), %rbx
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -56(%rbp)
	movq	%rbx, -104(%rbp)
	je	.L3554
.L3434:
	movq	-112(%rbp), %rax
	pxor	%xmm0, %xmm0
	movl	136(%rax), %ebx
	movq	-144(%rbp), %rax
	movaps	%xmm0, -80(%rbp)
	leaq	0(,%rax,8), %r14
	movq	%r14, %rdi
	call	__libc_alloca_cutoff
	cmpq	$4096, %r14
	jbe	.L3436
	testl	%eax, %eax
	je	.L3556
.L3436:
	leaq	30(%r14), %rax
	movb	$0, -117(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -160(%rbp)
.L3438:
	movq	-160(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	movq	-144(%rbp), %rax
	movl	0(%r13), %ecx
	movl	%eax, -116(%rbp)
	cltq
	movl	%ecx, -84(%rbp)
	salq	$3, %rax
	movq	%rax, -176(%rbp)
	leaq	-80(%rbp), %rax
	movq	%rax, -136(%rbp)
	addq	$8, %rax
	movq	%rax, -168(%rbp)
.L3440:
	leaq	-84(%rbp), %rax
	movq	%rax, -152(%rbp)
	jmp	.L3478
	.p2align 4,,10
	.p2align 3
.L3441:
	cmpl	$9, %esi
	je	.L3557
.L3442:
	cmpl	%ecx, %eax
	jne	.L3445
	cmpl	%ebx, 128(%r12)
	je	.L3558
.L3445:
	movq	112(%r12), %r14
	movq	(%r14), %rdi
	leaq	(%rdi,%r9), %rsi
	movzbl	8(%rsi), %eax
	testb	$8, %al
	jne	.L3559
	testb	$16, 10(%rsi)
	jne	.L3560
	cmpb	$4, %al
	je	.L3467
	movl	-84(%rbp), %ecx
.L3466:
	movl	%ecx, %edx
	movq	%r12, %rdi
	movl	%ecx, -128(%rbp)
	call	check_node_accept
	testb	%al, %al
	movl	-128(%rbp), %ecx
	je	.L3456
	movq	24(%r14), %rax
	movl	(%rax,%r15,4), %ebx
	leal	1(%rcx), %eax
.L3482:
	cmpq	$0, -104(%rbp)
	movl	%eax, -84(%rbp)
	je	.L3475
	cmpl	124(%r12), %eax
	jg	.L3476
	movq	136(%r12), %rdx
	cltq
	movq	(%rdx,%rax,8), %rax
	testq	%rax, %rax
	je	.L3476
	movl	12(%rax), %edi
	leaq	16(%rax), %rsi
	movl	%ebx, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L3476
.L3475:
	movl	$0, -76(%rbp)
.L3458:
	testl	%ebx, %ebx
	js	.L3477
	movl	-84(%rbp), %ecx
.L3478:
	movl	4(%r13), %eax
	cmpl	%ecx, %eax
	jl	.L3561
	movq	-112(%rbp), %rdi
	movslq	%ebx, %r15
	movq	%r15, %r9
	salq	$4, %r9
	movq	(%rdi), %rdx
	addq	%r9, %rdx
	movzbl	8(%rdx), %esi
	cmpl	$8, %esi
	jne	.L3441
	movl	(%rdx), %edx
	addl	$1, %edx
	cmpl	%edx, -116(%rbp)
	jle	.L3442
	movslq	%edx, %rdx
	leaq	0(%r13,%rdx,8), %rax
	movl	%ecx, (%rax)
	movl	$-1, 4(%rax)
	movl	4(%r13), %eax
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3559:
	addq	40(%r14), %r9
	movslq	-84(%rbp), %rdx
	movl	%ebx, %esi
	movq	136(%r12), %rax
	movq	-136(%rbp), %rdi
	movq	(%rax,%rdx,8), %r15
	movq	%r9, %r14
	call	re_node_set_insert
	testb	%al, %al
	je	.L3473
	movl	4(%r14), %eax
	testl	%eax, %eax
	jle	.L3456
	movl	12(%r15), %r11d
	leaq	16(%r15), %r10
	movq	8(%r14), %r15
	subl	$1, %eax
	movl	$-1, %ebx
	leaq	4(%r15,%rax,4), %r9
	jmp	.L3463
	.p2align 4,,10
	.p2align 3
.L3486:
	movl	%r14d, %ebx
.L3457:
	addq	$4, %r15
	cmpq	%r15, %r9
	je	.L3458
.L3463:
	movl	(%r15), %r14d
	movq	%r10, %rsi
	movl	%r11d, %edi
	movl	%r14d, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	je	.L3457
	cmpl	$-1, %ebx
	je	.L3486
	movq	-168(%rbp), %rsi
	movl	-76(%rbp), %edi
	movl	%ebx, %edx
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L3487
	movq	-104(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L3458
	movl	(%rcx), %r15d
	movl	-84(%rbp), %r9d
	leal	1(%r15), %eax
	cmpl	4(%rcx), %eax
	movl	%eax, (%rcx)
	je	.L3459
	movq	8(%rcx), %rax
.L3460:
	movslq	%r15d, %rdx
	salq	$5, %rdx
	leaq	(%rax,%rdx), %r15
	movl	%r14d, 4(%r15)
	movq	-176(%rbp), %r14
	movl	%r9d, (%r15)
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r15)
	je	.L3462
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	-136(%rbp), %rsi
	leaq	16(%r15), %rdi
	call	re_node_set_init_copy
	testl	%eax, %eax
	je	.L3458
.L3462:
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	jne	.L3454
.L3483:
	movq	-104(%rbp), %rdi
	call	free_fail_stack_return.part.20
	movl	$12, %eax
.L3433:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3557:
	movl	(%rdx), %edi
	leal	1(%rdi), %esi
	cmpl	%esi, -116(%rbp)
	jle	.L3442
	movslq	%esi, %rsi
	leaq	0(%r13,%rsi,8), %rax
	cmpl	%ecx, (%rax)
	jl	.L3562
	testb	$8, 10(%rdx)
	je	.L3444
	movq	-160(%rbp), %rdi
	cmpl	$-1, (%rdi,%rsi,8)
	jne	.L3563
.L3444:
	movl	%ecx, 4(%rax)
	movl	4(%r13), %eax
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3560:
	movl	-84(%rbp), %r8d
	leaq	200(%r14), %rsi
	movq	%r12, %rcx
	movl	%ebx, %edx
	movq	%r9, -128(%rbp)
	call	check_node_accept_bytes.isra.24
	testl	%eax, %eax
	movl	%eax, %r8d
	movl	-84(%rbp), %ecx
	jne	.L3472
	movq	-128(%rbp), %r9
	movq	(%r14), %rsi
	addq	%r9, %rsi
	jmp	.L3466
.L3469:
	testl	%r8d, %r8d
	je	.L3471
	movl	-84(%rbp), %ecx
	.p2align 4,,10
	.p2align 3
.L3472:
	movq	24(%r14), %rax
	movl	(%rax,%r15,4), %ebx
	leal	(%rcx,%r8), %eax
	jmp	.L3482
	.p2align 4,,10
	.p2align 3
.L3467:
	movl	(%rsi), %eax
	addl	$1, %eax
	cmpl	%eax, -116(%rbp)
	jle	.L3468
	cltq
	leaq	0(%r13,%rax,8), %rax
	movl	4(%rax), %edx
	movslq	(%rax), %rax
	movl	%edx, %r8d
	subl	%eax, %r8d
	cmpq	$0, -104(%rbp)
	je	.L3469
	cmpl	$-1, %eax
	je	.L3476
	cmpl	$-1, %edx
	je	.L3476
	testl	%r8d, %r8d
	je	.L3471
	movl	-84(%rbp), %ecx
	movl	44(%r12), %edx
	movq	8(%r12), %rdi
	subl	%ecx, %edx
	cmpl	%edx, %r8d
	jg	.L3476
	movslq	%ecx, %rsi
	movslq	%r8d, %rdx
	movl	%r8d, -184(%rbp)
	addq	%rdi, %rsi
	addq	%rax, %rdi
	movl	%ecx, -128(%rbp)
	call	memcmp@PLT
	testl	%eax, %eax
	movl	-128(%rbp), %ecx
	movl	-184(%rbp), %r8d
	je	.L3472
	.p2align 4,,10
	.p2align 3
.L3476:
	movq	-136(%rbp), %r8
	movl	-116(%rbp), %edx
	leaq	-84(%rbp), %rsi
	movq	-104(%rbp), %rdi
	movq	%r13, %rcx
	call	pop_fail_stack
	movl	-84(%rbp), %ecx
	movl	%eax, %ebx
	jmp	.L3440
	.p2align 4,,10
	.p2align 3
.L3558:
	cmpq	$0, -104(%rbp)
	je	.L3446
	movq	-144(%rbp), %rdx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3449:
	movl	0(%r13,%rax,8), %ecx
	testl	%ecx, %ecx
	js	.L3447
	cmpl	$-1, 4(%r13,%rax,8)
	je	.L3448
.L3447:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L3449
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	jne	.L3564
.L3450:
	movq	-104(%rbp), %rdi
	call	free_fail_stack_return.part.20
	xorl	%eax, %eax
	jmp	.L3433
	.p2align 4,,10
	.p2align 3
.L3448:
	movq	-136(%rbp), %r8
	movl	-116(%rbp), %edx
	movq	%r13, %rcx
	movq	-152(%rbp), %rsi
	movq	-104(%rbp), %rdi
	call	pop_fail_stack
	movslq	%eax, %r15
	movq	%r15, %r9
	movq	%r15, %rbx
	salq	$4, %r9
	jmp	.L3445
	.p2align 4,,10
	.p2align 3
.L3468:
	cmpq	$0, -104(%rbp)
	jne	.L3476
.L3471:
	movq	-136(%rbp), %rdi
	movl	%ebx, %esi
	movq	%r9, -128(%rbp)
	call	re_node_set_insert
	testb	%al, %al
	je	.L3473
	movq	-128(%rbp), %r9
	movq	40(%r14), %rax
	movslq	-84(%rbp), %rdx
	movq	8(%rax,%r9), %rax
	movq	%r9, -184(%rbp)
	movl	%edx, -128(%rbp)
	movl	(%rax), %ebx
	movq	136(%r12), %rax
	movq	(%rax,%rdx,8), %rax
	movl	%ebx, %edx
	movl	12(%rax), %edi
	leaq	16(%rax), %rsi
	call	re_node_set_contains.isra.4
	testl	%eax, %eax
	jne	.L3458
	movq	-184(%rbp), %r9
	movq	(%r14), %rsi
	movl	-128(%rbp), %ecx
	addq	%r9, %rsi
	jmp	.L3466
.L3477:
	cmpl	$-2, %ebx
	je	.L3473
	.p2align 4,,10
	.p2align 3
.L3456:
	cmpq	$0, -104(%rbp)
	jne	.L3476
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	movl	$1, %eax
	je	.L3433
	movq	-160(%rbp), %rdi
	movl	%eax, -104(%rbp)
	call	free@PLT
	movl	-104(%rbp), %eax
	jmp	.L3433
	.p2align 4,,10
	.p2align 3
.L3487:
	movl	%r14d, %ebx
	jmp	.L3458
	.p2align 4,,10
	.p2align 3
.L3562:
	movq	-176(%rbp), %rdx
	movq	-160(%rbp), %rdi
	movq	%r13, %rsi
	movl	%ecx, 4(%rax)
	movq	%r9, -184(%rbp)
	movl	%ecx, -128(%rbp)
	call	memcpy@PLT
	movl	4(%r13), %eax
	movl	-128(%rbp), %ecx
	movq	-184(%rbp), %r9
	jmp	.L3442
	.p2align 4,,10
	.p2align 3
.L3561:
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	jne	.L3565
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3555
.L3566:
	call	free_fail_stack_return.part.20
.L3555:
	leaq	-40(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3473:
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	jne	.L3454
.L3455:
	cmpq	$0, -104(%rbp)
	jne	.L3483
.L3554:
	leaq	-40(%rbp), %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
.L3563:
	movq	-176(%rbp), %rdx
	movq	%rdi, %rsi
	movq	%r13, %rdi
	movl	%ecx, -184(%rbp)
	movq	%r9, -128(%rbp)
	call	memcpy@PLT
	movl	4(%r13), %eax
	movq	-128(%rbp), %r9
	movl	-184(%rbp), %ecx
	jmp	.L3442
.L3565:
	movq	-160(%rbp), %rdi
	call	free@PLT
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	jne	.L3566
	jmp	.L3555
.L3459:
	leal	(%rax,%rax), %esi
	movq	-104(%rbp), %rax
	movl	%r9d, -128(%rbp)
	movslq	%esi, %rsi
	movq	8(%rax), %rdi
	salq	$5, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L3461
	movq	-104(%rbp), %rcx
	movl	-128(%rbp), %r9d
	sall	4(%rcx)
	movq	%rax, 8(%rcx)
	jmp	.L3460
.L3484:
	movq	$0, -104(%rbp)
	jmp	.L3434
.L3446:
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	je	.L3555
	movq	-160(%rbp), %rdi
	call	free@PLT
	xorl	%eax, %eax
	jmp	.L3433
.L3461:
	movq	-72(%rbp), %rdi
	call	free@PLT
	cmpb	$0, -117(%rbp)
	je	.L3483
.L3454:
	movq	-160(%rbp), %rdi
	call	free@PLT
	jmp	.L3455
.L3556:
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -160(%rbp)
	je	.L3437
	movb	$1, -117(%rbp)
	jmp	.L3438
.L3564:
	movq	-160(%rbp), %rdi
	call	free@PLT
	jmp	.L3450
.L3437:
	movq	-104(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L3554
	call	free_fail_stack_return.part.20
	jmp	.L3554
	.size	set_regs, .-set_regs
	.p2align 4,,15
	.type	re_search_internal, @function
re_search_internal:
	pushq	%r15
	pushq	%r14
	movl	%r9d, %r14d
	pushq	%r13
	pushq	%r12
	movl	%r8d, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %ebp
	movl	$24, %ecx
	subq	$456, %rsp
	movq	(%rdi), %rax
	leaq	256(%rsp), %r13
	movq	%rdi, 72(%rsp)
	movq	%rsi, 120(%rsp)
	movq	%rdi, %rsi
	movl	%edx, 132(%rsp)
	movq	%rax, %rbx
	movq	%rax, 64(%rsp)
	movq	%r13, %rdi
	xorl	%eax, %eax
	rep stosq
	movq	32(%rsi), %rdi
	movq	%rbx, 368(%rsp)
	testq	%rdi, %rdi
	movq	%rdi, 104(%rsp)
	je	.L3568
	movzbl	56(%rsi), %edx
	testb	$8, %dl
	je	.L3725
	cmpl	%r8d, %ebp
	je	.L3725
	andl	$1, %edx
	cmove	%rdi, %rax
	movq	%rax, 104(%rsp)
.L3568:
	movq	72(%rsp), %rax
	movl	$0, 140(%rsp)
	movq	48(%rax), %rax
	cmpq	512(%rsp), %rax
	jnb	.L3569
	movl	512(%rsp), %esi
	leal	-1(%rsi), %edx
	subl	%eax, %edx
	movslq	%edx, %rax
	subq	%rax, 512(%rsp)
	movl	%edx, 140(%rsp)
.L3569:
	movq	72(%rsp), %rdi
	cmpq	$0, 16(%rdi)
	je	.L3732
	movq	64(%rsp), %rsi
	movq	72(%rsi), %rax
	testq	%rax, %rax
	je	.L3732
	movq	80(%rsi), %rdx
	testq	%rdx, %rdx
	je	.L3732
	movq	88(%rsi), %rcx
	testq	%rcx, %rcx
	je	.L3732
	cmpq	$0, 96(%rsi)
	je	.L3732
	movl	12(%rax), %eax
	testl	%eax, %eax
	jne	.L3571
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jne	.L3571
	movl	12(%rcx), %eax
	testl	%eax, %eax
	je	.L3572
	cmpb	$0, 56(%rdi)
	js	.L3571
.L3572:
	testl	%ebp, %ebp
	je	.L3760
	testl	%r12d, %r12d
	movl	$1, 100(%rsp)
	jne	.L3567
.L3760:
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L3571:
	cmpq	$0, 512(%rsp)
	movl	$1, 40(%rsp)
	je	.L3909
.L3574:
	movq	64(%rsp), %r11
	movq	72(%rsp), %rax
	movl	132(%rsp), %edi
	movq	16(%r11), %rsi
	movq	40(%rax), %rbx
	movq	24(%rax), %rax
	movl	164(%r11), %edx
	leal	1(%rdi), %ecx
	movl	%edi, 320(%rsp)
	movl	%edi, 316(%rsp)
	movq	%rsi, (%rsp)
	movl	(%rsp), %esi
	shrq	$22, %rax
	movq	%rbx, 160(%rsp)
	movq	%rbx, 336(%rsp)
	andl	$1, %eax
	movl	%edx, 360(%rsp)
	movl	%edi, 328(%rsp)
	addl	$1, %esi
	movb	%al, 352(%rsp)
	movl	%edi, 324(%rsp)
	cmpl	%edx, %esi
	movq	%r13, %rdi
	cmovl	%edx, %esi
	cmpl	%ecx, %esi
	cmovg	%ecx, %esi
	testq	%rbx, %rbx
	movq	120(%rsp), %rcx
	setne	%bl
	orl	%ebx, %eax
	movb	%al, 355(%rsp)
	movzbl	160(%r11), %eax
	movq	%rcx, 256(%rsp)
	movl	%eax, %edx
	shrb	$3, %al
	shrb	$2, %dl
	andl	$1, %eax
	andl	$1, %edx
	movb	%al, 354(%rsp)
	movb	%dl, 353(%rsp)
	call	re_string_realloc_buffers
	testl	%eax, %eax
	movl	%eax, 100(%rsp)
	jne	.L3907
	movq	64(%rsp), %rsi
	leaq	168(%rsi), %rax
	movq	%rax, 344(%rsp)
	movzbl	160(%rsi), %eax
	movb	%al, (%rsp)
	shrb	$4, %al
	andl	$1, %eax
	cmpb	$0, 355(%rsp)
	movb	%al, 358(%rsp)
	jne	.L3717
	movq	64(%rsp), %rsi
	movq	120(%rsp), %rax
	cmpl	$1, 164(%rsi)
	movq	%rax, 264(%rsp)
	movl	132(%rsp), %eax
	jle	.L3716
.L3717:
	xorl	%eax, %eax
.L3716:
	movl	%eax, 300(%rsp)
	movl	%eax, 304(%rsp)
	movq	72(%rsp), %rax
	movl	%r14d, 328(%rsp)
	movl	%r14d, 324(%rsp)
	movl	$-1, 380(%rsp)
	movzbl	56(%rax), %eax
	shrb	$7, %al
	movb	%al, 357(%rsp)
	movq	64(%rsp), %rax
	movl	140(%rax), %eax
	leal	(%rax,%rax), %edx
	movl	%eax, (%rsp)
	movl	528(%rsp), %eax
	testl	%edx, %edx
	movl	%eax, 376(%rsp)
	jg	.L3578
.L3581:
	cmpq	$1, 512(%rsp)
	movl	%edx, 408(%rsp)
	movl	$1, 424(%rsp)
	movl	%edx, 432(%rsp)
	jbe	.L3910
.L3579:
	movl	308(%rsp), %eax
	cmpl	$2147483646, %eax
	ja	.L3908
	leal	1(%rax), %edi
	movslq	%edi, %rdi
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r10
	movq	%rax, 392(%rsp)
	je	.L3736
.L3585:
	movl	528(%rsp), %eax
	movl	%ebp, 196(%rsp)
	andl	$1, %eax
	cmpl	$1, %eax
	sbbl	%eax, %eax
	andl	$2, %eax
	addl	$4, %eax
	movl	%eax, 332(%rsp)
	xorl	%eax, %eax
	cmpl	%r12d, %ebp
	setle	%al
	leal	-1(%rax,%rax), %eax
	movl	%eax, 156(%rsp)
	movl	%r12d, %eax
	cmovle	%ebp, %eax
	movl	%eax, 136(%rsp)
	movl	%r12d, %eax
	cmovge	%ebp, %eax
	cmpq	$0, 104(%rsp)
	movl	%eax, 128(%rsp)
	movq	64(%rsp), %rax
	movl	164(%rax), %eax
	movl	%eax, 148(%rsp)
	movl	$8, %eax
	je	.L3587
	cmpl	$1, 148(%rsp)
	movl	$4, %ecx
	je	.L3588
	movq	72(%rsp), %rax
	testb	$64, 26(%rax)
	jne	.L3762
	cmpq	$0, 160(%rsp)
	jne	.L3762
.L3588:
	xorl	%edx, %edx
	cmpl	%r12d, %ebp
	movzbl	%bl, %eax
	setle	%dl
	orl	%ecx, %eax
	addl	%edx, %edx
	orl	%edx, %eax
.L3587:
	cmpl	128(%rsp), %ebp
	setg	%cl
	cmpl	136(%rsp), %ebp
	setl	%dl
	orb	%dl, %cl
	jne	.L3906
	leaq	196(%rsp), %rcx
	cmpl	%r12d, %ebp
	movl	$0, %edx
	cmovle	%rcx, %rdx
	testq	%rdx, %rdx
	movq	%rdx, 168(%rsp)
	setne	179(%rsp)
	subl	$4, %eax
	movl	%eax, 152(%rsp)
	movl	128(%rsp), %eax
	subl	$1, %eax
	movl	%eax, 180(%rsp)
	movq	120(%rsp), %rax
	subq	$1, %rax
	movq	%rax, 184(%rsp)
.L3696:
	movl	152(%rsp), %eax
	cmpl	$4, %eax
	ja	.L3894
	leaq	.L3594(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L3594:
	.long	.L3593-.L3594
	.long	.L3593-.L3594
	.long	.L3595-.L3594
	.long	.L3596-.L3594
	.long	.L3597-.L3594
	.text
	.p2align 4,,10
	.p2align 3
.L3909:
	movq	64(%rsp), %rax
	movl	140(%rax), %eax
	testl	%eax, %eax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 40(%rsp)
	jmp	.L3574
	.p2align 4,,10
	.p2align 3
.L3725:
	movq	$0, 104(%rsp)
	jmp	.L3568
.L3595:
	cmpl	%ebp, 128(%rsp)
	jle	.L3598
	movq	120(%rsp), %rdi
	movslq	%ebp, %rax
	movq	104(%rsp), %rsi
	movzbl	(%rdi,%rax), %eax
	cmpb	$0, (%rsi,%rax)
	jne	.L3597
	movl	180(%rsp), %ecx
	leal	1(%rbp), %eax
	movq	104(%rsp), %rsi
	movq	184(%rsp), %rdi
	cltq
	subl	%ebp, %ecx
	addq	%rax, %rcx
	jmp	.L3606
	.p2align 4,,10
	.p2align 3
.L3911:
	movzbl	1(%rdi,%rax), %edx
	addq	$1, %rax
	cmpb	$0, (%rsi,%rdx)
	jne	.L3888
.L3606:
	cmpq	%rax, %rcx
	movl	%eax, %ebp
	jne	.L3911
	movl	%ecx, 196(%rsp)
.L3598:
	cmpl	%ebp, 128(%rsp)
	jne	.L3597
	xorl	%eax, %eax
	movl	128(%rsp), %edi
	cmpl	%edi, 132(%rsp)
	jle	.L3608
	movq	120(%rsp), %rsi
	movslq	%edi, %rax
	movzbl	(%rsi,%rax), %eax
.L3608:
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L3609
	movzbl	(%rdi,%rax), %eax
.L3609:
	movq	104(%rsp), %rdi
	cmpb	$0, (%rdi,%rax)
	je	.L3906
	movl	128(%rsp), %ebp
	.p2align 4,,10
	.p2align 3
.L3597:
	movl	528(%rsp), %edx
	movl	%ebp, %esi
	movq	%r13, %rdi
	call	re_string_reconstruct
	testl	%eax, %eax
	movl	%eax, 100(%rsp)
	jne	.L3907
	cmpl	$1, 148(%rsp)
	je	.L3619
	movl	300(%rsp), %eax
	testl	%eax, %eax
	je	.L3619
	movq	272(%rsp), %rax
	cmpl	$-1, (%rax)
	je	.L3620
.L3619:
	movq	368(%rsp), %rbp
	movzbl	40(%rsp), %eax
	movq	$0, 400(%rsp)
	movl	$0, 424(%rsp)
	movl	$0, 200(%rsp)
	movq	72(%rbp), %rbx
	andl	$1, %eax
	movb	%al, 178(%rsp)
	movl	312(%rsp), %eax
	cmpb	$0, 80(%rbx)
	movl	%eax, (%rsp)
	js	.L3912
.L3621:
	movq	392(%rsp), %rax
	testq	%rax, %rax
	je	.L3747
	movslq	(%rsp), %rdx
	movq	%rbx, (%rax,%rdx,8)
	movl	140(%rbp), %eax
	testl	%eax, %eax
	jne	.L3913
.L3747:
	movzbl	179(%rsp), %eax
	movb	%al, 32(%rsp)
	movzbl	80(%rbx), %eax
.L3626:
	testb	$16, %al
	movl	$-1, 44(%rsp)
	movl	$0, 96(%rsp)
	jne	.L3914
.L3630:
	movq	168(%rsp), %rax
	movslq	312(%rsp), %r8
	movq	%rax, 88(%rsp)
	leaq	200(%rsp), %rax
	movq	%rax, 48(%rsp)
	leaq	208(%rsp), %rax
	movq	%rax, 80(%rsp)
	leaq	204(%rsp), %rax
	movq	%rax, 112(%rsp)
	jmp	.L3673
	.p2align 4,,10
	.p2align 3
.L3662:
	cmpq	%r12, %rbx
	movslq	312(%rsp), %r8
	sete	%al
	andb	%al, 32(%rsp)
	movzbl	80(%r12), %eax
	cmove	(%rsp), %ebp
	testb	$16, %al
	movl	%ebp, (%rsp)
	je	.L3671
	testb	%al, %al
	js	.L3915
.L3672:
	movl	40(%rsp), %r10d
	movl	%r8d, 44(%rsp)
	testl	%r10d, %r10d
	je	.L3627
	movl	40(%rsp), %eax
	movq	$0, 88(%rsp)
	movl	%eax, 96(%rsp)
.L3671:
	movq	%r12, %rbx
.L3673:
	cmpl	%r8d, 328(%rsp)
	jle	.L3664
	movl	308(%rsp), %eax
	leal	1(%r8), %ebp
	cmpl	%eax, %ebp
	jge	.L3916
.L3632:
	movl	300(%rsp), %eax
	cmpl	%eax, %ebp
	jge	.L3917
.L3634:
	testb	$32, 80(%rbx)
	jne	.L3918
.L3636:
	leal	1(%r8), %eax
	movl	%eax, 312(%rsp)
	movq	264(%rsp), %rax
	movzbl	(%rax,%r8), %r12d
.L3658:
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.L3654
	movq	(%rax,%r12,8), %r12
.L3655:
	cmpq	$0, 392(%rsp)
	je	.L3661
.L3652:
	movq	48(%rsp), %rdi
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	merge_state_with_log
	movq	%rax, %r12
.L3661:
	testq	%r12, %r12
	jne	.L3662
	movl	200(%rsp), %r12d
	movq	392(%rsp), %r10
	testl	%r12d, %r12d
	jne	.L3736
	testq	%r10, %r10
	je	.L3664
	movzbl	178(%rsp), %eax
	xorl	$1, %eax
	testb	%al, 96(%rsp)
	jne	.L3664
.L3669:
	movslq	312(%rsp), %rax
	movl	400(%rsp), %edi
	xorl	%esi, %esi
	movq	%rax, %rcx
	leaq	(%r10,%rax,8), %rdx
	jmp	.L3667
	.p2align 4,,10
	.p2align 3
.L3666:
	addq	$8, %rdx
	movl	%eax, %ecx
	movl	$1, %esi
	cmpq	$0, (%rdx)
	jne	.L3919
.L3667:
	leal	1(%rcx), %eax
	cmpl	%eax, %edi
	jge	.L3666
	testb	%sil, %sil
	jne	.L3920
.L3664:
	movq	88(%rsp), %rax
	testq	%rax, %rax
	je	.L3627
	movl	(%rsp), %esi
	addl	%esi, (%rax)
.L3627:
	movl	44(%rsp), %eax
	cmpl	$-1, %eax
	je	.L3676
	cmpl	$-2, %eax
	je	.L3908
	movl	44(%rsp), %eax
	movl	%eax, 380(%rsp)
	movq	72(%rsp), %rax
	testb	$16, 56(%rax)
	jne	.L3678
	cmpq	$1, 512(%rsp)
	jbe	.L3921
	movslq	44(%rsp), %rax
	movq	392(%rsp), %rbx
	movq	%r13, %rdi
	movq	%rbx, 8(%rsp)
	movq	(%rbx,%rax,8), %rsi
	movq	%rax, %rdx
	call	check_halt_state_context
	movl	%eax, 384(%rsp)
	movq	64(%rsp), %rax
	testb	$1, 160(%rax)
	jne	.L3723
	movq	64(%rsp), %rax
	movl	140(%rax), %r9d
	testl	%r9d, %r9d
	jne	.L3681
	jmp	.L3682
	.p2align 4,,10
	.p2align 3
.L3596:
	cmpl	%ebp, 128(%rsp)
	jle	.L3598
	movq	120(%rsp), %rsi
	movslq	%ebp, %rax
	movq	160(%rsp), %rdi
	movzbl	(%rsi,%rax), %eax
	movq	104(%rsp), %rsi
	movzbl	(%rdi,%rax), %eax
	cmpb	$0, (%rsi,%rax)
	jne	.L3597
	movl	180(%rsp), %ecx
	leal	1(%rbp), %eax
	movq	104(%rsp), %rsi
	movq	160(%rsp), %rdi
	movq	184(%rsp), %r8
	cltq
	subl	%ebp, %ecx
	addq	%rax, %rcx
	jmp	.L3604
	.p2align 4,,10
	.p2align 3
.L3603:
	movzbl	1(%r8,%rax), %edx
	addq	$1, %rax
	movzbl	(%rdi,%rdx), %edx
	cmpb	$0, (%rsi,%rdx)
	jne	.L3888
.L3604:
	cmpq	%rcx, %rax
	movl	%eax, %ebp
	jne	.L3603
	movl	%eax, 196(%rsp)
	jmp	.L3598
	.p2align 4,,10
	.p2align 3
.L3593:
	cmpl	%ebp, 136(%rsp)
	jg	.L3906
	movslq	%ebp, %rcx
	xorl	%esi, %esi
	addq	120(%rsp), %rcx
	movl	%ebp, %eax
	movq	104(%rsp), %r8
	movq	160(%rsp), %rdi
	movl	136(%rsp), %r10d
	movl	132(%rsp), %r9d
	jmp	.L3613
	.p2align 4,,10
	.p2align 3
.L3923:
	subl	$1, %eax
	subq	$1, %rcx
	movl	$1, %esi
	cmpl	%r10d, %eax
	jl	.L3922
.L3613:
	xorl	%edx, %edx
	cmpl	%r9d, %eax
	jge	.L3610
	movzbl	(%rcx), %edx
.L3610:
	testq	%rdi, %rdi
	je	.L3611
	movzbl	(%rdi,%rdx), %edx
.L3611:
	cmpb	$0, (%r8,%rdx)
	je	.L3923
	testb	%sil, %sil
	je	.L3597
	movl	%eax, 196(%rsp)
	movl	%eax, %ebp
	jmp	.L3597
	.p2align 4,,10
	.p2align 3
.L3907:
	movq	392(%rsp), %r10
.L3576:
	movq	%r10, %rdi
	call	free@PLT
	movq	64(%rsp), %rax
	movl	140(%rax), %ecx
	testl	%ecx, %ecx
	jne	.L3924
.L3715:
	movq	%r13, %rdi
	call	re_string_destruct
.L3567:
	movl	100(%rsp), %eax
	addq	$456, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3919:
	movq	48(%rsp), %rdi
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%eax, 312(%rsp)
	call	merge_state_with_log
	movl	200(%rsp), %r11d
	testl	%r11d, %r11d
	jne	.L3668
	testq	%rax, %rax
	jne	.L3900
	movq	392(%rsp), %r10
	jmp	.L3669
	.p2align 4,,10
	.p2align 3
.L3915:
	movl	%r8d, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	movl	%r8d, 8(%rsp)
	call	check_halt_state_context
	testl	%eax, %eax
	movslq	8(%rsp), %r8
	je	.L3671
	jmp	.L3672
	.p2align 4,,10
	.p2align 3
.L3916:
	cmpl	320(%rsp), %eax
	jge	.L3632
.L3633:
	leal	2(%r8), %esi
	movq	%r13, %rdi
	call	extend_buffers
	testl	%eax, %eax
	movl	%eax, 200(%rsp)
	jne	.L3908
	testb	$32, 80(%rbx)
	movslq	312(%rsp), %r8
	je	.L3636
	.p2align 4,,10
	.p2align 3
.L3918:
	movl	12(%rbx), %r14d
	movq	368(%rsp), %r15
	testl	%r14d, %r14d
	jle	.L3637
	leaq	200(%r15), %rax
	movl	%ebp, 144(%rsp)
	xorl	%r14d, %r14d
	movq	%rbx, %rbp
	movl	%r8d, %ebx
	movq	%rax, 56(%rsp)
	jmp	.L3653
	.p2align 4,,10
	.p2align 3
.L3928:
	testb	$1, %al
	je	.L3651
	andl	$8, %ecx
	jne	.L3651
.L3642:
	testb	$32, %dl
	je	.L3643
	testb	$2, %al
	je	.L3651
.L3643:
	andl	$128, %edx
	je	.L3639
	testb	$8, %al
	je	.L3651
	.p2align 4,,10
	.p2align 3
.L3639:
	movq	56(%rsp), %rsi
	movl	%ebx, %r8d
	movq	%r13, %rcx
	movl	%r9d, %edx
	movq	%r10, %rdi
	call	check_node_accept_bytes.isra.24
	testl	%eax, %eax
	je	.L3650
	movl	312(%rsp), %ebx
	movq	%r13, %rdi
	addl	%eax, %ebx
	cmpl	%eax, 424(%rsp)
	cmovge	424(%rsp), %eax
	movl	%ebx, %esi
	movl	%eax, 424(%rsp)
	call	clean_state_log_if_needed
	testl	%eax, %eax
	movl	%eax, 204(%rsp)
	jne	.L3645
	movq	24(%r15), %rax
	movq	392(%rsp), %r8
	movslq	(%rax,%r12,4), %rdx
	movslq	%ebx, %r12
	leaq	0(,%r12,8), %rcx
	addq	%rcx, %r8
	movq	(%r8), %rax
	salq	$4, %rdx
	addq	48(%r15), %rdx
	movq	%r8, 8(%rsp)
	testq	%rax, %rax
	je	.L3925
	movq	56(%rax), %rsi
	movq	80(%rsp), %rdi
	movq	%rcx, 8(%rsp)
	call	re_node_set_init_union
	testl	%eax, %eax
	movl	%eax, 204(%rsp)
	movq	8(%rsp), %rcx
	jne	.L3645
	movl	376(%rsp), %edx
	leal	-1(%rbx), %esi
	movq	%r13, %rdi
	movq	%rcx, 8(%rsp)
	call	re_string_context_at
	movq	8(%rsp), %rcx
	movq	392(%rsp), %rbx
	movq	%r15, %rsi
	movq	80(%rsp), %rdx
	movq	112(%rsp), %rdi
	addq	%rcx, %rbx
	movl	%eax, %ecx
	call	re_acquire_state_context
	movq	216(%rsp), %rdi
	movq	%rax, (%rbx)
	call	free@PLT
	movq	392(%rsp), %r10
	cmpq	$0, (%r10,%r12,8)
	je	.L3926
.L3650:
	movl	312(%rsp), %ebx
.L3651:
	leal	1(%r14), %eax
	addq	$1, %r14
	cmpl	%eax, 12(%rbp)
	jle	.L3927
.L3653:
	movq	16(%rbp), %rax
	movq	(%r15), %r10
	movslq	(%rax,%r14,4), %r12
	movq	%r12, %rax
	movq	%r12, %r9
	salq	$4, %rax
	addq	%r10, %rax
	testb	$16, 10(%rax)
	je	.L3651
	movl	8(%rax), %ecx
	testl	$261888, %ecx
	movl	%ecx, 8(%rsp)
	je	.L3639
	movl	376(%rsp), %edx
	movl	%ebx, %esi
	movq	%r13, %rdi
	movq	%r10, 24(%rsp)
	movl	%r12d, 20(%rsp)
	call	re_string_context_at
	movl	8(%rsp), %ecx
	movl	20(%rsp), %r9d
	movq	24(%rsp), %r10
	shrl	$8, %ecx
	movl	%ecx, %edx
	andw	$1023, %dx
	testb	$4, %cl
	jne	.L3928
	andl	$8, %ecx
	je	.L3642
	testb	$1, %al
	je	.L3642
	jmp	.L3651
	.p2align 4,,10
	.p2align 3
.L3654:
	movq	72(%rbx), %r15
	testq	%r15, %r15
	je	.L3656
	movl	312(%rsp), %eax
	movl	376(%rsp), %edx
	movq	%r13, %rdi
	leal	-1(%rax), %esi
	call	re_string_context_at
	testb	$1, %al
	je	.L3657
	movq	2048(%r15,%r12,8), %r12
	jmp	.L3655
	.p2align 4,,10
	.p2align 3
.L3925:
	movdqu	(%rdx), %xmm0
	leal	-1(%rbx), %esi
	movl	376(%rsp), %edx
	movq	%r13, %rdi
	movaps	%xmm0, 208(%rsp)
	call	re_string_context_at
	movq	%r15, %rsi
	movl	%eax, %ecx
	movq	80(%rsp), %rdx
	movq	112(%rsp), %rdi
	call	re_acquire_state_context
	movq	8(%rsp), %r8
	movq	392(%rsp), %r10
	movq	%rax, (%r8)
	cmpq	$0, (%r10,%r12,8)
	jne	.L3650
.L3926:
	movl	204(%rsp), %eax
	testl	%eax, %eax
	je	.L3650
	movq	%rbp, %rbx
	movl	144(%rsp), %ebp
	jmp	.L3649
	.p2align 4,,10
	.p2align 3
.L3917:
	cmpl	320(%rsp), %eax
	jge	.L3634
	jmp	.L3633
.L3694:
	movq	%r14, %rdi
	call	free@PLT
	.p2align 4,,10
	.p2align 3
.L3676:
	movq	%r13, %rdi
	call	match_ctx_clean
	movl	196(%rsp), %ebp
.L3620:
	addl	156(%rsp), %ebp
	cmpl	136(%rsp), %ebp
	movl	%ebp, 196(%rsp)
	jl	.L3906
	cmpl	128(%rsp), %ebp
	jle	.L3696
.L3906:
	movq	392(%rsp), %r10
	movl	$1, 100(%rsp)
	jmp	.L3576
	.p2align 4,,10
	.p2align 3
.L3732:
	movl	$1, 100(%rsp)
	jmp	.L3567
	.p2align 4,,10
	.p2align 3
.L3645:
	movq	%rbp, %rbx
	movq	392(%rsp), %r10
	movl	144(%rsp), %ebp
.L3649:
	testq	%r10, %r10
	movl	%eax, 200(%rsp)
	je	.L3736
.L3659:
	xorl	%r12d, %r12d
	jmp	.L3652
	.p2align 4,,10
	.p2align 3
.L3927:
	movslq	%ebx, %r8
	movq	%rbp, %rbx
	movl	144(%rsp), %ebp
.L3637:
	movl	$0, 200(%rsp)
	jmp	.L3636
	.p2align 4,,10
	.p2align 3
.L3657:
	movq	(%r15,%r12,8), %r12
	jmp	.L3655
	.p2align 4,,10
	.p2align 3
.L3678:
	movq	64(%rsp), %rax
	movl	140(%rax), %edx
	testl	%edx, %edx
	je	.L3682
.L3718:
	movslq	44(%rsp), %rax
	movq	392(%rsp), %rbx
	movq	%r13, %rdi
	movq	(%rbx,%rax,8), %rsi
	movq	%rax, %rdx
	call	check_halt_state_context
	movl	%eax, 384(%rsp)
.L3681:
	movq	%rbx, 8(%rsp)
.L3723:
	cmpl	$2147483646, 44(%rsp)
	movq	368(%rsp), %rax
	movl	384(%rsp), %r12d
	movq	%rax, (%rsp)
	ja	.L3684
	movl	44(%rsp), %r15d
	leal	1(%r15), %ebp
	movslq	%ebp, %rbx
	salq	$3, %rbx
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L3684
	movq	(%rsp), %rax
	movl	140(%rax), %r8d
	testl	%r8d, %r8d
	je	.L3685
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rax, 32(%rsp)
	je	.L3686
	leaq	208(%rsp), %rax
	movl	%ebp, 8(%rsp)
	movq	%rax, 24(%rsp)
.L3692:
	movslq	8(%rsp), %r8
	xorl	%esi, %esi
	movq	%rbx, %rdi
	leaq	0(,%r8,8), %rbp
	movq	%rbp, %rdx
	call	memset@PLT
	pxor	%xmm0, %xmm0
	movq	24(%rsp), %rax
	movq	%r14, 208(%rsp)
	movq	%rbx, 216(%rsp)
	movq	%r13, %rdi
	movl	%r12d, 224(%rsp)
	movl	%r15d, 228(%rsp)
	movups	%xmm0, 24(%rax)
	movq	%rax, %rsi
	call	sift_states_backward
	movq	240(%rsp), %rdi
	movl	%eax, 20(%rsp)
	call	free@PLT
	movl	20(%rsp), %eax
	testl	%eax, %eax
	jne	.L3687
	cmpq	$0, (%r14)
	jne	.L3688
	cmpq	$0, (%rbx)
	jne	.L3688
	leal	-1(%r15), %edx
	movslq	%edx, %rdx
	movq	%rdx, %rax
	negq	%rax
	leaq	-8(%rbp,%rax,8), %rax
	addq	392(%rsp), %rax
	jmp	.L3689
	.p2align 4,,10
	.p2align 3
.L3930:
	movq	-8(%rax,%rdx,8), %rsi
	testq	%rsi, %rsi
	je	.L3691
	testb	$16, 80(%rsi)
	jne	.L3929
.L3691:
	subq	$1, %rdx
.L3689:
	cmpq	$-1, %rdx
	movl	%edx, %r15d
	jne	.L3930
	movq	%r14, %rdi
	call	free@PLT
	movq	%rbx, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	match_ctx_clean
	movl	196(%rsp), %ebp
	jmp	.L3620
	.p2align 4,,10
	.p2align 3
.L3912:
	movl	376(%rsp), %edx
	leal	-1(%rax), %esi
	movq	%r13, %rdi
	call	re_string_context_at
	testb	$1, %al
	je	.L3622
	movq	80(%rbp), %rbx
.L3623:
	testq	%rbx, %rbx
	jne	.L3621
.L3908:
	movq	392(%rsp), %r10
	movl	$12, 100(%rsp)
	jmp	.L3576
.L3656:
	movq	368(%rsp), %rdi
	movq	%rbx, %rsi
	call	build_trtable
	testb	%al, %al
	jne	.L3658
	movq	392(%rsp), %r10
	movl	$12, 200(%rsp)
	testq	%r10, %r10
	jne	.L3659
	.p2align 4,,10
	.p2align 3
.L3736:
	movl	$12, 100(%rsp)
	jmp	.L3576
.L3578:
	movslq	%edx, %r14
	movl	%edx, (%rsp)
	movq	%r14, %rdi
	salq	$5, %rdi
	call	malloc@PLT
	leaq	0(,%r14,8), %rdi
	movq	%rax, %r15
	movq	%rax, 416(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 440(%rsp)
	je	.L3908
	testq	%r15, %r15
	movl	(%rsp), %edx
	jne	.L3581
	jmp	.L3908
.L3894:
	movq	%r13, (%rsp)
	movq	104(%rsp), %rbx
	movl	156(%rsp), %r14d
	movl	136(%rsp), %r15d
	movl	132(%rsp), %r12d
	movl	128(%rsp), %r13d
	jmp	.L3592
	.p2align 4,,10
	.p2align 3
.L3614:
	cmpl	%ebp, %r12d
	movq	%rbx, %rdx
	jle	.L3616
	movq	264(%rsp), %rdx
	movzbl	(%rdx,%rax), %edx
	addq	%rbx, %rdx
.L3616:
	cmpb	$0, (%rdx)
	jne	.L3895
	addl	%r14d, %ebp
	cmpl	%r15d, %ebp
	movl	%ebp, 196(%rsp)
	jl	.L3763
	cmpl	%r13d, %ebp
	jg	.L3763
.L3592:
	movl	%ebp, %eax
	subl	296(%rsp), %eax
	cmpl	%eax, 304(%rsp)
	ja	.L3614
	movl	528(%rsp), %edx
	movq	(%rsp), %rdi
	movl	%ebp, %esi
	call	re_string_reconstruct
	testl	%eax, %eax
	jne	.L3931
	movl	%ebp, %eax
	subl	296(%rsp), %eax
	jmp	.L3614
.L3622:
	testl	%eax, %eax
	je	.L3621
	movl	%eax, %edx
	andl	$6, %edx
	cmpl	$6, %edx
	je	.L3932
	testb	$2, %al
	je	.L3625
	movq	88(%rbp), %rbx
	jmp	.L3623
.L3921:
	movq	64(%rsp), %rax
	movl	140(%rax), %eax
	testl	%eax, %eax
	jne	.L3718
.L3682:
	cmpq	$0, 512(%rsp)
	je	.L3907
	cmpq	$1, 512(%rsp)
	je	.L3933
	movq	520(%rsp), %rdi
	movq	512(%rsp), %rsi
	movq	520(%rsp), %rax
	leaq	(%rdi,%rsi,8), %rdx
	addq	$8, %rax
.L3698:
	movl	$-1, 4(%rax)
	movl	$-1, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rdx
	jne	.L3698
	movq	520(%rsp), %rax
	movq	520(%rsp), %rsi
	movl	$0, (%rax)
	movl	380(%rsp), %eax
	movl	%eax, 4(%rsi)
	movq	72(%rsp), %rax
	testb	$16, 56(%rax)
	jne	.L3755
	cmpq	$1, 512(%rsp)
	je	.L3755
	movq	64(%rsp), %rax
	xorl	%r8d, %r8d
	testb	$1, 160(%rax)
	je	.L3700
	movl	140(%rax), %edi
	xorl	%r8d, %r8d
	testl	%edi, %edi
	setg	%r8b
.L3700:
	movq	520(%rsp), %rcx
	movq	512(%rsp), %rdx
	movq	%r13, %rsi
	movq	72(%rsp), %rdi
	call	set_regs
	testl	%eax, %eax
	jne	.L3701
	movq	520(%rsp), %rax
	movslq	(%rax), %rax
.L3699:
	movq	520(%rsp), %rsi
	movq	512(%rsp), %rbx
	movzbl	356(%rsp), %r11d
	movl	300(%rsp), %r10d
	movq	280(%rsp), %r9
	movl	304(%rsp), %r8d
	movl	196(%rsp), %edi
	leaq	(%rsi,%rbx,8), %rdx
	movq	%rsi, %rcx
	jmp	.L3708
.L3706:
	movslq	(%rcx), %rax
.L3708:
	cmpl	$-1, %eax
	je	.L3702
	testb	%r11b, %r11b
	movslq	4(%rcx), %rsi
	jne	.L3934
.L3703:
	addl	%edi, %eax
	addl	%edi, %esi
	movl	%eax, (%rcx)
	movl	%esi, 4(%rcx)
.L3702:
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.L3706
	movl	140(%rsp), %esi
	testl	%esi, %esi
	jle	.L3712
	movl	140(%rsp), %eax
	movq	520(%rsp), %rdi
	subl	$1, %eax
	addq	512(%rsp), %rax
	leaq	8(%rdi,%rax,8), %rax
.L3711:
	movl	$-1, (%rdx)
	movl	$-1, 4(%rdx)
	addq	$8, %rdx
	cmpq	%rdx, %rax
	jne	.L3711
.L3712:
	movq	64(%rsp), %rax
	movq	392(%rsp), %r10
	movq	208(%rax), %rcx
	testq	%rcx, %rcx
	je	.L3576
	movq	512(%rsp), %rax
	leaq	-1(%rax), %rsi
	xorl	%eax, %eax
	jmp	.L3710
.L3714:
	movslq	(%rcx,%rax,4), %rdx
	cmpl	%eax, %edx
	je	.L3713
	movq	520(%rsp), %rdi
	movl	8(%rdi,%rdx,8), %edx
	movl	%edx, 8(%rdi,%rax,8)
	movslq	(%rcx,%rax,4), %rdx
	movl	12(%rdi,%rdx,8), %edx
	movl	%edx, 12(%rdi,%rax,8)
.L3713:
	addq	$1, %rax
.L3710:
	cmpq	%rsi, %rax
	jne	.L3714
	jmp	.L3576
.L3762:
	xorl	%ecx, %ecx
	jmp	.L3588
.L3685:
	movl	44(%rsp), %eax
	pxor	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%r14, 208(%rsp)
	movq	$0, 216(%rsp)
	movl	%r12d, 224(%rsp)
	movl	%eax, 228(%rsp)
	leaq	208(%rsp), %rax
	movups	%xmm0, 232(%rsp)
	movq	%rax, %rsi
	call	sift_states_backward
	movq	240(%rsp), %rdi
	movl	%eax, (%rsp)
	call	free@PLT
	movl	(%rsp), %eax
	testl	%eax, %eax
	jne	.L3754
	cmpq	$0, (%r14)
	je	.L3694
.L3693:
	movq	392(%rsp), %rdi
	call	free@PLT
	movl	44(%rsp), %eax
	movq	%r14, 392(%rsp)
	movl	%r12d, 384(%rsp)
	movl	%eax, 380(%rsp)
	jmp	.L3682
.L3688:
	movl	8(%rsp), %ebp
	movq	(%rsp), %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movl	%r15d, 44(%rsp)
	movl	%ebp, %ecx
	call	merge_state_array
	movq	%rbx, %rdi
	movl	%eax, (%rsp)
	call	free@PLT
	movl	(%rsp), %eax
	testl	%eax, %eax
	je	.L3693
.L3754:
	movq	$0, 32(%rsp)
.L3687:
	movq	%r14, %rdi
	movl	%eax, (%rsp)
	call	free@PLT
	movq	32(%rsp), %rdi
	call	free@PLT
	movl	(%rsp), %eax
	cmpl	$1, %eax
	je	.L3676
	movq	392(%rsp), %r10
	movl	%eax, 100(%rsp)
	jmp	.L3576
	.p2align 4,,10
	.p2align 3
.L3924:
	movq	%r13, %rdi
	call	match_ctx_clean
	movq	440(%rsp), %rdi
	call	free@PLT
	movq	416(%rsp), %rdi
	call	free@PLT
	jmp	.L3715
.L3910:
	movq	64(%rsp), %rax
	testb	$2, 160(%rax)
	jne	.L3579
	jmp	.L3585
.L3684:
	movq	8(%rsp), %r10
	movl	$12, 100(%rsp)
	jmp	.L3576
.L3929:
	movq	%r13, %rdi
	call	check_halt_state_context
	movl	%eax, %r12d
	leal	1(%r15), %eax
	movl	%eax, 8(%rsp)
	jmp	.L3692
.L3895:
	movq	(%rsp), %r13
	jmp	.L3597
.L3763:
	movq	(%rsp), %r13
	movq	392(%rsp), %r10
	movl	$1, 100(%rsp)
	jmp	.L3576
.L3922:
	movl	%eax, 196(%rsp)
	jmp	.L3906
.L3668:
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L3662
	jmp	.L3664
	.p2align 4,,10
	.p2align 3
.L3900:
	movq	%rax, %r12
	jmp	.L3662
.L3914:
	cmpb	$0, 80(%rbx)
	js	.L3935
.L3631:
	movl	40(%rsp), %r15d
	movl	(%rsp), %eax
	testl	%r15d, %r15d
	movl	%eax, 44(%rsp)
	je	.L3627
	movl	40(%rsp), %eax
	movl	%eax, 96(%rsp)
	jmp	.L3630
.L3913:
	leaq	8(%rbx), %rbp
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	call	check_subexp_matching_top
	testl	%eax, %eax
	movl	%eax, 44(%rsp)
	movl	%eax, 200(%rsp)
	jne	.L3627
	movzbl	80(%rbx), %eax
	testb	$64, %al
	jne	.L3628
.L3629:
	movb	$0, 32(%rsp)
	jmp	.L3626
.L3935:
	movl	(%rsp), %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	check_halt_state_context
	testl	%eax, %eax
	je	.L3630
	jmp	.L3631
.L3932:
	movq	96(%rbp), %rbx
	jmp	.L3623
.L3625:
	testb	$4, %al
	je	.L3621
	movq	56(%rbx), %rdx
	leaq	200(%rsp), %rdi
	movl	%eax, %ecx
	movq	%rbp, %rsi
	call	re_acquire_state_context
	movq	%rax, %rbx
	jmp	.L3623
.L3686:
	movq	8(%rsp), %r10
	movq	%r14, %rdi
	movq	%r10, (%rsp)
	call	free@PLT
	movl	$12, 100(%rsp)
	movq	(%rsp), %r10
	jmp	.L3576
.L3931:
	movl	%eax, 100(%rsp)
	movq	(%rsp), %r13
	movq	392(%rsp), %r10
	jmp	.L3576
.L3628:
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	transit_state_bkref
	testl	%eax, %eax
	movl	%eax, 44(%rsp)
	movl	%eax, 200(%rsp)
	jne	.L3627
	movzbl	80(%rbx), %eax
	jmp	.L3629
.L3933:
	movq	520(%rsp), %rax
	movq	520(%rsp), %rsi
	movl	$0, (%rax)
	movl	380(%rsp), %eax
	movl	%eax, 4(%rsi)
	xorl	%eax, %eax
	jmp	.L3699
.L3934:
	cmpl	%eax, %r10d
	je	.L3757
	movl	(%r9,%rax,4), %eax
.L3704:
	cmpl	%esi, %r10d
	movl	%eax, (%rcx)
	je	.L3758
	movl	(%r9,%rsi,4), %esi
	jmp	.L3703
.L3755:
	xorl	%eax, %eax
	jmp	.L3699
.L3758:
	movl	%r8d, %esi
	jmp	.L3703
.L3757:
	movl	%r8d, %eax
	jmp	.L3704
.L3920:
	movl	%ecx, 312(%rsp)
	jmp	.L3664
.L3701:
	movq	392(%rsp), %r10
	movl	%eax, 100(%rsp)
	jmp	.L3576
.L3888:
	movl	%ebp, 196(%rsp)
	jmp	.L3597
	.size	re_search_internal, .-re_search_internal
	.p2align 4,,15
	.globl	__re_compile_pattern
	.type	__re_compile_pattern, @function
__re_compile_pattern:
	subq	$8, %rsp
	movq	re_syntax_options(%rip), %rcx
	movq	%rdi, %r8
	movq	%rdx, %rdi
	movq	%rcx, %rax
	shrq	$25, %rax
	andl	$1, %eax
	sall	$4, %eax
	movl	%eax, %edx
	movzbl	56(%rdi), %eax
	andl	$-17, %eax
	orl	%edx, %eax
	movq	%rsi, %rdx
	movq	%r8, %rsi
	orl	$-128, %eax
	movb	%al, 56(%rdi)
	call	re_compile_internal
	testl	%eax, %eax
	je	.L3937
	leaq	__re_error_msgid_idx(%rip), %rdx
	cltq
	leaq	__re_error_msgid(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	addq	(%rdx,%rax,8), %rsi
	movl	$5, %edx
	addq	$8, %rsp
	jmp	__dcgettext
	.p2align 4,,10
	.p2align 3
.L3937:
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__re_compile_pattern, .-__re_compile_pattern
	.weak	re_compile_pattern
	.set	re_compile_pattern,__re_compile_pattern
	.p2align 4,,15
	.globl	__re_set_syntax
	.type	__re_set_syntax, @function
__re_set_syntax:
	movq	re_syntax_options(%rip), %rax
	movq	%rdi, re_syntax_options(%rip)
	ret
	.size	__re_set_syntax, .-__re_set_syntax
	.weak	re_set_syntax
	.set	re_set_syntax,__re_set_syntax
	.p2align 4,,15
	.globl	__re_compile_fastmap
	.hidden	__re_compile_fastmap
	.type	__re_compile_fastmap, @function
__re_compile_fastmap:
	pushq	%r12
	pushq	%rbp
	xorl	%eax, %eax
	pushq	%rbx
	movq	32(%rdi), %rbp
	movq	%rdi, %rbx
	movq	(%rdi), %r12
	leaq	8(%rbp), %rdi
	movq	%rbp, %rcx
	movq	$0, 0(%rbp)
	movq	$0, 248(%rbp)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$256, %ecx
	shrl	$3, %ecx
	rep stosq
	movq	%rbp, %rcx
	movq	%rbx, %rdi
	movq	72(%r12), %rsi
	leaq	16(%rsi), %rdx
	addq	$12, %rsi
	call	re_compile_fastmap_iter.isra.23
	movq	80(%r12), %rax
	cmpq	%rax, 72(%r12)
	je	.L3941
	leaq	16(%rax), %rdx
	leaq	12(%rax), %rsi
	movq	%rbp, %rcx
	movq	%rbx, %rdi
	call	re_compile_fastmap_iter.isra.23
	movq	72(%r12), %rax
.L3941:
	movq	88(%r12), %rsi
	cmpq	%rax, %rsi
	je	.L3942
	leaq	16(%rsi), %rdx
	movq	%rbp, %rcx
	addq	$12, %rsi
	movq	%rbx, %rdi
	call	re_compile_fastmap_iter.isra.23
	movq	72(%r12), %rax
.L3942:
	movq	96(%r12), %rsi
	cmpq	%rax, %rsi
	je	.L3943
	leaq	16(%rsi), %rdx
	movq	%rbp, %rcx
	addq	$12, %rsi
	movq	%rbx, %rdi
	call	re_compile_fastmap_iter.isra.23
.L3943:
	orb	$8, 56(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__re_compile_fastmap, .-__re_compile_fastmap
	.weak	re_compile_fastmap
	.set	re_compile_fastmap,__re_compile_fastmap
	.p2align 4,,15
	.type	re_search_stub, @function
re_search_stub:
	pushq	%r15
	pushq	%r14
	leal	(%rcx,%r8), %r11d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	testl	%ecx, %ecx
	movq	(%rdi), %r13
	movl	136(%rsp), %eax
	movq	128(%rsp), %r14
	movl	%eax, 12(%rsp)
	js	.L3974
	cmpl	%edx, %ecx
	jg	.L3974
	cmpl	%edx, %r11d
	jg	.L3976
	testl	%r8d, %r8d
	js	.L3986
	cmpl	%r11d, %ecx
	jg	.L3976
.L3986:
	testl	%r11d, %r11d
	js	.L3977
	testl	%r8d, %r8d
	jns	.L3947
	cmpl	%r11d, %ecx
	movl	$0, %eax
	cmovle	%eax, %r11d
.L3947:
	movl	%ecx, %ebx
	movq	%rdi, %rbp
#APP
# 390 "regexec.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	leaq	216(%r13), %rdi
	testl	%eax, %eax
	movq	%rdi, 56(%rsp)
	jne	.L3950
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, 216(%r13)
# 0 "" 2
#NO_APP
.L3951:
	movzbl	56(%rbp), %eax
	movl	%eax, %ecx
	shrb	$5, %cl
	movl	%ecx, %edi
	andl	$3, %edi
	cmpl	%ebx, %r11d
	movl	%edi, 16(%rsp)
	jg	.L4000
.L3952:
	testb	$16, %al
	jne	.L3980
.L4002:
	testq	%r14, %r14
	je	.L3980
	andl	$6, %eax
	movq	48(%rbp), %r12
	cmpb	$4, %al
	je	.L4001
.L3954:
	addl	$1, %r12d
.L3998:
	movslq	%r12d, %rcx
	leaq	0(,%rcx,8), %rdi
.L3953:
	movl	%r9d, 52(%rsp)
	movl	%edx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	movl	%r11d, 32(%rsp)
	movq	%rcx, 24(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	movl	$-2, %r8d
	je	.L3955
	subq	$8, %rsp
	movq	%rbp, %rdi
	movl	24(%rsp), %eax
	pushq	%rax
	pushq	%r15
	movq	48(%rsp), %rcx
	pushq	%rcx
	movl	64(%rsp), %r11d
	movl	%ebx, %ecx
	movl	84(%rsp), %r9d
	movl	80(%rsp), %edx
	movq	72(%rsp), %rsi
	movl	%r11d, %r8d
	call	re_search_internal
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L3956
	cmpl	$1, %eax
	movl	$-1, %r8d
	je	.L3957
.L3971:
	movl	$-2, %r8d
.L3957:
	movq	%r15, %rdi
	movl	%r8d, 12(%rsp)
	call	free@PLT
	movl	12(%rsp), %r8d
.L3955:
#APP
# 454 "regexec.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L3973
	subl	$1, 216(%r13)
.L3945:
	addq	$72, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4000:
	cmpq	$0, 32(%rbp)
	je	.L3952
	testb	$8, %al
	jne	.L3952
	movq	%rbp, %rdi
	movl	%r9d, 48(%rsp)
	movl	%edx, 40(%rsp)
	movq	%rsi, 32(%rsp)
	movl	%r11d, 24(%rsp)
	call	__re_compile_fastmap
	movzbl	56(%rbp), %eax
	movl	48(%rsp), %r9d
	movl	40(%rsp), %edx
	movq	32(%rsp), %rsi
	movl	24(%rsp), %r11d
	testb	$16, %al
	je	.L4002
	.p2align 4,,10
	.p2align 3
.L3980:
	movl	$8, %edi
	movl	$1, %ecx
	movl	$1, %r12d
	xorl	%r14d, %r14d
	jmp	.L3953
	.p2align 4,,10
	.p2align 3
.L3956:
	testq	%r14, %r14
	je	.L3972
	movzbl	56(%rbp), %eax
	leal	1(%r12), %edx
	shrb	%al
	andl	$3, %eax
	je	.L4003
	cmpl	$1, %eax
	movl	$2, %ecx
	je	.L4004
.L3964:
	testl	%r12d, %r12d
	jle	.L3985
	leal	-1(%r12), %eax
	movq	8(%r14), %r8
	movq	16(%r14), %rdi
	movl	%r12d, %r9d
	leaq	4(,%rax,4), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L3968:
	movl	(%r15,%rax,2), %edx
	movl	%edx, (%r8,%rax)
	movl	4(%r15,%rax,2), %edx
	movl	%edx, (%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rsi
	jne	.L3968
.L3967:
	cmpl	%r9d, (%r14)
	jbe	.L3962
	leal	1(%r12), %eax
	movslq	%r12d, %rdx
	movq	16(%r14), %rdi
	cltq
	subq	%rax, %rdx
	salq	$2, %rdx
	addq	%rdx, %rdi
	addq	8(%r14), %rdx
	.p2align 4,,10
	.p2align 3
.L3970:
	movl	$-1, (%rdi,%rax,4)
	movl	%eax, %esi
	movl	$-1, (%rdx,%rax,4)
	addq	$1, %rax
	cmpl	%esi, (%r14)
	ja	.L3970
.L3962:
	leal	(%rcx,%rcx), %eax
	movzbl	56(%rbp), %ecx
	andl	$-7, %ecx
	orl	%eax, %ecx
	movb	%cl, 56(%rbp)
	andl	$6, %ecx
	je	.L3971
.L3972:
	cmpb	$0, 12(%rsp)
	movl	(%r15), %r8d
	je	.L3957
	movl	4(%r15), %r8d
	subl	%ebx, %r8d
	jmp	.L3957
	.p2align 4,,10
	.p2align 3
.L3976:
	movl	%edx, %r11d
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L4003:
	movslq	%edx, %rcx
	movl	%edx, 24(%rsp)
	salq	$2, %rcx
	movq	%rcx, %rdi
	movq	%rcx, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r14)
	movq	16(%rsp), %rcx
	movl	24(%rsp), %edx
	je	.L3999
	movq	%rcx, %rdi
	movl	%edx, 16(%rsp)
	movq	%rax, 24(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%r14)
	movl	16(%rsp), %edx
	movq	24(%rsp), %rsi
	je	.L4005
	movl	%edx, (%r14)
	movl	$1, %ecx
	jmp	.L3964
	.p2align 4,,10
	.p2align 3
.L3974:
	movl	$-1, %r8d
	jmp	.L3945
	.p2align 4,,10
	.p2align 3
.L3950:
	leaq	216(%r13), %rdi
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, (%rdi)
	je	.L3951
	movl	%r9d, 40(%rsp)
	movl	%edx, 32(%rsp)
	movq	%rsi, 24(%rsp)
	movl	%r11d, 16(%rsp)
	call	__lll_lock_wait_private
	movl	40(%rsp), %r9d
	movl	32(%rsp), %edx
	movq	24(%rsp), %rsi
	movl	16(%rsp), %r11d
	jmp	.L3951
	.p2align 4,,10
	.p2align 3
.L3973:
	xorl	%eax, %eax
#APP
# 454 "regexec.c" 1
	xchgl %eax, 216(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L3945
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	56(%rsp), %rdi
	movl	$202, %eax
#APP
# 454 "regexec.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L3945
	.p2align 4,,10
	.p2align 3
.L3977:
	xorl	%r11d, %r11d
	jmp	.L3947
	.p2align 4,,10
	.p2align 3
.L4001:
	movl	(%r14), %ecx
	cmpq	%r12, %rcx
	ja	.L3954
	testl	%ecx, %ecx
	movl	%ecx, %r12d
	jg	.L3998
	jmp	.L3980
	.p2align 4,,10
	.p2align 3
.L4004:
	cmpl	%edx, (%r14)
	movl	$1, %ecx
	jnb	.L3964
	movslq	%edx, %r8
	movq	8(%r14), %rdi
	movb	%cl, 40(%rsp)
	salq	$2, %r8
	movl	%edx, 32(%rsp)
	movq	%r8, %rsi
	movq	%r8, 24(%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	je	.L3999
	movq	24(%rsp), %r8
	movq	16(%r14), %rdi
	movq	%r8, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	16(%rsp), %r9
	movl	32(%rsp), %edx
	movzbl	40(%rsp), %ecx
	je	.L4006
	movq	%r9, 8(%r14)
	movq	%rax, 16(%r14)
	movl	%edx, (%r14)
	jmp	.L3964
	.p2align 4,,10
	.p2align 3
.L3985:
	xorl	%r9d, %r9d
	xorl	%r12d, %r12d
	jmp	.L3967
.L4005:
	movq	%rsi, %rdi
	call	free@PLT
.L3999:
	xorl	%ecx, %ecx
	jmp	.L3962
.L4006:
	movq	%r9, %rdi
	call	free@PLT
	xorl	%ecx, %ecx
	jmp	.L3962
	.size	re_search_stub, .-re_search_stub
	.p2align 4,,15
	.type	re_search_2_stub, @function
re_search_2_stub:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rcx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%r8d, %ebx
	movl	%ebx, %edi
	subq	$24, %rsp
	shrl	$31, %edi
	movl	96(%rsp), %eax
	movl	%edi, %ecx
	movl	104(%rsp), %r8d
	shrl	$31, %eax
	orb	%al, %cl
	jne	.L4013
	testl	%edx, %edx
	js	.L4013
	movl	%edx, %r14d
	addl	%ebx, %r14d
	jo	.L4013
	testl	%ebx, %ebx
	movl	%r9d, %ebp
	je	.L4015
	xorl	%r12d, %r12d
	testl	%edx, %edx
	movq	%rsi, (%rsp)
	movl	%edx, 8(%rsp)
	jne	.L4022
.L4014:
	movzbl	%r8b, %r8d
	movl	%r14d, %edx
	movl	%ebp, %ecx
	pushq	%r8
	pushq	96(%rsp)
	movq	%r13, %rsi
	movl	112(%rsp), %r9d
	movl	96(%rsp), %r8d
	movq	%r15, %rdi
	call	re_search_stub
	movq	%r12, %rdi
	movl	%eax, %ebx
	call	free@PLT
	popq	%rax
	popq	%rdx
.L4007:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4022:
	movslq	%r14d, %rdi
	movl	%r8d, 12(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L4013
	movslq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	movq	%r13, %rsi
	movslq	%ebx, %rdx
	movq	%rax, %rdi
	call	memcpy@PLT
	movq	%r12, %r13
	movl	12(%rsp), %r8d
	jmp	.L4014
	.p2align 4,,10
	.p2align 3
.L4013:
	movl	$-2, %ebx
	jmp	.L4007
	.p2align 4,,10
	.p2align 3
.L4015:
	movq	%rsi, %r13
	xorl	%r12d, %r12d
	jmp	.L4014
	.size	re_search_2_stub, .-re_search_2_stub
	.p2align 4,,15
	.globl	__regcomp
	.hidden	__regcomp
	.type	__regcomp, @function
__regcomp:
	movl	%edx, %eax
	pushq	%r14
	pushq	%r13
	andl	$1, %eax
	pushq	%r12
	pushq	%rbp
	cmpl	$1, %eax
	pushq	%rbx
	movq	%rdi, %rbx
	sbbq	%r14, %r14
	movq	$0, (%rdi)
	movq	$0, 8(%rdi)
	andl	$16601034, %r14d
	movq	$0, 16(%rdi)
	movl	$256, %edi
	movq	%rsi, %r12
	movl	%edx, %ebp
	addq	$242428, %r14
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 32(%rbx)
	je	.L4030
	movq	%rbp, %r13
	salq	$21, %r13
	andl	$4194304, %r13d
	orq	%r14, %r13
	testb	$4, %bpl
	jne	.L4033
	andb	$127, 56(%rbx)
.L4027:
	movzbl	56(%rbx), %edx
	shrl	$3, %ebp
	movq	%r12, %rdi
	andl	$1, %ebp
	movq	$0, 40(%rbx)
	sall	$4, %ebp
	andl	$-17, %edx
	orl	%edx, %ebp
	movb	%bpl, 56(%rbx)
	call	strlen
	movq	%r13, %rcx
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	re_compile_internal
	cmpl	$16, %eax
	movl	%eax, %ebp
	je	.L4031
	testl	%eax, %eax
	jne	.L4028
	movq	%rbx, %rdi
	call	__re_compile_fastmap
.L4023:
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4033:
	andq	$-65, %r13
	orb	$-128, 56(%rbx)
	orq	$256, %r13
	jmp	.L4027
	.p2align 4,,10
	.p2align 3
.L4031:
	movl	$8, %ebp
.L4028:
	movq	32(%rbx), %rdi
	call	free@PLT
	movq	$0, 32(%rbx)
	jmp	.L4023
	.p2align 4,,10
	.p2align 3
.L4030:
	movl	$12, %ebp
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__regcomp, .-__regcomp
	.weak	regcomp
	.set	regcomp,__regcomp
	.p2align 4,,15
	.globl	__regerror
	.type	__regerror, @function
__regerror:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	cmpl	$16, %edi
	ja	.L4043
	leaq	__re_error_msgid_idx(%rip), %rax
	movslq	%edi, %rdi
	leaq	__re_error_msgid(%rip), %rsi
	movq	%rdx, %rbp
	movl	$5, %edx
	movq	%rcx, %r12
	addq	(%rax,%rdi,8), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext
	movq	%rax, %rdi
	movq	%rax, %r13
	call	strlen
	testq	%r12, %r12
	leaq	1(%rax), %rbx
	je	.L4034
	cmpq	%r12, %rbx
	movq	%rbx, %rdx
	ja	.L4044
.L4037:
	movq	%r13, %rsi
	movq	%rbp, %rdi
	call	memcpy@PLT
.L4034:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L4043:
	call	abort
	.p2align 4,,10
	.p2align 3
.L4044:
	leaq	-1(%r12), %rdx
	movb	$0, -1(%rbp,%r12)
	jmp	.L4037
	.size	__regerror, .-__regerror
	.weak	regerror
	.set	regerror,__regerror
	.p2align 4,,15
	.globl	__regfree
	.hidden	__regfree
	.type	__regfree, @function
__regfree:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L4046
	call	free_dfa_content
.L4046:
	movq	32(%rbx), %rdi
	movq	$0, (%rbx)
	movq	$0, 8(%rbx)
	call	free@PLT
	movq	40(%rbx), %rdi
	movq	$0, 32(%rbx)
	call	free@PLT
	movq	$0, 40(%rbx)
	popq	%rbx
	ret
	.size	__regfree, .-__regfree
	.weak	regfree
	.set	regfree,__regfree
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	leaq	re_comp_buf(%rip), %rdi
	jmp	__regfree
	.size	free_mem, .-free_mem
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC16:
	.string	"No previous regular expression"
	.text
	.p2align 4,,15
	.weak	re_comp
	.type	re_comp, @function
re_comp:
	testq	%rdi, %rdi
	movq	re_comp_buf(%rip), %rax
	je	.L4069
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rax, %rax
	movq	32+re_comp_buf(%rip), %rbp
	je	.L4056
	leaq	re_comp_buf(%rip), %rdi
	movq	$0, 32+re_comp_buf(%rip)
	call	__regfree
	pxor	%xmm0, %xmm0
	movaps	%xmm0, re_comp_buf(%rip)
	movaps	%xmm0, 16+re_comp_buf(%rip)
	movaps	%xmm0, 48+re_comp_buf(%rip)
	movaps	%xmm0, 32+re_comp_buf(%rip)
	movq	%rbp, 32+re_comp_buf(%rip)
.L4056:
	testq	%rbp, %rbp
	je	.L4070
.L4057:
	movq	%rbx, %rdi
	orb	$-128, 56+re_comp_buf(%rip)
	call	strlen
	movq	re_syntax_options(%rip), %rcx
	leaq	re_comp_buf(%rip), %rdi
	movq	%rax, %rdx
	movq	%rbx, %rsi
	call	re_compile_internal
	testl	%eax, %eax
	je	.L4055
	leaq	__re_error_msgid_idx(%rip), %rdx
	cltq
	leaq	__re_error_msgid(%rip), %rsi
	addq	(%rdx,%rax,8), %rsi
	movl	$5, %edx
.L4068:
	addq	$8, %rsp
	leaq	_libc_intl_domainname(%rip), %rdi
	popq	%rbx
	popq	%rbp
	jmp	__dcgettext
	.p2align 4,,10
	.p2align 3
.L4070:
	movl	$256, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 32+re_comp_buf(%rip)
	movl	$5, %edx
	leaq	247+__re_error_msgid(%rip), %rsi
	jne	.L4057
	jmp	.L4068
	.p2align 4,,10
	.p2align 3
.L4069:
	testq	%rax, %rax
	jne	.L4066
	leaq	.LC16(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	jmp	__dcgettext
	.p2align 4,,10
	.p2align 3
.L4055:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4066:
	xorl	%eax, %eax
	ret
	.size	re_comp, .-re_comp
	.p2align 4,,15
	.globl	__regexec
	.hidden	__regexec
	.type	__regexec, @function
__regexec:
	testl	$-8, %r8d
	jne	.L4082
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %rbp
	movl	%r8d, %ecx
	movl	%r8d, %ebx
	subq	$40, %rsp
	andl	$4, %ecx
	jne	.L4088
	movq	%rsi, %rdi
	movl	%ecx, 20(%rsp)
	movq	%rsi, 8(%rsp)
	call	strlen
	movl	20(%rsp), %ecx
	movq	8(%rsp), %rsi
	movl	%eax, %edx
.L4074:
	movq	(%r15), %r14
#APP
# 213 "regexec.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	216(%r14), %r13
	jne	.L4075
	movl	$1, %edi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edi, 216(%r14)
# 0 "" 2
#NO_APP
.L4076:
	testb	$16, 56(%r15)
	je	.L4077
	subq	$8, %rsp
	pushq	%rbx
	pushq	$0
	pushq	$0
.L4087:
	movl	%edx, %r8d
	movl	%edx, %r9d
	movq	%r15, %rdi
	call	re_search_internal
	addq	$32, %rsp
	movl	%eax, %r8d
#APP
# 220 "regexec.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4079
	subl	$1, 216(%r14)
.L4080:
	xorl	%eax, %eax
	testl	%r8d, %r8d
	setne	%al
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L4088:
	movl	0(%rbp), %ecx
	movl	4(%rbp), %edx
	jmp	.L4074
	.p2align 4,,10
	.p2align 3
.L4077:
	subq	$8, %rsp
	pushq	%rbx
	pushq	%rbp
	pushq	%r12
	jmp	.L4087
	.p2align 4,,10
	.p2align 3
.L4082:
	movl	$2, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4075:
	xorl	%eax, %eax
	movl	$1, %edi
	lock cmpxchgl	%edi, 0(%r13)
	je	.L4076
	movq	%r13, %rdi
	movq	%rsi, 24(%rsp)
	movl	%edx, 20(%rsp)
	movl	%ecx, 8(%rsp)
	call	__lll_lock_wait_private
	movq	24(%rsp), %rsi
	movl	20(%rsp), %edx
	movl	8(%rsp), %ecx
	jmp	.L4076
	.p2align 4,,10
	.p2align 3
.L4079:
	xorl	%eax, %eax
#APP
# 220 "regexec.c" 1
	xchgl %eax, 216(%r14)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L4080
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 220 "regexec.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L4080
	.size	__regexec, .-__regexec
	.weak	regexec
	.set	regexec,__regexec
	.p2align 4,,15
	.globl	__re_match
	.type	__re_match, @function
__re_match:
	subq	$8, %rsp
	movl	%edx, %r9d
	pushq	$1
	pushq	%r8
	xorl	%r8d, %r8d
	call	re_search_stub
	addq	$24, %rsp
	ret
	.size	__re_match, .-__re_match
	.weak	re_match
	.set	re_match,__re_match
	.p2align 4,,15
	.globl	__re_search
	.type	__re_search, @function
__re_search:
	subq	$8, %rsp
	pushq	$0
	pushq	%r9
	movl	%edx, %r9d
	call	re_search_stub
	addq	$24, %rsp
	ret
	.size	__re_search, .-__re_search
	.weak	re_search
	.set	re_search,__re_search
	.p2align 4,,15
	.globl	__re_match_2
	.type	__re_match_2, @function
__re_match_2:
	subq	$8, %rsp
	pushq	$1
	movl	32(%rsp), %eax
	pushq	%rax
	pushq	32(%rsp)
	pushq	$0
	call	re_search_2_stub
	addq	$40, %rsp
	ret
	.size	__re_match_2, .-__re_match_2
	.weak	re_match_2
	.set	re_match_2,__re_match_2
	.p2align 4,,15
	.globl	__re_search_2
	.type	__re_search_2, @function
__re_search_2:
	subq	$8, %rsp
	pushq	$0
	movl	40(%rsp), %eax
	pushq	%rax
	pushq	40(%rsp)
	movl	40(%rsp), %eax
	pushq	%rax
	call	re_search_2_stub
	addq	$40, %rsp
	ret
	.size	__re_search_2, .-__re_search_2
	.weak	re_search_2
	.set	re_search_2,__re_search_2
	.p2align 4,,15
	.globl	__re_set_registers
	.type	__re_set_registers, @function
__re_set_registers:
	testl	%edx, %edx
	jne	.L4100
	andb	$-7, 56(%rdi)
	movl	$0, (%rsi)
	movq	$0, 16(%rsi)
	movq	$0, 8(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L4100:
	movzbl	56(%rdi), %eax
	andl	$-7, %eax
	orl	$2, %eax
	movb	%al, 56(%rdi)
	movl	%edx, (%rsi)
	movq	%rcx, 8(%rsi)
	movq	%r8, 16(%rsi)
	ret
	.size	__re_set_registers, .-__re_set_registers
	.weak	re_set_registers
	.set	re_set_registers,__re_set_registers
	.p2align 4,,15
	.weak	re_exec
	.type	re_exec, @function
re_exec:
	movq	%rdi, %rsi
	leaq	re_comp_buf(%rip), %rdi
	subq	$8, %rsp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	call	__regexec
	testl	%eax, %eax
	sete	%al
	addq	$8, %rsp
	movzbl	%al, %eax
	ret
	.size	re_exec, .-re_exec
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	re_comp_buf
	.comm	re_comp_buf,64,32
	.section	.rodata
	.align 32
	.type	utf8_sb_map, @object
	.size	utf8_sb_map, 32
utf8_sb_map:
	.quad	-1
	.quad	-1
	.zero	16
	.comm	re_syntax_options,8,8
	.align 32
	.type	__re_error_msgid_idx, @object
	.size	__re_error_msgid_idx, 136
__re_error_msgid_idx:
	.quad	0
	.quad	8
	.quad	17
	.quad	44
	.quad	72
	.quad	101
	.quad	120
	.quad	143
	.quad	174
	.quad	192
	.quad	205
	.quad	229
	.quad	247
	.quad	264
	.quad	301
	.quad	337
	.quad	364
	.align 32
	.type	__re_error_msgid, @object
	.size	__re_error_msgid, 382
__re_error_msgid:
	.string	"Success"
	.string	"No match"
	.string	"Invalid regular expression"
	.string	"Invalid collation character"
	.string	"Invalid character class name"
	.string	"Trailing backslash"
	.string	"Invalid back reference"
	.string	"Unmatched [, [^, [:, [., or [="
	.string	"Unmatched ( or \\("
	.string	"Unmatched \\{"
	.string	"Invalid content of \\{\\}"
	.string	"Invalid range end"
	.string	"Memory exhausted"
	.string	"Invalid preceding regular expression"
	.string	"Premature end of regular expression"
	.string	"Regular expression too big"
	.string	"Unmatched ) or \\)"
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC12:
	.quad	72340172838076673
	.quad	72340172838076673
	.hidden	__lll_lock_wait_private
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__iswctype
	.hidden	abort
	.hidden	_nl_current_LC_CTYPE
	.hidden	__collseq_table_lookup
	.hidden	__btowc
	.hidden	strlen
	.hidden	_nl_current_LC_COLLATE
	.hidden	__towlower
	.hidden	strcmp
	.hidden	memset
	.hidden	memmove
	.hidden	__iswalnum
	.hidden	__libc_alloca_cutoff
	.hidden	__wcrtomb
	.hidden	__towupper
	.hidden	__mbrtowc
