	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawnattr_init
	.hidden	__posix_spawnattr_init
	.type	__posix_spawnattr_init, @function
__posix_spawnattr_init:
	movq	$0, (%rdi)
	movq	$0, 328(%rdi)
	movq	%rdi, %rcx
	leaq	8(%rdi), %rdi
	xorl	%eax, %eax
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$336, %ecx
	shrl	$3, %ecx
	rep stosq
	ret
	.size	__posix_spawnattr_init, .-__posix_spawnattr_init
	.weak	posix_spawnattr_init
	.set	posix_spawnattr_init,__posix_spawnattr_init
