.text
.globl __execve
.type __execve,@function
.align 1<<4
__execve:
	movl $59, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __execve,.-__execve
.weak execve
execve = __execve
