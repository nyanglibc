	.text
	.p2align 4,,15
	.globl	posix_spawnattr_getflags
	.type	posix_spawnattr_getflags, @function
posix_spawnattr_getflags:
	movzwl	(%rdi), %eax
	movw	%ax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	posix_spawnattr_getflags, .-posix_spawnattr_getflags
