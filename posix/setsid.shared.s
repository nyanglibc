.text
.globl __setsid
.type __setsid,@function
.align 1<<4
__setsid:
	movl $112, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __setsid,.-__setsid
.globl __GI___setsid
.set __GI___setsid,__setsid
.weak setsid
setsid = __setsid
.globl __GI_setsid
.set __GI_setsid,setsid
