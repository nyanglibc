	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
.LC1:
	.string	"/proc/self/fd/%d"
.LC2:
	.string	"/proc/self/fd"
#NO_APP
	.text
	.p2align 4,,15
	.globl	fexecve
	.type	fexecve, @function
fexecve:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbx
	subq	$176, %rsp
	testq	%rsi, %rsi
	sete	%dl
	testq	%rbx, %rbx
	sete	%al
	orb	%al, %dl
	jne	.L9
	testl	%edi, %edi
	movl	%edi, %r12d
	js	.L9
	movq	%rsi, %r14
	movq	%rsi, %rdx
	movl	$4096, %r8d
	movq	%rbx, %r10
	leaq	.LC0(%rip), %rsi
	movl	$322, %eax
#APP
# 43 "../sysdeps/unix/sysv/linux/fexecve.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	movq	__libc_errno@gottpoff(%rip), %r13
	movl	%fs:0(%r13), %ebp
.L6:
	cmpl	$38, %ebp
	jne	.L4
	movl	%r12d, %ecx
	leaq	.LC1(%rip), %rdx
	movq	%rsp, %r12
	movl	$27, %esi
	movq	%r12, %rdi
	xorl	%eax, %eax
	call	__GI___snprintf
	movq	%rbx, %rdx
	movq	%r14, %rsi
	movq	%r12, %rdi
	call	__execve
	leaq	32(%rsp), %rsi
	leaq	.LC2(%rip), %rdi
	movl	%fs:0(%r13), %ebx
	call	__GI___stat64
	testl	%eax, %eax
	je	.L7
	cmpl	$2, %fs:0(%r13)
	cmove	%ebp, %ebx
.L7:
	movl	%ebx, %fs:0(%r13)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L4:
	addq	$176, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %r13
	movl	%eax, %ebp
	negl	%ebp
	movl	%ebp, %fs:0(%r13)
	jmp	.L6
	.size	fexecve, .-fexecve
	.hidden	__execve
