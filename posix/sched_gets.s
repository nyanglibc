.text
.globl __sched_getscheduler
.type __sched_getscheduler,@function
.align 1<<4
__sched_getscheduler:
	movl $145, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_getscheduler,.-__sched_getscheduler
.weak sched_getscheduler
sched_getscheduler = __sched_getscheduler
