	.text
	.globl	__popcountdi2
	.p2align 4,,15
	.globl	__sched_cpucount
	.type	__sched_cpucount, @function
__sched_cpucount:
	andq	$-8, %rdi
	pushq	%r12
	pushq	%rbp
	leaq	(%rsi,%rdi), %rbp
	pushq	%rbx
	cmpq	%rbp, %rsi
	jnb	.L5
	movq	%rsi, %rbx
	xorl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$8, %rbx
	movq	-8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L3
	call	__popcountdi2@PLT
	addl	%eax, %r12d
.L3:
	cmpq	%rbx, %rbp
	ja	.L4
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%r12d, %r12d
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__sched_cpucount, .-__sched_cpucount
