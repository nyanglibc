	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__glob_pattern_p
	.type	__glob_pattern_p, @function
__glob_pattern_p:
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L12
	xorl	%edx, %edx
	cmpb	$91, %al
	je	.L4
.L27:
	jle	.L26
	cmpb	$92, %al
	je	.L6
	cmpb	$93, %al
	jne	.L23
	testb	$4, %dl
	jne	.L15
.L23:
	movzbl	1(%rdi), %eax
	addq	$1, %rdi
.L8:
	testb	%al, %al
	je	.L10
	cmpb	$91, %al
	jne	.L27
.L4:
	orl	$4, %edx
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L26:
	cmpb	$42, %al
	je	.L15
	cmpb	$63, %al
	jne	.L23
.L15:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	testl	%esi, %esi
	movzbl	1(%rdi), %eax
	leaq	1(%rdi), %rcx
	je	.L14
	testb	%al, %al
	jne	.L9
	orl	$2, %edx
.L10:
	xorl	%eax, %eax
	cmpl	$1, %edx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	movq	%rcx, %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movzbl	2(%rdi), %eax
	orl	$2, %edx
	addq	$2, %rdi
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%eax, %eax
	ret
	.size	__glob_pattern_p, .-__glob_pattern_p
	.weak	glob_pattern_p
	.set	glob_pattern_p,__glob_pattern_p
