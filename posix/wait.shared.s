	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wait
	.type	__wait, @function
__wait:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movl	$-1, %edi
	jmp	__GI___waitpid
	.size	__wait, .-__wait
	.weak	wait
	.set	wait,__wait
