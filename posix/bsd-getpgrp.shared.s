	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__bsd_getpgrp
	.type	__bsd_getpgrp, @function
__bsd_getpgrp:
	jmp	__GI___getpgid
	.size	__bsd_getpgrp, .-__bsd_getpgrp
