	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__times
	.type	__times, @function
__times:
	movl	$100, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/times.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %rdx
	jbe	.L2
	cmpq	$-14, %rax
	je	.L14
.L2:
	cmpq	$-1, %rax
	movl	$0, %edx
	cmovne	%rax, %rdx
.L1:
	movq	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	testq	%rdi, %rdi
	je	.L1
	movq	(%rdi), %rax
	movq	%rax, (%rdi)
	movq	8(%rdi), %rax
	movq	%rax, 8(%rdi)
	movq	16(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	24(%rdi), %rax
	movq	%rax, 24(%rdi)
	jmp	.L1
	.size	__times, .-__times
	.weak	times
	.set	times,__times
