.text
.globl __getgid
.type __getgid,@function
.align 1<<4
__getgid:
	movl $104, %eax
	syscall
	ret
.size __getgid,.-__getgid
.weak getgid
getgid = __getgid
