	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	__spawnix, @function
__spawnix:
.LFB72:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	xorl	%r14d, %r14d
	subq	$584, %rsp
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r9, 24(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	cmpq	$2147483646, %r14
	je	.L20
.L2:
	addq	$1, %r14
	cmpq	$0, -8(%r15,%r14,8)
	jne	.L4
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$131106, %ecx
	movq	24(%rax), %rax
	leaq	33279(%rax,%r14,8), %rbx
	negq	%rax
	andq	%rax, %rbx
	movq	_rtld_global@GOTPCREL(%rip), %rax
	movq	%rbx, %rsi
	movl	4016(%rax), %edx
	sall	$2, %edx
	andl	$4, %edx
	orl	$3, %edx
	call	__GI___mmap
	cmpq	$-1, %rax
	movq	%rax, %rbp
	je	.L21
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L6
	movq	104+__libc_pthread_functions(%rip), %rax
	leaq	44(%rsp), %rsi
	movl	$1, %edi
#APP
# 357 "../sysdeps/unix/sysv/linux/spawni.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L6:
	movq	8(%rsp), %rax
	testq	%r12, %r12
	movl	$0, 236(%rsp)
	movq	%rax, 176(%rsp)
	movq	648(%rsp), %rax
	movq	%rax, 184(%rsp)
	movq	16(%rsp), %rax
	movq	%rax, 192(%rsp)
	je	.L22
.L7:
	movq	24(%rsp), %rax
	movq	%r12, 200(%rsp)
	leaq	48(%rsp), %r12
	movq	%r15, 208(%rsp)
	movq	%r14, 216(%rsp)
	movl	$8, %r10d
	movq	%r12, %rdx
	leaq	sigall_set(%rip), %rsi
	xorl	%edi, %edi
	movq	%rax, 224(%rsp)
	movl	640(%rsp), %eax
	movl	%eax, 232(%rsp)
	movl	$14, %eax
#APP
# 71 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	__spawni_child(%rip), %rdi
	leaq	0(%rbp,%rbx), %rsi
	xorl	%eax, %eax
	movq	%r12, %rcx
	movl	$16657, %edx
	call	__GI___clone
	testl	%eax, %eax
	movl	%eax, %r15d
	jle	.L8
	movl	236(%rsp), %r14d
	testl	%r14d, %r14d
	jg	.L23
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI___munmap
	testl	%r14d, %r14d
	jne	.L10
.L24:
	testq	%r13, %r13
	je	.L10
	movl	%r15d, 0(%r13)
.L10:
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	44(%rsp), %edi
#APP
# 416 "../sysdeps/unix/sysv/linux/spawni.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L1:
	addq	$584, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%eax, %edi
	call	__GI___waitpid
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	__GI___munmap
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%eax, %r14d
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	negl	%r14d
	call	__GI___munmap
	testl	%r14d, %r14d
	je	.L24
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	240(%rsp), %rdi
	movq	%r12, %rax
	movl	$42, %ecx
	leaq	240(%rsp), %r12
	rep stosq
	jmp	.L7
.L20:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %r14d
	movl	$7, %fs:(%rax)
	jmp	.L1
.L21:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r14d
	jmp	.L1
.LFE72:
	.size	__spawnix, .-__spawnix
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/bin/sh"
	.text
	.p2align 4,,15
	.type	__spawni_child, @function
__spawni_child:
.LFB71:
	pushq	%rbp
	xorl	%eax, %eax
	movl	$19, %ecx
	xorl	%esi, %esi
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	leaq	-208(%rbp), %r13
	pushq	%r12
	pushq	%rbx
	leaq	-336(%rbp), %rdx
	movl	$1, %r14d
	movl	$1, %r15d
	subq	$344, %rsp
	movq	%rdi, -360(%rbp)
	movq	152(%rdi), %rbx
	movq	144(%rdi), %r12
	movq	%r13, %rdi
	rep stosq
	xorl	%edi, %edi
	call	__GI___sigprocmask
	movzwl	(%rbx), %edx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L118:
	testq	%rax, 8(%rbx)
	je	.L26
.L30:
	movq	$0, -208(%rbp)
.L27:
	xorl	%edx, %edx
	movq	%r13, %rsi
	movl	%r14d, %edi
	call	__GI___libc_sigaction
.L113:
	movzwl	(%rbx), %edx
.L28:
	addl	$1, %r14d
	cmpl	$65, %r14d
	je	.L117
.L31:
	leal	-1(%r14), %ecx
	movq	%r15, %rax
	salq	%cl, %rax
	testb	$4, %dl
	jne	.L118
.L26:
	testq	%rax, -336(%rbp)
	je	.L28
	leal	-32(%r14), %eax
	cmpl	$1, %eax
	jbe	.L119
	xorl	%esi, %esi
	movq	%r13, %rdx
	movl	%r14d, %edi
	call	__GI___libc_sigaction
	cmpq	$1, -208(%rbp)
	jne	.L30
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L117:
	movl	%edx, %eax
	andl	$48, %eax
	cmpw	$16, %ax
	je	.L120
	testb	$32, %dl
	jne	.L121
.L37:
	testb	$-128, %dl
	jne	.L122
.L35:
	testb	$2, %dl
	jne	.L123
.L39:
	andb	$1, %dl
	jne	.L124
.L41:
	testq	%r12, %r12
	je	.L43
	cmpl	$0, 4(%r12)
	jle	.L43
	leaq	-352(%rbp), %rax
	leaq	.L49(%rip), %r13
	xorl	%r15d, %r15d
	movb	$0, -361(%rbp)
	movq	%rax, -376(%rbp)
	.p2align 4,,10
	.p2align 3
.L57:
	movq	%r15, %rcx
	salq	$5, %rcx
	addq	8(%r12), %rcx
	cmpl	$4, (%rcx)
	movq	%rcx, %r14
	ja	.L47
	movl	(%rcx), %eax
	movslq	0(%r13,%rax,4), %rax
	addq	%r13, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L49:
	.long	.L48-.L49
	.long	.L50-.L49
	.long	.L51-.L49
	.long	.L52-.L49
	.long	.L53-.L49
	.text
	.p2align 4,,10
	.p2align 3
.L52:
	movq	8(%rcx), %rdi
	call	__chdir
	testl	%eax, %eax
	jne	.L115
	.p2align 4,,10
	.p2align 3
.L47:
	leal	1(%r15), %eax
	addq	$1, %r15
	cmpl	%eax, 4(%r12)
	jg	.L57
.L43:
	testb	$8, (%rbx)
	movq	-360(%rbp), %rsi
	je	.L59
	leaq	136(%rbx), %rsi
.L59:
	xorl	%edx, %edx
	movl	$2, %edi
	call	__GI___sigprocmask
	movq	-360(%rbp), %rbx
	movq	%rbx, %rax
	movq	176(%rbx), %rdx
	movq	160(%rbx), %rsi
	movq	128(%rbx), %rdi
	call	*136(%rax)
	testb	$2, 184(%rbx)
	movq	__libc_errno@gottpoff(%rip), %rbx
	je	.L36
	cmpl	$8, %fs:(%rbx)
	jne	.L36
	movq	-360(%rbp), %rdi
	movq	%rsp, %r12
	leaq	.LC0(%rip), %r8
	movq	168(%rdi), %rcx
	movq	160(%rdi), %rsi
	movq	128(%rdi), %rdi
	leaq	16(,%rcx,8), %rdx
	leaq	22(%rdx), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	7(%rsp), %rax
	shrq	$3, %rax
	subq	$1, %rcx
	leaq	0(,%rax,8), %r13
	movq	%r8, 0(,%rax,8)
	movq	%rdi, 8(,%rax,8)
	jg	.L125
	movq	$0, 16(,%rax,8)
.L61:
	movq	-360(%rbp), %rax
	movq	%r13, %rsi
	leaq	.LC0(%rip), %rdi
	movq	176(%rax), %rdx
	call	*136(%rax)
	movq	%r12, %rsp
	.p2align 4,,10
	.p2align 3
.L36:
	movl	%fs:(%rbx), %eax
	movq	-360(%rbp), %rbx
	movl	$10, %edx
	movl	$127, %edi
	testl	%eax, %eax
	cmove	%edx, %eax
	movl	%eax, 188(%rbx)
	call	__GI__exit
	.p2align 4,,10
	.p2align 3
.L51:
	movl	8(%rcx), %edi
	call	__GI___close_nocancel
	movl	28(%r14), %edx
	movl	24(%r14), %esi
	xorl	%eax, %eax
	movq	16(%r14), %rdi
	call	__GI___open_nocancel
	cmpl	$-1, %eax
	je	.L115
	movl	8(%r14), %esi
	cmpl	%eax, %esi
	je	.L47
	movl	%eax, %edi
	movl	%eax, -368(%rbp)
	call	__GI___dup2
	cmpl	%eax, 8(%r14)
	movl	-368(%rbp), %edx
	jne	.L115
	movl	%edx, %edi
	call	__GI___close_nocancel
	testl	%eax, %eax
	je	.L47
	.p2align 4,,10
	.p2align 3
.L115:
	movq	__libc_errno@gottpoff(%rip), %rbx
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L48:
	movl	8(%rcx), %edi
	call	__GI___close_nocancel
	testl	%eax, %eax
	je	.L47
	cmpb	$0, -361(%rbp)
	je	.L126
.L54:
	movslq	8(%r14), %rax
	testl	%eax, %eax
	js	.L115
	cmpq	-352(%rbp), %rax
	jnb	.L115
	movb	$1, -361(%rbp)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	movl	8(%rcx), %edi
	call	__fchdir
	testl	%eax, %eax
	je	.L47
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L50:
	movl	8(%rcx), %r9d
	movl	12(%rcx), %esi
	cmpl	%esi, %r9d
	je	.L127
	movl	%r9d, %edi
	call	__GI___dup2
	cmpl	%eax, 12(%r14)
	je	.L47
	jmp	.L115
.L124:
	call	__getuid
	orl	$-1, %edx
	movl	%eax, %esi
	movl	$117, %eax
	movl	%edx, %edi
#APP
# 189 "../sysdeps/unix/sysv/linux/spawni.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L116
	testq	%rax, %rax
	jne	.L115
	call	__getgid
	orl	$-1, %edx
	movl	%eax, %esi
	movl	$119, %eax
	movl	%edx, %edi
#APP
# 190 "../sysdeps/unix/sysv/linux/spawni.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L116
	testq	%rax, %rax
	je	.L41
	jmp	.L115
.L127:
	xorl	%edx, %edx
	movl	%r9d, %edi
	xorl	%eax, %eax
	movl	$1, %esi
	movl	%r9d, -368(%rbp)
	call	__GI___fcntl
	cmpl	$-1, %eax
	movl	-368(%rbp), %r9d
	je	.L115
	andl	$-2, %eax
	movl	$2, %esi
	movl	%r9d, %edi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	__GI___fcntl
	addl	$1, %eax
	jne	.L47
	jmp	.L115
.L122:
	call	__setsid
	testl	%eax, %eax
	js	.L115
	movzwl	(%rbx), %edx
	jmp	.L35
.L121:
	movl	268(%rbx), %esi
	leaq	264(%rbx), %rdx
	xorl	%edi, %edi
	call	__GI___sched_setscheduler
	addl	$1, %eax
	je	.L115
.L114:
	movzwl	(%rbx), %edx
	jmp	.L37
.L123:
	movl	4(%rbx), %esi
	xorl	%edi, %edi
	call	__GI___setpgid
	testl	%eax, %eax
	jne	.L115
	movzwl	(%rbx), %edx
	jmp	.L39
.L126:
	movq	-376(%rbp), %rsi
	movl	$7, %edi
	call	__GI___getrlimit64
	jmp	.L54
.L120:
	leaq	264(%rbx), %rsi
	xorl	%edi, %edi
	call	__GI___sched_setparam
	addl	$1, %eax
	jne	.L114
	jmp	.L115
.L125:
	leaq	16(%r13), %rdi
	subq	$16, %rdx
	addq	$8, %rsi
	call	__GI_memcpy@PLT
	jmp	.L61
.L116:
	movq	__libc_errno@gottpoff(%rip), %rbx
	negl	%eax
	movl	%eax, %fs:(%rbx)
	jmp	.L36
.L119:
	movq	%r15, -208(%rbp)
	jmp	.L27
.LFE71:
	.size	__spawni_child, .-__spawni_child
	.p2align 4,,15
	.globl	__spawni
	.hidden	__spawni
	.type	__spawni, @function
__spawni:
.LFB73:
	subq	$8, %rsp
	leaq	__execve(%rip), %r11
	leaq	__execvpex(%rip), %rax
	movl	16(%rsp), %r10d
	testb	$1, %r10b
	cmove	%r11, %rax
	pushq	%rax
	pushq	%r10
	call	__spawnix
	addq	$24, %rsp
	ret
.LFE73:
	.size	__spawni, .-__spawni
	.section	.rodata
	.align 32
	.type	sigall_set, @object
	.size	sigall_set, 128
sigall_set:
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.hidden	__execvpex
	.hidden	__execve
	.hidden	__setsid
	.hidden	__getgid
	.hidden	__getuid
	.hidden	__fchdir
	.hidden	__chdir
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
