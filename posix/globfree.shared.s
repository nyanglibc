	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_globfree
	.hidden	__GI_globfree
	.type	__GI_globfree, @function
__GI_globfree:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	cmpq	$0, (%rbx)
	je	.L3
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L5:
	movq	16(%rbx), %rax
	addq	%rbp, %rax
	addq	$1, %rbp
	movq	(%rdi,%rax,8), %rdi
	call	free@PLT
	cmpq	(%rbx), %rbp
	movq	8(%rbx), %rdi
	jb	.L5
.L3:
	call	free@PLT
	movq	$0, 8(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_globfree, .-__GI_globfree
	.globl	globfree
	.set	globfree,__GI_globfree
	.globl	__GI_globfree64
	.set	__GI_globfree64,globfree
	.weak	globfree64
	.set	globfree64,globfree
