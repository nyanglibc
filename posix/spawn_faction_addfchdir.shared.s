	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	posix_spawn_file_actions_addfchdir_np
	.type	posix_spawn_file_actions_addfchdir_np, @function
posix_spawn_file_actions_addfchdir_np:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	4(%rdi), %edx
	cmpl	(%rdi), %edx
	je	.L7
.L2:
	movslq	%edx, %rax
	addl	$1, %edx
	salq	$5, %rax
	addq	8(%rbx), %rax
	movl	$4, (%rax)
	movl	%esi, 8(%rax)
	xorl	%eax, %eax
	movl	%edx, 4(%rbx)
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%esi, 12(%rsp)
	call	__posix_spawn_file_actions_realloc
	movl	%eax, %edx
	movl	$12, %eax
	testl	%edx, %edx
	jne	.L1
	movl	4(%rbx), %edx
	movl	12(%rsp), %esi
	jmp	.L2
	.size	posix_spawn_file_actions_addfchdir_np, .-posix_spawn_file_actions_addfchdir_np
	.hidden	__posix_spawn_file_actions_realloc
