	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver fattach,fattach@GLIBC_2.2.5
	.symver fdetach,fdetach@GLIBC_2.2.5
	.symver getmsg,getmsg@GLIBC_2.2.5
	.symver getpmsg,getpmsg@GLIBC_2.2.5
	.symver isastream,isastream@GLIBC_2.2.5
	.symver putmsg,putmsg@GLIBC_2.2.5
	.symver putpmsg,putpmsg@GLIBC_2.2.5
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	fattach
	.type	fattach, @function
fattach:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	fattach, .-fattach
	.p2align 4,,15
	.globl	fdetach
	.type	fdetach, @function
fdetach:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	fdetach, .-fdetach
	.p2align 4,,15
	.globl	getmsg
	.type	getmsg, @function
getmsg:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	getmsg, .-getmsg
	.p2align 4,,15
	.globl	getpmsg
	.type	getpmsg, @function
getpmsg:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	getpmsg, .-getpmsg
	.p2align 4,,15
	.globl	isastream
	.type	isastream, @function
isastream:
	subq	$8, %rsp
	movl	$1, %esi
	xorl	%eax, %eax
	call	__GI___fcntl
	addq	$8, %rsp
	sarl	$31, %eax
	ret
	.size	isastream, .-isastream
	.p2align 4,,15
	.globl	putmsg
	.type	putmsg, @function
putmsg:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	putmsg, .-putmsg
	.p2align 4,,15
	.globl	putpmsg
	.type	putpmsg, @function
putpmsg:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	putpmsg, .-putpmsg
