	.text
	.p2align 4,,15
	.type	exchange, @function
exchange:
	pushq	%r15
	pushq	%r14
	leaq	8(%rdi), %r11
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movslq	48(%rsi), %rbp
	pushq	%rbx
	movl	44(%rsi), %r12d
	movl	(%rsi), %ebx
	movq	%rbp, %r8
	movl	%r12d, %r9d
	movl	%ebx, %r10d
.L2:
	cmpl	%r8d, %r10d
	jle	.L11
.L19:
	cmpl	%r8d, %r9d
	jge	.L11
	movl	%r10d, %r13d
	movl	%r8d, %edx
	subl	%r8d, %r13d
	subl	%r9d, %edx
	cmpl	%edx, %r13d
	jg	.L3
	testl	%r13d, %r13d
	jle	.L5
	leal	-1(%r13), %ecx
	movslq	%r9d, %rdx
	leaq	(%rdi,%rdx,8), %rax
	addq	%rdx, %rcx
	leaq	(%r11,%rcx,8), %r14
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	movq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L8:
	movq	(%rax), %rcx
	movq	(%rax,%rdx,8), %r15
	movq	%r15, (%rax)
	movq	%rcx, (%rax,%rdx,8)
	addq	$8, %rax
	cmpq	%r14, %rax
	jne	.L8
.L5:
	addl	%r13d, %r9d
	cmpl	%r8d, %r10d
	jg	.L19
.L11:
	movl	%ebx, %eax
	movl	%ebx, 48(%rsi)
	subl	%r8d, %eax
	addl	%eax, %r12d
	popq	%rbx
	movl	%r12d, 44(%rsi)
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movslq	%r9d, %rcx
	subl	%edx, %r10d
	subl	$1, %edx
	addq	%rcx, %rdx
	leaq	(%rdi,%rcx,8), %rax
	leaq	(%r11,%rdx,8), %r13
	movslq	%r10d, %rdx
	subq	%rcx, %rdx
	.p2align 4,,10
	.p2align 3
.L6:
	movq	(%rax), %rcx
	movq	(%rax,%rdx,8), %r14
	movq	%r14, (%rax)
	movq	%rcx, (%rax,%rdx,8)
	addq	$8, %rax
	cmpq	%rax, %r13
	jne	.L6
	jmp	.L2
	.size	exchange, .-exchange
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"%s: option '%s%s' is ambiguous\n"
	.align 8
.LC1:
	.string	"%s: option '%s%s' is ambiguous; possibilities:"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC2:
	.string	" '%s%s'"
.LC3:
	.string	"\n"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"%s: unrecognized option '%s%s'\n"
	.align 8
.LC5:
	.string	"%s: option '%s%s' doesn't allow an argument\n"
	.align 8
.LC6:
	.string	"%s: option '%s%s' requires an argument\n"
	.text
	.p2align 4,,15
	.type	process_long_option, @function
process_long_option:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	subq	$120, %rsp
	movq	16(%rbp), %rax
	movl	%edi, -124(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%rcx, -64(%rbp)
	movq	%r8, -104(%rbp)
	movq	32(%rax), %r12
	movl	%r9d, -68(%rbp)
	movzbl	(%r12), %eax
	movq	%r12, -56(%rbp)
	testb	%al, %al
	movb	%al, -69(%rbp)
	je	.L52
	cmpb	$61, %al
	jne	.L22
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L104:
	cmpb	$61, %al
	je	.L59
.L22:
	addq	$1, %r12
	movzbl	(%r12), %eax
	testb	%al, %al
	jne	.L104
.L59:
	movq	%r12, %r14
	subq	-56(%rbp), %r14
.L21:
	movq	-64(%rbp), %rbx
	movq	(%rbx), %r15
	testq	%r15, %r15
	movq	%r15, -88(%rbp)
	je	.L24
	movq	%r12, -80(%rbp)
	movq	%r15, %r12
	movq	-56(%rbp), %r15
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L27:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%r12, %rdi
	call	strncmp
	testl	%eax, %eax
	jne	.L25
	movq	%r12, %rdi
	call	strlen
	cmpq	%r14, %rax
	je	.L102
.L25:
	addq	$32, %rbx
	movq	(%rbx), %r12
	addl	$1, %r13d
	testq	%r12, %r12
	jne	.L27
	movq	-80(%rbp), %r12
	movslq	%r13d, %rax
	movq	-64(%rbp), %rcx
	movq	%rax, -144(%rbp)
	addq	$30, %rax
	movq	-88(%rbp), %rdi
	andq	$-16, %rax
	xorl	%r15d, %r15d
	movl	$-1, -128(%rbp)
	movq	%r12, -88(%rbp)
	movl	$0, -80(%rbp)
	xorl	%ebx, %ebx
	movl	$0, -132(%rbp)
	movq	$0, -96(%rbp)
	movq	%rcx, %r12
	movq	%rax, -152(%rbp)
.L34:
	movq	-56(%rbp), %rsi
	movq	%r14, %rdx
	call	strncmp
	testl	%eax, %eax
	jne	.L28
	testq	%rbx, %rbx
	je	.L53
	movl	-68(%rbp), %eax
	testl	%eax, %eax
	je	.L105
.L29:
	movl	-80(%rbp), %eax
	testl	%eax, %eax
	jne	.L28
	movl	24(%rbp), %r11d
	testl	%r11d, %r11d
	jne	.L106
	cmpq	$0, -96(%rbp)
	movl	$1, -80(%rbp)
	je	.L28
.L31:
	movq	-96(%rbp), %rax
	movb	$1, (%rax,%r15)
	.p2align 4,,10
	.p2align 3
.L28:
	addq	$32, %r12
	movq	(%r12), %rdi
	addq	$1, %r15
	testq	%rdi, %rdi
	jne	.L34
	cmpq	$0, -96(%rbp)
	movq	-88(%rbp), %r12
	jne	.L60
	movl	-80(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L60
	testq	%rbx, %rbx
	jne	.L56
.L24:
	movl	-68(%rbp), %esi
	testl	%esi, %esi
	je	.L43
	movq	16(%rbp), %rax
	movq	-112(%rbp), %rcx
	movslq	(%rax), %rax
	movq	(%rcx,%rax,8), %rax
	cmpb	$45, 1(%rax)
	je	.L43
	movsbl	-69(%rbp), %esi
	movq	-120(%rbp), %rdi
	call	strchr
	movq	%rax, %rdx
	movl	$-1, %eax
	testq	%rdx, %rdx
	jne	.L20
.L43:
	movl	24(%rbp), %ecx
	testl	%ecx, %ecx
	jne	.L107
.L44:
	movq	16(%rbp), %rax
	movq	$0, 32(%rax)
	addl	$1, (%rax)
	movl	$0, 8(%rax)
	leaq	-40(%rbp), %rsp
	movl	$63, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L105:
	movl	8(%r12), %eax
	cmpl	%eax, 8(%rbx)
	jne	.L29
	movq	16(%r12), %rax
	cmpq	%rax, 16(%rbx)
	jne	.L29
	movl	24(%r12), %eax
	cmpl	%eax, 24(%rbx)
	jne	.L29
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L102:
	movq	-80(%rbp), %r12
.L26:
	movq	16(%rbp), %rax
	movq	16(%rbp), %rcx
	movl	(%rax), %edx
	movq	$0, 32(%rcx)
	leal	1(%rdx), %eax
	movl	%eax, (%rcx)
	cmpb	$0, (%r12)
	movl	8(%rbx), %ecx
	je	.L45
	testl	%ecx, %ecx
	je	.L46
	movq	16(%rbp), %rax
	addq	$1, %r12
	movq	%r12, 16(%rax)
.L47:
	movq	-104(%rbp), %rax
	testq	%rax, %rax
	je	.L51
	movl	%r13d, (%rax)
.L51:
	movq	16(%rbx), %rdx
	movl	24(%rbx), %eax
	testq	%rdx, %rdx
	je	.L20
	movl	%eax, (%rdx)
	xorl	%eax, %eax
.L20:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	cmpl	$1, %ecx
	jne	.L47
	cmpl	-124(%rbp), %eax
	jge	.L49
	movq	16(%rbp), %rcx
	addl	$2, %edx
	cltq
	movl	%edx, (%rcx)
	movq	-112(%rbp), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	16(%rbp), %rcx
	movq	%rax, 16(%rcx)
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L53:
	movl	%r15d, -128(%rbp)
	movq	%r12, %rbx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L106:
	cmpq	$0, -96(%rbp)
	jne	.L31
	movq	-144(%rbp), %rdi
	call	__libc_alloca_cutoff
	cmpl	$4096, %r13d
	jle	.L32
	testl	%eax, %eax
	je	.L108
.L32:
	subq	-152(%rbp), %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -96(%rbp)
.L33:
	movq	-144(%rbp), %rdx
	movq	-96(%rbp), %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movslq	-128(%rbp), %rax
	movq	-96(%rbp), %rcx
	movb	$1, (%rcx,%rax)
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L107:
	movq	-112(%rbp), %rax
	leaq	.LC4(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	movq	(%rax), %rbx
	call	__dcgettext
	movq	-56(%rbp), %r8
	movq	32(%rbp), %rcx
	movq	%rax, %rsi
	movq	stderr(%rip), %rdi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L60:
	movl	24(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L109
.L37:
	movl	-132(%rbp), %edi
	testl	%edi, %edi
	je	.L41
	movq	-96(%rbp), %rdi
	call	free@PLT
.L41:
	movq	-56(%rbp), %rbx
	movq	%rbx, %rdi
	call	strlen
	movq	16(%rbp), %rcx
	addq	%rbx, %rax
	movq	%rax, 32(%rcx)
	addl	$1, (%rcx)
	movl	$63, %eax
	movl	$0, 8(%rcx)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L52:
	movq	-56(%rbp), %r12
	xorl	%r14d, %r14d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L46:
	movl	24(%rbp), %edx
	testl	%edx, %edx
	je	.L48
	movq	-112(%rbp), %rax
	leaq	.LC5(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	(%rbx), %r13
	movl	$5, %edx
	movq	(%rax), %r12
	call	__dcgettext
	movq	32(%rbp), %rcx
	movq	stderr(%rip), %rdi
	movq	%rax, %rsi
	movq	%r13, %r8
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
.L48:
	movl	24(%rbx), %eax
	movq	16(%rbp), %rcx
	movl	%eax, 8(%rcx)
	movl	$63, %eax
	jmp	.L20
.L49:
	movl	24(%rbp), %eax
	testl	%eax, %eax
	jne	.L110
.L50:
	movl	24(%rbx), %eax
	movq	16(%rbp), %rcx
	movl	%eax, 8(%rcx)
	movq	-120(%rbp), %rax
	cmpb	$58, (%rax)
	setne	%al
	movzbl	%al, %eax
	leal	58(%rax,%rax,4), %eax
	jmp	.L20
.L109:
	movl	-80(%rbp), %r8d
	movq	-112(%rbp), %rax
	movl	$5, %edx
	testl	%r8d, %r8d
	movq	(%rax), %rbx
	je	.L38
	leaq	.LC0(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	call	__dcgettext
	movq	-56(%rbp), %r8
	movq	32(%rbp), %rcx
	movq	%rax, %rsi
	movq	stderr(%rip), %rdi
	movq	%rbx, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
	movq	16(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	jmp	.L37
.L110:
	movq	-112(%rbp), %rax
	leaq	.LC6(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	(%rbx), %r13
	movl	$5, %edx
	movq	(%rax), %r12
	call	__dcgettext
	movq	32(%rbp), %rcx
	movq	stderr(%rip), %rdi
	movq	%rax, %rsi
	movq	%r13, %r8
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
	jmp	.L50
.L38:
	leaq	.LC1(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	leaq	.LC2(%rip), %r12
	call	__dcgettext
	movq	32(%rbp), %rcx
	movq	stderr(%rip), %rdi
	movq	%rbx, %rdx
	movq	-56(%rbp), %r8
	movq	%rax, %rsi
	xorl	%eax, %eax
	call	__fxprintf_nocancel
	movq	-96(%rbp), %rcx
	leal	-1(%r13), %eax
	movq	stderr(%rip), %rdi
	movq	-64(%rbp), %r14
	movq	%rcx, %rbx
	leaq	1(%rcx,%rax), %r13
	jmp	.L40
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$1, %rbx
	addq	$32, %r14
	cmpq	%rbx, %r13
	je	.L111
.L40:
	cmpb	$0, (%rbx)
	je	.L39
	movq	(%r14), %rcx
	movq	32(%rbp), %rdx
	movq	%r12, %rsi
	xorl	%eax, %eax
	call	__fxprintf_nocancel
	movq	stderr(%rip), %rdi
	jmp	.L39
.L111:
	leaq	.LC3(%rip), %rsi
	xorl	%eax, %eax
	call	__fxprintf_nocancel
	movq	16(%rbp), %rax
	movq	32(%rax), %rax
	movq	%rax, -56(%rbp)
	jmp	.L37
.L108:
	movq	-144(%rbp), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -96(%rbp)
	je	.L54
	movl	$1, -132(%rbp)
	jmp	.L33
.L56:
	movl	-128(%rbp), %r13d
	jmp	.L26
.L54:
	movl	$1, -80(%rbp)
	jmp	.L28
	.size	process_long_option, .-process_long_option
	.section	.rodata.str1.1
.LC7:
	.string	"POSIXLY_CORRECT"
.LC8:
	.string	"--"
.LC9:
	.string	"-"
.LC10:
	.string	"%s: invalid option -- '%c'\n"
	.section	.rodata.str1.8
	.align 8
.LC11:
	.string	"%s: option requires an argument -- '%c'\n"
	.section	.rodata.str1.1
.LC12:
	.string	"-W "
	.text
	.p2align 4,,15
	.globl	_getopt_internal_r
	.type	_getopt_internal_r, @function
_getopt_internal_r:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	testl	%edi, %edi
	movq	112(%rsp), %rbx
	movq	%rcx, 8(%rsp)
	movq	%r8, 16(%rsp)
	movl	4(%rbx), %r13d
	jle	.L200
	movl	(%rbx), %eax
	movl	%edi, %r12d
	movq	%rsi, %r15
	movq	%rdx, %rbp
	movq	$0, 16(%rbx)
	testl	%eax, %eax
	je	.L115
	movl	24(%rbx), %esi
	testl	%esi, %esi
	jne	.L202
.L116:
	movl	%eax, 48(%rbx)
	movl	%eax, 44(%rbx)
	movq	$0, 32(%rbx)
	movzbl	0(%rbp), %eax
	cmpb	$45, %al
	je	.L203
	cmpb	$43, %al
	je	.L204
	movl	120(%rsp), %ecx
	xorl	%edx, %edx
	testl	%ecx, %ecx
	je	.L205
.L121:
	movl	$0, 40(%rbx)
.L119:
	movl	$1, 24(%rbx)
	movzbl	0(%rbp), %eax
.L123:
	cmpb	$58, %al
	movl	$0, %eax
	cmove	%eax, %r13d
	testq	%rdx, %rdx
	je	.L125
.L208:
	cmpb	$0, (%rdx)
	je	.L125
.L126:
	leaq	1(%rdx), %r9
	movq	%rbp, %rdi
	movq	%rdx, 32(%rsp)
	movq	%r9, 32(%rbx)
	movsbl	(%rdx), %r14d
	movq	%r9, 40(%rsp)
	movl	%r14d, %esi
	movb	%r14b, 28(%rsp)
	call	strchr
	movq	32(%rsp), %rdx
	movzbl	28(%rsp), %ecx
	movq	40(%rsp), %r9
	cmpb	$0, 1(%rdx)
	jne	.L145
	addl	$1, (%rbx)
.L145:
	subl	$58, %ecx
	cmpb	$1, %cl
	jbe	.L172
	testq	%rax, %rax
	je	.L172
	cmpb	$87, (%rax)
	movzbl	1(%rax), %ecx
	je	.L206
.L149:
	cmpb	$58, %cl
	je	.L207
.L112:
	addq	$56, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L202:
	movzbl	(%rdx), %eax
	movq	32(%rbx), %rdx
	leal	-43(%rax), %ecx
	andl	$253, %ecx
	jne	.L123
	movzbl	1(%rbp), %eax
	addq	$1, %rbp
	cmpb	$58, %al
	movl	$0, %eax
	cmove	%eax, %r13d
	testq	%rdx, %rdx
	jne	.L208
	.p2align 4,,10
	.p2align 3
.L125:
	movl	(%rbx), %eax
	cmpl	%eax, 48(%rbx)
	jle	.L127
	movl	%eax, 48(%rbx)
.L127:
	cmpl	44(%rbx), %eax
	jge	.L128
	movl	%eax, 44(%rbx)
.L128:
	cmpl	$1, 40(%rbx)
	je	.L209
.L129:
	cmpl	%eax, %r12d
	je	.L135
	movslq	%eax, %rdx
	leaq	.LC8(%rip), %r8
	movl	$3, %ecx
	movq	(%r15,%rdx,8), %r14
	movq	%r8, %rdi
	movq	%r14, %rsi
	repz cmpsb
	jne	.L136
	movl	44(%rbx), %edx
	movl	48(%rbx), %ecx
	addl	$1, %eax
	movl	%eax, (%rbx)
	cmpl	%ecx, %edx
	je	.L137
	cmpl	%ecx, %eax
	je	.L138
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	exchange
	movl	44(%rbx), %edx
.L138:
	cmpl	%r12d, %edx
	movl	%r12d, 48(%rbx)
	movl	%r12d, (%rbx)
	jne	.L162
.L200:
	movl	$-1, %r14d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L203:
	movl	$2, 40(%rbx)
	addq	$1, %rbp
	xorl	%edx, %edx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L136:
	cmpb	$45, (%r14)
	je	.L210
.L163:
	movl	40(%rbx), %edx
	testl	%edx, %edx
	je	.L200
	addl	$1, %eax
	movq	%r14, 16(%rbx)
	movl	$1, %r14d
	movl	%eax, (%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L115:
	movl	$1, (%rbx)
	movl	$1, %eax
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L135:
	movl	48(%rbx), %r12d
	movl	44(%rbx), %edx
	cmpl	%r12d, %edx
	je	.L200
.L162:
	movl	%edx, (%rbx)
	movl	$-1, %r14d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L209:
	movl	48(%rbx), %edx
	cmpl	%edx, 44(%rbx)
	je	.L130
	cmpl	%edx, %eax
	je	.L131
	movq	%rbx, %rsi
	movq	%r15, %rdi
	movl	%r9d, 28(%rsp)
	call	exchange
	movl	(%rbx), %edx
	movl	28(%rsp), %r9d
.L131:
	cmpl	%edx, %r12d
	jle	.L169
	leal	-1(%r12), %ecx
	movslq	%edx, %rax
	movl	%ecx, %edi
	subl	%edx, %edi
	leaq	1(%rax,%rdi), %rsi
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L134:
	leal	1(%rax), %edx
	addq	$1, %rax
	cmpq	%rax, %rsi
	movl	%edx, (%rbx)
	je	.L169
.L133:
	movq	(%r15,%rax,8), %rcx
	movl	%eax, %edx
	cmpb	$45, (%rcx)
	jne	.L134
	cmpb	$0, 1(%rcx)
	je	.L134
	movl	(%rbx), %eax
.L132:
	movl	%edx, 48(%rbx)
	jmp	.L129
	.p2align 4,,10
	.p2align 3
.L206:
	cmpq	$0, 8(%rsp)
	je	.L149
	cmpb	$59, %cl
	jne	.L149
	cmpb	$0, 1(%rdx)
	jne	.L150
	movslq	(%rbx), %rax
	cmpl	%r12d, %eax
	je	.L211
	movq	(%r15,%rax,8), %r9
.L150:
	leaq	.LC12(%rip), %rax
	subq	$8, %rsp
	movq	%r9, 32(%rbx)
	movq	$0, 16(%rbx)
	xorl	%r9d, %r9d
	pushq	%rax
	pushq	%r13
	pushq	%rbx
.L201:
	movq	48(%rsp), %r8
	movq	40(%rsp), %rcx
	movq	%rbp, %rdx
	movq	%r15, %rsi
	movl	%r12d, %edi
	call	process_long_option
	addq	$32, %rsp
	movl	%eax, %r14d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L130:
	cmpl	%edx, %eax
	je	.L131
	movl	%eax, 44(%rbx)
	movl	%eax, %edx
	jmp	.L131
	.p2align 4,,10
	.p2align 3
.L207:
	cmpb	$58, 2(%rax)
	movzbl	1(%rdx), %edx
	je	.L212
	testb	%dl, %dl
	movslq	(%rbx), %rax
	je	.L158
	addl	$1, %eax
	movq	%r9, 16(%rbx)
	movl	%eax, (%rbx)
.L159:
	movq	$0, 32(%rbx)
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L172:
	testl	%r13d, %r13d
	je	.L148
	leaq	.LC10(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	(%r15), %rbp
	movl	$5, %edx
	call	__dcgettext
	movq	stderr(%rip), %rdi
	movq	%rax, %rsi
	movl	%r14d, %ecx
	movq	%rbp, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
.L148:
	movl	%r14d, 8(%rbx)
	movl	$63, %r14d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L169:
	movl	%edx, %eax
	jmp	.L132
	.p2align 4,,10
	.p2align 3
.L210:
	movzbl	1(%r14), %ecx
	testb	%cl, %cl
	je	.L163
	cmpq	$0, 8(%rsp)
	je	.L142
	cmpb	$45, %cl
	je	.L213
	testl	%r9d, %r9d
	je	.L142
	cmpb	$0, 2(%r14)
	jne	.L144
	movsbl	%cl, %esi
	movq	%rbp, %rdi
	movl	%r9d, 28(%rsp)
	call	strchr
	testq	%rax, %rax
	movl	28(%rsp), %r9d
	jne	.L142
.L144:
	leaq	1(%r14), %rdx
	leaq	.LC9(%rip), %rax
	subq	$8, %rsp
	movq	%r15, %rsi
	movl	%r12d, %edi
	movq	%rdx, 32(%rbx)
	pushq	%rax
	movq	%rbp, %rdx
	pushq	%r13
	pushq	%rbx
	movq	48(%rsp), %r8
	movq	40(%rsp), %rcx
	call	process_long_option
	addq	$32, %rsp
	cmpl	$-1, %eax
	movl	%eax, %r14d
	jne	.L112
	movslq	(%rbx), %rax
	movq	(%r15,%rax,8), %r14
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	1(%r14), %rdx
	jmp	.L126
	.p2align 4,,10
	.p2align 3
.L204:
	movl	$0, 40(%rbx)
	addq	$1, %rbp
	xorl	%edx, %edx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L205:
	leaq	.LC7(%rip), %rdi
	movl	%r9d, 28(%rsp)
	call	getenv
	testq	%rax, %rax
	movl	28(%rsp), %r9d
	je	.L122
	movq	32(%rbx), %rdx
	jmp	.L121
	.p2align 4,,10
	.p2align 3
.L137:
	movl	%eax, 44(%rbx)
	movl	%eax, %edx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L212:
	testb	%dl, %dl
	je	.L156
	movq	%r9, 16(%rbx)
	addl	$1, (%rbx)
	jmp	.L159
.L213:
	leaq	2(%r14), %rdx
	subq	$8, %rsp
	movq	%rdx, 32(%rbx)
	pushq	%r8
	pushq	%r13
	pushq	%rbx
	jmp	.L201
	.p2align 4,,10
	.p2align 3
.L122:
	movl	$1, 40(%rbx)
	movq	32(%rbx), %rdx
	jmp	.L119
	.p2align 4,,10
	.p2align 3
.L158:
	cmpl	%eax, %r12d
	je	.L214
	leal	1(%rax), %edx
	movq	(%r15,%rax,8), %rax
	movl	%edx, (%rbx)
	movq	%rax, 16(%rbx)
	jmp	.L159
.L156:
	movq	$0, 16(%rbx)
	jmp	.L159
.L214:
	testl	%r13d, %r13d
	je	.L161
	leaq	.LC11(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	(%r15), %r12
	movl	$5, %edx
	call	__dcgettext
	movq	stderr(%rip), %rdi
	movq	%rax, %rsi
	movl	%r14d, %ecx
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
.L161:
	movl	%r14d, 8(%rbx)
	xorl	%r14d, %r14d
	cmpb	$58, 0(%rbp)
	setne	%r14b
	leal	58(%r14,%r14,4), %r14d
	jmp	.L159
.L211:
	testl	%r13d, %r13d
	je	.L152
	leaq	.LC11(%rip), %rsi
	leaq	_libc_intl_domainname(%rip), %rdi
	movq	(%r15), %r12
	movl	$5, %edx
	call	__dcgettext
	movq	stderr(%rip), %rdi
	movq	%rax, %rsi
	movl	%r14d, %ecx
	movq	%r12, %rdx
	xorl	%eax, %eax
	call	__fxprintf_nocancel
.L152:
	movl	%r14d, 8(%rbx)
	xorl	%r14d, %r14d
	cmpb	$58, 0(%rbp)
	setne	%r14b
	leal	58(%r14,%r14,4), %r14d
	jmp	.L112
	.size	_getopt_internal_r, .-_getopt_internal_r
	.p2align 4,,15
	.globl	_getopt_internal
	.type	_getopt_internal, @function
_getopt_internal:
	subq	$8, %rsp
	movl	optind(%rip), %eax
	movl	%eax, getopt_data(%rip)
	movl	opterr(%rip), %eax
	movl	%eax, 4+getopt_data(%rip)
	movl	16(%rsp), %eax
	pushq	%rax
	leaq	getopt_data(%rip), %rax
	pushq	%rax
	call	_getopt_internal_r
	movl	getopt_data(%rip), %edx
	movl	%edx, optind(%rip)
	movq	16+getopt_data(%rip), %rdx
	movq	%rdx, optarg(%rip)
	movl	8+getopt_data(%rip), %edx
	movl	%edx, optopt(%rip)
	addq	$24, %rsp
	ret
	.size	_getopt_internal, .-_getopt_internal
	.p2align 4,,15
	.globl	getopt
	.type	getopt, @function
getopt:
	subq	$16, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$0
	xorl	%ecx, %ecx
	call	_getopt_internal
	addq	$24, %rsp
	ret
	.size	getopt, .-getopt
	.p2align 4,,15
	.globl	__posix_getopt
	.type	__posix_getopt, @function
__posix_getopt:
	subq	$16, %rsp
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	pushq	$1
	xorl	%ecx, %ecx
	call	_getopt_internal
	addq	$24, %rsp
	ret
	.size	__posix_getopt, .-__posix_getopt
	.local	getopt_data
	.comm	getopt_data,56,32
	.globl	optopt
	.data
	.align 4
	.type	optopt, @object
	.size	optopt, 4
optopt:
	.long	63
	.globl	opterr
	.align 4
	.type	opterr, @object
	.size	opterr, 4
opterr:
	.long	1
	.globl	optind
	.align 4
	.type	optind, @object
	.size	optind, 4
optind:
	.long	1
	.comm	optarg,8,8
	.hidden	getenv
	.hidden	__fxprintf_nocancel
	.hidden	__dcgettext
	.hidden	_libc_intl_domainname
	.hidden	__libc_alloca_cutoff
	.hidden	strchr
	.hidden	strlen
	.hidden	strncmp
