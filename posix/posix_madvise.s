	.text
	.p2align 4,,15
	.globl	posix_madvise
	.type	posix_madvise, @function
posix_madvise:
	xorl	%eax, %eax
	cmpl	$4, %edx
	je	.L1
	movl	$28, %eax
#APP
# 34 "../sysdeps/unix/sysv/linux/posix_madvise.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	negl	%eax
.L1:
	rep ret
	.size	posix_madvise, .-posix_madvise
