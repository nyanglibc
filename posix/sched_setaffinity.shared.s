	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __sched_setaffinity_new,sched_setaffinity@@GLIBC_2.3.4
	.symver __sched_setaffinity_old,sched_setaffinity@GLIBC_2.3.3
#NO_APP
	.p2align 4,,15
	.globl	__GI___sched_setaffinity_new
	.hidden	__GI___sched_setaffinity_new
	.type	__GI___sched_setaffinity_new, @function
__GI___sched_setaffinity_new:
	movl	$203, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/sched_setaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___sched_setaffinity_new, .-__GI___sched_setaffinity_new
	.globl	__sched_setaffinity_new
	.set	__sched_setaffinity_new,__GI___sched_setaffinity_new
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__sched_setaffinity_old
	.type	__sched_setaffinity_old, @function
__sched_setaffinity_old:
	movq	%rsi, %rdx
	movl	$203, %eax
	movl	$128, %esi
#APP
# 33 "../sysdeps/unix/sysv/linux/sched_setaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__sched_setaffinity_old, .-__sched_setaffinity_old
