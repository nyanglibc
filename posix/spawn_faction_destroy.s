	.text
	.p2align 4,,15
	.globl	__posix_spawn_file_actions_destroy
	.hidden	__posix_spawn_file_actions_destroy
	.type	__posix_spawn_file_actions_destroy, @function
__posix_spawn_file_actions_destroy:
	pushq	%r12
	movq	%rdi, %r12
	pushq	%rbp
	movl	4(%r12), %eax
	pushq	%rbx
	movq	8(%rdi), %rdi
	testl	%eax, %eax
	jle	.L2
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$3, %edx
	jne	.L3
	movq	8(%rax), %rdi
	call	free@PLT
	movq	8(%r12), %rdi
.L3:
	addl	$1, %ebx
	addq	$32, %rbp
	cmpl	%ebx, 4(%r12)
	jle	.L2
.L6:
	leaq	(%rdi,%rbp), %rax
	movl	(%rax), %edx
	cmpl	$2, %edx
	jne	.L10
	movq	16(%rax), %rdi
	addl	$1, %ebx
	addq	$32, %rbp
	call	free@PLT
	cmpl	%ebx, 4(%r12)
	movq	8(%r12), %rdi
	jg	.L6
.L2:
	call	free@PLT
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	ret
	.size	__posix_spawn_file_actions_destroy, .-__posix_spawn_file_actions_destroy
	.weak	posix_spawn_file_actions_destroy
	.set	posix_spawn_file_actions_destroy,__posix_spawn_file_actions_destroy
