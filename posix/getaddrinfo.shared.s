	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	prefixcmp, @function
prefixcmp:
.LFB118:
	movl	16(%rsi), %edx
	cmpl	%edx, 16(%rdi)
	movl	$1, %eax
	jb	.L1
	setne	%al
	movzbl	%al, %eax
	negl	%eax
.L1:
	rep ret
.LFE118:
	.size	prefixcmp, .-prefixcmp
	.p2align 4,,15
	.type	scopecmp, @function
scopecmp:
.LFB119:
	movl	4(%rsi), %eax
	cmpl	%eax, 4(%rdi)
	ja	.L7
	setne	%al
	movzbl	%al, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$-1, %eax
	ret
.LFE119:
	.size	scopecmp, .-scopecmp
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	fini, @function
fini:
.LFB115:
	subq	$8, %rsp
	movq	labels(%rip), %rdi
	leaq	default_labels(%rip), %rax
	cmpq	%rax, %rdi
	je	.L9
	movq	%rax, labels(%rip)
	call	free@PLT
.L9:
	movq	precedence(%rip), %rdi
	leaq	default_precedence(%rip), %rax
	cmpq	%rax, %rdi
	je	.L10
	movq	%rax, precedence(%rip)
	call	free@PLT
.L10:
	movq	scopes(%rip), %rdi
	leaq	default_scopes(%rip), %rax
	cmpq	%rax, %rdi
	je	.L8
	movq	%rax, scopes(%rip)
	addq	$8, %rsp
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	addq	$8, %rsp
	ret
.LFE115:
	.size	fini, .-fini
	.text
	.p2align 4,,15
	.type	in6aicmp, @function
in6aicmp:
.LFB112:
	addq	$8, %rsi
	addq	$8, %rdi
	movl	$16, %edx
	jmp	__GI_memcmp
.LFE112:
	.size	in6aicmp, .-in6aicmp
	.p2align 4,,15
	.type	match_prefix, @function
match_prefix:
.LFB107:
	pushq	%rbp
	pushq	%rbx
	movzwl	(%rdi), %ecx
	cmpw	$2, %cx
	je	.L27
	cmpw	$10, %cx
	movl	%edx, %eax
	je	.L28
	popq	%rbx
	popq	%rbp
	ret
.L28:
	movzbl	8(%rdi), %ebx
.L16:
	movl	16(%rsi), %ecx
	leaq	8(%rdi), %r11
	movl	$65280, %ebp
	movzbl	(%rsi), %r8d
	cmpl	$7, %ecx
	jbe	.L25
	.p2align 4,,10
	.p2align 3
.L30:
	cmpb	%r8b, %bl
	jne	.L19
	leal	-8(%rcx), %edi
	movq	%rsi, %rdx
	movl	%edi, %eax
	shrl	$3, %eax
	leaq	1(%r11,%rax), %r10
	movq	%r11, %rax
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L21:
	cmpb	%r9b, %r8b
	jne	.L19
	subl	$8, %edi
.L20:
	addq	$1, %rax
	addq	$1, %rdx
	movl	%edi, %ecx
	cmpq	%r10, %rax
	movzbl	(%rdx), %r8d
	movzbl	(%rax), %r9d
	jne	.L21
	xorl	%r9d, %r8d
	movl	%ebp, %eax
	movzbl	%r8b, %r8d
	sarl	%cl, %eax
	testl	%eax, %r8d
	je	.L29
.L19:
	addq	$24, %rsi
	movl	16(%rsi), %ecx
	movzbl	(%rsi), %r8d
	cmpl	$7, %ecx
	ja	.L30
.L25:
	movl	%ebx, %r9d
	movl	%ebp, %eax
	xorl	%r9d, %r8d
	sarl	%cl, %eax
	movzbl	%r8b, %r8d
	testl	%eax, %r8d
	jne	.L19
.L29:
	movl	20(%rsi), %eax
	popq	%rbx
	popq	%rbp
	ret
.L27:
	movl	$10, %eax
	xorl	%edx, %edx
	movl	$-1, %ecx
	movw	%ax, -40(%rsp)
	movzwl	2(%rdi), %eax
	xorl	%ebx, %ebx
	movw	%dx, -24(%rsp)
	movl	4(%rdi), %edx
	movl	$0, -36(%rsp)
	movq	$0, -32(%rsp)
	movw	%cx, -22(%rsp)
	movl	$0, -16(%rsp)
	movw	%ax, -38(%rsp)
	leaq	-40(%rsp), %rax
	movl	%edx, -20(%rsp)
	movq	%rax, %rdi
	jmp	.L16
.LFE107:
	.size	match_prefix, .-match_prefix
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"/etc/gai.conf"
.LC2:
	.string	"label"
.LC3:
	.string	"reload"
.LC4:
	.string	"yes"
.LC5:
	.string	"scopev4"
.LC6:
	.string	"precedence"
	.text
	.p2align 4,,15
	.type	gaiconf_init, @function
gaiconf_init:
.LFB120:
	pushq	%r15
	pushq	%r14
	leaq	.LC0(%rip), %rsi
	pushq	%r13
	pushq	%r12
	leaq	.LC1(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	subq	$280, %rsp
	movq	$0, 56(%rsp)
	movq	$0, 64(%rsp)
	movb	$0, 54(%rsp)
	movq	$0, 72(%rsp)
	movq	$0, 80(%rsp)
	movb	$0, 55(%rsp)
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	je	.L34
	movq	%rax, %rdi
	movq	%rax, %rbp
	call	__GI_fileno
	leaq	128(%rsp), %rsi
	movl	%eax, %edi
	call	__GI___fstat64
	testl	%eax, %eax
	jne	.L272
	movl	0(%rbp), %eax
	leaq	96(%rsp), %r12
	movq	$0, 88(%rsp)
	movq	$0, 96(%rsp)
	movb	$0, 47(%rsp)
	xorl	%ebx, %ebx
	movq	$0, 16(%rsp)
	orb	$-128, %ah
	movl	%eax, 0(%rbp)
	.p2align 4,,10
	.p2align 3
.L35:
	testb	$16, %al
	leaq	88(%rsp), %r14
	jne	.L82
.L81:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movq	%r14, %rdi
	call	__getline
	testq	%rax, %rax
	jle	.L82
	movq	88(%rsp), %r15
	movl	$35, %esi
	movq	%r15, %rdi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L39
	movb	$0, (%rax)
	movq	88(%rsp), %r15
.L39:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rdi
	movsbq	(%r15), %rdx
	movq	%fs:(%rdi), %rax
	movq	%rdx, %rcx
	testb	$32, 1(%rax,%rdx,2)
	je	.L40
	.p2align 4,,10
	.p2align 3
.L41:
	addq	$1, %r15
	movsbq	(%r15), %rdx
	testb	$32, 1(%rax,%rdx,2)
	movq	%rdx, %rcx
	jne	.L41
.L40:
	testb	%cl, %cl
	movq	%r15, %rdx
	jne	.L43
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L275:
	testw	%cx, %cx
	jne	.L274
	movq	%r8, %rdx
.L43:
	movsbq	1(%rdx), %rcx
	leaq	1(%rdx), %r8
	movq	%rcx, %rsi
	movzwl	(%rax,%rcx,2), %ecx
	andw	$8192, %cx
	testb	%sil, %sil
	jne	.L275
	movq	%r8, %rsi
	movq	%r8, %r9
	subq	%r15, %rsi
.L125:
	xorl	%edx, %edx
.L114:
	testw	%cx, %cx
	je	.L45
	.p2align 4,,10
	.p2align 3
.L46:
	addq	$1, %r9
	movsbq	(%r9), %rcx
	testb	$32, 1(%rax,%rcx,2)
	movq	%rcx, %rdx
	jne	.L46
.L45:
	testb	%dl, %dl
	movq	%r9, %r13
	jne	.L48
	jmp	.L47
	.p2align 4,,10
	.p2align 3
.L276:
	movsbq	%dl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.L47
.L48:
	addq	$1, %r13
	movzbl	0(%r13), %edx
	testb	%dl, %dl
	jne	.L276
.L47:
	cmpq	%r15, %r13
	je	.L277
	testb	%dl, %dl
	je	.L51
	movb	$0, 0(%r13)
	movq	%fs:(%rdi), %rax
	addq	$1, %r13
.L51:
	movsbq	0(%r13), %rcx
	testb	$32, 1(%rax,%rcx,2)
	movq	%rcx, %rdx
	je	.L52
	.p2align 4,,10
	.p2align 3
.L53:
	addq	$1, %r13
	movsbq	0(%r13), %rcx
	testb	$32, 1(%rax,%rcx,2)
	movq	%rcx, %rdx
	jne	.L53
.L52:
	testb	%dl, %dl
	movq	%r13, %rcx
	jne	.L55
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L278:
	testb	$32, 1(%rax,%rdx,2)
	jne	.L54
.L55:
	addq	$1, %rcx
	movsbq	(%rcx), %rdx
	testb	%dl, %dl
	jne	.L278
.L54:
	cmpq	$6, %rsi
	movb	$0, (%rcx)
	je	.L57
	jle	.L279
	cmpq	$7, %rsi
	je	.L60
	cmpq	$10, %rsi
	jne	.L56
	leaq	.LC6(%rip), %rdi
	movl	$11, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L56
	leaq	55(%rsp), %rax
	leaq	72(%rsp), %r14
	movq	%rax, 32(%rsp)
	leaq	80(%rsp), %rax
	movq	%rax, 24(%rsp)
.L62:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r9, %rdi
	movl	$47, %esi
	movq	%r9, 8(%rsp)
	movl	$0, %fs:(%rax)
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r15
	movq	8(%rsp), %r9
	je	.L63
	leaq	112(%rsp), %rdx
	movb	$0, (%rax)
	movq	%r9, %rsi
	movl	$10, %edi
	call	__GI_inet_pton
	testl	%eax, %eax
	je	.L56
	movq	%r15, %rdi
	addq	$1, %rdi
	je	.L115
	leaq	104(%rsp), %rcx
	movl	$10, %edx
	movq	%rcx, %rsi
	movq	%rcx, 8(%rsp)
	call	__GI_strtoul
	cmpq	$-1, %rax
	movq	%rax, %r9
	je	.L56
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	cmpq	$128, %r9
	movq	8(%rsp), %rcx
	jbe	.L67
	.p2align 4,,10
	.p2align 3
.L56:
	movl	0(%rbp), %eax
	jmp	.L35
.L272:
	movq	%rbp, %rdi
	call	_IO_new_fclose@PLT
	.p2align 4,,10
	.p2align 3
.L34:
	call	fini
.L31:
	addq	$280, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	movb	$0, (%r8)
	movsbq	2(%rdx), %rcx
	leaq	2(%rdx), %r9
	movq	%fs:(%rdi), %rax
	movq	%r8, %rsi
	subq	%r15, %rsi
	movq	%rcx, %rdx
	movzwl	(%rax,%rcx,2), %ecx
	andw	$8192, %cx
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L277:
	movl	0(%rbp), %eax
	testb	$16, %al
	je	.L81
	.p2align 4,,10
	.p2align 3
.L82:
	movq	88(%rsp), %rdi
	call	free@PLT
	movq	%rbp, %rdi
	call	_IO_new_fclose@PLT
	movq	64(%rsp), %rbp
	testq	%rbp, %rbp
	je	.L280
	cmpb	$0, 54(%rsp)
	jne	.L83
	addq	$1, %rbp
	leaq	0(%rbp,%rbp,2), %rdi
	movq	%rbp, 64(%rsp)
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L281
.L70:
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L109
	.p2align 4,,10
	.p2align 3
.L110:
	movq	24(%rdi), %rbp
	call	free@PLT
	testq	%rbp, %rbp
	movq	%rbp, %rdi
	jne	.L110
.L109:
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L271
	.p2align 4,,10
	.p2align 3
.L112:
	movq	24(%rdi), %rbp
	call	free@PLT
	testq	%rbp, %rbp
	movq	%rbp, %rdi
	jne	.L112
	testq	%rbx, %rbx
	je	.L34
	.p2align 4,,10
	.p2align 3
.L113:
	movq	16(%rbx), %rbp
	movq	%rbx, %rdi
	call	free@PLT
	movq	%rbp, %rbx
.L271:
	testq	%rbx, %rbx
	jne	.L113
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L279:
	cmpq	$5, %rsi
	jne	.L56
	leaq	.LC2(%rip), %rdi
	movl	$6, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L56
	leaq	54(%rsp), %rax
	leaq	56(%rsp), %r14
	movq	%rax, 32(%rsp)
	leaq	64(%rsp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L62
	.p2align 4,,10
	.p2align 3
.L273:
	movzwl	(%rax), %ecx
	movq	%r15, %r9
	xorl	%esi, %esi
	andw	$8192, %cx
	jmp	.L125
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC5(%rip), %rdi
	movl	$8, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L56
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	%r9, %rdi
	movl	$47, %esi
	movq	%r9, 8(%rsp)
	xorl	%r14d, %r14d
	movl	$0, %fs:(%rax)
	call	__GI_strchr
	testq	%rax, %rax
	movq	8(%rsp), %r9
	je	.L71
	leaq	1(%rax), %r14
	movb	$0, (%rax)
.L71:
	leaq	112(%rsp), %r15
	movq	%r9, %rsi
	movl	$10, %edi
	movq	%r9, 8(%rsp)
	movq	%r15, %rdx
	call	__GI_inet_pton
	testl	%eax, %eax
	movq	8(%rsp), %r9
	je	.L72
	movl	112(%rsp), %edx
	testl	%edx, %edx
	jne	.L56
	movl	116(%rsp), %eax
	testl	%eax, %eax
	jne	.L56
	cmpl	$-65536, 120(%rsp)
	jne	.L56
	testq	%r14, %r14
	je	.L74
	leaq	104(%rsp), %rcx
	movq	%r14, %rdi
	movl	$10, %edx
	movq	%rcx, %rsi
	movq	%rcx, 8(%rsp)
	call	__GI_strtoul
	cmpq	$-1, %rax
	movq	%rax, %r14
	je	.L56
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	leaq	-96(%r14), %rax
	movq	8(%rsp), %rcx
	cmpq	$32, %rax
	ja	.L56
.L116:
	movl	$10, %edx
	movq	%rcx, %rsi
	movq	%r13, %rdi
	call	__GI_strtoul
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L56
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	cmpq	$2147483647, %r15
	ja	.L56
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L57:
	leaq	.LC3(%rip), %rdi
	movl	$7, %ecx
	movq	%r15, %rsi
	repz cmpsb
	jne	.L56
	leaq	.LC4(%rip), %rdi
	movl	$4, %ecx
	movq	%r9, %rsi
	repz cmpsb
	seta	%al
	setb	%dl
	subl	%edx, %eax
	xorl	%edx, %edx
	movsbl	%al, %eax
	testl	%eax, %eax
	sete	%dl
	movl	%edx, gaiconf_reload_flag(%rip)
	jne	.L56
	movl	$1, gaiconf_reload_flag_ever_set(%rip)
	movl	0(%rbp), %eax
	jmp	.L35
.L280:
	leaq	default_labels(%rip), %rax
	movq	%rax, %r13
	movq	%rax, %r14
.L38:
	movq	80(%rsp), %rbp
	testq	%rbp, %rbp
	je	.L132
	cmpb	$0, 55(%rsp)
	jne	.L91
	addq	$1, %rbp
	leaq	0(%rbp,%rbp,2), %rdi
	movq	%rbp, 80(%rsp)
	salq	$3, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L92
.L120:
	cmpq	%r13, %r14
	je	.L70
	movq	%r14, %rdi
	call	free@PLT
	jmp	.L70
.L63:
	leaq	112(%rsp), %rdx
	movq	%r9, %rsi
	movl	$10, %edi
	call	__GI_inet_pton
	testl	%eax, %eax
	je	.L56
.L115:
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	leaq	104(%rsp), %rcx
	movl	$128, %r9d
.L67:
	movq	%r13, %rdi
	movl	$10, %edx
	movq	%rcx, %rsi
	movq	%r9, 8(%rsp)
	call	__GI_strtoul
	cmpq	$-1, %rax
	movq	%rax, %r13
	je	.L56
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	cmpq	$2147483647, %r13
	ja	.L56
	movl	$32, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	8(%rsp), %r9
	je	.L270
	movq	(%r14), %rdx
	movl	%r9d, 16(%rax)
	movdqa	112(%rsp), %xmm0
	movl	%r13d, 20(%rax)
	movq	%rax, (%r14)
	movups	%xmm0, (%rax)
	movq	%rdx, 24(%rax)
	movq	24(%rsp), %rax
	movq	32(%rsp), %rdi
	addq	$1, (%rax)
	testq	%r9, %r9
	sete	%al
	orb	%al, (%rdi)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L132:
	leaq	default_precedence(%rip), %rax
	movq	%rax, 8(%rsp)
	movq	%rax, %r15
.L90:
	movq	16(%rsp), %rax
	testq	%rax, %rax
	je	.L133
	cmpb	$0, 47(%rsp)
	jne	.L98
	addq	$1, %rax
	leaq	(%rax,%rax,2), %rdi
	movq	%rax, 16(%rsp)
	salq	$2, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L123
	movl	16(%rsp), %eax
	subl	$1, %eax
	movslq	%eax, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r12,%rdx,4), %rdx
	movq	$0, (%rdx)
	movl	$14, 8(%rdx)
.L122:
	testl	%eax, %eax
	leal	-1(%rax), %edx
	jle	.L269
	movslq	%edx, %rax
	movl	%edx, %edx
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$2, %rcx
	salq	$2, %rdx
	leaq	-12(%r12,%rcx), %rsi
	leaq	(%r12,%rcx), %rax
	subq	%rdx, %rsi
	movq	%rbx, %rdx
	.p2align 4,,10
	.p2align 3
.L102:
	movq	(%rdx), %rcx
	subq	$12, %rax
	movq	%rcx, 12(%rax)
	movl	8(%rdx), %ecx
	movq	16(%rdx), %rdx
	movl	%ecx, 20(%rax)
	cmpq	%rsi, %rax
	jne	.L102
	jmp	.L269
.L133:
	leaq	default_scopes(%rip), %rbx
	movq	%rbx, %r12
.L97:
	movq	labels(%rip), %rdi
	movq	%r14, labels(%rip)
	cmpq	%r13, %rdi
	je	.L105
	call	free@PLT
.L105:
	movq	precedence(%rip), %rdi
	cmpq	8(%rsp), %rdi
	movq	%r15, precedence(%rip)
	je	.L106
	call	free@PLT
.L106:
	movq	scopes(%rip), %rdi
	movq	%r12, scopes(%rip)
	cmpq	%rbx, %rdi
	je	.L107
	call	free@PLT
.L107:
	movq	216(%rsp), %rax
	movq	%rax, gaiconf_mtime(%rip)
	movq	224(%rsp), %rax
	movq	%rax, 8+gaiconf_mtime(%rip)
	jmp	.L31
.L72:
	leaq	12(%r15), %rdx
	movq	%r9, %rsi
	movl	$2, %edi
	call	__GI_inet_pton
	testl	%eax, %eax
	je	.L56
	testq	%r14, %r14
	je	.L78
	leaq	104(%rsp), %rcx
	movq	%r14, %rdi
	movl	$10, %edx
	movq	%rcx, %rsi
	movq	%rcx, 8(%rsp)
	call	__GI_strtoul
	cmpq	$-1, %rax
	movq	%rax, %r14
	movq	8(%rsp), %rcx
	je	.L56
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	cmpq	$32, %r14
	ja	.L56
.L80:
	movl	$10, %edx
	movq	%rcx, %rsi
	movq	%r13, %rdi
	call	__GI_strtoul
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L56
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	addq	$96, %r14
	cmpq	$2147483647, %r15
	ja	.L56
.L75:
	movl	$24, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L270
	xorl	%edx, %edx
	cmpq	$96, %r14
	je	.L77
	movl	$128, %ecx
	orl	$-1, %edx
	subl	%r14d, %ecx
	sall	%cl, %edx
.L77:
	bswap	%edx
	addq	$1, 16(%rsp)
	movq	%rbx, 16(%rax)
	movl	%edx, 4(%rax)
	andl	124(%rsp), %edx
	cmpq	$96, %r14
	movl	%r15d, 8(%rax)
	movq	%rax, %rbx
	movl	%edx, (%rax)
	sete	%dl
	orb	%dl, 47(%rsp)
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L83:
	leaq	0(%rbp,%rbp,2), %rdi
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, %r14
	movl	%ebp, %eax
	testq	%r14, %r14
	je	.L70
.L117:
	testl	%eax, %eax
	movq	56(%rsp), %rdi
	leal	-1(%rax), %edx
	jle	.L267
	movslq	%edx, %rax
	movl	%edx, %edx
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$3, %rcx
	salq	$3, %rdx
	leaq	-24(%r14,%rcx), %rsi
	leaq	(%r14,%rcx), %rax
	subq	%rdx, %rsi
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L87:
	movdqu	(%rdx), %xmm0
	subq	$24, %rax
	movups	%xmm0, 24(%rax)
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rdx
	movq	%rcx, 40(%rax)
	cmpq	%rax, %rsi
	jne	.L87
	testq	%rdi, %rdi
	je	.L282
	.p2align 4,,10
	.p2align 3
.L89:
	movq	24(%rdi), %r12
	call	free@PLT
	movq	%r12, %rdi
.L267:
	testq	%rdi, %rdi
	jne	.L89
.L282:
	leaq	prefixcmp(%rip), %rcx
	movl	$24, %edx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	__GI_qsort
	leaq	default_labels(%rip), %rax
	movq	%rax, %r13
	jmp	.L38
.L91:
	leaq	0(%rbp,%rbp,2), %rdi
	salq	$3, %rdi
	call	malloc@PLT
	movq	%rax, %r15
	movl	%ebp, %eax
	testq	%r15, %r15
	je	.L120
.L119:
	testl	%eax, %eax
	movq	72(%rsp), %rdi
	leal	-1(%rax), %edx
	jle	.L268
	movslq	%edx, %rax
	movl	%edx, %edx
	leaq	(%rax,%rax,2), %rcx
	leaq	(%rdx,%rdx,2), %rdx
	salq	$3, %rcx
	salq	$3, %rdx
	leaq	-24(%r15,%rcx), %rsi
	leaq	(%r15,%rcx), %rax
	subq	%rdx, %rsi
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L94:
	movdqu	(%rdx), %xmm0
	subq	$24, %rax
	movups	%xmm0, 24(%rax)
	movq	16(%rdx), %rcx
	movq	24(%rdx), %rdx
	movq	%rcx, 40(%rax)
	cmpq	%rax, %rsi
	jne	.L94
	jmp	.L268
	.p2align 4,,10
	.p2align 3
.L96:
	movq	24(%rdi), %r12
	call	free@PLT
	movq	%r12, %rdi
.L268:
	testq	%rdi, %rdi
	jne	.L96
	leaq	prefixcmp(%rip), %rcx
	movl	$24, %edx
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	__GI_qsort
	leaq	default_precedence(%rip), %rax
	movq	%rax, 8(%rsp)
	jmp	.L90
.L98:
	movq	16(%rsp), %rax
	leaq	(%rax,%rax,2), %rdi
	salq	$2, %rdi
	call	malloc@PLT
	movq	%rax, %r12
	movl	16(%rsp), %eax
	testq	%r12, %r12
	jne	.L122
.L123:
	cmpq	%r13, %r14
	je	.L100
	movq	%r14, %rdi
	call	free@PLT
.L100:
	cmpq	8(%rsp), %r15
	je	.L70
	movq	%r15, %rdi
	call	free@PLT
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L104:
	movq	16(%rbx), %rbp
	movq	%rbx, %rdi
	call	free@PLT
	movq	%rbp, %rbx
.L269:
	testq	%rbx, %rbx
	jne	.L104
	movq	16(%rsp), %rsi
	leaq	scopecmp(%rip), %rcx
	movl	$12, %edx
	movq	%r12, %rdi
	leaq	default_scopes(%rip), %rbx
	call	__GI_qsort
	jmp	.L97
.L92:
	leal	-1(%rbp), %eax
	pxor	%xmm0, %xmm0
	movabsq	$171798691840, %rdi
	movslq	%eax, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r15,%rdx,8), %rdx
	movups	%xmm0, (%rdx)
	movq	%rdi, 16(%rdx)
	jmp	.L119
.L78:
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	movl	$32, %r14d
	leaq	104(%rsp), %rcx
	jmp	.L80
.L281:
	leal	-1(%rbp), %eax
	pxor	%xmm0, %xmm0
	movabsq	$4294967296, %rdi
	movslq	%eax, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%r14,%rdx,8), %rdx
	movups	%xmm0, (%rdx)
	movq	%rdi, 16(%rdx)
	jmp	.L117
.L74:
	movq	104(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L56
	movl	$128, %r14d
	leaq	104(%rsp), %rcx
	jmp	.L116
.L270:
	movq	88(%rsp), %rdi
	call	free@PLT
	movq	%rbp, %rdi
	call	_IO_new_fclose@PLT
	jmp	.L70
.LFE120:
	.size	gaiconf_init, .-gaiconf_init
	.p2align 4,,15
	.type	gaih_inet_serv.isra.4, @function
gaih_inet_serv.isra.4:
.LFB128:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	leaq	10(%rsi), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	movq	%rsi, %r14
	movq	%r8, %rbx
	subq	$72, %rsp
	leaq	32(%rsp), %rax
	leaq	24(%rsp), %rbp
	movq	%rdx, 8(%rsp)
	movq	%rax, (%rsp)
.L287:
	movq	8(%rbx), %r8
	movq	(%rbx), %rcx
	movq	%rbp, %r9
	movq	(%rsp), %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__getservbyname_r
	testl	%eax, %eax
	jne	.L284
	movq	24(%rsp), %rdx
	testq	%rdx, %rdx
	je	.L292
	movl	(%r14), %ecx
	movq	$0, (%r15)
	movl	%ecx, 8(%r15)
	testb	$2, 8(%r14)
	je	.L297
	movq	8(%rsp), %rsi
	movl	16(%rdx), %edx
	movl	(%rsi), %ecx
	movl	%edx, 16(%r15)
	movl	%ecx, 12(%r15)
.L298:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L284:
	cmpl	$34, %eax
	jne	.L292
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L287
	addq	$72, %rsp
	movl	$10, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L292:
	addq	$72, %rsp
	movl	$8, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L297:
	movl	4(%r14), %ecx
	movl	16(%rdx), %edx
	movl	%ecx, 12(%r15)
	movl	%edx, 16(%r15)
	jmp	.L298
.LFE128:
	.size	gaih_inet_serv.isra.4, .-gaih_inet_serv.isra.4
	.p2align 4,,15
	.type	get_scope, @function
get_scope:
.LFB106:
	movzwl	(%rdi), %edx
	cmpw	$10, %dx
	je	.L314
	cmpw	$2, %dx
	movl	$15, %eax
	jne	.L299
	movq	scopes(%rip), %rdx
	movl	4(%rdi), %esi
	movl	4(%rdx), %ecx
	leaq	12(%rdx), %rax
	andl	%esi, %ecx
	cmpl	(%rdx), %ecx
	je	.L305
	.p2align 4,,10
	.p2align 3
.L306:
	movq	%rax, %rdx
	addq	$12, %rax
	movl	4(%rdx), %ecx
	andl	%esi, %ecx
	cmpl	-12(%rax), %ecx
	jne	.L306
.L305:
	movl	8(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	cmpb	$-1, 8(%rdi)
	je	.L301
	movl	8(%rdi), %eax
	movl	%eax, %edx
	andl	$49407, %edx
	cmpl	$33022, %edx
	jne	.L315
	movl	$2, %eax
.L299:
	rep ret
	.p2align 4,,10
	.p2align 3
.L315:
	testl	%eax, %eax
	jne	.L303
	movl	12(%rdi), %ecx
	movl	$14, %eax
	testl	%ecx, %ecx
	jne	.L299
	movl	16(%rdi), %edx
	testl	%edx, %edx
	jne	.L299
	cmpl	$16777216, 20(%rdi)
	movl	$2, %edx
	cmove	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L301:
	movzbl	9(%rdi), %eax
	andl	$15, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L303:
	xorl	%eax, %eax
	cmpl	$49406, %edx
	setne	%al
	leal	5(%rax,%rax,8), %eax
	ret
.LFE106:
	.size	get_scope, .-get_scope
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC7:
	.string	"../sysdeps/posix/getaddrinfo.c"
	.align 8
.LC8:
	.string	"src->results[i].native == -1 || src->results[i].native == a1_native"
	.align 8
.LC9:
	.string	"src->results[i].native == -1 || src->results[i].native == a2_native"
	.align 8
.LC10:
	.string	"a1->source_addr.sin6_family == PF_INET"
	.align 8
.LC11:
	.string	"a2->source_addr.sin6_family == PF_INET"
	.align 8
.LC12:
	.string	"a1->source_addr.sin6_family == PF_INET6"
	.align 8
.LC13:
	.string	"a2->source_addr.sin6_family == PF_INET6"
	.text
	.p2align 4,,15
	.type	rfc3484_sort, @function
rfc3484_sort:
.LFB111:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movq	(%rdi), %r12
	movq	(%rsi), %r15
	movq	(%rdx), %rax
	leaq	(%r12,%r12,2), %r8
	leaq	(%r15,%r15,2), %rbx
	salq	$4, %r8
	salq	$4, %rbx
	leaq	(%rax,%r8), %rbp
	addq	%rax, %rbx
	movzbl	37(%rbx), %eax
	cmpb	$0, 37(%rbp)
	je	.L317
	testb	%al, %al
	je	.L369
	movq	0(%rbp), %rax
	movq	%rdx, 16(%rsp)
	leaq	8(%rbp), %r14
	movq	24(%rax), %r8
	movq	%r8, %rdi
	call	get_scope
	movl	%eax, %r9d
	movl	%eax, 12(%rsp)
	movq	(%rbx), %rax
	movq	24(%rax), %rdi
	call	get_scope
	movq	%r14, %rdi
	movl	%eax, %r10d
	movl	%eax, 32(%rsp)
	call	get_scope
	leaq	8(%rbx), %rcx
	movl	%eax, %r13d
	movq	%rcx, %rdi
	movq	%rcx, 24(%rsp)
	call	get_scope
	cmpl	%r9d, %r13d
	movq	16(%rsp), %r11
	jne	.L364
	cmpl	%r10d, %eax
	jne	.L369
.L364:
	cmpl	12(%rsp), %r13d
	je	.L372
	cmpl	32(%rsp), %eax
	jne	.L372
	.p2align 4,,10
	.p2align 3
.L320:
	movl	$1, %eax
.L316:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L317:
	testb	%al, %al
	jne	.L320
	movq	0(%rbp), %rax
	movq	%rdx, 16(%rsp)
	movq	24(%rax), %r13
	movq	%r13, %rdi
	call	get_scope
	movl	%eax, 12(%rsp)
	movq	(%rbx), %rax
	movq	24(%rax), %rdi
	call	get_scope
	movq	16(%rsp), %r11
	movl	%eax, 32(%rsp)
.L365:
	movq	precedence(%rip), %r14
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%r11, 16(%rsp)
	movq	%r14, %rsi
	call	match_prefix
	movl	%eax, %r13d
	movq	(%rbx), %rax
	xorl	%edx, %edx
	movq	%r14, %rsi
	movq	24(%rax), %rdi
	call	match_prefix
	cmpl	%eax, %r13d
	jg	.L369
	jl	.L320
	cmpb	$0, 37(%rbp)
	je	.L330
	movl	40(%rbp), %r13d
	movl	40(%rbx), %r14d
	cmpl	%r14d, %r13d
	je	.L330
	movl	44(%rbp), %esi
	movl	44(%rbx), %edi
	movq	16(%rsp), %r11
	cmpl	$-1, %esi
	movl	%esi, 56(%rsp)
	movl	%edi, 60(%rsp)
	je	.L375
	cmpl	$-1, %edi
	je	.L375
.L331:
	testl	%esi, %esi
	jne	.L421
	testl	%edi, %edi
	jne	.L320
	.p2align 4,,10
	.p2align 3
.L330:
	movl	32(%rsp), %ecx
	cmpl	%ecx, 12(%rsp)
	jl	.L369
	jg	.L320
	cmpb	$0, 37(%rbp)
	je	.L344
	movq	0(%rbp), %rcx
	movq	(%rbx), %rdx
	movl	4(%rcx), %eax
	cmpl	4(%rdx), %eax
	jne	.L344
	cmpl	$2, %eax
	je	.L422
	cmpl	$10, %eax
	jne	.L344
	cmpw	$10, 8(%rbp)
	jne	.L423
	cmpw	$10, 8(%rbx)
	jne	.L424
	movq	24(%rcx), %r8
	movq	24(%rdx), %r11
	movl	$8, %eax
.L359:
	movl	(%r8,%rax), %edx
	movl	8(%rbp,%rax), %ecx
	movl	(%r11,%rax), %edi
	movl	8(%rbx,%rax), %esi
	cmpl	%ecx, %edx
	jne	.L358
	cmpl	%edi, %esi
	jne	.L358
	addq	$4, %rax
	cmpq	$24, %rax
	jne	.L359
	.p2align 4,,10
	.p2align 3
.L344:
	cmpq	%r15, %r12
	jnb	.L320
	.p2align 4,,10
	.p2align 3
.L369:
	movl	$-1, %eax
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L372:
	movzbl	38(%rbx), %eax
	movzbl	38(%rbp), %edx
	movl	%eax, %esi
	andl	$1, %esi
	testb	$1, %dl
	jne	.L323
	testb	%sil, %sil
	jne	.L369
.L325:
	andl	$2, %eax
	andl	$2, %edx
	je	.L425
	testb	%al, %al
	je	.L369
.L327:
	movq	labels(%rip), %r13
	movq	%r8, %rdi
	movl	$2147483647, %edx
	movq	%r11, 40(%rsp)
	movq	%r13, %rsi
	call	match_prefix
	movq	%r14, %rdi
	movl	$2147483647, %edx
	movq	%r13, %rsi
	movl	%eax, 36(%rsp)
	call	match_prefix
	movl	%eax, %r14d
	movq	(%rbx), %rax
	movl	$2147483647, %edx
	movq	%r13, %rsi
	movq	24(%rax), %rdi
	call	match_prefix
	movq	24(%rsp), %rdi
	movl	$2147483647, %edx
	movq	%r13, %rsi
	movl	%eax, 16(%rsp)
	call	match_prefix
	movl	36(%rsp), %ecx
	movq	40(%rsp), %r11
	cmpl	%r14d, %ecx
	jne	.L373
	cmpl	%eax, 16(%rsp)
	jne	.L369
.L373:
	cmpl	%r14d, %ecx
	je	.L374
	cmpl	%eax, 16(%rsp)
	je	.L320
.L374:
	movq	0(%rbp), %rax
	movq	24(%rax), %r13
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L323:
	testb	%sil, %sil
	jne	.L325
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L425:
	testb	%al, %al
	je	.L327
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L375:
	cmpl	$-1, %esi
	je	.L426
	movl	$-1, %r13d
.L333:
	movl	$0, 60(%rsp)
.L334:
	leaq	60(%rsp), %rcx
	leaq	56(%rsp), %rsi
	movl	%r14d, %edx
	movl	%r13d, %edi
	movq	%r11, 16(%rsp)
	call	__check_native
	movq	16(%rsp), %r11
	movl	8(%r11), %eax
	testl	%eax, %eax
	jle	.L427
	subl	$1, %eax
	movl	60(%rsp), %edi
	movl	56(%rsp), %esi
	leaq	3(%rax,%rax,2), %rcx
	xorl	%eax, %eax
	salq	$4, %rcx
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L336:
	cmpl	$-1, %r14d
	je	.L338
	movq	(%r11), %rdx
	addq	%rax, %rdx
	cmpl	%r14d, 40(%rdx)
	je	.L428
.L338:
	addq	$48, %rax
	cmpq	%rcx, %rax
	je	.L331
.L340:
	cmpl	$-1, %r13d
	je	.L336
	movq	(%r11), %rdx
	addq	%rax, %rdx
	cmpl	%r13d, 40(%rdx)
	jne	.L336
	movl	44(%rdx), %r8d
	cmpl	$-1, %r8d
	je	.L337
	cmpl	%esi, %r8d
	jne	.L429
.L337:
	movl	%esi, 44(%rdx)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L428:
	movl	44(%rdx), %r8d
	cmpl	$-1, %r8d
	je	.L339
	cmpl	%edi, %r8d
	jne	.L430
.L339:
	movl	%edi, 44(%rdx)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L421:
	testl	%edi, %edi
	jne	.L330
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L426:
	cmpl	$-1, %edi
	movl	$0, 56(%rsp)
	je	.L333
	movl	%esi, %r14d
	jmp	.L334
.L422:
	cmpw	$2, 8(%rbp)
	jne	.L431
	cmpw	$2, 8(%rbx)
	jne	.L432
	movq	24(%rcx), %rax
	movl	12(%rbp), %edi
	movl	12(%rbx), %esi
	xorl	4(%rax), %edi
	movq	24(%rdx), %rax
	movl	$32, %edx
	movl	%edx, %ecx
	subb	39(%rbx), %cl
	xorl	4(%rax), %esi
	movl	$-1, %eax
	movl	%eax, %r11d
	bswap	%edi
	sall	%cl, %r11d
	movl	%edx, %ecx
	subb	39(%rbp), %cl
	bswap	%esi
	andl	%esi, %r11d
	sall	%cl, %eax
	testl	%edi, %eax
	jne	.L348
	xorl	%eax, %eax
	addl	$2147483616, %edx
	jmp	.L350
	.p2align 4,,10
	.p2align 3
.L434:
	addl	$1, %eax
	shrl	%edx
	cmpl	$32, %eax
	je	.L433
.L350:
	testl	%edx, %edi
	je	.L434
	testl	%r11d, %r11d
	je	.L366
	testl	%eax, %eax
	je	.L344
	jmp	.L369
	.p2align 4,,10
	.p2align 3
.L348:
	testl	%r11d, %r11d
	jne	.L344
	xorl	%eax, %eax
.L366:
	xorl	%edx, %edx
	movl	$-2147483648, %ecx
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L435:
	addl	$1, %edx
	shrl	%ecx
	cmpl	$32, %edx
	je	.L355
.L354:
	testl	%ecx, %esi
	je	.L435
.L353:
	cmpl	%edx, %eax
	jg	.L369
.L355:
	cmpl	%edx, %eax
	jl	.L320
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L358:
	xorl	%ecx, %edx
	xorl	%eax, %eax
	movl	%edx, %ecx
	movl	$-2147483648, %edx
	bswap	%ecx
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L436:
	addl	$1, %eax
	shrl	%edx
	cmpl	$32, %eax
	je	.L360
.L361:
	testl	%edx, %ecx
	je	.L436
.L360:
	xorl	%edi, %esi
	xorl	%edx, %edx
	movl	$-2147483648, %ecx
	bswap	%esi
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L437:
	addl	$1, %edx
	shrl	%ecx
	cmpl	$32, %edx
	je	.L355
.L362:
	testl	%ecx, %esi
	je	.L437
	jmp	.L353
.L427:
	movl	56(%rsp), %esi
	movl	60(%rsp), %edi
	jmp	.L331
.L433:
	testl	%r11d, %r11d
	je	.L366
	jmp	.L369
.L432:
	leaq	__PRETTY_FUNCTION__.14983(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC11(%rip), %rdi
	movl	$1542, %edx
	call	__GI___assert_fail
.L424:
	leaq	__PRETTY_FUNCTION__.14983(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$1573, %edx
	call	__GI___assert_fail
.L423:
	leaq	__PRETTY_FUNCTION__.14983(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$1572, %edx
	call	__GI___assert_fail
.L431:
	leaq	__PRETTY_FUNCTION__.14983(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$1541, %edx
	call	__GI___assert_fail
.L430:
	leaq	__PRETTY_FUNCTION__.14983(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$1512, %edx
	call	__GI___assert_fail
.L429:
	leaq	__PRETTY_FUNCTION__.14983(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$1506, %edx
	call	__GI___assert_fail
.LFE111:
	.size	rfc3484_sort, .-rfc3484_sort
	.p2align 4,,15
	.type	convert_hostent_to_gaih_addrtuple.isra.6, @function
convert_hostent_to_gaih_addrtuple.isra.6:
.LFB130:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %r13d
	subq	$40, %rsp
	movq	(%rcx), %rdx
	movq	%rdi, (%rsp)
	testq	%rdx, %rdx
	jne	.L440
	jmp	.L439
	.p2align 4,,10
	.p2align 3
.L447:
	movq	%rax, %rdx
.L440:
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L447
	movq	%rdx, %r15
.L439:
	movq	24(%r12), %rbp
	cmpq	$0, 0(%rbp)
	je	.L448
	xorl	%ebx, %ebx
	.p2align 4,,10
	.p2align 3
.L442:
	addq	$1, %rbx
	cmpq	$0, 0(%rbp,%rbx,8)
	jne	.L442
	testq	%rbx, %rbx
	movl	$1, %eax
	je	.L438
	cmpl	$16, 20(%r12)
	jbe	.L459
.L438:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L459:
	movl	$40, %esi
	movq	%rbx, %rdi
	call	calloc@PLT
	movq	%rax, %rcx
	movq	%rax, 16(%rsp)
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.L438
	leaq	(%rbx,%rbx,4), %rax
	leaq	40(%rcx), %r14
	xorl	%ebx, %ebx
	salq	$3, %rax
	movq	%rax, 24(%rsp)
	leaq	40(%rcx,%rax), %rax
	movq	%rax, 8(%rsp)
	jmp	.L446
	.p2align 4,,10
	.p2align 3
.L443:
	movslq	20(%r12), %rdx
	leaq	-20(%r14), %rdi
	movl	%r13d, -24(%r14)
	call	__GI_memcpy@PLT
.L444:
	movq	%r14, -40(%r14)
	addq	$8, %rbx
	addq	$40, %r14
	cmpq	8(%rsp), %r14
	je	.L445
	movq	24(%r12), %rbp
.L446:
	cmpl	$2, %r13d
	movq	0(%rbp,%rbx), %rsi
	jne	.L443
	movq	(%rsp), %rax
	cmpl	$10, (%rax)
	jne	.L443
	movl	$10, -24(%r14)
	movl	(%rsi), %eax
	movl	$-65536, -12(%r14)
	movl	%eax, -8(%r14)
	jmp	.L444
	.p2align 4,,10
	.p2align 3
.L448:
	addq	$40, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L445:
	movq	(%r12), %rax
	movq	16(%rsp), %rdi
	movq	24(%rsp), %rcx
	movq	%rax, 8(%rdi)
	movl	$1, %eax
	movq	$0, -40(%rdi,%rcx)
	movq	%rdi, (%r15)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.LFE130:
	.size	convert_hostent_to_gaih_addrtuple.isra.6, .-convert_hostent_to_gaih_addrtuple.isra.6
	.section	.rodata.str1.1
.LC14:
	.string	"dns [!UNAVAIL=return] files"
.LC15:
	.string	"hosts"
.LC16:
	.string	"gethostbyname4_r"
.LC17:
	.string	"gethostbyname3_r"
.LC18:
	.string	"gethostbyname2_r"
.LC19:
	.string	"getcanonname_r"
	.text
	.p2align 4,,15
	.type	gaih_inet.constprop.7, @function
gaih_inet.constprop.7:
.LFB131:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	nullserv(%rip), %r15
	pushq	%rbx
	movq	%rsi, %r14
	movq	%rdx, %r13
	movq	%r9, %r12
	subq	$424, %rsp
	cmpq	$0, 8(%rdx)
	movq	%rdi, -392(%rbp)
	movq	%rcx, -360(%rbp)
	movq	%r8, -408(%rbp)
	movq	%r15, -344(%rbp)
	movq	$0, -336(%rbp)
	je	.L461
	movl	8(%rdx), %edx
	leaq	20+gaih_inet_typeproto(%rip), %rsi
	jmp	.L462
	.p2align 4,,10
	.p2align 3
.L754:
	cmpl	(%rsi), %edx
	je	.L464
.L465:
	addq	$20, %rsi
	cmpb	$0, 10(%rsi)
	je	.L463
.L462:
	testl	%edx, %edx
	jne	.L754
.L464:
	movl	12(%r13), %eax
	movzbl	8(%rsi), %ecx
	testl	%eax, %eax
	je	.L466
	testb	$2, %cl
	jne	.L466
	cmpl	4(%rsi), %eax
	jne	.L465
.L466:
	xorl	%r8d, %r8d
	testq	%r14, %r14
	je	.L577
	andl	$1, %ecx
	je	.L755
.L468:
	movl	$8, %ebx
.L460:
	leaq	-40(%rbp), %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L461:
	xorl	%esi, %esi
	testq	%r14, %r14
	je	.L579
	movl	8(%r14), %esi
	testl	%esi, %esi
	js	.L756
	rolw	$8, %si
	movzwl	%si, %esi
.L579:
	leaq	-344(%rbp), %rdi
	leaq	20+gaih_inet_typeproto(%rip), %rax
	movl	$48, %r8d
	.p2align 4,,10
	.p2align 3
.L480:
	cmpb	$0, 9(%rax)
	je	.L479
#APP
# 441 "../sysdeps/posix/getaddrinfo.c" 1
	mov %rsp, %rcx
# 0 "" 2
#NO_APP
	subq	%r8, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
#APP
# 441 "../sysdeps/posix/getaddrinfo.c" 1
	sub %rsp , %rcx
# 0 "" 2
#NO_APP
	movl	(%rax), %ecx
	movq	$0, (%rdx)
	movl	%esi, 16(%rdx)
	movl	%ecx, 8(%rdx)
	movl	4(%rax), %ecx
	movl	%ecx, 12(%rdx)
	movq	%rdx, (%rdi)
	movq	%rdx, %rdi
.L479:
	addq	$20, %rax
	cmpb	$0, 10(%rax)
	jne	.L480
.L478:
	movq	-392(%rbp), %rdi
	movq	$0, -328(%rbp)
	testq	%rdi, %rdi
	je	.L481
#APP
# 461 "../sysdeps/posix/getaddrinfo.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	subq	$64, %rsp
	leaq	15(%rsp), %rsi
	andq	$-16, %rsi
#APP
# 461 "../sysdeps/posix/getaddrinfo.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	testb	$64, 0(%r13)
	movq	%rsi, -336(%rbp)
	movl	$0, 16(%rsi)
	movl	$0, 36(%rsi)
	movq	$0, (%rsi)
	je	.L586
	leaq	-304(%rbp), %rbx
	movq	%rbx, %rsi
	call	__GI___idna_to_dns_encoding
	testl	%eax, %eax
	jne	.L757
	movq	-304(%rbp), %rax
	movq	-336(%rbp), %rsi
	movb	$1, -417(%rbp)
	movq	%rax, -400(%rbp)
	jmp	.L482
	.p2align 4,,10
	.p2align 3
.L463:
	testl	%edx, %edx
	movl	$7, %ebx
	jne	.L460
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L586:
	movq	-392(%rbp), %rax
	movb	$0, -417(%rbp)
	movq	%rax, -400(%rbp)
.L482:
	movq	-400(%rbp), %rdi
	addq	$20, %rsi
	call	__GI___inet_aton_exact
	testl	%eax, %eax
	je	.L484
	testl	$-3, 4(%r13)
	jne	.L485
	movq	-336(%rbp), %r15
	movl	$2, 16(%r15)
.L486:
	testb	$2, 0(%r13)
	jne	.L631
	movq	%r15, %rax
	movb	$0, -418(%rbp)
	movq	$0, -416(%rbp)
	xorl	%r14d, %r14d
.L583:
	leaq	-304(%rbp), %rbx
	movq	%rbx, -432(%rbp)
	movq	%r14, %rbx
	movq	%r15, %r14
	cmpq	%r14, %rax
	movq	%rbx, %r15
	je	.L758
.L562:
	movl	16(%r14), %edx
	cmpw	$10, %dx
	movl	%edx, %ebx
	jne	.L623
	cmpb	$0, -418(%rbp)
	je	.L627
	movl	0(%r13), %eax
	andl	$24, %eax
	cmpl	$8, %eax
	jne	.L627
	movl	20(%r14), %ecx
	testl	%ecx, %ecx
	jne	.L627
	movl	24(%r14), %eax
	testl	%eax, %eax
	jne	.L627
	cmpl	$-65536, 28(%r14)
	je	.L568
	.p2align 4,,10
	.p2align 3
.L627:
	movq	$28, -368(%rbp)
.L567:
	movq	-344(%rbp), %r12
	testq	%r12, %r12
	je	.L569
	movq	-368(%rbp), %rax
	addq	$48, %rax
	movq	%rax, -376(%rbp)
	movzwl	%dx, %eax
	movl	%eax, -384(%rbp)
	movq	%r13, %rax
	movq	%r14, %r13
	movq	%r12, %r14
	movl	%ebx, %r12d
	movq	%rax, %rbx
	jmp	.L573
	.p2align 4,,10
	.p2align 3
.L761:
	movl	$0, 52(%rax)
	movdqu	20(%r13), %xmm0
	movups	%xmm0, 56(%rax)
	movl	36(%r13), %ecx
	movl	%ecx, 72(%rax)
.L572:
	movq	(%r14), %r14
	addq	$40, %rax
	xorl	%r15d, %r15d
	movq	%rax, -360(%rbp)
	testq	%r14, %r14
	je	.L759
.L573:
	movq	-376(%rbp), %rdi
	call	malloc@PLT
	movq	-360(%rbp), %rdi
	testq	%rax, %rax
	movq	%rax, (%rdi)
	je	.L760
	movl	(%rbx), %esi
	movl	-384(%rbp), %edi
	cmpw	$10, %r12w
	movl	16(%r14), %ecx
	movl	%esi, (%rax)
	movl	8(%r14), %esi
	movl	%edi, 4(%rax)
	movl	-368(%rbp), %edi
	movl	%esi, 8(%rax)
	movl	12(%r14), %esi
	movl	%edi, 16(%rax)
	movq	%r15, 32(%rax)
	movw	%r12w, 48(%rax)
	movq	$0, 40(%rax)
	movl	%esi, 12(%rax)
	leaq	48(%rax), %rsi
	movw	%cx, 50(%rax)
	movq	%rsi, 24(%rax)
	je	.L761
	movl	20(%r13), %ecx
	movq	$0, 56(%rax)
	movl	%ecx, 52(%rax)
	jmp	.L572
.L469:
	movzbl	8(%rsi), %ecx
	rolw	$8, %r8w
	movzwl	%r8w, %r8d
.L577:
#APP
# 424 "../sysdeps/posix/getaddrinfo.c" 1
	mov %rsp, %rdi
# 0 "" 2
#NO_APP
	subq	$48, %rsp
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
#APP
# 424 "../sysdeps/posix/getaddrinfo.c" 1
	sub %rsp , %rdi
# 0 "" 2
#NO_APP
	movl	(%rsi), %edi
	andl	$2, %ecx
	movq	%rdx, -344(%rbp)
	movq	$0, (%rdx)
	movl	%edi, 8(%rdx)
	je	.L762
	movl	%eax, 12(%rdx)
	movl	%r8d, 16(%rdx)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L484:
	movq	-336(%rbp), %r15
	movl	16(%r15), %eax
	testl	%eax, %eax
	jne	.L588
	movq	-400(%rbp), %rdi
	movl	$37, %esi
	call	__GI_strchr
	testq	%rax, %rax
	movq	%rax, %r14
	leaq	20(%r15), %rcx
	je	.L763
	movq	-400(%rbp), %rsi
	movq	%rax, %rdx
	movl	$10, %edi
	subq	%rsi, %rdx
	call	__GI___inet_pton_length
.L490:
	testl	%eax, %eax
	jle	.L764
	movl	4(%r13), %eax
	testl	%eax, %eax
	je	.L632
	cmpl	$10, %eax
	je	.L632
	cmpl	$2, %eax
	movl	$9, %ebx
	movq	$0, -416(%rbp)
	jne	.L487
	movq	-336(%rbp), %r15
	movl	20(%r15), %eax
	testl	%eax, %eax
	jne	.L487
	movl	24(%r15), %r11d
	testl	%r11d, %r11d
	jne	.L487
	cmpl	$-65536, 28(%r15)
	jne	.L487
	movl	32(%r15), %eax
	movl	$2, 16(%r15)
	movl	%eax, 20(%r15)
.L495:
	testq	%r14, %r14
	je	.L496
	leaq	36(%r15), %rdx
	leaq	1(%r14), %rsi
	leaq	20(%r15), %rdi
	call	__GI___inet6_scopeid_pton
	testl	%eax, %eax
	jne	.L596
	movq	-336(%rbp), %r15
.L496:
	testb	$2, 0(%r13)
	movl	$0, %edx
	movl	16(%r15), %eax
	cmovne	-400(%rbp), %rdx
	movq	%rdx, %r14
.L492:
	testl	%eax, %eax
	jne	.L753
	movl	0(%r13), %r10d
	andl	$4, %r10d
	jne	.L596
	movabsq	$-4294967294, %rax
	andq	0(%r13), %rax
	movabsq	$8589934592, %rdx
	cmpq	%rdx, %rax
	jne	.L497
	movq	__libc_h_errno@gottpoff(%rip), %r15
	leaq	-312(%rbp), %rcx
	leaq	-304(%rbp), %rbx
	movq	%r14, -368(%rbp)
	movq	%r13, -376(%rbp)
	movq	%rcx, %r13
	movq	%r15, %rax
	addq	%fs:0, %rax
	movq	%rax, %r14
	.p2align 4,,10
	.p2align 3
.L500:
	subq	$8, %rsp
	movq	8(%r12), %r8
	movq	-400(%rbp), %rdi
	pushq	%r14
	movq	(%r12), %rcx
	movq	%r13, %r9
	movq	%rbx, %rdx
	movl	$2, %esi
	call	__gethostbyname2_r
	cmpl	$34, %eax
	popq	%r9
	popq	%r10
	jne	.L498
	movl	%fs:(%r15), %eax
	cmpl	$-1, %eax
	jne	.L499
	movq	%r12, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L500
.L504:
	movl	$10, %ebx
	.p2align 4,,10
	.p2align 3
.L748:
	cmpb	$0, -417(%rbp)
	movq	$0, -416(%rbp)
	je	.L575
.L767:
	movq	-400(%rbp), %rdi
	call	free@PLT
	jmp	.L575
	.p2align 4,,10
	.p2align 3
.L762:
	movl	4(%rsi), %eax
	movl	%r8d, 16(%rdx)
	movl	%eax, 12(%rdx)
	jmp	.L478
	.p2align 4,,10
	.p2align 3
.L757:
	negl	%eax
	movl	%eax, %ebx
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L759:
	movq	%r13, %r14
	movq	%rbx, %r13
.L569:
	movq	-408(%rbp), %rax
	addl	$1, (%rax)
.L568:
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.L628
	movq	-336(%rbp), %rax
	cmpq	%r14, %rax
	jne	.L562
.L758:
	movl	0(%r13), %eax
	testb	$2, %al
	je	.L562
	testq	%r15, %r15
	cmove	-392(%rbp), %r15
	testb	$-128, %al
	jne	.L765
.L564:
	cmpq	$0, -416(%rbp)
	je	.L766
	movq	$0, -416(%rbp)
	jmp	.L562
	.p2align 4,,10
	.p2align 3
.L623:
	movq	$16, -368(%rbp)
	jmp	.L567
	.p2align 4,,10
	.p2align 3
.L760:
	movq	%r15, %rdi
	movl	$10, %ebx
	call	free@PLT
.L487:
	cmpb	$0, -417(%rbp)
	jne	.L767
.L575:
	movq	-328(%rbp), %rdi
	call	free@PLT
	movq	-416(%rbp), %rdi
	call	free@PLT
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L481:
#APP
# 944 "../sysdeps/posix/getaddrinfo.c" 1
	mov %rsp, %rdx
# 0 "" 2
#NO_APP
	movl	$64, %edi
	subq	%rdi, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %r15
#APP
# 944 "../sysdeps/posix/getaddrinfo.c" 1
	sub %rsp , %rdx
# 0 "" 2
#NO_APP
	movl	4(%r13), %esi
	pxor	%xmm0, %xmm0
	movq	%rax, -336(%rbp)
	movq	$0, 32(%rax)
	testl	%esi, %esi
	movaps	%xmm0, (%rax)
	movaps	%xmm0, 16(%rax)
	je	.L768
	cmpl	$2, %esi
	movq	%rax, %rdx
	sete	-418(%rbp)
	cmpl	$10, %esi
	je	.L769
.L560:
	cmpb	$0, -418(%rbp)
	je	.L618
	testb	$1, 0(%r13)
	movl	$2, 16(%rdx)
	jne	.L619
	movl	$16777343, 20(%rdx)
.L619:
	xorl	%r14d, %r14d
	movq	$0, -400(%rbp)
	movq	$0, -416(%rbp)
	movb	$0, -417(%rbp)
	movb	$0, -418(%rbp)
	jmp	.L488
	.p2align 4,,10
	.p2align 3
.L588:
	movq	$0, -416(%rbp)
	xorl	%r14d, %r14d
	movb	$0, -418(%rbp)
.L488:
	movq	%r15, %rax
	jmp	.L583
	.p2align 4,,10
	.p2align 3
.L485:
	movabsq	$-4294967288, %rax
	andq	0(%r13), %rax
	movabsq	$42949672968, %rdx
	movl	$9, %ebx
	movq	$0, -416(%rbp)
	cmpq	%rdx, %rax
	jne	.L487
	movq	-336(%rbp), %r15
	movl	20(%r15), %eax
	movq	$10, 16(%r15)
	movl	%eax, 32(%r15)
	movabsq	$-281474976710656, %rax
	movq	%rax, 24(%r15)
	jmp	.L486
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	20+gaih_inet_typeproto(%rip), %rbx
.L580:
	leaq	-344(%rbp), %rax
	movq	%r14, -376(%rbp)
	movq	%r13, %r14
	movq	%r12, %r13
	movq	%rax, -368(%rbp)
	.p2align 4,,10
	.p2align 3
.L475:
	movzbl	8(%rbx), %eax
	testb	$1, %al
	jne	.L472
	movl	8(%r14), %edx
	testl	%edx, %edx
	je	.L473
	cmpl	(%rbx), %edx
	jne	.L472
.L473:
	movl	12(%r14), %edx
	testl	%edx, %edx
	je	.L474
	testb	$2, %al
	jne	.L474
	cmpl	4(%rbx), %edx
	jne	.L472
.L474:
#APP
# 398 "../sysdeps/posix/getaddrinfo.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	subq	$48, %rsp
	leaq	15(%rsp), %r9
	andq	$-16, %r9
	movq	%r9, %r12
#APP
# 398 "../sysdeps/posix/getaddrinfo.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	movq	-376(%rbp), %rax
	leaq	12(%r14), %rdx
	movq	%r13, %r8
	movq	%r9, %rcx
	movq	%rbx, %rsi
	movq	(%rax), %rdi
	call	gaih_inet_serv.isra.4
	testl	%eax, %eax
	jne	.L472
	movq	-368(%rbp), %rax
	movq	%r12, -368(%rbp)
	movq	%r12, (%rax)
.L472:
	addq	$20, %rbx
	cmpb	$0, 10(%rbx)
	jne	.L475
	cmpq	%r15, -344(%rbp)
	movq	%r13, %r12
	movq	%r14, %r13
	jne	.L478
	jmp	.L468
	.p2align 4,,10
	.p2align 3
.L618:
	movb	$0, -417(%rbp)
	xorl	%r14d, %r14d
	movq	$0, -400(%rbp)
	movq	$0, -416(%rbp)
	jmp	.L488
.L765:
	movq	-432(%rbp), %rsi
	movq	%r15, %rdi
	call	__GI___idna_from_dns_encoding
	testl	%eax, %eax
	jne	.L565
	movq	-304(%rbp), %r15
	jmp	.L562
.L632:
	movq	-336(%rbp), %r15
	movl	$10, 16(%r15)
	jmp	.L495
.L755:
	movl	8(%r14), %r8d
	testl	%r8d, %r8d
	jns	.L469
	cmpb	$0, 10(%rsi)
	je	.L470
#APP
# 373 "../sysdeps/posix/getaddrinfo.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	subq	$48, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
#APP
# 373 "../sysdeps/posix/getaddrinfo.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	movq	(%r14), %rdi
	leaq	12(%r13), %rdx
	movq	%r12, %r8
	movq	%rcx, -344(%rbp)
	call	gaih_inet_serv.isra.4
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L478
	jmp	.L460
	.p2align 4,,10
	.p2align 3
.L596:
	movl	$2, %ebx
	jmp	.L748
.L631:
	movq	-400(%rbp), %r14
.L753:
	movq	$0, -416(%rbp)
	movb	$0, -418(%rbp)
	jmp	.L488
.L497:
	leaq	-320(%rbp), %rcx
	leaq	.LC14(%rip), %rdx
	leaq	.LC15(%rip), %rdi
	xorl	%esi, %esi
	movl	%r10d, -368(%rbp)
	call	__GI___nss_database_lookup2
	movl	%eax, %ebx
	call	__GI___resolv_context_get
	testq	%rax, %rax
	movq	%rax, -448(%rbp)
	movl	-368(%rbp), %r10d
	je	.L507
	testl	%ebx, %ebx
	jne	.L507
	leaq	-336(%rbp), %rax
	movq	-320(%rbp), %rdi
	movl	$-1, %r8d
	movl	$0, -460(%rbp)
	movl	%r8d, -424(%rbp)
	movq	%r12, %r15
	movq	%rax, -440(%rbp)
	leaq	-312(%rbp), %rax
	movb	$0, -418(%rbp)
	movq	$0, -416(%rbp)
	movl	%r8d, -432(%rbp)
	movq	%rax, -384(%rbp)
	movq	%r14, -456(%rbp)
	movq	%r13, -368(%rbp)
.L555:
	movq	-368(%rbp), %rax
	movl	4(%rax), %r8d
	testl	%r8d, %r8d
	je	.L770
.L508:
	movq	-368(%rbp), %rax
	testb	$2, (%rax)
	je	.L521
	leaq	.LC17(%rip), %rsi
	call	__GI___nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L771
.L522:
	movq	-368(%rbp), %rax
	movl	4(%rax), %eax
	cmpl	$10, %eax
	je	.L633
	xorl	%r13d, %r13d
	testl	%eax, %eax
	je	.L633
.L524:
	andl	$-3, %eax
	je	.L535
	movq	-368(%rbp), %rbx
	movabsq	$-4294967288, %rax
	andq	(%rbx), %rax
	movabsq	$42949672968, %rbx
	cmpq	%rbx, %rax
	je	.L536
.L747:
	cmpl	$1, -424(%rbp)
	sete	%al
	cmpl	$1, -432(%rbp)
	sete	%bl
.L537:
	testb	%bl, %bl
	movq	-320(%rbp), %rdx
	jne	.L634
	testb	%al, %al
	jne	.L634
	movl	-424(%rbp), %eax
	cmpl	$-2, %eax
	je	.L613
	cmpl	$-1, -432(%rbp)
	jne	.L635
	cmpl	$-1, %eax
	je	.L635
	movl	-424(%rbp), %eax
	leal	4(%rax,%rax), %ecx
	movl	%eax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L520:
	movl	8(%rdx), %eax
	shrl	%cl, %eax
	andl	$3, %eax
	cmpl	$1, %eax
	je	.L554
	cmpq	$0, 16(%rdx)
	leaq	16(%rdx), %rdi
	movq	%rdi, -320(%rbp)
	jne	.L555
.L554:
	movl	-432(%rbp), %r8d
	movq	-448(%rbp), %rdi
	movl	%r13d, -376(%rbp)
	movq	-456(%rbp), %r14
	movq	-368(%rbp), %r13
	movl	%r8d, -384(%rbp)
	call	__GI___resolv_context_put
	movl	-384(%rbp), %r8d
	movl	-376(%rbp), %r10d
	cmpl	$-2, %r8d
	jb	.L556
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L771:
	movq	-320(%rbp), %rdi
.L521:
	leaq	.LC18(%rip), %rsi
	call	__GI___nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.L522
	movq	__libc_errno@gottpoff(%rip), %r13
	movq	__libc_h_errno@gottpoff(%rip), %rbx
	movl	$2, %ecx
	movq	-320(%rbp), %rdx
	movl	$-1, -432(%rbp)
	movl	$16, %fs:0(%r13)
	movl	$-1, %fs:(%rbx)
	xorl	%r13d, %r13d
	jmp	.L520
.L770:
	leaq	.LC16(%rip), %rsi
	call	__GI___nss_lookup_function
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L738
	movq	%fs:0, %r14
	movq	__libc_h_errno@gottpoff(%rip), %rbx
	movq	__libc_errno@gottpoff(%rip), %r13
	leaq	(%r14,%rbx), %rax
	movq	%rax, -376(%rbp)
.L509:
	movq	%r12, %rdi
	call	__GI__dl_mcount_wrapper_check
	subq	$8, %rsp
	movq	8(%r15), %rcx
	movq	-440(%rbp), %rsi
	pushq	$0
	movq	-400(%rbp), %rdi
	leaq	(%r14,%r13), %r8
	movq	-376(%rbp), %r9
	movq	(%r15), %rdx
	call	*%r12
	cmpl	$1, %eax
	popq	%rsi
	popq	%rdi
	je	.L510
	cmpl	$-2, %eax
	movl	%fs:(%rbx), %edx
	jne	.L511
	cmpl	$34, %fs:0(%r13)
	jne	.L511
	cmpl	$-1, %edx
	jne	.L511
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L509
.L750:
	movq	-448(%rbp), %rdi
	movl	$10, %ebx
	call	__GI___resolv_context_put
	jmp	.L487
.L768:
	subq	%rdi, %rsp
	movb	$1, -418(%rbp)
	leaq	15(%rsp), %rdx
	andq	$-16, %rdx
	movaps	%xmm0, (%rdx)
	movq	%rdx, (%rax)
	movq	$0, 32(%rdx)
	movaps	%xmm0, 16(%rdx)
.L559:
	testb	$1, 0(%r13)
	movl	$10, 16(%rax)
	jne	.L560
	movdqu	__GI_in6addr_loopback(%rip), %xmm0
	movups	%xmm0, 20(%rax)
	jmp	.L560
.L764:
	movq	-336(%rbp), %r15
	xorl	%r14d, %r14d
	movl	16(%r15), %eax
	jmp	.L492
.L763:
	movq	-400(%rbp), %rsi
	movq	%rcx, %rdx
	movl	$10, %edi
	call	__GI_inet_pton
	jmp	.L490
.L536:
	movq	-368(%rbp), %rax
	testb	$16, (%rax)
	jne	.L535
	cmpb	$0, -418(%rbp)
	jne	.L747
.L535:
	movq	%fs:0, %r14
	movq	__libc_errno@gottpoff(%rip), %r13
	leaq	-304(%rbp), %rax
	movq	__libc_h_errno@gottpoff(%rip), %rbx
	movq	$0, -312(%rbp)
	movq	%rax, -376(%rbp)
	leaq	(%r14,%r13), %rax
	movq	%rax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L541:
	movq	%r12, %rdi
	call	__GI__dl_mcount_wrapper_check
	leaq	(%r14,%rbx), %rax
	subq	$8, %rsp
	pushq	-384(%rbp)
	pushq	$0
	movq	-432(%rbp), %r9
	movl	$2, %esi
	pushq	%rax
	movq	8(%r15), %r8
	movq	(%r15), %rcx
	movq	-376(%rbp), %rdx
	movq	-400(%rbp), %rdi
	call	*%r12
	addq	$32, %rsp
	cmpl	$-2, %eax
	jne	.L538
	movl	%fs:(%rbx), %edx
	cmpl	$-1, %edx
	jne	.L745
	cmpl	$34, %fs:0(%r13)
	jne	.L751
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L541
	jmp	.L750
.L633:
	movq	%fs:0, %r14
	movq	__libc_errno@gottpoff(%rip), %r13
	leaq	-304(%rbp), %rax
	movq	__libc_h_errno@gottpoff(%rip), %rbx
	movq	$0, -312(%rbp)
	movq	%rax, -376(%rbp)
	leaq	(%r14,%r13), %rax
	movq	%rax, -432(%rbp)
	.p2align 4,,10
	.p2align 3
.L529:
	movq	%r12, %rdi
	call	__GI__dl_mcount_wrapper_check
	leaq	(%r14,%rbx), %rax
	subq	$8, %rsp
	pushq	-384(%rbp)
	pushq	$0
	movq	-432(%rbp), %r9
	movl	$10, %esi
	pushq	%rax
	movq	8(%r15), %r8
	movq	(%r15), %rcx
	movq	-376(%rbp), %rdx
	movq	-400(%rbp), %rdi
	call	*%r12
	addq	$32, %rsp
	cmpl	$-2, %eax
	jne	.L526
	movl	%fs:(%rbx), %edx
	cmpl	$-1, %edx
	jne	.L744
	cmpl	$34, %fs:0(%r13)
	jne	.L751
	movq	%r15, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L529
	jmp	.L750
.L634:
	movq	-368(%rbp), %rax
	testb	$2, (%rax)
	je	.L611
	cmpq	$0, -456(%rbp)
	je	.L772
.L611:
	movl	$6, %ecx
	movl	$1, -432(%rbp)
	jmp	.L520
.L769:
	movq	(%rax), %rdx
	movb	$0, -418(%rbp)
	jmp	.L559
.L738:
	movq	-320(%rbp), %rdi
	jmp	.L508
.L565:
	cmpl	$-105, %eax
	je	.L564
	negl	%eax
	movl	%eax, %ebx
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L507:
	movq	-448(%rbp), %rdi
	movl	%r10d, -368(%rbp)
	call	__GI___resolv_context_put
	movl	-368(%rbp), %r10d
	movl	$0, -460(%rbp)
	movq	$0, -416(%rbp)
	movb	$0, -418(%rbp)
.L582:
	movq	__libc_h_errno@gottpoff(%rip), %rax
	movl	$11, %ebx
	cmpl	$-1, %fs:(%rax)
	je	.L487
.L556:
	testl	%r10d, %r10d
	je	.L557
	movl	-460(%rbp), %eax
	testl	%eax, %eax
	je	.L557
	cmpl	$-3, %r10d
	jne	.L615
	cmpl	$-3, %eax
	jne	.L615
	movl	$3, %ebx
	jmp	.L487
.L498:
	testl	%eax, %eax
	movq	-368(%rbp), %r14
	movq	-376(%rbp), %r13
	jne	.L502
	movq	-312(%rbp), %rdx
	testq	%rdx, %rdx
	je	.L503
	leaq	-328(%rbp), %rcx
	leaq	4(%r13), %rdi
	movl	$2, %esi
	call	convert_hostent_to_gaih_addrtuple.isra.6
	testb	%al, %al
	je	.L504
	movq	-328(%rbp), %r15
	movq	$0, -416(%rbp)
	movb	$0, -418(%rbp)
	movl	16(%r15), %eax
	movq	%r15, -336(%rbp)
.L505:
	testl	%eax, %eax
	jne	.L488
	movl	$2, %ebx
	jmp	.L487
.L511:
	cmpl	$2, %edx
	movl	%eax, -432(%rbp)
	movl	%edx, %eax
	je	.L773
	xorl	%r13d, %r13d
	cmpl	$4, %eax
	movl	-432(%rbp), %eax
	sete	%r13b
	movq	-320(%rbp), %rdx
	movl	%r13d, -460(%rbp)
	leal	4(%rax,%rax), %ecx
	jmp	.L520
.L628:
	xorl	%ebx, %ebx
	jmp	.L487
.L613:
	movl	-424(%rbp), %eax
	xorl	%ecx, %ecx
	movl	%eax, -432(%rbp)
	jmp	.L520
.L635:
	movl	-432(%rbp), %eax
	leal	4(%rax,%rax), %ecx
	jmp	.L520
.L502:
	movl	%fs:(%r15), %eax
	movl	$11, %ebx
	cmpl	$-1, %eax
	je	.L748
.L499:
	xorl	%ebx, %ebx
	cmpl	$2, %eax
	setne	%bl
	leal	3(%rbx,%rbx), %ebx
	jmp	.L748
.L766:
	movq	%r15, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.L562
	movl	$10, %ebx
	jmp	.L487
	.p2align 4,,10
	.p2align 3
.L538:
	movl	%eax, -432(%rbp)
	addl	$1, %eax
	cmpl	$1, %eax
	jbe	.L774
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	cmpl	$1, -432(%rbp)
	je	.L775
.L544:
	movq	-368(%rbp), %rax
	cmpl	$2, 4(%rax)
	je	.L610
	cmpl	$1, -424(%rbp)
	sete	%al
	jmp	.L537
.L610:
	movl	-432(%rbp), %ecx
	movl	%ebx, %eax
	movl	%r13d, -460(%rbp)
	movl	%ecx, -424(%rbp)
	jmp	.L537
.L745:
	movl	%eax, -432(%rbp)
	movl	%edx, %eax
.L539:
	cmpl	$2, %eax
	je	.L606
	xorl	%r13d, %r13d
	cmpl	$4, %eax
	sete	%r13b
	xorl	%ebx, %ebx
	jmp	.L544
.L503:
	xorl	%ebx, %ebx
	cmpl	$4, %fs:(%r15)
	sete	%bl
	leal	2(%rbx,%rbx,2), %ebx
	jmp	.L748
.L744:
	movl	%eax, -432(%rbp)
	movl	%edx, %eax
.L527:
	cmpl	$2, %eax
	movl	$-3, %r13d
	je	.L532
	xorl	%r13d, %r13d
	cmpl	$4, %eax
	sete	%r13b
.L532:
	movq	-368(%rbp), %rax
	movl	-432(%rbp), %ebx
	movl	%r13d, -460(%rbp)
	movl	4(%rax), %eax
	movl	%ebx, -424(%rbp)
	jmp	.L524
.L526:
	movl	%eax, -432(%rbp)
	addl	$1, %eax
	cmpl	$1, %eax
	jbe	.L776
	xorl	%r13d, %r13d
	cmpl	$1, -432(%rbp)
	jne	.L532
	movq	-368(%rbp), %rax
	movq	-376(%rbp), %rdx
	leaq	-328(%rbp), %rcx
	movl	$10, %esi
	leaq	4(%rax), %rdi
	call	convert_hostent_to_gaih_addrtuple.isra.6
	testb	%al, %al
	movl	%eax, %ebx
	je	.L751
	cmpq	$0, -456(%rbp)
	movq	-328(%rbp), %rax
	movq	-440(%rbp), %rcx
	movq	-312(%rbp), %rdi
	movq	%rax, (%rcx)
	jne	.L534
	testq	%rdi, %rdi
	jne	.L777
.L534:
	movq	-440(%rbp), %rax
	xorl	%r13d, %r13d
	cmpq	$0, (%rax)
	movzbl	-418(%rbp), %eax
	cmovne	%ebx, %eax
	movb	%al, -418(%rbp)
	jmp	.L532
.L606:
	xorl	%ebx, %ebx
	movl	$-3, %r13d
	jmp	.L544
.L773:
	movl	-432(%rbp), %eax
	movl	$-3, %r13d
	movq	-320(%rbp), %rdx
	movl	%r13d, -460(%rbp)
	leal	4(%rax,%rax), %ecx
	jmp	.L520
.L510:
	movq	-368(%rbp), %rbx
	movl	%eax, -432(%rbp)
	movq	-440(%rbp), %rax
	testb	$2, (%rbx)
	movq	(%rax), %rax
	je	.L512
	cmpq	$0, -456(%rbp)
	jne	.L512
	movq	8(%rax), %rbx
	movq	%rbx, -456(%rbp)
.L512:
	movl	$1, %r13d
	movl	$1, %edi
	movzbl	-418(%rbp), %r11d
	movq	-440(%rbp), %r9
	movq	-368(%rbp), %r10
	jmp	.L518
.L781:
	cmpl	%esi, %edx
	je	.L778
	movq	%rcx, (%r9)
.L514:
	movq	%rcx, %rax
.L518:
	testq	%rax, %rax
	je	.L779
	movl	16(%rax), %edx
	movq	(%rax), %rcx
	cmpl	$2, %edx
	je	.L780
.L513:
	movl	4(%r10), %esi
	testl	%esi, %esi
	jne	.L781
	movq	%rax, %r9
	xorl	%r13d, %r13d
	jmp	.L514
.L780:
	movabsq	$-4294967288, %rsi
	andq	(%r10), %rsi
	movabsq	$42949672968, %rbx
	cmpq	%rbx, %rsi
	jne	.L513
	movl	20(%rax), %edx
	movl	$10, 16(%rax)
	movq	%rax, %r9
	movq	$0, 20(%rax)
	movl	$-65536, 28(%rax)
	xorl	%r13d, %r13d
	movl	%edx, 32(%rax)
	jmp	.L514
.L779:
	movb	%r11b, -418(%rbp)
	movq	%r9, -440(%rbp)
	movl	$6, %ecx
	movq	-320(%rbp), %rdx
	movl	%r13d, -460(%rbp)
	jmp	.L520
.L778:
	xorl	%r13d, %r13d
	cmpl	$10, %edx
	movq	%rax, %r9
	cmove	%edi, %r11d
	jmp	.L514
.L772:
	leaq	.LC19(%rip), %rsi
	movq	%rdx, %rdi
	movq	-336(%rbp), %rbx
	call	__GI___nss_lookup_function
	movq	-400(%rbp), %r14
	testq	%rax, %rax
	movq	%rax, %r12
	movq	%r14, -312(%rbp)
	je	.L548
	movq	%rax, %rdi
	call	__GI__dl_mcount_wrapper_check
	movq	8(%rbx), %rdi
	movq	__libc_h_errno@gottpoff(%rip), %r9
	movq	%r14, %rbx
	movq	__libc_errno@gottpoff(%rip), %r8
	leaq	-304(%rbp), %rsi
	movq	-384(%rbp), %rcx
	movl	$256, %edx
	testq	%rdi, %rdi
	cmove	%r14, %rdi
	movq	%fs:0, %r14
	addq	%r14, %r9
	addq	%r14, %r8
	call	*%r12
	subl	$1, %eax
	je	.L548
	movq	%rbx, -312(%rbp)
.L548:
	movq	-400(%rbp), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, -456(%rbp)
	je	.L551
	movq	-320(%rbp), %rdx
	movq	%rax, -416(%rbp)
	movl	$6, %ecx
	movl	$1, -432(%rbp)
	jmp	.L520
.L557:
	movq	-336(%rbp), %r15
	movl	16(%r15), %eax
	jmp	.L505
.L615:
	movl	$5, %ebx
	jmp	.L487
.L775:
	movq	-368(%rbp), %rax
	movq	-376(%rbp), %rdx
	leaq	-328(%rbp), %rcx
	movl	$2, %esi
	leaq	4(%rax), %rdi
	call	convert_hostent_to_gaih_addrtuple.isra.6
	testb	%al, %al
	je	.L751
	cmpq	$0, -456(%rbp)
	movq	-328(%rbp), %rdx
	movq	-440(%rbp), %rbx
	movq	-312(%rbp), %rdi
	movq	%rdx, (%rbx)
	sete	%bl
	testq	%rdi, %rdi
	setne	%dl
	andb	%dl, %bl
	jne	.L782
	movl	%eax, %ebx
	jmp	.L544
.L774:
	movl	%fs:(%rbx), %eax
	cmpl	$-1, %eax
	jne	.L539
.L751:
	movq	-448(%rbp), %rdi
	movl	$11, %ebx
	call	__GI___resolv_context_put
	jmp	.L487
.L470:
	cmpb	$0, 30(%rsi)
	leaq	20(%rsi), %rbx
	jne	.L580
	jmp	.L468
.L782:
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, -456(%rbp)
	je	.L752
	movq	-456(%rbp), %rax
	movq	%rax, -416(%rbp)
	jmp	.L544
.L551:
	movq	-448(%rbp), %rdi
	movl	$10, %ebx
	call	__GI___resolv_context_put
	movq	$0, -416(%rbp)
	jmp	.L487
.L752:
	movq	-448(%rbp), %rdi
	movl	$11, %ebx
	call	__GI___resolv_context_put
	movq	$0, -416(%rbp)
	jmp	.L487
.L776:
	movl	%fs:(%rbx), %eax
	cmpl	$-1, %eax
	jne	.L527
	jmp	.L751
.L777:
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, -456(%rbp)
	je	.L752
	movq	-456(%rbp), %rax
	movq	%rax, -416(%rbp)
	jmp	.L534
.LFE131:
	.size	gaih_inet.constprop.7, .-gaih_inet.constprop.7
	.p2align 4,,15
	.globl	__GI_freeaddrinfo
	.hidden	__GI_freeaddrinfo
	.type	__GI_freeaddrinfo, @function
__GI_freeaddrinfo:
.LFB123:
	testq	%rdi, %rdi
	je	.L791
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L785:
	movq	32(%rbx), %rdi
	movq	40(%rbx), %rbp
	call	free@PLT
	movq	%rbx, %rdi
	movq	%rbp, %rbx
	call	free@PLT
	testq	%rbp, %rbp
	jne	.L785
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	rep ret
.LFE123:
	.size	__GI_freeaddrinfo, .-__GI_freeaddrinfo
	.globl	freeaddrinfo
	.set	freeaddrinfo,__GI_freeaddrinfo
	.section	.rodata.str1.8
	.align 8
.LC20:
	.string	"IN6_IS_ADDR_V4MAPPED (sin6->sin6_addr.s6_addr32)"
	.section	.rodata.str1.1
.LC21:
	.string	"canonname == NULL"
	.text
	.p2align 4,,15
	.globl	__GI_getaddrinfo
	.hidden	__GI_getaddrinfo
	.type	__GI_getaddrinfo, @function
__GI_getaddrinfo:
.LFB122:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbx
	movq	%rsi, %r12
	subq	$1432, %rsp
	testq	%rdi, %rdi
	movq	%rcx, -1416(%rbp)
	movq	$0, -1336(%rbp)
	je	.L795
	cmpb	$42, (%rdi)
	movq	%rdi, %r14
	je	.L950
.L796:
	testq	%r12, %r12
	jne	.L951
.L797:
	testq	%r13, %r13
	je	.L800
	movl	0(%r13), %eax
	testl	$-2048, %eax
	jne	.L869
	testb	$2, %al
	je	.L879
	testq	%r14, %r14
	je	.L869
.L879:
	movl	4(%r13), %edx
	testl	$-3, %edx
	setne	%bl
	cmpl	$10, %edx
	setne	%dl
	andb	%dl, %bl
	jne	.L870
	testb	$32, %al
	movq	$0, -1328(%rbp)
	movq	$0, -1320(%rbp)
	movb	$0, -1342(%rbp)
	movb	$0, -1341(%rbp)
	jne	.L952
.L802:
	testq	%r12, %r12
	je	.L873
	cmpb	$0, (%r12)
	jne	.L953
.L873:
	leaq	-1088(%rbp), %r9
	xorl	%esi, %esi
.L808:
	leaq	16(%r9), %r12
	leaq	-1336(%rbp), %rcx
	leaq	-1340(%rbp), %r8
	movq	%r14, %rdi
	movq	%r13, %rdx
	movl	$0, -1340(%rbp)
	movq	%r12, -1088(%rbp)
	movq	$1024, -1080(%rbp)
	call	gaih_inet.constprop.7
	movq	-1088(%rbp), %rdi
	movl	%eax, -1380(%rbp)
	cmpq	%r12, %rdi
	je	.L811
	call	free@PLT
.L811:
	movl	-1380(%rbp), %r12d
	movq	-1336(%rbp), %rdi
	testl	%r12d, %r12d
	jne	.L812
	xorl	%eax, %eax
	testq	%rdi, %rdi
	movl	$0, -1384(%rbp)
	je	.L954
	.p2align 4,,10
	.p2align 3
.L813:
	movq	40(%rdi), %rdi
	addl	$1, %eax
	testq	%rdi, %rdi
	jne	.L813
	movl	%eax, -1384(%rbp)
.L814:
	cmpl	$1, -1340(%rbp)
	jbe	.L815
	movl	__libc_pthread_functions_init(%rip), %r11d
	movl	once.15181(%rip), %eax
	testl	%r11d, %r11d
	movl	%eax, -1460(%rbp)
	je	.L816
	movq	128+__libc_pthread_functions(%rip), %rax
	leaq	gaiconf_init(%rip), %rsi
	leaq	once.15181(%rip), %rdi
#APP
# 2261 "../sysdeps/posix/getaddrinfo.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L817:
	movslq	-1384(%rbp), %rax
	leaq	0(,%rax,8), %r12
	movq	%rax, -1424(%rbp)
	subq	%rax, %r12
	salq	$3, %r12
	movq	%r12, %rdi
	call	__GI___libc_alloca_cutoff
	cmpq	$4096, %r12
	jbe	.L818
	testl	%eax, %eax
	je	.L955
.L818:
	addq	$30, %r12
	movl	$1, -1392(%rbp)
	andq	$-16, %r12
	subq	%r12, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, -1400(%rbp)
.L819:
	movq	-1424(%rbp), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	addq	-1400(%rbp), %rax
	testb	%bl, %bl
	movq	%rax, -1360(%rbp)
	je	.L956
.L820:
	movq	-1328(%rbp), %rdi
	testq	%rdi, %rdi
	je	.L821
	movq	-1320(%rbp), %rsi
	leaq	in6aicmp(%rip), %rcx
	movl	$24, %edx
	call	__GI_qsort
.L821:
	movq	-1336(%rbp), %r15
	testq	%r15, %r15
	je	.L876
	movq	-1400(%rbp), %rax
	leaq	-1232(%rbp), %rcx
	leaq	-1296(%rbp), %rsi
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movl	$-1, %r12d
	movq	%rcx, -1368(%rbp)
	addq	$8, %rcx
	movq	$0, -1352(%rbp)
	leaq	8(%rax), %r14
	movq	%rsi, -1408(%rbp)
	xorl	%eax, %eax
	movq	%rcx, -1376(%rbp)
	jmp	.L849
	.p2align 4,,10
	.p2align 3
.L962:
	cmpl	$2, %r13d
	jne	.L830
	cmpl	$10, 4(%r15)
	je	.L831
.L830:
	pxor	%xmm0, %xmm0
	movq	-1368(%rbp), %rsi
	movl	$16, %edx
	movl	%r12d, %edi
	movaps	%xmm0, -1232(%rbp)
	call	__GI___connect
	movl	$28, -1296(%rbp)
.L832:
	movl	16(%r15), %edx
	movq	24(%r15), %rsi
	movl	%r12d, %edi
	call	__GI___connect
	testl	%eax, %eax
	je	.L957
.L833:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$97, %fs:(%rax)
	jne	.L846
	cmpl	$10, %r13d
	je	.L958
.L846:
	movb	$0, 28(%r14)
.L828:
	movq	32(%r15), %rax
	testq	%rax, %rax
	je	.L847
	cmpq	$0, -1352(%rbp)
	jne	.L959
	movq	$0, 32(%r15)
	movq	%rax, -1352(%rbp)
.L847:
	movq	40(%r15), %rdx
	addq	$1, %rbx
	addq	$48, %r14
	movq	%r15, %rax
	testq	%rdx, %rdx
	je	.L960
	movq	%rdx, %r15
.L849:
	movq	-1360(%rbp), %rcx
	testq	%rax, %rax
	movq	%r15, -8(%r14)
	movl	$-1, 36(%r14)
	movq	%rbx, (%rcx,%rbx,8)
	je	.L823
	movl	16(%rax), %edx
	cmpl	16(%r15), %edx
	je	.L961
.L823:
	cmpl	$-1, %r12d
	movb	$0, 29(%r14)
	movb	$0, 30(%r14)
	movb	$0, 31(%r14)
	movl	$-1, 32(%r14)
	jne	.L962
.L829:
	movl	4(%r15), %r13d
	xorl	%edx, %edx
	movl	$524290, %esi
	movl	%r13d, %edi
	call	__GI___socket
	cmpl	$-1, %eax
	movl	%eax, %r12d
	movl	$28, -1296(%rbp)
	je	.L833
	jmp	.L832
	.p2align 4,,10
	.p2align 3
.L951:
	cmpb	$42, (%r12)
	jne	.L797
	cmpb	$0, 1(%r12)
	movl	$0, %eax
	cmove	%rax, %r12
	jmp	.L797
	.p2align 4,,10
	.p2align 3
.L950:
	cmpb	$0, 1(%rdi)
	jne	.L796
.L795:
	testq	%r12, %r12
	je	.L947
	xorl	%r14d, %r14d
	cmpb	$42, (%r12)
	jne	.L797
	cmpb	$0, 1(%r12)
	jne	.L797
.L947:
	movl	$-2, -1380(%rbp)
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L958:
	cmpl	$2, 4(%r15)
	jne	.L846
.L831:
	movl	%r12d, %edi
	call	__GI___close_nocancel
	jmp	.L829
	.p2align 4,,10
	.p2align 3
.L961:
	movq	24(%r15), %rsi
	movq	24(%rax), %rdi
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	jne	.L823
	movzbl	-20(%r14), %eax
	leaq	-48(%r14), %rsi
	cmpl	$8, %eax
	movq	%rax, %rdx
	jnb	.L824
	testb	$4, %al
	jne	.L963
	testl	%eax, %eax
	jne	.L964
.L825:
	movzbl	-19(%r14), %eax
	movb	%dl, 28(%r14)
	movb	%al, 29(%r14)
	movzbl	-18(%r14), %eax
	movb	%al, 30(%r14)
	movzbl	-17(%r14), %eax
	movb	%al, 31(%r14)
	movl	-16(%r14), %eax
	movl	%eax, 32(%r14)
	jmp	.L828
	.p2align 4,,10
	.p2align 3
.L957:
	movq	-1408(%rbp), %rdx
	movq	%r14, %rsi
	movl	%r12d, %edi
	call	__getsockname
	testl	%eax, %eax
	jne	.L833
	movl	-1296(%rbp), %eax
	movb	$1, 29(%r14)
	movb	%al, 28(%r14)
	cmpl	$2, 4(%r15)
	movq	-1328(%rbp), %rax
	sete	-1385(%rbp)
	testq	%rax, %rax
	movq	%rax, %rdi
	movzbl	-1385(%rbp), %ecx
	je	.L834
	cmpl	$2, %r13d
	jne	.L835
	testb	%cl, %cl
	jne	.L965
.L835:
	movdqu	8(%r14), %xmm0
	movups	%xmm0, -1224(%rbp)
.L837:
	movq	-1320(%rbp), %r10
	xorl	%eax, %eax
	movl	%r12d, -1436(%rbp)
	movq	%rax, %r12
	movl	%r13d, -1440(%rbp)
	movq	%rbx, -1456(%rbp)
	movq	%rdi, %r13
	movq	%r15, -1432(%rbp)
	movq	%r14, -1448(%rbp)
	movq	%r10, %rbx
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L841:
	leaq	(%r12,%rbx), %r15
	movq	-1376(%rbp), %rdi
	movl	$16, %edx
	shrq	%r15
	leaq	(%r15,%r15,2), %rax
	leaq	0(%r13,%rax,8), %r14
	leaq	8(%r14), %rsi
	call	__GI_memcmp
	testl	%eax, %eax
	jns	.L966
	movq	%r15, %rbx
.L949:
	cmpq	%r12, %rbx
	ja	.L841
	movq	-1432(%rbp), %r15
	movl	-1436(%rbp), %r12d
	movl	-1440(%rbp), %r13d
	movq	-1448(%rbp), %r14
	movq	-1456(%rbp), %rbx
.L834:
	cmpl	$10, %r13d
	jne	.L828
	cmpb	$0, -1385(%rbp)
	je	.L828
	movl	8(%r14), %r9d
	testl	%r9d, %r9d
	jne	.L844
	movl	12(%r14), %r8d
	testl	%r8d, %r8d
	jne	.L844
	cmpl	$-65536, 16(%r14)
	jne	.L844
	movl	20(%r14), %eax
	movl	$2, %edi
	movb	$16, 28(%r14)
	movw	%di, (%r14)
	movl	$10, %r13d
	movl	%eax, 4(%r14)
	jmp	.L828
.L878:
	movq	%rdi, %rdx
	.p2align 4,,10
	.p2align 3
.L858:
	movq	-1352(%rbp), %rax
	movq	$0, 40(%rdx)
	movq	%rax, 32(%rdi)
	movl	-1392(%rbp), %eax
	testl	%eax, %eax
	jne	.L815
	movq	-1400(%rbp), %rdi
	call	free@PLT
.L815:
	movq	-1328(%rbp), %rdi
	call	__free_in6ai
	movq	-1336(%rbp), %rax
	testq	%rax, %rax
	je	.L947
	movq	-1416(%rbp), %rbx
	movq	%rax, (%rbx)
.L794:
	movl	-1380(%rbp), %eax
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L824:
	movq	-48(%r14), %rcx
	movq	%rcx, (%r14)
	movl	%eax, %ecx
	movq	-8(%rsi,%rcx), %rdi
	movq	%rdi, -8(%r14,%rcx)
	leaq	8(%r14), %rdi
	movq	%r14, %rcx
	andq	$-8, %rdi
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	%eax, %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	rep movsq
	jmp	.L825
	.p2align 4,,10
	.p2align 3
.L800:
	leaq	-1320(%rbp), %rcx
	leaq	-1328(%rbp), %rdx
	leaq	-1341(%rbp), %rsi
	leaq	-1342(%rbp), %rdi
	leaq	default_hints(%rip), %r13
	movq	$0, -1328(%rbp)
	movq	$0, -1320(%rbp)
	movb	$0, -1342(%rbp)
	movb	$0, -1341(%rbp)
	call	__check_pf
.L865:
	cmpb	$0, -1342(%rbp)
	movzbl	-1341(%rbp), %ebx
	je	.L967
	testb	%bl, %bl
	jne	.L802
	movdqu	0(%r13), %xmm0
	movl	$2, %eax
	movaps	%xmm0, -1280(%rbp)
	movdqu	16(%r13), %xmm0
	movaps	%xmm0, -1264(%rbp)
	movdqu	32(%r13), %xmm0
	movaps	%xmm0, -1248(%rbp)
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L966:
	je	.L840
	leaq	1(%r15), %r12
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L812:
	call	__GI_freeaddrinfo
	movq	-1328(%rbp), %rdi
	call	__free_in6ai
	negl	-1380(%rbp)
	jmp	.L794
	.p2align 4,,10
	.p2align 3
.L953:
	leaq	-1088(%rbp), %r9
	movl	$10, %edx
	movq	%r12, %rdi
	movq	%r12, -1312(%rbp)
	movq	%r9, %rsi
	movq	%r9, -1352(%rbp)
	call	__GI_strtoul
	movl	%eax, -1304(%rbp)
	movq	-1088(%rbp), %rax
	movq	-1352(%rbp), %r9
	cmpb	$0, (%rax)
	je	.L809
	testl	$1024, 0(%r13)
	jne	.L807
	movl	$-1, -1304(%rbp)
.L809:
	leaq	-1312(%rbp), %rsi
	jmp	.L808
	.p2align 4,,10
	.p2align 3
.L960:
	cmpl	$-1, %r12d
	je	.L822
	movl	%r12d, %edi
	call	__GI___close_nocancel
.L822:
	movq	-1400(%rbp), %rax
	movl	gaiconf_reload_flag_ever_set(%rip), %esi
	movq	%rax, -1296(%rbp)
	movl	-1384(%rbp), %eax
	testl	%esi, %esi
	movl	%eax, -1288(%rbp)
	jne	.L968
	movq	-1424(%rbp), %rsi
	movq	-1360(%rbp), %rdi
	leaq	-1296(%rbp), %r8
	leaq	rfc3484_sort(%rip), %rcx
	movl	$8, %edx
	call	__GI___qsort_r
.L857:
	movq	-1360(%rbp), %rbx
	movq	-1400(%rbp), %r8
	movl	-1384(%rbp), %esi
	movq	(%rbx), %rax
	leaq	(%rax,%rax,2), %rax
	salq	$4, %rax
	cmpl	$1, %esi
	movq	(%r8,%rax), %rdi
	movq	%rdi, -1336(%rbp)
	jle	.L878
	leal	-2(%rsi), %edx
	leaq	8(%rbx), %rax
	movq	%rdi, %rcx
	leaq	16(%rbx,%rdx,8), %rsi
	.p2align 4,,10
	.p2align 3
.L859:
	movq	(%rax), %rdx
	addq	$8, %rax
	leaq	(%rdx,%rdx,2), %rdx
	salq	$4, %rdx
	cmpq	%rax, %rsi
	movq	(%r8,%rdx), %rdx
	movq	%rdx, 40(%rcx)
	movq	%rdx, %rcx
	jne	.L859
	jmp	.L858
	.p2align 4,,10
	.p2align 3
.L964:
	movzbl	(%rsi), %ecx
	testb	$2, %al
	movb	%cl, (%r14)
	je	.L825
	movl	%eax, %eax
	movzwl	-2(%rsi,%rax), %ecx
	movw	%cx, -2(%r14,%rax)
	jmp	.L825
.L967:
	testb	%bl, %bl
	je	.L871
	movdqu	0(%r13), %xmm0
	movl	$10, %eax
	movaps	%xmm0, -1280(%rbp)
	movdqu	16(%r13), %xmm0
	movaps	%xmm0, -1264(%rbp)
	movdqu	32(%r13), %xmm0
	movaps	%xmm0, -1248(%rbp)
.L861:
	movl	%eax, -1276(%rbp)
	movl	$1, %ebx
	leaq	-1280(%rbp), %r13
	jmp	.L802
.L816:
	movl	-1460(%rbp), %r10d
	testl	%r10d, %r10d
	jne	.L817
	call	gaiconf_init
	orl	$2, once.15181(%rip)
	jmp	.L817
.L871:
	movl	$1, %ebx
	jmp	.L802
	.p2align 4,,10
	.p2align 3
.L956:
	leaq	-1320(%rbp), %rcx
	leaq	-1328(%rbp), %rdx
	leaq	-1341(%rbp), %rsi
	leaq	-1342(%rbp), %rdi
	call	__check_pf
	jmp	.L820
	.p2align 4,,10
	.p2align 3
.L954:
	movl	-1380(%rbp), %eax
	movl	%eax, -1384(%rbp)
	jmp	.L814
.L965:
	movl	4(%r14), %eax
	movl	$16777343, %edx
	movq	$0, -1224(%rbp)
	movl	$-65536, -1216(%rbp)
	cmpb	$127, %al
	cmove	%edx, %eax
	movl	%eax, -1212(%rbp)
	jmp	.L837
.L869:
	movl	$-1, -1380(%rbp)
	jmp	.L794
.L963:
	movl	(%rsi), %ecx
	movl	%eax, %eax
	movl	%ecx, (%r14)
	movl	-4(%rsi,%rax), %ecx
	movl	%ecx, -4(%r14,%rax)
	jmp	.L825
.L968:
#APP
# 2445 "../sysdeps/posix/getaddrinfo.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L851
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, lock.15212(%rip)
# 0 "" 2
#NO_APP
.L852:
	movl	-1460(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L853
	movl	gaiconf_reload_flag(%rip), %edx
	testl	%edx, %edx
	jne	.L969
.L853:
	movq	-1424(%rbp), %rsi
	movq	-1360(%rbp), %rdi
	leaq	-1296(%rbp), %r8
	leaq	rfc3484_sort(%rip), %rcx
	movl	$8, %edx
	call	__GI___qsort_r
#APP
# 2449 "../sysdeps/posix/getaddrinfo.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L856
	subl	$1, lock.15212(%rip)
	jmp	.L857
.L870:
	movl	$-6, -1380(%rbp)
	jmp	.L794
.L840:
	movq	%r14, %r11
	movq	-1432(%rbp), %r15
	movl	-1436(%rbp), %r12d
	testq	%r11, %r11
	movl	-1440(%rbp), %r13d
	movq	-1448(%rbp), %r14
	movq	-1456(%rbp), %rbx
	je	.L834
	movzbl	(%r11), %eax
	movb	%al, 30(%r14)
	movzbl	1(%r11), %eax
	movb	%al, 31(%r14)
	movl	4(%r11), %eax
	movl	%eax, 32(%r14)
	jmp	.L834
.L952:
	leaq	-1320(%rbp), %rcx
	leaq	-1328(%rbp), %rdx
	leaq	-1341(%rbp), %rsi
	leaq	-1342(%rbp), %rdi
	call	__check_pf
	movl	4(%r13), %eax
	testl	%eax, %eax
	je	.L865
	cmpl	$2, %eax
	je	.L970
	cmpl	$10, %eax
	jne	.L871
	movzbl	-1341(%rbp), %ebx
	testb	%bl, %bl
	jne	.L802
.L807:
	movq	-1328(%rbp), %rdi
	call	__free_in6ai
	jmp	.L947
	.p2align 4,,10
	.p2align 3
.L970:
	movzbl	-1342(%rbp), %ebx
	testb	%bl, %bl
	jne	.L802
	jmp	.L807
.L876:
	movq	$0, -1352(%rbp)
	jmp	.L822
.L955:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, -1400(%rbp)
	je	.L971
	movl	$0, -1392(%rbp)
	jmp	.L819
.L969:
	leaq	-1232(%rbp), %rsi
	leaq	.LC1(%rip), %rdi
	call	__GI___stat64
	testl	%eax, %eax
	jne	.L854
	movq	gaiconf_mtime(%rip), %rax
	cmpq	%rax, -1144(%rbp)
	je	.L972
.L854:
	call	gaiconf_init
	jmp	.L853
.L959:
	leaq	__PRETTY_FUNCTION__.15207(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC21(%rip), %rdi
	movl	$2428, %edx
	call	__GI___assert_fail
.L856:
	xorl	%eax, %eax
#APP
# 2449 "../sysdeps/posix/getaddrinfo.c" 1
	xchgl %eax, lock.15212(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L857
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	lock.15212(%rip), %rdi
	movl	$202, %eax
#APP
# 2449 "../sysdeps/posix/getaddrinfo.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L857
	.p2align 4,,10
	.p2align 3
.L851:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, lock.15212(%rip)
	je	.L852
	leaq	lock.15212(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L852
.L972:
	movq	8+gaiconf_mtime(%rip), %rax
	cmpq	%rax, -1136(%rbp)
	jne	.L854
	jmp	.L853
.L971:
	movq	-1328(%rbp), %rdi
	call	__free_in6ai
	movl	$-10, -1380(%rbp)
	jmp	.L794
.L844:
	leaq	__PRETTY_FUNCTION__.15207(%rip), %rcx
	leaq	.LC7(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$2401, %edx
	call	__GI___assert_fail
.LFE122:
	.size	__GI_getaddrinfo, .-__GI_getaddrinfo
	.globl	getaddrinfo
	.set	getaddrinfo,__GI_getaddrinfo
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.14983, @object
	.size	__PRETTY_FUNCTION__.14983, 13
__PRETTY_FUNCTION__.14983:
	.string	"rfc3484_sort"
	.local	lock.15212
	.comm	lock.15212,4,4
	.align 8
	.type	__PRETTY_FUNCTION__.15207, @object
	.size	__PRETTY_FUNCTION__.15207, 12
__PRETTY_FUNCTION__.15207:
	.string	"getaddrinfo"
	.local	once.15181
	.comm	once.15181,4,4
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_fini__, @object
	.size	__elf_set___libc_subfreeres_element_fini__, 8
__elf_set___libc_subfreeres_element_fini__:
	.quad	fini
	.local	gaiconf_mtime
	.comm	gaiconf_mtime,16,16
	.local	gaiconf_reload_flag_ever_set
	.comm	gaiconf_reload_flag_ever_set,4,4
	.local	gaiconf_reload_flag
	.comm	gaiconf_reload_flag,4,4
	.section	.rodata
	.align 32
	.type	default_precedence, @object
	.size	default_precedence, 120
default_precedence:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.long	128
	.long	50
	.byte	32
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	16
	.long	30
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	96
	.long	20
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-1
	.byte	-1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	96
	.long	10
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	0
	.long	40
	.local	precedence
	.comm	precedence,8,8
	.align 32
	.type	default_labels, @object
	.size	default_labels, 192
default_labels:
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	1
	.long	128
	.long	0
	.byte	32
	.byte	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	16
	.long	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	96
	.long	3
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	-1
	.byte	-1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	96
	.long	4
	.byte	-2
	.byte	-64
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	10
	.long	5
	.byte	-4
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	7
	.long	6
	.byte	32
	.byte	1
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	32
	.long	7
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	0
	.long	1
	.local	labels
	.comm	labels,8,8
	.local	scopes
	.comm	scopes,8,8
	.align 32
	.type	default_scopes, @object
	.size	default_scopes, 36
default_scopes:
	.byte	-87
	.byte	-2
	.byte	0
	.byte	0
	.long	65535
	.long	2
	.byte	127
	.byte	0
	.byte	0
	.byte	0
	.long	255
	.long	2
	.byte	0
	.byte	0
	.byte	0
	.byte	0
	.long	0
	.long	14
	.align 32
	.type	default_hints, @object
	.size	default_hints, 48
default_hints:
	.long	40
	.long	0
	.long	0
	.long	0
	.long	0
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.align 32
	.type	gaih_inet_typeproto, @object
	.size	gaih_inet_typeproto, 180
gaih_inet_typeproto:
	.long	0
	.long	0
	.byte	0
	.byte	0
	.string	""
	.zero	7
	.zero	2
	.long	1
	.long	6
	.byte	0
	.byte	1
	.string	"tcp"
	.zero	4
	.zero	2
	.long	2
	.long	17
	.byte	0
	.byte	1
	.string	"udp"
	.zero	4
	.zero	2
	.long	6
	.long	33
	.byte	0
	.byte	0
	.string	"dccp"
	.zero	3
	.zero	2
	.long	2
	.long	136
	.byte	0
	.byte	0
	.string	"udplite"
	.zero	2
	.long	1
	.long	132
	.byte	0
	.byte	0
	.string	"sctp"
	.zero	3
	.zero	2
	.long	5
	.long	132
	.byte	0
	.byte	0
	.string	"sctp"
	.zero	3
	.zero	2
	.long	3
	.long	0
	.byte	3
	.byte	1
	.string	"raw"
	.zero	4
	.zero	2
	.long	0
	.long	0
	.byte	0
	.byte	0
	.string	""
	.zero	7
	.zero	2
	.local	nullserv
	.comm	nullserv,24,16
	.hidden	__lll_lock_wait_private
	.hidden	__check_pf
	.hidden	__free_in6ai
	.hidden	__getsockname
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
	.hidden	__gethostbyname2_r
	.hidden	__check_native
	.hidden	__getservbyname_r
	.hidden	__getline
