	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __fnmatch,fnmatch@@GLIBC_2.2.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"POSIXLY_CORRECT"
#NO_APP
	.text
	.p2align 4,,15
	.type	end_wpattern, @function
end_wpattern:
	pushq	%r12
	movl	$3221227009, %r12d
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
.L2:
	movl	4(%rbx), %edx
	testl	%edx, %edx
	je	.L12
.L3:
	cmpl	$91, %edx
	je	.L28
	leal	-33(%rdx), %ecx
	cmpl	$31, %ecx
	ja	.L16
	btq	%rcx, %r12
	jnc	.L16
	cmpl	$40, 8(%rbx)
	je	.L29
.L16:
	cmpl	$41, %edx
	je	.L17
	addq	$4, %rbx
	movl	4(%rbx), %edx
	testl	%edx, %edx
	jne	.L3
.L12:
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	movl	posixly_correct(%rip), %esi
	testl	%esi, %esi
	je	.L30
.L6:
	movl	8(%rbx), %edx
	cmpl	$33, %edx
	je	.L8
	movl	posixly_correct(%rip), %ecx
	testl	%ecx, %ecx
	jns	.L19
	cmpl	$94, %edx
	je	.L8
.L19:
	addq	$8, %rbx
.L10:
	cmpl	$93, %edx
	jne	.L13
	movl	4(%rbx), %edx
	addq	$4, %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L31:
	movl	(%rcx), %edx
	movq	%rcx, %rbx
.L13:
	cmpl	$93, %edx
	leaq	4(%rbx), %rcx
	je	.L2
	testl	%edx, %edx
	jne	.L31
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L8:
	movl	12(%rbx), %edx
	addq	$12, %rbx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L30:
	leaq	.LC0(%rip), %rdi
	call	__GI_getenv
	cmpq	$1, %rax
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, posixly_correct(%rip)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L29:
	leaq	8(%rbx), %rdi
	call	end_wpattern
	movq	%rax, %rbx
	movl	(%rax), %eax
	testl	%eax, %eax
	jne	.L2
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	leaq	8(%rbx), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	end_wpattern, .-end_wpattern
	.p2align 4,,15
	.type	end_pattern, @function
end_pattern:
	pushq	%r12
	movl	$3221227009, %r12d
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
.L33:
	movzbl	1(%rbx), %edx
	testb	%dl, %dl
	je	.L43
.L34:
	cmpb	$91, %dl
	je	.L58
	leal	-33(%rdx), %ecx
	cmpb	$31, %cl
	ja	.L47
	btq	%rcx, %r12
	jnc	.L47
	cmpb	$40, 2(%rbx)
	je	.L59
.L47:
	cmpb	$41, %dl
	je	.L48
	addq	$1, %rbx
	movzbl	1(%rbx), %edx
	testb	%dl, %dl
	jne	.L34
.L43:
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L58:
	movl	posixly_correct(%rip), %edx
	testl	%edx, %edx
	je	.L60
.L37:
	movzbl	2(%rbx), %edx
	cmpb	$33, %dl
	je	.L39
	movl	posixly_correct(%rip), %eax
	testl	%eax, %eax
	jns	.L50
	cmpb	$94, %dl
	je	.L39
.L50:
	addq	$2, %rbx
.L41:
	cmpb	$93, %dl
	jne	.L44
	movzbl	1(%rbx), %edx
	addq	$1, %rbx
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L61:
	movzbl	(%rcx), %edx
	movq	%rcx, %rbx
.L44:
	cmpb	$93, %dl
	leaq	1(%rbx), %rcx
	je	.L33
	testb	%dl, %dl
	jne	.L61
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L39:
	movzbl	3(%rbx), %edx
	addq	$3, %rbx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	.LC0(%rip), %rdi
	call	__GI_getenv
	cmpq	$1, %rax
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, posixly_correct(%rip)
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L59:
	leaq	2(%rbx), %rdi
	call	end_pattern
	cmpb	$0, (%rax)
	movq	%rax, %rbx
	jne	.L33
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L48:
	leaq	2(%rbx), %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	end_pattern, .-end_pattern
	.section	.rodata.str1.1
.LC1:
	.string	"fnmatch_loop.c"
.LC2:
	.string	"list != NULL"
.LC3:
	.string	"p[-1] == L_(')')"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC4:
	.string	"! \"Invalid extended matching operator\""
	.text
	.p2align 4,,15
	.type	ext_wmatch, @function
ext_wmatch:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbx
	movl	$3221227009, %r12d
	subq	$120, %rsp
	movl	%edi, -68(%rbp)
	movq	%rsi, %rdi
	movq	%rcx, -96(%rbp)
	movl	%r9d, -84(%rbp)
	movq	%rdx, -112(%rbp)
	movl	%r8d, -120(%rbp)
	movb	%r8b, -85(%rbp)
	movq	$0, -56(%rbp)
	call	__wcslen@PLT
	movq	%rax, %r14
	movl	-68(%rbp), %eax
	leaq	4(%r13), %rsi
	leaq	-56(%rbp), %rcx
	movq	16(%rbp), %r15
	movb	$0, -69(%rbp)
	movq	%rsi, %rbx
	xorl	%r9d, %r9d
	subl	$63, %eax
	movq	%rcx, -80(%rbp)
	movl	%eax, -104(%rbp)
.L63:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.L142
.L202:
	cmpl	$91, %eax
	je	.L207
	leal	-33(%rax), %edx
	cmpl	$31, %edx
	jbe	.L208
.L76:
	testq	%r9, %r9
	sete	%dl
	cmpl	$124, %eax
	sete	%al
	andb	%al, %dl
	movl	%edx, %r8d
	jne	.L89
	addq	$4, %rbx
	movl	(%rbx), %eax
	testl	%eax, %eax
	jne	.L202
	.p2align 4,,10
	.p2align 3
.L142:
	movl	$-1, %eax
.L72:
.L125:
	cmpb	$0, -69(%rbp)
	je	.L62
	movq	-56(%rbp), %rdi
	movl	%eax, %r12d
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L129:
	movl	8(%rdi), %esi
	movq	(%rdi), %rbx
	testl	%esi, %esi
	jne	.L209
	movq	%rbx, %rdi
.L128:
	testq	%rdi, %rdi
	jne	.L129
	movl	%r12d, %eax
.L62:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L208:
	btq	%rdx, %r12
	jnc	.L77
	cmpl	$40, 4(%rbx)
	je	.L210
.L77:
	cmpl	$41, %eax
	jne	.L76
	testq	%r9, %r9
	je	.L78
	subq	$1, %r9
	addq	$4, %rbx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L210:
	addq	$1, %r9
	addq	$4, %rbx
	jmp	.L63
	.p2align 4,,10
	.p2align 3
.L207:
	movl	posixly_correct(%rip), %r10d
	testl	%r10d, %r10d
	je	.L211
.L66:
	movl	4(%rbx), %eax
	cmpl	$33, %eax
	je	.L68
	movl	posixly_correct(%rip), %r8d
	testl	%r8d, %r8d
	jns	.L148
	cmpl	$94, %eax
	je	.L68
.L148:
	addq	$4, %rbx
.L70:
	cmpl	$93, %eax
	jne	.L73
	movl	4(%rbx), %eax
	addq	$4, %rbx
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L212:
	movl	(%rbx), %eax
.L73:
	addq	$4, %rbx
	cmpl	$93, %eax
	je	.L63
	testl	%eax, %eax
	jne	.L212
	jmp	.L142
	.p2align 4,,10
	.p2align 3
.L68:
	movl	8(%rbx), %eax
	addq	$8, %rbx
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L78:
	movl	-68(%rbp), %eax
	subl	$63, %eax
	cmpl	$1, %eax
	jbe	.L79
	movq	%rbx, %r14
	subq	%rsi, %r14
	sarq	$2, %r14
	addq	$1, %r14
.L79:
	movq	%r14, %rax
	movl	$4, %edi
	xorl	%r9d, %r9d
	mulq	%rdi
	leaq	16(%r15), %r8
	movq	%rax, %r14
	jo	.L213
.L80:
	testq	%r14, %r14
	movq	%rsi, -104(%rbp)
	js	.L140
	testq	%r9, %r9
	jne	.L140
	addq	%r14, %r8
	movq	%r8, %r12
	jo	.L140
	movq	%r8, %rdi
	addq	$16, %r14
	call	__GI___libc_alloca_cutoff
	cmpq	$4096, %r12
	movq	-104(%rbp), %rsi
	jbe	.L85
	testl	%eax, %eax
	je	.L214
.L85:
#APP
# 1067 "fnmatch_loop.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %r14
	andq	$-16, %r14
	subq	%r14, %rsp
	leaq	15(%rsp), %r14
	andq	$-16, %r14
#APP
# 1067 "fnmatch_loop.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, %r15
	xorl	%eax, %eax
.L86:
	movq	%rbx, %rdx
	leaq	12(%r14), %rdi
	movq	$0, (%r14)
	subq	%rsi, %rdx
	movl	%eax, 8(%r14)
	sarq	$2, %rdx
	call	__wmempcpy
	movl	$0, (%rax)
	movq	-80(%rbp), %rax
	movq	%r14, (%rax)
	movq	-56(%rbp), %r12
	leaq	4(%rbx), %rax
	movq	%rax, -80(%rbp)
	testq	%r12, %r12
	je	.L215
	cmpl	$41, (%rbx)
	jne	.L216
	movl	-68(%rbp), %ebx
	subl	$33, %ebx
	cmpl	$31, %ebx
	ja	.L101
	leaq	.L103(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L103:
	.long	.L102-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L104-.L103
	.long	.L105-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L101-.L103
	.long	.L106-.L103
	.long	.L107-.L103
	.text
	.p2align 4,,10
	.p2align 3
.L209:
	movq	%rbx, -56(%rbp)
	call	free@PLT
	movq	%rbx, %rdi
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L211:
	leaq	.LC0(%rip), %rdi
	movq	%rsi, -136(%rbp)
	movq	%r9, -128(%rbp)
	call	__GI_getenv
	cmpq	$1, %rax
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %r9
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, posixly_correct(%rip)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L89:
	cmpl	$1, -104(%rbp)
	movq	%r14, %rcx
	jbe	.L90
	movq	%rbx, %rcx
	subq	%rsi, %rcx
	sarq	$2, %rcx
	addq	$1, %rcx
.L90:
	movq	%rcx, %rax
	movl	$4, %edx
	xorl	%r9d, %r9d
	mulq	%rdx
	leaq	16(%r15), %rdi
	movq	%rax, %rcx
	jo	.L217
.L91:
	testq	%rcx, %rcx
	movq	%rsi, -136(%rbp)
	movb	%r8b, -128(%rbp)
	js	.L140
	testq	%r9, %r9
	jne	.L140
	movq	%rdi, %rdx
	movq	%rcx, -152(%rbp)
	addq	%rcx, %rdx
	jo	.L140
	movq	%rdx, %rdi
	movq	%rdx, -144(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-152(%rbp), %rcx
	movq	-144(%rbp), %rdx
	movzbl	-128(%rbp), %r8d
	movq	-136(%rbp), %rsi
	addq	$16, %rcx
	cmpq	$4096, %rdx
	jbe	.L96
	testl	%eax, %eax
	je	.L218
.L96:
#APP
# 1074 "fnmatch_loop.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %rcx
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
#APP
# 1074 "fnmatch_loop.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, %r15
	xorl	%eax, %eax
.L97:
	movq	%rbx, %rdx
	leaq	12(%rcx), %rdi
	movq	$0, (%rcx)
	subq	%rsi, %rdx
	movl	%eax, 8(%rcx)
	movq	%rcx, -128(%rbp)
	sarq	$2, %rdx
	call	__wmempcpy
	movq	-128(%rbp), %rcx
	movl	$0, (%rax)
	leaq	4(%rbx), %rsi
	movq	-80(%rbp), %rax
	movq	%rcx, -80(%rbp)
	movq	%rcx, (%rax)
	movl	4(%rbx), %eax
	testl	%eax, %eax
	je	.L142
	xorl	%r9d, %r9d
	movq	%rsi, %rbx
	jmp	.L202
.L214:
	movq	%r14, %rdi
	movq	%rsi, -104(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L140
	movl	$1, %eax
	movb	$1, -69(%rbp)
	movq	-104(%rbp), %rsi
	jmp	.L86
.L104:
	movzbl	-120(%rbp), %ecx
	subq	$8, %rsp
	movq	-112(%rbp), %rsi
	movq	-80(%rbp), %rdi
	movl	-84(%rbp), %r8d
	xorl	%r9d, %r9d
	movq	-96(%rbp), %rdx
	pushq	%r15
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	je	.L125
.L105:
	movl	-84(%rbp), %eax
	movq	%r12, -128(%rbp)
	movq	-96(%rbp), %r12
	movl	%eax, %ebx
	movl	%eax, %r14d
	andl	$-5, %ebx
	andl	$1, %r14d
	cmovne	%eax, %ebx
	leaq	-4(%r13), %rax
	movq	-112(%rbp), %r13
	movl	%r14d, -104(%rbp)
	movq	%rax, -120(%rbp)
.L117:
	cmpq	%r12, %r13
	ja	.L110
	movzbl	-85(%rbp), %eax
	movq	%r13, %r14
	movl	%eax, -68(%rbp)
	movq	-128(%rbp), %rax
	addq	$12, %rax
	movq	%rax, -96(%rbp)
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L219:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	pushq	%r15
.L203:
	movl	-68(%rbp), %ecx
	movq	-96(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%r11
	popq	%rdx
	jne	.L112
	cmpq	%r13, %r14
	je	.L113
	xorl	%ecx, %ecx
	cmpl	$47, -4(%r14)
	jne	.L114
	movl	-84(%rbp), %eax
	xorl	%ecx, %ecx
	andl	$5, %eax
	cmpl	$5, %eax
	sete	%cl
.L114:
	subq	$8, %rsp
	movq	-80(%rbp), %rdi
	xorl	%r9d, %r9d
	pushq	%r15
	movl	%ebx, %r8d
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%r9
	popq	%r10
	je	.L125
	xorl	%ecx, %ecx
	cmpl	$47, -4(%r14)
	jne	.L115
	movl	-84(%rbp), %eax
	xorl	%ecx, %ecx
	andl	$5, %eax
	cmpl	$5, %eax
	sete	%cl
.L115:
	subq	$8, %rsp
	movq	-120(%rbp), %rdi
	xorl	%r9d, %r9d
	pushq	%r15
	movl	%ebx, %r8d
	movq	%r12, %rdx
	movq	%r14, %rsi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%rdi
	popq	%r8
	je	.L125
.L112:
	addq	$4, %r14
	cmpq	%r14, %r12
	jb	.L110
.L116:
	movl	-104(%rbp), %ecx
	testl	%ecx, %ecx
	je	.L219
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	-84(%rbp), %r8d
	pushq	%r15
	jmp	.L203
.L102:
	movq	-96(%rbp), %rcx
	cmpq	%rcx, -112(%rbp)
	ja	.L206
	movl	-84(%rbp), %eax
	movq	-112(%rbp), %rbx
	movq	%r12, -104(%rbp)
	movl	%eax, %r14d
	movq	%rbx, %r13
	andl	$-5, %r14d
	testb	$1, %al
	cmovne	%eax, %r14d
	movzbl	-120(%rbp), %eax
	movl	%eax, -68(%rbp)
	.p2align 4,,10
	.p2align 3
.L120:
	movq	-104(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L121:
	subq	$8, %rsp
	movl	-68(%rbp), %ecx
	leaq	12(%r12), %rdi
	pushq	%r15
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%r9
	popq	%r10
	je	.L124
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L121
	cmpq	%r13, %rbx
	je	.L220
	xorl	%ecx, %ecx
	cmpl	$47, -4(%r13)
	jne	.L123
	movl	-84(%rbp), %eax
	xorl	%ecx, %ecx
	andl	$5, %eax
	cmpl	$5, %eax
	sete	%cl
.L123:
	subq	$8, %rsp
	movq	-80(%rbp), %rdi
	movq	-96(%rbp), %rdx
	pushq	%r15
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%r13, %rsi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%rdi
	popq	%r8
	je	.L125
.L124:
	addq	$4, %r13
	cmpq	%r13, -96(%rbp)
	jnb	.L120
.L206:
	movl	$1, %eax
	jmp	.L125
.L107:
	movzbl	-120(%rbp), %r13d
.L108:
	movl	-84(%rbp), %eax
	movq	-112(%rbp), %r14
	movl	%eax, %ebx
	andl	$-5, %ebx
	testb	$1, %al
	cmovne	%eax, %ebx
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L221:
	movq	(%r12), %r12
	testq	%r12, %r12
	movq	%r12, -56(%rbp)
	je	.L206
.L118:
	movq	-80(%rbp), %rsi
	leaq	12(%r12), %rdi
	call	__wcscat@PLT
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	%r15
	movl	%ebx, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%r11
	popq	%rdx
	jne	.L221
	jmp	.L125
.L106:
	movzbl	-120(%rbp), %r13d
	subq	$8, %rsp
	movq	-112(%rbp), %rsi
	movl	-84(%rbp), %r8d
	movq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	movq	-80(%rbp), %rdi
	pushq	%r15
	movl	%r13d, %ecx
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%rcx
	popq	%rsi
	je	.L125
	jmp	.L108
.L110:
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	movq	%rax, -56(%rbp)
	jne	.L117
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L218:
	movq	%rcx, %rdi
	movq	%rsi, -136(%rbp)
	movb	%r8b, -128(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L140
	movzbl	-128(%rbp), %r8d
	movl	$1, %eax
	movq	-136(%rbp), %rsi
	movb	%r8b, -69(%rbp)
	jmp	.L97
.L140:
	movl	$-2, %eax
	jmp	.L125
.L113:
	subq	$8, %rsp
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdi
	pushq	%r15
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movl	%ebx, %r8d
	movq	%r14, %rsi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L112
	jmp	.L125
.L220:
	movl	-68(%rbp), %ecx
	jmp	.L123
.L101:
	leaq	__PRETTY_FUNCTION__.9516(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$1176, %edx
	call	__GI___assert_fail
.L215:
	leaq	__PRETTY_FUNCTION__.9516(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$1078, %edx
	call	__GI___assert_fail
.L216:
	leaq	__PRETTY_FUNCTION__.9516(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$1079, %edx
	call	__GI___assert_fail
.L213:
	movl	$1, %r9d
	jmp	.L80
.L217:
	movl	$1, %r9d
	jmp	.L91
	.size	ext_wmatch, .-ext_wmatch
	.p2align 4,,15
	.type	internal_fnwmatch, @function
internal_fnwmatch:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movl	%r8d, %esi
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r15
	movq	%r14, %r13
	subq	$10440, %rsp
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movb	%cl, 16(%rsp)
	movq	%rdx, (%rsp)
	movl	%r8d, 8(%rsp)
	movq	%r9, 40(%rsp)
	movq	%fs:(%rax), %rax
	movq	24(%rax), %rax
	movq	200(%rax), %rax
	movq	%rax, 32(%rsp)
	movl	%r8d, %eax
	andl	$1, %eax
	movl	%eax, %ecx
	movl	%eax, 48(%rsp)
	negl	%ecx
	andl	$47, %ecx
	movl	%ecx, 52(%rsp)
	movl	%r8d, %ecx
	andl	$-5, %ecx
	testl	%eax, %eax
	movl	%ecx, 64(%rsp)
	cmovne	%r8d, %ecx
	shrl	%esi
	movl	%esi, %eax
	movl	%ecx, 68(%rsp)
	xorl	$1, %eax
	andl	$1, %eax
	movb	%al, 150(%rsp)
.L223:
	movl	(%r15), %r11d
	leaq	4(%r15), %rbx
	testl	%r11d, %r11d
	je	.L579
	movl	8(%rsp), %eax
	andl	$16, %eax
	movl	%eax, 12(%rsp)
	je	.L224
	movl	%r11d, %edi
	call	__GI___towlower
	movl	%eax, %r11d
.L224:
	leal	-33(%r11), %eax
	cmpl	$59, %eax
	ja	.L225
	leaq	.L227(%rip), %rsi
	movslq	(%rsi,%rax,4), %rax
	addq	%rsi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L227:
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L228-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L229-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L230-.L227
	.long	.L226-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L225-.L227
	.long	.L231-.L227
	.long	.L232-.L227
	.text
	.p2align 4,,10
	.p2align 3
.L232:
	testb	$2, 8(%rsp)
	jne	.L240
	movl	4(%r15), %r11d
	leaq	8(%r15), %rbx
	testl	%r11d, %r11d
	je	.L234
	movl	12(%rsp), %edi
	testl	%edi, %edi
	je	.L241
	movl	%r11d, %edi
	call	__GI___towlower
	cmpq	(%rsp), %r13
	movl	%eax, %r11d
	je	.L234
.L242:
	movl	0(%r13), %edi
	movl	%r11d, 12(%rsp)
	call	__GI___towlower
	movl	12(%rsp), %r11d
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L231:
	movl	posixly_correct(%rip), %r12d
	testl	%r12d, %r12d
	je	.L580
.L279:
	cmpq	(%rsp), %r13
	je	.L234
	movl	0(%r13), %eax
	cmpl	$46, %eax
	je	.L581
	cmpl	$47, %eax
	jne	.L282
	movl	48(%rsp), %ebp
	testl	%ebp, %ebp
	jne	.L234
.L282:
	movl	4(%r15), %r12d
	cmpl	$33, %r12d
	je	.L283
	movl	posixly_correct(%rip), %r10d
	testl	%r10d, %r10d
	jns	.L409
	cmpl	$94, %r12d
	je	.L283
.L409:
	movq	%rbx, %rbp
	movl	$0, 88(%rsp)
.L379:
	movl	12(%rsp), %r9d
	movl	%eax, %edx
	testl	%r9d, %r9d
	je	.L285
	movl	%eax, %edi
	movl	%r11d, 16(%rsp)
	call	__GI___towlower
	movl	16(%rsp), %r11d
	movl	%eax, %edx
.L285:
	movq	(%rsp), %rax
	movl	%r12d, %edi
	movq	%r13, 56(%rsp)
	leaq	4(%rbp), %r15
	movl	12(%rsp), %r12d
	movq	%rbx, 80(%rsp)
	movl	%r11d, 92(%rsp)
	subq	%r13, %rax
	movl	%edx, %r13d
	sarq	$2, %rax
	subq	$1, %rax
	movq	%rax, 96(%rsp)
	leaq	160(%rsp), %rax
	movq	%rax, 72(%rsp)
	jmp	.L362
	.p2align 4,,10
	.p2align 3
.L343:
	cmpl	%edi, %r13d
	je	.L571
.L344:
	movl	%edi, %r8d
	addq	$4, %r15
	movl	%r14d, %edi
	xorl	%ebx, %ebx
.L336:
	cmpl	$45, %edi
	je	.L582
.L301:
	cmpl	$93, %edi
	je	.L583
.L362:
	cmpl	$92, %edi
	sete	%bl
	andb	150(%rsp), %bl
	jne	.L584
	cmpl	$91, %edi
	je	.L585
	testl	%edi, %edi
	je	.L586
.L327:
	movl	(%r15), %r14d
.L328:
	testl	%r12d, %r12d
	je	.L288
	call	__GI___towlower
	movl	%eax, %edi
.L288:
	cmpl	$45, %r14d
	jne	.L343
	movl	4(%r15), %eax
	cmpl	$93, %eax
	je	.L343
	testl	%eax, %eax
	jne	.L344
	cmpl	%edi, %r13d
	jne	.L344
	.p2align 4,,10
	.p2align 3
.L571:
	movq	56(%rsp), %r13
.L302:
	movl	(%r15), %eax
.L375:
	cmpl	$93, %eax
	leaq	4(%r15), %rbx
	je	.L587
.L376:
	testl	%eax, %eax
	je	.L234
	cmpl	$92, %eax
	jne	.L363
	cmpb	$0, 150(%rsp)
	jne	.L588
.L363:
	cmpl	$91, %eax
	movl	4(%r15), %eax
	je	.L589
.L365:
	movq	%rbx, %r15
	cmpl	$93, %eax
	leaq	4(%r15), %rbx
	jne	.L376
.L587:
	movl	88(%rsp), %edi
	testl	%edi, %edi
	je	.L577
	.p2align 4,,10
	.p2align 3
.L234:
	movl	$1, 48(%rsp)
.L222:
	movl	48(%rsp), %eax
	addq	$10440, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L230:
	testb	$32, 8(%rsp)
	jne	.L233
.L236:
	cmpq	(%rsp), %r13
	je	.L234
	movl	0(%r13), %eax
	cmpl	$47, %eax
	je	.L590
	cmpl	$46, %eax
	sete	%al
	andb	%al, 16(%rsp)
	je	.L239
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L229:
	movl	8(%rsp), %eax
	andl	$5, %eax
	cmpl	$5, %eax
	jne	.L225
	cmpq	(%rsp), %r13
	je	.L234
	cmpl	$47, 0(%r13)
	jne	.L234
	movb	$1, 16(%rsp)
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L226:
	testb	$32, 8(%rsp)
	jne	.L591
.L225:
	cmpq	(%rsp), %r13
	je	.L234
.L326:
	movl	12(%rsp), %edx
	movl	0(%r13), %eax
	testl	%edx, %edx
	je	.L377
	movl	%eax, %edi
	movl	%r11d, 12(%rsp)
	call	__GI___towlower
	movl	12(%rsp), %r11d
.L377:
	cmpl	%r11d, %eax
	jne	.L234
.L577:
	movb	$0, 16(%rsp)
.L239:
	addq	$4, %r13
	movq	%rbx, %r15
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L228:
	movl	8(%rsp), %eax
	andl	$32, %eax
	movl	%eax, 24(%rsp)
	jne	.L592
.L245:
	cmpq	$0, 40(%rsp)
	jne	.L593
.L246:
	cmpq	(%rsp), %r13
	je	.L247
	cmpl	$46, 0(%r13)
	jne	.L247
	cmpb	$0, 16(%rsp)
	jne	.L234
.L247:
	movl	4(%r15), %ebx
	leaq	8(%r15), %rbp
	cmpl	$42, %ebx
	je	.L408
	cmpl	$63, %ebx
	jne	.L249
.L408:
	movl	24(%rsp), %r15d
	testl	%r15d, %r15d
	setne	%r12b
	.p2align 4,,10
	.p2align 3
.L512:
	cmpl	$40, 0(%rbp)
	jne	.L255
	testb	%r12b, %r12b
	jne	.L251
.L255:
	cmpl	$63, %ebx
	je	.L253
.L574:
	movq	%rbp, %rax
.L254:
	movl	(%rax), %ebx
	leaq	4(%rax), %rbp
	cmpl	$63, %ebx
	je	.L512
	cmpl	$42, %ebx
	je	.L512
.L249:
	testl	%ebx, %ebx
	je	.L594
	movq	(%rsp), %r15
	movl	52(%rsp), %esi
	movq	%r13, %rdi
	movq	$0, 2224(%rsp)
	movq	%r15, %rdx
	subq	%r13, %rdx
	sarq	$2, %rdx
	call	__GI___wmemchr
	testq	%rax, %rax
	movq	%rax, %r14
	cmove	%r15, %r14
	cmpl	$91, %ebx
	je	.L260
	movl	24(%rsp), %r12d
	testl	%r12d, %r12d
	jne	.L595
.L261:
	cmpl	$47, %ebx
	je	.L596
	movl	48(%rsp), %esi
	movl	64(%rsp), %r15d
	movl	8(%rsp), %eax
	testl	%esi, %esi
	cmovne	%eax, %r15d
	cmpl	$92, %ebx
	jne	.L268
	testb	$2, %al
	jne	.L268
	movl	0(%rbp), %ebx
.L268:
	movl	12(%rsp), %ecx
	testl	%ecx, %ecx
	jne	.L597
.L273:
	subq	$4, %rbp
	cmpq	%r13, %r14
	jbe	.L234
	leaq	2224(%rsp), %r12
	movl	%r15d, 24(%rsp)
	movzbl	16(%rsp), %r15d
	movq	%rbp, 56(%rsp)
	movl	%ebx, %ebp
	movl	12(%rsp), %ebx
	movq	%r12, %rax
	movq	%r13, %r12
	movq	%rax, %r13
	.p2align 4,,10
	.p2align 3
.L277:
	testl	%ebx, %ebx
	movl	(%r12), %eax
	je	.L274
	movl	%eax, %edi
	call	__GI___towlower
.L274:
	cmpl	%eax, %ebp
	je	.L598
.L275:
	addq	$4, %r12
	xorl	%r15d, %r15d
	cmpq	%r12, %r14
	ja	.L277
	movq	2224(%rsp), %r15
	testq	%r15, %r15
	je	.L234
.L265:
	movzbl	2240(%rsp), %eax
	movq	2232(%rsp), %r13
	movb	%al, 16(%rsp)
	jmp	.L223
	.p2align 4,,10
	.p2align 3
.L253:
	cmpq	%r13, (%rsp)
	je	.L234
	cmpl	$47, 0(%r13)
	je	.L599
.L256:
	addq	$4, %r13
	jmp	.L574
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%rbp, %rdi
	call	end_wpattern
	cmpq	%rbp, %rax
	jne	.L254
	jmp	.L255
	.p2align 4,,10
	.p2align 3
.L599:
	movl	48(%rsp), %r14d
	testl	%r14d, %r14d
	je	.L256
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L240:
	cmpq	(%rsp), %r13
	je	.L234
	movl	12(%rsp), %esi
	testl	%esi, %esi
	jne	.L242
	movl	0(%r13), %eax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L283:
	leaq	8(%r15), %rbp
	movl	8(%r15), %r12d
	movl	$1, 88(%rsp)
	jmp	.L379
	.p2align 4,,10
	.p2align 3
.L582:
	movl	(%r15), %edx
	movl	%r8d, 24(%rsp)
	cmpl	$93, %edx
	movl	%edx, 16(%rsp)
	je	.L327
	movq	32(%rsp), %rdi
	movl	%r13d, %esi
	leaq	4(%r15), %r14
	call	__collseq_table_lookup
	cmpl	$-1, %eax
	movl	%eax, %ecx
	je	.L346
	movl	24(%rsp), %r8d
	testb	%bl, %bl
	movl	16(%rsp), %edx
	movl	%r8d, %ebp
	je	.L600
.L347:
	cmpl	$91, %edx
	je	.L601
	cmpl	$92, %edx
	jne	.L360
	cmpb	$0, 150(%rsp)
	jne	.L602
.L360:
	testl	%edx, %edx
	je	.L234
.L349:
	testl	%r12d, %r12d
	je	.L355
	movl	%edx, %edi
	movl	%ecx, 16(%rsp)
	call	__GI___towlower
	movl	16(%rsp), %ecx
	movl	%eax, %edx
.L355:
	cmpl	$-1, %ebp
	je	.L384
	cmpl	%ecx, %ebp
	jbe	.L384
.L346:
	movl	(%r14), %edi
	leaq	4(%r14), %r15
	cmpl	$93, %edi
	jne	.L362
	.p2align 4,,10
	.p2align 3
.L583:
	movl	88(%rsp), %r10d
	movq	56(%rsp), %r13
	testl	%r10d, %r10d
	je	.L234
	movq	%r15, %rbx
	jmp	.L577
	.p2align 4,,10
	.p2align 3
.L585:
	movl	(%r15), %r14d
	cmpl	$58, %r14d
	je	.L603
	cmpl	$61, %r14d
	je	.L604
	cmpl	$46, %r14d
	jne	.L328
	movq	%r15, %rax
	xorl	%r14d, %r14d
	jmp	.L333
	.p2align 4,,10
	.p2align 3
.L329:
	testl	%edx, %edx
	je	.L234
.L330:
	addq	$1, %r14
	movq	%rbp, %rax
.L333:
	movl	4(%rax), %edx
	leaq	4(%rax), %rbp
	cmpl	$46, %edx
	jne	.L329
	cmpl	$93, 4(%rbp)
	jne	.L330
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	addq	$12, %rax
	movq	%rax, 24(%rsp)
	movl	8(%rbp), %eax
	movq	%fs:(%rcx), %rdx
	cmpl	$45, %eax
	movl	%eax, 16(%rsp)
	movq	24(%rdx), %rdx
	movl	64(%rdx), %ecx
	je	.L605
.L332:
	testl	%ecx, %ecx
	jne	.L400
	cmpq	$1, %r14
	jne	.L234
	movq	56(%rsp), %rax
	movl	4(%r15), %r8d
	cmpl	%r8d, (%rax)
	je	.L402
.L342:
	leaq	12(%rbp), %r15
	movl	16(%rsp), %edi
	jmp	.L336
	.p2align 4,,10
	.p2align 3
.L584:
	movl	(%r15), %edi
	testl	%edi, %edi
	je	.L234
	testl	%r12d, %r12d
	je	.L287
	call	__GI___towlower
	movl	%eax, %edi
.L287:
	movl	4(%r15), %r14d
	addq	$4, %r15
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L604:
	movl	4(%r15), %esi
	testl	%esi, %esi
	je	.L343
	cmpl	$61, 8(%r15)
	jne	.L343
	cmpl	$93, 12(%r15)
	jne	.L343
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	leaq	16(%r15), %r14
	movq	%fs:(%rax), %rax
	movq	24(%rax), %rax
	movl	64(%rax), %r8d
	testl	%r8d, %r8d
	jne	.L304
	movq	56(%rsp), %rax
	cmpl	(%rax), %esi
	je	.L403
.L309:
	movl	16(%r15), %edi
	addq	$20, %r15
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L603:
	movq	%r15, %rbp
	xorl	%eax, %eax
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L291:
	leal	-97(%rdx), %ecx
	cmpl	$24, %ecx
	ja	.L343
	addq	$1, %rax
	movq	%rbx, %rbp
	cmpq	$2048, %rax
	movl	%edx, 2220(%rsp,%rax,4)
	je	.L234
.L299:
	movl	4(%rbp), %edx
	leaq	4(%rbp), %rbx
	cmpl	$58, %edx
	jne	.L291
	cmpl	$93, 4(%rbx)
	jne	.L343
	movl	$0, 2224(%rsp,%rax,4)
	movq	72(%rsp), %rax
	leaq	2224(%rsp), %rdi
	movl	2224(%rsp), %ecx
	movq	%rax, %rsi
	leaq	2048(%rax), %r8
	jmp	.L294
	.p2align 4,,10
	.p2align 3
.L607:
	leal	-32(%rcx), %eax
	cmpl	$94, %eax
	ja	.L234
	leaq	1(%rsi), %rax
	addq	$4, %rdi
	movb	%cl, -1(%rax)
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	je	.L606
	movq	%rax, %rsi
.L294:
	cmpq	%r8, %rsi
	sete	%al
	cmpl	$36, %ecx
	sete	%dl
	orl	%edx, %eax
	movl	%ecx, %edx
	andl	$-33, %edx
	cmpl	$64, %edx
	sete	%dl
	orb	%dl, %al
	je	.L607
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L581:
	cmpb	$0, 16(%rsp)
	je	.L282
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L590:
	movl	48(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L577
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L580:
	leaq	.LC0(%rip), %rdi
	movl	%r11d, 24(%rsp)
	call	__GI_getenv
	cmpq	$1, %rax
	movl	24(%rsp), %r11d
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, posixly_correct(%rip)
	jmp	.L279
	.p2align 4,,10
	.p2align 3
.L597:
	movl	%ebx, %edi
	call	__GI___towlower
	movl	%eax, %ebx
	jmp	.L273
	.p2align 4,,10
	.p2align 3
.L241:
	cmpq	(%rsp), %r13
	je	.L234
	movl	0(%r13), %eax
	jmp	.L377
	.p2align 4,,10
	.p2align 3
.L602:
	leaq	8(%r15), %r14
	movl	4(%r15), %edx
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L601:
	cmpl	$46, 4(%r15)
	jne	.L349
	xorl	%r8d, %r8d
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L350:
	testl	%eax, %eax
	je	.L234
.L351:
	addq	$1, %r8
	movq	%rbx, %r14
.L354:
	movl	4(%r14), %eax
	leaq	4(%r14), %rbx
	cmpl	$46, %eax
	jne	.L350
	cmpl	$93, 4(%rbx)
	jne	.L351
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	addq	$12, %r14
	movq	%fs:(%rax), %rax
	movq	24(%rax), %rax
	movl	64(%rax), %edx
	testl	%edx, %edx
	jne	.L608
	cmpq	$1, %r8
	jne	.L234
	movl	8(%r15), %edx
	jmp	.L355
	.p2align 4,,10
	.p2align 3
.L600:
	movq	32(%rsp), %rdi
	movl	%r8d, %esi
	movl	%eax, 24(%rsp)
	call	__collseq_table_lookup
	movl	24(%rsp), %ecx
	movl	%eax, %ebp
	movl	16(%rsp), %edx
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L598:
	subq	$8, %rsp
	movzbl	%r15b, %ecx
	movq	%r13, %r9
	pushq	10504(%rsp)
	movl	40(%rsp), %r8d
	movq	%r12, %rsi
	movq	16(%rsp), %rdx
	movq	72(%rsp), %rdi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%r15
	popq	%rdx
	jne	.L275
.L276:
	movq	2224(%rsp), %r15
	testq	%r15, %r15
	jne	.L265
.L264:
	movl	$0, 48(%rsp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L596:
	movl	48(%rsp), %r9d
	testl	%r9d, %r9d
	jne	.L609
	movl	12(%rsp), %ecx
	movl	64(%rsp), %r15d
	testl	%ecx, %ecx
	je	.L273
	jmp	.L597
	.p2align 4,,10
	.p2align 3
.L589:
	cmpl	$58, %eax
	je	.L610
	cmpl	$61, %eax
	je	.L611
	cmpl	$46, %eax
	jne	.L365
	jmp	.L373
	.p2align 4,,10
	.p2align 3
.L374:
	movq	%rdx, %rbx
.L373:
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	je	.L234
	cmpl	$46, %eax
	jne	.L374
	cmpl	$93, 4(%rdx)
	jne	.L374
	leaq	12(%rbx), %r15
	movl	12(%rbx), %eax
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L588:
	movl	4(%r15), %r9d
	testl	%r9d, %r9d
	je	.L234
	movl	8(%r15), %eax
	addq	$8, %r15
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L611:
	movl	8(%r15), %r8d
	testl	%r8d, %r8d
	je	.L234
	cmpl	$61, 12(%r15)
	jne	.L234
	cmpl	$93, 16(%r15)
	jne	.L234
	movl	20(%r15), %eax
	addq	$20, %r15
	jmp	.L375
	.p2align 4,,10
	.p2align 3
.L579:
	cmpq	(%rsp), %r13
	movl	$0, 48(%rsp)
	je	.L222
	testb	$8, 8(%rsp)
	je	.L234
	cmpl	$47, 0(%r13)
	jne	.L234
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L384:
	movq	32(%rsp), %rdi
	movl	%edx, %esi
	movl	%ecx, 16(%rsp)
	call	__collseq_table_lookup
	cmpl	$-1, %eax
	movl	16(%rsp), %ecx
	je	.L612
.L361:
	cmpl	%ecx, %ebp
	cmovb	%ecx, %ebp
	cmpl	%ebp, %eax
	jb	.L346
.L403:
	movq	56(%rsp), %r13
	movq	%r14, %r15
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L586:
	movq	80(%rsp), %rbx
	movl	92(%rsp), %r11d
	movq	56(%rsp), %r13
	jmp	.L326
.L610:
	leaq	8(%r15), %rdx
	movl	8(%r15), %ecx
	leaq	8196(%r15), %rsi
	jmp	.L385
	.p2align 4,,10
	.p2align 3
.L368:
	subl	$97, %ecx
	cmpl	$24, %ecx
	ja	.L369
	addq	$4, %rdx
	movl	(%rdx), %ecx
	cmpq	%rsi, %rdx
	je	.L234
.L385:
	cmpl	$58, %ecx
	jne	.L368
	cmpl	$93, 4(%rdx)
	je	.L613
.L369:
	leaq	-4(%r15), %rdx
.L370:
	leaq	8(%rdx), %r15
	jmp	.L375
.L233:
	cmpl	$40, 4(%r15)
	jne	.L236
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rbx, %rsi
	pushq	10504(%rsp)
	movzbl	32(%rsp), %r8d
	movl	$63, %edi
	movl	24(%rsp), %r9d
	movq	16(%rsp), %rcx
	call	ext_wmatch
	cmpl	$-1, %eax
	popq	%r9
	popq	%r10
	je	.L236
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L591:
	cmpl	$40, 4(%r15)
	jne	.L225
	subq	$8, %rsp
	movq	%rbx, %rsi
	movl	%r11d, %edi
	pushq	10504(%rsp)
	movzbl	32(%rsp), %r8d
	movq	%r13, %rdx
	movl	24(%rsp), %r9d
	movq	16(%rsp), %rcx
	movl	%r11d, 32(%rsp)
	call	ext_wmatch
	popq	%rcx
	cmpl	$-1, %eax
	popq	%rsi
	movl	16(%rsp), %r11d
	je	.L225
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L592:
	cmpl	$40, 4(%r15)
	jne	.L245
	subq	$8, %rsp
	movq	%r13, %rdx
	movq	%rbx, %rsi
	pushq	10504(%rsp)
	movzbl	32(%rsp), %r8d
	movl	$42, %edi
	movl	24(%rsp), %r9d
	movq	16(%rsp), %rcx
	call	ext_wmatch
	cmpl	$-1, %eax
	popq	%rdx
	popq	%rcx
	je	.L246
.L405:
	movl	%eax, 48(%rsp)
	jmp	.L222
	.p2align 4,,10
	.p2align 3
.L605:
	movl	12(%rbp), %edi
	testl	%edi, %edi
	je	.L332
	testl	%ecx, %ecx
	jne	.L399
	cmpq	$1, %r14
	jne	.L234
	movl	4(%r15), %r8d
	jmp	.L342
.L400:
	movl	$0, 120(%rsp)
.L334:
	movl	168(%rdx), %eax
	movq	176(%rdx), %rcx
	movq	184(%rdx), %r8
	testl	%eax, %eax
	jle	.L337
	subl	$1, %eax
	movl	%r12d, 144(%rsp)
	movl	%r13d, 128(%rsp)
	leaq	8(%rcx,%rax,8), %r11
	leaq	4(%r15), %rax
	movq	%rbp, 136(%rsp)
	movb	%bl, 151(%rsp)
	movq	%r14, %rbp
	movq	%r15, 152(%rsp)
	movq	%rax, 104(%rsp)
	movq	%r8, %r13
	movq	%r11, %r14
	movq	%rcx, %r12
	jmp	.L340
	.p2align 4,,10
	.p2align 3
.L338:
	addq	$8, %r12
	cmpq	%r12, %r14
	je	.L614
.L340:
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L338
	movslq	4(%r12), %rdx
	movq	%rdx, %rax
	movzbl	0(%r13,%rdx), %edx
	leal	1(%rax,%rdx), %eax
	movslq	%eax, %rdx
	movzbl	0(%r13,%rdx), %edx
	leal	4(%rax,%rdx), %eax
	andl	$-4, %eax
	cltq
	leaq	4(%r13,%rax), %rbx
	movslq	(%rbx), %rax
	cmpq	%rbp, %rax
	movq	%rax, %r15
	jne	.L338
	leaq	4(%rbx), %rax
	movq	104(%rsp), %rdi
	movq	%rbp, %rdx
	movq	%rax, %rsi
	movq	%rax, 112(%rsp)
	call	__wmemcmp@PLT
	testl	%eax, %eax
	jne	.L338
	movl	120(%rsp), %eax
	movq	%rbp, %r14
	movl	144(%rsp), %r12d
	movl	%r15d, %r10d
	movl	128(%rsp), %r13d
	movq	136(%rsp), %rbp
	movq	%rbx, %r9
	testl	%eax, %eax
	jne	.L341
	movq	112(%rsp), %rsi
	movq	56(%rsp), %rdi
	movq	%r14, %rdx
	movq	%r9, 120(%rsp)
	movl	%r10d, 104(%rsp)
	call	__wmemcmp@PLT
	testl	%eax, %eax
	movl	104(%rsp), %r10d
	movq	120(%rsp), %r9
	je	.L615
.L341:
	leal	1(%r10), %eax
	leaq	12(%rbp), %r15
	movl	16(%rsp), %edi
	movl	$1, %ebx
	cltq
	movl	(%r9,%rax,4), %r8d
	jmp	.L336
.L595:
	leal	-33(%rbx), %eax
	cmpl	$31, %eax
	ja	.L261
	movl	$2147484673, %edx
	btq	%rax, %rdx
	jnc	.L261
	cmpl	$40, 0(%rbp)
	jne	.L261
.L260:
	cmpq	%r13, %r14
	leaq	-4(%rbp), %rbx
	jbe	.L234
	movzbl	16(%rsp), %ecx
	movl	68(%rsp), %r12d
	leaq	2224(%rsp), %rbp
	movq	(%rsp), %r15
.L266:
	subq	$8, %rsp
	movq	%rbp, %r9
	movl	%r12d, %r8d
	pushq	10504(%rsp)
	movq	%r15, %rdx
	movq	%r13, %rsi
	movq	%rbx, %rdi
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%r10
	popq	%r11
	je	.L276
	addq	$4, %r13
	xorl	%ecx, %ecx
	cmpq	%r13, %r14
	ja	.L266
	jmp	.L234
.L612:
	cmpl	%ecx, %ebp
	jne	.L346
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L614:
	movzbl	151(%rsp), %ebx
	movq	%rbp, %r14
	movl	144(%rsp), %r12d
	movq	152(%rsp), %r15
	movl	128(%rsp), %r13d
	movq	136(%rsp), %rbp
.L337:
	cmpq	$1, %r14
	jne	.L234
	movl	120(%rsp), %ecx
	movl	4(%r15), %r8d
	testl	%ecx, %ecx
	jne	.L342
	movq	56(%rsp), %rax
	cmpl	%r8d, (%rax)
	jne	.L342
.L402:
	movq	56(%rsp), %r13
	movq	24(%rsp), %r15
	jmp	.L302
	.p2align 4,,10
	.p2align 3
.L606:
	movq	72(%rsp), %rdi
	movb	$0, 1(%rsi)
	call	__wctype@PLT
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.L234
	movq	56(%rsp), %rax
	movl	(%rax), %edi
	call	__GI___iswctype
	testl	%eax, %eax
	jne	.L300
	leaq	12(%rbx), %r15
	movl	8(%rbx), %edi
	jmp	.L301
.L613:
	movl	8(%rdx), %eax
	jmp	.L370
.L593:
	movzbl	16(%rsp), %ecx
	movq	40(%rsp), %rax
	movl	$0, 48(%rsp)
	movq	%r15, (%rax)
	movq	%r13, 8(%rax)
	movb	%cl, 16(%rax)
	jmp	.L222
.L304:
	movq	136(%rax), %rdx
	movq	144(%rax), %rbp
	movq	152(%rax), %rbx
	movq	160(%rax), %rax
	movq	%rdx, %rdi
	movq	%rdx, 24(%rsp)
	movq	%rax, 16(%rsp)
	call	__collidx_table_lookup
	testl	%eax, %eax
	movl	%eax, %r8d
	movq	24(%rsp), %rdx
	js	.L616
.L305:
	testl	%r8d, %r8d
	je	.L309
	movl	%r8d, %eax
	movq	%rdx, %rdi
	movl	%r8d, 112(%rsp)
	andl	$16777215, %eax
	leaq	0(%rbp,%rax,4), %r10
	movl	(%r10), %eax
	movq	%r10, 104(%rsp)
	movl	%eax, 24(%rsp)
	movq	56(%rsp), %rax
	movl	(%rax), %esi
	call	__collidx_table_lookup
	testl	%eax, %eax
	movq	104(%rsp), %r10
	movl	112(%rsp), %r8d
	js	.L617
.L310:
	testl	%eax, %eax
	je	.L309
	movl	%eax, %edx
	sarl	$24, %r8d
	sarl	$24, %edx
	cmpl	%edx, %r8d
	jne	.L309
	andl	$16777215, %eax
	leaq	0(%rbp,%rax,4), %rcx
	movl	(%rcx), %edx
	cmpl	24(%rsp), %edx
	jne	.L309
	xorl	%eax, %eax
	jmp	.L325
.L618:
	movl	4(%r10,%rax,4), %esi
	addq	$1, %rax
	cmpl	(%rcx,%rax,4), %esi
	jne	.L309
.L325:
	cmpl	%eax, %edx
	jg	.L618
	cmpl	%eax, %edx
	jne	.L309
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L594:
	movq	%r13, %r14
	movl	48(%rsp), %r13d
	testl	%r13d, %r13d
	je	.L222
	testb	$8, 8(%rsp)
	movl	$0, 48(%rsp)
	jne	.L222
	movq	(%rsp), %rdx
	movl	$47, %esi
	movq	%r14, %rdi
	subq	%r14, %rdx
	sarq	$2, %rdx
	call	__GI___wmemchr
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 48(%rsp)
	jmp	.L222
.L399:
	movl	$1, 120(%rsp)
	jmp	.L334
.L608:
	movl	168(%rax), %edx
	movq	176(%rax), %r9
	movq	184(%rax), %r10
	testl	%edx, %edx
	jle	.L356
	leal	-1(%rdx), %eax
	leaq	8(%r9,%rax,8), %rax
	movq	%rax, 16(%rsp)
	leaq	8(%r15), %rax
	movq	%rax, 104(%rsp)
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L357:
	addq	$8, %r9
	cmpq	%r9, 16(%rsp)
	je	.L356
.L359:
	movl	(%r9), %r11d
	testl	%r11d, %r11d
	je	.L357
	movslq	4(%r9), %rdx
	movq	%rdx, %rax
	movzbl	(%r10,%rdx), %edx
	leal	1(%rax,%rdx), %eax
	movslq	%eax, %rdx
	movzbl	(%r10,%rdx), %edx
	leal	4(%rax,%rdx), %eax
	andl	$-4, %eax
	cltq
	leaq	4(%r10,%rax), %r11
	movslq	(%r11), %rax
	cmpq	%r8, %rax
	movl	%eax, 24(%rsp)
	jne	.L357
	movq	104(%rsp), %rdi
	leaq	4(%r11), %rsi
	movq	%r8, %rdx
	movq	%r9, 136(%rsp)
	movq	%r10, 128(%rsp)
	movl	%ecx, 144(%rsp)
	movq	%r11, 120(%rsp)
	movq	%r8, 112(%rsp)
	call	__wmemcmp@PLT
	testl	%eax, %eax
	movq	112(%rsp), %r8
	movq	120(%rsp), %r11
	movl	144(%rsp), %ecx
	movq	128(%rsp), %r10
	movq	136(%rsp), %r9
	jne	.L357
	movl	24(%rsp), %eax
	addl	$1, %eax
	cmpl	$-1, %ebp
	cltq
	movl	(%r11,%rax,4), %eax
	je	.L361
	cmpl	%ecx, %ebp
	jbe	.L361
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L356:
	cmpq	$1, %r8
	jne	.L234
	movl	8(%r15), %edx
	leaq	12(%rbx), %r14
	jmp	.L355
.L616:
	negl	%eax
	cltq
	leaq	(%rbx,%rax,4), %rax
.L306:
	movl	(%rax), %r8d
	leaq	8(%rax), %rcx
	movslq	4(%rax), %rax
	testl	%r8d, %r8d
	js	.L307
.L619:
	testq	%rax, %rax
	je	.L305
	leaq	(%rcx,%rax,4), %rax
	movl	(%rax), %r8d
	leaq	8(%rax), %rcx
	movslq	4(%rax), %rax
	testl	%r8d, %r8d
	jns	.L619
.L307:
	leaq	(%rcx,%rax,8), %rax
	jmp	.L306
.L609:
	cmpq	%r13, (%rsp)
	movq	%rbp, %r12
	movq	%r13, %r14
	ja	.L575
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L620:
	addq	$4, %r14
	cmpq	%r14, (%rsp)
	jbe	.L234
.L575:
	cmpl	$47, (%r14)
	jne	.L620
	movl	8(%rsp), %r8d
	subq	$8, %rsp
	leaq	4(%r14), %rsi
	pushq	10504(%rsp)
	movq	16(%rsp), %rdx
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movl	%r8d, %ecx
	shrl	$2, %ecx
	andl	$1, %ecx
	call	internal_fnwmatch
	testl	%eax, %eax
	popq	%rdi
	popq	%r8
	je	.L264
	jmp	.L234
	.p2align 4,,10
	.p2align 3
.L300:
	movq	56(%rsp), %r13
	leaq	12(%rbp), %r15
	jmp	.L302
.L617:
	negl	%eax
	cltq
	leaq	(%rbx,%rax,4), %rcx
	movq	96(%rsp), %rax
	movq	56(%rsp), %rbx
.L311:
	movl	(%rcx), %esi
	leaq	8(%rcx), %r9
	movslq	4(%rcx), %rdi
	testl	%esi, %esi
	js	.L621
	cmpq	%rdi, %rax
	movq	%rdi, %r11
	cmovbe	%rax, %r11
	testq	%r11, %r11
	je	.L316
	movl	4(%rbx), %edx
	cmpl	%edx, 8(%rcx)
	jne	.L396
	xorl	%edx, %edx
	jmp	.L317
.L318:
	movl	4(%rbx,%rdx,4), %ecx
	cmpl	%ecx, (%r9,%rdx,4)
	jne	.L397
.L317:
	addq	$1, %rdx
	cmpq	%rdx, %r11
	jne	.L318
.L316:
	cmpq	%rdi, %r11
	je	.L568
	leaq	(%r9,%rdi,4), %rcx
	jmp	.L311
.L621:
	movq	%rdi, %r11
	subq	$1, %r11
	je	.L395
	testq	%rax, %rax
	je	.L314
	movl	4(%rbx), %edx
	cmpl	%edx, 8(%rcx)
	jne	.L314
	xorl	%edx, %edx
.L315:
	addq	$1, %rdx
	cmpq	%rdx, %r11
	je	.L313
	cmpq	%rdx, %rax
	je	.L314
	movl	4(%rbx,%rdx,4), %ecx
	cmpl	%ecx, (%r9,%rdx,4)
	je	.L315
.L314:
	leaq	(%r9,%rdi,8), %rcx
	jmp	.L311
.L395:
	xorl	%edx, %edx
.L313:
	cmpq	%rdx, %rax
	je	.L314
	movl	-4(%r9,%rdi,4), %r11d
	movl	(%rbx,%rdi,4), %edx
	leaq	0(,%rdi,8), %rcx
	cmpl	%edx, %r11d
	jg	.L576
	cmpl	-4(%r9,%rcx), %edx
	jg	.L576
	movl	%esi, %eax
	subl	%r11d, %edx
	movq	16(%rsp), %rcx
	negl	%eax
	movslq	%edx, %rdx
	cltq
	addq	%rdx, %rax
	movl	(%rcx,%rax,4), %eax
	jmp	.L310
.L397:
	movq	%rdx, %r11
	jmp	.L316
.L568:
	movl	%esi, %eax
	jmp	.L310
.L615:
	movq	56(%rsp), %r13
	movq	24(%rsp), %r15
	leaq	-4(%r13,%r14,4), %r13
	jmp	.L302
.L396:
	xorl	%r11d, %r11d
	jmp	.L316
.L576:
	addq	%r9, %rcx
	jmp	.L311
	.size	internal_fnwmatch, .-internal_fnwmatch
	.p2align 4,,15
	.type	ext_match, @function
ext_match:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbx
	leaq	-56(%rbp), %r12
	movl	$3221227009, %r14d
	subq	$120, %rsp
	movl	%edi, -68(%rbp)
	movq	%rsi, %rdi
	movq	%rdx, -112(%rbp)
	movq	%rcx, -96(%rbp)
	movl	%r8d, -120(%rbp)
	movl	%r9d, -84(%rbp)
	movb	%r8b, -85(%rbp)
	movq	$0, -56(%rbp)
	call	__GI_strlen
	movq	%rax, -80(%rbp)
	movl	-68(%rbp), %eax
	leaq	1(%r13), %rsi
	movq	16(%rbp), %r15
	movb	$0, -69(%rbp)
	xorl	%edx, %edx
	movq	%rsi, %rbx
	subl	$63, %eax
	movl	%eax, -104(%rbp)
.L623:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L702
.L760:
	cmpb	$91, %al
	movl	$1, %r8d
	je	.L624
.L625:
	leal	-33(%rax), %ecx
	cmpb	$31, %cl
	jbe	.L772
.L636:
	testq	%rdx, %rdx
	sete	%cl
	cmpb	$124, %al
	sete	%al
	andb	%al, %cl
	movl	%ecx, %r9d
	jne	.L649
	addq	$1, %rbx
	movzbl	(%rbx), %eax
	testb	%al, %al
	jne	.L760
	.p2align 4,,10
	.p2align 3
.L702:
	movl	$-1, %eax
.L632:
.L685:
	cmpb	$0, -69(%rbp)
	je	.L622
	movq	-56(%rbp), %rdi
	movl	%eax, %r12d
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L689:
	cmpb	$0, 8(%rdi)
	movq	(%rdi), %rbx
	jne	.L773
	movq	%rbx, %rdi
.L688:
	testq	%rdi, %rdi
	jne	.L689
	movl	%r12d, %eax
.L622:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L772:
	btq	%rcx, %r14
	jnc	.L637
	cmpb	$40, 1(%rbx)
	je	.L774
.L637:
	cmpb	$41, %al
	jne	.L636
	testq	%rdx, %rdx
	je	.L638
	subq	$1, %rdx
	addq	$1, %rbx
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L774:
	addq	$1, %rdx
	addq	$1, %rbx
	jmp	.L623
	.p2align 4,,10
	.p2align 3
.L649:
	movq	%rbx, %rax
	leaq	16(%r15), %rdi
	subq	%rsi, %rax
	addq	$1, %rax
	cmpl	$1, -104(%rbp)
	cmovbe	-80(%rbp), %rax
	mulq	%r8
	testq	%rax, %rax
	js	.L700
	addq	%rax, %rdi
	jo	.L700
	movq	%rsi, -160(%rbp)
	movq	%r8, -152(%rbp)
	movb	%r9b, -136(%rbp)
	movq	%rax, -144(%rbp)
	movq	%rdi, -128(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-144(%rbp), %rcx
	movq	-128(%rbp), %rdx
	movzbl	-136(%rbp), %r9d
	movq	-152(%rbp), %r8
	movq	-160(%rbp), %rsi
	addq	$16, %rcx
	cmpq	$4096, %rdx
	jbe	.L656
	testl	%eax, %eax
	je	.L775
.L656:
#APP
# 1074 "fnmatch_loop.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %rcx
	andq	$-16, %rcx
	subq	%rcx, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
#APP
# 1074 "fnmatch_loop.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, %r15
	xorl	%eax, %eax
.L657:
	leaq	9(%rcx), %rdi
	movq	%rbx, %rdx
	movq	$0, (%rcx)
	subq	%rsi, %rdx
	movb	%al, 8(%rcx)
	movq	%r8, -136(%rbp)
	movq	%rcx, -128(%rbp)
	call	__GI_mempcpy@PLT
	movb	$0, (%rax)
	movzbl	1(%rbx), %eax
	leaq	1(%rbx), %rsi
	movq	-128(%rbp), %rcx
	movq	-136(%rbp), %r8
	testb	%al, %al
	movq	%rcx, (%r12)
	movq	%rcx, %r12
	je	.L702
	xorl	%edx, %edx
	cmpb	$91, %al
	movq	%rsi, %rbx
	jne	.L625
	.p2align 4,,10
	.p2align 3
.L624:
	movl	posixly_correct(%rip), %edi
	testl	%edi, %edi
	je	.L776
.L626:
	movzbl	1(%rbx), %eax
	cmpb	$33, %al
	je	.L628
	movl	posixly_correct(%rip), %ecx
	testl	%ecx, %ecx
	jns	.L708
	cmpb	$94, %al
	je	.L628
.L708:
	addq	$1, %rbx
.L630:
	cmpb	$93, %al
	jne	.L633
	movzbl	1(%rbx), %eax
	addq	$1, %rbx
	jmp	.L633
	.p2align 4,,10
	.p2align 3
.L777:
	movzbl	(%rbx), %eax
.L633:
	addq	$1, %rbx
	cmpb	$93, %al
	je	.L623
	testb	%al, %al
	jne	.L777
	jmp	.L702
	.p2align 4,,10
	.p2align 3
.L628:
	movzbl	2(%rbx), %eax
	addq	$2, %rbx
	jmp	.L630
	.p2align 4,,10
	.p2align 3
.L638:
	movl	-68(%rbp), %eax
	movq	%rbx, %rdi
	movq	-80(%rbp), %r14
	subq	%rsi, %rdi
	addq	$1, %rdi
	subl	$63, %eax
	cmpl	$1, %eax
	cmova	%rdi, %r14
	leaq	16(%r15), %rdi
	testq	%r14, %r14
	js	.L700
	movq	%rdi, %rdx
	addq	%r14, %rdx
	jo	.L700
	movq	%rdx, %rdi
	movq	%rsi, -104(%rbp)
	movq	%rdx, -80(%rbp)
	call	__GI___libc_alloca_cutoff
	movq	-80(%rbp), %rdx
	addq	$16, %r14
	movq	-104(%rbp), %rsi
	cmpq	$4096, %rdx
	jbe	.L645
	testl	%eax, %eax
	je	.L778
.L645:
#APP
# 1067 "fnmatch_loop.c" 1
	mov %rsp, %rax
# 0 "" 2
#NO_APP
	addq	$30, %r14
	andq	$-16, %r14
	subq	%r14, %rsp
	leaq	15(%rsp), %r14
	andq	$-16, %r14
#APP
# 1067 "fnmatch_loop.c" 1
	sub %rsp , %rax
# 0 "" 2
#NO_APP
	addq	%rax, %r15
	xorl	%eax, %eax
.L646:
	movq	%rbx, %rdx
	leaq	9(%r14), %rdi
	movq	$0, (%r14)
	subq	%rsi, %rdx
	movb	%al, 8(%r14)
	call	__GI_mempcpy@PLT
	movq	%r14, (%r12)
	movq	-56(%rbp), %r12
	movb	$0, (%rax)
	leaq	1(%rbx), %rax
	testq	%r12, %r12
	movq	%rax, -80(%rbp)
	je	.L779
	cmpb	$41, (%rbx)
	jne	.L780
	movl	-68(%rbp), %ebx
	subl	$33, %ebx
	cmpl	$31, %ebx
	ja	.L661
	leaq	.L663(%rip), %rdx
	movslq	(%rdx,%rbx,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L663:
	.long	.L662-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L664-.L663
	.long	.L665-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L661-.L663
	.long	.L666-.L663
	.long	.L667-.L663
	.text
	.p2align 4,,10
	.p2align 3
.L773:
	movq	%rbx, -56(%rbp)
	call	free@PLT
	movq	%rbx, %rdi
	jmp	.L688
	.p2align 4,,10
	.p2align 3
.L776:
	leaq	.LC0(%rip), %rdi
	movq	%rsi, -136(%rbp)
	movq	%rdx, -128(%rbp)
	call	__GI_getenv
	cmpq	$1, %rax
	movq	-136(%rbp), %rsi
	movq	-128(%rbp), %rdx
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, posixly_correct(%rip)
	jmp	.L626
.L778:
	movq	%r14, %rdi
	movq	%rsi, -80(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L700
	movl	$1, %eax
	movb	$1, -69(%rbp)
	movq	-80(%rbp), %rsi
	jmp	.L646
.L664:
	movzbl	-120(%rbp), %ecx
	subq	$8, %rsp
	movl	-84(%rbp), %r8d
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	-80(%rbp), %rdi
	pushq	%r15
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%r11
	popq	%rbx
	je	.L685
.L665:
	movl	-84(%rbp), %eax
	movq	%r12, -128(%rbp)
	movq	-96(%rbp), %r12
	movl	%eax, %ebx
	movl	%eax, %r14d
	andl	$-5, %ebx
	andl	$1, %r14d
	cmovne	%eax, %ebx
	leaq	-1(%r13), %rax
	movq	-112(%rbp), %r13
	movl	%r14d, -104(%rbp)
	movq	%rax, -120(%rbp)
.L677:
	cmpq	%r12, %r13
	ja	.L670
	movzbl	-85(%rbp), %eax
	movq	%r13, %r14
	movl	%eax, -68(%rbp)
	movq	-128(%rbp), %rax
	addq	$9, %rax
	movq	%rax, -96(%rbp)
	jmp	.L676
	.p2align 4,,10
	.p2align 3
.L781:
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	%ebx, %r8d
	pushq	%r15
.L768:
	movl	-68(%rbp), %ecx
	movq	-96(%rbp), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%r8
	popq	%r9
	jne	.L672
	cmpq	%r13, %r14
	je	.L673
	xorl	%ecx, %ecx
	cmpb	$47, -1(%r14)
	jne	.L674
	movl	-84(%rbp), %eax
	xorl	%ecx, %ecx
	andl	$5, %eax
	cmpl	$5, %eax
	sete	%cl
.L674:
	subq	$8, %rsp
	movq	-80(%rbp), %rdi
	xorl	%r9d, %r9d
	pushq	%r15
	movq	%r14, %rsi
	movl	%ebx, %r8d
	movq	%r12, %rdx
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	je	.L685
	xorl	%ecx, %ecx
	cmpb	$47, -1(%r14)
	jne	.L675
	movl	-84(%rbp), %eax
	xorl	%ecx, %ecx
	andl	$5, %eax
	cmpl	$5, %eax
	sete	%cl
.L675:
	subq	$8, %rsp
	movq	-120(%rbp), %rdi
	xorl	%r9d, %r9d
	pushq	%r15
	movq	%r12, %rdx
	movl	%ebx, %r8d
	movq	%r14, %rsi
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	je	.L685
.L672:
	addq	$1, %r14
	cmpq	%r14, %r12
	jb	.L670
.L676:
	movl	-104(%rbp), %r10d
	testl	%r10d, %r10d
	je	.L781
	subq	$8, %rsp
	xorl	%r9d, %r9d
	movl	-84(%rbp), %r8d
	pushq	%r15
	jmp	.L768
.L662:
	movq	-96(%rbp), %rsi
	cmpq	%rsi, -112(%rbp)
	ja	.L771
	movl	-84(%rbp), %eax
	movq	-112(%rbp), %rbx
	movq	%r12, -104(%rbp)
	movl	%eax, %r14d
	movq	%rbx, %r13
	andl	$-5, %r14d
	testb	$1, %al
	cmovne	%eax, %r14d
	movzbl	-120(%rbp), %eax
	movl	%eax, -68(%rbp)
	.p2align 4,,10
	.p2align 3
.L680:
	movq	-104(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L681:
	subq	$8, %rsp
	movl	-68(%rbp), %ecx
	leaq	9(%r12), %rdi
	pushq	%r15
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%r13, %rdx
	movq	%rbx, %rsi
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%r8
	popq	%r9
	je	.L684
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.L681
	cmpq	%r13, %rbx
	je	.L782
	xorl	%ecx, %ecx
	cmpb	$47, -1(%r13)
	jne	.L683
	movl	-84(%rbp), %eax
	xorl	%ecx, %ecx
	andl	$5, %eax
	cmpl	$5, %eax
	sete	%cl
.L683:
	subq	$8, %rsp
	movq	-80(%rbp), %rdi
	movq	-96(%rbp), %rdx
	pushq	%r15
	xorl	%r9d, %r9d
	movq	%r13, %rsi
	movl	%r14d, %r8d
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	je	.L685
.L684:
	addq	$1, %r13
	cmpq	%r13, -96(%rbp)
	jnb	.L680
.L771:
	movl	$1, %eax
	jmp	.L685
.L667:
	movzbl	-120(%rbp), %r13d
.L668:
	movl	-84(%rbp), %eax
	movq	-112(%rbp), %r14
	movl	%eax, %ebx
	andl	$-5, %ebx
	testb	$1, %al
	cmovne	%eax, %ebx
	jmp	.L678
	.p2align 4,,10
	.p2align 3
.L783:
	movq	(%r12), %r12
	testq	%r12, %r12
	movq	%r12, -56(%rbp)
	je	.L771
.L678:
	movq	-80(%rbp), %rsi
	leaq	9(%r12), %rdi
	call	__GI_strcat
	subq	$8, %rsp
	movq	-96(%rbp), %rdx
	xorl	%r9d, %r9d
	pushq	%r15
	movl	%ebx, %r8d
	movl	%r13d, %ecx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%r10
	popq	%r11
	jne	.L783
	jmp	.L685
.L666:
	movzbl	-120(%rbp), %r13d
	subq	$8, %rsp
	movl	-84(%rbp), %r8d
	movq	-96(%rbp), %rdx
	movq	-112(%rbp), %rsi
	xorl	%r9d, %r9d
	movq	-80(%rbp), %rdi
	pushq	%r15
	movl	%r13d, %ecx
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%rbx
	popq	%r14
	je	.L685
	jmp	.L668
.L670:
	movq	-128(%rbp), %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	movq	%rax, -128(%rbp)
	movq	%rax, -56(%rbp)
	jne	.L677
	jmp	.L771
	.p2align 4,,10
	.p2align 3
.L775:
	movq	%rcx, %rdi
	movq	%rsi, -144(%rbp)
	movq	%r8, -136(%rbp)
	movb	%r9b, -128(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rcx
	je	.L700
	movzbl	-128(%rbp), %r9d
	movl	$1, %eax
	movq	-136(%rbp), %r8
	movq	-144(%rbp), %rsi
	movb	%r9b, -69(%rbp)
	jmp	.L657
.L673:
	subq	$8, %rsp
	movl	-68(%rbp), %ecx
	movq	-80(%rbp), %rdi
	pushq	%r15
	xorl	%r9d, %r9d
	movq	%r12, %rdx
	movl	%ebx, %r8d
	movq	%r14, %rsi
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L672
	jmp	.L685
.L700:
	movl	$-2, %eax
	jmp	.L685
.L782:
	movl	-68(%rbp), %ecx
	jmp	.L683
.L661:
	leaq	__PRETTY_FUNCTION__.9247(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$1176, %edx
	call	__GI___assert_fail
.L779:
	leaq	__PRETTY_FUNCTION__.9247(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$1078, %edx
	call	__GI___assert_fail
.L780:
	leaq	__PRETTY_FUNCTION__.9247(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$1079, %edx
	call	__GI___assert_fail
	.size	ext_match, .-ext_match
	.p2align 4,,15
	.type	internal_fnmatch, @function
internal_fnmatch:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r10
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r14
	pushq	%rbp
	pushq	%rbx
	movl	%ecx, %r13d
	movq	%r10, %r11
	subq	$2248, %rsp
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%rdx, (%rsp)
	movq	%r9, 32(%rsp)
	movl	%r8d, 8(%rsp)
	movq	%fs:(%rax), %rax
	movq	24(%rax), %rax
	movq	192(%rax), %rax
	movq	%rax, 16(%rsp)
	movl	%r8d, %eax
	andl	$1, %eax
	movl	%eax, %esi
	movl	%eax, 12(%rsp)
	negl	%esi
	andl	$47, %esi
	movl	%esi, 48(%rsp)
	movl	%r8d, %esi
	andl	$-5, %esi
	testl	%eax, %eax
	leaq	175(%rsp), %rax
	movl	%esi, 52(%rsp)
	cmovne	%r8d, %esi
	movl	%esi, 80(%rsp)
	movq	%rax, 40(%rsp)
.L785:
	movzbl	(%r14), %ebx
	leaq	1(%r14), %r15
	testb	%bl, %bl
	je	.L1155
	movl	8(%rsp), %r10d
	andl	$16, %r10d
	je	.L786
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzbl	(%rax,%rbx,4), %ebx
.L786:
	leal	-33(%rbx), %eax
	cmpb	$59, %al
	ja	.L787
	leaq	.L789(%rip), %rdi
	movzbl	%al, %eax
	movslq	(%rdi,%rax,4), %rax
	addq	%rdi, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L789:
	.long	.L788-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L790-.L789
	.long	.L788-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L791-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L792-.L789
	.long	.L788-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L787-.L789
	.long	.L793-.L789
	.long	.L794-.L789
	.text
	.p2align 4,,10
	.p2align 3
.L794:
	testb	$2, 8(%rsp)
	jne	.L802
	movzbl	1(%r14), %eax
	leaq	2(%r14), %r15
	testb	%al, %al
	je	.L796
	testl	%r10d, %r10d
	je	.L803
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	cmpq	(%rsp), %r11
	movq	%fs:(%rdx), %rdx
	movl	(%rdx,%rax,4), %eax
	je	.L796
	movzbl	%al, %eax
.L804:
	movzbl	(%r11), %ecx
	movl	(%rdx,%rcx,4), %edx
	jmp	.L806
	.p2align 4,,10
	.p2align 3
.L793:
	movl	posixly_correct(%rip), %ebx
	testl	%ebx, %ebx
	je	.L1156
.L842:
	cmpq	(%rsp), %r11
	je	.L796
	movzbl	(%r11), %edx
	cmpb	$46, %dl
	je	.L1157
	cmpb	$47, %dl
	jne	.L845
	movl	12(%rsp), %r9d
	testl	%r9d, %r9d
	jne	.L796
.L845:
	movzbl	1(%r14), %ebx
	cmpb	$33, %bl
	je	.L846
	movl	posixly_correct(%rip), %r8d
	testl	%r8d, %r8d
	jns	.L987
	cmpb	$94, %bl
	je	.L846
.L987:
	movq	%r15, %rax
	movl	$0, 56(%rsp)
.L949:
	testl	%r10d, %r10d
	movl	%edx, %r9d
	je	.L848
	movzbl	%dl, %ecx
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movzbl	(%rdx,%rcx,4), %r9d
.L848:
	leaq	1(%rax), %r14
	movl	8(%rsp), %eax
	movq	%r11, 24(%rsp)
	movq	%r15, 64(%rsp)
	shrl	%eax
	xorl	$1, %eax
	movl	%eax, %r8d
	movq	(%rsp), %rax
	andl	$1, %r8d
	subq	%r11, %rax
	movq	40(%rsp), %r11
	subq	$1, %rax
	movq	%rax, 72(%rsp)
	jmp	.L931
	.p2align 4,,10
	.p2align 3
.L864:
	cmpb	%bl, %r9b
	je	.L1146
.L910:
	movl	%ebx, %eax
	addq	$1, %r14
	movzbl	%dl, %ebx
	xorl	%ebp, %ebp
.L903:
	cmpb	$45, %bl
	je	.L1158
.L861:
	cmpb	$93, %bl
	je	.L1159
.L931:
	cmpb	$92, %bl
	sete	%bpl
	andb	%r8b, %bpl
	jne	.L1160
	cmpb	$91, %bl
	je	.L1161
	testb	%bl, %bl
	je	.L1162
.L894:
	movzbl	(%r14), %edx
.L896:
	testl	%r10d, %r10d
	je	.L851
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzbl	(%rax,%rbx,4), %ebx
.L851:
	cmpb	$45, %dl
	jne	.L864
	movzbl	1(%r14), %eax
	cmpb	$93, %al
	je	.L864
	testb	%al, %al
	je	.L864
	jmp	.L910
	.p2align 4,,10
	.p2align 3
.L792:
	testb	$32, 8(%rsp)
	jne	.L795
.L798:
	cmpq	(%rsp), %r11
	je	.L796
	movzbl	(%r11), %eax
	cmpb	$47, %al
	je	.L1163
	cmpb	$46, %al
	sete	%al
	andb	%al, %r13b
	je	.L801
	.p2align 4,,10
	.p2align 3
.L796:
	movl	$1, 12(%rsp)
.L784:
	movl	12(%rsp), %eax
	addq	$2248, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L791:
	movl	8(%rsp), %eax
	andl	$5, %eax
	cmpl	$5, %eax
	jne	.L787
	cmpq	(%rsp), %r11
	je	.L796
	cmpb	$47, (%r11)
	jne	.L796
	movl	$1, %r13d
	jmp	.L801
	.p2align 4,,10
	.p2align 3
.L788:
	testb	$32, 8(%rsp)
	jne	.L1164
.L787:
	cmpq	(%rsp), %r11
	je	.L796
.L893:
	movzbl	(%r11), %edx
	testl	%r10d, %r10d
	movq	%rdx, %rax
	je	.L947
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movl	(%rdx,%rax,4), %edx
.L947:
	cmpl	%ebx, %edx
	jne	.L796
.L1154:
	xorl	%r13d, %r13d
.L801:
	addq	$1, %r11
	movq	%r15, %r14
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L790:
	movl	8(%rsp), %ebp
	andl	$32, %ebp
	jne	.L1165
.L807:
	cmpq	$0, 32(%rsp)
	jne	.L1166
.L808:
	cmpq	(%rsp), %r11
	je	.L809
	cmpb	$46, (%r11)
	jne	.L809
	testb	%r13b, %r13b
	jne	.L796
.L809:
	movzbl	1(%r14), %ebx
	leaq	2(%r14), %r15
	cmpb	$63, %bl
	je	.L986
	cmpb	$42, %bl
	jne	.L811
.L986:
	testl	%ebp, %ebp
	movl	%r10d, %r14d
	setne	%r12b
	.p2align 4,,10
	.p2align 3
.L1087:
	cmpb	$40, (%r15)
	jne	.L817
	testb	%r12b, %r12b
	jne	.L813
.L817:
	cmpb	$63, %bl
	je	.L815
.L1151:
	movq	%r15, %rax
.L816:
	movzbl	(%rax), %ebx
	leaq	1(%rax), %r15
	cmpb	$63, %bl
	je	.L1087
	cmpb	$42, %bl
	je	.L1087
	movl	%r14d, %r10d
.L811:
	testb	%bl, %bl
	je	.L1167
	movq	(%rsp), %r12
	movl	48(%rsp), %esi
	movq	%r11, %rdi
	movq	%r11, 24(%rsp)
	movl	%r10d, 56(%rsp)
	movq	$0, 176(%rsp)
	movq	%r12, %rdx
	subq	%r11, %rdx
	call	__GI_memchr
	testq	%rax, %rax
	movq	%rax, %r14
	movq	24(%rsp), %r11
	cmove	%r12, %r14
	cmpb	$91, %bl
	je	.L822
	testl	%ebp, %ebp
	movl	56(%rsp), %r10d
	jne	.L1168
.L823:
	cmpb	$47, %bl
	je	.L1169
	movl	12(%rsp), %ecx
	movl	52(%rsp), %ebp
	movl	8(%rsp), %eax
	testl	%ecx, %ecx
	cmovne	%eax, %ebp
	cmpb	$92, %bl
	jne	.L830
	testb	$2, %al
	jne	.L830
	movzbl	(%r15), %ebx
.L830:
	testl	%r10d, %r10d
	jne	.L1170
.L835:
	subq	$1, %r15
	cmpq	%r14, %r11
	jnb	.L796
	leaq	176(%rsp), %r12
	.p2align 4,,10
	.p2align 3
.L840:
	movzbl	(%r11), %edx
	testl	%r10d, %r10d
	movq	%rdx, %rax
	je	.L837
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	movl	(%rdx,%rax,4), %edx
.L837:
	cmpl	%ebx, %edx
	je	.L1171
.L838:
	addq	$1, %r11
	xorl	%r13d, %r13d
	cmpq	%r11, %r14
	jne	.L840
	movq	176(%rsp), %r14
	testq	%r14, %r14
	je	.L796
.L827:
	movq	184(%rsp), %r11
	movzbl	192(%rsp), %r13d
	jmp	.L785
	.p2align 4,,10
	.p2align 3
.L815:
	cmpq	%r11, (%rsp)
	je	.L796
	cmpb	$47, (%r11)
	je	.L1172
.L818:
	addq	$1, %r11
	jmp	.L1151
	.p2align 4,,10
	.p2align 3
.L813:
	movq	%r15, %rdi
	movq	%r11, 24(%rsp)
	call	end_pattern
	cmpq	%rax, %r15
	movq	24(%rsp), %r11
	jne	.L816
	jmp	.L817
	.p2align 4,,10
	.p2align 3
.L1172:
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.L818
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L802:
	cmpq	(%rsp), %r11
	je	.L796
	testl	%r10d, %r10d
	movl	$92, %eax
	jne	.L1173
.L805:
	movzbl	(%r11), %edx
.L806:
	cmpl	%eax, %edx
	je	.L1154
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L846:
	leaq	2(%r14), %rax
	movzbl	2(%r14), %ebx
	movl	$1, 56(%rsp)
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L1158:
	movzbl	(%r14), %edx
	cmpb	$93, %dl
	je	.L894
	movq	16(%rsp), %rdi
	movzbl	%r9b, %esi
	testb	%bpl, %bpl
	leaq	1(%r14), %r12
	movzbl	%dl, %ecx
	movsbl	%al, %ebx
	movzbl	(%rdi,%rsi), %r13d
	jne	.L913
	movzbl	%al, %ebx
	movq	16(%rsp), %rax
	movzbl	(%rax,%rbx), %ebx
.L913:
	cmpb	$91, %dl
	je	.L1174
	cmpb	$92, %dl
	jne	.L928
	testb	%r8b, %r8b
	je	.L928
	movzbl	1(%r14), %ecx
	leaq	2(%r14), %r12
.L928:
	testb	%cl, %cl
	je	.L796
.L916:
	testl	%r10d, %r10d
	je	.L929
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	cmpl	%r13d, %ebx
	movq	%fs:(%rax), %rax
	movl	(%rax,%rcx,4), %ecx
	ja	.L922
.L927:
	movq	16(%rsp), %rax
	movzbl	%cl, %ecx
	movzbl	(%rax,%rcx), %eax
.L930:
	cmpl	%r13d, %ebx
	cmovb	%r13d, %ebx
	cmpl	%ebx, %eax
	jnb	.L981
.L922:
	leaq	1(%r12), %r14
	movzbl	(%r12), %ebx
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L1161:
	movzbl	(%r14), %edx
	cmpb	$58, %dl
	je	.L1175
	cmpb	$61, %dl
	je	.L1176
	cmpb	$46, %dl
	jne	.L896
	movq	%r14, %rbx
	jmp	.L895
	.p2align 4,,10
	.p2align 3
.L897:
	testb	%al, %al
	je	.L796
.L898:
	movq	%rdx, %rbx
.L895:
	movzbl	1(%rbx), %eax
	movq	%rbx, %r13
	leaq	1(%rbx), %rdx
	subq	%r14, %r13
	cmpb	$46, %al
	jne	.L897
	cmpb	$93, 1(%rdx)
	jne	.L898
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	leaq	3(%rbx), %rcx
	movq	%rcx, 88(%rsp)
	movzbl	3(%rbx), %ecx
	movq	%fs:(%rax), %rax
	movq	24(%rax), %rax
	cmpb	$45, %cl
	movb	%cl, 84(%rsp)
	movl	64(%rax), %edx
	je	.L1177
.L900:
	testl	%edx, %edx
	jne	.L977
	cmpq	$1, %r13
	jne	.L796
	movq	24(%rsp), %rcx
	movzbl	1(%r14), %eax
	cmpb	%al, (%rcx)
	je	.L979
.L909:
	leaq	4(%rbx), %r14
	movzbl	84(%rsp), %ebx
	jmp	.L903
	.p2align 4,,10
	.p2align 3
.L1160:
	movzbl	(%r14), %ebx
	testb	%bl, %bl
	je	.L796
	testl	%r10d, %r10d
	je	.L850
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzbl	(%rax,%rbx,4), %ebx
.L850:
	movzbl	1(%r14), %edx
	addq	$1, %r14
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L929:
	cmpl	%r13d, %ebx
	jbe	.L927
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1176:
	movzbl	1(%r14), %eax
	testb	%al, %al
	je	.L864
	cmpb	$61, 2(%r14)
	jne	.L864
	cmpb	$93, 3(%r14)
	jne	.L864
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rcx
	leaq	4(%r14), %r13
	movq	%fs:(%rcx), %rdx
	movq	24(%rdx), %rcx
	movl	64(%rcx), %edi
	testl	%edi, %edi
	jne	.L865
	movq	24(%rsp), %rcx
	cmpb	(%rcx), %al
	je	.L1178
.L866:
	movzbl	4(%r14), %ebx
	addq	$5, %r14
	jmp	.L861
	.p2align 4,,10
	.p2align 3
.L1174:
	cmpb	$46, 1(%r14)
	jne	.L916
	movq	%r14, %rcx
	movq	%r12, %rbp
	notq	%rcx
	jmp	.L915
	.p2align 4,,10
	.p2align 3
.L917:
	testb	%al, %al
	je	.L796
.L918:
	movq	%rdx, %rbp
.L915:
	movzbl	1(%rbp), %eax
	leaq	0(%rbp,%rcx), %r15
	leaq	1(%rbp), %rdx
	cmpb	$46, %al
	jne	.L917
	cmpb	$93, 1(%rdx)
	jne	.L918
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	leaq	3(%rbp), %r12
	movq	%fs:(%rax), %rax
	movq	24(%rax), %rax
	movl	64(%rax), %edx
	testl	%edx, %edx
	jne	.L1179
	cmpq	$1, %r15
	jne	.L796
	cmpl	%r13d, %ebx
	movzbl	2(%r14), %ecx
	jbe	.L927
	jmp	.L922
	.p2align 4,,10
	.p2align 3
.L1175:
	movq	%r14, %rbp
	xorl	%eax, %eax
	jmp	.L859
	.p2align 4,,10
	.p2align 3
.L854:
	leal	-97(%rdx), %ecx
	cmpb	$24, %cl
	ja	.L855
	addq	$1, %rax
	movq	%r12, %rbp
	cmpq	$2048, %rax
	movb	%dl, (%r11,%rax)
	je	.L796
.L859:
	movzbl	1(%rbp), %edx
	leaq	1(%rbp), %r12
	cmpb	$58, %dl
	jne	.L854
	cmpb	$93, 1(%r12)
	je	.L1180
.L855:
	movzbl	(%r14), %edx
	jmp	.L851
	.p2align 4,,10
	.p2align 3
.L1163:
	movl	12(%rsp), %esi
	testl	%esi, %esi
	je	.L1154
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1157:
	testb	%r13b, %r13b
	je	.L845
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1156:
	leaq	.LC0(%rip), %rdi
	movq	%r11, 56(%rsp)
	movl	%r10d, 24(%rsp)
	call	__GI_getenv
	cmpq	$1, %rax
	movq	56(%rsp), %r11
	movl	24(%rsp), %r10d
	sbbl	%eax, %eax
	orl	$1, %eax
	movl	%eax, posixly_correct(%rip)
	jmp	.L842
	.p2align 4,,10
	.p2align 3
.L1170:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rax
	movzbl	(%rax,%rbx,4), %ebx
	jmp	.L835
	.p2align 4,,10
	.p2align 3
.L803:
	cmpq	(%rsp), %r11
	jne	.L805
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1173:
	movq	__libc_tsd_CTYPE_TOLOWER@gottpoff(%rip), %rdx
	movq	%fs:(%rdx), %rdx
	jmp	.L804
	.p2align 4,,10
	.p2align 3
.L1171:
	movl	%r10d, 56(%rsp)
	subq	$8, %rsp
	movzbl	%r13b, %ecx
	pushq	2312(%rsp)
	movq	16(%rsp), %rdx
	movq	%r11, %rsi
	movq	%r12, %r9
	movl	%ebp, %r8d
	movq	%r15, %rdi
	movq	%r11, 40(%rsp)
	call	internal_fnmatch
	popq	%r13
	testl	%eax, %eax
	popq	%rdx
	movq	24(%rsp), %r11
	movl	56(%rsp), %r10d
	jne	.L838
.L839:
	movq	176(%rsp), %r14
	testq	%r14, %r14
	jne	.L827
.L826:
	movl	$0, 12(%rsp)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L1169:
	movl	12(%rsp), %r8d
	testl	%r8d, %r8d
	jne	.L1181
	testl	%r10d, %r10d
	movl	52(%rsp), %ebp
	je	.L835
	jmp	.L1170
	.p2align 4,,10
	.p2align 3
.L1146:
	movq	24(%rsp), %r11
.L862:
	movzbl	(%r14), %eax
.L944:
	cmpb	$93, %al
	leaq	1(%r14), %r15
	je	.L1182
.L945:
	testb	%al, %al
	je	.L796
	cmpb	$92, %al
	jne	.L932
	testb	%r8b, %r8b
	jne	.L1183
.L932:
	cmpb	$91, %al
	movzbl	1(%r14), %eax
	je	.L1184
.L934:
	movq	%r15, %r14
	cmpb	$93, %al
	leaq	1(%r14), %r15
	jne	.L945
.L1182:
	movl	56(%rsp), %esi
	testl	%esi, %esi
	je	.L1154
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1184:
	cmpb	$58, %al
	je	.L1185
	cmpb	$61, %al
	je	.L1186
	cmpb	$46, %al
	jne	.L934
	jmp	.L942
	.p2align 4,,10
	.p2align 3
.L943:
	movq	%rdx, %r15
.L942:
	movzbl	1(%r15), %eax
	leaq	1(%r15), %rdx
	testb	%al, %al
	je	.L796
	cmpb	$46, %al
	jne	.L943
	cmpb	$93, 1(%rdx)
	jne	.L943
	leaq	3(%r15), %r14
	movzbl	3(%r15), %eax
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1183:
	cmpb	$0, 1(%r14)
	je	.L796
	movzbl	2(%r14), %eax
	addq	$2, %r14
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1186:
	cmpb	$0, 2(%r14)
	je	.L796
	cmpb	$61, 3(%r14)
	jne	.L796
	cmpb	$93, 4(%r14)
	jne	.L796
	movzbl	5(%r14), %eax
	addq	$5, %r14
	jmp	.L944
	.p2align 4,,10
	.p2align 3
.L1159:
	movl	56(%rsp), %edi
	movq	24(%rsp), %r11
	testl	%edi, %edi
	je	.L796
	movq	%r14, %r15
	jmp	.L1154
	.p2align 4,,10
	.p2align 3
.L1155:
	movq	%r11, %r10
	cmpq	(%rsp), %r10
	movl	8(%rsp), %r11d
	movl	$0, 12(%rsp)
	je	.L784
	andl	$8, %r11d
	je	.L796
	cmpb	$47, (%r10)
	jne	.L796
	jmp	.L784
.L1162:
	movq	64(%rsp), %r15
	movq	24(%rsp), %r11
	movl	$91, %ebx
	jmp	.L893
.L1180:
	leaq	176(%rsp), %rdi
	movq	%r11, 104(%rsp)
	movb	%r8b, 96(%rsp)
	movb	%r9b, 88(%rsp)
	movl	%r10d, 84(%rsp)
	movb	$0, 176(%rsp,%rax)
	call	__wctype@PLT
	testq	%rax, %rax
	movl	84(%rsp), %r10d
	movzbl	88(%rsp), %r9d
	movzbl	96(%rsp), %r8d
	movq	104(%rsp), %r11
	je	.L796
	movq	24(%rsp), %rcx
	movzbl	(%rcx), %ecx
	movl	%ecx, %edx
	shrb	$5, %dl
	movzbl	%dl, %edx
	movl	-32(%rax,%rdx,4), %eax
	btl	%ecx, %eax
	jc	.L860
	leaq	4(%rbp), %r14
	movzbl	2(%r12), %ebx
	jmp	.L861
.L1185:
	leaq	2(%r14), %rdx
	movzbl	2(%r14), %ecx
	leaq	2049(%r14), %rsi
	jmp	.L955
	.p2align 4,,10
	.p2align 3
.L937:
	subl	$97, %ecx
	cmpb	$24, %cl
	ja	.L938
	addq	$1, %rdx
	movzbl	(%rdx), %ecx
	cmpq	%rsi, %rdx
	je	.L796
.L955:
	cmpb	$58, %cl
	jne	.L937
	cmpb	$93, 1(%rdx)
	je	.L1187
.L938:
	leaq	-1(%r14), %rdx
.L939:
	leaq	2(%rdx), %r14
	jmp	.L944
.L1164:
	cmpb	$40, 1(%r14)
	jne	.L787
	movl	%r10d, 56(%rsp)
	subq	$8, %rsp
	movq	%r11, %rdx
	pushq	2312(%rsp)
	movl	24(%rsp), %r9d
	movzbl	%bl, %edi
	movq	16(%rsp), %rcx
	movzbl	%r13b, %r8d
	movq	%r15, %rsi
	movq	%r11, 40(%rsp)
	call	ext_match
	popq	%rdx
	cmpl	$-1, %eax
	popq	%rcx
	movq	24(%rsp), %r11
	movl	56(%rsp), %r10d
	je	.L787
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L1165:
	cmpb	$40, 1(%r14)
	jne	.L807
	movl	%r10d, 56(%rsp)
	subq	$8, %rsp
	movq	%r11, %rdx
	pushq	2312(%rsp)
	movl	24(%rsp), %r9d
	movzbl	%r13b, %r8d
	movq	16(%rsp), %rcx
	movq	%r15, %rsi
	movl	$42, %edi
	movq	%r11, 40(%rsp)
	call	ext_match
	popq	%rdx
	cmpl	$-1, %eax
	popq	%rcx
	movq	24(%rsp), %r11
	movl	56(%rsp), %r10d
	je	.L808
	jmp	.L983
	.p2align 4,,10
	.p2align 3
.L795:
	cmpb	$40, 1(%r14)
	jne	.L798
	subq	$8, %rsp
	movzbl	%r13b, %r8d
	movq	%r11, %rdx
	pushq	2312(%rsp)
	movl	24(%rsp), %r9d
	movl	$63, %edi
	movq	16(%rsp), %rcx
	movq	%r15, %rsi
	movq	%r11, 40(%rsp)
	call	ext_match
	popq	%rdi
	cmpl	$-1, %eax
	popq	%r8
	movq	24(%rsp), %r11
	je	.L798
.L983:
	movl	%eax, 12(%rsp)
	jmp	.L784
	.p2align 4,,10
	.p2align 3
.L1177:
	cmpb	$0, 4(%rbx)
	je	.L900
	testl	%edx, %edx
	jne	.L976
	cmpq	$1, %r13
	jne	.L796
	movzbl	1(%r14), %eax
	jmp	.L909
.L977:
	movl	$0, 112(%rsp)
.L901:
	movl	168(%rax), %edx
	movq	176(%rax), %r12
	movq	184(%rax), %r15
	testl	%edx, %edx
	jle	.L904
	leal	-1(%rdx), %eax
	leaq	1(%r14), %rcx
	movb	%bpl, 160(%rsp)
	movq	%rbx, 136(%rsp)
	movq	%r13, %rbp
	movl	%r10d, 120(%rsp)
	leaq	8(%r12,%rax,8), %rax
	movq	%rcx, 96(%rsp)
	movb	%r9b, 128(%rsp)
	movq	%r14, 168(%rsp)
	movq	%r15, %rbx
	movb	%r8b, 144(%rsp)
	movq	%rax, %r13
	movq	%r11, 152(%rsp)
	jmp	.L907
	.p2align 4,,10
	.p2align 3
.L905:
	addq	$8, %r12
	cmpq	%r12, %r13
	je	.L1188
.L907:
	movl	(%r12), %esi
	testl	%esi, %esi
	je	.L905
	movslq	4(%r12), %rdx
	movq	%rdx, %rax
	movzbl	(%rbx,%rdx), %edx
	leal	1(%rax,%rdx), %r15d
	movslq	%r15d, %rax
	movzbl	(%rbx,%rax), %edx
	cmpq	%rbp, %rdx
	movq	%rdx, %r14
	jne	.L905
	leaq	1(%rbx,%rax), %rax
	movq	96(%rsp), %rdi
	movq	%rbp, %rdx
	movq	%rax, %rsi
	movq	%rax, 104(%rsp)
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	jne	.L905
	movl	112(%rsp), %eax
	movzbl	%r14b, %ecx
	movl	120(%rsp), %r10d
	movl	%r15d, %r14d
	movzbl	128(%rsp), %r9d
	movq	%rbx, %r15
	movzbl	144(%rsp), %r8d
	movq	136(%rsp), %rbx
	movq	%rbp, %r13
	testl	%eax, %eax
	movq	152(%rsp), %r11
	jne	.L908
	movq	104(%rsp), %rsi
	movq	24(%rsp), %rdi
	movq	%r13, %rdx
	movq	%r11, 136(%rsp)
	movb	%r8b, 128(%rsp)
	movb	%r9b, 120(%rsp)
	movl	%r10d, 112(%rsp)
	movb	%cl, 96(%rsp)
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	movzbl	96(%rsp), %ecx
	movl	112(%rsp), %r10d
	movzbl	120(%rsp), %r9d
	movzbl	128(%rsp), %r8d
	movq	136(%rsp), %r11
	je	.L1189
.L908:
	leal	4(%r14,%rcx), %eax
	movl	$1, %ebp
	leaq	4(%rbx), %r14
	movzbl	84(%rsp), %ebx
	andl	$-4, %eax
	cltq
	movzbl	(%r15,%rax), %eax
	jmp	.L903
.L1168:
	leal	-33(%rbx), %eax
	cmpb	$31, %al
	ja	.L823
	movl	$2147484673, %edx
	btq	%rax, %rdx
	jnc	.L823
	cmpb	$40, (%r15)
	jne	.L823
.L822:
	subq	$1, %r15
	cmpq	%r14, %r11
	jnb	.L796
	movq	(%rsp), %r12
	leaq	176(%rsp), %rbp
	movq	%r11, %rbx
.L828:
	subq	$8, %rsp
	movq	%rbp, %r9
	movzbl	%r13b, %ecx
	pushq	2312(%rsp)
	movl	96(%rsp), %r8d
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%r9
	popq	%r10
	je	.L839
	addq	$1, %rbx
	xorl	%r13d, %r13d
	cmpq	%rbx, %r14
	jne	.L828
	jmp	.L796
.L1188:
	movq	%rbp, %r13
	movzbl	128(%rsp), %r9d
	movzbl	144(%rsp), %r8d
	movzbl	160(%rsp), %ebp
	movl	120(%rsp), %r10d
	movq	168(%rsp), %r14
	movq	136(%rsp), %rbx
	movq	152(%rsp), %r11
.L904:
	cmpq	$1, %r13
	jne	.L796
	movl	112(%rsp), %ecx
	movzbl	1(%r14), %eax
	testl	%ecx, %ecx
	jne	.L909
	movq	24(%rsp), %rcx
	cmpb	%al, (%rcx)
	jne	.L909
.L979:
	movq	24(%rsp), %r11
	movq	88(%rsp), %r14
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1187:
	movzbl	2(%rdx), %eax
	jmp	.L939
.L865:
	movq	80(%rcx), %rsi
	movq	104(%rcx), %rdi
	movq	96(%rcx), %r15
	movslq	(%rsi,%rax,4), %rax
	movq	%rdi, 96(%rsp)
	testq	%rax, %rax
	movq	%rax, %rbp
	js	.L1190
.L867:
	testl	%ebp, %ebp
	je	.L866
	movq	24(%rsp), %rax
	movzbl	(%rax), %eax
	movslq	(%rsi,%rax,4), %rax
	testq	%rax, %rax
	movq	%rax, %r12
	js	.L1191
.L872:
	testl	%r12d, %r12d
	je	.L866
	movl	%ebp, %edx
	movl	%r12d, %eax
	sarl	$24, %edx
	sarl	$24, %eax
	cmpl	%eax, %edx
	jne	.L866
	movq	88(%rcx), %rdx
	andl	$16777215, %ebp
	andl	$16777215, %r12d
	movslq	%ebp, %rax
	movzbl	(%rdx,%rax), %ecx
	movslq	%r12d, %rax
	cmpb	(%rdx,%rax), %cl
	jne	.L866
	testl	%ecx, %ecx
	je	.L973
	leal	1(%rbp), %eax
	addl	$1, %r12d
	movslq	%r12d, %rsi
	cltq
	addq	%rdx, %rax
	addq	%rsi, %rdx
	movzbl	(%rax), %esi
	cmpb	%sil, (%rdx)
	jne	.L866
	leal	-1(%rcx), %edi
	movl	$1, %esi
	addq	$1, %rdi
	jmp	.L892
	.p2align 4,,10
	.p2align 3
.L1192:
	movzbl	(%rax,%rsi), %ebx
	addq	$1, %rsi
	cmpb	-1(%rdx,%rsi), %bl
	jne	.L866
.L892:
	cmpq	%rsi, %rdi
	jne	.L1192
	cmpl	%esi, %ecx
	jne	.L866
.L973:
	movq	24(%rsp), %r11
	movq	%r13, %r14
	jmp	.L862
	.p2align 4,,10
	.p2align 3
.L1166:
	movq	32(%rsp), %rax
	movl	$0, 12(%rsp)
	movq	%r14, (%rax)
	movq	%r11, 8(%rax)
	movb	%r13b, 16(%rax)
	jmp	.L784
.L981:
	movq	%r12, %r14
	movq	24(%rsp), %r11
	jmp	.L862
.L1167:
	movl	12(%rsp), %ebx
	movq	%r11, %r10
	movl	8(%rsp), %r11d
	testl	%ebx, %ebx
	je	.L784
	andl	$8, %r11d
	movl	$0, 12(%rsp)
	jne	.L784
	movq	(%rsp), %rdx
	movl	$47, %esi
	movq	%r10, %rdi
	subq	%r10, %rdx
	call	__GI_memchr
	testq	%rax, %rax
	setne	%al
	movzbl	%al, %eax
	movl	%eax, 12(%rsp)
	jmp	.L784
.L1179:
	movl	168(%rax), %edx
	movq	184(%rax), %rsi
	movq	176(%rax), %rcx
	testl	%edx, %edx
	movq	%rsi, 112(%rsp)
	jle	.L923
	leal	-1(%rdx), %eax
	leaq	2(%r14), %rdi
	movq	%r14, 128(%rsp)
	movl	%r13d, 96(%rsp)
	movb	%r8b, 104(%rsp)
	movl	%ebx, %r13d
	leaq	8(%rcx,%rax,8), %rax
	movq	%rdi, 120(%rsp)
	movl	%r10d, 84(%rsp)
	movb	%r9b, 88(%rsp)
	movq	%r12, %r14
	movq	%rbp, 136(%rsp)
	movq	%rsi, %rbx
	movq	%rax, %r8
	jmp	.L926
	.p2align 4,,10
	.p2align 3
.L924:
	addq	$8, %rcx
	cmpq	%rcx, %r8
	je	.L1193
.L926:
	movl	(%rcx), %r9d
	testl	%r9d, %r9d
	je	.L924
	movslq	4(%rcx), %rdx
	movq	%rdx, %rax
	movzbl	(%rbx,%rdx), %edx
	leal	1(%rax,%rdx), %r12d
	movslq	%r12d, %rax
	movzbl	(%rbx,%rax), %edx
	cmpq	%r15, %rdx
	movq	%rdx, %rbp
	jne	.L924
	movq	120(%rsp), %rdi
	leaq	1(%rbx,%rax), %rsi
	movq	%r15, %rdx
	movq	%r11, 160(%rsp)
	movq	%rcx, 152(%rsp)
	movq	%r8, 144(%rsp)
	call	__GI_memcmp@PLT
	testl	%eax, %eax
	movq	144(%rsp), %r8
	movq	152(%rsp), %rcx
	movq	160(%rsp), %r11
	jne	.L924
	movl	%r12d, %ecx
	movzbl	%bpl, %eax
	movl	%r13d, %ebx
	leal	4(%rcx,%rax), %eax
	movl	96(%rsp), %r13d
	movq	112(%rsp), %rcx
	movl	84(%rsp), %r10d
	movzbl	88(%rsp), %r9d
	movq	%r14, %r12
	andl	$-4, %eax
	movzbl	104(%rsp), %r8d
	cltq
	cmpl	%r13d, %ebx
	movl	(%rcx,%rax), %eax
	ja	.L922
	movzbl	%al, %eax
	jmp	.L930
.L1193:
	movzbl	88(%rsp), %r9d
	movzbl	104(%rsp), %r8d
	movl	%r13d, %ebx
	movl	84(%rsp), %r10d
	movq	128(%rsp), %r14
	movq	136(%rsp), %rbp
	movl	96(%rsp), %r13d
.L923:
	cmpq	$1, %r15
	jne	.L796
	cmpl	%r13d, %ebx
	movzbl	2(%r14), %ecx
	leaq	4(%rbp), %r12
	ja	.L922
	jmp	.L927
.L976:
	movl	$1, 112(%rsp)
	jmp	.L901
.L1190:
	movq	%r15, %rdx
	movl	$4, %ebx
	subq	%rax, %rdx
.L868:
	movl	(%rdx), %ebp
	leaq	5(%rdx), %rdi
	movzbl	4(%rdx), %eax
	testl	%ebp, %ebp
	js	.L869
.L1194:
	testq	%rax, %rax
	je	.L867
	leaq	(%rdi,%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	je	.L868
	movq	%rbx, %rdi
	subq	%rax, %rdi
	addq	%rdi, %rdx
	movl	(%rdx), %ebp
	leaq	5(%rdx), %rdi
	movzbl	4(%rdx), %eax
	testl	%ebp, %ebp
	jns	.L1194
.L869:
	testq	%rax, %rax
	jne	.L1195
	movq	96(%rsp), %rdi
	movslq	%ebp, %rax
	negq	%rax
	movl	(%rdi,%rax,4), %ebp
	jmp	.L867
.L1181:
	movq	%r11, %r10
	cmpq	(%rsp), %r10
	movq	%r15, %r12
	movl	8(%rsp), %r11d
	jb	.L1152
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1196:
	addq	$1, %r10
	cmpq	%r10, (%rsp)
	je	.L796
.L1152:
	cmpb	$47, (%r10)
	jne	.L1196
	subq	$8, %rsp
	movl	%r11d, %ecx
	leaq	1(%r10), %rsi
	pushq	2312(%rsp)
	movq	16(%rsp), %rdx
	shrl	$2, %ecx
	andl	$1, %ecx
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movl	%r11d, %r8d
	call	internal_fnmatch
	testl	%eax, %eax
	popq	%rsi
	popq	%rdi
	je	.L826
	jmp	.L796
	.p2align 4,,10
	.p2align 3
.L1191:
	movq	24(%rsp), %rsi
	movq	%r15, %rdi
	movl	%r10d, 84(%rsp)
	subq	%rax, %rdi
	movb	%r9b, 88(%rsp)
	leaq	1(%rsi), %rbx
.L873:
	movzbl	4(%rdi), %eax
	movq	72(%rsp), %r9
	leaq	5(%rdi), %rdx
	movslq	(%rdi), %r12
	cmpq	%rax, %r9
	cmova	%rax, %r9
	testl	%r12d, %r12d
	js	.L1197
	testq	%r9, %r9
	je	.L877
	movzbl	5(%rdi), %edi
	cmpb	%dil, 1(%rsi)
	jne	.L966
	xorl	%edi, %edi
	jmp	.L878
.L879:
	movzbl	1(%rsi,%rdi), %r10d
	cmpb	%r10b, (%rdx,%rdi)
	jne	.L967
.L878:
	addq	$1, %rdi
	cmpq	%r9, %rdi
	jne	.L879
.L877:
	cmpq	%rax, %r9
	je	.L1142
	leaq	(%rdx,%rax), %rdi
	addq	$1, %rax
	andl	$3, %eax
	je	.L873
	movl	$4, %edx
	subq	%rax, %rdx
	addq	%rdx, %rdi
	jmp	.L873
.L1197:
	testq	%r9, %r9
	je	.L875
	movzbl	5(%rdi), %r10d
	cmpb	%r10b, 1(%rsi)
	jne	.L965
	xorl	%r10d, %r10d
	jmp	.L876
.L881:
	movzbl	1(%rsi,%r10), %r15d
	cmpb	%r15b, (%rdx,%r10)
	jne	.L968
.L876:
	addq	$1, %r10
	cmpq	%r9, %r10
	jne	.L881
.L875:
	cmpq	%rax, %r9
	je	.L969
	cmpq	72(%rsp), %r9
	je	.L883
	movzbl	(%rbx,%r9), %r10d
	cmpb	%r10b, (%rdx,%r9)
	ja	.L883
	testq	%rax, %rax
	movzbl	1(%rsi), %r15d
	je	.L1148
	leaq	(%rdx,%rax), %r10
	xorl	%r9d, %r9d
	cmpb	(%r10), %r15b
	jne	.L885
	movq	%rbx, 104(%rsp)
.L886:
	addq	$1, %r9
	cmpq	%r9, %rax
	je	.L1148
	movzbl	1(%rsi,%r9), %ebx
	cmpb	%bl, (%r10,%r9)
	je	.L886
	movq	104(%rsp), %rbx
.L885:
	leaq	(%rdx,%r9), %r10
	movzbl	(%rbx,%r9), %r9d
	cmpb	%r9b, (%r10,%rax)
	jnb	.L1148
	addq	%rax, %rax
.L1153:
	leaq	4(%rax), %rdi
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdi
	addq	%rdx, %rdi
	jmp	.L873
.L967:
	movq	%rdi, %r9
	jmp	.L877
.L883:
	addq	%rax, %rax
	jmp	.L1153
.L968:
	movq	%r10, %r9
	jmp	.L875
.L1148:
	xorl	%esi, %esi
	cmpb	%r15b, 5(%rdi)
	movl	84(%rsp), %r10d
	movzbl	88(%rsp), %r9d
	jne	.L889
.L888:
	movq	24(%rsp), %rdi
	addq	$1, %rsi
	movzbl	1(%rdi,%rsi), %edi
	cmpb	%dil, (%rdx,%rsi)
	je	.L888
.L889:
	xorl	%edi, %edi
.L890:
	movq	24(%rsp), %rbx
	movzbl	(%rdx,%rsi), %r15d
	salq	$8, %rdi
	movzbl	1(%rbx,%rsi), %ebx
	addq	$1, %rsi
	subl	%r15d, %ebx
	movslq	%ebx, %rbx
	addq	%rbx, %rdi
	cmpq	%rax, %rsi
	jb	.L890
.L882:
	movq	96(%rsp), %rsi
	movq	%rdi, %rax
	subq	%r12, %rax
	movl	(%rsi,%rax,4), %r12d
	jmp	.L872
.L860:
	movq	24(%rsp), %r11
	leaq	3(%rbp), %r14
	jmp	.L862
.L1195:
	addq	%rax, %rax
	leaq	4(%rax), %rdx
	addq	$1, %rax
	andl	$3, %eax
	subq	%rax, %rdx
	addq	%rdi, %rdx
	jmp	.L868
.L966:
	xorl	%r9d, %r9d
	jmp	.L877
.L965:
	xorl	%r9d, %r9d
	jmp	.L875
.L1178:
	movq	%rcx, %r11
	movq	%r13, %r14
	jmp	.L862
.L1189:
	movq	24(%rsp), %r11
	movq	88(%rsp), %r14
	leaq	-1(%r11,%r13), %r11
	jmp	.L862
.L969:
	movl	84(%rsp), %r10d
	movzbl	88(%rsp), %r9d
	xorl	%edi, %edi
	jmp	.L882
.L1142:
	movl	84(%rsp), %r10d
	movzbl	88(%rsp), %r9d
	jmp	.L872
	.size	internal_fnmatch, .-internal_fnmatch
	.section	.rodata.str1.1
.LC5:
	.string	"fnmatch.c"
.LC6:
	.string	"mbsinit (&ps)"
	.text
	.p2align 4,,15
	.globl	__fnmatch
	.type	__fnmatch, @function
__fnmatch:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edx, %r14d
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	movq	__libc_tsd_LOCALE@gottpoff(%rip), %rax
	movq	%rdi, -72(%rbp)
	movq	%rsi, -80(%rbp)
	movq	%fs:(%rax), %rax
	movq	(%rax), %rax
	cmpl	$1, 168(%rax)
	jne	.L1230
	movq	-80(%rbp), %r12
	movq	%r12, %rdi
	call	__GI_strlen
	subq	$8, %rsp
	movl	%r14d, %ecx
	leaq	(%r12,%rax), %rdx
	pushq	$0
	shrl	$2, %ecx
	xorl	%r9d, %r9d
	andl	$1, %ecx
	movl	%r14d, %r8d
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	internal_fnmatch
	popq	%rdx
	popq	%rcx
.L1198:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1230:
	movl	$1024, %esi
	movq	$0, -56(%rbp)
	movq	%rdi, -64(%rbp)
	call	__GI___strnlen
	cmpq	$1023, %rax
	leaq	-56(%rbp), %r13
	ja	.L1216
#APP
# 255 "fnmatch.c" 1
	mov %rsp, %rbx
# 0 "" 2
#NO_APP
	leaq	1(%rax), %rdx
	leaq	30(,%rdx,4), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
#APP
# 255 "fnmatch.c" 1
	sub %rsp , %rbx
# 0 "" 2
#NO_APP
	leaq	-64(%rbp), %rsi
	movq	%r13, %rcx
	movq	%r12, %rdi
	call	__mbsrtowcs
	cmpq	$-1, %rax
	je	.L1227
	cmpq	$0, -64(%rbp)
	je	.L1217
	movq	$0, 0(%r13)
.L1200:
	leaq	-72(%rbp), %r12
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%r13, %rcx
	movq	%r12, %rsi
	call	__mbsrtowcs
	cmpq	$-1, %rax
	je	.L1227
	movabsq	$4611686018427387902, %rdx
	cmpq	%rdx, %rax
	ja	.L1228
	leaq	1(%rax), %r15
	leaq	0(,%r15,4), %rdi
	call	malloc@PLT
	movl	-56(%rbp), %r10d
	movq	%rax, -88(%rbp)
	testl	%r10d, %r10d
	jne	.L1231
	movq	-88(%rbp), %rax
	testq	%rax, %rax
	je	.L1229
	movq	%r12, %rsi
	movq	%r13, %rcx
	movq	%r15, %rdx
	movq	%rax, %rdi
	movq	%rax, %r12
	call	__mbsrtowcs
.L1203:
	movl	-56(%rbp), %r9d
	testl	%r9d, %r9d
	jne	.L1232
	movq	-80(%rbp), %r15
	movl	$1024, %esi
	movq	%r15, %rdi
	call	__GI___strnlen
	cmpq	$1023, %rax
	movq	%r15, -64(%rbp)
	ja	.L1208
#APP
# 296 "fnmatch.c" 1
	mov %rsp, %rcx
# 0 "" 2
#NO_APP
	leaq	1(%rax), %rdx
	leaq	30(,%rdx,4), %rsi
	andq	$-16, %rsi
	subq	%rsi, %rsp
	leaq	15(%rsp), %r15
	andq	$-16, %r15
#APP
# 296 "fnmatch.c" 1
	sub %rsp , %rcx
# 0 "" 2
#NO_APP
	leaq	-64(%rbp), %rsi
	addq	%rcx, %rbx
	movq	%r15, %rdi
	movq	%r13, %rcx
	call	__mbsrtowcs
	cmpq	$-1, %rax
	movq	%rax, %r9
	je	.L1211
	cmpq	$0, -64(%rbp)
	je	.L1219
	movq	$0, 0(%r13)
.L1208:
	leaq	-80(%rbp), %r15
	xorl	%edx, %edx
	xorl	%edi, %edi
	movq	%r13, %rcx
	movq	%r15, %rsi
	call	__mbsrtowcs
	cmpq	$-1, %rax
	movq	%rax, %r9
	je	.L1211
	movabsq	$4611686018427387902, %rax
	cmpq	%rax, %r9
	ja	.L1233
	leaq	1(%r9), %rdx
	movq	%r9, -104(%rbp)
	leaq	0(,%rdx,4), %rdi
	movq	%rdx, -96(%rbp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	-96(%rbp), %rdx
	movq	-104(%rbp), %r9
	je	.L1234
	movl	-56(%rbp), %r8d
	testl	%r8d, %r8d
	jne	.L1235
	movq	%r15, %rsi
	movq	%r13, %rcx
	movq	%rax, %rdi
	movq	%r9, -104(%rbp)
	movq	%rax, -96(%rbp)
	call	__mbsrtowcs
	movq	-96(%rbp), %r10
	movq	-104(%rbp), %r9
	movq	%r10, %r15
.L1210:
	subq	$8, %rsp
	movl	%r14d, %ecx
	leaq	(%r15,%r9,4), %rdx
	pushq	%rbx
	shrl	$2, %ecx
	movq	%r15, %rsi
	andl	$1, %ecx
	xorl	%r9d, %r9d
	movl	%r14d, %r8d
	movq	%r12, %rdi
	movq	%r10, -104(%rbp)
	call	internal_fnwmatch
	movq	-104(%rbp), %r10
	movl	%eax, -96(%rbp)
	movq	%r10, %rdi
	call	free@PLT
	movq	-88(%rbp), %rdi
	call	free@PLT
	popq	%rsi
	movl	-96(%rbp), %eax
	popq	%rdi
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1217:
	movq	$0, -88(%rbp)
	jmp	.L1203
.L1233:
	movq	-88(%rbp), %rdi
	call	free@PLT
.L1228:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L1229:
	movl	$-2, %eax
	jmp	.L1198
	.p2align 4,,10
	.p2align 3
.L1219:
	xorl	%r10d, %r10d
	jmp	.L1210
.L1211:
	movq	-88(%rbp), %rdi
	call	free@PLT
.L1227:
	movl	$-1, %eax
	jmp	.L1198
.L1216:
	xorl	%ebx, %ebx
	jmp	.L1200
.L1234:
	movq	-88(%rbp), %rdi
	call	free@PLT
	movl	$-2, %eax
	jmp	.L1198
.L1235:
	leaq	__PRETTY_FUNCTION__.9564(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$337, %edx
	call	__GI___assert_fail
.L1232:
	leaq	__PRETTY_FUNCTION__.9564(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$291, %edx
	call	__GI___assert_fail
.L1231:
	leaq	__PRETTY_FUNCTION__.9564(%rip), %rcx
	leaq	.LC5(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$285, %edx
	call	__GI___assert_fail
	.size	__fnmatch, .-__fnmatch
	.globl	__GI_fnmatch
	.set	__GI_fnmatch,__fnmatch
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.9247, @object
	.size	__PRETTY_FUNCTION__.9247, 10
__PRETTY_FUNCTION__.9247:
	.string	"ext_match"
	.align 8
	.type	__PRETTY_FUNCTION__.9516, @object
	.size	__PRETTY_FUNCTION__.9516, 11
__PRETTY_FUNCTION__.9516:
	.string	"ext_wmatch"
	.align 8
	.type	__PRETTY_FUNCTION__.9564, @object
	.size	__PRETTY_FUNCTION__.9564, 10
__PRETTY_FUNCTION__.9564:
	.string	"__fnmatch"
	.local	posixly_correct
	.comm	posixly_correct,4,4
	.hidden	__mbsrtowcs
	.hidden	__collidx_table_lookup
	.hidden	__collseq_table_lookup
	.hidden	__wmempcpy
