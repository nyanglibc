	.text
	.p2align 4,,15
	.globl	__sched_setaffinity_new
	.hidden	__sched_setaffinity_new
	.type	__sched_setaffinity_new, @function
__sched_setaffinity_new:
	movl	$203, %eax
#APP
# 33 "../sysdeps/unix/sysv/linux/sched_setaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__sched_setaffinity_new, .-__sched_setaffinity_new
	.weak	sched_setaffinity
	.set	sched_setaffinity,__sched_setaffinity_new
