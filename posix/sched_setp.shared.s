.text
.globl __sched_setparam
.type __sched_setparam,@function
.align 1<<4
__sched_setparam:
	movl $142, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_setparam,.-__sched_setparam
.globl __GI___sched_setparam
.set __GI___sched_setparam,__sched_setparam
.weak sched_setparam
sched_setparam = __sched_setparam
.globl __GI_sched_setparam
.set __GI_sched_setparam,sched_setparam
