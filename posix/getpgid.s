.text
.globl __getpgid
.type __getpgid,@function
.align 1<<4
__getpgid:
	movl $121, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __getpgid,.-__getpgid
.weak getpgid
getpgid = __getpgid
