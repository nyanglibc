.text
.globl getresgid
.type getresgid,@function
.align 1<<4
getresgid:
	movl $120, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size getresgid,.-getresgid
.globl __GI_getresgid
.set __GI_getresgid,getresgid
