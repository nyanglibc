	.text
	.p2align 4,,15
	.globl	__posix_spawn_file_actions_adddup2
	.hidden	__posix_spawn_file_actions_adddup2
	.type	__posix_spawn_file_actions_adddup2, @function
__posix_spawn_file_actions_adddup2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	%esi, %ebp
	call	__spawn_valid_fd
	testb	%al, %al
	je	.L4
	movl	%r12d, %edi
	call	__spawn_valid_fd
	testb	%al, %al
	je	.L4
	movl	4(%rbx), %edx
	cmpl	(%rbx), %edx
	je	.L12
.L5:
	movslq	%edx, %rax
	addl	$1, %edx
	salq	$5, %rax
	addq	8(%rbx), %rax
	movl	$1, (%rax)
	movl	%ebp, 8(%rax)
	movl	%r12d, 12(%rax)
	movl	%edx, 4(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$9, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rdi
	call	__posix_spawn_file_actions_realloc
	movl	%eax, %edx
	movl	$12, %eax
	testl	%edx, %edx
	jne	.L1
	movl	4(%rbx), %edx
	jmp	.L5
	.size	__posix_spawn_file_actions_adddup2, .-__posix_spawn_file_actions_adddup2
	.weak	posix_spawn_file_actions_adddup2
	.set	posix_spawn_file_actions_adddup2,__posix_spawn_file_actions_adddup2
	.hidden	__posix_spawn_file_actions_realloc
	.hidden	__spawn_valid_fd
