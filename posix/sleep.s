	.text
	.p2align 4,,15
	.globl	__sleep
	.hidden	__sleep
	.type	__sleep, @function
__sleep:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %eax
	subq	$24, %rsp
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%rsp, %rdi
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	movq	%rdi, %rsi
	movl	%fs:(%rbx), %ebp
	call	__nanosleep
	testl	%eax, %eax
	js	.L6
	movl	%ebp, %fs:(%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	__sleep, .-__sleep
	.weak	sleep
	.set	sleep,__sleep
	.hidden	__nanosleep
