	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __posix_spawn,posix_spawn@@GLIBC_2.15
	.symver __posix_spawn_compat,posix_spawn@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__GI___posix_spawn
	.hidden	__GI___posix_spawn
	.type	__GI___posix_spawn, @function
__GI___posix_spawn:
	subq	$16, %rsp
	pushq	$0
	call	__spawni
	addq	$24, %rsp
	ret
	.size	__GI___posix_spawn, .-__GI___posix_spawn
	.globl	__posix_spawn
	.set	__posix_spawn,__GI___posix_spawn
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__posix_spawn_compat
	.type	__posix_spawn_compat, @function
__posix_spawn_compat:
	subq	$16, %rsp
	pushq	$2
	call	__spawni
	addq	$24, %rsp
	ret
	.size	__posix_spawn_compat, .-__posix_spawn_compat
	.hidden	__spawni
