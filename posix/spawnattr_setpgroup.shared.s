	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	posix_spawnattr_setpgroup
	.type	posix_spawnattr_setpgroup, @function
posix_spawnattr_setpgroup:
	movl	%esi, 4(%rdi)
	xorl	%eax, %eax
	ret
	.size	posix_spawnattr_setpgroup, .-posix_spawnattr_setpgroup
