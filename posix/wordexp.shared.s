	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	strlist_add__, @function
strlist_add__:
.LFB105:
	pushq	%r12
	pushq	%rbp
	leaq	24(%rdi), %rbp
	pushq	%rbx
	movq	%rsi, %r12
	movl	$8, %edx
	movq	%rbp, %rsi
	movq	%rdi, %rbx
	call	__GI___libc_dynarray_emplace_enlarge
	testb	%al, %al
	je	.L7
	movq	(%rbx), %rax
	movq	16(%rbx), %rdx
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movq	%r12, (%rdx,%rax,8)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movq	16(%rbx), %rdi
	cmpq	%rdi, %rbp
	je	.L3
	call	free@PLT
.L3:
	movq	%rbp, 16(%rbx)
	movq	$0, (%rbx)
	movq	$-1, 8(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.LFE105:
	.size	strlist_add__, .-strlist_add__
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	""
	.text
	.p2align 4,,15
	.type	w_addword, @function
w_addword:
.LFB82:
	testq	%rsi, %rsi
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	je	.L18
	movq	16(%rdi), %rax
	addq	(%rdi), %rax
	movq	%rsi, %rbp
	movq	8(%rdi), %rdi
	leaq	16(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L17
.L12:
	movq	(%rbx), %rcx
	movq	16(%rbx), %rdx
	movq	%rax, 8(%rbx)
	leaq	1(%rcx), %rsi
	addq	%rdx, %rcx
	addq	%rsi, %rdx
	movq	%rsi, (%rbx)
	movq	%rbp, (%rax,%rcx,8)
	movq	$0, (%rax,%rdx,8)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	.LC0(%rip), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %r12
	movq	%rax, %rbp
	je	.L17
	movq	16(%rbx), %rax
	addq	(%rbx), %rax
	movq	8(%rbx), %rdi
	leaq	16(,%rax,8), %rsi
	call	realloc@PLT
	testq	%rax, %rax
	jne	.L12
	movq	%r12, %rdi
	call	free@PLT
.L17:
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	ret
.LFE82:
	.size	w_addword, .-w_addword
	.section	.rodata.str1.1
.LC1:
	.string	"wordexp.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC2:
	.string	"buffer == NULL || *maxlen != 0"
	.text
	.p2align 4,,15
	.type	w_addchar, @function
w_addchar:
.LFB79:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	(%rsi), %rsi
	cmpq	(%rdx), %rsi
	je	.L28
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.L19
.L23:
	movb	%cl, (%rbx,%rsi)
	movq	0(%rbp), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 0(%rbp)
	movb	$0, 1(%rbx,%rax)
	movq	%rbx, %rax
.L19:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	testq	%rsi, %rsi
	jne	.L21
	testq	%rdi, %rdi
	jne	.L29
.L21:
	leaq	100(%rsi), %rax
	movq	%rbx, %rdi
	addq	$101, %rsi
	movl	%ecx, 8(%rsp)
	movq	%rax, (%rdx)
	call	realloc@PLT
	testq	%rax, %rax
	je	.L22
	movq	0(%rbp), %rsi
	movq	%rax, %rbx
	movl	8(%rsp), %ecx
	jmp	.L23
.L29:
	leaq	__PRETTY_FUNCTION__.11053(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$84, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L22:
	movq	%rbx, %rdi
	movq	%rax, 8(%rsp)
	call	free@PLT
	movq	8(%rsp), %rax
	jmp	.L19
.LFE79:
	.size	w_addchar, .-w_addchar
	.p2align 4,,15
	.type	parse_backslash, @function
parse_backslash:
.LFB83:
	movq	(%r8), %rax
	movsbl	1(%rcx,%rax), %ecx
	leaq	1(%rax), %r9
	movl	$5, %eax
	testb	%cl, %cl
	je	.L40
	cmpb	$10, %cl
	je	.L42
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%r8, %rbx
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	w_addchar
	movq	%rax, %rdx
	movq	%rax, 0(%rbp)
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L30
	addq	$1, (%rbx)
	xorl	%eax, %eax
.L30:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%r9, (%r8)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	rep ret
.LFE83:
	.size	parse_backslash, .-parse_backslash
	.p2align 4,,15
	.type	parse_qtd_backslash, @function
parse_qtd_backslash:
.LFB84:
	pushq	%r14
	movq	%rdx, %r14
	movq	(%r8), %rdx
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	movq	%rcx, %rbp
	pushq	%rbx
	movsbl	1(%rcx,%rdx), %ecx
	movq	%rdi, %r12
	movq	%r8, %rbx
	cmpb	$34, %cl
	je	.L45
	jg	.L46
	testb	%cl, %cl
	movl	$5, %eax
	je	.L43
	cmpb	$10, %cl
	jne	.L44
	leaq	1(%rdx), %rsi
	xorl	%eax, %eax
	movq	%rsi, (%r8)
.L43:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	cmpb	$92, %cl
	je	.L45
	cmpb	$96, %cl
	je	.L45
	cmpb	$36, %cl
	je	.L45
.L44:
	movsbl	0(%rbp,%rdx), %ecx
	movq	(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L50
	movq	(%rbx), %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	movsbl	1(%rbp,%rdx), %ecx
	movq	%r14, %rdx
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L50
.L72:
	addq	$1, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L45:
	movq	(%r12), %rdi
	movq	%r14, %rdx
	movq	%r13, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r12)
	jne	.L72
.L50:
	popq	%rbx
	movl	$1, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.LFE84:
	.size	parse_qtd_backslash, .-parse_qtd_backslash
	.p2align 4,,15
	.type	w_addmem, @function
w_addmem:
.LFB80:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rcx, %r14
	pushq	%rbx
	movq	(%rsi), %rdi
	movq	%rsi, %rbp
	movq	(%rdx), %rax
	movq	%r8, %rbx
	leaq	(%rdi,%r8), %rcx
	cmpq	%rax, %rcx
	jbe	.L74
	testq	%rax, %rax
	jne	.L75
	testq	%r13, %r13
	jne	.L82
.L75:
	leaq	(%rbx,%rbx), %rsi
	movl	$100, %ecx
	movq	%r13, %rdi
	cmpq	$100, %rsi
	cmovb	%rcx, %rsi
	addq	%rax, %rsi
	movq	%rsi, (%rdx)
	addq	$1, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L76
	movq	0(%rbp), %rdi
.L77:
	addq	%r12, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	call	__GI_mempcpy@PLT
	movb	$0, (%rax)
	addq	%rbx, 0(%rbp)
.L73:
	popq	%rbx
	movq	%r12, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L74:
	xorl	%r12d, %r12d
	testq	%r13, %r13
	je	.L73
	movq	%r13, %r12
	jmp	.L77
.L82:
	leaq	__PRETTY_FUNCTION__.11062(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$110, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L76:
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L73
.LFE80:
	.size	w_addmem, .-w_addmem
	.p2align 4,,15
	.type	eval_expr_multdiv, @function
eval_expr_multdiv:
.LFB90:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r13
	pushq	%rbx
	subq	$16, %rsp
	call	eval_expr_val
	testl	%eax, %eax
	jne	.L97
	leaq	8(%rsp), %rbx
	movl	%eax, %ebp
	movabsq	$-9223372036854775808, %r12
	.p2align 4,,10
	.p2align 3
.L85:
	movq	0(%r13), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L83
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	%dl, %rsi
	movq	%fs:(%rax), %rdi
	leaq	1(%rcx), %rax
	testb	$32, 1(%rdi,%rsi,2)
	jne	.L88
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L110:
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L83
	movsbq	%dl, %rsi
	addq	$1, %rax
	testb	$32, 1(%rdi,%rsi,2)
	je	.L96
.L88:
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, 0(%r13)
	jne	.L110
	movzbl	0, %eax
	ud2
	.p2align 4,,10
	.p2align 3
.L96:
	cmpb	$42, %dl
	je	.L111
	cmpb	$47, %dl
	jne	.L83
	addq	$1, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, 0(%r13)
	call	eval_expr_val
	testl	%eax, %eax
	jne	.L97
	movq	8(%rsp), %rcx
	testq	%rcx, %rcx
	je	.L97
	cmpq	$-1, %rcx
	movq	(%r14), %rax
	je	.L112
.L94:
	cqto
	idivq	%rcx
	movq	%rax, (%r14)
	jmp	.L85
	.p2align 4,,10
	.p2align 3
.L112:
	cmpq	%r12, %rax
	jne	.L94
	.p2align 4,,10
	.p2align 3
.L97:
	movl	$5, %ebp
.L83:
	addq	$16, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L111:
	addq	$1, %rcx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	movq	%rcx, 0(%r13)
	call	eval_expr_val
	testl	%eax, %eax
	jne	.L97
	movq	(%r14), %rax
	imulq	8(%rsp), %rax
	movq	%rax, (%r14)
	jmp	.L85
.LFE90:
	.size	eval_expr_multdiv, .-eval_expr_multdiv
	.p2align 4,,15
	.type	eval_expr, @function
eval_expr:
.LFB91:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$40, %rsp
	leaq	8(%rsp), %rbp
	movq	%rdi, 8(%rsp)
	movq	%rbp, %rdi
	call	eval_expr_multdiv
	testl	%eax, %eax
	jne	.L114
	leaq	24(%rsp), %r12
	movl	%eax, %r13d
.L115:
	movq	8(%rsp), %rcx
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	je	.L113
.L133:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movsbq	%dl, %rsi
	movq	%fs:(%rax), %rdi
	leaq	1(%rcx), %rax
	testb	$32, 1(%rdi,%rsi,2)
	jne	.L119
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L131:
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L113
	movsbq	%dl, %rsi
	addq	$1, %rax
	testb	$32, 1(%rdi,%rsi,2)
	je	.L124
.L119:
	testq	%rax, %rax
	movq	%rax, %rcx
	movq	%rax, 8(%rsp)
	jne	.L131
	movzbl	0, %eax
	ud2
	.p2align 4,,10
	.p2align 3
.L124:
	cmpb	$43, %dl
	je	.L132
	cmpb	$45, %dl
	jne	.L113
	addq	$1, %rcx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movq	%rcx, 8(%rsp)
	call	eval_expr_multdiv
	testl	%eax, %eax
	jne	.L114
	movq	8(%rsp), %rcx
	movq	24(%rsp), %rax
	subq	%rax, (%rbx)
	movzbl	(%rcx), %edx
	testb	%dl, %dl
	jne	.L133
.L113:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L132:
	addq	$1, %rcx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movq	%rcx, 8(%rsp)
	call	eval_expr_multdiv
	testl	%eax, %eax
	jne	.L114
	movq	24(%rsp), %rax
	addq	%rax, (%rbx)
	jmp	.L115
	.p2align 4,,10
	.p2align 3
.L114:
	addq	$40, %rsp
	movl	$5, %r13d
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.LFE91:
	.size	eval_expr, .-eval_expr
	.p2align 4,,15
	.type	eval_expr_val, @function
eval_expr_val:
.LFB89:
	movq	(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L167
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movzbl	(%rcx), %r8d
	movq	%rdi, %rbp
	testb	%r8b, %r8b
	je	.L146
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %rax
	movq	%fs:(%rax), %rdi
	movsbq	%r8b, %rax
	testb	$32, 1(%rdi,%rax,2)
	je	.L147
	movq	%rcx, %rbx
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L171:
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L139
	movsbq	%al, %rdx
	testb	$32, 1(%rdi,%rdx,2)
	je	.L137
.L138:
	addq	$1, %rbx
	jne	.L171
	movzbl	0, %eax
	ud2
.L147:
	movl	%r8d, %eax
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L137:
	cmpb	$40, %al
	jne	.L139
	cmpb	$41, %r8b
	je	.L140
	leaq	1(%rcx), %rax
	jmp	.L141
	.p2align 4,,10
	.p2align 3
.L172:
	cmpb	$41, %dl
	je	.L148
.L141:
	movq	%rax, 0(%rbp)
	movzbl	(%rax), %edx
	movq	%rax, %rcx
	addq	$1, %rax
	testb	%dl, %dl
	jne	.L172
.L148:
	testb	%dl, %dl
	jne	.L140
.L144:
	popq	%rbx
	movl	$5, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	leaq	1(%rcx), %rax
	leaq	1(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, 0(%rbp)
	movb	$0, (%rcx)
	call	eval_expr
	testl	%eax, %eax
	jne	.L144
	xorl	%eax, %eax
.L173:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L146:
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L139:
	xorl	%edx, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	__GI_strtol
	cmpq	%rbx, 0(%rbp)
	movq	%rax, (%r12)
	je	.L144
	xorl	%eax, %eax
	jmp	.L173
.L167:
	movzbl	0, %eax
	ud2
.LFE89:
	.size	eval_expr_val, .-eval_expr_val
	.section	.rodata.str1.1
.LC3:
	.string	"str != NULL"
	.text
	.p2align 4,,15
	.type	w_addstr, @function
w_addstr:
.LFB81:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	testq	%rcx, %rcx
	je	.L177
	movq	%rdi, %rbp
	movq	%rcx, %rdi
	movq	%rsi, %r12
	movq	%rdx, %r13
	movq	%rcx, %rbx
	call	__GI_strlen
	addq	$8, %rsp
	movq	%rbx, %rcx
	movq	%r13, %rdx
	popq	%rbx
	movq	%r12, %rsi
	movq	%rbp, %rdi
	movq	%rax, %r8
	popq	%rbp
	popq	%r12
	popq	%r13
	jmp	w_addmem
.L177:
	leaq	__PRETTY_FUNCTION__.11070(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$135, %edx
	call	__GI___assert_fail
.LFE81:
	.size	w_addstr, .-w_addstr
	.section	.rodata.str1.1
.LC4:
	.string	"HOME"
	.text
	.p2align 4,,15
	.type	parse_tilde, @function
parse_tilde:
.LFB85:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbx
	movq	%rdi, %r12
	subq	$1144, %rsp
	movq	(%rsi), %rax
	movq	%rdx, -1184(%rbp)
	movq	%r8, -1176(%rbp)
	testq	%rax, %rax
	je	.L179
	movq	(%rdi), %rbx
	movzbl	-1(%rbx,%rax), %eax
	cmpb	$61, %al
	je	.L246
	cmpb	$58, %al
	movq	%r9, -1160(%rbp)
	movq	%rcx, -1168(%rbp)
	jne	.L181
	movl	$61, %esi
	movq	%rbx, %rdi
	call	__GI_strchr
	movq	-1160(%rbp), %r9
	testq	%r9, %r9
	jne	.L181
	testq	%rax, %rax
	movq	-1168(%rbp), %rcx
	je	.L181
	.p2align 4,,10
	.p2align 3
.L179:
	movq	-1176(%rbp), %rax
	movq	(%rax), %r8
	leaq	1(%r8), %rdi
	leaq	(%rcx,%rdi), %rbx
	movzbl	(%rbx), %eax
	cmpb	$58, %al
	ja	.L184
	movabsq	$288371117935034881, %rdx
	movq	%rdi, %r15
	btq	%rax, %rdx
	jc	.L186
.L185:
	movzbl	1(%rcx,%r15), %eax
	movabsq	$288371117935034881, %rsi
	leaq	1(%r15), %rdx
	cmpb	$58, %al
	jbe	.L247
	.p2align 4,,10
	.p2align 3
.L190:
	cmpb	$92, %al
	je	.L187
	.p2align 4,,10
	.p2align 3
.L188:
	movq	%rdx, %r15
	movzbl	1(%rcx,%r15), %eax
	leaq	1(%r15), %rdx
	cmpb	$58, %al
	ja	.L190
.L247:
	btq	%rax, %rsi
	jnc	.L188
	cmpq	%rdx, %rdi
	je	.L186
	movq	%r15, %rsi
	movq	%rbx, %rdi
	leaq	-1144(%rbp), %r14
	subq	%r8, %rsi
	call	__GI___strnlen
	leaq	31(%rax), %rdx
	movq	%rbx, %rsi
	leaq	-1088(%rbp), %rbx
	andq	$-16, %rdx
	subq	%rdx, %rsp
	movq	%rax, %rdx
	leaq	15(%rsp), %rdi
	andq	$-16, %rdi
	movb	$0, (%rdi,%rax)
	call	__GI_memcpy@PLT
	leaq	16(%rbx), %rdx
	movq	%rax, -1168(%rbp)
	leaq	-1136(%rbp), %rax
	movq	$1024, -1080(%rbp)
	movl	$1024, %ecx
	movq	%rdx, -1088(%rbp)
	movq	%rax, -1160(%rbp)
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L204:
	movq	-1080(%rbp), %rcx
	movq	-1088(%rbp), %rdx
.L203:
	movq	-1160(%rbp), %rsi
	movq	-1168(%rbp), %rdi
	movq	%r14, %r8
	call	__getpwnam_r
	testl	%eax, %eax
	je	.L205
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L248
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L204
.L243:
	movl	$1, %eax
.L178:
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L246:
	testq	%r9, %r9
	je	.L179
.L181:
	movq	-1184(%rbp), %rdx
	movl	$126, %ecx
	movq	%r13, %rsi
	movq	%rbx, %rdi
.L242:
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r12)
	sete	%al
	leaq	-40(%rbp), %rsp
	movzbl	%al, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L184:
	cmpb	$92, %al
	movq	%rdi, %r15
	jne	.L185
	.p2align 4,,10
	.p2align 3
.L187:
	movl	$126, %ecx
	movq	-1184(%rbp), %rdx
	movq	%r13, %rsi
	movq	(%r12), %rdi
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L186:
	leaq	.LC4(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	je	.L192
	movq	-1184(%rbp), %rdx
	movq	(%r12), %rdi
	movq	%rax, %rcx
	movq	%r13, %rsi
	call	w_addstr
	movq	%rax, %rdx
	movq	%rax, (%r12)
	movl	$1, %eax
	testq	%rdx, %rdx
	je	.L178
.L193:
	xorl	%eax, %eax
	testq	%rdx, %rdx
	sete	%al
	leaq	-40(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L205:
	cmpq	$0, -1144(%rbp)
	movq	(%r12), %rdi
	je	.L208
	movq	-1104(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L208
	movq	-1184(%rbp), %rdx
	movq	%r13, %rsi
	call	w_addstr
	movq	%rax, (%r12)
	jmp	.L209
	.p2align 4,,10
	.p2align 3
.L248:
	movq	(%r12), %rdi
.L208:
	movq	-1184(%rbp), %r14
	movl	$126, %ecx
	movq	%r13, %rsi
	movq	%r14, %rdx
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L209
	movq	-1168(%rbp), %rcx
	movq	%r14, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	w_addstr
	movq	%rax, (%r12)
.L209:
	movq	-1088(%rbp), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L210
	call	free@PLT
.L210:
	movq	-1176(%rbp), %rax
	movq	%r15, (%rax)
.L241:
	movq	(%r12), %rdx
	jmp	.L193
	.p2align 4,,10
	.p2align 3
.L192:
	leaq	-1088(%rbp), %rbx
	leaq	-1144(%rbp), %r14
	call	__getuid
	leaq	16(%rbx), %rdx
	movl	%eax, %r15d
	leaq	-1136(%rbp), %rax
	movq	$1024, -1080(%rbp)
	movl	$1024, %ecx
	movq	%rdx, -1088(%rbp)
	movq	%rax, -1160(%rbp)
	jmp	.L194
	.p2align 4,,10
	.p2align 3
.L250:
	movq	-1080(%rbp), %rcx
	movq	-1088(%rbp), %rdx
.L194:
	movq	-1160(%rbp), %rsi
	movq	%r14, %r8
	movl	%r15d, %edi
	call	__getpwuid_r
	testl	%eax, %eax
	je	.L196
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$34, %fs:(%rax)
	jne	.L249
	movq	%rbx, %rdi
	call	__GI___libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L250
	jmp	.L243
.L196:
	cmpq	$0, -1144(%rbp)
	movq	(%r12), %rdi
	je	.L199
	movq	-1104(%rbp), %rcx
	testq	%rcx, %rcx
	je	.L199
	movq	-1184(%rbp), %rdx
	movq	%r13, %rsi
	call	w_addstr
	testq	%rax, %rax
	movq	%rax, (%r12)
	je	.L245
.L200:
	movq	-1088(%rbp), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L241
	call	free@PLT
	jmp	.L241
.L249:
	movq	(%r12), %rdi
.L199:
	movq	-1184(%rbp), %rdx
	movl	$126, %ecx
	movq	%r13, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r12)
	jne	.L200
.L245:
	movq	-1088(%rbp), %rdi
	addq	$16, %rbx
	cmpq	%rbx, %rdi
	je	.L243
	call	free@PLT
	jmp	.L243
.LFE85:
	.size	parse_tilde, .-parse_tilde
	.section	.rodata.str1.1
.LC5:
	.string	"-c"
.LC6:
	.string	"/bin/sh"
.LC7:
	.string	"IFS"
.LC8:
	.string	"/dev/null"
.LC9:
	.string	"IFS="
.LC10:
	.string	"-nc"
	.text
	.p2align 4,,15
	.type	exec_comm, @function
exec_comm:
.LFB115:
	pushq	%r15
	pushq	%r14
	movl	%r8d, %r15d
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$1416, %rsp
	andl	$4, %r15d
	movl	$0, 96(%rsp)
	jne	.L314
	testq	%rdi, %rdi
	je	.L251
	cmpb	$0, (%rdi)
	jne	.L381
.L251:
	addq	$1416, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L314:
	movl	$4, %r15d
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L381:
	movq	%rdi, 56(%rsp)
	leaq	104(%rsp), %rdi
	movq	%rsi, %r14
	movl	$524288, %esi
	movq	%r9, 32(%rsp)
	movl	%r8d, %ebx
	movq	%rcx, 24(%rsp)
	movq	%rdx, 8(%rsp)
	call	__pipe2
	testl	%eax, %eax
	js	.L315
.L254:
	leaq	144(%rsp), %rcx
	leaq	.LC6(%rip), %rax
	shrl	$4, %ebx
	andl	$1, %ebx
	movl	$-1, 100(%rsp)
	movl	$2, %ebp
	movq	%rcx, 48(%rsp)
	leaq	352(%rsp), %rcx
	movq	%rax, 112(%rsp)
	leaq	.LC5(%rip), %rax
	xorl	%r12d, %r12d
	movl	%r15d, 76(%rsp)
	movq	%rcx, 80(%rsp)
	leaq	376(%rsp), %rcx
	movq	%rcx, 64(%rsp)
.L313:
	movq	%rax, 120(%rsp)
	movq	48(%rsp), %rdi
	movq	56(%rsp), %rax
	movq	$0, 136(%rsp)
	movq	%rax, 128(%rsp)
	call	__posix_spawn_file_actions_init
	movl	108(%rsp), %esi
	cmpl	$-1, %esi
	je	.L262
	cmpl	$1, %esi
	movl	$1, %edx
	je	.L258
	movq	48(%rsp), %rdi
	call	__posix_spawn_file_actions_adddup2
	testl	%eax, %eax
	je	.L382
.L260:
	movq	48(%rsp), %rdi
	call	__posix_spawn_file_actions_destroy
	movl	100(%rsp), %eax
	testl	%eax, %eax
	movl	%eax, 72(%rsp)
	js	.L383
	cmpl	$1, %ebp
	jne	.L274
	movl	76(%rsp), %r15d
	leaq	96(%rsp), %rbx
	movl	72(%rsp), %ebp
	jmp	.L276
	.p2align 4,,10
	.p2align 3
.L384:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L251
.L276:
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	%ebp, %edi
	call	__GI___waitpid
	cmpl	$-1, %eax
	je	.L384
	cmpl	72(%rsp), %eax
	jne	.L251
	movl	96(%rsp), %edx
	movl	$5, %eax
	testl	%edx, %edx
	cmovne	%eax, %r15d
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L382:
	movl	108(%rsp), %esi
	movq	48(%rsp), %rdi
	call	__posix_spawn_file_actions_addclose
	testl	%eax, %eax
	jne	.L260
.L262:
	testl	%ebx, %ebx
	je	.L385
.L256:
	movq	64(%rsp), %rax
	leaq	.LC7(%rip), %rdi
	movq	$0, 352(%rsp)
	movq	$128, 360(%rsp)
	movq	%rax, 368(%rsp)
	call	__GI_getenv
	testq	%rax, %rax
	je	.L386
	movq	__environ@GOTPCREL(%rip), %rax
	movq	360(%rsp), %r8
	movq	(%rax), %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.L268
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L266:
	addq	$8, %rbx
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.L265
.L268:
	leaq	.LC9(%rip), %rdi
	movl	$4, %ecx
	movq	%rax, %rsi
	repz cmpsb
	je	.L266
	cmpq	$-1, %r8
	je	.L266
	movq	352(%rsp), %rdx
	cmpq	%rdx, %r8
	je	.L387
	leaq	1(%rdx), %rcx
	movq	%rcx, 352(%rsp)
	movq	368(%rsp), %rcx
	movq	%rax, (%rcx,%rdx,8)
	movq	360(%rsp), %r8
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L274:
	movl	108(%rsp), %edi
	call	__GI___close
	cmpq	$0, 32(%rsp)
	movl	$-1, 108(%rsp)
	je	.L388
	leaq	224(%rsp), %rax
	xorl	%r13d, %r13d
	movq	%rax, 40(%rsp)
	leaq	96(%rsp), %rax
	movq	%rax, 88(%rsp)
.L278:
	movq	40(%rsp), %rsi
	movl	104(%rsp), %edi
	movl	$128, %edx
	call	__GI___read
	cmpq	$-1, %rax
	je	.L389
	testl	%eax, %eax
	movl	%eax, %edx
	jle	.L292
.L293:
	movq	40(%rsp), %rcx
	leal	-1(%rdx), %eax
	leaq	1(%rax,%rcx), %rax
	movq	%rcx, %rbx
	movq	%rax, 16(%rsp)
	jmp	.L306
	.p2align 4,,10
	.p2align 3
.L392:
	movq	1480(%rsp), %rdi
	movl	%r15d, %esi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L390
	cmpb	$10, %r15b
	je	.L391
	movl	%r13d, %eax
	andl	$-3, %eax
	cmpl	$1, %eax
	jne	.L301
	movl	$2, %r13d
.L302:
	movq	(%r14), %rsi
	movq	32(%rsp), %rdi
	call	w_addword
	cmpl	$1, %eax
	je	.L289
	movq	24(%rsp), %rax
	xorl	%r12d, %r12d
	movq	$0, (%rax)
	movq	8(%rsp), %rax
	movq	$0, (%rax)
	movq	$0, (%r14)
.L301:
	addq	$1, %rbx
	cmpq	%rbx, 16(%rsp)
	je	.L278
.L306:
	movsbl	(%rbx), %r15d
	movq	1472(%rsp), %rdi
	movl	%r15d, %esi
	movl	%r15d, %ebp
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L392
	cmpl	$3, %r13d
	movq	(%r14), %rdi
	je	.L393
.L304:
	movq	24(%rsp), %rdx
	movq	8(%rsp), %rsi
	addq	$1, %r12
	movl	$0, %eax
	cmpb	$10, %bpl
	movl	%r15d, %ecx
	cmovne	%rax, %r12
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r14)
	je	.L289
	movl	$1, %r13d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L315:
	movl	$1, %r15d
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L385:
	movq	48(%rsp), %rdi
	leaq	.LC8(%rip), %rdx
	xorl	%r8d, %r8d
	movl	$1, %ecx
	movl	$2, %esi
	call	__posix_spawn_file_actions_addopen
	testl	%eax, %eax
	je	.L256
	jmp	.L260
	.p2align 4,,10
	.p2align 3
.L388:
	leaq	224(%rsp), %rax
	leaq	96(%rsp), %rbp
	movl	$-1, %r13d
	movq	%rax, 40(%rsp)
	.p2align 4,,10
	.p2align 3
.L277:
	movq	40(%rsp), %rsi
	movl	104(%rsp), %edi
	movl	$128, %edx
	call	__GI___read
	cmpq	$-1, %rax
	je	.L394
	testl	%eax, %eax
	movslq	%eax, %r8
	jle	.L281
.L282:
	movq	40(%rsp), %rcx
	movq	24(%rsp), %rdx
	addq	%r8, %r12
	movq	8(%rsp), %rsi
	movq	(%r14), %rdi
	call	w_addmem
	testq	%rax, %rax
	movq	%rax, (%r14)
	jne	.L277
.L289:
	movl	72(%rsp), %ebx
	movl	$9, %esi
	movl	%ebx, %edi
	call	__GI___kill
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L395:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L310
.L311:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	__GI___waitpid
	cmpl	$-1, %eax
	je	.L395
.L310:
	movl	104(%rsp), %edi
	movl	$1, %r15d
	call	__GI___close
	jmp	.L251
	.p2align 4,,10
	.p2align 3
.L390:
	cmpl	$2, %r13d
	movl	$0, %r13d
	jne	.L302
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L391:
	cmpl	$1, %r13d
	movl	$3, %eax
	cmove	%eax, %r13d
	jmp	.L301
	.p2align 4,,10
	.p2align 3
.L394:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%r13d, %r8d
	cmpl	$4, %fs:(%rax)
	je	.L277
.L281:
	xorl	%r15d, %r15d
	testl	%r8d, %r8d
	movl	72(%rsp), %ebx
	setne	%r15b
	jmp	.L284
	.p2align 4,,10
	.p2align 3
.L396:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L379
.L284:
	movl	%r15d, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	call	__GI___waitpid
	cltq
	cmpq	$-1, %rax
	je	.L396
	testq	%rax, %rax
	je	.L277
	.p2align 4,,10
	.p2align 3
.L379:
	movq	40(%rsp), %rbx
	jmp	.L287
	.p2align 4,,10
	.p2align 3
.L397:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L288
.L287:
	movl	104(%rsp), %edi
	movl	$128, %edx
	movq	%rbx, %rsi
	call	__GI___read
	cmpq	$-1, %rax
	je	.L397
	testl	%eax, %eax
	movslq	%eax, %r8
	jg	.L282
	.p2align 4,,10
	.p2align 3
.L288:
	subq	$1, %r12
	movq	8(%rsp), %rcx
	jmp	.L307
	.p2align 4,,10
	.p2align 3
.L399:
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.L308
	movq	(%r14), %rdx
	subq	$1, %rax
	addq	%rax, %rdx
	cmpb	$10, (%rdx)
	jne	.L308
	movq	%rax, (%rcx)
	movb	$0, (%rdx)
	leaq	-1(%r12), %rax
	cmpq	$0, (%rcx)
	je	.L398
	movq	%rax, %r12
.L307:
	cmpq	$-1, %r12
	jne	.L399
.L308:
	movl	104(%rsp), %edi
	call	__GI___close
	movl	96(%rsp), %eax
	movl	$-1, 104(%rsp)
	testl	%eax, %eax
	je	.L377
	leaq	.LC6(%rip), %rax
	movl	$-1, 100(%rsp)
	movl	$1, %ebp
	xorl	%ebx, %ebx
	movq	%rax, 112(%rsp)
	leaq	.LC10(%rip), %rax
	jmp	.L313
	.p2align 4,,10
	.p2align 3
.L393:
	movq	%rdi, %rsi
	movq	32(%rsp), %rdi
	call	w_addword
	cmpl	$1, %eax
	je	.L289
	movq	24(%rsp), %rax
	movsbl	(%rbx), %r15d
	xorl	%edi, %edi
	movq	$0, (%rax)
	movq	8(%rsp), %rax
	movl	%r15d, %ebp
	movq	$0, (%rax)
	movq	$0, (%r14)
	jmp	.L304
	.p2align 4,,10
	.p2align 3
.L389:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %edx
	cmpl	$4, %fs:(%rax)
	je	.L278
.L292:
	xorl	%ebx, %ebx
	testl	%edx, %edx
	movl	72(%rsp), %ebp
	setne	%bl
	movq	88(%rsp), %r15
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L400:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L380
.L295:
	movl	%ebx, %edx
	movq	%r15, %rsi
	movl	%ebp, %edi
	call	__GI___waitpid
	cltq
	cmpq	$-1, %rax
	je	.L400
	testq	%rax, %rax
	je	.L278
	.p2align 4,,10
	.p2align 3
.L380:
	movq	40(%rsp), %rbx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L401:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L288
.L298:
	movl	104(%rsp), %edi
	movl	$128, %edx
	movq	%rbx, %rsi
	call	__GI___read
	cmpq	$-1, %rax
	je	.L401
	testl	%eax, %eax
	movl	%eax, %edx
	jg	.L293
	jmp	.L288
	.p2align 4,,10
	.p2align 3
.L265:
	cmpq	$-1, %r8
	je	.L260
	movq	352(%rsp), %rax
	cmpq	%r8, %rax
	je	.L402
	leaq	1(%rax), %rdx
	movq	%rdx, 352(%rsp)
	movq	368(%rsp), %rdx
	movq	$0, (%rdx,%rax,8)
.L270:
	cmpq	$-1, 360(%rsp)
	je	.L260
	movq	368(%rsp), %r9
.L312:
	movq	48(%rsp), %rdx
	leaq	100(%rsp), %rdi
	leaq	112(%rsp), %r8
	leaq	.LC6(%rip), %rsi
	xorl	%ecx, %ecx
	call	__GI___posix_spawn
	movq	368(%rsp), %rdi
	cmpq	64(%rsp), %rdi
	je	.L272
	call	free@PLT
.L272:
	movq	64(%rsp), %rax
	movq	$0, 352(%rsp)
	movq	$128, 360(%rsp)
	movq	%rax, 368(%rsp)
	jmp	.L260
.L386:
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %r9
	jmp	.L312
.L258:
	movq	48(%rsp), %rdi
	movl	$1, %esi
	call	__posix_spawn_file_actions_adddup2
	testl	%eax, %eax
	je	.L262
	jmp	.L260
.L387:
	movq	80(%rsp), %rdi
	movq	%rax, %rsi
	call	strlist_add__
	movq	360(%rsp), %r8
	jmp	.L266
.L383:
	movl	104(%rsp), %edi
	movl	$1, %r15d
	call	__GI___close
	movl	108(%rsp), %edi
	call	__GI___close
	jmp	.L251
.L402:
	movq	80(%rsp), %rdi
	xorl	%esi, %esi
	call	strlist_add__
	jmp	.L270
.L398:
	movq	(%r14), %rdi
	call	free@PLT
	movq	24(%rsp), %rax
	movq	$0, (%rax)
	movq	8(%rsp), %rax
	movq	$0, (%rax)
	movq	$0, (%r14)
	jmp	.L308
.L377:
	movl	76(%rsp), %r15d
	jmp	.L251
.LFE115:
	.size	exec_comm, .-exec_comm
	.p2align 4,,15
	.type	parse_backtick, @function
parse_backtick:
.LFB119:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$88, %rsp
	movq	(%r8), %rax
	movq	%rdi, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdx, 24(%rsp)
	movl	%r9d, 36(%rsp)
	movsbl	(%rcx,%rax), %ecx
	movq	$0, 64(%rsp)
	movq	$0, 56(%rsp)
	movq	$0, 72(%rsp)
	testb	%cl, %cl
	je	.L413
	leaq	72(%rsp), %rax
	leaq	64(%rsp), %rbp
	leaq	56(%rsp), %rbx
	movq	%r8, %r15
	xorl	%r13d, %r13d
	movl	$1, %r12d
	movq	%rax, 40(%rsp)
.L412:
	cmpb	$92, %cl
	je	.L406
	cmpb	$96, %cl
	je	.L407
	movq	72(%rsp), %rdi
	movl	%r12d, %eax
	movq	%rbp, %rdx
	subl	%r13d, %eax
	movq	%rbx, %rsi
	cmpb	$39, %cl
	cmove	%eax, %r13d
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 72(%rsp)
	je	.L423
.L411:
	movq	(%r15), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, (%r15)
	movsbl	1(%r14,%rax), %ecx
	testb	%cl, %cl
	jne	.L412
	movq	72(%rsp), %rdi
.L404:
	call	free@PLT
	movl	$5, %r13d
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L406:
	testl	%r13d, %r13d
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	movq	40(%rsp), %rdi
	je	.L410
	call	parse_qtd_backslash
	testl	%eax, %eax
	je	.L411
	movq	72(%rsp), %rdi
	movl	%eax, 8(%rsp)
	call	free@PLT
	movl	8(%rsp), %eax
	movl	%eax, %r13d
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L407:
	pushq	160(%rsp)
	pushq	160(%rsp)
	movq	160(%rsp), %r9
	movl	52(%rsp), %r8d
	movq	40(%rsp), %rcx
	movq	32(%rsp), %rdx
	movq	24(%rsp), %rsi
	movq	88(%rsp), %rdi
	call	exec_comm
	movq	88(%rsp), %rdi
	movl	%eax, %r13d
	call	free@PLT
	popq	%rax
	popq	%rdx
.L403:
	addq	$88, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L410:
	call	parse_backslash
	testl	%eax, %eax
	movl	%eax, %r13d
	je	.L411
	movq	72(%rsp), %rdi
	call	free@PLT
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L423:
	movl	$1, %r13d
	jmp	.L403
.L413:
	xorl	%edi, %edi
	jmp	.L404
.LFE119:
	.size	parse_backtick, .-parse_backtick
	.p2align 4,,15
	.type	parse_arith, @function
parse_arith:
.LFB92:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$104, %rsp
	movq	(%r8), %rax
	movq	%rdi, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdx, 24(%rsp)
	movl	%r9d, 4(%rsp)
	movsbl	(%rcx,%rax), %ecx
	movq	$0, 40(%rsp)
	movq	$0, 32(%rsp)
	movq	$0, 48(%rsp)
	testb	%cl, %cl
	je	.L453
	leaq	40(%rsp), %r12
	leaq	32(%rsp), %rbp
	leaq	48(%rsp), %r13
	movq	%r8, %r15
	movl	$1, %ebx
.L452:
	cmpb	$59, %cl
	je	.L427
	jg	.L428
	cmpb	$36, %cl
	je	.L429
	jle	.L491
	cmpb	$40, %cl
	je	.L431
	cmpb	$41, %cl
	jne	.L426
	subl	$1, %ebx
	movq	48(%rsp), %rdi
	je	.L492
	movl	$41, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	jne	.L437
.L451:
	movl	$1, %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L431:
	addl	$1, %ebx
.L426:
	movq	48(%rsp), %rdi
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 48(%rsp)
	je	.L451
.L437:
	movq	(%r15), %rdx
	leaq	1(%rdx), %rax
	movq	%rax, (%r15)
	movsbl	1(%r14,%rdx), %ecx
	testb	%cl, %cl
	jne	.L452
	movq	48(%rsp), %rdi
.L425:
	call	free@PLT
	addq	$104, %rsp
	movl	$5, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L428:
	cmpb	$96, %cl
	je	.L433
	jg	.L434
	cmpb	$92, %cl
	je	.L435
	cmpb	$93, %cl
	jne	.L426
	cmpl	$1, %ebx
	movq	48(%rsp), %rdi
	jne	.L425
	testb	$1, 160(%rsp)
	je	.L425
	movq	$0, 56(%rsp)
	cmpb	$0, (%rdi)
	jne	.L493
	xorl	%edi, %edi
.L448:
	leaq	84(%rsp), %rsi
	movb	$0, 84(%rsp)
	xorl	%ecx, %ecx
	movl	$10, %edx
.L490:
	call	_itoa_word
	movq	8(%rsp), %rbx
	movq	24(%rsp), %rdx
	movq	%rax, %rcx
	movq	16(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addstr
	movq	48(%rsp), %rdi
	movq	%rax, (%rbx)
	call	free@PLT
	xorl	%eax, %eax
	cmpq	$0, (%rbx)
	sete	%al
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L433:
	subq	$8, %rsp
	addq	$1, %rax
	movq	%r15, %r8
	movq	%rax, (%r15)
	pushq	$0
	movq	%r14, %rcx
	pushq	$0
	pushq	$0
	movq	%r12, %rdx
	movl	36(%rsp), %r9d
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	parse_backtick
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L437
.L489:
	movq	48(%rsp), %rdi
	movl	%eax, 4(%rsp)
	call	free@PLT
	movl	4(%rsp), %eax
	jmp	.L424
	.p2align 4,,10
	.p2align 3
.L435:
	movq	%r15, %r8
	movq	%r14, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	parse_qtd_backslash
	testl	%eax, %eax
	je	.L437
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L429:
	pushq	$1
	pushq	$0
	movq	%r15, %r8
	pushq	$0
	pushq	$0
	movq	%r14, %rcx
	movl	36(%rsp), %r9d
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	parse_dollars
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L437
	jmp	.L489
	.p2align 4,,10
	.p2align 3
.L434:
	cmpb	$123, %cl
	je	.L427
	cmpb	$125, %cl
	jne	.L426
.L427:
	movq	48(%rsp), %rdi
	call	free@PLT
	movl	$2, %eax
.L424:
	addq	$104, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L491:
	cmpb	$10, %cl
	je	.L427
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L493:
	leaq	56(%rsp), %rsi
	call	eval_expr
	testl	%eax, %eax
	jne	.L449
	movq	56(%rsp), %rdi
	jmp	.L448
	.p2align 4,,10
	.p2align 3
.L449:
	movq	48(%rsp), %rdi
	call	free@PLT
	movl	$5, %eax
	jmp	.L424
.L453:
	xorl	%edi, %edi
	jmp	.L425
.L492:
	movl	160(%rsp), %edx
	movq	$0, 56(%rsp)
	testl	%edx, %edx
	jne	.L425
	cmpb	$41, 1(%r14,%rax)
	leaq	1(%rax), %rdx
	jne	.L425
	movq	%rdx, (%r15)
	xorl	%eax, %eax
	cmpb	$0, (%rdi)
	jne	.L494
.L443:
	movb	$0, 84(%rsp)
	leaq	84(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	movq	%rax, %rdi
	jmp	.L490
.L494:
	leaq	56(%rsp), %rsi
	call	eval_expr
	testl	%eax, %eax
	jne	.L449
	movq	56(%rsp), %rax
	testq	%rax, %rax
	jns	.L443
	movq	8(%rsp), %r14
	movq	24(%rsp), %rdx
	negq	%rax
	movq	16(%rsp), %rsi
	movl	$45, %ecx
	movq	%rax, %rbx
	movq	(%r14), %rdi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, (%r14)
	je	.L446
	movq	%rbx, %rax
	jmp	.L443
.L446:
	movq	48(%rsp), %rdi
	call	free@PLT
	movl	$1, %eax
	jmp	.L424
.LFE92:
	.size	parse_arith, .-parse_arith
	.section	.rodata.str1.1
.LC11:
	.string	"*@$"
.LC12:
	.string	"-=?+"
.LC13:
	.string	"*env == '@' && quoted"
.LC14:
	.string	"! \"Unrecognised action!\""
.LC15:
	.string	"*p"
.LC16:
	.string	"parameter null or not set"
.LC17:
	.string	"%s: %s\n"
.LC18:
	.string	"value != NULL"
	.text
	.p2align 4,,15
	.type	parse_dollars, @function
parse_dollars:
.LFB118:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$232, %rsp
	movq	(%r8), %rbp
	movl	%r9d, 48(%rsp)
	movq	%rdi, 16(%rsp)
	movq	%rsi, (%rsp)
	movq	%rdx, 8(%rsp)
	leaq	1(%rbp), %r9
	leaq	(%rcx,%r9), %rax
	movq	%rax, 24(%rsp)
	movzbl	(%rax), %eax
	cmpb	$39, %al
	je	.L971
	movq	%rcx, %rbx
	movq	%r8, %r14
	jle	.L986
	cmpb	$40, %al
	je	.L499
	cmpb	$91, %al
	jne	.L496
	addq	$2, %rbp
	subq	$8, %rsp
	movq	%rbp, (%r8)
	pushq	$1
.L972:
	movl	64(%rsp), %r9d
	movq	24(%rsp), %rdx
	movq	%r14, %r8
	movq	16(%rsp), %rsi
	movq	32(%rsp), %rdi
	movq	%rbx, %rcx
	call	parse_arith
	popq	%rdi
	popq	%r8
	movl	%eax, %r12d
.L495:
	addq	$232, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	leaq	2(%rbp), %rsi
	leaq	(%rcx,%rsi), %rdi
	cmpb	$40, (%rdi)
	je	.L987
.L502:
	movl	312(%rsp), %r11d
	movl	$0, %eax
	movq	%rsi, (%r14)
	movq	$0, 192(%rsp)
	movq	$0, 160(%rsp)
	testl	%r11d, %r11d
	cmove	288(%rsp), %rax
	xorl	%r15d, %r15d
	movq	%rax, 288(%rsp)
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.L510
	leaq	192(%rsp), %rdi
	movl	%eax, %edx
	movl	$1, %r12d
	movq	%r15, %rax
	xorl	%ebp, %ebp
	movq	%r14, %r15
	leaq	160(%rsp), %r13
	movq	%rbx, %r14
	movq	%rdi, %rbx
	jmp	.L518
	.p2align 4,,10
	.p2align 3
.L513:
	cmpb	$40, %dl
	je	.L515
	cmpb	$41, %dl
	jne	.L511
	testl	%ebp, %ebp
	jne	.L511
	subl	$1, %r12d
	je	.L988
	.p2align 4,,10
	.p2align 3
.L511:
	movq	%rbx, %rdx
	movq	%r13, %rsi
	movq	%rax, %rdi
	call	w_addchar
	testq	%rax, %rax
	je	.L704
	movq	(%r15), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, (%r15)
	movzbl	1(%r14,%rdx), %edx
	testb	%dl, %dl
	je	.L989
.L518:
	cmpb	$39, %dl
	movsbl	%dl, %ecx
	je	.L512
	jg	.L513
	cmpb	$34, %dl
	jne	.L511
	testl	%ebp, %ebp
	je	.L703
	cmpl	$2, %ebp
	setne	%bpl
	movzbl	%bpl, %ebp
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L515:
	cmpl	$1, %ebp
	adcl	$0, %r12d
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L512:
	testl	%ebp, %ebp
	je	.L701
	cmpl	$1, %ebp
	setne	%bpl
	movzbl	%bpl, %ebp
	addl	%ebp, %ebp
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L986:
	testb	%al, %al
	je	.L971
	cmpb	$34, %al
	je	.L971
.L496:
	movq	24(%rsp), %rax
	movq	%r9, (%r14)
	movq	$0, 112(%rsp)
	movq	$0, 104(%rsp)
	movq	$0, 128(%rsp)
	movq	$0, 120(%rsp)
	movzbl	(%rax), %eax
	cmpb	$123, %al
	movb	%al, 40(%rsp)
	je	.L990
	movzbl	40(%rsp), %eax
	cmpb	$35, %al
	je	.L705
.L955:
	movl	$0, 52(%rsp)
.L521:
	movq	__libc_tsd_CTYPE_B@gottpoff(%rip), %r12
	movsbq	%al, %rdx
	movq	%fs:(%r12), %rcx
	testb	$4, 1(%rcx,%rdx,2)
	jne	.L756
	cmpb	$95, %al
	je	.L756
	movsbl	%al, %ecx
	leal	-48(%rcx), %eax
	cmpl	$9, %eax
	ja	.L528
	xorl	%r15d, %r15d
	leaq	112(%rsp), %r13
	leaq	104(%rsp), %r12
	movq	%r15, %rdi
	movzbl	40(%rsp), %r15d
	jmp	.L530
	.p2align 4,,10
	.p2align 3
.L992:
	cmpb	$123, %r15b
	jne	.L529
	movq	(%r14), %rax
	leaq	1(%rax), %r9
	movq	%r9, (%r14)
	movsbl	1(%rbx,%rax), %ecx
	leal	-48(%rcx), %edx
	movl	%ecx, %eax
	cmpl	$9, %edx
	ja	.L991
.L530:
	movq	%r13, %rdx
	movq	%r12, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, %rdi
	jne	.L992
	.p2align 4,,10
	.p2align 3
.L525:
	movl	$1, %r12d
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.L533:
	movq	%r15, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L495
.L995:
	cmpl	$0, 52(%rsp)
	jne	.L522
	movq	%rbp, (%r14)
	.p2align 4,,10
	.p2align 3
.L971:
	movq	16(%rsp), %rbx
	movq	8(%rsp), %rdx
	movl	$36, %ecx
	movq	(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addchar
	movq	%rax, (%rbx)
.L973:
	xorl	%r12d, %r12d
	testq	%rax, %rax
	sete	%r12b
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L703:
	movl	$2, %ebp
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L701:
	movl	$1, %ebp
	jmp	.L511
	.p2align 4,,10
	.p2align 3
.L987:
	leal	3(%rbp), %edx
	movslq	%edx, %rdx
	movzbl	(%rcx,%rdx), %eax
	xorl	%ecx, %ecx
	testb	%al, %al
	jne	.L503
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L504:
	cmpb	$41, %al
	sete	%al
	movzbl	%al, %eax
	subl	%eax, %ecx
.L505:
	addq	$1, %rdx
	movzbl	(%rbx,%rdx), %eax
	testb	%al, %al
	je	.L502
.L503:
	testl	%ecx, %ecx
	je	.L667
	cmpb	$40, %al
	jne	.L504
.L666:
	addl	$1, %ecx
	jmp	.L505
	.p2align 4,,10
	.p2align 3
.L667:
	cmpb	$41, %al
	je	.L993
	cmpb	$40, %al
	je	.L666
	addq	$1, %rdx
	movzbl	(%rbx,%rdx), %eax
	testb	%al, %al
	jne	.L667
	jmp	.L502
	.p2align 4,,10
	.p2align 3
.L990:
	leaq	2(%rbp), %r9
	movq	%r9, (%r14)
	movzbl	2(%rbx,%rbp), %eax
	cmpb	$35, %al
	jne	.L955
	leaq	3(%rbp), %r9
	movl	$1, 52(%rsp)
	movq	%r9, (%r14)
	movzbl	3(%rbx,%rbp), %eax
	jmp	.L521
	.p2align 4,,10
	.p2align 3
.L756:
	leaq	104(%rsp), %rsi
	xorl	%r15d, %r15d
	leaq	112(%rsp), %r13
	movsbl	%al, %ecx
	movq	%r15, %rax
	movq	%rsi, 32(%rsp)
	.p2align 4,,10
	.p2align 3
.L876:
	movq	32(%rsp), %rsi
	movq	%r13, %rdx
	movq	%rax, %rdi
	call	w_addchar
	testq	%rax, %rax
	je	.L525
	movq	(%r14), %rdx
	movq	%fs:(%r12), %rsi
	leaq	1(%rdx), %r9
	movq	%r9, (%r14)
	movsbl	1(%rbx,%rdx), %ecx
	movsbq	%cl, %rdx
	testb	$8, (%rsi,%rdx,2)
	jne	.L876
	cmpb	$95, %cl
	je	.L876
	movq	%rax, %r15
	movl	$0, 56(%rsp)
.L527:
	cmpb	$123, 40(%rsp)
	je	.L994
	movl	$0, 40(%rsp)
	movl	$0, 32(%rsp)
	xorl	%r13d, %r13d
.L534:
	subq	$1, %r9
	movq	%r9, (%r14)
.L541:
	movq	24(%rsp), %rax
	cmpb	$123, (%rax)
	je	.L669
.L551:
	testq	%r15, %r15
	je	.L995
	movsbl	(%r15), %eax
	movl	%eax, %edx
	subl	$48, %eax
	cmpl	$9, %eax
	jbe	.L679
	movl	56(%rsp), %esi
	testl	%esi, %esi
	je	.L558
.L680:
	cmpb	$36, %dl
	je	.L996
	cmpb	$42, %dl
	sete	%al
	cmpb	$64, %dl
	sete	%cl
	orl	%ecx, %eax
	movl	52(%rsp), %ecx
	testl	%ecx, %ecx
	je	.L560
	testb	%al, %al
	jne	.L997
.L560:
	cmpb	$42, %dl
	je	.L561
	testb	%al, %al
	jne	.L998
	leaq	__PRETTY_FUNCTION__.11564(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$1458, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L989:
	movq	%rax, %r15
.L510:
	movq	%r15, %rdi
	movl	$5, %r12d
	call	free@PLT
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L704:
	movl	$1, %r12d
	jmp	.L495
	.p2align 4,,10
	.p2align 3
.L705:
	movl	$0, 40(%rsp)
	movl	$0, 32(%rsp)
	xorl	%r13d, %r13d
.L522:
	movl	__libc_argc(%rip), %eax
	leaq	180(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	movb	$0, 180(%rsp)
	xorl	%r15d, %r15d
	leal	-1(%rax), %edi
	movslq	%edi, %rdi
	call	_itoa_word
	movl	$0, 52(%rsp)
	movq	%rax, %rbp
.L554:
	testq	%rbp, %rbp
	movl	$0, 24(%rsp)
	je	.L557
.L568:
	movl	32(%rsp), %eax
	testl	%eax, %eax
	je	.L574
	leal	-35(%rax), %esi
	cmpl	$47, %esi
	movl	%esi, 64(%rsp)
	ja	.L575
	movl	$1, %eax
	movl	%esi, %ecx
	movabsq	$142936511610885, %rdx
	salq	%cl, %rax
	testq	%rdx, %rax
	jne	.L576
	testl	$335545344, %eax
	jne	.L577
	testb	$1, %ah
	je	.L575
	testq	%rbp, %rbp
	je	.L581
	cmpb	$0, 0(%rbp)
	jne	.L576
	movl	40(%rsp), %eax
	testl	%eax, %eax
	jne	.L999
	.p2align 4,,10
	.p2align 3
.L576:
	testq	%r13, %r13
	movq	$0, 152(%rsp)
	movq	$0, 144(%rsp)
	movq	$0, 136(%rsp)
	je	.L583
	movzbl	0(%r13), %eax
	testb	%al, %al
	je	.L583
	leaq	192(%rsp), %rsi
	movq	%rbp, 88(%rsp)
	xorl	%ebx, %ebx
	leaq	152(%rsp), %r14
	leaq	144(%rsp), %r12
	movq	%r13, 56(%rsp)
	movq	%rsi, 72(%rsp)
	leaq	136(%rsp), %rsi
	movq	%r13, %rbp
	movq	%rsi, 80(%rsp)
	jmp	.L605
	.p2align 4,,10
	.p2align 3
.L1001:
	cmpb	$36, %al
	je	.L587
	cmpb	$39, %al
	je	.L588
	cmpb	$34, %al
	je	.L1000
.L958:
	movsbl	0(%rbp), %ecx
	movq	136(%rsp), %rdi
	movq	%rbp, %r13
.L592:
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 136(%rsp)
	je	.L949
.L604:
	movq	%r13, %rbp
	addq	$1, %rbp
	je	.L950
	movzbl	1(%r13), %eax
	testb	%al, %al
	je	.L950
.L605:
	cmpb	$42, %al
	je	.L585
	jle	.L1001
	cmpb	$92, %al
	je	.L590
	cmpb	$126, %al
	je	.L591
	cmpb	$63, %al
	jne	.L958
.L585:
	testl	%ebx, %ebx
	movq	136(%rsp), %rdi
	jne	.L594
.L959:
	movsbl	0(%rbp), %ecx
	movq	%rbp, %r13
	jmp	.L592
	.p2align 4,,10
	.p2align 3
.L528:
	leaq	.LC11(%rip), %rdi
	movl	$3, %edx
	movl	%ecx, %esi
	movq	%r9, 32(%rsp)
	movl	%ecx, 56(%rsp)
	call	__GI_memchr
	testq	%rax, %rax
	movq	32(%rsp), %r9
	je	.L532
	movl	56(%rsp), %ecx
	leaq	112(%rsp), %rdx
	leaq	104(%rsp), %rsi
	xorl	%edi, %edi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L525
	movq	(%r14), %rax
	movl	$1, 56(%rsp)
	leaq	1(%rax), %r9
	movq	%r9, (%r14)
	jmp	.L527
.L648:
	movl	24(%rsp), %r9d
	testl	%r9d, %r9d
	je	.L649
	movq	%rbp, %rdi
	call	free@PLT
.L649:
	testq	%r13, %r13
	je	.L747
	movq	%r13, %rdi
	movl	$1, %r12d
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L533
.L650:
	movl	$1, %edx
	movq	%rax, %rsi
	movq	%r15, %rdi
	call	__setenv
	movl	$1, 24(%rsp)
.L574:
	movq	%r15, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	movl	52(%rsp), %r8d
	testl	%r8d, %r8d
	je	.L651
.L672:
	testq	%rbp, %rbp
	movb	$0, 212(%rsp)
	je	.L652
.L674:
	movq	%rbp, %rdi
	call	__GI_strlen
	leaq	212(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	movq	%rax, %rdi
	call	_itoa_word
	movq	%rax, %rcx
.L983:
	movq	16(%rsp), %rbx
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addstr
	movl	24(%rsp), %edi
	movq	%rax, (%rbx)
	testl	%edi, %edi
	je	.L973
.L689:
	movq	%rbp, %rdi
	call	free@PLT
	movq	16(%rsp), %rax
	movq	(%rax), %rax
	jmp	.L973
.L557:
	movl	48(%rsp), %eax
	andl	$32, %eax
	movl	%eax, 24(%rsp)
	jne	.L729
	xorl	%ebp, %ebp
	jmp	.L568
.L1004:
	movl	$0, 40(%rsp)
	movl	$0, 32(%rsp)
	xorl	%r13d, %r13d
	movl	$1, 56(%rsp)
.L669:
	movq	(%r14), %rax
	movl	$5, %r12d
	cmpb	$125, (%rbx,%rax)
	jne	.L533
	jmp	.L551
	.p2align 4,,10
	.p2align 3
.L993:
	cmpb	$41, 1(%rbx,%rdx)
	jne	.L502
	addq	$3, %rbp
	subq	$8, %rsp
	movq	%rbp, (%r14)
	pushq	$0
	jmp	.L972
	.p2align 4,,10
	.p2align 3
.L591:
	testl	%ebx, %ebx
	jne	.L958
	cmpq	$0, 144(%rsp)
	jne	.L958
	movq	72(%rsp), %r8
	movq	80(%rsp), %rdi
	xorl	%r9d, %r9d
	movq	%rbp, %rcx
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	$0, 192(%rsp)
	call	parse_tilde
	testl	%eax, %eax
	je	.L601
.L982:
	movl	%eax, %r12d
	movl	24(%rsp), %eax
	movq	56(%rsp), %r13
	movq	88(%rsp), %rbp
	testl	%eax, %eax
	jne	.L1002
.L602:
	movq	136(%rsp), %rdi
	call	free@PLT
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L590:
	movq	136(%rsp), %rdi
	movl	$92, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	leaq	1(%rbp), %r13
	call	w_addchar
	movq	%rax, 136(%rsp)
	movsbl	1(%rbp), %ecx
	movq	%rax, %rdi
	testb	%cl, %cl
	je	.L1003
	testq	%rax, %rax
	jne	.L592
.L949:
	movq	56(%rsp), %r13
.L962:
	movl	$1, %r12d
	jmp	.L533
.L1000:
	cmpl	$2, %ebx
	je	.L732
	testl	%ebx, %ebx
	jne	.L958
	movl	$2, %ebx
.L593:
	movq	%rbp, %r13
	jmp	.L604
	.p2align 4,,10
	.p2align 3
.L588:
	cmpl	$1, %ebx
	je	.L732
	testl	%ebx, %ebx
	jne	.L958
	movl	$1, %ebx
	jmp	.L593
	.p2align 4,,10
	.p2align 3
.L587:
	movq	$0, 192(%rsp)
	pushq	$1
	movq	%rbp, %rcx
	pushq	$0
	pushq	$0
	movq	%r14, %rdx
	pushq	$0
	movl	80(%rsp), %r9d
	movq	%r12, %rsi
	movq	104(%rsp), %r8
	movq	112(%rsp), %rdi
	call	parse_dollars
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L982
.L601:
	addq	192(%rsp), %rbp
	jmp	.L593
.L529:
	movq	%rax, %r15
	movq	24(%rsp), %rax
	cmpb	$123, (%rax)
	je	.L1004
	movsbl	(%rdi), %eax
	xorl	%r13d, %r13d
	movl	$0, 40(%rsp)
	movl	$0, 32(%rsp)
	movl	%eax, %edx
	subl	$48, %eax
	cmpl	$9, %eax
	ja	.L680
.L679:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	call	__GI_strtol
	cmpl	__libc_argc(%rip), %eax
	jge	.L557
	movq	__libc_argv(%rip), %rdx
	cltq
	movq	(%rdx,%rax,8), %rbp
	jmp	.L554
.L558:
	movq	%r15, %rdi
	call	__GI_getenv
	movq	%rax, %rbp
	jmp	.L554
.L532:
	cmpb	$123, 40(%rsp)
	je	.L706
	movl	$0, 56(%rsp)
	xorl	%r15d, %r15d
	movl	$0, 40(%rsp)
	movl	$0, 32(%rsp)
	xorl	%r13d, %r13d
	jmp	.L534
.L994:
	movsbl	(%rbx,%r9), %eax
.L531:
	cmpb	$45, %al
	je	.L535
	jg	.L536
	cmpb	$37, %al
	je	.L537
	cmpb	$43, %al
	je	.L535
	cmpb	$35, %al
	jne	.L708
	leaq	1(%r9), %rdx
	leaq	(%rbx,%rdx), %rax
	cmpb	$35, (%rax)
	je	.L1005
	movq	%rdx, %r9
	movl	$0, 40(%rsp)
	movl	$35, 32(%rsp)
.L542:
	movq	%r9, (%r14)
	movzbl	(%rax), %eax
	xorl	%r13d, %r13d
	testb	%al, %al
	je	.L534
	leaq	128(%rsp), %rsi
	leaq	120(%rsp), %rdi
	xorl	%ecx, %ecx
	movq	%rbp, 72(%rsp)
	movq	%r15, 64(%rsp)
	movl	%eax, %edx
	movq	%r14, %r15
	movq	%r13, %rax
	xorl	%r12d, %r12d
	movl	%ecx, %r13d
	movq	%rsi, %rbp
	movq	%rdi, %r14
	jmp	.L550
	.p2align 4,,10
	.p2align 3
.L1007:
	cmpb	$34, %dl
	je	.L546
	cmpb	$39, %dl
	jne	.L543
	testl	%r12d, %r12d
	je	.L716
	cmpl	$1, %r12d
	setne	%r12b
	movzbl	%r12b, %r12d
	addl	%r12d, %r12d
	.p2align 4,,10
	.p2align 3
.L543:
	movq	(%r15), %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	movsbl	(%rbx,%rdx), %ecx
	movq	%rbp, %rdx
	call	w_addchar
	testq	%rax, %rax
	je	.L719
	movq	(%r15), %rdx
	leaq	1(%rdx), %r9
	movq	%r9, (%r15)
	movzbl	1(%rbx,%rdx), %edx
	testb	%dl, %dl
	je	.L1006
.L550:
	cmpb	$92, %dl
	je	.L544
	jle	.L1007
	cmpb	$123, %dl
	je	.L548
	cmpb	$125, %dl
	jne	.L543
	testl	%r12d, %r12d
	jne	.L543
	testl	%r13d, %r13d
	je	.L945
	subl	$1, %r13d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L546:
	testl	%r12d, %r12d
	je	.L718
	cmpl	$2, %r12d
	setne	%r12b
	movzbl	%r12b, %r12d
	jmp	.L543
	.p2align 4,,10
	.p2align 3
.L544:
	testl	%r12d, %r12d
	jne	.L543
	leaq	1(%r9), %rdx
	movq	%rdx, (%r15)
	cmpb	$0, 1(%rbx,%r9)
	je	.L714
	movl	$92, %ecx
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movq	%rax, %rdi
	call	w_addchar
	testq	%rax, %rax
	jne	.L543
.L719:
	movq	64(%rsp), %r15
	movq	%rax, %r13
	movl	$1, %r12d
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L548:
	cmpl	$1, %r12d
	adcl	$0, %r13d
	jmp	.L543
.L718:
	movl	$2, %r12d
	jmp	.L543
.L716:
	movl	$1, %r12d
	jmp	.L543
.L536:
	cmpb	$61, %al
	je	.L535
	jle	.L1008
	cmpb	$63, %al
	je	.L535
	cmpb	$125, %al
	jne	.L708
	movl	$0, 40(%rsp)
	movl	$0, 32(%rsp)
	xorl	%r13d, %r13d
	jmp	.L541
.L988:
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L495
	pushq	304(%rsp)
	pushq	304(%rsp)
	movq	%rax, %rdi
	movq	304(%rsp), %r9
	movl	64(%rsp), %r8d
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rdx
	movq	32(%rsp), %rsi
	call	exec_comm
	movq	%r15, %rdi
	movl	%eax, %r12d
	call	free@PLT
	popq	%r9
	popq	%r10
	jmp	.L495
.L651:
	xorl	%r12d, %r12d
	testq	%rbp, %rbp
	je	.L495
.L673:
	cmpq	$0, 288(%rsp)
	je	.L757
	testb	$1, 312(%rsp)
	jne	.L757
	movq	%rbp, %rdi
	call	__GI___strdup
	movl	24(%rsp), %esi
	movq	%rax, %r15
	testl	%esi, %esi
	jne	.L692
.L658:
	testq	%r15, %r15
	je	.L750
	movq	16(%rsp), %r13
	movq	%r15, %rbx
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L659:
	movq	304(%rsp), %rsi
	movq	%rbx, %rdi
	call	__GI_strspn
	testl	%ebp, %ebp
	leaq	(%rbx,%rax), %r12
	jne	.L662
	cmpb	$0, (%r12)
	je	.L665
.L662:
	movq	296(%rsp), %rsi
	movq	%r12, %rdi
	call	__GI_strcspn
	movq	304(%rsp), %rsi
	leaq	(%r12,%rax), %rbp
	movq	%rbp, %rdi
	call	__GI_strspn
	leaq	0(%rbp,%rax), %rbx
	movq	0(%r13), %r14
	movzbl	(%rbx), %eax
	testb	%al, %al
	je	.L663
	movq	296(%rsp), %rdi
	movsbl	%al, %esi
	call	__GI_strchr
	testq	%rax, %rax
	je	.L663
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%r12, %rcx
	movb	$0, 0(%rbp)
	movq	%r14, %rdi
	addq	$1, %rbx
	call	w_addstr
	testq	%rax, %rax
	movq	%rax, 0(%r13)
	je	.L1009
.L752:
	movl	$1, %ebp
.L664:
	cmpq	%rbx, %r15
	je	.L659
	movq	0(%r13), %rsi
	movq	288(%rsp), %rdi
	call	w_addword
	cmpl	$1, %eax
	je	.L1010
	movq	8(%rsp), %rax
	movq	$0, (%rax)
	movq	(%rsp), %rax
	movq	$0, (%rax)
	movq	$0, 0(%r13)
	jmp	.L659
	.p2align 4,,10
	.p2align 3
.L663:
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	%r12, %rcx
	movb	$0, 0(%rbp)
	movq	%r14, %rdi
	call	w_addstr
	testq	%rax, %rax
	movq	%rax, 0(%r13)
	je	.L1011
.L677:
	cmpb	$0, (%rbx)
	je	.L665
	xorl	%ebp, %ebp
	jmp	.L664
.L1011:
	cmpb	$0, (%r12)
	je	.L677
.L678:
	movq	%r15, %rdi
	movl	$1, %r12d
	xorl	%r13d, %r13d
	call	free@PLT
	xorl	%r15d, %r15d
	jmp	.L533
	.p2align 4,,10
	.p2align 3
.L729:
	movl	$3, %r12d
	jmp	.L533
.L991:
	movq	%rdi, %r15
	movl	$1, 56(%rsp)
	jmp	.L531
.L577:
	testq	%rbp, %rbp
	je	.L576
	cmpb	$0, 0(%rbp)
	jne	.L579
	movl	40(%rsp), %eax
	testl	%eax, %eax
	jne	.L576
	movl	64(%rsp), %eax
	leaq	.L685(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L685:
	.long	.L607-.L685
	.long	.L606-.L685
	.long	.L607-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L646-.L685
	.long	.L606-.L685
	.long	.L610-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L611-.L685
	.long	.L606-.L685
	.long	.L612-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L607-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L606-.L685
	.long	.L607-.L685
	.text
.L612:
	testq	%rbp, %rbp
	je	.L635
	cmpb	$0, 0(%rbp)
	jne	.L613
	cmpl	$1, 40(%rsp)
	je	.L635
.L643:
	movl	24(%rsp), %r11d
	xorl	%r12d, %r12d
	testl	%r11d, %r11d
	je	.L533
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L533
.L611:
	testq	%rbp, %rbp
	je	.L648
	cmpb	$0, 0(%rbp)
	jne	.L613
	cmpl	$1, 40(%rsp)
	je	.L648
	movl	24(%rsp), %r10d
	xorl	%r12d, %r12d
	testl	%r10d, %r10d
	je	.L533
	movq	%rbp, %rdi
	call	free@PLT
	movl	40(%rsp), %r12d
	jmp	.L533
.L610:
	testq	%rbp, %rbp
	je	.L638
	cmpb	$0, 0(%rbp)
	jne	.L613
	movl	24(%rsp), %r14d
	testl	%r14d, %r14d
	jne	.L682
.L639:
	movl	40(%rsp), %r12d
	testl	%r12d, %r12d
	jne	.L647
	xorl	%r12d, %r12d
	testq	%rbp, %rbp
	jne	.L533
.L647:
	testq	%r13, %r13
	je	.L641
	movq	%r13, %rdi
	movl	$1, %r12d
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L533
	movq	%r15, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	cmpl	$0, 52(%rsp)
	jne	.L1012
	cmpq	$0, 288(%rsp)
	je	.L690
	testb	$1, 312(%rsp)
	jne	.L690
	movq	%rbp, %rdi
	call	__GI___strdup
	movq	%rax, %r15
.L692:
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L658
.L607:
	testq	%rbp, %rbp
	je	.L574
	testq	%r13, %r13
	sete	%al
	je	.L574
	cmpb	$0, 0(%r13)
	je	.L613
	orq	$-1, %rcx
	movq	%rbp, %rdi
	repnz scasb
	cmpl	$37, 32(%rsp)
	movq	%rcx, %rax
	notq	%rax
	leaq	-1(%rbp,%rax), %rbx
	je	.L961
	jbe	.L1013
	movl	32(%rsp), %eax
	cmpl	$76, %eax
	je	.L960
	cmpl	$82, %eax
	jne	.L574
	cmpq	%rbp, %rbx
	movq	%rbp, %r12
	jb	.L613
.L619:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__GI_fnmatch
	cmpl	$1, %eax
	jne	.L1014
	addq	$1, %r12
	cmpq	%r12, %rbx
	jnb	.L619
.L613:
	movq	%r15, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	movl	52(%rsp), %eax
	testl	%eax, %eax
	je	.L673
	movb	$0, 212(%rsp)
	jmp	.L674
.L961:
	cmpq	%rbp, %rbx
	jb	.L613
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movq	%r13, %rdi
	call	__GI_fnmatch
	cmpl	$1, %eax
	jne	.L1015
	subq	$1, %rbx
	jmp	.L961
.L960:
	cmpq	%rbp, %rbx
	jb	.L613
	movzbl	(%rbx), %r12d
	xorl	%edx, %edx
	movb	$0, (%rbx)
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	__GI_fnmatch
	cmpl	$1, %eax
	movb	%r12b, (%rbx)
	jne	.L1016
	subq	$1, %rbx
	jmp	.L960
.L609:
	testq	%rbp, %rbp
	je	.L643
.L644:
	cmpb	$0, 0(%rbp)
	jne	.L646
	cmpl	$0, 40(%rsp)
	jne	.L643
.L646:
	movl	24(%rsp), %ebx
	testl	%ebx, %ebx
	je	.L647
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L647
	.p2align 4,,10
	.p2align 3
.L732:
	xorl	%ebx, %ebx
	jmp	.L593
.L561:
	movq	__libc_argv(%rip), %rbx
	xorl	%r12d, %r12d
	movq	8(%rbx), %rdi
	leaq	16(%rbx), %rbp
	testq	%rdi, %rdi
	je	.L565
	.p2align 4,,10
	.p2align 3
.L564:
	addq	$8, %rbp
	call	__GI_strlen
	movq	-8(%rbp), %rdi
	leaq	1(%r12,%rax), %r12
	testq	%rdi, %rdi
	jne	.L564
.L565:
	movq	%r12, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L962
	movb	$0, (%rax)
	movq	8(%rbx), %rsi
	movq	%rax, %rdi
	movl	$2, %r12d
	testq	%rsi, %rsi
	jne	.L567
	jmp	.L956
	.p2align 4,,10
	.p2align 3
.L569:
	leaq	1(%rax), %rdi
	movb	$32, (%rax)
	movq	(%rbx,%r12,8), %rsi
	addq	$1, %r12
.L567:
	call	__GI_stpcpy@PLT
	cmpq	$0, (%rbx,%r12,8)
	jne	.L569
.L956:
	movl	$1, 24(%rsp)
	jmp	.L568
.L594:
	movl	$92, %ecx
	movq	%r14, %rdx
	movq	%r12, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, 136(%rsp)
	jne	.L959
	jmp	.L949
	.p2align 4,,10
	.p2align 3
.L950:
	movq	56(%rsp), %r13
	movq	88(%rsp), %rbp
.L583:
	movq	%r13, %rdi
	call	free@PLT
	movl	64(%rsp), %eax
	movq	136(%rsp), %r13
	cmpl	$47, %eax
	ja	.L606
	leaq	.L608(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L608:
	.long	.L607-.L608
	.long	.L606-.L608
	.long	.L607-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L609-.L608
	.long	.L606-.L608
	.long	.L610-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L611-.L608
	.long	.L606-.L608
	.long	.L612-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L607-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L606-.L608
	.long	.L607-.L608
	.text
.L1002:
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L602
.L996:
	movb	$0, 180(%rsp)
	call	__GI___getpid
	leaq	180(%rsp), %rsi
	movslq	%eax, %rdi
	xorl	%ecx, %ecx
	movl	$10, %edx
	call	_itoa_word
	movq	%rax, %rbp
	jmp	.L554
.L1006:
	movq	%r15, %r14
	movq	72(%rsp), %rbp
	movq	%rax, %r13
	movq	64(%rsp), %r15
	jmp	.L534
.L1008:
	cmpb	$58, %al
	jne	.L708
	leaq	1(%r9), %r8
	leaq	.LC12(%rip), %rdi
	movl	$4, %edx
	movq	%r9, 64(%rsp)
	xorl	%r13d, %r13d
	movl	$5, %r12d
	leaq	(%rbx,%r8), %rcx
	movq	%r8, 40(%rsp)
	movsbl	(%rcx), %esi
	movq	%rcx, 32(%rsp)
	call	__GI_memchr
	testq	%rax, %rax
	movq	32(%rsp), %rcx
	movq	40(%rsp), %r8
	movq	64(%rsp), %r9
	je	.L533
	movq	%r8, (%r14)
	movsbl	(%rcx), %eax
	addq	$2, %r9
	movl	$1, 40(%rsp)
	movl	%eax, 32(%rsp)
	leaq	(%rbx,%r9), %rax
	jmp	.L542
.L706:
	movl	$5, %r12d
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	jmp	.L533
.L757:
	movq	%rbp, %rcx
	jmp	.L983
.L998:
	movl	312(%rsp), %eax
	testl	%eax, %eax
	je	.L561
	cmpl	$2, __libc_argc(%rip)
	je	.L1017
	jle	.L571
	movq	__libc_argv(%rip), %rax
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	8(%rax), %rcx
	movq	16(%rsp), %rax
	movq	(%rax), %rdi
	call	w_addstr
	testq	%rax, %rax
	je	.L727
	movq	288(%rsp), %rdi
	movq	%rax, %rsi
	call	w_addword
	testl	%eax, %eax
	jne	.L727
	movq	__libc_argv(%rip), %rax
	movl	$2, %ebp
	cmpq	$0, 24(%rax)
	jne	.L573
	jmp	.L572
	.p2align 4,,10
	.p2align 3
.L1018:
	movq	288(%rsp), %rdi
	movq	%rax, %rsi
	call	w_addword
	testl	%eax, %eax
	jne	.L727
	movq	__libc_argv(%rip), %rax
	addq	$1, %rbp
	cmpq	$0, 16(%rax,%rbx)
	je	.L572
.L573:
	movq	(%rax,%rbp,8), %rdi
	leaq	0(,%rbp,8), %rbx
	call	__GI___strdup
	testq	%rax, %rax
	jne	.L1018
.L727:
	movl	312(%rsp), %r12d
	jmp	.L533
.L535:
	addq	$1, %r9
	movl	%eax, 32(%rsp)
	movl	$0, 40(%rsp)
	leaq	(%rbx,%r9), %rax
	jmp	.L542
.L708:
	movl	$5, %r12d
	xorl	%r13d, %r13d
	jmp	.L533
.L537:
	leaq	1(%r9), %rdx
	leaq	(%rbx,%rdx), %rax
	cmpb	$37, (%rax)
	je	.L1019
	movq	%rdx, %r9
	movl	$0, 40(%rsp)
	movl	$37, 32(%rsp)
	jmp	.L542
.L1017:
	movq	__libc_argv(%rip), %rax
	movq	8(%rax), %rbp
	jmp	.L554
.L652:
	leaq	212(%rsp), %rsi
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	movl	$10, %edx
	call	_itoa_word
	movq	16(%rsp), %rbx
	movq	8(%rsp), %rdx
	movq	%rax, %rcx
	movq	(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addstr
	movl	24(%rsp), %ecx
	movq	%rax, (%rbx)
	testl	%ecx, %ecx
	je	.L973
.L676:
	leaq	__PRETTY_FUNCTION__.11564(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC18(%rip), %rdi
	movl	$1888, %edx
	call	__GI___assert_fail
	.p2align 4,,10
	.p2align 3
.L997:
	movl	__libc_argc(%rip), %eax
	movl	$1, %edi
	leaq	180(%rsp), %rsi
	movl	$10, %edx
	movb	$0, 180(%rsp)
	testl	%eax, %eax
	cmovg	__libc_argc(%rip), %edi
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	subl	$1, %edi
	movslq	%edi, %rdi
	call	_itoa_word
	movq	16(%rsp), %rbx
	movq	8(%rsp), %rdx
	movq	%rax, %rcx
	movq	(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addstr
	movq	%r15, %rdi
	movq	%rax, (%rbx)
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	cmpq	$0, (%rbx)
	sete	%r12b
	jmp	.L495
.L579:
	movl	64(%rsp), %eax
	leaq	.L681(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L681:
	.long	.L607-.L681
	.long	.L606-.L681
	.long	.L607-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L644-.L681
	.long	.L606-.L681
	.long	.L610-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L611-.L681
	.long	.L606-.L681
	.long	.L612-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L607-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L606-.L681
	.long	.L607-.L681
	.text
.L571:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	jmp	.L495
.L581:
	movl	64(%rsp), %eax
	leaq	.L671(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L671:
	.long	.L574-.L671
	.long	.L606-.L671
	.long	.L574-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L643-.L671
	.long	.L606-.L671
	.long	.L638-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L648-.L671
	.long	.L606-.L671
	.long	.L635-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L574-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L606-.L671
	.long	.L574-.L671
	.text
.L635:
	cmpb	$0, 0(%r13)
	movq	%r13, %rax
	jne	.L637
	leaq	.LC16(%rip), %rsi
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	call	__GI___dcgettext
.L637:
	leaq	.LC17(%rip), %rsi
	movq	%rax, %rcx
	movq	%r15, %rdx
	xorl	%edi, %edi
	xorl	%eax, %eax
	call	__fxprintf
	jmp	.L643
.L638:
	cmpl	$0, 24(%rsp)
	je	.L647
.L682:
	movq	%rbp, %rdi
	call	free@PLT
	jmp	.L639
.L665:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	free@PLT
	jmp	.L495
.L750:
	xorl	%r13d, %r13d
	jmp	.L962
.L999:
	movl	64(%rsp), %eax
	leaq	.L686(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L686:
	.long	.L607-.L686
	.long	.L606-.L686
	.long	.L607-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L643-.L686
	.long	.L606-.L686
	.long	.L610-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L611-.L686
	.long	.L606-.L686
	.long	.L612-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L607-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L606-.L686
	.long	.L607-.L686
	.text
.L1005:
	addq	$2, %r9
	movl	$0, 40(%rsp)
	movl	$76, 32(%rsp)
	leaq	(%rbx,%r9), %rax
	jmp	.L542
.L641:
	movq	%r15, %rdi
	xorl	%r12d, %r12d
	call	free@PLT
	movl	52(%rsp), %edx
	testl	%edx, %edx
	je	.L495
	leaq	212(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	xorl	%edi, %edi
	movb	$0, 212(%rsp)
	call	_itoa_word
	movq	16(%rsp), %rbx
	movq	8(%rsp), %rdx
	movq	%rax, %rcx
	movq	(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addstr
	movq	%rax, (%rbx)
	jmp	.L676
.L1019:
	addq	$2, %r9
	movl	$0, 40(%rsp)
	movl	$82, 32(%rsp)
	leaq	(%rbx,%r9), %rax
	jmp	.L542
.L1010:
	movq	%r15, %rdi
	movl	%eax, %r12d
	xorl	%r13d, %r13d
	call	free@PLT
	xorl	%r15d, %r15d
	jmp	.L533
.L714:
	movq	64(%rsp), %r15
	movq	%rax, %r13
	movl	$5, %r12d
	jmp	.L533
.L945:
	movq	%r15, %r14
	movq	72(%rsp), %rbp
	movq	%rax, %r13
	movq	64(%rsp), %r15
	jmp	.L541
.L747:
	xorl	%ebp, %ebp
	leaq	.LC0(%rip), %rax
	jmp	.L650
.L572:
	movq	8(%rsp), %rdi
	movq	(%rsp), %rsi
	movq	$0, (%rdi)
	movq	16(%rsp), %rdi
	movq	$0, (%rsi)
	movq	$0, (%rdi)
	movq	(%rax,%rbp,8), %rbp
	jmp	.L554
.L690:
	movq	%rbp, %rcx
.L984:
	movq	16(%rsp), %rbx
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	movq	(%rbx), %rdi
	call	w_addstr
	movq	%rax, (%rbx)
	jmp	.L689
.L1013:
	cmpl	$35, 32(%rsp)
	jne	.L574
	cmpq	%rbp, %rbx
	jb	.L613
	movq	%rbp, %r12
.L626:
	movzbl	(%r12), %r14d
	xorl	%edx, %edx
	movb	$0, (%r12)
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	__GI_fnmatch
	cmpl	$1, %eax
	movb	%r14b, (%r12)
	jne	.L1020
	addq	$1, %r12
	cmpq	%r12, %rbx
	jnb	.L626
	movq	%r15, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	cmpl	$0, 52(%rsp)
	jne	.L672
	jmp	.L673
.L1015:
	subq	%rbp, %rbx
	leaq	1(%rbx), %rdi
	movq	%rbx, %r12
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L976
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	__GI_mempcpy@PLT
	cmpl	$0, 24(%rsp)
	movb	$0, (%rax)
	jne	.L1021
.L625:
	movq	%r15, %rdi
	call	free@PLT
	movq	%r13, %rdi
	call	free@PLT
	cmpl	$0, 52(%rsp)
	je	.L1022
	movb	$0, 212(%rsp)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	orq	$-1, %rcx
	leaq	212(%rsp), %rsi
	movl	$10, %edx
	repnz scasb
	movq	%rcx, %rax
	xorl	%ecx, %ecx
	notq	%rax
	leaq	-1(%rax), %rdi
	call	_itoa_word
	movq	%rax, %rcx
.L985:
	movq	16(%rsp), %r15
	movq	8(%rsp), %rdx
	movq	%rbx, %rbp
	movq	(%rsp), %rsi
	movq	(%r15), %rdi
	call	w_addstr
	movq	%rax, (%r15)
	jmp	.L689
.L1020:
	cmpl	$0, 24(%rsp)
	movq	%r12, %rdi
	jne	.L978
	movq	%r12, %rbp
	jmp	.L613
.L1022:
	cmpq	$0, 288(%rsp)
	je	.L758
	testb	$1, 312(%rsp)
	jne	.L758
	movq	%rbx, %rdi
	movq	%rbx, %rbp
	call	__GI___strdup
	movq	%rax, %r15
	jmp	.L692
.L1024:
	movq	%rbx, %rdi
.L978:
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rbx
	movq	%rbp, %rdi
	je	.L966
.L968:
	call	free@PLT
	jmp	.L625
.L758:
	movq	%rbx, %rcx
	jmp	.L985
.L976:
	cmpl	$0, 24(%rsp)
	movl	$1, %r12d
	je	.L533
	movq	%rbp, %rdi
.L966:
	call	free@PLT
	movl	24(%rsp), %r12d
	jmp	.L533
.L1021:
	movq	%rbp, %rdi
	jmp	.L968
.L1014:
	subq	%rbp, %r12
	leaq	1(%r12), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L976
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	__GI_mempcpy@PLT
	cmpl	$0, 24(%rsp)
	movb	$0, (%rax)
	jne	.L1023
	movq	%rbx, %rbp
	movl	$1, 24(%rsp)
	jmp	.L613
.L1003:
	leaq	__PRETTY_FUNCTION__.11564(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC15(%rip), %rdi
	movl	$1632, %edx
	call	__GI___assert_fail
.L1023:
	movq	%rbp, %rdi
	movq	%rbx, %rbp
	call	free@PLT
	jmp	.L613
.L575:
	leaq	__PRETTY_FUNCTION__.11564(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$1537, %edx
	call	__GI___assert_fail
.L1012:
	movb	$0, 212(%rsp)
	xorl	%eax, %eax
	movq	%rbp, %rdi
	orq	$-1, %rcx
	leaq	212(%rsp), %rsi
	movl	$10, %edx
	repnz scasb
	movq	%rcx, %rax
	xorl	%ecx, %ecx
	notq	%rax
	leaq	-1(%rax), %rdi
	call	_itoa_word
	movq	%rax, %rcx
	jmp	.L984
.L1009:
	cmpb	$0, (%r12)
	je	.L752
	jmp	.L678
.L606:
	leaq	__PRETTY_FUNCTION__.11564(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC14(%rip), %rdi
	movl	$1870, %edx
	call	__GI___assert_fail
.L1016:
	cmpl	$0, 24(%rsp)
	jne	.L1024
	movq	%rbx, %rbp
	jmp	.L613
.LFE118:
	.size	parse_dollars, .-parse_dollars
	.p2align 4,,15
	.globl	__GI_wordfree
	.hidden	__GI_wordfree
	.type	__GI_wordfree, @function
__GI_wordfree:
.LFB121:
	testq	%rdi, %rdi
	je	.L1040
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L1040
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movq	16(%rdi), %rdx
	leaq	(%rax,%rdx,8), %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L1027
	.p2align 4,,10
	.p2align 3
.L1028:
	addq	$8, %rbx
	call	free@PLT
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.L1028
	movq	8(%rbp), %rax
.L1027:
	movq	%rax, %rdi
	call	free@PLT
	movq	$0, 8(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L1040:
	rep ret
.LFE121:
	.size	__GI_wordfree, .-__GI_wordfree
	.globl	wordfree
	.set	wordfree,__GI_wordfree
	.section	.rodata.str1.1
.LC19:
	.string	"error == GLOB_NOSPACE"
.LC20:
	.string	"globbuf.gl_pathv[0] != NULL"
.LC21:
	.string	" \t"
.LC22:
	.string	"\n|&;<>(){}"
	.text
	.p2align 4,,15
	.globl	wordexp
	.type	wordexp, @function
wordexp:
.LFB122:
	pushq	%r15
	pushq	%r14
	movq	%rsi, %rax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$280, %rsp
	movq	%rsi, 16(%rsp)
	movq	(%rsi), %rsi
	movl	%edx, 88(%rsp)
	andl	$8, %edx
	movq	$0, 144(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 152(%rsp)
	movq	%rsi, 80(%rsp)
	movq	16(%rax), %rsi
	movq	%rsi, 104(%rsp)
	jne	.L1044
	movq	8(%rax), %rax
	movq	%rax, 96(%rsp)
.L1045:
	movl	88(%rsp), %eax
	movl	%eax, %esi
	andl	$2, %esi
	movl	%esi, 92(%rsp)
	jne	.L1047
	movq	16(%rsp), %r14
	andl	$1, %eax
	movl	%eax, %r15d
	movq	$0, (%r14)
	jne	.L1212
	movl	$8, %esi
	movl	$1, %edi
	call	calloc@PLT
	movq	16(%rsp), %rsi
	testq	%rax, %rax
	movq	%rax, 8(%rsi)
	je	.L1051
	movq	$0, 16(%rsi)
.L1047:
	leaq	.LC7(%rip), %rdi
	call	__GI_getenv
	testq	%rax, %rax
	movq	%rax, 8(%rsp)
	je	.L1052
	movzbl	(%rax), %edx
	testb	%dl, %dl
	je	.L1213
	leaq	124(%rsp), %rdi
	movq	8(%rsp), %rsi
	movq	%rdi, 24(%rsp)
	movq	%rdi, %rcx
	jmp	.L1053
	.p2align 4,,10
	.p2align 3
.L1214:
	cmpb	$32, %dl
	je	.L1137
.L1056:
	addq	$1, %rsi
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	je	.L1054
.L1053:
	leal	-9(%rdx), %eax
	cmpb	$1, %al
	ja	.L1214
.L1137:
	cmpq	%rdi, %rcx
	jbe	.L1058
	cmpb	124(%rsp), %dl
	je	.L1056
	movq	%rdi, %rax
	jmp	.L1060
	.p2align 4,,10
	.p2align 3
.L1215:
	cmpb	%dl, (%rax)
	je	.L1056
.L1060:
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L1215
.L1059:
	addq	$1, %rsi
	movb	%dl, (%rcx)
	addq	$1, %rcx
	movzbl	(%rsi), %edx
	testb	%dl, %dl
	jne	.L1053
	.p2align 4,,10
	.p2align 3
.L1054:
	movb	$0, (%rcx)
.L1055:
	movsbl	(%rbx), %r13d
	movq	$0, 128(%rsp)
	testb	%r13b, %r13b
	je	.L1061
	leaq	192(%rsp), %rax
	leaq	144(%rsp), %r12
	leaq	136(%rsp), %rbp
	xorl	%edx, %edx
	movq	%rax, 72(%rsp)
	.p2align 4,,10
	.p2align 3
.L1124:
	cmpb	$63, %r13b
	je	.L1063
	jg	.L1064
	cmpb	$36, %r13b
	je	.L1065
	jle	.L1216
	cmpb	$39, %r13b
	jne	.L1217
	movsbl	1(%rbx,%rdx), %ecx
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	testb	%cl, %cl
	je	.L1076
	cmpb	$39, %cl
	je	.L1090
	movq	152(%rsp), %rax
	jmp	.L1091
	.p2align 4,,10
	.p2align 3
.L1218:
	movq	128(%rsp), %rdx
	leaq	1(%rdx), %rcx
	movq	%rcx, 128(%rsp)
	movsbl	1(%rbx,%rdx), %ecx
	testb	%cl, %cl
	je	.L1087
	cmpb	$39, %cl
	je	.L1090
.L1091:
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rax, %rdi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 152(%rsp)
	jne	.L1218
.L1086:
	movq	%rax, %rdi
	call	free@PLT
.L1210:
	movl	$1, %r15d
.L1043:
	addq	$280, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L1212:
	movq	16(%r14), %rax
	movl	$8, %esi
	leaq	1(%rax), %rdi
	movq	%rax, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 8(%r14)
	jne	.L1047
	movq	152(%rsp), %rdi
	call	free@PLT
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1064:
	cmpb	$92, %r13b
	je	.L1069
	jle	.L1219
	cmpb	$96, %r13b
	je	.L1071
	cmpb	$126, %r13b
	jne	.L1062
	movq	16(%rsp), %rax
	leaq	152(%rsp), %rdi
	leaq	128(%rsp), %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	(%rax), %r9
	call	parse_tilde
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L1073
	.p2align 4,,10
	.p2align 3
.L1074:
	movq	128(%rsp), %rax
	movsbl	1(%rbx,%rax), %r13d
	leaq	1(%rax), %rdx
	movq	%rdx, 128(%rsp)
	testb	%r13b, %r13b
	jne	.L1124
.L1061:
	movq	152(%rsp), %rsi
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	je	.L1043
	movq	16(%rsp), %rdi
	call	w_addword
	movl	%eax, %r15d
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1090:
	cmpq	$0, 136(%rsp)
	jne	.L1074
	movq	16(%rsp), %rdi
	xorl	%esi, %esi
	call	w_addword
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L1074
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1216:
	cmpb	$34, %r13b
	jne	.L1062
	movsbl	1(%rbx,%rdx), %ecx
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	testb	%cl, %cl
	je	.L1076
	leaq	128(%rsp), %r14
	leaq	152(%rsp), %r13
.L1083:
	cmpb	$36, %cl
	je	.L1078
	jle	.L1220
	cmpb	$92, %cl
	je	.L1081
	cmpb	$96, %cl
	jne	.L1077
	addq	$1, %rax
	subq	$8, %rsp
	movq	%r14, %r8
	movq	%rax, 136(%rsp)
	pushq	$0
	movq	%rbx, %rcx
	pushq	$0
	pushq	$0
	movq	%r12, %rdx
	movl	120(%rsp), %r9d
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	parse_backtick
	addq	$32, %rsp
	testl	%eax, %eax
	jne	.L1199
.L1085:
	movq	128(%rsp), %rdx
	movsbl	1(%rbx,%rdx), %ecx
	leaq	1(%rdx), %rax
	movq	%rax, 128(%rsp)
	testb	%cl, %cl
	jne	.L1083
.L1076:
	movq	152(%rsp), %rax
.L1087:
	movq	%rax, %rdi
	movl	$5, %r15d
	call	free@PLT
.L1128:
	movl	92(%rsp), %eax
	testl	%eax, %eax
	je	.L1221
.L1126:
	movq	16(%rsp), %rax
	movq	80(%rsp), %rsi
	movq	%rsi, (%rax)
	movq	96(%rsp), %rsi
	movq	%rsi, 8(%rax)
	movq	104(%rsp), %rsi
	movq	%rsi, 16(%rax)
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1220:
	cmpb	$34, %cl
	je	.L1090
.L1077:
	movq	152(%rsp), %rdi
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 152(%rsp)
	jne	.L1085
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1219:
	cmpb	$91, %r13b
	je	.L1063
.L1062:
	leaq	.LC21(%rip), %rdi
	movl	%r13d, %esi
	call	__GI_strchr
	testq	%rax, %rax
	movq	152(%rsp), %r14
	je	.L1222
	testq	%r14, %r14
	je	.L1123
	movq	16(%rsp), %rdi
	movq	%r14, %rsi
	call	w_addword
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L1073
.L1123:
	movq	$0, 144(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 152(%rsp)
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1071:
	addq	$1, %rdx
	leaq	152(%rsp), %rdi
	subq	$8, %rsp
	movq	%rdx, 136(%rsp)
	pushq	32(%rsp)
	movq	%rbx, %rcx
	pushq	24(%rsp)
	pushq	40(%rsp)
	movq	%r12, %rdx
	movl	120(%rsp), %r9d
	movq	%rbp, %rsi
	leaq	160(%rsp), %r8
	call	parse_backtick
	addq	$32, %rsp
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L1074
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1217:
	cmpb	$42, %r13b
	jne	.L1062
.L1063:
	movq	128(%rsp), %r14
	movq	$0, 160(%rsp)
	movq	$0, 168(%rsp)
	movq	$0, 176(%rsp)
	movzbl	(%rbx,%r14), %edx
	testb	%dl, %dl
	je	.L1223
	leaq	160(%rsp), %rax
	movq	%rbp, 40(%rsp)
	movl	$1, %r15d
	xorl	%r13d, %r13d
	movq	%r12, 32(%rsp)
	movl	%edx, %ebp
	movq	%rax, 48(%rsp)
	leaq	128(%rsp), %rax
	movq	%rax, 56(%rsp)
	leaq	152(%rsp), %rax
	movq	%rax, 64(%rsp)
.L1103:
	movq	8(%rsp), %rdi
	movsbl	%bpl, %r12d
	movl	%r12d, %esi
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L1202
	cmpb	$39, %bpl
	je	.L1224
	cmpb	$34, %bpl
	je	.L1225
	cmpl	$1, %r13d
	je	.L1098
	cmpb	$36, %bpl
	je	.L1226
.L1098:
	cmpb	$92, %bpl
	jne	.L1096
	testl	%r13d, %r13d
	movq	56(%rsp), %r8
	movq	%rbx, %rcx
	movq	32(%rsp), %rdx
	movq	40(%rsp), %rsi
	movq	64(%rsp), %rdi
	je	.L1100
	call	parse_qtd_backslash
	movl	%eax, %r15d
.L1101:
	testl	%r15d, %r15d
	je	.L1095
.L1099:
	movq	48(%rsp), %rdi
	call	__GI_wordfree
.L1073:
	movq	152(%rsp), %rdi
	call	free@PLT
	cmpl	$1, %r15d
	jne	.L1128
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1225:
	testl	%r13d, %r13d
	je	.L1132
	cmpl	$2, %r13d
	jne	.L1096
.L1133:
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L1095:
	movq	128(%rsp), %rax
	movzbl	1(%rbx,%rax), %ebp
	leaq	1(%rax), %r14
	movq	%r14, 128(%rsp)
	testb	%bpl, %bpl
	jne	.L1103
.L1202:
	movq	32(%rsp), %r12
	movq	40(%rsp), %rbp
.L1093:
	movq	152(%rsp), %rsi
	movq	48(%rsp), %rdi
	leaq	-1(%r14), %r9
	movq	%r9, 128(%rsp)
	call	w_addword
	testl	%eax, %eax
	movl	%eax, %r15d
	movq	$0, 144(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 152(%rsp)
	jne	.L1099
	xorl	%r14d, %r14d
	movq	%rbx, 40(%rsp)
	movq	%rbp, %r15
.L1104:
	cmpq	%r14, 160(%rsp)
	jbe	.L1227
	movq	%r12, 32(%rsp)
	movq	72(%rsp), %r12
	leaq	0(,%r14,8), %r13
.L1117:
	movq	168(%rsp), %rax
	xorl	%edx, %edx
	movq	%r12, %rcx
	movl	$16, %esi
	movq	(%rax,%r13), %rdi
	call	__GI_glob
	testl	%eax, %eax
	jne	.L1228
	movq	8(%rsp), %rax
	cmpb	$0, (%rax)
	jne	.L1107
	movq	200(%rsp), %rax
	movq	32(%rsp), %r12
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L1229
	movq	152(%rsp), %rdi
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	w_addstr
	cmpq	$1, 192(%rsp)
	movq	%rax, %rdi
	movq	%rax, 152(%rsp)
	jbe	.L1110
	testq	%rax, %rax
	je	.L1110
	movl	$1, %ebx
	movl	$1, %r13d
	jmp	.L1111
	.p2align 4,,10
	.p2align 3
.L1230:
	movq	200(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	leal	1(%rbx), %ebp
	movq	(%rdx,%r13,8), %rcx
	movq	%r12, %rdx
	movq	%rbp, %rbx
	call	w_addstr
	cmpq	%rbp, 192(%rsp)
	movq	%rax, %rdi
	movq	%rax, 152(%rsp)
	jbe	.L1110
	testq	%rax, %rax
	je	.L1110
	movq	%rbp, %r13
.L1111:
	movl	$32, %ecx
	movq	%r12, %rdx
	movq	%r15, %rsi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 152(%rsp)
	jne	.L1230
.L1110:
	movq	72(%rsp), %rdi
	addq	$1, %r14
	call	__GI_globfree
	cmpq	$0, 152(%rsp)
	jne	.L1104
	movl	$1, %r15d
	jmp	.L1099
	.p2align 4,,10
	.p2align 3
.L1065:
	leaq	152(%rsp), %rdi
	pushq	$0
	pushq	32(%rsp)
	pushq	24(%rsp)
	pushq	40(%rsp)
	movq	%rbx, %rcx
	movl	120(%rsp), %r9d
	movq	%r12, %rdx
	movq	%rbp, %rsi
	leaq	160(%rsp), %r8
	call	parse_dollars
	addq	$32, %rsp
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L1074
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1069:
	leaq	152(%rsp), %rdi
	leaq	128(%rsp), %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	call	parse_backslash
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L1074
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1224:
	testl	%r13d, %r13d
	je	.L1130
	cmpl	$1, %r13d
	je	.L1133
.L1096:
	movq	32(%rsp), %rdx
	movq	40(%rsp), %rsi
	movl	%r12d, %ecx
	movq	152(%rsp), %rdi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 152(%rsp)
	jne	.L1095
.L1102:
	movq	48(%rsp), %rdi
	movq	32(%rsp), %r12
	movq	40(%rsp), %rbp
	call	__GI_wordfree
	testl	%r15d, %r15d
	je	.L1074
	movq	152(%rsp), %rax
	jmp	.L1086
	.p2align 4,,10
	.p2align 3
.L1078:
	pushq	$1
	pushq	32(%rsp)
	movq	%r14, %r8
	pushq	24(%rsp)
	pushq	40(%rsp)
	movq	%rbx, %rcx
	movl	120(%rsp), %r9d
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	parse_dollars
	addq	$32, %rsp
	testl	%eax, %eax
	je	.L1085
.L1199:
	movl	%eax, %r15d
	jmp	.L1073
	.p2align 4,,10
	.p2align 3
.L1081:
	movq	%r14, %r8
	movq	%rbx, %rcx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	parse_qtd_backslash
	testl	%eax, %eax
	je	.L1085
	jmp	.L1199
	.p2align 4,,10
	.p2align 3
.L1228:
	cmpl	$1, %eax
	movl	%eax, %r15d
	jne	.L1106
	movq	48(%rsp), %rdi
	call	__GI_wordfree
	movq	152(%rsp), %rdi
	call	free@PLT
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1100:
	call	parse_backslash
	movl	%eax, %r15d
	jmp	.L1101
	.p2align 4,,10
	.p2align 3
.L1107:
	movq	152(%rsp), %rdi
	testq	%rdi, %rdi
	je	.L1112
	call	free@PLT
	movq	$0, 144(%rsp)
	movq	$0, 136(%rsp)
	movq	$0, 152(%rsp)
.L1112:
	cmpq	$0, 192(%rsp)
	je	.L1113
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	16(%rsp), %rbp
	jmp	.L1116
	.p2align 4,,10
	.p2align 3
.L1114:
	movq	%rax, %rsi
	movq	%rbp, %rdi
	call	w_addword
	testl	%eax, %eax
	jne	.L1115
	leal	1(%rbx), %eax
	cmpq	192(%rsp), %rax
	movq	%rax, %rbx
	jnb	.L1113
.L1116:
	movq	200(%rsp), %rdx
	movq	(%rdx,%rax,8), %rdi
	call	__GI___strdup
	testq	%rax, %rax
	jne	.L1114
.L1115:
	movq	72(%rsp), %rdi
	movl	$1, %r15d
	call	__GI_globfree
	movq	48(%rsp), %rdi
	call	__GI_wordfree
	movq	152(%rsp), %rdi
	call	free@PLT
	jmp	.L1043
	.p2align 4,,10
	.p2align 3
.L1113:
	movq	%r12, %rdi
	addq	$1, %r14
	addq	$8, %r13
	call	__GI_globfree
	cmpq	%r14, 160(%rsp)
	ja	.L1117
	movq	40(%rsp), %rbx
	movq	32(%rsp), %r12
	movq	%r15, %rbp
.L1118:
	movq	48(%rsp), %rdi
	call	__GI_wordfree
	jmp	.L1074
	.p2align 4,,10
	.p2align 3
.L1130:
	movl	$1, %r13d
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1222:
	leaq	.LC22(%rip), %rdi
	movl	%r13d, %esi
	call	__GI_strchr
	testq	%rax, %rax
	jne	.L1121
	movl	%r13d, %ecx
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	w_addchar
	testq	%rax, %rax
	movq	%rax, 152(%rsp)
	jne	.L1074
	jmp	.L1210
	.p2align 4,,10
	.p2align 3
.L1226:
	xorl	%eax, %eax
	cmpl	$2, %r13d
	movq	%rbx, %rcx
	sete	%al
	pushq	%rax
	pushq	32(%rsp)
	pushq	24(%rsp)
	pushq	72(%rsp)
	movl	120(%rsp), %r9d
	movq	88(%rsp), %r8
	movq	64(%rsp), %rdx
	movq	72(%rsp), %rsi
	movq	96(%rsp), %rdi
	call	parse_dollars
	addq	$32, %rsp
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L1099
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1132:
	movl	$2, %r13d
	jmp	.L1095
	.p2align 4,,10
	.p2align 3
.L1044:
	movq	16(%rsp), %rdi
	call	__GI_wordfree
	movq	$0, 96(%rsp)
	jmp	.L1045
	.p2align 4,,10
	.p2align 3
.L1052:
	leaq	124(%rsp), %rax
	movl	$657696, 124(%rsp)
	movq	%rax, 24(%rsp)
	movq	%rax, 8(%rsp)
	jmp	.L1055
.L1213:
	leaq	124(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	%rax, %rcx
	jmp	.L1054
.L1221:
	movq	16(%rsp), %rdi
	call	__GI_wordfree
	jmp	.L1126
.L1058:
	jne	.L1056
	jmp	.L1059
	.p2align 4,,10
	.p2align 3
.L1051:
	movq	152(%rsp), %rdi
	movl	$1, %r15d
	call	free@PLT
	jmp	.L1043
.L1223:
	leaq	160(%rsp), %rax
	movq	%rax, 48(%rsp)
	jmp	.L1093
.L1227:
	movq	40(%rsp), %rbx
	movq	%r15, %rbp
	jmp	.L1118
.L1121:
	movq	%r14, %rdi
	movl	$2, %r15d
	call	free@PLT
	jmp	.L1128
.L1229:
	leaq	__PRETTY_FUNCTION__.11150(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC20(%rip), %rdi
	movl	$390, %edx
	call	__GI___assert_fail
.L1106:
	leaq	__PRETTY_FUNCTION__.11150(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC19(%rip), %rdi
	movl	$383, %edx
	call	__GI___assert_fail
.LFE122:
	.size	wordexp, .-wordexp
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.11053, @object
	.size	__PRETTY_FUNCTION__.11053, 10
__PRETTY_FUNCTION__.11053:
	.string	"w_addchar"
	.align 8
	.type	__PRETTY_FUNCTION__.11150, @object
	.size	__PRETTY_FUNCTION__.11150, 14
__PRETTY_FUNCTION__.11150:
	.string	"do_parse_glob"
	.align 8
	.type	__PRETTY_FUNCTION__.11564, @object
	.size	__PRETTY_FUNCTION__.11564, 12
__PRETTY_FUNCTION__.11564:
	.string	"parse_param"
	.align 8
	.type	__PRETTY_FUNCTION__.11070, @object
	.size	__PRETTY_FUNCTION__.11070, 9
__PRETTY_FUNCTION__.11070:
	.string	"w_addstr"
	.align 8
	.type	__PRETTY_FUNCTION__.11062, @object
	.size	__PRETTY_FUNCTION__.11062, 9
__PRETTY_FUNCTION__.11062:
	.string	"w_addmem"
	.hidden	__fxprintf
	.hidden	__libc_argv
	.hidden	__setenv
	.hidden	__libc_argc
	.hidden	_itoa_word
	.hidden	__posix_spawn_file_actions_addopen
	.hidden	__posix_spawn_file_actions_addclose
	.hidden	__posix_spawn_file_actions_destroy
	.hidden	__posix_spawn_file_actions_adddup2
	.hidden	__posix_spawn_file_actions_init
	.hidden	__pipe2
	.hidden	__getpwuid_r
	.hidden	__getuid
	.hidden	__getpwnam_r
