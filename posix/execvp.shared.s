	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_execvp
	.hidden	__GI_execvp
	.type	__GI_execvp, @function
__GI_execvp:
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %rdx
	jmp	__execvpe
	.size	__GI_execvp, .-__GI_execvp
	.globl	execvp
	.set	execvp,__GI_execvp
	.hidden	__execvpe
