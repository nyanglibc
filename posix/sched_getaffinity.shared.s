	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __sched_getaffinity_new,sched_getaffinity@@GLIBC_2.3.4
	.symver __sched_getaffinity_old,sched_getaffinity@GLIBC_2.3.3
#NO_APP
	.p2align 4,,15
	.globl	__GI___sched_getaffinity_new
	.hidden	__GI___sched_getaffinity_new
	.type	__GI___sched_getaffinity_new, @function
__GI___sched_getaffinity_new:
	movq	%rsi, %r9
	cmpq	$2147483647, %rsi
	movl	$204, %ecx
	movl	$2147483647, %esi
	movq	%rdx, %r10
	movl	%ecx, %eax
	cmovbe	%r9, %rsi
#APP
# 35 "../sysdeps/unix/sysv/linux/sched_getaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %r8
	ja	.L9
	cmpl	$-1, %eax
	je	.L5
	movslq	%eax, %r8
	movq	%r9, %rdx
	subq	$8, %rsp
	leaq	(%r10,%r8), %rdi
	subq	%r8, %rdx
	xorl	%esi, %esi
	call	__GI_memset@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%r8d
	movl	%r8d, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__GI___sched_getaffinity_new, .-__GI___sched_getaffinity_new
	.globl	__sched_getaffinity_new
	.set	__sched_getaffinity_new,__GI___sched_getaffinity_new
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__sched_getaffinity_old
	.type	__sched_getaffinity_old, @function
__sched_getaffinity_old:
	movq	%rsi, %rdx
	movl	$128, %esi
	jmp	__GI___sched_getaffinity_new
	.size	__sched_getaffinity_old, .-__sched_getaffinity_old
