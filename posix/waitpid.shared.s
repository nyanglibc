	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___waitpid
	.hidden	__GI___waitpid
	.type	__GI___waitpid, @function
__GI___waitpid:
	xorl	%ecx, %ecx
	jmp	__GI___wait4
	.size	__GI___waitpid, .-__GI___waitpid
	.globl	__waitpid
	.set	__waitpid,__GI___waitpid
	.weak	waitpid
	.set	waitpid,__waitpid
