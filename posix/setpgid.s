.text
.globl __setpgid
.type __setpgid,@function
.align 1<<4
__setpgid:
	movl $109, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __setpgid,.-__setpgid
.weak setpgid
setpgid = __setpgid
