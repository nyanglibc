	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_pread64
	.hidden	__libc_pread64
	.type	__libc_pread64, @function
__libc_pread64:
	movq	%rcx, %r10
#APP
# 25 "../sysdeps/unix/sysv/linux/pread64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$17, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/pread64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__libc_enable_asynccancel
	movq	%r12, %r10
	movl	%eax, %r8d
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$17, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/pread64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_pread64, .-__libc_pread64
	.weak	pread
	.set	pread,__libc_pread64
	.weak	__GI___pread
	.hidden	__GI___pread
	.set	__GI___pread,__libc_pread64
	.globl	__libc_pread
	.set	__libc_pread,__libc_pread64
	.weak	pread64
	.set	pread64,__libc_pread64
	.weak	__GI___pread64
	.hidden	__GI___pread64
	.set	__GI___pread64,__libc_pread64
	.weak	__pread64
	.set	__pread64,__GI___pread64
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
