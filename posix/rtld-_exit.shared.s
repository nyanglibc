	.text
	.p2align 4,,15
	.globl	__GI__exit
	.hidden	__GI__exit
	.type	__GI__exit, @function
__GI__exit:
	movl	%edi, %edx
	movl	$231, %r8d
	movl	$60, %esi
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edx, %edi
	movl	%esi, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/_exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L6
.L3:
#APP
# 34 "../sysdeps/unix/sysv/linux/_exit.c" 1
	hlt
# 0 "" 2
#NO_APP
.L4:
	movl	%edx, %edi
	movl	%r8d, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/_exit.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L2
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	negl	%eax
	movl	%eax, rtld_errno(%rip)
	jmp	.L3
	.size	__GI__exit, .-__GI__exit
	.globl	_exit
	.set	_exit,__GI__exit
	.weak	_Exit
	.set	_Exit,_exit
	.hidden	rtld_errno
