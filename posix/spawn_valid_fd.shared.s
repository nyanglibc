	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__spawn_valid_fd
	.hidden	__spawn_valid_fd
	.type	__spawn_valid_fd, @function
__spawn_valid_fd:
	pushq	%rbx
	movslq	%edi, %rbx
	movl	$4, %edi
	call	__GI___sysconf
	testl	%ebx, %ebx
	js	.L3
	cmpq	%rbx, %rax
	seta	%al
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__spawn_valid_fd, .-__spawn_valid_fd
