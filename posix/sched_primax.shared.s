.text
.globl __sched_get_priority_max
.type __sched_get_priority_max,@function
.align 1<<4
__sched_get_priority_max:
	movl $146, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_get_priority_max,.-__sched_get_priority_max
.globl __GI___sched_get_priority_max
.set __GI___sched_get_priority_max,__sched_get_priority_max
.weak sched_get_priority_max
sched_get_priority_max = __sched_get_priority_max
.globl __GI_sched_get_priority_max
.set __GI_sched_get_priority_max,sched_get_priority_max
