	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	posix_spawnattr_setschedpolicy
	.type	posix_spawnattr_setschedpolicy, @function
posix_spawnattr_setschedpolicy:
	cmpl	$2, %esi
	movl	$22, %eax
	ja	.L1
	movl	%esi, 268(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	posix_spawnattr_setschedpolicy, .-posix_spawnattr_setschedpolicy
