	.text
	.p2align 4,,15
	.globl	__nanosleep
	.hidden	__nanosleep
	.type	__nanosleep, @function
__nanosleep:
	subq	$8, %rsp
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	__clock_nanosleep
	testl	%eax, %eax
	jne	.L8
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
	.size	__nanosleep, .-__nanosleep
	.weak	nanosleep
	.set	nanosleep,__nanosleep
	.hidden	__clock_nanosleep
