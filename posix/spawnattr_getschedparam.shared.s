	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	posix_spawnattr_getschedparam
	.type	posix_spawnattr_getschedparam, @function
posix_spawnattr_getschedparam:
	movl	264(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	posix_spawnattr_getschedparam, .-posix_spawnattr_getschedparam
