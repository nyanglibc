.text
.globl getpgrp
.type getpgrp,@function
.align 1<<4
getpgrp:
	movl $111, %eax
	syscall
	ret
.size getpgrp,.-getpgrp
.globl __GI_getpgrp
.set __GI_getpgrp,getpgrp
