	.text
	.p2align 4,,15
	.globl	__sched_getaffinity_new
	.type	__sched_getaffinity_new, @function
__sched_getaffinity_new:
	movq	%rsi, %r9
	cmpq	$2147483647, %rsi
	movl	$204, %ecx
	movl	$2147483647, %esi
	movq	%rdx, %r10
	movl	%ecx, %eax
	cmovbe	%r9, %rsi
#APP
# 35 "../sysdeps/unix/sysv/linux/sched_getaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movq	%rax, %r8
	ja	.L9
	cmpl	$-1, %eax
	je	.L5
	movslq	%eax, %r8
	movq	%r9, %rdx
	subq	$8, %rsp
	leaq	(%r10,%r8), %rdi
	subq	%r8, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	negl	%r8d
	movl	%r8d, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__sched_getaffinity_new, .-__sched_getaffinity_new
	.weak	sched_getaffinity
	.set	sched_getaffinity,__sched_getaffinity_new
