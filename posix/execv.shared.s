	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	execv
	.type	execv, @function
execv:
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %rdx
	jmp	__execve
	.size	execv, .-execv
	.hidden	__execve
