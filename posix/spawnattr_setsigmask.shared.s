	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawnattr_setsigmask
	.hidden	__posix_spawnattr_setsigmask
	.type	__posix_spawnattr_setsigmask, @function
__posix_spawnattr_setsigmask:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, 136(%rdi)
	movdqu	16(%rsi), %xmm0
	movups	%xmm0, 152(%rdi)
	movdqu	32(%rsi), %xmm0
	movups	%xmm0, 168(%rdi)
	movdqu	48(%rsi), %xmm0
	movups	%xmm0, 184(%rdi)
	movdqu	64(%rsi), %xmm0
	movups	%xmm0, 200(%rdi)
	movdqu	80(%rsi), %xmm0
	movups	%xmm0, 216(%rdi)
	movdqu	96(%rsi), %xmm0
	movups	%xmm0, 232(%rdi)
	movdqu	112(%rsi), %xmm0
	movups	%xmm0, 248(%rdi)
	ret
	.size	__posix_spawnattr_setsigmask, .-__posix_spawnattr_setsigmask
	.weak	posix_spawnattr_setsigmask
	.set	posix_spawnattr_setsigmask,__posix_spawnattr_setsigmask
