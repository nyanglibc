	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	posix_spawnattr_getsigmask
	.type	posix_spawnattr_getsigmask, @function
posix_spawnattr_getsigmask:
	movdqu	136(%rdi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rsi)
	movdqu	152(%rdi), %xmm0
	movups	%xmm0, 16(%rsi)
	movdqu	168(%rdi), %xmm0
	movups	%xmm0, 32(%rsi)
	movdqu	184(%rdi), %xmm0
	movups	%xmm0, 48(%rsi)
	movdqu	200(%rdi), %xmm0
	movups	%xmm0, 64(%rsi)
	movdqu	216(%rdi), %xmm0
	movups	%xmm0, 80(%rsi)
	movdqu	232(%rdi), %xmm0
	movups	%xmm0, 96(%rsi)
	movdqu	248(%rdi), %xmm0
	movups	%xmm0, 112(%rsi)
	ret
	.size	posix_spawnattr_getsigmask, .-posix_spawnattr_getsigmask
