	.text
	.p2align 4,,15
	.globl	__posix_spawnp
	.type	__posix_spawnp, @function
__posix_spawnp:
	subq	$16, %rsp
	pushq	$1
	call	__spawni
	addq	$24, %rsp
	ret
	.size	__posix_spawnp, .-__posix_spawnp
	.weak	posix_spawnp
	.set	posix_spawnp,__posix_spawnp
	.hidden	__spawni
