	.text
	.p2align 4,,15
	.globl	__posix_spawn_file_actions_addclose
	.hidden	__posix_spawn_file_actions_addclose
	.type	__posix_spawn_file_actions_addclose, @function
__posix_spawn_file_actions_addclose:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%esi, %edi
	movl	%esi, %ebp
	subq	$8, %rsp
	call	__spawn_valid_fd
	testb	%al, %al
	movl	$9, %edx
	je	.L1
	movl	4(%rbx), %edx
	cmpl	(%rbx), %edx
	je	.L9
.L3:
	movslq	%edx, %rax
	addl	$1, %edx
	salq	$5, %rax
	addq	8(%rbx), %rax
	movl	$0, (%rax)
	movl	%ebp, 8(%rax)
	movl	%edx, 4(%rbx)
	xorl	%edx, %edx
.L1:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	%rbx, %rdi
	call	__posix_spawn_file_actions_realloc
	testl	%eax, %eax
	movl	$12, %edx
	jne	.L1
	movl	4(%rbx), %edx
	jmp	.L3
	.size	__posix_spawn_file_actions_addclose, .-__posix_spawn_file_actions_addclose
	.weak	posix_spawn_file_actions_addclose
	.set	posix_spawn_file_actions_addclose,__posix_spawn_file_actions_addclose
	.hidden	__posix_spawn_file_actions_realloc
	.hidden	__spawn_valid_fd
