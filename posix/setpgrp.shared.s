	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	setpgrp
	.type	setpgrp, @function
setpgrp:
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	__GI___setpgid
	.size	setpgrp, .-setpgrp
