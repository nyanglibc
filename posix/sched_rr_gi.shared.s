	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__sched_rr_get_interval
	.type	__sched_rr_get_interval, @function
__sched_rr_get_interval:
	movl	$148, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/sched_rr_gi.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__sched_rr_get_interval, .-__sched_rr_get_interval
	.globl	sched_rr_get_interval
	.set	sched_rr_get_interval,__sched_rr_get_interval
