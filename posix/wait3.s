	.text
	.p2align 4,,15
	.globl	__wait3
	.type	__wait3, @function
__wait3:
	movq	%rdx, %rcx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	$-1, %edi
	jmp	__wait4
	.size	__wait3, .-__wait3
	.weak	wait3
	.set	wait3,__wait3
	.hidden	__wait4
