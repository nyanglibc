.text
.globl __geteuid
.type __geteuid,@function
.align 1<<4
__geteuid:
	movl $107, %eax
	syscall
	ret
.size __geteuid,.-__geteuid
.weak geteuid
geteuid = __geteuid
