	.text
	.p2align 4,,15
	.globl	posix_spawn_file_actions_addchdir_np
	.type	posix_spawn_file_actions_addchdir_np, @function
posix_spawn_file_actions_addchdir_np:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rdi
	subq	$8, %rsp
	call	__strdup
	testq	%rax, %rax
	je	.L5
	movl	4(%rbx), %edx
	cmpl	(%rbx), %edx
	movq	%rax, %rbp
	je	.L8
.L3:
	movslq	%edx, %rax
	addl	$1, %edx
	salq	$5, %rax
	addq	8(%rbx), %rax
	movl	$3, (%rax)
	movq	%rbp, 8(%rax)
	xorl	%eax, %eax
	movl	%edx, 4(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rbx, %rdi
	call	__posix_spawn_file_actions_realloc
	testl	%eax, %eax
	jne	.L4
	movl	4(%rbx), %edx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rbp, %rdi
	call	free@PLT
	movl	$12, %eax
	jmp	.L1
	.size	posix_spawn_file_actions_addchdir_np, .-posix_spawn_file_actions_addchdir_np
	.hidden	__posix_spawn_file_actions_realloc
	.hidden	__strdup
