	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___nanosleep
	.hidden	__GI___nanosleep
	.type	__GI___nanosleep, @function
__GI___nanosleep:
	subq	$8, %rsp
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	__GI___clock_nanosleep
	testl	%eax, %eax
	jne	.L8
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	__libc_errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L1
	.size	__GI___nanosleep, .-__GI___nanosleep
	.globl	__nanosleep
	.set	__nanosleep,__GI___nanosleep
	.weak	nanosleep
	.set	nanosleep,__nanosleep
