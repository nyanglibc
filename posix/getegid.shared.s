.text
.globl __getegid
.type __getegid,@function
.align 1<<4
__getegid:
	movl $108, %eax
	syscall
	ret
.size __getegid,.-__getegid
.globl __GI___getegid
.set __GI___getegid,__getegid
.weak getegid
getegid = __getegid
.globl __GI_getegid
.set __GI_getegid,getegid
