.text
.globl __uname
.type __uname,@function
.align 1<<4
__uname:
	movl $63, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __uname,.-__uname
.weak uname
uname = __uname
