.text
.globl __getuid
.type __getuid,@function
.align 1<<4
__getuid:
	movl $102, %eax
	syscall
	ret
.size __getuid,.-__getuid
.weak getuid
getuid = __getuid
