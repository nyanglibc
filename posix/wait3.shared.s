	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__wait3
	.type	__wait3, @function
__wait3:
	movq	%rdx, %rcx
	movl	%esi, %edx
	movq	%rdi, %rsi
	movl	$-1, %edi
	jmp	__GI___wait4
	.size	__wait3, .-__wait3
	.weak	wait3
	.set	wait3,__wait3
