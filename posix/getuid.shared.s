.text
.globl __getuid
.type __getuid,@function
.align 1<<4
__getuid:
	movl $102, %eax
	syscall
	ret
.size __getuid,.-__getuid
.globl __GI___getuid
.set __GI___getuid,__getuid
.weak getuid
getuid = __getuid
.globl __GI_getuid
.set __GI_getuid,getuid
