	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawnattr_setsigdefault
	.hidden	__posix_spawnattr_setsigdefault
	.type	__posix_spawnattr_setsigdefault, @function
__posix_spawnattr_setsigdefault:
	movdqu	(%rsi), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, 8(%rdi)
	movdqu	16(%rsi), %xmm0
	movups	%xmm0, 24(%rdi)
	movdqu	32(%rsi), %xmm0
	movups	%xmm0, 40(%rdi)
	movdqu	48(%rsi), %xmm0
	movups	%xmm0, 56(%rdi)
	movdqu	64(%rsi), %xmm0
	movups	%xmm0, 72(%rdi)
	movdqu	80(%rsi), %xmm0
	movups	%xmm0, 88(%rdi)
	movdqu	96(%rsi), %xmm0
	movups	%xmm0, 104(%rdi)
	movdqu	112(%rsi), %xmm0
	movups	%xmm0, 120(%rdi)
	ret
	.size	__posix_spawnattr_setsigdefault, .-__posix_spawnattr_setsigdefault
	.weak	posix_spawnattr_setsigdefault
	.set	posix_spawnattr_setsigdefault,__posix_spawnattr_setsigdefault
