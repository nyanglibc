	.text
	.p2align 4,,15
	.globl	__fpathconf
	.type	__fpathconf, @function
__fpathconf:
.LFB4:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$144, %rsp
	cmpl	$6, %esi
	je	.L3
	jle	.L40
	cmpl	$13, %esi
	je	.L6
	cmpl	$20, %esi
	jne	.L2
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__fstatfs
	movq	%rbp, %rsi
	movl	%eax, %edi
	call	__statfs_symlinks
.L1:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__fstatfs
	movq	%rbp, %rsi
	movl	%eax, %edi
	call	__statfs_filesize_max
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	testl	%esi, %esi
	jne	.L2
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__fstatfs
	movl	%ebx, %ecx
	movq	%rbp, %rsi
	xorl	%edx, %edx
	movl	%eax, %edi
	call	__statfs_link_max
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%ebx, %ebx
	js	.L41
	cmpl	$20, %esi
	ja	.L10
	leaq	.L11(%rip), %rdx
	movl	%esi, %esi
	movslq	(%rdx,%rsi,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L11:
	.long	.L29-.L11
	.long	.L37-.L11
	.long	.L37-.L11
	.long	.L13-.L11
	.long	.L14-.L11
	.long	.L14-.L11
	.long	.L10-.L11
	.long	.L15-.L11
	.long	.L16-.L11
	.long	.L29-.L11
	.long	.L17-.L11
	.long	.L29-.L11
	.long	.L29-.L11
	.long	.L18-.L11
	.long	.L29-.L11
	.long	.L29-.L11
	.long	.L19-.L11
	.long	.L21-.L11
	.long	.L21-.L11
	.long	.L29-.L11
	.long	.L15-.L11
	.text
	.p2align 4,,10
	.p2align 3
.L3:
	movq	%rsp, %rbp
	movq	%rbp, %rsi
	call	__fstatfs
	movq	%rbp, %rsi
	movl	%eax, %edi
	call	__statfs_chown_restricted
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L43:
	movl	%r12d, %fs:0(%rbp)
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$255, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L21:
	movq	%rsp, %rsi
	movl	%ebx, %edi
	call	__fstatvfs64
	movl	%eax, %edx
	movq	$-1, %rax
	testl	%edx, %edx
	js	.L1
	movq	8(%rsp), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	$4096, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	movq	$-1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rsp, %rsi
	movl	%ebx, %edi
	call	__fstat64
	movl	%eax, %edx
	movq	$-1, %rax
	testl	%edx, %edx
	js	.L1
	movl	24(%rsp), %eax
	andl	$61440, %eax
	subl	$24576, %eax
	andb	$-33, %ah
	cmpl	$1, %eax
	sbbq	%rax, %rax
	andl	$2, %eax
	subq	$1, %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%rsp, %rsi
	movl	%ebx, %edi
	call	__fstatvfs64
	movl	%eax, %edx
	movq	$-1, %rax
	testl	%edx, %edx
	js	.L1
	movq	(%rsp), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__libc_errno@gottpoff(%rip), %rbp
	movq	%rsp, %rsi
	movl	%ebx, %edi
	movl	%fs:0(%rbp), %r12d
	call	__fstatvfs64
	testl	%eax, %eax
	js	.L42
	movq	80(%rsp), %rax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$32, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movq	$-1, %rax
	jmp	.L1
.L41:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movq	$-1, %rax
	jmp	.L1
.L42:
	movl	%fs:0(%rbp), %edx
	cmpl	$38, %edx
	je	.L43
	cmpl	$19, %edx
	movq	$-1, %rax
	jne	.L1
	movl	$22, %fs:0(%rbp)
	jmp	.L1
.LFE4:
	.size	__fpathconf, .-__fpathconf
	.weak	fpathconf
	.set	fpathconf,__fpathconf
	.hidden	__fstat64
	.hidden	__fstatvfs64
	.hidden	__statfs_chown_restricted
	.hidden	__statfs_link_max
	.hidden	__statfs_filesize_max
	.hidden	__statfs_symlinks
	.hidden	__fstatfs
