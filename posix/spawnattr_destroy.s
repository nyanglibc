	.text
	.p2align 4,,15
	.globl	__posix_spawnattr_destroy
	.hidden	__posix_spawnattr_destroy
	.type	__posix_spawnattr_destroy, @function
__posix_spawnattr_destroy:
	xorl	%eax, %eax
	ret
	.size	__posix_spawnattr_destroy, .-__posix_spawnattr_destroy
	.weak	posix_spawnattr_destroy
	.set	posix_spawnattr_destroy,__posix_spawnattr_destroy
