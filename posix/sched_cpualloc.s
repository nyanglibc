	.text
	.p2align 4,,15
	.globl	__sched_cpualloc
	.type	__sched_cpualloc, @function
__sched_cpualloc:
	addq	$63, %rdi
	shrq	$6, %rdi
	salq	$3, %rdi
	jmp	malloc@PLT
	.size	__sched_cpualloc, .-__sched_cpualloc
