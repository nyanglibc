	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_execlp
	.hidden	__GI_execlp
	.type	__GI_execlp, @function
__GI_execlp:
	pushq	%rbp
	movq	%rsi, %r10
	movq	%rsp, %rbp
	pushq	%rbx
	leaq	16(%rbp), %rax
	leaq	16(%rbp), %rsi
	subq	$88, %rsp
	movq	%rdx, -48(%rbp)
	movq	%r8, -32(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rcx, -40(%rbp)
	movq	%r9, -24(%rbp)
	movl	$16, -88(%rbp)
	movl	$16, %edx
	movq	%rax, -72(%rbp)
	movq	%rax, %r8
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$47, %edx
	ja	.L5
	movl	%edx, %ecx
	addl	$8, %edx
	addq	%r8, %rcx
.L6:
	cmpq	$0, (%rcx)
	je	.L22
	cmpq	$2147483647, %rax
	je	.L23
	addq	$1, %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L22:
	leaq	8(,%rax,8), %rcx
	movq	%rsp, %rbx
	leaq	16(%rbp), %rdx
	movl	$16, -88(%rbp)
	movl	$16, %r8d
	xorl	%r9d, %r9d
	leaq	22(%rcx), %rax
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	andq	$-16, %rax
	movq	%rdx, -72(%rbp)
	movq	%rdx, %r11
	subq	%rax, %rsp
	leaq	7(%rsp), %rax
	shrq	$3, %rax
	leaq	0(,%rax,8), %rsi
	movq	%r10, 0(,%rax,8)
	leaq	16(%rbp), %r10
	leaq	8(%rsi), %rax
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$47, %r8d
	ja	.L9
	movl	%r8d, %edx
	movl	$1, %r9d
	addl	$8, %r8d
	addq	%r11, %rdx
.L10:
	movq	(%rdx), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	cmpq	%rcx, %rax
	jne	.L11
	testb	%r9b, %r9b
	jne	.L24
.L12:
	movq	__environ@GOTPCREL(%rip), %rax
	movq	(%rax), %rdx
	call	__execvpe
	movq	%rbx, %rsp
	movq	-8(%rbp), %rbx
	leave
	ret
.L23:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	-8(%rbp), %rbx
	movl	$7, %fs:(%rax)
	movl	$-1, %eax
	leave
	ret
.L9:
	movq	%r10, %rdx
	addq	$8, %r10
	jmp	.L10
.L5:
	movq	%rsi, %rcx
	addq	$8, %rsi
	jmp	.L6
.L24:
	movl	%r8d, -88(%rbp)
	jmp	.L12
	.size	__GI_execlp, .-__GI_execlp
	.globl	execlp
	.set	execlp,__GI_execlp
	.hidden	__execvpe
