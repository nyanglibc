.text
.globl __sched_setscheduler
.type __sched_setscheduler,@function
.align 1<<4
__sched_setscheduler:
	movl $144, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __sched_setscheduler,.-__sched_setscheduler
.weak sched_setscheduler
sched_setscheduler = __sched_setscheduler
