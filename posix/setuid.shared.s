	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__setuid
	.type	__setuid, @function
__setuid:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	jne	.L11
	movl	$105, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/setuid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$56, %rsp
	movl	%edi, %eax
	movq	%rax, 8(%rsp)
	movl	$105, (%rsp)
	movq	%rsp, %rdi
	movq	224+__libc_pthread_functions(%rip), %rax
#APP
# 28 "../sysdeps/unix/sysv/linux/setuid.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__setuid, .-__setuid
	.weak	setuid
	.set	setuid,__setuid
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
