	.text
	.p2align 4,,15
	.globl	setpgrp
	.type	setpgrp, @function
setpgrp:
	xorl	%esi, %esi
	xorl	%edi, %edi
	jmp	__setpgid
	.size	setpgrp, .-setpgrp
	.hidden	__setpgid
