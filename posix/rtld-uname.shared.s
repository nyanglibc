.text
.globl __uname
.type __uname,@function
.align 1<<4
__uname:
	movl $63, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	lea rtld_errno(%rip), %rcx
	neg %eax
	movl %eax, (%rcx)
	or $-1, %rax
	ret
.size __uname,.-__uname
.globl __GI___uname
.set __GI___uname,__uname
.weak uname
uname = __uname
.globl __GI_uname
.set __GI_uname,uname
