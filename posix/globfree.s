	.text
	.p2align 4,,15
	.globl	globfree
	.hidden	globfree
	.type	globfree, @function
globfree:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L1
	cmpq	$0, (%rbx)
	je	.L3
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L5:
	movq	16(%rbx), %rax
	addq	%rbp, %rax
	addq	$1, %rbp
	movq	(%rdi,%rax,8), %rdi
	call	free@PLT
	cmpq	(%rbx), %rbp
	movq	8(%rbx), %rdi
	jb	.L5
.L3:
	call	free@PLT
	movq	$0, 8(%rbx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.size	globfree, .-globfree
	.weak	globfree64
	.set	globfree64,globfree
