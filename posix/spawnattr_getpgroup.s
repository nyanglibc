	.text
	.p2align 4,,15
	.globl	posix_spawnattr_getpgroup
	.type	posix_spawnattr_getpgroup, @function
posix_spawnattr_getpgroup:
	movl	4(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	posix_spawnattr_getpgroup, .-posix_spawnattr_getpgroup
