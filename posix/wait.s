	.text
	.p2align 4,,15
	.globl	__wait
	.type	__wait, @function
__wait:
	movq	%rdi, %rsi
	xorl	%edx, %edx
	movl	$-1, %edi
	jmp	__waitpid
	.size	__wait, .-__wait
	.weak	wait
	.set	wait,__wait
	.hidden	__waitpid
