	.text
	.p2align 4,,15
	.globl	__setresuid
	.hidden	__setresuid
	.type	__setresuid, @function
__setresuid:
	cmpq	$0, __nptl_setxid@GOTPCREL(%rip)
	jne	.L11
	movl	$117, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/setresuid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$56, %rsp
	movl	%edi, %eax
	movq	%rax, 8(%rsp)
	movl	%esi, %eax
	movq	%rsp, %rdi
	movq	%rax, 16(%rsp)
	movl	%edx, %eax
	movl	$117, (%rsp)
	movq	%rax, 24(%rsp)
	call	__nptl_setxid@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__setresuid, .-__setresuid
	.weak	setresuid
	.set	setresuid,__setresuid
	.weak	__nptl_setxid
	.hidden	__nptl_setxid
