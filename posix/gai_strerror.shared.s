	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"Unknown error"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__GI_gai_strerror
	.hidden	__GI_gai_strerror
	.type	__GI_gai_strerror, @function
__GI_gai_strerror:
	movl	$-9, %eax
	xorl	%edx, %edx
	leaq	msgidx(%rip), %rcx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$1, %rdx
	cmpq	$17, %rdx
	je	.L5
	movswl	(%rcx,%rdx,4), %eax
.L4:
	cmpl	%edi, %eax
	jne	.L2
	leaq	msgidx(%rip), %rax
	movzwl	2(%rax,%rdx,4), %esi
	leaq	msgstr(%rip), %rax
	addq	%rax, %rsi
.L3:
	leaq	__GI__libc_intl_domainname(%rip), %rdi
	movl	$5, %edx
	jmp	__GI___dcgettext
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	.LC0(%rip), %rsi
	jmp	.L3
	.size	__GI_gai_strerror, .-__GI_gai_strerror
	.globl	gai_strerror
	.set	gai_strerror,__GI_gai_strerror
	.section	.rodata
	.align 32
	.type	msgidx, @object
	.size	msgidx, 68
msgidx:
	.value	-9
	.value	0
	.value	-3
	.value	42
	.value	-1
	.value	79
	.value	-4
	.value	102
	.value	-6
	.value	145
	.value	-10
	.value	169
	.value	-5
	.value	195
	.value	-2
	.value	231
	.value	-8
	.value	257
	.value	-7
	.value	296
	.value	-11
	.value	322
	.value	-100
	.value	335
	.value	-101
	.value	366
	.value	-102
	.value	383
	.value	-103
	.value	404
	.value	-104
	.value	422
	.value	-105
	.value	446
	.align 32
	.type	msgstr, @object
	.size	msgstr, 485
msgstr:
	.string	"Address family for hostname not supported"
	.string	"Temporary failure in name resolution"
	.string	"Bad value for ai_flags"
	.string	"Non-recoverable failure in name resolution"
	.string	"ai_family not supported"
	.string	"Memory allocation failure"
	.string	"No address associated with hostname"
	.string	"Name or service not known"
	.string	"Servname not supported for ai_socktype"
	.string	"ai_socktype not supported"
	.string	"System error"
	.string	"Processing request in progress"
	.string	"Request canceled"
	.string	"Request not canceled"
	.string	"All requests done"
	.string	"Interrupted by a signal"
	.string	"Parameter string not correctly encoded"
