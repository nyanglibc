.globl __vfork
.type __vfork,@function
.align 1<<4
__vfork:
	popq %rdi
	movl $58, %eax
	syscall
	pushq %rdi
	cmpl $-4095, %eax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __vfork,.-__vfork
.globl __GI___vfork
.set __GI___vfork,__vfork
.weak vfork
vfork = __vfork
.globl __libc_vfork
.set __libc_vfork,__vfork
