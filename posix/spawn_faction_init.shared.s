	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawn_file_actions_realloc
	.hidden	__posix_spawn_file_actions_realloc
	.type	__posix_spawn_file_actions_realloc, @function
__posix_spawn_file_actions_realloc:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$8, %rsp
	movl	(%rdi), %eax
	movq	8(%rdi), %rdi
	leal	8(%rax), %ebx
	movslq	%ebx, %rsi
	salq	$5, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L3
	movq	%rax, 8(%rbp)
	movl	%ebx, 0(%rbp)
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$8, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__posix_spawn_file_actions_realloc, .-__posix_spawn_file_actions_realloc
	.p2align 4,,15
	.globl	__posix_spawn_file_actions_init
	.hidden	__posix_spawn_file_actions_init
	.type	__posix_spawn_file_actions_init, @function
__posix_spawn_file_actions_init:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 64(%rdi)
	ret
	.size	__posix_spawn_file_actions_init, .-__posix_spawn_file_actions_init
	.weak	posix_spawn_file_actions_init
	.set	posix_spawn_file_actions_init,__posix_spawn_file_actions_init
