	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_execle
	.hidden	__GI_execle
	.type	__GI_execle, @function
__GI_execle:
	pushq	%rbp
	movq	%rsi, %r10
	movq	%rsp, %rbp
	pushq	%r12
	pushq	%rbx
	leaq	16(%rbp), %rax
	leaq	16(%rbp), %rsi
	subq	$80, %rsp
	movq	%rdx, -48(%rbp)
	movq	%r8, -32(%rbp)
	movq	%rax, -80(%rbp)
	leaq	-64(%rbp), %rax
	movq	%rcx, -40(%rbp)
	movq	%r9, -24(%rbp)
	movl	$16, -88(%rbp)
	movl	$16, %edx
	movq	%rax, -72(%rbp)
	movq	%rax, %r8
	movl	$1, %eax
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$47, %edx
	ja	.L5
	movl	%edx, %ecx
	addl	$8, %edx
	addq	%r8, %rcx
.L6:
	cmpq	$0, (%rcx)
	je	.L28
	cmpq	$2147483647, %rax
	je	.L29
	addq	$1, %rax
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	8(,%rax,8), %rcx
	movq	%rsp, %rbx
	leaq	16(%rbp), %rdx
	movl	$16, -88(%rbp)
	movl	$16, %r8d
	xorl	%r11d, %r11d
	leaq	22(%rcx), %rax
	movq	%rdx, -80(%rbp)
	leaq	-64(%rbp), %rdx
	xorl	%r12d, %r12d
	andq	$-16, %rax
	movq	%rdx, -72(%rbp)
	movq	%rdx, %r9
	subq	%rax, %rsp
	leaq	7(%rsp), %rax
	shrq	$3, %rax
	leaq	0(,%rax,8), %rsi
	movq	%r10, 0(,%rax,8)
	leaq	16(%rbp), %r10
	leaq	8(%rsi), %rax
	addq	%rsi, %rcx
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$47, %r8d
	ja	.L9
	movl	%r8d, %edx
	movl	$1, %r12d
	addl	$8, %r8d
	addq	%r9, %rdx
.L10:
	movq	(%rdx), %rdx
	addq	$8, %rax
	movq	%rdx, -8(%rax)
	cmpq	%rax, %rcx
	jne	.L11
	testb	%r12b, %r12b
	je	.L12
	movl	%r8d, -88(%rbp)
.L12:
	testb	%r11b, %r11b
	je	.L13
	movq	%r10, -80(%rbp)
.L13:
	movl	-88(%rbp), %eax
	cmpl	$47, %eax
	ja	.L14
	addq	%r9, %rax
.L15:
	movq	(%rax), %rdx
	call	__execve
	movq	%rbx, %rsp
	leaq	-16(%rbp), %rsp
	popq	%rbx
	popq	%r12
	popq	%rbp
	ret
.L29:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %fs:(%rax)
	leaq	-16(%rbp), %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%r12
	popq	%rbp
	ret
.L9:
	movq	%r10, %rdx
	movl	$1, %r11d
	addq	$8, %r10
	jmp	.L10
.L5:
	movq	%rsi, %rcx
	addq	$8, %rsi
	jmp	.L6
.L14:
	movq	-80(%rbp), %rax
	jmp	.L15
	.size	__GI_execle, .-__GI_execle
	.globl	execle
	.set	execle,__GI_execle
	.hidden	__execve
