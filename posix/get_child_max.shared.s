	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__get_child_max
	.hidden	__get_child_max
	.type	__get_child_max, @function
__get_child_max:
	subq	$24, %rsp
	movl	$6, %edi
	movq	%rsp, %rsi
	call	__GI___getrlimit
	testl	%eax, %eax
	jne	.L3
	movq	(%rsp), %rax
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	$-1, %rax
	jmp	.L1
	.size	__get_child_max, .-__get_child_max
