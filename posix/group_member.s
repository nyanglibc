	.text
	.p2align 4,,15
	.globl	__group_member
	.hidden	__group_member
	.type	__group_member, @function
__group_member:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r13
	pushq	%r12
	pushq	%rbx
	movl	%edi, %r13d
	movl	$65536, %ebx
	subq	$8, %rsp
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L6:
	movl	%edx, %ebx
.L2:
	movslq	%ebx, %rax
	movl	%ebx, %edi
	leaq	30(,%rax,4), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %r12
	andq	$-16, %r12
	movq	%r12, %rsi
	call	__getgroups
	cmpl	%eax, %ebx
	leal	(%rbx,%rbx), %edx
	je	.L6
	cltq
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	subq	$1, %rax
	cmpl	%r13d, (%r12,%rax,4)
	je	.L7
.L3:
	testl	%eax, %eax
	jg	.L5
	leaq	-24(%rbp), %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	-24(%rbp), %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%rbp
	ret
	.size	__group_member, .-__group_member
	.weak	group_member
	.set	group_member,__group_member
	.hidden	__getgroups
