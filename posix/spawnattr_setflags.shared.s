	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__posix_spawnattr_setflags
	.hidden	__posix_spawnattr_setflags
	.type	__posix_spawnattr_setflags, @function
__posix_spawnattr_setflags:
	testw	$-256, %si
	movl	$22, %eax
	jne	.L1
	movw	%si, (%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__posix_spawnattr_setflags, .-__posix_spawnattr_setflags
	.weak	posix_spawnattr_setflags
	.set	posix_spawnattr_setflags,__posix_spawnattr_setflags
