	.text
	.p2align 4,,15
	.type	__spawnix, @function
__spawnix:
.LFB69:
	pushq	%r15
	pushq	%r14
	movq	%rcx, %r14
	pushq	%r13
	pushq	%r12
	movq	%r8, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	xorl	%ebx, %ebx
	subq	$584, %rsp
	movq	%rsi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%r9, 24(%rsp)
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	cmpq	$2147483646, %rbx
	je	.L26
.L2:
	addq	$1, %rbx
	cmpq	$0, -8(%r13,%rbx,8)
	jne	.L4
	movq	_dl_pagesize(%rip), %rax
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$-1, %r8d
	movl	$131106, %ecx
	leaq	33279(%rax,%rbx,8), %rbp
	negq	%rax
	andq	%rax, %rbp
	movl	_dl_stack_flags(%rip), %eax
	movq	%rbp, %rsi
	leal	0(,%rax,4), %edx
	andl	$4, %edx
	orl	$3, %edx
	call	__mmap
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L27
	cmpq	$0, __pthread_setcancelstate@GOTPCREL(%rip)
	je	.L6
	leaq	44(%rsp), %rsi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
.L6:
	movq	8(%rsp), %rax
	testq	%r14, %r14
	movl	$0, 236(%rsp)
	movq	%rax, 176(%rsp)
	movq	648(%rsp), %rax
	movq	%rax, 184(%rsp)
	movq	16(%rsp), %rax
	movq	%rax, 192(%rsp)
	je	.L28
.L7:
	movq	24(%rsp), %rax
	movq	%rbx, 216(%rsp)
	leaq	48(%rsp), %rbx
	movq	%r14, 200(%rsp)
	movq	%r13, 208(%rsp)
	movl	$8, %r10d
	movq	%rbx, %rdx
	leaq	sigall_set(%rip), %rsi
	xorl	%edi, %edi
	movq	%rax, 224(%rsp)
	movl	640(%rsp), %eax
	movl	%eax, 232(%rsp)
	movl	$14, %eax
#APP
# 71 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	__spawni_child(%rip), %rdi
	leaq	(%r15,%rbp), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	movl	$16657, %edx
	call	__clone
	testl	%eax, %eax
	movl	%eax, %r14d
	jle	.L8
	movl	236(%rsp), %r13d
	testl	%r13d, %r13d
	jg	.L29
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	__munmap
	testl	%r13d, %r13d
	jne	.L10
.L30:
	testq	%r12, %r12
	je	.L10
	movl	%r14d, (%r12)
.L10:
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$0, __pthread_setcancelstate@GOTPCREL(%rip)
	je	.L1
	movl	44(%rsp), %edi
	xorl	%esi, %esi
	call	__pthread_setcancelstate@PLT
.L1:
	addq	$584, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	%eax, %edi
	call	__waitpid
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	__munmap
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%eax, %r13d
	movq	%rbp, %rsi
	movq	%r15, %rdi
	negl	%r13d
	call	__munmap
	testl	%r13d, %r13d
	je	.L30
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	240(%rsp), %rdi
	movq	%r14, %rax
	movl	$42, %ecx
	leaq	240(%rsp), %r14
	rep stosq
	jmp	.L7
.L26:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$7, %r13d
	movl	$7, %fs:(%rax)
	jmp	.L1
.L27:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r13d
	jmp	.L1
.LFE69:
	.size	__spawnix, .-__spawnix
	.p2align 4,,15
	.type	__spawni_child, @function
__spawni_child:
.LFB68:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r15
	pushq	%rbp
	pushq	%rbx
	movl	$19, %ecx
	xorl	%esi, %esi
	movl	$1, %ebx
	movl	$1, %r13d
	subq	$328, %rsp
	movq	152(%rdi), %rbp
	movq	144(%rdi), %r14
	leaq	160(%rsp), %r12
	leaq	32(%rsp), %rdx
	movq	%r12, %rdi
	rep stosq
	xorl	%edi, %edi
	call	__sigprocmask
	movzwl	0(%rbp), %edx
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L119:
	testq	%rax, 8(%rbp)
	je	.L32
.L36:
	movq	$0, 160(%rsp)
.L33:
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	__libc_sigaction
.L114:
	movzwl	0(%rbp), %edx
.L34:
	addl	$1, %ebx
	cmpl	$65, %ebx
	je	.L118
.L37:
	leal	-1(%rbx), %ecx
	movq	%r13, %rax
	salq	%cl, %rax
	testb	$4, %dl
	jne	.L119
.L32:
	testq	%rax, 32(%rsp)
	je	.L34
	leal	-32(%rbx), %eax
	cmpl	$1, %eax
	jbe	.L120
	xorl	%esi, %esi
	movq	%r12, %rdx
	movl	%ebx, %edi
	call	__libc_sigaction
	cmpq	$1, 160(%rsp)
	jne	.L36
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L118:
	movl	%edx, %eax
	andl	$48, %eax
	cmpw	$16, %ax
	je	.L121
	testb	$32, %dl
	jne	.L122
.L43:
	testb	$-128, %dl
	jne	.L123
.L41:
	testb	$2, %dl
	jne	.L124
.L45:
	andb	$1, %dl
	jne	.L125
.L47:
	testq	%r14, %r14
	je	.L49
	cmpl	$0, 4(%r14)
	jle	.L49
	leaq	16(%rsp), %rax
	leaq	.L55(%rip), %rbx
	xorl	%r13d, %r13d
	movb	$0, 3(%rsp)
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L63:
	movq	%r13, %rcx
	salq	$5, %rcx
	addq	8(%r14), %rcx
	cmpl	$4, (%rcx)
	movq	%rcx, %r12
	ja	.L53
	movl	(%rcx), %eax
	movslq	(%rbx,%rax,4), %rax
	addq	%rbx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L55:
	.long	.L54-.L55
	.long	.L56-.L55
	.long	.L57-.L55
	.long	.L58-.L55
	.long	.L59-.L55
	.text
	.p2align 4,,10
	.p2align 3
.L58:
	movq	8(%rcx), %rdi
	call	__chdir
	testl	%eax, %eax
	jne	.L116
	.p2align 4,,10
	.p2align 3
.L53:
	leal	1(%r13), %eax
	addq	$1, %r13
	cmpl	%eax, 4(%r14)
	jg	.L63
.L49:
	testb	$8, 0(%rbp)
	movq	%r15, %rsi
	jne	.L126
.L65:
	xorl	%edx, %edx
	movl	$2, %edi
	call	__sigprocmask
	movq	176(%r15), %rdx
	movq	160(%r15), %rsi
	movq	128(%r15), %rdi
	call	*136(%r15)
.L116:
	movq	__libc_errno@gottpoff(%rip), %rdx
.L42:
	movl	%fs:(%rdx), %eax
	movl	$10, %edx
	movl	$127, %edi
	testl	%eax, %eax
	cmove	%edx, %eax
	movl	%eax, 188(%r15)
	call	_exit
	.p2align 4,,10
	.p2align 3
.L57:
	movl	8(%rcx), %edi
	call	__close_nocancel
	movl	28(%r12), %edx
	movl	24(%r12), %esi
	xorl	%eax, %eax
	movq	16(%r12), %rdi
	call	__open_nocancel
	cmpl	$-1, %eax
	je	.L116
	movl	8(%r12), %esi
	cmpl	%eax, %esi
	je	.L53
	movl	%eax, %edi
	movl	%eax, 4(%rsp)
	call	__dup2
	cmpl	%eax, 8(%r12)
	movl	4(%rsp), %edx
	jne	.L116
	movl	%edx, %edi
	call	__close_nocancel
	testl	%eax, %eax
	je	.L53
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L54:
	movl	8(%rcx), %edi
	call	__close_nocancel
	testl	%eax, %eax
	je	.L53
	cmpb	$0, 3(%rsp)
	je	.L127
.L60:
	movslq	8(%r12), %rax
	testl	%eax, %eax
	js	.L116
	cmpq	16(%rsp), %rax
	jnb	.L116
	movb	$1, 3(%rsp)
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L59:
	movl	8(%rcx), %edi
	call	__fchdir
	testl	%eax, %eax
	je	.L53
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L56:
	movl	8(%rcx), %r9d
	movl	12(%rcx), %esi
	cmpl	%esi, %r9d
	je	.L128
	movl	%r9d, %edi
	call	__dup2
	cmpl	%eax, 12(%r12)
	je	.L53
	jmp	.L116
.L125:
	call	__getuid
	orl	$-1, %edx
	movl	%eax, %esi
	movl	$117, %eax
	movl	%edx, %edi
#APP
# 189 "../sysdeps/unix/sysv/linux/spawni.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L117
	testq	%rax, %rax
	jne	.L116
	call	__getgid
	orl	$-1, %edx
	movl	%eax, %esi
	movl	$119, %eax
	movl	%edx, %edi
#APP
# 190 "../sysdeps/unix/sysv/linux/spawni.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L117
	testq	%rax, %rax
	je	.L47
	jmp	.L116
.L128:
	xorl	%edx, %edx
	movl	%r9d, %edi
	xorl	%eax, %eax
	movl	$1, %esi
	movl	%r9d, 4(%rsp)
	call	__fcntl
	cmpl	$-1, %eax
	movl	4(%rsp), %r9d
	je	.L116
	andl	$-2, %eax
	movl	$2, %esi
	movl	%r9d, %edi
	movl	%eax, %edx
	xorl	%eax, %eax
	call	__fcntl
	addl	$1, %eax
	jne	.L53
	jmp	.L116
.L123:
	call	__setsid
	testl	%eax, %eax
	js	.L116
	movzwl	0(%rbp), %edx
	jmp	.L41
.L122:
	movl	268(%rbp), %esi
	leaq	264(%rbp), %rdx
	xorl	%edi, %edi
	call	__sched_setscheduler
	addl	$1, %eax
	je	.L116
.L115:
	movzwl	0(%rbp), %edx
	jmp	.L43
.L124:
	movl	4(%rbp), %esi
	xorl	%edi, %edi
	call	__setpgid
	testl	%eax, %eax
	jne	.L116
	movzwl	0(%rbp), %edx
	jmp	.L45
.L127:
	movq	8(%rsp), %rsi
	movl	$7, %edi
	call	__getrlimit64
	jmp	.L60
.L121:
	leaq	264(%rbp), %rsi
	xorl	%edi, %edi
	call	__sched_setparam
	addl	$1, %eax
	jne	.L115
	jmp	.L116
.L126:
	leaq	136(%rbp), %rsi
	jmp	.L65
.L117:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	jmp	.L42
.L120:
	movq	%r13, 160(%rsp)
	jmp	.L33
.LFE68:
	.size	__spawni_child, .-__spawni_child
	.p2align 4,,15
	.globl	__spawni
	.hidden	__spawni
	.type	__spawni, @function
__spawni:
.LFB70:
	subq	$8, %rsp
	leaq	__execve(%rip), %r11
	leaq	__execvpex(%rip), %rax
	movl	16(%rsp), %r10d
	testb	$1, %r10b
	cmove	%r11, %rax
	pushq	%rax
	pushq	%r10
	call	__spawnix
	addq	$24, %rsp
	ret
.LFE70:
	.size	__spawni, .-__spawni
	.section	.rodata
	.align 32
	.type	sigall_set, @object
	.size	sigall_set, 128
sigall_set:
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.weak	__pthread_setcancelstate
	.hidden	__execvpex
	.hidden	__execve
	.hidden	__sched_setparam
	.hidden	__getrlimit64
	.hidden	__setpgid
	.hidden	__sched_setscheduler
	.hidden	__setsid
	.hidden	__fcntl
	.hidden	__getgid
	.hidden	__getuid
	.hidden	__fchdir
	.hidden	__dup2
	.hidden	__open_nocancel
	.hidden	__close_nocancel
	.hidden	_exit
	.hidden	__chdir
	.hidden	__libc_sigaction
	.hidden	__sigprocmask
	.hidden	__waitpid
	.hidden	__munmap
	.hidden	__clone
	.hidden	__mmap
