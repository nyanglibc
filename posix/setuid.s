	.text
	.p2align 4,,15
	.globl	__setuid
	.type	__setuid, @function
__setuid:
	cmpq	$0, __nptl_setxid@GOTPCREL(%rip)
	jne	.L11
	movl	$105, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/setuid.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	subq	$56, %rsp
	movl	%edi, %eax
	movq	%rsp, %rdi
	movl	$105, (%rsp)
	movq	%rax, 8(%rsp)
	call	__nptl_setxid@PLT
	addq	$56, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__setuid, .-__setuid
	.weak	setuid
	.set	setuid,__setuid
	.weak	__nptl_setxid
	.hidden	__nptl_setxid
