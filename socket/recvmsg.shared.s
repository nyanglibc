	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_recvmsg
	.type	__libc_recvmsg, @function
__libc_recvmsg:
#APP
# 28 "../sysdeps/unix/sysv/linux/recvmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$47, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/recvmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$47, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/recvmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_recvmsg, .-__libc_recvmsg
	.weak	__recvmsg
	.hidden	__recvmsg
	.set	__recvmsg,__libc_recvmsg
	.weak	recvmsg
	.set	recvmsg,__libc_recvmsg
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
