.text
.globl __socket
.type __socket,@function
.align 1<<4
__socket:
	movl $41, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __socket,.-__socket
.weak socket
socket = __socket
