	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_send
	.type	__libc_send, @function
__libc_send:
	movl	%ecx, %r10d
#APP
# 28 "../sysdeps/unix/sysv/linux/send.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movl	$44, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/send.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r14
	pushq	%r13
	movq	%rdx, %r14
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r13d
	pushq	%rbx
	movq	%rsi, %r12
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	xorl	%r9d, %r9d
	movl	%eax, %ebp
	xorl	%r8d, %r8d
	movl	%r13d, %r10d
	movq	%r14, %rdx
	movq	%r12, %rsi
	movl	%ebx, %edi
	movl	$44, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/send.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%ebp, %edi
	movq	%rax, 8(%rsp)
	call	__libc_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_send, .-__libc_send
	.weak	__GI___send
	.hidden	__GI___send
	.set	__GI___send,__libc_send
	.globl	__send
	.set	__send,__GI___send
	.weak	send
	.set	send,__libc_send
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
