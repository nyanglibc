.text
.globl __socketpair
.type __socketpair,@function
.align 1<<4
__socketpair:
	movq %rcx, %r10
	movl $53, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __socketpair,.-__socketpair
.globl __GI___socketpair
.set __GI___socketpair,__socketpair
.weak socketpair
socketpair = __socketpair
.globl __GI_socketpair
.set __GI_socketpair,socketpair
