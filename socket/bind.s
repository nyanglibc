.text
.globl __bind
.type __bind,@function
.align 1<<4
__bind:
	movl $49, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __bind,.-__bind
.weak bind
bind = __bind
