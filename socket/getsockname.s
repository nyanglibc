.text
.globl __getsockname
.type __getsockname,@function
.align 1<<4
__getsockname:
	movl $51, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __getsockname,.-__getsockname
.weak getsockname
getsockname = __getsockname
