.text
.globl __listen
.type __listen,@function
.align 1<<4
__listen:
	movl $50, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __listen,.-__listen
.globl __GI___listen
.set __GI___listen,__listen
.weak listen
listen = __listen
.globl __GI_listen
.set __GI_listen,listen
