	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/opensock.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"last_type != 0"
.LC2:
	.string	"/proc/net"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__opensock
	.hidden	__opensock
	.type	__opensock, @function
__opensock:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	last_family.4836(%rip), %edi
	testl	%edi, %edi
	je	.L2
	movl	last_type.4837(%rip), %esi
	testl	%esi, %esi
	je	.L23
	orl	$524288, %esi
	xorl	%edx, %edx
	call	__GI___socket
	cmpl	$-1, %eax
	je	.L24
.L1:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$97, %fs:(%rdx)
	jne	.L1
	movl	$0, last_family.4836(%rip)
	movl	$0, last_type.4837(%rip)
.L2:
	leaq	.LC2(%rip), %rdi
	movl	$4, %esi
	leaq	4+afs.4841(%rip), %rbx
	movq	%rsp, %r15
	call	__GI___access
	movl	%eax, %r12d
	movl	$12131, %eax
	movl	$1869770799, (%rsp)
	movw	%ax, 4(%rsp)
	movb	$0, 6(%rsp)
	leaq	220(%rbx), %r13
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L28:
	cmpl	$9, %ebp
	je	.L6
	xorl	%edx, %edx
	movl	$524290, %esi
	movl	%ebp, %edi
	call	__GI___socket
	cmpl	$-1, %eax
	movl	$2, %r14d
	jne	.L25
.L9:
	addq	$20, %rbx
	cmpq	%r13, %rbx
	je	.L26
.L10:
	cmpl	$-1, %r12d
	je	.L8
	cmpb	$0, (%rbx)
	jne	.L27
.L8:
	movl	-4(%rbx), %ebp
	cmpl	$6, %ebp
	jne	.L28
.L6:
	xorl	%edx, %edx
	movl	$524293, %esi
	movl	%ebp, %edi
	call	__GI___socket
	cmpl	$-1, %eax
	movl	$5, %r14d
	je	.L9
.L25:
	movl	%r14d, last_type.4837(%rip)
	movl	%ebp, last_family.4836(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	6(%r15), %rdi
	movq	%rbx, %rsi
	call	__GI_strcpy
	movl	$4, %esi
	movq	%r15, %rdi
	call	__GI___access
	cmpl	$-1, %eax
	jne	.L8
	addq	$20, %rbx
	cmpq	%r13, %rbx
	jne	.L10
.L26:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$2, %fs:(%rax)
	addq	$40, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L23:
	leaq	__PRETTY_FUNCTION__.4846(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$63, %edx
	call	__GI___assert_fail
	.size	__opensock, .-__opensock
	.section	.rodata
	.align 32
	.type	afs.4841, @object
	.size	afs.4841, 220
afs.4841:
	.long	1
	.string	"net/unix"
	.zero	6
	.zero	1
	.long	2
	.string	""
	.zero	14
	.zero	1
	.long	10
	.string	"net/if_inet6"
	.zero	2
	.zero	1
	.long	3
	.string	"net/ax25"
	.zero	6
	.zero	1
	.long	6
	.string	"net/nr"
	.zero	8
	.zero	1
	.long	11
	.string	"net/rose"
	.zero	6
	.zero	1
	.long	4
	.string	"net/ipx"
	.zero	7
	.zero	1
	.long	5
	.string	"net/appletalk"
	.zero	1
	.zero	1
	.long	19
	.string	"sys/net/econet"
	.zero	1
	.long	18
	.string	"sys/net/ash"
	.zero	3
	.zero	1
	.long	9
	.string	"net/x25"
	.zero	7
	.zero	1
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.4846, @object
	.size	__PRETTY_FUNCTION__.4846, 11
__PRETTY_FUNCTION__.4846:
	.string	"__opensock"
	.local	last_type.4837
	.comm	last_type.4837,4,4
	.local	last_family.4836
	.comm	last_family.4836,4,4
