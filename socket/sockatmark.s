	.text
	.p2align 4,,15
	.globl	sockatmark
	.type	sockatmark, @function
sockatmark:
	subq	$24, %rsp
	xorl	%eax, %eax
	movl	$35077, %esi
	leaq	12(%rsp), %rdx
	call	__ioctl
	cmpl	$-1, %eax
	je	.L1
	movl	12(%rsp), %eax
.L1:
	addq	$24, %rsp
	ret
	.size	sockatmark, .-sockatmark
	.hidden	__ioctl
