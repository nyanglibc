.text
.globl __getpeername
.type __getpeername,@function
.align 1<<4
__getpeername:
	movl $52, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __getpeername,.-__getpeername
.weak getpeername
getpeername = __getpeername
