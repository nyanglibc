	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___cmsg_nxthdr
	.hidden	__GI___cmsg_nxthdr
	.type	__GI___cmsg_nxthdr, @function
__GI___cmsg_nxthdr:
	movq	(%rsi), %rax
	cmpq	$15, %rax
	jbe	.L4
	addq	$7, %rax
	movq	40(%rdi), %rcx
	addq	32(%rdi), %rcx
	andq	$-8, %rax
	addq	%rsi, %rax
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	jb	.L4
	movq	(%rax), %rdi
	leaq	7(%rdi), %rdx
	andq	$-8, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rcx
	movl	$0, %edx
	cmovb	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	ret
	.size	__GI___cmsg_nxthdr, .-__GI___cmsg_nxthdr
	.globl	__cmsg_nxthdr
	.set	__cmsg_nxthdr,__GI___cmsg_nxthdr
