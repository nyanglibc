	.text
	.p2align 4,,15
	.globl	__cmsg_nxthdr
	.hidden	__cmsg_nxthdr
	.type	__cmsg_nxthdr, @function
__cmsg_nxthdr:
	movq	(%rsi), %rax
	cmpq	$15, %rax
	jbe	.L4
	addq	$7, %rax
	movq	40(%rdi), %rcx
	addq	32(%rdi), %rcx
	andq	$-8, %rax
	addq	%rsi, %rax
	leaq	16(%rax), %rdx
	cmpq	%rdx, %rcx
	jb	.L4
	movq	(%rax), %rdi
	leaq	7(%rdi), %rdx
	andq	$-8, %rdx
	addq	%rax, %rdx
	cmpq	%rdx, %rcx
	movl	$0, %edx
	cmovb	%rdx, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	ret
	.size	__cmsg_nxthdr, .-__cmsg_nxthdr
