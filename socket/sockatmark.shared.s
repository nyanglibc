	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	sockatmark
	.type	sockatmark, @function
sockatmark:
	subq	$24, %rsp
	xorl	%eax, %eax
	movl	$35077, %esi
	leaq	12(%rsp), %rdx
	call	__GI___ioctl
	cmpl	$-1, %eax
	je	.L1
	movl	12(%rsp), %eax
.L1:
	addq	$24, %rsp
	ret
	.size	sockatmark, .-sockatmark
