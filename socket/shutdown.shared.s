.text
.globl __shutdown
.type __shutdown,@function
.align 1<<4
__shutdown:
	movl $48, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __shutdown,.-__shutdown
.globl __GI___shutdown
.set __GI___shutdown,__shutdown
.weak shutdown
shutdown = __shutdown
.globl __GI_shutdown
.set __GI_shutdown,shutdown
