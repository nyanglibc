	.text
	.p2align 4,,15
	.globl	__recvmmsg
	.type	__recvmmsg, @function
__recvmmsg:
	movl	%ecx, %r10d
#APP
# 30 "../sysdeps/unix/sysv/linux/recvmmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$299, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/recvmmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movl	%ecx, %r13d
	pushq	%rbx
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__libc_enable_asynccancel
	movq	%r14, %r8
	movl	%eax, %r9d
	movl	%r13d, %r10d
	movl	%r12d, %edx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$299, %eax
#APP
# 30 "../sysdeps/unix/sysv/linux/recvmmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r9d, %edi
	movl	%eax, 12(%rsp)
	call	__libc_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__recvmmsg, .-__recvmmsg
	.weak	recvmmsg
	.set	recvmmsg,__recvmmsg
	.hidden	__libc_disable_asynccancel
	.hidden	__libc_enable_asynccancel
