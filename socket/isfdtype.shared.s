	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	isfdtype
	.type	isfdtype, @function
isfdtype:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	subq	$144, %rsp
	movq	__libc_errno@gottpoff(%rip), %rbx
	movq	%rsp, %rsi
	movl	%fs:(%rbx), %ebp
	call	__GI___fstat64
	testl	%eax, %eax
	movl	%ebp, %fs:(%rbx)
	jne	.L1
	movl	24(%rsp), %eax
	andl	$61440, %eax
	cmpl	%r12d, %eax
	sete	%al
	movzbl	%al, %eax
.L1:
	addq	$144, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	isfdtype, .-isfdtype
