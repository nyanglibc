	.text
	.p2align 4,,15
	.globl	__libc_sa_len
	.hidden	__libc_sa_len
	.type	__libc_sa_len, @function
__libc_sa_len:
	subl	$1, %edi
	xorl	%eax, %eax
	cmpw	$18, %di
	ja	.L1
	leaq	CSWTCH.1(%rip), %rax
	movzwl	%di, %edi
	movl	(%rax,%rdi,4), %eax
.L1:
	rep ret
	.size	__libc_sa_len, .-__libc_sa_len
	.section	.rodata
	.align 32
	.type	CSWTCH.1, @object
	.size	CSWTCH.1, 76
CSWTCH.1:
	.long	110
	.long	16
	.long	16
	.long	16
	.long	16
	.long	0
	.long	0
	.long	0
	.long	0
	.long	28
	.long	28
	.long	0
	.long	0
	.long	0
	.long	0
	.long	0
	.long	20
	.long	32
	.long	16
