.text
.globl __getsockopt
.type __getsockopt,@function
.align 1<<4
__getsockopt:
	movq %rcx, %r10
	movl $55, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __getsockopt,.-__getsockopt
.globl __GI___getsockopt
.set __GI___getsockopt,__getsockopt
.weak getsockopt
getsockopt = __getsockopt
.globl __GI_getsockopt
.set __GI_getsockopt,getsockopt
