	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __vtimes,vtimes@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	vtimes_one.part.0, @function
vtimes_one.part.0:
	pushq	%rbx
	movq	%rdi, %rbx
	movl	%esi, %edi
	subq	$144, %rsp
	movq	%rsp, %rsi
	call	__getrusage
	testl	%eax, %eax
	js	.L6
	movq	8(%rsp), %rax
	movabsq	$4835703278458516699, %rcx
	movq	%rax, %rsi
	salq	$4, %rsi
	subq	%rax, %rsi
	salq	$2, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rcx
	imull	$60, (%rsp), %eax
	sarq	$18, %rdx
	subq	%rsi, %rdx
	addl	%eax, %edx
	movq	24(%rsp), %rax
	movl	%edx, (%rbx)
	movq	%rax, %rsi
	salq	$4, %rsi
	subq	%rax, %rsi
	salq	$2, %rsi
	movq	%rsi, %rax
	sarq	$63, %rsi
	imulq	%rcx
	movq	56(%rsp), %rax
	addl	48(%rsp), %eax
	movl	%eax, 8(%rbx)
	movq	72(%rsp), %rax
	movq	%rdx, %rcx
	imull	$60, 16(%rsp), %edx
	sarq	$18, %rcx
	movl	%eax, 20(%rbx)
	movq	64(%rsp), %rax
	subq	%rsi, %rcx
	movl	%eax, 24(%rbx)
	movq	80(%rsp), %rax
	addl	%edx, %ecx
	movl	%ecx, 4(%rbx)
	movl	%eax, 28(%rbx)
	movq	88(%rsp), %rax
	movl	%eax, 32(%rbx)
	movq	96(%rsp), %rax
	movl	%eax, 36(%rbx)
	xorl	%eax, %eax
.L1:
	addq	$144, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$-1, %eax
	jmp	.L1
	.size	vtimes_one.part.0, .-vtimes_one.part.0
	.p2align 4,,15
	.globl	__vtimes
	.type	__vtimes, @function
__vtimes:
	testq	%rdi, %rdi
	pushq	%rbx
	movq	%rsi, %rbx
	je	.L11
	xorl	%esi, %esi
	call	vtimes_one.part.0
	testl	%eax, %eax
	js	.L14
.L11:
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.L7
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	vtimes_one.part.0
	sarl	$31, %eax
.L7:
	popq	%rbx
	ret
.L14:
	movl	$-1, %eax
	popq	%rbx
	ret
	.size	__vtimes, .-__vtimes
	.hidden	__getrusage
