	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ulimit
	.type	__ulimit, @function
__ulimit:
	pushq	%rbx
	subq	$96, %rsp
	cmpl	$2, %edi
	leaq	112(%rsp), %rax
	movq	%rsi, 56(%rsp)
	movl	$8, 24(%rsp)
	movq	%rax, 32(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 40(%rsp)
	je	.L3
	cmpl	$4, %edi
	je	.L4
	cmpl	$1, %edi
	je	.L18
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L15:
	movq	$-1, %rbx
	addq	$96, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movq	%rsp, %rsi
	call	__GI___getrlimit
	testl	%eax, %eax
	jne	.L15
	movq	(%rsp), %rax
	movabsq	$9223372036854775807, %rbx
	movq	%rax, %rdx
	shrq	$9, %rdx
	cmpq	$-1, %rax
	cmovne	%rdx, %rbx
	addq	$96, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$4, %edi
	call	__GI___sysconf
	addq	$96, %rsp
	movq	%rax, %rbx
	movq	%rbx, %rax
	popq	%rbx
	ret
.L3:
	movq	40(%rsp), %rax
	movl	$16, 24(%rsp)
	movq	8(%rax), %rbx
	movabsq	$36028797018963967, %rax
	cmpq	%rax, %rbx
	jbe	.L10
	movq	$-1, %rax
	movabsq	$9223372036854775807, %rbx
	movq	%rax, (%rsp)
	movq	%rax, 8(%rsp)
.L11:
	movq	%rsp, %rsi
	movl	$1, %edi
	call	__GI___setrlimit
	cmpl	$-1, %eax
	je	.L15
	addq	$96, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
.L10:
	movq	%rbx, %rax
	salq	$9, %rax
	movq	%rax, (%rsp)
	movq	%rax, 8(%rsp)
	jmp	.L11
	.size	__ulimit, .-__ulimit
	.weak	ulimit
	.set	ulimit,__ulimit
