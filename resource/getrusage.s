	.text
	.p2align 4,,15
	.globl	__getrusage
	.hidden	__getrusage
	.type	__getrusage, @function
__getrusage:
	movl	$98, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/getrusage.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__getrusage, .-__getrusage
	.weak	getrusage
	.set	getrusage,__getrusage
