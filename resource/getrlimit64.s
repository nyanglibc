	.text
	.p2align 4,,15
	.globl	__getrlimit64
	.hidden	__getrlimit64
	.type	__getrlimit64, @function
__getrlimit64:
	movq	%rsi, %r10
	xorl	%edx, %edx
	movl	%edi, %esi
	movl	$302, %eax
	xorl	%edi, %edi
#APP
# 39 "../sysdeps/unix/sysv/linux/getrlimit64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__getrlimit64, .-__getrlimit64
	.weak	getrlimit64
	.hidden	getrlimit64
	.set	getrlimit64,__getrlimit64
	.weak	getrlimit
	.set	getrlimit,__getrlimit64
	.globl	__getrlimit
	.set	__getrlimit,__getrlimit64
	.globl	__GI___getrlimit
	.set	__GI___getrlimit,__getrlimit64
	.globl	__GI_getrlimit
	.set	__GI_getrlimit,__getrlimit64
