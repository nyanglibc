	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__setrlimit64
	.type	__setrlimit64, @function
__setrlimit64:
	movq	%rsi, %rdx
	xorl	%r10d, %r10d
	movl	%edi, %esi
	movl	$302, %eax
	xorl	%edi, %edi
#APP
# 40 "../sysdeps/unix/sysv/linux/setrlimit64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	rep ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__setrlimit64, .-__setrlimit64
	.globl	__GI___setrlimit
	.set	__GI___setrlimit,__setrlimit64
	.weak	setrlimit
	.set	setrlimit,__setrlimit64
	.globl	__setrlimit
	.set	__setrlimit,__setrlimit64
	.weak	setrlimit64
	.set	setrlimit64,__setrlimit64
