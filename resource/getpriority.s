	.text
	.p2align 4,,15
	.globl	__getpriority
	.hidden	__getpriority
	.type	__getpriority, @function
__getpriority:
	movl	$140, %eax
#APP
# 39 "../sysdeps/unix/sysv/linux/getpriority.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	movl	$20, %edx
	subl	%eax, %edx
	testl	%eax, %eax
	cmovns	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__getpriority, .-__getpriority
	.weak	getpriority
	.set	getpriority,__getpriority
