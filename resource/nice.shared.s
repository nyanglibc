	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	nice
	.type	nice, @function
nice:
	pushq	%r12
	pushq	%rbp
	xorl	%esi, %esi
	pushq	%rbx
	movq	__libc_errno@gottpoff(%rip), %rbx
	movl	%edi, %ebp
	xorl	%edi, %edi
	movl	%fs:(%rbx), %r12d
	movl	$0, %fs:(%rbx)
	call	__GI___getpriority
	cmpl	$-1, %eax
	jne	.L2
	movl	%fs:(%rbx), %edx
	testl	%edx, %edx
	jne	.L5
.L2:
	leal	(%rax,%rbp), %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__GI___setpriority
	cmpl	$-1, %eax
	jne	.L4
	cmpl	$13, %fs:(%rbx)
	je	.L8
.L5:
	popq	%rbx
	movl	$-1, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%r12d, %fs:(%rbx)
	xorl	%esi, %esi
	xorl	%edi, %edi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__GI___getpriority
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$1, %fs:(%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	nice, .-nice
