.text
.globl __setpriority
.type __setpriority,@function
.align 1<<4
__setpriority:
	movl $141, %eax
	syscall
	cmpq $-4095, %rax
	jae 0f
	ret
0:
	movq __libc_errno@GOTTPOFF(%rip), %rcx
	neg %eax
	movl %eax, %fs:(%rcx)
	or $-1, %rax
	ret
.size __setpriority,.-__setpriority
.weak setpriority
setpriority = __setpriority
