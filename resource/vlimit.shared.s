	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	vlimit
	.type	vlimit, @function
vlimit:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	leal	-1(%rdi), %ebx
	subq	$16, %rsp
	cmpl	$5, %ebx
	ja	.L2
	movq	%rsp, %r12
	movl	%esi, %ebp
	movl	%ebx, %edi
	movq	%r12, %rsi
	call	__GI___getrlimit
	testl	%eax, %eax
	movl	$-1, %edx
	js	.L1
	movslq	%ebp, %rsi
	movl	%ebx, %edi
	movq	%rsi, (%rsp)
	movq	%r12, %rsi
	call	__GI___setrlimit
	movl	%eax, %edx
.L1:
	addq	$16, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$-1, %edx
	movl	$22, %fs:(%rax)
	jmp	.L1
	.size	vlimit, .-vlimit
