	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___getpriority
	.hidden	__GI___getpriority
	.type	__GI___getpriority, @function
__GI___getpriority:
	movl	$140, %eax
#APP
# 39 "../sysdeps/unix/sysv/linux/getpriority.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L5
	movl	$20, %edx
	subl	%eax, %edx
	testl	%eax, %eax
	cmovns	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movq	__libc_errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
	.size	__GI___getpriority, .-__GI___getpriority
	.globl	__getpriority
	.set	__getpriority,__GI___getpriority
	.weak	getpriority
	.set	getpriority,__getpriority
