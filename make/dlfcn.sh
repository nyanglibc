printf "\
DLFCN***************************************************************************\n"
fns_pie="\
sdlopen \
sdlclose \
sdlsym \
sdlvsym \
sdlerror \
sdladdr \
sdladdr1 \
sdlinfo \
sdlmopen \
sdlfreeres \
"
mkdir -p $build_dir/dlfcn
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/dlfcn/$fn.s -o $build_dir/dlfcn/$fn.o
done
printf 'CREATING STAMPS FILES\n'
printf 'dlfcn/sdlopen.o dlfcn/sdlclose.o dlfcn/sdlsym.o dlfcn/sdlvsym.o dlfcn/sdlerror.o dlfcn/sdladdr.o dlfcn/sdladdr1.o dlfcn/sdlinfo.o dlfcn/sdlmopen.o dlfcn/sdlfreeres.o\n' >$build_dir/dlfcn/stamp.o
printf '' >$build_dir/dlfcn/stamp.os
printf '' >$build_dir/dlfcn/stamp.oS
