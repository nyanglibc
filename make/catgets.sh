printf "\
CATGETS*************************************************************************\n"
gens="\
catgets.s.in \
catgets.shared.s.in \
"
conf_nlspath_str="$conf_prefix/share/locale/%L/%N:$conf_prefix/share/locale/%L/LC_MESSAGES/%N:$conf_prefix/share/locale/%l/%N:$conf_prefix/share/locale/%l/LC_MESSAGES/%N:"
conf_nlspath_str_bytes_n=$(printf '%s\n' "$conf_nlspath_str\0" | wc -c)
mkdir -p $build_dir/catgets
for g in $gens
do
	# order matters
	sed -E -e "s@CONF_NLSPATH_STR_BYTES_N@$conf_nlspath_str_bytes_n@;s@CONF_NLSPATH_STR@$conf_nlspath_str@g" \
			$src_dir/catgets/$g >$build_dir/catgets/$(basename $g .in)
done
paths="\
$build_dir/catgets/catgets \
$src_dir/catgets/open_catalog \
"
mkdir -p $build_dir/catgets
for p in $paths
do
	printf "ASSEMBLING PIE $fn\n"
	$as $p.s -o $build_dir/catgets/$(basename $p).o
	printf "ASSEMBLING SHARED $fn\n"
	$as $p.shared.s -o $build_dir/catgets/$(basename $p).os
done
printf 'CREATING STAMPS FILES\n'
printf 'catgets/catgets.o catgets/open_catalog.o\n' >$build_dir/catgets/stamp.o
printf 'catgets/catgets.os catgets/open_catalog.os\n' >$build_dir/catgets/stamp.os
printf '' >$build_dir/catgets/stamp.oS
