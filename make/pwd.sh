printf "\
PWD*****************************************************************************\n"
fns_pie_shared="\
fgetpwent \
getpw \
putpwent \
getpwent \
getpwnam \
getpwuid \
getpwent_r \
getpwnam_r \
getpwuid_r \
fgetpwent_r \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/pwd
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/pwd/$fn.s -o $build_dir/pwd/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/pwd/$fn.shared.s -o $build_dir/pwd/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'pwd/fgetpwent.o pwd/getpw.o pwd/putpwent.o pwd/getpwent.o pwd/getpwnam.o pwd/getpwuid.o pwd/getpwent_r.o pwd/getpwnam_r.o pwd/getpwuid_r.o pwd/fgetpwent_r.o\n' >$build_dir/pwd/stamp.o
printf 'pwd/fgetpwent.os pwd/getpw.os pwd/putpwent.os pwd/getpwent.os pwd/getpwnam.os pwd/getpwuid.os pwd/getpwent_r.os pwd/getpwnam_r.os pwd/getpwuid_r.os pwd/fgetpwent_r.os\n' >$build_dir/pwd/stamp.os
printf '' >$build_dir/pwd/stamp.oS
