printf "\
TERMIOS*************************************************************************\n"
fns="\
speed \
cfsetspeed \
tcsetattr \
tcgetattr \
tcgetpgrp \
tcsetpgrp \
tcdrain \
tcflow \
tcflush \
tcsendbrk \
cfmakeraw \
tcgetsid \
"
mkdir -p $build_dir/termios
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/termios/$fn.s -o $build_dir/termios/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/termios/$fn.shared.s -o $build_dir/termios/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'termios/speed.o termios/cfsetspeed.o termios/tcsetattr.o termios/tcgetattr.o termios/tcgetpgrp.o termios/tcsetpgrp.o termios/tcdrain.o termios/tcflow.o termios/tcflush.o termios/tcsendbrk.o termios/cfmakeraw.o termios/tcgetsid.o\n' >$build_dir/termios/stamp.o
printf 'termios/speed.os termios/cfsetspeed.os termios/tcsetattr.os termios/tcgetattr.os termios/tcgetpgrp.os termios/tcsetpgrp.os termios/tcdrain.os termios/tcflow.os termios/tcflush.os termios/tcsendbrk.os termios/cfmakeraw.os termios/tcgetsid.os\n' >$build_dir/termios/stamp.os
printf '' >$build_dir/termios/stamp.oS
