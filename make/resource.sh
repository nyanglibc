printf "\
RESOURCE************************************************************************\n"
fns="\
getrlimit \
setrlimit \
getrlimit64 \
setrlimit64 \
getrusage \
ulimit \
vlimit \
vtimes \
getpriority \
nice \
\
setpriority \
"
mkdir -p $build_dir/resource
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/resource/$fn.s -o $build_dir/resource/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/resource/$fn.shared.s -o $build_dir/resource/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'resource/getrlimit.o resource/setrlimit.o resource/getrlimit64.o resource/setrlimit64.o resource/getrusage.o resource/ulimit.o resource/vlimit.o resource/vtimes.o resource/getpriority.o resource/setpriority.o resource/nice.o\n' >$build_dir/resource/stamp.o
printf 'resource/getrlimit.os resource/setrlimit.os resource/getrlimit64.os resource/setrlimit64.os resource/getrusage.os resource/ulimit.os resource/vlimit.os resource/vtimes.os resource/getpriority.os resource/setpriority.os resource/nice.os\n' >$build_dir/resource/stamp.os
printf '' >$build_dir/resource/stamp.oS
