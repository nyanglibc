printf "\
TIME****************************************************************************\n"
gens="\
tzfile.s.in \
tzfile.shared.s.in \
"
mkdir -p $build_dir/time
conf_default_tzdir_str="$conf_prefix/share/zoneinfo"
conf_default_tzdir_str_bytes_n=$(printf "$conf_default_tzdir_str\0" | wc -c)
conf_tzdefault_str="$conf_prefix/etc/localtime"
conf_tzdefault_str_bytes_n=$(printf "$conf_tzdefault_str\0" | wc -c)
for g in $gens
do
	#order matters
	sed -E -e "\
s@CONF_DEFAULT_TZDIR_STR_BYTES_N@$conf_default_tzdir_str_bytes_n@;\
s@CONF_DEFAULT_TZDIR_STR@$conf_default_tzdir_str@;\
s@CONF_TZDEFAULT_STR_BYTES_N@$conf_tzdefault_str_bytes_n@;
s@CONF_TZDEFAULT_STR@$conf_tzdefault_str@" \
			$src_dir/time/$g >$build_dir/time/$(basename $g .in)
done
gens="\
tzset.s.in \
tzset.shared.s.in \
"
mkdir -p $build_dir/time
for g in $gens
do
	sed -E -e "s@CONF_TZDEFAULT_STR_BYTES_N@$conf_tzdefault_str_bytes_n@;s@CONF_TZDEFAULT_STR@$conf_tzdefault_str@" \
			$src_dir/time/$g >$build_dir/time/$(basename $g .in)
done
paths_pie_shared="\
$src_dir/time/offtime \
$src_dir/time/asctime \
$src_dir/time/clock \
$src_dir/time/ctime \
$src_dir/time/ctime_r \
$src_dir/time/difftime \
$src_dir/time/gmtime \
$src_dir/time/localtime \
$src_dir/time/mktime \
$src_dir/time/time \
$src_dir/time/gettimeofday \
$src_dir/time/settimeofday \
$src_dir/time/settimezone \
$src_dir/time/adjtime \
$build_dir/time/tzset \
$build_dir/time/tzfile \
$src_dir/time/getitimer \
$src_dir/time/setitimer \
$src_dir/time/stime \
$src_dir/time/dysize \
$src_dir/time/timegm \
$src_dir/time/ftime \
$src_dir/time/getdate \
$src_dir/time/strptime \
$src_dir/time/strptime_l \
$src_dir/time/strftime \
$src_dir/time/wcsftime \
$src_dir/time/strftime_l \
$src_dir/time/wcsftime_l \
$src_dir/time/timespec_get \
$src_dir/time/clock_getcpuclockid \
$src_dir/time/clock_getres \
$src_dir/time/clock_gettime \
$src_dir/time/clock_settime \
$src_dir/time/clock_nanosleep \
$src_dir/time/era \
$src_dir/time/alt_digit \
$src_dir/time/lc-time-cleanup \
$src_dir/time/ntp_gettime \
$src_dir/time/ntp_gettimex \
"
paths_pie="\
$paths_pie_shared \
"
paths_shared="\
$paths_pie_shared \
"
mkdir -p $build_dir/time
for p in $paths_pie
do
	printf "ASSEMBLING PIE $p\n"
	$as $p.s -o $build_dir/time/$(basename $p).o
done
for p in $paths_shared
do
	printf "ASSEMBLING SHARED $p\n"
	$as $p.shared.s -o $build_dir/time/$(basename $p).os
done
printf 'CREATING STAMPS FILES\n'
printf 'time/offtime.o time/asctime.o time/clock.o time/ctime.o time/ctime_r.o time/difftime.o time/gmtime.o time/localtime.o time/mktime.o time/time.o time/gettimeofday.o time/settimeofday.o time/settimezone.o time/adjtime.o time/tzset.o time/tzfile.o time/getitimer.o time/setitimer.o time/stime.o time/dysize.o time/timegm.o time/ftime.o time/getdate.o time/strptime.o time/strptime_l.o time/strftime.o time/wcsftime.o time/strftime_l.o time/wcsftime_l.o time/timespec_get.o time/clock_getcpuclockid.o time/clock_getres.o time/clock_gettime.o time/clock_settime.o time/clock_nanosleep.o time/era.o time/alt_digit.o time/lc-time-cleanup.o time/ntp_gettime.o time/ntp_gettimex.o\n' >$build_dir/time/stamp.o
printf 'time/offtime.os time/asctime.os time/clock.os time/ctime.os time/ctime_r.os time/difftime.os time/gmtime.os time/localtime.os time/mktime.os time/time.os time/gettimeofday.os time/settimeofday.os time/settimezone.os time/adjtime.os time/tzset.os time/tzfile.os time/getitimer.os time/setitimer.os time/stime.os time/dysize.os time/timegm.os time/ftime.os time/getdate.os time/strptime.os time/strptime_l.os time/strftime.os time/wcsftime.os time/strftime_l.os time/wcsftime_l.os time/timespec_get.os time/clock_getcpuclockid.os time/clock_getres.os time/clock_gettime.os time/clock_settime.os time/clock_nanosleep.os time/era.os time/alt_digit.os time/lc-time-cleanup.os time/ntp_gettime.os time/ntp_gettimex.os\n' >$build_dir/time/stamp.os
printf '' >$build_dir/time/stamp.oS
