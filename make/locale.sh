printf "\
LOCALE**************************************************************************\n"
gens="\
loadarchive.s.in \
loadarchive.shared.s.in \
"
mkdir -p $build_dir/locale
for g in $gens
do
	sed -E -e "s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/locale/$g >$build_dir/locale/$(basename $g .in)
done
gens="\
findlocale.s.in \
findlocale.shared.s.in \
"
conf_nl_default_locale_path_str="$conf_prefix/lib/locale"
conf_nl_default_locale_path_str_bytes_n=$(printf "$conf_nl_default_locale_path_str\0" | wc -c)
mkdir -p $build_dir/locale
for g in $gens
do
	# order matters
	sed -E -e "s@CONF_NL_DEFAULT_LOCALE_PATH_STR_BYTES_N@$conf_nl_default_locale_path_str_bytes_n@;s@CONF_NL_DEFAULT_LOCALE_PATH_STR@$conf_nl_default_locale_path_str@" \
			$src_dir/locale/$g >$build_dir/locale/$(basename $g .in)
done
paths="\
$src_dir/locale/setlocale \
$build_dir/locale/findlocale \
$src_dir/locale/loadlocale \
$build_dir/locale/loadarchive \
$src_dir/locale/localeconv \
$src_dir/locale/nl_langinfo \
$src_dir/locale/nl_langinfo_l \
$src_dir/locale/mb_cur_max \
$src_dir/locale/newlocale \
$src_dir/locale/duplocale \
$src_dir/locale/freelocale \
$src_dir/locale/uselocale \
$src_dir/locale/lc-ctype \
$src_dir/locale/lc-messages \
$src_dir/locale/lc-monetary \
$src_dir/locale/lc-numeric \
$src_dir/locale/lc-time \
$src_dir/locale/lc-paper \
$src_dir/locale/lc-name \
$src_dir/locale/lc-address \
$src_dir/locale/lc-telephone \
$src_dir/locale/lc-measurement \
$src_dir/locale/lc-identification \
$src_dir/locale/lc-collate \
$src_dir/locale/C-ctype \
$src_dir/locale/C-messages \
$src_dir/locale/C-monetary \
$src_dir/locale/C-numeric \
$src_dir/locale/C-time \
$src_dir/locale/C-paper \
$src_dir/locale/C-name \
$src_dir/locale/C-address \
$src_dir/locale/C-telephone \
$src_dir/locale/C-measurement \
$src_dir/locale/C-identification \
$src_dir/locale/C-collate \
$src_dir/locale/SYS_libc \
$src_dir/locale/C_name \
$src_dir/locale/xlocale \
$src_dir/locale/localename \
$src_dir/locale/global-locale \
$src_dir/locale/coll-lookup \
"
mkdir -p $build_dir/locale
for p in $paths
do
	printf "ASSEMBLING PIE $p\n"
	$as $p.s -o $build_dir/locale/$(basename $p).o
	printf "ASSEMBLING SHARED $p\n"
	$as $p.shared.s -o $build_dir/locale/$(basename $p).os
done
printf 'CREATING STAMPS FILES\n'
printf 'locale/setlocale.o locale/findlocale.o locale/loadlocale.o locale/loadarchive.o locale/localeconv.o locale/nl_langinfo.o locale/nl_langinfo_l.o locale/mb_cur_max.o locale/newlocale.o locale/duplocale.o locale/freelocale.o locale/uselocale.o locale/lc-ctype.o locale/lc-messages.o locale/lc-monetary.o locale/lc-numeric.o locale/lc-time.o locale/lc-paper.o locale/lc-name.o locale/lc-address.o locale/lc-telephone.o locale/lc-measurement.o locale/lc-identification.o locale/lc-collate.o locale/C-ctype.o locale/C-messages.o locale/C-monetary.o locale/C-numeric.o locale/C-time.o locale/C-paper.o locale/C-name.o locale/C-address.o locale/C-telephone.o locale/C-measurement.o locale/C-identification.o locale/C-collate.o locale/SYS_libc.o locale/C_name.o locale/xlocale.o locale/localename.o locale/global-locale.o locale/coll-lookup.o\n' >$build_dir/locale/stamp.o
printf 'locale/setlocale.os locale/findlocale.os locale/loadlocale.os locale/loadarchive.os locale/localeconv.os locale/nl_langinfo.os locale/nl_langinfo_l.os locale/mb_cur_max.os locale/newlocale.os locale/duplocale.os locale/freelocale.os locale/uselocale.os locale/lc-ctype.os locale/lc-messages.os locale/lc-monetary.os locale/lc-numeric.os locale/lc-time.os locale/lc-paper.os locale/lc-name.os locale/lc-address.os locale/lc-telephone.os locale/lc-measurement.os locale/lc-identification.os locale/lc-collate.os locale/C-ctype.os locale/C-messages.os locale/C-monetary.os locale/C-numeric.os locale/C-time.os locale/C-paper.os locale/C-name.os locale/C-address.os locale/C-telephone.os locale/C-measurement.os locale/C-identification.os locale/C-collate.os locale/SYS_libc.os locale/C_name.os locale/xlocale.os locale/localename.os locale/global-locale.os locale/coll-lookup.os\n' >$build_dir/locale/stamp.os
printf '' >$build_dir/locale/stamp.oS

# debian stolen C.utf8
cp -f $src_dir/locale/locale-archive $build_dir/locale/locale-archive
