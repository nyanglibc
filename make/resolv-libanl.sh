printf "\
RESOLV LIBANL*******************************************************************\n"
mkdir -p $build_dir/resolv/libanl
fns_pie_shared="\
gai_cancel \
gai_error \
gai_misc \
gai_notify \
gai_suspend \
getaddrinfo_a \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/resolv/libanl/$fn.s -o $build_dir/resolv/libanl/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/resolv/libanl/$fn.shared.s -o $build_dir/resolv/libanl/$fn.os
done
printf 'CREATING LIBANL.A\n'
cd $build_dir/resolv/libanl
$ar cruv libanl.a gai_cancel.o gai_error.o gai_misc.o gai_notify.o gai_suspend.o getaddrinfo_a.o
printf 'CREATING LIBANL_PIC.A\n'
$ar cruv libanl_pic.a gai_cancel.os gai_error.os gai_misc.os gai_notify.os gai_suspend.os getaddrinfo_a.os
printf 'CREATING LIBANL.SO.1\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libanl.versions.map \
	-soname=libanl.so.1 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/resolv/libanl/libanl.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/resolv/libanl/libanl_pic.a \
	--no-whole-archive \
	$build_dir/nptl/libpthread.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
