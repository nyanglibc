printf "\
ELF*****************************************************************************\n"
gens="\
dl-usage.shared.s.in \
dl-cache.s.in \
dl-cache.shared.s.in \
"
mkdir -p $build_dir/elf
for g in $gens
do
	printf "CONFIGURING $g\n"
	# order matters
	sed -E -e "s@CONF_PREFIX_BYTES_N@$conf_prefix_bytes_n@;s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/elf/$g >$build_dir/elf/$(basename $g .in)
done
gens="\
dl-load.s.in \
dl-load.shared.s.in \
"
# do not include the terminating 0
conf_system_dirs_str="$conf_prefix/lib/"
conf_system_dirs_str_bytes_n=$(printf "$conf_system_dirs_str\0" | wc -c)
for g in $gens
do
	printf "CONFIGURING $g\n"
	# order matters
	sed -E -e "s@CONF_SYSTEM_DIRS_STR_BYTES_N@$conf_system_dirs_str_bytes_n@;s@CONF_SYSTEM_DIRS_STR@$conf_system_dirs_str@" \
			$src_dir/elf/$g >$build_dir/elf/$(basename $g .in)
done
paths_pie_shared="\
$src_dir/elf/dl-iteratephdr \
$src_dir/elf/dl-addr \
$src_dir/elf/dl-addr-obj \
$src_dir/elf/dl-profstub \
$src_dir/elf/dl-libc \
$src_dir/elf/dl-sym \
$src_dir/elf/dl-error \
$src_dir/elf/libc_early_init \
$src_dir/elf/get-cpuid-feature-leaf \
$build_dir/elf/dl-load \
$src_dir/elf/dl-lookup \
$src_dir/elf/dl-object \
$src_dir/elf/dl-reloc \
$src_dir/elf/dl-deps \
$src_dir/elf/dl-runtime \
$src_dir/elf/dl-init \
$src_dir/elf/dl-fini \
$src_dir/elf/dl-debug \
$src_dir/elf/dl-scope \
$src_dir/elf/dl-execstack \
$src_dir/elf/dl-open \
$src_dir/elf/dl-close \
$src_dir/elf/dl-exception \
$src_dir/elf/dl-sort-maps \
$src_dir/elf/dl-lookup-direct \
$src_dir/elf/dl-call-libc-early-init \
$src_dir/elf/dl-write \
$src_dir/elf/dl-thread_gscope_wait \
$build_dir/elf/dl-cache \
$src_dir/elf/dl-tunables \
$src_dir/elf/tlsdesc \
$src_dir/elf/dl-get-cpu-features \
$src_dir/elf/dl-sysdep \
$src_dir/elf/dl-trampoline \
$src_dir/elf/dl-tlsdesc \
$src_dir/elf/tls_get_addr \
$src_dir/elf/dl-misc \
$src_dir/elf/dl-version \
$src_dir/elf/dl-profile \
$src_dir/elf/dl-tls \
$src_dir/elf/dl-origin \
"
paths_pie="\
$paths_pie_shared \
$src_dir/elf/dl-support \
$src_dir/elf/enbl-secure \
$src_dir/elf/dl-reloc-static-pie \
"
paths_shared="\
$paths_pie_shared \
$src_dir/elf/rtld \
$src_dir/elf/dl-environ \
$src_dir/elf/dl-minimal \
$src_dir/elf/dl-error-minimal \
$src_dir/elf/dl-conflict \
$src_dir/elf/dl-hwcaps \
$src_dir/elf/dl-hwcaps_split \
$src_dir/elf/dl-hwcaps-subdirs \
$build_dir/elf/dl-usage \
$src_dir/elf/dl-brk \
$src_dir/elf/dl-sbrk \
$src_dir/elf/dl-getcwd \
$src_dir/elf/dl-openat64 \
$src_dir/elf/dl-opendir \
"
mkdir -p $build_dir/elf
for p in $paths_pie
do
	printf "ASSEMBLING PIE $p\n"
	$as $p.s -o $build_dir/elf/$(basename $p .in).o
done
for p in $paths_shared
do
	printf "ASSEMBLING SHARED $p\n"
	$as $p.shared.s -o $build_dir/elf/$(basename $p .in).os
done
printf 'CREATING STAMPS FILES\n'
printf 'elf/dl-load.o elf/dl-lookup.o elf/dl-object.o elf/dl-reloc.o elf/dl-deps.o elf/dl-runtime.o elf/dl-init.o elf/dl-fini.o elf/dl-debug.o elf/dl-misc.o elf/dl-version.o elf/dl-profile.o elf/dl-tls.o elf/dl-origin.o elf/dl-scope.o elf/dl-execstack.o elf/dl-open.o elf/dl-close.o elf/dl-trampoline.o elf/dl-exception.o elf/dl-sort-maps.o elf/dl-lookup-direct.o elf/dl-call-libc-early-init.o elf/dl-write.o elf/dl-thread_gscope_wait.o elf/dl-cache.o elf/dl-tunables.o elf/tlsdesc.o elf/dl-tlsdesc.o elf/tls_get_addr.o elf/dl-get-cpu-features.o elf/dl-support.o elf/dl-iteratephdr.o elf/dl-addr.o elf/dl-addr-obj.o elf/enbl-secure.o elf/dl-profstub.o elf/dl-libc.o elf/dl-sym.o elf/dl-sysdep.o elf/dl-error.o elf/dl-reloc-static-pie.o elf/libc_early_init.o elf/get-cpuid-feature-leaf.o\n' >$build_dir/elf/stamp.o
printf 'elf/dl-iteratephdr.os elf/dl-addr.os elf/dl-addr-obj.os elf/dl-profstub.os elf/dl-libc.os elf/dl-sym.os elf/dl-error.os elf/libc_early_init.os elf/get-cpuid-feature-leaf.os\n' >$build_dir/elf/stamp.os
printf '' >$build_dir/elf/stamp.oS
printf 'CREATING SHARED DL-ALLOBJS.OS\n'
$ld -nostdlib -r -o $build_dir/elf/dl-allobjs.os $build_dir/elf/rtld.os $build_dir/elf/dl-load.os $build_dir/elf/dl-lookup.os $build_dir/elf/dl-object.os $build_dir/elf/dl-reloc.os $build_dir/elf/dl-deps.os $build_dir/elf/dl-runtime.os $build_dir/elf/dl-init.os $build_dir/elf/dl-fini.os $build_dir/elf/dl-debug.os $build_dir/elf/dl-misc.os $build_dir/elf/dl-version.os $build_dir/elf/dl-profile.os $build_dir/elf/dl-tls.os $build_dir/elf/dl-origin.os $build_dir/elf/dl-scope.os $build_dir/elf/dl-execstack.os $build_dir/elf/dl-open.os $build_dir/elf/dl-close.os $build_dir/elf/dl-trampoline.os $build_dir/elf/dl-exception.os $build_dir/elf/dl-sort-maps.os $build_dir/elf/dl-lookup-direct.os $build_dir/elf/dl-call-libc-early-init.os $build_dir/elf/dl-write.os $build_dir/elf/dl-thread_gscope_wait.os $build_dir/elf/dl-cache.os $build_dir/elf/dl-tunables.os $build_dir/elf/tlsdesc.os $build_dir/elf/dl-tlsdesc.os $build_dir/elf/tls_get_addr.os $build_dir/elf/dl-get-cpu-features.os $build_dir/elf/dl-sysdep.os $build_dir/elf/dl-environ.os $build_dir/elf/dl-minimal.os $build_dir/elf/dl-error-minimal.os $build_dir/elf/dl-conflict.os $build_dir/elf/dl-hwcaps.os $build_dir/elf/dl-hwcaps_split.os $build_dir/elf/dl-hwcaps-subdirs.os $build_dir/elf/dl-usage.os $build_dir/elf/dl-brk.os $build_dir/elf/dl-sbrk.os $build_dir/elf/dl-getcwd.os $build_dir/elf/dl-openat64.os $build_dir/elf/dl-opendir.os
