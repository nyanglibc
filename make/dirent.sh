printf "\
DIRENT**************************************************************************\n"
fns_pie_shared="\
opendir \
closedir \
readdir \
readdir_r \
rewinddir \
seekdir \
telldir \
scandir \
alphasort \
versionsort \
getdents \
getdents64 \
dirfd \
readdir64 \
readdir64_r \
scandir64 \
alphasort64 \
versionsort64 \
fdopendir \
scandirat \
scandirat64 \
scandir-cancel \
scandir-tail \
scandir64-tail \
getdirentries \
getdirentries64 \
"
fns_pie="\
$fns_pie_shared \
"

fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/dirent
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/dirent/$fn.s -o $build_dir/dirent/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/dirent/$fn.shared.s -o $build_dir/dirent/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'dirent/opendir.o dirent/closedir.o dirent/readdir.o dirent/readdir_r.o dirent/rewinddir.o dirent/seekdir.o dirent/telldir.o dirent/scandir.o dirent/alphasort.o dirent/versionsort.o dirent/getdents.o dirent/getdents64.o dirent/dirfd.o dirent/readdir64.o dirent/readdir64_r.o dirent/scandir64.o dirent/alphasort64.o dirent/versionsort64.o dirent/fdopendir.o dirent/scandirat.o dirent/scandirat64.o dirent/scandir-cancel.o dirent/scandir-tail.o dirent/scandir64-tail.o dirent/getdirentries.o dirent/getdirentries64.o\n' >$build_dir/dirent/stamp.o
printf 'dirent/opendir.os dirent/closedir.os dirent/readdir.os dirent/readdir_r.os dirent/rewinddir.os dirent/seekdir.os dirent/telldir.os dirent/scandir.os dirent/alphasort.os dirent/versionsort.os dirent/getdents.os dirent/getdents64.os dirent/dirfd.os dirent/readdir64.os dirent/readdir64_r.os dirent/scandir64.os dirent/alphasort64.os dirent/versionsort64.os dirent/fdopendir.os dirent/scandirat.os dirent/scandirat64.os dirent/scandir-cancel.os dirent/scandir-tail.os dirent/scandir64-tail.os dirent/getdirentries.os dirent/getdirentries64.os\n' >$build_dir/dirent/stamp.os
printf '' >$build_dir/dirent/stamp.oS
