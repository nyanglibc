printf "\
LIBSEGFAULT*********************************************************************\n"
mkdir -p $build_dir/debug/libSegFault
printf 'ASSEMBLING segfault.shared.s\n'
$as $src_dir/debug/libSegFault/segfault.shared.s \
	-o $build_dir/debug/libSegFault/segfault.os
printf 'CREATING LIBSEGFAULT.SO\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	-soname=libSegFault.so \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/debug/libSegFault/libSegFault.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/debug/libSegFault/segfault.os \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
