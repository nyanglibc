printf "\
SETJMP**************************************************************************\n"
fns="\
sigjmp \
longjmp \
jmp-unwind \
\
setjmp \
bsd-setjmp \
bsd-_setjmp \
__longjmp \
__longjmp_cancel \
"
mkdir -p $build_dir/setjmp
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/setjmp/$fn.s -o $build_dir/setjmp/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/setjmp/$fn.shared.s -o $build_dir/setjmp/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'setjmp/setjmp.o setjmp/sigjmp.o setjmp/bsd-setjmp.o setjmp/bsd-_setjmp.o setjmp/longjmp.o setjmp/__longjmp.o setjmp/jmp-unwind.o setjmp/__longjmp_cancel.o\n' >$build_dir/setjmp/stamp.o
printf 'setjmp/setjmp.os setjmp/sigjmp.os setjmp/bsd-setjmp.os setjmp/bsd-_setjmp.os setjmp/longjmp.os setjmp/__longjmp.os setjmp/jmp-unwind.os setjmp/__longjmp_cancel.os\n' >$build_dir/setjmp/stamp.os
printf '' >$build_dir/setjmp/stamp.oS
