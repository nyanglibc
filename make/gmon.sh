printf "\
GMON****************************************************************************\n"
fns_pie_shared="\
gmon \
mcount \
profil \
sprofil \
prof-freq \
_mcount \
"
mkdir -p $build_dir/gmon
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/gmon/$fn.s -o $build_dir/gmon/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/gmon/$fn.shared.s -o $build_dir/gmon/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'gmon/gmon.o gmon/mcount.o gmon/profil.o gmon/sprofil.o gmon/prof-freq.o gmon/_mcount.o\n' >$build_dir/gmon/stamp.o
printf 'gmon/gmon.os gmon/mcount.os gmon/profil.os gmon/sprofil.os gmon/prof-freq.os gmon/_mcount.os\n' >$build_dir/gmon/stamp.os
printf '' >$build_dir/gmon/stamp.oS
