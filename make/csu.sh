printf "\
CSU*****************************************************************************\n"
# ../sysdeps/unix/sysv/linux/x86_64/sysdep.S is empty
pie_shared_fns="\
check_fds \
init-first \
dso_handle \
errno \
errno-loc \
gmon-start \
libc-start \
start \
static-reloc \
version \
sysdep \
"
pie_fns="\
$pie_shared_fns \
abi-note \
crti \
crtn \
elf-init \
init \
libc-tls \
"
shared_fns="\
$pie_shared_fns \
unwind-resume \
"
nonshared_fns="\
elf-init \
"
mkdir -p $build_dir/csu
for fn in $pie_fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/csu/$fn.s -o $build_dir/csu/$fn.o
done
for fn in $shared_fns
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/csu/$fn.shared.s -o $build_dir/csu/$fn.os
done
for fn in $nonshared_fns
do
	printf "ASSEMBLING NONSHARED $fn\n"
	$as $src_dir/csu/$fn.nonshared.s -o $build_dir/csu/$fn.oS
done
#-------------------------------------------------------------------------------
printf "LD LINKING crt1.o\n"
# we remove eh 
$ld -m elf_x86_64 \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-o $build_dir/csu/crt1.o \
	-r \
	$build_dir/csu/start.o \
	$build_dir/csu/abi-note.o \
	$build_dir/csu/init.o \
	$build_dir/csu/static-reloc.o
#-------------------------------------------------------------------------------
printf "LD LINKING Scrt1.o\n"
# we remove eh 
$ld -m elf_x86_64 \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-o $build_dir/csu/Scrt1.o \
	-r \
	$build_dir/csu/start.os \
	$build_dir/csu/abi-note.o \
	$build_dir/csu/init.o
#-------------------------------------------------------------------------------
printf "LD LINKING rcrt1.o\n"
# we remove eh 
$ld  -m elf_x86_64 \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-o $build_dir/csu/rcrt1.o \
	-r \
	$build_dir/csu/start.o \
	$build_dir/csu/abi-note.o \
	$build_dir/csu/init.o
#-------------------------------------------------------------------------------
# Mcrt1.o is "null"
printf ".text\n" | $as --64 -o $build_dir/csu/Mcrt1.o -
#-------------------------------------------------------------------------------
printf 'LD LINKING gcrt1.o\n'
$ld -m elf_x86_64 \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-r \
	-o $build_dir/csu/gcrt1.o \
	$build_dir/csu/Scrt1.o \
	$build_dir/csu/gmon-start.os \
	$build_dir/csu/static-reloc.os
#-------------------------------------------------------------------------------
printf 'LD LINKING grctr1.o\n'
$ld  -m elf_x86_64 \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-o $build_dir/csu/grcrt1.o \
	-r \
	$build_dir/csu/rcrt1.o \
	$build_dir/csu/gmon-start.o
#-------------------------------------------------------------------------------
printf 'CREATING LIBC csu/stamp.o\n'
printf 'csu/init-first.o csu/libc-start.o csu/sysdep.o csu/version.o csu/check_fds.o csu/libc-tls.o csu/elf-init.o csu/dso_handle.o csu/errno.o csu/errno-loc.o\n' > $build_dir/csu/stamp.o
#-------------------------------------------------------------------------------
printf 'CREATING LIBC csu/stamp.os\n'
printf 'csu/init-first.os csu/libc-start.os csu/sysdep.os csu/version.os csu/check_fds.os csu/dso_handle.os csu/unwind-resume.os csu/errno.os csu/errno-loc.os\n' > $build_dir/csu/stamp.os
#-------------------------------------------------------------------------------
printf 'CREATING LIBC csu/stamp.oS\n' 
printf 'csu/elf-init.oS\n' > $build_dir/csu/stamp.oS
