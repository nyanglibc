printf "\
SOCKET**************************************************************************\n"
fns_pie_shared="\
accept \
connect \
recv \
recvfrom \
recvmsg \
send \
sendmsg \
sendto \
isfdtype \
opensock \
sockatmark \
accept4 \
recvmmsg \
sendmmsg \
sa_len \
cmsg_nxthdr \
bind \
getpeername \
getsockname \
getsockopt \
listen \
setsockopt \
shutdown \
socket \
socketpair \
"
fns_shared="\
"
mkdir -p $build_dir/socket
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/socket/$fn.s -o $build_dir/socket/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/socket/$fn.shared.s -o $build_dir/socket/$fn.os
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/socket/$fn.shared.s -o $build_dir/socket/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'socket/accept.o socket/bind.o socket/connect.o socket/getpeername.o socket/getsockname.o socket/getsockopt.o socket/listen.o socket/recv.o socket/recvfrom.o socket/recvmsg.o socket/send.o socket/sendmsg.o socket/sendto.o socket/setsockopt.o socket/shutdown.o socket/socket.o socket/socketpair.o socket/isfdtype.o socket/opensock.o socket/sockatmark.o socket/accept4.o socket/recvmmsg.o socket/sendmmsg.o socket/sa_len.o socket/cmsg_nxthdr.o\n' >$build_dir/socket/stamp.o
printf 'socket/accept.os socket/bind.os socket/connect.os socket/getpeername.os socket/getsockname.os socket/getsockopt.os socket/listen.os socket/recv.os socket/recvfrom.os socket/recvmsg.os socket/send.os socket/sendmsg.os socket/sendto.os socket/setsockopt.os socket/shutdown.os socket/socket.os socket/socketpair.os socket/isfdtype.os socket/opensock.os socket/sockatmark.os socket/accept4.os socket/recvmmsg.os socket/sendmmsg.os socket/sa_len.os socket/cmsg_nxthdr.os\n' >$build_dir/socket/stamp.os
printf '' >$build_dir/socket/stamp.oS
