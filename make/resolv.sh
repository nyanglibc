printf "\
RESOLV**************************************************************************\n"
fns_pie_shared="\
herror \
inet_addr \
inet_ntop \
inet_pton \
nsap_addr \
res_init \
res_hconf \
res_libc \
res-state \
res_randomid \
res-close \
resolv_context \
resolv_conf \
gai_sigqueue \
"
mkdir -p $build_dir/resolv
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/resolv/$fn.s -o $build_dir/resolv/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/resolv/$fn.shared.s -o $build_dir/resolv/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'resolv/herror.o resolv/inet_addr.o resolv/inet_ntop.o resolv/inet_pton.o resolv/nsap_addr.o resolv/res_init.o resolv/res_hconf.o resolv/res_libc.o resolv/res-state.o resolv/res_randomid.o resolv/res-close.o resolv/resolv_context.o resolv/resolv_conf.o resolv/gai_sigqueue.o\n' >$build_dir/resolv/stamp.o
printf 'resolv/herror.os resolv/inet_addr.os resolv/inet_ntop.os resolv/inet_pton.os resolv/nsap_addr.os resolv/res_init.os resolv/res_hconf.os resolv/res_libc.os resolv/res-state.os resolv/res_randomid.os resolv/res-close.os resolv/resolv_context.os resolv/resolv_conf.os resolv/gai_sigqueue.os\n' >$build_dir/resolv/stamp.os
printf '' >$build_dir/resolv/stamp.oS
