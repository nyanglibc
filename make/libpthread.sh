printf "\
LIBPTHREAD**********************************************************************\n"
mkdir -p $build_dir/nptl
printf 'CREATING LIBPTHREAD.A\n'
cd $build_dir/nptl
$ar cruv libpthread.a nptl-init.o nptlfreeres.o vars.o events.o pthread_create.o pthread_exit.o pthread_detach.o pthread_join.o pthread_tryjoin.o pthread_timedjoin.o pthread_clockjoin.o pthread_join_common.o pthread_yield.o pthread_getconcurrency.o pthread_setconcurrency.o pthread_setschedprio.o pthread_attr_getguardsize.o pthread_attr_setguardsize.o pthread_attr_getstackaddr.o pthread_attr_setstackaddr.o pthread_attr_getstacksize.o pthread_attr_setstacksize.o pthread_attr_getstack.o pthread_attr_setstack.o pthread_mutex_init.o pthread_mutex_destroy.o pthread_mutex_lock.o pthread_mutex_trylock.o pthread_mutex_timedlock.o pthread_mutex_unlock.o pthread_mutex_cond_lock.o pthread_mutexattr_init.o pthread_mutexattr_destroy.o pthread_mutexattr_getpshared.o pthread_mutexattr_setpshared.o pthread_mutexattr_gettype.o pthread_mutexattr_settype.o pthread_rwlock_init.o pthread_rwlock_destroy.o pthread_rwlock_rdlock.o pthread_rwlock_timedrdlock.o pthread_rwlock_clockrdlock.o pthread_rwlock_wrlock.o pthread_rwlock_timedwrlock.o pthread_rwlock_clockwrlock.o pthread_rwlock_tryrdlock.o pthread_rwlock_trywrlock.o pthread_rwlock_unlock.o pthread_rwlockattr_init.o pthread_rwlockattr_destroy.o pthread_rwlockattr_getpshared.o pthread_rwlockattr_setpshared.o pthread_rwlockattr_getkind_np.o pthread_rwlockattr_setkind_np.o pthread_cond_wait.o pthread_cond_signal.o pthread_cond_broadcast.o old_pthread_cond_wait.o old_pthread_cond_timedwait.o old_pthread_cond_signal.o old_pthread_cond_broadcast.o pthread_condattr_getpshared.o pthread_condattr_setpshared.o pthread_condattr_getclock.o pthread_condattr_setclock.o pthread_spin_init.o pthread_spin_destroy.o pthread_spin_lock.o pthread_spin_trylock.o pthread_spin_unlock.o pthread_barrier_init.o pthread_barrier_destroy.o pthread_barrier_wait.o pthread_barrierattr_init.o pthread_barrierattr_destroy.o pthread_barrierattr_getpshared.o pthread_barrierattr_setpshared.o pthread_key_create.o pthread_key_delete.o pthread_getspecific.o pthread_setspecific.o pthread_kill.o pthread_sigqueue.o pthread_cancel.o pthread_testcancel.o pthread_setcancelstate.o pthread_setcanceltype.o pthread_once.o old_pthread_atfork.o pthread_getcpuclockid.o shm-directory.o sem_init.o sem_destroy.o sem_open.o sem_close.o sem_unlink.o sem_getvalue.o sem_wait.o sem_timedwait.o sem_clockwait.o sem_post.o cleanup.o cleanup_defer.o cleanup_compat.o cleanup_defer_compat.o unwind.o pt-longjmp.o pt-cleanup.o cancellation.o lowlevellock.o pt-fork.o pt-fcntl.o write.o read.o close.o accept.o connect.o recv.o recvfrom.o send.o sendto.o fsync.o lseek.o lseek64.o msync.o open.o open64.o pause.o pread.o pread64.o pwrite.o pwrite64.o tcdrain.o msgrcv.o msgsnd.o sigwait.o sigsuspend.o recvmsg.o sendmsg.o pt-raise.o pt-system.o flockfile.o ftrylockfile.o funlockfile.o sigaction.o herrno.o res.o pthread_kill_other_threads.o pthread_setaffinity.o pthread_attr_getaffinity.o pthread_mutexattr_getrobust.o pthread_mutexattr_setrobust.o pthread_mutex_consistent.o cleanup_routine.o pthread_mutexattr_getprotocol.o pthread_mutexattr_setprotocol.o pthread_mutexattr_getprioceiling.o pthread_mutexattr_setprioceiling.o tpp.o pthread_mutex_getprioceiling.o pthread_mutex_setprioceiling.o pthread_setname.o pthread_getname.o pthread_setattr_default_np.o pthread_getattr_default_np.o pthread_mutex_conf.o libpthread-compat.o thrd_create.o thrd_detach.o thrd_exit.o thrd_join.o call_once.o mtx_destroy.o mtx_init.o mtx_lock.o mtx_timedlock.o mtx_trylock.o mtx_unlock.o cnd_broadcast.o cnd_destroy.o cnd_init.o cnd_signal.o cnd_timedwait.o cnd_wait.o tss_create.o tss_delete.o tss_get.o tss_set.o elision-lock.o elision-unlock.o elision-timed.o elision-trylock.o errno-loc.o futex-internal.o
printf 'CREATING LIBPTHREAD_PIC.A\n'
$ar cruv libpthread_pic.a nptl-init.os nptlfreeres.os vars.os events.os version.os pt-interp.os pthread_create.os pthread_exit.os pthread_detach.os pthread_join.os pthread_tryjoin.os pthread_timedjoin.os pthread_clockjoin.os pthread_join_common.os pthread_yield.os pthread_getconcurrency.os pthread_setconcurrency.os pthread_setschedprio.os pthread_attr_getguardsize.os pthread_attr_setguardsize.os pthread_attr_getstackaddr.os pthread_attr_setstackaddr.os pthread_attr_getstacksize.os pthread_attr_setstacksize.os pthread_attr_getstack.os pthread_attr_setstack.os pthread_mutex_init.os pthread_mutex_destroy.os pthread_mutex_lock.os pthread_mutex_trylock.os pthread_mutex_timedlock.os pthread_mutex_unlock.os pthread_mutex_cond_lock.os pthread_mutexattr_init.os pthread_mutexattr_destroy.os pthread_mutexattr_getpshared.os pthread_mutexattr_setpshared.os pthread_mutexattr_gettype.os pthread_mutexattr_settype.os pthread_rwlock_init.os pthread_rwlock_destroy.os pthread_rwlock_rdlock.os pthread_rwlock_timedrdlock.os pthread_rwlock_clockrdlock.os pthread_rwlock_wrlock.os pthread_rwlock_timedwrlock.os pthread_rwlock_clockwrlock.os pthread_rwlock_tryrdlock.os pthread_rwlock_trywrlock.os pthread_rwlock_unlock.os pthread_rwlockattr_init.os pthread_rwlockattr_destroy.os pthread_rwlockattr_getpshared.os pthread_rwlockattr_setpshared.os pthread_rwlockattr_getkind_np.os pthread_rwlockattr_setkind_np.os pthread_cond_wait.os pthread_cond_signal.os pthread_cond_broadcast.os old_pthread_cond_wait.os old_pthread_cond_timedwait.os old_pthread_cond_signal.os old_pthread_cond_broadcast.os pthread_condattr_getpshared.os pthread_condattr_setpshared.os pthread_condattr_getclock.os pthread_condattr_setclock.os pthread_spin_init.os pthread_spin_destroy.os pthread_spin_lock.os pthread_spin_trylock.os pthread_spin_unlock.os pthread_barrier_init.os pthread_barrier_destroy.os pthread_barrier_wait.os pthread_barrierattr_init.os pthread_barrierattr_destroy.os pthread_barrierattr_getpshared.os pthread_barrierattr_setpshared.os pthread_key_create.os pthread_key_delete.os pthread_getspecific.os pthread_setspecific.os pthread_kill.os pthread_sigqueue.os pthread_cancel.os pthread_testcancel.os pthread_setcancelstate.os pthread_setcanceltype.os pthread_once.os old_pthread_atfork.os pthread_getcpuclockid.os shm-directory.os sem_init.os sem_destroy.os sem_open.os sem_close.os sem_unlink.os sem_getvalue.os sem_wait.os sem_timedwait.os sem_clockwait.os sem_post.os cleanup.os cleanup_defer.os cleanup_compat.os cleanup_defer_compat.os unwind.os pt-longjmp.os pt-cleanup.os cancellation.os lowlevellock.os pt-fork.os pt-fcntl.os write.os read.os close.os accept.os connect.os recv.os recvfrom.os send.os sendto.os fsync.os lseek.os lseek64.os msync.os open.os open64.os pause.os pread.os pread64.os pwrite.os pwrite64.os tcdrain.os msgrcv.os msgsnd.os sigwait.os sigsuspend.os recvmsg.os sendmsg.os pt-raise.os pt-system.os flockfile.os ftrylockfile.os funlockfile.os sigaction.os herrno.os res.os pt-allocrtsig.os pthread_kill_other_threads.os pthread_setaffinity.os pthread_attr_getaffinity.os pthread_mutexattr_getrobust.os pthread_mutexattr_setrobust.os pthread_mutex_consistent.os cleanup_routine.os unwind-forcedunwind.os pthread_mutexattr_getprotocol.os pthread_mutexattr_setprotocol.os pthread_mutexattr_getprioceiling.os pthread_mutexattr_setprioceiling.os tpp.os pthread_mutex_getprioceiling.os pthread_mutex_setprioceiling.os pthread_setname.os pthread_getname.os pthread_setattr_default_np.os pthread_getattr_default_np.os pthread_mutex_conf.os libpthread-compat.os thrd_create.os thrd_detach.os thrd_exit.os thrd_join.os call_once.os mtx_destroy.os mtx_init.os mtx_lock.os mtx_timedlock.os mtx_trylock.os mtx_unlock.os cnd_broadcast.os cnd_destroy.os cnd_init.os cnd_signal.os cnd_timedwait.os cnd_wait.os tss_create.os tss_delete.os tss_get.os tss_set.os elision-lock.os elision-unlock.os elision-timed.os elision-trylock.os errno-loc.os futex-internal.os
printf 'CREATING LIBPTHREAD.SO.0\n'
# we need libgcc crtstartS.o crtendS.o
#
#/nyan/toolchains/current/bin/x86_64-nyan-linux-gnu-gcc
#-B/nyan/toolchains/current/bin/   -shared -static-libgcc -Wl,-O1  -Wl,-z,defs
#-Wl,-dynamic-linker=/nyan/nyanglibc/current/lib/ld-linux-x86-64.so.2
#-B/run/nyanglibc/build/nptl/ -B/run/nyanglibc/build/csu/
#-B/run/nyanglibc/build/nptl/
#-Wl,--version-script=/run/nyanglibc/build/libpthread.map
#-Wl,-soname=libpthread.so.0 -Wl,-z,combreloc -Wl,-z,relro -Wl,--hash-style=both
#-Wl,--enable-new-dtags,-z,nodelete,-z,initfirst -e __nptl_main
#-L/run/nyanglibc/build -L/run/nyanglibc/build/math -L/run/nyanglibc/build/elf
#-L/run/nyanglibc/build/dlfcn -L/run/nyanglibc/build/nss
#-L/run/nyanglibc/build/nis -L/run/nyanglibc/build/rt
#-L/run/nyanglibc/build/resolv -L/run/nyanglibc/build/mathvec
#-L/run/nyanglibc/build/support -L/run/nyanglibc/build/crypt
#-L/run/nyanglibc/build/nptl
#-Wl,-rpath-link=/run/nyanglibc/build:/run/nyanglibc/build/math:/run/nyanglibc/build/elf:/run/nyanglibc/build/dlfcn:/run/nyanglibc/build/nss:/run/nyanglibc/build/nis:/run/nyanglibc/build/rt:/run/nyanglibc/build/resolv:/run/nyanglibc/build/mathvec:/run/nyanglibc/build/support:/run/nyanglibc/build/crypt:/run/nyanglibc/build/nptl
#-o /run/nyanglibc/build/nptl/libpthread.so -T /run/nyanglibc/build/shlib.lds
#/run/nyanglibc/build/csu/abi-note.o -Wl,--whole-archive
#/run/nyanglibc/build/nptl/libpthread_pic.a -Wl,--no-whole-archive
#-Wl,--start-group /run/nyanglibc/build/libc.so
#/run/nyanglibc/build/libc_nonshared.a -Wl,--as-needed
#/run/nyanglibc/build/elf/ld.so -Wl,--no-as-needed -Wl,--end-group
#
#STRACE OF ABOVE IN ORDER TO KNOW HOW THE GCC DRIVER INVOKES LD
#
#/nyan/strace/current/bin/strace -o /run/gcc -ff -s 4096 /nyan/toolchains/current/bin/x86_64-nyan-linux-gnu-gcc \
#-B/nyan/toolchains/current/bin/   -shared -static-libgcc -Wl,-O1  -Wl,-z,defs \
#-Wl,-dynamic-linker=/nyan/nyanglibc/current/lib/ld-linux-x86-64.so.2 \
#-B$build_dir/nptl/ -B$build_dir/csu/ \
#-B$build_dir/nptl/ \
#-Wl,--version-script=$src_dir/libpthread.versions.map \
#-Wl,-soname=libpthread.so.0 -Wl,-z,combreloc -Wl,-z,relro -Wl,--hash-style=both \
#-Wl,--enable-new-dtags,-z,nodelete,-z,initfirst -e __nptl_main \
#-L$build_dir -L$build_dir/math -L$build_dir/elf \
#-L$build_dir/dlfcn -L$build_dir/nss \
#-L$build_dir/nis -L$build_dir/rt \
#-L$build_dir/resolv -L$build_dir/mathvec \
#-L$build_dir/support -L$build_dir/crypt \
#-L$build_dir/nptl \
#-Wl,-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
#-o $build_dir/nptl/libpthread.so -T $src_dir/shlib.lds \
#$build_dir/csu/abi-note.o -Wl,--whole-archive \
#$build_dir/nptl/libpthread_pic.a -Wl,--no-whole-archive \
#-Wl,--start-group $build_dir/libc.so \
#$build_dir/libc_nonshared.a -Wl,--as-needed \
#$build_dir/elf/ld-linux-x86-64.so.2 -Wl,--no-as-needed -Wl,--end-group
#$ld \
#	--eh-frame-hdr -m elf_x86_64 -shared \
#	-o $build_dir/nptl/libpthread.so \
#	-e __nptl_main \
#	$build_dir/csu/crti.o \
#	/nyan/toolchains/0/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/crtbeginS.o \
#	-L$build_dir \
#	-L$build_dir/math \
#	-L$build_dir/elf \
#	-L$build_dir/dlfcn \
#	-L$build_dir/nss \
#	-L$build_dir/nis \
#	-L$build_dir/rt \
#	-L$build_dir/resolv \
#	-L$build_dir/mathvec \
#	-L$build_dir/support \
#	-L$build_dir/crypt \
#	-L$build_dir/nptl \
#	-L/nyan/toolchains/current/bin \
#	-L$build_dir/nptl \
#	-L$build_dir/csu \
#	-L$build_dir/nptl \
#	-L/nyan/toolchains/0/lib/gcc/x86_64-nyan-linux-gnu/7.3.0 \
#	-L/nyan/toolchains/0/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/../../../../lib64 \
#	-L/lib/../lib64 \
#	-L/nyan/toolchains/0/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/../../../../x86_64-nyan-linux-gnu/lib \
#	-L/nyan/toolchains/0/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/../../.. \
#	-O1 \
#	-z defs \
#	-dynamic-linker=/nyan/nyanglibc/current/lib/ld-linux-x86-64.so.2 \
#	--version-script=$src_dir/libpthread.versions.map \
#	-soname=libpthread.so.0 \
#	-z combreloc \
#	-z relro \
#	--hash-style=both \
#	--enable-new-dtags \
#	-z nodelete \
#	-z initfirst \
#	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
#	/run/build/csu/abi-note.o \
#	--whole-archive \
#		$build_dir/nptl/libpthread_pic.a \
#	--no-whole-archive \
#	--start-group \
#		$build_dir/libc.so \
#		$build_dir/libc_nonshared.a \
#		--as-needed \
#			$build_dir/elf/ld-linux-x86-64.so.2 \
#		--no-as-needed \
#	--end-group \
#	-lgcc \
#	-lgcc_eh \
#	-lc \
#	-lgcc \
#	-lgcc_eh \
#	/nyan/toolchains/0/lib/gcc/x86_64-nyan-linux-gnu/7.3.0/crtendS.o \
#	$build_dir/nptl/crtn.o \
#	-T $src_dir/shlib.lds
# XXX:nptl/crti.o is a symlink to nptl/pt-crti.o and the -B paths give the crti
# from nptl a higher priority than the crti from the csu
$ld \
	-s \
	-shared \
	-o $build_dir/nptl/libpthread.so \
	-e __nptl_main \
	$build_dir/nptl/pt-crti.o \
	$build_dir/libgcc/libpthread/crtbeginS.o \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-L$build_dir/nptl \
	-L$build_dir/csu \
	-L$build_dir/nptl \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	--version-script=$src_dir/libpthread.versions.map \
	-soname=libpthread.so.0 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	--enable-new-dtags \
	-z nodelete \
	-z initfirst \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/nptl/libpthread_pic.a \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group \
	$build_dir/libgcc/libpthread/crtendS.o \
	$build_dir/nptl/crtn.o \
	-T $src_dir/shlib.lds
