printf "\
SUNRPC**************************************************************************\n"
fns_compat="\
auth_des \
auth_unix \
clnt_gen \
clnt_perr \
clnt_tcp \
clnt_udp \
get_myaddr \
key_call \
netname \
pm_getport \
rpc_thread \
svc \
svc_tcp \
svc_udp \
xcrypt \
xdr_array \
xdr \
xdr_intXX_t \
xdr_mem \
xdr_ref \
xdr_sizeof \
xdr_stdio \
svc_run \
"
fns_shared="\
$fns_compat \
auth_none \
authuxprot \
clnt_raw \
clnt_simp \
rpc_dtable \
getrpcport \
pmap_clnt \
pm_getmaps \
pmap_prot \
pmap_prot2 \
pmap_rmt \
rpc_prot \
rpc_common \
rpc_cmsg \
svc_auth \
svc_authux \
svc_raw \
svc_simple \
xdr_float \
xdr_rec \
publickey \
authdes_prot \
des_crypt \
des_impl \
des_soft \
key_prot \
openchild \
rtime \
svcauth_des \
clnt_unix \
svc_unix \
create_xid \
rpc_gethostbyname \
"
mkdir -p $build_dir/sunrpc
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/sunrpc/$fn.shared.s -o $build_dir/sunrpc/$fn.os
done
for fn in $fns_compat
do
	printf "ASSEMBLING COMPAT $fn\n"
	$as $src_dir/sunrpc/compat-$fn.shared.s -o $build_dir/sunrpc/compat-$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf '' >$build_dir/sunrpc/stamp.o
printf 'sunrpc/auth_none.os sunrpc/authuxprot.os sunrpc/clnt_raw.os sunrpc/clnt_simp.os sunrpc/rpc_dtable.os sunrpc/getrpcport.os sunrpc/pmap_clnt.os sunrpc/pm_getmaps.os sunrpc/pmap_prot.os sunrpc/pmap_prot2.os sunrpc/pmap_rmt.os sunrpc/rpc_prot.os sunrpc/rpc_common.os sunrpc/rpc_cmsg.os sunrpc/svc_auth.os sunrpc/svc_authux.os sunrpc/svc_raw.os sunrpc/svc_simple.os sunrpc/xdr_float.os sunrpc/xdr_rec.os sunrpc/publickey.os sunrpc/authdes_prot.os sunrpc/des_crypt.os sunrpc/des_impl.os sunrpc/des_soft.os sunrpc/key_prot.os sunrpc/openchild.os sunrpc/rtime.os sunrpc/svcauth_des.os sunrpc/clnt_unix.os sunrpc/svc_unix.os sunrpc/create_xid.os sunrpc/auth_des.os sunrpc/auth_unix.os sunrpc/clnt_gen.os sunrpc/clnt_perr.os sunrpc/clnt_tcp.os sunrpc/clnt_udp.os sunrpc/get_myaddr.os sunrpc/key_call.os sunrpc/netname.os sunrpc/pm_getport.os sunrpc/rpc_thread.os sunrpc/svc.os sunrpc/svc_tcp.os sunrpc/svc_udp.os sunrpc/xcrypt.os sunrpc/xdr_array.os sunrpc/xdr.os sunrpc/xdr_intXX_t.os sunrpc/xdr_mem.os sunrpc/xdr_ref.os sunrpc/xdr_sizeof.os sunrpc/xdr_stdio.os sunrpc/svc_run.os sunrpc/rpc_gethostbyname.os\n' >$build_dir/sunrpc/stamp.os
printf '' >$build_dir/sunrpc/stamp.oS
$ar crv $build_dir/sunrpc/librpc_compat_pic.a $build_dir/sunrpc/compat-auth_des.os $build_dir/sunrpc/compat-auth_unix.os $build_dir/sunrpc/compat-clnt_gen.os $build_dir/sunrpc/compat-clnt_perr.os $build_dir/sunrpc/compat-clnt_tcp.os $build_dir/sunrpc/compat-clnt_udp.os $build_dir/sunrpc/compat-get_myaddr.os $build_dir/sunrpc/compat-key_call.os $build_dir/sunrpc/compat-netname.os $build_dir/sunrpc/compat-pm_getport.os $build_dir/sunrpc/compat-rpc_thread.os $build_dir/sunrpc/compat-svc.os $build_dir/sunrpc/compat-svc_tcp.os $build_dir/sunrpc/compat-svc_udp.os $build_dir/sunrpc/compat-xcrypt.os $build_dir/sunrpc/compat-xdr_array.os $build_dir/sunrpc/compat-xdr.os $build_dir/sunrpc/compat-xdr_intXX_t.os $build_dir/sunrpc/compat-xdr_mem.os $build_dir/sunrpc/compat-xdr_ref.os $build_dir/sunrpc/compat-xdr_sizeof.os $build_dir/sunrpc/compat-xdr_stdio.os $build_dir/sunrpc/compat-svc_run.os
