printf "\
NSS LIBNSS_COMPAT***************************************************************\n"
mkdir -p $build_dir/nss/libnss_compat
fns_shared="\
compat-grp \
compat-pwd \
compat-spwd \
compat-initgroups \
nisdomain \
"
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/nss/libnss_compat/$fn.shared.s -o $build_dir/nss/libnss_compat/$fn.os
done
cd $build_dir/nss/libnss_compat
printf 'CREATING LIBNSS_COMPAT_PIC.A\n'
$ar cruv libnss_compat_pic.a compat-grp.os compat-pwd.os compat-spwd.os compat-initgroups.os nisdomain.os
printf 'CREATING LIBNSS_COMPAT.SO.2\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libnss_compat.versions.map \
	-soname=libnss_compat.so.2 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/nss/libnss_compat/libnss_compat.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/nss/libnss_compat/libnss_compat_pic.a \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
