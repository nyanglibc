printf "\
LOGIN LIBUTIL*******************************************************************\n"
mkdir -p $build_dir/login/libutil
fns_pie_shared="\
login \
login_tty \
logout \
logwtmp \
openpty \
forkpty \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/login/libutil/$fn.s -o $build_dir/login/libutil/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/login/libutil/$fn.shared.s -o $build_dir/login/libutil/$fn.os
done
printf 'CREATING LIBUTIL.A\n'
cd $build_dir/login/libutil
$ar cruv libutil.a login.o login_tty.o logout.o logwtmp.o openpty.o forkpty.o
printf 'CREATING LIBUTIL_PIC.A\n'
$ar cruv libutil_pic.a login.os login_tty.os logout.os logwtmp.os openpty.os forkpty.os
printf 'CREATING LIBUTIL.SO.1\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libutil.versions.map \
	-soname=libutil.so.1 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/login/libutil/libutil.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/login/libutil/libutil_pic.a \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
