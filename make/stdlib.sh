printf "\
STDLIB**************************************************************************\n"
fns_pie_shared="\
atof \
atoi \
atol \
atoll \
abort \
bsearch \
qsort \
msort \
getenv \
putenv \
setenv \
secure-getenv \
exit \
on_exit \
cxa_atexit \
cxa_finalize \
old_atexit \
quick_exit \
cxa_at_quick_exit \
cxa_thread_atexit_impl \
abs \
labs \
llabs \
div \
ldiv \
lldiv \
mblen \
mbstowcs \
mbtowc \
wcstombs \
wctomb \
random \
random_r \
rand \
rand_r \
drand48 \
erand48 \
lrand48 \
nrand48 \
mrand48 \
jrand48 \
srand48 \
seed48 \
lcong48 \
drand48_r \
erand48_r \
lrand48_r \
nrand48_r \
mrand48_r \
jrand48_r \
srand48_r \
seed48_r \
lcong48_r \
drand48-iter \
getrandom \
getentropy \
strfromf \
strfromd \
strfroml \
strtol \
strtoul \
strtoll \
strtoull \
strtol_l \
strtoul_l \
strtoll_l \
strtoull_l \
strtof \
strtod \
strtold \
strtof_l \
strtod_l \
strtold_l \
strtof_nan \
strtod_nan \
strtold_nan \
system \
canonicalize \
a64l \
l64a \
rpmatch \
strfmon \
strfmon_l \
getsubopt \
xpg_basename \
fmtmsg \
makecontext \
inlines \
cmp \
divmod_1 \
divrem \
udiv_qrnnd \
mod_1 \
mul \
mul_n \
dbl2mpn \
ldbl2mpn \
mpn2flt \
mpn2dbl \
mpn2ldbl \
float1282mpn \
strfromf128 \
strtof128 \
strtof128_l \
strtof128_nan \
mpn2float128 \
grouping \
groupingwc \
tens_in_limb \
fpioconst \
mp_clz_tab \
\
getcontext \
setcontext \
swapcontext \
add_n \
addmul_1 \
lshift \
rshift \
mul_1 \
sub_n \
submul_1 \
__start_context \
"
fns_pie="\
$fns_pie_shared \
atexit \
at_quick_exit \
"

fns_shared="\
$fns_pie_shared \
"
fns_nonshared="\
atexit \
at_quick_exit \
"

mkdir -p $build_dir/stdlib
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/stdlib/$fn.s -o $build_dir/stdlib/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/stdlib/$fn.shared.s -o $build_dir/stdlib/$fn.os
done
for fn in $fns_nonshared
do
	printf "ASSEMBLING NONSHARED $fn\n"
	$as $src_dir/stdlib/$fn.nonshared.s -o $build_dir/stdlib/$fn.oS
done
printf 'CREATING STAMPS FILES\n'
printf 'stdlib/atof.o stdlib/atoi.o stdlib/atol.o stdlib/atoll.o stdlib/abort.o stdlib/bsearch.o stdlib/qsort.o stdlib/msort.o stdlib/getenv.o stdlib/putenv.o stdlib/setenv.o stdlib/secure-getenv.o stdlib/exit.o stdlib/on_exit.o stdlib/atexit.o stdlib/cxa_atexit.o stdlib/cxa_finalize.o stdlib/old_atexit.o stdlib/quick_exit.o stdlib/at_quick_exit.o stdlib/cxa_at_quick_exit.o stdlib/cxa_thread_atexit_impl.o stdlib/abs.o stdlib/labs.o stdlib/llabs.o stdlib/div.o stdlib/ldiv.o stdlib/lldiv.o stdlib/mblen.o stdlib/mbstowcs.o stdlib/mbtowc.o stdlib/wcstombs.o stdlib/wctomb.o stdlib/random.o stdlib/random_r.o stdlib/rand.o stdlib/rand_r.o stdlib/drand48.o stdlib/erand48.o stdlib/lrand48.o stdlib/nrand48.o stdlib/mrand48.o stdlib/jrand48.o stdlib/srand48.o stdlib/seed48.o stdlib/lcong48.o stdlib/drand48_r.o stdlib/erand48_r.o stdlib/lrand48_r.o stdlib/nrand48_r.o stdlib/mrand48_r.o stdlib/jrand48_r.o stdlib/srand48_r.o stdlib/seed48_r.o stdlib/lcong48_r.o stdlib/drand48-iter.o stdlib/getrandom.o stdlib/getentropy.o stdlib/strfromf.o stdlib/strfromd.o stdlib/strfroml.o stdlib/strtol.o stdlib/strtoul.o stdlib/strtoll.o stdlib/strtoull.o stdlib/strtol_l.o stdlib/strtoul_l.o stdlib/strtoll_l.o stdlib/strtoull_l.o stdlib/strtof.o stdlib/strtod.o stdlib/strtold.o stdlib/strtof_l.o stdlib/strtod_l.o stdlib/strtold_l.o stdlib/strtof_nan.o stdlib/strtod_nan.o stdlib/strtold_nan.o stdlib/system.o stdlib/canonicalize.o stdlib/a64l.o stdlib/l64a.o stdlib/rpmatch.o stdlib/strfmon.o stdlib/strfmon_l.o stdlib/getsubopt.o stdlib/xpg_basename.o stdlib/fmtmsg.o stdlib/getcontext.o stdlib/setcontext.o stdlib/makecontext.o stdlib/swapcontext.o stdlib/inlines.o stdlib/add_n.o stdlib/addmul_1.o stdlib/cmp.o stdlib/divmod_1.o stdlib/divrem.o stdlib/udiv_qrnnd.o stdlib/lshift.o stdlib/rshift.o stdlib/mod_1.o stdlib/mul.o stdlib/mul_1.o stdlib/mul_n.o stdlib/sub_n.o stdlib/submul_1.o stdlib/dbl2mpn.o stdlib/ldbl2mpn.o stdlib/mpn2flt.o stdlib/mpn2dbl.o stdlib/mpn2ldbl.o stdlib/float1282mpn.o stdlib/strfromf128.o stdlib/strtof128.o stdlib/strtof128_l.o stdlib/strtof128_nan.o stdlib/mpn2float128.o stdlib/grouping.o stdlib/groupingwc.o stdlib/tens_in_limb.o stdlib/fpioconst.o stdlib/mp_clz_tab.o stdlib/__start_context.o\n' >$build_dir/stdlib/stamp.o
printf 'stdlib/atof.os stdlib/atoi.os stdlib/atol.os stdlib/atoll.os stdlib/abort.os stdlib/bsearch.os stdlib/qsort.os stdlib/msort.os stdlib/getenv.os stdlib/putenv.os stdlib/setenv.os stdlib/secure-getenv.os stdlib/exit.os stdlib/on_exit.os stdlib/cxa_atexit.os stdlib/cxa_finalize.os stdlib/old_atexit.os stdlib/quick_exit.os stdlib/cxa_at_quick_exit.os stdlib/cxa_thread_atexit_impl.os stdlib/abs.os stdlib/labs.os stdlib/llabs.os stdlib/div.os stdlib/ldiv.os stdlib/lldiv.os stdlib/mblen.os stdlib/mbstowcs.os stdlib/mbtowc.os stdlib/wcstombs.os stdlib/wctomb.os stdlib/random.os stdlib/random_r.os stdlib/rand.os stdlib/rand_r.os stdlib/drand48.os stdlib/erand48.os stdlib/lrand48.os stdlib/nrand48.os stdlib/mrand48.os stdlib/jrand48.os stdlib/srand48.os stdlib/seed48.os stdlib/lcong48.os stdlib/drand48_r.os stdlib/erand48_r.os stdlib/lrand48_r.os stdlib/nrand48_r.os stdlib/mrand48_r.os stdlib/jrand48_r.os stdlib/srand48_r.os stdlib/seed48_r.os stdlib/lcong48_r.os stdlib/drand48-iter.os stdlib/getrandom.os stdlib/getentropy.os stdlib/strfromf.os stdlib/strfromd.os stdlib/strfroml.os stdlib/strtol.os stdlib/strtoul.os stdlib/strtoll.os stdlib/strtoull.os stdlib/strtol_l.os stdlib/strtoul_l.os stdlib/strtoll_l.os stdlib/strtoull_l.os stdlib/strtof.os stdlib/strtod.os stdlib/strtold.os stdlib/strtof_l.os stdlib/strtod_l.os stdlib/strtold_l.os stdlib/strtof_nan.os stdlib/strtod_nan.os stdlib/strtold_nan.os stdlib/system.os stdlib/canonicalize.os stdlib/a64l.os stdlib/l64a.os stdlib/rpmatch.os stdlib/strfmon.os stdlib/strfmon_l.os stdlib/getsubopt.os stdlib/xpg_basename.os stdlib/fmtmsg.os stdlib/getcontext.os stdlib/setcontext.os stdlib/makecontext.os stdlib/swapcontext.os stdlib/inlines.os stdlib/add_n.os stdlib/addmul_1.os stdlib/cmp.os stdlib/divmod_1.os stdlib/divrem.os stdlib/udiv_qrnnd.os stdlib/lshift.os stdlib/rshift.os stdlib/mod_1.os stdlib/mul.os stdlib/mul_1.os stdlib/mul_n.os stdlib/sub_n.os stdlib/submul_1.os stdlib/dbl2mpn.os stdlib/ldbl2mpn.os stdlib/mpn2flt.os stdlib/mpn2dbl.os stdlib/mpn2ldbl.os stdlib/float1282mpn.os stdlib/strfromf128.os stdlib/strtof128.os stdlib/strtof128_l.os stdlib/strtof128_nan.os stdlib/mpn2float128.os stdlib/grouping.os stdlib/groupingwc.os stdlib/tens_in_limb.os stdlib/fpioconst.os stdlib/mp_clz_tab.os stdlib/__start_context.os\n' >$build_dir/stdlib/stamp.os
printf 'stdlib/atexit.oS stdlib/at_quick_exit.oS\n' >$build_dir/stdlib/stamp.oS
