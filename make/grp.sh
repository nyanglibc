printf "\
GRP*****************************************************************************\n"
fns_pie_shared="\
fgetgrent \
initgroups \
setgroups \
getgrent \
getgrgid \
getgrnam \
putgrent \
getgrent_r \
getgrgid_r \
getgrnam_r \
fgetgrent_r \
grp-merge \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/grp
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/grp/$fn.s -o $build_dir/grp/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/grp/$fn.shared.s -o $build_dir/grp/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'grp/fgetgrent.o grp/initgroups.o grp/setgroups.o grp/getgrent.o grp/getgrgid.o grp/getgrnam.o grp/putgrent.o grp/getgrent_r.o grp/getgrgid_r.o grp/getgrnam_r.o grp/fgetgrent_r.o grp/grp-merge.o\n' >$build_dir/grp/stamp.o
printf 'grp/fgetgrent.os grp/initgroups.os grp/setgroups.os grp/getgrent.os grp/getgrgid.os grp/getgrnam.os grp/putgrent.os grp/getgrent_r.os grp/getgrgid_r.os grp/getgrnam_r.os grp/fgetgrent_r.os grp/grp-merge.os\n' >$build_dir/grp/stamp.os
printf '' >$build_dir/grp/stamp.oS
