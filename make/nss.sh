printf "\
NSS*****************************************************************************\n"
fns_pie_shared="\
nsswitch \
getnssent \
getnssent_r \
digits_dots \
valid_field \
valid_list_field \
rewrite_field \
proto-lookup \
service-lookup \
hosts-lookup \
network-lookup \
grp-lookup \
pwd-lookup \
ethers-lookup \
spwd-lookup \
netgrp-lookup \
alias-lookup \
sgrp-lookup \
key-lookup \
rpc-lookup \
compat-lookup \
nss_hash \
nss_files_fopen \
nss_readline \
nss_parse_line_result \
nss_fgetent_r \
nss_module \
nss_action \
nss_action_parse \
nss_database \
"
mkdir -p $build_dir/nss
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/nss/$fn.s -o $build_dir/nss/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/nss/$fn.shared.s -o $build_dir/nss/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'nss/nsswitch.o nss/getnssent.o nss/getnssent_r.o nss/digits_dots.o nss/valid_field.o nss/valid_list_field.o nss/rewrite_field.o nss/proto-lookup.o nss/service-lookup.o nss/hosts-lookup.o nss/network-lookup.o nss/grp-lookup.o nss/pwd-lookup.o nss/ethers-lookup.o nss/spwd-lookup.o nss/netgrp-lookup.o nss/alias-lookup.o nss/sgrp-lookup.o nss/key-lookup.o nss/rpc-lookup.o nss/compat-lookup.o nss/nss_hash.o nss/nss_files_fopen.o nss/nss_readline.o nss/nss_parse_line_result.o nss/nss_fgetent_r.o nss/nss_module.o nss/nss_action.o nss/nss_action_parse.o nss/nss_database.o\n' >$build_dir/nss/stamp.o
printf 'nss/nsswitch.os nss/getnssent.os nss/getnssent_r.os nss/digits_dots.os nss/valid_field.os nss/valid_list_field.os nss/rewrite_field.os nss/proto-lookup.os nss/service-lookup.os nss/hosts-lookup.os nss/network-lookup.os nss/grp-lookup.os nss/pwd-lookup.os nss/ethers-lookup.os nss/spwd-lookup.os nss/netgrp-lookup.os nss/alias-lookup.os nss/sgrp-lookup.os nss/key-lookup.os nss/rpc-lookup.os nss/compat-lookup.os nss/nss_hash.os nss/nss_files_fopen.os nss/nss_readline.os nss/nss_parse_line_result.os nss/nss_fgetent_r.os nss/nss_module.os nss/nss_action.os nss/nss_action_parse.os nss/nss_database.os\n' >$build_dir/nss/stamp.os
printf '' >$build_dir/nss/stamp.oS
