printf "\
DEBUG***************************************************************************\n"
fns_pie_shared="\
backtrace \
backtracesyms \
backtracesymsfd \
noophooks \
stpcpy_chk \
strcat_chk \
strcpy_chk \
strncat_chk \
strncpy_chk \
stpncpy_chk \
sprintf_chk \
vsprintf_chk \
snprintf_chk \
vsnprintf_chk \
printf_chk \
fprintf_chk \
vprintf_chk \
vfprintf_chk \
gets_chk \
chk_fail \
readonly-area \
fgets_chk \
fgets_u_chk \
read_chk \
pread_chk \
pread64_chk \
recv_chk \
recvfrom_chk \
readlink_chk \
readlinkat_chk \
getwd_chk \
getcwd_chk \
realpath_chk \
fread_chk \
fread_u_chk \
wctomb_chk \
wcscpy_chk \
wmemcpy_chk \
wmemmove_chk \
wmempcpy_chk \
wcpcpy_chk \
wcsncpy_chk \
wcscat_chk \
wcsncat_chk \
wcpncpy_chk \
swprintf_chk \
vswprintf_chk \
wprintf_chk \
fwprintf_chk \
vwprintf_chk \
vfwprintf_chk \
fgetws_chk \
fgetws_u_chk \
confstr_chk \
getgroups_chk \
ttyname_r_chk \
gethostname_chk \
getdomainname_chk \
wcrtomb_chk \
mbsnrtowcs_chk \
wcsnrtombs_chk \
mbsrtowcs_chk \
wcsrtombs_chk \
mbstowcs_chk \
wcstombs_chk \
asprintf_chk \
vasprintf_chk \
dprintf_chk \
vdprintf_chk \
obprintf_chk \
vobprintf_chk \
longjmp_chk \
fdelt_chk \
poll_chk \
ppoll_chk \
explicit_bzero_chk \
stack_chk_fail \
fortify_fail \
\
memcpy_chk \
memmove_chk \
mempcpy_chk \
memset_chk \
wmemset_chk \
____longjmp_chk \
"
fns_nonshared="\
stack_chk_fail_local \
"
mkdir -p $build_dir/debug
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/debug/$fn.s -o $build_dir/debug/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/debug/$fn.shared.s -o $build_dir/debug/$fn.os
done
for fn in $fns_nonshared
do
	printf "ASSEMBLING NONSHARED $fn\n"
	$as $src_dir/debug/$fn.nonshared.s -o $build_dir/debug/$fn.oS
done
printf 'CREATING STAMPS FILES\n'
printf 'debug/backtrace.o debug/backtracesyms.o debug/backtracesymsfd.o debug/noophooks.o debug/memcpy_chk.o debug/memmove_chk.o debug/mempcpy_chk.o debug/memset_chk.o debug/stpcpy_chk.o debug/strcat_chk.o debug/strcpy_chk.o debug/strncat_chk.o debug/strncpy_chk.o debug/stpncpy_chk.o debug/sprintf_chk.o debug/vsprintf_chk.o debug/snprintf_chk.o debug/vsnprintf_chk.o debug/printf_chk.o debug/fprintf_chk.o debug/vprintf_chk.o debug/vfprintf_chk.o debug/gets_chk.o debug/chk_fail.o debug/readonly-area.o debug/fgets_chk.o debug/fgets_u_chk.o debug/read_chk.o debug/pread_chk.o debug/pread64_chk.o debug/recv_chk.o debug/recvfrom_chk.o debug/readlink_chk.o debug/readlinkat_chk.o debug/getwd_chk.o debug/getcwd_chk.o debug/realpath_chk.o debug/fread_chk.o debug/fread_u_chk.o debug/wctomb_chk.o debug/wcscpy_chk.o debug/wmemcpy_chk.o debug/wmemmove_chk.o debug/wmempcpy_chk.o debug/wcpcpy_chk.o debug/wcsncpy_chk.o debug/wcscat_chk.o debug/wcsncat_chk.o debug/wmemset_chk.o debug/wcpncpy_chk.o debug/swprintf_chk.o debug/vswprintf_chk.o debug/wprintf_chk.o debug/fwprintf_chk.o debug/vwprintf_chk.o debug/vfwprintf_chk.o debug/fgetws_chk.o debug/fgetws_u_chk.o debug/confstr_chk.o debug/getgroups_chk.o debug/ttyname_r_chk.o debug/gethostname_chk.o debug/getdomainname_chk.o debug/wcrtomb_chk.o debug/mbsnrtowcs_chk.o debug/wcsnrtombs_chk.o debug/mbsrtowcs_chk.o debug/wcsrtombs_chk.o debug/mbstowcs_chk.o debug/wcstombs_chk.o debug/asprintf_chk.o debug/vasprintf_chk.o debug/dprintf_chk.o debug/vdprintf_chk.o debug/obprintf_chk.o debug/vobprintf_chk.o debug/longjmp_chk.o debug/____longjmp_chk.o debug/fdelt_chk.o debug/poll_chk.o debug/ppoll_chk.o debug/explicit_bzero_chk.o debug/stack_chk_fail.o debug/fortify_fail.o\n' >$build_dir/debug/stamp.o
printf 'debug/backtrace.os debug/backtracesyms.os debug/backtracesymsfd.os debug/noophooks.os debug/memcpy_chk.os debug/memmove_chk.os debug/mempcpy_chk.os debug/memset_chk.os debug/stpcpy_chk.os debug/strcat_chk.os debug/strcpy_chk.os debug/strncat_chk.os debug/strncpy_chk.os debug/stpncpy_chk.os debug/sprintf_chk.os debug/vsprintf_chk.os debug/snprintf_chk.os debug/vsnprintf_chk.os debug/printf_chk.os debug/fprintf_chk.os debug/vprintf_chk.os debug/vfprintf_chk.os debug/gets_chk.os debug/chk_fail.os debug/readonly-area.os debug/fgets_chk.os debug/fgets_u_chk.os debug/read_chk.os debug/pread_chk.os debug/pread64_chk.os debug/recv_chk.os debug/recvfrom_chk.os debug/readlink_chk.os debug/readlinkat_chk.os debug/getwd_chk.os debug/getcwd_chk.os debug/realpath_chk.os debug/fread_chk.os debug/fread_u_chk.os debug/wctomb_chk.os debug/wcscpy_chk.os debug/wmemcpy_chk.os debug/wmemmove_chk.os debug/wmempcpy_chk.os debug/wcpcpy_chk.os debug/wcsncpy_chk.os debug/wcscat_chk.os debug/wcsncat_chk.os debug/wmemset_chk.os debug/wcpncpy_chk.os debug/swprintf_chk.os debug/vswprintf_chk.os debug/wprintf_chk.os debug/fwprintf_chk.os debug/vwprintf_chk.os debug/vfwprintf_chk.os debug/fgetws_chk.os debug/fgetws_u_chk.os debug/confstr_chk.os debug/getgroups_chk.os debug/ttyname_r_chk.os debug/gethostname_chk.os debug/getdomainname_chk.os debug/wcrtomb_chk.os debug/mbsnrtowcs_chk.os debug/wcsnrtombs_chk.os debug/mbsrtowcs_chk.os debug/wcsrtombs_chk.os debug/mbstowcs_chk.os debug/wcstombs_chk.os debug/asprintf_chk.os debug/vasprintf_chk.os debug/dprintf_chk.os debug/vdprintf_chk.os debug/obprintf_chk.os debug/vobprintf_chk.os debug/longjmp_chk.os debug/____longjmp_chk.os debug/fdelt_chk.os debug/poll_chk.os debug/ppoll_chk.os debug/explicit_bzero_chk.os debug/stack_chk_fail.os debug/fortify_fail.os\n' >$build_dir/debug/stamp.os
printf 'debug/stack_chk_fail_local.oS' >$build_dir/debug/stamp.oS
