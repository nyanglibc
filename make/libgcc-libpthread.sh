printf "\
LIBGCC LIBPTHREAD***************************************************************\n"
# from libpthread build, crtbeginS.o and crtendS.o may be required
mkdir -p $build_dir/libgcc/libpthread

paths="\
$src_dir/libgcc/libpthread/crtbeginS \
$src_dir/libgcc/libpthread/crtendS \
"
for p in $paths
do
	printf "ASSEMBLING $p.s\n"
	obj=$(basename $p).o
	$as $p.s -o $build_dir/libgcc/libpthread/$obj
done
