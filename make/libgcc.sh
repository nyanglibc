printf "\
LIBGCC**************************************************************************\n"
mkdir -p $build_dir/libgcc
non_preprocessed_files="\
popcountdi2 \
"
for f in $non_preprocessed_files
do
	printf "PREPROCESSING $src_dir/libgcc/$f.S to $build_dir/libgcc/$f.s\n"
	$cpp $src_dir/libgcc/$f.S >$build_dir/libgcc/$f.shared.s
done

preprocessed_paths="\
$build_dir/libgcc/popcountdi2 \
$src_dir/libgcc/multf3 \
$src_dir/libgcc/subtf3 \
$src_dir/libgcc/addtf3 \
$src_dir/libgcc/unordtf2 \
$src_dir/libgcc/getf2 \
$src_dir/libgcc/eqtf2 \
$src_dir/libgcc/letf2 \
$src_dir/libgcc/floatsitf \
$src_dir/libgcc/divtf3 \
$src_dir/libgcc/sfp-exceptions \
"
objs=
for p in $preprocessed_paths
do
	printf "ASSEMBLING $p.shared.s\n"
	obj=$(basename $p).o
	objs_path="$objs_path $build_dir/libgcc/$obj"
	$as $p.shared.s -o $build_dir/libgcc/$obj
done
$ar crv $build_dir/libgcc/libgcc.a $objs_path
