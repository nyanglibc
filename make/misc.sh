printf "\
MISC****************************************************************************\n"
fns_pie_shared="\
brk \
sbrk \
sstk \
readv \
writev \
preadv \
preadv64 \
pwritev \
pwritev64 \
preadv2 \
preadv64v2 \
pwritev2 \
pwritev64v2 \
setreuid \
setregid \
seteuid \
setegid \
getpagesize \
getdtsz \
gethostname \
getdomain \
select \
pselect \
fsync \
fdatasync \
reboot \
gethostid \
sethostid \
revoke \
mktemp \
mkstemp \
mkstemp64 \
mkdtemp \
mkostemp \
mkostemp64 \
mkstemps \
mkstemps64 \
mkostemps \
mkostemps64 \
ualarm \
usleep \
gtty \
stty \
ptrace \
fstab \
mntent \
mntent_r \
utimes \
lutimes \
futimes \
futimesat \
truncate \
ftruncate \
truncate64 \
ftruncate64 \
chflags \
fchflags \
insremque \
getttyent \
getusershell \
getpass \
ttyslot \
syslog \
daemon \
mmap \
mmap64 \
msync \
efgcvt \
efgcvt_r \
qefgcvt \
qefgcvt_r \
hsearch \
hsearch_r \
tsearch \
lsearch \
err \
error \
ustat \
getsysstats \
dirname \
regexp \
getloadavg \
getclktck \
getauxval \
ifunc-impl-list \
makedev \
allocate_once \
fd_to_filename \
single_threaded \
init-misc \
adjtimex \
umount \
umount2 \
readahead \
sysctl \
epoll_pwait \
signalfd \
eventfd_read \
eventfd_write \
epoll_wait \
tee \
vmsplice \
splice \
open_by_handle_at \
mlock2 \
pkey_mprotect \
pkey_set \
pkey_get \
timerfd_gettime \
timerfd_settime \
prctl \
process_vm_readv \
process_vm_writev \
clock_adjtime \
time64-support \
pselect32 \
xstat \
fxstat \
lxstat \
xstat64 \
fxstat64 \
lxstat64 \
fxstatat \
fxstatat64 \
xmknod \
xmknodat \
stub-syscalls \
\
ioctl \
sethostname \
setdomain \
acct \
chroot \
sync \
syncfs \
vhangup \
swapon \
swapoff \
syscall \
munmap \
mprotect \
madvise \
mincore \
remap_file_pages \
mlock \
munlock \
mlockall \
munlockall \
fgetxattr \
flistxattr \
fremovexattr \
fsetxattr \
getxattr \
listxattr \
lgetxattr \
llistxattr \
lremovexattr \
lsetxattr \
removexattr \
setxattr \
ioperm \
iopl \
clone \
setfsuid \
setfsgid \
eventfd \
prlimit \
personality \
arch_prctl \
modify_ldt \
syscall_clock_gettime \
fanotify_mark \
capget \
capset \
delete_module \
epoll_create \
epoll_create1 \
epoll_ctl \
init_module \
inotify_add_watch \
inotify_init \
inotify_init1 \
inotify_rm_watch \
klogctl \
mount \
mremap \
pivot_root \
quotactl \
sysinfo \
unshare \
timerfd_create \
fanotify_init \
name_to_handle_at \
setns \
memfd_create \
pkey_alloc \
pkey_free \
gettid \
tgkill \
"
fns_shared="\
create_module \
get_kernel_syms \
nfsservctl \
query_module \
uselib \
"
mkdir -p $build_dir/misc
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/misc/$fn.s -o $build_dir/misc/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/misc/$fn.shared.s -o $build_dir/misc/$fn.os
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/misc/$fn.shared.s -o $build_dir/misc/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'misc/brk.o misc/sbrk.o misc/sstk.o misc/ioctl.o misc/readv.o misc/writev.o misc/preadv.o misc/preadv64.o misc/pwritev.o misc/pwritev64.o misc/preadv2.o misc/preadv64v2.o misc/pwritev2.o misc/pwritev64v2.o misc/setreuid.o misc/setregid.o misc/seteuid.o misc/setegid.o misc/getpagesize.o misc/getdtsz.o misc/gethostname.o misc/sethostname.o misc/getdomain.o misc/setdomain.o misc/select.o misc/pselect.o misc/acct.o misc/chroot.o misc/fsync.o misc/sync.o misc/fdatasync.o misc/syncfs.o misc/reboot.o misc/gethostid.o misc/sethostid.o misc/revoke.o misc/vhangup.o misc/swapon.o misc/swapoff.o misc/mktemp.o misc/mkstemp.o misc/mkstemp64.o misc/mkdtemp.o misc/mkostemp.o misc/mkostemp64.o misc/mkstemps.o misc/mkstemps64.o misc/mkostemps.o misc/mkostemps64.o misc/ualarm.o misc/usleep.o misc/gtty.o misc/stty.o misc/ptrace.o misc/fstab.o misc/mntent.o misc/mntent_r.o misc/utimes.o misc/lutimes.o misc/futimes.o misc/futimesat.o misc/truncate.o misc/ftruncate.o misc/truncate64.o misc/ftruncate64.o misc/chflags.o misc/fchflags.o misc/insremque.o misc/getttyent.o misc/getusershell.o misc/getpass.o misc/ttyslot.o misc/syslog.o misc/syscall.o misc/daemon.o misc/mmap.o misc/mmap64.o misc/munmap.o misc/mprotect.o misc/msync.o misc/madvise.o misc/mincore.o misc/remap_file_pages.o misc/mlock.o misc/munlock.o misc/mlockall.o misc/munlockall.o misc/efgcvt.o misc/efgcvt_r.o misc/qefgcvt.o misc/qefgcvt_r.o misc/hsearch.o misc/hsearch_r.o misc/tsearch.o misc/lsearch.o misc/err.o misc/error.o misc/ustat.o misc/getsysstats.o misc/dirname.o misc/regexp.o misc/getloadavg.o misc/getclktck.o misc/fgetxattr.o misc/flistxattr.o misc/fremovexattr.o misc/fsetxattr.o misc/getxattr.o misc/listxattr.o misc/lgetxattr.o misc/llistxattr.o misc/lremovexattr.o misc/lsetxattr.o misc/removexattr.o misc/setxattr.o misc/getauxval.o misc/ifunc-impl-list.o misc/makedev.o misc/allocate_once.o misc/fd_to_filename.o misc/single_threaded.o misc/init-misc.o misc/ioperm.o misc/iopl.o misc/adjtimex.o misc/clone.o misc/umount.o misc/umount2.o misc/readahead.o misc/sysctl.o misc/setfsuid.o misc/setfsgid.o misc/epoll_pwait.o misc/signalfd.o misc/eventfd.o misc/eventfd_read.o misc/eventfd_write.o misc/prlimit.o misc/personality.o misc/epoll_wait.o misc/tee.o misc/vmsplice.o misc/splice.o misc/open_by_handle_at.o misc/mlock2.o misc/pkey_mprotect.o misc/pkey_set.o misc/pkey_get.o misc/timerfd_gettime.o misc/timerfd_settime.o misc/prctl.o misc/process_vm_readv.o misc/process_vm_writev.o misc/clock_adjtime.o misc/time64-support.o misc/pselect32.o misc/xstat.o misc/fxstat.o misc/lxstat.o misc/xstat64.o misc/fxstat64.o misc/lxstat64.o misc/fxstatat.o misc/fxstatat64.o misc/xmknod.o misc/xmknodat.o misc/arch_prctl.o misc/modify_ldt.o misc/syscall_clock_gettime.o misc/fanotify_mark.o misc/capget.o misc/capset.o misc/delete_module.o misc/epoll_create.o misc/epoll_create1.o misc/epoll_ctl.o misc/init_module.o misc/inotify_add_watch.o misc/inotify_init.o misc/inotify_init1.o misc/inotify_rm_watch.o misc/klogctl.o misc/mount.o misc/mremap.o misc/pivot_root.o misc/quotactl.o misc/sysinfo.o misc/unshare.o misc/timerfd_create.o misc/fanotify_init.o misc/name_to_handle_at.o misc/setns.o misc/memfd_create.o misc/pkey_alloc.o misc/pkey_free.o misc/gettid.o misc/tgkill.o misc/stub-syscalls.o\n' >$build_dir/misc/stamp.o
printf 'misc/brk.os misc/sbrk.os misc/sstk.os misc/ioctl.os misc/readv.os misc/writev.os misc/preadv.os misc/preadv64.os misc/pwritev.os misc/pwritev64.os misc/preadv2.os misc/preadv64v2.os misc/pwritev2.os misc/pwritev64v2.os misc/setreuid.os misc/setregid.os misc/seteuid.os misc/setegid.os misc/getpagesize.os misc/getdtsz.os misc/gethostname.os misc/sethostname.os misc/getdomain.os misc/setdomain.os misc/select.os misc/pselect.os misc/acct.os misc/chroot.os misc/fsync.os misc/sync.os misc/fdatasync.os misc/syncfs.os misc/reboot.os misc/gethostid.os misc/sethostid.os misc/revoke.os misc/vhangup.os misc/swapon.os misc/swapoff.os misc/mktemp.os misc/mkstemp.os misc/mkstemp64.os misc/mkdtemp.os misc/mkostemp.os misc/mkostemp64.os misc/mkstemps.os misc/mkstemps64.os misc/mkostemps.os misc/mkostemps64.os misc/ualarm.os misc/usleep.os misc/gtty.os misc/stty.os misc/ptrace.os misc/fstab.os misc/mntent.os misc/mntent_r.os misc/utimes.os misc/lutimes.os misc/futimes.os misc/futimesat.os misc/truncate.os misc/ftruncate.os misc/truncate64.os misc/ftruncate64.os misc/chflags.os misc/fchflags.os misc/insremque.os misc/getttyent.os misc/getusershell.os misc/getpass.os misc/ttyslot.os misc/syslog.os misc/syscall.os misc/daemon.os misc/mmap.os misc/mmap64.os misc/munmap.os misc/mprotect.os misc/msync.os misc/madvise.os misc/mincore.os misc/remap_file_pages.os misc/mlock.os misc/munlock.os misc/mlockall.os misc/munlockall.os misc/efgcvt.os misc/efgcvt_r.os misc/qefgcvt.os misc/qefgcvt_r.os misc/hsearch.os misc/hsearch_r.os misc/tsearch.os misc/lsearch.os misc/err.os misc/error.os misc/ustat.os misc/getsysstats.os misc/dirname.os misc/regexp.os misc/getloadavg.os misc/getclktck.os misc/fgetxattr.os misc/flistxattr.os misc/fremovexattr.os misc/fsetxattr.os misc/getxattr.os misc/listxattr.os misc/lgetxattr.os misc/llistxattr.os misc/lremovexattr.os misc/lsetxattr.os misc/removexattr.os misc/setxattr.os misc/getauxval.os misc/ifunc-impl-list.os misc/makedev.os misc/allocate_once.os misc/fd_to_filename.os misc/single_threaded.os misc/init-misc.os misc/ioperm.os misc/iopl.os misc/adjtimex.os misc/clone.os misc/umount.os misc/umount2.os misc/readahead.os misc/sysctl.os misc/setfsuid.os misc/setfsgid.os misc/epoll_pwait.os misc/signalfd.os misc/eventfd.os misc/eventfd_read.os misc/eventfd_write.os misc/prlimit.os misc/personality.os misc/epoll_wait.os misc/tee.os misc/vmsplice.os misc/splice.os misc/open_by_handle_at.os misc/mlock2.os misc/pkey_mprotect.os misc/pkey_set.os misc/pkey_get.os misc/timerfd_gettime.os misc/timerfd_settime.os misc/prctl.os misc/process_vm_readv.os misc/process_vm_writev.os misc/clock_adjtime.os misc/time64-support.os misc/pselect32.os misc/xstat.os misc/fxstat.os misc/lxstat.os misc/xstat64.os misc/fxstat64.os misc/lxstat64.os misc/fxstatat.os misc/fxstatat64.os misc/xmknod.os misc/xmknodat.os misc/arch_prctl.os misc/modify_ldt.os misc/syscall_clock_gettime.os misc/fanotify_mark.os misc/capget.os misc/capset.os misc/create_module.os misc/delete_module.os misc/epoll_create.os misc/epoll_create1.os misc/epoll_ctl.os misc/get_kernel_syms.os misc/init_module.os misc/inotify_add_watch.os misc/inotify_init.os misc/inotify_init1.os misc/inotify_rm_watch.os misc/klogctl.os misc/mount.os misc/mremap.os misc/nfsservctl.os misc/pivot_root.os misc/query_module.os misc/quotactl.os misc/sysinfo.os misc/unshare.os misc/uselib.os misc/timerfd_create.os misc/fanotify_init.os misc/name_to_handle_at.os misc/setns.os misc/memfd_create.os misc/pkey_alloc.os misc/pkey_free.os misc/gettid.os misc/tgkill.os misc/stub-syscalls.os\n' >$build_dir/misc/stamp.os
printf '' >$build_dir/misc/stamp.oS
