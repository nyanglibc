printf "\
NPTL****************************************************************************\n"
#LIBC -- START
fns_pie_shared_libc="\
alloca_cutoff \
libc-cancellation \
libc-cleanup \
libc-lowlevellock \
libc_multiple_threads \
libc_pthread_init \
old_pthread_cond_destroy \
old_pthread_cond_init \
pthread_attr_copy \
pthread_attr_destroy \
pthread_attr_extension \
pthread_attr_getdetachstate \
pthread_attr_getinheritsched \
pthread_attr_getschedparam \
pthread_attr_getschedpolicy \
pthread_attr_getscope \
pthread_attr_getsigmask \
pthread_attr_init \
pthread_attr_setaffinity \
pthread_attr_setdetachstate \
pthread_attr_setinheritsched \
pthread_attr_setschedparam \
pthread_attr_setschedpolicy \
pthread_attr_setscope \
pthread_attr_setsigmask \
pthread_attr_setsigmask_internal \
pthread_cond_destroy \
pthread_cond_init \
pthread_condattr_destroy \
pthread_condattr_init \
pthread_equal \
pthread_getaffinity \
pthread_getattr_np \
pthread_getschedparam \
pthread_self \
pthread_setschedparam \
pthread_sigmask \
register-atfork \
thrd_current \
thrd_equal \
thrd_sleep \
thrd_yield \
"
#XXX: crti.o is a softlink on pt-crti.o
fns_pie_libc="\
$fns_pie_shared_libc \
pthread_atfork \
\
pt-crti \
crtn \
"
fns_shared_libc="\
$fns_pie_shared_libc \
forward \
"
fns_nonshared_libc="\
pthread_atfork \
"
#LIBC -- END
#PTHREAD -- START
gens="\
pt-interp.shared.s.in\
"
mkdir -p $build_dir/nptl
for g in $gens
do
	sed -E -e "s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/nptl/$g >$build_dir/nptl/$(basename $g .in)
done
ln -sTf pt-interp.shared.s $build_dir/nptl/pt-interp.shared.v.s
paths_pie_shared_libpthread="\
$src_dir/nptl/nptl-init \
$src_dir/nptl/nptlfreeres \
$src_dir/nptl/vars \
$src_dir/nptl/events \
$src_dir/nptl/pthread_create \
$src_dir/nptl/pthread_exit \
$src_dir/nptl/pthread_detach \
$src_dir/nptl/pthread_join \
$src_dir/nptl/pthread_tryjoin \
$src_dir/nptl/pthread_timedjoin \
$src_dir/nptl/pthread_clockjoin \
$src_dir/nptl/pthread_join_common \
$src_dir/nptl/pthread_yield \
$src_dir/nptl/pthread_getconcurrency \
$src_dir/nptl/pthread_setconcurrency \
$src_dir/nptl/pthread_setschedprio \
$src_dir/nptl/pthread_attr_getguardsize \
$src_dir/nptl/pthread_attr_setguardsize \
$src_dir/nptl/pthread_attr_getstackaddr \
$src_dir/nptl/pthread_attr_setstackaddr \
$src_dir/nptl/pthread_attr_getstacksize \
$src_dir/nptl/pthread_attr_setstacksize \
$src_dir/nptl/pthread_attr_getstack \
$src_dir/nptl/pthread_attr_setstack \
$src_dir/nptl/pthread_mutex_init \
$src_dir/nptl/pthread_mutex_destroy \
$src_dir/nptl/pthread_mutex_lock \
$src_dir/nptl/pthread_mutex_trylock \
$src_dir/nptl/pthread_mutex_timedlock \
$src_dir/nptl/pthread_mutex_unlock \
$src_dir/nptl/pthread_mutex_cond_lock \
$src_dir/nptl/pthread_mutexattr_init \
$src_dir/nptl/pthread_mutexattr_destroy \
$src_dir/nptl/pthread_mutexattr_getpshared \
$src_dir/nptl/pthread_mutexattr_setpshared \
$src_dir/nptl/pthread_mutexattr_gettype \
$src_dir/nptl/pthread_mutexattr_settype \
$src_dir/nptl/pthread_rwlock_init \
$src_dir/nptl/pthread_rwlock_destroy \
$src_dir/nptl/pthread_rwlock_rdlock \
$src_dir/nptl/pthread_rwlock_timedrdlock \
$src_dir/nptl/pthread_rwlock_clockrdlock \
$src_dir/nptl/pthread_rwlock_wrlock \
$src_dir/nptl/pthread_rwlock_timedwrlock \
$src_dir/nptl/pthread_rwlock_clockwrlock \
$src_dir/nptl/pthread_rwlock_tryrdlock \
$src_dir/nptl/pthread_rwlock_trywrlock \
$src_dir/nptl/pthread_rwlock_unlock \
$src_dir/nptl/pthread_rwlockattr_init \
$src_dir/nptl/pthread_rwlockattr_destroy \
$src_dir/nptl/pthread_rwlockattr_getpshared \
$src_dir/nptl/pthread_rwlockattr_setpshared \
$src_dir/nptl/pthread_rwlockattr_getkind_np \
$src_dir/nptl/pthread_rwlockattr_setkind_np \
$src_dir/nptl/pthread_cond_wait \
$src_dir/nptl/pthread_cond_signal \
$src_dir/nptl/pthread_cond_broadcast \
$src_dir/nptl/old_pthread_cond_wait \
$src_dir/nptl/old_pthread_cond_timedwait \
$src_dir/nptl/old_pthread_cond_signal \
$src_dir/nptl/old_pthread_cond_broadcast \
$src_dir/nptl/pthread_condattr_getpshared \
$src_dir/nptl/pthread_condattr_setpshared \
$src_dir/nptl/pthread_condattr_getclock \
$src_dir/nptl/pthread_condattr_setclock \
$src_dir/nptl/pthread_spin_init \
$src_dir/nptl/pthread_spin_destroy \
$src_dir/nptl/pthread_barrier_init \
$src_dir/nptl/pthread_barrier_destroy \
$src_dir/nptl/pthread_barrier_wait \
$src_dir/nptl/pthread_barrierattr_init \
$src_dir/nptl/pthread_barrierattr_destroy \
$src_dir/nptl/pthread_barrierattr_getpshared \
$src_dir/nptl/pthread_barrierattr_setpshared \
$src_dir/nptl/pthread_key_create \
$src_dir/nptl/pthread_key_delete \
$src_dir/nptl/pthread_getspecific \
$src_dir/nptl/pthread_setspecific \
$src_dir/nptl/pthread_kill \
$src_dir/nptl/pthread_sigqueue \
$src_dir/nptl/pthread_cancel \
$src_dir/nptl/pthread_testcancel \
$src_dir/nptl/pthread_setcancelstate \
$src_dir/nptl/pthread_setcanceltype \
$src_dir/nptl/pthread_once \
$src_dir/nptl/old_pthread_atfork \
$src_dir/nptl/pthread_getcpuclockid \
$src_dir/nptl/shm-directory \
$src_dir/nptl/sem_init \
$src_dir/nptl/sem_destroy \
$src_dir/nptl/sem_open \
$src_dir/nptl/sem_close \
$src_dir/nptl/sem_unlink \
$src_dir/nptl/sem_getvalue \
$src_dir/nptl/sem_wait \
$src_dir/nptl/sem_timedwait \
$src_dir/nptl/sem_clockwait \
$src_dir/nptl/sem_post \
$src_dir/nptl/cleanup \
$src_dir/nptl/cleanup_defer \
$src_dir/nptl/cleanup_compat \
$src_dir/nptl/cleanup_defer_compat \
$src_dir/nptl/unwind \
$src_dir/nptl/pt-longjmp \
$src_dir/nptl/pt-cleanup \
$src_dir/nptl/cancellation \
$src_dir/nptl/lowlevellock \
$src_dir/nptl/pt-fork \
$src_dir/nptl/pt-fcntl \
$src_dir/nptl/write \
$src_dir/nptl/read \
$src_dir/nptl/close \
$src_dir/nptl/accept \
$src_dir/nptl/connect \
$src_dir/nptl/recv \
$src_dir/nptl/recvfrom \
$src_dir/nptl/send \
$src_dir/nptl/sendto \
$src_dir/nptl/fsync \
$src_dir/nptl/lseek \
$src_dir/nptl/lseek64 \
$src_dir/nptl/msync \
$src_dir/nptl/open \
$src_dir/nptl/open64 \
$src_dir/nptl/pause \
$src_dir/nptl/pread \
$src_dir/nptl/pread64 \
$src_dir/nptl/pwrite \
$src_dir/nptl/pwrite64 \
$src_dir/nptl/tcdrain \
$src_dir/nptl/msgrcv \
$src_dir/nptl/msgsnd \
$src_dir/nptl/sigwait \
$src_dir/nptl/sigsuspend \
$src_dir/nptl/recvmsg \
$src_dir/nptl/sendmsg \
$src_dir/nptl/pt-raise \
$src_dir/nptl/pt-system \
$src_dir/nptl/flockfile \
$src_dir/nptl/ftrylockfile \
$src_dir/nptl/funlockfile \
$src_dir/nptl/sigaction \
$src_dir/nptl/herrno \
$src_dir/nptl/res \
$src_dir/nptl/pthread_kill_other_threads \
$src_dir/nptl/pthread_setaffinity \
$src_dir/nptl/pthread_attr_getaffinity \
$src_dir/nptl/pthread_mutexattr_getrobust \
$src_dir/nptl/pthread_mutexattr_setrobust \
$src_dir/nptl/pthread_mutex_consistent \
$src_dir/nptl/cleanup_routine \
$src_dir/nptl/pthread_mutexattr_getprotocol \
$src_dir/nptl/pthread_mutexattr_setprotocol \
$src_dir/nptl/pthread_mutexattr_getprioceiling \
$src_dir/nptl/pthread_mutexattr_setprioceiling \
$src_dir/nptl/tpp \
$src_dir/nptl/pthread_mutex_getprioceiling \
$src_dir/nptl/pthread_mutex_setprioceiling \
$src_dir/nptl/pthread_setname \
$src_dir/nptl/pthread_getname \
$src_dir/nptl/pthread_setattr_default_np \
$src_dir/nptl/pthread_getattr_default_np \
$src_dir/nptl/pthread_mutex_conf \
$src_dir/nptl/libpthread-compat \
$src_dir/nptl/thrd_create \
$src_dir/nptl/thrd_detach \
$src_dir/nptl/thrd_exit \
$src_dir/nptl/thrd_join \
$src_dir/nptl/call_once \
$src_dir/nptl/mtx_destroy \
$src_dir/nptl/mtx_init \
$src_dir/nptl/mtx_lock \
$src_dir/nptl/mtx_timedlock \
$src_dir/nptl/mtx_trylock \
$src_dir/nptl/mtx_unlock \
$src_dir/nptl/cnd_broadcast \
$src_dir/nptl/cnd_destroy \
$src_dir/nptl/cnd_init \
$src_dir/nptl/cnd_signal \
$src_dir/nptl/cnd_timedwait \
$src_dir/nptl/cnd_wait \
$src_dir/nptl/tss_create \
$src_dir/nptl/tss_delete \
$src_dir/nptl/tss_get \
$src_dir/nptl/tss_set \
$src_dir/nptl/elision-lock \
$src_dir/nptl/elision-unlock \
$src_dir/nptl/elision-timed \
$src_dir/nptl/elision-trylock \
$src_dir/nptl/errno-loc \
$src_dir/nptl/futex-internal \
\
$src_dir/nptl/pthread_spin_lock \
$src_dir/nptl/pthread_spin_trylock \
$src_dir/nptl/pthread_spin_unlock \
"
paths_pie_libpthread="\
$paths_pie_shared_libpthread \
"
paths_shared_libpthread="\
$paths_pie_shared_libpthread \
$src_dir/nptl/version \
$build_dir/nptl/pt-interp \
$src_dir/nptl/pt-allocrtsig \
$src_dir/nptl/unwind-forcedunwind \
"
#PTHREAD -- END
################################################################################
mkdir -p $build_dir/nptl
#LIBC -- START
for fn in $fns_pie_libc
do
	printf "ASSEMBLING LIBC PIE $fn\n"
	$as $src_dir/nptl/$fn.s -o $build_dir/nptl/$fn.o
done
for fn in $fns_shared_libc
do
	printf "ASSEMBLING LIBC SHARED $fn\n"
	$as $src_dir/nptl/$fn.shared.s -o $build_dir/nptl/$fn.os
done
for fn in $fns_nonshared_libc
do
	printf "ASSEMBLING LIBC NONSHARED $fn\n"
	$as $src_dir/nptl/$fn.nonshared.s -o $build_dir/nptl/$fn.oS
done
printf 'CREATING LIBC STAMPS FILES\n'
printf 'nptl/alloca_cutoff.o nptl/libc-cancellation.o nptl/libc-cleanup.o nptl/libc-lowlevellock.o nptl/libc_multiple_threads.o nptl/libc_pthread_init.o nptl/old_pthread_cond_destroy.o nptl/old_pthread_cond_init.o nptl/pthread_atfork.o nptl/pthread_attr_copy.o nptl/pthread_attr_destroy.o nptl/pthread_attr_extension.o nptl/pthread_attr_getdetachstate.o nptl/pthread_attr_getinheritsched.o nptl/pthread_attr_getschedparam.o nptl/pthread_attr_getschedpolicy.o nptl/pthread_attr_getscope.o nptl/pthread_attr_getsigmask.o nptl/pthread_attr_init.o nptl/pthread_attr_setaffinity.o nptl/pthread_attr_setdetachstate.o nptl/pthread_attr_setinheritsched.o nptl/pthread_attr_setschedparam.o nptl/pthread_attr_setschedpolicy.o nptl/pthread_attr_setscope.o nptl/pthread_attr_setsigmask.o nptl/pthread_attr_setsigmask_internal.o nptl/pthread_cond_destroy.o nptl/pthread_cond_init.o nptl/pthread_condattr_destroy.o nptl/pthread_condattr_init.o nptl/pthread_equal.o nptl/pthread_getaffinity.o nptl/pthread_getattr_np.o nptl/pthread_getschedparam.o nptl/pthread_self.o nptl/pthread_setschedparam.o nptl/pthread_sigmask.o nptl/register-atfork.o nptl/thrd_current.o nptl/thrd_equal.o nptl/thrd_sleep.o nptl/thrd_yield.o\n' >$build_dir/nptl/stamp.o
printf 'nptl/alloca_cutoff.os nptl/forward.os nptl/libc-cancellation.os nptl/libc-cleanup.os nptl/libc-lowlevellock.os nptl/libc_multiple_threads.os nptl/libc_pthread_init.os nptl/old_pthread_cond_destroy.os nptl/old_pthread_cond_init.os nptl/pthread_attr_copy.os nptl/pthread_attr_destroy.os nptl/pthread_attr_extension.os nptl/pthread_attr_getdetachstate.os nptl/pthread_attr_getinheritsched.os nptl/pthread_attr_getschedparam.os nptl/pthread_attr_getschedpolicy.os nptl/pthread_attr_getscope.os nptl/pthread_attr_getsigmask.os nptl/pthread_attr_init.os nptl/pthread_attr_setaffinity.os nptl/pthread_attr_setdetachstate.os nptl/pthread_attr_setinheritsched.os nptl/pthread_attr_setschedparam.os nptl/pthread_attr_setschedpolicy.os nptl/pthread_attr_setscope.os nptl/pthread_attr_setsigmask.os nptl/pthread_attr_setsigmask_internal.os nptl/pthread_cond_destroy.os nptl/pthread_cond_init.os nptl/pthread_condattr_destroy.os nptl/pthread_condattr_init.os nptl/pthread_equal.os nptl/pthread_getaffinity.os nptl/pthread_getattr_np.os nptl/pthread_getschedparam.os nptl/pthread_self.os nptl/pthread_setschedparam.os nptl/pthread_sigmask.os nptl/register-atfork.os nptl/thrd_current.os nptl/thrd_equal.os nptl/thrd_sleep.os nptl/thrd_yield.os\n' >$build_dir/nptl/stamp.os
printf 'nptl/pthread_atfork.oS\n' >$build_dir/nptl/stamp.oS
#LIBC -- END
#LIBPTHREAD -- START
for p in $paths_pie_libpthread
do
	printf "ASSEMBLING LIBPTHREAD PIE $p\n"
	$as $p.s -o $build_dir/nptl/$(basename $p).o
done
for p in $paths_shared_libpthread
do
	printf "ASSEMBLING LIBPTHREAD SHARED $p\n"
	$as $p.shared.s -o $build_dir/nptl/$(basename $p).os
done
#LIBPTHREAD -- END
