printf "\
WCTYPE**************************************************************************\n"
fns_pie_shared="\
wcfuncs \
wctype \
iswctype \
wctrans \
towctrans \
wcfuncs_l \
wctype_l \
iswctype_l \
wctrans_l \
towctrans_l \
"
mkdir -p $build_dir/wctype
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/wctype/$fn.s -o $build_dir/wctype/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/wctype/$fn.shared.s -o $build_dir/wctype/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'wctype/wcfuncs.o wctype/wctype.o wctype/iswctype.o wctype/wctrans.o wctype/towctrans.o wctype/wcfuncs_l.o wctype/wctype_l.o wctype/iswctype_l.o wctype/wctrans_l.o wctype/towctrans_l.o\n' >$build_dir/wctype/stamp.o
printf 'wctype/wcfuncs.os wctype/wctype.os wctype/iswctype.os wctype/wctrans.os wctype/towctrans.os wctype/wcfuncs_l.os wctype/wctype_l.os wctype/iswctype_l.os wctype/wctrans_l.os wctype/towctrans_l.os\n' >$build_dir/wctype/stamp.os
printf '' >$build_dir/wctype/stamp.oS
