printf "\
NPTL_DB*************************************************************************\n"
mkdir -p $build_dir/nptl_db
fns_shared="\
td_init \
td_log \
td_ta_new \
td_ta_delete \
td_ta_get_nthreads \
td_ta_get_ph \
td_ta_map_id2thr \
td_ta_map_lwp2thr \
td_ta_thr_iter \
td_ta_tsd_iter \
td_thr_get_info \
td_thr_getfpregs \
td_thr_getgregs \
td_thr_getxregs \
td_thr_getxregsize \
td_thr_setfpregs \
td_thr_setgregs \
td_thr_setprio \
td_thr_setsigpending \
td_thr_setxregs \
td_thr_sigsetmask \
td_thr_tsd \
td_thr_validate \
td_thr_dbsuspend \
td_thr_dbresume \
td_ta_setconcurrency \
td_ta_enable_stats \
td_ta_reset_stats \
td_ta_get_stats \
td_ta_event_addr \
td_thr_event_enable \
td_thr_set_event \
td_thr_clear_event \
td_thr_event_getmsg \
td_ta_set_event \
td_ta_event_getmsg \
td_ta_clear_event \
td_symbol_list \
td_thr_tlsbase \
td_thr_tls_get_addr \
fetch-value \
"
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/nptl_db/$fn.shared.s -o $build_dir/nptl_db/$fn.os
done
cd $build_dir/nptl_db
printf 'CREATING LIBTHREAD_DB_PIC.A\n'
$ar cruv libthread_db_pic.a td_init.os td_log.os td_ta_new.os td_ta_delete.os td_ta_get_nthreads.os td_ta_get_ph.os td_ta_map_id2thr.os td_ta_map_lwp2thr.os td_ta_thr_iter.os td_ta_tsd_iter.os td_thr_get_info.os td_thr_getfpregs.os td_thr_getgregs.os td_thr_getxregs.os td_thr_getxregsize.os td_thr_setfpregs.os td_thr_setgregs.os td_thr_setprio.os td_thr_setsigpending.os td_thr_setxregs.os td_thr_sigsetmask.os td_thr_tsd.os td_thr_validate.os td_thr_dbsuspend.os td_thr_dbresume.os td_ta_setconcurrency.os td_ta_enable_stats.os td_ta_reset_stats.os td_ta_get_stats.os td_ta_event_addr.os td_thr_event_enable.os td_thr_set_event.os td_thr_clear_event.os td_thr_event_getmsg.os td_ta_set_event.os td_ta_event_getmsg.os td_ta_clear_event.os td_symbol_list.os td_thr_tlsbase.os td_thr_tls_get_addr.os fetch-value.os
printf 'CREATING LIBTHREAD_DB.SO.1\n'
$ld \
	-s \
	-shared \
	-O1 \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libthread_db.versions.map \
	-soname=libthread_db.so.1 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/nptl_db/libthread_db.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/nptl_db/libthread_db_pic.a \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
