printf "\
POSIX***************************************************************************\n"
gens="\
sysconf.s.in \
sysconf.shared.s.in \
"
mkdir -p $build_dir/posix
for g in $gens
do
	sed -E -e "s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/posix/$g >$build_dir/posix/$(basename $g .in)
done
paths="\
$src_dir/posix/times \
$src_dir/posix/wait \
$src_dir/posix/waitpid \
$src_dir/posix/wait3 \
$src_dir/posix/wait4 \
$src_dir/posix/waitid \
$src_dir/posix/sleep \
$src_dir/posix/pause \
$src_dir/posix/nanosleep \
$src_dir/posix/fork \
$src_dir/posix/_exit \
$src_dir/posix/fexecve \
$src_dir/posix/execv \
$src_dir/posix/execle \
$src_dir/posix/execl \
$src_dir/posix/execvp \
$src_dir/posix/execlp \
$src_dir/posix/execvpe \
$src_dir/posix/setuid \
$src_dir/posix/setgid \
$src_dir/posix/group_member \
$src_dir/posix/bsd-getpgrp \
$src_dir/posix/setpgrp \
$src_dir/posix/setresuid \
$src_dir/posix/setresgid \
$src_dir/posix/pathconf \
$build_dir/posix/sysconf \
$src_dir/posix/fpathconf \
$src_dir/posix/glob \
$src_dir/posix/glob64 \
$src_dir/posix/globfree \
$src_dir/posix/globfree64 \
$src_dir/posix/glob_pattern_p \
$src_dir/posix/fnmatch \
$src_dir/posix/regex \
$src_dir/posix/glob-lstat-compat \
$src_dir/posix/glob64-lstat-compat \
$src_dir/posix/confstr \
$src_dir/posix/getopt \
$src_dir/posix/getopt1 \
$src_dir/posix/sched_rr_gi \
$src_dir/posix/sched_getaffinity \
$src_dir/posix/sched_setaffinity \
$src_dir/posix/getaddrinfo \
$src_dir/posix/gai_strerror \
$src_dir/posix/wordexp \
$src_dir/posix/pread \
$src_dir/posix/pwrite \
$src_dir/posix/pread64 \
$src_dir/posix/pwrite64 \
$src_dir/posix/spawn_faction_init \
$src_dir/posix/spawn_faction_destroy \
$src_dir/posix/spawn_faction_addclose \
$src_dir/posix/spawn_faction_addopen \
$src_dir/posix/spawn_faction_adddup2 \
$src_dir/posix/spawn_valid_fd \
$src_dir/posix/spawn_faction_addchdir \
$src_dir/posix/spawn_faction_addfchdir \
$src_dir/posix/spawnattr_init \
$src_dir/posix/spawnattr_destroy \
$src_dir/posix/spawnattr_getdefault \
$src_dir/posix/spawnattr_setdefault \
$src_dir/posix/spawnattr_getflags \
$src_dir/posix/spawnattr_setflags \
$src_dir/posix/spawnattr_getpgroup \
$src_dir/posix/spawnattr_setpgroup \
$src_dir/posix/spawn \
$src_dir/posix/spawnp \
$src_dir/posix/spawni \
$src_dir/posix/spawnattr_getsigmask \
$src_dir/posix/spawnattr_getschedpolicy \
$src_dir/posix/spawnattr_getschedparam \
$src_dir/posix/spawnattr_setsigmask \
$src_dir/posix/spawnattr_setschedpolicy \
$src_dir/posix/spawnattr_setschedparam \
$src_dir/posix/posix_madvise \
$src_dir/posix/get_child_max \
$src_dir/posix/sched_cpucount \
$src_dir/posix/sched_cpualloc \
$src_dir/posix/sched_cpufree \
$src_dir/posix/streams-compat \
$src_dir/posix/init-posix \
$src_dir/posix/environ \
$src_dir/posix/sched_getcpu \
$src_dir/posix/oldglob \
$src_dir/posix/getcpu \
\
$src_dir/posix/uname \
$src_dir/posix/vfork \
$src_dir/posix/alarm \
$src_dir/posix/execve \
$src_dir/posix/getpid \
$src_dir/posix/getppid \
$src_dir/posix/getuid \
$src_dir/posix/geteuid \
$src_dir/posix/getgid \
$src_dir/posix/getegid \
$src_dir/posix/getgroups \
$src_dir/posix/getpgid \
$src_dir/posix/setpgid \
$src_dir/posix/getpgrp \
$src_dir/posix/getsid \
$src_dir/posix/setsid \
$src_dir/posix/getresuid \
$src_dir/posix/getresgid \
$src_dir/posix/sched_setp \
$src_dir/posix/sched_getp \
$src_dir/posix/sched_sets \
$src_dir/posix/sched_gets \
$src_dir/posix/sched_yield \
$src_dir/posix/sched_primax \
$src_dir/posix/sched_primin \
"
mkdir -p $build_dir/posix
for p in $paths
do
	printf "ASSEMBLING PIE $fn\n"
	$as $p.s -o $build_dir/posix/$(basename $p).o
	printf "ASSEMBLING SHARED $fn\n"
	$as $p.shared.s -o $build_dir/posix/$(basename $p).os
done
printf 'CREATING STAMPS FILES\n'
printf 'posix/uname.o posix/times.o posix/wait.o posix/waitpid.o posix/wait3.o posix/wait4.o posix/waitid.o posix/alarm.o posix/sleep.o posix/pause.o posix/nanosleep.o posix/fork.o posix/vfork.o posix/_exit.o posix/execve.o posix/fexecve.o posix/execv.o posix/execle.o posix/execl.o posix/execvp.o posix/execlp.o posix/execvpe.o posix/getpid.o posix/getppid.o posix/getuid.o posix/geteuid.o posix/getgid.o posix/getegid.o posix/getgroups.o posix/setuid.o posix/setgid.o posix/group_member.o posix/getpgid.o posix/setpgid.o posix/getpgrp.o posix/bsd-getpgrp.o posix/setpgrp.o posix/getsid.o posix/setsid.o posix/getresuid.o posix/getresgid.o posix/setresuid.o posix/setresgid.o posix/pathconf.o posix/sysconf.o posix/fpathconf.o posix/glob.o posix/glob64.o posix/globfree.o posix/globfree64.o posix/glob_pattern_p.o posix/fnmatch.o posix/regex.o posix/glob-lstat-compat.o posix/glob64-lstat-compat.o posix/confstr.o posix/getopt.o posix/getopt1.o posix/sched_setp.o posix/sched_getp.o posix/sched_sets.o posix/sched_gets.o posix/sched_yield.o posix/sched_primax.o posix/sched_primin.o posix/sched_rr_gi.o posix/sched_getaffinity.o posix/sched_setaffinity.o posix/getaddrinfo.o posix/gai_strerror.o posix/wordexp.o posix/pread.o posix/pwrite.o posix/pread64.o posix/pwrite64.o posix/spawn_faction_init.o posix/spawn_faction_destroy.o posix/spawn_faction_addclose.o posix/spawn_faction_addopen.o posix/spawn_faction_adddup2.o posix/spawn_valid_fd.o posix/spawn_faction_addchdir.o posix/spawn_faction_addfchdir.o posix/spawnattr_init.o posix/spawnattr_destroy.o posix/spawnattr_getdefault.o posix/spawnattr_setdefault.o posix/spawnattr_getflags.o posix/spawnattr_setflags.o posix/spawnattr_getpgroup.o posix/spawnattr_setpgroup.o posix/spawn.o posix/spawnp.o posix/spawni.o posix/spawnattr_getsigmask.o posix/spawnattr_getschedpolicy.o posix/spawnattr_getschedparam.o posix/spawnattr_setsigmask.o posix/spawnattr_setschedpolicy.o posix/spawnattr_setschedparam.o posix/posix_madvise.o posix/get_child_max.o posix/sched_cpucount.o posix/sched_cpualloc.o posix/sched_cpufree.o posix/streams-compat.o posix/init-posix.o posix/environ.o posix/sched_getcpu.o posix/oldglob.o posix/getcpu.o\n' >$build_dir/posix/stamp.o
printf 'posix/uname.os posix/times.os posix/wait.os posix/waitpid.os posix/wait3.os posix/wait4.os posix/waitid.os posix/alarm.os posix/sleep.os posix/pause.os posix/nanosleep.os posix/fork.os posix/vfork.os posix/_exit.os posix/execve.os posix/fexecve.os posix/execv.os posix/execle.os posix/execl.os posix/execvp.os posix/execlp.os posix/execvpe.os posix/getpid.os posix/getppid.os posix/getuid.os posix/geteuid.os posix/getgid.os posix/getegid.os posix/getgroups.os posix/setuid.os posix/setgid.os posix/group_member.os posix/getpgid.os posix/setpgid.os posix/getpgrp.os posix/bsd-getpgrp.os posix/setpgrp.os posix/getsid.os posix/setsid.os posix/getresuid.os posix/getresgid.os posix/setresuid.os posix/setresgid.os posix/pathconf.os posix/sysconf.os posix/fpathconf.os posix/glob.os posix/glob64.os posix/globfree.os posix/globfree64.os posix/glob_pattern_p.os posix/fnmatch.os posix/regex.os posix/glob-lstat-compat.os posix/glob64-lstat-compat.os posix/confstr.os posix/getopt.os posix/getopt1.os posix/sched_setp.os posix/sched_getp.os posix/sched_sets.os posix/sched_gets.os posix/sched_yield.os posix/sched_primax.os posix/sched_primin.os posix/sched_rr_gi.os posix/sched_getaffinity.os posix/sched_setaffinity.os posix/getaddrinfo.os posix/gai_strerror.os posix/wordexp.os posix/pread.os posix/pwrite.os posix/pread64.os posix/pwrite64.os posix/spawn_faction_init.os posix/spawn_faction_destroy.os posix/spawn_faction_addclose.os posix/spawn_faction_addopen.os posix/spawn_faction_adddup2.os posix/spawn_valid_fd.os posix/spawn_faction_addchdir.os posix/spawn_faction_addfchdir.os posix/spawnattr_init.os posix/spawnattr_destroy.os posix/spawnattr_getdefault.os posix/spawnattr_setdefault.os posix/spawnattr_getflags.os posix/spawnattr_setflags.os posix/spawnattr_getpgroup.os posix/spawnattr_setpgroup.os posix/spawn.os posix/spawnp.os posix/spawni.os posix/spawnattr_getsigmask.os posix/spawnattr_getschedpolicy.os posix/spawnattr_getschedparam.os posix/spawnattr_setsigmask.os posix/spawnattr_setschedpolicy.os posix/spawnattr_setschedparam.os posix/posix_madvise.os posix/get_child_max.os posix/sched_cpucount.os posix/sched_cpualloc.os posix/sched_cpufree.os posix/streams-compat.os posix/init-posix.os posix/environ.os posix/sched_getcpu.os posix/oldglob.os posix/getcpu.os\n' >$build_dir/posix/stamp.os
printf '' >$build_dir/posix/stamp.oS
