printf "\
INTL****************************************************************************\n"
gens="\
localealias.s.in \
localealias.shared.s.in \
dcigettext.s.in \
dcigettext.shared.s.in \
"
mkdir -p $build_dir/intl
for g in $gens
do
	sed -E -e "s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/intl/$g >$build_dir/intl/$(basename $g .in)
done
paths="\
$src_dir/intl/bindtextdom \
$src_dir/intl/dcgettext \
$src_dir/intl/dgettext \
$src_dir/intl/gettext \
$build_dir/intl/dcigettext \
$src_dir/intl/dcngettext \
$src_dir/intl/dngettext \
$src_dir/intl/ngettext \
$src_dir/intl/finddomain \
$src_dir/intl/loadmsgcat \
$build_dir/intl/localealias \
$src_dir/intl/textdomain \
$src_dir/intl/l10nflist \
$src_dir/intl/explodename \
$src_dir/intl/plural \
$src_dir/intl/plural-exp \
$src_dir/intl/hash-string \
"
mkdir -p $build_dir/intl
for p in $paths
do
	printf "ASSEMBLING PIE $p\n"
	$as $p.s -o $build_dir/intl/$(basename $p).o
	printf "ASSEMBLING SHARED $p\n"
	$as $p.shared.s -o $build_dir/intl/$(basename $p).os
done
printf 'CREATING STAMPS FILES\n'
printf 'intl/bindtextdom.o intl/dcgettext.o intl/dgettext.o intl/gettext.o intl/dcigettext.o intl/dcngettext.o intl/dngettext.o intl/ngettext.o intl/finddomain.o intl/loadmsgcat.o intl/localealias.o intl/textdomain.o intl/l10nflist.o intl/explodename.o intl/plural.o intl/plural-exp.o intl/hash-string.o\n' >$build_dir/intl/stamp.o
printf 'intl/bindtextdom.os intl/dcgettext.os intl/dgettext.os intl/gettext.os intl/dcigettext.os intl/dcngettext.os intl/dngettext.os intl/ngettext.os intl/finddomain.os intl/loadmsgcat.os intl/localealias.os intl/textdomain.os intl/l10nflist.os intl/explodename.os intl/plural.os intl/plural-exp.os intl/hash-string.os\n' >$build_dir/intl/stamp.os
printf '' >$build_dir/intl/stamp.oS
