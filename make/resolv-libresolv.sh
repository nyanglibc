printf "\
RESOLV LIBRESOLV****************************************************************\n"
mkdir -p $build_dir/resolv/libresolv
fns_pie_shared="\
res_comp \
res_debug \
res_data \
res_mkquery \
res_query \
res_send \
inet_net_ntop \
inet_net_pton \
inet_neta \
base64 \
ns_parse \
ns_name \
ns_netint \
ns_ttl \
ns_print \
ns_samedomain \
ns_date \
res_enable_icmp \
compat-hooks \
compat-gethnamaddr \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/resolv/libresolv/$fn.s -o $build_dir/resolv/libresolv/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/resolv/libresolv/$fn.shared.s -o $build_dir/resolv/libresolv/$fn.os
done
printf 'CREATING LIBRESOLV.A\n'
cd $build_dir/resolv/libresolv
$ar cruv libresolv.a res_comp.o res_debug.o res_data.o res_mkquery.o res_query.o res_send.o inet_net_ntop.o inet_net_pton.o inet_neta.o base64.o ns_parse.o ns_name.o ns_netint.o ns_ttl.o ns_print.o ns_samedomain.o ns_date.o res_enable_icmp.o compat-hooks.o compat-gethnamaddr.o
printf 'CREATING LIBRESOLV_PIC.A\n'
$ar cruv libresolv_pic.a res_comp.os res_debug.os res_data.os res_mkquery.os res_query.os res_send.os inet_net_ntop.os inet_net_pton.os inet_neta.os base64.os ns_parse.os ns_name.os ns_netint.os ns_ttl.os ns_print.os ns_samedomain.os ns_date.os res_enable_icmp.os compat-hooks.os compat-gethnamaddr.os
printf 'CREATING LIBRESOLV.SO.1\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libresolv.versions.map \
	-soname=libresolv.so.2 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/resolv/libresolv/libresolv.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/resolv/libresolv/libresolv_pic.a \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
		$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
