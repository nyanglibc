printf "\
WCSMBS**************************************************************************\n"
fns_pie_shared="\
wcscat \
wcscpy \
wcscspn \
wcsdup \
wcsncat \
wcsncmp \
wcsncpy \
wcspbrk \
wcsspn \
wcstok \
wcsstr \
wmemchr \
wmemcmp \
wmemcpy \
wmemmove \
wcpcpy \
wcpncpy \
wmempcpy \
btowc \
wctob \
mbsinit \
mbrlen \
mbrtowc \
wcrtomb \
mbsrtowcs \
wcsrtombs \
mbsnrtowcs \
wcsnrtombs \
wcsnlen \
wcschrnul \
wcstol \
wcstoul \
wcstoll \
wcstoull \
wcstod \
wcstold \
wcstof \
wcstol_l \
wcstoul_l \
wcstoll_l \
wcstoull_l \
wcstod_l \
wcstold_l \
wcstof_l \
wcstod_nan \
wcstold_nan \
wcstof_nan \
wcscoll \
wcsxfrm \
wcwidth \
wcswidth \
wcscoll_l \
wcsxfrm_l \
wcscasecmp \
wcsncase \
wcscasecmp_l \
wcsncase_l \
wcsmbsload \
mbsrtowcs_l \
isoc99_wscanf \
isoc99_vwscanf \
isoc99_fwscanf \
isoc99_vfwscanf \
isoc99_swscanf \
isoc99_vswscanf \
mbrtoc16 \
c16rtomb \
mbrtoc32 \
c32rtomb \
wcstof128_l \
wcstof128 \
wcstof128_nan \
\
wcschr \
wcscmp \
wcslen \
wcsrchr \
wmemset \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/wcsmbs
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/wcsmbs/$fn.s -o $build_dir/wcsmbs/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/wcsmbs/$fn.shared.s -o $build_dir/wcsmbs/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'wcsmbs/wcscat.o wcsmbs/wcschr.o wcsmbs/wcscmp.o wcsmbs/wcscpy.o wcsmbs/wcscspn.o wcsmbs/wcsdup.o wcsmbs/wcslen.o wcsmbs/wcsncat.o wcsmbs/wcsncmp.o wcsmbs/wcsncpy.o wcsmbs/wcspbrk.o wcsmbs/wcsrchr.o wcsmbs/wcsspn.o wcsmbs/wcstok.o wcsmbs/wcsstr.o wcsmbs/wmemchr.o wcsmbs/wmemcmp.o wcsmbs/wmemcpy.o wcsmbs/wmemmove.o wcsmbs/wmemset.o wcsmbs/wcpcpy.o wcsmbs/wcpncpy.o wcsmbs/wmempcpy.o wcsmbs/btowc.o wcsmbs/wctob.o wcsmbs/mbsinit.o wcsmbs/mbrlen.o wcsmbs/mbrtowc.o wcsmbs/wcrtomb.o wcsmbs/mbsrtowcs.o wcsmbs/wcsrtombs.o wcsmbs/mbsnrtowcs.o wcsmbs/wcsnrtombs.o wcsmbs/wcsnlen.o wcsmbs/wcschrnul.o wcsmbs/wcstol.o wcsmbs/wcstoul.o wcsmbs/wcstoll.o wcsmbs/wcstoull.o wcsmbs/wcstod.o wcsmbs/wcstold.o wcsmbs/wcstof.o wcsmbs/wcstol_l.o wcsmbs/wcstoul_l.o wcsmbs/wcstoll_l.o wcsmbs/wcstoull_l.o wcsmbs/wcstod_l.o wcsmbs/wcstold_l.o wcsmbs/wcstof_l.o wcsmbs/wcstod_nan.o wcsmbs/wcstold_nan.o wcsmbs/wcstof_nan.o wcsmbs/wcscoll.o wcsmbs/wcsxfrm.o wcsmbs/wcwidth.o wcsmbs/wcswidth.o wcsmbs/wcscoll_l.o wcsmbs/wcsxfrm_l.o wcsmbs/wcscasecmp.o wcsmbs/wcsncase.o wcsmbs/wcscasecmp_l.o wcsmbs/wcsncase_l.o wcsmbs/wcsmbsload.o wcsmbs/mbsrtowcs_l.o wcsmbs/isoc99_wscanf.o wcsmbs/isoc99_vwscanf.o wcsmbs/isoc99_fwscanf.o wcsmbs/isoc99_vfwscanf.o wcsmbs/isoc99_swscanf.o wcsmbs/isoc99_vswscanf.o wcsmbs/mbrtoc16.o wcsmbs/c16rtomb.o wcsmbs/mbrtoc32.o wcsmbs/c32rtomb.o wcsmbs/wcstof128_l.o wcsmbs/wcstof128.o wcsmbs/wcstof128_nan.o\n' >$build_dir/wcsmbs/stamp.o
printf 'wcsmbs/wcscat.os wcsmbs/wcschr.os wcsmbs/wcscmp.os wcsmbs/wcscpy.os wcsmbs/wcscspn.os wcsmbs/wcsdup.os wcsmbs/wcslen.os wcsmbs/wcsncat.os wcsmbs/wcsncmp.os wcsmbs/wcsncpy.os wcsmbs/wcspbrk.os wcsmbs/wcsrchr.os wcsmbs/wcsspn.os wcsmbs/wcstok.os wcsmbs/wcsstr.os wcsmbs/wmemchr.os wcsmbs/wmemcmp.os wcsmbs/wmemcpy.os wcsmbs/wmemmove.os wcsmbs/wmemset.os wcsmbs/wcpcpy.os wcsmbs/wcpncpy.os wcsmbs/wmempcpy.os wcsmbs/btowc.os wcsmbs/wctob.os wcsmbs/mbsinit.os wcsmbs/mbrlen.os wcsmbs/mbrtowc.os wcsmbs/wcrtomb.os wcsmbs/mbsrtowcs.os wcsmbs/wcsrtombs.os wcsmbs/mbsnrtowcs.os wcsmbs/wcsnrtombs.os wcsmbs/wcsnlen.os wcsmbs/wcschrnul.os wcsmbs/wcstol.os wcsmbs/wcstoul.os wcsmbs/wcstoll.os wcsmbs/wcstoull.os wcsmbs/wcstod.os wcsmbs/wcstold.os wcsmbs/wcstof.os wcsmbs/wcstol_l.os wcsmbs/wcstoul_l.os wcsmbs/wcstoll_l.os wcsmbs/wcstoull_l.os wcsmbs/wcstod_l.os wcsmbs/wcstold_l.os wcsmbs/wcstof_l.os wcsmbs/wcstod_nan.os wcsmbs/wcstold_nan.os wcsmbs/wcstof_nan.os wcsmbs/wcscoll.os wcsmbs/wcsxfrm.os wcsmbs/wcwidth.os wcsmbs/wcswidth.os wcsmbs/wcscoll_l.os wcsmbs/wcsxfrm_l.os wcsmbs/wcscasecmp.os wcsmbs/wcsncase.os wcsmbs/wcscasecmp_l.os wcsmbs/wcsncase_l.os wcsmbs/wcsmbsload.os wcsmbs/mbsrtowcs_l.os wcsmbs/isoc99_wscanf.os wcsmbs/isoc99_vwscanf.os wcsmbs/isoc99_fwscanf.os wcsmbs/isoc99_vfwscanf.os wcsmbs/isoc99_swscanf.os wcsmbs/isoc99_vswscanf.os wcsmbs/mbrtoc16.os wcsmbs/c16rtomb.os wcsmbs/mbrtoc32.os wcsmbs/c32rtomb.os wcsmbs/wcstof128_l.os wcsmbs/wcstof128.os wcsmbs/wcstof128_nan.os\n' >$build_dir/wcsmbs/stamp.os
printf '' >$build_dir/wcsmbs/stamp.oS
