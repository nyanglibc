printf "\
RESOLV LIBNSS_DNS****************************************************************\n"
mkdir -p $build_dir/resolv/libnss_dns
fns_shared="\
dns-host \
dns-network \
dns-canon \
"
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/resolv/libnss_dns/$fn.shared.s -o $build_dir/resolv/libnss_dns/$fn.os
done
cd $build_dir/resolv/libnss_dns
printf 'CREATING LIBNSS_DNS_PIC.A\n'
$ar cruv libnss_dns_pic.a dns-host.os dns-network.os dns-canon.os
printf 'CREATING LIBNSS_DNS.SO.2\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libnss_dns.versions.map \
	-soname=libnss_dns.so.2 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/resolv/libnss_dns/libnss_dns.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/resolv/libnss_dns/libnss_dns_pic.a \
	--no-whole-archive \
	$build_dir/resolv/libresolv/libresolv.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
