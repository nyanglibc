printf "\
INET****************************************************************************\n"
fns_pie_shared="\
htons \
inet_lnaof \
inet_mkadr \
inet_netof \
inet_ntoa \
inet_net \
herrno \
herrno-loc \
gethstbyad \
gethstbyad_r \
gethstbynm \
gethstbynm2 \
gethstbynm2_r \
gethstbynm_r \
gethstent \
gethstent_r \
getnetbyad \
getnetbyad_r \
getnetbynm \
getnetent \
getnetent_r \
getnetbynm_r \
getproto \
getproto_r \
getprtent \
getprtent_r \
getprtname \
getprtname_r \
getsrvbynm \
getsrvbynm_r \
getsrvbypt \
getsrvbypt_r \
getservent \
getservent_r \
getrpcent \
getrpcbyname \
getrpcbynumber \
getrpcent_r \
getrpcbyname_r \
getrpcbynumber_r \
ether_aton \
ether_aton_r \
ether_hton \
ether_line \
ether_ntoa \
ether_ntoa_r \
ether_ntoh \
rcmd \
rexec \
ruserpass \
bindresvport \
getnetgrent_r \
getnetgrent \
getaliasent_r \
getaliasent \
getaliasname \
getaliasname_r \
in6_addr \
getnameinfo \
if_index \
ifaddrs \
inet6_option \
getipv4sourcefilter \
setipv4sourcefilter \
getsourcefilter \
setsourcefilter \
inet6_opt \
inet6_rth \
inet6_scopeid_pton \
deadline \
idna \
idna_name_classify \
check_pf \
check_native \
ifreq \
netlink_assert_response \
\
htonl \
"
mkdir -p $build_dir/inet
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/inet/$fn.s -o $build_dir/inet/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/inet/$fn.shared.s -o $build_dir/inet/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'inet/htonl.o inet/htons.o inet/inet_lnaof.o inet/inet_mkadr.o inet/inet_netof.o inet/inet_ntoa.o inet/inet_net.o inet/herrno.o inet/herrno-loc.o inet/gethstbyad.o inet/gethstbyad_r.o inet/gethstbynm.o inet/gethstbynm2.o inet/gethstbynm2_r.o inet/gethstbynm_r.o inet/gethstent.o inet/gethstent_r.o inet/getnetbyad.o inet/getnetbyad_r.o inet/getnetbynm.o inet/getnetent.o inet/getnetent_r.o inet/getnetbynm_r.o inet/getproto.o inet/getproto_r.o inet/getprtent.o inet/getprtent_r.o inet/getprtname.o inet/getprtname_r.o inet/getsrvbynm.o inet/getsrvbynm_r.o inet/getsrvbypt.o inet/getsrvbypt_r.o inet/getservent.o inet/getservent_r.o inet/getrpcent.o inet/getrpcbyname.o inet/getrpcbynumber.o inet/getrpcent_r.o inet/getrpcbyname_r.o inet/getrpcbynumber_r.o inet/ether_aton.o inet/ether_aton_r.o inet/ether_hton.o inet/ether_line.o inet/ether_ntoa.o inet/ether_ntoa_r.o inet/ether_ntoh.o inet/rcmd.o inet/rexec.o inet/ruserpass.o inet/bindresvport.o inet/getnetgrent_r.o inet/getnetgrent.o inet/getaliasent_r.o inet/getaliasent.o inet/getaliasname.o inet/getaliasname_r.o inet/in6_addr.o inet/getnameinfo.o inet/if_index.o inet/ifaddrs.o inet/inet6_option.o inet/getipv4sourcefilter.o inet/setipv4sourcefilter.o inet/getsourcefilter.o inet/setsourcefilter.o inet/inet6_opt.o inet/inet6_rth.o inet/inet6_scopeid_pton.o inet/deadline.o inet/idna.o inet/idna_name_classify.o inet/check_pf.o inet/check_native.o inet/ifreq.o inet/netlink_assert_response.o\n' >$build_dir/inet/stamp.o
printf 'inet/htonl.os inet/htons.os inet/inet_lnaof.os inet/inet_mkadr.os inet/inet_netof.os inet/inet_ntoa.os inet/inet_net.os inet/herrno.os inet/herrno-loc.os inet/gethstbyad.os inet/gethstbyad_r.os inet/gethstbynm.os inet/gethstbynm2.os inet/gethstbynm2_r.os inet/gethstbynm_r.os inet/gethstent.os inet/gethstent_r.os inet/getnetbyad.os inet/getnetbyad_r.os inet/getnetbynm.os inet/getnetent.os inet/getnetent_r.os inet/getnetbynm_r.os inet/getproto.os inet/getproto_r.os inet/getprtent.os inet/getprtent_r.os inet/getprtname.os inet/getprtname_r.os inet/getsrvbynm.os inet/getsrvbynm_r.os inet/getsrvbypt.os inet/getsrvbypt_r.os inet/getservent.os inet/getservent_r.os inet/getrpcent.os inet/getrpcbyname.os inet/getrpcbynumber.os inet/getrpcent_r.os inet/getrpcbyname_r.os inet/getrpcbynumber_r.os inet/ether_aton.os inet/ether_aton_r.os inet/ether_hton.os inet/ether_line.os inet/ether_ntoa.os inet/ether_ntoa_r.os inet/ether_ntoh.os inet/rcmd.os inet/rexec.os inet/ruserpass.os inet/bindresvport.os inet/getnetgrent_r.os inet/getnetgrent.os inet/getaliasent_r.os inet/getaliasent.os inet/getaliasname.os inet/getaliasname_r.os inet/in6_addr.os inet/getnameinfo.os inet/if_index.os inet/ifaddrs.os inet/inet6_option.os inet/getipv4sourcefilter.os inet/setipv4sourcefilter.os inet/getsourcefilter.os inet/setsourcefilter.os inet/inet6_opt.os inet/inet6_rth.os inet/inet6_scopeid_pton.os inet/deadline.os inet/idna.os inet/idna_name_classify.os inet/check_pf.os inet/check_native.os inet/ifreq.os inet/netlink_assert_response.os\n' >$build_dir/inet/stamp.os
printf '' >$build_dir/inet/stamp.oS
