printf "\
ARGP****************************************************************************\n"
fns_pie_shared="\
argp-ba \
argp-fmtstream \
argp-fs-xinl \
argp-help \
argp-parse \
argp-pv \
argp-pvh \
argp-xinl \
argp-eexst \
"
mkdir -p $build_dir/argp
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/argp/$fn.s -o $build_dir/argp/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/argp/$fn.shared.s -o $build_dir/argp/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'argp/argp-ba.o argp/argp-fmtstream.o argp/argp-fs-xinl.o argp/argp-help.o argp/argp-parse.o argp/argp-pv.o argp/argp-pvh.o argp/argp-xinl.o argp/argp-eexst.o\n' >$build_dir/argp/stamp.o
printf 'argp/argp-ba.os argp/argp-fmtstream.os argp/argp-fs-xinl.os argp/argp-help.os argp/argp-parse.os argp/argp-pv.os argp/argp-pvh.os argp/argp-xinl.os argp/argp-eexst.os\n' >$build_dir/argp/stamp.os
printf '' >$build_dir/argp/stamp.oS
