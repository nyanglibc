printf "\
RTLD POSIX**********************************************************************\n"
fns_shared="\
_exit \
environ \
\
uname \
getpid \
"
mkdir -p $build_dir/posix
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/posix/rtld-$fn.shared.s -o $build_dir/posix/rtld-$fn.os
done
