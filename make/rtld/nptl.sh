printf "\
RTLD NPTL***********************************************************************\n"
fns_shared="\
libc-lowlevellock \
libc-cancellation \
forward \
"
mkdir -p $build_dir/nptl
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/nptl/rtld-$fn.shared.s -o $build_dir/nptl/rtld-$fn.os
done
