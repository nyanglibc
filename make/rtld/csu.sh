printf "\
RTLD CSU************************************************************************\n"
fns_shared="\
check_fds \
errno \
"
mkdir -p $build_dir/csu
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/csu/rtld-$fn.shared.s -o $build_dir/csu/rtld-$fn.os
done
