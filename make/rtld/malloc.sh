printf "\
RTLD MALLOC*********************************************************************\n"
fns_shared="\
scratch_buffer_set_array_size \
"
mkdir -p $build_dir/malloc
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/malloc/rtld-$fn.shared.s -o $build_dir/malloc/rtld-$fn.os
done
