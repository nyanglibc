printf "\
RTLD IO*************************************************************************\n"
fns_shared="\
stat64 \
fstat64 \
lstat64 \
fstatat64 \
openat64 \
lseek64 \
access \
close_nocancel \
fcntl_nocancel \
open64_nocancel \
read_nocancel \
pread64_nocancel \
write_nocancel \
"
mkdir -p $build_dir/io
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/io/rtld-$fn.shared.s -o $build_dir/io/rtld-$fn.os
done
