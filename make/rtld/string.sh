printf "\
RTLD STRING*********************************************************************\n"
fns_shared="\
strdup \
cacheinfo \
\
strchr \
strcmp \
strcspn \
strlen \
strnlen \
strncmp \
memchr \
memcmp \
memmove \
memset \
stpcpy \
rawmemchr \
"
mkdir -p $build_dir/string
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/string/rtld-$fn.shared.s -o $build_dir/string/rtld-$fn.os
done
