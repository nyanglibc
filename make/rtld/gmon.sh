printf "\
RTLD GMON***********************************************************************\n"
fns_shared="\
profil \
prof-freq \
"
mkdir -p $build_dir/gmon
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/gmon/rtld-$fn.shared.s -o $build_dir/gmon/rtld-$fn.os
done
