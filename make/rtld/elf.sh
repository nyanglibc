printf "\
RTLD ELF************************************************************************\n"
fns_shared="\
dl-addr-obj \
"
mkdir -p $build_dir/elf
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/elf/rtld-$fn.shared.s -o $build_dir/elf/rtld-$fn.os
done
