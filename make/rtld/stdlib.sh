printf "\
RTLD STDLIB*********************************************************************\n"
fns_shared="\
exit \
cxa_atexit \
cxa_thread_atexit_impl \
"
mkdir -p $build_dir/stdlib
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/stdlib/rtld-$fn.shared.s -o $build_dir/stdlib/rtld-$fn.os
done
