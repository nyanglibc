printf "\
RTLD SIGNAL*********************************************************************\n"
fns_shared="\
sigaction \
"
mkdir -p $build_dir/signal
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/signal/rtld-$fn.shared.s -o $build_dir/signal/rtld-$fn.os
done
