printf "\
RTLD DIRENT*********************************************************************\n"
fns_shared="\
closedir \
rewinddir \
readdir64 \
fdopendir \
getdents64 \
"
mkdir -p $build_dir/dirent
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/dirent/rtld-$fn.shared.s -o $build_dir/dirent/rtld-$fn.os
done
