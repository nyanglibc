printf "\
RTLD SETJMP*********************************************************************\n"
fns_shared="\
setjmp \
__longjmp \
"
mkdir -p $build_dir/setjmp
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/setjmp/rtld-$fn.shared.s -o $build_dir/setjmp/rtld-$fn.os
done
