printf "\
RTLD MISC***********************************************************************\n"
fns_shared="\
mmap64 \
munmap \
mprotect \
"
mkdir -p $build_dir/misc
for fn in $fns_shared
do
	printf "ASSEMBLING RTLD SHARED rtld-$fn\n"
	$as $src_dir/misc/rtld-$fn.shared.s -o $build_dir/misc/rtld-$fn.os
done
