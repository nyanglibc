printf "\
LOGIN***************************************************************************\n"
fns_pie_shared="\
getlogin \
getlogin_r \
setlogin \
getlogin_r_chk \
getutent \
getutent_r \
getutid \
getutline \
getutid_r \
getutline_r \
utmp_file \
utmpname \
updwtmp \
getpt \
grantpt \
unlockpt \
ptsname \
ptsname_r_chk \
setutxent \
getutxent \
endutxent \
getutxid \
getutxline \
pututxline \
utmpxname \
updwtmpx \
getutmpx \
getutmp \
"
mkdir -p $build_dir/login
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/login/$fn.s -o $build_dir/login/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/login/$fn.shared.s -o $build_dir/login/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'login/getlogin.o login/getlogin_r.o login/setlogin.o login/getlogin_r_chk.o login/getutent.o login/getutent_r.o login/getutid.o login/getutline.o login/getutid_r.o login/getutline_r.o login/utmp_file.o login/utmpname.o login/updwtmp.o login/getpt.o login/grantpt.o login/unlockpt.o login/ptsname.o login/ptsname_r_chk.o login/setutxent.o login/getutxent.o login/endutxent.o login/getutxid.o login/getutxline.o login/pututxline.o login/utmpxname.o login/updwtmpx.o login/getutmpx.o login/getutmp.o\n' >$build_dir/login/stamp.o
printf 'login/getlogin.os login/getlogin_r.os login/setlogin.os login/getlogin_r_chk.os login/getutent.os login/getutent_r.os login/getutid.os login/getutline.os login/getutid_r.os login/getutline_r.os login/utmp_file.os login/utmpname.os login/updwtmp.os login/getpt.os login/grantpt.os login/unlockpt.os login/ptsname.os login/ptsname_r_chk.os login/setutxent.os login/getutxent.os login/endutxent.os login/getutxid.os login/getutxline.os login/pututxline.os login/utmpxname.os login/updwtmpx.os login/getutmpx.os login/getutmp.os\n' >$build_dir/login/stamp.os
printf '' >$build_dir/login/stamp.oS
