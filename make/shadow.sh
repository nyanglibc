printf "\
SHADOW**************************************************************************\n"
fns_pie_shared="\
getspent \
getspnam \
sgetspent \
fgetspent \
putspent \
getspent_r \
getspnam_r \
sgetspent_r \
fgetspent_r \
lckpwdf \
"
mkdir -p $build_dir/shadow
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/shadow/$fn.s -o $build_dir/shadow/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/shadow/$fn.shared.s -o $build_dir/shadow/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'shadow/getspent.o shadow/getspnam.o shadow/sgetspent.o shadow/fgetspent.o shadow/putspent.o shadow/getspent_r.o shadow/getspnam_r.o shadow/sgetspent_r.o shadow/fgetspent_r.o shadow/lckpwdf.o\n' >$build_dir/shadow/stamp.o
printf 'shadow/getspent.os shadow/getspnam.os shadow/sgetspent.os shadow/fgetspent.os shadow/putspent.os shadow/getspent_r.os shadow/getspnam_r.os shadow/sgetspent_r.os shadow/fgetspent_r.os shadow/lckpwdf.os\n' >$build_dir/shadow/stamp.os
printf '' >$build_dir/shadow/stamp.oS
