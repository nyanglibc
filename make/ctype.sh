printf "\
CTYPE***************************************************************************\n"
fns="\
ctype \
ctype-c99 \
ctype-extn \
ctype-c99_l \
ctype_l \
isctype \
ctype-info \
"
mkdir -p $build_dir/ctype
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/ctype/$fn.s -o $build_dir/ctype/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/ctype/$fn.shared.s -o $build_dir/ctype/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'ctype/ctype.o ctype/ctype-c99.o ctype/ctype-extn.o ctype/ctype-c99_l.o ctype/ctype_l.o ctype/isctype.o ctype/ctype-info.o\n' >$build_dir/ctype/stamp.o
printf 'ctype/ctype.os ctype/ctype-c99.os ctype/ctype-extn.os ctype/ctype-c99_l.os ctype/ctype_l.os ctype/isctype.os ctype/ctype-info.os\n' >$build_dir/ctype/stamp.os
printf '' >$build_dir/ctype/stamp.oS
