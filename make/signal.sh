printf "\
SIGNAL**************************************************************************\n"
fns="\
signal \
raise \
killpg \
sigaction \
sigprocmask \
sigpending \
sigsuspend \
sigwait \
sigblock \
sigsetmask \
sigpause \
sigvec \
sigstack \
sigintr \
sigsetops \
sigempty \
sigfillset \
sigaddset \
sigdelset \
sigismem \
sigreturn \
siggetmask \
sysv_signal \
sigisempty \
sigandset \
sigorset \
allocrtsig \
sigtimedwait \
sigwaitinfo \
sigqueue \
sighold \
sigrelse \
sigignore \
sigset \
\
kill \
sigaltstack \
"
mkdir -p $build_dir/signal
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/signal/$fn.s -o $build_dir/signal/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/signal/$fn.shared.s -o $build_dir/signal/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'signal/signal.o signal/raise.o signal/killpg.o signal/sigaction.o signal/sigprocmask.o signal/kill.o signal/sigpending.o signal/sigsuspend.o signal/sigwait.o signal/sigblock.o signal/sigsetmask.o signal/sigpause.o signal/sigvec.o signal/sigstack.o signal/sigaltstack.o signal/sigintr.o signal/sigsetops.o signal/sigempty.o signal/sigfillset.o signal/sigaddset.o signal/sigdelset.o signal/sigismem.o signal/sigreturn.o signal/siggetmask.o signal/sysv_signal.o signal/sigisempty.o signal/sigandset.o signal/sigorset.o signal/allocrtsig.o signal/sigtimedwait.o signal/sigwaitinfo.o signal/sigqueue.o signal/sighold.o signal/sigrelse.o signal/sigignore.o signal/sigset.o\n' >$build_dir/signal/stamp.o
printf 'signal/signal.os signal/raise.os signal/killpg.os signal/sigaction.os signal/sigprocmask.os signal/kill.os signal/sigpending.os signal/sigsuspend.os signal/sigwait.os signal/sigblock.os signal/sigsetmask.os signal/sigpause.os signal/sigvec.os signal/sigstack.os signal/sigaltstack.os signal/sigintr.os signal/sigsetops.os signal/sigempty.os signal/sigfillset.os signal/sigaddset.os signal/sigdelset.os signal/sigismem.os signal/sigreturn.os signal/siggetmask.os signal/sysv_signal.os signal/sigisempty.os signal/sigandset.os signal/sigorset.os signal/allocrtsig.os signal/sigtimedwait.os signal/sigwaitinfo.os signal/sigqueue.os signal/sighold.os signal/sigrelse.os signal/sigignore.os signal/sigset.os\n' >$build_dir/signal/stamp.os
printf '' >$build_dir/signal/stamp.oS
