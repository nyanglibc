printf "\
ASSERT**************************************************************************\n"
fns="\
assert \
assert-perr \
__assert \
"
mkdir -p $build_dir/assert
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/assert/$fn.s -o $build_dir/assert/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/assert/$fn.shared.s -o $build_dir/assert/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'assert/assert.o assert/assert-perr.o assert/__assert.o\n' >$build_dir/assert/stamp.o
printf 'assert/assert.os assert/assert-perr.os assert/__assert.os\n' >$build_dir/assert/stamp.os
printf '' >$build_dir/assert/stamp.oS
