printf "\
GSHADOW*************************************************************************\n"
fns_pie_shared="\
getsgent \
getsgnam \
sgetsgent \
fgetsgent \
putsgent \
getsgent_r \
getsgnam_r \
sgetsgent_r \
fgetsgent_r \
"
mkdir -p $build_dir/gshadow
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/gshadow/$fn.s -o $build_dir/gshadow/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/gshadow/$fn.shared.s -o $build_dir/gshadow/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'gshadow/getsgent.o gshadow/getsgnam.o gshadow/sgetsgent.o gshadow/fgetsgent.o gshadow/putsgent.o gshadow/getsgent_r.o gshadow/getsgnam_r.o gshadow/sgetsgent_r.o gshadow/fgetsgent_r.o\n' >$build_dir/gshadow/stamp.o
printf 'gshadow/getsgent.os gshadow/getsgnam.os gshadow/sgetsgent.os gshadow/fgetsgent.os gshadow/putsgent.os gshadow/getsgent_r.os gshadow/getsgnam_r.os gshadow/sgetsgent_r.os gshadow/fgetsgent_r.os\n' >$build_dir/gshadow/stamp.os
printf '' >$build_dir/gshadow/stamp.oS
