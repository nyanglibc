printf "\
ELF EXTRA***********************************************************************\n"
gens="\
interp.shared.s.in \
"
mkdir -p $build_dir/elf
for g in $gens
do
	sed -E -e "s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/elf/$g >$build_dir/elf/$(basename $g .in)
done
paths_pie_nonlib="\
$src_dir/elf/static-stubs \
$src_dir/elf/cache \
$src_dir/elf/readlib \
$src_dir/elf/xmalloc \
$src_dir/elf/xstrdup \
$src_dir/elf/chroot_canon \
$src_dir/elf/stringtable \
"
paths_shared="\
$src_dir/elf/sofini \
$build_dir/elf/interp \
$src_dir/elf/sotruss-lib \
"
mkdir -p $build_dir/elf
for p in $paths_pie_nonlib
do
	printf "ASSEMBLING PIE $p\n"
	$as $p.s -o $build_dir/elf/$(basename $p .in).o
done
for p in $paths_shared
do
	printf "ASSEMBLING SHARED $p\n"
	$as $p.shared.s -o $build_dir/elf/$(basename $p .in).os
done
