printf "\
RT******************************************************************************\n"
fns_pie_shared="\
aio_cancel \
aio_error \
aio_fsync \
aio_misc \
aio_read \
aio_read64 \
aio_return \
aio_suspend \
aio_write \
aio_write64 \
lio_listio \
lio_listio64 \
aio_sigqueue \
aio_notify \
timer_create \
timer_delete \
timer_getoverr \
timer_gettime \
timer_settime \
shm_open \
shm_unlink \
mq_open \
mq_close \
mq_unlink \
mq_getattr \
\
mq_notify \
mq_send \
mq_receive \
mq_timedsend \
mq_timedreceive \
timer_routines \
librt-cancellation \
\
mq_setattr \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
rt-unwind-resume \
"
mkdir -p $build_dir/rt
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/rt/$fn.s -o $build_dir/rt/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/rt/$fn.shared.s -o $build_dir/rt/$fn.os
done
printf 'CREATING LIBRT.A\n'
cd $build_dir/rt
$ar cruv librt.a aio_cancel.o aio_error.o aio_fsync.o aio_misc.o aio_read.o aio_read64.o aio_return.o aio_suspend.o aio_write.o aio_write64.o lio_listio.o lio_listio64.o aio_sigqueue.o aio_notify.o timer_create.o timer_delete.o timer_getoverr.o timer_gettime.o timer_settime.o shm_open.o shm_unlink.o mq_open.o mq_close.o mq_unlink.o mq_getattr.o mq_setattr.o mq_notify.o mq_send.o mq_receive.o mq_timedsend.o mq_timedreceive.o timer_routines.o librt-cancellation.o
printf 'CREATING LIBRT_PIC.A\n'
$ar cruv librt_pic.a aio_cancel.os aio_error.os aio_fsync.os aio_misc.os aio_read.os aio_read64.os aio_return.os aio_suspend.os aio_write.os aio_write64.os lio_listio.os lio_listio64.os aio_sigqueue.os aio_notify.os timer_create.os timer_delete.os timer_getoverr.os timer_gettime.os timer_settime.os shm_open.os shm_unlink.os mq_open.os mq_close.os mq_unlink.os mq_getattr.os mq_setattr.os mq_notify.os mq_send.os mq_receive.os mq_timedsend.os mq_timedreceive.os timer_routines.os librt-cancellation.os rt-unwind-resume.os
printf 'CREATING LIBRT.SO.1\n'
# it is using libgcc crt{begin.end}S.o we did build for libpthread
$ld \
	-s \
	-shared  \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	$build_dir/libgcc/libpthread/crtbeginS.o \
	-L$build_dir/csu/ \
	--version-script=$src_dir/librt.versions.map \
	-soname=librt.so.1 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	--enable-new-dtags \
	-z nodelete \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/rt/librt.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o  \
	--whole-archive \
		$build_dir/rt/librt_pic.a \
	--no-whole-archive \
	$build_dir/nptl/libpthread.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group \
	$build_dir/libgcc/libpthread/crtendS.o
