printf "\
HESIOD**************************************************************************\n"
mkdir -p $build_dir/hesiod
fns_shared="\
hesiod \
hesiod-grp \
hesiod-proto \
hesiod-pwd \
hesiod-service \
"
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/hesiod/$fn.shared.s -o $build_dir/hesiod/$fn.os
done
cd $build_dir/hesiod
printf 'CREATING LIBNSS_HESIOD_PIC.A\n'
$ar cruv libnss_hesiod_pic.a hesiod.os hesiod-grp.os hesiod-proto.os hesiod-pwd.os hesiod-service.os
printf 'CREATING LIBNSS_HESIOD.SO.2\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libnss_hesiod.versions.map \
	-soname=libnss_hesiod.so.2 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/hesiod/libnss_hesiod.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/hesiod/libnss_hesiod_pic.a \
	--no-whole-archive \
	$build_dir/resolv/libresolv/libresolv.so \
	$build_dir/nss/libnss_files/libnss_files.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
