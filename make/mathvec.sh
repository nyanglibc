printf "\
MATHVEC*************************************************************************\n"
mkdir -p $build_dir/mathvec
fns_pie_shared="\
svml_d_cos2_core \
svml_d_cos4_core_avx \
svml_d_cos4_core \
svml_d_cos8_core \
svml_d_sin2_core \
svml_d_sin4_core_avx \
svml_d_sin4_core \
svml_d_sin8_core \
svml_d_trig_data \
svml_s_cosf4_core \
svml_s_cosf8_core_avx \
svml_s_cosf8_core \
svml_s_cosf16_core \
svml_s_trig_data \
svml_s_sinf4_core \
svml_s_sinf8_core_avx \
svml_s_sinf8_core \
svml_s_sinf16_core \
svml_d_sincos2_core \
svml_d_sincos4_core_avx \
svml_d_sincos4_core \
svml_d_sincos8_core \
svml_d_log2_core \
svml_d_log4_core_avx \
svml_d_log4_core \
svml_d_log8_core \
svml_d_log_data \
svml_s_logf4_core \
svml_s_logf8_core_avx \
svml_s_logf8_core \
svml_s_logf16_core \
svml_s_logf_data \
svml_d_exp2_core \
svml_d_exp4_core_avx \
svml_d_exp4_core \
svml_d_exp8_core \
svml_d_exp_data \
svml_s_expf4_core \
svml_s_expf8_core_avx \
svml_s_expf8_core \
svml_s_expf16_core \
svml_s_expf_data \
svml_d_pow2_core \
svml_d_pow4_core_avx \
svml_d_pow4_core \
svml_d_pow8_core \
svml_d_pow_data \
svml_s_powf4_core \
svml_s_powf8_core_avx \
svml_s_powf8_core \
svml_s_powf16_core \
svml_s_powf_data \
svml_s_sincosf4_core \
svml_s_sincosf8_core_avx \
svml_s_sincosf8_core \
svml_s_sincosf16_core \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/mathvec/$fn.s -o $build_dir/mathvec/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/mathvec/$fn.shared.s -o $build_dir/mathvec/$fn.os
done
printf 'CREATING LIBMVEC.A\n'
cd $build_dir/mathvec
$ar cruv libmvec.a svml_d_cos2_core.o svml_d_cos4_core_avx.o svml_d_cos4_core.o svml_d_cos8_core.o svml_d_sin2_core.o svml_d_sin4_core_avx.o svml_d_sin4_core.o svml_d_sin8_core.o svml_d_trig_data.o svml_s_cosf4_core.o svml_s_cosf8_core_avx.o svml_s_cosf8_core.o svml_s_cosf16_core.o svml_s_trig_data.o svml_s_sinf4_core.o svml_s_sinf8_core_avx.o svml_s_sinf8_core.o svml_s_sinf16_core.o svml_d_sincos2_core.o svml_d_sincos4_core_avx.o svml_d_sincos4_core.o svml_d_sincos8_core.o svml_d_log2_core.o svml_d_log4_core_avx.o svml_d_log4_core.o svml_d_log8_core.o svml_d_log_data.o svml_s_logf4_core.o svml_s_logf8_core_avx.o svml_s_logf8_core.o svml_s_logf16_core.o svml_s_logf_data.o svml_d_exp2_core.o svml_d_exp4_core_avx.o svml_d_exp4_core.o svml_d_exp8_core.o svml_d_exp_data.o svml_s_expf4_core.o svml_s_expf8_core_avx.o svml_s_expf8_core.o svml_s_expf16_core.o svml_s_expf_data.o svml_d_pow2_core.o svml_d_pow4_core_avx.o svml_d_pow4_core.o svml_d_pow8_core.o svml_d_pow_data.o svml_s_powf4_core.o svml_s_powf8_core_avx.o svml_s_powf8_core.o svml_s_powf16_core.o svml_s_powf_data.o svml_s_sincosf4_core.o svml_s_sincosf8_core_avx.o svml_s_sincosf8_core.o svml_s_sincosf16_core.o
printf 'CREATING LIBMVEC_PIC.A\n'
$ar cruv libmvec_pic.a svml_d_cos2_core.os svml_d_cos4_core_avx.os svml_d_cos4_core.os svml_d_cos8_core.os svml_d_sin2_core.os svml_d_sin4_core_avx.os svml_d_sin4_core.os svml_d_sin8_core.os svml_d_trig_data.os svml_s_cosf4_core.os svml_s_cosf8_core_avx.os svml_s_cosf8_core.os svml_s_cosf16_core.os svml_s_trig_data.os svml_s_sinf4_core.os svml_s_sinf8_core_avx.os svml_s_sinf8_core.os svml_s_sinf16_core.os svml_d_sincos2_core.os svml_d_sincos4_core_avx.os svml_d_sincos4_core.os svml_d_sincos8_core.os svml_d_log2_core.os svml_d_log4_core_avx.os svml_d_log4_core.os svml_d_log8_core.os svml_d_log_data.os svml_s_logf4_core.os svml_s_logf8_core_avx.os svml_s_logf8_core.os svml_s_logf16_core.os svml_s_logf_data.os svml_d_exp2_core.os svml_d_exp4_core_avx.os svml_d_exp4_core.os svml_d_exp8_core.os svml_d_exp_data.os svml_s_expf4_core.os svml_s_expf8_core_avx.os svml_s_expf8_core.os svml_s_expf16_core.os svml_s_expf_data.os svml_d_pow2_core.os svml_d_pow4_core_avx.os svml_d_pow4_core.os svml_d_pow8_core.os svml_d_pow_data.os svml_s_powf4_core.os svml_s_powf8_core_avx.os svml_s_powf8_core.os svml_s_powf16_core.os svml_s_powf_data.os svml_s_sincosf4_core.os svml_s_sincosf8_core_avx.os svml_s_sincosf8_core.os svml_s_sincosf16_core.os
printf 'CREATING LIBMVEC.SO.1\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libmvec.versions.map \
	-soname=libmvec.so.1 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/mathvec/libmvec.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/mathvec/libmvec_pic.a \
	--no-whole-archive \
	$build_dir/math/libm/libm.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
