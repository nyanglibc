printf "\
SYSVIPC*************************************************************************\n"
fns_pie_shared="\
ftok \
msgsnd \
msgrcv \
msgget \
msgctl \
semop \
semget \
semctl \
semtimedop \
shmat \
shmdt \
shmget \
shmctl \
"
mkdir -p $build_dir/sysvipc
for fn in $fns_pie_shared
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/sysvipc/$fn.s -o $build_dir/sysvipc/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/sysvipc/$fn.shared.s -o $build_dir/sysvipc/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'sysvipc/ftok.o sysvipc/msgsnd.o sysvipc/msgrcv.o sysvipc/msgget.o sysvipc/msgctl.o sysvipc/semop.o sysvipc/semget.o sysvipc/semctl.o sysvipc/semtimedop.o sysvipc/shmat.o sysvipc/shmdt.o sysvipc/shmget.o sysvipc/shmctl.o\n' >$build_dir/sysvipc/stamp.o
printf 'sysvipc/ftok.os sysvipc/msgsnd.os sysvipc/msgrcv.os sysvipc/msgget.os sysvipc/msgctl.os sysvipc/semop.os sysvipc/semget.os sysvipc/semctl.os sysvipc/semtimedop.os sysvipc/shmat.os sysvipc/shmdt.os sysvipc/shmget.os sysvipc/shmctl.os\n' >$build_dir/sysvipc/stamp.os
printf '' >$build_dir/sysvipc/stamp.oS
