printf "\
STDIO-COMMON********************************************************************\n"
fns_pie_shared="\
ctermid \
cuserid \
_itoa \
_itowa \
itoa-digits \
itoa-udigits \
itowa-digits \
vfprintf \
vprintf \
printf_fp \
reg-printf \
printf-prs \
printf_fphex \
reg-modifier \
reg-type \
printf_size \
fprintf \
printf \
snprintf \
sprintf \
asprintf \
dprintf \
vfwprintf \
vfscanf \
vfwscanf \
fscanf \
scanf \
sscanf \
perror \
psignal \
tmpfile \
tmpfile64 \
tmpnam \
tmpnam_r \
tempnam \
tempname \
getline \
getw \
putw \
remove \
rename \
renameat \
renameat2 \
flockfile \
ftrylockfile \
funlockfile \
isoc99_scanf \
isoc99_vscanf \
isoc99_fscanf \
isoc99_vfscanf \
isoc99_sscanf \
isoc99_vsscanf \
psiginfo \
gentempfd \
vfscanf-internal \
vfwscanf-internal \
iovfscanf \
iovfwscanf \
vfprintf-internal \
vfwprintf-internal \
errlist \
siglist \
printf-parsemb \
printf-parsewc \
fxprintf \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/stdio-common
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/stdio-common/$fn.s -o $build_dir/stdio-common/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/stdio-common/$fn.shared.s -o $build_dir/stdio-common/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'stdio-common/ctermid.o stdio-common/cuserid.o stdio-common/_itoa.o stdio-common/_itowa.o stdio-common/itoa-digits.o stdio-common/itoa-udigits.o stdio-common/itowa-digits.o stdio-common/vfprintf.o stdio-common/vprintf.o stdio-common/printf_fp.o stdio-common/reg-printf.o stdio-common/printf-prs.o stdio-common/printf_fphex.o stdio-common/reg-modifier.o stdio-common/reg-type.o stdio-common/printf_size.o stdio-common/fprintf.o stdio-common/printf.o stdio-common/snprintf.o stdio-common/sprintf.o stdio-common/asprintf.o stdio-common/dprintf.o stdio-common/vfwprintf.o stdio-common/vfscanf.o stdio-common/vfwscanf.o stdio-common/fscanf.o stdio-common/scanf.o stdio-common/sscanf.o stdio-common/perror.o stdio-common/psignal.o stdio-common/tmpfile.o stdio-common/tmpfile64.o stdio-common/tmpnam.o stdio-common/tmpnam_r.o stdio-common/tempnam.o stdio-common/tempname.o stdio-common/getline.o stdio-common/getw.o stdio-common/putw.o stdio-common/remove.o stdio-common/rename.o stdio-common/renameat.o stdio-common/renameat2.o stdio-common/flockfile.o stdio-common/ftrylockfile.o stdio-common/funlockfile.o stdio-common/isoc99_scanf.o stdio-common/isoc99_vscanf.o stdio-common/isoc99_fscanf.o stdio-common/isoc99_vfscanf.o stdio-common/isoc99_sscanf.o stdio-common/isoc99_vsscanf.o stdio-common/psiginfo.o stdio-common/gentempfd.o stdio-common/vfscanf-internal.o stdio-common/vfwscanf-internal.o stdio-common/iovfscanf.o stdio-common/iovfwscanf.o stdio-common/vfprintf-internal.o stdio-common/vfwprintf-internal.o stdio-common/errlist.o stdio-common/siglist.o stdio-common/printf-parsemb.o stdio-common/printf-parsewc.o stdio-common/fxprintf.o\n' >$build_dir/stdio-common/stamp.o
printf 'stdio-common/ctermid.os stdio-common/cuserid.os stdio-common/_itoa.os stdio-common/_itowa.os stdio-common/itoa-digits.os stdio-common/itoa-udigits.os stdio-common/itowa-digits.os stdio-common/vfprintf.os stdio-common/vprintf.os stdio-common/printf_fp.os stdio-common/reg-printf.os stdio-common/printf-prs.os stdio-common/printf_fphex.os stdio-common/reg-modifier.os stdio-common/reg-type.os stdio-common/printf_size.os stdio-common/fprintf.os stdio-common/printf.os stdio-common/snprintf.os stdio-common/sprintf.os stdio-common/asprintf.os stdio-common/dprintf.os stdio-common/vfwprintf.os stdio-common/vfscanf.os stdio-common/vfwscanf.os stdio-common/fscanf.os stdio-common/scanf.os stdio-common/sscanf.os stdio-common/perror.os stdio-common/psignal.os stdio-common/tmpfile.os stdio-common/tmpfile64.os stdio-common/tmpnam.os stdio-common/tmpnam_r.os stdio-common/tempnam.os stdio-common/tempname.os stdio-common/getline.os stdio-common/getw.os stdio-common/putw.os stdio-common/remove.os stdio-common/rename.os stdio-common/renameat.os stdio-common/renameat2.os stdio-common/flockfile.os stdio-common/ftrylockfile.os stdio-common/funlockfile.os stdio-common/isoc99_scanf.os stdio-common/isoc99_vscanf.os stdio-common/isoc99_fscanf.os stdio-common/isoc99_vfscanf.os stdio-common/isoc99_sscanf.os stdio-common/isoc99_vsscanf.os stdio-common/psiginfo.os stdio-common/gentempfd.os stdio-common/vfscanf-internal.os stdio-common/vfwscanf-internal.os stdio-common/iovfscanf.os stdio-common/iovfwscanf.os stdio-common/vfprintf-internal.os stdio-common/vfwprintf-internal.os stdio-common/errlist.os stdio-common/siglist.os stdio-common/printf-parsemb.os stdio-common/printf-parsewc.os stdio-common/fxprintf.os\n' >$build_dir/stdio-common/stamp.os
printf '' >$build_dir/stdio-common/stamp.oS
