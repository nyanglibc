printf "\
MALLOC**************************************************************************\n"
fns_pie_shared="\
malloc \
morecore \
mcheck \
mtrace \
obstack \
reallocarray \
scratch_buffer_dupfree \
scratch_buffer_grow \
scratch_buffer_grow_preserve \
scratch_buffer_set_array_size \
dynarray_at_failure \
dynarray_emplace_enlarge \
dynarray_finalize \
dynarray_resize \
dynarray_resize_clear \
alloc_buffer_alloc_array \
alloc_buffer_allocate \
alloc_buffer_copy_bytes \
alloc_buffer_copy_string \
alloc_buffer_create_failure \
set-freeres \
thread-freeres \
"
fns_pie="\
$fns_pie_shared \
"

fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/malloc
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/malloc/$fn.s -o $build_dir/malloc/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/malloc/$fn.shared.s -o $build_dir/malloc/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'malloc/malloc.o malloc/morecore.o malloc/mcheck.o malloc/mtrace.o malloc/obstack.o malloc/reallocarray.o malloc/scratch_buffer_dupfree.o malloc/scratch_buffer_grow.o malloc/scratch_buffer_grow_preserve.o malloc/scratch_buffer_set_array_size.o malloc/dynarray_at_failure.o malloc/dynarray_emplace_enlarge.o malloc/dynarray_finalize.o malloc/dynarray_resize.o malloc/dynarray_resize_clear.o malloc/alloc_buffer_alloc_array.o malloc/alloc_buffer_allocate.o malloc/alloc_buffer_copy_bytes.o malloc/alloc_buffer_copy_string.o malloc/alloc_buffer_create_failure.o malloc/set-freeres.o malloc/thread-freeres.o\n' >$build_dir/malloc/stamp.o
printf 'malloc/malloc.os malloc/morecore.os malloc/mcheck.os malloc/mtrace.os malloc/obstack.os malloc/reallocarray.os malloc/scratch_buffer_dupfree.os malloc/scratch_buffer_grow.os malloc/scratch_buffer_grow_preserve.os malloc/scratch_buffer_set_array_size.os malloc/dynarray_at_failure.os malloc/dynarray_emplace_enlarge.os malloc/dynarray_finalize.os malloc/dynarray_resize.os malloc/dynarray_resize_clear.os malloc/alloc_buffer_alloc_array.os malloc/alloc_buffer_allocate.os malloc/alloc_buffer_copy_bytes.os malloc/alloc_buffer_copy_string.os malloc/alloc_buffer_create_failure.os malloc/set-freeres.os malloc/thread-freeres.os\n' >$build_dir/malloc/stamp.os
printf '' >$build_dir/malloc/stamp.oS
