printf "\
DLFCN LIBDL*********************************************************************\n"
fns_pie_shared="\
dlopen \
dlclose \
dlsym \
dlvsym \
dlerror \
dladdr \
dladdr1 \
dlinfo \
dlmopen \
dlfreeres \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
dlfcn \
dlopenold \
"
mkdir -p $build_dir/dlfcn/libdl
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/dlfcn/libdl/$fn.s -o $build_dir/dlfcn/libdl/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/dlfcn/libdl/$fn.shared.s -o $build_dir/dlfcn/libdl/$fn.os
done
printf 'CREATING LIBDL.A\n'
cd $build_dir/dlfcn/libdl
$ar cruv libdl.a dlopen.o dlclose.o dlsym.o dlvsym.o dlerror.o dladdr.o dladdr1.o dlinfo.o dlmopen.o dlfreeres.o
printf 'CREATING LIBDL_PIC.A\n'
$ar cruv libdl_pic.a dlopen.os dlclose.os dlsym.os dlvsym.os dlerror.os dladdr.os dladdr1.os dlinfo.os dlmopen.os dlfcn.os dlfreeres.os dlopenold.os
printf 'CREATING LIBDL.SO.2\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libdl.versions.map \
	-soname=libdl.so.2 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/dlfcn/libdl/libdl.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/dlfcn/libdl/libdl_pic.a \
	--no-whole-archive \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
