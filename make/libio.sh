printf "\
LIBIO***************************************************************************\n"
fns_pie_shared="\
filedoalloc \
iofclose \
iofdopen \
iofflush \
iofgetpos \
iofgets \
iofopen \
iofopncook \
iofputs \
iofread \
iofsetpos \
ioftell \
wfiledoalloc \
iofwrite \
iogetdelim \
iogetline \
iogets \
iopadn \
iopopen \
ioputs \
ioseekoff \
ioseekpos \
iosetbuffer \
iosetvbuf \
ioungetc \
iovsprintf \
iovsscanf \
iofgetpos64 \
iofopen64 \
iofsetpos64 \
fputwc \
fputwc_u \
getwc \
getwc_u \
getwchar \
getwchar_u \
iofgetws \
iofgetws_u \
iofputws \
iofputws_u \
iogetwline \
iowpadn \
ioungetwc \
putwc \
putwc_u \
putwchar \
putwchar_u \
putchar \
putchar_u \
fwprintf \
swprintf \
vwprintf \
wprintf \
wscanf \
fwscanf \
vwscanf \
vswprintf \
iovswscanf \
swscanf \
wgenops \
wstrops \
wfileops \
iofwide \
fwide \
wmemstream \
clearerr \
feof \
ferror \
fileno \
fputc \
freopen \
fseek \
getc \
getchar \
memstream \
pclose \
putc \
rewind \
setbuf \
setlinebuf \
vasprintf \
iovdprintf \
vscanf \
vsnprintf \
obprintf \
fcloseall \
fseeko \
ftello \
freopen64 \
fseeko64 \
ftello64 \
__fbufsize \
__freading \
__fwriting \
__freadable \
__fwritable \
__flbf \
__fpurge \
__fpending \
__fsetlocking \
libc_fatal \
fmemopen \
oldfmemopen \
vtables \
clearerr_u \
feof_u \
ferror_u \
fputc_u \
getc_u \
getchar_u \
iofflush_u \
putc_u \
peekc \
iofread_u \
iofwrite_u \
iofgets_u \
iofputs_u \
fileops \
genops \
stdfiles \
stdio \
strops \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
oldiofopen \
oldiofdopen \
oldiofclose \
oldiopopen \
oldpclose \
oldtmpfile \
oldiofgetpos \
oldiofgetpos64 \
oldiofsetpos \
oldiofsetpos64 \
oldfileops \
oldstdfiles \
"
mkdir -p $build_dir/libio
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/libio/$fn.s -o $build_dir/libio/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/libio/$fn.shared.s -o $build_dir/libio/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'libio/filedoalloc.o libio/iofclose.o libio/iofdopen.o libio/iofflush.o libio/iofgetpos.o libio/iofgets.o libio/iofopen.o libio/iofopncook.o libio/iofputs.o libio/iofread.o libio/iofsetpos.o libio/ioftell.o libio/wfiledoalloc.o libio/iofwrite.o libio/iogetdelim.o libio/iogetline.o libio/iogets.o libio/iopadn.o libio/iopopen.o libio/ioputs.o libio/ioseekoff.o libio/ioseekpos.o libio/iosetbuffer.o libio/iosetvbuf.o libio/ioungetc.o libio/iovsprintf.o libio/iovsscanf.o libio/iofgetpos64.o libio/iofopen64.o libio/iofsetpos64.o libio/fputwc.o libio/fputwc_u.o libio/getwc.o libio/getwc_u.o libio/getwchar.o libio/getwchar_u.o libio/iofgetws.o libio/iofgetws_u.o libio/iofputws.o libio/iofputws_u.o libio/iogetwline.o libio/iowpadn.o libio/ioungetwc.o libio/putwc.o libio/putwc_u.o libio/putwchar.o libio/putwchar_u.o libio/putchar.o libio/putchar_u.o libio/fwprintf.o libio/swprintf.o libio/vwprintf.o libio/wprintf.o libio/wscanf.o libio/fwscanf.o libio/vwscanf.o libio/vswprintf.o libio/iovswscanf.o libio/swscanf.o libio/wgenops.o libio/wstrops.o libio/wfileops.o libio/iofwide.o libio/fwide.o libio/wmemstream.o libio/clearerr.o libio/feof.o libio/ferror.o libio/fileno.o libio/fputc.o libio/freopen.o libio/fseek.o libio/getc.o libio/getchar.o libio/memstream.o libio/pclose.o libio/putc.o libio/rewind.o libio/setbuf.o libio/setlinebuf.o libio/vasprintf.o libio/iovdprintf.o libio/vscanf.o libio/vsnprintf.o libio/obprintf.o libio/fcloseall.o libio/fseeko.o libio/ftello.o libio/freopen64.o libio/fseeko64.o libio/ftello64.o libio/__fbufsize.o libio/__freading.o libio/__fwriting.o libio/__freadable.o libio/__fwritable.o libio/__flbf.o libio/__fpurge.o libio/__fpending.o libio/__fsetlocking.o libio/libc_fatal.o libio/fmemopen.o libio/oldfmemopen.o libio/vtables.o libio/clearerr_u.o libio/feof_u.o libio/ferror_u.o libio/fputc_u.o libio/getc_u.o libio/getchar_u.o libio/iofflush_u.o libio/putc_u.o libio/peekc.o libio/iofread_u.o libio/iofwrite_u.o libio/iofgets_u.o libio/iofputs_u.o libio/fileops.o libio/genops.o libio/stdfiles.o libio/stdio.o libio/strops.o\n' >$build_dir/libio/stamp.o
printf 'libio/filedoalloc.os libio/iofclose.os libio/iofdopen.os libio/iofflush.os libio/iofgetpos.os libio/iofgets.os libio/iofopen.os libio/iofopncook.os libio/iofputs.os libio/iofread.os libio/iofsetpos.os libio/ioftell.os libio/wfiledoalloc.os libio/iofwrite.os libio/iogetdelim.os libio/iogetline.os libio/iogets.os libio/iopadn.os libio/iopopen.os libio/ioputs.os libio/ioseekoff.os libio/ioseekpos.os libio/iosetbuffer.os libio/iosetvbuf.os libio/ioungetc.os libio/iovsprintf.os libio/iovsscanf.os libio/iofgetpos64.os libio/iofopen64.os libio/iofsetpos64.os libio/fputwc.os libio/fputwc_u.os libio/getwc.os libio/getwc_u.os libio/getwchar.os libio/getwchar_u.os libio/iofgetws.os libio/iofgetws_u.os libio/iofputws.os libio/iofputws_u.os libio/iogetwline.os libio/iowpadn.os libio/ioungetwc.os libio/putwc.os libio/putwc_u.os libio/putwchar.os libio/putwchar_u.os libio/putchar.os libio/putchar_u.os libio/fwprintf.os libio/swprintf.os libio/vwprintf.os libio/wprintf.os libio/wscanf.os libio/fwscanf.os libio/vwscanf.os libio/vswprintf.os libio/iovswscanf.os libio/swscanf.os libio/wgenops.os libio/wstrops.os libio/wfileops.os libio/iofwide.os libio/fwide.os libio/wmemstream.os libio/clearerr.os libio/feof.os libio/ferror.os libio/fileno.os libio/fputc.os libio/freopen.os libio/fseek.os libio/getc.os libio/getchar.os libio/memstream.os libio/pclose.os libio/putc.os libio/rewind.os libio/setbuf.os libio/setlinebuf.os libio/vasprintf.os libio/iovdprintf.os libio/vscanf.os libio/vsnprintf.os libio/obprintf.os libio/fcloseall.os libio/fseeko.os libio/ftello.os libio/freopen64.os libio/fseeko64.os libio/ftello64.os libio/__fbufsize.os libio/__freading.os libio/__fwriting.os libio/__freadable.os libio/__fwritable.os libio/__flbf.os libio/__fpurge.os libio/__fpending.os libio/__fsetlocking.os libio/libc_fatal.os libio/fmemopen.os libio/oldfmemopen.os libio/vtables.os libio/oldiofopen.os libio/oldiofdopen.os libio/oldiofclose.os libio/oldiopopen.os libio/oldpclose.os libio/oldtmpfile.os libio/oldiofgetpos.os libio/oldiofgetpos64.os libio/oldiofsetpos.os libio/oldiofsetpos64.os libio/clearerr_u.os libio/feof_u.os libio/ferror_u.os libio/fputc_u.os libio/getc_u.os libio/getchar_u.os libio/iofflush_u.os libio/putc_u.os libio/peekc.os libio/iofread_u.os libio/iofwrite_u.os libio/iofgets_u.os libio/iofputs_u.os libio/fileops.os libio/genops.os libio/stdfiles.os libio/stdio.os libio/strops.os libio/oldfileops.os libio/oldstdfiles.os\n' >$build_dir/libio/stamp.os
printf '' >$build_dir/libio/stamp.oS
