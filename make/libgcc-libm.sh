printf "\
LIBGCC LIBM*********************************************************************\n"
mkdir -p $build_dir/libgcc/libm

paths="\
$src_dir/libgcc/libm/extenddftf2 \
$src_dir/libgcc/libm/fixtfsi \
$src_dir/libgcc/libm/floatunsitf \
$src_dir/libgcc/libm/muldc3 \
$src_dir/libgcc/libm/mulsc3 \
$src_dir/libgcc/libm/multc3 \
$src_dir/libgcc/libm/mulxc3 \
$src_dir/libgcc/libm/trunctfsf2 \
$src_dir/libgcc/libm/floatditf \
$src_dir/libgcc/libm/fixtfdi \
$src_dir/libgcc/libm/trunctfdf2 \
$src_dir/libgcc/libm/trunctfxf2 \
"
objs=
for p in $paths
do
	printf "ASSEMBLING $p.shared.s\n"
	obj=$(basename $p).o
	objs_path="$objs_path $build_dir/libgcc/libm/$obj"
	$as $p.shared.s -o $build_dir/libgcc/libm/$obj
done
$ar crv $build_dir/libgcc/libm/libgcc-libm.a $objs_path
