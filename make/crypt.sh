printf "\
CRYPT***************************************************************************\n"
mkdir -p $build_dir/crypt
fns_pie_shared="\
crypt-entry \
md5-crypt \
sha256-crypt \
sha512-crypt \
crypt \
crypt_util \
md5 \
sha256 \
sha512 \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/crypt/$fn.s -o $build_dir/crypt/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/crypt/$fn.shared.s -o $build_dir/crypt/$fn.os
done
printf 'CREATING LIBCRYPT.A\n'
cd $build_dir/crypt
$ar cruv libcrypt.a crypt-entry.o md5-crypt.o sha256-crypt.o sha512-crypt.o crypt.o crypt_util.o md5.o sha256.o sha512.o
printf 'CREATING LIBCRYPT_PIC.A\n'
$ar cruv libcrypt_pic.a crypt-entry.os md5-crypt.os sha256-crypt.os sha512-crypt.os crypt.os crypt_util.os md5.os sha256.os sha512.os
printf 'CREATING LIBCRYPT.SO.1\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libcrypt.versions.map \
	-soname=libcrypt.so.1 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/crypt \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/crypt:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/crypt/libcrypt.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/crypt/libcrypt_pic.a \
	--no-whole-archive \
	$build_dir/math/libm/libm.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
