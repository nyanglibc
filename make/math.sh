printf "\
MATH**************************************************************************\n"
fns="\
s_isinfl \
s_isnanl \
s_modfl \
s_frexpl \
s_signbitl \
s_isinf \
s_isnan \
s_finite \
s_ldexpl \
s_modf \
s_scalbn \
s_frexp \
s_ldexp \
s_isinff \
s_isnanf \
s_finitef \
s_modff \
s_scalbnf \
s_frexpf \
s_ldexpf \
s_isinff128 \
s_isnanf128 \
s_finitef128 \
s_copysignf128 \
s_modff128 \
s_scalbnf128 \
s_frexpf128 \
s_signbitf128 \
s_ldexpf128 \
setfpucw \
fpu_control \
\
s_finitel \
s_copysignl \
s_scalbnl \
s_copysign \
s_signbit \
s_copysignf \
s_signbitf \
"
mkdir -p $build_dir/math
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/math/$fn.s -o $build_dir/math/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/math/$fn.shared.s -o $build_dir/math/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'math/s_isinfl.o math/s_isnanl.o math/s_finitel.o math/s_copysignl.o math/s_modfl.o math/s_scalbnl.o math/s_frexpl.o math/s_signbitl.o math/s_ldexpl.o math/s_isinf.o math/s_isnan.o math/s_finite.o math/s_copysign.o math/s_modf.o math/s_scalbn.o math/s_frexp.o math/s_signbit.o math/s_ldexp.o math/s_isinff.o math/s_isnanf.o math/s_finitef.o math/s_copysignf.o math/s_modff.o math/s_scalbnf.o math/s_frexpf.o math/s_signbitf.o math/s_ldexpf.o math/s_isinff128.o math/s_isnanf128.o math/s_finitef128.o math/s_copysignf128.o math/s_modff128.o math/s_scalbnf128.o math/s_frexpf128.o math/s_signbitf128.o math/s_ldexpf128.o math/setfpucw.o math/fpu_control.o\n' >$build_dir/math/stamp.o
printf 'math/s_isinfl.os math/s_isnanl.os math/s_finitel.os math/s_copysignl.os math/s_modfl.os math/s_scalbnl.os math/s_frexpl.os math/s_signbitl.os math/s_ldexpl.os math/s_isinf.os math/s_isnan.os math/s_finite.os math/s_copysign.os math/s_modf.os math/s_scalbn.os math/s_frexp.os math/s_signbit.os math/s_ldexp.os math/s_isinff.os math/s_isnanf.os math/s_finitef.os math/s_copysignf.os math/s_modff.os math/s_scalbnf.os math/s_frexpf.os math/s_signbitf.os math/s_ldexpf.os math/s_isinff128.os math/s_isnanf128.os math/s_finitef128.os math/s_copysignf128.os math/s_modff128.os math/s_scalbnf128.os math/s_frexpf128.os math/s_signbitf128.os math/s_ldexpf128.os math/setfpucw.os math/fpu_control.os\n' >$build_dir/math/stamp.os
printf '' >$build_dir/math/stamp.oS
