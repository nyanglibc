printf "\
ICONV***************************************************************************\n"
gens="\
gconv_cache.s.in \
gconv_cache.shared.s.in \
"
mkdir -p $build_dir/iconv
for g in $gens
do
	sed -E -e "s@CONF_PREFIX@$conf_prefix@" \
			$src_dir/iconv/$g >$build_dir/iconv/$(basename $g .in)
done
gens="\
gconv_conf.s.in \
gconv_conf.shared.s.in \
"
conf_default_gconv_path_str="$conf_prefix/lib/gconv"
conf_default_gconv_path_str_bytes_n=$(printf '%s\n' "$conf_default_gconv_path_str\0" | wc -c)
mkdir -p $build_dir/iconv
for g in $gens
do
	sed -E -e "s@CONF_DEFAULT_GCONV_PATH_STR_BYTES_N@$conf_default_gconv_path_str_bytes_n@;s@CONF_DEFAULT_GCONV_PATH_STR@$conf_default_gconv_path_str@" \
			$src_dir/iconv/$g >$build_dir/iconv/$(basename $g .in)
done
paths="\
$build_dir/iconv/gconv_cache \
$build_dir/iconv/gconv_conf \
$src_dir/iconv/iconv_open \
$src_dir/iconv/iconv \
$src_dir/iconv/iconv_close \
$src_dir/iconv/gconv_open \
$src_dir/iconv/gconv \
$src_dir/iconv/gconv_close \
$src_dir/iconv/gconv_db \
$src_dir/iconv/gconv_builtin \
$src_dir/iconv/gconv_simple \
$src_dir/iconv/gconv_trans  \
$src_dir/iconv/gconv_dl \
$src_dir/iconv/gconv_charset \
"

mkdir -p $build_dir/iconv
for p in $paths
do
	printf "ASSEMBLING PIE $p\n"
	$as $p.s -o $build_dir/iconv/$(basename $p).o
	printf "ASSEMBLING SHARED $p\n"
	$as $p.shared.s -o $build_dir/iconv/$(basename $p).os
done
printf 'CREATING STAMPS FILES\n'
printf 'iconv/iconv_open.o iconv/iconv.o iconv/iconv_close.o iconv/gconv_open.o iconv/gconv.o iconv/gconv_close.o iconv/gconv_db.o iconv/gconv_conf.o iconv/gconv_builtin.o iconv/gconv_simple.o iconv/gconv_trans.o iconv/gconv_cache.o iconv/gconv_dl.o iconv/gconv_charset.o\n' >$build_dir/iconv/stamp.o
printf 'iconv/iconv_open.os iconv/iconv.os iconv/iconv_close.os iconv/gconv_open.os iconv/gconv.os iconv/gconv_close.os iconv/gconv_db.os iconv/gconv_conf.os iconv/gconv_builtin.os iconv/gconv_simple.os iconv/gconv_trans.os iconv/gconv_cache.os iconv/gconv_dl.os iconv/gconv_charset.os\n' >$build_dir/iconv/stamp.os
printf '' >$build_dir/iconv/stamp.oS
