printf "\
IO******************************************************************************\n"
fns="\
utime \
mkfifo \
mkfifoat \
stat \
fstat \
lstat \
stat64 \
fstat64 \
lstat64 \
fstatat \
fstatat64 \
statx \
mknod \
mknodat \
statfs64 \
fstatfs64 \
statvfs \
fstatvfs \
statvfs64 \
fstatvfs64 \
lchmod \
fchmodat \
open \
open_2 \
open64 \
open64_2 \
openat \
openat_2 \
openat64 \
openat64_2 \
read \
write \
lseek \
lseek64 \
access \
euidaccess \
faccessat \
fcntl \
fcntl64 \
lockf \
lockf64 \
close \
creat \
creat64 \
getcwd \
getwd \
getdirname \
ttyname \
ttyname_r \
isatty \
ftw \
ftw64 \
fts \
fts64 \
poll \
ppoll \
posix_fadvise \
posix_fadvise64 \
posix_fallocate \
posix_fallocate64 \
sendfile64 \
copy_file_range \
utimensat \
futimens \
file_change_detection \
xstatconv \
internal_statvfs \
internal_statvfs64 \
sync_file_range \
fallocate \
fallocate64 \
close_nocancel \
fcntl_nocancel \
open_nocancel \
open64_nocancel \
openat_nocancel \
openat64_nocancel \
read_nocancel \
pread64_nocancel \
write_nocancel \
statx_cp \
stat_t64_cp \
\
statfs \
fstatfs \
umask \
chmod \
fchmod \
mkdir \
mkdirat \
flock \
dup \
dup2 \
dup3 \
pipe \
pipe2 \
chdir \
fchdir \
chown \
fchown \
lchown \
fchownat \
link \
linkat \
symlink \
symlinkat \
readlink \
readlinkat \
unlink \
unlinkat \
rmdir \
sendfile \
"
mkdir -p $build_dir/io
for fn in $fns
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/io/$fn.s -o $build_dir/io/$fn.o
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/io/$fn.shared.s -o $build_dir/io/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'io/utime.o io/mkfifo.o io/mkfifoat.o io/stat.o io/fstat.o io/lstat.o io/stat64.o io/fstat64.o io/lstat64.o io/fstatat.o io/fstatat64.o io/statx.o io/mknod.o io/mknodat.o io/statfs.o io/fstatfs.o io/statfs64.o io/fstatfs64.o io/statvfs.o io/fstatvfs.o io/statvfs64.o io/fstatvfs64.o io/umask.o io/chmod.o io/fchmod.o io/lchmod.o io/fchmodat.o io/mkdir.o io/mkdirat.o io/open.o io/open_2.o io/open64.o io/open64_2.o io/openat.o io/openat_2.o io/openat64.o io/openat64_2.o io/read.o io/write.o io/lseek.o io/lseek64.o io/access.o io/euidaccess.o io/faccessat.o io/fcntl.o io/fcntl64.o io/flock.o io/lockf.o io/lockf64.o io/close.o io/dup.o io/dup2.o io/dup3.o io/pipe.o io/pipe2.o io/creat.o io/creat64.o io/chdir.o io/fchdir.o io/getcwd.o io/getwd.o io/getdirname.o io/chown.o io/fchown.o io/lchown.o io/fchownat.o io/ttyname.o io/ttyname_r.o io/isatty.o io/link.o io/linkat.o io/symlink.o io/symlinkat.o io/readlink.o io/readlinkat.o io/unlink.o io/unlinkat.o io/rmdir.o io/ftw.o io/ftw64.o io/fts.o io/fts64.o io/poll.o io/ppoll.o io/posix_fadvise.o io/posix_fadvise64.o io/posix_fallocate.o io/posix_fallocate64.o io/sendfile.o io/sendfile64.o io/copy_file_range.o io/utimensat.o io/futimens.o io/file_change_detection.o io/xstatconv.o io/internal_statvfs.o io/internal_statvfs64.o io/sync_file_range.o io/fallocate.o io/fallocate64.o io/close_nocancel.o io/fcntl_nocancel.o io/open_nocancel.o io/open64_nocancel.o io/openat_nocancel.o io/openat64_nocancel.o io/read_nocancel.o io/pread64_nocancel.o io/write_nocancel.o io/statx_cp.o io/stat_t64_cp.o\n' >$build_dir/io/stamp.o
printf 'io/utime.os io/mkfifo.os io/mkfifoat.os io/stat.os io/fstat.os io/lstat.os io/stat64.os io/fstat64.os io/lstat64.os io/fstatat.os io/fstatat64.os io/statx.os io/mknod.os io/mknodat.os io/statfs.os io/fstatfs.os io/statfs64.os io/fstatfs64.os io/statvfs.os io/fstatvfs.os io/statvfs64.os io/fstatvfs64.os io/umask.os io/chmod.os io/fchmod.os io/lchmod.os io/fchmodat.os io/mkdir.os io/mkdirat.os io/open.os io/open_2.os io/open64.os io/open64_2.os io/openat.os io/openat_2.os io/openat64.os io/openat64_2.os io/read.os io/write.os io/lseek.os io/lseek64.os io/access.os io/euidaccess.os io/faccessat.os io/fcntl.os io/fcntl64.os io/flock.os io/lockf.os io/lockf64.os io/close.os io/dup.os io/dup2.os io/dup3.os io/pipe.os io/pipe2.os io/creat.os io/creat64.os io/chdir.os io/fchdir.os io/getcwd.os io/getwd.os io/getdirname.os io/chown.os io/fchown.os io/lchown.os io/fchownat.os io/ttyname.os io/ttyname_r.os io/isatty.os io/link.os io/linkat.os io/symlink.os io/symlinkat.os io/readlink.os io/readlinkat.os io/unlink.os io/unlinkat.os io/rmdir.os io/ftw.os io/ftw64.os io/fts.os io/fts64.os io/poll.os io/ppoll.os io/posix_fadvise.os io/posix_fadvise64.os io/posix_fallocate.os io/posix_fallocate64.os io/sendfile.os io/sendfile64.os io/copy_file_range.os io/utimensat.os io/futimens.os io/file_change_detection.os io/xstatconv.os io/internal_statvfs.os io/internal_statvfs64.os io/sync_file_range.os io/fallocate.os io/fallocate64.os io/close_nocancel.os io/fcntl_nocancel.os io/open_nocancel.os io/open64_nocancel.os io/openat_nocancel.os io/openat64_nocancel.os io/read_nocancel.os io/pread64_nocancel.os io/write_nocancel.os io/statx_cp.os io/stat_t64_cp.os\n' >$build_dir/io/stamp.os
printf '' >$build_dir/io/stamp.oS
