printf "\
ICONVDATA***********************************************************************\n"
mkdir -p $build_dir/iconvdata
#-------------------------------------------------------------------------------
# libKSC.so helper
printf 'BUILDING LIBKSC HELPER MODULE\n'
$as $src_dir/iconvdata/ksc5601.shared.s -o $build_dir/iconvdata/ksc5601.os
$ld \
	-s \
	-shared \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-z defs \
	-L$build_dir/csu/ \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-soname libKSC.so \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/iconvdata/libKSC.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/iconvdata/ksc5601.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
#-------------------------------------------------------------------------------
# libJIS.so helper
printf 'BUILDING LIBJIS HELPER MODULE\n'
$as $src_dir/iconvdata/jis0201.shared.s -o $build_dir/iconvdata/jis0201.os
$as $src_dir/iconvdata/jis0208.shared.s -o $build_dir/iconvdata/jis0208.os
$as $src_dir/iconvdata/jis0212.shared.s -o $build_dir/iconvdata/jis0212.os
$ld \
	-s \
	-shared \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-z defs \
	-L$build_dir/csu/ \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-soname libJIS.so \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/iconvdata/libJIS.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/iconvdata/jis0201.os \
		$build_dir/iconvdata/jis0208.os \
		$build_dir/iconvdata/jis0212.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
#-------------------------------------------------------------------------------
# libGB.so helper
printf 'BUILDING LIBGB HELPER MODULE\n'
$as $src_dir/iconvdata/gb2312.shared.s -o $build_dir/iconvdata/gb2312.os
$ld \
	-s \
	-shared \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-z defs \
	-L$build_dir/csu/ \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-soname libGB.so \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/iconvdata/libGB.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/iconvdata/gb2312.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
#-------------------------------------------------------------------------------
# libCNS.so helper
printf 'BUILDING LIBCNS HELPER MODULE\n'
$as $src_dir/iconvdata/cns11643l1.shared.s -o $build_dir/iconvdata/cns11643l1.os
$as $src_dir/iconvdata/cns11643.shared.s -o $build_dir/iconvdata/cns11643.os
$ld \
	-s \
	-shared \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-z defs \
	-L$build_dir/csu/ \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-soname libCNS.so \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/iconvdata/libCNS.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/iconvdata/cns11643l1.os \
		$build_dir/iconvdata/cns11643.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
#-------------------------------------------------------------------------------
# libISOIR165.so helper
printf 'BUILDING LIBISOIR165 HELPER MODULE\n'
$as $src_dir/iconvdata/iso-ir-165.shared.s -o $build_dir/iconvdata/iso-ir-165.os
$ld \
	-s \
	-shared \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-z defs \
	-L$build_dir/csu/ \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-soname libISOIR165.so \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/iconvdata/libISOIR165.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/iconvdata/iso-ir-165.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
#-------------------------------------------------------------------------------
# libJISX0213.so helper
printf 'BUILDING LIBJISX0213 HELPER MODULE\n'
$as $src_dir/iconvdata/jisx0213.shared.s -o $build_dir/iconvdata/jisx0213.os
$ld \
	-s \
	-shared \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-z defs \
	-L$build_dir/csu/ \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-soname libJISX0213.so \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/iconvdata/libJISX0213.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--as-needed \
		$build_dir/iconvdata/jisx0213.os \
	--no-as-needed \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
fns_shared="\
iso8859-1 \
iso8859-2 \
iso8859-3 \
iso8859-4 \
iso8859-5 \
iso8859-6 \
iso8859-7 \
iso8859-8 \
iso8859-9 \
iso8859-10 \
iso8859-11 \
iso8859-13 \
iso8859-14 \
iso8859-15 \
iso8859-16 \
t.61 \
iso_6937 \
sjis \
koi-8 \
hp-roman8 \
hp-roman9 \
ebcdic-at-de \
ebcdic-at-de-a \
ebcdic-ca-fr \
euc-kr \
uhc \
johab \
big5 \
euc-jp \
euc-cn \
euc-tw \
iso646 \
ebcdic-dk-no \
ebcdic-dk-no-a \
ebcdic-es \
ebcdic-es-a \
ebcdic-es-s \
ebcdic-fi-se \
ebcdic-fi-se-a \
ebcdic-fr \
ebcdic-is-friss \
ebcdic-it \
ebcdic-pt \
ebcdic-uk \
ebcdic-us \
ibm037 \
ibm038 \
ibm274 \
ibm275 \
ibm423 \
ibm500 \
ibm870 \
ibm871 \
ibm891 \
ibm903 \
ibm904 \
ibm905 \
ibm1047 \
ibm874 \
cp737 \
cp775 \
iso-2022-kr \
hp-turkish8 \
hp-thai8 \
hp-greek8 \
koi8-r \
latin-greek \
latin-greek-1 \
ibm256 \
ibm273 \
ibm277 \
ibm278 \
ibm280 \
ibm281 \
ibm284 \
ibm285 \
ibm290 \
ibm297 \
ibm420 \
ibm424 \
ibm437 \
ibm850 \
ibm851 \
ibm852 \
ibm855 \
ibm857 \
ibm858 \
ibm860 \
ibm861 \
ibm862 \
ibm863 \
ibm864 \
ibm865 \
ibm868 \
ibm869 \
ibm875 \
ibm880 \
ibm866 \
cp1258 \
ibm922 \
ibm1124 \
ibm1129 \
ibm932 \
ibm943 \
ibm856 \
ibm930 \
ibm933 \
ibm935 \
ibm937 \
ibm939 \
ibm1046 \
ibm1132 \
ibm1133 \
ibm1160 \
ibm1161 \
ibm1162 \
ibm1163 \
ibm1164 \
ibm918 \
ibm1004 \
ibm1026 \
cp1125 \
cp1250 \
cp1251 \
cp1252 \
cp1253 \
cp1254 \
cp1255 \
cp1256 \
cp1257 \
iso-2022-jp \
macintosh \
iec_p27-1 \
asmo_449 \
ansi_x3.110 \
csn_369103 \
cwi \
dec-mcs \
ecma-cyrillic \
gost_19768-74 \
greek-ccitt \
greek7 \
greek7-old \
inis \
inis-8 \
inis-cyrillic \
iso_6937-2 \
iso_2033 \
iso_5427 \
iso_5427-ext \
iso_5428 \
iso_10367-box \
mac-is \
mac-uk \
nats-dano \
nats-sefi \
sami-ws2 \
iso-ir-197 \
tis-620 \
koi8-u \
gbk \
isiri-3342 \
gbgbk \
iso-2022-cn \
utf-16 \
unicode \
utf-32 \
utf-7 \
big5hkscs \
gb18030 \
iso-2022-cn-ext \
viscii \
gbbig5 \
cp10007 \
koi8-t \
georgian-ps \
georgian-academy \
iso-ir-209 \
mac-sami \
armscii-8 \
tcvn5712-1 \
euc-jisx0213 \
shift_jisx0213 \
iso-2022-jp-3 \
tscii \
ibm866nav \
cp932 \
euc-jp-ms \
pt154 \
rk1048 \
ibm1025 \
ibm1122 \
ibm1137 \
ibm1153 \
ibm1154 \
ibm1155 \
ibm1156 \
ibm1157 \
ibm1158 \
ibm803 \
ibm901 \
ibm902 \
ibm921 \
ibm1008 \
ibm1008_420 \
ibm1097 \
ibm1112 \
ibm1123 \
ibm1130 \
ibm1140 \
ibm1141 \
ibm1142 \
ibm1143 \
ibm1144 \
ibm1145 \
ibm1146 \
ibm1147 \
ibm1148 \
ibm1149 \
ibm1166 \
ibm1167 \
ibm4517 \
ibm4899 \
ibm4909 \
ibm4971 \
ibm5347 \
ibm9030 \
ibm9066 \
ibm9448 \
ibm12712 \
ibm16804 \
ibm1364 \
ibm1371 \
ibm1388 \
ibm1390 \
ibm1399 \
iso_11548-1 \
mik \
brf \
mac-centraleurope \
koi8-ru \
iso8859-9e \
cp770 \
cp771 \
cp772 \
cp773 \
cp774 \
"
for fn in $fns_shared
do
	printf "BUILDING $fn ICONV MODULE\n"
	helper_modules=
	rpath_origin=
	if test "$fn" = "euc-kr" -o "$fn" = "uhc" -o "$fn" = "johab" \
		-o "$fn" = "iso-2022-kr"; then
		helper_modules="$build_dir/iconvdata/libKSC.so"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "euc-jp" -o "$fn" = "euc-jp-ms"; then
		helper_modules="$build_dir/iconvdata/libJIS.so"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "euc-cn"; then
		helper_modules="$build_dir/iconvdata/libGB.so"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "euc-tw"; then
		helper_modules="$build_dir/iconvdata/libCNS.so"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "iso-2022-jp"; then
		helper_modules="\
			$build_dir/iconvdata/libJIS.so \
			$build_dir/iconvdata/libGB.so \
			$build_dir/iconvdata/libCNS.so \
			$build_dir/iconvdata/libKSC.so \
		"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "iso-2022-cn"; then
		helper_modules="\
			$build_dir/iconvdata/libGB.so \
			$build_dir/iconvdata/libCNS.so \
		"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "iso-2022-cn-ext"; then
		helper_modules="\
			$build_dir/iconvdata/libGB.so \
			$build_dir/iconvdata/libCNS.so \
			$build_dir/iconvdata/libISOIR165.so \
		"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "euc-jisx0213" -o "$fn" = "shift_jisx0213"; then
		helper_modules="\
			$build_dir/iconvdata/libJISX0213.so \
		"
		rpath_origin='-rpath \$ORIGIN'
	fi
	if test "$fn" = "iso-2022-jp-3"; then
		helper_modules="\
			$build_dir/iconvdata/libJIS.so \
			$build_dir/iconvdata/libJISX0213.so \
		"
		rpath_origin='-rpath \$ORIGIN'
	fi
	$as $src_dir/iconvdata/$fn.shared.s -o $build_dir/iconvdata/$fn.os
	# keep the upper case
	module_name=$(printf "$fn" | tr '[a-z]' '[A-Z]').so
        $ld \
		-s \
		-shared \
		--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
		-z defs \
		-L$build_dir/csu/  \
		--version-script=$src_dir/gconv.map \
		-z combreloc \
		-z relro \
		--hash-style=both \
		$rpath_origin \
		-L$build_dir \
		-L$build_dir/math \
		-L$build_dir/elf \
		-L$build_dir/dlfcn \
		-L$build_dir/nss \
		-L$build_dir/nis \
		-L$build_dir/rt \
		-L$build_dir/resolv \
		-L$build_dir/mathvec \
		-L$build_dir/support \
		-L$build_dir/crypt \
		-L$build_dir/nptl \
		-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
		-o "$build_dir/iconvdata/$module_name" \
		-T $src_dir/shlib.lds \
		$build_dir/csu/abi-note.o \
		--as-needed \
			$build_dir/iconvdata/$fn.os \
			$helper_modules \
		--no-as-needed \
		--start-group \
			$build_dir/libc.so \
			$build_dir/libc_nonshared.a \
			--as-needed \
				$build_dir/elf/ld-linux-x86-64.so.2 \
			--no-as-needed \
		--end-group
done
