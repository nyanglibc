printf "\
LIBMEMUSAGE*********************************************************************\n"
mkdir -p $build_dir/malloc/libmemusage
$as $src_dir/malloc/libmemusage/mcheck-init.s \
	-o $build_dir/malloc/libmemusage/mcheck-init.o
printf 'CREATING LIBMEMUSAGE.A\n'
cp $build_dir/malloc/libmemusage/mcheck-init.o \
	$build_dir/malloc/libmemusage/libmemusage.a
#-------------------------------------------------------------------------------
$as $src_dir/malloc/libmemusage/memusage.s \
	-o $build_dir/malloc/libmemusage/memusage.os
printf 'CREATING LIBMEMUSAGE.SO\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	-soname=libmemusage.so \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/malloc/libmemusage/libmemusage.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/malloc/libmemusage/memusage.os \
	--no-whole-archive \
	$build_dir/dlfcn/libdl/libdl.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
