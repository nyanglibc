printf "\
NSS LIBNSS_DB*******************************************************************\n"
mkdir -p $build_dir/nss/libnss_db
fns_shared="\
db-proto \
db-service \
db-grp \
db-pwd \
db-ethers \
db-spwd \
db-netgrp \
db-sgrp \
db-rpc \
db-initgroups \
db-open \
db-init \
hash-string \
"
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/nss/libnss_db/$fn.shared.s -o $build_dir/nss/libnss_db/$fn.os
done
cd $build_dir/nss/libnss_db
printf 'CREATING LIBNSS_DB_PIC.A\n'
$ar cruv libnss_db_pic.a db-proto.os db-service.os db-grp.os db-pwd.os db-ethers.os db-spwd.os db-netgrp.os db-sgrp.os db-rpc.os db-initgroups.os db-open.os db-init.os hash-string.os
printf 'CREATING LIBNSS_DB.SO.2\n'
$ld \
	-s \
	-shared \
	-O1 \
	-z defs \
	--dynamic-linker=/lib64/ld-linux-x86-64.so.2 \
	-L$build_dir/csu/ \
	--version-script=$src_dir/libnss_db.versions.map \
	-soname=libnss_db.so.2 \
	-z combreloc \
	-z relro \
	--hash-style=both \
	-L$build_dir \
	-L$build_dir/math \
	-L$build_dir/elf \
	-L$build_dir/dlfcn \
	-L$build_dir/nss \
	-L$build_dir/nis \
	-L$build_dir/rt \
	-L$build_dir/resolv \
	-L$build_dir/mathvec \
	-L$build_dir/support \
	-L$build_dir/crypt \
	-L$build_dir/nptl \
	-rpath-link=$build_dir:$build_dir/math:$build_dir/elf:$build_dir/dlfcn:$build_dir/nss:$build_dir/nis:$build_dir/rt:$build_dir/resolv:$build_dir/mathvec:$build_dir/support:$build_dir/crypt:$build_dir/nptl \
	-o $build_dir/nss/libnss_db/libnss_db.so \
	-T $src_dir/shlib.lds \
	$build_dir/csu/abi-note.o \
	--whole-archive \
		$build_dir/nss/libnss_db/libnss_db_pic.a \
	--no-whole-archive \
	$build_dir/nss/libnss_files/libnss_files.so \
	--start-group \
		$build_dir/libc.so \
		$build_dir/libc_nonshared.a \
		--as-needed \
			$build_dir/elf/ld-linux-x86-64.so.2 \
		--no-as-needed \
	--end-group
