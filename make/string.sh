printf "\
STRING**************************************************************************\n"
fns_pie_shared="\
strcoll \
strverscmp \
strdup \
strndup \
strerror \
_strerror \
strncat \
strncpy \
strsignal \
strstr \
strtok \
strtok_r \
strxfrm \
bcopy \
ffs \
ffsll \
stpncpy \
memccpy \
wordcopy \
strsep \
strcasestr \
swab \
strfry \
memfrob \
memmem \
argz-append \
argz-count \
argz-create \
argz-ctsep \
argz-next \
argz-delete \
argz-extract \
argz-insert \
argz-stringify \
argz-addsep \
argz-replace \
envz \
basename \
strcoll_l \
strxfrm_l \
string-inlines \
xpg-strerror \
strerror_l \
explicit_bzero \
sigdescr_np \
sigabbrev_np \
strerrorname_np \
strerrordesc_np \
strcasecmp_l-nonascii \
strncase_l-nonascii \
cacheinfo \
tls-internal \
\
strcat \
strchr \
strcmp \
strcpy \
strcspn \
strlen \
strnlen \
strncmp \
strrchr \
strpbrk \
strspn \
memchr \
memcmp \
memmove \
memset \
mempcpy \
bzero \
stpcpy \
strcasecmp \
strncase \
strcasecmp_l \
strncase_l \
memcpy \
rawmemchr \
strchrnul \
memrchr \
"
fns_pie="\
$fns_pie_shared \
"
fns_shared="\
$fns_pie_shared \
"
mkdir -p $build_dir/string
for fn in $fns_pie
do
	printf "ASSEMBLING PIE $fn\n"
	$as $src_dir/string/$fn.s -o $build_dir/string/$fn.o
done
for fn in $fns_shared
do
	printf "ASSEMBLING SHARED $fn\n"
	$as $src_dir/string/$fn.shared.s -o $build_dir/string/$fn.os
done
printf 'CREATING STAMPS FILES\n'
printf 'string/strcat.o string/strchr.o string/strcmp.o string/strcoll.o string/strcpy.o string/strcspn.o string/strverscmp.o string/strdup.o string/strndup.o string/strerror.o string/_strerror.o string/strlen.o string/strnlen.o string/strncat.o string/strncmp.o string/strncpy.o string/strrchr.o string/strpbrk.o string/strsignal.o string/strspn.o string/strstr.o string/strtok.o string/strtok_r.o string/strxfrm.o string/memchr.o string/memcmp.o string/memmove.o string/memset.o string/mempcpy.o string/bcopy.o string/bzero.o string/ffs.o string/ffsll.o string/stpcpy.o string/stpncpy.o string/strcasecmp.o string/strncase.o string/strcasecmp_l.o string/strncase_l.o string/memccpy.o string/memcpy.o string/wordcopy.o string/strsep.o string/strcasestr.o string/swab.o string/strfry.o string/memfrob.o string/memmem.o string/rawmemchr.o string/strchrnul.o string/argz-append.o string/argz-count.o string/argz-create.o string/argz-ctsep.o string/argz-next.o string/argz-delete.o string/argz-extract.o string/argz-insert.o string/argz-stringify.o string/argz-addsep.o string/argz-replace.o string/envz.o string/basename.o string/strcoll_l.o string/strxfrm_l.o string/string-inlines.o string/memrchr.o string/xpg-strerror.o string/strerror_l.o string/explicit_bzero.o string/sigdescr_np.o string/sigabbrev_np.o string/strerrorname_np.o string/strerrordesc_np.o string/strcasecmp_l-nonascii.o string/strncase_l-nonascii.o string/cacheinfo.o string/tls-internal.o\n' >$build_dir/string/stamp.o
printf 'string/strcat.os string/strchr.os string/strcmp.os string/strcoll.os string/strcpy.os string/strcspn.os string/strverscmp.os string/strdup.os string/strndup.os string/strerror.os string/_strerror.os string/strlen.os string/strnlen.os string/strncat.os string/strncmp.os string/strncpy.os string/strrchr.os string/strpbrk.os string/strsignal.os string/strspn.os string/strstr.os string/strtok.os string/strtok_r.os string/strxfrm.os string/memchr.os string/memcmp.os string/memmove.os string/memset.os string/mempcpy.os string/bcopy.os string/bzero.os string/ffs.os string/ffsll.os string/stpcpy.os string/stpncpy.os string/strcasecmp.os string/strncase.os string/strcasecmp_l.os string/strncase_l.os string/memccpy.os string/memcpy.os string/wordcopy.os string/strsep.os string/strcasestr.os string/swab.os string/strfry.os string/memfrob.os string/memmem.os string/rawmemchr.os string/strchrnul.os string/argz-append.os string/argz-count.os string/argz-create.os string/argz-ctsep.os string/argz-next.os string/argz-delete.os string/argz-extract.os string/argz-insert.os string/argz-stringify.os string/argz-addsep.os string/argz-replace.os string/envz.os string/basename.os string/strcoll_l.os string/strxfrm_l.os string/string-inlines.os string/memrchr.os string/xpg-strerror.os string/strerror_l.os string/explicit_bzero.os string/sigdescr_np.os string/sigabbrev_np.os string/strerrorname_np.os string/strerrordesc_np.os string/strcasecmp_l-nonascii.os string/strncase_l-nonascii.os string/cacheinfo.os string/tls-internal.os\n' >$build_dir/string/stamp.os
printf '' >$build_dir/string/stamp.oS
