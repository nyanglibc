	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getutxline
	.type	getutxline, @function
getutxline:
	jmp	__GI___getutline
	.size	getutxline, .-getutxline
