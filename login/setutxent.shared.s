	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	setutxent
	.type	setutxent, @function
setutxent:
	jmp	__setutent
	.size	setutxent, .-setutxent
	.hidden	__setutent
