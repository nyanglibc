	.text
	.p2align 4,,15
	.globl	__getlogin_r_chk
	.type	__getlogin_r_chk, @function
__getlogin_r_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	getlogin_r
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__getlogin_r_chk, .-__getlogin_r_chk
	.hidden	__chk_fail
	.hidden	getlogin_r
