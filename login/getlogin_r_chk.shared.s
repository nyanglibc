	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__getlogin_r_chk
	.type	__getlogin_r_chk, @function
__getlogin_r_chk:
	cmpq	%rdx, %rsi
	ja	.L7
	jmp	__GI_getlogin_r
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__getlogin_r_chk, .-__getlogin_r_chk
