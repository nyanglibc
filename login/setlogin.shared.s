	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section .gnu.glibc-stub.setlogin
	.previous
	.section .gnu.warning.setlogin
	.previous
#NO_APP
	.p2align 4,,15
	.globl	setlogin
	.type	setlogin, @function
setlogin:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$38, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	setlogin, .-setlogin
	.section	.gnu.warning.setlogin
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_setlogin, @object
	.size	__evoke_link_warning_setlogin, 49
__evoke_link_warning_setlogin:
	.string	"setlogin is not implemented and will always fail"
