	.text
	.p2align 4,,15
	.globl	unlockpt
	.type	unlockpt, @function
unlockpt:
	subq	$24, %rsp
	xorl	%eax, %eax
	movl	$1074025521, %esi
	leaq	12(%rsp), %rdx
	movl	$0, 12(%rsp)
	call	__ioctl
	testl	%eax, %eax
	je	.L1
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$25, %fs:(%rdx)
	je	.L8
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$22, %fs:(%rdx)
	addq	$24, %rsp
	ret
	.size	unlockpt, .-unlockpt
	.hidden	__ioctl
