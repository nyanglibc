	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/ptmx"
	.text
	.p2align 4,,15
	.globl	__posix_openpt
	.type	__posix_openpt, @function
__posix_openpt:
	movl	%edi, %esi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	jmp	__open
	.size	__posix_openpt, .-__posix_openpt
	.weak	posix_openpt
	.set	posix_openpt,__posix_openpt
	.p2align 4,,15
	.globl	__getpt
	.type	__getpt, @function
__getpt:
	leaq	.LC0(%rip), %rdi
	movl	$2, %esi
	xorl	%eax, %eax
	jmp	__open
	.size	__getpt, .-__getpt
	.weak	getpt
	.set	getpt,__getpt
	.hidden	__open
