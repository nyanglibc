	.text
	.p2align 4,,15
	.type	timeout_handler, @function
timeout_handler:
	rep ret
	.size	timeout_handler, .-timeout_handler
	.p2align 4,,15
	.type	try_file_lock, @function
try_file_lock:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movl	%esi, %ebp
	pushq	%rbx
	movl	%edi, %ebx
	xorl	%edi, %edi
	subq	$352, %rsp
	leaq	32(%rsp), %r12
	call	alarm
	leaq	192(%rsp), %rsi
	movl	%eax, %r13d
	leaq	timeout_handler(%rip), %rax
	movq	%r12, %rdx
	movl	$14, %edi
	movq	$0, 200(%rsp)
	movq	%rax, 192(%rsp)
	movl	$0, 328(%rsp)
	call	__sigaction
	movl	$10, %edi
	call	alarm
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movq	%rsp, %rdx
	movl	$7, %esi
	movl	%ebx, %edi
	movw	%ax, 30(%rsp)
	xorl	%eax, %eax
	movw	%bp, (%rsp)
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	call	__fcntl64_nocancel
	xorl	%edi, %edi
	shrl	$31, %eax
	movq	__libc_errno@gottpoff(%rip), %rbp
	movl	%eax, %ebx
	movl	%fs:0(%rbp), %r14d
	call	alarm
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$14, %edi
	call	__sigaction
	testl	%r13d, %r13d
	je	.L4
	movl	%r13d, %edi
	call	alarm
.L4:
	movl	%r14d, %fs:0(%rbp)
	addq	$352, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	try_file_lock, .-try_file_lock
	.p2align 4,,15
	.type	read_last_entry, @function
read_last_entry:
	pushq	%rbx
	movl	$384, %edx
	subq	$384, %rsp
	movq	file_offset(%rip), %rcx
	movl	file_fd(%rip), %edi
	movq	%rsp, %rbx
	movq	%rbx, %rsi
	call	__pread64_nocancel
	testq	%rax, %rax
	js	.L11
	movq	%rax, %rdx
	xorl	%eax, %eax
	cmpq	$384, %rdx
	jne	.L9
	leaq	last_entry(%rip), %rdi
	movl	$48, %ecx
	movq	%rbx, %rsi
	rep movsq
	addq	$384, file_offset(%rip)
	movl	$1, %eax
.L9:
	addq	$384, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	$-1, %rax
	jmp	.L9
	.size	read_last_entry, .-read_last_entry
	.p2align 4,,15
	.type	matches_last_entry, @function
matches_last_entry:
	xorl	%eax, %eax
	cmpq	$0, file_offset(%rip)
	jle	.L23
	movzwl	(%rdi), %edx
	movzwl	last_entry(%rip), %ecx
	leal	-1(%rdx), %esi
	cmpw	$3, %si
	ja	.L16
	cmpw	%cx, %dx
	sete	%al
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	subl	$5, %ecx
	cmpw	$3, %cx
	ja	.L23
	subl	$5, %edx
	cmpw	$3, %dx
	ja	.L23
	subq	$8, %rsp
	cmpb	$0, 40+last_entry(%rip)
	je	.L18
	cmpb	$0, 40(%rdi)
	je	.L18
	leaq	40(%rdi), %rsi
	leaq	40+last_entry(%rip), %rdi
	movl	$4, %edx
	call	strncmp
	testl	%eax, %eax
	sete	%al
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L23:
	rep ret
	.p2align 4,,10
	.p2align 3
.L18:
	leaq	8(%rdi), %rsi
	leaq	8+last_entry(%rip), %rdi
	movl	$32, %edx
	call	strncmp
	testl	%eax, %eax
	sete	%al
.L14:
	addq	$8, %rsp
	ret
	.size	matches_last_entry, .-matches_last_entry
	.p2align 4,,15
	.type	internal_getut_nolock, @function
internal_getut_nolock:
	pushq	%rbx
	movq	%rdi, %rbx
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L36:
	je	.L34
	movq	%rbx, %rdi
	call	matches_last_entry
	testb	%al, %al
	jne	.L35
.L29:
	call	read_last_entry
	testq	%rax, %rax
	jns	.L36
	movl	$-1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L34:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
	movl	$-1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	internal_getut_nolock, .-internal_getut_nolock
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/log/wtmpx"
.LC1:
	.string	"/var/run/utmpx"
.LC2:
	.string	"/var/run/utmp"
.LC3:
	.string	"/var/log/wtmp"
	.text
	.p2align 4,,15
	.globl	__libc_setutent
	.hidden	__libc_setutent
	.type	__libc_setutent, @function
__libc_setutent:
	subq	$8, %rsp
	movl	file_fd(%rip), %edi
	testl	%edi, %edi
	jns	.L38
	movq	__libc_utmp_file_name(%rip), %rdx
	leaq	.LC2(%rip), %rdi
	movl	$14, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	je	.L54
.L39:
	leaq	.LC3(%rip), %rdi
	movl	$14, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	je	.L55
.L41:
	leaq	.LC1(%rip), %rax
	movl	$15, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	je	.L56
.L42:
	leaq	.LC0(%rip), %rax
	movl	$15, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	je	.L57
.L40:
	movq	%rdx, %rdi
	xorl	%eax, %eax
	movl	$524288, %esi
	movb	$0, file_writable(%rip)
	call	__open_nocancel
	movl	%eax, %edi
	movl	%eax, file_fd(%rip)
	xorl	%eax, %eax
	cmpl	$-1, %edi
	je	.L37
.L38:
	xorl	%edx, %edx
	xorl	%esi, %esi
	call	__lseek64
	movq	$0, file_offset(%rip)
	movl	$1, %eax
.L37:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L54:
	leaq	.LC1(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	je	.L40
	movq	__libc_utmp_file_name(%rip), %rdx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L55:
	leaq	.LC0(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	leaq	.LC0(%rip), %rdx
	je	.L40
	movq	__libc_utmp_file_name(%rip), %rdx
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L56:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__access
	testl	%eax, %eax
	leaq	.LC2(%rip), %rdx
	jne	.L40
	movq	__libc_utmp_file_name(%rip), %rdx
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L57:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__access
	leaq	.LC3(%rip), %rdx
	testl	%eax, %eax
	cmove	__libc_utmp_file_name(%rip), %rdx
	jmp	.L40
	.size	__libc_setutent, .-__libc_setutent
	.p2align 4,,15
	.globl	__libc_getutent_r
	.hidden	__libc_getutent_r
	.type	__libc_getutent_r, @function
__libc_getutent_r:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$32, %rsp
	movl	file_fd(%rip), %edi
	movq	__libc_errno@gottpoff(%rip), %r12
	testl	%edi, %edi
	movl	%fs:(%r12), %r13d
	js	.L70
.L59:
	xorl	%esi, %esi
	call	try_file_lock
	testb	%al, %al
	jne	.L71
	call	read_last_entry
	pxor	%xmm0, %xmm0
	movl	file_fd(%rip), %edi
	movq	%rax, %r14
	movl	$2, %edx
	xorl	%eax, %eax
	movw	%ax, 30(%rsp)
	movw	%dx, (%rsp)
	xorl	%eax, %eax
	movq	%rsp, %rdx
	movl	$7, %esi
	movq	$0, 18(%rsp)
	movups	%xmm0, 2(%rsp)
	movl	$0, 26(%rsp)
	call	__fcntl64_nocancel
	cmpq	$0, %r14
	jle	.L72
	movq	last_entry(%rip), %rax
	leaq	8(%rbp), %rdi
	movq	%rbp, %rcx
	leaq	last_entry(%rip), %rsi
	andq	$-8, %rdi
	movq	%rax, 0(%rbp)
	movq	376+last_entry(%rip), %rax
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$384, %ecx
	shrl	$3, %ecx
	movq	%rax, 376(%rbp)
	xorl	%eax, %eax
	rep movsq
	movq	%rbp, (%rbx)
.L58:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L70:
	call	__libc_setutent
	testl	%eax, %eax
	movl	file_fd(%rip), %edi
	jne	.L59
.L60:
	movq	$0, (%rbx)
	addq	$32, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	movl	$-1, %eax
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L72:
	jne	.L60
	movl	%r13d, %fs:(%r12)
	jmp	.L60
	.size	__libc_getutent_r, .-__libc_getutent_r
	.p2align 4,,15
	.globl	__libc_getutid_r
	.hidden	__libc_getutid_r
	.type	__libc_getutid_r, @function
__libc_getutid_r:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	subq	$32, %rsp
	movl	file_fd(%rip), %edi
	testl	%edi, %edi
	js	.L85
.L74:
	xorl	%esi, %esi
	call	try_file_lock
	testb	%al, %al
	jne	.L75
	movq	%r12, %rdi
	call	internal_getut_nolock
	pxor	%xmm0, %xmm0
	movl	file_fd(%rip), %edi
	movl	%eax, %r12d
	movl	$2, %edx
	xorl	%eax, %eax
	movw	%ax, 30(%rsp)
	movw	%dx, (%rsp)
	xorl	%eax, %eax
	movq	%rsp, %rdx
	movl	$7, %esi
	movq	$0, 18(%rsp)
	movups	%xmm0, 2(%rsp)
	movl	$0, 26(%rsp)
	call	__fcntl64_nocancel
	testl	%r12d, %r12d
	js	.L75
	movq	last_entry(%rip), %rax
	leaq	8(%rbp), %rdi
	movq	%rbp, %rcx
	leaq	last_entry(%rip), %rsi
	andq	$-8, %rdi
	movq	%rax, 0(%rbp)
	movq	376+last_entry(%rip), %rax
	subq	%rdi, %rcx
	subq	%rcx, %rsi
	addl	$384, %ecx
	shrl	$3, %ecx
	movq	%rax, 376(%rbp)
	xorl	%eax, %eax
	rep movsq
	movq	%rbp, (%rbx)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L85:
	call	__libc_setutent
	testl	%eax, %eax
	movl	file_fd(%rip), %edi
	jne	.L74
.L75:
	movq	$0, (%rbx)
	addq	$32, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	__libc_getutid_r, .-__libc_getutid_r
	.p2align 4,,15
	.globl	__libc_getutline_r
	.hidden	__libc_getutline_r
	.type	__libc_getutline_r, @function
__libc_getutline_r:
	pushq	%r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	%rdx, %rbp
	subq	$32, %rsp
	movl	file_fd(%rip), %edi
	testl	%edi, %edi
	js	.L99
.L87:
	xorl	%esi, %esi
	call	try_file_lock
	testb	%al, %al
	jne	.L98
	addq	$8, %r13
	.p2align 4,,10
	.p2align 3
.L95:
	call	read_last_entry
	testq	%rax, %rax
	js	.L100
	je	.L101
	movzwl	last_entry(%rip), %eax
	leaq	last_entry(%rip), %rbx
	subl	$6, %eax
	cmpw	$1, %ax
	ja	.L95
	leaq	8(%rbx), %rsi
	movl	$32, %edx
	movq	%r13, %rdi
	call	strncmp
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L95
	pxor	%xmm0, %xmm0
	movl	file_fd(%rip), %edi
	xorl	%eax, %eax
	movl	$2, %edx
	movl	$7, %esi
	movw	%ax, 30(%rsp)
	movw	%dx, (%rsp)
	xorl	%eax, %eax
	movq	%rsp, %rdx
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	movups	%xmm0, 2(%rsp)
	call	__fcntl64_nocancel
	leaq	8(%r12), %rdi
	movq	%rbx, %rsi
	movq	(%rbx), %rax
	andq	$-8, %rdi
	movq	%rax, (%r12)
	movq	376+last_entry(%rip), %rax
	movq	%rax, 376(%r12)
	movq	%r12, %rax
	subq	%rdi, %rax
	leal	384(%rax), %ecx
	subq	%rax, %rsi
	movl	%ecx, %eax
	shrl	$3, %eax
	movl	%eax, %ecx
	movl	%r14d, %eax
	rep movsq
	movq	%r12, 0(%rbp)
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L101:
	pxor	%xmm0, %xmm0
	movl	file_fd(%rip), %edi
	movl	$2, %esi
	xorl	%ecx, %ecx
	movq	%rsp, %rdx
	movw	%si, (%rsp)
	xorl	%eax, %eax
	movl	$7, %esi
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	movw	%cx, 30(%rsp)
	call	__fcntl64_nocancel
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$3, %fs:(%rax)
.L98:
	movq	$0, 0(%rbp)
	movl	$-1, %r14d
	addq	$32, %rsp
	popq	%rbx
	movl	%r14d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, 30(%rsp)
	movl	file_fd(%rip), %edi
	movl	$2, %r8d
	movq	%rsp, %rdx
	movl	$7, %esi
	xorl	%eax, %eax
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	movl	$-1, %r14d
	movw	%r8w, (%rsp)
	call	__fcntl64_nocancel
	movq	$0, 0(%rbp)
	addq	$32, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L99:
	call	__libc_setutent
	testl	%eax, %eax
	movl	file_fd(%rip), %edi
	jne	.L87
	jmp	.L98
	.size	__libc_getutline_r, .-__libc_getutline_r
	.p2align 4,,15
	.globl	__libc_pututline
	.hidden	__libc_pututline
	.type	__libc_pututline, @function
__libc_pututline:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movl	file_fd(%rip), %r9d
	testl	%r9d, %r9d
	js	.L140
.L103:
	cmpb	$0, file_writable(%rip)
	jne	.L122
	movq	__libc_utmp_file_name(%rip), %rdx
	leaq	.LC2(%rip), %rdi
	movl	$14, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	je	.L141
.L105:
	leaq	.LC3(%rip), %rdi
	movl	$14, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	je	.L142
.L107:
	leaq	.LC1(%rip), %rax
	movl	$15, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	je	.L143
.L108:
	leaq	.LC0(%rip), %rax
	movl	$15, %ecx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	je	.L144
.L106:
	xorl	%eax, %eax
	movl	$524290, %esi
	movq	%rdx, %rdi
	call	__open_nocancel
	cmpl	$-1, %eax
	movl	%eax, %ebp
	je	.L139
	movl	file_fd(%rip), %esi
	movl	%eax, %edi
	call	__dup2
	testl	%eax, %eax
	movl	%ebp, %edi
	js	.L145
	call	__close_nocancel
	movb	$1, file_writable(%rip)
.L122:
	movl	file_fd(%rip), %edi
	movl	$1, %esi
	call	try_file_lock
	testb	%al, %al
	movl	%eax, %r12d
	jne	.L139
	movq	%rbx, %rdi
	call	matches_last_entry
	testb	%al, %al
	je	.L116
	subq	$384, file_offset(%rip)
	call	read_last_entry
	testq	%rax, %rax
	js	.L119
	jne	.L146
.L116:
	movq	%rbx, %rdi
	call	internal_getut_nolock
	testl	%eax, %eax
	js	.L147
.L113:
	movq	file_offset(%rip), %rax
	movl	$1, %r12d
	leaq	-384(%rax), %rbp
.L117:
	movl	file_fd(%rip), %edi
	xorl	%edx, %edx
	movq	%rbp, %rsi
	call	__lseek64
	testq	%rax, %rax
	js	.L119
	movl	file_fd(%rip), %edi
	movl	$384, %edx
	movq	%rbx, %rsi
	call	__write_nocancel
	testq	%rax, %rax
	js	.L119
	cmpq	$384, %rax
	movl	file_fd(%rip), %edi
	jne	.L148
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$2, %edx
	movw	%ax, 30(%rsp)
	movw	%dx, (%rsp)
	movl	$7, %esi
	movq	%rsp, %rdx
	xorl	%eax, %eax
	addq	$384, %rbp
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	call	__fcntl64_nocancel
	movq	%rbx, %rax
	movq	%rbp, file_offset(%rip)
.L102:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L147:
	movl	file_fd(%rip), %edi
	movl	$2, %edx
	xorl	%esi, %esi
	call	__lseek64
	movabsq	$-6148914691236517205, %rdx
	mulq	%rdx
	shrq	$8, %rdx
	leaq	(%rdx,%rdx,2), %rbp
	salq	$7, %rbp
	jmp	.L117
	.p2align 4,,10
	.p2align 3
.L141:
	leaq	.LC1(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	leaq	.LC1(%rip), %rdx
	je	.L106
	movq	__libc_utmp_file_name(%rip), %rdx
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L148:
	testb	%r12b, %r12b
	jne	.L121
	movq	%rbp, %rsi
	call	__ftruncate64
	movl	file_fd(%rip), %edi
.L121:
	pxor	%xmm0, %xmm0
	movl	$2, %esi
	xorl	%ecx, %ecx
	movw	%si, (%rsp)
	movq	%rsp, %rdx
	movl	$7, %esi
	xorl	%eax, %eax
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	movups	%xmm0, 2(%rsp)
	movw	%cx, 30(%rsp)
	call	__fcntl64_nocancel
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$28, %fs:(%rax)
.L139:
	xorl	%eax, %eax
.L149:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L140:
	call	__libc_setutent
	testl	%eax, %eax
	jne	.L103
	xorl	%eax, %eax
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L146:
	movq	%rbx, %rdi
	call	matches_last_entry
	testb	%al, %al
	jne	.L113
	jmp	.L116
	.p2align 4,,10
	.p2align 3
.L142:
	leaq	.LC0(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	leaq	.LC0(%rip), %rdx
	je	.L106
	movq	__libc_utmp_file_name(%rip), %rdx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L143:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__access
	testl	%eax, %eax
	leaq	.LC2(%rip), %rdx
	jne	.L106
	movq	__libc_utmp_file_name(%rip), %rdx
	jmp	.L108
	.p2align 4,,10
	.p2align 3
.L144:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__access
	leaq	.LC3(%rip), %rdx
	testl	%eax, %eax
	cmove	__libc_utmp_file_name(%rip), %rdx
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%edi, %edi
	pxor	%xmm0, %xmm0
	movw	%di, 30(%rsp)
	movl	file_fd(%rip), %edi
	movl	$2, %r8d
	movq	%rsp, %rdx
	movl	$7, %esi
	xorl	%eax, %eax
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	movw	%r8w, (%rsp)
	call	__fcntl64_nocancel
	xorl	%eax, %eax
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L145:
	call	__close_nocancel
	xorl	%eax, %eax
	jmp	.L102
	.size	__libc_pututline, .-__libc_pututline
	.p2align 4,,15
	.globl	__libc_endutent
	.hidden	__libc_endutent
	.type	__libc_endutent, @function
__libc_endutent:
	movl	file_fd(%rip), %edi
	testl	%edi, %edi
	js	.L153
	subq	$8, %rsp
	call	__close_nocancel
	movl	$-1, file_fd(%rip)
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L153:
	rep ret
	.size	__libc_endutent, .-__libc_endutent
	.p2align 4,,15
	.globl	__libc_updwtmp
	.hidden	__libc_updwtmp
	.type	__libc_updwtmp, @function
__libc_updwtmp:
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movl	$1, %esi
	subq	$40, %rsp
	call	__open_nocancel
	testl	%eax, %eax
	js	.L161
	movl	$1, %esi
	movl	%eax, %edi
	movl	%eax, %ebx
	call	try_file_lock
	testb	%al, %al
	jne	.L169
	movl	$2, %edx
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	__lseek64
	movabsq	$-6148914691236517205, %rdx
	movq	%rax, %rbp
	mulq	%rdx
	shrq	$8, %rdx
	leaq	(%rdx,%rdx,2), %rax
	movq	%rbp, %rdx
	salq	$7, %rax
	subq	%rax, %rdx
	je	.L159
	subq	%rdx, %rbp
	movl	%ebx, %edi
	movl	$-1, %r13d
	movq	%rbp, %rsi
	call	__ftruncate64
	xorl	%esi, %esi
	movl	$2, %edx
	movl	%ebx, %edi
	call	__lseek64
	testq	%rax, %rax
	js	.L160
.L159:
	movl	$384, %edx
	movq	%r12, %rsi
	movl	%ebx, %edi
	call	__write_nocancel
	xorl	%r13d, %r13d
	cmpq	$384, %rax
	jne	.L170
.L160:
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$2, %edx
	movw	%ax, 30(%rsp)
	movw	%dx, (%rsp)
	movl	%ebx, %edi
	movq	%rsp, %rdx
	movl	$7, %esi
	xorl	%eax, %eax
	movups	%xmm0, 2(%rsp)
	movq	$0, 18(%rsp)
	movl	$0, 26(%rsp)
	call	__fcntl64_nocancel
	movl	%ebx, %edi
	call	__close_nocancel
.L156:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L170:
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$-1, %r13d
	call	__ftruncate64
	jmp	.L160
	.p2align 4,,10
	.p2align 3
.L161:
	movl	$-1, %r13d
	jmp	.L156
	.p2align 4,,10
	.p2align 3
.L169:
	movl	%ebx, %edi
	movl	$-1, %r13d
	call	__close_nocancel
	jmp	.L156
	.size	__libc_updwtmp, .-__libc_updwtmp
	.local	last_entry
	.comm	last_entry,384,32
	.local	file_offset
	.comm	file_offset,8,8
	.local	file_writable
	.comm	file_writable,1,1
	.data
	.align 4
	.type	file_fd, @object
	.size	file_fd, 4
file_fd:
	.long	-1
	.hidden	__ftruncate64
	.hidden	__write_nocancel
	.hidden	__close_nocancel
	.hidden	__dup2
	.hidden	__access
	.hidden	__lseek64
	.hidden	__open_nocancel
	.hidden	__libc_utmp_file_name
	.hidden	strncmp
	.hidden	__pread64_nocancel
	.hidden	__fcntl64_nocancel
	.hidden	__sigaction
	.hidden	alarm
