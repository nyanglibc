	.text
	.p2align 4,,15
	.globl	__ptsname_r
	.hidden	__ptsname_r
	.type	__ptsname_r, @function
__ptsname_r:
	pushq	%r15
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	pushq	%r12
	xorl	%eax, %eax
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	movl	$2147767344, %esi
	subq	$56, %rsp
	movq	__libc_errno@gottpoff(%rip), %rbp
	leaq	12(%rsp), %rdx
	movl	%fs:0(%rbp), %r15d
	call	__ioctl
	testl	%eax, %eax
	je	.L7
	movl	%fs:0(%rbp), %ebx
.L1:
	addq	$56, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	12(%rsp), %edi
	leaq	16(%rsp), %r12
	leaq	36(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	$10, %edx
	movl	%eax, %ebx
	movb	$0, 36(%rsp)
	call	_itoa_word
	leaq	21(%r12), %rdx
	subq	%rax, %rdx
	leaq	9(%rdx), %rcx
	cmpq	%r14, %rcx
	ja	.L8
	movabsq	$8319397760812278831, %rcx
	leaq	9(%r13), %rdi
	movq	%rax, %rsi
	movq	%rcx, 0(%r13)
	movl	$47, %ecx
	movw	%cx, 8(%r13)
	call	memcpy@PLT
	movl	%r15d, %fs:0(%rbp)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$34, %fs:0(%rbp)
	movl	$34, %ebx
	jmp	.L1
	.size	__ptsname_r, .-__ptsname_r
	.weak	ptsname_r
	.set	ptsname_r,__ptsname_r
	.p2align 4,,15
	.globl	ptsname
	.type	ptsname, @function
ptsname:
	leaq	buffer(%rip), %rsi
	subq	$8, %rsp
	movl	$30, %edx
	call	__ptsname_r
	testl	%eax, %eax
	leaq	buffer(%rip), %rax
	movl	$0, %edx
	cmovne	%rdx, %rax
	addq	$8, %rsp
	ret
	.size	ptsname, .-ptsname
	.local	buffer
	.comm	buffer,30,16
	.hidden	_itoa_word
	.hidden	__ioctl
