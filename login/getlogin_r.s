	.text
	.p2align 4,,15
	.type	getlogin_r_fd0, @function
getlogin_r_fd0:
	pushq	%r14
	pushq	%r13
	movl	$512, %edx
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	xorl	%edi, %edi
	movq	%rsi, %r13
	subq	$1296, %rsp
	leaq	784(%rsp), %rbp
	movq	%rbp, %rsi
	call	__ttyname_r
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L12
.L1:
	addq	$1296, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	5(%rbp), %rsi
	leaq	16(%rsp), %rbp
	movl	$32, %edx
	leaq	8(%rbp), %rdi
	call	strncpy
#APP
# 66 "../sysdeps/unix/getlogin_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L3
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L4:
	call	__libc_setutent
	leaq	8(%rsp), %rdx
	leaq	400(%rsp), %rsi
	movq	%rbp, %rdi
	call	__libc_getutline_r
	testl	%eax, %eax
	movl	%eax, %ebx
	jns	.L5
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	movl	$2, %eax
	cmpl	$3, %ebx
	cmove	%eax, %ebx
.L5:
	call	__libc_endutent
#APP
# 78 "../sysdeps/unix/getlogin_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	subl	$1, __libc_utmp_lock(%rip)
.L7:
	testl	%ebx, %ebx
	jne	.L1
	movq	8(%rsp), %rax
	movl	$32, %esi
	leaq	44(%rax), %rbp
	movq	%rbp, %rdi
	call	__strnlen
	movq	%rax, %r14
	leaq	1(%rax), %rax
	cmpq	%r13, %rax
	jbe	.L8
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %ebx
	movl	$34, %fs:(%rax)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%r14, %rdx
	movq	%rbp, %rsi
	movq	%r12, %rdi
	call	memcpy@PLT
	movb	$0, (%r12,%r14)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
#APP
# 78 "../sysdeps/unix/getlogin_r.c" 1
	xchgl %eax, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L7
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__libc_utmp_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 78 "../sysdeps/unix/getlogin_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%ebx, %eax
	lock cmpxchgl	%edx, __libc_utmp_lock(%rip)
	je	.L4
	leaq	__libc_utmp_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.size	getlogin_r_fd0, .-getlogin_r_fd0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/self/loginuid"
	.text
	.p2align 4,,15
	.globl	__getlogin_r_loginuid
	.hidden	__getlogin_r_loginuid
	.type	__getlogin_r_loginuid, @function
__getlogin_r_loginuid:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r14
	pushq	%rbp
	pushq	%rbx
	xorl	%esi, %esi
	subq	$1144, %rsp
	movq	%rdi, 8(%rsp)
	leaq	.LC0(%rip), %rdi
	call	__open_nocancel
	cmpl	$-1, %eax
	je	.L18
	movl	%eax, %r12d
	leaq	36(%rsp), %rbp
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L32:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L31
.L14:
	movl	$12, %edx
	movq	%rbp, %rsi
	movl	%r12d, %edi
	call	__read_nocancel
	cmpq	$-1, %rax
	movq	%rax, %r15
	je	.L32
	movl	%r12d, %edi
	call	__close_nocancel
	testq	%r15, %r15
	jle	.L18
	cmpq	$12, %r15
	je	.L18
	leaq	16(%rsp), %rsi
	movl	$10, %edx
	movq	%rbp, %rdi
	movb	$0, 36(%rsp,%r15)
	call	strtoul
	movq	16(%rsp), %rdx
	movl	%eax, %r12d
	cmpq	%rbp, %rdx
	je	.L18
	cmpb	$0, (%rdx)
	jne	.L18
	cmpl	$-1, %eax
	je	.L33
	leaq	96(%rsp), %rbp
	movq	$1024, 104(%rsp)
	movl	$1024, %ecx
	leaq	24(%rsp), %rbx
	leaq	48(%rsp), %r13
	leaq	16(%rbp), %rdx
	movq	%rdx, 96(%rsp)
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	movq	104(%rsp), %rcx
	movq	96(%rsp), %rdx
.L20:
	movq	%rbx, %r8
	movq	%r13, %rsi
	movl	%r12d, %edi
	call	__getpwuid_r
	cmpl	$34, %eax
	movl	%eax, %r15d
	jne	.L34
	movq	%rbp, %rdi
	call	__libc_scratch_buffer_grow
	testb	%al, %al
	jne	.L35
	movl	$12, %r15d
.L21:
	movq	96(%rsp), %rdi
	addq	$16, %rbp
	cmpq	%rbp, %rdi
	je	.L13
	call	free@PLT
.L13:
	addq	$1144, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	%r12d, %edi
	call	__close_nocancel
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L34:
	testl	%eax, %eax
	jne	.L26
	cmpq	$0, 24(%rsp)
	je	.L26
	movq	48(%rsp), %rbx
	movq	%rbx, %rdi
	call	strlen
	leaq	1(%rax), %rdx
	cmpq	%r14, %rdx
	jbe	.L23
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$34, %r15d
	movl	$34, %fs:(%rax)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	movl	$-1, %r15d
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L33:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$6, %r15d
	movl	$6, %fs:(%rax)
	jmp	.L13
.L23:
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	call	memcpy@PLT
	jmp	.L21
.L18:
	movl	$-1, %r15d
	jmp	.L13
	.size	__getlogin_r_loginuid, .-__getlogin_r_loginuid
	.p2align 4,,15
	.globl	__getlogin_r
	.hidden	__getlogin_r
	.type	__getlogin_r, @function
__getlogin_r:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__getlogin_r_loginuid
	testl	%eax, %eax
	js	.L39
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	getlogin_r_fd0
	.size	__getlogin_r, .-__getlogin_r
	.weak	getlogin_r
	.hidden	getlogin_r
	.set	getlogin_r,__getlogin_r
	.hidden	strlen
	.hidden	__libc_scratch_buffer_grow
	.hidden	__getpwuid_r
	.hidden	strtoul
	.hidden	__close_nocancel
	.hidden	__read_nocancel
	.hidden	__open_nocancel
	.hidden	__lll_lock_wait_private
	.hidden	__strnlen
	.hidden	__libc_endutent
	.hidden	__libc_getutline_r
	.hidden	__libc_setutent
	.hidden	__libc_utmp_lock
	.hidden	strncpy
	.hidden	__ttyname_r
