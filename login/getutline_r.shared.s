	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___getutline_r
	.hidden	__GI___getutline_r
	.type	__GI___getutline_r, @function
__GI___getutline_r:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
#APP
# 37 "getutline_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %ecx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	%rbx, %rdi
	call	__libc_getutline_r
	movl	%eax, %r8d
#APP
# 41 "getutline_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	subl	$1, __libc_utmp_lock(%rip)
.L1:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, __libc_utmp_lock(%rip)
	je	.L3
	leaq	__libc_utmp_lock(%rip), %rdi
	movq	%rdx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rdx
	movq	(%rsp), %rsi
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
#APP
# 41 "getutline_r.c" 1
	xchgl %eax, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__libc_utmp_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 41 "getutline_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	__GI___getutline_r, .-__GI___getutline_r
	.globl	__getutline_r
	.set	__getutline_r,__GI___getutline_r
	.weak	getutline_r
	.set	getutline_r,__getutline_r
	.hidden	__lll_lock_wait_private
	.hidden	__libc_getutline_r
	.hidden	__libc_utmp_lock
