	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	getlogin_fd0, @function
getlogin_fd0:
	pushq	%rbx
	xorl	%edi, %edi
	movl	$512, %edx
	subq	$1296, %rsp
	leaq	784(%rsp), %rbx
	movq	%rbx, %rsi
	call	__ttyname_r
	testl	%eax, %eax
	jne	.L9
	call	__setutent
	leaq	5(%rbx), %rsi
	leaq	24(%rsp), %rdi
	movl	$32, %edx
	leaq	16(%rsp), %rbx
	call	__GI_strncpy
	leaq	8(%rsp), %rdx
	leaq	400(%rsp), %rsi
	movq	%rbx, %rdi
	call	__GI___getutline_r
	testl	%eax, %eax
	jns	.L4
	movq	__libc_errno@gottpoff(%rip), %rax
	xorl	%ebx, %ebx
	cmpl	$3, %fs:(%rax)
	jne	.L5
	movl	$2, %fs:(%rax)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L4:
	movq	8(%rsp), %rax
	leaq	name(%rip), %rdi
	movl	$32, %edx
	leaq	name(%rip), %rbx
	leaq	44(%rax), %rsi
	call	__GI_strncpy
	movb	$0, 32+name(%rip)
.L5:
	call	__endutent
	addq	$1296, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rdx
	xorl	%ebx, %ebx
	movl	%eax, %fs:(%rdx)
	addq	$1296, %rsp
	movq	%rbx, %rax
	popq	%rbx
	ret
	.size	getlogin_fd0, .-getlogin_fd0
	.p2align 4,,15
	.globl	getlogin
	.type	getlogin, @function
getlogin:
	leaq	name(%rip), %rdi
	subq	$8, %rsp
	movl	$33, %esi
	call	__getlogin_r_loginuid
	testl	%eax, %eax
	js	.L17
	leaq	name(%rip), %rax
	movl	$0, %edx
	cmovne	%rdx, %rax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	addq	$8, %rsp
	jmp	getlogin_fd0
	.size	getlogin, .-getlogin
	.local	name
	.comm	name,33,32
	.hidden	__getlogin_r_loginuid
	.hidden	__endutent
	.hidden	__setutent
	.hidden	__ttyname_r
