	.text
#APP
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__getutline
	.hidden	__getutline
	.type	__getutline, @function
__getutline:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	buffer(%rip), %rsi
	testq	%rsi, %rsi
	je	.L7
.L2:
	leaq	8(%rsp), %rdx
	movq	%rbx, %rdi
	call	__getutline_r
	testl	%eax, %eax
	js	.L4
	movq	8(%rsp), %rax
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$384, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rsi
	movq	%rax, buffer(%rip)
	jne	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	jmp	.L1
	.size	__getutline, .-__getutline
	.weak	getutline
	.set	getutline,__getutline
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
	.hidden	__getutline_r
