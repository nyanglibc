	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/log/wtmpx"
.LC1:
	.string	"/var/run/utmpx"
.LC2:
	.string	"/var/run/utmp"
.LC3:
	.string	"/var/log/wtmp"
	.text
	.p2align 4,,15
	.globl	__updwtmp
	.hidden	__updwtmp
	.type	__updwtmp, @function
__updwtmp:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	.LC2(%rip), %rdi
	movq	%rsi, %rbp
	movl	$14, %ecx
	subq	$8, %rsp
	movq	%rbx, %rsi
	repz cmpsb
	je	.L2
.L5:
	leaq	.LC3(%rip), %rdi
	movl	$14, %ecx
	movq	%rbx, %rsi
	repz cmpsb
	je	.L20
.L3:
	leaq	.LC1(%rip), %rax
	movl	$15, %ecx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	je	.L21
.L7:
	leaq	.LC0(%rip), %rax
	movl	$15, %ecx
	movq	%rbx, %rsi
	movq	%rax, %rdi
	repz cmpsb
	je	.L22
.L6:
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__libc_updwtmp
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__access
	testl	%eax, %eax
	je	.L7
	leaq	.LC2(%rip), %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%esi, %esi
	movq	%rax, %rdi
	call	__access
	testl	%eax, %eax
	leaq	.LC3(%rip), %rax
	cmovne	%rax, %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	.LC1(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	jne	.L5
	leaq	.LC1(%rip), %rbx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	.LC0(%rip), %rdi
	xorl	%esi, %esi
	call	__access
	testl	%eax, %eax
	jne	.L3
	leaq	.LC0(%rip), %rbx
	jmp	.L6
	.size	__updwtmp, .-__updwtmp
	.weak	updwtmp
	.set	updwtmp,__updwtmp
	.hidden	__access
	.hidden	__libc_updwtmp
