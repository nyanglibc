	.text
	.p2align 4,,15
	.globl	__ptsname_r_chk
	.type	__ptsname_r_chk, @function
__ptsname_r_chk:
	cmpq	%rcx, %rdx
	ja	.L7
	jmp	__ptsname_r
.L7:
	subq	$8, %rsp
	call	__chk_fail
	.size	__ptsname_r_chk, .-__ptsname_r_chk
	.hidden	__chk_fail
	.hidden	__ptsname_r
