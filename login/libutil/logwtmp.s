	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/log/wtmp"
	.text
	.p2align 4,,15
	.globl	logwtmp
	.type	logwtmp, @function
logwtmp:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movl	$48, %ecx
	movq	%rsi, %rbp
	movq	%rdx, %r12
	subq	$408, %rsp
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	rep stosq
	call	getpid@PLT
	movl	%eax, 20(%rsp)
	xorl	%eax, %eax
	cmpb	$0, 0(%rbp)
	leaq	8(%rbx), %rdi
	movq	%r13, %rsi
	movl	$32, %edx
	sete	%al
	addl	$7, %eax
	movw	%ax, 16(%rsp)
	call	strncpy@PLT
	leaq	44(%rbx), %rdi
	movq	%rbp, %rsi
	movl	$32, %edx
	call	strncpy@PLT
	leaq	76(%rbx), %rdi
	movl	$256, %edx
	movq	%r12, %rsi
	call	strncpy@PLT
	movq	%rsp, %rsi
	xorl	%edi, %edi
	call	__clock_gettime@PLT
	movq	(%rsp), %rax
	movq	8(%rsp), %rcx
	movabsq	$2361183241434822607, %rdx
	leaq	.LC0(%rip), %rdi
	movq	%rbx, %rsi
	movl	%eax, 356(%rsp)
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	sarq	$7, %rdx
	subq	%rcx, %rdx
	movl	%edx, 360(%rsp)
	call	updwtmp@PLT
	addq	$408, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	logwtmp, .-logwtmp
