	.text
	.p2align 4,,15
	.type	pts_name.constprop.0, @function
pts_name.constprop.0:
	pushq	%r14
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	$4096, %ebx
	movq	(%rsi), %rbp
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	(%r12), %rbp
	movl	$128, %ebx
	je	.L5
.L14:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L13
.L10:
	movq	%rax, %rbp
.L7:
	testq	%rbx, %rbx
	je	.L9
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movl	%r14d, %edi
	call	ptsname_r@PLT
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L3
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L4
	addq	%rbx, %rbx
	cmpq	(%r12), %rbp
	jne	.L14
.L5:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L10
.L13:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r13d
	movl	$12, %fs:(%rax)
.L3:
	cmpq	(%r12), %rbp
	je	.L1
	movq	%rbp, %rdi
	call	free@PLT
.L1:
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	popq	%rbx
	movq	%rbp, (%r12)
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	pts_name.constprop.0, .-pts_name.constprop.0
	.p2align 4,,15
	.globl	__GI_openpty
	.hidden	__GI_openpty
	.type	__GI_openpty, @function
__GI_openpty:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movq	%r8, %rbp
	subq	$4152, %rsp
	leaq	48(%rsp), %rbx
	movq	%rdi, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movb	$0, 48(%rsp)
	movq	%rbx, 40(%rsp)
	call	getpt@PLT
	cmpl	$-1, %eax
	movl	%eax, %r15d
	je	.L27
	movl	%eax, %edi
	call	grantpt@PLT
	testl	%eax, %eax
	movl	%r15d, %edi
	je	.L39
.L38:
	call	close@PLT
	movq	40(%rsp), %rbp
	movl	$-1, %r14d
.L24:
	cmpq	%rbx, %rbp
	je	.L15
	movq	%rbp, %rdi
	call	free@PLT
.L15:
	addq	$4152, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	call	unlockpt@PLT
	testl	%eax, %eax
	movl	%eax, %r14d
	je	.L40
	movl	%r15d, %edi
	movl	$-1, %r14d
	call	close@PLT
	movq	40(%rsp), %rbp
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L40:
	xorl	%eax, %eax
	movl	$258, %edx
	movl	$21569, %esi
	movl	%r15d, %edi
	call	ioctl@PLT
	cmpl	$-1, %eax
	movl	%eax, 28(%rsp)
	je	.L41
.L19:
	testq	%r12, %r12
	je	.L22
	movl	28(%rsp), %edi
	movq	%r12, %rdx
	movl	$2, %esi
	call	tcsetattr@PLT
.L22:
	testq	%rbp, %rbp
	je	.L23
	movl	28(%rsp), %edi
	movq	%rbp, %rdx
	movl	$21524, %esi
	xorl	%eax, %eax
	call	ioctl@PLT
.L23:
	movq	8(%rsp), %rax
	movl	28(%rsp), %ecx
	testq	%r13, %r13
	movq	40(%rsp), %rbp
	movl	%r15d, (%rax)
	movq	16(%rsp), %rax
	movl	%ecx, (%rax)
	je	.L24
	cmpb	$0, 0(%rbp)
	jne	.L25
	leaq	40(%rsp), %rsi
	movl	%r15d, %edi
	call	pts_name.constprop.0
	testl	%eax, %eax
	jne	.L26
	movq	40(%rsp), %rbp
.L25:
	movq	%rbp, %rsi
	movq	%r13, %rdi
	call	strcpy@PLT
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movl	%r15d, %edi
	call	close@PLT
	movl	28(%rsp), %edi
	jmp	.L38
	.p2align 4,,10
	.p2align 3
.L41:
	leaq	40(%rsp), %rsi
	movl	%r15d, %edi
	call	pts_name.constprop.0
	testl	%eax, %eax
	je	.L42
.L21:
	movl	%r15d, %edi
	call	close@PLT
	movq	40(%rsp), %rbp
	movl	28(%rsp), %r14d
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L27:
	movl	%eax, %r14d
	jmp	.L15
	.p2align 4,,10
	.p2align 3
.L42:
	movq	40(%rsp), %rdi
	movl	$258, %esi
	call	open@PLT
	cmpl	$-1, %eax
	movl	%eax, 28(%rsp)
	jne	.L19
	jmp	.L21
	.size	__GI_openpty, .-__GI_openpty
	.globl	openpty
	.set	openpty,__GI_openpty
