	.text
	.p2align 4,,15
	.globl	__GI_login_tty
	.hidden	__GI_login_tty
	.type	__GI_login_tty, @function
__GI_login_tty:
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebx
	subq	$8, %rsp
	call	setsid@PLT
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$21518, %esi
	movl	%ebx, %edi
	call	ioctl@PLT
	cmpl	$-1, %eax
	movl	%eax, %ebp
	jne	.L4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L18:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$16, %fs:(%rax)
	jne	.L6
.L4:
	xorl	%esi, %esi
	movl	%ebx, %edi
	call	dup2@PLT
	cmpl	$-1, %eax
	je	.L18
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$16, %fs:(%rax)
	jne	.L8
.L6:
	movl	$1, %esi
	movl	%ebx, %edi
	call	dup2@PLT
	cmpl	$-1, %eax
	je	.L19
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L20:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$16, %fs:(%rax)
	jne	.L7
.L8:
	movl	$2, %esi
	movl	%ebx, %edi
	call	dup2@PLT
	cmpl	$-1, %eax
	je	.L20
.L7:
	xorl	%ebp, %ebp
	cmpl	$2, %ebx
	jle	.L1
	movl	%ebx, %edi
	call	close@PLT
.L1:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__GI_login_tty, .-__GI_login_tty
	.globl	login_tty
	.set	login_tty,__GI_login_tty
