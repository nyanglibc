	.file	"login_tty.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/login
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/login/libutil/login_tty.v.d
# -MF /run/asm/login/libutil/login_tty.o.dt -MP
# -MT /run/asm/login/libutil/login_tty.o -D _LIBC_REENTRANT
# -D MODULE_NAME=libutil -D PIC -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h login_tty.c -mtune=generic
# -march=x86-64 -auxbase-strip /run/asm/login/libutil/login_tty.v.s -O2
# -Wall -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fpie -ftls-model=initial-exec
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fpic -fpie
# -fplt -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.globl	login_tty
	.type	login_tty, @function
login_tty:
.LFB15:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movl	%edi, %ebx	# fd, fd
	subq	$8, %rsp	#,
	.cfi_def_cfa_offset 32
# login_tty.c:44: 	(void) setsid();
	call	setsid@PLT	#
# login_tty.c:46: 	if (ioctl(fd, TIOCSCTTY, (char *)NULL) == -1)
	xorl	%edx, %edx	#
	xorl	%eax, %eax	#
	movl	$21518, %esi	#,
	movl	%ebx, %edi	# fd,
	call	ioctl@PLT	#
	cmpl	$-1, %eax	#, <retval>
	movl	%eax, %ebp	#, <retval>
	jne	.L4	#,
	jmp	.L1	#
	.p2align 4,,10
	.p2align 3
.L18:
# login_tty.c:66: 	while (dup2(fd, 0) == -1 && errno == EBUSY)
	movq	errno@gottpoff(%rip), %rax	#, tmp95
	cmpl	$16, %fs:(%rax)	#, errno
	jne	.L6	#,
.L4:
# login_tty.c:66: 	while (dup2(fd, 0) == -1 && errno == EBUSY)
	xorl	%esi, %esi	#
	movl	%ebx, %edi	# fd,
	call	dup2@PLT	#
	cmpl	$-1, %eax	#, _2
	je	.L18	#,
	jmp	.L6	#
	.p2align 4,,10
	.p2align 3
.L19:
# login_tty.c:68: 	while (dup2(fd, 1) == -1 && errno == EBUSY)
	movq	errno@gottpoff(%rip), %rax	#, tmp96
	cmpl	$16, %fs:(%rax)	#, errno
	jne	.L8	#,
.L6:
# login_tty.c:68: 	while (dup2(fd, 1) == -1 && errno == EBUSY)
	movl	$1, %esi	#,
	movl	%ebx, %edi	# fd,
	call	dup2@PLT	#
	cmpl	$-1, %eax	#, _4
	je	.L19	#,
	jmp	.L8	#
	.p2align 4,,10
	.p2align 3
.L20:
# login_tty.c:70: 	while (dup2(fd, 2) == -1 && errno == EBUSY)
	movq	errno@gottpoff(%rip), %rax	#, tmp97
	cmpl	$16, %fs:(%rax)	#, errno
	jne	.L7	#,
.L8:
# login_tty.c:70: 	while (dup2(fd, 2) == -1 && errno == EBUSY)
	movl	$2, %esi	#,
	movl	%ebx, %edi	# fd,
	call	dup2@PLT	#
	cmpl	$-1, %eax	#, _6
	je	.L20	#,
.L7:
# login_tty.c:74: 	return (0);
	xorl	%ebp, %ebp	# <retval>
# login_tty.c:72: 	if (fd > 2)
	cmpl	$2, %ebx	#, fd
	jle	.L1	#,
# login_tty.c:73: 		(void) close(fd);
	movl	%ebx, %edi	# fd,
	call	close@PLT	#
.L1:
# login_tty.c:75: }
	addq	$8, %rsp	#,
	.cfi_def_cfa_offset 24
	movl	%ebp, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE15:
	.size	login_tty, .-login_tty
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
