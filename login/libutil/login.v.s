	.file	"login.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/login
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/login/libutil/login.v.d
# -MF /run/asm/login/libutil/login.o.dt -MP
# -MT /run/asm/login/libutil/login.o -D _LIBC_REENTRANT
# -D MODULE_NAME=libutil -D PIC -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h login.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/login/libutil/login.v.s -O2 -Wall -Wwrite-strings
# -Wundef -Werror -Wstrict-prototypes -Wold-style-definition -std=gnu11
# -fverbose-asm -fgnu89-inline -fmerge-all-constants -frounding-math
# -fno-stack-protector -fmath-errno -fpie -ftls-model=initial-exec
# options enabled:  -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fpic -fpie
# -fplt -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.type	tty_name.constprop.0, @function
tty_name.constprop.0:
.LFB17:
	.cfi_startproc
	pushq	%r14	#
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	movl	%edi, %r14d	# fd, fd
	pushq	%r13	#
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	pushq	%r12	#
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	movq	%rsi, %r12	# tty, tty
	pushq	%rbp	#
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	pushq	%rbx	#
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
# login.c:36:   char *buf = *tty;		/* Buffer for ttyname, initially the user's. */
	movl	$4128, %ebx	#, buf_len
	movq	(%rsi), %rbp	# *tty_1(D), buf
	jmp	.L7	#
	.p2align 4,,10
	.p2align 3
.L9:
# login.c:58:       if (buf != *tty)
	cmpq	(%r12), %rbp	# *tty_1(D), buf
# login.c:56: 	buf_len = 128;		/* First time guess.  */
	movl	$128, %ebx	#, buf_len
# login.c:58:       if (buf != *tty)
	je	.L5	#,
.L14:
# login.c:60: 	new_buf = realloc (buf, buf_len);
	movq	%rbx, %rsi	# buf_len,
	movq	%rbp, %rdi	# buf,
	call	realloc@PLT	#
# login.c:63:       if (! new_buf)
	testq	%rax, %rax	# new_buf
	je	.L13	#,
.L10:
	movq	%rax, %rbp	# new_buf, buf
.L7:
# login.c:42:       if (buf_len)
	testq	%rbx, %rbx	# buf_len
	je	.L9	#,
# login.c:44: 	  rv = ttyname_r (fd, buf, buf_len);
	movq	%rbx, %rdx	# buf_len,
	movq	%rbp, %rsi	# buf,
	movl	%r14d, %edi	# fd,
	call	ttyname_r@PLT	#
# login.c:46: 	  if (rv != 0 || memchr (buf, '\0', buf_len))
	testl	%eax, %eax	# <retval>
# login.c:44: 	  rv = ttyname_r (fd, buf, buf_len);
	movl	%eax, %r13d	#, <retval>
# login.c:46: 	  if (rv != 0 || memchr (buf, '\0', buf_len))
	jne	.L3	#,
	xorl	%esi, %esi	#
	movq	%rbx, %rdx	# buf_len,
	movq	%rbp, %rdi	# buf,
	call	memchr@PLT	#
	testq	%rax, %rax	# _7
	jne	.L4	#,
# login.c:52: 	  buf_len += buf_len;	/* Double it */
	addq	%rbx, %rbx	# buf_len
# login.c:58:       if (buf != *tty)
	cmpq	(%r12), %rbp	# *tty_1(D), buf
	jne	.L14	#,
.L5:
# login.c:62: 	new_buf = malloc (buf_len);
	movq	%rbx, %rdi	# buf_len,
	call	malloc@PLT	#
# login.c:63:       if (! new_buf)
	testq	%rax, %rax	# new_buf
	jne	.L10	#,
.L13:
# login.c:66: 	  __set_errno (ENOMEM);
	movq	errno@gottpoff(%rip), %rax	#, tmp97
# login.c:65: 	  rv = -1;
	movl	$-1, %r13d	#, <retval>
# login.c:66: 	  __set_errno (ENOMEM);
	movl	$12, %fs:(%rax)	#, errno
.L3:
# login.c:74:   else if (buf != *tty)
	cmpq	(%r12), %rbp	# *tty_1(D), buf
	je	.L1	#,
# login.c:75:     free (buf);		/* Free what we malloced when returning an error.  */
	movq	%rbp, %rdi	# buf,
	call	free@PLT	#
.L1:
# login.c:78: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	movl	%r13d, %eax	# <retval>,
	popq	%rbp	#
	.cfi_def_cfa_offset 32
	popq	%r12	#
	.cfi_def_cfa_offset 24
	popq	%r13	#
	.cfi_def_cfa_offset 16
	popq	%r14	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	popq	%rbx	#
	.cfi_def_cfa_offset 40
# login.c:73:     *tty = buf;		/* Return buffer to the user.  */
	movq	%rbp, (%r12)	# buf, *tty_1(D)
# login.c:78: }
	movl	%r13d, %eax	# <retval>,
	popq	%rbp	#
	.cfi_def_cfa_offset 32
	popq	%r12	#
	.cfi_def_cfa_offset 24
	popq	%r13	#
	.cfi_def_cfa_offset 16
	popq	%r14	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE17:
	.size	tty_name.constprop.0, .-tty_name.constprop.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/"
.LC1:
	.string	"/var/run/utmp"
.LC2:
	.string	"/var/log/wtmp"
	.text
	.p2align 4,,15
	.globl	login
	.type	login, @function
login:
.LFB16:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
# login.c:94:   copy.ut_type = USER_PROCESS;
	movl	$7, %eax	#,
# login.c:82: {
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
	movq	%rdi, %rsi	# ut, ut
# login.c:91:   struct utmp copy = *ut;
	movl	$48, %ecx	#, tmp100
# login.c:82: {
	subq	$4528, %rsp	#,
	.cfi_def_cfa_offset 4560
# login.c:88:   char *tty = _tty;
	leaq	400(%rsp), %rbp	#, tmp118
# login.c:91:   struct utmp copy = *ut;
	leaq	16(%rsp), %rbx	#, tmp117
# login.c:98:   found_tty = tty_name (STDIN_FILENO, &tty, sizeof (_tty));
	leaq	8(%rsp), %r12	#, tmp101
# login.c:88:   char *tty = _tty;
	movq	%rbp, 8(%rsp)	# tmp118, tty
# login.c:91:   struct utmp copy = *ut;
	movq	%rbx, %rdi	# tmp117, tmp98
	rep movsq
# login.c:94:   copy.ut_type = USER_PROCESS;
	movw	%ax, 16(%rsp)	#, copy.ut_type
# login.c:95:   copy.ut_pid = getpid ();
	call	getpid@PLT	#
# login.c:98:   found_tty = tty_name (STDIN_FILENO, &tty, sizeof (_tty));
	xorl	%edi, %edi	#
	movq	%r12, %rsi	# tmp101,
# login.c:95:   copy.ut_pid = getpid ();
	movl	%eax, 20(%rsp)	# _1, copy.ut_pid
# login.c:98:   found_tty = tty_name (STDIN_FILENO, &tty, sizeof (_tty));
	call	tty_name.constprop.0	#
# login.c:99:   if (found_tty < 0)
	testl	%eax, %eax	# found_tty
	js	.L16	#,
.L19:
# login.c:108:       if (strncmp (tty, "/dev/", 5) == 0)
	movq	8(%rsp), %rdx	# tty, tty.0_2
	leaq	.LC0(%rip), %rdi	#, tmp102
	movl	$5, %ecx	#, tmp105
	movq	%rdx, %rsi	# tty.0_2, tty.0_2
	repz cmpsb
# login.c:109: 	ttyp = tty + 5;		/* Skip the "/dev/".  */
	leaq	5(%rdx), %rsi	#, ttyp
# login.c:108:       if (strncmp (tty, "/dev/", 5) == 0)
	seta	%cl	#, tmp106
	setb	%al	#, tmp107
	cmpb	%al, %cl	# tmp107, tmp106
	jne	.L28	#,
.L21:
# login.c:114:       strncpy (copy.ut_line, ttyp, UT_LINESIZE);
	leaq	8(%rbx), %rdi	#, tmp111
	movl	$32, %edx	#,
	call	strncpy@PLT	#
# login.c:117:       if (utmpname (_PATH_UTMP) == 0)
	leaq	.LC1(%rip), %rdi	#,
	call	utmpname@PLT	#
	testl	%eax, %eax	# _4
	je	.L29	#,
.L22:
# login.c:129:       if (tty != _tty)
	movq	8(%rsp), %rdi	# tty, tty.3_5
	cmpq	%rbp, %rdi	# tmp118, tty.3_5
	je	.L23	#,
# login.c:130: 	free (tty);		/* Free buffer malloced by tty_name.  */
	call	free@PLT	#
.L23:
# login.c:138:   updwtmp (_PATH_WTMP, &copy);
	leaq	.LC2(%rip), %rdi	#,
	movq	%rbx, %rsi	# tmp117,
	call	updwtmp@PLT	#
# login.c:139: }
	addq	$4528, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	.cfi_restore_state
# login.c:120: 	  setutent ();
	call	setutent@PLT	#
# login.c:123: 	  pututline (&copy);
	movq	%rbx, %rdi	# tmp117,
	call	pututline@PLT	#
# login.c:126: 	  endutent ();
	call	endutent@PLT	#
	jmp	.L22	#
	.p2align 4,,10
	.p2align 3
.L28:
# login.c:111: 	ttyp = basename (tty);
	movq	%rdx, %rdi	# tty.0_2,
	call	basename@PLT	#
	movq	%rax, %rsi	#, ttyp
	jmp	.L21	#
	.p2align 4,,10
	.p2align 3
.L16:
# login.c:100:     found_tty = tty_name (STDOUT_FILENO, &tty, sizeof (_tty));
	movq	%r12, %rsi	# tmp101,
	movl	$1, %edi	#,
	call	tty_name.constprop.0	#
# login.c:101:   if (found_tty < 0)
	testl	%eax, %eax	# found_tty
	jns	.L19	#,
# login.c:102:     found_tty = tty_name (STDERR_FILENO, &tty, sizeof (_tty));
	movq	%r12, %rsi	# tmp101,
	movl	$2, %edi	#,
	call	tty_name.constprop.0	#
# login.c:104:   if (found_tty >= 0)
	testl	%eax, %eax	# found_tty
	jns	.L19	#,
# login.c:135:     strncpy (copy.ut_line, "???", UT_LINESIZE);
	pxor	%xmm0, %xmm0	# tmp119
	movq	$4144959, 24(%rsp)	#, MEM[(void *)&copy + 8B]
	movq	$0, 32(%rsp)	#, MEM[(void *)&copy + 8B]
	movups	%xmm0, 24(%rbx)	# tmp119, MEM[(void *)&copy + 8B]
	jmp	.L23	#
	.cfi_endproc
.LFE16:
	.size	login, .-login
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
