	.file	"forkpty.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/login
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/login/libutil/forkpty.shared.v.d
# -MF /run/asm/login/libutil/forkpty.os.dt -MP
# -MT /run/asm/login/libutil/forkpty.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libutil -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h forkpty.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/login/libutil/forkpty.shared.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.globl	forkpty
	.type	forkpty, @function
forkpty:
.LFB6:
	.cfi_startproc
	pushq	%rbp	#
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushq	%rbx	#
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movq	%rsi, %rax	# name, name
	movq	%rdi, %rbp	# amaster, amaster
	movq	%rcx, %r8	# winp, winp
# forkpty.c:31:   if (openpty (&master, &slave, name, termp, winp) == -1)
	movq	%rdx, %rcx	# termp,
# forkpty.c:28: {
	subq	$24, %rsp	#,
	.cfi_def_cfa_offset 48
# forkpty.c:31:   if (openpty (&master, &slave, name, termp, winp) == -1)
	movq	%rax, %rdx	# name,
	leaq	12(%rsp), %rsi	#, tmp98
	leaq	8(%rsp), %rdi	#, tmp99
	call	__GI_openpty	#
	cmpl	$-1, %eax	#, <retval>
	movl	%eax, %ebx	#, <retval>
	je	.L1	#,
# forkpty.c:34:   switch (pid = fork ())
	call	fork@PLT	#
	cmpl	$-1, %eax	#, <retval>
	movl	%eax, %ebx	#, <retval>
	je	.L4	#,
	testl	%eax, %eax	# <retval>
	jne	.L15	#,
# forkpty.c:42:       close (master);
	movl	8(%rsp), %edi	# master,
	call	close@PLT	#
# forkpty.c:43:       if (login_tty (slave))
	movl	12(%rsp), %edi	# slave,
	call	__GI_login_tty	#
	testl	%eax, %eax	# <retval>
	movl	%eax, %ebx	#, <retval>
	jne	.L16	#,
.L1:
# forkpty.c:54: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	movl	%ebx, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	.cfi_restore_state
# forkpty.c:49:       *amaster = master;
	movl	8(%rsp), %eax	# master, master
# forkpty.c:50:       close (slave);
	movl	12(%rsp), %edi	# slave,
# forkpty.c:49:       *amaster = master;
	movl	%eax, 0(%rbp)	# master, *amaster_23(D)
# forkpty.c:50:       close (slave);
	call	close@PLT	#
# forkpty.c:54: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	movl	%ebx, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
# forkpty.c:37:       close (master);
	movl	8(%rsp), %edi	# master,
	call	close@PLT	#
# forkpty.c:38:       close (slave);
	movl	12(%rsp), %edi	# slave,
	call	close@PLT	#
# forkpty.c:54: }
	addq	$24, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	movl	%ebx, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 16
	popq	%rbp	#
	.cfi_def_cfa_offset 8
	ret
.L16:
	.cfi_restore_state
# forkpty.c:44: 	_exit (1);
	movl	$1, %edi	#,
	call	_exit@PLT	#
	.cfi_endproc
.LFE6:
	.size	forkpty, .-forkpty
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
