	.file	"logwtmp.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/login
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/login/libutil/logwtmp.shared.v.d
# -MF /run/asm/login/libutil/logwtmp.os.dt -MP
# -MT /run/asm/login/libutil/logwtmp.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libutil -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h logwtmp.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/login/libutil/logwtmp.shared.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/log/wtmp"
	.text
	.p2align 4,,15
	.globl	logwtmp
	.type	logwtmp, @function
logwtmp:
.LFB24:
	.cfi_startproc
	pushq	%r13	#
	.cfi_def_cfa_offset 16
	.cfi_offset 13, -16
	pushq	%r12	#
	.cfi_def_cfa_offset 24
	.cfi_offset 12, -24
	movq	%rdi, %r13	# line, line
	pushq	%rbp	#
	.cfi_def_cfa_offset 32
	.cfi_offset 6, -32
	pushq	%rbx	#
	.cfi_def_cfa_offset 40
	.cfi_offset 3, -40
# logwtmp.c:33:   memset (&ut, 0, sizeof (ut));
	xorl	%eax, %eax	# tmp100
	movl	$48, %ecx	#, tmp101
# logwtmp.c:29: {
	movq	%rsi, %rbp	# name, name
	movq	%rdx, %r12	# host, host
	subq	$408, %rsp	#,
	.cfi_def_cfa_offset 448
# logwtmp.c:33:   memset (&ut, 0, sizeof (ut));
	leaq	16(%rsp), %rbx	#, tmp117
	movq	%rbx, %rdi	# tmp117, tmp99
	rep stosq
# logwtmp.c:34:   ut.ut_pid = getpid ();
	call	getpid@PLT	#
	movl	%eax, 20(%rsp)	# _1, ut.ut_pid
# logwtmp.c:35:   ut.ut_type = name[0] ? USER_PROCESS : DEAD_PROCESS;
	xorl	%eax, %eax	#
	cmpb	$0, 0(%rbp)	#, *name_13(D)
# logwtmp.c:36:   strncpy (ut.ut_line, line, sizeof ut.ut_line);
	leaq	8(%rbx), %rdi	#, tmp103
	movq	%r13, %rsi	# line,
	movl	$32, %edx	#,
# logwtmp.c:35:   ut.ut_type = name[0] ? USER_PROCESS : DEAD_PROCESS;
	sete	%al	#, iftmp.0_8
	addl	$7, %eax	#, iftmp.0_8
	movw	%ax, 16(%rsp)	# iftmp.0_8, ut.ut_type
# logwtmp.c:36:   strncpy (ut.ut_line, line, sizeof ut.ut_line);
	call	strncpy@PLT	#
# logwtmp.c:37:   strncpy (ut.ut_name, name, sizeof ut.ut_name);
	leaq	44(%rbx), %rdi	#, tmp105
	movq	%rbp, %rsi	# name,
	movl	$32, %edx	#,
	call	strncpy@PLT	#
# logwtmp.c:38:   strncpy (ut.ut_host, host, sizeof ut.ut_host);
	leaq	76(%rbx), %rdi	#, tmp107
	movl	$256, %edx	#,
	movq	%r12, %rsi	# host,
	call	strncpy@PLT	#
# logwtmp.c:41:   __clock_gettime64 (CLOCK_REALTIME, &ts);
	movq	%rsp, %rsi	#, tmp108
	xorl	%edi, %edi	#
	call	__clock_gettime@PLT	#
# logwtmp.c:42:   TIMESPEC_TO_TIMEVAL (&ut.ut_tv, &ts);
	movq	(%rsp), %rax	# ts.tv_sec, ts.tv_sec
	movq	8(%rsp), %rcx	# ts.tv_nsec, ts.tv_nsec
	movabsq	$2361183241434822607, %rdx	#, tmp113
# logwtmp.c:44:   updwtmp (_PATH_WTMP, &ut);
	leaq	.LC0(%rip), %rdi	#,
	movq	%rbx, %rsi	# tmp117,
# logwtmp.c:42:   TIMESPEC_TO_TIMEVAL (&ut.ut_tv, &ts);
	movl	%eax, 356(%rsp)	# ts.tv_sec, ut.ut_tv.tv_sec
	movq	%rcx, %rax	# ts.tv_nsec, tmp119
	sarq	$63, %rcx	#, tmp115
	imulq	%rdx	# tmp113
	sarq	$7, %rdx	#, tmp114
	subq	%rcx, %rdx	# tmp115, tmp110
	movl	%edx, 360(%rsp)	# tmp110, ut.ut_tv.tv_usec
# logwtmp.c:44:   updwtmp (_PATH_WTMP, &ut);
	call	updwtmp@PLT	#
# logwtmp.c:45: }
	addq	$408, %rsp	#,
	.cfi_def_cfa_offset 40
	popq	%rbx	#
	.cfi_def_cfa_offset 32
	popq	%rbp	#
	.cfi_def_cfa_offset 24
	popq	%r12	#
	.cfi_def_cfa_offset 16
	popq	%r13	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE24:
	.size	logwtmp, .-logwtmp
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
