	.text
	.p2align 4,,15
	.type	tty_name.constprop.0, @function
tty_name.constprop.0:
	pushq	%r14
	movl	%edi, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbp
	pushq	%rbx
	movl	$4128, %ebx
	movq	(%rsi), %rbp
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L9:
	cmpq	(%r12), %rbp
	movl	$128, %ebx
	je	.L5
.L14:
	movq	%rbx, %rsi
	movq	%rbp, %rdi
	call	realloc@PLT
	testq	%rax, %rax
	je	.L13
.L10:
	movq	%rax, %rbp
.L7:
	testq	%rbx, %rbx
	je	.L9
	movq	%rbx, %rdx
	movq	%rbp, %rsi
	movl	%r14d, %edi
	call	ttyname_r@PLT
	testl	%eax, %eax
	movl	%eax, %r13d
	jne	.L3
	xorl	%esi, %esi
	movq	%rbx, %rdx
	movq	%rbp, %rdi
	call	memchr@PLT
	testq	%rax, %rax
	jne	.L4
	addq	%rbx, %rbx
	cmpq	(%r12), %rbp
	jne	.L14
.L5:
	movq	%rbx, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	jne	.L10
.L13:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %r13d
	movl	$12, %fs:(%rax)
.L3:
	cmpq	(%r12), %rbp
	je	.L1
	movq	%rbp, %rdi
	call	free@PLT
.L1:
	popq	%rbx
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	popq	%rbx
	movq	%rbp, (%r12)
	movl	%r13d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	tty_name.constprop.0, .-tty_name.constprop.0
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/dev/"
.LC1:
	.string	"/var/run/utmp"
.LC2:
	.string	"/var/log/wtmp"
	.text
	.p2align 4,,15
	.globl	login
	.type	login, @function
login:
	pushq	%r12
	pushq	%rbp
	movl	$7, %eax
	pushq	%rbx
	movq	%rdi, %rsi
	movl	$48, %ecx
	subq	$4528, %rsp
	leaq	400(%rsp), %rbp
	leaq	16(%rsp), %rbx
	leaq	8(%rsp), %r12
	movq	%rbp, 8(%rsp)
	movq	%rbx, %rdi
	rep movsq
	movw	%ax, 16(%rsp)
	call	getpid@PLT
	xorl	%edi, %edi
	movq	%r12, %rsi
	movl	%eax, 20(%rsp)
	call	tty_name.constprop.0
	testl	%eax, %eax
	js	.L16
.L19:
	movq	8(%rsp), %rdx
	leaq	.LC0(%rip), %rdi
	movl	$5, %ecx
	movq	%rdx, %rsi
	repz cmpsb
	leaq	5(%rdx), %rsi
	seta	%cl
	setb	%al
	cmpb	%al, %cl
	jne	.L28
.L21:
	leaq	8(%rbx), %rdi
	movl	$32, %edx
	call	strncpy@PLT
	leaq	.LC1(%rip), %rdi
	call	utmpname@PLT
	testl	%eax, %eax
	je	.L29
.L22:
	movq	8(%rsp), %rdi
	cmpq	%rbp, %rdi
	je	.L23
	call	free@PLT
.L23:
	leaq	.LC2(%rip), %rdi
	movq	%rbx, %rsi
	call	updwtmp@PLT
	addq	$4528, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	call	setutent@PLT
	movq	%rbx, %rdi
	call	pututline@PLT
	call	endutent@PLT
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rdx, %rdi
	call	basename@PLT
	movq	%rax, %rsi
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%r12, %rsi
	movl	$1, %edi
	call	tty_name.constprop.0
	testl	%eax, %eax
	jns	.L19
	movq	%r12, %rsi
	movl	$2, %edi
	call	tty_name.constprop.0
	testl	%eax, %eax
	jns	.L19
	pxor	%xmm0, %xmm0
	movq	$4144959, 24(%rsp)
	movq	$0, 32(%rsp)
	movups	%xmm0, 24(%rbx)
	jmp	.L23
	.size	login, .-login
