	.file	"openpty.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/login
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/login/libutil/openpty.shared.v.d
# -MF /run/asm/login/libutil/openpty.os.dt -MP
# -MT /run/asm/login/libutil/openpty.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libutil -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h openpty.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/login/libutil/openpty.shared.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.p2align 4,,15
	.type	pts_name.constprop.0, @function
pts_name.constprop.0:
.LFB17:
	.cfi_startproc
	pushq	%r14	#
	.cfi_def_cfa_offset 16
	.cfi_offset 14, -16
	movl	%edi, %r14d	# fd, fd
	pushq	%r13	#
	.cfi_def_cfa_offset 24
	.cfi_offset 13, -24
	pushq	%r12	#
	.cfi_def_cfa_offset 32
	.cfi_offset 12, -32
	movq	%rsi, %r12	# pts, pts
	pushq	%rbp	#
	.cfi_def_cfa_offset 40
	.cfi_offset 6, -40
	pushq	%rbx	#
	.cfi_def_cfa_offset 48
	.cfi_offset 3, -48
# openpty.c:38:   char *buf = *pts;
	movl	$4096, %ebx	#, buf_len
	movq	(%rsi), %rbp	# *pts_1(D), buf
	jmp	.L7	#
	.p2align 4,,10
	.p2align 3
.L9:
# openpty.c:60:       if (buf != *pts)
	cmpq	(%r12), %rbp	# *pts_1(D), buf
# openpty.c:58: 	buf_len = 128;		/* First time guess.  */
	movl	$128, %ebx	#, buf_len
# openpty.c:60:       if (buf != *pts)
	je	.L5	#,
.L14:
# openpty.c:62: 	new_buf = realloc (buf, buf_len);
	movq	%rbx, %rsi	# buf_len,
	movq	%rbp, %rdi	# buf,
	call	realloc@PLT	#
# openpty.c:65:       if (! new_buf)
	testq	%rax, %rax	# new_buf
	je	.L13	#,
.L10:
	movq	%rax, %rbp	# new_buf, buf
.L7:
# openpty.c:44:       if (buf_len)
	testq	%rbx, %rbx	# buf_len
	je	.L9	#,
# openpty.c:46: 	  rv = ptsname_r (fd, buf, buf_len);
	movq	%rbx, %rdx	# buf_len,
	movq	%rbp, %rsi	# buf,
	movl	%r14d, %edi	# fd,
	call	ptsname_r@PLT	#
# openpty.c:48: 	  if (rv != 0 || memchr (buf, '\0', buf_len))
	testl	%eax, %eax	# <retval>
# openpty.c:46: 	  rv = ptsname_r (fd, buf, buf_len);
	movl	%eax, %r13d	#, <retval>
# openpty.c:48: 	  if (rv != 0 || memchr (buf, '\0', buf_len))
	jne	.L3	#,
	xorl	%esi, %esi	#
	movq	%rbx, %rdx	# buf_len,
	movq	%rbp, %rdi	# buf,
	call	memchr@PLT	#
	testq	%rax, %rax	# _7
	jne	.L4	#,
# openpty.c:54: 	  buf_len += buf_len;	/* Double it */
	addq	%rbx, %rbx	# buf_len
# openpty.c:60:       if (buf != *pts)
	cmpq	(%r12), %rbp	# *pts_1(D), buf
	jne	.L14	#,
.L5:
# openpty.c:64: 	new_buf = malloc (buf_len);
	movq	%rbx, %rdi	# buf_len,
	call	malloc@PLT	#
# openpty.c:65:       if (! new_buf)
	testq	%rax, %rax	# new_buf
	jne	.L10	#,
.L13:
# openpty.c:68: 	  __set_errno (ENOMEM);
	movq	errno@gottpoff(%rip), %rax	#, tmp97
# openpty.c:67: 	  rv = -1;
	movl	$-1, %r13d	#, <retval>
# openpty.c:68: 	  __set_errno (ENOMEM);
	movl	$12, %fs:(%rax)	#, errno
.L3:
# openpty.c:76:   else if (buf != *pts)
	cmpq	(%r12), %rbp	# *pts_1(D), buf
	je	.L1	#,
# openpty.c:77:     free (buf);		/* Free what we malloced when returning an error.  */
	movq	%rbp, %rdi	# buf,
	call	free@PLT	#
.L1:
# openpty.c:80: }
	popq	%rbx	#
	.cfi_remember_state
	.cfi_def_cfa_offset 40
	movl	%r13d, %eax	# <retval>,
	popq	%rbp	#
	.cfi_def_cfa_offset 32
	popq	%r12	#
	.cfi_def_cfa_offset 24
	popq	%r13	#
	.cfi_def_cfa_offset 16
	popq	%r14	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	.cfi_restore_state
	popq	%rbx	#
	.cfi_def_cfa_offset 40
# openpty.c:75:     *pts = buf;		/* Return buffer to the user.  */
	movq	%rbp, (%r12)	# buf, *pts_1(D)
# openpty.c:80: }
	movl	%r13d, %eax	# <retval>,
	popq	%rbp	#
	.cfi_def_cfa_offset 32
	popq	%r12	#
	.cfi_def_cfa_offset 24
	popq	%r13	#
	.cfi_def_cfa_offset 16
	popq	%r14	#
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE17:
	.size	pts_name.constprop.0, .-pts_name.constprop.0
	.p2align 4,,15
	.globl	__GI_openpty
	.hidden	__GI_openpty
	.type	__GI_openpty, @function
__GI_openpty:
.LFB16:
	.cfi_startproc
	pushq	%r15	#
	.cfi_def_cfa_offset 16
	.cfi_offset 15, -16
	pushq	%r14	#
	.cfi_def_cfa_offset 24
	.cfi_offset 14, -24
	pushq	%r13	#
	.cfi_def_cfa_offset 32
	.cfi_offset 13, -32
	pushq	%r12	#
	.cfi_def_cfa_offset 40
	.cfi_offset 12, -40
	movq	%rdx, %r13	# name, name
	pushq	%rbp	#
	.cfi_def_cfa_offset 48
	.cfi_offset 6, -48
	pushq	%rbx	#
	.cfi_def_cfa_offset 56
	.cfi_offset 3, -56
	movq	%rcx, %r12	# termp, termp
	movq	%r8, %rbp	# winp, winp
	subq	$4152, %rsp	#,
	.cfi_def_cfa_offset 4208
# openpty.c:94:   char *buf = _buf;
	leaq	48(%rsp), %rbx	#, tmp107
# openpty.c:88: {
	movq	%rdi, 8(%rsp)	# amaster, %sfp
	movq	%rsi, 16(%rsp)	# aslave, %sfp
# openpty.c:97:   *buf = '\0';
	movb	$0, 48(%rsp)	#, MEM[(char *)&_buf]
# openpty.c:94:   char *buf = _buf;
	movq	%rbx, 40(%rsp)	# tmp107, buf
# openpty.c:99:   master = getpt ();
	call	getpt@PLT	#
# openpty.c:100:   if (master == -1)
	cmpl	$-1, %eax	#, master
# openpty.c:99:   master = getpt ();
	movl	%eax, %r15d	#, master
# openpty.c:100:   if (master == -1)
	je	.L27	#,
# openpty.c:103:   if (grantpt (master))
	movl	%eax, %edi	# master,
	call	grantpt@PLT	#
	testl	%eax, %eax	# _1
# openpty.c:149:     close (master);
	movl	%r15d, %edi	# master,
# openpty.c:103:   if (grantpt (master))
	je	.L39	#,
.L38:
# openpty.c:152:       close (slave);
	call	close@PLT	#
	movq	40(%rsp), %rbp	# buf, pretmp_47
	movl	$-1, %r14d	#, <retval>
.L24:
# openpty.c:155:   if (buf != _buf)
	cmpq	%rbx, %rbp	# tmp107, pretmp_47
	je	.L15	#,
# openpty.c:156:     free (buf);
	movq	%rbp, %rdi	# pretmp_47,
	call	free@PLT	#
.L15:
# openpty.c:159: }
	addq	$4152, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 56
	movl	%r14d, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 48
	popq	%rbp	#
	.cfi_def_cfa_offset 40
	popq	%r12	#
	.cfi_def_cfa_offset 32
	popq	%r13	#
	.cfi_def_cfa_offset 24
	popq	%r14	#
	.cfi_def_cfa_offset 16
	popq	%r15	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L39:
	.cfi_restore_state
# openpty.c:106:   if (unlockpt (master))
	call	unlockpt@PLT	#
	testl	%eax, %eax	# <retval>
	movl	%eax, %r14d	#, <retval>
	je	.L40	#,
# openpty.c:149:     close (master);
	movl	%r15d, %edi	# master,
	movl	$-1, %r14d	#, <retval>
	call	close@PLT	#
	movq	40(%rsp), %rbp	# buf, pretmp_47
	jmp	.L24	#
	.p2align 4,,10
	.p2align 3
.L40:
# openpty.c:111:   slave = ioctl (master, TIOCGPTPEER, O_RDWR | O_NOCTTY);
	xorl	%eax, %eax	#
	movl	$258, %edx	#,
	movl	$21569, %esi	#,
	movl	%r15d, %edi	# master,
	call	ioctl@PLT	#
# openpty.c:113:   if (slave == -1)
	cmpl	$-1, %eax	#, slave
# openpty.c:111:   slave = ioctl (master, TIOCGPTPEER, O_RDWR | O_NOCTTY);
	movl	%eax, 28(%rsp)	# slave, %sfp
# openpty.c:113:   if (slave == -1)
	je	.L41	#,
.L19:
# openpty.c:127:   if (termp)
	testq	%r12, %r12	# termp
	je	.L22	#,
# openpty.c:128:     tcsetattr (slave, TCSAFLUSH, termp);
	movl	28(%rsp), %edi	# %sfp,
	movq	%r12, %rdx	# termp,
	movl	$2, %esi	#,
	call	tcsetattr@PLT	#
.L22:
# openpty.c:130:   if (winp)
	testq	%rbp, %rbp	# winp
	je	.L23	#,
# openpty.c:131:     ioctl (slave, TIOCSWINSZ, winp);
	movl	28(%rsp), %edi	# %sfp,
	movq	%rbp, %rdx	# winp,
	movl	$21524, %esi	#,
	xorl	%eax, %eax	#
	call	ioctl@PLT	#
.L23:
# openpty.c:134:   *amaster = master;
	movq	8(%rsp), %rax	# %sfp, amaster
# openpty.c:135:   *aslave = slave;
	movl	28(%rsp), %ecx	# %sfp, slave
# openpty.c:136:   if (name != NULL)
	testq	%r13, %r13	# name
	movq	40(%rsp), %rbp	# buf, pretmp_47
# openpty.c:134:   *amaster = master;
	movl	%r15d, (%rax)	# master, *amaster_34(D)
# openpty.c:135:   *aslave = slave;
	movq	16(%rsp), %rax	# %sfp, aslave
	movl	%ecx, (%rax)	# slave, *aslave_36(D)
# openpty.c:136:   if (name != NULL)
	je	.L24	#,
# openpty.c:138:       if (*buf == '\0')
	cmpb	$0, 0(%rbp)	#, *pretmp_69
	jne	.L25	#,
# openpty.c:139:         if (pts_name (master, &buf, sizeof (_buf)))
	leaq	40(%rsp), %rsi	#, tmp104
	movl	%r15d, %edi	# master,
	call	pts_name.constprop.0	#
	testl	%eax, %eax	# _7
	jne	.L26	#,
	movq	40(%rsp), %rbp	# buf, pretmp_47
.L25:
# openpty.c:142:       strcpy (name, buf);
	movq	%rbp, %rsi	# pretmp_47,
	movq	%r13, %rdi	# name,
	call	strcpy@PLT	#
	jmp	.L24	#
	.p2align 4,,10
	.p2align 3
.L26:
# openpty.c:149:     close (master);
	movl	%r15d, %edi	# master,
	call	close@PLT	#
# openpty.c:152:       close (slave);
	movl	28(%rsp), %edi	# %sfp,
	jmp	.L38	#
	.p2align 4,,10
	.p2align 3
.L41:
# openpty.c:118:       if (pts_name (master, &buf, sizeof (_buf)))
	leaq	40(%rsp), %rsi	#, tmp102
	movl	%r15d, %edi	# master,
	call	pts_name.constprop.0	#
	testl	%eax, %eax	# _3
	je	.L42	#,
.L21:
# openpty.c:149:     close (master);
	movl	%r15d, %edi	# master,
	call	close@PLT	#
	movq	40(%rsp), %rbp	# buf, pretmp_47
	movl	28(%rsp), %r14d	# %sfp, <retval>
	jmp	.L24	#
	.p2align 4,,10
	.p2align 3
.L27:
# openpty.c:101:     return -1;
	movl	%eax, %r14d	# master, <retval>
	jmp	.L15	#
	.p2align 4,,10
	.p2align 3
.L42:
# openpty.c:121:       slave = open (buf, O_RDWR | O_NOCTTY);
	movq	40(%rsp), %rdi	# buf,
	movl	$258, %esi	#,
	call	open@PLT	#
# openpty.c:122:       if (slave == -1)
	cmpl	$-1, %eax	#, slave
# openpty.c:121:       slave = open (buf, O_RDWR | O_NOCTTY);
	movl	%eax, 28(%rsp)	# slave, %sfp
# openpty.c:122:       if (slave == -1)
	jne	.L19	#,
# openpty.c:147:  on_error:
	jmp	.L21	#
	.cfi_endproc
.LFE16:
	.size	__GI_openpty, .-__GI_openpty
	.globl	openpty
	.set	openpty,__GI_openpty
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
