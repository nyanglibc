	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/run/utmp"
	.text
	.p2align 4,,15
	.globl	logout
	.type	logout, @function
logout:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	leaq	.LC0(%rip), %rdi
	xorl	%ebx, %ebx
	subq	$800, %rsp
	call	utmpname@PLT
	cmpl	$-1, %eax
	je	.L1
	call	setutent@PLT
	movl	$7, %edx
	leaq	40(%rsp), %rdi
	movq	%r12, %rsi
	movw	%dx, 32(%rsp)
	leaq	32(%rsp), %rbp
	movl	$32, %edx
	call	strncpy@PLT
	leaq	8(%rsp), %rdx
	leaq	416(%rsp), %rsi
	movq	%rbp, %rdi
	call	getutline_r@PLT
	testl	%eax, %eax
	jns	.L9
.L3:
	call	endutent@PLT
.L1:
	addq	$800, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	8(%rsp), %rax
	pxor	%xmm0, %xmm0
	leaq	16(%rsp), %rsi
	xorl	%ebx, %ebx
	leaq	84(%rax), %rdi
	movq	$0, 76(%rax)
	movq	$0, 324(%rax)
	movups	%xmm0, 44(%rax)
	andq	$-8, %rdi
	movups	%xmm0, 60(%rax)
	subl	%edi, %eax
	leal	332(%rax), %ecx
	xorl	%eax, %eax
	shrl	$3, %ecx
	rep stosq
	xorl	%edi, %edi
	call	__clock_gettime@PLT
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rax
	movabsq	$2361183241434822607, %rdx
	movq	24(%rsp), %rcx
	movl	%eax, 340(%rdi)
	movq	%rcx, %rax
	sarq	$63, %rcx
	imulq	%rdx
	movl	$8, %eax
	movw	%ax, (%rdi)
	sarq	$7, %rdx
	subq	%rcx, %rdx
	movl	%edx, 344(%rdi)
	call	pututline@PLT
	testq	%rax, %rax
	setne	%bl
	jmp	.L3
	.size	logout, .-logout
