	.text
	.p2align 4,,15
	.globl	forkpty
	.type	forkpty, @function
forkpty:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rax
	movq	%rdi, %rbp
	movq	%rcx, %r8
	movq	%rdx, %rcx
	subq	$24, %rsp
	movq	%rax, %rdx
	leaq	12(%rsp), %rsi
	leaq	8(%rsp), %rdi
	call	__GI_openpty
	cmpl	$-1, %eax
	movl	%eax, %ebx
	je	.L1
	call	fork@PLT
	cmpl	$-1, %eax
	movl	%eax, %ebx
	je	.L4
	testl	%eax, %eax
	jne	.L15
	movl	8(%rsp), %edi
	call	close@PLT
	movl	12(%rsp), %edi
	call	__GI_login_tty
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L16
.L1:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movl	8(%rsp), %eax
	movl	12(%rsp), %edi
	movl	%eax, 0(%rbp)
	call	close@PLT
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	8(%rsp), %edi
	call	close@PLT
	movl	12(%rsp), %edi
	call	close@PLT
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	ret
.L16:
	movl	$1, %edi
	call	_exit@PLT
	.size	forkpty, .-forkpty
