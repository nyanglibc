	.file	"logout.c"
# GNU C11 (GCC) version 7.3.0 (x86_64-nyan-linux-gnu)
#	compiled by GNU C version 7.3.0, GMP version 6.1.2, MPFR version 4.0.1, MPC version 1.1.0, isl version none
# GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
# options passed:  -I ../include
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build/login
# -I /root/wip/nyanglibc/builds/0/nyanglibc/build
# -I ../sysdeps/unix/sysv/linux/x86_64/64
# -I ../sysdeps/unix/sysv/linux/x86_64
# -I ../sysdeps/unix/sysv/linux/x86/include
# -I ../sysdeps/unix/sysv/linux/x86 -I ../sysdeps/x86/nptl
# -I ../sysdeps/unix/sysv/linux/wordsize-64 -I ../sysdeps/x86_64/nptl
# -I ../sysdeps/unix/sysv/linux/include -I ../sysdeps/unix/sysv/linux
# -I ../sysdeps/nptl -I ../sysdeps/pthread -I ../sysdeps/gnu
# -I ../sysdeps/unix/inet -I ../sysdeps/unix/sysv -I ../sysdeps/unix/x86_64
# -I ../sysdeps/unix -I ../sysdeps/posix -I ../sysdeps/x86_64/64
# -I ../sysdeps/x86_64/fpu -I ../sysdeps/x86/fpu -I ../sysdeps/x86_64
# -I ../sysdeps/x86/include -I ../sysdeps/x86
# -I ../sysdeps/ieee754/float128 -I ../sysdeps/ieee754/ldbl-96/include
# -I ../sysdeps/ieee754/ldbl-96 -I ../sysdeps/ieee754/dbl-64
# -I ../sysdeps/ieee754/flt-32 -I ../sysdeps/wordsize-64
# -I ../sysdeps/ieee754 -I ../sysdeps/generic -I .. -I ../libio -I .
# -MD /run/asm/login/libutil/logout.shared.v.d
# -MF /run/asm/login/libutil/logout.os.dt -MP
# -MT /run/asm/login/libutil/logout.os -D _LIBC_REENTRANT
# -D MODULE_NAME=libutil -D PIC -D SHARED -D TOP_NAMESPACE=glibc
# -include /root/wip/nyanglibc/builds/0/nyanglibc/build/libc-modules.h
# -include ../include/libc-symbols.h logout.c -mtune=generic -march=x86-64
# -auxbase-strip /run/asm/login/libutil/logout.shared.v.s -O2 -Wall
# -Wwrite-strings -Wundef -Werror -Wstrict-prototypes
# -Wold-style-definition -std=gnu11 -fverbose-asm -fgnu89-inline
# -fmerge-all-constants -frounding-math -fno-stack-protector -fmath-errno
# -fPIC -ftls-model=initial-exec
# options enabled:  -fPIC -faggressive-loop-optimizations -falign-labels
# -fasynchronous-unwind-tables -fauto-inc-dec -fbranch-count-reg
# -fcaller-saves -fchkp-check-incomplete-type -fchkp-check-read
# -fchkp-check-write -fchkp-instrument-calls -fchkp-narrow-bounds
# -fchkp-optimize -fchkp-store-bounds -fchkp-use-static-bounds
# -fchkp-use-static-const-bounds -fchkp-use-wrappers -fcode-hoisting
# -fcombine-stack-adjustments -fcommon -fcompare-elim -fcprop-registers
# -fcrossjumping -fcse-follow-jumps -fdefer-pop
# -fdelete-null-pointer-checks -fdevirtualize -fdevirtualize-speculatively
# -fdwarf2-cfi-asm -fearly-inlining -feliminate-unused-debug-types
# -fexpensive-optimizations -fforward-propagate -ffp-int-builtin-inexact
# -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime -fgnu-unique
# -fguess-branch-probability -fhoist-adjacent-loads -fident -fif-conversion
# -fif-conversion2 -findirect-inlining -finline -finline-atomics
# -finline-functions-called-once -finline-small-functions -fipa-bit-cp
# -fipa-cp -fipa-icf -fipa-icf-functions -fipa-icf-variables -fipa-profile
# -fipa-pure-const -fipa-ra -fipa-reference -fipa-sra -fipa-vrp
# -fira-hoist-pressure -fira-share-save-slots -fira-share-spill-slots
# -fisolate-erroneous-paths-dereference -fivopts -fkeep-static-consts
# -fleading-underscore -flifetime-dse -flra-remat -flto-odr-type-merging
# -fmath-errno -fmerge-all-constants -fmerge-debug-strings
# -fmove-loop-invariants -fomit-frame-pointer -foptimize-sibling-calls
# -foptimize-strlen -fpartial-inlining -fpeephole -fpeephole2 -fplt
# -fprefetch-loop-arrays -free -freg-struct-return -freorder-blocks
# -freorder-functions -frerun-cse-after-loop -frounding-math
# -fsched-critical-path-heuristic -fsched-dep-count-heuristic
# -fsched-group-heuristic -fsched-interblock -fsched-last-insn-heuristic
# -fsched-rank-heuristic -fsched-spec -fsched-spec-insn-heuristic
# -fsched-stalled-insns-dep -fschedule-fusion -fschedule-insns2
# -fsemantic-interposition -fshow-column -fshrink-wrap
# -fshrink-wrap-separate -fsigned-zeros -fsplit-ivs-in-unroller
# -fsplit-wide-types -fssa-backprop -fssa-phiopt -fstdarg-opt
# -fstore-merging -fstrict-aliasing -fstrict-overflow
# -fstrict-volatile-bitfields -fsync-libcalls -fthread-jumps
# -ftoplevel-reorder -ftrapping-math -ftree-bit-ccp -ftree-builtin-call-dce
# -ftree-ccp -ftree-ch -ftree-coalesce-vars -ftree-copy-prop -ftree-cselim
# -ftree-dce -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
# -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
# -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop -ftree-pre
# -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink -ftree-slsr
# -ftree-sra -ftree-switch-conversion -ftree-tail-merge -ftree-ter
# -ftree-vrp -funit-at-a-time -funwind-tables -fverbose-asm
# -fzero-initialized-in-bss -m128bit-long-double -m64 -m80387
# -malign-stringops -mavx256-split-unaligned-load
# -mavx256-split-unaligned-store -mfancy-math-387 -mfp-ret-in-387 -mfxsr
# -mglibc -mieee-fp -mlong-double-80 -mmmx -mno-sse4 -mpush-args -mred-zone
# -msse -msse2 -mstv -mtls-direct-seg-refs -mvzeroupper

	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/var/run/utmp"
	.text
	.p2align 4,,15
	.globl	logout
	.type	logout, @function
logout:
.LFB24:
	.cfi_startproc
	pushq	%r12	#
	.cfi_def_cfa_offset 16
	.cfi_offset 12, -16
	pushq	%rbp	#
	.cfi_def_cfa_offset 24
	.cfi_offset 6, -24
	movq	%rdi, %r12	# line, line
	pushq	%rbx	#
	.cfi_def_cfa_offset 32
	.cfi_offset 3, -32
# logout.c:33:   if (utmpname (_PATH_UTMP) == -1)
	leaq	.LC0(%rip), %rdi	#,
# logout.c:34:     return 0;
	xorl	%ebx, %ebx	# <retval>
# logout.c:27: {
	subq	$800, %rsp	#,
	.cfi_def_cfa_offset 832
# logout.c:33:   if (utmpname (_PATH_UTMP) == -1)
	call	utmpname@PLT	#
	cmpl	$-1, %eax	#, _1
	je	.L1	#,
# logout.c:37:   setutent ();
	call	setutent@PLT	#
# logout.c:40:   tmp.ut_type = USER_PROCESS;
	movl	$7, %edx	#,
# logout.c:41:   strncpy (tmp.ut_line, line, sizeof tmp.ut_line);
	leaq	40(%rsp), %rdi	#, tmp103
	movq	%r12, %rsi	# line,
# logout.c:40:   tmp.ut_type = USER_PROCESS;
	movw	%dx, 32(%rsp)	#, tmp.ut_type
# logout.c:41:   strncpy (tmp.ut_line, line, sizeof tmp.ut_line);
	leaq	32(%rsp), %rbp	#, tmp102
	movl	$32, %edx	#,
	call	strncpy@PLT	#
# logout.c:44:   if (getutline_r (&tmp, &utbuf, &ut) >= 0)
	leaq	8(%rsp), %rdx	#, tmp104
	leaq	416(%rsp), %rsi	#, tmp105
	movq	%rbp, %rdi	# tmp102,
	call	getutline_r@PLT	#
	testl	%eax, %eax	# _2
	jns	.L9	#,
.L3:
# logout.c:60:   endutent ();
	call	endutent@PLT	#
.L1:
# logout.c:63: }
	addq	$800, %rsp	#,
	.cfi_remember_state
	.cfi_def_cfa_offset 32
	movl	%ebx, %eax	# <retval>,
	popq	%rbx	#
	.cfi_def_cfa_offset 24
	popq	%rbp	#
	.cfi_def_cfa_offset 16
	popq	%r12	#
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	.cfi_restore_state
# logout.c:47:       memset (ut->ut_name, '\0', sizeof ut->ut_name);
	movq	8(%rsp), %rax	# ut, ut.0_3
	pxor	%xmm0, %xmm0	# tmp131
# logout.c:51:       __clock_gettime64 (CLOCK_REALTIME, &ts);
	leaq	16(%rsp), %rsi	#, tmp120
# logout.c:55:       if (pututline (ut) != NULL)
	xorl	%ebx, %ebx	# <retval>
# logout.c:48:       memset (ut->ut_host, '\0', sizeof ut->ut_host);
	leaq	84(%rax), %rdi	#, tmp117
	movq	$0, 76(%rax)	#,
	movq	$0, 324(%rax)	#,
# logout.c:47:       memset (ut->ut_name, '\0', sizeof ut->ut_name);
	movups	%xmm0, 44(%rax)	# tmp131, MEM[(void *)ut.0_3 + 44B]
# logout.c:48:       memset (ut->ut_host, '\0', sizeof ut->ut_host);
	andq	$-8, %rdi	#, tmp117
# logout.c:47:       memset (ut->ut_name, '\0', sizeof ut->ut_name);
	movups	%xmm0, 60(%rax)	# tmp131, MEM[(void *)ut.0_3 + 44B]
# logout.c:48:       memset (ut->ut_host, '\0', sizeof ut->ut_host);
	subl	%edi, %eax	# tmp117, D.2946
	leal	332(%rax), %ecx	#, tmp112
	xorl	%eax, %eax	# tmp113
	shrl	$3, %ecx	#,
	rep stosq
# logout.c:51:       __clock_gettime64 (CLOCK_REALTIME, &ts);
	xorl	%edi, %edi	#
	call	__clock_gettime@PLT	#
# logout.c:52:       TIMESPEC_TO_TIMEVAL (&ut->ut_tv, &ts);
	movq	8(%rsp), %rdi	# ut, ut.2_7
	movq	16(%rsp), %rax	# ts.tv_sec, ts.tv_sec
	movabsq	$2361183241434822607, %rdx	#, tmp125
	movq	24(%rsp), %rcx	# ts.tv_nsec, ts.tv_nsec
	movl	%eax, 340(%rdi)	# ts.tv_sec, ut.2_7->ut_tv.tv_sec
	movq	%rcx, %rax	# ts.tv_nsec, tmp133
	sarq	$63, %rcx	#, tmp127
	imulq	%rdx	# tmp125
# logout.c:53:       ut->ut_type = DEAD_PROCESS;
	movl	$8, %eax	#,
	movw	%ax, (%rdi)	#, ut.2_7->ut_type
# logout.c:52:       TIMESPEC_TO_TIMEVAL (&ut->ut_tv, &ts);
	sarq	$7, %rdx	#, tmp126
	subq	%rcx, %rdx	# tmp127, tmp122
	movl	%edx, 344(%rdi)	# tmp122, ut.2_7->ut_tv.tv_usec
# logout.c:55:       if (pututline (ut) != NULL)
	call	pututline@PLT	#
	testq	%rax, %rax	# _12
	setne	%bl	#, <retval>
	jmp	.L3	#
	.cfi_endproc
.LFE24:
	.size	logout, .-logout
	.ident	"GCC: (GNU) 7.3.0"
	.section	.note.GNU-stack,"",@progbits
