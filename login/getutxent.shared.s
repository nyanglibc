	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getutxent
	.type	getutxent, @function
getutxent:
	jmp	__GI___getutent
	.size	getutxent, .-getutxent
