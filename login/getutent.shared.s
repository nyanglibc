	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.section __libc_freeres_ptrs, "aw", %nobits
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__GI___getutent
	.hidden	__GI___getutent
	.type	__GI___getutent, @function
__GI___getutent:
	subq	$24, %rsp
	movq	buffer(%rip), %rdi
	testq	%rdi, %rdi
	je	.L7
.L2:
	leaq	8(%rsp), %rsi
	call	__GI___getutent_r
	testl	%eax, %eax
	js	.L4
	movq	8(%rsp), %rax
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$384, %edi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	%rax, buffer(%rip)
	jne	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	jmp	.L1
	.size	__GI___getutent, .-__GI___getutent
	.globl	__getutent
	.set	__getutent,__GI___getutent
	.weak	getutent
	.set	getutent,__getutent
	.section	__libc_freeres_ptrs
	#,"aw",@progbits
	.align 8
	.type	buffer, @object
	.size	buffer, 8
buffer:
	.zero	8
