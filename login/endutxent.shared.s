	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	endutxent
	.type	endutxent, @function
endutxent:
	jmp	__endutent
	.size	endutxent, .-endutxent
	.hidden	__endutent
