	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getutxid
	.type	getutxid, @function
getutxid:
	jmp	__GI___getutid
	.size	getutxid, .-getutxid
