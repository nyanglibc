	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	grantpt
	.type	grantpt, @function
grantpt:
	subq	$24, %rsp
	xorl	%eax, %eax
	movl	$2147767344, %esi
	leaq	12(%rsp), %rdx
	call	__GI___ioctl
	testl	%eax, %eax
	je	.L1
	movq	__libc_errno@gottpoff(%rip), %rdx
	cmpl	$25, %fs:(%rdx)
	je	.L8
.L1:
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$22, %fs:(%rdx)
	addq	$24, %rsp
	ret
	.size	grantpt, .-grantpt
