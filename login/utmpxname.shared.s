	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	utmpxname
	.type	utmpxname, @function
utmpxname:
	jmp	__utmpname
	.size	utmpxname, .-utmpxname
	.hidden	__utmpname
