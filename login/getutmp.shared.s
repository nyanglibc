	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getutmp
	.type	getutmp, @function
getutmp:
	movq	(%rdi), %rdx
	movq	%rdi, %rax
	movq	%rdx, (%rsi)
	movq	376(%rdi), %rdx
	leaq	8(%rsi), %rdi
	andq	$-8, %rdi
	movq	%rdx, 376(%rsi)
	subq	%rdi, %rsi
	movq	%rsi, %rcx
	subq	%rsi, %rax
	addl	$384, %ecx
	movq	%rax, %rsi
	shrl	$3, %ecx
	rep movsq
	ret
	.size	getutmp, .-getutmp
	.globl	getutmpx
	.set	getutmpx,getutmp
