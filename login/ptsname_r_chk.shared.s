	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__ptsname_r_chk
	.type	__ptsname_r_chk, @function
__ptsname_r_chk:
	cmpq	%rcx, %rdx
	ja	.L7
	jmp	__ptsname_r
.L7:
	subq	$8, %rsp
	call	__GI___chk_fail
	.size	__ptsname_r_chk, .-__ptsname_r_chk
	.hidden	__ptsname_r
