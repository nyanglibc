	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	__setutent.part.0, @function
__setutent.part.0:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__libc_utmp_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 37 "getutent_r.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	ret
	.size	__setutent.part.0, .-__setutent.part.0
	.set	__endutent.part.3,__setutent.part.0
	.set	__GI___pututline.part.2,__setutent.part.0
	.set	__GI___getutent_r.part.1,__setutent.part.0
	.p2align 4,,15
	.globl	__setutent
	.hidden	__setutent
	.type	__setutent, @function
__setutent:
	subq	$8, %rsp
#APP
# 33 "getutent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L5:
	call	__libc_setutent
#APP
# 37 "getutent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L6
	subl	$1, __libc_utmp_lock(%rip)
.L3:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __libc_utmp_lock(%rip)
	je	.L5
	leaq	__libc_utmp_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
#APP
# 37 "getutent_r.c" 1
	xchgl %eax, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L3
	addq	$8, %rsp
	jmp	__setutent.part.0
	.size	__setutent, .-__setutent
	.weak	setutent
	.set	setutent,__setutent
	.p2align 4,,15
	.globl	__GI___getutent_r
	.hidden	__GI___getutent_r
	.type	__GI___getutent_r, @function
__GI___getutent_r:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
#APP
# 47 "getutent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L11:
	movq	%rbx, %rdi
	call	__libc_getutent_r
#APP
# 51 "getutent_r.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L12
	subl	$1, __libc_utmp_lock(%rip)
.L9:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __libc_utmp_lock(%rip)
	je	.L11
	leaq	__libc_utmp_lock(%rip), %rdi
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rsi
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%edx, %edx
#APP
# 51 "getutent_r.c" 1
	xchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L9
	movl	%eax, 8(%rsp)
	call	__GI___getutent_r.part.1
	movl	8(%rsp), %eax
	jmp	.L9
	.size	__GI___getutent_r, .-__GI___getutent_r
	.globl	__getutent_r
	.set	__getutent_r,__GI___getutent_r
	.weak	getutent_r
	.set	getutent_r,__getutent_r
	.p2align 4,,15
	.globl	__GI___pututline
	.hidden	__GI___pututline
	.type	__GI___pututline, @function
__GI___pututline:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
#APP
# 64 "getutent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L16
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L17:
	movq	%rbx, %rdi
	call	__libc_pututline
#APP
# 68 "getutent_r.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L18
	subl	$1, __libc_utmp_lock(%rip)
.L15:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __libc_utmp_lock(%rip)
	je	.L17
	leaq	__libc_utmp_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%edx, %edx
#APP
# 68 "getutent_r.c" 1
	xchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L15
	movq	%rax, 8(%rsp)
	call	__GI___pututline.part.2
	movq	8(%rsp), %rax
	jmp	.L15
	.size	__GI___pututline, .-__GI___pututline
	.globl	__pututline
	.set	__pututline,__GI___pututline
	.weak	pututline
	.set	pututline,__pututline
	.p2align 4,,15
	.globl	__endutent
	.hidden	__endutent
	.type	__endutent, @function
__endutent:
	subq	$8, %rsp
#APP
# 79 "getutent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L22
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L23:
	call	__libc_endutent
#APP
# 83 "getutent_r.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L24
	subl	$1, __libc_utmp_lock(%rip)
.L21:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __libc_utmp_lock(%rip)
	je	.L23
	leaq	__libc_utmp_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
#APP
# 83 "getutent_r.c" 1
	xchgl %eax, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L21
	addq	$8, %rsp
	jmp	__endutent.part.3
	.size	__endutent, .-__endutent
	.weak	endutent
	.set	endutent,__endutent
	.hidden	__libc_utmp_lock
	.comm	__libc_utmp_lock,4,4
	.hidden	__libc_endutent
	.hidden	__libc_pututline
	.hidden	__libc_getutent_r
	.hidden	__lll_lock_wait_private
	.hidden	__libc_setutent
