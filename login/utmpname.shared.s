	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__utmpname
	.hidden	__utmpname
	.type	__utmpname, @function
__utmpname:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
#APP
# 42 "utmpname.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	call	__libc_endutent
	movq	__libc_utmp_file_name(%rip), %r12
	movq	%rbp, %rdi
	movq	%r12, %rsi
	call	__GI_strcmp
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L4
	leaq	default_file_name(%rip), %rsi
	movq	%rbp, %rdi
	call	__GI_strcmp
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L15
	movq	%rbp, %rdi
	call	__GI___strdup
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L9
	movq	__libc_utmp_file_name(%rip), %rdi
	leaq	default_file_name(%rip), %rax
	cmpq	%rax, %rdi
	je	.L6
	call	free@PLT
.L6:
	movq	%rbx, __libc_utmp_file_name(%rip)
	xorl	%ebx, %ebx
.L4:
#APP
# 72 "utmpname.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L7
	subl	$1, __libc_utmp_lock(%rip)
.L1:
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	movq	%r12, %rdi
	call	free@PLT
	leaq	default_file_name(%rip), %rax
	movq	%rax, __libc_utmp_file_name(%rip)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
#APP
# 72 "utmpname.c" 1
	xchgl %eax, __libc_utmp_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__libc_utmp_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 72 "utmpname.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __libc_utmp_lock(%rip)
	je	.L3
	leaq	__libc_utmp_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
.L9:
	movl	$-1, %ebx
	jmp	.L4
	.size	__utmpname, .-__utmpname
	.weak	utmpname
	.set	utmpname,__utmpname
	.hidden	__libc_utmp_file_name
	.globl	__libc_utmp_file_name
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	__libc_utmp_file_name, @object
	.size	__libc_utmp_file_name, 8
__libc_utmp_file_name:
	.quad	default_file_name
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	default_file_name, @object
	.size	default_file_name, 14
default_file_name:
	.string	"/var/run/utmp"
	.hidden	__lll_lock_wait_private
	.hidden	__libc_endutent
	.hidden	__libc_utmp_lock
