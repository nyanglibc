	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"../nptl/pthread_mutex_lock.c"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"e != EDEADLK || (kind != PTHREAD_MUTEX_ERRORCHECK_NP && kind != PTHREAD_MUTEX_RECURSIVE_NP)"
	.section	.rodata.str1.1
.LC3:
	.string	"e != ESRCH || !robust"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"robust || (oldval & FUTEX_OWNER_DIED) == 0"
	.section	.rodata.str1.1
.LC5:
	.string	"mutex->__data.__owner == 0"
	.text
	.p2align 4,,15
	.type	__pthread_mutex_lock_full, @function
__pthread_mutex_lock_full:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
#APP
# 170 "../nptl/pthread_mutex_lock.c" 1
	movl %fs:720,%r13d
# 0 "" 2
#NO_APP
	movl	16(%rdi), %ecx
	andl	$127, %ecx
	subl	$16, %ecx
	cmpl	$51, %ecx
	ja	.L82
	movl	$1, %eax
	movabsq	$64425492480, %rdx
	movq	%rdi, %rbx
	salq	%cl, %rax
	testq	%rdx, %rax
	jne	.L4
	movabsq	$4222124650659840, %rdx
	leaq	16(%rdi), %r14
	testq	%rdx, %rax
	jne	.L5
	testb	$15, %al
	je	.L82
	leaq	32(%rdi), %r8
#APP
# 178 "../nptl/pthread_mutex_lock.c" 1
	movq %r8,%fs:752
# 0 "" 2
#NO_APP
	movl	(%rdi), %ecx
	xorl	%esi, %esi
	movl	$202, %r12d
	movl	$-2147483648, %ebp
	movl	$1, %r9d
.L7:
	testl	%ecx, %ecx
	jne	.L8
	movl	%r13d, %edx
	movl	%ecx, %eax
	orl	%esi, %edx
	lock cmpxchgl	%edx, (%rbx)
	testl	%eax, %eax
	movl	%eax, %ecx
	jne	.L8
	cmpl	$2147483646, 8(%rbx)
	je	.L86
	movl	$1, 4(%rbx)
#APP
# 333 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	andq	$-2, %rax
	movq	%r8, -8(%rax)
#APP
# 333 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 333 "../nptl/pthread_mutex_lock.c" 1
	movq %r8,%fs:736
# 0 "" 2
# 336 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
.L21:
	movl	%r13d, 8(%rbx)
	addl	$1, 12(%rbx)
	addq	$24, %rsp
	popq	%rbx
	xorl	%eax, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L88:
	cmpl	$-1, %ebp
	je	.L82
	orl	$-1, %esi
	movl	%ebp, %edi
	call	__pthread_tpp_change_priority
.L82:
	movl	$22, %eax
.L1:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L5:
	movl	16(%rdi), %eax
	cmpl	8(%rdi), %r13d
	movl	$-1, %ebp
	movl	(%rdi), %ecx
	je	.L87
.L39:
	movl	$202, %r15d
.L42:
	shrl	$19, %ecx
	movl	%ecx, %r12d
	call	__pthread_current_priority
	cmpl	%r12d, %eax
	jg	.L88
	movl	%r12d, %esi
	movl	%ebp, %edi
	call	__pthread_tpp_change_priority
	testl	%eax, %eax
	jne	.L1
	movl	%r12d, %r8d
	sall	$19, %r8d
	movl	%r8d, %ebp
	movl	%r8d, %eax
	orl	$1, %ebp
	lock cmpxchgl	%ebp, (%rbx)
	cmpl	%eax, %r8d
	je	.L41
	movl	%r8d, %r9d
	orl	$2, %r9d
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%r8d, %eax
	lock cmpxchgl	%r9d, (%rbx)
	cmpl	%r8d, %eax
	je	.L41
.L44:
	movl	%ebp, %eax
	lock cmpxchgl	%r9d, (%rbx)
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-524288, %edx
	cmpl	%edx, %r8d
	jne	.L52
	cmpl	%eax, %r8d
	je	.L43
	movl	(%r14), %esi
	xorl	%r10d, %r10d
	movl	%r9d, %edx
	movq	%rbx, %rdi
	movl	%r15d, %eax
	notl	%esi
	andl	$128, %esi
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L43
	addl	$11, %eax
	cmpl	$11, %eax
	ja	.L18
	movl	%eax, %ecx
	movl	$1, %edi
	salq	%cl, %rdi
	movq	%rdi, %rax
	testl	$2177, %eax
	jne	.L43
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L52:
	movl	%r12d, %ebp
	jmp	.L42
.L41:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L89
.L35:
	movl	$1, 4(%rbx)
	jmp	.L21
.L4:
	movl	16(%rdi), %r8d
	movl	%r8d, %r9d
	andl	$3, %r9d
	andl	$16, %r8d
	jne	.L90
.L22:
	movl	(%rbx), %eax
	andl	$1073741823, %eax
	cmpl	%eax, %r13d
	je	.L91
.L23:
	xorl	%eax, %eax
	lock cmpxchgl	%r13d, (%rbx)
	testl	%eax, %eax
	je	.L26
	testl	%r8d, %r8d
	movl	$6, %esi
	movl	$128, %ebp
	jne	.L27
	movl	16(%rbx), %ebp
	andl	$128, %ebp
	movl	%ebp, %esi
	xorb	$-122, %sil
.L27:
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 257 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-11, %eax
	je	.L28
	jg	.L29
	cmpl	$-35, %eax
	je	.L28
	cmpl	$-22, %eax
	je	.L28
	cmpl	$-110, %eax
	je	.L28
.L18:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L26:
	testl	%r8d, %r8d
	je	.L35
.L46:
	cmpl	$2147483646, 8(%rbx)
	je	.L92
	movl	$1, 4(%rbx)
#APP
# 488 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	leaq	32(%rbx), %rax
	andq	$-2, %rdx
	movq	%rax, -8(%rdx)
#APP
# 488 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 32(%rbx)
	movq	%fs:16, %rdi
	leaq	736(%rdi), %rdx
	movq	%rdx, 24(%rbx)
	orq	$1, %rax
#APP
# 488 "../nptl/pthread_mutex_lock.c" 1
	movq %rax,%fs:736
# 0 "" 2
# 491 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	jmp	.L21
.L90:
	leaq	32(%rdi), %rax
	orq	$1, %rax
#APP
# 364 "../nptl/pthread_mutex_lock.c" 1
	movq %rax,%fs:752
# 0 "" 2
#NO_APP
	jmp	.L22
.L87:
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L50
	cmpl	$1, %eax
	jne	.L39
	movl	4(%rdi), %eax
	cmpl	%ebp, %eax
	je	.L25
.L84:
	addl	$1, %eax
	movl	%eax, 4(%rbx)
	xorl	%eax, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L29:
	cmpl	$-4, %eax
	jl	.L18
	cmpl	$-3, %eax
	jle	.L28
	testl	%eax, %eax
	jne	.L18
.L28:
	movl	%eax, %edx
	andl	$-33, %edx
	cmpl	$-35, %edx
	jne	.L30
	cmpl	$-35, %eax
	je	.L93
	cmpl	$-3, %eax
	jne	.L32
	testl	%r8d, %r8d
	jne	.L94
.L32:
	leaq	12(%rsp), %rbx
	.p2align 4,,10
	.p2align 3
.L33:
	movl	%ebp, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	$0, 12(%rsp)
	call	__GI___futex_abstimed_wait64
	jmp	.L33
.L30:
	movl	(%rbx), %eax
	andl	$1073741824, %eax
	testl	%r8d, %r8d
	jne	.L34
	testl	%eax, %eax
	je	.L35
	leaq	__PRETTY_FUNCTION__.10083(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$434, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movl	%ecx, %eax
	movl	%ecx, %edx
	andl	$1073741824, %eax
	jne	.L95
	andl	$1073741823, %edx
	cmpl	%r13d, %edx
	je	.L96
.L12:
	testl	%ecx, %ecx
	movl	%ecx, %edx
	jns	.L97
.L15:
	xorl	%r10d, %r10d
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movl	%r12d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L98
.L16:
	movl	(%rbx), %eax
	movl	%ebp, %esi
.L11:
	movl	%eax, %ecx
	jmp	.L7
.L34:
	testl	%eax, %eax
	je	.L46
#APP
# 439 "../nptl/pthread_mutex_lock.c" 1
	lock;andl $-1073741825, (%rbx)
# 0 "" 2
#NO_APP
	movabsq	$9223372032559808513, %rax
	movq	%rax, 4(%rbx)
#APP
# 449 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	leaq	32(%rbx), %rax
	andq	$-2, %rdx
	movq	%rax, -8(%rdx)
#APP
# 449 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 32(%rbx)
	movq	%fs:16, %rdi
	leaq	736(%rdi), %rdx
	movq	%rdx, 24(%rbx)
	orq	$1, %rax
#APP
# 449 "../nptl/pthread_mutex_lock.c" 1
	movq %rax,%fs:736
# 0 "" 2
# 452 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$130, %eax
	jmp	.L1
.L96:
	movl	(%r14), %edx
	andl	$127, %edx
	cmpl	$18, %edx
	je	.L83
	cmpl	$17, %edx
	jne	.L12
#APP
# 271 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	4(%rbx), %edx
	cmpl	$-1, %edx
	je	.L25
	addl	$1, %edx
	movl	%edx, 4(%rbx)
	jmp	.L1
.L95:
	movl	%r13d, %eax
	andl	$-2147483648, %edx
	orl	%esi, %eax
	orl	%eax, %edx
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rbx)
	cmpl	%eax, %ecx
	jne	.L11
	movabsq	$9223372032559808513, %rax
	movq	%rax, 4(%rbx)
#APP
# 236 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	andq	$-2, %rax
	movq	%r8, -8(%rax)
#APP
# 236 "../nptl/pthread_mutex_lock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 236 "../nptl/pthread_mutex_lock.c" 1
	movq %r8,%fs:736
# 0 "" 2
# 239 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$130, %eax
	jmp	.L1
.L83:
#APP
# 381 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$35, %eax
	jmp	.L1
.L92:
	movl	$0, 4(%rbx)
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 307 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L99
.L37:
#APP
# 478 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$131, %eax
	jmp	.L1
.L94:
	leaq	__PRETTY_FUNCTION__.10083(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$424, %edx
	call	__assert_fail@PLT
.L99:
	cmpl	$-11, %eax
	je	.L37
	jg	.L38
	cmpl	$-38, %eax
	je	.L37
	cmpl	$-35, %eax
	je	.L37
	cmpl	$-110, %eax
	jne	.L18
	jmp	.L37
.L98:
	addl	$11, %eax
	cmpl	$11, %eax
	ja	.L18
	movl	%eax, %ecx
	movq	%r9, %rdi
	salq	%cl, %rdi
	movq	%rdi, %rax
	testl	$2177, %eax
	je	.L18
	jmp	.L16
.L89:
	leaq	__PRETTY_FUNCTION__.10083(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$581, %edx
	call	__assert_fail@PLT
.L93:
	subl	$1, %r9d
	cmpl	$1, %r9d
	ja	.L32
	leaq	__PRETTY_FUNCTION__.10083(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$421, %edx
	call	__assert_fail@PLT
.L86:
	movl	$0, 4(%rbx)
#APP
# 322 "../nptl/pthread_mutex_lock.c" 1
	xchgl %ecx, (%rbx)
# 0 "" 2
#NO_APP
	subl	$1, %ecx
	jle	.L37
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 322 "../nptl/pthread_mutex_lock.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L37
.L97:
	orl	$-2147483648, %edx
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rbx)
	je	.L15
	movl	(%rbx), %eax
	movl	%eax, %ecx
	jmp	.L7
.L38:
	cmpl	$-4, %eax
	jl	.L18
	cmpl	$-3, %eax
	jle	.L37
	addl	$1, %eax
	je	.L37
	jmp	.L18
.L91:
	cmpl	$2, %r9d
	je	.L83
	cmpl	$1, %r9d
	jne	.L23
#APP
# 389 "../nptl/pthread_mutex_lock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	4(%rbx), %eax
	cmpl	$-1, %eax
	jne	.L84
.L25:
	movl	$11, %eax
	jmp	.L1
.L50:
	movl	$35, %eax
	jmp	.L1
	.size	__pthread_mutex_lock_full, .-__pthread_mutex_lock_full
	.section	.rodata.str1.8
	.align 8
.LC6:
	.string	"PTHREAD_MUTEX_TYPE (mutex) == PTHREAD_MUTEX_ERRORCHECK_NP"
	.text
	.p2align 4,,15
	.globl	__GI___pthread_mutex_lock
	.hidden	__GI___pthread_mutex_lock
	.type	__GI___pthread_mutex_lock, @function
__GI___pthread_mutex_lock:
	movl	16(%rdi), %eax
	movl	%eax, %edx
	andl	$383, %edx
	andl	$124, %eax
	jne	.L133
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	%edx, %edx
	jne	.L102
	movl	__pthread_force_elision(%rip), %ecx
	testl	%ecx, %ecx
	jne	.L103
.L108:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L134
.L104:
	movl	8(%rbx), %edx
	testl	%edx, %edx
	jne	.L135
.L115:
#APP
# 153 "../nptl/pthread_mutex_lock.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	addl	$1, 12(%rbx)
	movl	%eax, 8(%rbx)
	xorl	%eax, %eax
.L100:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L103:
	movl	16(%rdi), %eax
	testb	$3, %ah
	je	.L106
	testb	$1, %ah
	je	.L108
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L106:
	orb	$1, %ah
	movl	%eax, 16(%rdi)
.L107:
	movl	16(%rbx), %edx
	addq	$8, %rsp
	leaq	22(%rbx), %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	andl	$128, %edx
	jmp	__lll_lock_elision
	.p2align 4,,10
	.p2align 3
.L133:
	jmp	__pthread_mutex_lock_full
	.p2align 4,,10
	.p2align 3
.L102:
	cmpl	$256, %edx
	je	.L107
	movl	16(%rdi), %edx
	andl	$127, %edx
	cmpl	$1, %edx
	jne	.L110
#APP
# 99 "../nptl/pthread_mutex_lock.c" 1
	movl %fs:720,%ecx
# 0 "" 2
#NO_APP
	cmpl	%ecx, 8(%rdi)
	jne	.L111
	movl	4(%rdi), %eax
	cmpl	$-1, %eax
	je	.L123
	addl	$1, %eax
	movl	%eax, 4(%rdi)
	xorl	%eax, %eax
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L134:
	movl	16(%rbx), %esi
	movq	%rbx, %rdi
	andl	$128, %esi
	call	__lll_lock_wait
	jmp	.L104
.L111:
	lock cmpxchgl	%edx, (%rdi)
	jne	.L136
.L113:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	jne	.L137
	movl	$1, 4(%rbx)
	jmp	.L115
.L110:
	movl	16(%rdi), %edx
	andl	$127, %edx
	cmpl	$3, %edx
	jne	.L116
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	jne	.L138
.L117:
	cmpl	$0, 8(%rbx)
	je	.L115
	leaq	__PRETTY_FUNCTION__.9988(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$141, %edx
	call	__assert_fail@PLT
.L136:
	movl	16(%rdi), %esi
	andl	$128, %esi
	call	__lll_lock_wait
	jmp	.L113
.L123:
	movl	$11, %eax
	jmp	.L100
.L135:
	leaq	__PRETTY_FUNCTION__.9988(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$81, %edx
	call	__assert_fail@PLT
.L138:
	movswl	20(%rdi), %eax
	movswl	__mutex_aconf(%rip), %edx
	movl	$1, %ecx
	leal	10(%rax,%rax), %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
.L121:
	addl	$1, %ebp
	leal	-1(%rbp), %eax
	cmpl	%eax, %edx
	jle	.L139
#APP
# 135 "../nptl/pthread_mutex_lock.c" 1
	pause
# 0 "" 2
#NO_APP
	movl	%esi, %eax
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L121
.L120:
	movswl	20(%rbx), %eax
	movl	$8, %esi
	subl	%eax, %ebp
	movl	%eax, %ecx
	movl	%ebp, %eax
	cltd
	idivl	%esi
	addl	%ecx, %eax
	movw	%ax, 20(%rbx)
	jmp	.L117
.L116:
#APP
# 145 "../nptl/pthread_mutex_lock.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	movl	16(%rdi), %eax
	andl	$127, %eax
	cmpl	$2, %eax
	jne	.L140
	cmpl	%edx, 8(%rdi)
	jne	.L108
	movl	$35, %eax
	jmp	.L100
.L137:
	leaq	__PRETTY_FUNCTION__.9988(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$117, %edx
	call	__assert_fail@PLT
.L139:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	je	.L120
	movl	16(%rbx), %esi
	movq	%rbx, %rdi
	andl	$128, %esi
	call	__lll_lock_wait
	jmp	.L120
.L140:
	leaq	__PRETTY_FUNCTION__.9988(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$146, %edx
	call	__assert_fail@PLT
	.size	__GI___pthread_mutex_lock, .-__GI___pthread_mutex_lock
	.weak	pthread_mutex_lock
	.set	pthread_mutex_lock,__GI___pthread_mutex_lock
	.globl	__pthread_mutex_lock
	.set	__pthread_mutex_lock,__GI___pthread_mutex_lock
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.10083, @object
	.size	__PRETTY_FUNCTION__.10083, 26
__PRETTY_FUNCTION__.10083:
	.string	"__pthread_mutex_lock_full"
	.align 16
	.type	__PRETTY_FUNCTION__.9988, @object
	.size	__PRETTY_FUNCTION__.9988, 21
__PRETTY_FUNCTION__.9988:
	.string	"__pthread_mutex_lock"
	.hidden	__mutex_aconf
	.hidden	__lll_lock_wait
	.hidden	__lll_lock_elision
	.hidden	__pthread_force_elision
	.hidden	__pthread_current_priority
	.hidden	__pthread_tpp_change_priority
