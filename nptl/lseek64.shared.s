	.text
	.p2align 4,,15
	.globl	__lseek64
	.hidden	__lseek64
	.type	__lseek64, @function
__lseek64:
	movl	$8, %eax
#APP
# 36 "../sysdeps/unix/sysv/linux/lseek64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
	.size	__lseek64, .-__lseek64
	.weak	lseek64
	.set	lseek64,__lseek64
	.globl	__libc_lseek64
	.set	__libc_lseek64,__lseek64
	.globl	__libc_lseek
	.set	__libc_lseek,__lseek64
	.weak	__lseek
	.set	__lseek,__lseek64
	.weak	lseek
	.set	lseek,__lseek64
