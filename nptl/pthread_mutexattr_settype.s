	.text
	.p2align 4,,15
	.globl	__pthread_mutexattr_settype
	.type	__pthread_mutexattr_settype, @function
__pthread_mutexattr_settype:
	cmpl	$3, %esi
	movl	$22, %eax
	ja	.L1
	testl	%esi, %esi
	movl	$512, %eax
	cmove	%eax, %esi
	movl	(%rdi), %eax
	andl	$-251662336, %eax
	orl	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, (%rdi)
.L1:
	rep ret
	.size	__pthread_mutexattr_settype, .-__pthread_mutexattr_settype
	.weak	pthread_mutexattr_settype
	.set	pthread_mutexattr_settype,__pthread_mutexattr_settype
	.weak	pthread_mutexattr_setkind_np
	.set	pthread_mutexattr_setkind_np,__pthread_mutexattr_settype
