	.text
	.p2align 4,,15
	.globl	thrd_create
	.type	thrd_create, @function
thrd_create:
	subq	$8, %rsp
	movq	%rdx, %rcx
	movq	%rsi, %rdx
	movq	$-1, %rsi
	call	__pthread_create_2_1@PLT
	cmpl	$12, %eax
	je	.L3
	jle	.L13
	cmpl	$16, %eax
	je	.L6
	cmpl	$110, %eax
	jne	.L2
	movl	$4, %eax
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testl	%eax, %eax
	je	.L1
.L2:
	movl	$2, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$3, %eax
	addq	$8, %rsp
	ret
	.size	thrd_create, .-thrd_create
