	.text
	.p2align 4,,15
	.globl	__pthread_attr_setschedparam
	.type	__pthread_attr_setschedparam, @function
__pthread_attr_setschedparam:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	4(%rdi), %ebp
	movq	%rdi, %rbx
	movl	(%rsi), %r14d
	movl	%ebp, %edi
	call	__sched_get_priority_min
	movl	%ebp, %edi
	movl	%eax, %r12d
	call	__sched_get_priority_max
	testl	%r12d, %r12d
	js	.L4
	testl	%eax, %eax
	js	.L4
	cmpl	%r12d, %r14d
	jl	.L4
	cmpl	%eax, %r14d
	jg	.L4
	movl	0(%r13), %eax
	orl	$32, 8(%rbx)
	movl	%eax, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	popq	%rbx
	movl	$22, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__pthread_attr_setschedparam, .-__pthread_attr_setschedparam
	.globl	pthread_attr_setschedparam
	.set	pthread_attr_setschedparam,__pthread_attr_setschedparam
	.hidden	__sched_get_priority_max
	.hidden	__sched_get_priority_min
