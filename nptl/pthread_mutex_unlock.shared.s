	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	__pthread_mutex_unlock_full, @function
__pthread_mutex_unlock_full:
	movl	16(%rdi), %eax
	andl	$127, %eax
	subl	$16, %eax
	cmpl	$51, %eax
	ja	.L43
	leaq	.L4(%rip), %rdx
	movslq	(%rdx,%rax,4), %rax
	addq	%rdx, %rax
	jmp	*%rax
	.section	.rodata
	.align 4
	.align 4
.L4:
	.long	.L3-.L4
	.long	.L5-.L4
	.long	.L3-.L4
	.long	.L3-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L6-.L4
	.long	.L7-.L4
	.long	.L6-.L4
	.long	.L6-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L6-.L4
	.long	.L8-.L4
	.long	.L6-.L4
	.long	.L6-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L43-.L4
	.long	.L9-.L4
	.long	.L10-.L4
	.long	.L11-.L4
	.long	.L9-.L4
	.text
	.p2align 4,,10
	.p2align 3
.L10:
#APP
# 298 "pthread_mutex_unlock.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	cmpl	%edx, 8(%rdi)
	movl	$1, %eax
	jne	.L1
	subl	$1, 4(%rdi)
	jne	.L78
	.p2align 4,,10
	.p2align 3
.L9:
	testl	%esi, %esi
	movl	$0, 8(%rdi)
	je	.L39
	subl	$1, 12(%rdi)
.L39:
	movl	(%rdi), %eax
.L40:
	movl	%eax, %r8d
	andl	$-524288, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	jne	.L40
	andl	$524287, %eax
	cmpl	$1, %eax
	jbe	.L42
	movl	16(%rdi), %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$202, %eax
	andl	$128, %esi
	xorb	$-127, %sil
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L82
.L42:
	movl	%r8d, %edi
	movl	$-1, %esi
	sarl	$19, %edi
	jmp	__pthread_tpp_change_priority
	.p2align 4,,10
	.p2align 3
.L5:
#APP
# 108 "pthread_mutex_unlock.c" 1
	movl %fs:720,%ecx
# 0 "" 2
#NO_APP
	movl	(%rdi), %eax
	movl	8(%rdi), %edx
	andl	$1073741823, %eax
	cmpl	%ecx, %eax
	je	.L83
.L12:
#APP
# 119 "pthread_mutex_unlock.c" 1
	movl %fs:720,%ecx
# 0 "" 2
#NO_APP
	cmpl	%edx, %ecx
	movl	$1, %eax
	je	.L84
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
#APP
# 308 "pthread_mutex_unlock.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	cmpl	%edx, 8(%rdi)
	movl	$1, %eax
	jne	.L1
	testl	$524287, (%rdi)
	jne	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	(%rdi), %eax
#APP
# 132 "pthread_mutex_unlock.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	movl	%eax, %ecx
	andl	$1073741823, %ecx
	cmpl	%edx, %ecx
	jne	.L48
	testl	%eax, %eax
	jne	.L85
.L48:
	movl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
#APP
# 184 "pthread_mutex_unlock.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	cmpl	%edx, 8(%rdi)
	movl	$1, %eax
	jne	.L1
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L78
.L21:
	testl	%esi, %esi
	movl	%eax, 8(%rdi)
	je	.L27
	subl	$1, 12(%rdi)
.L27:
	movl	16(%rdi), %eax
	movl	$128, %esi
	testb	$16, %al
	je	.L86
.L28:
	movl	(%rdi), %edx
	testl	%edx, %edx
	js	.L32
#APP
# 279 "pthread_mutex_unlock.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%edx, %eax
	jne	.L32
	xorl	%edx, %edx
	lock cmpxchgl	%edx, (%rdi)
	jne	.L33
.L79:
#APP
# 175 "pthread_mutex_unlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
.L78:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	(%rdi), %eax
#APP
# 222 "pthread_mutex_unlock.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	movl	%eax, %ecx
	andl	$1073741823, %ecx
	cmpl	%edx, %ecx
	jne	.L48
	testl	%eax, %eax
	je	.L48
	movl	16(%rdi), %eax
	andl	$16, %eax
	je	.L26
	xorl	%eax, %eax
	cmpl	$2147483647, 8(%rdi)
	je	.L24
.L26:
	movl	16(%rdi), %edx
	andl	$16, %edx
	je	.L21
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L8:
#APP
# 195 "pthread_mutex_unlock.c" 1
	movl %fs:720,%ecx
# 0 "" 2
#NO_APP
	movl	(%rdi), %eax
	movl	8(%rdi), %edx
	andl	$1073741823, %eax
	cmpl	%ecx, %eax
	je	.L87
.L22:
#APP
# 206 "pthread_mutex_unlock.c" 1
	movl %fs:720,%ecx
# 0 "" 2
#NO_APP
	cmpl	%edx, %ecx
	movl	$1, %eax
	jne	.L1
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L78
.L25:
	leaq	32(%rdi), %rdx
	orq	$1, %rdx
#APP
# 246 "pthread_mutex_unlock.c" 1
	movq %rdx,%fs:752
# 0 "" 2
#NO_APP
	movq	32(%rdi), %rcx
	movq	24(%rdi), %r8
	movq	%rcx, %rdx
	andq	$-2, %rdx
	movq	%r8, -8(%rdx)
	movq	24(%rdi), %rdx
	andq	$-2, %rdx
	movq	%rcx, (%rdx)
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L43:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	subl	$1, 4(%rdi)
	jne	.L78
.L16:
	xorl	%r8d, %r8d
.L15:
	leaq	32(%rdi), %rax
#APP
# 146 "pthread_mutex_unlock.c" 1
	movq %rax,%fs:752
# 0 "" 2
#NO_APP
	movq	32(%rdi), %rdx
	movq	24(%rdi), %rcx
	movq	%rdx, %rax
	andq	$-2, %rax
	movq	%rcx, -8(%rax)
	movq	24(%rdi), %rax
	andq	$-2, %rax
	movq	%rdx, (%rax)
	testl	%esi, %esi
	movq	$0, 24(%rdi)
	movq	$0, 32(%rdi)
	movl	%r8d, 8(%rdi)
	je	.L17
	subl	$1, 12(%rdi)
.L17:
	xorl	%eax, %eax
#APP
# 163 "pthread_mutex_unlock.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	js	.L88
.L20:
	jmp	.L79
	.p2align 4,,10
	.p2align 3
.L85:
	cmpl	$2147483647, 8(%rdi)
	jne	.L16
.L13:
	movl	$2147483646, %r8d
	jmp	.L15
.L88:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$1, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L20
	cmpl	$-22, %eax
	je	.L20
	cmpl	$-14, %eax
	je	.L20
.L19:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L83:
	cmpl	$2147483647, %edx
	jne	.L12
	subl	$1, 4(%rdi)
	je	.L13
.L23:
	movl	$131, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L87:
	cmpl	$2147483647, %edx
	jne	.L22
	subl	$1, 4(%rdi)
	jne	.L23
.L24:
	movl	$2147483646, %eax
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L82:
	cmpl	$-22, %eax
	je	.L42
	cmpl	$-14, %eax
	je	.L42
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L86:
	movl	16(%rdi), %esi
	andl	$128, %esi
	jmp	.L28
.L32:
	xorb	$-121, %sil
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$202, %eax
#APP
# 307 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L79
	cmpl	$-11, %eax
	je	.L79
	jg	.L36
	cmpl	$-38, %eax
	je	.L79
	cmpl	$-35, %eax
	je	.L79
	cmpl	$-110, %eax
	jne	.L19
	jmp	.L79
.L36:
	cmpl	$-4, %eax
	jl	.L19
	cmpl	$-3, %eax
	jle	.L79
	addl	$1, %eax
	je	.L79
	jmp	.L19
.L33:
	testl	%eax, %eax
	js	.L32
#APP
# 279 "pthread_mutex_unlock.c" 1
	movl %fs:720,%ecx
# 0 "" 2
#NO_APP
	cmpl	%ecx, %eax
	jne	.L32
	lock cmpxchgl	%edx, (%rdi)
	je	.L79
	jmp	.L33
	.size	__pthread_mutex_unlock_full, .-__pthread_mutex_unlock_full
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"pthread_mutex_unlock.c"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"type == PTHREAD_MUTEX_ERRORCHECK_NP"
	.text
	.p2align 4,,15
	.globl	__pthread_mutex_unlock_usercnt
	.hidden	__pthread_mutex_unlock_usercnt
	.type	__pthread_mutex_unlock_usercnt, @function
__pthread_mutex_unlock_usercnt:
	movl	16(%rdi), %eax
	movl	%eax, %edx
	andl	$383, %edx
	andl	$124, %eax
	jne	.L111
	testl	%edx, %edx
	movl	%eax, %r8d
	jne	.L91
.L99:
	testl	%esi, %esi
	movl	$0, 8(%rdi)
	jne	.L112
.L92:
	movl	16(%rdi), %esi
	xorl	%eax, %eax
#APP
# 58 "pthread_mutex_unlock.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L113
.L89:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	subl	$1, 12(%rdi)
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L111:
	jmp	__pthread_mutex_unlock_full
	.p2align 4,,10
	.p2align 3
.L91:
	cmpl	$256, %edx
	jne	.L95
	movl	16(%rdi), %esi
	andl	$128, %esi
	jmp	__lll_unlock_elision
	.p2align 4,,10
	.p2align 3
.L113:
	andl	$128, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movl	$202, %eax
#APP
# 58 "pthread_mutex_unlock.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L89
.L95:
	movl	16(%rdi), %eax
	andl	$127, %eax
	cmpl	$1, %eax
	jne	.L96
#APP
# 74 "pthread_mutex_unlock.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, 8(%rdi)
	je	.L97
.L101:
	movl	$1, %r8d
	jmp	.L89
.L97:
	subl	$1, 4(%rdi)
	je	.L99
	jmp	.L89
.L96:
	movl	16(%rdi), %eax
	andl	$127, %eax
	cmpl	$3, %eax
	je	.L99
	cmpl	$2, %edx
	jne	.L114
#APP
# 89 "pthread_mutex_unlock.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, 8(%rdi)
	jne	.L101
	cmpl	$0, (%rdi)
	je	.L101
	jmp	.L99
.L114:
	leaq	__PRETTY_FUNCTION__.8807(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	movl	$88, %edx
	call	__assert_fail@PLT
	.size	__pthread_mutex_unlock_usercnt, .-__pthread_mutex_unlock_usercnt
	.p2align 4,,15
	.globl	__GI___pthread_mutex_unlock
	.hidden	__GI___pthread_mutex_unlock
	.type	__GI___pthread_mutex_unlock, @function
__GI___pthread_mutex_unlock:
	movl	$1, %esi
	jmp	__pthread_mutex_unlock_usercnt
	.size	__GI___pthread_mutex_unlock, .-__GI___pthread_mutex_unlock
	.weak	pthread_mutex_unlock
	.set	pthread_mutex_unlock,__GI___pthread_mutex_unlock
	.globl	__pthread_mutex_unlock
	.set	__pthread_mutex_unlock,__GI___pthread_mutex_unlock
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.8807, @object
	.size	__PRETTY_FUNCTION__.8807, 31
__PRETTY_FUNCTION__.8807:
	.string	"__pthread_mutex_unlock_usercnt"
	.hidden	__lll_unlock_elision
	.hidden	__pthread_tpp_change_priority
