	.text
	.p2align 4,,15
	.globl	__pthread_setaffinity_new
	.type	__pthread_setaffinity_new, @function
__pthread_setaffinity_new:
	movl	720(%rdi), %edi
	movl	$203, %eax
#APP
# 33 "pthread_setaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	negl	%edx
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmova	%edx, %eax
	ret
	.size	__pthread_setaffinity_new, .-__pthread_setaffinity_new
	.weak	pthread_setaffinity_np
	.set	pthread_setaffinity_np,__pthread_setaffinity_new
