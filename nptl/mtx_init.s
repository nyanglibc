	.text
	.p2align 4,,15
	.globl	mtx_init
	.type	mtx_init, @function
mtx_init:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movl	%esi, %ebp
	subq	$16, %rsp
	leaq	12(%rsp), %rbx
	movq	%rbx, %rdi
	call	__pthread_mutexattr_init@PLT
	cmpl	$1, %ebp
	je	.L3
	cmpl	$3, %ebp
	je	.L3
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	__pthread_mutexattr_settype@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__pthread_mutex_init@PLT
	cmpl	$12, %eax
	je	.L6
.L24:
	jle	.L23
	cmpl	$16, %eax
	je	.L9
	cmpl	$110, %eax
	jne	.L5
	movl	$4, %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	__pthread_mutexattr_settype@PLT
	movq	%rbx, %rsi
	movq	%r12, %rdi
	call	__pthread_mutex_init@PLT
	cmpl	$12, %eax
	jne	.L24
.L6:
	addq	$16, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	testl	%eax, %eax
	je	.L1
.L5:
	addq	$16, %rsp
	movl	$2, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.size	mtx_init, .-mtx_init
