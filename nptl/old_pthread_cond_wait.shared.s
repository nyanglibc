	.text
#APP
	.symver __pthread_cond_wait_2_0,pthread_cond_wait@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pthread_cond_wait_2_0
	.type	__pthread_cond_wait_2_0, @function
__pthread_cond_wait_2_0:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L8
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__pthread_cond_wait@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdi, %rbp
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L3
	movq	%rbx, %rax
	lock cmpxchgq	%rdi, 0(%rbp)
	jne	.L4
.L6:
	movq	0(%rbp), %rbx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__pthread_cond_wait@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	popq	%rbx
	movl	$12, %eax
	popq	%rbp
	popq	%r12
	ret
.L4:
	call	free@PLT
	jmp	.L6
	.size	__pthread_cond_wait_2_0, .-__pthread_cond_wait_2_0
