	.text
	.p2align 4,,15
	.globl	__GI___pthread_mutexattr_init
	.hidden	__GI___pthread_mutexattr_init
	.type	__GI___pthread_mutexattr_init, @function
__GI___pthread_mutexattr_init:
	movl	$0, (%rdi)
	xorl	%eax, %eax
	ret
	.size	__GI___pthread_mutexattr_init, .-__GI___pthread_mutexattr_init
	.weak	pthread_mutexattr_init
	.set	pthread_mutexattr_init,__GI___pthread_mutexattr_init
	.globl	__pthread_mutexattr_init
	.set	__pthread_mutexattr_init,__GI___pthread_mutexattr_init
