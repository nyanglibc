	.text
	.p2align 4,,15
	.globl	__GI___pthread_cleanup_upto
	.hidden	__GI___pthread_cleanup_upto
	.type	__GI___pthread_cleanup_upto, @function
__GI___pthread_cleanup_upto:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	%fs:16, %rax
	movq	1688(%rax), %rbp
	addq	1680(%rax), %rbp
	subq	%rbp, %rsi
#APP
# 36 "pt-cleanup.c" 1
	movq %fs:760,%rbx
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	je	.L4
	movq	%rbx, %rdx
	movq	48(%rdi), %rax
	movq	%rbx, %rcx
#APP
# 40 "../sysdeps/x86_64/jmpbuf-unwind.h" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	subq	%rbp, %rdx
	subq	%rbp, %rax
	cmpq	%rax, %rdx
	jnb	.L3
	cmpq	%rdx, %rsi
	movq	%rsi, %r12
	movq	%rdi, %r13
	jb	.L5
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L12:
	movq	%rbx, %rdx
	movq	48(%r13), %rax
	movq	%rbx, %rcx
#APP
# 40 "../sysdeps/x86_64/jmpbuf-unwind.h" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	subq	%rbp, %rdx
	subq	%rbp, %rax
	cmpq	%rax, %rdx
	jnb	.L3
	cmpq	%r12, %rdx
	jbe	.L4
.L5:
	movq	8(%rbx), %rdi
	call	*(%rbx)
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.L12
.L4:
	xorl	%ecx, %ecx
.L3:
#APP
# 60 "pt-cleanup.c" 1
	movq %rcx,%fs:760
# 0 "" 2
#NO_APP
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___pthread_cleanup_upto, .-__GI___pthread_cleanup_upto
	.globl	__pthread_cleanup_upto
	.set	__pthread_cleanup_upto,__GI___pthread_cleanup_upto
