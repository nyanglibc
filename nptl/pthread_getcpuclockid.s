	.text
	.p2align 4,,15
	.globl	pthread_getcpuclockid
	.type	pthread_getcpuclockid, @function
pthread_getcpuclockid:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	jle	.L3
	notl	%eax
	leal	6(,%rax,8), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$3, %eax
	ret
	.size	pthread_getcpuclockid, .-pthread_getcpuclockid
