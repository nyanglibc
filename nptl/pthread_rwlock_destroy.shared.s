	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_destroy
	.type	__pthread_rwlock_destroy, @function
__pthread_rwlock_destroy:
	xorl	%eax, %eax
	ret
	.size	__pthread_rwlock_destroy, .-__pthread_rwlock_destroy
	.globl	pthread_rwlock_destroy
	.set	pthread_rwlock_destroy,__pthread_rwlock_destroy
