	.text
	.p2align 4,,15
	.globl	pthread_attr_getguardsize
	.type	pthread_attr_getguardsize, @function
pthread_attr_getguardsize:
	movq	16(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_attr_getguardsize, .-pthread_attr_getguardsize
