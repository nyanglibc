	.text
	.p2align 4,,15
	.globl	__pthread_attr_setstack
	.type	__pthread_attr_setstack, @function
__pthread_attr_setstack:
	cmpq	$16383, %rdx
	movl	$22, %eax
	jbe	.L1
	orl	$8, 8(%rdi)
	movq	%rdx, 32(%rdi)
	addq	%rsi, %rdx
	movq	%rdx, 24(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__pthread_attr_setstack, .-__pthread_attr_setstack
	.globl	pthread_attr_setstack
	.set	pthread_attr_setstack,__pthread_attr_setstack
