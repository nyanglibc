	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_condattr_destroy
	.type	__pthread_condattr_destroy, @function
__pthread_condattr_destroy:
	xorl	%eax, %eax
	ret
	.size	__pthread_condattr_destroy, .-__pthread_condattr_destroy
	.globl	pthread_condattr_destroy
	.set	pthread_condattr_destroy,__pthread_condattr_destroy
