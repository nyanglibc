	.text
	.p2align 4,,15
	.globl	__libc_recvfrom
	.type	__libc_recvfrom, @function
__libc_recvfrom:
	movl	%ecx, %r10d
#APP
# 27 "../sysdeps/unix/sysv/linux/recvfrom.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$45, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/recvfrom.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r15
	pushq	%r14
	movq	%r8, %r15
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r14d
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r13
	movq	%rsi, %r12
	movl	%edi, %ebx
	subq	$24, %rsp
	movq	%r9, 8(%rsp)
	call	__pthread_enable_asynccancel
	movq	8(%rsp), %r9
	movl	%eax, %ebp
	movq	%r15, %r8
	movl	%r14d, %r10d
	movq	%r13, %rdx
	movq	%r12, %rsi
	movl	%ebx, %edi
	movl	$45, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/recvfrom.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%ebp, %edi
	movq	%rax, 8(%rsp)
	call	__pthread_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_recvfrom, .-__libc_recvfrom
	.weak	__recvfrom
	.hidden	__recvfrom
	.set	__recvfrom,__libc_recvfrom
	.weak	recvfrom
	.set	recvfrom,__libc_recvfrom
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
