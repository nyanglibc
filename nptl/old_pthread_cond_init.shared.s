	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __pthread_cond_init_2_0,pthread_cond_init@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pthread_cond_init_2_0
	.type	__pthread_cond_init_2_0, @function
__pthread_cond_init_2_0:
	xorl	%eax, %eax
	testq	%rsi, %rsi
	movq	$0, (%rdi)
	je	.L1
	movl	(%rsi), %eax
	movl	$22, %edx
	testl	%eax, %eax
	cmovne	%edx, %eax
.L1:
	rep ret
	.size	__pthread_cond_init_2_0, .-__pthread_cond_init_2_0
