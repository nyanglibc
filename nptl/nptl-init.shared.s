	.text
	.p2align 4,,15
	.type	__nptl_set_robust, @function
__nptl_set_robust:
	addq	$736, %rdi
	movl	$24, %esi
	movl	$273, %eax
#APP
# 119 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	ret
	.size	__nptl_set_robust, .-__nptl_set_robust
	.p2align 4,,15
	.type	sigcancel_handler, @function
sigcancel_handler:
	cmpl	$32, %edi
	je	.L14
	rep ret
	.p2align 4,,10
	.p2align 3
.L14:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %ebp
	call	__getpid@PLT
	cmpl	%eax, %ebp
	je	.L15
.L3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	cmpl	$-6, 8(%rbx)
	jne	.L3
	movq	%fs:16, %rsi
#APP
# 139 "nptl-init.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L17:
	testb	$16, %dl
	jne	.L3
	movl	%edx, %eax
	lock cmpxchgl	%ecx, 776(%rsi)
	cmpl	%eax, %edx
	je	.L16
	movl	%eax, %edx
.L5:
	movl	%edx, %ecx
	orl	$12, %ecx
	cmpl	%ecx, %edx
	jne	.L17
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
#APP
# 156 "nptl-init.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	andl	$2, %edx
	je	.L3
	movq	%fs:16, %rax
#APP
# 304 "./pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "./pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__GI___pthread_unwind
	.size	sigcancel_handler, .-sigcancel_handler
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	sighandler_setxid, @function
sighandler_setxid:
	cmpl	$33, %edi
	je	.L31
	rep ret
	.p2align 4,,10
	.p2align 3
.L31:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %ebp
	call	__getpid@PLT
	cmpl	%eax, %ebp
	je	.L32
.L18:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$-6, 8(%rbx)
	jne	.L18
	movq	__xidcmd(%rip), %rax
	movq	16(%rax), %rsi
	movq	8(%rax), %rdi
	movq	24(%rax), %rdx
	movl	(%rax), %eax
#APP
# 190 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	__xidcmd(%rip), %rdi
	movl	%eax, %esi
	negl	%esi
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmovbe	%eax, %esi
	call	__nptl_setxid_error
	movq	%fs:16, %rdi
	leaq	776(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L21:
#APP
# 202 "nptl-init.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$-65, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	jne	.L21
	movl	$1, 1564(%rdi)
	xorl	%r10d, %r10d
	addq	$1564, %rdi
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L33
.L22:
	movq	__xidcmd(%rip), %rax
	movl	$-1, %edx
	lock xaddl	%edx, 32(%rax)
	cmpl	$1, %edx
	jne	.L18
	movq	__xidcmd(%rip), %rax
	xorl	%r10d, %r10d
	movl	$129, %esi
	leaq	32(%rax), %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L18
	cmpl	$-22, %eax
	je	.L18
	cmpl	$-14, %eax
	je	.L18
.L23:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	cmpl	$-22, %eax
	je	.L22
	cmpl	$-14, %eax
	je	.L22
	jmp	.L23
	.size	sighandler_setxid, .-sighandler_setxid
	.p2align 4,,15
	.globl	__pthread_initialize_minimal_internal
	.type	__pthread_initialize_minimal_internal, @function
__pthread_initialize_minimal_internal:
	pushq	%r12
	pushq	%rbp
	movl	$218, %eax
	pushq	%rbx
	subq	$192, %rsp
	movq	%fs:16, %rdx
	leaq	720(%rdx), %rdi
#APP
# 28 "../sysdeps/unix/sysv/linux/pthread-pids.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, 720(%rdx)
	leaq	784(%rdx), %rax
#APP
# 231 "nptl-init.c" 1
	movq %rax,%fs:1296
# 0 "" 2
# 232 "nptl-init.c" 1
	movb $1,%fs:1554
# 0 "" 2
#NO_APP
	leaq	736(%rdx), %rdi
	movq	$-32, 744(%rdx)
	movl	$24, %esi
	movl	$273, %eax
	movq	%rdi, 728(%rdx)
	movq	%rdi, 736(%rdx)
#APP
# 243 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	__libc_stack_end@GOTPCREL(%rip), %rax
	movq	(%rax), %rax
#APP
# 252 "nptl-init.c" 1
	movq %rax,%fs:1688
# 0 "" 2
#NO_APP
	movzbl	__nptl_initial_report_events(%rip), %eax
#APP
# 257 "nptl-init.c" 1
	movb %al,%fs:1553
# 0 "" 2
#NO_APP
	leaq	32(%rsp), %rbx
	leaq	sigcancel_handler(%rip), %rax
	xorl	%edx, %edx
	movl	$32, %edi
	movq	$0, 40(%rsp)
	movl	$4, 168(%rsp)
	movq	%rbx, %rsi
	movq	%rax, 32(%rsp)
	call	__libc_sigaction@PLT
	leaq	sighandler_setxid(%rip), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	$33, %edi
	movl	$268435460, 168(%rsp)
	movq	%rax, 32(%rsp)
	call	__libc_sigaction@PLT
	movabsq	$6442450944, %rax
	leaq	8(%rbx), %rsi
	orq	%rax, 40(%rsp)
	movl	$8, %r10d
	xorl	%edx, %edx
	movl	$1, %edi
	movl	$14, %eax
#APP
# 279 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	__static_tls_size(%rip), %rdi
	leaq	8(%rsp), %rsi
	call	_dl_get_tls_static_info@PLT
	movq	8(%rsp), %rcx
	cmpq	$15, %rcx
	leaq	-1(%rcx), %rax
	ja	.L36
	movq	$16, 8(%rsp)
	movl	$15, %eax
	movl	$16, %ecx
.L36:
	movq	%rax, __static_tls_align_m1(%rip)
	movq	__static_tls_size(%rip), %rax
	xorl	%edx, %edx
	leaq	16(%rsp), %rsi
	movl	$3, %edi
	leaq	-1(%rcx,%rax), %rax
	divq	%rcx
	imulq	%rcx, %rax
	movq	%rax, __static_tls_size(%rip)
	call	__getrlimit@PLT
	testl	%eax, %eax
	jne	.L37
	movq	16(%rsp), %rdx
	cmpq	$-1, %rdx
	je	.L37
	cmpq	$16383, %rdx
	movl	$16384, %eax
	cmovbe	%rax, %rdx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$2097152, %edx
.L39:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movq	24(%rax), %rbx
	movq	__static_tls_size(%rip), %rax
	addq	%rbx, %rax
	addq	$2048, %rax
	cmpq	%rdx, %rax
	cmovb	%rdx, %rax
	movq	%rbx, %rdx
	leaq	-1(%rbx,%rax), %rax
	negq	%rdx
	andq	%rdx, %rax
	movl	$1, %edx
	movq	%rax, 16(%rsp)
	xorl	%eax, %eax
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L49
.L40:
	movq	16(%rsp), %rax
	movq	%rbx, 16+__default_pthread_attr(%rip)
	movq	%rax, 32+__default_pthread_attr(%rip)
	xorl	%eax, %eax
#APP
# 319 "nptl-init.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L50
.L41:
	movq	_rtld_global@GOTPCREL(%rip), %rbp
	movq	__GI___pthread_mutex_lock@GOTPCREL(%rip), %rax
	movq	%rax, 3984(%rbp)
	movq	__GI___pthread_mutex_unlock@GOTPCREL(%rip), %rax
	movq	%rax, 3992(%rbp)
	movl	2444(%rbp), %eax
	movl	$0, 2444(%rbp)
	testl	%eax, %eax
	leal	-1(%rax), %ebx
	je	.L42
	leaq	2440(%rbp), %r12
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%r12, %rdi
	subl	$1, %ebx
	call	__GI___pthread_mutex_lock@PLT
	cmpl	$-1, %ebx
	jne	.L43
.L42:
	leaq	__make_stacks_executable(%rip), %rax
	leaq	pthread_functions(%rip), %rdx
	leaq	__reclaim_stacks(%rip), %rsi
	leaq	__fork_generation(%rip), %rdi
	movq	%rax, 4008(%rbp)
	leaq	__pthread_init_static_tls(%rip), %rax
	movq	%rax, 4096(%rbp)
	call	__libc_pthread_init@PLT
	movq	%rax, __libc_multiple_threads_ptr(%rip)
	call	__pthread_tunables_init
	addq	$192, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 319 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L49:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L40
	.size	__pthread_initialize_minimal_internal, .-__pthread_initialize_minimal_internal
	.weak	__pthread_initialize_minimal
	.set	__pthread_initialize_minimal,__pthread_initialize_minimal_internal
	.p2align 4,,15
	.globl	__pthread_get_minstack
	.type	__pthread_get_minstack, @function
__pthread_get_minstack:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rdx
	movq	__static_tls_size(%rip), %rax
	addq	24(%rdx), %rax
	addq	$16384, %rax
	ret
	.size	__pthread_get_minstack, .-__pthread_get_minstack
	.local	__nptl_initial_report_events
	.comm	__nptl_initial_report_events,1,1
	.hidden	__xidcmd
	.comm	__xidcmd,8,8
	.section	.data.rel.ro,"aw",@progbits
	.align 32
	.type	pthread_functions, @object
	.size	pthread_functions, 240
pthread_functions:
	.quad	__pthread_cond_broadcast
	.quad	__pthread_cond_signal
	.quad	__pthread_cond_wait
	.quad	__pthread_cond_timedwait
	.quad	__pthread_cond_broadcast_2_0
	.quad	__pthread_cond_signal_2_0
	.quad	__pthread_cond_wait_2_0
	.quad	__pthread_cond_timedwait_2_0
	.quad	__pthread_exit
	.quad	__GI___pthread_mutex_destroy
	.quad	__GI___pthread_mutex_init
	.quad	__GI___pthread_mutex_lock
	.quad	__GI___pthread_mutex_unlock
	.quad	__GI___pthread_setcancelstate
	.quad	__pthread_setcanceltype
	.quad	__GI___pthread_cleanup_upto
	.quad	__GI___pthread_once
	.quad	__GI___pthread_rwlock_rdlock
	.quad	__GI___pthread_rwlock_wrlock
	.quad	__GI___pthread_rwlock_unlock
	.quad	__GI___pthread_key_create
	.quad	__GI___pthread_getspecific
	.quad	__GI___pthread_setspecific
	.quad	__pthread_cleanup_push_defer
	.quad	__pthread_cleanup_pop_restore
	.quad	__nptl_nthreads
	.quad	__GI___pthread_unwind
	.quad	__nptl_deallocate_tsd
	.quad	__nptl_setxid
	.quad	__nptl_set_robust
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	nptl_version, @object
	.size	nptl_version, 5
nptl_version:
	.string	"2.33"
	.hidden	__static_tls_align_m1
	.comm	__static_tls_align_m1,8,8
	.hidden	__static_tls_size
	.comm	__static_tls_size,8,8
	.hidden	__libc_multiple_threads_ptr
	.comm	__libc_multiple_threads_ptr,8,8
	.weak	__GI___pthread_setspecific
	.weak	__GI___pthread_getspecific
	.weak	__GI___pthread_key_create
	.weak	__GI___pthread_rwlock_unlock
	.weak	__GI___pthread_rwlock_wrlock
	.weak	__GI___pthread_rwlock_rdlock
	.weak	__GI___pthread_once
	.weak	__GI___pthread_setcancelstate
	.weak	__GI___pthread_mutex_init
	.weak	__GI___pthread_mutex_destroy
	.weak	__GI___pthread_mutex_unlock
	.weak	__GI___pthread_mutex_lock
	.hidden	__nptl_setxid
	.hidden	__nptl_deallocate_tsd
	.hidden	__nptl_nthreads
	.hidden	__lll_lock_wait_private
	.hidden	__pthread_tunables_init
	.hidden	__pthread_init_static_tls
	.hidden	__fork_generation
	.hidden	__reclaim_stacks
	.hidden	__make_stacks_executable
	.hidden	__default_pthread_attr
	.hidden	__default_pthread_attr_lock
	.hidden	__nptl_setxid_error
