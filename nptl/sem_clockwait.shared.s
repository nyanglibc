	.text
	.p2align 4,,15
	.type	__sem_wait_cleanup, @function
__sem_wait_cleanup:
	movabsq	$-4294967296, %rax
	lock addq	%rax, (%rdi)
	ret
	.size	__sem_wait_cleanup, .-__sem_wait_cleanup
	.p2align 4,,15
	.type	do_futex_wait, @function
do_futex_wait:
	movl	8(%rdi), %r8d
	movq	%rdx, %rcx
	movl	%esi, %edx
	xorl	%esi, %esi
	jmp	__GI___futex_abstimed_wait_cancelable64
	.size	do_futex_wait, .-do_futex_wait
	.p2align 4,,15
	.type	__new_sem_wait_slow64, @function
__new_sem_wait_slow64:
	pushq	%r15
	pushq	%r14
	movl	%esi, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movabsq	$4294967296, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	lock xaddq	%rbp, (%rdi)
	leaq	__sem_wait_cleanup(%rip), %rsi
	movabsq	$-4294967297, %r15
	movq	%rsp, %r12
	movq	%rdi, %rdx
	movq	%r12, %rdi
	call	__pthread_cleanup_push
.L5:
	testl	%ebp, %ebp
	je	.L13
	leaq	0(%rbp,%r15), %rdx
	movq	%rbp, %rax
	lock cmpxchgq	%rdx, (%rbx)
	movq	%rax, %rbp
	jne	.L5
	xorl	%ebx, %ebx
.L9:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	__pthread_cleanup_pop
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r13, %rdx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	call	do_futex_wait
	cmpl	$110, %eax
	je	.L7
	cmpl	$4, %eax
	je	.L7
	cmpl	$75, %eax
	je	.L7
	movq	(%rbx), %rbp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movabsq	$-4294967296, %rax
	lock addq	%rax, (%rbx)
	movl	$-1, %ebx
	jmp	.L9
	.size	__new_sem_wait_slow64, .-__new_sem_wait_slow64
	.p2align 4,,15
	.globl	__sem_clockwait
	.type	__sem_clockwait, @function
__sem_clockwait:
	cmpl	$1, %esi
	ja	.L17
	cmpq	$999999999, 8(%rdx)
	ja	.L17
	movq	(%rdi), %rax
	testl	%eax, %eax
	je	.L19
	leaq	-1(%rax), %rcx
	lock cmpxchgq	%rcx, (%rdi)
	jne	.L19
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	jmp	__new_sem_wait_slow64
	.p2align 4,,10
	.p2align 3
.L17:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__sem_clockwait, .-__sem_clockwait
	.weak	sem_clockwait
	.set	sem_clockwait,__sem_clockwait
	.hidden	__pthread_cleanup_pop
	.hidden	__pthread_cleanup_push
