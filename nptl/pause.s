	.text
	.p2align 4,,15
	.globl	__libc_pause
	.type	__libc_pause, @function
__libc_pause:
#APP
# 29 "../sysdeps/unix/sysv/linux/pause.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$34, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/pause.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	subq	$24, %rsp
	call	__pthread_enable_asynccancel@PLT
	movl	%eax, %edi
	movl	$34, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/pause.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%eax, 12(%rsp)
	call	__pthread_disable_asynccancel@PLT
	movl	12(%rsp), %eax
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__libc_pause, .-__libc_pause
	.weak	pause
	.set	pause,__libc_pause
