 .hidden __pthread_initialize_minimal_internal
 .section .init,"ax",@progbits
 .p2align 2
 .globl _init
 .hidden _init
 .type _init, @function
_init:

 subq $8, %rsp
 call __pthread_initialize_minimal_internal
 .section .fini,"ax",@progbits
 .p2align 2
 .globl _fini
 .hidden _fini
 .type _fini, @function
_fini:

 subq $8, %rsp
