	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_setrobust
	.type	pthread_mutexattr_setrobust, @function
pthread_mutexattr_setrobust:
	testl	%esi, %esi
	jne	.L8
	andl	$-1073741825, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	cmpl	$1, %esi
	jne	.L9
	orl	$1073741824, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$22, %eax
	ret
	.size	pthread_mutexattr_setrobust, .-pthread_mutexattr_setrobust
	.weak	pthread_mutexattr_setrobust_np
	.set	pthread_mutexattr_setrobust_np,pthread_mutexattr_setrobust
