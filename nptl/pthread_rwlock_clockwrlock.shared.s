	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_clockwrlock
	.type	__pthread_rwlock_clockwrlock, @function
__pthread_rwlock_clockwrlock:
	testq	%rdx, %rdx
	je	.L2
	cmpl	$1, %esi
	ja	.L49
	cmpq	$999999999, 8(%rdx)
	ja	.L49
.L2:
	movl	24(%rdi), %ecx
#APP
# 603 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, %ecx
	je	.L50
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movl	(%rdi), %eax
.L4:
	movl	%eax, %ecx
	movl	%eax, %r8d
	orl	$2, %ecx
	lock cmpxchgl	%ecx, (%rdi)
	jne	.L4
	testb	$2, %r8b
	movq	%rdx, %rbp
	movl	%esi, %r12d
	movq	%rdi, %rbx
	leaq	12(%rdi), %r13
	movl	$1, %eax
	jne	.L118
.L5:
	testb	$1, %r8b
	movl	%eax, 12(%rbx)
	jne	.L23
.L21:
	movl	%r8d, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	jne	.L26
	movl	%r8d, %edx
	movl	%r8d, %eax
	orl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	movl	%eax, %r8d
	jne	.L22
	movl	$1, 8(%rbx)
.L23:
#APP
# 947 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	movl	%eax, 24(%rbx)
	xorl	%r8d, %r8d
.L1:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$22, %r8d
.L113:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	movl	$35, %r8d
	jmp	.L113
	.p2align 4,,10
	.p2align 3
.L118:
	movl	48(%rdi), %r14d
	testl	%r14d, %r14d
	jne	.L119
.L6:
	xorl	%edx, %edx
	movl	$128, %r15d
.L7:
	testb	$2, %r8b
	jne	.L8
	movl	%r8d, %ecx
	movl	%r8d, %eax
	orl	$2, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	movl	%eax, %r8d
	jne	.L7
	testl	%r14d, %r14d
	jne	.L120
.L10:
	orl	$2, %r8d
	cmpb	$1, %dl
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$3, %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%r14d, %r14d
	jne	.L121
.L11:
	movl	28(%rbx), %r8d
	movl	0(%r13), %eax
	testl	%r8d, %r8d
	movl	%eax, %ecx
	cmovne	%r15d, %r8d
	andl	$-3, %ecx
	cmpl	$1, %ecx
	jne	.L15
	cmpl	$3, %eax
	je	.L14
	movl	$3, %ecx
	lock cmpxchgl	%ecx, 0(%r13)
	jne	.L15
.L14:
	movq	%rbp, %rcx
	movl	%r12d, %edx
	movl	$3, %esi
	movq	%r13, %rdi
	call	__GI___futex_abstimed_wait64
	cmpl	$110, %eax
	movl	%eax, %r8d
	je	.L56
	cmpl	$75, %eax
	je	.L56
	movl	(%rbx), %r8d
	movl	$1, %edx
	jmp	.L7
.L22:
	testb	$1, %al
	je	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	8(%rbx), %r14
	movb	$0, 15(%rsp)
	.p2align 4,,10
	.p2align 3
.L24:
	movl	(%r14), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$2, %edx
	je	.L46
	cmpb	$0, 15(%rsp)
	jne	.L23
	movl	(%rbx), %eax
	testb	$1, %al
	je	.L24
	movl	(%r14), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$2, %edx
	jne	.L23
	movb	$1, 15(%rsp)
.L46:
	movl	28(%rbx), %r15d
	movl	$128, %edx
	testl	%r15d, %r15d
	cmovne	%edx, %r15d
	testb	$2, %al
	jne	.L31
	movl	$2, %edx
	lock cmpxchgl	%edx, (%r14)
	jne	.L24
.L31:
	movl	%r15d, %r8d
	movq	%rbp, %rcx
	movl	%r12d, %edx
	movl	$2, %esi
	movq	%r14, %rdi
	call	__GI___futex_abstimed_wait64
	cmpl	$110, %eax
	movl	%eax, %r8d
	je	.L29
	cmpl	$75, %eax
	jne	.L24
.L29:
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.L33
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	jne	.L122
.L33:
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$1, %edx
	jne	.L45
	xchgl	0(%r13), %edx
.L44:
	movl	%eax, %ecx
	xorl	$2, %ecx
	andl	$-5, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	movl	%eax, %r8d
	jne	.L39
	andl	$2, %edx
	je	.L41
	movl	%r15d, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L123
.L41:
	andl	$4, %r8d
	je	.L54
	movl	%r15d, %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	xorb	$-127, %sil
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L124
.L54:
	movl	$110, %r8d
	jmp	.L1
.L39:
	testb	$1, %al
	je	.L44
	movl	%edx, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L45:
	movl	(%r14), %eax
	orl	$2, %eax
	cmpl	$2, %eax
	jne	.L23
	movl	(%r14), %eax
	orl	$2, %eax
	cmpl	$2, %eax
	je	.L45
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L120:
	lock subl	$1, 4(%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L121:
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rcx
	testl	%eax, %eax
	jns	.L11
	leal	2147483647(%rax), %esi
	lock cmpxchgl	%esi, (%rcx)
	jne	.L7
	movl	(%rbx), %r8d
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movl	(%rbx), %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L119:
	lock addl	$1, 4(%rdi)
	jmp	.L6
.L124:
	cmpl	$-22, %eax
	je	.L54
	cmpl	$-14, %eax
	je	.L54
.L36:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	xorl	%ecx, %ecx
	xchgl	0(%r13), %ecx
.L37:
	movl	%eax, %esi
	orl	$-2147483648, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L35
	andl	$2, %ecx
	je	.L1
	movl	%r15d, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L1
	cmpl	$-22, %eax
	je	.L1
	cmpl	$-14, %eax
	je	.L1
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L123:
	cmpl	$-22, %eax
	je	.L41
	cmpl	$-14, %eax
	je	.L41
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L56:
	testl	%r14d, %r14d
	je	.L1
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
.L19:
	cmpl	$-2147483647, %eax
	je	.L52
	leal	-1(%rax), %ecx
.L18:
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L19
	cmpl	$-2147483647, %eax
	jne	.L1
	movl	(%rbx), %r8d
	movl	$3, %eax
	orl	$2, %r8d
	jmp	.L5
.L52:
	xorl	%ecx, %ecx
	jmp	.L18
.L35:
	testl	%eax, %eax
	jne	.L37
	movl	%ecx, 12(%rbx)
	jmp	.L33
	.size	__pthread_rwlock_clockwrlock, .-__pthread_rwlock_clockwrlock
	.weak	pthread_rwlock_clockwrlock
	.set	pthread_rwlock_clockwrlock,__pthread_rwlock_clockwrlock
