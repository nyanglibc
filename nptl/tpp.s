	.text
	.p2align 4,,15
	.globl	__init_sched_fifo_prio
	.type	__init_sched_fifo_prio, @function
__init_sched_fifo_prio:
	subq	$8, %rsp
	movl	$1, %edi
	call	__sched_get_priority_max@PLT
	movl	%eax, __sched_fifo_max_prio(%rip)
	movl	$1, %edi
	call	__sched_get_priority_min@PLT
	movl	%eax, __sched_fifo_min_prio(%rip)
	addq	$8, %rsp
	ret
	.size	__init_sched_fifo_prio, .-__init_sched_fifo_prio
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"tpp.c"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"new_prio == -1 || (new_prio >= fifo_min_prio && new_prio <= fifo_max_prio)"
	.align 8
.LC2:
	.string	"previous_prio == -1 || (previous_prio >= fifo_min_prio && previous_prio <= fifo_max_prio)"
	.text
	.p2align 4,,15
	.globl	__pthread_tpp_change_priority
	.type	__pthread_tpp_change_priority, @function
__pthread_tpp_change_priority:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%edi, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	subq	$24, %rsp
	movq	%fs:16, %r14
#APP
# 55 "tpp.c" 1
	movq %fs:1712,%rbx
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	movl	__sched_fifo_min_prio(%rip), %r13d
	movl	__sched_fifo_max_prio(%rip), %r15d
	je	.L87
.L5:
	cmpl	$-1, %ebp
	je	.L9
	cmpl	%ebp, %r13d
	jg	.L48
	cmpl	%ebp, %r15d
	jl	.L48
	cmpl	$-1, %r12d
	je	.L88
.L41:
	cmpl	%r12d, %r13d
	jg	.L49
	cmpl	%r12d, %r15d
	jl	.L49
	cmpl	$-1, %ebp
	movl	(%rbx), %r15d
	je	.L14
.L39:
	movl	%ebp, %eax
	subl	%r13d, %eax
	cltq
	leaq	(%rbx,%rax,4), %rdx
	movl	4(%rdx), %eax
	cmpl	$-1, %eax
	je	.L46
	addl	$1, %eax
	cmpl	%ebp, %r15d
	movl	%eax, 4(%rdx)
	jge	.L89
	cmpl	$-1, %r12d
	jne	.L90
.L20:
	leaq	1560(%r14), %r12
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%r12)
	jne	.L91
.L21:
	movl	780(%r14), %eax
	movl	%ebp, (%rbx)
	testb	$32, %al
	je	.L22
.L86:
	testb	$64, %al
	je	.L92
.L29:
	movl	1592(%r14), %eax
	cmpl	%ebp, %eax
	movl	%eax, 12(%rsp)
	jge	.L93
	movl	%ebp, 12(%rsp)
.L32:
	movl	1596(%r14), %esi
	movl	720(%r14), %edi
	leaq	12(%rsp), %rdx
	call	__sched_setscheduler@PLT
	testl	%eax, %eax
	js	.L94
.L34:
	xorl	%ebx, %ebx
.L31:
	xorl	%eax, %eax
#APP
# 154 "tpp.c" 1
	xchgl %eax, 1560(%r14)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L4
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 154 "tpp.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L89:
	cmpl	$-1, %r12d
	je	.L35
.L14:
	movl	%r12d, %eax
	subl	%r13d, %eax
	cltq
	leaq	(%rbx,%rax,4), %rdx
	subl	$1, 4(%rdx)
	jne	.L35
	cmpl	%r15d, %r12d
	jne	.L35
	cmpl	%r12d, %ebp
	jge	.L35
.L42:
	leal	-1(%r12), %ebp
	cmpl	%ebp, %r13d
	jg	.L17
	movl	%ebp, %eax
	subl	%r13d, %eax
	cltq
	movl	4(%rbx,%rax,4), %esi
	testl	%esi, %esi
	jne	.L17
	subl	$2, %r12d
	movslq	%r12d, %rax
	subl	%r13d, %r12d
	movslq	%r12d, %r12
	subq	%rax, %r12
	leaq	(%rbx,%r12,4), %rdx
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L19:
	subq	$1, %rax
	movl	8(%rdx,%rax,4), %ecx
	testl	%ecx, %ecx
	jne	.L17
.L18:
	cmpl	%eax, %r13d
	movl	%eax, %ebp
	jle	.L19
.L17:
	cmpl	%ebp, %r15d
	jne	.L20
	.p2align 4,,10
	.p2align 3
.L35:
	xorl	%ebx, %ebx
.L4:
	addq	$24, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L88:
	movl	(%rbx), %r15d
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L9:
	cmpl	$-1, %r12d
	jne	.L41
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L87:
	cmpl	$-1, %r13d
	je	.L47
	cmpl	$-1, %r15d
	je	.L47
.L6:
	movl	%r15d, %eax
	movl	$1, %esi
	subl	%r13d, %eax
	addl	$1, %eax
	cltq
	leaq	4(,%rax,4), %rdi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L45
	leal	-1(%r13), %eax
	movl	%eax, (%rbx)
#APP
# 79 "tpp.c" 1
	movq %rbx,%fs:1712
# 0 "" 2
#NO_APP
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L93:
	cmpl	%r15d, %eax
	jl	.L32
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L94:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L22:
	movl	720(%r14), %edi
	leaq	1592(%r14), %rsi
	call	__sched_getparam@PLT
	testl	%eax, %eax
	je	.L27
	testb	$64, 780(%r14)
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	je	.L26
.L28:
	testl	%ebx, %ebx
	jne	.L31
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L92:
	xorl	%ebx, %ebx
.L26:
	movl	720(%r14), %edi
	call	__sched_getscheduler@PLT
	cmpl	$-1, %eax
	movl	%eax, 1596(%r14)
	je	.L95
	orl	$64, 780(%r14)
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L46:
	movl	$11, %ebx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L47:
	call	__init_sched_fifo_prio
	movl	__sched_fifo_min_prio(%rip), %r13d
	movl	__sched_fifo_max_prio(%rip), %r15d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L27:
	movl	780(%r14), %eax
	movl	%eax, %edx
	orl	$32, %edx
	movl	%edx, 780(%r14)
	jmp	.L86
	.p2align 4,,10
	.p2align 3
.L95:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L45:
	movl	$12, %ebx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L91:
	movq	%r12, %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L21
.L49:
	leaq	__PRETTY_FUNCTION__.7807(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$87, %edx
	call	__assert_fail@PLT
.L48:
	leaq	__PRETTY_FUNCTION__.7807(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$84, %edx
	call	__assert_fail@PLT
.L90:
	movl	%r12d, %eax
	subl	%r13d, %eax
	cltq
	leaq	(%rbx,%rax,4), %rdx
	subl	$1, 4(%rdx)
	jne	.L20
	cmpl	%r12d, %ebp
	jge	.L20
	cmpl	%r12d, %r15d
	je	.L42
	jmp	.L20
	.size	__pthread_tpp_change_priority, .-__pthread_tpp_change_priority
	.p2align 4,,15
	.globl	__pthread_current_priority
	.type	__pthread_current_priority, @function
__pthread_current_priority:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	%fs:16, %rbx
	movl	780(%rbx), %eax
	andl	$96, %eax
	cmpl	$96, %eax
	je	.L115
	leaq	1560(%rbx), %rbp
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	jne	.L116
.L99:
	movl	780(%rbx), %eax
	testb	$32, %al
	je	.L117
.L100:
	testb	$64, %al
	je	.L118
.L107:
	movl	1592(%rbx), %r8d
.L103:
	xorl	%eax, %eax
#APP
# 192 "tpp.c" 1
	xchgl %eax, 1560(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L119
.L96:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L115:
	movl	1592(%rbx), %r8d
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L117:
	movl	720(%rbx), %edi
	leaq	1592(%rbx), %rsi
	call	__sched_getparam@PLT
	testl	%eax, %eax
	jne	.L101
	movl	780(%rbx), %eax
	orl	$32, %eax
	movl	%eax, 780(%rbx)
	jmp	.L100
	.p2align 4,,10
	.p2align 3
.L116:
	movq	%rbp, %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L99
	.p2align 4,,10
	.p2align 3
.L118:
	movl	720(%rbx), %edi
	call	__sched_getscheduler@PLT
	cmpl	$-1, %eax
	movl	%eax, 1596(%rbx)
	jne	.L120
.L114:
	movl	$-1, %r8d
	jmp	.L103
	.p2align 4,,10
	.p2align 3
.L101:
	testb	$64, 780(%rbx)
	jne	.L114
	movl	720(%rbx), %edi
	call	__sched_getscheduler@PLT
	cmpl	$-1, %eax
	movl	%eax, 1596(%rbx)
	je	.L114
	orl	$64, 780(%rbx)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L119:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 192 "tpp.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L96
.L120:
	orl	$64, 780(%rbx)
	jmp	.L107
	.size	__pthread_current_priority, .-__pthread_current_priority
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.7807, @object
	.size	__PRETTY_FUNCTION__.7807, 30
__PRETTY_FUNCTION__.7807:
	.string	"__pthread_tpp_change_priority"
	.globl	__sched_fifo_max_prio
	.data
	.align 4
	.type	__sched_fifo_max_prio, @object
	.size	__sched_fifo_max_prio, 4
__sched_fifo_max_prio:
	.long	-1
	.globl	__sched_fifo_min_prio
	.align 4
	.type	__sched_fifo_min_prio, @object
	.size	__sched_fifo_min_prio, 4
__sched_fifo_min_prio:
	.long	-1
