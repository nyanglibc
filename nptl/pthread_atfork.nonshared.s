	.text
	.p2align 4,,15
	.globl	__pthread_atfork
	.hidden	__pthread_atfork
	.type	__pthread_atfork, @function
__pthread_atfork:
	movq	__dso_handle(%rip), %rcx
	jmp	__register_atfork@PLT
	.size	__pthread_atfork, .-__pthread_atfork
	.weak	pthread_atfork
	.hidden	pthread_atfork
	.set	pthread_atfork,__pthread_atfork
	.hidden	__dso_handle
