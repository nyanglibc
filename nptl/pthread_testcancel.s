	.text
	.p2align 4,,15
	.globl	__pthread_testcancel
	.type	__pthread_testcancel, @function
__pthread_testcancel:
#APP
# 26 "pthread_testcancel.c" 1
	movl %fs:776,%eax
# 0 "" 2
#NO_APP
	andl	$-71, %eax
	cmpl	$8, %eax
	je	.L7
	rep ret
.L7:
	subq	$8, %rsp
#APP
# 26 "pthread_testcancel.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
#APP
# 304 "pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__pthread_unwind@PLT
	.size	__pthread_testcancel, .-__pthread_testcancel
	.globl	pthread_testcancel
	.set	pthread_testcancel,__pthread_testcancel
