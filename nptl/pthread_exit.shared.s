	.text
	.p2align 4,,15
	.globl	__pthread_exit
	.type	__pthread_exit, @function
__pthread_exit:
	subq	$8, %rsp
#APP
# 26 "pthread_exit.c" 1
	movq %rdi,%fs:1584
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
#APP
# 304 "pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__GI___pthread_unwind
	.size	__pthread_exit, .-__pthread_exit
	.weak	pthread_exit
	.set	pthread_exit,__pthread_exit
