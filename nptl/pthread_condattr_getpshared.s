	.text
	.p2align 4,,15
	.globl	pthread_condattr_getpshared
	.type	pthread_condattr_getpshared, @function
pthread_condattr_getpshared:
	movl	(%rdi), %eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_condattr_getpshared, .-pthread_condattr_getpshared
