	.text
	.p2align 4,,15
	.globl	__new_sem_destroy
	.type	__new_sem_destroy, @function
__new_sem_destroy:
	xorl	%eax, %eax
	ret
	.size	__new_sem_destroy, .-__new_sem_destroy
	.weak	sem_destroy
	.set	sem_destroy,__new_sem_destroy
