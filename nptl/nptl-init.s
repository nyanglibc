	.text
	.p2align 4,,15
	.type	sigcancel_handler, @function
sigcancel_handler:
.LFB97:
	cmpl	$32, %edi
	je	.L13
	rep ret
	.p2align 4,,10
	.p2align 3
.L13:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %ebp
	call	__getpid@PLT
	cmpl	%eax, %ebp
	je	.L14
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	cmpl	$-6, 8(%rbx)
	jne	.L1
	movq	%fs:16, %rsi
#APP
# 139 "nptl-init.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L16:
	testb	$16, %dl
	jne	.L1
	movl	%edx, %eax
	lock cmpxchgl	%ecx, 776(%rsi)
	cmpl	%eax, %edx
	je	.L15
	movl	%eax, %edx
.L3:
	movl	%edx, %ecx
	orl	$12, %ecx
	cmpl	%ecx, %edx
	jne	.L16
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L15:
#APP
# 156 "nptl-init.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	andl	$2, %edx
	je	.L1
	movq	%fs:16, %rax
#APP
# 304 "./pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "./pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__pthread_unwind@PLT
.LFE97:
	.size	sigcancel_handler, .-sigcancel_handler
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	sighandler_setxid, @function
sighandler_setxid:
.LFB98:
	cmpl	$33, %edi
	je	.L30
	rep ret
	.p2align 4,,10
	.p2align 3
.L30:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$8, %rsp
	movl	16(%rsi), %ebp
	call	__getpid@PLT
	cmpl	%eax, %ebp
	je	.L31
.L17:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	cmpl	$-6, 8(%rbx)
	jne	.L17
	movq	__xidcmd(%rip), %rax
	movq	16(%rax), %rsi
	movq	8(%rax), %rdi
	movq	24(%rax), %rdx
	movl	(%rax), %eax
#APP
# 190 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	__xidcmd(%rip), %rdi
	movl	%eax, %esi
	negl	%esi
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmovbe	%eax, %esi
	call	__nptl_setxid_error@PLT
	movq	%fs:16, %rdi
	leaq	776(%rdi), %rcx
	.p2align 4,,10
	.p2align 3
.L20:
#APP
# 202 "nptl-init.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$-65, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	jne	.L20
	movl	$1, 1564(%rdi)
	xorl	%r10d, %r10d
	addq	$1564, %rdi
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L32
.L21:
	movq	__xidcmd(%rip), %rax
	movl	$-1, %edx
	lock xaddl	%edx, 32(%rax)
	cmpl	$1, %edx
	jne	.L17
	movq	__xidcmd(%rip), %rax
	xorl	%r10d, %r10d
	movl	$129, %esi
	leaq	32(%rax), %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L17
	cmpl	$-22, %eax
	je	.L17
	cmpl	$-14, %eax
	je	.L17
.L22:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$-22, %eax
	je	.L21
	cmpl	$-14, %eax
	je	.L21
	jmp	.L22
.LFE98:
	.size	sighandler_setxid, .-sighandler_setxid
	.p2align 4,,15
	.globl	__nptl_set_robust
	.type	__nptl_set_robust, @function
__nptl_set_robust:
.LFB96:
	addq	$736, %rdi
	movl	$24, %esi
	movl	$273, %eax
#APP
# 119 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	ret
.LFE96:
	.size	__nptl_set_robust, .-__nptl_set_robust
	.p2align 4,,15
	.globl	__pthread_initialize_minimal_internal
	.type	__pthread_initialize_minimal_internal, @function
__pthread_initialize_minimal_internal:
.LFB99:
	pushq	%rbx
	movl	$218, %eax
	subq	$192, %rsp
	movq	%fs:16, %rdx
	leaq	720(%rdx), %rdi
#APP
# 28 "../sysdeps/unix/sysv/linux/pthread-pids.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, 720(%rdx)
	leaq	784(%rdx), %rax
#APP
# 231 "nptl-init.c" 1
	movq %rax,%fs:1296
# 0 "" 2
# 232 "nptl-init.c" 1
	movb $1,%fs:1554
# 0 "" 2
#NO_APP
	leaq	736(%rdx), %rdi
	movq	$-32, 744(%rdx)
	movl	$24, %esi
	movl	$273, %eax
	movq	%rdi, 728(%rdx)
	movq	%rdi, 736(%rdx)
#APP
# 243 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	__libc_stack_end(%rip), %rax
#APP
# 252 "nptl-init.c" 1
	movq %rax,%fs:1688
# 0 "" 2
#NO_APP
	movzbl	__nptl_initial_report_events(%rip), %eax
#APP
# 257 "nptl-init.c" 1
	movb %al,%fs:1553
# 0 "" 2
#NO_APP
	leaq	32(%rsp), %rbx
	leaq	sigcancel_handler(%rip), %rax
	xorl	%edx, %edx
	movl	$32, %edi
	movq	$0, 40(%rsp)
	movl	$4, 168(%rsp)
	movq	%rbx, %rsi
	movq	%rax, 32(%rsp)
	call	__libc_sigaction@PLT
	leaq	sighandler_setxid(%rip), %rax
	xorl	%edx, %edx
	movq	%rbx, %rsi
	movl	$33, %edi
	movl	$268435460, 168(%rsp)
	movq	%rax, 32(%rsp)
	call	__libc_sigaction@PLT
	movabsq	$6442450944, %rax
	leaq	8(%rbx), %rsi
	orq	%rax, 40(%rsp)
	movl	$8, %r10d
	xorl	%edx, %edx
	movl	$1, %edi
	movl	$14, %eax
#APP
# 279 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	__static_tls_size(%rip), %rdi
	leaq	8(%rsp), %rsi
	call	_dl_get_tls_static_info@PLT
	movq	8(%rsp), %rcx
	cmpq	$15, %rcx
	leaq	-1(%rcx), %rax
	ja	.L36
	movq	$16, 8(%rsp)
	movl	$15, %eax
	movl	$16, %ecx
.L36:
	movq	%rax, __static_tls_align_m1(%rip)
	movq	__static_tls_size(%rip), %rax
	xorl	%edx, %edx
	leaq	16(%rsp), %rsi
	movl	$3, %edi
	leaq	-1(%rcx,%rax), %rax
	divq	%rcx
	imulq	%rcx, %rax
	movq	%rax, __static_tls_size(%rip)
	call	__getrlimit@PLT
	testl	%eax, %eax
	jne	.L37
	movq	16(%rsp), %rcx
	cmpq	$-1, %rcx
	je	.L37
	cmpq	$16383, %rcx
	movl	$16384, %eax
	cmovbe	%rax, %rcx
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L37:
	movl	$2097152, %ecx
.L39:
	movq	_dl_pagesize(%rip), %rdx
	movq	__static_tls_size(%rip), %rax
	addq	%rdx, %rax
	addq	$2048, %rax
	cmpq	%rcx, %rax
	cmovb	%rcx, %rax
	leaq	-1(%rdx,%rax), %rax
	negq	%rdx
	andq	%rax, %rdx
	xorl	%eax, %eax
	movq	%rdx, 16(%rsp)
	movl	$1, %edx
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L43
.L40:
	movq	16(%rsp), %rax
	movq	%rax, 32+__default_pthread_attr(%rip)
	movq	_dl_pagesize(%rip), %rax
	movq	%rax, 16+__default_pthread_attr(%rip)
	xorl	%eax, %eax
#APP
# 319 "nptl-init.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L44
.L41:
	movq	__pthread_init_static_tls@GOTPCREL(%rip), %rax
	movq	__reclaim_stacks@GOTPCREL(%rip), %rsi
	leaq	__fork_generation(%rip), %rdi
	xorl	%edx, %edx
	movq	%rax, _dl_init_static_tls(%rip)
	call	__libc_pthread_init@PLT
	movq	%rax, __libc_multiple_threads_ptr(%rip)
	call	__pthread_tunables_init@PLT
	addq	$192, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L44:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 319 "nptl-init.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L40
.LFE99:
	.size	__pthread_initialize_minimal_internal, .-__pthread_initialize_minimal_internal
	.weak	__pthread_initialize_minimal
	.set	__pthread_initialize_minimal,__pthread_initialize_minimal_internal
	.p2align 4,,15
	.globl	__pthread_get_minstack
	.type	__pthread_get_minstack, @function
__pthread_get_minstack:
.LFB100:
	movq	__static_tls_size(%rip), %rax
	addq	_dl_pagesize(%rip), %rax
	addq	$16384, %rax
	ret
.LFE100:
	.size	__pthread_get_minstack, .-__pthread_get_minstack
	.local	__nptl_initial_report_events
	.comm	__nptl_initial_report_events,1,1
	.comm	__xidcmd,8,8
	.section	.rodata.str1.1,"aMS",@progbits,1
	.type	nptl_version, @object
	.size	nptl_version, 5
nptl_version:
	.string	"2.33"
	.comm	__static_tls_align_m1,8,8
	.comm	__static_tls_size,8,8
	.comm	__libc_multiple_threads_ptr,8,8
