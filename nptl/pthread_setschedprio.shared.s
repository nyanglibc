	.text
	.p2align 4,,15
	.globl	pthread_setschedprio
	.type	pthread_setschedprio, @function
pthread_setschedprio:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	jle	.L7
	pushq	%r12
	pushq	%rbp
	leaq	1560(%rdi), %rbp
	pushq	%rbx
	movl	%esi, %r12d
	movq	%rdi, %rbx
	xorl	%eax, %eax
	movl	$1, %edx
	subq	$16, %rsp
	movl	%esi, 12(%rsp)
	lock cmpxchgl	%edx, 0(%rbp)
	jne	.L16
.L3:
	movq	1712(%rbx), %rax
	testq	%rax, %rax
	jne	.L17
.L4:
	movl	720(%rbx), %edi
	leaq	12(%rsp), %rsi
	call	sched_setparam@PLT
	cmpl	$-1, %eax
	je	.L18
	orl	$32, 780(%rbx)
	movl	%r12d, 12(%rsp)
	xorl	%r8d, %r8d
	movl	%r12d, 1592(%rbx)
.L6:
	xorl	%eax, %eax
#APP
# 61 "pthread_setschedprio.c" 1
	xchgl %eax, 0(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L19
.L1:
	addq	$16, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$3, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L17:
	movl	(%rax), %eax
	cmpl	%r12d, %eax
	jle	.L4
	movl	%eax, 12(%rsp)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r8d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L19:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 61 "pthread_setschedprio.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	pthread_setschedprio, .-pthread_setschedprio
	.hidden	__lll_lock_wait_private
