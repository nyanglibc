	.text
	.p2align 4,,15
	.globl	pthread_condattr_setpshared
	.type	pthread_condattr_setpshared, @function
pthread_condattr_setpshared:
	cmpl	$1, %esi
	ja	.L3
	movl	(%rdi), %eax
	andl	$-2, %eax
	orl	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_condattr_setpshared, .-pthread_condattr_setpshared
