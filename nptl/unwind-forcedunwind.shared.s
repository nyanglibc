	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"libgcc_s.so.1"
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC1:
	.string	"libgcc_s.so.1 must be installed for pthread_cancel to work\n"
	.section	.rodata.str1.1
.LC2:
	.string	"_Unwind_Resume"
.LC3:
	.string	"__gcc_personality_v0"
.LC4:
	.string	"_Unwind_ForcedUnwind"
.LC5:
	.string	"_Unwind_GetCFA"
	.text
	.p2align 4,,15
	.globl	pthread_cancel_init
	.hidden	pthread_cancel_init
	.type	pthread_cancel_init, @function
pthread_cancel_init:
	cmpq	$0, libgcc_s_handle(%rip)
	je	.L2
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	leaq	.LC0(%rip), %rdi
	pushq	%rbp
	pushq	%rbx
	movl	$-2147483646, %esi
	subq	$8, %rsp
	call	__libc_dlopen_mode@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L5
	leaq	.LC2(%rip), %rsi
	movq	%rax, %rdi
	call	__libc_dlsym@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L5
	leaq	.LC3(%rip), %rsi
	movq	%rbx, %rdi
	call	__libc_dlsym@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L5
	leaq	.LC4(%rip), %rsi
	movq	%rbx, %rdi
	call	__libc_dlsym@PLT
	testq	%rax, %rax
	movq	%rax, %r12
	je	.L5
	leaq	.LC5(%rip), %rsi
	movq	%rbx, %rdi
	call	__libc_dlsym@PLT
	testq	%rax, %rax
	je	.L5
	movq	%rbp, %rdx
#APP
# 73 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
# 67 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
#NO_APP
	movq	%rax, libgcc_s_getcfa(%rip)
	movq	%rdx, __libgcc_s_resume(%rip)
	movq	%r13, %rdx
#APP
# 69 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
#NO_APP
	movq	%rdx, libgcc_s_personality(%rip)
	movq	%r12, %rdx
#APP
# 71 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	xor %fs:48, %rdx
rol $2*8+1, %rdx
# 0 "" 2
#NO_APP
	movq	%rdx, libgcc_s_forcedunwind(%rip)
	movq	%rbx, libgcc_s_handle(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L5:
	leaq	.LC1(%rip), %rdi
	call	__libc_fatal@PLT
	.size	pthread_cancel_init, .-pthread_cancel_init
	.p2align 4,,15
	.globl	__nptl_unwind_freeres
	.hidden	__nptl_unwind_freeres
	.type	__nptl_unwind_freeres, @function
__nptl_unwind_freeres:
	movq	libgcc_s_handle(%rip), %rdi
	testq	%rdi, %rdi
	je	.L22
	movq	$0, libgcc_s_handle(%rip)
	jmp	__libc_dlclose@PLT
	.p2align 4,,10
	.p2align 3
.L22:
	rep ret
	.size	__nptl_unwind_freeres, .-__nptl_unwind_freeres
	.p2align 4,,15
	.globl	_Unwind_Resume
	.type	_Unwind_Resume, @function
_Unwind_Resume:
	subq	$24, %rsp
	cmpq	$0, libgcc_s_handle(%rip)
	je	.L28
.L26:
	movq	__libgcc_s_resume(%rip), %rax
	addq	$24, %rsp
#APP
# 104 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L28:
	movq	%rdi, 8(%rsp)
	call	pthread_cancel_init
	movq	8(%rsp), %rdi
	jmp	.L26
	.size	_Unwind_Resume, .-_Unwind_Resume
	.p2align 4,,15
	.globl	__gcc_personality_v0
	.type	__gcc_personality_v0, @function
__gcc_personality_v0:
	subq	$40, %rsp
	cmpq	$0, libgcc_s_handle(%rip)
	je	.L33
.L31:
	movq	libgcc_s_personality(%rip), %rax
	addq	$40, %rsp
#APP
# 118 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%r8, 24(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movl	%esi, 4(%rsp)
	movl	%edi, (%rsp)
	call	pthread_cancel_init
	movl	(%rsp), %edi
	movl	4(%rsp), %esi
	movq	8(%rsp), %rdx
	movq	16(%rsp), %rcx
	movq	24(%rsp), %r8
	jmp	.L31
	.size	__gcc_personality_v0, .-__gcc_personality_v0
	.p2align 4,,15
	.globl	_Unwind_ForcedUnwind
	.type	_Unwind_ForcedUnwind, @function
_Unwind_ForcedUnwind:
	subq	$40, %rsp
	cmpq	$0, libgcc_s_handle(%rip)
	je	.L38
.L36:
	movq	libgcc_s_forcedunwind(%rip), %rax
	addq	$40, %rsp
#APP
# 134 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L38:
	movq	%rdx, 24(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 8(%rsp)
	call	pthread_cancel_init
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	movq	24(%rsp), %rdx
	jmp	.L36
	.size	_Unwind_ForcedUnwind, .-_Unwind_ForcedUnwind
	.p2align 4,,15
	.globl	_Unwind_GetCFA
	.type	_Unwind_GetCFA, @function
_Unwind_GetCFA:
	subq	$24, %rsp
	cmpq	$0, libgcc_s_handle(%rip)
	je	.L43
.L41:
	movq	libgcc_s_getcfa(%rip), %rax
	addq	$24, %rsp
#APP
# 147 "../sysdeps/nptl/unwind-forcedunwind.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rdi, 8(%rsp)
	call	pthread_cancel_init
	movq	8(%rsp), %rdi
	jmp	.L41
	.size	_Unwind_GetCFA, .-_Unwind_GetCFA
	.local	libgcc_s_getcfa
	.comm	libgcc_s_getcfa,8,8
	.local	libgcc_s_forcedunwind
	.comm	libgcc_s_forcedunwind,8,8
	.local	libgcc_s_personality
	.comm	libgcc_s_personality,8,8
	.hidden	__libgcc_s_resume
	.comm	__libgcc_s_resume,8,8
	.local	libgcc_s_handle
	.comm	libgcc_s_handle,8,8
