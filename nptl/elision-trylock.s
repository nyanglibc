	.text
	.p2align 4,,15
	.globl	__lll_trylock_elision
	.type	__lll_trylock_elision, @function
__lll_trylock_elision:
	xabort	$253
	movzwl	(%rsi), %eax
	testw	%ax, %ax
	jg	.L2
	movl	$-1, %eax
	xbegin	.L3
.L3:
	cmpl	$-1, %eax
	je	.L13
	testb	$2, %al
	je	.L14
.L6:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	setne	%al
	movzbl	%al, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	movzwl	(%rsi), %eax
	subl	$1, %eax
	movw	%ax, (%rsi)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L13:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L1
	xabort	$255
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	movswl	(%rsi), %eax
	movl	4+__elision_aconf(%rip), %edx
	cmpl	%edx, %eax
	je	.L6
	movw	%dx, (%rsi)
	jmp	.L6
	.size	__lll_trylock_elision, .-__lll_trylock_elision
