	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_getscope
	.type	__pthread_attr_getscope, @function
__pthread_attr_getscope:
	movl	8(%rdi), %eax
	sarl	$2, %eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getscope, .-__pthread_attr_getscope
	.globl	pthread_attr_getscope
	.set	pthread_attr_getscope,__pthread_attr_getscope
