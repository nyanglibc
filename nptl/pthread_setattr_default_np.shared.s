	.text
	.p2align 4,,15
	.globl	pthread_setattr_default_np
	.type	pthread_setattr_default_np, @function
pthread_setattr_default_np:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$72, %rsp
	movl	4(%rdi), %ebp
	cmpl	$2, %ebp
	ja	.L6
	movl	(%rdi), %r12d
	movq	%rdi, %rbx
	testl	%r12d, %r12d
	jg	.L4
.L7:
	movq	32(%rbx), %rax
	subq	$1, %rax
	cmpq	$16382, %rax
	jbe	.L6
	testb	$8, 8(%rbx)
	jne	.L6
	movq	%rbx, %rsi
	movq	%rsp, %rdi
	call	__pthread_attr_copy@PLT
	testl	%eax, %eax
	movl	%eax, %ebx
	jne	.L1
	movl	$1, %edx
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L16
.L9:
	cmpq	$0, 32(%rsp)
	jne	.L10
	movq	32+__default_pthread_attr(%rip), %rax
	movq	%rax, 32(%rsp)
.L10:
	leaq	__default_pthread_attr(%rip), %rdi
	call	__pthread_attr_destroy@PLT
	movdqa	(%rsp), %xmm0
	movq	48(%rsp), %rax
	movups	%xmm0, __default_pthread_attr(%rip)
	movdqa	16(%rsp), %xmm0
	movq	%rax, 48+__default_pthread_attr(%rip)
	xorl	%eax, %eax
	movups	%xmm0, 16+__default_pthread_attr(%rip)
	movdqa	32(%rsp), %xmm0
	movups	%xmm0, 32+__default_pthread_attr(%rip)
#APP
# 81 "pthread_setattr_default_np.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 81 "pthread_setattr_default_np.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%ebp, %edi
	call	__sched_get_priority_min@PLT
	movl	%ebp, %edi
	movl	%eax, %r13d
	call	__sched_get_priority_max@PLT
	testl	%r13d, %r13d
	js	.L6
	testl	%eax, %eax
	js	.L6
	cmpl	%r13d, %r12d
	jl	.L6
	cmpl	%eax, %r12d
	jle	.L7
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$22, %ebx
.L1:
	addq	$72, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L16:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L9
	.size	pthread_setattr_default_np, .-pthread_setattr_default_np
	.p2align 4,,15
	.globl	__default_pthread_attr_freeres
	.hidden	__default_pthread_attr_freeres
	.type	__default_pthread_attr_freeres, @function
__default_pthread_attr_freeres:
	leaq	__default_pthread_attr(%rip), %rdi
	jmp	__pthread_attr_destroy@PLT
	.size	__default_pthread_attr_freeres, .-__default_pthread_attr_freeres
	.hidden	__lll_lock_wait_private
	.hidden	__default_pthread_attr
	.hidden	__default_pthread_attr_lock
