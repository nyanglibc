	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	__condvar_confirm_wakeup, @function
__condvar_confirm_wakeup:
	addq	$36, %rdi
	movl	$-8, %eax
	lock xaddl	%eax, (%rdi)
	shrl	$2, %eax
	cmpl	$3, %eax
	je	.L9
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorb	$-127, %sil
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L1
	cmpl	$-22, %eax
	je	.L1
	cmpl	$-14, %eax
	je	.L1
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	__condvar_confirm_wakeup, .-__condvar_confirm_wakeup
	.p2align 4,,15
	.type	__condvar_release_lock, @function
__condvar_release_lock:
	movl	32(%rdi), %eax
	leaq	32(%rdi), %rcx
.L11:
	movl	%eax, %edi
	movl	%eax, %edx
	andl	$-4, %edi
	lock cmpxchgl	%edi, (%rcx)
	jne	.L11
	andl	$3, %edx
	cmpl	$2, %edx
	je	.L20
.L10:
	rep ret
	.p2align 4,,10
	.p2align 3
.L20:
	xorb	$-127, %sil
	xorl	%r10d, %r10d
	movl	$1, %edx
	movq	%rcx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L10
	cmpl	$-22, %eax
	je	.L10
	cmpl	$-14, %eax
	je	.L10
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	__condvar_release_lock, .-__condvar_release_lock
	.p2align 4,,15
	.type	__condvar_dec_grefs, @function
__condvar_dec_grefs:
	movl	%esi, %esi
	movl	$-2, %eax
	leaq	16(%rdi,%rsi,4), %rdi
	lock xaddl	%eax, (%rdi)
	cmpl	$3, %eax
	je	.L28
.L21:
	rep ret
	.p2align 4,,10
	.p2align 3
.L28:
	lock andl	$-2, (%rdi)
	xorl	%r10d, %r10d
	movl	$202, %eax
	xorb	$-127, %dl
	movl	%edx, %esi
	movl	$2147483647, %edx
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L21
	cmpl	$-22, %eax
	je	.L21
	cmpl	$-14, %eax
	je	.L21
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	__condvar_dec_grefs, .-__condvar_dec_grefs
	.p2align 4,,15
	.type	__condvar_cancel_waiting, @function
__condvar_cancel_waiting:
	pushq	%r15
	pushq	%r14
	leaq	32(%rdi), %r8
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r9
	pushq	%rbp
	pushq	%rbx
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movl	%ecx, %r14d
	subq	$8, %rsp
	movl	32(%rdi), %eax
.L30:
	testb	$3, %al
	jne	.L54
	movl	%eax, %edx
	orl	$1, %edx
	lock cmpxchgl	%edx, (%r8)
	jne	.L30
.L31:
	movq	8(%rbx), %rax
	shrq	%rax
	cmpq	%r9, %rax
	ja	.L42
	movl	32(%rbx), %edx
	shrl	$2, %edx
	addq	%rdx, %rax
	leaq	(%rbx,%rbp,4), %rdx
	cmpq	%r9, %rax
	movl	24(%rdx), %eax
	ja	.L40
	cmpl	$-536870912, %eax
	je	.L41
.L53:
	subl	$1, %eax
	movl	%r14d, %esi
	movq	%rbx, %rdi
	movl	%eax, 24(%rdx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__condvar_release_lock
	.p2align 4,,10
	.p2align 3
.L40:
	testl	%eax, %eax
	jne	.L53
.L42:
	movq	%rbx, %rdi
	movl	%r14d, %esi
	call	__condvar_release_lock
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__pthread_cond_signal@PLT
	.p2align 4,,10
	.p2align 3
.L54:
	movl	%r14d, %r13d
	movl	$202, %r12d
	movl	$1, %r15d
	xorb	$-128, %r13b
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L37:
	movl	(%r8), %eax
.L33:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-4, %edx
	andl	$3, %ecx
	orl	$2, %edx
	cmpl	$2, %ecx
	je	.L35
	lock cmpxchgl	%edx, (%r8)
	jne	.L33
	testb	$3, %al
	je	.L31
	andl	$-4, %eax
	orl	$2, %eax
	movl	%eax, %edx
.L35:
	xorl	%r10d, %r10d
	movl	%r13d, %esi
	movq	%r8, %rdi
	movl	%r12d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L37
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L38
	movq	%r15, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L37
.L38:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	movq	%rbx, %rdi
	movl	%r14d, %esi
	call	__condvar_release_lock
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	__pthread_cond_broadcast@PLT
	.size	__condvar_cancel_waiting, .-__condvar_cancel_waiting
	.p2align 4,,15
	.type	__condvar_cleanup_waiting, @function
__condvar_cleanup_waiting:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %ebp
	movq	%rdi, %rbx
	movq	8(%rdi), %r12
	movl	24(%rdi), %edx
	andl	$1, %ebp
	movl	%ebp, %esi
	movq	%r12, %rdi
	call	__condvar_dec_grefs
	movq	(%rbx), %rsi
	movl	24(%rbx), %ecx
	movl	%ebp, %edx
	movq	%r12, %rdi
	shrq	%rsi
	call	__condvar_cancel_waiting
	movl	24(%rbx), %esi
	leaq	40(%r12,%rbp,4), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$202, %eax
	xorb	$-127, %sil
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L59
.L56:
	movl	24(%rbx), %esi
	movq	%r12, %rdi
	call	__condvar_confirm_wakeup
	movq	16(%rbx), %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__pthread_mutex_cond_lock@PLT
	.p2align 4,,10
	.p2align 3
.L59:
	cmpl	$-22, %eax
	je	.L56
	cmpl	$-14, %eax
	je	.L56
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.size	__condvar_cleanup_waiting, .-__condvar_cleanup_waiting
	.p2align 4,,15
	.globl	__pthread_cond_wait
	.type	__pthread_cond_wait, @function
__pthread_cond_wait:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	$2, %r12d
	movq	%rdi, %rbx
	subq	$120, %rsp
	lock xaddq	%r12, (%rdi)
	movl	$8, %ebp
	movl	%r12d, %eax
	andl	$1, %eax
	movl	%eax, 8(%rsp)
	movq	%r12, %rax
	shrq	%rax
	movq	%rax, (%rsp)
	lock xaddl	%ebp, 36(%rdi)
	movl	$128, %eax
	andl	$1, %ebp
	movq	%r13, %rdi
	cmovne	%eax, %ebp
	xorl	%esi, %esi
	call	__pthread_mutex_unlock_usercnt@PLT
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L87
	movl	8(%rsp), %r11d
	leaq	80(%rsp), %rcx
	leaq	0(,%r11,4), %r10
	leaq	40(%rbx,%r10), %r15
	movl	(%r15), %eax
	movq	%rcx, 24(%rsp)
	leaq	48(%rsp), %rcx
	movl	%r14d, 12(%rsp)
	movq	%r12, %r14
	movq	%r10, %r12
	movq	%rcx, 16(%rsp)
.L64:
	testb	$1, %al
	jne	.L85
	testl	%eax, %eax
	je	.L88
	leal	-2(%rax), %edx
	lock cmpxchgl	%edx, (%r15)
	jne	.L64
	movl	12(%rsp), %r14d
	movq	8(%rbx), %rdx
	leaq	8(%rbx), %rcx
	movq	%rdx, %rax
	shrq	%rax
	cmpq	%rax, (%rsp)
	jb	.L89
.L72:
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__condvar_confirm_wakeup
	movq	%r13, %rdi
	call	__pthread_mutex_cond_lock@PLT
	testl	%eax, %eax
	cmove	%r14d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	cmpq	%rax, %r11
	jne	.L72
	movl	(%r15), %eax
.L76:
	movq	(%rcx), %rsi
	cmpq	%rsi, %rdx
	jne	.L72
	testb	$1, %al
	jne	.L79
	leal	2(%rax), %esi
	lock cmpxchgl	%esi, (%r15)
	jne	.L76
.L79:
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r15, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L72
	cmpl	$-22, %eax
	je	.L72
	cmpl	$-14, %eax
	je	.L72
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L88:
	lock addl	$2, 16(%rbx,%r12)
	movl	(%r15), %eax
	testb	$1, %al
	jne	.L68
	movq	%r11, 40(%rsp)
	movq	8(%rbx), %rax
	shrq	%rax
	cmpq	%rax, (%rsp)
	jb	.L68
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rdi
	leaq	__condvar_cleanup_waiting(%rip), %rsi
	movq	%r14, 80(%rsp)
	movq	%rbx, 88(%rsp)
	movq	%r13, 96(%rsp)
	movl	%ebp, 104(%rsp)
	call	__pthread_cleanup_push@PLT
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%ebp, %r8d
	movq	%r15, %rdi
	call	__futex_abstimed_wait_cancelable64@PLT
	movq	16(%rsp), %rdi
	xorl	%esi, %esi
	movl	%eax, 32(%rsp)
	call	__pthread_cleanup_pop@PLT
	movl	32(%rsp), %eax
	cmpl	$110, %eax
	je	.L82
	cmpl	$75, %eax
	movq	40(%rsp), %r11
	je	.L82
	movl	8(%rsp), %esi
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movq	%r11, 32(%rsp)
	call	__condvar_dec_grefs
	movl	(%r15), %eax
	movq	32(%rsp), %r11
	jmp	.L64
	.p2align 4,,10
	.p2align 3
.L87:
	movl	8(%rsp), %edx
	movq	(%rsp), %rsi
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	call	__condvar_cancel_waiting
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__condvar_confirm_wakeup
	addq	$120, %rsp
	movl	%r14d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L82:
	movl	8(%rsp), %r14d
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movl	%eax, 12(%rsp)
	movl	%r14d, %esi
	call	__condvar_dec_grefs
	movq	(%rsp), %rsi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	call	__condvar_cancel_waiting
	movl	12(%rsp), %eax
	movl	%eax, %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L85:
	movl	12(%rsp), %r14d
	jmp	.L72
	.p2align 4,,10
	.p2align 3
.L68:
	movl	8(%rsp), %esi
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movl	12(%rsp), %r14d
	call	__condvar_dec_grefs
	jmp	.L72
	.size	__pthread_cond_wait, .-__pthread_cond_wait
	.weak	pthread_cond_wait
	.set	pthread_cond_wait,__pthread_cond_wait
	.p2align 4,,15
	.globl	__pthread_cond_timedwait
	.type	__pthread_cond_timedwait, @function
__pthread_cond_timedwait:
	cmpq	$999999999, 8(%rdx)
	ja	.L112
	pushq	%r15
	pushq	%r14
	leaq	36(%rdi), %rax
	pushq	%r13
	pushq	%r12
	movl	$2, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rdx, 32(%rsp)
	movl	36(%rdi), %edx
	shrl	%edx
	andl	$1, %edx
	movl	%edx, 28(%rsp)
	lock xaddq	%r13, (%rdi)
	movl	$8, %ebp
	movl	%r13d, %ecx
	andl	$1, %ecx
	movl	%ecx, 4(%rsp)
	movq	%r13, %rcx
	shrq	%rcx
	movq	%rcx, 8(%rsp)
	lock xaddl	%ebp, (%rax)
	movl	$128, %eax
	andl	$1, %ebp
	movq	%r12, %rdi
	cmovne	%eax, %ebp
	xorl	%esi, %esi
	call	__pthread_mutex_unlock_usercnt@PLT
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L121
	movl	4(%rsp), %r11d
	leaq	96(%rsp), %rsi
	leaq	64(%rsp), %rcx
	leaq	0(,%r11,4), %r10
	leaq	40(%rbx,%r10), %r15
	movl	(%r15), %eax
	movl	%r14d, 24(%rsp)
	movq	%r13, %r14
	movq	%rsi, 40(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%r10, %r13
.L94:
	testb	$1, %al
	jne	.L116
	testl	%eax, %eax
	je	.L122
	leal	-2(%rax), %edx
	lock cmpxchgl	%edx, (%r15)
	jne	.L94
	movl	24(%rsp), %r14d
	movq	8(%rbx), %rdx
	leaq	8(%rbx), %rcx
	movq	%rdx, %rax
	shrq	%rax
	cmpq	%rax, 8(%rsp)
	jb	.L123
.L102:
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__condvar_confirm_wakeup
	movq	%r12, %rdi
	call	__pthread_mutex_cond_lock@PLT
	testl	%eax, %eax
	cmove	%r14d, %eax
.L90:
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L123:
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	cmpq	%rax, %r11
	jne	.L102
	movl	(%r15), %eax
.L106:
	movq	(%rcx), %rsi
	cmpq	%rsi, %rdx
	jne	.L102
	testb	$1, %al
	jne	.L109
	leal	2(%rax), %esi
	lock cmpxchgl	%esi, (%r15)
	jne	.L106
.L109:
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r15, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L102
	cmpl	$-22, %eax
	je	.L102
	cmpl	$-14, %eax
	je	.L102
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L122:
	lock addl	$2, 16(%rbx,%r13)
	movl	(%r15), %eax
	testb	$1, %al
	jne	.L98
	movq	%r11, 56(%rsp)
	movq	8(%rbx), %rax
	shrq	%rax
	cmpq	%rax, 8(%rsp)
	jb	.L98
	movq	40(%rsp), %rdx
	movq	16(%rsp), %rdi
	leaq	__condvar_cleanup_waiting(%rip), %rsi
	movq	%r14, 96(%rsp)
	movq	%rbx, 104(%rsp)
	movq	%r12, 112(%rsp)
	movl	%ebp, 120(%rsp)
	call	__pthread_cleanup_push@PLT
	movq	32(%rsp), %rcx
	movl	28(%rsp), %edx
	xorl	%esi, %esi
	movl	%ebp, %r8d
	movq	%r15, %rdi
	call	__futex_abstimed_wait_cancelable64@PLT
	movq	16(%rsp), %rdi
	xorl	%esi, %esi
	movl	%eax, 48(%rsp)
	call	__pthread_cleanup_pop@PLT
	movl	48(%rsp), %eax
	cmpl	$110, %eax
	je	.L113
	cmpl	$75, %eax
	movq	56(%rsp), %r11
	je	.L113
	movl	4(%rsp), %esi
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movq	%r11, 48(%rsp)
	call	__condvar_dec_grefs
	movl	(%r15), %eax
	movq	48(%rsp), %r11
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L112:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L121:
	movl	4(%rsp), %edx
	movq	8(%rsp), %rsi
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	call	__condvar_cancel_waiting
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__condvar_confirm_wakeup
	movl	%r14d, %eax
	jmp	.L90
	.p2align 4,,10
	.p2align 3
.L113:
	movl	4(%rsp), %r14d
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movl	%eax, 24(%rsp)
	movl	%r14d, %esi
	call	__condvar_dec_grefs
	movq	8(%rsp), %rsi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	call	__condvar_cancel_waiting
	movl	24(%rsp), %eax
	movl	%eax, %r14d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L116:
	movl	24(%rsp), %r14d
	jmp	.L102
	.p2align 4,,10
	.p2align 3
.L98:
	movl	4(%rsp), %esi
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movl	24(%rsp), %r14d
	call	__condvar_dec_grefs
	jmp	.L102
	.size	__pthread_cond_timedwait, .-__pthread_cond_timedwait
	.weak	pthread_cond_timedwait
	.set	pthread_cond_timedwait,__pthread_cond_timedwait
	.p2align 4,,15
	.globl	__pthread_cond_clockwait
	.type	__pthread_cond_clockwait, @function
__pthread_cond_clockwait:
	cmpq	$999999999, 8(%rcx)
	movl	$22, %eax
	ja	.L146
	cmpl	$1, %edx
	jbe	.L156
	rep ret
	.p2align 4,,10
	.p2align 3
.L156:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	$2, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbx
	subq	$136, %rsp
	movq	%rcx, 32(%rsp)
	movl	%edx, 28(%rsp)
	lock xaddq	%r13, (%rdi)
	movl	$8, %ebp
	movl	%r13d, %eax
	andl	$1, %eax
	movl	%eax, 4(%rsp)
	movq	%r13, %rax
	shrq	%rax
	movq	%rax, 8(%rsp)
	lock xaddl	%ebp, 36(%rdi)
	movl	$128, %eax
	andl	$1, %ebp
	movq	%r12, %rdi
	cmovne	%eax, %ebp
	xorl	%esi, %esi
	call	__pthread_mutex_unlock_usercnt@PLT
	testl	%eax, %eax
	movl	%eax, %r14d
	jne	.L157
	movl	4(%rsp), %r11d
	leaq	96(%rsp), %rsi
	leaq	64(%rsp), %rcx
	leaq	0(,%r11,4), %r10
	leaq	40(%rbx,%r10), %r15
	movl	(%r15), %eax
	movl	%r14d, 24(%rsp)
	movq	%r13, %r14
	movq	%rsi, 40(%rsp)
	movq	%rcx, 16(%rsp)
	movq	%r10, %r13
.L128:
	testb	$1, %al
	jne	.L151
	testl	%eax, %eax
	je	.L158
	leal	-2(%rax), %edx
	lock cmpxchgl	%edx, (%r15)
	jne	.L128
	movl	24(%rsp), %r14d
	movq	8(%rbx), %rdx
	leaq	8(%rbx), %rcx
	movq	%rdx, %rax
	shrq	%rax
	cmpq	%rax, 8(%rsp)
	jb	.L159
.L136:
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__condvar_confirm_wakeup
	movq	%r12, %rdi
	call	__pthread_mutex_cond_lock@PLT
	testl	%eax, %eax
	cmove	%r14d, %eax
.L124:
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L159:
	movq	%rdx, %rax
	notq	%rax
	andl	$1, %eax
	cmpq	%rax, %r11
	jne	.L136
	movl	(%r15), %eax
.L140:
	movq	(%rcx), %rsi
	cmpq	%rsi, %rdx
	jne	.L136
	testb	$1, %al
	jne	.L143
	leal	2(%rax), %esi
	lock cmpxchgl	%esi, (%r15)
	jne	.L140
.L143:
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r15, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L136
	cmpl	$-22, %eax
	je	.L136
	cmpl	$-14, %eax
	je	.L136
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L158:
	lock addl	$2, 16(%rbx,%r13)
	movl	(%r15), %eax
	testb	$1, %al
	jne	.L132
	movq	%r11, 56(%rsp)
	movq	8(%rbx), %rax
	shrq	%rax
	cmpq	%rax, 8(%rsp)
	jb	.L132
	movq	40(%rsp), %rdx
	movq	16(%rsp), %rdi
	leaq	__condvar_cleanup_waiting(%rip), %rsi
	movq	%r14, 96(%rsp)
	movq	%rbx, 104(%rsp)
	movq	%r12, 112(%rsp)
	movl	%ebp, 120(%rsp)
	call	__pthread_cleanup_push@PLT
	movq	32(%rsp), %rcx
	movl	28(%rsp), %edx
	xorl	%esi, %esi
	movl	%ebp, %r8d
	movq	%r15, %rdi
	call	__futex_abstimed_wait_cancelable64@PLT
	movq	16(%rsp), %rdi
	xorl	%esi, %esi
	movl	%eax, 48(%rsp)
	call	__pthread_cleanup_pop@PLT
	movl	48(%rsp), %eax
	cmpl	$110, %eax
	je	.L148
	cmpl	$75, %eax
	movq	56(%rsp), %r11
	je	.L148
	movl	4(%rsp), %esi
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movq	%r11, 48(%rsp)
	call	__condvar_dec_grefs
	movl	(%r15), %eax
	movq	48(%rsp), %r11
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L146:
	rep ret
	.p2align 4,,10
	.p2align 3
.L157:
	movl	4(%rsp), %edx
	movq	8(%rsp), %rsi
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	call	__condvar_cancel_waiting
	movl	%ebp, %esi
	movq	%rbx, %rdi
	call	__condvar_confirm_wakeup
	movl	%r14d, %eax
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L148:
	movl	4(%rsp), %r14d
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movl	%eax, 24(%rsp)
	movl	%r14d, %esi
	call	__condvar_dec_grefs
	movq	8(%rsp), %rsi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	movq	%rbx, %rdi
	call	__condvar_cancel_waiting
	movl	24(%rsp), %eax
	movl	%eax, %r14d
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L151:
	movl	24(%rsp), %r14d
	jmp	.L136
.L132:
	movl	4(%rsp), %esi
	movl	%ebp, %edx
	movq	%rbx, %rdi
	movl	24(%rsp), %r14d
	call	__condvar_dec_grefs
	jmp	.L136
	.size	__pthread_cond_clockwait, .-__pthread_cond_clockwait
	.weak	pthread_cond_clockwait
	.set	pthread_cond_clockwait,__pthread_cond_clockwait
