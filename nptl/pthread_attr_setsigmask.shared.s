	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	pthread_attr_setsigmask_np
	.type	pthread_attr_setsigmask_np, @function
pthread_attr_setsigmask_np:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__GI___pthread_attr_setsigmask_internal
	testl	%eax, %eax
	jne	.L1
	movq	40(%rbx), %rdx
	movabsq	$-6442450945, %rcx
	andq	%rcx, 16(%rdx)
.L1:
	popq	%rbx
	ret
	.size	pthread_attr_setsigmask_np, .-pthread_attr_setsigmask_np
