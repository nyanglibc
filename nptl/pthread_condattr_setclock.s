	.text
	.p2align 4,,15
	.globl	pthread_condattr_setclock
	.type	pthread_condattr_setclock, @function
pthread_condattr_setclock:
	cmpl	$1, %esi
	movl	$22, %eax
	ja	.L1
	movl	(%rdi), %eax
	addl	%esi, %esi
	andl	$-3, %eax
	orl	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, (%rdi)
.L1:
	rep ret
	.size	pthread_condattr_setclock, .-pthread_condattr_setclock
