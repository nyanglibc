	.text
	.p2align 4,,15
	.globl	__lll_clocklock_elision
	.type	__lll_clocklock_elision, @function
__lll_clocklock_elision:
	movzwl	(%rsi), %eax
	testw	%ax, %ax
	jg	.L2
	movl	8+__elision_aconf(%rip), %r9d
	movl	$-1, %r10d
	testl	%r9d, %r9d
	jg	.L3
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L6:
	testb	$2, %al
	je	.L29
	subl	$1, %r9d
	je	.L4
.L3:
	movl	%r10d, %eax
	xbegin	.L5
.L5:
	cmpl	$-1, %eax
	jne	.L6
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L24
	xabort	$255
	subl	$1, %r9d
	jne	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, (%rdi)
	jne	.L30
.L24:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movzwl	(%rsi), %eax
	subl	$1, %eax
	movw	%ax, (%rsi)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L29:
	testb	$1, %al
	je	.L10
	shrl	$24, %eax
	cmpl	$255, %eax
	je	.L31
.L10:
	movswl	(%rsi), %eax
	movl	4+__elision_aconf(%rip), %r9d
	cmpl	%r9d, %eax
	je	.L4
.L27:
	movw	%r9w, (%rsi)
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L30:
	pushq	%r14
	movl	$2, %r14d
	pushq	%r13
	movl	%r8d, %r13d
	pushq	%r12
	movq	%rcx, %r12
	pushq	%rbp
	movl	%edx, %ebp
	pushq	%rbx
	movq	%rdi, %rbx
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%r13d, %r8d
	movq	%r12, %rcx
	movl	%ebp, %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$22, %eax
	je	.L1
	cmpl	$110, %eax
	je	.L1
	cmpl	$75, %eax
	je	.L1
.L13:
	movl	%r14d, %eax
#APP
# 364 "../sysdeps/nptl/futex-internal.h" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L14
	xorl	%eax, %eax
.L1:
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
.L31:
	movswl	(%rsi), %eax
	movl	__elision_aconf(%rip), %r9d
	cmpl	%r9d, %eax
	je	.L4
	jmp	.L27
	.size	__lll_clocklock_elision, .-__lll_clocklock_elision
