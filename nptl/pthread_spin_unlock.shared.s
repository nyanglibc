.globl pthread_spin_unlock
.type pthread_spin_unlock,@function
.align 1<<4
pthread_spin_unlock:
 movl $1, (%rdi)
 xorl %eax, %eax
 retq
.size pthread_spin_unlock,.-pthread_spin_unlock
 .globl pthread_spin_init
pthread_spin_init = pthread_spin_unlock
