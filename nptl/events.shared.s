	.text
	.p2align 4,,15
	.globl	__GI___nptl_create_event
	.hidden	__GI___nptl_create_event
	.type	__GI___nptl_create_event, @function
__GI___nptl_create_event:
	rep ret
	.size	__GI___nptl_create_event, .-__GI___nptl_create_event
	.globl	__nptl_create_event
	.set	__nptl_create_event,__GI___nptl_create_event
	.p2align 4,,15
	.globl	__GI___nptl_death_event
	.hidden	__GI___nptl_death_event
	.type	__GI___nptl_death_event, @function
__GI___nptl_death_event:
	rep ret
	.size	__GI___nptl_death_event, .-__GI___nptl_death_event
	.globl	__nptl_death_event
	.set	__nptl_death_event,__GI___nptl_death_event
