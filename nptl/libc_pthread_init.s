	.text
	.p2align 4,,15
	.globl	__libc_pthread_init
	.type	__libc_pthread_init, @function
__libc_pthread_init:
	subq	$8, %rsp
	movq	%rsi, %rdx
	movq	%rdi, __fork_generation_pointer(%rip)
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	xorl	%edi, %edi
	call	__register_atfork
	leaq	__libc_multiple_threads(%rip), %rax
	addq	$8, %rsp
	ret
	.size	__libc_pthread_init, .-__libc_pthread_init
	.hidden	__fork_generation_pointer
	.comm	__fork_generation_pointer,8,8
	.hidden	__libc_multiple_threads
	.hidden	__register_atfork
