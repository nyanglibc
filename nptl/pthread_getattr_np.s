	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"rce"
.LC1:
	.string	"/proc/self/maps"
.LC2:
	.string	"%lx-%lx"
	.text
	.p2align 4,,15
	.globl	__pthread_getattr_np
	.type	__pthread_getattr_np, @function
__pthread_getattr_np:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %rbx
	subq	$120, %rsp
	call	__pthread_attr_init
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L1
#APP
# 44 "pthread_getattr_np.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	leaq	1560(%r13), %rcx
	testl	%eax, %eax
	movq	%rcx, 8(%rsp)
	jne	.L3
	movl	%ebp, %eax
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 1560(%r13)
# 0 "" 2
#NO_APP
.L4:
	movl	1592(%r13), %eax
	movl	%eax, (%rbx)
	movl	1596(%r13), %eax
	cmpq	%r13, 1576(%r13)
	movl	%eax, 4(%rbx)
	movl	780(%r13), %eax
	movl	%eax, 8(%rbx)
	je	.L42
.L5:
	movq	1704(%r13), %rax
	movq	%rax, 16(%rbx)
	movq	1680(%r13), %rax
	testq	%rax, %rax
	je	.L6
	movq	1688(%r13), %rdx
	movq	%rdx, %rcx
	subq	1696(%r13), %rcx
	orl	$8, 8(%rbx)
	addq	%rdx, %rax
	movq	%rax, 24(%rbx)
	movq	%rcx, 32(%rbx)
.L7:
	xorl	%r15d, %r15d
	movl	$16, %r12d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L43:
	movq	%rax, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	__pthread_getaffinity_np
	cmpl	$22, %eax
	movq	%r14, %r15
	jne	.L31
	cmpq	$1048575, %r12
	ja	.L31
.L22:
	addq	%r12, %r12
	movq	%r15, %rdi
	movq	%r12, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.L43
	movl	$12, %ebp
.L21:
	movq	%r15, %rdi
	call	free@PLT
.L20:
#APP
# 202 "pthread_getattr_np.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L25
	subl	$1, 1560(%r13)
.L26:
	testl	%ebp, %ebp
	je	.L1
	movq	%rbx, %rdi
	call	__pthread_attr_destroy
.L1:
	addq	$120, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	leaq	1560(%r13), %rcx
	movl	$1, %edx
	movl	%ebp, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L4
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L42:
	orl	$1, %eax
	movl	%eax, 8(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L31:
	testl	%eax, %eax
	jne	.L24
	movq	%r14, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__pthread_attr_setaffinity_np
	movq	%r14, %r15
	movl	%eax, %ebp
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	_IO_new_fopen@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L44
	leaq	96(%rsp), %rsi
	movl	$3, %edi
	call	__getrlimit
	testl	%eax, %eax
	je	.L10
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r12d
.L11:
	movq	%r14, %rdi
	call	_IO_new_fclose@PLT
.L9:
	orl	$8, 8(%rbx)
	testl	%r12d, %r12d
	je	.L7
	movl	%r12d, %ebp
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%eax, %eax
#APP
# 202 "pthread_getattr_np.c" 1
	xchgl %eax, 1560(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L26
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	8(%rsp), %rdi
	movl	$202, %eax
#APP
# 202 "pthread_getattr_np.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L26
	.p2align 4,,10
	.p2align 3
.L44:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r12d
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L24:
	cmpl	$38, %eax
	movq	%r14, %r15
	cmovne	%eax, %ebp
	jmp	.L21
.L10:
	movq	_dl_pagesize(%rip), %rax
	movl	(%r14), %r12d
	leaq	64(%rsp), %rdx
	leaq	80(%rsp), %rcx
	movl	%ebp, 28(%rsp)
	movq	%r13, 32(%rsp)
	movq	%rbx, 40(%rsp)
	movq	$0, 16(%rsp)
	leaq	72(%rsp), %r15
	movq	%rax, 48(%rsp)
	movq	__libc_stack_end(%rip), %rax
	orl	$32768, %r12d
	movl	%r12d, (%r14)
	movq	%rdx, %rbx
	movq	$0, 64(%rsp)
	movq	$0, 72(%rsp)
	movq	%rcx, %r13
	movq	%rax, 56(%rsp)
	leaq	88(%rsp), %rax
	movq	%rax, %rbp
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r14, %rdx
	movq	%r15, %rsi
	movq	%rbx, %rdi
	call	__getline
	testq	%rax, %rax
	jle	.L40
	movq	64(%rsp), %rdi
	leaq	.LC2(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbp, %rcx
	movq	%r13, %rdx
	call	__isoc99_sscanf
	cmpl	$2, %eax
	jne	.L15
	movq	__libc_stack_end(%rip), %rdx
	cmpq	%rdx, 80(%rsp)
	movq	88(%rsp), %rax
	ja	.L16
	cmpq	%rax, %rdx
	jb	.L45
.L16:
	movq	%rax, 16(%rsp)
.L15:
	movl	(%r14), %r12d
.L12:
	andl	$16, %r12d
	je	.L19
.L40:
	movl	28(%rsp), %ebp
	movq	32(%rsp), %r13
	movl	$2, %r12d
	movq	40(%rsp), %rbx
.L18:
	movq	64(%rsp), %rdi
	call	free@PLT
	jmp	.L11
.L45:
	movq	48(%rsp), %rcx
	movq	40(%rsp), %rbx
	movl	28(%rsp), %ebp
	movq	32(%rsp), %r13
	movq	%rcx, %rdx
	negq	%rdx
	andq	56(%rsp), %rdx
	addq	%rcx, %rdx
	movq	96(%rsp), %rcx
	movq	%rdx, 24(%rbx)
	addq	%rdx, %rcx
	subq	16(%rsp), %rdx
	subq	%rax, %rcx
	movq	%rcx, %rax
	movq	_dl_pagesize(%rip), %rcx
	negq	%rcx
	andq	%rcx, %rax
	cmpq	%rdx, %rax
	cmovbe	%rax, %rdx
	movq	%rdx, 32(%rbx)
	jmp	.L18
	.size	__pthread_getattr_np, .-__pthread_getattr_np
	.weak	pthread_getattr_np
	.set	pthread_getattr_np,__pthread_getattr_np
	.hidden	__isoc99_sscanf
	.hidden	__getline
	.hidden	__getrlimit
	.hidden	__pthread_attr_setaffinity_np
	.hidden	__lll_lock_wait_private
	.hidden	__pthread_attr_destroy
	.hidden	__pthread_getaffinity_np
	.hidden	__pthread_attr_init
