	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_setprioceiling
	.type	pthread_mutexattr_setprioceiling, @function
pthread_mutexattr_setprioceiling:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movl	%esi, %ebx
	subq	$8, %rsp
	movl	__sched_fifo_min_prio(%rip), %eax
	cmpl	$-1, %eax
	je	.L4
	movl	__sched_fifo_max_prio(%rip), %eax
	cmpl	$-1, %eax
	je	.L4
	movl	__sched_fifo_min_prio(%rip), %eax
	cmpl	%ebx, %eax
	jg	.L7
.L5:
	movl	__sched_fifo_max_prio(%rip), %eax
	cmpl	%eax, %ebx
	jg	.L7
	testl	$-4096, %ebx
	jne	.L7
	movl	0(%rbp), %esi
	sall	$12, %ebx
	xorl	%eax, %eax
	andl	$-16773121, %esi
	orl	%esi, %ebx
	movl	%ebx, 0(%rbp)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	call	__init_sched_fifo_prio
	movl	__sched_fifo_min_prio(%rip), %eax
	cmpl	%ebx, %eax
	jle	.L5
.L7:
	addq	$8, %rsp
	movl	$22, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	pthread_mutexattr_setprioceiling, .-pthread_mutexattr_setprioceiling
	.hidden	__init_sched_fifo_prio
	.hidden	__sched_fifo_max_prio
	.hidden	__sched_fifo_min_prio
