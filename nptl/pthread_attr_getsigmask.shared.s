	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___pthread_attr_getsigmask_np
	.hidden	__GI___pthread_attr_getsigmask_np
	.type	__GI___pthread_attr_getsigmask_np, @function
__GI___pthread_attr_getsigmask_np:
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L2
	cmpb	$0, 144(%rax)
	je	.L2
	movdqu	16(%rax), %xmm0
	movups	%xmm0, (%rsi)
	movdqu	32(%rax), %xmm0
	movups	%xmm0, 16(%rsi)
	movdqu	48(%rax), %xmm0
	movups	%xmm0, 32(%rsi)
	movdqu	64(%rax), %xmm0
	movups	%xmm0, 48(%rsi)
	movdqu	80(%rax), %xmm0
	movups	%xmm0, 64(%rsi)
	movdqu	96(%rax), %xmm0
	movups	%xmm0, 80(%rsi)
	movdqu	112(%rax), %xmm0
	movups	%xmm0, 96(%rsi)
	movdqu	128(%rax), %xmm0
	xorl	%eax, %eax
	movups	%xmm0, 112(%rsi)
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	$0, (%rsi)
	movl	$-1, %eax
	ret
	.size	__GI___pthread_attr_getsigmask_np, .-__GI___pthread_attr_getsigmask_np
	.globl	__pthread_attr_getsigmask_np
	.set	__pthread_attr_getsigmask_np,__GI___pthread_attr_getsigmask_np
	.weak	pthread_attr_getsigmask_np
	.set	pthread_attr_getsigmask_np,__pthread_attr_getsigmask_np
