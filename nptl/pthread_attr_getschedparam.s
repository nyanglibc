	.text
	.p2align 4,,15
	.globl	__pthread_attr_getschedparam
	.type	__pthread_attr_getschedparam, @function
__pthread_attr_getschedparam:
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getschedparam, .-__pthread_attr_getschedparam
	.globl	pthread_attr_getschedparam
	.set	pthread_attr_getschedparam,__pthread_attr_getschedparam
