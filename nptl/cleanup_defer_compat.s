	.text
	.p2align 4,,15
	.globl	_pthread_cleanup_push_defer
	.type	_pthread_cleanup_push_defer, @function
_pthread_cleanup_push_defer:
	movq	%fs:16, %rcx
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
#APP
# 30 "cleanup_defer_compat.c" 1
	movq %fs:760,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rdi)
#APP
# 32 "cleanup_defer_compat.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	addq	$776, %rcx
	testb	$2, %dl
	jne	.L3
.L2:
	sarl	%edx
	andl	$1, %edx
	movl	%edx, 16(%rdi)
#APP
# 54 "cleanup_defer_compat.c" 1
	movq %rdi,%fs:760
# 0 "" 2
#NO_APP
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$-3, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L2
	jmp	.L4
	.size	_pthread_cleanup_push_defer, .-_pthread_cleanup_push_defer
	.globl	__pthread_cleanup_push_defer
	.set	__pthread_cleanup_push_defer,_pthread_cleanup_push_defer
	.p2align 4,,15
	.globl	_pthread_cleanup_pop_restore
	.type	_pthread_cleanup_pop_restore, @function
_pthread_cleanup_pop_restore:
	movq	%rdi, %rdx
	movq	24(%rdi), %rax
#APP
# 65 "cleanup_defer_compat.c" 1
	movq %rax,%fs:760
# 0 "" 2
#NO_APP
	movl	16(%rdi), %eax
	testl	%eax, %eax
	jne	.L17
.L10:
	testl	%esi, %esi
	je	.L9
	movq	8(%rdx), %rdi
	jmp	*(%rdx)
	.p2align 4,,10
	.p2align 3
.L9:
	rep ret
	.p2align 4,,10
	.p2align 3
.L17:
#APP
# 69 "cleanup_defer_compat.c" 1
	movl %fs:776,%ecx
# 0 "" 2
#NO_APP
	testb	$2, %cl
	jne	.L10
	movq	%fs:16, %rax
	leaq	776(%rax), %rdi
.L11:
	movl	%ecx, %r8d
	movl	%ecx, %eax
	orl	$2, %r8d
	lock cmpxchgl	%r8d, (%rdi)
	cmpl	%eax, %ecx
	jne	.L13
#APP
# 86 "cleanup_defer_compat.c" 1
	movl %fs:776,%eax
# 0 "" 2
#NO_APP
	andl	$-71, %eax
	cmpl	$8, %eax
	jne	.L10
	subq	$8, %rsp
#APP
# 86 "cleanup_defer_compat.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
#APP
# 304 "pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__pthread_unwind@PLT
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%eax, %ecx
	jmp	.L11
	.size	_pthread_cleanup_pop_restore, .-_pthread_cleanup_pop_restore
	.globl	__pthread_cleanup_pop_restore
	.set	__pthread_cleanup_pop_restore,_pthread_cleanup_pop_restore
