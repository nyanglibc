	.text
	.p2align 4,,15
	.globl	_pthread_cleanup_push
	.type	_pthread_cleanup_push, @function
_pthread_cleanup_push:
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
#APP
# 31 "cleanup_compat.c" 1
	movq %fs:760,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 24(%rdi)
#APP
# 33 "cleanup_compat.c" 1
	movq %rdi,%fs:760
# 0 "" 2
#NO_APP
	ret
	.size	_pthread_cleanup_push, .-_pthread_cleanup_push
	.globl	__pthread_cleanup_push
	.set	__pthread_cleanup_push,_pthread_cleanup_push
	.p2align 4,,15
	.globl	_pthread_cleanup_pop
	.type	_pthread_cleanup_pop, @function
_pthread_cleanup_pop:
	movq	%rdi, %rax
	movq	24(%rdi), %rdx
#APP
# 43 "cleanup_compat.c" 1
	movq %rdx,%fs:760
# 0 "" 2
#NO_APP
	testl	%esi, %esi
	je	.L3
	movq	8(%rdi), %rdi
	jmp	*(%rax)
	.p2align 4,,10
	.p2align 3
.L3:
	rep ret
	.size	_pthread_cleanup_pop, .-_pthread_cleanup_pop
	.globl	__pthread_cleanup_pop
	.set	__pthread_cleanup_pop,_pthread_cleanup_pop
