	.text
	.p2align 4,,15
	.globl	pthread_mutex_consistent
	.type	pthread_mutex_consistent, @function
pthread_mutex_consistent:
	movl	16(%rdi), %edx
	movl	$22, %eax
	andl	$16, %edx
	je	.L1
	cmpl	$2147483647, 8(%rdi)
	jne	.L1
#APP
# 34 "pthread_mutex_consistent.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	pthread_mutex_consistent, .-pthread_mutex_consistent
	.weak	pthread_mutex_consistent_np
	.set	pthread_mutex_consistent_np,pthread_mutex_consistent
