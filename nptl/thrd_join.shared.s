	.text
	.p2align 4,,15
	.globl	thrd_join
	.type	thrd_join, @function
thrd_join:
	pushq	%rbx
	movq	%rsi, %rbx
	subq	$16, %rsp
	leaq	8(%rsp), %rsi
	call	__pthread_join@PLT
	testq	%rbx, %rbx
	je	.L2
	movq	8(%rsp), %rdx
	movl	%edx, (%rbx)
.L2:
	cmpl	$12, %eax
	je	.L4
	jle	.L17
	cmpl	$16, %eax
	je	.L7
	cmpl	$110, %eax
	jne	.L3
	movl	$4, %eax
.L1:
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	addq	$16, %rsp
	movl	$1, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	testl	%eax, %eax
	je	.L1
.L3:
	addq	$16, %rsp
	movl	$2, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	addq	$16, %rsp
	movl	$3, %eax
	popq	%rbx
	ret
	.size	thrd_join, .-thrd_join
