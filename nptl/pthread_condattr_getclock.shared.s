	.text
	.p2align 4,,15
	.globl	pthread_condattr_getclock
	.type	pthread_condattr_getclock, @function
pthread_condattr_getclock:
	movl	(%rdi), %eax
	sarl	%eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_condattr_getclock, .-pthread_condattr_getclock
