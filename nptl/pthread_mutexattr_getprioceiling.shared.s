	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_getprioceiling
	.type	pthread_mutexattr_getprioceiling, @function
pthread_mutexattr_getprioceiling:
	movl	(%rdi), %eax
	sarl	$12, %eax
	andl	$4095, %eax
	je	.L11
.L5:
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	__sched_fifo_min_prio(%rip), %edx
	cmpl	$-1, %edx
	je	.L12
	movl	__sched_fifo_min_prio(%rip), %edx
	testl	%edx, %edx
	jle	.L5
	movl	__sched_fifo_min_prio(%rip), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	subq	$24, %rsp
	movq	%rsi, 8(%rsp)
	movl	%eax, 4(%rsp)
	call	__init_sched_fifo_prio
	movq	8(%rsp), %rsi
	movl	4(%rsp), %eax
	movl	__sched_fifo_min_prio(%rip), %edx
	testl	%edx, %edx
	jle	.L2
	movl	__sched_fifo_min_prio(%rip), %eax
.L2:
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	addq	$24, %rsp
	ret
	.size	pthread_mutexattr_getprioceiling, .-pthread_mutexattr_getprioceiling
	.hidden	__init_sched_fifo_prio
	.hidden	__sched_fifo_min_prio
