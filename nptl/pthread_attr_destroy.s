	.text
	.p2align 4,,15
	.globl	__pthread_attr_destroy
	.hidden	__pthread_attr_destroy
	.type	__pthread_attr_destroy, @function
__pthread_attr_destroy:
	movq	40(%rdi), %rax
	testq	%rax, %rax
	je	.L7
	pushq	%rbx
	movq	%rdi, %rbx
	movq	(%rax), %rdi
	call	free@PLT
	movq	40(%rbx), %rdi
	call	free@PLT
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_destroy, .-__pthread_attr_destroy
	.weak	pthread_attr_destroy
	.set	pthread_attr_destroy,__pthread_attr_destroy
