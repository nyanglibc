	.text
	.p2align 4,,15
	.globl	pthread_rwlockattr_destroy
	.type	pthread_rwlockattr_destroy, @function
pthread_rwlockattr_destroy:
	xorl	%eax, %eax
	ret
	.size	pthread_rwlockattr_destroy, .-pthread_rwlockattr_destroy
