	.text
	.p2align 4,,15
	.globl	__pthread_getaffinity_np
	.hidden	__pthread_getaffinity_np
	.type	__pthread_getaffinity_np, @function
__pthread_getaffinity_np:
	movq	%rsi, %r8
	cmpq	$2147483647, %rsi
	movl	$2147483647, %esi
	movq	%rdx, %r9
	cmovbe	%r8, %rsi
	movl	720(%rdi), %edi
	movl	$204, %eax
#APP
# 34 "pthread_getaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L2
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cltq
	movq	%r8, %rdx
	subq	$8, %rsp
	leaq	(%r9,%rax), %rdi
	subq	%rax, %rdx
	xorl	%esi, %esi
	call	memset@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__pthread_getaffinity_np, .-__pthread_getaffinity_np
	.weak	pthread_getaffinity_np
	.set	pthread_getaffinity_np,__pthread_getaffinity_np
