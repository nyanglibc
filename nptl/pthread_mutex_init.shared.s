	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__GI___pthread_mutex_init
	.hidden	__GI___pthread_mutex_init
	.type	__GI___pthread_mutex_init, @function
__GI___pthread_mutex_init:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$24, %rsp
	testq	%rsi, %rsi
	je	.L2
	movl	(%rsi), %eax
	movq	%rsi, %r12
	movl	%eax, %edx
	andl	$805306368, %edx
	je	.L4
	cmpl	$268435456, %edx
	jne	.L31
	movl	tpi_supported.8778(%rip), %eax
	testl	%eax, %eax
	je	.L36
.L6:
	movl	tpi_supported.8778(%rip), %eax
	testl	%eax, %eax
	js	.L11
.L4:
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rbp)
	movups	%xmm0, 0(%rbp)
	movups	%xmm0, 16(%rbp)
	movl	(%r12), %eax
	movl	%eax, %ebx
	andl	$251662335, %ebx
	testl	$1073741824, %eax
	je	.L33
	orl	$16, %ebx
.L33:
	movl	%eax, %edx
	andl	$805306368, %edx
	cmpl	$268435456, %edx
	je	.L17
	cmpl	$536870912, %edx
	jne	.L16
	movl	%eax, %r13d
	orl	$64, %ebx
	sarl	$12, %r13d
	andl	$4095, %r13d
	je	.L19
.L34:
	sall	$19, %r13d
.L20:
	movl	%r13d, 0(%rbp)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L31:
	testl	$1073741824, %eax
	je	.L4
.L11:
	addq	$24, %rsp
	movl	$95, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	orl	$32, %ebx
.L16:
	movl	%ebx, %edx
	orb	$-128, %dl
	testl	$-1073741824, %eax
	cmovne	%edx, %ebx
.L22:
	movl	%ebx, 16(%rbp)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pxor	%xmm0, %xmm0
	movq	$0, 32(%rdi)
	xorl	%ebx, %ebx
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L19:
	movl	__sched_fifo_min_prio(%rip), %edx
	cmpl	$-1, %edx
	je	.L37
.L21:
	movl	__sched_fifo_min_prio(%rip), %edx
	testl	%edx, %edx
	jle	.L20
	movl	__sched_fifo_min_prio(%rip), %r13d
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L36:
	movl	$0, 12(%rsp)
	leaq	12(%rsp), %rdi
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$135, %esi
	movl	$202, %eax
#APP
# 307 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	movl	$1, %edx
	ja	.L38
.L7:
	movl	%edx, tpi_supported.8778(%rip)
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L37:
	call	__init_sched_fifo_prio
	movl	(%r12), %eax
	jmp	.L21
.L38:
	cmpl	$-11, %eax
	je	.L9
	jg	.L10
	cmpl	$-38, %eax
	je	.L25
	cmpl	$-35, %eax
	je	.L9
	cmpl	$-110, %eax
	je	.L9
.L8:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$-4, %eax
	jl	.L8
	cmpl	$-3, %eax
	jle	.L9
	cmpl	$-1, %eax
	jne	.L8
.L9:
	cmpl	$-38, %eax
	movl	$1, %edx
	jne	.L7
.L25:
	movl	$-1, %edx
	jmp	.L7
	.size	__GI___pthread_mutex_init, .-__GI___pthread_mutex_init
	.weak	pthread_mutex_init
	.set	pthread_mutex_init,__GI___pthread_mutex_init
	.globl	__pthread_mutex_init
	.set	__pthread_mutex_init,__GI___pthread_mutex_init
	.local	tpi_supported.8778
	.comm	tpi_supported.8778,4,4
	.hidden	__init_sched_fifo_prio
	.hidden	__sched_fifo_min_prio
