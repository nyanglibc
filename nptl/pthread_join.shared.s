	.text
	.p2align 4,,15
	.globl	__pthread_join
	.type	__pthread_join, @function
__pthread_join:
	movl	$1, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	__pthread_clockjoin_ex
	.size	__pthread_join, .-__pthread_join
	.weak	pthread_join
	.set	pthread_join,__pthread_join
	.hidden	__pthread_clockjoin_ex
