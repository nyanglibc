	.text
#APP
	.symver __pthread_cond_broadcast_2_0,pthread_cond_broadcast@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pthread_cond_broadcast_2_0
	.type	__pthread_cond_broadcast_2_0, @function
__pthread_cond_broadcast_2_0:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L8
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__pthread_cond_broadcast@PLT
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdi, %rbp
	movl	$1, %esi
	movl	$48, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L3
	movq	%rbx, %rax
	lock cmpxchgq	%rdi, 0(%rbp)
	jne	.L4
.L6:
	movq	0(%rbp), %rbx
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__pthread_cond_broadcast@PLT
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$8, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	ret
.L4:
	call	free@PLT
	jmp	.L6
	.size	__pthread_cond_broadcast_2_0, .-__pthread_cond_broadcast_2_0
