	.text
	.p2align 4,,15
	.globl	__GI___pthread_register_cancel
	.hidden	__GI___pthread_register_cancel
	.type	__GI___pthread_register_cancel, @function
__GI___pthread_register_cancel:
#APP
# 31 "cleanup.c" 1
	movq %fs:768,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 72(%rdi)
#APP
# 32 "cleanup.c" 1
	movq %fs:760,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 80(%rdi)
#APP
# 35 "cleanup.c" 1
	movq %rdi,%fs:768
# 0 "" 2
#NO_APP
	ret
	.size	__GI___pthread_register_cancel, .-__GI___pthread_register_cancel
	.globl	__pthread_register_cancel
	.set	__pthread_register_cancel,__GI___pthread_register_cancel
	.p2align 4,,15
	.globl	__GI___pthread_unregister_cancel
	.hidden	__GI___pthread_unregister_cancel
	.type	__GI___pthread_unregister_cancel, @function
__GI___pthread_unregister_cancel:
	movq	72(%rdi), %rax
#APP
# 46 "cleanup.c" 1
	movq %rax,%fs:768
# 0 "" 2
#NO_APP
	ret
	.size	__GI___pthread_unregister_cancel, .-__GI___pthread_unregister_cancel
	.globl	__pthread_unregister_cancel
	.set	__pthread_unregister_cancel,__GI___pthread_unregister_cancel
