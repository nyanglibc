	.text
	.globl	__pthread_keys
	.bss
	.align 32
	.type	__pthread_keys, @object
	.size	__pthread_keys, 16384
__pthread_keys:
	.zero	16384
	.comm	__pthread_multiple_threads,4,4
	.globl	__default_pthread_attr_lock
	.align 4
	.type	__default_pthread_attr_lock, @object
	.size	__default_pthread_attr_lock, 4
__default_pthread_attr_lock:
	.zero	4
	.comm	__default_pthread_attr,56,32
