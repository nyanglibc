	.text
	.p2align 4,,15
	.globl	__thrd_sleep
	.type	__thrd_sleep, @function
__thrd_sleep:
	subq	$8, %rsp
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	__clock_nanosleep
	testl	%eax, %eax
	je	.L1
	cmpl	$4, %eax
	sete	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L1:
	addq	$8, %rsp
	ret
	.size	__thrd_sleep, .-__thrd_sleep
	.weak	thrd_sleep
	.set	thrd_sleep,__thrd_sleep
	.hidden	__clock_nanosleep
