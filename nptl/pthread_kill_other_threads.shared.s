	.text
#APP
	.symver __pthread_kill_other_threads_np,pthread_kill_other_threads_np@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pthread_kill_other_threads_np
	.type	__pthread_kill_other_threads_np, @function
__pthread_kill_other_threads_np:
	rep ret
	.size	__pthread_kill_other_threads_np, .-__pthread_kill_other_threads_np
