	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_getrobust
	.type	pthread_mutexattr_getrobust, @function
pthread_mutexattr_getrobust:
	movl	(%rdi), %eax
	sarl	$30, %eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_mutexattr_getrobust, .-pthread_mutexattr_getrobust
	.weak	pthread_mutexattr_getrobust_np
	.set	pthread_mutexattr_getrobust_np,pthread_mutexattr_getrobust
