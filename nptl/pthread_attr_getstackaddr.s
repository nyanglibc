	.text
#APP
	.section .gnu.warning.pthread_attr_getstackaddr
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_getstackaddr
	.type	__pthread_attr_getstackaddr, @function
__pthread_attr_getstackaddr:
	movq	24(%rdi), %rax
	movq	%rax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getstackaddr, .-__pthread_attr_getstackaddr
	.globl	pthread_attr_getstackaddr
	.set	pthread_attr_getstackaddr,__pthread_attr_getstackaddr
	.section	.gnu.warning.pthread_attr_getstackaddr
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_pthread_attr_getstackaddr, @object
	.size	__evoke_link_warning_pthread_attr_getstackaddr, 82
__evoke_link_warning_pthread_attr_getstackaddr:
	.string	"the use of `pthread_attr_getstackaddr' is deprecated, use `pthread_attr_getstack'"
