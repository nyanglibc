	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__libc_pthread_init
	.type	__libc_pthread_init, @function
__libc_pthread_init:
	pushq	%rbx
	movq	%rdi, __fork_generation_pointer(%rip)
	xorl	%ecx, %ecx
	movq	%rdx, %rbx
	xorl	%edi, %edi
	movq	%rsi, %rdx
	xorl	%esi, %esi
	call	__GI___register_atfork
	leaq	__libc_pthread_functions(%rip), %rcx
	xorl	%edx, %edx
	.p2align 4,,10
	.p2align 3
.L2:
	movq	(%rbx,%rdx,8), %rax
#APP
# 70 "libc_pthread_init.c" 1
	xor %fs:48, %rax
rol $2*8+1, %rax
# 0 "" 2
#NO_APP
	movq	%rax, (%rcx,%rdx,8)
	addq	$1, %rdx
	cmpq	$30, %rdx
	jne	.L2
	leaq	__libc_multiple_threads(%rip), %rax
	movl	$1, __libc_pthread_functions_init(%rip)
	popq	%rbx
	ret
	.size	__libc_pthread_init, .-__libc_pthread_init
	.hidden	__fork_generation_pointer
	.comm	__fork_generation_pointer,8,8
	.hidden	__libc_pthread_functions_init
	.hidden	__libc_multiple_threads
	.hidden	__libc_pthread_functions
