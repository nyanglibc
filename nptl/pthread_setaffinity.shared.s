	.text
#APP
	.symver __pthread_setaffinity_new,pthread_setaffinity_np@@GLIBC_2.3.4
	.symver __pthread_setaffinity_old,pthread_setaffinity_np@GLIBC_2.3.3
#NO_APP
	.p2align 4,,15
	.globl	__pthread_setaffinity_new
	.type	__pthread_setaffinity_new, @function
__pthread_setaffinity_new:
	movl	720(%rdi), %edi
	movl	$203, %eax
#APP
# 33 "pthread_setaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	negl	%edx
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmova	%edx, %eax
	ret
	.size	__pthread_setaffinity_new, .-__pthread_setaffinity_new
	.p2align 4,,15
	.globl	__pthread_setaffinity_old
	.type	__pthread_setaffinity_old, @function
__pthread_setaffinity_old:
	movq	%rsi, %rdx
	movl	$128, %esi
	jmp	__pthread_setaffinity_new@PLT
	.size	__pthread_setaffinity_old, .-__pthread_setaffinity_old
