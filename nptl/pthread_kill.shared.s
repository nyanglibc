	.text
	.p2align 4,,15
	.globl	__pthread_kill
	.type	__pthread_kill, @function
__pthread_kill:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	720(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.L3
	leal	-32(%rsi), %edx
	movl	$22, %eax
	cmpl	$1, %edx
	jbe	.L1
	movl	%esi, %ebp
	call	__getpid@PLT
	movl	%ebp, %edx
	movl	%eax, %edi
	movl	%ebx, %esi
	movl	$234, %eax
#APP
# 53 "../sysdeps/unix/sysv/linux/pthread_kill.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	negl	%edx
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmova	%edx, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$8, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	__pthread_kill, .-__pthread_kill
	.globl	pthread_kill
	.set	pthread_kill,__pthread_kill
