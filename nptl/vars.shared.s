	.text
	.hidden	__GI___pthread_keys
	.globl	__GI___pthread_keys
	.bss
	.align 32
	.type	__GI___pthread_keys, @object
	.size	__GI___pthread_keys, 16384
__GI___pthread_keys:
	.zero	16384
	.globl	__pthread_keys
	.set	__pthread_keys,__GI___pthread_keys
	.hidden	__pthread_multiple_threads
	.comm	__pthread_multiple_threads,4,4
	.hidden	__default_pthread_attr_lock
	.globl	__default_pthread_attr_lock
	.align 4
	.type	__default_pthread_attr_lock, @object
	.size	__default_pthread_attr_lock, 4
__default_pthread_attr_lock:
	.zero	4
	.hidden	__default_pthread_attr
	.comm	__default_pthread_attr,56,32
