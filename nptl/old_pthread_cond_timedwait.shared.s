	.text
#APP
	.symver __pthread_cond_timedwait_2_0,pthread_cond_timedwait@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pthread_cond_timedwait_2_0
	.type	__pthread_cond_timedwait_2_0, @function
__pthread_cond_timedwait_2_0:
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %r12
	pushq	%rbx
	subq	$16, %rsp
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.L7
.L2:
	addq	$16, %rsp
	movq	%r12, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	__pthread_cond_timedwait@PLT
	.p2align 4,,10
	.p2align 3
.L7:
	movq	%rdi, %rbp
	movl	$1, %esi
	movl	$48, %edi
	movq	%rdx, 8(%rsp)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L3
	movq	%rbx, %rax
	lock cmpxchgq	%rdi, 0(%rbp)
	movq	8(%rsp), %rdx
	jne	.L4
	movq	0(%rbp), %rbx
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$16, %rsp
	movl	$12, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
.L4:
	movq	%rdx, 8(%rsp)
	call	free@PLT
	movq	0(%rbp), %rbx
	movq	8(%rsp), %rdx
	jmp	.L2
	.size	__pthread_cond_timedwait_2_0, .-__pthread_cond_timedwait_2_0
