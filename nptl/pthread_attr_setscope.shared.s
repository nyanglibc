	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_setscope
	.type	__pthread_attr_setscope, @function
__pthread_attr_setscope:
	testl	%esi, %esi
	je	.L3
	cmpl	$1, %esi
	movl	$22, %edx
	movl	$95, %eax
	cmovne	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	andl	$-5, 8(%rdi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_setscope, .-__pthread_attr_setscope
	.globl	pthread_attr_setscope
	.set	pthread_attr_setscope,__pthread_attr_setscope
