	.text
	.p2align 4,,15
	.globl	__libc_accept
	.type	__libc_accept, @function
__libc_accept:
#APP
# 26 "../sysdeps/unix/sysv/linux/accept.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$43, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/accept.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__pthread_enable_asynccancel@PLT
	movq	%r12, %rdx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$43, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/accept.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__pthread_disable_asynccancel@PLT
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__libc_accept, .-__libc_accept
	.weak	accept
	.set	accept,__libc_accept
