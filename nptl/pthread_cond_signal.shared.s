	.text
#APP
	.symver __pthread_cond_signal,pthread_cond_signal@@GLIBC_2.3.2
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__pthread_cond_signal
	.type	__pthread_cond_signal, @function
__pthread_cond_signal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movl	36(%rdi), %ebp
	movl	%ebp, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	je	.L49
	movl	$128, %eax
	andl	$1, %ebp
	leaq	32(%rdi), %rbx
	cmovne	%eax, %ebp
	movl	32(%rdi), %eax
	movq	%rdi, %r9
.L4:
	testb	$3, %al
	jne	.L77
	movl	%eax, %edx
	orl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L4
.L5:
	movq	(%r9), %rax
	leaq	40(%r9), %r12
	movq	%rax, %rdx
	notq	%rdx
	andl	$1, %edx
	leaq	4(%rdx), %rcx
	movl	%edx, %r8d
	movl	8(%r9,%rcx,4), %ecx
	testl	%ecx, %ecx
	jne	.L16
	movq	%rax, %rsi
	shrq	%rax
	andl	$1, %esi
	movq	%rsi, (%rsp)
	movl	32(%r9), %ecx
	leaq	(%r9,%rsi,4), %r15
	movl	%ecx, %edi
	shrl	$2, %edi
	movl	%edi, 12(%rsp)
	movq	8(%r9), %rcx
	addl	24(%r15), %eax
	shrq	%rcx
	movq	%rcx, 16(%rsp)
	subl	%edi, %eax
	cmpl	%eax, %ecx
	jne	.L78
.L17:
	movl	32(%r9), %eax
.L30:
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$-4, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L30
	andl	$3, %edx
	xorl	%r12d, %r12d
	cmpl	$2, %edx
	jne	.L49
.L31:
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L79
.L28:
	testb	%r12b, %r12b
	jne	.L80
.L49:
	addq	$40, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	cmpq	$1, (%rsp)
	movq	8(%r9), %rcx
	movl	12(%rsp), %eax
	sbbl	%edx, %edx
	andl	$2, %edx
	leal	-1(%rdx,%rax,2), %eax
	addq	%rcx, %rax
	movq	%rax, 8(%r9)
	movq	24(%rsp), %rax
	movl	$0, (%rax)
	movq	(%r9), %rax
.L24:
	movq	%rax, %rcx
	movq	%rax, %rdx
	xorq	$1, %rcx
	lock cmpxchgq	%rcx, (%r9)
	jne	.L24
	movq	%rdx, %rax
	movl	12(%rsp), %edx
	addl	16(%rsp), %edx
	shrq	%rax
	subl	%edx, %eax
	movl	32(%r9), %edx
	leal	0(,%rax,4), %ecx
	andl	$3, %edx
	orl	%ecx, %edx
	movl	%edx, %esi
	xchgl	(%rbx), %esi
	xorl	%esi, %edx
	andl	$3, %edx
	je	.L25
	orl	$2, %ecx
	movl	%ecx, 32(%r9)
.L25:
	addl	24(%r15), %eax
	movl	(%rsp), %r8d
	testl	%eax, %eax
	movl	%eax, 24(%r15)
	je	.L17
	.p2align 4,,10
	.p2align 3
.L16:
	movl	%r8d, %eax
	salq	$2, %rax
	leaq	(%r12,%rax), %rdi
	lock addl	$2, (%rdi)
	subl	$1, 24(%r9,%rax)
	movl	32(%r9), %eax
.L26:
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$-4, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L26
	andl	$3, %edx
	cmpl	$2, %edx
	je	.L81
.L27:
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L49
	cmpl	$-22, %eax
	je	.L49
	cmpl	$-14, %eax
	je	.L49
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L78:
	leaq	40(%r9), %r12
	salq	$2, %rdx
	leaq	(%r12,%rdx), %rax
	movq	%rax, 24(%rsp)
	lock orl	$1, (%rax)
	leaq	16(%r9,%rdx), %r8
	movl	(%r8), %eax
.L18:
	movl	%eax, %edx
	lock cmpxchgl	%eax, (%r8)
	jne	.L18
	shrl	%edx
	je	.L19
	movl	%ebp, %r14d
	movl	$202, %r13d
	xorb	$-128, %r14b
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	movl	(%r8), %eax
	shrl	%eax
	je	.L19
.L22:
	movl	(%r8), %eax
.L20:
	movl	%eax, %ecx
	orl	$1, %ecx
	lock cmpxchgl	%ecx, (%r8)
	jne	.L20
	movl	%ecx, %eax
	movl	%ecx, %edx
	shrl	%eax
	je	.L21
	xorl	%r10d, %r10d
	movl	%r14d, %esi
	movq	%r8, %rdi
	movl	%r13d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L21
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L13
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L21
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L79:
	cmpl	$-22, %eax
	je	.L28
	cmpl	$-14, %eax
	je	.L28
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L81:
	movl	$1, %r12d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L77:
	movl	%ebp, %r12d
	movl	$202, %r8d
	movl	$1, %r13d
	xorb	$-128, %r12b
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-4, %edx
	andl	$3, %ecx
	orl	$2, %edx
	cmpl	$2, %ecx
	je	.L9
	lock cmpxchgl	%edx, (%rbx)
	jne	.L7
	testb	$3, %al
	je	.L5
	andl	$-4, %eax
	orl	$2, %eax
	movl	%eax, %edx
.L9:
	xorl	%r10d, %r10d
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%r8d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L82
.L11:
	movl	(%rbx), %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L82:
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L13
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L11
.L13:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
.L80:
	leaq	40(%r9,%r8,4), %rdi
	jmp	.L27
	.size	__pthread_cond_signal, .-__pthread_cond_signal
