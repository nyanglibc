	.text
	.p2align 4,,15
	.type	cleanup, @function
cleanup:
	movq	%rdi, -8(%rsp)
	movq	%fs:16, %rax
	xorl	%edx, %edx
	lock cmpxchgq	%rdx, -8(%rsp)
	ret
	.size	cleanup, .-cleanup
	.p2align 4,,15
	.globl	__pthread_clockjoin_ex
	.hidden	__pthread_clockjoin_ex
	.type	__pthread_clockjoin_ex, @function
__pthread_clockjoin_ex:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	js	.L17
	cmpq	%rdi, 1576(%rdi)
	je	.L8
	movq	%fs:16, %r9
	cmpq	%rdi, %r9
	je	.L6
	cmpq	%rdi, 1576(%r9)
	je	.L28
.L7:
	leaq	1576(%rdi), %r10
	xorl	%r11d, %r11d
	movq	%r9, %rax
	lock cmpxchgq	%r11, (%r10)
	je	.L8
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movl	%edx, %ebp
	movq	%rdi, %rbx
	subq	$56, %rsp
	testb	%r8b, %r8b
	jne	.L9
.L27:
	movq	1584(%rbx), %rax
.L10:
	testq	%r13, %r13
	movl	$-1, 720(%rbx)
	je	.L16
	movq	%rax, 0(%r13)
.L16:
	movq	%rbx, %rdi
	call	__free_tcb
	xorl	%eax, %eax
.L3:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L28:
	testb	$60, 776(%rdi)
	jne	.L7
.L6:
	movl	776(%r9), %r10d
	movl	$35, %eax
	andl	$-71, %r10d
	cmpl	$8, %r10d
	je	.L7
	rep ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	leaq	16(%rsp), %r15
	leaq	cleanup(%rip), %rsi
	movq	%r10, %rdx
	leaq	720(%rbx), %r14
	movq	%r15, %rdi
	call	__pthread_cleanup_push
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$128, %r8d
	movq	%r12, %rcx
	movl	%ebp, %edx
	movq	%r14, %rdi
	call	__GI___futex_abstimed_wait_cancelable64
	cmpl	$110, %eax
	je	.L12
	cmpl	$75, %eax
	je	.L12
.L11:
	movl	(%r14), %esi
	testl	%esi, %esi
	jne	.L13
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	__pthread_cleanup_pop
	movq	1584(%rbx), %rax
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L17:
	movl	$3, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	xorl	%esi, %esi
	movq	%r15, %rdi
	movl	%eax, 12(%rsp)
	call	__pthread_cleanup_pop
	movl	12(%rsp), %eax
	testl	%eax, %eax
	je	.L27
	movq	$0, 1576(%rbx)
	jmp	.L3
	.size	__pthread_clockjoin_ex, .-__pthread_clockjoin_ex
	.hidden	__pthread_cleanup_pop
	.hidden	__pthread_cleanup_push
	.hidden	__free_tcb
