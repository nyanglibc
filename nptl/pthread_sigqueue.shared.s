	.text
	.p2align 4,,15
	.globl	pthread_sigqueue
	.type	pthread_sigqueue, @function
pthread_sigqueue:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	addq	$-128, %rsp
	movl	720(%rdi), %ebx
	testl	%ebx, %ebx
	jle	.L3
	leal	-32(%rsi), %ecx
	movl	$22, %eax
	cmpl	$1, %ecx
	jbe	.L1
	movq	%rsp, %r14
	movl	%esi, %r12d
	movq	%rdx, %rbp
	call	getpid@PLT
	leaq	8(%r14), %rdi
	movl	%eax, %r13d
	movl	$15, %ecx
	xorl	%eax, %eax
	movq	$0, 4(%rsp)
	movq	$0, 120(%rsp)
	rep stosq
	movl	%r12d, (%rsp)
	movl	$-1, 8(%rsp)
	movl	%r13d, 16(%rsp)
	call	getuid@PLT
	movq	%rbp, 24(%rsp)
	movl	%eax, 20(%rsp)
	movq	%r14, %r10
	movl	%r12d, %edx
	movl	%ebx, %esi
	movl	%r13d, %edi
	movl	$297, %eax
#APP
# 64 "../sysdeps/unix/sysv/linux/pthread_sigqueue.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	negl	%edx
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmova	%edx, %eax
.L1:
	subq	$-128, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	subq	$-128, %rsp
	movl	$3, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	pthread_sigqueue, .-pthread_sigqueue
