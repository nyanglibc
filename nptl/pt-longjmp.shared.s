	.text
#APP
	.symver longjmp_alias,longjmp@GLIBC_2.2.5
	.symver siglongjmp_alias,siglongjmp@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	longjmp_compat, @function
longjmp_compat:
	subq	$8, %rsp
	call	__libc_siglongjmp@PLT
	.size	longjmp_compat, .-longjmp_compat
	.globl	longjmp_alias
	.set	longjmp_alias,longjmp_compat
	.globl	siglongjmp_alias
	.set	siglongjmp_alias,longjmp_alias
