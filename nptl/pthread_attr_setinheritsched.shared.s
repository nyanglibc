	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_setinheritsched
	.type	__pthread_attr_setinheritsched, @function
__pthread_attr_setinheritsched:
	cmpl	$1, %esi
	movl	$22, %eax
	ja	.L1
	testl	%esi, %esi
	movl	8(%rdi), %eax
	jne	.L6
	andl	$-3, %eax
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L6:
	orl	$2, %eax
	movl	%eax, 8(%rdi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_setinheritsched, .-__pthread_attr_setinheritsched
	.globl	pthread_attr_setinheritsched
	.set	pthread_attr_setinheritsched,__pthread_attr_setinheritsched
