	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"r"
.LC1:
	.string	"/proc/mounts"
.LC2:
	.string	"/etc/fstab"
.LC3:
	.string	"tmpfs"
.LC4:
	.string	"shm"
	.text
	.p2align 4,,15
	.type	where_is_shmfs, @function
where_is_shmfs:
	pushq	%r15
	pushq	%r14
	leaq	defaultdir(%rip), %rdi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$712, %rsp
	leaq	64(%rsp), %rbp
	movq	%rbp, %rsi
	call	__statfs@PLT
	testl	%eax, %eax
	jne	.L2
	movq	64(%rsp), %rax
	cmpq	$16914836, %rax
	je	.L16
	movl	$2240043254, %edx
	cmpq	%rdx, %rax
	je	.L16
.L2:
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	call	__setmntent@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	je	.L35
.L5:
	leaq	192(%rsp), %r13
	leaq	16(%rsp), %r12
	leaq	.LC3(%rip), %r14
.L12:
	movl	$512, %ecx
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	__getmntent_r@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L10
	movq	16(%r15), %rax
	movl	$6, %ecx
	movq	%r14, %rdi
	movq	%rax, %rsi
	repz cmpsb
	je	.L6
	leaq	.LC4(%rip), %rdi
	movl	$4, %ecx
	movq	%rax, %rsi
	repz cmpsb
	jne	.L12
.L6:
	movq	8(%r15), %rdi
	movq	%rbp, %rsi
	call	__statfs@PLT
	testl	%eax, %eax
	jne	.L12
	movq	64(%rsp), %rax
	cmpq	$16914836, %rax
	je	.L17
	movl	$2240043254, %esi
	cmpq	%rsi, %rax
	jne	.L12
.L17:
	movq	8(%r15), %r15
	movq	%r15, %rdi
	call	strlen@PLT
	testq	%rax, %rax
	je	.L12
	leaq	2(%rax), %rdi
	movq	%rax, 8(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	movq	%rax, mountpoint(%rip)
	je	.L10
	movq	8(%rsp), %rdx
	movq	%r15, %rsi
	movq	%rax, %rdi
	call	__mempcpy@PLT
	cmpb	$47, -1(%rax)
	je	.L11
	movb	$47, (%rax)
	addq	$1, %rax
.L11:
	movb	$0, (%rax)
	subq	%rbp, %rax
	movq	%rax, 8+mountpoint(%rip)
.L10:
	movq	%rbx, %rdi
	call	__endmntent@PLT
.L1:
	addq	$712, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	leaq	defaultdir(%rip), %rax
	movq	$9, 8+mountpoint(%rip)
	movq	%rax, mountpoint(%rip)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	.LC0(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	call	__setmntent@PLT
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.L5
	jmp	.L1
	.size	where_is_shmfs, .-where_is_shmfs
	.p2align 4,,15
	.globl	__shm_directory
	.type	__shm_directory, @function
__shm_directory:
	pushq	%rbx
	leaq	where_is_shmfs(%rip), %rsi
	movq	%rdi, %rbx
	leaq	once(%rip), %rdi
	call	__pthread_once@PLT
	movq	mountpoint(%rip), %rax
	testq	%rax, %rax
	je	.L40
	movq	8+mountpoint(%rip), %rdx
	movq	%rdx, (%rbx)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L40:
	movq	errno@gottpoff(%rip), %rdx
	movl	$38, %fs:(%rdx)
	popq	%rbx
	ret
	.size	__shm_directory, .-__shm_directory
	.p2align 4,,15
	.globl	__shm_directory_freeres
	.type	__shm_directory_freeres, @function
__shm_directory_freeres:
	movq	mountpoint(%rip), %rdi
	leaq	defaultdir(%rip), %rax
	cmpq	%rax, %rdi
	je	.L41
	jmp	free@PLT
	.p2align 4,,10
	.p2align 3
.L41:
	rep ret
	.size	__shm_directory_freeres, .-__shm_directory_freeres
	.local	once
	.comm	once,4,4
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
	.type	defaultdir, @object
	.size	defaultdir, 10
defaultdir:
	.string	"/dev/shm/"
	.local	mountpoint
	.comm	mountpoint,16,16
	.weak	__pthread_once
