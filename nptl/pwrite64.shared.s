	.text
	.p2align 4,,15
	.globl	__libc_pwrite64
	.hidden	__libc_pwrite64
	.type	__libc_pwrite64, @function
__libc_pwrite64:
	movq	%rcx, %r10
#APP
# 25 "../sysdeps/unix/sysv/linux/pwrite64.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$18, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/pwrite64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rcx, %r12
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__pthread_enable_asynccancel
	movq	%r12, %r10
	movl	%eax, %r8d
	movq	%r13, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$18, %eax
#APP
# 25 "../sysdeps/unix/sysv/linux/pwrite64.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__pthread_disable_asynccancel
	movq	8(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_pwrite64, .-__libc_pwrite64
	.weak	pwrite
	.set	pwrite,__libc_pwrite64
	.weak	__pwrite
	.set	__pwrite,__libc_pwrite64
	.globl	__libc_pwrite
	.set	__libc_pwrite,__libc_pwrite64
	.weak	pwrite64
	.set	pwrite64,__libc_pwrite64
	.weak	__pwrite64
	.set	__pwrite64,__libc_pwrite64
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
