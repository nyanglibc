	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_trywrlock
	.type	__pthread_rwlock_trywrlock, @function
__pthread_rwlock_trywrlock:
	movl	(%rdi), %eax
	movl	48(%rdi), %ecx
.L2:
	testb	$2, %al
	jne	.L7
	movl	%eax, %edx
	shrl	$3, %edx
	testl	%edx, %edx
	jne	.L14
.L5:
	movl	%eax, %edx
	orl	$3, %edx
	lock cmpxchgl	%edx, (%rdi)
	jne	.L2
	testb	$1, %al
	movl	$1, 12(%rdi)
	jne	.L3
	movl	$1, 8(%rdi)
.L3:
#APP
# 58 "pthread_rwlock_trywrlock.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	movl	%eax, 24(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	testl	%ecx, %ecx
	je	.L7
	testb	$1, %al
	jne	.L5
.L7:
	movl	$16, %eax
	ret
	.size	__pthread_rwlock_trywrlock, .-__pthread_rwlock_trywrlock
	.globl	pthread_rwlock_trywrlock
	.set	pthread_rwlock_trywrlock,__pthread_rwlock_trywrlock
