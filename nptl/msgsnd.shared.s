	.text
	.p2align 4,,15
	.globl	__libc_msgsnd
	.type	__libc_msgsnd, @function
__libc_msgsnd:
	movl	%ecx, %r10d
#APP
# 26 "../sysdeps/unix/sysv/linux/msgsnd.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$69, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/msgsnd.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r13
	pushq	%r12
	movl	%ecx, %r13d
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$24, %rsp
	call	__pthread_enable_asynccancel
	movl	%r13d, %r10d
	movl	%eax, %r8d
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$69, %eax
#APP
# 26 "../sysdeps/unix/sysv/linux/msgsnd.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__pthread_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__libc_msgsnd, .-__libc_msgsnd
	.weak	msgsnd
	.set	msgsnd,__libc_msgsnd
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
