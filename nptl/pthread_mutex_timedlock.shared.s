	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.align 8
.LC1:
	.string	"../nptl/pthread_mutex_timedlock.c"
	.align 8
.LC2:
	.string	"e != EDEADLK || (kind != PTHREAD_MUTEX_ERRORCHECK_NP && kind != PTHREAD_MUTEX_RECURSIVE_NP)"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC3:
	.string	"e != ESRCH || !robust"
	.section	.rodata.str1.8
	.align 8
.LC4:
	.string	"robust || (oldval & FUTEX_OWNER_DIED) == 0"
	.section	.rodata.str1.1
.LC5:
	.string	"mutex->__data.__owner == 0"
	.text
	.p2align 4,,15
	.globl	__pthread_mutex_clocklock_common
	.type	__pthread_mutex_clocklock_common, @function
__pthread_mutex_clocklock_common:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
#APP
# 51 "../nptl/pthread_mutex_timedlock.c" 1
	movl %fs:720,%r15d
# 0 "" 2
#NO_APP
	movl	16(%rdi), %eax
	leaq	16(%rdi), %r13
	andl	$383, %eax
	cmpl	$35, %eax
	jg	.L3
	cmpl	$32, %eax
	jge	.L4
	cmpl	$2, %eax
	je	.L5
	jle	.L139
	cmpl	$3, %eax
	je	.L9
	subl	$16, %eax
	cmpl	$3, %eax
	ja	.L80
	leaq	32(%rdi), %r14
#APP
# 142 "../nptl/pthread_mutex_timedlock.c" 1
	movq %r14,%fs:752
# 0 "" 2
#NO_APP
	movl	$-2147483648, %r9d
	movl	(%rdi), %edx
	xorl	%edi, %edi
.L36:
	testl	%edx, %edx
	jne	.L37
	movl	%r15d, %ecx
	movl	%edx, %eax
	orl	%edi, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	testl	%eax, %eax
	movl	%eax, %edx
	jne	.L37
	cmpl	$2147483646, 8(%rbx)
	je	.L140
	movl	$1, 4(%rbx)
#APP
# 297 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	andq	$-2, %rax
	movq	%r14, -8(%rax)
#APP
# 297 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 297 "../nptl/pthread_mutex_timedlock.c" 1
	movq %r14,%fs:736
# 0 "" 2
# 300 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
	.p2align 4,,10
	.p2align 3
#NO_APP
.L17:
	movl	%r15d, 8(%rbx)
	addl	$1, 12(%rbx)
	xorl	%ecx, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$67, %eax
	jg	.L11
	cmpl	$64, %eax
	jge	.L12
	subl	$48, %eax
	cmpl	$3, %eax
	ja	.L80
.L4:
	testl	%r12d, %r12d
	jne	.L80
	movl	16(%rbx), %r8d
	movl	%r8d, %r13d
	andl	$3, %r13d
	andl	$16, %r8d
	je	.L50
	leaq	32(%rbx), %rax
	orq	$1, %rax
#APP
# 335 "../nptl/pthread_mutex_timedlock.c" 1
	movq %rax,%fs:752
# 0 "" 2
#NO_APP
.L50:
	movl	(%rbx), %eax
	andl	$1073741823, %eax
	cmpl	%r15d, %eax
	je	.L141
.L51:
	xorl	%eax, %eax
	lock cmpxchgl	%r15d, (%rbx)
	testl	%eax, %eax
	je	.L53
	testl	%r8d, %r8d
	movl	$6, %esi
	movl	$128, %r12d
	je	.L142
.L54:
	movq	%rbp, %r10
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 257 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-11, %eax
	je	.L56
	jg	.L57
	cmpl	$-35, %eax
	je	.L56
	cmpl	$-22, %eax
	je	.L56
	cmpl	$-110, %eax
	je	.L58
.L55:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L139:
	testl	%eax, %eax
	jne	.L143
	movl	__pthread_force_elision(%rip), %edx
	testl	%edx, %edx
	je	.L24
	movl	16(%rdi), %eax
	testb	$3, %ah
	je	.L25
	testb	$1, %ah
	je	.L24
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	cmpl	$256, %eax
	jne	.L144
.L13:
	movl	16(%rbx), %r8d
	leaq	20(%rbx), %rsi
	movq	%rbp, %rcx
	movl	%r12d, %edx
	movq	%rbx, %rdi
	andl	$128, %r8d
	call	__lll_clocklock_elision
	movl	%eax, %ecx
.L1:
	addq	$40, %rsp
	movl	%ecx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L17
	movswl	20(%rdi), %eax
	movswl	__mutex_aconf(%rip), %edx
	movl	$1, %ecx
	leal	10(%rax,%rax), %eax
	cmpl	%edx, %eax
	cmovle	%eax, %edx
	xorl	%r13d, %r13d
	xorl	%esi, %esi
.L35:
	addl	$1, %r13d
	leal	-1(%r13), %eax
	cmpl	%eax, %edx
	jle	.L145
#APP
# 130 "../nptl/pthread_mutex_timedlock.c" 1
	pause
# 0 "" 2
#NO_APP
	movl	%esi, %eax
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L35
.L34:
	movswl	20(%rbx), %eax
	movl	%r13d, %ecx
	subl	%eax, %ecx
	movl	%eax, %edx
	leal	7(%rcx), %eax
	testl	%ecx, %ecx
	cmovns	%ecx, %eax
	sarl	$3, %eax
	addl	%edx, %eax
	movw	%ax, 20(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L5:
	cmpl	%r15d, 8(%rdi)
	je	.L86
.L24:
	movl	16(%rbx), %r14d
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	movl	$2, %r13d
	je	.L17
	.p2align 4,,10
	.p2align 3
.L23:
	movl	%r13d, %eax
#APP
# 364 "../sysdeps/nptl/futex-internal.h" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L17
	movl	%r14d, %r8d
	movq	%rbp, %rcx
	movl	%r12d, %edx
	andl	$128, %r8d
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__GI___futex_abstimed_wait64
	cmpl	$22, %eax
	movl	%eax, %ecx
	je	.L26
	cmpl	$110, %eax
	je	.L26
	cmpl	$75, %eax
	jne	.L23
.L27:
	movl	$75, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L143:
	cmpl	$1, %eax
	jne	.L80
.L8:
	cmpl	%r15d, 8(%rbx)
	jne	.L14
	movl	4(%rbx), %eax
	cmpl	$-1, %eax
	je	.L43
.L137:
	addl	$1, %eax
	xorl	%ecx, %ecx
	movl	%eax, 4(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L144:
	cmpl	$257, %eax
	je	.L8
	.p2align 4,,10
	.p2align 3
.L80:
	movl	$22, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L12:
	movl	16(%rdi), %eax
	cmpl	%r15d, 8(%rdi)
	movl	$-1, %r14d
	movl	(%rdi), %ecx
	je	.L146
.L73:
	shrl	$19, %ecx
	movl	%ecx, 12(%rsp)
	call	__pthread_current_priority
	cmpl	12(%rsp), %eax
	jg	.L147
	movl	12(%rsp), %esi
	movl	%r14d, %edi
	call	__pthread_tpp_change_priority
	testl	%eax, %eax
	movl	%eax, %ecx
	jne	.L1
	movl	12(%rsp), %r10d
	sall	$19, %r10d
	movl	%r10d, %eax
	orl	$1, %eax
	movl	%eax, %edi
	movl	%eax, 8(%rsp)
	movl	%r10d, %eax
	lock cmpxchgl	%edi, (%rbx)
	cmpl	%eax, %r10d
	je	.L72
	movl	%r10d, %r14d
	orl	$2, %r14d
	jmp	.L75
	.p2align 4,,10
	.p2align 3
.L148:
	movl	0(%r13), %r8d
	movq	%rbp, %rcx
	movl	%r12d, %edx
	movl	%r14d, %esi
	movq	%rbx, %rdi
	andl	$128, %r8d
	call	__GI___futex_abstimed_wait64
	cmpl	$110, %eax
	movl	%eax, %ecx
	je	.L1
	cmpl	$75, %eax
	movl	4(%rsp), %r10d
	je	.L1
.L77:
	movl	%r10d, %eax
	lock cmpxchgl	%r14d, (%rbx)
	cmpl	%r10d, %eax
	je	.L72
.L75:
	movl	8(%rsp), %eax
	lock cmpxchgl	%r14d, (%rbx)
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-524288, %edx
	cmpl	%edx, %r10d
	jne	.L88
	cmpl	%eax, %r10d
	je	.L77
	cmpq	$999999999, 8(%rbp)
	movl	%r10d, 4(%rsp)
	jbe	.L148
.L76:
	movl	12(%rsp), %edi
	movl	$-1, %esi
	call	__pthread_tpp_change_priority
	movl	$22, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L14:
	movl	16(%rbx), %r14d
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	movl	$2, %r13d
	jne	.L16
.L20:
	movl	$1, 4(%rbx)
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L53:
	testl	%r8d, %r8d
	je	.L20
.L79:
	cmpl	$2147483646, 8(%rbx)
	je	.L149
	movl	$1, 4(%rbx)
#APP
# 460 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	leaq	32(%rbx), %rax
	andq	$-2, %rdx
	movq	%rax, -8(%rdx)
#APP
# 460 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 32(%rbx)
	movq	%fs:16, %rsi
	leaq	736(%rsi), %rdx
	movq	%rdx, 24(%rbx)
	orq	$1, %rax
#APP
# 460 "../nptl/pthread_mutex_timedlock.c" 1
	movq %rax,%fs:736
# 0 "" 2
# 463 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L19:
	movl	%r14d, %r8d
	movq	%rbp, %rcx
	movl	%r12d, %edx
	andl	$128, %r8d
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__GI___futex_abstimed_wait64
	cmpl	$22, %eax
	movl	%eax, %ecx
	je	.L18
	cmpl	$110, %eax
	je	.L18
	cmpl	$75, %eax
	je	.L27
.L16:
	movl	%r13d, %eax
#APP
# 364 "../sysdeps/nptl/futex-internal.h" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L19
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$-4, %eax
	jl	.L55
	cmpl	$-3, %eax
	jle	.L56
	testl	%eax, %eax
	jne	.L55
.L56:
	cmpl	$-110, %eax
	je	.L58
	movl	%eax, %edx
	andl	$-33, %edx
	cmpl	$-35, %edx
	jne	.L60
	cmpl	$-35, %eax
	je	.L150
	cmpl	$-3, %eax
	jne	.L62
	testl	%r8d, %r8d
	jne	.L151
.L62:
	leaq	28(%rsp), %rbx
	.p2align 4,,10
	.p2align 3
.L63:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movl	%r12d, %r8d
	movq	%rbp, %rcx
	movq	%rbx, %rdi
	movl	$0, 28(%rsp)
	call	__GI___futex_abstimed_wait64
	cmpl	$110, %eax
	jne	.L63
.L58:
	movl	$110, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L146:
	andl	$3, %eax
	cmpl	$2, %eax
	je	.L86
	cmpl	$1, %eax
	jne	.L73
	movl	4(%rdi), %eax
	cmpl	%r14d, %eax
	jne	.L137
.L43:
	movl	$11, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L88:
	movl	12(%rsp), %r14d
	jmp	.L73
	.p2align 4,,10
	.p2align 3
.L72:
	movl	8(%rbx), %eax
	testl	%eax, %eax
	je	.L20
	leaq	__PRETTY_FUNCTION__.10063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$563, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L37:
	movl	%edx, %ecx
	andl	$1073741824, %ecx
	jne	.L152
	movl	%edx, %eax
	andl	$1073741823, %eax
	cmpl	%r15d, %eax
	je	.L153
.L41:
	cmpq	$999999999, 8(%rbp)
	ja	.L80
	cmpq	$0, 0(%rbp)
	js	.L58
	testl	%edx, %edx
	movl	%edx, %esi
	js	.L46
	orl	$-2147483648, %esi
	movl	%edx, %eax
	lock cmpxchgl	%esi, (%rbx)
	jne	.L154
.L46:
	movq	%rbp, %rcx
	movl	$128, %r8d
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	%r9d, 4(%rsp)
	call	__GI___futex_abstimed_wait64
	cmpl	$110, %eax
	movl	%eax, %ecx
	je	.L1
	cmpl	$75, %eax
	je	.L1
	movl	4(%rsp), %r9d
	movl	(%rbx), %edx
	movl	%r9d, %edi
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L60:
	testl	%eax, %eax
	jne	.L155
	movl	(%rbx), %eax
	andl	$1073741824, %eax
	testl	%r8d, %r8d
	jne	.L65
	testl	%eax, %eax
	je	.L20
	leaq	__PRETTY_FUNCTION__.10063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	movl	$411, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L86:
	movl	$35, %ecx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L142:
	movl	16(%rbx), %r12d
	andl	$128, %r12d
	movl	%r12d, %esi
	xorb	$-122, %sil
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L25:
	orb	$1, %ah
	movl	%eax, 16(%rdi)
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L140:
	movl	$0, 4(%rbx)
#APP
# 286 "../nptl/pthread_mutex_timedlock.c" 1
	xchgl %edx, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jg	.L156
.L67:
#APP
# 450 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$131, %ecx
	jmp	.L1
.L141:
	cmpl	$2, %r13d
	je	.L136
	cmpl	$1, %r13d
	jne	.L51
#APP
# 360 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	4(%rbx), %eax
	cmpl	$-1, %eax
	jne	.L137
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L155:
	movl	%eax, %ecx
	negl	%ecx
	jmp	.L1
.L152:
	movl	%edx, %ecx
	movl	%r15d, %eax
	orl	%edi, %eax
	andl	$-2147483648, %ecx
	orl	%eax, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rbx)
	cmpl	%eax, %edx
	je	.L157
	movl	%eax, %edx
	jmp	.L36
.L150:
	subl	$1, %r13d
	cmpl	$1, %r13d
	ja	.L62
	leaq	__PRETTY_FUNCTION__.10063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC2(%rip), %rdi
	movl	$393, %edx
	call	__assert_fail@PLT
.L158:
	cmpl	$110, %eax
	je	.L32
	cmpl	$75, %eax
	je	.L32
.L31:
	movl	$2, %eax
#APP
# 364 "../sysdeps/nptl/futex-internal.h" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	je	.L34
	movl	%r14d, %r8d
	movq	%rbp, %rcx
	movl	%r12d, %edx
	andl	$128, %r8d
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__GI___futex_abstimed_wait64
	cmpl	$22, %eax
	movl	%eax, %ecx
	jne	.L158
.L32:
	movswl	20(%rbx), %eax
	movl	$8, %edi
	subl	%eax, %r13d
	movl	%eax, %esi
	movl	%r13d, %eax
	cltd
	idivl	%edi
	addl	%esi, %eax
	movw	%ax, 20(%rbx)
.L26:
	testl	%ecx, %ecx
	jne	.L1
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L65:
	testl	%eax, %eax
	je	.L79
#APP
# 416 "../nptl/pthread_mutex_timedlock.c" 1
	lock;andl $-1073741825, (%rbx)
# 0 "" 2
#NO_APP
	movabsq	$9223372032559808513, %rax
	movq	%rax, 4(%rbx)
#APP
# 426 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	leaq	32(%rbx), %rax
	andq	$-2, %rdx
	movq	%rax, -8(%rdx)
#APP
# 426 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 32(%rbx)
	movq	%fs:16, %rsi
	leaq	736(%rsi), %rdx
	movq	%rdx, 24(%rbx)
	orq	$1, %rax
#APP
# 426 "../nptl/pthread_mutex_timedlock.c" 1
	movq %rax,%fs:736
# 0 "" 2
# 429 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$130, %ecx
	jmp	.L1
.L147:
.L71:
	cmpl	$-1, %r14d
	je	.L80
	movl	%r14d, 12(%rsp)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L149:
	movl	$0, 4(%rbx)
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 307 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L67
	cmpl	$-11, %eax
	je	.L67
	jg	.L68
	cmpl	$-38, %eax
	je	.L67
	cmpl	$-35, %eax
	je	.L67
	cmpl	$-110, %eax
	jne	.L55
	jmp	.L67
	.p2align 4,,10
	.p2align 3
.L157:
	movabsq	$9223372032559808513, %rax
	movq	%rax, 4(%rbx)
#APP
# 190 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	andq	$-2, %rax
	movq	%r14, -8(%rax)
#APP
# 190 "../nptl/pthread_mutex_timedlock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 190 "../nptl/pthread_mutex_timedlock.c" 1
	movq %r14,%fs:736
# 0 "" 2
# 193 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$130, %ecx
	jmp	.L1
.L136:
#APP
# 352 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$35, %ecx
	jmp	.L1
.L18:
	testl	%ecx, %ecx
	je	.L20
	jmp	.L1
.L153:
	movl	0(%r13), %eax
	andl	$127, %eax
	cmpl	$18, %eax
	je	.L136
	cmpl	$17, %eax
	jne	.L41
#APP
# 219 "../nptl/pthread_mutex_timedlock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	4(%rbx), %eax
	cmpl	$-1, %eax
	je	.L43
	addl	$1, %eax
	movl	%eax, 4(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 286 "../nptl/pthread_mutex_timedlock.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L67
.L145:
	movl	16(%rbx), %r14d
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L31
	movswl	20(%rbx), %eax
	movl	$8, %esi
	subl	%eax, %r13d
	movl	%eax, %ecx
	movl	%r13d, %eax
	cltd
	idivl	%esi
	addl	%eax, %ecx
	movw	%cx, 20(%rbx)
	jmp	.L17
.L68:
	cmpl	$-4, %eax
	jl	.L55
	cmpl	$-3, %eax
	jle	.L67
	addl	$1, %eax
	je	.L67
	jmp	.L55
.L154:
	movl	(%rbx), %edx
	jmp	.L36
.L151:
	leaq	__PRETTY_FUNCTION__.10063(%rip), %rcx
	leaq	.LC1(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$396, %edx
	call	__assert_fail@PLT
	.size	__pthread_mutex_clocklock_common, .-__pthread_mutex_clocklock_common
	.p2align 4,,15
	.globl	__pthread_mutex_clocklock
	.type	__pthread_mutex_clocklock, @function
__pthread_mutex_clocklock:
	cmpl	$1, %esi
	ja	.L160
	jmp	__pthread_mutex_clocklock_common@PLT
	.p2align 4,,10
	.p2align 3
.L160:
	movl	$22, %eax
	ret
	.size	__pthread_mutex_clocklock, .-__pthread_mutex_clocklock
	.weak	pthread_mutex_clocklock
	.set	pthread_mutex_clocklock,__pthread_mutex_clocklock
	.p2align 4,,15
	.globl	__pthread_mutex_timedlock
	.type	__pthread_mutex_timedlock, @function
__pthread_mutex_timedlock:
	movq	%rsi, %rdx
	xorl	%esi, %esi
	jmp	__pthread_mutex_clocklock_common@PLT
	.size	__pthread_mutex_timedlock, .-__pthread_mutex_timedlock
	.weak	pthread_mutex_timedlock
	.set	pthread_mutex_timedlock,__pthread_mutex_timedlock
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	__PRETTY_FUNCTION__.10063, @object
	.size	__PRETTY_FUNCTION__.10063, 33
__PRETTY_FUNCTION__.10063:
	.string	"__pthread_mutex_clocklock_common"
	.hidden	__pthread_tpp_change_priority
	.hidden	__pthread_current_priority
	.hidden	__mutex_aconf
	.hidden	__lll_clocklock_elision
	.hidden	__pthread_force_elision
