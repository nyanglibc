	.text
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	free_mem, @function
free_mem:
	pushq	%rbx
#APP
# 145 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, atfork_lock(%rip)
# 0 "" 2
#NO_APP
.L3:
	movq	16+fork_handlers(%rip), %rdi
	leaq	24+fork_handlers(%rip), %rbx
	cmpq	%rbx, %rdi
	je	.L4
	call	free@PLT
.L4:
	movq	$0, fork_handlers(%rip)
	movq	$48, 8+fork_handlers(%rip)
	movq	%rbx, 16+fork_handlers(%rip)
#APP
# 149 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	subl	$1, atfork_lock(%rip)
.L1:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, atfork_lock(%rip)
	je	.L3
	leaq	atfork_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
#APP
# 149 "register-atfork.c" 1
	xchgl %eax, atfork_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	atfork_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 149 "register-atfork.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	popq	%rbx
	ret
	.size	free_mem, .-free_mem
	.text
	.p2align 4,,15
	.globl	__register_atfork
	.hidden	__register_atfork
	.type	__register_atfork, @function
__register_atfork:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdx, %rbx
	subq	$16, %rsp
#APP
# 40 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L10
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, atfork_lock(%rip)
# 0 "" 2
#NO_APP
.L11:
	cmpb	$0, fork_handler_init(%rip)
	jne	.L12
	leaq	24+fork_handlers(%rip), %rax
	movq	$48, 8+fork_handlers(%rip)
	movb	$1, fork_handler_init(%rip)
	movq	%rax, 16+fork_handlers(%rip)
	xorl	%eax, %eax
.L13:
	movq	%rax, %r8
	addq	$1, %rax
	salq	$5, %r8
	addq	16+fork_handlers(%rip), %r8
	movq	%rax, fork_handlers(%rip)
	testq	%r8, %r8
	je	.L14
	movq	%r12, (%r8)
	movq	%rbp, 8(%r8)
	movq	%rbx, 16(%r8)
	movq	%rcx, 24(%r8)
.L14:
#APP
# 58 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L18
	subl	$1, atfork_lock(%rip)
.L19:
	cmpq	$1, %r8
	sbbl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	andl	$12, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	movq	8+fork_handlers(%rip), %rdx
	cmpq	$-1, %rdx
	je	.L21
	movq	fork_handlers(%rip), %rax
	cmpq	%rax, %rdx
	jne	.L13
	leaq	24+fork_handlers(%rip), %rsi
	movl	$32, %edx
	movq	%rcx, 8(%rsp)
	leaq	-24(%rsi), %rdi
	call	__libc_dynarray_emplace_enlarge
	testb	%al, %al
	movq	8(%rsp), %rcx
	je	.L27
	movq	fork_handlers(%rip), %rax
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, atfork_lock(%rip)
	je	.L11
	leaq	atfork_lock(%rip), %rdi
	movq	%rcx, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rcx
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
#APP
# 58 "register-atfork.c" 1
	xchgl %eax, atfork_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L19
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	atfork_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 58 "register-atfork.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%r8d, %r8d
	jmp	.L14
.L27:
	movq	16+fork_handlers(%rip), %rdi
	leaq	24+fork_handlers(%rip), %rbx
	cmpq	%rbx, %rdi
	je	.L16
	call	free@PLT
.L16:
	movq	%rbx, 16+fork_handlers(%rip)
	movq	$0, fork_handlers(%rip)
	xorl	%r8d, %r8d
	movq	$-1, 8+fork_handlers(%rip)
	jmp	.L14
	.size	__register_atfork, .-__register_atfork
	.p2align 4,,15
	.globl	__unregister_atfork
	.hidden	__unregister_atfork
	.type	__unregister_atfork, @function
__unregister_atfork:
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 80 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L29
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, atfork_lock(%rip)
# 0 "" 2
#NO_APP
.L30:
	movq	fork_handlers(%rip), %rcx
	testq	%rcx, %rcx
	je	.L32
	movq	16+fork_handlers(%rip), %rsi
	cmpq	24(%rsi), %rbx
	je	.L46
	leaq	32(%rsi), %rax
	xorl	%edx, %edx
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L35:
	movq	%rax, %r8
	addq	$32, %rax
	cmpq	-8(%rax), %rbx
	je	.L33
.L34:
	addq	$1, %rdx
	cmpq	%rdx, %rcx
	jne	.L35
.L32:
#APP
# 106 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L43
	subl	$1, atfork_lock(%rip)
.L28:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movq	%rsi, %r8
	.p2align 4,,10
	.p2align 3
.L33:
	movq	%rcx, %rax
	leaq	32(%r8), %rdi
	movq	%r8, %rdx
	salq	$5, %rax
	addq	%rax, %rsi
	movq	%rdi, %rax
	cmpq	%rdi, %rsi
	je	.L45
	.p2align 4,,10
	.p2align 3
.L38:
	cmpq	%rbx, 24(%rax)
	je	.L37
	movdqu	(%rax), %xmm0
	addq	$32, %rdx
	movups	%xmm0, -32(%rdx)
	movdqu	16(%rax), %xmm0
	movups	%xmm0, -16(%rdx)
.L37:
	addq	$32, %rax
	cmpq	%rax, %rsi
	jne	.L38
	addq	$64, %r8
	subq	%r8, %rsi
	andq	$-32, %rsi
	leaq	32(%rdi,%rsi), %rdi
.L45:
	subq	%rdx, %rdi
	movq	%rdi, %rdx
	sarq	$5, %rdx
	testq	%rdx, %rdx
	je	.L32
	xorl	%esi, %esi
	xorl	%eax, %eax
.L42:
	testq	%rcx, %rcx
	je	.L58
	subq	$1, %rcx
	movl	$1, %esi
.L40:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L42
	testb	%sil, %sil
	je	.L32
.L59:
	movq	%rcx, fork_handlers(%rip)
	jmp	.L32
	.p2align 4,,10
	.p2align 3
.L58:
	addq	$1, %rax
	cmpq	%rax, %rdx
	jne	.L40
	testb	%sil, %sil
	je	.L32
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L29:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, atfork_lock(%rip)
	je	.L30
	leaq	atfork_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L43:
	xorl	%eax, %eax
#APP
# 106 "register-atfork.c" 1
	xchgl %eax, atfork_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L28
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	atfork_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 106 "register-atfork.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	popq	%rbx
	ret
	.size	__unregister_atfork, .-__unregister_atfork
	.p2align 4,,15
	.globl	__run_fork_handlers
	.hidden	__run_fork_handlers
	.type	__run_fork_handlers, @function
__run_fork_handlers:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$8, %rsp
	testl	%edi, %edi
	jne	.L61
	testb	%sil, %sil
	je	.L63
#APP
# 117 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L64
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, atfork_lock(%rip)
# 0 "" 2
#NO_APP
.L63:
	movq	fork_handlers(%rip), %rbx
	testq	%rbx, %rbx
	je	.L60
	leaq	-1(%rbx), %rbp
	salq	$5, %rbx
	subq	$32, %rbx
	.p2align 4,,10
	.p2align 3
.L67:
	movq	16+fork_handlers(%rip), %rax
	movq	(%rax,%rbx), %rax
	testq	%rax, %rax
	je	.L68
	call	*%rax
.L68:
	testq	%rbp, %rbp
	je	.L60
	movq	fork_handlers(%rip), %rdi
	subq	$1, %rbp
	subq	$32, %rbx
	cmpq	%rdi, %rbp
	jb	.L67
	movq	%rbp, %rsi
	call	__libc_dynarray_at_failure
	.p2align 4,,10
	.p2align 3
.L61:
	movq	fork_handlers(%rip), %r13
	xorl	%ebx, %ebx
	testq	%r13, %r13
	jne	.L71
	.p2align 4,,10
	.p2align 3
.L76:
	testb	%r12b, %r12b
	je	.L60
#APP
# 138 "register-atfork.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L77
	subl	$1, atfork_lock(%rip)
.L60:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L73:
	cmpl	$2, %ebp
	je	.L94
.L74:
	addq	$1, %rbx
	cmpq	%r13, %rbx
	je	.L76
	movq	fork_handlers(%rip), %rdi
	cmpq	%rdi, %rbx
	jnb	.L95
.L71:
	movq	%rbx, %rdx
	salq	$5, %rdx
	addq	16+fork_handlers(%rip), %rdx
	cmpl	$1, %ebp
	jne	.L73
	movq	16(%rdx), %rax
	testq	%rax, %rax
	je	.L74
	call	*%rax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L94:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L74
	call	*%rax
	jmp	.L74
	.p2align 4,,10
	.p2align 3
.L95:
	movq	%rbx, %rsi
	call	__libc_dynarray_at_failure
	.p2align 4,,10
	.p2align 3
.L64:
	movl	%edi, %eax
	lock cmpxchgl	%edx, atfork_lock(%rip)
	je	.L63
	leaq	atfork_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L63
.L77:
	xorl	%eax, %eax
#APP
# 138 "register-atfork.c" 1
	xchgl %eax, atfork_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L60
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	atfork_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 138 "register-atfork.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L60
	.size	__run_fork_handlers, .-__run_fork_handlers
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_free_mem__, @object
	.size	__elf_set___libc_subfreeres_element_free_mem__, 8
__elf_set___libc_subfreeres_element_free_mem__:
	.quad	free_mem
	.local	atfork_lock
	.comm	atfork_lock,4,4
	.local	fork_handler_init
	.comm	fork_handler_init,1,1
	.local	fork_handlers
	.comm	fork_handlers,1560,32
	.hidden	__libc_dynarray_at_failure
	.hidden	__libc_dynarray_emplace_enlarge
	.hidden	__lll_lock_wait_private
