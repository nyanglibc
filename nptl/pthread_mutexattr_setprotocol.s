	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_setprotocol
	.type	pthread_mutexattr_setprotocol, @function
pthread_mutexattr_setprotocol:
	cmpl	$2, %esi
	ja	.L3
	movl	(%rdi), %eax
	sall	$28, %esi
	andl	$-805306369, %eax
	orl	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, (%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_mutexattr_setprotocol, .-pthread_mutexattr_setprotocol
