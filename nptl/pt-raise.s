	.text
	.p2align 4,,15
	.globl	raise
	.type	raise, @function
raise:
	pushq	%rbx
	movq	$-1, %r8
	movabsq	$-6442450945, %rax
	movl	%edi, %ebx
	movl	$8, %r10d
	xorl	%edi, %edi
	subq	$136, %rsp
	leaq	-120(%rsp), %r9
	movq	%rax, 8(%rsp)
	movq	%r8, 16(%rsp)
	movq	%r8, 24(%rsp)
	movq	%r8, 32(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r8, 40(%rsp)
	movq	%r8, 48(%rsp)
	movq	%r9, %rdx
	movq	%r8, 56(%rsp)
	movq	%r8, 64(%rsp)
	movl	$14, %eax
	movq	%r8, 72(%rsp)
	movq	%r8, 80(%rsp)
	movq	%r8, 88(%rsp)
	movq	%r8, 96(%rsp)
	movq	%r8, 104(%rsp)
	movq	%r8, 112(%rsp)
	movq	%r8, 120(%rsp)
	movq	%r8, 128(%rsp)
#APP
# 81 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	$39, %ecx
	movl	%ecx, %eax
#APP
# 42 "../sysdeps/unix/sysv/linux/raise.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	%rax, %rdi
	movl	$186, %eax
#APP
# 43 "../sysdeps/unix/sysv/linux/raise.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %esi
	movl	%ebx, %edx
	movl	$234, %eax
#APP
# 45 "../sysdeps/unix/sysv/linux/raise.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L2
	movl	%eax, %r8d
.L3:
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%r9, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	addq	$136, %rsp
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	jmp	.L3
	.size	raise, .-raise
	.weak	gsignal
	.set	gsignal,raise
