	.text
	.p2align 4,,15
	.globl	__pthread_mutex_destroy
	.type	__pthread_mutex_destroy, @function
__pthread_mutex_destroy:
	movl	16(%rdi), %eax
	testb	$16, %al
	jne	.L2
	movl	12(%rdi), %edx
	movl	$16, %eax
	testl	%edx, %edx
	jne	.L1
.L2:
	movl	$-1, 16(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__pthread_mutex_destroy, .-__pthread_mutex_destroy
	.weak	pthread_mutex_destroy
	.set	pthread_mutex_destroy,__pthread_mutex_destroy
