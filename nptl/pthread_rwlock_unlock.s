	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_unlock
	.type	__pthread_rwlock_unlock, @function
__pthread_rwlock_unlock:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r8
	subq	$8, %rsp
	movl	24(%rdi), %edx
#APP
# 40 "pthread_rwlock_unlock.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, %edx
	je	.L58
	movl	28(%rdi), %ebx
	movl	$128, %eax
	testl	%ebx, %ebx
	cmovne	%eax, %ebx
	movl	(%rdi), %eax
.L18:
	leal	-8(%rax), %r9d
	movl	%r9d, %edx
	shrl	$3, %edx
	testl	%edx, %edx
	je	.L59
.L16:
	lock cmpxchgl	%r9d, (%r8)
	movl	%eax, %ebp
	jne	.L18
	testb	$1, %r9b
	jne	.L60
.L20:
	xorl	%ebp, %r9d
	andl	$4, %r9d
	je	.L14
	movl	%ebx, %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	xorb	$-127, %sil
	movq	%r8, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L55
.L14:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L59:
	movl	%r9d, %edx
	orl	$1, %edx
	testb	$2, %r9b
	cmovne	%edx, %r9d
	andl	$-5, %r9d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L60:
	leaq	8(%r8), %rdi
	movl	$1, %eax
	xchgl	(%rdi), %eax
	testb	$2, %al
	je	.L20
	movl	%ebx, %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	xorb	$-127, %sil
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L20
	cmpl	$-22, %eax
	je	.L20
	cmpl	$-14, %eax
	je	.L20
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L58:
	movl	28(%rdi), %ebp
	movl	$128, %eax
	leaq	12(%rdi), %rbx
	movl	$0, 24(%rdi)
	testl	%ebp, %ebp
	cmovne	%eax, %ebp
	xorl	%r9d, %r9d
	xchgl	(%rbx), %r9d
	movl	48(%rdi), %eax
	andl	$2, %r9d
	testl	%eax, %eax
	jne	.L4
.L6:
	movl	(%r8), %eax
.L5:
	movl	%eax, %edx
	movl	%eax, %ecx
	shrl	$3, %edx
	testl	%edx, %edx
	setne	%dl
	xorl	$2, %ecx
	movzbl	%dl, %edx
	xorl	%ecx, %edx
	lock cmpxchgl	%edx, (%r8)
	jne	.L5
	shrl	$3, %eax
	testl	%eax, %eax
	jne	.L61
.L11:
	testl	%r9d, %r9d
	je	.L14
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L14
.L55:
	cmpl	$-22, %eax
	je	.L14
	cmpl	$-14, %eax
	jne	.L10
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	4(%rdi), %eax
	leaq	4(%rdi), %rdx
.L56:
	testl	%eax, %eax
	je	.L6
	movl	%eax, %ecx
	orl	$-2147483648, %ecx
	lock cmpxchgl	%ecx, (%rdx)
	je	.L11
	jmp	.L56
	.p2align 4,,10
	.p2align 3
.L61:
	leaq	8(%r8), %rdi
	xorl	%eax, %eax
	xchgl	(%rdi), %eax
	testb	$2, %al
	je	.L11
	movl	%ebp, %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	xorb	$-127, %sil
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L11
	cmpl	$-22, %eax
	je	.L11
	cmpl	$-14, %eax
	je	.L11
.L10:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.size	__pthread_rwlock_unlock, .-__pthread_rwlock_unlock
	.weak	pthread_rwlock_unlock
	.set	pthread_rwlock_unlock,__pthread_rwlock_unlock
