	.text
	.p2align 4,,15
	.globl	pthread_attr_setguardsize
	.type	pthread_attr_setguardsize, @function
pthread_attr_setguardsize:
	movq	%rsi, 16(%rdi)
	xorl	%eax, %eax
	ret
	.size	pthread_attr_setguardsize, .-pthread_attr_setguardsize
