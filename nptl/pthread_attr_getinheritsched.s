	.text
	.p2align 4,,15
	.globl	__pthread_attr_getinheritsched
	.type	__pthread_attr_getinheritsched, @function
__pthread_attr_getinheritsched:
	movl	8(%rdi), %eax
	sarl	%eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getinheritsched, .-__pthread_attr_getinheritsched
	.globl	pthread_attr_getinheritsched
	.set	pthread_attr_getinheritsched,__pthread_attr_getinheritsched
