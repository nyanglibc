	.text
#APP
	.symver __new_sem_getvalue,sem_getvalue@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__new_sem_getvalue
	.type	__new_sem_getvalue, @function
__new_sem_getvalue:
	movq	(%rdi), %rax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__new_sem_getvalue, .-__new_sem_getvalue
