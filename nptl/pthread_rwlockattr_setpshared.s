	.text
	.p2align 4,,15
	.globl	pthread_rwlockattr_setpshared
	.type	pthread_rwlockattr_setpshared, @function
pthread_rwlockattr_setpshared:
	cmpl	$1, %esi
	ja	.L3
	movl	%esi, 4(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_rwlockattr_setpshared, .-pthread_rwlockattr_setpshared
