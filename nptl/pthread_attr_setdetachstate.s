	.text
	.p2align 4,,15
	.globl	__pthread_attr_setdetachstate
	.type	__pthread_attr_setdetachstate, @function
__pthread_attr_setdetachstate:
	cmpl	$1, %esi
	je	.L2
	testl	%esi, %esi
	jne	.L8
	andl	$-2, 8(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	orl	$1, 8(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$22, %eax
	ret
	.size	__pthread_attr_setdetachstate, .-__pthread_attr_setdetachstate
	.globl	pthread_attr_setdetachstate
	.set	pthread_attr_setdetachstate,__pthread_attr_setdetachstate
