	.text
	.p2align 4,,15
	.globl	pthread_yield
	.type	pthread_yield, @function
pthread_yield:
	jmp	sched_yield@PLT
	.size	pthread_yield, .-pthread_yield
