	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_getpshared
	.type	pthread_mutexattr_getpshared, @function
pthread_mutexattr_getpshared:
	movl	(%rdi), %eax
	shrl	$31, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_mutexattr_getpshared, .-pthread_mutexattr_getpshared
