	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/self/task/%u/comm"
	.text
	.p2align 4,,15
	.globl	pthread_setname_np
	.type	pthread_setname_np, @function
pthread_setname_np:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rdi
	movq	%rsi, %r12
	movl	$34, %ebx
	subq	$40, %rsp
	call	strlen@PLT
	cmpq	$15, %rax
	ja	.L1
	cmpq	%r13, %fs:16
	je	.L20
	movl	720(%r13), %edx
	movq	%rsp, %rbx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	movq	%rax, %rbp
	xorl	%eax, %eax
	call	sprintf@PLT
	xorl	%eax, %eax
	movl	$2, %esi
	movq	%rbx, %rdi
	call	__open64_nocancel@PLT
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L4
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L21:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	cmpl	$4, %ebx
	jne	.L8
.L4:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	__write_nocancel@PLT
	cmpq	$-1, %rax
	je	.L21
	testq	%rax, %rax
	js	.L22
	xorl	%ebx, %ebx
	cmpq	%rbp, %rax
	setne	%bl
	leal	(%rbx,%rbx,4), %ebx
.L8:
	movl	%r13d, %edi
	call	__close_nocancel@PLT
.L1:
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	movq	%r12, %rsi
	movl	$15, %edi
	call	prctl@PLT
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L1
.L18:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L22:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	jmp	.L8
	.size	pthread_setname_np, .-pthread_setname_np
