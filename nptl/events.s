	.text
	.p2align 4,,15
	.globl	__nptl_create_event
	.type	__nptl_create_event, @function
__nptl_create_event:
	rep ret
	.size	__nptl_create_event, .-__nptl_create_event
	.p2align 4,,15
	.globl	__nptl_death_event
	.type	__nptl_death_event, @function
__nptl_death_event:
	rep ret
	.size	__nptl_death_event, .-__nptl_death_event
