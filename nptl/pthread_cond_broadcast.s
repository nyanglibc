	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_cond_broadcast
	.type	__pthread_cond_broadcast, @function
__pthread_cond_broadcast:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$56, %rsp
	movl	36(%rdi), %ebp
	movl	%ebp, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	je	.L48
	movl	$128, %eax
	andl	$1, %ebp
	leaq	32(%rdi), %rbx
	cmovne	%eax, %ebp
	movl	32(%rdi), %eax
	movq	%rdi, %r9
.L4:
	testb	$3, %al
	jne	.L78
	movl	%eax, %edx
	orl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L4
.L5:
	movq	(%r9), %r14
	movl	%r14d, %r12d
	movq	%r14, %r8
	notl	%r12d
	shrq	%r8
	andl	$1, %r12d
	movl	%r12d, %r13d
	salq	$2, %r13
	leaq	(%r9,%r13), %rdx
	movl	24(%rdx), %eax
	testl	%eax, %eax
	je	.L16
	leaq	40(%r9,%r13), %rdi
	addl	%eax, %eax
	lock addl	%eax, (%rdi)
	xorl	%r10d, %r10d
	movl	$202, %eax
	movl	%ebp, %esi
	movl	$0, 24(%rdx)
	movl	$2147483647, %edx
	xorb	$-127, %sil
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L79
.L16:
	movl	32(%r9), %eax
	movl	%r14d, %edi
	andl	$1, %edi
	shrl	$2, %eax
	movl	%eax, %esi
	movl	%eax, 24(%rsp)
	movq	8(%r9), %rax
	movl	%edi, 28(%rsp)
	salq	$2, %rdi
	leaq	(%r9,%rdi), %r15
	movq	%rdi, 16(%rsp)
	shrq	%rax
	addl	24(%r15), %r8d
	movq	%rax, 8(%rsp)
	subl	%esi, %r8d
	cmpl	%eax, %r8d
	jne	.L80
.L17:
	movl	32(%r9), %eax
.L30:
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$-4, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L30
	andl	$3, %edx
	cmpl	$2, %edx
	je	.L81
.L48:
	addq	$56, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L80:
	leaq	40(%r9), %rax
	movq	%rax, 40(%rsp)
	addq	%r13, %rax
	movq	%rax, 32(%rsp)
	lock orl	$1, (%rax)
	leaq	16(%r9,%r13), %r8
	movl	(%r8), %eax
.L18:
	movl	%eax, %edx
	lock cmpxchgl	%eax, (%r8)
	jne	.L18
	shrl	%edx
	je	.L19
	movl	%ebp, %r14d
	movl	$202, %r13d
	xorb	$-128, %r14b
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	movl	(%r8), %eax
	shrl	%eax
	je	.L19
.L22:
	movl	(%r8), %eax
.L20:
	movl	%eax, %ecx
	orl	$1, %ecx
	lock cmpxchgl	%ecx, (%r8)
	jne	.L20
	movl	%ecx, %eax
	movl	%ecx, %edx
	shrl	%eax
	je	.L21
	xorl	%r10d, %r10d
	movl	%r14d, %esi
	movq	%r8, %rdi
	movl	%r13d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L21
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L13
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	je	.L13
	movl	(%r8), %eax
	shrl	%eax
	jne	.L22
	.p2align 4,,10
	.p2align 3
.L19:
	testl	%r12d, %r12d
	movl	$-1, %eax
	movq	8(%r9), %rdx
	cmove	%eax, %r12d
	movl	24(%rsp), %eax
	leal	(%r12,%rax,2), %eax
	addq	%rdx, %rax
	movq	%rax, 8(%r9)
	movq	32(%rsp), %rax
	movl	$0, (%rax)
	movq	(%r9), %rax
.L24:
	movq	%rax, %rcx
	movq	%rax, %rdx
	xorq	$1, %rcx
	lock cmpxchgq	%rcx, (%r9)
	jne	.L24
	movq	%rdx, %rax
	movl	24(%rsp), %edx
	addl	8(%rsp), %edx
	shrq	%rax
	subl	%edx, %eax
	movl	32(%r9), %edx
	leal	0(,%rax,4), %ecx
	andl	$3, %edx
	orl	%ecx, %edx
	movl	%edx, %esi
	xchgl	(%rbx), %esi
	xorl	%esi, %edx
	andl	$3, %edx
	je	.L25
	orl	$2, %ecx
	movl	%ecx, 32(%r9)
.L25:
	addl	24(%r15), %eax
	testl	%eax, %eax
	movl	%eax, 24(%r15)
	je	.L33
	movq	40(%rsp), %rdi
	addq	16(%rsp), %rdi
	addl	%eax, %eax
	lock addl	%eax, (%rdi)
	movl	$0, 24(%r15)
	movl	32(%r9), %eax
.L26:
	movl	%eax, %ecx
	movl	%eax, %edx
	andl	$-4, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	jne	.L26
	andl	$3, %edx
	xorb	$-127, %bpl
	cmpl	$2, %edx
	jne	.L27
	movl	28(%rsp), %r12d
	movl	$1, %r8d
	jmp	.L31
	.p2align 4,,10
	.p2align 3
.L81:
	xorb	$-127, %bpl
	xorl	%r8d, %r8d
.L31:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	%ebp, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L82
.L28:
	testb	%r8b, %r8b
	je	.L48
	leaq	40(%r9,%r12,4), %rdi
.L27:
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movl	%ebp, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L48
	cmpl	$-22, %eax
	je	.L48
	cmpl	$-14, %eax
	je	.L48
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L79:
	cmpl	$-22, %eax
	je	.L16
	cmpl	$-14, %eax
	je	.L16
.L13:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L33:
	movl	28(%rsp), %r12d
	jmp	.L17
.L82:
	cmpl	$-22, %eax
	je	.L28
	cmpl	$-14, %eax
	je	.L28
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L78:
	movl	%ebp, %r12d
	movl	$202, %r8d
	movl	$1, %r13d
	xorb	$-128, %r12b
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%eax, %edx
	movl	%eax, %ecx
	andl	$-4, %edx
	andl	$3, %ecx
	orl	$2, %edx
	cmpl	$2, %ecx
	je	.L9
	lock cmpxchgl	%edx, (%rbx)
	jne	.L7
	testb	$3, %al
	je	.L5
	andl	$-4, %eax
	orl	$2, %eax
	movl	%eax, %edx
.L9:
	xorl	%r10d, %r10d
	movl	%r12d, %esi
	movq	%rbx, %rdi
	movl	%r8d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L83
	movl	(%rbx), %eax
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L83:
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L13
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	je	.L13
	movl	(%rbx), %eax
	jmp	.L7
	.size	__pthread_cond_broadcast, .-__pthread_cond_broadcast
	.weak	pthread_cond_broadcast
	.set	pthread_cond_broadcast,__pthread_cond_broadcast
