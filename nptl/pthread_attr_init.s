	.text
	.p2align 4,,15
	.globl	__pthread_attr_init
	.hidden	__pthread_attr_init
	.type	__pthread_attr_init, @function
__pthread_attr_init:
	pxor	%xmm0, %xmm0
	pushq	%rbx
	movq	$0, 48(%rdi)
	movq	%rdi, %rbx
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	call	__getpagesize
	cltq
	movq	%rax, 16(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.size	__pthread_attr_init, .-__pthread_attr_init
	.weak	pthread_attr_init
	.set	pthread_attr_init,__pthread_attr_init
	.hidden	__attr_list_lock
	.globl	__attr_list_lock
	.bss
	.align 4
	.type	__attr_list_lock, @object
	.size	__attr_list_lock, 4
__attr_list_lock:
	.zero	4
	.hidden	__attr_list
	.comm	__attr_list,8,8
	.hidden	__getpagesize
