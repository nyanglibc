	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	pthread_barrier_destroy
	.type	pthread_barrier_destroy, @function
pthread_barrier_destroy:
	movl	$2147483647, %ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	8(%rdi)
	subl	%edx, %ecx
	movl	(%rdi), %edx
	subl	%edx, %ecx
	lock xaddl	%ecx, 16(%rdi)
	cmpl	%ecx, %edx
	jbe	.L11
	testl	%edx, %edx
	je	.L11
	pushq	%rbx
	movq	%rdi, %r8
	movl	$202, %r9d
	movl	$1, %ebx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L3:
	movl	(%r8), %edx
	testl	%edx, %edx
	je	.L15
.L5:
	movl	12(%r8), %esi
	xorl	%r10d, %r10d
	movq	%r8, %rdi
	movl	%r9d, %eax
	xorb	$-128, %sil
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L3
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L4
	movq	%rbx, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L3
.L4:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	xorl	%eax, %eax
	ret
	.size	pthread_barrier_destroy, .-pthread_barrier_destroy
