	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_getprotocol
	.type	pthread_mutexattr_getprotocol, @function
pthread_mutexattr_getprotocol:
	movl	(%rdi), %eax
	sarl	$28, %eax
	andl	$3, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_mutexattr_getprotocol, .-pthread_mutexattr_getprotocol
