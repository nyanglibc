	.text
	.p2align 4,,15
	.globl	__pthread_condattr_init
	.type	__pthread_condattr_init, @function
__pthread_condattr_init:
	movl	$0, (%rdi)
	xorl	%eax, %eax
	ret
	.size	__pthread_condattr_init, .-__pthread_condattr_init
	.globl	pthread_condattr_init
	.set	pthread_condattr_init,__pthread_condattr_init
