	.text
	.p2align 4,,15
	.globl	__pthread_getattr_default_np
	.type	__pthread_getattr_default_np, @function
__pthread_getattr_default_np:
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	$1, %edx
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L6
.L2:
	leaq	__default_pthread_attr(%rip), %rsi
	movq	%rbx, %rdi
	call	__pthread_attr_copy@PLT
	movl	%eax, %r8d
	xorl	%eax, %eax
#APP
# 26 "pthread_getattr_default_np.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L7
	movl	%r8d, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 26 "pthread_getattr_default_np.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%r8d, %eax
	popq	%rbx
	ret
	.size	__pthread_getattr_default_np, .-__pthread_getattr_default_np
	.weak	pthread_getattr_default_np
	.set	pthread_getattr_default_np,__pthread_getattr_default_np
