	.text
	.p2align 4,,15
	.globl	__pthread_sigmask
	.hidden	__pthread_sigmask
	.type	__pthread_sigmask, @function
__pthread_sigmask:
	movq	%rsi, %rax
	subq	$16, %rsp
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rcx
	movabsq	$6442450944, %rsi
	testq	%rsi, %rcx
	jne	.L3
	movq	%rax, %rsi
.L2:
	movl	$8, %r10d
	movl	$14, %eax
#APP
# 41 "pthread_sigmask.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, %edx
	negl	%edx
	cmpl	$-4096, %eax
	movl	$0, %eax
	cmova	%edx, %eax
	addq	$16, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movdqu	(%rax), %xmm0
	leaq	-120(%rsp), %rsi
	movaps	%xmm0, -120(%rsp)
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, -104(%rsp)
	movdqu	32(%rax), %xmm0
	movaps	%xmm0, -88(%rsp)
	movdqu	48(%rax), %xmm0
	movaps	%xmm0, -72(%rsp)
	movdqu	64(%rax), %xmm0
	movaps	%xmm0, -56(%rsp)
	movdqu	80(%rax), %xmm0
	movaps	%xmm0, -40(%rsp)
	movdqu	96(%rax), %xmm0
	movaps	%xmm0, -24(%rsp)
	movdqu	112(%rax), %xmm0
	movabsq	$-6442450945, %rax
	andq	%rax, %rcx
	movaps	%xmm0, -8(%rsp)
	movq	%rcx, -120(%rsp)
	jmp	.L2
	.size	__pthread_sigmask, .-__pthread_sigmask
	.weak	pthread_sigmask
	.set	pthread_sigmask,__pthread_sigmask
