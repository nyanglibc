	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	__futex_abstimed_wait_common64.part.0, @function
__futex_abstimed_wait_common64.part.0:
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movl	%r8d, %esi
	movq	%rcx, %r10
	subq	$40, %rsp
	cmpl	$1, %edx
	sbbl	%eax, %eax
	andl	$256, %eax
	addl	$137, %eax
	xorl	%eax, %esi
	testb	%r9b, %r9b
	je	.L3
#APP
# 74 "../sysdeps/nptl/futex-internal.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
.L3:
	movl	$-1, %r9d
	xorl	%r8d, %r8d
	movl	%ebp, %edx
	movl	$202, %eax
#APP
# 78 "../sysdeps/nptl/futex-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L6:
	cmpl	$-22, %eax
	je	.L8
	jle	.L32
	cmpl	$-4, %eax
	je	.L8
	testl	%eax, %eax
	je	.L8
	cmpl	$-11, %eax
	je	.L8
.L7:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L32:
	cmpl	$-110, %eax
	je	.L8
	cmpl	$-75, %eax
	jne	.L7
.L8:
	addq	$40, %rsp
	negl	%eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rdi, 16(%rsp)
	movl	%esi, 8(%rsp)
	movq	%rcx, 24(%rsp)
	call	__pthread_enable_asynccancel
	movl	$-1, %r9d
	movl	%eax, %ebx
	xorl	%r8d, %r8d
	movq	24(%rsp), %r10
	movl	%ebp, %edx
	movl	8(%rsp), %esi
	movq	16(%rsp), %rdi
	movl	$202, %eax
#APP
# 74 "../sysdeps/nptl/futex-internal.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%ebx, %edi
	movq	%rax, 8(%rsp)
	call	__pthread_disable_asynccancel
	movq	8(%rsp), %rax
	jmp	.L6
	.size	__futex_abstimed_wait_common64.part.0, .-__futex_abstimed_wait_common64.part.0
	.p2align 4,,15
	.globl	__GI___futex_abstimed_wait64
	.hidden	__GI___futex_abstimed_wait64
	.type	__GI___futex_abstimed_wait64, @function
__GI___futex_abstimed_wait64:
	testq	%rcx, %rcx
	jne	.L41
.L34:
	cmpl	$1, %edx
	movl	$22, %eax
	jbe	.L42
	rep ret
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%r9d, %r9d
	jmp	__futex_abstimed_wait_common64.part.0
	.p2align 4,,10
	.p2align 3
.L41:
	cmpq	$0, (%rcx)
	jns	.L34
	movl	$110, %eax
	ret
	.size	__GI___futex_abstimed_wait64, .-__GI___futex_abstimed_wait64
	.globl	__futex_abstimed_wait64
	.set	__futex_abstimed_wait64,__GI___futex_abstimed_wait64
	.p2align 4,,15
	.globl	__GI___futex_abstimed_wait_cancelable64
	.hidden	__GI___futex_abstimed_wait_cancelable64
	.type	__GI___futex_abstimed_wait_cancelable64, @function
__GI___futex_abstimed_wait_cancelable64:
	testq	%rcx, %rcx
	jne	.L51
.L44:
	cmpl	$1, %edx
	movl	$22, %eax
	jbe	.L52
	rep ret
	.p2align 4,,10
	.p2align 3
.L52:
	movl	$1, %r9d
	jmp	__futex_abstimed_wait_common64.part.0
	.p2align 4,,10
	.p2align 3
.L51:
	cmpq	$0, (%rcx)
	jns	.L44
	movl	$110, %eax
	ret
	.size	__GI___futex_abstimed_wait_cancelable64, .-__GI___futex_abstimed_wait_cancelable64
	.globl	__futex_abstimed_wait_cancelable64
	.set	__futex_abstimed_wait_cancelable64,__GI___futex_abstimed_wait_cancelable64
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
