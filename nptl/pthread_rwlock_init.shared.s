	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_init
	.type	__pthread_rwlock_init, @function
__pthread_rwlock_init:
	pxor	%xmm0, %xmm0
	leaq	default_rwlockattr(%rip), %rax
	testq	%rsi, %rsi
	movq	$0, 48(%rdi)
	cmove	%rax, %rsi
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, (%rdi)
	movups	%xmm0, 32(%rdi)
	movl	(%rsi), %eax
	movl	4(%rsi), %edx
	movl	%eax, 48(%rdi)
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	movl	%eax, 28(%rdi)
	xorl	%eax, %eax
	ret
	.size	__pthread_rwlock_init, .-__pthread_rwlock_init
	.globl	pthread_rwlock_init
	.set	pthread_rwlock_init,__pthread_rwlock_init
	.section	.rodata.cst8,"aM",@progbits,8
	.align 8
	.type	default_rwlockattr, @object
	.size	default_rwlockattr, 8
default_rwlockattr:
	.zero	8
