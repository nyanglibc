	.text
#APP
	.globl __pthread_mutex_lock
	.globl __pthread_mutex_trylock
	.globl __pthread_mutex_unlock
	.globl __pthread_once
	.globl __pthread_cancel
	.globl __pthread_key_create
	.globl __pthread_key_delete
	.globl __pthread_setspecific
	.globl __pthread_getspecific
#NO_APP
	.p2align 4,,15
	.type	free_stacks, @function
free_stacks:
	pushq	%r13
	pushq	%r12
	leaq	stack_cache(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	subq	$8, %rsp
	movq	8+stack_cache(%rip), %rbx
	cmpq	%r12, %rbx
	movq	8(%rbx), %rbp
	jne	.L4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	%r12, %rbp
	movq	8(%rbp), %rax
	movq	%rbp, %rbx
	je	.L1
	movq	%rax, %rbp
.L4:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L6
	movq	%rbx, in_flight_stack(%rip)
	movq	8(%rbx), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	-704(%rbx), %rdi
	xorl	%esi, %esi
	movq	984(%rbx), %rax
	movq	$0, in_flight_stack(%rip)
	subq	%rax, stack_cache_actsize(%rip)
	call	_dl_deallocate_tls@PLT
	movq	984(%rbx), %rsi
	movq	976(%rbx), %rdi
	call	__munmap@PLT
	testl	%eax, %eax
	jne	.L11
	cmpq	%r13, stack_cache_actsize(%rip)
	ja	.L6
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L11:
	call	abort@PLT
	.size	free_stacks, .-free_stacks
	.p2align 4,,15
	.type	change_stack_perm, @function
change_stack_perm:
	subq	$8, %rsp
	movq	1696(%rdi), %rax
	movq	1688(%rdi), %rsi
	movl	$7, %edx
	subq	%rax, %rsi
	addq	1680(%rdi), %rax
	movq	%rax, %rdi
	call	__mprotect@PLT
	testl	%eax, %eax
	je	.L12
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L12:
	addq	$8, %rsp
	ret
	.size	change_stack_perm, .-change_stack_perm
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/createthread.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"*stopped_start"
	.text
	.p2align 4,,15
	.type	create_thread, @function
create_thread:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movq	%rcx, %rsi
	subq	$16, %rsp
	testq	%rbp, %rbp
	je	.L23
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.L21
	cmpq	$0, (%rax)
	movl	$1, %r13d
	je	.L21
.L22:
	movl	$1, %eax
	movb	$1, (%r12)
	movb	%al, 1555(%rbx)
	cmpb	$0, (%r12)
	je	.L25
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	1560(%rbx), %rdi
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L25
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private@PLT
	movq	8(%rsp), %rsi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L21:
	testb	$2, 8(%rbp)
	jne	.L50
.L23:
	movzbl	(%r12), %eax
	xorl	%r13d, %r13d
	movb	%al, 1555(%rbx)
	cmpb	$0, (%r12)
	jne	.L51
.L25:
	leaq	720(%rbx), %r8
	subq	$8, %rsp
	leaq	start_thread(%rip), %rdi
	movq	%rbx, %rcx
	movl	$4001536, %edx
	xorl	%eax, %eax
	pushq	%r8
	movq	%rbx, %r9
	call	__clone@PLT
	cmpl	$-1, %eax
	popq	%rdx
	popq	%rcx
	je	.L52
	testq	%rbp, %rbp
	movb	$1, (%r14)
	je	.L33
	testl	%r13d, %r13d
	je	.L30
	cmpb	$0, (%r12)
	je	.L53
	movq	40(%rbp), %rax
	movl	720(%rbx), %edi
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	movl	$203, %eax
#APP
# 122 "../sysdeps/unix/sysv/linux/createthread.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L49
.L30:
	testb	$2, 8(%rbp)
	je	.L33
	cmpb	$0, (%r12)
	je	.L54
	movl	1596(%rbx), %esi
	movl	720(%rbx), %edi
	leaq	1592(%rbx), %rdx
	movl	$144, %eax
#APP
# 144 "../sysdeps/unix/sysv/linux/createthread.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L49
.L33:
	xorl	%eax, %eax
.L18:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L49:
.L32:
	movl	%eax, %ebp
	call	__getpid@PLT
	movl	720(%rbx), %esi
	movl	%eax, %edi
	movl	$32, %edx
	movl	$234, %eax
#APP
# 133 "../sysdeps/unix/sysv/linux/createthread.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%ebp, %eax
	negl	%eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L52:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%r13d, %r13d
	jmp	.L22
.L54:
	leaq	__PRETTY_FUNCTION__.13456(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$142, %edx
	call	__assert_fail@PLT
.L53:
	leaq	__PRETTY_FUNCTION__.13456(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$120, %edx
	call	__assert_fail@PLT
	.size	create_thread, .-create_thread
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	setxid_mark_thread.isra.0, @function
setxid_mark_thread.isra.0:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r8
	pushq	%rbx
	cmpl	$-1, 1564(%rdi)
	je	.L71
.L56:
	movl	$0, 1564(%r8)
.L63:
	movl	776(%r8), %eax
	testb	$16, %al
	jne	.L72
	movl	%eax, %edx
	orl	$64, %edx
	lock cmpxchgl	%edx, 776(%r8)
	jne	.L63
.L55:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	1564(%rdi), %rbx
	movl	$-2, %r9d
	movl	$-1, %eax
	lock cmpxchgl	%r9d, (%rbx)
	jne	.L56
	movl	$202, %ebp
	movl	$1, %r12d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$-2, 1564(%r8)
	jne	.L56
.L59:
	xorl	%r10d, %r10d
	movl	%r9d, %edx
	movl	$128, %esi
	movq	%rbx, %rdi
	movl	%ebp, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L57
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L58
	movq	%r12, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L57
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L72:
	testb	$64, %al
	jne	.L55
	movl	$1, 1564(%r8)
	leaq	1564(%r8), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L55
	cmpl	$-22, %eax
	je	.L55
	cmpl	$-14, %eax
	je	.L55
.L58:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
	.size	setxid_mark_thread.isra.0, .-setxid_mark_thread.isra.0
	.p2align 4,,15
	.type	setxid_unmark_thread.isra.2, @function
setxid_unmark_thread.isra.2:
.L75:
	movl	776(%rdi), %eax
	testb	$64, %al
	je	.L73
	movl	%eax, %edx
	andl	$-65, %edx
	lock cmpxchgl	%edx, 776(%rdi)
	jne	.L75
	movl	$1, 1564(%rdi)
	xorl	%r10d, %r10d
	addq	$1564, %rdi
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L86
.L73:
	rep ret
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	$-22, %eax
	je	.L73
	cmpl	$-14, %eax
	je	.L73
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	setxid_unmark_thread.isra.2, .-setxid_unmark_thread.isra.2
	.p2align 4,,15
	.type	setxid_signal_thread.part.4, @function
setxid_signal_thread.part.4:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__getpid@PLT
	movl	720(%rbp), %esi
	movl	%eax, %edi
	movl	$33, %edx
	movl	$234, %eax
#APP
# 1031 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	xorl	%edx, %edx
	cmpl	$-4096, %eax
	ja	.L87
#APP
# 1036 "allocatestack.c" 1
	lock;incl 32(%rbx)
# 0 "" 2
#NO_APP
	movl	$1, %edx
.L87:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	setxid_signal_thread.part.4, .-setxid_signal_thread.part.4
	.p2align 4,,15
	.type	__nptl_deallocate_tsd.part.5, @function
__nptl_deallocate_tsd.part.5:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	__pthread_keys(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	$4, 8(%rsp)
.L100:
#APP
# 269 "pthread_create.c" 1
	movb $0,%fs:1552
# 0 "" 2
#NO_APP
	movl	$32, %r12d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L98:
#APP
# 275 "pthread_create.c" 1
	movq %fs:1296(,%rbp,8),%rbx
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	je	.L93
	movq	%rbp, %r15
	movq	%r12, %rax
	addq	$8, %rbx
	salq	$9, %r15
	salq	$4, %rax
	addq	%r13, %r15
	leaq	(%rax,%r13), %r14
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$16, %r15
	addq	$16, %rbx
	cmpq	%r14, %r15
	je	.L93
.L97:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	(%r15), %rax
	cmpq	%rax, -8(%rbx)
	movq	$0, (%rbx)
	jne	.L95
	movq	8(%r15), %rdx
	testq	%rdx, %rdx
	je	.L95
	addq	$16, %r15
	addq	$16, %rbx
	call	*%rdx
	cmpq	%r14, %r15
	jne	.L97
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$1, %rbp
	addq	$32, %r12
	cmpq	$32, %rbp
	jne	.L98
	xorl	%eax, %eax
#APP
# 311 "pthread_create.c" 1
	movb %fs:1552,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	je	.L99
	subq	$1, 8(%rsp)
	movq	8(%rsp), %rsi
	jne	.L100
	movq	%fs:16, %rax
	leaq	792(%rax), %rdi
	movq	$0, 784(%rax)
	movq	$0, 1288(%rax)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	1296(%rax), %ecx
	movq	%rsi, %rax
	shrl	$3, %ecx
	rep stosq
.L99:
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L102:
#APP
# 328 "pthread_create.c" 1
	movq %fs:1296(,%rbx,8),%rdi
# 0 "" 2
#NO_APP
	testq	%rdi, %rdi
	je	.L101
	call	free@PLT
#APP
# 334 "pthread_create.c" 1
	movq $0,%fs:1296(,%rbx,8)
# 0 "" 2
#NO_APP
.L101:
	addq	$1, %rbx
	cmpq	$32, %rbx
	jne	.L102
#APP
# 338 "pthread_create.c" 1
	movb $0,%fs:1552
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__nptl_deallocate_tsd.part.5, .-__nptl_deallocate_tsd.part.5
	.p2align 4,,15
	.globl	__nptl_stacks_freeres
	.type	__nptl_stacks_freeres, @function
__nptl_stacks_freeres:
	xorl	%edi, %edi
	jmp	free_stacks
	.size	__nptl_stacks_freeres, .-__nptl_stacks_freeres
	.p2align 4,,15
	.globl	__deallocate_stack
	.type	__deallocate_stack, @function
__deallocate_stack:
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L127
.L122:
	leaq	704(%rbx), %rax
	movq	%rax, in_flight_stack(%rip)
	movq	712(%rbx), %rcx
	movq	704(%rbx), %rdx
	movq	%rcx, 8(%rdx)
	movq	712(%rbx), %rcx
	movq	%rdx, (%rcx)
	cmpb	$0, 1554(%rbx)
	movq	$0, in_flight_stack(%rip)
	jne	.L123
	movq	%rax, %rdx
	orq	$1, %rdx
	movq	%rdx, in_flight_stack(%rip)
	movq	stack_cache(%rip), %rdx
	leaq	stack_cache(%rip), %rsi
	movq	%rsi, 712(%rbx)
	movq	%rdx, 704(%rbx)
	movq	%rax, 8(%rdx)
	movq	%rax, stack_cache(%rip)
	movq	stack_cache_actsize(%rip), %rax
	addq	1688(%rbx), %rax
	movq	$0, in_flight_stack(%rip)
	cmpq	$41943040, %rax
	movq	%rax, stack_cache_actsize(%rip)
	ja	.L128
.L124:
	xorl	%eax, %eax
#APP
# 806 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L129
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L129:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 806 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_deallocate_tls@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$41943040, %edi
	call	free_stacks
	jmp	.L124
	.size	__deallocate_stack, .-__deallocate_stack
	.p2align 4,,15
	.globl	__make_stacks_executable
	.type	__make_stacks_executable, @function
__make_stacks_executable:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	call	_dl_make_stack_executable@PLT
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L144
.L130:
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L145
.L132:
	movq	_dl_stack_used(%rip), %rbx
	leaq	_dl_stack_used(%rip), %r12
	cmpq	%r12, %rbx
	jne	.L135
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%rbx), %rbx
	cmpq	%r12, %rbx
	je	.L133
.L135:
	leaq	-704(%rbx), %rdi
	call	change_stack_perm
	testl	%eax, %eax
	je	.L146
.L139:
	movl	%eax, %ebp
.L134:
	xorl	%eax, %eax
#APP
# 851 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L130
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 851 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L133:
	movq	stack_cache(%rip), %rbx
	leaq	stack_cache(%rip), %r12
	cmpq	%r12, %rbx
	jne	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rbx), %rbx
	cmpq	%r12, %rbx
	je	.L134
.L136:
	leaq	-704(%rbx), %rdi
	call	change_stack_perm
	testl	%eax, %eax
	je	.L147
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L132
	.size	__make_stacks_executable, .-__make_stacks_executable
	.section	.rodata.str1.1
.LC3:
	.string	"allocatestack.c"
.LC4:
	.string	"l->next->prev == elem"
	.text
	.p2align 4,,15
	.globl	__reclaim_stacks
	.type	__reclaim_stacks, @function
__reclaim_stacks:
	movq	in_flight_stack(%rip), %rax
	movq	%fs:16, %r10
	testq	%rax, %rax
	je	.L172
	movq	%rax, %rdx
	andq	$-2, %rdx
	testb	$1, %al
	je	.L151
	movq	_dl_stack_used(%rip), %rsi
	leaq	_dl_stack_used(%rip), %r9
	movq	%r9, %rax
	movq	8(%rsi), %rcx
	cmpq	%r9, %rcx
	je	.L173
.L152:
	cmpq	%rcx, %rdx
	jne	.L174
	movq	%rsi, (%rdx)
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	movq	_dl_stack_used(%rip), %rsi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rdx), %rax
	movq	8(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	8(%rdx), %rdx
	movq	%rax, (%rdx)
.L172:
	movq	_dl_stack_used(%rip), %rsi
	leaq	_dl_stack_used(%rip), %r9
.L150:
	xorl	%eax, %eax
	cmpq	%r9, %rsi
	je	.L155
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	-704(%rsi), %rdx
	cmpq	%rdx, %r10
	je	.L156
	movq	984(%rsi), %rdx
	addq	%rdx, stack_cache_actsize(%rip)
	cmpb	$0, 848(%rsi)
	movl	$0, 16(%rsi)
	jne	.L175
.L156:
	movq	(%rsi), %rsi
	cmpq	%r9, %rsi
	jne	.L154
	movq	(%r9), %rax
	cmpq	%r9, %rax
	je	.L155
	leaq	stack_cache(%rip), %rsi
	movq	stack_cache(%rip), %rdx
	movq	%rsi, 8(%rax)
	movq	8(%r9), %rax
	movq	%rdx, (%rax)
	movq	stack_cache(%rip), %rdx
	movq	%rax, 8(%rdx)
	movq	(%r9), %rax
	movq	%rax, stack_cache(%rip)
.L155:
	leaq	704(%r10), %rdx
	movq	%rdx, in_flight_stack(%rip)
	movq	712(%r10), %rcx
	movq	704(%r10), %rax
	movq	%rcx, 8(%rax)
	movq	712(%r10), %rcx
	movq	%rax, (%rcx)
	leaq	_dl_stack_user(%rip), %rcx
	movq	$0, in_flight_stack(%rip)
	movq	%r9, 8+_dl_stack_used(%rip)
	movq	%r9, _dl_stack_used(%rip)
	xorl	%eax, %eax
	movq	%rcx, 8+_dl_stack_user(%rip)
	movq	%rcx, _dl_stack_user(%rip)
#APP
# 950 "allocatestack.c" 1
	movb %fs:1554,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L176
	movq	%r9, 704(%r10)
	movq	%r9, 712(%r10)
	movq	%rdx, 8+_dl_stack_used(%rip)
	movq	%rdx, _dl_stack_used(%rip)
.L161:
	movl	$1, __nptl_nthreads(%rip)
	movq	$0, in_flight_stack(%rip)
	movl	$0, _dl_stack_cache_lock(%rip)
	movl	$0, __default_pthread_attr_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	88(%rsi), %rdi
	movl	%esi, %ecx
	leaq	600(%rsi), %rdx
	leaq	848(%rsi), %r8
	movq	$0, 80(%rsi)
	andq	$-8, %rdi
	movq	$0, -16(%rdx)
	subl	%edi, %ecx
	addl	$592, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	$0, 848(%rsi)
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L157
	leaq	8(%rcx), %rdi
	movq	$0, (%rcx)
	movq	$0, 504(%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$512, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	$1, 848(%rsi)
.L157:
	addq	$8, %rdx
	cmpq	%r8, %rdx
	jne	.L158
	jmp	.L156
.L173:
	movq	stack_cache(%rip), %rdi
	leaq	stack_cache(%rip), %rax
	movq	8(%rdi), %rcx
	cmpq	%rax, %rcx
	je	.L150
	movq	%rdi, %rsi
	jmp	.L152
.L176:
	movq	%rcx, 704(%r10)
	movq	%rcx, 712(%r10)
	movq	%rdx, 8+_dl_stack_user(%rip)
	movq	%rdx, _dl_stack_user(%rip)
	jmp	.L161
.L174:
	leaq	__PRETTY_FUNCTION__.13263(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	subq	$8, %rsp
	movl	$889, %edx
	call	__assert_fail@PLT
	.size	__reclaim_stacks, .-__reclaim_stacks
	.p2align 4,,15
	.globl	__nptl_setxid_error
	.type	__nptl_setxid_error, @function
__nptl_setxid_error:
.L180:
	movl	36(%rdi), %eax
	cmpl	%esi, %eax
	je	.L177
	cmpl	$-1, %eax
	jne	.L186
	lock cmpxchgl	%esi, 36(%rdi)
	jne	.L180
.L177:
	rep ret
.L186:
	subq	$24, %rsp
	movl	%esi, 12(%rsp)
	call	abort@PLT
	.size	__nptl_setxid_error, .-__nptl_setxid_error
	.p2align 4,,15
	.globl	__nptl_setxid
	.type	__nptl_setxid, @function
__nptl_setxid:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movl	$1, %edx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L244
.L188:
	movq	_dl_stack_used(%rip), %rbp
	leaq	_dl_stack_used(%rip), %r14
	movl	$0, 32(%rbx)
	movq	%rbx, __xidcmd(%rip)
	movl	$-1, 36(%rbx)
	movq	%fs:16, %r15
	cmpq	%r14, %rbp
	je	.L189
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L190
	call	setxid_mark_thread.isra.0
.L190:
	movq	0(%rbp), %rbp
	cmpq	%r14, %rbp
	jne	.L191
.L189:
	movq	_dl_stack_user(%rip), %rbp
	leaq	_dl_stack_user(%rip), %r13
	cmpq	%r13, %rbp
	je	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L193
	call	setxid_mark_thread.isra.0
.L193:
	movq	0(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.L194
.L192:
	leaq	32(%rbx), %r12
	movl	$202, %ebp
	.p2align 4,,10
	.p2align 3
.L211:
	movq	(%r14), %rdx
	cmpq	%r14, %rdx
	je	.L195
	xorl	%r9d, %r9d
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L196:
	movq	(%rdx), %rdx
	cmpq	%r14, %rdx
	je	.L245
.L198:
	leaq	-704(%rdx), %rsi
	cmpq	%rsi, %r15
	je	.L196
	testb	$64, 72(%rdx)
	je	.L196
	movq	%rbx, %rdi
	movq	%rdx, 8(%rsp)
	movl	%r9d, (%rsp)
	call	setxid_signal_thread.part.4
	movq	8(%rsp), %rdx
	movl	(%rsp), %r9d
	movq	(%rdx), %rdx
	addl	%eax, %r9d
	cmpq	%r14, %rdx
	jne	.L198
.L245:
	movq	0(%r13), %rdx
	cmpq	%r13, %rdx
	jne	.L206
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L204:
	movq	(%rdx), %rdx
	cmpq	%r13, %rdx
	je	.L203
.L206:
	leaq	-704(%rdx), %rsi
	cmpq	%rsi, %r15
	je	.L204
	testb	$64, 72(%rdx)
	je	.L204
	movq	%rbx, %rdi
	movl	%r9d, 8(%rsp)
	movq	%rdx, (%rsp)
	call	setxid_signal_thread.part.4
	movq	(%rsp), %rdx
	movl	8(%rsp), %r9d
	movq	(%rdx), %rdx
	addl	%eax, %r9d
	cmpq	%r13, %rdx
	jne	.L206
	.p2align 4,,10
	.p2align 3
.L203:
	movl	32(%rbx), %edx
	testl	%edx, %edx
	je	.L207
.L210:
	xorl	%r10d, %r10d
	movl	$128, %esi
	movq	%r12, %rdi
	movl	%ebp, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L203
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L209
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L203
.L209:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	testl	%r9d, %r9d
	jne	.L211
	movq	_dl_stack_used(%rip), %rbp
	cmpq	%r14, %rbp
	je	.L242
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L214
	call	setxid_unmark_thread.isra.2
.L214:
	movq	0(%rbp), %rbp
	cmpq	%r14, %rbp
	jne	.L212
.L242:
	movq	_dl_stack_user(%rip), %rbp
	jmp	.L243
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L215
	call	setxid_unmark_thread.isra.2
.L215:
	movq	0(%rbp), %rbp
.L243:
	cmpq	%r13, %rbp
	jne	.L216
.L202:
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rdx
	movl	(%rbx), %eax
#APP
# 1160 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	xorl	%esi, %esi
	cmpl	$-4096, %eax
	movl	%eax, %ebp
	ja	.L246
.L217:
	movq	%rbx, %rdi
	call	__nptl_setxid_error
	xorl	%eax, %eax
#APP
# 1171 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L247
.L187:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L195:
	movq	0(%r13), %rdx
	xorl	%r9d, %r9d
	cmpq	%r13, %rdx
	jne	.L206
	movl	32(%rbx), %edx
	testl	%edx, %edx
	jne	.L210
	jmp	.L202
.L244:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L188
.L246:
	movl	%eax, %esi
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	negl	%esi
	movl	%esi, %fs:(%rax)
	jmp	.L217
.L247:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 1171 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L187
	.size	__nptl_setxid, .-__nptl_setxid
	.p2align 4,,15
	.globl	__pthread_init_static_tls
	.type	__pthread_init_static_tls, @function
__pthread_init_static_tls:
	pushq	%r14
	pushq	%r13
	xorl	%eax, %eax
	pushq	%r12
	pushq	%rbp
	movl	$1, %edx
	pushq	%rbx
	movq	%rdi, %rbx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L258
.L249:
	movq	_dl_stack_used(%rip), %rbp
	leaq	_dl_stack_used(%rip), %r13
	cmpq	%r13, %rbp
	je	.L250
	movq	$-704, %r14
	.p2align 4,,10
	.p2align 3
.L251:
	movq	%r14, %rdi
	subq	1112(%rbx), %rdi
	movq	1080(%rbx), %rdx
	movq	1088(%rbx), %r12
	movq	1072(%rbx), %rsi
	addq	%rbp, %rdi
	subq	%rdx, %r12
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	0(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.L251
.L250:
	movq	_dl_stack_user(%rip), %rbp
	leaq	_dl_stack_user(%rip), %r13
	cmpq	%r13, %rbp
	je	.L252
	movq	$-704, %r14
	.p2align 4,,10
	.p2align 3
.L253:
	movq	%r14, %rdi
	subq	1112(%rbx), %rdi
	movq	1080(%rbx), %rdx
	movq	1088(%rbx), %r12
	movq	1072(%rbx), %rsi
	addq	%rbp, %rdi
	subq	%rdx, %r12
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	0(%rbp), %rbp
	cmpq	%r13, %rbp
	jne	.L253
.L252:
	xorl	%eax, %eax
#APP
# 1206 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L259
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L258:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L249
	.p2align 4,,10
	.p2align 3
.L259:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 1206 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__pthread_init_static_tls, .-__pthread_init_static_tls
	.p2align 4,,15
	.globl	__find_in_stack_list
	.type	__find_in_stack_list, @function
__find_in_stack_list:
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L273
.L261:
	movq	_dl_stack_used(%rip), %rax
	leaq	_dl_stack_used(%rip), %rcx
	cmpq	%rcx, %rax
	jne	.L265
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L274:
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	je	.L262
.L265:
	leaq	-704(%rax), %rdx
	cmpq	%rdx, %rbx
	jne	.L274
	testq	%rbx, %rbx
	je	.L262
.L266:
	xorl	%eax, %eax
#APP
# 243 "pthread_create.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L275
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L262:
	movq	_dl_stack_user(%rip), %rax
	leaq	_dl_stack_user(%rip), %rcx
	cmpq	%rcx, %rax
	je	.L270
	.p2align 4,,10
	.p2align 3
.L268:
	leaq	-704(%rax), %rdx
	cmpq	%rdx, %rbx
	je	.L266
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L268
.L270:
	xorl	%ebx, %ebx
	jmp	.L266
	.p2align 4,,10
	.p2align 3
.L275:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 243 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	%rbx, %rax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L273:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L261
	.size	__find_in_stack_list, .-__find_in_stack_list
	.p2align 4,,15
	.globl	__nptl_deallocate_tsd
	.type	__nptl_deallocate_tsd, @function
__nptl_deallocate_tsd:
	xorl	%eax, %eax
#APP
# 258 "pthread_create.c" 1
	movb %fs:1552,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	je	.L276
	jmp	__nptl_deallocate_tsd.part.5
	.p2align 4,,10
	.p2align 3
.L276:
	rep ret
	.size	__nptl_deallocate_tsd, .-__nptl_deallocate_tsd
	.p2align 4,,15
	.globl	__free_tcb
	.type	__free_tcb, @function
__free_tcb:
#APP
# 349 "pthread_create.c" 1
	lock;btsl $5, 776(%rdi); setc %al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L278
	pushq	%rbx
	movq	%rdi, %rbx
	movq	1712(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L287
.L280:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__deallocate_stack
	.p2align 4,,10
	.p2align 3
.L278:
	rep ret
	.p2align 4,,10
	.p2align 3
.L287:
	movq	$0, 1712(%rbx)
	call	free@PLT
	jmp	.L280
	.size	__free_tcb, .-__free_tcb
	.section	.rodata.str1.1
.LC5:
	.string	"freesize < size"
	.text
	.p2align 4,,15
	.type	start_thread, @function
start_thread:
	pushq	%r12
	pushq	%rbp
	leaq	1720(%rdi), %rdx
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	__resp@gottpoff(%rip), %rax
	movq	%rdx, %fs:(%rax)
	call	__ctype_init@PLT
	movq	%rbx, 8(%rsp)
	leaq	736(%rbx), %rdi
	movl	$24, %esi
	movl	$273, %eax
#APP
# 395 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	16(%rsp), %rdi
	call	_setjmp@PLT
	movq	8(%rsp), %r8
	movl	%eax, %ebx
	movq	$0, 88(%rsp)
	movq	$0, 96(%rsp)
	movl	$8, %r10d
	xorl	%edx, %edx
	movl	$2, %edi
	movl	$14, %eax
	leaq	2288(%r8), %rsi
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
# 432 "pthread_create.c" 1
	xchgl %edx, 1564(%r8)
# 0 "" 2
#NO_APP
	cmpl	$-2, %edx
	je	.L330
.L292:
	testl	%ebx, %ebx
	jne	.L293
	leaq	16(%rsp), %rax
#APP
# 438 "pthread_create.c" 1
	movq %rax,%fs:768
# 0 "" 2
#NO_APP
	movq	8(%rsp), %rax
	cmpb	$0, 1555(%rax)
	jne	.L294
.L301:
	movq	8(%rsp), %rcx
	cmpb	$0, 2416(%rcx)
	movq	1600(%rcx), %rax
	movq	1608(%rcx), %rdi
	jne	.L295
	call	*%rax
.L302:
#APP
# 474 "pthread_create.c" 1
	movq %rax,%fs:1584
# 0 "" 2
#NO_APP
.L293:
	cmpq	$0, __call_tls_dtors@GOTPCREL(%rip)
	je	.L306
	call	__call_tls_dtors@PLT
.L306:
	xorl	%eax, %eax
#APP
# 258 "pthread_create.c" 1
	movb %fs:1552,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L331
.L305:
	call	__libc_thread_freeres@PLT
#APP
# 492 "pthread_create.c" 1
	lock;decl __nptl_nthreads(%rip); sete %al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L332
	movq	8(%rsp), %rax
	cmpb	$0, 1553(%rax)
	jne	.L309
.L312:
	movq	8(%rsp), %rax
#APP
# 528 "pthread_create.c" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
#NO_APP
	cmpb	$0, 1554(%rax)
	jne	.L310
	movq	8(%rsp), %rbp
	call	__getpagesize@PLT
	movq	%rsp, %rdx
	negl	%eax
	movq	1680(%rbp), %rbx
	cltq
	subq	%rbx, %rdx
	andq	%rdx, %rax
	cmpq	%rax, 1688(%rbp)
	jbe	.L333
	cmpq	$16384, %rax
	jbe	.L310
	leaq	-16384(%rax), %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	call	__madvise@PLT
.L310:
	movq	8(%rsp), %rax
	testb	$64, 776(%rax)
	jne	.L334
.L316:
	movq	8(%rsp), %rax
	cmpq	%rax, 1576(%rax)
	je	.L320
.L322:
	movl	$60, %edx
	.p2align 4,,10
	.p2align 3
.L321:
	xorl	%edi, %edi
	movl	%edx, %eax
#APP
# 35 "../sysdeps/unix/sysv/linux/exit-thread.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L321
.L295:
	call	*%rax
	cltq
	jmp	.L302
.L294:
	call	__pthread_enable_asynccancel@PLT
	movl	%eax, %r12d
	movq	8(%rsp), %rax
	movl	$1, %edx
	leaq	1560(%rax), %rbp
	movl	%ebx, %eax
	lock cmpxchgl	%edx, 0(%rbp)
	jne	.L297
.L300:
	xorl	%eax, %eax
	movq	8(%rsp), %rcx
#APP
# 454 "pthread_create.c" 1
	xchgl %eax, 1560(%rcx)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L299
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 454 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L299:
	movl	%r12d, %edi
	call	__pthread_disable_asynccancel@PLT
	jmp	.L301
.L330:
	movq	8(%rsp), %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	1564(%rax), %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L292
	cmpl	$-22, %eax
	je	.L292
	cmpl	$-14, %eax
	je	.L292
.L319:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
.L331:
	call	__nptl_deallocate_tsd.part.5
	jmp	.L305
.L332:
	xorl	%edi, %edi
	call	exit@PLT
.L333:
	leaq	__PRETTY_FUNCTION__.13112(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$379, %edx
	call	__assert_fail@PLT
.L297:
	movq	%rbp, %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L300
.L309:
	movq	8(%rsp), %rcx
	movl	__nptl_threads_events(%rip), %eax
	orl	1616(%rcx), %eax
	testb	$1, %ah
	je	.L312
	cmpq	$0, 1640(%rcx)
	je	.L335
.L313:
	call	__nptl_death_event@PLT
	jmp	.L312
.L320:
	movq	8(%rsp), %rdi
	call	__free_tcb
	jmp	.L322
.L334:
	leaq	1564(%rax), %rbx
	movl	$202, %r9d
	movl	$1, %r8d
	jmp	.L315
.L318:
	movq	8(%rsp), %rax
	testb	$64, 776(%rax)
	je	.L336
.L315:
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$128, %esi
	movq	%rbx, %rdi
	movl	%r9d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L318
	addl	$11, %eax
	cmpl	$11, %eax
	ja	.L319
	movl	%eax, %ecx
	movq	%r8, %rsi
	salq	%cl, %rsi
	movq	%rsi, %rax
	testl	$2177, %eax
	je	.L319
	jmp	.L318
.L335:
	movq	%rcx, %rax
	movl	$9, 1624(%rcx)
	movq	%rcx, 1632(%rax)
.L314:
	movq	__nptl_last_event(%rip), %rax
	movq	8(%rsp), %rcx
	movq	%rax, 1640(%rcx)
	lock cmpxchgq	%rcx, __nptl_last_event(%rip)
	je	.L313
	jmp	.L314
.L336:
	movl	$0, 1564(%rax)
	jmp	.L316
	.size	start_thread, .-start_thread
	.section	.rodata.str1.1
.LC6:
	.string	"powerof2 (pagesize_m1 + 1)"
.LC7:
	.string	"size > adj + TLS_TCB_SIZE"
.LC8:
	.string	"errno == ENOMEM"
.LC9:
	.string	"size != 0"
.LC10:
	.string	"mem != NULL"
.LC11:
	.string	"pthread_create.c"
.LC12:
	.string	"stopped_start"
.LC13:
	.string	"pd->stopped_start"
	.text
	.p2align 4,,15
	.globl	__pthread_create_2_1
	.type	__pthread_create_2_1, @function
__pthread_create_2_1:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$280, %rsp
	cmpb	$0, __libc_single_threaded(%rip)
	movq	%rdi, (%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rcx, 16(%rsp)
	je	.L338
	movb	$0, __libc_single_threaded(%rip)
.L338:
	leaq	-1(%r13), %rax
	cmpq	$-3, %rax
	jbe	.L414
	leaq	80(%rsp), %rbp
	movq	%rbp, %rdi
	call	__pthread_getattr_default_np@PLT
	testl	%eax, %eax
	movl	%eax, %r8d
	jne	.L337
	movb	$1, 43(%rsp)
	jmp	.L339
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%r13, %rbp
	movb	$0, 43(%rsp)
.L339:
	call	__getpagesize@PLT
	leal	-1(%rax), %r9d
	movl	%eax, %r12d
	movslq	%r9d, %r9
	leaq	1(%r9), %r8
	movq	%r9, %r14
	andq	%r8, %r14
	jne	.L484
	movq	32(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L485
	testb	$8, 8(%rbp)
	jne	.L486
.L345:
	movq	__static_tls_align_m1(%rip), %rax
	notq	%rax
	andq	%rax, %rbx
	je	.L487
	movq	16(%rbp), %rdx
	negl	%r12d
	movslq	%r12d, %rax
	leaq	(%r9,%rdx), %r12
	andq	%rax, %r12
	cmpq	%r12, %rdx
	jbe	.L488
.L354:
	movl	$22, %r8d
.L347:
	cmpb	$0, 43(%rsp)
	je	.L337
	leaq	80(%rsp), %rdi
	movl	%r8d, (%rsp)
	call	__pthread_attr_destroy@PLT
	movl	(%rsp), %r8d
.L337:
	addq	$280, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L488:
	addq	%r12, %rbx
	jc	.L354
	movq	__static_tls_size(%rip), %rdx
	leaq	2048(%r9,%rdx), %rdx
	addq	%r12, %rdx
	andq	%rdx, %rax
	cmpq	%rax, %rbx
	jb	.L354
	movl	_dl_stack_flags(%rip), %r15d
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, _dl_stack_cache_lock(%rip)
	jne	.L489
.L357:
	leal	0(,%r15,4), %eax
	leaq	stack_cache(%rip), %rdx
	andl	$4, %eax
	movl	%eax, 48(%rsp)
	orl	$3, %eax
	movl	%eax, 44(%rsp)
	movq	stack_cache(%rip), %rax
	cmpq	%rdx, %rax
	je	.L358
	xorl	%r15d, %r15d
	jmp	.L361
	.p2align 4,,10
	.p2align 3
.L359:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L360
.L361:
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L359
	movq	984(%rax), %rcx
	cmpq	%rcx, %rbx
	ja	.L359
	cmpq	%rcx, %rbx
	leaq	-704(%rax), %rsi
	je	.L415
	testq	%r15, %r15
	je	.L416
	cmpq	1688(%r15), %rcx
	movq	(%rax), %rax
	cmovb	%rsi, %r15
	cmpq	%rdx, %rax
	jne	.L361
	.p2align 4,,10
	.p2align 3
.L360:
	testq	%r15, %r15
	je	.L358
	leaq	0(,%rbx,4), %rax
	cmpq	%rax, 1688(%r15)
	ja	.L358
	leaq	704(%r15), %rsi
	movl	$-1, 1564(%r15)
	movq	%rsi, 56(%rsp)
	movq	%rsi, in_flight_stack(%rip)
	movq	712(%r15), %rdx
	movq	704(%r15), %rax
	movq	%rdx, 8(%rax)
	movq	712(%r15), %rdx
	movq	%rax, (%rdx)
	movq	%rsi, %rax
	orq	$1, %rax
	movq	%rax, in_flight_stack(%rip)
	movq	_dl_stack_used(%rip), %rax
	leaq	_dl_stack_used(%rip), %rdi
	movq	%rdi, 712(%r15)
	movq	%rax, 704(%r15)
	movq	%rsi, 8(%rax)
	movq	%rsi, _dl_stack_used(%rip)
	movq	$0, in_flight_stack(%rip)
	movq	1688(%r15), %rdx
	xorl	%eax, %eax
	subq	%rdx, stack_cache_actsize(%rip)
#APP
# 215 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L490
.L364:
	movq	8(%r15), %rcx
	pxor	%xmm0, %xmm0
	movq	1680(%r15), %rax
	movq	%rdx, 24(%rsp)
	movl	$0, 776(%r15)
	movq	$0, 760(%r15)
	cmpq	$0, -16(%rcx)
	movq	$0, 1640(%r15)
	movq	%rax, 32(%rsp)
	movups	%xmm0, 2424(%r15)
	je	.L418
	movq	%rbx, 48(%rsp)
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L366:
	addq	$1, %r14
	movq	%r14, %rax
	salq	$4, %rax
	movq	8(%rbx,%rax), %rdi
	call	free@PLT
	movq	-16(%rbx), %rax
	cmpq	%rax, %r14
	jb	.L366
	movq	%rbx, %rcx
	leaq	1(%rax), %rdx
	movq	48(%rsp), %rbx
	salq	$4, %rdx
.L365:
	movq	%rcx, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	%r15, %rdi
	call	_dl_allocate_tls_init@PLT
.L367:
	movq	1696(%r15), %rsi
	cmpq	%rsi, %r12
	ja	.L491
	movq	24(%rsp), %rax
	subq	%r12, %rsi
	subq	%rbx, %rax
	cmpq	%rax, %rsi
	ja	.L492
.L383:
	movq	%r12, 1704(%r15)
.L352:
	leaq	736(%r15), %rax
	movl	$0, 1560(%r15)
	movq	$-32, 744(%r15)
	movq	$0, 752(%r15)
	movq	%r15, 16(%r15)
	movq	%rax, 728(%r15)
	movq	%rax, 736(%r15)
	movq	%r15, %rax
	subq	__static_tls_size(%rip), %rax
	cmpq	$-1, %r13
	movq	%r15, (%r15)
	sete	2416(%r15)
	movl	8(%rbp), %esi
	leaq	2496(%rax), %rbx
	movq	8(%rsp), %rax
	movl	%esi, %ecx
	andl	$-97, %ecx
	movq	%rax, 1600(%r15)
	movq	16(%rsp), %rax
	movq	%rax, 1608(%r15)
	movq	%fs:16, %rdx
	movl	780(%rdx), %eax
	movdqa	1616(%rdx), %xmm0
	andl	$96, %eax
	orl	%ecx, %eax
	testb	$1, %sil
	movl	$0, %ecx
	cmovne	%r15, %rcx
	movl	%eax, 780(%r15)
	movq	%rcx, 1576(%r15)
	movaps	%xmm0, 1616(%r15)
	movq	1632(%rdx), %rcx
	movq	%rcx, 1632(%r15)
	movl	1596(%rdx), %ecx
	movl	1592(%rdx), %edx
	movl	%ecx, 1596(%r15)
	movl	%edx, 1592(%r15)
#APP
# 697 "pthread_create.c" 1
	movq %fs:40,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 40(%r15)
#APP
# 702 "pthread_create.c" 1
	movq %fs:48,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 48(%r15)
#APP
# 22 "../sysdeps/x86/nptl/tls-setup.h" 1
	movl %fs:72,%edx
# 0 "" 2
#NO_APP
	testb	$2, %sil
	movl	%edx, 72(%r15)
	jne	.L493
.L390:
	cmpl	$1, __nptl_nthreads(%rip)
	je	.L494
.L395:
	movq	(%rsp), %rax
	movq	%r15, (%rax)
#APP
# 751 "pthread_create.c" 1
	lock;incl __nptl_nthreads(%rip)
# 0 "" 2
#NO_APP
	leaq	144(%rsp), %r12
	movb	$0, 78(%rsp)
	movb	$0, 79(%rsp)
	movl	$8, %r10d
	leaq	sigall_set(%rip), %rsi
	xorl	%edi, %edi
	movq	%r12, %rdx
	movl	$14, %eax
#APP
# 71 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.L396
	cmpb	$0, 144(%rax)
	je	.L396
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, 2288(%r15)
	movdqu	32(%rax), %xmm0
	movaps	%xmm0, 2304(%r15)
	movdqu	48(%rax), %xmm0
	movaps	%xmm0, 2320(%r15)
	movdqu	64(%rax), %xmm0
	movaps	%xmm0, 2336(%r15)
	movdqu	80(%rax), %xmm0
	movaps	%xmm0, 2352(%r15)
	movdqu	96(%rax), %xmm0
	movaps	%xmm0, 2368(%r15)
	movdqu	112(%rax), %xmm0
	movaps	%xmm0, 2384(%r15)
	movdqu	128(%rax), %xmm0
	movaps	%xmm0, 2400(%r15)
.L397:
	xorl	%eax, %eax
#APP
# 606 "pthread_create.c" 1
	movb %fs:1553,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L495
.L398:
	leaq	79(%rsp), %r8
	leaq	78(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	movq	%r15, %rdi
	call	create_thread
	movl	%eax, %r8d
.L399:
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	testl	%r8d, %r8d
	jne	.L496
.L403:
	cmpb	$0, 78(%rsp)
	je	.L410
	leaq	1560(%r15), %rdi
	xorl	%eax, %eax
#APP
# 870 "pthread_create.c" 1
	xchgl %eax, 1560(%r15)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L497
.L410:
#APP
# 875 "pthread_create.c" 1
	movl $1,%fs:24
# 0 "" 2
#NO_APP
	xorl	%r8d, %r8d
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L485:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L498
.L343:
	movq	32+__default_pthread_attr(%rip), %rbx
	xorl	%eax, %eax
#APP
# 419 "allocatestack.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L499
.L344:
	testb	$8, 8(%rbp)
	je	.L345
	movq	32(%rbp), %rax
	movq	24(%rbp), %rdx
	testq	%rax, %rax
	jne	.L412
.L346:
	movq	__static_tls_align_m1(%rip), %rax
	leaq	-2496(%rdx), %r9
	andq	%r9, %rax
	leaq	2496(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L500
	subq	%rax, %r9
	xorl	%eax, %eax
	subq	%rbx, %rdx
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	movq	$0, (%r9)
	movq	$0, 2488(%r9)
	movq	%r9, %r15
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2496, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	784(%r9), %rax
	movq	%rdx, 1680(%r9)
	movq	%rbx, 1688(%r9)
	movb	$1, 1554(%r9)
	movl	$1, 24(%r9)
	movq	%r9, %rdi
	movq	%rax, 1296(%r9)
	movq	__libc_multiple_threads_ptr(%rip), %rax
	movl	$1, (%rax)
	movl	$-1, 1564(%r9)
	movl	$1, __pthread_multiple_threads(%rip)
	call	_dl_allocate_tls@PLT
	testq	%rax, %rax
	je	.L501
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L502
.L351:
	movq	_dl_stack_user(%rip), %rdx
	leaq	704(%r15), %rax
	leaq	_dl_stack_user(%rip), %rbx
	movq	%rbx, 712(%r15)
	movq	%rdx, 704(%r15)
	movq	%rax, 8(%rdx)
	movq	%rax, _dl_stack_user(%rip)
	xorl	%eax, %eax
#APP
# 506 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L352
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 506 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L352
	.p2align 4,,10
	.p2align 3
.L396:
	movdqa	144(%rsp), %xmm0
	movabsq	$-2147483649, %rax
	movaps	%xmm0, 2288(%r15)
	andq	%rax, 2288(%r15)
	movdqa	160(%rsp), %xmm0
	movaps	%xmm0, 2304(%r15)
	movdqa	176(%rsp), %xmm0
	movaps	%xmm0, 2320(%r15)
	movdqa	192(%rsp), %xmm0
	movaps	%xmm0, 2336(%r15)
	movdqa	208(%rsp), %xmm0
	movaps	%xmm0, 2352(%r15)
	movdqa	224(%rsp), %xmm0
	movaps	%xmm0, 2368(%r15)
	movdqa	240(%rsp), %xmm0
	movaps	%xmm0, 2384(%r15)
	movdqa	256(%rsp), %xmm0
	movaps	%xmm0, 2400(%r15)
	jmp	.L397
	.p2align 4,,10
	.p2align 3
.L416:
	movq	%rsi, %r15
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L494:
	call	_IO_enable_locks@PLT
	jmp	.L395
	.p2align 4,,10
	.p2align 3
.L496:
	cmpb	$0, 79(%rsp)
	jne	.L503
#APP
# 844 "pthread_create.c" 1
	lock;decl __nptl_nthreads(%rip)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
#APP
# 848 "pthread_create.c" 1
	xchgl %eax, 1564(%r15)
# 0 "" 2
#NO_APP
	cmpl	$-2, %eax
	je	.L504
.L408:
	movq	%r15, %rdi
	movl	%r8d, (%rsp)
	call	__deallocate_stack
	movl	(%rsp), %r8d
.L405:
	cmpl	$12, %r8d
	jne	.L347
.L388:
	movl	$11, %r8d
	jmp	.L347
	.p2align 4,,10
	.p2align 3
.L495:
	movl	__nptl_threads_events(%rip), %eax
	orl	1616(%r15), %eax
	testb	$-128, %al
	je	.L398
	leaq	79(%rsp), %r8
	leaq	78(%rsp), %rdx
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	movq	%r15, %rdi
	movb	$1, 78(%rsp)
	call	create_thread
	testl	%eax, %eax
	movl	%eax, %r8d
	jne	.L399
	cmpb	$0, 78(%rsp)
	je	.L505
	cmpb	$0, 1555(%r15)
	je	.L506
	movl	$8, 1624(%r15)
	movq	%r15, 1632(%r15)
.L402:
	movq	__nptl_last_event(%rip), %rax
	movq	%rax, 1640(%r15)
	lock cmpxchgq	%r15, __nptl_last_event(%rip)
	jne	.L402
	call	__nptl_create_event@PLT
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L403
	.p2align 4,,10
	.p2align 3
.L493:
	testb	$96, %sil
	je	.L390
	testb	$64, %sil
	je	.L392
	movl	4(%rbp), %edx
	orl	$64, %eax
	movl	%eax, 780(%r15)
	movl	%edx, 1596(%r15)
.L392:
	andl	$32, %esi
	je	.L393
	movl	0(%rbp), %edx
	orl	$32, %eax
	movl	%eax, 780(%r15)
	movl	%edx, 1592(%r15)
.L393:
	movl	%eax, %edx
	andl	$96, %edx
	cmpl	$96, %edx
	je	.L390
	testb	$64, %al
	jne	.L394
	xorl	%edi, %edi
	movl	$145, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/default-sched.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, 1596(%r15)
	movl	780(%r15), %eax
	orl	$64, %eax
	movl	%eax, 780(%r15)
.L394:
	testb	$32, %al
	jne	.L390
	leaq	1592(%r15), %rsi
	xorl	%edi, %edi
	movl	$143, %eax
#APP
# 37 "../sysdeps/unix/sysv/linux/default-sched.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	orl	$32, 780(%r15)
	jmp	.L390
	.p2align 4,,10
	.p2align 3
.L486:
	movq	24(%rbp), %rdx
	movq	%rbx, %rax
.L412:
	movq	__static_tls_size(%rip), %rdi
	leaq	2048(%rdi), %rcx
	cmpq	%rax, %rcx
	jbe	.L346
	jmp	.L354
	.p2align 4,,10
	.p2align 3
.L503:
	cmpb	$0, 78(%rsp)
	jne	.L405
	leaq	__PRETTY_FUNCTION__.13628(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$837, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L489:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movq	%r8, 24(%rsp)
	call	__lll_lock_wait_private@PLT
	movq	24(%rsp), %r8
	jmp	.L357
	.p2align 4,,10
	.p2align 3
.L358:
	xorl	%eax, %eax
#APP
# 197 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L507
.L363:
	leaq	(%r8,%rbx), %r15
	testw	%bx, %bx
	movl	$0, %edx
	movl	$-1, %r8d
	movl	$131106, %ecx
	movq	%r15, %rsi
	cmovne	%rbx, %rsi
	testq	%r12, %r12
	cmove	44(%rsp), %edx
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movq	%rsi, 24(%rsp)
	call	__mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, 32(%rsp)
	je	.L482
	cmpq	$0, 32(%rsp)
	je	.L508
	movq	32(%rsp), %rcx
	movq	24(%rsp), %rdi
	movq	__static_tls_align_m1(%rip), %rax
	leaq	-2496(%rcx,%rdi), %r9
	notq	%rax
	andq	%rax, %r9
	testq	%r12, %r12
	movq	%r9, %r15
	je	.L373
	subq	%r12, %rdi
	movl	44(%rsp), %edx
	movq	%rdi, %rsi
	leaq	(%rcx,%r12), %rdi
	call	__mprotect@PLT
	testl	%eax, %eax
	je	.L373
	movq	errno@gottpoff(%rip), %r14
	movl	%fs:(%r14), %edx
	testl	%edx, %edx
	jne	.L509
.L373:
	movq	32(%rsp), %rax
	movq	%r12, 1696(%r15)
	movq	%r15, %rdi
	movl	$1, 24(%r15)
	movq	%rax, 1680(%r15)
	movq	24(%rsp), %rax
	movq	%rax, 1688(%r15)
	leaq	784(%r15), %rax
	movq	%rax, 1296(%r15)
	movq	__libc_multiple_threads_ptr(%rip), %rax
	movl	$1, (%rax)
	movl	$-1, 1564(%r15)
	movl	$1, __pthread_multiple_threads(%rip)
	call	_dl_allocate_tls@PLT
	testq	%rax, %rax
	je	.L510
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L511
.L376:
	leaq	704(%r15), %rdi
	movq	%rdi, %rax
	movq	%rdi, 56(%rsp)
	orq	$1, %rax
	movq	%rax, in_flight_stack(%rip)
	movq	_dl_stack_used(%rip), %rax
	leaq	_dl_stack_used(%rip), %rsi
	movq	%rsi, 712(%r15)
	movq	%rax, 704(%r15)
	movq	%rdi, 8(%rax)
	movq	%rdi, _dl_stack_used(%rip)
	movq	$0, in_flight_stack(%rip)
	xorl	%eax, %eax
#APP
# 640 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L512
.L377:
	testb	$1, _dl_stack_flags(%rip)
	je	.L367
	movl	48(%rsp), %eax
	testl	%eax, %eax
	jne	.L367
	movq	%r15, %rdi
	call	change_stack_perm
	testl	%eax, %eax
	je	.L367
	movq	24(%rsp), %rsi
	movq	32(%rsp), %rdi
	movl	%eax, (%rsp)
	call	__munmap@PLT
	movl	(%rsp), %r8d
	jmp	.L405
	.p2align 4,,10
	.p2align 3
.L497:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 870 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L410
	.p2align 4,,10
	.p2align 3
.L501:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$12, %fs:(%rax)
	je	.L388
	leaq	__PRETTY_FUNCTION__.13121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$495, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%rsi, %r15
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L498:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movq	%r8, 32(%rsp)
	movq	%r9, 24(%rsp)
	call	__lll_lock_wait_private@PLT
	movq	32(%rsp), %r8
	movq	24(%rsp), %r9
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L499:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 419 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L344
	.p2align 4,,10
	.p2align 3
.L418:
	movl	$16, %edx
	jmp	.L365
.L491:
	movq	32(%rsp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	__mprotect@PLT
	testl	%eax, %eax
	je	.L481
.L384:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, _dl_stack_cache_lock(%rip)
	jne	.L513
.L381:
	movq	56(%rsp), %rax
	movq	%rax, in_flight_stack(%rip)
	movq	712(%r15), %rdx
	movq	704(%r15), %rax
	movq	%rdx, 8(%rax)
	movq	712(%r15), %rdx
	movq	%rax, (%rdx)
	movq	$0, in_flight_stack(%rip)
	xorl	%eax, %eax
#APP
# 686 "allocatestack.c" 1
	xchgl %eax, _dl_stack_cache_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L514
.L382:
	xorl	%esi, %esi
	movq	%r15, %rdi
	call	_dl_deallocate_tls@PLT
	movq	24(%rsp), %rsi
	movq	32(%rsp), %rdi
	call	__munmap@PLT
.L482:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r8d
.L371:
	testl	%r8d, %r8d
	jne	.L405
	movq	$0, 16
	ud2
	.p2align 4,,10
	.p2align 3
.L510:
	movq	errno@gottpoff(%rip), %rbx
	cmpl	$12, %fs:(%rbx)
	jne	.L515
	movq	24(%rsp), %rsi
	movq	32(%rsp), %rdi
	call	__munmap@PLT
	movl	%fs:(%rbx), %r8d
	jmp	.L371
.L490:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 215 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	1688(%r15), %rdx
	jmp	.L364
.L492:
	movq	32(%rsp), %rax
	movl	44(%rsp), %edx
	leaq	(%rax,%r12), %rdi
	call	__mprotect@PLT
	testl	%eax, %eax
	jne	.L384
.L481:
	movq	%r12, 1696(%r15)
	jmp	.L383
.L509:
	movq	24(%rsp), %rsi
	movq	32(%rsp), %rdi
	call	__munmap@PLT
	movl	%fs:(%r14), %r8d
	jmp	.L371
.L502:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L351
.L507:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 197 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L363
.L504:
	leaq	1564(%r15), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L408
	cmpl	$-22, %eax
	je	.L408
	cmpl	$-14, %eax
	je	.L408
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L512:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 640 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L377
.L511:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L376
.L513:
	leaq	_dl_stack_cache_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L381
.L514:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	_dl_stack_cache_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 686 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L382
.L487:
	leaq	__PRETTY_FUNCTION__.13121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$520, %edx
	call	__assert_fail@PLT
.L484:
	leaq	__PRETTY_FUNCTION__.13121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$408, %edx
	call	__assert_fail@PLT
.L500:
	leaq	__PRETTY_FUNCTION__.13121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$444, %edx
	call	__assert_fail@PLT
.L508:
	leaq	__PRETTY_FUNCTION__.13121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$570, %edx
	call	__assert_fail@PLT
.L515:
	leaq	__PRETTY_FUNCTION__.13121(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$625, %edx
	call	__assert_fail@PLT
.L506:
	leaq	__PRETTY_FUNCTION__.13628(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L505:
	leaq	__PRETTY_FUNCTION__.13628(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$798, %edx
	call	__assert_fail@PLT
	.size	__pthread_create_2_1, .-__pthread_create_2_1
	.weak	pthread_create
	.set	pthread_create,__pthread_create_2_1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.13112, @object
	.size	__PRETTY_FUNCTION__.13112, 19
__PRETTY_FUNCTION__.13112:
	.string	"advise_stack_range"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.13456, @object
	.size	__PRETTY_FUNCTION__.13456, 14
__PRETTY_FUNCTION__.13456:
	.string	"create_thread"
	.align 8
	.type	__PRETTY_FUNCTION__.13121, @object
	.size	__PRETTY_FUNCTION__.13121, 15
__PRETTY_FUNCTION__.13121:
	.string	"allocate_stack"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.13628, @object
	.size	__PRETTY_FUNCTION__.13628, 21
__PRETTY_FUNCTION__.13628:
	.string	"__pthread_create_2_1"
	.align 16
	.type	__PRETTY_FUNCTION__.13263, @object
	.size	__PRETTY_FUNCTION__.13263, 17
__PRETTY_FUNCTION__.13263:
	.string	"__reclaim_stacks"
	.globl	_thread_db_const_thread_area
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
	.type	_thread_db_const_thread_area, @object
	.size	_thread_db_const_thread_area, 4
_thread_db_const_thread_area:
	.long	25
	.globl	_thread_db_dtv_slotinfo_list_slotinfo
	.section	.rodata
	.align 8
	.type	_thread_db_dtv_slotinfo_list_slotinfo, @object
	.size	_thread_db_dtv_slotinfo_list_slotinfo, 12
_thread_db_dtv_slotinfo_list_slotinfo:
	.long	128
	.long	0
	.long	16
	.globl	_thread_db__dl_stack_user
	.align 8
	.type	_thread_db__dl_stack_user, @object
	.size	_thread_db__dl_stack_user, 12
_thread_db__dl_stack_user:
	.long	128
	.long	1
	.long	0
	.globl	_thread_db__dl_stack_used
	.set	_thread_db__dl_stack_used,_thread_db__dl_stack_user
	.globl	_thread_db_dtv_dtv
	.align 8
	.type	_thread_db_dtv_dtv, @object
	.size	_thread_db_dtv_dtv, 12
_thread_db_dtv_dtv:
	.long	128
	.long	134217727
	.long	0
	.globl	_thread_db_link_map_l_tls_offset
	.align 8
	.type	_thread_db_link_map_l_tls_offset, @object
	.size	_thread_db_link_map_l_tls_offset, 12
_thread_db_link_map_l_tls_offset:
	.long	64
	.long	1
	.long	1112
	.globl	_thread_db_link_map_l_tls_modid
	.align 8
	.type	_thread_db_link_map_l_tls_modid, @object
	.size	_thread_db_link_map_l_tls_modid, 12
_thread_db_link_map_l_tls_modid:
	.long	64
	.long	1
	.long	1120
	.globl	_thread_db_pthread_key_data_level2_data
	.align 8
	.type	_thread_db_pthread_key_data_level2_data, @object
	.size	_thread_db_pthread_key_data_level2_data, 12
_thread_db_pthread_key_data_level2_data:
	.long	128
	.long	32
	.long	0
	.globl	_thread_db_sizeof_pthread_key_data_level2
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_pthread_key_data_level2, @object
	.size	_thread_db_sizeof_pthread_key_data_level2, 4
_thread_db_sizeof_pthread_key_data_level2:
	.long	512
	.globl	_thread_db___pthread_keys
	.section	.rodata
	.align 8
	.type	_thread_db___pthread_keys, @object
	.size	_thread_db___pthread_keys, 12
_thread_db___pthread_keys:
	.long	128
	.long	1024
	.long	0
	.globl	_thread_db___nptl_initial_report_events
	.align 8
	.type	_thread_db___nptl_initial_report_events, @object
	.size	_thread_db___nptl_initial_report_events, 12
_thread_db___nptl_initial_report_events:
	.long	8
	.long	1
	.long	0
	.globl	_thread_db___nptl_nthreads
	.align 8
	.type	_thread_db___nptl_nthreads, @object
	.size	_thread_db___nptl_nthreads, 12
_thread_db___nptl_nthreads:
	.long	32
	.long	1
	.long	0
	.globl	_thread_db_td_eventbuf_t_eventdata
	.align 8
	.type	_thread_db_td_eventbuf_t_eventdata, @object
	.size	_thread_db_td_eventbuf_t_eventdata, 12
_thread_db_td_eventbuf_t_eventdata:
	.long	64
	.long	1
	.long	16
	.globl	_thread_db_td_eventbuf_t_eventnum
	.align 8
	.type	_thread_db_td_eventbuf_t_eventnum, @object
	.size	_thread_db_td_eventbuf_t_eventnum, 12
_thread_db_td_eventbuf_t_eventnum:
	.long	32
	.long	1
	.long	8
	.globl	_thread_db_sizeof_td_eventbuf_t
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_td_eventbuf_t, @object
	.size	_thread_db_sizeof_td_eventbuf_t, 4
_thread_db_sizeof_td_eventbuf_t:
	.long	24
	.globl	_thread_db_td_thr_events_t_event_bits
	.section	.rodata
	.align 8
	.type	_thread_db_td_thr_events_t_event_bits, @object
	.size	_thread_db_td_thr_events_t_event_bits, 12
_thread_db_td_thr_events_t_event_bits:
	.long	32
	.long	2
	.long	0
	.globl	_thread_db_sizeof_td_thr_events_t
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_td_thr_events_t, @object
	.size	_thread_db_sizeof_td_thr_events_t, 4
_thread_db_sizeof_td_thr_events_t:
	.long	8
	.globl	_thread_db_list_t_prev
	.section	.rodata
	.align 8
	.type	_thread_db_list_t_prev, @object
	.size	_thread_db_list_t_prev, 12
_thread_db_list_t_prev:
	.long	64
	.long	1
	.long	8
	.globl	_thread_db_dtv_slotinfo_map
	.set	_thread_db_dtv_slotinfo_map,_thread_db_list_t_prev
	.globl	_thread_db_dtv_slotinfo_list_next
	.set	_thread_db_dtv_slotinfo_list_next,_thread_db_list_t_prev
	.globl	_thread_db_pthread_dtvp
	.set	_thread_db_pthread_dtvp,_thread_db_list_t_prev
	.globl	_thread_db_pthread_key_data_data
	.set	_thread_db_pthread_key_data_data,_thread_db_list_t_prev
	.globl	_thread_db_pthread_key_struct_destr
	.set	_thread_db_pthread_key_struct_destr,_thread_db_list_t_prev
	.globl	_thread_db_list_t_next
	.align 8
	.type	_thread_db_list_t_next, @object
	.size	_thread_db_list_t_next, 12
_thread_db_list_t_next:
	.long	64
	.long	1
	.long	0
	.globl	_thread_db_dtv_slotinfo_gen
	.set	_thread_db_dtv_slotinfo_gen,_thread_db_list_t_next
	.globl	_thread_db_dtv_slotinfo_list_len
	.set	_thread_db_dtv_slotinfo_list_len,_thread_db_list_t_next
	.globl	_thread_db__dl_tls_dtv_slotinfo_list
	.set	_thread_db__dl_tls_dtv_slotinfo_list,_thread_db_list_t_next
	.globl	_thread_db_dtv_t_counter
	.set	_thread_db_dtv_t_counter,_thread_db_list_t_next
	.globl	_thread_db_dtv_t_pointer_val
	.set	_thread_db_dtv_t_pointer_val,_thread_db_list_t_next
	.globl	_thread_db_pthread_key_data_seq
	.set	_thread_db_pthread_key_data_seq,_thread_db_list_t_next
	.globl	_thread_db_pthread_key_struct_seq
	.set	_thread_db_pthread_key_struct_seq,_thread_db_list_t_next
	.globl	_thread_db___nptl_last_event
	.set	_thread_db___nptl_last_event,_thread_db_list_t_next
	.globl	_thread_db_sizeof_list_t
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_list_t, @object
	.size	_thread_db_sizeof_list_t, 4
_thread_db_sizeof_list_t:
	.long	16
	.globl	_thread_db_sizeof_dtv_slotinfo
	.set	_thread_db_sizeof_dtv_slotinfo,_thread_db_sizeof_list_t
	.globl	_thread_db_sizeof_dtv_slotinfo_list
	.set	_thread_db_sizeof_dtv_slotinfo_list,_thread_db_sizeof_list_t
	.globl	_thread_db_sizeof_pthread_key_data
	.set	_thread_db_sizeof_pthread_key_data,_thread_db_sizeof_list_t
	.globl	_thread_db_sizeof_pthread_key_struct
	.set	_thread_db_sizeof_pthread_key_struct,_thread_db_sizeof_list_t
	.globl	_thread_db_pthread_nextevent
	.section	.rodata
	.align 8
	.type	_thread_db_pthread_nextevent, @object
	.size	_thread_db_pthread_nextevent, 12
_thread_db_pthread_nextevent:
	.long	64
	.long	1
	.long	1640
	.globl	_thread_db_pthread_eventbuf_eventmask_event_bits
	.align 8
	.type	_thread_db_pthread_eventbuf_eventmask_event_bits, @object
	.size	_thread_db_pthread_eventbuf_eventmask_event_bits, 12
_thread_db_pthread_eventbuf_eventmask_event_bits:
	.long	32
	.long	2
	.long	1616
	.globl	_thread_db_pthread_eventbuf_eventmask
	.align 8
	.type	_thread_db_pthread_eventbuf_eventmask, @object
	.size	_thread_db_pthread_eventbuf_eventmask, 12
_thread_db_pthread_eventbuf_eventmask:
	.long	64
	.long	1
	.long	1616
	.globl	_thread_db_pthread_eventbuf
	.align 8
	.type	_thread_db_pthread_eventbuf, @object
	.size	_thread_db_pthread_eventbuf, 12
_thread_db_pthread_eventbuf:
	.long	192
	.long	1
	.long	1616
	.globl	_thread_db_pthread_specific
	.align 8
	.type	_thread_db_pthread_specific, @object
	.size	_thread_db_pthread_specific, 12
_thread_db_pthread_specific:
	.long	2048
	.long	1
	.long	1296
	.globl	_thread_db_pthread_schedparam_sched_priority
	.align 8
	.type	_thread_db_pthread_schedparam_sched_priority, @object
	.size	_thread_db_pthread_schedparam_sched_priority, 12
_thread_db_pthread_schedparam_sched_priority:
	.long	32
	.long	1
	.long	1592
	.globl	_thread_db_pthread_schedpolicy
	.align 8
	.type	_thread_db_pthread_schedpolicy, @object
	.size	_thread_db_pthread_schedpolicy, 12
_thread_db_pthread_schedpolicy:
	.long	32
	.long	1
	.long	1596
	.globl	_thread_db_pthread_cancelhandling
	.align 8
	.type	_thread_db_pthread_cancelhandling, @object
	.size	_thread_db_pthread_cancelhandling, 12
_thread_db_pthread_cancelhandling:
	.long	32
	.long	1
	.long	776
	.globl	_thread_db_pthread_start_routine
	.align 8
	.type	_thread_db_pthread_start_routine, @object
	.size	_thread_db_pthread_start_routine, 12
_thread_db_pthread_start_routine:
	.long	64
	.long	1
	.long	1600
	.globl	_thread_db_pthread_tid
	.align 8
	.type	_thread_db_pthread_tid, @object
	.size	_thread_db_pthread_tid, 12
_thread_db_pthread_tid:
	.long	32
	.long	1
	.long	720
	.globl	_thread_db_pthread_report_events
	.align 8
	.type	_thread_db_pthread_report_events, @object
	.size	_thread_db_pthread_report_events, 12
_thread_db_pthread_report_events:
	.long	8
	.long	1
	.long	1553
	.globl	_thread_db_pthread_list
	.align 8
	.type	_thread_db_pthread_list, @object
	.size	_thread_db_pthread_list, 12
_thread_db_pthread_list:
	.long	128
	.long	1
	.long	704
	.globl	_thread_db_sizeof_pthread
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_pthread, @object
	.size	_thread_db_sizeof_pthread, 4
_thread_db_sizeof_pthread:
	.long	2496
	.local	in_flight_stack
	.comm	in_flight_stack,8,8
	.section	.data.rel.local,"aw",@progbits
	.align 16
	.type	stack_cache, @object
	.size	stack_cache, 16
stack_cache:
	.quad	stack_cache
	.quad	stack_cache
	.local	stack_cache_actsize
	.comm	stack_cache_actsize,8,8
	.globl	__nptl_nthreads
	.data
	.align 4
	.type	__nptl_nthreads, @object
	.size	__nptl_nthreads, 4
__nptl_nthreads:
	.long	1
	.local	__nptl_last_event
	.comm	__nptl_last_event,8,8
	.local	__nptl_threads_events
	.comm	__nptl_threads_events,8,8
	.comm	__pthread_debug,4,4
	.section	.rodata
	.align 32
	.type	sigall_set, @object
	.size	sigall_set, 128
sigall_set:
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.weak	__call_tls_dtors
