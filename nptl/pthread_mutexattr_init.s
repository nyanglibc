	.text
	.p2align 4,,15
	.globl	__pthread_mutexattr_init
	.type	__pthread_mutexattr_init, @function
__pthread_mutexattr_init:
	movl	$0, (%rdi)
	xorl	%eax, %eax
	ret
	.size	__pthread_mutexattr_init, .-__pthread_mutexattr_init
	.weak	pthread_mutexattr_init
	.set	pthread_mutexattr_init,__pthread_mutexattr_init
