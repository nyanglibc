	.text
	.p2align 4,,15
	.globl	__pthread_detach
	.type	__pthread_detach, @function
__pthread_detach:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	js	.L4
	xorl	%eax, %eax
	lock cmpxchgq	%rdi, 1576(%rdi)
	jne	.L14
	movl	776(%rdi), %eax
	andl	$16, %eax
	jne	.L15
	rep ret
	.p2align 4,,10
	.p2align 3
.L15:
	subq	$8, %rsp
	call	__free_tcb
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$3, %eax
	ret
.L14:
	cmpq	%rdi, 1576(%rdi)
	movl	$22, %edx
	movl	$0, %eax
	cmove	%edx, %eax
	ret
	.size	__pthread_detach, .-__pthread_detach
	.weak	pthread_detach
	.set	pthread_detach,__pthread_detach
	.hidden	__free_tcb
