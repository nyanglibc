	.text
	.p2align 4,,15
	.globl	mtx_lock
	.type	mtx_lock, @function
mtx_lock:
	subq	$8, %rsp
	call	__GI___pthread_mutex_lock
	cmpl	$12, %eax
	je	.L3
	jle	.L13
	cmpl	$16, %eax
	je	.L6
	cmpl	$110, %eax
	jne	.L2
	movl	$4, %eax
.L1:
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$1, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	testl	%eax, %eax
	je	.L1
.L2:
	movl	$2, %eax
	addq	$8, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$3, %eax
	addq	$8, %rsp
	ret
	.size	mtx_lock, .-mtx_lock
