	.text
	.p2align 4,,15
	.globl	__pthread_timedjoin_np
	.type	__pthread_timedjoin_np, @function
__pthread_timedjoin_np:
	movq	%rdx, %rcx
	movl	$1, %r8d
	xorl	%edx, %edx
	jmp	__pthread_clockjoin_ex
	.size	__pthread_timedjoin_np, .-__pthread_timedjoin_np
	.weak	pthread_timedjoin_np
	.set	pthread_timedjoin_np,__pthread_timedjoin_np
	.hidden	__pthread_clockjoin_ex
