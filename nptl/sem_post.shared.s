	.text
#APP
	.symver __new_sem_post,sem_post@@GLIBC_2.2.5
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
#NO_APP
	.text
	.p2align 4,,15
	.globl	__new_sem_post
	.type	__new_sem_post, @function
__new_sem_post:
	movl	8(%rdi), %esi
	movq	(%rdi), %rax
.L4:
	cmpl	$2147483647, %eax
	je	.L17
	leaq	1(%rax), %rdx
	lock cmpxchgq	%rdx, (%rdi)
	jne	.L4
	shrq	$32, %rax
	testq	%rax, %rax
	je	.L7
	xorb	$-127, %sil
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L18
.L7:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	cmpl	$-22, %eax
	je	.L7
	cmpl	$-14, %eax
	je	.L7
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L17:
	movq	errno@gottpoff(%rip), %rax
	movl	$75, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__new_sem_post, .-__new_sem_post
