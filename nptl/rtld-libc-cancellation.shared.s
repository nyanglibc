	.text
	.p2align 4,,15
	.globl	__libc_enable_asynccancel
	.hidden	__libc_enable_asynccancel
	.type	__libc_enable_asynccancel, @function
__libc_enable_asynccancel:
	movq	%fs:16, %rsi
#APP
# 35 "../nptl/cancellation.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
.L3:
	movl	%edx, %ecx
	orl	$2, %ecx
	cmpl	%ecx, %edx
	je	.L1
	movl	%edx, %eax
	lock cmpxchgl	%ecx, 776(%rsi)
	cmpl	%eax, %edx
	jne	.L5
	andl	$-69, %ecx
	cmpl	$10, %ecx
	je	.L10
.L1:
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	%eax, %edx
	jmp	.L3
.L10:
	subq	$8, %rsp
#APP
# 50 "../nptl/cancellation.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
#APP
# 304 "pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__pthread_unwind@PLT
	.size	__libc_enable_asynccancel, .-__libc_enable_asynccancel
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__libc_disable_asynccancel
	.hidden	__libc_disable_asynccancel
	.type	__libc_disable_asynccancel, @function
__libc_disable_asynccancel:
	andl	$2, %edi
	jne	.L21
#APP
# 78 "../nptl/cancellation.c" 1
	movl %fs:776,%ecx
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
	leaq	776(%rax), %r8
.L13:
	movl	%ecx, %edx
	movl	%ecx, %eax
	andl	$-3, %edx
	lock cmpxchgl	%edx, (%r8)
	cmpl	%eax, %ecx
	jne	.L17
	andl	$12, %ecx
	cmpl	$4, %ecx
	je	.L24
.L21:
	rep ret
	.p2align 4,,10
	.p2align 3
.L24:
	pushq	%rbx
	movl	$202, %r9d
	movl	$1, %ebx
.L16:
	xorl	%r10d, %r10d
	movl	$128, %esi
	movq	%r8, %rdi
	movl	%r9d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L25
.L14:
#APP
# 102 "../nptl/cancellation.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	movl	%edx, %eax
	andl	$12, %eax
	cmpl	$4, %eax
	je	.L16
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%eax, %ecx
	jmp	.L13
.L25:
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L15
	movq	%rbx, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L14
.L15:
	leaq	.LC0(%rip), %rdi
	call	__GI___libc_fatal
	.size	__libc_disable_asynccancel, .-__libc_disable_asynccancel
