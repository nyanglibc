	.text
	.p2align 4,,15
	.globl	_dl_tunable_set_elision_skip_lock_busy
	.type	_dl_tunable_set_elision_skip_lock_busy, @function
_dl_tunable_set_elision_skip_lock_busy:
	movq	(%rdi), %rax
	movl	%eax, __elision_aconf(%rip)
	ret
	.size	_dl_tunable_set_elision_skip_lock_busy, .-_dl_tunable_set_elision_skip_lock_busy
	.p2align 4,,15
	.globl	_dl_tunable_set_elision_skip_lock_internal_abort
	.type	_dl_tunable_set_elision_skip_lock_internal_abort, @function
_dl_tunable_set_elision_skip_lock_internal_abort:
	movq	(%rdi), %rax
	movl	%eax, 4+__elision_aconf(%rip)
	ret
	.size	_dl_tunable_set_elision_skip_lock_internal_abort, .-_dl_tunable_set_elision_skip_lock_internal_abort
	.p2align 4,,15
	.globl	_dl_tunable_set_elision_retry_try_xbegin
	.type	_dl_tunable_set_elision_retry_try_xbegin, @function
_dl_tunable_set_elision_retry_try_xbegin:
	movq	(%rdi), %rax
	movl	%eax, 8+__elision_aconf(%rip)
	ret
	.size	_dl_tunable_set_elision_retry_try_xbegin, .-_dl_tunable_set_elision_retry_try_xbegin
	.p2align 4,,15
	.globl	_dl_tunable_set_elision_skip_trylock_internal_abort
	.type	_dl_tunable_set_elision_skip_trylock_internal_abort, @function
_dl_tunable_set_elision_skip_trylock_internal_abort:
	movq	(%rdi), %rax
	movl	%eax, 12+__elision_aconf(%rip)
	ret
	.size	_dl_tunable_set_elision_skip_trylock_internal_abort, .-_dl_tunable_set_elision_skip_trylock_internal_abort
	.p2align 4,,15
	.type	elision_init, @function
elision_init:
	pushq	%rbx
	movl	$7, %edi
	subq	$16, %rsp
	movq	_dl_tunable_set_elision_enable@GOTPCREL(%rip), %rdx
	leaq	12(%rsp), %rbx
	movq	%rbx, %rsi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_elision_skip_lock_busy@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$10, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_elision_skip_lock_internal_abort@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$21, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_elision_retry_try_xbegin@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$6, %edi
	call	__tunable_get_val@PLT
	movq	_dl_tunable_set_elision_skip_trylock_internal_abort@GOTPCREL(%rip), %rdx
	movq	%rbx, %rsi
	movl	$17, %edi
	call	__tunable_get_val@PLT
	movl	__pthread_force_elision(%rip), %eax
	testl	%eax, %eax
	jne	.L6
	movl	$0, 8+__elision_aconf(%rip)
.L6:
	addq	$16, %rsp
	popq	%rbx
	ret
	.size	elision_init, .-elision_init
	.p2align 4,,15
	.globl	_dl_tunable_set_elision_enable
	.type	_dl_tunable_set_elision_enable, @function
_dl_tunable_set_elision_enable:
	cmpl	$1, (%rdi)
	je	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	_rtld_global_ro@GOTPCREL(%rip), %rax
	movl	176(%rax), %eax
	shrl	$11, %eax
	andl	$1, %eax
	movl	%eax, __pthread_force_elision(%rip)
	ret
	.size	_dl_tunable_set_elision_enable, .-_dl_tunable_set_elision_enable
	.p2align 4,,15
	.globl	__lll_lock_elision
	.hidden	__lll_lock_elision
	.type	__lll_lock_elision, @function
__lll_lock_elision:
	movzwl	(%rsi), %eax
	testw	%ax, %ax
	jg	.L13
	movl	8+__elision_aconf(%rip), %ecx
	testl	%ecx, %ecx
	jle	.L21
	movl	$-1, %r8d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L16:
	testb	$2, %al
	je	.L35
	subl	$1, %ecx
	je	.L21
.L22:
	movl	%r8d, %eax
	xbegin	.L15
.L15:
	cmpl	$-1, %eax
	jne	.L16
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L32
	xabort	$255
	subl	$1, %ecx
	jne	.L22
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	movl	$1, %ecx
	lock cmpxchgl	%ecx, (%rdi)
	jne	.L30
.L32:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movzwl	(%rsi), %eax
	subl	$1, %eax
	movw	%ax, (%rsi)
	jmp	.L21
	.p2align 4,,10
	.p2align 3
.L35:
	testb	$1, %al
	je	.L20
	shrl	$24, %eax
	cmpl	$255, %eax
	je	.L36
.L20:
	movswl	(%rsi), %eax
	movl	4+__elision_aconf(%rip), %ecx
	cmpl	%ecx, %eax
	je	.L21
.L34:
	movw	%cx, (%rsi)
	jmp	.L21
.L30:
	subq	$8, %rsp
	movl	%edx, %esi
	call	__lll_lock_wait
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
.L36:
	movswl	(%rsi), %eax
	movl	__elision_aconf(%rip), %ecx
	cmpl	%ecx, %eax
	je	.L21
	jmp	.L34
	.size	__lll_lock_elision, .-__lll_lock_elision
	.globl	__pthread_init_array
	.section	.init_array,"aw"
	.align 8
	.type	__pthread_init_array, @object
	.size	__pthread_init_array, 8
__pthread_init_array:
	.quad	elision_init
	.hidden	__pthread_force_elision
	.globl	__pthread_force_elision
	.bss
	.align 4
	.type	__pthread_force_elision, @object
	.size	__pthread_force_elision, 4
__pthread_force_elision:
	.zero	4
	.hidden	__elision_aconf
	.globl	__elision_aconf
	.data
	.align 16
	.type	__elision_aconf, @object
	.size	__elision_aconf, 16
__elision_aconf:
	.long	3
	.long	3
	.long	3
	.long	3
	.hidden	__lll_lock_wait
