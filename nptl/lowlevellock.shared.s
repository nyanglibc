	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__lll_lock_wait_private
	.hidden	__lll_lock_wait_private
	.type	__lll_lock_wait_private, @function
__lll_lock_wait_private:
	movl	(%rdi), %eax
	movq	%rdi, %r8
	cmpl	$2, %eax
	jne	.L2
.L3:
	xorl	%r10d, %r10d
	movl	$2, %edx
	movl	$128, %esi
	movq	%r8, %rdi
	movl	$202, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L8
.L2:
	movl	$2, %eax
	xchgl	(%r8), %eax
	testl	%eax, %eax
	jne	.L3
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L4
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L2
.L4:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	__lll_lock_wait_private, .-__lll_lock_wait_private
	.p2align 4,,15
	.globl	__lll_lock_wait
	.hidden	__lll_lock_wait
	.type	__lll_lock_wait, @function
__lll_lock_wait:
	movl	(%rdi), %eax
	movq	%rdi, %r8
	movl	%esi, %r9d
	cmpl	$2, %eax
	jne	.L10
.L11:
	movl	%r9d, %esi
	xorl	%r10d, %r10d
	movl	$2, %edx
	xorb	$-128, %sil
	movq	%r8, %rdi
	movl	$202, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L15
.L10:
	movl	$2, %eax
	xchgl	(%r8), %eax
	testl	%eax, %eax
	jne	.L11
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L12
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L10
.L12:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	__lll_lock_wait, .-__lll_lock_wait
