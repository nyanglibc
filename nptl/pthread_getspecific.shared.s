	.text
	.p2align 4,,15
	.globl	__GI___pthread_getspecific
	.hidden	__GI___pthread_getspecific
	.type	__GI___pthread_getspecific, @function
__GI___pthread_getspecific:
	cmpl	$31, %edi
	ja	.L2
	movl	%edi, %edx
	addq	$49, %rdx
	salq	$4, %rdx
	addq	%fs:16, %rdx
.L3:
	movq	8(%rdx), %rax
	testq	%rax, %rax
	je	.L1
	movl	%edi, %edi
	leaq	__GI___pthread_keys(%rip), %rcx
	movq	(%rdx), %rsi
	salq	$4, %rdi
	cmpq	%rsi, (%rcx,%rdi)
	jne	.L11
.L1:
	rep ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	$0, 8(%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1023, %edi
	ja	.L6
	movl	%edi, %ecx
	movl	%edi, %edx
	andl	$31, %ecx
	shrl	$5, %edx
#APP
# 45 "pthread_getspecific.c" 1
	movq %fs:1296(,%rdx,8),%rax
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L6
	salq	$4, %rcx
	leaq	(%rax,%rcx), %rdx
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.size	__GI___pthread_getspecific, .-__GI___pthread_getspecific
	.weak	pthread_getspecific
	.set	pthread_getspecific,__GI___pthread_getspecific
	.globl	__pthread_getspecific
	.set	__pthread_getspecific,__GI___pthread_getspecific
