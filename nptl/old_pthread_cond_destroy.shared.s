	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __pthread_cond_destroy_2_0,pthread_cond_destroy@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__pthread_cond_destroy_2_0
	.type	__pthread_cond_destroy_2_0, @function
__pthread_cond_destroy_2_0:
	subq	$8, %rsp
	movq	(%rdi), %rdi
	call	free@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__pthread_cond_destroy_2_0, .-__pthread_cond_destroy_2_0
