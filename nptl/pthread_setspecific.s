	.text
	.p2align 4,,15
	.globl	__pthread_setspecific
	.type	__pthread_setspecific, @function
__pthread_setspecific:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	cmpl	$31, %edi
	ja	.L2
	movl	%edi, %edi
	leaq	__pthread_keys(%rip), %rdx
	movq	%rdi, %rax
	salq	$4, %rax
	movq	(%rdx,%rax), %rdx
	movl	$22, %eax
	movl	%edx, %r12d
	andl	$1, %edx
	je	.L1
	addq	$49, %rdi
	salq	$4, %rdi
	addq	%fs:16, %rdi
	testq	%rsi, %rsi
	je	.L4
.L18:
#APP
# 82 "pthread_setspecific.c" 1
	movb $1,%fs:1552
# 0 "" 2
#NO_APP
.L4:
	movl	%r12d, %eax
	movq	%rbp, 8(%rdi)
	movq	%rax, (%rdi)
	xorl	%eax, %eax
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cmpl	$1023, %edi
	movl	$22, %eax
	ja	.L1
	movl	%edi, %edx
	leaq	__pthread_keys(%rip), %rcx
	salq	$4, %rdx
	movq	(%rcx,%rdx), %rdx
	movl	%edx, %r12d
	andl	$1, %edx
	je	.L1
	movl	%edi, %r13d
	movl	%edi, %ebx
	shrl	$5, %r13d
	andl	$31, %ebx
#APP
# 61 "pthread_setspecific.c" 1
	movq %fs:1296(,%r13,8),%rax
# 0 "" 2
#NO_APP
	testq	%rax, %rax
	je	.L20
.L5:
	movl	%ebx, %edi
	salq	$4, %rdi
	addq	%rax, %rdi
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L20:
	testq	%rsi, %rsi
	je	.L1
	movl	$16, %esi
	movl	$32, %edi
	call	calloc@PLT
	testq	%rax, %rax
	je	.L10
#APP
# 75 "pthread_setspecific.c" 1
	movq %rax,%fs:1296(,%r13,8)
# 0 "" 2
#NO_APP
	jmp	.L5
.L10:
	movl	$12, %eax
	jmp	.L1
	.size	__pthread_setspecific, .-__pthread_setspecific
	.weak	pthread_setspecific
	.set	pthread_setspecific,__pthread_setspecific
