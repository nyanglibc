	.text
	.p2align 4,,15
	.globl	pthread_setconcurrency
	.type	pthread_setconcurrency, @function
pthread_setconcurrency:
	testl	%edi, %edi
	js	.L3
	movl	%edi, __concurrency_level(%rip)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_setconcurrency, .-pthread_setconcurrency
	.comm	__concurrency_level,4,4
