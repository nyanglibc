	.text
	.p2align 4,,15
	.globl	pthread_rwlockattr_setkind_np
	.type	pthread_rwlockattr_setkind_np, @function
pthread_rwlockattr_setkind_np:
	cmpl	$2, %esi
	ja	.L3
	movl	%esi, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_rwlockattr_setkind_np, .-pthread_rwlockattr_setkind_np
