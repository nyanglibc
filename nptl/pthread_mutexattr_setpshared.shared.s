	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_setpshared
	.type	pthread_mutexattr_setpshared, @function
pthread_mutexattr_setpshared:
	testl	%esi, %esi
	jne	.L6
	andl	$2147483647, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	cmpl	$1, %esi
	jne	.L7
	orl	$-2147483648, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$22, %eax
	ret
	.size	pthread_mutexattr_setpshared, .-pthread_mutexattr_setpshared
