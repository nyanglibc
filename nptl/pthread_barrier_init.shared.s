	.text
	.p2align 4,,15
	.globl	__pthread_barrier_init
	.type	__pthread_barrier_init, @function
__pthread_barrier_init:
	leal	-1(%rdx), %eax
	cmpl	$2147483645, %eax
	ja	.L5
	xorl	%eax, %eax
	testq	%rsi, %rsi
	movl	$0, 16(%rdi)
	movl	%edx, 8(%rdi)
	movq	$0, (%rdi)
	je	.L4
	movl	(%rsi), %edx
	xorl	%eax, %eax
	testl	%edx, %edx
	setne	%al
	sall	$7, %eax
.L4:
	movl	%eax, 12(%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$22, %eax
	ret
	.size	__pthread_barrier_init, .-__pthread_barrier_init
	.weak	pthread_barrier_init
	.set	pthread_barrier_init,__pthread_barrier_init
