	.text
	.p2align 4,,15
	.globl	__libc_alloca_cutoff
	.hidden	__libc_alloca_cutoff
	.type	__libc_alloca_cutoff, @function
__libc_alloca_cutoff:
#APP
# 28 "alloca_cutoff.c" 1
	movq %fs:1688,%rax
# 0 "" 2
#NO_APP
	shrq	$2, %rax
	movl	$65536, %edx
	subq	$1, %rax
	cmpq	$65535, %rax
	ja	.L2
#APP
# 28 "alloca_cutoff.c" 1
	movq %fs:1688,%rdx
# 0 "" 2
#NO_APP
	shrq	$2, %rdx
	movl	$262144, %eax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L2:
	xorl	%eax, %eax
	cmpq	%rdi, %rdx
	setnb	%al
	ret
	.size	__libc_alloca_cutoff, .-__libc_alloca_cutoff
