	.text
	.p2align 4,,15
	.globl	pthread_rwlockattr_getpshared
	.type	pthread_rwlockattr_getpshared, @function
pthread_rwlockattr_getpshared:
	movl	4(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_rwlockattr_getpshared, .-pthread_rwlockattr_getpshared
