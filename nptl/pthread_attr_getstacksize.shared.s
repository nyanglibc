	.text
	.p2align 4,,15
	.globl	__pthread_attr_getstacksize
	.type	__pthread_attr_getstacksize, @function
__pthread_attr_getstacksize:
	pushq	%rbx
	movq	32(%rdi), %r8
	movq	%rsi, %rbx
	testq	%r8, %r8
	je	.L6
.L2:
	movq	%r8, (%rbx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L7
.L3:
	movq	32+__default_pthread_attr(%rip), %r8
	xorl	%eax, %eax
#APP
# 37 "pthread_attr_getstacksize.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L2
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 37 "pthread_attr_getstacksize.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L2
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L3
	.size	__pthread_attr_getstacksize, .-__pthread_attr_getstacksize
	.globl	pthread_attr_getstacksize
	.set	pthread_attr_getstacksize,__pthread_attr_getstacksize
	.hidden	__lll_lock_wait_private
	.hidden	__default_pthread_attr
	.hidden	__default_pthread_attr_lock
