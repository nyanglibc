	.text
#APP
	.symver __pthread_attr_getaffinity_new,pthread_attr_getaffinity_np@@GLIBC_2.3.4
	.symver __pthread_attr_getaffinity_old,pthread_attr_getaffinity_np@GLIBC_2.3.3
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_getaffinity_new
	.type	__pthread_attr_getaffinity_new, @function
__pthread_attr_getaffinity_new:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	movq	%rdx, %rdi
	subq	$8, %rsp
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.L2
	movq	(%rax), %rsi
	testq	%rsi, %rsi
	je	.L2
	movq	8(%rax), %rdx
	cmpq	%rdx, %rbx
	jnb	.L3
	cmpb	$0, (%rsi,%rbx)
	jne	.L8
	leaq	1(%rsi,%rbx), %rax
	leaq	(%rsi,%rdx), %rcx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L6:
	addq	$1, %rax
	cmpb	$0, -1(%rax)
	jne	.L8
.L5:
	cmpq	%rcx, %rax
	jne	.L6
.L3:
	cmpq	%rdx, %rbx
	cmovbe	%rbx, %rdx
	call	__mempcpy@PLT
	movq	40(%rbp), %rdx
	xorl	%ebp, %ebp
	movq	8(%rdx), %rdx
	cmpq	%rbx, %rdx
	jb	.L18
.L1:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$22, %ebp
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rbx, %rdx
	movl	$-1, %esi
	xorl	%ebp, %ebp
	call	memset@PLT
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	subq	%rdx, %rbx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbx, %rdx
	call	memset@PLT
	jmp	.L1
	.size	__pthread_attr_getaffinity_new, .-__pthread_attr_getaffinity_new
	.p2align 4,,15
	.globl	__pthread_attr_getaffinity_old
	.type	__pthread_attr_getaffinity_old, @function
__pthread_attr_getaffinity_old:
	movq	%rsi, %rdx
	movl	$128, %esi
	jmp	__pthread_attr_getaffinity_new@PLT
	.size	__pthread_attr_getaffinity_old, .-__pthread_attr_getaffinity_old
