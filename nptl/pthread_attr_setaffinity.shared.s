	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __pthread_attr_setaffinity_np,pthread_attr_setaffinity_np@@GLIBC_2.32
	.symver __pthread_attr_setaffinity_alias,pthread_attr_setaffinity_np@GLIBC_2.3.4
	.symver __pthread_attr_setaffinity_old,pthread_attr_setaffinity_np@GLIBC_2.3.3
#NO_APP
	.p2align 4,,15
	.globl	__GI___pthread_attr_setaffinity_np
	.hidden	__GI___pthread_attr_setaffinity_np
	.type	__GI___pthread_attr_setaffinity_np, @function
__GI___pthread_attr_setaffinity_np:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L8
	testq	%rsi, %rsi
	movq	%rsi, %r12
	jne	.L2
.L8:
	movq	40(%rbx), %rax
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.L1
	movq	(%rax), %rdi
	call	free@PLT
	movq	40(%rbx), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
.L1:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rdx, %r13
	call	__pthread_attr_extension
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L1
	movq	40(%rbx), %rax
	cmpq	%r12, 8(%rax)
	movq	(%rax), %rdi
	je	.L5
	movq	%r12, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L7
	movq	40(%rbx), %rax
	movq	%rdi, (%rax)
	movq	%r12, 8(%rax)
.L5:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	__GI_memcpy@PLT
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$12, %ebp
	jmp	.L1
	.size	__GI___pthread_attr_setaffinity_np, .-__GI___pthread_attr_setaffinity_np
	.globl	__pthread_attr_setaffinity_np
	.set	__pthread_attr_setaffinity_np,__GI___pthread_attr_setaffinity_np
	.globl	__pthread_attr_setaffinity_alias
	.set	__pthread_attr_setaffinity_alias,__pthread_attr_setaffinity_np
	.p2align 4,,15
	.globl	__pthread_attr_setaffinity_old
	.type	__pthread_attr_setaffinity_old, @function
__pthread_attr_setaffinity_old:
	movq	%rsi, %rdx
	movl	$128, %esi
	jmp	__GI___pthread_attr_setaffinity_np
	.size	__pthread_attr_setaffinity_old, .-__pthread_attr_setaffinity_old
	.hidden	__pthread_attr_extension
