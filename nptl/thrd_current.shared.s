	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	thrd_current
	.type	thrd_current, @function
thrd_current:
	movq	%fs:16, %rax
	ret
	.size	thrd_current, .-thrd_current
