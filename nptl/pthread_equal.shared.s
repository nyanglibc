	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_equal
	.type	__pthread_equal, @function
__pthread_equal:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	sete	%al
	ret
	.size	__pthread_equal, .-__pthread_equal
	.weak	pthread_equal
	.set	pthread_equal,__pthread_equal
