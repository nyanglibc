	.text
	.p2align 4,,15
	.globl	pthread_barrierattr_setpshared
	.type	pthread_barrierattr_setpshared, @function
pthread_barrierattr_setpshared:
	cmpl	$1, %esi
	ja	.L3
	movl	%esi, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_barrierattr_setpshared, .-pthread_barrierattr_setpshared
