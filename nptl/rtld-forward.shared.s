	.text
#APP
	.symver __pthread_cond_broadcast,pthread_cond_broadcast@@GLIBC_2.3.2
	.symver __pthread_cond_signal,pthread_cond_signal@@GLIBC_2.3.2
	.symver __pthread_cond_wait,pthread_cond_wait@@GLIBC_2.3.2
	.symver __pthread_cond_timedwait,pthread_cond_timedwait@@GLIBC_2.3.2
#NO_APP
	.p2align 4,,15
	.globl	__pthread_cond_broadcast
	.type	__pthread_cond_broadcast, @function
__pthread_cond_broadcast:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L1
	movq	__libc_pthread_functions(%rip), %rax
#APP
# 65 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L1:
	xorl	%eax, %eax
	ret
	.size	__pthread_cond_broadcast, .-__pthread_cond_broadcast
	.p2align 4,,15
	.globl	__pthread_cond_signal
	.type	__pthread_cond_signal, @function
__pthread_cond_signal:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L4
	movq	8+__libc_pthread_functions(%rip), %rax
#APP
# 75 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L4:
	xorl	%eax, %eax
	ret
	.size	__pthread_cond_signal, .-__pthread_cond_signal
	.p2align 4,,15
	.globl	__pthread_cond_wait
	.type	__pthread_cond_wait, @function
__pthread_cond_wait:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L6
	movq	16+__libc_pthread_functions(%rip), %rax
#APP
# 86 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L6:
	xorl	%eax, %eax
	ret
	.size	__pthread_cond_wait, .-__pthread_cond_wait
	.p2align 4,,15
	.globl	__pthread_cond_timedwait
	.type	__pthread_cond_timedwait, @function
__pthread_cond_timedwait:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L8
	movq	24+__libc_pthread_functions(%rip), %rax
#APP
# 99 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
	ret
	.size	__pthread_cond_timedwait, .-__pthread_cond_timedwait
	.p2align 4,,15
	.globl	__pthread_exit
	.type	__pthread_exit, @function
__pthread_exit:
	subq	$8, %rsp
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L13
	movq	64+__libc_pthread_functions(%rip), %rax
#APP
# 106 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L13:
	xorl	%edi, %edi
	call	exit@PLT
	.size	__pthread_exit, .-__pthread_exit
	.globl	pthread_exit
	.set	pthread_exit,__pthread_exit
	.p2align 4,,15
	.globl	pthread_mutex_destroy
	.type	pthread_mutex_destroy, @function
pthread_mutex_destroy:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L14
	movq	72+__libc_pthread_functions(%rip), %rax
#APP
# 111 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%eax, %eax
	ret
	.size	pthread_mutex_destroy, .-pthread_mutex_destroy
	.p2align 4,,15
	.globl	pthread_mutex_init
	.type	pthread_mutex_init, @function
pthread_mutex_init:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L16
	movq	80+__libc_pthread_functions(%rip), %rax
#APP
# 113 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L16:
	xorl	%eax, %eax
	ret
	.size	pthread_mutex_init, .-pthread_mutex_init
	.p2align 4,,15
	.globl	pthread_mutex_lock
	.type	pthread_mutex_lock, @function
pthread_mutex_lock:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L18
	movq	88+__libc_pthread_functions(%rip), %rax
#APP
# 117 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	ret
	.size	pthread_mutex_lock, .-pthread_mutex_lock
	.p2align 4,,15
	.globl	pthread_mutex_unlock
	.type	pthread_mutex_unlock, @function
pthread_mutex_unlock:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L20
	movq	96+__libc_pthread_functions(%rip), %rax
#APP
# 119 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L20:
	xorl	%eax, %eax
	ret
	.size	pthread_mutex_unlock, .-pthread_mutex_unlock
	.p2align 4,,15
	.weak	__pthread_setcancelstate
	.type	__pthread_setcancelstate, @function
__pthread_setcancelstate:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L22
	movq	104+__libc_pthread_functions(%rip), %rax
#APP
# 121 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%eax, %eax
	ret
	.size	__pthread_setcancelstate, .-__pthread_setcancelstate
	.globl	pthread_setcancelstate
	.set	pthread_setcancelstate,__pthread_setcancelstate
	.p2align 4,,15
	.globl	pthread_setcanceltype
	.type	pthread_setcanceltype, @function
pthread_setcanceltype:
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L24
	movq	112+__libc_pthread_functions(%rip), %rax
#APP
# 125 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L24:
	xorl	%eax, %eax
	ret
	.size	pthread_setcanceltype, .-pthread_setcanceltype
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.globl	__pthread_unwind
	.hidden	__pthread_unwind
	.type	__pthread_unwind, @function
__pthread_unwind:
	subq	$8, %rsp
	movl	__libc_pthread_functions_init(%rip), %eax
	movq	%rdi, %rdx
	testl	%eax, %eax
	jne	.L27
	movl	$39, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/safe-fatal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	$9, %esi
	movl	%eax, %edi
	movl	$62, %eax
#APP
# 29 "../sysdeps/unix/sysv/linux/safe-fatal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
.L27:
	movq	208+__libc_pthread_functions(%rip), %rax
	movq	%rdx, %rdi
#APP
# 127 "forward.c" 1
	ror $2*8+1, %rax
xor __pointer_chk_guard_local(%rip), %rax
# 0 "" 2
#NO_APP
	call	*%rax
	.size	__pthread_unwind, .-__pthread_unwind
	.hidden	__libc_pthread_functions_init
	.comm	__libc_pthread_functions_init,4,4
	.hidden	__libc_pthread_functions
	.comm	__libc_pthread_functions,240,32
