	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_setschedparam
	.type	__pthread_setschedparam, @function
__pthread_setschedparam:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	jle	.L9
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$24, %rsp
#APP
# 40 "pthread_setschedparam.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	1560(%rdi), %r13
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%r13)
# 0 "" 2
#NO_APP
.L4:
	movq	1712(%rbx), %rax
	movq	%r12, %rdx
	testq	%rax, %rax
	jne	.L18
.L5:
	movl	720(%rbx), %edi
	movl	%ebp, %esi
	call	__GI___sched_setscheduler
	cmpl	$-1, %eax
	je	.L19
	movl	%ebp, 1596(%rbx)
	movl	(%r12), %eax
	xorl	%r8d, %r8d
	orl	$96, 780(%rbx)
	movl	%eax, 1592(%rbx)
.L7:
#APP
# 68 "pthread_setschedparam.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L8
	subl	$1, 1560(%rbx)
.L1:
	addq	$24, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$3, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%r13)
	je	.L4
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L18:
	movl	(%rax), %eax
	cmpl	(%r12), %eax
	jle	.L5
	movl	%eax, 12(%rsp)
	leaq	12(%rsp), %rdx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L19:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r8d
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	xorl	%eax, %eax
#APP
# 68 "pthread_setschedparam.c" 1
	xchgl %eax, 0(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 68 "pthread_setschedparam.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
	.size	__pthread_setschedparam, .-__pthread_setschedparam
	.globl	pthread_setschedparam
	.set	pthread_setschedparam,__pthread_setschedparam
	.hidden	__lll_lock_wait_private
