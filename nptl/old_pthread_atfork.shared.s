	.text
#APP
	.symver __dyn_pthread_atfork,pthread_atfork@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__dyn_pthread_atfork
	.type	__dyn_pthread_atfork, @function
__dyn_pthread_atfork:
	movq	__dso_handle(%rip), %rcx
	jmp	__register_atfork@PLT
	.size	__dyn_pthread_atfork, .-__dyn_pthread_atfork
	.hidden	__dso_handle
