	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_clockrdlock
	.type	__pthread_rwlock_clockrdlock, @function
__pthread_rwlock_clockrdlock:
	testq	%rdx, %rdx
	je	.L2
	cmpl	$1, %esi
	ja	.L31
	cmpq	$999999999, 8(%rdx)
	ja	.L31
.L2:
	movl	24(%rdi), %ecx
#APP
# 298 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, %ecx
	je	.L32
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	cmpl	$2, 48(%rdi)
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	je	.L4
.L11:
	movl	$8, %eax
	lock xaddl	%eax, (%rbx)
	addl	$8, %eax
.L5:
	testl	%eax, %eax
	js	.L13
	testb	$1, %al
	jne	.L14
.L35:
	xorl	%edx, %edx
.L1:
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	(%rdi), %esi
	movl	$128, %r13d
.L8:
	movl	%esi, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L11
	movl	%esi, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	je	.L11
	movl	%esi, %edx
	movl	%esi, %eax
	orl	$4, %edx
	lock cmpxchgl	%edx, (%rbx)
	movl	%eax, %esi
	je	.L7
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L62:
	cmpl	$75, %eax
	je	.L1
.L7:
	movl	(%rbx), %esi
	testb	$4, %sil
	je	.L8
	movl	28(%rbx), %r8d
	movl	%r12d, %edx
	movq	%rbp, %rcx
	movq	%rbx, %rdi
	testl	%r8d, %r8d
	cmovne	%r13d, %r8d
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %edx
	jne	.L62
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	leal	-8(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L5
	movl	$11, %edx
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$22, %edx
.L57:
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$35, %edx
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L14:
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L63
	movl	%eax, %edx
	xorl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L14
	leaq	8(%rbx), %rdi
	xorl	%eax, %eax
	xchgl	(%rdi), %eax
	testb	$2, %al
	je	.L35
	cmpl	$1, 28(%rbx)
	movl	$2147483647, %edx
	movl	$202, %eax
	sbbl	%esi, %esi
	xorl	%r10d, %r10d
	andl	$128, %esi
	addl	$1, %esi
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L35
	cmpl	$-22, %eax
	je	.L35
	cmpl	$-14, %eax
	je	.L35
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L63:
	leaq	8(%rbx), %r13
	xorl	%r14d, %r14d
.L19:
	movl	0(%r13), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$3, %edx
	je	.L28
	testb	%r14b, %r14b
	jne	.L35
	movl	(%rbx), %eax
	testb	$1, %al
	jne	.L19
	movl	0(%r13), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$3, %edx
	jne	.L35
	movl	$1, %r14d
.L28:
	movl	28(%rbx), %r8d
	movl	$128, %edx
	testl	%r8d, %r8d
	cmovne	%edx, %r8d
	testb	$2, %al
	jne	.L24
	movl	$3, %edx
	lock cmpxchgl	%edx, 0(%r13)
	jne	.L19
.L24:
	movl	%r12d, %edx
	movq	%rbp, %rcx
	movl	$3, %esi
	movq	%r13, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %edx
	je	.L22
	cmpl	$75, %eax
	jne	.L19
.L22:
	movl	(%rbx), %eax
.L25:
	testb	$1, %al
	je	.L27
	leal	-8(%rax), %ecx
	lock cmpxchgl	%ecx, (%rbx)
	je	.L1
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L27:
	movl	0(%r13), %eax
	orl	$2, %eax
	cmpl	$3, %eax
	je	.L27
	jmp	.L35
	.size	__pthread_rwlock_clockrdlock, .-__pthread_rwlock_clockrdlock
	.weak	pthread_rwlock_clockrdlock
	.set	pthread_rwlock_clockrdlock,__pthread_rwlock_clockrdlock
