	.text
	.p2align 4,,15
	.globl	__h_errno_location
	.type	__h_errno_location, @function
__h_errno_location:
	movq	__h_errno@gottpoff(%rip), %rax
	addq	%fs:0, %rax
	ret
	.size	__h_errno_location, .-__h_errno_location
