	.text
#APP
	.symver system_alias,system@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	system_compat, @function
system_compat:
	jmp	__libc_system@PLT
	.size	system_compat, .-system_compat
	.globl	system_alias
	.set	system_alias,system_compat
