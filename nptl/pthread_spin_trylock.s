.globl pthread_spin_trylock
.type pthread_spin_trylock,@function
.align 1<<4
pthread_spin_trylock:
 movl $1, %eax
 xorl %ecx, %ecx
 lock
 cmpxchgl %ecx, (%rdi)
 movl $16, %eax
 cmovel %ecx, %eax
 retq
.size pthread_spin_trylock,.-pthread_spin_trylock
