	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___pthread_attr_setsigmask_internal
	.hidden	__GI___pthread_attr_setsigmask_internal
	.type	__GI___pthread_attr_setsigmask_internal, @function
__GI___pthread_attr_setsigmask_internal:
	testq	%rsi, %rsi
	je	.L10
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rsi, %rbx
	subq	$8, %rsp
	call	__pthread_attr_extension
	testl	%eax, %eax
	jne	.L1
	movq	40(%rbp), %rdx
	movdqu	(%rbx), %xmm0
	movups	%xmm0, 16(%rdx)
	movdqu	16(%rbx), %xmm0
	movups	%xmm0, 32(%rdx)
	movdqu	32(%rbx), %xmm0
	movups	%xmm0, 48(%rdx)
	movdqu	48(%rbx), %xmm0
	movups	%xmm0, 64(%rdx)
	movdqu	64(%rbx), %xmm0
	movups	%xmm0, 80(%rdx)
	movdqu	80(%rbx), %xmm0
	movups	%xmm0, 96(%rdx)
	movdqu	96(%rbx), %xmm0
	movups	%xmm0, 112(%rdx)
	movdqu	112(%rbx), %xmm0
	movb	$1, 144(%rdx)
	movups	%xmm0, 128(%rdx)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movq	40(%rdi), %rdx
	xorl	%eax, %eax
	testq	%rdx, %rdx
	je	.L7
	movb	$0, 144(%rdx)
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	rep ret
	.size	__GI___pthread_attr_setsigmask_internal, .-__GI___pthread_attr_setsigmask_internal
	.globl	__pthread_attr_setsigmask_internal
	.set	__pthread_attr_setsigmask_internal,__GI___pthread_attr_setsigmask_internal
	.hidden	__pthread_attr_extension
