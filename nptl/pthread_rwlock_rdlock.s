	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_rdlock
	.type	__pthread_rwlock_rdlock, @function
__pthread_rwlock_rdlock:
	movl	24(%rdi), %edx
#APP
# 298 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, %edx
	je	.L29
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	cmpl	$2, 48(%rdi)
	movq	%rdi, %rbx
	je	.L3
.L10:
	movl	$8, %eax
	lock xaddl	%eax, (%rbx)
	addl	$8, %eax
.L4:
	testl	%eax, %eax
	js	.L12
	testb	$1, %al
	jne	.L13
.L32:
	xorl	%edx, %edx
.L1:
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	(%rdi), %esi
	movl	$128, %ebp
.L7:
	movl	%esi, %eax
	andl	$3, %eax
	cmpl	$2, %eax
	jne	.L10
	movl	%esi, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	je	.L10
	movl	%esi, %edx
	movl	%esi, %eax
	orl	$4, %edx
	lock cmpxchgl	%edx, (%rbx)
	movl	%eax, %esi
	je	.L6
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L56:
	cmpl	$75, %eax
	je	.L1
.L6:
	movl	(%rbx), %esi
	testb	$4, %sil
	je	.L7
	movl	28(%rbx), %r8d
	movq	%rbx, %rdi
	testl	%r8d, %r8d
	cmovne	%ebp, %r8d
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %edx
	jne	.L56
	popq	%rbx
	movl	%edx, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	%eax, %edx
	andl	$3, %edx
	cmpl	$1, %edx
	jne	.L57
	movl	%eax, %edx
	xorl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L13
	leaq	8(%rbx), %rdi
	xorl	%eax, %eax
	xchgl	(%rdi), %eax
	testb	$2, %al
	je	.L32
	cmpl	$1, 28(%rbx)
	movl	$2147483647, %edx
	movl	$202, %eax
	sbbl	%esi, %esi
	xorl	%r10d, %r10d
	andl	$128, %esi
	addl	$1, %esi
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L32
	cmpl	$-22, %eax
	je	.L32
	cmpl	$-14, %eax
	je	.L32
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L29:
	movl	$35, %edx
	movl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L12:
	leal	-8(%rax), %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L4
	movl	$11, %edx
	jmp	.L1
.L57:
	leaq	8(%rbx), %rbp
	xorl	%r12d, %r12d
.L18:
	movl	0(%rbp), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$3, %edx
	je	.L27
	testb	%r12b, %r12b
	jne	.L32
	movl	(%rbx), %eax
	testb	$1, %al
	jne	.L18
	movl	0(%rbp), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$3, %edx
	jne	.L32
	movl	$1, %r12d
.L27:
	movl	28(%rbx), %r8d
	movl	$128, %edx
	testl	%r8d, %r8d
	cmovne	%edx, %r8d
	testb	$2, %al
	jne	.L23
	movl	$3, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	jne	.L18
.L23:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movl	$3, %esi
	movq	%rbp, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %edx
	je	.L21
	cmpl	$75, %eax
	jne	.L18
.L21:
	movl	(%rbx), %eax
.L24:
	testb	$1, %al
	je	.L26
	leal	-8(%rax), %ecx
	lock cmpxchgl	%ecx, (%rbx)
	je	.L1
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L26:
	movl	0(%rbp), %eax
	orl	$2, %eax
	cmpl	$3, %eax
	je	.L26
	jmp	.L32
	.size	__pthread_rwlock_rdlock, .-__pthread_rwlock_rdlock
	.weak	pthread_rwlock_rdlock
	.set	pthread_rwlock_rdlock,__pthread_rwlock_rdlock
