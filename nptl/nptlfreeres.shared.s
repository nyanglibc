	.text
	.p2align 4,,15
	.globl	__libpthread_freeres
	.type	__libpthread_freeres, @function
__libpthread_freeres:
	subq	$8, %rsp
	call	__default_pthread_attr_freeres
	call	__nptl_stacks_freeres
	call	__shm_directory_freeres
	addq	$8, %rsp
	jmp	__nptl_unwind_freeres
	.size	__libpthread_freeres, .-__libpthread_freeres
	.hidden	__nptl_unwind_freeres
	.hidden	__shm_directory_freeres
	.hidden	__nptl_stacks_freeres
	.hidden	__default_pthread_attr_freeres
