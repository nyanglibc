	.text
	.p2align 4,,15
	.globl	__libc_msgrcv
	.type	__libc_msgrcv, @function
__libc_msgrcv:
	movq	%rcx, %r10
#APP
# 27 "../sysdeps/unix/sysv/linux/msgrcv.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$70, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/msgrcv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movl	%r8d, %r13d
	pushq	%rbx
	movq	%rcx, %r12
	movq	%rdx, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__pthread_enable_asynccancel@PLT
	movl	%r13d, %r8d
	movl	%eax, %r9d
	movq	%r12, %r10
	movq	%rbp, %rdx
	movq	%r14, %rsi
	movl	%ebx, %edi
	movl	$70, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/msgrcv.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r9d, %edi
	movq	%rax, 8(%rsp)
	call	__pthread_disable_asynccancel@PLT
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_msgrcv, .-__libc_msgrcv
	.weak	msgrcv
	.set	msgrcv,__libc_msgrcv
