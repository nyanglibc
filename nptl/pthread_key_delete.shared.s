	.text
	.p2align 4,,15
	.globl	__pthread_key_delete
	.type	__pthread_key_delete, @function
__pthread_key_delete:
	cmpl	$1023, %edi
	ja	.L4
	movl	%edi, %edi
	leaq	__GI___pthread_keys(%rip), %rax
	salq	$4, %rdi
	addq	%rax, %rdi
	movq	(%rdi), %rdx
	testb	$1, %dl
	je	.L4
	movl	%edx, %eax
	addl	$1, %edx
	lock cmpxchgq	%rdx, (%rdi)
	jne	.L4
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$22, %eax
	ret
	.size	__pthread_key_delete, .-__pthread_key_delete
	.weak	pthread_key_delete
	.set	pthread_key_delete,__pthread_key_delete
