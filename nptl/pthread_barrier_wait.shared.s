	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_barrier_wait
	.type	__pthread_barrier_wait, @function
__pthread_barrier_wait:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r9
	pushq	%r13
	pushq	%r12
	movl	$1, %r13d
	pushq	%rbp
	pushq	%rbx
	movl	$2147483647, %r12d
	movl	$202, %ebp
	subq	$8, %rsp
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$1, %r8d
	lock xaddl	%r8d, (%r9)
	movl	8(%r9), %r14d
	xorl	%edx, %edx
	movl	%r12d, %eax
	movl	%r12d, %ebx
	addl	$1, %r8d
	divl	%r14d
	subl	%edx, %ebx
	cmpl	%ebx, %r8d
	ja	.L8
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L4:
	movl	(%r9), %r8d
	cmpl	%r8d, %ebx
	jnb	.L2
.L8:
	movl	12(%r9), %esi
	xorl	%r10d, %r10d
	movl	%r8d, %edx
	movq	%r9, %rdi
	movl	%ebp, %eax
	xorb	$-128, %sil
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L4
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L6
	movq	%r13, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L4
.L6:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
.L3:
	movl	4(%r9), %ecx
	leaq	4(%r9), %r12
.L9:
	leal	(%r14,%rcx), %eax
	cmpl	%r8d, %eax
	ja	.L30
	movl	%r8d, %eax
	xorl	%edx, %edx
	movl	%r8d, %ebp
	divl	%r14d
	movl	%ecx, %eax
	subl	%edx, %ebp
	lock cmpxchgl	%ebp, (%r12)
	movl	%eax, %ecx
	jne	.L9
	movl	12(%r9), %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movq	%r12, %rdi
	movl	$202, %eax
	xorb	$-127, %sil
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L31
.L10:
	cmpl	%ebp, %r8d
	jbe	.L11
.L12:
	movl	$202, %r13d
	movl	$1, %r15d
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L15:
	movl	(%r12), %ebp
	cmpl	%ebp, %r8d
	jbe	.L11
.L16:
	movl	12(%r9), %esi
	xorl	%r10d, %r10d
	movl	%ebp, %edx
	movq	%r12, %rdi
	movl	%r13d, %eax
	xorb	$-128, %sil
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L15
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L6
	movq	%r15, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	je	.L6
	movl	(%r12), %ebp
	cmpl	%ebp, %r8d
	ja	.L16
.L11:
	leaq	16(%r9), %rdx
	movl	$1, %eax
	lock xaddl	%eax, (%rdx)
	addl	$1, %eax
	cmpl	%eax, %ebx
	je	.L32
.L19:
	xorl	%edx, %edx
	movl	%r8d, %eax
	divl	%r14d
	xorl	%eax, %eax
	testl	%edx, %edx
	sete	%al
	addq	$8, %rsp
	popq	%rbx
	negl	%eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L31:
	cmpl	$-22, %eax
	je	.L10
	cmpl	$-14, %eax
	je	.L10
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L32:
	movl	$0, 4(%r9)
	movl	$0, 16(%r9)
	movl	12(%r9), %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movl	$0, (%r9)
	movq	%r9, %rdi
	movl	$202, %eax
	xorb	$-127, %sil
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L19
	cmpl	$-22, %eax
	je	.L19
	cmpl	$-14, %eax
	je	.L19
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L30:
	cmpl	%ecx, %r8d
	movl	%ecx, %ebp
	ja	.L12
	jmp	.L11
	.size	__pthread_barrier_wait, .-__pthread_barrier_wait
	.weak	pthread_barrier_wait
	.set	pthread_barrier_wait,__pthread_barrier_wait
