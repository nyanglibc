	.text
	.p2align 4,,15
	.globl	__close
	.type	__close, @function
__close:
#APP
# 27 "../sysdeps/unix/sysv/linux/close.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$3, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/close.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__pthread_enable_asynccancel
	movl	%ebx, %edi
	movl	%eax, %edx
	movl	$3, %eax
#APP
# 27 "../sysdeps/unix/sysv/linux/close.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%edx, %edi
	movl	%eax, 12(%rsp)
	call	__pthread_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	errno@gottpoff(%rip), %rcx
	negl	%eax
	movl	%eax, %fs:(%rcx)
	movl	$-1, %eax
	jmp	.L6
	.size	__close, .-__close
	.weak	close
	.set	close,__close
	.globl	__libc_close
	.set	__libc_close,__close
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
