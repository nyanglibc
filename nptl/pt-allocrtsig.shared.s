	.text
	.p2align 4,,15
	.globl	__libc_current_sigrtmin
	.type	__libc_current_sigrtmin, @function
__libc_current_sigrtmin:
	jmp	__libc_current_sigrtmin_private@PLT
	.size	__libc_current_sigrtmin, .-__libc_current_sigrtmin
	.p2align 4,,15
	.globl	__libc_current_sigrtmax
	.type	__libc_current_sigrtmax, @function
__libc_current_sigrtmax:
	jmp	__libc_current_sigrtmax_private@PLT
	.size	__libc_current_sigrtmax, .-__libc_current_sigrtmax
	.p2align 4,,15
	.globl	__libc_allocate_rtsig
	.type	__libc_allocate_rtsig, @function
__libc_allocate_rtsig:
	jmp	__libc_allocate_rtsig_private@PLT
	.size	__libc_allocate_rtsig, .-__libc_allocate_rtsig
