	.text
	.p2align 4,,15
	.globl	pthread_getconcurrency
	.type	pthread_getconcurrency, @function
pthread_getconcurrency:
	movl	__concurrency_level(%rip), %eax
	ret
	.size	pthread_getconcurrency, .-pthread_getconcurrency
