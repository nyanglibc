	.text
	.p2align 4,,15
	.globl	pthread_rwlockattr_init
	.type	pthread_rwlockattr_init, @function
pthread_rwlockattr_init:
	movq	$0, (%rdi)
	xorl	%eax, %eax
	ret
	.size	pthread_rwlockattr_init, .-pthread_rwlockattr_init
