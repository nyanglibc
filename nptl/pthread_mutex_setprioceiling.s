	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	pthread_mutex_setprioceiling
	.type	pthread_mutex_setprioceiling, @function
pthread_mutex_setprioceiling:
	movl	16(%rdi), %eax
	testb	$64, %al
	je	.L31
	pushq	%r15
	pushq	%r14
	leaq	16(%rdi), %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	__sched_fifo_min_prio(%rip), %eax
	cmpl	$-1, %eax
	je	.L5
	movl	__sched_fifo_max_prio(%rip), %eax
	cmpl	$-1, %eax
	je	.L5
.L4:
	movl	__sched_fifo_min_prio(%rip), %eax
	cmpl	%ebp, %eax
	jg	.L2
	movl	__sched_fifo_max_prio(%rip), %eax
	cmpl	%eax, %ebp
	jg	.L2
	testl	$-4096, %ebp
	jne	.L2
	movl	16(%rbx), %eax
#APP
# 55 "pthread_mutex_setprioceiling.c" 1
	movl %fs:720,%edx
# 0 "" 2
#NO_APP
	cmpl	%edx, 8(%rbx)
	je	.L6
	movl	(%rbx), %r9d
.L7:
	andl	$-524288, %r9d
	movl	%r9d, %r15d
	movl	%r9d, %eax
	orl	$1, %r15d
	lock cmpxchgl	%r15d, (%rbx)
	cmpl	%eax, %r9d
	movl	%eax, %r8d
	je	.L10
	movl	%r9d, %r12d
	orl	$2, %r12d
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L11:
	movl	%r9d, %eax
	lock cmpxchgl	%r12d, (%rbx)
	cmpl	%r9d, %eax
	je	.L10
.L13:
	movl	%r15d, %eax
	lock cmpxchgl	%r12d, (%rbx)
	movl	%eax, %r8d
	andl	$-524288, %eax
	cmpl	%eax, %r9d
	jne	.L10
	cmpl	%r8d, %r9d
	je	.L11
	movl	(%r14), %esi
	xorl	%r10d, %r10d
	movl	%r12d, %edx
	movq	%rbx, %rdi
	movl	$202, %eax
	notl	%esi
	andl	$128, %esi
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L11
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L12
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L11
.L12:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L5:
	call	__init_sched_fifo_prio@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L10:
	xorl	%esi, %esi
	testq	%r13, %r13
	je	.L14
	shrl	$19, %r8d
	xorl	%esi, %esi
	movl	%r8d, 0(%r13)
.L14:
	sall	$19, %ebp
	orl	%esi, %ebp
	movl	%ebp, (%rbx)
#APP
# 117 "pthread_mutex_setprioceiling.c" 1
	lock;orl $0, (%rsp)
# 0 "" 2
#NO_APP
	movl	16(%rbx), %esi
	movl	$202, %ecx
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movq	%rbx, %rdi
	movl	%ecx, %eax
	andl	$128, %esi
	xorb	$-127, %sil
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	%rax, %rdx
	xorl	%eax, %eax
	cmpq	$-4096, %rdx
	ja	.L35
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$8, %rsp
	movl	$22, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L31:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	andl	$127, %eax
	cmpl	$66, %eax
	je	.L18
	cmpl	$65, %eax
	movl	(%rbx), %r9d
	jne	.L7
	shrl	$19, %r9d
	movl	%ebp, %esi
	movl	%r9d, %edi
	movl	%r9d, %r12d
	call	__pthread_tpp_change_priority@PLT
	testl	%eax, %eax
	jne	.L1
	testq	%r13, %r13
	je	.L17
	movl	%r12d, 0(%r13)
.L17:
	movl	(%rbx), %esi
	andl	$524287, %esi
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L35:
	cmpl	$-22, %edx
	je	.L20
	cmpl	$-14, %edx
	jne	.L12
.L20:
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$35, %eax
	jmp	.L1
	.size	pthread_mutex_setprioceiling, .-pthread_mutex_setprioceiling
