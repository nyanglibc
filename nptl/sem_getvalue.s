	.text
	.p2align 4,,15
	.globl	__new_sem_getvalue
	.type	__new_sem_getvalue, @function
__new_sem_getvalue:
	movq	(%rdi), %rax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__new_sem_getvalue, .-__new_sem_getvalue
	.weak	sem_getvalue
	.set	sem_getvalue,__new_sem_getvalue
