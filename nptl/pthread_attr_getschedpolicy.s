	.text
	.p2align 4,,15
	.globl	__pthread_attr_getschedpolicy
	.type	__pthread_attr_getschedpolicy, @function
__pthread_attr_getschedpolicy:
	movl	4(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getschedpolicy, .-__pthread_attr_getschedpolicy
	.globl	pthread_attr_getschedpolicy
	.set	pthread_attr_getschedpolicy,__pthread_attr_getschedpolicy
