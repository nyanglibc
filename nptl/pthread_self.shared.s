	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	pthread_self
	.type	pthread_self, @function
pthread_self:
	movq	%fs:16, %rax
	ret
	.size	pthread_self, .-pthread_self
