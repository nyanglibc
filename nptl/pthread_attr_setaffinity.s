	.text
	.p2align 4,,15
	.globl	__pthread_attr_setaffinity_np
	.hidden	__pthread_attr_setaffinity_np
	.type	__pthread_attr_setaffinity_np, @function
__pthread_attr_setaffinity_np:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L8
	testq	%rsi, %rsi
	movq	%rsi, %r12
	jne	.L2
.L8:
	movq	40(%rbx), %rax
	xorl	%ebp, %ebp
	testq	%rax, %rax
	je	.L1
	movq	(%rax), %rdi
	call	free@PLT
	movq	40(%rbx), %rax
	movq	$0, (%rax)
	movq	$0, 8(%rax)
.L1:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movq	%rdx, %r13
	call	__pthread_attr_extension
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L1
	movq	40(%rbx), %rax
	cmpq	%r12, 8(%rax)
	movq	(%rax), %rdi
	je	.L5
	movq	%r12, %rsi
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	je	.L7
	movq	40(%rbx), %rax
	movq	%rdi, (%rax)
	movq	%r12, 8(%rax)
.L5:
	movq	%r12, %rdx
	movq	%r13, %rsi
	call	memcpy@PLT
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	movl	$12, %ebp
	jmp	.L1
	.size	__pthread_attr_setaffinity_np, .-__pthread_attr_setaffinity_np
	.weak	pthread_attr_setaffinity_np
	.set	pthread_attr_setaffinity_np,__pthread_attr_setaffinity_np
	.hidden	__pthread_attr_extension
