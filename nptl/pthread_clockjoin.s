	.text
	.p2align 4,,15
	.globl	__pthread_clockjoin_np
	.type	__pthread_clockjoin_np, @function
__pthread_clockjoin_np:
	cmpl	$1, %edx
	jbe	.L4
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %r8d
	jmp	__pthread_clockjoin_ex@PLT
	.size	__pthread_clockjoin_np, .-__pthread_clockjoin_np
	.weak	pthread_clockjoin_np
	.set	pthread_clockjoin_np,__pthread_clockjoin_np
