	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_setschedpolicy
	.type	__pthread_attr_setschedpolicy, @function
__pthread_attr_setschedpolicy:
	cmpl	$2, %esi
	movl	$22, %eax
	ja	.L1
	orl	$64, 8(%rdi)
	movl	%esi, 4(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__pthread_attr_setschedpolicy, .-__pthread_attr_setschedpolicy
	.globl	pthread_attr_setschedpolicy
	.set	pthread_attr_setschedpolicy,__pthread_attr_setschedpolicy
