	.text
#APP
	.symver fcntl_alias,fcntl@GLIBC_2.2.5
	.symver __fcntl_alias,__fcntl@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	fcntl_compat, @function
fcntl_compat:
	subq	$88, %rsp
	leaq	96(%rsp), %rax
	movq	%rdx, 48(%rsp)
	movl	$16, 8(%rsp)
	movq	%rax, 16(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	xorl	%eax, %eax
	call	__libc_fcntl64@PLT
	addq	$88, %rsp
	ret
	.size	fcntl_compat, .-fcntl_compat
	.weak	__fcntl_alias
	.set	__fcntl_alias,fcntl_compat
	.weak	fcntl_alias
	.set	fcntl_alias,fcntl_compat
