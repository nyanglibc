.globl pthread_spin_lock
.type pthread_spin_lock,@function
.align 1<<4
pthread_spin_lock:
1: LOCK
 decl 0(%rdi)
 jne 2f
 xor %eax, %eax
 ret
 .align 16
2: rep
 nop
 cmpl $0, 0(%rdi)
 jg 1b
 jmp 2b
.size pthread_spin_lock,.-pthread_spin_lock
