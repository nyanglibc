	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_timedwrlock
	.type	__pthread_rwlock_timedwrlock, @function
__pthread_rwlock_timedwrlock:
	testq	%rsi, %rsi
	je	.L2
	cmpq	$999999999, 8(%rsi)
	ja	.L48
.L2:
	movl	24(%rdi), %edx
#APP
# 603 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, %edx
	je	.L49
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	(%rdi), %eax
.L4:
	movl	%eax, %ecx
	movl	%eax, %edx
	orl	$2, %ecx
	lock cmpxchgl	%ecx, (%rdi)
	jne	.L4
	testb	$2, %dl
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	leaq	12(%rdi), %r12
	movl	$1, %eax
	jne	.L117
.L5:
	testb	$1, %dl
	movl	%eax, 12(%rbx)
	jne	.L23
.L21:
	movl	%edx, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	jne	.L26
	movl	%edx, %ecx
	movl	%edx, %eax
	orl	$1, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	movl	%eax, %edx
	jne	.L22
	movl	$1, 8(%rbx)
.L23:
#APP
# 947 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	movl	%eax, 24(%rbx)
	xorl	%r8d, %r8d
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L22:
	testb	$1, %al
	je	.L21
	.p2align 4,,10
	.p2align 3
.L26:
	leaq	8(%rbx), %r15
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L24:
	movl	(%r15), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$2, %edx
	je	.L46
	testb	%r13b, %r13b
	jne	.L23
	movl	(%rbx), %eax
	testb	$1, %al
	je	.L24
	movl	(%r15), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$2, %edx
	jne	.L23
	movl	$1, %r13d
.L46:
	movl	28(%rbx), %r14d
	movl	$128, %edx
	testl	%r14d, %r14d
	cmovne	%edx, %r14d
	testb	$2, %al
	jne	.L31
	movl	$2, %edx
	lock cmpxchgl	%edx, (%r15)
	jne	.L24
.L31:
	movl	%r14d, %r8d
	xorl	%edx, %edx
	movq	%rbp, %rcx
	movl	$2, %esi
	movq	%r15, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %r8d
	je	.L29
	cmpl	$75, %eax
	jne	.L24
.L29:
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.L33
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	je	.L33
	xorl	%ecx, %ecx
	xchgl	(%r12), %ecx
.L37:
	movl	%eax, %esi
	orl	$-2147483648, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L35
	andl	$2, %ecx
	je	.L1
	movl	%r14d, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L1
	cmpl	$-22, %eax
	je	.L1
	cmpl	$-14, %eax
	je	.L1
.L36:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
.L35:
	testl	%eax, %eax
	jne	.L37
	movl	%ecx, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L33:
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$1, %edx
	jne	.L45
	xchgl	(%r12), %edx
.L44:
	movl	%eax, %ecx
	xorl	$2, %ecx
	andl	$-5, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	movl	%eax, %r8d
	jne	.L39
	andl	$2, %edx
	je	.L41
	movl	%r14d, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%r12, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L118
.L41:
	andl	$4, %r8d
	je	.L53
	movl	%r14d, %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	xorb	$-127, %sil
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L119
.L53:
	movl	$110, %r8d
	jmp	.L1
.L39:
	testb	$1, %al
	je	.L44
	movl	%edx, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L45:
	movl	(%r15), %eax
	orl	$2, %eax
	cmpl	$2, %eax
	jne	.L23
	movl	(%r15), %eax
	orl	$2, %eax
	cmpl	$2, %eax
	je	.L45
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L49:
	movl	$35, %r8d
.L112:
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movl	$22, %r8d
	jmp	.L112
	.p2align 4,,10
	.p2align 3
.L117:
	movl	48(%rdi), %r13d
	testl	%r13d, %r13d
	jne	.L120
.L6:
	xorl	%ecx, %ecx
	movl	$128, %r14d
.L7:
	testb	$2, %dl
	jne	.L8
	movl	%edx, %esi
	movl	%edx, %eax
	orl	$2, %esi
	lock cmpxchgl	%esi, (%rbx)
	movl	%eax, %edx
	jne	.L7
	testl	%r13d, %r13d
	jne	.L121
.L10:
	orl	$2, %edx
	cmpb	$1, %cl
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$3, %eax
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%r13d, %r13d
	jne	.L122
.L11:
	movl	28(%rbx), %r8d
	movl	(%r12), %eax
	testl	%r8d, %r8d
	movl	%eax, %edx
	cmovne	%r14d, %r8d
	andl	$-3, %edx
	cmpl	$1, %edx
	jne	.L15
	cmpl	$3, %eax
	je	.L14
	movl	$3, %edx
	lock cmpxchgl	%edx, (%r12)
	jne	.L15
.L14:
	xorl	%edx, %edx
	movq	%rbp, %rcx
	movl	$3, %esi
	movq	%r12, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %r8d
	je	.L55
	cmpl	$75, %eax
	je	.L55
	movl	(%rbx), %edx
	movl	$1, %ecx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L122:
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rsi
	testl	%eax, %eax
	jns	.L11
	leal	2147483647(%rax), %edi
	lock cmpxchgl	%edi, (%rsi)
	jne	.L7
	movl	(%rbx), %edx
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L121:
	lock subl	$1, 4(%rbx)
	jmp	.L10
	.p2align 4,,10
	.p2align 3
.L15:
	movl	(%rbx), %edx
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L120:
	lock addl	$1, 4(%rdi)
	jmp	.L6
.L119:
	cmpl	$-22, %eax
	je	.L53
	cmpl	$-14, %eax
	je	.L53
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L118:
	cmpl	$-22, %eax
	je	.L41
	cmpl	$-14, %eax
	je	.L41
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L55:
	testl	%r13d, %r13d
	je	.L1
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
.L19:
	cmpl	$-2147483647, %eax
	je	.L51
	leal	-1(%rax), %ecx
.L18:
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L19
	cmpl	$-2147483647, %eax
	jne	.L1
	movl	(%rbx), %edx
	movl	$3, %eax
	orl	$2, %edx
	jmp	.L5
.L51:
	xorl	%ecx, %ecx
	jmp	.L18
	.size	__pthread_rwlock_timedwrlock, .-__pthread_rwlock_timedwrlock
	.weak	pthread_rwlock_timedwrlock
	.set	pthread_rwlock_timedwrlock,__pthread_rwlock_timedwrlock
