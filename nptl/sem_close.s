	.text
	.p2align 4,,15
	.type	walker, @function
walker:
	movq	(%rdi), %rax
	movq	(%rdx), %rcx
	cmpq	%rcx, 24(%rax)
	je	.L4
	rep ret
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rax, 8(%rdx)
	ret
	.size	walker, .-walker
	.p2align 4,,15
	.globl	sem_close
	.type	sem_close, @function
sem_close:
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	$1, %edx
	subq	$24, %rsp
	lock cmpxchgl	%edx, __sem_mappings_lock(%rip)
	jne	.L12
.L6:
	movq	__sem_mappings(%rip), %rdi
	leaq	walker(%rip), %rsi
	movq	%rsp, %rdx
	movq	%rbx, (%rsp)
	movq	$0, 8(%rsp)
	call	__twalk_r@PLT
	movq	8(%rsp), %rbx
	testq	%rbx, %rbx
	je	.L7
	movl	16(%rbx), %eax
	xorl	%ebp, %ebp
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 16(%rbx)
	je	.L13
.L8:
	xorl	%eax, %eax
#APP
# 78 "../sysdeps/pthread/sem_close.c" 1
	xchgl %eax, __sem_mappings_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L14
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	__sem_search@GOTPCREL(%rip), %rdx
	leaq	__sem_mappings(%rip), %rsi
	movq	%rbx, %rdi
	call	__tdelete@PLT
	movq	24(%rbx), %rdi
	movl	$32, %esi
	call	munmap@PLT
	movq	%rbx, %rdi
	movl	%eax, %ebp
	call	free@PLT
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L12:
	leaq	__sem_mappings_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L14:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__sem_mappings_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 78 "../sysdeps/pthread/sem_close.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
.L7:
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	movl	$22, %fs:(%rax)
	jmp	.L8
	.size	sem_close, .-sem_close
