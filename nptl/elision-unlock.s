	.text
	.p2align 4,,15
	.globl	__lll_unlock_elision
	.type	__lll_unlock_elision, @function
__lll_unlock_elision:
	movl	(%rdi), %eax
	testl	%eax, %eax
	je	.L5
	xorl	%eax, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/x86/elision-unlock.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L6
.L3:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xend
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	xorb	$-127, %sil
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$202, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/x86/elision-unlock.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L3
	.size	__lll_unlock_elision, .-__lll_unlock_elision
