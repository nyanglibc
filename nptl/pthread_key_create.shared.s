	.text
	.p2align 4,,15
	.globl	__GI___pthread_key_create
	.hidden	__GI___pthread_key_create
	.type	__GI___pthread_key_create, @function
__GI___pthread_key_create:
	leaq	__GI___pthread_keys(%rip), %r8
	xorl	%ecx, %ecx
	movq	%r8, %rdx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L2:
	addq	$1, %rcx
	addq	$16, %rdx
	cmpq	$1024, %rcx
	je	.L16
.L4:
	movq	(%rdx), %rax
	testb	$1, %al
	jne	.L2
	cmpq	$-2, %rax
	je	.L2
	leaq	1(%rax), %r9
	lock cmpxchgq	%r9, (%rdx)
	jne	.L2
	movq	%rcx, %rax
	salq	$4, %rax
	movq	%rsi, 8(%r8,%rax)
	movl	%ecx, (%rdi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$11, %eax
	ret
	.size	__GI___pthread_key_create, .-__GI___pthread_key_create
	.weak	pthread_key_create
	.set	pthread_key_create,__GI___pthread_key_create
	.globl	__pthread_key_create
	.set	__pthread_key_create,__GI___pthread_key_create
