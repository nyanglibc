	.text
	.p2align 4,,15
	.globl	pthread_mutexattr_gettype
	.type	pthread_mutexattr_gettype, @function
pthread_mutexattr_gettype:
	movl	(%rdi), %eax
	andl	$251661823, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_mutexattr_gettype, .-pthread_mutexattr_gettype
	.weak	pthread_mutexattr_getkind_np
	.set	pthread_mutexattr_getkind_np,pthread_mutexattr_gettype
