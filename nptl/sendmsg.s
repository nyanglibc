	.text
	.p2align 4,,15
	.globl	__libc_sendmsg
	.type	__libc_sendmsg, @function
__libc_sendmsg:
#APP
# 28 "../sysdeps/unix/sysv/linux/sendmsg.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$46, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/sendmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L9
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%r12
	pushq	%rbp
	movl	%edx, %r12d
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__pthread_enable_asynccancel@PLT
	movl	%r12d, %edx
	movl	%eax, %r8d
	movq	%rbp, %rsi
	movl	%ebx, %edi
	movl	$46, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/sendmsg.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L10
.L4:
	movl	%r8d, %edi
	movq	%rax, 8(%rsp)
	call	__pthread_disable_asynccancel@PLT
	movq	8(%rsp), %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	ret
.L10:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movq	$-1, %rax
	jmp	.L4
	.size	__libc_sendmsg, .-__libc_sendmsg
	.weak	__sendmsg
	.set	__sendmsg,__libc_sendmsg
	.weak	sendmsg
	.set	sendmsg,__libc_sendmsg
