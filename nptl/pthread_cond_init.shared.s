	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __pthread_cond_init,pthread_cond_init@@GLIBC_2.3.2
#NO_APP
	.p2align 4,,15
	.globl	__GI___pthread_cond_init
	.hidden	__GI___pthread_cond_init
	.type	__GI___pthread_cond_init, @function
__GI___pthread_cond_init:
	pxor	%xmm0, %xmm0
	testq	%rsi, %rsi
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	je	.L10
	movl	(%rsi), %eax
	testb	$1, %al
	je	.L4
	orl	$1, 36(%rdi)
.L4:
	testb	$2, %al
	je	.L10
	orl	$2, 36(%rdi)
.L10:
	xorl	%eax, %eax
	ret
	.size	__GI___pthread_cond_init, .-__GI___pthread_cond_init
	.globl	__pthread_cond_init
	.set	__pthread_cond_init,__GI___pthread_cond_init
