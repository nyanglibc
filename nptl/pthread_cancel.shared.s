	.text
	.p2align 4,,15
	.globl	__pthread_cancel
	.type	__pthread_cancel, @function
__pthread_cancel:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	jle	.L8
	pushq	%rbx
	movq	%rdi, %rbx
	call	pthread_cancel_init
	leaq	776(%rbx), %rcx
.L3:
	movl	776(%rbx), %eax
	movl	%eax, %edx
	orl	$12, %edx
	cmpl	%edx, %eax
	je	.L7
	movl	%edx, %esi
	andl	$-69, %esi
	cmpl	$10, %esi
	je	.L17
#APP
# 82 "pthread_cancel.c" 1
	movl $1,%fs:24
# 0 "" 2
#NO_APP
	movq	__libc_multiple_threads_ptr(%rip), %rsi
	movl	$1, (%rsi)
	movl	$1, __pthread_multiple_threads(%rip)
	lock cmpxchgl	%edx, (%rcx)
	jne	.L3
.L7:
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L17:
	movl	%eax, %edx
	orl	$4, %edx
	lock cmpxchgl	%edx, (%rcx)
	jne	.L3
	call	__getpid@PLT
	movl	720(%rbx), %esi
	movl	%eax, %edi
	movl	$32, %edx
	movl	$234, %eax
#APP
# 70 "pthread_cancel.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L7
	negl	%eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$3, %eax
	ret
	.size	__pthread_cancel, .-__pthread_cancel
	.weak	pthread_cancel
	.set	pthread_cancel,__pthread_cancel
	.hidden	__pthread_multiple_threads
	.hidden	__libc_multiple_threads_ptr
	.hidden	pthread_cancel_init
