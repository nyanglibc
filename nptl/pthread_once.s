	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	__pthread_once_slow, @function
__pthread_once_slow:
	pushq	%r12
	pushq	%rbp
	movl	$202, %r8d
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	$1, %r9d
	subq	$32, %rsp
.L13:
	movl	(%rbx), %eax
.L3:
	testb	$2, %al
	je	.L19
.L7:
	addq	$32, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L19:
	movl	__fork_generation(%rip), %edx
	orl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L3
	cmpl	%eax, %edx
	jne	.L4
	testb	$1, %al
	je	.L4
	xorl	%r10d, %r10d
	movl	$128, %esi
	movq	%rbx, %rdi
	movl	%r8d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L13
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L6
	movq	%r9, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L13
.L6:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L4:
	movq	%rsp, %r12
	leaq	clear_once_control(%rip), %rsi
	movq	%rbx, %rdx
	movq	%r12, %rdi
	call	__pthread_cleanup_push@PLT
	call	*%rbp
	xorl	%esi, %esi
	movq	%r12, %rdi
	call	__pthread_cleanup_pop@PLT
	movl	$2, (%rbx)
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movl	$129, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L7
	cmpl	$-22, %eax
	je	.L7
	cmpl	$-14, %eax
	je	.L7
	jmp	.L6
	.size	__pthread_once_slow, .-__pthread_once_slow
	.p2align 4,,15
	.type	clear_once_control, @function
clear_once_control:
	movl	$0, (%rdi)
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L26
.L20:
	rep ret
	.p2align 4,,10
	.p2align 3
.L26:
	cmpl	$-22, %eax
	je	.L20
	cmpl	$-14, %eax
	je	.L20
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	clear_once_control, .-clear_once_control
	.p2align 4,,15
	.globl	__pthread_once
	.type	__pthread_once, @function
__pthread_once:
	movl	(%rdi), %eax
	testb	$2, %al
	je	.L29
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	jmp	__pthread_once_slow
	.size	__pthread_once, .-__pthread_once
	.weak	pthread_once
	.set	pthread_once,__pthread_once
	.comm	__fork_generation,8,8
