	.text
	.p2align 4,,15
	.globl	__pthread_attr_getstack
	.type	__pthread_attr_getstack, @function
__pthread_attr_getstack:
	movq	32(%rdi), %rcx
	movq	24(%rdi), %rax
	subq	%rcx, %rax
	movq	%rax, (%rsi)
	movq	%rcx, (%rdx)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getstack, .-__pthread_attr_getstack
	.globl	pthread_attr_getstack
	.set	pthread_attr_getstack,__pthread_attr_getstack
