	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__thrd_sleep
	.type	__thrd_sleep, @function
__thrd_sleep:
	subq	$8, %rsp
	movq	%rdi, %rdx
	movq	%rsi, %rcx
	xorl	%edi, %edi
	xorl	%esi, %esi
	call	__GI___clock_nanosleep
	testl	%eax, %eax
	je	.L1
	cmpl	$4, %eax
	sete	%al
	movzbl	%al, %eax
	subl	$2, %eax
.L1:
	addq	$8, %rsp
	ret
	.size	__thrd_sleep, .-__thrd_sleep
	.weak	thrd_sleep
	.set	thrd_sleep,__thrd_sleep
