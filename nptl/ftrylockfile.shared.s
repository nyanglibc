	.text
	.p2align 4,,15
	.globl	__ftrylockfile
	.type	__ftrylockfile, @function
__ftrylockfile:
	movq	136(%rdi), %rdx
	movq	%fs:16, %rcx
	cmpq	%rcx, 8(%rdx)
	je	.L2
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L4
	movq	136(%rdi), %rax
	movq	%rcx, 8(%rax)
	movl	$1, 4(%rax)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	addl	$1, 4(%rdx)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$16, %eax
	ret
	.size	__ftrylockfile, .-__ftrylockfile
	.weak	ftrylockfile
	.set	ftrylockfile,__ftrylockfile
	.globl	_IO_ftrylockfile
	.set	_IO_ftrylockfile,__ftrylockfile
