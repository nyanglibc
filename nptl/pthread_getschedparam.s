	.text
	.p2align 4,,15
	.globl	__pthread_getschedparam
	.type	__pthread_getschedparam, @function
__pthread_getschedparam:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	jle	.L15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
#APP
# 39 "pthread_getschedparam.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	leaq	1560(%rdi), %r13
	jne	.L3
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, 0(%r13)
# 0 "" 2
#NO_APP
.L4:
	movl	780(%rbx), %eax
	testb	$32, %al
	je	.L26
.L5:
	testb	$64, %al
	je	.L27
.L11:
	movl	1596(%rbx), %eax
	xorl	%r8d, %r8d
	movl	%eax, 0(%rbp)
	movl	1592(%rbx), %eax
	movl	%eax, (%r12)
.L8:
#APP
# 69 "pthread_getschedparam.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L9
	subl	$1, 1560(%rbx)
.L1:
	addq	$8, %rsp
	movl	%r8d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movl	720(%rbx), %edi
	call	__sched_getscheduler
	cmpl	$-1, %eax
	movl	%eax, 1596(%rbx)
	jne	.L28
.L24:
	movl	$1, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L26:
	movl	720(%rbx), %edi
	leaq	1592(%rbx), %rsi
	call	__sched_getparam
	testl	%eax, %eax
	je	.L29
	testb	$64, 780(%rbx)
	jne	.L24
	movl	720(%rbx), %edi
	call	__sched_getscheduler
	movl	%eax, 1596(%rbx)
	addl	$1, %eax
	je	.L24
	orl	$64, 780(%rbx)
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L29:
	movl	780(%rbx), %eax
	orl	$32, %eax
	movl	%eax, 780(%rbx)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L15:
	movl	$3, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 0(%r13)
	je	.L4
	movq	%r13, %rdi
	call	__lll_lock_wait_private
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L9:
	xorl	%eax, %eax
#APP
# 69 "pthread_getschedparam.c" 1
	xchgl %eax, 0(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%r13, %rdi
	movl	$202, %eax
#APP
# 69 "pthread_getschedparam.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L28:
	orl	$64, 780(%rbx)
	jmp	.L11
	.size	__pthread_getschedparam, .-__pthread_getschedparam
	.globl	pthread_getschedparam
	.set	pthread_getschedparam,__pthread_getschedparam
	.hidden	__lll_lock_wait_private
	.hidden	__sched_getparam
	.hidden	__sched_getscheduler
