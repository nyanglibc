	.text
	.p2align 4,,15
	.globl	__new_sem_init
	.type	__new_sem_init, @function
__new_sem_init:
	testl	%edx, %edx
	js	.L8
	movl	%edx, %eax
	testl	%esi, %esi
	movq	%rax, (%rdi)
	movl	$128, %eax
	cmovne	%eax, %esi
	xorl	%eax, %eax
	movl	%esi, 8(%rdi)
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__new_sem_init, .-__new_sem_init
	.weak	sem_init
	.set	sem_init,__new_sem_init
