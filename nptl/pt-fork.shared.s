	.text
#APP
	.symver fork_alias,fork@GLIBC_2.2.5
	.symver __fork_alias,__fork@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	fork_compat, @function
fork_compat:
	jmp	__libc_fork@PLT
	.size	fork_compat, .-fork_compat
	.globl	__fork_alias
	.set	__fork_alias,fork_compat
	.globl	fork_alias
	.set	fork_alias,fork_compat
