	.text
	.p2align 4,,15
	.globl	pthread_mutex_getprioceiling
	.type	pthread_mutex_getprioceiling, @function
pthread_mutex_getprioceiling:
	movl	16(%rdi), %eax
	testb	$64, %al
	je	.L3
	movl	(%rdi), %eax
	shrl	$19, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movl	$22, %eax
	ret
	.size	pthread_mutex_getprioceiling, .-pthread_mutex_getprioceiling
