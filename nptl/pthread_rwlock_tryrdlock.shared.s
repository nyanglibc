	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_tryrdlock
	.type	__pthread_rwlock_tryrdlock, @function
__pthread_rwlock_tryrdlock:
	movl	(%rdi), %eax
.L6:
	movl	%eax, %edx
	andl	$2, %edx
	testb	$1, %al
	jne	.L2
	testl	%edx, %edx
	je	.L3
	cmpl	$2, 48(%rdi)
	je	.L12
.L3:
	leal	8(%rax), %edx
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L2:
	testl	%edx, %edx
	jne	.L12
	leal	8(%rax), %edx
	xorl	$1, %edx
.L5:
	testl	%edx, %edx
	js	.L13
	lock cmpxchgl	%edx, (%rdi)
	jne	.L6
	testb	$1, %al
	jne	.L7
.L15:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	leaq	8(%rdi), %rcx
	xorl	%eax, %eax
	xchgl	(%rcx), %eax
	testb	$2, %al
	je	.L15
	cmpl	$1, 28(%rdi)
	movl	$2147483647, %edx
	movq	%rcx, %rdi
	movl	$202, %eax
	sbbl	%esi, %esi
	xorl	%r10d, %r10d
	andl	$128, %esi
	addl	$1, %esi
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L15
	cmpl	$-22, %eax
	je	.L15
	cmpl	$-14, %eax
	je	.L15
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L12:
	movl	$16, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movl	$11, %eax
	ret
	.size	__pthread_rwlock_tryrdlock, .-__pthread_rwlock_tryrdlock
	.globl	pthread_rwlock_tryrdlock
	.set	pthread_rwlock_tryrdlock,__pthread_rwlock_tryrdlock
