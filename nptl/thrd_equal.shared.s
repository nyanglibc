	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	thrd_equal
	.type	thrd_equal, @function
thrd_equal:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	sete	%al
	ret
	.size	thrd_equal, .-thrd_equal
