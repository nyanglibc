	.text
#APP
	.section .gnu.warning.pthread_attr_setstackaddr
	.previous
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_setstackaddr
	.type	__pthread_attr_setstackaddr, @function
__pthread_attr_setstackaddr:
	movq	%rsi, 24(%rdi)
	orl	$8, 8(%rdi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_setstackaddr, .-__pthread_attr_setstackaddr
	.globl	pthread_attr_setstackaddr
	.set	pthread_attr_setstackaddr,__pthread_attr_setstackaddr
	.section	.gnu.warning.pthread_attr_setstackaddr
	#,"a",@progbits
	.align 32
	.type	__evoke_link_warning_pthread_attr_setstackaddr, @object
	.size	__evoke_link_warning_pthread_attr_setstackaddr, 82
__evoke_link_warning_pthread_attr_setstackaddr:
	.string	"the use of `pthread_attr_setstackaddr' is deprecated, use `pthread_attr_setstack'"
