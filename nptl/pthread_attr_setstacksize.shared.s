	.text
	.p2align 4,,15
	.globl	__pthread_attr_setstacksize
	.type	__pthread_attr_setstacksize, @function
__pthread_attr_setstacksize:
	cmpq	$16383, %rsi
	movl	$22, %eax
	jbe	.L1
	movq	%rsi, 32(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__pthread_attr_setstacksize, .-__pthread_attr_setstacksize
	.globl	pthread_attr_setstacksize
	.set	pthread_attr_setstacksize,__pthread_attr_setstacksize
