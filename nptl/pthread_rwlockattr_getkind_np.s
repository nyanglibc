	.text
	.p2align 4,,15
	.globl	pthread_rwlockattr_getkind_np
	.type	pthread_rwlockattr_getkind_np, @function
pthread_rwlockattr_getkind_np:
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_rwlockattr_getkind_np, .-pthread_rwlockattr_getkind_np
