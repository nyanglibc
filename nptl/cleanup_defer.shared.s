	.text
	.p2align 4,,15
	.globl	__pthread_register_cancel_defer
	.type	__pthread_register_cancel_defer, @function
__pthread_register_cancel_defer:
	movq	%fs:16, %rcx
#APP
# 31 "cleanup_defer.c" 1
	movq %fs:768,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 72(%rdi)
#APP
# 32 "cleanup_defer.c" 1
	movq %fs:760,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 80(%rdi)
#APP
# 34 "cleanup_defer.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	addq	$776, %rcx
	testb	$2, %dl
	jne	.L3
.L2:
	sarl	%edx
	andl	$1, %edx
	movl	%edx, 88(%rdi)
#APP
# 57 "cleanup_defer.c" 1
	movq %rdi,%fs:768
# 0 "" 2
#NO_APP
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	%eax, %edx
	.p2align 4,,10
	.p2align 3
.L3:
	movl	%edx, %esi
	movl	%edx, %eax
	andl	$-3, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	je	.L2
	jmp	.L4
	.size	__pthread_register_cancel_defer, .-__pthread_register_cancel_defer
	.p2align 4,,15
	.globl	__pthread_unregister_cancel_restore
	.type	__pthread_unregister_cancel_restore, @function
__pthread_unregister_cancel_restore:
	movq	72(%rdi), %rax
#APP
# 68 "cleanup_defer.c" 1
	movq %rax,%fs:768
# 0 "" 2
#NO_APP
	movl	88(%rdi), %eax
	testl	%eax, %eax
	je	.L9
#APP
# 72 "cleanup_defer.c" 1
	movl %fs:776,%edx
# 0 "" 2
#NO_APP
	testb	$2, %dl
	jne	.L9
	movq	%fs:16, %rax
	leaq	776(%rax), %rcx
.L11:
	movl	%edx, %esi
	movl	%edx, %eax
	orl	$2, %esi
	lock cmpxchgl	%esi, (%rcx)
	cmpl	%eax, %edx
	jne	.L12
#APP
# 89 "cleanup_defer.c" 1
	movl %fs:776,%eax
# 0 "" 2
#NO_APP
	andl	$-71, %eax
	cmpl	$8, %eax
	je	.L16
.L9:
	rep ret
.L16:
	subq	$8, %rsp
#APP
# 89 "cleanup_defer.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
#APP
# 304 "pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__GI___pthread_unwind
	.p2align 4,,10
	.p2align 3
.L12:
	movl	%eax, %edx
	jmp	.L11
	.size	__pthread_unregister_cancel_restore, .-__pthread_unregister_cancel_restore
