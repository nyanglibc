	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_cond_destroy
	.hidden	__pthread_cond_destroy
	.type	__pthread_cond_destroy, @function
__pthread_cond_destroy:
	movl	36(%rdi), %eax
	leaq	36(%rdi), %r8
.L2:
	movl	%eax, %ecx
	movl	%eax, %edx
	orl	$4, %ecx
	lock cmpxchgl	%ecx, (%r8)
	jne	.L2
	movl	%edx, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	je	.L14
	pushq	%rbx
	movl	$202, %r9d
	movl	$1, %ebx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L4:
	movl	(%r8), %edx
	movl	%edx, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	je	.L18
.L6:
	xorl	%r10d, %r10d
	movl	$128, %esi
	movq	%r8, %rdi
	movl	%r9d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L4
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L5
	movq	%rbx, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L4
.L5:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal
	.p2align 4,,10
	.p2align 3
.L18:
	xorl	%eax, %eax
	popq	%rbx
	ret
.L14:
	xorl	%eax, %eax
	ret
	.size	__pthread_cond_destroy, .-__pthread_cond_destroy
	.weak	pthread_cond_destroy
	.set	pthread_cond_destroy,__pthread_cond_destroy
	.hidden	__libc_fatal
