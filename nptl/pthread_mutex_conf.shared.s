	.text
	.p2align 4,,15
	.type	_dl_tunable_set_mutex_spin_count, @function
_dl_tunable_set_mutex_spin_count:
	movq	(%rdi), %rax
	movl	%eax, __mutex_aconf(%rip)
	ret
	.size	_dl_tunable_set_mutex_spin_count, .-_dl_tunable_set_mutex_spin_count
	.p2align 4,,15
	.globl	__pthread_tunables_init
	.hidden	__pthread_tunables_init
	.type	__pthread_tunables_init, @function
__pthread_tunables_init:
	subq	$24, %rsp
	leaq	_dl_tunable_set_mutex_spin_count(%rip), %rdx
	movl	$27, %edi
	leaq	12(%rsp), %rsi
	call	__tunable_get_val@PLT
	addq	$24, %rsp
	ret
	.size	__pthread_tunables_init, .-__pthread_tunables_init
	.hidden	__mutex_aconf
	.globl	__mutex_aconf
	.data
	.align 4
	.type	__mutex_aconf, @object
	.size	__mutex_aconf, 4
__mutex_aconf:
	.long	100
