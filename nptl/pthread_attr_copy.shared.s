	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___pthread_attr_copy
	.hidden	__GI___pthread_attr_copy
	.type	__GI___pthread_attr_copy, @function
__GI___pthread_attr_copy:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbp
	subq	$72, %rsp
	movq	48(%rsi), %rax
	movdqu	(%rsi), %xmm0
	movq	%rax, 48(%rsp)
	movq	40(%rsi), %rax
	movaps	%xmm0, (%rsp)
	testq	%rax, %rax
	movdqu	16(%rsi), %xmm0
	movaps	%xmm0, 16(%rsp)
	movdqu	32(%rsi), %xmm0
	movaps	%xmm0, 32(%rsp)
	movq	$0, 40(%rsp)
	je	.L2
	movq	%rsi, %rbx
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	jne	.L3
	cmpb	$0, 144(%rax)
	movq	%rsp, %r13
	jne	.L4
.L2:
	movdqa	(%rsp), %xmm0
	xorl	%r12d, %r12d
	movq	48(%rsp), %rax
	movups	%xmm0, 0(%rbp)
	movdqa	16(%rsp), %xmm0
	movq	%rax, 48(%rbp)
	movl	%r12d, %eax
	movups	%xmm0, 16(%rbp)
	movdqa	32(%rsp), %xmm0
	movups	%xmm0, 32(%rbp)
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	movq	(%rax), %rdx
	movq	%rsp, %r13
	movq	%r13, %rdi
	call	__GI___pthread_attr_setaffinity_np
	testl	%eax, %eax
	movl	%eax, %r12d
	jne	.L5
	movq	40(%rbx), %rax
	cmpb	$0, 144(%rax)
	je	.L2
	.p2align 4,,10
	.p2align 3
.L4:
	leaq	16(%rax), %rsi
	movq	%r13, %rdi
	call	__GI___pthread_attr_setsigmask_internal
	testl	%eax, %eax
	movl	%eax, %r12d
	je	.L2
.L5:
	movq	%r13, %rdi
	call	__GI___pthread_attr_destroy
	addq	$72, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	__GI___pthread_attr_copy, .-__GI___pthread_attr_copy
	.globl	__pthread_attr_copy
	.set	__pthread_attr_copy,__GI___pthread_attr_copy
