	.text
	.p2align 4,,15
	.globl	pthread_tryjoin_np
	.type	pthread_tryjoin_np, @function
pthread_tryjoin_np:
	movl	720(%rdi), %eax
	testl	%eax, %eax
	jne	.L2
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	__pthread_clockjoin_ex@PLT
	.p2align 4,,10
	.p2align 3
.L2:
	movl	$16, %eax
	ret
	.size	pthread_tryjoin_np, .-pthread_tryjoin_np
