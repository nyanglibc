	.text
	.p2align 4,,15
	.globl	__nptl_main
	.type	__nptl_main, @function
__nptl_main:
	leaq	banner(%rip), %rsi
	movl	$1, %edi
	subq	$8, %rsp
	movl	$261, %edx
	call	__libc_write@PLT
	xorl	%edi, %edi
	call	_exit@PLT
	.size	__nptl_main, .-__nptl_main
	.section	.rodata.str1.32,"aMS",@progbits,1
	.align 32
	.type	banner, @object
	.size	banner, 262
banner:
	.ascii	"Nativ"
	.string	"e POSIX Threads Library\nCopyright (C) 2021 Free Software Foundation, Inc.\nThis is free software; see the source for copying conditions.\nThere is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A\nPARTICULAR PURPOSE.\nForced unwind support included.\n"
