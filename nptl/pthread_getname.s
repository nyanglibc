	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"/proc/self/task/%u/comm"
	.text
	.p2align 4,,15
	.globl	pthread_getname_np
	.type	pthread_getname_np, @function
pthread_getname_np:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	$34, %ebx
	subq	$40, %rsp
	cmpq	$15, %rdx
	jbe	.L1
	cmpq	%rdi, %fs:16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	je	.L21
	movl	720(%rdi), %edx
	movq	%rsp, %rbx
	leaq	.LC0(%rip), %rsi
	movq	%rbx, %rdi
	xorl	%eax, %eax
	call	sprintf@PLT
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	call	__open64_nocancel@PLT
	cmpl	$-1, %eax
	movl	%eax, %r13d
	jne	.L4
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L22:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	cmpl	$4, %ebx
	jne	.L8
.L4:
	movq	%rbp, %rdx
	movq	%r12, %rsi
	movl	%r13d, %edi
	call	__read_nocancel@PLT
	cmpq	$-1, %rax
	je	.L22
	testq	%rax, %rax
	js	.L23
	leaq	-1(%r12,%rax), %rdx
	cmpb	$10, (%rdx)
	je	.L24
	cmpq	%rbp, %rax
	movl	$34, %ebx
	je	.L8
	movb	$0, (%r12,%rax)
	xorl	%ebx, %ebx
.L8:
	movl	%r13d, %edi
	call	__close_nocancel@PLT
.L1:
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L21:
	xorl	%eax, %eax
	movl	$16, %edi
	call	prctl@PLT
	testl	%eax, %eax
	movl	%eax, %ebx
	je	.L1
.L19:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L24:
	movb	$0, (%rdx)
	xorl	%ebx, %ebx
	jmp	.L8
.L23:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %ebx
	jmp	.L8
	.size	pthread_getname_np, .-pthread_getname_np
