	.text
	.p2align 4,,15
	.globl	__libc_tcdrain
	.type	__libc_tcdrain, @function
__libc_tcdrain:
#APP
# 28 "../sysdeps/unix/sysv/linux/tcdrain.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L2
	movl	$1, %edx
	movl	$21513, %esi
	movl	$16, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/tcdrain.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L11
	rep ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbx
	movl	%edi, %ebx
	subq	$16, %rsp
	call	__pthread_enable_asynccancel
	movl	$1, %edx
	movl	%eax, %r8d
	movl	$21513, %esi
	movl	%ebx, %edi
	movl	$16, %eax
#APP
# 28 "../sysdeps/unix/sysv/linux/tcdrain.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L12
.L6:
	movl	%r8d, %edi
	movl	%eax, 12(%rsp)
	call	__pthread_disable_asynccancel
	movl	12(%rsp), %eax
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	ret
.L12:
	movq	errno@gottpoff(%rip), %rdx
	negl	%eax
	movl	%eax, %fs:(%rdx)
	movl	$-1, %eax
	jmp	.L6
	.size	__libc_tcdrain, .-__libc_tcdrain
	.weak	tcdrain
	.set	tcdrain,__libc_tcdrain
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
