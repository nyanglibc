	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../nptl/pthread_mutex_trylock.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"robust"
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"The futex facility returned an unexpected error code.\n"
	.section	.rodata.str1.1
.LC3:
	.string	"mutex->__data.__owner == 0"
	.text
	.p2align 4,,15
	.globl	__pthread_mutex_trylock
	.type	__pthread_mutex_trylock, @function
__pthread_mutex_trylock:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
#APP
# 38 "../nptl/pthread_mutex_trylock.c" 1
	movl %fs:720,%r12d
# 0 "" 2
#NO_APP
	movl	16(%rdi), %eax
	andl	$383, %eax
	cmpl	$35, %eax
	jg	.L3
	cmpl	$32, %eax
	jge	.L4
	cmpl	$3, %eax
	jg	.L5
	cmpl	$2, %eax
	jge	.L6
	testl	%eax, %eax
	jne	.L112
	movl	__pthread_force_elision(%rip), %eax
	testl	%eax, %eax
	jne	.L113
.L6:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L104
	addl	$1, 12(%rbx)
	movl	%r12d, 8(%rbx)
	xorl	%ebp, %ebp
.L1:
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	cmpl	$67, %eax
	jg	.L10
	cmpl	$64, %eax
	jge	.L11
	subl	$48, %eax
	cmpl	$3, %eax
	ja	.L52
.L4:
	movl	16(%rbx), %edx
	movl	%edx, %ecx
	andl	$16, %ecx
	je	.L30
	leaq	32(%rbx), %rax
	orq	$1, %rax
#APP
# 239 "../nptl/pthread_mutex_trylock.c" 1
	movq %rax,%fs:752
# 0 "" 2
#NO_APP
.L30:
	movl	(%rbx), %eax
	andl	$1073741823, %eax
	cmpl	%eax, %r12d
	je	.L114
.L31:
	xorl	%eax, %eax
	lock cmpxchgl	%r12d, (%rbx)
	testl	%eax, %eax
	je	.L33
	testl	$1073741824, %eax
	je	.L107
	testl	%ecx, %ecx
	je	.L115
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$8, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 300 "../nptl/pthread_mutex_trylock.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L36
	cmpl	$-11, %eax
	je	.L107
.L36:
	testl	$1073741824, (%rbx)
	jne	.L116
.L37:
	cmpl	$2147483646, 8(%rbx)
	je	.L117
#APP
# 363 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	leaq	32(%rbx), %rax
	andq	$-2, %rdx
	movq	%rax, -8(%rdx)
#APP
# 363 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 32(%rbx)
	movq	%fs:16, %rsi
	leaq	736(%rsi), %rdx
	movq	%rdx, 24(%rbx)
	orq	$1, %rax
#APP
# 363 "../nptl/pthread_mutex_trylock.c" 1
	movq %rax,%fs:736
# 0 "" 2
# 366 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
.L49:
	movl	%r12d, 8(%rbx)
	addl	$1, 12(%rbx)
	xorl	%ebp, %ebp
	movl	$1, 4(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	$256, %eax
	jne	.L118
.L12:
	leaq	22(%rbx), %rsi
	movq	%rbx, %rdi
	call	__lll_trylock_elision@PLT
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L1
.L104:
	movl	$16, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	subl	$16, %eax
	cmpl	$3, %eax
	ja	.L52
	leaq	16(%rdi), %rcx
	leaq	32(%rdi), %rdi
#APP
# 96 "../nptl/pthread_mutex_trylock.c" 1
	movq %rdi,%fs:752
# 0 "" 2
#NO_APP
	movl	(%rbx), %ebp
	xorl	%esi, %esi
.L19:
	testl	$1073741824, %ebp
	jne	.L119
.L20:
	andl	$1073741823, %ebp
	cmpl	%ebp, %r12d
	je	.L120
.L22:
	movl	%esi, %eax
	lock cmpxchgl	%r12d, (%rbx)
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L25
	testl	$1073741824, %eax
	je	.L107
	cmpl	$2147483646, 8(%rbx)
	je	.L47
	testl	$1073741824, %ebp
	je	.L20
.L119:
	movl	%ebp, %edx
	movl	%ebp, %eax
	andl	$-2147483648, %edx
	orl	%r12d, %edx
	lock cmpxchgl	%edx, (%rbx)
	cmpl	%eax, %ebp
	je	.L121
	movl	%eax, %ebp
	jmp	.L19
	.p2align 4,,10
	.p2align 3
.L112:
	cmpl	$1, %eax
	jne	.L52
.L8:
	cmpl	%r12d, 8(%rbx)
	jne	.L13
	movl	4(%rbx), %eax
	cmpl	$-1, %eax
	je	.L24
.L106:
	addl	$1, %eax
	xorl	%ebp, %ebp
	movl	%eax, 4(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L118:
	cmpl	$257, %eax
	je	.L8
.L52:
	movl	$22, %ebp
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L11:
	movl	16(%rdi), %edx
	cmpl	8(%rdi), %r12d
	movl	$-1, %r14d
	movl	(%rdi), %eax
	jne	.L45
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L43:
	movl	%r13d, %esi
	movl	%r14d, %edi
	call	__pthread_tpp_change_priority@PLT
	testl	%eax, %eax
	movl	%eax, %ebp
	jne	.L1
	movl	%r13d, %edx
	sall	$19, %edx
	movl	%edx, %ecx
	movl	%edx, %eax
	orl	$1, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	cmpl	%eax, %edx
	je	.L44
	movl	%eax, %ecx
	movl	%r13d, %r14d
	andl	$-524288, %ecx
	cmpl	%ecx, %edx
	je	.L123
.L45:
	shrl	$19, %eax
	movl	%eax, %r13d
	call	__pthread_current_priority@PLT
	cmpl	%r13d, %eax
	jle	.L43
	cmpl	$-1, %r14d
	movl	$22, %ebp
	je	.L1
	movl	$-1, %esi
	movl	%r14d, %edi
	call	__pthread_tpp_change_priority@PLT
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L25:
	cmpl	$2147483646, 8(%rbx)
	jne	.L124
.L47:
	cmpl	%eax, %r12d
	movl	$0, 4(%rbx)
	je	.L125
.L39:
#APP
# 354 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$131, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L120:
	movl	(%rcx), %eax
	andl	$127, %eax
	cmpl	$18, %eax
	je	.L102
	cmpl	$17, %eax
	jne	.L22
.L109:
#APP
# 264 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	4(%rbx), %eax
	cmpl	$-1, %eax
	jne	.L106
.L24:
	movl	$11, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rbx)
	jne	.L104
	movl	%r12d, 8(%rbx)
	movl	$1, 4(%rbx)
	xorl	%ebp, %ebp
	addl	$1, 12(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L33:
	testl	%ecx, %ecx
	je	.L49
	jmp	.L37
	.p2align 4,,10
	.p2align 3
.L113:
	movl	16(%rdi), %eax
	testb	$3, %ah
	je	.L17
	testb	$1, %ah
	je	.L6
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L107:
#APP
# 310 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$16, %ebp
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L122:
	andl	$3, %edx
	cmpl	$2, %edx
	je	.L55
	cmpl	$1, %edx
	jne	.L45
	movl	4(%rdi), %eax
	cmpl	%r14d, %eax
	jne	.L106
	jmp	.L24
	.p2align 4,,10
	.p2align 3
.L44:
	movl	8(%rbx), %ebp
	testl	%ebp, %ebp
	jne	.L126
	movl	%r12d, 8(%rbx)
	addl	$1, 12(%rbx)
	movl	$1, 4(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L125:
	xorl	%eax, %eax
#APP
# 190 "../nptl/pthread_mutex_trylock.c" 1
	xchgl %eax, (%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L39
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 190 "../nptl/pthread_mutex_trylock.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L39
	.p2align 4,,10
	.p2align 3
.L123:
	movl	$-1, %esi
	movl	%r13d, %edi
	call	__pthread_tpp_change_priority@PLT
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L124:
#APP
# 203 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	andq	$-2, %rax
	movq	%rdi, -8(%rax)
#APP
# 203 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 203 "../nptl/pthread_mutex_trylock.c" 1
	movq %rdi,%fs:736
# 0 "" 2
# 206 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	%r12d, 8(%rbx)
	addl	$1, 12(%rbx)
	movl	$1, 4(%rbx)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L17:
	orb	$1, %ah
	movl	%eax, 16(%rdi)
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L114:
	andl	$3, %edx
	cmpl	$2, %edx
	je	.L102
	cmpl	$1, %edx
	jne	.L31
	jmp	.L109
	.p2align 4,,10
	.p2align 3
.L121:
	movabsq	$9223372032559808513, %rax
	movq	%rax, 4(%rbx)
#APP
# 129 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	andq	$-2, %rax
	movq	%rdi, -8(%rax)
#APP
# 129 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 129 "../nptl/pthread_mutex_trylock.c" 1
	movq %rdi,%fs:736
# 0 "" 2
# 132 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$130, %ebp
	jmp	.L1
.L116:
#APP
# 320 "../nptl/pthread_mutex_trylock.c" 1
	lock;andl $-1073741825, (%rbx)
# 0 "" 2
#NO_APP
	movabsq	$9223372032559808513, %rax
	movq	%rax, 4(%rbx)
#APP
# 330 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	leaq	32(%rbx), %rdx
	andq	$-2, %rax
	movq	%rdx, -8(%rax)
#APP
# 330 "../nptl/pthread_mutex_trylock.c" 1
	movq %fs:736,%rax
# 0 "" 2
#NO_APP
	movq	%rax, 32(%rbx)
	movq	%fs:16, %rax
	addq	$736, %rax
	movq	%rax, 24(%rbx)
#APP
# 330 "../nptl/pthread_mutex_trylock.c" 1
	movq %rdx,%fs:736
# 0 "" 2
# 333 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$130, %ebp
	jmp	.L1
.L55:
	movl	$35, %ebp
	jmp	.L1
.L102:
#APP
# 256 "../nptl/pthread_mutex_trylock.c" 1
	movq $0,%fs:752
# 0 "" 2
#NO_APP
	movl	$35, %ebp
	jmp	.L1
.L117:
	movl	$0, 4(%rbx)
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$7, %esi
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 307 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L39
	cmpl	$-11, %eax
	je	.L39
	jg	.L41
	cmpl	$-38, %eax
	je	.L39
	cmpl	$-35, %eax
	je	.L39
	cmpl	$-110, %eax
	je	.L39
.L40:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
.L41:
	cmpl	$-4, %eax
	jl	.L40
	cmpl	$-3, %eax
	jle	.L39
	addl	$1, %eax
	je	.L39
	jmp	.L40
.L126:
	leaq	__PRETTY_FUNCTION__.8832(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC3(%rip), %rdi
	movl	$443, %edx
	call	__assert_fail@PLT
.L115:
	leaq	__PRETTY_FUNCTION__.8832(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$293, %edx
	call	__assert_fail@PLT
	.size	__pthread_mutex_trylock, .-__pthread_mutex_trylock
	.weak	pthread_mutex_trylock
	.set	pthread_mutex_trylock,__pthread_mutex_trylock
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.8832, @object
	.size	__PRETTY_FUNCTION__.8832, 24
__PRETTY_FUNCTION__.8832:
	.string	"__pthread_mutex_trylock"
