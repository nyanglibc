	.text
	.p2align 4,,15
	.globl	__pthread_mutexattr_destroy
	.type	__pthread_mutexattr_destroy, @function
__pthread_mutexattr_destroy:
	xorl	%eax, %eax
	ret
	.size	__pthread_mutexattr_destroy, .-__pthread_mutexattr_destroy
	.globl	pthread_mutexattr_destroy
	.set	pthread_mutexattr_destroy,__pthread_mutexattr_destroy
