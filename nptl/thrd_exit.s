	.text
	.p2align 4,,15
	.globl	thrd_exit
	.type	thrd_exit, @function
thrd_exit:
	subq	$8, %rsp
	movslq	%edi, %rdi
	call	__pthread_exit@PLT
	.size	thrd_exit, .-thrd_exit
