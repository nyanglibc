	.text
	.p2align 4,,15
	.globl	__pthread_setcanceltype
	.type	__pthread_setcanceltype, @function
__pthread_setcanceltype:
	cmpl	$1, %edi
	ja	.L9
	subq	$24, %rsp
	movq	%fs:16, %r8
#APP
# 32 "pthread_setcanceltype.c" 1
	movl %fs:776,%eax
# 0 "" 2
#NO_APP
	movl	%eax, 12(%rsp)
	movl	12(%rsp), %ecx
.L7:
	movl	%ecx, %eax
	movl	%ecx, %edx
	orl	$2, %eax
	andl	$-3, %edx
	cmpl	$1, %edi
	cmove	%eax, %edx
	testq	%rsi, %rsi
	je	.L5
	movl	%ecx, %eax
	sarl	%eax
	andl	$1, %eax
	movl	%eax, (%rsi)
.L5:
	cmpl	%edx, %ecx
	je	.L8
	movl	%ecx, %eax
	lock cmpxchgl	%edx, 776(%r8)
	cmpl	%eax, %ecx
	jne	.L10
	andl	$-69, %edx
	cmpl	$10, %edx
	je	.L19
.L8:
	xorl	%eax, %eax
	addq	$24, %rsp
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movl	$22, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L10:
	movl	%eax, %ecx
	jmp	.L7
.L19:
#APP
# 58 "pthread_setcanceltype.c" 1
	movq $-1,%fs:1584
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
#APP
# 304 "pthreadP.h" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
# 307 "pthreadP.h" 1
	movq %fs:768,%rdi
# 0 "" 2
#NO_APP
	call	__GI___pthread_unwind
	.size	__pthread_setcanceltype, .-__pthread_setcanceltype
	.globl	pthread_setcanceltype
	.set	pthread_setcanceltype,__pthread_setcanceltype
