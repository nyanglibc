	.text
	.p2align 4,,15
	.type	check_add_mapping, @function
check_add_mapping:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r12
	pushq	%rbx
	leaq	-192(%rbp), %rsi
	movq	%rdi, %r14
	movl	%edx, %edi
	movl	%edx, %r13d
	movq	%rcx, %rbx
	subq	$168, %rsp
	call	__fstat64@PLT
	testl	%eax, %eax
	jne	.L20
	movl	$1, %edx
	lock cmpxchgl	%edx, __sem_mappings_lock(%rip)
	jne	.L21
.L4:
	leaq	32(%r12), %rax
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%rax, -200(%rbp)
	leaq	62(%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rcx
	andq	$-16, %rcx
	leaq	32(%rcx), %rdi
	movq	%rcx, %r15
	call	memcpy@PLT
	movq	-192(%rbp), %rax
	leaq	__sem_search(%rip), %rdx
	leaq	__sem_mappings(%rip), %rsi
	movq	%r15, %rdi
	movq	%rax, (%r15)
	movq	-184(%rbp), %rax
	movq	%rax, 8(%r15)
	call	__tfind@PLT
	testq	%rax, %rax
	je	.L5
	movq	(%rax), %rax
	movq	24(%rax), %r12
	addl	$1, 16(%rax)
	leaq	-1(%rbx), %rax
	cmpq	$-3, %rax
	setbe	%r8b
	cmpq	%r12, %rbx
	setne	%al
	andl	%eax, %r8d
.L6:
	xorl	%eax, %eax
#APP
# 124 "../sysdeps/pthread/sem_open.c" 1
	xchgl %eax, __sem_mappings_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L22
.L3:
	testb	%r8b, %r8b
	jne	.L23
.L1:
	leaq	-40(%rbp), %rsp
	movq	%r12, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L23:
	movq	errno@gottpoff(%rip), %r13
	movl	$32, %esi
	movq	%rbx, %rdi
	movl	%fs:0(%r13), %r14d
	call	munmap@PLT
	movl	%r14d, %fs:0(%r13)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L20:
	leaq	-1(%rbx), %rax
	cmpq	$-3, %rax
	setbe	%r8b
	testq	%rbx, %rbx
	setne	%al
	xorl	%r12d, %r12d
	andl	%eax, %r8d
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	leaq	32(%r12), %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L24
	testq	%rbx, %rbx
	je	.L25
.L8:
	movq	-192(%rbp), %rax
	leaq	32(%r15), %rdi
	movq	%r12, %rdx
	movl	$1, 16(%r15)
	movq	%rbx, 24(%r15)
	movq	%r14, %rsi
	movq	%rax, (%r15)
	movq	-184(%rbp), %rax
	movq	%rax, 8(%r15)
	call	memcpy@PLT
	leaq	-1(%rbx), %rax
	cmpq	$-3, %rax
	setbe	%r12b
	cmpq	$-1, %rbx
	je	.L10
	leaq	__sem_search(%rip), %rdx
	leaq	__sem_mappings(%rip), %rsi
	movq	%r15, %rdi
	call	__tsearch@PLT
	testq	%rax, %rax
	je	.L10
	movq	%rbx, %r12
	xorl	%r8d, %r8d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L10:
	movq	%r15, %rdi
	call	free@PLT
	testq	%rbx, %rbx
	setne	%r8b
	andl	%r12d, %r8d
	xorl	%r12d, %r12d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L22:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__sem_mappings_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 124 "../sysdeps/pthread/sem_open.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L21:
	leaq	__sem_mappings_lock(%rip), %rdi
	call	__lll_lock_wait_private@PLT
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L24:
	leaq	-1(%rbx), %rax
	cmpq	$-3, %rax
	setbe	%r8b
	testq	%rbx, %rbx
	setne	%al
	xorl	%r12d, %r12d
	andl	%eax, %r8d
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L25:
	xorl	%r9d, %r9d
	movl	%r13d, %r8d
	movl	$1, %ecx
	movl	$3, %edx
	movl	$32, %esi
	xorl	%edi, %edi
	call	mmap@PLT
	movq	%rax, %rbx
	jmp	.L8
	.size	check_add_mapping, .-check_add_mapping
	.p2align 4,,15
	.globl	__sem_search
	.type	__sem_search, @function
__sem_search:
	movq	8(%rsi), %rax
	cmpq	%rax, 8(%rdi)
	je	.L27
.L32:
	sbbl	%eax, %eax
	orl	$1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	movq	(%rsi), %rax
	cmpq	%rax, (%rdi)
	jne	.L32
	addq	$32, %rsi
	addq	$32, %rdi
	jmp	strcmp@PLT
	.size	__sem_search, .-__sem_search
	.p2align 4,,15
	.globl	sem_open
	.type	sem_open, @function
sem_open:
	pushq	%rbp
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r15d
	pushq	%rbx
	movq	%rdi, %rbx
	leaq	-160(%rbp), %rdi
	subq	$168, %rsp
	movq	%rdx, -80(%rbp)
	movq	%rcx, -72(%rbp)
	call	__shm_directory@PLT
	testq	%rax, %rax
	je	.L34
	cmpb	$47, (%rbx)
	movq	%rax, %r13
	jne	.L36
	.p2align 4,,10
	.p2align 3
.L35:
	addq	$1, %rbx
	cmpb	$47, (%rbx)
	je	.L35
.L36:
	movq	%rbx, %rdi
	call	strlen@PLT
	leaq	1(%rax), %r12
	cmpq	$1, %r12
	je	.L38
	cmpq	$254, %r12
	ja	.L38
	movl	$47, %esi
	movq	%rbx, %rdi
	call	strchr@PLT
	testq	%rax, %rax
	jne	.L38
	movq	-160(%rbp), %rdx
	movq	%r13, %rsi
	leaq	34(%rdx,%r12), %rax
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %rdi
	movq	%rax, -200(%rbp)
	call	__mempcpy@PLT
	leaq	4(%rax), %rdi
	movq	%r12, %rdx
	movq	%rbx, %rsi
	movl	$778921331, (%rax)
	call	memcpy@PLT
	leaq	-164(%rbp), %rsi
	movl	$1, %edi
	call	__pthread_setcancelstate@PLT
	movl	%r15d, %eax
	andl	$192, %eax
	cmpl	$192, %eax
	je	.L40
.L41:
	movl	%r15d, %esi
	movq	-200(%rbp), %rdi
	xorl	%eax, %eax
	andl	$-131140, %esi
	orl	$131074, %esi
	call	__libc_open@PLT
	cmpl	$-1, %eax
	jne	.L42
	xorl	%r14d, %r14d
	testb	$64, %r15b
	je	.L43
	movq	errno@gottpoff(%rip), %r11
	cmpl	$2, %fs:(%r11)
	je	.L40
	.p2align 4,,10
	.p2align 3
.L43:
	movl	-164(%rbp), %edi
	xorl	%esi, %esi
	call	__pthread_setcancelstate@PLT
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L40:
	leaq	16(%rbp), %rax
	movl	$24, -152(%rbp)
	movq	%rax, -144(%rbp)
	leaq	-96(%rbp), %rax
	movl	16(%rax), %ecx
	movq	%rax, -136(%rbp)
	movl	24(%rax), %eax
	movl	%ecx, -188(%rbp)
	testl	%eax, %eax
	jns	.L49
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
.L50:
	xorl	%r14d, %r14d
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L38:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$22, %fs:(%rax)
.L33:
	leaq	-40(%rbp), %rsp
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L42:
	xorl	%ecx, %ecx
	movl	%eax, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movl	%eax, -184(%rbp)
	call	check_add_mapping
	movq	errno@gottpoff(%rip), %r11
	movl	-184(%rbp), %r10d
	movq	%rax, %r14
.L44:
	cmpq	$-1, %r14
	movl	$0, %eax
	cmove	%rax, %r14
.L57:
	movl	%r10d, %edi
	movl	%fs:(%r11), %ebx
	movq	%r11, -184(%rbp)
	call	__libc_close@PLT
	movq	-184(%rbp), %r11
	movl	%ebx, %fs:(%r11)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L49:
	movq	-160(%rbp), %rdx
	movq	%rax, -128(%rbp)
	movq	%r13, %rsi
	pxor	%xmm0, %xmm0
	movq	$128, -120(%rbp)
	leaq	41(%rdx), %rax
	movaps	%xmm0, -112(%rbp)
	andq	$-16, %rax
	subq	%rax, %rsp
	leaq	15(%rsp), %rax
	andq	$-16, %rax
	movq	%rax, %rdi
	movq	%rax, -184(%rbp)
	call	__mempcpy@PLT
	movl	$50, -208(%rbp)
	movq	%rax, %r14
	jmp	.L53
	.p2align 4,,10
	.p2align 3
.L76:
	cmpl	$17, %fs:(%r11)
	jne	.L50
	subl	$1, -208(%rbp)
	je	.L80
.L53:
	movq	-184(%rbp), %rdi
	movl	$22616, %eax
	movl	$1482184792, (%r14)
	movw	%ax, 4(%r14)
	movb	$0, 6(%r14)
	call	__mktemp@PLT
	testq	%rax, %rax
	je	.L50
	movl	-188(%rbp), %edx
	movq	-184(%rbp), %rdi
	xorl	%eax, %eax
	movl	$194, %esi
	call	__libc_open@PLT
	cmpl	$-1, %eax
	movl	%eax, %r10d
	movq	errno@gottpoff(%rip), %r11
	je	.L76
	leaq	-128(%rbp), %r14
	jmp	.L52
	.p2align 4,,10
	.p2align 3
.L81:
	cmpl	$4, %fs:(%r11)
	jne	.L55
.L52:
	movl	%r10d, %edi
	movl	$32, %edx
	movq	%r14, %rsi
	movq	%r11, -208(%rbp)
	movl	%r10d, -188(%rbp)
	call	__libc_write@PLT
	cmpq	$-1, %rax
	movl	-188(%rbp), %r10d
	movq	-208(%rbp), %r11
	je	.L81
	cmpq	$32, %rax
	jne	.L55
	xorl	%r9d, %r9d
	movl	%r10d, %r8d
	xorl	%edi, %edi
	movl	$1, %ecx
	movl	$3, %edx
	movl	$32, %esi
	movq	%r11, -208(%rbp)
	movl	%r10d, -188(%rbp)
	call	mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, %r14
	movl	-188(%rbp), %r10d
	movq	-208(%rbp), %r11
	je	.L78
	movq	-200(%rbp), %rsi
	movq	-184(%rbp), %rdi
	movq	%r11, -208(%rbp)
	movl	%r10d, -188(%rbp)
	call	link@PLT
	testl	%eax, %eax
	movl	-188(%rbp), %r10d
	movq	-208(%rbp), %r11
	je	.L59
	movl	$32, %esi
	movq	%r14, %rdi
	call	munmap@PLT
	testb	$-128, %r15b
	movl	-188(%rbp), %r10d
	movq	-208(%rbp), %r11
	jne	.L78
	cmpl	$17, %fs:(%r11)
	movq	%r11, -208(%rbp)
	movl	%r10d, -188(%rbp)
	movq	-184(%rbp), %rdi
	je	.L61
	call	unlink@PLT
	xorl	%r14d, %r14d
	movl	-188(%rbp), %r10d
	movq	-208(%rbp), %r11
	jmp	.L57
	.p2align 4,,10
	.p2align 3
.L34:
	movq	errno@gottpoff(%rip), %rax
	xorl	%r14d, %r14d
	movl	$38, %fs:(%rax)
	jmp	.L33
.L55:
	movq	%r11, -200(%rbp)
	movl	%r10d, -188(%rbp)
.L79:
	movq	-184(%rbp), %rdi
	xorl	%r14d, %r14d
	call	unlink@PLT
	movl	-188(%rbp), %r10d
	movq	-200(%rbp), %r11
	jmp	.L57
.L80:
	movl	$11, %fs:(%r11)
	jmp	.L50
.L78:
	movq	%r11, -200(%rbp)
	jmp	.L79
.L59:
	movq	%r14, %rcx
	movl	%r10d, %edx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	movq	%r11, -200(%rbp)
	movl	%r10d, -188(%rbp)
	call	check_add_mapping
	movq	-184(%rbp), %rdi
	movq	%rax, %r14
	call	unlink@PLT
	movl	-188(%rbp), %r10d
	movq	-200(%rbp), %r11
	jmp	.L44
.L61:
	call	unlink@PLT
	movl	-188(%rbp), %r10d
	movl	%r10d, %edi
	call	__libc_close@PLT
	jmp	.L41
	.size	sem_open, .-sem_open
	.globl	__sem_mappings_lock
	.bss
	.align 4
	.type	__sem_mappings_lock, @object
	.size	__sem_mappings_lock, 4
__sem_mappings_lock:
	.zero	4
	.comm	__sem_mappings,8,8
	.weak	__pthread_setcancelstate
