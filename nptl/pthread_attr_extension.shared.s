	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__pthread_attr_extension
	.hidden	__pthread_attr_extension
	.type	__pthread_attr_extension, @function
__pthread_attr_extension:
	cmpq	$0, 40(%rdi)
	je	.L2
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	pushq	%rbx
	movl	$1, %esi
	movq	%rdi, %rbx
	movl	$152, %edi
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, 40(%rbx)
	je	.L9
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L9:
	movq	__libc_errno@gottpoff(%rip), %rax
	popq	%rbx
	movl	%fs:(%rax), %eax
	ret
	.size	__pthread_attr_extension, .-__pthread_attr_extension
