	.text
#APP
	.symver __pthread_create_2_1,pthread_create@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.type	free_stacks, @function
free_stacks:
	pushq	%r13
	pushq	%r12
	leaq	stack_cache(%rip), %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r13
	subq	$8, %rsp
	movq	8+stack_cache(%rip), %rbx
	cmpq	%r12, %rbx
	movq	8(%rbx), %rbp
	jne	.L4
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L6:
	cmpq	%r12, %rbp
	movq	8(%rbp), %rax
	movq	%rbp, %rbx
	je	.L1
	movq	%rax, %rbp
.L4:
	movl	16(%rbx), %eax
	testl	%eax, %eax
	jg	.L6
	movq	%rbx, in_flight_stack(%rip)
	movq	8(%rbx), %rdx
	movq	(%rbx), %rax
	movq	%rdx, 8(%rax)
	movq	8(%rbx), %rdx
	movq	%rax, (%rdx)
	leaq	-704(%rbx), %rdi
	xorl	%esi, %esi
	movq	984(%rbx), %rax
	movq	$0, in_flight_stack(%rip)
	subq	%rax, stack_cache_actsize(%rip)
	call	_dl_deallocate_tls@PLT
	movq	984(%rbx), %rsi
	movq	976(%rbx), %rdi
	call	__munmap@PLT
	testl	%eax, %eax
	jne	.L11
	cmpq	%r13, stack_cache_actsize(%rip)
	ja	.L6
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
.L11:
	call	abort@PLT
	.size	free_stacks, .-free_stacks
	.p2align 4,,15
	.type	change_stack_perm, @function
change_stack_perm:
	subq	$8, %rsp
	movq	1696(%rdi), %rax
	movq	1688(%rdi), %rsi
	movl	$7, %edx
	subq	%rax, %rsi
	addq	1680(%rdi), %rax
	movq	%rax, %rdi
	call	__mprotect@PLT
	testl	%eax, %eax
	je	.L12
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
.L12:
	addq	$8, %rsp
	ret
	.size	change_stack_perm, .-change_stack_perm
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"../sysdeps/unix/sysv/linux/createthread.c"
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"*stopped_start"
	.text
	.p2align 4,,15
	.type	create_thread, @function
create_thread:
	pushq	%r14
	pushq	%r13
	movq	%r8, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	movq	%rdx, %r12
	movq	%rdi, %rbx
	movq	%rcx, %rsi
	subq	$16, %rsp
	testq	%rbp, %rbp
	je	.L23
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.L21
	cmpq	$0, (%rax)
	movl	$1, %r13d
	je	.L21
.L22:
	movl	$1, %eax
	movb	$1, (%r12)
	movb	%al, 1555(%rbx)
	cmpb	$0, (%r12)
	je	.L25
	.p2align 4,,10
	.p2align 3
.L51:
	leaq	1560(%rbx), %rdi
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, (%rdi)
	je	.L25
	movq	%rsi, 8(%rsp)
	call	__lll_lock_wait_private
	movq	8(%rsp), %rsi
	jmp	.L25
	.p2align 4,,10
	.p2align 3
.L21:
	testb	$2, 8(%rbp)
	jne	.L50
.L23:
	movzbl	(%r12), %eax
	xorl	%r13d, %r13d
	movb	%al, 1555(%rbx)
	cmpb	$0, (%r12)
	jne	.L51
.L25:
	leaq	720(%rbx), %r8
	subq	$8, %rsp
	leaq	start_thread(%rip), %rdi
	movq	%rbx, %rcx
	movl	$4001536, %edx
	xorl	%eax, %eax
	pushq	%r8
	movq	%rbx, %r9
	call	__clone@PLT
	cmpl	$-1, %eax
	popq	%rdx
	popq	%rcx
	je	.L52
	testq	%rbp, %rbp
	movb	$1, (%r14)
	je	.L33
	testl	%r13d, %r13d
	je	.L30
	cmpb	$0, (%r12)
	je	.L53
	movq	40(%rbp), %rax
	movl	720(%rbx), %edi
	movq	8(%rax), %rsi
	movq	(%rax), %rdx
	movl	$203, %eax
#APP
# 122 "../sysdeps/unix/sysv/linux/createthread.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L49
.L30:
	testb	$2, 8(%rbp)
	je	.L33
	cmpb	$0, (%r12)
	je	.L54
	movl	1596(%rbx), %esi
	movl	720(%rbx), %edi
	leaq	1592(%rbx), %rdx
	movl	$144, %eax
#APP
# 144 "../sysdeps/unix/sysv/linux/createthread.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	ja	.L49
.L33:
	xorl	%eax, %eax
.L18:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L49:
.L32:
	movl	%eax, %ebp
	call	__getpid@PLT
	movl	720(%rbx), %esi
	movl	%eax, %edi
	movl	$32, %edx
	movl	$234, %eax
#APP
# 133 "../sysdeps/unix/sysv/linux/createthread.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%ebp, %eax
	negl	%eax
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L52:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	xorl	%r13d, %r13d
	jmp	.L22
.L54:
	leaq	__PRETTY_FUNCTION__.13521(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$142, %edx
	call	__assert_fail@PLT
.L53:
	leaq	__PRETTY_FUNCTION__.13521(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$120, %edx
	call	__assert_fail@PLT
	.size	create_thread, .-create_thread
	.section	.rodata.str1.8
	.align 8
.LC2:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.type	setxid_mark_thread.isra.0, @function
setxid_mark_thread.isra.0:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r8
	pushq	%rbx
	cmpl	$-1, 1564(%rdi)
	je	.L71
.L56:
	movl	$0, 1564(%r8)
.L63:
	movl	776(%r8), %eax
	testb	$16, %al
	jne	.L72
	movl	%eax, %edx
	orl	$64, %edx
	lock cmpxchgl	%edx, 776(%r8)
	jne	.L63
.L55:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L71:
	leaq	1564(%rdi), %rbx
	movl	$-2, %r9d
	movl	$-1, %eax
	lock cmpxchgl	%r9d, (%rbx)
	jne	.L56
	movl	$202, %ebp
	movl	$1, %r12d
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L57:
	cmpl	$-2, 1564(%r8)
	jne	.L56
.L59:
	xorl	%r10d, %r10d
	movl	%r9d, %edx
	movl	$128, %esi
	movq	%rbx, %rdi
	movl	%ebp, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L57
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L58
	movq	%r12, %rax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L57
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L72:
	testb	$64, %al
	jne	.L55
	movl	$1, 1564(%r8)
	leaq	1564(%r8), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L55
	cmpl	$-22, %eax
	je	.L55
	cmpl	$-14, %eax
	je	.L55
.L58:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
	.size	setxid_mark_thread.isra.0, .-setxid_mark_thread.isra.0
	.p2align 4,,15
	.type	setxid_unmark_thread.isra.2, @function
setxid_unmark_thread.isra.2:
.L75:
	movl	776(%rdi), %eax
	testb	$64, %al
	je	.L73
	movl	%eax, %edx
	andl	$-65, %edx
	lock cmpxchgl	%edx, 776(%rdi)
	jne	.L75
	movl	$1, 1564(%rdi)
	xorl	%r10d, %r10d
	addq	$1564, %rdi
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L86
.L73:
	rep ret
	.p2align 4,,10
	.p2align 3
.L86:
	cmpl	$-22, %eax
	je	.L73
	cmpl	$-14, %eax
	je	.L73
	leaq	.LC2(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	setxid_unmark_thread.isra.2, .-setxid_unmark_thread.isra.2
	.p2align 4,,15
	.type	setxid_signal_thread.part.4, @function
setxid_signal_thread.part.4:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	call	__getpid@PLT
	movl	720(%rbp), %esi
	movl	%eax, %edi
	movl	$33, %edx
	movl	$234, %eax
#APP
# 1031 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	xorl	%edx, %edx
	cmpl	$-4096, %eax
	ja	.L87
#APP
# 1036 "allocatestack.c" 1
	lock;incl 32(%rbx)
# 0 "" 2
#NO_APP
	movl	$1, %edx
.L87:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.size	setxid_signal_thread.part.4, .-setxid_signal_thread.part.4
	.p2align 4,,15
	.type	__nptl_deallocate_tsd.part.5, @function
__nptl_deallocate_tsd.part.5:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	leaq	__GI___pthread_keys(%rip), %r13
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	$4, 8(%rsp)
.L100:
#APP
# 269 "pthread_create.c" 1
	movb $0,%fs:1552
# 0 "" 2
#NO_APP
	movl	$32, %r12d
	xorl	%ebp, %ebp
	.p2align 4,,10
	.p2align 3
.L98:
#APP
# 275 "pthread_create.c" 1
	movq %fs:1296(,%rbp,8),%rbx
# 0 "" 2
#NO_APP
	testq	%rbx, %rbx
	je	.L93
	movq	%rbp, %r15
	movq	%r12, %rax
	addq	$8, %rbx
	salq	$9, %r15
	salq	$4, %rax
	addq	%r13, %r15
	leaq	(%rax,%r13), %r14
	jmp	.L97
	.p2align 4,,10
	.p2align 3
.L95:
	addq	$16, %r15
	addq	$16, %rbx
	cmpq	%r14, %r15
	je	.L93
.L97:
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L95
	movq	(%r15), %rax
	cmpq	%rax, -8(%rbx)
	movq	$0, (%rbx)
	jne	.L95
	movq	8(%r15), %rdx
	testq	%rdx, %rdx
	je	.L95
	addq	$16, %r15
	addq	$16, %rbx
	call	*%rdx
	cmpq	%r14, %r15
	jne	.L97
	.p2align 4,,10
	.p2align 3
.L93:
	addq	$1, %rbp
	addq	$32, %r12
	cmpq	$32, %rbp
	jne	.L98
	xorl	%eax, %eax
#APP
# 311 "pthread_create.c" 1
	movb %fs:1552,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	je	.L99
	subq	$1, 8(%rsp)
	movq	8(%rsp), %rsi
	jne	.L100
	movq	%fs:16, %rax
	leaq	792(%rax), %rdi
	movq	$0, 784(%rax)
	movq	$0, 1288(%rax)
	andq	$-8, %rdi
	subl	%edi, %eax
	leal	1296(%rax), %ecx
	movq	%rsi, %rax
	shrl	$3, %ecx
	rep stosq
.L99:
	movl	$1, %ebx
	.p2align 4,,10
	.p2align 3
.L102:
#APP
# 328 "pthread_create.c" 1
	movq %fs:1296(,%rbx,8),%rdi
# 0 "" 2
#NO_APP
	testq	%rdi, %rdi
	je	.L101
	call	free@PLT
#APP
# 334 "pthread_create.c" 1
	movq $0,%fs:1296(,%rbx,8)
# 0 "" 2
#NO_APP
.L101:
	addq	$1, %rbx
	cmpq	$32, %rbx
	jne	.L102
#APP
# 338 "pthread_create.c" 1
	movb $0,%fs:1552
# 0 "" 2
#NO_APP
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.size	__nptl_deallocate_tsd.part.5, .-__nptl_deallocate_tsd.part.5
	.p2align 4,,15
	.globl	__nptl_stacks_freeres
	.hidden	__nptl_stacks_freeres
	.type	__nptl_stacks_freeres, @function
__nptl_stacks_freeres:
	xorl	%edi, %edi
	jmp	free_stacks
	.size	__nptl_stacks_freeres, .-__nptl_stacks_freeres
	.p2align 4,,15
	.globl	__deallocate_stack
	.hidden	__deallocate_stack
	.type	__deallocate_stack, @function
__deallocate_stack:
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	$1, %edx
	subq	$8, %rsp
	movq	_rtld_global@GOTPCREL(%rip), %rbp
	lock cmpxchgl	%edx, 4144(%rbp)
	jne	.L127
.L122:
	leaq	704(%rbx), %rax
	movq	%rax, in_flight_stack(%rip)
	movq	712(%rbx), %rcx
	movq	704(%rbx), %rdx
	movq	%rcx, 8(%rdx)
	movq	712(%rbx), %rcx
	movq	%rdx, (%rcx)
	cmpb	$0, 1554(%rbx)
	movq	$0, in_flight_stack(%rip)
	jne	.L123
	movq	%rax, %rdx
	orq	$1, %rdx
	movq	%rdx, in_flight_stack(%rip)
	movq	stack_cache(%rip), %rdx
	leaq	stack_cache(%rip), %rsi
	movq	%rsi, 712(%rbx)
	movq	%rdx, 704(%rbx)
	movq	%rax, 8(%rdx)
	movq	%rax, stack_cache(%rip)
	movq	stack_cache_actsize(%rip), %rax
	addq	1688(%rbx), %rax
	movq	$0, in_flight_stack(%rip)
	cmpq	$41943040, %rax
	movq	%rax, stack_cache_actsize(%rip)
	ja	.L128
.L124:
	xorl	%eax, %eax
#APP
# 806 "allocatestack.c" 1
	xchgl %eax, 4144(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L129
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L127:
	leaq	4144(%rbp), %rdi
	call	__lll_lock_wait_private
	jmp	.L122
	.p2align 4,,10
	.p2align 3
.L123:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_dl_deallocate_tls@PLT
	jmp	.L124
	.p2align 4,,10
	.p2align 3
.L129:
	leaq	4144(%rbp), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 806 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movl	$41943040, %edi
	call	free_stacks
	jmp	.L124
	.size	__deallocate_stack, .-__deallocate_stack
	.p2align 4,,15
	.globl	__make_stacks_executable
	.hidden	__make_stacks_executable
	.type	__make_stacks_executable, @function
__make_stacks_executable:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	call	_dl_make_stack_executable@PLT
	testl	%eax, %eax
	movl	%eax, %ebp
	je	.L144
.L130:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L144:
	movq	_rtld_global@GOTPCREL(%rip), %r12
	movl	$1, %edx
	lock cmpxchgl	%edx, 4144(%r12)
	jne	.L145
.L132:
	movq	4112(%r12), %rbx
	leaq	4112(%r12), %r13
	cmpq	%r13, %rbx
	jne	.L135
	jmp	.L133
	.p2align 4,,10
	.p2align 3
.L146:
	movq	(%rbx), %rbx
	cmpq	%r13, %rbx
	je	.L133
.L135:
	leaq	-704(%rbx), %rdi
	call	change_stack_perm
	testl	%eax, %eax
	je	.L146
.L139:
	movl	%eax, %ebp
.L134:
	xorl	%eax, %eax
#APP
# 851 "allocatestack.c" 1
	xchgl %eax, 4144(%r12)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L130
	leaq	4144(%r12), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 851 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L133:
	movq	stack_cache(%rip), %rbx
	leaq	stack_cache(%rip), %r13
	cmpq	%r13, %rbx
	jne	.L136
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L147:
	movq	(%rbx), %rbx
	cmpq	%r13, %rbx
	je	.L134
.L136:
	leaq	-704(%rbx), %rdi
	call	change_stack_perm
	testl	%eax, %eax
	je	.L147
	jmp	.L139
	.p2align 4,,10
	.p2align 3
.L145:
	leaq	4144(%r12), %rdi
	call	__lll_lock_wait_private
	jmp	.L132
	.size	__make_stacks_executable, .-__make_stacks_executable
	.section	.rodata.str1.1
.LC3:
	.string	"allocatestack.c"
.LC4:
	.string	"l->next->prev == elem"
	.text
	.p2align 4,,15
	.globl	__reclaim_stacks
	.hidden	__reclaim_stacks
	.type	__reclaim_stacks, @function
__reclaim_stacks:
	movq	in_flight_stack(%rip), %rax
	movq	%fs:16, %r10
	testq	%rax, %rax
	je	.L172
	movq	%rax, %rdx
	andq	$-2, %rdx
	testb	$1, %al
	je	.L151
	movq	_rtld_global@GOTPCREL(%rip), %r9
	movq	4112(%r9), %rsi
	leaq	4112(%r9), %rax
	movq	8(%rsi), %rcx
	cmpq	%rax, %rcx
	je	.L173
.L152:
	cmpq	%rcx, %rdx
	jne	.L174
	movq	%rsi, (%rdx)
	movq	%rax, 8(%rdx)
	movq	%rdx, (%rax)
	movq	4112(%r9), %rsi
	jmp	.L150
	.p2align 4,,10
	.p2align 3
.L151:
	movq	(%rdx), %rax
	movq	8(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	8(%rdx), %rdx
	movq	%rax, (%rdx)
.L172:
	movq	_rtld_global@GOTPCREL(%rip), %r9
	movq	4112(%r9), %rsi
.L150:
	leaq	4112(%r9), %rdx
	xorl	%eax, %eax
	cmpq	%rdx, %rsi
	movq	%rdx, %r11
	je	.L155
	.p2align 4,,10
	.p2align 3
.L154:
	leaq	-704(%rsi), %rdx
	cmpq	%rdx, %r10
	je	.L156
	movq	984(%rsi), %rdx
	addq	%rdx, stack_cache_actsize(%rip)
	cmpb	$0, 848(%rsi)
	movl	$0, 16(%rsi)
	jne	.L175
.L156:
	movq	(%rsi), %rsi
	cmpq	%r11, %rsi
	jne	.L154
	movq	4112(%r9), %rax
	cmpq	%rsi, %rax
	je	.L155
	leaq	stack_cache(%rip), %rsi
	movq	stack_cache(%rip), %rdx
	movq	%rsi, 8(%rax)
	movq	4120(%r9), %rax
	movq	%rdx, (%rax)
	movq	stack_cache(%rip), %rdx
	movq	%rax, 8(%rdx)
	movq	4112(%r9), %rax
	movq	%rax, stack_cache(%rip)
.L155:
	leaq	704(%r10), %rcx
	movq	%rcx, in_flight_stack(%rip)
	movq	712(%r10), %rdx
	movq	704(%r10), %rax
	movq	%rdx, 8(%rax)
	movq	712(%r10), %rdx
	movq	%rax, (%rdx)
	leaq	4112(%r9), %rax
	leaq	4128(%r9), %rsi
	movq	$0, in_flight_stack(%rip)
	xorl	%edx, %edx
	movq	%rax, 4120(%r9)
	movq	%rax, 4112(%r9)
	movq	%rsi, 4136(%r9)
	movq	%rsi, 4128(%r9)
#APP
# 950 "allocatestack.c" 1
	movb %fs:1554,%dl
# 0 "" 2
#NO_APP
	testb	%dl, %dl
	jne	.L176
	movq	%rax, 704(%r10)
	movq	%rax, 712(%r10)
	movq	4112(%r9), %rax
	movq	%rcx, 8(%rax)
	movq	%rcx, 4112(%r9)
.L161:
	movl	$1, __nptl_nthreads(%rip)
	movq	$0, in_flight_stack(%rip)
	movl	$0, 4144(%r9)
	movl	$0, __default_pthread_attr_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L175:
	leaq	88(%rsi), %rdi
	movl	%esi, %ecx
	leaq	600(%rsi), %rdx
	leaq	848(%rsi), %r8
	movq	$0, 80(%rsi)
	andq	$-8, %rdi
	movq	$0, -16(%rdx)
	subl	%edi, %ecx
	addl	$592, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	$0, 848(%rsi)
	.p2align 4,,10
	.p2align 3
.L158:
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	je	.L157
	leaq	8(%rcx), %rdi
	movq	$0, (%rcx)
	movq	$0, 504(%rcx)
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$512, %ecx
	shrl	$3, %ecx
	rep stosq
	movb	$1, 848(%rsi)
.L157:
	addq	$8, %rdx
	cmpq	%r8, %rdx
	jne	.L158
	jmp	.L156
.L173:
	movq	stack_cache(%rip), %rdi
	leaq	stack_cache(%rip), %rax
	movq	8(%rdi), %rcx
	cmpq	%rax, %rcx
	je	.L150
	movq	%rdi, %rsi
	jmp	.L152
.L176:
	movq	%rsi, 704(%r10)
	movq	%rsi, 712(%r10)
	movq	4128(%r9), %rax
	movq	%rcx, 8(%rax)
	movq	%rcx, 4128(%r9)
	jmp	.L161
.L174:
	leaq	__PRETTY_FUNCTION__.13328(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC4(%rip), %rdi
	subq	$8, %rsp
	movl	$889, %edx
	call	__assert_fail@PLT
	.size	__reclaim_stacks, .-__reclaim_stacks
	.p2align 4,,15
	.globl	__nptl_setxid_error
	.hidden	__nptl_setxid_error
	.type	__nptl_setxid_error, @function
__nptl_setxid_error:
.L180:
	movl	36(%rdi), %eax
	cmpl	%esi, %eax
	je	.L177
	cmpl	$-1, %eax
	jne	.L186
	lock cmpxchgl	%esi, 36(%rdi)
	jne	.L180
.L177:
	rep ret
.L186:
	subq	$24, %rsp
	movl	%esi, 12(%rsp)
	call	abort@PLT
	.size	__nptl_setxid_error, .-__nptl_setxid_error
	.p2align 4,,15
	.globl	__nptl_setxid
	.hidden	__nptl_setxid
	.type	__nptl_setxid, @function
__nptl_setxid:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	movl	$1, %edx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	_rtld_global@GOTPCREL(%rip), %r13
	lock cmpxchgl	%edx, 4144(%r13)
	jne	.L243
.L188:
	movq	4112(%r13), %rbp
	leaq	4112(%r13), %r12
	movl	$0, 32(%rbx)
	movq	%rbx, __xidcmd(%rip)
	movl	$-1, 36(%rbx)
	movq	%fs:16, %r15
	cmpq	%r12, %rbp
	je	.L189
	.p2align 4,,10
	.p2align 3
.L191:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L190
	call	setxid_mark_thread.isra.0
.L190:
	movq	0(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.L191
.L189:
	movq	4128(%r13), %rbp
	leaq	4128(%r13), %r12
	cmpq	%r12, %rbp
	je	.L192
	.p2align 4,,10
	.p2align 3
.L194:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L193
	call	setxid_mark_thread.isra.0
.L193:
	movq	0(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.L194
.L192:
	leaq	4112(%r13), %r12
	leaq	32(%rbx), %rbp
	.p2align 4,,10
	.p2align 3
.L211:
	movq	4112(%r13), %rdx
	cmpq	%r12, %rdx
	je	.L195
	xorl	%r14d, %r14d
	jmp	.L198
	.p2align 4,,10
	.p2align 3
.L196:
	movq	(%rdx), %rdx
	cmpq	%r12, %rdx
	je	.L244
.L198:
	leaq	-704(%rdx), %rsi
	cmpq	%rsi, %r15
	je	.L196
	testb	$64, 72(%rdx)
	je	.L196
	movq	%rbx, %rdi
	movq	%rdx, (%rsp)
	call	setxid_signal_thread.part.4
	movq	(%rsp), %rdx
	addl	%eax, %r14d
	movq	(%rdx), %rdx
	cmpq	%r12, %rdx
	jne	.L198
.L244:
	movq	4128(%r13), %rdx
	leaq	4128(%r13), %rax
	cmpq	%rax, %rdx
	je	.L203
.L200:
	leaq	4128(%r13), %rcx
	jmp	.L206
	.p2align 4,,10
	.p2align 3
.L204:
	movq	(%rdx), %rdx
	cmpq	%rcx, %rdx
	je	.L203
.L206:
	leaq	-704(%rdx), %rsi
	cmpq	%rsi, %r15
	je	.L204
	testb	$64, 72(%rdx)
	je	.L204
	movq	%rbx, %rdi
	movq	%rcx, 8(%rsp)
	movq	%rdx, (%rsp)
	call	setxid_signal_thread.part.4
	movq	(%rsp), %rdx
	movq	8(%rsp), %rcx
	addl	%eax, %r14d
	movq	(%rdx), %rdx
	cmpq	%rcx, %rdx
	jne	.L206
.L203:
	movl	32(%rbx), %edx
	testl	%edx, %edx
	je	.L207
.L201:
	movl	$202, %r8d
	jmp	.L210
	.p2align 4,,10
	.p2align 3
.L208:
	movl	32(%rbx), %edx
	testl	%edx, %edx
	je	.L207
.L210:
	xorl	%r10d, %r10d
	movl	$128, %esi
	movq	%rbp, %rdi
	movl	%r8d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L208
	leal	11(%rax), %ecx
	cmpl	$11, %ecx
	ja	.L209
	movl	$1, %eax
	salq	%cl, %rax
	testl	$2177, %eax
	jne	.L208
.L209:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L207:
	testl	%r14d, %r14d
	jne	.L211
	movq	4112(%r13), %rbp
	leaq	4112(%r13), %rax
	movq	%rax, %r12
	cmpq	%rax, %rbp
	je	.L242
	.p2align 4,,10
	.p2align 3
.L212:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L214
	call	setxid_unmark_thread.isra.2
.L214:
	movq	0(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.L212
.L242:
	movq	4128(%r13), %rbp
	leaq	4128(%r13), %r12
	cmpq	%r12, %rbp
	je	.L202
	.p2align 4,,10
	.p2align 3
.L216:
	leaq	-704(%rbp), %rdi
	cmpq	%rdi, %r15
	je	.L215
	call	setxid_unmark_thread.isra.2
.L215:
	movq	0(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.L216
.L202:
	movq	16(%rbx), %rsi
	movq	8(%rbx), %rdi
	movq	24(%rbx), %rdx
	movl	(%rbx), %eax
#APP
# 1160 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	xorl	%esi, %esi
	cmpl	$-4096, %eax
	movl	%eax, %ebp
	ja	.L245
.L217:
	movq	%rbx, %rdi
	call	__nptl_setxid_error
	xorl	%eax, %eax
#APP
# 1171 "allocatestack.c" 1
	xchgl %eax, 4144(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L246
.L187:
	addq	$24, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L195:
	movq	4128(%r13), %rdx
	leaq	4128(%r13), %rax
	xorl	%r14d, %r14d
	cmpq	%rax, %rdx
	jne	.L200
	movl	32(%rbx), %edx
	testl	%edx, %edx
	jne	.L201
	jmp	.L202
.L243:
	leaq	4144(%r13), %rdi
	call	__lll_lock_wait_private
	jmp	.L188
.L245:
	movl	%eax, %esi
	movq	errno@gottpoff(%rip), %rax
	movl	$-1, %ebp
	negl	%esi
	movl	%esi, %fs:(%rax)
	jmp	.L217
.L246:
	leaq	4144(%r13), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 1171 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L187
	.size	__nptl_setxid, .-__nptl_setxid
	.p2align 4,,15
	.globl	__pthread_init_static_tls
	.hidden	__pthread_init_static_tls
	.type	__pthread_init_static_tls, @function
__pthread_init_static_tls:
	pushq	%r14
	pushq	%r13
	xorl	%eax, %eax
	movq	_rtld_global@GOTPCREL(%rip), %r13
	pushq	%r12
	movl	$1, %edx
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	lock cmpxchgl	%edx, 4144(%r13)
	jne	.L257
.L248:
	movq	4112(%r13), %rbp
	leaq	4112(%r13), %rax
	cmpq	%rax, %rbp
	je	.L249
	movq	$-704, %r14
	.p2align 4,,10
	.p2align 3
.L250:
	movq	%r14, %rdi
	subq	1112(%rbx), %rdi
	movq	1080(%rbx), %rdx
	movq	1088(%rbx), %r12
	movq	1072(%rbx), %rsi
	addq	%rbp, %rdi
	subq	%rdx, %r12
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	0(%rbp), %rbp
	leaq	4112(%r13), %rax
	cmpq	%rax, %rbp
	jne	.L250
.L249:
	movq	4128(%r13), %rbp
	leaq	4128(%r13), %rax
	cmpq	%rax, %rbp
	je	.L251
	movq	$-704, %r14
	.p2align 4,,10
	.p2align 3
.L252:
	movq	%r14, %rdi
	subq	1112(%rbx), %rdi
	movq	1080(%rbx), %rdx
	movq	1088(%rbx), %r12
	movq	1072(%rbx), %rsi
	addq	%rbp, %rdi
	subq	%rdx, %r12
	call	__mempcpy@PLT
	xorl	%esi, %esi
	movq	%r12, %rdx
	movq	%rax, %rdi
	call	memset@PLT
	movq	0(%rbp), %rbp
	leaq	4128(%r13), %rax
	cmpq	%rax, %rbp
	jne	.L252
.L251:
	xorl	%eax, %eax
#APP
# 1206 "allocatestack.c" 1
	xchgl %eax, 4144(%r13)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L258
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L257:
	leaq	4144(%r13), %rdi
	call	__lll_lock_wait_private
	jmp	.L248
	.p2align 4,,10
	.p2align 3
.L258:
	leaq	4144(%r13), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 1206 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.size	__pthread_init_static_tls, .-__pthread_init_static_tls
	.p2align 4,,15
	.globl	__find_in_stack_list
	.hidden	__find_in_stack_list
	.type	__find_in_stack_list, @function
__find_in_stack_list:
	pushq	%rbp
	pushq	%rbx
	xorl	%eax, %eax
	movq	%rdi, %rbx
	movl	$1, %edx
	subq	$8, %rsp
	movq	_rtld_global@GOTPCREL(%rip), %rbp
	lock cmpxchgl	%edx, 4144(%rbp)
	jne	.L274
.L260:
	movq	4112(%rbp), %rax
	leaq	4112(%rbp), %rcx
	cmpq	%rcx, %rax
	jne	.L264
	jmp	.L261
	.p2align 4,,10
	.p2align 3
.L275:
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	je	.L261
.L264:
	leaq	-704(%rax), %rdx
	cmpq	%rdx, %rbx
	jne	.L275
	testq	%rbx, %rbx
	je	.L261
.L265:
	xorl	%eax, %eax
#APP
# 243 "pthread_create.c" 1
	xchgl %eax, 4144(%rbp)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L276
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L261:
	movq	4128(%rbp), %rax
	leaq	4128(%rbp), %rcx
	cmpq	%rcx, %rax
	je	.L269
	.p2align 4,,10
	.p2align 3
.L267:
	leaq	-704(%rax), %rdx
	cmpq	%rdx, %rbx
	je	.L265
	movq	(%rax), %rax
	cmpq	%rcx, %rax
	jne	.L267
.L269:
	xorl	%ebx, %ebx
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L276:
	leaq	4144(%rbp), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 243 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	leaq	4144(%rbp), %rdi
	call	__lll_lock_wait_private
	jmp	.L260
	.size	__find_in_stack_list, .-__find_in_stack_list
	.p2align 4,,15
	.globl	__nptl_deallocate_tsd
	.hidden	__nptl_deallocate_tsd
	.type	__nptl_deallocate_tsd, @function
__nptl_deallocate_tsd:
	xorl	%eax, %eax
#APP
# 258 "pthread_create.c" 1
	movb %fs:1552,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	je	.L277
	jmp	__nptl_deallocate_tsd.part.5
	.p2align 4,,10
	.p2align 3
.L277:
	rep ret
	.size	__nptl_deallocate_tsd, .-__nptl_deallocate_tsd
	.p2align 4,,15
	.globl	__free_tcb
	.hidden	__free_tcb
	.type	__free_tcb, @function
__free_tcb:
#APP
# 349 "pthread_create.c" 1
	lock;btsl $5, 776(%rdi); setc %al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L279
	pushq	%rbx
	movq	%rdi, %rbx
	movq	1712(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.L288
.L281:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__deallocate_stack
	.p2align 4,,10
	.p2align 3
.L279:
	rep ret
	.p2align 4,,10
	.p2align 3
.L288:
	movq	$0, 1712(%rbx)
	call	free@PLT
	jmp	.L281
	.size	__free_tcb, .-__free_tcb
	.section	.rodata.str1.1
.LC5:
	.string	"freesize < size"
	.text
	.p2align 4,,15
	.type	start_thread, @function
start_thread:
	pushq	%r12
	pushq	%rbp
	leaq	1720(%rdi), %rdx
	pushq	%rbx
	movq	%rdi, %rbx
	addq	$-128, %rsp
	movq	__resp@gottpoff(%rip), %rax
	movq	%rdx, %fs:(%rax)
	call	__ctype_init@PLT
	movq	%rbx, 8(%rsp)
	leaq	736(%rbx), %rdi
	movl	$24, %esi
	movl	$273, %eax
#APP
# 395 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	leaq	16(%rsp), %rdi
	call	_setjmp@PLT
	movq	8(%rsp), %r8
	movl	%eax, %ebx
	movq	$0, 88(%rsp)
	movq	$0, 96(%rsp)
	movl	$8, %r10d
	xorl	%edx, %edx
	movl	$2, %edi
	movl	$14, %eax
	leaq	2288(%r8), %rsi
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
# 432 "pthread_create.c" 1
	xchgl %edx, 1564(%r8)
# 0 "" 2
#NO_APP
	cmpl	$-2, %edx
	je	.L329
.L293:
	testl	%ebx, %ebx
	jne	.L294
	leaq	16(%rsp), %rax
#APP
# 438 "pthread_create.c" 1
	movq %rax,%fs:768
# 0 "" 2
#NO_APP
	movq	8(%rsp), %rax
	cmpb	$0, 1555(%rax)
	jne	.L295
.L302:
	movq	8(%rsp), %rcx
	cmpb	$0, 2416(%rcx)
	movq	1600(%rcx), %rax
	movq	1608(%rcx), %rdi
	jne	.L296
	call	*%rax
.L303:
#APP
# 474 "pthread_create.c" 1
	movq %rax,%fs:1584
# 0 "" 2
#NO_APP
.L294:
	call	__call_tls_dtors@PLT
	xorl	%eax, %eax
#APP
# 258 "pthread_create.c" 1
	movb %fs:1552,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L304
.L306:
	call	__libc_thread_freeres@PLT
#APP
# 492 "pthread_create.c" 1
	lock;decl __nptl_nthreads(%rip); sete %al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L330
	movq	8(%rsp), %rax
	cmpb	$0, 1553(%rax)
	jne	.L308
.L311:
	movq	8(%rsp), %rax
#APP
# 528 "pthread_create.c" 1
	lock;orl $16, 776(%rax)
# 0 "" 2
#NO_APP
	cmpb	$0, 1554(%rax)
	jne	.L309
	movq	8(%rsp), %rbp
	call	__getpagesize@PLT
	movq	%rsp, %rdx
	negl	%eax
	movq	1680(%rbp), %rbx
	cltq
	subq	%rbx, %rdx
	andq	%rdx, %rax
	cmpq	%rax, 1688(%rbp)
	jbe	.L331
	cmpq	$16384, %rax
	ja	.L332
.L309:
	movq	8(%rsp), %rax
	testb	$64, 776(%rax)
	jne	.L333
.L315:
	movq	8(%rsp), %rax
	cmpq	%rax, 1576(%rax)
	je	.L319
.L321:
	movl	$60, %edx
	.p2align 4,,10
	.p2align 3
.L320:
	xorl	%edi, %edi
	movl	%edx, %eax
#APP
# 35 "../sysdeps/unix/sysv/linux/exit-thread.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L320
.L296:
	call	*%rax
	cltq
	jmp	.L303
.L329:
	movq	8(%rsp), %rax
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	1564(%rax), %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L293
	cmpl	$-22, %eax
	je	.L293
	cmpl	$-14, %eax
	je	.L293
.L318:
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
.L295:
	call	__pthread_enable_asynccancel
	movl	%eax, %r12d
	movq	8(%rsp), %rax
	movl	$1, %edx
	leaq	1560(%rax), %rbp
	movl	%ebx, %eax
	lock cmpxchgl	%edx, 0(%rbp)
	jne	.L298
.L301:
	xorl	%eax, %eax
	movq	8(%rsp), %rcx
#APP
# 454 "pthread_create.c" 1
	xchgl %eax, 1560(%rcx)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L300
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 454 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L300:
	movl	%r12d, %edi
	call	__pthread_disable_asynccancel
	jmp	.L302
.L304:
	call	__nptl_deallocate_tsd.part.5
	jmp	.L306
.L330:
	xorl	%edi, %edi
	call	exit@PLT
.L319:
	movq	8(%rsp), %rdi
	call	__free_tcb
	jmp	.L321
.L333:
	leaq	1564(%rax), %rbx
	movl	$202, %r9d
	movl	$1, %r8d
	jmp	.L314
.L317:
	movq	8(%rsp), %rax
	testb	$64, 776(%rax)
	je	.L334
.L314:
	xorl	%r10d, %r10d
	xorl	%edx, %edx
	movl	$128, %esi
	movq	%rbx, %rdi
	movl	%r9d, %eax
#APP
# 146 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L317
	addl	$11, %eax
	cmpl	$11, %eax
	ja	.L318
	movl	%eax, %ecx
	movq	%r8, %rsi
	salq	%cl, %rsi
	movq	%rsi, %rax
	testl	$2177, %eax
	je	.L318
	jmp	.L317
.L298:
	movq	%rbp, %rdi
	call	__lll_lock_wait_private
	jmp	.L301
.L308:
	movq	8(%rsp), %rcx
	movl	__nptl_threads_events(%rip), %eax
	orl	1616(%rcx), %eax
	testb	$1, %ah
	je	.L311
	cmpq	$0, 1640(%rcx)
	je	.L335
.L312:
	call	__GI___nptl_death_event
	jmp	.L311
.L332:
	leaq	-16384(%rax), %rsi
	movl	$4, %edx
	movq	%rbx, %rdi
	call	__madvise@PLT
	jmp	.L309
.L334:
	movl	$0, 1564(%rax)
	jmp	.L315
.L331:
	leaq	__PRETTY_FUNCTION__.13177(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC5(%rip), %rdi
	movl	$379, %edx
	call	__assert_fail@PLT
.L335:
	movq	%rcx, %rax
	movl	$9, 1624(%rcx)
	movq	%rcx, 1632(%rax)
.L313:
	movq	__nptl_last_event(%rip), %rax
	movq	8(%rsp), %rcx
	movq	%rax, 1640(%rcx)
	lock cmpxchgq	%rcx, __nptl_last_event(%rip)
	je	.L312
	jmp	.L313
	.size	start_thread, .-start_thread
	.section	.rodata.str1.1
.LC6:
	.string	"powerof2 (pagesize_m1 + 1)"
.LC7:
	.string	"size > adj + TLS_TCB_SIZE"
.LC8:
	.string	"errno == ENOMEM"
.LC9:
	.string	"size != 0"
.LC10:
	.string	"mem != NULL"
.LC11:
	.string	"pthread_create.c"
.LC12:
	.string	"stopped_start"
.LC13:
	.string	"pd->stopped_start"
	.text
	.p2align 4,,15
	.globl	__pthread_create_2_1
	.type	__pthread_create_2_1, @function
__pthread_create_2_1:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	subq	$296, %rsp
	movq	__libc_single_threaded@GOTPCREL(%rip), %rax
	movq	%rdi, 8(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rcx, 24(%rsp)
	cmpb	$0, (%rax)
	je	.L337
	movb	$0, (%rax)
.L337:
	leaq	-1(%r13), %rax
	cmpq	$-3, %rax
	jbe	.L413
	leaq	96(%rsp), %rbp
	movq	%rbp, %rdi
	call	__GI___pthread_getattr_default_np
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L336
	movb	$1, 59(%rsp)
	jmp	.L338
	.p2align 4,,10
	.p2align 3
.L413:
	movq	%r13, %rbp
	movb	$0, 59(%rsp)
.L338:
	call	__getpagesize@PLT
	leal	-1(%rax), %r14d
	movl	%eax, %r12d
	movslq	%r14d, %r14
	leaq	1(%r14), %r8
	movq	%r14, %r15
	andq	%r8, %r15
	jne	.L483
	movq	32(%rbp), %rbx
	testq	%rbx, %rbx
	je	.L484
	testb	$8, 8(%rbp)
	jne	.L485
.L344:
	movq	__static_tls_align_m1(%rip), %rax
	notq	%rax
	andq	%rax, %rbx
	je	.L486
	movq	16(%rbp), %rdx
	negl	%r12d
	movslq	%r12d, %rax
	leaq	(%r14,%rdx), %r12
	andq	%rax, %r12
	cmpq	%r12, %rdx
	jbe	.L487
.L353:
	movl	$22, %r15d
.L346:
	cmpb	$0, 59(%rsp)
	je	.L336
	leaq	96(%rsp), %rdi
	call	__pthread_attr_destroy@PLT
.L336:
	addq	$296, %rsp
	movl	%r15d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L487:
	addq	%r12, %rbx
	movq	%rbx, 32(%rsp)
	jc	.L353
	movq	__static_tls_size(%rip), %rdx
	leaq	2048(%r14,%rdx), %rdx
	addq	%r12, %rdx
	andq	%rdx, %rax
	cmpq	%rax, %rbx
	jb	.L353
	movq	_rtld_global@GOTPCREL(%rip), %rbx
	xorl	%eax, %eax
	movl	$1, %ecx
	movl	4016(%rbx), %r14d
	lock cmpxchgl	%ecx, 4144(%rbx)
	jne	.L488
.L356:
	leal	0(,%r14,4), %eax
	leaq	stack_cache(%rip), %rdx
	andl	$4, %eax
	movl	%eax, 64(%rsp)
	orl	$3, %eax
	movl	%eax, 60(%rsp)
	movq	stack_cache(%rip), %rax
	cmpq	%rdx, %rax
	je	.L357
	xorl	%r14d, %r14d
	movq	32(%rsp), %rdi
	jmp	.L360
	.p2align 4,,10
	.p2align 3
.L358:
	movq	(%rax), %rax
	cmpq	%rdx, %rax
	je	.L359
.L360:
	movl	16(%rax), %ecx
	testl	%ecx, %ecx
	jg	.L358
	movq	984(%rax), %rcx
	cmpq	%rcx, %rdi
	ja	.L358
	cmpq	%rcx, %rdi
	leaq	-704(%rax), %rsi
	je	.L414
	testq	%r14, %r14
	je	.L415
	cmpq	1688(%r14), %rcx
	movq	(%rax), %rax
	cmovb	%rsi, %r14
	cmpq	%rdx, %rax
	jne	.L360
	.p2align 4,,10
	.p2align 3
.L359:
	testq	%r14, %r14
	je	.L357
	movq	32(%rsp), %rax
	salq	$2, %rax
	cmpq	%rax, 1688(%r14)
	ja	.L357
	leaq	704(%r14), %rdi
	movl	$-1, 1564(%r14)
	movq	%rdi, 72(%rsp)
	movq	%rdi, in_flight_stack(%rip)
	movq	712(%r14), %rdx
	movq	704(%r14), %rax
	movq	%rdx, 8(%rax)
	movq	712(%r14), %rdx
	movq	%rax, (%rdx)
	movq	%rdi, %rax
	orq	$1, %rax
	movq	%rax, in_flight_stack(%rip)
	movq	4112(%rbx), %rax
	movq	%rax, 704(%r14)
	leaq	4112(%rbx), %rax
	movq	%rax, 712(%r14)
	movq	4112(%rbx), %rax
	movq	%rdi, 8(%rax)
	movq	%rdi, 4112(%rbx)
	movq	$0, in_flight_stack(%rip)
	movq	1688(%r14), %r8
	xorl	%eax, %eax
	subq	%r8, stack_cache_actsize(%rip)
#APP
# 215 "allocatestack.c" 1
	xchgl %eax, 4144(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L489
.L363:
	movq	8(%r14), %rcx
	pxor	%xmm0, %xmm0
	movq	1680(%r14), %rax
	movq	%r8, 40(%rsp)
	movl	$0, 776(%r14)
	movq	$0, 760(%r14)
	cmpq	$0, -16(%rcx)
	movq	$0, 1640(%r14)
	movq	%rax, 48(%rsp)
	movups	%xmm0, 2424(%r14)
	je	.L417
	movq	%rbx, 64(%rsp)
	movq	%rcx, %rbx
	.p2align 4,,10
	.p2align 3
.L365:
	addq	$1, %r15
	movq	%r15, %rax
	salq	$4, %rax
	movq	8(%rbx,%rax), %rdi
	call	free@PLT
	movq	-16(%rbx), %rax
	cmpq	%rax, %r15
	jb	.L365
	movq	%rbx, %rcx
	leaq	1(%rax), %rdx
	movq	64(%rsp), %rbx
	salq	$4, %rdx
.L364:
	movq	%rcx, %rdi
	xorl	%esi, %esi
	call	memset@PLT
	movq	%r14, %rdi
	call	_dl_allocate_tls_init@PLT
.L366:
	movq	1696(%r14), %rsi
	cmpq	%rsi, %r12
	ja	.L490
	movq	40(%rsp), %rax
	subq	32(%rsp), %rax
	subq	%r12, %rsi
	cmpq	%rax, %rsi
	ja	.L491
.L382:
	movq	%r12, 1704(%r14)
.L351:
	leaq	736(%r14), %rax
	movl	$0, 1560(%r14)
	movq	$-32, 744(%r14)
	movq	$0, 752(%r14)
	movq	%r14, 16(%r14)
	movq	%rax, 728(%r14)
	movq	%rax, 736(%r14)
	movq	%r14, %rax
	subq	__static_tls_size(%rip), %rax
	cmpq	$-1, %r13
	movq	%r14, (%r14)
	sete	2416(%r14)
	movl	8(%rbp), %esi
	leaq	2496(%rax), %rbx
	movq	16(%rsp), %rax
	movl	%esi, %ecx
	andl	$-97, %ecx
	movq	%rax, 1600(%r14)
	movq	24(%rsp), %rax
	movq	%rax, 1608(%r14)
	movq	%fs:16, %rdx
	movl	780(%rdx), %eax
	movdqa	1616(%rdx), %xmm0
	andl	$96, %eax
	orl	%ecx, %eax
	testb	$1, %sil
	movl	$0, %ecx
	cmovne	%r14, %rcx
	movl	%eax, 780(%r14)
	movq	%rcx, 1576(%r14)
	movaps	%xmm0, 1616(%r14)
	movq	1632(%rdx), %rcx
	movq	%rcx, 1632(%r14)
	movl	1596(%rdx), %ecx
	movl	1592(%rdx), %edx
	movl	%ecx, 1596(%r14)
	movl	%edx, 1592(%r14)
#APP
# 697 "pthread_create.c" 1
	movq %fs:40,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 40(%r14)
#APP
# 702 "pthread_create.c" 1
	movq %fs:48,%rdx
# 0 "" 2
#NO_APP
	movq	%rdx, 48(%r14)
#APP
# 22 "../sysdeps/x86/nptl/tls-setup.h" 1
	movl %fs:72,%edx
# 0 "" 2
#NO_APP
	testb	$2, %sil
	movl	%edx, 72(%r14)
	jne	.L492
.L389:
	cmpl	$1, __nptl_nthreads(%rip)
	je	.L493
.L394:
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
#APP
# 751 "pthread_create.c" 1
	lock;incl __nptl_nthreads(%rip)
# 0 "" 2
#NO_APP
	leaq	160(%rsp), %r12
	movb	$0, 94(%rsp)
	movb	$0, 95(%rsp)
	movl	$8, %r10d
	leaq	sigall_set(%rip), %rsi
	xorl	%edi, %edi
	movq	%r12, %rdx
	movl	$14, %eax
#APP
# 71 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.L395
	cmpb	$0, 144(%rax)
	je	.L395
	movdqu	16(%rax), %xmm0
	movaps	%xmm0, 2288(%r14)
	movdqu	32(%rax), %xmm0
	movaps	%xmm0, 2304(%r14)
	movdqu	48(%rax), %xmm0
	movaps	%xmm0, 2320(%r14)
	movdqu	64(%rax), %xmm0
	movaps	%xmm0, 2336(%r14)
	movdqu	80(%rax), %xmm0
	movaps	%xmm0, 2352(%r14)
	movdqu	96(%rax), %xmm0
	movaps	%xmm0, 2368(%r14)
	movdqu	112(%rax), %xmm0
	movaps	%xmm0, 2384(%r14)
	movdqu	128(%rax), %xmm0
	movaps	%xmm0, 2400(%r14)
.L396:
	xorl	%eax, %eax
#APP
# 606 "pthread_create.c" 1
	movb %fs:1553,%al
# 0 "" 2
#NO_APP
	testb	%al, %al
	jne	.L494
.L397:
	leaq	94(%rsp), %rdx
	leaq	95(%rsp), %r8
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	call	create_thread
	movl	%eax, %r15d
.L398:
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	testl	%r15d, %r15d
	jne	.L495
.L402:
	cmpb	$0, 94(%rsp)
	je	.L409
	leaq	1560(%r14), %rdi
	xorl	%eax, %eax
#APP
# 870 "pthread_create.c" 1
	xchgl %eax, 1560(%r14)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L496
.L409:
#APP
# 875 "pthread_create.c" 1
	movl $1,%fs:24
# 0 "" 2
#NO_APP
	xorl	%r15d, %r15d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L484:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, __default_pthread_attr_lock(%rip)
	jne	.L497
.L342:
	movq	32+__default_pthread_attr(%rip), %rbx
	xorl	%eax, %eax
#APP
# 419 "allocatestack.c" 1
	xchgl %eax, __default_pthread_attr_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L498
.L343:
	testb	$8, 8(%rbp)
	je	.L344
	movq	32(%rbp), %rax
	movq	24(%rbp), %rdx
	testq	%rax, %rax
	jne	.L411
.L345:
	movq	__static_tls_align_m1(%rip), %rax
	leaq	-2496(%rdx), %r9
	andq	%r9, %rax
	leaq	2496(%rax), %rcx
	cmpq	%rcx, %rbx
	jbe	.L499
	subq	%rax, %r9
	xorl	%eax, %eax
	subq	%rbx, %rdx
	leaq	8(%r9), %rdi
	movq	%r9, %rcx
	movq	$0, (%r9)
	movq	$0, 2488(%r9)
	movq	%r9, %r14
	andq	$-8, %rdi
	subq	%rdi, %rcx
	addl	$2496, %ecx
	shrl	$3, %ecx
	rep stosq
	leaq	784(%r9), %rax
	movq	%rdx, 1680(%r9)
	movq	%rbx, 1688(%r9)
	movb	$1, 1554(%r9)
	movl	$1, 24(%r9)
	movq	%r9, %rdi
	movq	%rax, 1296(%r9)
	movq	__libc_multiple_threads_ptr(%rip), %rax
	movl	$1, (%rax)
	movl	$-1, 1564(%r9)
	movl	$1, __pthread_multiple_threads(%rip)
	call	_dl_allocate_tls@PLT
	testq	%rax, %rax
	je	.L500
	movq	_rtld_global@GOTPCREL(%rip), %rbx
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 4144(%rbx)
	jne	.L501
.L350:
	movq	4128(%rbx), %rdx
	leaq	704(%r14), %rax
	movq	%rdx, 704(%r14)
	leaq	4128(%rbx), %rdx
	movq	%rdx, 712(%r14)
	movq	4128(%rbx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rax, 4128(%rbx)
	xorl	%eax, %eax
#APP
# 506 "allocatestack.c" 1
	xchgl %eax, 4144(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L351
	leaq	4144(%rbx), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 506 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L351
	.p2align 4,,10
	.p2align 3
.L395:
	movdqa	160(%rsp), %xmm0
	movabsq	$-2147483649, %rax
	movaps	%xmm0, 2288(%r14)
	andq	%rax, 2288(%r14)
	movdqa	176(%rsp), %xmm0
	movaps	%xmm0, 2304(%r14)
	movdqa	192(%rsp), %xmm0
	movaps	%xmm0, 2320(%r14)
	movdqa	208(%rsp), %xmm0
	movaps	%xmm0, 2336(%r14)
	movdqa	224(%rsp), %xmm0
	movaps	%xmm0, 2352(%r14)
	movdqa	240(%rsp), %xmm0
	movaps	%xmm0, 2368(%r14)
	movdqa	256(%rsp), %xmm0
	movaps	%xmm0, 2384(%r14)
	movdqa	272(%rsp), %xmm0
	movaps	%xmm0, 2400(%r14)
	jmp	.L396
	.p2align 4,,10
	.p2align 3
.L415:
	movq	%rsi, %r14
	jmp	.L358
	.p2align 4,,10
	.p2align 3
.L493:
	call	_IO_enable_locks@PLT
	jmp	.L394
	.p2align 4,,10
	.p2align 3
.L495:
	cmpb	$0, 95(%rsp)
	jne	.L502
#APP
# 844 "pthread_create.c" 1
	lock;decl __nptl_nthreads(%rip)
# 0 "" 2
#NO_APP
	xorl	%eax, %eax
#APP
# 848 "pthread_create.c" 1
	xchgl %eax, 1564(%r14)
# 0 "" 2
#NO_APP
	cmpl	$-2, %eax
	je	.L503
.L407:
	movq	%r14, %rdi
	call	__deallocate_stack
.L404:
	cmpl	$12, %r15d
	jne	.L346
.L387:
	movl	$11, %r15d
	jmp	.L346
	.p2align 4,,10
	.p2align 3
.L494:
	movl	__nptl_threads_events(%rip), %eax
	orl	1616(%r14), %eax
	testb	$-128, %al
	je	.L397
	leaq	94(%rsp), %rdx
	leaq	95(%rsp), %r8
	movq	%rbx, %rcx
	movq	%rbp, %rsi
	movq	%r14, %rdi
	movb	$1, 94(%rsp)
	call	create_thread
	testl	%eax, %eax
	movl	%eax, %r15d
	jne	.L398
	cmpb	$0, 94(%rsp)
	je	.L504
	cmpb	$0, 1555(%r14)
	je	.L505
	movl	$8, 1624(%r14)
	movq	%r14, 1632(%r14)
.L401:
	movq	__nptl_last_event(%rip), %rax
	movq	%rax, 1640(%r14)
	lock cmpxchgq	%r14, __nptl_last_event(%rip)
	jne	.L401
	call	__GI___nptl_create_event
	movl	$8, %r10d
	xorl	%edx, %edx
	movq	%r12, %rsi
	movl	$2, %edi
	movl	$14, %eax
#APP
# 105 "../sysdeps/unix/sysv/linux/internal-signals.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L402
	.p2align 4,,10
	.p2align 3
.L492:
	testb	$96, %sil
	je	.L389
	testb	$64, %sil
	je	.L391
	movl	4(%rbp), %edx
	orl	$64, %eax
	movl	%eax, 780(%r14)
	movl	%edx, 1596(%r14)
.L391:
	andl	$32, %esi
	je	.L392
	movl	0(%rbp), %edx
	orl	$32, %eax
	movl	%eax, 780(%r14)
	movl	%edx, 1592(%r14)
.L392:
	movl	%eax, %edx
	andl	$96, %edx
	cmpl	$96, %edx
	je	.L389
	testb	$64, %al
	jne	.L393
	xorl	%edi, %edi
	movl	$145, %eax
#APP
# 31 "../sysdeps/unix/sysv/linux/default-sched.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	movl	%eax, 1596(%r14)
	movl	780(%r14), %eax
	orl	$64, %eax
	movl	%eax, 780(%r14)
.L393:
	testb	$32, %al
	jne	.L389
	leaq	1592(%r14), %rsi
	xorl	%edi, %edi
	movl	$143, %eax
#APP
# 37 "../sysdeps/unix/sysv/linux/default-sched.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	orl	$32, 780(%r14)
	jmp	.L389
	.p2align 4,,10
	.p2align 3
.L485:
	movq	24(%rbp), %rdx
	movq	%rbx, %rax
.L411:
	movq	__static_tls_size(%rip), %rsi
	leaq	2048(%rsi), %rcx
	cmpq	%rax, %rcx
	jbe	.L345
	jmp	.L353
	.p2align 4,,10
	.p2align 3
.L502:
	cmpb	$0, 94(%rsp)
	jne	.L404
	leaq	__PRETTY_FUNCTION__.13693(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$837, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L488:
	leaq	4144(%rbx), %rdi
	movq	%r8, 40(%rsp)
	call	__lll_lock_wait_private
	movq	40(%rsp), %r8
	jmp	.L356
	.p2align 4,,10
	.p2align 3
.L357:
	xorl	%eax, %eax
#APP
# 197 "allocatestack.c" 1
	xchgl %eax, 4144(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L506
.L362:
	movq	32(%rsp), %rax
	movl	$0, %edx
	movl	$131106, %ecx
	addq	%rax, %r8
	testw	%ax, %ax
	cmovne	%rax, %r8
	testq	%r12, %r12
	cmove	60(%rsp), %edx
	movq	%r8, %rsi
	movq	%r8, 40(%rsp)
	xorl	%r9d, %r9d
	xorl	%edi, %edi
	movl	$-1, %r8d
	call	__mmap@PLT
	cmpq	$-1, %rax
	movq	%rax, 48(%rsp)
	je	.L481
	cmpq	$0, 48(%rsp)
	je	.L507
	movq	48(%rsp), %rdi
	movq	40(%rsp), %rsi
	movq	__static_tls_align_m1(%rip), %rax
	leaq	-2496(%rdi,%rsi), %r9
	notq	%rax
	andq	%rax, %r9
	testq	%r12, %r12
	movq	%r9, %r14
	je	.L372
	movl	60(%rsp), %edx
	subq	%r12, %rsi
	addq	%r12, %rdi
	call	__mprotect@PLT
	testl	%eax, %eax
	je	.L372
	movq	errno@gottpoff(%rip), %r15
	movl	%fs:(%r15), %edx
	testl	%edx, %edx
	jne	.L508
.L372:
	movq	48(%rsp), %rax
	movq	%r12, 1696(%r14)
	movq	%r14, %rdi
	movl	$1, 24(%r14)
	movq	%rax, 1680(%r14)
	movq	40(%rsp), %rax
	movq	%rax, 1688(%r14)
	leaq	784(%r14), %rax
	movq	%rax, 1296(%r14)
	movq	__libc_multiple_threads_ptr(%rip), %rax
	movl	$1, (%rax)
	movl	$-1, 1564(%r14)
	movl	$1, __pthread_multiple_threads(%rip)
	call	_dl_allocate_tls@PLT
	testq	%rax, %rax
	je	.L509
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 4144(%rbx)
	jne	.L510
.L375:
	leaq	704(%r14), %rsi
	movq	%rsi, %rax
	movq	%rsi, 72(%rsp)
	orq	$1, %rax
	movq	%rax, in_flight_stack(%rip)
	movq	4112(%rbx), %rax
	movq	%rax, 704(%r14)
	leaq	4112(%rbx), %rax
	movq	%rax, 712(%r14)
	movq	4112(%rbx), %rax
	movq	%rsi, 8(%rax)
	movq	%rsi, 4112(%rbx)
	movq	$0, in_flight_stack(%rip)
	xorl	%eax, %eax
#APP
# 640 "allocatestack.c" 1
	xchgl %eax, 4144(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L511
.L376:
	testb	$1, 4016(%rbx)
	je	.L366
	movl	64(%rsp), %eax
	testl	%eax, %eax
	jne	.L366
	movq	%r14, %rdi
	call	change_stack_perm
	testl	%eax, %eax
	movl	%eax, %r15d
	je	.L366
	movq	40(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	__munmap@PLT
	jmp	.L404
	.p2align 4,,10
	.p2align 3
.L496:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 870 "pthread_create.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L409
	.p2align 4,,10
	.p2align 3
.L500:
	movq	errno@gottpoff(%rip), %rax
	cmpl	$12, %fs:(%rax)
	je	.L387
	leaq	__PRETTY_FUNCTION__.13186(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$495, %edx
	call	__assert_fail@PLT
	.p2align 4,,10
	.p2align 3
.L414:
	movq	%rsi, %r14
	jmp	.L359
	.p2align 4,,10
	.p2align 3
.L497:
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movq	%r8, 32(%rsp)
	call	__lll_lock_wait_private
	movq	32(%rsp), %r8
	jmp	.L342
	.p2align 4,,10
	.p2align 3
.L498:
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	__default_pthread_attr_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 419 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L343
	.p2align 4,,10
	.p2align 3
.L417:
	movl	$16, %edx
	jmp	.L364
.L490:
	movq	48(%rsp), %rdi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	__mprotect@PLT
	testl	%eax, %eax
	je	.L480
.L383:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, 4144(%rbx)
	jne	.L512
.L380:
	movq	72(%rsp), %rax
	movq	%rax, in_flight_stack(%rip)
	movq	712(%r14), %rdx
	movq	704(%r14), %rax
	movq	%rdx, 8(%rax)
	movq	712(%r14), %rdx
	movq	%rax, (%rdx)
	movq	$0, in_flight_stack(%rip)
	xorl	%eax, %eax
#APP
# 686 "allocatestack.c" 1
	xchgl %eax, 4144(%rbx)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jg	.L513
.L381:
	xorl	%esi, %esi
	movq	%r14, %rdi
	call	_dl_deallocate_tls@PLT
	movq	40(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	__munmap@PLT
.L481:
	movq	errno@gottpoff(%rip), %rax
	movl	%fs:(%rax), %r15d
.L370:
	testl	%r15d, %r15d
	jne	.L404
	movq	$0, 16
	ud2
	.p2align 4,,10
	.p2align 3
.L509:
	movq	errno@gottpoff(%rip), %rbx
	cmpl	$12, %fs:(%rbx)
	jne	.L514
	movq	40(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	__munmap@PLT
	movl	%fs:(%rbx), %r15d
	jmp	.L370
.L489:
	leaq	4144(%rbx), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 215 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	movq	1688(%r14), %r8
	jmp	.L363
.L491:
	movq	48(%rsp), %rax
	movl	60(%rsp), %edx
	leaq	(%rax,%r12), %rdi
	call	__mprotect@PLT
	testl	%eax, %eax
	jne	.L383
.L480:
	movq	%r12, 1696(%r14)
	jmp	.L382
.L508:
	movq	40(%rsp), %rsi
	movq	48(%rsp), %rdi
	call	__munmap@PLT
	movl	%fs:(%r15), %r15d
	jmp	.L370
.L501:
	leaq	4144(%rbx), %rdi
	call	__lll_lock_wait_private
	jmp	.L350
.L506:
	leaq	4144(%rbx), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 197 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L362
.L503:
	leaq	1564(%r14), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L407
	cmpl	$-22, %eax
	je	.L407
	cmpl	$-14, %eax
	je	.L407
	leaq	.LC2(%rip), %rdi
	call	__libc_fatal@PLT
	.p2align 4,,10
	.p2align 3
.L511:
	leaq	4144(%rbx), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 640 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L376
.L510:
	leaq	4144(%rbx), %rdi
	call	__lll_lock_wait_private
	jmp	.L375
.L512:
	leaq	4144(%rbx), %rdi
	call	__lll_lock_wait_private
	jmp	.L380
.L513:
	leaq	4144(%rbx), %rdi
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 686 "allocatestack.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L381
.L486:
	leaq	__PRETTY_FUNCTION__.13186(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC9(%rip), %rdi
	movl	$520, %edx
	call	__assert_fail@PLT
.L483:
	leaq	__PRETTY_FUNCTION__.13186(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC6(%rip), %rdi
	movl	$408, %edx
	call	__assert_fail@PLT
.L499:
	leaq	__PRETTY_FUNCTION__.13186(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC7(%rip), %rdi
	movl	$444, %edx
	call	__assert_fail@PLT
.L507:
	leaq	__PRETTY_FUNCTION__.13186(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC10(%rip), %rdi
	movl	$570, %edx
	call	__assert_fail@PLT
.L514:
	leaq	__PRETTY_FUNCTION__.13186(%rip), %rcx
	leaq	.LC3(%rip), %rsi
	leaq	.LC8(%rip), %rdi
	movl	$625, %edx
	call	__assert_fail@PLT
.L505:
	leaq	__PRETTY_FUNCTION__.13693(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC13(%rip), %rdi
	movl	$799, %edx
	call	__assert_fail@PLT
.L504:
	leaq	__PRETTY_FUNCTION__.13693(%rip), %rcx
	leaq	.LC11(%rip), %rsi
	leaq	.LC12(%rip), %rdi
	movl	$798, %edx
	call	__assert_fail@PLT
	.size	__pthread_create_2_1, .-__pthread_create_2_1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.13177, @object
	.size	__PRETTY_FUNCTION__.13177, 19
__PRETTY_FUNCTION__.13177:
	.string	"advise_stack_range"
	.section	.rodata.str1.8
	.align 8
	.type	__PRETTY_FUNCTION__.13521, @object
	.size	__PRETTY_FUNCTION__.13521, 14
__PRETTY_FUNCTION__.13521:
	.string	"create_thread"
	.align 8
	.type	__PRETTY_FUNCTION__.13186, @object
	.size	__PRETTY_FUNCTION__.13186, 15
__PRETTY_FUNCTION__.13186:
	.string	"allocate_stack"
	.section	.rodata.str1.16
	.align 16
	.type	__PRETTY_FUNCTION__.13693, @object
	.size	__PRETTY_FUNCTION__.13693, 21
__PRETTY_FUNCTION__.13693:
	.string	"__pthread_create_2_1"
	.align 16
	.type	__PRETTY_FUNCTION__.13328, @object
	.size	__PRETTY_FUNCTION__.13328, 17
__PRETTY_FUNCTION__.13328:
	.string	"__reclaim_stacks"
	.globl	_thread_db_const_thread_area
	.section	.rodata.cst4,"aM",@progbits,4
	.align 4
	.type	_thread_db_const_thread_area, @object
	.size	_thread_db_const_thread_area, 4
_thread_db_const_thread_area:
	.long	25
	.globl	_thread_db_dtv_slotinfo_list_slotinfo
	.section	.rodata
	.align 8
	.type	_thread_db_dtv_slotinfo_list_slotinfo, @object
	.size	_thread_db_dtv_slotinfo_list_slotinfo, 12
_thread_db_dtv_slotinfo_list_slotinfo:
	.long	128
	.long	0
	.long	16
	.globl	_thread_db_rtld_global__dl_stack_used
	.align 8
	.type	_thread_db_rtld_global__dl_stack_used, @object
	.size	_thread_db_rtld_global__dl_stack_used, 12
_thread_db_rtld_global__dl_stack_used:
	.long	128
	.long	1
	.long	4112
	.globl	_thread_db_rtld_global__dl_stack_user
	.align 8
	.type	_thread_db_rtld_global__dl_stack_user, @object
	.size	_thread_db_rtld_global__dl_stack_user, 12
_thread_db_rtld_global__dl_stack_user:
	.long	128
	.long	1
	.long	4128
	.globl	_thread_db_rtld_global__dl_tls_dtv_slotinfo_list
	.align 8
	.type	_thread_db_rtld_global__dl_tls_dtv_slotinfo_list, @object
	.size	_thread_db_rtld_global__dl_tls_dtv_slotinfo_list, 12
_thread_db_rtld_global__dl_tls_dtv_slotinfo_list:
	.long	64
	.long	1
	.long	4032
	.globl	_thread_db__rtld_global
	.align 8
	.type	_thread_db__rtld_global, @object
	.size	_thread_db__rtld_global, 12
_thread_db__rtld_global:
	.long	33216
	.long	1
	.long	0
	.globl	_thread_db_sizeof_rtld_global
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_rtld_global, @object
	.size	_thread_db_sizeof_rtld_global, 4
_thread_db_sizeof_rtld_global:
	.long	4152
	.globl	_thread_db_dtv_dtv
	.section	.rodata
	.align 8
	.type	_thread_db_dtv_dtv, @object
	.size	_thread_db_dtv_dtv, 12
_thread_db_dtv_dtv:
	.long	128
	.long	134217727
	.long	0
	.globl	_thread_db_link_map_l_tls_offset
	.align 8
	.type	_thread_db_link_map_l_tls_offset, @object
	.size	_thread_db_link_map_l_tls_offset, 12
_thread_db_link_map_l_tls_offset:
	.long	64
	.long	1
	.long	1112
	.globl	_thread_db_link_map_l_tls_modid
	.align 8
	.type	_thread_db_link_map_l_tls_modid, @object
	.size	_thread_db_link_map_l_tls_modid, 12
_thread_db_link_map_l_tls_modid:
	.long	64
	.long	1
	.long	1120
	.globl	_thread_db_pthread_key_data_level2_data
	.align 8
	.type	_thread_db_pthread_key_data_level2_data, @object
	.size	_thread_db_pthread_key_data_level2_data, 12
_thread_db_pthread_key_data_level2_data:
	.long	128
	.long	32
	.long	0
	.globl	_thread_db_sizeof_pthread_key_data_level2
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_pthread_key_data_level2, @object
	.size	_thread_db_sizeof_pthread_key_data_level2, 4
_thread_db_sizeof_pthread_key_data_level2:
	.long	512
	.globl	_thread_db___pthread_keys
	.section	.rodata
	.align 8
	.type	_thread_db___pthread_keys, @object
	.size	_thread_db___pthread_keys, 12
_thread_db___pthread_keys:
	.long	128
	.long	1024
	.long	0
	.globl	_thread_db___nptl_initial_report_events
	.align 8
	.type	_thread_db___nptl_initial_report_events, @object
	.size	_thread_db___nptl_initial_report_events, 12
_thread_db___nptl_initial_report_events:
	.long	8
	.long	1
	.long	0
	.globl	_thread_db___nptl_nthreads
	.align 8
	.type	_thread_db___nptl_nthreads, @object
	.size	_thread_db___nptl_nthreads, 12
_thread_db___nptl_nthreads:
	.long	32
	.long	1
	.long	0
	.globl	_thread_db_td_eventbuf_t_eventdata
	.align 8
	.type	_thread_db_td_eventbuf_t_eventdata, @object
	.size	_thread_db_td_eventbuf_t_eventdata, 12
_thread_db_td_eventbuf_t_eventdata:
	.long	64
	.long	1
	.long	16
	.globl	_thread_db_td_eventbuf_t_eventnum
	.align 8
	.type	_thread_db_td_eventbuf_t_eventnum, @object
	.size	_thread_db_td_eventbuf_t_eventnum, 12
_thread_db_td_eventbuf_t_eventnum:
	.long	32
	.long	1
	.long	8
	.globl	_thread_db_sizeof_td_eventbuf_t
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_td_eventbuf_t, @object
	.size	_thread_db_sizeof_td_eventbuf_t, 4
_thread_db_sizeof_td_eventbuf_t:
	.long	24
	.globl	_thread_db_td_thr_events_t_event_bits
	.section	.rodata
	.align 8
	.type	_thread_db_td_thr_events_t_event_bits, @object
	.size	_thread_db_td_thr_events_t_event_bits, 12
_thread_db_td_thr_events_t_event_bits:
	.long	32
	.long	2
	.long	0
	.globl	_thread_db_sizeof_td_thr_events_t
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_td_thr_events_t, @object
	.size	_thread_db_sizeof_td_thr_events_t, 4
_thread_db_sizeof_td_thr_events_t:
	.long	8
	.globl	_thread_db_list_t_prev
	.section	.rodata
	.align 8
	.type	_thread_db_list_t_prev, @object
	.size	_thread_db_list_t_prev, 12
_thread_db_list_t_prev:
	.long	64
	.long	1
	.long	8
	.globl	_thread_db_dtv_slotinfo_map
	.set	_thread_db_dtv_slotinfo_map,_thread_db_list_t_prev
	.globl	_thread_db_dtv_slotinfo_list_next
	.set	_thread_db_dtv_slotinfo_list_next,_thread_db_list_t_prev
	.globl	_thread_db_pthread_dtvp
	.set	_thread_db_pthread_dtvp,_thread_db_list_t_prev
	.globl	_thread_db_pthread_key_data_data
	.set	_thread_db_pthread_key_data_data,_thread_db_list_t_prev
	.globl	_thread_db_pthread_key_struct_destr
	.set	_thread_db_pthread_key_struct_destr,_thread_db_list_t_prev
	.globl	_thread_db_list_t_next
	.align 8
	.type	_thread_db_list_t_next, @object
	.size	_thread_db_list_t_next, 12
_thread_db_list_t_next:
	.long	64
	.long	1
	.long	0
	.globl	_thread_db_dtv_slotinfo_gen
	.set	_thread_db_dtv_slotinfo_gen,_thread_db_list_t_next
	.globl	_thread_db_dtv_slotinfo_list_len
	.set	_thread_db_dtv_slotinfo_list_len,_thread_db_list_t_next
	.globl	_thread_db_dtv_t_counter
	.set	_thread_db_dtv_t_counter,_thread_db_list_t_next
	.globl	_thread_db_dtv_t_pointer_val
	.set	_thread_db_dtv_t_pointer_val,_thread_db_list_t_next
	.globl	_thread_db_pthread_key_data_seq
	.set	_thread_db_pthread_key_data_seq,_thread_db_list_t_next
	.globl	_thread_db_pthread_key_struct_seq
	.set	_thread_db_pthread_key_struct_seq,_thread_db_list_t_next
	.globl	_thread_db___nptl_last_event
	.set	_thread_db___nptl_last_event,_thread_db_list_t_next
	.globl	_thread_db_sizeof_list_t
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_list_t, @object
	.size	_thread_db_sizeof_list_t, 4
_thread_db_sizeof_list_t:
	.long	16
	.globl	_thread_db_sizeof_dtv_slotinfo
	.set	_thread_db_sizeof_dtv_slotinfo,_thread_db_sizeof_list_t
	.globl	_thread_db_sizeof_dtv_slotinfo_list
	.set	_thread_db_sizeof_dtv_slotinfo_list,_thread_db_sizeof_list_t
	.globl	_thread_db_sizeof_pthread_key_data
	.set	_thread_db_sizeof_pthread_key_data,_thread_db_sizeof_list_t
	.globl	_thread_db_sizeof_pthread_key_struct
	.set	_thread_db_sizeof_pthread_key_struct,_thread_db_sizeof_list_t
	.globl	_thread_db_pthread_nextevent
	.section	.rodata
	.align 8
	.type	_thread_db_pthread_nextevent, @object
	.size	_thread_db_pthread_nextevent, 12
_thread_db_pthread_nextevent:
	.long	64
	.long	1
	.long	1640
	.globl	_thread_db_pthread_eventbuf_eventmask_event_bits
	.align 8
	.type	_thread_db_pthread_eventbuf_eventmask_event_bits, @object
	.size	_thread_db_pthread_eventbuf_eventmask_event_bits, 12
_thread_db_pthread_eventbuf_eventmask_event_bits:
	.long	32
	.long	2
	.long	1616
	.globl	_thread_db_pthread_eventbuf_eventmask
	.align 8
	.type	_thread_db_pthread_eventbuf_eventmask, @object
	.size	_thread_db_pthread_eventbuf_eventmask, 12
_thread_db_pthread_eventbuf_eventmask:
	.long	64
	.long	1
	.long	1616
	.globl	_thread_db_pthread_eventbuf
	.align 8
	.type	_thread_db_pthread_eventbuf, @object
	.size	_thread_db_pthread_eventbuf, 12
_thread_db_pthread_eventbuf:
	.long	192
	.long	1
	.long	1616
	.globl	_thread_db_pthread_specific
	.align 8
	.type	_thread_db_pthread_specific, @object
	.size	_thread_db_pthread_specific, 12
_thread_db_pthread_specific:
	.long	2048
	.long	1
	.long	1296
	.globl	_thread_db_pthread_schedparam_sched_priority
	.align 8
	.type	_thread_db_pthread_schedparam_sched_priority, @object
	.size	_thread_db_pthread_schedparam_sched_priority, 12
_thread_db_pthread_schedparam_sched_priority:
	.long	32
	.long	1
	.long	1592
	.globl	_thread_db_pthread_schedpolicy
	.align 8
	.type	_thread_db_pthread_schedpolicy, @object
	.size	_thread_db_pthread_schedpolicy, 12
_thread_db_pthread_schedpolicy:
	.long	32
	.long	1
	.long	1596
	.globl	_thread_db_pthread_cancelhandling
	.align 8
	.type	_thread_db_pthread_cancelhandling, @object
	.size	_thread_db_pthread_cancelhandling, 12
_thread_db_pthread_cancelhandling:
	.long	32
	.long	1
	.long	776
	.globl	_thread_db_pthread_start_routine
	.align 8
	.type	_thread_db_pthread_start_routine, @object
	.size	_thread_db_pthread_start_routine, 12
_thread_db_pthread_start_routine:
	.long	64
	.long	1
	.long	1600
	.globl	_thread_db_pthread_tid
	.align 8
	.type	_thread_db_pthread_tid, @object
	.size	_thread_db_pthread_tid, 12
_thread_db_pthread_tid:
	.long	32
	.long	1
	.long	720
	.globl	_thread_db_pthread_report_events
	.align 8
	.type	_thread_db_pthread_report_events, @object
	.size	_thread_db_pthread_report_events, 12
_thread_db_pthread_report_events:
	.long	8
	.long	1
	.long	1553
	.globl	_thread_db_pthread_list
	.align 8
	.type	_thread_db_pthread_list, @object
	.size	_thread_db_pthread_list, 12
_thread_db_pthread_list:
	.long	128
	.long	1
	.long	704
	.globl	_thread_db_sizeof_pthread
	.section	.rodata.cst4
	.align 4
	.type	_thread_db_sizeof_pthread, @object
	.size	_thread_db_sizeof_pthread, 4
_thread_db_sizeof_pthread:
	.long	2496
	.local	in_flight_stack
	.comm	in_flight_stack,8,8
	.section	.data.rel.local,"aw",@progbits
	.align 16
	.type	stack_cache, @object
	.size	stack_cache, 16
stack_cache:
	.quad	stack_cache
	.quad	stack_cache
	.local	stack_cache_actsize
	.comm	stack_cache_actsize,8,8
	.hidden	__nptl_nthreads
	.globl	__nptl_nthreads
	.data
	.align 4
	.type	__nptl_nthreads, @object
	.size	__nptl_nthreads, 4
__nptl_nthreads:
	.long	1
	.local	__nptl_last_event
	.comm	__nptl_last_event,8,8
	.local	__nptl_threads_events
	.comm	__nptl_threads_events,8,8
	.hidden	__pthread_debug
	.comm	__pthread_debug,4,4
	.section	.rodata
	.align 32
	.type	sigall_set, @object
	.size	sigall_set, 128
sigall_set:
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.quad	-1
	.hidden	__pthread_multiple_threads
	.hidden	__libc_multiple_threads_ptr
	.hidden	__default_pthread_attr
	.hidden	__static_tls_size
	.hidden	__static_tls_align_m1
	.hidden	__pthread_disable_asynccancel
	.hidden	__pthread_enable_asynccancel
	.hidden	__xidcmd
	.hidden	__default_pthread_attr_lock
	.hidden	__lll_lock_wait_private
