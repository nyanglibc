	.text
	.p2align 4,,15
	.globl	__GI___pthread_mutex_destroy
	.hidden	__GI___pthread_mutex_destroy
	.type	__GI___pthread_mutex_destroy, @function
__GI___pthread_mutex_destroy:
	movl	16(%rdi), %eax
	testb	$16, %al
	jne	.L2
	movl	12(%rdi), %edx
	movl	$16, %eax
	testl	%edx, %edx
	jne	.L1
.L2:
	movl	$-1, 16(%rdi)
	xorl	%eax, %eax
.L1:
	rep ret
	.size	__GI___pthread_mutex_destroy, .-__GI___pthread_mutex_destroy
	.weak	pthread_mutex_destroy
	.set	pthread_mutex_destroy,__GI___pthread_mutex_destroy
	.globl	__pthread_mutex_destroy
	.set	__pthread_mutex_destroy,__GI___pthread_mutex_destroy
