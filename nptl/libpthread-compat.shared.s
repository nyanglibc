	.text
#APP
	.symver __libpthread_version_placeholder0,__libpthread_version_placeholder@GLIBC_2.2.6
#NO_APP
	.section	.text.compat,"ax",@progbits
	.p2align 4,,15
	.type	__libpthread_version_placeholder, @function
__libpthread_version_placeholder:
	rep ret
	.size	__libpthread_version_placeholder, .-__libpthread_version_placeholder
	.globl	__libpthread_version_placeholder0
	.set	__libpthread_version_placeholder0,__libpthread_version_placeholder
