	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __pthread_getaffinity_np,pthread_getaffinity_np@@GLIBC_2.32
	.symver __pthread_getaffinity_alias,pthread_getaffinity_np@GLIBC_2.3.4
	.symver __pthread_getaffinity_old,pthread_getaffinity_np@GLIBC_2.3.3
#NO_APP
	.p2align 4,,15
	.globl	__GI___pthread_getaffinity_np
	.hidden	__GI___pthread_getaffinity_np
	.type	__GI___pthread_getaffinity_np, @function
__GI___pthread_getaffinity_np:
	movq	%rsi, %r8
	cmpq	$2147483647, %rsi
	movl	$2147483647, %esi
	movq	%rdx, %r9
	cmovbe	%r8, %rsi
	movl	720(%rdi), %edi
	movl	$204, %eax
#APP
# 34 "pthread_getaffinity.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpl	$-4096, %eax
	jbe	.L2
	negl	%eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	cltq
	movq	%r8, %rdx
	subq	$8, %rsp
	leaq	(%r9,%rax), %rdi
	subq	%rax, %rdx
	xorl	%esi, %esi
	call	__GI_memset@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	ret
	.size	__GI___pthread_getaffinity_np, .-__GI___pthread_getaffinity_np
	.globl	__pthread_getaffinity_np
	.set	__pthread_getaffinity_np,__GI___pthread_getaffinity_np
	.globl	__pthread_getaffinity_alias
	.set	__pthread_getaffinity_alias,__pthread_getaffinity_np
	.p2align 4,,15
	.globl	__pthread_getaffinity_old
	.type	__pthread_getaffinity_old, @function
__pthread_getaffinity_old:
	movq	%rsi, %rdx
	movl	$128, %esi
	jmp	__GI___pthread_getaffinity_np
	.size	__pthread_getaffinity_old, .-__pthread_getaffinity_old
