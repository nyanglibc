	.text
	.p2align 4,,15
	.globl	pthread_barrierattr_getpshared
	.type	pthread_barrierattr_getpshared, @function
pthread_barrierattr_getpshared:
	movl	(%rdi), %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	pthread_barrierattr_getpshared, .-pthread_barrierattr_getpshared
