	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___libc_alloca_cutoff
	.hidden	__GI___libc_alloca_cutoff
	.type	__GI___libc_alloca_cutoff, @function
__GI___libc_alloca_cutoff:
#APP
# 28 "alloca_cutoff.c" 1
	movq %fs:1688,%rax
# 0 "" 2
#NO_APP
	shrq	$2, %rax
	movl	$65536, %edx
	subq	$1, %rax
	cmpq	$65535, %rax
	ja	.L2
#APP
# 28 "alloca_cutoff.c" 1
	movq %fs:1688,%rdx
# 0 "" 2
#NO_APP
	shrq	$2, %rdx
	movl	$262144, %eax
	testq	%rdx, %rdx
	cmove	%rax, %rdx
.L2:
	xorl	%eax, %eax
	cmpq	%rdi, %rdx
	setnb	%al
	ret
	.size	__GI___libc_alloca_cutoff, .-__GI___libc_alloca_cutoff
	.globl	__libc_alloca_cutoff
	.set	__libc_alloca_cutoff,__GI___libc_alloca_cutoff
