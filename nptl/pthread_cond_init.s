	.text
	.p2align 4,,15
	.globl	__pthread_cond_init
	.hidden	__pthread_cond_init
	.type	__pthread_cond_init, @function
__pthread_cond_init:
	pxor	%xmm0, %xmm0
	testq	%rsi, %rsi
	movups	%xmm0, (%rdi)
	movups	%xmm0, 16(%rdi)
	movups	%xmm0, 32(%rdi)
	je	.L10
	movl	(%rsi), %eax
	testb	$1, %al
	je	.L4
	orl	$1, 36(%rdi)
.L4:
	testb	$2, %al
	je	.L10
	orl	$2, 36(%rdi)
.L10:
	xorl	%eax, %eax
	ret
	.size	__pthread_cond_init, .-__pthread_cond_init
	.weak	pthread_cond_init
	.set	pthread_cond_init,__pthread_cond_init
