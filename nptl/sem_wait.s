	.text
	.p2align 4,,15
	.type	__sem_wait_cleanup, @function
__sem_wait_cleanup:
	movabsq	$-4294967296, %rax
	lock addq	%rax, (%rdi)
	ret
	.size	__sem_wait_cleanup, .-__sem_wait_cleanup
	.p2align 4,,15
	.type	do_futex_wait.constprop.2, @function
do_futex_wait.constprop.2:
	movl	8(%rdi), %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__futex_abstimed_wait_cancelable64@PLT
	.size	do_futex_wait.constprop.2, .-do_futex_wait.constprop.2
	.p2align 4,,15
	.type	__new_sem_wait_slow64.constprop.1, @function
__new_sem_wait_slow64.constprop.1:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movabsq	$4294967296, %rbp
	movq	%rdi, %rbx
	subq	$40, %rsp
	lock xaddq	%rbp, (%rdi)
	leaq	__sem_wait_cleanup(%rip), %rsi
	movabsq	$-4294967297, %r13
	movq	%rsp, %r12
	movq	%rdi, %rdx
	movq	%r12, %rdi
	call	__pthread_cleanup_push@PLT
.L5:
	testl	%ebp, %ebp
	je	.L13
	leaq	0(%rbp,%r13), %rdx
	movq	%rbp, %rax
	lock cmpxchgq	%rdx, (%rbx)
	movq	%rax, %rbp
	jne	.L5
	xorl	%ebx, %ebx
.L9:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	__pthread_cleanup_pop@PLT
	addq	$40, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%rbx, %rdi
	call	do_futex_wait.constprop.2
	cmpl	$110, %eax
	je	.L7
	cmpl	$4, %eax
	je	.L7
	cmpl	$75, %eax
	je	.L7
	movq	(%rbx), %rbp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movabsq	$-4294967296, %rax
	lock addq	%rax, (%rbx)
	movl	$-1, %ebx
	jmp	.L9
	.size	__new_sem_wait_slow64.constprop.1, .-__new_sem_wait_slow64.constprop.1
	.p2align 4,,15
	.globl	__new_sem_wait
	.type	__new_sem_wait, @function
__new_sem_wait:
	pushq	%rbx
	movq	%rdi, %rbx
	call	__pthread_testcancel@PLT
	movq	(%rbx), %rax
	testl	%eax, %eax
	je	.L16
	leaq	-1(%rax), %rdx
	lock cmpxchgq	%rdx, (%rbx)
	jne	.L16
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__new_sem_wait_slow64.constprop.1
	.size	__new_sem_wait, .-__new_sem_wait
	.weak	sem_wait
	.set	sem_wait,__new_sem_wait
	.p2align 4,,15
	.globl	__new_sem_trywait
	.type	__new_sem_trywait, @function
__new_sem_trywait:
	movq	(%rdi), %rax
.L23:
	testl	%eax, %eax
	je	.L22
	leaq	-1(%rax), %rdx
	lock cmpxchgq	%rdx, (%rdi)
	jne	.L23
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L22:
	movq	errno@gottpoff(%rip), %rax
	movl	$11, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__new_sem_trywait, .-__new_sem_trywait
	.weak	sem_trywait
	.set	sem_trywait,__new_sem_trywait
