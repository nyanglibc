	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"The futex facility returned an unexpected error code.\n"
	.text
	.p2align 4,,15
	.globl	__pthread_rwlock_wrlock
	.type	__pthread_rwlock_wrlock, @function
__pthread_rwlock_wrlock:
	movl	24(%rdi), %edx
#APP
# 603 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	cmpl	%eax, %edx
	je	.L47
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %eax
.L3:
	movl	%eax, %ecx
	movl	%eax, %edx
	orl	$2, %ecx
	lock cmpxchgl	%ecx, (%rdi)
	jne	.L3
	testb	$2, %dl
	movq	%rdi, %rbx
	leaq	12(%rdi), %rbp
	movl	$1, %eax
	jne	.L112
.L4:
	testb	$1, %dl
	movl	%eax, 12(%rbx)
	jne	.L22
.L20:
	movl	%edx, %eax
	shrl	$3, %eax
	testl	%eax, %eax
	jne	.L25
	movl	%edx, %ecx
	movl	%edx, %eax
	orl	$1, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	movl	%eax, %edx
	jne	.L21
	movl	$1, 8(%rbx)
.L22:
#APP
# 947 "pthread_rwlock_common.c" 1
	movl %fs:720,%eax
# 0 "" 2
#NO_APP
	movl	%eax, 24(%rbx)
	xorl	%r8d, %r8d
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L47:
	movl	$35, %r8d
	movl	%r8d, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L112:
	movl	48(%rdi), %r12d
	testl	%r12d, %r12d
	jne	.L113
.L5:
	xorl	%ecx, %ecx
	movl	$128, %r13d
.L6:
	testb	$2, %dl
	jne	.L7
	movl	%edx, %esi
	movl	%edx, %eax
	orl	$2, %esi
	lock cmpxchgl	%esi, (%rbx)
	movl	%eax, %edx
	jne	.L6
	testl	%r12d, %r12d
	jne	.L114
.L9:
	orl	$2, %edx
	cmpb	$1, %cl
	sbbl	%eax, %eax
	andl	$-2, %eax
	addl	$3, %eax
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L7:
	testl	%r12d, %r12d
	jne	.L115
.L10:
	movl	28(%rbx), %r8d
	movl	0(%rbp), %eax
	testl	%r8d, %r8d
	movl	%eax, %edx
	cmovne	%r13d, %r8d
	andl	$-3, %edx
	cmpl	$1, %edx
	jne	.L14
	cmpl	$3, %eax
	je	.L13
	movl	$3, %edx
	lock cmpxchgl	%edx, 0(%rbp)
	jne	.L14
.L13:
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$3, %esi
	movq	%rbp, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %r8d
	je	.L53
	cmpl	$75, %eax
	je	.L53
	movl	(%rbx), %edx
	movl	$1, %ecx
	jmp	.L6
.L21:
	testb	$1, %al
	je	.L20
	.p2align 4,,10
	.p2align 3
.L25:
	leaq	8(%rbx), %r12
	xorl	%r13d, %r13d
	.p2align 4,,10
	.p2align 3
.L23:
	movl	(%r12), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$2, %edx
	je	.L45
	testb	%r13b, %r13b
	jne	.L22
	movl	(%rbx), %eax
	testb	$1, %al
	je	.L23
	movl	(%r12), %eax
	movl	%eax, %edx
	orl	$2, %edx
	cmpl	$2, %edx
	jne	.L22
	movl	$1, %r13d
.L45:
	movl	28(%rbx), %r14d
	movl	$128, %edx
	testl	%r14d, %r14d
	cmovne	%edx, %r14d
	testb	$2, %al
	jne	.L30
	movl	$2, %edx
	lock cmpxchgl	%edx, (%r12)
	jne	.L23
.L30:
	movl	%r14d, %r8d
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movl	$2, %esi
	movq	%r12, %rdi
	call	__futex_abstimed_wait64@PLT
	cmpl	$110, %eax
	movl	%eax, %r8d
	je	.L28
	cmpl	$75, %eax
	jne	.L23
.L28:
	movl	48(%rbx), %eax
	testl	%eax, %eax
	je	.L32
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
	testl	%eax, %eax
	je	.L32
	xorl	%ecx, %ecx
	xchgl	0(%rbp), %ecx
.L36:
	movl	%eax, %esi
	orl	$-2147483648, %esi
	lock cmpxchgl	%esi, (%rdx)
	jne	.L34
	andl	$2, %ecx
	je	.L1
	movl	%r14d, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	jbe	.L1
	cmpl	$-22, %eax
	je	.L1
	cmpl	$-14, %eax
	je	.L1
.L35:
	leaq	.LC0(%rip), %rdi
	call	__libc_fatal@PLT
.L34:
	testl	%eax, %eax
	jne	.L36
	movl	%ecx, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L32:
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$1, %edx
	jne	.L44
	xchgl	0(%rbp), %edx
.L43:
	movl	%eax, %ecx
	xorl	$2, %ecx
	andl	$-5, %ecx
	lock cmpxchgl	%ecx, (%rbx)
	movl	%eax, %r8d
	jne	.L38
	andl	$2, %edx
	je	.L40
	movl	%r14d, %esi
	xorl	%r10d, %r10d
	movl	$1, %edx
	xorb	$-127, %sil
	movq	%rbp, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L116
.L40:
	andl	$4, %r8d
	je	.L51
	movl	%r14d, %esi
	xorl	%r10d, %r10d
	movl	$2147483647, %edx
	xorb	$-127, %sil
	movq	%rbx, %rdi
	movl	$202, %eax
#APP
# 209 "../sysdeps/nptl/futex-internal.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	cmpq	$-4096, %rax
	ja	.L117
.L51:
	movl	$110, %r8d
	jmp	.L1
.L38:
	testb	$1, %al
	je	.L43
	movl	%edx, 12(%rbx)
	.p2align 4,,10
	.p2align 3
.L44:
	movl	(%r12), %eax
	orl	$2, %eax
	cmpl	$2, %eax
	jne	.L22
	movl	(%r12), %eax
	orl	$2, %eax
	cmpl	$2, %eax
	je	.L44
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L14:
	movl	(%rbx), %edx
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L113:
	lock addl	$1, 4(%rdi)
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L115:
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rsi
	testl	%eax, %eax
	jns	.L10
	leal	2147483647(%rax), %edi
	lock cmpxchgl	%edi, (%rsi)
	jne	.L6
	movl	(%rbx), %edx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L114:
	lock subl	$1, 4(%rbx)
	jmp	.L9
.L117:
	cmpl	$-22, %eax
	je	.L51
	cmpl	$-14, %eax
	je	.L51
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L116:
	cmpl	$-22, %eax
	je	.L40
	cmpl	$-14, %eax
	je	.L40
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L53:
	testl	%r12d, %r12d
	je	.L1
	movl	4(%rbx), %eax
	leaq	4(%rbx), %rdx
.L18:
	cmpl	$-2147483647, %eax
	je	.L49
	leal	-1(%rax), %ecx
.L17:
	lock cmpxchgl	%ecx, (%rdx)
	jne	.L18
	cmpl	$-2147483647, %eax
	jne	.L1
	movl	(%rbx), %edx
	movl	$3, %eax
	orl	$2, %edx
	jmp	.L4
.L49:
	xorl	%ecx, %ecx
	jmp	.L17
	.size	__pthread_rwlock_wrlock, .-__pthread_rwlock_wrlock
	.weak	pthread_rwlock_wrlock
	.set	pthread_rwlock_wrlock,__pthread_rwlock_wrlock
