	.text
#APP
	.symver __new_sem_destroy,sem_destroy@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__new_sem_destroy
	.type	__new_sem_destroy, @function
__new_sem_destroy:
	xorl	%eax, %eax
	ret
	.size	__new_sem_destroy, .-__new_sem_destroy
