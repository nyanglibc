	.text
	.p2align 4,,15
	.type	unwind_stop, @function
unwind_stop:
	pushq	%r15
	pushq	%r14
	movq	%r9, %r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	movq	%fs:16, %rax
#APP
# 47 "unwind.c" 1
	movq %fs:760,%r13
# 0 "" 2
#NO_APP
	movl	%esi, %ebx
	movq	1688(%rax), %rbp
	addq	1680(%rax), %rbp
	andl	$16, %ebx
	movq	%r13, %r12
	je	.L19
.L2:
	testq	%r13, %r13
	je	.L11
	movq	80(%r14), %r15
	movq	%r8, %rdi
	movl	$1, %ebx
	call	_Unwind_GetCFA@PLT
	cmpq	%r15, %r13
	je	.L11
.L6:
	subq	%rbp, %rax
	movq	%rax, 8(%rsp)
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L8:
	testl	%ebx, %ebx
	jne	.L10
	movq	%r13, %rcx
	movq	%r13, %rdx
	subq	%rbp, %rcx
	cmpq	%rcx, 8(%rsp)
	jb	.L9
.L10:
	movq	%r13, %r12
.L7:
	movq	24(%r12), %r13
	movq	8(%r12), %rdi
	call	*(%r12)
	cmpq	%r13, %r15
	jne	.L8
	movq	%r15, %rdx
.L9:
#APP
# 90 "unwind.c" 1
	movq %rdx,%fs:760
# 0 "" 2
#NO_APP
	testl	%ebx, %ebx
	je	.L16
	.p2align 4,,10
	.p2align 3
.L11:
	movl	$1, %esi
	movq	%r14, %rdi
	call	__libc_longjmp@PLT
	.p2align 4,,10
	.p2align 3
.L19:
	movq	%r8, %rdi
	movq	%r8, 8(%rsp)
	call	_Unwind_GetCFA@PLT
	movq	48(%r14), %rdx
	subq	%rbp, %rax
	movq	8(%rsp), %r8
#APP
# 40 "../sysdeps/x86_64/jmpbuf-unwind.h" 1
	ror $2*8+1, %rdx
xor %fs:48, %rdx
# 0 "" 2
#NO_APP
	subq	%rbp, %rdx
	cmpq	%rdx, %rax
	jnb	.L2
	testq	%r13, %r13
	jne	.L13
.L16:
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
.L13:
	movq	80(%r14), %r15
	movq	%r8, %rdi
	call	_Unwind_GetCFA@PLT
	cmpq	%r15, %r12
	je	.L16
	movq	%rax, %rcx
	movq	%r12, %rdx
	subq	%rbp, %rcx
	subq	%rbp, %rdx
	cmpq	%rdx, %rcx
	jnb	.L6
	jmp	.L16
	.size	unwind_stop, .-unwind_stop
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align 8
.LC0:
	.string	"FATAL: exception not rethrown\n"
	.text
	.p2align 4,,15
	.type	unwind_cleanup, @function
unwind_cleanup:
	leaq	.LC0(%rip), %rdi
	subq	$8, %rsp
	call	__libc_fatal@PLT
	.size	unwind_cleanup, .-unwind_cleanup
	.p2align 4,,15
	.globl	__GI___pthread_unwind
	.hidden	__GI___pthread_unwind
	.type	__GI___pthread_unwind, @function
__GI___pthread_unwind:
	subq	$8, %rsp
	movq	%rdi, %rdx
#APP
# 128 "unwind.c" 1
	movq $0,%fs:1648
# 0 "" 2
#NO_APP
	leaq	unwind_cleanup(%rip), %rax
#APP
# 129 "unwind.c" 1
	movq %rax,%fs:1656
# 0 "" 2
#NO_APP
	movq	%fs:16, %rax
	leaq	unwind_stop(%rip), %rsi
	leaq	1648(%rax), %rdi
	call	_Unwind_ForcedUnwind@PLT
	call	abort@PLT
	.size	__GI___pthread_unwind, .-__GI___pthread_unwind
	.globl	__pthread_unwind
	.set	__pthread_unwind,__GI___pthread_unwind
	.p2align 4,,15
	.globl	__GI___pthread_unwind_next
	.hidden	__GI___pthread_unwind_next
	.type	__GI___pthread_unwind_next, @function
__GI___pthread_unwind_next:
	subq	$8, %rsp
	movq	72(%rdi), %rdi
	call	__GI___pthread_unwind
	.size	__GI___pthread_unwind_next, .-__GI___pthread_unwind_next
	.globl	__pthread_unwind_next
	.set	__pthread_unwind_next,__GI___pthread_unwind_next
