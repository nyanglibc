	.text
	.p2align 4,,15
	.globl	__pthread_cleanup_routine
	.type	__pthread_cleanup_routine, @function
__pthread_cleanup_routine:
	movl	16(%rdi), %edx
	movq	%rdi, %rax
	testl	%edx, %edx
	je	.L1
	movq	8(%rdi), %rdi
	jmp	*(%rax)
	.p2align 4,,10
	.p2align 3
.L1:
	rep ret
	.size	__pthread_cleanup_routine, .-__pthread_cleanup_routine
