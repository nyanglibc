	.text
	.p2align 4,,15
	.type	__sem_wait_cleanup, @function
__sem_wait_cleanup:
	movabsq	$-4294967296, %rax
	lock addq	%rax, (%rdi)
	ret
	.size	__sem_wait_cleanup, .-__sem_wait_cleanup
	.p2align 4,,15
	.type	do_futex_wait.constprop.2, @function
do_futex_wait.constprop.2:
	movl	8(%rdi), %r8d
	movq	%rsi, %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	__GI___futex_abstimed_wait_cancelable64
	.size	do_futex_wait.constprop.2, .-do_futex_wait.constprop.2
	.p2align 4,,15
	.type	__new_sem_wait_slow64.constprop.1, @function
__new_sem_wait_slow64.constprop.1:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movabsq	$4294967296, %rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	lock xaddq	%rbp, (%rdi)
	leaq	__sem_wait_cleanup(%rip), %rsi
	movabsq	$-4294967297, %r13
	movq	%rsp, %r12
	movq	%rdi, %rdx
	movq	%r12, %rdi
	call	__pthread_cleanup_push
.L5:
	testl	%ebp, %ebp
	je	.L13
	leaq	0(%rbp,%r13), %rdx
	movq	%rbp, %rax
	lock cmpxchgq	%rdx, (%rbx)
	movq	%rax, %rbp
	jne	.L5
	xorl	%ebx, %ebx
.L9:
	movq	%r12, %rdi
	xorl	%esi, %esi
	call	__pthread_cleanup_pop
	addq	$32, %rsp
	movl	%ebx, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L13:
	movq	%r14, %rsi
	movq	%rbx, %rdi
	call	do_futex_wait.constprop.2
	cmpl	$110, %eax
	je	.L7
	cmpl	$4, %eax
	je	.L7
	cmpl	$75, %eax
	je	.L7
	movq	(%rbx), %rbp
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L7:
	movq	errno@gottpoff(%rip), %rdx
	movl	%eax, %fs:(%rdx)
	movabsq	$-4294967296, %rax
	lock addq	%rax, (%rbx)
	movl	$-1, %ebx
	jmp	.L9
	.size	__new_sem_wait_slow64.constprop.1, .-__new_sem_wait_slow64.constprop.1
	.p2align 4,,15
	.globl	__sem_timedwait
	.type	__sem_timedwait, @function
__sem_timedwait:
	cmpq	$999999999, 8(%rsi)
	ja	.L25
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	subq	$8, %rsp
	call	__GI___pthread_testcancel
	movq	(%rbx), %rax
	testl	%eax, %eax
	je	.L18
	leaq	-1(%rax), %rdx
	lock cmpxchgq	%rdx, (%rbx)
	jne	.L18
	addq	$8, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	addq	$8, %rsp
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__new_sem_wait_slow64.constprop.1
	.p2align 4,,10
	.p2align 3
.L25:
	movq	errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movl	$-1, %eax
	ret
	.size	__sem_timedwait, .-__sem_timedwait
	.weak	sem_timedwait
	.set	sem_timedwait,__sem_timedwait
	.hidden	__pthread_cleanup_pop
	.hidden	__pthread_cleanup_push
