	.text
	.p2align 4,,15
	.globl	__pthread_attr_getdetachstate
	.type	__pthread_attr_getdetachstate, @function
__pthread_attr_getdetachstate:
	movl	8(%rdi), %eax
	andl	$1, %eax
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	ret
	.size	__pthread_attr_getdetachstate, .-__pthread_attr_getdetachstate
	.globl	pthread_attr_getdetachstate
	.set	pthread_attr_getdetachstate,__pthread_attr_getdetachstate
