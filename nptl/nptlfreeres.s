	.text
	.p2align 4,,15
	.globl	__libpthread_freeres
	.type	__libpthread_freeres, @function
__libpthread_freeres:
	subq	$8, %rsp
	cmpq	$0, __default_pthread_attr_freeres@GOTPCREL(%rip)
	je	.L2
	call	__default_pthread_attr_freeres@PLT
.L2:
	cmpq	$0, __nptl_stacks_freeres@GOTPCREL(%rip)
	je	.L3
	call	__nptl_stacks_freeres@PLT
.L3:
	cmpq	$0, __shm_directory_freeres@GOTPCREL(%rip)
	je	.L4
	call	__shm_directory_freeres@PLT
.L4:
	cmpq	$0, __nptl_unwind_freeres@GOTPCREL(%rip)
	je	.L1
	addq	$8, %rsp
	jmp	__nptl_unwind_freeres@PLT
	.p2align 4,,10
	.p2align 3
.L1:
	addq	$8, %rsp
	ret
	.size	__libpthread_freeres, .-__libpthread_freeres
	.weak	__nptl_unwind_freeres
	.weak	__shm_directory_freeres
	.weak	__nptl_stacks_freeres
	.weak	__default_pthread_attr_freeres
