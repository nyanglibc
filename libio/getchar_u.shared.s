	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	getchar_unlocked
	.type	getchar_unlocked, @function
getchar_unlocked:
	movq	stdin@GOTPCREL(%rip), %rax
	movq	(%rax), %rdi
	movq	8(%rdi), %rax
	cmpq	16(%rdi), %rax
	jnb	.L4
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	(%rax), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	jmp	__GI___uflow
	.size	getchar_unlocked, .-getchar_unlocked
