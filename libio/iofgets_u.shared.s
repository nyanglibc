	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fgets_unlocked
	.hidden	__GI___fgets_unlocked
	.type	__GI___fgets_unlocked, @function
__GI___fgets_unlocked:
	testl	%esi, %esi
	jle	.L7
	cmpl	$1, %esi
	je	.L16
	pushq	%r12
	pushq	%rbp
	subl	$1, %esi
	pushq	%rbx
	movq	%rdx, %rbx
	movl	(%rdx), %edx
	movq	%rdi, %r12
	movl	$1, %r8d
	movl	$10, %ecx
	movl	%edx, %ebp
	andl	$-33, %edx
	movl	%edx, (%rbx)
	movslq	%esi, %rdx
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	andl	$32, %ebp
	call	__GI__IO_getline
	xorl	%edi, %edi
	testq	%rax, %rax
	movl	(%rbx), %edx
	je	.L5
	testb	$32, %dl
	je	.L6
	movq	__libc_errno@gottpoff(%rip), %rcx
	xorl	%edi, %edi
	cmpl	$11, %fs:(%rcx)
	je	.L6
.L5:
	orl	%edx, %ebp
	movq	%rdi, %rax
	movl	%ebp, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movb	$0, (%r12,%rax)
	movl	(%rbx), %edx
	movq	%r12, %rdi
	movq	%rdi, %rax
	orl	%edx, %ebp
	movl	%ebp, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movb	$0, (%rdi)
.L13:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%edi, %edi
	jmp	.L13
	.size	__GI___fgets_unlocked, .-__GI___fgets_unlocked
	.globl	__fgets_unlocked
	.set	__fgets_unlocked,__GI___fgets_unlocked
	.weak	__GI_fgets_unlocked
	.hidden	__GI_fgets_unlocked
	.set	__GI_fgets_unlocked,__fgets_unlocked
	.weak	fgets_unlocked
	.set	fgets_unlocked,__GI_fgets_unlocked
