	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_seekoff_unlocked
	.hidden	_IO_seekoff_unlocked
	.type	_IO_seekoff_unlocked, @function
_IO_seekoff_unlocked:
.LFB68:
	cmpl	$2, %edx
	ja	.L22
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	testl	%ecx, %ecx
	je	.L4
	movl	192(%rdi), %eax
	testl	%eax, %eax
	js	.L27
	je	.L4
	movq	160(%rdi), %rax
	cmpq	$0, 64(%rax)
	je	.L4
	cmpl	$1, %edx
	je	.L28
.L12:
	movq	%rbx, %rdi
	movl	%ecx, 12(%rsp)
	movl	%edx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	__GI__IO_free_wbackup_area
	movl	12(%rsp), %ecx
	movl	8(%rsp), %edx
	movq	(%rsp), %rsi
	.p2align 4,,10
	.p2align 3
.L4:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdi
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdi, %rax
	movq	%rbp, %r8
	subq	%rdi, %r8
	cmpq	%r8, %rax
	jbe	.L29
.L9:
	movq	72(%rbp), %rax
	addq	$24, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L27:
	cmpq	$0, 72(%rdi)
	je	.L4
	cmpl	$1, %edx
	jne	.L8
	testl	$256, (%rdi)
	je	.L8
	movq	16(%rbx), %rax
	subq	8(%rbx), %rax
	subq	%rax, %rsi
.L8:
	movq	%rbx, %rdi
	movl	%ecx, 12(%rsp)
	movl	%edx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	__GI__IO_free_backup_area
	movq	(%rsp), %rsi
	movl	8(%rsp), %edx
	movl	12(%rsp), %ecx
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L29:
	movl	%ecx, 12(%rsp)
	movl	%edx, 8(%rsp)
	movq	%rsi, (%rsp)
	call	_IO_vtable_check
	movl	12(%rsp), %ecx
	movl	8(%rsp), %edx
	movq	(%rsp), %rsi
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L22:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	movq	$-1, %rax
	ret
.L28:
	testl	$256, (%rdi)
	je	.L12
	call	__GI_abort
.LFE68:
	.size	_IO_seekoff_unlocked, .-_IO_seekoff_unlocked
	.p2align 4,,15
	.globl	_IO_seekoff
	.type	_IO_seekoff, @function
_IO_seekoff:
.LFB69:
	pushq	%r14
	pushq	%r13
	movl	%ecx, %r13d
	pushq	%r12
	movl	%edx, %r12d
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	movq	%rsi, %rbp
	andl	$32768, %edx
	jne	.L31
	movq	136(%rdi), %rdi
	movq	%fs:16, %r14
	cmpq	%r14, 8(%rdi)
	je	.L32
#APP
# 68 "ioseekoff.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L33
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L34:
	movq	136(%rbx), %rdi
	movq	%r14, 8(%rdi)
.L32:
	addl	$1, 4(%rdi)
.L31:
	movl	%r13d, %ecx
	movl	%r12d, %edx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_seekoff_unlocked
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L30
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L30
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L40
	subl	$1, (%rdi)
.L30:
	popq	%rbx
	movq	%r8, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L33:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L34
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L34
	.p2align 4,,10
	.p2align 3
.L40:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L30
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L30
.L44:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L42
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L42
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L43
	subl	$1, (%rdi)
.L42:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L43:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L42
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L42
.LFE69:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA69:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE69-.LLSDACSB69
.LLSDACSB69:
	.uleb128 .LEHB0-.LFB69
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L44-.LFB69
	.uleb128 0
	.uleb128 .LEHB1-.LFB69
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE69:
	.text
	.size	_IO_seekoff, .-_IO_seekoff
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
