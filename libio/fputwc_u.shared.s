	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	fputwc_unlocked
	.type	fputwc_unlocked, @function
fputwc_unlocked:
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movl	$1, %esi
	movq	%rbx, %rdi
	subq	$8, %rsp
	call	_IO_fwide@PLT
	testl	%eax, %eax
	js	.L5
	movq	160(%rbx), %rdx
	movl	%ebp, %eax
	testq	%rdx, %rdx
	je	.L3
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L3
	leaq	4(%rcx), %rsi
	movq	%rsi, 32(%rdx)
	movl	%ebp, (%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L3:
	addq	$8, %rsp
	movl	%ebp, %esi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	__GI___woverflow
	.size	fputwc_unlocked, .-fputwc_unlocked
