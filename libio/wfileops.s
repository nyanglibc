	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"wfileops.c"
.LC1:
	.string	"status == __codecvt_partial"
	.text
	.p2align 4,,15
	.globl	_IO_wfile_underflow
	.hidden	_IO_wfile_underflow
	.type	_IO_wfile_underflow, @function
_IO_wfile_underflow:
.LFB72:
	movl	(%rdi), %eax
	testb	$16, %al
	jne	.L78
	testb	$4, %al
	jne	.L79
	movq	160(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jb	.L80
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$88, %rsp
	movq	8(%rdi), %rdx
	movq	16(%rdi), %rcx
	movq	152(%rdi), %r15
	cmpq	%rcx, %rdx
	jb	.L81
	movq	56(%rdi), %rax
	movq	%rax, 16(%rdi)
	movq	%rax, 8(%rdi)
	movq	%rax, 24(%rdi)
.L10:
	testq	%rax, %rax
	je	.L82
.L11:
	movq	%rax, 48(%rbx)
	movq	%rax, 40(%rbx)
	movq	%rax, 32(%rbx)
	movq	160(%rbx), %rax
	cmpq	$0, 48(%rax)
	je	.L83
.L13:
	testl	$514, (%rbx)
	je	.L84
	movq	stdout(%rip), %rbp
	movl	0(%rbp), %edx
	movl	%edx, %esi
	andl	$32768, %esi
	jne	.L51
	movq	136(%rbp), %rcx
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rcx)
	je	.L52
#APP
# 211 "wfileops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L19
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rcx)
# 0 "" 2
#NO_APP
.L20:
	movq	stdout(%rip), %rdi
	movq	136(%rbp), %rcx
	movl	(%rdi), %edx
	movq	%r12, 8(%rcx)
.L18:
	addl	$1, 4(%rcx)
.L17:
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	andl	$648, %edx
	movq	%r12, %rax
	subq	%r13, %rax
	cmpl	$640, %edx
	movq	%rax, (%rsp)
	je	.L23
.L27:
	testl	$32768, 0(%rbp)
	jne	.L25
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L25
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L28
	subl	$1, (%rdi)
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L80:
	movl	(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L84:
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
.L25:
	subq	%r13, %r12
	movq	%r12, (%rsp)
.L16:
	movq	%rbx, %rdi
	leaq	64(%rsp), %r12
	xorl	%ebp, %ebp
.LEHB0:
	call	_IO_switch_to_get_mode
	movq	160(%rbx), %rax
	movq	48(%rax), %rdx
	movq	%rdx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rdx, 8(%rax)
	movq	%rdx, 40(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 24(%rax)
	leaq	56(%rsp), %rax
	movq	%rax, 8(%rsp)
	.p2align 4,,10
	.p2align 3
.L29:
	movq	216(%rbx), %r14
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	(%rsp), %rax
	jnb	.L85
.L30:
	movq	16(%rbx), %rsi
	movq	64(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	*112(%r14)
	testq	%rax, %rax
	jle	.L86
	movq	16(%rbx), %rcx
	movq	144(%rbx), %rdx
	addq	%rax, %rcx
	cmpq	$-1, %rdx
	movq	%rcx, 16(%rbx)
	je	.L50
	addq	%rax, %rdx
	movq	%rdx, 144(%rbx)
.L50:
	movq	160(%rbx), %r14
	movq	8(%rbx), %rsi
	testq	%rbp, %rbp
	movq	88(%r14), %rdx
	leaq	8(%r14), %r9
	leaq	88(%r14), %r10
	movq	%rdx, 96(%r14)
	movq	%rsi, 24(%rbx)
	jne	.L87
	pushq	%r9
	pushq	56(%r14)
	movq	%rsi, %rdx
	movq	24(%rsp), %r8
	movq	8(%r14), %r9
	movq	%r10, %rsi
	movq	%r15, %rdi
	call	__libio_codecvt_in
	popq	%rdx
	popq	%rcx
.L35:
	movq	56(%rsp), %rsi
	xorl	%ebp, %ebp
	movq	%rsi, 8(%rbx)
.L36:
	movq	160(%rbx), %rdx
	movq	48(%rdx), %rdi
	cmpq	%rdi, 8(%rdx)
	jne	.L37
	cmpl	$2, %eax
	je	.L43
	cmpl	$1, %eax
	jne	.L88
	testq	%rbp, %rbp
	jne	.L40
	movq	8(%rbx), %rsi
	movq	24(%rbx), %r14
	movq	16(%rbx), %rcx
	subq	%rsi, %rcx
	cmpq	%rsi, %r14
	jb	.L89
	cmpq	$15, %rcx
	ja	.L43
	movq	%rcx, %rdx
	movq	%r12, %rdi
	movq	%rcx, 16(%rsp)
	call	memcpy@PLT
	movq	16(%rsp), %rcx
	movq	%rcx, %rbp
.L44:
	movq	%r14, 16(%rbx)
	movq	%r14, 8(%rbx)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L83:
	movq	64(%rax), %rdi
	testq	%rdi, %rdi
	je	.L14
	call	free@PLT
	andl	$-257, (%rbx)
.L14:
	movq	%rbx, %rdi
	call	_IO_wdoallocbuf
	jmp	.L13
	.p2align 4,,10
	.p2align 3
.L82:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L12
	call	free@PLT
	andl	$-257, (%rbx)
.L12:
	movq	%rbx, %rdi
	call	_IO_doallocbuf
	movq	56(%rbx), %rax
	movq	%rax, 16(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, 24(%rbx)
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	movq	%rsi, %rax
	subq	%r12, %rax
	jne	.L90
.L45:
	cmpq	$16, %rbp
	je	.L43
	movq	24(%rbx), %r14
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L86:
	orq	%rbp, %rax
	movl	(%rbx), %edx
	jne	.L32
	orl	$16, %edx
	movq	$-1, 144(%rbx)
	movl	%edx, (%rbx)
.L76:
	movl	$-1, %eax
.L1:
	addq	$88, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L89:
	movq	%rcx, %rdx
	movq	%r14, %rdi
	movq	%rcx, 16(%rsp)
	call	memmove
	movq	24(%rbx), %rax
	movq	16(%rsp), %rcx
	subq	%rcx, 16(%rbx)
	movq	%rax, 8(%rbx)
	jmp	.L29
	.p2align 4,,10
	.p2align 3
.L90:
	subq	%rax, %rbp
	movq	%r12, %rdi
	movq	%rbp, %rdx
	call	memmove
	jmp	.L45
	.p2align 4,,10
	.p2align 3
.L85:
	call	_IO_vtable_check
	jmp	.L30
	.p2align 4,,10
	.p2align 3
.L87:
	movl	$16, %r8d
	leaq	(%r12,%rbp), %r11
	movq	%r10, 40(%rsp)
	subq	%rbp, %r8
	movq	%r9, 32(%rsp)
	cmpq	%rax, %r8
	movq	%r11, %rdi
	movq	%r11, 16(%rsp)
	cmova	%rax, %r8
	movq	%r8, %rdx
	movq	%r8, 24(%rsp)
	call	__mempcpy@PLT
	movq	32(%rsp), %r9
	movq	24(%rsp), %r8
	movq	%r15, %rdi
	movq	%rax, %rcx
	movq	%r12, %rdx
	pushq	%r9
	pushq	56(%r14)
	addq	%r8, %rbp
	movq	56(%rsp), %r10
	movq	24(%rsp), %r8
	movq	8(%r14), %r9
	movq	%r10, %rsi
	call	__libio_codecvt_in
	testq	%rbp, %rbp
	popq	%rsi
	popq	%rdi
	je	.L35
	movq	56(%rsp), %rsi
	movq	16(%rsp), %r11
	movl	$0, %edi
	movq	%rsi, %rdx
	subq	%r11, %rdx
	cmovs	%rdi, %rdx
	addq	%rdx, 8(%rbx)
	jmp	.L36
	.p2align 4,,10
	.p2align 3
.L32:
	orl	$32, %edx
	testq	%rbp, %rbp
	movl	%edx, (%rbx)
	je	.L76
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	movl	$-1, %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L78:
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	cmpl	$2, %eax
	jne	.L9
	.p2align 4,,10
	.p2align 3
.L43:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	orl	$32, (%rbx)
	jmp	.L76
	.p2align 4,,10
	.p2align 3
.L81:
	movq	48(%rax), %r9
	movq	88(%rax), %rsi
	leaq	8(%rax), %rdi
	movq	%rdx, 64(%rsp)
	movq	%rsi, 96(%rax)
	movq	%r9, (%rax)
	leaq	88(%rax), %rsi
	movq	%r9, 16(%rax)
	pushq	%rdi
	movq	%r15, %rdi
	pushq	56(%rax)
	leaq	80(%rsp), %r8
	call	__libio_codecvt_in
.LEHE0:
	movq	8(%rbx), %rdx
	movq	80(%rsp), %rsi
	movq	%rdx, 24(%rbx)
	movq	160(%rbx), %rdx
	movq	%rsi, 8(%rbx)
	popq	%r8
	movq	(%rdx), %rcx
	cmpq	8(%rdx), %rcx
	popq	%r9
	jnb	.L7
	movl	(%rcx), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L51:
	movq	%rbp, %rdi
	jmp	.L17
	.p2align 4,,10
	.p2align 3
.L79:
	orl	$32, %eax
	movl	%eax, (%rdi)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L37:
	movq	(%rdx), %rax
	movl	(%rax), %eax
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L23:
	movq	216(%rdi), %rax
	movq	%rax, %r14
	subq	%r13, %rax
	cmpq	(%rsp), %rax
	jnb	.L91
.L26:
	movq	%r14, %rax
	movl	$-1, %esi
.LEHB1:
	call	*24(%rax)
	jmp	.L27
.L9:
	movq	16(%rbx), %rdx
	movq	56(%rbx), %rdi
	subq	%rsi, %rdx
	call	memmove
	movq	56(%rbx), %rax
	movq	16(%rbx), %rdx
	addq	%rax, %rdx
	subq	8(%rbx), %rdx
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rdx, 16(%rbx)
	jmp	.L10
.L52:
	movq	%rbp, %rdi
	jmp	.L18
.L19:
	movl	%esi, %eax
	lock cmpxchgl	%edx, (%rcx)
	je	.L20
	movq	%rcx, %rdi
	call	__lll_lock_wait_private
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L91:
	call	_IO_vtable_check
.LEHE1:
	movq	stdout(%rip), %rdi
	jmp	.L26
.L28:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L25
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L16
.L88:
	leaq	__PRETTY_FUNCTION__.11489(%rip), %rcx
	leaq	.LC0(%rip), %rsi
	leaq	.LC1(%rip), %rdi
	movl	$290, %edx
	call	__assert_fail
.L53:
	testl	$32768, 0(%rbp)
	movq	%rax, %r8
	jne	.L47
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L47
	movq	$0, 8(%rdi)
#APP
# 885 "../libio/libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L48
	subl	$1, (%rdi)
.L47:
	movq	%r8, %rdi
.LEHB2:
	call	_Unwind_Resume@PLT
.LEHE2:
.L48:
#APP
# 885 "../libio/libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L47
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "../libio/libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L47
.LFE72:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA72:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE72-.LLSDACSB72
.LLSDACSB72:
	.uleb128 .LEHB0-.LFB72
	.uleb128 .LEHE0-.LEHB0
	.uleb128 0
	.uleb128 0
	.uleb128 .LEHB1-.LFB72
	.uleb128 .LEHE1-.LEHB1
	.uleb128 .L53-.LFB72
	.uleb128 0
	.uleb128 .LEHB2-.LFB72
	.uleb128 .LEHE2-.LEHB2
	.uleb128 0
	.uleb128 0
.LLSDACSE72:
	.text
	.size	_IO_wfile_underflow, .-_IO_wfile_underflow
	.p2align 4,,15
	.type	adjust_wide_data, @function
adjust_wide_data:
.LFB77:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movq	152(%rdi), %rbp
	movq	%rbp, %rdi
	call	__libio_codecvt_encoding
	cmpb	$1, %r12b
	je	.L93
	testl	%eax, %eax
	jle	.L93
	movq	8(%rbx), %rdx
	subq	24(%rbx), %rdx
	movslq	%eax, %rcx
	movq	160(%rbx), %rsi
	movq	%rdx, %rax
	cqto
	idivq	%rcx
	movq	8(%rsi), %rdx
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 8(%rsi)
.L94:
	movq	%rax, (%rsi)
	xorl	%eax, %eax
.L92:
	addq	$16, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L93:
	movq	24(%rbx), %rdx
	leaq	8(%rsp), %r12
	movq	%rdx, 8(%rsp)
.L98:
	movq	160(%rbx), %rax
	movq	%r12, %r8
	movq	88(%rax), %rcx
	leaq	8(%rax), %rdi
	leaq	88(%rax), %rsi
	movq	%rcx, 96(%rax)
	pushq	%rdi
	movq	%rbp, %rdi
	pushq	56(%rax)
	movq	8(%rbx), %rcx
	movq	16(%rax), %r9
	call	__libio_codecvt_in
	cmpl	$2, %eax
	popq	%rdx
	popq	%rcx
	je	.L103
	cmpl	$1, %eax
	je	.L104
	movq	160(%rbx), %rsi
	movq	8(%rsi), %rax
	jmp	.L94
	.p2align 4,,10
	.p2align 3
.L103:
	orl	$32, (%rbx)
	movl	$-1, %eax
	jmp	.L92
	.p2align 4,,10
	.p2align 3
.L104:
	movq	24(%rbx), %rdx
	jmp	.L98
.LFE77:
	.size	adjust_wide_data, .-adjust_wide_data
	.p2align 4,,15
	.globl	_IO_wfile_seekoff
	.hidden	_IO_wfile_seekoff
	.type	_IO_wfile_seekoff, @function
_IO_wfile_seekoff:
.LFB79:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$200, %rsp
	testl	%ecx, %ecx
	movq	160(%rdi), %rax
	je	.L184
	movq	8(%rax), %rdi
	cmpq	%rdi, 16(%rax)
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	24(%rax), %rcx
	movq	32(%rax), %rdx
	je	.L185
.L125:
	xorl	%r14d, %r14d
	cmpq	%rdx, %rcx
	jnb	.L126
.L127:
	movq	%r15, %rdi
	call	_IO_switch_to_wget_mode
	movl	%eax, %edx
	movl	$4294967295, %eax
	testl	%edx, %edx
	jne	.L105
	movq	160(%r15), %rax
.L151:
	cmpq	$0, 48(%rax)
	je	.L186
.L128:
	cmpl	$1, %r12d
	je	.L131
	cmpl	$2, %r12d
	jne	.L130
	movq	216(%r15), %rdx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	movq	%rdx, %rax
	subq	%r13, %rbp
	subq	%r13, %rax
	cmpq	%rax, %rbp
	jbe	.L187
.L136:
	leaq	48(%rsp), %rsi
	movq	%r15, %rdi
	call	*144(%rdx)
	testl	%eax, %eax
	je	.L188
	.p2align 4,,10
	.p2align 3
.L138:
	movq	%r15, %rdi
	call	_IO_unsave_markers
	movq	216(%r15), %r14
	movq	%r14, %rax
	subq	%r13, %rax
	cmpq	%rbp, %rax
	jnb	.L189
.L149:
	movl	%r12d, %edx
	movq	%rbx, %rsi
	movq	%r15, %rdi
	call	*128(%r14)
	cmpq	$-1, %rax
	je	.L105
	movq	56(%r15), %rdx
	andl	$-17, (%r15)
	movq	%rax, 144(%r15)
	movq	%rdx, 24(%r15)
	movq	%rdx, 8(%r15)
	movq	%rdx, 16(%r15)
	movq	%rdx, 40(%r15)
	movq	%rdx, 32(%r15)
	movq	%rdx, 48(%r15)
	movq	160(%r15), %rdx
	movq	48(%rdx), %rcx
	movq	%rcx, 16(%rdx)
	movq	%rcx, (%rdx)
	movq	%rcx, 8(%rdx)
	movq	%rcx, 32(%rdx)
	movq	%rcx, 24(%rdx)
	movq	%rcx, 40(%rdx)
.L105:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L185:
	cmpq	%rdx, %rcx
	jne	.L125
	movl	$1, %r14d
	.p2align 4,,10
	.p2align 3
.L126:
	testl	$2048, (%r15)
	jne	.L127
	jmp	.L151
	.p2align 4,,10
	.p2align 3
.L131:
	movq	152(%r15), %rbp
	movq	%rbp, %rdi
	call	__libio_codecvt_encoding
	testl	%eax, %eax
	jle	.L133
	movq	160(%r15), %rcx
	cltq
	movq	8(%rcx), %rdx
	subq	(%rcx), %rdx
	sarq	$2, %rdx
	imulq	%rdx, %rax
	subq	%rax, %rbx
	movq	16(%r15), %rax
	subq	8(%r15), %rax
	subq	%rax, %rbx
.L134:
	movq	144(%r15), %rax
	cmpq	$-1, %rax
	je	.L135
	addq	%rax, %rbx
	xorl	%r12d, %r12d
.L130:
	movq	%r15, %rdi
	call	_IO_free_wbackup_area
	movq	144(%r15), %rdx
	movl	(%r15), %eax
	cmpq	$-1, %rdx
	je	.L140
	cmpq	$0, 24(%r15)
	je	.L140
	testb	$1, %ah
	jne	.L140
	movq	56(%r15), %rcx
	movq	%rcx, %rsi
	subq	16(%r15), %rsi
	addq	%rdx, %rsi
	cmpq	%rsi, %rbx
	jl	.L140
	cmpq	%rbx, %rdx
	jg	.L190
	.p2align 4,,10
	.p2align 3
.L140:
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	subq	%rcx, %rbp
	testb	$4, %al
	movq	%rcx, %r13
	jne	.L138
	movq	56(%r15), %rdx
	movq	64(%r15), %rax
	movq	%rbx, %r8
	movq	%rdx, %rsi
	subq	%rax, %rsi
	subq	%rdx, %rax
	andq	%rbx, %rsi
	subq	%rsi, %r8
	cmpq	%r8, %rax
	jge	.L143
	movq	%rbx, %rsi
	xorl	%r8d, %r8d
.L143:
	movq	216(%r15), %r9
	movq	%r9, %rax
	subq	%rcx, %rax
	cmpq	%rbp, %rax
	jnb	.L191
.L144:
	xorl	%edx, %edx
	movq	%rcx, 8(%rsp)
	movq	%r8, (%rsp)
	movq	%r15, %rdi
	call	*128(%r9)
	testq	%rax, %rax
	movq	%rax, 16(%rsp)
	js	.L182
	movq	(%rsp), %r8
	testq	%r8, %r8
	je	.L156
	movq	216(%r15), %rax
	movq	8(%rsp), %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	cmpq	%rbp, %rdx
	jnb	.L192
.L146:
	testl	%r14d, %r14d
	movq	112(%rax), %rax
	movq	56(%r15), %rsi
	movq	%r8, %rdx
	jne	.L147
	movq	64(%r15), %rdx
	subq	%rsi, %rdx
.L147:
	movq	%r8, (%rsp)
	movq	%r15, %rdi
	call	*%rax
	movq	(%rsp), %r8
	movq	%rax, %r14
	movq	%rax, %rdx
	cmpq	%rax, %r8
	jle	.L145
	cmpq	$-1, %rax
	movq	%r8, %rbx
	movl	$1, %r12d
	je	.L138
	subq	%rax, %rbx
	movl	$1, %r12d
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L184:
	cmpq	$0, 48(%rax)
	je	.L152
	movl	(%rdi), %edx
	movq	32(%rax), %r12
	movq	24(%rax), %rbp
	movl	%edx, %r13d
	andl	$4096, %r13d
	cmpq	%rbp, %r12
	jbe	.L108
	testl	%r13d, %r13d
	je	.L108
	movq	216(%rdi), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbx, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L193
.L109:
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	call	*128(%rbx)
	cmpq	$-1, %rax
	je	.L182
	movq	%rax, 144(%r15)
	movl	(%r15), %edx
	movq	160(%r15), %rax
.L108:
	andb	$1, %dh
	movq	(%rax), %r14
	movq	8(%rax), %rbx
	jne	.L194
	movq	16(%rax), %rax
	movq	%rax, 8(%rsp)
.L114:
	movq	152(%r15), %rax
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	__libio_codecvt_encoding
	cmpq	%rbp, %r12
	ja	.L115
	testl	%eax, %eax
	movq	16(%r15), %rcx
	jle	.L116
	subq	%r14, %rbx
	cltq
	subq	8(%r15), %rcx
	sarq	$2, %rbx
	imulq	%rbx, %rax
	negq	%rax
	subq	%rcx, %rax
	movq	%rax, %rbx
.L107:
	movq	144(%r15), %rax
	cmpq	$-1, %rax
	je	.L195
.L121:
	addq	%rbx, %rax
	jns	.L105
.L124:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	jmp	.L182
	.p2align 4,,10
	.p2align 3
.L188:
	movl	72(%rsp), %eax
	andl	$61440, %eax
	cmpl	$32768, %eax
	jne	.L138
	addq	96(%rsp), %rbx
	xorl	%r12d, %r12d
	jmp	.L130
	.p2align 4,,10
	.p2align 3
.L194:
	cmpq	%r14, %rbx
	ja	.L124
	movq	64(%rax), %r14
	movq	80(%rax), %rbx
	movq	%r14, 8(%rsp)
	jmp	.L114
	.p2align 4,,10
	.p2align 3
.L186:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.L129
	call	free@PLT
	andl	$-257, (%r15)
.L129:
	movq	%r15, %rdi
	call	_IO_doallocbuf
	movq	56(%r15), %rax
	movq	%rax, 40(%r15)
	movq	%rax, 32(%r15)
	movq	%rax, 48(%r15)
	movq	%rax, 24(%r15)
	movq	%rax, 8(%r15)
	movq	%rax, 16(%r15)
	movq	160(%r15), %rax
	movq	48(%rax), %rdx
	movq	%rdx, 32(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 40(%rax)
	movq	%rdx, 16(%rax)
	movq	%rdx, (%rax)
	movq	%rdx, 8(%rax)
	jmp	.L128
	.p2align 4,,10
	.p2align 3
.L156:
	xorl	%edx, %edx
	xorl	%r14d, %r14d
.L145:
	movq	56(%r15), %rax
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rax, 24(%r15)
	movq	%rax, 40(%r15)
	addq	%rax, %r8
	movq	%rax, 32(%r15)
	movq	%rax, 48(%r15)
	addq	%rax, %rdx
	movq	160(%r15), %rax
	movq	%rdx, 16(%r15)
	movq	%r8, 8(%r15)
	movq	48(%rax), %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, (%rax)
	movq	%rdx, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 40(%rax)
	call	adjust_wide_data
	testl	%eax, %eax
	jne	.L138
	movq	16(%rsp), %rax
	andl	$-17, (%r15)
	addq	%r14, %rax
	movq	%rax, 144(%r15)
	movq	%rbx, %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L115:
	movq	160(%r15), %rbp
	movq	32(%rbp), %rcx
	movq	24(%rbp), %r14
	movq	%rcx, %rbx
	subq	%r14, %rbx
	testl	%eax, %eax
	jle	.L117
	sarq	$2, %rbx
	cltq
	imulq	%rax, %rbx
.L118:
	testl	%r13d, %r13d
	movq	40(%r15), %rax
	je	.L120
	subq	32(%r15), %rax
	addq	%rax, %rbx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L195:
	movq	216(%r15), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L196
.L122:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	jne	.L121
.L182:
	movq	$-1, %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L152:
	xorl	%ebx, %ebx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L135:
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r13
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rbp
	subq	%r13, %rbp
	jmp	.L138
	.p2align 4,,10
	.p2align 3
.L189:
	call	_IO_vtable_check
	jmp	.L149
	.p2align 4,,10
	.p2align 3
.L120:
	subq	16(%r15), %rax
	addq	%rax, %rbx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L133:
	movq	160(%r15), %rsi
	movq	16(%r15), %rcx
	movq	%rbp, %rdi
	movq	24(%r15), %rdx
	movq	(%rsi), %r8
	subq	16(%rsi), %r8
	addq	$88, %rsi
	movq	8(%rsi), %rax
	sarq	$2, %r8
	movq	%rax, (%rsi)
	call	__libio_codecvt_length
	movq	24(%r15), %rcx
	movslq	%eax, %rdx
	leaq	(%rcx,%rdx), %rax
	movq	%rax, 8(%r15)
	movq	160(%r15), %rax
	movq	(%rax), %rsi
	movq	%rsi, 8(%rax)
	movq	16(%r15), %rax
	subq	%rcx, %rax
	subq	%rdx, %rax
	subq	%rax, %rbx
	jmp	.L134
	.p2align 4,,10
	.p2align 3
.L187:
	movq	%rdx, (%rsp)
	call	_IO_vtable_check
	movq	(%rsp), %rdx
	jmp	.L136
	.p2align 4,,10
	.p2align 3
.L190:
	movq	%rbx, %rax
	movq	%rcx, 24(%r15)
	movq	%rcx, 40(%r15)
	subq	%rsi, %rax
	movq	%rcx, 32(%r15)
	movq	%rcx, 48(%r15)
	addq	%rcx, %rax
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rax, 8(%r15)
	movq	160(%r15), %rax
	movq	48(%rax), %rdx
	movq	%rdx, 16(%rax)
	movq	%rdx, (%rax)
	movq	%rdx, 8(%rax)
	movq	%rdx, 32(%rax)
	movq	%rdx, 24(%rax)
	movq	%rdx, 40(%rax)
	call	adjust_wide_data
	testl	%eax, %eax
	jne	.L135
	movq	144(%r15), %rsi
	andl	$-17, (%r15)
	testq	%rsi, %rsi
	js	.L183
	movq	216(%r15), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rdi
	subq	%rdx, %rax
	subq	%rdx, %rdi
	cmpq	%rdi, %rax
	jbe	.L197
.L150:
	xorl	%edx, %edx
	movq	%r15, %rdi
	call	*128(%rbp)
.L183:
	movq	%rbx, %rax
	jmp	.L105
	.p2align 4,,10
	.p2align 3
.L116:
	movq	160(%r15), %rax
	movq	%r14, %r8
	subq	8(%rsp), %r8
	movq	24(%r15), %rdx
	movq	(%rsp), %rdi
	leaq	48(%rsp), %rsi
	movq	96(%rax), %rax
	sarq	$2, %r8
	movq	%rax, 48(%rsp)
	call	__libio_codecvt_length
	movq	16(%r15), %rdx
	subq	24(%r15), %rdx
	cltq
	subq	%rdx, %rax
	movq	%rax, %rbx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L117:
	movq	%rbx, %rdi
	movq	%rcx, 8(%rsp)
	call	malloc@PLT
	movq	%rax, 32(%rsp)
	movq	%rax, %r12
	movq	96(%rbp), %rax
	addq	%r12, %rbx
	movq	%r14, 40(%rsp)
	leaq	48(%rsp), %rsi
	movq	%r14, %rdx
	movq	%r12, %r9
	movq	%rax, 48(%rsp)
	leaq	32(%rsp), %rax
	pushq	%rax
	pushq	%rbx
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rdi
	leaq	56(%rsp), %r8
	call	__libio_codecvt_out
	testl	%eax, %eax
	popq	%rdx
	popq	%rcx
	jne	.L198
	movq	32(%rsp), %rbx
	movq	%r12, %rdi
	subq	%r12, %rbx
	call	free@PLT
	jmp	.L118
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rcx, 24(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%r8, 8(%rsp)
	movq	%r9, (%rsp)
	call	_IO_vtable_check
	movq	24(%rsp), %rcx
	movq	16(%rsp), %rsi
	movq	8(%rsp), %r8
	movq	(%rsp), %r9
	jmp	.L144
	.p2align 4,,10
	.p2align 3
.L192:
	movq	%r8, 8(%rsp)
	movq	%rax, (%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %r8
	movq	(%rsp), %rax
	jmp	.L146
	.p2align 4,,10
	.p2align 3
.L193:
	call	_IO_vtable_check
	jmp	.L109
.L196:
	call	_IO_vtable_check
	jmp	.L122
.L198:
	movq	%r12, %rdi
	call	free@PLT
	movl	$4294967295, %eax
	jmp	.L105
.L197:
	call	_IO_vtable_check
	movq	144(%r15), %rsi
	jmp	.L150
.LFE79:
	.size	_IO_wfile_seekoff, .-_IO_wfile_seekoff
	.p2align 4,,15
	.type	_IO_wfile_underflow_maybe_mmap, @function
_IO_wfile_underflow_maybe_mmap:
.LFB74:
	pushq	%rbx
	movq	%rdi, %rbx
	call	_IO_file_underflow_maybe_mmap@PLT
	cmpl	$-1, %eax
	je	.L200
	movq	160(%rbx), %rax
	movq	%rbx, %rdi
	popq	%rbx
	movq	224(%rax), %rax
	movq	32(%rax), %rax
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L200:
	popq	%rbx
	ret
.LFE74:
	.size	_IO_wfile_underflow_maybe_mmap, .-_IO_wfile_underflow_maybe_mmap
	.p2align 4,,15
	.type	_IO_wfile_underflow_mmap, @function
_IO_wfile_underflow_mmap:
.LFB73:
	movl	(%rdi), %eax
	testb	$4, %al
	jne	.L219
	movq	160(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jb	.L220
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rdx
	cmpq	16(%rdi), %rdx
	movq	152(%rdi), %rbp
	jnb	.L221
	movq	48(%rax), %r9
	movq	%rdx, 8(%rsp)
	testq	%r9, %r9
	je	.L222
.L208:
	movq	88(%rax), %rcx
	leaq	8(%rax), %rdi
	movq	%r9, (%rax)
	movq	%r9, 16(%rax)
	leaq	88(%rax), %rsi
	movq	%rcx, 96(%rax)
	movq	16(%rbx), %rcx
	pushq	%rdi
	pushq	56(%rax)
	movq	%rbp, %rdi
	leaq	24(%rsp), %r8
	call	__libio_codecvt_in
	movq	24(%rsp), %rax
	movq	%rax, 8(%rbx)
	movq	160(%rbx), %rax
	popq	%rcx
	popq	%rsi
	movq	(%rax), %rdx
	cmpq	8(%rax), %rdx
	jnb	.L210
	movl	(%rdx), %eax
.L202:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L220:
	movl	(%rdx), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L221:
	call	_IO_file_underflow_mmap@PLT
	movl	%eax, %edx
	movl	$-1, %eax
	cmpl	%eax, %edx
	je	.L202
	movq	160(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	48(%rax), %r9
	movq	%rdx, 8(%rsp)
	testq	%r9, %r9
	jne	.L208
.L222:
	movq	64(%rax), %rdi
	testq	%rdi, %rdi
	je	.L209
	call	free@PLT
	andl	$-257, (%rbx)
.L209:
	movq	%rbx, %rdi
	call	_IO_wdoallocbuf
	movq	160(%rbx), %rax
	movq	8(%rbx), %rdx
	movq	48(%rax), %r9
	jmp	.L208
	.p2align 4,,10
	.p2align 3
.L210:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$84, %fs:(%rax)
	orl	$32, (%rbx)
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L219:
	orl	$32, %eax
	movl	%eax, (%rdi)
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
.LFE73:
	.size	_IO_wfile_underflow_mmap, .-_IO_wfile_underflow_mmap
	.p2align 4,,15
	.globl	_IO_wdo_write
	.hidden	_IO_wdo_write
	.type	_IO_wdo_write, @function
_IO_wdo_write:
.LFB71:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$72, %rsp
	testq	%rdx, %rdx
	je	.L224
	movq	40(%rdi), %r9
	cmpq	%r9, 48(%rdi)
	movq	%rsi, %r12
	movq	152(%rdi), %rax
	movq	%rdx, %r15
	movq	32(%rdi), %rbp
	movq	%rax, (%rsp)
	je	.L248
.L225:
	leaq	32(%rsp), %rax
	movq	%rax, 8(%rsp)
	leaq	40(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	48(%rsp), %rax
	movq	%rax, 24(%rsp)
	jmp	.L232
	.p2align 4,,10
	.p2align 3
.L249:
	movq	24(%rsp), %rbp
	leaq	64(%rsp), %rax
	movq	%rbp, 40(%rsp)
	movq	%rbp, %r9
	movq	%rbp, %r13
.L228:
	movq	160(%rbx), %rdi
	pushq	16(%rsp)
	leaq	(%r12,%r15,4), %rcx
	pushq	%rax
	movq	24(%rsp), %r8
	movq	%r12, %rdx
	leaq	88(%rdi), %rsi
	movq	16(%rsp), %rdi
	call	__libio_codecvt_out
	movl	%eax, %r14d
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	popq	%rax
	popq	%rdx
	movq	40(%rsp), %rdx
	subq	%r13, %rdx
	call	_IO_new_do_write@PLT
	cmpl	$-1, %eax
	je	.L223
	movq	32(%rsp), %rdx
	movq	%rdx, %rax
	subq	%r12, %rax
	sarq	$2, %rax
	subq	%rax, %r15
	testl	%r14d, %r14d
	je	.L230
	cmpq	%r12, %rdx
	je	.L231
	cmpl	$1, %r14d
	jne	.L231
.L230:
	testq	%r15, %r15
	je	.L224
	movq	40(%rbx), %r9
	movq	32(%rbx), %rbp
	movq	%rdx, %r12
.L232:
	movq	%r9, %rax
	movq	%rbp, %r13
	subq	%rbp, %rax
	cmpq	$15, %rax
	jbe	.L249
	movq	%r9, 40(%rsp)
	movq	64(%rbx), %rax
	jmp	.L228
	.p2align 4,,10
	.p2align 3
.L224:
	movq	160(%rbx), %rdx
	movl	(%rbx), %eax
	movq	48(%rdx), %rcx
	andl	$514, %eax
	movq	%rcx, 16(%rdx)
	movq	%rcx, (%rdx)
	movq	%rcx, 8(%rdx)
	movq	%rcx, 32(%rdx)
	movq	%rcx, 24(%rdx)
	jne	.L234
.L235:
	movq	56(%rdx), %rcx
.L233:
	movq	%rcx, 40(%rdx)
.L223:
	addq	$72, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L234:
	movq	%rcx, 40(%rdx)
	addq	$72, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L248:
	cmpq	%rbp, %r9
	je	.L225
	movq	%r9, %rdx
	movq	%rbp, %rsi
	subq	%rbp, %rdx
	call	_IO_new_do_write@PLT
	cmpl	$-1, %eax
	je	.L223
	movq	40(%rbx), %r9
	movq	32(%rbx), %rbp
	jmp	.L225
	.p2align 4,,10
	.p2align 3
.L231:
	movq	160(%rbx), %rdx
	xorl	%eax, %eax
	testq	%r15, %r15
	setne	%al
	negl	%eax
	testl	$514, (%rbx)
	movq	48(%rdx), %rcx
	movq	%rcx, 16(%rdx)
	movq	%rcx, (%rdx)
	movq	%rcx, 8(%rdx)
	movq	%rcx, 32(%rdx)
	movq	%rcx, 24(%rdx)
	jne	.L233
	jmp	.L235
.LFE71:
	.size	_IO_wdo_write, .-_IO_wdo_write
	.p2align 4,,15
	.globl	_IO_wfile_overflow
	.hidden	_IO_wfile_overflow
	.type	_IO_wfile_overflow, @function
_IO_wfile_overflow:
.LFB75:
	movl	(%rdi), %edx
	testb	$8, %dl
	jne	.L283
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %ebp
	movq	%rdi, %rbx
	subq	$8, %rsp
	testb	$8, %dh
	jne	.L254
	movq	160(%rdi), %rcx
	cmpq	$0, 24(%rcx)
	je	.L284
	movq	(%rcx), %rax
	movq	56(%rcx), %r8
	cmpq	%r8, %rax
	je	.L258
	movq	8(%rcx), %rsi
	movq	8(%rdi), %rdi
	movq	16(%rbx), %r9
.L257:
	movq	%rsi, (%rcx)
	movq	%rsi, 16(%rcx)
	movq	64(%rbx), %rsi
	movq	%rax, 32(%rcx)
	movq	%rax, 24(%rcx)
	movq	%r8, 40(%rcx)
	movq	%rdi, 40(%rbx)
	movq	%rdi, 32(%rbx)
	movq	%rsi, 48(%rbx)
	movl	%edx, %esi
	movq	%r9, 8(%rbx)
	orl	$2048, %esi
	andl	$514, %edx
	movq	%r9, 24(%rbx)
	movl	%esi, (%rbx)
	je	.L254
	movq	%rax, 40(%rcx)
.L254:
	cmpl	$-1, %ebp
	je	.L285
	movq	160(%rbx), %rcx
	movq	32(%rcx), %rax
	cmpq	56(%rcx), %rax
	je	.L286
.L262:
	leaq	4(%rax), %rdx
	movq	%rdx, 32(%rcx)
	movl	%ebp, (%rax)
	movl	(%rbx), %eax
	testb	$2, %al
	jne	.L266
	testb	$2, %ah
	je	.L267
	cmpl	$10, %ebp
	je	.L266
.L267:
	addq	$8, %rsp
	movl	%ebp, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	movl	192(%rbx), %ecx
	testl	%ecx, %ecx
	jle	.L287
	movq	160(%rbx), %rax
	movq	%rbx, %rdi
	movq	24(%rax), %rsi
	movq	32(%rax), %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	subq	%rsi, %rdx
	sarq	$2, %rdx
	jmp	_IO_wdo_write
	.p2align 4,,10
	.p2align 3
.L283:
	movq	__libc_errno@gottpoff(%rip), %rax
	orl	$32, %edx
	movl	%edx, (%rdi)
	movl	$9, %fs:(%rax)
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L266:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L288
	movq	24(%rcx), %rsi
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
	cmpl	$-1, %eax
	sete	%al
.L269:
	testb	%al, %al
	je	.L267
.L270:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L288:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	_IO_do_write
	cmpl	$-1, %eax
	sete	%al
	jmp	.L269
	.p2align 4,,10
	.p2align 3
.L258:
	movq	56(%rdi), %rdi
	movq	48(%rcx), %rsi
	movq	%rax, %r8
	movq	%rdi, 16(%rbx)
	movq	%rdi, %r9
	movq	%rsi, (%rcx)
	movq	%rsi, 8(%rcx)
	movq	%rsi, %rax
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L287:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	subq	%rsi, %rdx
	jmp	_IO_do_write
	.p2align 4,,10
	.p2align 3
.L286:
	movl	192(%rbx), %edx
	testl	%edx, %edx
	jle	.L289
	movq	24(%rcx), %rsi
	movq	%rbx, %rdi
	subq	%rsi, %rax
	movq	%rax, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
	cmpl	$-1, %eax
	sete	%al
.L264:
	testb	%al, %al
	jne	.L270
	movq	160(%rbx), %rcx
	movq	32(%rcx), %rax
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L284:
	call	_IO_wdoallocbuf
	movq	%rbx, %rdi
	call	_IO_free_wbackup_area
	movq	160(%rbx), %rcx
	cmpq	$0, 32(%rbx)
	movq	48(%rcx), %rax
	movq	%rax, 16(%rcx)
	movq	%rax, (%rcx)
	movq	%rax, 8(%rcx)
	je	.L256
	movq	56(%rcx), %r8
	movq	8(%rbx), %rdi
	movq	%rax, %rsi
	movq	16(%rbx), %r9
	movl	(%rbx), %edx
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L256:
	movq	%rbx, %rdi
	call	_IO_doallocbuf
	movq	160(%rbx), %rcx
	movq	56(%rbx), %rdi
	movl	(%rbx), %edx
	movq	%rdi, 16(%rbx)
	movq	(%rcx), %rax
	movq	%rdi, %r9
	movq	56(%rcx), %r8
	movq	8(%rcx), %rsi
	jmp	.L257
	.p2align 4,,10
	.p2align 3
.L289:
	movq	32(%rbx), %rsi
	movq	40(%rbx), %rdx
	movq	%rbx, %rdi
	subq	%rsi, %rdx
	call	_IO_do_write
	cmpl	$-1, %eax
	sete	%al
	jmp	.L264
.LFE75:
	.size	_IO_wfile_overflow, .-_IO_wfile_overflow
	.p2align 4,,15
	.globl	_IO_wfile_sync
	.hidden	_IO_wfile_sync
	.type	_IO_wfile_sync, @function
_IO_wfile_sync:
.LFB76:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %rax
	movq	32(%rax), %rdx
	movq	24(%rax), %rsi
	cmpq	%rsi, %rdx
	jbe	.L291
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jle	.L305
	subq	%rsi, %rdx
	sarq	$2, %rdx
	call	_IO_wdo_write
	testl	%eax, %eax
	setne	%al
.L293:
	testb	%al, %al
	jne	.L303
	movq	160(%rbx), %rax
.L291:
	movq	(%rax), %rbp
	movq	8(%rax), %r12
	cmpq	%r12, %rbp
	jne	.L306
.L297:
	movq	$-1, 144(%rbx)
	addq	$24, %rsp
	xorl	%eax, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L306:
	movq	152(%rbx), %r13
	movq	%r13, %rdi
	call	__libio_codecvt_encoding
	testl	%eax, %eax
	jle	.L298
	subq	%r12, %rbp
	movslq	%eax, %rsi
	sarq	$2, %rbp
	imulq	%rbp, %rsi
.L299:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L307
.L300:
	movl	$1, %edx
	movq	%rbx, %rdi
	call	*128(%rbp)
	cmpq	$-1, %rax
	je	.L301
	movq	160(%rbx), %rax
	movq	(%rax), %rdx
	movq	%rdx, 8(%rax)
	movq	8(%rbx), %rax
	movq	%rax, 16(%rbx)
	jmp	.L297
	.p2align 4,,10
	.p2align 3
.L301:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$29, %fs:(%rax)
	je	.L297
.L303:
	addq	$24, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L305:
	movq	32(%rdi), %rsi
	movq	40(%rdi), %rdx
	subq	%rsi, %rdx
	call	_IO_do_write
	testl	%eax, %eax
	setne	%al
	jmp	.L293
	.p2align 4,,10
	.p2align 3
.L298:
	movq	160(%rbx), %rsi
	movq	16(%rbx), %rcx
	movq	%r13, %rdi
	movq	24(%rbx), %rdx
	movq	(%rsi), %r8
	subq	16(%rsi), %r8
	addq	$88, %rsi
	movq	8(%rsi), %rax
	sarq	$2, %r8
	movq	%rax, (%rsi)
	call	__libio_codecvt_length
	movq	24(%rbx), %rdx
	cltq
	leaq	(%rdx,%rax), %rcx
	subq	16(%rbx), %rdx
	movq	%rcx, 8(%rbx)
	leaq	(%rdx,%rax), %rsi
	jmp	.L299
	.p2align 4,,10
	.p2align 3
.L307:
	movq	%rsi, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rsi
	jmp	.L300
.LFE76:
	.size	_IO_wfile_sync, .-_IO_wfile_sync
	.p2align 4,,15
	.globl	_IO_wfile_xsputn
	.hidden	_IO_wfile_xsputn
	.type	_IO_wfile_xsputn, @function
_IO_wfile_xsputn:
.LFB80:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	xorl	%ebx, %ebx
	subq	$8, %rsp
	testq	%rdx, %rdx
	je	.L308
	movq	%rdi, %r13
	movq	%rdx, %rbp
	movq	160(%rdi), %r14
	movl	0(%r13), %edx
	movq	%rsi, %r12
	movq	40(%r14), %rax
	movq	32(%r14), %rdi
	andl	$2560, %edx
	cmpl	$2560, %edx
	je	.L310
	subq	%rdi, %rax
	xorl	%r15d, %r15d
	sarq	$2, %rax
.L311:
	testq	%rax, %rax
	je	.L325
	cmpq	%rax, %rbp
	cmovbe	%rbp, %rax
	cmpq	$20, %rax
	movq	%rax, %rbx
	ja	.L335
	leal	-1(%rax), %eax
	leaq	4(,%rax,4), %rcx
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L319:
	movl	(%r12,%rax), %edx
	movl	%edx, (%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rcx
	jne	.L319
	addq	%rcx, %rdi
	addq	%rcx, %r12
	movq	%rdi, 32(%r14)
.L318:
	movq	%rbp, %rax
	subq	%rbx, %rax
	movq	%rax, %rbx
	jne	.L316
	movq	%rbp, %rbx
.L320:
	testl	%r15d, %r15d
	je	.L308
	movq	160(%r13), %rax
	movq	32(%rax), %rdx
	movq	24(%rax), %rsi
	cmpq	%rsi, %rdx
	je	.L308
	subq	%rsi, %rdx
	movq	%r13, %rdi
	sarq	$2, %rdx
	call	_IO_wdo_write
.L308:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L325:
	movq	%rbp, %rbx
.L316:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	movq	%r13, %rdi
	call	_IO_wdefault_xsputn
	subq	%rax, %rbx
	subq	%rbx, %rbp
	movq	%rbp, %rbx
	jmp	.L320
	.p2align 4,,10
	.p2align 3
.L310:
	movq	56(%r14), %rax
	xorl	%r15d, %r15d
	subq	%rdi, %rax
	sarq	$2, %rax
	cmpq	%rax, %rbp
	ja	.L311
	leaq	(%rsi,%rbp,4), %rsi
	cmpq	%rsi, %r12
	jnb	.L311
	cmpl	$10, -4(%rsi)
	leaq	-4(%rsi), %rdx
	je	.L312
	movq	%rdx, %rcx
	subq	%r12, %rcx
	addq	$3, %rcx
	notq	%rcx
	andq	$-4, %rcx
	addq	%rsi, %rcx
	jmp	.L314
	.p2align 4,,10
	.p2align 3
.L315:
	subq	$4, %rdx
	cmpl	$10, (%rdx)
	je	.L312
.L314:
	cmpq	%rcx, %rdx
	jne	.L315
	xorl	%r15d, %r15d
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L335:
	movq	%r12, %rsi
	movq	%rax, %rdx
	leaq	(%r12,%rbx,4), %r12
	call	__wmempcpy
	movq	%rax, 32(%r14)
	jmp	.L318
	.p2align 4,,10
	.p2align 3
.L312:
	subq	%r12, %rdx
	movl	$1, %r15d
	sarq	$2, %rdx
	leaq	1(%rdx), %rax
	jmp	.L311
.LFE80:
	.size	_IO_wfile_xsputn, .-_IO_wfile_xsputn
	.section	.rodata.str1.16,"aMS",@progbits,1
	.align 16
	.type	__PRETTY_FUNCTION__.11489, @object
	.size	__PRETTY_FUNCTION__.11489, 20
__PRETTY_FUNCTION__.11489:
	.string	"_IO_wfile_underflow"
	.hidden	_IO_wfile_jumps_maybe_mmap
	.globl	_IO_wfile_jumps_maybe_mmap
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_wfile_jumps_maybe_mmap, @object
	.size	_IO_wfile_jumps_maybe_mmap, 168
_IO_wfile_jumps_maybe_mmap:
	.quad	0
	.quad	0
	.quad	_IO_new_file_finish
	.quad	_IO_wfile_overflow
	.quad	_IO_wfile_underflow_maybe_mmap
	.quad	_IO_wdefault_uflow
	.quad	_IO_wdefault_pbackfail
	.quad	_IO_wfile_xsputn
	.quad	_IO_file_xsgetn
	.quad	_IO_wfile_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf_mmap
	.quad	_IO_wfile_sync
	.quad	_IO_wfile_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_file_close
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_wfile_jumps_mmap
	.globl	_IO_wfile_jumps_mmap
	.align 32
	.type	_IO_wfile_jumps_mmap, @object
	.size	_IO_wfile_jumps_mmap, 168
_IO_wfile_jumps_mmap:
	.quad	0
	.quad	0
	.quad	_IO_new_file_finish
	.quad	_IO_wfile_overflow
	.quad	_IO_wfile_underflow_mmap
	.quad	_IO_wdefault_uflow
	.quad	_IO_wdefault_pbackfail
	.quad	_IO_wfile_xsputn
	.quad	_IO_file_xsgetn
	.quad	_IO_wfile_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_file_setbuf_mmap
	.quad	_IO_wfile_sync
	.quad	_IO_wfile_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_file_close_mmap
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_wfile_jumps
	.globl	_IO_wfile_jumps
	.align 32
	.type	_IO_wfile_jumps, @object
	.size	_IO_wfile_jumps, 168
_IO_wfile_jumps:
	.quad	0
	.quad	0
	.quad	_IO_new_file_finish
	.quad	_IO_wfile_overflow
	.quad	_IO_wfile_underflow
	.quad	_IO_wdefault_uflow
	.quad	_IO_wdefault_pbackfail
	.quad	_IO_wfile_xsputn
	.quad	_IO_file_xsgetn
	.quad	_IO_wfile_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_new_file_setbuf
	.quad	_IO_wfile_sync
	.quad	_IO_wfile_doallocate
	.quad	_IO_file_read
	.quad	_IO_new_file_write
	.quad	_IO_file_seek
	.quad	_IO_file_close
	.quad	_IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.weak	__start___libc_IO_vtables
	.weak	__stop___libc_IO_vtables
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	_IO_file_stat
	.hidden	_IO_file_close
	.hidden	_IO_file_seek
	.hidden	_IO_file_read
	.hidden	_IO_file_xsgetn
	.hidden	_IO_wdefault_pbackfail
	.hidden	_IO_wdefault_uflow
	.hidden	__wmempcpy
	.hidden	_IO_wdefault_xsputn
	.hidden	_IO_do_write
	.hidden	__libio_codecvt_out
	.hidden	__libio_codecvt_length
	.hidden	_IO_free_wbackup_area
	.hidden	_IO_unsave_markers
	.hidden	_IO_switch_to_wget_mode
	.hidden	__libio_codecvt_encoding
	.hidden	__assert_fail
	.hidden	__lll_lock_wait_private
	.hidden	_IO_vtable_check
	.hidden	memmove
	.hidden	_IO_doallocbuf
	.hidden	_IO_wdoallocbuf
	.hidden	__libio_codecvt_in
	.hidden	_IO_switch_to_get_mode
