	.text
	.p2align 4,,15
	.globl	clearerr_unlocked
	.type	clearerr_unlocked, @function
clearerr_unlocked:
	andl	$-49, (%rdi)
	ret
	.size	clearerr_unlocked, .-clearerr_unlocked
