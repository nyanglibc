	.text
	.hidden	_IO_list_all
	.globl	_IO_list_all
	.section	.data.rel.local,"aw",@progbits
	.align 8
	.type	_IO_list_all, @object
	.size	_IO_list_all, 8
_IO_list_all:
	.quad	_IO_2_1_stderr_
	.globl	_IO_2_1_stderr_
	.align 32
	.type	_IO_2_1_stderr_, @object
	.size	_IO_2_1_stderr_, 224
_IO_2_1_stderr_:
	.long	-72540026
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_IO_2_1_stdout_
	.long	2
	.long	0
	.quad	-1
	.value	0
	.byte	0
	.byte	0
	.zero	4
	.quad	_IO_stdfile_2_lock
	.quad	-1
	.quad	0
	.quad	_IO_wide_data_2
	.quad	0
	.zero	40
	.quad	_IO_file_jumps
	.align 32
	.type	_IO_wide_data_2, @object
	.size	_IO_wide_data_2, 232
_IO_wide_data_2:
	.zero	224
	.quad	_IO_wfile_jumps
	.local	_IO_stdfile_2_lock
	.comm	_IO_stdfile_2_lock,16,16
	.globl	_IO_2_1_stdout_
	.align 32
	.type	_IO_2_1_stdout_, @object
	.size	_IO_2_1_stdout_, 224
_IO_2_1_stdout_:
	.long	-72540028
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	_IO_2_1_stdin_
	.long	1
	.long	0
	.quad	-1
	.value	0
	.byte	0
	.byte	0
	.zero	4
	.quad	_IO_stdfile_1_lock
	.quad	-1
	.quad	0
	.quad	_IO_wide_data_1
	.quad	0
	.zero	40
	.quad	_IO_file_jumps
	.align 32
	.type	_IO_wide_data_1, @object
	.size	_IO_wide_data_1, 232
_IO_wide_data_1:
	.zero	224
	.quad	_IO_wfile_jumps
	.local	_IO_stdfile_1_lock
	.comm	_IO_stdfile_1_lock,16,16
	.globl	_IO_2_1_stdin_
	.align 32
	.type	_IO_2_1_stdin_, @object
	.size	_IO_2_1_stdin_, 224
_IO_2_1_stdin_:
	.long	-72540024
	.zero	4
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.quad	0
	.long	0
	.long	0
	.quad	-1
	.value	0
	.byte	0
	.byte	0
	.zero	4
	.quad	_IO_stdfile_0_lock
	.quad	-1
	.quad	0
	.quad	_IO_wide_data_0
	.quad	0
	.zero	40
	.quad	_IO_file_jumps
	.align 32
	.type	_IO_wide_data_0, @object
	.size	_IO_wide_data_0, 232
_IO_wide_data_0:
	.zero	224
	.quad	_IO_wfile_jumps
	.local	_IO_stdfile_0_lock
	.comm	_IO_stdfile_0_lock,16,16
	.hidden	_IO_wfile_jumps
	.hidden	_IO_file_jumps
