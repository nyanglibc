	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	_IO_wmem_sync, @function
_IO_wmem_sync:
	movq	160(%rdi), %rax
	pushq	%rbx
	movq	%rdi, %rbx
	movq	40(%rax), %rsi
	cmpq	%rsi, 32(%rax)
	je	.L5
.L2:
	movq	24(%rax), %rcx
	movq	240(%rbx), %rdx
	movq	%rcx, (%rdx)
	movq	32(%rax), %rdx
	subq	24(%rax), %rdx
	movq	248(%rbx), %rcx
	movq	%rdx, %rax
	sarq	$2, %rax
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%esi, %esi
	call	_IO_wstr_overflow@PLT
	movq	160(%rbx), %rax
	subq	$4, 32(%rax)
	jmp	.L2
	.size	_IO_wmem_sync, .-_IO_wmem_sync
	.p2align 4,,15
	.type	_IO_wmem_finish, @function
_IO_wmem_finish:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	160(%rdi), %rax
	movq	240(%rbx), %rbp
	movq	24(%rax), %rdi
	movq	32(%rax), %rax
	subq	%rdi, %rax
	sarq	$2, %rax
	leaq	4(,%rax,4), %rsi
	call	realloc@PLT
	movq	%rax, 0(%rbp)
	movq	240(%rbx), %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.L7
	movq	160(%rbx), %rdx
	movq	32(%rdx), %rax
	subq	24(%rdx), %rax
	movl	$0, (%rcx,%rax)
	movq	248(%rbx), %rcx
	sarq	$2, %rax
	movq	%rax, (%rcx)
	movq	$0, 48(%rdx)
.L7:
	addq	$8, %rsp
	movq	%rbx, %rdi
	xorl	%esi, %esi
	popq	%rbx
	popq	%rbp
	jmp	_IO_wstr_finish@PLT
	.size	_IO_wmem_finish, .-_IO_wmem_finish
	.p2align 4,,15
	.globl	open_wmemstream
	.type	open_wmemstream, @function
open_wmemstream:
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	$504, %edi
	movq	%rsi, %r12
	subq	$8, %rsp
	call	malloc@PLT
	testq	%rax, %rax
	je	.L15
	movq	%rax, %rbx
	leaq	256(%rax), %rax
	movl	$8192, %esi
	movl	$1, %edi
	movq	%rax, 136(%rbx)
	call	calloc@PLT
	testq	%rax, %rax
	movq	%rax, %rbp
	je	.L17
	leaq	272(%rbx), %rcx
	leaq	_IO_wmem_jumps(%rip), %r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_no_init@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	movl	$2048, %edx
	movq	%rbp, %rcx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_IO_wstr_init_static@PLT
	movq	malloc@GOTPCREL(%rip), %rdx
	movl	116(%rbx), %eax
	movq	%r13, 240(%rbx)
	movq	%r12, 248(%rbx)
	movq	%rdx, 224(%rbx)
	movq	free@GOTPCREL(%rip), %rdx
	andl	$-9, %eax
	orb	$-128, %al
	movl	%eax, 116(%rbx)
	movq	%rdx, 232(%rbx)
.L12:
	addq	$8, %rsp
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L15:
	xorl	%ebx, %ebx
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L17:
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L12
	.size	open_wmemstream, .-open_wmemstream
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_wmem_jumps, @object
	.size	_IO_wmem_jumps, 168
_IO_wmem_jumps:
	.quad	0
	.quad	0
	.quad	_IO_wmem_finish
	.quad	_IO_wstr_overflow
	.quad	_IO_wstr_underflow
	.quad	__GI__IO_wdefault_uflow
	.quad	_IO_wstr_pbackfail
	.quad	__GI__IO_wdefault_xsputn
	.quad	__GI__IO_wdefault_xsgetn
	.quad	_IO_wstr_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_wmem_sync
	.quad	__GI__IO_wdefault_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
