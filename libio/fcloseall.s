	.text
	.p2align 4,,15
	.globl	__fcloseall
	.hidden	__fcloseall
	.type	__fcloseall, @function
__fcloseall:
	jmp	_IO_cleanup@PLT
	.size	__fcloseall, .-__fcloseall
	.weak	fcloseall
	.set	fcloseall,__fcloseall
