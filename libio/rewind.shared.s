	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI_rewind
	.hidden	__GI_rewind
	.type	__GI_rewind, @function
__GI_rewind:
.LFB68:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movl	(%rdi), %edx
	andl	$32768, %edx
	jne	.L2
	movq	136(%rdi), %rdi
	movq	%fs:16, %rbp
	cmpq	%rbp, 8(%rdi)
	je	.L3
#APP
# 34 "rewind.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%rbp, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	movl	$3, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
.LEHB0:
	call	_IO_seekoff_unlocked
	movl	(%rbx), %eax
	movl	%eax, %edx
	andl	$-49, %edx
	testb	$-128, %ah
	movl	%edx, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L11
	subl	$1, (%rdi)
.L1:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L11:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L15:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L13
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L13
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L14
	subl	$1, (%rdi)
.L13:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L14:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L13
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L13
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L15-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	__GI_rewind, .-__GI_rewind
	.globl	rewind
	.set	rewind,__GI_rewind
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	_IO_seekoff_unlocked
