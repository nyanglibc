	.text
	.p2align 4,,15
	.globl	setlinebuf
	.type	setlinebuf, @function
setlinebuf:
	xorl	%ecx, %ecx
	movl	$1, %edx
	xorl	%esi, %esi
	jmp	_IO_setvbuf
	.size	setlinebuf, .-setlinebuf
	.hidden	_IO_setvbuf
