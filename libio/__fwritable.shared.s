	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__fwritable
	.type	__fwritable, @function
__fwritable:
	xorl	%eax, %eax
	testb	$8, (%rdi)
	sete	%al
	ret
	.size	__fwritable, .-__fwritable
