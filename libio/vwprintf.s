	.text
	.p2align 4,,15
	.globl	__vwprintf
	.type	__vwprintf, @function
__vwprintf:
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	movq	stdout(%rip), %rdi
	xorl	%ecx, %ecx
	jmp	__vfwprintf_internal
	.size	__vwprintf, .-__vwprintf
	.globl	vwprintf
	.set	vwprintf,__vwprintf
	.hidden	__vfwprintf_internal
