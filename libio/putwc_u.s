	.text
	.p2align 4,,15
	.globl	putwc_unlocked
	.hidden	putwc_unlocked
	.type	putwc_unlocked, @function
putwc_unlocked:
	movq	160(%rsi), %rdx
	movq	%rsi, %rax
	testq	%rdx, %rdx
	je	.L2
	movq	32(%rdx), %rcx
	cmpq	40(%rdx), %rcx
	jnb	.L2
	leaq	4(%rcx), %rax
	movq	%rax, 32(%rdx)
	movl	%edi, (%rcx)
	movl	%edi, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L2:
	movl	%edi, %esi
	movq	%rax, %rdi
	jmp	__woverflow
	.size	putwc_unlocked, .-putwc_unlocked
	.hidden	__woverflow
