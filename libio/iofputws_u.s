	.text
	.p2align 4,,15
	.globl	fputws_unlocked
	.hidden	fputws_unlocked
	.type	fputws_unlocked, @function
fputws_unlocked:
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %r12
	pushq	%rbx
	movq	%rsi, %rbx
	call	__wcslen@PLT
	movl	$1, %esi
	movq	%rbx, %rdi
	movq	%rax, %r13
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L5
	movq	216(%rbx), %r14
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movl	%eax, %ebp
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r14, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L8
.L4:
	movq	%r13, %rdx
	movq	%r12, %rsi
	movq	%rbx, %rdi
	call	*56(%r14)
	cmpq	%r13, %rax
	jne	.L5
.L1:
	popq	%rbx
	movl	%ebp, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	call	_IO_vtable_check
	jmp	.L4
	.p2align 4,,10
	.p2align 3
.L5:
	movl	$-1, %ebp
	jmp	.L1
	.size	fputws_unlocked, .-fputws_unlocked
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.hidden	_IO_vtable_check
