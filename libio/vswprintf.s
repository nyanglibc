	.text
	.p2align 4,,15
	.type	_IO_wstrn_overflow, @function
_IO_wstrn_overflow:
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	leaq	240(%rdi), %rbx
	subq	$8, %rsp
	movq	160(%rdi), %rdx
	cmpq	%rbx, 48(%rdx)
	je	.L2
	leaq	496(%rdi), %r13
	movq	%rdi, %rbp
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	movq	%r13, %rdx
	call	_IO_wsetb
	movq	160(%rbp), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rbx, 16(%rdx)
	movq	%rbx, (%rdx)
	movq	%r13, 8(%rdx)
.L2:
	movq	%rbx, 32(%rdx)
	movq	%rbx, 40(%rdx)
	addq	$8, %rsp
	popq	%rbx
	movl	%r12d, %eax
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.size	_IO_wstrn_overflow, .-_IO_wstrn_overflow
	.p2align 4,,15
	.globl	__vswprintf_internal
	.hidden	__vswprintf_internal
	.type	__vswprintf_internal, @function
__vswprintf_internal:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$744, %rsp
	testq	%rsi, %rsi
	movq	$0, 376(%rsp)
	je	.L8
	leaq	240(%rsp), %rbx
	movl	%r8d, %r15d
	leaq	_IO_wstrn_jumps(%rip), %r8
	movq	%rdi, %r12
	movq	%rcx, %r14
	movq	%rdx, %r13
	movq	%rsp, %rcx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%rsi, %rbp
	movl	$32768, %esi
	call	_IO_no_init@PLT
	movq	%rbx, %rdi
	movl	$1, %esi
	call	_IO_fwide@PLT
	leaq	-1(%rbp), %rdx
	movq	%rbx, %rdi
	movq	%r12, %rcx
	movq	%r12, %rsi
	movl	$0, (%r12)
	call	_IO_wstr_init_static@PLT
	movq	%r14, %rdx
	movq	%rbx, %rdi
	movl	%r15d, %ecx
	movq	%r13, %rsi
	addq	$240, %rbx
	call	__vfwprintf_internal
	movq	400(%rsp), %rdx
	cmpq	%rbx, 48(%rdx)
	je	.L8
	movq	32(%rdx), %rdx
	movl	$0, (%rdx)
.L5:
	addq	$744, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L8:
	movl	$-1, %eax
	jmp	.L5
	.size	__vswprintf_internal, .-__vswprintf_internal
	.p2align 4,,15
	.globl	__vswprintf
	.type	__vswprintf, @function
__vswprintf:
	xorl	%r8d, %r8d
	jmp	__vswprintf_internal
	.size	__vswprintf, .-__vswprintf
	.weak	vswprintf
	.set	vswprintf,__vswprintf
	.hidden	_IO_wstrn_jumps
	.globl	_IO_wstrn_jumps
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_wstrn_jumps, @object
	.size	_IO_wstrn_jumps, 168
_IO_wstrn_jumps:
	.quad	0
	.quad	0
	.quad	_IO_wstr_finish
	.quad	_IO_wstrn_overflow
	.quad	_IO_wstr_underflow
	.quad	_IO_wdefault_uflow
	.quad	_IO_wstr_pbackfail
	.quad	_IO_wdefault_xsputn
	.quad	_IO_wdefault_xsgetn
	.quad	_IO_wstr_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_default_setbuf
	.quad	_IO_default_sync
	.quad	_IO_wdefault_doallocate
	.quad	_IO_default_read
	.quad	_IO_default_write
	.quad	_IO_default_seek
	.quad	_IO_default_sync
	.quad	_IO_default_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_wdefault_doallocate
	.hidden	_IO_wdefault_xsgetn
	.hidden	_IO_wdefault_xsputn
	.hidden	_IO_wdefault_uflow
	.hidden	__vfwprintf_internal
	.hidden	_IO_wsetb
