	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver __new_pclose,pclose@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	__new_pclose
	.type	__new_pclose, @function
__new_pclose:
	jmp	_IO_new_fclose@PLT
	.size	__new_pclose, .-__new_pclose
