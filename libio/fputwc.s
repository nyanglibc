	.text
	.p2align 4,,15
	.globl	fputwc
	.type	fputwc, @function
fputwc:
.LFB71:
	pushq	%r12
	pushq	%rbp
	movl	%edi, %ebp
	pushq	%rbx
	movl	(%rsi), %edx
	movq	%rsi, %rbx
	andl	$32768, %edx
	jne	.L2
	movq	136(%rsi), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L3
#APP
# 35 "fputwc.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L4
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L5:
	movq	136(%rbx), %rdi
	movq	%r12, 8(%rdi)
.L3:
	addl	$1, 4(%rdi)
.L2:
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	testl	%eax, %eax
	js	.L18
	movq	160(%rbx), %rax
	movl	%ebp, %r8d
	testq	%rax, %rax
	je	.L9
	movq	32(%rax), %rdx
	cmpq	40(%rax), %rdx
	jnb	.L9
	leaq	4(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movl	%ebp, (%rdx)
.L8:
	testl	$32768, (%rbx)
	jne	.L1
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L14
	subl	$1, (%rdi)
.L1:
	popq	%rbx
	movl	%r8d, %eax
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L18:
	movl	$-1, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L9:
	movl	%ebp, %esi
	movq	%rbx, %rdi
.LEHB0:
	call	__woverflow
	movl	%eax, %r8d
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L4:
	movl	$1, %ecx
	movl	%edx, %eax
	lock cmpxchgl	%ecx, (%rdi)
	je	.L5
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L5
	.p2align 4,,10
	.p2align 3
.L14:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L19:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L16
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L16
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L17
	subl	$1, (%rdi)
.L16:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L17:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L16
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L16
.LFE71:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA71:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE71-.LLSDACSB71
.LLSDACSB71:
	.uleb128 .LEHB0-.LFB71
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L19-.LFB71
	.uleb128 0
	.uleb128 .LEHB1-.LFB71
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE71:
	.text
	.size	fputwc, .-fputwc
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
	.hidden	__woverflow
