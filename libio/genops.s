	.text
	.section	__libc_freeres_fn,"ax",@progbits
	.p2align 4,,15
	.type	buffer_free, @function
buffer_free:
.LFB105:
	pushq	%rbx
	movq	freeres_list(%rip), %rbx
	movb	$1, dealloc_buffers(%rip)
	testq	%rbx, %rbx
	je	.L1
	.p2align 4,,10
	.p2align 3
.L3:
	movq	176(%rbx), %rdi
	call	free@PLT
	movq	168(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rbx, freeres_list(%rip)
	jne	.L3
.L1:
	popq	%rbx
	ret
.LFE105:
	.size	buffer_free, .-buffer_free
	.text
	.p2align 4,,15
	.type	save_for_backup, @function
save_for_backup:
.LFB77:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r12
	movq	%rdi, %rbp
	subq	$40, %rsp
	movq	88(%rdi), %rsi
	movq	96(%rdi), %rax
	movq	24(%rdi), %r8
	movq	72(%rdi), %r15
	movq	%rsi, %r14
	movq	%rax, %rdx
	subq	%r8, %r13
	subq	%r15, %r14
	testq	%rax, %rax
	movq	%r13, %rbx
	je	.L34
	.p2align 4,,10
	.p2align 3
.L11:
	movslq	16(%rdx), %rcx
	movq	(%rdx), %rdx
	cmpq	%rcx, %rbx
	cmovg	%rcx, %rbx
	testq	%rdx, %rdx
	jne	.L11
	movq	%r13, %rdx
	subq	%rbx, %rdx
	cmpq	%r14, %rdx
	ja	.L35
	subq	%rdx, %r14
	testq	%rbx, %rbx
	leaq	(%r15,%r14), %rdi
	js	.L12
	testq	%rdx, %rdx
	jne	.L36
	movq	%rdi, 80(%rbp)
.L21:
	movl	%r13d, %edx
	.p2align 4,,10
	.p2align 3
.L22:
	subl	%edx, 16(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L22
.L23:
	xorl	%eax, %eax
.L10:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	100(%rdx), %rax
	movq	%rsi, 24(%rsp)
	movq	%r8, 16(%rsp)
	movq	%rdx, 8(%rsp)
	movq	%rax, %rdi
	movq	%rax, (%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r14
	je	.L26
	testq	%rbx, %rbx
	leaq	100(%rax), %rcx
	movq	8(%rsp), %rdx
	movq	16(%rsp), %r8
	movq	24(%rsp), %rsi
	js	.L37
	leaq	(%r8,%rbx), %rsi
	movq	%rcx, %rdi
	call	memcpy@PLT
	movq	%rax, %rcx
.L17:
	movq	%r15, %rdi
	movq	%rcx, 8(%rsp)
	call	free@PLT
	movq	%r14, 72(%rbp)
	subq	24(%rbp), %r12
	addq	(%rsp), %r14
	movq	96(%rbp), %rax
	movq	8(%rsp), %rcx
	movq	%r12, %r13
	movq	%r14, 88(%rbp)
.L18:
	testq	%rax, %rax
	movq	%rcx, 80(%rbp)
	jne	.L21
	jmp	.L23
.L38:
	movq	%rsi, %rdi
.L12:
	movq	%rbx, %rdx
	addq	%rbx, %rsi
	negq	%rdx
	call	memmove
	movq	%r14, %rdi
	movq	24(%rbp), %rsi
	movq	%r12, %rdx
	subq	%rbx, %rdi
	addq	72(%rbp), %rdi
	subq	%rsi, %rdx
.L33:
	call	memcpy@PLT
	subq	24(%rbp), %r12
	movq	72(%rbp), %rcx
	movq	96(%rbp), %rax
	addq	%r14, %rcx
	movq	%r12, %r13
	jmp	.L18
	.p2align 4,,10
	.p2align 3
.L36:
	leaq	(%r8,%rbx), %rsi
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L34:
	testq	%r13, %r13
	js	.L38
	movq	%rsi, 80(%rdi)
	jmp	.L23
	.p2align 4,,10
	.p2align 3
.L37:
	movq	%rbx, %rdx
	movq	%rcx, %rdi
	addq	%rbx, %rsi
	negq	%rdx
	movq	%rcx, 8(%rsp)
	call	__mempcpy@PLT
	movq	16(%rsp), %r8
	movq	%r13, %rdx
	movq	%rax, %rdi
	movq	%r8, %rsi
	call	memcpy@PLT
	movq	8(%rsp), %rcx
	jmp	.L17
.L26:
	movl	$-1, %eax
	jmp	.L10
.LFE77:
	.size	save_for_backup, .-save_for_backup
	.p2align 4,,15
	.type	flush_cleanup, @function
flush_cleanup:
.LFB68:
	movq	run_fp(%rip), %rax
	testq	%rax, %rax
	je	.L41
	testl	$32768, (%rax)
	je	.L48
.L41:
	movl	4+list_all_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L39
	movq	$0, 8+list_all_lock(%rip)
#APP
# 47 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L46
	subl	$1, list_all_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L46:
#APP
# 47 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L39
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 47 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L39:
	rep ret
	.p2align 4,,10
	.p2align 3
.L48:
	movq	136(%rax), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L41
	movq	$0, 8(%rdi)
#APP
# 46 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L43
	subl	$1, (%rdi)
	jmp	.L41
	.p2align 4,,10
	.p2align 3
.L43:
#APP
# 46 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L41
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 46 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L41
.LFE68:
	.size	flush_cleanup, .-flush_cleanup
	.p2align 4,,15
	.type	_IO_un_link.part.2, @function
_IO_un_link.part.2:
.LFB129:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	subq	$32, %rsp
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %rbx
	testq	%rbx, %rbx
	je	.L50
	leaq	flush_cleanup(%rip), %rsi
	movq	%rsp, %rdi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L51:
	movq	%fs:16, %r12
	cmpq	8+list_all_lock(%rip), %r12
	je	.L52
#APP
# 59 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L53
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L54:
	movq	%r12, 8+list_all_lock(%rip)
.L52:
	movl	0(%rbp), %esi
	movl	4+list_all_lock(%rip), %edx
	movq	%rbp, run_fp(%rip)
	movl	%esi, %r8d
	addl	$1, %edx
	andl	$32768, %r8d
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L55
	movq	136(%rbp), %rdi
	movq	%fs:16, %r12
	cmpq	8(%rdi), %r12
	je	.L56
#APP
# 61 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L57
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L58:
	movl	0(%rbp), %esi
	movq	136(%rbp), %rdi
	movl	%esi, %r8d
	movq	%r12, 8(%rdi)
	andl	$32768, %r8d
.L56:
	addl	$1, 4(%rdi)
	movq	_IO_list_all(%rip), %rdi
	testq	%rdi, %rdi
	je	.L59
.L75:
	cmpq	%rdi, %rbp
	je	.L90
	movq	104(%rdi), %rcx
	testq	%rcx, %rcx
	je	.L59
	cmpq	%rcx, %rbp
	jne	.L62
	jmp	.L91
	.p2align 4,,10
	.p2align 3
.L64:
	cmpq	%rdx, %rbp
	je	.L92
	movq	%rdx, %rcx
.L62:
	movq	104(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.L64
.L59:
	andb	$127, %sil
	testl	%r8d, %r8d
	movl	%esi, 0(%rbp)
	jne	.L89
.L65:
	movq	136(%rbp), %rdi
	movl	4(%rdi), %eax
	leal	-1(%rax), %ecx
	testl	%ecx, %ecx
	movl	%ecx, 4(%rdi)
	movl	4+list_all_lock(%rip), %edx
	jne	.L66
	movq	$0, 8(%rdi)
#APP
# 76 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L68
	subl	$1, (%rdi)
	movl	4+list_all_lock(%rip), %edx
.L66:
	subl	$1, %edx
	movq	$0, run_fp(%rip)
	testl	%edx, %edx
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L70
	movq	$0, 8+list_all_lock(%rip)
#APP
# 78 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L71
	subl	$1, list_all_lock(%rip)
.L70:
	testq	%rbx, %rbx
	je	.L49
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L49:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L55:
	movq	_IO_list_all(%rip), %rdi
	testq	%rdi, %rdi
	jne	.L75
	andb	$127, %sil
	movl	%esi, 0(%rbp)
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L90:
	movq	104(%rbp), %rax
	andb	$127, %sil
	testl	%r8d, %r8d
	movq	%rax, _IO_list_all(%rip)
	movl	%esi, 0(%rbp)
	je	.L65
.L89:
	movl	4+list_all_lock(%rip), %edx
	jmp	.L66
	.p2align 4,,10
	.p2align 3
.L50:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L92:
	addq	$104, %rcx
.L63:
	movq	104(%rbp), %rax
	movq	%rax, (%rcx)
	jmp	.L59
	.p2align 4,,10
	.p2align 3
.L71:
#APP
# 78 "genops.c" 1
	xchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L70
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 78 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L70
	.p2align 4,,10
	.p2align 3
.L53:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L54
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L57:
	movl	%r8d, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L58
	call	__lll_lock_wait_private
	jmp	.L58
	.p2align 4,,10
	.p2align 3
.L91:
	leaq	104(%rdi), %rcx
	jmp	.L63
.L68:
#APP
# 76 "genops.c" 1
	xchgl %r8d, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %r8d
	movl	4+list_all_lock(%rip), %edx
	jle	.L66
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 76 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L89
.LFE129:
	.size	_IO_un_link.part.2, .-_IO_un_link.part.2
	.p2align 4,,15
	.globl	_IO_un_link
	.hidden	_IO_un_link
	.type	_IO_un_link, @function
_IO_un_link:
.LFB69:
	testb	$-128, (%rdi)
	je	.L93
	jmp	_IO_un_link.part.2
	.p2align 4,,10
	.p2align 3
.L93:
	rep ret
.LFE69:
	.size	_IO_un_link, .-_IO_un_link
	.p2align 4,,15
	.globl	_IO_link_in
	.hidden	_IO_link_in
	.type	_IO_link_in, @function
_IO_link_in:
.LFB70:
	movl	(%rdi), %eax
	testb	$-128, %al
	jne	.L124
	pushq	%r12
	pushq	%rbp
	orb	$-128, %al
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %rbp
	movl	%eax, (%rdi)
	testq	%rbp, %rbp
	je	.L97
	leaq	flush_cleanup(%rip), %rsi
	movq	%rsp, %rdi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L98:
	movq	%fs:16, %r12
	cmpq	%r12, 8+list_all_lock(%rip)
	je	.L99
#APP
# 93 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L100
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L101:
	movq	%r12, 8+list_all_lock(%rip)
.L99:
	movl	4+list_all_lock(%rip), %edx
	movl	(%rbx), %ecx
	movq	%rbx, run_fp(%rip)
	addl	$1, %edx
	andl	$32768, %ecx
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L102
	movq	136(%rbx), %rdi
	movq	%fs:16, %r12
	cmpq	%r12, 8(%rdi)
	je	.L128
#APP
# 95 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L105
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L106:
	movl	(%rbx), %eax
	movq	136(%rbx), %rdi
	movq	_IO_list_all(%rip), %rdx
	addl	$1, 4(%rdi)
	testb	$-128, %ah
	movq	%r12, 8(%rdi)
	movq	%rbx, _IO_list_all(%rip)
	movq	%rdx, 104(%rbx)
	jne	.L127
.L104:
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	movl	4+list_all_lock(%rip), %edx
	jne	.L107
	movq	$0, 8(%rdi)
#APP
# 100 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L109
	subl	$1, (%rdi)
	movl	4+list_all_lock(%rip), %edx
	jmp	.L107
	.p2align 4,,10
	.p2align 3
.L97:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L98
	.p2align 4,,10
	.p2align 3
.L124:
	rep ret
	.p2align 4,,10
	.p2align 3
.L102:
	movq	_IO_list_all(%rip), %rax
	movq	%rbx, _IO_list_all(%rip)
	movq	%rax, 104(%rbx)
.L107:
	subl	$1, %edx
	movq	$0, run_fp(%rip)
	testl	%edx, %edx
	movl	%edx, 4+list_all_lock(%rip)
	jne	.L111
	movq	$0, 8+list_all_lock(%rip)
#APP
# 102 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L112
	subl	$1, list_all_lock(%rip)
.L111:
	testq	%rbp, %rbp
	je	.L95
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L95:
	addq	$32, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L128:
	movq	_IO_list_all(%rip), %rax
	addl	$1, 4(%rdi)
	movq	%rbx, _IO_list_all(%rip)
	movq	%rax, 104(%rbx)
	jmp	.L104
	.p2align 4,,10
	.p2align 3
.L100:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L101
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L101
	.p2align 4,,10
	.p2align 3
.L112:
#APP
# 102 "genops.c" 1
	xchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %edx
	jle	.L111
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 102 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L111
	.p2align 4,,10
	.p2align 3
.L105:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L106
	call	__lll_lock_wait_private
	jmp	.L106
	.p2align 4,,10
	.p2align 3
.L109:
#APP
# 100 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	movl	4+list_all_lock(%rip), %edx
	jle	.L107
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 100 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L127:
	movl	4+list_all_lock(%rip), %edx
	jmp	.L107
.LFE70:
	.size	_IO_link_in, .-_IO_link_in
	.p2align 4,,15
	.globl	_IO_least_marker
	.type	_IO_least_marker, @function
_IO_least_marker:
.LFB71:
	movq	96(%rdi), %rdx
	movq	%rsi, %rax
	subq	24(%rdi), %rax
	testq	%rdx, %rdx
	je	.L129
	.p2align 4,,10
	.p2align 3
.L131:
	movslq	16(%rdx), %rcx
	movq	(%rdx), %rdx
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
	testq	%rdx, %rdx
	jne	.L131
.L129:
	rep ret
.LFE71:
	.size	_IO_least_marker, .-_IO_least_marker
	.p2align 4,,15
	.globl	_IO_switch_to_main_get_area
	.type	_IO_switch_to_main_get_area, @function
_IO_switch_to_main_get_area:
.LFB72:
	movq	16(%rdi), %rax
	movq	88(%rdi), %rdx
	andl	$-257, (%rdi)
	movq	%rdx, 16(%rdi)
	movq	%rax, 88(%rdi)
	movq	24(%rdi), %rdx
	movq	72(%rdi), %rax
	movq	%rdx, 72(%rdi)
	movq	%rax, 24(%rdi)
	movq	%rax, 8(%rdi)
	ret
.LFE72:
	.size	_IO_switch_to_main_get_area, .-_IO_switch_to_main_get_area
	.p2align 4,,15
	.globl	_IO_switch_to_backup_area
	.type	_IO_switch_to_backup_area, @function
_IO_switch_to_backup_area:
.LFB73:
	movq	16(%rdi), %rdx
	movq	88(%rdi), %rax
	movq	72(%rdi), %rcx
	orl	$256, (%rdi)
	movq	%rdx, 88(%rdi)
	movq	24(%rdi), %rdx
	movq	%rax, 16(%rdi)
	movq	%rcx, 24(%rdi)
	movq	%rdx, 72(%rdi)
	movq	%rax, 8(%rdi)
	ret
.LFE73:
	.size	_IO_switch_to_backup_area, .-_IO_switch_to_backup_area
	.p2align 4,,15
	.globl	_IO_switch_to_get_mode
	.hidden	_IO_switch_to_get_mode
	.type	_IO_switch_to_get_mode, @function
_IO_switch_to_get_mode:
.LFB74:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	40(%rdi), %rax
	cmpq	32(%rdi), %rax
	jbe	.L139
	movq	216(%rdi), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L148
.L140:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%rbp)
	cmpl	$-1, %eax
	je	.L138
	movq	40(%rbx), %rax
.L139:
	movl	(%rbx), %edx
	testb	$1, %dh
	jne	.L149
	cmpq	%rax, 16(%rbx)
	movq	56(%rbx), %rcx
	movq	%rcx, 24(%rbx)
	jnb	.L143
	movq	%rax, 16(%rbx)
.L143:
	andb	$-9, %dh
	movq	%rax, 8(%rbx)
	movq	%rax, 48(%rbx)
	movq	%rax, 32(%rbx)
	movl	%edx, (%rbx)
	xorl	%eax, %eax
.L138:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L149:
	movq	80(%rbx), %rcx
	movq	%rcx, 24(%rbx)
	jmp	.L143
	.p2align 4,,10
	.p2align 3
.L148:
	call	_IO_vtable_check
	jmp	.L140
.LFE74:
	.size	_IO_switch_to_get_mode, .-_IO_switch_to_get_mode
	.p2align 4,,15
	.globl	_IO_free_backup_area
	.hidden	_IO_free_backup_area
	.type	_IO_free_backup_area, @function
_IO_free_backup_area:
.LFB75:
	pushq	%rbx
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	movq	72(%rdi), %rdi
	testb	$1, %ah
	je	.L151
	andb	$-2, %ah
	movq	%rdi, 8(%rbx)
	movl	%eax, (%rbx)
	movq	88(%rbx), %rax
	movq	%rax, 16(%rbx)
	movq	24(%rbx), %rax
	movq	%rdi, 24(%rbx)
	movq	%rax, %rdi
.L151:
	call	free@PLT
	movq	$0, 72(%rbx)
	movq	$0, 88(%rbx)
	movq	$0, 80(%rbx)
	popq	%rbx
	ret
.LFE75:
	.size	_IO_free_backup_area, .-_IO_free_backup_area
	.p2align 4,,15
	.globl	__overflow
	.hidden	__overflow
	.type	__overflow, @function
__overflow:
.LFB76:
	pushq	%rbx
	subq	$16, %rsp
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L157
	movl	$-1, 192(%rdi)
.L157:
	movq	216(%rdi), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbx, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L160
	movq	24(%rbx), %rax
	addq	$16, %rsp
	popq	%rbx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L160:
	movl	%esi, 12(%rsp)
	movq	%rdi, (%rsp)
	call	_IO_vtable_check
	movq	24(%rbx), %rax
	movl	12(%rsp), %esi
	movq	(%rsp), %rdi
	addq	$16, %rsp
	popq	%rbx
	jmp	*%rax
.LFE76:
	.size	__overflow, .-__overflow
	.p2align 4,,15
	.globl	__underflow
	.hidden	__underflow
	.type	__underflow, @function
__underflow:
.LFB78:
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L162
	movl	$-1, 192(%rdi)
.L163:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	$2048, (%rdi)
	jne	.L165
.L169:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	jb	.L188
	movl	(%rbx), %eax
	testb	$1, %ah
	jne	.L189
.L170:
	cmpq	$0, 96(%rbx)
	je	.L171
	movq	16(%rbx), %rsi
	movq	%rbx, %rdi
	call	save_for_backup
	testl	%eax, %eax
	jne	.L168
.L172:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L190
.L174:
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L189:
	andb	$-2, %ah
	movq	88(%rbx), %rcx
	movq	%rdx, 88(%rbx)
	movl	%eax, (%rbx)
	movq	72(%rbx), %rax
	movq	24(%rbx), %rdx
	movq	%rcx, 16(%rbx)
	cmpq	%rax, %rcx
	movq	%rax, 24(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rdx, 72(%rbx)
	jbe	.L170
.L188:
	movzbl	(%rax), %eax
.L161:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L162:
	cmpl	$-1, %eax
	je	.L163
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L165:
	call	_IO_switch_to_get_mode
	cmpl	$-1, %eax
	jne	.L169
.L168:
	movl	$-1, %eax
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L171:
	cmpq	$0, 72(%rbx)
	je	.L172
	movq	%rbx, %rdi
	call	_IO_free_backup_area
	jmp	.L172
	.p2align 4,,10
	.p2align 3
.L190:
	call	_IO_vtable_check
	jmp	.L174
.LFE78:
	.size	__underflow, .-__underflow
	.p2align 4,,15
	.globl	__uflow
	.hidden	__uflow
	.type	__uflow, @function
__uflow:
.LFB79:
	movl	192(%rdi), %eax
	testl	%eax, %eax
	jne	.L192
	movl	$-1, 192(%rdi)
.L193:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	testl	$2048, (%rdi)
	jne	.L195
.L199:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rdx
	cmpq	%rdx, %rax
	jb	.L201
	movl	(%rbx), %eax
	testb	$1, %ah
	jne	.L219
	cmpq	$0, 96(%rbx)
	je	.L202
.L222:
	movq	16(%rbx), %rsi
	movq	%rbx, %rdi
	call	save_for_backup
	testl	%eax, %eax
	jne	.L198
.L203:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L220
.L205:
	movq	40(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L219:
	andb	$-2, %ah
	movq	88(%rbx), %rcx
	movq	%rdx, 88(%rbx)
	movl	%eax, (%rbx)
	movq	72(%rbx), %rax
	movq	24(%rbx), %rdx
	movq	%rcx, 16(%rbx)
	cmpq	%rax, %rcx
	movq	%rax, 24(%rbx)
	movq	%rdx, 72(%rbx)
	jbe	.L221
.L201:
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movzbl	(%rax), %eax
.L191:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L192:
	cmpl	$-1, %eax
	je	.L193
	movl	$-1, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L195:
	call	_IO_switch_to_get_mode
	cmpl	$-1, %eax
	jne	.L199
.L198:
	movl	$-1, %eax
	jmp	.L191
	.p2align 4,,10
	.p2align 3
.L221:
	cmpq	$0, 96(%rbx)
	movq	%rax, 8(%rbx)
	jne	.L222
.L202:
	cmpq	$0, 72(%rbx)
	je	.L203
	movq	%rbx, %rdi
	call	_IO_free_backup_area
	jmp	.L203
	.p2align 4,,10
	.p2align 3
.L220:
	call	_IO_vtable_check
	jmp	.L205
.LFE79:
	.size	__uflow, .-__uflow
	.p2align 4,,15
	.globl	_IO_setb
	.hidden	_IO_setb
	.type	_IO_setb, @function
_IO_setb:
.LFB80:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	56(%rdi), %rdi
	movl	(%rbx), %eax
	testq	%rdi, %rdi
	je	.L224
	testb	$1, %al
	je	.L231
.L224:
	movq	%rdx, 64(%rbx)
	movl	%eax, %edx
	andl	$-2, %eax
	orl	$1, %edx
	testl	%ecx, %ecx
	movq	%rsi, 56(%rbx)
	cmove	%edx, %eax
	movl	%eax, (%rbx)
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L231:
	movl	%ecx, 28(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	free@PLT
	movl	(%rbx), %eax
	movl	28(%rsp), %ecx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	jmp	.L224
.LFE80:
	.size	_IO_setb, .-_IO_setb
	.p2align 4,,15
	.globl	_IO_doallocbuf
	.hidden	_IO_doallocbuf
	.type	_IO_doallocbuf, @function
_IO_doallocbuf:
.LFB81:
	cmpq	$0, 56(%rdi)
	je	.L250
	rep ret
	.p2align 4,,10
	.p2align 3
.L250:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	(%rdi), %eax
	movq	%rdi, %rbx
	testb	$2, %al
	je	.L235
	movl	192(%rdi), %edx
	testl	%edx, %edx
	jg	.L235
	leaq	132(%rdi), %r12
	leaq	131(%rdi), %rbp
.L239:
	orl	$1, %eax
	movq	%rbp, 56(%rbx)
	movq	%r12, 64(%rbx)
	movl	%eax, (%rbx)
.L232:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L235:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L251
.L237:
	movq	%rbx, %rdi
	call	*104(%rbp)
	cmpl	$-1, %eax
	jne	.L232
	movq	56(%rbx), %rdi
	leaq	132(%rbx), %r12
	leaq	131(%rbx), %rbp
	movl	(%rbx), %eax
	testq	%rdi, %rdi
	je	.L239
	testb	$1, %al
	jne	.L239
	call	free@PLT
	movl	(%rbx), %eax
	jmp	.L239
	.p2align 4,,10
	.p2align 3
.L251:
	call	_IO_vtable_check
	jmp	.L237
.LFE81:
	.size	_IO_doallocbuf, .-_IO_doallocbuf
	.p2align 4,,15
	.globl	_IO_default_underflow
	.type	_IO_default_underflow, @function
_IO_default_underflow:
.LFB82:
	movl	$-1, %eax
	ret
.LFE82:
	.size	_IO_default_underflow, .-_IO_default_underflow
	.p2align 4,,15
	.globl	_IO_default_uflow
	.hidden	_IO_default_uflow
	.type	_IO_default_uflow, @function
_IO_default_uflow:
.LFB83:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	216(%rdi), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L260
.L254:
	movq	%rbx, %rdi
	call	*32(%rbp)
	cmpl	$-1, %eax
	je	.L253
	movq	8(%rbx), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 8(%rbx)
	movzbl	(%rax), %eax
.L253:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L260:
	call	_IO_vtable_check
	jmp	.L254
.LFE83:
	.size	_IO_default_uflow, .-_IO_default_uflow
	.p2align 4,,15
	.globl	_IO_default_xsputn
	.hidden	_IO_default_xsputn
	.type	_IO_default_xsputn, @function
_IO_default_xsputn:
.LFB84:
	testq	%rdx, %rdx
	je	.L280
	pushq	%r15
	pushq	%r14
	movq	%rsi, %r14
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %r12
	movq	%rdx, %rbp
	subq	$8, %rsp
	jmp	.L270
	.p2align 4,,10
	.p2align 3
.L264:
	testq	%rbx, %rbx
	jne	.L285
.L265:
	subq	%rbx, %rbp
.L263:
	testq	%rbp, %rbp
	je	.L267
	movq	216(%r12), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbx, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L286
.L268:
	movzbl	(%r14), %esi
	movq	%r12, %rdi
	leaq	1(%r14), %r15
	call	*24(%rbx)
	cmpl	$-1, %eax
	je	.L287
	subq	$1, %rbp
	movq	%r15, %r14
.L270:
	movq	40(%r12), %rdi
	movq	48(%r12), %rbx
	cmpq	%rbx, %rdi
	jnb	.L263
	subq	%rdi, %rbx
	cmpq	%rbx, %rbp
	cmovbe	%rbp, %rbx
	cmpq	$20, %rbx
	jbe	.L264
	movq	%r14, %rsi
	movq	%rbx, %rdx
	addq	%rbx, %r14
	call	__mempcpy@PLT
	movq	%rax, 40(%r12)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L287:
	subq	%rbp, %r13
.L267:
	addq	$8, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L280:
	xorl	%eax, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L285:
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L266:
	movzbl	(%r14,%rax), %edx
	movb	%dl, (%rdi,%rax)
	addq	$1, %rax
	cmpq	%rax, %rbx
	jne	.L266
	addq	%rbx, %rdi
	addq	%rbx, %r14
	movq	%rdi, 40(%r12)
	jmp	.L265
	.p2align 4,,10
	.p2align 3
.L286:
	call	_IO_vtable_check
	jmp	.L268
.LFE84:
	.size	_IO_default_xsputn, .-_IO_default_xsputn
	.p2align 4,,15
	.globl	_IO_sgetn
	.hidden	_IO_sgetn
	.type	_IO_sgetn, @function
_IO_sgetn:
.LFB85:
	pushq	%rbx
	subq	$32, %rsp
	movq	216(%rdi), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rcx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbx, %r8
	subq	%rcx, %rax
	subq	%rcx, %r8
	cmpq	%r8, %rax
	jbe	.L291
	movq	64(%rbx), %rax
	addq	$32, %rsp
	popq	%rbx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L291:
	movq	%rdx, 24(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	64(%rbx), %rax
	movq	24(%rsp), %rdx
	movq	16(%rsp), %rsi
	movq	8(%rsp), %rdi
	addq	$32, %rsp
	popq	%rbx
	jmp	*%rax
.LFE85:
	.size	_IO_sgetn, .-_IO_sgetn
	.p2align 4,,15
	.globl	_IO_default_xsgetn
	.hidden	_IO_default_xsgetn
	.type	_IO_default_xsgetn, @function
_IO_default_xsgetn:
.LFB86:
	pushq	%r14
	movq	%rdx, %r14
	pushq	%r13
	movq	%rdi, %r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	movq	%rsi, %rbp
	pushq	%rbx
	jmp	.L298
	.p2align 4,,10
	.p2align 3
.L294:
	testq	%rax, %rax
	jne	.L308
.L295:
	subq	%rbx, %r14
.L293:
	testq	%r14, %r14
	je	.L297
	movq	%r13, %rdi
	call	__underflow
	cmpl	$-1, %eax
	je	.L309
.L298:
	movq	8(%r13), %rsi
	movq	16(%r13), %rax
	cmpq	%rax, %rsi
	jnb	.L293
	subq	%rsi, %rax
	cmpq	%rax, %r14
	cmovbe	%r14, %rax
	cmpq	$20, %rax
	movq	%rax, %rbx
	jbe	.L294
	movq	%rbp, %rdi
	movq	%rax, %rdx
	call	__mempcpy@PLT
	addq	%rbx, 8(%r13)
	movq	%rax, %rbp
	jmp	.L295
	.p2align 4,,10
	.p2align 3
.L309:
	subq	%r14, %r12
.L297:
	popq	%rbx
	movq	%r12, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L308:
	leal	-1(%rax), %ecx
	xorl	%eax, %eax
	addq	$1, %rcx
	.p2align 4,,10
	.p2align 3
.L296:
	movzbl	(%rsi,%rax), %edx
	movb	%dl, 0(%rbp,%rax)
	addq	$1, %rax
	cmpq	%rcx, %rax
	jne	.L296
	addq	%rax, %rsi
	addq	%rax, %rbp
	movq	%rsi, 8(%r13)
	jmp	.L295
.LFE86:
	.size	_IO_default_xsgetn, .-_IO_default_xsgetn
	.p2align 4,,15
	.globl	_IO_default_setbuf
	.type	_IO_default_setbuf, @function
_IO_default_setbuf:
.LFB87:
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rdx, %rbp
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	216(%rdi), %r12
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r12, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L327
.L311:
	movq	%rbx, %rdi
	call	*96(%r12)
	cmpl	$-1, %eax
	je	.L318
	movl	(%rbx), %eax
	testq	%r13, %r13
	movq	56(%rbx), %rdi
	movl	%eax, %edx
	je	.L319
	testq	%rbp, %rbp
	je	.L319
	andl	$-3, %edx
	addq	%r13, %rbp
	testq	%rdi, %rdi
	movl	%edx, (%rbx)
	je	.L317
	testb	$1, %al
	je	.L328
.L317:
	orl	$1, %edx
	movq	%r13, 56(%rbx)
	movq	%rbp, 64(%rbx)
	movl	%edx, (%rbx)
.L316:
	movq	$0, 48(%rbx)
	movq	$0, 40(%rbx)
	movq	%rbx, %rax
	movq	$0, 32(%rbx)
	movq	$0, 16(%rbx)
	movq	$0, 8(%rbx)
	movq	$0, 24(%rbx)
.L310:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L319:
	orl	$2, %edx
	testq	%rdi, %rdi
	leaq	132(%rbx), %rbp
	movl	%edx, (%rbx)
	leaq	131(%rbx), %r12
	je	.L315
	testb	$1, %al
	je	.L329
.L315:
	orl	$1, %edx
	movq	%r12, 56(%rbx)
	movq	%rbp, 64(%rbx)
	movl	%edx, (%rbx)
	jmp	.L316
	.p2align 4,,10
	.p2align 3
.L328:
	call	free@PLT
	movl	(%rbx), %edx
	jmp	.L317
	.p2align 4,,10
	.p2align 3
.L329:
	call	free@PLT
	movl	(%rbx), %edx
	jmp	.L315
	.p2align 4,,10
	.p2align 3
.L327:
	call	_IO_vtable_check
	jmp	.L311
	.p2align 4,,10
	.p2align 3
.L318:
	xorl	%eax, %eax
	jmp	.L310
.LFE87:
	.size	_IO_default_setbuf, .-_IO_default_setbuf
	.p2align 4,,15
	.globl	_IO_default_seekpos
	.type	_IO_default_seekpos, @function
_IO_default_seekpos:
.LFB88:
	pushq	%rbx
	subq	$32, %rsp
	movq	216(%rdi), %rbx
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %r8
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbx, %rcx
	subq	%r8, %rax
	subq	%r8, %rcx
	cmpq	%rcx, %rax
	jbe	.L333
	movq	72(%rbx), %rax
	addq	$32, %rsp
	movl	%edx, %ecx
	popq	%rbx
	xorl	%edx, %edx
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L333:
	movl	%edx, 28(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movl	28(%rsp), %edx
	movq	72(%rbx), %rax
	movq	16(%rsp), %rsi
	movq	8(%rsp), %rdi
	addq	$32, %rsp
	popq	%rbx
	movl	%edx, %ecx
	xorl	%edx, %edx
	jmp	*%rax
.LFE88:
	.size	_IO_default_seekpos, .-_IO_default_seekpos
	.p2align 4,,15
	.globl	_IO_default_doallocate
	.hidden	_IO_default_doallocate
	.type	_IO_default_doallocate, @function
_IO_default_doallocate:
.LFB89:
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$8192, %edi
	call	malloc@PLT
	movq	%rax, %rbp
	movl	$-1, %eax
	testq	%rbp, %rbp
	je	.L334
	movq	56(%rbx), %rdi
	leaq	8192(%rbp), %r12
	movl	(%rbx), %edx
	testq	%rdi, %rdi
	je	.L336
	testb	$1, %dl
	je	.L344
.L336:
	andl	$-2, %edx
	movq	%rbp, 56(%rbx)
	movq	%r12, 64(%rbx)
	movl	%edx, (%rbx)
	movl	$1, %eax
.L334:
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L344:
	call	free@PLT
	movl	(%rbx), %edx
	jmp	.L336
.LFE89:
	.size	_IO_default_doallocate, .-_IO_default_doallocate
	.p2align 4,,15
	.globl	_IO_enable_locks
	.hidden	_IO_enable_locks
	.type	_IO_enable_locks, @function
_IO_enable_locks:
.LFB92:
	movl	stdio_needs_locking(%rip), %eax
	testl	%eax, %eax
	jne	.L345
	movq	_IO_list_all(%rip), %rax
	movl	$1, stdio_needs_locking(%rip)
	testq	%rax, %rax
	je	.L345
	.p2align 4,,10
	.p2align 3
.L347:
	orl	$128, 116(%rax)
	movq	104(%rax), %rax
	testq	%rax, %rax
	jne	.L347
.L345:
	rep ret
.LFE92:
	.size	_IO_enable_locks, .-_IO_enable_locks
	.p2align 4,,15
	.globl	_IO_old_init
	.type	_IO_old_init, @function
_IO_old_init:
.LFB93:
	movl	stdio_needs_locking(%rip), %edx
	orl	$-72548352, %esi
	movl	$0, 116(%rdi)
	movl	%esi, (%rdi)
	testl	%edx, %edx
	je	.L353
	movl	$128, 116(%rdi)
.L353:
	xorl	%eax, %eax
	movq	$0, 56(%rdi)
	movq	$0, 64(%rdi)
	movw	%ax, 128(%rdi)
	movq	136(%rdi), %rax
	movq	$0, 24(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 32(%rdi)
	testq	%rax, %rax
	movq	$0, 40(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 104(%rdi)
	movq	$0, 72(%rdi)
	movq	$0, 80(%rdi)
	movq	$0, 88(%rdi)
	movq	$0, 96(%rdi)
	je	.L352
	movq	$0, (%rax)
	movq	$0, 8(%rax)
.L352:
	rep ret
.LFE93:
	.size	_IO_old_init, .-_IO_old_init
	.p2align 4,,15
	.globl	_IO_init_internal
	.hidden	_IO_init_internal
	.type	_IO_init_internal, @function
_IO_init_internal:
.LFB90:
	call	_IO_old_init
	movl	$-1, 192(%rdi)
	movq	$-1, 160(%rdi)
	movq	$0, 168(%rdi)
	ret
.LFE90:
	.size	_IO_init_internal, .-_IO_init_internal
	.p2align 4,,15
	.globl	_IO_init
	.type	_IO_init, @function
_IO_init:
.LFB91:
	call	_IO_old_init
	movl	$-1, 192(%rdi)
	movq	$-1, 160(%rdi)
	movq	$0, 168(%rdi)
	ret
.LFE91:
	.size	_IO_init, .-_IO_init
	.p2align 4,,15
	.globl	_IO_no_init
	.type	_IO_no_init, @function
_IO_no_init:
.LFB94:
	movl	%edx, %r10d
	movq	%rdi, %r9
	call	_IO_old_init
	testl	%r10d, %r10d
	movl	%r10d, 192(%rdi)
	js	.L361
	movq	%rcx, 160(%rdi)
	movq	$0, 48(%rcx)
	movq	$0, 56(%rcx)
	movq	$0, 16(%rcx)
	movq	$0, (%rcx)
	movq	$0, 8(%rcx)
	movq	$0, 24(%rcx)
	movq	$0, 32(%rcx)
	movq	$0, 40(%rcx)
	movq	$0, 64(%rcx)
	movq	$0, 72(%rcx)
	movq	$0, 80(%rcx)
	movq	%r8, 224(%rcx)
	movq	$0, 168(%r9)
	ret
	.p2align 4,,10
	.p2align 3
.L361:
	movq	$-1, 160(%rdi)
	movq	$0, 168(%r9)
	ret
.LFE94:
	.size	_IO_no_init, .-_IO_no_init
	.p2align 4,,15
	.globl	_IO_default_sync
	.type	_IO_default_sync, @function
_IO_default_sync:
.LFB95:
	xorl	%eax, %eax
	ret
.LFE95:
	.size	_IO_default_sync, .-_IO_default_sync
	.p2align 4,,15
	.globl	_IO_default_finish
	.hidden	_IO_default_finish
	.type	_IO_default_finish, @function
_IO_default_finish:
.LFB96:
	pushq	%rbx
	movq	%rdi, %rbx
	movq	56(%rdi), %rdi
	testq	%rdi, %rdi
	je	.L365
	testb	$1, (%rbx)
	je	.L381
.L365:
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L366
	.p2align 4,,10
	.p2align 3
.L367:
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L367
.L366:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L368
	call	free@PLT
	movq	$0, 72(%rbx)
.L368:
	testb	$-128, (%rbx)
	jne	.L382
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L381:
	call	free@PLT
	movq	$0, 64(%rbx)
	movq	$0, 56(%rbx)
	jmp	.L365
	.p2align 4,,10
	.p2align 3
.L382:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_IO_un_link.part.2
.LFE96:
	.size	_IO_default_finish, .-_IO_default_finish
	.p2align 4,,15
	.globl	_IO_default_seekoff
	.type	_IO_default_seekoff, @function
_IO_default_seekoff:
.LFB97:
	movq	$-1, %rax
	ret
.LFE97:
	.size	_IO_default_seekoff, .-_IO_default_seekoff
	.p2align 4,,15
	.globl	_IO_sputbackc
	.hidden	_IO_sputbackc
	.type	_IO_sputbackc, @function
_IO_sputbackc:
.LFB98:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	8(%rdi), %rax
	cmpq	24(%rdi), %rax
	jbe	.L385
	cmpb	%sil, -1(%rax)
	je	.L393
.L385:
	movq	216(%rbx), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L394
.L387:
	movq	%rbx, %rdi
	call	*48(%rbp)
	cmpl	$-1, %eax
	je	.L384
.L386:
	andl	$-17, (%rbx)
.L384:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L393:
	subq	$1, %rax
	movq	%rax, 8(%rdi)
	movzbl	%sil, %eax
	jmp	.L386
	.p2align 4,,10
	.p2align 3
.L394:
	movl	%esi, 12(%rsp)
	call	_IO_vtable_check
	movl	12(%rsp), %esi
	jmp	.L387
.LFE98:
	.size	_IO_sputbackc, .-_IO_sputbackc
	.p2align 4,,15
	.globl	_IO_sungetc
	.type	_IO_sungetc, @function
_IO_sungetc:
.LFB99:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	8(%rdi), %rax
	cmpq	24(%rdi), %rax
	jbe	.L396
	leaq	-1(%rax), %rdx
	movq	%rdx, 8(%rdi)
	movzbl	-1(%rax), %eax
.L397:
	andl	$-17, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L396:
	movq	216(%rdi), %rbp
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%rbp, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L405
.L398:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*48(%rbp)
	cmpl	$-1, %eax
	jne	.L397
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L405:
	call	_IO_vtable_check
	jmp	.L398
.LFE99:
	.size	_IO_sungetc, .-_IO_sungetc
	.p2align 4,,15
	.globl	_IO_adjust_column
	.hidden	_IO_adjust_column
	.type	_IO_adjust_column, @function
_IO_adjust_column:
.LFB100:
	movslq	%edx, %r8
	addq	%rsi, %r8
	cmpq	%r8, %rsi
	jnb	.L407
	cmpb	$10, -1(%r8)
	leaq	-1(%r8), %rcx
	jne	.L409
	jmp	.L408
	.p2align 4,,10
	.p2align 3
.L411:
	subq	$1, %rcx
	cmpb	$10, (%rcx)
	je	.L408
.L409:
	cmpq	%rcx, %rsi
	jne	.L411
.L407:
	leal	(%rdx,%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L408:
	subq	%rcx, %r8
	leal	-1(%r8), %eax
	ret
.LFE100:
	.size	_IO_adjust_column, .-_IO_adjust_column
	.p2align 4,,15
	.globl	_IO_flush_all_lockp
	.type	_IO_flush_all_lockp, @function
_IO_flush_all_lockp:
.LFB101:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movl	%edi, %ebp
	subq	$40, %rsp
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %r13
	testq	%r13, %r13
	je	.L414
	leaq	flush_cleanup(%rip), %rsi
	movq	%rsp, %rdi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L415:
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+list_all_lock(%rip)
	je	.L416
#APP
# 692 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L417
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L418:
	movq	%rbx, 8+list_all_lock(%rip)
.L416:
	movl	4+list_all_lock(%rip), %eax
	movq	_IO_list_all(%rip), %rbx
	xorl	%r12d, %r12d
	addl	$1, %eax
	testq	%rbx, %rbx
	movl	%eax, 4+list_all_lock(%rip)
	je	.L419
	movl	$1, %r14d
	jmp	.L432
	.p2align 4,,10
	.p2align 3
.L424:
	movq	160(%rbx), %rax
	movq	24(%rax), %rsi
	cmpq	%rsi, 32(%rax)
	jbe	.L426
.L425:
	movq	216(%rbx), %r15
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r15, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L448
.L427:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%r15)
	cmpl	$-1, %eax
	movl	$-1, %eax
	cmove	%eax, %r12d
.L426:
	testl	%ebp, %ebp
	je	.L429
	testl	$32768, (%rbx)
	jne	.L429
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L429
	movq	$0, 8(%rdi)
#APP
# 710 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L431
	subl	$1, (%rdi)
	.p2align 4,,10
	.p2align 3
.L429:
	movq	104(%rbx), %rbx
	movq	$0, run_fp(%rip)
	testq	%rbx, %rbx
	je	.L449
.L432:
	testl	%ebp, %ebp
	movq	%rbx, run_fp(%rip)
	je	.L420
	movl	(%rbx), %edx
	andl	$32768, %edx
	jne	.L420
	movq	136(%rbx), %rdi
	movq	%fs:16, %r15
	cmpq	%r15, 8(%rdi)
	je	.L421
#APP
# 699 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L422
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %r14d, (%rdi)
# 0 "" 2
#NO_APP
.L423:
	movq	136(%rbx), %rdi
	movq	%r15, 8(%rdi)
.L421:
	addl	$1, 4(%rdi)
.L420:
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jg	.L424
	movq	32(%rbx), %rax
	cmpq	%rax, 40(%rbx)
	ja	.L425
	jmp	.L426
	.p2align 4,,10
	.p2align 3
.L449:
	movl	4+list_all_lock(%rip), %eax
.L419:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L434
	movq	$0, 8+list_all_lock(%rip)
#APP
# 715 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L435
	subl	$1, list_all_lock(%rip)
.L434:
	testq	%r13, %r13
	je	.L413
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L413:
	addq	$40, %rsp
	movl	%r12d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L414:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L415
	.p2align 4,,10
	.p2align 3
.L448:
	call	_IO_vtable_check
	jmp	.L427
.L435:
#APP
# 715 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L434
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 715 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L434
	.p2align 4,,10
	.p2align 3
.L422:
	movl	%edx, %eax
	lock cmpxchgl	%r14d, (%rdi)
	je	.L423
	call	__lll_lock_wait_private
	jmp	.L423
	.p2align 4,,10
	.p2align 3
.L431:
#APP
# 710 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L429
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 710 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L429
	.p2align 4,,10
	.p2align 3
.L417:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L418
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L418
.LFE101:
	.size	_IO_flush_all_lockp, .-_IO_flush_all_lockp
	.p2align 4,,15
	.globl	_IO_cleanup
	.type	_IO_cleanup, @function
_IO_cleanup:
.LFB106:
	pushq	%r15
	pushq	%r14
	xorl	%edi, %edi
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	call	_IO_flush_all_lockp
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %r12
	movl	%eax, %r13d
	testq	%r12, %r12
	je	.L451
	leaq	flush_cleanup(%rip), %rsi
	movq	%rsp, %rdi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L452:
	movq	%fs:16, %rbx
	cmpq	8+list_all_lock(%rip), %rbx
	je	.L453
#APP
# 787 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L454
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L455:
	movq	%rbx, 8+list_all_lock(%rip)
.L453:
	movl	4+list_all_lock(%rip), %eax
	movq	_IO_list_all(%rip), %rbx
	addl	$1, %eax
	testq	%rbx, %rbx
	movl	%eax, 4+list_all_lock(%rip)
	je	.L456
	xorl	%r14d, %r14d
	jmp	.L470
	.p2align 4,,10
	.p2align 3
.L458:
	movl	$-1, 192(%rbx)
	movq	104(%rbx), %rbx
	testq	%rbx, %rbx
	je	.L494
.L470:
	movl	(%rbx), %ebp
	andl	$2, %ebp
	jne	.L458
	movl	192(%rbx), %edx
	testl	%edx, %edx
	je	.L458
.L465:
	movq	136(%rbx), %rdx
	testq	%rdx, %rdx
	je	.L460
	movq	%fs:16, %rsi
	cmpq	8(%rdx), %rsi
	je	.L461
#APP
# 807 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %ecx
	jne	.L462
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %ecx, (%rdx)
# 0 "" 2
#NO_APP
.L463:
	testl	%eax, %eax
	je	.L464
	call	__sched_yield
	cmpl	$1, %ebp
	jne	.L476
	movl	$2, %ebp
.L460:
	cmpb	$0, dealloc_buffers(%rip)
	je	.L495
.L466:
	movq	216(%rbx), %r15
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r15, %rcx
	subq	%rdx, %rax
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L496
.L467:
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	*88(%r15)
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jle	.L468
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	call	_IO_wsetb
.L468:
	cmpl	$2, %ebp
	je	.L458
	movq	136(%rbx), %rdi
	testq	%rdi, %rdi
	je	.L458
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L458
	movq	$0, 8(%rdi)
#APP
# 831 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L469
	subl	$1, (%rdi)
	jmp	.L458
	.p2align 4,,10
	.p2align 3
.L494:
	movl	4+list_all_lock(%rip), %eax
.L456:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L472
	movq	$0, 8+list_all_lock(%rip)
#APP
# 842 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L473
	subl	$1, list_all_lock(%rip)
.L472:
	testq	%r12, %r12
	je	.L450
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L450:
	addq	$40, %rsp
	movl	%r13d, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L476:
	movl	$1, %ebp
	jmp	.L465
	.p2align 4,,10
	.p2align 3
.L495:
	movl	(%rbx), %eax
	testb	$1, %al
	jne	.L466
	orl	$1, %eax
	movl	%eax, (%rbx)
	movq	freeres_list(%rip), %rax
	movq	%rbx, freeres_list(%rip)
	movq	%rax, 168(%rbx)
	movq	56(%rbx), %rax
	movq	%rax, 176(%rbx)
	jmp	.L466
	.p2align 4,,10
	.p2align 3
.L451:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L452
.L473:
#APP
# 842 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L472
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 842 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L472
	.p2align 4,,10
	.p2align 3
.L462:
	movl	%r14d, %eax
	lock cmpxchgl	%ecx, (%rdx)
	setne	%al
	movzbl	%al, %eax
	jmp	.L463
	.p2align 4,,10
	.p2align 3
.L496:
	call	_IO_vtable_check
	jmp	.L467
	.p2align 4,,10
	.p2align 3
.L454:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L455
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L455
	.p2align 4,,10
	.p2align 3
.L461:
	addl	$1, 4(%rdx)
	jmp	.L460
.L464:
	movq	136(%rbx), %rax
	movq	%rsi, 8(%rax)
	movl	$1, 4(%rax)
	jmp	.L460
.L469:
#APP
# 831 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L458
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 831 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L458
.LFE106:
	.size	_IO_cleanup, .-_IO_cleanup
	.p2align 4,,15
	.globl	_IO_flush_all
	.hidden	_IO_flush_all
	.type	_IO_flush_all, @function
_IO_flush_all:
.LFB102:
	movl	$1, %edi
	jmp	_IO_flush_all_lockp
.LFE102:
	.size	_IO_flush_all, .-_IO_flush_all
	.p2align 4,,15
	.globl	_IO_flush_all_linebuffered
	.hidden	_IO_flush_all_linebuffered
	.type	_IO_flush_all_linebuffered, @function
_IO_flush_all_linebuffered:
.LFB103:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	movq	_pthread_cleanup_push_defer@GOTPCREL(%rip), %rbp
	testq	%rbp, %rbp
	je	.L499
	leaq	flush_cleanup(%rip), %rsi
	movq	%rsp, %rdi
	xorl	%edx, %edx
	call	_pthread_cleanup_push_defer@PLT
.L500:
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+list_all_lock(%rip)
	je	.L501
#APP
# 738 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L502
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L503:
	movq	%rbx, 8+list_all_lock(%rip)
.L501:
	movl	4+list_all_lock(%rip), %eax
	movq	_IO_list_all(%rip), %rbx
	addl	$1, %eax
	testq	%rbx, %rbx
	movl	%eax, 4+list_all_lock(%rip)
	je	.L504
	movl	$202, %r12d
	jmp	.L515
	.p2align 4,,10
	.p2align 3
.L534:
	movq	136(%rbx), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L506
#APP
# 744 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L507
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L508:
	movq	136(%rbx), %rdi
	movq	%r13, 8(%rdi)
.L506:
	movl	(%rbx), %eax
	addl	$1, 4(%rdi)
	andl	$520, %eax
	cmpl	$512, %eax
	je	.L521
.L509:
	testl	$32768, (%rbx)
	jne	.L512
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L512
	movq	$0, 8(%rdi)
#APP
# 749 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L514
	subl	$1, (%rdi)
.L512:
	movq	104(%rbx), %rbx
	movq	$0, run_fp(%rip)
	testq	%rbx, %rbx
	je	.L533
.L515:
	movl	(%rbx), %edx
	movq	%rbx, run_fp(%rip)
	movl	%edx, %ecx
	andl	$32768, %ecx
	je	.L534
	andl	$520, %edx
	cmpl	$512, %edx
	jne	.L512
.L521:
	movq	216(%rbx), %r13
	movq	__start___libc_IO_vtables@GOTPCREL(%rip), %rdx
	movq	__stop___libc_IO_vtables@GOTPCREL(%rip), %rax
	movq	%r13, %rsi
	subq	%rdx, %rax
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L535
.L510:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*24(%r13)
	jmp	.L509
	.p2align 4,,10
	.p2align 3
.L533:
	movl	4+list_all_lock(%rip), %eax
.L504:
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L517
	movq	$0, 8+list_all_lock(%rip)
#APP
# 754 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L518
	subl	$1, list_all_lock(%rip)
.L517:
	testq	%rbp, %rbp
	je	.L498
	movq	%rsp, %rdi
	xorl	%esi, %esi
	call	_pthread_cleanup_pop_restore@PLT
.L498:
	addq	$40, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L499:
	leaq	flush_cleanup(%rip), %rax
	movq	$0, 8(%rsp)
	movq	%rax, (%rsp)
	jmp	.L500
.L518:
#APP
# 754 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L517
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 754 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L517
	.p2align 4,,10
	.p2align 3
.L507:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L508
	call	__lll_lock_wait_private
	jmp	.L508
	.p2align 4,,10
	.p2align 3
.L535:
	call	_IO_vtable_check
	jmp	.L510
	.p2align 4,,10
	.p2align 3
.L514:
#APP
# 749 "genops.c" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L512
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	%r12d, %eax
#APP
# 749 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L512
	.p2align 4,,10
	.p2align 3
.L502:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L503
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L503
.LFE103:
	.size	_IO_flush_all_linebuffered, .-_IO_flush_all_linebuffered
	.weak	_flushlbf
	.set	_flushlbf,_IO_flush_all_linebuffered
	.p2align 4,,15
	.globl	_IO_init_marker
	.type	_IO_init_marker, @function
_IO_init_marker:
.LFB107:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rsi), %edx
	movq	%rsi, 8(%rdi)
	testb	$8, %dh
	jne	.L544
.L537:
	andb	$1, %dh
	movq	8(%rsi), %rax
	jne	.L545
	subq	24(%rsi), %rax
	movl	%eax, 16(%rbx)
.L539:
	movq	96(%rsi), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 96(%rsi)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L545:
	subq	16(%rsi), %rax
	movl	%eax, 16(%rbx)
	jmp	.L539
	.p2align 4,,10
	.p2align 3
.L544:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	_IO_switch_to_get_mode
	movq	8(%rsp), %rsi
	movl	(%rsi), %edx
	jmp	.L537
.LFE107:
	.size	_IO_init_marker, .-_IO_init_marker
	.p2align 4,,15
	.globl	_IO_remove_marker
	.type	_IO_remove_marker, @function
_IO_remove_marker:
.LFB108:
	movq	8(%rdi), %rax
	movq	96(%rax), %rdx
	testq	%rdx, %rdx
	je	.L546
	cmpq	%rdi, %rdx
	jne	.L549
	jmp	.L556
	.p2align 4,,10
	.p2align 3
.L551:
	cmpq	%rax, %rdi
	je	.L550
	movq	%rax, %rdx
.L549:
	movq	(%rdx), %rax
	testq	%rax, %rax
	jne	.L551
.L546:
	rep ret
	.p2align 4,,10
	.p2align 3
.L556:
	leaq	96(%rax), %rdx
.L550:
	movq	(%rdi), %rax
	movq	%rax, (%rdx)
	ret
.LFE108:
	.size	_IO_remove_marker, .-_IO_remove_marker
	.p2align 4,,15
	.globl	_IO_marker_difference
	.type	_IO_marker_difference, @function
_IO_marker_difference:
.LFB109:
	movl	16(%rdi), %eax
	subl	16(%rsi), %eax
	ret
.LFE109:
	.size	_IO_marker_difference, .-_IO_marker_difference
	.p2align 4,,15
	.globl	_IO_marker_delta
	.type	_IO_marker_delta, @function
_IO_marker_delta:
.LFB110:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L562
	testl	$256, (%rax)
	movq	8(%rax), %rdx
	jne	.L563
	subl	24(%rax), %edx
	movl	16(%rdi), %eax
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L563:
	subl	16(%rax), %edx
	movl	16(%rdi), %eax
	subl	%edx, %eax
	ret
.L562:
	movl	$-1, %eax
	ret
.LFE110:
	.size	_IO_marker_delta, .-_IO_marker_delta
	.p2align 4,,15
	.globl	_IO_seekmark
	.type	_IO_seekmark, @function
_IO_seekmark:
.LFB111:
	movq	8(%rsi), %rdx
	movl	$-1, %eax
	cmpq	%rdi, %rdx
	jne	.L564
	movslq	16(%rsi), %rax
	movl	(%rdx), %ecx
	movl	%ecx, %edi
	andl	$256, %edi
	testl	%eax, %eax
	js	.L566
	testl	%edi, %edi
	movq	24(%rdx), %rsi
	je	.L568
	andb	$-2, %ch
	movq	88(%rdx), %rdi
	movl	%ecx, (%rdx)
	movq	16(%rdx), %rcx
	movq	%rdi, 16(%rdx)
	movq	%rcx, 88(%rdx)
	movq	72(%rdx), %rcx
	movq	%rsi, 72(%rdx)
	movq	%rcx, 24(%rdx)
	movq	%rcx, %rsi
.L568:
	addq	%rsi, %rax
	movq	%rax, 8(%rdx)
	xorl	%eax, %eax
.L564:
	rep ret
	.p2align 4,,10
	.p2align 3
.L566:
	testl	%edi, %edi
	movq	16(%rdx), %rsi
	jne	.L568
	orb	$1, %ch
	movq	72(%rdx), %rdi
	movl	%ecx, (%rdx)
	movq	88(%rdx), %rcx
	movq	%rsi, 88(%rdx)
	movq	24(%rdx), %rsi
	movq	%rdi, 24(%rdx)
	movq	%rcx, 16(%rdx)
	movq	%rsi, 72(%rdx)
	movq	%rcx, %rsi
	addq	%rsi, %rax
	movq	%rax, 8(%rdx)
	xorl	%eax, %eax
	jmp	.L564
.LFE111:
	.size	_IO_seekmark, .-_IO_seekmark
	.p2align 4,,15
	.globl	_IO_unsave_markers
	.hidden	_IO_unsave_markers
	.type	_IO_unsave_markers, @function
_IO_unsave_markers:
.LFB112:
	cmpq	$0, 96(%rdi)
	je	.L574
	movq	$0, 96(%rdi)
.L574:
	cmpq	$0, 72(%rdi)
	je	.L573
	jmp	_IO_free_backup_area
	.p2align 4,,10
	.p2align 3
.L573:
	rep ret
.LFE112:
	.size	_IO_unsave_markers, .-_IO_unsave_markers
	.p2align 4,,15
	.globl	_IO_default_pbackfail
	.hidden	_IO_default_pbackfail
	.type	_IO_default_pbackfail, @function
_IO_default_pbackfail:
.LFB113:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movl	%esi, %r12d
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movl	(%rdi), %r13d
	movq	8(%rdi), %rbp
	movq	24(%rdi), %r14
	movl	%r13d, %eax
	andl	$256, %eax
	cmpq	%r14, %rbp
	jbe	.L577
	testl	%eax, %eax
	je	.L599
.L578:
	leaq	-1(%rbp), %rax
	movq	%rax, 8(%rbx)
	movb	%r12b, -1(%rbp)
.L580:
	movzbl	%r12b, %eax
.L576:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L577:
	testl	%eax, %eax
	je	.L600
	movq	16(%rbx), %rdx
	subq	%r14, %rdx
	leaq	(%rdx,%rdx), %r15
	movq	%rdx, 8(%rsp)
	movq	%r15, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r13
	je	.L581
	movq	8(%rsp), %rdx
	movq	%r15, %rbp
	movq	%r14, %rsi
	subq	%rdx, %rbp
	addq	%rax, %rbp
	movq	%rbp, %rdi
	call	memcpy@PLT
	movq	%r14, %rdi
	call	free@PLT
	movq	%r13, 24(%rbx)
	addq	%r15, %r13
	movq	%rbp, 80(%rbx)
	movq	%r13, 16(%rbx)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L599:
	movzbl	-1(%rbp), %eax
	cmpl	%esi, %eax
	je	.L601
	cmpq	$0, 72(%rdi)
	je	.L584
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	save_for_backup
	testl	%eax, %eax
	jne	.L581
	movq	8(%rbx), %rcx
	movl	(%rbx), %r13d
	movq	88(%rbx), %rbp
	movq	72(%rbx), %rax
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L600:
	movq	72(%rbx), %rax
	testq	%rax, %rax
	je	.L584
	movq	%rbp, %rcx
	movq	88(%rbx), %rbp
.L582:
	movq	16(%rbx), %rdx
	orl	$256, %r13d
	movq	%rbp, 16(%rbx)
	movl	%r13d, (%rbx)
	movq	%rax, 24(%rbx)
	movq	%rcx, 72(%rbx)
	movq	%rdx, 88(%rbx)
	jmp	.L578
	.p2align 4,,10
	.p2align 3
.L584:
	movl	$128, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L581
	leaq	128(%rax), %rdx
	movq	%rbp, %rcx
	movq	%rdx, 80(%rbx)
	movq	%rdx, %rbp
	jmp	.L582
	.p2align 4,,10
	.p2align 3
.L601:
	subq	$1, %rbp
	movq	%rbp, 8(%rdi)
	jmp	.L580
.L581:
	movl	$-1, %eax
	jmp	.L576
.LFE113:
	.size	_IO_default_pbackfail, .-_IO_default_pbackfail
	.p2align 4,,15
	.globl	_IO_default_seek
	.type	_IO_default_seek, @function
_IO_default_seek:
.LFB114:
	movq	$-1, %rax
	ret
.LFE114:
	.size	_IO_default_seek, .-_IO_default_seek
	.p2align 4,,15
	.globl	_IO_default_stat
	.type	_IO_default_stat, @function
_IO_default_stat:
.LFB115:
	movl	$-1, %eax
	ret
.LFE115:
	.size	_IO_default_stat, .-_IO_default_stat
	.p2align 4,,15
	.globl	_IO_default_read
	.type	_IO_default_read, @function
_IO_default_read:
.LFB116:
	movq	$-1, %rax
	ret
.LFE116:
	.size	_IO_default_read, .-_IO_default_read
	.p2align 4,,15
	.globl	_IO_default_write
	.type	_IO_default_write, @function
_IO_default_write:
.LFB117:
	xorl	%eax, %eax
	ret
.LFE117:
	.size	_IO_default_write, .-_IO_default_write
	.p2align 4,,15
	.globl	_IO_default_showmanyc
	.type	_IO_default_showmanyc, @function
_IO_default_showmanyc:
.LFB131:
	movl	$-1, %eax
	ret
.LFE131:
	.size	_IO_default_showmanyc, .-_IO_default_showmanyc
	.p2align 4,,15
	.globl	_IO_default_imbue
	.type	_IO_default_imbue, @function
_IO_default_imbue:
.LFB119:
	rep ret
.LFE119:
	.size	_IO_default_imbue, .-_IO_default_imbue
	.p2align 4,,15
	.globl	_IO_iter_begin
	.hidden	_IO_iter_begin
	.type	_IO_iter_begin, @function
_IO_iter_begin:
.LFB120:
	movq	_IO_list_all(%rip), %rax
	ret
.LFE120:
	.size	_IO_iter_begin, .-_IO_iter_begin
	.p2align 4,,15
	.globl	_IO_iter_end
	.hidden	_IO_iter_end
	.type	_IO_iter_end, @function
_IO_iter_end:
.LFB121:
	xorl	%eax, %eax
	ret
.LFE121:
	.size	_IO_iter_end, .-_IO_iter_end
	.p2align 4,,15
	.globl	_IO_iter_next
	.hidden	_IO_iter_next
	.type	_IO_iter_next, @function
_IO_iter_next:
.LFB122:
	movq	104(%rdi), %rax
	ret
.LFE122:
	.size	_IO_iter_next, .-_IO_iter_next
	.p2align 4,,15
	.globl	_IO_iter_file
	.hidden	_IO_iter_file
	.type	_IO_iter_file, @function
_IO_iter_file:
.LFB123:
	movq	%rdi, %rax
	ret
.LFE123:
	.size	_IO_iter_file, .-_IO_iter_file
	.p2align 4,,15
	.globl	_IO_list_lock
	.hidden	_IO_list_lock
	.type	_IO_list_lock, @function
_IO_list_lock:
.LFB124:
	pushq	%rbx
	movq	%fs:16, %rbx
	cmpq	%rbx, 8+list_all_lock(%rip)
	je	.L613
#APP
# 1096 "genops.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L614
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, list_all_lock(%rip)
# 0 "" 2
#NO_APP
.L615:
	movq	%rbx, 8+list_all_lock(%rip)
.L613:
	addl	$1, 4+list_all_lock(%rip)
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L614:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, list_all_lock(%rip)
	je	.L615
	leaq	list_all_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L615
.LFE124:
	.size	_IO_list_lock, .-_IO_list_lock
	.p2align 4,,15
	.globl	_IO_list_unlock
	.hidden	_IO_list_unlock
	.type	_IO_list_unlock, @function
_IO_list_unlock:
.LFB125:
	movl	4+list_all_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+list_all_lock(%rip)
	jne	.L617
	movq	$0, 8+list_all_lock(%rip)
#APP
# 1105 "genops.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L620
	subl	$1, list_all_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L620:
#APP
# 1105 "genops.c" 1
	xchgl %eax, list_all_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L617
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	list_all_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 1105 "genops.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L617:
	rep ret
.LFE125:
	.size	_IO_list_unlock, .-_IO_list_unlock
	.p2align 4,,15
	.globl	_IO_list_resetlock
	.hidden	_IO_list_resetlock
	.type	_IO_list_resetlock, @function
_IO_list_resetlock:
.LFB126:
	movq	$0, list_all_lock(%rip)
	movq	$0, 8+list_all_lock(%rip)
	ret
.LFE126:
	.size	_IO_list_resetlock, .-_IO_list_resetlock
	.section	__libc_atexit,"aw",@progbits
	.align 8
	.type	__elf_set___libc_atexit_element__IO_cleanup__, @object
	.size	__elf_set___libc_atexit_element__IO_cleanup__, 8
__elf_set___libc_atexit_element__IO_cleanup__:
	.quad	_IO_cleanup
	.section	__libc_subfreeres,"aw",@progbits
	.align 8
	.type	__elf_set___libc_subfreeres_element_buffer_free__, @object
	.size	__elf_set___libc_subfreeres_element_buffer_free__, 8
__elf_set___libc_subfreeres_element_buffer_free__:
	.quad	buffer_free
	.local	freeres_list
	.comm	freeres_list,8,8
	.local	dealloc_buffers
	.comm	dealloc_buffers,1,1
	.local	stdio_needs_locking
	.comm	stdio_needs_locking,4,4
	.local	run_fp
	.comm	run_fp,8,8
	.local	list_all_lock
	.comm	list_all_lock,16,16
	.weak	__stop___libc_IO_vtables
	.weak	__start___libc_IO_vtables
	.weak	_pthread_cleanup_pop_restore
	.weak	_pthread_cleanup_push_defer
	.hidden	_IO_wsetb
	.hidden	__sched_yield
	.hidden	_IO_vtable_check
	.hidden	__lll_lock_wait_private
	.hidden	_IO_list_all
	.hidden	memmove
