	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	__GI___fflush_unlocked
	.hidden	__GI___fflush_unlocked
	.type	__GI___fflush_unlocked, @function
__GI___fflush_unlocked:
	testq	%rdi, %rdi
	je	.L7
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	$16, %rsp
	movq	216(%rdi), %rbx
	subq	%rdx, %rax
	movq	%rbx, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L8
.L3:
	call	*96(%rbx)
	testl	%eax, %eax
	setne	%al
	addq	$16, %rsp
	movzbl	%al, %eax
	negl	%eax
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	jmp	__GI__IO_flush_all
	.p2align 4,,10
	.p2align 3
.L8:
	movq	%rdi, 8(%rsp)
	call	_IO_vtable_check
	movq	8(%rsp), %rdi
	jmp	.L3
	.size	__GI___fflush_unlocked, .-__GI___fflush_unlocked
	.globl	__fflush_unlocked
	.set	__fflush_unlocked,__GI___fflush_unlocked
	.weak	__GI_fflush_unlocked
	.hidden	__GI_fflush_unlocked
	.set	__GI_fflush_unlocked,__fflush_unlocked
	.weak	fflush_unlocked
	.set	fflush_unlocked,__GI_fflush_unlocked
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
