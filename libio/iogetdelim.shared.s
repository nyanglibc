	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_getdelim
	.type	_IO_getdelim, @function
_IO_getdelim:
.LFB68:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$40, %rsp
	testq	%rdi, %rdi
	je	.L33
	testq	%rsi, %rsi
	je	.L33
	movl	%edx, 28(%rsp)
	movl	(%rcx), %edx
	movq	%rcx, %rbx
	movq	%rsi, 8(%rsp)
	movq	%rdi, %rbp
	movl	%edx, %ecx
	andl	$32768, %ecx
	jne	.L5
	movq	136(%rbx), %rdi
	movq	%fs:16, %r13
	cmpq	%r13, 8(%rdi)
	je	.L6
#APP
# 52 "iogetdelim.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	movl	$1, %edx
	jne	.L7
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, (%rdi)
# 0 "" 2
#NO_APP
.L8:
	movq	136(%rbx), %rdi
	movl	(%rbx), %eax
	addl	$1, 4(%rdi)
	testb	$32, %al
	movq	%r13, 8(%rdi)
	jne	.L29
.L27:
	cmpq	$0, 0(%rbp)
	je	.L12
	movq	8(%rsp), %rax
	cmpq	$0, (%rax)
	jne	.L15
.L12:
	movq	8(%rsp), %rax
	movl	$120, %edi
	movq	$120, (%rax)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, 0(%rbp)
	je	.L40
.L15:
	movq	8(%rbx), %r10
	movq	16(%rbx), %r14
	subq	%r10, %r14
	testq	%r14, %r14
	jle	.L42
.L14:
	xorl	%r15d, %r15d
	jmp	.L22
	.p2align 4,,10
	.p2align 3
.L18:
	movq	8(%rsp), %rcx
	leaq	(%r15,%r14), %r13
	movq	0(%rbp), %rdi
	leaq	1(%r13), %rax
	movq	(%rcx), %rdx
	cmpq	%rax, %rdx
	jnb	.L19
	addq	%rdx, %rdx
	cmpq	%rax, %rdx
	cmovb	%rax, %rdx
	movq	%rdx, %rsi
	movq	%rdx, 16(%rsp)
	call	realloc@PLT
	testq	%rax, %rax
	movq	%rax, %rdi
	movq	16(%rsp), %rdx
	je	.L40
	movq	%rax, 0(%rbp)
	movq	8(%rsp), %rax
	movq	8(%rbx), %r10
	movq	%rdx, (%rax)
.L19:
	addq	%r15, %rdi
	movq	%r14, %rdx
	movq	%r10, %rsi
	call	__GI_memcpy@PLT
	addq	%r14, 8(%rbx)
	testq	%r12, %r12
	jne	.L21
	movq	%rbx, %rdi
.LEHB0:
	call	__GI___underflow
	cmpl	$-1, %eax
	je	.L21
	movq	8(%rbx), %r10
	movq	16(%rbx), %r14
	movq	%r13, %r15
	subq	%r10, %r14
.L22:
	movl	28(%rsp), %esi
	movq	%r14, %rdx
	movq	%r10, %rdi
	movq	%r10, 16(%rsp)
	call	__GI_memchr
	movq	16(%rsp), %r10
	movq	%rax, %r12
	subq	%r10, %rax
	addq	$1, %rax
	testq	%r12, %r12
	cmovne	%rax, %r14
	movabsq	$9223372036854775807, %rax
	subq	%r15, %rax
	cmpq	%r14, %rax
	jg	.L18
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r13
	movl	$75, %fs:(%rax)
	movl	(%rbx), %eax
.L11:
	testb	$-128, %ah
	jne	.L1
	movq	136(%rbx), %rdi
.L28:
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L1
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L23
	subl	$1, (%rdi)
	jmp	.L1
	.p2align 4,,10
	.p2align 3
.L5:
	andl	$32, %edx
	je	.L27
	movq	$-1, %r13
.L1:
	addq	$40, %rsp
	movq	%r13, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L29:
	movq	$-1, %r13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L40:
	movl	(%rbx), %eax
	movq	$-1, %r13
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L6:
	addl	$1, 4(%rdi)
	andl	$32, %edx
	je	.L27
	movq	$-1, %r13
	jmp	.L28
	.p2align 4,,10
	.p2align 3
.L21:
	movq	0(%rbp), %rax
	movb	$0, (%rax,%r13)
	movl	(%rbx), %eax
	jmp	.L11
	.p2align 4,,10
	.p2align 3
.L42:
	movq	%rbx, %rdi
	call	__GI___underflow
	cmpl	$-1, %eax
	je	.L40
	movq	8(%rbx), %r10
	movq	16(%rbx), %r14
	subq	%r10, %r14
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L7:
	movl	%ecx, %eax
	lock cmpxchgl	%edx, (%rdi)
	je	.L8
	call	__lll_lock_wait_private
.LEHE0:
	jmp	.L8
	.p2align 4,,10
	.p2align 3
.L23:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L1
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L1
.L33:
	movq	__libc_errno@gottpoff(%rip), %rax
	movq	$-1, %r13
	movl	$22, %fs:(%rax)
	jmp	.L1
.L32:
	testl	$32768, (%rbx)
	movq	%rax, %r8
	jne	.L25
	movq	136(%rbx), %rdi
	movl	4(%rdi), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4(%rdi)
	jne	.L25
	movq	$0, 8(%rdi)
#APP
# 885 "libioP.h" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L26
	subl	$1, (%rdi)
.L25:
	movq	%r8, %rdi
.LEHB1:
	call	_Unwind_Resume@PLT
.LEHE1:
.L26:
#APP
# 885 "libioP.h" 1
	xchgl %eax, (%rdi)
# 0 "" 2
#NO_APP
	subl	$1, %eax
	jle	.L25
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	movl	$202, %eax
#APP
# 885 "libioP.h" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L25
.LFE68:
	.globl	__gcc_personality_v0
	.section	.gcc_except_table,"a",@progbits
.LLSDA68:
	.byte	0xff
	.byte	0xff
	.byte	0x1
	.uleb128 .LLSDACSE68-.LLSDACSB68
.LLSDACSB68:
	.uleb128 .LEHB0-.LFB68
	.uleb128 .LEHE0-.LEHB0
	.uleb128 .L32-.LFB68
	.uleb128 0
	.uleb128 .LEHB1-.LFB68
	.uleb128 .LEHE1-.LEHB1
	.uleb128 0
	.uleb128 0
.LLSDACSE68:
	.text
	.size	_IO_getdelim, .-_IO_getdelim
	.weak	getdelim
	.set	getdelim,_IO_getdelim
	.weak	__getdelim
	.set	__getdelim,_IO_getdelim
	.hidden	DW.ref.__gcc_personality_v0
	.weak	DW.ref.__gcc_personality_v0
	.section	.data.DW.ref.__gcc_personality_v0,"awG",@progbits,DW.ref.__gcc_personality_v0,comdat
	.align 8
	.type	DW.ref.__gcc_personality_v0, @object
	.size	DW.ref.__gcc_personality_v0, 8
DW.ref.__gcc_personality_v0:
	.quad	__gcc_personality_v0
	.hidden	__lll_lock_wait_private
