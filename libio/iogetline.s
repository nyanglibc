	.text
	.p2align 4,,15
	.globl	_IO_getline_info
	.hidden	_IO_getline_info
	.type	_IO_getline_info, @function
_IO_getline_info:
.LFB69:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r15
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r13
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %rbp
	movl	%ecx, %r12d
	subq	$40, %rsp
	testq	%r9, %r9
	movl	%r8d, 20(%rsp)
	movq	%r9, 24(%rsp)
	je	.L2
	movl	$0, (%r9)
.L2:
	movl	192(%r15), %eax
	testl	%eax, %eax
	je	.L31
.L3:
	testq	%r13, %r13
	movq	%rbp, 8(%rsp)
	jne	.L15
	jmp	.L16
	.p2align 4,,10
	.p2align 3
.L5:
	cmpq	%r13, %rbx
	movl	%r12d, %esi
	movq	%r14, %rdi
	cmovnb	%r13, %rbx
	movq	%rbx, %rdx
	call	memchr
	testq	%rax, %rax
	movq	%rax, %r8
	jne	.L32
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%r14, %rsi
	subq	%rbx, %r13
	addq	%rbx, %rbp
	call	memcpy@PLT
	addq	%rbx, 8(%r15)
	testq	%r13, %r13
	je	.L33
.L15:
	movq	8(%r15), %r14
	movq	16(%r15), %rbx
	subq	%r14, %rbx
	testq	%rbx, %rbx
	jg	.L5
	movq	%r15, %rdi
	call	__uflow
	cmpl	$-1, %eax
	je	.L34
	cmpl	%eax, %r12d
	je	.L35
	subq	$1, %r13
	movb	%al, 0(%rbp)
	addq	$1, %rbp
	testq	%r13, %r13
	jne	.L15
.L33:
	subq	8(%rsp), %rbp
.L1:
	addq	$40, %rsp
	movq	%rbp, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L32:
	movq	%rax, %r12
	movl	20(%rsp), %eax
	movq	%rbp, %rbx
	subq	%r14, %r12
	subq	8(%rsp), %rbx
	testl	%eax, %eax
	js	.L14
	addq	$1, %r8
	cmpl	$1, %eax
	sbbq	$-1, %r12
.L14:
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movq	%r14, %rsi
	movq	%r8, 8(%rsp)
	leaq	(%r12,%rbx), %rbp
	call	memcpy@PLT
	movq	8(%rsp), %r8
	movq	%r8, 8(%r15)
	jmp	.L1
.L31:
	movl	$-1, 192(%r15)
	jmp	.L3
.L34:
	movq	24(%rsp), %rcx
	subq	8(%rsp), %rbp
	testq	%rcx, %rcx
	je	.L1
	movl	%eax, (%rcx)
	jmp	.L1
.L35:
	cmpl	$0, 20(%rsp)
	jle	.L8
	leaq	1(%rbp), %rbx
	movb	%r12b, 0(%rbp)
.L9:
	subq	8(%rsp), %rbx
	movq	%rbx, %rbp
	jmp	.L1
.L16:
	xorl	%ebp, %ebp
	jmp	.L1
.L8:
	movq	%rbp, %rbx
	je	.L9
	movl	%r12d, %esi
	movq	%r15, %rdi
	call	_IO_sputbackc
	jmp	.L9
.LFE69:
	.size	_IO_getline_info, .-_IO_getline_info
	.p2align 4,,15
	.globl	_IO_getline
	.hidden	_IO_getline
	.type	_IO_getline, @function
_IO_getline:
.LFB68:
	xorl	%r9d, %r9d
	jmp	_IO_getline_info
.LFE68:
	.size	_IO_getline, .-_IO_getline
	.hidden	_IO_sputbackc
	.hidden	__uflow
	.hidden	memchr
