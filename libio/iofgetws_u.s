	.text
	.p2align 4,,15
	.globl	fgetws_unlocked
	.type	fgetws_unlocked, @function
fgetws_unlocked:
	testl	%esi, %esi
	jle	.L7
	cmpl	$1, %esi
	je	.L16
	pushq	%r12
	pushq	%rbp
	subl	$1, %esi
	pushq	%rbx
	movq	%rdx, %rbx
	movl	(%rdx), %edx
	movq	%rdi, %r12
	movl	$1, %r8d
	movl	$10, %ecx
	movl	%edx, %ebp
	andl	$-33, %edx
	movl	%edx, (%rbx)
	movslq	%esi, %rdx
	movq	%rdi, %rsi
	movq	%rbx, %rdi
	andl	$32, %ebp
	call	_IO_getwline@PLT
	xorl	%edi, %edi
	testq	%rax, %rax
	movl	(%rbx), %edx
	je	.L5
	testb	$32, %dl
	je	.L6
	movq	__libc_errno@gottpoff(%rip), %rcx
	xorl	%edi, %edi
	cmpl	$11, %fs:(%rcx)
	je	.L6
.L5:
	orl	%edx, %ebp
	movq	%rdi, %rax
	movl	%ebp, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L6:
	movl	$0, (%r12,%rax,4)
	movl	(%rbx), %edx
	movq	%r12, %rdi
	movq	%rdi, %rax
	orl	%edx, %ebp
	movl	%ebp, (%rbx)
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	movl	$0, (%rdi)
.L13:
	movq	%rdi, %rax
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	xorl	%edi, %edi
	jmp	.L13
	.size	fgetws_unlocked, .-fgetws_unlocked
