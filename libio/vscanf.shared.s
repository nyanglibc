	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.globl	_IO_vscanf
	.type	_IO_vscanf, @function
_IO_vscanf:
	movq	stdin@GOTPCREL(%rip), %rax
	movq	%rsi, %rdx
	movq	%rdi, %rsi
	xorl	%ecx, %ecx
	movq	(%rax), %rdi
	jmp	__vfscanf_internal
	.size	_IO_vscanf, .-_IO_vscanf
	.weak	vscanf
	.set	vscanf,_IO_vscanf
	.hidden	__vfscanf_internal
