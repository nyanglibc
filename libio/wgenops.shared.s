	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
#NO_APP
	.p2align 4,,15
	.type	save_for_wbackup.isra.0, @function
save_for_wbackup.isra.0:
.LFB95:
	pushq	%r15
	pushq	%r14
	movq	%rdi, %r9
	pushq	%r13
	pushq	%r12
	movq	%rdx, %r12
	pushq	%rbp
	pushq	%rbx
	movq	%rsi, %r13
	subq	$56, %rsp
	movq	(%rdi), %rax
	movq	%rdx, 8(%rsp)
	movq	(%rsi), %rdx
	movq	%rax, %r14
	movq	80(%rdx), %rsi
	movq	16(%rdx), %r10
	movq	64(%rdx), %rdi
	movq	%rsi, %r8
	subq	%r10, %r12
	subq	%rdi, %r8
	movq	%r12, %rbp
	movq	%r8, %r11
	sarq	$2, %rbp
	sarq	$2, %r11
	testq	%rax, %rax
	movq	%rbp, %rbx
	movq	%rbp, %rcx
	je	.L26
	.p2align 4,,10
	.p2align 3
.L2:
	movslq	16(%r14), %r8
	movq	(%r14), %r14
	cmpq	%r8, %rbx
	cmovg	%r8, %rbx
	testq	%r14, %r14
	jne	.L2
	movq	%rbp, %r12
	subq	%rbx, %r12
	cmpq	%r11, %r12
	ja	.L27
	movq	%r11, %r15
	subq	%r12, %r15
	leaq	0(,%r15,4), %r14
	addq	%r14, %rdi
	testq	%rbx, %rbx
	js	.L3
	testq	%r12, %r12
	movq	%r9, 16(%rsp)
	jne	.L28
	movq	%rdi, 72(%rdx)
	.p2align 4,,10
	.p2align 3
.L13:
	subl	%ecx, 16(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L13
.L14:
	xorl	%eax, %eax
.L1:
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L27:
	leaq	400(,%r12,4), %rax
	movq	%r9, 40(%rsp)
	movq	%rsi, 32(%rsp)
	movq	%r10, 24(%rsp)
	movq	%rax, %rdi
	movq	%rax, 16(%rsp)
	call	malloc@PLT
	testq	%rax, %rax
	movq	%rax, %r15
	je	.L17
	testq	%rbx, %rbx
	leaq	0(,%rbx,4), %rax
	leaq	400(%r15), %r14
	movq	24(%rsp), %r10
	movq	32(%rsp), %rsi
	movq	40(%rsp), %r9
	js	.L29
	leaq	(%r10,%rax), %rsi
	movq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r9, 24(%rsp)
	call	__wmemcpy
	movq	24(%rsp), %r9
.L8:
	movq	0(%r13), %rax
	movq	%r9, 24(%rsp)
	movq	64(%rax), %rdi
	call	free@PLT
	movq	0(%r13), %rdx
	movq	24(%rsp), %r9
	movq	8(%rsp), %rcx
	movq	%r15, 64(%rdx)
	subq	16(%rdx), %rcx
	addq	16(%rsp), %r15
	movq	(%r9), %rax
	sarq	$2, %rcx
	movq	%r15, 80(%rdx)
.L9:
	testq	%rax, %rax
	movq	%r14, 72(%rdx)
	jne	.L13
	jmp	.L14
.L30:
	movq	%rsi, %rdi
.L3:
	leaq	(%rsi,%rbx,4), %rsi
	movq	%rbx, %rdx
	movq	%r9, 16(%rsp)
	negq	%rdx
	subq	%rbx, %r15
	call	__wmemmove
	movq	0(%r13), %rax
	movq	8(%rsp), %rbp
	movq	16(%rax), %rsi
	movq	64(%rax), %rax
	movq	%rbp, %rdx
	subq	%rsi, %rdx
	leaq	(%rax,%r15,4), %rdi
	sarq	$2, %rdx
	call	__wmemcpy
	movq	0(%r13), %rdx
	movq	%rbp, %rcx
	movq	16(%rsp), %r9
	subq	16(%rdx), %rcx
	addq	64(%rdx), %r14
	movq	(%r9), %rax
	sarq	$2, %rcx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L28:
	leaq	(%r10,%rbx,4), %rsi
	movq	%r12, %rdx
	call	__wmemcpy
	movq	0(%r13), %rdx
	movq	8(%rsp), %rcx
	movq	16(%rsp), %r9
	subq	16(%rdx), %rcx
	addq	64(%rdx), %r14
	movq	(%r9), %rax
	sarq	$2, %rcx
	jmp	.L9
	.p2align 4,,10
	.p2align 3
.L26:
	testq	%r12, %r12
	movq	%r11, %r15
	movq	%r8, %r14
	js	.L30
	movq	%rsi, 72(%rdx)
	jmp	.L14
	.p2align 4,,10
	.p2align 3
.L29:
	movq	%rbx, %rdx
	addq	%rax, %rsi
	movq	%r14, %rdi
	negq	%rdx
	movq	%r9, 32(%rsp)
	call	__wmempcpy
	movq	24(%rsp), %r10
	movq	%rbp, %rdx
	movq	%rax, %rdi
	movq	%r10, %rsi
	call	__wmempcpy
	movq	32(%rsp), %r9
	jmp	.L8
.L17:
	movl	$-1, %eax
	jmp	.L1
.LFE95:
	.size	save_for_wbackup.isra.0, .-save_for_wbackup.isra.0
	.p2align 4,,15
	.globl	__GI__IO_least_wmarker
	.hidden	__GI__IO_least_wmarker
	.type	__GI__IO_least_wmarker, @function
__GI__IO_least_wmarker:
.LFB71:
	movq	160(%rdi), %rdx
	movq	%rsi, %rax
	subq	16(%rdx), %rax
	movq	96(%rdi), %rdx
	sarq	$2, %rax
	testq	%rdx, %rdx
	je	.L31
	.p2align 4,,10
	.p2align 3
.L33:
	movslq	16(%rdx), %rcx
	movq	(%rdx), %rdx
	cmpq	%rcx, %rax
	cmovg	%rcx, %rax
	testq	%rdx, %rdx
	jne	.L33
.L31:
	rep ret
.LFE71:
	.size	__GI__IO_least_wmarker, .-__GI__IO_least_wmarker
	.globl	_IO_least_wmarker
	.set	_IO_least_wmarker,__GI__IO_least_wmarker
	.p2align 4,,15
	.globl	__GI__IO_switch_to_main_wget_area
	.hidden	__GI__IO_switch_to_main_wget_area
	.type	__GI__IO_switch_to_main_wget_area, @function
__GI__IO_switch_to_main_wget_area:
.LFB72:
	movq	160(%rdi), %rax
	andl	$-257, (%rdi)
	movq	8(%rax), %rdx
	movq	80(%rax), %rcx
	movq	%rdx, 80(%rax)
	movq	64(%rax), %rdx
	movq	%rcx, 8(%rax)
	movq	16(%rax), %rcx
	movq	%rdx, (%rax)
	movq	%rdx, 16(%rax)
	movq	%rcx, 64(%rax)
	ret
.LFE72:
	.size	__GI__IO_switch_to_main_wget_area, .-__GI__IO_switch_to_main_wget_area
	.globl	_IO_switch_to_main_wget_area
	.set	_IO_switch_to_main_wget_area,__GI__IO_switch_to_main_wget_area
	.p2align 4,,15
	.globl	__GI__IO_switch_to_wbackup_area
	.hidden	__GI__IO_switch_to_wbackup_area
	.type	__GI__IO_switch_to_wbackup_area, @function
__GI__IO_switch_to_wbackup_area:
.LFB73:
	movq	160(%rdi), %rax
	orl	$256, (%rdi)
	movq	8(%rax), %rcx
	movq	80(%rax), %rdx
	movq	64(%rax), %rsi
	movq	%rcx, 80(%rax)
	movq	16(%rax), %rcx
	movq	%rdx, 8(%rax)
	movq	%rdx, (%rax)
	movq	%rsi, 16(%rax)
	movq	%rcx, 64(%rax)
	ret
.LFE73:
	.size	__GI__IO_switch_to_wbackup_area, .-__GI__IO_switch_to_wbackup_area
	.globl	_IO_switch_to_wbackup_area
	.set	_IO_switch_to_wbackup_area,__GI__IO_switch_to_wbackup_area
	.p2align 4,,15
	.globl	__GI__IO_wsetb
	.hidden	__GI__IO_wsetb
	.type	__GI__IO_wsetb, @function
__GI__IO_wsetb:
.LFB74:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$32, %rsp
	movq	160(%rdi), %r8
	movl	116(%rbx), %eax
	movq	48(%r8), %rdi
	testq	%rdi, %rdi
	je	.L41
	testb	$8, %al
	je	.L48
.L41:
	movq	%rdx, 56(%r8)
	movl	%eax, %edx
	andl	$-9, %eax
	orl	$8, %edx
	testl	%ecx, %ecx
	movq	%rsi, 48(%r8)
	cmove	%edx, %eax
	movl	%eax, 116(%rbx)
	addq	$32, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L48:
	movl	%ecx, 28(%rsp)
	movq	%rdx, 16(%rsp)
	movq	%rsi, 8(%rsp)
	call	free@PLT
	movq	160(%rbx), %r8
	movl	116(%rbx), %eax
	movl	28(%rsp), %ecx
	movq	16(%rsp), %rdx
	movq	8(%rsp), %rsi
	jmp	.L41
.LFE74:
	.size	__GI__IO_wsetb, .-__GI__IO_wsetb
	.globl	_IO_wsetb
	.set	_IO_wsetb,__GI__IO_wsetb
	.p2align 4,,15
	.globl	__GI__IO_wdefault_pbackfail
	.hidden	__GI__IO_wdefault_pbackfail
	.type	__GI__IO_wdefault_pbackfail, @function
__GI__IO_wdefault_pbackfail:
.LFB75:
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	movq	%rdi, %r13
	pushq	%rbp
	pushq	%rbx
	movl	%esi, %r12d
	subq	$24, %rsp
	movq	160(%rdi), %rbx
	movl	(%rdi), %r14d
	movq	(%rbx), %rbp
	movq	16(%rbx), %r15
	movl	%r14d, %eax
	andl	$256, %eax
	cmpq	%r15, %rbp
	jbe	.L50
	testl	%eax, %eax
	je	.L71
.L51:
	leaq	-4(%rbp), %rax
	movq	%rax, (%rbx)
	movl	%r12d, -4(%rbp)
	movl	%r12d, %eax
.L49:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L50:
	testl	%eax, %eax
	je	.L72
	movq	8(%rbx), %rbx
	subq	%r15, %rbx
	sarq	$2, %rbx
	leaq	0(,%rbx,8), %r14
	leaq	(%rbx,%rbx), %rbp
	movq	%r14, %rdi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L54
	subq	%rbx, %rbp
	movq	%rbx, %rdx
	movq	%r15, %rsi
	leaq	(%rax,%rbp,4), %rbp
	movq	%rax, 8(%rsp)
	movq	%rbp, %rdi
	call	__wmemcpy
	movq	160(%r13), %rax
	movq	16(%rax), %rdi
	call	free@PLT
	movq	8(%rsp), %rcx
	movq	160(%r13), %rbx
	addq	%rcx, %r14
	movq	%rcx, 16(%rbx)
	movq	%rbp, (%rbx)
	movq	%r14, 8(%rbx)
	movq	%rbp, 72(%rbx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L71:
	movq	8(%rdi), %rdx
	movsbl	-1(%rdx), %eax
	cmpl	%esi, %eax
	je	.L73
	cmpq	$0, 64(%rbx)
	je	.L56
	leaq	160(%r13), %rsi
	leaq	96(%r13), %rdi
	movq	%rbp, %rdx
	call	save_for_wbackup.isra.0
	testl	%eax, %eax
	jne	.L54
	movq	160(%r13), %rbx
	movl	0(%r13), %r14d
	movq	(%rbx), %rcx
	movq	80(%rbx), %rbp
	movq	64(%rbx), %rax
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L72:
	movq	64(%rbx), %rax
	testq	%rax, %rax
	je	.L56
	movq	%rbp, %rcx
	movq	80(%rbx), %rbp
.L55:
	movq	8(%rbx), %rdx
	orl	$256, %r14d
	movl	%r14d, 0(%r13)
	movq	%rbp, 8(%rbx)
	movq	%rax, 16(%rbx)
	movq	%rcx, 64(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rbp, (%rbx)
	jmp	.L51
	.p2align 4,,10
	.p2align 3
.L56:
	movl	$512, %edi
	call	malloc@PLT
	testq	%rax, %rax
	je	.L54
	leaq	512(%rax), %rdx
	movq	%rbp, %rcx
	movq	%rax, 64(%rbx)
	movq	%rdx, 80(%rbx)
	movq	%rdx, 72(%rbx)
	movq	%rdx, %rbp
	jmp	.L55
	.p2align 4,,10
	.p2align 3
.L73:
	subq	$1, %rdx
	movq	%rdx, 8(%rdi)
	jmp	.L49
	.p2align 4,,10
	.p2align 3
.L54:
	movl	$-1, %eax
	jmp	.L49
.LFE75:
	.size	__GI__IO_wdefault_pbackfail, .-__GI__IO_wdefault_pbackfail
	.globl	_IO_wdefault_pbackfail
	.set	_IO_wdefault_pbackfail,__GI__IO_wdefault_pbackfail
	.p2align 4,,15
	.globl	__GI__IO_wdefault_finish
	.hidden	__GI__IO_wdefault_finish
	.type	__GI__IO_wdefault_finish, @function
__GI__IO_wdefault_finish:
.LFB76:
	pushq	%rbx
	movq	160(%rdi), %rdx
	movq	%rdi, %rbx
	movq	48(%rdx), %rdi
	testq	%rdi, %rdi
	je	.L75
	testb	$8, 116(%rbx)
	je	.L87
.L75:
	movq	96(%rbx), %rax
	testq	%rax, %rax
	je	.L76
	.p2align 4,,10
	.p2align 3
.L77:
	movq	$0, 8(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.L77
.L76:
	cmpq	$0, 72(%rbx)
	je	.L78
	movq	64(%rdx), %rdi
	call	free@PLT
	movq	$0, 72(%rbx)
.L78:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	__GI__IO_un_link
	.p2align 4,,10
	.p2align 3
.L87:
	call	free@PLT
	movq	160(%rbx), %rdx
	movq	$0, 56(%rdx)
	movq	$0, 48(%rdx)
	jmp	.L75
.LFE76:
	.size	__GI__IO_wdefault_finish, .-__GI__IO_wdefault_finish
	.globl	_IO_wdefault_finish
	.set	_IO_wdefault_finish,__GI__IO_wdefault_finish
	.p2align 4,,15
	.globl	__GI__IO_wdefault_uflow
	.hidden	__GI__IO_wdefault_uflow
	.type	__GI__IO_wdefault_uflow, @function
__GI__IO_wdefault_uflow:
.LFB77:
	pushq	%rbp
	pushq	%rbx
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	216(%rdi), %rbp
	subq	%rdx, %rax
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L94
.L89:
	movq	%rbx, %rdi
	call	*32(%rbp)
	movl	$-1, %edx
	cmpl	%edx, %eax
	je	.L88
	movq	160(%rbx), %rdx
	movq	(%rdx), %rax
	leaq	4(%rax), %rcx
	movq	%rcx, (%rdx)
	movl	(%rax), %edx
.L88:
	addq	$8, %rsp
	movl	%edx, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L94:
	call	_IO_vtable_check
	jmp	.L89
.LFE77:
	.size	__GI__IO_wdefault_uflow, .-__GI__IO_wdefault_uflow
	.globl	_IO_wdefault_uflow
	.set	_IO_wdefault_uflow,__GI__IO_wdefault_uflow
	.p2align 4,,15
	.globl	__GI___woverflow
	.hidden	__GI___woverflow
	.type	__GI___woverflow, @function
__GI___woverflow:
.LFB78:
	pushq	%r12
	pushq	%rbp
	movl	%esi, %r12d
	pushq	%rbx
	movl	192(%rdi), %eax
	movq	%rdi, %rbx
	testl	%eax, %eax
	je	.L99
.L96:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L100
	movq	24(%rbp), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L99:
	movl	$1, %esi
	call	_IO_fwide@PLT
	jmp	.L96
	.p2align 4,,10
	.p2align 3
.L100:
	call	_IO_vtable_check
	movq	24(%rbp), %rax
	movl	%r12d, %esi
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	popq	%r12
	jmp	*%rax
.LFE78:
	.size	__GI___woverflow, .-__GI___woverflow
	.globl	__woverflow
	.set	__woverflow,__GI___woverflow
	.p2align 4,,15
	.globl	__GI__IO_wdefault_xsputn
	.hidden	__GI__IO_wdefault_xsputn
	.type	__GI__IO_wdefault_xsputn, @function
__GI__IO_wdefault_xsputn:
.LFB81:
	pushq	%r15
	pushq	%r14
	xorl	%eax, %eax
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	subq	$24, %rsp
	testq	%rdx, %rdx
	movq	%rdx, 8(%rsp)
	je	.L101
	movq	%rdi, %rbp
	movq	%rsi, %r12
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L110:
	movq	160(%rbp), %r15
	movq	%r12, %r14
	movq	32(%r15), %rdi
	movq	40(%r15), %rbx
	subq	%rdi, %rbx
	testq	%rbx, %rbx
	jle	.L103
	sarq	$2, %rbx
	cmpq	%r13, %rbx
	cmova	%r13, %rbx
	cmpq	$20, %rbx
	jg	.L122
	testq	%rbx, %rbx
	je	.L103
	leaq	0(,%rbx,4), %rsi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L107:
	movl	(%r12,%rax), %edx
	movl	%edx, (%rdi,%rax)
	addq	$4, %rax
	cmpq	%rax, %rsi
	jne	.L107
	addq	%rsi, %rdi
	leaq	(%r12,%rsi), %r14
	subq	%rbx, %r13
	movq	%rdi, 32(%r15)
.L103:
	testq	%r13, %r13
	je	.L108
	movl	(%r14), %esi
	movq	%rbp, %rdi
	leaq	4(%r14), %r12
	call	__GI___woverflow
	cmpl	$-1, %eax
	je	.L123
	subq	$1, %r13
	jmp	.L110
	.p2align 4,,10
	.p2align 3
.L123:
	subq	%r13, 8(%rsp)
.L108:
	movq	8(%rsp), %rax
.L101:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L122:
	movq	%rbx, %rdx
	movq	%r12, %rsi
	leaq	(%r12,%rbx,4), %r14
	call	__wmempcpy
	subq	%rbx, %r13
	movq	%rax, 32(%r15)
	jmp	.L103
.LFE81:
	.size	__GI__IO_wdefault_xsputn, .-__GI__IO_wdefault_xsputn
	.globl	_IO_wdefault_xsputn
	.set	_IO_wdefault_xsputn,__GI__IO_wdefault_xsputn
	.p2align 4,,15
	.globl	__GI__IO_wdoallocbuf
	.hidden	__GI__IO_wdoallocbuf
	.type	__GI__IO_wdoallocbuf, @function
__GI__IO_wdoallocbuf:
.LFB83:
	movq	160(%rdi), %rsi
	cmpq	$0, 48(%rsi)
	je	.L133
	rep ret
	.p2align 4,,10
	.p2align 3
.L133:
	pushq	%rbx
	testb	$2, (%rdi)
	movq	%rdi, %rbx
	jne	.L127
	movq	224(%rsi), %rax
	call	*104(%rax)
	cmpl	$-1, %eax
	je	.L134
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L134:
	movq	160(%rbx), %rsi
.L127:
	movq	%rbx, %rdi
	leaq	220(%rsi), %rdx
	xorl	%ecx, %ecx
	popq	%rbx
	addq	$216, %rsi
	jmp	__GI__IO_wsetb
.LFE83:
	.size	__GI__IO_wdoallocbuf, .-__GI__IO_wdoallocbuf
	.globl	_IO_wdoallocbuf
	.set	_IO_wdoallocbuf,__GI__IO_wdoallocbuf
	.p2align 4,,15
	.globl	__GI__IO_wdefault_doallocate
	.hidden	__GI__IO_wdefault_doallocate
	.type	__GI__IO_wdefault_doallocate, @function
__GI__IO_wdefault_doallocate:
.LFB84:
	pushq	%rbx
	movq	%rdi, %rbx
	movl	$8192, %edi
	call	malloc@PLT
	movq	%rax, %rsi
	movl	$-1, %eax
	testq	%rsi, %rsi
	je	.L135
	leaq	32768(%rsi), %rdx
	movl	$1, %ecx
	movq	%rbx, %rdi
	call	__GI__IO_wsetb
	movl	$1, %eax
.L135:
	popq	%rbx
	ret
.LFE84:
	.size	__GI__IO_wdefault_doallocate, .-__GI__IO_wdefault_doallocate
	.globl	_IO_wdefault_doallocate
	.set	_IO_wdefault_doallocate,__GI__IO_wdefault_doallocate
	.p2align 4,,15
	.globl	__GI__IO_switch_to_wget_mode
	.hidden	__GI__IO_switch_to_wget_mode
	.type	__GI__IO_switch_to_wget_mode, @function
__GI__IO_switch_to_wget_mode:
.LFB85:
	movq	160(%rdi), %rax
	pushq	%rbx
	movq	%rdi, %rbx
	movq	32(%rax), %rdx
	cmpq	24(%rax), %rdx
	jbe	.L142
	movq	224(%rax), %rax
	movl	$-1, %esi
	call	*24(%rax)
	cmpl	$-1, %eax
	je	.L141
	movq	160(%rbx), %rax
	movq	32(%rax), %rdx
.L142:
	movl	(%rbx), %ecx
	testb	$1, %ch
	jne	.L150
	cmpq	%rdx, 8(%rax)
	movq	48(%rax), %rsi
	movq	%rsi, 16(%rax)
	jnb	.L145
	movq	%rdx, 8(%rax)
.L145:
	andb	$-9, %ch
	movq	%rdx, (%rax)
	movq	%rdx, 40(%rax)
	movq	%rdx, 24(%rax)
	movl	%ecx, (%rbx)
	xorl	%eax, %eax
.L141:
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L150:
	movq	72(%rax), %rsi
	movq	%rsi, 16(%rax)
	jmp	.L145
.LFE85:
	.size	__GI__IO_switch_to_wget_mode, .-__GI__IO_switch_to_wget_mode
	.globl	_IO_switch_to_wget_mode
	.set	_IO_switch_to_wget_mode,__GI__IO_switch_to_wget_mode
	.p2align 4,,15
	.globl	__GI__IO_free_wbackup_area
	.hidden	__GI__IO_free_wbackup_area
	.type	__GI__IO_free_wbackup_area, @function
__GI__IO_free_wbackup_area:
.LFB86:
	pushq	%rbx
	movl	(%rdi), %edx
	movq	%rdi, %rbx
	movq	160(%rdi), %rax
	testb	$1, %dh
	movq	64(%rax), %rdi
	je	.L152
	andb	$-2, %dh
	movq	80(%rax), %rcx
	movl	%edx, (%rbx)
	movq	8(%rax), %rdx
	movq	%rdi, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rdx, 80(%rax)
	movq	16(%rax), %rdx
	movq	%rdi, 16(%rax)
	movq	%rdx, 64(%rax)
	movq	%rdx, %rdi
.L152:
	call	free@PLT
	movq	160(%rbx), %rax
	movq	$0, 64(%rax)
	movq	$0, 80(%rax)
	movq	$0, 72(%rax)
	popq	%rbx
	ret
.LFE86:
	.size	__GI__IO_free_wbackup_area, .-__GI__IO_free_wbackup_area
	.globl	_IO_free_wbackup_area
	.set	_IO_free_wbackup_area,__GI__IO_free_wbackup_area
	.p2align 4,,15
	.globl	__GI___wuflow
	.hidden	__GI___wuflow
	.type	__GI___wuflow, @function
__GI___wuflow:
.LFB79:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	192(%rdi), %edx
	testl	%edx, %edx
	js	.L162
	movq	%rdi, %rbx
	je	.L182
.L161:
	testl	$2048, (%rbx)
	jne	.L164
.L167:
	movq	160(%rbx), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L169
	movl	(%rbx), %edx
	testb	$1, %dh
	je	.L168
	andb	$-2, %dh
	movq	80(%rax), %rsi
	movl	%edx, (%rbx)
	movq	64(%rax), %rdx
	movq	%rcx, 80(%rax)
	movq	16(%rax), %rcx
	movq	%rsi, 8(%rax)
	cmpq	%rdx, %rsi
	movq	%rdx, 16(%rax)
	movq	%rcx, 64(%rax)
	ja	.L169
	movq	%rdx, (%rax)
.L168:
	cmpq	$0, 96(%rbx)
	je	.L170
	movq	8(%rax), %rdx
	leaq	160(%rbx), %rsi
	leaq	96(%rbx), %rdi
	call	save_for_wbackup.isra.0
	testl	%eax, %eax
	jne	.L162
.L171:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L183
.L173:
	movq	40(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L169:
	leaq	4(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	(%rdx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L164:
	movq	%rbx, %rdi
	call	__GI__IO_switch_to_wget_mode
	cmpl	$-1, %eax
	jne	.L167
.L162:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L182:
	movl	$1, %esi
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L162
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jne	.L161
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	jmp	.L161
	.p2align 4,,10
	.p2align 3
.L170:
	cmpq	$0, 64(%rax)
	je	.L171
	movq	%rbx, %rdi
	call	__GI__IO_free_wbackup_area
	jmp	.L171
	.p2align 4,,10
	.p2align 3
.L183:
	call	_IO_vtable_check
	jmp	.L173
.LFE79:
	.size	__GI___wuflow, .-__GI___wuflow
	.globl	__wuflow
	.set	__wuflow,__GI___wuflow
	.p2align 4,,15
	.globl	__GI___wunderflow
	.hidden	__GI___wunderflow
	.type	__GI___wunderflow, @function
__GI___wunderflow:
.LFB80:
	pushq	%rbp
	pushq	%rbx
	subq	$8, %rsp
	movl	192(%rdi), %edx
	testl	%edx, %edx
	js	.L189
	movq	%rdi, %rbx
	je	.L209
.L188:
	testl	$2048, (%rbx)
	jne	.L191
.L194:
	movq	160(%rbx), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rcx
	cmpq	%rcx, %rdx
	jb	.L208
	movl	(%rbx), %edx
	testb	$1, %dh
	je	.L195
	andb	$-2, %dh
	movq	80(%rax), %rsi
	movl	%edx, (%rbx)
	movq	64(%rax), %rdx
	movq	%rcx, 80(%rax)
	movq	16(%rax), %rcx
	movq	%rsi, 8(%rax)
	cmpq	%rdx, %rsi
	movq	%rdx, 16(%rax)
	movq	%rdx, (%rax)
	movq	%rcx, 64(%rax)
	ja	.L208
.L195:
	cmpq	$0, 96(%rbx)
	je	.L196
	movq	8(%rax), %rdx
	leaq	160(%rbx), %rsi
	leaq	96(%rbx), %rdi
	call	save_for_wbackup.isra.0
	testl	%eax, %eax
	jne	.L189
.L197:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L210
.L199:
	movq	32(%rbp), %rax
	addq	$8, %rsp
	movq	%rbx, %rdi
	popq	%rbx
	popq	%rbp
	jmp	*%rax
	.p2align 4,,10
	.p2align 3
.L208:
	movl	(%rdx), %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L191:
	movq	%rbx, %rdi
	call	__GI__IO_switch_to_wget_mode
	cmpl	$-1, %eax
	jne	.L194
.L189:
	addq	$8, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L209:
	movl	$1, %esi
	call	_IO_fwide@PLT
	cmpl	$1, %eax
	jne	.L189
	movl	192(%rbx), %eax
	testl	%eax, %eax
	jne	.L188
	movl	$1, %esi
	movq	%rbx, %rdi
	call	_IO_fwide@PLT
	jmp	.L188
	.p2align 4,,10
	.p2align 3
.L196:
	cmpq	$0, 72(%rbx)
	je	.L197
	movq	%rbx, %rdi
	call	__GI__IO_free_wbackup_area
	jmp	.L197
	.p2align 4,,10
	.p2align 3
.L210:
	call	_IO_vtable_check
	jmp	.L199
.LFE80:
	.size	__GI___wunderflow, .-__GI___wunderflow
	.globl	__wunderflow
	.set	__wunderflow,__GI___wunderflow
	.p2align 4,,15
	.globl	__GI__IO_wdefault_xsgetn
	.hidden	__GI__IO_wdefault_xsgetn
	.type	__GI__IO_wdefault_xsgetn, @function
__GI__IO_wdefault_xsgetn:
.LFB82:
	pushq	%r14
	pushq	%r13
	movq	%rsi, %r14
	pushq	%r12
	pushq	%rbp
	movq	%rdx, %r12
	pushq	%rbx
	movq	%rdi, %rbp
	movq	%rdx, %r13
	.p2align 4,,10
	.p2align 3
.L218:
	movq	160(%rbp), %rdx
	movq	(%rdx), %rsi
	movq	8(%rdx), %rbx
	subq	%rsi, %rbx
	testq	%rbx, %rbx
	jle	.L212
	sarq	$2, %rbx
	cmpq	%r13, %rbx
	cmova	%r13, %rbx
	cmpq	$20, %rbx
	jg	.L228
	testq	%rbx, %rbx
	je	.L212
	leal	-1(%rbx), %eax
	leaq	4(,%rax,4), %rdi
	xorl	%eax, %eax
	.p2align 4,,10
	.p2align 3
.L216:
	movl	(%rsi,%rax), %ecx
	movl	%ecx, (%r14,%rax)
	addq	$4, %rax
	cmpq	%rdi, %rax
	jne	.L216
	addq	%rax, %rsi
	addq	%rax, %r14
	subq	%rbx, %r13
	movq	%rsi, (%rdx)
.L212:
	testq	%r13, %r13
	je	.L217
	movq	%rbp, %rdi
	call	__GI___wunderflow
	cmpl	$-1, %eax
	jne	.L218
	subq	%r13, %r12
.L217:
	popq	%rbx
	movq	%r12, %rax
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	ret
	.p2align 4,,10
	.p2align 3
.L228:
	movq	%rbx, %rdx
	movq	%r14, %rdi
	subq	%rbx, %r13
	call	__wmempcpy
	movq	%rax, %r14
	movq	160(%rbp), %rax
	leaq	0(,%rbx,4), %rdx
	addq	%rdx, (%rax)
	jmp	.L212
.LFE82:
	.size	__GI__IO_wdefault_xsgetn, .-__GI__IO_wdefault_xsgetn
	.globl	_IO_wdefault_xsgetn
	.set	_IO_wdefault_xsgetn,__GI__IO_wdefault_xsgetn
	.p2align 4,,15
	.globl	__GI__IO_sputbackwc
	.hidden	__GI__IO_sputbackwc
	.type	__GI__IO_sputbackwc, @function
__GI__IO_sputbackwc:
.LFB88:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$24, %rsp
	movq	160(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	16(%rax), %rdx
	jbe	.L230
	cmpl	%esi, -4(%rdx)
	je	.L238
.L230:
	movq	216(%rbx), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	cmpq	%rcx, %rax
	jbe	.L239
.L232:
	movq	%rbx, %rdi
	call	*48(%rbp)
.L231:
	cmpl	$-1, %eax
	je	.L229
	andl	$-17, (%rbx)
.L229:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L238:
	subq	$4, %rdx
	movq	%rdx, (%rax)
	movl	%esi, %eax
	jmp	.L231
	.p2align 4,,10
	.p2align 3
.L239:
	movl	%esi, 12(%rsp)
	call	_IO_vtable_check
	movl	12(%rsp), %esi
	jmp	.L232
.LFE88:
	.size	__GI__IO_sputbackwc, .-__GI__IO_sputbackwc
	.globl	_IO_sputbackwc
	.set	_IO_sputbackwc,__GI__IO_sputbackwc
	.p2align 4,,15
	.globl	_IO_sungetwc
	.type	_IO_sungetwc, @function
_IO_sungetwc:
.LFB89:
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$8, %rsp
	movq	160(%rdi), %rax
	movq	(%rax), %rdx
	cmpq	16(%rax), %rdx
	jbe	.L241
	leaq	-4(%rdx), %rcx
	movq	%rcx, (%rax)
	movl	-4(%rdx), %eax
.L242:
	cmpl	$-1, %eax
	je	.L240
	andl	$-17, (%rbx)
.L240:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	ret
	.p2align 4,,10
	.p2align 3
.L241:
	movq	216(%rdi), %rbp
	leaq	__start___libc_IO_vtables(%rip), %rdx
	leaq	__stop___libc_IO_vtables(%rip), %rax
	subq	%rdx, %rax
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	cmpq	%rsi, %rax
	jbe	.L249
.L243:
	movl	$-1, %esi
	movq	%rbx, %rdi
	call	*48(%rbp)
	jmp	.L242
	.p2align 4,,10
	.p2align 3
.L249:
	call	_IO_vtable_check
	jmp	.L243
.LFE89:
	.size	_IO_sungetwc, .-_IO_sungetwc
	.p2align 4,,15
	.globl	_IO_adjust_wcolumn
	.type	_IO_adjust_wcolumn, @function
_IO_adjust_wcolumn:
.LFB90:
	movslq	%edx, %rcx
	leaq	(%rsi,%rcx,4), %r8
	cmpq	%r8, %rsi
	jnb	.L251
	cmpl	$10, -4(%r8)
	leaq	-4(%r8), %rcx
	je	.L252
	movq	%rcx, %rax
	subq	%rsi, %rax
	movq	%rax, %rsi
	addq	$3, %rsi
	notq	%rsi
	andq	$-4, %rsi
	addq	%r8, %rsi
	jmp	.L254
	.p2align 4,,10
	.p2align 3
.L256:
	subq	$4, %rcx
	cmpl	$10, (%rcx)
	je	.L252
.L254:
	cmpq	%rsi, %rcx
	jne	.L256
.L251:
	leal	(%rdx,%rdi), %eax
	ret
	.p2align 4,,10
	.p2align 3
.L252:
	subq	%rcx, %r8
	sarq	$2, %r8
	leal	-1(%r8), %eax
	ret
.LFE90:
	.size	_IO_adjust_wcolumn, .-_IO_adjust_wcolumn
	.p2align 4,,15
	.globl	_IO_init_wmarker
	.type	_IO_init_wmarker, @function
_IO_init_wmarker:
.LFB91:
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$16, %rsp
	movl	(%rsi), %edx
	movq	%rsi, 8(%rdi)
	testb	$8, %dh
	jne	.L267
.L260:
	movq	160(%rsi), %rcx
	andb	$1, %dh
	movq	(%rcx), %rax
	jne	.L268
	subq	16(%rcx), %rax
	sarq	$2, %rax
	movl	%eax, 16(%rbx)
.L262:
	movq	96(%rsi), %rax
	movq	%rax, (%rbx)
	movq	%rbx, 96(%rsi)
	addq	$16, %rsp
	popq	%rbx
	ret
	.p2align 4,,10
	.p2align 3
.L268:
	subq	8(%rcx), %rax
	sarq	$2, %rax
	movl	%eax, 16(%rbx)
	jmp	.L262
	.p2align 4,,10
	.p2align 3
.L267:
	movq	%rsi, %rdi
	movq	%rsi, 8(%rsp)
	call	__GI__IO_switch_to_wget_mode
	movq	8(%rsp), %rsi
	movl	(%rsi), %edx
	jmp	.L260
.LFE91:
	.size	_IO_init_wmarker, .-_IO_init_wmarker
	.p2align 4,,15
	.globl	_IO_wmarker_delta
	.type	_IO_wmarker_delta, @function
_IO_wmarker_delta:
.LFB92:
	movq	8(%rdi), %rax
	testq	%rax, %rax
	je	.L273
	movq	160(%rax), %rcx
	movl	(%rax), %eax
	testb	$1, %ah
	movq	(%rcx), %rdx
	jne	.L274
	subq	16(%rcx), %rdx
	movl	16(%rdi), %eax
	sarq	$2, %rdx
	subl	%edx, %eax
	ret
	.p2align 4,,10
	.p2align 3
.L274:
	subq	8(%rcx), %rdx
	movl	16(%rdi), %eax
	sarq	$2, %rdx
	subl	%edx, %eax
	ret
.L273:
	movl	$-1, %eax
	ret
.LFE92:
	.size	_IO_wmarker_delta, .-_IO_wmarker_delta
	.p2align 4,,15
	.globl	_IO_seekwmark
	.type	_IO_seekwmark, @function
_IO_seekwmark:
.LFB93:
	movq	8(%rsi), %rcx
	movl	$-1, %eax
	cmpq	%rdi, %rcx
	jne	.L275
	movslq	16(%rsi), %rdx
	movl	(%rcx), %esi
	movq	160(%rcx), %rax
	movl	%esi, %edi
	andl	$256, %edi
	testl	%edx, %edx
	js	.L277
	testl	%edi, %edi
	movq	16(%rax), %r8
	je	.L279
	andl	$-257, %esi
	movl	%esi, (%rcx)
	movq	8(%rax), %rcx
	movq	80(%rax), %rsi
	movq	%rcx, 80(%rax)
	movq	64(%rax), %rcx
	movq	%rsi, 8(%rax)
	movq	%r8, 64(%rax)
	movq	%rcx, 16(%rax)
	movq	%rcx, %r8
.L279:
	leaq	(%r8,%rdx,4), %rdx
	movq	%rdx, (%rax)
	xorl	%eax, %eax
.L275:
	rep ret
	.p2align 4,,10
	.p2align 3
.L277:
	testl	%edi, %edi
	movq	8(%rax), %r8
	jne	.L279
	orl	$256, %esi
	movq	64(%rax), %rdi
	movl	%esi, (%rcx)
	movq	80(%rax), %rcx
	movq	16(%rax), %rsi
	movq	%r8, 80(%rax)
	movq	%rdi, 16(%rax)
	movq	%rcx, %r8
	movq	%rcx, 8(%rax)
	leaq	(%r8,%rdx,4), %rdx
	movq	%rsi, 64(%rax)
	movq	%rdx, (%rax)
	xorl	%eax, %eax
	jmp	.L275
.LFE93:
	.size	_IO_seekwmark, .-_IO_seekwmark
	.p2align 4,,15
	.globl	_IO_unsave_wmarkers
	.type	_IO_unsave_wmarkers, @function
_IO_unsave_wmarkers:
.LFB94:
	cmpq	$0, 96(%rdi)
	je	.L285
	movq	$0, 96(%rdi)
.L285:
	cmpq	$0, 72(%rdi)
	je	.L284
	jmp	__GI__IO_free_wbackup_area
	.p2align 4,,10
	.p2align 3
.L284:
	rep ret
.LFE94:
	.size	_IO_unsave_wmarkers, .-_IO_unsave_wmarkers
	.hidden	_IO_vtable_check
	.hidden	__stop___libc_IO_vtables
	.hidden	__start___libc_IO_vtables
	.hidden	__wmempcpy
	.hidden	__wmemmove
	.hidden	__wmemcpy
