	.text
#APP
	memmove = __GI_memmove
	memset = __GI_memset
	memcpy = __GI_memcpy
	.symver _IO_new_popen,_IO_popen@@GLIBC_2.2.5
	.symver __new_popen,popen@@GLIBC_2.2.5
	.symver _IO_new_proc_open,_IO_proc_open@@GLIBC_2.2.5
	.symver _IO_new_proc_close,_IO_proc_close@@GLIBC_2.2.5
#NO_APP
	.p2align 4,,15
	.globl	_IO_new_proc_close
	.type	_IO_new_proc_close, @function
_IO_new_proc_close:
	pushq	%r13
	pushq	%r12
	pushq	%rbp
	pushq	%rbx
	movq	%rdi, %rbx
	subq	$56, %rsp
	movl	__libc_pthread_functions_init(%rip), %ebp
	testl	%ebp, %ebp
	je	.L2
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	16(%rsp), %rdi
	xorl	%edx, %edx
#APP
# 259 "iopopen.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	unlock(%rip), %rsi
	call	*%rax
.L3:
	movq	%fs:16, %r12
	cmpq	%r12, 8+proc_file_chain_lock(%rip)
	je	.L4
#APP
# 260 "iopopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L5
	movl	$1, %edx
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %edx, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
.L6:
	movq	%r12, 8+proc_file_chain_lock(%rip)
.L4:
	movl	4+proc_file_chain_lock(%rip), %ecx
	movq	proc_file_chain(%rip), %rdx
	movl	$-1, %r12d
	leal	1(%rcx), %eax
	testq	%rdx, %rdx
	movl	%eax, 4+proc_file_chain_lock(%rip)
	je	.L7
	cmpq	%rbx, %rdx
	jne	.L9
	jmp	.L35
	.p2align 4,,10
	.p2align 3
.L10:
	cmpq	%rbx, %rax
	je	.L36
	movq	%rax, %rdx
.L9:
	movq	232(%rdx), %rax
	testq	%rax, %rax
	jne	.L10
	movl	$-1, %r12d
.L7:
	testl	%ecx, %ecx
	movl	%ecx, 4+proc_file_chain_lock(%rip)
	jne	.L12
	movq	$0, 8+proc_file_chain_lock(%rip)
#APP
# 272 "iopopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L13
	subl	$1, proc_file_chain_lock(%rip)
.L12:
	testl	%ebp, %ebp
	je	.L15
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 273 "iopopen.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L15:
	cmpl	$-1, %r12d
	je	.L16
	movl	112(%rbx), %edi
	call	__GI___close_nocancel
	testl	%eax, %eax
	js	.L16
	leaq	12(%rsp), %r12
	leaq	16(%rsp), %r13
	jmp	.L20
	.p2align 4,,10
	.p2align 3
.L37:
	movq	__libc_errno@gottpoff(%rip), %rax
	cmpl	$4, %fs:(%rax)
	jne	.L16
.L20:
	movl	__libc_pthread_functions_init(%rip), %edx
	testl	%edx, %edx
	je	.L17
	movq	104+__libc_pthread_functions(%rip), %rax
	movq	%r13, %rsi
	movl	$1, %edi
#APP
# 285 "iopopen.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L17:
	movl	224(%rbx), %edi
	xorl	%edx, %edx
	movq	%r12, %rsi
	call	__GI___waitpid
	movl	%eax, %ebp
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	je	.L18
	movq	104+__libc_pthread_functions(%rip), %rax
	xorl	%esi, %esi
	movl	16(%rsp), %edi
#APP
# 288 "iopopen.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L18:
	cmpl	$-1, %ebp
	je	.L37
	movl	12(%rsp), %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L16:
	addq	$56, %rsp
	movl	$-1, %eax
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	ret
	.p2align 4,,10
	.p2align 3
.L36:
	addq	$232, %rdx
.L8:
	movq	232(%rbx), %rax
	xorl	%r12d, %r12d
	movq	%rax, (%rdx)
	jmp	.L7
.L13:
#APP
# 272 "iopopen.c" 1
	xchgl %ecx, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %ecx
	jle	.L12
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	proc_file_chain_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 272 "iopopen.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L12
	.p2align 4,,10
	.p2align 3
.L2:
	leaq	unlock(%rip), %rax
	movq	$0, 24(%rsp)
	movq	%rax, 16(%rsp)
	jmp	.L3
	.p2align 4,,10
	.p2align 3
.L5:
	xorl	%eax, %eax
	movl	$1, %edx
	lock cmpxchgl	%edx, proc_file_chain_lock(%rip)
	je	.L6
	leaq	proc_file_chain_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L6
	.p2align 4,,10
	.p2align 3
.L35:
	leaq	proc_file_chain(%rip), %rdx
	jmp	.L8
	.size	_IO_new_proc_close, .-_IO_new_proc_close
	.p2align 4,,15
	.type	unlock, @function
unlock:
	movl	4+proc_file_chain_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+proc_file_chain_lock(%rip)
	jne	.L38
	movq	$0, 8+proc_file_chain_lock(%rip)
#APP
# 59 "iopopen.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L41
	subl	$1, proc_file_chain_lock(%rip)
	ret
	.p2align 4,,10
	.p2align 3
.L41:
#APP
# 59 "iopopen.c" 1
	xchgl %eax, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L38
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	proc_file_chain_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 59 "iopopen.c" 1
	syscall
	
# 0 "" 2
#NO_APP
.L38:
	rep ret
	.size	unlock, .-unlock
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"sh"
.LC1:
	.string	"-c"
.LC2:
	.string	"/bin/sh"
	.text
	.p2align 4,,15
	.globl	_IO_new_proc_open
	.type	_IO_new_proc_open, @function
_IO_new_proc_open:
	pushq	%r15
	pushq	%r14
	xorl	%r14d, %r14d
	pushq	%r13
	pushq	%r12
	movq	%rsi, %r13
	pushq	%rbp
	pushq	%rbx
	xorl	%ebp, %ebp
	movq	%rdi, %rbx
	xorl	%esi, %esi
	subq	$200, %rsp
.L44:
	movzbl	(%rdx), %eax
	testb	%al, %al
	je	.L91
.L50:
	addq	$1, %rdx
	cmpb	$114, %al
	je	.L46
	cmpb	$119, %al
	je	.L74
	cmpb	$101, %al
	je	.L92
.L45:
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$22, %fs:(%rax)
	xorl	%eax, %eax
.L43:
	addq	$200, %rsp
	popq	%rbx
	popq	%rbp
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	ret
	.p2align 4,,10
	.p2align 3
.L46:
	movzbl	(%rdx), %eax
	movl	$1, %r14d
	testb	%al, %al
	jne	.L50
.L91:
	cmpl	%esi, %r14d
	je	.L45
	cmpl	$-1, 112(%rbx)
	jne	.L90
	leaq	40(%rsp), %rax
	movl	$524288, %esi
	movq	%rax, %rdi
	movq	%rax, 24(%rsp)
	call	__pipe2
	testl	%eax, %eax
	js	.L90
	movl	%r14d, %eax
	leaq	112(%rsp), %r12
	xorl	$1, %eax
	cmpl	$1, %r14d
	movl	%eax, 20(%rsp)
	sbbl	%eax, %eax
	movq	%r12, %rdi
	andl	$-4, %eax
	addl	$8, %eax
	movl	%eax, 12(%rsp)
	call	__posix_spawn_file_actions_init
	movslq	%r14d, %rax
	movl	40(%rsp,%rax,4), %esi
	movq	%rax, (%rsp)
	cmpl	%r14d, %esi
	je	.L93
.L54:
	movl	%r14d, %edx
	movq	%r12, %rdi
	call	__posix_spawn_file_actions_adddup2
	testl	%eax, %eax
	jne	.L56
	movl	__libc_pthread_functions_init(%rip), %eax
	testl	%eax, %eax
	movl	%eax, 16(%rsp)
	je	.L57
	movq	184+__libc_pthread_functions(%rip), %rax
	leaq	48(%rsp), %rdi
	xorl	%edx, %edx
#APP
# 194 "iopopen.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	leaq	unlock(%rip), %rsi
	call	*%rax
.L58:
	movq	%fs:16, %r15
	cmpq	%r15, 8+proc_file_chain_lock(%rip)
	je	.L59
#APP
# 195 "iopopen.c" 1
	movl %fs:24,%eax
# 0 "" 2
#NO_APP
	testl	%eax, %eax
	jne	.L60
	movl	$1, %esi
#APP
# 47 "../sysdeps/unix/sysv/linux/x86/lowlevellock.h" 1
	cmpxchgl %esi, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
.L61:
	movq	%r15, 8+proc_file_chain_lock(%rip)
.L59:
	movq	proc_file_chain(%rip), %r15
	addl	$1, 4+proc_file_chain_lock(%rip)
	testq	%r15, %r15
	je	.L62
	.p2align 4,,10
	.p2align 3
.L64:
	movl	112(%r15), %esi
	cmpl	%esi, %r14d
	je	.L65
	movq	%r12, %rdi
	call	__posix_spawn_file_actions_addclose
	testl	%eax, %eax
	jne	.L67
.L65:
	movq	232(%r15), %r15
	testq	%r15, %r15
	jne	.L64
.L62:
	movq	__environ@GOTPCREL(%rip), %rax
	leaq	224(%rbx), %rdi
	leaq	80(%rsp), %r8
	leaq	.LC2(%rip), %rsi
	xorl	%ecx, %ecx
	movq	%r12, %rdx
	movq	%r13, 96(%rsp)
	movq	$0, 104(%rsp)
	movq	(%rax), %r9
	leaq	.LC0(%rip), %rax
	movq	%rax, 80(%rsp)
	leaq	.LC1(%rip), %rax
	movq	%rax, 88(%rsp)
	call	__GI___posix_spawn
	testl	%eax, %eax
	je	.L94
.L67:
	xorl	%ebp, %ebp
.L66:
	movl	4+proc_file_chain_lock(%rip), %eax
	subl	$1, %eax
	testl	%eax, %eax
	movl	%eax, 4+proc_file_chain_lock(%rip)
	jne	.L70
	movq	$0, 8+proc_file_chain_lock(%rip)
#APP
# 200 "iopopen.c" 1
	movl %fs:24,%edx
# 0 "" 2
#NO_APP
	testl	%edx, %edx
	jne	.L71
	subl	$1, proc_file_chain_lock(%rip)
.L70:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	je	.L73
	movq	192+__libc_pthread_functions(%rip), %rax
	leaq	48(%rsp), %rdi
	xorl	%esi, %esi
#APP
# 201 "iopopen.c" 1
	ror $2*8+1, %rax
xor %fs:48, %rax
# 0 "" 2
#NO_APP
	call	*%rax
.L73:
	movq	%r12, %rdi
	call	__posix_spawn_file_actions_destroy
	testb	%bpl, %bpl
	je	.L56
	movl	(%rbx), %eax
	movl	12(%rsp), %r14d
	andl	$-13, %eax
	orl	%eax, %r14d
	movq	%rbx, %rax
	movl	%r14d, (%rbx)
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L92:
	movl	$1, %ebp
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L74:
	movl	$1, %esi
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L93:
	xorl	%edx, %edx
	xorl	%eax, %eax
	movl	$1030, %esi
	movl	%r14d, %edi
	call	__GI___fcntl
	testl	%eax, %eax
	movl	%eax, %r15d
	jns	.L55
	.p2align 4,,10
	.p2align 3
.L56:
	movq	(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__GI___close_nocancel
	movslq	20(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__GI___close_nocancel
	movq	__libc_errno@gottpoff(%rip), %rax
	movl	$12, %fs:(%rax)
.L90:
	xorl	%eax, %eax
	jmp	.L43
	.p2align 4,,10
	.p2align 3
.L55:
	movq	(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__GI___close_nocancel
	movq	(%rsp), %rax
	movl	%r15d, %esi
	movl	%r15d, 40(%rsp,%rax,4)
	jmp	.L54
	.p2align 4,,10
	.p2align 3
.L94:
	movq	(%rsp), %rax
	movl	40(%rsp,%rax,4), %edi
	call	__GI___close_nocancel
	movslq	20(%rsp), %rax
	movq	24(%rsp), %rcx
	testl	%ebp, %ebp
	leaq	(%rcx,%rax,4), %r13
	jne	.L68
	movl	0(%r13), %edi
	xorl	%edx, %edx
	movl	$2, %esi
	xorl	%eax, %eax
	call	__GI___fcntl
.L68:
	movl	0(%r13), %eax
	movl	$1, %ebp
	movl	%eax, 112(%rbx)
	movq	proc_file_chain(%rip), %rax
	movq	%rbx, proc_file_chain(%rip)
	movq	%rax, 232(%rbx)
	jmp	.L66
.L57:
	leaq	unlock(%rip), %rax
	movq	$0, 56(%rsp)
	movq	%rax, 48(%rsp)
	jmp	.L58
.L60:
	xorl	%eax, %eax
	movl	$1, %esi
	lock cmpxchgl	%esi, proc_file_chain_lock(%rip)
	je	.L61
	leaq	proc_file_chain_lock(%rip), %rdi
	call	__lll_lock_wait_private
	jmp	.L61
.L71:
#APP
# 200 "iopopen.c" 1
	xchgl %eax, proc_file_chain_lock(%rip)
# 0 "" 2
#NO_APP
	cmpl	$1, %eax
	jle	.L70
	xorl	%r10d, %r10d
	movl	$1, %edx
	movl	$129, %esi
	leaq	proc_file_chain_lock(%rip), %rdi
	movl	$202, %eax
#APP
# 200 "iopopen.c" 1
	syscall
	
# 0 "" 2
#NO_APP
	jmp	.L70
	.size	_IO_new_proc_open, .-_IO_new_proc_open
	.p2align 4,,15
	.globl	_IO_new_popen
	.type	_IO_new_popen, @function
_IO_new_popen:
	pushq	%r12
	pushq	%rbp
	movq	%rdi, %rbp
	pushq	%rbx
	movl	$256, %edi
	movq	%rsi, %r12
	call	malloc@PLT
	testq	%rax, %rax
	je	.L97
	leaq	240(%rax), %rdx
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rax, %rbx
	movq	%rdx, 136(%rax)
	call	_IO_init_internal
	leaq	_IO_proc_jumps(%rip), %rax
	movq	%rbx, %rdi
	movq	%rax, 216(%rbx)
	call	_IO_new_file_init_internal
	movq	%r12, %rdx
	movq	%rbp, %rsi
	movq	%rbx, %rdi
	call	_IO_new_proc_open@PLT
	testq	%rax, %rax
	je	.L99
.L95:
	movq	%rbx, %rax
	popq	%rbx
	popq	%rbp
	popq	%r12
	ret
	.p2align 4,,10
	.p2align 3
.L97:
	xorl	%ebx, %ebx
	jmp	.L95
	.p2align 4,,10
	.p2align 3
.L99:
	movq	%rbx, %rdi
	call	__GI__IO_un_link
	movq	%rbx, %rdi
	xorl	%ebx, %ebx
	call	free@PLT
	jmp	.L95
	.size	_IO_new_popen, .-_IO_new_popen
	.globl	__new_popen
	.set	__new_popen,_IO_new_popen
	.local	proc_file_chain_lock
	.comm	proc_file_chain_lock,16,16
	.local	proc_file_chain
	.comm	proc_file_chain,8,8
	.section	__libc_IO_vtables,"aw",@progbits
	.align 32
	.type	_IO_proc_jumps, @object
	.size	_IO_proc_jumps, 168
_IO_proc_jumps:
	.quad	0
	.quad	0
	.quad	_IO_new_file_finish
	.quad	_IO_new_file_overflow
	.quad	_IO_new_file_underflow
	.quad	__GI__IO_default_uflow
	.quad	__GI__IO_default_pbackfail
	.quad	_IO_new_file_xsputn
	.quad	__GI__IO_default_xsgetn
	.quad	_IO_new_file_seekoff
	.quad	_IO_default_seekpos
	.quad	_IO_new_file_setbuf
	.quad	_IO_new_file_sync
	.quad	__GI__IO_file_doallocate
	.quad	__GI__IO_file_read
	.quad	_IO_new_file_write
	.quad	__GI__IO_file_seek
	.quad	_IO_new_proc_close
	.quad	__GI__IO_file_stat
	.quad	_IO_default_showmanyc
	.quad	_IO_default_imbue
	.hidden	_IO_new_file_init_internal
	.hidden	_IO_init_internal
	.hidden	__posix_spawn_file_actions_destroy
	.hidden	__posix_spawn_file_actions_addclose
	.hidden	__posix_spawn_file_actions_adddup2
	.hidden	__posix_spawn_file_actions_init
	.hidden	__pipe2
	.hidden	__lll_lock_wait_private
	.hidden	__libc_pthread_functions
	.hidden	__libc_pthread_functions_init
